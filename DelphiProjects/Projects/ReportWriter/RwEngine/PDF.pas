{**************************************************}
{                                                  }
{                   llPDFLib                       }
{            Version  2.3,  10.04.2003             }
{      Copyright (c) 2002, 2003 llionsoft          }
{             All rights reserved                  }
{            mailto:info@llion.net                 }
{                                                  }
{**************************************************}
{$I pdf.inc}
unit PDF;

interface

uses Windows, SysUtils, Classes, Graphics, Math, Jpeg, ShellApi, pdfMisc, EvStreamUtils, isVCLBugFix;

const
  llPDFLibDefaultCharset: TFontCharset = ANSI_Charset;

type
  TDoublePoint = record
    x, y: Extended;
  end;

  TPDFPageOrientation = (poPagePortrait, poPageLandscape);
  TPDFVersion = (v13, v14);
  TPDFPageType = (ptPage, ptXForm, ptAnnotation);
  TPDFLineJoin = (ljMiter, ljRound, ljBevel);
  TPDFLineCap = (lcButtEnd, lcRound, lcProjectingSquare);
  TCJKFont = (cjkChinaTrad, cjkChinaSimpl, cjkJapan, cjkKorean);
  TPDFSubmitType = (stGet, stPost, stFDF);

  TViewerPreference = (vpHideToolBar, vpHideMenuBar, vpHideWindowUI,
    vpFitWindow, vpCenterWindow);
  TViewerPreferences = set of TViewerPreference;

  TAnnotationFlag = (afInvisible, afHidden, afPrint, afNoZoom, afNoRotate, afNoView, afReadOnly);

  TAnnotationFlags = set of TAnnotationFlag;
  TEncripKey = array[1..16] of Byte;

  THorJust = (hjLeft, hjCenter, hjRight);
  TVertJust = (vjUp, vjCenter, vjDown);

  TCompressionType = (ctNone, ctFlate);
  TImageCompressionType = (itcFlate, itcJpeg, itcCCITT3, itcCCITT32d, itcCCITT4);
  TPDFCriptoOption = (coPrint, coModifyStructure, coCopyInformation, coModifyAnnotation);
  TPDFCtiptoOptions = set of TPDFCriptoOption;
  TPDFKeyLength = (kl40, kl128);

  TPageLayout = (plSinglePage, plOneColumn, plTwoColumnLeft, plTwoColumnRight);

  TPageMode = (pmUseNone, pmUseOutlines, pmUseThumbs, pmFullScreen);
  TPDFPageRotate = (pr0, pr90, pr180, pr270);
  TPDFFontEmbeddingType = (fetFullFont, fetFullCharset, fetUsedGlyphsOnly, fetNotEmbed);

  TPDFException = class(Exception);

  TPDFDocument = class;
  TPDFFonts = class;
  TPDFImages = class;
  TPDFImage = class;
  TPDFPage = class;

  TPDFControlHint = class(TObject)
  private
    FCaption: string;
    FCharset: TFontCharset;
  public
    property Caption: string read FCaption write FCaption;
    property Charset: TFontCharset read FCharset write FCharset;
  end;

  TPDFControlFont = class
  private
    FSize: Integer;
    FName: string;
    FColor: TColor;
    FCharset: TFontCharset;
    FStyle: TFontStyles;
    procedure SetCharset(const Value: TFontCharset);
    procedure SetSize(const Value: Integer);
  public
    constructor Create;
    property Name: string read FName write FName;
    property Charset: TFontCharset read FCharset write SetCharset;
    property Style: TFontStyles read FStyle write FStyle;
    property Size: Integer read FSize write SetSize;
    property Color: TColor read FColor write FColor;
  end;

  TPDFPageSize = (psUserDefined, psLetter, psA4, psA3, psLegal, psB5, psC5, ps8x11, psB4,
    psA5, psFolio, psExecutive, psEnvB4, psEnvB5, psEnvC6, psEnvDL, psEnvMonarch,
    psEnv9, psEnv10, psEnv11);

  TPDFDocInfo = class(TPersistent)
  private
    Producer: string;
    ID: Integer;
    FKeywords: string;
    FTitle: string;
    FCreator: string;
    FAuthor: string;
    FSubject: string;
    FCreationDate: TDateTime;
    procedure SetAuthor(const Value: string);
    procedure SetCreationDate(const Value: TDateTime);
    procedure SetCreator(const Value: string);
    procedure SetKeywords(const Value: string);
    procedure SetSubject(const Value: string);
    procedure SetTitle(const Value: string);
  public
    property CreationDate: TDateTime read FCreationDate write SetCreationDate;
  published
    property Author: string read FAuthor write SetAuthor;
    property Creator: string read FCreator write SetCreator;
    property Keywords: string read FKeywords write SetKeywords;
    property Subject: string read FSubject write SetSubject;
    property Title: string read FTitle write SetTitle;
  end;

  TTextAnnotationIcon = (taiComment, taiKey, taiNote, taiHelp, taiNewParagraph, taiParagraph, taiInsert);
  TPDFControl = class;

  TPDFControls = class
  private
    FControls: TList;
    function GetCount: Integer;
    function GetControl(Index: Integer): TPDFControl;
  public
    constructor Create;
    destructor Destroy; override;
    function Add(Control: TPDFControl): Integer;
    function IndexOf(Control: TPDFControl): Integer;
    procedure Delete(Control: TPDFControl);
    procedure Clear;
    property Count: Integer read GetCount;
    property Items[Index: Integer]: TPDFControl read GetControl; default;
  end;

  TPDFAction = class(TObject)
  private
    FOwner: TPDFDocument;
    ActionID: Integer;
    FNext: TPDFAction;
    procedure Prepare;
    procedure Save; virtual; abstract;
  public
    constructor Create; virtual;
  end;

  TPDFActionClass = class of TPDFAction;

  TPDFURLAction = class(TPDFAction)
  private
    FURL: string;
    procedure Save; override;
    procedure SetURL(const Value: string);
  public
    property URL: string read FURL write SetURL;
  end;

  TPDFJavaScriptAction = class(TPDFAction)
  private
    FJavaScript: string;
    procedure Save; override;
    procedure SetJavaScript(const Value: string);
  public
    property JavaScript: string read FJavaScript write SetJavaScript;
  end;

  TPDFGoToPageAction = class(TPDFAction)
  private
    FTopOffset: Integer;
    FPageIndex: Integer;
    procedure Save; override;
    procedure SetPageIndex(const Value: Integer);
    procedure SetTopOffset(const Value: Integer);
  public
    property PageIndex: Integer read FPageIndex write SetPageIndex;
    property TopOffset: Integer read FTopOffset write SetTopOffset;
  end;

  TPDFVisibeControlAction = class(TPDFAction)
  private
    FVisible: Boolean;
    FControls: TPDFControls;
    procedure Save; override;
  public
    constructor Create; override;
    destructor Destroy; override;
    property Visible: Boolean read FVisible write FVisible;
    property Controls: TPDFControls read FControls;
  end;

  TPDFSubmitFormAction = class(TPDFAction)
  private
    FFields: TPDFControls;
    FRG: TList;
    FIncludeEmptyFields: Boolean;
    FURL: string;
    FSubmitType: TPDFSubmitType;
    procedure Save; override;
    procedure SetURL(const Value: string);
  public
    constructor Create; override;
    destructor Destroy; override;
    property IncludeFields: TPDFControls read FFields;
    property URL: string read FURL write SetURL;
    property SendEmpty: Boolean read FIncludeEmptyFields write FIncludeEmptyFields;
    property SubmitType: TPDFSubmitType read FSubmitType write FSubmitType;
  end;

  TPDFResetFormAction = class(TPDFAction)
  private
    FFields: TPDFControls;
    FRG: TList;
    procedure Save; override;
  public
    constructor Create; override;
    destructor Destroy; override;
    property ResetFields: TPDFControls read FFields;
  end;

  TPDFImportDataAction = class(TPDFAction)
  private
    FFileName: string;
    procedure Save; override;
    procedure SetFileName(const Value: string);
  public
    property FileName: string read FFileName write SetFileName;
  end;

  TPDFCustomAnnotation = class(TObject)
  private
    FOwner: TPDFPage;
    AnnotID: Integer;
    FLeft, FTop, FRight, FBottom: Integer;
    FFlags: TAnnotationFlags;
    FBorderStyle: string;
    FBorderColor: TColor;
    function CalcFlags: Integer;
    procedure Save; virtual; abstract;
    function GetBox: TRect;
    procedure SetBox(const Value: TRect);
  public
    constructor Create(AOwner: TPDFPage);
    property Box: TRect read GetBox write SetBox;
    property BorderStyle: string read FBorderStyle write FBorderStyle;
    property Flags: TAnnotationFlags read FFlags write FFlags;
    property BorderColor: TColor read FBorderColor write FBorderColor;
  end;


  TPDFControl = class(TPDFCustomAnnotation)
  private
    FRequired: Boolean;
    FReadOnly: Boolean;
    FName: string;
    FOnMouseDown: TPDFAction;
    FOnMouseExit: TPDFAction;
    FOnMouseEnter: TPDFAction;
    FOnMouseUp: TPDFAction;
    FOnLostFocus: TPDFAction;
    FOnSetFocus: TPDFAction;
    FColor: TColor;
    FFont: TPDFControlFont;
    FFN: string;
    FHint: TPDFControlHint;
    procedure SetName(const Value: string);
    procedure SetOnMouseDown(const Value: TPDFAction);
    procedure SetOnMouseEnter(const Value: TPDFAction);
    procedure SetOnMouseExit(const Value: TPDFAction);
    procedure SetOnMouseUp(const Value: TPDFAction);
    procedure SetOnLostFocus(const Value: TPDFAction);
    procedure SetOnSetFocus(const Value: TPDFAction);
    procedure SetColor(const Value: TColor);
    function CalcActions: string;
  public
    constructor Create(AOwner: TPDFPage; AName: string); virtual;
    destructor Destroy; override;
    property ReadOnly: Boolean read FReadOnly write FReadOnly;
    property Required: Boolean read FRequired write FRequired;
    property Name: string read FName write SetName;
    property Color: TColor read FColor write SetColor;
    property Font: TPDFControlFont read FFont;
    property Hint: TPDFControlHint read FHint;
    property OnMouseUp: TPDFAction read FOnMouseUp write SetOnMouseUp;
    property OnMouseDown: TPDFAction read FOnMouseDown write SetOnMouseDown;
    property OnMouseEnter: TPDFAction read FOnMouseEnter write SetOnMouseEnter;
    property OnMouseExit: TPDFAction read FOnMouseExit write SetOnMouseExit;
    property OnSetFocus: TPDFAction read FOnSetFocus write SetOnSetFocus;
    property OnLostFocus: TPDFAction read FOnLostFocus write SetOnLostFocus;
  end;

  TPDFControlClass = class of TPDFControl;

  TPDFRadioGroup = class
  private
    FButtons: TPDFControls;
    FOwner: TPDFDocument;
    GroupID: Integer;
    FName: string;
    procedure Save;
  public
    constructor Create(AOwner: TPDFDocument; Name: string);
    destructor Destroy; override;
  end;


  TPDFButton = class(TPDFControl)
  private
    FCaption: string;
    procedure Save; override;
  protected
    FUp: TPDFPage;
    FDown: TPDFPage;
    procedure Paint; virtual;
  public
    constructor Create(AOwner: TPDFPage; AName: string); override;
    property Caption: string read FCaption write FCaption;
  end;

  TPDFInputControl = class(TPDFControl)
  private
    FOnOtherControlChanged: TPDFJavaScriptAction;
    FOnKeyPress: TPDFJavaScriptAction;
    FOnBeforeFormatting: TPDFJavaScriptAction;
    FOnChange: TPDFJavaScriptAction;
    procedure SetOnOtherControlChanged(const Value: TPDFJavaScriptAction);
    procedure SetOnKeyPress(const Value: TPDFJavaScriptAction);
    procedure SetOnBeforeFormatting(const Value: TPDFJavaScriptAction);
    procedure SetOnChange(const Value: TPDFJavaScriptAction);
    function CalcActions: string;
  public
    property OnKeyPress: TPDFJavaScriptAction read FOnKeyPress write SetOnKeyPress;
    property OnBeforeFormatting: TPDFJavaScriptAction read FOnBeforeFormatting write SetOnBeforeFormatting;
    property OnChange: TPDFJavaScriptAction read FOnChange write SetOnChange;
    property OnOtherControlChanged: TPDFJavaScriptAction read FOnOtherCOntrolChanged write SetOnOtherCOntrolChanged;
  end;

  TPDFCheckBox = class(TPDFInputControl)
  private
    FChecked: Boolean;
    FCaption: string;
    procedure Save; override;
  protected
    FCheck: TPDFPage;
    FUnCheck: TPDFPage;
    procedure Paint; virtual;
  public
    constructor Create(AOwner: TPDFPage; AName: string); override;
    property Checked: Boolean read FChecked write FChecked;
    property Caption: string read FCaption write FCaption;
  end;

  TPDFRadioButton = class(TPDFInputControl)
  private
    FRG: TPDFRadioGroup;
    FChecked: Boolean;
    FExportValue: string;
    procedure Save; override;
    procedure SetExportValue(const Value: string);
  protected
    FCheck: TPDFPage;
    FUnCheck: TPDFPage;
    procedure Paint; virtual;
  public
    constructor Create(AOwner: TPDFPage; AName: string); override;
    property ExportValue: string read FExportValue write SetExportValue;
    property Checked: Boolean read FChecked write FChecked;
  end;


  TPDFEdit = class(TPDFInputControl)
  private
    FText: string;
    FIsPassword: Boolean;
    FShowBorder: Boolean;
    FMultiline: Boolean;
    FMaxLength: Integer;
    FJustification: THorJust;
    procedure Save; override;
    procedure SetMaxLength(const Value: Integer);
  protected
    FShow: TPDFPage;
    procedure Paint; virtual;
  public
    constructor Create(AOwner: TPDFPage; AName: string); override;
    property Text: string read FText write FText;
    property Multiline: Boolean read FMultiline write FMultiline;
    property IsPassword: Boolean read FIsPassword write FIsPassword;
    property ShowBorder: Boolean read FShowBorder write FShowBorder;
    property MaxLength: Integer read FMaxLength write SetMaxLength;
    property Justification: THorJust read FJustification write FJustification;
  end;

  TPDFComboBox = class(TPDFInputControl)
  private
    FEditEnabled: Boolean;
    FItems: TStringList;
    FText: string;
    procedure Save; override;
  protected
    FShow: TPDFPage;
    procedure Paint; virtual;
  public
    constructor Create(AOwner: TPDFPage; AName: string); override;
    destructor Destroy; override;
    property Items: TStringList read FItems;
    property EditEnabled: Boolean read FEditEnabled write FEditEnabled;
    property Text: string read FText write FText;
  end;

  TPDFTextAnnotation = class(TPDFCustomAnnotation)
  private
    FText: string;
    FCaption: string;
    FTextAnnotationIcon: TTextAnnotationIcon;
    FOpened: Boolean;
    FCharset: TFontCharset;
    procedure Save; override;
  public
    property Caption: string read FCaption write FCaption;
    property Text: string read FText write FText;
    property TextAnnotationIcon: TTextAnnotationIcon read FTextAnnotationIcon write FTextAnnotationIcon;
    property Opened: Boolean read FOpened write FOpened;
    property Charset: TFontCharset read FCharset write FCharset;
  end;

  TPDFActionAnnotation = class(TPDFCustomAnnotation)
  private
    FAction: TPDFAction;
    procedure Save; override;
    procedure SetAction(const Value: TPDFAction);
  public
    property Action: TPDFAction read FAction write SetAction;
  end;

  TPDFActions = class(TObject)
  private
    FOwner: TPDFDocument;
    FActions: TList;
    function GetCount: Integer;
    function GetAction(Index: Integer): TPDFAction;
    procedure Clear;
  public
    constructor Create(AOwner: TPDFDocument);
    destructor Destroy; override;
    function Add(Action: TPDFAction): Integer;
    function IndexOf(Action: TPDFAction): Integer;
    procedure Delete(Action: TPDFAction);
    property Count: Integer read GetCount;
    property Items[Index: Integer]: TPDFAction read GetAction; default;
  end;

  TPDFAcroForm = class(TObject)
  private
    AcroID: Integer;
    FOwner: TPDFDocument;
    FFields: TPDFControls;
    FFonts: TList;
    FRadioGroups: TList;
    procedure Save;
    function GetEmpty: Boolean;
  public
    constructor Create(AOwner: TPDFDocument);
    procedure Clear;
    destructor Destroy; override;
    property Empty: Boolean read GetEmpty;
  end;

  TPDFPages = class;

  TPDFPage = class(TObject)
  public
    FContent: TStringList;
  private
    FOwner: TPDFDocument;
    FAnnot: TList;
    FMatrix: TTextCTM;
    FForms: TPDFPages;
    FIsForm: Boolean;
    FRes: Integer;
    D2P: Extended;
    // ProcSet Options
    FTextUsed: Boolean;
    FGrayUsed: Boolean;
    FColorUsed: Boolean;
    FAskCanvas: Boolean;
    FHeight: Integer;
    FWidth: Integer;
    FPageSize: TPDFPageSize;
    FX: Extended;
    FY: Extended;
    FRotate: TPDFPageRotate;
    FLinkedFont: TList;
    FLinkedImages: TList;
    ResourceID: Integer;
    PageID: Integer;
    ContentID: Integer;
    FEBold: Boolean;
    FEItalic: Boolean;
    FEUnderLine: Boolean;
    FEStrike: Boolean;
    FCTM: TTextCTM;
    FF: TTextCTM;
    FCurrentFontSize: Extended;
    FCurrentFontIndex: Integer;
    FCurrentDash: string;
    FHorizontalScaling: Extended;
    FCharSpace: Extended;
    FWordSpace: Extended;
    FRise: Extended;
    FRender: Integer;
    FFontInited: Boolean;
    FTextInited: Boolean;
    FTextLeading: Extended;
    FSaveCount: Integer;
    FTextAngle: Extended;
    FRealAngle: Extended;
    FOrientation: TPDFPageOrientation;
    Factions: Boolean;
    FMF: TMetafile;
    FCanvas: TCanvas;
    FWaterMark: Integer;
    FThumbnail: Integer;
    FRemoveCR: Boolean;
    FEmulationEnabled: Boolean;
    FCanvasOver: Boolean;
    procedure SetHeight(const Value: Integer);
    procedure SetWidth(const Value: Integer);
    procedure SetPageSize(Value: TPDFPageSize);
    procedure Save;
    function IntToExtX(AX: Extended): Extended;
    function IntToExtY(AY: Extended): Extended;
    function ExtToIntX(AX: Extended): Extended;
    function ExtToIntY(AY: Extended): Extended;
    procedure DrawArcWithBezier(CenterX, CenterY, RadiusX, RadiusY, StartAngle, SweepRange: Extended; UseMoveTo: Boolean = True);
    procedure SetRotate(const Value: TPDFPageRotate);
    procedure RawSetTextPosition(X, Y: Extended);
    procedure RawTextOut(X, Y, Orientation: Extended; TextStr: string);
    procedure RawLineTo(X, Y: Extended);
    procedure RawMoveTo(X, Y: Extended);
    procedure RawRect(X, Y, W, H: Extended);
    function RawArc(X1, Y1, x2, y2, x3, y3, x4, y4: Extended): TDoublePoint; overload;
    function RawArc(X1, Y1, x2, y2, BegAngle, EndAngle: Extended): TDoublePoint; overload;
    function RawPie(X1, Y1, x2, y2, x3, y3, x4, y4: Extended): TDoublePoint; overload;
    function RawPie(X1, Y1, x2, y2, BegAngle, EndAngle: Extended): TDoublePoint; overload;
    procedure RawCircle(X, Y, R: Extended);
    procedure RawEllipse(x1, y1, x2, y2: Extended);
    procedure RawRectRotated(X, Y, W, H, Angle: Extended);
    procedure RawShowImage(ImageIndex: Integer; x, y, w, h, angle: Extended);
    procedure RawConcat(A, B, C, D, E, F: Extended);
    procedure RawTranslate(XT, YT: Extended);
    procedure DeleteAllAnnotations;
    procedure RawCurveto(X1, Y1, X2, Y2, X3, Y3: Extended);
    procedure ConcatTextMatrix(A, B, C, D, X, Y: Extended);
    procedure SetTextMatrix(A, B, C, D, X, Y: Extended);
    procedure ResetTextCTM;
    procedure SetOrientation(const Value: TPDFPageOrientation);
    function GetHeight: Integer;
    function GetWidth: Integer;
    procedure CreateCanvas;
    procedure DeleteCanvas;
    procedure CloseCanvas;

    procedure SetWaterMark(const Value: Integer);
    procedure SetThumbnail(const Value: Integer);
    procedure PrepareID;
  public
    constructor Create(AOwner: TPDFDocument);
    destructor Destroy; override;

    // Annotation
    function SetAnnotation(ARect: TRect; Title, Text: string; Color: TColor; Flags: TAnnotationFlags; Opened: Boolean; Charset: TFontCharset = ANSI_CHARSET): TPDFCustomAnnotation;
    function SetUrl(ARect: TRect; URL: string): TPDFCustomAnnotation;
    function SetAction(ARect: TRect; Action: TPDFAction): TPDFCustomAnnotation;
    function SetLinkToPage(ARect: TRect; PageIndex: Integer; TopOffset: Integer): TPDFCustomAnnotation;
    function CreateControl(CClass: TPDFControlClass; ControlName: string; Box: TRect): TPDFControl;

    // Text operation
    procedure BeginText;
    procedure EndText;
    procedure RotateText(Degrees: Extended);
    procedure SetActiveFont(FontName: string; FontStyle: TFontStyles; FontSize: Extended; FontCharset: TFontCharset = ANSI_CHARSET);
    procedure SetCharacterSpacing(Spacing: Extended);
    procedure SetHorizontalScaling(Scale: Extended);
    procedure SetLineCap(LineCap: TPDFLineCap);
    procedure SetLineJoin(LineJoin: TPDFLineJoin);
    procedure SetMiterLimit(MiterLimit: Extended);
    procedure SetTextPosition(X, Y: Extended);
    procedure SetTextRenderingMode(Mode: integer);
    procedure SetTextRise(Rise: Extended);
    procedure SetWordSpacing(Spacing: Extended);
    procedure SkewText(Alpha, Beta: Extended);
{$IFDEF CB}
    procedure TextOutput(X, Y, Orientation: Extended; TextStr: string);
{$ELSE}
    procedure TextOut(X, Y, Orientation: Extended; TextStr: string);
{$ENDIF}
    function TextOutBox(LTCornX, LTCornY, Interval, BoxWidth, BoxHeight: integer; TextStr: string): integer;
    procedure TextShow(TextStr: string);
    function GetTextWidth(Text: string): Extended;
    procedure TextBox(Rect: TRect; Text: string; Hor: THorJust; Vert: TVertJust);
    // Path operation
    procedure Clip;
    procedure EoClip;
    procedure EoFill;
    procedure EoFillAndStroke;
    procedure Fill;
    procedure FillAndStroke;
    procedure NewPath;
    procedure ClosePath;
    procedure Stroke;

    // Graphic primitive operation
    procedure LineTo(X, Y: Extended);
    procedure MoveTo(X, Y: Extended);
    procedure Curveto(X1, Y1, X2, Y2, X3, Y3: Extended);
    procedure Rectangle(X1, Y1, X2, Y2: Extended);
    // Advanced graphic operation
    procedure Circle(X, Y, R: Extended);
    procedure Ellipse(X1, Y1, X2, Y2: Extended);
    function Arc(X1, Y1, x2, y2, x3, y3, x4, y4: Extended): TDoublePoint; overload;
    function Arc(X1, Y1, x2, y2, BegAngle, EndAngle: Extended): TDoublePoint; overload;
    procedure Pie(X1, Y1, x2, y2, x3, y3, x4, y4: Extended); overload;
    procedure Pie(X1, Y1, x2, y2, BegAngle, EndAngle: Extended); overload;
    procedure RectRotated(X, Y, W, H, Angle: Extended);
    procedure RoundRect(X1, Y1, X2, Y2, X3, Y3: Integer);
    // Graphic state operation
    procedure GStateRestore;
    procedure GStateSave;
    procedure NoDash;
    procedure SetCMYKColor(C, M, Y, K: Extended);
    procedure SetCMYKColorFill(C, M, Y, K: Extended);
    procedure SetCMYKColorStroke(C, M, Y, K: Extended);
    procedure SetRGBColor(R, G, B: Extended);
    procedure SetRGBColorFill(R, G, B: Extended);
    procedure SetRGBColorStroke(R, G, B: Extended);
    procedure SetDash(DashSpec: string);
    procedure SetFlat(FlatNess: integer);
    procedure SetGray(Gray: Extended);
    procedure SetGrayFill(Gray: Extended);
    procedure SetGrayStroke(Gray: Extended);
    procedure SetLineWidth(lw: Extended);

    // Transform operation
    procedure Concat(A, B, C, D, E, F: Extended);
    procedure Scale(SX, SY: Extended);
    procedure Rotate(Angle: Extended);
    procedure Translate(XT, YT: Extended);
    // Picture
    procedure ShowImage(ImageIndex: Integer; x, y, w, h, angle: Extended);

    // Other
    procedure Comment(st: string);
    procedure AppendAction(Action: string);
    procedure PlayMetaFile(MF: TMetaFile); overload;
    procedure PlayMetaFile(MF: TMetafile; x, y, XScale, YScale: Extended); overload;
    procedure LoadCurrentFontWidth(var Arr: TLargeWidthArray);
    property Orientation: TPDFPageOrientation read FOrientation write SetOrientation;
    property Size: TPDFPageSize read FPageSize write SetPageSize;
    property Height: Integer read GetHeight write SetHeight;
    property Width: Integer read GetWidth write SetWidth;
    property PageRotate: TPDFPageRotate read FRotate write SetRotate;
    property WaterMark: Integer read FWaterMark write SetWaterMark;
    property Thumbnail: Integer read FThumbnail write SetThumbnail;
    property TextInited: Boolean read FTextInited;
    property CanvasOver: Boolean read FCanvasOver write FCanvasOver;
    property Owner: TPDFDocument read FOwner;
  end;

  TPDFPages = class(TObject)
  private
    IsWaterMarks: Boolean;
    FOwner: TPDFDocument;
    FPages: TList;
    function GetCount: Integer;
    function GetPage(Index: Integer): TPDFPage;
    procedure Clear;
  public
    constructor Create(AOwner: TPDFDocument; WM: Boolean);
    destructor Destroy; override;
    function Add: TPDFPage;
    function IndexOf(Page: TPDFPage): Integer;
    procedure Delete(Page: TPDFPage);
    property Count: Integer read GetCount;
    property Items[Index: Integer]: TPDFPage read GetPage; default;
  end;

  TPDFOutlines = class;

  TPDFOutlineNode = class(TObject)
  private
    OutlineNodeID: Integer;
    FChild: TList;
    FOwner: TPDFOutlines;
    FParent: TPDFOutlineNode;
    FPrev: TPDFOutlineNode;
    FNext: TPDFOutlineNode;
    FTitle: string;
    FExpanded: Boolean;
    FCharset: TFontCharset;
    FAction: TPDFAction;
    FColor: TColor;
    FStyle: TFontStyles;
    procedure SetTitle(const Value: string);
    function GetCount: Integer;
    function GetHasChildren: Boolean;
    function GetItem(Index: Integer): TPDFOutlineNode;
    procedure Save;
    procedure SetExpanded(const Value: Boolean);
    procedure SetCharset(const Value: TFontCharset);
    procedure SetAction(const Value: TPDFAction);
  public
    procedure Delete;
    procedure DeleteChildren;
    constructor Create(AOwner: TPDFOutlines);
    destructor Destroy; override;
    function GetFirstChild: TPDFOutlineNode;
    function GetLastChild: TPDFOutlineNode;
    function GetNext: TPDFOutlineNode;
    function GetNextChild(Node: TPDFOutlineNode): TPDFOutlineNode;
    function GetNextSibling: TPDFOutlineNode;
    function GetPrev: TPDFOutlineNode;
    function GetPrevChild(Node: TPDFOutlineNode): TPDFOutlineNode;
    function GetPrevSibling: TPDFOutlineNode;
    property Action: TPDFAction read FAction write SetAction;
    property Title: string read FTitle write SetTitle;
    property Color: TColor read FColor write FColor;
    property Style: TFontStyles read FStyle write FStyle;
    property Count: Integer read GetCount;
    property HasChildren: Boolean read GetHasChildren;
    property Item[Index: Integer]: TPDFOutlineNode read GetItem;
    property Expanded: Boolean read FExpanded write SetExpanded;
    property Charset: TFontCharset read FCharset write SetCharset;
  end;

  TPDFOutlines = class(TObject)
  private
    OutlinesID: Integer;
    FList: TList;
    FOwner: TPDFDocument;
    function GetCount: Integer;
    function GetItem(Index: Integer): TPDFOutlineNode;
    function Add(Node: TPDFOutlineNode): TPDFOutlineNode; overload;
    function AddChild(Node: TPDFOutlineNode): TPDFOutlineNode; overload;
    function AddChildFirst(Node: TPDFOutlineNode): TPDFOutlineNode; overload;
    function AddFirst(Node: TPDFOutlineNode): TPDFOutlineNode; overload;
    function Insert(Node: TPDFOutlineNode): TPDFOutlineNode; overload;
  public
    procedure Clear;
    procedure Delete(Node: TPDFOutlineNode);
    function GetFirstNode: TPDFOutlineNode;

    function Add(Node: TPDFOutlineNode; Title: string; Action: TPDFAction; Charset: TFontCharset = ANSI_CHARSET): TPDFOutlineNode; overload;
    function AddChild(Node: TPDFOutlineNode; Title: string; Action: TPDFAction; Charset: TFontCharset = ANSI_CHARSET): TPDFOutlineNode; overload;
    function AddChildFirst(Node: TPDFOutlineNode; Title: string; Action: TPDFAction; Charset: TFontCharset = ANSI_CHARSET): TPDFOutlineNode; overload;
    function AddFirst(Node: TPDFOutlineNode; Title: string; Action: TPDFAction; Charset: TFontCharset = ANSI_CHARSET): TPDFOutlineNode; overload;
    function Insert(Node: TPDFOutlineNode; Title: string; Action: TPDFAction; Charset: TFontCharset = ANSI_CHARSET): TPDFOutlineNode; overload;

    constructor Create(AOwner: TPDFDocument);
    destructor Destroy; override;
    property Count: Integer read GetCount;
    property Item[Index: Integer]: TPDFOutlineNode read GetItem; default;
  end;

  TPDFFont = class(TObject)
  private
    FontID: Integer;
    FontDescriptorID: Integer;
    FontFileID: Integer;
    FontName: string;
    Standart: Boolean;
    AliasName: string;
    StdID: Byte;
    IsCJK: Boolean;
    CJKType: TCJKFont;
    FWidth: TWidthArray;
    WidthLoaded: Boolean;
    FTS: TPDFFontEmbeddingType;
    procedure Save(Doc: TPDFDocument);
    procedure LoadingWidth;
    function GetWidth(Index: Integer): Word;
  public
    RealStyle: TFontStyles;
    RealCharset: TFontCharset;
    RealName: string;
    FUsed: array[0..255] of Boolean;
    constructor Create;
  end;

  TPDFFonts = class(TObject)
  private
    FOwner: TPDFDocument;
    FFonts: TList;
    FLast: TPDFFont;
    FLastIndex: Integer;
    function GetCount: Integer;
    function GetFont(Index: Integer): TPDFFont;
    procedure EmbiddingFontFiles;
  public
    constructor Create(AOwner: TPDFDocument);
    destructor Destroy; override;
    procedure Delete(Index: Integer);
    procedure Clear;
    function Add: TPDFFont;
    function IndexOf(FontName: TFontName; FontStyle: TFontStyles; FontCharset: TFontCharset): Integer;
    function CheckFont(FontName: TFontName; FontStyle: TFontStyles; FontCharset: TFontCharset): string;
    property Count: Integer read GetCount;
    property Items[Index: Integer]: TPDFFont read GetFont; default;
  end;

  TPDFImage = class(TObject)
  private
    FDoc: TPDFDocument;
    FCT: TImageCompressionType;
    ImageName: string;
    PictureID: Integer;
    FHeight: Integer;
    FWidth: Integer;
    Data: TMemoryStream;
    FBitPerPixel: Integer;
    GrayScale: Boolean;
    Saved: Boolean;
    FIsMask: Boolean;
    FMaskIndex: Integer;
    procedure Save(Doc: TPDFDocument);
  public
    constructor Create(Image: TGraphic; Compression: TImageCompressionType; Doc: TPDFDocument; MaskIndex: Integer = -1; IsMask: Boolean = False; TransparentColor: TColor = -1); overload;
    constructor Create(FileName: TFileName; Compression: TImageCompressionType; Doc: TPDFDocument; MaskIndex: Integer = -1; IsMask: Boolean = False; TransparentColor: TColor = -1); overload;
    destructor Destroy; override;
    property Height: Integer read FHeight;
    property Width: Integer read FWidth;
    property BitPerPixel: Integer read FBitPerPixel;
  end;

  TPDFImages = class(TObject)
  private
    FOwner: TPDFDocument;
    FImages: TList;
    function GetCount: Integer;
    function GetImage(Index: Integer): TPDFImage;
  public
    constructor Create(AOwner: TPDFDocument);
    destructor Destroy; override;
    procedure Delete(Index: Integer);
    procedure Clear;
    function Add(Image: TGraphic; Compression: TImageCompressionType; MaskIndex: Integer = -1; IsMask: Boolean = False; TransparentColor: TColor = -1): TPDFImage; overload;
    function Add(FileName: TFileName; Compression: TImageCompressionType; MaskIndex: Integer = -1; IsMask: Boolean = False; TransparentColor: TColor = -1): TPDFImage; overload;
    property Count: Integer read GetCount;
    property Items[Index: Integer]: TPDFImage read GetImage; default;
  end;

  TPDFDocument = class(TComponent)
  private
    IDOffset: array of Integer;
    CurID: Integer;
    EncriptID: Integer;
    PagesID: Integer;
    CatalogID: Integer;
    XREFOffset: Integer;
    FAcroForm: TPDFAcroForm;
    FJSF: TPDFJSFList;
    FFileName: TFileName;
    FPages: TPDFPages;
    FWaterMarks: TPDFPages;
    FActions: TPDFActions;
    FFonts: TPDFFonts;
    FImages: TPDFImages;
    FRadioGroups: TList;
    FStream: TStream;
    FCompression: TCompressionType;
    FDocumentInfo: TPDFDocInfo;
    FPageLayout: TPageLayout;
    FPageMode: TPageMode;
    FCurrentPage: TPDFPage;
    FOutlines: TPDFOutlines;
    FProtectionEnabled: Boolean;
    FUserPassword: string;
    FOwnerPassword: string;
    FOCP: string;
    FProtectionOptions: TPDFCtiptoOptions;
    FKey: TEncripKey;
    FPFlags: Integer;
    FileID: string;
    FNotEmbeddedFont: TStringList;
    FOutputStream: TStream;
    FJPEGQuality: Integer;
    FPrinting: Boolean;
    FAborted: Boolean;
    FAutoLaunch: Boolean;
    FResolution: Integer;
    FViewerPreferences: TViewerPreferences;
    FOpenDocumentAction: TPDFAction;
    FACURL: Boolean;
    FOnePass: Boolean;
    FVersion: TPDFVersion;
{$IFDEF PASStream}
    FPAS: TStream;
{$ENDIF}
    FEMFImageLostQuality: Boolean;
    FEMFSimpleText: Boolean;
    FPKL: TPDFKeyLength;
    FEmbeddingStyle: TPDFFontEmbeddingType;
    procedure CalcKey;
    procedure SaveXREF;
    procedure SaveTrailer;
    procedure SaveDocumentInfo;
    procedure SetFileName(const Value: TFileName);
    procedure SetCompression(const Value: TCompressionType);
    procedure SetDocumentInfo(const Value: TPDFDocInfo);
    procedure SaveToStream(st: string; CR: Boolean = True);
    function GetNextID: Integer;
    procedure SetPageLayout(const Value: TPageLayout);
    procedure SetPageMode(const Value: TPageMode);
    procedure SetOwnerPassword(const Value: string);
    procedure SetProtectionEnabled(const Value: Boolean);
    procedure SetProtectionOptions(const Value: TPDFCtiptoOptions);
    procedure SetUserPassword(const Value: string);
    procedure GenFileID;
    procedure CalcOwnerPassword;
    function CalcUserPassword: string;
    procedure SetNotEmbeddedFont(const Value: TStringList);
    function GetPageCount: Integer;
    function GetPage(Index: Integer): TPDFPage;
    procedure SetOutputStream(const Value: TStream);
    procedure SetJPEGQuality(const Value: Integer);
    procedure SetAutoLaunch(const Value: Boolean);
{$IFDEF PASStream}
    procedure SavePasStream(PasString: string);
{$ENDIF}
    function GetCanvas: TCanvas;
    function GetPageHeight: Integer;
    function GetPageNumber: Integer;
    function GetPageWidth: Integer;
    function GetWaterMark(Index: Integer): TPDFPage;
    procedure AppendAction(const Action: TPDFAction);
    procedure SetOpenDocumentAction(const Value: TPDFAction);
    procedure StartObj(var ID: Integer);
    procedure CloseHObj;
    procedure CloseObj;
    procedure StartStream;
    procedure CloseStream;
    procedure SetOnePass(const Value: Boolean);
    procedure ClearAll;
    procedure DeleteAllRadioGroups;
    function CreateRadioGroup(Name: string): TPDFRadioGroup;
    procedure SetVersion(const Value: TPDFVersion);
    procedure StoreDocument;
    procedure SetProtectionKeyLength(const Value: TPDFKeyLength);
    procedure SetEmbeddingStyle(const Value: TPDFFontEmbeddingType);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Abort;
    procedure BeginDoc;
    procedure EndDoc;
    procedure NewPage;
    procedure SetCurrentPage(Index: Integer);
    function GetCurrentPageIndex: Integer;
    function AddImage(Image: TGraphic; Compression: TImageCompressionType; MaskIndex: Integer = -1; IsMask: Boolean = False; TransparentColor: TColor = -1): Integer; overload;
    function AddImage(FileName: TFileName; Compression: TImageCompressionType; MaskIndex: Integer = -1; IsMask: Boolean = False; TransparentColor: TColor = -1): Integer; overload;
    function CreateWaterMark: Integer;
    function CreateAction(CClass: TPDFActionClass): TPDFAction;
    procedure AddJSFunction(AName, AParams, ABody: string);
    property OpenDocumentAction: TPDFAction read FOpenDocumentAction write SetOpenDocumentAction;
    property Outlines: TPDFOutlines read FOutlines;
    property CurrentPage: TPDFPage read FCurrentPage;
    property PageCount: Integer read GetPageCount;
    property Page[Index: Integer]: TPDFPage read GetPage; default;
    property WaterMark[Index: Integer]: TPDFPage read GetWaterMark;
    property Printing: Boolean read FPrinting;
    property Aborted: Boolean read FAborted;
    property Canvas: TCanvas read GetCanvas;
    property OutputStream: TStream read FOutputStream write SetOutputStream;
    property PageHeight: Integer read GetPageHeight;
    property PageWidth: Integer read GetPageWidth;
    property PageNumber: Integer read GetPageNumber;
{$IFDEF PASStream}
    property PASStream: TStream read FPAS write FPAS;
{$ENDIF}
  published
    property AutoLaunch: Boolean read FAutoLaunch write SetAutoLaunch;
    property FileName: TFileName read FFileName write SetFileName;
    property Compression: TCompressionType read FCompression write SetCompression;
    property DocumentInfo: TPDFDocInfo read FDocumentInfo write SetDocumentInfo;
    property PageLayout: TPageLayout read FPageLayout write SetPageLayout;
    property PageMode: TPageMode read FPageMode write SetPageMode;
    property ProtectionEnabled: Boolean read FProtectionEnabled write SetProtectionEnabled;
    property ProtectionOptions: TPDFCtiptoOptions read FProtectionOptions write SetProtectionOptions;
    property OwnerPassword: string read FOwnerPassword write SetOwnerPassword;
    property UserPassword: string read FUserPassword write SetUserPassword;
    property NonEmbeddedFont: TStringList read FNotEmbeddedFont write SetNotEmbeddedFont;
    property JPEGQuality: Integer read FJPEGQuality write SetJPEGQuality;
    property Resolution: Integer read FResolution write FResolution;
    property ViewerPreferences: TViewerPreferences read FViewerPreferences write FViewerPreferences;
    property AutoCreateURL: Boolean read FACURL write FACURL;
    property OnePass: Boolean read FOnePass write SetOnePass;
    property Version: TPDFVersion read FVersion write SetVersion;
    property EMFImageAsJpeg: Boolean read FEMFImageLostQuality write FEMFImageLostQuality;
    property EMFSimpleText: Boolean read FEMFSimpleText write FEMFSimpleText;
    property ProtectionKeyLength: TPDFKeyLength read FPKL write SetProtectionKeyLength;
    property EmbeddingStyle: TPDFFontEmbeddingType read FEmbeddingStyle write SetEmbeddingStyle;
  end;


implementation

uses pdfRC4, pdfResources, pdfGliphNames, pdfCCITT, pdfWMF, pdfZLib,
  pdfMD5, pdfTTF;

const
  PassKey: array[1..32] of Byte = ($28, $BF, $4E, $5E, $4E, $75, $8A, $41, $64, $00, $4E,
    $56, $FF, $FA, $01, $08, $2E, $2E, $00, $B6, $D0, $68,
    $3E, $80, $2F, $0C, $A9, $FE, $64, $53, $69, $7A);


{$IFDEF LLPDFEVAL}
var
  s1, s2, s3: string;
  KeyData: TRC4Data;
const
{$I SOP.inc}
  ComponentName: string = 'TPDFDocument';
{$ENDIF}


function EnCodeString(Encoding: Boolean; Key: TEncripKey; KeyLength: TPDFKeyLength; ID: Integer; Source: string): string;
var
  FullKey: array[1..21] of Byte;
  Digest: TMD5Byte16;
  AKey: TRC4Data;
  S: string;
begin
  if (Source = '') or (not Encoding) then
  begin
    Result := Source;
    Exit;
  end;
  S := Source;
  FillChar(FullKey, 21, 0);
  if KeyLength = kl40 then
  begin
    Move(Key, FullKey, 5);
    Move(ID, FullKey[6], 3);
    xCalcMD5(@FullKey, 10, Digest);
    RC4Init(AKey, @Digest, 10);
  end
  else
  begin
    Move(Key, FullKey, 16);
    Move(ID, FullKey[17], 3);
    xCalcMD5(@FullKey, 21, Digest);
    RC4Init(AKey, @Digest, 16);
  end;
  RC4Crypt(AKey, @s[1], @S[1], Length(S));
  Result := S;
end;

function EnCodeHexString(Encoding: Boolean; Key: TEncripKey; KeyLength: TPDFKeyLength; ID: Integer; Source: string): string;
var
  FullKey: array[1..21] of Byte;
  Digest: TMD5Byte16;
  AKey: TRC4Data;
  a: array of Byte;
  S: string;
  i: Integer;
begin
  if (Source = '') or (not Encoding) then
  begin
    Result := Source;
    Exit;
  end;
  S := '';
  FillChar(FullKey, 21, 0);
  if KeyLength = kl40 then
  begin
    Move(Key, FullKey, 5);
    Move(ID, FullKey[6], 3);
    xCalcMD5(@FullKey, 10, Digest);
    RC4Init(AKey, @Digest, 10);
  end else
  begin
    Move(Key, FullKey, 16);
    Move(ID, FullKey[17], 3);
    xCalcMD5(@FullKey, 21, Digest);
    RC4Init(AKey, @Digest, 16);
  end;
  SetLength(a, Length(Source) div 2);
  for i := 1 to Length(Source) div 2 do
    a[i - 1] := StrToInt('$' + Source[i shl 1 - 1] + Source[i shl 1]);
  RC4Crypt(AKey, @a[0], @a[0], Length(Source) div 2);
  for i := 1 to Length(Source) div 2 do
    S := S + ByteToHex(a[i - 1]);
  Result := S;
end;


procedure EnCodeStream(Encoding: Boolean; Key: TEncripKey; KeyLength: TPDFKeyLength; ID: Integer; Source: TMemoryStream);
var
  FullKey: array[1..21] of Byte;
  Digest: TMD5Byte16;
  AKey: TRC4Data;
begin
  if (not Encoding) or (Source.Size = 0) then
    Exit;
  FillChar(FullKey, 21, 0);
  if KeyLength = kl40 then
  begin
    Move(Key, FullKey, 5);
    Move(ID, FullKey[6], 3);
    xCalcMD5(@FullKey, 10, Digest);
    RC4Init(AKey, @Digest, 10);
  end else
  begin
    Move(Key, FullKey, 16);
    Move(ID, FullKey[17], 3);
    xCalcMD5(@FullKey, 21, Digest);
    RC4Init(AKey, @Digest, 16);
  end;
  RC4Crypt(AKey, Source.Memory, Source.Memory, Source.Size);
end;

function UnicodeChar(Text: string; Charset: Integer): string;
var
  A: array of Word;
  i: Integer;
  W: PWideChar;
  CodePage: Integer;
  OS: Integer;
begin
  Result := '';
  case Charset of
    EASTEUROPE_CHARSET: CodePage := 1250;
    RUSSIAN_CHARSET: CodePage := 1251;
    GREEK_CHARSET: CodePage := 1253;
    TURKISH_CHARSET: CodePage := 1254;
    BALTIC_CHARSET: CodePage := 1257;
    SHIFTJIS_CHARSET: CodePage := 932;
    129: CodePage := 949;
    CHINESEBIG5_CHARSET: CodePage := 950;
    GB2312_CHARSET: CodePage := 936;
  else
    CodePage := 1252;
  end;
  OS := MultiByteToWideChar(CodePage, 0, PChar(Text), Length(Text), nil, 0);
  if OS = 0 then Exit;
  SetLength(A, OS);
  W := @a[0];
  if MultiByteToWideChar(CodePage, 0, PChar(Text), Length(Text), W, OS) <> 0 then
  begin
    Result := 'FEFF';
    for i := 0 to OS - 1 do
      Result := Result + WordToHex(A[i]);
  end;
end;

function ReplStr(Source: string; Ch: Char; Sub: string): string;
var
  I: Integer;
begin
  Result := '';
  for i := 1 to Length(Source) do
    if Source[I] <> Ch then Result := Result + Source[i] else Result := Result + Sub;
end;

function EscapeSpecialChar(TextStr: string): string;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(TextStr) do
    case TextStr[I] of
      '(': Result := Result + '\(';
      ')': Result := Result + '\)';
      '\': Result := Result + '\\';
      #13: Result := result + '\r';
      #10: Result := result + '\n';
    else Result := Result + chr(Ord(textstr[i]));
    end;
end;


function IntToStrWithZero(ID, Count: Integer): string;
var
  s, d: string;
  I: Integer;
begin
  s := IntToStr(ID);
  I := Count - Length(s);
  d := '';
  for I := 0 to I - 1 do
    d := d + '0';
  Result := d + s;
end;

procedure NormalizeRect(var Rect: TRect); overload;
begin
  if Rect.Left > Rect.Right then swp(Rect.Left, Rect.Right);
  if Rect.Top > Rect.Bottom then swp(Rect.Top, Rect.Bottom);
end;

procedure NormalizeRect(var x1, y1, x2, y2: integer); overload;
begin
  if x1 > x2 then swp(x2, x1);
  if y1 > y2 then swp(y2, y1);
end;

procedure NormalizeRect(var x1, y1, x2, y2: Extended); overload;
begin
  if x1 > x2 then swp(x2, x1);
  if y1 > y2 then swp(y2, y1);
end;


function DPoint(x, y: Extended): TDoublePoint;
begin
  Result.x := x;
  Result.y := y;
end;

procedure RotateCoordinate(X, Y, Angle: Extended; var XO, YO: Extended);
var
  rcos, rsin: Extended;
begin
  Angle := Angle * (PI / 180);
  rcos := cos(angle);
  rsin := sin(angle);
  XO := rcos * x - rsin * y;
  YO := rsin * x + rcos * y;
end;


function FontTest(FontName: string; var FontStyle: TFontStyles; var FontCharset: TFontCharset; var FullFontName: string): Boolean;
type
  TFontInfo = record
    FontName: TFontName;
    Style: TFontStyles;
    Charset: TFontCharset;
    FullFontName: string;
    Error: Boolean;
    DefautCharset: TFontCharset;
    DefItalic: Boolean;
    DefBold: Boolean;
    Step: Integer;
  end;

  function Back(const Enum: ENUMLOGFONTEX; const PFD: TNEWTEXTMETRICEXA; FT: DWORD; var FI: TFontInfo): Integer; stdcall;
  var
    Bold, Italic: Boolean;
    Er: Boolean;
  begin
    if FT <> TRUETYPE_FONTTYPE then
    begin
      Result := 1;
      Exit;
    end;
    Bold := Enum.elfLogFont.lfWeight >= 600;
    Italic := Enum.elfLogFont.lfItalic <> 0;
    if FI.Step = 0 then
    begin
      FI.DefautCharset := Enum.elfLogFont.lfCharSet;
      FI.DefItalic := Italic;
      FI.DefBold := Bold;
    end;
    Inc(FI.Step);
    Er := False;
    if (fsbold in FI.Style) <> Bold then Er := True;
    if (fsItalic in FI.Style) <> Italic then Er := True;
    if Enum.elfLogFont.lfCharSet <> FI.Charset then Er := True;
    FI.Error := Er;
    if Er then
      Result := 1 else
    begin
      FI.FullFontName := Enum.elfFullName;
      Result := 0;
    end;
  end;
var
  LogFont: TLogFont;
  DC: HDC;
  ST: TFontStyles;
  FI: TFontInfo;
begin
  FI.FontName := FontName;
  FI.Charset := FontCharset;
  FI.Style := FontStyle;
  FillChar(LogFont, SizeOf(LogFont), 0);
  LogFont.lfCharSet := DEFAULT_CHARSET;
  move(FI.FontName[1], LogFont.lfFaceName, Length(FI.FontName));
  FI.DefautCharset := 0;
  FI.Step := 0;
  FI.Error := True;
  ST := FI.Style;
  DC := GetDC(0);
  try
    EnumFontFamiliesEx(DC, LogFont, @Back, Integer(@FI), 0);
    if FI.Step <> 0 then
      if FI.Error then
      begin
        if fsItalic in FI.Style then
        begin
          FI.Style := FI.Style - [fsItalic];
          EnumFontFamiliesEx(DC, LogFont, @Back, Integer(@FI), 0);
        end;
        if FI.Error then
          if fsBold in FI.Style then
          begin
            FI.Style := FI.Style - [fsBold];
            EnumFontFamiliesEx(DC, LogFont, @Back, Integer(@FI), 0);
          end;
        if FI.Error then
        begin
          FI.Style := [];
          if FI.DefItalic then FI.Style := FI.Style + [fsItalic];
          if FI.DefBold then FI.Style := FI.Style + [fsBold];
          EnumFontFamiliesEx(DC, LogFont, @Back, Integer(@FI), 0);
        end;
        if FI.Error then
        begin
          FI.Style := ST;
          FI.Charset := FI.DefautCharset;
          EnumFontFamiliesEx(DC, LogFont, @Back, Integer(@FI), 0);
        end;
        if FI.Error then
          if fsItalic in FI.Style then
          begin
            FI.Style := FI.Style - [fsItalic];
            EnumFontFamiliesEx(DC, LogFont, @Back, Integer(@FI), 0);
          end;
        if FI.Error then
          if fsBold in FI.Style then
          begin
            FI.Style := FI.Style - [fsBold];
            EnumFontFamiliesEx(DC, LogFont, @Back, Integer(@FI), 0);
          end;
        if FI.Error then
        begin
          FI.Style := [];
          if FI.DefItalic then FI.Style := FI.Style + [fsItalic];
          if FI.DefBold then FI.Style := FI.Style + [fsBold];
          EnumFontFamiliesEx(DC, LogFont, @Back, Integer(@FI), 0);
        end;
      end;
  finally
    ReleaseDC(0, DC);
  end;
  Result := not FI.Error;
  if not FI.Error then
  begin
    FontName := FI.FontName;
    FullFontName := FI.FullFontName;
    FontStyle := FI.Style;
    FontCharset := FI.Charset;
  end;
end;


{ TPDFDocument }

procedure TPDFDocument.Abort;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  ClearAll;
  if FOutputStream = nil then DeleteFile(FileName);
  FAborted := True;
  FPrinting := False;
end;

procedure TPDFDocument.BeginDoc;
begin
  if FPrinting then
    raise TPDFException.Create(SGenerationPDFFileInProgress);
  ClearAll;
  GenFileID;
  if FPKL = kl40 then
  begin
    FPFlags := $7FFFFFE0;
    FPFlags := FPFlags shl 1;
    if coPrint in ProtectionOptions then FPFlags := FPFlags or 4;
    if coModifyStructure in ProtectionOptions then FPFlags := FPFlags or 8;
    if coCopyInformation in ProtectionOptions then FPFlags := FPFlags or 16;
    if coModifyAnnotation in ProtectionOptions then FPFlags := FPFlags or 32;
  end
  else
  begin
    FPFlags := $7FFFF860;
    FPFlags := FPFlags shl 1;
    if coPrint in ProtectionOptions then FPFlags := FPFlags or $804;
    if coModifyStructure in ProtectionOptions then FPFlags := FPFlags or $408;
    if coCopyInformation in ProtectionOptions then FPFlags := FPFlags or $210;
    if coModifyAnnotation in ProtectionOptions then FPFlags := FPFlags or $120;
  end;
  if ProtectionEnabled then CalcKey;
  if FOutputStream = nil then
    FStream := TFileStream.Create(FileName, fmCreate) else FStream := TIsMemoryStream.Create;
  if Version = v13 then SaveToStream('%PDF-1.3')
  else SaveToStream('%PDF-1.4');
  SaveToStream('%����');
  FPrinting := True;
  FAborted := False;
  PagesID := GetNextID;
  FCurrentPage := FPages.Add;
  FCurrentPage.CreateCanvas;
end;

procedure TPDFDocument.CalcOwnerPassword;

var
  Key: TRC4Data;
  Pass: array[1..32] of byte;
  I, J: Byte;
  Digest, DG1: TMD5Byte16;
begin
  if FOwnerPassword <> '' then Move(FOwnerPassword[1], Pass, 32);
  for I := 1 to 32 - Length(FOwnerPassword) do
    Pass[I + Length(FOwnerPassword)] := passkey[I];
  xCalcMD5(@Pass[1], 32, Digest);
  if FPKL = kl128 then
  begin
    for I := 1 to 50 do xCalcMD5(@Digest, 16, Digest);
    RC4Init(Key, @Digest, 16);
  end else RC4Init(Key, @Digest, 5);
  if FUserPassword <> '' then Move(FUserPassword[1], Pass, 32);
  for I := 1 to 32 - Length(FUserPassword) do
    Pass[I + Length(FUserPassword)] := passkey[I];
  SetLength(FOCP, 32);
  RC4Crypt(Key, @Pass, @FOCP[1], 32);
  if FPKL = kl128 then
    for i := 1 to 19 do
    begin
      for J := 0 to 15 do DG1[j] := Digest[j] xor I;
      RC4Init(Key, @DG1, 16);
      RC4Crypt(Key, @FOCP[1], @FOCP[1], 32);
    end;
end;

function TPDFDocument.CalcUserPassword: string;
var
  I, J: Byte;
  Op: array[1..32] of Byte;
  AKey: TRC4Data;
  Context: TMD5Ctx;
  Digest: TMD5Byte16;
  K2: TEncripKey;
  C: string;
begin
  if FPKL = kl40 then
  begin
    RC4Init(AKey, @FKey, 5);
    RC4Crypt(AKey, @PassKey, @op, 32);
    Result := '';
    for I := 1 to 32 do Result := Result + chr(op[I]);
  end
  else
  begin
    MD5Init(Context);
    MD5Update(Context, PassKey, 32);
    C := '';
    for i := 1 to 16 do
      C := C + chr(StrToInt('$' + FileID[i shl 1 - 1] + FileID[i shl 1]));
    MD5Update(Context, C[1], 16);
    MD5Final(Digest, Context);
    for I := 0 to 19 do
    begin
      for J := 1 to 16 do K2[j] := FKey[j] xor i;
      RC4Init(AKey, @k2, 16);
      RC4Crypt(AKey, @DIgest, @Digest, 16);
    end;
    SetLength(Result, 32);
    Move(Digest, Result[1], 16);
    Randomize;
    for I := 17 to 32 do Result[i] := char(Random(200) + 32);
  end;
end;

constructor TPDFDocument.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FRadioGroups := TList.Create;
  FPages := TPDFPages.Create(Self, False);
  FActions := TPDFActions.Create(Self);
  FWaterMarks := TPDFPages.Create(Self, True);
  FFonts := TPDFFonts.Create(Self);
  FNotEmbeddedFont := TStringList.Create;
  FImages := TPDFImages.Create(Self);
  FOutlines := TPDFOutlines.Create(Self);
  FDocumentInfo := TPDFDocInfo.Create;
  FAcroForm := TPDFAcroForm.Create(Self);
  FDocumentInfo.Creator := 'llPDFLib program';
  FDocumentInfo.CreationDate := Now;
  FDocumentInfo.Producer := 'llPDFLib 2.x';
  FDocumentInfo.Author := 'Windows 9x/NT/2000/XP User';
  FDocumentInfo.Title := 'No Title';
  FDocumentInfo.Subject := 'None';
  FDocumentInfo.Keywords := 'llPDFLib';
  FJSF := TPDFJSFList.Create;
  FCurrentPage := nil;
  FOutputStream := nil;
  FJPEGQuality := 80;
  FAborted := False;
  FPrinting := False;
  FResolution := 72;
  FOpenDocumentAction := nil;
  FOnePass := False;
  EMFImageAsJpeg := False;
  FEmbeddingStyle := fetUsedGlyphsOnly;
end;

destructor TPDFDocument.Destroy;
begin
  FJSF.Free;
  FRadioGroups.Free;
  FDocumentInfo.Free;
  FOutlines.Free;
  FImages.Free;
  FFonts.Free;
  FPages.Free;
  FActions.Free;
  FWaterMarks.Free;
  FNotEmbeddedFont.Free;
  FAcroForm.Free;
  inherited;
end;


procedure TPDFDocument.EndDoc;
begin
  try
    StoreDocument;
  except
    on Exception do
    begin
      AbortEx;
      raise;
    end;
  end;
  if FOutputStream <> nil then
  begin
    FStream.Position := 0;
    FOutputStream.CopyFrom(FStream, FStream.Size);
  end;
  ClearAll;
  FPrinting := False;
  FAborted := False;
  if (FOutputStream = nil) and (AutoLaunch) then
  try
    ShellExecute(GetActiveWindow, 'open', PChar(FFileName), nil, nil, SW_NORMAL);
  except
  end;
end;

procedure TPDFDocument.NewPage;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  FCurrentPage.CloseCanvas;
  if OnePass then FCurrentPage.Save;
  FCurrentPage := FPages.Add;
  FCurrentPage.CreateCanvas;
end;

procedure TPDFDocument.SetFileName(const Value: TFileName);
begin
  if FPrinting then
    raise TPDFException.Create(SGenerationPDFFileInProgress);
  FFileName := Value;
end;

procedure TPDFDocument.SetCompression(const Value: TCompressionType);
begin
  FCompression := Value;
end;

function TPDFDocument.GetNextID: Integer;
begin
  Inc(CurID);
  SetLength(IDOffset, CurID);
  Result := CurID;
end;

procedure TPDFDocument.SaveTrailer;
begin
  SaveToStream('trailer');
  SaveToStream('<<');
  SaveToStream('/Size ' + IntToStr(CurID + 1));
  SaveToStream('/Root ' + IntToStr(CatalogID) + ' 0 R');
  SaveToStream('/Info ' + IntToStr(FDocumentInfo.ID) + ' 0 R');
  if FProtectionEnabled then
    SaveToStream('/Encrypt ' + IntToStr(EncriptID) + ' 0 R');
  SaveToStream('/ID [<' + FileID + '><' + FileID + '>]');
  SaveToStream('>>');
end;

procedure TPDFDocument.SaveXREF;
var
  I: Integer;
begin
  XREFOffset := FStream.Position;
  SaveToStream('xref');
  SaveToStream('0 ' + IntToStr(CurID + 1));
  SaveToStream(IntToStrWithZero(0, 10) + ' ' + IntToStr($FFFF) + ' f');
  for I := 0 to CurID - 1 do
    SaveToStream(IntToStrWithZero(idoffset[I], 10) + ' ' + IntToStrWithZero(0, 5) + ' n');
end;

procedure TPDFDocument.SaveDocumentInfo;
begin
  StartObj(FDocumentInfo.ID);
  SaveToStream('/Creator (' + EscapeSpecialChar(EnCodeString(FProtectionEnabled, FKey, FPKL, FDocumentInfo.ID, FDocumentInfo.Creator)) + ')');
  SaveToStream('/CreationDate (' + EscapeSpecialChar(EnCodeString(FProtectionEnabled, FKey, FPKL, FDocumentInfo.ID, 'D:' + FormatDateTime('yyyymmddhhnnss', FDocumentInfo.CreationDate))) + ')');
  SaveToStream('/Producer (' + EscapeSpecialChar(EnCodeString(FProtectionEnabled, FKey, FPKL, FDocumentInfo.ID, FDocumentInfo.Producer)) + ')');
  SaveToStream('/Author (' + EscapeSpecialChar(EnCodeString(FProtectionEnabled, FKey, FPKL, FDocumentInfo.ID, FDocumentInfo.Author)) + ')');
  SaveToStream('/Title (' + EscapeSpecialChar(EnCodeString(FProtectionEnabled, FKey, FPKL, FDocumentInfo.ID, FDocumentInfo.Title)) + ')');
  SaveToStream('/Subject (' + EscapeSpecialChar(EnCodeString(FProtectionEnabled, FKey, FPKL, FDocumentInfo.ID, FDocumentInfo.Subject)) + ')');
  SaveToStream('/Keywords (' + EscapeSpecialChar(EnCodeString(FProtectionEnabled, FKey, FPKL, FDocumentInfo.ID, FDocumentInfo.Keywords)) + ')');
  CloseHObj;
  CloseObj;
end;

procedure TPDFDocument.SetDocumentInfo(const Value: TPDFDocInfo);
begin
  FDocumentInfo.Creator := Value.Creator;
  FDocumentInfo.CreationDate := Value.CreationDate;
  FDocumentInfo.Author := Value.Author;
  FDocumentInfo.Title := Value.Title;
  FDocumentInfo.Subject := Value.Subject;
  FDocumentInfo.Keywords := Value.Keywords;
end;

procedure TPDFDocument.SaveToStream(st: string; CR: Boolean);
var
  WS: string;
  Ad: Pointer;
begin
  WS := st;
  if CR then WS := WS + #13#10;
  Ad := @WS[1];
  FStream.Write(ad^, Length(WS));
end;

procedure TPDFDocument.SetPageLayout(const Value: TPageLayout);
begin
  FPageLayout := Value;
end;

procedure TPDFDocument.SetPageMode(const Value: TPageMode);
begin
  FPageMode := Value;
end;

procedure TPDFDocument.SetCurrentPage(Index: Integer);
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  if OnePass then
    raise TPDFException.Create(SCannotChangePageInOnePassMode);
  FCurrentPage.CloseCanvas;
  FCurrentPage := FPages[Index];
  FCurrentPage.CreateCanvas;
end;

procedure TPDFDocument.SetOwnerPassword(const Value: string);
begin
  if FPrinting then
    raise TPDFException.Create(SGenerationPDFFileInProgress);
  FOwnerPassword := Value;
end;

procedure TPDFDocument.SetProtectionEnabled(const Value: Boolean);
begin
  if FPrinting then
    raise TPDFException.Create(SGenerationPDFFileInProgress);
  FProtectionEnabled := Value;
end;

procedure TPDFDocument.SetProtectionOptions(
  const Value: TPDFCtiptoOptions);
begin
  if FPrinting then
    raise TPDFException.Create(SGenerationPDFFileInProgress);
  FProtectionOptions := Value;
end;

procedure TPDFDocument.SetUserPassword(const Value: string);
begin
  if FPrinting then
    raise TPDFException.Create(SGenerationPDFFileInProgress);
  FUserPassword := Value;
end;

procedure TPDFDocument.CalcKey;
var
  Digest: TMD5Byte16;
  W, Z, C: string;
  Cont: TMD5Ctx;
  i: Integer;
begin

  CalcOwnerPassword;
  z := FOCP;
  W := Copy(FUserPassword, 1, 32);
  SetLength(W, 32);
  if Length(FUserPassword) < 32 then
    Move(PassKey, W[Length(FUserPassword) + 1], 32 - Length(FUserPassword));
  C := '';
  for i := 1 to 16 do
    C := C + chr(StrToInt('$' + FileID[i shl 1 - 1] + FileID[i shl 1]));
  MD5Init(Cont);
  MD5Update(Cont, w[1], 32);
  MD5Update(Cont, z[1], 32);
  MD5Update(Cont, FPFlags, 4);
  MD5Update(Cont, C[1], 16);
  MD5Final(Digest, Cont);
  if FPKL = kl128 then
  begin
    for i := 1 to 50 do xCalcMD5(@Digest, 16, Digest);
    Move(Digest, FKey, 16);
  end else Move(Digest, FKey, 5);
end;

procedure TPDFDocument.GenFileID;
var
  s: string;
begin
  s := FileName + FormatDateTime('ddd dd-mm-yyyy hh:nn:ss.zzz', Now);
  FileID := LowerCase(md5result(s));
end;

procedure TPDFDocument.SetNotEmbeddedFont(const Value: TStringList);
begin
  FNotEmbeddedFont.Assign(Value);
end;

function TPDFDocument.GetCurrentPageIndex: Integer;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  Result := FPages.FPages.IndexOf(Pointer(FCurrentPage));
end;

function TPDFDocument.AddImage(Image: TGraphic; Compression: TImageCompressionType; MaskIndex: Integer = -1;
  IsMask: Boolean = False; TransparentColor: TColor = -1): Integer;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  Result := FImages.FImages.IndexOf(Pointer(FImages.Add(Image, Compression, MaskIndex, IsMask, TransparentColor)));
end;

function TPDFDocument.AddImage(FileName: TFileName; Compression: TImageCompressionType;
  MaskIndex: Integer = -1; IsMask: Boolean = False; TransparentColor: TColor = -1): Integer;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  Result := FImages.FImages.IndexOf(Pointer(FImages.Add(FileName, Compression, MaskIndex, IsMask, TransparentColor)));
end;

function TPDFDocument.GetPageCount: Integer;
begin
  Result := FPages.Count;
end;

function TPDFDocument.GetPage(Index: Integer): TPDFPage;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  if OnePass then
    raise TPDFException.Create(SCannotAccessToPageInOnePassMode);
  if (Index < 0) or (Index > FPages.Count - 1) then
    raise TPDFException.Create(SOutOfRange);
  Result := FPages[Index];
end;

procedure TPDFDocument.SetOutputStream(const Value: TStream);
begin
  if FPrinting then
    raise TPDFException.Create(SGenerationPDFFileInProgress);
  FOutputStream := Value;
end;

procedure TPDFDocument.SetJPEGQuality(const Value: Integer);
begin
  FJPEGQuality := Value;
end;

procedure TPDFDocument.SetAutoLaunch(const Value: Boolean);
begin
  FAutoLaunch := Value;
end;


function TPDFDocument.GetCanvas: TCanvas;
begin
  if CurrentPage = nil then Result := nil else
  begin
    Result := CurrentPage.FCanvas;
    CurrentPage.FAskCanvas := True;
  end;
end;

function TPDFDocument.GetPageHeight: Integer;
var
  DC: HDC;
  I: Integer;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  DC := GetDC(0);
  I := GetDeviceCaps(dc, LOGPIXELSX);
  ReleaseDC(0, DC);
  Result := MulDiv(CurrentPage.FHeight, I, 72);
end;

function TPDFDocument.GetPageNumber: Integer;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  Result := GetCurrentPageIndex + 1;
end;

function TPDFDocument.GetPageWidth: Integer;
var
  DC: HDC;
  I: Integer;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  DC := GetDC(0);
  I := GetDeviceCaps(dc, LOGPIXELSX);
  ReleaseDC(0, DC);
  Result := MulDiv(CurrentPage.FWidth, I, 72);
end;

function TPDFDocument.CreateWaterMark: Integer;
var
  P: TPDFPage;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  P := FWaterMarks.Add;
  Result := FWaterMarks.IndexOf(P);
end;

function TPDFDocument.GetWaterMark(Index: Integer): TPDFPage;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  Result := FWaterMarks.GetPage(Index);
end;

procedure TPDFDocument.AppendAction(const Action: TPDFAction);
var
  i: Integer;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  if FActions.IndexOf(Action) < 0 then
  begin
    i := FActions.Add(Action);
    if FActions[i].ActionID < 1 then
      FActions[i].ActionID := GetNextID;
  end;
end;

procedure TPDFDocument.SetOpenDocumentAction(const Value: TPDFAction);
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  if FOpenDocumentAction = nil then FOpenDocumentAction := Value
  else
  begin
    Value.FNext := FOpenDocumentAction;
    FOpenDocumentAction := Value;
  end;
  AppendAction(Value);
end;

procedure TPDFDocument.CloseHObj;
begin
  SaveToStream('>>');
end;

procedure TPDFDocument.CloseObj;
begin
  SaveToStream('endobj');
end;

procedure TPDFDocument.StartObj(var ID: Integer);
var
  Offset: Integer;
begin
  if ID <= 0 then ID := GetNextID;
  Offset := FStream.Position;
  if ID > CurID then raise TPDFException.Create(SOutOfRange);
  IDOffset[ID - 1] := Offset;
  SaveToStream(IntToStr(ID) + ' 0 obj');
  SaveToStream('<<', False);
end;

procedure TPDFDocument.CloseStream;
begin
  SaveToStream('endstream');
end;

procedure TPDFDocument.StartStream;
begin
  SaveToStream('stream');
end;

procedure TPDFDocument.SetOnePass(const Value: Boolean);
begin
  if FPrinting then
    raise TPDFException.Create(SGenerationPDFFileInProgress);
  FOnePass := Value;
end;

procedure TPDFDocument.ClearAll;
begin
  CurID := 0;
  FDocumentInfo.ID := 0;
  PagesID := 0;
  CatalogID := 0;
  EncriptID := 0;
  IDOffset := nil;
  DeleteAllRadioGroups;
  FImages.Clear;
  FFonts.Clear;
  FPages.Clear;
  FWaterMarks.Clear;
  FActions.Clear;
  FOutlines.Clear;
  FCurrentPage := nil;
  FAcroForm.Clear;
  FJSF.Clear;
  FOpenDocumentAction := nil;
  if FStream <> nil then
  begin
    FStream.Free;
    FStream := nil;
  end;
end;

procedure TPDFDocument.DeleteAllRadioGroups;
var
  I: Integer;
begin
  for I := 0 to FRadioGroups.Count - 1 do
    TPDFRadioGroup(FRadioGroups[I]).Free;
  FRadioGroups.Clear;
end;

function TPDFDocument.CreateAction(CClass: TPDFActionClass): TPDFAction;
var
  SO: TPDFAction;
begin
  SO := CClass.Create;
  AppendAction(SO);
  Result := SO;
end;

function TPDFDocument.CreateRadioGroup(Name: string): TPDFRadioGroup;
var
  RG: TPDFRadioGroup;
begin
  RG := TPDFRadioGroup.Create(Self, Name);
  RG.GroupID := GetNextID;
  FRadioGroups.Add(RG);
  Result := RG;
end;

procedure TPDFDocument.SetVersion(const Value: TPDFVersion);
begin
  FVersion := Value;
  if Value = v13 then FPKL := kl40;
end;

procedure TPDFDocument.StoreDocument;
var
  i, K: Integer;
  NID: Integer;
begin
  if not FPrinting then
    raise TPDFException.Create(SGenerationPDFFileNotActivated);
  FCurrentPage.CloseCanvas;
  if OnePass then FCurrentPage.Save;
{$IFDEF LLPDFEVAL}
{$I eval.inc}
{$ENDIF}
  SaveDocumentInfo;
  i := 0;
  while i < FActions.Count do
  begin
    FActions[i].Prepare;
    Inc(i);
  end;
  for i := 0 to FWaterMarks.Count - 1 do
    FWaterMarks[i].CloseCanvas;
  for i := 0 to FWaterMarks.Count - 1 do
    FWaterMarks[i].Save;

  StartObj(PagesID);
  SaveToStream('/Type /Pages');
  SaveToStream('/Kids [');
  for i := 0 to FPages.Count - 1 do
    SaveToStream(IntToStr(FPages[i].PageID) + ' 0 R');
  SaveToStream(']');
  SaveToStream('/Count ' + IntToStr(FPages.Count));
  CloseHObj;
  CloseObj;
  if not OnePass then
    for i := 0 to FPages.Count - 1 do
      FPages[i].Save;
  for i := 0 to FActions.Count - 1 do
    FActions[i].Save;
  if Outlines.Count <> 0 then
  begin
    FOutlines.OutlinesID := GetNextID;
    for i := 0 to FOutlines.Count - 1 do
      FOutlines[i].OutlineNodeID := GetNextID;
    for i := 0 to FOutlines.Count - 1 do
      FOutlines[i].Save;
    StartObj(FOutlines.OutlinesID);
    SaveToStream('/Type /Outlines');
    SaveToStream('/Count ' + IntToStr(FOutlines.Count));
    for i := 0 to FOutlines.Count - 1 do
    begin
      if (FOutlines[i].FParent = nil) and (FOutlines[i].FPrev = nil) then
        SaveToStream('/First ' + IntToStr(FOutlines[i].OutlineNodeID) + ' 0 R');
      if (FOutlines[i].FParent = nil) and (FOutlines[i].FNext = nil) then
        SaveToStream('/Last ' + IntToStr(FOutlines[i].OutlineNodeID) + ' 0 R');
    end;
    CloseHObj;
    CloseObj;
  end;
  for i := 0 to FRadioGroups.Count - 1 do
    TPDFRadioGroup(FRadioGroups[i]).Save;
  if not FAcroForm.Empty then FAcroForm.Save;
  StartObj(CatalogID);
  SaveToStream('/Type /Catalog');
  SaveToStream(' /Pages ' + IntToStr(PagesID) + ' 0 R');
  case PageLayout of
    plSinglePage: SaveToStream('/PageLayout /SinglePage');
    plOneColumn: SaveToStream('/PageLayout /OneColumn');
    plTwoColumnLeft: SaveToStream('/Pagelayout /TwoColumnLeft');
    plTwoColumnRight: SaveToStream('/PageLayout /TwoColumnRight');
  end;
  if ViewerPreferences <> [] then
  begin
    SaveToStream('/ViewerPreferences <<');
    if vpHideToolBar in ViewerPreferences then SaveToStream('/HideToolbar true');
    if vpHideMenuBar in ViewerPreferences then SaveToStream('/HideMenubar true');
    if vpHideWindowUI in ViewerPreferences then SaveToStream('/HideWindowUI true');
    if vpFitWindow in ViewerPreferences then SaveToStream('/FitWindow true');
    if vpCenterWindow in ViewerPreferences then SaveToStream('/CenterWindow true');
    SaveToStream('>>');
  end;
  case PageMode of
    pmUseNone: SaveToStream('/PageMode /UseNone');
    pmUseOutlines: SaveToStream('/PageMode /UseOutlines');
    pmUseThumbs: SaveToStream('/PageMode /UseThumbs');
    pmFullScreen: SaveToStream('/PageMode /FullScreen');
  end;
  if FOpenDocumentAction <> nil then
    SaveToStream('/OpenAction ' + IntToStr(FOpenDocumentAction.ActionID) + ' 0 R');
  if FOutlines.Count <> 0 then
    SaveToStream('/Outlines ' + IntToStr(FOutlines.OutlinesID) + ' 0 R');
  if not FAcroForm.Empty then
    SaveToStream('/AcroForm ' + IntToStr(FAcroForm.AcroID) + ' 0 R');
  if FJSF.Count <> 0 then
  begin
    NID := GetNextID;
    SaveToStream('/Names ' + IntToStr(NID) + ' 0 R');
  end;
  CloseHObj;
  CloseObj;
  if FJSF.Count <> 0 then
  begin
    StartObj(NID);
    NID := GetNextID;
    SaveToStream('/JavaScript ' + IntToStr(NID) + ' 0 R');
    CloseHObj;
    CloseObj;
    for i := 0 to FJSF.Count - 1 do
      FJSF[i].ID := GetNextID;
    StartObj(NID);
    SaveToStream('/Names [', False);
    for i := 0 to FJSF.Count - 1 do
      SaveToStream(' (' + EscapeSpecialChar(EnCodeString(FProtectionEnabled, FKey, FPKL, NID, FJSF[i].Name)) + ') ' + IntToStr(FJSF[i].ID) + ' 0 R', False);
    SaveToStream(']');
    CloseHObj;
    CloseObj;
    for i := 0 to FJSF.Count - 1 do
    begin
      K := FJSF[i].ID;
      StartObj(K);
      SaveToStream('/S /JavaScript /JS (' + EscapeSpecialChar(EnCodeString(FProtectionEnabled, FKey, FPKL, FJSF[i].ID,
        'function ' + FJSF[i].Name + '(' + FJSF[i].Params + ')' + '{' + FJSF[i].Body + '}')) + ') ');
      CloseHObj;
      CloseObj;
    end;
  end;
  if FProtectionEnabled then
  begin
    StartObj(EncriptID);
    SaveToStream('/Filter /Standard');
    if FPKL = kl40 then
    begin
      SaveToStream('/V 1');
      SaveToStream('/R 2');
    end else
    begin
      SaveToStream('/V 2');
      SaveToStream('/R 3');
      SaveToStream('/Length 128');
    end;
    SaveToStream('/P ' + IntToStr(FPFlags));
    SaveToStream('/O (' + EscapeSpecialChar(FOCP) + ')');
    SaveToStream('/U (' + EscapeSpecialChar(CalcUserPassword) + ')');
    CloseHObj;
    CloseObj;
  end;
  FFonts.EmbiddingFontFiles;
  for i := 0 to FFonts.Count - 1 do
    FFonts[i].Save(Self);
  if not FOnePass then
    for i := 0 to FImages.Count - 1 do
      FImages[i].Save(Self);
  SaveXREF;
  SaveTrailer;
  SaveToStream('startxref');
  SaveToStream(IntToStr(XREFOffset));
  SaveToStream('%%EOF');
end;


{$IFDEF PASStream}

procedure TPDFDocument.SavePasStream(PasString: string);
var
  WS: string;
  Ad: Pointer;
begin
  if PASStream <> nil then
  begin
    WS := PasString;
    WS := WS + #13#10;
    Ad := @WS[1];
    PASStream.Write(ad^, Length(WS));
  end;
end;
{$ENDIF}

procedure TPDFDocument.SetProtectionKeyLength(const Value: TPDFKeyLength);
begin
  if FPKL <> Value then
  begin
    if Printing then raise TPDFException.Create('Cannot change this value');
    FPKL := Value;
    if Value = kl128 then FVersion := v14;
  end;
end;

procedure TPDFDocument.AddJSFunction(AName, AParams, ABody: string);
begin
  FJSF.Add(AName, AParams, ABody);
end;

procedure TPDFDocument.SetEmbeddingStyle(const Value: TPDFFontEmbeddingType);
begin
  FEmbeddingStyle := Value;
end;

{ TPDFPage }

procedure TPDFPage.AppendAction(Action: string);
begin
  FContent.Add(Action);
end;

function TPDFPage.Arc(X1, Y1, x2, y2, BegAngle,
  EndAngle: Extended): TDoublePoint;
var
  d: TDoublePoint;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('Arc(' + FormatFloat(x1) + ',' + FormatFloat(y1) + ',' + FormatFloat(x2) + ',' + FormatFloat(y2) + ',' +
    FormatFloat(BegAngle) + ',' + FormatFloat(EndAngle) + ');');
{$ENDIF}
  D := RawArc(ExtToIntX(X1), ExtToIntY(Y1), ExtToIntX(X2), ExtToIntY(Y2), -EndAngle, -BegAngle);
  Result := DPoint(IntToExtX(d.x), IntToExtY(d.y));
end;

function TPDFPage.Arc(X1, Y1, x2, y2, x3, y3, x4, y4: Extended): TDoublePoint;
var
  d: TDoublePoint;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('Arc(' + FormatFloat(x1) + ',' + FormatFloat(y1) + ',' + FormatFloat(x2) + ',' + FormatFloat(y2) + ',' +
    FormatFloat(x3) + ',' + FormatFloat(y3) + ',' + FormatFloat(x4) + ',' + FormatFloat(y4) + ');');
{$ENDIF}
  D := RawArc(ExtToIntX(X1), ExtToIntY(Y1), ExtToIntX(X2), ExtToIntY(Y2),
    ExtToIntX(X4), ExtToIntY(Y4), ExtToIntX(X3), ExtToIntY(y3));
  Result := DPoint(IntToExtX(d.x), IntToExtY(d.y));
end;

procedure TPDFPage.BeginText;
begin
  if FTextInited then
    raise TPDFException.Create(SCannotBeginTextObjectTwice);
{$IFDEF PASStream}
  FOwner.SavePasStream('BeginText;');
{$ENDIF}
  AppendAction('BT');
  FTextInited := True;
  ResetTextCTM;
  if FFontInited then
    if FEItalic then SkewText(0, 0);
end;

procedure TPDFPage.Circle(X, Y, R: Extended);
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('Circle(' + FormatFloat(x) + ',' + FormatFloat(y) + ',' + FormatFloat(R) + ');');
{$ENDIF}
  RawCircle(ExtToIntX(X), ExtToIntY(Y), ExtToIntX(R));
end;

procedure TPDFPage.Clip;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('Clip;');
{$ENDIF}
  AppendAction('W');
end;

procedure TPDFPage.ClosePath;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('ClosePath;');
{$ENDIF}
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  AppendAction('h');
end;

procedure TPDFPage.Comment(st: string);
begin
  AppendAction('% ' + st);
end;

procedure TPDFPage.ConcatTextMatrix(A, B, C, D, X, Y: Extended);
var
  sCTM: TTextCTM;
begin
  if not FTextInited then
    raise TPDFException.Create(STextObjectNotInited);
  sCTM.a := a;
  sCTM.b := b;
  sCTM.c := c;
  sCTM.d := d;
  sCTM.x := x;
  sCTM.y := y;
  MultiplyCTM(FCTM, sCTM);
  setTextMatrix(FCTM.a, FCTM.b, FCTM.c, FCTM.d, FCTM.x, FCTM.y);
end;

constructor TPDFPage.Create(AOwner: TPDFDocument);
begin
  FOwner := AOwner;
  PageID := FOwner.GetNextID;
  FAnnot := TList.Create;
  FCurrentDash := '[] 0';
  FContent := TStringList.Create;
  FLinkedFont := TList.Create;
  FLinkedImages := TList.Create;
  FForms := TPDFPages.Create(FOwner, True);
  FMF := nil;
  FRes := FOwner.FResolution;
  D2P := FRes / 72;
  Size := psA4;
  PageRotate := pr0;
  FCharSpace := 0;
  FWordSpace := 0;
  FRemoveCR := False;
  FEmulationEnabled := True;
  FHorizontalScaling := 100;
  FFontInited := False;
  FCurrentFontSize := 10;
  FTextLeading := 0;
  FRender := 0;
  FRise := 0;
  FTextInited := False;
  FSaveCount := 0;
  GStateSave;
  ResetTextCTM;
  Factions := False;
  FWaterMark := -1;
  FThumbnail := -1;
  FMatrix.a := 1;
  FMatrix.b := 0;
  FMatrix.c := 0;
  FMatrix.d := 1;
  FMatrix.x := 0;
  FMatrix.y := 0;
  FF := FMatrix;
  FTextUsed := False;
  FColorUsed := False;
  FGrayUsed := False;
  FCanvasOver := True;
end;

procedure TPDFPage.Curveto(X1, Y1, X2, Y2, X3, Y3: Extended);
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('CurveTo(' + FormatFloat(x1) + ',' + FormatFloat(y1) + ',' + FormatFloat(x2) + ',' + FormatFloat(y2) + ',' +
    FormatFloat(x3) + ',' + FormatFloat(y3) + ');');
{$ENDIF}
  RawCurveto(ExtToIntX(x1), ExtToIntY(y1), ExtToIntX(x2), ExtToIntY(y2), ExtToIntX(x3), ExtToIntY(y3));
end;

destructor TPDFPage.Destroy;
begin
  DeleteCanvas;
  DeleteAllAnnotations;
  FAnnot.Free;
  FLinkedImages.Free;
  FLinkedFont.Free;
  FContent.Free;
  FForms.Free;
  inherited;
end;

procedure TPDFPage.DrawArcWithBezier(CenterX, CenterY, RadiusX, RadiusY,
  StartAngle, SweepRange: Extended; UseMoveTo: Boolean);
var
  Coord, C2: array[0..3] of TDoublePoint;
  a, b, c, x, y: Extended;
  ss, cc: Double;
  i: Integer;
begin
  if SweepRange = 0 then
  begin
    if UseMoveTo then
      RawMoveTo(CenterX + RadiusX * cos(StartAngle),
        CenterY - RadiusY * sin(StartAngle));
    RawLineTo(CenterX + RadiusX * cos(StartAngle),
      CenterY - RadiusY * sin(StartAngle));
    Exit;
  end;
  b := sin(SweepRange / 2);
  c := cos(SweepRange / 2);
  a := 1 - c;
  x := a * 4 / 3;
  y := b - x * c / b;
  ss := sin(StartAngle + SweepRange / 2);
  cc := cos(StartAngle + SweepRange / 2);
  Coord[0] := DPoint(c, b);
  Coord[1] := DPoint(c + x, y);
  Coord[2] := DPoint(c + x, -y);
  Coord[3] := DPoint(c, -b);
  for i := 0 to 3 do
  begin
    C2[i].x := CenterX + RadiusX * (Coord[i].x * cc + Coord[i].y * ss) - 0.0001;
    C2[i].y := CenterY + RadiusY * (-Coord[i].x * ss + Coord[i].y * cc) - 0.0001;
  end;
  if UseMoveTo then RawMoveTo(C2[0].x, C2[0].y);
  RawCurveto(C2[1].x, C2[1].y, C2[2].x, C2[2].y, C2[3].x, C2[3].y);
end;

procedure TPDFPage.Ellipse(X1, Y1, X2, Y2: Extended);
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('Ellipse(' + FormatFloat(x1) + ',' + FormatFloat(y1) + ',' + FormatFloat(x2) + ',' + FormatFloat(y2) + ');');
{$ENDIF}
  RawEllipse(ExtToIntX(X1), ExtToIntY(Y1), ExtToIntX(X2), ExtToIntY(Y2));
end;

procedure TPDFPage.EndText;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('EndText;');
{$ENDIF}
  AppendAction('ET');
  FTextInited := False;
end;

procedure TPDFPage.EoClip;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('EoClip;');
{$ENDIF}
  AppendAction('W*');
end;

procedure TPDFPage.EoFill;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('EoFill;');
{$ENDIF}
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  AppendAction('f*');
end;

procedure TPDFPage.EoFillAndStroke;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('EoFillAndStroke;');
{$ENDIF}
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  AppendAction('B*');
end;

function TPDFPage.ExtToIntX(AX: Extended): Extended;
begin
  Result := AX / D2P;
  Factions := True;
end;

function TPDFPage.ExtToIntY(AY: Extended): Extended;
begin
  Result := FHeight - AY / D2P;
  Factions := True;
end;

procedure TPDFPage.Fill;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('Fill;');
{$ENDIF}
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  AppendAction('f');
end;

procedure TPDFPage.FillAndStroke;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('FillAndStroke;');
{$ENDIF}
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  AppendAction('B');
end;

procedure TPDFPage.GStateRestore;
begin
  if FSaveCount <> 1 then
  begin
{$IFDEF PASStream}
    FOwner.SavePasStream('GStateRestore;');
{$ENDIF}
    AppendAction('Q');
    Dec(FSaveCount);
  end;
end;

procedure TPDFPage.GStateSave;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('GStateSave;');
{$ENDIF}
  Inc(FSaveCount);
  AppendAction('q');
end;

function TPDFPage.IntToExtX(AX: Extended): Extended;
begin
  Result := AX * D2P;
  Factions := True;
end;

function TPDFPage.IntToExtY(AY: Extended): Extended;
begin
  Result := (FHeight - AY) * D2P;
  Factions := True;
end;

procedure TPDFPage.LineTo(X, Y: Extended);
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('LineTo(' + FormatFloat(x) + ',' + FormatFloat(Y) + ');');
{$ENDIF}
  RawLineTo(ExttointX(X), ExtToIntY(Y));
end;

procedure TPDFPage.MoveTo(X, Y: Extended);
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('MoveTo(' + FormatFloat(x) + ',' + FormatFloat(Y) + ');');
{$ENDIF}
  RawMoveTo(ExttointX(X), ExtToIntY(Y));
end;

procedure TPDFPage.NewPath;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('NewPath;');
{$ENDIF}
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  AppendAction('n');
end;

procedure TPDFPage.NoDash;
begin
  SetDash('[] 0');
end;

procedure TPDFPage.Pie(X1, Y1, x2, y2, BegAngle, EndAngle: Extended);
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('Pie(' + FormatFloat(x1) + ',' + FormatFloat(y1) + ',' + FormatFloat(x2) + ',' + FormatFloat(y2) + ',' +
    FormatFloat(BegAngle) + ',' + FormatFloat(EndAngle) + ');');
{$ENDIF}
  RawPie(ExtToIntX(X1), ExtToIntY(Y1), ExtToIntX(X2), ExtToIntY(Y2), -EndAngle, -BegAngle);
end;

procedure TPDFPage.Pie(X1, Y1, x2, y2, x3, y3, x4, y4: Extended);
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('Pie(' + FormatFloat(x1) + ',' + FormatFloat(y1) + ',' + FormatFloat(x2) + ',' + FormatFloat(y2) + ',' +
    FormatFloat(x3) + ',' + FormatFloat(y3) + ',' + FormatFloat(x4) + ',' + FormatFloat(y4) + ')');
{$ENDIF}
  RawPie(ExtToIntX(X1), ExtToIntY(Y1), ExtToIntX(X2),
    ExtToIntY(Y2), ExtToIntX(X4), ExtToIntY(Y4), ExtToIntX(X3), ExtToIntY(y3));
end;

function TPDFPage.RawArc(X1, Y1, x2, y2, x3, y3, x4,
  y4: Extended): TDoublePoint;
var
  CenterX, CenterY: Extended;
  RadiusX, RadiusY: Extended;
  StartAngle,
    EndAngle,
    SweepRange: Extended;
  UseMoveTo: Boolean;
begin
  CenterX := (x1 + x2) / 2;
  CenterY := (y1 + y2) / 2;
  RadiusX := (abs(x1 - x2) - 1) / 2;
  RadiusY := (abs(y1 - y2) - 1) / 2;
  if RadiusX < 0 then RadiusX := 0;
  if RadiusY < 0 then RadiusY := 0;

  StartAngle := ArcTan2(-(y3 - CenterY) * RadiusX,
    (x3 - CenterX) * RadiusY);
  EndAngle := ArcTan2(-(y4 - CenterY) * RadiusX,
    (x4 - CenterX) * RadiusY);
  SweepRange := EndAngle - StartAngle;

  if SweepRange < 0 then SweepRange := SweepRange + 2 * PI;

  Result := DPoint(CenterX + RadiusX * cos(StartAngle),
    CenterY - RadiusY * sin(StartAngle));

  UseMoveTo := True;
  while SweepRange > PI / 2 do
  begin
    DrawArcWithBezier(CenterX, CenterY, RadiusX, RadiusY,
      StartAngle, PI / 2, UseMoveTo);
    SweepRange := SweepRange - PI / 2;
    StartAngle := StartAngle + PI / 2;
    UseMoveTo := False;
  end;
  if SweepRange >= 0 then
    DrawArcWithBezier(CenterX, CenterY, RadiusX, RadiusY,
      StartAngle, SweepRange, UseMoveTo);
end;


function TPDFPage.RawArc(X1, Y1, x2, y2, BegAngle,
  EndAngle: Extended): TDoublePoint;
var
  CenterX, CenterY: Extended;
  RadiusX, RadiusY: Extended;
  StartAngle,
    EndsAngle,
    SweepRange: Extended;
  UseMoveTo: Boolean;
begin
  CenterX := (x1 + x2) / 2;
  CenterY := (y1 + y2) / 2;
  RadiusX := (abs(x1 - x2) - 1) / 2;
  RadiusY := (abs(y1 - y2) - 1) / 2;
  if RadiusX < 0 then RadiusX := 0;
  if RadiusY < 0 then RadiusY := 0;

  StartAngle := BegAngle * pi / 180;
  EndsAngle := EndAngle * pi / 180;
  SweepRange := EndsAngle - StartAngle;

  if SweepRange < 0 then SweepRange := SweepRange + 2 * PI;

  Result := DPoint(CenterX + RadiusX * cos(StartAngle),
    CenterY - RadiusY * sin(StartAngle));
  UseMoveTo := True;
  while SweepRange > PI / 2 do
  begin
    DrawArcWithBezier(CenterX, CenterY, RadiusX, RadiusY,
      StartAngle, PI / 2, UseMoveTo);
    SweepRange := SweepRange - PI / 2;
    StartAngle := StartAngle + PI / 2;
    UseMoveTo := False;
  end;
  if SweepRange >= 0 then
    DrawArcWithBezier(CenterX, CenterY, RadiusX, RadiusY,
      StartAngle, SweepRange, UseMoveTo);
end;

procedure TPDFPage.RawCircle(X, Y, R: Extended);
const
  b: Extended = 0.5522847498;
begin
  RawMoveto(X + R, Y);
  RawCurveto(X + R, Y + b * R, X + b * R, Y + R, X, Y + R);
  RawCurveto(X - b * R, Y + R, X - R, Y + b * R, X - R, Y);
  RawCurveto(X - R, Y - b * R, X - b * R, Y - R, X, Y - R);
  RawCurveto(X + b * R, Y - R, X + R, Y - b * R, X + R, Y);
end;

procedure TPDFPage.RawConcat(A, B, C, D, E, F: Extended);
begin
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  FF.a := A;
  FF.b := B;
  FF.c := C;
  FF.d := D;
  FF.x := E;
  FF.y := F;
  AppendAction(FormatFloat(A) + ' ' + FormatFloat(B) + ' ' +
    FormatFloat(C) + ' ' + FormatFloat(D) + ' ' +
    FormatFloat(E) + ' ' + FormatFloat(F) + ' cm');
end;

procedure TPDFPage.RawCurveto(X1, Y1, X2, Y2, X3, Y3: Extended);
begin
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  AppendAction(FormatFloat(x1) + ' ' + FormatFloat(y1) + ' ' + FormatFloat(x2) + ' ' +
    FormatFloat(y2) + ' ' + FormatFloat(x3) + ' ' + FormatFloat(y3) + ' c');
  FX := X3;
  FY := Y3;
end;

procedure TPDFPage.RawEllipse(x1, y1, x2, y2: Extended);
const
  b = 0.5522847498;
var
  RX, RY, X, Y: Extended;
begin
  Rx := (x2 - x1) / 2;
  Ry := (y2 - y1) / 2;
  X := x1 + Rx;
  Y := y1 + Ry;
  RawMoveto(X + Rx, Y);
  RawCurveto(X + RX, Y + b * RY, X + b * RX, Y + RY, X, Y + RY);
  RawCurveto(X - b * RX, Y + RY, X - RX, Y + b * RY, X - RX, Y);
  RawCurveto(X - RX, Y - b * RY, X - b * RX, Y - RY, X, Y - RY);
  RawCurveto(X + b * RX, Y - RY, X + RX, Y - b * RY, X + RX, Y);
end;

procedure TPDFPage.RawLineTo(X, Y: Extended);
begin
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  AppendAction(FormatFloat(X) + ' ' + FormatFloat(Y) + ' l');
  FX := X;
  FY := Y;
end;

procedure TPDFPage.RawMoveTo(X, Y: Extended);
begin
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  AppendAction(FormatFloat(X) + ' ' + FormatFloat(Y) + ' m');
  FX := X;
  FY := Y;
end;


function TPDFPage.RawPie(X1, Y1, x2, y2, BegAngle,
  EndAngle: Extended): TDoublePoint;
var
  CX, CY: Extended;
  dp: TDoublePoint;
begin
  dp := RawArc(X1, Y1, x2, y2, BegAngle, EndAngle);
  CX := X1 + (x2 - X1) / 2;
  CY := Y1 + (Y2 - Y1) / 2;
  RawLineTo(CX, CY);
  RawMoveTo(dp.x, dp.y);
  RawLineTo(CX, CY);
end;

function TPDFPage.RawPie(X1, Y1, x2, y2, x3, y3, x4,
  y4: Extended): TDoublePoint;
var
  CX, CY: Extended;
  dp: TDoublePoint;
begin
  dp := RawArc(X1, Y1, x2, y2, x3, y3, x4, y4);
  CX := X1 + (x2 - X1) / 2;
  CY := Y1 + (Y2 - Y1) / 2;
  RawLineTo(CX, CY);
  RawMoveTo(dp.x, dp.y);
  RawLineTo(CX, CY);
end;

procedure TPDFPage.RawRect(X, Y, W, H: Extended);
begin
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  AppendAction(FormatFloat(x) + ' ' + FormatFloat(y) + ' ' +
    FormatFloat(w) + ' ' + FormatFloat(h) + ' re');
  FX := X;
  FY := Y;
end;

procedure TPDFPage.RawRectRotated(X, Y, W, H, Angle: Extended);
var
  xo, yo: Extended;
begin
  RawMoveto(x, y);
  RotateCoordinate(w, 0, angle, xo, yo);
  RawLineto(x + xo, y + yo);
  RotateCoordinate(w, h, angle, xo, yo);
  RawLineto(x + xo, y + yo);
  RotateCoordinate(0, h, angle, xo, yo);
  RawLineto(x + xo, y + yo);
end;

procedure TPDFPage.RawSetTextPosition(X, Y: Extended);
begin
  if not FTextInited then
    raise TPDFException.Create(STextObjectNotInited);
  AppendAction(FormatFloat(FCTM.a) + ' ' + FormatFloat(FCTM.b) + ' ' + FormatFloat(FCTM.c) + ' ' +
    FormatFloat(FCTM.d) + ' ' + FormatFloat(x) + ' ' + FormatFloat(y) + ' Tm');
  FCTM.x := X;
  FCTM.y := Y;
end;

procedure TPDFPage.RawTextOut(X, Y, Orientation: Extended;
  TextStr: string);
begin
  if not FTextInited then
    raise TPDFException.Create(STextObjectNotInited);
  if not FFontInited then
    raise TPDFException.Create(SActiveFontNotSetting);
  RawSetTextPosition(X, Y);
  if Orientation <> FTextAngle then
    Rotatetext(Orientation);
  TextShow(TextStr);
end;

procedure TPDFPage.RawTranslate(XT, YT: Extended);
begin
  RawConcat(1, 0, 0, 1, xt, yt);
end;

procedure TPDFPage.Rectangle(X1, Y1, X2, Y2: Extended);
var
  convw, convh, H, W: Extended;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('Rectangle(' + FormatFloat(x1) + ',' + FormatFloat(y1) + ',' + FormatFloat(x2) + ',' + FormatFloat(y2) + ');');
{$ENDIF}
  NormalizeRect(x1, y1, x2, y2);
  W := X2 - X1;
  H := Y2 - Y1;
  convw := ExtToIntX(X1 + W) - ExtToIntX(X1);
  convh := ExtToIntY(Y1 + H) - ExtToIntY(Y1);
  RawRect(ExtToIntX(X1), ExtToIntY(y1), convw, convh);
end;

procedure TPDFPage.RectRotated(X, Y, W, H, Angle: Extended);
var
  convw, convh: Extended;
begin
  convw := ExtToIntX(X + W) - ExtToIntY(X);
  convh := ExtToIntY(Y + H) - ExtToIntY(Y);
  RawRectRotated(ExtToIntX(X), ExtToIntY(y), convw, convh, Angle);
end;

procedure TPDFPage.Rotate(Angle: Extended);
var
  vsin, vcos: Extended;
begin
  Angle := Angle * (PI / 180);
  vsin := sin(angle);
  vcos := cos(angle);
  RawConcat(vcos, vsin, -vsin, vcos, 0, 0);
end;

procedure TPDFPage.RotateText(Degrees: Extended);
var
  a, b, c, d, e, f, angle, vcos, vsin: Extended;
begin
  if not FTextInited then
    raise TPDFException.Create(STextObjectNotInited);
  FRealAngle := Degrees;
  Degrees := Degrees - FTextAngle;
  angle := PI * degrees / 180.0;
  vcos := cos(angle);
  vsin := sin(angle);
  a := vcos;
  b := vsin;
  c := -vsin;
  d := vcos;
  e := 0.0;
  f := 0.0;
  ConcatTextMatrix(a, b, c, d, e, f);
  FTextAngle := Degrees;
end;

procedure TPDFPage.Save;
var
  I: Integer;
  MS: TMemoryStream;
  CS: TCompressionStream;
  S, O: string;
begin
  PrepareID;
  for I := FSaveCount downto 1 do GStateRestore;
{$IFDEF LLPDFEVAL}
  if not FIsForm then
  begin
    NoDash;
    BeginText;
    SetLineWidth(1);
    SetRGBColorFill(0.5, 0.5, 0.5);
    SetRGBColorStroke(1, 0, 0);
    SetTextRenderingMode(2);
    SetActiveFont('Helvetica', [], 40);
    SetHorizontalScaling(50);
{$IFNDEF CB}
    TextOut((Width - GetTextWidth(s1)) / 2, (Height - 40) shr 1 - 40, 0, s1);
    TextOut((Width - GetTextWidth(s2)) / 2, (Height - 40) shr 1, 0, s2);
    TextOut((Width - GetTextWidth(s3)) / 2, (Height - 40) shr 1 + 40, 0, s3);
{$ELSE}
    TextOutput((Width - GetTextWidth(s1)) / 2, (Height - 40) shr 1 - 40, 0, s1);
    TextOutput((Width - GetTextWidth(s2)) / 2, (Height - 40) shr 1, 0, s2);
    TextOutput((Width - GetTextWidth(s3)) / 2, (Height - 40) shr 1 + 40, 0, s3);
{$ENDIF}
    SetUrl(Rect(round((Width - GetTextWidth(s2)) / 2), (Height - 40) shr 1 + 50, round((Width + GetTextWidth(s2)) / 2),
      (Height - 40) shr 1 + 90), s3);
    EndText;
  end;
{$ENDIF}
  for I := 0 to FForms.Count - 1 do FForms[I].Save;
  ResourceID := FOwner.GetNextID;
  if not FIsForm then
    ContentID := FOwner.GetNextID;
  FSaveCount := 2;
  GStateRestore;
  if FRemoveCR then
  begin
    S := FContent.Text;
    O := '';
    I := Pos(#13#10, S);
    while I <> 0 do
    begin
      O := O + ' ' + Copy(S, 1, I - 1);
      Delete(S, 1, I + 1);
      I := Pos(#13#10, S);
    end;
    O := O + ' ' + S;
    FContent.Text := O;
  end;
  for I := 0 to FAnnot.Count - 1 do
  begin
    TPDFCustomAnnotation(FAnnot[I]).Save;
  end;
  FOwner.StartObj(ResourceID);
  FOwner.SaveToStream(' /ProcSet [/PDF ', False);
  if FTextUsed then FOwner.SaveToStream('/Text ', False);
  if FGrayUsed then FOwner.SaveToStream('/ImageB ', False);
  if FColorUsed then FOwner.SaveToStream('/ImageC ', False);
  FOwner.SaveToStream(']');
  if FLinkedFont.Count > 0 then
  begin
    FOwner.SaveToStream('/Font <<');
    for I := 0 to FLinkedFont.Count - 1 do
      FOwner.SaveToStream('/' + TPDFFont(FLinkedFont[I]).AliasName + ' ' + IntToStr(TPDFFont(FLinkedFont[I]).FontID) + ' 0 R');
    FOwner.SaveToStream('>>');
  end;
  if (FLinkedImages.Count > 0) or (FWaterMark >= 0) or (FForms.Count > 0) then
  begin
    FOwner.SaveToStream(' /XObject <<');
    for I := 0 to FLinkedImages.Count - 1 do
      FOwner.SaveToStream('/' + TPDFImage(FLinkedImages[I]).ImageName + ' ' + IntToStr(TPDFImage(FLinkedImages[I]).PictureID) + ' 0 R');
    if FWaterMark >= 0 then FOwner.SaveToStream('/Form' + IntToStr(FWaterMark) + ' ' + IntToStr(FOwner.FWaterMarks[FWaterMark].PageID) + ' 0 R');
    for I := 0 to FForms.Count - 1 do
      FOwner.SaveToStream('/IF' + IntToStr(FForms.IndexOf(FForms[I])) + ' ' + IntToStr(FForms[I].PageID) + ' 0 R');
    FOwner.SaveToStream('>>');
  end;
  FOwner.CloseHObj;
  FOwner.CloseObj;
  if not FIsForm then
  begin
    if FWaterMark >= 0 then
      FContent.Insert(0, 'q /Form' + IntToStr(FWaterMark) + ' Do   Q');
    FOwner.StartObj(ContentID);
    if FOwner.Compression = ctFlate then
    begin
      MS := TIsMemoryStream.Create;
      try
        CS := TCompressionStream.Create(clDefault, MS);
        try
          FContent.SaveToStream(CS);
        finally
          CS.Free;
        end;
        FOwner.SaveToStream('/Length ' + IntToStr(MS.size));
        FOwner.SaveToStream('/Filter /FlateDecode');
        FOwner.CloseHObj;
        FOwner.StartStream;
        MS.Position := 0;
        EnCodeStream(FOwner.FProtectionEnabled, FOwner.FKey, FOwner.FPKL, ContentID, MS);
        FOwner.FStream.CopyFrom(MS, MS.Size);
        FOwner.SaveToStream('');
      finally
        MS.Free;
      end;
    end else
    begin
      FOwner.SaveToStream('/Length ' + IntToStr(Length(FContent.Text)));
      FOwner.CloseHObj;
      FOwner.StartStream;
      FOwner.SaveToStream(EnCodeString(FOwner.FProtectionEnabled, FOwner.FKey, FOwner.FPKL, ContentID, FContent.Text));
    end;
    FContent.Text := '';
    FOwner.CloseStream;
    FOwner.CloseObj;
    FOwner.StartObj(PageID);
    FOwner.SaveToStream('/Type /Page');
    FOwner.SaveToStream('/Parent ' + IntToStr(FOwner.PagesID) + ' 0 R');
    FOwner.SaveToStream('/Resources ' + IntToStr(ResourceID) + ' 0 R');
    FOwner.SaveToStream('/Contents [' + IntToStr(ContentID) + ' 0 R]');
    if FThumbnail >= 0 then FOwner.SaveToStream('/Thumb ' + IntToStr(FOwner.FImages[FThumbnail].PictureID) + ' 0 R');
    FOwner.SaveToStream('/MediaBox [0 0 ' + IntToStr(FWidth) + ' ' + IntToStr(FHeight) + ']');
    case FRotate of
      pr90: FOwner.SaveToStream('/Rotate 90');
      pr180: FOwner.SaveToStream('/Rotate 180');
      pr270: FOwner.SaveToStream('/Rotate 270');
    end;
    if FAnnot.Count <> 0 then
    begin
      FOwner.SaveToStream('/Annots [');
      for I := 0 to FAnnot.Count - 1 do
        FOwner.SaveToStream(IntToStr(TPDFCustomAnnotation(FAnnot[I]).AnnotID) + ' 0 R');
      FOwner.SaveToStream(']');
    end;
    FOwner.CloseHObj;
    FOwner.CloseObj;
  end else
  begin
    FOwner.StartObj(PageID);
    FOwner.SaveToStream('/Type /XObject');
    FOwner.SaveToStream('/Subtype /Form');
    FOwner.SaveToStream('/Resources ' + IntToStr(ResourceID) + ' 0 R');
    FOwner.SaveToStream('/Matrix [' + FormatFloat(FMatrix.a) + ' ' + FormatFloat(FMatrix.b) + ' ' + FormatFloat(FMatrix.c) + ' ' +
      FormatFloat(FMatrix.d) + ' ' + FormatFloat(FMatrix.x) + ' ' + FormatFloat(FMatrix.y) + ' ]');
    FOwner.SaveToStream('/BBox [0 0 ' + IntToStr(FWidth) + ' ' + IntToStr(FHeight) + ']');
    if FOwner.Compression = ctFlate then
    begin
      MS := TIsMemoryStream.Create;
      try
        CS := TCompressionStream.Create(clDefault, MS);
        try
          FContent.SaveToStream(CS);
        finally
          CS.Free;
        end;
        FOwner.SaveToStream('/Length ' + IntToStr(MS.size));
        FOwner.SaveToStream('/Filter /FlateDecode');
        FOwner.CloseHObj;
        FOwner.StartStream;
        MS.Position := 0;
        EnCodeStream(FOwner.FProtectionEnabled, FOwner.FKey, FOwner.FPKL, PageID, MS);
        FOwner.FStream.CopyFrom(MS, MS.Size);
        FOwner.SaveToStream('');
      finally
        MS.Free;
      end;
    end else
    begin
      FOwner.SaveToStream('/Length ' + IntToStr(Length(FContent.Text)));
      FOwner.CloseHObj;
      FOwner.StartStream;
      FOwner.SaveToStream(EnCodeString(FOwner.FProtectionEnabled, FOwner.FKey, FOwner.FPKL, PageID, FContent.Text));
    end;
    FContent.Text := '';
    FOwner.CloseStream;
    FOwner.CloseObj;
  end;
end;

procedure TPDFPage.Scale(SX, SY: Extended);
begin
  RawConcat(sx, 0, 0, sy, 0, 0);
end;

procedure TPDFPage.SetActiveFont(FontName: string; FontStyle: TFontStyles; FontSize: Extended; FontCharset: TFontCharset = ANSI_CHARSET);
{$IFDEF PASStream}
var
  R: string;
{$ENDIF}
begin
{$IFDEF PASStream}
  R := '[';
  if fsBold in FontStyle then R := R + 'fsBold,';
  if fsItalic in FontStyle then R := R + 'fsItalic,';
  if fsStrikeOut in FontStyle then R := R + 'fsStrikeOut,';
  if fsUnderLine in FontStyle then R := R + 'fsUnderLine,';
  if R[Length(R)] = ',' then Delete(R, Length(R), 1);
  R := R + ']';
  FOwner.SavePasStream('SetActiveFont(''' + FontName + ''',' + R + ',' + FormatFloat(FontSize) + ',' + FormatFloat(FontCharset) + ');');
{$ENDIF}
  if not FTextInited then AppendAction('BT');
  FOwner.FFonts.CheckFont(FontName, FontStyle, FontCharset);
  if FLinkedFont.IndexOf(Pointer(FOwner.FFonts.FLast)) = -1 then
    FLinkedFont.Add(Pointer(FOwner.FFonts.FLast));
  AppendAction('/' + FOwner.FFonts.FLast.AliasName + ' ' + FormatFloat(FontSize) + ' Tf');
  FCurrentFontIndex := FOwner.FFonts.FLastIndex;
  FCurrentFontSize := FontSize;
  FFontInited := True;
  if FEmulationEnabled then
  begin
    FEUnderLine := fsUnderline in FontStyle;
    FEStrike := fsStrikeOut in FontStyle;
  end;
  if FOwner.FFonts.FLast.Standart then
  begin
    FEBold := False;
    FEItalic := False;
    if not FTextInited then AppendAction('ET')
  end else
    if FEmulationEnabled then
    begin
      FEBold := (fsBold in FontStyle) and (not (fsBold in FOwner.FFonts.FLast.RealStyle));
      FEItalic := (fsItalic in FontStyle) and (not (fsItalic in FOwner.FFonts.FLast.RealStyle));
      if not FTextInited then AppendAction('ET') else SkewText(0, 0); ;
    end;
end;

procedure TPDFPage.SetCharacterSpacing(Spacing: Extended);
begin
  if not FTextInited then
    AppendAction('BT');
  Spacing := Spacing / D2P;
  AppendAction(FormatFloat(Spacing) + ' Tc');
  FCharSpace := Spacing;
  if not FTextInited then
    AppendAction('ET');
end;

procedure TPDFPage.SetCMYKColor(C, M, Y, K: Extended);
begin
  SetCMYKColorFill(C, M, Y, K);
  SetCMYKColorStroke(C, M, Y, K);
end;

procedure TPDFPage.SetCMYKColorFill(C, M, Y, K: Extended);
begin
  AppendAction(FormatFloat(C) + ' ' + FormatFloat(M) + ' ' + FormatFloat(Y) + ' ' + FormatFloat(K) + ' k');
end;

procedure TPDFPage.SetCMYKColorStroke(C, M, Y, K: Extended);
begin
  AppendAction(FormatFloat(C) + ' ' + FormatFloat(M) + ' ' + FormatFloat(Y) + ' ' + FormatFloat(K) + ' K');
end;

procedure TPDFPage.SetDash(DashSpec: string);
begin
  if FCurrentDash <> DashSpec then
  begin
{$IFDEF PASStream}
    FOwner.SavePasStream('SetDash(' + DashSpec + ');');
{$ENDIF}
    AppendAction(DashSpec + ' d');
    FCurrentDash := DashSpec;
  end;
end;

procedure TPDFPage.SetFlat(FlatNess: integer);
begin
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  AppendAction(Format('%d i', [FlatNess]));
end;

procedure TPDFPage.SetGray(Gray: Extended);
begin
  SetGrayFill(Gray);
  SetGrayStroke(Gray);
end;

procedure TPDFPage.SetGrayFill(Gray: Extended);
begin
  AppendAction(FormatFloat(Gray) + ' g');
end;

procedure TPDFPage.SetGrayStroke(Gray: Extended);
begin
  AppendAction(FormatFloat(Gray) + ' G');
end;

procedure TPDFPage.SetHeight(const Value: Integer);
var
  DC: HDC;
  I: Integer;
begin
  if Factions then
    raise TPDFException.Create(SPageInProgress);
  if FHeight <> Value then
  begin
{$IFDEF PASStream}
    FOwner.SavePasStream('Height:=' + FormatFloat(Value) + ';');
{$ENDIF}

    FHeight := round(Value / D2P);
    FPageSize := psUserDefined;
    if FMF <> nil then
      if not FAskCanvas then
      begin
        DC := GetDC(0);
        I := GetDeviceCaps(dc, LOGPIXELSX);
        ReleaseDC(0, DC);
        FMF.Height := MulDiv(FHeight, I, 72);
        FCanvas.Free;
        FCanvas := TMetafileCanvas.Create(FMF, 0);
      end;
  end;
end;

procedure TPDFPage.SetHorizontalScaling(Scale: Extended);
begin
  if not FTextInited then
    raise TPDFException.Create(STextObjectNotInited);
  AppendAction(FormatFloat(Scale) + ' Tz');
  FHorizontalScaling := Scale;
end;

procedure TPDFPage.SetLineCap(LineCap: TPDFLineCap);
begin
  AppendAction(Format('%d j', [Ord(LineCap)]));
end;

procedure TPDFPage.SetLineJoin(LineJoin: TPDFLineJoin);
begin
  AppendAction(Format('%d J', [Ord(LineJoin)]));
end;

procedure TPDFPage.SetLineWidth(lw: Extended);
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('SetLineWidth(' + FormatFloat(lw) + ');');
{$ENDIF}
  AppendAction(FormatFloat(lw / D2P) + ' w');
end;

procedure TPDFPage.SetMiterLimit(MiterLimit: Extended);
begin
  MiterLimit := MiterLimit / D2P;
  AppendAction(FormatFloat(MiterLimit) + ' M');
end;

procedure TPDFPage.SetPageSize(Value: TPDFPageSize);
var
  I: Integer;
  DC: HDC;
begin
  if Factions then
    raise TPDFException.Create(SPageInProgress);
  FPageSize := Value;
  case Value of
    psLetter:
      begin
        FHeight := 792;
        FWidth := 612;
      end;
    psA4:
      begin
        FHeight := 842;
        FWidth := 595;
      end;
    psA3:
      begin
        FHeight := 1190;
        FWidth := 842;
      end;
    psLegal:
      begin
        FHeight := 1008;
        FWidth := 612;
      end;
    psB5:
      begin
        FHeight := 728;
        FWidth := 516;
      end;
    psC5:
      begin
        FHeight := 649;
        FWidth := 459;
      end;
    ps8x11:
      begin
        FHeight := 792;
        FWidth := 595;
      end;
    psB4:
      begin
        FHeight := 1031;
        FWidth := 728;
      end;
    psA5:
      begin
        FHeight := 595;
        FWidth := 419;
      end;
    psFolio:
      begin
        FHeight := 936;
        FWidth := 612;
      end;
    psExecutive:
      begin
        FHeight := 756;
        FWidth := 522;
      end;
    psEnvB4:
      begin
        FHeight := 1031;
        FWidth := 728;
      end;
    psEnvB5:
      begin
        FHeight := 708;
        FWidth := 499;
      end;
    psEnvC6:
      begin
        FHeight := 459;
        FWidth := 323;
      end;
    psEnvDL:
      begin
        FHeight := 623;
        FWidth := 312;
      end;
    psEnvMonarch:
      begin
        FHeight := 540;
        FWidth := 279;
      end;
    psEnv9:
      begin
        FHeight := 639;
        FWidth := 279;
      end;
    psEnv10:
      begin
        FHeight := 684;
        FWidth := 297;
      end;
    psEnv11:
      begin
        FHeight := 747;
        FWidth := 324;
      end;
  end;
  SetOrientation(FOrientation);
  if FMF <> nil then
    if not FAskCanvas then
    begin
      DC := GetDC(0);
      I := GetDeviceCaps(dc, LOGPIXELSX);
      ReleaseDC(0, DC);
      FMF.Height := MulDiv(FHeight, I, 72);
      FMF.Width := MulDiv(FWidth, I, 72);
      FCanvas.Free;
      FCanvas := TMetafileCanvas.Create(FMF, 0);
    end;
end;


procedure TPDFPage.SetRGBColor(R, G, B: Extended);
begin
  SetRGBcolorFill(R, G, B);
  SetRGBcolorStroke(R, G, B);
end;

procedure TPDFPage.SetRGBColorFill(R, G, B: Extended);
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('SetRGBColorFill(' + FormatFloat(R) + ',' + FormatFloat(G) + ',' + FormatFloat(B) + ');');
{$ENDIF}
  AppendAction(FormatFloat(R) + ' ' + FormatFloat(G) + ' ' + FormatFloat(B) + ' rg');
end;

procedure TPDFPage.SetRGBColorStroke(R, G, B: Extended);
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('SetRGBColorStroke(' + FormatFloat(R) + ',' + FormatFloat(G) + ',' + FormatFloat(B) + ');');
{$ENDIF}
  AppendAction(FormatFloat(R) + ' ' + FormatFloat(G) + ' ' + FormatFloat(B) + ' RG');
end;

procedure TPDFPage.SetRotate(const Value: TPDFPageRotate);
begin
  FRotate := Value;
end;

procedure TPDFPage.SetTextMatrix(A, B, C, D, X, Y: Extended);
begin
  if not FTextInited then
    raise TPDFException.Create(STextObjectNotInited);
  AppendAction(FormatFloat(A) + ' ' + FormatFloat(B) + ' ' + FormatFloat(C) + ' ' +
    FormatFloat(D) + ' ' + FormatFloat(X) + ' ' + FormatFloat(Y) + ' Tm');
  FCTM.a := A;
  FCTM.b := B;
  FCTM.c := C;
  FCTM.d := D;
  FCTM.x := X;
  FCTM.y := Y;
end;

procedure TPDFPage.SetTextPosition(X, Y: Extended);
begin
  RawSetTextPosition(ExtToIntX(X), ExtToIntY(Y) - FCurrentFontSize);
end;

procedure TPDFPage.SetTextRenderingMode(Mode: integer);
begin
  if not FTextInited then
    AppendAction('BT');
  AppendAction(Format('%d Tr', [mode]));
  if not FTextInited then
    AppendAction('ET');
  FRender := Mode;
end;

procedure TPDFPage.SetTextRise(Rise: Extended);
begin
  if not FTextInited then
    raise TPDFException.Create(STextObjectNotInited);
  Rise := Rise / D2P;
  AppendAction(FormatFloat(Rise) + ' Ts');
  FRise := Rise;
end;

procedure TPDFPage.SetWidth(const Value: Integer);
var
  DC: HDC;
  I: Integer;
begin
  if Factions then
    raise TPDFException.Create(SPageInProgress);
  if FWidth <> Value then
  begin
{$IFDEF PASStream}
    FOwner.SavePasStream('Width:=' + FormatFloat(Value) + ';');
{$ENDIF}
    FWidth := round(Value / D2P);
    FPageSize := psUserDefined;
    if FMF <> nil then
      if not FAskCanvas then
      begin
        DC := GetDC(0);
        I := GetDeviceCaps(dc, LOGPIXELSX);
        ReleaseDC(0, DC);
        FMF.Width := MulDiv(FWidth, I, 72);
        FCanvas.Free;
        FCanvas := TMetafileCanvas.Create(FMF, 0);
      end;
  end;
end;


procedure TPDFPage.SetWordSpacing(Spacing: Extended);
begin
  Spacing := Spacing / D2P;
  if not FTextInited then
    AppendAction('BT');
  AppendAction(FormatFloat(Spacing) + ' Tw');
  if not FTextInited then
    AppendAction('ET');
  FWordSpace := Spacing;
end;

procedure TPDFPage.SkewText(Alpha, Beta: Extended);
var
  a, b, c, d, e, f: Extended;
begin
  if not FTextInited then
    raise TPDFException.Create(STextObjectNotInited);
  if FEItalic then Beta := Beta + 7;
  a := 1.0;
  b := tan(PI * alpha / 180.0);
  c := tan(PI * beta / 180.0);
  d := 1.0;
  e := 0.0;
  f := 0.0;
  ConcatTextMatrix(a, b, c, d, e, f);
end;

procedure TPDFPage.Stroke;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('Stroke;');
{$ENDIF}
  if FTextInited then
    raise TPDFException.Create(STextObjectInited);
  AppendAction('S');
end;

function TPDFPage.TextOutBox(LTCornX, LTCornY, Interval, BoxWidth, BoxHeight: integer; TextStr: string): integer;
var
  i, Count: integer;
  StrLine, OutTxtLine: string;
  Ch: char;
  CWidth, StrWidth, OutWidth: Extended;
begin
  if not FFontInited then
    raise TPDFException.Create(SActiveFontNotSetting);
  Result := 0; //
  if FOwner.FFonts[FCurrentFontIndex].IsCJK then Exit;
  i := 1;
  Count := 0;
  StrWidth := 0;
  OutWidth := 0;
  OutTxtLine := '';
  StrLine := '';
  while i <= Length(TextStr) do
  begin
    ch := TextStr[i];
    CWidth := FOwner.FFonts[FCurrentFontIndex].GetWidth(Ord(ch)) * FCurrentFontSize / 1000;
    if FHorizontalScaling <> 100 then
      CWidth := CWidth * FHorizontalScaling / 100;
    if CWidth > 0 then
      CWidth := CWidth + FCharSpace
    else
      CWidth := 0;
    if (ch = ' ') and (FWordSpace > 0) and (i <> Length(TextStr)) then
      CWidth := CWidth + FWordSpace;
    if ((OutWidth + StrWidth + CWidth) < BoxWidth) and (i < Length(TextStr)) and (not (Ch in [#10, #13])) then
    begin
      StrWidth := StrWidth + CWidth;
      StrLine := StrLine + ch;
      if ch = ' ' then
      begin
        OutTxtLine := OutTxtLine + StrLine;
        OutWidth := OutWidth + StrWidth;
        StrWidth := 0;
        StrLine := '';
      end;
    end
    else
    begin
      if (ch = #13) and (i <> Length(TextStr)) and (TextStr[i + 1] = #10) then Inc(i);
      if i = Length(TextStr) then
      begin
        StrWidth := StrWidth + CWidth;
        StrLine := StrLine + ch;
      end;
      if OutWidth = 0 then
      begin
{$IFNDEF CB}
        TextOut(LTCornX, LTCornY + Count * Interval, 0, OutTxtLine + StrLine);
{$ELSE}
        TextOutput(LTCornX, LTCornY + Count * Interval, 0, OutTxtLine + StrLine);
{$ENDIF}
        Result := Result + Length(OutTxtLine + StrLine);
        StrLine := ch;
        StrWidth := CWidth;
      end
      else
      begin
{$IFNDEF CB}
        TextOut(LTCornX, LTCornY + Count * Interval, 0, OutTxtLine);
{$ELSE}
        TextOutput(LTCornX, LTCornY + Count * Interval, 0, OutTxtLine);
{$ENDIF}
        Result := Result + Length(OutTxtLine);
        StrLine := StrLine + ch;
        StrWidth := StrWidth + CWidth;
      end;
      OutTxtLine := '';
      OutWidth := 0;
      Inc(Count);
      if Count * Interval > BoxHeight then exit;
    end;
    Inc(i);
  end;
end;

{$IFDEF CB}

procedure TPDFPage.TextOutput(X, Y, Orientation: Extended; TextStr: string);
{$ELSE}

procedure TPDFPage.TextOut(X, Y, Orientation: Extended; TextStr: string);
{$ENDIF}
  procedure CheckURL(CH: string);
  var
    S: string;
    LB, L: Extended;
    A: Integer;
    I: Integer;
  begin
    A := Pos(uppercase(CH), UpperCase(TextStr));
    if A <> 0 then
    begin
      if A <> 1 then
        if TextStr[A - 1] > #32 then Exit;
      S := Copy(TextStr, 1, A - 1);
      LB := GetTextWidth(S);
      S := Copy(TextStr, A, Length(TextStr));
      I := 1;
      while (S[I] > #32) and (I <= Length(S)) do Inc(I);
      S := Copy(S, 1, I);
      L := GetTextWidth(S);
      SetUrl(Rect(round(X + LB), round(y + 0.33 * FCurrentFontSize), round(X + LB + L), round(Y + FCurrentFontSize)), S);
    end;
  end;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('TextOut(' + FormatFloat(x) + ',' + FormatFloat(y) + ',' + FormatFloat(Orientation) + ',''' + textStr + ''');');
{$ENDIF}
  if Orientation = 0 then
  begin
    RawTextOut(ExtToIntX(X), ExtToIntY(Y) - FCurrentFontSize * 0.9, Orientation, TextStr);
    if FOwner.FACURL then
      if not FIsForm then
      begin
        CheckURL('http://');
        CheckURL('mailto:');
        CheckURL('ftp://');
      end;
  end else
  begin
    RawTextOut(ExtToIntX(X) + FCurrentFontSize * sin(Orientation * Pi / 180),
      ExtToIntY(Y) - FCurrentFontSize * cos(Orientation * Pi / 180), Orientation, TextStr);
  end;
  FTextAngle := Orientation;
end;

procedure TPDFPage.TextShow(TextStr: string);
var I: Integer;
  F: TPDFFont;
  s: string;
begin
  if not FTextInited then
    raise TPDFException.Create(STextObjectNotInited);
  if not FFontInited then
    raise TPDFException.Create(SActiveFontNotSetting);
  FTextUsed := True;
  F := FOwner.FFonts[FCurrentFontIndex];
  if not (F.Standart or F.IsCJK) then
    for I := 1 to Length(TextStr) do
      F.FUsed[Ord(textstr[I])] := True;
//  if FOwner.FFonts[FCurrentFontIndex].RealCharset <> symbol_charset then
  s := EscapeSpecialChar(TextStr);
//  else s := StrToOctet(TextStr);
  AppendAction('(' + s + ') Tj');
  if FEBold then
  begin
    AppendAction('-0.02 -0.02 TD');
    AppendAction('(' + s + ') Tj');
    AppendAction('0.02 0 TD');
    AppendAction('(' + s + ') Tj');
    AppendAction('-0.02 0.02 TD');
    AppendAction('(' + s + ') Tj');
  end;
  if FEUnderLine or FEStrike then
  begin
    AppendAction('ET');
    FTextInited := False;
  end;
  if FEUnderLine then
  begin
    RawRectRotated(FCTM.x + 3 * sin((PI / 180) * FRealAngle), FCTM.y - 3 * cos(FRealAngle * (PI / 180)),
      GetTextWidth(TextStr) / D2P, FCurrentFontSize * 0.05, FRealAngle);
  end;
  if FEStrike then
  begin
    RawRectRotated(FCTM.x - FCurrentFontSize / 4 * sin((PI / 180) * FRealAngle), FCTM.y + FCurrentFontSize / 4 * cos(FRealAngle * (PI / 180)),
      GetTextWidth(TextStr) / d2p, FCurrentFontSize * 0.05, FRealAngle);
  end;
  if FEUnderLine or FEStrike then
  begin
    case FRender of
      0: Fill;
      1: Stroke;
      2: FillAndStroke;
    else Fill;
    end;
    AppendAction('BT');
    FTextInited := True;
    AppendAction(FormatFloat(FCTM.a) + ' ' + FormatFloat(FCTM.b) + ' ' + FormatFloat(FCTM.c) + ' ' +
      FormatFloat(FCTM.d) + ' ' + FormatFloat(FCTM.x) + ' ' + FormatFloat(FCTM.y) + ' Tm');
  end;
end;

procedure TPDFPage.Translate(XT, YT: Extended);
begin
  RawTranslate(ExtToIntX(XT), ExtToIntY(YT));
end;

procedure TPDFPage.RawShowImage(ImageIndex: Integer; x, y, w, h, angle: Extended);
begin
  if (ImageIndex < 0) or (ImageIndex > FOwner.Fimages.Count - 1) then
    raise TPDFException.Create(SOutOfRange);
  if (FOwner.Fimages[ImageIndex].FBitPerPixel = 1) or (FOwner.Fimages[ImageIndex].GrayScale) then
    FGrayUsed := True else FColorUsed := True;
  GStateSave;
  RawTranslate(x, y);
  if Abs(angle) > 0.001 then
    Rotate(angle);
  RawConcat(w, 0, 0, h, 0, 0);
  AppendAction('/' + FOwner.Fimages[ImageIndex].ImageName + ' Do');
  GStateRestore;
  if FLinkedImages.IndexOf(Pointer(FOwner.Fimages[ImageIndex])) = -1 then FLinkedImages.Add(Pointer(FOwner.Fimages[ImageIndex]));
end;

procedure TPDFPage.ShowImage(ImageIndex: Integer; x, y, w, h, angle: Extended);
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('ShowImage(' + FormatFloat(ImageIndex) + ',' + FormatFloat(x) + ',' + FormatFloat(y) + ',' + FormatFloat(w) + ',' +
    FormatFloat(h) + ',' + FormatFloat(angle) + ');');
{$ENDIF}
  RawShowImage(ImageIndex, ExtToIntX(X), ExtToIntY(y) - h / d2p, w / d2p, h / d2p, angle);
end;

procedure TPDFPage.DeleteAllAnnotations;
var
  i: Integer;
begin
  for i := 0 to FAnnot.Count - 1 do
    TPDFCustomAnnotation(FAnnot[i]).Free;
  FAnnot.Clear;
end;

function TPDFPage.GetTextWidth(Text: string): Extended;
var
  i: integer;
  ch: char;
  tmpWidth: Extended;
begin
  if not FFontInited then
    raise TPDFException.Create(SActiveFontNotSetting);
  Result := 0;
  if FOwner.FFonts[FCurrentFontIndex].IsCJK then Exit;
  for i := 1 to Length(Text) do
  begin
    ch := Text[i];
    tmpWidth := FOwner.FFonts[FCurrentFontIndex].GetWidth(Ord(ch)) * FCurrentFontSize / 1000;
    if FHorizontalScaling <> 100 then
      tmpWidth := tmpWidth * FHorizontalScaling / 100;
    if tmpWidth > 0 then
      tmpWidth := tmpWidth + FCharSpace
    else
      tmpWidth := 0;
    if (ch = ' ') and (FWordSpace > 0) and (i <> Length(Text)) then
      tmpWidth := tmpWidth + FWordSpace;
    Result := Result + tmpWidth;
  end;
  Result := IntToExtX(Result - FCharSpace);
end;

function TPDFPage.SetAnnotation(ARect: TRect; Title, Text: string;
  Color: TColor; Flags: TAnnotationFlags; Opened: Boolean; Charset: TFontCharset = ANSI_CHARSET): TPDFCustomAnnotation;
var
  A: TPDFTextAnnotation;
begin
  if FIsForm then
    raise TPDFException.Create(SCannotCreateAnnotationToWatermark);
  A := TPDFTextAnnotation.Create(Self);
  A.Caption := Title;
  A.Text := Text;
  A.BorderColor := Color;
  A.Box := Rect(ARect.Left, ARect.Top, ARect.Right, ARect.Bottom);
  A.Flags := Flags;
  A.Charset := Charset;
  A.Opened := Opened;
  Result := A;
end;


function TPDFPage.SetLinkToPage(ARect: TRect; PageIndex,
  TopOffset: Integer): TPDFCustomAnnotation;
var
  A: TPDFActionAnnotation;
begin
  if FIsForm then
    raise TPDFException.Create(SCannotCreateLinkToPageToWatermark);
  A := TPDFActionAnnotation.Create(Self);
  A.Box := Rect(ARect.Left, ARect.Top, ARect.Right, ARect.Bottom);
  A.Action := TPDFGoToPageAction.Create;
  TPDFGoToPageAction(A.Action).PageIndex := PageIndex;
  TPDFGoToPageAction(A.Action).TopOffset := TopOffset;
  A.BorderStyle := '[]';
  Result := A;
end;

function TPDFPage.SetUrl(ARect: TRect; URL: string): TPDFCustomAnnotation;
var
  A: TPDFActionAnnotation;
begin
  if FIsForm then
    raise TPDFException.Create(SCannotCreateURLToWatermark);
  A := TPDFActionAnnotation.Create(Self);
  A.Box := Rect(ARect.Left, ARect.Top, ARect.Right, ARect.Bottom);
  A.Action := TPDFURLAction.Create;
  TPDFURLAction(A.Action).URL := URL;
  A.BorderStyle := '[]';
  Result := A;
end;

procedure TPDFPage.RoundRect(X1, Y1, X2, Y2, X3, Y3: Integer);
const
  b = 0.5522847498;
var
  RX, RY: Extended;
begin
{$IFDEF PASStream}
  FOwner.SavePasStream('RoundRect(' + FormatFloat(x1) + ',' + FormatFloat(y1) + ',' + FormatFloat(x2) + ',' + FormatFloat(y2) + ',' +
    FormatFloat(x3) + ',' + FormatFloat(y3) + ');');
{$ENDIF}
  NormalizeRect(x1, y1, x2, y2);
  Rx := x3 / 2;
  Ry := y3 / 2;
  MoveTo(X1 + RX, Y1);
  LineTo(X2 - RX, Y1);
  Curveto(X2 - RX + b * RX, Y1, X2, Y1 + RY - b * RY, X2, Y1 + ry);
  LineTo(X2, Y2 - RY);
  Curveto(X2, Y2 - RY + b * RY, X2 - RX + b * RX, Y2, X2 - RX, Y2);
  LineTo(X1 + RX, Y2);
  Curveto(X1 + RX - b * RX, Y2, X1, Y2 - RY + b * RY, X1, Y2 - RY);
  LineTo(X1, Y1 + RY);
  Curveto(X1, Y1 + RY - b * RY, X1 + RX - b * RX, Y1, X1 + RX, Y1);
  ClosePath;
end;

procedure TPDFPage.TextBox(Rect: TRect; Text: string; Hor: THorJust; Vert: TVertJust);
var
  x, y: Extended;
begin
  NormalizeRect(Rect);
  Y := Rect.Top;
  x := Rect.Left;
  case Hor of
    hjLeft: x := Rect.Left;
    hjRight: x := Rect.Right - GetTextWidth(Text);
    hjCenter: x := Rect.Left + (Rect.Right - Rect.Left - GetTextWidth(Text)) / 2;
  end;
  case Vert of
    vjUp: y := Rect.Top;
    vjDown: y := Rect.Bottom - FCurrentFontSize;
    vjCenter: y := Rect.Top + (Rect.Bottom - Rect.Top - FCurrentFontSize) / 2;
  end;
{$IFDEF CB}
  TextOutput(x, y, 0, Text);
{$ELSE}
  TextOut(x, y, 0, Text);
{$ENDIF}
end;


procedure TPDFPage.ResetTextCTM;
begin
  FCTM.a := 1;
  FCTM.b := 0;
  FCTM.c := 0;
  FCTM.d := 1;
  FCTM.x := 0;
  FCTM.y := 0;
  FTextAngle := 0;
  FRealAngle := 0;
end;

procedure TPDFPage.SetOrientation(const Value: TPDFPageOrientation);
begin
  if Factions then
    raise TPDFException.Create(SPageInProgress);
  FOrientation := Value;
  if Value = poPagePortrait then
    if FWidth > FHeight then swp(FWidth, FHeight);
  if Value = poPageLandScape then
    if FWidth < FHeight then swp(FWidth, FHeight);
end;

procedure TPDFPage.Concat(A, B, C, D, E, F: Extended);
begin
  RawConcat(A, B, C, D, ExtToIntX(E), -ExtToIntX(F));
end;

function TPDFPage.GetHeight: Integer;
begin
  Result := Round(FHeight * D2P);
end;

function TPDFPage.GetWidth: Integer;
begin
  Result := Round(FWidth * D2P);
end;

procedure TPDFPage.CloseCanvas;
var
  s: string;
  Pars: TEMWParser;
  SZ: TSize;
  Z: Boolean;
begin
  if FMF = nil then Exit;
  FCanvas.Free;
  Z := False;
  Pars := TEMWParser.Create(Self);
  try
    FMF.Enhanced := True;
    Pars.LoadMetaFile(FMF);
    SZ := Pars.GetMax;
    if (SZ.cx <= 0) or (SZ.cy <= 0) then Z := True;
    if not Z then
    begin
      s := FContent.Text;
      FContent.Clear;
      Pars.Execute;
      if FCanvasOver then FContent.Text := FContent.Text + #13 + s
      else FContent.Text := s + #13 + FContent.Text;
    end;
  finally
    Pars.Free;
  end;
  FMF.Free;
  FMF := nil;
  FAskCanvas := False;
end;

procedure TPDFPage.PlayMetaFile(MF: TMetaFile);
begin
  PlayMetaFile(MF, 0, 0, 1, 1);
end;

procedure TPDFPage.PlayMetaFile(MF: TMetafile; x, y, XScale, YScale: Extended);
var
  P: TPDFPage;
  Pars: TEMWParser;
  S: TSize;
  Z: Boolean;
  NMF: TMetafile;
  MFC: TMetafileCanvas;
  AX: Extended;
  XS, YS: Integer;
  DC: HDC;
  W, H: Integer;
begin
  Z := False;
  AX := 1;

  P := FForms.Add;
  if P.FCanvas <> nil then
    P.DeleteCanvas;
  P.FWidth := FWidth;
  P.FHeight := FHeight;
  P.FRes := FRes;
  P.D2P := D2P;
  NMF := TMetafile.Create;
  try
    DC := GetDC(0);
    XS := GetDeviceCaps(DC, HORZRES);
    YS := GetDeviceCaps(DC, VERTRES);
    ReleaseDC(0, DC);
    if (MF.Height > YS) or (MF.Width > XS) then
    begin
      AX := Min(YS / MF.Height, XS / MF.Width);
      NMF.Height := Round(MF.Height * AX);
      NMF.Width := Round(MF.Width * AX);
    end else
    begin
      NMF.Height := MF.Height;
      NMF.Width := MF.Width;
    end;
    W := NMF.Width;
    H := NMF.Height;
    MFC := TMetafileCanvas.Create(NMF, 0);
    try
      if AX = 1 then MFC.Draw(0, 0, MF) else
        MFC.StretchDraw(Rect(0, 0, W - 1, H - 1), MF);
    finally
      MFC.Free;
    end;
    Pars := TEMWParser.Create(P);
    try
      MF.Enhanced := True;
      Pars.LoadMetaFile(NMF);
      S := Pars.GetMax;
      if (S.cx = 0) or (S.cy = 0) then Z := True;
      if not Z then
      begin
        P.Width := abs(S.cx);
        P.Height := abs(S.cy);
        Pars.Execute;
      end else FForms.Delete(P);
    finally
      Pars.Free;
    end;
  finally
    NMF.Free;
  end;
  if not Z then
  begin
    AppendAction('q /IF' + IntToStr(FForms.IndexOf(P)) + ' Do Q');
    P.FMatrix.x := x / D2P;
    P.FMatrix.y := (Height - P.Height * YScale / AX - y) / D2P;
    P.FMatrix.a := XScale / AX;
    P.FMatrix.d := YScale / AX;
  end;
end;

procedure TPDFPage.CreateCanvas;
var
  DC: HDC;
  I: Integer;
begin
  if FMF = nil then
  begin
    FMF := TMetafile.Create;
    FAskCanvas := False;
    DC := GetDC(0);
    I := GetDeviceCaps(dc, LOGPIXELSX);
    ReleaseDC(0, DC);
    FMF.Height := MulDiv(FHeight, I, 72);
    FMF.Width := MulDiv(FWidth, I, 72);
    FCanvas := TMetafileCanvas.Create(FMF, 0);
  end else
    raise TPDFException.Create(SCanvasForThisPageAlreadyCreated);
end;

procedure TPDFPage.SetWaterMark(const Value: Integer);
begin
  if Value < 0 then
  begin
    FWaterMark := Value;
    Exit;
  end;
  if FIsForm then
    raise TPDFException.Create(SWatermarkCannotHaveWatermark);
  if Value >= FOwner.FWaterMarks.Count then
    raise TPDFException.Create(SWaterMarkWithSuchIndexNotCreatedYetForThisDocument);
  FWaterMark := Value;
end;

procedure TPDFPage.DeleteCanvas;
begin
  if FMF <> nil then
  begin
    FCanvas.Free;
    FMF.Free;
    FMF := nil;
    FCanvas := nil;
    FAskCanvas := False;
  end;
end;

function TPDFPage.SetAction(ARect: TRect; Action: TPDFAction): TPDFCustomAnnotation;
var
  A: TPDFActionAnnotation;
begin
  if FIsForm then
    raise TPDFException.Create(SCannotCreateURLToWatermark);
  A := TPDFActionAnnotation.Create(Self);
//  FAnnot.Add(Pointer(A));
  A.Box := Rect(ARect.Left, ARect.Top, ARect.Right, ARect.Bottom);
  A.Action := Action;
  A.BorderStyle := '[]';
  Result := A;
end;

procedure TPDFPage.SetThumbnail(const Value: Integer);
begin
  if Value < 0 then
  begin
    FThumbnail := Value;
    Exit;
  end;
  if FIsForm then
    raise TPDFException.Create(SWatermarkCannotHaveThumbnail);
  if Value >= FOwner.FImages.Count then
    raise TPDFException.Create(SImageWithSuchIndexNotCreatedYetForThisDocument);
  FThumbnail := Value;
end;

procedure TPDFPage.PrepareID;
var
  I: Integer;
begin
  for I := 0 to FLinkedFont.Count - 1 do
    if TPDFFont(FLinkedFont[I]).FontID = -1 then TPDFFont(FLinkedFont[I]).FontID := FOwner.GetNextID;
  for I := 0 to FLinkedImages.Count - 1 do
    if TPDFImage(FLinkedImages[I]).PictureID = -1 then
      TPDFImage(FLinkedImages[I]).Save(FOwner);
  for I := 0 to FAnnot.Count - 1 do
    if TPDFCustomAnnotation(FAnnot[I]).AnnotID = -1 then TPDFCustomAnnotation(FAnnot[I]).AnnotID := FOwner.GetNextID;
  if FThumbnail >= 0 then
    if FOwner.FImages[FThumbnail].PictureID = -1 then FOwner.FImages[FThumbnail].Save(FOwner);
end;

function TPDFPage.CreateControl(CClass: TPDFControlClass; ControlName: string;
  Box: TRect): TPDFControl;
var
  SO: TPDFControl;
begin
  SO := CClass.Create(Self, ControlName);
  SO.Box := Box;
  Result := SO;
end;

procedure TPDFPage.LoadCurrentFontWidth(var Arr: TLargeWidthArray);
var
  I, K: Integer;
begin
  if Owner.FFonts[FCurrentFontIndex].IsCJK then
  begin
    K := round(1000 * FCurrentFontSize * D2P);
    for i := 0 to 255 do
      Arr[I] := K;
    Exit;
  end;
  if Owner.FFonts[FCurrentFontIndex].Standart then
  begin
    K := Owner.FFonts[FCurrentFontIndex].StdID;
    for i := 0 to 255 do
      Arr[I] := round(StWidth[K, I] * FCurrentFontSize * D2P);
  end else
    for i := 0 to 255 do
      Arr[I] := round(Owner.FFonts[FCurrentFontIndex].FWidth[I] * FCurrentFontSize * D2P);
end;

{ TPDFPages }

function TPDFPages.Add: TPDFPage;
begin
  Result := TPDFPage.Create(FOwner);
  FPages.Add(Pointer(Result));
  Result.FIsForm := IsWaterMarks;
end;

procedure TPDFPages.Clear;
var
  I: Integer;
begin
  for I := 0 to FPages.Count - 1 do
    TPDFPage(FPages[I]).Free;
  FPages.Clear;
end;

constructor TPDFPages.Create(AOwner: TPDFDocument; WM: Boolean);
begin
  FOwner := AOwner;
  FPages := TList.Create;
  IsWaterMarks := WM;
end;

procedure TPDFPages.Delete(Page: TPDFPage);
var
  I: Integer;
begin
  I := IndexOf(Page);
  if I <> -1 then
  begin
    Page.Free;
    FPages.Delete(I);
  end;
end;

destructor TPDFPages.Destroy;
begin
  Clear;
  FPages.Free;
end;

function TPDFPages.GetCount: Integer;
begin
  Result := FPages.Count;
end;

function TPDFPages.GetPage(Index: Integer): TPDFPage;
begin
  Result := TPDFPage(FPages[Index]);
end;

function TPDFPages.IndexOf(Page: TPDFPage): Integer;
begin
  Result := FPages.IndexOf(Pointer(Page));
end;

{ TPDFFonts }

function TPDFFonts.Add: TPDFFont;
begin
  Result := TPDFFont.Create;
  FFonts.Add(Pointer(Result));
  Result.FontID := FOwner.GetNextID;
end;

function TPDFFonts.CheckFont(FontName: TFontName; FontStyle: TFontStyles; FontCharset: TFontCharset): string;
var
  I: Integer;
  k: Integer;
  FF: TPDFFont;
  FFN: string;
  ID: Integer;
  FA: string;
  FAD, FD: string;
  RC: TFontCharset;
  RS: TFontStyles;
  LF: TLogFont;
  DC: HDC;
  function A(const Enum: ENUMLOGFONTEX; const PFD: TNEWTEXTMETRICEXA; FT: DWORD; var Test: Integer): Integer; stdcall;
  begin
    Test := 1;
    Result := 0;
  end;
begin
  if FontCharset = default_charset then FontCharset := llPDFLibDefaultCharset;
  if FontCharset in [$80, 129, 136, 134] then
  begin
    case FontCharset of
      $80: FFN := 'JapanFont';
      129: FFN := 'KoreanFont';
      136: FFN := 'ChineseTraditionalFont';
    else FFN := 'ChineseSimplifiedFont';
    end;
    i := IndexOf(FFN, FontStyle, FontCharset);
    if i <> -1 then
    begin
      FLast := FFonts[i];
      FLastIndex := I;
    end else
    begin
      FD := '';
      if fsBold in FontStyle then
        FD := FD + 'B';
      if fsItalic in FontStyle then
        FD := FD + 'I';
      FF := Add;
      FF.Standart := True;
      FF.FontName := FFN;
      FF.RealName := FFN;
      FF.IsCJK := True;
      case FontCharset of
        $80:
          begin
            FF.AliasName := 'CIDJ' + FD;
            FF.CJKType := cjkJapan;
          end;
        129:
          begin
            FF.AliasName := 'CIDK' + FD;
            FF.CJKType := cjkKorean;
          end;
        136:
          begin
            FF.AliasName := 'CIDCT' + FD;
            FF.CJKType := cjkChinaTrad;
          end;
      else
        begin
          FF.AliasName := 'CIDCS' + FD;
          FF.CJKType := cjkChinaSimpl;
        end;
      end;
      FF.RealStyle := FontStyle;
      FF.RealCharset := FontCharset;
      FLast := FF;
      FLastIndex := FFonts.Count - 1;
    end;
    Exit;
  end;
  if UpperCase(FontName) = 'ARIAL' then
    if FontCharset = ANSI_CHARSET then FontName := 'Helvetica' else
    begin
      DC := GetDC(0);
      try
        FillChar(LF, SizeOf(LF), 0);
        LF.lfFaceName := 'ARIAL';
        LF.lfCharSet := FontCharset;
        I := 0;
        EnumFontFamiliesEx(DC, LF, @A, Longint(@I), 0);
        if I = 0 then FontName := 'Helvetica';
      finally
        ReleaseDC(0, DC);
      end;
    end;
  if UpperCase(FontName) = 'COURIER NEW' then
    if FontCharset = ANSI_CHARSET then FontName := 'Courier' else
    begin
      DC := GetDC(0);
      try
        FillChar(LF, SizeOf(LF), 0);
        LF.lfFaceName := 'COURIER NEW';
        LF.lfCharSet := FontCharset;
        I := 0;
        EnumFontFamiliesEx(DC, LF, @A, Longint(@I), 0);
        if I = 0 then FontName := 'Courier';
      finally
        ReleaseDC(0, DC);
      end;
    end;
  if UpperCase(FontName) = 'TIMES NEW ROMAN' then
    if FontCharset = ANSI_CHARSET then FontName := 'Times' else
    begin
      DC := GetDC(0);
      try
        FillChar(LF, SizeOf(LF), 0);
        LF.lfFaceName := 'TIMES NEW ROMAN';
        LF.lfCharSet := FontCharset;
        I := 0;
        EnumFontFamiliesEx(DC, LF, @A, Longint(@I), 0);
        if I = 0 then FontName := 'Times';
      finally
        ReleaseDC(0, DC);
      end;
    end;

  RS := FontStyle;
  RS := RS - [fsUnderline, fsStrikeOut];
  RC := FontCharset;
  ID := -1;
  FA := UpperCase(FontName);
  if FA = 'HELVETICA' then
    if (fsBold in FontStyle) and (fsItalic in FontStyle) then
    begin
      FFN := 'Helvetica-BoldOblique';
      ID := 3;
    end
    else
      if fsBold in FontStyle then
      begin
        FFN := 'Helvetica-Bold';
        ID := 1;
      end
      else
        if fsItalic in FontStyle then
        begin
          FFN := 'Helvetica-Oblique';
          ID := 2;
        end
        else
        begin
          FFN := 'Helvetica';
          ID := 0;
        end;
  if FA = 'TIMES' then
    if (fsBold in FontStyle) and (fsItalic in FontStyle) then
    begin
      FFN := 'Times-BoldItalic';
      ID := 7;
    end
    else
      if fsBold in FontStyle then
      begin
        FFN := 'Times-Bold';
        ID := 5;
      end
      else
        if fsItalic in FontStyle then
        begin
          FFN := 'Times-Italic';
          ID := 6;
        end
        else
        begin
          FFN := 'Times-Roman';
          ID := 4;
        end;
  if FA = 'COURIER' then
    if (fsBold in FontStyle) and (fsItalic in FontStyle) then
    begin
      FFN := 'Courier-BoldOblique';
      ID := 11;
    end
    else
      if fsBold in FontStyle then
      begin
        FFN := 'Courier-Bold';
        ID := 9;
      end
      else
        if fsItalic in FontStyle then
        begin
          FFN := 'Courier-Oblique';
          ID := 10;
        end
        else
        begin
          FFN := 'Courier';
          ID := 8;
        end;
  if FA = 'SYMBOL' then
  begin
    FFN := 'Symbol';
    ID := 12;
  end;
  if FA = 'ZAPFDINGBATS' then
  begin
    FFN := 'ZapfDingbats';
    ID := 13
  end;
  if ID <> -1 then
  begin
    RC := ANSI_CHARSET;
    i := IndexOf(FFN, RS, RC);
    if i <> -1 then
    begin
      FLast := FFonts[i];
      FLastIndex := I;
    end else
    begin
      FF := Add;
      FF.Standart := True;
      FF.FontName := FFN;
      FF.RealName := FFN;
      FF.StdID := ID + 1;
      FF.RealStyle := RS;
      FF.AliasName := 'F' + IntToStr(ID);
      FLast := FF;
      FLastIndex := FFonts.Count - 1;
    end;
    Exit;
  end;
  if not FontTest(FontName, RS, RC, FAD) then
  begin
    Result := CheckFont('Arial', FontStyle, FontCharset);
    Exit;
  end;
  FD := FontName;
  if fsBold in RS then
    FD := FD + ' Bold';
  if fsItalic in RS then
    FD := FD + ' Italic';
  FFN := ReplStr(FD, ' ', '#20');
  I := IndexOf(FontName, RS, RC);
  if I <> -1 then
  begin
    FLast := FFonts[i];
    FLastIndex := I;
  end else
  begin
    FF := Add;
    FF.Standart := False;
    FF.FontName := FFN;
    FF.RealName := FontName;
    FF.RealStyle := RS;
    FF.RealCharset := RC;
    k := 0;
    for i := 0 to Count - 1 do
      if not Items[i].Standart then Inc(k);
    FF.AliasName := 'TT' + IntToStr(k);
    FLast := FF;
    FLastIndex := Count - 1;
    Items[FLastIndex].LoadingWidth;
  end;
end;

procedure TPDFFonts.Clear;
var
  I: Integer;
begin
  for I := 0 to FFonts.Count - 1 do
    TPDFFont(FFonts[I]).Free;
  FFonts.Clear;
end;

constructor TPDFFonts.Create(AOwner: TPDFDocument);
begin
  FFonts := TList.Create;
  FOwner := AOwner;
end;

procedure TPDFFonts.Delete(Index: Integer);
begin
  TPDFFont(FFonts[Index]).Free;
  FFonts.Delete(Index);
end;

destructor TPDFFonts.Destroy;
begin
  Clear;
  FFonts.Free;
  inherited;
end;

procedure TPDFFonts.EmbiddingFontFiles;
var
  I, O, RS: Integer;
  Files: TStringList;
  MS: TMemoryStream;
  CS: TCompressionStream;
  F: Boolean;
  P: Pointer;
  FZ: Cardinal;
  B: TBitmap;
begin
  if FOwner.EmbeddingStyle <> fetFullFont then
  begin
    for I := 0 to Count - 1 do
    begin
      if Items[I].Standart then Continue;
      F := False;
      for O := 0 to FOwner.NonEmbeddedFont.Count - 1 do
        if UpperCase(FOwner.NonEmbeddedFont[O]) = UpperCase(Items[i].RealName) then
        begin
          F := True;
          items[I].FTS := fetNotEmbed;
          Break;
        end;
      if not F then Items[i].FTS := FOwner.EmbeddingStyle;
    end;
    Exit;
  end;
  Files := TStringList.Create;
  try
    for I := 0 to Count - 1 do
    begin
      if Items[I].Standart then Continue;
      F := False;
      for O := 0 to FOwner.NonEmbeddedFont.Count - 1 do
        if UpperCase(FOwner.NonEmbeddedFont[O]) = UpperCase(Items[i].RealName) then
        begin
          F := True;
          Items[I].FontFileID := -1;
          Break;
        end;
      if F then Continue;
      RS := 0;
      O := Files.IndexOf(Items[I].FontName);
      if O = -1 then
      begin
        Items[I].FontFileID := FOwner.GetNextID;
        Files.AddObject(Items[i].FontName, TObject(Items[I].FontFileID));
        MS := TIsMemoryStream.Create;
        try
          B := TBitmap.Create;
          try
            B.Canvas.Font.Name := Items[i].RealName;
            B.Canvas.Font.Style := Items[i].RealStyle;
            FZ := GetFontData(B.Canvas.Handle, 0, 0, nil, 0);
            if FZ = GDI_ERROR then
              raise TPDFException.Create(SCannotReceiveDataForFont + Items[i].FontName);
            GetMem(P, FZ);
            try
              if GetFontData(B.Canvas.Handle, 0, 0, P, FZ) = GDI_ERROR then
                raise TPDFException.Create(SCannotReceiveDataForFont + Items[i].FontName);
              CS := TCompressionStream.Create(clDefault, MS);
              try
                CS.Write(P^, FZ);
              finally
                CS.Free;
              end;
              RS := FZ;
            finally
              FreeMem(P);
            end;
          finally
            B.Free;
          end;
          MS.Position := 0;
          FOwner.StartObj(Items[I].FontFileID);
          FOwner.SaveToStream('/Filter /FlateDecode /Length ' + IntToStr(MS.Size) + ' /Length1 ' + IntToStr(RS));
          FOwner.CloseHObj;
          FOwner.StartStream;
          EnCodeStream(FOwner.FProtectionEnabled, FOwner.FKey, FOwner.FPKL, Items[I].FontFileID, MS);
          FOwner.FStream.CopyFrom(MS, MS.Size);
          FOwner.SaveToStream('');
          FOwner.CloseStream;
          FOwner.CloseObj;
        finally
          MS.Free;
        end;
      end else Items[I].FontFileID := Integer(Files.Objects[O]);
    end;
  finally
    Files.Free;
  end;
end;

function TPDFFonts.GetCount: Integer;
begin
  Result := FFonts.Count;
end;

function TPDFFonts.GetFont(Index: Integer): TPDFFont;
begin
  Result := TPDFFont(FFonts[Index]);
end;


function TPDFFonts.IndexOf(FontName: TFontName; FontStyle: TFontStyles;
  FontCharset: TFontCharset): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to Count - 1 do
    if (UpperCase(Items[i].RealName) = UpperCase(FontName)) and (Items[i].RealStyle = FontStyle) and (Items[i].RealCharset = FontCharset) then
    begin
      Result := i;
      Break;
    end;
end;

{ TPDFImages }

function TPDFImages.Add(Image: TGraphic; Compression: TImageCompressionType; MaskIndex: Integer = -1; IsMask: Boolean = False; TransparentColor: TColor = -1): TPDFImage;
begin
  Result := TPDFImage.Create(Image, Compression, FOwner, MaskIndex, IsMask, TransparentColor);
  FImages.Add(Pointer(Result));
  Result.ImageName := 'Im' + IntToStr(Count - 1);
end;

function TPDFImages.Add(FileName: TFileName; Compression: TImageCompressionType; MaskIndex: Integer = -1; IsMask: Boolean = False; TransparentColor: TColor = -1): TPDFImage;
begin
  Result := TPDFImage.Create(FileName, Compression, FOwner, MaskIndex, IsMask, TransparentColor);
  FImages.Add(Pointer(Result));
  Result.ImageName := 'Im' + IntToStr(Count - 1);
end;

procedure TPDFImages.Clear;
var
  I: Integer;
begin
  for I := 0 to FImages.Count - 1 do
    TPDFImage(FImages[I]).Free;
  FImages.Clear;
end;

constructor TPDFImages.Create(AOwner: TPDFDocument);
begin
  FOwner := AOwner;
  FImages := TList.Create;
end;

procedure TPDFImages.Delete(Index: Integer);
begin
  TPDFImage(FImages[Index]).Free;
  FImages.Delete(Index);
end;

destructor TPDFImages.Destroy;
begin
  Clear;
  FImages.Free;
  inherited;
end;

function TPDFImages.GetCount: Integer;
begin
  Result := FImages.Count;
end;

function TPDFImages.GetImage(Index: Integer): TPDFImage;
begin
  Result := TPDFImage(FImages[Index]);
end;

{ TPDFFont }

constructor TPDFFont.Create;
begin
  WidthLoaded := False;
  IsCJK := False;
  FontID := -1;
  FontFileID := -1;
end;

function TPDFFont.GetWidth(Index: Integer): Word;
begin
  if IsCJK then
  begin
    Result := 1000;
    Exit;
  end;
  if Standart then
  begin
    Result := stWidth[StdID, Index];
    Exit;
  end;
  Result := FWidth[Index];
end;

procedure TPDFFont.LoadingWidth;
type
  A = array[0..255] of ABC;
var
  AR: ^A;
  BM: TBitmap;
  I: Integer;
begin
  New(Ar);
  try
    BM := TBitmap.Create;
    try
      BM.Canvas.Font.Name := RealName;
      BM.Canvas.Font.Style := RealStyle;
      BM.Canvas.Font.Charset := RealCharset;
      BM.Canvas.Font.Size := Round(1000 * 72 / BM.Canvas.Font.PixelsPerInch);
      GetCharABCWidths(BM.Canvas.Handle, 0, 255, ar^);
      for I := 0 to 255 do
        FWidth[I] := ar^[I].abcA + Integer(ar^[I].abcB) + ar^[I].abcC;
    finally
      BM.Free;
    end;
  finally
    Dispose(AR);
  end;
end;

procedure TPDFFont.Save(Doc: TPDFDocument);
var
  BM: TBitmap;
  I, J: Integer;
  S, Wid: string;
  OTM: OUTLINETEXTMETRIC;
  CodePage: Integer;
  st: string;
  ad: array[0..127] of word;
  W: PWideChar;
  k: Integer;
  F: Boolean;
  DDD: Integer;
  LR: Boolean;
  MS, MS1: TMemoryStream;
  CS: TCompressionStream;
  RS: Integer;
begin
  if IsCJK then
  begin
    st := '';
    if fsbold in RealStyle then st := st + 'Bold';
    if fsItalic in RealStyle then st := st + 'Italic';
    if st <> '' then st := ',' + st;
    DDD := Doc.GetNextID;
    FontDescriptorID := Doc.GetNextID;
    Doc.StartObj(FontID);
    case CJKType of
      cjkJapan:
        begin
          Doc.SaveToStream('/Type /Font /Subtype /Type0  /BaseFont /HeiseiMin-W3' + st);
          Doc.SaveToStream('/DescendantFonts  [ ' + IntToStr(DDD) + ' 0 R ] /Encoding /90msp-RKSJ-H');
          Doc.CloseHObj;
          Doc.CloseObj;
          Doc.StartObj(DDD);
          Doc.SaveToStream('/Type /Font /Subtype /CIDFontType2 /BaseFont /HeiseiMin-W3' + st);
          Doc.SaveToStream('/WinCharSet 128 /FontDescriptor ' + IntToStr(FontDescriptorID) + ' 0 R');
          Doc.SaveToStream('/CIDSystemInfo << /Registry (' + EscapeSpecialChar(EnCodeString(Doc.ProtectionEnabled, Doc.FKey, Doc.FPKL, DDD, 'Adobe')) +
            ') /Ordering (' + EscapeSpecialChar(EnCodeString(Doc.ProtectionEnabled, Doc.FKey, Doc.FPKL, DDD, 'Japan1')) + ') /Supplement 2 >>');
          Doc.SaveToStream('/DW 1000 /W [ 1 95 500 231 632 500 ]');
          Doc.CloseHObj;
          Doc.CloseObj;
          Doc.StartObj(FontDescriptorID);
          Doc.SaveToStream('/Type /FontDescriptor');
          Doc.SaveToStream('/Ascent 857');
          Doc.SaveToStream('/CapHeight 718');
          Doc.SaveToStream('/Descent -143');
          Doc.SaveToStream('/Flags 6');
          Doc.SaveToStream('/FontBBox [-123 -257 1001 910]');
          Doc.SaveToStream('/FontName /HeiseiMin-W3' + st);
          Doc.SaveToStream('/ItalicAngle 0');
          Doc.SaveToStream('/MissingWidth 500');
          Doc.SaveToStream('/StemV 93');
          Doc.SaveToStream('/StemH 31');
          Doc.SaveToStream('/XHeight 500');
          Doc.SaveToStream('/Leading 250');
          Doc.SaveToStream('/MaxWidth 1000');
          Doc.SaveToStream('/AvgWidth 702');
          Doc.SaveToStream('/Style << /Panose <010502020400000000000000> >>');
          Doc.CloseHObj;
          Doc.CloseObj;
        end;
      cjkKorean:
        begin
          Doc.SaveToStream('/Type /Font /Subtype /Type0  /BaseFont /HYSMyeongJo-Medium' + st);
          Doc.SaveToStream('/DescendantFonts  [ ' + IntToStr(DDD) + ' 0 R ] /Encoding /KSCms-UHC-H');
          Doc.CloseHObj;
          Doc.CloseObj;
          Doc.StartObj(DDD);
          Doc.SaveToStream('/Type /Font /Subtype /CIDFontType2 /BaseFont /HYSMyeongJo-Medium' + st);
          Doc.SaveToStream('/WinCharSet 128 /FontDescriptor ' + IntToStr(FontDescriptorID) + ' 0 R');
          Doc.SaveToStream('/CIDSystemInfo << /Registry (' + EscapeSpecialChar(EnCodeString(Doc.ProtectionEnabled, Doc.FKey, Doc.FPKL, DDD, 'Adobe')) +
            ') /Ordering (' + EscapeSpecialChar(EnCodeString(Doc.ProtectionEnabled, Doc.FKey, Doc.FPKL, DDD, 'Korea1')) + ') /Supplement 1 >>');
          Doc.SaveToStream('/DW 1000 /W [ 1 95 500 8094 8190 500 ]');
          Doc.CloseHObj;
          Doc.CloseObj;
          Doc.StartObj(FontDescriptorID);
          Doc.SaveToStream('/Type /FontDescriptor');
          Doc.SaveToStream('/Ascent 880');
          Doc.SaveToStream('/CapHeight 800');
          Doc.SaveToStream('/Descent -120');
          Doc.SaveToStream('/Flags 6');
          Doc.SaveToStream('/FontBBox [-0 -148 1001 880]');
          Doc.SaveToStream('/FontName /HYSMyeongJo-Medium' + st);
          Doc.SaveToStream('/ItalicAngle 0');
          Doc.SaveToStream('/MissingWidth 500');
          Doc.SaveToStream('/StemV 93');
          Doc.SaveToStream('/StemH 31');
          Doc.SaveToStream('/XHeight 616');
          Doc.SaveToStream('/Leading 250');
          Doc.SaveToStream('/MaxWidth 1000');
          Doc.SaveToStream('/AvgWidth 1000');
          Doc.SaveToStream('/Style << /Panose <010502020400000000000000> >>');
          Doc.CloseHObj;
          Doc.CloseObj;
        end;
      cjkChinaTrad:
        begin
          Doc.SaveToStream('/Type /Font /Subtype /Type0  /BaseFont /MSung-Light' + st);
          Doc.SaveToStream('/DescendantFonts  [ ' + IntToStr(DDD) + ' 0 R ] /Encoding /ETenms-B5-H');
          Doc.CloseHObj;
          Doc.CloseObj;
          Doc.StartObj(DDD);
          Doc.SaveToStream('/Type /Font /Subtype /CIDFontType2 /BaseFont /MSung-Light' + st);
          Doc.SaveToStream('/WinCharSet 128 /FontDescriptor ' + IntToStr(FontDescriptorID) + ' 0 R');
          Doc.SaveToStream('/CIDSystemInfo << /Registry (' + EscapeSpecialChar(EnCodeString(Doc.ProtectionEnabled, Doc.FKey, Doc.FPKL, DDD, 'Adobe')) +
            ') /Ordering (' + EscapeSpecialChar(EnCodeString(Doc.ProtectionEnabled, Doc.FKey, Doc.FPKL, DDD, 'CNS1')) + ') /Supplement 0 >>');
          Doc.SaveToStream('/DW 1000 /W [ 1 95 500 13648 13742 500 ]');
          Doc.CloseHObj;
          Doc.CloseObj;
          Doc.StartObj(FontDescriptorID);
          Doc.SaveToStream('/Type /FontDescriptor');
          Doc.SaveToStream('/Ascent 880');
          Doc.SaveToStream('/CapHeight 880');
          Doc.SaveToStream('/Descent -120');
          Doc.SaveToStream('/Flags 6');
          Doc.SaveToStream('/FontBBox [-160 -249 1015 888]');
          Doc.SaveToStream('/FontName /MSung-Light' + st);
          Doc.SaveToStream('/ItalicAngle 0');
          Doc.SaveToStream('/MissingWidth 500');
          Doc.SaveToStream('/StemV 93');
          Doc.SaveToStream('/StemH 31');
          Doc.SaveToStream('/XHeight 616');
          Doc.SaveToStream('/Leading 250');
          Doc.SaveToStream('/MaxWidth 1000');
          Doc.SaveToStream('/AvgWidth 1000');
          Doc.SaveToStream('/Style << /Panose <010502020400000000000000> >>');
          Doc.CloseHObj;
          Doc.CloseObj;
        end;
    else
      begin
        Doc.SaveToStream('/Type /Font /Subtype /Type0  /BaseFont /STSong-Light' + st);
        Doc.SaveToStream('/DescendantFonts  [ ' + IntToStr(DDD) + ' 0 R ] /Encoding /GB-EUC-H');
        Doc.CloseHObj;
        Doc.CloseObj;
        Doc.StartObj(DDD);
        Doc.SaveToStream('/Type /Font /Subtype /CIDFontType2 /BaseFont /STSong-Light' + st);
        Doc.SaveToStream('/WinCharSet 128 /FontDescriptor ' + IntToStr(FontDescriptorID) + ' 0 R');
        Doc.SaveToStream('/CIDSystemInfo << /Registry (' + EscapeSpecialChar(EnCodeString(Doc.ProtectionEnabled, Doc.FKey, Doc.FPKL, DDD, 'Adobe')) +
          ') /Ordering (' + EscapeSpecialChar(EnCodeString(Doc.ProtectionEnabled, Doc.FKey, Doc.FPKL, DDD, 'GB1')) + ') /Supplement 2 >>');
        Doc.SaveToStream('/DW 1000 /W [ 1 95 500 814 939 500 7712 [ 500 ] 7716 [ 500 ] ]');
        Doc.CloseHObj;
        Doc.CloseObj;
        Doc.StartObj(FontDescriptorID);
        Doc.SaveToStream('/Type /FontDescriptor');
        Doc.SaveToStream('/Ascent 880');
        Doc.SaveToStream('/CapHeight 880');
        Doc.SaveToStream('/Descent -120');
        Doc.SaveToStream('/Flags 6');
        Doc.SaveToStream('/FontBBox [-25 -254 1000 880]');
        Doc.SaveToStream('/FontName /STSong-Light' + st);
        Doc.SaveToStream('/ItalicAngle 0');
        Doc.SaveToStream('/MissingWidth 500');
        Doc.SaveToStream('/StemV 93');
        Doc.SaveToStream('/StemH 31');
        Doc.SaveToStream('/XHeight 616');
        Doc.SaveToStream('/Leading 250');
        Doc.SaveToStream('/MaxWidth 1000');
        Doc.SaveToStream('/AvgWidth 1000');
        Doc.SaveToStream('/Style << /Panose <010502020400000000000000> >>');
        Doc.CloseHObj;
        Doc.CloseObj;
      end;
    end;
    Exit;
  end;
  if not Standart then
  begin
    if not WidthLoaded then LoadingWidth;
    BM := TBitmap.Create;
    try
      BM.Canvas.Font.Name := RealName;
      BM.Canvas.Font.Style := RealStyle;
      BM.Canvas.Font.Charset := RealCharset;
      BM.Canvas.Font.Size := Round(1000 * 72 / BM.Canvas.Font.PixelsPerInch);
      FillChar(OTM, SizeOf(OTM), 0);
      OTM.otmSize := SizeOf(OTM);
      GetOutlineTextMetrics(BM.Canvas.Handle, SizeOf(OTM), @OTM);
      Wid := '';
      for I := 0 to 15 do
      begin
        S := '';
        for J := 0 to 15 do
        begin
          if FTS = fetUsedGlyphsOnly then
          begin
            if FUsed[I * 16 + J] then S := S + IntToStr(FWidth[I * 16 + J]) + ' '
            else S := S + '0 ';
          end else S := S + IntToStr(FWidth[I * 16 + J]) + ' ';
        end;
        Wid := Wid + #13#10 + S;
      end;
    finally
      BM.Free;
    end;
    S := '';
    case RealCharset of
      BALTIC_CHARSET: S := '-Baltic';
      EASTEUROPE_CHARSET: S := '-EastEurope';
      GREEK_CHARSET: S := '-Greek';
      MAC_CHARSET: S := '-MAC';
      OEM_CHARSET: S := '-OEM';
      RUSSIAN_CHARSET: S := '-Russian';
      SYMBOL_CHARSET: S := '-Symbol';
      TURKISH_CHARSET: S := '-Turkish';
    end;
    if FTS in [fetFullCharset, fetUsedGlyphsOnly] then
    begin
      MS := TIsMemoryStream.Create;
      try
        GetFontSubset(FTS, Self, MS);
        RS := MS.Size;
        MS.Position := 0;
        MS1 := TIsMemoryStream.Create;
        try
          CS := TCompressionStream.Create(clDefault, MS1);
          try
            CS.CopyFrom(MS, MS.Size);
          finally
            CS.Free;
          end;
          MS.Clear;
          Doc.StartObj(FontFileID);
          Doc.SaveToStream('/Filter /FlateDecode /Length ' + IntToStr(MS1.Size) + ' /Length1 ' + IntToStr(RS));
          Doc.CloseHObj;
          Doc.StartStream;
          ms1.Position := 0;
          EnCodeStream(Doc.FProtectionEnabled, Doc.FKey, Doc.FPKL, FontFileID, MS1);
          Doc.FStream.CopyFrom(MS1, MS1.Size);
          Doc.SaveToStream('');
          Doc.CloseStream;
          Doc.CloseObj;
        finally
          MS1.Free;
        end;
      finally
        MS.Free;
      end;
    end;
    Doc.StartObj(FontDescriptorID);
    Doc.SaveToStream('/Type /FontDescriptor');
    Doc.SaveToStream('/Ascent ' + IntToStr(OTM.otmAscent));
    Doc.SaveToStream('/CapHeight 666');
    Doc.SaveToStream('/Descent ' + IntToStr(OTM.otmDescent));
    Doc.SaveToStream('/Flags 32');
    Doc.SaveToStream('/FontBBox [' + IntToStr(OTM.otmrcFontBox.Left) + ' ' + IntToStr(OTM.otmrcFontBox.Bottom) + ' ' + IntToStr(OTM.otmrcFontBox.Right) + ' ' + IntToStr(OTM.otmrcFontBox.Top) + ']');
    Doc.SaveToStream('/FontName /' + FontName + S);
    Doc.SaveToStream('/ItalicAngle ' + IntToStr(OTM.otmItalicAngle));
    Doc.SaveToStream('/StemV 87');
    if FontFileID <> -1 then
      Doc.SaveToStream('/FontFile2 ' + IntToStr(FontFileID) + ' 0 R');
    Doc.CloseHObj;
    Doc.CloseObj;
    Doc.StartObj(FontID);
    Doc.SaveToStream('/Type /Font');
    Doc.SaveToStream('/Subtype /TrueType');
    Doc.SaveToStream('/BaseFont /' + FontName);
    Doc.SaveToStream('/FirstChar 0');
    Doc.SaveToStream('/LastChar 255');
    if RealCharset = ANSI_CHARSET then Doc.SaveToStream('/Encoding /WinAnsiEncoding') else
      if RealCharset = Symbol_charset then
       if UpperCase(FontName) = 'WINGDINGS' then Doc.SaveToStream('/Encoding /MacExpertEncoding') 
        else Doc.SaveToStream('/Encoding /WinAnsiEncoding') else
        if RealCharset = MAC_CHARSET then Doc.SaveToStream('/Encoding /MacRomanEncoding') else
        begin
          Doc.SaveToStream('/Encoding <</Type/Encoding');
          case RealCharset of
            BALTIC_CHARSET: CodePage := 1257;
            CHINESEBIG5_CHARSET: CodePage := 950;
            EASTEUROPE_CHARSET: CodePage := 1250;
            GB2312_CHARSET: CodePage := 936;
            GREEK_CHARSET: CodePage := 1253;
            OEM_CHARSET: CodePage := CP_OEMCP;
            RUSSIAN_CHARSET: CodePage := 1251;
            SHIFTJIS_CHARSET: CodePage := 932;
            TURKISH_CHARSET: CodePage := 1254;
            HEBREW_CHARSET: CodePage := 1255;
            ARABIC_CHARSET: CodePage := 1256;
            THAI_CHARSET: CodePage := 874;
            VIETNAMESE_CHARSET: CodePage := 1258;
          else CodePage := 1252;
          end;
          st := '';
          for i := 129 to 255 do st := st + Chr(i);
          W := @ad;
          i := MultiByteToWideChar(CodePage, 0, PChar(st), 128, w, 128);
          if i <> 0 then
          begin
            Doc.SaveToStream('/Differences [129 ');
            for i := 0 to 127 do
            begin
              F := False;
              for k := 0 to 1050 do
                if ad[I] = AllGliph[k].UnicodeID then
                begin
                  LR := (I mod 16) = 0;
                  Doc.SaveToStream('/' + Allgliph[k].Name, LR);
                  F := True;
                  Break;
                end;
              if not F then
                if ad[I] > 256 then
                  Doc.SaveToStream('/uni' + WordToHex(ad[I]), False) else Doc.SaveToStream('/space', false);
            end;
            Doc.SaveToStream(']');
          end
          else
          begin
            Doc.SaveToStream('/BaseEncoding /WinAnsiEncoding');
          end;
          Doc.CloseHObj;
        end;
    Doc.SaveToStream('/FontDescriptor ' + IntToStr(FontDescriptorID) + ' 0 R');
    Doc.SaveToStream('/Widths [' + Wid + ']');
    Doc.CloseHObj;
    Doc.CloseObj;
  end else
  begin
    Doc.StartObj(FontID);
    Doc.SaveToStream('/Type /Font');
    Doc.SaveToStream('/Subtype /Type1');
    Doc.SaveToStream('/BaseFont /' + FontName);
    if (UpperCase(FontName) <> 'SYMBOL') and (UpperCase(FontName) <> 'ZAPFDINGBATS') then
      Doc.SaveToStream('/Encoding /WinAnsiEncoding');
    Doc.SaveToStream('/FirstChar 32');
    Doc.SaveToStream('/LastChar 255');
    Doc.CloseHObj;
    Doc.CloseObj;
  end;
end;

{ TPDFImage }

constructor TPDFImage.Create(Image: TGraphic;
  Compression: TImageCompressionType; Doc: TPDFDocument;
  MaskIndex: Integer = -1; IsMask: Boolean = False; TransparentColor: TColor = -1);
var
  J: TJPEGImage;
  B: TBitmap;
  CS: TCompressionStream;
  pb: PByteArray;
  bb: Byte;
  x, y: Integer;
begin
  PictureID := -1;
  if MaskIndex >= 0 then
  begin
    if IsMask then
      raise TPDFException.Create('SMaskImageCannotHaveMask');
    if (MaskIndex >= Doc.FImages.GetCount) then
      raise TPDFException.Create('SUnknowMaskImageOutOfBound');
    if not TPDFImage(Doc.FImages.FImages[MaskIndex]).FIsMask then
      raise TPDFException.Create('SMaskImageNotMarkedAsMaskImage');
  end;
  if IsMask and (not (Image is TBitmap)) then
    raise TPDFException.Create('SCreateMaskAvailableOnlyForBitmapImages');
  FMaskIndex := MaskIndex;
  FIsMask := IsMask;
  Saved := False;
  GrayScale := False;
  FCT := Compression;
  FDoc := Doc;
  FWidth := Image.Width;
  FHeight := Image.Height;
  if not ((Image is TJPEGImage) or (Image is TBitmap)) then
    raise TPDFException.Create('SNotValidImage');
  Data := TIsMemoryStream.Create;
  if not IsMask then
  begin
    if FCT = ITCjpeg then
    begin
      J := TJPEGImage.Create;
      try
        J.Assign(Image);
        J.ProgressiveEncoding := False;
        if Doc <> nil then
          J.CompressionQuality := Doc.FJPEGQuality else J.CompressionQuality := 80;
        J.SaveToStream(Data);
        Data.Position := 0;
        if J.Grayscale then
          Grayscale := True;
        FBitPerPixel := 8;
      finally
        J.Free;
      end;
    end;
    if FCT = itcFlate then
    begin
      B := TBitmap.Create;
      try
        B.Assign(Image);
        b.PixelFormat := pf24bit;
        CS := TCompressionStream.Create(clDefault, Data);
        try
          for y := 0 to B.Height - 1 do
          begin
            pb := B.ScanLine[y];
            x := 0;
            while x <= (b.Width - 1) * 3 do
            begin
              bb := pb[x];
              pb[x] := pb[x + 2];
              pb[x + 2] := bb;
              x := x + 3;
            end;
            CS.Write(pb^, b.Width * 3);
          end;
        finally
          CS.Free;
        end;
        FBitPerPixel := 8;
        Data.Position := 0;
      finally
        B.Free;
      end;
    end;
    if FCT in [itcCCITT3, itcCCITT32d, itcCCITT4] then
    begin
      if not (Image is TBitmap) then
      begin
        Data.Free;
        raise TPDFException.Create('CCITT compression work only for bitmap');
      end;
      if TBitmap(Image).PixelFormat <> pf1bit then
      begin
        Data.Free;
        raise Exception.Create('Not b/w image.');
      end;
      FBitPerPixel := 1;
      case FCT of
        itcCCITT3: SaveBMPtoCCITT(TBitmap(Image), Data, CCITT31D);
        itcCCITT32d: SaveBMPtoCCITT(TBitmap(Image), Data, CCITT32D);
        itcCCITT4: SaveBMPtoCCITT(TBitmap(Image), Data, CCITT42D);
      end;
      Data.Position := 0;
    end;
  end
  else
  begin
    B := TBitmap.Create;
    try
      B.Assign(Image);
      B.PixelFormat := pf24bit;
      if TransparentColor = -1 then TransparentColor := B.TransparentColor;
      B.Mask(TransparentColor);
      GrayScale := True;
      FCT := itcCCITT4;
      FBitPerPixel := 1;
      B.Monochrome := True;
      B.PixelFormat:=pf1bit;
      SaveBMPtoCCITT(B, Data, CCITT42D);
      Data.Position := 0;
    finally
      B.Free;
    end;
  end;
end;

constructor TPDFImage.Create(FileName: TFileName;
  Compression: TImageCompressionType; Doc: TPDFDocument; MaskIndex: Integer = -1;
  IsMask: Boolean = False; TransparentColor: TColor = -1);
var
  Gr: TGraphic;
  MS: TMemoryStream;
  BMSig: Word;
begin
  MS := TIsMemoryStream.Create;
  try
    MS.LoadFromFile(FileName);
    MS.Position := 0;
    MS.Read(BMSig, 2);
    MS.Position := 0;
    if BMSig = 19778 then GR := TBitmap.Create else GR := TJPEGImage.Create;
    try
      Gr.LoadFromStream(MS);
      Create(Gr, Compression, Doc, MaskIndex, IsMask, TransparentColor);
    finally
      Gr.Free;
    end;
  finally
    MS.Free;
  end;
end;

destructor TPDFImage.Destroy;
begin
  Data.Free;
  inherited;
end;

procedure TPDFImage.Save(Doc: TPDFDocument);
begin
  if Saved then Exit;
  if FMaskIndex <> -1 then TPDFImage(FDoc.FImages.FImages[FMaskIndex]).Save(Doc);
  Doc.StartObj(PictureID);
  Doc.SaveToStream('/Type /XObject');
  Doc.SaveToStream('/Subtype /Image');
  if (FBitPerPixel <> 1) and (not GrayScale) then Doc.SaveToStream('/ColorSpace /DeviceRGB')
  else Doc.SaveToStream('/ColorSpace /DeviceGray');
  Doc.SaveToStream('/BitsPerComponent ' + IntToStr(BitPerPixel));
  Doc.SaveToStream('/Width ' + IntToStr(FWidth));
  Doc.SaveToStream('/Height ' + IntToStr(FHeight));
  if FIsMask then Doc.SaveToStream('/ImageMask true');
  if FMaskIndex <> -1 then Doc.SaveToStream('/Mask ' + IntToStr(TPDFImage(FDoc.FImages.FImages[FMaskIndex]).PictureID) + ' 0 R');
  case FCT of
    itcJpeg: Doc.SaveToStream('/Filter /DCTDecode');
    itcFlate: Doc.SaveToStream('/Filter /FlateDecode');
  else Doc.SaveToStream('/Filter [/CCITTFaxDecode]');
  end;
  Doc.SaveToStream('/Length ' + IntToStr(Data.Size));
  case FCT of
    itcCCITT3: Doc.SaveToStream('/DecodeParms [<</K 0 /Columns ' + Inttostr(FWidth) + ' /Rows ' + IntToStr(FHeight) + '>>]');
    itcCCITT32d: Doc.SaveToStream('/DecodeParms [<</K 1 /Columns ' + Inttostr(FWidth) + ' /Rows ' + IntToStr(FHeight) + '>>]');
    itcCCITT4: Doc.SaveToStream('/DecodeParms [<</K -1 /Columns ' + Inttostr(FWidth) + ' /Rows ' + IntToStr(FHeight) + '>>]');
  end;
  Doc.CloseHObj;
  Doc.StartStream;
  Data.Position := 0;
  EnCodeStream(Doc.FProtectionEnabled, Doc.FKey, Doc.FPKL, PictureID, Data);
  Doc.FStream.CopyFrom(Data, Data.Size);
  Data.Clear;
  Doc.CloseStream;
  Doc.CloseObj;
  Saved := True;
end;

{ TPDFOutlines }

function TPDFOutlines.Add(Node: TPDFOutlineNode): TPDFOutlineNode;
var
  N, T, M: TPDFOutlineNode;
  I: Integer;
begin
  N := TPDFOutlineNode.Create(Self);
  if Node <> nil then T := Node.FParent else T := nil;
  N.FParent := T;
  N.FNext := nil;
  M := nil;
  for I := 0 to FList.Count - 1 do
    if (TPDFOutlineNode(FList[I]).FParent = T) and (TPDFOutlineNode(FList[I]).FNext = nil) then
    begin
      M := TPDFOutlineNode(FList[I]);
      Break;
    end;
  if M <> nil then M.FNext := N;
  N.FPrev := M;
  FList.Add(Pointer(N));
  if T <> nil then T.FChild.Add(Pointer(N));
  Result := N;
end;

function TPDFOutlines.AddChild(Node: TPDFOutlineNode): TPDFOutlineNode;
var
  N, T, M: TPDFOutlineNode;
  I: Integer;
begin
  N := TPDFOutlineNode.Create(Self);
  T := Node;
  N.FParent := T;
  N.FNext := nil;
  M := nil;
  for I := 0 to FList.Count - 1 do
    if (TPDFOutlineNode(FList[I]).FParent = T) and (TPDFOutlineNode(FList[I]).FNext = nil) then
    begin
      M := TPDFOutlineNode(FList[I]);
      Break;
    end;
  if M <> nil then M.FNext := N;
  N.FPrev := M;
  FList.Add(Pointer(N));
  if T <> nil then T.FChild.Add(Pointer(N));
  Result := N;
end;

function TPDFOutlines.AddChildFirst(
  Node: TPDFOutlineNode): TPDFOutlineNode;
var
  N, T, M: TPDFOutlineNode;
  I: Integer;
begin
  N := TPDFOutlineNode.Create(Self);
  T := Node;
  N.FParent := T;
  N.FPrev := nil;
  M := nil;
  for I := 0 to FList.Count - 1 do
    if (TPDFOutlineNode(FList[I]).FParent = T) and (TPDFOutlineNode(FList[I]).FPrev = nil) then
    begin
      M := TPDFOutlineNode(FList[I]);
      Break;
    end;
  if M <> nil then M.FPrev := N;
  N.FNext := M;
  FList.Add(Pointer(N));
  if T <> nil then T.FChild.Add(Pointer(N));
  Result := N;
end;

function TPDFOutlines.AddFirst(Node: TPDFOutlineNode): TPDFOutlineNode;
var
  N, T, M: TPDFOutlineNode;
  I: Integer;
begin
  N := TPDFOutlineNode.Create(Self);
  if Node <> nil then T := Node.FParent else T := nil;
  N.FParent := T;
  N.FPrev := nil;
  M := nil;
  for I := 0 to FList.Count - 1 do
    if (TPDFOutlineNode(FList[I]).FParent = T) and (TPDFOutlineNode(FList[I]).FPrev = nil) then
    begin
      M := TPDFOutlineNode(FList[I]);
      Break;
    end;
  if M <> nil then M.FPrev := N;
  N.FNext := M;
  FList.Add(Pointer(N));
  if T <> nil then T.FChild.Add(Pointer(N));
  Result := N;
end;


procedure TPDFOutlines.Clear;
begin
  while FList.Count <> 0 do
    TPDFOutlineNode(FList[0]).Delete;
end;

constructor TPDFOutlines.Create(AOwner: TPDFDocument);
begin
  FList := TList.Create;
  FOwner := AOwner;
end;

procedure TPDFOutlines.Delete(Node: TPDFOutlineNode);
begin
  Node.Delete;
end;

destructor TPDFOutlines.Destroy;
begin
  Clear;
  FList.Free;
  inherited;
end;

function TPDFOutlines.GetCount: Integer;
begin
  Result := FList.Count;
end;

function TPDFOutlines.GetFirstNode: TPDFOutlineNode;
begin
  if FList.Count <> 0 then Result := TPDFOutlineNode(FList[0]) else Result := nil;
end;

function TPDFOutlines.GetItem(Index: Integer): TPDFOutlineNode;
begin
  Result := TPDFOutlineNode(FList[Index]);
end;

function TPDFOutlines.Insert(Node: TPDFOutlineNode): TPDFOutlineNode;
var
  N, Ne: TPDFOutlineNode;
begin
  if Node = nil then
  begin
    Result := Add(nil);
    Exit;
  end;
  N := TPDFOutlineNode.Create(Self);
  Ne := Node.FNext;
  N.FParent := Node.FParent;
  N.FPrev := Node;
  N.FNext := Node.FNext;
  Node.FNext := N;
  if Ne <> nil then Ne.FPrev := N;
  FList.Add(Pointer(N));
  if N.FParent <> nil then N.FParent.FChild.Add(Pointer(N));
  Result := N;
end;

function TPDFOutlines.Add(Node: TPDFOutlineNode; Title: string; Action: TPDFAction; Charset: TFontCharset = ANSI_CHARSET): TPDFOutlineNode;
begin
  Result := Add(Node);
  Result.Title := Title;
  Result.Action := Action;
  Result.Charset := Charset;
end;

function TPDFOutlines.AddChild(Node: TPDFOutlineNode; Title: string;
  Action: TPDFAction; Charset: TFontCharset): TPDFOutlineNode;
begin
  Result := AddChild(Node);
  Result.Title := Title;
  Result.Action := Action;
  Result.Charset := Charset;
end;

function TPDFOutlines.AddChildFirst(Node: TPDFOutlineNode; Title: string;
  Action: TPDFAction; Charset: TFontCharset): TPDFOutlineNode;
begin
  Result := AddChildFirst(Node);
  Result.Title := Title;
  Result.Action := Action;
  Result.Charset := Charset;
end;

function TPDFOutlines.AddFirst(Node: TPDFOutlineNode; Title: string;
  Action: TPDFAction; Charset: TFontCharset): TPDFOutlineNode;
begin
  Result := Add(Node);
  Result.Title := Title;
  Result.Action := Action;
  Result.Charset := Charset;
end;

function TPDFOutlines.Insert(Node: TPDFOutlineNode; Title: string;
  Action: TPDFAction; Charset: TFontCharset): TPDFOutlineNode;
begin
  Result := Insert(Node);
  Result.Title := Title;
  Result.Action := Action;
  Result.Charset := Charset;
end;

{ TPDFOutlineNode }

constructor TPDFOutlineNode.Create(AOwner: TPDFOutlines);
begin
  if AOwner = nil then
    raise TPDFException.Create(SOutlineNodeMustHaveOwner);
  FOwner := AOwner;
  FChild := TList.Create;
end;

procedure TPDFOutlineNode.Delete;
var
  I: Integer;
  P, N: TPDFOutlineNode;
begin
  DeleteChildren;
  P := GetPrev;
  N := GetNext;
  if P <> nil then
    P.FNext := N;
  if N <> nil then
    N.FPrev := P;
  I := FOwner.FList.IndexOf(Pointer(Self));
  if I <> -1 then FOwner.FList.Delete(I);
  if FParent <> nil then
  begin
    I := FParent.FChild.IndexOf(Pointer(Self));
    if I <> -1 then FParent.FChild.Delete(I);
  end;
  Free;
end;

procedure TPDFOutlineNode.DeleteChildren;
begin
  while FChild.Count <> 0 do
    TPDFOutlineNode(FChild[0]).Delete;
end;

destructor TPDFOutlineNode.Destroy;
begin
  FChild.Free;
  inherited;
end;


function TPDFOutlineNode.GetCount: Integer;
begin
  Result := FChild.Count;
end;


function TPDFOutlineNode.GetFirstChild: TPDFOutlineNode;
var
  I: Integer;
begin
  Result := nil;
  if Count = 0 then
    Exit;
  for I := 0 to FChild.Count - 1 do
    if TPDFOutlineNode(FChild[I]).FPrev = nil then
    begin
      Result := TPDFOutlineNode(FChild[I]);
      Exit;
    end;
end;

function TPDFOutlineNode.GetHasChildren: Boolean;
begin
  Result := Count <> 0;
end;

function TPDFOutlineNode.GetItem(Index: Integer): TPDFOutlineNode;
begin
  Result := TPDFOutlineNode(FChild[Index]);
end;

function TPDFOutlineNode.GetLastChild: TPDFOutlineNode;
var
  I: Integer;
begin
  Result := nil;
  if Count = 0 then
    Exit;
  for I := 0 to FChild.Count - 1 do
    if TPDFOutlineNode(FChild[I]).FNext = nil then
    begin
      Result := TPDFOutlineNode(FChild[I]);
      Exit;
    end;
end;

function TPDFOutlineNode.GetNext: TPDFOutlineNode;
var
  I: Integer;
begin
  I := FOwner.FList.IndexOf(Self);
  if I <> FOwner.FList.Count - 1 then Result := FOwner[i + 1] else Result := nil;
end;

function TPDFOutlineNode.GetNextChild(
  Node: TPDFOutlineNode): TPDFOutlineNode;
var
  i: Integer;
begin
  i := FChild.IndexOf(Pointer(Node));
  if (i = -1) or (i = FChild.Count - 1) then Result := nil else Result := TPDFOutlineNode(FChild[i + 1]);
end;

function TPDFOutlineNode.GetNextSibling: TPDFOutlineNode;
begin
  Result := FNext;
end;

function TPDFOutlineNode.GetPrev: TPDFOutlineNode;
var
  I: Integer;
begin
  I := FOwner.FList.IndexOf(Self);
  if I <> 0 then Result := FOwner[i - 1] else Result := nil;
end;

function TPDFOutlineNode.GetPrevChild(
  Node: TPDFOutlineNode): TPDFOutlineNode;
var
  i: Integer;
begin
  i := FChild.IndexOf(Pointer(Node));
  if (i = -1) or (i = 0) then Result := nil else Result := TPDFOutlineNode(FChild[i - 1]);
end;

function TPDFOutlineNode.GetPrevSibling: TPDFOutlineNode;
begin
  Result := FPrev;
end;

procedure TPDFOutlineNode.SetTitle(const Value: string);
begin
  FTitle := Value;
end;

procedure TPDFOutlineNode.Save;
var
  I: Integer;
begin
  FOwner.FOwner.StartObj(OutlineNodeID);
  if FCharset = ANSI_charset then
    FOwner.FOwner.SaveToStream('/Title (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, OutlineNodeID, Title)) + ')')
  else
    FOwner.FOwner.SaveToStream('/Title <' + EnCodeHexString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, OutlineNodeID, UnicodeChar(FTitle, FCharset)) + '>');
  if FOwner.FOwner.Version = v14 then
  begin
    if Color <> 0 then
      FOwner.FOwner.SaveToStream('/C [' + FormatFloat(GetRValue(Color) / 255) + ' ' +
        FormatFloat(GetGValue(Color) / 255) + ' ' + FormatFloat(GetBValue(Color) / 255) + ' ]');
    I := 0;
    if fsbold in Style then I := I or 2;
    if fsItalic in Style then I := I or 1;
    if I <> 0 then
      FOwner.FOwner.SaveToStream('/F ' + IntToStr(I));
  end;
  if FChild.Count <> 0 then
  begin
    if FExpanded then
      FOwner.FOwner.SaveToStream('/Count ' + IntToStr(FChild.Count))
    else FOwner.FOwner.SaveToStream('/Count -' + IntToStr(FChild.Count));
    FOwner.FOwner.SaveToStream('/First ' + IntToStr(GetFirstChild.OutlineNodeID) + ' 0 R');
    FOwner.FOwner.SaveToStream('/Last ' + IntToStr(GetLastChild.OutlineNodeID) + ' 0 R');
  end;
  if FParent = nil then FOwner.FOwner.SaveToStream('/Parent ' + IntToStr(FOwner.OutlinesID) + ' 0 R')
  else FOwner.FOwner.SaveToStream('/Parent ' + IntToStr(FParent.OutlineNodeID) + ' 0 R');
  if FNext <> nil then
    FOwner.FOwner.SaveToStream('/Next ' + IntToStr(FNext.OutlineNodeID) + ' 0 R');
  if FPrev <> nil then
    FOwner.FOwner.SaveToStream('/Prev ' + IntToStr(FPrev.OutlineNodeID) + ' 0 R');
  if FAction <> nil then
    FOwner.FOwner.SaveToStream('/A ' + IntToStr(FAction.ActionID) + ' 0 R');
  FOwner.FOwner.CloseHObj;
  FOwner.FOwner.CloseObj;
end;

procedure TPDFOutlineNode.SetExpanded(const Value: Boolean);
begin
  FExpanded := Value;
end;

procedure TPDFOutlineNode.SetCharset(const Value: TFontCharset);
begin
  FCharset := Value;
end;

procedure TPDFOutlineNode.SetAction(const Value: TPDFAction);
begin
  if Value <> nil then
  begin
    FOwner.FOwner.AppendAction(Value);
    FAction := Value;
  end;
end;

{ TPDFDocInfo }

procedure TPDFDocInfo.SetAuthor(const Value: string);
begin
  FAuthor := Value;
end;

procedure TPDFDocInfo.SetCreationDate(const Value: TDateTime);
begin
  FCreationDate := Value;
end;

procedure TPDFDocInfo.SetCreator(const Value: string);
begin
  FCreator := Value;
end;

procedure TPDFDocInfo.SetKeywords(const Value: string);
begin
  FKeywords := Value;
end;

procedure TPDFDocInfo.SetSubject(const Value: string);
begin
  FSubject := Value;
end;

procedure TPDFDocInfo.SetTitle(const Value: string);
begin
  FTitle := Value;
end;

{ TPDFCustomAnnotation }

function TPDFCustomAnnotation.CalcFlags: Integer;
begin
  Result := 0;
  if afInvisible in FFlags then Result := Result or 1;
  if afHidden in FFlags then Result := Result or 2;
  if afPrint in FFlags then Result := Result or 4;
  if afNoZoom in FFlags then Result := Result or 8;
  if afNoRotate in FFlags then Result := Result or 16;
  if afNoView in FFlags then Result := Result or 32;
  if afReadOnly in FFlags then Result := Result or 64;
end;

constructor TPDFCustomAnnotation.Create(AOwner: TPDFPage);
begin
  if AOwner = nil then
    raise TPDFException.Create(SAnnotationMustHaveTPDFPageAsOwner);
  FOwner := AOwner;
  AnnotID := FOwner.FOwner.GetNextID;
  FBorderStyle := '[0 0 1]';
  FBorderColor := clYellow;
  FOwner.FAnnot.Add(Pointer(Self));
end;

function TPDFCustomAnnotation.GetBox: TRect;
begin
  Result.Left := Round(FOwner.IntToExtX(FLeft));
  Result.Top := Round(FOwner.IntToExtY(FTop));
  Result.Bottom := Round(FOwner.IntToExtY(FBottom));
  Result.Right := Round(FOwner.IntToExtX(FRight));
end;


procedure TPDFCustomAnnotation.SetBox(const Value: TRect);
var
  R: TRect;
begin
  R := Value;
  NormalizeRect(R.Left, R.Top, R.Right, R.Bottom);
  FLeft := Round(FOwner.ExtToIntX(R.Left));
  FTop := Round(FOwner.ExtToIntY(R.Top));
  FBottom := Round(FOwner.ExtToIntY(R.Bottom));
  FRight := Round(FOwner.ExtToIntX(R.Right));
end;

{ TPDFTextAnnotation }


procedure TPDFTextAnnotation.Save;
begin
  FOwner.FOwner.StartObj(AnnotID);
  FOwner.FOwner.SaveToStream('/Type /Annot');
  FOwner.FOwner.SaveToStream('/Subtype /Text');
  FOwner.FOwner.SaveToStream('/Border ' + FBorderStyle);
  FOwner.FOwner.SaveToStream('/F ' + IntToStr(CalcFlags));
  FOwner.FOwner.SaveToStream('/C [' + FormatFloat(GetRValue(FBorderColor) / 255) + ' ' +
    FormatFloat(GetGValue(FBorderColor) / 255) + ' ' + FormatFloat(GetBValue(FBorderColor) / 255) + ' ]');
  FOwner.FOwner.SaveToStream('/Rect [' + IntToStr(Round(FLeft)) + ' ' + IntToStr(FBottom) +
    ' ' + IntToStr(FRight) + ' ' + IntToStr(FTop) + ']');
  if FCharset = ANSI_CHARSET then
  begin
    FOwner.FOwner.SaveToStream('/T (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, FCaption)) + ')');
    FOwner.FOwner.SaveToStream('/Contents (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, FText)) + ')');
  end else
  begin
    FOwner.FOwner.SaveToStream('/T <' + EnCodeHexString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, UnicodeChar(FCaption, FCharset)) + '>');
    FOwner.FOwner.SaveToStream('/Contents <' + EnCodeHexString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, UnicodeChar(FText, FCharset)) + '>');
  end;
  case TextAnnotationIcon of
    taiComment: FOwner.FOwner.SaveToStream('/Name /Comment');
    taiKey: FOwner.FOwner.SaveToStream('/Name /Key');
    taiNote: FOwner.FOwner.SaveToStream('/Name /Note');
    taiHelp: FOwner.FOwner.SaveToStream('/Name /Help');
    taiNewParagraph: FOwner.FOwner.SaveToStream('/Name /NewParagraph');
    taiParagraph: FOwner.FOwner.SaveToStream('/Name /Paragraph');
    taiInsert: FOwner.FOwner.SaveToStream('/Name /Insert');
  end;
  if FOpened then FOwner.FOwner.SaveToStream('/Open true') else FOwner.FOwner.SaveToStream('/Open false');
  FOwner.FOwner.CloseHObj;
  FOwner.FOwner.CloseObj;
end;

{ TPDFActionAnotation }

procedure TPDFActionAnnotation.Save;
begin
  FOwner.FOwner.StartObj(AnnotID);
  FOwner.FOwner.SaveToStream('/Type /Annot');
  FOwner.FOwner.SaveToStream('/Subtype /Link');
  FOwner.FOwner.SaveToStream('/Border ' + FBorderStyle);
  FOwner.FOwner.SaveToStream('/F ' + IntToStr(CalcFlags));
  FOwner.FOwner.SaveToStream('/C [' + FormatFloat(GetRValue(FBorderColor) / 255) + ' ' +
    FormatFloat(GetGValue(FBorderColor) / 255) + ' ' + FormatFloat(GetBValue(FBorderColor) / 255) + ' ]');
  FOwner.FOwner.SaveToStream('/Rect [' + IntToStr(Round(FLeft)) + ' ' + IntToStr(FBottom) +
    ' ' + IntToStr(FRight) + ' ' + IntToStr(FTop) + ']');
  FOwner.FOwner.SaveToStream('/A ' + IntToStr(FAction.ActionID) + ' 0 R');
  FOwner.FOwner.CloseHObj;
  FOwner.FOwner.CloseObj;
end;

procedure TPDFActionAnnotation.SetAction(const Value: TPDFAction);
begin
  FOwner.FOwner.AppendAction(Value);
  FAction := Value;
end;


{ TPDFActions }

function TPDFActions.Add(Action: TPDFAction): Integer;
begin
  Result := FActions.Add(Pointer(Action));
  Action.FOwner := FOwner;
end;

procedure TPDFActions.Clear;
var
  I: Integer;
begin
  for I := 0 to FActions.Count - 1 do
    TPDFAction(FActions[I]).Free;
  FActions.Clear;
end;

constructor TPDFActions.Create(AOwner: TPDFDocument);
begin
  FActions := TList.Create;
  FOwner := AOwner;
end;

procedure TPDFActions.Delete(Action: TPDFAction);
var
  I: Integer;
begin
  I := IndexOf(Action);
  if I <> -1 then
  begin
    TPDFAction(FActions[I]).Free;
    FActions.Delete(I);
  end;
end;

destructor TPDFActions.Destroy;
begin
  Clear;
  FActions.Free;
  inherited;
end;

function TPDFActions.GetAction(Index: Integer): TPDFAction;
begin
  Result := TPDFAction(FActions[Index]);
end;

function TPDFActions.GetCount: Integer;
begin
  Result := FActions.Count;
end;

function TPDFActions.IndexOf(Action: TPDFAction): Integer;
begin
  Result := FActions.IndexOf(Pointer(Action));
end;

{ TPDFURLAction }

procedure TPDFURLAction.Save;
begin
  FOwner.StartObj(ActionID);
  FOwner.SaveToStream('/S /URI /URI (' + EscapeSpecialChar(EnCodeString(FOwner.FProtectionEnabled, FOwner.FKey, FOwner.FPKL, ActionID, FURL)) + ')');
  if FNext <> nil then
    FOwner.SaveToStream('/Next ' + IntToStr(FNext.ActionID) + ' 0 R');
  FOwner.CloseHObj;
  FOwner.CloseObj;
end;

procedure TPDFURLAction.SetURL(const Value: string);
begin
  if Value = '' then
    raise TPDFException.Create(SURLCannotBeEmpty);
  FURL := Value;
end;

{ TPDFAction }

constructor TPDFAction.Create;
begin
  FNext := nil;
  ActionID := 0;
end;

procedure TPDFAction.Prepare;
begin
  if ActionID < 1 then
    ActionID := FOwner.GetNextID;
  if FNext <> nil then
    FOwner.AppendAction(FNext);
end;

{ TPDFJavaScriptAction }

procedure TPDFJavaScriptAction.Save;
begin
  FOwner.StartObj(ActionID);
  FOwner.SaveToStream('/S /JavaScript /JS (' + EscapeSpecialChar(EnCodeString(FOwner.FProtectionEnabled, FOwner.FKey, FOwner.FPKL, ActionID, FJavaScript)) + ')');
  if FNext <> nil then
    FOwner.SaveToStream('/Next ' + IntToStr(FNext.ActionID) + ' 0 R');
  FOwner.CloseHObj;
  FOwner.CloseObj;
end;

procedure TPDFJavaScriptAction.SetJavaScript(const Value: string);
begin
  if Value = '' then
    raise TPDFException.Create(SJavaScriptCannotBeEmpty);
  FJavaScript := Value;
end;

{ TPDFGoToPageAction }

procedure TPDFGoToPageAction.Save;
begin
  FOwner.StartObj(ActionID);
  FOwner.SaveToStream('/S /GoTo /D [' + IntToStr(FOwner.FPages[FPageIndex].PageID) +
    ' 0 R /FitH ' + IntToStr(Round(FOwner.FPages[PageIndex].ExtToIntY(FTopOffset))) + ']');
  if FNext <> nil then
    FOwner.SaveToStream('/Next ' + IntToStr(FNext.ActionID) + ' 0 R');
  FOwner.CloseHObj;
  FOwner.CloseObj;
end;

procedure TPDFGoToPageAction.SetPageIndex(const Value: Integer);
begin
  if Value < 0 then
    raise TPDFException.Create(SPageIndexCannotBeNegative);
  FPageIndex := Value;
end;

procedure TPDFGoToPageAction.SetTopOffset(const Value: Integer);
begin
  if Value < 0 then
    raise TPDFException.Create(STopOffsetCannotBeNegative);
  FTopOffset := Value;
end;

{ TPDFVisibeControlAction }

constructor TPDFVisibeControlAction.Create;
begin
  FControls := TPDFControls.Create;
  Visible := True;
end;

destructor TPDFVisibeControlAction.Destroy;
begin
  FControls.Free;
end;

procedure TPDFVisibeControlAction.Save;
var
  I: Integer;
begin
  FOwner.StartObj(ActionID);
  FOwner.SaveToStream('/S /Hide /T [', False);
  for I := 0 to FControls.Count - 1 do
    FOwner.SaveToStream(IntToStr(FControls[I].AnnotID) + ' 0 R ', False);
  FOwner.SaveToStream(']');
  if FVisible then FOwner.SaveToStream('/H false');
  if FNext <> nil then
    FOwner.SaveToStream('/Next ' + IntToStr(FNext.ActionID) + ' 0 R');
  FOwner.CloseHObj;
  FOwner.CloseObj;
end;


{ TPDFControls }

function TPDFControls.Add(Control: TPDFControl): Integer;
begin
  Result := FControls.Add(Pointer(Control));
end;

procedure TPDFControls.Clear;
begin
  FControls.Clear;
end;

constructor TPDFControls.Create;
begin
  FControls := TList.Create;
end;

procedure TPDFControls.Delete(Control: TPDFControl);
begin
  FControls.Delete(IndexOf(Control));
end;

destructor TPDFControls.Destroy;
begin
  FControls.Free;
end;

function TPDFControls.GetControl(Index: Integer): TPDFControl;
begin
  Result := TPDFControl(FControls[Index]);
end;

function TPDFControls.GetCount: Integer;
begin
  Result := FControls.Count;
end;

function TPDFControls.IndexOf(Control: TPDFControl): Integer;
begin
  Result := FControls.IndexOf(Pointer(Control));
end;

{ TPDFSubmitFormAction }

constructor TPDFSubmitFormAction.Create;
begin
  URL := 'http://www.borland.com';
  FFields := TPDFControls.Create;
  FRG := TList.Create
end;

destructor TPDFSubmitFormAction.Destroy;
begin
  FRG.Free;
  FFields.Free;
end;

procedure TPDFSubmitFormAction.Save;
var
  I, Flag: Integer;
  S: string;
begin
  I := 0;
  while I < FFields.Count do
  begin
    if FFields[I] is TPDFButton then FFields.Delete(FFields[I]) else
      if FFields[I] is TPDFRadioButton then
      begin
        if FRG.IndexOf(TPDFRadioButton(FFields[I]).FRG) < 0 then
          FRG.Add(TPDFRadioButton(FFields[I]).FRG);
        FFields.Delete(FFields[I]);
      end else Inc(I);
  end;
  Flag := 0;
  if SubmitType <> StFDF then
  begin
    if SubmitType <> stPost then
      Flag := Flag or 8;
    Flag := Flag or 4;
    S := '';
  end
  else S := '#FDF';
  if FIncludeEmptyFields then Flag := Flag or 2;
  FOwner.StartObj(ActionID);
  FOwner.SaveToStream('/S /SubmitForm');
  FOwner.SaveToStream('/F <</FS /URL /F (' + EscapeSpecialChar(EnCodeString(FOwner.FProtectionEnabled, FOwner.FKey, FOwner.FPKL, ActionID, FURL + S)) + ')>>');
  if (FFields.Count > 0) or (FRG.Count > 0) then
  begin
    FOwner.SaveToStream('/Fields [', False);
    for I := 0 to FFields.Count - 1 do
      FOwner.SaveToStream(IntToStr(FFields[I].AnnotID) + ' 0 R ', False);
    for I := 0 to FRG.Count - 1 do
      FOwner.SaveToStream(IntToStr(TPDFRadioGroup(FRG[I]).GroupID) + ' 0 R ', False);
    FOwner.SaveToStream(']');
  end;
  FOwner.SaveToStream('/Flags ' + IntToStr(Flag));
  if FNext <> nil then
    FOwner.SaveToStream('/Next ' + IntToStr(FNext.ActionID) + ' 0 R');
  FOwner.CloseHObj;
  FOwner.CloseObj;
end;

procedure TPDFSubmitFormAction.SetURL(const Value: string);
begin
  if Value = '' then
    raise TPDFException.Create(SURLCannotBeEmpty);
  FURL := Value;
end;

{ TPDFResetFormAction }

constructor TPDFResetFormAction.Create;
begin
  FFields := TPDFControls.Create;
  FRG := TList.Create;
end;

destructor TPDFResetFormAction.Destroy;
begin
  FRG.Free;
  FFields.Free;
end;

procedure TPDFResetFormAction.Save;
var
  I: Integer;
begin
  I := 0;
  while I < FFields.Count do
  begin
    if FFields[I] is TPDFButton then FFields.Delete(FFields[I]) else
      if FFields[I] is TPDFRadioButton then
      begin
        if FRG.IndexOf(TPDFRadioButton(FFields[I]).FRG) < 0 then
          FRG.Add(TPDFRadioButton(FFields[I]).FRG);
        FFields.Delete(FFields[I]);
      end else Inc(I);
  end;
  FOwner.StartObj(ActionID);
  FOwner.SaveToStream('/S /ResetForm');
  if (FFields.Count > 0) or (FRG.Count > 0) then
  begin
    FOwner.SaveToStream('/Fields [', False);
    for I := 0 to FFields.Count - 1 do
      FOwner.SaveToStream(IntToStr(FFields[I].AnnotID) + ' 0 R ', False);
    for I := 0 to FRG.Count - 1 do
      FOwner.SaveToStream(IntToStr(TPDFRadioGroup(FRG[I]).GroupID) + ' 0 R ', False);
    FOwner.SaveToStream(']');
  end;
  if FNext <> nil then
    FOwner.SaveToStream('/Next ' + IntToStr(FNext.ActionID) + ' 0 R');
  FOwner.CloseHObj;
  FOwner.CloseObj;
end;

{ TPDFImportDataAction }

procedure TPDFImportDataAction.Save;
begin
  FOwner.StartObj(ActionID);
  FOwner.SaveToStream('/S /ImportData');
  FOwner.SaveToStream('/F <</Type /FileSpec /F (' + EscapeSpecialChar(EnCodeString(FOwner.FProtectionEnabled, FOwner.FKey, FOwner.FPKL, ActionID, FFileName)) + ')>>');
  if FNext <> nil then
    FOwner.SaveToStream('/Next ' + IntToStr(FNext.ActionID) + ' 0 R');
  FOwner.CloseHObj;
  FOwner.CloseObj;
end;

procedure TPDFImportDataAction.SetFileName(const Value: string);
begin
  if Value = '' then
    raise TPDFException.Create(SFileNameCannotBeEmpty);
  FFileName := Value;
end;

{ TPDFControl }

function TPDFControl.CalcActions: string;
begin
  Result := '';
  if OnMouseUp <> nil then
    Result := Result + '/A ' + IntToStr(OnMouseUp.ActionID) + ' 0 R'#13#10;
  Result := Result + '/AA <<';
  if OnMouseEnter <> nil then
    Result := Result + '/E ' + IntToStr(OnMouseEnter.ActionID) + ' 0 R';
  if OnMouseExit <> nil then
    Result := Result + '/X ' + IntToStr(OnMouseExit.ActionID) + ' 0 R';
  if OnMouseDown <> nil then
    Result := Result + '/D ' + IntToStr(OnMouseDown.ActionID) + ' 0 R';
  if OnLostFocus <> nil then
    Result := Result + '/Bl ' + IntToStr(OnLostFocus.ActionID) + ' 0 R';
  if OnSetFocus <> nil then
    Result := Result + '/D ' + IntToStr(OnSetFocus.ActionID) + ' 0 R';
end;

constructor TPDFControl.Create(AOwner: TPDFPage; AName: string);
begin
  inherited Create(AOwner);
  Flags := [afPrint];
  Name := AName;
  FFont := TPDFControlFont.Create;
  FHint := TPDFControlHint.Create;
  FBorderColor := clBlack;
  if not ((Self is TPDFButton) or (Self is TPDFRadioButton)) then
    FOwner.FOwner.FAcroForm.FFields.Add(Self);
end;

destructor TPDFControl.Destroy;
begin
  FFont.Free;
  FHint.Free;
  inherited;
end;

procedure TPDFControl.SetColor(const Value: TColor);
begin
  FColor := Value;
end;

procedure TPDFControl.SetName(const Value: string);
begin
  FName := Value;
end;

procedure TPDFControl.SetOnLostFocus(const Value: TPDFAction);
begin
  if FOnLostFocus = nil then FOnLostFocus := Value
  else
  begin
    Value.FNext := FOnLostFocus;
    FOnLostFocus := Value;
  end;
  FOwner.FOwner.AppendAction(Value);
end;

procedure TPDFControl.SetOnMouseDown(const Value: TPDFAction);
begin
  if FOnMouseDown = nil then FOnMouseDown := Value
  else
  begin
    Value.FNext := FOnMouseDown;
    FOnMouseDown := Value;
  end;
  FOwner.FOwner.AppendAction(Value);
end;

procedure TPDFControl.SetOnMouseEnter(const Value: TPDFAction);
begin
  if FOnMouseEnter = nil then FOnMouseEnter := Value
  else
  begin
    Value.FNext := FOnMouseEnter;
    FOnMouseEnter := Value;
  end;
  FOwner.FOwner.AppendAction(Value);
end;

procedure TPDFControl.SetOnMouseExit(const Value: TPDFAction);
begin
  if FOnMouseExit = nil then FOnMouseExit := Value
  else
  begin
    Value.FNext := FOnMouseExit;
    FOnMouseExit := Value;
  end;
  FOwner.FOwner.AppendAction(Value);
end;

procedure TPDFControl.SetOnMouseUp(const Value: TPDFAction);
begin
  if FOnMouseUp = nil then FOnMouseUp := Value
  else
  begin
    Value.FNext := FOnMouseUp;
    FOnMouseUp := Value;
  end;
  FOwner.FOwner.AppendAction(Value);
end;

procedure TPDFControl.SetOnSetFocus(const Value: TPDFAction);
begin
  if FOnSetFocus = nil then FOnSetFocus := Value
  else
  begin
    Value.FNext := FOnSetFocus;
    FOnSetFocus := Value;
  end;
  FOwner.FOwner.AppendAction(Value);
end;

{ TPDFAcroForm }

procedure TPDFAcroForm.Clear;
begin
  FFields.Clear;
  FFonts.Clear;
  FRadioGroups.Clear;
end;

constructor TPDFAcroForm.Create(AOwner: TPDFDocument);
begin
  FFields := TPDFControls.Create;
  FFonts := TList.Create;
  FOwner := AOwner;
  FRadioGroups := TList.Create;
end;

destructor TPDFAcroForm.Destroy;
begin
  FRadioGroups.Free;
  FFonts.Free;
  FFields.Free;
end;

function TPDFAcroForm.GetEmpty: Boolean;
begin
  Result := (FFields.Count = 0) and (FRadioGroups.Count = 0);
end;

procedure TPDFAcroForm.Save;
var
  I, ResID: Integer;
begin
  if (FFields.Count = 0) and (FRadioGroups.Count = 0) then Exit;
  FOwner.StartObj(ResID);
  FOwner.SaveToStream('/Font <<', False);
  for I := 0 to FFonts.Count - 1 do
    FOwner.SaveToStream('/' + TPDFFont(FFonts[I]).AliasName + ' ' + IntToStr(TPDFFont(FFonts[I]).FontID) + ' 0 R ', False);
  FOwner.SaveToStream('>>');
  FOwner.CloseHObj;
  FOwner.CloseObj;
  FOwner.StartObj(AcroID);
  FOwner.SaveToStream('/Fields [', False);
  for I := 0 to FFields.Count - 1 do
    FOwner.SaveToStream(IntToStr(FFields[I].AnnotID) + ' 0 R ', False);
  for I := 0 to FRadioGroups.Count - 1 do
    FOwner.SaveToStream(IntToStr(TPDFRadioGroup(FRadioGroups[I]).GroupID) + ' 0 R ', False);
  FOwner.SaveToStream(']', False);
  FOwner.SaveToStream('/DR ' + IntToStr(ResID) + ' 0 R');
  FOwner.SaveToStream('/CO [', False);
  for I := 0 to FFields.Count - 1 do
    if FFields[I] is TPDFInputControl then
      if TPDFInputControl(FFields[I]).OnOtherCOntrolChanged <> nil then
        FOwner.SaveToStream(IntToStr(FFields[I].AnnotID) + ' 0 R ', False);
  FOwner.SaveToStream(']', False);
  FOwner.CloseHObj;
  FOwner.CloseObj;
end;

{ TPDFInputControl }

function TPDFInputControl.CalcActions: string;
begin
  Result := inherited CalcActions;
  if OnKeyPress <> nil then
    Result := Result + '/K ' + IntToStr(OnKeyPress.ActionID) + ' 0 R';
  if OnBeforeFormatting <> nil then
    Result := Result + '/F ' + IntToStr(OnBeforeFormatting.ActionID) + ' 0 R';
  if OnChange <> nil then
    Result := Result + '/V ' + IntToStr(OnChange.ActionID) + ' 0 R';
  if OnOtherControlChanged <> nil then
    Result := Result + '/C ' + IntToStr(OnOtherControlChanged.ActionID) + ' 0 R';
end;

procedure TPDFInputControl.SetOnBeforeFormatting(const Value: TPDFJavaScriptAction);
begin
  FOnBeforeFormatting := Value;
  FOwner.FOwner.AppendAction(Value);
end;

procedure TPDFInputControl.SetOnChange(const Value: TPDFJavaScriptAction);
begin
  FOnChange := Value;
  FOwner.FOwner.AppendAction(Value);
end;

procedure TPDFInputControl.SetOnKeyPress(const Value: TPDFJavaScriptAction);
begin
  FOnKeyPress := Value;
  FOwner.FOwner.AppendAction(Value);
end;

procedure TPDFInputControl.SetOnOtherCOntrolChanged(const Value: TPDFJavaScriptAction);
begin
  FOnOtherCOntrolChanged := Value;
  FOwner.FOwner.AppendAction(Value);
end;

{ TPDFControlFont }

constructor TPDFControlFont.Create;
begin
  FName := 'Arial';
  FColor := clBlack;
  FCharset := 0;
  FSize := 8;
  FStyle := [];
end;

procedure TPDFControlFont.SetCharset(const Value: TFontCharset);
begin
  FCharset := Value;
end;

procedure TPDFControlFont.SetSize(const Value: Integer);
begin
  if Value < 0 then
    raise TPDFException.Create(SCannotSetNegativeSize);
  FSize := Value;
end;

{ TPDFButton }

constructor TPDFButton.Create(AOwner: TPDFPage; AName: string);
begin
  inherited Create(AOwner, AName);
  FColor := clSilver;
end;

procedure TPDFButton.Paint;
var
  x, y, z: array[0..3] of Byte;
  i: Integer;
begin
  I := ColorToRGB(FFont.Color);
  Move(i, x[0], 4);
  i := ColorToRGB(FColor);
  Move(i, z[0], 4);
  i := ColorToRGB(FBorderColor);
  Move(i, y[0], 4);
  with FUp do
  begin
    Width := abs(FRight - FLeft);
    Height := abs(FBottom - FTop);
    SetRGBColorFill(z[0] / 255 * 0.6, z[1] / 255 * 0.6, z[2] / 255 * 0.6);
    MoveTo(Width - 0.5, 0.5);
    LineTo(Width - 0.5, Height - 0.5);
    LineTo(0.5, Height - 0.5);
    LineTo(1.5, Height - 1.5);
    LineTo(Width - 1.5, Height - 1.5);
    LineTo(Width - 1.5, 1.5);
    LineTo(Width - 0.5, 0.5);
    Fill;
    SetRGBColorFill(z[0] / 255 * 1.4, z[1] / 255 * 1.4, z[2] / 255 * 1.4);
    MoveTo(0.5, Height - 0.5);
    LineTo(0.5, 0.5);
    LineTo(Width - 0.5, 0.5);
    LineTo(Width - 1.5, 1.5);
    LineTo(1.5, 1.5);
    LineTo(1.5, Height - 1.5);
    LineTo(0.5, Height - 0.5);
    Fill;
    SetRGBColorFill(z[0] / 255, z[1] / 255, z[2] / 255);
    Rectangle(1.5, 1.5, Width - 1.5, Height - 1.5);
    Fill;
    SetLineWidth(1);
    SetRGBColorStroke(y[0] / 255, y[1] / 255, y[2] / 255);
    Rectangle(0, 0, Width, Height);
    Stroke;
    BeginText;
    SetRGBColorFill(x[0] / 255, X[1] / 255, X[2] / 255);
    SetActiveFont(FFont.Name, FFont.Style, FFont.Size, FFont.Charset);
    FFN := FOwner.FFonts.FLast.AliasName;
    TextBox(Rect(0, 0, Width, Height), Caption, hjCenter, vjCenter);
    EndText;
  end;
  with FDown do
  begin
    Width := abs(FRight - FLeft);
    Height := abs(FBottom - FTop);
    SetLineWidth(1);
    SetRGBColorFill(z[0] / 255, z[1] / 255, z[2] / 255);
    Rectangle(0, 0, Width, Height);
    Fill;
    SetRGBColorStroke(z[0] / 255 * 1.4, z[1] / 255 * 1.4, z[2] / 255 * 1.4);
    MoveTo(Width - 0.5, 1);
    LineTo(Width - 0.5, Height - 0.5);
    LineTo(1, Height - 0.5);
    Stroke;
    SetRGBColorStroke(z[0] / 255 * 0.6, z[1] / 255 * 0.6, z[2] / 255 * 0.6);
    MoveTo(0.5, Height - 1);
    LineTo(0.5, 0.5);
    LineTo(Width - 1, 0.5);
    Stroke;
    SetRGBColorStroke(y[0] / 255, y[1] / 255, y[2] / 255);
    Rectangle(0, 0, Width, Height);
    Stroke;
    BeginText;
    SetRGBColorFill(x[0] / 255, X[1] / 255, X[2] / 255);
    SetActiveFont(FFont.Name, FFont.Style, FFont.Size, FFont.Charset);
    TextBox(Rect(0, 0, Width, Height - 2), Caption, hjCenter, vjCenter);
    EndText;
  end;
end;

procedure TPDFButton.Save;
var
  i, j: Integer;
begin
  FUp := TPDFPage.Create(FOwner.FOwner);
  try
    FUp.FIsForm := True;
    try
      FDown := TPDFPage.Create(FOwner.FOwner);
      FDown.FIsForm := True;
      Paint;
      FUp.Save;
      FDown.Save;
      for i := 0 to FUp.FLinkedFont.Count - 1 do
        if FOwner.FOwner.FAcroForm.FFonts.IndexOf(FUp.FLinkedFont[i]) = -1 then
          FOwner.FOwner.FAcroForm.FFonts.Add(FUp.FLinkedFont[i]);
      FOwner.FOwner.StartObj(AnnotID);
      FOwner.FOwner.SaveToStream('/Type /Annot');
      FOwner.FOwner.SaveToStream('/Subtype /Widget');
      FOwner.FOwner.SaveToStream('/Rect [' + IntToStr(Round(FLeft)) + ' ' + IntToStr(FBottom) +
        ' ' + IntToStr(FRight) + ' ' + IntToStr(FTop) + ']');
      FOwner.FOwner.SaveToStream('/P ' + IntToStr(FOwner.PageID) + ' 0 R');
      FOwner.FOwner.SaveToStream('/MK <</CA (' +
        EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, Caption)) + ') ', False);
      FOwner.FOwner.SaveToStream('/BC [' + FormatFloat(GetRValue(FBorderColor) / 255) + ' ' +
        FormatFloat(GetGValue(FBorderColor) / 255) + ' ' + FormatFloat(GetBValue(FBorderColor) / 255) + ' ]', False);
      FOwner.FOwner.SaveToStream('/BG [' + FormatFloat(GetRValue(FColor) / 255) + ' ' +
        FormatFloat(GetGValue(FColor) / 255) + ' ' + FormatFloat(GetBValue(FColor) / 255) + ' ]', False);
      FOwner.FOwner.SaveToStream('>>');
      FOwner.FOwner.SaveToStream('/DA (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled,
        FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, '/' + FFN + ' ' + IntToStr(FFont.Size))) + ' Tf ' +
        FormatFloat(GetRValue(FFont.Color) / 255) + ' ' +
        FormatFloat(GetGValue(FFont.Color) / 255) + ' ' + FormatFloat(GetBValue(FFont.Color) / 255) + ' rg)');
      FOwner.FOwner.SaveToStream('/BS <</W 1 /S /B>>');
      FOwner.FOwner.SaveToStream('/T (' +
        EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, Name)) + ')');
      FOwner.FOwner.SaveToStream('/FT /Btn');
      if FHint.Caption <> '' then
        if (FHint.Charset in [0..2]) then
          FOwner.FOwner.SaveToStream('/TU (' +
            EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, FHint.Caption)) + ')')
        else
          FOwner.FOwner.SaveToStream('/TU <' +
            EnCodeHexString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, UnicodeChar(FHint.Caption, FHint.Charset)) + '>');
      i := 0;
      if FReadOnly then i := i or 1;
      if FRequired then i := i or 2;
      j := 1 shl 16;
      i := i or j;
      FOwner.FOwner.SaveToStream('/F ' + IntToStr(CalcFlags));
      FOwner.FOwner.SaveToStream('/Ff ' + IntToStr(i));
      FOwner.FOwner.SaveToStream('/H /P');
      FOwner.FOwner.SaveToStream('/AP <</N ' + IntToStr(FUp.PageID) + ' 0 R ', False);
      FOwner.FOwner.SaveToStream('/D ' + IntToStr(FDown.PageID) + ' 0 R', False);
      FOwner.FOwner.SaveToStream('>>');
      FOwner.FOwner.SaveToStream(CalcActions + '>>');
      FOwner.FOwner.CloseHObj;
      FOwner.FOwner.CloseObj;
    finally
      FDown.Free;
    end;
  finally
    FUp.Free;
  end;
end;

{ TPDFCheckBox }

constructor TPDFCheckBox.Create(AOwner: TPDFPage; AName: string);
begin
  inherited Create(AOwner, AName);
  FColor := clWhite;
end;

procedure TPDFCheckBox.Paint;
var
  x, y, z: array[0..3] of Byte;
  i: Integer;
begin
  I := ColorToRGB(FFont.Color);
  Move(i, x[0], 4);
  i := ColorToRGB(FColor);
  Move(i, z[0], 4);
  i := ColorToRGB(FBorderColor);
  Move(i, y[0], 4);
  with FCheck do
  begin
    Width := abs(FRight - FLeft);
    Height := abs(FBottom - FTop);
    SetLineWidth(1);
    SetRGBColorFill(z[0] / 255, z[1] / 255, z[2] / 255);
    Rectangle(0, 0, Width, Height);
    Fill;
    SetRGBColor(y[0] / 255, y[1] / 255, y[2] / 255);
    Rectangle(0.5, 0.5, Height - 0.5, Height - 0.5);
    Stroke;
    BeginText;
    SetActiveFont('ZapfDingbats', [], Height - 2);
    TextBox(Rect(2, 2, Height - 2, Height - 4), '8', hjCenter, vjCenter);
    SetRGBColorFill(x[0] / 255, X[1] / 255, X[2] / 255);
    SetActiveFont(FFont.Name, FFont.Style, FFont.Size, FFont.Charset);
    TextBox(Rect(Height + 4, 0, Width, Height), Caption, hjLeft, vjCenter);
    EndText;
  end;
  with FUncheck do
  begin
    Width := abs(FRight - FLeft);
    Height := abs(FBottom - FTop);
    SetLineWidth(1);
    SetRGBColorFill(z[0] / 255, z[1] / 255, z[2] / 255);
    Rectangle(0, 0, Width, Height);
    Fill;
    SetRGBColorStroke(y[0] / 255, y[1] / 255, y[2] / 255);
    Rectangle(0.5, 0.5, Height - 0.5, Height - 0.5);
    Stroke;
    BeginText;
    SetRGBColorFill(x[0] / 255, X[1] / 255, X[2] / 255);
    SetActiveFont(FFont.Name, FFont.Style, FFont.Size, FFont.Charset);
    TextBox(Rect(Height + 4, 0, Width, Height), Caption, hjLeft, vjCenter);
    EndText;
  end;
end;

procedure TPDFCheckBox.Save;
var
  i: Integer;
begin
  FCheck := TPDFPage.Create(FOwner.FOwner);
  try
    FCheck.FIsForm := True;
    FUnCheck := TPDFPage.Create(FOwner.FOwner);
    try
      FUnCheck.FIsForm := True;
      Paint;
      FCheck.Save;
      FUnCheck.Save;
      FOwner.FOwner.StartObj(AnnotID);
      FOwner.FOwner.SaveToStream('/Type /Annot');
      FOwner.FOwner.SaveToStream('/Subtype /Widget');
      FOwner.FOwner.SaveToStream('/H /T');
      FOwner.FOwner.SaveToStream('/Rect [' + IntToStr(Round(FLeft)) + ' ' + IntToStr(FBottom) +
        ' ' + IntToStr(FRight) + ' ' + IntToStr(FTop) + ']');
      FOwner.FOwner.SaveToStream('/P ' + IntToStr(FOwner.PageID) + ' 0 R');
      if FChecked then FOwner.FOwner.SaveToStream('/V /Yes /AS /Yes') else FOwner.FOwner.SaveToStream('/V /Off /AS /Off');
      FOwner.FOwner.SaveToStream('/T (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, Name)) + ')');
      FOwner.FOwner.SaveToStream('/FT /Btn');
      FOwner.FOwner.SaveToStream('/F ' + IntToStr(CalcFlags));
      i := 0;
      if FReadOnly then i := i or 1;
      if FRequired then i := i or 2;
      FOwner.FOwner.SaveToStream('/Ff ' + IntToStr(i));
      FOwner.FOwner.SaveToStream('/AP <</N << /Yes ' + IntToStr(FCheck.PageID) + ' 0 R ', False);
      FOwner.FOwner.SaveToStream('/Off  ' + IntToStr(FUnCheck.PageID) + ' 0 R >> ', False);
      FOwner.FOwner.SaveToStream('>>');
      FOwner.FOwner.SaveToStream(CalcActions + '>>');
      FOwner.FOwner.CloseHObj;
      FOwner.FOwner.CloseObj;
    finally
      FUnCheck.Free;
    end;
  finally
    FCheck.Free;
  end;
end;

{ TPDFEdit }

constructor TPDFEdit.Create(AOwner: TPDFPage; AName: string);
begin
  inherited Create(AOwner, AName);
  FBorderColor := clBlack;
  FColor := clWhite;
  FShowBorder := True;
  FMultiline := False;
  FIsPassword := False;
  FMaxLength := 0;
end;

procedure TPDFEdit.Paint;
var
  x, y, z: array[0..3] of Byte;
  i: Integer;
  s: string;
begin
  i := ColorToRGB(FColor);
  Move(i, z[0], 4);
  i := ColorToRGB(FBorderColor);
  Move(i, y[0], 4);
  I := ColorToRGB(FFont.Color);
  Move(i, x[0], 4);
  with FShow do
  begin
    Width := abs(FRight - FLeft);
    Height := abs(FBottom - FTop);
    FEmulationEnabled := False;
    SetLineWidth(1);
    SetRGBColorFill(z[0] / 255, z[1] / 255, z[2] / 255);
    SetRGBColorStroke(y[0] / 255, y[1] / 255, y[2] / 255);
    Rectangle(0, 0, Width, Height);
    if FShowBorder then FillAndStroke else Fill;
    NewPath;
    Rectangle(0, 0, Width, Height);
    Clip;
    NewPath;
    AppendAction('/Tx BMC');
    BeginText;
    SetRGBColorFill(x[0] / 255, X[1] / 255, X[2] / 255);
    SetActiveFont(FFont.Name, FFont.Style, FFont.Size, FFont.Charset);
    FFN := FOwner.FFonts.FLast.AliasName;
    if FText <> '' then
      if not FIsPassword then
        TextBox(Rect(2, 2, Width - 2, Height - 2), FText, FJustification, vjCenter)
      else
      begin
        s := '';
        for i := 1 to Length(FText) do s := s + '*';
        TextBox(Rect(2, 2, Width - 2, Height - 2), s, FJustification, vjCenter)
      end;
    EndText;
    AppendAction('EMC');
  end;
end;

procedure TPDFEdit.Save;
var
  i, j: Integer;
begin
  FShow := TPDFPage.Create(FOwner.FOwner);
  try
    FShow.FIsForm := True;
    FShow.FRemoveCR := True;
    Paint;
    for i := 0 to FShow.FLinkedFont.Count - 1 do
      if FOwner.FOwner.FAcroForm.FFonts.IndexOf(FShow.FLinkedFont[i]) = -1 then
        FOwner.FOwner.FAcroForm.FFonts.Add(FShow.FLinkedFont[i]);
    FShow.Save;
    FOwner.FOwner.StartObj(AnnotID);
    FOwner.FOwner.SaveToStream('/Type /Annot');
    FOwner.FOwner.SaveToStream('/Subtype /Widget');
    FOwner.FOwner.SaveToStream('/Rect [' + IntToStr(Round(FLeft)) + ' ' + IntToStr(FBottom) +
      ' ' + IntToStr(FRight) + ' ' + IntToStr(FTop) + ']');
    FOwner.FOwner.SaveToStream('/FT /Tx');
    FOwner.FOwner.SaveToStream('/F ' + IntToStr(CalcFlags));
    FOwner.FOwner.SaveToStream('/P ' + IntToStr(FOwner.PageID) + ' 0 R');
    FOwner.FOwner.SaveToStream('/T (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, Name)) + ')');
    i := 0;
    if FReadOnly then i := i or 1;
    if FRequired then i := i or 2;
    if FMultiline then
    begin
      j := 1 shl 12;
      i := i or j;
    end;
    if FIsPassword then
    begin
      j := 1 shl 13;
      i := i or j;
    end;
    FOwner.FOwner.SaveToStream('/Ff ' + IntToStr(i));
    case FJustification of
      hjCenter: FOwner.FOwner.SaveToStream('/Q 1');
      hjRight: FOwner.FOwner.SaveToStream('/Q 2');
    end;
    if FText <> '' then
      FOwner.FOwner.SaveToStream('/V (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, FText)) + ')');
    if FText <> '' then
      FOwner.FOwner.SaveToStream('/DV (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, FText)) + ')');
    if FMaxLength <> 0 then
      FOwner.FOwner.SaveToStream('/MaxLen ' + IntToStr(FMaxLength));
    FOwner.FOwner.SaveToStream('/DA (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled,
      FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, '/' + FFN + ' ' + IntToStr(FFont.Size))) + ' Tf ' +
      FormatFloat(GetRValue(FFont.Color) / 255) + ' ' +
      FormatFloat(GetGValue(FFont.Color) / 255) + ' ' + FormatFloat(GetBValue(FFont.Color) / 255) + ' rg)');
    FOwner.FOwner.SaveToStream('/AP <</N ' + IntToStr(FShow.PageID) + ' 0 R >> ');
    FOwner.FOwner.SaveToStream(CalcActions + '>>');
    FOwner.FOwner.CloseHObj;
    FOwner.FOwner.CloseObj;
  finally
    FShow.Free;
  end;
end;

procedure TPDFEdit.SetMaxLength(const Value: Integer);
begin
  if Value < 0 then FMaxLength := 0 else FMaxLength := Value;
end;

{ TPDFComboBox }

constructor TPDFComboBox.Create(AOwner: TPDFPage; AName: string);
begin
  inherited Create(AOwner, AName);
  FItems := TStringList.Create;
  FEditEnabled := True;
  FBorderColor := clBlack;
  FColor := clWhite;
end;

destructor TPDFComboBox.Destroy;
begin
  inherited;
  FItems.Free;
end;

procedure TPDFComboBox.Paint;
var
  x, y, z: array[0..3] of Byte;
  i: Integer;
begin
  i := ColorToRGB(FColor);
  Move(i, z[0], 4);
  i := ColorToRGB(FBorderColor);
  Move(i, y[0], 4);
  I := ColorToRGB(FFont.Color);
  Move(i, x[0], 4);
  with FShow do
  begin
    Width := abs(FRight - FLeft);
    Height := abs(FBottom - FTop);
    FEmulationEnabled := False;
    SetLineWidth(1);
    SetRGBColorFill(z[0] / 255, z[1] / 255, z[2] / 255);
    SetRGBColorStroke(y[0] / 255, y[1] / 255, y[2] / 255);
    Rectangle(0, 0, Width, Height);
    FillAndStroke;
    NewPath;
    Rectangle(0, 0, Width, Height);
    Clip;
    NewPath;
    AppendAction('/Tx BMC');
    BeginText;
    SetRGBColorFill(x[0] / 255, X[1] / 255, X[2] / 255);
    SetActiveFont(FFont.Name, FFont.Style, FFont.Size, FFont.Charset);
    FFN := FOwner.FFonts.FLast.AliasName;
    if FText <> '' then
      TextBox(Rect(2, 2, Width - 2, Height - 2), FText, hjLeft, vjCenter);
    EndText;
    AppendAction('EMC');
  end;
end;

procedure TPDFComboBox.Save;
var
  i, j: Integer;
begin
  FShow := TPDFPage.Create(FOwner.FOwner);
  try
    FShow.FIsForm := True;
    FShow.FRemoveCR := True;
    Paint;
    for i := 0 to FShow.FLinkedFont.Count - 1 do
      if FOwner.FOwner.FAcroForm.FFonts.IndexOf(FShow.FLinkedFont[i]) = -1 then
        FOwner.FOwner.FAcroForm.FFonts.Add(FShow.FLinkedFont[i]);
    FShow.Save;
    FOwner.FOwner.StartObj(AnnotID);
    FOwner.FOwner.SaveToStream('/Type /Annot');
    FOwner.FOwner.SaveToStream('/Subtype /Widget');
    FOwner.FOwner.SaveToStream('/Rect [' + IntToStr(Round(FLeft)) + ' ' + IntToStr(FBottom) +
      ' ' + IntToStr(FRight) + ' ' + IntToStr(FTop) + ']');
    FOwner.FOwner.SaveToStream('/FT /Ch');
    FOwner.FOwner.SaveToStream('/F ' + IntToStr(CalcFlags));
    FOwner.FOwner.SaveToStream('/P ' + IntToStr(FOwner.PageID) + ' 0 R');
    FOwner.FOwner.SaveToStream('/T (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, Name)) + ')');
    i := 0;
    if FReadOnly then i := i or 1;
    if FRequired then i := i or 2;
    j := 1 shl 17;
    i := i or j;
    if FEditEnabled then
    begin
      j := 1 shl 18;
      i := i or j;
    end;
    FOwner.FOwner.SaveToStream('/Ff ' + IntToStr(i));
    FOwner.FOwner.SaveToStream('/Opt [', False);
    for i := 0 to Items.Count - 1 do
      FOwner.FOwner.SaveToStream('(' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, Items[i])) + ')');
    FOwner.FOwner.SaveToStream(']');
    FOwner.FOwner.SaveToStream('/F 4');
    if FText <> '' then
      FOwner.FOwner.SaveToStream('/V (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, FText)) + ')');
    if FText <> '' then
      FOwner.FOwner.SaveToStream('/DV (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, FText)) + ')');
    FOwner.FOwner.SaveToStream('/DA (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled,
      FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, '/' + FFN + ' ' + IntToStr(FFont.Size))) + ' Tf ' +
      FormatFloat(GetRValue(FFont.Color) / 255) + ' ' +
      FormatFloat(GetGValue(FFont.Color) / 255) + ' ' + FormatFloat(GetBValue(FFont.Color) / 255) + ' rg)');
    FOwner.FOwner.SaveToStream('/AP <</N ' + IntToStr(FShow.PageID) + ' 0 R >> ');
    FOwner.FOwner.SaveToStream(CalcActions + '>>');
    FOwner.FOwner.CloseHObj;
    FOwner.FOwner.CloseObj;
  finally
    FShow.Free;
  end;
end;

{ TPDFRadioButton }

constructor TPDFRadioButton.Create(AOwner: TPDFPage; AName: string);
var
  I: Integer;
  F: Boolean;
begin
  inherited Create(AOwner, AName);
  FExportValue := '';
  FColor := clWhite;
  F := False;
  for I := 0 to FOwner.FOwner.FRadioGroups.Count - 1 do
    if UpperCase(AName) = UpperCase(TPDFRadioGroup(FOwner.FOwner.FRadioGroups[I]).FName) then
    begin
      FRG := TPDFRadioGroup(FOwner.FOwner.FRadioGroups[I]);
      F := True;
      Break;
    end;
  if not F then
    FRG := FOwner.FOwner.CreateRadioGroup(AName);
  FRG.FButtons.Add(Self);
  ExportValue := '';
end;

procedure TPDFRadioButton.Paint;
var
  x, y, z: array[0..3] of Byte;
  i: Integer;
begin
  I := ColorToRGB(FFont.Color);
  Move(i, x[0], 4);
  i := ColorToRGB(FColor);
  Move(i, z[0], 4);
  i := ColorToRGB(FBorderColor);
  Move(i, y[0], 4);
  with FCheck do
  begin
    Width := abs(FRight - FLeft);
    Height := abs(FBottom - FTop);
    SetLineWidth(1);
    SetRGBColorFill(z[0] / 255, z[1] / 255, z[2] / 255);
    SetRGBColorStroke(y[0] / 255, y[1] / 255, y[2] / 255);
    Circle(Width / 2, Height / 2, Height / 2 - 0.5);
    FillAndStroke;
    SetRGBColorFill(x[0] / 255, X[1] / 255, X[2] / 255);
    Circle(Width / 2, Height / 2, Height / 4 - 0.5);
    Fill;
    BeginText;
    SetActiveFont('ZapfDingbats', [], 10);
    FFN := FOwner.FFonts.FLast.AliasName;
    EndText;
  end;
  with FUncheck do
  begin
    Width := abs(FRight - FLeft);
    Height := abs(FBottom - FTop);
    SetLineWidth(1);
    SetRGBColorFill(z[0] / 255, z[1] / 255, z[2] / 255);
    SetRGBColorStroke(y[0] / 255, y[1] / 255, y[2] / 255);
    Circle(Width / 2, Height / 2, Height / 2 - 0.5);
    FillAndStroke;
  end;
end;

procedure TPDFRadioButton.Save;
var
  i: Integer;
begin
  FCheck := TPDFPage.Create(FOwner.FOwner);
  try
    FCheck.FIsForm := True;
    FUnCheck := TPDFPage.Create(FOwner.FOwner);
    try
      FUnCheck.FIsForm := True;
      Paint;
      FCheck.Save;
      FUnCheck.Save;
      for i := 0 to FCheck.FLinkedFont.Count - 1 do
        if FOwner.FOwner.FAcroForm.FFonts.IndexOf(FCheck.FLinkedFont[i]) = -1 then
          FOwner.FOwner.FAcroForm.FFonts.Add(FCheck.FLinkedFont[i]);
      FOwner.FOwner.StartObj(AnnotID);
      FOwner.FOwner.SaveToStream('/Type /Annot');
      FOwner.FOwner.SaveToStream('/Subtype /Widget');
      FOwner.FOwner.SaveToStream('/Rect [' + IntToStr(Round(FLeft)) + ' ' + IntToStr(FBottom) +
        ' ' + IntToStr(FRight) + ' ' + IntToStr(FTop) + ']');
      FOwner.FOwner.SaveToStream('/P ' + IntToStr(FOwner.PageID) + ' 0 R');
      if FChecked then FOwner.FOwner.SaveToStream('/AS /' + FExportValue) else FOwner.FOwner.SaveToStream('/AS /Off');
      FOwner.FOwner.SaveToStream('/MK <</CA (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, 'l')) + ') ', False);
      FOwner.FOwner.SaveToStream('/AC (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, '��')) + ')/RC (' +
        EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled, FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, '��')) + ')', False);
      FOwner.FOwner.SaveToStream('/BC [' + FormatFloat(GetRValue(FBorderColor) / 255) + ' ' +
        FormatFloat(GetGValue(FBorderColor) / 255) + ' ' + FormatFloat(GetBValue(FBorderColor) / 255) + ' ]', False);
      FOwner.FOwner.SaveToStream('/BG [' + FormatFloat(GetRValue(FColor) / 255) + ' ' +
        FormatFloat(GetGValue(FColor) / 255) + ' ' + FormatFloat(GetBValue(FColor) / 255) + ' ]', False);
      FOwner.FOwner.SaveToStream('>>');
      FOwner.FOwner.SaveToStream('/DA (' + EscapeSpecialChar(EnCodeString(FOwner.FOwner.FProtectionEnabled,
        FOwner.FOwner.FKey, FOwner.FOwner.FPKL, AnnotID, '/' + FFN + ' 0')) + ' Tf ' +
        FormatFloat(GetRValue(FFont.Color) / 255) + ' ' +
        FormatFloat(GetGValue(FFont.Color) / 255) + ' ' + FormatFloat(GetBValue(FFont.Color) / 255) + ' rg)');
      FOwner.FOwner.SaveToStream('/F ' + IntToStr(CalcFlags));
      FOwner.FOwner.SaveToStream('/Parent ' + IntToStr(FRG.GroupID) + ' 0 R');
      FOwner.FOwner.SaveToStream('/AP <</N << /' + FExportValue + ' ' + IntToStr(FCheck.PageID) + ' 0 R ', False);
      FOwner.FOwner.SaveToStream('/Off  ' + IntToStr(FUnCheck.PageID) + ' 0 R >> ', False);
      FOwner.FOwner.SaveToStream('/D << /' + FName + ' ' + IntToStr(FCheck.PageID) + ' 0 R ', False);
      FOwner.FOwner.SaveToStream('/Off  ' + IntToStr(FUnCheck.PageID) + ' 0 R >> ', False);
      FOwner.FOwner.SaveToStream('>>');
      FOwner.FOwner.SaveToStream('/H /T');
      FOwner.FOwner.SaveToStream(CalcActions + '>>');
      FOwner.FOwner.CloseHObj;
      FOwner.FOwner.CloseObj;
    finally
      FUnCheck.Free;
    end;
  finally
    FCheck.Free;
  end;
end;

procedure TPDFRadioButton.SetExportValue(const Value: string);
var
  I: Integer;
  WS: string;
begin
  WS := ReplStr(Value, ' ', '_');
  if WS = '' then
    if FExportValue <> '' then
      raise TPDFException.Create(SCannotSetEmptyExportValue) else
    FExportValue := FName + IntToStr(FRG.FButtons.Count) else
    for I := 0 to FRG.FButtons.Count - 1 do
      if UpperCase(TPDFRadioButton(FRG.FButtons[I]).FName) = UpperCase(WS) then
        raise TPDFException.Create(SExportValuePresent);
  FExportValue := WS;
end;

{ TPDFRadioGroup }

constructor TPDFRadioGroup.Create(AOwner: TPDFDocument; Name: string);
begin
  FOwner := AOwner;
  FName := Name;
  FButtons := TPDFControls.Create;
end;

destructor TPDFRadioGroup.Destroy;
begin
  FButtons.Free;
  inherited;
end;

procedure TPDFRadioGroup.Save;
var
  I: Integer;
begin
  FOwner.FAcroForm.FRadioGroups.Add(Self);
  FOwner.StartObj(GroupID);
  FOwner.SaveToStream('/FT /Btn');
  FOwner.SaveToStream('/T (' + EscapeSpecialChar(EnCodeString(FOwner.FProtectionEnabled, FOwner.FKey, FOwner.FPKL, GroupID, FName)) + ')');
  for I := 0 to FButtons.Count - 1 do
    if TPDFRadioButton(FButtons[I]).FChecked then
    begin
      FOwner.SaveToStream('/V /' + TPDFRadioButton(FButtons[I]).FExportValue);
      FOwner.SaveToStream('/DV /' + TPDFRadioButton(FButtons[I]).FExportValue);
      Break;
    end;
  FOwner.SaveToStream('/Kids [', False);
  for I := 0 to FButtons.Count - 1 do FOwner.SaveToStream(IntToStr(TPDFRadioButton(FButtons[I]).AnnotID) + ' 0 R ', False);
  FOwner.SaveToStream(']');
  I := 0;
  if TPDFRadioButton(FButtons[0]).FReadOnly then i := i or 1;
  if TPDFRadioButton(FButtons[0]).FRequired then i := i or 2;
  if FButtons.Count <> 1 then I := I or (1 shl 14);
  I := I or (1 shl 15);
  FOwner.SaveToStream('/Ff ' + IntToStr(I));
  FOwner.CloseHObj;
  FOwner.CloseObj;
end;

{$IFDEF LLPDFEVAL}

procedure InitTrial;
begin
  RC4Init(KeyData, @ComponentName[1], Length(ComponentName));
  SetLength(s1, SizeOf(sopp1));
  RC4Reset(KeyData);
  RC4Crypt(KeyData, @sopp1[1], @s1[1], SizeOf(sopp1));
  SetLength(s2, SizeOf(sopp2));
  RC4Reset(KeyData);
  RC4Crypt(KeyData, @sopp2[1], @s2[1], SizeOf(sopp2));
  SetLength(s3, SizeOf(sopp3));
  RC4Reset(KeyData);
  RC4Crypt(KeyData, @sopp3[1], @s3[1], SizeOf(sopp3));
  RC4Burn(KeyData);
end;
{$ENDIF}

initialization
  begin
{$IFDEF LLPDFEVAL}
    InitTrial;
{$ENDIF}
  end;
end.

