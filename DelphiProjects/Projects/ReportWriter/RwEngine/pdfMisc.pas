{**************************************************}
{                                                  }
{                   llPDFLib                       }
{            Version  2.3,  10.04.2003             }
{      Copyright (c) 2002, 2003 llionsoft          }
{             All rights reserved                  }
{            mailto:info@llion.net                 }
{                                                  }
{**************************************************}
{$I pdf.inc}

unit pdfMisc;

interface
uses windows, sysutils, graphics, classes;
type
  PByteArray = ^TByteArray;
  TByteArray = array[0..MaxInt - 1] of Byte;

  TTextCTM = record
    a, b, c, d, x, y: Extended;
  end;

  TWidthArray = array[0..255] of Word;
  TLargeWidthArray = array[0..255] of Cardinal;

  TPDFJSFunction = class
  private
    FID: Integer;
    FBody: string;
    FName: string;
    FParams: string;
    procedure SetBody(const Value: string);
    procedure SetID(const Value: Integer);
    procedure SetName(const Value: string);
    procedure SetParams(const Value: string);
  public
    property Name: string read FName write SetName;
    property Body: string read FBody write SetBody;
    property Params: string read FParams write SetParams;
    property ID: Integer read FID write SetID;
  end;

  TPDFJSFList = class
  private
    FList:TList;
    function GetItems(Index: Integer): TPDFJSFunction;
    procedure SetItems(Index: Integer; const Value: TPDFJSFunction);
  public
  constructor Create;
  destructor Destroy;override;
  function Add(Name,Params,Body:string):Integer;
  function Count:Integer;
  procedure Clear;
  property Items[Index:Integer]:TPDFJSFunction read GetItems write SetItems; default;
  end;


procedure swp(var A, B: Integer); overload;
procedure swp(var A, B: Extended); overload;
function IsTrueType(FontName: string): Boolean;
function ByteToHex(B: Byte): string;
function WordToHex(W: Word): string;
function FormatFloat(Value: Extended): string;
procedure MultiplyCTM(var T: TTextCTM; const S: TTextCTM);
function StrToOctet(st: string): string;
function ByteToOct(B: Byte): string;
implementation

function ByteToOct(B: Byte): string;
begin
  Result := '';
  while B > 7 do
  begin
    Result := IntToStr(B mod 8) + Result;
    b := b div 8;
  end;
  Result := IntToStr(b) + Result;
end;

function StrToOctet(st: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Length(st) do
    Result := Result + '\' + ByteToOct(Ord(st[i]));
end;

procedure MultiplyCTM(var T: TTextCTM; const S: TTextCTM);
var
  T1: TTextCTM;
begin
  Move(T, T1, SizeOf(T));
  T.a := S.a * T1.a + S.b * T1.c;
  T.b := S.a * T1.b + S.b * T1.d;
  T.c := S.c * T1.a + S.d * T1.c;
  T.d := S.c * T1.b + S.d * T1.d;
  T.x := S.x * T1.a + S.y * T1.c + T1.x;
  T.y := S.x * T1.b + S.y * T1.d + T1.y;
end;

function ByteToHex(B: Byte): string;
const
  H: string[16] = '0123456789ABCDEF';
begin
  Result := H[1 + b shr 4] + H[1 + b and $F];
end;

function WordToHex(W: Word): string;
begin
  Result := ByteToHex(Hi(W)) + ByteToHex(Lo(W))
end;

function FormatFloat(Value: Extended): string;
var
  c: Char;
begin
  c := DecimalSeparator;
  DecimalSeparator := '.';
  Result := SysUtils.FormatFloat('0.####', Value);
  DecimalSeparator := c;
end;


function IsTrueType(FontName: string): Boolean;
var
  LF: TLogFont;
  TT: Boolean;
  DC: HDC;
  function Check(const Enum: ENUMLOGFONTEX; const PFD: TNEWTEXTMETRICEXA; FT: DWORD; var TT: Boolean): Integer; stdcall;
  begin
    TT := (FT = TRUETYPE_FONTTYPE);
    Result := 1;
  end;
begin
  if FontName = '' then
  begin
    Result := False;
    Exit;
  end;
  FillChar(LF, SizeOf(LF), 0);
  LF.lfCharSet := DEFAULT_CHARSET;
  Move(FontName[1], LF.lfFaceName, Length(FontName));
  DC := GetDC(0);
  try
    EnumFontFamiliesEx(DC, LF, @Check, Integer(@TT), 0);
  finally
    ReleaseDC(0, DC);
  end;
  Result := tt;
end;


procedure swp(var A, B: Integer); overload;
var
  C: Integer;
begin
  C := A;
  A := B;
  B := C;
end;

procedure swp(var A, B: Extended); overload;
var
  C: Extended;
begin
  C := A;
  A := B;
  B := C;
end;


{ TPDFJSFunction }

procedure TPDFJSFunction.SetBody(const Value: string);
begin
  FBody := Value;
end;

procedure TPDFJSFunction.SetID(const Value: Integer);
begin
  FID := Value;
end;

procedure TPDFJSFunction.SetName(const Value: string);
begin
  FName := Value;
end;

procedure TPDFJSFunction.SetParams(const Value: string);
begin
  FParams := Value;
end;

{ TPDFJSFList }

function TPDFJSFList.Add(Name, Params, Body: string): Integer;
var
 F:TPDFJSFunction;
begin
 F:=TPDFJSFunction.Create;
 F.FBody:=Body;
 F.FName:=Name;
 F.FParams:=Params;
 Result:=FList.Add(Pointer(F));
end;

procedure TPDFJSFList.Clear;
var
I:Integer;
begin
 for I:=0 to FList.Count-1 do
   TPDFJSFunction(FList[i]).Free;
   FList.Clear;
end;

function TPDFJSFList.Count: Integer;
begin
  Result:=FList.Count;
end;

constructor TPDFJSFList.Create;
begin
  FList:=TList.Create;
end;

destructor TPDFJSFList.Destroy;
begin
  FList.Free;
  inherited;
end;

function TPDFJSFList.GetItems(Index: Integer): TPDFJSFunction;
begin
  Result:=TPDFJSFunction(FList[Index]);
end;

procedure TPDFJSFList.SetItems(Index: Integer;
  const Value: TPDFJSFunction);
begin
  TPDFJSFunction(FList[Index]).FID:=Value.FID;
  TPDFJSFunction(FList[Index]).FBody:=Value.FBody;
  TPDFJSFunction(FList[Index]).FName:=Value.FName;
  TPDFJSFunction(FList[Index]).FParams:=Value.FParams;
end;

end.

