object rwPassword: TrwPassword
  Left = 588
  Top = 339
  ActiveControl = edPass
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Access password'
  ClientHeight = 91
  ClientWidth = 232
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 5
    Width = 107
    Height = 13
    Caption = 'Please input Password'
  end
  object OK: TButton
    Left = 157
    Top = 58
    Width = 70
    Height = 23
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object Cancel: TButton
    Left = 75
    Top = 58
    Width = 70
    Height = 23
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object edPass: TEdit
    Left = 5
    Top = 25
    Width = 222
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 0
  end
end
