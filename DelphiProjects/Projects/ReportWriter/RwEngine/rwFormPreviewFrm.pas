// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwFormPreviewFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, rwOneRepPreviewFrm, Registry, rwPreviewUtils, rwTypes, rwPrinters,
  rwBasicPrint, BlockCiphers, rwEngineTypes, EvStreamUtils, CoolTrayIcon,
  Menus, rwBasicUtils, isUtils, rwConnectionInt;

const
  CM_SET_BOUNDS_EXT = WM_USER + 1;

type
  TrwFormPreview = class(TForm)
    Timer1: TTimer;
    Preview: TrwOneRepPreview;
    PopupMenu: TPopupMenu;
    Close1: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PreviewactOpenExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Close1Click(Sender: TObject);
  private
    FCaption: String;
    FEmbedded: Boolean;
    FRunAsCOM: Boolean;

    procedure UpdateCaption;
    procedure CMSetBoundExt(var Message: TMessage); message CM_SET_BOUNDS_EXT;

  protected
    procedure CreateParams(var Params: TCreateParams); override;

  public
    function SetFocusedControl(Control: TWinControl): Boolean; override;

    property  RunAsCOM: Boolean read FRunAsCOM write FRunAsCOM;

    procedure ShowWithOpenDialog;
    procedure EmbedInto(AParent: HWND);
    procedure SetBoundsExt(const ABounds: TrwBounds);
  end;

  TrwThreadPrintInfoRec = record
    FileName:     String;
    Password:     String;
    ErrorFile:    String;
    PrintJobInfo: TrwPrintJobInfoRec;
  end;

  PTrwThreadPrintInfoRec = ^TrwThreadPrintInfoRec;

  procedure RegisterDefaultApp;
  procedure Preview;

  procedure ModalPreview(AFileName: String = ''); overload;
  procedure ModalPreview(const pPreviewInfo: PTrwPreviewInfoRec); overload;
  procedure PrintFromParams(const pThreadPrintInfo: PTrwThreadPrintInfoRec);

  procedure MakeParamFile(const AFileName: String; const pThreadPrintInfo: PTrwThreadPrintInfoRec);
  procedure ReadParamsFromFile(const AFileName: String; const pThreadPrintInfo: PTrwThreadPrintInfoRec);

  var PreviewMainForm: TrwFormPreview = nil;

implementation

uses
  isBasicUtils{WM_USER_NOTIFY_SETFOCUS};
{$R *.DFM}

procedure MakeParamFile(const AFileName: String; const pThreadPrintInfo: PTrwThreadPrintInfoRec);
var
  F:  IEvDualStream;
begin
  F := TEvDualStreamHolder.CreateFromNewFile(AFileName);
  F.WriteString(pThreadPrintInfo^.FileName);
  F.WriteString(pThreadPrintInfo^.Password);
  F.WriteString(pThreadPrintInfo^.ErrorFile);
  with pThreadPrintInfo^.PrintJobInfo do
  begin
    F.WriteString(PrintJobName);
    F.WriteString(PrinterName);
    F.WriteString(TrayName);
    F.WriteInteger(OutBinNbr);
    F.WriteInteger(Ord(Duplexing));
    F.WriteBoolean(PrintPCLAsGraphic);
    F.WriteInteger(PrinterVerticalOffset);
    F.WriteInteger(PrinterHorizontalOffset);
    F.WriteInteger(FirstPage);
    F.WriteInteger(LastPage);
    F.WriteInteger(Integer((@Layers)^));
    F.WriteInteger(Copies);
  end;
end;

procedure ReadParamsFromFile(const AFileName: String; const pThreadPrintInfo: PTrwThreadPrintInfoRec);
var
  F: IEvDualStream;
begin
  Initialize(pThreadPrintInfo^);
  F := TEvDualStreamHolder.CreateFromFile(AFileName);
  pThreadPrintInfo^.FileName := F.ReadString;  
  pThreadPrintInfo^.Password := F.ReadString;  
  pThreadPrintInfo^.ErrorFile := F.ReadString;
  with pThreadPrintInfo^.PrintJobInfo do
  begin
    PrintJobName := F.ReadString;
    PrinterName := F.ReadString;
    TrayName := F.ReadString;
    OutBinNbr := F.ReadInteger;
    Duplexing := TrwDuplexPrintType(F.ReadInteger);
    PrintPCLAsGraphic := F.ReadBoolean;
    PrinterVerticalOffset := F.ReadInteger;
    PrinterHorizontalOffset := F.ReadInteger;
    FirstPage := F.ReadInteger;
    LastPage := F.ReadInteger;
    Layers := TrwPrintableLayers(F.ReadInteger);
    Copies :=  F.ReadInteger;
  end;
end;

procedure PrintFromParams(const pThreadPrintInfo: PTrwThreadPrintInfoRec);
var
  FS: TEvFileStream;
begin
  FS := TEvFileStream.Create(pThreadPrintInfo^.FileName, fmOpenRead);
  try
    PrintReport(FS, nil, @(pThreadPrintInfo^.PrintJobInfo), nil);
  finally
    FS.Free;
  end;
end;


procedure RegisterDefaultApp;
var
  TempRegistry: TRegistry;
begin
  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      try
        RootKey := HKEY_CLASSES_ROOT;
        OpenKey('.rwa', True);
        WriteString('', 'EvRwPreview');
        CloseKey;
        OpenKey('EvRwPreview', True);
        WriteString('', 'Evolution Report Writer Result');
        CloseKey;
        OpenKey('EvRwPreview\shell\open\command', True);
        WriteString('', Application.ExeName + ' "%1"');
      except
        // it's done in case if user is not administrator
      end;
    end;
  finally
    TempRegistry.Free;
  end;
end;


procedure Preview;
var
  pPreviewInfo: PTrwPreviewInfoRec;
  tmpList : TStringList;
  i : integer;
begin
  Application.CreateForm(TrwFormPreview, PreviewMainForm);
  PreviewMainForm.Preview.AutoConvertOldFormat := True;
  PreviewMainForm.Preview.StandAloneUtilityMode := True;

  if ParamCount > 0 then
  begin
    try
      if (ParamCount < 2) then
      begin
        PreviewMainForm.Preview.PreviewReport(ParamStr(1));
      end
      else
      begin
        tmpList := TStringList.Create;
        try
          for i := 1 to ParamCount do
            tmpList.Add(ParamStr(i));
          pPreviewInfo := PreviewMainForm.Preview.PreparePreviewInfo(tmpList);
          PreviewMainForm.Preview.PreviewReport(pPreviewInfo, true);
        finally
          tmpList.Free;
        end;
      end;
    except
      on E: Exception do
      begin
        PreviewMainForm.Tag := 1;
        PreviewMainForm.Preview.PreviewReport('');
        PrvMessage(E.Message, mtError, [mbOk]);
      end;
    end
  end

  else
  begin
    PreviewMainForm.Tag := 1;
    PreviewMainForm.Preview.PreviewReport('');
    Application.MainForm.Hide;
    PreviewMainForm.Show;
  end;
end;

procedure ModalPreview(AFileName: String = '');
var
  F: TrwFormPreview;
begin
  F := TrwFormPreview.Create(Application);
  try
    F.Preview.PreviewReport(AFileName);
    F.ShowModal;
  finally
    F.Free;
  end;
end;


procedure ModalPreview(const pPreviewInfo: PTrwPreviewInfoRec); overload;
var
  F: TrwFormPreview;
begin
  F := TrwFormPreview.Create(Application);
  try
    F.Preview.PreviewReport(pPreviewInfo);
    F.ShowModal;
  finally
    F.Free;
  end;
end;



{ TrwFormPreview }

procedure TrwFormPreview.ShowWithOpenDialog;
begin
  Tag := 1;
  Preview.PreviewReport('');
  ShowModal;
end;

procedure TrwFormPreview.FormShow(Sender: TObject);
begin
  if Tag = 1 then
    Timer1.Enabled := True;
  UpdateCaption;
end;

procedure TrwFormPreview.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  Preview.actOpen.Execute;
end;

procedure TrwFormPreview.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Preview.BeforeClosePreview;

  if FRunAsCOM then
  begin
    Action := caHide;
//    HideAppFromTaskBar;
  end
  else
    Action := caFree;
end;

procedure TrwFormPreview.PreviewactOpenExecute(Sender: TObject);
begin
  Preview.actOpenExecute(Sender);
  UpdateCaption;
end;

procedure TrwFormPreview.FormCreate(Sender: TObject);
begin
  FCaption := Caption;
end;

procedure TrwFormPreview.UpdateCaption;
begin
  Caption := FCaption;
  if Preview.FileName <> '' then
    Caption := Caption + ' "' + Preview.FileName + '"';
end;

procedure TrwFormPreview.EmbedInto(AParent: HWND);
begin
  if AParent <> 0 then
  begin
    FEmbedded := True;
    RecreateWnd;
    HideAppFromTaskBar;
    Visible := True;
    Windows.SetParent(Handle, AParent);
  end

  else
  begin
    FEmbedded := False;
    Parent := nil;
    Visible := False;
    Width := 800;
    Height := 560;
    Left := (Screen.Width - Width) div 2;
    Top := (Screen.Height - Height) div 2;
    WindowState := wsMaximized;
    DestroyWindowHandle;
//    ShowAppOnTaskBar;
    ShowModal;
  end;
end;

procedure TrwFormPreview.CreateParams(var Params: TCreateParams);
begin
  inherited;

  if FEmbedded then
    with Params do
      Style := WS_VISIBLE or WS_TABSTOP or WS_CHILD;
end;

procedure TrwFormPreview.Close1Click(Sender: TObject);
begin
  Application.Terminate;
end;

function TrwFormPreview.SetFocusedControl(Control: TWinControl): Boolean;
var
  p: HWND;
begin
  if HandleAllocated then
  begin
    p := windows.GetParent(Handle);
    if p <> 0 then
      PostMessage(p, WM_USER_NOTIFY_SETFOCUS, 0, 0);
  end;
  Result := inherited SetFocusedControl(Control);
end;

procedure TrwFormPreview.SetBoundsExt(const ABounds: TrwBounds);
begin
  if HandleAllocated then
    PostMessage(Handle, CM_SET_BOUNDS_EXT,
      MakeWParam(ABounds.Left, ABounds.Top), MakeLParam(ABounds.Width, ABounds.Height));
end;

procedure TrwFormPreview.CMSetBoundExt(var Message: TMessage);
begin
  SetBounds(Message.WParamLo, Message.WParamHi, Message.LParamLo, Message.LParamHi);
end;

initialization
  DisableProcessWindowsGhosting;

end.

