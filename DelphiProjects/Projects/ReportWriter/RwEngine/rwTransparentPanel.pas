unit rwTransparentPanel;

interface

uses Messages, Classes, Controls, Windows, Graphics, ExtCtrls,
     Themes, Forms;

const
  RW_PARENTPOSCHANGED = WM_USER + 100;

type
  TrwTransparentPanel = class(TCustomPanel)
  private
    FTransparent: Boolean;
    FDrawing: Boolean;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure WMEraseBkgnd(var Message: TWmEraseBkgnd); message WM_ERASEBKGND;
    procedure PaintHandler(var Message: TWMPaint);
    procedure PaintControls(DC: HDC);
    procedure SetTransparent(const Value: Boolean);
    procedure WMWindowPosChanged(var Message: TWMWindowPosChanged); message WM_WINDOWPOSCHANGED;
    procedure RwParentPosChanged(var Message: TMessage); message RW_PARENTPOSCHANGED;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure Paint; override;
    procedure PaintWindow(DC: HDC); override;
    procedure DoOnParentPosChanged; virtual;
  public
    property DockManager;
    constructor Create(AOwner: TComponent); override;
  published
    property Transparent: Boolean read FTransparent write SetTransparent;
    property Align;
    property Alignment;
    property Anchors;
    property AutoSize;
    property BevelInner;
    property BevelOuter;
    property BevelWidth;
    property BiDiMode;
    property BorderWidth;
    property BorderStyle;
    property Caption;
    property Color;
    property Constraints;
    property Ctl3D;
    property UseDockManager default True;
    property DockSite;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property FullRepaint;
    property Font;
    property Locked;
    property ParentBiDiMode;
    property ParentBackground;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnCanResize;
    property OnClick;
    property OnConstrainedResize;
    property OnContextPopup;
    property OnDockDrop;
    property OnDockOver;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGetSiteInfo;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property OnStartDock;
    property OnStartDrag;
    property OnUnDock;
  end;

implementation

{ TrwTransparentPanel }

constructor TrwTransparentPanel.Create(AOwner: TComponent);
begin
  inherited;
  ControlStyle := [csAcceptsControls, csCaptureMouse, csClickEvents,
                   csDoubleClicks, csReplicatable];
  FDrawing := False;
  FTransparent := True;
end;

procedure TrwTransparentPanel.CreateParams(var Params: TCreateParams);
begin
  inherited;
  if Transparent then
  begin
    Params.ExStyle := Params.ExStyle or WS_EX_TRANSPARENT;
    ControlStyle := ControlStyle - [csOpaque];
  end
  else
  begin
    Params.ExStyle := Params.ExStyle and not WS_EX_TRANSPARENT;
    ControlStyle := ControlStyle + [csOpaque];
  end;
end;

procedure TrwTransparentPanel.DoOnParentPosChanged;
begin
  NotifyControls(RW_PARENTPOSCHANGED);
end;

procedure TrwTransparentPanel.Paint;
const
  Alignments: array[TAlignment] of Longint = (DT_LEFT, DT_RIGHT, DT_CENTER);
var
  Rect: TRect;
  TopColor, BottomColor: TColor;
  FontHeight: Integer;
  Flags: Longint;

  procedure AdjustColors(Bevel: TPanelBevel);
  begin
    TopColor := clBtnHighlight;
    if Bevel = bvLowered then TopColor := clBtnShadow;
    BottomColor := clBtnShadow;
    if Bevel = bvLowered then BottomColor := clBtnHighlight;
  end;

begin
  Rect := GetClientRect;
  if BevelOuter <> bvNone then
  begin
    AdjustColors(BevelOuter);
    Frame3D(Canvas, Rect, TopColor, BottomColor, BevelWidth);
  end;
  Frame3D(Canvas, Rect, Color, Color, BorderWidth);
  if BevelInner <> bvNone then
  begin
    AdjustColors(BevelInner);
    Frame3D(Canvas, Rect, TopColor, BottomColor, BevelWidth);
  end;
  with Canvas do
  begin
    if not Transparent and (not ThemeServices.ThemesEnabled or not ParentBackground) then
    begin
      Brush.Color := Color;
      FillRect(Rect);
    end;
    Brush.Style := bsClear;
    Font := Self.Font;
    FontHeight := TextHeight('W');
    with Rect do
    begin
      Top := ((Bottom + Top) - FontHeight) div 2;
      Bottom := Top + FontHeight;
    end;
    Flags := DT_EXPANDTABS or DT_VCENTER or Alignments[Alignment];
    Flags := DrawTextBiDiModeFlags(Flags);
    DrawText(Handle, PChar(Caption), -1, Rect, Flags);
  end;
end;

procedure TrwTransparentPanel.PaintControls(DC: HDC);
var
  i, SaveIndex: Integer;
  FrameBrush: HBRUSH;
begin
  for i := 0 to ControlCount - 1 do
  begin
    with Controls[i] do
      if Visible and RectVisible(DC, Rect(Left, Top, Left + Width, Top + Height)) then
      begin
        SaveIndex := SaveDC(DC);
        MoveWindowOrg(DC, Left, Top);
        IntersectClipRect(DC, 0, 0, Width, Height);
        Perform(WM_PAINT, Integer(DC), 0);
        RestoreDC(DC, SaveIndex);
      end;
  end;

  for i := 0 to ControlCount - 1 do
    if Controls[i] is TWinControl then
      with TWinControl(Controls[i]) do
        if Ctl3D and (csFramed in ControlStyle) and
          (Visible or (csDesigning in ComponentState) and
          not (csNoDesignVisible in ControlStyle)) then
        begin
          FrameBrush := CreateSolidBrush(ColorToRGB(clBtnShadow));
          FrameRect(DC, Rect(Left - 1, Top - 1, Left + Width, Top + Height),
            FrameBrush);
          DeleteObject(FrameBrush);
          FrameBrush := CreateSolidBrush(ColorToRGB(clBtnHighlight));
          FrameRect(DC, Rect(Left, Top, Left + Width + 1, Top + Height + 1),
            FrameBrush);
          DeleteObject(FrameBrush);
        end;
end;

procedure TrwTransparentPanel.PaintHandler(var Message: TWMPaint);
var
  SaveIndex, Clip: Integer;
  DC: HDC;
  PS: TPaintStruct;

  procedure CheckChildren(AParent: TWinControl);
  var
    i: Integer;
    R: TRect;

    function FindIntersection(AParent: TWinControl; var ARect: TRect): Boolean;
    var
      R1, R2: TRect;
    begin
      R1 := AParent.ClientRect;
      Result := IntersectRect(R2, R1, ARect);
      ARect := R2;
      if Result and (AParent <> Self) then
      begin
        ARect.TopLeft := AParent.ClientToScreen(ARect.TopLeft);
        ARect.BottomRight := AParent.ClientToScreen(ARect.BottomRight);
        ARect.TopLeft := AParent.Parent.ScreenToClient(ARect.TopLeft);
        ARect.BottomRight := AParent.Parent.ScreenToClient(ARect.BottomRight);
        Result := FindIntersection(AParent.Parent, ARect);
      end;
    end;

  begin
    for i := 0 to AParent.ControlCount - 1 do
      with AParent.Controls[i] do
        if Visible then
        begin
          if (csOpaque in ControlStyle) then
          begin
            R := BoundsRect;
            if FindIntersection(AParent, R) then
              Clip := ExcludeClipRect(DC, R.Left, R.Top, R.Right, R.Bottom);
          end
          else if AParent.Controls[i] is TWinControl then
            CheckChildren(TWinControl(AParent.Controls[i]));
          if Clip = NullRegion then
            break;
      end;
  end;

begin
  DC := Message.DC;
  if DC = 0 then
    DC := BeginPaint(Handle, PS);

  try
    if ControlCount = 0 then
      PaintWindow(DC)

    else
    begin
      SaveIndex := SaveDC(DC);
      try
        Clip := SimpleRegion;
//        CheckChildren(Self); bug in here! This transparent panel sucks(because of VCL restrictions).
        if Clip <> NullRegion then
          PaintWindow(DC);
      finally
        RestoreDC(DC, SaveIndex);
      end;
    end;

    PaintControls(DC);

//    DoForegroundPaint(DC);

  finally
    if Message.DC = 0 then
      EndPaint(Handle, PS);
  end;
end;

procedure TrwTransparentPanel.PaintWindow(DC: HDC);
var
  OldHandle: THandle;
begin
  Canvas.Lock;
  try
    OldHandle := Canvas.Handle;
    Canvas.Handle := DC;
    try
      TControlCanvas(Canvas).UpdateTextFlags;
      Paint;
    finally
      Canvas.Handle := OldHandle;
    end;
  finally
    Canvas.Unlock;
  end;
end;

procedure TrwTransparentPanel.RwParentPosChanged(var Message: TMessage);
begin
  DoOnParentPosChanged;
end;

procedure TrwTransparentPanel.SetTransparent(const Value: Boolean);
begin
  FTransparent := Value;
  RecreateWnd;
end;

var
  FDrawingBackground: Boolean = False;

procedure TrwTransparentPanel.WMEraseBkgnd(var Message: TWmEraseBkgnd);
var
  R: TRect;
  P: TWinControl;
begin
  if Transparent then
  begin
    if not FDrawingBackground and not FDrawing then
    begin
      FDrawingBackground := True;
      FDrawing := True;
      try
        P := Parent;
//        while Assigned(P) and not (csOpaque in P.ControlStyle) do
//          P := P.Parent;
        GetClipBox(Message.DC, R);
        R.TopLeft := ClientToScreen(R.TopLeft);
        R.BottomRight := ClientToScreen(R.BottomRight);
        R.TopLeft := P.ScreenToClient(R.TopLeft);
        R.BottomRight := P.ScreenToClient(R.BottomRight);
        InvalidateRect(P.Handle, @R, True);
      finally
        FDrawing := False;
        FDrawingBackground := False;
      end;
    end;
    Message.Result := 0;
  end
  else
  begin
    Canvas.Brush.Color := Color;
    Canvas.Brush.Style := bsSolid;
    Message.Result := 1;
  end;
end;

procedure TrwTransparentPanel.WMPaint(var Message: TWMPaint);
var
  DC, MemDC: HDC;
  MemBitmap, OldBitmap: HBITMAP;
  PS: TPaintStruct;
begin
  if FDrawing then
    Exit;

  if not DoubleBuffered or (Message.DC <> 0) then
    PaintHandler(Message)

  else
  begin
    DC := GetDC(0);
    MemBitmap := CreateCompatibleBitmap(DC, ClientRect.Right, ClientRect.Bottom);
    ReleaseDC(0, DC);
    MemDC := CreateCompatibleDC(0);
    OldBitmap := SelectObject(MemDC, MemBitmap);
    try
      DC := BeginPaint(Handle, PS);
      Perform(WM_ERASEBKGND, MemDC, MemDC);
      Message.DC := MemDC;
      WMPaint(Message);
      Message.DC := 0;
      BitBlt(DC, 0, 0, ClientRect.Right, ClientRect.Bottom, MemDC, 0, 0, SRCCOPY);
      EndPaint(Handle, PS);
    finally
      SelectObject(MemDC, OldBitmap);
      DeleteDC(MemDC);
      DeleteObject(MemBitmap);
    end;
  end;
end;

procedure TrwTransparentPanel.WMWindowPosChanged(var Message: TWMWindowPosChanged);
begin
  inherited;
  NotifyControls(RW_PARENTPOSCHANGED);  
end;

end.
