unit rwRegister;

interface

uses Classes, rwBasicClasses, rwDB, rwCommonClasses, rwReportControls, rwReport,
     rwPrintPage, rwInputFormControls, rwTypes, rwPrintElements, Graphics, rwDataDictionary,
     rwUtils, rwQBInfo, rmReport, rwGraphics, rmCommonClasses, rmPrintFormControls,
     rmPrintTable, rmFormula, Jpeg, rmASCIIFormControls, rmXMLFormControls, rmXMLBuiltInTypes;

procedure RegRWClasses;

implementation

procedure RegRWClasses;
begin
  RegisterRWClasses([TrwComponent, TrwNoVisualComponent],
                    ['RW Object', 'Non-Visual Object' ]);

  RegisterRWClasses([TrwLabel, TrwDBText, TrwSysLabel, TrwShape, TrwContainer, TrwImage, TrwDBImage,
                     TrwFrame, TrwPCLLabel, TrwBarcode1D, TrwBarcodePDF417],
                    ['Label', 'DataBase Label', 'System Info Label', 'Shape', 'Container', 'Picture', 'DataBase Picture',
                     'Frame', 'PCL Label', 'Barcode 1D', 'Barcode PDF417']);

  RegisterRWClasses([TrmTable, TrmTableCell, TrmDBTableBand, TrmDBTableHeaderBand, TrmDBTableDetailBand, TrmDBTableTotalBand,
                     TrmDBTableGroupHeaderBand, TrmDBTableGroupFooterBand,
                     TrmDBTable, TrmDBTableQuery,
                     TrmASCIIItem, TrmASCIINode, TrmASCIIRecord, TrmASCIIField],
                    ['Table', 'Table Cell', 'DataBase Table Band', 'Header Band', 'Detail Band', 'Total Band',
                     'Group Header Band', 'Group Footer Band',
                     'DataBase Table', 'DataBase Table Query',
                     'ASCII Object', 'ASCII Node Object', 'ASCII Record', 'ASCII Field']);

  RegisterRWClasses([TrwPage, TrwGroup, TrwInputFormContainer,
                     TrwCustomReport, TrwReport, TrwSubReport, TrmReport, TrmPrintForm, TrmASCIIForm],
                    ['Page', 'Group', 'Input Form', 'Custom Report', 'Report', 'SubReport',
                     'Report', 'Print Form', 'ASCII Form']);

  RegisterRWClasses([TrmXMLItem, TrmXMLNode, TrmXMLForm,
                     TrmXMLSimpleType, TrmXMLSimpleTypeByRestriction, TrmXMLSimpleTypeByList, TrmXMLSimpleTypeByUnion,
                     TrmXMLattribute, TrmXMLattributeGroup, TrmXMLgroup, TrmXMLsequence, TrmXMLchoice, TrmXMLelement,
                     TrmXMLduration, TrmXMLdateTime, TrmXMLtime, TrmXMLdate, TrmXMLgYearMonth, TrmXMLgYear,
                     TrmXMLgMonthDay, TrmXMLgDay, TrmXMLgMonth, TrmXMLstring, TrmXMLBoolean, TrmXMLBase64Binary,
                     TrmXMLhexBinary, TrmXMLfloat, TrmXMLdecimal, TrmXMLdouble, TrmXMLanyURI, TrmXMLQName,
                     TrmXMLNotation, TrmXMLnormalizedString, TrmXMLinteger, TrmXMLtoken, TrmXMLlanguage,
                     TrmXMLName, TrmXMLNCName, TrmXMLID, TrmXMLIDREF, TrmXMLENTITY, TrmXMLnonPositiveInteger,
                     TrmXMLnegativeInteger, TrmXMLlong, TrmXMLint, TrmXMLshort, TrmXMLbyte, TrmXMLnonNegativeInteger,
                     TrmXMLpositiveInteger, TrmXMLunsignedLong, TrmXMLunsignedInt, TrmXMLunsignedShort,
                     TrmXMLunsignedByte, TrmXMLfragment, TrmXMLFragmentValue],
                    ['XML Object', 'XML Node Object', 'XML Form',
                     'XML type anySimpleType', 'XML Simple Type By Restriction', 'XML Simple Type By List', 'XML Simple Type By Union',
                     'XML type attribute', 'XML type attributeGroup', 'XML type group', 'XML element sequence', 'XML element choice', 'XML element',
                     'XML type duration', 'XML type dateTime', 'XML type time', 'XML type date', 'XML type gYearMonth', 'XML type gYear',
                     'XML type gMonthDay', 'XML type gDay', 'XML type gMonth', 'XML type string', 'XML type Boolean', 'XML type Base64Binary',
                     'XML type hexBinary', 'XML type float', 'XML type decimal', 'XML type double', 'XML type anyURI', 'XML type QName',
                     'XML type Notation', 'XML type normalizedString', 'XML type integer', 'XML type token', 'XML type language',
                     'XML type Name', 'XML type NCName', 'XML type ID', 'XML type IDREF', 'XML type ENTITY', 'XML type nonPositiveInteger',
                     'XML type negativeInteger', 'XML type long', 'XML type int', 'XML type short', 'XML type byte', 'XML type nonNegativeInteger',
                     'XML type positiveInteger', 'XML type unsignedLong', 'XML type unsignedInt', 'XML type unsignedShort',
                     'XML type unsignedByte', 'XML fragment', 'XML value of fragment']);



  RegisterRWClasses([TrwText, TrwEdit, TrwCheckBox, TrwPanel, TrwDateEdit, TrwGroupBox,
                     TrwRadioGroup, TrwComboBox, TrwDBGrid, TrwDBComboBox, TrwPageControl,
                     TrwTabSheet, TrwButton, TrwFileDialog],
                    ['Label', 'Edit Box', 'Check Box', 'Container', 'Date Time Edit Box', 'Group Box',
                     'Radio Group Box', 'Combo Box', 'Grid', 'DataBase Combo Box', 'Tabs Control',
                     'Tab Sheet', 'Button', 'File Open/Save Dialog']);

  RegisterRWClasses([TrwDataSet, TrwQuery, TrwBuffer, TrwFields, TrwField, TrwDictionary,
                     TrwCustomObject, TrwLibComponents, TrwLibComponent, TrwLibFunctions,
                     TrwLibFunction],
                    ['Data Set', 'Query', 'Temporary Table', 'Fields', 'Field', 'Data Dictionary',
                     'User Object', 'Library of Components', 'Library Component', 'Library of Functions',
                     'Library Function']);

  RegisterRWClasses([TrwBand, TrwControl, TrwPrintableControl, TrwChildComponent,
                     TrwPrintableContainer, TrwFramedContainer, TrwCustomText, TrwFormControl,
                     TrwWinFormControl, TrwParamFormControl, TrmPrintControl, TrmPrintContainer,
                     TrmPrintText, TrmPrintPanel, TrmPrintImage, TrmPrintPageNumber],
                    ['Band', 'Control', 'Printable Object', 'Printable Child Object',
                     'Printable Container', 'Framed Container', 'Printable Label', 'Input Form Object',
                     'Input Form Container', 'Input Form Parameter Object',
                     'Print Form Object', 'Print Form Container', 'Print Form Text', 'Print Panel', 'Print Image',
                     'Page Number']);

  RegisterRWClasses([TrwStrings, TFont, TrwFont, TrwCollection, TrwCollectionItem, TrwPCLFile, TrwQBQuery,
                     TrwQBShowingFields, TrwQBShowingField, TrwQBSelectedTables, TrwQBSelectedTable,
                     TrwQBExpression, TrwPicture],
                    ['List of Strings', 'Font', 'Font', 'Collection of Items', 'Collection Item', 'PCL Data', 'Query Builder Data',
                     'Query Builder Result Fields', 'Query Builder Result Field',
                     'Query Builder Selected Tables', 'Query Builder Selected Table', 'Query Builder Expression',
                     'Picture']);

  RegisterClasses([TrwGlobalVarFunct, TrwReportForm, TrwQBConsts, TrwQBConst,
                   TrmFMLFormulas, TrmFMLFormula, TrmFMLFormulaContent, TrmFMLFormulaItem,
                   TrmFMLAggrFunctHolder, TBitmap, TMetafile, TIcon, TJPEGImage]);
end;


end.
