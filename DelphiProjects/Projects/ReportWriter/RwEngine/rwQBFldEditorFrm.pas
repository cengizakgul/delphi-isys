unit rwQBFldEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwQBInfo, ExtCtrls, rwDataDictionary, rwUtils, comctrls,
  rwQBExpressionFrm, rwTypes, Menus, rwEngineTypes;

type

  TrwQBFldEditor = class(TForm)
    pnlField: TrwQBExpress;
    btnCancel: TButton;
    btnOK: TButton;
    pnlTmpFld: TPanel;
    Label5: TLabel;
    cbTmpFlds: TComboBox;
    pnlAlias: TPanel;
    lAlias: TLabel;
    edtAlias: TEdit;
    btnSUM: TButton;
    btnCOUNT: TButton;
    btnMIN: TButton;
    btnMAX: TButton;
    procedure edtAliasExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cbTmpFldsChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnSUMClick(Sender: TObject);
    procedure btnCOUNTClick(Sender: TObject);
    procedure btnMINClick(Sender: TObject);
    procedure btnMAXClick(Sender: TObject);
  private
    FField: TrwQBShowingField;
    FFields: TrwQBShowingFields;
    procedure CheckErrors;
    procedure OnChangeExpression(Sender: TObject);
    procedure AddFunction(AFunct: String);
  public
  end;

function QBEditField(AField: TrwQBShowingField): Boolean;

implementation

{$R *.DFM}


function QBEditField(AField: TrwQBShowingField): Boolean;
var
  Frm: TrwQBFldEditor;
  i: Integer;
  rwT: TrwDataDictTable;
begin
  Result := False;

  Frm := TrwQBFldEditor.Create(Application);
  with Frm do
  try
    FFields.FOwner := TrwQBQuery(TrwQBShowingFields(AField.Collection).Owner);
    FField.Assign(AField);

    edtAlias.Text := AField.FieldAlias;
    rwT := TrwDataDictTable(DataDictionary.Tables.TableByName(TrwQBQuery(FFields.Owner).TmpTableName));
    if Assigned(rwT) then
      for i := 0 to rwT.Fields.Count-1 do
        cbTmpFlds.Items.AddObject(rwT.Fields[i].Name, rwT.Fields[i]);

    pnlTmpFld.Visible := TrwQBQuery(FFields.Owner).SQLStatement <> rssSelect;

    pnlField.ReadOnly := TrwQBQuery(FFields.Owner).Union;
    btnSUM.Enabled := not pnlField.ReadOnly;
    btnCOUNT.Enabled := btnSUM.Enabled;
    btnMIN.Enabled := btnSUM.Enabled;
    btnMAX.Enabled := btnSUM.Enabled;

    pnlAlias.Visible := not pnlTmpFld.Visible;
    pnlTmpFld.Top := pnlAlias.Top;
    if pnlTmpFld.Visible then
      FField.FieldAlias := '';

    if pnlTmpFld.Visible then
    begin
      Caption := 'Altering Field Editor';
      pnlField.lExpr.Caption := 'Source Expression';
      cbTmpFlds.ItemIndex := cbTmpFlds.Items.IndexOf(AField.TmpFieldName);
    end;

    if ShowModal = mrOK then
    begin
      FField.InternalName := AField.InternalName;
      AField.Assign(FField);
      Result := True;
    end;

  finally
    Free;
  end;
end;


procedure TrwQBFldEditor.edtAliasExit(Sender: TObject);
begin
  edtAlias.Text := StringReplace(Trim(edtAlias.Text), ' ', '_', [rfReplaceAll]);
  FField.FieldAlias := edtAlias.Text;
end;

procedure TrwQBFldEditor.FormCreate(Sender: TObject);
begin
  FFields := TrwQBShowingFields.Create(TrwQBShowingField);
  FFields.Add;
  FField := TrwQBShowingField(FFields[0]);
  pnlField.Expression := FField;
  pnlField.pnlExpr.ReadOnly := True;
  pnlField.OnChangeExpression := OnChangeExpression; 
end;

procedure TrwQBFldEditor.FormDestroy(Sender: TObject);
begin
  FFields.Free;
end;

procedure TrwQBFldEditor.cbTmpFldsChange(Sender: TObject);
begin
  FField.TmpFieldName := cbTmpFlds.Text;
end;

procedure TrwQBFldEditor.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOK then
  begin
    CanClose := False;
    CheckErrors;
    CanClose := True;
  end;
end;

procedure TrwQBFldEditor.CheckErrors;
var
  Err: String;
begin
  Err := '';
  with FField do
  begin
    if Count = 0 then
      Err := 'Expression is empty';

    if not pnlTmpFld.Visible and (Length(FieldAlias) = 0) and
      ((Count > 1) or ((Count = 1) and (TrwQBExpressionItem(Items[0]).ItemType <> eitField))) then
      Err := 'Alias is empty';
  end;

  if Err <> '' then
    raise ErwException.Create(Err);
end;

procedure TrwQBFldEditor.OnChangeExpression(Sender: TObject);
var
  i: Integer;
  Tbl: TrwQBSelectedTable;
  Fld: TrwQBShowingField;
begin
  edtAlias.OnExit(nil);
  
  if (FField.FieldAlias = '') and (FField.Count > 1) then
  begin
    for i := 0 to FField.Count - 1 do
      if TrwQBExpressionItem(FField.Items[i]).ItemType = eitField then
      begin
        Tbl := FField.GetSelectedTables.TableByInternalName(TrwQBExpressionItem(FField.Items[i]).Description);
        if Tbl.IsSUBQuery then
        begin
          Fld := Tbl.SUBQuery.ShowingFields.FieldByInternalName(TrwQBExpressionItem(FField.Items[i]).Value);
          if Fld.FieldAlias <> '' then
            edtAlias.Text := Fld.FieldAlias
          else
            edtAlias.Text := Fld.GetFieldName(False);
        end
        else
          edtAlias.Text := TrwQBExpressionItem(FField.Items[i]).Value;

        edtAlias.OnExit(nil);
        break;
      end;
  end;
end;

procedure TrwQBFldEditor.btnSUMClick(Sender: TObject);
begin
  if FField.ExprType in [rwvInteger, rwvFloat, rwvCurrency, rwvVariant] then
    AddFunction('SUM');
end;

procedure TrwQBFldEditor.AddFunction(AFunct: String);
var
  Itm: TrwQBExpressionItem;
begin
  if (FField.Count = 0) or
     (TrwQBExpressionItem(FField.Items[0]).ItemType = eitFunction) and
      AnsiSameText(TrwQBExpressionItem(FField.Items[0]).Value, AFunct)  then
    Exit;

  Itm := FField.AddItem(eitFunction, AFunct);
  Itm.Index := 0;
  FField.AddItem(eitSeparator, ',');
  FField.AddItem(eitFalse, '''N''', 'No');
  FField.AddItem(eitBracket, ')');
  pnlField.pnlExpr.ReDraw;
  OnChangeExpression(nil);
end;

procedure TrwQBFldEditor.btnCOUNTClick(Sender: TObject);
begin
  AddFunction('COUNTVAL');
end;

procedure TrwQBFldEditor.btnMINClick(Sender: TObject);
begin
  if FField.ExprType in [rwvInteger, rwvFloat, rwvCurrency, rwvDate, rwvVariant] then
    AddFunction('MIN');
end;

procedure TrwQBFldEditor.btnMAXClick(Sender: TObject);
begin
  if FField.ExprType in [rwvInteger, rwvFloat, rwvCurrency, rwvDate, rwvVariant] then
    AddFunction('MAX');
end;

end.
