unit rwQueryBuilderFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList, ComCtrls, StdCtrls, rwCustomDataDictionary, rwDataDictionary,
  ExtCtrls, ToolWin, ActnList, Grids, Menus, Clipbrd, rwQBInfo,
  rwQBFldEditorFrm, rwQBSortFldEditorFrm, Buttons,
  rwQBTableFrm, rwTypes, rwMessages, rwUtils, rwDB, rwQBParamsFrm, Mask,
  rwDesignClasses, Db, mwCustomEdit, mwGeneralSyn,
  rwLogQuery, Math, isRegistryFunctions, Variants, sbAPI, sbSQL, rwDebugInfo,
  rwInputFormControls, rwExtendedControls, rwBasicClasses, rwQBConstsFrm,
  sbConstants, rwBasicUtils, rwEngineTypes, rwQBExprEditorFrm, rmTypes,
  EvStreamUtils, rwAdapterUtils, rwEngine, isBasicUtils, isUtils;

type

  {Structure for Drag&Drop operations}

  TrwDragObjectType = (rdoNone, rdoTable, rdoTemplate, rdoField, rdoSortField);
  TrwqbMiscParams = (qmpNone, qmpDontActivateTable);

  TrwDragObject = record
    DropPoint: TPoint;
    MiscParams: TrwqbMiscParams;
    case ObjectType: TrwDragObjectType of
      rdoNone: ();

      rdoTable: (
        TableName: ShortString;
        Table: TrwQBSelectedTable);

      rdoTemplate:
        (Template: TrwQBTemplate);

      rdoField: (
        FieldName: ShortString;
        Field: TCollectionItem;
        TableOwnerName: ShortString;
        TableOwner: TrwQBSelectedTable;
        TableDest:  TrwQBSelectedTable);

      rdoSortField:
        (SortField: TCollectionItem);
  end;


  TrwDictGroup = class (TCollectionItem)
  private
    FList: TList;
    FMenuItem: TTreeNode;
    FName: String;
    procedure SetName(const Value: string);
    function GetItem(index: Integer): TObject;
  public
    property Name: string read FName write SetName;
    property MenuItem: TTreeNode read FMenuItem;
    property Items[index: Integer]: TObject read GetItem; default;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    function Add(AObject: TObject): Integer;
    procedure Delete(AObject: TObject);
    function Count: Integer;
  end;

  TrwDictGroups = class (TCollection)
  private
    FRootNode: TTreeNode;
  public
    function IndexOf(AGroup: String): TrwDictGroup;
    property RootNode: TTreeNode read FRootNode write FRootNode;
  end;

  TrwQBJoinLine = record
    LPoint: TPoint;
    LPointNum: Byte;
    RPoint: TPoint;
    RPointNum: Byte;
    Length: Integer;
    Angle: Extended;
  end;


  TrwQBScrollArea = class(TScrollBox)
  private
    FCanvas: TControlCanvas;
    FCalcLines: array [1..16] of TrwQBJoinLine;

    procedure WMPaint(var Message: TMessage); message WM_PAINT;
    function  JoinAt(X, Y: Integer): TrwQBJoin;

  protected
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;


  TrwQBJoinHintWindow = class(THintWindow)
  private
    FJoin: TrwQBJoin;
    procedure ShowJoinHint(AJoin: TrwQBJoin; AMousePos: TPoint);
  end;

  // External parameters (global published variables)
  TrwQBExtParam = class(TCollectionItem)
  private
    FParamDisplayName: String;
    FParamName: String;
    FValue: Variant;
    FParamType: TrwVarTypeKind;
  public
    procedure Assign(Source: TPersistent); override;

    property ParamName: String read FParamName write FParamName;
    property ParamDisplayName: String read FParamDisplayName write FParamDisplayName;
    property ParamType: TrwVarTypeKind read FParamType write FParamType;
    property Value: Variant read FValue write FValue;
  end;

  TrwQBExtParams = class(TCollection)
  private
    function GetItem(Index: Integer): TrwQBExtParam;
  public
    function ParamByName(const AParamName: String): TrwQBExtParam;
    property Items[Index: Integer]: TrwQBExtParam read GetItem; default;
  end;



  {TrwQueryBuilder is form for construct logical SQL-statements}

  TrwQueryBuilder = class(TForm)
    ctbBar: TControlBar;
    ActionList1: TActionList;
    actClose: TAction;
    actNewTable: TAction;
    Panel2: TPanel;
    Splitter1: TSplitter;
    actSearch: TAction;
    actNewField: TAction;
    pmFields: TPopupMenu;
    DelField: TAction;
    RemoveField1: TMenuItem;
    pmSort: TPopupMenu;
    MenuItem1: TMenuItem;
    actCancel: TAction;
    actEditField: TAction;
    Edit1: TMenuItem;
    actDelSortField: TAction;
    actEditSortField: TAction;
    Edit2: TMenuItem;
    actAddSortField: TAction;
    Add1: TMenuItem;
    actAddField: TAction;
    Add2: TMenuItem;
    actEditDict: TAction;
    actAddDict: TAction;
    actDelDict: TAction;
    actTable: TAction;
    actExpDict: TAction;
    actImpDict: TAction;
    SaveDialog: TSaveDialog;
    OpenDialog: TOpenDialog;
    actAddSUBQuery: TAction;
    actAddJoin: TAction;
    actOpenSQL: TAction;
    Panel1: TPanel;
    pcFields: TPageControl;
    tsSelFlds: TTabSheet;
    lvFields: TListView;
    tsSort: TTabSheet;
    lvSort: TListView;
    tsMisc: TTabSheet;
    chBDistinct: TCheckBox;
    pnMainSQL: TPanel;
    lSQLType: TLabel;
    lTmpTbl: TLabel;
    cbSQLType: TComboBox;
    cbTmpTbl: TComboBox;
    tsSQL: TTabSheet;
    Splitter2: TSplitter;
    pnlTables: TPanel;
    tvTables: TTreeView;
    Label2: TLabel;
    pnlMasterTables: TPanel;
    tvChildTables: TTreeView;
    Label3: TLabel;
    lCurrTable: TLabel;
    ToolBar2: TToolBar;
    ToolButton11: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    ToolButton21: TToolButton;
    Splitter3: TSplitter;
    Label4: TLabel;
    tvQuery: TTreeView;
    ToolButton1: TToolButton;
    actDelSUBQuery: TAction;
    pmSubQuery: TPopupMenu;
    AddSubQuery1: TMenuItem;
    Edit3: TMenuItem;
    tbDictEdit: TToolBar;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    actEdSubQueryDescr: TAction;
    EditSubQueryDescription1: TMenuItem;
    tsResult: TTabSheet;
    pmQueryType: TPopupMenu;
    actAddParentQuery: TAction;
    ParentQuery1: TMenuItem;
    AddSubQuery2: TMenuItem;
    AddQuery1: TMenuItem;
    ParentQuery2: TMenuItem;
    ToolButton8: TToolButton;
    btnHidePnls: TToolButton;
    actHidePanels: TAction;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton12: TToolButton;
    actSave: TAction;
    actOpen: TAction;
    sdQuery: TSaveDialog;
    odQuery: TOpenDialog;
    memDescr: TMemo;
    Splitter4: TSplitter;
    pmJoin: TPopupMenu;
    Jointype1: TMenuItem;
    miINNER: TMenuItem;
    miOUTERl: TMenuItem;
    miOUTERr: TMenuItem;
    RemoveJoin1: TMenuItem;
    gbMacros: TGroupBox;
    ToolButton13: TToolButton;
    memPlan: TMemo;
    Splitter5: TSplitter;
    pmPasteTable: TPopupMenu;
    miPasteTable: TMenuItem;
    actMaximizeResult: TAction;
    pmResults: TPopupMenu;
    Maximize1: TMenuItem;
    pcSQL: TPageControl;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    actConsts: TAction;
    actMakeUnion: TAction;
    MakeUNION1: TMenuItem;
    MakeUNION2: TMenuItem;
    pnlInsertMacro: TPanel;
    lInsMacro: TLabel;
    edMacroInsert: TEdit;
    pnlSelectMacro: TPanel;
    Label9: TLabel;
    edMacroSelect: TEdit;
    pnlFromMacro: TPanel;
    Label5: TLabel;
    edMacroFrom: TEdit;
    pnlWhereMacro: TPanel;
    Label6: TLabel;
    edMacroWhere: TEdit;
    pnlOrderByMacro: TPanel;
    Label7: TLabel;
    edMacroOrder: TEdit;
    N1: TMenuItem;
    N2: TMenuItem;
    ToolButton22: TToolButton;
    actSyncDD: TAction;
    btnView: TToolButton;
    actView: TAction;
    ilPict: TImageList;
    tbCustom: TToolBar;
    procedure actCloseExecute(Sender: TObject);
    procedure actNewTableExecute(Sender: TObject);
    procedure scbDsgnAreaDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure tvTablesEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure lvFieldsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure actNewFieldExecute(Sender: TObject);
    procedure tvTablesClick(Sender: TObject);
    procedure lvFieldsClick(Sender: TObject);
    procedure DelFieldExecute(Sender: TObject);
    procedure lvSortClick(Sender: TObject);
    procedure lvSortDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure MenuItem1Click(Sender: TObject);
    procedure lvSortEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure actCancelExecute(Sender: TObject);
    procedure actEditFieldExecute(Sender: TObject);
    procedure actDelSortFieldExecute(Sender: TObject);
    procedure actEditSortFieldExecute(Sender: TObject);
    procedure actAddSortFieldExecute(Sender: TObject);
    procedure actAddFieldExecute(Sender: TObject);
    procedure actEditDictExecute(Sender: TObject);
    procedure actAddDictExecute(Sender: TObject);
    procedure actDelDictExecute(Sender: TObject);
    procedure chBDistinctClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure actTableExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure scbDsgnAreaClick(Sender: TObject);
    procedure cbSQLTypeChange(Sender: TObject);
    procedure pcFieldsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure lvFieldsDblClick(Sender: TObject);
    procedure cbTmpTblChange(Sender: TObject);
    procedure actExpDictExecute(Sender: TObject);
    procedure actImpDictExecute(Sender: TObject);
    procedure lvFieldsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure actAddSUBQueryExecute(Sender: TObject);
    procedure actAddJoinExecute(Sender: TObject);
    procedure pcFieldsChange(Sender: TObject);
    procedure actOpenSQLExecute(Sender: TObject);
    procedure lvSortDblClick(Sender: TObject);
    procedure lvFieldsEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure tvQueryChange(Sender: TObject; Node: TTreeNode);
    procedure tvQueryEdited(Sender: TObject; Node: TTreeNode;
      var S: String);
    procedure actDelSUBQueryExecute(Sender: TObject);
    procedure actEdSubQueryDescrExecute(Sender: TObject);
    procedure actAddParentQueryExecute(Sender: TObject);
    procedure actHidePanelsExecute(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actOpenExecute(Sender: TObject);
    procedure tvQueryDeletion(Sender: TObject; Node: TTreeNode);
    procedure tvChildTablesChange(Sender: TObject; Node: TTreeNode);
    procedure miOUTERrClick(Sender: TObject);
    procedure pmJoinPopup(Sender: TObject);
    procedure RemoveJoin1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actSearchExecute(Sender: TObject);
    procedure edMacroSelectExit(Sender: TObject);
    procedure pmPasteTablePopup(Sender: TObject);
    procedure miPasteTableClick(Sender: TObject);
    procedure grResult1DblClick(Sender: TObject);
    procedure actMaximizeResultExecute(Sender: TObject);
    procedure tvQueryContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure actConstsExecute(Sender: TObject);
    procedure actMakeUnionExecute(Sender: TObject);
    procedure tvChildTablesCompare(Sender: TObject; Node1,
      Node2: TTreeNode; Data: Integer; var Compare: Integer);
    procedure actSyncDDExecute(Sender: TObject);
    procedure actViewExecute(Sender: TObject);

  private
    memSQL: TmwCustomEdit;
    FMainQBQuery: TrwQBQuery;
    FQBQuery: TrwQBQuery;
    FSearchFill: Boolean;
    FKeyWords: TStringList;
    fl_upd_dict: Boolean;
    fl_upd_tmpl: Boolean;
    fl_upd_const: Boolean;
    FListGroups: TrwDictGroups;
    FListTmplGroups: TrwDictGroups;
    FSQLParams: TrwQBParams;
    FAskPassword: Boolean;
    FTemplateNode: TTreeNode;
    FDataDictionaryNode: TTreeNode;
    FJoinHintWnd: TrwQBJoinHintWindow;
    FCurSearchIndex: Integer;
    FResultGrid: TrwDBGrid;
    FPrevQBFrm: TrwQueryBuilder;
    FConstFrm: TrwQBConstsEdit;
    FSecurityRec: TrwDesignerSecurityRec;
    FSQLSyn: TmwGeneralSyn;
    FAppCustomTBControl: TForm;
    FCustomTBPrevParent: HWND;
    FErrors: String;

    procedure CreateCustomTB;
    procedure DestroyCustomTB;
    procedure Initialize;
    procedure Finalize;
    procedure FillTreeView;
    procedure FillTemplates;
    procedure FillChildTreeView(AMasterTable: TrwDataDictTable);
    function  AddTableToTree(AObj: TrwDataDictTable; ANode: TTreeNode): TTreeNode;
    function  AddTemplateToTree(ATmpl: TrwQBTemplate; ANode: TTreeNode): TTreeNode;

    function  AddVisualTable(ATable: TrwQBSelectedTable): TrwQBTable;
    procedure AddVisualField(AField: TrwQBShowingField);
    procedure AddVisualSortField(AField: TrwQBSortField);
    procedure AddParentQuery(const Union: Boolean);
    procedure SyncContents(AQuery: TrwQBQuery);
    function  FixUpQuery(AQuery: TrwQBQuery): Boolean;
    procedure SyncSortFields;
    procedure SyncMainQuery;
    procedure SyncUnionShowingFields(AQuery: TrwQBQuery);
    procedure CleanUpDragObj;
    function  CreateQueryNode(AQuery: TrwQBQuery): TTreeNode;
    function  CheckCortProduct(AQuery: TrwQBQuery): Boolean;
    function  CheckMultJoins(AQuery: TrwQBQuery): Boolean;
    function  CheckUnion(AQuery: TrwQBQuery): Boolean;
    function  CheckBasicErrors(AQuery: TrwQBQuery): Boolean;
    function  CheckErrors(AQuery: TrwQBQuery): Boolean;
    procedure OpenSelectedTable(ATable: TrwQBSelectedTable);
    procedure UpdateVisualField(AField: TrwQBShowingField; AItem: TListItem);
    function  GetUniqueTableAlias: String;
    function  TabSheetByQuery(AQuery: TrwQBQuery): TTabSheet;
    procedure CloseCurrentTable;
    procedure ShowJoinHint(AJoin: TrwQBJoin; ATopLeft: TPoint);
    procedure SaveState;
    procedure RestoreState;
    procedure UndockedBarsVisible(AVisible: Boolean);
    procedure FOnFindDialog(Sender: TObject);
    procedure FOnCloseSearch(Sender: TObject);
    procedure DeleteTabSheet(ATabSheet: TTabSheet);
    procedure MakeTableParamList(ddTable: TrwDataDictTable; ATableParams: TrwQBExpressions);
    procedure FixTableParamsIfItNeeds;
    function  SQLSyn: TmwGeneralSyn;

    procedure ClearErrors;
    procedure AddError(const AError: String);
    procedure ShowErrors;

  public
    DragObject: TrwDragObject;
    CurrentActiveTable: TrwQBTable;
    WizardView: Boolean;
    Refreshing: Boolean;
    ExternalParams: TrwQBExtParams;

    procedure SyncFields;
    procedure SyncUnionTables;
    procedure RefreshFields(ATable: TrwQBSelectedTable);
    procedure RefreshSortFields(AField: TrwQBShowingField);
    procedure ChildTables(AVisible: Boolean);
    procedure SyncSQLResult;
    function  NodeByQuery(AQuery: TrwQBQuery): TTreeNode;
    procedure SwitchQuery;
    procedure HideJoinHint;
    procedure RefreshTableArea;
  end;


  TrwQBFormHolder = class(TInterfacedObject, IrwDesignerForm)
  private
    FForm: TrwQueryBuilder;
    FParams: TrwQBExtParams;
  protected
    function  IsClosed: Boolean;
    function  GetData: IEvDualStream;
  public
    constructor Create(const AQueryRes: IEvDualStream; const AWizard: Boolean; const AGlobalVars: IrmVariables);
    destructor  Destroy; override;
  end;



function ShowQueryBuilder(AQuery: TrwQBQuery; IgnoreInheritance: Boolean; AWizard: Boolean = False; AExtParams: TrwQBExtParams = nil): Boolean;
function ShowQueryBuilderExt(AQuery: TrwQBQuery; IgnoreInheritance: Boolean; AWizard: Boolean = False; AGlobalVars: IrmVariables = nil): Boolean;
function ShowQueryBuilderExt2(const AQueryData: IEvDualStream; AWizard: Boolean; AGlobalVars: IrmVariables): IrwDesignerForm;

var
  FQBFrm: TrwQueryBuilder;
  CF_QBQUERY: Word;
  CF_QBCONDITION: Word;
  CF_QBEXPRESSION: Word;
  CF_QBTABLE: Word;


implementation

uses rwQBTableContentFrm, rwQBJoinEditorFrm, rwQBTemplateEditorFrm, rwDDAddTableFrm,
     rwDDTableEditFrm, rwLogViewerFrm;

{$R *.DFM}

function ShowQueryBuilder(AQuery: TrwQBQuery; IgnoreInheritance: Boolean; AWizard: Boolean = False; AExtParams: TrwQBExtParams = nil): Boolean;
var
  Frm: TrwQueryBuilder;
begin
  Result := False;

  if Assigned(AQuery) and (AQuery.Owner is TrwQuery) and
     (TrwQuery(AQuery.Owner).SQL.Text <> '') and (TrwQuery(AQuery.Owner).SQL.Text <> AQuery.SQL) then
    if MessageDlg('SQL property of the query has been changed manually.'+#13+'Proceed?', mtWarning, [mbYes, mbNo], 0) <> mrYes then
      Exit;

  Frm := TrwQueryBuilder.Create(Application);
  with Frm do
    try
      if Debugging then
        actClose.Enabled := False;
      WizardView := AWizard;
      ExternalParams := AExtParams;
      Initialize;
      if Assigned(AQuery) then
      begin
        FMainQBQuery.Assign(AQuery);
        FixTableParamsIfItNeeds;
      end;

      SyncMainQuery;

      if ShowModal = mrOk then
      begin
        if TrwSQLStatementType(cbSQLType.ItemIndex) <> rssSelect then
          FMainQBQuery.SortFields.Clear;

        if not (TrwSQLStatementType(cbSQLType.ItemIndex) in [rssSelect, rssInsert]) then
          FMainQBQuery.ShowingFields.Clear;

        if Assigned(AQuery) then
        begin
          if not IgnoreInheritance then
            CheckInheritedAndShowError(TrwComponent(AQuery.Owner));
          AQuery.Assign(FMainQBQuery);
        end
        else
          Clipboard.AsText := FMainQBQuery.SQL;
        Result := True;
      end;

    finally;
      Finalize;
      Free;
    end;
end;


function ShowQueryBuilderExt(AQuery: TrwQBQuery; IgnoreInheritance: Boolean; AWizard: Boolean = False; AGlobalVars: IrmVariables = nil): Boolean;
var
  Params: TrwQBExtParams;
  Param: TrwQBExtParam;
  i: Integer;
  vInfo: TrmVFHVarInfoRec;
begin
  if Assigned(AGlobalVars) then
  begin
    Params := TrwQBExtParams.Create(TrwQBExtParam);
    try
      for i := 0 to AGlobalVars.GetItemCount - 1 do
      begin
        vInfo := (AGlobalVars.GetItemByIndex(i) as IrmVariable).GetVarInfo;
        if vInfo.ShowInQB then
        begin
          Param := TrwQBExtParam(Params.Add);
          Param.ParamName := vInfo.Name;
          if vInfo.DisplayName <> '' then
            Param.ParamDisplayName := vInfo.DisplayName
          else
            Param.ParamDisplayName := vInfo.Name;
          Param.Value := vInfo.Value;
        end;
      end;

      Result := ShowQueryBuilder(AQuery, IgnoreInheritance, AWizard, Params);
    finally
      FreeAndNil(Params);
    end
  end

  else
    Result := ShowQueryBuilder(AQuery, IgnoreInheritance, AWizard, nil);
end;


function ShowQueryBuilderExt2(const AQueryData: IEvDualStream; AWizard: Boolean; AGlobalVars: IrmVariables): IrwDesignerForm;
begin
  Result := TrwQBFormHolder.Create(AQueryData, AWizard, AGlobalVars);
end;

                   {TrwQueryBuilder}

{ Filling dictionary treeview }

procedure TrwQueryBuilder.FillTreeView;
var
  i: Integer;
  Grp: TrwDictGroup;
  rwT: TrwDataDictTable;

  procedure FillGroupsContent;
  var
    i,j: Integer;
  begin
    for i := 0 to FListGroups.Count - 1 do
    begin
      if TrwDictGroup(FListGroups.Items[i]).Name = '' then
        Continue;

      TrwDictGroup(FListGroups.Items[i]).MenuItem.DeleteChildren;
      for j := 0 to TrwDictGroup(FListGroups.Items[i]).Count - 1 do
        AddTableToTree(TrwDataDictTable(TrwDictGroup(FListGroups.Items[i])[j]),
          TrwDictGroup(FListGroups.Items[i]).MenuItem);
    end;

    for i := 0 to FListGroups.Count - 1 do
    begin
      if TrwDictGroup(FListGroups.Items[i]).Name <> '' then
        Continue;

      for j := 0 to TrwDictGroup(FListGroups.Items[i]).Count - 1 do
        AddTableToTree(TrwDataDictTable(TrwDictGroup(FListGroups.Items[i])[j]), FListGroups.RootNode);
    end;
  end;

begin
  Busy;
  tvTables.SortType := stNone;
  tvTables.Items.BeginUpdate;
  FListGroups.Clear;
  FListTmplGroups.Clear;
  tvTables.Items.Clear;
  cbTmpTbl.Clear;


  FDataDictionaryNode := tvTables.Items.AddObject(nil, 'Data Dictionary', nil);
  FDataDictionaryNode.ImageIndex := 29;
  FDataDictionaryNode.SelectedIndex := FDataDictionaryNode.ImageIndex;
  FDataDictionaryNode.StateIndex := FDataDictionaryNode.ImageIndex;
  FListGroups.RootNode := FDataDictionaryNode;

  for i := 0 to DataDictionary.Tables.Count - 1 do
  begin
    rwT := TrwDataDictTable(DataDictionary.Tables[i]);
    Grp := FListGroups.IndexOf(rwT.GroupName);
    if not Assigned(Grp) then
    begin
      Grp := TrwDictGroup(FListGroups.Add);
      Grp.Name := rwT.GroupName;
    end;

    Grp.Add(rwT);

    if rwT.GroupName = DDG_TEMPTABLES then
      cbTmpTbl.Items.AddObject(rwT.Name, rwT);
  end;

  FillGroupsContent;

  FillTemplates;

  tvTables.SortType := stBoth;
  tvTables.Items.EndUpdate;
  FDataDictionaryNode.Expand(False);
  Ready;
end;


procedure TrwQueryBuilder.FillChildTreeView(AMasterTable: TrwDataDictTable);

  procedure FillGroupsContent;
  var
    i,j,g: Integer;
    N: TTreeNode;
    Obj: TrwDataDictTable;
  begin
    g := 0;
    for i := 0 to FListGroups.Count - 1 do
    begin
      N := tvChildTables.Items.AddObject(nil, '', nil);
      N.ImageIndex := 0;
      N.SelectedIndex := N.ImageIndex;
      N.StateIndex := N.ImageIndex;
      N.Text := TrwDictGroup(FListGroups.Items[i]).Name;
      for j := 0 to TrwDictGroup(FListGroups.Items[i]).Count - 1 do
      begin
        Obj := TrwDataDictTable(TrwDictGroup(FListGroups.Items[i])[j]);
        if Assigned(Obj.ForeignKeys.ForeignKeyByTable(AMasterTable)) then
          AddTableToTree(Obj, N);
      end;

      if not N.HasChildren then
        N.Free
      else
        Inc(g);
    end;

    if g = 1 then
      tvChildTables.Items[0].Expand(False);
  end;

begin
  Busy;
  tvChildTables.Items.BeginUpdate;
  tvChildTables.Items.Clear;
  FillGroupsContent;
  tvChildTables.SortType := stNone;
  tvChildTables.SortType := stText;
  tvChildTables.Items.EndUpdate;
  Ready;
end;


function TrwQueryBuilder.AddTableToTree(AObj: TrwDataDictTable; ANode: TTreeNode): TTreeNode;
var
  h: string;
  i: Integer;

  procedure AddField(AAttr: TrwDataDictField);
  var
    h: string;
    N: TTreeNode;
  begin
    if WizardView then
    begin
      h := TrwDataDictField(AAttr).DisplayName;
      if h = '' then
        Exit;
    end
    else
      h := TrwDataDictField(AAttr).Name;

    N := TTreeView(ANode.TreeView).Items.AddChildObject(Result, h, AAttr);

    if Assigned(AAttr.Table.ForeignKeys.ForeignKeyByField(AAttr)) then
      N.ImageIndex := 18
    else if Assigned(AAttr.Table.PrimaryKey.FindField(AAttr)) then
      N.ImageIndex := 20
    else
      N.ImageIndex := 1;
    N.StateIndex := N.ImageIndex;
    N.SelectedIndex := N.ImageIndex;
  end;

begin
  if WizardView then
  begin
    h := AObj.DisplayName;
    if h = '' then
      Exit;
  end
  else
    h := AObj.Name;

  Result := TTreeView(ANode.TreeView).Items.AddChildObject(ANode, h, AObj);
  Result.SelectedIndex := 12;
  Result.StateIndex := Result.SelectedIndex;
  Result.ImageIndex := Result.SelectedIndex;

  for i := 0 to AObj.Fields.Count - 1 do
    AddField(TrwDataDictField(AObj.Fields[i]));

  if WizardView and not Result.HasChildren then
    FreeAndNil(Result);
end;


procedure TrwQueryBuilder.actCloseExecute(Sender: TObject);
begin
  if FQBQuery.SelectedTables.Count = 0 then
    FQBQuery.SelectedTables.Clear

  else if CheckErrors(FQBQuery) then
    Exit;

  ModalResult := mrOK;    
  Close;
  ModalResult := mrOK;
end;

procedure TrwQueryBuilder.actNewTableExecute(Sender: TObject);
var
  i, j, l: Integer;
  Obj: TrwDataDictTable;
  rwF: TDataDictField;
  Tbl: string;
  Ref: TrwQBSelectedTable;
  T: TrwQBTable;
  RefInt: TDataDictForeignKey;
  Q: TrwQBQuery;

begin
  SyncSQLResult;

  if DragObject.ObjectType = rdoField then
  begin
    Obj := TrwDataDictTable(DataDictionary.Tables.TableByName(DragObject.TableOwnerName));
    rwF := Obj.Fields.FieldByName(DragObject.FieldName);
    RefInt := Obj.ForeignKeys.ForeignKeyByField(rwF);
    if not Assigned(RefInt) then
      Exit;

    Tbl := RefInt.Table.Name;
  end

  else if DragObject.ObjectType = rdoTemplate then
  begin
    Tbl := qbEmbeddedQueryName;
    RefInt := nil;
  end

  else
  begin
    Tbl := DragObject.TableName;
    RefInt := nil;
  end;


  i := FQBQuery.SelectedTables.Add(Tbl);
  TrwQBSelectedTable(FQBQuery.SelectedTables.Items[i]).Left := DragObject.DropPoint.X;
  TrwQBSelectedTable(FQBQuery.SelectedTables.Items[i]).Top := DragObject.DropPoint.Y;

  if FQBQuery.Union then
    TrwQBSelectedTable(FQBQuery.SelectedTables.Items[i]).SUBQuery.Description := 'Union Item'
  else
    TrwQBSelectedTable(FQBQuery.SelectedTables.Items[i]).Alias := GetUniqueTableAlias;

  if DragObject.ObjectType = rdoTemplate then
  begin
    Q := DragObject.Template.CreateQuery;
    if FixUpQuery(Q) then
    begin
      ShowMessage('Selected template is not appropriate for this query');
      Q.Free;
      TrwQBSelectedTable(FQBQuery.SelectedTables.Items[i]).Free;
      CleanUpDragObj;
      Exit;
    end;


    try
      TrwQBSelectedTable(FQBQuery.SelectedTables.Items[i]).SUBQuery.Assign(Q);
      SyncContents(TrwQBSelectedTable(FQBQuery.SelectedTables.Items[i]).SUBQuery);
    finally
      Q.Free;
    end;
  end;

  FQBQuery.SelectedTables[i].IsolatedFiltering := True;

  T := AddVisualTable(TrwQBSelectedTable(FQBQuery.SelectedTables.Items[i]));

  if DragObject.ObjectType = rdoTemplate then
  begin
    tvQuery.Selected := NodeByQuery(FQBQuery);
    SwitchQuery;
  end;

  if DragObject.MiscParams <> qmpDontActivateTable then
    T.Perform(RW_QBTABLE_OPEN, 0, 0);

  if not TrwQBSelectedTable(FQBQuery.SelectedTables.Items[i]).IsSUBQuery then
  begin
    Obj := TrwDataDictTable(DataDictionary.Tables.TableByName(FQBQuery.SelectedTables[i].TableName));
    MakeTableParamList(Obj, FQBQuery.SelectedTables[i].TableParams);

    if not Assigned(RefInt) then
      for j := 0 to Obj.ForeignKeys.Count - 1 do
      begin
        if Obj = Obj.ForeignKeys[j].Table then
          Continue;
        Tbl := Obj.ForeignKeys[j].Table.Name;
        Ref := FQBQuery.SelectedTables.FindTable(Tbl);
        if Assigned(Ref) and not Assigned(FindJoin(FQBQuery.SelectedTables[i], Ref)) then
          FQBQuery.Joins.Add(Ref, FQBQuery.SelectedTables[i], rjtJoin, Obj.ForeignKeys[j]);
      end;

    for j := 0 to FQBQuery.SelectedTables.Count - 1 do
    begin

      if (j = i) or FQBQuery.SelectedTables[j].IsSUBQuery or
         Assigned(RefInt) and (FQBQuery.SelectedTables[j] <> DragObject.TableOwner) then
        continue;

      Obj := TrwDataDictTable(DataDictionary.Tables.TableByName(FQBQuery.SelectedTables[j].TableName));

      for l := 0 to Obj.ForeignKeys.Count - 1 do
        if AnsiSameText(Obj.ForeignKeys[l].Table.Name, FQBQuery.SelectedTables[i].TableName) then
        begin
          if Assigned(RefInt) and (Obj.ForeignKeys[l] <> RefInt) then
            continue;
          if not Assigned(FindJoin(FQBQuery.SelectedTables[i], FQBQuery.SelectedTables[j])) then
            FQBQuery.Joins.Add(FQBQuery.SelectedTables[i],
              FQBQuery.SelectedTables[j], rjtJoin, Obj.ForeignKeys[l]);
        end;
    end;
  end;

  if (DragObject.ObjectType = rdoField) and DragObject.TableOwner.IsSUBQuery then
    FQBQuery.Joins.Add(FQBQuery.SelectedTables[i], DragObject.TableOwner,
      rjtJoin, RefInt, TrwQBShowingField(DragObject.Field).InternalName);

  pcFields.OnChange(nil);
  CleanUpDragObj;
  RefreshTableArea;

  if FQBQuery.SQLStatement in [rssUpdate, rssDelete] then
  begin
    cbSQLType.ItemIndex := 0;
    cbSQLType.OnChange(nil);
  end;
end;

function TrwQueryBuilder.AddVisualTable(ATable: TrwQBSelectedTable): TrwQBTable;
var
  T: TTabSheet;
begin
  T := TabSheetByQuery(TrwQBSelectedTables(ATable.Collection).QBQuery);
  Result := TrwQBTable.Create(T);
  Result.Parent := TrwQBScrollArea(T.Controls[0]);
  Result.Initialize(ATable);
  Result.Visible := True;
  ATable.VisualComponent := Result;
end;

procedure TrwQueryBuilder.scbDsgnAreaDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (DragObject.ObjectType in [rdoTable, rdoField, rdoTemplate]) and not FQBQuery.Union;
end;

procedure TrwQueryBuilder.tvTablesEndDrag(Sender, Target: TObject; X, Y: Integer);
begin
  if Target = TrwQBScrollArea(pcSQL.ActivePage.Controls[0]) then
  begin
    DragObject.DropPoint := Point(X, Y);
    actNewTable.Execute;
    DragObject.DropPoint := Point(10, 10);
  end
end;

procedure TrwQueryBuilder.lvFieldsDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (DragObject.ObjectType = rdoField);
end;

procedure TrwQueryBuilder.actNewFieldExecute(Sender: TObject);
var
  F: TrwQBShowingField;
begin
  SyncSQLResult;

  pcFields.ActivePage := tsSelFlds;

  F := TrwQBShowingField(FQBQuery.ShowingFields.Items[FQBQuery.ShowingFields.Add]);
  F.AddItem(eitField, DragObject.FieldName, DragObject.TableOwner.InternalName);
  AddVisualField(F);

  if Assigned(FQBQuery.ParentTable) then
    AddShowingFldToListView(TrwQBTable(FQBQuery.ParentTable.VisualComponent).lvFields, F);

  pcFields.OnChange(nil);
  CleanUpDragObj;
end;

procedure TrwQueryBuilder.AddVisualField(AField: TrwQBShowingField);
var
  L: TListItem;
begin
  L := lvFields.Items.Add;
  L.SubItems.Add('');
  L.SubItems.Add('');
  L.SubItems.Add('');
  L.SubItems.Add('');
  UpdateVisualField(AField, L);
end;

procedure TrwQueryBuilder.AddVisualSortField(AField: TrwQBSortField);
var
  L: TListItem;
begin
  L := lvSort.Items.Add;
  L.Data := AField;

  L.Caption := AField.Field.GetFieldName(WizardView);

  L.SubItems.Add('');
  if AField.SortDirection = rsdASC then
    L.SubItems[0] := 'Ascend'
  else
    L.SubItems[0] := 'Descend';
end;

procedure TrwQueryBuilder.tvTablesClick(Sender: TObject);
var
  TV: TTreeView;
begin
  TV := TTreeView(Sender);
  if not Assigned(TV.Selected) or not Assigned(TV.Selected.Data) then
  begin
    DragObject.ObjectType := rdoNone;
    Exit;
  end;     

  if TObject(TV.Selected.Data) is TrwDataDictTable then
  begin
    DragObject.ObjectType := rdoTable;
    DragObject.TableName := TrwDataDictTable(TV.Selected.Data).Name
  end

  else if TObject(TV.Selected.Data) is TrwQBTemplate then
  begin
    DragObject.ObjectType := rdoTemplate;
    DragObject.Template := TrwQBTemplate(TV.Selected.Data);
  end

  else
    DragObject.ObjectType := rdoNone;
end;

procedure TrwQueryBuilder.lvFieldsClick(Sender: TObject);
begin
  if Assigned(lvFields.Selected) then
  begin
    DragObject.ObjectType := rdoField;
    DragObject.Field := TrwQBShowingField(lvFields.Selected.Data);
  end
  else
    DragObject.ObjectType := rdoNone;
end;

procedure TrwQueryBuilder.DelFieldExecute(Sender: TObject);
var
  i: Integer;
begin
  if FQBQuery.Union then
    Exit;

  if DragObject.ObjectType <> rdoField then Exit;

  SyncSQLResult;

  RemoveShowingField(TrwQBShowingField(DragObject.Field));

  for i := 0 to lvFields.Items.Count - 1 do
    if lvFields.Items[i].Data = DragObject.Field then
    begin
      lvFields.Items.Delete(i);
      break;
    end;

  SyncSortFields;
  pcFields.OnChange(nil);

  CleanUpDragObj;
end;

procedure TrwQueryBuilder.RefreshFields(ATable: TrwQBSelectedTable);
var
  i: Integer;
begin
  for i := 0 to lvFields.Items.Count - 1 do
    UpdateVisualField(TrwQBShowingField(lvFields.Items[i].Data), lvFields.Items[i]);
end;

procedure TrwQueryBuilder.RefreshSortFields(AField: TrwQBShowingField);
var
  i: Integer;
  Attr: TrwDataDictField;
begin
  for i := 0 to lvSort.Items.Count - 1 do
    if TrwQBSortField(lvSort.Items[i].Data).Field = AField then
      if AField.FieldAlias <> '' then
        lvSort.Items[i].Caption := AField.FieldAlias
      else
      begin
        Attr := AField.Attribute;
        if WizardView and Assigned(Attr) and (Length(Attr.DisplayName) > 0) then
          lvSort.Items[i].Caption := Attr.DisplayName
        else
          lvSort.Items[i].Caption := Attr.Name
      end;
end;

procedure TrwQueryBuilder.lvSortClick(Sender: TObject);
begin
  if Assigned(lvSort.Selected) then
  begin
    DragObject.ObjectType := rdoSortField;
    DragObject.SortField := TrwQBSortField(lvSort.Selected.Data);
  end
  else
    DragObject.ObjectType := rdoNone;
end;

procedure TrwQueryBuilder.lvSortDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (DragObject.ObjectType = rdoSortField);
end;

procedure TrwQueryBuilder.MenuItem1Click(Sender: TObject);
var
  i: Integer;
begin
  if DragObject.ObjectType <> rdoField then Exit;

  for i := 0 to lvSort.Items.Count - 1 do
    if lvSort.Items[i].Data = DragObject.Field then
    begin
      lvSort.Items.Delete(i);
      break;
    end;

  DragObject.Field.Free;
end;

procedure TrwQueryBuilder.lvSortEndDrag(Sender, Target: TObject; X,
  Y: Integer);
var
  li, li2: TListItem;
  i: Integer;
begin
  if (Sender = Target) and (Sender = lvSort) then
  begin
    SyncSQLResult;
    
    li := nil;
    for i := 0 to lvSort.Items.Count - 1 do
      if lvSort.Items[i].Data = DragObject.SortField then
      begin
        li := lvSort.Items[i];
        break;
      end;

    li2 := lvSort.GetItemAt(X, Y);
    if not Assigned(li2) or not Assigned(li) then
      Exit;

    TrwQBSortField(li.Data).Index := TrwQBSortField(li2.Data).Index;
    i := TrwQBSortField(li.Data).Index;
    SyncFields;
    lvSort.Items[i].Selected := True;
  end;
end;

procedure TrwQueryBuilder.actCancelExecute(Sender: TObject);
begin
  ModalResult := mrAbort;
  Close;
  ModalResult := mrAbort;
end;

procedure TrwQueryBuilder.SyncContents(AQuery: TrwQBQuery);
var
  i: Integer;
begin
  tvQuery.Items.BeginUpdate;
  try
    CreateQueryNode(AQuery);

    for i := 0 to AQuery.SelectedTables.Count - 1 do
    begin
      AddVisualTable(AQuery.SelectedTables[i]);
      if AQuery.SelectedTables[i].IsSUBQuery then
        SyncContents(AQuery.SelectedTables[i].SUBQuery);
    end;

  finally
    tvQuery.Items.EndUpdate;
  end;
end;

procedure TrwQueryBuilder.actEditFieldExecute(Sender: TObject);
var
  i: Integer;
  L1: TListItem;
  T: TrwQBSelectedTable;
begin
  if FQBQuery.Union and (FQBQuery.SQLStatement <> rssInsert) then
    Exit;

  if DragObject.ObjectType <> rdoField then Exit;

  SyncSQLResult;

  L1 := nil;

  for i := 0 to lvFields.Items.Count - 1 do
    if lvFields.Items[i].Data = DragObject.Field then
    begin
      L1 := lvFields.Items[i];
      break;
    end;

  if Assigned(L1) and QBEditField(TrwQBShowingField(DragObject.Field)) then
  begin
    UpdateVisualField(TrwQBShowingField(DragObject.Field), L1);
    RefreshSortFields(TrwQBShowingField(DragObject.Field));
  end;


  T := FQBQuery.ParentTable;
  while Assigned(T) do
  begin
    PostMessage(TForm(T.VisualComponent).Handle, RW_QBTABLE_REFRESH, 0, 0);
    T := TrwQBSelectedTables(T.Collection).QBQuery.ParentTable;
  end;

  CleanUpDragObj;
end;

procedure TrwQueryBuilder.actDelSortFieldExecute(Sender: TObject);
var
  i: Integer;
begin
  if DragObject.ObjectType <> rdoSortField then Exit;

  SyncSQLResult;
  
  for i := 0 to lvSort.Items.Count - 1 do
    if lvSort.Items[i].Data = DragObject.SortField then
    begin
      lvSort.Items.Delete(i);
      break;
    end;

  DragObject.SortField.Free;

  CleanUpDragObj;
end;

procedure TrwQueryBuilder.actEditSortFieldExecute(Sender: TObject);
var
  i: Integer;
  L: TListItem;
begin
  if DragObject.ObjectType <> rdoSortField then Exit;

  SyncSQLResult;
  
  L := nil;

  for i := 0 to lvSort.Items.Count - 1 do
    if lvSort.Items[i].Data = DragObject.SortField then
    begin
      L := lvSort.Items[i];
      break;
    end;

  if Assigned(L) and QBEditSortField(FQBQuery, TrwQBSortField(DragObject.SortField)) then
  begin
    L.Caption := TrwQBSortField(DragObject.SortField).Field.GetFieldName(WizardView);
    if TrwQBSortField(DragObject.SortField).SortDirection = rsdASC then
      L.SubItems[0] := 'Ascend'
    else
      L.SubItems[0] := 'Descend';
  end;

  CleanUpDragObj;  
end;

procedure TrwQueryBuilder.actAddSortFieldExecute(Sender: TObject);
var
  Fld: TrwQBSortField;
begin
  SyncSQLResult;
  
  Fld := TrwQBSortField(FQBQuery.SortFields.Add);

  if QBEditSortField(FQBQuery, Fld) then
    AddVisualSortField(Fld)
  else
    Fld.Free;

  CleanUpDragObj;
end;

procedure TrwQueryBuilder.actAddFieldExecute(Sender: TObject);
var
  Fld: TrwQBShowingField;
begin
  if FQBQuery.Union then
    Exit;

  SyncSQLResult;
  
  Fld := TrwQBShowingField(FQBQuery.ShowingFields.Items[FQBQuery.ShowingFields.Add]);
  if QBEditField(Fld) then
    AddVisualField(Fld)
  else
    Fld.Free;

  if Assigned(FQBQuery.ParentTable) then
    PostMessage(TForm(FQBQuery.ParentTable.VisualComponent).Handle, RW_QBTABLE_REFRESH, 0, 0);

  CleanUpDragObj;
end;

procedure TrwQueryBuilder.actEditDictExecute(Sender: TObject);
var
  TN: TTreeNode;
  Obj: TrwDataDictTable;
  Grp: TrwDictGroup;
  Tmpl: TrwQBTemplate;
begin
  ChildTables(False);
  TN := tvTables.Selected;

  if not Assigned(TN) or not Assigned(TN.Data) then
    Exit;

  if TObject(TN.Data) is TrwQBTemplate then
  begin
    if not FSecurityRec.QBTemplates.Modify then
      Exit;

    Tmpl := TrwQBTemplate(TN.Data);
    Grp := FListTmplGroups.IndexOf(Tmpl.Group);
    if EditTemplate(Tmpl) then
    begin
      TN.Free;
      Grp.Delete(Tmpl);
      if Grp.Count = 0 then
        Grp.Free;
      Grp := FListTmplGroups.IndexOf(Tmpl.Group);
      if not Assigned(Grp) then
      begin
        Grp := TrwDictGroup(FListTmplGroups.Add);
        Grp.Name := Tmpl.Group;
      end;
      Grp.Add(Tmpl);
      Grp.MenuItem.Expand(False);
      TN := AddTemplateToTree(Tmpl, Grp.MenuItem);
      fl_upd_tmpl := True;
      tvTables.SortType := stNone;
      tvTables.SortType := stText;
      tvTables.Selected := TN;
    end
  end

  else
  begin
    if not FSecurityRec.DataDictionary.Modify then
      Exit;

    if TObject(TN.Data) is TrwDataDictField then
      TN := TN.Parent;
    Obj := TrwDataDictTable(TN.Data);
    Grp := FListGroups.IndexOf(Obj.GroupName);
    if EditDDTable(Obj) then
    begin
      fl_upd_dict := True;
      TN.Free;
      Grp.Delete(Obj);
      if Grp.Count = 0 then
        Grp.Free;
      Grp := FListGroups.IndexOf(Obj.GroupName);
      if not Assigned(Grp) then
      begin
        Grp := TrwDictGroup(FListGroups.Add);
        Grp.Name := Obj.GroupName;
      end;
      Grp.Add(Obj);
      if Obj.GroupName = '' then
        TN := AddTableToTree(Obj, FListGroups.RootNode)
      else
        TN := AddTableToTree(Obj, Grp.MenuItem);
      tvTables.SortType := stNone;
      tvTables.SortType := stText;
      tvTables.Selected := TN;
    end;
  end;
end;

procedure TrwQueryBuilder.actAddDictExecute(Sender: TObject);
var
  Grp: TrwDictGroup;
  Obj: TrwDataDictTable;
  N: TTreeNode;
  Tmpl: TrwQBTemplate;
begin
  ChildTables(False);

  if not Assigned(tvTables.Selected) then
    Exit;

  N := tvTables.Selected;
  if Assigned(N.Data) then
    N := N.Parent;

  if N = FTemplateNode then
  begin
    if not FSecurityRec.QBTemplates.Modify then
      Exit;

    Tmpl := TrwQBTemplate.Create(nil);
    if EditTemplate(Tmpl) then
    begin
      Tmpl.Collection := QBTemplates;
      fl_upd_tmpl := True;
      Grp := FListTmplGroups.IndexOf(Tmpl.Group);
      if not Assigned(Grp) then
      begin
        Grp := TrwDictGroup(FListTmplGroups.Add);
        Grp.Name := Tmpl.Group;
      end;
      Grp.Add(Tmpl);
      Grp.MenuItem.Expand(False);
      AddTemplateToTree(Tmpl, Grp.MenuItem).Selected := True;
      tvTables.SortType := stNone;
      tvTables.SortType := stText;
    end
    else
      Tmpl.Free;
  end

  else
  begin
    if not FSecurityRec.DataDictionary.Modify then
      Exit;

    Obj := AddDDTable;
    if Assigned(Obj) then
      if EditDDTable(Obj) then
      begin
        fl_upd_dict := True;
        Grp := FListGroups.IndexOf(Obj.GroupName);
        if not Assigned(Grp) then
        begin
          Grp := TrwDictGroup(FListGroups.Add);
          Grp.Name := Obj.GroupName;
        end;
        Grp.Add(Obj);
        if Obj.GroupName = '' then
          N := FListGroups.RootNode
        else
          N :=  Grp.MenuItem;
        N.Expand(False);
        AddTableToTree(Obj, N).Selected := True;
        tvTables.SortType := stNone;
        tvTables.SortType := stText;
      end
      else
        DataDictionary.DeleteTable(Obj);
  end;
end;

procedure TrwQueryBuilder.actDelDictExecute(Sender: TObject);
var
  Grp: TrwDictGroup;
  TN: TTreeNode;
  Tmpl: TrwQBTemplate;
begin
  TN := tvTables.Selected;

  if not Assigned(TN) or not Assigned(TN.Data) then
    Exit;

  if MessageDlg('You are about deletion an item. Proceed?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
    Exit;

  if TObject(TN.Data) is TrwQBTemplate then
  begin
    if not FSecurityRec.QBTemplates.Modify then
      Exit;

    Tmpl := TrwQBTemplate(TN.Data);
    Grp := FListTmplGroups.IndexOf(Tmpl.Group);
    Grp.Delete(Tmpl);
    if Grp.Count = 0 then
      Grp.Free;
    Tmpl.Free;
    TN.Free;
    fl_upd_tmpl := True;
  end

  else if TObject(TN.Data) is TrwDataDictTable then
  begin
    if not FSecurityRec.DataDictionary.Modify then
      Exit;

    Grp := FListGroups.IndexOf(TrwDataDictTable(TN.Data).GroupName);
    Grp.Delete(TrwDataDictTable(TN.Data));
    if Grp.Count = 0 then
      Grp.Free;
    DataDictionary.DeleteTable(TrwDataDictTable(TN.Data));
    TN.Free;
    fl_upd_dict := True;
  end;
end;



{ TrwDictGroup }

constructor TrwDictGroup.Create(ACollection: TCollection);
begin
  inherited Create(ACollection);
  FList := TList.Create;
end;

destructor TrwDictGroup.Destroy;
begin
  inherited;
  FreeAndNil(FMenuItem);
  FreeAndNil(FList);
end;

function TrwDictGroup.GetItem(index: Integer): TObject;
begin
  Result :=  TObject(FList[Index]);
end;

function TrwDictGroup.Add(AObject: TObject): Integer;
begin
  Result := FList.Add(AObject);
end;

procedure TrwDictGroup.SetName(const Value: string);
begin
  FName := Value;

  if FName = '' then
    FreeAndNil(FMenuItem)

  else
    if not Assigned(FMenuItem) then
    begin
      FMenuItem := FQBFrm.tvTables.Items.AddChildObject(TrwDictGroups(Collection).RootNode, '', nil);
      FMenuItem.ImageIndex := 0;
      FMenuItem.SelectedIndex := FMenuItem.ImageIndex;
      FMenuItem.StateIndex := FMenuItem.ImageIndex;
      FMenuItem.Text := FName;
    end;
end;

function TrwDictGroup.Count: Integer;
begin
  Result := FList.Count;
end;

procedure TrwDictGroup.Delete(AObject: TObject);
var
  i: Integer;
begin
  i := FList.IndexOf(AObject);
  FList.Delete(i);
end;

{ TrwDictGroups }

function TrwDictGroups.IndexOf(AGroup: String): TrwDictGroup;
var
  i: Integer;
begin
  Result := nil;
  AGroup := AnsiUpperCase(AGroup);
  for i := 0 to Count-1 do
    if AnsiUpperCase(TrwDictGroup(Items[i]).Name) = AGroup then
    begin
      Result := TrwDictGroup(Items[i]);
      break;
    end;
end;


procedure TrwQueryBuilder.chBDistinctClick(Sender: TObject);
begin
  SyncSQLResult;
  FQBQuery.DistinctResult := chBDistinct.Checked;
end;


procedure TrwQueryBuilder.Initialize;
begin
  FSecurityRec := RWEngineExt.AppAdapter.GetSecurityDescriptor;
  FAskPassword := True;
  FPrevQBFrm := FQBFrm;
  FQBFrm := Self;

  tbDictEdit.Visible := not WizardView;

  if not WizardView then
  begin
    actEditDict.Enabled := (FSecurityRec.DataDictionary.Modify or FSecurityRec.QBTemplates.Modify);
    actAddDict.Enabled  := actEditDict.Enabled;
    actDelDict.Enabled  := actEditDict.Enabled;
    actSyncDD.Enabled   := actEditDict.Enabled;
    actConsts.Enabled   := FSecurityRec.QBConstants.Modify;
    actExpDict.Enabled  := (FSecurityRec.DataDictionary.Modify and FSecurityRec.QBTemplates.Modify and FSecurityRec.QBConstants.Modify);
    actImpDict.Enabled  := actExpDict.Enabled;
  end;

  FListGroups := TrwDictGroups.Create(TrwDictGroup);
  FListTmplGroups := TrwDictGroups.Create(TrwDictGroup);

  FMainQBQuery := TrwQBQuery.Create(nil);

  fl_upd_dict := False;
  fl_upd_tmpl := False;
  fl_upd_const := False;
  if WizardView then
  begin
    pnMainSQL.Visible := False;
    gbMacros.Visible := False;
  end;

  FKeyWords := TStringList.Create;
  FSearchFill := False;
  DragObject.DropPoint := Point(10, 10);
  FillTreeView;
end;


procedure TrwQueryBuilder.Finalize;
var
  h: String;
begin
  FMainQBQuery.Free;
  FKeyWords.Free;
  FListGroups.Free;
  FListTmplGroups.Free;

  h := '';
  if fl_upd_dict then
    h := 'Data Dictionary';

  if fl_upd_tmpl then
  begin
    if h <> '' then
      h := h + ' and ';
    h := h + 'Templates';
  end;

  if fl_upd_const then
  begin
    if h <> '' then
      h := h + ' and ';
    h := h + 'Constants';
  end;

  if fl_upd_dict or fl_upd_tmpl or fl_upd_const then
    if MessageDlg('Do you want to save changes of ' + h + ' to the database?',
         mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if fl_upd_dict and FSecurityRec.DataDictionary.Modify then
        DataDictionary.Update;
      if fl_upd_tmpl and FSecurityRec.QBTemplates.Modify then
        QBTemplates.Store;
      if fl_upd_const and FSecurityRec.QBConstants.Modify then
        TrwQBConsts(SQLConsts).Store;
    end
    else
    begin
      if fl_upd_dict then
        DataDictionary.Initialize;
      if fl_upd_tmpl then
        QBTemplates.Initialize;
      if fl_upd_const then
        TrwQBConsts(SQLConsts).Load;
    end;

  FQBFrm := FPrevQBFrm;
end;


procedure TrwQueryBuilder.FormCreate(Sender: TObject);
begin
  CreateCustomTB;

  FResultGrid := TrwDBGrid.Create(nil);
  FResultGrid.MultiSelection := False;
  TExtDBGrid(FResultGrid.VisualControl.RealObject).PopupMenu := pmResults;
  TExtDBGrid(FResultGrid.VisualControl.RealObject).OnDblClick := grResult1DblClick;
  FResultGrid.VisualControl.RealObject.Parent := tsResult;
  FResultGrid.Align := alClient;

  InitCacheInfo;

  FJoinHintWnd := nil;
  ExternalParams := nil;
  memSQL := TmwCustomEdit.Create(Self);
  memSQL.ReadOnly := True;
  memSQL.Gutter.Visible := False;
  memSQL.HighLighter := SQLSyn;
  memSQL.Align := alClient;
  memSQL.Parent := tsSQL;

  Refreshing := False;
  CurrentActiveTable := nil;
  cbSQLType.ItemIndex := 0;
  cbSQLType.OnChange(nil);
end;

procedure TrwQueryBuilder.FormDestroy(Sender: TObject);
begin
 DestroyCustomTB;
 memSQL.Free;
 FreeCacheInfo;
end;

procedure TrwQueryBuilder.actTableExecute(Sender: TObject);
begin
  ChildTables(False);

  if not Assigned(tvTables.Selected) or not Assigned(tvTables.Selected.Data) or
     not (TObject(tvTables.Selected.Data) is TrwDataDictTable) then
    ShowMessage('No table is selected')
  else
    ShowTableContents(TrwDataDictTable(tvTables.Selected.Data));
end;

procedure TrwQueryBuilder.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i: Integer;
begin
  ActiveControl := nil;

  for i := 0 to pcSQL.PageCount - 1 do
    with TrwQBScrollArea(pcSQL.Pages[i].Controls[0]) do
    begin
      HorzScrollBar.Position := 0;
      VertScrollBar.Position := 0;
    end;

  SaveState;
  UndockedBarsVisible(False);
end;

procedure TrwQueryBuilder.scbDsgnAreaClick(Sender: TObject);
begin
  CloseCurrentTable;
  ChildTables(False);
end;

procedure TrwQueryBuilder.cbSQLTypeChange(Sender: TObject);
var
  Col: TListColumn;
begin
  if Assigned(FQBQuery) and (TrwSQLStatementType(cbSQLType.ItemIndex) in [rssUpdate, rssDelete]) and
     ((FQBQuery.SelectedTables.Count > 1) or
     (FQBQuery.SelectedTables.Count = 1) and ((FQBQuery.SelectedTables[0].lqObject = nil) or
     (FQBQuery.SelectedTables[0].lqObject.TableType <> rwtBuffer)) or
      FQBQuery.Union) then
  begin
    cbSQLType.ItemIndex := Ord(FQBQuery.SQLStatement);
    Exit;
  end;

  if TrwSQLStatementType(cbSQLType.ItemIndex) = rssSelect then
  begin
    tsSort.TabVisible := (FQBQuery = FMainQBQuery);
    if lvFields.Columns.Count = 5 then
    begin
      lvFields.Columns[0].Width := lvFields.Columns[0].Width + lvFields.Columns[3].Width;
      lvFields.Columns[4].Free;
    end;
  end
  else
  begin
    tsSort.TabVisible := False;
    if lvFields.Columns.Count = 4 then
    begin
      Col := lvFields.Columns.Add;
      Col.Caption := 'Buffer''s Field';
      Col.Width := 120;
      lvFields.Columns[0].Width := lvFields.Columns[0].Width - Col.Width;
    end;
  end;

  if Assigned(FQBQuery) and FQBQuery.Union then
  begin
    lvFields.Columns[2].Caption := '';
    lvFields.Columns[3].Caption := '';
  end
  else
  begin
    lvFields.Columns[2].Caption := 'Field Alias';
    lvFields.Columns[3].Caption := 'Table';
  end;


  if TrwSQLStatementType(cbSQLType.ItemIndex) <> rssDelete then
    tsSelFlds.TabVisible := True
  else
  begin
    if pcFields.ActivePage = tsSelFlds then
      pcFields.ActivePage := tsMisc;
    tsSelFlds.TabVisible := False;
  end;


  pnlInsertMacro.Visible := False;
  pnlSelectMacro.Visible := False;
  pnlFromMacro.Visible := False;
  pnlWhereMacro.Visible := False;
  pnlOrderByMacro.Visible := False;

  case TrwSQLStatementType(cbSQLType.ItemIndex) of
    rssSelect:
      begin
        if Assigned(FQBQuery) and not FQBQuery.Union then
        begin
          pnlSelectMacro.Visible := True;
          pnlFromMacro.Visible := True;
          pnlWhereMacro.Visible := True;
        end;
        pnlOrderByMacro.Visible := True;

        cbTmpTbl.Enabled := False;
        cbTmpTbl.ItemIndex := -1;
        cbTmpTbl.OnChange(nil);
        chBDistinct.Enabled := True;
        edMacroInsert.Text := '';
        edMacroSelect.OnExit(nil);
      end;

    rssInsert:
      begin
        pnlInsertMacro.Visible := True;
        if Assigned(FQBQuery) and not FQBQuery.Union then
        begin
          pnlSelectMacro.Visible := True;
          pnlFromMacro.Visible := True;
          pnlWhereMacro.Visible := True;
        end;
        pnlOrderByMacro.Visible := True;

        cbTmpTbl.Enabled := True;
        cbTmpTbl.ItemIndex := -1;
        chBDistinct.Enabled := True;
      end;

    rssDelete:
      begin
        pnlFromMacro.Visible := True;
        pnlWhereMacro.Visible := True;

        cbTmpTbl.Enabled := False;
        chBDistinct.Checked := False;
        chBDistinct.Enabled := False;
        if Assigned(FQBQuery) and (FQBQuery.SelectedTables.Count = 1) then
          FQBQuery.TmpTableName := FQBQuery.SelectedTables[0].lqObject.Name;
        edMacroInsert.Text := '';
        edMacroSelect.Text := '';
        edMacroOrder.Text := '';
        edMacroSelect.OnExit(nil);
      end;
  end;

  pnlInsertMacro.Top  := 1000;
  pnlSelectMacro.Top  := 1000;
  pnlFromMacro.Top    := 1000;
  pnlWhereMacro.Top   := 1000;
  pnlOrderByMacro.Top := 1000;

  if Assigned(FQBQuery) then
  begin
    FQBQuery.SQLStatement := TrwSQLStatementType(cbSQLType.ItemIndex);
    if Assigned(Sender) then
      SyncFields;
    SyncSQLResult;
  end;
end;

procedure TrwQueryBuilder.pcFieldsDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (DragObject.ObjectType = rdoField) and not FQBQuery.Union;
end;

procedure TrwQueryBuilder.lvFieldsDblClick(Sender: TObject);
begin
  actEditField.Execute;
end;

procedure TrwQueryBuilder.cbTmpTblChange(Sender: TObject);
begin
  if Assigned(FQBQuery) then
    FQBQuery.TmpTableName := cbTmpTbl.Text;
end;


procedure TrwQueryBuilder.actExpDictExecute(Sender: TObject);
var
  FS: TEvFileStream;
  i: Integer;
begin
  ChildTables(False);

  with DataDictionary do
  begin
    i := 0;
    while i < Tables.Count do
      if TrwDataDictTable(Tables[i]).TableType = rwtBuffer then
        DeleteTable(Tables[i])
      else
        i:=i+1;

    if SaveDialog.Execute then
    begin
      FS := TEvFileStream.Create(SaveDialog.FileName, fmCreate);
      try
        SaveToStream(FS);
        QBTemplates.SaveToStream(FS);
        TrwQBConsts(SQLConsts).SaveToStream(FS);
      finally
        FS.Free;
      end;
    end;
  end;
end;

procedure TrwQueryBuilder.actImpDictExecute(Sender: TObject);
var
  FS: TEvFileStream;
begin
  ChildTables(False);

  with DataDictionary do
  begin
    if OpenDialog.Execute then
    begin
      FS := TEvFileStream.Create(OpenDialog.FileName, fmOpenRead);
      try
        FQBQuery.Clear;
        LoadFromStream(FS);
        fl_upd_dict := True;

        if FS.Position < FS.Size then
        begin
          QBTemplates.LoadFromStream(FS);
          fl_upd_tmpl := True;
        end;

        if FS.Position < FS.Size then
        begin
          TrwQBConsts(SQLConsts).LoadFromStream(FS);
          fl_upd_const := True;
        end;

        FillTreeView;
      finally
        FS.Free;
      end;
    end;
  end;
end;

procedure TrwQueryBuilder.lvFieldsSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  TListView(Sender).OnClick(Sender);
  if FQBQuery.Union and Assigned(CurrentActiveTable) then
    CurrentActiveTable.UpdateFieldPosition;
end;


procedure TrwQueryBuilder.actAddSUBQueryExecute(Sender: TObject);
begin
  DragObject.TableName := qbEmbeddedQueryName;
  DragObject.ObjectType := rdoTable;
  DragObject.DropPoint.X := 10;
  DragObject.DropPoint.Y := 10;
  actNewTable.Execute;
  CleanUpDragObj;

  SyncContents(FQBQuery.SelectedTables[FQBQuery.SelectedTables.Count - 1].SUBQuery);
  tvQuery.Selected := NodeByQuery(FQBQuery.SelectedTables[FQBQuery.SelectedTables.Count - 1].SUBQuery);
  SwitchQuery;
end;


procedure TrwQueryBuilder.SyncFields;
var
  i: Integer;
  lMacros: array [1..5] of String;
  h: String;
begin
  if FQBQuery.Union then
    SyncUnionShowingFields(FQBQuery);

  lvFields.Items.BeginUpdate;
  try
    lvFields.Items.Clear;

    i := 0;
    while i < FQBQuery.ShowingFields.Count do
    begin
      AddVisualField(TrwQBShowingField(FQBQuery.ShowingFields.Items[i]));
      Inc(i);
    end;

    for i := Low(lMacros) to High(lMacros) do
      lMacros[i] := '';
    h := FQBQuery.Macros;
    i := 1;
    while Length(h) > 0 do
    begin
      lMacros[i] := GetNextStrValue(h, ',');
      Inc(i);
    end;
    edMacroSelect.Text := lMacros[1];
    edMacroFrom.Text   := lMacros[2];
    edMacroWhere.Text  := lMacros[3];
    edMacroOrder.Text  := lMacros[4];
    edMacroInsert.Text := lMacros[5];

    SyncSortFields;

    chBDistinct.Checked := FQBQuery.DistinctResult;
    cbSQLType.ItemIndex := Ord(FQBQuery.SQLStatement);
    cbSQLType.OnChange(nil);
    cbTmpTbl.ItemIndex := cbTmpTbl.Items.IndexOf(FQBQuery.TmpTableName);

    pnMainSQL.Visible := not WizardView and (FQBQuery = FMainQBQuery);
    pcFields.OnChange(nil);
  finally
    lvFields.Items.EndUpdate;
  end;

  lvFields.HideSelection := not FQBQuery.Union;
end;


procedure TrwQueryBuilder.SyncSortFields;
var
  i: Integer;
begin
  lvSort.Items.BeginUpdate;
  try
    lvSort.Items.Clear;

    for i := 0 to FQBQuery.SortFields.Count - 1 do
      AddVisualSortField(TrwQBSortField(FQBQuery.SortFields.Items[i]));

  finally
    lvSort.Items.EndUpdate;
  end;
end;

procedure TrwQueryBuilder.actAddJoinExecute(Sender: TObject);
begin
  SyncSQLResult;

  if DragObject.ObjectType = rdoField then
    QBAddJoin(FQBQuery, DragObject.TableOwner, DragObject.FieldName, DragObject.TableDest)
  else
    QBAddJoin(FQBQuery);

  pcFields.OnChange(nil);
  CleanUpDragObj;
end;


procedure TrwQueryBuilder.CleanUpDragObj;
begin
  DragObject.ObjectType := rdoNone;
  DragObject.MiscParams := qmpNone;
end;


function TrwQueryBuilder.CreateQueryNode(AQuery: TrwQBQuery): TTreeNode;

  function NewTabSheet: TTabSheet;
  var
    S: TrwQBScrollArea;
  begin
    Result := TTabSheet.Create(Self);
    Result.PageControl := pcSQL;
    Result.Tag := Integer(Pointer(AQuery));
    Result.TabVisible := False;

    S := TrwQBScrollArea.Create(Result);
    S.Color := $009A9A9A;
    S.Align := alClient;
    S.Parent := Result;
    S.OnClick := scbDsgnAreaClick;
    S.OnDragOver := scbDsgnAreaDragOver;
  end;


begin
  if Assigned(AQuery.ParentTable) then
    Result := NodeByQuery(TrwQBSelectedTables(AQuery.ParentTable.Collection).QBQuery)
  else
    Result := nil;

  NewTabSheet;

  Result := tvQuery.Items.AddChildObject(Result, AQuery.Description, AQuery);
  if AQuery.Union then
    Result.ImageIndex := 26
  else
    Result.ImageIndex := 2;
  Result.StateIndex := Result.ImageIndex;
  Result.SelectedIndex := Result.ImageIndex;
end;

procedure TrwQueryBuilder.pcFieldsChange(Sender: TObject);
begin
  if pcFields.ActivePage = tsSQL then
    memSQL.Text := FQBQuery.SQL(True)

  else if pcFields.ActivePage = tsResult then
    FResultGrid.DataSource := TrwQuery(FQBQuery.ResultDataset);
end;


procedure TrwQueryBuilder.actOpenSQLExecute(Sender: TObject);
var
  N: TTreeNode;
  i: Integer;
  h: String;
  ResRec: TsbResultRec;
  DS: TrwQuery;

  function MakeTime: String;
  var
    H, M, S, MS: Word;
  begin
    DecodeTime(MSecsToTimeStamp(ResRec.Time).Time, H, M, S, MS);
    Result := '';
    if (Result <> '') or (H > 0) then
      Result := Result + IntToStr(H) + ' h ';
    if (Result <> '') or (M > 0) then
      Result := Result + IntToStr(M) + ' min ';
    if (Result <> '') or (S > 0) then
      Result := Result + IntToStr(S) + ' sec ';
    if (Result = '') or (Result <> '') and (MS > 0) then
      Result := Result + IntToStr(MS) + ' msec';
  end;

begin
  N := tvQuery.Selected;

  if CheckErrors(FQBQuery) then
    Exit;

  tvQuery.Selected := N;
  tvQuery.OnChange(nil, nil);
  SyncSQLResult;

  if not Assigned(FSQLParams) then
    FSQLParams := TrwQBParams.Create(Self);

  FSQLParams.FillParams(FQBQuery.SQL(True));

  if FSQLParams.Params.ItemCount > 0 then
    if FSQLParams.ShowModal <> mrOK then
      Exit;

  pcFields.ActivePage := tsResult;
  pcFields.OnChange(nil);

  Application.ProcessMessages;
  if FQBQuery.SQLStatement = rssSelect then
  begin
    DS := TrwQuery(FQBQuery.ResultDataset);
    DS.Sort := '';
  end
  else
    DS := nil;

  if Assigned(FResultGrid.DataSource.DataSet) then
    FResultGrid.DataSource.DataSet.Close;

  ResRec := ShowSQLContents(FQBQuery.SQL(True), FSQLParams.Params, DS);
  memPlan.Text := 'PLAN:   ' + ResRec.Plan + #13#10#13#10'COST = ' + FormatFloat('#,##0', ResRec.Cost) +
                  #13#10'TIME = ' + MakeTime;

  if WizardView and Assigned(DS) then
    for i := 0 to DS.VCLDataSet.Fields.Count - 1 do
    begin
      if FQBQuery.ShowingFields[i].FieldAlias = '' then
        h := FQBQuery.ShowingFields[i].GetFieldName(True)
      else
        h := FQBQuery.ShowingFields[i].FieldAlias;

      DS.VCLDataSet.Fields[i].DisplayLabel := StringReplace(h, '_', ' ', [rfReplaceAll]);
    end;
end;


procedure TrwQueryBuilder.ChildTables(AVisible: Boolean);
var
  rwT: TrwDataDictTable;
begin
  if AVisible and Assigned(CurrentActiveTable) then
  begin
    if not CurrentActiveTable.Table.IsSUBQuery then
    begin
      rwT := TrwDataDictTable(DataDictionary.Tables.TableByName(CurrentActiveTable.Table.TableName));
      FillChildTreeView(rwT);

      if WizardView then
        lCurrTable.Caption := rwT.DisplayName
      else
        lCurrTable.Caption := rwT.Name;
    end
    else
    begin
      lCurrTable.Caption := 'SubQuery';
      tvChildTables.Items.Clear;
    end;
  end;

  if not AVisible and FQBQuery.Union then
  begin
    tvChildTables.Items.Clear;
    lCurrTable.Caption := 'UNION';
    pnlMasterTables.Visible := True;
    pnlTables.Visible := False;
  end
  else
  begin
    pnlMasterTables.Visible := AVisible;
    pnlTables.Visible := not AVisible;
  end;
end;


procedure TrwQueryBuilder.lvSortDblClick(Sender: TObject);
begin
  actEditSortField.Execute;
end;

procedure TrwQueryBuilder.lvFieldsEndDrag(Sender, Target: TObject; X, Y: Integer);
var
  li, li2: TListItem;
  i, j, k: Integer;
  T: TrwQBSelectedTable;
  SF: TrwQBShowingFields;
begin
  if (Sender = Target) and (Sender = lvFields) then
  begin
    SyncSQLResult;

    li := nil;
    for i := 0 to lvFields.Items.Count - 1 do
      if lvFields.Items[i].Data = DragObject.Field then
      begin
        li := lvFields.Items[i];
        break;
      end;

    li2 := lvFields.GetItemAt(X, Y);
    if not Assigned(li2) or not Assigned(li)  then
      Exit;

    if FQBQuery.Union then
    begin
      j := TrwQBShowingField(li.Data).Index;
      k := TrwQBShowingField(li2.Data).Index;
      for i := 0 to FQBQuery.SelectedTables.Count - 1 do
      begin
        SF := FQBQuery.SelectedTables[i].SUBQuery.ShowingFields;
        if (SF.Count > j) and (SF.Count > k) then
          SF[j].Index := SF[k].Index;
        PostMessage(TForm(FQBQuery.SelectedTables[i].VisualComponent).Handle, RW_QBTABLE_REFRESH, 0, 0);
      end;
    end
    else
      TrwQBShowingField(li.Data).Index := TrwQBShowingField(li2.Data).Index;

    i := TrwQBShowingField(li.Data).Index;
    SyncFields;
    lvFields.Items[i].Selected := True;

    T := FQBQuery.ParentTable;
    while Assigned(T) do
    begin
      PostMessage(TForm(T.VisualComponent).Handle, RW_QBTABLE_REFRESH, 0, 0);
      T := TrwQBSelectedTables(T.Collection).QBQuery.ParentTable;
    end;
  end;
end;


procedure TrwQueryBuilder.tvQueryChange(Sender: TObject; Node: TTreeNode);
begin
  if Assigned(tvQuery.Selected) then
    SwitchQuery;
end;

procedure TrwQueryBuilder.tvQueryEdited(Sender: TObject; Node: TTreeNode; var S: String);
begin
  TrwQBQuery(Node.Data).Description := S;
  if Assigned(TrwQBQuery(Node.Data).ParentTable) then
    TrwQBTable(TrwQBQuery(Node.Data).ParentTable.VisualComponent).UpdateCaption;
end;


procedure TrwQueryBuilder.actDelSUBQueryExecute(Sender: TObject);
var
  T: TrwQBSelectedTable;
begin
  if not Assigned(tvQuery.Selected) or
     not Assigned(TrwQBQuery(tvQuery.Selected.Data).ParentTable) or
    (MessageDlg('You are about to delete SubQuery. Proceed?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes) then
    Exit;

  SyncSQLResult;

  T := TrwQBQuery(tvQuery.Selected.Data).ParentTable;
  tvQuery.Selected := tvQuery.Selected.Parent;
  SwitchQuery;
  TrwQBTable(T.VisualComponent).RemoveTable;
end;

procedure TrwQueryBuilder.actEdSubQueryDescrExecute(Sender: TObject);
var
  h: String;
begin
  if not Assigned(tvQuery.Selected) then
    Exit;

  h := TrwQBQuery(tvQuery.Selected.Data).Description;
  if InputQuery('SubQuery Description', 'Type text of description', h) then
  begin
    tvQuery.Selected.Text := Trim(h);
    TrwQBQuery(tvQuery.Selected.Data).Description := tvQuery.Selected.Text;
    if Assigned(TrwQBQuery(tvQuery.Selected.Data).ParentTable) then
      TrwQBTable(TrwQBQuery(tvQuery.Selected.Data).ParentTable.VisualComponent).UpdateCaption;
  end;
end;


procedure TrwQueryBuilder.SyncSQLResult;
begin
  if Assigned(FQBQuery) then
    TrwQuery(FQBQuery.ResultDataset).Close;
  memPlan.Text := '';
end;


function TrwQueryBuilder.CheckCortProduct(AQuery: TrwQBQuery): Boolean;
var
  i, j: Integer;
  fl: Boolean;

  function CheckPart(ATable: TrwQBSelectedTable; AExpr: TrwQBExpression; AAllBesides: Boolean): Variant;
  var
    i: Integer;
  begin
    Result := Unassigned;
    for i := 0 to AExpr.Count - 1 do
    begin
      if AExpr[i].ItemType = eitField then
      begin
        Result := AnsiSameText(AExpr[i].Description, ATable.InternalName);
        if AAllBesides then
          Result := not Result;
      end;

      if Result then
        break;
    end;
  end;


  function CheckFilterNode(ATable: TrwQBSelectedTable; AFilterNode: TrwQBFilterNode): Boolean;
  var
    i, j: Integer;
    lf, rf: Variant;
  begin
    Result := False;

    for i := 0 to AFilterNode.Conditions.Count - 1 do
    begin
      lf := CheckPart(ATable, TrwQBFilterCondition(AFilterNode.Conditions.Items[i]).LeftPart, False);
      if not VarIsEmpty(lf) then
        for j := 0 to TrwQBFilterCondition(AFilterNode.Conditions.Items[i]).RightPart.Count -1 do
        begin
          rf :=  CheckPart(ATable,
            TrwQBExpression(TrwQBFilterCondition(AFilterNode.Conditions.Items[i]).RightPart.Items[j]), lf);
          if not VarIsEmpty(rf) then
            if rf <> lf then
            begin
              Result := True;
              Exit;
            end;
        end;
    end;

    for i := 0 to AFilterNode.Nodes.Count - 1 do
    begin
      Result := CheckFilterNode(ATable, TrwQBFilterNode(AFilterNode.Nodes.Items[i]));
      if Result then
        Exit;
    end;
  end;

begin
  Result := False;

  if (AQuery.SelectedTables.Count <= 1) or AQuery.Union then
    Exit;

  for i := 0 to AQuery.SelectedTables.Count - 1 do
  begin
    if AQuery.SelectedTables[i].IsSUBQuery and CheckCortProduct(AQuery.SelectedTables[i].SUBQuery) then
    begin
      Result := True;
      Exit;
    end;

    fl := False;
    for j := 0 to AQuery.Joins.Count - 1 do
      if (AQuery.Joins[j].LeftTable = AQuery.SelectedTables[i]) or
         (AQuery.Joins[j].RightTable = AQuery.SelectedTables[i]) then
      begin
        fl := True;
        Break;
      end;

    if not fl then
      for j := 0 to AQuery.SelectedTables.Count - 1 do
      begin
        fl := CheckFilterNode(AQuery.SelectedTables[i], AQuery.SelectedTables[j].Filter);
        if fl then
          break;
      end;

    if not fl then
    begin
      OpenSelectedTable(AQuery.SelectedTables[i]);
      Result := True;
      Exit;
    end;
  end;
end;


procedure TrwQueryBuilder.OpenSelectedTable(ATable: TrwQBSelectedTable);
begin
  tvQuery.Selected := NodeByQuery(TrwQBSelectedTables(ATable.Collection).QBQuery);
  SwitchQuery;
  TrwQBTable(ATable.VisualComponent).Perform(RW_QBTABLE_OPEN, 0, 0);
end;


function TrwQueryBuilder.CheckMultJoins(AQuery: TrwQBQuery): Boolean;
var
  i, j, k: Integer;
  L: TList;
begin
  Result := False;

  if AQuery.Union then
    Exit;

  L := TList.Create;
  try
    for i := 0 to AQuery.SelectedTables.Count - 1 do
    begin
      if AQuery.SelectedTables[i].IsSUBQuery and CheckMultJoins(AQuery.SelectedTables[i].SUBQuery) then
      begin
        Result := True;
        Exit;
      end;

      L.Clear;
      for j := 0 to AQuery.Joins.Count - 1 do
        if (AQuery.Joins[j].LeftTable = AQuery.SelectedTables[i]) then
          L.Add(AQuery.Joins[j]);

      for j := 0 to L.Count - 2 do
        for k := j + 1 to L.Count - 1  do
          if (TrwQBJoin(L[j]).RightTable = TrwQBJoin(L[k]).RightTable) and
             (TrwQBJoin(L[j]).RightTableFields = TrwQBJoin(L[k]).RightTableFields) then
          begin
            OpenSelectedTable(AQuery.SelectedTables[i]);
            Result := True;
            Exit;
          end;

      L.Clear;
      for j := 0 to AQuery.Joins.Count - 1 do
        if (AQuery.Joins[j].RightTable = AQuery.SelectedTables[i]) then
          L.Add(AQuery.Joins[j]);

      for j := 0 to L.Count - 2 do
        for k := j + 1 to L.Count - 1  do
          if (TrwQBJoin(L[j]).LeftTable = TrwQBJoin(L[k]).LeftTable) and
             (TrwQBJoin(L[j]).LeftTableFields = TrwQBJoin(L[k]).LeftTableFields) then
          begin
            OpenSelectedTable(AQuery.SelectedTables[i]);
            Result := True;
            Exit;
          end;
    end;

  finally
    L.Free
  end;
end;


function TrwQueryBuilder.CheckErrors(AQuery: TrwQBQuery): Boolean;
var
  t: String;
begin
  Result := True;

  if CheckBasicErrors(AQuery) then
  begin
    ShowMessage('Query has wrong structure!');
    Exit;
  end;

  if CheckUnion(AQuery) then
  begin
    t := CurrentActiveTable.Table.SUBQuery.Description;
    if Length(t) = 0 then
      t := '#' + IntToStr(CurrentActiveTable.Table.Index);

    ShowMessage('Query "' + t + '" has wrong structure!');
    Exit;
  end;


  if CheckCortProduct(AQuery) then
  begin
    if Length(CurrentActiveTable.Table.Alias) = 0 then
      t := CurrentActiveTable.Table.TableName
    else
      t := CurrentActiveTable.Table.Alias;

    if MessageDlg('Table "' + t + '" does not have any joins!' + #13 +
       'A result of the query(subquery) will be cartesian product. In most cases it is an error.' + #13 +
       'Proceed without correction?',
       mtWarning, [mbYes, mbNo], 0) <> mrYes then
      Exit;
  end;


  if CheckMultJoins(AQuery) then
  begin
    if Length(CurrentActiveTable.Table.Alias) = 0 then
      t := CurrentActiveTable.Table.TableName
    else
      t := CurrentActiveTable.Table.Alias;

    if MessageDlg('The table "' + t + '" has multiple joins to the same table! In most cases it is an error.' + #13 +
       'Proceed without correction?', mtWarning, [mbYes, mbNo], 0) <> mrYes then
      Exit;
  end;

  Result := False;
end;

procedure TrwQueryBuilder.UpdateVisualField(AField: TrwQBShowingField; AItem: TListItem);
var
  t, f: String;
  Tbl: TrwQBSelectedTable;
begin
  if (AField.Count = 1) and (TrwQBExpressionItem(AField.Items[0]).ItemType = eitField) then
  begin
    Tbl := FQBQuery.SelectedTables.TableByInternalName(TrwQBExpressionItem(AField.Items[0]).Description);

    if FQBQuery.Union then
      t := ''
    else
      t := Tbl.GetTableName(WizardView);

    f := AField.GetFieldName(WizardView);
  end
  else
  begin
    t := '';
    f := AField.TextForDesign(WizardView);
  end;

  AItem.Data := AField;
  AItem.Caption := f;

  if AField.Attribute <> nil then
    AItem.SubItems[0] := DDTypeIntoString(AField.Attribute.FieldType)
  else
    AItem.SubItems[0] := QBTypeIntoString(AField.ExprType);

  if not FQBQuery.Union then
    AItem.SubItems[1] := AField.FieldAlias;

  AItem.SubItems[2] := t;
  AItem.SubItems[3] := AField.TmpFieldName;
end;

procedure TrwQueryBuilder.actAddParentQueryExecute(Sender: TObject);
begin
  AddParentQuery(False);
end;


function TrwQueryBuilder.GetUniqueTableAlias: String;
var
  l: Integer;

  function AliasExists(ATables: TrwQBSelectedTables; AAlias: String): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := 0 to ATables.Count - 1 do
    begin
      Result := AnsiSameText(ATables[i].Alias, AAlias);
      if not Result and ATables[i].IsSUBQuery then
        Result := AliasExists(ATables[i].SUBQuery.SelectedTables, AAlias);
      if Result then
        break;
    end;
  end;

begin
  l := 1;
  while True do
  begin
    Result := 't' + IntToStr(l);
    if not AliasExists(FMainQBQuery.SelectedTables, Result) then
      break;
    Inc(l)
  end;
end;

procedure TrwQueryBuilder.actHidePanelsExecute(Sender: TObject);
begin
  if btnHidePnls.Down then
  begin
    Panel2.Hide;
    pcFields.Hide;
    Splitter1.Hide;
    Splitter2.Hide;
  end

  else
  begin
    Panel2.Show;
    pcFields.Show;
    Splitter1.Show;
    Splitter2.Show;
    Splitter1.Left := Panel2.Width + 10;
    Splitter2.Top := pcFields.Top - 10;
  end;
end;

procedure TrwQueryBuilder.FillTemplates;
var
  i: Integer;
  Grp: TrwDictGroup;

  procedure FillGroupsContent;
  var
    i,j: Integer;
  begin
    for i := 0 to FListTmplGroups.Count - 1 do
    begin
      if TrwDictGroup(FListTmplGroups.Items[i]).Name = '' then
        Continue;

      TrwDictGroup(FListTmplGroups.Items[i]).MenuItem.DeleteChildren;
      for j := 0 to TrwDictGroup(FListTmplGroups.Items[i]).Count - 1 do
        AddTemplateToTree(TrwQBTemplate(TrwDictGroup(FListTmplGroups.Items[i])[j]),
          TrwDictGroup(FListTmplGroups.Items[i]).MenuItem);
    end;

    for i := 0 to FListTmplGroups.Count - 1 do
    begin
      if TrwDictGroup(FListTmplGroups.Items[i]).Name <> '' then
        Continue;

      for j := 0 to TrwDictGroup(FListTmplGroups.Items[i]).Count - 1 do
        AddTemplateToTree(TrwQBTemplate(TrwDictGroup(FListTmplGroups.Items[i])[j]), FListTmplGroups.RootNode);
    end;
  end;

begin
  FTemplateNode := tvTables.Items.AddObject(nil, 'Templates', nil);
  FTemplateNode.ImageIndex := 24;
  FTemplateNode.SelectedIndex := FTemplateNode.ImageIndex;
  FTemplateNode.StateIndex := FTemplateNode.ImageIndex;
  FListTmplGroups.RootNode := FTemplateNode;

  for i := 0 to QBTemplates.Count - 1 do
  begin
    Grp := FListTmplGroups.IndexOf(QBTemplates[i].Group);
    if not Assigned(Grp) then
    begin
      Grp := TrwDictGroup(FListTmplGroups.Add);
      Grp.Name := QBTemplates[i].Group;
    end;

    Grp.Add(QBTemplates[i]);
  end;

  FillGroupsContent;
end;


procedure TrwQueryBuilder.actSaveExecute(Sender: TObject);
var
  FS: TEvFileStream;
begin
  if (FQBQuery <> FMainQBQuery) and
     (MessageDlg('You are about saving ONLY selected SubQuery. Proceed?', mtWarning,
      [mbYes, mbNo], 0) <> mrYes) then
    Exit;


  if sdQuery.Execute then
  begin
    FS := TEvFileStream.Create(sdQuery.FileName, fmCreate);
    try
      FQBQuery.SaveToStream(FS);
    finally
      FS.Free;
    end;
  end;
end;


procedure TrwQueryBuilder.actOpenExecute(Sender: TObject);
var
  FS: TEvFileStream;
begin
  if (FQBQuery <> FMainQBQuery) and
     (MessageDlg('You are about loading ONLY selected SubQuery. Proceed?', mtWarning,
      [mbYes, mbNo], 0) <> mrYes) then
    Exit;

  if odQuery.Execute then
  begin
    Update;
    if Assigned(FQBQuery.ParentTable) then
      RemoveAllExprItems(TrwQBSelectedTables(FQBQuery.ParentTable.Collection).QBQuery, '', FQBQuery.ParentTable.InternalName);
    FS := TEvFileStream.Create(odQuery.FileName, fmOpenRead);
    try
      FQBQuery.ReadFromStream(FS);
    finally
      FS.Free;
    end;
    SyncMainQuery;
  end;
end;


procedure TrwQueryBuilder.SyncMainQuery;
var
  i: Integer;
begin
  Busy;
  ClearErrors;
  tvQuery.Items.BeginUpdate;
  try
    Refreshing := True;
    for i := pcSQL.PageCount - 1 downto 0 do
      DeleteTabSheet(pcSQL.Pages[i]);
    tvQuery.Items.Clear;
    Refreshing := False;

    if FixUpQuery(FMainQBQuery) then
      AddError('Some tables have been removed. Please check your query.');

    SyncContents(FMainQBQuery);
    tvQuery.Items[0].Selected := True;
    SwitchQuery;
    tvQuery.Items[0].Expand(True);
  finally
    tvQuery.Items.EndUpdate;
    tvQuery.Refresh;
    Ready;
    ShowErrors;
  end;
end;


procedure TrwQueryBuilder.tvQueryDeletion(Sender: TObject;  Node: TTreeNode);
begin
  if not Refreshing then
    DeleteTabSheet(TabSheetByQuery(TrwQBQuery(Node.Data)));
end;


procedure TrwQueryBuilder.SwitchQuery;
begin
  CloseCurrentTable;
  pcSQL.ActivePage := TabSheetByQuery(TrwQBQuery(tvQuery.Selected.Data));
  FQBQuery := TrwQBQuery(tvQuery.Selected.Data);
  SyncFields;
  if FQBQuery.Union then
    SyncUnionTables;
  ChildTables(False);
end;


function TrwQueryBuilder.TabSheetByQuery(AQuery: TrwQBQuery): TTabSheet;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to pcSQL.PageCount - 1 do
    if Pointer(pcSQL.Pages[i].Tag) = AQuery then
    begin
      Result := pcSQL.Pages[i];
      break;
    end;
end;


function TrwQueryBuilder.NodeByQuery(AQuery: TrwQBQuery): TTreeNode;
var
  i: Integer;
begin
  Result := nil;
  tvQuery.Items.GetFirstNode;  //bag
  for i := 0 to tvQuery.Items.Count - 1 do
  begin
    if tvQuery.Items[i].Data = AQuery then
    begin
      Result := tvQuery.Items[i];
      break;
    end;
  end;
end;

function TrwQueryBuilder.AddTemplateToTree(ATmpl: TrwQBTemplate; ANode: TTreeNode): TTreeNode;
begin
  Result := TTreeView(ANode.TreeView).Items.AddChildObject(ANode, ATmpl.Name, ATmpl);
  Result.SelectedIndex := 2;
  Result.StateIndex := Result.SelectedIndex;
  Result.ImageIndex := Result.SelectedIndex;
end;

procedure TrwQueryBuilder.CloseCurrentTable;
begin
  if Assigned(CurrentActiveTable) then
    CurrentActiveTable.Perform(RW_QBTABLE_CLOSE, 0, 0);
  CurrentActiveTable := nil;
end;

procedure TrwQueryBuilder.tvChildTablesChange(Sender: TObject; Node: TTreeNode);
begin
  if Assigned(Node.Data) then
  begin
    if TObject(Node.Data) is TrwDataDictTable then
      memDescr.Text := TrwDataDictTable(Node.Data).Notes
    else if TObject(Node.Data) is TrwDataDictField then
      memDescr.Text := TrwDataDictField(Node.Data).Notes
    else if TObject(Node.Data) is TrwQBTemplate then
      memDescr.Text := TrwQBTemplate(Node.Data).Description
    else
      memDescr.Text := '';
  end    
  else
    memDescr.Text := '';
end;

function TrwQueryBuilder.FixUpQuery(AQuery: TrwQBQuery): Boolean;
var
  i: Integer;
begin
  Result := False;
  i := 0;
  while i <= AQuery.SelectedTables.Count - 1 do
  begin
    if not AQuery.SelectedTables[i].IsSUBQuery
       and (AQuery.SelectedTables[i].lqObject = nil) then
    begin
      RemoveAllExprItems(AQuery, '', AQuery.SelectedTables[i].InternalName);
      AQuery.SelectedTables[i].Free;
      Result := True;
      Continue;
    end;

    if AQuery.SelectedTables[i].IsSUBQuery then
      if FixUpQuery(AQuery.SelectedTables[i].SUBQuery) then
        Result := True;
    Inc(i);
  end;
end;



{ TrwQBScrollArea }

constructor TrwQBScrollArea.Create(AOwner: TComponent);
begin
  inherited;
  FCanvas := TControlCanvas.Create;
  FCanvas.Control := Self;
  DoubleBuffered := True;
end;


destructor TrwQBScrollArea.Destroy;
begin
  FCanvas.Free;
  inherited;
end;




{
     _________1_________
    |                   |
   4|      Left         |2
    |___________________| \
              3            \
                            \
                             \
                     _________1_________
                    |                   |
                   4|      Right        |2
                    |___________________|
                              3

}



function TrwQBScrollArea.JoinAt(X, Y: Integer): TrwQBJoin;
var
  i: Integer;
  K, B: Extended;
  P: TPoint;
  R: TRect;
  fl: Boolean;
begin
  Result := nil;

  with FQBFrm do
    for i := 0 to FQBQuery.Joins.Count - 1 do
    begin
      if Abs(FQBQuery.Joins[i].VisualLine.Right - FQBQuery.Joins[i].VisualLine.Left) < 5 then
        fl := Abs(FQBQuery.Joins[i].VisualLine.Right - X) <= 5

      else
      begin
        K := (FQBQuery.Joins[i].VisualLine.Bottom - FQBQuery.Joins[i].VisualLine.Top) /
          (FQBQuery.Joins[i].VisualLine.Right - FQBQuery.Joins[i].VisualLine.Left);
        B := FQBQuery.Joins[i].VisualLine.Top - K * FQBQuery.Joins[i].VisualLine.Left;

        if K = 0 then
          fl := Abs(FQBQuery.Joins[i].VisualLine.Bottom - Y) <= 5
        else
          fl := (Abs(X * K + B - Y) <= 5) or (Abs((Y - B)/K - X) <= 5);
      end;

      if fl then
      begin
        if FQBQuery.Joins[i].VisualLine.Bottom > FQBQuery.Joins[i].VisualLine.Top then
        begin
          R.Top := FQBQuery.Joins[i].VisualLine.Top - 5;
          R.Bottom := FQBQuery.Joins[i].VisualLine.Bottom + 5;
        end
        else
        begin
          R.Bottom := FQBQuery.Joins[i].VisualLine.Top + 5;
          R.Top := FQBQuery.Joins[i].VisualLine.Bottom - 5;
        end;

        if FQBQuery.Joins[i].VisualLine.Right > FQBQuery.Joins[i].VisualLine.Left then
        begin
          R.Left := FQBQuery.Joins[i].VisualLine.Left - 5;
          R.Right := FQBQuery.Joins[i].VisualLine.Right + 5;
        end
        else
        begin
          R.Right := FQBQuery.Joins[i].VisualLine.Left + 5;
          R.Left := FQBQuery.Joins[i].VisualLine.Right - 5;
        end;

        P := Point(X, Y);
        fl := PtInRect(R, P);
      end;

      if fl then
      begin
        Result := FQBQuery.Joins[i];
        Exit;
      end;
    end;
end;

procedure TrwQBScrollArea.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  J: TrwQBJoin;
begin
  inherited;

  J := JoinAt(X, Y);
  if Assigned(J) then
    FQBFrm.ShowJoinHint(J, ClientToScreen(Point(X, Y)))
  else
    FQBFrm.HideJoinHint;
end;


procedure TrwQBScrollArea.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  J: TrwQBJoin;
  P: TPoint;
begin
  inherited;

  if Button = mbRight then
  begin
    with FQBFrm do
    begin
      J := JoinAt(X, Y);
      P := Self.ClientToScreen(Point(X, Y));
      if Assigned(J) then
      begin
        pmJoin.Tag := J.Index;
        pmJoin.PopUp(P.X, P.Y);
      end
      else
      begin
        DragObject.DropPoint := Point(X, Y);
        pmPasteTable.PopUp(P.X, P.Y);
      end;
    end;
  end;
end;


procedure TrwQBScrollArea.WMPaint(var Message: TMessage);
var
  Joins: TrwQBJoins;
  i: Integer;
  Lt, Rt: TRect;
  L: TrwQBJoinLine;

  procedure DrawEndLine(APNum: Integer; APoint: TPoint);
  const
    bH = 3;
    bW = 8;
  var
    R: TRect;
  begin
    FCanvas.Brush.Color := clBlack;
    case APNum of
      1: begin
           R.Top := APoint.Y - bH;
           R.Left := APoint.X - bW div 2;
           R.Bottom := R.Top + bH;
           R.Right := R.Left + bW;
         end;

      2: begin
           R.Top := APoint.Y - bW div 2;
           R.Left := APoint.X;
           R.Bottom := R.Top + bW;
           R.Right := R.Left + bH;
         end;

      3: begin
           R.Top := APoint.Y;
           R.Left := APoint.X - bW div 2;
           R.Bottom := R.Top + bH;
           R.Right := R.Left + bW;
         end;

      4: begin
           R.Top := APoint.Y - bW div 2;
           R.Left := APoint.X - bH;
           R.Bottom := R.Top + bW;
           R.Right := R.Left + bH;
         end;
    end;

    FCanvas.FillRect(R);
  end;


  procedure DrawArrow(AJoinType: TrwQBJoinType);
  var
    X, Y, n: Integer;
  begin
    FCanvas.Pen.Width := 1;
    FCanvas.Brush.Color := Color;
    FCanvas.Pen.Style := psSolid;

    if AJoinType = rjtJoin then
      FCanvas.Pen.Color := clBlack
    else
      FCanvas.Pen.Color := clMaroon;

    FCanvas.MoveTo(L.LPoint.X, L.LPoint.Y);

    case AJoinType of
      rjtJoin:
        FCanvas.LineTo(L.RPoint.X, L.RPoint.Y);

      rjtLeftJoin:
        begin
          n := L.Length div 2;
          X := Abs(Round(n * Cos(L.Angle)));
          Y := Abs(Round(n * Sin(L.Angle)));
          if (L.Angle < -Pi/2) or (L.Angle > Pi/2) then
            X := -X;
          if L.Angle > 0 then
            Y := -Y;
          FCanvas.LineTo(L.RPoint.X - X, L.RPoint.Y - Y);

          FCanvas.Pen.Style := psDot;
          FCanvas.LineTo(L.RPoint.X, L.RPoint.Y);
        end;

      rjtRightJoin:
        begin
          FCanvas.Pen.Style := psDot;
          n := L.Length div 2;
          X := Abs(Round(n * Cos(L.Angle)));
          Y := Abs(Round(n * Sin(L.Angle)));
          if (L.Angle < -Pi/2) or (L.Angle > Pi/2) then
            X := -X;
          if L.Angle > 0 then
            Y := -Y;
          FCanvas.LineTo(L.LPoint.X + X, L.LPoint.Y + Y);

          FCanvas.Pen.Style := psSolid;
          FCanvas.LineTo(L.RPoint.X, L.RPoint.Y);
        end;
    end;

    DrawEndLine(L.LPointNum, L.LPoint);
    DrawEndLine(L.RPointNum, L.RPoint);
  end;

  procedure FillCalcLine(APoint: TPoint; ALPointNum: Byte);
  var
    P: TPoint;
    Ind: Integer;

    procedure SetLine(ARPointNum: Byte);
    var
      Ang: Extended;
      Rp: TPoint;
    begin
      if ALPointNum = ARPointNum then
        FCalcLines[Ind].Length := MaxInt
      else
      begin
        FCalcLines[Ind].LPoint := APoint;
        FCalcLines[Ind].LPointNum := ALPointNum;
        FCalcLines[Ind].RPoint := P;
        FCalcLines[Ind].RPointNum := ARPointNum;
        FCalcLines[Ind].Length := Round(Sqrt(Sqr(APoint.X - P.X) + Sqr(APoint.Y - P.Y)));


        Rp.X := FCalcLines[Ind].RPoint.X - FCalcLines[Ind].LPoint.X;
        Rp.Y := FCalcLines[Ind].LPoint.Y - FCalcLines[Ind].RPoint.Y;

        if Rp.X = 0 then
          if Rp.Y > 0 then
            Ang := Pi/2
          else
            Ang := -Pi/2
        else
          Ang := ArcTan2(Rp.Y, Rp.X);

        FCalcLines[Ind].Angle := Ang;
      end;
    end;

  begin
    Ind := ALPointNum * 4 - 3;
    P := Point(Rt.Left + (Rt.Right - Rt.Left) div 2, Rt.Top);
    SetLine(1);

    Inc(Ind);
    P := Point(Rt.Right, Rt.Top + (Rt.Bottom - Rt.Top) div 2);
    SetLine(2);

    Inc(Ind);
    P := Point(Rt.Left + (Rt.Right - Rt.Left) div 2, Rt.Bottom);
    SetLine(3);

    Inc(Ind);
    P := Point(Rt.Left, Rt.Top + (Rt.Bottom - Rt.Top) div 2);
    SetLine(4);
  end;

  procedure FillCalLinesArr;
  var
    P: TPoint;
  begin
    P := Point(Lt.Left + (Lt.Right - Lt.Left) div 2, Lt.Top);
    FillCalcLine(P, 1);

    P := Point(Lt.Right, Lt.Top + (Lt.Bottom - Lt.Top) div 2);
    FillCalcLine(P, 2);

    P := Point(Lt.Left + (Lt.Right - Lt.Left) div 2, Lt.Bottom);
    FillCalcLine(P, 3);

    P := Point(Lt.Left, Lt.Top + (Lt.Bottom - Lt.Top) div 2);
    FillCalcLine(P, 4);
  end;


  procedure FindBestLine;
  const
    MinAng = Pi/10;
  var
    i, j: Integer;
  begin
    for i := Low(FCalcLines) to High(FCalcLines) - 1 do
      for j := i + 1 to High(FCalcLines) do
        if FCalcLines[i].Length > FCalcLines[j].Length then
        begin
          L := FCalcLines[i];
          FCalcLines[i] := FCalcLines[j];
          FCalcLines[j] := L;
        end;

    j := 1;
    for i := Low(FCalcLines) to High(FCalcLines) do
    begin
        if not (
           (FCalcLines[i].RPointNum = 4) and not (Abs(FCalcLines[i].Angle) < Pi/2 - MinAng) or
           (FCalcLines[i].RPointNum = 2) and not (Abs(FCalcLines[i].Angle) > Pi/2 + MinAng) or
           (FCalcLines[i].LPointNum = 2) and not (Abs(FCalcLines[i].Angle) < Pi/2 - MinAng) or
           (FCalcLines[i].LPointNum = 4) and not (Abs(FCalcLines[i].Angle) > Pi/2 + MinAng) or
           (FCalcLines[i].RPointNum = 1) and not ((FCalcLines[i].Angle > -Pi + MinAng) and (FCalcLines[i].Angle < -MinAng)) or
           (FCalcLines[i].RPointNum = 3) and not ((FCalcLines[i].Angle < Pi - MinAng) and (FCalcLines[i].Angle > MinAng)) or
           (FCalcLines[i].LPointNum = 1) and not ((FCalcLines[i].Angle > MinAng) and (FCalcLines[i].Angle < Pi - MinAng)) or
           (FCalcLines[i].LPointNum = 3) and not ((FCalcLines[i].Angle < -MinAng) and (FCalcLines[i].Angle > -Pi + MinAng))
           )  and (FCalcLines[i].Length < MaxInt)  then
        begin
          j := i;
          break;
        end;
    end;

    L := FCalcLines[j];
  end;


begin
  inherited;

  Joins := TrwQBQuery(Pointer(TTabSheet(Parent).Tag)).Joins;

  for i := 0 to Joins.Count - 1 do
  begin
    Lt := TControl(Joins[i].LeftTable.VisualComponent).BoundsRect;
    Rt := TControl(Joins[i].RightTable.VisualComponent).BoundsRect;
    FillCalLinesArr;
    FindBestLine;
    Joins[i].VisualLine := Rect(L.LPoint.X, L.LPoint.Y, L.RPoint.X, L.RPoint.Y);
    DrawArrow(Joins[i].JoinType);
  end;
end;

procedure TrwQueryBuilder.ShowJoinHint(AJoin: TrwQBJoin; ATopLeft: TPoint);
begin
  if Assigned(FJoinHintWnd) then
    if FJoinHintWnd.FJoin <> AJoin then
      HideJoinHint
    else
      FJoinHintWnd.ShowJoinHint(AJoin, ATopLeft)

  else
  begin
    FJoinHintWnd := TrwQBJoinHintWindow.Create(Self);
    FJoinHintWnd.ShowJoinHint(AJoin, ATopLeft);
  end;
end;

procedure TrwQueryBuilder.HideJoinHint;
begin
  if Assigned(FJoinHintWnd) then
  begin
    FJoinHintWnd.Free;
    FJoinHintWnd := nil;
  end;
end;


{ TrwQBJoinHintWindow }

procedure TrwQBJoinHintWindow.ShowJoinHint(AJoin: TrwQBJoin; AMousePos: TPoint);
var
  t: String;
  R: TRect;
begin
  if FJoin = AJoin then
  begin
    Left := AMousePos.X - Width div 2;
    Top := AMousePos.Y + 20;
    Exit;
  end;

  FJoin := AJoin;
  t := AJoin.TextForDesign(FQBFrm.WizardView);
  R.Left := 0;
  R.Top := 0;
  DrawText(Canvas.Handle, PChar(t), -1, R,
    DT_LEFT or DT_NOPREFIX or DrawTextBiDiModeFlagsReadingOnly or DT_CALCRECT);
  R.Left := AMousePos.X - R.Right div 2;
  R.Top := AMousePos.Y + 20;
  R.Right := R.Right + R.Left + 6;
  R.Bottom := R.Bottom + R.Top + 3;
  ActivateHintData(R, t, nil);
end;

procedure TrwQueryBuilder.miOUTERrClick(Sender: TObject);
var
  J: TrwQBJoin;
begin
  SyncSQLResult;

  J := FQBQuery.Joins[pmJoin.Tag];

  TMenuItem(Sender).Checked := True;
  if Sender = miINNER then
    J.JoinType := rjtJoin
  else if Sender = miOUTERl then
    J.JoinType := rjtRightJoin
  else if Sender = miOUTERr then
    J.JoinType := rjtLeftJoin;

  RefreshTableArea;
  pcFields.OnChange(nil);
end;

procedure TrwQueryBuilder.pmJoinPopup(Sender: TObject);
var
  J: TrwQBJoin;
  h: String;
begin
  HideJoinHint;
  J := FQBQuery.Joins[pmJoin.Tag];

  h := J.LeftTable.GetTableName(WizardView, True);
  if J.LeftTable.Alias <> '' then
    h := J.LeftTable.Alias + '   ' + h;
  miOUTERl.Caption := 'OUTER (' + h + ')';


  h := J.RightTable.GetTableName(WizardView, True);
  if J.RightTable.Alias <> '' then
    h := J.RightTable.Alias + '   ' + h;
  miOUTERr.Caption := 'OUTER (' + h + ')';

  case J.JoinType of
    rjtJoin:      miINNER.Checked := True;
    rjtRightJoin: miOUTERl.Checked := True;
    rjtLeftJoin:  miOUTERr.Checked := True;
  end;
end;

procedure TrwQueryBuilder.RemoveJoin1Click(Sender: TObject);
begin
  FQBFrm.SyncSQLResult;
  FQBQuery.Joins[pmJoin.Tag].Free;
  RefreshTableArea;
  FQBFrm.pcFields.OnChange(nil);
end;

procedure TrwQueryBuilder.RefreshTableArea;
begin
  TrwQBScrollArea(pcSQL.ActivePage.Controls[0]).Invalidate;
end;

procedure TrwQueryBuilder.RestoreState;
var
  i: Integer;
  h: String;
  mh: Integer;
  R: TRect;
  C: TToolBar;
begin
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TToolBar then
    begin
      C := TToolBar(Components[i]);
      h := ReadFromRegistry('Misc\rwQueryBuilder\ToolsBar\', C.Name, '');
      if Length(h) > 0 then
        try
          if Boolean(StrToInt(GetNextStrValue(h, ','))) then
          begin
            C.Left := StrToInt(GetNextStrValue(h, ','));
            C.Top := StrToInt(h);
          end
          else
          begin
            R.Left := StrToInt(GetNextStrValue(h, ','));
            R.Top  := StrToInt(h);
            R.Right := R.Left + C.Width*2;
            R.Bottom := R.Top + C.Height*2;
            C.ManualFloat(R);
          end;
        except
        end;
    end;


  mh := 0;
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TToolBar then
    begin
      C := TToolBar(Components[i]);
      h := ReadFromRegistry('Misc\rwQueryBuilder\ToolsBar\', C.Name, '');
      if Length(h) > 0 then
        try
          if Boolean(StrToInt(GetNextStrValue(h, ','))) then
          begin
            C.Left := StrToInt(GetNextStrValue(h, ','));
            C.Top := StrToInt(h);
            if mh < C.Top + C.Height then
              mh := C.Top + C.Height;
          end;
        except
        end;
    end;

   ctbBar.Height := mh;

  tvQuery.Height := StrToInt(ReadFromRegistry('Misc\rwQueryBuilder\', 'QueryListHeight', IntToStr(tvQuery.Height)));
  memDescr.Height := StrToInt(ReadFromRegistry('Misc\rwQueryBuilder\', 'DescrHeight', IntToStr(memDescr.Height)));
  Panel2.Width := StrToInt(ReadFromRegistry('Misc\rwQueryBuilder\', 'TablePanelWidth', IntToStr(Panel2.Width)));
  pcFields.Height := StrToInt(ReadFromRegistry('Misc\rwQueryBuilder\', 'QueryPropHeight', IntToStr(pcFields.Height)));
  memPlan.Height := StrToInt(ReadFromRegistry('Misc\rwQueryBuilder\', 'PlanHeight', IntToStr(memPlan.Height)));
end;

procedure TrwQueryBuilder.SaveState;
var
  i: Integer;
  C: TToolBar;
  h: String;
begin
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TToolBar then
    begin
      C := TToolBar(Components[i]);
      if C.Parent is TControlBar then
        h := '1,' + IntToStr(C.Left) + ',' + IntToStr(C.Top)
      else
        h := '0,' + IntToStr(C.Parent.Left) + ',' + IntToStr(C.Parent.Top);
      WriteToRegistry('Misc\rwQueryBuilder\ToolsBar\', C.Name, h);
    end;

  WriteToRegistry('Misc\rwQueryBuilder\', 'QueryListHeight', IntToStr(tvQuery.Height));
  WriteToRegistry('Misc\rwQueryBuilder\', 'DescrHeight', IntToStr(memDescr.Height));
  WriteToRegistry('Misc\rwQueryBuilder\', 'TablePanelWidth', IntToStr(Panel2.Width));
  WriteToRegistry('Misc\rwQueryBuilder\', 'QueryPropHeight', IntToStr(pcFields.Height));
  WriteToRegistry('Misc\rwQueryBuilder\', 'PlanHeight', IntToStr(memPlan.Height));
end;

procedure TrwQueryBuilder.UndockedBarsVisible(AVisible: Boolean);
var
  i: Integer;
  C: TToolBar;
begin
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TToolBar then
    begin
      C := TToolBar(Components[i]);
      if C.Parent is FloatingDockSiteClass then
        C.Parent.Visible := AVisible;
    end;
end;

procedure TrwQueryBuilder.FormShow(Sender: TObject);
begin
  Caption := 'Query Builder [' + GetAppAdapterInfo(AppAdapterModuleName).ApplicationName + ']';
  RestoreState;
  UndockedBarsVisible(False);
end;

procedure TrwQueryBuilder.actSearchExecute(Sender: TObject);
var
  lSearchDialog: TFindDialog;
begin
  lSearchDialog := TFindDialog.Create(Self);
  lSearchDialog.OnFind := FOnFindDialog;
  lSearchDialog.OnClose := FOnCloseSearch;
  lSearchDialog.Options := lSearchDialog.Options + [frHideUpDown, frHideMatchCase, frHideWholeWord];
  FCurSearchIndex := 0;
  lSearchDialog.Execute;
end;

procedure TrwQueryBuilder.FOnFindDialog(Sender: TObject);
var
  h: String;
  TV: TTreeView;
  i: Integer;
begin
  h := AnsiUpperCase(TFindDialog(Sender).FindText);
  if pnlTables.Visible then
    TV := tvTables
  else
    TV := tvChildTables;

  for i := FCurSearchIndex to TV.Items.Count - 1 do
  begin
    if Pos(h, AnsiUpperCase(TV.Items[i].Text)) > 0 then
    begin
      TV.Items[i].Selected := True;
      FCurSearchIndex := i + 1;
      TV.HideSelection := False;
      Exit;
    end;
  end;
end;

procedure TrwQueryBuilder.FOnCloseSearch(Sender: TObject);
begin
  tvTables.HideSelection := True;
  tvChildTables.HideSelection := True;
end;

procedure TrwQueryBuilder.edMacroSelectExit(Sender: TObject);
begin
  if Assigned(FQBQuery) then
    FQBQuery.Macros := edMacroSelect.Text + ',' + edMacroFrom.Text +
      ',' + edMacroWhere.Text + ',' + edMacroOrder.Text + ',' + edMacroInsert.Text;
end;

procedure TrwQueryBuilder.pmPasteTablePopup(Sender: TObject);
begin
  miPasteTable.Enabled := Clipboard.HasFormat(CF_QBTABLE);
end;


procedure TrwQueryBuilder.miPasteTableClick(Sender: TObject);
var
  Tbl: TrwQBSelectedTable;
  T: TrwQBTable;
begin
  Tbl := TrwQBSelectedTable.Create(FQBQuery.SelectedTables);
  Tbl.ContentFromString(GetFromClipboard(CF_QBTABLE));

  if FQBQuery.Union and not Tbl.IsSUBQuery then
  begin
    Tbl.Free;
    Exit;
  end;

  Tbl.Left := DragObject.DropPoint.X;
  Tbl.Top := DragObject.DropPoint.Y;
  Tbl.InternalName := IntToStr(Integer(Pointer(Tbl)));
  if not FQBQuery.Union then
    Tbl.Alias := GetUniqueTableAlias
  else
    Tbl.Alias := '';
  T := AddVisualTable(Tbl);

  if Tbl.IsSUBQuery then
  begin
    Tbl.SUBQuery.Description := Tbl.SUBQuery.Description + ' (COPY)';
    FixUpQuery(Tbl.SUBQuery);
    SyncContents(Tbl.SUBQuery);
    tvQuery.Selected := NodeByQuery(FQBQuery);
    SwitchQuery;
  end;


  T.Perform(RW_QBTABLE_OPEN, 0, 0);
end;


procedure TrwQueryBuilder.grResult1DblClick(Sender: TObject);
begin
  actMaximizeResult.Execute;
end;


procedure TrwQueryBuilder.actMaximizeResultExecute(Sender: TObject);
begin
  ShowDataSetContents(FResultGrid.DataSource);
end;

procedure TrwQueryBuilder.tvQueryContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
var
  N: TTreeNode;
  P: TPoint;
begin
  N := tvQuery.GetNodeAt(MousePos.X, MousePos.Y);
  if Assigned(N) and (tvQuery.Selected <> N) then
    tvQuery.Selected := N;
  P := tvQuery.ClientToScreen(MousePos);
  pmSubQuery.Popup(P.X, P.Y);
  Handled := True;
end;


procedure TrwQueryBuilder.DeleteTabSheet(ATabSheet: TTabSheet);
var
  S: TrwQBScrollArea;
begin
  S := TrwQBScrollArea(ATabSheet.Controls[0]);
  while S.ControlCount > 0 do
  begin
    S.Controls[0].Hide;
    S.Controls[0].Parent := nil;
  end;
  ATabSheet.Free;
end;

procedure TrwQueryBuilder.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if not (ModalResult in [mrOK, mrAbort]) then
    case MessageDlg('Do you want to save changes you made?', mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
      mrYes: ModalResult := mrOK;
      mrNo:  ModalResult := mrCancel;
    else
      CanClose := False;
    end;
end;

procedure TrwQueryBuilder.actConstsExecute(Sender: TObject);
begin
  if not Assigned(FConstFrm) then
    FConstFrm := TrwQBConstsEdit.Create(Self);

  FConstFrm.ShowModal;
  fl_upd_const := FConstFrm.Modified;
end;


procedure TrwQueryBuilder.AddParentQuery(const Union: Boolean);
var
  Q: TrwQBQuery;
begin
   CloseCurrentTable;

    Q := TrwQBQuery.Create(nil);
    try
      Q.Assign(FQBQuery);
      FQBQuery.Clear;
      FQBQuery.Union := Union;
      FQBQuery.SelectedTables.Add(qbEmbeddedQueryName);

      FQBQuery.SelectedTables[0].SUBQuery.Assign(Q);
      FQBQuery.SelectedTables[0].Alias := GetUniqueTableAlias;
      FQBQuery.SelectedTables[0].Left := 10;
      FQBQuery.SelectedTables[0].Top := 10;

      if FQBQuery = FMainQBQuery then
        FQBQuery.Description := 'Main Statement'
      else
        FQBQuery.Description := FQBQuery.ParentTable.Alias;

      if AnsiSameText(FQBQuery.SelectedTables[0].SUBQuery.Description, 'Main Statement') or
         Assigned(FQBQuery.ParentTable) and
         AnsiSameText(FQBQuery.SelectedTables[0].SUBQuery.Description, FQBQuery.ParentTable.Alias) then
        FQBQuery.SelectedTables[0].SUBQuery.Description := FQBQuery.SelectedTables[0].Alias;
    finally
      Q.Free;
    end;


    Q := FQBQuery.SelectedTables[0].SUBQuery;

    if Union then
    begin
      FQBQuery.Description := 'Union of SubQueries';
      FQBQuery.ShowingFields.Clear;
      SyncUnionShowingFields(FQBQuery);
    end;

    SyncMainQuery;
    tvQuery.Selected := NodeByQuery(Q);
    SwitchQuery;
end;

procedure TrwQueryBuilder.actMakeUnionExecute(Sender: TObject);
begin
  AddParentQuery(True);
end;

procedure TrwQueryBuilder.SyncUnionShowingFields(AQuery: TrwQBQuery);
var
  Q: TrwQBQuery;
  i, j: Integer;
  Fld: TrwQBShowingField;
begin
  Q := AQuery.SelectedTables[0].SUBQuery;

  //Sync fields position and add new fields
  for i := 0 to Q.ShowingFields.Count - 1 do
  begin
    Fld := nil;
    for j := 0 to AQuery.ShowingFields.Count - 1 do
      if AnsiSameText(AQuery.ShowingFields[j][0].Value, Q.ShowingFields[i].InternalName) then
      begin
        Fld := AQuery.ShowingFields[j];
        break;
      end;

    if not Assigned(Fld) then
    begin
      j := AQuery.ShowingFields.Add;
      Fld := AQuery.ShowingFields[j];
    end
    else
      Fld.Clear;

    Fld.AddItem(eitField, Q.ShowingFields[i].InternalName, AQuery.SelectedTables[0].InternalName);

    Fld.Index := i;
  end;

  //Delete removed fields
  for i := AQuery.ShowingFields.Count - 1 downto Q.ShowingFields.Count do
    RemoveShowingField(AQuery.ShowingFields[i]);
end;

procedure TrwQueryBuilder.SyncUnionTables;
var
  i: Integer;
begin
  for i := 0 to FQBQuery.SelectedTables.Count - 1 do
    TrwQBTable(FQBQuery.SelectedTables[i].VisualComponent).CheckUnionStructure;
end;

function TrwQueryBuilder.CheckUnion(AQuery: TrwQBQuery): Boolean;
var
  i, j: Integer;
  QBase: TrwQBQuery;
  QCurr: TrwQBQuery;
  fl: Boolean;
begin
  Result := False;

  for i := 0 to AQuery.SelectedTables.Count - 1 do
    if AQuery.SelectedTables[i].IsSUBQuery and AQuery.SelectedTables[i].SUBQuery.Union and
       CheckUnion(AQuery.SelectedTables[i].SUBQuery) then
    begin
      Result := True;
      Exit;
    end;

  if AQuery.Union then
  begin
    QBase := AQuery.SelectedTables[0].SUBQuery;
    fl := False;
    for i := 1 to AQuery.SelectedTables.Count - 1 do
    begin
      QCurr := AQuery.SelectedTables[i].SUBQuery;

      if QCurr.ShowingFields.Count <> QBase.ShowingFields.Count then
        fl := True
      else
      begin
        for j := 0 to QCurr.ShowingFields.Count - 1 do
          if QCurr.ShowingFields[j].ExprType <> QBase.ShowingFields[j].ExprType then
          begin
            fl := True;
            break;
          end;
      end;

      if fl then
      begin
        OpenSelectedTable(AQuery.SelectedTables[i]);
        Result := True;
        Exit;
      end;
    end;
  end;
end;

function TrwQueryBuilder.CheckBasicErrors(AQuery: TrwQBQuery): Boolean;
var
  i: Integer;
begin
  Result := False;

  for i := 0 to AQuery.SelectedTables.Count - 1 do
    if AQuery.SelectedTables[i].IsSUBQuery and CheckBasicErrors(AQuery.SelectedTables[i].SUBQuery) then
    begin
      Result := True;
      Exit;
    end;

  if (AQuery.SelectedTables.Count = 0) or (AQuery.ShowingFields.Count = 0) then
  begin
    Result := True;
    tvQuery.Selected := NodeByQuery(AQuery);
    SwitchQuery;
  end;
end;

procedure TrwQueryBuilder.tvChildTablesCompare(Sender: TObject; Node1, Node2: TTreeNode; Data: Integer; var Compare: Integer);
begin
  if (Node1 = FDataDictionaryNode) and (Node2 = FTemplateNode) then
    Compare := -1

  else if (Node2 = FDataDictionaryNode) and (Node1 = FTemplateNode) then
    Compare := 1

  else if (Node1.ImageIndex = 0) and (Node2.ImageIndex <> 0) then
    Compare := -1

  else if (Node2.ImageIndex = 0) and (Node1.ImageIndex <> 0) then
    Compare := 1

  else
    Compare := CompareStr(Node1.Text, Node2.Text);
end;

procedure TrwQueryBuilder.actSyncDDExecute(Sender: TObject);
begin
  try
    RWEngineExt.AppAdapter.StartDesignerProgressBar('Data Dictionary Synchronization...');
    DataDictionary.Synchronize;
    FillTreeView;
  finally
    RWEngineExt.AppAdapter.EndDesignerProgressBar;
    Update;
    if DataDictionary.SyncLog <> '' then
    begin
      fl_upd_dict := True;
      ShowLog(Self, DataDictionary.SyncLog, 'Data Dictionarty Synchronization Log');
    end;
  end;
end;

procedure TrwQueryBuilder.MakeTableParamList(ddTable: TrwDataDictTable; ATableParams: TrwQBExpressions);
var
  j: Integer;
  Par: TrwQBExpression;
  C: TsbCustomSQLConstant;
  DS: TrwDataSet;
  t: TrwQBExprItemType;
begin
  for j := 0 to ddTable.Params.Count - 1 do
    with TrwDataDictParam(ddTable.Params[j]) do
    begin
      Par := TrwQBExpression(ATableParams.Add);

      if DefaultValue = '' then
        Par.AddItem(eitParam, ddTable.Params[j].Name)

      else if AnsiSameText(DefaultValue, '<NULL>') then
        Par.AddItem(eitNull, 'Null', 'Null')

      else
      begin
        if DefaultValue[1] = '@' then
        begin
          C := SQLConsts.ConstantByName(Copy(DefaultValue, 2, Length(DefaultValue) - 1));
          if Assigned(C) then
          begin
            Par.AddItem(eitConst, C.ConstName, C.DisplayName + '(' + StringReplace(C.StrValue, #13, ', ', [rfReplaceAll]) + ')');
            Continue;
          end;  
        end;

        if ParamType in [ddtInteger, ddtString, ddtBoolean] then
        begin
          DS := CreateLookupList(ddTable, Name, True);
          try
            if Assigned(DS) then
            begin
              if DS.PLocate('_key', DefaultValue, True, False) then
              begin
                case ParamType of
                  ddtInteger:  t := eitIntConst;
                  ddtString:   t := eitStrConst;
                  ddtBoolean:  t := eitStrConst;
                else
                  t := eitNone;
                end;

                Par.AddItem(t, DefaultValue, DS.PFieldValue('text'));
                Continue;
              end;
            end;
          finally
            FreeAndNil(DS);
          end;
        end;

        case ParamType of
          ddtInteger:  Par.AddItem(eitIntConst, StrToInt(DefaultValue));

          ddtCurrency,
          ddtFloat:    Par.AddItem(eitFloatConst, StrToFloat(DefaultValue));

          ddtDateTime: Par.AddItem(eitDateConst, DefaultValue);
          ddtString:   Par.AddItem(eitStrConst, DefaultValue);
          ddtBoolean:  Par.AddItem(eitStrConst, DefaultValue);
        else
          Par.AddItem(eitNull, 'Null', 'Null');
        end;
      end;
    end;
end;

procedure TrwQueryBuilder.FixTableParamsIfItNeeds;
var
  Pars: TrwQBExpressions;
  fl: Boolean;

  procedure FixQuery(const AQuery: TrwQBQuery);
  var
    i, j: Integer;
  begin
    for i := 0 to AQuery.SelectedTables.Count - 1 do
      if AQuery.SelectedTables[i].IsSUBQuery then
        FixQuery(AQuery.SelectedTables[i].SUBQuery)
      else if AQuery.SelectedTables[i].lqObject <> nil then
      begin
        Pars.Clear;
        MakeTableParamList(AQuery.SelectedTables[i].lqObject, Pars);

        if AQuery.SelectedTables[i].TableParams.Count > Pars.Count then
        begin
          fl := True;
          while AQuery.SelectedTables[i].TableParams.Count > Pars.Count do
            AQuery.SelectedTables[i].TableParams.Delete(AQuery.SelectedTables[i].TableParams.Count - 1);
        end
        else if AQuery.SelectedTables[i].TableParams.Count < Pars.Count then
        begin
          fl := True;
          for j := Pars.Count - (Pars.Count - AQuery.SelectedTables[i].TableParams.Count) to Pars.Count - 1 do
            TrwQBExpression(AQuery.SelectedTables[i].TableParams.Add).Assign(Pars.Items[j]);
        end;
      end;
  end;

begin
   fl := False;
   Pars := TrwQBExpressions.Create(TrwQBExpression);
   try
     FixQuery(FMainQBQuery);
   finally
     FreeAndNil(Pars);
   end;

   if fl then
     ISErrMessage('Some table parameters have been updated. Please check you query.');
end;

procedure TrwQueryBuilder.actViewExecute(Sender: TObject);
var
  Q: TrwQBQuery;
begin
  CloseCurrentTable;

  Q := FMainQBQuery;
  FMainQBQuery := TrwQBQuery.Create(nil);
  try
    SyncMainQuery;
    Update;
    WizardView := btnView.Down;
    Initialize;
  finally
    FreeAndNil(FMainQBQuery);
    FMainQBQuery := Q;
  end;
  Update;
  SyncMainQuery;
end;

{ TrwQBExtParam }

procedure TrwQBExtParam.Assign(Source: TPersistent);
begin
  FParamName := TrwQBExtParam(Source).ParamName;
  FParamDisplayName := TrwQBExtParam(Source).ParamDisplayName;
  FParamType := TrwQBExtParam(Source).ParamType;
  FValue := TrwQBExtParam(Source).Value;
end;


{ TrwQBExtParams }

function TrwQBExtParams.GetItem(Index: Integer): TrwQBExtParam;
begin
  Result := TrwQBExtParam(inherited Items[Index]);
end;

function TrwQBExtParams.ParamByName(const AParamName: String): TrwQBExtParam;
var
  i: Integer;
begin
  Result := nil;
  for i := 0  to Count - 1 do
    if SameText(Items[i].ParamName, AParamName) then
    begin
      Result := Items[i];
      break;
    end;
end;

function TrwQueryBuilder.SQLSyn: TmwGeneralSyn;
var
  i: Integer;
  lSQLSyn: TmwGeneralSyn;
begin
  if not Assigned(FSQLSyn) then
  begin
    if not Assigned(FSQLSyn) then
    begin
      lSQLSyn := TmwGeneralSyn.Create(nil);
      lSQLSyn.SymbolAttri.Foreground := clGray;
      lSQLSyn.Comments := [csPasStyle];
      lSQLSyn.CommentAttri.Foreground := clNavy;
      lSQLSyn.CommentAttri.Style := [fsItalic];
      lSQLSyn.NumberAttri.Foreground := clPurple;
      lSQLSyn.StringAttri.Foreground := clBlue;
      for i := 0 to sqlKeyWords.Count - 1 do
        if sqlKeyWords[i].Code >= kwSelect then
          lSQLSyn.KeyWords.Add(sqlKeyWords[i].Text);
      FSQLSyn := lSQLSyn;
    end;
  end;

  Result := FSQLSyn;
end;


{ TrwQBFormHolder }

constructor TrwQBFormHolder.Create(const AQueryRes: IEvDualStream; const AWizard: Boolean; const AGlobalVars: IrmVariables);
var
  Param: TrwQBExtParam;
  i: Integer;
  vInfo: TrmVFHVarInfoRec;
begin
  if Assigned(AGlobalVars) then
  begin
    FParams := TrwQBExtParams.Create(TrwQBExtParam);
    for i := 0 to AGlobalVars.GetItemCount - 1 do
    begin
      vInfo := (AGlobalVars.GetItemByIndex(i) as IrmVariable).GetVarInfo;
      if vInfo.ShowInQB then
      begin
        Param := TrwQBExtParam(FParams.Add);
        Param.ParamName := vInfo.Name;
        if vInfo.DisplayName <> '' then
          Param.ParamDisplayName := vInfo.DisplayName
        else
          Param.ParamDisplayName := vInfo.Name;
        Param.Value := vInfo.Value;
      end;
    end;
  end
  else
   FParams := nil;

  FForm := TrwQueryBuilder.Create(Application);
  with FForm do
  begin
    WizardView := AWizard;
    ExternalParams := FParams;
    Initialize;

    if Assigned(AQueryRes) and (AQueryRes.Size > 0) then
    begin
      AQueryRes.Position := 0;
      FMainQBQuery.ReadFromStream(AQueryRes.RealStream);
      FixTableParamsIfItNeeds;
    end;

    SyncMainQuery;

    Show;
  end;
end;

destructor TrwQBFormHolder.Destroy;
begin
  FForm.Finalize;
  FreeAndNil(FForm);
  FreeAndNil(FParams);
  inherited;
end;

function TrwQBFormHolder.GetData: IEvDualStream;
begin
  with FForm do
  begin
    if ModalResult = mrOk then
    begin
      if TrwSQLStatementType(cbSQLType.ItemIndex) <> rssSelect then
        FMainQBQuery.SortFields.Clear;

      if not (TrwSQLStatementType(cbSQLType.ItemIndex) in [rssSelect, rssInsert]) then
        FMainQBQuery.ShowingFields.Clear;

      Result := TEvDualStreamHolder.CreateInMemory;
      FMainQBQuery.SaveToStream(Result.RealStream);
      Result.Position := 0;      
    end
    else
      Result := nil;
  end;
end;

function TrwQBFormHolder.IsClosed: Boolean;
begin
  Result := not (Assigned(FForm) and FForm.Visible);
end;

procedure TrwQueryBuilder.CreateCustomTB;
begin
  DestroyCustomTB;

  FAppCustomTBControl := TForm(rwEngineExt.AppAdapter.GetCustomControl('Query Builder Toolbar'));
  if Assigned(FAppCustomTBControl) then
  begin
    tbCustom.Visible := True;
    FAppCustomTBControl.BorderStyle := bsNone;
    FAppCustomTBControl.Left := 0;
    FAppCustomTBControl.Top := 0;
    FAppCustomTBControl.Visible := True;
    tbCustom.ClientWidth := FAppCustomTBControl.Width;
    FCustomTBPrevParent := Windows.SetParent(FAppCustomTBControl.Handle, tbCustom.Handle);
  end;
end;

procedure TrwQueryBuilder.DestroyCustomTB;
begin
 if Assigned(FAppCustomTBControl) then
 begin
   FAppCustomTBControl.Hide;
   Windows.SetParent(FAppCustomTBControl.Handle, FCustomTBPrevParent);
   FAppCustomTBControl := nil;
 end;
end;

procedure TrwQueryBuilder.AddError(const AError: String);
begin
  if AError <> '' then
    AddStrValue(FErrors, AError, #13);
end;

procedure TrwQueryBuilder.ClearErrors;
begin
  FErrors := '';
end;

procedure TrwQueryBuilder.ShowErrors;
begin
  if FErrors <> '' then
    ISErrMessage(FErrors);
end;

initialization
  CF_QBQUERY := RegisterClipboardFormat('CF_QBQUERY');
  CF_QBCONDITION := RegisterClipboardFormat('CF_QBCONDITION');
  CF_QBEXPRESSION := RegisterClipboardFormat('CF_QBEXPRESSION');
  CF_QBTABLE := RegisterClipboardFormat('CF_QBTABLE');

end.
