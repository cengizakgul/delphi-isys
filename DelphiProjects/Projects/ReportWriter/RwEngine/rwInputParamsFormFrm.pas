unit rwInputParamsFormFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, rwCommonClasses, DB, rwReport, rwBasicClasses,
  rwTypes, rwDebugInfo, rwVirtualMachine, rwUtils, Buttons, rwEngine;

type

  {TrwInputParamsForm is base form for showing Input Form contents}

  TrwInputParamsForm = class(TForm)
    pnlBtns: TPanel;
    bvlBottom: TBevel;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FInputForm: TrwInputFormContainer;
    FParentWindow: HWND;
    procedure SetInputForm(const Value: TrwInputFormContainer);
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    constructor CreateAsFrame;
    procedure SetExternalParentWindow(const AParent: HWND);

    property InputForm: TrwInputFormContainer read FInputForm write SetInputForm;
  end;

  function  ShowInFormContents(const AInputForm: TrwInputFormContainer; const ACaption: String; AParent: TWinControl): Boolean;


implementation

{$R *.DFM}


function ShowInFormContents(const AInputForm: TrwInputFormContainer; const ACaption: String; AParent: TWinControl): Boolean;
var
  FInputForm: TrwInputFormContainer;
begin
  if not Debugging then
    Busy
  else
    DebugInfo.InputFormCode := True;

  try
    FInputForm := AInputForm;
    FInputForm.Top := 0;
    FInputForm.Left := 0;

    if not Assigned(AParent) then
    begin
      AParent := TrwInputParamsForm.Create(nil);
      TrwInputParamsForm(AParent).InputForm := FInputForm;
    end
    else
    begin
      AParent.ClientWidth := FInputForm.Width;
      AParent.ClientHeight := FInputForm.Height;
      FInputForm.Align := alClient;
    end;

    if AParent is TrwInputParamsForm then
    begin
      try
        if Debugging then
        begin
          TrwInputParamsForm(AParent).Show;
//          FInputForm.SetParentForm(AParent);
          FInputForm.HideRunTimeControls;
          AParent.Refresh;

          while TrwInputParamsForm(AParent).Visible and not DebugInfo.Aborted do
          begin
            TrwInputParamsForm(AParent).Enabled := not DebugInfo.Waiting;
            if GetActiveWindow <> TrwInputParamsForm(AParent).Handle then
              SetWindowPos(TrwInputParamsForm(AParent).Handle, HWND_TOP, 0, 0, 0, 0,
                SWP_NOACTIVATE or SWP_NOSIZE or SWP_NOMOVE);
            Application.ProcessMessages;
            Sleep(1);
          end;

          if DebugInfo.Aborted then
            Result := False
          else
          begin
            Result := (FInputForm.CloseResult = mrOk);
            FInputForm.DoClose;
          end;

        end
        else
        begin
          if ACaption <> '' then
            TrwInputParamsForm(AParent).Caption := ACaption;

          TrwInputParamsForm(AParent).ShowModal;
          Result := (FInputForm.CloseResult = mrOk);
          Busy;
          FInputForm.DoClose;
        end;

      finally
        if AParent is TrwInputParamsForm then
          AParent.Free;
        FInputForm.SetParentForm(nil);
        Application.ProcessMessages;
      end;
    end

    else
    begin
      FInputForm.SetParentForm(AParent);
      FInputForm.HideRunTimeControls;
      AParent.Refresh;
      FInputForm.DoAfterCreate;
      Result := True;
    end;

  finally
    if not Debugging then
      Ready
    else
      DebugInfo.InputFormCode := False;
  end;
end;


procedure TrwInputParamsForm.btnOKClick(Sender: TObject);
begin
  ModalResult := TButton(Sender).ModalResult;
  FInputForm.CloseResult := ModalResult;
  if FInputForm.DoCloseQuery then
    Close;
end;

constructor TrwInputParamsForm.CreateAsFrame;
begin
  inherited Create(nil);
  pnlBtns.Visible := False;
  RecreateWnd;
end;

procedure TrwInputParamsForm.CreateParams(var Params: TCreateParams);
begin
  inherited;
  if not pnlBtns.Visible then
    Params.Style := WS_CHILD or WS_CLIPSIBLINGS or WS_TABSTOP or WS_GROUP;
end;

procedure TrwInputParamsForm.FormShow(Sender: TObject);
begin
  if Assigned(FInputForm) and not FInputForm.IsDesignMode then
  begin
    try
      FInputForm.SetParentForm(Self);
      FInputForm.HideRunTimeControls;
      FInputForm.DoAfterCreate;
      ActiveControl := TWinControl(FInputForm.VisualControl.RealObject);
    finally
      pnlBtns.Enabled := True;
      Ready;
    end;
  end;
end;

procedure TrwInputParamsForm.SetInputForm(const Value: TrwInputFormContainer);
begin
  FInputForm := Value;

  if Assigned(FInputForm) then
  begin
    ClientWidth := FInputForm.Width;
    ClientHeight := FInputForm.Height + pnlBtns.Height;
    pnlBtns.Color := FInputForm.Color;
  end;
end;


procedure TrwInputParamsForm.FormCreate(Sender: TObject);
begin
  CurrentInputForm := Self;
end;

procedure TrwInputParamsForm.FormDestroy(Sender: TObject);
begin
  CurrentInputForm := nil;
end;

procedure TrwInputParamsForm.SetExternalParentWindow(const AParent: HWND);
var
  Style: DWORD;
begin
  if FParentWindow <> AParent then
    if (FParentWindow = 0) and (AParent <> 0) then
    begin
      Style := GetWindowLong(Handle, GWL_STYLE);
      Style := Style and not WS_POPUP;
      Style := Style or WS_CHILD;
      SetWindowLong(Handle, GWL_STYLE, Style);
      Windows.SetParent(Handle, AParent);
      FParentWindow := AParent;
    end
    else if (FParentWindow <> 0) and (AParent = 0) then
    begin
      Windows.SetParent(Handle, Application.Handle); // Very important to assign Application.Handle! Should not be 0!
      FParentWindow := 0;
      Style := GetWindowLong(Handle, GWL_STYLE);
      Style := Style and not WS_CHILD;
      Style := Style or WS_POPUP;
      SetWindowLong(Handle, GWL_STYLE, Integer(Style));
    end
    else else if (FParentWindow <> 0) and (AParent <> 0) then
    begin
      FParentWindow := AParent;
      Windows.SetParent(Handle, AParent);
    end;
end;

end.
