unit sbSQL;

interface

uses Classes, Sysutils, sbAPI, Windows, Variants, Math, sbConstants;


const
  sbVMStackSize = 255;

type
  {Type of tokens in SQL text}
  TsbTypeTokens = (stUnknown, stKeyword, stNumber, stQuotedText, stParameter, stConst);

  {sqlKeyWords}
  TsbKeyWord = (kwUnknown, kwFinish, kwSelect, kwFrom, kwWhere, kwBy, kwGroup,
    kwHaving, kwOrder, kwUnion, kwAnd, kwOr, kwPlus, kwMinus, kwMul, kwDiv, kwConcat,
    kwStick, kwLbracket, kwRbracket, kwLSquareBracket, kwRSquareBracket, kwEqual, kwComma, kwLess,
    kwMore, kwNeq, kwLeq, kwMeq, kwNot, kwRemb, kwReme,
    kwExists, kwLike, kwBetween, kwDistinct, kwAll, kwAsc, kwCreate, kwTable, kwInteger, kwAutoInc,
    kwFloat, kwString, kwChar, kwVarchar, kwDate, kwBlob, kwCurrency, kwBoolean, kwDrop, kwInsert,
    kwInto, kwDelete, kwUpdate, kwValues, kwDesc, kwIn, kwColon, kwConst, kwSet,
    kwIs, kwNull, kwLeftJoin, kwLeft, kwRight, kwFull, kwJoin,
    kwOuter, kwInner, kwOn, kwAs,
    kwSum, kwMin, kwMax, kwCount, kwAvg,
    kwUpper, kwLower, kwCast, kwSubst, kwAbs, kwSubStr, kwTrimLeft, kwTrimRight, kwTrim, kwFormat,
    kwDay, kwMonth, kwYear, kwBeginOfYear, kwEndOfYear, kwBeginOfMonth, kwEndOfMonth,
    kwBeginOfQuarter, kwEndOfQuarter, kwQuarter, kwQuarterBeginDate, kwQuarterEndDate,
    kwRound, kwTrunc, kwCheckIf, kwSetVar, kwGetVar, kwStrLength, kwStrBegin, kwStrEnd,
    kwSubStrPos, kwRoundFin);

  TsbFunctionType = (sftUnknown, sftUpper, sftLower, sftCast, sftSubst, sftAbs,
                     sftSubStr, sftTrimLeft, sftTrimRight, sftTrim, sftFormat,
                     sftDay, sftMonth, sftYear, sftBeginOfYear, sftEndOfYear,
                     sftBeginOfMonth, sftEndOfMonth, sftBeginOfQuarter,
                     sftEndOfQuarter, sftQuarter, sftQuarterBeginDate,
                     sftQuarterEndDate, sftRound, sftTrunc, sftCheckIf,
                     sftSetVar, sftGetVar, sftStrLength, sftStrBegin, sftStrEnd,
                     sftSubStrPos, sftRoundFin);

  TsbTypeKeyWord = (tkwNone, tkwUnaryFunct, tkwAgrFunct);

         {Keyword record}
  TsbKeyWordRec = record
    Text: string[16];
    Code: TsbKeyWord;
    WordType: TsbTypeKeyWord;
  end;


       {List of sqlKeyWords}
  TsbKeyWords = class(TStringList)
  private
    function GetItem(AIndex: Integer): TsbKeyWordRec;
  public
    property Items[index: Integer]: TsbKeyWordRec read GetItem; default;
    constructor Create;
    function Add(const AText: string; ACode: TsbKeyWord): Integer; reintroduce;
    function FindKeyWord(const AToken: string): TsbKeyWord;
    function FindKeyWordByCode(AKeyWord: TsbKeyWord): string;
    function KeyWordType(AKeyWord: TsbKeyWord): TsbTypeKeyWord;
  end;

  TsbNodeType      = (sbnAND, sbnOR);
  TsbCompOperation = (scoEqual, scoLess, scoMore, scoNotEqual, scoLessOrEqual, scoMoreOrEqual, scoIn,
                      scoBetween, scoIsNull, scoLike, scoExists);
  TsbCalcOperation = (smoNone, smoMOVA, smoMOVB, smoReverseSignA, smoAdd, smoSubstract, smoDivide,
                      smoMultiply, smoConcat, smoPushA, smoPopB, smoSwapAB, smoDefineArray);
  TsbConditionType =  (sctScalarCondition, sctFilter, sctJoin, sctLeftJoin, sctRightJoin, sctFullJoin, sctVectorCondition);
  TsbExpressionType = (setVector, setComplexVector, setScalar);
  TsbJoinType =       (sjtInnerJoin, sjtLeftJoin, sjtRightJoin, sjtFullJoin);
  TsbAggrFunctType =  (safSum, safMin, safMax, safCount, safAvg);

  TsbConditionNode = class;

  TsbTableItem = class;
  TsbAggrFunction = class;

  TsbExpressionItem = record
    FConstValue: Variant;
    FField: TsbField;
    FTable: TsbTableItem;
    FParam: TsbParam;
    FFunction: TsbFunctionType;
    FAggrFunction: TsbAggrFunction;
    FOperation: TsbCalcOperation;
  end;

  PTsbExpressionItem = ^TsbExpressionItem;


  TsbTableList = class;
  TsbSQLParser = class;

  TsbEmbeddedSQL = class
  private
    FSQLText: string;
    FParams: TsbParams;
    FParser: TsbSQLParser;
    procedure Prepare(APrevFrom: TsbTableList);
  public
    constructor Create;
    destructor Destroy; override;
    property   Parser: TsbSQLParser read FParser;
  end;


  TsbExpression = class(TsbItemList)
  private
    FPosition: Integer;
    FAlias: String;
    FSQL: TsbEmbeddedSQL;
    FType: TsbExpressionType;
    FJoinTable: TsbTableItem;
    FOuter: Boolean;
    function GetItem(Index: Integer): PTsbExpressionItem;
  public
    destructor Destroy; override;
    procedure   Clear; override;
    function    Add(AOperation: TsbCalcOperation): Integer;
    property Items[Index: Integer]: PTsbExpressionItem read GetItem; default;
    property SQL: TsbEmbeddedSQL read FSQL;
    function CurrentItem: PTsbExpressionItem;
    procedure Assign(ASource: TsbItemList); override;
  end;


  TsbConditionItem = class
  private
    FParent: TsbConditionNode;
    FLeftExpression:  TsbExpression;
    FRightExpression: TsbExpression;
    FOperation: TsbCompOperation;
    FType: TsbConditionType;
    FNot: Boolean;
    FStrExpression: string;
  public
    constructor Create(AOwner: TsbConditionNode);
    destructor  Destroy; override;
    procedure   CreateExpression(AType: Char);
    procedure   Assign(ASource: TsbConditionItem);
    property    LeftExpression:  TsbExpression read FLeftExpression;
    property    RightExpression: TsbExpression read FRightExpression;
    property    StrExpression: string read FStrExpression;
    property    NotOper: Boolean read FNot;
  end;


  TsbConditionNode = class(TsbItemList)
  private
    FParent:  TsbConditionNode;
    FType:    TsbNodeType;
    FNot:     Boolean;
    function  GetItem(Index: Integer): TObject;
  public
    constructor Create(AOwner: TsbConditionNode);
    class procedure Collapse(ANode: TsbConditionNode);
    function    AddNode: Integer;
    function    AddItem: Integer;
    procedure   Delete(AIndex: Integer); overload;
    procedure   Delete(AItem: TObject); overload;
    procedure   InsertNode(AIndex: Integer; AItem: TObject; ADelete: Boolean = True);
    property    Items[Index: Integer]: TObject read GetItem;
    property    OperType: TsbNodeType read FType;
    property    NotOper: Boolean read FNot;
  end;


  TsbJoin = class
  private
    FRightTable: TsbTableItem;
    FJoinType: TsbJoinType;
  end;


  TsbJoins = class(TsbItemList)
  private
    function GetItem(Index: Integer): TsbJoin;
  public
    function Add(ATable: TsbTableItem; AJoinType: TsbJoinType): Integer;
    function JoinByTable(ATable: TsbTableItem): Integer;
    property Items[Index: Integer]: TsbJoin read GetItem; default;
  end;


  TsbTempTable = class;

  TsbParentJoinInfo = class
  private
    FParentTable: TsbTableItem;
    FJoinCondition: TsbConditionNode;
    FSelfJoinFields: TList;
    FForeignJoinFields: TList;
    FFilterCondition: TsbConditionNode;
    FJoinIndexName: String;
    FNaturalJoin: Boolean;
    procedure SetJoinIndexName(const Value: String);
    property  JoinIndexName: String read FJoinIndexName write SetJoinIndexName;
  public
    constructor Create;
    destructor Destroy; override;
    function  FilterCondition(ATest: Boolean = False): TsbConditionNode;
    function  JoinCondition(ATest: Boolean = False): TsbConditionNode;
    property  SelfJoinFields: TList read FSelfJoinFields;
    property  ForeignJoinFields: TList read FForeignJoinFields;
  end;


  TsbTableItem = class
  private
    FPhysicalName: String;
    FAlias: String;
    FTable: TsbTableCursor;
    FSelRecs: TsbTempTable;
    FJoins: TsbJoins;
    FParentJoinInfo: TsbParentJoinInfo;
    FAlreadyJoined: Boolean;
    FManualJoined: Boolean;
    FIndex: Integer;
    FIndexEvaluation:  Real;
    FSearchEvaluation: Real;
    FOccupied: Boolean;
    FResultFieldRef: Integer;

    procedure SetAlias(const AValue: string);
    procedure InitTempTable;
  public
    constructor Create;
    destructor  Destroy; override;
    procedure   OpenTable;
    property    Alias: string read FAlias write SetAlias;
    property    Table: TsbTableCursor read FTable;
    property    ParentJoinInfo: TsbParentJoinInfo read FParentJoinInfo;
  end;


  TsbTableList = class(TsbItemList)
  private
    FPrevFrom: TsbTableList;
    function GetItem(Index: Integer): TsbTableItem;
  public
    constructor Create;
    function    Add(const ATableName: string): Integer; overload;
    function    Add(ATable: TsbTableCursor): Integer; overload;
    function    FindTable(const ATableName: string):TsbTableItem;
    function    FindField(const AFieldName: string; var ATable: TsbTableItem): TsbField;
    property Items[Index: Integer]: TsbTableItem read GetItem; default;
    property PrevFrom: TsbTableList read FPrevFrom;
  end;


  TsbSelectField = class
  private
    FExpression: TsbExpression;
    FAggrFunctsExist: Boolean;
    FHidden: Boolean;
  public
    constructor Create;
    destructor Destroy; override;
    property Expression: TsbExpression read FExpression;
  end;


  TsbSelectFieldList = class(TsbItemList)
  private
    FDistinct: Boolean;
    function GetItem(Index: Integer): TsbSelectField;
  public
    function Add: TsbSelectField;
    property Items[Index: Integer]: TsbSelectField read GetItem; default;
  end;

  TsbTempUniqGroup = class;

  TsbAggrFunction = class
  private
    FFunction: TsbAggrFunctType;
    FExpression: TsbExpression;
    FCurrentValue: Variant;
    FSecCurrentValue: Integer;
    FTempTable: TsbTempUniqGroup;
    FNull: Boolean;

    function GetDistinct: Boolean;
    procedure SetDistinct(const Value: Boolean);
    property Distinct: Boolean read GetDistinct write SetDistinct;
  public
    constructor Create;
    destructor  Destroy; override;
    property    Expression: TsbExpression read FExpression;
  end;


  TsbAggrFunctionList = class(TsbItemList)
  private
    function GetItem(Index: Integer): TsbAggrFunction;
    procedure MakeFinalResult;
    procedure PrepareForCalc;
  public
    function Add(AFunctType: TsbAggrFunctType; ADistinct: Boolean): Integer;
    property Items[Index: Integer]: TsbAggrFunction read GetItem; default;
  end;


  TsbJoinedTablesTreeList = class(TsbItemList)
  private
    function GetItem(Index: Integer): TsbTableItem;
  public
    procedure  Clear; override;
    procedure  Delete(AIndex: Integer);
    function   Add(ATable: TsbTableItem): Integer;
    property Items[Index: Integer]: TsbTableItem read GetItem; default;
  end;



  TsbGraphJoin = class
  private
    FJoinType:  TsbJoinType;
    FMandatory: Boolean;
    FJoinCondition: TsbConditionNode;
    FOccupied: Boolean;
    FWeight: Real;
    FNextNodeIndex: Integer;
    FExtraCondition: Boolean;
    function JoinCondition: TsbConditionNode;
  public
    constructor Create;
    destructor Destroy; override;
  end;


  TsbGraph = class(TsbItemList)
  private
    FJoins: array of array of TsbGraphJoin;
    FFirstNode: Integer;
    function GetNode(Index: Integer): TsbTableItem;
  public
    destructor  Destroy; override;
    procedure   Clear; override;
    function AddNode(ATable: TsbTableItem): Integer;
    function AddJoin(ATableL, ATableR: TsbTableItem; AJoinType: TsbJoinType): TsbGraphJoin;
    function AnalyzeGraph: Int64;
    property Nodes[Index: Integer]: TsbTableItem read GetNode; default;
  end;


  TsbOrderByItem = class
  private
    FSelIndex: Integer;
    FDesc: Boolean;
  end;


  TsbOrderByList = class(TsbItemList)
  private
    function GetItem(Index: Integer): TsbOrderByItem;
  public
    function Add(AIndex: Integer; ADesc: Boolean): Integer;
    property Items[Index: Integer]: TsbOrderByItem read GetItem; default;
  end;


  TsbGroupByList = class(TsbItemList)
  private
    function GetItem(Index: Integer): Integer;
  public
    function Add(AIndex: Integer): Integer;
    procedure Clear; override;
    property Items[Index: Integer]: Integer read GetItem; default;
  end;


  TsbTempUniqGroup = class(TsbItemList)
  private
    function CompareValues(const AValue1, AValue2: Variant): Shortint;
    function  IndexOf(const AValue: Variant): Integer;
  public
    function  Add(const AValue: Variant): Boolean;
    procedure Clear; override;
  end;


  TsbSQLInfo = class
  private
    FAlwaysEmptyResult: Boolean;
    FSelect:  TsbSelectFieldList;
    FFrom:    TsbTableList;
    FWhere:   TsbConditionNode;
    FOrder:   TsbOrderByList;
    FGroup:   TsbGroupByList;
    FHaving:  TsbConditionNode;
    FJoinTreeList: TsbJoinedTablesTreeList;
    FAggrFunct: TsbAggrFunctionList;
    FResult: TsbTempTable;
    procedure  ImplementJoins;
  public
    property    Select: TsbSelectFieldList read FSelect;
    property    From:   TsbTableList read FFrom;
    property    Where:  TsbConditionNode read FWhere;
    property    Order:  TsbOrderByList read FOrder;
    property    Group:  TsbGroupByList read FGroup;
    property    Having: TsbConditionNode read FHaving;
    constructor Create;
    destructor  Destroy; override;
  end;


  TsbSQLInfoList = class(TsbItemList)
  private
    FDistinct: Boolean;  //Distinct for UNION (True by default)
    FCurrentSQLInfo: Integer;
    function GetItem(Index: Integer): TsbSQLInfo;
  public
    constructor Create;
    function Add: Integer;
    procedure Clear; override;
    procedure CleanUpItem(AIndex: Integer);
    function IsUnion: Boolean;
    property Items[Index: Integer]: TsbSQLInfo read GetItem; default;
  end;


  TsbUpdField = class
  private
    FField: TsbField;
    FRightPart: TsbExpression;
  public
    constructor Create;
    destructor  Destroy; override;
  end;


  TsbUpdFieldList = class(TsbItemList)
  private
    function GetItem(Index: Integer): TsbUpdField;
  public
    function Add(AField: TsbField): TsbUpdField;
    property Items[Index: Integer]: TsbUpdField read GetItem; default;
  end;


  TExecSQLType = (sesUnknown, sesCreate, sesDrop, sesInsert, sesDelete, sesUpdate);

  TsbSQLExecInfo = class
  private
    FTableName: string;
    FTable:     TsbTableCursor;
    FFieldList: TsbFields;
    FUpdFieldList: TsbUpdFieldList;
    FType: TExecSQLType;
  public
    constructor Create;
    destructor  Destroy; override;

    property    SQLType: TExecSQLType read FType;
    property    TableName: string read FTableName write FTableName;
    property    FieldList: TsbFields read FFieldList;
  end;



  TsbVMVar = record
    Name: string;
    Value: Variant;
  end;

  PTsbVMVar = ^TsbVMVar;

  TsbVMVarList = class
  private
    FList: array of TsbVMVar;
  public
    destructor  Destroy; override;
    procedure   Clear;
    function    IndexOf(const AName: string): Integer;
    function    GetVar(const AIndex: Integer): PTsbVMVar;
    function    AddVar(const AName: string): Integer;
  end;


  TsbVirtualMachine = class
  private
    FUseCounter: Integer;
    FStack: array[0..sbVMStackSize] of Variant;
    FStackPointer: Integer;
    FRegA: Variant;
    FRegB: Variant;
    FDefExprType: Boolean;
    FExprFieldName: string;
    FOuterMet: Boolean;
    FIgnoreOuterMet: Boolean;
    FToday: TDateTime;
    FNow: TDateTime;
    FVariables: TsbVMVarList;
    function  VariantsAreSame(const A, B: Variant): Boolean;
    procedure CheckDate(var Val1, Val2: Variant); overload;
    function  CheckDate(const Val: Variant): Variant; overload;
    function  ValueInArray(const AValue, AArray: Variant): Boolean;
    procedure Push(const AValue: Variant);
    function  Pop: Variant;
    function  StrLikeStr(const AStr, APattern: String): Boolean;
    function  ExecuteFunct(const AFunction: TsbFunctionType; const AParams: array of variant): Variant;
    procedure CheckParams(const AFunctCode: TsbKeyWord; const AParams: array of variant; const AProperNumb: Byte);
    function  DefineTypeResult(const AParams: array of Variant): Variant;

    function  FunctUpper(const AParams: array of variant): Variant;
    function  FunctLower(const AParams: array of variant): Variant;
    function  FunctCast(const AParams: array of variant): Variant;
    function  FunctSubst(const AParams: array of variant): Variant;
    function  FunctAbs(const AParams: array of variant): Variant;
    function  FunctSubStr(const AParams: array of variant): Variant;
    function  FunctTrimLeft(const AParams: array of variant): Variant;
    function  FunctTrimRight(const AParams: array of variant): Variant;
    function  FunctTrim(const AParams: array of variant): Variant;
    function  FunctFormat(const AParams: array of variant): Variant;
    function  FunctDecodeDate(const AParams: array of variant; const AType: Char): Variant;
    function  FunctBeginOfYear(const AParams: array of variant): Variant;
    function  FunctEndOfYear(const AParams: array of variant): Variant;
    function  FunctBeginOfMonth(const AParams: array of variant): Variant;
    function  FunctEndOfMonth(const AParams: array of variant): Variant;
    function  FunctBeginOfQuarter(const AParams: array of variant): Variant;
    function  FunctEndOfQuarter(const AParams: array of variant): Variant;
    function  FunctQuarter(const AParams: array of variant): Variant;
    function  FunctQuarterBeginDate(const AParams: array of variant): Variant;
    function  FunctQuarterEndDate(const AParams: array of variant): Variant;
    function  FunctRound(const AParams: array of variant): Variant;
    function  FunctRoundFin(const AParams: array of variant): Variant;
    function  FunctTrunc(const AParams: array of variant): Variant;
    function  FunctSetVar(const AParams: array of variant): Variant;
    function  FunctGetVar(const AParams: array of variant): Variant;
    function  FunctStrLength(const AParams: array of variant): Variant;
    function  FunctStrBegin(const AParams: array of variant): Variant;
    function  FunctStrEnd(const AParams: array of variant): Variant;
    function  FunctSubStrPos(const AParams: array of variant): Variant;

    function  FunctCheckIf(const AParams: array of variant; AExpression: TsbExpression): Variant;  //exception

  public
    constructor Create;
    destructor  Destroy; override;
    procedure InitVM;
    procedure CalcExpression(AExpression: TsbExpression);
    function CalcCondition(ACondition: TsbConditionItem): Boolean;
    property RegA: Variant read FRegA;
    property RegB: Variant read FRegB;
  end;


  TsbScannerRes = record
    sToken:       string;
    sPrevToken:   string;
    sTokenType:   TsbTypeTokens;
    sKeyWord:     TsbKeyWord;
    sKeyWordType: TsbTypeKeyWord;
    sPos:         Integer;
    sWasBack:     Boolean;
    sText:        string;
  end;


  TsbSQLParser = class
  private
    FStatus: TsbScannerRes;
    FSQL: string;
    FSQLInfoList: TsbSQLInfoList;
    FSQLExecInfo: TsbSQLExecInfo;
    FParams: TsbParams;
    FCostEvaluation: Int64;
    FResultCursor: TsbTableCursor;
    FPrepared: Boolean;
    FSelectPart: Boolean;
    FHavingPart: Boolean;
    FUpdatePart: Boolean;
    FExternalResulCursor: Boolean;
    FDoNotOptimize: Boolean;

    function  CurrSQLInfo: TsbSQLInfo;
    function  GetUniqFieldName(const ABaseName: String; AFields: TsbFields): String;
    procedure ParseSELECT;
    procedure ParseFROM;
    function  ParseTable: TsbTableItem;
    function  ParseJoins(ALeftTable: TsbTableItem): TsbTableItem;
    procedure ParseWHERE;
    procedure ParseORDERBY;
    procedure ParseGROUPBY;
    procedure ParseHAVING;
    procedure ParseUNION;
    procedure ParseCREATE;
    procedure ParseDROP;
    procedure ParseINSERT;
    procedure ParseUPDATE;
    procedure ParseDELETE;
    procedure ParseSELECTStatement;
    procedure CreateResultTableStructure(AFields: TsbFields);
    procedure ParseLogConditions(ACondition: TsbConditionNode);
    function  ParseLogElement(ACondition: TsbConditionNode): TsbConditionNode;
    procedure ParseLogTerm(ACondition: TsbConditionNode);
    procedure ParseCondition(ACondition: TsbConditionNode);
    procedure ParseCompareExpression(ACondition: TsbConditionNode; ANot: Boolean);
    procedure ParseExpression(AExpression: TsbExpression);
    procedure ParseTerm(AExpression: TsbExpression);
    procedure ParseUnaryMinus(AExpression: TsbExpression);
    procedure ParseBrackets(AExpression: TsbExpression);
    procedure ParseArray(AExpression: TsbExpression);
    procedure ParseFactor(AExpression: TsbExpression);
    function  GetEmbeddedSelect: String;
    function  CheckTypeCompatibility(const AType1, AType2: TsbFieldType): Boolean;

    procedure OptimisationCondition(var ACondition: TsbConditionNode);
    function  DeleteEmptyNodes(var ACondition: TsbConditionNode): Boolean;
    procedure Normalization(ACondition: TsbConditionNode);
    procedure PackConditions(ACondition: TsbConditionNode);
    procedure AnalyseExpression(AExpression: TsbExpression);
    procedure AnalyseConditions(ACondition: TsbConditionNode);
    procedure SortConditions(ACondition: TsbConditionNode);
    function  RemoveScalars(ACondition: TsbConditionNode): Variant;
    procedure CreateJoinInfo;
    procedure CreateFilters;
    procedure AnalyzeTables;
    procedure AnalyzeSelect;
    procedure JoinAnalyze;
    procedure CleanUpWhere;
    procedure CreateIndexes;
    procedure ActivateIndexes(AActive: Boolean);
    procedure SetSQL(const Value: String);
    procedure ClearPreparedInfo;
    procedure SetParams(const Value: TsbParams);
    procedure ExecCreate;
    procedure ExecDrop;
    procedure UpdateTableRecord;
    procedure ExecInsert;
    procedure ExecUpdate;
    procedure ExecDelete;

  public
    constructor Create;
    destructor  Destroy; override;
    procedure   Prepare;
    procedure   Open(AResultCursor: TsbTableCursor = nil);
    procedure   Execute;
    function    Plan: String;
    function    CostEvaluation: Int64;
    function    GetResultCursor: TsbTableCursor;

    property    SQL: String read FSQL write SetSQL;
    property    Params: TsbParams read FParams write SetParams;
    property    SQLInfoList: TsbSQLInfoList read FSQLInfoList;
    property    SQLExecInfo: TsbSQLExecInfo read FSQLExecInfo;
  end;



  TsbTempTable = class
  private
    FTable: TList;
    FFields: TStringList;
    FRecNo: Integer;
    function GetField(Index: Integer): Integer;
    procedure SetField(Index: Integer; const Value: Integer);
    procedure SetRecNo(const Value: Integer);
  public
    constructor Create;
    destructor  Destroy; override;
    procedure   DropTable;
    procedure   CreateTable(const AFieldNames: array of string);
    function    RecordCount: Integer;
    procedure   Append; overload;
    procedure   Append(const ARecNum: Integer); overload;
    procedure   Delete;
    procedure   ClearTable;
    function    IndexOfField(const AFldName: String): Integer;

    property    Fields[Index: Integer]: Integer read GetField write SetField;
    property    RecNo: Integer read FRecNo write SetRecNo;
  end;


  ESBParser = class (EsbException)
  private
    FErrLine: Integer;
    FErrCol: Integer;
    FOriginalMessage: string;
  public
    constructor Create(const AMessage: String; ALine, ACol: Integer);
  end;


  TsbOpenTableFunct = function(ATableName: string): TsbTableCursor;
  TsbTableParamsProc = procedure(ATable: TsbTableCursor; AAccessMode: TsbAccessModeType);
  TsbCustomParsingEndProc = procedure(ASQLInfo: TObject);


procedure InitThreadVars_sbSQL;
procedure InitializationSbVM;
procedure DeInitializationSbVM;
function  PrepareFilter(ATable: TsbTableCursor): TsbSQLInfo;
function  CheckConditions(ACond: TsbConditionNode): Boolean;
function  VM: TsbVirtualMachine;
procedure SetSQLConstClass(ASQLConstsClass: TsbCustomSQLConstantsClass);
function  SQLConsts: TsbCustomSQLConstants;
procedure SetSBTempDir(const ADir: string);

function  ParseCustomSQL(const ASQL: string; AParams: TsbParams;
                         AOpenTableProc: TsbOpenTableFunct; ACustomParsingEndProc: TsbCustomParsingEndProc;
                         ADoNotOptimize: Boolean): TsbSQLParser;
function  CustomSQL(const ASQL: string; AParams: TsbParams; AResultCursor: TsbTableCursor;
                    AOpenTableProc: TsbOpenTableFunct; ATableParamsProc: TsbTableParamsProc): TsbResultRec;

procedure ShowParserError(AScannerRes: TsbScannerRes; AMessage: String; const AParams: array of const);
procedure ShowParserPosError(AScannerRes: TsbScannerRes; E: ESBParser);

procedure ScannerGetNextToken(var AStatus: TsbScannerRes);
procedure ScannerChangePosition(var AStatus: TsbScannerRes; const APos: Integer);
procedure ScannerPutBackToken(var AStatus: TsbScannerRes);
procedure ScannerCheckNextToken(var AStatus: TsbScannerRes; const AKeyWord: TsbKeyWord);
procedure ScannerCheckToken(var AStatus: TsbScannerRes; const AKeyWord: TsbKeyWord);
function  ScannerEOF(var AStatus: TsbScannerRes): Boolean;
procedure ScannerCheckEnd(var AStatus: TsbScannerRes);
function  ScannerSearchToken(var AStatus: TsbScannerRes; const AKeyWord: TsbKeyWord): Boolean;
function  ScannerNextTokenIs(var AStatus: TsbScannerRes; const AKeyWord: TsbKeyWord): Boolean;




resourcestring
  sbeSyntaxError =    'Syntax error. Unknown token "%s"';
  sbeNClQuotes =      'Not closed quotes';
  sbeNClRem =         'Not closed comment';
  sbeNBalRB =         'Not balanced round brackets';
  sbeNExpToken =      'Expected "%s" but "%s" met';
  sbeUnknSymbol =     'Unknown symbol "%s"';
  sbeTableExp =       'A table name expected but "%s" met';
  sbeTableNotFound =  'Table "%s" is not found';
  sbeFieldNotFound =  'Field "%s" is not found';
  sbeConstantNotFound=  'Constant "%s" is not found';
  sbeTypeNotSupported = 'Type is not supported';
  sbeInvalidClause    = 'Invalid %s clause';
  sbeInvalidAlias     = 'Alias "%s" conflicts with a table in the same statement';
  sbeOCantImplJoins   = 'Can not implement joins. There are ambiguous circular references.';
  sbeAliasRequired    = 'Table alias is required';
  sbeInvalidFieldSize = 'Size of the string field must be between 1 and 255';
  sbeFieldExists      = 'Field "%s" already exists';
  sbeColCountMismatch = 'Count of columns does not equal count of values';
  sbeTableParMismatch = 'Table parameters mismatch';
  sbeOverflow =         'Overflow! Too many join combinations';
  sbeUnionColCountMismatch = 'Count of columns of each union participant must be the same';
  sbeUnionColTypeMismatch =  'Type mismatch for column #%s. Types of columns of each union participant must be the same.';


  sbeVMStackViolation =      'VM: Stack violation';
  sbeVMStackOverflow =       'VM: Stack overflow';
  sbeVMUnknownInstruction =  'VM: Unknown instruction';
  sbeVMFunctParamsMismatch = 'Parameters mismatch. Function "%s"';
  sbeVMTooManyColumns      = 'Single column subquery has more than one column';
  sbeVMTooManyRows         = 'Single row subquery produced more than one row';


var
  sqlKeyWords: TsbKeyWords;

implementation

var
  FSQLConstsClass: TsbCustomSQLConstantsClass = nil;
  FSQLConsts: TsbCustomSQLConstants = nil;


threadvar
   FVM:  TsbVirtualMachine;
   SbTempDir: String;
   FOpenTableFunct: TsbOpenTableFunct;
   FTableParamsProc: TsbTableParamsProc;
   FCustomParsingEndProc: TsbCustomParsingEndProc;


procedure InitThreadVars_sbSQL;
begin
  FVM := nil;
  SbTempDir := '';
  FOpenTableFunct := nil;
  FTableParamsProc := nil;
  FCustomParsingEndProc := nil;
end;

procedure SetSBTempDir(const ADir: string);
begin
  SbTempDir := ADir;
end;


procedure SetSQLConstClass(ASQLConstsClass: TsbCustomSQLConstantsClass);
begin
  FSQLConstsClass := ASQLConstsClass;
end;


function VM: TsbVirtualMachine;
begin
  Result := FVM;
end;


function  SQLConsts: TsbCustomSQLConstants;
begin
  if not Assigned(FSQLConsts) and Assigned(FSQLConstsClass) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(FSQLConsts) then
        FSQLConsts := FSQLConstsClass.Initialize;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;
  Result := FSQLConsts;
end;


procedure ShowParserError(AScannerRes: TsbScannerRes; AMessage: String; const AParams: array of const);
var
  i, L, C: Integer;
begin
  AMessage := Format(AMessage, AParams);
  L := 1;
  C := 0;
  for i := 1 to Length(AScannerRes.sText) do
  begin
    if AScannerRes.sText[i] = #13 then
    begin
      Inc(L);
      C := 1;
    end;
    if AScannerRes.sText[i] >= ' ' then
      Inc(C);
    if i = AScannerRes.sPos - 1 then
      break;
  end;

  raise ESBParser.Create(AMessage, L, C);
end;


procedure ShowParserPosError(AScannerRes: TsbScannerRes; E: ESBParser);
var
  i, h, j: Integer;
begin
  j := 1;
  h := 0;
  for i := 1 to Length(AScannerRes.sText) do
  begin
    if AScannerRes.sText[i] = #13 then
    begin
      Inc(j);
      h := 1;
    end;
    if AScannerRes.sText[i] >= ' ' then
      Inc(h);
    if i = AScannerRes.sPos then
      break;
  end;

  if E.FErrLine = 1 then
  begin
    E.FErrCol := h + E.FErrCol;
    E.FErrLine := j;
  end
  else
    E.FErrLine := E.FErrLine + j - 1;

  E.Message := E.FOriginalMessage + #13 + 'Line: ' + IntToStr(E.FErrLine) + '   Column: ' + IntToStr(E.FErrCol);
end;


procedure InitializationSbVM;
begin
  if not Assigned(FVM) then
  begin
    FVM := TsbVirtualMachine.Create;
    FVM.FUseCounter := 1;
  end
  else
    Inc(FVM.FUseCounter);

  FVM.FToday := Date;
  FVM.FNow := Now;
  FVM.FIgnoreOuterMet := False;
end;


procedure DeInitializationSbVM;
begin
  if Assigned(FVM) then
  begin
    Dec(FVM.FUseCounter);
    if FVM.FUseCounter = 0 then
      FreeAndNil(FVM);
  end;
end;


function PrepareFilter(ATable: TsbTableCursor): TsbSQLInfo;
var
  S: TsbSQLParser;
  i: Integer;
begin
  S := TsbSQLParser.Create;
  try
    S.CurrSQLInfo.FFrom.Add(ATable);
    S.FSQL := ATable.Filter;
    S.FStatus.sText := S.FSQL + sqlKeyWords.FindKeyWordByCode(kwFinish);
    ScannerChangePosition(S.FStatus, 1);
    S.CurrSQLInfo.FWhere := TsbConditionNode.Create(nil);
    S.ParseLogConditions(S.CurrSQLInfo.FWhere);
    if Assigned(S.CurrSQLInfo.FWhere) then
    begin
      S.DeleteEmptyNodes(S.CurrSQLInfo.FWhere);
      S.Normalization(S.CurrSQLInfo.FWhere);
      S.PackConditions(S.CurrSQLInfo.FWhere);
      S.AnalyseConditions(S.CurrSQLInfo.FWhere);
      S.SortConditions(S.CurrSQLInfo.FWhere);
    end;
    Result := S.CurrSQLInfo;
    i := S.FSQLInfoList.IndexOf(S.CurrSQLInfo);
    S.FSQLInfoList.CleanUpItem(i);
  finally
    S.Free;
  end;
end;



function ParseCustomSQL(const ASQL: string; AParams: TsbParams;
   AOpenTableProc: TsbOpenTableFunct; ACustomParsingEndProc: TsbCustomParsingEndProc;
   ADoNotOptimize: Boolean): TsbSQLParser;
var
  lOpenTableFunct: TsbOpenTableFunct;
  lTableParamsProc: TsbTableParamsProc;
  lCustomParsingEndProc: TsbCustomParsingEndProc;
begin
  Result := TsbSQLParser.Create;
  try
    lOpenTableFunct := FOpenTableFunct;
    lTableParamsProc := FTableParamsProc;
    lCustomParsingEndProc := FCustomParsingEndProc;
    try
      FOpenTableFunct := AOpenTableProc;
      FCustomParsingEndProc := ACustomParsingEndProc;
      FTableParamsProc := nil;

      try
        Result.SQL := ASQL;
        Result.Params.Assign(AParams);
        Result.FExternalResulCursor := True;
        Result.FDoNotOptimize := ADoNotOptimize;
        Result.Prepare;
      finally
        FOpenTableFunct := nil;
      end;

      if Assigned(ACustomParsingEndProc) then
        ACustomParsingEndProc(Result);

    finally
      FOpenTableFunct := lOpenTableFunct;
      FTableParamsProc := lTableParamsProc;
      FCustomParsingEndProc := lCustomParsingEndProc;
    end;

  except
    Result.Free;
    raise;
  end;
end;



function CustomSQL(const ASQL: string; AParams: TsbParams; AResultCursor: TsbTableCursor;
   AOpenTableProc: TsbOpenTableFunct; ATableParamsProc: TsbTableParamsProc): TsbResultRec;
var
  P: TsbSQLParser;
  lOpenTableFunct: TsbOpenTableFunct;
  lTableParamsProc: TsbTableParamsProc;
  lCustomParsingEndProc: TsbCustomParsingEndProc;
  flFiltered: Boolean;
begin
  P := TsbSQLParser.Create;
  try
    lOpenTableFunct := FOpenTableFunct;
    lTableParamsProc := FTableParamsProc;
    lCustomParsingEndProc := FCustomParsingEndProc;
    FOpenTableFunct := AOpenTableProc;
    FTableParamsProc := ATableParamsProc;
    FCustomParsingEndProc := nil;
    if Assigned(AResultCursor) then
    begin
      flFiltered := AResultCursor.Filtered;
      AResultCursor.Filtered := False;
    end
    else
      flFiltered := False;

    try
      P.SQL := ASQL;
      if Assigned(AParams) then
        P.Params.Assign(AParams);

      VM.InitVM;

      if Assigned(AResultCursor) then
        P.Open(AResultCursor)
      else
        P.Execute;

  //    Result.Cursor := P.GetResultCursor;
      Result.Plan := P.Plan;
      Result.Cost := P.CostEvaluation;
    finally
      VM.InitVM;
      FOpenTableFunct := lOpenTableFunct;
      FTableParamsProc := lTableParamsProc;
      FCustomParsingEndProc := lCustomParsingEndProc;
      if Assigned(AResultCursor) then
        AResultCursor.Filtered := flFiltered;
    end;

  finally
    P.Free;
  end;
end;


function CheckConditions(ACond: TsbConditionNode): Boolean;

  function CalcCondition (ACondition: TsbConditionNode): Boolean;
  var
    i, n: Integer;
    C: TObject;
    fl: Boolean;
  begin
    if ACondition.FType = sbnAND then
      Result := True
    else
      Result := False;

    n := ACondition.ItemCount - 1;
    for i := 0 to n do
    begin
      C := ACondition.Items[i];
      if C is TsbConditionNode then
        fl := CalcCondition(TsbConditionNode(C))
      else
        fl := FVM.CalcCondition(TsbConditionItem(C));

      if ACondition.FType = sbnAND then
      begin
        Result := Result and fl;
        if not Result then
          Exit;
      end
      else
      begin
        Result := Result or fl;
        if Result then
          Exit;
      end;
    end;
  end;

begin
  if Assigned(ACond) then
    Result := CalcCondition(ACond)
  else
    Result := True;
end;



            {TsqlKeyWords}

constructor TsbKeyWords.Create;
begin
  inherited;

  {Fill list by sqlKeyWords}
  Add('+', kwPlus);
  Add('-', kwMinus);
  Add('*', kwMul);
  Add('/', kwDiv);
  Add('|', kwStick);
  Add('||', kwConcat);
  Add('(', kwLbracket);
  Add(')', kwRbracket);
  Add('[', kwLSquareBracket);
  Add(']', kwRSquareBracket);
  Add('=', kwEqual);
  Add(',', kwComma);
  Add(':', kwColon);
  Add('@', kwConst);
  Add('<', kwLess);
  Add('>', kwMore);
  Add('>=', kwMeq);
  Add('<=', kwLeq);
  Add('<>', kwNeq);
  Add('+=', kwLeftJoin);  //temporary
  Add('=+', kwLeftJoin);
  Add('/*', kwRemb);
  Add('*/', kwReme);
  Add('SELECT', kwSelect);
  Add('FROM', kwFrom);
  Add('WHERE', kwWhere);
  Add('ORDER', kwOrder);
  Add('GROUP', kwGroup);
  Add('BY', kwBy);
  Add('HAVING', kwHaving);
  Add('UNION', kwUnion);
  Add('AND', kwAnd);
  Add('OR', kwOr);
  Add('NOT', kwNot);
  Add('LEFT',  kwLeft);
  Add('RIGHT', kwRight);
  Add('FULL',  kwFull);
  Add('JOIN',  kwJoin);
  Add('INNER',  kwInner);
  Add('OUTER', kwOuter);
  Add('ON',    kwOn);
  Add('EXISTS', kwExists);
  Add('LIKE', kwLike);
  Add('BETWEEN', kwBetween);
  Add('DISTINCT', kwDistinct);
  Add('ALL', kwAll);
  Add('SUM', kwSum);
  Add('MIN', kwMin);
  Add('MAX', kwMax);
  Add('COUNT', kwCount);
  Add('AVG', kwAvg);

  Add('UPPER', kwUpper);
  Add('LOWER', kwLower);
  Add('CAST', kwCast);
  Add('SUBST', kwSubst);
  Add('ABS', kwAbs);
  Add('SUBSTR', kwSubStr);
  Add('TRIMLEFT', kwTrimLeft);
  Add('TRIMRIGHT', kwTrimRight);
  Add('TRIM', kwTrim);
  Add('FORMAT', kwFormat);
  Add('DAY', kwDay);
  Add('MONTH', kwMonth);
  Add('YEAR', kwYear);
  Add('BEGINOFYEAR', kwBeginOfYear);
  Add('ENDOFYEAR', kwEndOfYear);
  Add('BEGINOFMONTH', kwBeginOfMonth);
  Add('ENDOFMONTH', kwEndOfMonth);
  Add('BEGINOFQUARTER', kwBeginOfQuarter);
  Add('ENDOFQUARTER', kwEndOfQuarter);
  Add('QUARTER', kwQuarter);
  Add('QUARTERBEGINDATE', kwQuarterBeginDate);
  Add('QUARTERENDDATE', kwQuarterEndDate);
  Add('ROUND', kwRound);
  Add('ROUNDFIN', kwRoundFin);
  Add('TRUNC', kwTrunc);
  Add('CHECKIF', kwCheckIf);
  Add('STRLENGTH', kwStrLength);
  Add('STRBEGIN', kwStrBegin);
  Add('STREND', kwStrEnd);
  Add('SUBSTRPOS', kwSubStrPos);
//  Add('SETVAR', kwSetVar);
//  Add('GETVAR', kwGetVar);

  Add('ASC', kwAsc);
  Add('DESC', kwDesc);
  Add('IN', kwIn);
  Add('AS', kwAs);
  Add('CREATE', kwCreate);
  Add('TABLE', kwTable);
  Add('INSERT', kwInsert);
  Add('UPDATE', kwUpdate);
  Add('DELETE', kwDelete);
  Add('INTEGER', kwInteger);
  Add('AUTO_INCREMENT', kwAutoInc);
  Add('FLOAT', kwFloat);
  Add('CURRENCY', kwCurrency);
  Add('BOOLEAN', kwBoolean);
  Add('STRING', kwString);
  Add('CHAR', kwChar);
  Add('VARCHAR', kwString);
  Add('DATE', kwDate);
  Add('BLOB', kwBlob);
  Add('DROP', kwDrop);
  Add('INTO', kwInto);
  Add('SET', kwSet);
  Add('VALUES', kwValues);
  Add('IS', kwIs);
  Add('NULL', kwNull);
  Add(#0, kwFinish);

  Sorted := True;
end;

function TsbKeyWords.GetItem(AIndex: Integer): TsbKeyWordRec;
begin
  Result.Text := Strings[AIndex];
  Result.Code := TsbKeyWord(Objects[AIndex]);
  Result.WordType := KeyWordType(Result.Code);
end;

function TsbKeyWords.Add(const AText: string; ACode: TsbKeyWord): Integer;
begin
  Result := AddObject(AText, Pointer(ACode));
end;

function TsbKeyWords.FindKeyWord(const AToken: string): TsbKeyWord;
var
  i: Integer;
begin
  i := IndexOf(AnsiUpperCase(AToken));
  if i = -1 then
    Result := kwUnknown
  else
    Result := Items[i].Code;
end;

function TsbKeyWords.FindKeyWordByCode(AKeyWord: TsbKeyWord): string;
var
  i: Integer;
begin
  i := IndexOfObject(Pointer(AKeyWord));
  if i = -1 then
    Result := ''
  else
    Result := Items[i].Text;
end;


function TsbKeyWords.KeyWordType(AKeyWord: TsbKeyWord): TsbTypeKeyWord;
begin
  if AKeyWord < kwSum then
    Result := tkwNone
  else if (AKeyWord >= kwSum) and (AKeyWord < kwUpper) then
    Result := tkwAgrFunct
  else
    Result := tkwUnaryFunct;
end;



{ TsbConditionNode }

constructor TsbConditionNode.Create(AOwner: TsbConditionNode);
begin
  inherited Create;
  FParent := AOwner;
  FType := sbnAND;
  FNot := False;
end;


function TsbConditionNode.GetItem(Index: Integer): TObject;
begin
  Result := TObject(FList[Index]);
end;


function TsbConditionNode.AddNode: Integer;
var
  C: TsbConditionNode;
begin
  C := TsbConditionNode.Create(Self);
  Result := FList.Add(C);
end;


function TsbConditionNode.AddItem: Integer;
var
  C: TsbConditionItem;
begin
  C := TsbConditionItem.Create(Self);
  Result := FList.Add(C);
end;


procedure TsbConditionNode.Delete(AIndex: Integer);
begin
  if Assigned(FList[AIndex]) then
    TObject(FList[AIndex]).Free;
  FList.Delete(AIndex);
end;


procedure TsbConditionNode.Delete(AItem: TObject);
begin
  Delete(IndexOf(AItem));
end;


class procedure TsbConditionNode.Collapse(ANode: TsbConditionNode);
var
  i: Integer;
  O: TObject;
begin
  O := TObject(ANode.FList[0]);
  if Assigned(ANode.FParent) then
  begin
    i := ANode.FParent.IndexOf(ANode);
    ANode.FParent.FList[i] := O;
  end;
  if O is TsbConditionNode then
  begin
    TsbConditionNode(O).FParent := ANode.FParent;
    TsbConditionNode(O).FNot := TsbConditionNode(O).FNot xor ANode.FNot;
  end
  else
  begin
    if not Assigned(ANode.FParent) then
      Exit;
    TsbConditionItem(O).FParent := ANode.FParent;
    TsbConditionItem(O).FNot := TsbConditionItem(O).FNot xor ANode.FNot;
  end;
  ANode.FList.Delete(0);
  ANode.Free;
end;


procedure TsbConditionNode.InsertNode(AIndex: Integer; AItem: TObject; ADelete: Boolean = True);
var
  P: TsbConditionNode;
begin
  FList.Insert(AIndex, AItem);

  if AItem is TsbConditionNode then
  begin
    P := TsbConditionNode(AItem).FParent;
    TsbConditionNode(AItem).FParent := Self;
  end
  else
  begin
    P := TsbConditionItem(AItem).FParent;
    TsbConditionItem(AItem).FParent := Self;
  end;

  if Assigned(P) then
    if ADelete then
      P.FList.Delete(P.IndexOf(AItem))
    else
      P.FList[P.IndexOf(AItem)] := nil;
end;



{ TsbSQLInfo }

constructor TsbSQLInfo.Create;
begin
  FFrom := TsbTableList.Create;
  FSelect := TsbSelectFieldList.Create;
  FWhere := nil;
  FOrder := nil;
  FGroup := nil;
  FHaving:= nil;
  FAggrFunct := nil;
  FJoinTreeList := TsbJoinedTablesTreeList.Create;
end;


destructor TsbSQLInfo.Destroy;
begin
  FreeAndNil(FAggrFunct);
  FreeAndNil(FHaving);
  FreeAndNil(FGroup);
  FreeAndNil(FOrder);
  FreeAndNil(FWhere);
  FreeAndNil(FFrom);
  FreeAndNil(FSelect);
  FreeAndNil(FJoinTreeList);
  FreeAndNil(FResult);
  inherited;
end;


procedure TsbSQLInfo.ImplementJoins;
var
  i, j: Integer;
  n: Integer;
  lTop: TsbTableItem;
  lFld: array of string;


  function CheckKey(ATable: TsbTableItem): Boolean;
  var
    i: Integer;
  begin
    Result := True;
    for i := 0 to ATable.FParentJoinInfo.FSelfJoinFields.Count -  1do
      if TsbField(ATable.FParentJoinInfo.FSelfJoinFields[i]).Value <>
         TsbField(ATable.FParentJoinInfo.FForeignJoinFields[i]).Value then
      begin
        Result := False;
        break;
      end;
  end;


  function CheckMultipleKeys(ATable: TsbTableItem; APriorLeftOuter: Boolean): Boolean;
  var
    i, j: Integer;
    n: Integer;

      function JumpByIndexes(ATable: TsbTableItem; APriorLeftOuter: Boolean): Boolean;
      var
        i: Integer;
        T: TsbTableItem;
      begin
        Result := True;
        for i := 0 to ATable.FJoins.ItemCount - 1 do
        begin
          T := ATable.FJoins[i].FRightTable;
          if ATable.FTable.OuterPosition then
          begin
            T.FTable.OuterPosition := True;
            Result := True;
          end
          else
          begin
            T.FTable.OuterPosition := False;

            if T.FParentJoinInfo.FNaturalJoin then
            begin
              T.FTable.First;
              Result := T.FTable.LocateByPreparedCondition(T.FParentJoinInfo.FJoinCondition);
            end
            else
              Result := T.FTable.FindKeyByFieldList(ATable.FJoins[i].FRightTable.FParentJoinInfo.FForeignJoinFields);

            if not Result then
            begin
              if ATable.FJoins[i].FJoinType = sjtLeftJoin then
              begin
                T.FTable.OuterPosition := True;
                Result := True;
              end
            end

            else if not T.FParentJoinInfo.FNaturalJoin then
            begin
              Result := CheckConditions(T.FParentJoinInfo.FJoinCondition);
              if not Result and (ATable.FJoins[i].FJoinType = sjtLeftJoin) then
              begin
                T.FTable.OuterPosition := True;
                Result := True;
              end;
            end;
          end;

          if not Result then
          begin
            if APriorLeftOuter then
            begin
              ATable.FTable.OuterPosition := True;
              T.FTable.OuterPosition := True;
            end
            else
              break;
          end;

          repeat
            Result := JumpByIndexes(T, APriorLeftOuter or (ATable.FJoins[i].FJoinType = sjtLeftJoin));
            if Result then
              break;
            T.FTable.Next;

            if T.FParentJoinInfo.FNaturalJoin then
            begin
              Result := T.FTable.LocateByPreparedCondition(T.FParentJoinInfo.FJoinCondition);
              if not Result then
                break;
            end;
          until not(not T.FTable.Eof and (T.FParentJoinInfo.FNaturalJoin  or
            CheckKey(T) and CheckConditions(T.FParentJoinInfo.FJoinCondition)));


          if not Result then
            break;
        end;
      end;


      procedure DoCartesianProduct(AIndex: Integer);
      var
        i, j, m, n: Integer;
      begin
        if AIndex = ATable.FJoins.ItemCount then
        begin
          n := ATable.FJoins.ItemCount - 1;
          for i := 0 to n do
          begin
            m := ATable.FJoins[i].FRightTable.FSelRecs.FFields.Count - 1;
            for j := 0  to m do
              ATable.FSelRecs.Fields[Integer(ATable.FJoins[i].FRightTable.FSelRecs.FFields.Objects[j])] :=
                ATable.FJoins[i].FRightTable.FSelRecs.Fields[j];
          end;

          if ATable.FTable.OuterPosition then
            ATable.FSelRecs.Fields[0] := 0
          else
            ATable.FSelRecs.Fields[0] := ATable.FTable.InternalRecordPos;
          ATable.FSelRecs.RecNo := ATable.FSelRecs.RecNo + 1;
          Exit;
        end;

        n := ATable.FJoins[AIndex].FRightTable.FSelRecs.RecordCount;
        for i := 1 to n do
        begin
          ATable.FJoins[AIndex].FRightTable.FSelRecs.RecNo := i;
          DoCartesianProduct(AIndex+1);
        end;
      end;


  begin
    ATable.FSelRecs.ClearTable;
    while ATable.FTable.OuterPosition or
          not ATable.FTable.Eof and
          (ATable.FParentJoinInfo.FNaturalJoin  or
           CheckKey(ATable) and CheckConditions(ATable.FParentJoinInfo.FJoinCondition)) do
    begin
      Result := JumpByIndexes(ATable, APriorLeftOuter);
      if Result then
      begin
        for i := 0 to ATable.FJoins.ItemCount - 1 do
        begin
          Result := CheckMultipleKeys(ATable.FJoins[i].FRightTable,
            APriorLeftOuter or (ATable.FJoins[i].FJoinType = sjtLeftJoin));
          if not Result then
            break;
        end;

        if Result then
        begin
          n := 1;
          for i := 0 to ATable.FJoins.ItemCount - 1 do
          begin
            n := n * ATable.FJoins[i].FRightTable.FSelRecs.RecordCount;
            if n < 0 then
              ShowSBError(sbeOverflow, []);
          end;

          if n > 0 then
          begin
            i := ATable.FSelRecs.RecNo;
            ATable.FSelRecs.Append(n);
            ATable.FSelRecs.RecNo := i + 1;

            for i := 0 to ATable.FJoins.ItemCount - 1 do
            begin
              for j := 0 to ATable.FJoins[i].FRightTable.FSelRecs.FFields.Count - 1 do
               ATable.FJoins[i].FRightTable.FSelRecs.FFields.Objects[j] :=
                 Pointer(ATable.FSelRecs.IndexOfField(ATable.FJoins[i].FRightTable.FSelRecs.FFields[j]));
            end;

            DoCartesianProduct(0);
          end;
        end;
      end;

      if ATable.FTable.OuterPosition then
        break;

      if (ATable <> lTop) and ATable.FParentJoinInfo.FNaturalJoin then
        repeat
          ATable.FTable.Next;
        until ATable.FTable.Eof or ATable.Ftable.LocateByPreparedCondition(ATable.FParentJoinInfo.FJoinCondition)
      else
        ATable.FTable.Next;
    end;
    Result := (ATable.FSelRecs.RecordCount > 0);
  end;


  procedure DoFinalCartesianProductByJoins(AIndex: Integer);
  var
    i, j, n, m: Integer;
  begin
    if AIndex = FJoinTreeList.ItemCount then
    begin
      n := FJoinTreeList.ItemCount - 1;
      for i := 0 to n do
      begin
        m := FJoinTreeList[i].FSelRecs.FFields.Count - 1;
        for j := 0  to m do
          FResult.Fields[Integer(FJoinTreeList[i].FSelRecs.FFields.Objects[j])] := FJoinTreeList[i].FSelRecs.Fields[j];
      end;
      FResult.RecNo := FResult.RecNo + 1;
      Exit;
    end;

    n := FJoinTreeList[AIndex].FSelRecs.RecordCount;
    for i := 1 to n do
    begin
      FJoinTreeList[AIndex].FSelRecs.RecNo := i;
      DoFinalCartesianProductByJoins(AIndex+1);
    end;
  end;

begin
  // JOINS

  for i := 0 to FJoinTreeList.ItemCount - 1 do
  begin
    lTop := FJoinTreeList[i];
    lTop.InitTempTable;

    lTop.FTable.First;
    CheckMultipleKeys(lTop, False);
  end;

  if FJoinTreeList.ItemCount > 1 then
  begin
    n := 0;
    for i := 0 to FJoinTreeList.ItemCount - 1 do
    begin
      if n = 0 then
        n := FJoinTreeList[i].FSelRecs.RecordCount
      else
        n := Integer(Cardinal(n) * Cardinal(FJoinTreeList[i].FSelRecs.RecordCount));

      if n < 0 then
        ShowSBError(sbeOverflow, []);

      SetLength(lFld, Length(lFld)+FJoinTreeList[i].FSelRecs.FFields.Count);
      for j := 0 to FJoinTreeList[i].FSelRecs.FFields.Count - 1 do
        lFld[Length(lFld)-FJoinTreeList[i].FSelRecs.FFields.Count+j] :=
          FJoinTreeList[i].FSelRecs.FFields[j];
    end;
    FResult := TsbTempTable.Create;
    FResult.CreateTable(lFld);
    SetLength(lFld, 0);

    FResult.Append(n);
    FResult.RecNo := 1;

    for i := 0 to FJoinTreeList.ItemCount - 1 do
    begin
      for j := 0  to FJoinTreeList[i].FSelRecs.FFields.Count - 1 do
        FJoinTreeList[i].FSelRecs.FFields.Objects[j] := Pointer(FResult.IndexOfField(FJoinTreeList[i].FSelRecs.FFields[j]));
    end;

    DoFinalCartesianProductByJoins(0);
  end

  else
  begin
    FResult := FJoinTreeList[0].FSelRecs;
    FJoinTreeList[0].FSelRecs := nil;
  end;

  for i := 0 to FFrom.ItemCount - 1 do
    FreeAndNil(FFrom[i].FSelRecs);
end;


{ TsbSQLInfoList }

function TsbSQLInfoList.Add: Integer;
begin
  Result := FList.Add(TsbSQLInfo.Create);
end;


procedure TsbSQLInfoList.CleanUpItem(AIndex: Integer);
begin
  FList[AIndex] := nil;
  if FCurrentSQLInfo = AIndex then
    FCurrentSQLInfo := -1;
end;

procedure TsbSQLInfoList.Clear;
begin
  inherited;
  FCurrentSQLInfo := -1;
end;


constructor TsbSQLInfoList.Create;
begin
  inherited;
  FDistinct := True;
  Add;
  FCurrentSQLInfo := 0;
end;

function TsbSQLInfoList.GetItem(Index: Integer): TsbSQLInfo;
begin
  Result := TsbSQLInfo(FList[Index]);
end;


function TsbSQLInfoList.IsUnion: Boolean;
begin
  Result := ItemCount > 1;
end;


{ TsbConditionItem }

constructor TsbConditionItem.Create(AOwner: TsbConditionNode);
begin
  FParent := AOwner;
  FLeftExpression := nil;
  FRightExpression := nil;
  FStrExpression := '';
end;

procedure TsbConditionItem.Assign(ASource: TsbConditionItem);
begin
  FreeAndNil(FLeftExpression);
  if Assigned(ASource.LeftExpression) then
  begin
    CreateExpression('L');
    LeftExpression.Assign(ASource.LeftExpression);
  end;

  FreeAndNil(FRightExpression);
  if Assigned(ASource.RightExpression) then
  begin
    CreateExpression('R');
    RightExpression.Assign(ASource.RightExpression);
  end;

  FStrExpression := ASource.StrExpression;
  ASource.FNot := ASource.FNot;
end;

procedure TsbConditionItem.CreateExpression(AType: Char);
begin
   case AType of
     'L': if not Assigned(FLeftExpression) then
            FLeftExpression := TsbExpression.Create;
     'R': if not Assigned(FRightExpression) then
            FRightExpression := TsbExpression.Create;
   end;
end;

destructor TsbConditionItem.Destroy;
begin
  FreeAndNil(FLeftExpression);
  FreeAndNil(FRightExpression);

  inherited;
end;



{ TsbExpression }

destructor TsbExpression.Destroy;
begin
  FreeAndNil(FSQL);
  inherited;
end;


procedure TsbExpression.Clear;
var
  i: Integer;
begin
  for i := 0 to FList.Count-1 do
    Dispose(PTsbExpressionItem(FList[i]));
  FList.Clear;
end;


function TsbExpression.GetItem(Index: Integer): PTsbExpressionItem;
begin
  Result := PTsbExpressionItem(FList[Index]);
end;


function TsbExpression.Add(AOperation: TsbCalcOperation): Integer;
var
  itm: PTsbExpressionItem;
begin
  New(itm);
  itm^.FField := nil;
  itm^.FTable := nil;
  itm^.FConstValue := Unassigned;
  itm^.FParam := nil;
  itm^.FFunction := sftUnknown;
  itm^.FAggrFunction := nil;

  itm^.FOperation := AOperation;
  Result := FList.Add(itm);
end;


function TsbExpression.CurrentItem: PTsbExpressionItem;
begin
  Result := Items[FPosition];
end;

procedure TsbExpression.Assign(ASource: TsbItemList);
var
  i, j: Integer;
begin
  Clear;

  for i := 0 to ASource.ItemCount - 1 do
  begin
    j := Add(TsbExpression(ASource)[i].FOperation);
    Items[j].FConstValue := TsbExpression(ASource)[i].FConstValue;
    Items[j].FField := TsbExpression(ASource)[i].FField;
    Items[j].FTable := TsbExpression(ASource)[i].FTable;
    Items[j].FParam := TsbExpression(ASource)[i].FParam;
    Items[j].FFunction := TsbExpression(ASource)[i].FFunction;
    Items[j].FAggrFunction := TsbExpression(ASource)[i].FAggrFunction;
  end;
end;


{ TsbTableItem }

constructor TsbTableItem.Create;
begin
  FSelRecs := nil;
  FJoins := TsbJoins.Create;
  FParentJoinInfo := TsbParentJoinInfo.Create;
  FManualJoined := False;
end;


destructor TsbTableItem.Destroy;
begin
  if Length(FPhysicalName) > 0 then
    sbClose(FTable);
  FreeAndNil(FJoins);
  FreeAndNil(FParentJoinInfo);
  FreeAndNil(FSelRecs);
  inherited;
end;


procedure TsbTableItem.InitTempTable;
var
  lFld: array of string;

  procedure CheckJoins(AJoins: TsbJoins);
  var
    i: Integer;
  begin
    for i := 0 to AJoins.ItemCount - 1 do
    begin
      SetLength(lFld, Length(lFld)+1);
      lFld[High(lFld)] := AJoins[i].FRightTable.FAlias;
      CheckJoins(AJoins[i].FRightTable.FJoins);
      AJoins[i].FRightTable.InitTempTable;
    end;
  end;

begin
  if not Assigned(FSelRecs) then
  begin
    SetLength(lFld, 1);
    lFld[0] := FAlias;
    CheckJoins(FJoins);
    FSelRecs := TsbTempTable.Create;
    FSelRecs.CreateTable(lFld);
    SetLength(lFld, 0);
  end;
end;


procedure TsbTableItem.OpenTable;
begin
  if Assigned(FOpenTableFunct) then
    FTable := FOpenTableFunct(FPhysicalName)
  else
  begin
    FTable := sbOpenTable(FPhysicalName, nil, sbmRead{sbmMemory});
    FTable.DisableNotification;
  end;
end;


procedure TsbTableItem.SetAlias(const AValue: string);
begin
  FAlias := AnsiUpperCase(AValue);
end;



{ TsbTableList }

constructor TsbTableList.Create;
begin
  inherited;
  FPrevFrom := nil;
end;


function TsbTableList.Add(const ATableName: string): Integer;
var
  T: TsbTableItem;
begin
  T := TsbTableItem.Create;
  T.FIndex := FList.Add(T);
  T.FPhysicalName := ATableName;
  T.Alias := ATableName;
  T.OpenTable;
  Result := T.FIndex;
end;


function TsbTableList.Add(ATable: TsbTableCursor): Integer;
var
  T: TsbTableItem;
begin
  T := TsbTableItem.Create;
  T.FIndex := FList.Add(T);
  T.FPhysicalName := '';
  T.FTable := ATable;
  T.Alias := ATable.TableName;
  Result := T.FIndex;
end;


function TsbTableList.GetItem(Index: Integer): TsbTableItem;
begin
  Result := TsbTableItem(FList[Index]);
end;


function TsbTableList.FindField(const AFieldName: string; var ATable: TsbTableItem): TsbField;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to FList.Count-1 do
  begin
    Result := TsbTableItem(FList[i]).FTable.Fields.FindField(AFieldName);
    if Assigned(Result) then
    begin
      ATable := TsbTableItem(FList[i]);
      Exit;
    end;
  end;

  if Assigned(FPrevFrom) then
    Result := FPrevFrom.FindField(AFieldName, ATable);
end;


function TsbTableList.FindTable(const ATableName: string): TsbTableItem;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to FList.Count-1 do
    if SameText(TsbTableItem(FList[i]).FAlias, ATableName) then
    begin
      Result := TsbTableItem(FList[i]);
      Exit;
    end;

  if Assigned(FPrevFrom) then
    Result := FPrevFrom.FindTable(ATableName);
end;



{ TsbSelectField }

constructor TsbSelectField.Create;
begin
  FExpression := TsbExpression.Create;
  FAggrFunctsExist := False;
  FHidden := False;
end;


destructor TsbSelectField.Destroy;
begin
  FreeAndNil(FExpression);
  inherited;
end;



{ TsbSelectFieldList }

function TsbSelectFieldList.Add: TsbSelectField;
begin
  Result := TsbSelectField.Create;
  FList.Add(Result);
end;


function TsbSelectFieldList.GetItem(Index: Integer): TsbSelectField;
begin
  Result := TsbSelectField(FList[Index]);
end;



{ TsbJoins }

function TsbJoins.Add(ATable: TsbTableItem; AJoinType: TsbJoinType): Integer;
var
  J: TsbJoin;
begin
  J := TsbJoin.Create;
  J.FRightTable := ATable;
  J.FJoinType := AJoinType;

  Result := FList.Add(J);
end;


function TsbJoins.GetItem(Index: Integer): TsbJoin;
begin
  Result := TsbJoin(FList[Index]);
end;


function TsbJoins.JoinByTable(ATable: TsbTableItem): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to ItemCount - 1 do
    if Items[i].FRightTable = ATable then
    begin
      Result := i;
      break;
    end;
end;


{ TsbJoinedTablesTreeList }

function TsbJoinedTablesTreeList.Add(ATable: TsbTableItem): Integer;
begin
  Result := FList.Add(ATable);
end;


procedure TsbJoinedTablesTreeList.Clear;
begin
  FList.Clear;
end;


procedure TsbJoinedTablesTreeList.Delete(AIndex: Integer);
begin
  FList.Delete(AIndex);
end;


function TsbJoinedTablesTreeList.GetItem(Index: Integer): TsbTableItem;
begin
  Result := TsbTableItem(FList[Index]);
end;




{ TsbOrderByList }

function TsbOrderByList.Add(AIndex: Integer; ADesc: Boolean): Integer;
var
  T: TsbOrderByItem;
begin
  T := TsbOrderByItem.Create;
  T.FSelIndex := AIndex;
  T.FDesc := ADesc;
  Result := FList.Add(T);
end;


function TsbOrderByList.GetItem(Index: Integer): TsbOrderByItem;
begin
  Result := TsbOrderByItem(FList[Index]);
end;



{ TsbVirtualMachine }

constructor TsbVirtualMachine.Create;
begin
  FVariables := TsbVMVarList.Create;
  InitVM;
end;


destructor TsbVirtualMachine.Destroy;
begin
  FreeAndNil(FVariables);
  inherited;
end;

function TsbVirtualMachine.ValueInArray(const AValue, AArray: Variant): Boolean;
var
  i: Integer;
  A, B: PVarArray;
  Pa, Pb: Pointer;
begin
  if VarIsArray(AValue) then
  begin
    // compare arrays
    Result := False;
    if VarArrayHighBound(AValue, 1) - VarArrayLowBound(AValue, 1) <> VarArrayHighBound(AArray, 1) - VarArrayLowBound(AArray, 1) then
      exit;

    A := VarArrayAsPSafeArray(AValue);
    B := VarArrayAsPSafeArray(AArray);
    Result := True;
    for i := VarArrayLowBound(AArray, 1) to VarArrayHighBound(AArray, 1) do
    begin
      Pa := Pointer(Cardinal(A.Data) + Cardinal(i * A.ElementSize));
      Pb := Pointer(Cardinal(B.Data) + Cardinal(i * B.ElementSize));
      if not VariantsAreSame(PVariant(Pa)^, PVariant(Pb)^) then
      begin
        Result := False;
        break;
      end;
    end;
  end

  else
  begin
    // single value in array
    Result := False;
    A := VarArrayAsPSafeArray(AArray);
    for i := VarArrayLowBound(AArray, 1) to VarArrayHighBound(AArray, 1) do
    begin
      Pa := Pointer(Cardinal(A.Data) + Cardinal(i * A.ElementSize));
      if VariantsAreSame(PVariant(Pa)^,  AValue) then
      begin
        Result := True;
        break;
      end;
    end;
  end;
end;

function TsbVirtualMachine.CalcCondition(ACondition: TsbConditionItem): Boolean;
var
  LeftVal, RightVal, RightVal2: Variant;
  lCur: TsbTableCursor;
  fl: Boolean;
begin
  Result := False;
  if Assigned(ACondition.FLeftExpression) then
  begin
    ACondition.FLeftExpression.FPosition := 0;
    CalcExpression(ACondition.FLeftExpression);
    LeftVal := FRegA;
    if FOuterMet then
    begin
      Result := True;
      Exit;
    end;

    if VarIsNull(LeftVal) and (ACondition.FOperation <> scoIsNull) then
    begin
      Result := False;
      Exit;
    end;
  end;

  if Assigned(ACondition.FRightExpression) then
    ACondition.FRightExpression.FPosition := 0;

  if ACondition.FOperation = scoIn then
  begin
    if ACondition.FNot then
      Result := True
    else
      Result := False;

    if Assigned(ACondition.FRightExpression.FSQL) then
    begin
      ACondition.FRightExpression.FSQL.FParser.Open; //Recursion!
      lCur := ACondition.FRightExpression.FSQL.FParser.FResultCursor;
      if lCur.Fields.Count <> 1 then
        ShowSBError(sbeVMTooManyColumns, []);
      Result := False;
      while not lCur.Eof do
      begin
        if LeftVal = lCur.Fields[0].Value then
        begin
          if ACondition.FNot then
            Result := False
          else
            Result := True;
          break;
        end;
        lCur.Next;
      end;
    end

    else
      repeat
        CalcExpression(ACondition.FRightExpression);
        RightVal :=  FRegA;
        CheckDate(LeftVal, RightVal);

        if VarIsArray(RightVal) then
          fl := ValueInArray(LeftVal, RightVal)
        else if VarIsArray(LeftVal) then
          fl := ValueInArray(RightVal, LeftVal)
        else
          fl := LeftVal = RightVal;

        if fl then
          if ACondition.FNot then
          begin
            Result := False;
            break;
          end
          else
          begin
            Result := True;
            break;
          end;
      until ACondition.FRightExpression.FPosition >= ACondition.FRightExpression.ItemCount;
    Exit;
  end

  else if ACondition.FOperation = scoExists then
  begin
    ACondition.FRightExpression.FSQL.FParser.Open; //Recursion!
    Result := (ACondition.FRightExpression.FSQL.FParser.FResultCursor.RecordCount > 0);
  end

  else if ACondition.FOperation = scoIsNull then
    Result := VarIsNull(LeftVal)

  else
  begin
    if Assigned(ACondition.FRightExpression.FSQL) then
    begin
      ACondition.FRightExpression.FSQL.FParser.Open; //Recursion!
      lCur := ACondition.FRightExpression.FSQL.FParser.FResultCursor;
      if lCur.Fields.Count <> 1 then
        ShowSBError(sbeVMTooManyColumns, []);
      if lCur.RecordCount > 1 then
        ShowSBError(sbeVMTooManyRows, []);

      RightVal := lCur.Fields[0].Value;
    end
    else
    begin
      CalcExpression(ACondition.FRightExpression);
      RightVal :=  FRegA;
    end;

    if FOuterMet then
    begin
      Result := True;
      Exit;
    end;

    if VarIsNull(RightVal) then
    begin
      Result := False;
      Exit;
    end;

    CheckDate(LeftVal, RightVal);
    case ACondition.FOperation of
      scoEqual:        begin
                         if VarIsArray(RightVal) then
                           Result := ValueInArray(LeftVal, RightVal)
                         else if VarIsArray(LeftVal) then
                           Result := ValueInArray(RightVal, LeftVal)
                         else
                           Result := LeftVal = RightVal;
                       end;

      scoLess:         Result := (LeftVal < RightVal);

      scoMore:         Result := (LeftVal > RightVal);

      scoNotEqual:     begin
                         if VarIsArray(RightVal) then
                           Result := not ValueInArray(LeftVal, RightVal)
                         else if VarIsArray(LeftVal) then
                           Result := not ValueInArray(RightVal, LeftVal)
                         else
                           Result := (LeftVal <> RightVal);
                       end;

      scoLessOrEqual:  Result := (LeftVal <= RightVal);

      scoMoreOrEqual:  Result := (LeftVal >= RightVal);

      scoLike:         Result := StrLikeStr(LeftVal, RightVal);

      scoBetween:      begin
                         CalcExpression(ACondition.FRightExpression);
                         RightVal2 :=  FRegA;
                         CheckDate(LeftVal, RightVal2);
                         Result := (LeftVal >= RightVal) and (LeftVal <= RightVal2);
                       end;
    end;
  end;

  if ACondition.FNot then
    Result := not Result;
end;


procedure TsbVirtualMachine.CalcExpression(AExpression: TsbExpression);
var
  V: Variant;
  i: Integer;

  function CalcFunction(AFunction: TsbFunctionType; AParamCount: Integer): Variant;
  var
    Par: array of Variant;
    i: Integer;
  begin
    try
      SetLength(Par, AParamCount);

      if AFunction = sftCheckIf then // This is an exception! CheckIF is rather a branching operator than a function.
      begin
        Assert(AParamCount = 5, 'Unexpected error');
        CalcExpression(AExpression);
        Par[3] := FRegA;
        Par[4] := FRegB;
        for i := 0 to 2 do
        begin
          CalcExpression(AExpression);
          Par[i] := FRegA;
        end;
        Result := FunctCheckIf(Par, AExpression);
      end

      else
      begin
        for i := 0 to AParamCount - 1 do
        begin
          CalcExpression(AExpression);
          Par[i] := FRegA;
        end;
        Result := ExecuteFunct(AFunction, Par);
      end;

    finally
      SetLength(Par, 0);
    end;
  end;


  procedure Operand(var Result: Variant);
  var
    fnct: TsbFunctionType;
    n: Int64;
    v: Variant;
    parCount: Integer;
  begin
    if Assigned(AExpression.CurrentItem.FField) then
      if FDefExprType then
      begin
        case AExpression.CurrentItem.FField.FieldType of
          sbfString:   Result := VarAsType(StringOfChar(' ', AExpression.CurrentItem.FField.Size), varString);
          sbfInteger:  Result := VarAsType(Integer(Trunc(Random(10000))), varInteger);
          sbfAutoInc:  Result := VarAsType(Integer(Trunc(Random(10000))), varInteger);
          sbfByte:     Result := VarAsType(Byte(Trunc(Random(255))), varByte);
          sbfDateTime: Result := VarAsType(Date+Trunc(Random(365)), varDate);
          sbfFloat:    Result := VarAsType(Random(10000),varDouble);
          sbfCurrency: Result := VarAsType(Random(10000), varCurrency);
          sbfBoolean:  Result := True;
          sbfBLOB:     Result := VarAsType(' ', varOleStr);
        else
          Result := Unassigned;
        end;
        if Length(FExprFieldName) = 0 then
          FExprFieldName := AExpression.CurrentItem.FField.FieldName
        else
          FExprFieldName :=  ' ';
      end
      else
      begin
        FOuterMet := AExpression.CurrentItem.FField.Cursor.OuterPosition;
        if FOuterMet then
        begin
          TVarData(Result).VType := varNull;
          if FIgnoreOuterMet then
            FOuterMet := False;
        end
        else
          with AExpression.CurrentItem^.FField do
          begin
            n := Cursor.AuxInternalRecordPos;
            if n <> -1 then
              Cursor.InternalRecordPos := Cardinal(n);
            Result := Value;
          end;
      end

    else if Assigned(AExpression.CurrentItem.FParam) then
      Result := AExpression.CurrentItem.FParam.Value

    else if Assigned(AExpression.CurrentItem.FAggrFunction) then
      Result := AExpression.CurrentItem.FAggrFunction.FCurrentValue

    else if AExpression.CurrentItem.FFunction <> sftUnknown then
    begin
      fnct := AExpression.CurrentItem.FFunction;
      parCount := AExpression.CurrentItem.FConstValue;
      Inc(AExpression.FPosition);
      PUSH(FRegA);
      PUSH(FRegB);
      v := CalcFunction(fnct, parCount);
      FRegB := POP;
      FRegA := POP;
      Result := v;
    end

    else
      Result := AExpression.CurrentItem.FConstValue;
  end;


begin
  FOuterMet := False;

  if FDefExprType then
    FExprFieldName := '';

  while AExpression.FPosition < AExpression.ItemCount do
  begin
    case AExpression.CurrentItem.FOperation of
      smoMOVA:         Operand(FRegA);

      smoMOVB:         Operand(FRegB);

      smoPushA:        PUSH(FRegA);

      smoPopB:         FRegB := POP;

      smoSwapAB:       begin
                         V := FRegA;
                         FRegA := FRegB;
                         FRegB := V;
                       end;

      smoAdd:          if VarIsNull(FRegA) or VarIsNull(FRegB) then
                         TVarData(FRegA).VType := varNull
                       else
                         FRegA := FRegA + FRegB;

      smoSubstract:    if VarIsNull(FRegA) or VarIsNull(FRegB) then
                         TVarData(FRegA).VType := varNull
                       else
                       begin
                         CheckDate(FRegA, FRegB);
                         FRegA := FRegA - FRegB;
                       end;

      smoReverseSignA: if VarIsNull(FRegA) then
                         TVarData(FRegA).VType := varNull
                       else
                         FRegA := - FRegA;

      smoDivide:       if not FDefExprType then
                       begin
                         if VarIsNull(FRegA) or VarIsNull(FRegB) then
                           TVarData(FRegA).VType := varNull
                         else
                           FRegA := FRegA/FRegB;
                       end
                       else
                         FRegA := VarAsType(Random(10000),varDouble);

      smoMultiply:     if VarIsNull(FRegA) or VarIsNull(FRegB) then
                         TVarData(FRegA).VType := varNull
                       else
                         FRegA := FRegA * FRegB;

      smoConcat:       if VarIsNull(FRegA) and VarIsNull(FRegB) then
                         TVarData(FRegA).VType := varNull
                       else
                         FRegA := VarToStr(FRegA) + VarToStr(FRegB);

      smoDefineArray:  begin
                         V := VarArrayCreate([0, AExpression.CurrentItem.FConstValue - 1], varVariant);
                         Inc(AExpression.FPosition);
                         for i := VarArrayLowBound(V, 1) to VarArrayHighBound(V, 1) do
                         begin
                           CalcExpression(AExpression);              //recursion
                           V[i] := FRegA;
                         end;
                         FRegA := V;
                       end;

      smoNone:         begin
                         Inc(AExpression.FPosition);
                         break;
                       end;
    else
      ShowSBError(sbeVMUnknownInstruction, []);
    end;

    Inc(AExpression.FPosition);
  end;

  if FDefExprType and (Length(AExpression.FAlias) > 0) then
    FExprFieldName :=  AExpression.FAlias;
end;


function TsbVirtualMachine.Pop: Variant;
begin
  if FStackPointer < 0 then
    ShowSBError(sbeVMStackViolation, []);
  Result := FStack[FStackPointer];
  VarClear(FStack[FStackPointer]); // release memory
  Dec(FStackPointer);
end;


procedure TsbVirtualMachine.Push(const AValue: Variant);
begin
  if FStackPointer >= sbVMStackSize then
    ShowSBError(sbeVMStackOverflow, []);
  Inc(FStackPointer);
  FStack[FStackPointer] := AValue;
end;


procedure TsbVirtualMachine.InitVM;
begin
  FStackPointer := -1;
  FDefExprType := False;
  FIgnoreOuterMet := False;
  FVariables.Clear;
end;


function TsbVirtualMachine.StrLikeStr(const AStr, APattern: String): Boolean;
Label L;
var
  Ns, Np, i, j, h: Integer;

  function SkipWildcards: Boolean;
  begin
    Result := False;
    while (j <= Np) and (APattern[j] = '%') do
    begin
      Inc(j);
      Result := True;
    end;
  end;

begin
  Ns := Length(AStr);
  Np := Length(APattern);

  j := 1;
  i := 1;
  Result := False;

  while (i <= Ns) and (j <= Np)  do
  begin
    if SkipWildcards then
    begin
      h := j;
      while (i <= Ns) and (j <= Np) and (APattern[j] <> '%') do
      begin
        if not ((APattern[j] = '?') or (AStr[i] = APattern[j])) then
          j := h
        else
          Inc(j);
        Inc(i);
      end;

      if (i > Ns) or (j > Np) then
        break;
    end

    else
    begin
      if not ((APattern[j] = '?') or (AStr[i] = APattern[j])) then
        Exit;
      Inc(j);
      Inc(i);
    end;
  end;

  if (j <= Np) and (i > Ns) then
  begin
    if SkipWildcards and (j > Np) then
      Result := True;
  end
  else if (j > Np) and (i <= Ns) then
  begin
    if APattern[Np] = '%' then
      Result := True;
  end
  else
    Result := True;
end;


function TsbVirtualMachine.ExecuteFunct(const AFunction: TsbFunctionType;
  const AParams: array of variant): Variant;
begin
  case AFunction of
    sftUpper:          Result := FunctUpper(AParams);
    sftLower:          Result := FunctLower(AParams);
    sftCast:           Result := FunctCast(AParams);
    sftSubst:          Result := FunctSubst(AParams);
    sftAbs:            Result := FunctAbs(AParams);
    sftSubStr:         Result := FunctSubStr(AParams);
    sftTrimLeft:       Result := FunctTrimLeft(AParams);
    sftTrimRight:      Result := FunctTrimRight(AParams);
    sftTrim:           Result := FunctTrim(AParams);
    sftFormat:         Result := FunctFormat(AParams);
    sftDay:            Result := FunctDecodeDate(AParams, 'D');
    sftMonth:          Result := FunctDecodeDate(AParams, 'M');
    sftYear:           Result := FunctDecodeDate(AParams, 'Y');
    sftBeginOfYear:    Result := FunctBeginOfYear(AParams);
    sftEndOfYear:      Result := FunctEndOfYear(AParams);
    sftBeginOfMonth:   Result := FunctBeginOfMonth(AParams);
    sftEndOfMonth:     Result := FunctEndOfMonth(AParams);
    sftBeginOfQuarter: Result := FunctBeginOfQuarter(AParams);
    sftEndOfQuarter:   Result := FunctEndOfQuarter(AParams);
    sftQuarter:        Result := FunctQuarter(AParams);
    sftQuarterBeginDate: Result := FunctQuarterBeginDate(AParams);
    sftQuarterEndDate:   Result := FunctQuarterEndDate(AParams);
    sftRound:          Result := FunctRound(AParams);
    sftRoundFin:       Result := FunctRoundFin(AParams);
    sftTrunc:          Result := FunctTrunc(AParams);
    sftSetVar:         Result := FunctSetVar(AParams);
    sftGetVar:         Result := FunctGetVar(AParams);
    sftStrLength:      Result := FunctStrLength(AParams);
    sftStrBegin:       Result := FunctStrBegin(AParams);
    sftStrEnd:         Result := FunctStrEnd(AParams);
    sftSubStrPos:      Result := FunctSubStrPos(AParams);
  end;
end;


procedure TsbVirtualMachine.CheckParams(const AFunctCode: TsbKeyWord;
  const AParams: array of variant; const AProperNumb: Byte);
begin
  if Length(AParams) <> AProperNumb then
    ShowSBError(sbeVMFunctParamsMismatch, [sqlKeyWords.FindKeyWordByCode(AFunctCode)]);
end;


function TsbVirtualMachine.FunctLower(const AParams: array of variant): Variant;
begin
  CheckParams(kwLower, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
    Result := AnsiLowerCase(AParams[0]);
end;


function TsbVirtualMachine.FunctUpper(const AParams: array of variant): Variant;
begin
  CheckParams(kwUpper, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
    Result := AnsiUpperCase(AParams[0]);
end;


function TsbVirtualMachine.FunctCast(const AParams: array of variant): Variant;
var
  vt, t, s: Word;
begin
  if Length(AParams) > 1 then
  begin
    vt := AParams[1];
    if Length(AParams) = 3 then
      s := AParams[2]
    else
      if (vt = varString) or (vt = varStrArg) then
        s := 255
      else
        s := 0;

    if (vt = varString) or (vt = varStrArg) then
      t := varString
    else
      t := vt;
  end
  else
  begin
    CheckParams(kwCast, AParams, 3);
    Exit;
  end;

  if not FDefExprType and VarIsNull(AParams[0]) then
  begin
    Result := Null;
    Exit;
  end;

  if t = varDate then
    Result := VarAsType(CheckDate(AParams[0]), t)
  else
  begin
    if FDefExprType then
      try
        Result := VarAsType(AParams[0], t);
      except
        case t of
          varInteger:  Result := VarAsType(Integer(Trunc(Random(10000))), varInteger);
          varByte:     Result := VarAsType(Byte(Trunc(Random(255))), varByte);
          varDate:     Result := VarAsType(Date+Trunc(Random(365)), varDate);
          varDouble,
          varSingle:   Result := VarAsType(Random(10000),varDouble);
          varCurrency: Result := VarAsType(Random(10000), varCurrency);
          varBoolean:  Result := True;
        else
          Result := Unassigned;
        end;
      end
    else
      Result := VarAsType(AParams[0], t);

    if t = varString then
    begin
      Result := Copy(Result, 1, s);
      if (vt = varStrArg) or FDefExprType then
      begin
        s := s - Length(Result);
        if s > 0 then
          Result := Result + StringOfChar(' ', s);
      end
      else
        Result := TrimRight(Result);
    end;
  end;
end;


function TsbVirtualMachine.FunctSubst(const AParams: array of variant): Variant;
begin
  CheckParams(kwSubst, AParams, 3);
  if FDefExprType then
    Result := DefineTypeResult([AParams[0], AParams[2]])

  else
    if AParams[0] = AParams[1] then
      Result := AParams[2]
    else
      Result := AParams[0];
end;


function TsbVirtualMachine.FunctAbs(const AParams: array of variant): Variant;
begin
  CheckParams(kwAbs, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
    Result := Abs(AParams[0]);
end;


function TsbVirtualMachine.FunctSubStr(const AParams: array of variant): Variant;
begin
  CheckParams(kwSubStr, AParams, 3);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
  begin
    Result := Copy(AParams[0], Integer(AParams[1]), Integer(AParams[2]));
    if FDefExprType and (Length(Result) = 0) then
      Result := AParams[0];
  end;
end;


function TsbVirtualMachine.FunctTrimLeft(const AParams: array of variant): Variant;
begin
  CheckParams(kwTrimLeft, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
  begin
    Result := TrimLeft(AParams[0]);
    if FDefExprType and (Length(Result) = 0) then
      Result := AParams[0];
  end;
end;


function TsbVirtualMachine.FunctTrimRight(const AParams: array of variant): Variant;
begin
  CheckParams(kwTrimRight, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
  begin
    Result := TrimRight(AParams[0]);
    if FDefExprType and (Length(Result) = 0) then
      Result := AParams[0];
  end;
end;


function TsbVirtualMachine.FunctTrim(const AParams: array of variant): Variant;
begin
  CheckParams(kwTrim, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
  begin
    Result := Trim(AParams[0]);
    if FDefExprType and (Length(Result) = 0) then
      Result := AParams[0];
  end;
end;


function TsbVirtualMachine.FunctFormat(const AParams: array of variant): Variant;
begin
  CheckParams(kwFormat, AParams, 2);

  if VarIsNull(AParams[1]) then
    TVarData(Result).VType := varNull
  else
  begin
    if (Length(AParams[0]) = 0) then
      Result := AParams[1]
    else
    begin
      if FDefExprType then
      begin
        case VarType(AParams[1]) of
            varSmallint,
            varInteger,
            varSingle,
            varDouble,
            varCurrency: Result := AParams[0];
            varDate:     Result := FormatDateTime(AParams[0], Now);
        else
          Result := AParams[1];
        end;
      end
      else
        case VarType(AParams[1]) of
            varSmallint,
            varInteger,
            varSingle,
            varDouble,
            varCurrency: Result := FormatFloat(AParams[0], AParams[1]);
            varDate:     Result := FormatDateTime(AParams[0], CheckDate(AParams[1]));
        else
          Result := AParams[1];
        end;
    end;
  end;
end;


function TsbVirtualMachine.FunctDecodeDate(const AParams: array of variant; const AType: Char): Variant;
var
  D, M, Y: Word;
begin
  CheckParams(kwDay, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
  begin
    DecodeDate(CheckDate(AParams[0]), Y, M, D);
    case AType of
      'D': Result := Integer(D);
      'M': Result := Integer(M);
      'Y': Result := Integer(Y);
    else
      Result := 0;
    end;
  end;
end;


function TsbVirtualMachine.VariantsAreSame(const A, B: Variant): Boolean;
var
  LA, LB: TVarData;
begin
  LA := FindVarData(A)^;
  LB := FindVarData(B)^;
  if LA.VType = varEmpty then
    Result := LB.VType = varEmpty
  else if LA.VType = varNull then
    Result := LB.VType = varNull
  else if LB.VType in [varEmpty, varNull] then
    Result := False
  else if (LB.VType = varOleStr) or (LB.VType = varString) or
          (LA.VType = varOleStr) or (LA.VType = varString) then
    Result := AnsiSameStr(VarAsType(A, varString), VarAsType(B, varString))
  else
    Result := A = B;
end;

procedure TsbVirtualMachine.CheckDate(var Val1, Val2: Variant);
begin
  if VarType(Val1) = varDate then
  begin
    if VarType(Val2) = varString then
      if AnsiUpperCase(Val2) = 'NOW' then
        Val2 := FNow
      else if AnsiUpperCase(Val2) = 'TODAY' then
        Val2 := FToday
      else
        Val2 := VarAsType(Val2, varDate);
  end

  else if VarType(Val2) = varDate then
  begin
    if VarType(Val1) = varString then
      if AnsiUpperCase(Val1) = 'NOW' then
        Val1 := FNow
      else if AnsiUpperCase(Val1) = 'TODAY' then
        Val1 := FToday
      else
        Val1 := VarAsType(Val1, varDate);
  end;
end;


function TsbVirtualMachine.CheckDate(const Val: Variant): Variant;
begin
  if VarType(Val) = varString then
  begin
    if AnsiUpperCase(Val) = 'NOW' then
      Result := FNow
    else if AnsiUpperCase(Val) = 'TODAY' then
      Result := FToday
    else
      Result := Val;
  end
  else
    Result := Val;
end;



function TsbVirtualMachine.FunctBeginOfMonth(const AParams: array of variant): Variant;
var
  D, M, Y: Word;
begin
  CheckParams(kwBeginOfMonth, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
  begin
    DecodeDate(CheckDate(AParams[0]), Y, M, D);
    Result := EncodeDate(Y, M, 1);
  end;
end;

function TsbVirtualMachine.FunctBeginOfQuarter(const AParams: array of variant): Variant;
var
  D, M, Y: Word;
begin
  CheckParams(kwBeginOfQuarter, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
  begin
    DecodeDate(CheckDate(AParams[0]), Y, M, D);
    M := ((M - 1) div 3) * 3 + 1;
    Result := EncodeDate(Y, M, 1);
  end;
end;

function TsbVirtualMachine.FunctBeginOfYear(const AParams: array of variant): Variant;
var
  D, M, Y: Word;
begin
  CheckParams(kwBeginOfYear, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
  begin
    DecodeDate(CheckDate(AParams[0]), Y, M, D);
    Result := EncodeDate(Y, 1, 1);
  end;
end;

function TsbVirtualMachine.FunctEndOfMonth(const AParams: array of variant): Variant;
var
  D, M, Y: Word;
begin
  CheckParams(kwEndOfMonth, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
  begin
    DecodeDate(CheckDate(AParams[0]), Y, M, D);
    Inc(M);
    if M = 13 then
    begin
      M := 1;
      Inc(Y);
    end;
    Result := EncodeDate(Y, M, 1);
    Result := Result - 1;
  end;
end;

function TsbVirtualMachine.FunctEndOfQuarter(const AParams: array of variant): Variant;
var
  D, M, Y: Word;
begin
  CheckParams(kwEndOfQuarter, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
  begin
    DecodeDate(CheckDate(AParams[0]), Y, M, D);
    M := ((M - 1) div 3) * 3 + 4;
    if M > 12 then
    begin
      M := 1;
      Inc(Y);
    end;
    Result := EncodeDate(Y, M, 1);
    Result := Result - 1;
  end;
end;


function TsbVirtualMachine.FunctEndOfYear(const AParams: array of variant): Variant;
var
  D, M, Y: Word;
begin
  CheckParams(kwEndOfYear, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
  begin
    DecodeDate(CheckDate(AParams[0]), Y, M, D);
    Result := EncodeDate(Y, 12, 31);
  end;
end;

function TsbVirtualMachine.FunctQuarter(const AParams: array of variant): Variant;
var
  D, M, Y: Word;
begin
  CheckParams(kwQuarter, AParams, 1);
  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
  begin
    DecodeDate(CheckDate(AParams[0]), Y, M, D);
    Result := Integer((M + 2) div 3);
  end;
end;

function TsbVirtualMachine.FunctQuarterBeginDate(const AParams: array of variant): Variant;
begin
  CheckParams(kwQuarterBeginDate, AParams, 2);
  if FDefExprType then
    Result := EncodeDate(1972, 6, 8)
  else
    Result := EncodeDate(Integer(AParams[1]), Integer(AParams[0])*3-2, 1);

end;

function TsbVirtualMachine.FunctQuarterEndDate(const AParams: array of variant): Variant;
var
  D: TDateTime;
begin
  CheckParams(kwQuarterEndDate, AParams, 2);
  if FDefExprType then
    Result := EncodeDate(1972, 6, 8)
  else
  begin
    D := EncodeDate(Integer(AParams[1]), Integer(AParams[0])*3, 15);
    Result := FunctEndOfMonth([D]);
  end;
end;


function TsbVirtualMachine.FunctRound(const AParams: array of variant): Variant;
begin
  CheckParams(kwRound, AParams, 2);

  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
    Result := RoundTo(AParams[0], - AParams[1]);
end;


function TsbVirtualMachine.FunctRoundFin(const AParams: array of variant): Variant;
begin
  CheckParams(kwRoundFin, AParams, 2);

  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull
  else
    Result := RoundTo(AParams[0] + 1 / MaxLongInt, - AParams[1]);
end;


function TsbVirtualMachine.FunctTrunc(const AParams: array of variant): Variant;
begin
  CheckParams(kwTrunc, AParams, 1);

  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull

  else
    Result := Integer(Trunc(AParams[0]));
end;



function TsbVirtualMachine.FunctCheckIf(const AParams: array of variant; AExpression: TsbExpression): Variant;
var
  V1, V2: Variant;
  Op: String;
  fl: Boolean;
begin
  CheckParams(kwCheckIf, AParams, 5);

  if FDefExprType then
  begin
    CalcExpression(AExpression);
    V1 := FRegA;
    CalcExpression(AExpression);
    V2 := FRegA;
    Result := DefineTypeResult([V1, V2]);
  end

  else
  begin
    V1 := AParams[0];
    Op := AParams[1];
    V2 := AParams[2];

    fl := False;

    if VarIsNull(V1) or VarIsNull(V2) then
    begin
      if AnsiSameStr(Op, '=') and VarIsNull(V1) and VarIsNull(V2) then
        fl := True
      else if AnsiSameStr(Op, '<>') and
          (VarIsNull(V1) and not VarIsNull(V2) or VarIsNull(V2) and not VarIsNull(V1)) then
        fl := True;
    end

    else
      if AnsiSameStr(Op, '=') and (V1 = V2) then
        fl := True
      else if AnsiSameStr(Op, '>') and (V1 > V2) then
        fl := True
      else if AnsiSameStr(Op, '<') and (V1 < V2) then
        fl := True
      else if AnsiSameStr(Op, '<>') and (V1 <> V2) then
        fl := True
      else if AnsiSameStr(Op, '>=') and (V1 >= V2) then
        fl := True
      else if AnsiSameStr(Op, '<=') and (V1 <= V2) then
        fl := True;

    if fl then
      CalcExpression(AExpression)
    else
    begin
      AExpression.FPosition := AParams[3];
      CalcExpression(AExpression);
    end;
    AExpression.FPosition := AParams[4];
    Result := FRegA;
  end;
end;



function TsbVirtualMachine.FunctGetVar(const AParams: array of variant): Variant;
var
  i: Integer;
begin
  CheckParams(kwSetVar, AParams, 1);
  i := FVariables.IndexOf(AParams[0]);
  if i = -1 then
    i := FVariables.AddVar(AParams[0]);
  Result := FVariables.GetVar(i).Value;
end;


function TsbVirtualMachine.FunctSetVar(const AParams: array of variant): Variant;
var
  i: Integer;
begin
  CheckParams(kwSetVar, AParams, 2);
  Result := AParams[1];

  i := FVariables.IndexOf(AParams[0]);
  if i = -1 then
    i := FVariables.AddVar(AParams[0]);
  FVariables.GetVar(i).Value := Result;
end;


function TsbVirtualMachine.FunctStrLength(const AParams: array of variant): Variant;
begin
  CheckParams(kwStrLength, AParams, 1);

  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull

  else
    Result := Integer(Length(String(AParams[0])));
end;


function TsbVirtualMachine.FunctStrBegin(const AParams: array of variant): Variant;
begin
  CheckParams(kwStrBegin, AParams, 2);

  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull

  else
    Result := String(Copy(String(AParams[0]), 1, Integer(AParams[1])));
end;


function TsbVirtualMachine.FunctStrEnd(const AParams: array of variant): Variant;
var
  i: Integer;
begin
  CheckParams(kwStrEnd, AParams, 2);

  if VarIsNull(AParams[0]) then
    TVarData(Result).VType := varNull

  else
  begin
    i := Length(String(AParams[0])) - Integer(AParams[1]) + 1;
    if i < 1 then
      i := 1;
    Result := String(Copy(String(AParams[0]), i, Integer(AParams[1])));
  end;
end;


function TsbVirtualMachine.FunctSubStrPos(const AParams: array of variant): Variant;
begin
  CheckParams(kwSubStrPos, AParams, 2);

  if VarIsNull(AParams[0]) or VarIsNull(AParams[1]) then
    TVarData(Result).VType := varNull

  else
    Result := Pos(AParams[1], AParams[0]);
end;


function TsbVirtualMachine.DefineTypeResult(const AParams: array of Variant): Variant;
var
  t1, t2, i: Integer;
begin
  Result := AParams[0];

  for i := Low(AParams) + 1 to High(AParams) do
  begin
    t1 := VarType(Result);
    t2 := VarType(AParams[i]);

    if t1 <> t2 then
    begin
      if (t1 = varNull) and (t2 <> varNull) then
        Result := AParams[i]

      else if (t1 = varInteger) and (t2 in [varDouble, varCurrency]) then
        Result := AParams[i]

      else if (t1 = varCurrency) and (t2 = varDouble) then
        Result := AParams[i]

      else if (t1 <> varString) and (t2 = varString) then
        Result := AParams[i]

      else if (t1 = varString) or
              (t1 <> varNull) and (t2 = varNull) or
              (t1 in [varDouble, varCurrency]) and (t2 = varInteger) or
              (t1 = varDouble) and (t2 = varCurrency) then
        
      else
        Result := VarAsType(Result, varString);
    end

    else if (t1 = varString) and (Length(Result) < Length(AParams[i])) then
      Result := AParams[i];
  end;
end;




{ TsbSQLParser }

constructor TsbSQLParser.Create;
begin
  FSQLInfoList := TsbSQLInfoList.Create;
  FSQLExecInfo := nil;
  FParams := TsbParams.Create;
  FResultCursor := nil;
  FExternalResulCursor := False;
  FPrepared := False;
  FDoNotOptimize := False;
end;


destructor TsbSQLParser.Destroy;
begin
  if Assigned(FResultCursor) and not FExternalResulCursor then
    FreeAndNil(FResultCursor);

  FreeAndNil(FSQLInfoList);
  FreeAndNil(FSQLExecInfo);
  FreeAndNil(FParams);

  inherited;
end;


procedure TsbSQLParser.Prepare;
begin
  if FPrepared then
    ClearPreparedInfo;

  InitializationSbVM;
  try
    FStatus.sText := FSQL + sqlKeyWords.FindKeyWordByCode(kwFinish);
    ScannerChangePosition(FStatus, 1);

    ScannerGetNextToken(FStatus);
    ScannerPutBackToken(FStatus);

    case FStatus.sKeyWord of
      kwSelect: ParseSELECTStatement;
      kwCreate: ParseCREATE;
      kwDrop:   ParseDROP;
      kwInsert: ParseINSERT;
      kwUpdate: ParseUPDATE;
      kwDelete: ParseDELETE;
    else
      ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);
    end;

    ScannerGetNextToken(FStatus);
    ScannerCheckEnd(FStatus);
  finally
    DeInitializationSbVM;
    FPrepared := True;
  end;
end;


procedure TsbSQLParser.ParseSELECT;
var
  F: TsbSelectField;
  i: Integer;
  h: string;
  T: TsbTableItem;

  procedure AddAllFieldsForTable(ATable: TsbTableItem);
  var
    j, k: Integer;
  begin
    for j := 0 to ATable.FTable.Fields.Count - 1 do
    begin
      F := CurrSQLInfo.FSelect.Add;
      k := F.FExpression.Add(smoMOVA);
      F.FExpression.Items[k].FField := ATable.FTable.Fields[j];
      F.FExpression.Items[k].FTable := ATable;
      F.FExpression.FAlias := '';
    end;
  end;

begin
  ScannerCheckNextToken(FStatus, kwSelect);
  ScannerGetNextToken(FStatus);
  if FStatus.sKeyWord = kwDistinct then
     CurrSQLInfo.FSelect.FDistinct := True
  else
  begin
    CurrSQLInfo.FSelect.FDistinct := False;
    if FStatus.sKeyWord <> kwAll then
      ScannerPutBackToken(FStatus);
  end;

  ScannerGetNextToken(FStatus);
  if FStatus.sKeyWord = kwMul then  //*
  begin
    for i := 0 to CurrSQLInfo.FFrom.ItemCount - 1 do
      AddAllFieldsForTable(CurrSQLInfo.FFrom[i]);
    Exit;
  end
  else
    ScannerPutBackToken(FStatus);

  repeat
    ScannerGetNextToken(FStatus);

    if (FStatus.sTokenType = stUnknown) and (FStatus.sToken[Length(FStatus.sToken)] = '.') then  //tbl.*
    begin
      i := FStatus.sPos;
      h := FStatus.sToken;
      ScannerGetNextToken(FStatus);
      if FStatus.sKeyWord = kwMul then
      begin
        Delete(h, Length(h), 1);
        T := CurrSQLInfo.FFrom.FindTable(h);
        if not Assigned(T) then
          ShowParserError(FStatus, sbeTableNotFound, [h]);
        AddAllFieldsForTable(T);
        ScannerGetNextToken(FStatus);
        continue;
      end
      else
        ScannerChangePosition(FStatus, i);
    end;

    F := CurrSQLInfo.FSelect.Add;
    ParseExpression(F.FExpression);
    if FStatus.sTokenType = stUnknown then
    begin
      F.FExpression.FAlias := FStatus.sToken;
      ScannerGetNextToken(FStatus);
    end
    else
      F.FExpression.FAlias := '';
  until (FStatus.sKeyWord <> kwComma);

  ScannerPutBackToken(FStatus);
end;


procedure TsbSQLParser.ParseFROM;
var
  Tbl: TsbTableItem;
  i: Integer;

  procedure TblPart;
  begin
    Tbl := ParseTable;
    ScannerGetNextToken(FStatus);
    if FStatus.sKeyWord in [kwLeft, kwRight, kwFull, kwInner, kwJoin] then
    begin
      ScannerPutBackToken(FStatus);
      ParseJoins(Tbl);
    end
    else
      ScannerPutBackToken(FStatus);
  end;

begin
  ScannerCheckNextToken(FStatus, kwFrom);

  repeat
    i := FStatus.sPos;
    ScannerGetNextToken(FStatus);
    if FStatus.sKeyWord = kwLBracket then
    begin
      ScannerGetNextToken(FStatus);
      if FStatus.sKeyWord = kwSelect then
      begin
        ScannerChangePosition(FStatus, i);
        ScannerGetNextToken(FStatus);
        TblPart;
      end
      else
      begin
        Tbl := ParseTable;
        ParseJoins(Tbl);
        ScannerCheckNextToken(FStatus, kwRBracket);
      end;
    end
    else
      TblPart;

    ScannerGetNextToken(FStatus);

  until (FStatus.sKeyWord <> kwComma);

  ScannerPutBackToken(FStatus);
end;


function TsbSQLParser.GetEmbeddedSelect: String;
var
  i: Integer;
begin
  ScannerCheckNextToken(FStatus, kwLbracket);
  i := FStatus.sPos;
  ScannerPutBackToken(FStatus);
  ScannerSearchToken(FStatus, kwRbracket);
  Result := Copy(FStatus.sText, i, FStatus.sPos - i);
  ScannerCheckNextToken(FStatus, kwRbracket);
end;


function TsbSQLParser.ParseTable: TsbTableItem;
var
  n, i: Integer;
  lSQL: String;
  R: TsbResultRec;
  Expr: TsbExpression;
  T: TsbTableCursor;
  P: TsbSQLParser;
begin
  if not (FStatus.sTokenType in [stUnknown, stQuotedText]) then
  begin
    if (FStatus.sKeyWord = kwLBracket) and not FUpdatePart then
    begin
      ScannerPutBackToken(FStatus);
      n := FStatus.sPos;
      lSQL := GetEmbeddedSelect;                 //Recursion to itself!
      try
        if Assigned(FCustomParsingEndProc) then
        begin
          P := ParseCustomSQL(lSQL, Params, FOpenTableFunct, FCustomParsingEndProc, FDoNotOptimize);
          try
            T := FOpenTableFunct('');
            P.CreateResultTableStructure(T.Fields);
            n := CurrSQLInfo.FFrom.Add(T);
            CurrSQLInfo.FFrom[n].FPhysicalName := ' ';
          finally
            P.Free;
          end;
        end
        else
        begin
          R := sbOpenQuery(lSQL, FParams);
          n := CurrSQLInfo.FFrom.Add(R.Cursor);
          CurrSQLInfo.FFrom[n].FPhysicalName := ' ';
        end
      except
        on E: ESBParser do
        begin
          ScannerChangePosition(FStatus, n);
          ShowParserPosError(FStatus, E);
          raise;
        end
        else
          raise;
      end;
    end
    else
    begin
      n := -1;
      ShowParserError(FStatus, sbeTableExp, [FStatus.sToken]);
    end;
  end
  else
  begin
    n := CurrSQLInfo.FFrom.Add(FStatus.sToken);

    if Assigned(CurrSQLInfo.FFrom[n].FTable.AuxParams) and (CurrSQLInfo.FFrom[n].FTable.AuxParams.Count > 0) then
    begin
      ScannerCheckNextToken(FStatus, kwLbracket);
      i := 0;
      repeat
        ScannerGetNextToken(FStatus);
        if i = CurrSQLInfo.FFrom[n].FTable.AuxParams.Count then
          ShowParserError(FStatus, sbeTableParMismatch, []);
        Expr := TsbExpression.Create;
        CurrSQLInfo.FFrom[n].FTable.AuxParams[i] := Expr;
        Inc(i);
        ParseExpression(Expr);
      until FStatus.sKeyWord <> kwComma;

      if i <> CurrSQLInfo.FFrom[n].FTable.AuxParams.Count then
        ShowParserError(FStatus, sbeTableParMismatch, []);

      ScannerCheckToken(FStatus, kwRbracket);
    end;

    if Assigned(FTableParamsProc) then
      FTableParamsProc(CurrSQLInfo.FFrom[n].FTable, sbmRead);
  end;

  CurrSQLInfo.FFrom[n].FAlreadyJoined := False;
  ScannerGetNextToken(FStatus);
  if FStatus.sTokenType = stUnknown then
  begin
    if Assigned(CurrSQLInfo.FFrom.FindTable(FStatus.sToken)) then
      ShowParserError(FStatus, sbeInvalidAlias, [FStatus.sToken]);
    CurrSQLInfo.FFrom[n].Alias := FStatus.sToken;
  end
  else if FStatus.sTokenType <> stKeyword then
    ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken])
  else
    ScannerPutBackToken(FStatus);
  Result := CurrSQLInfo.FFrom[n];
end;


function TsbSQLParser.ParseJoins(ALeftTable: TsbTableItem): TsbTableItem;
var
  JT: TsbJoinType;
  i: Integer;
begin
  ScannerGetNextToken(FStatus);
  JT := sjtInnerJoin;
  if FStatus.sKeyWord in [kwLeft] then
  begin
    JT := sjtLeftJoin;
    ScannerGetNextToken(FStatus);
    if FStatus.sKeyWord = kwOuter then
      ScannerGetNextToken(FStatus);
  end
  else if FStatus.sKeyWord in [kwRight] then
  begin
    JT := sjtRightJoin;
    ScannerGetNextToken(FStatus);
    if FStatus.sKeyWord = kwOuter then
      ScannerGetNextToken(FStatus);
  end
  else if FStatus.sKeyWord in [kwFull] then
    JT := sjtFullJoin
  else if FStatus.sKeyWord = kwInner then
    ScannerGetNextToken(FStatus);

  ScannerCheckToken(FStatus, kwJoin);

  i := FStatus.sPos;
  ScannerGetNextToken(FStatus);
  if FStatus.sKeyWord = kwLBracket then
  begin
    ScannerGetNextToken(FStatus);
    if FStatus.sKeyWord = kwSelect then
    begin
      ScannerChangePosition(FStatus, i);
      ScannerGetNextToken(FStatus);
      Result := ParseTable;
    end
    else
    begin
      Result := ParseTable;
      ParseJoins(Result);
      ScannerCheckNextToken(FStatus, kwRBracket);
    end;
  end
  else
    Result := ParseTable;

  ALeftTable.FJoins.Add(Result, JT);
  ALeftTable.FManualJoined := True;
  Result.FManualJoined := True;

  ScannerCheckNextToken(FStatus, kwON);

  Result.FParentJoinInfo.FJoinCondition := TsbConditionNode.Create(nil);
  Result.FParentJoinInfo.FParentTable := ALeftTable;
  ScannerGetNextToken(FStatus);
  ParseLogElement(Result.FParentJoinInfo.FJoinCondition);
  ScannerPutBackToken(FStatus);

  DeleteEmptyNodes(Result.FParentJoinInfo.FJoinCondition);
  Normalization(Result.FParentJoinInfo.FJoinCondition);
  PackConditions(Result.FParentJoinInfo.FJoinCondition);
  AnalyseConditions(Result.FParentJoinInfo.FJoinCondition);
end;


procedure TsbSQLParser.ParseWHERE;
begin
  ScannerCheckNextToken(FStatus, kwWhere);
  CurrSQLInfo.FWhere := TsbConditionNode.Create(nil);
  ParseLogConditions(CurrSQLInfo.FWhere);
end;


procedure TsbSQLParser.ParseGROUPBY;
var
  j: Integer;
  h: String;
  T: TsbTableItem;
  F: TsbField;
  fl: Boolean;
  SF: TsbSelectField;
begin
  ScannerCheckNextToken(FStatus, kwGROUP);
  ScannerCheckNextToken(FStatus, kwBY);

  CurrSQLInfo.FGroup := TsbGroupByList.Create;
  repeat
    ScannerGetNextToken(FStatus);

    if FStatus.sTokenType = stUnknown then
    begin
      j := Pos('.', FStatus.sToken);
      if (j <> 0) then
      begin
        h := Copy(FStatus.sToken, 1, j-1);
        T := CurrSQLInfo.FFrom.FindTable(h);
        if not Assigned(T) then
          ShowParserError(FStatus, sbeTableNotFound, [h]);
        h := Copy(FStatus.sToken, j+1, Length(FStatus.sToken)-j);
        F := T.FTable.Fields.FindField(h);
      end
      else
        F := CurrSQLInfo.FFrom.FindField(FStatus.sToken, T);

      if not Assigned(F) then
        ShowParserError(FStatus, sbeFieldNotFound, [FStatus.sToken]);

      fl := False;
      for j := 0 to CurrSQLInfo.FSelect.ItemCount - 1 do
        if (CurrSQLInfo.FSelect[j].FExpression.FType = setVector) and
           (CurrSQLInfo.FSelect[j].FExpression[0].FField = F) then
        begin
          CurrSQLInfo.FGroup.Add(j);
          fl :=True;
          break;
        end;

      if not fl then
      begin
        SF := CurrSQLInfo.FSelect.Add;
        j := SF.FExpression.Add(smoMOVA);
        SF.FExpression.Items[j]^.FField := F;
        SF.FExpression.Items[j]^.FTable := T;
        CurrSQLInfo.FGroup.Add(CurrSQLInfo.FSelect.ItemCount-1);
        SF.FHidden := True;
      end;
    end

    else if FStatus.sTokenType = stNumber then
    begin
      try
        j := StrToInt(FStatus.sToken);
      except
        j := 0;
      end;

      if (j <= 0) or (j > CurrSQLInfo.FSelect.ItemCount) then
        ShowParserError(FStatus, sbeInvalidClause, ['GROUP BY']);

      CurrSQLInfo.FGroup.Add(j-1);
    end

    else
      ShowParserError(FStatus, sbeInvalidClause, ['GROUP BY']);

    ScannerGetNextToken(FStatus);
  until FStatus.sKeyWord <> kwComma;

  for j := 0 to CurrSQLInfo.FSelect.ItemCount - 1 do
  begin
    if CurrSQLInfo.FSelect[j].FExpression.FType = setScalar then
      if CurrSQLInfo.FGroup.IndexOf(Pointer(j)) = -1 then
        CurrSQLInfo.FGroup.Add(j);
  end;

  ScannerPutBackToken(FStatus);
end;


procedure TsbSQLParser.ParseHAVING;
begin
  ScannerCheckNextToken(FStatus, kwHaving);
  CurrSQLInfo.FHaving := TsbConditionNode.Create(nil);
  ParseLogConditions(CurrSQLInfo.FHaving);
end;


procedure TsbSQLParser.ParseUNION;
begin
  ScannerCheckNextToken(FStatus, kwUnion);
  if ScannerNextTokenIs(FStatus, kwALL) then
  begin
    ScannerCheckNextToken(FStatus, kwALL);
    FSQLInfoList.FDistinct := False;
  end;
end;


procedure TsbSQLParser.ParseORDERBY;
var
  j, n: Integer;
  h: String;
  T: TsbTableItem;
  F: TsbField;
begin
  ScannerCheckNextToken(FStatus, kwORDER);
  ScannerCheckNextToken(FStatus, kwBY);

  CurrSQLInfo.FOrder := TsbOrderByList.Create;
  repeat
    n := -1;
    ScannerGetNextToken(FStatus);
    if FStatus.sTokenType = stUnknown then
    begin
      j := Pos('.', FStatus.sToken);
      if (j <> 0) then
      begin
        h := Copy(FStatus.sToken, 1, j-1);
        T := CurrSQLInfo.FFrom.FindTable(h);
        if not Assigned(T) then
          ShowParserError(FStatus, sbeTableNotFound, [h]);
        h := Copy(FStatus.sToken, j+1, Length(FStatus.sToken)-j);
        F := T.FTable.Fields.FindField(h);
      end
      else
        F := CurrSQLInfo.FFrom.FindField(FStatus.sToken, T);
      if not Assigned(F) then
        ShowParserError(FStatus, sbeFieldNotFound, [FStatus.sToken]);

      n := -1;
      for j := 0 to CurrSQLInfo.FSelect.ItemCount - 1 do
        if  not CurrSQLInfo.FSelect[j].FHidden and
           (CurrSQLInfo.FSelect[j].FExpression.FType = setVector) and
           (CurrSQLInfo.FSelect[j].FExpression[0].FField = F) then
        begin
          n := CurrSQLInfo.FOrder.Add(j, False);
          break;
        end;

      if n = -1 then
        ShowParserError(FStatus, sbeInvalidClause, ['ORDER BY']);
    end

    else if FStatus.sTokenType = stNumber then
    begin
      try
        j := StrToInt(FStatus.sToken);
      except
        j := 0;
      end;

      if (j <= 0) or (j > CurrSQLInfo.FSelect.ItemCount) or CurrSQLInfo.FSelect[j-1].FHidden then
        ShowParserError(FStatus, sbeInvalidClause, ['ORDER BY']);

      n := CurrSQLInfo.FOrder.Add(j-1, False);
    end

    else
      ShowParserError(FStatus, sbeInvalidClause, ['ORDER BY']);


    ScannerGetNextToken(FStatus);
    if FStatus.sKeyWord = kwDesc then
      CurrSQLInfo.FOrder[n].FDesc := True

    else if FStatus.sKeyWord = kwAsc then

    else
      ScannerPutBackToken(FStatus);

    ScannerGetNextToken(FStatus);
  until FStatus.sKeyWord <> kwComma;

  ScannerPutBackToken(FStatus);
end;


procedure TsbSQLParser.ParseCREATE;
var
  h: string;
  t: TsbFieldType;
  s: Word;
begin
  if not Assigned(FSQLExecInfo) then
    FSQLExecInfo := TsbSQLExecInfo.Create;

  FSelectPart := False;
  FHavingPart := False;
  FUpdatePart := False;

  ScannerCheckNextToken(FStatus, kwCreate);
  ScannerCheckNextToken(FStatus, kwTable);
  ScannerGetNextToken(FStatus);
  if not (FStatus.sTokenType in [stUnknown, stQuotedText])  then
    ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);

  FSQLExecInfo.FType := sesCreate;
  FSQLExecInfo.FTableName := FStatus.sToken;
  FSQLExecInfo.FFieldList := TsbFields.Create;

  ScannerCheckNextToken(FStatus, kwLbracket);
  repeat
    ScannerGetNextToken(FStatus);
    if FStatus.sTokenType <> stUnknown then
      ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);
    h := FStatus.sToken;
    s := 0;

    ScannerGetNextToken(FStatus);
    case FStatus.sKeyWord of
     kwInteger: t := sbfInteger;
     kwAutoInc: t := sbfAutoInc;
     kwFloat:   t := sbfFloat;
     kwDate:    t := sbfDateTime;
     kwBlob:    t := sbfBlob;
     kwCurrency:t := sbfCurrency;
     kwBoolean: t := sbfBoolean;

     kwString:  begin
                  t := sbfString;
                  ScannerGetNextToken(FStatus);
                  if FStatus.sKeyWord = kwLBracket then
                  begin
                    ScannerGetNextToken(FStatus);
                    if FStatus.sTokenType <> stNumber then
                      ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);
                    s := StrToInt(FStatus.sToken);
                    if not ((s > 0) and (s <= 255)) then
                      ShowParserError(FStatus, sbeInvalidFieldSize, []);
                    ScannerCheckNextToken(FStatus, kwRbracket);
                  end
                  else
                    ScannerPutBackToken(FStatus);
                end;
    else
      t := sbfUnknown;
      ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);
    end;

    if Assigned(FSQLExecInfo.FFieldList.FindField(h)) then
      ShowParserError(FStatus, sbeFieldExists, [h]);

    FSQLExecInfo.FFieldList.CreateField(h, t, s);
    ScannerGetNextToken(FStatus);
  until FStatus.sKeyWord <> kwComma;
  ScannerCheckToken(FStatus, kwRBracket);
end;


procedure TsbSQLParser.ParseDROP;
begin
  if not Assigned(FSQLExecInfo) then
    FSQLExecInfo := TsbSQLExecInfo.Create;

  FSelectPart := False;
  FHavingPart := False;
  FUpdatePart := False;
    
  ScannerCheckNextToken(FStatus, kwDrop);
  ScannerCheckNextToken(FStatus, kwTable);
  ScannerGetNextToken(FStatus);
  if not (FStatus.sTokenType in [stUnknown, stQuotedText])  then
    ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);

  FSQLExecInfo.FType := sesDrop;
  FSQLExecInfo.FTableName := FStatus.sToken;
end;


procedure TsbSQLParser.ParseINSERT;
var
  F: TsbField;
  i, j: Integer;
begin
  if not Assigned(FSQLExecInfo) then
    FSQLExecInfo := TsbSQLExecInfo.Create;

  FSelectPart := False;
  FHavingPart := False;
  FUpdatePart := False;

  ScannerCheckNextToken(FStatus, kwInsert);
  ScannerCheckNextToken(FStatus, kwInto);
  ScannerGetNextToken(FStatus);
  if not (FStatus.sTokenType in [stUnknown, stQuotedText])  then
    ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);

  FSQLExecInfo.FType := sesInsert;
  FSQLExecInfo.FTableName := FStatus.sToken;

  if Assigned(FOpenTableFunct) then
  begin
    FSQLExecInfo.FTable := FOpenTableFunct(FSQLExecInfo.FTableName);
    if Assigned(FTableParamsProc) then
      FTableParamsProc(FSQLExecInfo.FTable, sbmReadWrite);
  end
  else
    FSQLExecInfo.FTable := sbOpenTable(FSQLExecInfo.FTableName, nil, sbmReadWrite);

  FSQLExecInfo.FUpdFieldList := TsbUpdFieldList.Create;

  ScannerCheckNextToken(FStatus, kwLbracket);
  repeat
    ScannerGetNextToken(FStatus);
    if FStatus.sTokenType <> stUnknown then
      ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);

    F := FSQLExecInfo.FTable.Fields.FindField(FStatus.sToken);
    if not Assigned(F) then
      ShowParserError(FStatus, sbeFieldNotFound, [FStatus.sToken]);

    FSQLExecInfo.FUpdFieldList.Add(F);

    ScannerGetNextToken(FStatus);
  until FStatus.sKeyWord <> kwComma;
  ScannerCheckToken(FStatus, kwRBracket);

  ScannerGetNextToken(FStatus);

  if FStatus.sKeyWord = kwValues then
  begin
    i := 0;
    ScannerCheckNextToken(FStatus, kwLbracket);
    repeat
     if i = FSQLExecInfo.FUpdFieldList.ItemCount then
       ShowParserError(FStatus, sbeColCountMismatch, []);
     ScannerGetNextToken(FStatus);
     ParseExpression(FSQLExecInfo.FUpdFieldList[i].FRightPart);
     Inc(i);
    until FStatus.sKeyWord <> kwComma;

   if i <> FSQLExecInfo.FUpdFieldList.ItemCount then
     ShowParserError(FStatus, sbeColCountMismatch, []);

    ScannerCheckToken(FStatus, kwRBracket);
  end

  else if FStatus.sKeyWord = kwSelect then
  begin
    ScannerPutBackToken(FStatus);
    ParseSELECTStatement;
    if Assigned(FResultCursor) then
    begin
      if (FResultCursor.Fields.Count <> FSQLExecInfo.FUpdFieldList.ItemCount) then
        ShowParserError(FStatus, sbeColCountMismatch, []);

      for i := 0 to FSQLExecInfo.FUpdFieldList.ItemCount - 1 do
      begin
        j := FSQLExecInfo.FUpdFieldList[i].FRightPart.Add(smoMOVA);
        FSQLExecInfo.FUpdFieldList[i].FRightPart[j].FField := FResultCursor.Fields[i];
      end;
    end;
  end

  else
    ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);
end;


procedure TsbSQLParser.ParseUPDATE;
var
  F: TsbField;
  i: Integer;
  T: TsbTableItem;
begin
  if not Assigned(FSQLExecInfo) then
    FSQLExecInfo := TsbSQLExecInfo.Create;

  if not Assigned(FSQLInfoList) then
    FSQLInfoList := TsbSQLInfoList.Create;

  FSelectPart := False;
  FHavingPart := False;
  FUpdatePart := False;

  ScannerCheckNextToken(FStatus, kwUpdate);
  ScannerGetNextToken(FStatus);
  FUpdatePart := True;
  T := ParseTable;
  FUpdatePart := False;

  FSQLExecInfo.FType := sesUpdate;

  if Assigned(FCustomParsingEndProc) then
  begin
    if Assigned(FOpenTableFunct) then
    begin
      FSQLExecInfo.FTable := FOpenTableFunct(T.FTable.TableName);
      if Assigned(FTableParamsProc) then
        FTableParamsProc(FSQLExecInfo.FTable, sbmReadWrite);
    end
  end
  else
    FSQLExecInfo.FTable := sbOpenTable(T.FTable.TableName, nil, sbmReadWrite);

  FSQLExecInfo.FUpdFieldList := TsbUpdFieldList.Create;

  ScannerCheckNextToken(FStatus, kwSet);

  i := 0;
  repeat
    ScannerGetNextToken(FStatus);
    if FStatus.sTokenType <> stUnknown then
      ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);

    F := FSQLExecInfo.FTable.Fields.FindField(FStatus.sToken);
    if not Assigned(F) then
      ShowParserError(FStatus, sbeFieldNotFound, [FStatus.sToken]);
    FSQLExecInfo.FUpdFieldList.Add(F);

    ScannerCheckNextToken(FStatus, kwEqual);

    ScannerGetNextToken(FStatus);
    ParseExpression(FSQLExecInfo.FUpdFieldList[i].FRightPart);
    Inc(i);
  until FStatus.sKeyWord <> kwComma;
  ScannerPutBackToken(FStatus);

  if ScannerNextTokenIs(FStatus, kwWHERE) then
  begin
    ParseWHERE;
    OptimisationCondition(CurrSQLInfo.FWhere);
    CleanUpWhere;
  end;

  if Assigned(CurrSQLInfo.FWhere) then
    SortConditions(CurrSQLInfo.FWhere);
end;


procedure TsbSQLParser.ParseDELETE;
var
  T: TsbTableItem;
begin
  if not Assigned(FSQLExecInfo) then
    FSQLExecInfo := TsbSQLExecInfo.Create;

  if not Assigned(FSQLInfoList) then
    FSQLInfoList := TsbSQLInfoList.Create;

  FSelectPart := False;
  FHavingPart := False;
  FUpdatePart := False;

  ScannerCheckNextToken(FStatus, kwDelete);
  ScannerCheckNextToken(FStatus, kwFrom);
  ScannerGetNextToken(FStatus);
  FUpdatePart := True;
  T := ParseTable;
  FUpdatePart := False;

  FSQLExecInfo.FType := sesDelete;

  if Assigned(FCustomParsingEndProc) then
  begin
    if Assigned(FOpenTableFunct) then
    begin
      FSQLExecInfo.FTable := FOpenTableFunct(T.FTable.TableName);
      if Assigned(FTableParamsProc) then
        FTableParamsProc(FSQLExecInfo.FTable, sbmReadWrite);
    end
  end  
  else
    FSQLExecInfo.FTable := sbOpenTable(T.FTable.TableName, nil, sbmReadWrite);

  if ScannerNextTokenIs(FStatus, kwWHERE) then
  begin
    ParseWHERE;
    OptimisationCondition(CurrSQLInfo.FWhere);
    CleanUpWhere;
  end;

  if Assigned(CurrSQLInfo.FWhere) then
    SortConditions(CurrSQLInfo.FWhere);
end;


procedure TsbSQLParser.ParseLogConditions(ACondition: TsbConditionNode);
begin
  ScannerGetNextToken(FStatus);
  ParseLogElement(ACondition);
  ScannerPutBackToken(FStatus);
end;


  (* <log_element> => {<log_term>[ OR <log_term>]} *)

function TsbSQLParser.ParseLogElement(ACondition: TsbConditionNode): TsbConditionNode;
var
  i: Integer;
begin
  i := ACondition.AddNode;
  Result := TsbConditionNode(ACondition.Items[i]);
  Result.FType := sbnOR;
  Result.FNot :=  False;

  ParseLogTerm(Result);

  while FStatus.sKeyWord = kwOR do
  begin
    ScannerGetNextToken(FStatus);
    ParseLogTerm(Result);
  end;

  if Result.ItemCount = 0 then
    ACondition.Delete(i);
end;


  (* <log_term>  => { <condition> [AND <condition>]} *)

procedure TsbSQLParser.ParseLogTerm(ACondition: TsbConditionNode);
var
  i: Integer;
  Cond: TsbConditionNode;
begin
  i := ACondition.AddNode;
  Cond := TsbConditionNode(ACondition.Items[i]);
  Cond.FType := sbnAND;
  ParseCondition(Cond);

  while FStatus.sKeyWord = kwAND do
  begin
    ScannerGetNextToken(FStatus);
    ParseCondition(Cond);
  end;

  if Cond.ItemCount = 0 then
    ACondition.Delete(i);
end;


  (* <condition> => {[NOT] [(] {<log_element> | <compare_expression>} [)] *)

procedure TsbSQLParser.ParseCondition(ACondition: TsbConditionNode);
var
  fl_not: Boolean;
  Cond: TsbConditionNode;
begin
  fl_not := False;
  while FStatus.sKeyWord = kwNot do
  begin
    fl_not := not fl_not;
    ScannerGetNextToken(FStatus);
  end;

  if (FStatus.sKeyWord = kwLbracket) then
  begin
    ScannerGetNextToken(FStatus);
    Cond := ParseLogElement(ACondition);
    if fl_not and Assigned(Cond) then
      Cond.FNot := not Cond.FNot;
    ScannerPutBackToken(FStatus);
    ScannerCheckNextToken(FStatus, kwRbracket);
    ScannerGetNextToken(FStatus);
  end
  else
    ParseCompareExpression(ACondition, fl_not);
end;


  (* <compare_expression> => {[NOT] <expression> {{<|>|=|<>|>=|<=|+=} <expression> |(<Select_statement>)} |
                                           {IN (<list_of_expressions>|<Select_statement>)} |
                                           {BETWEEN <expression> AND <expression>} |
                                           {LIKE <expression>} |
                                           {IS [NOT] NULL}}|
                                    {EXISTS (<Select_statement>)}
   *)

procedure TsbSQLParser.ParseCompareExpression(ACondition: TsbConditionNode; ANot: Boolean);
var
  i: Integer;
  CondItem: TsbConditionItem;
  h: TsbKeyWord;
  lBegPos: Integer;
  lNotBegPos: Integer;

  procedure EmbeddedSQL;
  var
    n: Integer;
  begin
    n := FStatus.sPos;
    CondItem.FRightExpression.FSQL := TsbEmbeddedSQL.Create;
    CondItem.FRightExpression.FSQL.FSQLText := GetEmbeddedSelect;
    CondItem.FRightExpression.FSQL.FParams := FParams;
    try
      CondItem.FRightExpression.FSQL.Prepare(CurrSQLInfo.FFrom);
    except
      on E: ESBParser do
      begin
        ScannerChangePosition(FStatus, n);
        ShowParserPosError(FStatus, E);
        raise;
      end
      else
        raise;
    end;
  end;

begin
  ScannerPutBackToken(FStatus);
  lBegPos := FStatus.sPos;
  lNotBegPos := -1;
  ScannerGetNextToken(FStatus);

  try
    i := ACondition.AddItem;
    CondItem := TsbConditionItem(ACondition.Items[i]);
    CondItem.FNot := ANot;

    if FStatus.sKeyWord = kwExists then
    begin
      CondItem.CreateExpression('R');
      EmbeddedSQL;
      CondItem.FOperation := scoExists;
      ScannerGetNextToken(FStatus);
      Exit;
    end;

    CondItem.CreateExpression('L');
    ParseExpression(CondItem.FLeftExpression);

    if (FStatus.sKeyWord = kwNot) then
    begin
      ScannerPutBackToken(FStatus);
      lNotBegPos := FStatus.sPos;
      ScannerGetNextToken(FStatus);
      CondItem.FNot := not CondItem.FNot;
      ScannerGetNextToken(FStatus);
      if not (FStatus.sKeyWord in [kwIn, kwBetween, kwLike]) then
        ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);
    end;

    case FStatus.sKeyWord of
      kwEqual,
      kwLeftJoin:CondItem.FOperation := scoEqual;

      kwLess:    CondItem.FOperation := scoLess;
      kwMore:    CondItem.FOperation := scoMore;
      kwMeq:     CondItem.FOperation := scoMoreOrEqual;
      kwLeq:     CondItem.FOperation := scoLessOrEqual;
      kwNeq:     CondItem.FOperation := scoNotEqual;
      kwIn:      CondItem.FOperation := scoIn;
      kwBetween: CondItem.FOperation := scoBetween;
      kwIs:      CondItem.FOperation := scoIsNull;
      kwLike:    CondItem.FOperation := scoLike;
    else
      ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);
    end;

    h := FStatus.sKeyWord;
    CondItem.CreateExpression('R');

    case h of
      kwLeftJoin:  CondItem.FRightExpression.FOuter := True;
    else
      CondItem.FLeftExpression.FOuter := False;
      CondItem.FRightExpression.FOuter := False;
    end;

    //In
    if CondItem.FOperation = scoIn then
    begin
      i := FStatus.sPos;
      ScannerCheckNextToken(FStatus, kwLbracket);
      ScannerGetNextToken(FStatus);
      if FStatus.sKeyWord = kwSelect then
      begin
        ScannerChangePosition(FStatus, i);
        ScannerGetNextToken(FStatus);
        ScannerPutBackToken(FStatus);
        EmbeddedSQL;
      end

      else
      begin
        ScannerPutBackToken(FStatus);
        repeat
          ScannerGetNextToken(FStatus);
          ParseExpression(CondItem.FRightExpression);
          CondItem.FRightExpression.Add(smoNone);
        until FStatus.sKeyWord <> kwComma;
        ScannerPutBackToken(FStatus);
        ScannerCheckNextToken(FStatus, kwRbracket);
      end;

      ScannerGetNextToken(FStatus);
    end

    //Between
    else if CondItem.FOperation = scoBetween then
    begin
      ScannerGetNextToken(FStatus);
      ParseExpression(CondItem.FRightExpression);
      ScannerPutBackToken(FStatus);
      ScannerCheckNextToken(FStatus, kwAND);
      CondItem.FRightExpression.Add(smoNone);
      ScannerGetNextToken(FStatus);
      ParseExpression(CondItem.FRightExpression);
    end

    //Like
    else if CondItem.FOperation = scoLike then
    begin
      ScannerGetNextToken(FStatus);
      ParseExpression(CondItem.FRightExpression);
    end

    //Is null
    else if CondItem.FOperation = scoIsNull then
    begin
      ScannerGetNextToken(FStatus);
      if (FStatus.sKeyWord = kwNot) then
      begin
        ScannerPutBackToken(FStatus);
        lNotBegPos := FStatus.sPos;
        ScannerGetNextToken(FStatus);
        CondItem.FNot := not CondItem.FNot;
      end
      else
        ScannerPutBackToken(FStatus);
      ScannerCheckNextToken(FStatus, kwNull);
      ScannerGetNextToken(FStatus);
    end

    else
    begin
      i := FStatus.sPos;
      ScannerGetNextToken(FStatus);
      if FStatus.sKeyWord = kwLBracket then
      begin
        ScannerGetNextToken(FStatus);
        if FStatus.sKeyWord = kwSelect then
        begin
          ScannerChangePosition(FStatus, i);
          ScannerGetNextToken(FStatus);
          ScannerPutBackToken(FStatus);
          EmbeddedSQL;
          Exit;
        end
        else
        begin
          ScannerChangePosition(FStatus, i);
          ScannerGetNextToken(FStatus);
        end;
      end;

      ParseExpression(CondItem.FRightExpression);
    end;

  finally
    ScannerPutBackToken(FStatus);
    CondItem.FStrExpression := Copy(FStatus.sText, lBegPos, FStatus.sPos - lBegPos);
    if lNotBegPos <> -1 then
      Delete(CondItem.FStrExpression, lNotBegPos - lBegPos + 1, 3);
    ScannerGetNextToken(FStatus);
  end;
end;


  (* <expression> => {<term>[+<term>][-<term>]} *)

procedure TsbSQLParser.ParseExpression(AExpression: TsbExpression);
var
  lKeyWord: TsbKeyWord;
begin
  ParseTerm(AExpression);
  lKeyWord := FStatus.sKeyWord;

  while (lKeyWord in [kwPlus, kwMinus, kwConcat]) do
  begin
    AExpression.Add(smoPushA);
    ScannerGetNextToken(FStatus);
    ParseTerm(AExpression);
    AExpression.Add(smoPopB);
    AExpression.Add(smoSwapAB);
    if lKeyWord = kwPlus then
      AExpression.Add(smoAdd)

    else if lKeyWord = kwConcat then
      AExpression.Add(smoConcat)

    else
      AExpression.Add(smoSubstract);
    lKeyWord := FStatus.sKeyWord;
  end;
end;


  (* <term>  => {<factor> {*|/} <factor>} *)

procedure TsbSQLParser.ParseTerm(AExpression: TsbExpression);
var
  lKeyWord: TsbKeyWord;
begin
  ParseUnaryMinus(AExpression);
  lKeyWord := FStatus.sKeyWord;

  while (lKeyWord in [kwMul, kwDiv]) do
  begin
    AExpression.Add(smoPushA);
    ScannerGetNextToken(FStatus);
    ParseUnaryMinus(AExpression);
    AExpression.Add(smoPopB);
    if lKeyWord = kwMul then
      AExpression.Add(smoMultiply)
    else
    begin
      AExpression.Add(smoSwapAB);
      AExpression.Add(smoDivide);
    end;
    lKeyWord := FStatus.sKeyWord;
  end;
end;


  (* Unary - or +  { {+|-} <factor> } *)

procedure TsbSQLParser.ParseUnaryMinus(AExpression: TsbExpression);
var
  lKeyWord: TsbKeyWord;
begin
  lKeyWord := FStatus.sKeyWord;

  if (lKeyWord in [kwMinus, kwPlus]) then
    ScannerGetNextToken(FStatus);

  ParseBrackets(AExpression);

  if lKeyWord = kwMinus then
    AExpression.Add(smoReverseSignA);
end;


  (* {[(] <expression> [)]} *)

procedure TsbSQLParser.ParseBrackets(AExpression: TsbExpression);
begin
  if (FStatus.sKeyWord = kwLbracket) then
  begin
    ScannerGetNextToken(FStatus);
    ParseExpression(AExpression);
    ScannerPutBackToken(FStatus);
    ScannerCheckNextToken(FStatus, kwRbracket);
    ScannerGetNextToken(FStatus);
  end
  else
    ParseArray(AExpression);
end;


  (* {[<expression>, <expression> ...]} *)

procedure TsbSQLParser.ParseArray(AExpression: TsbExpression);
var
  i, ArrItemsCount: Integer;
begin
  if (FStatus.sKeyWord = kwLSquareBracket) then
  begin
    ArrItemsCount := 0;
    i := AExpression.Add(smoDefineArray);
    ScannerGetNextToken(FStatus);

    while FStatus.sKeyWord <> kwRSquareBracket do
    begin
      ParseExpression(AExpression);
      ScannerPutBackToken(FStatus);
      Inc(ArrItemsCount);
      AExpression.Add(smoNone); // delimiter for array item expression

      ScannerGetNextToken(FStatus);
      if FStatus.sKeyWord <> kwComma then
        break;

      ScannerGetNextToken(FStatus);
    end;
    AExpression[i].FConstValue := ArrItemsCount;
    ScannerCheckToken(FStatus, kwRSquareBracket);
    ScannerGetNextToken(FStatus);
  end
  else
    ParseFactor(AExpression);
end;


  (* <factor> => { <const>|<field>|<parameter> } *)

procedure TsbSQLParser.ParseFactor(AExpression: TsbExpression);
var
  i, j, pc: Integer;
  T: TsbTableItem;
  h: String;
  Par: TsbParam;
  Cnst: TsbCustomSQLConstant;
  fl: Boolean;
  kw, kw2: TsbKeyWord;
  kwt: TsbTypeKeyWord;
begin
  i := AExpression.Add(smoMOVA);
  case FStatus.sTokenType of
    stKeyWord:
      if FStatus.sKeyWord = kwNull then
        TVarData(AExpression.Items[i].FConstValue).VType := varNull

      else if not (FSelectPart or FHavingPart) and (FStatus.sKeyWordType <> tkwUnaryFunct) or
             (FSelectPart or FHavingPart) and (FStatus.sKeyWordType = tkwNone) then
        ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken])

      else
      begin
        kwt := FStatus.sKeyWordType;
        kw := FStatus.sKeyWord;
        ScannerCheckNextToken(FStatus, kwLbracket);
        if kwt = tkwUnaryFunct then
        begin
          j := i;
          pc := 0;
          AExpression.Items[j].FFunction := TsbFunctionType(Ord(kw) - Ord(kwUpper) + 1);
          if AExpression.Items[j].FFunction = sftCast then
          begin
            ScannerGetNextToken(FStatus);
            ParseExpression(AExpression);
            AExpression.Add(smoNone);
            Inc(pc);
            ScannerCheckToken(FStatus, kwAs);
            ScannerGetNextToken(FStatus);
            i := AExpression.Add(smoMOVA);
            case FStatus.sKeyWord of
              kwInteger: AExpression[i].FConstValue := varInteger;

              kwFloat:   AExpression[i].FConstValue := varDouble;

              kwString,
              kwVarchar,
              kwChar:    begin
                           kw2 := FStatus.sKeyWord;
                           if kw2 = kwChar then
                             AExpression[i].FConstValue := varStrArg
                           else
                             AExpression[i].FConstValue := varString;

                           ScannerGetNextToken(FStatus);
                           if FStatus.sKeyWord = kwLBracket then
                           begin
                             AExpression.Add(smoNone);
                             Inc(pc);
                             ScannerGetNextToken(FStatus);
                             ParseExpression(AExpression);
                             ScannerCheckToken(FStatus, kwRbracket);
                           end
                           else
                           begin
                             if kw2 = kwChar then
                             begin
                               AExpression.Add(smoNone);
                               Inc(pc);
                               i := AExpression.Add(smoMOVA);
                               AExpression.Items[i].FConstValue := 1;
                             end;
                             ScannerPutBackToken(FStatus);
                           end;
                         end;

              kwDate:    AExpression[i].FConstValue := varDate;

              kwCurrency:AExpression[i].FConstValue := varCurrency;

              kwBoolean: AExpression[i].FConstValue := varBoolean;
            else
              ShowParserError(FStatus, sbeTypeNotSupported, [FStatus.sToken]);
            end;
            AExpression.Add(smoNone);
            Inc(pc);
            ScannerGetNextToken(FStatus);
          end

          else
          begin
            if AExpression.Items[j].FFunction = sftCheckIf then  //exception of rules!
            begin
              AExpression.Add(smoMOVA); //beginning of Else condition
              AExpression.Add(smoMOVB); //end of function
              AExpression.Add(smoNone); 
            end;

            if not ScannerNextTokenIs(FStatus, kwRbracket) then
              repeat
                ScannerGetNextToken(FStatus);

                if (AExpression.Items[j].FFunction = sftCheckIf) and (pc = 4) then //exception of rules!
                  AExpression.Items[j + 1].FConstValue := AExpression.ItemCount;

                ParseExpression(AExpression);
                AExpression.Add(smoNone);

                if (AExpression.Items[j].FFunction = sftCheckIf) and (pc = 4) then //exception of rules!
                  AExpression.Items[j + 2].FConstValue := AExpression.ItemCount;

                Inc(pc);
              until FStatus.sKeyWord <> kwComma;
          end;

          AExpression.Add(smoNone);
          AExpression.Items[j].FConstValue := pc; // update parameters count
        end

        else
        begin
          ScannerGetNextToken(FStatus);
          if FStatus.sKeyWord = kwDistinct then
            fl := True
          else
          begin
            fl := False;
            if FStatus.sKeyWord <> kwAll then
              ScannerPutBackToken(FStatus);
          end;
          ScannerGetNextToken(FStatus);

          if not Assigned(CurrSQLInfo.FAggrFunct) then
            CurrSQLInfo.FAggrFunct := TsbAggrFunctionList.Create;

          j := CurrSQLInfo.FAggrFunct.Add(TsbAggrFunctType(Ord(kw) - Ord(kwSum)), fl);
          AExpression.Items[i].FAggrFunction := CurrSQLInfo.FAggrFunct[j];

          if not fl and (CurrSQLInfo.FAggrFunct[j].FFunction = safCount) and (FStatus.sKeyWord = kwMul) then
          begin
            i := CurrSQLInfo.FAggrFunct[j].FExpression.Add(smoMOVA);
            CurrSQLInfo.FAggrFunct[j].FExpression[i].FConstValue := 0;
            ScannerGetNextToken(FStatus);
          end
          else
            ParseExpression(CurrSQLInfo.FAggrFunct[j].FExpression);
        end;

        ScannerPutBackToken(FStatus);
        ScannerCheckNextToken(FStatus, kwRbracket);
      end;


    stUnknown:
      begin
        j := Pos('.', FStatus.sToken);
        if (j <> 0) then
        begin
          h := Copy(FStatus.sToken, 1, j-1);
          T := CurrSQLInfo.FFrom.FindTable(h);
          if not Assigned(T) then
            ShowParserError(FStatus, sbeTableNotFound, [h]);
          h := Copy(FStatus.sToken, j+1, Length(FStatus.sToken)-j);
          AExpression.Items[i].FField := T.FTable.Fields.FindField(h);
          AExpression.Items[i].FTable := T;
        end
        else
        begin
          AExpression.Items[i]^.FField := CurrSQLInfo.FFrom.FindField(FStatus.sToken, T);
          AExpression.Items[i].FTable := T;
        end;

        if not Assigned(AExpression.Items[i].FField) then
          ShowParserError(FStatus, sbeFieldNotFound, [FStatus.sToken]);
      end;

    stQuotedText:
        AExpression.Items[i].FConstValue := FStatus.sToken;

    stNumber:
        if (Pos('.', FStatus.sToken) <> 0) or (Pos('E', FStatus.sToken) <> 0) then
          AExpression.Items[i].FConstValue := StrToFloat(FStatus.sToken)
        else
          AExpression.Items[i].FConstValue := StrToInt(FStatus.sToken);

    stParameter:
      begin
        Par := FParams.ParamByName(FStatus.sToken);
        if not Assigned(Par) then
          Par := FParams[FParams.Add(FStatus.sToken)];
        AExpression.Items[i].FParam := Par;
      end;

    stConst:
      begin
        if SQLConsts <> nil then
          Cnst := SQLConsts.ConstantByName(FStatus.sToken)
        else
          Cnst := nil;

        if not Assigned(Cnst) then
          ShowParserError(FStatus, sbeConstantNotFound, [FStatus.sToken])
        else
          AExpression.Items[i].FConstValue := Cnst.Value;
      end;
  else
    ShowParserError(FStatus, sbeSyntaxError, [FStatus.sToken]);
  end;

  ScannerGetNextToken(FStatus);
end;



function TsbSQLParser.DeleteEmptyNodes(var ACondition: TsbConditionNode): Boolean;
var
  P: TsbConditionNode;
  C: TObject;
  i: Integer;
begin
  Result := False;
  i := 0;
  while i <= ACondition.ItemCount - 1 do
  begin
    C := ACondition.Items[i];
    if C is TsbConditionNode then
    begin
      if not DeleteEmptyNodes(TsbConditionNode(C)) then
        Inc(i);
    end
    else
      Inc(i);
  end;

  if ACondition.ItemCount = 0 then
  begin
    P := ACondition.FParent;
    if Assigned(P) then
    begin
      P.Delete(ACondition);
      Result := True;
    end
    else
    begin
      ACondition.Free;
      CurrSQLInfo.FWhere := nil;
      Result := True;
    end;
    Exit;
  end;

  if ACondition.ItemCount = 1 then
  begin
    C := ACondition.Items[0];
    if C is TsbConditionNode then
    begin
      TsbConditionNode.Collapse(ACondition);
      ACondition := TsbConditionNode(C);
    end
    else
    begin
      TsbConditionNode.Collapse(ACondition);
      ACondition := ACondition;
    end;
  end;
end;


// Normalization of WHERE

procedure TsbSQLParser.Normalization(ACondition: TsbConditionNode);
var
  C1, C2: TObject;
  i: Integer;
begin
  if TsbConditionNode(ACondition).FNot then
  begin
    if TsbConditionNode(ACondition).FType = sbnAND then
      TsbConditionNode(ACondition).FType := sbnOR
    else
      TsbConditionNode(ACondition).FType := sbnAND;
    TsbConditionNode(ACondition).FNot := False;

    for i := 0 to TsbConditionNode(ACondition).ItemCount - 1 do
    begin
      C2 := TsbConditionNode(ACondition).Items[i];
      if C2 is TsbConditionNode then
        TsbConditionNode(C2).FNot := not TsbConditionNode(C2).FNot
      else
        TsbConditionItem(C2).FNot := not TsbConditionItem(C2).FNot;
    end;
  end;

  i := 0;
  while i <= ACondition.ItemCount - 1 do
  begin
    C1 := ACondition.Items[i];
    if C1 is TsbConditionNode then
      Normalization(TsbConditionNode(C1))
    else
      if TsbConditionItem(C1).FNot then
      begin
        TsbConditionItem(C1).FNot := False;
        case TsbConditionItem(C1).FOperation of
          scoEqual:        begin
                             TsbConditionItem(C1).FOperation := scoNotEqual;
                             TsbConditionItem(C1).FStrExpression := StringReplace(TsbConditionItem(C1).FStrExpression, '=', '<>', []);
                           end;
          scoLess:         begin
                             TsbConditionItem(C1).FOperation := scoMoreOrEqual;
                             TsbConditionItem(C1).FStrExpression := StringReplace(TsbConditionItem(C1).FStrExpression, '<', '>=', []);
                           end;
          scoMore:         begin
                             TsbConditionItem(C1).FOperation := scoLessOrEqual;
                             TsbConditionItem(C1).FStrExpression := StringReplace(TsbConditionItem(C1).FStrExpression, '>', '<=', []);
                           end;

          scoNotEqual:     begin
                             TsbConditionItem(C1).FOperation := scoEqual;
                             TsbConditionItem(C1).FStrExpression := StringReplace(TsbConditionItem(C1).FStrExpression, '<>', '=', []);
                           end;

          scoLessOrEqual: begin
                            TsbConditionItem(C1).FOperation := scoMore;
                            TsbConditionItem(C1).FStrExpression := StringReplace(TsbConditionItem(C1).FStrExpression, '<=', '>', []);
                           end;

          scoMoreOrEqual:  begin
                             TsbConditionItem(C1).FOperation := scoLess;
                             TsbConditionItem(C1).FStrExpression := StringReplace(TsbConditionItem(C1).FStrExpression, '>=', '<', []);
                           end;

        else
          TsbConditionItem(C1).FNot := True;
        end;
      end;
    Inc(i);
  end;
end;


procedure TsbSQLParser.PackConditions(ACondition: TsbConditionNode);
var
  C: TObject;
  i, j: Integer;
begin
  if not Assigned(ACondition) then
    Exit;

  i := 0;
  while i <= ACondition.ItemCount - 1 do
  begin
    C := ACondition.Items[i];
    if (C is TsbConditionNode) and ((ACondition.FType = TsbConditionNode(C).FType) or (TsbConditionNode(C).ItemCount = 1)) then
    begin
      PackConditions(TsbConditionNode(C));
      if ACondition.FNot = TsbConditionNode(C).FNot then
      begin
        j := TsbConditionNode(C).ItemCount;
        while TsbConditionNode(C).ItemCount > 0 do
          ACondition.InsertNode(i, TsbConditionNode(C).Items[0]);
        ACondition.Delete(C);
        Inc(i, j);
      end
      else
        Inc(i);
    end
    else
      Inc(i);
  end;

  if (ACondition.FType = sbnOR) and (ACondition.ItemCount = 1) then
    ACondition.FType := sbnAND;
end;


procedure TsbSQLParser.AnalyseExpression(AExpression: TsbExpression);
var
  i: Integer;
  t: TsbExpressionType;
begin
  AExpression.FJoinTable := nil;

  if Assigned(AExpression.FSQL) then
  begin
    AExpression.FType := setComplexVector;
    Exit;
  end;

  t := setScalar;
  for i := 0 to AExpression.ItemCount -1 do
  begin
    if Assigned(AExpression.Items[i].FField) then
    begin
      if Assigned(AExpression.FJoinTable) then
      begin
        if AExpression.FJoinTable <> AExpression.Items[i].FTable then
        begin
          t := setComplexVector;
          AExpression.FJoinTable := nil;
          break;
        end
      end

      else
      begin
        AExpression.FJoinTable := AExpression.Items[i].FTable;
        t := setVector;
      end
    end

    else if Assigned(AExpression.Items[i].FAggrFunction) then
      t := setComplexVector;
  end;

  AExpression.FType := t;
end;


procedure TsbSQLParser.AnalyseConditions(ACondition: TsbConditionNode);
var
  C: TObject;
  i: Integer;

  procedure AnalyseCondition(ACondition: TsbConditionItem);
  var
    te1, te2: TsbExpressionType;
  begin
    if Assigned(ACondition.FLeftExpression) then
    begin
      AnalyseExpression(ACondition.FLeftExpression);
      te1 := ACondition.FLeftExpression.FType;
    end
    else
      te1 := setComplexVector;

    if Assigned(ACondition.FRightExpression) then
    begin
      AnalyseExpression(ACondition.FRightExpression);
      te2 := ACondition.FRightExpression.FType;
    end
    else
      te2 := setComplexVector;

    if (te1 = setScalar) and (te2 = setScalar) then
      ACondition.FType := sctScalarCondition

    else if (te1 = setVector) and (te2 = setScalar) or
            (te1 = setScalar) and (te2 = setVector) then
      ACondition.FType := sctFilter

    else if (te1 = setVector) and (te2 = setVector) and
            (ACondition.FLeftExpression.ItemCount = 1) and (ACondition.FRightExpression.ItemCount = 1) then
      if ACondition.FLeftExpression.FJoinTable = ACondition.FRightExpression.FJoinTable then
        ACondition.FType := sctFilter
      else
        if ACondition.FOperation = scoEqual then
        begin
          if not ACondition.FLeftExpression.FOuter and not ACondition.FRightExpression.FOuter then
            ACondition.FType := sctJoin
          else if not ACondition.FLeftExpression.FOuter and ACondition.FRightExpression.FOuter then
           ACondition.FType := sctLeftJoin
          else if ACondition.FLeftExpression.FOuter and not ACondition.FRightExpression.FOuter then
            ACondition.FType := sctRightJoin
          else
           ACondition.FType := sctFullJoin;
        end
        else
          ACondition.FType := sctVectorCondition
    else
      ACondition.FType := sctVectorCondition;
  end;

begin
  for i := 0 to ACondition.ItemCount - 1 do
  begin
    C := ACondition.Items[i];
    if C is TsbConditionNode then
      AnalyseConditions(TsbConditionNode(C))
    else
      AnalyseCondition(TsbConditionItem(C));
  end;
end;


procedure TsbSQLParser.SortConditions(ACondition: TsbConditionNode);
var
  C1, C2: TObject;
  i, j: Integer;
  t1, t2: Integer;
begin
  for i := 0 to ACondition.ItemCount - 2 do
  begin
    C1 := ACondition.Items[i];
    if C1 is TsbConditionNode then
    begin
      SortConditions(TsbConditionNode(C1));
      t1 := Ord(High(TsbConditionType))+1;
    end
    else
      t1 := Ord(TsbConditionItem(C1).FType);

    for j := i+1 to ACondition.ItemCount - 1 do
    begin
      C2 := ACondition.Items[j];
      if C2 is TsbConditionNode then
        t2 := Ord(High(TsbConditionType))+1
      else
        t2 := Ord(TsbConditionItem(C2).FType);
      if t1 > t2 then
      begin
        ACondition.Exchange(i, j);
        C1 := ACondition.Items[i];
        t1 := Ord(TsbConditionItem(C1).FType);
      end;
    end;
  end;
end;



procedure TsbSQLParser.Open(AResultCursor: TsbTableCursor = nil);
var
  i, k: Integer;
  h: string;
  aFld: array of string;
  F: TsbFields;
  FldRecNo: TsbField;
  V: Variant;
  ResGrp, ResDist: TsbTableCursor;
  AF: TsbAggrFunction;
  SF: TsbSelectField;

  procedure CreateOrderFields(TmpBuf: TsbTableCursor);
  var
    i, n: Integer;
  begin
    n := CurrSQLInfo.FOrder.ItemCount - 1;
    for i := 0 to n do
    begin
      aFld[i] := TmpBuf.Fields[CurrSQLInfo.FOrder[i].FSelIndex].FieldName;
      if CurrSQLInfo.FOrder[i].FDesc then
        aFld[i] := aFld[i] + ' DESC';
    end;
  end;


  procedure SyncTablesPos(const APos: Integer);
  var
    j, n: Integer;
  begin
    if APos <> -1 then
    begin
      CurrSQLInfo.FResult.RecNo := APos;
      n := CurrSQLInfo.FFrom.ItemCount - 1;
      for j := 0 to n do
        with CurrSQLInfo.FFrom[j] do
        begin
          FTable.AuxInternalRecordPos := CurrSQLInfo.FResult.Fields[FResultFieldRef];
          FTable.OuterPosition := False;
        end;
    end

    else
    begin
      n := CurrSQLInfo.FFrom.ItemCount - 1;
      for j := 0 to n do
        CurrSQLInfo.FFrom[j].FTable.OuterPosition := True;
    end;
  end;


  //This is function-wraper!
  //There is Delphi compiler ERROR which produces memory leak
  procedure BugFixAssignField(const Fld1, Fld2: TsbField);
  begin
    Fld1.Value := Fld2.Value;
  end;


  procedure AddGrpRecToResDist;
  var
    j, n: Integer;
    SF: TsbSelectField;
  begin
    if Assigned(CurrSQLInfo.FAggrFunct) then
      CurrSQLInfo.FAggrFunct.MakeFinalResult;

    if not CheckConditions(CurrSQLInfo.FHaving) then
      Exit;

    ResDist.Append;
    n := ResDist.Fields.Count - 1;
    for j := 0 to n do
    begin
      SF := CurrSQLInfo.FSelect[j];
      if SF.FAggrFunctsExist then
      begin
        if VarIsNull(FldRecNo.Value) then
          SyncTablesPos(-1)
        else
          SyncTablesPos(FldRecNo.Value);
        SF.FExpression.FPosition := 0;
        FVM.CalcExpression(SF.FExpression);
        ResDist.Fields[j].Value := FVM.FRegA;
      end
      else
        BugFixAssignField(ResDist.Fields[j], ResGrp.Fields[j]);
    end;
    ResDist.Post;
  end;


  procedure AppendRecToResGrp(ARecNo: Integer);
  var
    j, n: Integer;
    SF: TsbSelectField;
  begin
    n := CurrSQLInfo.FSelect.ItemCount - 1;
    ResGrp.Append;
    for j := 0 to n do
    begin
      SF := CurrSQLInfo.FSelect[j];
      if not SF.FAggrFunctsExist then
      begin
        SF.FExpression.FPosition := 0;
        FVM.CalcExpression(SF.FExpression);
        ResGrp.Fields[j].Value := FVM.FRegA;
      end;
    end;

    if Assigned(FldRecNo) then
      FldRecNo.Value := ARecNo;
    ResGrp.Post;
  end;


  procedure CloseTempBuffer(var TmpBuf: TsbTableCursor);
  begin
    if Assigned(TmpBuf) and (TmpBuf <> FResultCursor) then
    begin
      sbClose(TmpBuf);
      TmpBuf := nil;
    end;
  end;


  procedure DistinctRes(TmpBuf: TsbTableCursor);
  var
    i, j, k: Integer;
    fl: Boolean;
  begin
    if Assigned(CurrSQLInfo.FOrder) then
      SetLength(aFld, TmpBuf.Fields.Count + CurrSQLInfo.FOrder.ItemCount)
    else
      SetLength(aFld, TmpBuf.Fields.Count);

    if Assigned(CurrSQLInfo.FOrder) then
    begin
      CreateOrderFields(TmpBuf);
      k := CurrSQLInfo.FOrder.ItemCount;
    end
    else
      k := 0;

    for i := 0 to TmpBuf.Fields.Count - 1 do
    begin
      fl := True;
      if Assigned(CurrSQLInfo.FOrder) then
        for j := 0 to CurrSQLInfo.FOrder.ItemCount - 1 do
          if CurrSQLInfo.FOrder.Items[j].FSelIndex = i then
          begin
            fl := False;
            break;
          end;

      if fl then
      begin
        aFld[k] := TmpBuf.Fields[i].FieldName;
        Inc(k);
      end;
    end;

    if Length(aFld) <> k then
      SetLength(aFld, k);
    h:= sbGetUniqueFileName(SbTempDir, '~indDistinct', sbIndexExt);
    TmpBuf.CreateIndex(h, aFld, [isbUnique]);
    SetLength(aFld, 0);
    TmpBuf.SetIndex(h, True);
  end;

begin
  InitializationSbVM;
  try
    if not FPrepared then
    begin
      if Assigned(FResultCursor) and (FResultCursor <> AResultCursor) then
        FResultCursor.Free;
      FResultCursor := AResultCursor;
      FExternalResulCursor := Assigned(FResultCursor);
      Prepare;
    end
    else
      FResultCursor.Clear;

    if Assigned(FSQLExecInfo) then
      Exit;


    FSQLInfoList.FCurrentSQLInfo := 0;
    repeat  //Loop by UNIONS
      if not CurrSQLInfo.FAlwaysEmptyResult then
        CurrSQLInfo.ImplementJoins;

      ActivateIndexes(False);

      ResGrp  := FResultCursor;
      ResDist := FResultCursor;

      if Assigned(CurrSQLInfo.FAggrFunct) or Assigned(CurrSQLInfo.FGroup) then
      begin
        F := TsbFields.Create;
        try
          F.Assign(FResultCursor.Fields);
          for i := 0 to CurrSQLInfo.FSelect.ItemCount - 1 do
          begin
            SF := CurrSQLInfo.FSelect[i];
            if SF.FHidden then
              F.CreateField(GetUniqFieldName('~GroupField', F),
                SF.FExpression[0].FField.FieldType,
                SF.FExpression[0].FField.Size);
          end;

          F.CreateField('~TempRecordNumber', sbfInteger);
          h := sbGetUniqueFileName(SbTempDir, '~tmpGrRes', sbTableExt);
          sbCreateTable(h, F);
          ResGrp := sbOpenTable(h, nil, sbmReadWrite, True);
        finally
          F.Free;
        end;
        FldRecNo := ResGrp.Fields[ResGrp.Fields.Count-1];
      end
      else
        FldRecNo := nil;


      if FSQLInfoList.IsUnion and CurrSQLInfo.FSelect.FDistinct then
      begin
        h := sbGetUniqueFileName(SbTempDir, '~tmpDistRes', sbTableExt);
        sbCreateTable(h, FResultCursor.Fields);
        ResDist := sbOpenTable(h, nil, sbmReadWrite, True);
        if ResGrp = FResultCursor then
          ResGrp := ResDist;
      end;

      //filling result table
      if Assigned(CurrSQLInfo.FResult) then
      begin
        k := CurrSQLInfo.FFrom.ItemCount - 1;
        for i := 0 to k do
          CurrSQLInfo.FFrom[i].FResultFieldRef := CurrSQLInfo.FResult.IndexOfField(CurrSQLInfo.FFrom[i].Alias);

        FVM.FIgnoreOuterMet := True;
        for i := 1 to CurrSQLInfo.FResult.RecordCount do
        begin
          SyncTablesPos(i);

          if CheckConditions(CurrSQLInfo.FWhere) then
            AppendRecToResGrp(i);
        end;

        //Group By
        if Assigned(CurrSQLInfo.FAggrFunct) or Assigned(CurrSQLInfo.FGroup) then
        begin
          if Assigned(CurrSQLInfo.FGroup) then
          begin
            SetLength(aFld, CurrSQLInfo.FGroup.ItemCount);
            for i := 0 to CurrSQLInfo.FGroup.ItemCount - 1 do
              aFld[i] := ResGrp.Fields[CurrSQLInfo.FGroup[i]].FieldName;

            h:= sbGetUniqueFileName(SbTempDir, '~indGroup', sbIndexExt);
            ResGrp.CreateIndex(h, aFld, []);
            SetLength(aFld, 0);
            ResGrp.SetIndex(h, True);
          end
          else
            ResGrp.SetIndex('');

          if Assigned(CurrSQLInfo.FAggrFunct) then
            CurrSQLInfo.FAggrFunct.PrepareForCalc;

          ResGrp.First;
          while not ResGrp.Eof do
          begin
            if ResGrp.IsKeyValueChanged and not ResGrp.BOF then
            begin
              ResGrp.Prior;
              AddGrpRecToResDist;
              if Assigned(CurrSQLInfo.FAggrFunct) then
                CurrSQLInfo.FAggrFunct.PrepareForCalc;
              ResGrp.Next;
            end;

            SyncTablesPos(FldRecNo.Value);
            if Assigned(CurrSQLInfo.FAggrFunct) then
            begin
              for i := 0 to CurrSQLInfo.FAggrFunct.ItemCount - 1 do
              begin
                AF := CurrSQLInfo.FAggrFunct[i];
                AF.FExpression.FPosition := 0;
                FVM.CalcExpression(AF.FExpression);
                V := FVM.FRegA;

                if AF.Distinct then
                  if not AF.FTempTable.Add(V) then
                    Continue;

                if not VarIsNull(V) then
                begin
                  case AF.FFunction of
                    safSum:   Inc(AF.FCurrentValue, V);

                    safAvg:   begin
                                Inc(AF.FCurrentValue, V);
                                Inc(AF.FSecCurrentValue);
                              end;

                    safMin:   if AF.FNull or (AF.FCurrentValue > V) then
                                AF.FCurrentValue := V;

                    safMax:   if AF.FNull or (AF.FCurrentValue < V) then
                                AF.FCurrentValue := V;

                    safCount:  Inc(AF.FCurrentValue);
                  end;
                  AF.FNull := False;
                end;
              end;
            end;

            ResGrp.Next;
          end;

          if ResGrp.RecordCount = 0 then
          begin
            SyncTablesPos(-1);
            AppendRecToResGrp(-1);
            if Assigned(CurrSQLInfo.FAggrFunct) then
              AddGrpRecToResDist;
          end
          else
            AddGrpRecToResDist;

          CloseTempBuffer(ResGrp);
        end;

        FreeAndNil(CurrSQLInfo.FResult);

        if CurrSQLInfo.FSelect.FDistinct then   //Distinct and OrderBy
        begin
          DistinctRes(ResDist);

          if FSQLInfoList.IsUnion then         //Add sub-result into final result table
          begin
            ResDist.First;
            k := ResDist.Fields.Count - 1;
            while not ResDist.EOF do
            begin
              FResultCursor.Append;
              for i := 0 to k do
                BugFixAssignField(FResultCursor.Fields[i], ResDist.Fields[i]);
              FResultCursor.Post;
              ResDist.Next;
            end;
          end;

          CloseTempBuffer(ResDist);
        end;
      end;

      CloseTempBuffer(ResGrp);
      CloseTempBuffer(ResDist);

      ActivateIndexes(True);      //?????

      Inc(FSQLInfoList.FCurrentSQLInfo);

    until FSQLInfoList.FCurrentSQLInfo = FSQLInfoList.ItemCount;


    FSQLInfoList.FCurrentSQLInfo := FSQLInfoList.ItemCount - 1;

    if FSQLInfoList.IsUnion and FSQLInfoList.FDistinct then //Distinction for UNION and ORDER BY
      DistinctRes(FResultCursor)

    else if Assigned(CurrSQLInfo.FOrder) and                //ORDER BY only
           ((FSQLInfoList.IsUnion and not FSQLInfoList.FDistinct) or
           (not FSQLInfoList.IsUnion and not CurrSQLInfo.FSelect.FDistinct)) then
    begin
      SetLength(aFld, CurrSQLInfo.FOrder.ItemCount);
      CreateOrderFields(FResultCursor);
      h:= sbGetUniqueFileName(SbTempDir, '~indOrderBy', sbIndexExt);
      FResultCursor.CreateIndex(h, aFld, []);
      SetLength(aFld, 0);
      FResultCursor.SetIndex(h, True);
    end;

    FResultCursor.First;

  finally
    FSQLInfoList.FCurrentSQLInfo := 0;
    CloseTempBuffer(ResGrp);
    CloseTempBuffer(ResDist);
    DeInitializationSbVM;
  end
end;



procedure TsbSQLParser.Execute;
begin
  InitializationSbVM;
  try
    if not FPrepared then
      Prepare;

    if not Assigned(FSQLExecInfo) then
      Exit;

    case FSQLExecInfo.FType of
      sesCreate: ExecCreate;
      sesDrop:   ExecDrop;
      sesInsert: ExecInsert;
      sesUpdate: ExecUpdate;
      sesDelete: ExecDelete;
    end;

  finally
    DeInitializationSbVM;
  end;
end;



procedure TsbSQLParser.JoinAnalyze;
var
  i: Integer;
  MTRecNum: Integer;
  h: Extended;

  function AnalyzeBranch(ALeftTable, ARightTable: TsbTableItem; AJoinType: TsbJoinType): Extended;
  var
    i: Integer;
    n: Real;
    sFld, fFld: TsbField;
  begin
    Result := 0;
    PackConditions(ARightTable.FParentJoinInfo.FJoinCondition);
    if Assigned(ARightTable.FParentJoinInfo.FJoinCondition) and (ARightTable.FParentJoinInfo.FJoinCondition.FType = sbnAnd) then
    begin
      i := 0;
      while i < ARightTable.FParentJoinInfo.FJoinCondition.ItemCount do
      begin
        if (ARightTable.FParentJoinInfo.FJoinCondition.Items[i] is TsbConditionItem) then
        begin
          if (TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FType = sctFilter) then
          begin

            if (TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FLeftExpression.FType = setVector) then
            begin
              if(TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FLeftExpression.FJoinTable = ARightTable) or
                (TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FRightExpression.FJoinTable = ARightTable)
              then
              begin
                ARightTable.FParentJoinInfo.FilterCondition.InsertNode(ARightTable.FParentJoinInfo.FilterCondition.ItemCount,
                  TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]));
              end
              else if (AJoinType = sjtInnerJoin) and
                      ((TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FLeftExpression.FJoinTable = ALeftTable) or
                      (TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FRightExpression.FJoinTable = ALeftTable))
              then
              begin
                ALeftTable.FParentJoinInfo.FilterCondition.InsertNode(ALeftTable.FParentJoinInfo.FilterCondition.ItemCount,
                  TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]));
              end
              else
                Inc(i);
            end
            else
              Inc(i);
          end

          else if (TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FType in [sctJoin, sctLeftJoin]) then
          begin
            if TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FLeftExpression.FJoinTable = ARightTable then
            begin
              sFld := TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FLeftExpression[0]^.FField;
              fFld := TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FRightExpression[0]^.FField;
            end
            else if TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FRightExpression.FJoinTable = ARightTable then
            begin
              sFld := TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FRightExpression[0]^.FField;
              fFld := TsbConditionItem(ARightTable.FParentJoinInfo.FJoinCondition.Items[i]).FLeftExpression[0]^.FField;
            end
            else
            begin
              sFld := nil;
              fFld := nil;
            end;

            if Assigned(sFld) and Assigned(fFld) then
            begin
              ARightTable.FParentJoinInfo.FSelfJoinFields.Add(sFld);
              ARightTable.FParentJoinInfo.FForeignJoinFields.Add(fFld);
              ARightTable.FParentJoinInfo.FJoinCondition.Delete(i);

              if not Assigned(FCustomParsingEndProc) and not ARightTable.Table.Filtered and
                 (sbFindSysIndInfo(ARightTable.Table.TableName + ':' + sFld.FieldName) <> -1) then
                n := 0
              else
                n := ARightTable.FIndexEvaluation;

              Result := Result + n + MTRecNum*ARightTable.FSearchEvaluation;
            end
            else
              Inc(i);
          end
          else
           Inc(i);
        end

        else
          Inc(i);
      end;
    end;

    PackConditions(ARightTable.FParentJoinInfo.FJoinCondition);
    if Assigned(ARightTable.FParentJoinInfo.FJoinCondition) and (ARightTable.FParentJoinInfo.FJoinCondition.ItemCount = 0) then
      FreeAndNil(ARightTable.FParentJoinInfo.FJoinCondition);

    for i := 0 to ARightTable.FJoins.ItemCount - 1 do
      Result := Result + AnalyzeBranch(ARightTable, ARightTable.FJoins[i].FRightTable, ARightTable.FJoins[i].FJoinType);
  end;

begin
  FCostEvaluation := 0;

  for i := 0 to CurrSQLInfo.FJoinTreeList.ItemCount - 1 do
  begin
    MTRecNum := CurrSQLInfo.FJoinTreeList[i].FTable.RecordCount;
    h := AnalyzeBranch(nil, CurrSQLInfo.FJoinTreeList[i], sjtInnerJoin);
    if h = 0 then
      h := MTRecNum;
    if i > 0 then
      FCostEvaluation := Round(FCostEvaluation * h)
    else
      FCostEvaluation := Round(h);
  end;
end;




procedure TsbSQLParser.CreateJoinInfo;
var
  i, j: Integer;
  GR: TsbGraph;


  procedure AddGraphToJoinTree;
  var
    i: Integer;

    procedure AddJoins(AIndex: Integer);
    var
      i: Integer;
    begin
      for i := 0 to GR.ItemCount - 1 do
        if Assigned(GR.FJoins[AIndex, i]) then
        begin
          if GR[AIndex].FJoins.JoinByTable(GR[i]) <> -1 then
            continue;
          GR[AIndex].FJoins.Add(GR[i], GR.FJoins[AIndex, i].FJoinType);
          GR[i].FParentJoinInfo.JoinCondition.InsertNode(GR[i].FParentJoinInfo.JoinCondition.ItemCount,
             GR.FJoins[AIndex, i].FJoinCondition);
          GR.FJoins[AIndex, i].FJoinCondition := nil;
          AddJoins(i);
        end;
    end;

  begin
    if GR.FFirstNode = -1 then
      Exit;
    CurrSQLInfo.FJoinTreeList.Add(GR[GR.FFirstNode]);
    AddJoins(GR.FFirstNode);

    for i := 0 to GR.ItemCount - 1 do
      GR[i].FAlreadyJoined := True;
  end;


  procedure GetJoinedTables(ATable: TsbTableItem; AIndex: Integer; var ATl, ATr: TsbTableItem; var AJT: TsbJoinType);
  begin
    ATr := nil;
    ATl := nil;
    if not ((CurrSQLInfo.FWhere.Items[AIndex] is TsbConditionItem) and
      (TsbConditionItem(CurrSQLInfo.FWhere.Items[AIndex]).FType in [sctJoin, sctLeftJoin])) then
      Exit;

    AJT := sjtInnerJoin;
    if TsbConditionItem(CurrSQLInfo.FWhere.Items[AIndex]).FType = sctJoin then
    begin
      if TsbConditionItem(CurrSQLInfo.FWhere.Items[AIndex]).FLeftExpression.FJoinTable = ATable then
        ATr := TsbConditionItem(CurrSQLInfo.FWhere.Items[AIndex]).FRightExpression.FJoinTable
      else if TsbConditionItem(CurrSQLInfo.FWhere.Items[AIndex]).FRightExpression.FJoinTable = ATable then
        ATr := TsbConditionItem(CurrSQLInfo.FWhere.Items[AIndex]).FLeftExpression.FJoinTable;

      ATl := ATable;
    end

    else if TsbConditionItem(CurrSQLInfo.FWhere.Items[AIndex]).FType = sctLeftJoin then
    begin
      AJT := sjtLeftJoin;
      if TsbConditionItem(CurrSQLInfo.FWhere.Items[AIndex]).FLeftExpression.FJoinTable = ATable then
      begin
        ATl := ATable;
        ATr := TsbConditionItem(CurrSQLInfo.FWhere.Items[AIndex]).FRightExpression.FJoinTable;
      end
      else if TsbConditionItem(CurrSQLInfo.FWhere.Items[AIndex]).FRightExpression.FJoinTable = ATable then
      begin
        ATl := TsbConditionItem(CurrSQLInfo.FWhere.Items[AIndex]).FLeftExpression.FJoinTable;
        ATr := ATable;
      end;
    end;
  end;


  procedure CreateGraphInfo(ATable: TsbTableItem);
  var
    i, k: Integer;
    Tl, Tr, Tlc, Trc, T: TsbTableItem;
    JT, JTc: TsbJoinType;
    J: TsbGraphJoin;
  begin
    if GR.IndexOf(ATable) = -1 then
      GR.AddNode(ATable);

    if not Assigned(CurrSQLInfo.FWhere) or (CurrSQLInfo.FWhere.FType = sbnOR) then
      Exit;

    for i := 0  to CurrSQLInfo.FWhere.ItemCount - 1 do
    begin
      GetJoinedTables(ATable, i, Tl, Tr, JT);
      if not Assigned(Tl) or Tl.FAlreadyJoined or not Assigned(Tr) or Tr.FAlreadyJoined or
         (CurrSQLInfo.FFrom.IndexOf(Tl) = -1) or (CurrSQLInfo.FFrom.IndexOf(Tr) = -1) then
        Continue;

      if Tl = ATable then
        T := Tr
      else
        T := Tl;

      if GR.IndexOf(T) = -1 then
         GR.AddNode(T);

      J := GR.AddJoin(Tl, Tr, JT);
      J.JoinCondition.InsertNode(J.JoinCondition.ItemCount, TsbConditionItem(CurrSQLInfo.FWhere.Items[i]), False);

      for k := i + 1 to CurrSQLInfo.FWhere.ItemCount - 1 do
      begin
        GetJoinedTables(ATable, k, Tlc, Trc, JTc);
        if not ((Tlc = Tl) and (Trc = Tr) or (Tlc = Tr) and (Trc = Tl)) then
          Continue;

        if JT <> JTc then
        begin
          JT := JTc;
          J := GR.AddJoin(Tl, Tr, JT);
        end;
        J.JoinCondition.InsertNode(J.JoinCondition.ItemCount, TsbConditionItem(CurrSQLInfo.FWhere.Items[k]), False);
      end;

      CreateGraphInfo(T);
    end;

  end;


  //Remove extra joins(circles)
  procedure RemoveExtraJoins;
  var
    i, j, k, l: Integer;
    N: TsbConditionNode;

    function IsLeftParentOfRight(ALeftNode, ARightNode : Integer): Boolean;
    var
      i: Integer;
    begin
      Result := False;
      for i := 0 to GR.ItemCount - 1 do
        if Assigned(GR.FJoins[i, ARightNode]) and not GR.FJoins[i, ARightNode].FExtraCondition then
        begin
          if ALeftNode = i then
            Result := True
          else
            Result := IsLeftParentOfRight(ALeftNode, i);
          break;
        end;
    end;

  begin
    for i := 0 to GR.ItemCount - 1 do
      for j := 0 to GR.ItemCount - 1 do
      begin
        if not Assigned(GR.FJoins[i, j]) or not GR.FJoins[i, j].FExtraCondition then
          Continue;

        if IsLeftParentOfRight(i, j) then
          k := j
        else if IsLeftParentOfRight(j, i) then
          k := i
        else
          k := -1;

        N := nil;
        if k <> -1 then
        begin
          for l := 0 to GR.ItemCount - 1 do
            if Assigned(GR.FJoins[l, k]) and not GR.FJoins[l, k].FExtraCondition then
            begin
              N := GR.FJoins[l, k].FJoinCondition;
              break;
            end;
        end
        else
          N := CurrSQLInfo.FWhere;

        if Assigned(N) then
        begin
          N.InsertNode(N.ItemCount, GR.FJoins[i, j].FJoinCondition);
          GR.FJoins[i, j].FJoinCondition := nil;
        end;

        FreeAndNil(GR.FJoins[i, j]);
      end;
  end;


begin
  for i := 0 to CurrSQLInfo.FFrom.ItemCount -1 do
  begin
    if CurrSQLInfo.FFrom[i].FAlreadyJoined then
      Continue;

    GR := TsbGraph.Create;
    try
      CreateGraphInfo(CurrSQLInfo.FFrom[i]);

      //garbage collection
      if Assigned(CurrSQLInfo.FWhere) then
      begin
        j := 0;
        while j < CurrSQLInfo.FWhere.ItemCount do
          if not Assigned(CurrSQLInfo.FWhere.Items[j]) then
            CurrSQLInfo.FWhere.Delete(j)
          else
            Inc(j);
      end;

      GR.AnalyzeGraph;
      RemoveExtraJoins;
      AddGraphToJoinTree;
    finally
      Gr.Free;
    end;
  end;
end;


procedure TsbSQLParser.CreateIndexes;
var
  i: Integer;

  procedure CreateBranchIndexes(ATable: TsbTableItem);
  var
    i: Integer;
    Fld: array of string;
    fl: Boolean;
  begin
    SetLength(Fld, ATable.FParentJoinInfo.FSelfJoinFields.Count);
    try
      for i := 0 to ATable.FParentJoinInfo.FSelfJoinFields.Count - 1 do
        Fld[i] := TsbField(ATable.FParentJoinInfo.FSelfJoinFields[i]).FieldName;

      if Assigned(ATable.FParentJoinInfo.FFilterCondition) and not ATable.FTable.Filtered then
        ATable.FTable.ApplayPreparedFilter(ATable.FParentJoinInfo.FFilterCondition);

      if Length(Fld) > 0 then
      begin
        ATable.FParentJoinInfo.JoinIndexName := sbGetUniqueFileName(SbTempDir, '~ind', sbIndexExt);
        ATable.FParentJoinInfo.JoinIndexName := ATable.FTable.CreateIndex(ATable.FParentJoinInfo.JoinIndexName, Fld, [], True);
        fl := ATable.FTable.Filtered or ATable.FTable.IsIndexSet;
        ATable.FTable.Filtered := False;
        ATable.FTable.SetIndex(ATable.FParentJoinInfo.JoinIndexName, fl);
      end;

    finally
      SetLength(Fld, 0);
    end;

    for i := 0 to ATable.FJoins.ItemCount - 1 do
      CreateBranchIndexes(ATable.FJoins[i].FRightTable);
  end;

begin
  for i := 0 to CurrSQLInfo.FJoinTreeList.ItemCount - 1 do
    CreateBranchIndexes(CurrSQLInfo.FJoinTreeList[i]);
end;


procedure TsbSQLParser.ActivateIndexes(AActive: Boolean);
var
  i: Integer;

  procedure ActivateBranchIndexes(ATable: TsbTableItem);
  var
    i: Integer;
  begin
    ATable.FTable.IndexEnabled := AActive;
    for i := 0 to ATable.FJoins.ItemCount - 1 do
      ActivateBranchIndexes(ATable.FJoins[i].FRightTable);
  end;

begin
  for i := 0 to CurrSQLInfo.FJoinTreeList.ItemCount - 1 do
    ActivateBranchIndexes(CurrSQLInfo.FJoinTreeList[i]);
end;


function TsbSQLParser.RemoveScalars(ACondition: TsbConditionNode): Variant;
var
  C: TObject;
  i: Integer;
  fl: Variant;
begin
  Result := True;
  i := 0;
  while i <= ACondition.ItemCount - 1 do
  begin
    C := ACondition.Items[i];
    if C is TsbConditionNode then
      fl := RemoveScalars(TsbConditionNode(C))

    else
    begin
      if TsbConditionItem(C).FType = sctScalarCondition then
        fl := FVM.CalcCondition(TsbConditionItem(C))
      else
      begin
        Result := Unassigned;
        fl := Unassigned;
      end;
    end;

    if not VarIsEmpty(fl) then
    begin
      if ACondition.FType = sbnAND then
      begin
        if fl then
        begin
          ACondition.Delete(C);
          Continue;
        end
        else
        begin
          Result := False;
          Exit;
        end;
      end
      else
      begin
        if not fl then
        begin
          ACondition.Delete(C);
          Continue;
        end
        else
        begin
          Result := True;
          Exit;
        end;
      end
    end
    else
    begin
      Result := Unassigned;
      Inc(i);
    end;
  end;
end;


procedure TsbSQLParser.SetSQL(const Value: String);
begin
  FSQL := Value;
  ClearPreparedInfo;
end;


procedure TsbSQLParser.ClearPreparedInfo;
begin
  FreeAndNil(FSQLInfoList);
  FreeAndNil(FSQLExecInfo);
  FreeAndNil(FResultCursor);
  FPrepared := False;
end;


procedure TsbSQLParser.CleanUpWhere;
begin
  if Assigned(CurrSQLInfo.FWhere) and (CurrSQLInfo.FWhere.ItemCount = 0) then
    FreeAndNil(CurrSQLInfo.FWhere);

  if Assigned(CurrSQLInfo.FHaving) and (CurrSQLInfo.FHaving.ItemCount = 0) then
    FreeAndNil(CurrSQLInfo.FHaving);
end;


procedure TsbSQLParser.CreateFilters;
var
  i: Integer;

  procedure TableFilter(ATable: TsbTableItem);
  var
    i, j: Integer;
    fl: Boolean;
    Jn: TsbJoin;
  begin
    i := 0;
    while i < CurrSQLInfo.FWhere.ItemCount do
    begin
      if (CurrSQLInfo.FWhere.Items[i] is TsbConditionItem) and
         (TsbConditionItem(CurrSQLInfo.FWhere.Items[i]).FType = sctFilter) then
        if ((TsbConditionItem(CurrSQLInfo.FWhere.Items[i]).FLeftExpression.FJoinTable = ATable) or
           (TsbConditionItem(CurrSQLInfo.FWhere.Items[i]).FRightExpression.FJoinTable = ATable)) then
        begin
          fl := False;

          if ATable.FManualJoined then
            if Assigned(ATable.FParentJoinInfo.FParentTable) then
            begin
              for j := 0 to ATable.FParentJoinInfo.FParentTable.FJoins.ItemCount - 1 do
              begin
                Jn := ATable.FParentJoinInfo.FParentTable.FJoins[j];
                if (Jn.FRightTable = ATable) and (Jn.FJoinType in [sjtLeftJoin, sjtFullJoin]) then
                begin
                  fl := True;
                  break;
                end;
              end;
            end

            else
            begin
              for j := 0 to ATable.FJoins.ItemCount - 1 do
              begin
                Jn := ATable.FJoins[j];
                if Jn.FJoinType in [sjtRightJoin, sjtFullJoin] then
                begin
                  fl := True;
                  break;
                end;
              end;
            end

          else
            for j := 0 to CurrSQLInfo.FWhere.ItemCount - 1 do
             if (CurrSQLInfo.FWhere.Items[j] is TsbConditionItem) and
                ((TsbConditionItem(CurrSQLInfo.FWhere.Items[j]).FType  in [sctLeftJoin, sctFullJoin]) and
                 (TsbConditionItem(CurrSQLInfo.FWhere.Items[j]).FRightExpression.FJoinTable = ATable)
                  OR
                 (TsbConditionItem(CurrSQLInfo.FWhere.Items[j]).FType  in [sctRightJoin, sctFullJoin]) and
                 (TsbConditionItem(CurrSQLInfo.FWhere.Items[j]).FLeftExpression.FJoinTable = ATable)
                ) then
               begin
                 fl := True;
                 break;
               end;


          if fl then
          begin
            j := ATable.FParentJoinInfo.FilterCondition.AddItem;
            TsbConditionItem(ATable.FParentJoinInfo.FilterCondition.Items[j]).Assign(TsbConditionItem(CurrSQLInfo.FWhere.Items[i]))
          end

          else
          begin
            ATable.FParentJoinInfo.FilterCondition.InsertNode(ATable.FParentJoinInfo.FilterCondition.ItemCount,
              TsbConditionItem(CurrSQLInfo.FWhere.Items[i]));
            Continue;
          end;  
        end;
       Inc(i);
    end;
  end;

begin
  if not Assigned(CurrSQLInfo.FWhere) or (CurrSQLInfo.FWhere.FType = sbnOR) then
    Exit;

  for i := 0 to CurrSQLInfo.FFrom.ItemCount - 1 do
  begin
    if CurrSQLInfo.FFrom[i].FAlreadyJoined then
      Continue;
    TableFilter(CurrSQLInfo.FFrom[i]);
    if Assigned(CurrSQLInfo.FFrom[i].FParentJoinInfo.FFilterCondition) then
      CurrSQLInfo.FFrom[i].FTable.ApplayPreparedFilter(CurrSQLInfo.FFrom[i].FParentJoinInfo.FFilterCondition);
  end;
end;


procedure TsbSQLParser.SetParams(const Value: TsbParams);
begin
  FParams.Assign(Value);
end;


function TsbSQLParser.CostEvaluation: Int64;
begin
  Result := FCostEvaluation;
end;


function TsbSQLParser.Plan: String;
var
  i, j: Integer;
  h: String;

  function NodePlan(ATable: TsbTableItem): string;
  var
    i: Integer;
  begin
    Result := ATable.Alias;
    if not ATable.FParentJoinInfo.FNaturalJoin then
      Result := 'Sort(' + Result + ')';

    for i := 0 to ATable.FJoins.ItemCount - 1 do
    begin
      if Length(Result) <> 0 then
        Result := Result + ', ';
      Result := Result + NodePlan(ATable.FJoins[i].FRightTable);
    end;

    if ATable.FJoins.ItemCount > 0 then
      Result := 'Join(' + Result +')';
  end;

begin
  Result := '';

  if not Assigned(FSQLInfoList) then
    Exit;

  for j := 0 to FSQLInfoList.ItemCount - 1 do
  begin
    if FSQLInfoList.ItemCount > 1 then
      Result := Result +#13#10'QUERY #' + IntToStr(j + 1) + ': ';

    h := '';
    for i := 0 to FSQLInfoList[j].FJoinTreeList.ItemCount - 1 do
    begin
      if Length(h) <> 0 then
        h := h + ', ';
      h := h + NodePlan(FSQLInfoList[j].FJoinTreeList[i]);
    end;
    Result := Result + h;

    if FSQLInfoList[j].FJoinTreeList.ItemCount > 1 then
      Result := 'Product(' + Result +')';

    if FSQLInfoList[j].FSelect.FDistinct or Assigned(FSQLInfoList[j].FOrder) then
      Result := 'Sort(' + Result + ')';
  end;
end;


procedure TsbSQLParser.AnalyzeTables;
var
  i: Integer;
begin
  for i := 0 to CurrSQLInfo.FFrom.ItemCount - 1 do
  begin
    if CurrSQLInfo.FFrom[i].FTable.RecordCount > 0 then
    begin
      CurrSQLInfo.FFrom[i].FIndexEvaluation := CurrSQLInfo.FFrom[i].FTable.RecordCount * Ln(CurrSQLInfo.FFrom[i].FTable.RecordCount);
      CurrSQLInfo.FFrom[i].FSearchEvaluation := Ln(CurrSQLInfo.FFrom[i].FTable.RecordCount) / Ln(2);
    end
    else
    begin
      CurrSQLInfo.FFrom[i].FIndexEvaluation := 0;
      CurrSQLInfo.FFrom[i].FSearchEvaluation := 0;
    end;
  end;
end;


function TsbSQLParser.GetResultCursor: TsbTableCursor;
begin
  Result := FResultCursor;
  FResultCursor := nil;
end;


procedure TsbSQLParser.AnalyzeSelect;
var
  i, j: Integer;
begin
  if Assigned(CurrSQLInfo.FAggrFunct) then
    for i := 0 to CurrSQLInfo.FSelect.ItemCount - 1 do
    begin
      for j := 0 to CurrSQLInfo.FSelect[i].FExpression.ItemCount - 1 do
        if Assigned(CurrSQLInfo.FSelect[i].FExpression[j].FAggrFunction) then
        begin
          CurrSQLInfo.FSelect[i].FAggrFunctsExist := True;
          break;
        end;

      if not CurrSQLInfo.FSelect[i].FAggrFunctsExist then
        if not Assigned(CurrSQLInfo.FGroup) or (CurrSQLInfo.FGroup.IndexOf(Pointer(i)) = -1) then
          ShowParserError(FStatus, sbeInvalidClause, ['GROUP BY']);
    end;
end;



procedure TsbSQLParser.ExecCreate;
begin
  sbCreateTable(FSQLExecInfo.FTableName, FSQLExecInfo.FFieldList);
end;


procedure TsbSQLParser.ExecDrop;
var
  T: TsbTableCursor;
begin
  T := sbOpenTable(FSQLExecInfo.FTableName, nil, sbmRead);
  sbDropTable(T);
end;


procedure TsbSQLParser.ExecInsert;
var
  h: TsbSQLExecInfo;

  procedure AppendRecord;
  begin
    FSQLExecInfo.FTable.Append;
    try
      UpdateTableRecord;
      FSQLExecInfo.FTable.Post;
    except
      FSQLExecInfo.FTable.Cancel;
      raise;
    end;
  end;

begin
  if Assigned(FSQLInfoList) then
  begin
    h := FSQLExecInfo;
    try
      FSQLExecInfo := nil;
      Open;
    finally
      FSQLExecInfo := h;
    end;

    while not FResultCursor.Eof do
    begin
      AppendRecord;
      FResultCursor.Next;
    end;
  end

  else
    AppendRecord;
end;


procedure TsbSQLParser.ExecUpdate;
begin
  if Assigned(CurrSQLInfo.FWhere) then
    CurrSQLInfo.FFrom[0].FTable.ApplayPreparedFilter(CurrSQLInfo.FWhere);

  CurrSQLInfo.FFrom[0].FTable.First;
  while not CurrSQLInfo.FFrom[0].FTable.Eof do
  begin
    FSQLExecInfo.FTable.InternalRecordPos := CurrSQLInfo.FFrom[0].FTable.InternalRecordPos;
    FSQLExecInfo.FTable.Edit;
    try
      UpdateTableRecord;
      FSQLExecInfo.FTable.Post;
    except
      FSQLExecInfo.FTable.Cancel;
      raise;
    end;
    CurrSQLInfo.FFrom[0].FTable.Next;
  end;
end;


procedure TsbSQLParser.ExecDelete;
begin
  if Assigned(CurrSQLInfo.FWhere) then
    CurrSQLInfo.FFrom[0].FTable.ApplayPreparedFilter(CurrSQLInfo.FWhere)
  else
  begin
    FSQLExecInfo.FTable.Clear;
    Exit;
  end;

  CurrSQLInfo.FFrom[0].FTable.First;
  while not CurrSQLInfo.FFrom[0].FTable.Eof do
  begin
    FSQLExecInfo.FTable.InternalRecordPos := CurrSQLInfo.FFrom[0].FTable.InternalRecordPos;
    FSQLExecInfo.FTable.Delete;
    CurrSQLInfo.FFrom[0].FTable.Next;
  end;
end;


procedure TsbSQLParser.UpdateTableRecord;
var
  i: Integer;
  V: Variant;
begin
  with FSQLExecInfo do
    for i := 0 to FUpdFieldList.ItemCount - 1 do
    begin
      FUpdFieldList[i].FRightPart.FPosition := 0;
      FVM.CalcExpression(FUpdFieldList[i].FRightPart);
      V := FVM.FRegA;
      if FUpdFieldList[i].FField.FieldType = sbfDateTime then
        V := FVM.CheckDate(V);
      FUpdFieldList[i].FField.Value := V;
    end;
end;


procedure TsbSQLParser.CreateResultTableStructure(AFields: TsbFields);
var
  i, s: Integer;
  V: Variant;
  t: TsbFieldType;
  h: string;
begin
  FVM.FDefExprType := True;
  try
    if Assigned(CurrSQLInfo.FAggrFunct) then
      for i := 0 to CurrSQLInfo.FAggrFunct.ItemCount - 1 do
      begin
        if CurrSQLInfo.FAggrFunct[i].FFunction = safAvg then
        begin
          CurrSQLInfo.FAggrFunct[i].FCurrentValue := 0;
          CurrSQLInfo.FAggrFunct[i].FCurrentValue := VarAsType(CurrSQLInfo.FAggrFunct[i].FCurrentValue, varDouble);
        end
        else if CurrSQLInfo.FAggrFunct[i].FFunction = safCount then
        begin
          CurrSQLInfo.FAggrFunct[i].FCurrentValue := 0;
          CurrSQLInfo.FAggrFunct[i].FCurrentValue := VarAsType(CurrSQLInfo.FAggrFunct[i].FCurrentValue, varInteger);
        end
        else
        begin
          FVM.CalcExpression(CurrSQLInfo.FAggrFunct[i].FExpression);
          CurrSQLInfo.FAggrFunct[i].FCurrentValue := FVM.FRegA;
        end;
      end;

    for i := 0 to CurrSQLInfo.FSelect.ItemCount - 1 do
    begin
      if CurrSQLInfo.FSelect.Items[i].FHidden then
        Continue;

      CurrSQLInfo.FSelect.Items[i].FExpression.FPosition := 0;
      FVM.CalcExpression(CurrSQLInfo.FSelect.Items[i].FExpression);
      V := FVM.FRegA;
      s := 0;
      case VarType(V) of
        varSmallint,
        varInteger:  t := sbfInteger;

        varSingle,
        varDouble:   t := sbfFloat;

        varCurrency: t := sbfCurrency;

        varDate:     t := sbfDateTime;

        varBoolean:  t := sbfBoolean;

        varByte:     t := sbfByte;

        varNull:     begin
                       t := sbfString;
                       s := 255;
                     end;

        varString:   begin
                       t := sbfString;
                       s := Length(V);
                       if s > 255 then
                         s := 255;
                     end;

        varOLEStr:  t := sbfBLOB;
      else
        t := sbfUnknown;
        ShowParserError(FStatus, sbeTypeNotSupported, []);
      end;

      h := FVM.FExprFieldName;
      if (h = ' ') or Assigned(AFields.FindField(h)) then
      begin
        if h = ' ' then
          h := 'FIELD'
        else
          h := FVM.FExprFieldName;

        h := GetUniqFieldNAme(h, AFields);
      end;
      AFields.CreateField(h, t, s);
    end;

  finally
    FVM.FDefExprType := False;
  end;
end;


procedure TsbSQLParser.ParseSELECTStatement;
var
  i, j: Integer;
  flUnion: Boolean;

  procedure CreateResultTable;
  var
    Flds: TsbFields;
    h: string;
  begin
    // Creating Final Result table (real data)
    Flds := TsbFields.Create;
    try
      CreateResultTableStructure(Flds);
      h := sbGetUniqueFileName(SbTempDir, '~tmpRes', sbTableExt);
      sbCreateTable(h, Flds);

      FResultCursor := sbOpenTable(h, FResultCursor, sbmReadWrite, True);
    finally
      Flds.Free;
    end;
  end;


  procedure CheckUnionFieldTypes;
  var
    Flds: TsbFields;
    i: Integer;
  begin
    Flds := TsbFields.Create;
    try
      CreateResultTableStructure(Flds);

      if FResultCursor.Fields.Count <> Flds.Count then
        ShowParserError(FStatus, sbeUnionColCountMismatch, []);

      for i := 0 to FResultCursor.Fields.Count - 1 do
        if not CheckTypeCompatibility(FResultCursor.Fields[i].FieldType, Flds[i].FieldType) then
          ShowParserError(FStatus, sbeUnionColTypeMismatch, [IntToStr(i + 1)]);

    finally
      Flds.Free;
    end;
  end;


begin
  if not Assigned(FSQLInfoList) then
    FSQLInfoList := TsbSQLInfoList.Create;
  flUnion := False;

  repeat                       //loop by UNIONS
    if flUnion then
      FSQLInfoList.FCurrentSQLInfo := FSQLInfoList.Add;

    i := FStatus.sPos;
    FSelectPart := False;
    FHavingPart := False;
    FUpdatePart := False;
    flUnion := False;
    ScannerSearchToken(FStatus, kwFROM);
    ParseFROM;
    j := FStatus.sPos;

    ScannerChangePosition(FStatus, i);
    FSelectPart := True;
    ParseSELECT;
    FSelectPart := False;
    ScannerCheckNextToken(FStatus, kwFrom);
    for i := 0 to CurrSQLInfo.FSelect.ItemCount - 1 do
      AnalyseExpression(CurrSQLInfo.FSelect[i].FExpression);

    ScannerChangePosition(FStatus, j);
    if ScannerNextTokenIs(FStatus, kwWHERE) then
      ParseWHERE;

    if ScannerNextTokenIs(FStatus, kwGROUP) then
      ParseGROUPBY;

    if ScannerNextTokenIs(FStatus, kwHAVING) then
    begin
      FHavingPart := True;
      ParseHAVING;
      FHavingPart := False;
    end;

    if ScannerNextTokenIs(FStatus, kwUNION) then
    begin
      ParseUNION;
      flUnion := True;
    end

    else if ScannerNextTokenIs(FStatus, kwORDER) then
      ParseORDERBY;

    if not FExternalResulCursor or  FExternalResulCursor and Assigned(FResultCursor) then
      if FSQLInfoList.IsUnion then
        CheckUnionFieldTypes
      else
        CreateResultTable;

    OptimisationCondition(CurrSQLInfo.FWhere);
    if CurrSQLInfo.FAlwaysEmptyResult then
      Continue;

    OptimisationCondition(CurrSQLInfo.FHaving);
    if CurrSQLInfo.FAlwaysEmptyResult then
      Continue;

    if Assigned(CurrSQLInfo.FHaving) then
      SortConditions(CurrSQLInfo.FHaving);


    CreateFilters;
    AnalyzeTables;
    AnalyzeSelect;
    CreateJoinInfo;
    JoinAnalyze;

    if not Assigned(FCustomParsingEndProc) then
      CreateIndexes;

    CleanUpWhere;

    if Assigned(CurrSQLInfo.FWhere) then
    begin
      PackConditions(CurrSQLInfo.FWhere);
      SortConditions(CurrSQLInfo.FWhere);
    end;

    CurrSQLInfo.FAlwaysEmptyResult := False;

  until not flUnion;
end;


procedure TsbSQLParser.OptimisationCondition(var ACondition: TsbConditionNode);
var
  fl: Variant;
begin
  if Assigned(ACondition) then
  begin
    DeleteEmptyNodes(ACondition);
    Normalization(ACondition);
    PackConditions(ACondition);
    AnalyseConditions(ACondition);
    if not FDoNotOptimize then
    begin
      fl := RemoveScalars(ACondition);
      if not VarIsEmpty(fl) then
        if fl then
          FreeAndNil(ACondition)
        else
        begin
          CurrSQLInfo.FAlwaysEmptyResult := True;
          Exit;
        end;
    end;

    if Assigned(ACondition) then
      PackConditions(ACondition);
  end;
end;


function TsbSQLParser.GetUniqFieldName(const ABaseName: String; AFields: TsbFields): String;
var
  j: Integer;
  h: string;
begin
  for j := 1 to 10000 do
  begin
    h := ABaseName + IntToStr(j);
    if not Assigned(AFields.FindField(h)) then
    begin
      Result := h;
      break;
    end;
  end;
end;


function TsbSQLParser.CurrSQLInfo: TsbSQLInfo;
begin
  Result := FSQLInfoList.Items[FSQLInfoList.FCurrentSQLInfo];
end;



function TsbSQLParser.CheckTypeCompatibility(const AType1, AType2: TsbFieldType): Boolean;
begin
  Result := (AType1 = AType2) or
            (AType1 = sbfString) and (AType2 <> sbfBLOB) or
            (AType1 = sbfInteger) and not (AType2 in [sbfString, sbfBLOB]) or
            (AType1 = sbfFloat) and not (AType2 in [sbfString, sbfBLOB]);
end;

{ TsbTempTable }

constructor TsbTempTable.Create;
begin
  FTable := TList.Create;
  FFields := TStringList.Create;
  ClearTable;
end;


destructor TsbTempTable.Destroy;
begin
  FreeAndNil(FTable);
  FreeAndNil(FFields);
  inherited;
end;


procedure TsbTempTable.CreateTable(const AFieldNames: array of string);
var
  i: Integer;
begin
  DropTable;

  for i := Low(AFieldNames) to High(AFieldNames) do
    FFields.Add(AnsiUpperCase(AFieldNames[i]));
end;


procedure TsbTempTable.DropTable;
begin
  FTable.Clear;
  FFields.Clear;
  FRecNo := 0;
end;


function TsbTempTable.GetField(Index: Integer): Integer;
begin
  Result := Integer(FTable[(FRecNo-1)*FFields.Count+Index]);
end;


procedure TsbTempTable.SetField(Index: Integer; const Value: Integer);
begin
  FTable[(FRecNo-1)*FFields.Count+Index] := Pointer(Value);
end;


function TsbTempTable.RecordCount: Integer;
begin
  Result := FTable.Count div FFields.Count;
end;


procedure TsbTempTable.Append;
begin
  Append(1);
end;


procedure TsbTempTable.Append(const ARecNum: Integer);
begin
  if ARecNum > 0 then
  begin
    FRecNo := RecordCount + 1;
    FTable.Count := FTable.Count + FFields.Count * ARecNum;
  end;
end;


procedure TsbTempTable.SetRecNo(const Value: Integer);
begin
  if (Value <> FRecNo) and (Value >= 1) and (Value <= RecordCount) then
    FRecNo := Value;
end;


procedure TsbTempTable.Delete;
var
  i: Integer;
begin
  for i := 1 to FFields.Count do
    FTable.Delete((FRecNo-1)*FFields.Count);
end;


procedure TsbTempTable.ClearTable;
begin
  FTable.Clear;
  FRecNo := 0;
end;


function TsbTempTable.IndexOfField(const AFldName: String): Integer;
begin
  Result := FFields.IndexOf(AFldName);
end;



{ TsbParentJoinInfoRec }

constructor TsbParentJoinInfo.Create;
begin
  FSelfJoinFields := TList.Create;
  FForeignJoinFields := TList.Create;
  FParentTable := nil;
  FJoinCondition := nil;
  FFilterCondition := nil;
  FJoinIndexName := '';
  FNaturalJoin := True;
end;


destructor TsbParentJoinInfo.Destroy;
begin
  FreeAndNil(FJoinCondition);
  FreeAndNil(FFilterCondition);
  FreeAndNil(FSelfJoinFields);
  FreeAndNil(FForeignJoinFields);
  inherited;
end;


function TsbParentJoinInfo.FilterCondition(ATest: Boolean = False): TsbConditionNode;
begin
  if not ATest and not Assigned(FFilterCondition) then
  begin
    FFilterCondition := TsbConditionNode.Create(nil);
    FFilterCondition.FType := sbnAND;
  end;
  Result :=FFilterCondition;
end;


function TsbParentJoinInfo.JoinCondition(ATest: Boolean = False): TsbConditionNode;
begin
  if not ATest and not Assigned(FJoinCondition) then
  begin
    FJoinCondition := TsbConditionNode.Create(nil);
    FJoinCondition.FType := sbnAND;
  end;
  Result :=FJoinCondition;
end;


procedure TsbParentJoinInfo.SetJoinIndexName(const Value: String);
begin
  FJoinIndexName := Value;
  FNaturalJoin := Length(FJoinIndexName) = 0;
end;




{ TsbGraph }
destructor TsbGraph.Destroy;
begin
  Clear;
  inherited;
end;


function TsbGraph.AddJoin(ATableL, ATableR: TsbTableItem; AJoinType: TsbJoinType): TsbGraphJoin;
var
  i, j: Integer;
begin
  i := FList.IndexOf(ATableL);
  j := FList.IndexOf(ATableR);

  if Assigned(FJoins[i, j]) then
    raise ESBParser.CreateFmt('Exclusive joins for tables "' + ATableL.FAlias + '" and "' + ATableR.FAlias + '"', []);

  Result := TsbGraphJoin.Create;
  Result.FJoinType := AJoinType;
  FJoins[i, j] := Result;
end;


function TsbGraph.AddNode(ATable: TsbTableItem): Integer;
var
  T: TsbTableItem;


  procedure AddN(T: TsbTableItem);
  var
    i: Integer;
  begin
    i := FList.Add(T);
    if ATable = T then
      Result := i;
    SetLength(FJoins, ItemCount, ItemCount);
    for i := Low(FJoins) to High(FJoins) do
      FJoins[High(FJoins), i] := nil;
    for i := Low(FJoins) to High(FJoins) do
      FJoins[i, High(FJoins)] := nil;
  end;


  procedure AddManualJoin(ALTbl, ARTbl: TsbTableItem; AJoinType: TsbJoinType);
  var
    i: Integer;
    J: TsbGraphJoin;
  begin
    AddN(ARTbl);

    if Assigned(ALTbl) then
    begin
      J := AddJoin(ALTbl, ARTbl, AJoinType);
      J.FMandatory := True;
    end;

    for i := 0 to ARTbl.FJoins.ItemCount - 1 do
      AddManualJoin(ARTbl, ARTbl.FJoins[i].FRightTable, ARTbl.FJoins[i].FJoinType);
  end;


begin
  if ATable.FManualJoined then
  begin
    Result := -1;
    T := ATable;
    while Assigned(T.FParentJoinInfo.FParentTable) do
      T := T.FParentJoinInfo.FParentTable;
    AddManualJoin(nil, T, sjtInnerJoin);
  end
  else
    AddN(ATable);
end;


function TsbGraph.GetNode(Index: Integer): TsbTableItem;
begin
  Result := TsbTableItem(FList[Index]);
end;


function TsbGraph.AnalyzeGraph: Int64;

const
  cVeryExpensiveWay: Real = 1E38;

var
  i, j, k, lCurrNode: Integer;
  lNodes: TList;
  lCurrJoins: array of array of TsbGraphJoin;
  lCheapestWay, lCurrentWay: Real;
  fl: Boolean;


  function LookNode(ANodeIndex: Integer): Boolean;
  var
    i, j:  Integer;
    gJ:  TsbGraphJoin;
    lRibs: TList;
  begin
    Result := False;

    if lCurrentWay > lCheapestWay then
      Exit;

    Nodes[ANodeIndex].FOccupied := True;

    lRibs := TList.Create;
    try
      gJ := nil;
      for i := 0 to ItemCount - 1 do
      begin
        if Assigned(FJoins[ANodeIndex, i]) and not FJoins[ANodeIndex, i].FOccupied  then
          gJ := FJoins[ANodeIndex, i]
        else if Assigned(FJoins[i, ANodeIndex]) and not FJoins[i, ANodeIndex].FOccupied then
        begin
          if not FJoins[i, ANodeIndex].FMandatory and (FJoins[i, ANodeIndex].FJoinType = sjtInnerJoin) then
            gJ := FJoins[i, ANodeIndex]
          else
            Exit;
        end
        else
          Continue;

        gJ.FOccupied := True;
        gJ.FWeight := Nodes[i].FSearchEvaluation*Nodes[lCurrNode].FTable.RecordCount;
        gJ.FNextNodeIndex := i;
        lRibs.Add(gJ);
      end;

      if lRibs.Count = 0 then
      begin
        Result := True;
        Exit;
      end;

      //Sort ribs by weights
      for i := 0 to lRibs.Count - 2 do
        for j := i+1 to lRibs.Count - 1 do
        begin
          if TsbGraphJoin(lRibs[i]).FMandatory and not TsbGraphJoin(lRibs[j]).FMandatory then
            Continue;

          if TsbGraphJoin(lRibs[j]).FMandatory and not TsbGraphJoin(lRibs[i]).FMandatory then
          begin
            lRibs.Exchange(i, j);
            Continue;
          end;

          if (TsbGraphJoin(lRibs[i]).FJoinType = sjtLeftJoin) and (TsbGraphJoin(lRibs[j]).FJoinType = sjtInnerJoin) then
            Continue;

          if (TsbGraphJoin(lRibs[j]).FJoinType = sjtLeftJoin) and (TsbGraphJoin(lRibs[i]).FJoinType = sjtInnerJoin) then
          begin
            lRibs.Exchange(i, j);
            Continue;
          end;

          if TsbGraphJoin(lRibs[i]).FWeight > TsbGraphJoin(lRibs[j]).FWeight then
            lRibs.Exchange(i, j);
        end;


      for i := 0 to lRibs.Count - 1 do
      begin
        if Nodes[TsbGraphJoin(lRibs[i]).FNextNodeIndex].FOccupied then
          if TsbGraphJoin(lRibs[i]).FJoinType = sjtLeftJoin then
            Exit
          else
            Continue;


        lCurrentWay := lCurrentWay + TsbGraphJoin(lRibs[i]).FWeight;
        if not LookNode(TsbGraphJoin(lRibs[i]).FNextNodeIndex) then
        begin
          lCurrentWay := lCurrentWay - TsbGraphJoin(lRibs[i]).FWeight;
          Nodes[TsbGraphJoin(lRibs[i]).FNextNodeIndex].FOccupied := False;
        end
        else
          lCurrJoins[ANodeIndex, TsbGraphJoin(lRibs[i]).FNextNodeIndex] := TsbGraphJoin(lRibs[i]);
      end;

      Result := True;

    finally
      lRibs.Free;
    end;
  end;

begin
  Result := 0;
  FFirstNode := -1;

  if ItemCount = 0 then
    Exit;

  lNodes := TList.Create;
  SetLength(lCurrJoins, ItemCount, ItemCount);
  try
    for i := 0 to ItemCount - 1 do
      lNodes.Add(Pointer(i));

    //sorting nodes by size
    for i := 0 to ItemCount - 2 do
      for j := i + 1 to ItemCount - 1 do
        if Nodes[Integer(lNodes[i])].FTable.RecordCount > Nodes[Integer(lNodes[j])].FTable.RecordCount then
          lNodes.Exchange(i, j);


     lCheapestWay := cVeryExpensiveWay;
     for i := 0 to ItemCount - 1 do
     begin
       lCurrNode := Integer(lNodes[i]);
       for j := 0 to ItemCount - 1 do
         for k := 0 to ItemCount - 1 do
           lCurrJoins[j, k] := nil;

       lCurrentWay := 0;
       for j := 0 to ItemCount - 1 do
       begin
         Nodes[Integer(lNodes[j])].FOccupied := False;
         if Integer(lNodes[j]) <> lCurrNode then
           lCurrentWay := lCurrentWay+Nodes[Integer(lNodes[j])].FIndexEvaluation;
       end;

       for k := 0 to ItemCount - 1 do
         for j := 0 to ItemCount - 1 do
           if Assigned(FJoins[k, j]) then
             FJoins[k, j].FOccupied := False;


       if lCurrentWay > lCheapestWay then
         Continue;

       if LookNode(lCurrNode) and (lCurrentWay < lCheapestWay) then
       begin
         // Are all nodes occupied?
         fl := True;
         for j := 0 to ItemCount - 1 do
           if not Nodes[j].FOccupied then
           begin
            fl := False;
            break;
           end;

         if fl then
         begin
           FFirstNode := lCurrNode;
           lCheapestWay := lCurrentWay;

           for j := 0 to ItemCount - 1 do
             for k := 0 to ItemCount - 1 do
               if Assigned(FJoins[j, k])then
                 FJoins[j, k].FExtraCondition := True;

           for j := 0 to ItemCount - 1 do
             for k := 0 to ItemCount - 1 do
               if Assigned(lCurrJoins[j, k])then
                 lCurrJoins[j, k].FExtraCondition := False;

           for j := 0 to ItemCount - 1 do
             for k := 0 to ItemCount - 1 do
               if Assigned(lCurrJoins[j, k]) or Assigned(lCurrJoins[k, j]) then
                 FJoins[j, k] := lCurrJoins[j, k];
           end;
       end;
     end;


  finally
    SetLength(lCurrJoins, 0, 0);
    lNodes.Free;
  end;

  if FFirstNode = -1 then
    ShowSBError(sbeOCantImplJoins, []);

  Result := Round(lCheapestWay);
end;


procedure TsbGraph.Clear;
var
  i,j: Integer;
begin
  for i := Low(FJoins) to High(FJoins) do
    for j := Low(FJoins) to High(FJoins) do
      if Assigned(FJoins[i, j]) then
        FJoins[i, j].Free;

  SetLength(FJoins, 0, 0);
  FList.Clear;
end;



{ TsbGraphJoin }

constructor TsbGraphJoin.Create;
begin
  FJoinCondition := nil;
  FMandatory := False;
end;


destructor TsbGraphJoin.Destroy;
begin
  FreeAndNil(FJoinCondition);
  inherited;
end;


function TsbGraphJoin.JoinCondition: TsbConditionNode;
begin
  if not Assigned(FJoinCondition) then
  begin
    FJoinCondition := TsbConditionNode.Create(nil);
    FJoinCondition.FType := sbnAND;
  end;
  Result := FJoinCondition;
end;



{ TsbEmbeddedSQL }

constructor TsbEmbeddedSQL.Create;
begin
  FParser := nil;
end;


destructor TsbEmbeddedSQL.Destroy;
begin
  FreeAndNil(FParser);
  inherited;
end;


procedure TsbEmbeddedSQL.Prepare(APrevFrom: TsbTableList);
begin
  FreeAndNil(FParser);
  FParser := TsbSQLParser.Create;
  FParser.SQL := FSQLText;
  FParser.Params := FParams;
  FParser.FSQLInfoList := TsbSQLInfoList.Create;
  FParser.CurrSQLInfo.FFrom.FPrevFrom := APrevFrom;
  FParser.Prepare;
end;



{ TsbGroupByList }

function TsbGroupByList.Add(AIndex: Integer): Integer;
begin
  Result := FList.Add(Pointer(AIndex));
end;


procedure TsbGroupByList.Clear;
begin
  FList.Clear;
end;


function TsbGroupByList.GetItem(Index: Integer): Integer;
begin
  Result := Integer(FList[Index]);
end;



{ TsbTempGroup }

function TsbTempUniqGroup.Add(const AValue: Variant): Boolean;
var
  V: PVariant;
  i: Integer;
  j: ShortInt;
begin
  i := IndexOf(AValue);

  if i <> -1 then
  begin
    j := CompareValues(PVariant(FList[i])^, AValue);
    if j < 0 then
      Inc(i)
    else if j = 0 then
    begin
      Result := False;
      Exit;
    end
  end
  else
    i := 0;

  New(V);
  V^ := AValue;
  FList.Insert(i, V);
  Result := True;
end;


procedure TsbTempUniqGroup.Clear;
var
  i: Integer;
begin
  for i := 0 to FList.Count-1 do
    Dispose(PVariant(FList[i]));
  FList.Clear;
end;



function TsbTempUniqGroup.CompareValues(const AValue1, AValue2: Variant): Shortint;
begin
  if VarIsNull(AValue1) then
    if VarIsNull(AValue2) then
      Result := 0
    else
      Result := -1
  else
    if VarIsNull(AValue2) then
      Result := 1
    else
      if AValue1 < AValue2 then
        Result := -1
      else if AValue1 > AValue2 then
        Result := 1
      else
        Result := 0;
end;


function TsbTempUniqGroup.IndexOf(const AValue: Variant): Integer;
var
  bp, ep: Integer;
  i: Shortint;
  V: Variant;
begin
  Result := -1;
  bp := 0;
  ep := FList.Count - 1;

  while ep >= bp do
  begin
    Result := bp + (ep+1-bp) div 2;
    V := PVariant(FList[Result])^;
    i := CompareValues(AValue, V);
    if i < 0 then
      ep := Result - 1

    else if i > 0 then
      bp := Result + 1

    else
      break;
  end;
end;



{ TsbAggrFunction }

constructor TsbAggrFunction.Create;
begin
  FExpression := TsbExpression.Create;
  FNull := True;
  FCurrentValue := 0;
  FSecCurrentValue := 0;
  FTempTable := nil;
end;


destructor TsbAggrFunction.Destroy;
begin
  FreeAndNil(FTempTable);
  FreeAndNil(FExpression);
  inherited;
end;


function TsbAggrFunction.GetDistinct: Boolean;
begin
  Result := Assigned(FTempTable);
end;

procedure TsbAggrFunction.SetDistinct(const Value: Boolean);
begin
  FreeAndNil(FTempTable);

  if Value then
    FTempTable := TsbTempUniqGroup.Create;
end;



{ TsbAggrFunctionList }

function TsbAggrFunctionList.Add(AFunctType: TsbAggrFunctType; ADistinct: Boolean): Integer;
var
  F: TsbAggrFunction;
begin
  F := TsbAggrFunction.Create;
  F.FFunction := AFunctType;
  F.Distinct := ADistinct;
  Result := FList.Add(F);
end;


function TsbAggrFunctionList.GetItem(Index: Integer): TsbAggrFunction;
begin
  Result := TsbAggrFunction(FList[Index]);
end;


procedure TsbAggrFunctionList.MakeFinalResult;
var
  j: Integer;
begin
  for j := 0 to ItemCount - 1 do
  begin
    if Items[j].FNull then
      TVarData(Items[j].FCurrentValue).VType := varNull
    else if Items[j].FFunction = safAvg then
      Items[j].FCurrentValue := Items[j].FCurrentValue/Items[j].FSecCurrentValue;
  end;
end;


procedure TsbAggrFunctionList.PrepareForCalc;
var
  i: Integer;
begin
  for i := 0 to ItemCount - 1 do
  begin
    if Items[i].Distinct then
      Items[i].FTempTable.Clear;

    if Items[i].FFunction = safCount then
      Items[i].FNull := False
    else
      Items[i].FNull := True;

    case Items[i].FFunction of
      safSum,
      safCount: Items[i].FCurrentValue := 0;

      safAvg:  begin
                  Items[i].FCurrentValue := 0;
                  Items[i].FSecCurrentValue := 0;
                end;

      safMin:   Items[i].FCurrentValue := MaxDouble;

      safMax:   Items[i].FCurrentValue := MinDouble;
    end;
  end;
end;



{ TsbSQLExecInfo }

constructor TsbSQLExecInfo.Create;
begin
  FTableName := '';
  FFieldList := nil;
  FTable := nil;
  FUpdFieldList := nil;
  FType := sesUnknown;
end;


destructor TsbSQLExecInfo.Destroy;
begin
  FreeAndNil(FUpdFieldList);
  FreeAndNil(FFieldList);
  if Assigned(FTable) then
    sbClose(FTable);
  inherited;
end;



{ TsbUpdFieldList }

function TsbUpdFieldList.Add(AField: TsbField): TsbUpdField;
begin
  Result := TsbUpdField.Create;
  Result.FField := AField;
  FList.Add(Result);
end;


function TsbUpdFieldList.GetItem(Index: Integer): TsbUpdField;
begin
  Result := TsbUpdField(FList[Index]);
end;



{ TsbUpdField }

constructor TsbUpdField.Create;
begin
  FRightPart := TsbExpression.Create;
end;


destructor TsbUpdField.Destroy;
begin
  FreeAndNil(FRightPart);
  inherited;
end;



{ ESBParser }

constructor ESBParser.Create(const AMessage: String; ALine, ACol: Integer);
begin
 FOriginalMessage := AMessage;
 FErrLine := ALine;
 FErrCol := ACol;
 Message := FOriginalMessage + #13 + 'Line: ' + IntToStr(FErrLine) + '   Column: ' + IntToStr(FErrCol);
end;



procedure ScannerGetNextToken(var AStatus: TsbScannerRes);
var
  l_tok: TsbKeyWord;
  l_token: string;
  i: Integer;

  function isDigit(c: Char): Boolean;
  begin
    if (c >= '0') and (c <= '9') or (c = '.') then
      Result := True
    else
      Result := False;
  end;

  function isLetter(c: Char): Boolean;
  begin
    if (c >= 'A') and (c <= 'Z') or (c >= 'a') and (c <= 'z') or isDigit(c) or
      (c in ['_', '$']) then
      Result := True
    else
      Result := False;
  end;

  function IsTable(AToken: string): Boolean;
  begin
    Result := False
  end;


  procedure GetQuotedText(Q: Char);
  begin
    with AStatus do
    begin
      Inc(sPos);
      while ((sText[sPos] <> Q) and not (sText[sPos] in [#10, #13])) do
      begin
        sToken := sToken + sText[sPos];
        Inc(sPos);
      end;

      if sText[sPos] in [#10, #13] then
        ShowParserError(AStatus, sbeNClQuotes, []);

      Inc(sPos);
      if sText[sPos] = Q then
      begin
        sToken := sToken + Q;
        GetQuotedText(Q);
      end;
    end;
  end;


begin
  with AStatus do
  begin
    if sWasBack then
    begin
      ScannerChangePosition(AStatus, sPos + Length(sToken));
      Exit;
    end;

    sToken := '';
    sTokenType := stUnknown;
    sKeyWord := kwUnknown;
    sKeyWordType := tkwNone;

    while (sText[sPos] = ' ') or (sText[sPos] in [#10, #13]) do {Skip spaces}
      Inc(sPos);

    if ScannerEOF(AStatus) then {Is it end text? }
    begin
      sKeyWord := kwFinish;
      sTokenType := stKeyWord;
    end

    else if sText[sPos] = '''' then  {Is it quoted text with apostrophe?}
    begin
      GetQuotedText('''');
      sTokenType := stQuotedText;
    end

    else if sText[sPos] = '"' then  {Is it quoted text with quotes?}
    begin
      GetQuotedText('"');
      sTokenType := stQuotedText;
    end

    else if IsDigit(sText[sPos]) then {Is it number?}
    begin
      while IsDigit(sText[sPos]) do
      begin
        sToken := sToken + sText[sPos];
        Inc(sPos);
      end;
      sTokenType := stNumber;
    end

    else if isLetter(sText[sPos]) then {Is it KeyWord?}
    begin
      while isLetter(sText[sPos]) do
      begin
        sToken := sToken + sText[sPos];
        Inc(sPos);
      end;

      sKeyWord := sqlKeyWords.FindKeyWord(sToken);
      if sKeyWord <> kwUnknown then
        sTokenType := stKeyWord;
    end

    else
    begin
      sKeyWord := sqlKeyWords.FindKeyWord(sText[sPos]); {Is it token begining with no letter?}
      l_tok := kwUnknown;
      while sKeyWord <> kwUnknown do
      begin
        l_tok := sKeyWord;
        sToken := sToken + sText[sPos];
        Inc(sPos);

        if ScannerEOF(AStatus) then
          break;

        sKeyWord := sqlKeyWords.FindKeyWord(sToken + sText[sPos]);
        sTokenType := stKeyWord;
      end;

      if Length(sToken) > 0 then
      begin
        sKeyWord := l_tok;
        sTokenType := stKeyWord;

        if sKeyWord = kwColon then { Is it parameter? }
        begin
          sToken := '';
          sTokenType := stParameter;
          sKeyWord := kwUnknown;
          while isLetter(sText[sPos]) do
          begin
            sToken := sToken + sText[sPos];
            Inc(sPos);
          end;
        end

        else if sKeyWord = kwConst then { Is it constant? }
        begin
          sToken := '';
          sTokenType := stConst;
          sKeyWord := kwUnknown;
          while isLetter(sText[sPos]) do
          begin
            sToken := sToken + sText[sPos];
            Inc(sPos);
          end;
        end

        else if sKeyWord = kwRemb then { Is it begin of comment? }
        begin
          l_token := sqlKeyWords.FindKeyWordByCode(kwReme);
          while not ScannerEOF(AStatus) do
          begin
            sToken := '';
            for i := 0 to Length(l_token) - 1 do
              sToken := sToken + sText[sPos + i];
            if sToken = l_token then
              break;
            Inc(sPos);
          end;

          if ScannerEOF(AStatus) then
            ShowParserError(AStatus, sbeNClRem, []);

          Inc(sPos, Length(l_token));

          ScannerGetNextToken(AStatus);
        end
      end

      else
      begin
        sToken := sText[sPos];
        ShowParserError(AStatus, sbeUnknSymbol, [sToken]);
      end
    end;

    if sTokenType = stKeyWord then
      sKeyWordType := sqlKeyWords.KeyWordType(sKeyWord);
  end;
end;


procedure ScannerPutBackToken(var AStatus: TsbScannerRes);
begin
  with AStatus do
  begin
    ScannerChangePosition(AStatus, sPos - Length(sToken));
    sWasBack := True;
  end;
end;


procedure ScannerChangePosition(var AStatus: TsbScannerRes; const APos: Integer);
begin
  with AStatus do
  begin
    sPos := APos;
    sWasBack := False;
  end;
end;


procedure ScannerCheckNextToken(var AStatus: TsbScannerRes; const AKeyWord: TsbKeyWord);
begin
  ScannerGetNextToken(AStatus);
  ScannerCheckToken(AStatus, AKeyWord);
end;


procedure ScannerCheckToken(var AStatus: TsbScannerRes; const AKeyWord: TsbKeyWord);
begin
  if AStatus.sKeyWord <> AKeyWord then
    ShowParserError(AStatus, sbeNExpToken, [sqlKeyWords.FindKeyWordByCode(AKeyWord), AStatus.sToken]);
end;


function  ScannerEOF(var AStatus: TsbScannerRes): Boolean;
begin
  Result := AStatus.sPos >= Length(AStatus.sText);
end;


procedure ScannerCheckEnd(var AStatus: TsbScannerRes);
begin
  if not ScannerEOF(AStatus) then
    ShowParserError(AStatus, sbeSyntaxError, [AStatus.sToken]);
end;


function ScannerSearchToken(var AStatus: TsbScannerRes; const AKeyWord: TsbKeyWord): Boolean;
var
  brCount: Integer;
begin
  brCount := 0;
  Result := False;
  while not ScannerEOF(AStatus) do
  begin
    ScannerGetNextToken(AStatus);

    if AStatus.sKeyWord = kwLbracket then
      Inc(brCount)
    else if AStatus.sKeyWord = kwRbracket then
      Dec(brCount);

    if AStatus.sKeyWord = AKeyWord then
      if brCount = 0 then
      begin
        ScannerPutBackToken(AStatus);
        Result := True;
        break;
      end;
  end;

  if brCount <> 0 then
    ShowParserError(AStatus, sbeNBalRB, []);
end;


function ScannerNextTokenIs(var AStatus: TsbScannerRes; const AKeyWord: TsbKeyWord): Boolean;
begin
  ScannerGetNextToken(AStatus);
  Result := (AStatus.sKeyWord = AKeyWord);
  ScannerPutBackToken(AStatus);
end;



{ TsbVMVarList }

function TsbVMVarList.AddVar(const AName: string): Integer;
begin
  SetLength(FList, Length(FList) + 1);
  Result := High(FList);
  FList[Result].Name := AnsiUpperCase(AName);
  FList[Result].Value := Null;
end;


procedure TsbVMVarList.Clear;
begin
  SetLength(FList, 0);
end;


destructor TsbVMVarList.Destroy;
begin
  Clear;
  inherited;
end;


function TsbVMVarList.GetVar(const AIndex: Integer): PTsbVMVar;
begin
  Result := Addr(FList[AIndex]);
end;


function TsbVMVarList.IndexOf(const AName: string): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := Low(FList) to High(FList) do
    if SameText(FList[i].Name, AName) then
    begin
      Result := i;
      break;
    end;
end;




initialization
  Randomize;
  sqlKeyWords := TsbKeyWords.Create;

finalization
  FreeAndNil(sqlKeyWords);
  FreeAndNil(FSQLConsts);

end.
