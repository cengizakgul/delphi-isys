unit rwParser;

interface

uses Classes, SysUtils, TypInfo, Forms, Windows, rwBasicClasses,
     rmTypes, rwTypes, rwMessages, rwRendering, Syncobjs, rwUtils, rwBasicUtils, Variants,
     rwEngineTypes, rwRTTI, rwVirtualMachine;


type

   TrwParser = class;

  {Type of tokens in handler text}

  TrwpTypeToken = (rwtUnknown, rwtKeyword, rwtSymbol, rwtNumber, rwtString);

  {Type of keywords}

  TrwpTypeKeyWord = (rwkUnknown, rwkEndOfText, rwkSemicolomn, rwkPlus,
    rwkMinus, rwkMul, rwkDivide, rwkLbracket,
    rwkRbracket, rwkEqual, rwkComma, rwkLess,
    rwkMore, rwkNeq, rwkLeq, rwkMeq, rwkRem,
    rwkRemb, rwkReme, rwkColon, rwkAssign,
    rwkSquareLBracket, rwkSquareRBracket,
    rwkPoint, rwkFunction,
    rwkProcedure, rwkEnd, rwkAnd, rwkOr,
    rwkNot, rwkVar, rwkInteger, rwkBoolean,
    rwkFloat, rwkString, rwkDate, rwkPointer,
    rwkCurrency, rwkVariant,
    rwkExponentiate, rwkIf,
    rwkThen, rwkElse, rwkElseIf, rwkEndIf, rwkArray,
    rwkFor, rwkTo, rwkDo,
    rwkEndFor, rwkWhile, rwkEndWhile,
    rwkMod, rwkDiv, rwkInherited,
    rwkNil, rwkExit, rwkBreak, rwkContinue);


  {Type of functions}

  TrwpTypeFunction = (rwfUnknown, rwfTrue, rwfFalse, rwfNull, rwfLength, rwfArrLength, rwfSetArrLength,
    rwfAsString, rwfAsInteger, rwfAsFloat, rwfAsCurrency, rwfAsDate, rwfAsBoolean, rwfAsVariant,
    rwfFormat, rwfSubstValue, rwfSubstSubStr, rwfPosSubStr,
    rwfCopySubStr, rwfRound, rwfTrunc,
    rwfToday, rwfTime, rwfNow, rwfSetTime, rwfChr, rwfOrd,
    rwfCreateObject, rwfDestroyObject, rwfIndexOfArrayElement, rwfUpperCase, rwfLowerCase,
    rwfTrimLeft, rwfTrimRight, rwfTrim, rwfDelSubStr, rwfShowError, rwfTypeOf,
    rwfSign, rwfAbs, rwfDesignerOpened,
    rwfDictionary, rwfSkipPage, rwfSetPrintPos, rwfGetPrintPos, rwfPageNumber,
    rwfLibComponents, rwfLibFunctions, rwfFileExists,
    rwfAppendASCIILine, rwfInsertASCIILine, rwfUpdateASCIILine, rwfDeleteASCIILine,
    rwfGetASCIILine, rwfASCIILineCount, rwfAbort, rwfRandomRange,
    rwfExternal, rwfTimeZoneOffset);


  {TrwpKeyWordRec is description of keyword}

  TrwpKeyWordRec = record
    Text: string[16];
    Code: TrwpTypeKeyWord;
  end;

   {TrwpKeyWords is list of keywords}

  TrwpKeyWords = class(TStringList)
  private
    function GetItem(AIndex: Integer): TrwpKeyWordRec;
  public
    property Items[index: Integer]: TrwpKeyWordRec read GetItem; default;
    constructor Create;
    function Add(AText: string; ACode: TrwpTypeKeyWord): Integer; reintroduce;
    function FindKeyWord(AToken: string): TrwpTypeKeyWord;
    function FindKeyWordByCode(AKeyWord: TrwpTypeKeyWord): string;
  end;


  { Embedded functions}

  TrwpRWFunction = class(TrwFunction)
  private
    FTypeFunct: TrwpTypeFunction;
    FExternalTypeFunct: Integer;
  public
    property TypeFunct: TrwpTypeFunction read FTypeFunct write FTypeFunct;
    property ExternalTypeFunct: Integer read FExternalTypeFunct write FExternalTypeFunct;
  end;


  TrwpRWFunctions = class(TrwFunctions)
  public
    procedure Initialize; override;
    function  AddFunc(const AGroup: String; const AHeader: String; AType: TrwpTypeFunction; AParser: TrwParser;
                      const AddInfo: PTrwExtFunctAddInfoRec = nil; const AExtType: Integer = 0): TrwpRWFunction;
    procedure SetFunctionAddInfo(AFunct: TrwpRWFunction; AddInfo: PTrwExtFunctAddInfoRec);
  end;



  {TrwVariables is list of local variables for handler}

  TrwVariables = class(TList)
  private
    function  GetItem(Index: Integer): PTrwVariable;
    function  GetVarInfo: string;
    procedure SetVarInfo(Value: string);

  public
    property Items[index: Integer]: PTrwVariable read GetItem; default;

    destructor Destroy; override;
    function  Add(AVarType: TrwVarTypeKind; AName: string; APointerType: String = ''): Integer; reintroduce;
    procedure Delete(Index: Integer); reintroduce;
    procedure Clear; override;
    function  IndexOf(const AVarName: string): Integer;
    property  VarInfo: string read GetVarInfo write SetVarInfo;
  end;


  TrwFactorType = (rftExpression, rftVariable, rftArrayElement, rftStrElement, rftConstArray, rftProperty);

  TrwVarType = record
    VarType:        TrwVarTypeKind;
    ClassInf:       IrwClass;
    Obj:            TObject;
    FactorType:     TrwFactorType;
    LeftAssignPart: Boolean;
  end;


  {TrwParser is class for implementing of event handler}

  TrwScannerRes = record
    sToken: string;
    sPrevToken: string;
    sTokenType: TrwpTypeToken;
    sKeyWord: TrwpTypeKeyWord;
    sPos: Integer;
    sWasBack: Boolean;
    sText: string;
  end;



  TrwDynArrayParam = array of ShortString;

  TrwSetKeyword = set of TrwpTypeKeyWord;


  TrwParser = class
  private
    FStatus: TrwScannerRes;
    FLocalVars: TrwVariables;
    FEventName: string;
    FComponent: TrwComponent;
    FComponentsOwner: TrwComponent;
    FLibComp: TrwComponent;
    FRootOwner: TrwComponent;
    FDebInfBegPos: Integer;
    FDebInfAddr: Integer;
    FDebugIndInfo: string;
    FDebInfLastLine: Integer;
    FDebInfLastCRLinePos: Integer;
    FDebFirstAddr: Integer;
    FExitsList: TList;
    FCurrBreakList: TList;
    FCurrContinueList: TList;
    FDefType: TrwVarTypeKind;
    FVarDefFinished: Boolean;

       {Scanner}
    procedure PutBackToken;
    procedure ParseBranch(AKeyWords: TrwSetKeyword);
    procedure ChangeScannersPosition(APos: LongInt);
    procedure GetNextToken;
    function  GetLineBegPos: Integer;

       {Syntax analizer of expressions}
    procedure StatExpression(var AType: TrwVarType; ALeftAssignPart: Boolean = False; ADefType: TrwVarTypeKind = rwvUnknown);
    procedure StatExpressionLevel2(var AType: TrwVarType);
    procedure StatExpressionLevel2_1(var AType: TrwVarType);
    procedure StatExpressionLevel3(var AType: TrwVarType);
    procedure StatExpressionLevel4(var AType: TrwVarType);
    procedure StatExpressionLevel4_1(var AType: TrwVarType);
    procedure StatExpressionLevel4_2(var AType: TrwVarType);
    procedure StatExpressionLevel4_3(var AType: TrwVarType);
    procedure StatExpressionLevel5(var AType: TrwVarType);
    procedure StatExpressionLevel6(var AType: TrwVarType);
    procedure StatExpressionLevel7(var AType: TrwVarType);
    function  DefineMathOperType(const AType1, AType2: TrwVarTypeKind): TrwVarTypeKind;

    procedure Error(ACode: Integer);
    procedure StartPars(const AExtVarDef: string; ActiveVM: TrwVirtualMachine);
    procedure CheckNextToken(AKeyWord: TrwpTypeKeyWord);
    procedure CheckNextTokenType(ATokenType: TrwpTypeToken);
    procedure CheckType(const AType: TrwVarType; const ATypes: array of TrwVarTypeKind);
    procedure CheckTypeCompatibility(const AType1: TrwVarType; const AType2: TrwVarType);
    procedure ParseStatement;
    procedure Identificator(AIdentificator: string; var AType: TrwVarType; AFirstStep: Boolean = True);
    procedure PutType(var AType: TrwVarType; const AVar: TrwVariable);
    procedure FuncAsType(const AType: TrwVarTypeKind; const ANullConv: Boolean);

      {Realization of statements}
    procedure StatVariableDef(AVarName: string = '');
    procedure StatIf;
    procedure StatFor;
    procedure StatWhile;
    procedure StatExit;
    procedure StatBreak;
    procedure StatContinue;
    procedure StatInherited;
    procedure StatAssign;
    procedure StatConstArray(var AType: TrwVarType);

    procedure AddCode(const ACodes: array of variant);
    procedure EditCode(const AAddr: Integer; const ACode: Variant);
    function  CodeLastAddr: Integer;
    procedure AddDebInf(const Addr, BegPos, EndPos: Integer);
    procedure AddDebSymbInf;
    function  AddLocalVar(AVarType: TrwVarTypeKind; AName: string; APointerType: String = ''): Integer;
  public
    constructor Create;
    destructor Destroy; override;
    function   ValueByExpressionForDebug(AExpression: string): Variant;
  end;


procedure SetCompiledCode(const ACode: PTrwCode; const AEntryPointOffset: Integer);
procedure SetCompilingPointer(const P: Integer);
procedure SetDebInf(const ACode: PTrwDebInf);
procedure SetDebInfPointer(const P: Integer);
procedure SetDebSymbInf(const ACode: PTrwDebInf);
procedure SetDebSymbInfPointer(const P: Integer);
function GetCompiledCode: PTrwCode;
function GetEntryPointOffset: Integer;
function GetCompilingPointer: Integer;
function GetDebInf: PTrwDebInf;
function GetDebInfPointer: Integer;
function GetDebSymbInf: PTrwDebInf;
function GetDebSymbInfPointer: Integer;

procedure SetCodeInsightLocVars(const AVar: TrwVariables);
procedure SetCodeInsightFuncParams(const AParams: TrwFuncParams);
function GetCodeInsightInfo: TrwVarType;
function GetCodeInsightLocVars: TrwVariables;
function GetCodeInsightFuncParams: TrwFuncParams;

procedure CompileHandler(const AComponent: TrwComponent; const AText: string; const AExtVarDef: String = ''; ActiveVM: TrwVirtualMachine = nil);
procedure CompileFunction(const AText: string);
procedure CompileWatchList(const AText: string; AVarInfo: string; const AComponent: TrwComponent; ActiveVM: TrwVirtualMachine = nil);

procedure ScannerGetNextToken(var AStatus: TrwScannerRes);
procedure ScannerChangePosition(var AStatus: TrwScannerRes; const APos: Integer);
procedure ScannerPutBackToken(var AStatus: TrwScannerRes);
procedure ScannerCheckNextToken(var AStatus: TrwScannerRes; const AKeyWord: TrwpTypeKeyWord);
procedure ScannerCheckNextTokenType(var AStatus: TrwScannerRes; const ATokenType: TrwpTypeToken);

function  ParseFunctionHeader(const AText: string; const AParams: TrwFuncParams;
                              var AParBegPos, AParEndPos, AEndPos: Integer): string;

function RWFunctions: TrwpRWFunctions;
function KeyWords: TrwpKeyWords;

procedure InitThreadVars_rwParser;


implementation

uses rwReport, rwCommonClasses, rwDebugInfo, rwEngine;

var
  FRWFunctionsSrc: TrwpRWFunctions = nil;
  FKeyWordsSrc: TrwpKeyWords = nil;

threadvar
  FRWFunctions: TrwpRWFunctions;
  FKeyWords: TrwpKeyWords;
  gtvCompiledCode: PTrwCode;
  gtvCompilingPointer: Integer;
  gtvDebInf: PTrwDebInf;
  gtvDebInfPointer: Integer;
  gtvDebSymbInf: PTrwDebInf;
  gtvDebSymbInfPointer: Integer;
  gtvCodeInsightInfo: TrwVarType;
  gtvEntryPointOffset: Integer;

  gtvCodeInsightLocalVars: TrwVariables;
  gtvCodeInsightFuncParams: TrwFuncParams;


procedure InitThreadVars_rwParser;
begin
  FRWFunctions := nil;
  FKeyWords := nil;
  gtvCompiledCode := nil;
  gtvCompilingPointer := 0;
  gtvDebInf := nil;
  gtvDebInfPointer := 0;
  gtvDebSymbInf := nil;
  gtvDebSymbInfPointer := 0;
  gtvCodeInsightLocalVars := nil;
  gtvCodeInsightFuncParams := nil;
end;



procedure SetCompiledCode(const ACode: PTrwCode; const AEntryPointOffset: Integer);
begin
  gtvCompiledCode := ACode;
  gtvEntryPointOffset := AEntryPointOffset;
end;

procedure SetCompilingPointer(const P: Integer);
begin
  gtvCompilingPointer := P;
end;

procedure SetDebInf(const ACode: PTrwDebInf);
begin
 gtvDebInf := ACode;
end;

procedure SetDebInfPointer(const P: Integer);
begin
  gtvDebInfPointer := P;
end;

procedure SetDebSymbInf(const ACode: PTrwDebInf);
begin
  gtvDebSymbInf := ACode;
end;

procedure SetDebSymbInfPointer(const P: Integer);
begin
  gtvDebSymbInfPointer := P;
end;


function GetCompiledCode: PTrwCode;
begin
  Result := gtvCompiledCode;
end;

function GetEntryPointOffset: Integer;
begin
  Result := gtvEntryPointOffset;
end;

function GetCompilingPointer: Integer;
begin
  Result := gtvCompilingPointer;
end;

function GetDebInf: PTrwDebInf;
begin
  Result := gtvDebInf;
end;

function GetDebInfPointer: Integer;
begin
  Result := gtvDebInfPointer;
end;

function GetDebSymbInf: PTrwDebInf;
begin
  Result := gtvDebInf;
end;

function GetDebSymbInfPointer: Integer;
begin
  Result := gtvDebSymbInfPointer;
end;

function GetCodeInsightInfo: TrwVarType;
begin
  Result := gtvCodeInsightInfo;
end;

procedure SetCodeInsightLocVars(const AVar: TrwVariables);
begin
  gtvCodeInsightLocalVars := AVar;
end;

function GetCodeInsightLocVars: TrwVariables;
begin
  Result := gtvCodeInsightLocalVars;
end;

function GetCodeInsightFuncParams: TrwFuncParams;
begin
  Result := gtvCodeInsightFuncParams;
end;

procedure SetCodeInsightFuncParams(const AParams: TrwFuncParams);
begin
  gtvCodeInsightFuncParams := AParams;
end;



function RWFunctions: TrwpRWFunctions;
begin
  if not Assigned(FRWFunctions) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(FRWFunctionsSrc) then
      begin
        FRWFunctionsSrc := TrwpRWFunctions.Create(nil, TrwpRWFunction);
        FRWFunctionsSrc.Initialize;
      end;
      FRWFunctions := FRWFunctionsSrc;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := FRWFunctions;
end;


function KeyWords: TrwpKeyWords;
begin
  if not Assigned(FKeyWords) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(FKeyWordsSrc) then
        FKeyWordsSrc := TrwpKeyWords.Create;
      FKeyWords := FKeyWordsSrc;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := FKeyWordsSrc;
end;


function KeyWordToVarType(AKeyWord: TrwpTypeKeyWord): TrwVarTypeKind;
begin
  case AKeyWord of
    rwkInteger: Result := rwvInteger;
    rwkBoolean: Result := rwvBoolean;
    rwkFloat:   Result := rwvFloat;
    rwkCurrency:Result := rwvCurrency;
    rwkString:  Result := rwvString;
    rwkDate:    Result := rwvDate;
    rwkArray:   Result := rwvArray;
    rwkPointer:  Result := rwvPointer;
    rwkVariant: Result := rwvVariant;
  else
    Result := rwvUnknown;
  end;
end;


procedure CompileHandler(const AComponent: TrwComponent; const AText: string; const AExtVarDef: String = ''; ActiveVM: TrwVirtualMachine = nil);
var
  lParser: TrwParser;
  fl_err: Boolean;
begin
  fl_err :=False;
  lParser := TrwParser.Create;

  with lParser do
  try
    FStatus.sText := AText;
    try
      FComponent := AComponent;
      StartPars(AExtVarDef, ActiveVM);
    except
      on ErwParserError do
        raise;

      on E: Exception do
      begin
        FStatus.sToken := E.Message;
        fl_err := True;
      end;
    end;

    if fl_err then
      Error(20);

  finally
    Free;
  end;
end;


procedure CompileFunction(const AText: string);
begin
  CompileHandler(nil, AText);
end;


procedure CompileWatchList(const AText: string; AVarInfo: string; const AComponent: TrwComponent; ActiveVM: TrwVirtualMachine = nil);
begin
  if Length(AVarInfo) = 0 then
    AVarInfo := ' ';
  CompileHandler(AComponent, AText, AVarInfo, ActiveVM);
end;




  {TrwVariables}

destructor TrwVariables.Destroy;
begin
  Clear;
  inherited;
end;

procedure TrwVariables.Clear;
var
  i: Integer;
  lRec: PTrwVariable;
begin
  for i := 0 to Count - 1 do
  begin
    lRec := Items[i];
    Dispose(lRec);
  end;

  inherited Clear;
end;

function TrwVariables.GetItem(Index: Integer): PTrwVariable;
begin
  Result := PTrwVariable(inherited Items[Index]);
end;

function TrwVariables.Add(AVarType: TrwVarTypeKind; AName: string; APointerType: String = ''): Integer;
var
  lRec: PTrwVariable;
begin
  New(lRec);

  lRec^.VarType := AVarType;
  lRec^.Name := AName;
  lRec^.PointerType := APointerType;
  lRec^.Reference := False;

  Result := inherited Add(Pointer(lRec));
end;

procedure TrwVariables.Delete(Index: Integer);
var
  lRec: PTrwVariable;
begin
  lRec := Items[Index];

  Dispose(lRec);

  inherited Delete(Index);
end;


function TrwVariables.GetVarInfo: string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Count - 1 do
    Result := Result + Items[i].Name + ',' + IntToStr(Ord(Items[i].VarType)) + ',' +
      Items[i].PointerType + ',' + IntToStr(Integer(Items[i].Reference)) + ',';
end;


procedure TrwVariables.SetVarInfo(Value: string);
var
  V: PTrwVariable;
begin
  Clear;
  Value := Trim(Value);
  while Length(Value) > 0 do
  begin
    V := Items[Add(rwvVariant, '')];
    V.Name :=  GetNextStrValue(Value, ',');
    V.VarType :=  TrwVarTypeKind(StrToInt(GetNextStrValue(Value, ',')));
    V.PointerType := GetNextStrValue(Value, ',');
    V.Reference :=  Boolean(StrToInt(GetNextStrValue(Value, ',')));
  end;
end;


function TrwVariables.IndexOf(const AVarName: string): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to Count -1 do
    if SameText(Items[i].Name, AVarName) then
    begin
      Result := i;
      break;
    end;
end;



{TrwParser}

constructor TrwParser.Create;
begin
  inherited Create;

  FLocalVars := TrwVariables.Create;
  FExitsList := TList.Create;
end;

destructor TrwParser.Destroy;
begin
  FLocalVars.Free;
  FExitsList.Free;
  inherited;
end;


procedure TrwParser.StartPars(const AExtVarDef: string; ActiveVM: TrwVirtualMachine);
var
  i, lResultIndex: Integer;
  P: TrwFuncParams;
  lParBegPos, lParEndPos, lEndPos, lBegFor: Integer;
  O: TObject;
begin
  FExitsList.Clear;
  FLocalVars.Clear;
  lResultIndex := -1;
  FDebInfBegPos := 0;
  FDebInfAddr := gtvCompilingPointer;


  // define important variabled (Self, Owner and RootOwner)
  if Assigned(FComponent) then
  begin
    FDebugIndInfo := GetComponentPath(FComponent);

    if Debugging and FComponent.IsLibComponent and AnsiSameText(FComponent.PClassName, DebugInfo.CurrentLibClass) or
       FComponent.IsVirtualOwner then
      FComponentsOwner := FComponent
    else
      FComponentsOwner := FComponent.VirtualOwner;

    FRootOwner := FComponent.RootOwner;
  end

  else
  begin
    FComponentsOwner := nil;
    FRootOwner := nil;
    FDebugIndInfo := cParserLibFunct;
  end;


  if Length(AExtVarDef) = 0 then
  begin
    if Assigned(FComponent) then    //Self
    begin
      if FComponent is TrwGlobalVarFunct then
        AddLocalVar(rwvPointer, '')            //Just take this place. There is no Self here
      else
        AddLocalVar(rwvPointer, cParserSelf, FComponent.PClassName);

      AddLocalVar(rwvPointer, '');  //Owner

      if Assigned(FRootOwner) then  //Root Owner. In Library Compiling Mode it points to owner component itself
        AddLocalVar(rwvPointer, cParserRootOwner, FRootOwner.PClassName)
      else
        AddLocalVar(rwvPointer, '');

      AddCode([rmoNewVar, rwvPointer]);  //self
      FuncAsType(rwvPointer, False);
      AddCode([rmoMOVlocVarA, 0]);

      if FComponentsOwner = FComponent then //owner
        AddCode([rmoNewVar, rwvPointer,
                 rmoMOVAlocVar, 0,
                 rmoMOVlocVarA, 1])
      else
      begin
        AddCode([rmoNewVar, rwvPointer,
                 rmoMOVAlocVar, 0,
                 rmoPushA,
                 rmoPUSHConst, 0,
                 rmoPUSHconst, rwvPointer,
                 rmoPUSHconst, 'Owner',
                 rmoCallEmbFunc, refObjectMethod,
                 rmoMOVlocVarA, 1]);
      end;

      AddCode([rmoNewVar, rwvPointer,
               rmoMOVAlocVar, 0,
               rmoPushA,
               rmoPUSHConst, 0,
               rmoPUSHconst, rwvPointer,
               rmoPUSHconst, 'RootOwner',
               rmoCallEmbFunc, refObjectMethod,
               rmoMOVlocVarA, 2]);

      if Assigned(FRootOwner) and FRootOwner.LibDesignMode then  // There is LIBCOMP only in Library Compiling Mode
      begin
        FLibComp := FComponent;
        // go up until RootOwner met
        i := 0;
        while FLibComp <> FRootOwner do
        begin
          FLibComp := FLibComp.VirtualOwner;
          Inc(i);
        end;

        AddCode([rmoMOVAlocVar, 0]);

        if i > 0 then
        begin
          AddCode([rmoMOVBconst, i]);
          lBegFor := CodeLastAddr + 1;

          // for loop "i" times. Doing Owner.Owner ...
          AddCode([rmoPushB,
                   rmoPushA,
                   rmoPUSHConst, 0,
                   rmoPUSHconst, rwvPointer,
                   rmoPUSHconst, 'Owner',
                   rmoCallEmbFunc, refObjectMethod,

                   rmoPopB,
                   rmoDecB,
                   rmoPushA,
                   rmoMOVAconst, 0,
                   rmoCmpAeB,
                   rmoPopA
                  ]);
          AddCode([rmoJumpIfFalse, lBegFor - (CodeLastAddr + 2)
                  ]);
        end;

        AddLocalVar(rwvPointer, cParserLibComp, FLibComp.PClassName);  //LibComp
        AddCode([rmoNewVar, rwvPointer,
                 rmoMOVlocVarA, 3]);
      end

      else
        FLibComp := nil;
    end;

    P := TrwFuncParams.Create(TrwFuncParam);
    try
      FEventName := ParseFunctionHeader(FStatus.sText, P, lParBegPos, lParEndPos, lEndPos);
      ChangeScannersPosition(lEndPos);

      for i := P.Count - 1 downto 0 do
      begin
        AddLocalVar(P[i].Descriptor.VarType, P[i].Descriptor.Name, P[i].Descriptor.PointerType);
        FLocalVars[FLocalVars.Count-1].Reference := P[i].Descriptor.Reference;

        AddCode([rmoNewVar, P[i].Descriptor.VarType]);
        if not P[i].Descriptor.Reference and (P[i].Descriptor.VarType <> rwvVariant) then
          FuncAsType(P[i].Descriptor.VarType, False)
        else
          AddCode([rmoPopA]);
        AddCode([rmoMOVlocVarA, FLocalVars.Count-1]);
      end;

      if P.Result.VarType <> rwvUnknown then
      begin
        AddLocalVar(P.Result.VarType, 'Result', P.Result.PointerType);
        lResultIndex := FLocalVars.Count-1;
        AddCode([rmoNewVar, P.Result.VarType]);
      end;

    finally
      P.Free;
    end;
  end

  else
  begin
    FLocalVars.VarInfo := AExtVarDef;

    // RM only. Assign Self, Owner, RootOwner from current VM state
    if Assigned(ActiveVM) and not Assigned(FComponent) and (FLocalVars.IndexOf(cParserSelf) = 0) then
    begin
      O := TObject(Pointer(Integer(ActiveVM.ReportCode^[ActiveVM.CurrHeapTop])));
      if Assigned(O) then
        FComponent := O as TrwComponent;

      O := TObject(Pointer(Integer(ActiveVM.ReportCode^[ActiveVM.CurrHeapTop + 1])));
      if Assigned(O) then
        FComponentsOwner := O as TrwComponent;

      O := TObject(Pointer(Integer(ActiveVM.ReportCode^[ActiveVM.CurrHeapTop + 2])));
      if Assigned(O) then
        FRootOwner := O as TrwComponent;
    end;

    ChangeScannersPosition(1);
  end;

//  AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);  Function header is not for debugging

  FDebInfLastCRLinePos := MaxInt;
  FDebFirstAddr := gtvDebInfPointer;

  FVarDefFinished := False;

  GetNextToken;
  while (FStatus.sKeyWord <> rwkEnd) do
  begin
    ParseStatement;
    GetNextToken;
  end;

  PutBackToken;
  FDebInfBegPos := GetLineBegPos;
  FDebInfAddr := gtvCompilingPointer;
  GetNextToken;

  if lResultIndex <> -1 then
    AddCode([rmoMovAlocVar, lResultIndex]);

  AddCode([rmoRet]);

  AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);
  AddDebSymbInf;

  for i := 0 to FExitsList.Count -1 do
    EditCode(Integer(FExitsList[i]), CodeLastAddr - Integer(FExitsList[i]));
end;


procedure TrwParser.CheckNextToken(AKeyWord: TrwpTypeKeyWord);
begin
  ScannerCheckNextToken(FStatus, AKeyWord);
end;


procedure TrwParser.CheckNextTokenType(ATokenType: TrwpTypeToken);
begin
  ScannerCheckNextTokenType(FStatus, ATokenType);
end;


procedure TrwParser.CheckType(const AType: TrwVarType; const ATypes: array of TrwVarTypeKind);
var
  i: Integer;
begin
  if AType.VarType = rwvVariant then
    Exit;

  for i := Low(ATypes) to High(ATypes) do
    if AType.VarType = ATypes[i] then
      Exit;

  case ATypes[Low(ATypes)] of
    rwvInteger:  Error(10);
    rwvBoolean:  Error(12);
    rwvString:   Error(13);
    rwvFloat:    Error(11);
    rwvDate:     Error(14);
    rwvArray:    Error(16);
    rwvPointer:   Error(15);
  end;
end;


procedure TrwParser.CheckTypeCompatibility(const AType1: TrwVarType; const AType2: TrwVarType);
var
  f: Boolean;
begin
  if (AType1.VarType = rwvVariant) or (AType2.VarType = rwvVariant) then
    Exit;

  case AType1.VarType of
    rwvInteger:   f := AType2.VarType in [rwvInteger, rwvBoolean];

    rwvBoolean,
    rwvString,
    rwvArray:     f := (AType1.VarType = AType2.VarType);

    rwvDate:      f := AType2.VarType in [rwvDate, rwvFloat, rwvCurrency, rwvInteger];

    rwvFloat,
    rwvCurrency:  f := AType2.VarType in [rwvFloat, rwvCurrency, rwvInteger, rwvDate];

    rwvPointer:    begin
                    f := (AType2.VarType = rwvPointer);
                    if f then
                    begin
                      if Assigned(AType1.ClassInf) then
                      begin
                        f := Assigned(AType2.ClassInf);
                        if f then
                          f := AType2.ClassInf.DescendantOf(AType1.ClassInf);
                      end
                    end

                    else
                      f := (AType2.VarType = rwvArray) and not Assigned(AType1.ClassInf) ;
                  end;
  else
    f := False;
  end;

  if not f then
    Error(23);
end;


procedure TrwParser.ParseStatement;
var
  lPos: Integer;
  t: TrwVarType;
  kw: TrwpTypeKeyWord;
begin
  PutBackToken;
  FDebInfBegPos := GetLineBegPos;
  FDebInfAddr := gtvCompilingPointer;
  GetNextToken;

  if (FStatus.sTokenType = rwtUnknown) then
  begin
    FVarDefFinished := True;

    PutBackToken;
    lPos := FStatus.sPos;
    repeat
      GetNextToken;
    until FStatus.sKeyWord in [rwkSemicolomn, rwkEndOfText, rwkAssign];

    ChangeScannersPosition(lPos);

    if (FStatus.sKeyWord = rwkAssign) then
      StatAssign

    else
    begin
      GetNextToken;
      t.LeftAssignPart := False;
      try
        Identificator(FStatus.sToken, t);
      finally
        gtvCodeInsightInfo := t;
      end;

      AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);
    end;
  end

  else if (FStatus.sTokenType = rwtKeyWord) then
  begin
    kw := FStatus.sKeyWord;
    case FStatus.sKeyWord of
      rwkVar:       StatVariableDef;
      rwkIf:        StatIf;
      rwkFor:       StatFor;
      rwkWhile:     StatWhile;
      rwkExit:      StatExit;
      rwkBreak:     StatBreak;
      rwkContinue:  StatContinue;
      rwkInherited: StatInherited;
      rwkEndOfText: Error(26);
    else
      Error(1);
    end;

    if kw <> rwkVar then
      FVarDefFinished := True;

  end
  else
  begin
    if (FStatus.sKeyWord <> rwkSemicolomn) then
      Error(1)
    else
      PutBackToken;

    FVarDefFinished := True;
  end;

  CheckNextToken(rwkSemicolomn);
end;


procedure TrwParser.StatAssign;
var
  t, t2: TrwVarType;
begin
  StatExpression(t, True);
  CheckNextToken(rwkAssign);
  if t.FactorType = rftExpression then
    Error(31);

  AddCode([rmoPUSHB]);

  case t.FactorType of
    rftVariable:
      begin
        StatExpression(t2);
        CheckTypeCompatibility(t, t2);

        if t.VarType <> rwvVariant then
        begin
          AddCode([rmoPushA]);
          FuncAsType(t.VarType, False);
        end;

        AddCode([rmoPOPB,
                 rmoMOVrefBA]);
      end;

    rftArrayElement:
      begin
        AddCode([rmoPUSHA]);
        StatExpression(t2);
        CheckTypeCompatibility(t, t2);
        AddCode([rmoPUSHA,
                 rmoCallEmbProc, repSetArrayElement]);
      end;

    rftStrElement:
      begin
        AddCode([rmoPUSHA]);
        StatExpression(t2);
        CheckTypeCompatibility(t, t2);
        AddCode([rmoPUSHA,
                 rmoCallEmbProc, repSetStrElement]);
      end;


    rftProperty:
      begin
        AddCode([rmoPUSHA]);
        StatExpression(t2);
        CheckTypeCompatibility(t, t2);
        AddCode([rmoPUSHA,
                 rmoCallEmbProc, repSetObjectProp]);
      end;
  end;

  AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);
end;


procedure TrwParser.Identificator(AIdentificator: string; var AType: TrwVarType; AFirstStep: Boolean = True);
var
  i: Integer;
  lTempObject: TObject;

  function ParseComponentName: Boolean;
  var
    i: Integer;
  begin
    Result := False;

    if not (AType.Obj is TrwComponent) then
      Exit;

    i := TrwComponent(AType.Obj).FindVirtualChild(AIdentificator);
    if i = -1 then
      Exit

    else
    begin
      if AFirstStep then
        AddCode([rmoMOVAlocVar, 1,
                 rmoPUSHA])
      else
        if AType.LeftAssignPart then
          AddCode([rmoPUSHB])
        else
          AddCode([rmoPUSHA]);

      AddCode([rmoPUSHconst, i]);
      AddCode([rmoCallEmbFunc, refFindObjectByIndex]);

      if AType.LeftAssignPart then
        AddCode([rmoMOVBA]);

      AType.VarType := rwvPointer;
      AType.Obj := TrwComponent(AType.Obj).GetVirtualChild(i);


      if AType.Obj is TrwComponent then
        AType.ClassInf := rwRTTIInfo.FindClass(TrwComponent(AType.Obj).PClassName)
      else
        AType.ClassInf := rwRTTIInfo.FindClass(AType.Obj.ClassName);

      Result := True;
    end;
  end;


  function ParseFuncParams(const AParams: TrwFuncParams): Integer;
  var
    t, t1: TrwVarType;
  begin
    Result := 0;
    if Assigned(gtvCodeInsightFuncParams) then
    begin
      gtvCodeInsightFuncParams.Assign(AParams);
      gtvCodeInsightFuncParams.Tag := 1;
    end;

    GetNextToken;
    if (FStatus.sKeyWord = rwkLBracket) then
    begin
      GetNextToken;
      if (FStatus.sKeyWord <> rwkRBracket) then
      begin
        PutBackToken;
        repeat
          Inc(Result);

          if Assigned(AParams) then
          begin
            if Result > AParams.Count then
              Error(24);
            PutType(t1, AParams[Result-1].Descriptor^);
            StatExpression(t, (t1.FactorType = rftVariable));
            CheckTypeCompatibility(t1, t);

            if t1.FactorType = rftVariable then
              if t.FactorType <> rftVariable then
                Error(24)
              else
                AddCode([rmoPUSHB])
            else
              AddCode([rmoPUSHA]);
          end
          else
          begin
            StatExpression(t);
            AddCode([rmoPUSHA]);
          end;
          GetNextToken;
        until (FStatus.sKeyWord <> rwkComma);
        PutBackToken;
      end
      else
        PutBackToken;

      CheckNextToken(rwkRBracket);
    end
    else
      PutBackToken;

    if Assigned(AParams) and (Result <> AParams.Count) then
      Error(24);

    if Assigned(gtvCodeInsightFuncParams) then
    begin
      gtvCodeInsightFuncParams.Clear;
      gtvCodeInsightFuncParams.Tag := 0;
    end;
  end;


  procedure ParseIndexProp;
  var
    t: TrwVarType;
    M: TrwPublishedMethod;
  begin
    GetNextToken;
    if (FStatus.sKeyWord = rwkSquareLBracket) and (AType.VarType = rwvPointer) and
       Assigned(AType.ClassInf) then
    begin
      M := PublishedMethods.IndexOf(AType.Obj.ClassType, 'Items');

      if not Assigned(M) then
        Error(36);

      if AType.LeftAssignPart then
      begin
        AddCode([rmoPUSHB,
                 rmoPUSHA,
                 rmoCallEmbFunc, refObjectPropValue]);
      end;
      AddCode([rmoPushA]);

      StatExpression(t);
      CheckType(t, [rwvInteger]);
      CheckNextToken(rwkSquareRBracket);

      AddCode([rmoPushA]);
      AddCode([rmoPUSHConst, 1,
               rmoPUSHconst, M.Params.Result.VarType,
               rmoPUSHconst, 'Items',
               rmoCallEmbFunc, refObjectMethod]);

      PutType(AType, M.Params.Result^);

      if AType.LeftAssignPart and (AType.VarType in [rwvPointer, rwvVariant])then
        AddCode([rmoMOVBA]);
    end
    else
      PutBackToken;
  end;


  procedure ParseArrElement;
  var
    t: TrwVarType;
  begin
    GetNextToken;
    if (FStatus.sKeyWord = rwkSquareLBracket) then
    begin
      CheckType(AType, [rwvArray, rwvString]);

      if AType.VarType = rwvArray then
        AType.FactorType := rftArrayElement
      else
        AType.FactorType := rftStrElement;

      if AType.LeftAssignPart or (AType.VarType = rwvArray) then
        AddCode([rmoPushB])
      else
        AddCode([rmoPushA]);

      StatExpression(t);
      CheckType(t, [rwvInteger]);
      CheckNextToken(rwkSquareRBracket);

      if AType.LeftAssignPart then
        AddCode([rmoPopB])

      else
      begin
        AddCode([rmoPushA,
                 rmoCallEmbFunc]);

        if AType.VarType = rwvArray then
          AddCode([refArrayElement])
        else
          AddCode([refStrElement]);
      end;

       if AType.VarType = rwvArray then
         AType.VarType := rwvVariant

       else
         AType.VarType := rwvString;
    end
    else
      PutBackToken;
  end;


  function ParseEvent(const AComponent: TrwComponent; const AContext: Integer): Boolean;
  var
    i, j: Integer;
    Ev: TrwEvent;
  begin
    i := AComponent.EventsList.IndexOf(AIdentificator);
    if i <> - 1 then
    begin
      if AContext <> 0 then
        AddCode([rmoMOVAlocvar, AContext,
                 rmoPUSHA])
      else
        if AType.LeftAssignPart then
          AddCode([rmoPUSHB])
        else
          AddCode([rmoPUSHA]);

       if (AContext <> 0) and FComponentsOwner.IsVirtualOwner then
         j := -3
       else
         j := AComponent.ComponentIndex;

       if AComponent is TrwGlobalVarFunct then
         AddCode([rmoPUSHconst, j,
                  rmoCallEmbFunc, refFindObjectByIndex,
                  rmoPUSHA]);

       Ev := AComponent.EventsList[i];
       i := ParseFuncParams(Ev.Params);
       AddCode([rmoPUSHconst, i,
                rmoCallEvent, Ev.Name]);
       PutType(AType, Ev.Params.Result^);

       if AType.LeftAssignPart and (Ev.Params.Result.VarType <> rwvUnknown) then
         AddCode([rmoMOVBA]);

       AType.FactorType := rftExpression;
       Result := True;
    end
    else
      Result := False;
  end;


  function ParseVarFunc(const AGlVarFunc: TrwGlobalVarFunct; const AContext: Integer; ANonVis, AInhNonVis: TrwVarVisibilities): Boolean;
  var
    i: Integer;
  begin
    // AContext defines: 0  Current context
    //                   1  Owner
    //                   2  RootOwner
    //                   3  LibComp

    i := AGlVarFunc.Variables.IndexOf(AIdentificator);
    if i <> -1 then
    begin
      //variables

      if not AGlVarFunc.Variables[i].InheritedItem and (AGlVarFunc.Variables[i].Visibility in ANonVis)
         or
         AGlVarFunc.Variables[i].InheritedItem and (AGlVarFunc.Variables[i].Visibility in AInhNonVis)
      then
      begin
        Result := False;
        Exit;
      end;

      PutType(AType, AGlVarFunc.Variables[i].PVarRec^);

      if AContext <> 0 then
      begin
        AddCode([rmoMOVAlocvar, AContext,
                 rmoPUSHA]);
        if not AType.LeftAssignPart and (AType.VarType = rwvArray) then
          AddCode([rmoPUSHA]);
      end

      else
        if AType.LeftAssignPart then
          AddCode([rmoPUSHB])
        else
        begin
          AddCode([rmoPUSHA]);
          if AType.VarType = rwvArray then
            AddCode([rmoPUSHA]);
        end;

      if AType.LeftAssignPart or (AType.VarType = rwvArray) then
        AddCode([rmoPushConst, i,
                 rmoCallEmbFunc, refAddrGlobVar,
                 rmoMOVBA]);

      if not AType.LeftAssignPart then
        AddCode([rmoMOVAglobVar, i]);

      AType.FactorType := rftVariable;
      Result := True;
      Exit;
    end;

    //functions
    Result := ParseEvent(AGlVarFunc, AContext);
  end;


  function ParseLibFunctions: Boolean;
  var
    Fnc: TrwLibFunction;
  begin
    Fnc := TrwLibFunction(SystemLibFunctions.FindFunction(AIdentificator));
    if Assigned(Fnc) then
    begin
      ParseFuncParams(Fnc.Params);
      PutType(AType, Fnc.Params.Result^);
      AddCode([rmoCallLibFunc, AIdentificator]);
      if AType.LeftAssignPart and (AType.VarType in [rwvPointer, rwvVariant])then
        AddCode([rmoMOVBA]);
        Result := True;
    end
    else
      Result := False;
  end;


  function ParseEmbeddedFunctions: Boolean;
  var
    Fnc: TrwFunction;
    ft: TrwVMEmbeddedFunc;
    pt: TrwVMEmbeddedProc;
  begin
    Fnc := RWFunctions.FindFunction(AIdentificator);
    if Assigned(Fnc) then
    begin
      ParseFuncParams(Fnc.Params);
      PutType(AType, Fnc.Params.Result^);
      pt := repUnknown;
      ft := refUnknown;
      case TrwpRWFunction(Fnc).TypeFunct of
        rwfTrue:         AddCode([rmoMOVAconst, True]);
        rwfFalse:        AddCode([rmoMOVAconst, False]);
        rwfNull:         AddCode([rmoMOVAconst, Null]);

        rwfAsString:     FuncAsType(rwvString, True);
        rwfAsInteger:    FuncAsType(rwvInteger, True);
        rwfAsFloat:      FuncAsType(rwvFloat, True);
        rwfAsCurrency:   FuncAsType(rwvCurrency, True);
        rwfAsVariant:    FuncAsType(rwvVariant, True);
        rwfAsDate:       FuncAsType(rwvDate, True);
        rwfAsBoolean:    FuncAsType(rwvBoolean, True);

        rwfSetArrLength: pt := repSetArrLength;
        rwfSetTime:      pt := repSetTime;
        rwfShowError:    pt := repShowError;
        rwfAbort:        pt := repAbort;
        rwfDestroyObject: pt := repDestroyObject;

        rwfLength:       ft := refLength;
        rwfArrLength:    ft := refArrLength;
        rwfFormat:       ft := refFormat;
        rwfSubstValue:   ft := refSubstFldValue;
        rwfSubstSubStr:  ft := refSubstSubStr;
        rwfPosSubStr:    ft := refPosSubStr;
        rwfCopySubStr:   ft := refCopySubStr;
        rwfDelSubStr:    ft := refDelSubStr;
        rwfRound:        ft := refRound;
        rwfRandomRange:  ft := refRandomRange;
        rwfTrunc:        ft := refTrunc;
        rwfToday:        ft := refToday;
        rwfTime:         ft := refTime;
        rwfNow:          ft := refNow;
        rwfChr:          ft := refChr;
        rwfOrd:          ft := refOrd;
        rwfCreateObject: ft := refCreateObject;
        rwfIndexOfArrayElement: ft := refIndexOfArrayElement;
        rwfUpperCase:    ft := refUpperCase;
        rwfLowerCase:    ft := refLowerCase;
        rwfTrimLeft:     ft := refTrimLeft;
        rwfTrimRight:    ft := refTrimRight;
        rwfTrim:         ft := refTrim;
        rwfTypeOf:       ft := refTypeOf;
        rwfSign:         ft := refSign;
        rwfAbs:          ft := refAbs;
        rwfDesignerOpened: ft := refDesignerOpened;
        rwfTimeZoneOffset: ft := refTimeZoneOffset;

        rwfDictionary,
        rwfLibComponents,
        rwfLibFunctions,
        rwfFileExists,
        rwfAppendASCIILine,
        rwfInsertASCIILine,
        rwfUpdateASCIILine,
        rwfDeleteASCIILine,
        rwfGetASCIILine,
        rwfASCIILineCount,
        rwfSkipPage,
        rwfSetPrintPos,
        rwfGetPrintPos,
        rwfPageNumber:
                         begin
                           ft := refCustomCall;
                           AddCode([rmoPUSHconst, Ord(TrwpRWFunction(Fnc).TypeFunct) - Ord(rwfDictionary) + 1]);
                         end;

        rwfExternal:     begin
                           ft := refCustomCall;
                           AddCode([rmoPUSHconst, Fnc.Params.Count,
                                    rmoPUSHconst, rwVMExternalFunctBase + TrwpRWFunction(Fnc).ExternalTypeFunct]);
                         end;
      end;

      if pt <> repUnknown then
        AddCode([rmoCallEmbProc, pt])

      else if ft <> refUnknown then
      begin
        AddCode([rmoCallEmbFunc, ft]);
        if AType.LeftAssignPart and (AType.VarType in [rwvPointer, rwvVariant])then
          AddCode([rmoMOVBA]);
      end;

      Result := True;
    end

    else
      Result := False;
  end;


  function ParseProperty: Boolean;
  var
    ClassTypeInfo: PTypeInfo;
    ClassPropInfo: PPropInfo;
    TypeData: PTypeData;
    M: TrwPublishedMethod;
    j: Integer;
  begin
    Result := False;

    ClassTypeInfo := AType.Obj.ClassInfo;
    ClassPropInfo := GetPropInfo(ClassTypeInfo, AIdentificator);

    if not Assigned(ClassPropInfo) and (AType.Obj is TrwComponent) then
    begin
      j := TrwComponent(AType.Obj)._UserProperties.IndexOf(AIdentificator);
      if j <> - 1 then
        ClassPropInfo := TrwComponent(AType.Obj)._UserProperties[j].PPropInfo;
    end
    else
      j := -1;

    if not Assigned(ClassPropInfo) then
    begin
      AType.Obj.MethodAddress(AIdentificator);
      if not Assigned(AType.Obj.MethodAddress('P'+AIdentificator)) then
      begin
        if AType.Obj is TrwComponent then
          Result := ParseEvent(TrwComponent(AType.Obj), 0);
        Exit;
      end;
    end;

    if Assigned(ClassPropInfo) then     //property
    begin
      AType.FactorType := rftProperty;
      case ClassPropInfo^.PropType^.Kind of
        tkInteger,
        tkInt64:    AType.VarType := rwvInteger;

        tkEnumeration:                                //???
                    begin
                       if  ClassPropInfo^.PropType^ = TypeInfo(Boolean) then
                         AType.VarType := rwvBoolean
                       else
                         AType.VarType := rwvInteger;
                    end;

        tkChar,
        tkWChar,
        tkLString,
        tkWString,
        tkString:   AType.VarType := rwvString;

        tkFloat:    AType.VarType := rwvFloat;

        tkClass:    AType.VarType := rwvPointer;

        tkVariant:  AType.VarType := rwvVariant;
      else
        AType.VarType := rwvUnknown;
      end;

      if AType.LeftAssignPart then
        AddCode([rmoMOVAconst, ClassPropInfo^.Name])
      else
        AddCode([rmoPUSHA,
           rmoPUSHconst, ClassPropInfo^.Name,
           rmoCallEmbFunc, refObjectPropValue]);

      if AType.VarType = rwvPointer then
      begin
        if j = -1 then
        begin
          TypeData := GetTypeData(ClassPropInfo^.PropType^);
          AType.ClassInf := rwRTTIInfo.FindClass(TypeData^.ClassType.ClassName);
          AType.Obj := GetObjectProp(AType.Obj, ClassPropInfo);
        end

        else
        begin
          AType.ClassInf :=  rwRTTIInfo.FindClass(TrwComponent(AType.Obj)._UserProperties[j].PointerType);
          AType.Obj := nil;
        end;
      end

      else
      begin
        AType.ClassInf := nil;
        AType.Obj := nil;
      end;
    end


    else //method
    begin
      M := PublishedMethods.IndexOf(AType.Obj.ClassType, AIdentificator);
      AddCode([rmoPushA]);
      i := ParseFuncParams(M.Params);
      AddCode([rmoPUSHConst, i,
               rmoPUSHconst, M.Params.Result.VarType,
               rmoPUSHconst, AIdentificator,
               rmoCallEmbFunc, refObjectMethod]);

      PutType(AType, M.Params.Result^);

      if AType.LeftAssignPart and (AType.VarType in [rwvPointer, rwvVariant])then
        AddCode([rmoMOVBA]);
    end;
    Result := True;
  end;


  procedure CreateTempObject;
  begin
    if not Assigned(AType.ClassInf) then
      Error(33);

    lTempObject := AType.ClassInf.CreateObjectInstance(False, True);
  end;


  function ParseTypeConversion: Boolean;
  var
    lClassInf: IrwClass;
    kw: TrwpTypeKeyWord;
    tv: TrwVarTypeKind;
    t: TrwVarType;
  begin
    kw := KeyWords.FindKeyWord(AIdentificator);
    if kw <> rwkUnknown then
    begin
      tv := KeyWordToVarType(kw);
      if tv = rwvUnknown then
        Error(1);

      CheckNextToken(rwkLBracket);
      StatExpression(t, AType.LeftAssignPart);
      CheckNextToken(rwkRBracket);
      AddCode([rmoPushA]);
      FuncAsType(tv, True);

      AType.FactorType := t.FactorType;
      AType.VarType := tv;
      AType.ClassInf := nil;
      AType.Obj := nil;
      Result := True;
    end

    else
    begin
      lClassInf := rwRTTIInfo.FindClass(AIdentificator);
      Result := Assigned(lClassInf);

      if Result then
      begin
        CheckNextToken(rwkLBracket);
        StatExpression(t, AType.LeftAssignPart);
        CheckType(t, [rwvPointer]);
        CheckNextToken(rwkRBracket);

        AType.FactorType := t.FactorType;
        AType.VarType := rwvPointer;
        AType.ClassInf := lClassInf;
        AType.Obj := nil;
      end;
    end;

    if Result and AType.LeftAssignPart and not (t.FactorType in [rftVariable, rftArrayElement]) then
      AddCode([rmoMOVBA]);
  end;


  function ParseKeyWord: Boolean;
  var
    kw: TrwpTypeKeyWord;
  begin
    Result := False;
    kw := KeyWords.FindKeyWord(AIdentificator);
    if kw <> rwkUnknown then
    begin
      case kw of
        rwkNil: begin
                  AddCode([rmoMOVAconst, 0]);
                  AType.VarType := rwvVariant;
                  AType.ClassInf := nil;
                  AType.Obj := nil;
                end;
      else
        Exit;
      end;

      if AType.LeftAssignPart then
        AddCode([rmoMOVBA]);
      AType.FactorType := rftExpression;
      Result := True;
    end;
  end;


begin
  if AFirstStep then
  begin
    AType.VarType := rwvUnknown;
    AType.ClassInf := nil;
    AType.Obj := FComponentsOwner;
    i := FLocalVars.IndexOf(AnsiUpperCase(AIdentificator));
  end;

  if AFirstStep and (i <> -1) then
  begin
    PutType(AType, FLocalVars[i]^);
    AType.FactorType := rftVariable;
    if FLocalVars[i].Reference then
    begin
      AddCode([rmoMOVBLocVar, i,
               rmoMOVArefB])
    end

    else
    begin
      if SameText(AIdentificator, cParserLibComp) then
        AType.Obj := FLibComp
      else if SameText(AIdentificator, cParserSelf) then
        AType.Obj := FComponent
      else if SameText(AIdentificator, cParserRootOwner) then
        AType.Obj := FRootOwner;

      if AType.LeftAssignPart or (AType.VarType = rwvArray) then
        AddCode([rmoPushConst, i,
                 rmoCallEmbFunc, refAddrLocVar,
                 rmoMOVBA]);
      if not(AType.LeftAssignPart and (AType.VarType = rwvArray)) then
        AddCode([rmoMOVAlocVar, i]);
    end;
  end

  else if AFirstStep and ParseKeyWord then

  else if AFirstStep and ParseTypeConversion then

  else if not AFirstStep and ParseProperty then

  else if not AFirstStep and (AType.Obj is TrwComponent) and
          Assigned(TrwComponent(AType.Obj).GlobalVarFunc) and
          (
            (FComponentsOwner = AType.Obj) and ParseVarFunc(FComponentsOwner.GlobalVarFunc, 0, [], [rvvPrivate])
            or
            (FComponentsOwner <> AType.Obj) and ParseVarFunc(TrwComponent(AType.Obj).GlobalVarFunc, 0, [rvvProtected, rvvPrivate], [rvvProtected, rvvPrivate])
          ) then

{  else if AFirstStep and (AType.Obj is TrwComponent) and (TrwComponent(AType.Obj).VirtualOwner <> nil) and
        (TrwComponent(AType.Obj).VirtualOwner.GlobalVarFunc <> nil) and
        ParseVarFunc(TrwComponent(AType.Obj).VirtualOwner.GlobalVarFunc, 1, [], [rvvPrivate]) then
}
  else if AFirstStep and Assigned(FLibComp) and Assigned(FLibComp.GlobalVarFunc) and
        ParseVarFunc(FLibComp.GlobalVarFunc, 3, [], [rvvPrivate]) then

  else if AFirstStep and Assigned(FRootOwner) and Assigned(FRootOwner.GlobalVarFunc) and
          ParseVarFunc(FRootOwner.GlobalVarFunc, 2, [], [rvvPrivate]) then

  else if ParseComponentName then
    AType.FactorType := rftExpression

  else if ParseLibFunctions then

  else if not ParseEmbeddedFunctions then
  begin
    FStatus.sToken := AIdentificator;
    Error(34);
  end;


  ParseIndexProp;

  ParseArrElement;

  GetNextToken;

  // Something.Something ....
  if (FStatus.sKeyWord = rwkPoint) then
  begin
    if AType.LeftAssignPart then
    begin
      if AType.FactorType = rftArrayElement then
      begin
        AddCode([rmoPushB,
                 rmoPushA,
                 rmoCallEmbFunc, refArrayElement,
                 rmoMOVBA]);
        AType.VarType := rwvVariant;
      end

      else if AType.FactorType = rftVariable then
        AddCode([rmoMOVArefB,
                 rmoMOVBA])

      else if AType.FactorType = rftStrElement then
      begin
        AddCode([rmoPushB,
                 rmoPushA,
                 rmoCallEmbFunc, refStrElement,
                 rmoMOVBA]);
        AType.VarType := rwvString;
      end

      else if AType.FactorType = rftProperty then
        AddCode([rmoPUSHB,
                 rmoPUSHA,
                 rmoCallEmbFunc, refObjectPropValue,
                 rmoMOVBA]);
    end;

    CheckType(AType, [rwvPointer]);

    if not Assigned(AType.Obj) then
    begin
      CreateTempObject;
      AType.Obj := lTempObject;
    end
    else
      lTempObject := nil;

    try
      GetNextToken;
      if not (FStatus.sTokenType in [rwtUnknown, rwtKeyword]) then
        Error(1);

      Identificator(FStatus.sToken, AType, False);
    finally
      if Assigned(lTempObject) then
      begin
        lTempObject.Free;
        AType.Obj := nil;
      end;
    end;
  end

  else
    PutBackToken;
end;


function TrwParser.ValueByExpressionForDebug(AExpression: string): Variant;
{var
  lText: string;
  lToken: string;
  lPrevToken: string;
  lTokenType: TrwpTypeToken;
  lKeyWord: TrwpTypeKeyWord;
  lPos: LongInt;
  lWasBack: Boolean;}
begin
{  lText := FText;
  lToken := FStatus.sToken;
  lPrevToken := FStatus.sPrevToken;
  lTokenType := FStatus.sTokenType;
  lKeyWord := FStatus.sKeyWord;
  lPos := FPos;
  lWasBack := FStatus.sWasBack;

  try
    try
      FText := AExpression + #0;
      FPos := 1;
      FStatus.sWasBack := False;
//      StatExpression;
      if VarIsNull(Result) then
        Result := 'Null';
    except
      Result := 'Error';
    end;

  finally
    FText := lText;
    FStatus.sToken := lToken;
    FStatus.sPrevToken := lPrevToken;
    FStatus.sTokenType := lTokenType;
    FStatus.sKeyWord := lKeyWord;
    FPos := lPos;
    FStatus.sWasBack := lWasBack;
  end;
}
end;



(*
        { [var] <var_name> : {Integer|Float|Date|String|Array|Pointer} }
*)

procedure TrwParser.StatVariableDef(AVarName: string = '');
var
  lVarName, vn: string;
  lVarType: TrwVarTypeKind;
  i: Integer;
  lPointerType: String;
begin
  if FVarDefFinished then
    Error(1);

  if AVarName = '' then
  begin
    CheckNextTokenType(rwtUnknown);
    vn := FStatus.sToken;
    lVarName := AnsiUpperCase(FStatus.sToken);
  end
  else
  begin
    vn := AVarName;
    lVarName := AnsiUpperCase(AVarName);
  end;

  CheckNextToken(rwkColon);

  lPointerType := '';
  lVarType := rwvUnknown;
  GetNextToken;
  if FStatus.sTokenType = rwtKeyWord then
    lVarType := KeyWordToVarType(FStatus.sKeyWord)

  else if FStatus.sTokenType = rwtUnknown then
  begin
    if rwRTTIInfo.FindClass(FStatus.sToken) = nil then
      Error(29);

    lVarType := rwvPointer;
    lPointerType := FStatus.sToken;
  end
  else
    Error(1);

  i := FLocalVars.IndexOf(lVarName);
  if (i <> -1) then
  begin
    FStatus.sToken := vn;
    Error(27);
  end;

  AddLocalVar(lVarType, vn, lPointerType);

  AddCode([rmoNewVar, lVarType]);
//  AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);  var declaration is not for debugging
end;



(*
       { if <logical_expression> then [statements] [elseif [statements]] [else [statements]] endif }
*)

procedure TrwParser.StatIf;
var
  lEndAddr: TList;
  i, n: Integer;

  procedure StatThen;
  var
    n1, n2: Integer;
    t: TrwVarType;
  begin
    StatExpression(t);
    CheckType(t, [rwvBoolean]);
    CheckNextToken(rwkThen);
    AddCode([rmoMovBconst, False,
             rmoCmpAneB,
             rmoJumpIfFalse, 0]);
    n1 := CodeLastAddr;             //jump to next branch
    AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);
    ParseBranch([rwkElse, rwkElseIf, rwkEndIf]);
    AddCode([rmoJump, 0]);
    n2 := CodeLastAddr;             //jump to the end
    EditCode(n1, n2+1-n1);
    lEndAddr.Add(Pointer(n2));
  end;


  procedure StatIfElseIf;
  begin
    if (FStatus.sKeyWord = rwkElseIf) then
    begin
      StatThen;
      StatIfElseIf;
    end

    else if (FStatus.sKeyWord = rwkElse) then
      ParseBranch([rwkElseIf, rwkEndIf]);
  end;

begin
  lEndAddr := TList.Create;

  try
    StatThen;

    while not (FStatus.sKeyWord in [rwkEndIf, rwkEndOfText]) do
      StatIfElseIf;

    n := CodeLastAddr + 1;
    for i := 0 to lEndAddr.Count -1 do
      EditCode(Integer(lEndAddr[i]), n - Integer(lEndAddr[i]));

  finally
    lEndAddr.Free;
  end;
end;


procedure TrwParser.ParseBranch(AKeyWords: TrwSetKeyword);
begin
  repeat
    FDebInfBegPos := GetLineBegPos;
    GetNextToken;
    if (FStatus.sKeyWord in AKeyWords) then break;
    ParseStatement;
  until False;

  PutBackToken;
  FDebInfBegPos := GetLineBegPos;
  FDebInfAddr := gtvCompilingPointer;
  GetNextToken;
end;

(*
       { for <int_variable> := <int_expression> to <int_expression> do [statements] EndFor }
*)

procedure TrwParser.StatFor;
var
  lCounter, lBegFor, lEndFor, lLoopEnd: Integer;
  t: TrwVarType;
  lBreakList, OldBreakList: TList;
  lContinueList, OldContinueList: TList;
  i: Integer;
begin
  GetNextToken;
  if (FStatus.sTokenType <> rwtUnknown) then
    Error(1);

  lCounter := FLocalVars.IndexOf(AnsiUpperCase(FStatus.sToken));
  if lCounter = -1 then
  begin
    i := FComponentsOwner.GlobalVarFunc.Variables.IndexOf(FStatus.sToken);
    if (i <> -1) and
       (not FComponentsOwner.GlobalVarFunc.Variables[i].InheritedItem or
       FComponentsOwner.GlobalVarFunc.Variables[i].InheritedItem and
       (FComponentsOwner.GlobalVarFunc.Variables[i].Visibility <> rvvPrivate))
        then
      Error(28)
    else
      Error(4);
  end;

  lBreakList := TList.Create;
  lContinueList := TList.Create;
  OldBreakList := FCurrBreakList;
  OldContinueList := FCurrContinueList;
  FCurrBreakList := lBreakList;
  FCurrContinueList := lContinueList;
  try
    CheckNextToken(rwkAssign);
    StatExpression(t);
    CheckType(t, [rwvInteger]);
    AddCode([rmoPushA]);
    FuncAsType(rwvInteger, False);
    AddCode([rmoMOVlocVarA, lCounter]);
    lBegFor := CodeLastAddr+1;

    CheckNextToken(rwkTo);
    StatExpression(t);
    CheckType(t, [rwvInteger]);
    AddCode([rmoMovBlocVar, lCounter,
             rmoCmpAgeB,
             rmoJumpIfFalse, 0]);
    lEndFor := CodeLastAddr;

    CheckNextToken(rwkDo);

    AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);

    ParseBranch([rwkEndFor]);

    lLoopEnd := CodeLastAddr;

    AddCode([rmoMOVALocVar, lCounter,
             rmoIncA,
             rmoMOVLocVarA, lCounter,
             rmoJump]);
    AddCode([lBegFor -(CodeLastAddr+1)]);

    EditCode(lEndFor, CodeLastAddr+1 - lEndFor);

    AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);

    for i := 0 to lBreakList.Count -1 do
      EditCode(Integer(lBreakList[i]), CodeLastAddr + 1 - Integer(lBreakList[i]));

    for i := 0 to lContinueList.Count -1 do
      EditCode(Integer(lContinueList[i]), lLoopEnd + 1 - Integer(lContinueList[i]));

  finally
    FCurrBreakList := OldBreakList;
    FCurrContinueList := OldContinueList;
    lBreakList.Free;
    lContinueList.Free;
  end;
end;


(*
       { while <bool_expression> do [statements] EndWhile }
*)

procedure TrwParser.StatWhile;
var
  lBeg, lEnd: Integer;
  t: TrwVarType;
  lBreakList, OldBreakList: TList;
  lContinueList, OldContinueList: TList;
  i: Integer;
begin
  lBreakList := TList.Create;
  lContinueList := TList.Create;
  OldBreakList := FCurrBreakList;
  OldContinueList := FCurrContinueList;
  FCurrBreakList := lBreakList;
  FCurrContinueList := lContinueList;

  try
    lBeg := CodeLastAddr+1;
    StatExpression(t);
    CheckType(t, [rwvBoolean]);
    CheckNextToken(rwkDo);
    AddCode([rmoMovBconst, False,
             rmoCmpAneB,
             rmoJumpIfFalse, 0]);
    lEnd := CodeLastAddr;

    AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);

    ParseBranch([rwkEndWhile]);

    AddCode([rmoJump]);
    AddCode([lBeg -(CodeLastAddr+1)]);

    EditCode(lEnd, CodeLastAddr+1 - lEnd);

    AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);

    for i := 0 to lBreakList.Count -1 do
      EditCode(Integer(lBreakList[i]), CodeLastAddr + 1 - Integer(lBreakList[i]));

    for i := 0 to lContinueList.Count -1 do
      EditCode(Integer(lContinueList[i]), lBeg - Integer(lContinueList[i]));

  finally
    FCurrBreakList := OldBreakList;
    FCurrContinueList := OldContinueList;
    lBreakList.Free;
    lContinueList.Free;
  end;
end;


procedure TrwParser.StatInherited;
var
  P: TrwFuncParams;
  Ev: TrwEvent;
  i, j: Integer;
  cl, prc: String;
  Own: TrwComponent;
  cPath, h: string;
  LC: TrwLibComponent;
  LibCompClass: IrwClass;
begin
  if not Assigned(FComponent) then
    Error(35);

  cl := '';
  prc := '';

  // Checking Owners chain since event can be overriden on Owner level
  cPath := FComponent.PathFromRootOwner;
  GetNextStrValue(cPath, '.');
  Own := FComponent.RootOwner;
  while Assigned(Own) do
  begin
    if Own.IsLibComponent then
    begin
      LibCompClass := rwRTTIInfo.FindClass(Own._LibComponent);
      if Assigned(LibCompClass) and LibCompClass.IsUserClass then
      begin
        LC := TrwLibComponent(LibCompClass.GetUserClass.RealObject);
        h := cPath;
        if h <> '' then
          h := h + '.';
        h := h + FEventName;
        if LC.VMT.IndexOf(h) <> -1 then
        begin
          cl := Own._LibComponent;
          prc := h;
          break;
        end
      end;
    end;
    h := GetNextStrValue(cPath, '.');
    i := Own.FindVirtualChild(h);
    if i <> -1 then
      Own := TrwComponent(Own.GetVirtualChild(i))
    else
      Own := nil;    
  end;

  if (cl = '') or (prc = '') and FComponent.IsLibComponent then
    //Owners are clear for overridings. Let's go into component's ancestor class
    if not Assigned(GetClass(FComponent._LibComponent)) then
    begin
      cl := FComponent._LibComponent;
      prc := FEventName;
    end;

  if (cl = '') or (prc = '') then
    // There is nothing to call. Just skip this INHERITED declaration.
    Exit;

  Ev := FComponent.Events[FEventName];
  if not Assigned(Ev) then
    Error(32);
  P := Ev.Params;
  for i := 0 to P.Count - 1 do
  begin
    j := FLocalVars.IndexOf(P[i].Descriptor.Name);
    if j = -1 then
      Error(32);

    AddCode([rmoMOVALocVar, j,
             rmoPUSHA]);
  end;

  AddCode([ rmoMOVALocVar, 0,   //Self
            rmoPUSHA,
            rmoCallLibCompEvent, cl, prc]);

  if P.Result.VarType <> rwvUnknown then
  begin
    j := FLocalVars.IndexOf('Result');
    if j = -1 then
      Error(32);

    AddCode([rmoMOVLocVarA, j]);
  end;

  AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);
end;


//                Syntax analizer of expressions

procedure TrwParser.StatExpression(var AType: TrwVarType; ALeftAssignPart: Boolean = False; ADefType: TrwVarTypeKind = rwvUnknown);
begin
  FDefType := ADefType;
  AType.LeftAssignPart := ALeftAssignPart;
  GetNextToken;
  StatExpressionLevel2(AType);
  PutBackToken;
end;


(*         Logical operation of comparision
         { <element> {<|>|=|<>|>=|<=} <element> }
*)

procedure TrwParser.StatExpressionLevel2(var AType: TrwVarType);
var
  lOper: TrwpTypeKeyWord;
  Op: TrwVMOperation;
  t: TrwVarType;
begin
  StatExpressionLevel2_1(AType);

  lOper := FStatus.sKeyWord;

  if lOper in [rwkEqual, rwkLess, rwkMore, rwkMeq, rwkLeq, rwkNeq] then
  begin
    CheckType(AType, [rwvBoolean, rwvFloat, rwvInteger, rwvCurrency, rwvDate, rwvString, rwvPointer]);
    AddCode([rmoPushA]);

    GetNextToken;
    StatExpressionLevel2_1(t);
    if not((lOper in [rwkEqual, rwkNeq]) and (t.VarType = rwvPointer) and (AType.VarType = rwvPointer)) then
    begin
      CheckType(t, [rwvBoolean, rwvFloat, rwvInteger, rwvCurrency, rwvDate, rwvString]);
      CheckTypeCompatibility(AType, t);
    end;

    AddCode([rmoPopB,
             rmoSwapAB]);

    case lOper of
      rwkLess:  op := rmoCmpAlB;
      rwkLeq:   op := rmoCmpAleB;
      rwkMore:  op := rmoCmpAgB;
      rwkMeq:   op := rmoCmpAgeB;
      rwkNeq:   op := rmoCmpAneB;
    else
      op := rmoCmpAeB;
    end;

    AddCode([op,
             rmoPushFlag,
             rmoPopA]);

    AType.VarType := rwvBoolean;
    AType.FactorType := rftExpression;
  end;
end;


function TrwParser.DefineMathOperType(const AType1,  AType2: TrwVarTypeKind): TrwVarTypeKind;
begin
  Result := AType1;

  if AType1 = rwvVariant then
    Result := AType2

  else if AType1 <> AType2 then
    if AType2 in [rwvFloat, rwvCurrency, rwvDate] then
      Result := rwvFloat;
end;



(*      <element>  is Additon and Subtraction for two terms
               { <term>{+|-}<term> }
*)

procedure TrwParser.StatExpressionLevel2_1(var AType: TrwVarType);
var
  lOper: TrwpTypeKeyWord;
  Op: TrwVMOperation;
  t: TrwVarType;
begin
  StatExpressionLevel3(AType);

  lOper := FStatus.sKeyWord;

  t := AType;

  while (lOper in [rwkPlus, rwkMinus]) do
  begin
    AddCode([rmoPushA]);

    GetNextToken;
    StatExpressionLevel3(AType);

    AddCode([rmoPopB,
             rmoSwapAB]);


    if lOper = rwkPlus then
    begin
      Op := rmoAdd;

      if t.VarType in [rwvFloat, rwvInteger, rwvCurrency, rwvDate, rwvString, rwvVariant] then
      begin
        if t.VarType = rwvString then
          CheckType(AType, [rwvString])
        else if (AType.VarType = rwvString) and (AType.VarType = rwvVariant) then
          t.VarType := rwvString
        else
        begin
          CheckType(AType, [t.VarType, rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
          t.VarType := DefineMathOperType(t.VarType, AType.VarType);
        end;
      end
      else
        Error(23);
    end

    else
    begin
      Op := rmoSubstract;
      CheckType(t, [rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
      CheckType(AType, [t.VarType, rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
      t.VarType := DefineMathOperType(t.VarType, AType.VarType);
    end;
    AddCode([op]);

    lOper := FStatus.sKeyWord;
    t.FactorType := rftExpression;
  end;

  AType := t;
end;


(*      <term>  is Multiplication or Division for two factors
                 { <factor> {*|/} <factor> }
*)

procedure TrwParser.StatExpressionLevel3(var AType: TrwVarType);
var
  lOper: TrwpTypeKeyWord;
  Op: TrwVMOperation;
  t: TrwVarType;
begin
  StatExpressionLevel4(AType);

  lOper := FStatus.sKeyWord;

  t := AType;
  while (lOper in [rwkMul, rwkDivide, rwkDiv, rwkMod]) do
  begin
    AddCode([rmoPushA]);
    GetNextToken;
    StatExpressionLevel4(AType);

    AddCode([rmoPopB,
             rmoSwapAB]);

    case lOper of
      rwkMul:
        begin
          CheckType(t, [rwvFloat, rwvInteger, rwvCurrency]);
          CheckType(AType, [t.VarType, rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
          Op := rmoMultiply;
          t.VarType := DefineMathOperType(t.VarType, AType.VarType);
        end;

      rwkDivide:
        begin
          CheckType(t, [rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
          CheckType(AType, [t.VarType, rwvFloat, rwvInteger, rwvCurrency]);
          Op := rmoDivide;
          t.VarType := rwvFloat;
        end;

      rwkDiv:
        begin
          CheckType(t, [rwvInteger, rwvFloat, rwvDate]);
          CheckType(AType, [rwvInteger]);
          t.VarType := rwvInteger;
          Op := rmoDiv;
        end;

    else
        begin
          CheckType(t, [rwvInteger, rwvFloat, rwvDate]);
          CheckType(AType, [rwvInteger]);
          t.VarType := rwvInteger;
          Op := rmoMod;
        end;
    end;
    AddCode([op]);

    lOper := FStatus.sKeyWord;
    t.FactorType := rftExpression;
  end;
  AType := t;
end;


(*      Operation of Exponentiate
            { ^ <factor> }
*)

procedure TrwParser.StatExpressionLevel4(var AType: TrwVarType);
begin
  StatExpressionLevel4_1(AType);

  if (FStatus.sKeyWord = rwkExponentiate) then
  begin
    CheckType(AType, [rwvFloat, rwvInteger, rwvCurrency]);
    AddCode([rmoPushA]);
    GetNextToken;
    StatExpressionLevel4_1(AType);
    CheckType(AType, [rwvFloat, rwvInteger, rwvCurrency]);
    AddCode([rmoPopB,
             rmoSwapAB,
             rmoAexpB]);

    AType.VarType := rwvFloat;
    AType.FactorType := rftExpression;
  end;
end;


(*               Operation 'OR'
              { <factor> or <factor> }
*)

procedure TrwParser.StatExpressionLevel4_1(var AType: TrwVarType);
var
  lOper: TrwpTypeKeyWord;
begin
  StatExpressionLevel4_2(AType);

  lOper := FStatus.sKeyWord;

  while (lOper = rwkOr) do
  begin
    CheckType(AType, [rwvBoolean]);
    AddCode([rmoPushA]);
    GetNextToken;
    StatExpressionLevel4_2(AType);
    CheckType(AType, [rwvBoolean]);
    AddCode([rmoPopB,
             rmoSwapAB,
             rmoAorB]);
    lOper := FStatus.sKeyWord;
    AType.FactorType := rftExpression;
  end;
end;


(*               Operation 'AND'
              { <factor> and <factor> }
*)

procedure TrwParser.StatExpressionLevel4_2(var AType: TrwVarType);
var
  lOper: TrwpTypeKeyWord;
begin
  StatExpressionLevel4_3(AType);

  lOper := FStatus.sKeyWord;

  while (lOper = rwkAnd) do
  begin
    CheckType(AType, [rwvBoolean]);
    AddCode([rmoPushA]);
    GetNextToken;
    StatExpressionLevel4_3(AType);
    CheckType(AType, [rwvBoolean]);
    AddCode([rmoPopB,
             rmoSwapAB,
             rmoAandB]);
    lOper := FStatus.sKeyWord;
    AType.FactorType := rftExpression;
  end;
end;


(*               Operation 'NOT'
                { not <factor> }
*)

procedure TrwParser.StatExpressionLevel4_3(var AType: TrwVarType);
var
  flNot: Boolean;
begin
  if (FStatus.sKeyWord = rwkNot) then
  begin
    flNot := True;
    GetNextToken;
  end
  else
    flNot := False;

  StatExpressionLevel5(AType);

  if flNot then
  begin
    CheckType(AType, [rwvBoolean]);
    AddCode([rmoNotA]);
    AType.FactorType := rftExpression;
  end;
end;


(*             Unary - or +
               { {+|-} <factor> }
*)

procedure TrwParser.StatExpressionLevel5(var AType: TrwVarType);
var
  flRev, f: Boolean;
begin
  flRev := False;
  if (FStatus.sKeyWord in [rwkMinus, rwkPlus]) then
  begin
    f := True;
    if FStatus.sKeyWord = rwkMinus then
      flRev := True;
    GetNextToken;
  end
  else
   f := False;

  StatExpressionLevel6(AType);

  if f then
  begin
    CheckType(AType, [rwvFloat, rwvInteger, rwvCurrency]);
    AType.FactorType := rftExpression;
  end;

  if flRev then
    AddCode([rmoReverseSignA]);
end;


(*          Expression in round brackets
                { ( <expression> ) }
*)

procedure TrwParser.StatExpressionLevel6(var AType: TrwVarType);
begin
  if (FStatus.sKeyWord = rwkLbracket) then
  begin
    GetNextToken;
    StatExpressionLevel2(AType);
    PutBackToken;
    CheckNextToken(rwkRbracket);
    GetNextToken;
  end
  else
    StatExpressionLevel7(AType);
end;


(*                <factor>
       { <identificator>|<Number>|<String> }
*)

procedure TrwParser.StatExpressionLevel7(var AType: TrwVarType);
var
  Res: Variant;
begin
  case FStatus.sTokenType of
    rwtUnknown, rwtKeyWord:
      try
        Identificator(FStatus.sToken, AType, True);
      finally
        gtvCodeInsightInfo := AType;
      end;

    rwtString:
      begin
        AddCode([rmoMOVAconst, FStatus.sToken]);
        AType.VarType := rwvString;
        AType.FactorType := rftExpression;
      end;

    rwtNumber:
      begin
        if (Pos('.', FStatus.sToken) <> 0) or (Pos('E', FStatus.sToken) <> 0) then
        begin
          Res := StrToFloat(FStatus.sToken);
          AType.VarType := rwvFloat;
        end
        else
        begin
          Res := StrToInt(FStatus.sToken);
          AType.VarType := rwvInteger;
        end;
        AType.FactorType := rftExpression;
        AddCode([rmoMOVAconst, Res]);
      end;

    rwtSymbol:
      if FStatus.sKeyWord = rwkSquareLBracket then
      begin
        PutBackToken;
        StatConstArray(AType);
      end
      else
        Error(1);
  else
    Error(1);

  end;

  GetNextToken;
end;


procedure TrwParser.GetNextToken;
begin
  ScannerGetNextToken(FStatus);
end;


function TrwParser.GetLineBegPos: Integer;
begin
  Result := FStatus.sPos;
  while (Result > 0) and (Result < Length(FStatus.sText)) and
        not (FStatus.sText[Result] in [#10, #13]) do
    Dec(Result);
end;


procedure TrwParser.ChangeScannersPosition(APos: LongInt);
begin
  ScannerChangePosition(FStatus, APos);
end;

procedure TrwParser.Error(ACode: Integer);
var
  str, h, h2, h3, h4: string;
  LineCount, PosCount, i: Integer;
  Cl: IrwClass;
begin
  case ACode of
    1: str := ResErrSyntaxError;
    2: str := ResErrNBalRB;
    3: str := ResErrNClQuotes;
    4: str := Format(ResErrNDefVar, [FStatus.sToken]);
    5: str := Format(ResErrNDefPar, [FStatus.sToken]);
    6: str := Format(ResErrNDefProc, [FStatus.sToken]);
    7: str := Format(ResErrNDefProp, [FStatus.sToken, FStatus.sPrevToken]);
    8: str := Format(ResErrNExpToken, [KeyWords.FindKeyWordByCode(FStatus.sKeyWord), FStatus.sToken]);
    9: str := Format(ResErrWrongNumb, [FStatus.sToken]);
    10: str := ResErrExpInteger;
    11: str := ResErrExpFloat;
    12: str := ResErrExpBoolean;
    13: str := ResErrExpString;
    14: str := ResErrExpDate;
    15: str := ResErrExpObject;
    16: str := ResErrExpArray;
    17: str := ResErrExpVarName;
    18: str := Format(ResErrWrongIndex, [FStatus.sPrevToken, FStatus.sToken]);
    19: str := Format(ResErrNDefConst, [FStatus.sToken]);
    20: str := FStatus.sToken;
    21: str := Format(ResErrWrongStrIndex, [FStatus.sPrevToken, FStatus.sToken]);
    22: str := Format(ResErrNDefFunc, [FStatus.sToken]);
    23: str := ResErrWrongType;
    24: str := ResErrMismParams;
    25: str := Format(ResErrUnknSymbol, [FStatus.sToken]);
    26: str := ResErrUfinishedText;
    27: str := Format(ResErrAlreadyDefVar, [FStatus.sToken]);
    28: str := ResErrWrongForCounter;
    29: str := Format(ResErrTypeIsIncorrect, [FStatus.sToken]);
    30: str := Format(ResErrWrongClass, [FStatus.sToken]);
    31: str := ResErrLeftSideCannotAssign;
    32: str := ResErrInternalCompileError;
    33: str := ResErrAbstractPointer;
    34: str := Format(ResErrUnknIdentif, [FStatus.sToken]);
    35: str := ResErrNotApplicableInstr;
    36: str := ResErrNoIndexProperty;
  end;

  LineCount := 1;
  PosCount := 0;
  for i := 1 to Length(FStatus.sText) do
  begin
    if (FStatus.sText[i] <> #10) then
      if (FStatus.sText[i] = #13) then
      begin
        Inc(LineCount);
        PosCount := 0;
      end
      else
        Inc(PosCount);
    if (i + 1 >= FStatus.sPos) then
      break;
  end;

  if Assigned(FComponentsOwner) then
  begin
    Cl := rwRTTIInfo.FindClass(FComponentsOwner.Name);
    if FComponentsOwner.IsLibComponent and Assigned(Cl) and Cl.IsUserClass then
      h := cParserLibComp
    else
      h := FComponentsOwner.Name;
  end

  else
    h := '';

  if Assigned(FComponent) then
  begin
    Cl := rwRTTIInfo.FindClass(FComponentsOwner.Name);
    if FComponent.IsLibComponent and Assigned(Cl) and Cl.IsUserClass  then
      h2 := cParserLibComp
    else
      h2 := FComponent.Name;

    h4 := GetComponentPath(FComponent);

    if Assigned(FComponentsOwner) and FComponentsOwner.IsLibComponent and SameText(h, cParserLibComp) then
      h3 := FComponentsOwner._LibComponent
    else
      h3 := '';
  end
  else
  begin
    h2 := '';
    h3 := '';
    h4 := '';
  end;

  raise ErwParserError.CreateParserError(h3, h2, h, FEventName, h4, LineCount,
    PosCount, FStatus.sPos, str);
end;


procedure TrwParser.AddCode(const ACodes: array of variant);
var
  i: Integer;
begin
  for i := Low(ACodes) to High(ACodes) do
  begin
    if High(gtvCompiledCode^) < gtvCompilingPointer then
      SetLength(gtvCompiledCode^, Length(gtvCompiledCode^)+512);
    gtvCompiledCode^[gtvCompilingPointer] := ACodes[i];
    Inc(gtvCompilingPointer);
  end;
end;


function  TrwParser.CodeLastAddr: Integer;
begin
  Result := gtvCompilingPointer-1;
end;


procedure TrwParser.EditCode(const AAddr: Integer; const ACode: Variant);
begin
  gtvCompiledCode^[AAddr] := ACode;
end;


procedure TrwParser.AddDebInf(const Addr, BegPos, EndPos: Integer);
var
  h: string;
  i, n: Integer;
begin
  if not Assigned(gtvDebInf) or
     Assigned(FComponent) and (FComponent.DsgnLockState <> rwLSUnlocked) or
     Assigned(FComponentsOwner) and (FComponentsOwner.DsgnLockState <> rwLSUnlocked) then
    Exit;

  if High(gtvDebInf^) < gtvDebInfPointer then
    SetLength(gtvDebInf^, Length(gtvDebInf^)+512);

  h := IntToStr(Addr) + ';' + IntToStr(CodeLastAddr) + ';' +
    FDebugIndInfo + ';' + FEventName;

  if FDebInfLastCRLinePos >= BegPos then
  begin
    FDebInfLastCRLinePos := 0;
    FDebInfLastLine := 1;
  end;
  n := FDebInfLastCRLinePos + 1;
  for i := n to BegPos do
  begin
    if (FStatus.sText[i] = #13) then
    begin
      Inc(FDebInfLastLine);
      FDebInfLastCRLinePos := i;
    end;
  end;

  gtvDebInf^[gtvDebInfPointer] := h + ';' + IntToStr(FDebInfLastLine) + ';' + Copy(FStatus.sText, BegPos, EndPos - BegPos + 1);

  Inc(gtvDebInfPointer);

  FDebInfBegPos := GetLineBegPos;
  FDebInfAddr := gtvCompilingPointer;
end;


procedure TrwParser.AddDebSymbInf;
begin
  if not Assigned(gtvDebSymbInf) or
     Assigned(FComponent) and (FComponent.DsgnLockState <> rwLSUnlocked) or
     Assigned(FComponentsOwner) and (FComponentsOwner.DsgnLockState <> rwLSUnlocked) then
    Exit;


  if High(gtvDebSymbInf^) < gtvDebSymbInfPointer then
    SetLength(gtvDebSymbInf^, Length(gtvDebSymbInf^) + 32);

  gtvDebSymbInf^[gtvDebSymbInfPointer] := FDebugIndInfo + ';' + FEventName + ';' +
     IntToStr(FDebFirstAddr) + ';' + IntToStr(gtvDebInfPointer - 1) + ';' + FLocalVars.VarInfo;

  Inc(gtvDebSymbInfPointer);
end;


procedure TrwParser.PutType(var AType: TrwVarType; const AVar: TrwVariable);
begin
  AType.VarType := AVar.VarType;
  if (AVar.VarType = rwvPointer) and (AVar.PointerType <> '') then
    AType.ClassInf := rwRTTIInfo.FindClass(AVar.PointerType)
  else
    AType.ClassInf := nil;  

  if AVar.Reference then
    AType.FactorType := rftVariable
  else
    AType.FactorType := rftExpression;

  AType.Obj := nil;
end;


procedure TrwParser.PutBackToken;
begin
  ScannerPutBackToken(FStatus);
end;


procedure TrwParser.StatConstArray(var AType: TrwVarType);
var
  n: Integer;
  t: TrwVarType;
begin
  CheckNextToken(rwkSquareLBracket);

  n := 0;
  GetNextToken;
  if (FStatus.sKeyWord <> rwkSquareRBracket) then
  begin
    PutBackToken;
    repeat
      Inc(n);
      StatExpression(t);
      if t.VarType = rwvArray then
        Error(23);
      AddCode([rmoPUSHA]);
      GetNextToken;
    until (FStatus.sKeyWord <> rwkComma);
    PutBackToken;
  end
  else
    PutBackToken;

  CheckNextToken(rwkSquareRBracket);
  AddCode([rmoPUSHConst, n,
           rmoCallEmbFunc, refCreateConstArray]);

  AType.VarType := rwvArray;
  AType.ClassInf := nil;
  AType.Obj := nil;
  AType.FactorType := rftConstArray;
end;


procedure TrwParser.StatExit;
begin
  AddCode([rmoJump, 0]);
  FExitsList.Add(Pointer(CodeLastAddr));
  AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);  
end;



procedure TrwParser.StatBreak;
begin
  AddCode([rmoJump, 0]);
  FCurrBreakList.Add(Pointer(CodeLastAddr));
  AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);
end;


procedure TrwParser.StatContinue;
begin
  AddCode([rmoJump, 0]);
  FCurrContinueList.Add(Pointer(CodeLastAddr));
  AddDebInf(FDebInfAddr, FDebInfBegPos, FStatus.sPos);
end;


procedure TrwParser.FuncAsType(const AType: TrwVarTypeKind; const ANullConv: Boolean);
begin
  // The actual value must be pushed into stack at shis point. Caller must care about it.

  AddCode([rmoPushConst, AType,
           rmoPushConst, ANullConv,
           rmoCallEmbFunc, refConvertToType]);
end;


function TrwParser.AddLocalVar(AVarType: TrwVarTypeKind; AName, APointerType: String): Integer;
begin
  Result := FLocalVars.Add(AVarType, AnsiUpperCase(AName), APointerType);

  if Assigned(gtvCodeInsightLocalVars) and (Length(AName) > 0) then
    gtvCodeInsightLocalVars.Add(AVarType, AName, APointerType);
end;


{TrwpKeyWords}

constructor TrwpKeyWords.Create;
begin
  inherited Create;

  {Fill list by keywords}
  Add(';', rwkSemicolomn);
  Add('.', rwkPoint);
  Add('+', rwkPlus);
  Add('-', rwkMinus);
  Add('*', rwkMul);
  Add('/', rwkDivide);
  Add('(', rwkLbracket);
  Add(')', rwkRbracket);
  Add('[', rwkSquareLBracket);
  Add(']', rwkSquareRBracket);
  Add('=', rwkEqual);
  Add(',', rwkComma);
  Add(':', rwkColon);
  Add('<', rwkLess);
  Add('>', rwkMore);
  Add('>=', rwkMeq);
  Add('<=', rwkLeq);
  Add('<>', rwkNeq);
  Add('{', rwkRemb);
  Add('}', rwkReme);
  Add('//', rwkRem);
  Add(':=', rwkAssign);
  Add('^', rwkExponentiate);
  Add('MOD', rwkMod);
  Add('DIV', rwkDiv);
  Add('AND', rwkAnd);
  Add('OR', rwkOr);
  Add('NOT', rwkNot);
  Add('PROCEDURE', rwkProcedure);
  Add('FUNCTION', rwkFunction);
  Add('END', rwkEnd);
  Add('VAR', rwkVar);
  Add('INTEGER', rwkInteger);
  Add('BOOLEAN', rwkBoolean);
  Add('FLOAT', rwkFloat);
  Add('CURRENCY', rwkCurrency);
  Add('VARIANT', rwkVariant);
  Add('STRING', rwkString);
  Add('DATE', rwkDate);
  Add('POINTER', rwkPointer);
  Add('OBJECT', rwkPointer);
  Add('IF', rwkIf);
  Add('THEN', rwkThen);
  Add('ELSE', rwkElse);
  Add('ELSEIF', rwkElseIf);
  Add('ENDIF', rwkEndIf);
  Add('ARRAY', rwkArray);
  Add('FOR', rwkFor);
  Add('ENDFOR', rwkEndFor);
  Add('TO', rwkTo);
  Add('DO', rwkDo);
  Add('WHILE', rwkWhile);
  Add('ENDWHILE', rwkEndWhile);
  Add('INHERITED', rwkInherited);
  Add('NIL', rwkNil);
  Add('EXIT', rwkExit);
  Add('BREAK', rwkBreak);
  Add('CONTINUE', rwkContinue);

  Sorted := True;
end;

function TrwpKeyWords.GetItem(AIndex: Integer): TrwpKeyWordRec;
begin
  Result.Text := Strings[AIndex];
  Result.Code := TrwpTypeKeyWord(Objects[AIndex]);
end;

function TrwpKeyWords.Add(AText: string; ACode: TrwpTypeKeyWord): Integer;
begin
  Result := AddObject(AText, Pointer(ACode));
end;

function TrwpKeyWords.FindKeyWord(AToken: string): TrwpTypeKeyWord;
var
  i: Integer;
begin
  i := IndexOf(AnsiUpperCase(AToken));
  if i = -1 then
    Result := rwkUnknown
  else
    Result := Items[i].Code;
end;

function TrwpKeyWords.FindKeyWordByCode(AKeyWord: TrwpTypeKeyWord): string;
var
  i: Integer;
begin
  i := IndexOfObject(Pointer(AKeyWord));
  if i = -1 then
    Result := ''
  else
    Result := Items[i].Text;
end;



{ TrwpRWFunctions }

function TrwpRWFunctions.AddFunc(const AGroup: String; const AHeader: String; AType: TrwpTypeFunction; AParser: TrwParser;
                                 const AddInfo: PTrwExtFunctAddInfoRec = nil; const AExtType: Integer = 0): TrwpRWFunction;
var
  P, Pb, Pe: Integer;
begin
  Result := TrwpRWFunction(Add);
  Result.Name := ParseFunctionHeader(AHeader, Result.Params, Pb, Pe, P);
  Result.TypeFunct := AType;
  Result.ExternalTypeFunct := AExtType;
  Result.Group := AGroup;
  if AGroup <> '' then
    Result.DesignTimeInfo := [fdtShowInEditor];

  if Assigned(AddInfo) then
    SetFunctionAddInfo(Result, AddInfo);
end;

procedure TrwpRWFunctions.SetFunctionAddInfo(AFunct: TrwpRWFunction; AddInfo: PTrwExtFunctAddInfoRec);
var
  i, n: Integer;
  Par: TrwFuncParam;
  Domain: TIniString;
  DomainArr: TrwDomain;
  h: String;
begin
  AFunct.DisplayName := AddInfo.DisplayName;
  AFunct.Group := AddInfo.GroupName;
  AFunct.Description := AddInfo.Description;

  if AddInfo.IsTextFilter then
    AFunct.DesignTimeInfo := AFunct.DesignTimeInfo + [fdtDataFilter]
  else
    AFunct.DesignTimeInfo := AFunct.DesignTimeInfo - [fdtDataFilter];

   for i := Low(AddInfo.ParamsInfo) to High(AddInfo.ParamsInfo) do
   begin
     Par := AFunct.Params.ParamByName(AddInfo.ParamsInfo[i].Name);
     if Assigned(Par) then
     begin
       Par.DisplayName := AddInfo.ParamsInfo[i].DisplayName;
       Domain := AddInfo.ParamsInfo[i].Domain;
       n := 0;
       while Domain <> '' do
       begin
         if High(DomainArr) < n then
           SetLength(DomainArr, Length(DomainArr) + 100);
         h := GetNextStrValue(Domain, #13);
         DomainArr[n].Value := Par.StrToDomainValue(GetNextStrValue(h, '='));
         DomainArr[n].Description := h;
         Inc(n);
       end;
       SetLength(DomainArr, n);

       Par.Domain := DomainArr;
       Par.DefaultValue := AddInfo.ParamsInfo[i].DefaulValue;
     end;
   end;
end;

procedure TrwpRWFunctions.Initialize;
var
  Prs: TrwParser;
  ExtFuncts: TIniString;
  h, ft: String;
  EmptyVar: Variant;
  Funct: TrwpRWFunction;
  FnctAddInfo: TrwExtFunctAddInfoRec;

  function CreateAddInfo(const ADisplayName, AGroupName, ADescription: String; const AIsTextFilter: Boolean;
                         const AParamsInfo: array of TrwFunctParamInfoRec): PTrwExtFunctAddInfoRec;
  var
    i: Integer;
  begin
    FnctAddInfo.DisplayName := ADisplayName;
    FnctAddInfo.GroupName := AGroupName;
    FnctAddInfo.Description := ADescription;
    FnctAddInfo.IsTextFilter := AIsTextFilter;

    SetLength(FnctAddInfo.ParamsInfo, Length(AParamsInfo));

    for i := Low(AParamsInfo) to High(AParamsInfo) do
      FnctAddInfo.ParamsInfo[i] := AParamsInfo[i];

    Result := @FnctAddInfo;
  end;

  function CreateParamInfo(const AName, ADisplayName: String; const ADomain: TIniString; const ADefaulValue: String): TrwFunctParamInfoRec;
  begin
    Result.Name := AName;
    Result.DisplayName := ADisplayName;
    Result.Domain := ADomain;
    Result.DefaulValue := ADefaulValue;
  end;


begin
  VarClear(EmptyVar);

  Prs := TrwParser.Create;
  try
    AddFunc('', 'function True (): Boolean', rwfTrue, Prs);
    AddFunc('', 'function False (): Boolean', rwfFalse, Prs);
    AddFunc('', 'function Null (): Variant', rwfNull, Prs);
    AddFunc('', 'procedure SetArrLength (var Arr: Array; Len: Integer)', rwfSetArrLength, Prs);
    AddFunc('String Routines', 'function Length (Str: String): Integer  function', rwfLength, Prs);
    AddFunc('', 'function ArrLength (var Arr: Array): Integer', rwfArrLength, Prs);
    AddFunc('Type Conversion', 'function AsString (Value: Variant): String', rwfAsString, Prs);
    AddFunc('Type Conversion', 'function AsInteger (Value: Variant): Integer', rwfAsInteger, Prs);
    AddFunc('Type Conversion', 'function AsFloat (Value: Variant): Float', rwfAsFloat, Prs);
    AddFunc('Type Conversion', 'function AsCurrency (Value: Variant): Currency', rwfAsCurrency, Prs);
    AddFunc('', 'function AsVariant (Value: Variant): Variant', rwfAsVariant, Prs);
    AddFunc('Type Conversion', 'function AsDate (Value: Variant): Date', rwfAsDate, Prs);
    AddFunc('Type Conversion', 'function AsBoolean (Value: Variant): Boolean', rwfAsBoolean, Prs);

    AddFunc('', 'function Format (FormatStr: String; Value: Variant): String', rwfFormat, Prs,
              CreateAddInfo('', 'Misc', '', False,
                            [CreateParamInfo('FormatStr', '', '', '#,##0.00')])
            );

    AddFunc('', 'function SubstSubStr (Str: String; SubStr: String; SubstStr: String): String', rwfSubstSubStr, Prs,
            CreateAddInfo('Replace Text', 'String Routines', 'Substitute text pattern with other text', True,
                          [CreateParamInfo('SubStr', 'Text Pattern', '', ''),
                           CreateParamInfo('SubstStr', 'Replace with', '', '')])
           );

    AddFunc('String Routines', 'function PosSubStr (Str: String; SubStr: String): Integer', rwfPosSubStr, Prs);

    AddFunc('', 'function CopySubStr (Str: String; BegPos: Integer; Length: Integer): String', rwfCopySubStr, Prs,
              CreateAddInfo('Text Part', 'String Routines', 'Returns part of text', True,
                            [CreateParamInfo('BegPos', 'Starts from', '', '1'),
                             CreateParamInfo('Length', 'Text Length', '', '1')])
            );


    AddFunc('', 'function DelSubStr (Str: String; BegPos: Integer; Length: Integer): String', rwfDelSubStr, Prs,
              CreateAddInfo('Delete Text', 'String Routines', 'Deletes a fragment of text', True,
                            [CreateParamInfo('BegPos', 'Starts from', '', '1'),
                             CreateParamInfo('Length', 'Text Length', '', '1')])
            );


    AddFunc('Math Routines', 'function Round (Value: Float; Precision: Integer): Float', rwfRound, Prs,
            CreateAddInfo('', 'Math Routines', '', False,
              [CreateParamInfo('Precision', '', '', '0')])
           );

    AddFunc('Math Routines', 'function RandRange(AFrom: Integer; ATo: Integer): Integer', rwfRandomRange, Prs,
              CreateAddInfo('', 'Math Routines', '', False,
                            [CreateParamInfo('AFrom', '', '', '0'),
                             CreateParamInfo('ATo', '', '', IntToStr(MaxInt))])
            );


    AddFunc('Math Routines', 'function Trunc (Value: Float): Integer', rwfTrunc, Prs);
    AddFunc('Date Routines', 'function Today (): Date', rwfToday, Prs);
    AddFunc('Date Routines', 'function Time (): Date', rwfTime, Prs);
    AddFunc('Date Routines', 'function Now (): Date', rwfNow, Prs);


    AddFunc('', 'function SetTime (Dat: Date; Time: String): Date', rwfSetTime, Prs,
            CreateAddInfo('', 'Date Routines', '', False,
              [CreateParamInfo('Time', '', '', '23:59:59')])
           );

    AddFunc('Misc', 'function Chr (Code: Integer): String', rwfChr, Prs);
    AddFunc('Misc', 'function Ord (Symbol: String): Integer', rwfOrd, Prs);
    AddFunc('', 'function CreateObject (ClassName: String; Owner: TrwComponent): Pointer', rwfCreateObject, Prs);
    AddFunc('', 'function DestroyObject (AObject: Pointer): Boolean', rwfDestroyObject, Prs);
    AddFunc('', 'function IndexOfArrayElement (var Arr: Array; Elem: Variant): Integer', rwfIndexOfArrayElement, Prs);

    AddFunc('String Routines', 'function UpperCase (Str: String): String', rwfUpperCase, Prs,
              CreateAddInfo('Uppercase', 'String Routines', 'Makes all letters uppercase', True, []));


    AddFunc('String Routines', 'function LowerCase (Str: String): String', rwfLowerCase, Prs);
              CreateAddInfo('Lowercase', 'String Routines', 'Makes all letters lowercase', True, []);

    AddFunc('String Routines', 'function TrimLeft (Str: String): String', rwfTrimLeft, Prs,
              CreateAddInfo('Trim Left', 'String Routines', 'Deletes leading spaces', True, []));

    AddFunc('String Routines', 'function TrimRight (Str: String): String', rwfTrimRight, Prs,
              CreateAddInfo('Trim Right', 'String Routines', 'Deletes trailing spaces', True, []));

    AddFunc('String Routines', 'function Trim (Str: String): String', rwfTrim, Prs,
              CreateAddInfo('Trim', 'String Routines', 'Deletes leading and trailing spaces', True, []));

    AddFunc('', 'procedure ShowError (Message: String)', rwfShowError, Prs);
    AddFunc('', 'procedure Abort (ReturnValue: Variant)', rwfAbort, Prs);
    AddFunc('', 'function TypeOf (Expression: Variant): Integer', rwfTypeOf, Prs);
    AddFunc('Math Routines', 'function Sign (Value: Float): Integer', rwfSign, Prs);
    AddFunc('Math Routines', 'function Abs (Value: Float): Float', rwfAbs, Prs);
    AddFunc('', 'function DesignerOpened : Boolean', rwfDesignerOpened, Prs);

    AddFunc('', 'function Dictionary (): TrwDictionary', rwfDictionary, Prs);
    AddFunc('', 'function LibComponents (): TrwLibComponents', rwfLibComponents, Prs);
    AddFunc('', 'function LibFunctions (): TrwLibFunctions', rwfLibFunctions, Prs);
    AddFunc('', 'function FileExists(AFileName: String): Boolean', rwfFileExists, Prs);
    AddFunc('', 'procedure SkipPage ()', rwfSkipPage, Prs);
    AddFunc('', 'procedure SetPrintPos (NewPrintPos: Integer)', rwfSetPrintPos, Prs);
    AddFunc('', 'function GetPrintPos (): Integer', rwfGetPrintPos, Prs);
    AddFunc('', 'function PageNumber (): Integer', rwfPageNumber, Prs);
    AddFunc('', 'procedure AppendASCIILine (AText: String)', rwfAppendASCIILine, Prs);
    AddFunc('', 'procedure InsertASCIILine (AText: String; ALine: Integer)', rwfInsertASCIILine, Prs);
    AddFunc('', 'procedure UpdateASCIILine (AText: String; ALine: Integer)', rwfUpdateASCIILine, Prs);
    AddFunc('', 'procedure DeleteASCIILine (ALine: Integer)', rwfDeleteASCIILine, Prs);
    AddFunc('', 'function GetASCIILine (ALine: Integer): String', rwfGetASCIILine, Prs);
    AddFunc('', 'function ASCIILineCount (): Integer', rwfASCIILineCount, Prs);
    AddFunc('', 'function SubstValue (Value: Variant; ASubstID: String): Variant', rwfSubstValue, Prs);
    AddFunc('Date Routines', 'function TimeZoneOffset (): Integer', rwfTimeZoneOffset, Prs);


    //Load External Functions
    ExtFuncts := '';
    RWEngineExt.AppAdapter.GetExternalFunctionDeclarations(ExtFuncts);
    while ExtFuncts <> '' do
    begin
      ft := GetNextStrValue(ExtFuncts, #13);
      h := GetNextStrValue(ft, '=');
      Funct := AddFunc('Misc', h, rwfExternal, Prs, nil, StrToInt(Trim(ft)));

      with FnctAddInfo do
      begin
        DisplayName := '';
        GroupName := '';
        Description := '';
        IsTextFilter := False;
        ParamsInfo := nil;
      end;

      FnctAddInfo := RWEngineExt.AppAdapter.GetExternalFunctionAdditionalInfo(Funct.Name);
      SetFunctionAddInfo(Funct, @FnctAddInfo);
    end;

  finally
    Prs.Free;
  end;
end;



// Scanner

procedure ScannerPutBackToken(var AStatus: TrwScannerRes);
begin
  if AStatus.sWasBack then
    raise ErwException.Create(ResErrInternalCompileError);
  ScannerChangePosition(AStatus, AStatus.sPos - Length(AStatus.sToken));
  AStatus.sWasBack := True;
end;


procedure ScannerChangePosition(var AStatus: TrwScannerRes; const APos: Integer);
begin
  AStatus.sPos := APos;
  AStatus.sWasBack := False;
end;


procedure ScannerCheckNextToken(var AStatus: TrwScannerRes; const AKeyWord: TrwpTypeKeyWord);
begin
  ScannerGetNextToken(AStatus);
  if (AStatus.sKeyWord <> AKeyWord) then
  begin
    AStatus.sKeyWord := AKeyWord;
    raise ErwException.CreateFmt(ResErrNExpToken, [KeyWords.FindKeyWordByCode(AStatus.sKeyWord), AStatus.sToken])
  end;
end;


procedure ScannerCheckNextTokenType(var AStatus: TrwScannerRes; const ATokenType: TrwpTypeToken);
begin
  ScannerGetNextToken(AStatus);
  if (AStatus.sTokenType <> ATokenType) then
    raise ErwException.Create(ResErrSyntaxError);
end;


procedure ScannerGetNextToken(var AStatus: TrwScannerRes);
var
  l_tok: TrwpTypeKeyWord;
  l_token: string;
  i: Integer;

  function isDigit(c: Char): Boolean;
  begin
    if ((c >= '0') and (c <= '9')) then
      Result := True
    else
      Result := False;
  end;

  function isLetter(c: Char): Boolean;
  begin
    if ((c >= 'A') and (c <= 'Z') or (c >= 'a') and (c <= 'z') or isDigit(c) or
      (c in ['_'])) then
      Result := True
    else
      Result := False;
  end;

  procedure ReadQuotedString;

    procedure ReadDoubleQuote;
    begin
      with AStatus do
        while True do
          if (sText[sPos] = '''') and (sText[sPos + 1] = '''') then
          begin
            sToken := sToken + '''';
            Inc(sPos, 2);
          end
          else
            Break;
    end;

  begin
    with AStatus do
    begin
      repeat
        ReadDoubleQuote;
        if (sText[sPos] <> '''') then
        begin
          sToken := sToken + sText[sPos];
          Inc(sPos);
          ReadDoubleQuote;
        end;
      until ((sText[sPos] = '''') or (sText[sPos] in [#10, #13, #0]));

      if (sText[sPos] in [#10, #13, #0]) then
        raise ErwException.Create(ResErrNClQuotes);
    end;
  end;


begin
  with AStatus do
  begin
    if sWasBack then
    begin
      ScannerChangePosition(AStatus, sPos + Length(sToken));
      Exit;
    end;

    sPrevToken := sToken;
    sToken := '';
    sTokenType := rwtUnknown;
    sKeyWord := rwkUnknown;

    while (sText[sPos] = ' ') or (sText[sPos] in [#10, #13]) do {Skip spaces}
      Inc(sPos);

    if (sText[sPos] = #0) then {Is it end text? }
    begin
      sKeyWord := rwkEndOfText;
      sTokenType := rwtKeyWord;
    end

    else if (sText[sPos] = '''') then {Is it quoted text?}
    begin
      Inc(sPos);
      ReadQuotedString;
      sToken := sToken;
      Inc(sPos);
      sTokenType := rwtString;
    end

    else if IsDigit(sText[sPos]) then {Is it number?}
    begin
      while IsDigit(sText[sPos]) or (sText[sPos] = '.') do
      begin
        sToken := sToken + sText[sPos];
        Inc(sPos);
      end;

      if AnsiUpperCase(sText[sPos]) = 'E' then
      begin
        sToken := sToken + 'E';
        Inc(sPos);
        if sText[sPos] in ['+', '-'] then
        begin
          sToken := sToken + sText[sPos];
          Inc(sPos);
        end;
        while IsDigit(sText[sPos]) do
        begin
          sToken := sToken + sText[sPos];
          Inc(sPos);
        end;
      end;

      sTokenType := rwtNumber;
    end

    else if isLetter(sText[sPos]) then {Is it keyword ?}
    begin
      while isLetter(sText[sPos]) do
      begin
        sToken := sToken + sText[sPos];
        Inc(sPos);
      end;

      sKeyWord := KeyWords.FindKeyWord(sToken);
      if (sKeyWord = rwkUnknown) then
        sTokenType := rwtUnknown
      else
        sTokenType := rwtKeyword;
    end

    else
    begin
      sKeyWord := KeyWords.FindKeyWord(sText[sPos]); {Is it symbol?}
      sTokenType := rwtSymbol;
      l_tok := rwkUnknown;

      while (sKeyWord <> rwkUnknown) do
      begin
        l_tok := sKeyWord;
        sToken := sToken + sText[sPos];
        sPos := sPos + 1;

        if (sText[sPos] = #0) then break;

        sKeyWord := KeyWords.FindKeyWord(sToken + sText[sPos]);
      end;

      if (sToken <> '') then
      begin
        sKeyWord := l_tok;
        sTokenType := rwtSymbol;

        if (sKeyWord = rwkRem) then { Is it line comment? }
        begin
          while not (sText[sPos] in [#0, #10, #13]) do
            Inc(sPos);
          ScannerGetNextToken(AStatus);
        end

        else if (sKeyWord = rwkRemb) then { Is it begin of comment? }
        begin
          l_token := KeyWords.FindKeyWordByCode(rwkReme);
          while sText[sPos] <> #0 do
          begin
            sToken := '';
            for i := 0 to Length(l_token) - 1 do
              sToken := sToken + sText[sPos + i];
            if (sToken = l_token) then
              break;
            Inc(sPos);
          end;

          if (sText[sPos] = #0) then
          begin
            sToken := l_token;
            raise ErwException.CreateFmt(ResErrNExpToken, [KeyWords.FindKeyWordByCode(sKeyWord), sToken]);
          end;

          sPos := sPos + Length(l_token);

          ScannerGetNextToken(AStatus);
        end
      end

      else
      begin
        sToken := sText[sPos];
        raise ErwException.CreateFmt(ResErrUnknSymbol, [sToken]);
      end;
    end
  end;
end;



function ParseFunctionHeader(const AText: string; const AParams: TrwFuncParams;
  var AParBegPos, AParEndPos, AEndPos: Integer): string;
var
  fl_var: Boolean;
  Proc: Boolean;
  ParName: string;
  i: Integer;
  lStatus: TrwScannerRes;
  OldParams: TrwFuncParams;
  Par: TrwFuncParam;  

  procedure ParseType(const PVar: PTrwVariable);
  begin
    with lStatus do
      if sTokenType = rwtKeyword then
        PVar^.VarType := KeyWordToVarType(sKeyWord)
      else
      begin
        PVar^.VarType := rwvPointer;
        PVar^.PointerType := sToken;
        if rwRTTIInfo.FindClass(sToken) = nil then
          raise ErwException.CreateFmt(ResErrWrongClass, [sToken]);
      end;
  end;


begin
  OldParams := nil;
  try
    with lStatus do
    begin
      if Assigned(AParams) then
      begin
        OldParams := TrwFuncParams.Create(TrwFuncParam);
        OldParams.Assign(AParams);
        AParams.Clear;
      end;

      sText := AText+#0;
      sPos := 1;
      sWasBack := False;
      AParBegPos := 1;
      AParEndPos := 1;

      ScannerGetNextToken(lStatus);
      if not (sKeyWord in [rwkFunction, rwkProcedure]) then
      begin
        sKeyWord := rwkFunction;
        raise ErwException.CreateFmt(ResErrNExpToken, [KeyWords.FindKeyWordByCode(sKeyWord), sToken]);
      end
      else
        Proc := (sKeyWord = rwkProcedure);

      ScannerGetNextToken(lStatus);
      if (sTokenType <> rwtUnknown) then
        raise ErwException.Create(ResErrSyntaxError);
      Result := sToken;

      ScannerGetNextToken(lStatus);
      if (sKeyWord = rwkLBracket) then
      begin
        AParBegPos := sPos;
        ScannerGetNextToken(lStatus);
        if (sKeyWord <> rwkRBracket) then
        begin
          ScannerPutBackToken(lStatus);
          repeat
            fl_var := False;
            ScannerGetNextToken(lStatus);
            if (sKeyWord = rwkVar) then
              fl_var := True
            else if (sKeyWord = rwkRBracket) then
              break

            else
              ScannerPutBackToken(lStatus);

            ScannerGetNextToken(lStatus);
            if (sTokenType <> rwtUnknown) then
              raise ErwException.Create(ResErrSyntaxError);
            ParName := sToken;

            ScannerCheckNextToken(lStatus, rwkColon);
            if Assigned(AParams) then
              i := AParams.AddParam(fl_var, ParName).Index
            else
              i := -1;

            ScannerGetNextToken(lStatus);
            if Assigned(AParams) then
              ParseType(AParams[i].Descriptor);

            ScannerGetNextToken(lStatus);
          until (sKeyWord <> rwkSemicolomn);
          ScannerPutBackToken(lStatus);
          AParEndPos := sPos;
          ScannerCheckNextToken(lStatus, rwkRBracket);
        end;
      end
      else
        ScannerPutBackToken(lStatus);

      if not Proc then
      begin
        ScannerCheckNextToken(lStatus, rwkColon);
        ScannerGetNextToken(lStatus);
        if Assigned(AParams) then
          ParseType(AParams.Result);
      end
      else
        if Assigned(AParams) then
          AParams.Result.VarType := rwvUnknown;

      AEndPos := sPos;

      if Assigned(AParams) then
        for i := 0 to OldParams.Count - 1 do
        begin
          Par := AParams.ParamByName(OldParams[i].Descriptor.Name);
          if Assigned(Par) then
          begin
            Par.DisplayName := OldParams[i].DisplayName;
            Par.Domain := OldParams[i].Domain;
            Par.DefaultValue := OldParams[i].DefaultValue;
          end;
        end;
    end;

  finally
    FreeAndNil(OldParams);
  end;
end;


initialization

finalization
  if Assigned(FRWFunctionsSrc) then
    FRWFunctionsSrc.Free;

  if Assigned(FKeyWordsSrc) then
    FKeyWordsSrc.Free;

end.
