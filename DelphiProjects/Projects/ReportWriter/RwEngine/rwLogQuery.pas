unit rwLogQuery;

interface

uses Classes, Windows, SysUtils, rwDataDictionary,
     rwTypes, rwUtils, DB, Variants, rwMessages, rwVirtualMachine,
     sbAPI, sbSQL, sbConstants, Dialogs, Registry, rwCustomDataDictionary,
     rwBasicUtils, rwEngineTypes;


type

  TrwLQCachedTable = class;
  TrwLQCachedTableImage = class;


  TrwLQVirtualTable = class(TsbTableCursor)
  private
    FrwDDTable: TrwDataDictTable;
    FCacheTable: TrwLQCachedTable;
    procedure ClearAuxParams;

  protected
  public
    constructor Create; override;
    destructor  Destroy; override;
    procedure   Initial(ArwDDTable: TrwDataDictTable);
  end;


  TrwLQCachedTableImage = class(TsbInterfacedObject, IrwCachedTableImage)
  private
    FOwner: TrwLQCachedTable;
    FParams: TsbParams;
    FPhysicalSBName: string;

    // Interface Implementation
    function CachedTable: IrwCachedTable;
    function GetParams: TrwParamList;
    function GetSBTable: IrwSBTable;
    procedure SetPhysicalSBName(const Value: string);

  public
    constructor Create;
    destructor  Destroy; override;
    procedure   Cache;

    property Params: TsbParams read FParams;
    property PhysicalSBName: string read FPhysicalSBName write SetPhysicalSBName;
  end;


  TrwLQCachedTable = class(TInterfacedCollectionItem, IrwCachedTable)
  private
    FFields: TStringList;
    FTableName: String;
    FrwDDTable: TrwDataDictTable;
    FFilter: string;
    FImages: TList;
    procedure OnChangeFields(Sender: TObject);
    function  GetItem(Index: Integer): TrwLQCachedTableImage;
    function  Add: TrwLQCachedTableImage;
    procedure ReadParamsData(Reader: TReader);
    procedure WriteParamsData(Writer: TWriter);

    // Interface Implementation
    function  ddTable: IrwDDTable;
    function  GetTableImageByParams(const Params: array of TrwParamRec): IrwCachedTableImage;
    function  GetFields: TrwStrList;
    procedure SetFields(const FieldList: TrwStrList);
    function  GetExternalFields: TrwStrList;
    function  GetFilter: String;

  protected
    procedure DefineProperties(Filer: TFiler); override;
  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    function    FindImage(AParams: TsbParams): Integer;
    procedure   DeleteFiles;
    property    Items[Index: Integer]: TrwLQCachedTableImage read GetItem; default;
  published
    property TableName: String read FTableName;
    property Fields: TStringList read FFields;
    property Filter: string read FFilter;
  end;


  TrwLQCacheInfo = class(TInterfacedCollection, IrwDBCache)
  private
    FUniqCounter: Integer;
//    FPresenceMutex: THandle;
    FCachePath: String;
    FPathLock: THandle;
    function  GetItem(Index: Integer): TrwLQCachedTable;
    function  Add(ATableName: string): TrwLQCachedTable;
    procedure InitCachePath;
    procedure ClearCachePath;

    function  GetCachedTable(const ATable: String): IrwCachedTable;

    procedure EmptyCachedTables;

  public
    constructor Create(ItemClass: TCollectionItemClass);
    destructor  Destroy; override;
    procedure GatherSQLInfo(AParser: TsbSQLParser);
    function  FindTable(const AName: String): Integer;
    function  CopyTable(AIndex: Integer): TrwLQCachedTable;
    property  Items[Index: Integer]: TrwLQCachedTable read GetItem; default;
    procedure SaveToStream(AStream: TStream);

   // Interface Implementation
    function  GetCachePath: String;
    procedure ClearCache;
  end;


                       {*** Logical Query ***}

  TrwLQQuery = class(TsbTableCursor)
  private
    FSQL: TStrings;
    FParams: TsbParams;
    FParser: TsbSQLParser;
    FResInfo: TsbResultRec;
    FOriginalIndexName: String;

    procedure OnChangeSQL(Sender: TObject);
    procedure SetSQL(ASQL: TStrings);

    procedure SetParams(const Value: TsbParams);
    procedure FreeSQLInfo;

  public
    constructor Create; override;
    destructor  Destroy; override;
    procedure   Close; override;
    procedure   CloseIndex(AIndexName: string); override;
    procedure   SetIndex(AIndexName: string; ATemporary: Boolean = False); override;
    procedure   OpenSQL;
    procedure   ExecSQL;
    procedure   Prepare;

    property SQL: TStrings read FSQL write SetSQL;
    property Params: TsbParams read FParams write SetParams;
    property ResInfo: TsbResultRec read FResInfo;
  end;


  procedure InitThreadVars_lqQuery;
  procedure InitCacheInfo;
  procedure FreeCacheInfo;
  function  CacheInfo: TrwLQCacheInfo;

implementation

uses  rwEngine, rwBasicClasses, rwDB, isBasicUtils;

const cCompSelectedFieldsParam = 'QuerySelectedFields';
      cPathLockFileName = 'pathlock.tmp';


threadvar
  FCacheInfo: TrwLQCacheInfo;
  FCacheInfoCounter: Integer;


procedure InitThreadVars_lqQuery;
begin
  FCacheInfo := nil;
  FCacheInfoCounter := 0;
end;


function CacheInfo: TrwLQCacheInfo;
begin
  Result := FCacheInfo;
end;


procedure InitCacheInfo;
begin
  if not Assigned(FCacheInfo) then
  begin
    FCacheInfo := TrwLQCacheInfo.Create(TrwLQCachedTable);
    FCacheInfoCounter := 1;
    InitializationSbVM;
  end
  else
    Inc(FCacheInfoCounter);
end;


procedure FreeCacheInfo;
begin
  Dec(FCacheInfoCounter);
  if FCacheInfoCounter < 0 then
    FCacheInfoCounter := 0;

  if Assigned(FCacheInfo) and (FCacheInfoCounter = 0) then
    try
      FreeSysIndPool;
    finally
      try
        DeInitializationSbVM;
      finally
        try
          FreeAndNil(FCacheInfo);
        finally
          try
            FreeDataDictionary;
          finally
            InitThreadVars_rwShared;
          end;
        end;
      end;
    end;
end;


function OpenVirtualTable(ATableName: string): TsbTableCursor;
var
  T: TrwDataDictTable;
begin
  if ATableName = '' then
    Result := TrwLQVirtualTable.Create
  else
  begin
    T := TrwDataDictTable(DataDictionary.Tables.TableByName(ATableName));
    if Assigned(T) then
    begin
      Result := TrwLQVirtualTable.Create;
      TrwLQVirtualTable(Result).Initial(T);
    end
    else
      raise ESBParser.CreateFmt(ResErrTableNotFound, [ATableName]);
  end;
end;


function OpenVirtualTable2(ATableName: string): TsbTableCursor;
var
  i: Integer;
begin
  i := CacheInfo.FindTable(ATableName);
  if i = -1 then
    raise ErwException.CreateFmt(ResErrCacheTable, [ATableName]);

  Result := TrwLQVirtualTable.Create;
  Result.InternalTag := 0;
  Result.TableName := ATableName;
  Result.AuxParams.Count := CacheInfo[i][0].Params.ItemCount;
end;


procedure OpenVirtualTable3(ATable: TsbTableCursor; AAccessMode: TsbAccessModeType);
var
  i, k: Integer;
  P: TsbParams;
  T: TrwLQCachedTable;
  Img: TrwLQCachedTableImage;
begin
  P := TsbParams.Create;
  InitializationSbVM;
  try
    i := CacheInfo.FindTable(ATable.TableName);
    if i = -1 then
      raise ErwException.CreateFmt(ResErrCacheTable, [ATable.TableName])
    else
      T := CacheInfo[i];

    for i := 0 to ATable.AuxParams.Count - 1 do
    begin
      k := P.Add(IntToStr(i));
      VM.CalcExpression(ATable.AuxParams[i]);
      P[k].Value := VM.RegA;
    end;

    i := T.FindImage(P);
    if i = -1 then
    begin
      i := CacheInfo.FindTable(ATable.TableName);
      T := CacheInfo[i];
      Img := T.Add;
      for i := 0 to T.FrwDDTable.Params.Count - 1 do
        P[i].Name := T.FrwDDTable.Params[i].Name;
      Img.Params.Assign(P);
    end
    else
      Img := T[i];

    Img.Cache;
    ATable.TableName := Img.PhysicalSBName;
    ATable.Open(AAccessMode);

  finally
    DeInitializationSbVM;
    P.Free;
  end;
end;


procedure CustomParsingEndProc(AParser: TsbSQLParser);
begin
  CacheInfo.GatherSQLInfo(AParser);
end;



         {TrwLQQuery}

constructor TrwLQQuery.Create;
begin
  inherited;

  FTemporary := True;
  FSQL := TStringList.Create;
  TStringList(FSQL).OnChange := OnChangeSQL;
  FParams := TsbParams.Create;
  FParser := nil;
  FOriginalIndexName := '';
end;


destructor TrwLQQuery.Destroy;
begin
  Close;
  FSQL.Free;
  FParams.Free;
  FreeSQLInfo;
  inherited;
end;


procedure TrwLQQuery.SetSQL(ASQL: TStrings);
begin
  if ASQL <> nil then
    FSQL.Assign(ASQL);
end;


procedure TrwLQQuery.OnChangeSQL(Sender: TObject);
begin
  Close;
  FreeSQLInfo;
  FParams.ParseSQL(SQL.Text, True);
end;



procedure TrwLQQuery.ExecSQL;
var
  lTime, t: DWORD;

  procedure CreateBuffer;
  var
    h: string;
    j: Integer;
    lTableName: string;
    lPhysType: TDataDictDataType;
    T: TrwLQCachedTable;
    rwT: TrwDataDictTable;
    rwF: TrwDataDictField;
  begin
    DataDictionary.UpdateSysTablesStructure;

    lTableName := FParser.SQLExecInfo.TableName;
    rwT := TrwDataDictTable(DataDictionary.Tables.TableByName(lTableName));
    if Assigned(rwT) then
    begin
      if rwT.TableType <> rwtBuffer then
        raise ErwException.CreateFmt(ResErrWrongOperationForTable, [FParser.SQLExecInfo.TableName]);

      j := CacheInfo.FindTable(lTableName);
      if j <> -1 then
        CacheInfo.Delete(j);

      j := rwT.BufferCreationCount;
      h := rwT.ExternalName;
      DataDictionary.DeleteTable(rwT);
      try
        sbDropTable(h);
      except
      end;
    end
    else
    begin
      h := sbGetUniqueFileName(CacheInfo.GetCachePath, 'b', sbTableExt);
      j := 0;
    end;

    rwT := TrwDataDictTable(DataDictionary.Tables.AddTable);
    rwT.Name := lTableName;
    rwT.TableType := rwtBuffer;
    rwT.GroupName := DDG_TEMPTABLES;
    rwT.ExternalName := h;
    rwT.BufferCreationCount := j + 1;

    for j := 0 to FParser.SQLExecInfo.FieldList.Count - 1 do
    begin
      case FParser.SQLExecInfo.FieldList[j].FieldType of
        sbfString:    lPhysType := ddtString;
        sbfInteger:   lPhysType := ddtInteger;
        sbfDateTime:  lPhysType := ddtDateTime;
        sbfFloat:     lPhysType := ddtFloat;
        sbfCurrency:  lPhysType := ddtCurrency;
        sbfBoolean:   lPhysType := ddtBoolean;
        sbfBLOB:      lPhysType := ddtBLOB;
      else
        raise ErwException.Create(ResErrInternalLQError);
      end;

      rwF := TrwDataDictField(rwT.Fields.AddField);
      rwF.Name := FParser.SQLExecInfo.FieldList[j].FieldName;
      rwF.ExternalName := rwF.Name;
      rwF.FieldType := lPhysType;
    end;

    FParser.SQLExecInfo.TableName := h;
    try
      FParser.Execute;

      T := CacheInfo.Add(AnsiUpperCase(lTableName));
      T.FrwDDTable := rwT;
      for j := 0 to FParser.SQLExecInfo.FieldList.Count - 1 do
        T.Fields.Add(AnsiUpperCase(FParser.SQLExecInfo.FieldList[j].FieldName));
      T.Add.PhysicalSBName := h;

    except
      rwT.BufferCreationCount := rwT.BufferCreationCount - 1;
      if rwT.BufferCreationCount = 0 then
        DataDictionary.DeleteTable(rwT);
      j := CacheInfo.FindTable(lTableName);
      if j <> -1 then
        CacheInfo.Delete(j);
      raise;
    end;
  end;


  procedure DropBuffer;
  var
    i: Integer;
    lTableName: string;
    rwT: TrwDataDictTable;
  begin
    DataDictionary.UpdateSysTablesStructure;

    lTableName := FParser.SQLExecInfo.TableName;
    rwT := TrwDataDictTable(DataDictionary.Tables.TableByName(lTableName));

    if not Assigned(rwT) then
      raise ErwException.CreateFmt(ResErrTableNotFound, [FParser.SQLExecInfo.TableName]);
    if rwT.TableType <> rwtBuffer then
      raise ErwException.CreateFmt(ResErrWrongOperationForTable, [FParser.SQLExecInfo.TableName]);

    FParser.SQLExecInfo.TableName := rwT.ExternalName;

    rwT.BufferCreationCount := rwT.BufferCreationCount - 1;
    if rwT.BufferCreationCount = 0 then
      try
        FParser.Execute;
        i := CacheInfo.FindTable(lTableName);
        if i <> -1 then
          CacheInfo.Delete(i);
      finally
        DataDictionary.DeleteTable(rwT);
      end;
  end;


begin
  InitCacheInfo;
  try
    if not Assigned(FParser) then
      Prepare;

    if Assigned(FParser.SQLExecInfo) then
    begin
      if not RwStatRec.Lock then
      begin
        lTime := GetTickCount;
        t := RwStatRec.ConnectionTime + RwStatRec.RemoteQueriesTime + RwStatRec.CacheDataTime;
      end
      else
      begin
        lTime := 0;
        t := 0;
      end;

      if FParser.SQLExecInfo.SQLType = sesCreate then
        CreateBuffer
      else if FParser.SQLExecInfo.SQLType = sesDrop then
        DropBuffer
      else
        FResInfo := CustomSQL(SQL.Text, Params, nil, @OpenVirtualTable2, @OpenVirtualTable3);

      if not RwStatRec.Lock then
      begin
        lTime := GetTickCount - lTime - (RwStatRec.ConnectionTime + RwStatRec.RemoteQueriesTime + RwStatRec.CacheDataTime - t);
        Inc(RwStatRec.LogicalQueriesTime, lTime);
        Inc(RwStatRec.LogicalQueries);
      end;
    end;
  finally
    FreeCacheInfo;
  end;
end;


procedure TrwLQQuery.SetParams(const Value: TsbParams);
begin
  FParams.Assign(Value);
end;


procedure TrwLQQuery.OpenSQL;
var
  lTime, t: DWORD;
begin
  InitCacheInfo;
  try
    if not Assigned(FParser) then
      Prepare;

    if not RwStatRec.Lock then
    begin
      lTime := GetTickCount;
      t := RwStatRec.ConnectionTime + RwStatRec.RemoteQueriesTime + RwStatRec.CacheDataTime;
    end
    else
    begin
      lTime := 0;
      t := 0;
    end;

    FOriginalIndexName := '';
    try
      FResInfo := CustomSQL(SQL.Text, Params, Self, @OpenVirtualTable2, @OpenVirtualTable3);
    except
      Close;
      raise;
    end;

    if IndexEnabled then
      FOriginalIndexName := FActiveIndex.Name;

    if not RwStatRec.Lock then
    begin
      lTime := GetTickCount - lTime - (RwStatRec.ConnectionTime + RwStatRec.RemoteQueriesTime + RwStatRec.CacheDataTime - t);
      Inc(RwStatRec.LogicalQueriesTime, lTime);
      Inc(RwStatRec.LogicalQueries);
    end;
  finally
    FreeCacheInfo;
  end;
end;


procedure TrwLQQuery.FreeSQLInfo;
begin
  if Assigned(FParser) then
  begin
    FParser.Free;
    FParser := nil;
  end;
end;


procedure TrwLQQuery.Prepare;
var
  lStatus: TsbScannerRes;
  p: Integer;
  h: string;
begin
  SetSbTempDir(CacheInfo.GetCachePath);
  lStatus.sText := SQL.Text + #0;
  ScannerChangePosition(lStatus, 1);
  ScannerGetNextToken(lStatus);
  if lStatus.sKeyWord in [kwCreate, kwDrop] then
  begin
    p := lStatus.sPos;
    ScannerGetNextToken(lStatus);
    if AnsiSameText(lStatus.sToken, 'Buffer') then
      h := Copy(SQL.Text, 1, p) + ' table ' + Copy(SQL.Text, lStatus.sPos, Length(SQL.Text) - lStatus.sPos + 1)
    else
      h := SQL.Text;
  end
  else
    h := SQL.Text;

  FreeSQLInfo;
  FParser := ParseCustomSQL(h, Params, @OpenVirtualTable, @CustomParsingEndProc, True);
end;

procedure TrwLQQuery.CloseIndex(AIndexName: string);
begin
  if not (Assigned(FActiveIndex) and AnsiSameText(FActiveIndex.Name, FOriginalIndexName)) or
     not AnsiSameText(AIndexName, FOriginalIndexName) then
  begin
    inherited;
    SetIndex('');
  end;
end;


procedure TrwLQQuery.SetIndex(AIndexName: string; ATemporary: Boolean);
begin
  if (Length(FOriginalIndexName) > 0) and (Length(AIndexName) = 0) then
    inherited SetIndex(FOriginalIndexName, True)
  else
    inherited;
end;

procedure TrwLQQuery.Close;
begin
  FOriginalIndexName := '';
  inherited;
end;


{ TrwLQVirtualTable }

procedure TrwLQVirtualTable.ClearAuxParams;
var
  i: Integer;
begin
  for i := 0 to FAuxParams.Count - 1 do
    TObject(FAuxParams[i]).Free;
  FAuxParams.Clear;
end;

constructor TrwLQVirtualTable.Create;
begin
  inherited;
  FAuxParams := TList.Create;
  InternalTag := 1;
  FrwDDTable := nil;
  FCacheTable := nil;
end;

destructor TrwLQVirtualTable.Destroy;
begin
  ClearAuxParams;
  FAuxParams.Free;
  inherited;
end;


procedure TrwLQVirtualTable.Initial(ArwDDTable: TrwDataDictTable);
var
  i: Integer;
  t: TsbFieldType;
  Q: TrwQuery;
begin
  if (ArwDDTable.TableType = rwtDDComponent) and not ArwDDTable.Prepared  then
  begin
    Q := TrwQuery.Create(nil);
    try
      i := SystemLibComponents.IndexOf(ArwDDTable.ExternalName);
      SystemLibComponents[i].CreateComp(Q);
      ArwDDTable.Prepared := True;
      PrepareQueries(Q);
      Q.Prepare;
    finally
      Q.Free;
    end;
  end

  else if (ArwDDTable.TableType = rwtDDQuery) and not ArwDDTable.Prepared then
  begin
    Q := TrwQuery.Create(nil);
    try
      Q.SQL.Text := ArwDDTable.SQLText;
      ArwDDTable.Prepared := True;
      Q.Prepare;
    finally
      Q.Free;
    end;
  end;


  TableName := ArwDDTable.Name;
  FrwDDTable := ArwDDTable;
  ClearAuxParams;

  Fields.Clear;
  for i := 0 to FrwDDTable.Fields.Count - 1 do
  begin
    case FrwDDTable.Fields[i].FieldType of
      ddtDateTime:  t := sbfDateTime;
      ddtInteger:   t := sbfInteger;
      ddtFloat:     t := sbfFloat;
      ddtCurrency:  t := sbfCurrency;
      ddtBoolean:   t := sbfBoolean;
      ddtString:    t := sbfString;
      ddtBLOB,
      ddtMemo,
      ddtGraphic:   t := sbfBLOB;
    else
      t := sbfUnknown;
    end;
    Fields.CreateField(FrwDDTable.Fields[i].Name, t, 254);
  end;

  for i := 0 to FrwDDTable.Params.Count - 1 do
    AuxParams.Add(nil);
end;



{ TrwLQCacheInfo }

constructor TrwLQCacheInfo.Create;
begin
  inherited;
  FUniqCounter := 0;
  InitCachePath;
end;


destructor TrwLQCacheInfo.Destroy;
begin
  ClearCachePath;
  inherited;
end;


function TrwLQCacheInfo.Add(ATableName: string): TrwLQCachedTable;
begin
  Result := TrwLQCachedTable(inherited Add);
  Result.FTableName := AnsiUpperCase(ATableName);
end;


function TrwLQCacheInfo.CopyTable(AIndex: Integer): TrwLQCachedTable;
begin
  Result := Add(Items[AIndex].TableName);
  Result.FFields.Assign(Items[AIndex].FFields);
  Result.FrwDDTable := Items[AIndex].FrwDDTable;
end;


function TrwLQCacheInfo.FindTable(const AName: String): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to Count - 1 do
    if AnsiSameText(AName, Items[i].FTableName) then
    begin
      Result := i;
      break;
    end;
end;


procedure TrwLQCacheInfo.GatherSQLInfo(AParser: TsbSQLParser);
var
  i, j, k: Integer;
  T: TrwLQCachedTable;
  lFltr: string;
  lIncorrectFiltr: Boolean;
  fl: Boolean;
  h: String;
  lSQLInfo: TsbSQLInfo;

  procedure AddField(AField: TsbField);
  var
    T: TrwLQCachedTable;
  begin
    T := TrwLQVirtualTable(AField.Cursor).FCacheTable;
    if Assigned(T) then
      if T.Fields.IndexOf(AnsiUpperCase(AField.FieldName)) = -1 then
        T.Fields.Add(AnsiUpperCase(AField.FieldName));
  end;


  procedure GetFieldsFromExpresion(AExpr: TsbExpression);
  var
    i: Integer;
  begin
    if not Assigned(AExpr) then
      exit;

    for i := 0 to AExpr.ItemCount - 1 do
    begin
      if Assigned(AExpr[i].FField) then
        AddField(AExpr[i].FField)

      else if Assigned(AExpr[i].FAggrFunction) then
        GetFieldsFromExpresion(AExpr[i].FAggrFunction.Expression);
    end;

    if Assigned(AExpr.SQL) then
      GatherSQLInfo(AExpr.SQL.Parser);
  end;


  procedure GetFieldsFromCondition(ACond: TObject);
  var
    i: Integer;
  begin
    if not Assigned(ACond) then
      exit;

    if ACond is TsbConditionItem then
    begin
      GetFieldsFromExpresion(TsbConditionItem(ACond).LeftExpression);
      GetFieldsFromExpresion(TsbConditionItem(ACond).RightExpression);
    end

    else
      for i := 0 to TsbConditionNode(ACond).ItemCount - 1 do
        GetFieldsFromCondition(TsbConditionNode(ACond).Items[i]);
  end;


  function GetFilterFromCondition(ACond: TObject; ATable: TrwLQCachedTable): string;
  var
    i: Integer;
    op: string;
    h: String;

    function GetFilterExpression(AExpr: String): string;
    var
      lStatus: TsbScannerRes;
      Cnst: TsbCustomSQLConstant;
      i: integer;
      h: String;
      rwF: TrwDataDictField;
    begin
      Result := '';
      lStatus.sText := AExpr + #0;
      ScannerChangePosition(lStatus, 1);
      ScannerGetNextToken(lStatus);
      while lStatus.sKeyWord <> kwFinish do
      begin
        h := lStatus.sToken;
        if lStatus.sTokenType = stUnknown then
        begin
          i := Pos('.', h);
          if i > 0 then
            System.Delete(h, 1, i);
          rwF := TrwDataDictField(ATable.FrwDDTable.Fields.FieldByName(h));
          if not Assigned(rwF) then
          begin
            lIncorrectFiltr := True;
            Exit;
          end;
          h := rwF.ExternalName;
        end

        else if lStatus.sTokenType = stQuotedText then
          h := AnsiQuotedStr(h, '''')

        else if (lStatus.sTokenType = stConst) then
        begin
          Cnst := SQLConsts.ConstantByName(h);
          if VarIsArray(Cnst.Value) or (Cnst.Value = Null) then
          begin
            Result := '';
            Exit;
          end
          else
            if Cnst.ConstType in [sctString, sctDateTime] then
              h := AnsiQuotedStr(String(Cnst.Value), '''')
            else
              h := String(Cnst.Value);
        end

        else if (lStatus.sTokenType = stParameter) or
                ((lStatus.sTokenType = stKeyWord) and
                 ((lStatus.sKeyWordType <> tkwNone) or (lStatus.sKeyWord = kwLSquareBracket))) then
        begin
          Result := '';
          Exit;
        end;

        Result := Result + ' ' + h;

        ScannerGetNextToken(lStatus);
      end;
    end;

  begin
    Result := '';
    if not Assigned(ACond) then
      exit;

    if ACond is TsbConditionItem then
    begin
      Result := GetFilterExpression(TsbConditionItem(ACond).StrExpression);
      if (Length(Result) > 0) and TsbConditionItem(ACond).NotOper then
        Result := 'NOT ' + Result;
    end

    else
    begin
      if TsbConditionNode(ACond).OperType = sbnAND then
        op := ' AND '
      else
        op := ' OR ';

      for i := 0 to TsbConditionNode(ACond).ItemCount - 1 do
      begin
        h := GetFilterFromCondition(TsbConditionNode(ACond).Items[i], ATable);
        if lIncorrectFiltr then
          Exit;
        if Length(h) > 0 then
        begin
          if Length(Result) > 0 then
            Result := Result + op ;
          Result := Result + h;
        end;
      end;
      if Length(Result) > 0 then
      begin
        Result := '(' + Result + ')';
        if TsbConditionNode(ACond).NotOper then
          Result := 'NOT ' + Result;
      end
    end;
  end;


  procedure AddTableInfo(AFrom: TsbTableList);
  var
    i, j, k, l: Integer;
    T: TrwLQCachedTable;
    Tsql: TrwLQVirtualTable;
    P: TsbParams;
    Img: TrwLQCachedTableImage;
    Q: TrwQuery;
    flAddAllFields: Boolean;
  begin
    P := TsbParams.Create;
    InitializationSbVM;
    try
      for i := 0 to AFrom.ItemCount - 1 do
      begin
        Tsql := TrwLQVirtualTable(AFrom[i].Table);
        if not Assigned(Tsql.FrwDDTable) then
          Continue;
        P.Clear;
        for j := 0 to Tsql.AuxParams.Count - 1 do
        begin
          if not Assigned(TsbExpression(Tsql.AuxParams[j])) then
            continue;
          k := P.Add(TrwLQVirtualTable(Tsql).FrwDDTable.Params[j].Name);
          VM.CalcExpression(TsbExpression(Tsql.AuxParams[j]));
          P[k].Value := VM.RegA;
        end;
        j := FindTable(Tsql.TableName);
        if j <> -1 then
          T := Items[j]
        else
          T := Add(Tsql.TableName);
        Tsql.FCacheTable := T;
        T.FrwDDTable := Tsql.FrwDDTable;

        if j = -1 then
        begin
          for l := 0 to T.FrwDDTable.PrimaryKey.Count - 1 do
            T.FFields.Add(T.FrwDDTable.PrimaryKey[l].Field.Name);
          for l := 0 to T.FrwDDTable.LogicalKey.Count - 1 do
            if T.FFields.IndexOf(T.FrwDDTable.LogicalKey[l].Field.Name) = -1 then
              T.FFields.Add(T.FrwDDTable.LogicalKey[l].Field.Name);
        end;

        if Tsql.FrwDDTable.TableType = rwtDDComponent then    // Does component care of selected fields?
        begin
          Q := TrwQuery(SystemLibComponents[SystemLibComponents.IndexOf(Tsql.FrwDDTable.ExternalName)].CreateComp);
          try
            flAddAllFields := Q.GlobalVarFunc.Variables.IndexOf(cCompSelectedFieldsParam) = -1;
          finally
            FreeAndNil(Q);
          end;
        end
        else
          flAddAllFields := Tsql.FrwDDTable.TableType = rwtDDQuery;


        if flAddAllFields then  //Structure of component and LQ is static
          for l := 0 to Tsql.FrwDDTable.Fields.Count - 1 do
            if T.FFields.IndexOf(AnsiUpperCase(Tsql.FrwDDTable.Fields[l].Name)) = -1 then
              T.FFields.Add(AnsiUpperCase(Tsql.FrwDDTable.Fields[l].Name));

        if Tsql.FrwDDTable.TableType in [rwtDDComponent, rwtDDQuery] then
          T.FFilter := '1=1';

        j := T.FindImage(P);
        if j = -1 then
        begin
          Img := T.Add;
          Img.Params.Assign(P);
        end;
      end;

    finally
      DeInitializationSbVM;
      P.Free;
    end;
  end;

begin
  if not Assigned(AParser.SQLInfoList) then
    Exit;

  for k := 0 to AParser.SQLInfoList.ItemCount - 1 do
  begin
    lSQLInfo := AParser.SQLInfoList[k];

    AddTableInfo(lSQLInfo.From);

    for j := 0 to lSQLInfo.Select.ItemCount - 1 do
      GetFieldsFromExpresion(lSQLInfo.Select[j].Expression);

    if Assigned(lSQLInfo.Where) then
      for j := 0 to lSQLInfo.Where.ItemCount - 1 do
        GetFieldsFromCondition(lSQLInfo.Where.Items[j]);

    for i := 0 to lSQLInfo.From.ItemCount - 1 do
    begin
      T := TrwLQVirtualTable(lSQLInfo.From[i].Table).FCacheTable;

      if Assigned(T) then
      begin
        if T.FFilter <> '1=1' then
        begin
          lIncorrectFiltr := False;
          lFltr := GetFilterFromCondition(lSQLInfo.From[i].ParentJoinInfo.FilterCondition, T);
          if (Length(lFltr) = 0) or lIncorrectFiltr then
          begin
            T.FFilter := '1=1';
            T.DeleteFiles;
          end
          else
          begin
            h := T.FFilter;
            fl :=  True;
            while Length(h) > 0 do
              if GetNextStrValue(h, #13) = lFltr then
              begin
                fl := False;
                break;
              end;

            if fl then
            begin
              if Length(T.FFilter) > 0 then
                T.FFilter := T.FFilter + #13;
              T.FFilter := T.FFilter + lFltr;
              T.DeleteFiles;
            end;
          end;
        end;
      end;

      GetFieldsFromCondition(lSQLInfo.From[i].ParentJoinInfo.FilterCondition(True));
      GetFieldsFromCondition(lSQLInfo.From[i].ParentJoinInfo.JoinCondition(True));

      for j := 0 to lSQLInfo.From[i].ParentJoinInfo.SelfJoinFields.Count - 1 do
        AddField(TsbField(lSQLInfo.From[i].ParentJoinInfo.SelfJoinFields[j]));

      for j := 0 to lSQLInfo.From[i].ParentJoinInfo.ForeignJoinFields.Count - 1 do
        AddField(TsbField(lSQLInfo.From[i].ParentJoinInfo.ForeignJoinFields[j]));
    end;
  end;
end;


function TrwLQCacheInfo.GetItem(Index: Integer): TrwLQCachedTable;
begin
  Result := TrwLQCachedTable(inherited Items[Index]);
end;



procedure TrwLQCacheInfo.SaveToStream(AStream: TStream);
var
  Writer: TWriter;
begin
  Writer := TWriter.Create(AStream, 1024);
  try
    Writer.WriteCollection(Self);
  finally
    Writer.Free;
  end;
end;


procedure TrwLQCacheInfo.InitCachePath;
var
  h: String;
begin
  h := GetTmpDir + 'Cache' + GetUniqueId + PathDelim;

  if DirExists(h) then
    ClearDir(h, False)
  else
    ForceDirectories(h);

  FCachePath := h;
  FPathLock := CreateFile(PChar(h + cPathLockFileName), GENERIC_READ or GENERIC_WRITE, 0, nil, CREATE_NEW, 0, 0);

  if (not DirExists(FCachePath)) or (FPathLock = INVALID_HANDLE_VALUE) then
    RaiseLastOSError; 
end;


{ It is old implementation for multithreaded version

procedure TrwLQCacheInfo.InitCachePath;
var
  h: String;
  i, n: Integer;

  procedure ClearDeadCache;
  var
    FileInfo: TSearchRec;
    DosCode: Integer;
    Path, h: String;
  begin
    Path := GetTmpDir;

    if not DirExists(Path) then
      Exit;

    DosCode := SysUtils.FindFirst(NormalDir(Path) + 'Cache*', faDirectory, FileInfo);
    try
      while DosCode = 0 do
      begin
        h := RWENGINE_MUTEX_NAME + Copy(FileInfo.Name, 6, Length(FileInfo.Name) - 5);
        if OpenMutex(MUTEX_ALL_ACCESS, False, PChar(h)) = 0 then
          ClearDir(NormalDir(Path) + FileInfo.Name, True);
        DosCode := SysUtils.FindNext(FileInfo);
      end;
    finally
      Sysutils.FindClose(FileInfo);
    end;
  end;

begin
  GlobalNameSpace.BeginWrite;
  try
    ClearDeadCache;  Looks like it deletes real

    h := RWENGINE_MUTEX_NAME + IntToStr(GetCurrentThreadId);

    n := 0;
    for i := 1 to 100 do
    begin
      FPresenceMutex := OpenMutex(MUTEX_ALL_ACCESS, False, PChar(h + '_' + IntToStr(i)));
      if FPresenceMutex = 0 then
      begin
        n := i;
        break;
      end;
    end;

    if FPresenceMutex <> 0 then
      Assert(False, 'Unexpected error. Cannot initialize RW Engine');

    FPresenceMutex := CreateMutex(nil, False, PChar(h + '_' + IntToStr(n)));

    h := GetTmpDir + 'Cache' + IntToStr(GetCurrentThreadId) + '_' + IntToStr(n) + PathDelim;
    if DirExists(h) then
      ClearDir(h, False)
    else
      ForceDirectories(h);

    FCachePath := h;
  finally
    GlobalNameSpace.EndWrite;
  end;

  Assert(DirExists(FCachePath));
end;
}

procedure TrwLQCacheInfo.ClearCachePath;
begin
//  CloseHandle(FPresenceMutex);
//  FPresenceMutex := 0;
  CloseHandle(FPathLock);
  ClearDir(FCachePath, True);
  FreeSysIndPool;
  FCachePath := '';
end;

function TrwLQCacheInfo.GetCachePath: String;
begin
  Result := FCachePath;
end;


function TrwLQCacheInfo.GetCachedTable(const ATable: String): IrwCachedTable;
var
  i: Integer;
begin
  i := FindTable(ATable);

  if i <> -1 then
    Result := Items[i]
  else
    Result := nil;
end;

procedure TrwLQCacheInfo.EmptyCachedTables;
var
  i: Integer;
  Accept: Boolean;
begin
  for i := 0 to Count - 1 do
  begin
    Accept := True;
    if Items[i].FrwDDTable.TableType = rwtDDExternal then
    begin
      RWEngineExt.AppAdapter.DBCacheCleaning(Items[i].FrwDDTable.ExternalName, Accept);
      if Accept then
        Items[i].DeleteFiles;
    end
    else if Items[i].FrwDDTable.TableType in [rwtDDQuery, rwtDDComponent] then
      Items[i].DeleteFiles;
  end;
end;


procedure TrwLQCacheInfo.ClearCache;
begin
  try
    try
      Clear;
    except
      ClearDir(FCachePath, False);
    end;
  finally
    FUniqCounter := 0;
    DataDictionary.UpdateSysTablesStructure;
  end;
end;

{ TrwLQCachedTable }

constructor TrwLQCachedTable.Create(Collection: TCollection);
begin
  inherited;
  FImages := TList.Create;
  FFields := TStringList.Create;
  FFields.OnChange := OnChangeFields;
  FTableName := '';
  FrwDDTable := nil;
  FFilter := '';
end;


destructor TrwLQCachedTable.Destroy;
var
  i: Integer;
begin
  DeleteFiles;
  FFields.Free;

  for i := 0 to FImages.Count -1 do
    Items[i].Free;
  FImages.Free;

  inherited;
end;


procedure TrwLQCachedTable.DeleteFiles;
var
  i: Integer;
begin
  for i := 0 to FImages.Count -1 do
    if Length(Items[i].PhysicalSBName) > 0 then
    begin
      if not (FrwDDTable.TableType in [rwtBuffer, rwtDDSystem]) then
        if FileExists(Items[i].PhysicalSBName) then
        begin
          try
            sbDropTable(Items[i].PhysicalSBName);
          except
            DeleteFile(Items[i].PhysicalSBName);
          end;
        end;
      Items[i].PhysicalSBName := '';
    end;
end;

procedure TrwLQCachedTable.OnChangeFields(Sender: TObject);
begin
  DeleteFiles;
end;


function TrwLQCachedTable.GetItem(Index: Integer): TrwLQCachedTableImage;
begin
  Result := TrwLQCachedTableImage(FImages.Items[Index]);
end;

function TrwLQCachedTable.Add: TrwLQCachedTableImage;
begin
  Result := TrwLQCachedTableImage.Create;
  Result.FOwner := Self;
  FImages.Add(Result);
end;


function TrwLQCachedTable.FindImage(AParams: TsbParams): Integer;
var
  i, j, k: Integer;
  fl: Boolean;
  A1, A2: PVarArray;
  P1, P2: Pointer;
begin
  Result := -1;
  for i := 0 to FImages.Count - 1 do
  begin
    fl := True;
    for j := 0 to Items[i].FParams.ItemCount - 1 do
      if VarIsArray(AParams[j].Value) or VarIsArray(Items[i].FParams[j].Value) then
      begin
        if not VarIsArray(AParams[j].Value) or not VarIsArray(Items[i].FParams[j].Value) then
        begin
          fl := False;
          break;
        end;

        if (VarArrayLowBound(AParams[j].Value, 1) <> VarArrayLowBound(Items[i].FParams[j].Value, 1)) or
           (VarArrayHighBound(AParams[j].Value, 1) <> VarArrayHighBound(Items[i].FParams[j].Value, 1)) then
        begin
          fl := False;
          break;
        end;

        A1 := VarArrayAsPSafeArray(AParams[j].Value);
        A2 := VarArrayAsPSafeArray(Items[i].FParams[j].Value);

        for k := VarArrayLowBound(AParams[j].Value, 1) to VarArrayHighBound(AParams[j].Value, 1) do
        begin
          P1 := Pointer(Cardinal(A1.Data) + Cardinal(k * A1.ElementSize));
          P2 := Pointer(Cardinal(A2.Data) + Cardinal(k * A2.ElementSize));
          if not VariantsAreSame(PVariant(P1)^, PVariant(P2)^) then
          begin
            fl := False;
            break;
          end;
        end;

        if not fl then
          break;
      end

      else if not VariantsAreSame(AParams[j].Value, Items[i].FParams[j].Value) then
      begin
        fl := False;
        break;
      end;

    if fl then
    begin
      Result := i;
      break;
    end;
  end;
end;


procedure TrwLQCachedTable.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('Params', ReadParamsData, WriteParamsData, True);
end;


procedure TrwLQCachedTable.ReadParamsData(Reader: TReader);
var
  i, n: Integer;
  h: String;
  Img: TrwLQCachedTableImage;
begin
  Reader.ReadListBegin;
  while not Reader.EndOfList do
  begin
    Img := Add;
    Reader.ReadListBegin;
    while not Reader.EndOfList do
    begin
      i := Img.Params.Add(Reader.ReadString);
      n := Reader.ReadInteger;
      h := Reader.ReadString;
      if n = varNull then
        Img.Params[i].Value := Null
      else
        Img.Params[i].Value := VarAsType(h, n)
    end;
    Reader.ReadListEnd;
  end;
  Reader.ReadListEnd;
end;


procedure TrwLQCachedTable.WriteParamsData(Writer: TWriter);
var
  i, j: Integer;
begin
  Writer.WriteListBegin;
  for i := 0 to FImages.Count - 1 do
  begin
    Writer.WriteListBegin;
    for j := 0 to Items[i].Params.ItemCount - 1 do
    begin
      Writer.WriteString(Items[i].Params[j].Name);
      Writer.WriteInteger(VarType(Items[i].Params[j].Value));
      if VarIsNull(Items[i].Params[j].Value) then
        Writer.WriteString('null')
      else
        Writer.WriteString(Items[i].Params[j].Value);
    end;
    Writer.WriteListEnd;
  end;
  Writer.WriteListEnd;
end;



function TrwLQCachedTable.ddTable: IrwDDTable;
begin
  Result := FrwDDTable;
end;

function TrwLQCachedTable.GetFilter: String;
begin
  Result := StringReplace(FFilter, #13, ' OR'#13, [rfReplaceAll]);
  if Result = '1=1' then
    Result := '';
end;

function TrwLQCachedTable.GetTableImageByParams(const Params: array of TrwParamRec): IrwCachedTableImage;
var
  i, j: Integer;
  P: TsbParams;
begin
  Result := nil;

  P := TsbParams.Create;
  try
    for i := Low(Params) to High(Params) do
    begin
      j := P.Add(Params[i].Name);
      P[j].Value := Params[i].Value;
    end;

    i := FindImage(P);
    if i <> -1 then
      Result := Items[i];

  finally
    FreeAndNil(P);
  end;
end;

function TrwLQCachedTable.GetFields: TrwStrList;
var
  i: Integer;
begin
  SetLength(Result, FFields.Count);
  for i := 0 to FFields.Count - 1 do
    Result[i] := FFields[i];
end;

function TrwLQCachedTable.GetExternalFields: TrwStrList;
var
  i: Integer;
begin
  SetLength(Result, FFields.Count);
  for i := 0 to FFields.Count - 1 do
    Result[i] := TrwDataDictField(FrwDDTable.Fields.FieldByName(FFields[i])).ExternalName;
end;

procedure TrwLQCachedTable.SetFields(const FieldList: TrwStrList);
var
  i: Integer;
begin
  FFields.Clear;
  FFields.Capacity := High(FieldList) - Low(FieldList) + 1;
  for i := Low(FieldList) to High(FieldList) do
    FFields.Add(FieldList[i]);
end;

{ TrwLQCachedTableImage }

constructor TrwLQCachedTableImage.Create;
begin
  FParams := TsbParams.Create;
  PhysicalSBName := '';
  FOwner := nil;
end;


destructor TrwLQCachedTableImage.Destroy;
begin
  FreeAndNil(FParams);
  inherited;
end;


procedure TrwLQCachedTableImage.Cache;
var
  TableFileName, flt: string;
  i, j: Integer;
  lTime: DWORD;
  lQuery: TrwQuery;
  lqQuery: TrwLQQuery;
  ITbl: IrwSBTable;
  fLock: Boolean;
  Flds: TsbFields;
  rwF: TrwDataDictField;
  Prms: array of TrwParamRec;
  ParValue, SelFields: Variant;
begin
  if Length(PhysicalSBName) > 0 then
    exit;

  fLock := RwStatRec.Lock;
  RwStatRec.Lock := True;
  try
    if FOwner.FrwDDTable.TableType in [rwtBuffer, rwtDDSystem] then
    begin
      PhysicalSBName := FOwner.FrwDDTable.ExternalName;
      exit;
    end;

    CacheInfo.FUniqCounter := CacheInfo.FUniqCounter + 1;
    TableFileName := CacheInfo.GetCachePath + 'c' + IntToStr(CacheInfo.FUniqCounter) + sbTableExt;

    if FOwner.FrwDDTable.TableType <> rwtDDComponent then
      for i := 0 to FParams.ItemCount - 1 do
      begin
        if VarIsNull(FParams[i].Value) or VarIsArray(FParams[i].Value) then
          Continue;

        case FOwner.FrwDDTable.Params[i].ParamType of
          ddtDateTime:  if SameText(VarAsType(FParams[i].Value, varString), 'now') or
                            SameText(VarAsType(FParams[i].Value, varString), 'today') then
                           FParams[i].Value := VarAsType(FParams[i].Value, varString)
                         else
                           FParams[i].Value := VarAsType(FParams[i].Value, varDate);

          ddtInteger:   FParams[i].Value := VarAsType(FParams[i].Value, varInteger);
          ddtFloat:     FParams[i].Value := VarAsType(FParams[i].Value, varDouble);
          ddtCurrency:  FParams[i].Value := VarAsType(FParams[i].Value, varCurrency);
          ddtString:    FParams[i].Value := VarAsType(FParams[i].Value, varString);
          ddtBoolean:   FParams[i].Value := VarAsType(FParams[i].Value, varBoolean);
        end;
      end;


    if FOwner.FrwDDTable.TableType = rwtDDComponent then
    begin
      lQuery := TrwQuery.Create(nil);
      try
        i := SystemLibComponents.IndexOf(FOwner.FrwDDTable.ExternalName);
        SystemLibComponents[i].CreateComp(lQuery);

        for i := 0 to FParams.ItemCount - 1 do
        begin
          j := lQuery.GlobalVarFunc.Variables.IndexOf(FParams[i].Name);
          if j <> -1 then
          begin
            ParValue := FParams[i].Value;
            if not VarIsNull(ParValue) then
              case lQuery.GlobalVarFunc.Variables[j].VarType of
                rwvBoolean:
                  ParValue := (ParValue = 'Y');

                rwvArray:
                  begin
                    if not VarIsArray(FParams[i].Value) then
                      ParValue := VarArrayOf([FParams[i].Value]);
                  end;
              end;

            lQuery.GlobalVarFunc.Variables[j].Value := ParValue;
            lQuery.GlobalVarFunc.Variables[j].UpdateValue;
          end;
        end;

        // make list of selected fields
        j := lQuery.GlobalVarFunc.Variables.IndexOf(cCompSelectedFieldsParam);
        if j <> -1 then
        begin
          SelFields := VarArrayCreate([0, FOwner.FFields.Count - 1], varVariant);
          for i := 0 to FOwner.FFields.Count - 1 do
            SelFields[i] := FOwner.FFields[i];
          lQuery.GlobalVarFunc.Variables[j].Value := SelFields;
          lQuery.GlobalVarFunc.Variables[j].UpdateValue;
          SelFields := Unassigned;
        end;

        lQuery.Open;
        sbCopyTable(TsbTableCursor(lQuery.DataSet), TableFileName);
      finally
        FreeAndNil(lQuery);
      end;
    end

    else if FOwner.FrwDDTable.TableType = rwtDDQuery then
    begin
      lqQuery := TrwLQQuery.Create;
      try
        lqQuery.SQL.Text := FOwner.FrwDDTable.SQLText;
        lqQuery.Params.Assign(FParams);
        lqQuery.OpenSQL;
        sbCopyTable(TsbTableCursor(lqQuery), TableFileName)
      finally
        FreeAndNil(lqQuery);
      end;
    end

    else
    begin
      if not fLock then
        lTime := GetTickCount
      else
        lTime := 0;

      SetLength(Prms, FOwner.FrwDDTable.Params.Count);
      for i := Low(Prms) to High(Prms) do
      begin
        Prms[i].Name := FOwner.FrwDDTable.Params[i].Name;
        Prms[i].Value := Params[i].Value;
        Prms[i].ParamType := FOwner.FrwDDTable.Params[i].ParamType;
      end;

      RWEngineExt.AppAdapter.BeforeCacheTable(FOwner, Prms);

      Flds := TsbFields.Create;
      try
        for i := 0 to FOwner.FFields.Count - 1 do
        begin
          rwF := TrwDataDictField(FOwner.FrwDDTable.Fields.FieldByName(FOwner.FFields[i]));
          Flds.CreateField(rwF.Name, DDTypetoSBType(rwF.FieldType), rwF.Size);
        end;
        sbCreateTable(TableFileName, Flds);
      finally
        FreeAndNil(Flds);
      end;

      ITbl := sbOpenTable(TableFileName, sbmReadWrite);
      try
        Flt := FOwner.GetFilter;

        RWEngineExt.AppAdapter.TableDataRequest(FOwner, ITbl, Prms);

        if not fLock then
          lTime := GetTickCount - lTime;

        Inc(RwStatRec.RemoteQueriesTime, lTime);
        Inc(RwStatRec.RemoteQueries);
        RwStatRec.CacheDataSize := RwStatRec.CacheDataSize + TsbTableCursor(ITbl.RealObject).TableSize;
      finally
        ITbl := nil;
      end;
    end;

    PhysicalSBName := TableFileName;

    if not fLock then
      lTime := GetTickCount
    else
      lTime := 0;

    if FOwner.FrwDDTable.TableType = rwtDDExternal then
      RWEngineExt.AppAdapter.AfterCacheTable(Self);

    if not fLock then
    begin
      lTime := GetTickCount - lTime;
      Inc(RwStatRec.CacheDataTime, lTime);
    end;

  finally
    RwStatRec.Lock := fLock;
  end;
end;


function TrwLQCachedTableImage.CachedTable: IrwCachedTable;
begin
  Result := FOwner;
end;

function TrwLQCachedTableImage.GetParams: TrwParamList;
var
  i: Integer;
begin
  SetLength(Result, Params.ItemCount);
  for i := 0 to Params.ItemCount - 1 do
  begin
    Result[i].Name := Params[i].Name;
    Result[i].Value := Params[i].Value;
  end;
end;

function TrwLQCachedTableImage.GetSBTable: IrwSBTable;
begin
  if PhysicalSBName <> '' then
    Result := sbOpenTable(PhysicalSBName, sbmReadWrite)
  else
    Result := nil;
end;

procedure TrwLQCachedTableImage.SetPhysicalSBName(const Value: string);
begin
  FPhysicalSBName := Value;
end;

end.
