unit rmCommonClasses;

interface

uses
  Windows, Classes, Graphics, TypInfo, StdCtrls, SysUtils, Variants, Math, Controls, Messages,
  Forms, XMLdom,
  rwEngineTypes, rwBasicClasses, rwTypes, rwUtils, rwCommonClasses, rwGraphics, rmTypes,
  rwBasicUtils, rwDB;

const
  cCalcTag = '[Calc]';
  cErrorTag = 'ERROR!';

type
  IrmCustomRenderEngine = interface
  ['{AF715D1C-39A8-4A48-BE45-296CE6753F04}']
    function RealObject: TObject;
    function ResultType: TrwResultFormatType;
  end;


  IrmRWARenderEngine = interface(IrmCustomRenderEngine)
  ['{89E58DC1-D126-423C-954B-1E6FE95B6E20}']
     function  CurrentPageNumber: Integer;
     procedure StorePageDataToResult;
     procedure AddFrame(ABounds: TRect; ACompare: Boolean; ALayerNumber: Integer; AColor: TColor;
                        ABoundLines: TrwSetColumnLines; ABoundLinesColor: TColor;
                        ABoundLinesWidth: Integer; ABoundLinesStyle: TPenStyle);
     procedure AddLine(X1, X2, Y1, Y2: Integer; ACompare: Boolean; ALayerNumber: Integer; ALineColor: TColor;
                       ALineWidth: Integer; ALineStyle: TPenStyle);
     procedure AddText(ABounds: TRect; ACompare: Boolean; ALayerNumber: Integer;
                       AAlignment: TAlignment; ALayout: TTextLayout; AColor: TColor; AWordWrap: Boolean;
                       const ADigitNet: String; AFont: TrwFont; const AText: String);
     procedure AddPicture(ABounds: TRect; ACompare: Boolean; ALayerNumber: Integer;
                          ATransparent: Boolean; APicture: TrwPicture; AStretch: Boolean);
  end;


  IrmASCIIRenderEngine = interface(IrmCustomRenderEngine)
  ['{89E58DC1-D126-423C-954B-1E6FE95B6E20}']
    procedure   AppendASCIILine(const AText: String);
    procedure   InsertASCIILine(const AText: String; const ALine: Integer);
    procedure   UpdateASCIILine(const AText: String; const ALine: Integer);
    procedure   DeleteASCIILine(const ALine: Integer);
    procedure   CutOffUpToLine(const ALine: Integer);
    function    GetASCIILine(const ALine: Integer): String;
    function    ASCIILineCount: Integer;
  end;


  TxmlDocNodeHandle = Cardinal;

  IrmXMLRenderEngine = interface(IrmCustomRenderEngine)
  ['{3AFC119D-34D1-4EA5-B506-4EBAFE4A7B17}']
    function  AddElement(const AElementName: String): TxmlDocNodeHandle;
    procedure AddElementTerminator;    
    function  AddAttribute(const AAttributeName: String; const AValue: String): TxmlDocNodeHandle;
    function  AddText(const AText: String): TxmlDocNodeHandle;
    function  AddComment(const AComment: String): TxmlDocNodeHandle;
    function  AddFragment(const AText: String): TxmlDocNodeHandle;
    procedure CutOffUpToPos(const AItemPos: Integer);
    function  LastItemPos: Integer;
  end;
  

  IrmReportRenderer = interface
    ['{614C1EDD-CAA0-479D-B7E6-61E2C39A5A8F}']
     function  Render: IrwResult;
     function  RWARenderEngine: IrmRWARenderEngine;
     function  ASCIIRenderEngine: IrmASCIIRenderEngine;
     function  XMLRenderEngine: IrmXMLRenderEngine;
  end;


  TrmdMousePosition = (rmdMPClient, rmdMPLeftBound, rmdMPRightBound, rmdMPTopBound, rmdMPBottomBound);
  TrmdMousePositions = set of TrmdMousePosition;

  TrmdBoundsResizer = class;
  TrmdMovableDesignObject = class;
  TrmdMovableDesignObjectClass = class of TrmdMovableDesignObject;

  TrmGraphicControl = class(TrwControl)
  private
    FCursor: TCursor;
    FResizer: TrmdBoundsResizer;
    FBounsToApplay: TRect;
    FOldMousePos: TPoint;
    FStartResizeMousePosition: TrmdMousePosition;
    FObjectTag: TrmdMovableDesignObject;

    procedure SetCursor(const Value: TCursor);
    procedure InitResizer;
    procedure UpdateResizer;
    procedure HideResizer;

  protected
    FAllowedResizingBounds: TrmdMousePositions;
    FMousePosition:  TrmdMousePosition;
    FObjectTagClass: TrmdMovableDesignObjectClass;

    function  ResizingBoundsNow: Boolean;
    procedure ApplyMouseResizing(NewBounds: TRect; MousePosition: TrmdMousePosition; Shift: TShiftState); virtual;
    function  MouseResizingThreshold: Integer; virtual;
    function  MouseResizingDiscretion: Integer; virtual;

    procedure Notify(AEvent: TrmDesignEventType); override;

    property  ObjectTag: TrmdMovableDesignObject read FObjectTag;
    procedure ShowTag; virtual;
    procedure HideTag; virtual;
    procedure SyncTagPos; virtual;

    procedure PaintControl(ACanvas: TrwCanvas); virtual;
    procedure SetDesignParent(Value: TWinControl); override;
    procedure Refresh; override;
    procedure Invalidate; virtual;
    procedure InvalidateRect(const ARect: TRect); virtual;
    procedure SetDescription(const Value: String); override;
    procedure SetName(const NewName: TComponentName); override;
    function  GetShowText: String; virtual;

  //Interface
  protected
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    function  GetBoundsRect: TRect; override;
    procedure SetBoundsRect(ARect: TRect); override;
  //end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function    DoNotPaint: Boolean; virtual;
    procedure   PaintToCanvas(ACanvas: TrwCanvas); virtual;
    procedure   MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;
    procedure   MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;
    procedure   MouseMove(Shift: TShiftState; X, Y: Integer); virtual;
    procedure   BeforeChangePos; override;
    procedure   AfterChangePos; override;
    procedure   DoSyncVisualControl(const APropID: TrwChangedPropID); override;
    function    CanvasDPI: Integer; virtual; abstract;

    property    Cursor: TCursor read FCursor write SetCursor;
  end;


 // Value Holder
 TOnChangeValue = procedure (var ANewValue: Variant) of object;

 TrmsValueHolder = class
 private
    FOwner: IrmQueryAwareObject;
    FValue: Variant;
    FDataField: String;
    FDataSource: TrwDataSet;
    FFieldIndex: Integer;
    FOnChangeValue: TOnChangeValue;
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
    procedure SetDataField(const AValue: String);
    procedure  PrepareDataField;
 public
   constructor Create(AOwner: IrmQueryAwareObject);
   procedure   SetValueFromDataSource;

   property  Value: Variant read GetValue write SetValue;
   property  DataField: String read FDataField write SetDataField;
   property  OnChangeValue: TOnChangeValue read FOnChangeValue write FOnChangeValue;
 end;


 // Design-Time supplementary objects

  TrmdCustomGraphicWorkSpace = class;

  TrmdCustomDesignObject = class(TObject)
  private
    FOwner: TrmGraphicControl;
    FVisible: Boolean;
    FParent: TrmdCustomGraphicWorkSpace;
    FShape:   HRGN;
    procedure SetVisible(const Value: Boolean);
  protected
    FTransparent: Boolean;
    FIgnoreMouse: Boolean;
    procedure PaintToCanvas(ACanvas: TrwCanvas); virtual;
    function  GetShapeRegion: HRGN; virtual; abstract;
  public
    constructor Create(AOwner: TrmGraphicControl); virtual;
    destructor  Destroy; override;
    procedure   Invalidate; virtual;
    procedure   UpdateRegion;
    procedure   MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;
    procedure   MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;
    procedure   MouseMove(Shift: TShiftState; X, Y: Integer); virtual;

    property Visible: Boolean read FVisible write SetVisible;
    property Owner:  TrmGraphicControl read FOwner;
    property Parent: TrmdCustomGraphicWorkSpace read FParent;
    property Shape: HRGN read FShape;
  end;


  TrmdMovableDesignObject = class(TrmdCustomDesignObject)
  private
    FAllowMove: Boolean;
  public
    constructor Create(AOwner: TrmGraphicControl); override;
    procedure   MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;

    property    AllowMove: Boolean read FAllowMove write FAllowMove;
  end;


  TrmdSimpleTextTag = class(TrmdMovableDesignObject)
  private
    FText: String;
    FFontSize: Integer;
    FTextOffset: TPoint;
  protected
    procedure MakeText; virtual;
    procedure SetTextMetrics;
    function  GetTextSize: TSize;
    procedure CorrectTagBounds(var ATagRect: TRect); virtual;
    procedure PaintToCanvas(ACanvas: TrwCanvas); override;
    function  GetShapeRegion: HRGN; override;
    procedure PainTag(ACanvas: TrwCanvas; ShapeRegion: HRGN);    
  public
    constructor Create(AOwner: TrmGraphicControl); override;
  end;


  TrmdCustomWorkSpace = class(TCustomControl, IrwVisualControl)
  private
    FrwObject: TrwComponent;
    function  RealObject: TControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
  protected
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID); virtual;
    function  Container: TWinControl; virtual;
    procedure Notify(AEvent: TrmDesignEventType); virtual;
    procedure UpdateSize; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrmdCustomGraphicWorkSpace = class(TrmdCustomWorkSpace)
  private
    FCanvas: TrwCanvas;
    FDsgnObjects: TList;
    FLastMouseControl: TrmGraphicControl;
    procedure WMEraseBkgnd(var Message: TWmEraseBkgnd); message WM_ERASEBKGND;
  protected
    procedure PaintWindow(DC: HDC); override;
    procedure Paint; override;
    procedure PaintCustomDsgnObjects;
    procedure WndProc(var Message: TMessage); override;
    function  DispatchMessage(var Message: TMessage): Boolean; virtual;
    procedure AddDsgnObject(AObject: TrmdCustomDesignObject);
    procedure DeleteDsgnObject(AObject: TrmdCustomDesignObject);
    function  IndexOfDsgnObject(AObject: TrmdCustomDesignObject): Integer;
    procedure DeleteAllDsgnObject;
    procedure UpdateMouseLastControl(const AObject: TrmGraphicControl);
  public
    FObjectOffset: TPoint;
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    property    Canvas: TrwCanvas read FCanvas;
  end;


  TrmdBoundsResizer = class(TCustomControl)
  protected
    procedure Paint; override;
  end;

var FZoomFactor: Extended;

function  ZoomFactor: Extended;
function  Zoomed(XLogPix: Integer): Integer; overload;
function  Zoomed(RectLogPix: TRect): TRect; overload;
function  Zoomed(PosLogPix: TPoint): TPoint; overload;
function  UnZoomed(XLogPix: Integer): Integer; overload;
function  UnZoomed(RectLogPix: TRect): TRect; overload;
function  UnZoomed(PosLogPix: TPoint): TPoint; overload;

procedure SetCaptureRMControl(AControl: TrmGraphicControl);
function  GetCaptureRMControl: TrmGraphicControl;
procedure SetCaptureDsgnControl(AControl: TrmdCustomDesignObject);

function  DataFieldInfo(AObject: TrwComponent; DefaultDS: TrwComponent): TrmDataFieldInfoRec;

implementation

uses Types;

var
  FCapturedControl: TObject;


function DataFieldInfo(AObject: TrwComponent; DefaultDS: TrwComponent): TrmDataFieldInfoRec;
var
  h: String;
  O: IrmDesignObject;
begin
  Initialize(Result);

  h := (AObject as IrmQueryAwareObject).GetFieldName;
  if h <> '' then
  begin
    Result.Name := GetPrevStrValue(h, '.');
    Result.RootPath := h;
    if Result.RootPath = '' then
      if Assigned(DefaultDS) then
      begin
        Result.RootPath := DefaultDS.PathFromRootOwner;
        Result.DefaultDS := True;
      end
      else
        Exit;

    O := ComponentByPath(Result.RootPath, AObject.RootOwner);
    Result.DefaultDS := Assigned(DefaultDS) and (O.RealObject = DefaultDS);    
    Result.RootPath := Result.RootPath + '.' + Result.Name;

    if Assigned(O) and Supports(O.RealObject, IrmQueryDrivenObject) then
    begin
      Result.DataSource := O as IrmQueryDrivenObject;
      Result.QBField := Result.DataSource.GetQuery.GetQBQuery.GetQBFieldByName(Result.Name);
      if Assigned(Result.QBField) then
        Result.RootPath := Result.RootPath + '.' + Result.QBField.GetID;
    end;
  end;
end;


function Zoomed(XLogPix: Integer): Integer;
var
  n: Extended;
begin
  n := XLogPix * ZoomFactor;
  Result := Trunc(n);
  if Frac(n) >= 0.5 then
    Inc(Result);
end;

function Zoomed(RectLogPix: TRect): TRect;
begin
  Result.Left := Zoomed(RectLogPix.Left);
  Result.Top := Zoomed(RectLogPix.Top);
  Result.Right := Zoomed(RectLogPix.Right);
  Result.Bottom := Zoomed(RectLogPix.Bottom);
end;

function ZoomFactor: Extended;
begin
  if FZoomFactor = 0 then
    FZoomFactor := 1;
  Result := FZoomFactor;
end;

function Zoomed(PosLogPix: TPoint): TPoint;
begin
  Result.X := Zoomed(PosLogPix.X);
  Result.Y := Zoomed(PosLogPix.Y);
end;

function UnZoomed(XLogPix: Integer): Integer;
begin
  Result := Round(XLogPix / ZoomFactor);
end;

function UnZoomed(RectLogPix: TRect): TRect;
begin
  Result.Left := UnZoomed(RectLogPix.Left);
  Result.Top := UnZoomed(RectLogPix.Top);
  Result.Right := UnZoomed(RectLogPix.Right);
  Result.Bottom := UnZoomed(RectLogPix.Bottom);
end;

function UnZoomed(PosLogPix: TPoint): TPoint;
begin
  Result.X := UnZoomed(PosLogPix.X);
  Result.Y := UnZoomed(PosLogPix.Y);
end;

procedure SetCaptureRMControl(AControl: TrmGraphicControl);
begin
  FCapturedControl := AControl;
  if Assigned(AControl) then
    SetCaptureControl(AControl.GetWinControlParent)
  else
    SetCaptureControl(nil);
end;

function GetCaptureRMControl: TrmGraphicControl;
begin
  Result := nil;
  if FCapturedControl is TrmGraphicControl then
    Result := TrmGraphicControl(FCapturedControl);
end;

procedure SetCaptureDsgnControl(AControl: TrmdCustomDesignObject);
begin
  FCapturedControl := AControl;
  if Assigned(AControl) then
    SetCaptureControl(AControl.Parent)
  else
    SetCaptureControl(nil);
end;


{ TrmdBoundsResizer }

procedure TrmdBoundsResizer.Paint;
begin
  with Canvas do
  begin
    Brush.Color := clGray;
    Brush.Style := bsSolid;
    FillRect(ClientRect);
  end;
end;


{ TrmdCustomDesignObject }

constructor TrmdCustomDesignObject.Create(AOwner: TrmGraphicControl);
begin
  FOwner := AOwner;
  ((FOwner as IrmDesignObject).GetWinControlParent as TrmdCustomGraphicWorkSpace).AddDsgnObject(Self);
end;

destructor TrmdCustomDesignObject.Destroy;
begin
  if Assigned(Parent) and not (csDestroying in Parent.ComponentState) then
  begin
    Visible := False;
    FParent.DeleteDsgnObject(Self);
  end

  else if FShape <> 0 then
    DeleteObject(FShape);

  inherited;
end;

procedure TrmdCustomDesignObject.Invalidate;
begin
  if Assigned(Parent) and Visible and (FShape <> 0) then
    InvalidateRgn(FParent.Handle, FShape, FTransparent);
end;

procedure TrmdCustomDesignObject.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
end;

procedure TrmdCustomDesignObject.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
end;

procedure TrmdCustomDesignObject.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
end;

procedure TrmdCustomDesignObject.PaintToCanvas(ACanvas: TrwCanvas);
begin
end;

procedure TrmdCustomDesignObject.SetVisible(const Value: Boolean);
begin
  FVisible := Value;
  UpdateRegion;
end;

procedure TrmdCustomDesignObject.UpdateRegion;
begin
  if Assigned(FParent) and (FShape <> 0) then
  begin
    InvalidateRgn(FParent.Handle, FShape, True);
    DeleteObject(FShape);
    FShape := 0;
  end;

  if Visible then
  begin
    FShape := GetShapeRegion;
    Invalidate;
  end;  
end;

{ TrmdCustomWorkSpace }

function TrmdCustomWorkSpace.Container: TWinControl;
begin
  Result := Self;
end;

constructor TrmdCustomWorkSpace.Create(AOwner: TComponent);
begin
  inherited Create(nil);

  FrwObject := AOwner as TrwComponent;

  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrmdCustomWorkSpace.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [];
  Result.ResizingDirections := [];
  Result.Selectable := True;
  Result.SelectorType := rwDBISBNone;
  Result.StandaloneObject := False;
  Result.AcceptControls := True;
  Result.AcceptedClases := '';
end;

destructor TrmdCustomWorkSpace.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrmdCustomWorkSpace.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrmdCustomWorkSpace.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType = rwCPInvalidate then
    Invalidate;
end;

function TrmdCustomWorkSpace.RealObject: TControl;
begin
  Result := Self;
end;

function TrmdCustomWorkSpace.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrmdCustomWorkSpace.UpdateSize;
begin
end;


{ TrmdCustomGraphicWorkSpace }

constructor TrmdCustomGraphicWorkSpace.Create(AOwner: TComponent);
begin
  inherited;
  FCanvas := TrwCanvas.Create;
  FDsgnObjects := TList.Create;
end;

destructor TrmdCustomGraphicWorkSpace.Destroy;
begin
  DeleteAllDsgnObject;
  FreeAndNil(FDsgnObjects);
  FreeAndNil(FCanvas);
  inherited;
end;

procedure TrmdCustomGraphicWorkSpace.AddDsgnObject(AObject: TrmdCustomDesignObject);
begin
  Assert(IndexOfDsgnObject(AObject) = -1);
  FDsgnObjects.Add(AObject);
  AObject.FParent := Self;
end;

procedure TrmdCustomGraphicWorkSpace.DeleteDsgnObject(AObject: TrmdCustomDesignObject);
var
  i: Integer;
begin
  i := IndexOfDsgnObject(AObject);
  Assert(i <> -1);
  FDsgnObjects.Delete(i);
  AObject.FParent := nil;  
end;

procedure TrmdCustomGraphicWorkSpace.Paint;
begin
end;

procedure TrmdCustomGraphicWorkSpace.PaintCustomDsgnObjects;
var
  i: Integer;
  O: TrmdCustomDesignObject;
begin
  for i := 0 to FDsgnObjects.Count - 1 do
  begin
    O := TrmdCustomDesignObject(FDsgnObjects[i]);
    if O.Visible then
    begin
      ExtSelectClipRgn(FCanvas.Handle, O.Shape, RGN_OR);
      O.PaintToCanvas(FCanvas);
    end;
  end;
end;

procedure TrmdCustomGraphicWorkSpace.PaintWindow(DC: HDC);
var
  ClipRgn: HRGN;

  procedure CheckOpaqueCustomDsgnObjects;
  var
    i: Integer;
    O: TrmdCustomDesignObject;
  begin
    for i := 0 to FDsgnObjects.Count - 1 do
    begin
      O := TrmdCustomDesignObject(FDsgnObjects[i]);
      if O.Visible and not O.FTransparent then
        ExtSelectClipRgn(FCanvas.Handle, O.Shape, RGN_DIFF);
    end;
  end;

begin
  FCanvas.Handle := DC;
  try
    ClipRgn := CreateRectRgn(0, 0, Width, Height);
    SelectClipRgn(DC, ClipRgn);
    CheckOpaqueCustomDsgnObjects;
    Paint;
    PaintCustomDsgnObjects;
    DeleteObject(ClipRgn);
  finally
    FCanvas.Handle := 0;
  end;
end;

procedure TrmdCustomGraphicWorkSpace.WMEraseBkgnd(var Message: TWmEraseBkgnd);
begin
  Message.Result := 0;
end;

function TrmdCustomGraphicWorkSpace.IndexOfDsgnObject(AObject: TrmdCustomDesignObject): Integer;
begin
  Result := FDsgnObjects.IndexOf(AObject);
end;

procedure TrmdCustomGraphicWorkSpace.DeleteAllDsgnObject;
var
  i: Integer;
begin
  for i := 0 to FDsgnObjects.Count - 1 do
    TrmdCustomDesignObject(FDsgnObjects[i]).FParent := nil;
  FDsgnObjects.Clear;
end;

procedure TrmdCustomGraphicWorkSpace.WndProc(var Message: TMessage);

  function DispatchOriginalMessage: Boolean;
  begin
    Result := IsDesignMsg(FrwObject, Message);
    if not Result then
      inherited WndProc(Message);
  end;

begin
  if not DispatchMessage(Message) then
    DispatchOriginalMessage;
end;


function TrmdCustomGraphicWorkSpace.DispatchMessage(var Message: TMessage): Boolean;
var
  i: Integer;
  O: TrmdCustomDesignObject;
  P: TPoint;

  procedure ControlMouseEvent;
  begin
    with TWMMouse(Message) do
      case Message.Msg of
        WM_MOUSEMOVE:      O.MouseMove(KeysToShiftState(Keys), XPos, YPos);
        WM_LBUTTONDOWN:    O.MouseDown(mbLeft, KeysToShiftState(Keys), XPos, YPos);
        WM_LBUTTONUP:      O.MouseUp(mbLeft, KeysToShiftState(Keys), XPos, YPos);
        WM_LBUTTONDBLCLK:;
        WM_RBUTTONDOWN:    O.MouseDown(mbRight, KeysToShiftState(Keys), XPos, YPos);
        WM_RBUTTONUP:      O.MouseUp(mbRight, KeysToShiftState(Keys), XPos, YPos);
        WM_RBUTTONDBLCLK:;
        WM_MBUTTONDOWN:    O.MouseDown(mbMiddle, KeysToShiftState(Keys), XPos, YPos);
        WM_MBUTTONUP:      O.MouseUp(mbMiddle, KeysToShiftState(Keys), XPos, YPos);
        WM_MBUTTONDBLCLK:;
        WM_MOUSEWHEEL:;
      end;
  end;

begin
  Result := False;

  if (Message.Msg >= WM_MOUSEFIRST) and (Message.Msg <= WM_MOUSELAST) then
  begin
    if Assigned(FCapturedControl) and not(FCapturedControl is TrmdCustomDesignObject) then
      Exit;

    P := Point(Smallint(Message.LParamLo), Smallint(Message.LParamHi));
    O := nil;

    if Assigned(FCapturedControl) then
      O := TrmdCustomDesignObject(FCapturedControl)

    else
      for i := 0 to FDsgnObjects.Count - 1 do
        if not TrmdCustomDesignObject(FDsgnObjects[i]).FIgnoreMouse and PtInRegion(TrmdCustomDesignObject(FDsgnObjects[i]).Shape, P.X, P.Y) then
        begin
          O := TrmdCustomDesignObject(FDsgnObjects[i]);
          break;
        end;

    if Assigned(O) then
    begin
      ControlMouseEvent;
      Result := True;
    end;
  end;
end;


procedure TrmdCustomGraphicWorkSpace.UpdateMouseLastControl(const AObject: TrmGraphicControl);
begin
  if (FLastMouseControl <> AObject) and Assigned(FLastMouseControl) then
    FLastMouseControl.FMousePosition := rmdMPClient;
  FLastMouseControl := AObject;
end;

{ TrmdMovableDesignObject }

constructor TrmdMovableDesignObject.Create(AOwner: TrmGraphicControl);
begin
  inherited;
  AllowMove := True;
end;

procedure TrmdMovableDesignObject.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  P1, P2: TPoint;
  O: IrmDesignObject;
begin
  Owner.FMousePosition := rmdMPClient;
  O := Owner as IrmDesignObject;

  O.GetDesigner.Notify(rmdSelectObject, VarArrayOf([Integer(Pointer(Owner as IrmDesignObject)), False]));

  if AllowMove then
  begin
    O := Owner as IrmDesignObject;
    P1 := O.ClientToScreen(Point(0, 0));
    P2 := O.GetWinControlParent.ClientToScreen(Point(X, Y));
    O.GetDesigner.Notify(rmdObjectTagMouseDown, VarArrayOf([P2.X - P1.X, P2.Y - P1.Y]));
  end;
end;



{ TrmdSimpleTextTag }

procedure TrmdSimpleTextTag.CorrectTagBounds(var ATagRect: TRect);
begin
  OffsetRect(ATagRect, ATagRect.Left - ATagRect.Right, ATagRect.Top - ATagRect.Bottom);
end;

constructor TrmdSimpleTextTag.Create(AOwner: TrmGraphicControl);
var
  kf: Extended;
begin
  inherited;

  MakeText;

  SetTextMetrics;
  // convert to objects DPI
  kf := FOwner.CanvasDPI / cVirtualCanvasRes;
  FFontSize := Round(FFontSize * kf);
  FTextOffset.X := Round(FTextOffset.X * kf);
  FTextOffset.Y := Round(FTextOffset.Y * kf);

  Visible := True;
end;

function TrmdSimpleTextTag.GetShapeRegion: HRGN;
var
  TxtSize: TSize;
  R: TRect;
begin
  MakeText;
  
  TxtSize := GetTextSize;

  R.TopLeft := Owner.ClientToScreen(Point(0, 0));
  R.TopLeft := Parent.ScreenToClient(R.TopLeft);
  R.Right   := R.Left + TxtSize.cx;
  R.Bottom  := R.Top + TxtSize.cy;

  CorrectTagBounds(R);

  Result := CreateRectRgnIndirect(R);
end;

function TrmdSimpleTextTag.GetTextSize: TSize;
begin
  Parent.Canvas.Font.Name := 'Areal';
  Parent.Canvas.Font.Style := [];
  Parent.Canvas.Font.Size := Zoomed(FFontSize);
  Result.cy := Parent.Canvas.TextHeight(FText) + Zoomed(FTextOffset.Y);
  Result.cx := Parent.Canvas.TextWidth(FText) + Zoomed(FTextOffset.X);
end;

procedure TrmdSimpleTextTag.MakeText;
begin
  FText := FOwner.GetShowText;
end;

procedure TrmdSimpleTextTag.PainTag(ACanvas: TrwCanvas; ShapeRegion: HRGN);
var
  R: TRect;
  flSelected: Boolean;
begin
  flSelected := Owner.Designer.IsObjectSelected(Owner);

  with ACanvas do
  begin
    if flSelected then
      Brush.Color := clYellow
    else
      Brush.Color := $00E1FFFF;
    Brush.Style := bsSolid;
    FillRgn(Handle, ShapeRegion, Brush.Handle);
    Brush.Style := bsClear;

    GetRgnBox(ShapeRegion, R);
    Font.Name := 'Areal';
    Font.Size := Zoomed(FFontSize);
    Font.Style := [fsItalic];
    Font.Color := clBlack;
    DrawText(FText, R, DT_CENTER OR DT_VCENTER OR DT_SINGLELINE);

    Pen.Mode := pmCopy;
    Pen.Style := psSolid;
    Pen.Color := clGray;
    Pen.Width := 1;
    Rectangle(R);
  end;
end;

procedure TrmdSimpleTextTag.PaintToCanvas(ACanvas: TrwCanvas);
begin
  PainTag(ACanvas, Shape);
end;

procedure TrmdSimpleTextTag.SetTextMetrics;
begin
  // All in virtual DPI
  FFontSize := 44;
  FTextOffset := Point(52, 26);
end;


{ TrmGraphicControl }

constructor TrmGraphicControl.Create(AOwner: TComponent);
begin
  inherited;
  FCursor := crDefault;  
end;

destructor TrmGraphicControl.Destroy;
begin
  FreeAndNil(FObjectTag);
  inherited;
end;

function TrmGraphicControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  if VisualControlCreated then
    Result := inherited DesignBehaviorInfo
  else
  begin
    Result.MovingDirections := [];
    Result.ResizingDirections := [];
    Result.Selectable := GetDesigner.LibComponentDesigninig;
    Result.SelectorType := rwDBISBDots;
    Result.StandaloneObject := True;
    Result.AcceptControls := False;    
    Result.AcceptedClases := '';
  end;
end;

function TrmGraphicControl.DoNotPaint: Boolean;
var
  fl: Boolean;
begin
  Result := GetDesigner <> nil;
  if Result then
  begin
    fl := GetDesigner.LibComponentDesigninig;
    Result := not (fl or not fl and (not InheritedComponent or InheritedComponent and (DsgnLockState <> rwLSHidden)));
  end;  
end;

procedure TrmGraphicControl.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (GetDesigner = nil) or not ((DsgnLockState = rwLSUnlocked) or GetDesigner.LibComponentDesigninig) then
    Exit;

  if (FMousePosition in FAllowedResizingBounds) and (Button = mbLeft) then
  begin
    FOldMousePos := Point(X, Y);
    FStartResizeMousePosition := FMousePosition;
    InitResizer;
  end;
end;

procedure TrmGraphicControl.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  dLeft, dRight, dTop, dBottom, d, dX, dY: Integer;
begin
  if (FAllowedResizingBounds = []) or (GetDesigner = nil) or not ((DsgnLockState = rwLSUnlocked) or GetDesigner.LibComponentDesigninig) then
  begin
    Cursor := crDefault;
    Exit;
  end;

  if not ResizingBoundsNow then
  begin
    d := MouseResizingThreshold;
    if X <= d then
      FMousePosition := rmdMPLeftBound
    else if Width - X <= d then
      FMousePosition := rmdMPRightBound
    else if Y <= d then
      FMousePosition := rmdMPTopBound
    else if Height - Y <= d then
      FMousePosition := rmdMPBottomBound
    else
      FMousePosition := rmdMPClient;

    if not (FMousePosition in FAllowedResizingBounds) then
    begin
      Cursor := crDefault;
      FMousePosition := rmdMPClient;
      Exit;
    end;

    if FMousePosition in FAllowedResizingBounds then
      case FMousePosition of
        rmdMPLeftBound,
        rmdMPRightBound:  Cursor := crHSplit;

        rmdMPTopBound,
        rmdMPBottomBound: Cursor := crVSplit;
      else
         Cursor := crDefault;
      end

    else
      Cursor := crDefault;
  end

  else
  begin
    dLeft   := 0;
    dRight  := 0;
    dTop    := 0;
    dBottom := 0;

    dX := Round((X - FOldMousePos.X) / MouseResizingDiscretion) * MouseResizingDiscretion;
    dY := Round((Y - FOldMousePos.Y) / MouseResizingDiscretion) * MouseResizingDiscretion;

    case FMousePosition of
      rmdMPLeftBound:
        dLeft := dX;
      rmdMPRightBound:
        dRight := dX;
      rmdMPTopBound:
        dTop := dY;
      rmdMPBottomBound:
        dBottom := dY;
    end;

    FBounsToApplay.Left := FBounsToApplay.Left + dLeft;
    FBounsToApplay.Right := FBounsToApplay.Right + dRight;
    FBounsToApplay.Top := FBounsToApplay.Top + dTop;
    FBounsToApplay.Bottom := FBounsToApplay.Bottom + dBottom;

    Inc(FOldMousePos.X, dLeft + dRight);
    Inc(FOldMousePos.Y, dTop + dBottom);

    UpdateResizer;
  end;
end;

procedure TrmGraphicControl.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (FAllowedResizingBounds <> []) and (Button = mbLeft) and ResizingBoundsNow then
  begin
    HideResizer;
    ApplyMouseResizing(FBounsToApplay, FMousePosition, Shift);
  end;
end;

procedure TrmGraphicControl.PaintControl(ACanvas: TrwCanvas);
begin
end;

procedure TrmGraphicControl.PaintToCanvas(ACanvas: TrwCanvas);
begin
  PaintControl(ACanvas);
end;

procedure TrmGraphicControl.SetCursor(const Value: TCursor);
begin
  FCursor := Value;
  GetWinControlParent.Perform(CM_CURSORCHANGED, 0, 0);
end;

procedure TrmGraphicControl.SetDesignParent(Value: TWinControl);
var
  i: Integer;
begin
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TrwNoVisualComponent then
       TrwComponent(Components[i]).DesignParent := Value;
end;

procedure TrmGraphicControl.Refresh;
begin
  Invalidate;
end;

procedure TrmGraphicControl.Invalidate;
begin
  InvalidateRect(Rect(0, 0, Width, Height));
end;

procedure TrmGraphicControl.InvalidateRect(const ARect: TRect);
var
  P: TWinControl;
  R: TRect;
begin
  P := GetWinControlParent;
  if Assigned(P) and P.HandleAllocated then
  begin
    R.TopLeft := ClientToScreen(ARect.TopLeft);
    R.BottomRight := ClientToScreen(ARect.BottomRight);
    R.TopLeft := P.ScreenToClient(R.TopLeft);
    R.BottomRight := P.ScreenToClient(R.BottomRight);
    Windows.InvalidateRect(P.Handle, @R, True);
  end;
end;

function TrmGraphicControl.GetBoundsRect: TRect;
begin
  Result := BoundsRect;
end;

procedure TrmGraphicControl.SetBoundsRect(ARect: TRect);
begin
  if not EqualRect(BoundsRect, ARect) then
    SetBounds(ARect.Left, ARect.Top, ARect.Right - ARect.Left, ARect.Bottom - ARect.Top);
end;

procedure TrmGraphicControl.BeforeChangePos;
begin
  inherited;
  Invalidate;
end;

procedure TrmGraphicControl.AfterChangePos;
begin
  inherited;
  Invalidate;
end;

procedure TrmGraphicControl.DoSyncVisualControl(const APropID: TrwChangedPropID);
begin
  inherited;
  Invalidate;
end;

procedure TrmGraphicControl.SetDescription(const Value: String);
begin
  inherited;
  Invalidate;
end;

procedure TrmGraphicControl.SetName(const NewName: TComponentName);
begin
  inherited;
  Invalidate;
end;

procedure TrmGraphicControl.ApplyMouseResizing(NewBounds: TRect; MousePosition: TrmdMousePosition; Shift: TShiftState);
begin
end;

procedure TrmGraphicControl.HideResizer;
begin
  FreeAndNil(FResizer);
  SetCaptureRMControl(nil);
end;

procedure TrmGraphicControl.InitResizer;
begin
  FreeAndNil(FResizer);
  if FMousePosition <> rmdMPClient then
  begin
    FBounsToApplay := BoundsRect;
    FResizer := TrmdBoundsResizer.Create(nil);
    FResizer.SetBounds(0,0,0,0);
    FResizer.Parent := GetWinControlParent;
    FResizer.BringToFront;
    SetCaptureRMControl(Self);
    UpdateResizer;
  end;
end;

function TrmGraphicControl.ResizingBoundsNow: Boolean;
begin
  Result := Assigned(FResizer);
end;

procedure TrmGraphicControl.UpdateResizer;
var
  R: TRect;
begin
  R.TopLeft := GetParent.ClientToScreen(FBounsToApplay.TopLeft);
  R.BottomRight := GetParent.ClientToScreen(FBounsToApplay.BottomRight);
  R.TopLeft := FResizer.Parent.ScreenToClient(R.TopLeft);
  R.BottomRight := FResizer.Parent.ScreenToClient(R.BottomRight);

  case FMousePosition of
    rmdMPLeftBound:
      FResizer.SetBounds(R.Left - 1, 0, 2, FResizer.Parent.ClientHeight);

    rmdMPRightBound:
      FResizer.SetBounds(R.Right - 1, 0, 2, FResizer.Parent.ClientHeight);

    rmdMPTopBound:
      FResizer.SetBounds(0, R.Top - 1 , FResizer.Parent.ClientWidth, 2);

    rmdMPBottomBound:
      FResizer.SetBounds(0, R.Bottom - 1 ,FResizer.Parent.ClientWidth, 2);
  end;

  FResizer.Parent.Update;
end;

function TrmGraphicControl.MouseResizingThreshold: Integer;
begin
  Result := 4;
end;

function TrmGraphicControl.MouseResizingDiscretion: Integer;
begin
  Result := 1;
end;

procedure TrmGraphicControl.HideTag;
begin
  if Assigned(FObjectTag) then
    FObjectTag.Visible := False;
end;

procedure TrmGraphicControl.ShowTag;
begin
 if not Assigned(FObjectTag) then
  begin
    if Assigned(FObjectTagClass) then
      FObjectTag := FObjectTagClass.Create(Self)
    else
      FObjectTag := nil;
 end
 else
   FObjectTag.Visible := True;
end;

procedure TrmGraphicControl.SyncTagPos;
begin
  if Assigned(FObjectTag) and FObjectTag.Visible then
    FObjectTag.UpdateRegion;
end;

procedure TrmGraphicControl.Notify(AEvent: TrmDesignEventType);
begin
  inherited;

  if AEvent in [rmdSelectObject] then
    ShowTag
  else if AEvent in [rmdDestroyResizer] then
    HideTag
  else if AEvent in [rmdZoomChanged] then
    SyncTagPos;
end;

function TrmGraphicControl.GetShowText: String;
begin
  if Description <> '' then
    Result := Description
  else
    Result := Name;
end;


{ TrmsValueHolder }

constructor TrmsValueHolder.Create(AOwner: IrmQueryAwareObject);
begin
  FOwner := AOwner;
  FFieldIndex := -1;
  FValue := Null;
end;

function TrmsValueHolder.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TrmsValueHolder.PrepareDataField;
var
  Rec: TrmDataFieldInfoRec;
  Fld: TrwField;
begin
  if FFieldIndex = -1 then
  begin
    if not Assigned(Rec.DataSource) then
    begin
      Rec := FOwner.GetDataFieldInfo;
      FDataSource := TrwDataSet(Rec.DataSource.GetQuery.VCLObject);
    end
    else
      Initialize(Rec);

    if FDataSource.PActive then
    begin
      if Rec.Name = '' then
        Rec := FOwner.GetDataFieldInfo;
      Fld := FDataSource.Fields.FindField(Rec.Name);
      if Assigned(Fld) then
        FFieldIndex := Fld.Index;
    end;
  end;
end;

procedure TrmsValueHolder.SetDataField(const AValue: String);
var
  Rec: TrmDataFieldInfoRec;
begin
  FDataField := AValue;
  FValue := Null;
  FFieldIndex := -1;
  FDataSource := nil;

  if not TrwComponent(FOwner.VCLObject).Loading then
  begin
    Rec := FOwner.GetDataFieldInfo;
    if Assigned(Rec.DataSource) then
    begin
      if Rec.DefaultDS then
        FDataField := Rec.Name;
    end
    else
      FDataField := '';
  end;
end;

procedure TrmsValueHolder.SetValue(const AValue: Variant);
var
  V: Variant;
begin
  if Assigned(FOnChangeValue) then
  begin
    V := AValue;
    FOnChangeValue(V);
    FValue := V;
  end
  else
    FValue := AValue;
end;

procedure TrmsValueHolder.SetValueFromDataSource;

  procedure SetValueFromDataSourceForChildren(AParent: IrmDesignObject);
  var
    i: Integer;
    C: IrmDesignObject;
  begin
    for i := 0 to AParent.GetControlCount - 1 do
    begin
      C := AParent.GetControlByIndex(i);
      if Supports(C.RealObject, IrmQueryAwareObject) then
        (C as IrmQueryAwareObject).SetValueFromDataSource;
      SetValueFromDataSourceForChildren(C);
    end;
  end;

begin
  if DataField <> '' then
  begin
    PrepareDataField;
    if (FFieldIndex <> -1) and FDataSource.PActive then
      Value := FDataSource.Fields[FFieldIndex].Value
    else
      Value := Null;
  end;    

  SetValueFromDataSourceForChildren(FOwner);
end;

end.
