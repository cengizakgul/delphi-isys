unit rwQBInfo;

interface

uses Windows, Classes, SysUtils, Forms, DB, Variants,
     rwBasicClasses, rwDataDictionary, rmTypes, sbSQL, sbConstants,
     rwCustomDataDictionary, EvStreamUtils, rwBasicUtils, rwEngineTypes,
     rwTypes;
const
  qbEmbeddedQueryName = 'SubQuery';

type
  { TrwQBFilterNode store information about filter condition for table}

  TrwQBQuery = class;
  TrwQBSelectedTable = class;
  TrwQBExpression = class;
  TrwQBSelectedTables = class;

  TrwQBFilterNodeType = (rntAnd, rntOr);

  TrwSQLStatementType = (rssSelect, rssInsert, rssUpdate, rssDelete);

  TrwQBExprItemType = (eitNone, eitBracket, eitSeparator, eitOperation,
                       eitFunction, eitField, eitParam, eitConst, eitIntConst,
                       eitFloatConst, eitStrConst, eitDateConst,
                       eitNull, eitTrue, eitFalse);


  TrwQBFunction = class(TrwFunction)
  private
    FHeaderText: String;
    FDefaultParams: String;
  public
    property HeaderText: String read FHeaderText;

    procedure GetParam(const AParamIndex: Integer; var AType: TrwQBExprItemType;
                       var AValue: Variant; var ADescr: String);
  end;


  TrwQBFunctions = class(TrwFunctions)
  public
    procedure Initialize; override;
    procedure AddFunction(const AHeaderText: String; const AGroup: String;
                          const ADescription: String; const ADefaultParams: String = '');
    procedure Execute(const APar1: string; const APar2: string = ''); override;
  end;




  TrwQBTemplate = class (TrwLibCollectionItem)
  public
    function  CreateQuery: TrwQBQuery;
    procedure LoadFromStream(AStream: TStream);
  end;



  TrwQBTemplates = class (TrwLibCollection)
  private
    function  GetItem(Index: Integer): TrwQBTemplate;
  public
    property  Items[Index: Integer]: TrwQBTemplate read GetItem; default;
    function  Add: TrwQBTemplate; reintroduce;
    procedure Initialize; override;
    procedure Store; override;
    procedure SaveToStream(AStream: TStream); override;
    procedure LoadFromStream(AStream: TStream); override;
  end;




  TrwQBOwnedCollection = class(TInterfacedCollection)
  protected
    function  GetOwner: TPersistent; override;
  public
    FOwner: TPersistent;
  end;


  TrwQBExpressionItem = class(TInterfacedCollectionItem)
  private
    FValue: Variant;
    FItemType: TrwQBExprItemType;
    FDescription: String;
    function GetOwnr: TrwQBExpression;
    procedure SetValue(const Value: Variant);
  public
    function  IsDescrNotEmpty: Boolean;
    procedure Assign(ASource: TPersistent); override;
    property  Owner: TrwQBExpression read GetOwnr;
    function  GetSelectedTable: TrwQBSelectedTable;
    function  GetDDFieldInfo: TrwDataDictField;

  published
    property ItemType: TrwQBExprItemType read FItemType write FItemType;
    property Value: Variant read FValue write SetValue;
    property Description: String read FDescription write FDescription stored IsDescrNotEmpty;
  end;


  TrwQBExpression = class(TInterfacedCollectionItem)
  private
    FOwner: TObject;
    FContent: TrwQBOwnedCollection;
    FExprType: TrwVarTypeKind;
    procedure SetContent(const Value: TrwQBOwnedCollection);
    function GetItem(Index: Integer): TrwQBExpressionItem;
    function GetExprType: TrwVarTypeKind;
  public
    property Owner: TObject read FOwner write FOwner;
    property Items[Index: Integer]: TrwQBExpressionItem read GetItem; default;

    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    function  TextForDesign(AWizard: Boolean = False): string;
    function  TextForQuery: string;
    function  AddItem(AType: TrwQBExprItemType; AValue: Variant; ADescr: String = ''): TrwQBExpressionItem;
    function  Count: Integer;
    procedure Clear;
    procedure Assign(Source: TPersistent); override;
    function  GetSelectedTables: TrwQBSelectedTables; virtual;
    function  ThereAreAggrFncts: Boolean;
    function  CheckTypeCompatibility(AType1, AType2: TrwVarTypeKind; AIgnoreArrays: Boolean = False): Boolean;
    function  CheckErrors(var ErrItem: Integer): String;
    function  ContentToString: String;
    procedure ContentFromString(AContent: String);
    procedure FixUpTableIntName(const AOldName, ANewName: String);

  published
    property ExprType: TrwVarTypeKind read GetExprType stored False;
    property Content: TrwQBOwnedCollection read FContent write SetContent;
  end;


  TrwQBExpressions = class(TrwQBOwnedCollection)
  end;

  TrwQBExpressionsClass = class of TrwQBExpressions;


  TrwQBFilterNodeCollection = class(TInterfacedCollection)
  private
    FTable: TrwQBSelectedTable;
  public
    property Table: TrwQBSelectedTable read FTable write FTable;
  end;


  TrwQBFilterNode = class(TInterfacedCollectionItem)
  private
    FConditions: TCollection;
    FNodes: TCollection;
    FNodeType: TrwQBFilterNodeType;
    FTable: TrwQBSelectedTable;
    FNotOper: Boolean;

    procedure SetConditions(AItems: TCollection);
    procedure SetNodes(AItems: TCollection);
    procedure SetTable(const Value: TrwQBSelectedTable);
    function IsNodesNotEmpty: Boolean;

  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function TextForDesign: string;
    function TextForQuery(ATable: TrwQBSelectedTable): string;
    procedure FixUpTableIntName(const AOldName, ANewName: String);
    property Table: TrwQBSelectedTable read FTable write SetTable;

  published
    property NodeType: TrwQBFilterNodeType read FNodeType write FNodeType default rntAnd;
    property Conditions: TCollection read FConditions write SetConditions;
    property Nodes: TCollection read FNodes write SetNodes stored IsNodesNotEmpty;
    property NotOper: Boolean read FNotOper write FNotOper default False;                     //obsolete
  end;


  TrwQBFilterCondition = class(TInterfacedCollectionItem)
  private
    FOperation: string;
    FNotOper: Boolean;
    FLeftPart: TrwQBExpression;
    FRightPart: TrwQBExpressions;
    FOperationAux: string;
    FObsoleteStr: string;
    procedure SetLeftPart(const Value: TrwQBExpression);
    procedure SetRightPart(const Value: TrwQBExpressions);
    function IsNotEmptyLeftPart: Boolean;
    function IsNotEmptyRightPart: Boolean;
    function IsNotEmptyOperationAux: Boolean;

  public
    function TextForDesign(AWizard: Boolean = False): string;
    function TextForQuery(ATable: TrwQBSelectedTable): string;
    procedure Assign(Source: TPersistent); override;
    procedure FixUpTableIntName(const AOldName, ANewName: String);
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

  published
    property NotOper: Boolean read FNotOper write FNotOper default False;
    property Operation: string read FOperation write FOperation;
    property OperationAux: string read FOperationAux write FOperationAux stored IsNotEmptyOperationAux;
    property LeftPart: TrwQBExpression read FLeftPart write SetLeftPart stored IsNotEmptyLeftPart;
    property RightPart: TrwQBExpressions read FRightPart write SetRightPart stored IsNotEmptyRightPart;

    property SUBQuery: string read FObsoleteStr write FObsoleteStr stored False;                  //obsolete
    property LeftFieldName: string read FObsoleteStr write FObsoleteStr stored False;             //obsolete
    property LeftExpression: string read FObsoleteStr write FObsoleteStr stored False;            //obsolete
    property RightValue: string read FObsoleteStr write FObsoleteStr stored False;                //obsolete
    property RightExpression: string read FObsoleteStr write FObsoleteStr stored False;           //obsolete
    property FromValue: string read FObsoleteStr write FObsoleteStr stored False;                 //obsolete
    property ToValue: string read FObsoleteStr write FObsoleteStr stored False;                   //obsolete
    property InValue: string read FObsoleteStr write FObsoleteStr stored False;                   //obsolete
  end;


  { TrwQBSelectedTable store information about selected table}

  TrwQBSelectedTable = class(TInterfacedCollectionItem)
  private
    FDestroying: Boolean;
    FTableName: string;
    FAlias: string;
    FInternalName: string;
    FLeft: Integer;
    FTop: Integer;
    FWidth: Integer;
    FHeight: Integer;
    FSplitter: Integer;
    FFieldColumnWidth: Integer;
    FVisualComponent: TComponent;
    FFilter: TrwQBFilterNode;
    FSUBQuery: TrwQBQuery;
    FTableParams: TrwQBExpressions;
    FIsolatedFiltering: Boolean;

    procedure SetFilter(AFilter: TrwQBFilterNode);
    procedure SetTableName(const Value: string);
    procedure SetSUBQuery(const Value: TrwQBQuery);
    procedure SetAlias(const Value: string);
    function  IsAliasNotEmpty: Boolean;
    function  IsParamsNotEmpty: Boolean;
    procedure SetLeft(const Value: Integer);
    procedure SetTop(const Value: Integer);
    procedure SetInternalName(const Value: string);
    function  GetTableParams: TrwQBExpressions;
    procedure SetTableParams(const Value: TrwQBExpressions);
    function GetParams: string;
    procedure SetParams(const Value: string);

  public
    property Destroying: Boolean read FDestroying;
    property VisualComponent: TComponent read FVisualComponent write FVisualComponent;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function  IsSUBQuery: Boolean;
    function  lqObject: TrwDataDictTable;
    function  GetTableName(AWizard: Boolean = False; AIgnoreAlias: Boolean = False): string;
    function  ContentToString: String;
    procedure ContentFromString(AContent: String);

  published
    property TableName: string read FTableName write SetTableName;
    property InternalName: string read FInternalName write SetInternalName;
    property Alias: string read FAlias write SetAlias stored IsAliasNotEmpty;
    property TableParams: TrwQBExpressions read GetTableParams write SetTableParams stored IsParamsNotEmpty;
    property Left: Integer read FLeft write SetLeft;
    property Top: Integer read FTop write SetTop;
    property Width: Integer read FWidth write FWidth;
    property Height: Integer read FHeight write FHeight;
    property Splitter: Integer read FSplitter write FSplitter;
    property FieldColumnWidth: Integer read FFieldColumnWidth write FFieldColumnWidth;
    property Filter: TrwQBFilterNode read FFilter write SetFilter;
    property SUBQuery: TrwQBQuery read FSUBQuery write SetSUBQuery stored IsSUBQuery;
    property IsolatedFiltering: Boolean read FIsolatedFiltering write FIsolatedFiltering default False;

    property Params: string read GetParams write SetParams stored False;    //obsolete
  end;

  { TrwQBSelectedTables store information about list of selected tables}

  TrwQBSelectedTables = class(TInterfacedCollection)
  private
    FQBQuery: TrwQBQuery;
    function GetItem(Index: Integer): TrwQBSelectedTable;
    procedure SetItem(Index: Integer; const Value: TrwQBSelectedTable);

  public
    function Add(ATableName: string): Integer; reintroduce;
    function FindTable(ATableName: string): TrwQBSelectedTable;
    function TableByInternalName(AInternalName: string): TrwQBSelectedTable;
    property Items[Index: Integer]: TrwQBSelectedTable read GetItem write SetItem; default;
    property QBQuery: TrwQBQuery read FQBQuery;
  end;

  TrwQBJoinType = (rjtJoin, rjtLeftJoin, rjtRightJoin, rjtFullJoin);

  { TrwQBJoin store information about join of two tables}

  TrwQBJoin = class(TInterfacedCollectionItem)
  private
    FDestroying: Boolean;
    FLeftTable: TrwQBSelectedTable;
    FRightTable: TrwQBSelectedTable;
    FJoinType: TrwQBJoinType;
    FLeftTableIntName: string;
    FRightTableIntName: string;
    FLeftTableFields: string;
    FRightTableFields: string;
    FVisualLine: TRect;

    procedure SetTableIntName(Index: Integer; Value: string);
    procedure SetRightTableFields(const Value: string);
    procedure SetLeftTableFields(const Value: string);

  public
    property Destroying: Boolean read FDestroying;
    property VisualLine: TRect read FVisualLine write FVisualLine;
    property LeftTable: TrwQBSelectedTable read FLeftTable;
    property RightTable: TrwQBSelectedTable read FRightTable;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function TextForDesign(AWizard: Boolean = False): String;

  published
    property JoinType: TrwQBJoinType read FJoinType write FJoinType default rjtJoin;
    property LeftTableIntName: string index 0 read FLeftTableIntName write SetTableIntName;
    property RightTableIntName: string index 1 read FRightTableIntName write SetTableIntName;
    property LeftTableFields: string read FLeftTableFields write SetLeftTableFields;
    property RightTableFields: string read FRightTableFields write SetRightTableFields;
  end;

  { TrwQBJoins store information about list of joins}

  TrwQBJoins = class(TInterfacedCollection)
  private
    FQBQuery: TrwQBQuery;
    function GetItem(Index: Integer): TrwQBJoin;

  public
    property QBQuery: TrwQBQuery read FQBQuery;
    property Items[Index: Integer]: TrwQBJoin read GetItem; default;
    function Add(ALeftTable: TrwQBSelectedTable; ARightTable: TrwQBSelectedTable; AType: TrwQBJoinType; ACondition: TDataDictForeignKey; AConditionField: String = ''): Integer; reintroduce;
  end;


  { TrwQBShowingField store information about field for showing}

  TrwQBShowingField = class(TrwQBExpression, IrwQBShowingField)
  private
    FFieldAlias: string;
    FInternalName: string;
    FTmpFieldName: string;
    FObsoleteBool: Boolean;
    FObsoleteStr: string;

    function  IsTmpFieldNameNotEmpty: Boolean;
    function  IsFieldAliasNotEmpty: Boolean;

  //Interface
  private
    function VCLObject: TObject;
    function GetID: String;
    function GetIndex: Integer;
    function GetName: String;
    function GetDataType: TrwVarTypeKind;
  //End

  public
    procedure Assign(Source: TPersistent); override;
    function  Attribute: TrwDataDictField;
    function  GetFieldName(AWizard: Boolean = False; AIgnoreAlias: Boolean = True): string;
    function  GetSelectedTables: TrwQBSelectedTables; override;

  published
    property TableIntName: string read FObsoleteStr write FObsoleteStr stored False;     //obsolete
    property FieldName: string read FObsoleteStr write FObsoleteStr stored False;        //obsolete
    property AggrFunction: string read FObsoleteStr write FObsoleteStr stored False;     //obsolete
    property Distinct: Boolean read FObsoleteBool write FObsoleteBool stored False;      //obsolete

    property InternalName: string read FInternalName write FInternalName;
    property FieldAlias: string read FFieldAlias write FFieldAlias stored IsFieldAliasNotEmpty;
    property TmpFieldName: string read FTmpFieldName write FTmpFieldName stored IsTmpFieldNameNotEmpty;

    function PName: Variant;
  end;


  { TrwQBShowingFields store information about list of fields for showing}

  TrwQBShowingFields = class(TrwQBExpressions)
  private
    function GetField(Index: Integer): TrwQBShowingField;
    procedure SetField(Index: Integer; Value: TrwQBShowingField);

  public
    property Items[Index: Integer]: TrwQBShowingField read GetField write SetField; default;
    function FieldByInternalName(const AInternalName: string): TrwQBShowingField;
    function FieldByName(const AName: string): TrwQBShowingField;
    function FindField(const AName: string): TrwQBShowingField;
    function Add: Integer; reintroduce;

  published
    function  PCount: Variant;
    function  PFieldByName(AFieldName: Variant): Variant;
    function  PItems(AIndex: Variant): Variant;
    procedure PDeleteWhatNotInList(AFieldList: Variant);
  end;


  { TrwQBSortField store information about field for sorting}

  TrwQBSortDirection = (rsdAsc, rsdDesc);

  TrwQBSortField = class(TInterfacedCollectionItem)
  private
    FField: TrwQBShowingField;
    FFieldIntName: string;
    FSortDirection: TrwQBSortDirection;

    procedure SetFieldIntName(Value: string);

  public
    property Field: TrwQBShowingField read FField;
    procedure Assign(Source: TPersistent); override;

  published
    property FieldIntName: string read FFieldIntName write SetFieldIntName;
    property SortDirection: TrwQBSortDirection read FSortDirection write FSortDirection;
  end;

  { TrwQBSortFields store information about list of fields for sorting}

  TrwQBSortFields = class(TInterfacedCollection)
  private
    FQBQuery: TrwQBQuery;

  public
    procedure Assign(Source: TPersistent); override;
    function  SortSQLCondition: String;
    property  QBQuery: TrwQBQuery read FQBQuery;
  end;


  { TrwQBQuery store information about query }

  TrwQBQuery = class(TrwPersistent, IrwQBQuery)
  private
    FSelectedTables: TrwQBSelectedTables;
    FJoins: TrwQBJoins;
    FShowingFields: TrwQBShowingFields;
    FSortFields: TrwQBSortFields;
    FOnAssign: TNotifyEvent;
    FDistinctResult: Boolean;
    FSQLStatement: TrwSQLStatementType;
    FTmpTableName: string;
    FParentTable: TrwQBSelectedTable;
    FDescription: String;
    FResultDataSet: TComponent;
    FMacros: String;
    FUnion: Boolean;

    procedure SetSelectedTables(Value: TrwQBSelectedTables);
    procedure SetJoins(Value: TrwQBJoins);
    procedure SetShowingFields(Value: TrwQBShowingFields);
    procedure SetSortFields(Value: TrwQBSortFields);
    function  IsNotEmptySelectedTables: Boolean;
    function  IsNotEmptyJoins: Boolean;
    function  IsNotEmptyShowingFields: Boolean;
    function  IsNotEmptySortFields: Boolean;
    function IsTmpTableNameIsNotEmpty: Boolean;
    function IsDescriptionIsNotEmpty: Boolean;
    function IsMacrosNotEmpty: Boolean;

  // Interface
  private
    function  GetFieldList: TCommaDilimitedString;
    function  GetQBFieldByName(const AFieldName: String): IrwQBShowingField;
    function  GetQBFieldByID(const AFieldID: String): IrwQBShowingField;
  // End

  public
    property ParentTable: TrwQBSelectedTable read FParentTable;

    constructor Create(AOwner: TPersistent); override;
    destructor Destroy; override;
    function  SQL(RootQuery: Boolean = False; Indent: String = ''): string;
    procedure Assign(Source: TPersistent); override;
    procedure Clear;
    function  ResultDataset: TComponent;
    function  IsConditionedSQL: Boolean;
    function  IsUnionItem: Boolean;
    function  IsEmpty: Boolean;
    procedure SaveToStream(AStream: TStream);
    procedure ReadFromStream(AStream: TStream);

  published
    property SelectedTables: TrwQBSelectedTables read FSelectedTables write SetSelectedTables stored IsNotEmptySelectedTables;
    property Joins: TrwQBJoins read FJoins write SetJoins stored IsNotEmptyJoins;
    property ShowingFields: TrwQBShowingFields read FShowingFields write SetShowingFields stored IsNotEmptyShowingFields;
    property SortFields: TrwQBSortFields read FSortFields write SetSortFields stored IsNotEmptySortFields;
    property DistinctResult: Boolean read FDistinctResult write FDistinctResult;
    property SQLStatement: TrwSQLStatementType read FSQLStatement write FSQLStatement default rssSelect;
    property TmpTableName: string read FTmpTableName write FTmpTableName stored IsTmpTableNameIsNotEmpty;
    property Description: String read FDescription write FDescription stored IsDescriptionIsNotEmpty;
    property OnAssign: TNotifyEvent read FOnAssign write FOnAssign stored False;
    property Macros: String read FMacros write FMacros stored IsMacrosNotEmpty;
    property Union: Boolean read FUnion write FUnion default False;

    function PSQLText: Variant;
  end;

  {Evolution Constants}

  TrwQBConst = class(TsbCustomSQLConstant)
  private
    FGroup: String;
  public
    procedure Assign(ASource: TPersistent); override;
  published
    property Group: String read FGroup write FGroup;
    property ConstName;
    property DisplayName;
    property ConstType;
    property StrValue;
  end;


  TrwQBConsts = class(TsbCustomSQLConstants)
  public
    class function Initialize: TsbCustomSQLConstants; override;
    function  AddConst(const AGroup, AName, ADisplayName: String; const AConstType: TsbConstType; const AStrValue: String): TrwQBConst;
    procedure Load;
    procedure Store;
    procedure SaveToStream(AStream: TStream);
    procedure LoadFromStream(AStream: TStream);
    procedure ConstsByGroup(const AGroup: String; AStringList: TStringList);
  end;


  procedure RemoveShowingField(AField: TrwQBShowingField);
  procedure RemoveAllExprItems(AQuery: TrwQBQuery; AIntFldName: String; AIntTblName: String);
  function  FindJoin(ALeftTable, ARightTable: TrwQBSelectedTable): TrwQBJoin;

implementation

uses rwEngine, rwParser, rwUtils, rwDB;


procedure RemoveShowingField(AField: TrwQBShowingField);
var
  i: Integer;
  lFields: TrwQBShowingFields;
begin
  lFields := TrwQBShowingFields(AField.Collection);

  for i := TrwQBQuery(lFields.FOwner).SortFields.Count -1 downto 0 do
    if AnsiSameText(TrwQBSortField(TrwQBQuery(lFields.FOwner).SortFields.Items[i]).FieldIntName,
         AField.InternalName) then
      TrwQBSortField(TrwQBQuery(lFields.FOwner).SortFields.Items[i]).Free;

  if Assigned(TrwQBQuery(lFields.FOwner).ParentTable) then
    RemoveAllExprItems(TrwQBSelectedTables(TrwQBQuery(lFields.FOwner).ParentTable.Collection).FQBQuery,
      AField.InternalName, TrwQBQuery(lFields.FOwner).ParentTable.InternalName);

  AField.Free;

  if Assigned(TrwQBQuery(lFields.Owner).ParentTable) and
     Assigned(TrwQBQuery(lFields.Owner).ParentTable.FVisualComponent) then
    PostMessage(TForm(TrwQBQuery(lFields.Owner).ParentTable.FVisualComponent).Handle,
      RW_QBTABLE_REFRESH, 0, 0);
end;


procedure RemoveAllExprItems(AQuery: TrwQBQuery; AIntFldName: String; AIntTblName: String);
var
  i: Integer;

  function CheckJoin(AJoin: TrwQBJoin): Boolean;

    function CheckJoinField(AFields: String): Boolean;
    var
      h: String;
    begin
      Result := False;
      while Length(AFields) > 0 do
      begin
        h := GetNextStrValue(AFields,',');
        if AnsiSameText(h, AIntFldName) then
        begin
          Result := True;
          break;
        end;
      end;
    end;

  begin
    if (Length(AIntFldName) > 0) then
      Result := (AnsiSameText(AJoin.LeftTableIntName, AIntTblName) and
                CheckJoinField(AJoin.LeftTableFields)) or
                (AnsiSameText(AJoin.RightTableIntName, AIntTblName) and
                CheckJoinField(AJoin.RightTableFields))
    else
      Result := AnsiSameText(AJoin.LeftTableIntName, AIntTblName) or
                AnsiSameText(AJoin.RightTableIntName, AIntTblName);
  end;


  function CheckExpression(AExpr: TrwQBExpression): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := 0 to AExpr.Count - 1 do
      if (TrwQBExpressionItem(AExpr.Items[i]).ItemType = eitField) and
         ((Length(AIntFldName) > 0) and
          AnsiSameText(TrwQBExpressionItem(AExpr.Items[i]).Value, AIntFldName) and
          AnsiSameText(TrwQBExpressionItem(AExpr.Items[i]).Description, AIntTblName)) or
         ((Length(AIntFldName) = 0) and
          AnsiSameText(TrwQBExpressionItem(AExpr.Items[i]).Description, AIntTblName)) then
      begin
        Result := True;
        break;
      end;
  end;


  function RemoveCondition(ACond: TrwQBFilterCondition): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    if not CheckExpression(ACond.LeftPart) then
    begin
      for i := 0 to ACond.RightPart.Count - 1 do
        if CheckExpression(TrwQBExpression(ACond.RightPart.Items[i])) then
        begin
          ACond.Free;
          Result := True;
          break;
        end
    end

    else
    begin
      ACond.Free;
      Result := True;
    end;
  end;


  function RemoveFromNode(ANode: TrwQBFilterNode): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := ANode.Conditions.Count - 1 downto 0 do
      Result := Result or RemoveCondition(TrwQBFilterCondition(ANode.Conditions.Items[i]));

    for i := ANode.Nodes.Count - 1 downto 0 do
      Result := Result or RemoveFromNode(TrwQBFilterNode(ANode.Nodes.Items[i]));

    if Assigned(ANode.Collection) and (ANode.Conditions.Count = 0) and (ANode.Nodes.Count = 0) then
      ANode.Free;
  end;


begin
  for i := 0 to AQuery.SelectedTables.Count - 1 do
    if RemoveFromNode(TrwQBSelectedTable(AQuery.SelectedTables[i]).Filter) and
       Assigned(TrwQBSelectedTable(AQuery.SelectedTables[i]).VisualComponent) then
      PostMessage(TForm(TrwQBSelectedTable(AQuery.SelectedTables[i]).VisualComponent).Handle,
                  RW_QBTABLE_REFRESH, 0, 0);

  for i := AQuery.Joins.Count - 1 downto 0 do
    if CheckJoin(AQuery.Joins[i]) then
      AQuery.Joins[i].Free;

  for i := AQuery.ShowingFields.Count - 1 downto 0 do
    if CheckExpression(TrwQBShowingField(AQuery.ShowingFields.Items[i])) then
      RemoveShowingField(TrwQBShowingField(AQuery.ShowingFields.Items[i]));
end;


function FindJoin(ALeftTable, ARightTable: TrwQBSelectedTable): TrwQBJoin;
var
  i: Integer;
  Jns: TrwQBJoins;
begin
  Result := nil;
  Jns := TrwQBSelectedTables(ALeftTable.Collection).QBQuery.Joins;

  for i := 0 to Jns.Count - 1 do
    if (Jns[i].LeftTable = ALeftTable) and (Jns[i].RightTable = ARightTable)
        or
       (Jns[i].LeftTable = ARightTable) and (Jns[i].RightTable = ALeftTable) then
    begin
      Result := Jns[i];
      break;
    end;
end;


  {TrwQBSelectedTable}

constructor TrwQBSelectedTable.Create(Collection: TCollection);
begin
  inherited Create(Collection);

  FAlias := '';
  FDestroying := False;
  FVisualComponent := nil;
  FSUBQuery := nil;
  FTableParams := nil;

  FFilter := TrwQBFilterNode.Create(nil);
  FFilter.Table := Self;
  FFilter.NodeType := rntAnd;

  Left := 10;
  Top := 10;
  Width := 205;
  Height := 305;
  Splitter := 20;
  FieldColumnWidth := 150;
end;

destructor TrwQBSelectedTable.Destroy;
begin
  FDestroying := True;

  if Assigned(FTableParams) then
    FTableParams.Free;

  if Assigned(FSUBQuery) then
    FSUBQuery.Free;

  if Assigned(FVisualComponent) then
    FVisualComponent.Free;

  FFilter.Free;

  inherited;
end;

procedure TrwQBSelectedTable.SetFilter(AFilter: TrwQBFilterNode);
begin
  FFilter.Assign(AFilter);
end;

procedure TrwQBSelectedTable.Assign(Source: TPersistent);
begin
  if Assigned(FSUBQuery) then
  begin
    FSUBQuery.Free;
    FSUBQuery := nil;
  end;

  if Assigned(TrwQBSelectedTable(Source).FSUBQuery) then
  begin
    FSUBQuery := TrwQBQuery.Create(Self);
    FSUBQuery.Assign(TrwQBSelectedTable(Source).FSUBQuery);
    FSUBQuery.FParentTable := Self;
  end;

  if Assigned(FTableParams) then
  begin
    FTableParams.Free;
    FTableParams := nil;
  end;

  FTableName := TrwQBSelectedTable(Source).TableName;
  FInternalName := TrwQBSelectedTable(Source).InternalName;
  FAlias := TrwQBSelectedTable(Source).Alias;

  if TrwQBSelectedTable(Source).IsParamsNotEmpty then
    TableParams := TrwQBSelectedTable(Source).TableParams;

  FLeft := TrwQBSelectedTable(Source).Left;
  FTop := TrwQBSelectedTable(Source).Top;
  FWidth := TrwQBSelectedTable(Source).Width;
  FHeight := TrwQBSelectedTable(Source).Height;
  FSplitter := TrwQBSelectedTable(Source).Splitter;
  FFieldColumnWidth := TrwQBSelectedTable(Source).FieldColumnWidth;
  FFilter.Assign(TrwQBSelectedTable(Source).Filter);
  FIsolatedFiltering := TrwQBSelectedTable(Source).IsolatedFiltering;
end;


procedure TrwQBSelectedTable.SetTableName(const Value: string);
begin
  FTableName := Value;
  if AnsiSameText(FTableName, qbEmbeddedQueryName) then
  begin
    if Assigned(FSUBQuery) then
      FSUBQuery.Free;
    FSUBQuery := TrwQBQuery.Create(TrwQBSelectedTables(Collection).QBQuery.Owner);
    FSUBQuery.Description := '';
    FSUBQuery.FParentTable := Self;
  end;
end;


function TrwQBSelectedTable.IsSUBQuery: Boolean;
begin
  Result := Assigned(SUBQuery);
end;


procedure TrwQBSelectedTable.SetSUBQuery(const Value: TrwQBQuery);
begin
  FSUBQuery.Assign(Value);
end;


function TrwQBSelectedTable.lqObject: TrwDataDictTable;
begin
  Result := TrwDataDictTable(DataDictionary.Tables.TableByName(TableName));
end;


procedure TrwQBSelectedTable.SetAlias(const Value: string);
begin
  FAlias := Value;
  if IsSUBQuery and (Length(FSUBQuery.Description) = 0) then
    FSUBQuery.Description := Value;
end;

function TrwQBSelectedTable.IsAliasNotEmpty: Boolean;
begin
  Result := Length(FAlias) > 0;
end;


function TrwQBSelectedTable.IsParamsNotEmpty: Boolean;
begin
  Result := Assigned(FTableParams) and (FTableParams.Count > 0);
end;

function TrwQBSelectedTable.GetTableName(AWizard: Boolean = False; AIgnoreAlias: Boolean = False): string;
var
  Tbl: TrwDataDictTable;
begin
  if AIgnoreAlias or (Length(Alias) = 0) then
  begin
    Tbl := lqObject;
    if Assigned(Tbl) then
      if AWizard then
      begin
        Result := Tbl.DisplayName;
        if not AnsiSameText(lqObject.GroupName, Tbl.DisplayName) then
          Result := Result + ' [' + lqObject.GroupName + ']';
      end

      else
        Result := Tbl.ExternalName
    else
      Result := TableName;
  end
  else
    Result := Alias;
end;


procedure TrwQBSelectedTable.SetLeft(const Value: Integer);
begin
  if Value >= 0 then
    FLeft := Value;
end;

procedure TrwQBSelectedTable.SetTop(const Value: Integer);
begin
  if Value >= 0 then
    FTop := Value;
end;


procedure TrwQBSelectedTable.ContentFromString(AContent: String);
var
  Reader: TrwReader;
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create(Length(AContent));
  MS.WriteBuffer(AContent[1], Length(AContent));
  MS.Position := 0;

//  Clear;
  Reader := TrwReader.Create(MS.RealStream);
  try
    Reader.ReadListBegin;
    while not Reader.EndOfList do
      Reader.ReadProperty(Self);

  finally
    Reader.Free;
  end;
end;


function TrwQBSelectedTable.ContentToString: String;
var
  Writer: TrwWriter;
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create;
  Writer := TrwWriter.Create(MS.RealStream, 1024);
  try
    Writer.WriteListBegin;
    Writer.WriteProperties(Self);
    Writer.WriteListEnd;
  finally
    Writer.Free;
  end;
  SetLength(Result, MS.Size);
  MS.Position := 0;
  MS.ReadBuffer(Result[1], MS.Size);
end;


procedure TrwQBSelectedTable.SetInternalName(const Value: string);
begin
  if not AnsiSameText(FInternalName, Value) then
  begin
    Filter.FixUpTableIntName(FInternalName, Value);
    FInternalName := Value;
  end;  
end;


function TrwQBSelectedTable.GetTableParams: TrwQBExpressions;
begin
  if not Assigned(FTableParams) then
    FTableParams := TrwQBExpressions.Create(TrwQBExpression);

  Result := FTableParams;
end;

procedure TrwQBSelectedTable.SetTableParams(const Value: TrwQBExpressions);
begin
  TableParams.Assign(Value);
end;

function TrwQBSelectedTable.GetParams: string;
begin
end;

procedure TrwQBSelectedTable.SetParams(const Value: string);
var
  h: String;
begin
  if Value = '' then
    Exit;

  with TrwQBExpression(TableParams.Add) do
  begin
    if AnsiSameText(Value, 'Null') then
      AddItem(eitNull, 'Null', 'Null')
    else
    begin
      h := AnsiDequotedStr(AnsiDequotedStr(Value, ''''), '"');
      if Copy(h, 1, 1) = ':' then
        AddItem(eitParam, Copy(h, 2, Length(h) - 1))
      else
        AddItem(eitDateConst, h);
    end;
  end;
end;

{TrwQBSelectedTables}

function TrwQBSelectedTables.Add(ATableName: string): Integer;
var
  T: TCollectionItem;
begin
  T := inherited Add;
  TrwQBSelectedTable(T).TableName := ATableName;
  TrwQBSelectedTable(T).FInternalName := IntToStr(Integer(Pointer(T)));
  Result := T.Index;
end;

function TrwQBSelectedTables.TableByInternalName(AInternalName: string): TrwQBSelectedTable;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if AnsiUpperCase(TrwQBSelectedTable(Items[i]).InternalName) = AInternalName then
    begin
      Result := TrwQBSelectedTable(Items[i]);
      break;
    end;
end;

function TrwQBSelectedTables.FindTable(ATableName: string): TrwQBSelectedTable;
var
  i: Integer;
begin
  Result := nil;
  ATableName := AnsiUpperCase(ATableName);
  for i := 0 to Count - 1 do
    if AnsiUpperCase(TrwQBSelectedTable(Items[i]).TableName) = ATableName then
    begin
      Result := TrwQBSelectedTable(Items[i]);
      break;
    end;
end;

function TrwQBSelectedTables.GetItem(Index: Integer): TrwQBSelectedTable;
begin
  Result := TrwQBSelectedTable(inherited Items[Index]);
end;

procedure TrwQBSelectedTables.SetItem(Index: Integer;
  const Value: TrwQBSelectedTable);
begin
  TrwQBSelectedTable(inherited Items[Index]).Assign(Value);
end;


{TrwQBJoin}

constructor TrwQBJoin.Create(Collection: TCollection);
begin
  inherited Create(Collection);

  FDestroying := False;
  FJoinType := rjtJoin;
end;

destructor TrwQBJoin.Destroy;
begin
  FDestroying := True;
  inherited;
end;

procedure TrwQBJoin.Assign(Source: TPersistent);
begin
  FJoinType := TrwQBJoin(Source).JoinType;
  LeftTableIntName := TrwQBJoin(Source).LeftTableIntName;
  RightTableIntName := TrwQBJoin(Source).RightTableIntName;
  LeftTableFields := TrwQBJoin(Source).LeftTableFields;
  RightTableFields := TrwQBJoin(Source).RightTableFields;
end;

procedure TrwQBJoin.SetTableIntName(Index: Integer; Value: string);
var
  T: TrwQBSelectedTable;
begin
  T := TrwQBJoins(Collection).FQBQuery.SelectedTables.TableByInternalName(Value);
  if (Index = 0) then
  begin
    FLeftTable := T;
    FLeftTableIntName := T.InternalName;
  end
  else
  begin
    FRightTable := T;
    FRightTableIntName := T.InternalName;
  end;
end;


procedure TrwQBJoin.SetRightTableFields(const Value: string);
var
  sF: TrwQBShowingField;
begin
  FRightTableFields := Value;

  if RightTable.IsSUBQuery then
  begin
    sF := RightTable.SUBQuery.ShowingFields.FieldByName(FRightTableFields);
    if Assigned(sF) then
      FRightTableFields := sF.InternalName;
  end;
end;


procedure TrwQBJoin.SetLeftTableFields(const Value: string);
var
  sF: TrwQBShowingField;
begin
  FLeftTableFields := Value;

  if LeftTable.IsSUBQuery then
  begin
    sF := LeftTable.SUBQuery.ShowingFields.FieldByName(FLeftTableFields);
    if Assigned(sF) then
      FLeftTableFields := sF.InternalName;
  end;
end;


function TrwQBJoin.TextForDesign(AWizard: Boolean = False): String;
var
  lf, rf: String;

  function GetJoinPart(ATable: TrwQBSelectedTable; var AFields: String): String;
  var
    sF: TrwQBShowingField;
    h: String;
    rwF: TDataDictField;
    rwFK: TDataDictForeignKey;
  begin
    Result := ATable.GetTableName(AWizard) + '.';
    h := GetNextStrValue(AFields, ',');

    if ATable.IsSUBQuery then
    begin
      sF := ATable.SUBQuery.ShowingFields.FieldByInternalName(h);
      if sF.FieldAlias <> '' then
        h := sF.FieldAlias
      else
        h := sF.GetFieldName(AWizard);
    end

    else
      if AWizard then
      begin
        rwF := ATable.lqObject.Fields.FieldByName(h);
        h := rwF.DisplayName;
        rwFK := ATable.lqObject.ForeignKeys.ForeignKeyByField(rwF);
        if Assigned(rwFK) and not AnsiSameText(TrwDataDictTable(rwFK.Table).GroupName, h) then
          h := h + ' [' + TrwDataDictTable(rwFK.Table).GroupName + ']';
      end;

    Result := Result + h;
  end;


begin
  Result := '';
  lf := LeftTableFields;
  rf := RightTableFields;
  while (lf <> '') and (rf <> '') do
  begin
    if Result <> '' then
      Result := Result + #13;
    Result := Result + GetJoinPart(LeftTable, lf) + '  =  ' +  GetJoinPart(RightTable, rf);
  end;
end;


{TrwQBJoins}

function TrwQBJoins.Add(ALeftTable: TrwQBSelectedTable; ARightTable: TrwQBSelectedTable; AType: TrwQBJoinType; ACondition: TDataDictForeignKey; AConditionField: String = ''): Integer;
var
  J: TCollectionItem;
  i: Integer;
  lTablObj: TrwDataDictTable;
begin
  J := inherited Add;
  TrwQBJoin(J).LeftTableIntName := ALeftTable.InternalName;
  TrwQBJoin(J).RightTableIntName := ARightTable.InternalName;
  TrwQBJoin(J).JoinType := AType;
  Result := J.Index;

  if Assigned(ACondition) then
  begin
    lTablObj := TrwDataDictTable(DataDictionary.Tables.TableByName(ALeftTable.TableName));

    TrwQBJoin(J).LeftTableFields := '';
    TrwQBJoin(J).RightTableFields := '';
    for i := 0 to lTablObj.PrimaryKey.Count - 1 do
    begin
      if (TrwQBJoin(J).LeftTableFields <> '') then
      begin
        TrwQBJoin(J).LeftTableFields := TrwQBJoin(J).LeftTableFields + ',';
        TrwQBJoin(J).RightTableFields := TrwQBJoin(J).RightTableFields + ',';
      end;
      TrwQBJoin(J).LeftTableFields := TrwQBJoin(J).LeftTableFields + lTablObj.PrimaryKey[i].Field.Name;

      if ARightTable.IsSUBQuery then
        TrwQBJoin(J).RightTableFields := TrwQBJoin(J).RightTableFields + AConditionField
      else
        TrwQBJoin(J).RightTableFields := TrwQBJoin(J).RightTableFields + ACondition.Fields[i].Field.Name;
    end;
  end;
end;


function TrwQBJoins.GetItem(Index: Integer): TrwQBJoin;
begin
  Result := TrwQBJoin(inherited Items[Index]);
end;



{TrwQBQuery}

constructor TrwQBQuery.Create(AOwner: TPersistent);
begin
  inherited;
  FSelectedTables := TrwQBSelectedTables.Create(TrwQBSelectedTable);
  FSelectedTables.FQBQuery := Self;
  FJoins := TrwQBJoins.Create(TrwQBJoin);
  FJoins.FQBQuery := Self;
  FShowingFields := TrwQBShowingFields.Create(TrwQBShowingField);
  FShowingFields.FOwner := Self;
  FSortFields := TrwQBSortFields.Create(TrwQBSortField);
  FSortFields.FQBQuery := Self;
  FSQLStatement := rssSelect;
  FParentTable := nil;
  FDescription := 'Main Statement';
  FMacros := '';
  FResultDataSet := nil;
  FUnion := False;
end;

destructor TrwQBQuery.Destroy;
begin
  if Assigned(FResultDataSet) then
    FResultDataSet.Free;
  FShowingFields.Free;
  FJoins.Free;
  FSelectedTables.Free;
  FSortFields.Free;
  inherited;
end;

procedure TrwQBQuery.Assign(Source: TPersistent);
begin
  Clear;
  FSelectedTables.Assign(TrwQBQuery(Source).SelectedTables);
  FJoins.Assign(TrwQBQuery(Source).Joins);
  FShowingFields.Assign(TrwQBQuery(Source).ShowingFields);
  FSortFields.Assign(TrwQBQuery(Source).SortFields);
  DistinctResult := TrwQBQuery(Source).DistinctResult;
  SQLStatement := TrwQBQuery(Source).SQLStatement;
  FTmpTableName := TrwQBQuery(Source).TmpTableName;
  FDescription := TrwQBQuery(Source).Description;
  FMacros := TrwQBQuery(Source).Macros;
  FUnion := TrwQBQuery(Source).Union;
  if not Assigned(Owner) then
    Owner := TrwQBQuery(Source).Owner;

  if Assigned(FOnAssign) then FOnAssign(Self);
end;

procedure TrwQBQuery.Clear;
begin
  FSortFields.Clear;
  FShowingFields.Clear;
  FJoins.Clear;
  FSelectedTables.Clear;
end;

procedure TrwQBQuery.SetSelectedTables(Value: TrwQBSelectedTables);
begin
  FSelectedTables.Assign(Value);
end;

procedure TrwQBQuery.SetJoins(Value: TrwQBJoins);
begin
  FJoins.Assign(Value);
end;

procedure TrwQBQuery.SetShowingFields(Value: TrwQBShowingFields);
begin
  FShowingFields.Assign(Value);
end;

procedure TrwQBQuery.SetSortFields(Value: TrwQBSortFields);
begin
  FSortFields.Assign(Value);
end;

function TrwQBQuery.SQL(RootQuery: Boolean = False; Indent: String = ''): string;
var
  h, h1, h2, h3, hw: string;
  i, j: Integer;
  LFld, RFld: String;
  sGrp: string;
  fl_Grp: Boolean;
  sInd: String;
  Q: TrwQBQuery;
  lMacros: array [1..5] of String;
  OuterTables: TList;


  function MakeJoinPart(ATable: TrwQBSelectedTable; AField: string): string;
  var
    sF: TrwQBShowingField;
  begin
    if ATable.Alias <> '' then
      Result := ATable.Alias
    else
      Result := ATable.TableName;

    if ATable.IsSUBQuery then
    begin
      sF := ATable.SUBQuery.ShowingFields.FieldByInternalName(AField);

      if sF.FieldAlias <> '' then
        AField := sF.FieldAlias
      else
        AField := sF.GetFieldName(False);
    end;

    Result := Result + '.' + AField;
  end;


  procedure MakeSort(var AText: String);
  var
    i: Integer;
  begin
    if (SortFields.Count > 0) and not Assigned(ParentTable) and (SQLStatement = rssSelect) then
    begin
      AText := AText + #13#10 + sInd + 'ORDER BY' + #13#10 + sInd + '   ';
      for i := 0 to SortFields.Count - 1 do
        if Assigned(TrwQBSortField(SortFields.Items[i]).Field) then
        begin
          AText := AText + IntToStr(TrwQBSortField(SortFields.Items[i]).Field.Index + 1);
          if TrwQBSortField(SortFields.Items[i]).SortDirection = rsdDESC then
            AText := AText + ' DESC';
          if (i < SortFields.Count - 1) then
            AText := AText + ', ';
        end;
    end;
  end;


  function MakeUnion: String;
  var
    i: Integer;
  begin
    Result := '';

    if SQLStatement = rssInsert then
    begin
      Result := 'INSERT INTO ' + TmpTableName+' (' + #13#10;
      for i := 0 to ShowingFields.Count - 1 do
      begin
        Result := Result + '   ' + TrwQBShowingField(ShowingFields.Items[i]).TmpFieldName;
        if (i < ShowingFields.Count - 1) then
          Result := Result + ',';
        Result := Result + #13#10 + sInd;
      end;

      if lMacros[5] <> '' then
        Result := Result + '   ' + lMacros[5];
      Result := Result +')' +#13#10 + sInd;
    end;

    for i := 0 to SelectedTables.Count - 1 do
    begin
      if i > 0 then
      begin
        Result := Result + #13#10 + sInd + 'UNION';
        if not DistinctResult then
          Result := Result + ' ALL';
        Result := Result + #13#10#13#10;
      end;

      Result := Result + sInd + SelectedTables.Items[i].SUBQuery.SQL;
    end;

    MakeSort(Result);

    if lMacros[4] <> '' then
      Result := Result + #13#10 + sInd + '   ' + lMacros[4];

    Result := Result + #13#10;
  end;


  function OuterFilteredTable(const ATable: TrwQBSelectedTable): String;
  var
    i, j: Integer;
    Flds: TStringList;
    Expr: TrwQBExpression;
    OldsInd, h, h1: String;

    procedure AddFld(const AFld: String);
    var
      h: String;
    begin
      if ATable.IsSUBQuery then
        h := ATable.SUBQuery.ShowingFields.FieldByInternalName(AFld).GetFieldName(False, False)
      else
        h := AFld;

      if Flds.IndexOf(h) = -1 then
        Flds.Add(h);
    end;

  begin
    OldsInd := sInd;
    sInd := sInd + '   ';

    Flds := TStringList.Create;

    //searching for fields involved into this query
    for i := 0 to ShowingFields.Count - 1 do
    begin
      Expr := ShowingFields[i];
      for j := 0 to Expr.Count - 1 do
        if (Expr[j].ItemType = eitField) and (ATable.InternalName = Expr[j].Description) then
          AddFld(Expr[j].Value);
    end;

    for i := 0 to Joins.Count - 1 do
    begin
      if Joins[i].FRightTable = ATable then
        h := Joins[i].FRightTableFields
      else if Joins[i].FLeftTable = ATable then
        h := Joins[i].FLeftTableFields
      else
        Continue;

       while h <> '' do
       begin
         h1 := GetNextStrValue(h, ',');
         AddFld(h1);
       end;
    end;

    h := '';
    if ATable.Alias <> '' then
      h1 := ATable.Alias
    else
      h1 := '';
      
    for i := 0 to Flds.Count - 1 do
    begin
      if i > 0 then
        h := h + ', ';
      h := h + #13#10 + sInd + '   ';
      if h1 <> '' then
        h := h + h1 + '.';
      h := h + Flds[i];
    end;

    Result := 'SELECT' + h;

    if ATable.IsSUBQuery then
      h := '(' + ATable.SUBQuery.SQL(False, sInd + '   ') + sInd + '   )'
    else
    begin
      h := ATable.TableName;
      if ATable.IsParamsNotEmpty then
      begin
        h := h + '(';
        for j := 0 to ATable.TableParams.Count - 1 do
        begin
          if j > 0 then
            h := h + ', ';
          h := h + TrwQBExpression(ATable.TableParams.Items[j]).TextForQuery;
        end;
        h := h + ')';
      end;
    end;

    Result := Result + #13#10 + sInd + ' FROM' + #13#10 +
                sInd + '   ' + h + ' ' + h1 + #13#10 +
                sInd + ' WHERE' + #13#10 +
                sInd + '   ' + ATable.Filter.TextForQuery(ATable);

    FreeAndNil(Flds);

    sInd := OldsInd;
  end;


  procedure FindOuterTables;
  var
    i: Integer;
    Tbl: TrwQBSelectedTable;

    function OtherTablesInvolved2(const AExpression: TrwQBExpression): Boolean;
    var
      i: Integer;
    begin
      Result := False;

      for i := 0 to AExpression.Count - 1 do
        if (AExpression[i].ItemType = eitField) and (AExpression[i].Description <> Tbl.InternalName) then
        begin
          Result := True;
          break;
        end;
    end;

    function OtherTablesInvolved(const ANode: TrwQBFilterNode): Boolean;
    var
      i, j: Integer;
    begin
      Result := False;

      for i := 0 to ANode.Conditions.Count - 1 do
      begin
        Result := OtherTablesInvolved2(TrwQBFilterCondition(ANode.Conditions.Items[i]).LeftPart);
        if Result then
          break;

        for j := 0 to TrwQBFilterCondition(ANode.Conditions.Items[i]).RightPart.Count - 1 do
        begin
          Result := OtherTablesInvolved2(TrwQBExpression(TrwQBFilterCondition(ANode.Conditions.Items[i]).RightPart.Items[j]));
          if Result then
            break;
        end;

        if Result then
          break;
      end;

      if Result then
        Exit;

      for i := 0 to ANode.Nodes.Count - 1 do
      begin
        Result := OtherTablesInvolved(TrwQBFilterNode(ANode.Nodes.Items[i]));
        if Result then
          break;
      end;
    end;

  begin
    for i := 0 to Joins.Count - 1 do
    begin
      if Joins[i].JoinType = rjtLeftJoin then
        Tbl := Joins[i].FRightTable
      else if Joins[i].JoinType = rjtRightJoin then
        Tbl := Joins[i].FLeftTable
      else
        Tbl := nil;

      if not Assigned(Tbl) or not Tbl.IsolatedFiltering or
        (Tbl.Filter.Conditions.Count = 0) and (Tbl.Filter.Nodes.Count = 0) or
        (OuterTables.IndexOf(Tbl) <> -1) or OtherTablesInvolved(Tbl.Filter) then
        Continue;

      OuterTables.Add(Tbl);
    end;
  end;

begin
  if SelectedTables.Count = 0 then
  begin
    Result := '';
    Exit;
  end;

  if Indent = '' then
  begin
    sInd := '';
    if not RootQuery then
    begin
      Q := Self;
      while Assigned(Q) and Assigned(Q.ParentTable) do
      begin
        if not Q.ParentTable.SUBQuery.IsUnionItem then
          sInd := sInd + '   ';
        Q := TrwQBSelectedTables(Q.ParentTable.Collection).FQBQuery;
      end;
    end;
  end
  else
    sInd := Indent;

  for i := Low(lMacros) to High(lMacros) do
    lMacros[i] := '';
  h := Macros;
  i := 1;
  while Length(h) > 0 do
  begin
    lMacros[i] := GetNextStrValue(h, ',');
    Inc(i);
  end;

  if Union then
  begin
    Result := MakeUnion;
    Exit;
  end;

  if SQLStatement in [rssSelect, rssInsert] then
    if DistinctResult then
      h := 'SELECT DISTINCT' + #13#10 + sInd
    else
      h := 'SELECT' + #13#10 + sInd
  else
    h := '';

  if SQLStatement = rssInsert then
    h3 := 'INSERT INTO ' + TmpTableName+' (' + #13#10
  else if SQLStatement = rssUpdate then
    h3 := 'UPDATE ' + TmpTableName
  else if SQLStatement = rssDelete then
    h3 := 'DELETE ';

  sGrp := '';
  fl_Grp := False;
  for i := 0 to ShowingFields.Count - 1 do
  begin
    h := h + '   ' + TrwQBShowingField(ShowingFields.Items[i]).TextForQuery;

    if not TrwQBShowingField(ShowingFields.Items[i]).ThereAreAggrFncts then
    begin
      if sGrp <> '' then
        sGrp := sGrp + ', ';
      sGrp := sGrp + IntToStr(ShowingFields.Items[i].Index + 1);
    end
    else
      fl_Grp := True;

    if (SQLStatement = rssSelect) and (TrwQBShowingField(ShowingFields.Items[i]).FieldAlias <> '') then
      h := h + '  ' + TrwQBShowingField(ShowingFields.Items[i]).FieldAlias;

    if SQLStatement = rssInsert then
      h3 := h3 + '   ' + TrwQBShowingField(ShowingFields.Items[i]).TmpFieldName
    else  if SQLStatement = rssUpdate then
    ;//UPDATE

    if (i < ShowingFields.Count - 1) then
    begin
      h := h + ',';
      h3 := h3 + ',';
    end;
    h := h + #13#10 + sInd;
    h3 := h3 + #13#10 + sInd;
  end;

  if SQLStatement = rssInsert then
  begin
    if lMacros[5] <> '' then
      h3 := h3 + '   ' + lMacros[5];
    h3 := h3 +')' +#13#10 + sInd;
  end
  else
    h3 := h3 + #13#10 + sInd;


  OuterTables := TList.Create;
  try
    if SQLStatement in [rssSelect, rssInsert, rssDelete] then
    begin
      if lMacros[1] <> '' then
        h := h + #13#10 + sInd + '   ' + lMacros[1];

      h := h + #13#10 + sInd + 'FROM';
      h2 := '';

      FindOuterTables;

      for i := 0 to SelectedTables.Count - 1 do
      begin
        if OuterTables.IndexOf(SelectedTables[i]) <> -1 then
          // Outer table with filter
          h1 :='   (' + OuterFilteredTable(SelectedTables[i]) + ')'

        else if SelectedTables.Items[i].IsSUBQuery then
          // SUBQuery
          if SelectedTables.Items[i].SUBQuery.IsConditionedSQL  then
            Continue
          else
            h1 := '   (' +SelectedTables.Items[i].SUBQuery.SQL + ')'

        else
        begin
          // Simple table
          h1 := '   ' + SelectedTables.Items[i].TableName;
          if SelectedTables.Items[i].IsParamsNotEmpty then
          begin
            h1 := h1 + '(';
            for j := 0 to SelectedTables.Items[i].TableParams.Count - 1 do
            begin
              if j > 0 then
                h1 := h1 + ', ';
              h1 := h1 + TrwQBExpression(SelectedTables.Items[i].TableParams.Items[j]).TextForQuery;
            end;
            h1 := h1 + ')';
          end;
        end;

        if SelectedTables.Items[i].Alias <> '' then
          h1 := h1 + '  ' + SelectedTables.Items[i].Alias;

        if h2 <> '' then
          h2 := h2 + ',';

        h2 := h2 + #13#10 + sInd + h1;
      end;
    end;

    if lMacros[2] <> '' then
      h2 := h2 + #13#10 + sInd + '   ' + lMacros[2];

    h := h + h2;

    //Joins
    hw := '';
    for i := 0 to Joins.Count - 1 do
    begin
      LFld := TrwQBJoin(Joins.Items[i]).LeftTableFields;
      RFld := TrwQBJoin(Joins.Items[i]).RightTableFields;
      hw := hw + '   ';

      while Length(LFld) > 0 do
      begin
        h1 := MakeJoinPart(TrwQBJoin(Joins.Items[i]).LeftTable, GetNextStrValue(LFld, ','));
        h2 := MakeJoinPart(TrwQBJoin(Joins.Items[i]).RightTable, GetNextStrValue(RFld, ','));

        if TrwQBJoin(Joins.Items[i]).JoinType = rjtLeftJoin then
          hw := hw + h1 + ' =+ ' + h2
        else if TrwQBJoin(Joins.Items[i]).JoinType = rjtRightJoin then
          hw := hw + h2 + ' =+ ' + h1
        else
          hw := hw + h1 + ' = ' + h2;

        if Length(LFld) > 0 then
          hw := hw + '  AND  ';
      end;

      if i < Joins.Count - 1 then
        hw := hw + '  AND';
      hw := hw + #13#10 + sInd;
    end;

    for i := 0 to SelectedTables.Count - 1 do
      if OuterTables.IndexOf(SelectedTables[i]) = -1 then
      begin
        h1 := SelectedTables[i].Filter.TextForQuery(SelectedTables[i]);
        if h1 <> '' then
        begin
          if hw <> '' then
            hw := hw + '  AND' + #13#10 + sInd;
          hw := hw + '  ' + h1;
        end;
      end;

    if hw <> '' then
      h := h + #13#10 + sInd + 'WHERE' + #13#10 + sInd + hw + #13#10 + sInd;

    if lMacros[3] <> '' then
      h := h + #13#10 + sInd + '   ' + lMacros[3];

    if fl_Grp and (sGrp <> '') then
      h := h + #13#10 + sInd + 'GROUP BY' + #13#10 + sInd + '   ' + sGrp + #13#10 + sInd;

    MakeSort(h);

    if lMacros[4] <> '' then
      h := h + #13#10 + sInd + '   ' + lMacros[4];

    if SQLStatement <> rssSelect then
      h := h3 + #13#10 + sInd + h;

    Result := h + #13#10;

  finally
    FreeAndNil(OuterTables);
  end;
end;

function TrwQBQuery.IsNotEmptyJoins: Boolean;
begin
  if (Owner is TrwComponent) and  TrwComponent(Owner).IsLibComponent then
    Result := True
  else
    Result := Joins.Count > 0;
end;

function TrwQBQuery.IsNotEmptySelectedTables: Boolean;
begin
  if (Owner is TrwComponent) and  TrwComponent(Owner).IsLibComponent then
    Result := True
  else
    Result := SelectedTables.Count > 0;
end;

function TrwQBQuery.IsNotEmptyShowingFields: Boolean;
begin
  if (Owner is TrwComponent) and  TrwComponent(Owner).IsLibComponent then
    Result := True
  else
    Result := ShowingFields.Count > 0;
end;

function TrwQBQuery.IsNotEmptySortFields: Boolean;
begin
  if (Owner is TrwComponent) and TrwComponent(Owner).IsLibComponent then
    Result := True
  else
    Result := SortFields.Count > 0;
end;


function TrwQBQuery.IsConditionedSQL: Boolean;

  function CheckNode(ANode: TrwQBFilterNode): Boolean;
  var
    i: Integer;
    C: TrwQBFilterCondition;
  begin
    Result := False;
    for i := 0 to ANode.Conditions.Count - 1 do
    begin
      C := TrwQBFilterCondition(ANode.Conditions.Items[i]);
      if (AnsiSameText(C.Operation, 'EXISTS') or AnsiSameText(C.Operation, 'IN')) and
         AnsiSameText(C.OperationAux, FParentTable.InternalName) then
      begin
        Result := True;
        break;
      end;
    end;
  end;


  function CheckTables: Boolean;
  var
    i: Integer;
  begin
    Result := False;
    if Assigned(FParentTable) then
      for i := 0 to TrwQBSelectedTables(FParentTable.Collection).Count - 1 do
      begin
        Result := CheckNode(TrwQBSelectedTable(TrwQBSelectedTables(FParentTable.Collection).Items[i]).Filter);
        if Result then
          break;
      end;
  end;

begin
  Result := CheckTables;
end;


function TrwQBQuery.IsUnionItem: Boolean;
begin
  if Assigned(FParentTable) then
    Result := FParentTable.IsSUBQuery and TrwQBSelectedTables(FParentTable.Collection).QBQuery.Union
  else
    Result := False;
end;


function TrwQBQuery.ResultDataset: TComponent;
begin
  if not Assigned(FResultDataSet) then
    FResultDataSet := TrwQuery.Create(nil);
  Result := FResultDataSet;
end;


function TrwQBQuery.IsTmpTableNameIsNotEmpty: Boolean;
begin
  Result := Length(FTmpTableName) > 0;
end;


function TrwQBQuery.IsDescriptionIsNotEmpty: Boolean;
begin
  Result := Length(FDescription) > 0;
end;


procedure TrwQBQuery.SaveToStream(AStream: TStream);
var
  Writer: TrwWriter;
begin
  Writer := TrwWriter.Create(AStream, 1024);
  try
    Writer.WriteListBegin;
    Writer.WriteProperties(Self);
    Writer.WriteListEnd;
  finally
    Writer.Free;
  end;
end;


procedure TrwQBQuery.ReadFromStream(AStream: TStream);
var
  Reader: TrwReader;
begin
  Clear;
  Reader := TrwReader.Create(AStream);
  try
    Reader.ReadListBegin;
    while not Reader.EndOfList do
      Reader.ReadProperty(Self);

  finally
    Reader.Free;
  end;

  if Assigned(FOnAssign) then FOnAssign(Self);
end;


function TrwQBQuery.IsMacrosNotEmpty: Boolean;
begin
  Result := Length(FMacros) > 0;
end;


function TrwQBQuery.PSQLText: Variant;
begin
  Result := SQL;
end;


function TrwQBQuery.IsEmpty: Boolean;
begin
  Result := SelectedTables.Count = 0;
end;

function TrwQBQuery.GetFieldList: TCommaDilimitedString;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to ShowingFields.Count - 1 do
  begin
    if i > 0 then
      Result := Result + ',';
    Result := Result + ShowingFields[i].GetFieldName(False, False);
  end;
end;

function TrwQBQuery.GetQBFieldByID(const AFieldID: String): IrwQBShowingField;
begin
  Result := ShowingFields.FieldByInternalName(AFieldID);
end;

function TrwQBQuery.GetQBFieldByName(const AFieldName: String): IrwQBShowingField;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to ShowingFields.Count - 1 do
    if AnsiSameText(ShowingFields[i].GetFieldName(False, False), AFieldName) then
    begin
      Result := ShowingFields[i];
      break;
    end;
end;


{TrwQBFilterNode}

constructor TrwQBFilterNode.Create(Collection: TCollection);
begin
  inherited Create(Collection);

  FConditions := TrwQBFilterNodeCollection.Create(TrwQBFilterCondition);
  FNodes := TrwQBFilterNodeCollection.Create(TrwQBFilterNode);
  FNotOper := False;
  FNodeType := rntAnd;
  if Assigned(Collection) then
    Table := TrwQBFilterNodeCollection(Collection).FTable;
end;

destructor TrwQBFilterNode.Destroy;
begin
  FConditions.Free;
  FNodes.Free;
  inherited;
end;

procedure TrwQBFilterNode.SetConditions(AItems: TCollection);
begin
  FConditions.Assign(AItems)
end;

procedure TrwQBFilterNode.SetNodes(AItems: TCollection);
begin
  FNodes.Assign(AItems)
end;

procedure TrwQBFilterNode.Assign(Source: TPersistent);
begin
  FNodeType := TrwQBFilterNode(Source).NodeType;
  Conditions := TrwQBFilterNode(Source).Conditions;
  Nodes := TrwQBFilterNode(Source).Nodes;
  FNotOper := TrwQBFilterNode(Source).NotOper;
end;

function TrwQBFilterNode.TextForDesign: string;
begin
  if NodeType = rntAnd then
    Result := 'AND'
  else
    if NodeType = rntOr then
    Result := 'OR';
end;

function TrwQBFilterNode.TextForQuery(ATable: TrwQBSelectedTable): string;
var
  op, h, h1: string;
  AAlias: string;
  i: Integer;
begin
  if ATable.Alias <> '' then
    AAlias := ATable.Alias
  else
    AAlias := ATable.TableName;

  if NodeType = rntAnd then
    op := 'AND'
  else if NodeType = rntOr then
    op := 'OR';

  h := '';
  for i := 0 to FConditions.Count - 1 do
  begin
    h1 := TrwQBFilterCondition(FConditions.Items[i]).TextForQuery(ATable);
    if h <> '' then
      h := h + ' ' + op + ' ';
    h := h + h1;
  end;

  for i := 0 to FNodes.Count - 1 do
  begin
    h1 := '(' + TrwQBFilterNode(FNodes.Items[i]).TextForQuery(ATable) + ')';
    if h <> '' then
      h := h + ' ' + op + ' ';
    h := h + h1;
  end;

  if NotOper or (NodeType = rntOr) and (h <> '') then
    h := '(' + h + ')';

  if NotOper then
    h := 'NOT ' + h;

  Result := h;
end;


procedure TrwQBFilterNode.SetTable(const Value: TrwQBSelectedTable);
begin
  FTable := Value;
  TrwQBFilterNodeCollection(FConditions).FTable := FTable;
  TrwQBFilterNodeCollection(FNodes).FTable := FTable;
end;



function TrwQBFilterNode.IsNodesNotEmpty: Boolean;
begin
  Result := Nodes.Count > 0;
end;


procedure TrwQBFilterNode.FixUpTableIntName(const AOldName,  ANewName: String);
var
  i: Integer;
begin
  for i := 0 to Conditions.Count - 1 do
    TrwQBFilterCondition(Conditions.Items[i]).FixUpTableIntName(AOldName, ANewName);

  for i := 0 to Nodes.Count - 1 do
    TrwQBFilterNode(Nodes.Items[i]).FixUpTableIntName(AOldName, ANewName);
end;


{TrwQBFilterCondition}

function TrwQBFilterCondition.TextForDesign(AWizard: Boolean = False): string;
var
  T: TrwQBSelectedTable;
  h: String;
begin
  Result := '';

  if AnsiSameText(Operation, 'EXISTS') then
  begin
    T := TrwQBSelectedTables(TrwQBFilterNodeCollection(Collection).FTable.Collection).TableByInternalName(OperationAux);
    h := '';
    if Assigned(T) then
      if T.Alias <> '' then
        h := T.Alias
      else
        h := T.TableName;

    Result := 'Result of SubQuery ' + h + ' EXISTS';
  end

  else
  begin
    Result := LeftPart.TextForDesign(AWizard);

    if AnsiSameText(Operation, 'LIKE') and (OperationAux <> '') then
    begin
      if OperationAux[1] = 'B' then
        Result := Result + ' STARTED '
      else if OperationAux[1] = 'A' then
        Result := Result + ' ANYWHERE ';
      if OperationAux[2] = '0' then
        Result := Result + 'LIKE'
      else
        Result := Result + 'Like';
    end

    else
      Result := Result + ' ' + Operation;

    if AnsiSameText(Operation, 'BETWEEN') then
      Result := Result + ' ' + TrwQBExpression(RightPart.Items[0]).TextForDesign(AWizard) + ' AND ' + TrwQBExpression(RightPart.Items[1]).TextForDesign(AWizard)

    else if AnsiSameText(Operation, 'IN') then
      Result := Result + ' (list)'

    else if AnsiSameText(Operation, 'IS NULL') then

    else
      Result := Result + ' ' + TrwQBExpression(RightPart.Items[0]).TextForDesign(AWizard);
  end;
end;


function TrwQBFilterCondition.TextForQuery(ATable: TrwQBSelectedTable): string;
var
  AAlias: string;
  h1: String;
  i: Integer;
begin
  if ATable.Alias <> '' then
    AAlias := ATable.Alias
  else
    AAlias := ATable.TableName;

  Result := '';


  if AnsiSameText(Operation, 'EXISTS') then
    Result := 'EXISTS (' + #13#10 + '   ' +
        TrwQBSelectedTables(ATable.Collection).TableByInternalName(OperationAux).FSUBQuery.SQL + ' )'

  else
  begin
    Result := LeftPart.TextForQuery;

    if AnsiSameText(Operation, 'LIKE') and (OperationAux <> '') then
    begin
      if OperationAux[2] = '0' then
        Result := 'UPPER(' + Result + ')';
      Result := Result + ' LIKE ';
      h1 := TrwQBExpression(RightPart.Items[0]).TextForQuery;
      if (TrwQBExpression(RightPart.Items[0]).Count = 1) and (TrwQBExpression(RightPart.Items[0]).ExprType = rwvString) then
      begin
        if OperationAux[1] = 'B' then
          h1 := Copy(h1, 1, Length(h1) - 1) + '%' + ''''
        else if OperationAux[1] = 'A' then
          h1 := '''' + '%' + Copy(h1, 2, Length(h1) - 2) + '%' + ''''
      end
      else
      begin
        if OperationAux[1] = 'B' then
          h1 := h1 + ' || ''%'''
        else if OperationAux[1] = 'A' then
          h1 := '''%''  || ' + h1 + ' || ''%''';
      end;

      if OperationAux[2] = '0' then
       h1 := 'UPPER(' + h1 + ')';

      Result := Result + h1;
    end

    else
    begin
      Result := Result + ' ' + Operation;

      if AnsiSameText(Operation, 'BETWEEN') then
        Result := Result + ' ' + TrwQBExpression(RightPart.Items[0]).TextForQuery + ' AND ' +
                  TrwQBExpression(RightPart.Items[1]).TextForQuery

      else if AnsiSameText(Operation, 'IN') then
      begin
        h1 := '';
        Result := Result + ' (';
        for i := 0 to RightPart.Count - 1 do
        begin
          if Length(h1) > 0 then
            h1 := h1 + ', ';
          h1 := h1 + TrwQBExpression(RightPart.Items[i]).TextForQuery;
        end;
        Result := Result + h1 + ')';
      end

      else if AnsiSameText(Operation, 'IS NULL') then

      else
        Result := Result + ' ' + TrwQBExpression(RightPart.Items[0]).TextForQuery;
    end;
  end;

  if NotOper then
    Result := 'NOT (' + Result + ')';
end;


procedure TrwQBFilterCondition.Assign(Source: TPersistent);
begin
  FNotOper := TrwQBFilterCondition(Source).NotOper;
  FOperation := TrwQBFilterCondition(Source).Operation;
  FOperationAux := TrwQBFilterCondition(Source).OperationAux;
  LeftPart := TrwQBFilterCondition(Source).LeftPart;
  RightPart := TrwQBFilterCondition(Source).RightPart;
end;


destructor TrwQBFilterCondition.Destroy;
begin
  FLeftPart.Free;
  FRightPart.Free;

  inherited;
end;


procedure TrwQBFilterCondition.SetLeftPart(const Value: TrwQBExpression);
begin
  FLeftPart.Assign(Value);
end;

procedure TrwQBFilterCondition.SetRightPart(const Value: TrwQBExpressions);
begin
  FRightPart.Assign(Value);
end;


constructor TrwQBFilterCondition.Create(Collection: TCollection);
begin
  inherited;

  FNotOper := False;

  FLeftPart := TrwQBExpression.Create(nil);
  FLeftPart.FOwner := Self;
  FRightPart := TrwQBExpressions.Create(TrwQBExpression);
  FRightPart.FOwner := Self;
end;


function TrwQBFilterCondition.IsNotEmptyRightPart: Boolean;
begin
  Result := FRightPart.Count > 0;
end;


function TrwQBFilterCondition.IsNotEmptyOperationAux: Boolean;
begin
  Result := Length(FOperationAux) > 0;
end;


function TrwQBFilterCondition.IsNotEmptyLeftPart: Boolean;
begin
  Result := FLeftPart.Count > 0;
end;

procedure TrwQBFilterCondition.FixUpTableIntName(const AOldName, ANewName: String);
var
  i: Integer;
begin
  LeftPart.FixUpTableIntName(AOldName, ANewName);

  for i := 0 to RightPart.Count - 1 do
    TrwQBExpression(RightPart.Items[i]).FixUpTableIntName(AOldName, ANewName);
end;


{TrwQBShowingField}

procedure TrwQBShowingField.Assign(Source: TPersistent);
begin
  inherited;
  FInternalName := TrwQBShowingField(Source).InternalName;
  FieldAlias := TrwQBShowingField(Source).FieldAlias;
  FTmpFieldName := TrwQBShowingField(Source).TmpFieldName;
end;


function TrwQBShowingField.Attribute: TrwDataDictField;

  procedure GetField(AField: TrwQBShowingField);
  var
    F: TrwQBShowingField;
    Tbl: TrwQBSelectedTable;
  begin
    if Count <> 1 then
      exit;

    Tbl := AField.GetSelectedTables.TableByInternalName(TrwQBExpressionItem(AField.Items[0]).Description);

    if not Assigned(Tbl) then
      exit;

    if Tbl.IsSUBQuery then
    begin
      F := Tbl.SUBQuery.ShowingFields.FieldByInternalName(TrwQBExpressionItem(AField.Items[0]).Value);
      GetField(F);
    end
    else if Tbl.lqObject <> nil then
      Result := TrwDataDictField(Tbl.lqObject.Fields.FieldByName(TrwQBExpressionItem(AField.Items[0]).Value));
  end;

begin
  Result := nil;
  GetField(Self);
end;


function TrwQBShowingField.GetDataType: TrwVarTypeKind;
begin
  Result := ExprType;
end;

function TrwQBShowingField.GetFieldName(AWizard: Boolean = False; AIgnoreAlias: Boolean = True): string;
var
  Attr: TrwDataDictField;
  Tbl: TrwQBSelectedTable;
  Fld: TrwQBShowingField;
  rwFK: TDataDictForeignKey;
begin
  if not AIgnoreAlias and (FieldAlias <> '') then
  begin
    Result := FieldAlias;
    Exit;
  end;

  if (Count = 1) and (TrwQBExpressionItem(Items[0]).ItemType = eitField) then
  begin
    Tbl := GetSelectedTables.TableByInternalName(TrwQBExpressionItem(Items[0]).Description);
    if Tbl.IsSUBQuery then
    begin
      Fld := Tbl.SUBQuery.ShowingFields.FieldByInternalName(TrwQBExpressionItem(Items[0]).Value);
      if not Assigned(Fld) then
        Result := 'UNKNOWN FIELD'
      else if Fld.FieldAlias <> '' then
        Result := Fld.FieldAlias
      else
        Result := Fld.GetFieldName(AWizard);
    end

    else
    begin
      Attr := Attribute;
      if Assigned(Attr) then
        if AWizard and (Attr.DisplayName <> '') then
        begin
          Result := Attr.DisplayName;
          rwFK := Attr.Table.ForeignKeys.ForeignKeyByField(Attr);
          if Assigned(rwFK) and not AnsiSameText(TrwDataDictTable(rwFK.Table).GroupName, Result) then
            Result := Result + ' [' + TrwDataDictTable(rwFK.Table).GroupName + ']';
        end

        else
          Result := Attr.Name
      else
        Result := 'UNKNOWN FIELD';
    end
  end

  else
    Result := FieldAlias;
end;

function TrwQBShowingField.GetID: String;
begin
  Result := InternalName;
end;

function TrwQBShowingField.GetIndex: Integer;
begin
  Result := Index;
end;

function TrwQBShowingField.GetName: String;
begin
  Result := GetFieldName(False, False);
end;

function TrwQBShowingField.GetSelectedTables: TrwQBSelectedTables;
begin
  Result := TrwQBQuery(TrwQBShowingFields(Collection).FOwner).SelectedTables;
end;

function TrwQBShowingField.IsFieldAliasNotEmpty: Boolean;
begin
  Result := Length(FieldAlias) > 0;
end;

function TrwQBShowingField.IsTmpFieldNameNotEmpty: Boolean;
begin
  REsult := Length(TmpFieldName) > 0;
end;

function TrwQBShowingField.PName: Variant;
begin
  Result := GetFieldName(False, False);
end;

function TrwQBShowingField.VCLObject: TObject;
begin
  Result := Self;
end;


{TrwQBShowingFields}

function TrwQBShowingFields.Add: Integer;
var
  T: TCollectionItem;
begin
  T := inherited Add;
  TrwQBShowingField(T).InternalName := IntToStr(Integer(Pointer(T)));
  Result := T.Index;
end;


function TrwQBShowingFields.FieldByInternalName(const AInternalName: string): TrwQBShowingField;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if AnsiUpperCase(TrwQBShowingField(Items[i]).InternalName) = AInternalName then
    begin
      Result := TrwQBShowingField(Items[i]);
      break;
    end;
end;


function TrwQBShowingFields.FieldByName(const AName: string): TrwQBShowingField;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if (TrwQBShowingField(Items[i]).FieldAlias <> '') and AnsiSameText(TrwQBShowingField(Items[i]).FieldAlias, AName)
       or
       (TrwQBShowingField(Items[i]).Count = 1) and
       (TrwQBExpressionItem(TrwQBShowingField(Items[i]).Items[0]).ItemType = eitField) and
       AnsiSameText(TrwQBExpressionItem(TrwQBShowingField(Items[i]).Items[0]).Value, AName) then
    begin
      Result := TrwQBShowingField(Items[i]);
      break;
    end;
end;

function TrwQBShowingFields.FindField(const AName: string): TrwQBShowingField;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if SameText(TrwQBShowingField(Items[i]).GetFieldName(False, False), AName) then
    begin
      Result := TrwQBShowingField(Items[i]);
      break;
    end;
end;

function TrwQBShowingFields.GetField(Index: Integer): TrwQBShowingField;
begin
  Result := TrwQBShowingField(inherited Items[Index]);
end;


function TrwQBShowingFields.PCount: Variant;
begin
  Result := Count;
end;

procedure TrwQBShowingFields.PDeleteWhatNotInList(AFieldList: Variant);
var
  L: TStringList;
  i: Integer;
begin
  if VarArrayHighBound(AFieldList, 1) - VarArrayLowBound(AFieldList, 1) + 1 = 0 then
    Exit;

  L := TStringList.Create;
  try
    L.Capacity := VarArrayHighBound(AFieldList, 1) - VarArrayLowBound(AFieldList, 1) + 1;
    for i := VarArrayLowBound(AFieldList, 1) to VarArrayHighBound(AFieldList, 1) do
      L.Add(UpperCase(AFieldList[i]));
    L.Sorted := True;

    for i := Count - 1 downto 0 do
      if L.IndexOf(UpperCase(Items[i].GetFieldName(False, False))) = -1 then
        Delete(i);

  finally
    FreeAndNil(L);
  end;
end;

function TrwQBShowingFields.PFieldByName(AFieldName: Variant): Variant;
var
  F: IrwQBShowingField;
begin
  F := TrwQBQuery(Owner).GetQBFieldByName(AFieldName);
  if Assigned(F) then
    Result := Integer(Pointer(F.VCLObject))
  else
    Result := 0;
end;

function TrwQBShowingFields.PItems(AIndex: Variant): Variant;
begin
  Result := Integer(Pointer(Items[AIndex]));
end;

procedure TrwQBShowingFields.SetField(Index: Integer; Value: TrwQBShowingField);
begin
  Items[Index].Assign(Value);
end;



  {TrwQBSortField}

procedure TrwQBSortField.Assign(Source: TPersistent);
begin
  FieldIntName := TrwQBSortField(Source).FieldIntName;
  FSortDirection := TrwQBSortField(Source).SortDirection;
end;

procedure TrwQBSortField.SetFieldIntName(Value: string);
begin
  FField := TrwQBSortFields(Collection).FQBQuery.ShowingFields.FieldByInternalName(Value);
  if Assigned(FField) then
    FFieldIntName := FField.InternalName
  else
    FFieldIntName := '';
end;


procedure TrwQBSortFields.Assign(Source: TPersistent);
var
  i: Integer;
begin
  inherited Assign(Source);

  for i := Count-1 downto 0 do
  begin
    if not Assigned(TrwQBSortField(Items[i]).Field) then
      TrwQBSortField(Items[i]).Free;
  end;
end;


{ TrwQBExpressionItem }

procedure TrwQBExpressionItem.Assign(ASource: TPersistent);
begin
  FItemType := TrwQBExpressionItem(ASource).ItemType;
  FValue := TrwQBExpressionItem(ASource).Value;
  FDescription := TrwQBExpressionItem(ASource).Description;
end;


function TrwQBExpressionItem.GetDDFieldInfo: TrwDataDictField;
var
  Tbl: TrwQBSelectedTable;
  SF: TrwQBShowingField;
  fn: String;
begin
  Result := nil;

  fn := Value;
  Tbl := GetSelectedTable;
  while Tbl.IsSUBQuery do
  begin
    SF := Tbl.SUBQuery.ShowingFields.FieldByInternalName(fn);
    if (SF.Count = 1) and (SF[0].ItemType = eitField) then
    begin
      Tbl := SF[0].GetSelectedTable;
      fn := SF[0].Value;
    end
    else
      break;
  end;

  if not Tbl.IsSUBQuery then
    Result := TrwDataDictField(Tbl.lqObject.Fields.FieldByName(fn));
end;

function TrwQBExpressionItem.GetOwnr: TrwQBExpression;
begin
  Result := TrwQBExpression(TrwQBOwnedCollection(Collection).Owner);
end;


function TrwQBExpressionItem.GetSelectedTable: TrwQBSelectedTable;
begin
  Result := TrwQBExpression(Owner).GetSelectedTables.TableByInternalName(Description);
end;


function TrwQBExpressionItem.IsDescrNotEmpty: Boolean;
begin
  Result := Length(Description) > 0;
end;


procedure TrwQBExpressionItem.SetValue(const Value: Variant);
begin
  if (VarType(Value) = varInteger) and (Value < 0) then
    varCast(FValue, Value, varDouble)
  else
    FValue := Value;  
end;

{ TrwQBExpression }

function TrwQBExpression.AddItem(AType: TrwQBExprItemType; AValue: Variant; ADescr: String = ''): TrwQBExpressionItem;
begin
  Result := TrwQBExpressionItem(FContent.Add);
  Result.FItemType := AType;
  Result.FValue := AValue;
  Result.Description := ADescr;
  FExprType := rwvUnknown;
end;


procedure TrwQBExpression.Assign(Source: TPersistent);
begin
  FContent.Assign(TrwQBExpression(Source).Content);
  FExprType := TrwQBExpression(Source).FExprType;
end;


//CHECKING ERRORS

function TrwQBExpression.CheckErrors(var ErrItem: Integer): String;
var
  lCurrItem: TrwQBExpressionItem;
  lPos: Integer;
  lExprType: TrwVarTypeKind;
  FEOFItem: TrwQBExpressionItem;

  procedure Error(AErrNbr: Integer; APar: string = '');
  begin
    case AErrNbr of
      1: Result := 'Syntax error';
      2: Result := 'Table is not found';
      3: Result := 'Field is not found';
      4: Result := 'Function is not found';
      5: Result := 'Expected "' + APar + '"';
      6: Result := 'Types mismatch';
      7: Result := 'Expected Integer type';
      8: Result := 'Expected Float type';
      9: Result := 'Expected String type';
      10: Result := 'Expected Date type';
    end;

    raise ErwException.Create(Result);
  end;


  procedure GetNextToken;
  begin
    if lPos = Count - 1 then
      lCurrItem := FEOFItem
    else
    begin
      Inc(lPos);
      lCurrItem := Items[lPos];
    end;
  end;


  procedure CheckNextToken(AItemType: TrwQBExprItemType; AValue: Variant);
  begin
    GetNextToken;
    if (lCurrItem.ItemType <> AItemType) or (lCurrItem.Value <> AValue) then
      Error(5, AValue);
  end;

  procedure CheckType(const AType: TrwVarTypeKind; const ATypes: array of TrwVarTypeKind);
  var
    i: Integer;
  begin
    if AType = rwvVariant then
      Exit;

    for i := Low(ATypes) to High(ATypes) do
      if AType = ATypes[i] then
        Exit;

    case ATypes[Low(ATypes)] of
      rwvInteger:  Error(7);
      rwvString:   Error(8);
      rwvFloat:    Error(9);
      rwvDate:     Error(10);
    end;
  end;

  function DefineMathOperType(const AType1,  AType2: TrwVarTypeKind): TrwVarTypeKind;
  begin
    Result := AType1;

    if AType1 = rwvVariant then
      Result := AType2

    else if AType1 <> AType2 then
      if AType2 in [rwvFloat, rwvCurrency, rwvDate] then
        Result := rwvFloat;
  end;


  procedure ExpressionLevel2(var AType: TrwVarTypeKind); forward;
  procedure ExpressionLevel3(var AType: TrwVarTypeKind); forward;
  procedure ExpressionLevel4(var AType: TrwVarTypeKind); forward;
  procedure ExpressionLevel5(var AType: TrwVarTypeKind); forward;
  procedure ExpressionLevel6(var AType: TrwVarTypeKind); forward;


  (*      <element>  is Additon and Subtraction for two terms
                 { <term>{+|-}<term> }
  *)
  procedure ExpressionLevel1(var AType: TrwVarTypeKind);
  var
    lOper: TrwQBExpressionItem;
    t: TrwVarTypeKind;
  begin
    ExpressionLevel2(AType);

    lOper := lCurrItem;
    t := AType;

    while (lOper.ItemType = eitOperation) and (String(lOper.Value)[1] in ['+', '-']) do
    begin
      GetNextToken;
      ExpressionLevel2(AType);

      if lOper.Value = '+' then
      begin
        if t in [rwvFloat, rwvInteger, rwvCurrency, rwvDate, rwvString, rwvVariant] then
        begin
          if t = rwvString then
          begin
            CheckType(AType, [rwvString]);
            lOper.Description := ' || ';
          end

          else if (AType = rwvString) and (AType = rwvVariant) then
            t := rwvString

          else
          begin
            CheckType(AType, [t, rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
            t := DefineMathOperType(t, AType);
          end;
        end
        else
          Error(6);
      end

      else
      begin
        CheckType(t, [rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
        CheckType(AType, [t, rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
        t := DefineMathOperType(t, AType);
      end;

      lOper := lCurrItem;
    end;

    AType := t;
  end;


  procedure ExpressionLevel2(var AType: TrwVarTypeKind);
  var
    lOper: TrwQBExpressionItem;
    t: TrwVarTypeKind;
  begin
    ExpressionLevel3(AType);

    lOper := lCurrItem;
    t := AType;

    while (lOper.ItemType = eitOperation) and (String(lOper.Value)[1] in ['*', '/']) do
    begin
      GetNextToken;
      ExpressionLevel3(AType);

      if lOper.Value = '*' then
      begin
        CheckType(t, [rwvFloat, rwvInteger, rwvCurrency]);
        CheckType(AType, [t, rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
        t := DefineMathOperType(t, AType);
      end

      else
      begin
        CheckType(t, [rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
        CheckType(AType, [t, rwvFloat, rwvInteger, rwvCurrency]);
        t := rwvFloat;
      end;

      lOper := lCurrItem;
    end;

    AType := t;
  end;


  (*             Unary - or +
                 { {+|-} <factor> }
  *)
  procedure ExpressionLevel3(var AType: TrwVarTypeKind);
  var
    f: Boolean;
  begin
    if (lCurrItem.ItemType = eitOperation) and (String(lCurrItem.Value)[1] in ['-', '+']) then
    begin
      f := True;
      GetNextToken;
    end
    else
      f := False;

    ExpressionLevel4(AType);

    if f then
      CheckType(AType, [rwvFloat, rwvInteger, rwvCurrency]);
  end;


  (*          Expression in round brackets
                  { ( <expression> ) }
  *)
  procedure ExpressionLevel4(var AType: TrwVarTypeKind);
  begin
    if (lCurrItem.ItemType = eitBracket) and (lCurrItem.Value = '(') then
    begin
      GetNextToken;
      ExpressionLevel1(AType);
      if not((lCurrItem.ItemType = eitBracket) and (lCurrItem.Value = ')')) then
        Error(5, ')');
      GetNextToken;
    end
    else
      ExpressionLevel5(AType);
  end;


  (*          Expression in square brackets (array)
                  { [ <expression>, <expression>... ] }
  *)
  procedure ExpressionLevel5(var AType: TrwVarTypeKind);
  begin
    if (lCurrItem.ItemType = eitBracket) and (lCurrItem.Value = '[') then
    begin
      GetNextToken;
      while not((lCurrItem.ItemType = eitBracket) and (lCurrItem.Value = ']')) do
      begin
        ExpressionLevel1(AType);

        if not((lCurrItem.ItemType = eitSeparator) and (lCurrItem.Value = ',')) then
          break;

        GetNextToken;
      end;

      if not((lCurrItem.ItemType = eitBracket) and (lCurrItem.Value = ']')) then
        Error(5, ']');
      AType := rwvArray;

      GetNextToken;
    end
    else
      ExpressionLevel6(AType);
  end;



  (*                <factor>
         { <identificator>|<Number>|<String> }
  *)
  procedure ExpressionLevel6(var AType: TrwVarTypeKind);
  var
    Tbl: TrwQBSelectedTable;
    Fld: TrwQBShowingField;
    Fnct: TrwFunction;
    lType: TrwVarTypeKind;
    i: Integer;
    rwF: TDataDictField;
  begin
    case lCurrItem.ItemType of
      eitStrConst:   AType := rwvString;
      eitIntConst:   AType := rwvInteger;
      eitFloatConst: AType := rwvFloat;
      eitDateConst:  AType := rwvDate;
      eitParam:      AType := rwvVariant;
      eitNull:       AType := rwvVariant;
      eitTrue:       AType := rwvBoolean;
      eitFalse:      AType := rwvBoolean;

      eitConst:
        begin
          with SQLConsts.ConstantByName(lCurrItem.Value) do
            case ConstType of
              sctString:   AType := rwvString;
              sctInteger:  AType := rwvInteger;
              sctDateTime: AType := rwvDate;
              sctFloat:    AType := rwvFloat;
            end;
        end;

      eitField:
        begin
          Tbl := GetSelectedTables.TableByInternalName(lCurrItem.Description);
          if not Assigned(Tbl) then
            Error(2);
          if Tbl.IsSUBQuery then
          begin
            Fld := Tbl.SUBQuery.ShowingFields.FieldByInternalName(lCurrItem.Value);
            if not Assigned(Fld) then
              Error(3);
            AType := Fld.ExprType;
          end

          else
          begin
            rwF := Tbl.lqObject.Fields.FieldByName(lCurrItem.Value);
            if not Assigned(rwF) then
              Error(3);
            AType := DDTypeToRWType(rwF.FieldType);
          end;


        end;


      eitFunction:
        begin
          Fnct := QBFunctions.FindFunction(lCurrItem.Value);
          if not Assigned(Fnct) then
            Error(4);

          if Fnct.Params.Count = 0 then
            GetNextToken;

          AType := rwvUnknown;
          for i := 0 to Fnct.Params.Count - 1 do
          begin
            GetNextToken;
            ExpressionLevel1(lType);
            if not CheckTypeCompatibility(Fnct.Params[i].Descriptor.VarType, lType) then
              Error(6);

            if i = 0 then
              if (Fnct.Name = 'MIN') or (Fnct.Name = 'MAX') or (Fnct.Name = 'SUM') then  //an exception for aggrigate functions
                AType := lType;

            if i < Fnct.Params.Count - 1 then
              if not((lCurrItem.ItemType = eitSeparator) and (lCurrItem.Value = ',')) then
                Error(5, ',');
          end;

          if not((lCurrItem.ItemType = eitBracket) and (lCurrItem.Value = ')')) then
            Error(5, ')');

          if AType = rwvUnknown then
            AType := Fnct.Params.Result.VarType;
        end;

    else
      Error(1);
    end;

    GetNextToken;
  end;

begin
  Result := '';
  ErrItem := -1;
  FEOFItem := TrwQBExpressionItem.Create(nil);
  try
    FEOFItem.ItemType := eitNone;
    FEOFItem.Value := '';
    FExprType := rwvUnknown;
    lPos := -1;
    GetNextToken;

    try
      ExpressionLevel1(lExprType);
      if lCurrItem.ItemType <> eitNone then
        Error(1);
      FExprType := lExprType
    except
      if lCurrItem.ItemType = eitNone then
        ErrItem := Count -1
      else
        ErrItem := lPos;
    end;
  finally
    FEOFItem.Free;
  end;
end;


function TrwQBExpression.CheckTypeCompatibility(AType1, AType2: TrwVarTypeKind; AIgnoreArrays: Boolean = False): Boolean;
begin
  if (AType1 = rwvVariant) or (AType2 = rwvVariant) or (AIgnoreArrays and (AType2 = rwvArray)) then
  begin
    Result := True;
    Exit;
  end;

  case AType1 of
    rwvInteger:  Result := AType2 in [rwvInteger, rwvFloat, rwvCurrency];

    rwvString:   Result := AType2 in [AType1, rwvBoolean];

    rwvBoolean:  Result := AType2 in [rwvBoolean];

    rwvDate:     Result := AType2 in [rwvDate, rwvFloat, rwvCurrency, rwvInteger];

    rwvFloat,
    rwvCurrency: Result := AType2 in [rwvFloat, rwvCurrency, rwvInteger, rwvDate];
  else
    Result := False;
  end;
end;

procedure TrwQBExpression.Clear;
begin
  FContent.Clear;
  FExprType := rwvUnknown; 
end;

procedure TrwQBExpression.ContentFromString(AContent: String);
var
  Reader: TrwReader;
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create(Length(AContent));
  MS.WriteBuffer(AContent[1], Length(AContent));
  MS.Position := 0;

  Clear;
  Reader := TrwReader.Create(MS.RealStream);
  try
    Reader.ReadListBegin;
    while not Reader.EndOfList do
      Reader.ReadProperty(Self);

  finally
    Reader.Free;
  end;
end;


function TrwQBExpression.ContentToString: String;
var
  Writer: TrwWriter;
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create;
  Writer := TrwWriter.Create(MS.RealStream, 1024);
  try
    Writer.WriteListBegin;
    Writer.WriteProperties(Self);
    Writer.WriteListEnd;
  finally
    Writer.Free;
  end;
  SetLength(Result, MS.Size);
  MS.Position := 0;
  MS.ReadBuffer(Result[1], MS.Size);
end;

function TrwQBExpression.Count: Integer;
begin
  Result := Content.Count;
end;

constructor TrwQBExpression.Create(Collection: TCollection);
begin
  inherited;

  FContent := TrwQBOwnedCollection.Create(TrwQBExpressionItem);
  FContent.FOwner := Self;
  FExprType := rwvUnknown;
end;

destructor TrwQBExpression.Destroy;
begin
  Clear;
  FContent.Free;
  inherited;
end;


procedure TrwQBExpression.FixUpTableIntName(const AOldName, ANewName: String);
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    if (Items[i].ItemType = eitField) and
       AnsiSameStr(AOldName, Items[i].Description) then
      Items[i].Description := ANewName;
end;

function TrwQBExpression.GetExprType: TrwVarTypeKind;
var
  n: Integer;
begin
  if FExprType = rwvUnknown then
    CheckErrors(n);

  Result := FExprType;
end;

function TrwQBExpression.GetItem(Index: Integer): TrwQBExpressionItem;
begin
  Result := TrwQBExpressionItem(FContent.Items[Index]);
end;

function TrwQBExpression.GetSelectedTables: TrwQBSelectedTables;
begin
  if FOwner is TrwQBFilterCondition then
    Result := TrwQBSelectedTables(TrwQBFilterNodeCollection(TrwQBFilterCondition(FOwner).Collection).FTable.Collection)
  else
    Result := TrwQBSelectedTables(TrwQBFilterNodeCollection(TrwQBFilterCondition(TrwQBExpressions(Collection).FOwner).Collection).FTable.Collection);
end;


procedure TrwQBExpression.SetContent(const Value: TrwQBOwnedCollection);
begin
  FContent.Assign(Value);
end;


function TrwQBExpression.TextForDesign(AWizard: Boolean = False): string;
var
  i: Integer;
  h: String;
  Tbls: TrwQBSelectedTables;
  Tbl: TrwQBSelectedTable;
  Fld: TrwQBShowingField;
  C: TsbCustomSQLConstant;
begin
  Result := '';

  i := 0;
  while i < Count do
  begin
    if TrwQBExpressionItem(Items[i]).IsDescrNotEmpty and
      not (TrwQBExpressionItem(Items[i]).ItemType in [eitField, eitOperation]) then
    begin
      case TrwQBExpressionItem(Items[i]).ItemType of
        eitConst:
          begin
            C := SQLConsts.ConstantByName(TrwQBExpressionItem(Items[i]).Value);
            if Assigned(C) then
              h := C.DisplayName + ' (' + StringReplace(C.StrValue, #13, ', ', [rfReplaceAll]) + ')'
            else
              h := '@'+VarAsType(TrwQBExpressionItem(Items[i]).Value, varString);
          end;
      else
        h := TrwQBExpressionItem(Items[i]).Description;
      end;
    end

    else
      case TrwQBExpressionItem(Items[i]).ItemType of
        eitBracket,
        eitSeparator,
        eitOperation,
        eitIntConst,
        eitFloatConst:    h := VarAsType(TrwQBExpressionItem(Items[i]).Value, varString);

        eitField:         begin
                            Tbls := GetSelectedTables;
                            Tbl := Tbls.TableByInternalName(TrwQBExpressionItem(Items[i]).Description);

                            if Assigned(Tbl) then
                            begin
                              if Tbl.Alias <> '' then
                                h := Tbl.Alias
                              else
                                if AWizard and not Tbl.IsSUBQuery then
                                  h := Tbl.lqObject.DisplayName
                                else
                                  h := Tbl.TableName;
                              h := h + '.';

                              if Tbl.IsSUBQuery then
                              begin
                                Fld := Tbl.SUBQuery.ShowingFields.FieldByInternalName(TrwQBExpressionItem(Items[i]).Value);
                                if Fld.FieldAlias <> '' then
                                  h := h + Fld.FieldAlias
                                else
                                  h := h + Fld.GetFieldName(AWizard);
                              end
                              else
                              begin
                                if AWizard then
                                  h := h + Tbl.lqObject.Fields.FieldByName(TrwQBExpressionItem(Items[i]).Value).DisplayName
                                else
                                  h := h + TrwQBExpressionItem(Items[i]).Value;
                              end
                            end

                            else
                              h :=  '''UNKNOWN FIELD''';
                          end;

        eitParam:         h := ':'+VarAsType(TrwQBExpressionItem(Items[i]).Value, varString);

        eitConst:         h := '@'+VarAsType(TrwQBExpressionItem(Items[i]).Value, varString);

        eitStrConst,
        eitDateConst:     h := ''''+VarAsType(TrwQBExpressionItem(Items[i]).Value, varString) + '''';

        eitFunction:      h := VarAsType(TrwQBExpressionItem(Items[i]).Value, varString) + '(';
      else
        h := TrwQBExpressionItem(Items[i]).Description;
      end;

    Result := Result + h;

    Inc(i);
  end;
end;


function TrwQBExpression.TextForQuery: string;
var
  lCurrItem: TrwQBExpressionItem;
  lPos: Integer;
  FEOFItem: TrwQBExpressionItem;

  procedure _ExpressionLevel2(var Res: String); forward;
  procedure _ExpressionLevel3(var Res: String); forward;
  procedure _ExpressionLevel4(var Res: String); forward;
  procedure _ExpressionLevel5(var Res: String); forward;
  procedure _ExpressionLevel6(var Res: String); forward;

  procedure _Err;
  begin
    raise ErwException.Create('An error occured duering generating SQL statement');
  end;

  procedure _GetNextToken;
  begin
    if lPos = Count - 1 then
      lCurrItem := FEOFItem
    else
    begin
      Inc(lPos);
      lCurrItem := Items[lPos];
    end;
  end;

  procedure _CheckNextToken(AItemType: TrwQBExprItemType; AValue: Variant);
  begin
    _GetNextToken;
    if (lCurrItem.ItemType <> AItemType) or (lCurrItem.Value <> AValue) then
      _Err;
  end;



  (*      <element>  is Additon and Subtraction for two terms
                 { <term>{+|-}<term> }
  *)
  procedure _ExpressionLevel1(var Res: String);
  var
    lOper: TrwQBExpressionItem;
  begin
    _ExpressionLevel2(Res);
    lOper := lCurrItem;
    while (lOper.ItemType = eitOperation) and (String(lOper.Value)[1] in ['+', '-']) do
    begin
      if Length(lOper.Description) = 0 then
        Res := Res + lOper.Value
      else
        Res := Res + lOper.Description;
      _GetNextToken;
      _ExpressionLevel2(Res);
      lOper := lCurrItem;
    end;
  end;


  procedure _ExpressionLevel2(var Res: String);
  var
    lOper: TrwQBExpressionItem;
  begin
    _ExpressionLevel3(Res);
    lOper := lCurrItem;
    while (lOper.ItemType = eitOperation) and (String(lOper.Value)[1] in ['*', '/']) do
    begin
      Res := Res + lOper.Value;
      _GetNextToken;
      _ExpressionLevel3(Res);
      lOper := lCurrItem;
    end;
  end;


  (*             Unary - or +
                 { {+|-} <factor> }
  *)
  procedure _ExpressionLevel3(var Res: String);
  begin
    if (lCurrItem.ItemType = eitOperation) and (String(lCurrItem.Value)[1] in ['-', '+']) then
    begin
      Res := Res + lCurrItem.Value;
      _GetNextToken;
    end;
    _ExpressionLevel4(Res);
  end;


  (*          Expression in round brackets
                  { ( <expression> ) }
  *)
  procedure _ExpressionLevel4(var Res: String);
  begin
    if (lCurrItem.ItemType = eitBracket) and (lCurrItem.Value = '(') then
    begin
      Res := Res + '(';
      _GetNextToken;
      _ExpressionLevel1(Res);
      if not((lCurrItem.ItemType = eitBracket) and (lCurrItem.Value = ')')) then
        _Err;
      Res := Res + ')';
      _GetNextToken;
    end
    else
      _ExpressionLevel5(Res);
  end;


  (*          Expression in square brackets (array)
                  { [ <expression>, <expression>... ] }
  *)
  procedure _ExpressionLevel5(var Res: String);
  begin
    if (lCurrItem.ItemType = eitBracket) and (lCurrItem.Value = '[') then
    begin
      Res := Res + '[';
      _GetNextToken;
      while not((lCurrItem.ItemType = eitBracket) and (lCurrItem.Value = ']')) do
      begin
        _ExpressionLevel1(Res);

        if not((lCurrItem.ItemType = eitSeparator) and (lCurrItem.Value = ',')) then
          break;

        Res := Res + ', ';
        _GetNextToken;
      end;

      if not((lCurrItem.ItemType = eitBracket) and (lCurrItem.Value = ']')) then
        _Err;
      Res := Res + ']';

      _GetNextToken;
    end
    else
      _ExpressionLevel6(Res);
  end;



  (*                <factor>
         { <identificator>|<Number>|<String> }
  *)
  procedure _ExpressionLevel6(var Res: String);
  var
    Fnct: TrwFunction;
    i: Integer;
    Tbls: TrwQBSelectedTables;
    Tbl: TrwQBSelectedTable;
    Fld: TrwQBShowingField;
    Params: array [1..5] of string;
    h: String;

    function ConvNull(Expr: String; Par: String; SubstPar: String): String;
    begin
      if AnsiSameText(Par, '''Y''') then
        Result := 'Subst(' + Expr + ', Null, ' + SubstPar + ')'
      else
        Result := Expr;
    end;

    procedure Functs;
    var
      h1: String;
      i: Integer;
    begin
      h1 := Fnct.Name;
      if AnsiSameText(h1, 'SUM') or AnsiSameText(h1, 'MIN') or
         AnsiSameText(h1, 'MAX') or AnsiSameText(h1, 'AVG') or
         AnsiSameText(h1, 'COUNTVAL') then
      begin
        if AnsiSameText(h1, 'COUNTVAL') then
          Res := Res + 'COUNT('
        else
          Res := Res + h1 + '(';
        if AnsiSameText(Params[2], '''Y''') then
          Res := Res + 'DISTINCT ';
        Res := Res + Params[1];
      end

      else if AnsiSameText(h1, 'COUNT') then
      begin
        Res := Res + 'COUNT(';
        if AnsiSameText(Params[1], 'Y') then
          Res := Res + 'DISTINCT ';
        Res := Res + '*';
      end

      else if AnsiSameText(h1, 'AsInteger') then
        Res := Res + 'CAST(' + ConvNull(Params[1], Params[2], '0') + ' AS INTEGER'

      else if AnsiSameText(h1, 'AsFloat') then
        Res := Res + 'CAST(' + ConvNull(Params[1], Params[2], '0') + ' AS FLOAT'

      else if AnsiSameText(h1, 'AsDate') then
        Res := Res + 'CAST(' + ConvNull(Params[1], Params[2], '0') + ' AS DATE'

      else if AnsiSameText(h1, 'AsString') then
        Res := Res + 'CAST(' + ConvNull(Params[1], Params[2], '''''') + ' AS VARCHAR'

      else if AnsiSameText(h1, 'AsCurrency') then
        Res := Res + 'CAST(' + ConvNull(Params[1], Params[2], '0') + ' AS CURRENCY'

      else if AnsiSameText(h1, 'AsChar') then
        Res := Res + 'CAST(' + ConvNull(Params[1], Params[3], '0') + ' AS CHAR(' + Params[2] + ')'

      else
      begin
        Res := Res + h1 +'(';
        for i := 1 to Fnct.Params.Count do
        begin
          Res := Res + Params[i];
          if i < Fnct.Params.Count then
            Res := Res + ', ';
        end;
      end;
    end;

  begin
    case lCurrItem.ItemType of
      eitStrConst,
      eitDateConst:
        begin
          if AnsiSameText(lCurrItem.Value, 'NULL') then
            Res := Res + VarAsType(lCurrItem.Value, varString)
          else
            Res := Res + '''' + VarAsType(lCurrItem.Value, varString) + '''';
        end;

      eitParam:      Res := Res + ':' + VarAsType(lCurrItem.Value, varString);

      eitConst:      Res := Res + '@' + VarAsType(lCurrItem.Value, varString);

      eitNull,
      eitTrue,
      eitFalse,
      eitIntConst,
      eitFloatConst: Res := Res + VarAsType(lCurrItem.Value, varString);

      eitField:
        begin
          Tbls := GetSelectedTables;
          Tbl := Tbls.TableByInternalName(lCurrItem.Description);
          if Assigned(Tbl) then
          begin
            if Tbl.Alias <> '' then
              Res := Res + Tbl.Alias
            else
              Res := Res + Tbl.TableName;
            Res := Res + '.';

            if Tbl.IsSUBQuery then
            begin
              Fld := Tbl.SUBQuery.ShowingFields.FieldByInternalName(lCurrItem.Value);
              if Fld.FieldAlias <> '' then
                Res := Res + Fld.FieldAlias
              else
                Res := Res + Fld.GetFieldName(False);
            end
            else
              Res := Res + lCurrItem.Value;
          end
          else
            Res := '''UNKNOWN FIELD''';
        end;


      eitFunction:
        begin
          Fnct := QBFunctions.FindFunction(lCurrItem.Value);
          if not Assigned(Fnct) then
            _Err;

          for i := 1 to Fnct.Params.Count do
          begin
            _GetNextToken;
            h := '';
            _ExpressionLevel1(h);
            Params[i] := h;
            if i < Fnct.Params.Count - 1 then
              if not((lCurrItem.ItemType = eitSeparator) and (lCurrItem.Value = ',')) then
                _Err;
          end;

          if not((lCurrItem.ItemType = eitBracket) and (lCurrItem.Value = ')')) then
            _Err;

          Functs;

          Res := Res + ')';
        end;

    else
      _Err;
    end;

    _GetNextToken;
  end;

begin
  Result := '';
  FEOFItem := TrwQBExpressionItem.Create(nil);
  try
    FEOFItem.ItemType := eitNone;
    FEOFItem.Value := '';
    FExprType := rwvUnknown;
    lPos := -1;
    _GetNextToken;

    _ExpressionLevel1(Result);
    if lCurrItem.ItemType <> eitNone then
      _Err;
  finally
    FEOFItem.Free;
  end;
end;



function TrwQBExpression.ThereAreAggrFncts: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to Count - 1 do
    if (TrwQBExpressionItem(Items[i]).ItemType = eitFunction) and
       (AnsiSameText(TrwQBExpressionItem(Items[i]).Value, 'SUM') or
        AnsiSameText(TrwQBExpressionItem(Items[i]).Value, 'COUNT') or
        AnsiSameText(TrwQBExpressionItem(Items[i]).Value, 'COUNTVAL') or
        AnsiSameText(TrwQBExpressionItem(Items[i]).Value, 'MIN') or
        AnsiSameText(TrwQBExpressionItem(Items[i]).Value, 'MAX') or
        AnsiSameText(TrwQBExpressionItem(Items[i]).Value, 'AVG')) then
    begin
      Result := True;
      break;
    end;
end;



{ TrwQBFunctions }

procedure TrwQBFunctions.AddFunction(const AHeaderText, AGroup, ADescription: String;
  const ADefaultParams: String = '');
var
  F: TrwQBFunction;
  P, Pb, Pe: Integer;
begin
  F := TrwQBFunction(AddLibItem('', AGroup, ADescription));
  F.FHeaderText := AHeaderText;
  F.FDefaultParams := ADefaultParams;
  F.Name := ParseFunctionHeader(AHeaderText, F.Params, Pb, Pe, P);
end;

procedure TrwQBFunctions.Execute(const APar1, APar2: string);
begin
end;

procedure TrwQBFunctions.Initialize;
begin
  AddFunction('function AVG(AValue: Float; ADistinct: Boolean): Float', 'Aggregate Functions',
                'Aggregate function.', #0 + IntToStr(Ord(eitFalse)));
  AddFunction('function COUNT(ADistinct: Boolean): Integer', 'Aggregate Functions',
                'Aggregate function.', IntToStr(Ord(eitFalse)));
  AddFunction('function COUNTVAL(AValue: Variant; ADistinct: Boolean): Integer', 'Aggregate Functions',
                'Aggregate function.', #0+IntToStr(Ord(eitFalse)));
  AddFunction('function MAX(AValue: Float; ADistinct: Boolean): Float', 'Aggregate Functions',
                'Aggregate function.', #0 + IntToStr(Ord(eitFalse)));
  AddFunction('function MIN(AValue: Float; ADistinct: Boolean): Float', 'Aggregate Functions',
                'Aggregate function.', #0 + IntToStr(Ord(eitFalse)));
  AddFunction('function SUM(AValue: Float; ADistinct: Boolean): Float', 'Aggregate Functions',
                'Aggregate function.', #0 + IntToStr(Ord(eitFalse)));

  AddFunction('function AsChar(AValue: Variant; ALength: Integer; AConvertNull: Boolean): String', 'Type Conversion',
                'Converts AValue to string value with the certain length.',
                 #0 + IntToStr(Ord(eitIntConst)) + #1 + '32' + #0 + IntToStr(Ord(eitTrue)));
  AddFunction('function AsCurrency(AValue: Variant; AConvertNull: Boolean): Currency', 'Type Conversion',
                'Converts AValue to currency value.', #0 + IntToStr(Ord(eitTrue)));
  AddFunction('function AsDate(AValue: Variant; AConvertNull: Boolean): Date', 'Type Conversion',
                'Converts AValue to date value.', #0 + IntToStr(Ord(eitTrue)));
  AddFunction('function AsFloat(AValue: Variant; AConvertNull: Boolean): Float', 'Type Conversion',
                'Converts AValue to float value.', #0 + IntToStr(Ord(eitTrue)));
  AddFunction('function AsInteger(AValue: Variant; AConvertNull: Boolean): Integer', 'Type Conversion',
                'Converts AValue to integer value.', #0 + IntToStr(Ord(eitTrue)));
  AddFunction('function AsString(AValue: Variant; AConvertNull: Boolean): String', 'Type Conversion',
                'Converts AValue to string value.', #0 + IntToStr(Ord(eitTrue)));

  AddFunction('function Lower(AText: String): String', 'String Routines',
                'Returns a copy of a string in lowercase.');
  AddFunction('function SubStr(AText: String; AFirstChar: Integer; ALength: Integer): String', 'String Routines',
                'Returns a substring of a string');
  AddFunction('function Trim(AText: String): String', 'String Routines',
                'Trims leading and trailing spaces from a string');
  AddFunction('function TrimLeft(AText: String): String', 'String Routines',
                'Trims leading spaces from a string');
  AddFunction('function TrimRight(AText: String): String', 'String Routines',
                'Trims trailing spaces from a string');
  AddFunction('function Upper(AText: String): String', 'String Routines',
                'Returns a copy of a string in uppercase');
  AddFunction('function StrLength(AText: String): Integer', 'String Routines',
                'Returns length of a string');
  AddFunction('function StrBegin(AText: String; ALength: Integer): String', 'String Routines',
                'Returns the beginning part of a string');
  AddFunction('function StrEnd(AText: String; ALength: Integer): String', 'String Routines',
                'Returns the ending part of a string');
  AddFunction('function SubStrPos(AText: String; ASubStr: String): Integer', 'String Routines',
                'Returns a position of substring within of the AText');

  AddFunction('function Day(ADate: Date): Integer', 'Date Routines',
                'Returns a number of day');
  AddFunction('function Month(ADate: Date): Integer', 'Date Routines',
                'Returns a number of month');
  AddFunction('function Year(ADate: Date): Integer', 'Date Routines',
                'Returns a year');
  AddFunction('function BeginOfYear(ADate: Date): Date', 'Date Routines',
                'Returns first date of the year for the ADate');
  AddFunction('function EndOfYear(ADate: Date): Date', 'Date Routines',
                'Returns last date of the year for the ADate');
  AddFunction('function BeginOfMonth(ADate: Date): Date', 'Date Routines',
                'Returns first date of the month for the ADate');
  AddFunction('function EndOfMonth(ADate: Date): Date', 'Date Routines',
                'Returns last date of the month for the ADate');
  AddFunction('function BeginOfQuarter(ADate: Date): Date', 'Date Routines',
                'Returns first date of the quarter for the ADate');
  AddFunction('function EndOfQuarter(ADate: Date): Date', 'Date Routines',
                'Returns last date of the quarter for the ADate');
  AddFunction('function Quarter(ADate: Date): Integer', 'Date Routines',
                'Returns number of the quarter of ADate');
  AddFunction('function QuarterBeginDate(AQuarter: Integer; AYear: Integer): Date', 'Date Routines',
                'Returns first date of the quarter');
  AddFunction('function QuarterEndDate(AQuarter: Integer; AYear: Integer): Date', 'Date Routines',
                'Returns last date of the quarter');

  AddFunction('function Abs(AValue: Float): Float', 'Math Routines',
                'Returns an absolute value.');
  AddFunction('function Round(AValue: Float; APrecision: Integer): Float', 'Math Routines',
                'Returns rounded value. Function randomly rounds number 5 up or down. It makes a total more precise.', #0 +
                IntToStr(Ord(eitIntConst)) + #1 + '0');
  AddFunction('function RoundFin(AValue: Float; APrecision: Integer): Float', 'Math Routines',
                'Returns rounded value. Always rounds number 5 up.', #0 + IntToStr(Ord(eitIntConst)) + #1 + '0');
  AddFunction('function Trunc(AValue: Float): Integer', 'Math Routines',
                'Truncates a float-type value to an integer-type value');

  AddFunction('function Format(AFormatStr: String; AValue: Variant): String', 'Misc',
                'Formats AValue with AFormatStr and returns formated string value', IntToStr(Ord(eitStrConst)) + #1 + '#,##0.00' + #0);
  AddFunction('function Subst(AValue: Variant; ACompareValue: Variant; AResultValue: Variant): Variant', 'Misc',
                'Returns AResultValue if AValue equals to ACompareValue. Otherwise returns AValue.',
                 #0 +  IntToStr(Ord(eitNull)) + #0);
  AddFunction('function CheckIf(AValue1: Variant; AIfOper: String; AValue2: Variant; AThenValue: Variant; AElseValue: Variant): Variant', 'Misc',
              'If result of comparison is true function calcultates and returns AThenValue. Otherwise function calculates and returns AElseValue', #0 + IntToStr(Ord(eitStrConst)) + #1 + '='#0 + #0#0#0);
{  AddFunction('function SetVar(AVarName: String; AValue: Variant): Variant', 'Misc',
              'Assigns AValue to the internal variable AVarName and returns assigned value. If the variable does not exist function creates at first this variable.');
  AddFunction('function GetVar(AVarName: String): Variant', 'Misc',
              'Returns a value of the internal variable AVarName.');
}              
end;



{ TrwQBFunction }

procedure TrwQBFunction.GetParam(const AParamIndex: Integer;
  var AType: TrwQBExprItemType; var AValue: Variant; var ADescr: String);
var
  i: Integer;
  h, h1: String;
begin
  h := FDefaultParams;
  for i := 1 to AParamIndex do
    h1 := GetNextStrValue(h, #0);
  h := h1;

  h1 := GetNextStrValue(h, #1);
  if h1 <> '' then
  begin
    AType := TrwQBExprItemType(StrToInt(h1));
    h1 := GetNextStrValue(h, #1);
    case AType of
      eitIntConst:   AValue := StrToInt(h1);
      eitFloatConst: AValue := StrToFloat(h1);
      eitDateConst:  AValue := StrToDate(h1);
    else
      AValue := h1;
    end;

    ADescr := GetNextStrValue(h, #1);
  end

  else
    AType := eitNone;
end;


{ TrwQBTemplate }

function TrwQBTemplate.CreateQuery: TrwQBQuery;
var
  MS: IEvDualStream;
begin
  Result := TrwQBQuery.Create(nil);
  MS := TEvDualStreamHolder.Create(Length(Data));
  MS.WriteBuffer(Data[1], Length(Data));
  MS.Position := 0;
  Result.ReadFromStream(MS.RealStream);
end;


procedure TrwQBTemplate.LoadFromStream(AStream: TStream);
var
  h: String;
begin
  SetLength(h, AStream.Size);
  AStream.Read(PString(h)^, AStream.Size);
  Data := h;
end;


{ TrwQBTemplates }

function TrwQBTemplates.Add: TrwQBTemplate;
begin
  Result := TrwQBTemplate.Create(Self);
end;


function TrwQBTemplates.GetItem(Index: Integer): TrwQBTemplate;
begin
  Result := TrwQBTemplate(inherited Items[Index])
end;


procedure TrwQBTemplates.Initialize;
var
  MS: IEvDualStream;
begin
  Clear;
  MS := TEvDualStreamHolder.Create;
  RWEngineExt.AppAdapter.LoadQBTemplates(MS.RealStream);
  MS.Position := 0;
  if MS.Size > 0 then
    LoadFromStream(MS.RealStream);
end;

procedure TrwQBTemplates.LoadFromStream(AStream: TStream);
var
  Reader: TReader;
begin
  Reader := TReader.Create(AStream, 1024);
  try
    Reader.NextValue;
    Reader.ReadValue;
    Reader.ReadCollection(Self);
  finally
    Reader.Free;
  end;
end;

procedure TrwQBTemplates.Store;
var
  MS: IEvDualStream;
begin
  inherited;
  MS := TEvDualStreamHolder.Create;
  SaveToStream(MS.RealStream);
  MS.Position := 0;
  RWEngineExt.AppAdapter.UnloadQBTemplates(MS.RealStream);
end;


procedure TrwQBTemplates.SaveToStream(AStream: TStream);
var
  Writer: TWriter;
begin
  Writer := TWriter.Create(AStream, 1024);
  try
    Writer.WriteCollection(Self);
  finally
    Writer.Free;
  end;
end;



{ TrwQBOwnedCollection }

function TrwQBOwnedCollection.GetOwner: TPersistent;
begin
  Result := FOwner;
end;


{ TlqConsts }

class function TrwQBConsts.Initialize: TsbCustomSQLConstants;
begin
  Result := TrwQBConsts.Create(TrwQBConst);
  TrwQBConsts(Result).Load;
end;

function TrwQBConsts.AddConst(const AGroup, AName, ADisplayName: String; const AConstType: TsbConstType; const AStrValue: String): TrwQBConst;
begin
  Result := TrwQBConst(Add);
  Result.Group := AGroup;
  Result.ConstName := AName;
  Result.DisplayName := ADisplayName;
  Result.ConstType := AConstType;
  Result.StrValue := AStrValue;
end;

procedure TrwQBConsts.ConstsByGroup(const AGroup: String; AStringList: TStringList);
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    with TrwQBConst(Items[i]) do
      if (Length(AGroup) = 0) or AnsiSameText(Group, AGroup) then
         AStringList.AddObject(ConstName, Items[i]);
end;

procedure TrwQBConsts.Load;
var
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create;
  RWEngineExt.AppAdapter.LoadSQLConstants(MS.RealStream);
  MS.Position := 0;
  if MS.Size > 0 then
    LoadFromStream(MS.RealStream);
end;

procedure TrwQBConsts.LoadFromStream(AStream: TStream);
var
  Reader: TReader;
begin
  Reader := TReader.Create(AStream, 1024);
  try
    Reader.NextValue;
    Reader.ReadValue;
    Reader.ReadCollection(Self);
  finally
    Reader.Free;
  end;
end;

procedure TrwQBConsts.Store;
var
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create;
  SaveToStream(MS.RealStream);
  RWEngineExt.AppAdapter.UnLoadSQLConstants(MS.RealStream);
end;

procedure TrwQBConsts.SaveToStream(AStream: TStream);
var
  Writer: TWriter;
begin
  Writer := TWriter.Create(AStream, 1024);
  try
    Writer.WriteCollection(Self);
  finally
    Writer.Free;
  end;
end;


{ TrwQBConst }

procedure TrwQBConst.Assign(ASource: TPersistent);
begin
  inherited;
  FGroup := TrwQBConst(ASource).Group;
end;

function TrwQBSortFields.SortSQLCondition: String;
var
  i: Integer;
begin
  Result := '';
  if Count > 0 then
  begin
    for i := 0 to Count - 1 do
      if Assigned(TrwQBSortField(Items[i]).Field) then
      begin
        Result := Result + TrwQBSortField(Items[i]).Field.GetFieldName(False, False);
        if TrwQBSortField(Items[i]).SortDirection = rsdDESC then
          Result := Result + ' DESC';
        if i < Count - 1 then
          Result := Result + ', ';
      end;
  end;
end;

initialization
  SetSQLConstClass(TrwQBConsts);

end.
