unit rwQBJoinEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwQBInfo, rwUtils, ComCtrls, rwDataDictionary;

type
  TrwQBJoinEditor = class(TForm)
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    cbTable1: TComboBox;
    cbField1: TComboBox;
    Label2: TLabel;
    cbTable2: TComboBox;
    Label4: TLabel;
    cbField2: TComboBox;
    Label5: TLabel;
    cbJoin: TComboBox;
    btnOK: TButton;
    btnCancel: TButton;
    procedure cbTable1Change(Sender: TObject);
  private
  public
  end;



function QBAddJoin(AQBQuery: TrwQBQuery; ATable1: TrwQBSelectedTable = nil; AField1: String = '';
                   ATable2: TrwQBSelectedTable = nil; AField2: String = ''): Boolean;


implementation

{$R *.DFM}

uses rwQBTableFrm, rwQueryBuilderFrm;



function QBAddJoin(AQBQuery: TrwQBQuery; ATable1: TrwQBSelectedTable = nil; AField1: String = '';
                   ATable2: TrwQBSelectedTable = nil; AField2: String = ''): Boolean;
var
  Frm: TrwQBJoinEditor;
  Jn: TrwQBJoin;
  lT, rT: TrwQBSelectedTable;

  procedure FillTableList(ACmbBox: TComboBox);
  var
    i: Integer;
    h: String;
  begin
    for i := 0 to AQBQuery.SelectedTables.Count - 1 do
    begin
      if TrwQBSelectedTable(AQBQuery.SelectedTables[i]).IsSUBQuery and
         TrwQBSelectedTable(AQBQuery.SelectedTables[i]).SUBQuery.IsConditionedSQL then
        continue;

      h := TrwQBSelectedTable(AQBQuery.SelectedTables[i]).GetTableName(FQBFrm.WizardView, True);
      if TrwQBSelectedTable(AQBQuery.SelectedTables[i]).Alias <> '' then
        h := TrwQBSelectedTable(AQBQuery.SelectedTables[i]).Alias + '   ' + h;

      ACmbBox.Items.AddObject(h, AQBQuery.SelectedTables[i]);
    end;
  end;


  function GetFieldName(ACmbBox: TComboBox; AIndex: Integer): String;
  begin
    if TObject(ACmbBox.Items.Objects[AIndex]) is TrwDataDictField then
      Result := TrwDataDictField(ACmbBox.Items.Objects[AIndex]).Name
    else
      Result := TrwQBShowingField(ACmbBox.Items.Objects[AIndex]).InternalName;
  end;


  procedure SetField(ACmbBox: TComboBox; AField: string);
  var
    i: Integer;
  begin
    ACmbBox.ItemIndex := -1;

    for i := 0 to ACmbBox.Items.Count - 1 do
    begin
      if AnsiSameText(GetFieldName(ACmbBox, i), AField) then
      begin
        ACmbBox.ItemIndex := i;
        break;
      end;
    end;
  end;


begin
  Result := False;

  if ATable1 = ATable2 then
    Exit;

  Frm := TrwQBJoinEditor.Create(nil);
  with Frm do
    try
      FillTableList(cbTable1);
      if Assigned(ATable1) then
      begin
        cbTable1.ItemIndex := cbTable1.Items.IndexOfObject(ATable1);
        if cbTable1.ItemIndex = -1 then
          Exit;
      end
      else
        cbTable1.ItemIndex := 0;
      cbTable1.OnChange(cbTable1);
      SetField(cbField1, AField1);

      FillTableList(cbTable2);
      if Assigned(ATable2) then
      begin
        cbTable2.ItemIndex := cbTable2.Items.IndexOfObject(ATable2);
        if cbTable2.ItemIndex = -1 then
          Exit;
      end

      else
        cbTable2.ItemIndex := 0;
      cbTable2.OnChange(cbTable2);
      if Length(AField2) = 0 then
        cbField2.ItemIndex := cbField2.Items.IndexOf(cbField1.Text)
      else
        SetField(cbField2, AField2);

      cbJoin.ItemIndex := 0;

      Result := (ShowModal = mrOK) and (cbField1.ItemIndex <> -1) and (cbField2.ItemIndex <> -1) and
                (cbTable1.ItemIndex <> cbTable2.ItemIndex);

      if Result then
      begin
        lT := TrwQBSelectedTable(cbTable1.Items.Objects[cbTable1.ItemIndex]);
        rT := TrwQBSelectedTable(cbTable2.Items.Objects[cbTable2.ItemIndex]);
        Jn := FindJoin(lT, rT);

        if not Assigned(Jn) then
          Jn := TrwQBJoin(AQBQuery.Joins.Items[AQBQuery.Joins.Add(lT, rT, TrwQBJoinType(cbJoin.ItemIndex), nil)])
        else
          Jn.JoinType := TrwQBJoinType(cbJoin.ItemIndex);

        if Jn.LeftTableFields <> '' then
        begin
          Jn.LeftTableFields := Jn.LeftTableFields + ',';
          Jn.RightTableFields := Jn.RightTableFields + ',';
        end;
        Jn.LeftTableFields := Jn.LeftTableFields + GetFieldName(cbField1, cbField1.ItemIndex);
        Jn.RightTableFields := Jn.RightTableFields + GetFieldName(cbField2, cbField2.ItemIndex);
      end;
    finally
      Frm.Free;
    end;
end;


procedure TrwQBJoinEditor.cbTable1Change(Sender: TObject);
var
  i: Integer;
  Flds: TComboBox;
  LV: TListView;
  T: TrwQBSelectedTable;
begin
  if TComboBox(Sender).ItemIndex = -1 then Exit;

  T := TrwQBSelectedTable(TComboBox(Sender).Items.Objects[TComboBox(Sender).ItemIndex]);

  if Sender = cbTable1 then
  begin
    Flds := cbField1;
    cbJoin.Items[2] := 'OUTER (' + TComboBox(Sender).Items[TComboBox(Sender).ItemIndex] + ')';
  end

  else
  begin
    Flds := cbField2;
    cbJoin.Items[1] := 'OUTER (' + TComboBox(Sender).Items[TComboBox(Sender).ItemIndex] + ')';
  end;

  LV := TrwQBTable(T.VisualComponent).lvFields;
  LV.UpdateItems(0, MaxInt);  //Delphi bag
  Flds.Clear;
  for i := 0 to LV.Items.Count - 1  do
    Flds.Items.AddObject(LV.Items[i].Caption, LV.Items[i].Data);
end;

end.
