unit rmPrintTable;

interface

uses Classes, Windows, Graphics, StdCtrls, SysUtils, Variants, Controls, Messages,
     rmCommonClasses, rwGraphics, rwTypes, rwBasicClasses, evStreamUtils,
     rwDB, rwQBInfo, rwEngineTypes, rwUtils, rwEngine, sbAPI, rmPrintFormControls,
     rmTypes, rmFormula, Math;

const
  cMinTableCellWidth = 60;
  cMinTableCellHeight = 60;

type
  TrmTableCell = class;
  TrmTableCellClass = class of TrmTableCell;
  TrmCustomTable = class;


  TrmTableRowInfo = class(TList)
  public
    procedure   LinkTableCell(ATableCell: TrmTableCell; ABeginCell, AEndCell: Integer);
  end;

  TrmTableInfo = class(TList)
  private
    FOwner: TrmCustomTable;
    FColWidths:  TList;
    FRowHeights: TList;
    FSkipValueChecking: Boolean;
    function  GetColsCount: Integer;
    function  GetRowsCount: Integer;
    function  GetRowHeight(ARow: Integer): Integer;
    procedure SetRowHeight(ARow: Integer; Value: Integer);
    function  GetColWidth(ACol: Integer): Integer;
    procedure SetColWidth(ACol: Integer; Value: Integer);
    function  CheckRowConstrain(ARow: Integer; var Value: Integer): Boolean;
    function  CheckColConstrain(ACol: Integer; var Value: Integer): Boolean;

  // Functionality for storing/restorunf Col widths and Row heights
  private
    FStoredColWidths:  TList;
    FStoredRowHeights: TList;
  public
    procedure   StoreState;
    procedure   RestoreState;

  public
    constructor Create;
    destructor  Destroy; override;
    procedure   Clear; override;
    procedure   Delete(Index: Integer); reintroduce;
    procedure   InsertRow(AFirstRow: Integer; AQuantity: Integer = 1; EmptyRow: Boolean = False; ARowHeight: Integer = cMinTableCellHeight);
    procedure   InsertCol(AFirstCol: Integer; AQuantity: Integer = 1; AColWidth: Integer = cMinTableCellWidth);
    procedure   DeleteRow(AFirstRow: Integer; AQuantity: Integer = 1);
    procedure   LinkTableCell(ATableCell: TrmTableCell; ALeftCell, ATopCell, ARightCell, ABottomCell: Integer);
    procedure   SplitCell(ATableCell: TrmTableCell; XPieces, YPieces: Integer);
    procedure   MergeCells(X1, Y1, X2, Y2: Integer);
    procedure   PackColumns(X1, X2: Integer);
    procedure   PackRows(Y1, Y2: Integer);
    function    Cell(X, Y: Integer): TrmTableCell;

    property    RowsCount: Integer read GetRowsCount;
    property    ColsCount: Integer read GetColsCount;
    property    RowHeight[Index: Integer]: Integer read GetRowHeight write SetRowHeight;
    property    ColWidth[Index: Integer]: Integer read GetColWidth write SetColWidth;
  end;


  TrmListOfCells = array of TrmTableCell;
  PTrmListOfCells = ^TrmListOfCells;


  TrmRenderedBorderLineInfo = record
    PointA:       TPoint;
    PointB:       TPoint;
    Width:        Integer;
    Color:        TColor;
    Style:        TPenStyle;
    Compare:      Boolean;
    LayerNumber:  TrwLayerNumber;
  end;

  PTrmRenderedBorderLineInfo = ^TrmRenderedBorderLineInfo;

  TrmRenderedBorderLines = class(TList)
  private
    FOwner: TrmCustomTable;
    function  GetItem(Index: Integer): TrmRenderedBorderLineInfo;

  public
    constructor Create(AOwner: TrmCustomTable);
    procedure Clear; override;
    function  AddLine(const ALineInfo: TrmRenderedBorderLineInfo): Integer;
    procedure CreateRWAObjects;

    property  Items[Index: Integer]: TrmRenderedBorderLineInfo read GetItem; default;
  end;



  TrmCustomTable = class(TrmPrintContainer)
  private
     FTableInfo: TrmTableInfo;
     FResizingDsblCount: Integer;
     FCurrentSelectedCells: TrmListOfCells;
     procedure ReadGridInfo(Stream: TStream);
     procedure WriteGridInfo(Stream: TStream);

     procedure PropogateGlobVarsToChildren; virtual; abstract;

  protected
    procedure Loaded; override;
    procedure AlignChildren; override;
    procedure SetPosition(Index: Integer; Value: Integer); override;
    function  GetPosition(Index: Integer): Integer; override;
    function  CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean; override;
    procedure InitializeTableInfo; virtual; abstract;
    procedure DefineProperties(Filer: TFiler); override;
    procedure UpdateCellBoundsInfo; virtual; abstract;
    procedure UpdateCellTableInfo; virtual; abstract;
    procedure Notify(AEvent: TrmDesignEventType); override;
    function  ResizingDisabled: Boolean;
    procedure EnableResizing;
    procedure DisableResizing;
    procedure PaintForeground(ACanvas: TrwCanvas); override;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    function  FindCellRectangle(const ACornerCell1, ACornerCell2: TrmTableCell): TRect;
    function  FindCellPosRectangle(const ACornerCell1, ACornerCell2: TrmTableCell): TRect;
    function  GetCellsInRect(const ACornerCell1: TrmTableCell; const ACornerCell2: TrmTableCell): TrmListOfCells;
    function  CellAtPos(const APos: TPoint): TrmTableCell; virtual;
    procedure SetTableWidth(const ANewWidth: Integer);
    procedure SetTableHeight(const ANewHeight: Integer);

    //rendering
  private
    FRenderedBorderLines: TrmRenderedBorderLines;
  protected
    function  RenderedBorderLines: TrmRenderedBorderLines;
  public
    procedure DoPrint; override;  
    //end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   AfterChangePos; override;
    function    TopMostRow: Integer; virtual;
    function    BottomMostRow: Integer; virtual;
    function    LeftMostCol: Integer; virtual;
    function    RightMostCol: Integer; virtual;

    property    TableInfo: TrmTableInfo read FTableInfo;

  published
    property Width stored DontStoreIndProperty;
    property Height stored DontStoreIndProperty;
  end;


  TrmTable = class(TrmCustomTable, IrmTable)
  private
    FCellClass: String;
    FChanging: Boolean;
    FFont: TrwFont;
    FApplyingBorderChanges: Boolean;
    procedure SetFont(const Value: TrwFont);
    procedure OnChangeFont(Sender: TObject);
    procedure PropogateGlobVarsToChildren; override;

  protected
    procedure UpdateCellBoundsInfo; override;
    procedure SyncTagPos; override;
    procedure SetMultiPaged(const Value: Boolean); override;
    procedure ReadState(Reader: TReader); override;
    procedure WriteState(Writer: TWriter); override;
    procedure InitializeTableInfo; override;
    procedure Loaded; override;
    procedure SetColor(Value: TColor); override;
    procedure CreateInitialCell;
    procedure AfterChangeCellLayout; virtual;
    procedure UpdateCellTableInfo; override;
    function  CellAtPos(const APos: TPoint): TrmTableCell; override;

  //Interface
    procedure AddChildObject(AObject: IrmDesignObject); override;
    procedure RemoveChildObject(AObject: IrmDesignObject); override;
    function  MergeCells(const ACornerCell1Name, ACornerCell2Name: String): IrmTableCell; overload;
    function  GetCellsInRect(const ACornerCell1: IrmTableCell; const ACornerCell2: IrmTableCell): TrmListOfObjects; overload;
    procedure ClearTable;
  //end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   InitializeForDesign; override;
    procedure   AfterConstruction; override;
    procedure   SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure   SplitCell(const ACell: TrmTableCell; const ACols, ARows: Integer); overload;
    function    MergeCells(const ACornerCell1: TrmTableCell; const ACornerCell2: TrmTableCell): TrmTableCell; overload;
    procedure   GetCellRect(ACell1, ACell2: TrmTableCell; ARectCells: TList);
    function    HasGapFromBottom: Boolean; virtual;
    function    HasGapFromRight: Boolean; virtual;

    //rendering
  protected
    procedure StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean); override;
    procedure ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean); override;
  public
    procedure DoCalculate; override;
    procedure CreateRWAObject; override;
  //end

  published
    property Align;
    property Font: TrwFont read FFont write SetFont;
  end;

  TrmDBTable = class;

  TrmTableCell = class(TrmPrintContainer, IrmTableCell, IrmQueryAwareObject)
  private
    FTable: TrmTable;
    FCellRect: TRect;
    FValueHolder: TrmsValueHolder;
    FFormat: String;
    FFont: TrwFont;
    FTableFont: Boolean;
    FAlignment: TAlignment;
    FLayout: TTextLayout;
    FBorderLines: TrwBorderLines;
    FCellTextSize: TSize;
    FForecastWidth: Integer;
    FWordWrap: Boolean;
    FAutoHeight: Boolean;
    FMouseSelectionActive: Boolean;
    FFixedWidth: Boolean;
    FSubstitution: Boolean;
    FEmptyText: String;
    FSubstitID: String;

    procedure ReadSize(Reader: TReader);
    procedure WriteSize(Writer: TWriter);
    procedure SetFormat(const Value: String);
    procedure SetFont(const Value: TrwFont);
    procedure OnChangeFont(Sender: TObject);
    procedure OnChangeBorderLines(Sender: TObject);
    function  IsNotTableFont: Boolean;
    function  IsNotTableColor: Boolean;
    procedure SetAlignment(const Value: TAlignment);
    procedure SetLayout(const Value: TTextLayout);
    procedure SetBorderLines(const Value: TrwBorderLines);
    procedure CalcTextSize(ARect: TRect);
    function  GetValue: Variant;
    function  GetSubTable: TrmDBTable;
    procedure ApplyBorderLinesForSubTable;
    function  CanWriteValue: Boolean;
    function  FormulaValue: TrmFMLFormula;
    function  FormulaTextFilter: TrmFMLFormula;
    procedure UpdateTableInfo;
    procedure SetAutoHeight(const AValue: Boolean);
    procedure SetWordWrap(const AValue: Boolean);
    procedure SetFixedWidth(const Value: Boolean);
    procedure SyncTableTagPos;
    function  GetDataField: String;
    procedure SetDataField(const AValue: String);
    function  GetDBTable: TrmDBTable;
    function  StoreInnerMarginLeft: Boolean;
    function  StoreInnerMarginRight: Boolean;
    function  StoreInnerMarginTop: Boolean;
    function  StoreInnerMarginBottom: Boolean;

  protected
    procedure DoAlign; override;
    procedure SetInnerMarginBottom(const Value: Integer); override;
    procedure SetInnerMarginLeft(const Value: Integer); override;
    procedure SetInnerMarginRight(const Value: Integer); override;
    procedure SetInnerMarginTop(const Value: Integer); override;
    procedure SetColor(Value: TColor); override;
    procedure SetPosition(Index: Integer; Value: Integer); override;
    function  GetPosition(Index: Integer): Integer; override;
    function  GetShowText: String; override;
    function  GetText: String; virtual;
    procedure DefineProperties(Filer: TFiler); override;
    procedure SetMultiPaged(const Value: Boolean); override;
    procedure SetPrintOnEachPage(const Value: Boolean); override;
    procedure SetBlockParentIfEmpty(const Value: Boolean); override;
    procedure SetValue(const AValue: Variant); virtual;
    procedure CheckAutosize;
    function  CellTextSize: TSize;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    procedure PaintControl(ACanvas: TrwCanvas); override;
    function  CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean; override;
    procedure Loaded; override;
    procedure ApplyMouseResizing(NewBounds: TRect; MousePosition: TrmdMousePosition; Shift: TShiftState); override;
    procedure PaintForeground(ACanvas: TrwCanvas); override;

  //Interface
    function  GetFieldName: String;
    procedure SetFieldName(const AFieldName: String);
    function  GetDataFieldInfo: TrmDataFieldInfoRec;
    procedure SetValueFromDataSource;
    procedure CreateSubTable; virtual;
    procedure DestroySubTable;
    procedure SplitCell(const ACols, ARows: Integer);
    function  GetClosestCell(const ADirection: Char): IrmTableCell;
  //end

  //rendering
  private
    procedure PrepareTextForPrinting;
  protected
    procedure SetRendStatus(const Value: TrmRWARenderingStatus); override;
    procedure StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean); override;
    procedure StoreChildren(const AStream: IEvDualStream; AStorePrintState: Boolean); override;
    procedure ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean); override;
    procedure ReStoreChildren(const AStream: IEvDualStream; ARestorePrintState: Boolean); override;
    function  PreCutChildren: Boolean; override;
  public
    procedure CreateRWAObject; override;
    procedure DoCalculate; override;
 //end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   BeforeChangePos; override;
    procedure   AfterChangePos; override;
    procedure   Assign(Source: TPersistent); override;
    procedure   SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure   SetCellSize(X, Y, AWidth, AHeight: Integer);
    procedure   GetNeighbours(const AEdge: Char; const ACells: TList);
    procedure   MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure   MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure   MouseMove(Shift: TShiftState; X, Y: Integer); override;


    property    CellRect: TRect read FCellRect;
    property    Table: TrmTable read FTable;
    property    DBTable: TrmDBTable read GetDBTable;
    property    SubTable: TrmDBTable read GetSubTable;

  published
    property Left stored False;
    property Top stored False;
    property Width stored DontStoreIndProperty;
    property Height stored DontStoreIndProperty;
    property Color stored IsNotTableColor;
    property PrintOnEachPage stored False;
    property Multipaged stored False;
    property BlockParentIfEmpty stored False;
    property Value: Variant read GetValue write SetValue stored CanWriteValue;
    property DataField: String read GetDataField write SetDataField;
    property Format: String read FFormat write SetFormat;
    property Text: String read GetText stored False;
    property Font: TrwFont read FFont write SetFont stored IsNotTableFont;
    property Alignment: TAlignment read FAlignment write SetAlignment;
    property Layout: TTextLayout read FLayout write SetLayout;
    property BorderLines: TrwBorderLines read FBorderLines write SetBorderLines;
    property AutoHeight: Boolean read FAutoHeight write SetAutoHeight;
    property WordWrap: Boolean read FWordWrap write SetWordWrap;
    property FixedWidth: Boolean read FFixedWidth write SetFixedWidth;
    property InnerMarginLeft stored StoreInnerMarginLeft;
    property InnerMarginRight stored StoreInnerMarginRight;
    property InnerMarginTop stored StoreInnerMarginTop;
    property InnerMarginBottom stored StoreInnerMarginBottom;

    property Substitution: Boolean read FSubstitution write FSubstitution stored False;  // obsolete
    property SubstID: String read FSubstitID write FSubstitID stored False;  // obsolete
    property EmptyText: String read FEmptyText write FEmptyText stored False;  // obsolete


    function PSubTable: Variant;
  end;


  {Table Group}
  TrmDBTableGroupHeaderBand = class;
  TrmDBTableGroupFooterBand = class;

  TrmRenderingGroupFieldInfo = record
    Field: TrwField;
    GroupValue: Variant;
  end;

  TrmDBTableGroup = class(TrwCollectionItem, IrmDBTableGroup)
  private
    FFields: TStringList;
    FGroupName: String;
    FDisabled: Boolean;
    FHeaderBand: TrmDBTableGroupHeaderBand;
    FFooterBand: TrmDBTableGroupFooterBand;

    procedure SetFieldList(const Value: TStringList);

  protected
    function  AsAncestor(const AncestorItem: TrwCollectionItem): Boolean; override;
    procedure ApplyDelta(const DeltaCollectionItem: TrwCollectionItem); override;

    // interface
    function  GetGroupName: String;
    procedure SetGroupName(const Value: String);
    function  GetFields: TCommaDilimitedString;
    procedure SetFields(const Value: TCommaDilimitedString);
    function  GetDisabled: Boolean;
    procedure SetDisabled(const Value: Boolean);
    function  GetPrintHeaderBand: Boolean;
    procedure SetPrintHeaderBand(const Value: Boolean);
    function  GetPrintFooterBand: Boolean;
    procedure SetPrintFooterBand(const Value: Boolean);

  //rendering
  private
    FGroupFields: array of  TrmRenderingGroupFieldInfo;  // represents group field values
    procedure PrepareToCalc;
    procedure ClearCalcInfo;
    procedure StartNewGroup;
    procedure StoreGroupValues;
    function  IsGroupChanged: Boolean;
    function  IsGroupBeginning: Boolean;
  //end

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;

  published
    property GroupName: String read FGroupName write SetGroupName stored IsNotInherited;
    property Fields: TStringList read FFields write SetFieldList stored IsNotInherited;
    property Disabled: Boolean read FDisabled write FDisabled;
  end;


  TrmDBTableGroups = class(TrwCollection, IrmDBTableGroups)
  private
    FPageBreakGroup: TrmDBTableGroup;
    function  GetItem(Index: Integer): TrmDBTableGroup;

  protected
    procedure Update(Item: TCollectionItem); override;

    //interface
    function  GetGroupByName(const AGroupName: String): IrmDBTableGroup;
    procedure DeleteItem(const AItemIndex: Integer); override;
    function  GetPageBreakGroupID: String;
    procedure SetPageBreakGroup(const AGroupID: String);

  public
    property  Items[Index: Integer]: TrmDBTableGroup read GetItem; default;

    constructor Create(AOwner: TrwComponent);
    function    GroupByName(const AGroupName: String): TrmDBTableGroup;
    procedure   Assign(Source: TPersistent); override;
  end;



  { DB Table Band}

  TrmDBTableBandType = (rwTBTDetail, rwTBTHeader, rwTBTTotal, rwTBTGroupHeader, rwTBTGroupFooter,
                        rwTBTPageBreak);

  TrmDBTableBand = class(TrmTable)
  private
    FBandType: TrmDBTableBandType;
    FTopMostRow: Integer;
    FBottomMostRow: Integer;
    FChangingTags: Boolean;
    FPrintIfNoData: Boolean;
    procedure LinkBandToTable; virtual;
    procedure SetBandType(const Value: TrmDBTableBandType);

  protected
    procedure SyncTagPos; override;
    procedure SetAlign(Value: TAlign); override;
    procedure SetPosition(Index: Integer; Value: Integer); override;
    function  GetPosition(Index: Integer): Integer; override;
    procedure InitializeTableInfo; override;
    procedure UpdateCellBoundsInfo; override;
    procedure AfterChangeCellLayout; override;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    procedure SetPrintOnEachPage(const Value: Boolean); override;
    procedure SetPrintIfNoData(const Value: Boolean); virtual;

  //rendering
  public
    procedure DoCalculate; override;
    procedure BeforePrint; override;
    procedure CalcCutting; override;
  //end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   InitializeForDesign; override;
    procedure   BeforeChangePos; override;
    procedure   AfterChangePos; override;
    function    LeftMostCol: Integer; override;
    function    RightMostCol: Integer; override;
    function    TopMostRow: Integer; override;
    function    BottomMostRow: Integer; override;
    function    HasGapFromBottom: Boolean; override;
    function    HasGapFromRight: Boolean; override;

  published
    property Left stored False;
    property Top stored False;
    property Multipaged stored False;
    property PrintOnEachPage stored False;
    property BandType: TrmDBTableBandType read FBandType write SetBandType stored False;
    property PrintIfNoData: Boolean read FPrintIfNoData write SetPrintIfNoData stored False;
  end;


  TrmDBTableHeaderBand = class(TrmDBTableBand)
  private
    procedure LinkBandToTable; override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property PrintOnEachPage stored True;
    property PrintIfNoData stored True;
  end;


  TrmDBTableDetailBand = class(TrmDBTableBand)
  private
    procedure LinkBandToTable; override;
  protected
    procedure SetPrintIfNoData(const Value: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
  end;


  TrmDBTableTotalBand = class(TrmDBTableBand)
  private
    FAlignToBound: Boolean;
    procedure LinkBandToTable; override;
    procedure SetAlignToBound(const Value: Boolean);
  public
    constructor Create(AOwner: TComponent); override;
  published
    property AlignToBound: Boolean read FAlignToBound write SetAlignToBound;
    property PrintIfNoData stored True;    
  end;


  TrmDBTableCustomGroupBand = class(TrmDBTableBand)
  private
    FGroupID: String;
    FGroup: TrmDBTableGroup;
    procedure ReadGroupID(Reader: TReader);
    procedure WriteGroupID(Writer: TWriter);
  protected
    procedure DefineProperties(Filer: TFiler); override;
    procedure SetDescription(const Value: String); override;
    procedure SetPrintIfNoData(const Value: Boolean); override;
  public
    property GroupID: String read FGroupID;
  published
    property Description stored False;
  end;


  TrmDBTableGroupHeaderBand = class(TrmDBTableCustomGroupBand)
  protected
    function  GetDescription: String; override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property PrintOnEachPage stored True;
  end;


  TrmDBTableGroupFooterBand = class(TrmDBTableCustomGroupBand)
  protected
    function  GetDescription: String; override;
  public
    constructor Create(AOwner: TComponent); override;
  end;


  TrmDBTableQuery = class(TrwQuery)
  private
    FTable: TrmDBTable;
  protected
    procedure DoBeforeOpen; override;
    procedure DoAfterOpen; override;
    procedure DoAfterClose; override;
    procedure OnAssignQBInfo(Sender: TObject); override;

  //Interface
    function  CallCustomFunction(const AFunctionName: String; const AParams: array of Variant): Variant; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    property Table: TrmDBTable read FTable;
  end;


  { DB Table}

  TrmDBTableRenderContent = class;
  TrmdTableBorder = class;

  TrmDBTable = class(TrmCustomTable, IrmDBTableObject, IrmQueryDrivenObject)
  private
    FHeaderBand:  TrmDBTableBand;
    FDetailBand:  TrmDBTableBand;
    FTotalBand:   TrmDBTableBand;
    FGroups:      TrmDBTableGroups;
    FGroupsRW:    TrmDBTableGroups; // For streaming only
    FQuery: TrmDBTableQuery;
    FMinWorkAreaSize: Integer;
    FChangingTags: Boolean;

    FSBTStandAloneMode: Boolean;
    FSBTDesignBorder: TrmdTableBorder;
    function  GetMasterFields: TrwStrings;
    procedure SetMasterFields(const AValue: TrwStrings);

    procedure ReadGroups(Reader: TReader);
    procedure WriteGroups(Writer: TWriter);
    procedure ReadGroupOrder(Reader: TReader);
    procedure WriteGroupOrder(Writer: TWriter);
    procedure ReadPageBreakGroup(Reader: TReader);
    procedure WritePageBreakGroup(Writer: TWriter);
    function  GetQueryBuilderInfo: TrwQBQuery;
    procedure SetQueryBuilderInfo(const Value: TrwQBQuery);
    function  GetParamsFromVars: Boolean;
    procedure SetParamsFromVars(const Value: Boolean);
    procedure PropogateGlobVarsToChildren; override;
    procedure UpdateGroupBandsPosition;
    function  GetDataSource: TrwDataSet;

  protected
    procedure SetContainer(Value: TrmPrintContainer); override;
    procedure ShowTag; override;
    procedure HideTag; override;
    procedure SyncTagPos; override;
    procedure SetAlign(Value: TAlign); override;    
    procedure SetColor(Value: TColor); override;
    function  GetPosition(Index: Integer): Integer; override;
    procedure SetPosition(Index: Integer; Value: Integer); override;
    procedure SetMultiPaged(const Value: Boolean); override;
    function  CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean; override;
    procedure ReadState(Reader: TReader); override;
    procedure WriteState(Writer: TWriter); override;
    procedure Loaded; override;
    procedure InitializeTableInfo; override;
    procedure UpdateCellBoundsInfo; override;
    procedure UpdateCellTableInfo; override;
    procedure PaintForeground(ACanvas: TrwCanvas); override;
    function  GetWorkAreaRect: TRect;
    function  GetZoomedWorkAreaRect(ACanvas: TrwCanvas): TRect;
    procedure DefineProperties(Filer: TFiler); override;
    procedure SetInheritedComponentProperty(AValue: Boolean); override;
    procedure AfterInitLibComponent; override;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    procedure ApplyMouseResizing(NewBounds: TRect; MousePosition: TrmdMousePosition; Shift: TShiftState); override;

  //Interface
    procedure Notify(AEvent: TrmDesignEventType); override;
    function  CallCustomFunction(const AFunctionName: String; const AParams: array of Variant): Variant; override;
    function  GetGroups: IrmDBTableGroups;
    function  IsSubTable: Boolean;
    function  GetQuery: IrmQuery;
  //End

  //rendering
  private
    FRendContent: TrmDBTableRenderContent;  // Stores calculated content of the table
    FRendLastBandPos: Integer;              // Bottom position of the last calculate/printed band
    FRendCurrentBand: TrmDBTableBand;       // Curently printing band. It gets used for cutting needs.
    FCurrentTblVersion: Integer;            // Nees for SubTables in order to distinguish different result sets
    FGrouping: Boolean;                     // There are active groups
    FContentPosBeforeCalc: Integer;         // Used as an indicator that table is empty
    procedure CalcCurrentTblVersion;
    procedure PrepareForReadingContent;
    procedure GetDataFromDataSource;
    procedure SBTAlignBands;
  protected
    procedure OnPageCalculated; override;
    function  PreCutChildren: Boolean; override;
    function  GetEachPageHeaderHeight: Integer; override;
    function  GetEachPageFooterHeight: Integer; override;
    procedure StorePrintState(const AStream: IEvDualStream); override;
    procedure ReStorePrintState(const AStream: IEvDualStream); override;
  public
    function  BeforeCalculate: Boolean; override;
    procedure DoCalculate; override;
    function  AfterCalculate: Boolean; override;
    procedure DoPrint; override;
    procedure PrepareForNextPrintCycle; override;
    procedure PrintBands;
    procedure CreateRWAObject; override;
    procedure NotifyAggregateFunctions; override;
  //end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   AfterConstruction; override;
    procedure   BeforeDestruction; override;
    procedure   AfterChangePos; override;
    procedure   InitializeForDesign; override;
    function    BandByType(const ABandType: TrmDBTableBandType; AGroup: TrmDBTableGroup = nil): TrmDBTableBand;
    function    AddBand(const ABandType: TrmDBTableBandType; AGroup: TrmDBTableGroup = nil): TrmDBTableBand;
    procedure   SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure   SyncCellsByQuery;
    function    CellOwner: TrmTableCell;
    function    MainTable: TrmCustomTable;

    property    Groups: TrmDBTableGroups read FGroups;

   published
    property Align;
    property Height stored True;
    property QueryBuilderInfo: TrwQBQuery read GetQueryBuilderInfo write SetQueryBuilderInfo;
    property ParamsFromVars: Boolean read GetParamsFromVars write SetParamsFromVars stored DontStoreProperty;
    property MasterFields: TrwStrings read GetMasterFields write SetMasterFields stored IsSubTable;

    property DataSource: TrwDataSet read GetDataSource;
  end;


  // Rendering objects

  TrmDBTableRenderContent = class(TPersistent)
  private
    FReading: Boolean;
    FData: TsbTableCursor;
    FCurrentTblVersion: Integer;
    FCurrentSeqNbr: Integer;
    procedure SetCurrentTblVersion(const Value: Integer);
    procedure SetReading(Value: Boolean);
  public
    constructor Create;
    destructor  Destroy; override;
    procedure   StoreBand(const ABand: TrmDBTableBand);
    procedure   UpdateBand(const ABand: TrmDBTableBand);
    procedure   ReadBand(const ABand: TrmDBTableBand);
    procedure   StorePageBreak;
    procedure   First;
    procedure   Last;
    procedure   Prior;
    procedure   Next;
    procedure   StoreState(const AStream: IEvDualStream);
    procedure   RestoreState(const AStream: IEvDualStream);
    function    Eof: Boolean;
    function    BandType: TrmDBTableBandType;
    function    GroupNbr: Integer;
    function    BandSize: Integer;

    property    CurrentTblVersion: Integer read FCurrentTblVersion write SetCurrentTblVersion;
    property    CurrentSeqNbr: Integer read FCurrentSeqNbr;
  end;




  // Design-Time supplementary objects

  TrmdTableBorder = class(TrmdCustomDesignObject)
  private
    FPattern: TBitmap;
    procedure PreparePattern;
  protected
    procedure PaintToCanvas(ACanvas: TrwCanvas); override;
    function  GetShapeRegion: HRGN; override;
  public
    constructor Create(AOwner: TrmGraphicControl); override;
    destructor  Destroy; override;
  end;


  TrmdTableTag = class(TrmdSimpleTextTag)
  protected
    procedure CorrectTagBounds(var ATagRect: TRect); override;
  end;


  TrmdTableBandTag = class(TrmdTableTag)
  protected
    function  GetShapeRegion: HRGN; override;
    function  GetBracketRegion: HRGN;
    function  GetTagRegion: HRGN;
    procedure PaintToCanvas(ACanvas: TrwCanvas); override;
  public
    constructor Create(AOwner: TrmGraphicControl); override;
  end;


implementation

uses Types;

var
  cDefaultCellBorderSize: Integer;

{
// VERY IMPORTANT!
// All names of table objects (Bands, Cells, SubTables) MUST be unique from the most top table-owner to the bottom.
function GetUniqueNameForTableItem(ATableItem: TrmPrintControl; Prefix: string): string;
var
  C: TComponent;
begin
  C := ATableItem;
  while not((C is TrmCustomTable) and not (C.Owner is TrmCustomTable)) do
    C := C.Owner;

  Assert(C is TrmCustomTable, 'Unexpected Error');

  Result := GetGlobalUniqueNameForComponent(C, Prefix);
end;
}

{ TrmTableRowInfo }

procedure TrmTableRowInfo.LinkTableCell(ATableCell: TrmTableCell; ABeginCell, AEndCell: Integer);
var
  i: Integer;
begin
  for i := ABeginCell to AEndCell do
    Items[i] := ATableCell;
end;


{ TrmTableInfo }

constructor TrmTableInfo.Create;
begin
  FColWidths := TList.Create;
  FRowHeights := TList.Create;
  FStoredColWidths := TList.Create;
  FStoredRowHeights := TList.Create;
end;

destructor TrmTableInfo.Destroy;
begin
  inherited;
  FreeAndNil(FStoredColWidths);
  FreeAndNil(FStoredRowHeights);
  FreeAndNil(FColWidths);
  FreeAndNil(FRowHeights);
end;

procedure TrmTableInfo.Clear;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    TrmTableInfo(Items[i]).Free;

  inherited;
  FColWidths.Clear;
  FRowHeights.Clear;
end;

procedure TrmTableInfo.LinkTableCell(ATableCell: TrmTableCell; ALeftCell, ATopCell, ARightCell, ABottomCell: Integer);
var
  i: Integer;
begin
  for i := ATopCell to ABottomCell do
    TrmTableRowInfo(Items[i]).LinkTableCell(ATableCell, ALeftCell, ARightCell);

  ATableCell.FCellRect := Rect(ALeftCell, ATopCell, ARightCell, ABottomCell);
end;

procedure TrmTableInfo.SplitCell(ATableCell: TrmTableCell; XPieces, YPieces: Integer);
var
  R: TRect;
  n, i, j, d, cs, dCS: Integer;
begin
  FOwner.DisableResizing;
  try
    // split by rows
    R := ATableCell.FCellRect;
    n := R.Bottom - R.Top + 1;
    if n < YPieces then
    begin
      d := Trunc((YPieces - n) / n + 0.999999);
      n := YPieces - n;
      i := 0;
      while n > 0 do
      begin
        if n - d < 0 then
          d := n;
        cs := RowHeight[R.Top + i];
        dCS := cs div (d + 1);
        RowHeight[R.Top + i] := cs - dCS * d;
        InsertRow(R.Top + i, d, False, dCS);
        Inc(i, d + 1);
        Dec(n, d);
      end;

      //correct row size
      R := ATableCell.FCellRect;
      for i := R.Top to R.Bottom - 1 do
        if RowHeight[i] < cMinTableCellHeight then
        begin
          d := cMinTableCellHeight - RowHeight[i];
          for j := R.Top to R.Bottom do
            if RowHeight[j] - d > cMinTableCellHeight then
              RowHeight[j] := RowHeight[j] - d;
          RowHeight[i] := cMinTableCellHeight;
        end;
    end;

    
    // split by columns
    R := ATableCell.FCellRect;
    n := R.Right - R.Left + 1;
    if n < XPieces then
    begin
      d := Trunc((XPieces - n) / n + 0.999999);
      n := XPieces - n;
      i := 0;
      while n > 0 do
      begin
        if n - d < 0 then
          d := n;
        cs := ColWidth[R.Left + i];
        dCS := cs div (d + 1);
        ColWidth[R.Left + i] := cs - dCS * d;
        InsertCol(R.Left + i, d, dCS);
        Inc(i, d + 1);
        Dec(n, d);
      end;

      //correct columns size
      R := ATableCell.FCellRect;
      for i := R.Left to R.Right - 1 do
        if ColWidth[i] < cMinTableCellWidth then
        begin
          d := cMinTableCellWidth - ColWidth[i];
          for j := R.Left to R.Right do
            if ColWidth[j] - d > cMinTableCellWidth then
              ColWidth[j] := ColWidth[j] - d;
          ColWidth[i] := cMinTableCellWidth;
        end;
    end;

  finally
    FOwner.EnableResizing;
  end;
end;

procedure TrmTableInfo.InsertRow(AFirstRow: Integer; AQuantity: Integer = 1; EmptyRow: Boolean = False; ARowHeight: Integer = cMinTableCellHeight);
var
  R: TrmTableRowInfo;
  i: Integer;

  procedure UpdateCellsRect;
  var
    TC: TrmTableCell;
    i, TopCellInd, LeftCellInd: Integer;
    CellsToUpdate: TList;
    RowInfo: TrmTableRowInfo;
  begin
    CellsToUpdate := TList.Create;
    try
      //Make unique list of cells located beneath of splitting cells
      for i := 0 to TrmTableRowInfo(Items[AFirstRow]).Count - 1 do
      begin
        RowInfo := TrmTableRowInfo(Items[AFirstRow]);
        TC := TrmTableCell(RowInfo.Items[i]);
        TopCellInd := TC.FCellRect.Bottom + 1;

        while TopCellInd < Count do
        begin
          RowInfo := TrmTableRowInfo(Items[TopCellInd]);
          TC := TrmTableCell(RowInfo.Items[i]);
          if CellsToUpdate.IndexOf(TC) = -1 then
            CellsToUpdate.Add(TC);
          TopCellInd := TC.FCellRect.Bottom + 1;
        end;
      end;

      //and update CellRectInfo for them
      for i := 0 to CellsToUpdate.Count - 1 do
      begin
        TC := TrmTableCell(CellsToUpdate[i]);
        Inc(TC.FCellRect.Top, AQuantity);
        Inc(TC.FCellRect.Bottom, AQuantity);
      end;

      //Update CellRectInfo of splitting cells
      RowInfo := TrmTableRowInfo(Items[AFirstRow]);
      LeftCellInd := 0;
      while LeftCellInd < RowInfo.Count do
      begin
        TC := TrmTableCell(RowInfo.Items[LeftCellInd]);
        Inc(TC.FCellRect.Bottom, AQuantity);
        if EmptyRow then
          Inc(TC.FCellRect.Top, AQuantity);
        LeftCellInd := TC.FCellRect.Right + 1;
      end;

    finally
      FreeAndNil(CellsToUpdate);
    end;
  end;

begin
  if RowsCount = 0 then
  begin
    for i := 1 to AQuantity do
    begin
      R := TrmTableRowInfo.Create;
      Add(R);
      R.Add(nil);
      FRowHeights.Add(Pointer(ARowHeight));
      FColWidths.Add(Pointer(cMinTableCellWidth));
    end;
  end

  else
  begin
    if AFirstRow < Count then
      UpdateCellsRect;

    //add new rows
    for i := 1 to AQuantity do
    begin
      R := TrmTableRowInfo.Create;
      if EmptyRow then
        R.Count := ColsCount
      else
        R.Assign(TrmTableRowInfo(Items[AFirstRow]));
      Insert(AFirstRow, R);
      FRowHeights.Insert(AFirstRow, Pointer(ARowHeight));
    end;
  end;
end;

procedure TrmTableInfo.InsertCol(AFirstCol, AQuantity, AColWidth: Integer);
var
  i: Integer;

  procedure UpdateCellsFromRight;
  var
    TC: TrmTableCell;
    i, LeftCellInd: Integer;
    CellsToUpdate: TList;
    RowInfo: TrmTableRowInfo;
  begin
    CellsToUpdate := TList.Create;
    try
      //Make unique list of cells located from the left of splitting cells
      for i := 0 to RowsCount - 1 do
      begin
        RowInfo := TrmTableRowInfo(Items[i]);
        TC := TrmTableCell(RowInfo.Items[AFirstCol]);
        LeftCellInd := TC.FCellRect.Right + 1;

        while LeftCellInd < RowInfo.Count do
        begin
          TC := TrmTableCell(RowInfo.Items[LeftCellInd]);
          if CellsToUpdate.IndexOf(TC) = -1 then
            CellsToUpdate.Add(TC);
          LeftCellInd := TC.FCellRect.Right + 1;
        end;
      end;

      //and update CellRectInfo for them
      for i := 0 to CellsToUpdate.Count - 1 do
      begin
        TC := TrmTableCell(CellsToUpdate[i]);
        Inc(TC.FCellRect.Left, AQuantity);
        Inc(TC.FCellRect.Right, AQuantity);
      end;

    finally
      FreeAndNil(CellsToUpdate);
    end;
  end;

  procedure SplitAtomCols;
  var
    TC: TrmTableCell;
    i, j: Integer;
    RowInfo: TrmTableRowInfo;
  begin
    for i := 0  to RowsCount - 1 do
    begin
      RowInfo := TrmTableRowInfo(Items[i]);

      TC := TrmTableCell(RowInfo.Items[AFirstCol]);
      for j := 1 to AQuantity do
        RowInfo.Insert(AFirstCol, TC);

      if TC.FCellRect.Top = i then
        Inc(TC.FCellRect.Right, AQuantity);
    end;
  end;

begin
  UpdateCellsFromRight;
  SplitAtomCols;

  for i := 1 to AQuantity do
    FColWidths.Insert(AFirstCol, Pointer(AColWidth));
end;

function TrmTableInfo.GetColsCount: Integer;
begin
  if Count > 0 then
    Result := FColWidths.Count
  else
    Result := 0;
end;

function TrmTableInfo.GetRowsCount: Integer;
begin
  Result := Count;
end;

function TrmTableInfo.Cell(X, Y: Integer): TrmTableCell;
begin
  Result := TrmTableCell(TrmTableRowInfo(Items[Y])[X]);
end;

function TrmTableInfo.GetRowHeight(ARow: Integer): Integer;
begin
  Result := Integer(FRowHeights[ARow]);
end;

procedure TrmTableInfo.SetRowHeight(ARow: Integer; Value: Integer);
var
  i: Integer;
  C: TrmTableCell;
begin
  if (FSkipValueChecking or (Value <> Integer(FRowHeights[ARow]))) and  CheckRowConstrain(ARow, Value) then
  begin
    if not FOwner.ResizingDisabled then
    begin
      C := nil;
      for i := 0 to ColsCount - 1 do
        if C <> Cell(i, ARow) then
        begin
          C := Cell(i, ARow);
          C.BeforeChangePos;
        end;
    end;

    FRowHeights[ARow] := Pointer(Value);

    if not FOwner.ResizingDisabled then
    begin
      C := nil;
      for i := 0 to ColsCount - 1 do
        if C <> Cell(i, ARow) then
        begin
          C := Cell(i, ARow);
          C.AfterChangePos;
        end;
    end;
  end;
end;

function TrmTableInfo.GetColWidth(ACol: Integer): Integer;
begin
  Result := Integer(FColWidths[ACol]);
end;

procedure TrmTableInfo.SetColWidth(ACol: Integer; Value: Integer);
var
  i: Integer;
  C: TrmTableCell;
  OldSkipValueChecking: Boolean;
begin
  if (FSkipValueChecking or (Value <> Integer(FColWidths[ACol]))) and CheckColConstrain(ACol, Value) then
  begin
    if not FOwner.ResizingDisabled then
    begin
      C := nil;
      for i := 0 to RowsCount - 1 do
        if C <> Cell(ACol, i) then
        begin
          C := Cell(ACol, i);
          C.BeforeChangePos;
        end;
    end;

    FColWidths[ACol] := Pointer(Value);

//  Update Row Heights
    OldSkipValueChecking := FSkipValueChecking;
    FSkipValueChecking := True;
    try
      for i := RowsCount - 1 downto 0 do
        SetRowHeight(i, RowHeight[i]);
    finally
      FSkipValueChecking := OldSkipValueChecking;
    end;

    if not FOwner.ResizingDisabled then
    begin
      C := nil;
      for i := 0 to RowsCount - 1 do
        if C <> Cell(ACol, i) then
        begin
          C := Cell(ACol, i);
          C.AfterChangePos;
        end;
    end;
  end;
end;

procedure TrmTableInfo.MergeCells(X1, Y1, X2, Y2: Integer);
begin
  PackRows(y1, y2);
  PackColumns(x1, x2);
end;



procedure TrmTableInfo.DeleteRow(AFirstRow, AQuantity: Integer);
var
  i, j: Integer;
  R: TrmTableRowInfo;
begin
  for i := AFirstRow + AQuantity to RowsCount - 1 do
  begin
    R := TrmTableRowInfo(Items[i]);
    for j := 0 to R.Count - 1 do
      with TrmTableCell(R.Items[j]) do
      begin
        if FCellRect.Top = i  then
          Dec(FCellRect.Top, AQuantity);
        if FCellRect.Bottom = i then
          Dec(FCellRect.Bottom, AQuantity);
      end;
  end;

  for i := 1 to AQuantity do
    Delete(AFirstRow);

  if RowsCount = 0 then
    Clear
  else
    PackColumns(0, ColsCount - 1);
end;

procedure TrmTableInfo.Delete(Index: Integer);
begin
  TrmTableInfo(Items[Index]).Free;
  FRowHeights.Delete(Index);
  inherited;
end;

procedure TrmTableInfo.PackColumns(X1, X2: Integer);

  function ColEmpty(ACol: Integer): Boolean;
  var
    i: Integer;
  begin
    Result := True;
    for i := 0 to RowsCount - 1 do
    begin
      with Cell(ACol, i).FCellRect do
        if (Left = ACol) and (Right = ACol) then
        begin
          Result := False;
          break;
        end;
    end;
  end;

  procedure DeleteCol(ACol: Integer);
  var
    i, j: Integer;
    C: TrmTableCell;
  begin
    for i := 0 to RowsCount - 1 do
    begin
      for j := ACol to ColsCount - 1 do
      begin
        C := Cell(j, i);
        if (j <> ACol) and (C.FCellRect.Left = j) and (C.FCellRect.Top = i) then
          Dec(C.FCellRect.Left);
        if (C.FCellRect.Right = j) and (C.FCellRect.Top = i) then
          Dec(C.FCellRect.Right);
      end;

      TrmTableRowInfo(Items[i]).Delete(ACol);
    end;

    if ACol < ColsCount - 1 then
      i := ACol + 1
    else
      i := ACol - 1;
    ColWidth[i] := ColWidth[i] + ColWidth[ACol];

    FColWidths.Delete(ACol);
  end;

var
  i: Integer;

begin
  FOwner.DisableResizing;
  try
    i := x1;
    while i < x2 do
    begin
      if ColEmpty(i) then
      begin
        DeleteCol(i);
        Dec(x2);
      end
      else
        Inc(i);
    end;
  finally
    FOwner.EnableResizing;
  end;
end;

procedure TrmTableInfo.PackRows(Y1, Y2: Integer);

  function RowEmpty(ARow: Integer): Boolean;
  var
    i: Integer;
  begin
    Result := True;
    for i := 0 to ColsCount - 1 do
    begin
      with Cell(i, ARow).FCellRect do
        if (Top = ARow) and (Bottom = ARow) then
        begin
          Result := False;
          break;
        end;
    end;
  end;

  procedure DeleteRow(ARow: Integer);
  var
    i, j: Integer;
    C: TrmTableCell;
  begin
    for j := 0 to ColsCount - 1 do
    begin
      for i := ARow to RowsCount - 1 do
      begin
        C := Cell(j, i);
        if (i <> ARow) and (C.FCellRect.Top = i) and (C.FCellRect.Left = j) then
          Dec(C.FCellRect.Top);
        if (C.FCellRect.Bottom = i) and (C.FCellRect.Left = j) then
          Dec(C.FCellRect.Bottom);
      end;
    end;

    if ARow < RowsCount - 1 then
      i := ARow + 1
    else
      i := ARow - 1;
    RowHeight[i] := RowHeight[i] + RowHeight[ARow];

    Delete(ARow);
  end;

var
  i: Integer;

begin
  FOwner.DisableResizing;
  try
    i := y1;
    while i < y2 do
    begin
      if RowEmpty(i) then
      begin
        DeleteRow(i);
        Dec(y2);
      end
      else
        Inc(i);
    end;
  finally
    FOwner.EnableResizing;
  end;
end;

function TrmTableInfo.CheckColConstrain(ACol: Integer; var Value: Integer): Boolean;
var
  d, i, l, t, w, h, CellWidth, CellHeight: Integer;
  C: TrmTableCell;
  DsgMode: Boolean;
begin
  DsgMode := FOwner.IsDesignMode;

  if DsgMode then
  begin
    if Value < cMinTableCellWidth then
      Value := cMinTableCellWidth;
  end
  else
  begin
    if Value < 0 then
      Value := 0;
  end;

  d := Value - Integer(FColWidths[ACol]);

  if (FSkipValueChecking or (d <> 0)) and not FOwner.ResizingDisabled then
  begin
    C := nil;
    for i := 0 to RowsCount - 1 do
    begin
      if C = Cell(ACol, i) then
        Continue;

      C := Cell(ACol, i);
      CellWidth := C.Width;
      CellHeight := C.Height;
      l := 0;
      t := 0;
      w := CellWidth + d;
      h := CellHeight;
      C.CheckSizeConstrains(l, t, w, h);

      if (d <> 0) and (w - CellWidth = 0) then
      begin
        d := 0;
        break;
      end
      else
        if not DsgMode or (Integer(FColWidths[ACol]) + w - CellWidth >= cMinTableCellWidth) then
          d := w - CellWidth;
    end;
    Value := Integer(FColWidths[ACol]) + d;
    Result := d <> 0;
  end
  else
    Result := True;
end;

function TrmTableInfo.CheckRowConstrain(ARow: Integer; var Value: Integer): Boolean;
var
  d, i, l, t, w, h, CellWidth, CellHeight: Integer;
  C: TrmTableCell;
  DsgMode: Boolean;
begin
  DsgMode := FOwner.IsDesignMode;

  if DsgMode then
  begin
    if Value < cMinTableCellHeight then
      Value := cMinTableCellHeight;
  end
  else
  begin
    if Value < 0 then
      Value := 0;
  end;

  d := Value - Integer(FRowHeights[ARow]);
  if (FSkipValueChecking or (d <> 0)) and not FOwner.ResizingDisabled then
  begin
    C := nil;
    for i := 0 to ColsCount - 1 do
    begin
      if C = Cell(i, ARow) then
        Continue;

      C := Cell(i, ARow);
      CellWidth := C.Width;
      CellHeight := C.Height;
      l := 0;
      t := 0;
      w := CellWidth;
      h := CellHeight + d;
      C.CheckSizeConstrains(l, t, w, h);

      if (d <> 0) and ((h - CellHeight = 0) or (CellWidth <> w)) then
      begin
        d := 0;
        break;
      end
      else
        if not DsgMode or (Integer(FRowHeights[ARow]) + h - CellHeight >= cMinTableCellHeight) then
          d := h - CellHeight;
    end;
    Value := Integer(FRowHeights[ARow]) + d;
    Result := d <> 0;
  end
  else
    Result := True;
end;

procedure TrmTableInfo.RestoreState;
begin
  FRowHeights.Assign(FStoredRowHeights, laCopy);
  FColWidths.Assign(FStoredColWidths, laCopy);
end;

procedure TrmTableInfo.StoreState;
begin
  FStoredRowHeights.Assign(FRowHeights, laCopy);
  FStoredColWidths.Assign(FColWidths, laCopy);
end;


{ TrmCustomTable }

procedure TrmCustomTable.AfterChangePos;
begin
  inherited;
  SyncTagPos;
end;

procedure TrmCustomTable.AlignChildren;
begin
// do nothing
end;

function TrmCustomTable.BottomMostRow: Integer;
begin
  Result := FTableInfo.RowsCount - 1;
end;

function TrmCustomTable.CellAtPos(const APos: TPoint): TrmTableCell;
begin
  Result := nil;
end;

function TrmCustomTable.CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean;
//var
//  d, h: Integer;
begin
  Result := inherited CheckSizeConstrains(ALeft, ATop, AWidth, AHeight);
{
  if not Assigned(FTableInfo) then
    Exit;

  h := Height;
  d := FTableInfo.RowHeight[BottomMostRow] + (AHeight - h);
  if FTableInfo.CheckRowConstrain(BottomMostRow, d) then
    AHeight := h - FTableInfo.RowHeight[BottomMostRow] + d
  else
  begin
    AHeight := h;
    Result := False;
  end;

  h := Width;
  d := FTableInfo.ColWidth[RightMostCol] + (AWidth - h);
  if FTableInfo.CheckColConstrain(RightMostCol, d) then
    AWidth := h - FTableInfo.ColWidth[RightMostCol] + d
  else
  begin
    AWidth := h;
    Result := False;
  end;
}
end;

constructor TrmCustomTable.Create(AOwner: TComponent);
begin
  inherited;
  FSubChildrenNamesUnique := True;
  FObjectTagClass := TrmdTableTag;
  InitializeTableInfo;
end;

procedure TrmCustomTable.DefineProperties(Filer: TFiler);

  function NeedToWrite: Boolean;
  var
    W: TWriter;
    T: TrmCustomTable;
  begin
    Result := True;

    if IsLibComponent or IsInheritedComponent and (Filer is TWriter) then
    begin
      W := TWriter(Filer);
      if W.Ancestor is TrmCustomTable then
      begin
        T := TrmCustomTable(W.Ancestor);

        Result := (FTableInfo.FColWidths.Count <> T.FTableInfo.FColWidths.Count) or
                  (FTableInfo.FRowHeights.Count <> T.FTableInfo.FRowHeights.Count) or
                   not CompareMem(FTableInfo.FColWidths.List, T.FTableInfo.FColWidths.List, FTableInfo.FColWidths.Count * SizeOf(Pointer)) or
                   not CompareMem(FTableInfo.FRowHeights.List, T.FTableInfo.FRowHeights.List, FTableInfo.FRowHeights.Count * SizeOf(Pointer));
      end;
    end;
  end;

begin
  inherited;
  if FTableInfo.FOwner = Self then
    Filer.DefineBinaryProperty('GridInfo', ReadGridInfo, WriteGridInfo, NeedToWrite);
end;

function TrmCustomTable.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.AcceptControls := False;
  Result.AcceptedClases := '';
end;

destructor TrmCustomTable.Destroy;
begin
  FreeAndNil(FRenderedBorderLines);

  inherited;

  if FTableInfo.FOwner = Self then
    FreeAndNil(FTableInfo);
end;

procedure TrmCustomTable.DisableResizing;
begin
  Inc(FResizingDsblCount);
end;

procedure TrmCustomTable.DoPrint;
begin
  inherited;

  if Assigned(FRenderedBorderLines) then
    FRenderedBorderLines.CreateRWAObjects;
end;

procedure TrmCustomTable.EnableResizing;
begin
  if FResizingDsblCount = 0 then
    Exit;
  Dec(FResizingDsblCount);
end;

function TrmCustomTable.FindCellPosRectangle(const ACornerCell1, ACornerCell2: TrmTableCell): TRect;
var
  C: TrmTableCell;
  CellRect: TRect;
begin
  CellRect := FindCellRectangle(ACornerCell1, ACornerCell2);
  C := TableInfo.Cell(CellRect.Left, CellRect.Top);
  Result.Left := C.Left;
  Result.Top := C.Top;
  C := TableInfo.Cell(CellRect.Right, CellRect.Bottom);
  Result.Right := C.Left + C.Width;
  Result.Bottom := C.Top + C.Height;
end;

function TrmCustomTable.FindCellRectangle(const ACornerCell1, ACornerCell2: TrmTableCell): TRect;
begin
  Result.Top := Min(ACornerCell1.CellRect.Top, ACornerCell2.CellRect.Top);
  Result.Bottom := Max(ACornerCell1.CellRect.Bottom, ACornerCell2.CellRect.Bottom);
  Result.Left := Min(ACornerCell1.CellRect.Left, ACornerCell2.CellRect.Left);
  Result.Right := Max(ACornerCell1.CellRect.Right, ACornerCell2.CellRect.Right);
end;

function TrmCustomTable.GetCellsInRect(const ACornerCell1, ACornerCell2: TrmTableCell): TrmListOfCells;
var
  R: TRect;
  i, j: Integer;
  L: TList;
  C: TrmTableCell;
begin
  L := TList.Create;
  try
    R := FindCellRectangle(ACornerCell1, ACornerCell2);
    for i := R.Top to R.Bottom do
      for j := R.Left to R.Right do
      begin
        C := TableInfo.Cell(j, i);
        if (R.Top <= C.CellRect.Top) and (R.Left <= C.CellRect.Left) and (R.Bottom >= C.CellRect.Bottom) and (R.Right >= C.CellRect.Right) and
           (L.IndexOf(C) = -1) then
          L.Add(C);
      end;

    SetLength(Result, L.Count);
    for i := Low(Result) to High(Result) do
      Result[i] := TrmTableCell(L[i]);

  finally
    FreeAndNil(L);
  end;
end;

function TrmCustomTable.GetPosition(Index: Integer): Integer;

  function GetWidth: Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := LeftMostCol to RightMostCol do
      Inc(Result, TableInfo.ColWidth[i]);

    if (Result = 0) and IsDesignMode then
      Result := 400;
  end;

  function GetHeight: Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := TopMostRow to BottomMostRow do
      Inc(Result, TableInfo.RowHeight[i]);

    if (Result = 0) and IsDesignMode then
      Result := 200;
  end;

begin
  Result := 0;
  if Index in [0, 1] then
    Result := inherited GetPosition(Index)

  else if Assigned(FTableInfo) then
    case Index of
      2: Result := GetWidth;
      3: Result := GetHeight;
    end
end;

function TrmCustomTable.LeftMostCol: Integer;
begin
  Result := 0;
end;

procedure TrmCustomTable.Loaded;
begin
  inherited;
  UpdateCellTableInfo;
  PropogateGlobVarsToChildren;
end;

procedure TrmCustomTable.Notify(AEvent: TrmDesignEventType);
var
  SelObjects: TrmListOfObjects;
  PrevSelCells: TrmListOfCells;

  procedure InvalidateSelectedCells(ACells: PTrmListOfCells);
  var
    i: Integer;
  begin
    for i := Low(ACells^) to High(ACells^) do
      ACells^[i].Invalidate;
  end;

begin
  if AEvent in [rmdSelectChildObject] then
    ShowTag
  else if AEvent in [rmdDestroyChildResizer] then
    HideTag;

  SelObjects := nil;
  if AEvent in [rmdSelectChildObject, rmdDestroyChildResizer] then
  begin
    if GetDesigner <> nil then
    begin
      PrevSelCells := FCurrentSelectedCells;
      SetLength(FCurrentSelectedCells, 0);
      InvalidateSelectedCells(@PrevSelCells);

      SelObjects := GetDesigner.GetSelectedObjects;
      if (Length(SelObjects) = 2) and (SelObjects[0].ObjectInheritsFrom('TrmTableCell')) and (TrmTableCell(SelObjects[0].RealObject).Owner = Self) then
      begin
        FCurrentSelectedCells := GetCellsInRect(TrmTableCell(SelObjects[0].RealObject), TrmTableCell(SelObjects[1].RealObject));
        InvalidateSelectedCells(@FCurrentSelectedCells);
      end;
    end;
  end;

  inherited;
end;

procedure TrmCustomTable.PaintForeground(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  if BottomMostRow = -1 then
  begin
    R := ZoomedBoundsRect(ACanvas);
    OffsetRect(R, -R.Left, -R.Top);
    with ACanvas do
    begin
      Pen.Width := 1;
      Pen.Mode := pmCopy;
      Pen.Color := clGray;
      Pen.Style := psDot;
      Rectangle(R);
    end;
  end;  
end;

procedure TrmCustomTable.ReadGridInfo(Stream: TStream);
var
  P: PPointerList;
  cc, rc: Byte;
  i: Integer;
  R: TrmTableRowInfo;
begin
  FTableInfo.Clear;

  Stream.Read(cc, SizeOf(cc));
  Stream.Read(rc, SizeOf(rc));
  FTableInfo.Capacity := rc;
  for i := 1 to rc  do
  begin
    R := TrmTableRowInfo.Create;
    R.Count := cc;
    FTableInfo.Add(R);
  end;
  FTableInfo.FColWidths.Count := cc;
  FTableInfo.FRowHeights.Count := rc;

  P := FTableInfo.FColWidths.List;
  Stream.Read(P^, FTableInfo.ColsCount * SizeOf(Pointer));
  P := FTableInfo.FRowHeights.List;
  Stream.Read(P^, FTableInfo.RowsCount * SizeOf(Pointer));
end;

function TrmCustomTable.RenderedBorderLines: TrmRenderedBorderLines;
begin
  if not Assigned(FRenderedBorderLines) then
    FRenderedBorderLines := TrmRenderedBorderLines.Create(Self);

  Result := FRenderedBorderLines;
end;

function TrmCustomTable.ResizingDisabled: Boolean;
begin
  Result := FResizingDsblCount > 0;
end;

function TrmCustomTable.RightMostCol: Integer;
begin
  Result := FTableInfo.ColsCount - 1;
end;

procedure TrmCustomTable.SetPosition(Index, Value: Integer);
var
  w, h: Integer;
begin
  if Index in [0, 1] then
    inherited
  else if Assigned(FTableInfo) then
  begin
    w := Width;
    h := Height;
    case Index of
      2: w := Value;
      3: h := Value;
    end;
    SetBounds(Left, Top, w, h);
  end;
end;

procedure TrmCustomTable.SetTableHeight(const ANewHeight: Integer);
var
  i, h: Integer;
  dHeight: Integer;
begin
  dHeight := ANewHeight - Height;
  for i := BottomMostRow downto TopMostRow do
  begin
    if dHeight = 0 then
      Break;
    h := FTableInfo.GetRowHeight(i);
    FTableInfo.SetRowHeight(i, h + dHeight);
    Dec(dHeight, FTableInfo.GetRowHeight(i) - h);
  end;
end;

procedure TrmCustomTable.SetTableWidth(const ANewWidth: Integer);
var
  i, w: Integer;
  dWidth: Integer;
begin
  dWidth := ANewWidth - Width;
  for i := RightMostCol downto LeftMostCol do
  begin
    if dWidth = 0 then
      Break;
    w := FTableInfo.GetColWidth(i);
    FTableInfo.SetColWidth(i, w + dWidth);
    Dec(dWidth, FTableInfo.GetColWidth(i) - w);
  end;
end;

function TrmCustomTable.TopMostRow: Integer;
begin
  Result := 0;
end;

procedure TrmCustomTable.WriteGridInfo(Stream: TStream);
var
  n: Byte;
begin
  n := FTableInfo.ColsCount;
  Stream.Write(n, SizeOf(n));
  n := FTableInfo.RowsCount;
  Stream.Write(n, SizeOf(n));
  Stream.Write(FTableInfo.FColWidths.List^, FTableInfo.ColsCount * SizeOf(Pointer));
  Stream.Write(FTableInfo.FRowHeights.List^, FTableInfo.RowsCount * SizeOf(Pointer));
end;


  { TrmTable }

constructor TrmTable.Create(AOwner: TComponent);
begin
  inherited;
  FVirtOwner := True;

  FCellClass := 'TrmTableCell';

  FChanging := False;
  Color := clNone;
  FFont := TrwFont.Create(Self);
  Font.Name := 'Arial';
  Font.Height := -83;
  Font.Color := clBlack;
  FFont.OnChange := OnChangeFont;
  FApplyingBorderChanges := False;
  MultiPaged := False;
end;

destructor TrmTable.Destroy;
begin
  FreeAndNil(FFont);
  inherited;
end;

procedure TrmTable.ReadState(Reader: TReader);
var
  PrevOwner: TComponent;
  PrevRoot: TComponent;
  PrevParent: TComponent;
  i: Integer;
begin
  for i := ComponentCount - 1 downto 0 do
    if (Components[i] is TrmTableCell) and not TrmTableCell(Components[i]).InheritedComponent then
      Components[i].Free;

  PrevOwner := Reader.Owner;
  PrevParent := Reader.Parent;
  PrevRoot := Reader.Root;
  Reader.Owner := Self;
  Reader.Root := Self;
  Reader.Parent := nil;
  inherited;
  Reader.Owner := PrevOwner;
  Reader.Root := PrevRoot;
  Reader.Parent := PrevParent;
end;

procedure TrmTable.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  BeforeChangePos;
  CheckSizeConstrains(ALeft, ATop, AWidth, AHeight);
  FLeft := ALeft;
  FTop := ATop;
  if Assigned(FTableInfo) then
  begin
    SetTableWidth(AWidth);
    SetTableHeight(AHeight);
  end;
  AfterChangePos;
end;

procedure TrmTable.SplitCell(const ACell: TrmTableCell; const ACols, ARows: Integer);
var
  i, j, dx, dy: Integer;
//  k: Integer;
//  s: String;
  R: TRect;
  C: TrmTableCell;
  NewCells: array of array of TrmTableCell;
  CellClassName: string;
begin
  if (ACell.Width div ACols < cMinTableCellWidth) or (ACell.Height div ARows < cMinTableCellHeight) then
    raise ErwException.Create('Cannot split table cell "' + ACell.Name + '". It is to small. Please make it bigger.');

  CellClassName:=(ACell as IrmDesignObject).GetClassName;

  DisableResizing;
  ACell.DisableAlign;
  try
    //prepare Table Cell Info
    FTableInfo.SplitCell(ACell, ACols, ARows);

   // create new cells
{    k := 1;
    for i := 0 to ComponentCount - 1 do
    begin
      s := Components[i].Name;
      if (s = '') or (s[1] <> 'C') then
        continue;
      Delete(s, 1, 1);
      Val(s, j, dx);
      if (dx = 0) and (j > k) then
        k := j;
    end;
    Inc(k);
}
    SetLength(NewCells, ARows, ACols);
    NewCells[0, 0] := ACell;
    for i := 0 to ARows - 1 do
      for j := 0 to ACols - 1 do
      begin
        if not ((i = 0) and (j = 0)) then
        begin
          NewCells[i, j] := TrmTableCell(CreateRWComponentByClass(RWEngineExt.AppAdapter.SubstDefaultClassName(CellClassName), True).RealObject);
          C := NewCells[i, j];
          ACell.FTable.InsertComponent(C);
          C.FTable := ACell.FTable;
          C.Container := Self;
          C.Assign(ACell);
          C.Name := GetUniqueNameForComponent(C, True, 'C', '', True);
          C.Description := 'Cell';
//          C.Name := 'C' + IntToStr(k);
//          Inc(k);
        end;
      end;

    R := ACell.FCellRect;
    dx := (R.Right - R.Left + 1) div ACols;
    dy := (R.Bottom - R.Top + 1) div ARows;

    // update Table Cell Info
    for i := 0 to ARows - 1 do
      for j := 0 to ACols - 1 do
        with NewCells[i, j] do
        begin
          with FCellRect do
          begin
            Top := R.Top + i * dy;
            Left := R.Left + j * dx;

            if j = ACols - 1 then
              Right := R.Right
            else
              Right := Left + (dx - 1);

            if i = ARows - 1 then
              Bottom := R.Bottom
            else
              Bottom := Top + (dy - 1);

            FTableInfo.LinkTableCell(NewCells[i, j], Left, Top, Right, Bottom);
          end;

          if Self.IsDesignMode then
            SetDesigner(Self.GetDesigner);

          Container := Self;
        end;

  finally
    EnableResizing;
    ACell.EnableAlign;
  end;

  ACell.CheckAutosize;
  ACell.DoAlign;
  AfterChangeCellLayout;
end;

function TrmTable.MergeCells(const ACornerCell1: TrmTableCell; const ACornerCell2: TrmTableCell): TrmTableCell;
var
  i, j, dx, dy, ltcX, ltcY: Integer;
  C: TrmTableCell;
  Chld: TrmPrintControl;
  L: TList;
  CellRect: TRect;
begin
  CellRect := FindCellRectangle(ACornerCell1, ACornerCell2);

  for i := CellRect.Top to CellRect.Bottom do
    for j := CellRect.Left to CellRect.Right do
    begin
      C := FTableInfo.Cell(j, i);
      if not((C.CellRect.Left >= CellRect.Left) and (C.CellRect.Right <= CellRect.Right) and
             (C.CellRect.Top >= CellRect.Top) and (C.CellRect.Bottom <= CellRect.Bottom)) then
        raise ErwException.Create('Cannot merge cells! Selected cells do not constitute a rectangle area!');
    end;


  Result := nil;
  for i := CellRect.Top to CellRect.Bottom do
    for j := CellRect.Left to CellRect.Right do
    begin
      C := FTableInfo.Cell(j, i);
      if (C <> Result) and C.InheritedComponent then
      begin
        if Assigned(Result) then
          raise ErwException.Create('Cannot merge inherited cells!');
        Result := C;
      end;
    end;

  if not Assigned(Result) then
    Result := ACornerCell1;


  DisableResizing;
  Result.DisableAlign;
  try
    L := TList.Create;
    try
      for i := CellRect.Top to CellRect.Bottom do
        for j := CellRect.Left to CellRect.Right do
        begin
          C := FTableInfo.Cell(j, i);
          if C <> Result then
            if L.IndexOf(C) = -1 then
              L.Add(C);
        end;

      C := FTableInfo.Cell(CellRect.Left, CellRect.Top);
      ltcX := C.Left;
      ltcY := C.Top;
      for i := 0 to L.Count - 1 do
      begin
        C := TrmTableCell(L[i]);

        if Assigned(C.SubTable) then
          C.DestroySubTable;

        dx := C.Left - ltcX;
        dy := C.Top - ltcY;
        for j := C.ControlCount -1 downto 0 do
        begin
          Chld := C.Controls[j];
          Chld.Container := nil;
          if Assigned(Result.SubTable) then
            Chld.Free
          else
          begin
            Chld.Left := Chld.Left + dx;
            Chld.Top := Chld.Top + dy;
            Chld.Container := Result;
          end;
        end;

        C.Free;
      end;

    finally
      FreeAndNil(L);
    end;

    FTableInfo.LinkTableCell(Result, CellRect.Left, CellRect.Top, CellRect.Right, CellRect.Bottom);
    FTableInfo.MergeCells(CellRect.Left, CellRect.Top, CellRect.Right, CellRect.Bottom);
  finally
    EnableResizing;
    Result.EnableAlign;
  end;

  Result.DoAlign;
  AfterChangeCellLayout;
end;

procedure TrmTable.SetColor(Value: TColor);
var
  i: Integer;
  Clr: TColor;
begin
  Clr := Color;
  inherited;

  for i := 0 to ControlCount - 1 do
    if TrmTableCell(Controls[i]).Color = Clr then
      TrmTableCell(Controls[i]).Color := Value;
end;

procedure TrmTable.WriteState(Writer: TWriter);
var
  OldRoot, OldRootAncestor: TComponent;
begin
  OldRootAncestor := Writer.RootAncestor;
  OldRoot := Writer.Root;
  try
    if Assigned(OldRootAncestor) then
      Writer.RootAncestor := OldRootAncestor.FindComponent(Name);
    if not Assigned(Writer.RootAncestor) then
      Writer.RootAncestor := Self;
    Writer.Root := Self;
    inherited;
  finally
    Writer.Root := OldRoot;
    Writer.RootAncestor := OldRootAncestor;
  end;
end;


procedure TrmTable.Loaded;
begin
  inherited;
  AfterChangeCellLayout;
end;

procedure TrmTable.OnChangeFont(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to ControlCount - 1 do
    if TrmTableCell(Controls[i]).FTableFont then
    begin
      TrmTableCell(Controls[i]).Font := Font;
      TrmTableCell(Controls[i]).FTableFont := True;
    end
    else
      TrmTableCell(Controls[i]).FTableFont := Font.TheSame(TrmTableCell(Controls[i]).Font);
end;

procedure TrmTable.GetCellRect(ACell1, ACell2: TrmTableCell; ARectCells: TList);
var
  x1, y1, x2, y2, i, j, n: Integer;
  C1, C2: TrmTableCell;

  function CheckLeftBound(ACell: TrmTableCell): Boolean;
  var
    C: TrmTableCell;
  begin
    Result := True;
    C := ACell;
    while (C.FCellRect.Bottom < y2) and (C.FCellRect.Bottom < BottomMostRow) do
    begin
      C := FTableInfo.Cell(C.FCellRect.Left, C.FCellRect.Bottom + 1);
      if C.FCellRect.Left <> x1  then
      begin
        Result := False;
        break;
      end;
    end;
  end;

  function CheckRightBound(ACell: TrmTableCell): Boolean;
  var
    C: TrmTableCell;
  begin
    Result := True;
    C := ACell;
    while (C.FCellRect.Bottom < y2) and (C.FCellRect.Bottom < BottomMostRow) do
    begin
      C := FTableInfo.Cell(C.FCellRect.Right, C.FCellRect.Bottom + 1);
      if C.FCellRect.Right <> x2  then
      begin
        Result := False;
        break;
      end;
    end;
  end;

  function CheckTopBound(ACell: TrmTableCell): Boolean;
  var
    C: TrmTableCell;
  begin
    Result := True;
    C := ACell;
    while (C.FCellRect.Right < x2) and (C.FCellRect.Right < RightMostCol) do
    begin
      C := FTableInfo.Cell(C.FCellRect.Right + 1, C.FCellRect.Top);
      if C.FCellRect.Top <> y1  then
      begin
        Result := False;
        break;
      end;
    end;
  end;

  function CheckBottomBound(ACell: TrmTableCell): Boolean;
  var
    C: TrmTableCell;
  begin
    Result := True;
    C := ACell;
    while (C.FCellRect.Right < x2) and (C.FCellRect.Right < RightMostCol) do
    begin
      C := FTableInfo.Cell(C.FCellRect.Right + 1, C.FCellRect.Bottom);
      if C.FCellRect.Bottom <> y2  then
      begin
        Result := False;
        break;
      end;
    end;
  end;

begin
  if ACell1.FCellRect.Left < ACell2.FCellRect.Left then
    x1 := ACell1.FCellRect.Left
  else
    x1 := ACell2.FCellRect.Left;

  if ACell1.FCellRect.Top < ACell2.FCellRect.Top then
    y1 := ACell1.FCellRect.Top
  else
    y1 := ACell2.FCellRect.Top;

  if ACell1.FCellRect.Right > ACell2.FCellRect.Right then
    x2 := ACell1.FCellRect.Right
  else
    x2 := ACell2.FCellRect.Right;

  if ACell1.FCellRect.Bottom > ACell2.FCellRect.Bottom then
    y2 := ACell1.FCellRect.Bottom
  else
    y2 := ACell2.FCellRect.Bottom;

  if CheckLeftBound(FTableInfo.Cell(x1, y1)) and
     CheckRightBound(FTableInfo.Cell(x2, y1)) and
     CheckTopBound(FTableInfo.Cell(x1, y1)) and
     CheckBottomBound(FTableInfo.Cell(x2, y1)) then
  begin
    ARectCells.Count := (x2 - x1 + 1) * (y2 - y1 +1);
    ARectCells[0] := FTableInfo.Cell(x1, y1);
    C2 := FTableInfo.Cell(x2, y2);
    n := 1;
    for i := y1 to y2 do
      for j := x1 to x2 do
      begin
        C1 := FTableInfo.Cell(j, i);
        if (C1 <> C2) and (ARectCells.IndexOf(C1) = -1) then
        begin
          ARectCells[n] := C1;
          Inc(n);
        end;
      end;
    ARectCells[n] := C2;
    ARectCells.Count := n + 1;
  end
  else
    ARectCells.Clear;
end;

procedure TrmTable.InitializeTableInfo;
begin
  FTableInfo := TrmTableInfo.Create;
  FTableInfo.FOwner := Self;
end;

procedure TrmTable.CreateInitialCell;
var
  C: TrmTableCell;
  R: TRect;

  procedure SetBorderLine(BL: TrwBorderLine);
  begin
    BL.Visible := True;
    BL.Color := clBlack;
    BL.Width := 3;
    BL.Style := psSolid;
  end;

begin
  C := TrmTableCell(CreateRWComponentByClass(RWEngineExt.AppAdapter.SubstDefaultClassName(FCellClass), True).RealObject);
  AddChildObject(C);
  C.Name := GetUniqueNameForComponent(C, True, 'C', '', True);
  C.Description := 'Cell';
  R := ClientRect;

  C.BorderLines.BeginUpdate;
  try
    SetBorderLine(C.BorderLines.TopLine);
    SetBorderLine(C.BorderLines.BottomLine);
    SetBorderLine(C.BorderLines.LeftLine);
    SetBorderLine(C.BorderLines.RightLine);
  finally
    C.BorderLines.EndUpdate;
  end;

  AfterChangeCellLayout;
end;

procedure TrmTable.AfterConstruction;
begin
  inherited;
end;


procedure TrmTable.AfterChangeCellLayout;
begin
  AfterChangePos;
end;

function TrmTable.HasGapFromBottom: Boolean;
begin
  Result := BottomMostRow = TableInfo.RowsCount - 1;
end;

function TrmTable.HasGapFromRight: Boolean;
begin
  Result := RightMostCol = TableInfo.ColsCount - 1;
end;


procedure TrmTable.InitializeForDesign;
begin
  inherited;
  if ControlCount = 0 then
  begin
    CreateInitialCell;
    Width := 1000;
    Height := 500;
  end;
end;

procedure TrmTable.CreateRWAObject;
begin
  if Color <> clNone then
    Renderer.AddFrame(PrintRectOnCurrPage, Compare, LayerNumber, Color, [], clBlack, 1, psSolid);
end;


procedure TrmTable.StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean);
begin
  inherited;
  StoreFont(AStream, Font);
end;

procedure TrmTable.ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean);
begin
  inherited;
  ReStoreFont(AStream, Font);
end;

procedure TrmTable.SetMultiPaged(const Value: Boolean);
begin
  //always False
  inherited SetMultiPaged(False);
end;

procedure TrmTable.PropogateGlobVarsToChildren;
var
  i: Integer;
begin
  if Assigned(GlobalVarFunc) then
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TrmDBTable then
      begin
        TrmDBTable(Components[i]).SetExternalGlobalVarFunc(GlobalVarFunc);
        TrmDBTable(Components[i]).PropogateGlobVarsToChildren;
      end;
end;


procedure TrmTable.UpdateCellTableInfo;
var
  i: Integer;
begin
  for i := 0 to ControlCount - 1 do
    TrmTableCell(Controls[i]).UpdateTableInfo;
end;

procedure TrmTable.SyncTagPos;
var
  i: Integer;
begin
  inherited;

  for i := 0 to ControlCount - 1 do
    TrmTableCell(Controls[i]).SyncTableTagPos;
end;

function TrmTable.CellAtPos(const APos: TPoint): TrmTableCell;
var
  i: Integer;
  C: TrmTableCell;
begin
  Result := nil;
  for i := ControlCount - 1  downto 0 do
  begin
    C := TrmTableCell(Controls[i]);
    if PtInRect(C.BoundsRect, APos) then
    begin
      Result := C;
      break;
    end;
  end;
end;

procedure TrmTable.DoCalculate;
var
  i: Integer;
begin
  inherited;

  for i := 0 to ControlCount - 1 do
    if Assigned(TrmTableCell(Controls[i]).SubTable) then
      TrmTableCell(Controls[i]).SubTable.SBTAlignBands;
end;

function TrmTable.MergeCells(const ACornerCell1Name, ACornerCell2Name: String): IrmTableCell;
var
  C1, C2: TrmTableCell;
begin
  C1 := TrmTableCell(FindComponent(ACornerCell1Name));
  C2 := TrmTableCell(FindComponent(ACornerCell2Name));
  Result := MergeCells(C1, C2);
end;

function TrmTable.GetCellsInRect(const ACornerCell1, ACornerCell2: IrmTableCell): TrmListOfObjects;
var
  i: Integer;
  Cells: TrmListOfCells;
begin
  Cells := GetCellsInRect(TrmTableCell(ACornerCell1.RealObject), TrmTableCell(ACornerCell2.RealObject));

  SetLength(Result, Length(Cells));
  for i := Low(Cells) to High(Cells) do
    Result[i] := Cells[i];
end;

procedure TrmTable.ClearTable;
var
  i: Integer;
begin
  if BottomMostRow = -1 then
    Exit;

  for i := 0 to ControlCount - 1 do
   if TrmTableCell(Controls[i]).IsInheritedComponent then
     raise ErwException.Create('Cannot delete inherited cells!');

  DisableResizing;
  try
    i := ComponentCount - 1;
    while i >= 0 do
    begin
      if Components[i] is TrmTableCell then
        Components[i].Free;
      Dec(i);
    end;

{    if FTableInfo.FOwner = Self then
      FTableInfo.Clear
    else if BottomMostRow <> -1
}
    FTableInfo.DeleteRow(TopMostRow, BottomMostRow - TopMostRow + 1);

    UpdateCellBoundsInfo;

  finally
    EnableResizing;
  end;
  AfterChangeCellLayout;
end;

procedure TrmTable.UpdateCellBoundsInfo;
begin
 // empty
end;

procedure TrmTable.AddChildObject(AObject: IrmDesignObject);
var
  btm: Integer;
begin
  inherited;
  if AObject.RealObject is TrmTableCell then
  begin
    if BottomMostRow = -1 then
      btm := TopMostRow
    else
      btm := BottomMostRow;

    TrmTableCell(AObject.RealObject).FTable := Self;
    FTableInfo.InsertRow(btm, 1, True);
    FTableInfo.LinkTableCell(TrmTableCell(AObject.RealObject), LeftMostCol, btm, RightMostCol, btm);
    TrmTableCell(AObject.RealObject).Container := Self;
  end;
end;


procedure TrmTable.RemoveChildObject(AObject: IrmDesignObject);
begin
  inherited;
  if AObject.RealObject is TrmTableCell then
    if (TopMostRow = 1) and (BottomMostRow = 1) and (LeftMostCol = 1) and (RightMostCol = 1) then
    begin
      TrmTableCell(AObject.RealObject).Container := nil;
      TrmTableCell(AObject.RealObject).FTable := nil;
      FTableInfo.Clear;
    end
    else
      Assert(False);
end;

{TrmTableCell }

constructor TrmTableCell.Create(AOwner: TComponent);
begin
  FValueHolder := TrmsValueHolder.Create(Self);
  inherited;
  FTable := TrmTable(AOwner);

  FInnerMarginLeft := cDefaultCellBorderSize;
  FInnerMarginRight := cDefaultCellBorderSize;
  FInnerMarginTop := cDefaultCellBorderSize;
  FInnerMarginBottom := cDefaultCellBorderSize;

  FFont := TrwFont.Create(Self);

  if Assigned(FTable) then
  begin
    Font := FTable.Font;
    Color := FTable.Color;
    FTableFont := True;    
  end
  else
  begin
    Font.Name := 'Arial';
    Font.Height := -83;
    Font.Color := clBlack;
    Color := clNone;
    FTableFont := False;
  end;

  FFont.OnChange := OnChangeFont;

  FAutoHeight := True;
  FWordWrap := True;
  Alignment := taLeftJustify;
  Layout := tlTop;
  MultiPaged := False;
  BlockParentIfEmpty := True;

  FBorderLines := TrwBorderLines.Create(Self);
  FBorderLines.OnChange := OnChangeBorderLines;

  FForecastWidth := 0;
  FAllowedResizingBounds := [rmdMPLeftBound, rmdMPRightBound, rmdMPTopBound, rmdMPBottomBound];
end;

destructor TrmTableCell.Destroy;
begin
  FreeAndNil(FFont);
  FreeAndNil(FValueHolder);  
  inherited;
end;

procedure TrmTableCell.Assign(Source: TPersistent);
begin
  inherited;
  Color := TrmTableCell(Source).Color;
  Font := TrmTableCell(Source).Font;
  Alignment := TrmTableCell(Source).Alignment;
  Layout := TrmTableCell(Source).Layout;
  BorderLines := TrmTableCell(Source).BorderLines;
  FTableFont := TrmTableCell(Source).FTableFont;
end;

procedure TrmTableCell.DefineProperties(Filer: TFiler);

  function NeedToWrite: Boolean;
  var
    W: TWriter;
    C: TrmTableCell;
    R1,R2: TRect;
  begin
    Result := True;

    if IsInheritedComponent and (Filer is TWriter) then
    begin
      W := TWriter(Filer);
      if W.Ancestor is TrmTableCell then
      begin
        C := TrmTableCell(W.Ancestor);
        R1 := C.CellRect;
        R2 := CellRect;
        Result := not EqualRect(R1, R2);
      end;
    end;
  end;

begin
  inherited;
  Filer.DefineProperty('Size', ReadSize, WriteSize, NeedToWrite);
end;

procedure TrmTableCell.ReadSize(Reader: TReader);
var
  p: DWORD;
  w: Word;
begin
  p := Reader.ReadInteger;
  w := HiWord(p);
  FCellRect.Left := HiByte(w);
  FCellRect.Top :=  LoByte(w);
  w := LoWord(p);
  FCellRect.Right := HiByte(w);
  FCellRect.Bottom := LoByte(w);
end;

procedure TrmTableCell.WriteSize(Writer: TWriter);
var
  p: DWORD;
begin
  p := MakeLong(MakeWord(Byte(FCellRect.Bottom), Byte(FCellRect.Right)),
                MakeWord(Byte(FCellRect.Top), Byte(FCellRect.Left)));
  Writer.WriteInteger(p);
end;

procedure TrmTableCell.SetCellSize(X, Y, AWidth, AHeight: Integer);
begin
  if (AWidth <> Table.TableInfo.ColWidth[CellRect.Right]) or (AHeight <> Table.TableInfo.RowHeight[CellRect.Bottom]) then
  begin
    if AWidth <> Table.TableInfo.ColWidth[CellRect.Right] then
      Table.TableInfo.ColWidth[CellRect.Right] := AWidth;

    if AHeight <> Table.TableInfo.RowHeight[CellRect.Bottom] then
      Table.TableInfo.RowHeight[CellRect.Bottom] := AHeight;
  end;
end;

procedure TrmTableCell.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  if Loading then
    Exit;

  SetCellSize(FCellRect.Right, FCellRect.Bottom,
    FTable.TableInfo.ColWidth[FCellRect.Right] + (AWidth - Width),
    FTable.FTableInfo.RowHeight[FCellRect.Bottom] + (AHeight - Height));
end;

procedure TrmTableCell.SetValue(const AValue: Variant);
begin
  if SubTable <> nil then
    Exit;

  FValueHolder.Value := AValue;

  if RendStatus = rmRWARSCalculated then
    PrepareTextForPrinting;

  CheckAutosize;
  Invalidate;
end;

procedure TrmTableCell.SetFormat(const Value: String);
begin
  FFormat := Value;
  Invalidate;
end;

function TrmTableCell.GetText: String;
var
  Rec: TrmDataFieldInfoRec;

  function SimpleText: String;
  begin
    Result := FormatValue(FFormat, Value);
    if (FormulaTextFilter <> nil) and (Renderer <> nil) then
      Result := FormulaTextFilter.Calculate([Result]);
  end;

begin
  if IsDesignMode and (DataField <> '') then
  begin
    Rec := GetDataFieldInfo;
    if Assigned(Rec.QBField) then
      Result := '[' + Rec.QBField.GetName + ']'
    else
      Result := '[' + DataField + ']';
  end

  else
    Result := SimpleText;
end;

procedure TrmTableCell.SetFont(const Value: TrwFont);
begin
  FFont.Assign(Value);
  FTableFont := False;
end;

procedure TrmTableCell.OnChangeFont(Sender: TObject);
begin
  FTableFont := Assigned(FTable) and FTable.Font.TheSame(Font);
  CheckAutosize;
  Invalidate;  
end;


procedure TrmTable.SetFont(const Value: TrwFont);
begin
  FFont := Value;
end;

function TrmTableCell.IsNotTableFont: Boolean;
begin
  if (csWriting in ComponentState) then
    Result := not FTableFont
  else
    Result := True;
end;

function TrmTableCell.IsNotTableColor: Boolean;
begin
  if (csWriting in ComponentState) then
    Result := Assigned(FTable) and (FTable.Color <> Color)
  else
    Result := True;
end;

procedure TrmTableCell.SetAlignment(const Value: TAlignment);
begin
  FAlignment := Value;
  Invalidate;  
end;

procedure TrmTableCell.SetLayout(const Value: TTextLayout);
begin
  FLayout := Value;
  Invalidate;  
end;

procedure TrmTableCell.SetBorderLines(const Value: TrwBorderLines);
begin
  FBorderLines.Assign(Value);
end;

procedure TrmTableCell.OnChangeBorderLines(Sender: TObject);
var
  i: Integer;
  C: TrmTableCell;
begin
  if Loading or not Assigned(FTable) or FTable.FChanging then
    Exit;

  // update border lines of neighboring cells
  FTable.FChanging := True;
  try
    if FCellRect.Left > 0 then
      for i := FCellRect.Top to FCellRect.Bottom do
      begin
        C := FTable.FTableInfo.Cell(FCellRect.Left - 1, i);
        if (C.FCellRect.Top >= FCellRect.Top) and (C.FCellRect.Bottom <= FCellRect.Bottom) then
        begin
          C.BorderLines.RightLine := BorderLines.LeftLine;
          C.ApplyBorderLinesForSubTable;
        end;
        C.Invalidate;
      end;

    if FCellRect.Right < FTable.FTableInfo.ColsCount - 1 then
      for i := FCellRect.Top to FCellRect.Bottom do
      begin
        C := FTable.FTableInfo.Cell(FCellRect.Right + 1, i);
        if (C.FCellRect.Top >= FCellRect.Top) and (C.FCellRect.Bottom <= FCellRect.Bottom) then
        begin
          C.BorderLines.LeftLine := BorderLines.RightLine;
          C.ApplyBorderLinesForSubTable;
        end;
        C.Invalidate
      end;

    if FCellRect.Top > 0 then
      for i := FCellRect.Left to FCellRect.Right do
      begin
        C := FTable.FTableInfo.Cell(i, FCellRect.Top - 1);
        if (C.FCellRect.Left >= FCellRect.Left) and (C.FCellRect.Right <= FCellRect.Right) then
        begin
          C.BorderLines.BottomLine := BorderLines.TopLine;
          C.ApplyBorderLinesForSubTable;
        end;
        C.Invalidate;
      end;

    if FCellRect.Bottom < FTable.FTableInfo.RowsCount - 1 then
      for i := FCellRect.Left to FCellRect.Right do
      begin
        C := FTable.FTableInfo.Cell(i, FCellRect.Bottom + 1);
        if (C.FCellRect.Left >= FCellRect.Left) and (C.FCellRect.Right <= FCellRect.Right) then
        begin
          C.BorderLines.TopLine := BorderLines.BottomLine;
          C.ApplyBorderLinesForSubTable;
        end;
        C.Invalidate;
      end;

  finally
    FTable.FChanging := False;
  end;

  Invalidate;
  FTable.Invalidate;
end;


procedure TrmTableCell.GetNeighbours(const AEdge: Char; const ACells: TList);
var
  n, i, j: Integer;
begin
  if AEdge in ['t', 'b'] then
    n := FCellRect.Right - FCellRect.Left + 1
  else
    n := FCellRect.Bottom - FCellRect.Top + 1;

  ACells.Count := n;
  j := 0;

  case AEdge of
    't': if FCellRect.Top > 0 then
         begin
           for i := FCellRect.Left to FCellRect.Right do
           begin
             ACells[j] := FTable.FTableInfo.Cell(i, FCellRect.Top - 1);
             Inc(j);
             if TrmTableCell(ACells[j - 1]).FCellRect.Right >= FCellRect.Right then
               break;
           end;
         end;

    'b': if FCellRect.Bottom < FTable.FTableInfo.RowsCount - 1 then
         begin
           for i := FCellRect.Left to FCellRect.Right do
           begin
             ACells[j] := FTable.FTableInfo.Cell(i, FCellRect.Bottom + 1);
             Inc(j);
             if TrmTableCell(ACells[j - 1]).FCellRect.Right >= FCellRect.Right then
               break;
           end;
         end;

    'l': if FCellRect.Left > 0 then
         begin
           for i := FCellRect.Top to FCellRect.Bottom do
           begin
             ACells[j] := FTable.FTableInfo.Cell(FCellRect.Left - 1, i);
             Inc(j);
             if TrmTableCell(ACells[j - 1]).FCellRect.Bottom >= FCellRect.Bottom then
               break;
           end;

         end;
    'r': if FCellRect.Left < FTable.FTableInfo.ColsCount - 1 then
         begin
           for i := FCellRect.Top to FCellRect.Bottom do
           begin
             ACells[j] := FTable.FTableInfo.Cell(FCellRect.Right + 1, i);
             Inc(j);
             if TrmTableCell(ACells[j - 1]).FCellRect.Bottom >= FCellRect.Bottom then
               break;
           end;
         end;
  end;

  ACells.Count := j;
end;

procedure TrmTableCell.CheckAutosize;
begin
  if (ControlCount > 0) or VarIsNull(Value) or Loading then
    Exit;


{  Table.TableInfo.FSkipValueChecking := True;
  try
    Table.TableInfo.ColWidth[CellRect.Right] := Table.TableInfo.ColWidth[CellRect.Right];
    Table.TableInfo.RowHeight[CellRect.Bottom] := Table.TableInfo.RowHeight[CellRect.Bottom];
  finally
    Table.TableInfo.FSkipValueChecking := False;
  end;
}
  if AutoHeight then
    if SubTable = nil then
    begin
      FCellTextSize.cx := 0;
      FCellTextSize.cy := 0;
      Height := CellTextSize.cy;
    end;
end;

function TrmTableCell.CellTextSize: TSize;
begin
  if (FCellTextSize.cy = 0) and (Text <> '') then
    CalcTextSize(ClientRect);
  Result := FCellTextSize;
end;

procedure TrmTableCell.CalcTextSize(ARect: TRect);
var
  Tinf: TrwTextDrawInfoRec;
begin
  if Loading then
    Exit;

  Tinf.CalcOnly := True;
  Tinf.Canvas := nil;
  Tinf.ZoomRatio := 1;
  Tinf.Text := Text;
  Tinf.Font := Font;
  Tinf.Color := Color;
  Tinf.DigitNet := '';
  Tinf.WordWrap := WordWrap;
  Tinf.Alignment := Alignment;
  Tinf.Layout := Layout;
  Tinf.TextRect := ARect;
  OffsetRect(Tinf.TextRect, -Tinf.TextRect.Left, -Tinf.TextRect.Top);

  DrawTextImageRM(@Tinf);

  FCellTextSize.cx := Tinf.TextRect.Right - Tinf.TextRect.Left;
  FCellTextSize.cy := Tinf.TextRect.Bottom - Tinf.TextRect.Top;
end;

function TrmTableCell.GetValue: Variant;
begin
  if not (csWriting in ComponentState) and Assigned(Table) and not Table.FChanging and (Renderer <> nil) then
    Calculate;

  Result := FValueHolder.Value;
end;

procedure TrmTableCell.CreateSubTable;
var
  T: TrmDBTable;
begin
  if Assigned(SubTable) then
    Exit;

  T := TrmDBTable(CreateRWComponentByClass(RWEngineExt.AppAdapter.SubstDefaultClassName('TrmDBTable'), True).RealObject);
  Table.InsertComponent(T);
  T.InitializeForDesign;
  T.Container := Self;
  T.SetDesigner(GetDesigner);
end;

procedure TrmTableCell.DestroySubTable;
begin
  if SubTable = nil then
    Exit;

  SubTable.Free;
end;

function TrmTableCell.GetSubTable: TrmDBTable;
begin
  if (ControlCount = 1) and (Controls[0] is TrmDBTable) then
    Result := TrmDBTable(Controls[0])
  else
    Result := nil;
end;

procedure TrmTableCell.ApplyBorderLinesForSubTable;
var
  i: Integer;
  C: TrmTableCell;
  T: TrmDBTable;
begin
  if Assigned(SubTable) then
  begin
    T := SubTable;

    for i := T.LeftMostCol to T.RightMostCol do
    begin
      C := T.FTableInfo.Cell(i, T.TopMostRow);
      C.BorderLines.TopLine := BorderLines.TopLine;
      C := T.FTableInfo.Cell(i, T.BottomMostRow);
      C.BorderLines.BottomLine := BorderLines.BottomLine;
    end;

    for i := T.TopMostRow to T.BottomMostRow do
    begin
      C := T.FTableInfo.Cell(T.RightMostCol, i);
      C.BorderLines.RightLine := BorderLines.RightLine;
      C := T.FTableInfo.Cell(T.LeftMostCol, i);
      C.BorderLines.LeftLine := BorderLines.LeftLine;
    end;

    T.Invalidate;
  end;
end;

function TrmTableCell.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.MovingDirections := [];
  Result.ResizingDirections := [];
  Result.StandaloneObject := False;
end;


procedure TrmTableCell.ApplyMouseResizing(NewBounds: TRect; MousePosition: TrmdMousePosition; Shift: TShiftState);
var
  Ci1, Ci2, d: Integer;
  flMove: Boolean;
  Tbl: TrmCustomTable;

  procedure ColumnChange;
  var
    n: Integer;
  begin
    // Change in the middle
    if (Ci1 >= 0) and (Ci2 < Table.TableInfo.ColsCount) then
    begin
      if d < 0 then
      begin
        d := - d;
        if not flMove then
        begin
          // increase C2
          n := Table.TableInfo.ColWidth[Ci2];
          Table.TableInfo.ColWidth[Ci2] := Table.TableInfo.ColWidth[Ci2] + d;
          d := Table.TableInfo.ColWidth[Ci2] - n;
          //decrease C1
          n := Table.TableInfo.ColWidth[Ci1];
          Table.TableInfo.ColWidth[Ci1] := Table.TableInfo.ColWidth[Ci1] - d;
          //correct C2
          d := (n - Table.TableInfo.ColWidth[Ci1]) - d;
          Table.TableInfo.ColWidth[Ci2] := Table.TableInfo.ColWidth[Ci2] + d;
        end
        else
          //decrease C1
          Table.TableInfo.ColWidth[Ci1] := Table.TableInfo.ColWidth[Ci1] - d;
      end
      else
      begin
        n := Table.TableInfo.ColWidth[Ci1];
        // increase C1
        Table.TableInfo.ColWidth[Ci1] := Table.TableInfo.ColWidth[Ci1] + d;
        if not flMove then
        begin
          //decrease C2
          d := Table.TableInfo.ColWidth[Ci1] - n;
          n := Table.TableInfo.ColWidth[Ci2];
          Table.TableInfo.ColWidth[Ci2] := Table.TableInfo.ColWidth[Ci2] - d;
          //correct C1
          d := (n - Table.TableInfo.ColWidth[Ci2]) - d;
          Table.TableInfo.ColWidth[Ci1] := Table.TableInfo.ColWidth[Ci1] + d;
        end;
      end;

      Exit;
    end;

    // Left Edge of the Table
    if Ci1 <  0 then
    begin
      if d < 0 then
      begin
        d := -d;
        Table.TableInfo.ColWidth[Ci2] := Table.TableInfo.ColWidth[Ci2] + d;
        if Assigned(Tbl) then
          Tbl.Left := Tbl.Left - d;
      end
      else
      begin
        n := Table.TableInfo.ColWidth[Ci2];
        Table.TableInfo.ColWidth[Ci2] := Table.TableInfo.ColWidth[Ci2] - d;
        d := n - Table.TableInfo.ColWidth[Ci2];
        if Assigned(Tbl) then
          Tbl.Left := Tbl.Left + d;
      end;

      Exit;
    end;

    // Right Edge of the Table
    if Ci2 >= Table.TableInfo.ColsCount then
      Table.Width := Table.Width + d;
  end;

  procedure RowChange;
  var
    n: Integer;
  begin
    // Change in the middle
    if (Ci1 >= 0) and (Ci2 < Table.TableInfo.RowsCount) then
    begin
      if d < 0 then
      begin
        d := - d;
        if not flMove then
        begin
          // increase C2
          n := Table.TableInfo.RowHeight[Ci2];
          Table.TableInfo.RowHeight[Ci2] := Table.TableInfo.RowHeight[Ci2] + d;
          d := Table.TableInfo.RowHeight[Ci2] - n;
          //decrease C1
          n := Table.TableInfo.RowHeight[Ci1];
          Table.TableInfo.RowHeight[Ci1] := Table.TableInfo.RowHeight[Ci1] - d;
          //correct C2
          d := (n - Table.TableInfo.RowHeight[Ci1]) - d;
          Table.TableInfo.RowHeight[Ci2] := Table.TableInfo.RowHeight[Ci2] + d;
        end
        else
          //decrease C1
          Table.TableInfo.RowHeight[Ci1] := Table.TableInfo.RowHeight[Ci1] - d;
      end
      else
      begin
        n := Table.TableInfo.RowHeight[Ci1];
        // increase C1
        Table.TableInfo.RowHeight[Ci1] := Table.TableInfo.RowHeight[Ci1] + d;
        if not flMove then
        begin
          //decrease C2
          d := Table.TableInfo.RowHeight[Ci1] - n;
          n := Table.TableInfo.RowHeight[Ci2];
          Table.TableInfo.RowHeight[Ci2] := Table.TableInfo.RowHeight[Ci2] - d;
          //correct C1
          d := (n - Table.TableInfo.RowHeight[Ci2]) - d;
          Table.TableInfo.RowHeight[Ci1] := Table.TableInfo.RowHeight[Ci1] + d;
        end;
      end;

      Exit;
    end;

    // Top Edge of the Table
    if Ci1 <  0 then
    begin
      if d < 0 then
      begin
        d := -d;
        Table.TableInfo.RowHeight[Ci2] := Table.TableInfo.RowHeight[Ci2] + d;
        if Assigned(Tbl) then
          Tbl.Top := Tbl.Top - d;
      end
      else
      begin
        n := Table.TableInfo.RowHeight[Ci2];
        Table.TableInfo.RowHeight[Ci2] := Table.TableInfo.RowHeight[Ci2] - d;
        d := n - Table.TableInfo.RowHeight[Ci2];
        if Assigned(Tbl) then
          Tbl.Top := Tbl.Top + d;
      end;

      Exit;
    end;

    // Bottom Edge of the Table
    if Ci2 >= Table.TableInfo.RowsCount then
      Table.Height := Table.Height + d;
  end;

begin
  Table.DisableAlign;
  BeforeChangePos;
  try
    Tbl := Table;
    if Tbl is TrmDBTableBand then
      Tbl := TrmCustomTable(TrmDBTableBand(Tbl).Owner);
    if (Tbl is TrmDBTable) and TrmDBTable(Tbl).IsSubTable then
      Tbl := nil;

    flMove := ssShift in Shift;
    // Column change
    if MousePosition in [rmdMPLeftBound, rmdMPRightBound] then
    begin
      if MousePosition = rmdMPLeftBound then
      begin
        Ci1 := CellRect.Left - 1;
        Ci2 := CellRect.Left;
        d := NewBounds.Left - BoundsRect.Left;
      end
      else if MousePosition = rmdMPRightBound then
      begin
        Ci1 := CellRect.Right;
        Ci2 := CellRect.Right + 1;
        d := NewBounds.Right - BoundsRect.Right;
      end;
      ColumnChange;
    end

    // Row change
    else
    begin
      if MousePosition = rmdMPTopBound then
      begin
        Ci1 := CellRect.Top - 1;
        Ci2 := CellRect.Top;
        d := NewBounds.Top - BoundsRect.Top;
      end
      else if MousePosition = rmdMPBottomBound then
      begin
        Ci1 := CellRect.Bottom;
        Ci2 := CellRect.Bottom + 1;
        d := NewBounds.Bottom - BoundsRect.Bottom;
      end;
      RowChange;
    end

  finally
    Table.EnableAlign;
    AfterChangePos;
  end;
end;

procedure TrmTableCell.PaintControl(ACanvas: TrwCanvas);

  procedure PaintText;
  var
    Tinf: TrwTextDrawInfoRec;
  begin
    Tinf.Text := GetShowText;

    if Tinf.Text = '' then
      Exit;

    ACanvas.Brush.Style  := bsClear;
    Tinf.CalcOnly := False;
    Tinf.ZoomRatio := ZoomFactor;
    Tinf.Canvas := ACanvas;
    Tinf.Font := Font;
    Tinf.Color := Color;
    Tinf.DigitNet := '';
    Tinf.WordWrap := WordWrap;
    Tinf.Alignment := Alignment;
    Tinf.Layout := Layout;
    Tinf.TextRect := ClientRect;

    DrawTextImageRM(@Tinf);
  end;

begin
  if ControlCount = 0 then
    PaintText
  else
    inherited;
end;


function TrmTableCell.CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean;
var
  R: TRect;
  W: Integer;
begin
  W := AWidth;

  
  Result := True;
  ALeft := 0;
  ATop := 0;

  if SubTable <> nil then
  begin
    if not SubTable.FSBTStandAloneMode then
    begin
      R.Left := 0;
      R.Top := 0;
      R.Right := AWidth;
      R.Bottom := AHeight;
      SubTable.FSBTStandAloneMode := True;
      try
        SubTable.CheckSizeConstrains(R.Left, R.Top, R.Right, R.Bottom);
      finally
        SubTable.FSBTStandAloneMode := False;
      end;
      AWidth := R.Right;
      AHeight := R.Bottom;
    end
  end

  else if (Text <> '') and AutoHeight then
  begin
    R := Rect(0, 0, AWidth, AHeight);
    AdjustClientRect(R);
    OffsetRect(R, -R.Left, -R.Top);
    CalcTextSize(R);
    R := Rect(0, 0, CellTextSize.cx, CellTextSize.cy);
    UnAdjustClientRect(R);
    OffsetRect(R, -R.Left, -R.Top);
//    if AWidth < R.Right then                         I do not have AutoWidth yet
//      AWidth := R.Right;
    if AHeight < R.Bottom then
      AHeight := R.Bottom;
    FCellTextSize.cx := 0;
    FCellTextSize.cy := 0;
  end;

  if FixedWidth then
    AWidth := Width;


  if (DataField <> '') and (AWidth > W) then
    AWidth := W;
end;

procedure TrmTableCell.AfterChangePos;
begin
  if Assigned(FTable) then
  begin
    if SubTable <> nil then
      SubTable.SetBounds(0, 0, Width, Height);
    FTable.AfterChangePos;
    inherited;
  end;
end;

procedure TrmTableCell.BeforeChangePos;
begin
  if Assigned(FTable) then
  begin
    inherited;
    FTable.BeforeChangePos;
  end;
end;


function TrmTableCell.GetPosition(Index: Integer): Integer;

  function GetLeft: Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := FTable.LeftMostCol to CellRect.Left - 1 do
      Inc(Result, FTable.TableInfo.ColWidth[i]);
  end;

  function GetTop: Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := FTable.TopMostRow to CellRect.Top - 1 do
      Inc(Result, FTable.TableInfo.RowHeight[i]);
  end;

  function GetWidth: Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := CellRect.Left to CellRect.Right do
      Inc(Result, FTable.TableInfo.ColWidth[i]);
  end;

  function GetHeight: Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := CellRect.Top to CellRect.Bottom do
      Inc(Result, FTable.TableInfo.RowHeight[i]);
  end;

begin
  if not Assigned(FTable) then
  begin
    Result := 0;
    Exit;
  end;

  case Index of
    0: Result := GetLeft;
    1: Result := GetTop;
    2: Result := GetWidth;
    3: Result := GetHeight;
  else
    Result := 0;
  end;
end;

procedure TrmTableCell.SetPosition(Index, Value: Integer);
var
  w, h: Integer;
begin
  if not Assigned(FTable) then
    Exit;

  w := Width;
  h := Height;
  case Index of
    2: w := Value;
    3: h := Value;
  end;

  SetBounds(Left, Top, w, h);
end;


procedure TrmTableCell.Loaded;
begin
  UpdateTableInfo;

  if SubTable <> nil then
  begin
    FInnerMarginLeft := 0;
    FInnerMarginRight := 0;
    FInnerMarginTop := 0;
    FInnerMarginBottom := 0;
  end;

  if FormulaValue <> nil then
    FValueHolder.Value := Null;

  inherited;
end;

procedure TrmTableCell.CreateRWAObject;
var
  R, RTxt: TRect;
  TR: TStringBoundsRec;
  h: String;
//  w: Integer;
  flTopCell, flLeftCell, flBottomCell, flRightCell: Boolean;

  function EdgeLeftTopFix(BL: TrwBorderLine): Integer;
  begin
    Result := BL.Width div 2;
  end;

  function EdgeRightBottomFix(BL: TrwBorderLine): Integer;
  var
    w: Integer;
  begin
    w := BL.Width;
    Result := w div 2;
    if Result * 2 < w then
      Inc(Result);
  end;

  procedure AddLine(BL: TrwBorderLine; X1, Y1, X2, Y2: Integer);
  var
    LineInfo: TrmRenderedBorderLineInfo;
  begin
    if BL.Visible then
    begin
      LineInfo.PointA := Point(X1, Y1);
      LineInfo.PointB := Point(X2, Y2);
      LineInfo.Width := BL.Width;
      LineInfo.Color := BL.Color;
      LineInfo.Style := BL.Style;
      LineInfo.Compare := Compare;
      LineInfo.LayerNumber := LayerNumber;
      
      Table.RenderedBorderLines.AddLine(LineInfo);
    end;
  end;

begin
  if Color <> clNone then
    Renderer.AddFrame(PrintRectOnCurrPage, Compare, LayerNumber, Color, [], clBlack, 1, psSolid);

  // add text
  if RendText <> '' then
  begin
    RTxt := PrintRectOnCurrPage;
    Inc(RTxt.Left, GetLeftClientAmendment);
    Inc(RTxt.Right, GetRightClientAmendment);
    Inc(RTxt.Top, GetTopClientAmendment);
    Inc(RTxt.Bottom, GetBottomClientAmendment);

    R := RTxt;
    OffsetRect(R, -R.Left, -R.Top);
    TR := TextInRect(R, RendText, RendTextHeight, True);
    if (TR.BeginPos > 0) and (TR.EndPos > 0) then
    begin
      h := Copy(RendText, TR.BeginPos, TR.EndPos);
      RendText := Copy(RendText, TR.EndPos + 1, Length(RendText) - TR.EndPos);
    end
    else
      h := '';

    if h <> '' then
      Renderer.AddText(RTxt, Compare, LayerNumber, Alignment, Layout, clNone, False, '', Font, h);
  end;


  // add border
  R := PrintRectOnCurrPage;

  if (DBTable is TrmDBTable) and TrmDBTable(DBTable).IsSubTable then
  begin
    flTopCell := False;
    flLeftCell := False;
    flBottomCell := CellRect.Bottom = Table.FTableInfo.RowsCount - 1;
    flRightCell := False;
  end
  else
  begin
    flTopCell := CellRect.Top = 0;
    flLeftCell := CellRect.Left = 0;
    flBottomCell := CellRect.Bottom = Table.FTableInfo.RowsCount - 1;
    flRightCell := CellRect.Right = Table.FTableInfo.ColsCount - 1;
  end;

  if flLeftCell then
  begin
//    w := EdgeLeftTopFix(BorderLines.LeftLine);
    AddLine(BorderLines.LeftLine, R.Left, R.Top, R.Left, R.Bottom);
  end
  else
    AddLine(BorderLines.LeftLine, R.Left, R.Top, R.Left, R.Bottom);

  if flTopCell then
  begin
//    w := EdgeLeftTopFix(BorderLines.TopLine);
    AddLine(BorderLines.TopLine, R.Left, R.Top, R.Right, R.Top);
  end
  else
    AddLine(BorderLines.TopLine, R.Left, R.Top, R.Right, R.Top);

  if flRightCell then
  begin
//    w := EdgeRightBottomFix(BorderLines.RightLine);
    AddLine(BorderLines.RightLine, R.Right, R.Top, R.Right, R.Bottom)
  end
  else
    AddLine(BorderLines.RightLine, R.Right, R.Top, R.Right, R.Bottom);

  if flBottomCell then
  begin
//    w := EdgeRightBottomFix(BorderLines.BottomLine);
    AddLine(BorderLines.BottomLine, R.Left, R.Bottom, R.Right, R.Bottom)
  end
  else
    AddLine(BorderLines.BottomLine, R.Left, R.Bottom, R.Right, R.Bottom);
end;


procedure TrmTableCell.DoCalculate;
begin
  inherited;
  if (SubTable <> nil) and AutoHeight then
    if SubTable.RendStatus = rmRWARSCalculated then
      Height := SubTable.Height
    else
      Height := 0;
end;

procedure TrmTableCell.StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean);
begin
  inherited;
  if SubTable = nil then
  begin
    AStream.WriteVariant(Value);
    AStream.WriteString(Format);
    StoreFont(AStream, Font);
    AStream.WriteInteger(Ord(Alignment));
    AStream.WriteInteger(Ord(Layout));
    StoreBorderLines(AStream, BorderLines);
    StoreRendInfo(AStream);
  end
  else
  begin
    if SubTable.RendStatus = rmRWARSCalculated then
      AStream.WriteInteger(SubTable.FCurrentTblVersion)
    else
      AStream.WriteInteger(0); //Bogus reference for empty table

    StoreBorderLines(AStream, BorderLines);
    StoreRendInfo(AStream);
  end;
end;


procedure TrmTableCell.ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean);
var
  R: TRect;
begin
  inherited;
  if SubTable = nil then
  begin
    Value := AStream.ReadVariant;
    Format := AStream.ReadString;
    ReStoreFont(AStream, Font);
    Alignment := TAlignment(AStream.ReadInteger);
    Layout := TTextLayout(AStream.ReadInteger);
    ReStoreBorderLines(AStream, BorderLines);
    ReStoreRendInfo(AStream);
  end

  else
  begin
    SubTable.FCurrentTblVersion := AStream.ReadInteger;
    R := CalculatedBounds;
    OffsetRect(R, -R.Left, -R.Top);
    SubTable.CalculatedBounds := R;
    ReStoreBorderLines(AStream, BorderLines);
    ReStoreRendInfo(AStream);
  end;
end;

procedure TrmTableCell.SetMultiPaged(const Value: Boolean);
begin
  //always False
  inherited SetMultiPaged(False);
end;

procedure TrmTableCell.SetPrintOnEachPage(const Value: Boolean);
begin
  //always False
  inherited SetPrintOnEachPage(False);
end;

function TrmTableCell.CanWriteValue: Boolean;
begin
  Result := FormulaValue = nil;
end;

function TrmTableCell.FormulaValue: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatSetProperty, 'Value');
end;

procedure TrmTableCell.PrepareTextForPrinting;
var
  Tinf: TrwTextDrawInfoRec;
begin
  Tinf.CalcOnly := True;
  Tinf.Canvas := nil;
  Tinf.ZoomRatio := 1;
  Tinf.Text := Text;
  Tinf.Font := Font;
  Tinf.DigitNet := '';
  Tinf.WordWrap := WordWrap;
  Tinf.Alignment := Alignment;
  Tinf.Layout := Layout;
  Tinf.TextRect := ClientRect;
  OffsetRect(Tinf.TextRect, -Tinf.TextRect.Left, -Tinf.TextRect.Top);

  DrawTextImageRM(@Tinf);

  RendText := Tinf.Text;
  RendTextHeight := Tinf.LineHeight;
end;

procedure TrmTableCell.SetRendStatus(const Value: TrmRWARenderingStatus);
begin
  inherited;
  if RendStatus = rmRWARSCalculated then
    PrepareTextForPrinting;
end;

procedure TrmTableCell.UpdateTableInfo;
begin
  if Assigned(FTable) then
    FTable.FTableInfo.LinkTableCell(Self, FCellRect.Left, FCellRect.Top, FCellRect.Right, FCellRect.Bottom);
end;

function TrmTableCell.PSubTable: Variant;
begin
  Result := Integer(Pointer(SubTable));
end;

procedure TrmTableCell.ReStoreChildren(const AStream: IEvDualStream; ARestorePrintState: Boolean);
begin
  if not Assigned(SubTable) or ARestorePrintState then
    inherited;
end;

procedure TrmTableCell.StoreChildren(const AStream: IEvDualStream; AStorePrintState: Boolean);
begin
  if not Assigned(SubTable) or AStorePrintState then
    inherited;
end;

procedure TrmTableCell.PaintForeground(ACanvas: TrwCanvas);
var
  R: TRect;
  flTopCell, flLeftCell, flBottomCell, flRightCell: Boolean;
  w: Integer;

  function PenWidth(BL: TrwBorderLine): Integer;
  begin
    if  BL.Visible then
    begin
      Result := Zoomed(BL.Width);
      if Result = 0 then
        Result := 1;
    end
    else
      Result := 1;
  end;

  procedure DrawLine(BL: TrwBorderLine; X1, Y1, X2, Y2: Integer);
  begin
    with ACanvas do
    begin
      Pen.Mode := pmCopy;
      Pen.Width := PenWidth(BL);
      if BL.Visible then
      begin
        Pen.Style := BL.Style;
        Pen.Color := BL.Color;
      end
      else
      begin
        Pen.Style := psSolid;
        Pen.Color := clSilver;
      end;

      MoveTo(X1, Y1);
      LineTo(X2, Y2);
    end;
  end;

  function EdgeLeftTopFix(BL: TrwBorderLine): Integer;
  var
    w: Integer;
  begin
    w := PenWidth(BL);
    Result := w div 2;
  end;

  function EdgeRightBottomFix(BL: TrwBorderLine): Integer;
  var
    w: Integer;
  begin
    w := PenWidth(BL);
    Result := w div 2;
    if Result * 2 < w then
      Inc(Result);
  end;

  procedure PaintSelection;
  var
    i: Integer;
    R: TRect;
  begin
    for i := Low(Table.FCurrentSelectedCells) to High(Table.FCurrentSelectedCells) do
      if Table.FCurrentSelectedCells[i] = Self then
      begin
        R := ZoomedBoundsRect(ACanvas);
        OffsetRect(R, -R.Left, -R.Top);
        if flLeftCell then
          Inc(R.Left);
        if flTopCell then
          Inc(R.Top);
        if flRightCell then
          Dec(R.Right);
        if flBottomCell then
          Dec(R.Bottom);

        InvertRect(ACanvas.Handle, R);
        break;
      end;
  end;

  procedure DrawNet;
  var
    Rc: TRect;
    i, S, p: Integer;
    pt: TPoint;
  begin
    with ACanvas do
    begin
      Rc := CellRect;

      Pen.Mode := pmCopy;
      Pen.Width := 1;
      Pen.Style := psDot;
      Pen.Color := $00DFDFDF;

      S := R.Right - R.Left;
      p := R.Top;
      for i := Rc.Top to Rc.Bottom - 1 do
      begin
        Inc(p, Table.TableInfo.RowHeight[i]);
        Pt := ZoomedClientPoint(ACanvas, Point(0, p));
        MoveTo(0, Pt.y);
        LineTo(S, Pt.y);
      end;

      S := R.Bottom - R.Top;
      p := R.Left;
      for i := Rc.Left to Rc.Right - 1 do
      begin
        Inc(p, Table.TableInfo.ColWidth[i]);
        Pt := ZoomedClientPoint(ACanvas, Point(p, 0));
        MoveTo(Pt.x, 0);
        LineTo(Pt.x, S);
      end;
    end;
  end;


begin
  R := ZoomedBoundsRect(ACanvas);
  OffsetRect(R, -R.Left, -R.Top);

  if (DBTable is TrmDBTable) and TrmDBTable(DBTable).IsSubTable then
  begin
    flTopCell := False;
    flLeftCell := False;
    flBottomCell := CellRect.Bottom = Table.FTableInfo.RowsCount - 1;
    flRightCell := False;
  end
  else
  begin
    flTopCell := CellRect.Top = 0;
    flLeftCell := CellRect.Left = 0;
    flBottomCell := CellRect.Bottom = Table.FTableInfo.RowsCount - 1;
    flRightCell := CellRect.Right = Table.FTableInfo.ColsCount - 1;
  end;

  if flLeftCell then
  begin
    w := EdgeLeftTopFix(BorderLines.LeftLine);
    DrawLine(BorderLines.LeftLine, R.Left + w, R.Top, R.Left + w, R.Bottom);
  end
  else
    DrawLine(BorderLines.LeftLine, R.Left, R.Top, R.Left, R.Bottom);

  if flTopCell then
  begin
    w := EdgeLeftTopFix(BorderLines.TopLine);
    DrawLine(BorderLines.TopLine, R.Left, R.Top + w, R.Right, R.Top + w);
  end
  else
    DrawLine(BorderLines.TopLine, R.Left, R.Top, R.Right, R.Top);

  if flRightCell then
  begin
    w := EdgeRightBottomFix(BorderLines.RightLine);
    DrawLine(BorderLines.RightLine, R.Right - w, R.Top, R.Right - w, R.Bottom)
  end
  else
    DrawLine(BorderLines.RightLine, R.Right, R.Top, R.Right, R.Bottom);

  if flBottomCell then
  begin
    w := EdgeRightBottomFix(BorderLines.BottomLine);
    DrawLine(BorderLines.BottomLine, R.Left, R.Bottom - w, R.Right, R.Bottom - w)
  end
  else
    DrawLine(BorderLines.BottomLine, R.Left, R.Bottom, R.Right, R.Bottom);

  if not Assigned(SubTable) and GetDesigner.GetDesignerSettings.ShowTableGrid then
    DrawNet;

  PaintSelection;
end;

procedure TrmTableCell.SetAutoHeight(const AValue: Boolean);
begin
  if FAutoHeight <> AValue then
  begin
    FAutoHeight := AValue;
    if FAutoHeight then
      CheckAutosize;
  end;
end;

procedure TrmTableCell.SetWordWrap(const AValue: Boolean);
begin
  if FWordWrap <> AValue then
  begin
    FWordWrap := AValue;
    if AutoHeight then
      CheckAutosize
    else
      Invalidate;  
  end;    
end;

procedure TrmTableCell.SyncTableTagPos;
begin
  if Assigned(SubTable) then
    SubTable.SyncTagPos;
end;

procedure TrmTableCell.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;

  if (Shift = [ssLeft]) and not ResizingBoundsNow then
  begin
    SetCaptureRMControl(Self);
    FMouseSelectionActive := True;
    FMousePosition := rmdMPClient;
  end;
end;

procedure TrmTableCell.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  SelObjects: TrmListOfObjects;
  fl: Boolean;
  i: Integer;
  P: TPoint;
  C: TrmTableCell;
begin
  SelObjects := nil;
  if FMouseSelectionActive then
  begin
    P := Point(X + Left, Y + Top);
    C := Table.CellAtPos(P);

    if not Assigned(C) then
      Exit;

    SelObjects := GetDesigner.GetSelectedObjects;

    if C = Self then
    begin
      if Length(SelObjects) > 1 then
        GetDesigner.Notify(rmdSelectObject, VarArrayOf([Integer(Pointer(Self as IrmDesignObject)), False]))
    end

    else
    begin
      fl := False;
      for i := Low(SelObjects) to High(SelObjects) do
        if SelObjects[i].RealObject = C then
        begin
          fl := True;
          break;
        end;

      if not fl then
      begin
        C.FMousePosition := rmdMPClient;
        GetDesigner.Notify(rmdSelectObject, VarArrayOf([Integer(Pointer(C as IrmDesignObject)), True]))
      end;
    end;
  end

  else
    inherited;
end;

procedure TrmTableCell.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;

  if (Button = mbLeft) and (Shift = []) and FMouseSelectionActive then
  begin
    SetCaptureRMControl(nil);
    FMouseSelectionActive := False;
  end;
end;

procedure TrmTableCell.SetColor(Value: TColor);
begin
  inherited;
  if Assigned(SubTable) and (SubTable.Color <> Value) then
    SubTable.Color := Value;
end;

procedure TrmTableCell.DoAlign;
begin
  if Loading then
    AlignChildren;
end;

procedure TrmTableCell.SetFixedWidth(const Value: Boolean);
begin
  FFixedWidth := Value;
end;


function TrmTableCell.PreCutChildren: Boolean;
var
  lc, h, CutAmd: Integer;
begin
  if ControlCount > 0 then
    Result := inherited PreCutChildren

  else
  begin
    Result := True;

    if RendText = '' then
      Exit;

    h := FBottomCutClientPosition - FTopCutClientPosition;
    if h < RendTextHeight then
    begin
      Dec(FBottomCutClientPosition, h);
      Dec(FBottomCutPosition, h);
      Result := False;
      Exit;
    end;

    lc := TextLineCount(RendText);
    if h div RendTextHeight < lc then
    begin
      CutAmd := - (h - (h div RendTextHeight) * RendTextHeight);
      Result := CutAmd = 0;
      if not Result then
      begin
        Inc(FBottomCutClientPosition, CutAmd);
        Inc(FBottomCutPosition, CutAmd);
      end;
    end;
  end;
end;

function TrmTableCell.GetClosestCell(const ADirection: Char): IrmTableCell;

  procedure NeighbourFromUp;
  begin
    if CellRect.Top - 1 < 0 then
      Exit;
    Result := Table.FTableInfo.Cell(CellRect.Left + (CellRect.Right - CellRect.Left) div 2, CellRect.Top - 1);
  end;

  procedure NeighbourFromDown;
  begin
    if CellRect.Bottom + 1 >= Table.FTableInfo.RowsCount then
      Exit;
    Result := Table.FTableInfo.Cell(CellRect.Left + (CellRect.Right - CellRect.Left) div 2, CellRect.Bottom + 1);
  end;

  procedure NeighbourFromLeft;
  begin
    if CellRect.Left - 1 < 0 then
      Exit;
    Result := Table.FTableInfo.Cell(CellRect.Left - 1, CellRect.Top + (CellRect.Bottom - CellRect.Top) div 2);
  end;

  procedure NeighbourFromRight;
  begin
    if CellRect.Right + 1 >= Table.FTableInfo.ColsCount then
      Exit;
    Result := Table.FTableInfo.Cell(CellRect.Right + 1, CellRect.Top + (CellRect.Bottom - CellRect.Top) div 2);
  end;

begin
  Result := nil;
  case ADirection of
    'U': NeighbourFromUp;
    'D': NeighbourFromDown;
    'L': NeighbourFromLeft;
    'R': NeighbourFromRight;
  end;
end;

procedure TrmTableCell.SplitCell(const ACols, ARows: Integer);
begin
  Table.SplitCell(Self, ACols, ARows);
end;

function TrmTableCell.FormulaTextFilter: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatDataFilter, 'Text');
end;


function TrmTableCell.GetShowText: String;
var
  Rec: TrmDataFieldInfoRec;
begin
  if DataField <> '' then
  begin
    Rec := GetDataFieldInfo;
    if Assigned(Rec.QBField) then
      Result := '[' + Rec.Name + ']'
    else
      Result := cErrorTag;
  end

  else
  begin
    if FormulaValue <> nil then
      Result := cCalcTag
    else
      Result := FormatValue(FFormat, Value);
  end;
end;

procedure TrmTableCell.SetBlockParentIfEmpty(const Value: Boolean);
begin
  //always True
  inherited SetBlockParentIfEmpty(True);
end;

function TrmTableCell.GetDataFieldInfo: TrmDataFieldInfoRec;
begin
  Result := DataFieldInfo(Self, DBTable);
end;

function TrmTableCell.GetFieldName: String;
begin
  Result := DataField;
end;

procedure TrmTableCell.SetFieldName(const AFieldName: String);
begin
  DataField := AFieldName;
end;

procedure TrmTableCell.SetValueFromDataSource;
begin
  FValueHolder.SetValueFromDataSource;
end;

function TrmTableCell.GetDataField: String;
begin
  Result := FValueHolder.DataField;
end;

procedure TrmTableCell.SetDataField(const AValue: String);
begin
  FValueHolder.DataField := AValue;
end;

function TrmTableCell.GetDBTable: TrmDBTable;
begin
  if Table is TrmDBTableBand then
    Result := TrmDBTable(Table.Owner)
  else
    Result := nil;
end;

procedure TrmTableCell.SetInnerMarginBottom(const Value: Integer);
begin
  inherited;
  CheckAutosize;
  Invalidate;
end;

procedure TrmTableCell.SetInnerMarginLeft(const Value: Integer);
begin
  inherited;
  CheckAutosize;
  Invalidate;
end;

procedure TrmTableCell.SetInnerMarginRight(const Value: Integer);
begin
  inherited;
  CheckAutosize;
  Invalidate;
end;

procedure TrmTableCell.SetInnerMarginTop(const Value: Integer);
begin
  inherited;
  CheckAutosize;
  Invalidate;
end;

function TrmTableCell.StoreInnerMarginBottom: Boolean;
begin
  Result := DontStoreProperty or (FInnerMarginBottom <> cDefaultCellBorderSize);
end;

function TrmTableCell.StoreInnerMarginLeft: Boolean;
begin
  Result := DontStoreProperty or (FInnerMarginLeft <> cDefaultCellBorderSize);
end;

function TrmTableCell.StoreInnerMarginRight: Boolean;
begin
  Result := DontStoreProperty or (FInnerMarginRight <> cDefaultCellBorderSize);
end;

function TrmTableCell.StoreInnerMarginTop: Boolean;
begin
  Result := DontStoreProperty or (FInnerMarginTop <> cDefaultCellBorderSize);
end;

{ TrmDBTableBand }

constructor TrmDBTableBand.Create(AOwner: TComponent);
begin
  FBottomMostRow := -1;
  inherited;
  FObjectTagClass := TrmdTableBandTag;
end;

destructor TrmDBTableBand.Destroy;
begin
  Container := nil;

  if not (csDestroying in TrmDBTable(Owner).ComponentState) then
  begin
    if BottomMostRow <> -1 then
      FTableInfo.DeleteRow(TopMostRow, BottomMostRow - TopMostRow + 1);

    case FBandType of
      rwTBTDetail: TrmDBTable(Owner).FDetailBand := nil;
      rwTBTHeader: TrmDBTable(Owner).FHeaderBand := nil;
      rwTBTTotal:  TrmDBTable(Owner).FTotalBand := nil;
    end;
  end;

  TrmDBTable(Owner).UpdateCellBoundsInfo;

  inherited;
end;

procedure TrmDBTableBand.AfterChangeCellLayout;
begin
  TrmDBTable(Owner).UpdateCellBoundsInfo;
  inherited;
end;

procedure TrmDBTableBand.AfterChangePos;
begin
  inherited;
  TrmDBTable(Owner).AfterChangePos;
end;

procedure TrmDBTableBand.BeforeChangePos;
begin
  inherited;
  TrmDBTable(Owner).BeforeChangePos;
end;

function TrmDBTableBand.BottomMostRow: Integer;
begin
  Result := FBottomMostRow;
end;

function TrmDBTableBand.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.MovingDirections := [];
  Result.ResizingDirections := [];
  Result.StandaloneObject := False;
end;

procedure TrmDBTableBand.DoCalculate;
begin
  Top := TrmDBTable(Owner).FRendLastBandPos;

  inherited;

  OnPageCalculated;        // exception for TrmDBTable

  TrmDBTable(Owner).FRendLastBandPos := CalculatedBounds.Bottom;
end;

function TrmDBTableBand.GetPosition(Index: Integer): Integer;
var
  T: TrmDBTable;

  function GetLeft: Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := T.LeftMostCol to LeftMostCol - 1 do
      Inc(Result, TableInfo.ColWidth[i]);
  end;

  function GetTop: Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := T.TopMostRow to TopMostRow - 1 do
      Inc(Result, TableInfo.RowHeight[i]);
  end;

  function GetWidth: Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := LeftMostCol to RightMostCol do
      Inc(Result, TableInfo.ColWidth[i]);
  end;

  function GetHeight: Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := TopMostRow to BottomMostRow do
      Inc(Result, TableInfo.RowHeight[i]);
  end;

begin
  Result := 0;

  if not Assigned(FTableInfo) then
    Exit;

  T := TrmDBTable(Owner);

  if (RendStatus <> rmRWARSNone) and (Index = 1) then
    Result := FTop

  else
    case Index of
      0: Result := GetLeft;
      1: Result := GetTop;
      2: Result := GetWidth;
      3: Result := GetHeight;
    end;
end;

function TrmDBTableBand.HasGapFromBottom: Boolean;
begin
  Result := inherited HasGapFromBottom;
end;

function TrmDBTableBand.HasGapFromRight: Boolean;
begin
  Result := inherited HasGapFromRight and not (TrmDBTable(Owner).Container is TrmTableCell);
end;

procedure TrmDBTableBand.InitializeTableInfo;
begin
  FTableInfo := TrmDBTable(Owner).FTableInfo;
end;


function TrmDBTableBand.LeftMostCol: Integer;
begin
  Result := TrmDBTable(Owner).LeftMostCol;
end;

procedure TrmDBTableBand.LinkBandToTable;
begin
  case FBandType of
    rwTBTDetail: TrmDBTable(Owner).FDetailBand := Self;
    rwTBTHeader: TrmDBTable(Owner).FHeaderBand := Self;
    rwTBTTotal:  TrmDBTable(Owner).FTotalBand :=  Self;
  end;
end;


function TrmDBTableBand.RightMostCol: Integer;
begin
  Result := TrmDBTable(Owner).RightMostCol;
end;

procedure TrmDBTableBand.SetAlign(Value: TAlign);
begin
  // do nothing
end;

procedure TrmDBTableBand.SetBandType(const Value: TrmDBTableBandType);
begin
  if Loading then
    FBandType := Value;
end;

procedure TrmDBTableBand.SetPosition(Index, Value: Integer);
begin
  if (RendStatus <> rmRWARSNone) and (Index  = 1) then
  begin
    FTop := Value;
    Exit;
  end;

  if Index in [0, 1] then
  begin
    case Index of
      0: TrmDBTable(Owner).Left := TrmDBTable(Owner).Left + (Value - Left);
      1: TrmDBTable(Owner).Top := TrmDBTable(Owner).Top + (Value - Top);
    end;

    Exit;
  end;
    
  inherited;
end;

procedure TrmDBTableBand.SetPrintOnEachPage(const Value: Boolean);
begin
  if BandType in [rwTBTHeader, rwTBTGroupHeader] then
    inherited
  else
    inherited SetPrintOnEachPage(False);
end;


procedure TrmDBTableBand.SyncTagPos;
begin
  if not FChangingTags and Assigned(Container) then
  begin
    FChangingTags := True;
    try
      inherited;
      TrmDBTable(Container).SyncTagPos;
    finally
      FChangingTags := False;
    end;
  end;
end;

function TrmDBTableBand.TopMostRow: Integer;
begin
  Result := FTopMostRow;
end;

procedure TrmDBTableBand.UpdateCellBoundsInfo;
var
  i: Integer;
  C: TrmTableCell;
begin
  FTopMostRow := MaxInt;
  FBottomMostRow := -1;

  for i := 0 to ControlCount - 1 do
  begin
    C := TrmTableCell(Controls[i]);
    if C.CellRect.Top < FTopMostRow then
      FTopMostRow := C.CellRect.Top;
    if C.CellRect.Bottom > FBottomMostRow then
      FBottomMostRow := C.CellRect.Bottom;
  end;

  if FBottomMostRow = -1 then
    FTopMostRow := 0;
end;


procedure TrmDBTableBand.BeforePrint;
var
  OldValue: Boolean;
begin
  OldValue := PrintOnEachPage;
  PrintOnEachPage := False;
  inherited;
  PrintOnEachPage := OldValue;
end;

procedure TrmDBTableBand.CalcCutting;
var
  OldValue: Boolean;
begin
  OldValue := PrintOnEachPage;
  PrintOnEachPage := False;
  inherited;
  PrintOnEachPage := OldValue;
end;

procedure TrmDBTableBand.SetPrintIfNoData(const Value: Boolean);
begin
  FPrintIfNoData := Value;
end;


procedure TrmDBTableBand.InitializeForDesign;
begin
  inherited;
  Height := 106;
end;

{ TrmDBTableQuery }

function TrmDBTableQuery.CallCustomFunction(const AFunctionName: String; const AParams: array of Variant): Variant;
begin
  if SameText(AFunctionName, 'UpdateCells') then
  begin
    Table.SyncCellsByQuery;
    Result := Unassigned;
  end

  else
    Result := inherited CallCustomFunction(AFunctionName, AParams);
end;

constructor TrmDBTableQuery.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FTable := TrmDBTable(AOwner);
  Name := 'Query';
  FTable.FQuery := Self;
  ParamsFromVars := True;
  FAllowToNotifyFormulas := False;
end;

destructor TrmDBTableQuery.Destroy;
begin
  if Assigned(FTable) then
    FTable.FQuery := nil;
  inherited;
end;

procedure TrmDBTableQuery.DoAfterClose;
var
  i: Integer;
begin
  for i := 0 to Table.Groups.Count - 1 do
    Table.Groups[i].ClearCalcInfo;

  inherited;
end;

procedure TrmDBTableQuery.DoAfterOpen;
var
  i: Integer;
begin
  for i := 0 to Table.Groups.Count - 1 do
    if not Table.Groups[i].Disabled then
      Table.Groups[i].PrepareToCalc;

  inherited;
end;

procedure TrmDBTableQuery.DoBeforeOpen;
var
  srt, h: String;
  i: Integer;
begin
  srt := '';
  for i := 0 to Table.Groups.Count - 1 do
    if not Table.Groups[i].Disabled then
    begin
      if srt <> '' then
        srt := srt + ',';
      srt := srt + Table.Groups[i].FFields.CommaText;
    end;

  Table.FGrouping := srt <> '';
  if Table.FGrouping then
  begin
    h := QueryBuilderInfo.SortFields.SortSQLCondition;
    if h <> '' then
      srt := srt + ', ' + h;
    Sort := srt;
  end;

  inherited;
end;

procedure TrmDBTableQuery.OnAssignQBInfo(Sender: TObject);
begin
  inherited;
  if FTable.IsDesignMode then
    FTable.GetDesigner.Notify(rmdQueryChanged, Integer(Pointer(Self as IrmDesignObject)))
end;


{ TrmDBTable }

constructor TrmDBTable.Create(AOwner: TComponent);
begin
  FGroups := TrmDBTableGroups.Create(Self);
  inherited;
  FVirtOwner := True;
  FAllowedResizingBounds := [rmdMPLeftBound, rmdMPRightBound, rmdMPTopBound, rmdMPBottomBound];
  MultiPaged := True;

  FMinWorkAreaSize := Round(ConvertUnit(15, utScreenPixels, utLogicalPixels));
  FHeight := FMinWorkAreaSize;

  FHeaderBand  := nil;
  FTotalBand := nil;
  FQuery := nil;
end;

destructor TrmDBTable.Destroy;
begin
  FreeAndNil(FGroups);
  FreeAndNil(FRendContent);
  inherited;
end;

procedure TrmDBTable.InitializeForDesign;
var
  i: Integer;
begin
  inherited;

  if ControlCount = 0 then
  begin
    AddBand(rwTBTDetail);
    if Owner is TrmCustomTable then
    begin
      Description := 'SubTable';
    end
    else
    begin
      AddBand(rwTBTHeader);
      AddBand(rwTBTTotal);
    end;
  end

  else
    for i := 0 to ControlCount - 1 do
      TrmDBTableBand(Controls[i]).InitializeForDesign;

  if not (Owner is TrmCustomTable) then
    SetBounds(Left, Top, 2000, 1000);
end;

procedure TrmDBTable.ReadState(Reader: TReader);
var
  PrevOwner: TComponent;
  PrevRoot: TComponent;
  PrevParent: TComponent;
  i: Integer;
begin
  i := 0;
  while i < ComponentCount do
    if not TrwComponent(Components[i]).InheritedComponent then
      Components[i].Free
    else
      Inc(i);

  PrevOwner := Reader.Owner;
  PrevParent := Reader.Parent;
  PrevRoot := Reader.Root;
  Reader.Owner := Self;
  Reader.Root := Self;
  Reader.Parent := nil;
  inherited;
  Reader.Owner := PrevOwner;
  Reader.Root := PrevRoot;
  Reader.Parent := PrevParent;
end;

procedure TrmDBTable.WriteState(Writer: TWriter);
var
  OldRoot, OldRootAncestor: TComponent;
begin
  OldRootAncestor := Writer.RootAncestor;
  OldRoot := Writer.Root;
  try
    if Assigned(OldRootAncestor) then
      Writer.RootAncestor := OldRootAncestor.FindComponent(Name);
    if not Assigned(Writer.RootAncestor) then
      Writer.RootAncestor := Self;
    Writer.Root := Self;
    inherited;
  finally
    Writer.Root := OldRoot;
    Writer.RootAncestor := OldRootAncestor;
  end;
end;

function TrmDBTable.AddBand(const ABandType: TrmDBTableBandType; AGroup: TrmDBTableGroup = nil): TrmDBTableBand;

  procedure FindGroupBandPosition(ABand: TrmDBTableCustomGroupBand);
  var
    n, i: Integer;
  begin
    if ABandType = rwTBTGroupHeader then
    begin
      ABand.FTopMostRow := FDetailBand.TopMostRow;

      n := AGroup.Index + 1;
      for i := n to Groups.Count - 1 do
        if Assigned(Groups[i].FHeaderBand) then
        begin
          ABand.FTopMostRow := Groups[i].FHeaderBand.TopMostRow;
          break;
        end;
    end

    else if ABandType = rwTBTGroupFooter then
    begin
      ABand.FTopMostRow := FDetailBand.BottomMostRow + 1;

      n := AGroup.Index + 1;
      for i := n to Groups.Count - 1 do
        if Assigned(Groups[i].FFooterBand) then
        begin
          ABand.FTopMostRow := Groups[i].FFooterBand.BottomMostRow + 1;
          break;
        end;
    end;

    ABand.FBottomMostRow := ABand.FTopMostRow;
  end;

begin
  if BandByType(ABandType, AGroup) = nil then
  begin
    case ABandType of
      rwTBTDetail:
        begin
          Result := TrmDBTableDetailBand.Create(Self);
          Result.Name := GetUniqueNameForComponent(Result, True, 'DetailBand', '', True);
          Result.Description := 'Detail Band';
          Result.FTopMostRow := 0;   //exception, since Detail Band gets created first automatically
          Result.FBottomMostRow := 0;
        end;

      rwTBTHeader:
        begin
          Result := TrmDBTableHeaderBand.Create(Self);
          Result.Name := GetUniqueNameForComponent(Result, True, 'HeaderBand', '', True);
          Result.Description := 'Header Band';
          Result.FTopMostRow := 0;
          Result.FBottomMostRow := 0;
        end;

      rwTBTTotal:
        begin
          Result := TrmDBTableTotalBand.Create(Self);
          Result.Name := GetUniqueNameForComponent(Result, True, 'TotalBand', '', True);
          Result.Description := 'Total Band';
          Result.FTopMostRow := FTableInfo.RowsCount;
          Result.FBottomMostRow := FTableInfo.RowsCount;
        end;

      rwTBTGroupHeader:
        begin
          Assert(Assigned(AGroup));
          Result := TrmDBTableGroupHeaderBand.Create(Self);
          TrmDBTableGroupHeaderBand(Result).FGroupID := AGroup.ItemID;
          TrmDBTableGroupHeaderBand(Result).FGroup := AGroup;
          Result.Name := GetUniqueNameForComponent(Result, True, 'GroupHeader', '', True);
          FindGroupBandPosition(TrmDBTableCustomGroupBand(Result));
          AGroup.FHeaderBand := TrmDBTableGroupHeaderBand(Result);
        end;

      rwTBTGroupFooter:
        begin
          Assert(Assigned(AGroup));
          Result := TrmDBTableGroupFooterBand.Create(Self);
          TrmDBTableGroupFooterBand(Result).FGroupID := AGroup.ItemID;
          TrmDBTableGroupFooterBand(Result).FGroup := AGroup;
          Result.Name := GetUniqueNameForComponent(Result, True, 'GroupFooter', '', True);
          FindGroupBandPosition(TrmDBTableCustomGroupBand(Result));
          AGroup.FFooterBand := TrmDBTableGroupFooterBand(Result);
        end;

    else
      Result := nil;
    end;

    Result.CreateInitialCell;
    if Height < Result.Height + FMinWorkAreaSize then
      Height := Height + (Result.Height + FMinWorkAreaSize - Height);
    Result.Container := Self;

    if IsDesignMode then
    begin
      Result.InitializeForDesign;
      Result.SetDesigner(GetDesigner);
      SyncTagPos;
    end;
  end

  else
    Result := nil;
end;

function TrmDBTable.BandByType(const ABandType: TrmDBTableBandType; AGroup: TrmDBTableGroup = nil): TrmDBTableBand;
begin
  case ABandType of
    rwTBTDetail:      Result := FDetailBand;
    rwTBTHeader:      Result := FHeaderBand;
    rwTBTTotal:       Result := FTotalBand;
    rwTBTGroupHeader: Result := AGroup.FHeaderBand;
    rwTBTGroupFooter: Result := AGroup.FFooterBand;
  else
    Result := nil;
  end;
end;

procedure TrmDBTable.Loaded;
var
  i: Integer;
  Grp: TrmDBTableGroup;
begin
  inherited;
  FQuery.SQL.Text := QueryBuilderInfo.SQL;

  //link group bands
  for i := 0 to ControlCount - 1 do
    if Controls[i] is TrmDBTableCustomGroupBand then
    begin
      Grp := TrmDBTableGroup(Groups.FindItemByID(TrmDBTableCustomGroupBand(Controls[i]).GroupID));
      TrmDBTableCustomGroupBand(Controls[i]).FGroup := Grp;
      if Controls[i] is TrmDBTableGroupHeaderBand then
        Grp.FHeaderBand := TrmDBTableGroupHeaderBand(Controls[i])
      else if Controls[i] is TrmDBTableGroupFooterBand then
        Grp.FFooterBand := TrmDBTableGroupFooterBand(Controls[i])
      else
        Assert(False);
    end;
end;

procedure TrmDBTable.GetDataFromDataSource;
var
  i, j: Integer;
  B: TrmDBTableBand;
begin
  for i := 0 to ControlCount -1 do
  begin
    B := TrmDBTableBand(Controls[i]);
    B.FChanging := True;
  end;

  try

    for i := 0 to ControlCount -1 do
    begin
      B := TrmDBTableBand(Controls[i]);
      for j := 0 to B.ControlCount - 1 do
        TrmTableCell(B.Controls[j]).SetValueFromDataSource;
    end;

  finally
    for i := 0 to ControlCount -1 do
    begin
      B := TrmDBTableBand(Controls[i]);
      B.FChanging := False;
    end;
  end;
end;

procedure TrmDBTable.InitializeTableInfo;
begin
  FTableInfo := TrmTableInfo.Create;
  FTableInfo.FOwner := Self;
end;


procedure TrmDBTable.AfterConstruction;
begin
  inherited;
  FQuery := TrmDBTableQuery.Create(Self);
end;

procedure TrmDBTable.UpdateCellBoundsInfo;
var
  i: Integer;
begin
  for i := 0 to ControlCount - 1 do
    TrmDBTableBand(Controls[i]).UpdateCellBoundsInfo;


  if ControlCount = 0 then
  begin
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TrmDBTableBand then
        Exit;

    FTableInfo.Clear;
  end;
end;

function TrmDBTable.GetQueryBuilderInfo: TrwQBQuery;
begin
  Result := FQuery.QueryBuilderInfo;
end;

procedure TrmDBTable.SetQueryBuilderInfo(const Value: TrwQBQuery);
begin
  FQuery.QueryBuilderInfo.Assign(Value);
end;

function TrmDBTable.GetParamsFromVars: Boolean;
begin
  Result := FQuery.ParamsFromVars;
end;

procedure TrmDBTable.SetParamsFromVars(const Value: Boolean);
begin
  FQuery.ParamsFromVars := Value;
end;

procedure TrmDBTable.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  BeforeChangePos;
  CheckSizeConstrains(ALeft, ATop, AWidth, AHeight);
  FLeft := ALeft;
  FTop := ATop;
  FHeight := AHeight;
  if Assigned(FTableInfo) then
    SetTableWidth(AWidth);
  AfterChangePos;
end;


procedure TrmDBTable.PaintForeground(ACanvas: TrwCanvas);
var
  R: TRect;
  h: Integer;
begin
  inherited;

  with ACanvas do
  begin
    R := GetZoomedWorkAreaRect(ACanvas);

    if not IsSubTable then
    begin
      Pen.Width := 1;
      Pen.Mode := pmCopy;
      Pen.Color := clGray;
      Pen.Style := psDot;
      Brush.Color := clNone;
      Brush.Style := bsClear;
      MoveTo(R.Left, R.Top);
      LineTo(R.Left, R.Bottom);
      MoveTo(R.Left, R.Bottom - 1);
      LineTo(R.Right, R.Bottom -1);
      MoveTo(R.Right - 1, R.Bottom);
      LineTo(R.Right - 1, R.Top);
    end;

    Font.Name := 'Arial';
    h := Zoomed(69);
    Font.Height := -h;
    Font.Color := clSilver;
    Font.Style := [fsItalic];
    Brush.Style := bsClear;

    DrawText(GetShowText, R, DT_CENTER or DT_VCENTER or DT_SINGLELINE);
  end;
end;

function TrmDBTable.GetWorkAreaRect: TRect;
var
  i, n: Integer;
begin
  Result := ClientRect;
  n := 0;
  for i := 0 to ControlCount - 1 do
    if n < Controls[i].BoundsRect.Bottom then
      n := Controls[i].BoundsRect.Bottom;

  Inc(Result.Top, n);
end;


function TrmDBTable.GetPosition(Index: Integer): Integer;
begin
  if Index = 3 then
    Result :=  FHeight
  else
    Result := inherited GetPosition(Index);
end;

procedure TrmDBTable.SetPosition(Index, Value: Integer);
var
  l, t, w: Integer;
begin
  if Loading then
  begin
    if Index = 3 then
    begin
      BeforeChangePos;
      l := Left;
      t := Top;
      w := Width;
      CheckSizeConstrains(l, t, w, Value);
      FHeight := Value;
      AfterChangePos;
    end
    else
      inherited;
  end

  else
  begin
    if Index in [0, 1] then
      inherited
    else if Index = 3 then
      FHeight := Value;
  end;
end;

function TrmDBTable.CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean;
var
  ts: Integer;

  procedure dbTableLogic;
  var
    h: Integer;
  begin
    h := inherited GetPosition(3);

    if IsDesignMode then
      Inc(h, FMinWorkAreaSize);

    if h > AHeight then
    begin
      AHeight := h;
      Result := False;
    end
    else
      Result := True;

    h := AHeight;
    Result := inherited CheckSizeConstrains(ALeft, ATop, AWidth, AHeight) and Result;
    AHeight:= h;
  end;

begin
  if IsSubTable then
  begin
    ALeft := 0;
    ATop := 0;

    if RendStatus = rmRWARSCalculated then
    begin
      Result := True;

      ts := Width;
      // Calculated Table can only grow and never shrink
      if AWidth < ts then
      begin
        AWidth := ts;
        Result := False;
      end;
      ts := Height;

      if Aheight < Height then
      begin
        AHeight := ts;
        Result := False;
      end;
    end

    else if not IsDesignMode then
      Result := True

    else if not FSBTStandAloneMode and (CellOwner <> nil) then
    begin
      AWidth := CellOwner.ClientWidth;
      AHeight := CellOwner.ClientHeight;
      Result := True;
    end

    else
      dbTableLogic;
  end

  else
    dbTableLogic;
end;


procedure TrmDBTable.AfterChangePos;
var
  h: Integer;
begin
  if IsDesignMode then
  begin
    h := inherited GetPosition(3) + FMinWorkAreaSize;
    if h > FHeight then
      FHeight := h;
  end;

  inherited;

  if not Loading and IsSubTable then
    if (CellOwner <> nil) and not FSBTStandAloneMode then
    begin
      FSBTStandAloneMode := True;
      try
        CellOwner.SetBounds(0, 0, Width, Height);
      finally
        FSBTStandAloneMode := False;
      end;
    end;
end;

procedure TrmDBTable.DoCalculate;
var
  DetailBand: TrmDBTableBand;
  fl_close_ds: Boolean;
  R: TRect;
  FDataSetIsEmpty: Boolean;
  i: Integer;

  procedure CalcBand(const ABand: TrmDBTableBand);
  begin
    if not Assigned(ABand) or FDataSetIsEmpty and not ABand.PrintIfNoData then
      Exit;
    ABand.Calculate;
    if ABand.RendStatus = rmRWARSCalculated then
      FRendContent.StoreBand(ABand);
    TableInfo.RestoreState;
  end;

  procedure CalcGroupHeaders;
  var
    i, j: Integer;
  begin
    for i := 0 to Groups.Count - 1 do
      if not Groups[i].Disabled and Groups[i].IsGroupBeginning then
      begin
        for j := i to Groups.Count - 1 do
        begin
          if Groups[j].Disabled then
            Continue;
          Groups[j].StoreGroupValues;
          if Assigned(Groups[j].FHeaderBand) then
            CalcBand(Groups[j].FHeaderBand);
        end;

        break;
      end;
  end;

  procedure CalcGroupFooters;
  var
    i, j: Integer;
  begin
    if FGrouping then
    begin
      DataSource.PDisableControls;
      try
        DataSource.PNext;

        for i := 0 to Groups.Count - 1 do
          if not Groups[i].Disabled and (DataSource.PEof or Groups[i].IsGroupChanged) then
          begin
            if not DataSource.PEof then
              DataSource.PPrior;

            for j := Groups.Count - 1 downto i do
            begin
              if Groups[j].Disabled then
                Continue;
              if Assigned(Groups[j].FFooterBand) then
                CalcBand(Groups[j].FFooterBand);
              Groups[j].StartNewGroup;
            end;

            if not DataSource.PEof then
              DataSource.PNext;

            // Page Break
            for j := Groups.Count - 1 downto i do
              if Groups[j] = Groups.FPageBreakGroup then
              begin
                FRendContent.StorePageBreak;
                break;
              end;

            break;
          end;

      finally
        DataSource.PEnableControls;
      end;
    end

    else
      DataSource.PNext;
  end;


begin
  if not Assigned(DataSource) then
    Exit;

  if IsSubTable then
  begin
    SetRendStatusForChildren(rmRWARSNone);
    if not DataSource.PActive then
      DataSource.Open;

    FSBTStandAloneMode := True;
  end;

  TableInfo.StoreState;

  if not Assigned(FRendContent) then
    FRendContent := TrmDBTableRenderContent.Create;

  CalcCurrentTblVersion;
  FRendContent.CurrentTblVersion := FCurrentTblVersion;

  // Open linked DataSet or set it on first row
  fl_close_ds := not DataSource.PActive;
  if not fl_close_ds then
    DataSource.PFirst
  else
    DataSource.Open;

  NotifyAggregateFunctions;
  GetDataFromDataSource;
  FRendLastBandPos := 0;
  DetailBand := BandByType(rwTBTDetail);
  FDataSetIsEmpty := DataSource.PRecordCount = 0;

  if FDataSetIsEmpty then
  begin
    CalcBand(BandByType(rwTBTHeader));
    CalcBand(BandByType(rwTBTTotal));

    // Disable printing for not calculated bands
    for i := 0 to ControlCount - 1 do
      if Controls[i].RendStatus = rmRWARSNone then
        (Controls[i] as TrmDBTableBand).SetRendStatusChildrenToo(rmRWARSPrinted);
  end
  else
  begin
    CalcBand(BandByType(rwTBTHeader));

    while not DataSource.PEof do
    begin
      CalcGroupHeaders;

      CalcBand(DetailBand);

      CalcGroupFooters;

      if not DataSource.PEof then
      begin
        DetailBand.SetRendStatusChildrenToo(rmRWARSNone);
        GetDataFromDataSource;
      end;
    end;
    CalcBand(BandByType(rwTBTTotal));
  end;

  // close DataSet if it was opened automatically
  if fl_close_ds then
    DataSource.Close;

  if not MultiPaged then
  begin
    R := BoundsRect;
    R.Bottom := R.Top + FRendLastBandPos;
    SetBoundsRect(R);
  end;


  if IsSubTable then
    FSBTStandAloneMode := False;
end;


procedure TrmDBTable.DoPrint;
begin
  PrepareForReadingContent;
  CalcCutting;
  CalcPositionOnCurrentPage;
  CreateRWAObject;
  PrintBands;
  PrepareForNextPrintCycle;
end;

procedure TrmDBTable.PrintBands;
var
  Grp: TrmDBTableGroup;

  procedure PrintBand(ABand: TrmDBTableBand);
  var
    R: TRect;
  begin
    R := ABand.CalculatedBounds;
    OffsetRect(R, 0, FRendLastBandPos - R.Top);
    ABand.CalculatedBounds := R;
    ABand.Print;
  end;

  procedure PrintCurrentBand;
  begin
    if not Assigned(FRendCurrentBand) or (FRendCurrentBand.RendStatus = rmRWARSPrinted) then
      Exit;

    FRendContent.ReadBand(FRendCurrentBand);  // read band content
    PrintBand(FRendCurrentBand);              // and print it
  end;

  function TimeForExit: Boolean;
  begin
    Result := not Assigned(FRendContent) or FRendContent.Eof or (Assigned(FRendCurrentBand) and (FRendCurrentBand.RendStatus <> rmRWARSPrinted));
  end;


  procedure ForgetPreviousBand;
  begin
    if Assigned(FRendCurrentBand) and (FRendCurrentBand.RendStatus = rmRWARSPrinted) then
    begin
      FRendLastBandPos := FRendCurrentBand.CalculatedBounds.Bottom;
      FRendCurrentBand := nil;
    end;
  end;


  procedure PrintHeaders;
  var
    i: Integer;
    Bnd: TrmDBTableBand;
  begin
    FRendLastBandPos := FTopCutClientPosition - GetEachPageHeaderHeight;

    // Print Table Header
    if Assigned(FHeaderBand) and (FHeaderBand.RendStatus = rmRWARSCalculated) then
    begin
      PrintBand(FHeaderBand);
      FRendLastBandPos := FHeaderBand.CalculatedBounds.Bottom;
    end;

    // Print Group Headers
    if Assigned(FRendCurrentBand) and (FRendCurrentBand.BandType in [rwTBTDetail, rwTBTGroupFooter]) then
    begin
      for i := 0 to Groups.Count - 1 do
      begin
        Bnd := Groups[i].FHeaderBand;
        if Assigned(Bnd) and (Bnd.RendStatus = rmRWARSCalculated) then
        begin
          PrintBand(Bnd);
          FRendLastBandPos := Bnd.CalculatedBounds.Bottom;
        end;
      end
    end

    else
      if FRendContent.Eof then
        for i := 0 to Groups.Count - 1 do
        begin
          Bnd := Groups[i].FHeaderBand;
          if Assigned(Bnd) and (Bnd.RendStatus = rmRWARSCalculated) then
            Bnd.SetRendStatusChildrenToo(rmRWARSPrinted); // In case when FRendContent has reached the end
        end;
  end;

begin
  if RendStatus = rmRWARSPrinting then
    PrintHeaders;

  if Assigned(FRendCurrentBand) then
  begin
    // finish previously cut band
    FRendCurrentBand.Print;
    ForgetPreviousBand;
  end;

  if TimeForExit then
    Exit;

  // print table content
  repeat
    // if Page Break met
    if FRendContent.BandType = rwTBTPageBreak then
    begin
      repeat
        FRendContent.Next;
      until TimeForExit or (FRendContent.BandType <> rwTBTPageBreak);

      if TimeForExit then
        break;

      FRendLastBandPos := FBottomCutClientPosition; // artificially skip to the bottom
    end;

    if FRendContent.GroupNbr = -1 then
      Grp := nil
    else
      Grp := Groups[FRendContent.GroupNbr];
    FRendCurrentBand := BandByType(FRendContent.BandType, Grp);
    Assert(Assigned(FRendCurrentBand));
    FRendCurrentBand.SetRendStatusChildrenToo(rmRWARSCalculated); // let's pretend it is not printed yet
    PrintCurrentBand;
    ForgetPreviousBand;

    FRendContent.Next; // go to next calculated band
  until TimeForExit;
end;

procedure TrmDBTable.CreateRWAObject;
begin
end;

procedure TrmDBTable.OnPageCalculated;
begin
  CalculatedBounds := BoundsRect;
end;

procedure TrmDBTable.CalcCurrentTblVersion;

  procedure ForCells(TblBnd: TrmDBTableBand); forward;

  procedure ForBands(Tbl: TrmDBTable);
  var
    i: Integer;
  begin
    for i := 0 to ControlCount - 1 do
      ForCells(TrmDBTableBand(Controls[i]));
  end;

  procedure ForCells(TblBnd: TrmDBTableBand);
  var
    i: Integer;
  begin
    for i := 0 to ControlCount - 1 do
      if TrmTableCell(Controls[i]).SubTable <> nil then
      begin
        TrmTableCell(Controls[i]).SubTable.FCurrentTblVersion := 0;
        ForBands(TrmTableCell(Controls[i]).SubTable);
      end;
  end;

begin
  if IsSubTable then
    Inc(FCurrentTblVersion)
  else
  begin
    FCurrentTblVersion := 1;
    ForBands(Self);
  end;
end;

procedure TrmDBTable.SyncCellsByQuery;
var
  DetailBnd, HeaderBnd, TotalBand: TrmDBTableBand;
  C: TrmTableCell;
  i: Integer;
  fld: String;
  SF: TrwQBShowingField;

  procedure UpdateCells(Bnd: TrmDBTableBand);
  var
    C1, C2: TrmTableCell;
    n: Integer;
  begin
    C1 := TrmTableCell(Bnd.TableInfo.Cell(Bnd.LeftMostCol, Bnd.TopMostRow));
    C2 := TrmTableCell(Bnd.TableInfo.Cell(Bnd.RightMostCol, Bnd.BottomMostRow));
    if C1 <> C2 then
      Bnd.MergeCells(C1, C2);

    n := FQuery.QueryBuilderInfo.ShowingFields.Count - Bnd.ControlCount;
    if n > 0 then
    begin
      Bnd.TableInfo.Cell(Bnd.LeftMostCol, Bnd.TopMostRow);
      Bnd.SplitCell(C1, n + 1, 1);
    end;
  end;

begin
  DetailBnd := BandByType(rwTBTDetail);
  HeaderBnd := BandByType(rwTBTHeader);
  TotalBand := BandByType(rwTBTTotal);

  // update Detail
  UpdateCells(DetailBnd);

  C := nil;
  for i := 0 to DetailBnd.ControlCount - 1 do
  begin
    if i = 0 then
      C := TrmTableCell(DetailBnd.TableInfo.Cell(DetailBnd.LeftMostCol, DetailBnd.TopMostRow))
    else
      C := TrmTableCell(DetailBnd.TableInfo.Cell(C.CellRect.Right + 1, DetailBnd.TopMostRow));
    SF := FQuery.QueryBuilderInfo.ShowingFields[i];
    fld := SF.GetFieldName(False, False);
    C.Name := GetUniqueNameForComponent(C, True, 'c' + Fld, '', True);
    C.DataField := fld;
    C.Description := SF.GetFieldName(True, False);

    case SF.ExprType of
      rwvInteger:
        begin
          C.Alignment := taRightJustify;
          C.Format := '#,###.##';
        end;

      rwvString:
        begin
          C.Alignment := taLeftJustify;
          C.Format := '';
        end;

      rwvFloat:
        begin
          C.Alignment := taRightJustify;
          C.Format := '#,##0.#####';
        end;

      rwvDate:
        begin
          C.Alignment := taLeftJustify;
          C.Format := 'dd/mm/yyyy';
        end;

      rwvCurrency:
        begin
          C.Alignment := taRightJustify;
          C.Format := '#,##0.00';
        end;
    end;
  end;
  DetailBnd.Height := DetailBnd.Height + 1;
  DetailBnd.Height := DetailBnd.Height - 1;


  // update Header
  if Assigned(HeaderBnd) then
  begin
    HeaderBnd.Font.Style := HeaderBnd.Font.Style + [fsBold];
    UpdateCells(HeaderBnd);
    for i := 0 to HeaderBnd.ControlCount - 1 do
    begin
      if i = 0 then
        C := TrmTableCell(HeaderBnd.TableInfo.Cell(HeaderBnd.LeftMostCol, HeaderBnd.TopMostRow))
      else
        C := TrmTableCell(HeaderBnd.TableInfo.Cell(C.CellRect.Right + 1, HeaderBnd.TopMostRow));
      C.Name := GetUniqueNameForComponent(C, True, 'cTitle', '', True);
      C.Description := 'Cell';
      C.Value := FQuery.QueryBuilderInfo.ShowingFields[i].GetFieldName(True, False);
      C.Alignment := taCenter;
    end;
    HeaderBnd.Height := HeaderBnd.Height + 1;
    HeaderBnd.Height := HeaderBnd.Height - 1;
  end;

  if Assigned(TotalBand) then
  begin
    TotalBand.Font.Style := TotalBand.Font.Style + [fsBold];
    UpdateCells(TotalBand);
    for i := 0 to TotalBand.ControlCount - 1 do
    begin
      if i = 0 then
        C := TrmTableCell(TotalBand.TableInfo.Cell(TotalBand.LeftMostCol, TotalBand.TopMostRow))
      else
        C := TrmTableCell(TotalBand.TableInfo.Cell(C.CellRect.Right + 1, TotalBand.TopMostRow));
      C.Name := GetUniqueNameForComponent(C, True, 'cTotal', '', True);
      C.Description := 'Cell';
    end;
    TotalBand.Height := TotalBand.Height + 1;
    TotalBand.Height := TotalBand.Height - 1;
  end;
end;


function TrmDBTable.CallCustomFunction(const AFunctionName: String; const AParams: array of Variant): Variant;
var
  Bnd: TrmDBTableBand;
  BndType: TrmDBTableBandType;
begin
  if SameText(AFunctionName, 'ChangeBands') then
  begin
    Result := 0;

    if SameText(AParams[0], 'Header') then
      BndType := rwTBTHeader
    else if SameText(AParams[0], 'Total') then
      BndType := rwTBTTotal
    else if SameText(AParams[0], 'Detail') then
      BndType := rwTBTDetail
    else
      Exit;

    if AParams[1] then
    begin
      Bnd := AddBand(BndType);
      if Assigned(Bnd) then
        Result := Integer(Pointer(Bnd as IrmDesignObject));
    end
    else
    begin
      Bnd := BandByType(BndType);
      FreeAndNil(Bnd);
    end;
  end

  else if SameText(AFunctionName, 'GetBandByType') then
  begin
    Result := 0;

    if SameText(AParams[0], 'Header') then
      BndType := rwTBTHeader
    else if SameText(AParams[0], 'Detail') then
      BndType := rwTBTDetail
    else if SameText(AParams[0], 'Total') then
      BndType := rwTBTTotal
    else
      Exit;

    Bnd := BandByType(BndType);
    Result := Integer(Pointer(Bnd as IrmDesignObject));
  end

  else
    Result := inherited CallCustomFunction(AFunctionName, AParams);
end;

procedure TrmDBTable.PropogateGlobVarsToChildren;
var
  i: Integer;
begin
  if Assigned(GlobalVarFunc) then
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TrmDBTableBand then
      begin
        TrmDBTableBand(Components[i]).SetExternalGlobalVarFunc(GlobalVarFunc);
        TrmDBTableBand(Components[i]).PropogateGlobVarsToChildren;
      end;
end;

procedure TrmDBTable.UpdateCellTableInfo;
var
  i: Integer;
begin
  for i := 0 to ControlCount - 1 do
    TrmDBTableBand(Controls[i]).UpdateCellTableInfo;
end;

procedure TrmDBTable.PrepareForReadingContent;
begin
  if (RendStatus = rmRWARSCalculated) and Assigned(FRendContent) then
  begin
    FRendContent.CurrentTblVersion := FCurrentTblVersion;
    FRendContent.First;
    FRendCurrentBand := nil;
    FRendLastBandPos := 0;
  end;
end;

procedure TrmDBTable.ReStorePrintState(const AStream: IEvDualStream);
var
  h: String;
begin
  inherited;

  h := AStream.ReadString;

  if h <> '' then
    FRendCurrentBand := TrmDBTableBand(FindComponent(h))
  else
    FRendCurrentBand := nil;

  FRendLastBandPos := AStream.ReadInteger;
  FCurrentTblVersion := AStream.ReadInteger;
  FRendContent.RestoreState(AStream);
end;

procedure TrmDBTable.StorePrintState(const AStream: IEvDualStream);
var
  h: String;
begin
  inherited;

  if Assigned(FRendCurrentBand) then
    h := FRendCurrentBand.Name
  else
    h := '';
  AStream.WriteString(h);
  AStream.WriteInteger(FRendLastBandPos);
  AStream.WriteInteger(FCurrentTblVersion);
  FRendContent.StoreState(AStream);
end;

function TrmDBTable.GetZoomedWorkAreaRect(ACanvas: TrwCanvas): TRect;
var
  P: TPoint;
  F: TrmPrintForm;
begin
  F := PrintForm;
  GetWindowOrgEx(ACanvas.Handle, P);

  Result := GetWorkAreaRect;
  Result := ClientToForm(Result);
  OffsetRect(Result, F.Left, F.Top);
  Result := Zoomed(Result);
  OffsetRect(Result, P.X, P.Y);
end;

procedure TrmDBTable.SyncTagPos;
var
  i: Integer;
begin
  if not FChangingTags then
  begin
    FChangingTags := True;
    try
      inherited;
      for i := 0 to ControlCount - 1 do
        TrmDBTableBand(Controls[i]).SyncTagPos;
    finally
      FChangingTags := False;
    end;
  end;

  if Assigned(FSBTDesignBorder) then
    FSBTDesignBorder.UpdateRegion;
end;

procedure TrmDBTable.HideTag;
var
  i: Integer;
begin
  inherited;

  for i := 0 to ControlCount - 1 do
    TrmDBTableBand(Controls[i]).HideTag;

  if Assigned(FSBTDesignBorder) then
    FreeAndNil(FSBTDesignBorder);
end;

procedure TrmDBTable.ShowTag;
var
  i: Integer;
begin
  if IsSubTable and not Assigned(FSBTDesignBorder) then
    FSBTDesignBorder := TrmdTableBorder.Create(Self);

  inherited;

  for i := 0 to ControlCount - 1 do
    TrmDBTableBand(Controls[i]).ShowTag;

  ObjectTag.AllowMove := not IsSubTable;
end;

procedure TrmDBTable.SetColor(Value: TColor);
var
  i: Integer;
  Clr: TColor;
begin
  Clr := Color;
  inherited;

  for i := 0 to ComponentCount - 1 do
    if (Components[i] is TrmDBTableBand) and (TrmDBTableBand(Components[i]).Color = Clr) then
      TrmDBTableBand(Components[i]).Color := Value;

  if IsSubTable and (CellOwner.Color <> Value) then
    CellOwner.Color := Value;
end;

function TrmDBTable.GetGroups: IrmDBTableGroups;
begin
  Result := Groups;
end;

procedure TrmDBTable.UpdateGroupBandsPosition;
var
  Grp: TrmDBTableGroup;
  BandEdgeRow, Delta, i, j: Integer;
  Band: TrmDBTableBand;
  C: TrmTableCell;
  R: TRect;
begin
  if Loading or not Assigned(FDetailBand) then
    Exit;

  //Header Bands
  BandEdgeRow := FDetailBand.TopMostRow - 1;
  for i := Groups.Count - 1 downto 0 do
  begin
    Grp := Groups[i];
    Band := Grp.FHeaderBand;
    if Assigned(Band) then
    begin
      if Band.FBottomMostRow <> BandEdgeRow then
      begin
        Delta := BandEdgeRow - Band.FBottomMostRow;

        //Move Cell Rows
        if Delta > 0 then
          for j := Band.FTopMostRow to Band.BottomMostRow do
            TableInfo.FRowHeights.Move(j, j + Delta);

        // Update Cells
        Inc(Band.FTopMostRow, Delta);
        Inc(Band.FBottomMostRow, Delta);
        for j := 0 to Band.ControlCount - 1 do
        begin
          C := TrmTableCell(Band.Controls[j]);
          R := C.FCellRect;
          OffsetRect(R, 0, Delta);
          C.FCellRect := R;
        end;
      end;
      BandEdgeRow := Band.TopMostRow - 1;
    end;
  end;


  //Footer Bands
  BandEdgeRow := FDetailBand.BottomMostRow + 1;
  for i := Groups.Count - 1 downto 0 do
  begin
    Grp := Groups[i];
    Band := Grp.FFooterBand;
    if Assigned(Band) then
    begin
      if Band.FTopMostRow <> BandEdgeRow then
      begin
        Delta := BandEdgeRow - Band.FBottomMostRow;

        //Move Cell Rows
        if Delta < 0 then
          for j := Band.FTopMostRow to Band.BottomMostRow do
            TableInfo.FRowHeights.Move(j, j + Delta);

        // Update Cells
        Inc(Band.FTopMostRow, Delta);
        Inc(Band.FBottomMostRow, Delta);
        for j := 0 to Band.ControlCount - 1 do
        begin
          C := TrmTableCell(Band.Controls[j]);
          R := C.FCellRect;
          OffsetRect(R, 0, Delta);
          C.FCellRect := R;
        end;
      end;
      BandEdgeRow := Band.BottomMostRow + 1;
    end;  
  end;

  SyncTagPos;
end;

procedure TrmDBTable.DefineProperties(Filer: TFiler);
var
  TblAncestor: TrmDBTable;
  fl_store_order: Boolean;
  h1, h2: String;
begin
  inherited;
  FGroupsRW := TrmDBTableGroups.Create(Self);
  try
    fl_store_order := False;
    if Filer is TWriter then
    begin
      FGroupsRW.Assign(FGroups);
      TblAncestor := TrmDBTable(TWriter(Filer).Ancestor);
      if Assigned(TblAncestor) then
      begin
        FGroupsRW.MakeDelta(TblAncestor.Groups);
        h1 := FGroups.GetItemOrder;
        h2 := TblAncestor.Groups.GetItemOrder;
        fl_store_order := not AnsiSameStr(Copy(h1, 1, Length(h2)), h2);
      end;
    end;

    Filer.DefineProperty('Groups', ReadGroups, WriteGroups, FGroupsRW.Count > 0);
    Filer.DefineProperty('GroupOrder', ReadGroupOrder, WriteGroupOrder, fl_store_order);
    Filer.DefineProperty('PageBreakGroup', ReadPageBreakGroup, WritePageBreakGroup, Assigned(FGroupsRW.FPageBreakGroup) or IsInheritedComponent);
  finally
    FreeAndNil(FGroupsRW);
  end;
end;

procedure TrmDBTable.ReadGroups(Reader: TReader);
begin
  Reader.ReadValue;
  Reader.ReadCollection(FGroupsRW);

  FGroups.ApplyDelta(FGroupsRW);
end;

procedure TrmDBTable.WriteGroups(Writer: TWriter);
begin
  Writer.WriteCollection(FGroupsRW);
end;


procedure TrmDBTable.SetInheritedComponentProperty(AValue: Boolean);
begin
  inherited;
  Groups.SetInherited(AValue);
end;

procedure TrmDBTable.AfterInitLibComponent;
begin
  inherited;
  Groups.SetInherited(True);
end;

procedure TrmDBTable.ReadGroupOrder(Reader: TReader);
begin
  FGroups.SetItemOrder(Reader.ReadString);
end;

procedure TrmDBTable.WriteGroupOrder(Writer: TWriter);
begin
  Writer.WriteString(FGroups.GetItemOrder);
end;

function TrmDBTable.GetDataSource: TrwDataSet;
begin
  Result := FQuery;
end;

function TrmDBTable.PreCutChildren: Boolean;
var
  TopPos, BottomPos: Integer;
  Bnd: TrmDBTableBand;
  MS: IEvDualStream;
  Grp: TrmDBTableGroup;
  R: TRect;
  LastStoredBandPos: Integer;
  CutAmd: Integer;
  PageBreakMet: Boolean;

  procedure StoreSate;
  begin
    MS := TEvDualStreamHolder.Create;
    FRendContent.StoreState(MS);
  end;

  procedure RestoreSate;
  begin
    MS.Position := 0;
    FRendContent.ReStoreState(MS);
  end;

  procedure StoreBand(const ABand: TrmDBTableBand);
  begin
    LastStoredBandPos := MS.Position;
    ABand.StoreToStream(MS, True);
  end;

  procedure RestoreBand(const ABand: TrmDBTableBand);
  begin
    MS.Position := LastStoredBandPos;
    ABand.ReStoreFromStream(MS, True);
    MS.Position := LastStoredBandPos;
  end;

begin
  // find cutting band
  PrepareForReadingContent;
  Result := True;

  if not Assigned(FRendCurrentBand) and FRendContent.Eof then
    Exit;

  StoreSate;
  try
    Bnd := FRendCurrentBand;
    BottomPos := 0;

    if Assigned(Bnd) then
    begin
      Inc(BottomPos, Bnd.CalculatedBounds.Bottom);
      if BottomPos > FBottomCutClientPosition then
      begin
        Result := PreCutChild(Bnd);
        exit;
      end;
    end
    else
      Inc(BottomPos, FRendLastBandPos);

    // looking for band which might be cut
    TopPos := BottomPos;
    PageBreakMet := False;
    while not FRendContent.Eof do
    begin
      TopPos := BottomPos;
      
      // Stop searching if it's a page break
      if FRendContent.BandType = rwTBTPageBreak then
      begin
        PageBreakMet := True;
        break;
      end;

      Inc(BottomPos, FRendContent.BandSize);
      if BottomPos >= FBottomCutClientPosition then
        break;

      FRendContent.Next;
    end;

    if PageBreakMet then
    begin
      CutAmd := TopPos - FBottomCutClientPosition;
      Result := CutAmd = 0;
      if not Result then
      begin
        Inc(FBottomCutClientPosition, CutAmd);
        Inc(FBottomCutPosition, CutAmd);
      end;
      Exit;
    end;

    // find band
    if FRendContent.GroupNbr = -1 then
      Grp := nil
    else
      Grp := Groups[FRendContent.GroupNbr];
    Bnd := BandByType(FRendContent.BandType, Grp);

    if Bnd.FBandType in [rwTBTGroupHeader]  then
    begin
      // We don't want to have just header(s) at the cutting edge
      CutAmd := TopPos - FBottomCutClientPosition;
      Result := CutAmd = 0;
      if not Result then
      begin
        Inc(FBottomCutClientPosition, CutAmd);
        Inc(FBottomCutPosition, CutAmd);
      end;
    end

    else if BottomPos > FBottomCutClientPosition then
    begin
      StoreBand(Bnd);
      try
        Bnd.SetRendStatusChildrenToo(rmRWARSCalculated);
        FRendContent.ReadBand(Bnd);   // read band from content table
        R := Bnd.CalculatedBounds;
        OffsetRect(R, 0, TopPos - R.Top);
        Bnd.CalculatedBounds := R;
        Result := PreCutChild(Bnd);
      finally
        RestoreBand(Bnd);
      end;
    end;

  finally
    ReStoreSate;
  end;
end;

function TrmDBTable.GetEachPageHeaderHeight: Integer;
var
  i: Integer;
begin
  Result := 0;

  // reserve top space for headers
  if RendStatus = rmRWARSPrinting then
  begin
    if Assigned(FHeaderBand) and FHeaderBand.PrintOnEachPage then
      Inc(Result, FHeaderBand.CalculatedBounds.Bottom - FHeaderBand.CalculatedBounds.Top);

    if Assigned(FRendCurrentBand) and (FRendCurrentBand.BandType in [rwTBTDetail, rwTBTGroupFooter]) then
      for i := 0 to Groups.Count - 1 do
      begin
        if not Groups[i].FDisabled and Assigned(Groups[i].FHeaderBand) and Groups[i].FHeaderBand.PrintOnEachPage then
          Inc(Result, Groups[i].FHeaderBand.CalculatedBounds.Bottom - Groups[i].FHeaderBand.CalculatedBounds.Top);
      end;
  end;
end;

procedure TrmDBTable.ReadPageBreakGroup(Reader: TReader);
begin
  FGroups.SetPageBreakGroup(Reader.ReadString);
end;

procedure TrmDBTable.WritePageBreakGroup(Writer: TWriter);
begin
  Writer.WriteString(FGroups.GetPageBreakGroupID);
end;

function TrmDBTable.GetEachPageFooterHeight: Integer;
begin
  Result := 0;
end;

procedure TrmDBTable.PrepareForNextPrintCycle;
var
  OldValue: Boolean;
begin
  if Assigned(FRendCurrentBand) then
  begin
    OldValue := FRendCurrentBand.PrintOnEachPage;
    FRendCurrentBand.PrintOnEachPage := False;
  end
  else
    OldValue := False;

  inherited;

  if Assigned(FRendCurrentBand) then
    FRendCurrentBand.PrintOnEachPage := OldValue;
end;

procedure TrmDBTable.NotifyAggregateFunctions;
begin
  DataSource.NotifyAggregateFunctions;
end;

function TrmDBTable.GetQuery: IrmQuery;
begin
  Result := FQuery;
end;

function TrmDBTable.GetMasterFields: TrwStrings;
begin
  Result := DataSource.MasterFields;
end;

function TrmDBTable.IsSubTable: Boolean;
begin
  Result := CellOwner <> nil;
end;

procedure TrmDBTable.SetMasterFields(const AValue: TrwStrings);
begin
  DataSource.MasterFields := AValue;
end;


procedure TrmDBTable.BeforeDestruction;
begin
  if Assigned(Container) then
    Container := nil;
  inherited;
end;


function TrmDBTable.CellOwner: TrmTableCell;
begin
  if Container is TrmTableCell then
    Result := TrmTableCell(Container)
  else
   Result := nil;
end;


function TrmDBTable.MainTable: TrmCustomTable;
var
  C: TrmPrintContainer;
begin
  Result := nil;

  if IsSubTable then
  begin
    C := CellOwner.Table;
    while Assigned(C) and not((C.ClassType = TrmDBTable) or (C.ClassType = TrmTable)) do
      C := C.Container;

    if Assigned(C) then
      Result := TrmCustomTable(C);
  end;
end;

procedure TrmDBTable.SetAlign(Value: TAlign);
begin
  if not IsSubTable then
    inherited;
end;


procedure TrmDBTable.SetMultiPaged(const Value: Boolean);
begin
  if IsSubTable then
    inherited SetMultiPaged(False)
  else
    inherited;
end;

function TrmDBTable.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;

  if IsSubTable then
  begin
    Result.MovingDirections := [];
    Result.ResizingDirections := [];
//    Result.StandaloneObject := False;
  end;
end;


procedure TrmDBTable.ApplyMouseResizing(NewBounds: TRect; MousePosition: TrmdMousePosition; Shift: TShiftState);
begin
  if IsSubTable then
    CellOwner.SetBounds(0, 0, NewBounds.Right - NewBounds.Left, NewBounds.Bottom - NewBounds.Top)  
  else
    inherited;
end;

procedure TrmDBTable.Notify(AEvent: TrmDesignEventType);
begin
  if IsSubTable then
  begin
    if AEvent in [rmdSelectObject, rmdSelectChildObject] then
    begin
      Invalidate;
      ShowTag;
    end

    else if AEvent in [rmdDestroyResizer, rmdDestroyChildResizer] then
    begin
      Invalidate;
      HideTag;
    end

    else if AEvent = rmdZoomChanged then
      SyncTagPos;

    if not (AEvent in [rmdSelectObject, rmdSelectChildObject, rmdDestroyResizer, rmdDestroyChildResizer]) then
      inherited;
  end

  else
    inherited;
end;

procedure TrmDBTable.SBTAlignBands;
var
  Bnd: TrmDBTableBand;
  d: Integer;
  R: TRect;
  Grp: TrmDBTableGroup;
begin
  // Expand previously calculated total bands if adjacent cells got bigger

  Bnd := BandByType(rwTBTTotal);

  if Assigned(Bnd) and TrmDBTableTotalBand(Bnd).AlignToBound then
  begin
    if (FRendContent.BandType = rwTBTTotal) and (FRendContent.CurrentSeqNbr > 0) then  // "CurrentSeqNbr = 0" means that table content is empty for this step
    begin
      d := (CellOwner.Height - Bnd.Height) - Bnd.Top;
      if d > 0 then
      begin
        R := Bnd.CalculatedBounds;
        Bnd.Top := Bnd.Top + d;
        OffsetRect(R, 0, d);
        Bnd.CalculatedBounds := R;
        FRendContent.UpdateBand(Bnd);

        FRendContent.FData.Prior;
        if FRendContent.GroupNbr = -1 then
          Grp := nil
        else
          Grp := Groups[FRendContent.GroupNbr];
        Bnd := BandByType(FRendContent.BandType, Grp);

        TableInfo.StoreState;
        Bnd.SetRendStatusChildrenToo(rmRWARSNone);
        Bnd.Height := Bnd.Height + d;
        Bnd.OnPageCalculated;
        Bnd.SetRendStatusChildrenToo(rmRWARSCalculated);
        FRendContent.UpdateBand(Bnd);
        TableInfo.RestoreState;
        FRendContent.FData.Next;
      end;
    end;
  end;
end;

procedure TrmDBTable.SetContainer(Value: TrmPrintContainer);
var
  Fml: TrmFMLFormula;
  w, h: Integer;
begin
  if Value = Container then
    Exit;

  if IsSubTable then
  begin
    CellOwner.FInnerMarginLeft := cDefaultCellBorderSize;
    CellOwner.FInnerMarginRight := cDefaultCellBorderSize;
    CellOwner.FInnerMarginTop := cDefaultCellBorderSize;
    CellOwner.FInnerMarginBottom := cDefaultCellBorderSize;
  end;

  if Value is TrmTableCell then
  begin
    Multipaged := False;
    PrintOnEachPage := False;
    Cutable := True;
    TrmTableCell(Value).DestroyChildren;
    TrmTableCell(Value).DataField := '';
    TrmTableCell(Value).FInnerMarginLeft := 0;
    TrmTableCell(Value).FInnerMarginRight := 0;
    TrmTableCell(Value).FInnerMarginTop := 0;
    TrmTableCell(Value).FInnerMarginBottom := 0;
    TrmTableCell(Value).FAutoHeight := True;
    Fml := TrmTableCell(Value).FormulaValue;
    FreeAndNil(Fml);
    TrmTableCell(Value).Value := Null;
    Top := 0;
    Left := 0;
    w := TrmTableCell(Value).ClientWidth;
    h := TrmTableCell(Value).ClientHeight;
    inherited;

    if not TrmTableCell(Value).Loading then
    begin
      TrmTableCell(Value).Table.Height := TrmTableCell(Value).ClientHeight - FMinWorkAreaSize;
      TrmTableCell(Value).Height := h;
      TrmTableCell(Value).Width := w;
      TrmTableCell(Value).AfterChangePos;
    end;
    
    TrmTableCell(Value).ApplyBorderLinesForSubTable;

    if Owner is TrmDBTableBand then
      DataSource.MasterDataSource := TrmDBTable(TrmDBTableBand(Owner).Owner).DataSource
    else
      DataSource.MasterDataSource := nil;
  end
  else
  begin
    DataSource.MasterDataSource := nil;
    inherited;
  end;
end;

function TrmDBTable.AfterCalculate: Boolean;
begin
  Result := inherited AfterCalculate;

  if Result and BlockParentIfEmpty and (FContentPosBeforeCalc = FRendContent.FData.RecordCount) then
  begin
    RendStatus := rmRWARSPrinted;
    Result := False;
  end;
end;

function TrmDBTable.BeforeCalculate: Boolean;
begin
  Result := inherited BeforeCalculate;

  if not Assigned(FRendContent) then
    FContentPosBeforeCalc := 0
  else
    FContentPosBeforeCalc := FRendContent.FData.RecordCount;
end;



{ TrmDBTableRenderContent }

function TrmDBTableRenderContent.BandSize: Integer;
begin
  Result := FData.Fields[4].AsInteger;
end;

function TrmDBTableRenderContent.BandType: TrmDBTableBandType;
begin
  Result := TrmDBTableBandType(FData.Fields[2].AsByte);
end;

constructor TrmDBTableRenderContent.Create;
var
  h: String;
  Fields: TsbFields;
begin
  Fields := TsbFields.Create;
  try
    Fields.CreateField('TableVersion', sbfInteger);
    Fields.CreateField('SeqNbr', sbfInteger);
    Fields.CreateField('BandType', sbfByte);
    Fields.CreateField('GroupNbr', sbfByte);
    Fields.CreateField('BandSize', sbfInteger);
    Fields.CreateField('Data', sbfBLOB);
    h := sbGetUniqueFileName(GetThreadTmpDir, 'DBTableContent', sbTableExt);
    sbCreateTable(h, Fields);
  finally
    FreeAndNil(Fields);
  end;

  FData := sbOpenTable(h, nil, sbmReadWrite, True);
  FCurrentTblVersion := 0;
  FCurrentSeqNbr := 0;
  FReading := False;
end;

destructor TrmDBTableRenderContent.Destroy;
begin
  sbClose(FData);
  inherited;
end;


function TrmDBTableRenderContent.Eof: Boolean;
begin
  Result := FData.Eof or (FCurrentTblVersion <> FData.Fields[0].AsInteger); 
end;

procedure TrmDBTableRenderContent.First;
begin
  SetReading(True);
  FData.FindKey([FCurrentTblVersion, 1]);
end;

function TrmDBTableRenderContent.GroupNbr: Integer;
begin
  if FData.Fields[3].IsNull then
    Result := -1
  else
    Result := FData.Fields[3].AsByte;
end;

procedure TrmDBTableRenderContent.Last;
begin
  SetReading(True);
  if FData.FindKey([FCurrentTblVersion + 1, 1]) then
    FData.Prior
  else
    FData.Last;
end;

procedure TrmDBTableRenderContent.Next;
begin
  SetReading(True);
  FData.Next;
end;

procedure TrmDBTableRenderContent.Prior;
begin
  SetReading(True);
  if FCurrentTblVersion = FData.Fields[0].AsInteger then
  begin
    FData.Prior;
    if FCurrentTblVersion <> FData.Fields[0].AsInteger then
      FData.Next;
  end;
end;

procedure TrmDBTableRenderContent.ReadBand(const ABand: TrmDBTableBand);
var
  S: IEvDualStream;
begin
  SetReading(True);

  S := TEvDualStreamHolder.CreateInMemory;
  FData.Fields[5].SaveBlobToStream(S.RealStream);
  FCurrentSeqNbr := FData.Fields[1].AsInteger;
  S.Position := 0;
  ABand.ReStoreFromStream(S, False);
end;

procedure TrmDBTableRenderContent.RestoreState(const AStream: IEvDualStream);
begin
  FData.RecNo := AStream.ReadInteger;
  if AStream.ReadBoolean then
    FData.Last;
  FCurrentTblVersion := AStream.ReadInteger;
  FCurrentSeqNbr := AStream.ReadInteger;
end;

procedure TrmDBTableRenderContent.SetCurrentTblVersion(const Value: Integer);
begin
  if FCurrentTblVersion <> Value then
    FCurrentSeqNbr := 0;
  FCurrentTblVersion := Value;
end;


procedure TrmDBTableRenderContent.SetReading(Value: Boolean);
var
  h: String;
begin
  if Value <> FReading then
  begin
    FReading := Value;

    if FReading then
    begin
      h := sbGetUniqueFileName(GetThreadTmpDir, 'DBTableContent', sbIndexExt);
      FData.CreateIndex(h, ['TableVersion', 'SeqNbr'], []);
      FData.SetIndex(h);
    end
    else
      FData.CloseAllIndexes;
  end;
end;

procedure TrmDBTableRenderContent.StoreBand(const ABand: TrmDBTableBand);
var
  S: IEvDualStream;
begin
  SetReading(False);

  if Assigned(ABand) then
  begin
    S := TEvDualStreamHolder.CreateInMemory;
    ABand.StoreToStream(S, False);
  end;

  FData.Append;
  FData.Fields[0].AsInteger := FCurrentTblVersion;  //Table Version
  Inc(FCurrentSeqNbr);
  FData.Fields[1].AsInteger := FCurrentSeqNbr;      //Order within version

  if ABand is TrmDBTableCustomGroupBand then
    FData.Fields[3].AsByte := TrmDBTableCustomGroupBand(ABand).FGroup.Index; //Group Number

  if Assigned(ABand) then
  begin
    FData.Fields[2].AsByte := Ord(ABand.BandType); //Band Type
    FData.Fields[4].AsInteger := ABand.CalculatedBounds.Bottom - ABand.CalculatedBounds.Top; //Band Size
    S.Position := 0;
    FData.Fields[5].LoadBlobFromStream(S.RealStream); // Band Data
  end
  else
    FData.Fields[2].AsByte := Ord(rwTBTPageBreak); //Page Break
  
  FData.Post;
end;

procedure TrmDBTableRenderContent.StorePageBreak;
begin
  StoreBand(nil);
end;

procedure TrmDBTableRenderContent.StoreState(const AStream: IEvDualStream);
begin
  AStream.WriteInteger(FData.RecNo);
  AStream.WriteBoolean(FData.Eof);
  AStream.WriteInteger(FCurrentTblVersion);
  AStream.WriteInteger(FCurrentSeqNbr);
end;

procedure TrmDBTableRenderContent.UpdateBand(const ABand: TrmDBTableBand);
var
  S: IEvDualStream;
begin
  S := TEvDualStreamHolder.CreateInMemory;
  ABand.StoreToStream(S, False);
  FData.Edit;
  FData.Fields[4].AsInteger := ABand.CalculatedBounds.Bottom - ABand.CalculatedBounds.Top; //Band Size
  S.Position := 0;
  FData.Fields[5].LoadBlobFromStream(S.RealStream); // Data
  FData.Post;
end;


{ TrmdTableBorder }

constructor TrmdTableBorder.Create(AOwner: TrmGraphicControl);
begin
  inherited;
  PreparePattern;
//  FTransparent := True;
  FIgnoreMouse := True;
  Visible := True;
end;

destructor TrmdTableBorder.Destroy;
begin
  FreeAndNil(FPattern);
  inherited;
end;

function TrmdTableBorder.GetShapeRegion: HRGN;
var
  Rgn: THandle;
  T: TrmCustomTable;
  R: TRect;
  P: TPoint;
begin
  T := TrmCustomTable(Owner);

  P := TrmCustomTable(Owner).ClientToScreen(Point(0, 0));
  P := Parent.ScreenToClient(P);

  R := Zoomed(Rect(0, 0, T.Width, T.Height));
  OffsetRect(R, P.X, P.Y);
  Result := CreateRectRgnIndirect(R);
  InflateRect(R, Zoomed(31), Zoomed(31));
  Rgn := CreateRectRgnIndirect(R);

  CombineRgn(Result, Rgn, Result, RGN_DIFF);
  DeleteObject(Rgn);
end;

procedure TrmdTableBorder.PaintToCanvas(ACanvas: TrwCanvas);
begin
  with ACanvas do
  begin
    Brush.Style := bsSolid;
    Brush.Bitmap.Assign(FPattern);
    Brush.Bitmap.Transparent := True;
    Brush.Bitmap.TransparentMode := tmFixed;
    Brush.Bitmap.TransparentColor := clWhite;
//    SetBkMode(Handle, TRANSPARENT);
    FillRgn(Handle, Shape, Brush.Handle);
    Brush.Bitmap.Assign(nil);
    Brush.Style := bsClear;
  end;
end;


procedure TrmdTableBorder.PreparePattern;
begin
  FreeAndNil(FPattern);
  FPattern := TBitmap.Create;

  FPattern.Width := 8;
  FPattern.Height := 8;
  FPattern.Canvas.Brush.Color := clWhite;
  FPattern.Canvas.FillRect(Rect(0, 0, FPattern.Width, FPattern.Height));
  FPattern.Canvas.Pen.Style := psSolid;
  FPattern.Canvas.Pen.Mode := pmCopy;
  FPattern.Canvas.Pen.Color := clBlue;

  // Hatch bitmap
  FPattern.Canvas.MoveTo(0, 1);
  FPattern.Canvas.LineTo(2, 0);
  FPattern.Canvas.MoveTo(0, 5);
  FPattern.Canvas.LineTo(6, -1);
  FPattern.Canvas.MoveTo(2, 7);
  FPattern.Canvas.LineTo(8, 1);
  FPattern.Canvas.MoveTo(6, 7);
  FPattern.Canvas.LineTo(8, 5);
end;

{ TrmdTableBandTag }

constructor TrmdTableBandTag.Create(AOwner: TrmGraphicControl);
begin
  inherited;
  AllowMove := False;
end;

function TrmdTableBandTag.GetBracketRegion: HRGN;
var
  P, P2: TPoint;
  n: Integer;
  Rgn: HRGN;
begin
  P := TrmCustomTable(Owner).ClientToScreen(Point(0, 0));
  P := Parent.ScreenToClient(P);
  P2 := TrmCustomTable(Owner).ClientToScreen(Point(0, TrmCustomTable(Owner).Height));
  P2 := Parent.ScreenToClient(P2);
  Dec(P.X, Zoomed(31));
  Inc(P.Y, Zoomed(13));
  Dec(P2.X, Zoomed(31));
  Dec(P2.Y, Zoomed(13));

  //Rectangle
  Result := CreateRectRgn(P.X - Zoomed(38), P.Y, P2.X, P2.Y);
  Rgn := CreateRectRgn(P.X - Zoomed(26), P.Y + Zoomed(13), P2.X, P2.Y - Zoomed(13));
  //Substract internal rectangle
  CombineRgn(Result, Result, Rgn, RGN_DIFF);
  DeleteObject(Rgn);

  //Make a "tail"
  n := P.Y + (P2.Y - P.Y) div 2;
  P := Point(P.X - Zoomed(63), n - Zoomed(6));
  P2 := Point(P.X + Zoomed(26), n + Zoomed(6));
  Rgn := CreateRectRgn(P.X, P.Y, P2.X, P2.Y);
  CombineRgn(Result, Result, Rgn, RGN_OR);
  DeleteObject(Rgn);
end;

function TrmdTableBandTag.GetShapeRegion: HRGN;
var
  Rgn: HRGN;
begin
  Result := GetBracketRegion;
  Rgn := GetTagRegion;
  CombineRgn(Result, Result, Rgn, RGN_OR);
  DeleteObject(Rgn);
end;

function TrmdTableBandTag.GetTagRegion: HRGN;
var
  P: TPoint;
  TxtSize: TSize;
begin
  TxtSize := GetTextSize;
  P := TrmCustomTable(Owner).ClientToScreen(Point(0, TrmCustomTable(Owner).Height div 2));
  P := Parent.ScreenToClient(P);
  Dec(P.X, Zoomed(125));
  Result := CreateRectRgn(P.X - TxtSize.cx, P.Y - TxtSize.cy div 2, P.X, P.Y + (TxtSize.cy - TxtSize.cy div 2));
end;

procedure TrmdTableBandTag.PaintToCanvas(ACanvas: TrwCanvas);
var
  ShapePart: HRGN;

  function IsSelected: Boolean;
  var
    SelObjects: TrmListOfObjects;
    i: Integer;
    Tbl: TrmDBTable;
    C: TrmPrintControl;
  begin
    Result := Owner.Designer.IsObjectSelected(Owner);

    if not Result then
    begin
      Tbl := TrmDBTable(TrmDBTableBand(Owner).Owner);
      SelObjects := Owner.Designer.GetSelectedObjects;
      for i := Low(SelObjects) to High(SelObjects) do
        if SelObjects[i].ObjectType = rwDOTPrintForm then
        begin
          C := TrmPrintControl(SelObjects[i].RealObject);
          while Assigned(C) and (C <> Tbl) do
          begin
            if C = Owner then
            begin
              Result := True;
              break;
            end;
            C := C.Container;
          end;

          if Result then
            break;
        end;
    end;
    SetLength(SelObjects, 0);
  end;

begin
  with ACanvas do
  begin
    ShapePart := GetBracketRegion;
    if IsSelected then
      Brush.Color := clRed
    else
      Brush.Color := $0097AAF4;
    Brush.Style := bsSolid;
    FillRgn(Handle, ShapePart, Brush.Handle);
    DeleteObject(ShapePart);

    ShapePart := GetTagRegion;
    PainTag(ACanvas, ShapePart);
    DeleteObject(ShapePart);
  end;
end;

{ TrmDBTableHeaderBand }

constructor TrmDBTableHeaderBand.Create(AOwner: TComponent);
begin
  inherited;
  FBandType := rwTBTHeader;
  PrintOnEachPage := True;
  LinkBandToTable; 
end;

procedure TrmDBTableHeaderBand.LinkBandToTable;
begin
  TrmDBTable(Owner).FHeaderBand := Self;
end;

{ TrmDBTableDetailBand }

constructor TrmDBTableDetailBand.Create(AOwner: TComponent);
begin
  inherited;
  FBandType := rwTBTDetail;
  LinkBandToTable;  
end;

procedure TrmDBTableDetailBand.LinkBandToTable;
begin
  TrmDBTable(Owner).FDetailBand := Self;
end;

procedure TrmDBTableDetailBand.SetPrintIfNoData(const Value: Boolean);
begin
  inherited SetPrintIfNoData(False);
end;

{ TrmDBTableTotalBand }

constructor TrmDBTableTotalBand.Create(AOwner: TComponent);
begin
  inherited;
  FBandType := rwTBTTotal;
  AlignToBound := True;
  LinkBandToTable;
end;

procedure TrmDBTableTotalBand.LinkBandToTable;
begin
  TrmDBTable(Owner).FTotalBand :=  Self;
end;

procedure TrmDBTableTotalBand.SetAlignToBound(const Value: Boolean);
begin
  FAlignToBound := Value;
  TrmDBTable(Owner).Invalidate;
end;



{ TrmDBTableGroup }

constructor TrmDBTableGroup.Create(Collection: TCollection);
begin
  inherited;
  FFields := TStringList.Create;
end;

destructor TrmDBTableGroup.Destroy;
begin
  FreeAndNil(FFields);
  inherited;
end;

procedure TrmDBTableGroup.Assign(Source: TPersistent);
begin
  inherited;
  GroupName := TrmDBTableGroup(Source).GroupName;
  Fields := TrmDBTableGroup(Source).Fields;
  Disabled := TrmDBTableGroup(Source).Disabled;
end;

procedure TrmDBTableGroup.ClearCalcInfo;
begin
  SetLength(FGroupFields, 0);
end;

function TrmDBTableGroup.GetDisabled: Boolean;
begin
  Result := FDisabled;
end;

function TrmDBTableGroup.GetFields: TCommaDilimitedString;
begin
  Result := FFields.CommaText;
end;

function TrmDBTableGroup.GetGroupName: String;
begin
  Result := FGroupName;
end;

function TrmDBTableGroup.GetPrintFooterBand: Boolean;
begin
  Result := Assigned(FFooterBand);
end;

function TrmDBTableGroup.GetPrintHeaderBand: Boolean;
begin
  Result := Assigned(FHeaderBand);
end;

function TrmDBTableGroup.IsGroupBeginning: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(FGroupFields) to High(FGroupFields) do
  begin
    Result := VarIsEmpty(FGroupFields[i].GroupValue);
    if not Result then
      break;
  end;
end;

function TrmDBTableGroup.IsGroupChanged: Boolean;
var
  i: Integer;
  x1, x2: Boolean;
begin
  Result := False;

  for i := Low(FGroupFields) to High(FGroupFields) do
  begin
    x1 := VarIsNull(FGroupFields[i].GroupValue);
    x2 := VarIsNull(FGroupFields[i].Field.Value);
    if VarIsEmpty(FGroupFields[i].GroupValue) or
       x1 and not x2 or
       not x1 and x2 or
       not x1 and not x2 and (String(FGroupFields[i].GroupValue) <> String(FGroupFields[i].Field.Value)) then
    begin
      Result := True;
      break;
    end;
  end;
end;

function TrmDBTableGroup.AsAncestor( const AncestorItem: TrwCollectionItem): Boolean;
begin
  Result := TrmDBTableGroup(AncestorItem).Disabled = Disabled;
end;

procedure TrmDBTableGroup.ApplyDelta(const DeltaCollectionItem: TrwCollectionItem);
begin
  Disabled := TrmDBTableGroup(DeltaCollectionItem).Disabled;
end;

procedure TrmDBTableGroup.PrepareToCalc;
var
  i: Integer;
begin
  SetLength(FGroupFields, Fields.Count);
  for i := 0 to Fields.Count - 1 do
    FGroupFields[i].Field := TrmDBTable(TrmDBTableGroups(Collection).Owner).DataSource.Fields.FindField(Fields[i]);

  StartNewGroup;
end;

procedure TrmDBTableGroup.SetDisabled(const Value: Boolean);
begin
  FDisabled := Value;
end;

procedure TrmDBTableGroup.SetFieldList(const Value: TStringList);
begin
  FFields.Assign(Value);
end;

procedure TrmDBTableGroup.SetFields(const Value: TCommaDilimitedString);
begin
  FFields.CommaText := Value;
end;

procedure TrmDBTableGroup.SetGroupName(const Value: String);
begin
  FGroupName := Value;
end;

procedure TrmDBTableGroup.SetPrintFooterBand(const Value: Boolean);
begin
  if Value then
  begin
    if not Assigned(FFooterBand) then
      TrmDBTable(Collection.Owner).AddBand(rwTBTGroupFooter, Self)
  end
  else
    FreeAndNil(FFooterBand);
end;

procedure TrmDBTableGroup.SetPrintHeaderBand(const Value: Boolean);
begin
  if Value then
  begin
    if not Assigned(FHeaderBand) then
      TrmDBTable(Collection.Owner).AddBand(rwTBTGroupHeader, Self)
  end
  else
    FreeAndNil(FHeaderBand);
end;

procedure TrmDBTableGroup.StartNewGroup;
var
  i: Integer;
begin
  for i := 0 to Fields.Count - 1 do
    VarClear(FGroupFields[i].GroupValue);

  if Assigned(FHeaderBand) then
    FHeaderBand.SetRendStatusChildrenToo(rmRWARSNone);

  if Assigned(FFooterBand) then
    FFooterBand.SetRendStatusChildrenToo(rmRWARSNone);
end;

procedure TrmDBTableGroup.StoreGroupValues;
var
  i: Integer;
begin
  for i := Low(FGroupFields) to High(FGroupFields) do
    FGroupFields[i].GroupValue := FGroupFields[i].Field.Value
end;


{ TrmDBTableGroups }

constructor TrmDBTableGroups.Create(AOwner: TrwComponent);
begin
  inherited Create(AOwner, TrmDBTableGroup);
end;

procedure TrmDBTableGroups.Assign(Source: TPersistent);
begin
  inherited;
  if Assigned(TrmDBTableGroups(Source).FPageBreakGroup) then
    FPageBreakGroup := TrmDBTableGroup(FindItemByID(TrmDBTableGroups(Source).FPageBreakGroup.ItemID))
  else
    FPageBreakGroup := nil;
end;

procedure TrmDBTableGroups.DeleteItem(const AItemIndex: Integer);
begin
  FreeAndNil(Items[AItemIndex].FHeaderBand);
  FreeAndNil(Items[AItemIndex].FFooterBand);
  inherited;
end;

function TrmDBTableGroups.GetGroupByName(const AGroupName: String): IrmDBTableGroup;
begin
  Result := GroupByName(AGroupName);
end;

function TrmDBTableGroups.GetItem(Index: Integer): TrmDBTableGroup;
begin
  Result := TrmDBTableGroup(inherited Items[Index]);
end;

function TrmDBTableGroups.GetPageBreakGroupID: String;
begin
  if Assigned(FPageBreakGroup) then
    Result := FPageBreakGroup.ItemID
  else
    Result := '';
end;

function TrmDBTableGroups.GroupByName(const AGroupName: String): TrmDBTableGroup;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count -1 do
    if AnsiSameText(Items[i].GroupName, AGroupName) then
    begin
      Result := Items[i];
      break;
    end;
end;

procedure TrmDBTableGroups.SetPageBreakGroup(const AGroupID: String);
begin
  FPageBreakGroup := TrmDBTableGroup(FindItemByID(AGroupID));
end;

procedure TrmDBTableGroups.Update(Item: TCollectionItem);
begin
  if not Assigned(Item) then
    TrmDBTable(Owner).UpdateGroupBandsPosition;
end;


{ TrmDBTableCustomGroupBand }

procedure TrmDBTableCustomGroupBand.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('GroupID', ReadGroupID, WriteGroupID, not InheritedComponent);
end;

procedure TrmDBTableCustomGroupBand.ReadGroupID(Reader: TReader);
begin
  FGroupID := Reader.ReadString;
end;

procedure TrmDBTableCustomGroupBand.SetDescription(const Value: String);
begin
// do nothing
end;

procedure TrmDBTableCustomGroupBand.SetPrintIfNoData(const Value: Boolean);
begin
  inherited SetPrintIfNoData(False);
end;

procedure TrmDBTableCustomGroupBand.WriteGroupID(Writer: TWriter);
begin
  Writer.WriteString(GroupID);
end;


{ TrmDBTableGroupHeaderBand }

constructor TrmDBTableGroupHeaderBand.Create(AOwner: TComponent);
begin
  inherited;
  FBandType := rwTBTGroupHeader;
end;


function TrmDBTableGroupHeaderBand.GetDescription: String;
begin
  Result := 'Header of ' + FGroup.GroupName;
end;


{ TrmDBTableGroupFooterBand }

constructor TrmDBTableGroupFooterBand.Create(AOwner: TComponent);
begin
  inherited;
  FBandType := rwTBTGroupFooter;
end;

function TrmDBTableGroupFooterBand.GetDescription: String;
begin
  Result := 'Footer of ' + FGroup.GroupName;
end;

{ TrmdTableTag }

procedure TrmdTableTag.CorrectTagBounds(var ATagRect: TRect);
var
  d: Integer;
begin
  inherited;
  d := Zoomed(13);
  OffsetRect(ATagRect, -d, -d);
end;

{ TrmRenderedBorderLines }

function TrmRenderedBorderLines.AddLine(const ALineInfo: TrmRenderedBorderLineInfo): Integer;
var
  pLineInfo: PTrmRenderedBorderLineInfo;
begin
  New(pLineInfo);
  pLineInfo^ := ALineInfo;
  Result := Add(pLineInfo);
end;

procedure TrmRenderedBorderLines.Clear;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    Dispose(PTrmRenderedBorderLineInfo(inherited Items[i]));

  inherited;
end;

constructor TrmRenderedBorderLines.Create(AOwner: TrmCustomTable);
begin
  inherited Create;
  FOwner := AOwner;
end;

procedure TrmRenderedBorderLines.CreateRWAObjects;
var
  i: Integer;
begin
  for i :=  Count - 1 downto 0 do  // backward order! Children's lines always go first
    with Items[i] do
      FOwner.Renderer.AddLine(PointA.X, PointA.Y, PointB.X, PointB.Y, Compare, LayerNumber, Color, Width, Style);

  Clear;    
end;

function TrmRenderedBorderLines.GetItem(Index: Integer): TrmRenderedBorderLineInfo;
begin
  Result := PTrmRenderedBorderLineInfo(inherited Items[Index])^;
end;


initialization
  cDefaultCellBorderSize := Round(ConvertUnit(4, utScreenPixels, utLogicalPixels));

end.
