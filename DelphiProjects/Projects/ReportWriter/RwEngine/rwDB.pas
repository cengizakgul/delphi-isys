unit rwDB;

interface

uses
  DB, rwBasicClasses, SysUtils, Classes, rwTypes, rwCommonClasses,
  rwMessages, rwQBInfo, rwExtendedControls, rwDataDictionary, rwBasicUtils,
  sbAPI, rwLogQuery, rwUtils, Variants, rwDesignClasses, rwEngineTypes,
  ISBasicClasses, ISDataAccessComponents, rmTypes;

type

  {TrwField is description of field}

  TrwField = class(TrwCollectionItem)
  private
    FsbField: TsbField;
    FFieldName: string;
    FFieldType: TFieldType;
    FKeyField: Boolean;
    FFieldSize: Integer;
    FVisible: Boolean;
    FDisplayLabel: String;
    FDisplayWidth: Integer;

    procedure SetFieldName(Value: string);
    procedure SetFieldType(Value: TFieldType);
    procedure SetFieldSize(const Value: Integer);
    function  GetValue: Variant;
    procedure SetValue(const Value: Variant);
    procedure SetVisible(const Value: Boolean);
    procedure SetDisplayLabel(const Value: String);
    procedure SetDisplayWidth(const Value: Integer);
    function  IsDisplayLabelNotEmpty: Boolean;

  public
    constructor Create(Collection: TCollection); override;
    procedure Assign(Source: TPersistent); override;

  published
    property FieldName: string read FFieldName write SetFieldName;
    property FieldType: TFieldType read FFieldType write SetFieldType;
    property KeyField:  Boolean read FKeyField write FKeyField default False;
    property FieldSize: Integer read FFieldSize write SetFieldSize default 0;
    property Visible:   Boolean read FVisible write SetVisible default True;
    property DisplayLabel: String read FDisplayLabel write SetDisplayLabel stored IsDisplayLabelNotEmpty;
    property DisplayWidth: Integer read FDisplayWidth write SetDisplayWidth default 0;

    property Value: Variant read GetValue write SetValue stored False;
  end;



  {TrwField is list of fields}

  TrwFields = class(TrwCollection)
  private
    function  GetItem(Index: Integer): TrwField;
    procedure SetItem(Index: Integer; Value: TrwField);

  public
    procedure Assign(Source: TPersistent); override;

  public
    property Items[Index: Integer]: TrwField read GetItem write SetItem; default;

    function FindField(AFieldName: string): TrwField;
    function CreateField(const AFieldName: string; const AFieldType: TFieldType;
                         const AFieldSize: Integer; const AKeyField: Boolean = False;
                         const AVisible: Boolean = True): TrwField;

  published
    function PCount: Variant;
    function PFieldByName(AFieldName: Variant): Variant;
    function PItems(AIndex: Variant): Variant;
    function PCreateField(AFieldName: Variant; AFieldType: Variant; AFieldSize: Variant): Variant;
  end;



  {TrwDataSet is parent class for access to DataSet}

  TrwDataSet = class(TrwNoVisualComponent)
  private
    FQuery: TsbTableCursor;
    FFields: TrwFields;
    FDBComponents: TList;
    FExternalUse: Boolean;
    FMasterDataSource: TrwDataSet;
    FMasterFields: TrwStrings;
    FFilter: String;
    FMasterLookupFilter: String;
    FOnDataChange: TNotifyEvent;
    FSort: String;
    FVCLDataSet: TsbDataset;
    FAfterPost: TNotifyEvent;
    FPosting: Boolean;
    FLockSyncVCL: Boolean;
    FOnChangeFilter: TNotifyEvent;
    FOnChangeSort: TNotifyEvent;

    procedure SetMasterDataSource(const Value: TrwDataSet);
    procedure SetMasterFields(const Value: TrwStrings);
    procedure OpenLikeLookUp;
    procedure FilterRecord(DataSet: TsbTableCursor; var Accept: Boolean);
    procedure OnChangeDataState(Sender: TObject);
    procedure OnAfterScroll(Sender: TObject);
    procedure SetSort(const Value: String);
    procedure CreateSortIndex;
    function  IsFilterNotEmpty: Boolean;
    function  IsSortNotEmpty: Boolean;
    procedure CheckActive;
    procedure CheckEditMode;

  protected
    FAllowToNotifyFormulas: Boolean;

    procedure SendNotification(AOperation: TrwNotifyOperation); override;
    procedure RegisterComponentEvents; override;
    procedure BlankUpdate(DataSet: TDataSet; UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
    procedure CreateQuery; virtual;

    procedure DoBeforeOpen; virtual;
    procedure DoAfterOpen; virtual;
    procedure DoBeforeClose; virtual;
    procedure DoAfterClose; virtual;

    function  GetData: Variant;
    procedure SetData(const Value: Variant); virtual;

    function  GetDataStr: String;
    procedure SetDataStr(const AValue: String); virtual;

    procedure SetFilter(const Value: String);
    procedure OpenQuery; virtual;
    procedure CreateFields; virtual;
    procedure ClearFields; virtual;

  //rendering
  public
    procedure  NotifyAggregateFunctions; override;

  public
    property DataSet: TsbTableCursor read FQuery;
    property ExternalUse: Boolean read FExternalUse write FExternalUse;
    property OnDataChange: TNotifyEvent read FOnDataChange write FOnDataChange;
    property AfterPost: TNotifyEvent read FAfterPost write FAfterPost;
    property OnChangeFilter: TNotifyEvent read FOnChangeFilter write FOnChangeFilter;
    property OnChangeSort: TNotifyEvent read FOnChangeSort write FOnChangeSort;

    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   BeforeDestruction; override;
    procedure   RWNotification(Sender: TrwComponent; AOperation: TrwNotifyOperation); override;
    procedure   SetFilterNoApplay(const Value: String);

    procedure Open;
    procedure Close;

    function  GetFieldValue(ADataField: string): Variant;
    procedure GetFieldList(AList: TStrings);

    procedure RegisterComponent(AComponent: TrwComponent);
    procedure UnRegisterComponent(AComponent: TrwComponent);
    procedure UnRegisterAllComponents;
    function  VCLDataSet: TsbDataset;
    procedure SyncVCLDataSet;

  published
    property  Left;
    property  Top;
    property  MasterDataSource: TrwDataSet read FMasterDataSource write SetMasterDataSource;
    property  MasterFields: TrwStrings read FMasterFields write SetMasterFields;
    property  Filter: String read FFilter write SetFilter stored IsFilterNotEmpty;
    property  Sort: String read FSort write SetSort stored IsSortNotEmpty;
    property  Data: Variant read GetData write SetData stored False;
    property  Fields: TrwFields read FFields stored False;
    property  DataStr: String read GetDataStr write SetDataStr stored False;

    function  PFieldValue(ADataField: Variant): Variant;
    procedure PNext;
    procedure PFirst;
    procedure PLast;
    procedure PPrior;
    function  PMoveBy(Distance: Variant): Variant;
    function  PEof: Variant;
    function  PBof: Variant;
    function  PState: Variant;
    function  PActive: Variant;
    procedure POpen;
    procedure PClose;
    function  PRecordCount: Variant;
    function  PLocate(KeyField: Variant; Value: Variant; CaseSensitive: Variant; PartialKey: Variant): Variant;
    procedure PEdit;
    procedure PPost;
    procedure PCancel;
    procedure PInsert;
    procedure PAppend;
    procedure PDelete;
    procedure PSetFieldValue(ADataField: Variant; Value: Variant);
    function  PFieldExists(ADataField: Variant): Variant;
    function  PRecordNumber: Variant;
    function  PFieldCount: Variant;
    procedure PDisableControls;
    procedure PEnableControls;
    function  PGetDataStr(AStoreStructure: Variant; AStoreData: Variant): Variant;
  end;


  {TrwQuery is class for access to database}

  TrwQuery = class(TrwDataSet, IrmQuery)
  private
    FQueryBuilderInfo: TrwQBQuery;
    FMacros: TStringList;
    FMacroPrefix: string;
    FSQL: TrwStrings;
    FSQLItems: TStrings;
    FParamsFromVars: Boolean;

    function  GetSQL: TrwStrings;
    procedure SetSQL(Value: TrwStrings);
    procedure FOnChangeSQL(Sender: TObject);
    procedure SetQueryBuilderInfo(Value: TrwQBQuery);
    procedure SetMacroPrefix(const Value: string);
    procedure CreateMacros;
    function  CheckScriptItems: Boolean;
    procedure ExecSQLScript(ActionType: Byte);

  protected
    procedure OnAssignQBInfo(Sender: TObject); virtual;
    procedure CreateQuery; override;
    procedure OpenQuery; override;
    procedure ChangeSQL;
    procedure ExecQuery;
    function  SetMacros: String;

  // Interface
  protected
    function  GetQBQuery: IrwQBQuery;
    function  GetMasterQuery: IrmQuery;
    procedure Clear;
  // End

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function    GetParamByName(Param:String): TsbParam;
    procedure   Prepare;

  published
    property MacroPrefix: string read FMacroPrefix write SetMacroPrefix;
    property SQL: TrwStrings read GetSQL write SetSQL;
    property QueryBuilderInfo: TrwQBQuery read FQueryBuilderInfo write SetQueryBuilderInfo;
    property ParamsFromVars: Boolean read FParamsFromVars write FParamsFromVars;

    procedure PSetParameterValue(AName: Variant; Value: Variant);
    function  PGetParameterValue(AName: Variant): Variant;
    function  PParameterExists(AName: Variant): Variant;
    procedure PExecute;
    procedure PSetMacroValue(AName: Variant; Value: Variant);
    function  PGetMacroValue(AName: Variant): Variant;
  end;


  {TrwBuffer is class for temporary table of the database}

  TrwBuffer = class(TrwDataSet)
  private
    FCreated: Boolean;
    FFieldsRead: TrwFields;
    FTableName: string;
    FCreateSQL: string;
    FExecQuery: TrwLQQuery;

    procedure SetFields(Value: TrwFields);
    function  GetFields: TrwFields;
    procedure SetTableName(Value: string);
    procedure FixUpTableName;
    procedure CreateFieldStructure(ADataSet: TDataSet);
    procedure LoadFromFile(AFileName: String; ADelimiter: String; AScreenChar: String);
    function  IsFieldsNotEmpty: Boolean;

  protected
    procedure WriteState(Writer: TWriter); override;
    procedure Loaded; override;
    procedure CreateQuery; override;
    procedure OpenQuery; override;
    procedure SetInheritedComponentProperty(AValue: Boolean); override;
    procedure AfterInitLibComponent; override;
    procedure SetData(const Value: Variant); override;
    procedure SetDataStr(const AValue: String); override;
    procedure CreateFields; override;
    procedure ClearFields; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    procedure CreateBuffer;
    procedure DropBuffer;
  published
    property TableName: string read FTableName write SetTableName;
    property Fields: TrwFields read GetFields write SetFields stored IsFieldsNotEmpty;

    procedure PClear;
    procedure PCreateLikeDataSet(ADataSet: Variant);
    procedure PFillLikeDataSet(ADataSet: Variant);
    procedure PLoadFromFile(AFileName: Variant; ADelimiter: Variant; AScreenChar: Variant);
    procedure PSetDataStr(AData: Variant; ALoadStructure: Variant; ALoadData: Variant);
  end;



implementation

uses rwReport, rwCustomDataDictionary, rmPrintTable, rmASCIIFormControls, rmXMLFormControls;


  {TrwDataSet}

constructor TrwDataSet.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FMasterFields := TrwStrings.Create;
  FDBComponents := TList.Create;
  FExternalUse := False;
  FOnDataChange := nil;
  FAfterPost := nil;
  FOnChangeFilter := nil;
  FOnChangeSort := nil;
  FPosting := False;
  FQuery := nil;
  FMasterDataSource := nil;
  FFilter := '';
  FMasterLookupFilter := '';
  FSort := '';
  FAllowToNotifyFormulas := True;

  FFields := TrwFields.Create(Self, TrwField);

  FVCLDataSet := nil;
  FLockSyncVCL := True;
end;


destructor TrwDataSet.Destroy;
begin
  MasterDataSource := nil;
  FreeAndNil(FMasterFields);
  UnRegisterAllComponents;
  FreeAndNil(FDBComponents);
  FreeAndNil(FFields);
  FreeAndNil(FVCLDataSet);
  FreeAndNil(FQuery);
  inherited;
end;

procedure TrwDataSet.OnChangeDataState(Sender: TObject);
begin
  if FPosting and FQuery.NotificationEnabled and Assigned(FAfterPost) then
    FAfterPost(Self);

  SyncVCLDataSet;

  SendNotification(rnoDataSetChanged);
end;

procedure TrwDataSet.RegisterComponent(AComponent: TrwComponent);
var
  i: Integer;
begin
  i := FDBComponents.IndexOf(AComponent);
  if (i = -1) then
    FDBComponents.Add(AComponent);
end;

procedure TrwDataSet.UnRegisterComponent(AComponent: TrwComponent);
var
  i: Integer;
begin
  i := FDBComponents.IndexOf(AComponent);

  if (i <> -1) then
    FDBComponents.Delete(i);
end;

procedure TrwDataSet.UnRegisterAllComponents;
begin
  SendNotification(rnoDestroy);
  FDBComponents.Clear;
end;

procedure TrwDataSet.SendNotification(AOperation: TrwNotifyOperation);
var
  i: Integer;
begin
  if Assigned(FOnDataChange) then
    FOnDataChange(Self);

  inherited;


 if AOperation = rnoDestroy then
  while FDBComponents.Count > 0  do
    TrwComponent(FDBComponents[FDBComponents.Count - 1]).RWNotification(Self, AOperation)

 else
 begin
   for i := 0 to FDBComponents.Count - 1 do
     if TObject(FDBComponents[i]) is TrwDataSet then
       TrwComponent(FDBComponents[i]).RWNotification(Self, AOperation);

   for i := 0 to FDBComponents.Count - 1 do
     if not (TObject(FDBComponents[i]) is TrwDataSet) then
       TrwComponent(FDBComponents[i]).RWNotification(Self, AOperation);
 end;
end;

procedure TrwDataSet.RegisterComponentEvents;
begin
  inherited;

  RegisterEvents([cEventOnFilter, cEventBeforeOpen, cEventAfterOpen,
    cEventBeforeClose, cEventAfterClose, cEventAfterScroll,
    cEventBeforeInsert, cEventBeforeEdit, cEventBeforeDelete,
    cEventBeforeCancel, cEventBeforePost,
    cEventAfterInsert, cEventAfterEdit, cEventAfterDelete,
    cEventAfterCancel, cEventAfterPost]);
end;

procedure TrwDataSet.BlankUpdate(DataSet: TDataSet; UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
begin
end;


function TrwDataSet.PFieldValue(ADataField: Variant): Variant;
begin
  Result := GetFieldValue(ADataField);
end;


function TrwDataSet.PLocate(KeyField: Variant; Value: Variant; CaseSensitive: Variant;
  PartialKey: Variant): Variant;
var
  Flds: array of string;
  Vals: array of Variant;
  h: String;
  i: Integer;
begin
  CheckActive;

  if VarIsArray(Value) then
    i := VarArrayHighBound(Value, 1) - VarArrayLowBound(Value, 1) + 1
  else
    i := 1;

  SetLength(Flds, i);
  SetLength(Vals, i);
  h := KeyField;
  h := StringReplace(h, ',', ';', [rfReplaceAll, rfIgnoreCase]);
  for i := Low(Flds) to High(Flds) do
  begin
    Flds[i] := Trim(GetNextStrValue(h, ';'));
    if Length(Flds) = 1 then
      Vals[i] := Value
    else
      Vals[i] := Value[i];
  end;

  FLockSyncVCL := False;
  try
    Result := FQuery.Locate(Flds, Vals, CaseSensitive, PartialKey);
    SyncVCLDataSet;    
  finally
    FLockSyncVCL := True;
  end;
end;

procedure TrwDataSet.PEdit;
var
  V: Variant;
begin
  CheckActive;
  
  V := True;
  if FQuery.NotificationEnabled then
    ExecuteEventsHandler('BeforeEdit', [Integer(Addr(V))]);
  if V then
  begin
    FQuery.Edit;
    if FQuery.NotificationEnabled then
      ExecuteEventsHandler('AfterEdit', []);
  end;
end;

procedure TrwDataSet.PPost;
var
  V: Variant;
begin
  CheckActive;
  CheckEditMode;
  
  V := True;
  if FQuery.NotificationEnabled then
    ExecuteEventsHandler('BeforePost', [Integer(Addr(V))]);
  if V then
  begin
    FPosting := True;
    try
      FQuery.Post;
    finally
      FPosting := False;
    end;
    if FQuery.NotificationEnabled then
      ExecuteEventsHandler('AfterPost', []);
  end;
end;

procedure TrwDataSet.PCancel;
var
  V: Variant;
begin
  CheckActive;
  CheckEditMode;
    
  V := True;
  if FQuery.NotificationEnabled then
    ExecuteEventsHandler('BeforeCancel', [Integer(Addr(V))]);
  if V then
  begin
    FQuery.Cancel;
    if FQuery.NotificationEnabled then
      ExecuteEventsHandler('AfterCancel', []);
  end;
end;

procedure TrwDataSet.PInsert;
var
  V: Variant;
begin
  CheckActive;
  
  V := True;
  if FQuery.NotificationEnabled then
    ExecuteEventsHandler('BeforeInsert', [Integer(Addr(V))]);
  if V then
  begin
    FQuery.Insert;
    if FQuery.NotificationEnabled then
      ExecuteEventsHandler('AfterInsert', []);
  end;
end;

procedure TrwDataSet.PAppend;
var
  V: Variant;
begin
  CheckActive;
  
  V := True;
  if FQuery.NotificationEnabled then
    ExecuteEventsHandler('BeforeInsert', [Integer(Addr(V))]);
  if V then
  begin
    FQuery.Append;
    if FQuery.NotificationEnabled then
      ExecuteEventsHandler('AfterInsert', []);
  end;
end;

procedure TrwDataSet.PDelete;
var
  V: Variant;
begin
  CheckActive;
  
  V := True;
  if FQuery.NotificationEnabled then
    ExecuteEventsHandler('BeforeDelete', [Integer(Addr(V))]);
  if V then
  begin
    FQuery.Delete;
    if FQuery.NotificationEnabled then
      ExecuteEventsHandler('AfterDelete', []);
  end;
end;

procedure TrwDataSet.PSetFieldValue(ADataField: Variant; Value: Variant);
var
  F: TsbField;
begin
  CheckActive;
  CheckEditMode;
  
  F := FQuery.Fields.FieldByName(ADataField);
  if not Assigned(F) then
    raise ErwException.CreateFmt(ResErrWrongFieldName, [ADataField]);

  F.Value := Value;
end;

procedure TrwDataSet.PNext;
begin
  CheckActive;
  FLockSyncVCL := False;
  try
    FQuery.Next;
    SyncVCLDataSet;
  finally
    FLockSyncVCL := True;
  end;
end;

procedure TrwDataSet.PFirst;
begin
  CheckActive;
  FLockSyncVCL := False;
  try
    FQuery.First;
    SyncVCLDataSet;
  finally
    FLockSyncVCL := True;
  end;
end;

procedure TrwDataSet.PLast;
begin
  CheckActive;
  FLockSyncVCL := False;
  try
    FQuery.Last;
    SyncVCLDataSet;
  finally
    FLockSyncVCL := True;
  end;
end;

procedure TrwDataSet.PPrior;
begin
  CheckActive;
  FLockSyncVCL := False;
  try
    FQuery.Prior;
    SyncVCLDataSet;
  finally
    FLockSyncVCL := True;
  end;
end;

function TrwDataSet.PEof: Variant;
begin
  Result := FQuery.Eof;
end;

function TrwDataSet.PBof: Variant;
begin
  Result := FQuery.Bof;
end;

function TrwDataSet.PState: Variant;
begin
  Result := FQuery.State;
end;

function TrwDataSet.PActive: Variant;
begin
  Result := FQuery.Active;
end;

procedure TrwDataSet.POpen;
begin
  Open;
end;

procedure TrwDataSet.PClose;
begin
  Close;
end;

function TrwDataSet.PRecordCount: Variant;
begin
  Result := Integer(FQuery.RecordCount);
end;


procedure TrwDataSet.OnAfterScroll(Sender: TObject);
begin
  if FQuery.NotificationEnabled then
    ExecuteEventsHandler('AfterScroll', []);
end;


function TrwDataSet.PMoveBy(Distance: Variant): Variant;
var
  D: Int64;
begin
  CheckActive;
  FLockSyncVCL := False;
  try
    D := Distance;
    FQuery.MoveBy(D);
    SyncVCLDataSet;
    Result := D;
  finally
    FLockSyncVCL := True;
  end;
end;


function TrwDataSet.PRecordNumber: Variant;
begin
  Result := Integer(FQuery.RecNo);
end;


function TrwDataSet.PFieldExists(ADataField: Variant): Variant;
begin
  CheckActive;
  
  if Assigned(FQuery.Fields.FindField(ADataField)) then
    Result := True
  else
    Result := False;
end;


procedure TrwDataSet.PDisableControls;
begin
  FQuery.DisableNotification;
  if Assigned(FVCLDataSet) then
    FVCLDataSet.DisableControls;
end;


procedure TrwDataSet.PEnableControls;
begin
  FQuery.EnableNotification;
  if Assigned(FVCLDataSet) then
    FVCLDataSet.EnableControls;

  if FQuery.Active and FQuery.NotificationEnabled then
  begin
    if Assigned(FVCLDataSet) then
      if FVCLDataSet.RecordCount > 0 then
        FVCLDataSet.Resync([rmExact])
      else
        FVCLDataSet.Resync([]);
    FQuery.NotifyChange;
  end;
end;


procedure TrwDataSet.GetFieldList(AList: TStrings);
var
  i: Integer;
begin
  AList.Clear;

  if not PActive then
    Open;

  for i := 0 to FQuery.Fields.Count - 1 do
    AList.AddObject(FQuery.Fields[i].FieldName, FQuery.Fields[i]);
end;


function TrwDataSet.GetFieldValue(ADataField: string): Variant;
var
  Fld: TsbField;
begin
  CheckActive;
  Fld := FQuery.Fields.FieldByName(ADataField);
  if not Assigned(Fld) then
    raise ErwException.CreateFmt(ResErrWrongFieldName, [ADataField]);

  Result := Fld.Value;
end;


procedure TrwDataSet.CreateQuery;
begin
  FQuery.OnAfterScroll := OnAfterScroll;
  FQuery.OnChangeDataState := OnChangeDataState;
  FQuery.OnFilterRecord := FilterRecord;
end;


procedure TrwDataSet.Open;
begin
  if not PActive then
  begin
    DoBeforeOpen;
    PDisableControls;
    try
      OpenQuery;
      CreateFields;
      CreateSortIndex;
      if Assigned(FVCLDataSet) then
        FVCLDataSet.Open;
    finally
      PEnableControls;
    end;
    DoAfterOpen;
  end;
end;


procedure TrwDataSet.Close;
begin
  if PActive then
  begin
    DoBeforeClose;
    FQuery.Close;
    if Assigned(FVCLDataSet) then
      FVCLDataSet.Close;
    ClearFields;
    DoAfterClose;
  end;
end;


procedure TrwDataSet.FilterRecord(DataSet: TsbTableCursor; var Accept: Boolean);
var
  V: Variant;
begin
  V := Accept;
  ExecuteEventsHandler('OnFilterRecord', [Integer(Addr(V))]);
  Accept := V;
end;


procedure TrwDataSet.OpenLikeLookUp;
var
  Par: TsbParam;
  i: Integer;
  h, h1, Mfld, Dfld: String;
  fl: Boolean;
  v: Variant;
begin
  FMasterLookupFilter := '';

  if not FMasterDataSource.PActive then
    Close;

  if FMasterDataSource.PState <> sbcBrowse then
    exit;

  fl := False;
  h := '';
  for i := 0 to FMasterFields.Count - 1 do
  begin
    Dfld := FMasterFields[i];
    Mfld := Trim(GetNextStrValue(Dfld, '='));
    Dfld := Trim(Dfld);
    if Dfld = '' then
      Dfld := Mfld;

    if Self is TrwQuery then
      Par := TrwQuery(Self).GetParamByName(Dfld)
    else
      Par := nil;

    if Assigned(Par) then
    begin
      if PActive then
        Close;
      Par.Value := FMasterDataSource.GetFieldValue(Mfld);
      fl := True;
    end

    else if not fl then
    begin
      v := FMasterDataSource.GetFieldValue(Mfld);
      case VarType(v) of
        varString,
        varOleStr,
        varDate:     h1 := ' = "' + VarAsType(v, varString) + '"';

        varInteger,
        varDouble,
        varSmallint,
        varSingle,
        varByte,
        varCurrency: h1 := ' = ' + VarAsType(v, varString);

        varNull:     h1 := ' is null';
      else
        Continue;
      end;

      if Length(h) > 0 then
        h := h + ' and ';
      h := h + Dfld + h1;
    end;
  end;

  if not fl and (Length(h) > 0) then
  begin
    FMasterLookupFilter := h;
    SetFilter(FFilter);
  end;

  if not PActive then
    Open;
end;


procedure TrwDataSet.SetMasterDataSource(const Value: TrwDataSet);
var
  V: TrwDataSet;
begin
  if Value = Self then
    raise ErwException.Create(ResErrCircularReference);

  if Assigned(Value) then
  begin
    if Assigned(FMasterDataSource) then
      FMasterDataSource.UnRegisterComponent(Self);
    V := Value;
    while (V is TrwDataSet) and Assigned(TrwDataSet(V).MasterDataSource) do
      if TrwQuery(V).MasterDataSource = Self then
        raise ErwException.Create(ResErrCircularReference)
      else
        V := TrwDataSet(V).MasterDataSource;

    Value.RegisterComponent(Self)
  end

  else
    if Assigned(FMasterDataSource) then
      FMasterDataSource.UnRegisterComponent(Self);

  FMasterDataSource := Value;
end;


procedure TrwDataSet.SetMasterFields(const Value: TrwStrings);
begin
  FMasterFields.Assign(Value);
end;


function TrwDataSet.GetData: Variant;
var
  DS: TISBasicClientDataSet;
begin
  CheckActive;
  Result := Unassigned;
  DS := sbSBCursorToClientDataSet(FQuery);
  try
    Result := DS.Data;
  finally
    FreeAndNil(DS);
  end;
end;

procedure TrwDataSet.SetData(const Value: Variant);
begin
end;

procedure TrwDataSet.SetFilter(const Value: String);
begin
  PDisableControls;
  try
    FQuery.Filtered := False;
    if Length(FMasterLookupFilter) > 0 then
      if Length(Value) > 0 then
        FQuery.Filter := '(' + Value + ') and ' + FMasterLookupFilter
      else
        FQuery.Filter := FMasterLookupFilter
    else
      FQuery.Filter := Value;

    if (Length(FQuery.Filter) > 0) and FQuery.Active then
      FQuery.Filtered := True
    else if FQuery.Active and (Events['OnFilterRecord'].FCodeAddress <> -1) then
      FQuery.Filtered := True;
    FFilter := Value;

    if Assigned(FOnChangeFilter) then
      FOnChangeFilter(Self);

  finally
    PEnableControls;
    if FQuery.Active and Assigned(FVCLDataSet) then
      FVCLDataSet.First;
  end;
end;


procedure TrwDataSet.RWNotification(Sender: TrwComponent;
  AOperation: TrwNotifyOperation);
begin
  inherited;

  if Sender = FMasterDataSource then
    case AOperation of
      rnoDataSetChanged: OpenLikeLookUp;
      rnoDestroy:        MasterDataSource := nil;
    end;
end;


procedure TrwDataSet.OpenQuery;
begin
  if (Length(FQuery.Filter) > 0) or (Events['OnFilterRecord'].FCodeAddress <> -1) then
    FQuery.Filtered := True;
end;


function TrwDataSet.PFieldCount: Variant;
begin
  Result := FQuery.Fields.Count;
end;



procedure TrwDataSet.SetSort(const Value: String);
var
  fl: Boolean;
begin
  fl := FSort <> Value;

  if Value = '' then
  begin
    if FSort <> '' then
      FQuery.CloseActiveIndex;

    FSort := '';
    if Assigned(FVCLDataSet) and FVCLDataSet.Active then
      FVCLDataSet.First;

    if fl and Assigned(FOnChangeSort) then
      FOnChangeSort(Self);

    Exit;
  end;

  if fl then
  begin
    FSort := Value;
    if FQuery.Active then
    begin
      CreateSortIndex;
      FQuery.NotifyChange;
      if Assigned(FVCLDataSet) then
        FVCLDataSet.First;

      if Assigned(FOnChangeSort) then
        FOnChangeSort(Self);
    end;
  end;
end;


procedure TrwDataSet.ClearFields;
begin
  FFields.Clear;
end;


procedure TrwDataSet.CreateFields;
var
  i: Integer;
  F: TrwField;
  t: TFieldType;
begin
  for i := 0 to FQuery.Fields.Count - 1 do
  begin
    case FQuery.Fields[i].FieldType of
      sbfString:   t := ftString;
      sbfInteger:  t := ftInteger;
      sbfByte:     t := ftBytes;
      sbfDateTime: t := ftDateTime;
      sbfFloat:    t := ftFloat;
      sbfCurrency: t := ftCurrency;
      sbfBoolean:  t := ftBoolean;      
      sbfBLOB:     t := ftBLOB;
    else
      t := ftUnknown;
    end;

    F := TrwField(FFields.Add);
    F.FFieldName := FQuery.Fields[i].FieldName;
    F.FFieldType := t;
    F.FFieldSize := FQuery.Fields[i].Size;
    F.FKeyField  := False;
    F.FsbField := FQuery.Fields[i];
  end;
end;


procedure TrwDataSet.CreateSortIndex;
var
  h, h1, h2, h3, dsc: String;
  A: Array of String;
  fl, tf: Boolean;
begin
  if Length(FSort) = 0 then
    Exit;

  h := FSort;
  h := StringReplace(h, ',', ';', [rfReplaceAll, rfIgnoreCase]);
  while Length(h) > 0 do
  begin
    SetLength(A, Length(A) + 1);
    h1 := Trim(GetNextStrValue(h, ';'));
    h3 := h1;
    dsc := GetNextStrValue(h3, ' ');
    if AnsiSameText(dsc, 'DESC') then
      h1 := h3 + ' DESC';
    A[High(A)] := h1;
  end;

  PDisableControls;
  try
    FQuery.CloseActiveIndex;
    h := sbGetUniqueFileName(GetThreadTmpDir, '~GridSort', sbIndexExt);
    fl := FQuery.Filtered;
    if fl then
      FQuery.Filtered := False;
    FQuery.CloseIndex(h);
    h2 := FQuery.CreateIndex(h, A, []);
    if AnsiSameText(h, h2) then
      tf := True
    else
      tf := False;
    FQuery.SetIndex(h2, tf);
    if fl then
      FQuery.Filtered := True;

  finally
    PEnableControls;
  end;
end;


procedure TrwDataSet.BeforeDestruction;
begin
  inherited;
  if Assigned(FVCLDataSet) then
    FVCLDataSet.Close;
  if Assigned(FQuery) and FQuery.Active then
    FQuery.Close;
end;


function TrwDataSet.VCLDataSet: TsbDataset;
begin
  if not Assigned(FVCLDataSet) then
  begin
    FVCLDataSet := TsbDataSet.Create(nil);
    FVCLDataSet.Cursor := FQuery;
  end;
  Result := FVCLDataSet;
end;


procedure TrwDataSet.SetFilterNoApplay(const Value: String);
begin
  FFilter := Value;
end;

procedure TrwDataSet.SyncVCLDataSet;
begin
  if not FLockSyncVCL and Assigned(FVCLDataSet) and FVCLDataSet.Active and
    (FVCLDataSet.RecNo <> Integer(FQuery.RecNo)) then
    FVCLDataSet.SyncPosition;
end;



function TrwDataSet.IsFilterNotEmpty: Boolean;
begin
  Result := InheritedComponent or IsLibComponent or not (csWriting in ComponentState)
           or (Length(Filter) > 0) ;
end;

function TrwDataSet.IsSortNotEmpty: Boolean;
begin
  Result := InheritedComponent or IsLibComponent or not (csWriting in ComponentState)
            or (Length(Sort) > 0);
end;

procedure TrwDataSet.CheckActive;
begin
  if not FQuery.Active then
    raise ErwException.Create(ResErrDataSetIsNotActive);
end;

procedure TrwDataSet.CheckEditMode;
begin
  if not (FQuery.State in [sbcInsert, sbcAppend, sbcEdit]) then
    raise ErwException.Create(ResErrDataSetIsNotEdit);
end;

procedure TrwDataSet.DoAfterClose;
begin
  ExecuteEventsHandler('AfterClose', []);
end;

procedure TrwDataSet.DoBeforeOpen;
begin
  ExecuteEventsHandler('BeforeOpen', []);
end;

procedure TrwDataSet.DoAfterOpen;
begin
  if FAllowToNotifyFormulas then
    NotifyAggregateFunctions;

  ExecuteEventsHandler('AfterOpen', []);
end;

procedure TrwDataSet.DoBeforeClose;
begin
  if FAllowToNotifyFormulas then
    EmptyAggregateFunctions;
    
  ExecuteEventsHandler('BeforeClose', []);
end;


procedure TrwDataSet.NotifyAggregateFunctions;
begin
  if FQuery.Active and ParticipantOfAggregateFunctions then
  begin
    PDisableControls;
    try
      FQuery.First;
      while not FQuery.Eof do
      begin
        inherited;
        FQuery.Next;
      end;
      FQuery.First;
    finally
      PEnableControls;
    end;
  end;
end;


function TrwDataSet.GetDataStr: String;
begin
  Result := FQuery.GetStrData;
end;


procedure TrwDataSet.SetDataStr(const AValue: String);
begin
end;


function TrwDataSet.PGetDataStr(AStoreStructure, AStoreData: Variant): Variant;
begin
  if AStoreStructure and not AStoreData then
    Result := FQuery.GetStrData(sbDLMStructureOnly)

  else if not AStoreStructure and AStoreData then
    Result := FQuery.GetStrData(sbDLMDataOnly)

  else if AStoreStructure and AStoreData then
    Result := FQuery.GetStrData(sbDLMAll)

  else
    Result := '';
end;

{TrwQuery}

constructor TrwQuery.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  CreateQuery;

  FSQL := TrwStrings.Create;
  TStringList(FSQL).OnChange := FOnChangeSQL;

  FMacros := TStringList.Create;
  FMacroPrefix := '#';
  FParamsFromVars := False;

  FQueryBuilderInfo := TrwQBQuery.Create(Self);
  FQueryBuilderInfo.OnAssign := OnAssignQBInfo;
end;

destructor TrwQuery.Destroy;
begin
  FreeAndNil(FSQL);
  FreeAndNil(FSQLItems);
  FreeAndNil(FQueryBuilderInfo);
  FreeAndNil(FMacros);
  inherited;
end;

procedure TrwQuery.SetQueryBuilderInfo(Value: TrwQBQuery);
begin
  FQueryBuilderInfo.Assign(Value);
end;


function TrwQuery.GetSQL: TrwStrings;
begin
  Result := FSQL;
end;

procedure TrwQuery.FOnChangeSQL(Sender: TObject);
begin
  CreateMacros;
  ChangeSQL;
end;

procedure TrwQuery.SetSQL(Value: TrwStrings);
begin
  FSQL.Assign(Value);
end;

procedure TrwQuery.PSetParameterValue(AName: Variant; Value: Variant);
var
  lParam: TsbParam;
begin
  lParam := GetParamByName(AName);
  if not Assigned(lParam) then
    raise ErwException.CreateFmt(ResErrWrongParameterName, [AName]);

  case VarType(Value) of
    varBoolean:
      if Value then
        lParam.Value := 'Y'
      else
        lParam.Value := 'N';
  else
    lParam.Value := Value;
  end;
end;

function TrwQuery.PGetParameterValue(AName: Variant): Variant;
var
  Par: TsbParam;
begin
  Par := GetParamByName(AName);
  if not Assigned(Par) then
    raise ErwException.CreateFmt(ResErrWrongParameterName, [AName]);

  Result := Par.Value;
end;

procedure TrwQuery.PExecute;
begin
  ExecQuery;
end;

procedure TrwQuery.OnAssignQBInfo(Sender: TObject);
begin
  if IsDesignMode and not ((csLoading in ComponentState) or Loading) or FExternalUse then
    SQL.Text := FQueryBuilderInfo.SQL;
end;

function TrwQuery.SetMacros: String;
var
  i, j: Integer;
  P: TsbParams;
  Par: TsbParam;

  procedure Skip(C: Char; var APos: Integer);
  begin
    repeat
      Inc(APos);
    until (Result[APos] = C) or (APos > Length(Result));

    Inc(APos);
    if (APos <= Length(Result)) and (Result[APos] = C) then
      Skip(C, APos);
  end;

  procedure GetParamsFromVars(AOwner: TrwComponent);
  var
    i, j: Integer;
    O: TrwComponent;
  begin
    if FParamsFromVars then
    begin
      O := AOwner;
      while Assigned(O) and not Assigned(O.GlobalVarFunc) do
        O := TrwComponent(O.Owner);

      if Assigned(O) and Assigned(O.GlobalVarFunc)  then
        for i := 0 to TrwLQQuery(FQuery).Params.ItemCount - 1 do
        begin
          j := O.GlobalVarFunc.Variables.IndexOf(TrwLQQuery(FQuery).Params[i].Name);
          if j <> -1 then
            TrwLQQuery(FQuery).Params[i].Value := O.GlobalVarFunc.Variables[j].Value
          else
            GetParamsFromVars(TrwComponent(O.Owner));  //recursion
        end;
    end;
  end;

begin
  Result := FSQL.Text;
  for i := 0 to FMacros.Count-1 do
    Result := StringReplace(Result, FMacroPrefix+FMacros.Names[i], FMacros.Values[FMacros.Names[i]], [rfReplaceAll, rfIgnoreCase]);

  i := 1;
  while i <= Length(Result) do
  begin
    if Result[i] = '''' then
      Skip('''', i)

    else if Result[i] = '"' then
      Skip('"', i)

    else if Result[i] = FMacroPrefix then
    begin
      j := 1;
      while not (Result[i+j] in [' ', #13, #10]) do
        Inc(j);

      Delete(Result, i, j);
    end

    else
      Inc(i);
  end;

  if TrwLQQuery(FQuery).SQL.Text <> Result then
  begin
    P := TsbParams.Create;
    try
      P.Assign(TrwLQQuery(FQuery).Params);
      TrwLQQuery(FQuery).SQL.Text := Result;

      for i := 0 to P.ItemCount - 1 do
      begin
        Par := TrwLQQuery(FQuery).Params.ParamByName(P[i].Name);
        if Assigned(Par) then
          Par.Value := P[i].Value;
      end;
    finally
      FreeAndNil(P);
    end;
  end;

  if Self is TrmDBTableQuery then
    GetParamsFromVars(TrmDBTableQuery(Self).Table)
  else if Self is TrmASCIIQuery then
    GetParamsFromVars(TrmASCIIQuery(Self).Node)
  else if Self is TrmXMLQuery then
    GetParamsFromVars(TrmXMLQuery(Self).Node)
  else
    GetParamsFromVars(TrwComponent(Owner));
end;


function TrwQuery.PGetMacroValue(AName: Variant): Variant;
begin
  Result := FMacros.Values[AName];
end;

procedure TrwQuery.PSetMacroValue(AName, Value: Variant);
var
  i,j: Integer;
begin
  j := FMacros.Count;
  AName := AnsiUpperCase(AName);
  for i := 0 to FMacros.Count-1 do
    if Length(FMacros.Names[i]) < Length(AName) then
    begin
      j := i;
      break;
    end
    else if AnsiUpperCase(FMacros.Names[i]) = AName then
    begin
      j := -1;
      break;
    end;
  if j = -1 then
    FMacros.Values[AName] := Value
  else
    FMacros.Insert(j, AName+'='+Value);
end;

procedure TrwQuery.SetMacroPrefix(const Value: string);
var
  OldMacroPrefix: String;
begin
  OldMacroPrefix := FMacroPrefix;
  FMacroPrefix := Value;
  if FMacroPrefix = '' then
    FMacroPrefix := '#';
  if Value <> OldMacroPrefix then
    CreateMacros;
end;


procedure TrwQuery.CreateMacros;
begin
  FMacros.Clear;
end;


procedure TrwQuery.CreateQuery;
begin
  FQuery := TrwLQQuery.Create;
  inherited;
end;


procedure TrwQuery.OpenQuery;
begin
  inherited;
  SetMacros;

  if CheckScriptItems then
    ExecSQLScript(0)
  else
    TrwLQQuery(FQuery).OpenSQL;
end;


procedure TrwQuery.ChangeSQL;
begin
  TrwLQQuery(FQuery).SQL.Assign(FSQL);
end;


procedure TrwQuery.ExecQuery;
begin
  SetMacros;

  if CheckScriptItems then
    ExecSQLScript(1)
  else
    TrwLQQuery(FQuery).ExecSQL;
end;


function TrwQuery.GetParamByName(Param: String): TsbParam;
begin
  SetMacros;
  Result := TrwLQQuery(FQuery).Params.ParamByName(Param);
end;

procedure TrwQuery.Prepare;
begin
  SetMacros;

  if CheckScriptItems then
    ExecSQLScript(2)
  else
    TrwLQQuery(FQuery).Prepare;
end;

function TrwQuery.GetMasterQuery: IrmQuery;
begin
  if Assigned(MasterDataSource) and (MasterDataSource is TrwQuery) then
    Result := TrwQuery(MasterDataSource)
  else
    Result := nil;
end;

procedure TrwQuery.Clear;
begin
  QueryBuilderInfo.Clear;
  SQL.Clear;
  OnAssignQBInfo(Self);
end;

function TrwQuery.GetQBQuery: IrwQBQuery;
begin
  Result := QueryBuilderInfo;
end;

function TrwQuery.PParameterExists(AName: Variant): Variant;
begin
  Result := Assigned(GetParamByName(AName));
end;

function TrwQuery.CheckScriptItems: Boolean;
var
  h: String;
  Delim: String;
begin
  Result := False;
  FreeAndNil(FSQLItems);

  h := TrwLQQuery(FQuery).SQL.Text;

  if SameText(GetNextStrValue(h, ' '), 'SCRIPT') then
  begin
    Delim := GetNextStrValue(h, #13);

    FSQLItems := TStringList.Create;
         
    while h <> '' do
      FSQLItems.Add(GetNextStrValue(h, Delim));

    Result := True;
  end;
end;


procedure TrwQuery.ExecSQLScript(ActionType: Byte);
var
  OldSQL: String;
  OldParams: TsbParams;
  i: Integer;
  h: String;
  RunQuery, TempQuery: TrwLQQuery;
begin
  OldSQL := TrwLQQuery(FQuery).SQL.Text;
  OldParams := TsbParams.Create;
  OldParams.Assign(TrwLQQuery(FQuery).Params);

  if ActionType = 0 then
    TempQuery := TrwLQQuery.Create
  else
    TempQuery := nil;  

  try
    for i := 0 to FSQLItems.Count - 1 do
    begin
      if ActionType <> 2 then
      begin
        h := FSQLItems[i];
        h := StringReplace(h, #10, ' ', [rfReplaceAll]);
        h := TrimLeft(StringReplace(h, #13, ' ', [rfReplaceAll]));
        h := GetNextStrValue(h, ' ');

        if SameText(h, 'SELECT') then
          ActionType := 0
        else
          ActionType := 1;
      end;

      if Assigned(TempQuery) and (ActionType = 1) then
        RunQuery := TempQuery
      else
        RunQuery := TrwLQQuery(FQuery);

      RunQuery.SQL.Text := FSQLItems[i];
      RunQuery.Params.Assign(OldParams);
      case ActionType of
        0: RunQuery.OpenSQL;
        1: RunQuery.ExecSQL;
        2: RunQuery.Prepare;
      end;
    end;

  finally
    FreeAndNil(FSQLItems);
    TrwLQQuery(FQuery).SQL.Text := OldSQL;
    TrwLQQuery(FQuery).Params.Assign(OldParams);
    FreeAndNil(OldParams);
    FreeAndNil(TempQuery);
  end;
end;

{TrwField}

procedure TrwField.Assign(Source: TPersistent);
begin
  inherited;
  FieldName := TrwField(Source).FieldName;
  FieldType := TrwField(Source).FieldType;
  FFieldSize := TrwField(Source).FieldSize;
  FKeyField  := TrwField(Source).KeyField;
  FVisible := TrwField(Source).Visible;
  FDisplayLabel := TrwField(Source).DisplayLabel;
  FDisplayWidth := TrwField(Source).DisplayWidth;
end;

constructor TrwField.Create(Collection: TCollection);
begin
  inherited;
  FVisible := True;
  FKeyField := False;
  FDisplayLabel := '';
  FDisplayWidth := 0;
  FsbField := nil;
  FFieldName := '';
end;

function TrwField.GetValue: Variant;
begin
  Result := FsbField.Value;
end;

function TrwField.IsDisplayLabelNotEmpty: Boolean;
begin
  Result := (Length(DisplayLabel) > 0);
end;

procedure TrwField.SetDisplayLabel(const Value: String);
begin
  FDisplayLabel := Value;
  if TrwFields(Collection).Owner is TrwDataSet then
    TrwDataSet(TrwFields(Collection).Owner).SendNotification(rnoLayoutChanged);
end;

procedure TrwField.SetDisplayWidth(const Value: Integer);
begin
  FDisplayWidth := Value;
  if TrwFields(Collection).Owner is TrwDataSet then
    TrwDataSet(TrwFields(Collection).Owner).SendNotification(rnoLayoutChanged);
end;

procedure TrwField.SetFieldName(Value: string);
begin
  if (TrwFields(Collection).Owner <> nil) and
    (not (TrwFields(Collection).Owner is TrwBuffer) or Boolean(TrwBuffer(TrwFields(Collection).Owner).PActive)) then
    Exit;

  FFieldName := Value;

  if (TrwFields(Collection).Owner <> nil) then
    TrwBuffer(TrwFields(Collection).Owner).DropBuffer;
end;

procedure TrwField.SetFieldSize(const Value: Integer);
begin
  if (TrwFields(Collection).Owner <> nil) and
    (not (TrwFields(Collection).Owner is TrwBuffer) or Boolean(TrwBuffer(TrwFields(Collection).Owner).PActive)) then
    Exit;

  if FieldType <> ftString then
    FFieldSize := 0
  else
    FFieldSize := Value;
end;

procedure TrwField.SetFieldType(Value: TFieldType);
begin
  if (TrwFields(Collection).Owner <> nil) and
    (not (TrwFields(Collection).Owner is TrwBuffer) or Boolean(TrwBuffer(TrwFields(Collection).Owner).PActive)) then
    Exit;

  FFieldType := Value;
  if FieldType = ftString then
    FFieldSize := 255
  else
   FFieldSize := 0;

  if (TrwFields(Collection).Owner <> nil) then
    TrwBuffer(TrwFields(Collection).Owner).DropBuffer;
end;


procedure TrwField.SetValue(const Value: Variant);
begin
  FsbField.Value := Value;
end;


procedure TrwField.SetVisible(const Value: Boolean);
begin
  FVisible := Value;
  if TrwFields(Collection).Owner is TrwDataSet then
    TrwDataSet(TrwFields(Collection).Owner).SendNotification(rnoLayoutChanged);
end;


{TrwFields}

function TrwFields.GetItem(Index: Integer): TrwField;
begin
  Result := TrwField(inherited Items[Index]);
end;

procedure TrwFields.SetItem(Index: Integer; Value: TrwField);
begin
  inherited SetItem(Index, TCollectionItem(Value));
end;

procedure TrwFields.Assign(Source: TPersistent);
var
  i: Integer;
  CI: TrwField;
begin
  if (Owner <> nil) and (TrwComponent(Owner).InheritedComponent or TrwComponent(Owner).IsLibComponent) then
  begin
    i := 0;
    while i < Count do
      if not Items[i].InheritedItem then
        Delete(i)
      else
        Inc(i);
    for i := 0 to TrwCollection(Source).Count-1 do
    begin
      CI := FindField(TrwField(TrwCollection(Source).Items[i]).FieldName);
      if TrwCollectionItem(TrwCollection(Source).Items[i]).InheritedItem and
        Assigned(CI) and (TrwField(TrwCollection(Source).Items[i]).FieldType = CI.FieldType) then

//        CI.Assign(TrwCollection(Source).Items[i])

      else if not Assigned(CI) and not (TrwCollectionItem(TrwCollection(Source).Items[i]).InheritedItem) then
      begin
        CI := TrwField(Add);
        CI.Assign(TrwCollection(Source).Items[i]);
      end;
    end;
  end

  else
    inherited;

  if (Owner <> nil) and (Owner is TrwBuffer) then
  begin
    TrwBuffer(Owner).DropBuffer;
    if (TrwBuffer(Owner).FTableName <> '') and (Count > 0) and not TrwBuffer(Owner).LoadingInheritances and
       not TrwBuffer(Owner).Loading then
      TrwBuffer(Owner).CreateBuffer;
  end;
end;

function TrwFields.FindField(AFieldName: string): TrwField;
var
  i: Integer;
begin
  Result := nil;
  AFieldName := AnsiUpperCase(AFieldName);
  for i := 0 to Count-1 do
    if AnsiUpperCase(Items[i].FieldName) = AFieldName then
    begin
      Result := Items[i];
      break;
    end;
end;


function TrwFields.CreateField(const AFieldName: string; const AFieldType: TFieldType; const AFieldSize: Integer;
                               const AKeyField: Boolean = False; const AVisible: Boolean = True): TrwField;
begin
 Result := TrwField(Add);
 Result.FieldName := AFieldName;
 Result.FieldType := AFieldType;
 Result.FieldSize := AFieldSize;
 Result.KeyField  := AKeyField;
 Result.Visible := AVisible;
end;


function TrwFields.PCount: Variant;
begin
  Result := Count;
end;


function TrwFields.PFieldByName(AFieldName: Variant): Variant;
begin
  Result := Integer(Pointer(FindField(AFieldName)));
end;


function TrwFields.PItems(AIndex: Variant): Variant;
begin
  if (Count = 0) and (GetOwner is TrwQuery) then
    raise ErwException.Create('Dataset is not opened. ' + TrwQuery(GetOwner).GetPathFromRootOwner);

  Result := Integer(Pointer(GetItem(AIndex)));
end;


function TrwFields.PCreateField(AFieldName, AFieldType, AFieldSize: Variant): Variant;
begin
  Result := Integer(Pointer(CreateField(AFieldName, TFieldType(Integer(AFieldType)),
                            AFieldSize, False, True)));
end;


{TrwBuffer}

constructor TrwBuffer.Create(AOwner: TComponent);
begin
  inherited;

  FCreated := False;
  FFieldsRead := TrwFields.Create(nil, TrwField);
  FCreateSQL := '';
  FExecQuery := TrwLQQuery.Create;

  CreateQuery;
end;


destructor TrwBuffer.Destroy;
begin
  DropBuffer;
  FreeAndNil(FFieldsRead);
  FreeAndNil(FExecQuery);
  inherited;
end;


procedure TrwBuffer.SetFields(Value: TrwFields);
begin
  Fields.Assign(Value);
end;

procedure TrwBuffer.SetTableName(Value: string);
begin
  if FTableName <> Value then
  begin
    DropBuffer;
    FTableName := Trim(Value);
    if (FTableName <> '') and (Fields.Count > 0) and not LoadingInheritances and not Loading then
      CreateBuffer;
  end;
end;

procedure TrwBuffer.CreateBuffer;
var
  h: string;
  lFldType: string;
  i: Integer;
begin
  if FCreated then Exit;

  if not Loading and ((FTableName = '') or (Fields.Count = 0)) then
    raise ErwException.CreateFmt(ResErrWrongBufferStructure, [Name]);

  h := 'create buffer ' + FTableName + ' (';

  for i := 0 to Fields.Count - 1 do
  begin
    if (i > 0) then
      h := h + ', ';

    case Fields[i].FieldType of
      ftDate,
      ftDateTime: lFldType := 'Date';

      ftFloat:    lFldType := 'Float';

      ftCurrency: lFldType := 'Currency';

      ftInteger: lFldType := 'Integer';

      ftBoolean: lFldType := 'Boolean';

      ftString:  if Fields[i].FieldSize <= 255 then
                   lFldType := 'String('+IntToStr(Fields[i].FieldSize)+')'
                 else
                   lFldType := 'Blob';

      ftMemo,
      ftBlob,
      ftGraphic: lFldType := 'Blob';
    else
      raise ErwException.Create('Field type is not applicable');
    end;

    h := h + Fields[i].FieldName + ' ' + lFldType;
  end;

  h := h + ')';

  if FCreateSQL <> h then
  begin
    FExecQuery.SQL.Text := h;
    FExecQuery.ExecSQL;
    FCreateSQL := h;
    FCreated := True;
  end;
end;


procedure TrwBuffer.DropBuffer;
begin
  if FCreated then
  begin
    Close;
    FExecQuery.SQL.Text := 'drop buffer ' + FTableName;
    FExecQuery.ExecSQL;
    FCreateSQL := '';
    FCreated := False;
  end;
end;


procedure TrwBuffer.PClear;
var
  fl: Boolean;
begin
  if FCreated then
  begin
    PDisableControls;
    try
      fl := FQuery.Active;
      if not fl then
      begin
        FixUpTableName;
        FQuery.Open;
      end;

      FQuery.Clear;

      if not fl then
        FQuery.Close;
    finally
      PEnableControls;
    end;
  end;
end;



procedure TrwBuffer.Loaded;
var
  i: Integer;
begin
  for i := 0 to FFieldsRead.Count -1 do
    if not FFieldsRead[i].InheritedItem then
      if not Assigned(FFields.FindField(FFieldsRead[i].FieldName)) then
        FFields.Add.Assign(FFieldsRead[i]);

  FFieldsRead.Clear;

  inherited;
  if (FTableName <> '') and (Fields.Count > 0) and not LoadingInheritances then
    CreateBuffer;
end;


procedure TrwBuffer.SetInheritedComponentProperty(AValue: Boolean);
begin
  inherited;
  FFields.SetInherited(AValue);
end;

procedure TrwBuffer.AfterInitLibComponent;
begin
  inherited;
  FFields.SetInherited(True);
end;

function TrwBuffer.GetFields: TrwFields;
begin
  if Loading or (csLoading in ComponentState) or (csWriting in ComponentState) then
    Result := FFieldsRead
  else
    Result := FFields;
end;


procedure TrwBuffer.CreateQuery;
begin
  FQuery := TsbTableCursor.Create;
  inherited;
end;


procedure TrwBuffer.OpenQuery;
begin
  CreateBuffer;
  FixUpTableName;
  inherited;
  FQuery.Open;
end;


procedure TrwBuffer.FixUpTableName;
var
  T: TrwDataDictTable;
begin
  T := TrwDataDictTable(DataDictionary.Tables.TableByName(TableName));
  if Assigned(T) then
    FQuery.TableName := T.ExternalName;
end;


procedure TrwBuffer.SetData(const Value: Variant);
var
  DS: TISRWClientDataSet;
begin
  DropBuffer;

  if VarIsNull(Value) then
    Exit;

  DS := TISRWClientDataSet.Create(nil);
  try
    DS.Data := Value;

    CreateFieldStructure(DS);

    CreateBuffer;
    FixUpTableName;

    DoBeforeOpen;
    sbClientDataSetToSBCursor(DS, FQuery, False);
    CreateFields;    
    DoAfterOpen;
  finally
    FreeAndNil(DS);
  end;
end;


procedure TrwBuffer.ClearFields;
var
  i: Integer;
begin
  for i := 0 to FFields.Count - 1 do
    FFields[i].FsbField := nil;
end;


procedure TrwBuffer.CreateFields;
var
  i: Integer;
begin
  for i := 0 to FQuery.Fields.Count - 1 do
    FFields[i].FsbField :=  FQuery.Fields[i];
end;

procedure TrwBuffer.PCreateLikeDataSet(ADataSet: Variant);
begin
  Close;
  Fields.Assign(TrwDataSet(Pointer(Integer(ADataSet))).Fields);
  CreateBuffer;
  FixUpTableName;
end;


procedure TrwBuffer.CreateFieldStructure(ADataSet: TDataSet);
var
  i: Integer;
begin
  Fields.Clear;
  for i := 0 to ADataSet.Fields.Count - 1 do
    Fields.CreateField(ADataSet.Fields[i].FieldName,
      ADataSet.Fields[i].DataType, ADataSet.Fields[i].Size, False);
end;


procedure TrwBuffer.PFillLikeDataSet(ADataSet: Variant);
var
  DS: TsbTableCursor;
  i: Integer;
begin
  Close;
  PCreateLikeDataSet(ADataSet);
  DS := TrwDataSet(Pointer(Integer(ADataSet))).FQuery;

  if not DS.IndexEnabled then
    sbCopyTable(DS, FQuery.TableName)
  else
  begin
    FQuery.DisableNotification;
    DS.DisableNotification;
    try
      FQuery.Open;
      DS.First;
      while not DS.Eof do
      begin
        FQuery.Append;
        for i := 0 to DS.Fields.Count - 1 do
          FQuery.Fields[i].Value := DS.Fields[i].Value;
        FQuery.Post;
        DS.Next;
      end;
      FQuery.Close;
    finally
      DS.EnableNotification;
      FQuery.EnableNotification;
      DS.NotifyChange;
    end;
  end;
end;

procedure TrwBuffer.WriteState(Writer: TWriter);
var
  i: Integer;
begin
  FFieldsRead.Clear;
  for i := 0 to FFields.Count - 1 do
    if not FFields[i].InheritedItem then
      FFieldsRead.Add.Assign(FFields[i]);

  inherited;
end;

procedure TrwBuffer.LoadFromFile(AFileName, ADelimiter, AScreenChar: String);
var
  F: TextFile;
  sLine: String;
  fDelim: Boolean;

  procedure DelimitedReading;
  var
    i, j, k: Integer;
    h: String;

    function FindChar(AIndex: Integer; AChar: Char): Integer;
    begin
      Result := 0;
      while Length(sLine) >= AIndex do
      begin
        if sLine[AIndex] = AChar then
        begin
          Result := AIndex;
          break;
        end;
        Inc(AIndex);
      end;
    end;

  begin
    j := 1;
    for i := 0 to Fields.Count - 1 do
    begin
      if Length(sLine) < j then
        break;

      if Length(AScreenChar) > 0 then
      begin
        j := FindChar(j, AScreenChar[1]);
        Inc(j);
        k := FindChar(j, AScreenChar[1]);
        Dec(k);
      end
      else
      begin
        k := FindChar(j, ADelimiter[1]);
        if k = 0 then
          k := Length(sLine)
        else
          Dec(k);
      end;

      h := Copy(sLine, j, k - j + 1);
      Fields[i].Value := h;
      if Length(AScreenChar) > 0 then
        j := k + 2
      else
        j := k + 1;

      if Length(ADelimiter) > 0 then
      begin
        j := FindChar(j, ADelimiter[1]);
        if j = 0 then
          j := Length(sLine) + 1
        else
          Inc(j);
      end;
    end;
  end;


  procedure FixedLengthReading;
  var
    i, j: Integer;
    h: String;
  begin
    j := 1;
    for i := 0 to Fields.Count - 1 do
    begin
      if Length(sLine) < j then
        break;
      h := Copy(sLine, j, Fields[i].DisplayWidth);
      Fields[i].Value := h;
      Inc(j, Fields[i].DisplayWidth);
    end;
  end;

begin
  fDelim := (ADelimiter <> '') or (AScreenChar <> '');

  AssignFile(F, AFileName);
  POpen;
  try
    Reset(F);

    while not Eof(F) do
    begin
      ReadLn(F, sLine);
      if Length(sLine) = 0 then
        Continue;

      PAppend;
      try
        if fDelim then
          DelimitedReading
        else
          FixedLengthReading;
        PPost;
      except
        PCancel;
        raise;
      end;
    end;

  finally
    CloseFile(F);
    PClose;
  end;
end;


procedure TrwBuffer.PLoadFromFile(AFileName, ADelimiter, AScreenChar: Variant);
begin
  LoadFromFile(AFileName, ADelimiter, AScreenChar);
end;


function TrwBuffer.IsFieldsNotEmpty: Boolean;
var
  i: Integer;
begin
  if not (csWriting in ComponentState) then
    Result := True
  else
  begin
    Result := False;
    for i := 0 to FFields.Count - 1 do
      if not FFields[i].InheritedItem then
      begin
        Result := True;
        break;
      end;
  end;
end;

procedure TrwBuffer.SetDataStr(const AValue: String);
begin
  if AValue = '' then
    Exit;

  PDisableControls;
  try
    if Fields.Count = 0 then
    begin
      if not AnsiSameText(Copy(AValue, 1, 6), 'FIELDS') then
        raise ErwException.Create('Data requires structure description');

      Fields.CreateField('Fld', ftInteger, 0);
      CreateBuffer;
      FixUpTableName;
      DropBuffer;

      FQuery.SetStrData(AValue, sbDLMStructureOnly);
      FQuery.Open;
      inherited ClearFields;
      inherited CreateFields;
      FQuery.Close;

      sbDropTable(FQuery.TableName);
      CreateBuffer;
      FixUpTableName;
    end

    else
      if not FQuery.Active then
        FQuery.Clear;

    FQuery.SetStrData(AValue, sbDLMDataOnly);

  finally
    PEnableControls;
  end;

  Open;
end;

procedure TrwBuffer.PSetDataStr(AData, ALoadStructure, ALoadData: Variant);
begin
  if AData = '' then
    Exit;

  PDisableControls;
  try
    if (Fields.Count = 0) or ALoadStructure then
    begin
      if not AnsiSameText(Copy(AData, 1, 6), 'FIELDS') then
        raise ErwException.Create('Data requires structure description');

      FQuery.Close;        
      inherited ClearFields;
      Fields.CreateField('Fld', ftInteger, 0);
      CreateBuffer;
      FixUpTableName;
      DropBuffer;

      FQuery.SetStrData(AData, sbDLMStructureOnly);
      FQuery.Open;
      inherited ClearFields;
      inherited CreateFields;
      FQuery.Close;

      sbDropTable(FQuery.TableName);
      CreateBuffer;
      FixUpTableName;

      if ALoadData then
        FQuery.SetStrData(AData, sbDLMDataOnly);
    end

    else if ALoadData then
    begin
      if not FQuery.Active then
        FQuery.Clear;

      if ALoadData then
        FQuery.SetStrData(AData, sbDLMDataOnly);
    end;

  finally
    PEnableControls;
  end;

  Open;
end;

end.
