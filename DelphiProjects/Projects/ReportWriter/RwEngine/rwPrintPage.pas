unit rwPrintPage;

interface

uses Classes, ExtCtrls, rwTypes, Graphics, Controls, Windows, SysUtils,
     Forms, Messages, rwPreviewUtils, rwPrinters, rwEngineTypes,
     BlockCiphers, AbZipPrc, AbUnzPrc, rwGraphics, EvStreamUtils, isBaseClasses;

type

  TrwReportArchiveHeader = class(TComponent)
  private
    FPrepared: TDateTime;
    FReportType: TReportType;
    FPagesNum: Integer;
    FPixelsPerInch: Integer;

  published
    property ReportType: TReportType read FReportType write FReportType;
    property Prepared: TDateTime read FPrepared write FPrepared;
    property PagesNum: Integer read FPagesNum write FPagesNum;
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;
  end;


  TrwPrintRepository = class(TComponent)
  private
    FPixelsPerInch: Integer;
  protected
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
  published
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;
  end;


  TrwPrintMark = class(TCollectionItem)
  private
    FPageNum: Integer;
    FTop: SmallInt;
    FLeft: SmallInt;
    FHeight: SmallInt;
    FWidth: SmallInt;
    FText: string;
    function  GetData: string;
    procedure SetData(const Value: string);
  public
    property PageNum: Integer read FPageNum write FPageNum;
    property Left: SmallInt read FLeft write FLeft;
    property Top: SmallInt read FTop write FTop;
    property Width: SmallInt read FWidth write FWidth;
    property Height: SmallInt read FHeight write FHeight;
    property Text: String read FText write FText;

    procedure Assign(Source: TPersistent); override;

  published
    property Data: string read GetData write SetData;
  end;


  TrwPrintMarks = class(TCollection)
  private
    function  GetItem(Index: Integer): TrwPrintMark;
    procedure SetItem(Index: Integer; const Value: TrwPrintMark);
  public
    property Items[Index: Integer]: TrwPrintMark read GetItem write SetItem; default;
  end;

  IrwLabelChanges = interface(IisInterfacedObject)
  ['{194DEDB4-3EB5-4685-859C-88A699497FD4}']
    procedure AddChange(const APageNum: integer; const AItemNum: integer; const AChange: String);
    procedure DeleteChange(const APageNum: integer; const AItemNum: integer);
    function  FindChange(const APageNum: integer; const AItemNum: integer): String;
    procedure InitializeChanges(const APagesCount: integer);
    function  ChangesCount: integer;
  end;

  TrwLabelChanges = class(TisInterfacedobject, IrwLabelChanges)
  private
    FPagesList: IisInterfaceList;
    FCurrentPageChanges: IisListOfValues;
    FCurrentPageNum: integer;
    FInitialized: boolean;

    procedure Clear;
    function  InternalSearch(const APageNum: integer; const AItemNum: String): IisNamedValue;
  protected
    procedure DoOnConstruction; override;

    // IrwLabelChanges
    procedure AddChange(const APageNum: integer; const AItemNum: integer; const AChange: String);
    procedure DeleteChange(const APageNum: integer; const AItemNum: integer);
    function  FindChange(const APageNum: integer; const AItemNum: integer): String;
    procedure InitializeChanges(const APagesCount: integer);
    function  ChangesCount: integer;

    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
  public
    destructor Destroy; override;
    class function GetTypeID: String; override;
  end;

  TrwPrintHeader = class(TComponent)
  private
    FPagesList: TList;
    FReposList: TList;
    FReportType: TReportType;
    FVersion: Byte;
    FPixelsPerInch: Integer;
    FLayersList: TList;
    FMarks: TrwPrintMarks;
    FTraysList: TStrings;
    FOutBinsList: TList;
    FLabelChangesList: IrwLabelChanges;

    procedure WriteReposList(Writer: TWriter);
    procedure ReadReposList(Reader: TReader);
    procedure WritePagesList(Writer: TWriter);
    procedure ReadPagesList(Reader: TReader);
    procedure WriteLayersList(Writer: TWriter);
    procedure ReadLayersList(Reader: TReader);
    procedure WriteTraysList(Writer: TWriter);
    procedure ReadTraysList(Reader: TReader);
    procedure WriteOutBinsList(Writer: TWriter);
    procedure ReadOutBinsList(Reader: TReader);
    function  LayersListNotEmpty: Boolean;
    function  TraysListNotEmpty: Boolean;
    function  OutBinsListNotEmpty: Boolean;
    function  MarksNotEmpty: Boolean;
    procedure SetMarks(const Value: TrwPrintMarks);

  protected
    procedure DefineProperties(Filer: TFiler); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function PagesCount: Integer;
    property ReposList: TList read FReposList;
    property LayersList: TList read FLayersList;
    property PagesList: TList read FPagesList;
    property TraysList: TStrings read FTraysList;
    property OutBinsList: TList read FOutBinsList;
    property LabelChangesList: IrwLabelChanges read FLabelChangesList;

  published
    property ReportType: TReportType read FReportType write FReportType;
    property Version: Byte read FVersion write FVersion;
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;
    property Marks: TrwPrintMarks read FMarks write SetMarks stored MarksNotEmpty;
  end;


  TrwMarkTextWindow = class(THintWindow)
  private
    FMark: TrwPrintMark;
    procedure ShowMarkText(AMark: TrwPrintMark; AMousePos: TPoint);
  end;


  TrwHighlightInfoRec = record
    Text: String;
    TextRect: TRect;
    TextObjPosition: Int64;
  end;

  PTrwHighlightInfoRec = ^TrwHighlightInfoRec;


  {TrwVirtualPage is sheet of paper (page) for generating result}

  TrwVirtualPage = class(TCustomControl)
  private
    FRepository: TrwPrintRepository;
    FHeader: TrwPrintHeader;
    FPageOrientation: TrwPaperOrientation;
    FPageNum: Integer;
    FPageSize: TrwPaperSize;
    FDuplexing: Boolean;
    FKf_ZoomX: Extended;
    FKf_ZoomY: Extended;
    FMarkTextWnd: TrwMarkTextWindow;
    FTray: String;
    FOutBin: Integer;
    FUnusedImagesReleased: Boolean;

    procedure WritePageData(Writer: TWriter);
    procedure ReadPageData(Reader: TReader);
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    function  GetTray: String;
    function  GetPixelsPerInch: Integer;

  protected
    FCanvas: TCanvas;
    FData: TStream;
    FPrnResX:   Integer;
    FPrnResY:   Integer;
    FPrinterOffSets: TPoint;
    FScaleFonts: Boolean;
    FReadBounds: TRect;
    FHighlightInfo: TrwHighlightInfoRec;

    procedure ReadState(Reader: TReader); override;
    procedure DefinePrintMetric;
    procedure DefineProperties(Filer: TFiler); override;
    procedure Paint; override;
    procedure DrawMark(AMark: TrwPrintMark; ACanvas: TCanvas);
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure HideMarkText;
    procedure ShowMarkText(AMark: TrwPrintMark; X, Y: Word);
    procedure Loaded; override;
    procedure ScaleSize; virtual;
    procedure OnChangeRepository; virtual;
    procedure ReleaseImageHandlers;

  public
    property Header: TrwPrintHeader read FHeader write FHeader;
    property Repository: TrwPrintRepository read FRepository write FRepository;
    property Data: TStream read FData;
    property PixelsPerInch: Integer read GetPixelsPerInch;
    property Kf_ZoomX: Extended read FKf_ZoomX;
    property Kf_ZoomY: Extended read FKf_ZoomY;
    property PrinterOffSets: TPoint read FPrinterOffSets;
    property PrnResX: Integer read FPrnResX;
    property PrnResY: Integer read FPrnResY;
    property ScaleFonts: Boolean read FScaleFonts;
    property PageData: TStream read FData;
    property Tray: String read GetTray;
    property OutBin: Integer read FOutBin;
    property HighlightInfo: TrwHighlightInfoRec read FHighlightInfo;

    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure ChangeZoom(kX, kY: Extended);
    function  CreateMark(ALeft, ATop, AWidth, AHeight: Word; AText: string; APageNum: Integer = 0): TrwPrintMark;
    function  MarkAtPos(X, Y: Word): TrwPrintMark;
    function  ScaleRect(ARect: TRect): TRect;
    procedure PreparePage(const APageNum: Integer; AData: TStream); virtual;
    procedure Initialize(AStream: TStream);
    procedure WriteHeaderInfo(AStream: TStream);
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;

  published
    property Top stored False;
    property Left stored False;
    property PageSize: TrwPaperSize read FPageSize write FPageSize;
    property PageOrientation: TrwPaperOrientation read FPageOrientation write FPageOrientation;
    property Duplexing: Boolean read FDuplexing write FDuplexing default False;
    property PageNum: Integer read FPageNum write FPageNum stored False;
  end;

  procedure StorePrintObject(AComponent: TComponent; APage: TrwVirtualPage); overload;
  procedure CompressPage(APage: TrwVirtualPage);
  procedure SecureRWA(ASource: TStream; ADest: TStream; const PreviewPassword, PrintPassword: String; Compress: Boolean = True);
  function  UnSecureRWA(ASource: TStream; ADest: TStream; const Password: String): TrwRWASecurityRec;
  function  IsSecureRWA(AData: TStream): Byte;
  function  IsRWAFormat(AData: TStream): Boolean;
  function  IsXMLFormat(AData: TStream): Boolean;  
  function  IsXLSFormat(AData: TStream): Boolean;

implementation

uses rwPrintElements, rwOneRepPreviewFrm, isBasicUtils;


const
  cSecureTag = 'RWS';
  cRWADNA    = 'TPF0';
  cXMLDNA    = '<?xml';



function IsSecureRWA(AData: TStream): Byte;
var
  s: String;
  p: Integer;
begin
  Result := 0;
  p := AData.Position;
  try
    AData.Position := 0;
    s := StringOfChar(' ',  Length(cSecureTag));
    AData.Read(s[1], Length(cSecureTag));
    if AnsiSameStr(s, cSecureTag) then
      AData.Read(Result, SizeOf(Result));
  finally
    AData.Position := p;
  end;
end;

function IsRWAFormat(AData: TStream): Boolean;
var
  s: String;
  p: Integer;
  n: Byte;
begin
  Result := False;
  p := AData.Position;
  try
    AData.Position := 0;
    s := StringOfChar(' ', Length(cRWADNA));
    AData.Read(s[1], Length(cRWADNA));
    if AnsiSameStr(s, cRWADNA) then
    begin
      AData.Read(n, 1);
      s := StringOfChar(' ', n);
      AData.Read(s[1], n);
      Result := SameText(s, 'TrwRenderingPaper') or SameText(s, 'TrwEnginePaper') or SameText(s, 'TrwVirtualPage') or
                SameText(s, 'TrwPrintRepository');
    end;

  finally
    AData.Position := p;
  end;
end;

function IsXMLFormat(AData: TStream): Boolean;
var
  s: String;
  p: Integer;
begin
  p := AData.Position;
  try
    AData.Position := 0;
    s := StringOfChar(' ', Length(cXMLDNA));
    AData.Read(s[1], Length(cXMLDNA));
    Result := AnsiSameStr(s, cXMLDNA);
  finally
    AData.Position := p;
  end;
end;

function  IsXLSFormat(AData: TStream): Boolean;
var
  s, s1: String;
  p: Integer;
  xlsSignature: String;
begin
  xlsSignature := 'D0CF11E0A1B11AE1';
  p := AData.Position;
  try
    AData.Position := 0;
    s := StringOfChar(' ', 8);
    s1 := StringOfChar(' ', 8);
    HexToBin(PChar(xlsSignature), PChar(s1), 8);
    AData.Read(s[1], 8);
    Result := AnsiSameStr(s, s1);
  finally
    AData.Position := p;
  end;
end;

procedure SecureRWA(ASource: TStream; ADest: TStream; const PreviewPassword, PrintPassword: String; Compress: Boolean = True);
var
  b: Byte;
  n: Byte;
  h: String;
  BF: TBlowfishCipherFixed;
  hS: TTempFileStream;
  Psw: String;

  function EncryptStr(const Passwd: String; var Data: String): Boolean;
  var
    BF: TBlowfishCipherFixed;
  begin
    try
      BF := TBlowfishCipherFixed.CreateStrKey(Passwd);
      try
        Data := Char(Byte(Length(Data))) + Data;
        Data := BF.EncryptedString(Data);
      finally
        BF.Free;
      end;

      Result := True;

    except
      Result := False
    end;
  end;

begin
  if IsSecureRWA(ASource) <> 0 then
    raise ErwException.Create('RWA stream error');

  hS := TTempFileStream.Create;
  try
    if Compress then
      DeflateStream(ASource, hS)
    else
      hS.CopyFrom(ASource, ASource.Size - ASource.Position);
    hS.Position := 0;

    ADest.Size := 0;
    ADest.Write(cSecureTag[1], Length(cSecureTag));

    if Compress then
      b := 1
    else
      b := 0;

    if (PreviewPassword <> '') or (PrintPassword <> '') then
      b := b or 2;

    ADest.Write(b, SizeOf(b));

    if (b and 2) <> 0 then
    begin
      Random(MaxInt);
      Psw := IntToStr(Random(MaxInt));

      h := Psw;
      EncryptStr(PreviewPassword, h);
      n := Length(h);
      ADest.Write(n, SizeOf(n));
      ADest.Write(h[1], Length(h));

      if PrintPassword <> '' then
      begin
        h := PreviewPassword;
        EncryptStr(PrintPassword, h);
      end
      else
        h := '';
      n := Length(h);
      ADest.Write(n, SizeOf(n));
      if h <> '' then
        ADest.Write(h[1], Length(h));

      BF := TBlowfishCipherFixed.CreateStrKey(Psw);
      try
        BF.EncryptStream(hS, ADest);
      finally
        BF.Free;
      end;
    end

    else
      ADest.CopyFrom(hS, hS.Size);

  finally
    hS.Free;
  end;
end;


function UnSecureRWA(ASource: TStream; ADest: TStream; const Password: String): TrwRWASecurityRec;
var
  ss: Integer;
  BF: TBlowfishCipherFixed;
  hS1, hs2: TTempFileStream;
  s: Byte;
  Psw, PrintP, PreviewP: String;

  function DecryptStr(const Passwd: String; var Data: String): Boolean;
  var
    BF: TBlowfishCipherFixed;
  begin
    Result := False;
    try
      BF := TBlowfishCipherFixed.CreateStrKey(Passwd);
      try
        Data := BF.DecryptedString(Data);
        if Length(Data)>= 2 then
        begin
          if Ord(Data[1]) = (Length(Data) - 1) then
          begin
            Delete(Data, 1, 1);
            Result := True;
          end;
        end;
      finally
        BF.Free;
      end;

    except
    end;
  end;

begin
  Result.Rights := 0;
  Result.Compressed := False;

  ss := IsSecureRWA(ASource);
  if ss = 0 then
    raise ErwException.Create('RWA stream error');

  ASource.Position := Length(cSecureTag) + SizeOf(Byte);

  hS1 := TTempFileStream.Create;
  hS2 := TTempFileStream.Create;
  try
    if (ss and 2) <> 0 then
    begin
      Psw := '';

      ASource.Read(s, SizeOf(s));
      SetLength(PreviewP, s);
      ASource.Read(PreviewP[1], s);

      ASource.Read(s, SizeOf(s));
      SetLength(PrintP, s);
      ASource.Read(PrintP[1], s);

      if DecryptStr(Password, PrintP) then
      begin
        if DecryptStr(PrintP, PreviewP) then
        begin
          Psw := PreviewP;
          Result.PreviewPassword := PrintP;
          Result.PrintPassword := Password;
          Result.Rights := 2;
        end;
      end
      else
      begin if DecryptStr(Password, PreviewP) then
        Result.PreviewPassword := Password;
        Result.PrintPassword := '';
        Psw := PreviewP;
        Result.Rights := 1;
      end;

      if Result.Rights = 0 then
        raise ErwException.Create('Password is incorrect');

      BF := TBlowfishCipherFixed.CreateStrKey(Psw);
      try
        hS1.Size := 0;
        hS1.CopyFrom(ASource, ASource.Size - ASource.Position);
        hS1.Position := 0;
        hS2.Size := 0;
        BF.DecryptStream(hS1, hS2);
      finally
        BF.Free;
      end;
    end

    else
    begin
      hS2.CopyFrom(ASource, ASource.Size - 4);
      Result.Rights := 2;
    end;

    if ASource = ADest then
      ADest.Size := 0;

    hS2.Position := 0;
    if (ss and 1) <> 0 then
    begin
      Result.Compressed := True;
      hS1.Size := 0;
      InflateStream(hS2, hS1);
      hS1.Position := 0;
      ADest.CopyFrom(hS1, hS1.Size);
    end
    else
      ADest.CopyFrom(hS2, hS2.Size);

  finally
    hS1.Free;
    hS2.Free;
  end;
end;


procedure StorePrintObject(AComponent: TrwPrintObject; APage: TrwVirtualPage); overload;
var
  i: Integer;
  PO: TrwPrintObject;
begin
  PO := nil;
  for i := 0 to APage.Repository.ComponentCount - 1 do
  begin
    PO := TrwPrintObject(APage.Repository.Components[i]);
    if PO.ClassInfo = AComponent.ClassInfo then
      if PO.CompareObject(AComponent) then
        break;
    PO := nil;
  end;

  if not Assigned(PO) then
  begin
    PO := TrwPrintObjectClass(AComponent.ClassType).Create(APage.Repository);
    PO.Assign(AComponent);
  end
  else
    PO.Assign(AComponent, True);

  PO.WriteObjectInfo(APage.FData);
end;



procedure StorePrintObject(AComponent: TComponent; APage: TrwVirtualPage); overload;
var
  i: Integer;
  CI: TClass;
  obj_class: TrwPrintObjectClass;
  PO: TrwPrintObject;
  F: TGraphicFrame;
begin
  if AComponent is TrwPrintObject then
  begin
     StorePrintObject(TrwPrintObject(AComponent), APage);
     Exit;
  end;

  CI := AComponent.ClassInfo;
  if CI = TFrameLabel.ClassInfo then
    obj_class := TrwPrintText
  else if CI = TGraphicFrame.ClassInfo then
    obj_class := TrwPrintFrame
  else if CI = TExtShape.ClassInfo then
    obj_class := TrwPrintShape
  else if CI = TPCLLabel.ClassInfo then
    obj_class := TrwPrintPCLText
  else if CI = TFrameImage.ClassInfo then
    obj_class := TrwPrintImage
  else if CI = TExtBarcode1D.ClassInfo then
    obj_class := TrwPrintBarcode1D
  else if CI = TExtBarcodePDF417.ClassInfo then
    obj_class := TrwPrintBarcodePDF417
  else
    raise ErwException.Create('Unknown print object ' + AComponent.ClassName);

  PO := nil;
  for i := 0 to APage.Repository.ComponentCount - 1 do
  begin
    PO := TrwPrintObject(APage.Repository.Components[i]);
    if PO.ClassInfo = obj_class.ClassInfo then
      if PO.CompareObject(AComponent) then
        break;
    PO := nil;
  end;

  if not Assigned(PO) then
    PO := obj_class.CreateAs(APage.Repository, AComponent);

  PO.WriteObjectInfo(APage.FData, AComponent);

  if (CI = TFrameLabel.ClassInfo) and
     (TFrameLabel(AComponent).BoundLines <> []) then
  begin
    F := TGraphicFrame.Create(nil);
    try
      F.LayerNumber := TFrameLabel(AComponent).LayerNumber;
      F.Transparent := True;
      F.Compare := TFrameLabel(AComponent).Compare;
      F.SetBounds(TFrameLabel(AComponent).Left, TFrameLabel(AComponent).Top,
        TFrameLabel(AComponent).Width, TFrameLabel(AComponent).Height);
      F.BoundLines := TFrameLabel(AComponent).BoundLines;
      F.BoundLinesColor := TFrameLabel(AComponent).BoundLinesColor;
      F.BoundLinesWidth := TFrameLabel(AComponent).BoundLinesWidth;
      F.BoundLinesStyle := TFrameLabel(AComponent).BoundLinesStyle;
      StorePrintObject(F, APage);
    finally
      F.Free;
    end;
  end

  else if (CI = TFrameImage.ClassInfo) and
     (TFrameImage(AComponent).BoundLines <> []) then
  begin
    F := TGraphicFrame.Create(nil);
    try
      F.LayerNumber := TFrameImage(AComponent).LayerNumber;
      F.Transparent := True;
      F.Compare := TFrameImage(AComponent).Compare;
      F.SetBounds(TFrameImage(AComponent).Left, TFrameImage(AComponent).Top,
        TFrameImage(AComponent).Width, TFrameImage(AComponent).Height);
      F.BoundLines := TFrameImage(AComponent).BoundLines;
      F.BoundLinesColor := TFrameImage(AComponent).BoundLinesColor;
      F.BoundLinesWidth := TFrameImage(AComponent).BoundLinesWidth;
      F.BoundLinesStyle := TFrameImage(AComponent).BoundLinesStyle;
      StorePrintObject(F, APage);
    finally
      F.Free;
    end;
  end

  else if (CI = TPCLLabel.ClassInfo) and
     (TPCLLabel(AComponent).BoundLines <> []) then
  begin
    F := TGraphicFrame.Create(nil);
    try
      F.LayerNumber := TPCLLabel(AComponent).LayerNumber;
      F.Transparent := True;
      F.Compare := TPCLLabel(AComponent).Compare;
      F.SetBounds(TPCLLabel(AComponent).Left, TPCLLabel(AComponent).Top,
        TPCLLabel(AComponent).Width, TPCLLabel(AComponent).Height);
      F.BoundLines := TPCLLabel(AComponent).BoundLines;
      F.BoundLinesColor := TPCLLabel(AComponent).BoundLinesColor;
      F.BoundLinesWidth := TPCLLabel(AComponent).BoundLinesWidth;
      F.BoundLinesStyle := TPCLLabel(AComponent).BoundLinesStyle;
      StorePrintObject(F, APage);
    finally
      F.Free;
    end;
  end;
end;


procedure CompressPage(APage: TrwVirtualPage);
var
  i: Integer;
begin
  APage.FData.Size := 0;
  for i := 0 to APage.ComponentCount - 1 do
    StorePrintObject(APage.Components[i], APage);

  APage.DestroyComponents;
end;



  {TrwVirtualPage}

constructor TrwVirtualPage.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FCanvas := Canvas;

  FRepository := TrwPrintRepository.Create(nil);
  FHeader := TrwPrintHeader.Create(nil);
  FMarkTextWnd := nil;

  FData := TIsMemoryStream.Create;

  Color := clWhite;
  BevelOuter := bvNone;
  Width := 0;
  Height := 0;
  Duplexing := False;
  FKf_ZoomX := 1;
  FKf_ZoomY := 1;
  FTray := '';
  FOutBin := 0;
end;


destructor TrwVirtualPage.Destroy;
begin
  HideMarkText;
  FRepository.Free;
  FHeader.Free;
  FData.Free;
  inherited;
end;


procedure TrwVirtualPage.ChangeZoom(kX, kY: Extended);
begin
  FKf_ZoomX := kX;
  FKf_ZoomY := kY;
end;


function TrwVirtualPage.CreateMark(ALeft, ATop, AWidth, AHeight: Word; AText: string; APageNum: Integer = 0): TrwPrintMark;
begin
  Result := TrwPrintMark(Header.Marks.Add);
  Result.Top := ATop;
  Result.Left := ALeft;
  Result.Width := AWidth;
  Result.Height := AHeight;
  Result.Text := AText;

  if APageNum = 0 then
    Result.PageNum := PageNum
  else
    Result.PageNum := APageNum;
end;

procedure TrwVirtualPage.ReadState(Reader: TReader);
begin
  FDuplexing := False;
  FReadBounds := Rect(0,0,0,0);
  inherited;
end;


procedure TrwVirtualPage.DefinePrintMetric;
begin
  if rwPrinter is TrwDummyPrinter then
  begin
    FPrinterOffSets.X := 100;
    FPrinterOffSets.Y := 150;
    FPrnResX := 600;
    FPrnResY := 600;
    FKf_ZoomX := 1;
    FKf_ZoomY := 1;
  end

  else
  begin
    if not rwPrinter.Printing and (rwPrinter.Orientation <> TrwPrinterOrientation(Ord(PageOrientation))) then
      rwPrinter.Orientation := TrwPrinterOrientation(Ord(PageOrientation));

    FPrinterOffSets.X := GetDeviceCaps(rwPrinter.Handle, PHYSICALOFFSETX);
    FPrinterOffSets.Y := GetDeviceCaps(rwPrinter.Handle, PHYSICALOFFSETY);
    FPrnResX := GetDeviceCaps(rwPrinter.Handle, LOGPIXELSX);
    FPrnResY := GetDeviceCaps(rwPrinter.Handle, LOGPIXELSY);
    FKf_ZoomX := 1;
    FKf_ZoomY := 1;
  end;
end;


procedure TrwVirtualPage.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('PageData', ReadPageData, WritePageData, True);
end;


procedure TrwVirtualPage.ReadPageData(Reader: TReader);
begin
  FData.Size := Reader.ReadInteger;
  Reader.Read(TMemoryStream(FData).Memory^, FData.Size);
end;

procedure TrwVirtualPage.WritePageData(Writer: TWriter);
begin
  Writer.WriteInteger(FData.Size);
  Writer.Write(TMemoryStream(FData).Memory^, FData.Size)
end;

procedure TrwVirtualPage.Paint;
var
  n: Word;
  p: Integer;
  C: TrwPrintObject;
  i: Integer;
  R: TRect;
  Rgn: HRGN;
  SavedDC: Integer;
begin
  SavedDC := SaveDC(FCanvas.Handle);

  Rgn := CreateRectRgn(0, 0, Width, Height);
  SelectClipRgn(FCanvas.Handle, Rgn);

  FScaleFonts := True;
  FData.Position := 0;

  while FData.Position < FData.Size do
  begin
    FData.ReadBuffer(n, SizeOf(n));
    p := FData.Position;
    C := TrwPrintObject(Repository.Components[n]);
    C.UsedOnCurrentPaper := True;
    C.PrepareToPaint(FData);
    R := ScaleRect(Rect(C.Left, C.Top, C.Right, C.Bottom));
    if C.Visible and RectVisible(FCanvas.Handle, R) then
    begin
      FData.Position := p;
      C.Paint(FCanvas, Self);
    end;

    if FData.Position <= p then
      break;
  end;

  ReleaseImageHandlers;

//marks
  for i := 0 to FHeader.Marks.Count -1 do
    if FHeader.Marks[i].PageNum = PageNum then
      with FHeader.Marks[i] do
      begin
        R := Rect(Left, Top, Left + Width, Top + Height);
        R := ScaleRect(R);
        if RectVisible(FCanvas.Handle, R) then
          DrawMark(FHeader.Marks[i], FCanvas);
      end;

  RestoreDC(FCanvas.Handle, SavedDC);
  DeleteObject(Rgn);
end;


function TrwVirtualPage.ScaleRect(ARect: TRect): TRect;
begin
  Result.Left   := Round(ARect.Left * Kf_ZoomX);
  Result.Top    := Round(ARect.Top * Kf_ZoomY);
  Result.Right  := Round(ARect.Right * Kf_ZoomX);
  Result.Bottom := Round(ARect.Bottom * Kf_ZoomY);
end;


procedure TrwVirtualPage.PreparePage(const APageNum: Integer; AData: TStream);
var
  n, r: Cardinal;
  i: Integer;
  lrs: TrwPrintableLayers;
  C: TrwPrintObject;
  P: ^Byte;
begin
  n := Cardinal(Header.PagesList[APageNum - 1]);

  lrs := [];
  r := 0;
  for i := 0 to Header.ReposList.Count - 1 do
    if n < Cardinal(Header.ReposList[i]) then
    begin
      r := Cardinal(Header.ReposList[i]);
      if Header.LayersListNotEmpty and (Header.LayersList.Count = Header.ReposList.Count) then
      begin
        P := @lrs;
        P^ := LoByte(LoWord(Integer(Header.LayersList[i])));
      end;
      lrs := lrs + rwPrinter.PrintJobInfo.Layers;

      if Header.TraysListNotEmpty and (Header.TraysList.Count = Header.ReposList.Count) then
        FTray := Header.TraysList[i];

      if Header.OutBinsListNotEmpty and (Header.OutBinsList.Count = Header.ReposList.Count) then
        FOutBin := Integer(Header.OutBinsList[i]);

      break;
    end;

  if Cardinal(Repository.Tag) <> r then
  begin
    Repository.Tag := 0;
    Repository.DestroyComponents;
    AData.Position := r;
    AData.ReadComponent(FRepository);
    Repository.Tag := r;

    if lrs <> [] then
    begin
      lrs := lrs + [0];
      for i := 0 to Repository.ComponentCount - 1 do
      begin
        C := TrwPrintObject(Repository.Components[i]);
        C.Visible := (C.LayerNumber in lrs);
      end;
    end;

    OnChangeRepository;
  end;

  for i := 0 to Repository.ComponentCount - 1 do
    TrwPrintObject(Repository.Components[i]).UsedOnCurrentPaper := False;
  FUnusedImagesReleased := False;    

  AData.Position := n;

  AData.ReadComponent(Self);
  FPageNum := APageNum;
end;

procedure TrwVirtualPage.DrawMark(AMark: TrwPrintMark; ACanvas: TCanvas);
var
  X, Y, W, H: Integer;
  Rect: TRect;
begin
  Rect := Classes.Rect(AMark.Left, AMark.Top, AMark.Left + AMark.Width, AMark.Top + AMark.Height);
  Rect := ScaleRect(Rect);

  with ACanvas do
  begin
    Pen.Color := clFuchsia;
    Brush.Color := clNone;
    Brush.Style := bsClear;
    Pen.Width := Round(1 * Kf_ZoomY);
    Pen.Style := psSolid;
    Pen.Mode := pmCopy;

    X := Rect.Left + Pen.Width div 2;
    Y := Rect.Top + Pen.Width div 2;

    W := (Rect.Right - Rect.Left) - Pen.Width + 1;
    H := (Rect.Bottom - Rect.Top) - Pen.Width + 1;
    if Pen.Width = 0 then
    begin
      Dec(W);
      Dec(H);
    end;

    Ellipse(X, Y, X + W, Y + H);
  end;
end;

function TrwVirtualPage.MarkAtPos(X, Y: Word): TrwPrintMark;
var
  i: Integer;
  M: TrwPrintMark;
  P: TPoint;
begin
  Result := nil;
  P := Point(X, Y);

  for i := 0 to FHeader.Marks.Count -1 do
  begin
    M := FHeader.Marks[i];
    if (FHeader.Marks[i].PageNum = PageNum) and
       PtInRect(Rect(M.Left, M.Top, M.Left + M.Width, M.Top + M.Height), P)  then
    begin
      Result := M;
      break;
    end;
  end;
end;


procedure TrwVirtualPage.HideMarkText;
begin
  if Assigned(FMarkTextWnd) then
  begin
    FMarkTextWnd.Free;
    FMarkTextWnd := nil;
  end;
end;

procedure TrwVirtualPage.ShowMarkText(AMark: TrwPrintMark; X, Y: Word);
begin
  if not Assigned(FMarkTextWnd) then
    FMarkTextWnd := TrwMarkTextWindow.Create(Self);
  FMarkTextWnd.ShowMarkText(AMark, Point(X, Y));
end;


procedure TrwVirtualPage.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  M: TrwPrintMark;
  P: TPoint;
begin
  inherited;

  M := MarkAtPos(Word(Round(X/Kf_ZoomX)), Word(Round(Y/Kf_ZoomY)));
  if not Assigned(M) or (Length(M.Text) = 0) then
  begin
    HideMarkText;
    Exit;
  end;
  P := ClientToScreen(Point(X, Y));
  ShowMarkText(M, P.X, P.Y);
end;


procedure TrwVirtualPage.WMPaint(var Message: TWMPaint);
var
  DC: HDC;
  PS: TPaintStruct;
  MemDC: HDC;
  MemBitmap, OldBitmap: HBITMAP;
begin
  if not FDoubleBuffered or (Message. DC <> 0) then
  begin
    DC := Message.DC;
    if DC = 0 then
      DC := BeginPaint(Handle, PS);
    try
      PaintWindow(DC);
    finally
      if Message.DC = 0 then
        EndPaint(Handle, PS);
    end;
  end

  else
  begin
    DC := GetDC(0);
    MemBitmap := CreateCompatibleBitmap(DC, ClientRect.Right, ClientRect.Bottom);
    ReleaseDC(0, DC);
    MemDC := CreateCompatibleDC(0);
    OldBitmap := SelectObject(MemDC, MemBitmap);
    try
      DC := BeginPaint(Handle, PS);
      Perform(WM_ERASEBKGND, Integer(MemDC), Integer(MemDC));
      Message.DC := MemDC;
      WMPaint(Message);
      Message.DC := 0;
      BitBlt(DC, 0, 0, ClientRect.Right, ClientRect.Bottom, MemDC, 0, 0, SRCCOPY);
      EndPaint(Handle, PS);
    finally
      SelectObject(MemDC, OldBitmap);
      DeleteDC(MemDC);
      DeleteObject(MemBitmap);
    end;
  end;
end;



procedure TrwVirtualPage.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  if csLoading in ComponentState then
  begin
    if (FReadBounds.Bottom = 0) or (AHeight <> Height) then
      FReadBounds.Bottom := AHeight;
    if (FReadBounds.Right = 0) or (AWidth <> Width) then
      FReadBounds.Right := AWidth;
    FReadBounds.Left := 0;
    FReadBounds.Top := 0;
  end
  else
    inherited;
end;


procedure TrwVirtualPage.Loaded;
begin
  inherited;
  ScaleSize;
end;


procedure TrwVirtualPage.ScaleSize;
begin
  BoundsRect := FReadBounds;
end;


procedure TrwVirtualPage.Initialize(AStream: TStream);
var
  n: Cardinal;
begin
  AStream.Position := AStream.Size - SizeOf(n);
  AStream.ReadBuffer(n, SizeOf(n));
  AStream.Position := n;
  AStream.ReadComponent(Header);
  AStream.Position := 0;
end;

procedure TrwVirtualPage.WriteHeaderInfo(AStream: TStream);
var
  n:  Cardinal;
begin
  n := AStream.Position;
  AStream.WriteComponent(Header);
  AStream.WriteBuffer(n, SizeOf(n));
end;


procedure TrwVirtualPage.OnChangeRepository;
begin
end;


function TrwVirtualPage.GetTray: String;
begin
  Result := FTray;
end;


function TrwVirtualPage.GetPixelsPerInch: Integer;
begin
  Result := Repository.PixelsPerInch;
  if Result = 0 then
    Result :=  Header.PixelsPerInch;
end;


procedure TrwVirtualPage.ReleaseImageHandlers;
var
  i: Integer;
begin
  if not FUnusedImagesReleased then
  begin
    for i := 0 to Repository.ComponentCount - 1 do
      if (Repository.Components[i] is TrwPrintImage) and not TrwPrintImage(Repository.Components[i]).UsedOnCurrentPaper then
        TrwPrintImage(Repository.Components[i]).Picture.DestroyPictureHandler;

    FUnusedImagesReleased := True;
  end;  
end;

{ TrwPrintRepository }

procedure TrwPrintRepository.GetChildren(Proc: TGetChildProc; Root: TComponent);
var
  i: Integer;
begin
  for i := 0 to ComponentCount - 1 do
    Proc(Components[i]);
end;


{ TrwReportHeader }

constructor TrwPrintHeader.Create(AOwner: TComponent);
begin
  inherited;
  FVersion := 2;
  PixelsPerInch := cScreenCanvasRes;
  FPagesList := TList.Create;
  FReposList := TList.Create;
  FLayersList := TList.Create;
  FTraysList := TStringList.Create;
  FOutBinsList := TList.Create;
  FMarks := TrwPrintMarks.Create(TrwPrintMark);
  FLabelChangesList := TrwLabelChanges.Create;
end;

destructor TrwPrintHeader.Destroy;
begin
  FPagesList.Free;
  FReposList.Free;
  FLayersList.Free;
  FTraysList.Free;
  FOutBinsList.Free;
  FMarks.Free;
  inherited;
end;

procedure TrwPrintHeader.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('ReposList', ReadReposList, WriteReposList, True);
  Filer.DefineProperty('PagesList', ReadPagesList, WritePagesList, True);
  Filer.DefineProperty('LayersList', ReadLayersList, WriteLayersList, LayersListNotEmpty);
  Filer.DefineProperty('TraysList', ReadTraysList, WriteTraysList, TraysListNotEmpty);
  Filer.DefineProperty('OutBinsList', ReadOutBinsList, WriteOutBinsList, OutBinsListNotEmpty);
end;

procedure TrwPrintHeader.ReadPagesList(Reader: TReader);
begin
  FPagesList.Count := Reader.ReadInteger;
  Reader.Read(FPagesList.List^, FPagesList.Count * SizeOf(Pointer));
  FLabelChangesList.InitializeChanges(FPagesList.Count);
end;

procedure TrwPrintHeader.WritePagesList(Writer: TWriter);
begin
  Writer.WriteInteger(FPagesList.Count);
  Writer.Write(FPagesList.List^, FPagesList.Count * SizeOf(Pointer));
end;

procedure TrwPrintHeader.ReadReposList(Reader: TReader);
begin
  FReposList.Count := Reader.ReadInteger;
  Reader.Read(FReposList.List^, FReposList.Count * SizeOf(Pointer));
end;

procedure TrwPrintHeader.WriteReposList(Writer: TWriter);
begin
  Writer.WriteInteger(FReposList.Count);
  Writer.Write(FReposList.List^, FReposList.Count * SizeOf(Pointer));
end;

function TrwPrintHeader.PagesCount: Integer;
begin
  Result := PagesList.Count;
end;


function TrwPrintHeader.LayersListNotEmpty: Boolean;
begin
  Result := FLayersList.Count > 0;
end;

function TrwPrintHeader.TraysListNotEmpty: Boolean;
begin
  Result := FTraysList.Count > 0;
end;

function TrwPrintHeader.OutBinsListNotEmpty: Boolean;
begin
  Result := FOutBinsList.Count > 0;
end;

procedure TrwPrintHeader.ReadLayersList(Reader: TReader);
begin
  FLayersList.Count := Reader.ReadInteger;
  Reader.Read(FLayersList.List^, FLayersList.Count * SizeOf(Pointer));
end;

procedure TrwPrintHeader.WriteLayersList(Writer: TWriter);
begin
  Writer.WriteInteger(FLayersList.Count);
  Writer.Write(FLayersList.List^, FLayersList.Count * SizeOf(Pointer));
end;

procedure TrwPrintHeader.ReadOutBinsList(Reader: TReader);
begin
  FOutBinsList.Count := Reader.ReadInteger;
  Reader.Read(FOutBinsList.List^, FOutBinsList.Count * SizeOf(Pointer));
end;

procedure TrwPrintHeader.WriteOutBinsList(Writer: TWriter);
begin
  Writer.WriteInteger(FOutBinsList.Count);
  Writer.Write(FOutBinsList.List^, FOutBinsList.Count * SizeOf(Pointer));
end;

procedure TrwPrintHeader.ReadTraysList(Reader: TReader);
begin
  FTraysList.CommaText := Reader.ReadString;
end;

procedure TrwPrintHeader.WriteTraysList(Writer: TWriter);
begin
  Writer.WriteString(FTraysList.CommaText);
end;

function TrwPrintHeader.MarksNotEmpty: Boolean;
begin
  Result := FMarks.Count > 0;
end;

procedure TrwPrintHeader.SetMarks(const Value: TrwPrintMarks);
begin
  FMarks.Assign(Value);
end;


{ TrwPrintMark }

procedure TrwPrintMark.Assign(Source: TPersistent);
begin
  FPageNum := TrwPrintMark(Source).PageNum;
  FTop := TrwPrintMark(Source).Top;
  FLeft := TrwPrintMark(Source).Left;
  FHeight := TrwPrintMark(Source).Height;
  FWidth := TrwPrintMark(Source).Width;
  FText := TrwPrintMark(Source).Text;
end;


function TrwPrintMark.GetData: string;
var
  i: Integer;

  procedure WriteDword(DW: Integer);
  begin
    CopyMemory(@(Result[i]), @DW, SizeOf(DW));
    i := i + SizeOf(DW);
  end;

  procedure WriteWord(W: SmallInt);
  begin
    CopyMemory(@(Result[i]), @W, SizeOf(W));
    i := i + SizeOf(W);
  end;


begin
  SetLength(Result, SizeOf(PageNum) + SizeOf(Left) + SizeOf(Top) + SizeOf(Width) +
    SizeOf(Height) + SizeOf(Word) + Length(Text));
  i := 1;

  WriteDword(PageNum);
  WriteWord(Left);
  WriteWord(Top);
  WriteWord(Width);
  WriteWord(Height);
  WriteWord(Length(Text));
  if Length(Text) > 0 then
    CopyMemory(@(Result[i]), @(Text[1]), Length(Text));
end;

procedure TrwPrintMark.SetData(const Value: string);
var
  i: Integer;
  n: Word;

  function ReadDword: Integer;
  begin
    CopyMemory(@Result, @(Value[i]), SizeOf(Result));
    i := i + SizeOf(Result);
  end;

  function ReadWord: SmallInt;
  begin
    CopyMemory(@Result, @(Value[i]), SizeOf(Result));
    i := i + SizeOf(Result);
  end;


begin
  i := 1;
  PageNum := ReadDword;
  Left := ReadWord;
  Top := ReadWord;
  Width := ReadWord;
  Height := ReadWord;
  n := ReadWord;
  SetLength(FText, n);
  if n > 0 then
    CopyMemory(@(Text[1]), @(Value[i]), Length(FText));
end;

{ TrwPrintMarks }

function TrwPrintMarks.GetItem(Index: Integer): TrwPrintMark;
begin
  Result := TrwPrintMark(inherited Items[Index])
end;

procedure TrwPrintMarks.SetItem(Index: Integer; const Value: TrwPrintMark);
begin
  inherited Items[Index] := Value;
end;


{ TrwMarkTextWindow }

procedure TrwMarkTextWindow.ShowMarkText(AMark: TrwPrintMark; AMousePos: TPoint);
var
  R: TRect;
begin
  if FMark = AMark then
  begin
    Left := AMousePos.X - Width div 2;
    Top := AMousePos.Y + 20;
    Exit;
  end;

  FMark := AMark;

  R.Left := 0;
  R.Top := 0;
  DrawText(Canvas.Handle, PChar(FMark.Text), -1, R,
    DT_LEFT or DT_NOPREFIX or DrawTextBiDiModeFlagsReadingOnly or DT_CALCRECT);
  R.Left := AMousePos.X - R.Right div 2;
  R.Top := AMousePos.Y + 20;
  R.Right := R.Right + R.Left + 6;
  R.Bottom := R.Bottom + R.Top + 3;
  ActivateHintData(R, FMark.Text, nil);
end;


{ TrwLabelChanges }

procedure TrwLabelChanges.AddChange(const APageNum, AItemNum: integer; const AChange: String);
var
  ExistingValue: IisNamedValue;
  S: String;
begin
  CheckCondition(FInitialized, 'List of changes is not initialized');
  S := IntToStr(AItemNum);
  ExistingValue := InternalSearch(APageNum, S);
  if ExistingValue <> nil then
    ExistingValue.Value := AChange
  else
    FCurrentPageChanges.AddValue(S, AChange);
end;

function TrwLabelChanges.ChangesCount: integer;
var
  i: integer;
  PageChanges: IisListOfValues;
begin
  Result := 0;
  if FPagesList.Count > 0 then
    for i := 0 to FPagesList.Count - 1 do
    begin
      PageChanges := FPagesList[i] as IisListOfValues;
      Result := Result + PageChanges.Count;
    end;
end;

procedure TrwLabelChanges.Clear;
begin
  FPagesList.Clear;
  FCurrentPageChanges := nil;
  FCurrentPageNum := -1;
  FInitialized := false;
end;

procedure TrwLabelChanges.DeleteChange(const APageNum, AItemNum: integer);
var
  ExistingValue: IisNamedValue;
  S: String;
begin
  CheckCondition(FInitialized, 'List of changes is not initialized');
  S := IntToStr(AItemNum);
  ExistingValue := InternalSearch(APageNum, S);
  if ExistingValue <> nil then
    FCurrentPageChanges.DeleteValue(S)
  else
    raise Exception.Create('Cannot delete item. Item# ' + S + ' not found on page# ' + IntToStr(APageNum));
end;

destructor TrwLabelChanges.Destroy;
begin
  inherited;
end;

procedure TrwLabelChanges.DoOnConstruction;
begin
  inherited;
  FPagesList := TisInterfaceList.Create;
  Clear;
end;

function TrwLabelChanges.FindChange(const APageNum, AItemNum: integer): String;
var
  ExistingValue: IisNamedValue;
  S: String;
begin
  Result := '';
  CheckCondition(FInitialized, 'List of changes is not initialized');
  S := IntToStr(AItemNum);
  ExistingValue := InternalSearch(APageNum, S);
  if ExistingValue <> nil then
    Result := ExistingValue.Value;
end;

class function TrwLabelChanges.GetTypeID: String;
begin
  Result := 'TrwLabelChanges';
end;

procedure TrwLabelChanges.InitializeChanges(const APagesCount: integer);
var
  PageChangesList: IisListOfValues;
  i: integer;
begin
  Clear;
  for i := 1 to APagesCount do
  begin
    PageChangesList := TisListOfValues.Create;
    FPagesList.Add(PageChangesList);
  end;
  FInitialized := true;
end;

function TrwLabelChanges.InternalSearch(const APageNum: integer; const AItemNum: String): IisNamedValue;
begin
  if APageNum <> FCurrentPageNum then
  begin
    FCurrentPageChanges := FPagesList[APageNum - 1] as IisListOfValues;
    CheckCondition(FCurrentPageChanges <> nil, 'Page #' + IntToStr(APageNum) + ' does not exist');
    FCurrentPageNum := APageNum;
  end;
  Result := FCurrentPageChanges.FindValue(AItemNum);
end;

procedure TrwLabelChanges.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  Clear;
  (FPagesList as IisInterfacedObject).ReadFromStream(AStream);
  FInitialized := true;
end;

procedure TrwLabelChanges.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  (FPagesList as IisInterfacedObject).WriteToStream(AStream);
end;

initialization
  RegisterClasses([TExtShape, TFrameLabel, TGraphicFrame, TFrameImage, TPCLLabel,
                   TrwPrintHeader, TrwPrintRepository, TrwReportArchiveHeader,
                   TrwPrintText, TrwPrintFrame, TrwPrintLine, TrwPrintShape,
                   TrwPrintImage, TrwPrintPCLText, TrwPrintMark, TrwPrintMarks,
                   TrwPrintBarcode1D, TrwPrintBarcodePDF417]);

  ObjectFactory.Register([TrwLabelChanges]);

finalization
  ObjectFactory.Unregister([TrwLabelChanges]);

end.
