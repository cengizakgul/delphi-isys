object rwRWASecurity: TrwRWASecurity
  Left = 566
  Top = 433
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Report Security'
  ClientHeight = 195
  ClientWidth = 354
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object rgPasswd: TGroupBox
    Left = 8
    Top = 59
    Width = 335
    Height = 84
    Caption = 'Specify Password'
    TabOrder = 1
    object evLabel1: TLabel
      Left = 9
      Top = 54
      Width = 111
      Height = 13
      Caption = 'Preview Only Password'
    end
    object evLabel2: TLabel
      Left = 9
      Top = 23
      Width = 103
      Height = 13
      Caption = 'Full Access Password'
    end
    object edtFullAccess: TEdit
      Left = 136
      Top = 19
      Width = 186
      Height = 21
      MaxLength = 20
      PasswordChar = '*'
      TabOrder = 0
    end
    object edtPreviewAccess: TEdit
      Left = 136
      Top = 51
      Width = 186
      Height = 21
      MaxLength = 20
      PasswordChar = '*'
      TabOrder = 1
    end
  end
  object chbCompression: TCheckBox
    Left = 8
    Top = 155
    Width = 84
    Height = 17
    Caption = 'Compression'
    TabOrder = 2
  end
  object btnOK: TButton
    Left = 268
    Top = 165
    Width = 75
    Height = 22
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 4
  end
  object btnCancel: TButton
    Left = 181
    Top = 165
    Width = 75
    Height = 22
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object rgLevel: TRadioGroup
    Left = 8
    Top = 8
    Width = 335
    Height = 41
    Caption = 'Security Level'
    Columns = 3
    Items.Strings = (
      'Unsecured'
      'One password'
      'Two passwords')
    TabOrder = 0
    OnClick = rgLevelClick
  end
end
