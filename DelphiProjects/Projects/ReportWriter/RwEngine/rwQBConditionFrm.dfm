object rwQBCondition: TrwQBCondition
  Left = 471
  Top = 351
  Width = 467
  Height = 318
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'Condition Constructor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    459
    291)
  PixelsPerInch = 96
  TextHeight = 13
  object btnOK: TButton
    Left = 312
    Top = 264
    Width = 65
    Height = 23
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 4
  end
  object btnCancel: TButton
    Left = 389
    Top = 264
    Width = 65
    Height = 23
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 5
  end
  object GroupBox2: TGroupBox
    Left = 6
    Top = 96
    Width = 448
    Height = 161
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Right Part'
    TabOrder = 2
    object pcOper: TPageControl
      Left = 2
      Top = 15
      Width = 444
      Height = 144
      ActivePage = tsBetween
      Align = alClient
      Style = tsButtons
      TabOrder = 0
      object tsExpression: TTabSheet
        Caption = 'Expression'
        TabVisible = False
        OnShow = tsExpressionShow
        DesignSize = (
          436
          134)
        inline exprRight1: TrwQBExpress
          Left = 2
          Top = -1
          Width = 432
          Height = 132
          Anchors = [akLeft, akTop, akRight, akBottom]
          AutoScroll = False
          TabOrder = 0
          inherited pnlExpr: TrwQBExprPanel
            Width = 432
            Height = 117
            PopupMenu = exprRight1.pnlExpr.pmClipbrd
            inherited pnlCanvas: TPanel
              Width = 432
              Height = 117
            end
          end
        end
      end
      object tsBetween: TTabSheet
        Caption = 'Between'
        ImageIndex = 1
        TabVisible = False
        OnResize = tsBetweenResize
        OnShow = tsBetweenShow
        DesignSize = (
          436
          134)
        object lAnd: TLabel
          Left = 193
          Top = 57
          Width = 36
          Height = 24
          Alignment = taCenter
          AutoSize = False
          Caption = 'AND'
          Layout = tlCenter
        end
        inline exprBtw1: TrwQBExpress
          Left = 2
          Top = -2
          Width = 193
          Height = 134
          Anchors = [akLeft, akTop, akBottom]
          AutoScroll = False
          TabOrder = 0
          inherited lExpr: TLabel
            Width = 78
            Caption = 'Initial Expression'
          end
          inherited pnlExpr: TrwQBExprPanel
            Width = 193
            Height = 119
            PopupMenu = exprBtw1.pnlExpr.pmClipbrd
            inherited pnlCanvas: TPanel
              Width = 193
              Height = 119
              Anchors = [akLeft, akTop, akBottom]
            end
          end
        end
        inline exprBtw2: TrwQBExpress
          Left = 228
          Top = -2
          Width = 207
          Height = 134
          Anchors = [akLeft, akTop, akBottom]
          AutoScroll = False
          TabOrder = 1
          inherited lExpr: TLabel
            Width = 76
            Caption = 'Final Expression'
          end
          inherited pnlExpr: TrwQBExprPanel
            Width = 207
            Height = 119
            PopupMenu = exprBtw2.pnlExpr.pmClipbrd
            inherited pnlCanvas: TPanel
              Width = 207
              Height = 119
              Anchors = [akLeft, akTop, akBottom]
            end
          end
        end
      end
      object tsLike: TTabSheet
        Caption = 'Like'
        ImageIndex = 2
        TabVisible = False
        OnShow = tsLikeShow
        DesignSize = (
          436
          134)
        object gbOperAux: TGroupBox
          Left = 5
          Top = -1
          Width = 135
          Height = 101
          Caption = 'Matching'
          TabOrder = 0
          OnExit = gbOperAuxExit
          object cbLikeCS: TCheckBox
            Left = 10
            Top = 72
            Width = 101
            Height = 17
            Caption = 'Case-sensitive'
            Checked = True
            State = cbChecked
            TabOrder = 2
          end
          object rbLikeBeg: TRadioButton
            Left = 10
            Top = 18
            Width = 115
            Height = 17
            Caption = 'Partial at Beginning'
            Checked = True
            TabOrder = 0
            TabStop = True
          end
          object rbLikeAny: TRadioButton
            Left = 10
            Top = 42
            Width = 115
            Height = 17
            Caption = 'Partial Anywhere'
            TabOrder = 1
          end
        end
        inline exprRight2: TrwQBExpress
          Left = 148
          Top = 2
          Width = 285
          Height = 97
          Anchors = [akLeft, akTop, akRight]
          AutoScroll = False
          TabOrder = 1
          inherited lExpr: TLabel
            Width = 81
            Caption = 'String Expression'
          end
          inherited pnlExpr: TrwQBExprPanel
            Width = 285
            Height = 82
            PopupMenu = exprRight2.pnlExpr.pmClipbrd
            inherited pnlCanvas: TPanel
              Width = 285
              Height = 82
            end
          end
        end
      end
      object tsINCustom: TTabSheet
        Caption = 'INCustom'
        ImageIndex = 3
        TabVisible = False
        OnShow = tsINCustomShow
        DesignSize = (
          436
          134)
        object Label2: TLabel
          Left = 2
          Top = -2
          Width = 62
          Height = 13
          Caption = 'List of values'
        end
        object lbList: TListBox
          Left = 2
          Top = 13
          Width = 344
          Height = 118
          Anchors = [akLeft, akTop, akRight, akBottom]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 16
          ParentFont = False
          TabOrder = 0
          OnDblClick = Button3Click
        end
        object Button1: TButton
          Left = 355
          Top = 13
          Width = 76
          Height = 23
          Anchors = [akTop, akRight]
          Caption = 'Add Item'
          TabOrder = 1
          OnClick = Button1Click
        end
        object Button2: TButton
          Left = 355
          Top = 71
          Width = 76
          Height = 23
          Anchors = [akTop, akRight]
          Caption = 'Delete Item'
          TabOrder = 3
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 355
          Top = 42
          Width = 76
          Height = 23
          Anchors = [akTop, akRight]
          Caption = 'Edit Item'
          TabOrder = 2
          OnClick = Button3Click
        end
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 263
    Top = 3
    Width = 190
    Height = 89
    Anchors = [akTop, akRight]
    Caption = 'Compare Operation'
    TabOrder = 1
    object cbOper: TComboBox
      Left = 12
      Top = 21
      Width = 166
      Height = 21
      Style = csDropDownList
      DropDownCount = 11
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbOperChange
      Items.Strings = (
        '='
        '<>'
        '>'
        '<'
        '>='
        '<='
        'LIKE'
        'BETWEEN'
        'IN (Custom values)'
        'IN (Lookup values)'
        'IS NULL')
    end
    object chbNot: TCheckBox
      Left = 12
      Top = 56
      Width = 175
      Height = 17
      Caption = 'Inverse compare result (NOT)'
      TabOrder = 1
      OnClick = chbNotClick
    end
  end
  object GroupBox3: TGroupBox
    Left = 6
    Top = 3
    Width = 249
    Height = 90
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Left Part'
    TabOrder = 0
    DesignSize = (
      249
      90)
    inline exprLeft: TrwQBExpress
      Left = 8
      Top = 14
      Width = 232
      Height = 67
      Anchors = [akLeft, akTop, akRight, akBottom]
      AutoScroll = False
      TabOrder = 0
      inherited pnlExpr: TrwQBExprPanel
        Width = 232
        Height = 52
        PopupMenu = exprLeft.pnlExpr.pmClipbrd
        inherited pnlCanvas: TPanel
          Width = 232
          Height = 52
          OnClick = pnlExprpnlCanvasClick2
        end
      end
    end
    object pnlSUBQuery: TPanel
      Left = 9
      Top = 19
      Width = 234
      Height = 25
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      DesignSize = (
        234
        25)
      object Label1: TLabel
        Left = 0
        Top = 4
        Width = 50
        Height = 13
        Caption = 'SUBQuery'
      end
      object cbSUBQuery: TComboBox
        Left = 60
        Top = 2
        Width = 173
        Height = 21
        Style = csDropDownList
        Anchors = [akLeft, akTop, akRight]
        DropDownCount = 11
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbSUBQueryChange
      end
    end
  end
  object btnCheck: TButton
    Left = 6
    Top = 264
    Width = 65
    Height = 23
    Anchors = [akLeft, akBottom]
    Caption = 'Check'
    TabOrder = 3
    OnClick = btnCheckClick
  end
end
