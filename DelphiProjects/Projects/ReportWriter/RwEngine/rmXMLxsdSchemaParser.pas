unit rmXMLxsdSchemaParser;

interface

uses SysUtils, XMLDoc, XMLIntf, Classes, Variants, MSXML2_TLB, Dialogs, Controls, HTTPApp,
     rmTypes, rmXMLFormControls, rwBasicClasses, rwRTTI, rwBasicUtils, rwUtils, rwLogQuery;

const
  cXMLClassPrefix = 'TrmXML';
  cXMLUserClassPrefix = 'TrmlXML';
  cXMLrwW3CNameSpace = 'http://www.w3.org/2001/XMLSchema';
  cXMLrwNameSpace = 'http://www.isystemsllc.com/ReportWriter';
  cXMLrwTypes = 'RW';


type
  IrmXMLxsdSchemaParser = interface
  ['{7613F6CE-065C-45BD-8F26-8F862D3423ED}']
    procedure ExecuteParsing;
  end;


  TrmXMLxsdSchemaParser = class(TInterfacedObject, IrmXMLxsdSchemaParser)
  private
    FXMLForm: TrmXMLForm;
    FSchemaCache: IXMLDOMSchemaCollection2;
    FSchema: ISchema;
    FFileName: String;
    FNameList: TStringList;
    FTopElementName: String;
    procedure ExecuteParsing;
    procedure ParseSchema(const ASchemaName: String);
    function  ParseSchemaItem(const ASchemaItem: ISchemaItem; const AObjOwner: TrmXMLNode): TrmXMLItem;
    function  ParseComplexType(const ASchemaItem: ISchemaComplexType; const AObjOwner: TrmXMLNode): TrmXMLelement;
    function  ParseSimpleType(const ASchemaItem: ISchemaType; const AObjOwner: TrmXMLNode): TrmXMLSimpleType;
    function  ParseElement(const ASchemaItem: ISchemaElement; const AObjOwner: TrmXMLNode): TrmXMLelement;
    procedure ParseAll(const ASchemaItem: ISchemaModelGroup; const AObjOwner: TrmXMLNode);
    function  ParseChoice(const ASchemaItem: ISchemaModelGroup; const AObjOwner: TrmXMLNode): TrmXMLchoice;
    function  ParseSequence(const ASchemaItem: ISchemaModelGroup; const AObjOwner: TrmXMLNode): TrmXMLsequence;
    function  ParseAttribute(const ASchemaItem: ISchemaAttribute; const AObjOwner: TrmXMLNode): TrmXMLAttribute;
    function  ParseAttributeGroup(const ASchemaItem: ISchemaAttributeGroup; const AObjOwner: TrmXMLNode): TrmXMLattributeGroup;
    function  ParseGroup(const ASchemaItem: ISchemaModelGroup; const AObjOwner: TrmXMLNode): TrmXMLgroup;

    procedure ParseSchemaType(const ASchemaType: ISchemaItem);
    function  RemoveNSPrefix(const AText: String): String;
    function  CreateObjectByClass(const AXMLType, ArwParentClass: String; const AOwner: TrmXMLNode): TrmXMLItem;
    procedure RegisterRwComponent(const rmObject: TrmXMLItem; const ATypeName: String);
    procedure SetObjectOwner(const AChild: TrmXMLItem; const AOwner: TrmXMLNode);
    procedure SetObjName(const AObject: TrmXMLItem; const ANamePattern: String; ATag: Integer);
    function  CheckTypeInheritence(const ASchemaType: ISchemaType; const AObjOwner: TrmXMLNode): TrmXMLItem;
    function  CheckItemInheritence(const ASchemaType: ISchemaType; const AObjOwner: TrmXMLNode): TrmXMLItem;
    function  ParseAnnotation(const ASchemaItem: ISchemaItem): String;
    function  GetRWTag(const ASchemaItem: ISchemaItem): Integer;
    function  FindObjectByTag(const ATag: Integer; AOwner: TrmXMLNode): TrmXMLItem;
  public
    constructor Create(AXMLForm: TrmXMLForm; const AFileName: TFileName);
    destructor  Destroy; override;
  end;


function CreateXMLDoc: IXMLDOMDocument2;

implementation

const cXMLrwTag = 'Tag';

function CreateXMLDoc: IXMLDOMDocument2;
begin
  try
    Result := CoDOMDocument60.Create;
  except
    on E: Exception do
      rwError('Supporting XML schemas requires library MSXML 6.0 or higher' + #13#13 + E.Message);
  end;
end;


{ TrmXMLxsdSchemaParser }

constructor TrmXMLxsdSchemaParser.Create(AXMLForm: TrmXMLForm; const AFileName: TFileName);
begin
  FXMLForm := AXMLForm;
  FFileName := AFileName;
  FNameList := TStringList.Create;
  FNameList.Sorted := True;
end;

destructor TrmXMLxsdSchemaParser.Destroy;
begin
  FreeAndNil(FNameList);
  inherited;
end;

procedure TrmXMLxsdSchemaParser.ExecuteParsing;
var
  SchemaDoc: IXMLDOMDocument2;
  DlgRes: Integer;
  ExistingSchemasCount: Integer;
  TopSchema: TrmXMLSchema;
  lTagID: Integer;

  function PrepareSchema(const ASchemaFileName: String): String;
  var
    N: IXMLDOMNode;
    Ns: IXMLDOMNodeList;
    SchemaDoc: IXMLDOMDocument2;
    SchemaName, SchemaDescription, IncludeFileName: String;
    rwSchema: TrmXMLSchema;
    i: Integer;
    CurDir: String;
    A: IXMLDOMAttribute;

    procedure AddRWIDAttribute(const ANode: IXMLDOMNode);
    var
      i: Integer;
      A: IXMLDOMAttribute;
    begin
      if ANode.nodeType = NODE_ELEMENT then
      begin
        if AnsiSameText(ANode.baseName, 'sequence') or
           AnsiSameText(ANode.baseName, 'choice') or
           AnsiSameText(ANode.baseName, 'group') or
           AnsiSameText(ANode.baseName, 'attributeGroup') then
        begin
          A := SchemaDoc.createAttribute(cXMLrwTypes + ':' + cXMLrwTag);
          ANode.attributes.setNamedItem(A);
          A.value := lTagID;
          Inc(lTagID);
        end;

        if ANode.hasChildNodes then
          for i := 0 to ANode.childNodes.length - 1 do
            AddRWIDAttribute(ANode.childNodes[i]);
      end;
    end;

  begin
    SchemaDoc := CreateXMLDoc;
    SchemaDoc.async := False;
    SchemaDoc.load(ASchemaFileName);

    A := SchemaDoc.createAttribute('xmlns:' + cXMLrwTypes);
    A.Value := cXMLrwNameSpace;
    SchemaDoc.DocumentElement.attributes.setNamedItem(A);

    AddRWIDAttribute(SchemaDoc.DocumentElement);

    SchemaDoc.setProperty('SelectionNamespaces', 'xmlns:xsd="' + cXMLrwW3CNameSpace + '"');

    N := SchemaDoc.DocumentElement.selectSingleNode('./xsd:annotation/xsd:documentation');
    if Assigned(N) then
      SchemaDescription := N.text;

    N := SchemaDoc.DocumentElement.selectSingleNode('./xsd:element');
    if Assigned(N) then
    begin
      N := N.attributes.getNamedItem('name');
      if Assigned(N) then
        Result := N.text;
    end
    else
      Result := '';

    Ns := SchemaDoc.DocumentElement.selectNodes('./xsd:include/@schemaLocation');
    for i := 0 to Ns.length - 1 do
    begin
      N := Ns.item[i];

      IncludeFileName := UnixPathToDosPath(VarToStr(N.nodeValue));

      CurDir := GetCurrentDir;
      SetCurrentDir(ExtractFilePath(ExpandFileName(ASchemaFileName)));

      SchemaName := ExtractFileName(IncludeFileName);
      PrepareSchema(IncludeFileName);

      SetCurrentDir(CurDir);

      if DlgRes = mrCancel then
        Exit;

      N.nodeValue := SchemaName;
    end;

    Ns := SchemaDoc.DocumentElement.selectNodes('./xsd:import/@schemaLocation');
    for i := 0 to Ns.length - 1 do
    begin
      N := Ns.item[i];

      IncludeFileName := UnixPathToDosPath(VarToStr(N.nodeValue));

      CurDir := GetCurrentDir;
      SetCurrentDir(ExtractFilePath(ExpandFileName(ASchemaFileName)));

      SchemaName := ExtractFileName(IncludeFileName);
      PrepareSchema(IncludeFileName);

      SetCurrentDir(CurDir);

      if DlgRes = mrCancel then
        Exit;

      N.nodeValue := SchemaName;
    end;


    SchemaName := ChangeFileExt(ExtractFileName(ASchemaFileName), '');

    rwSchema := FXMLForm.Schemas.FindSchema(SchemaName);
    if Assigned(rwSchema) then
    begin
      if rwSchema.InheritedItem or (rwSchema.Index >= ExistingSchemasCount) then
        exit;

      DlgRes := MessageDlg('Schema "' + SchemaName + '" is already registered. Do you want to override it?', mtConfirmation, [mbYes, mbNo, mbCancel], 0);
      if DlgRes = mrYes then
        rwSchema.Free
      else
        Exit;
    end;

    rwSchema := FXMLForm.Schemas.Add as TrmXMLSchema;
    rwSchema.Name := SchemaName;
    rwSchema.Description := SchemaDescription;
    IncludeFileName := GetThreadTmpDir + SchemaName;
    SchemaDoc.save(IncludeFileName);
    FSchemaCache.add(VarToStr(SchemaDoc.documentElement.getAttribute('targetNamespace')), IncludeFileName);
    try
      rwSchema.LoadFromFile(IncludeFileName);
    finally
      DeleteFile(IncludeFileName);
    end;
  end;

begin
  DlgRes := mrNone;
  lTagID := 1;

  ExistingSchemasCount := FXMLForm.Schemas.Count;

  FSchemaCache := CoXMLSchemaCache60.Create;

  FTopElementName := PrepareSchema(FFileName);

  if DlgRes = mrCancel then
    Exit;

  FXMLForm.Schemas.BeginWorkWithItems;
  try
    TopSchema := FXMLForm.Schemas[FXMLForm.Schemas.Count - 1];
    SchemaDoc := CreateXMLDoc;
    SchemaDoc.resolveExternals := true;
    SchemaDoc.async := False;
    SchemaDoc.load(TopSchema.SchemaFileName);


    FXMLForm.NameSpace := VarToStr(SchemaDoc.documentElement.getAttribute('targetNamespace'));

    if FXMLForm.NameSpace = '' then
      FXMLForm.NameSpace := 'MySchema';

    if FXMLForm.TopElement <> nil then
      FXMLForm.TopElement.Free;

    ParseSchema(FXMLForm.NameSpace);
  finally
    FXMLForm.Schemas.EndWorkWithItems;
  end;
end;

function TrmXMLxsdSchemaParser.ParseComplexType(const ASchemaItem: ISchemaComplexType; const AObjOwner: TrmXMLNode): TrmXMLelement;
var
  i: Integer;
  Obj: TrmXMLItem;
begin
  Obj := CheckTypeInheritence(ASchemaItem, nil);

  if Obj is TrmXMLelement then
  begin
    Result := Obj as TrmXMLelement;
    SetObjectOwner(Result, AObjOwner);
  end
  else
  begin
    Result := CreateObjectByClass('element', 'TrmXMLelement', AObjOwner) as TrmXMLelement;
    if Assigned(Obj) then
    begin
      SetObjectOwner(Obj, Result);
      SetObjName(Obj, 'ValueHolder', GetRWTag(ASchemaItem));
    end
  end;

  ParseSchemaItem(ASchemaItem.contentModel, Result);

  for i := 0 to ASchemaItem.attributes.length - 1 do
    ParseAttribute(ASchemaItem.attributes[i] as ISchemaAttribute, Result);
end;

procedure TrmXMLxsdSchemaParser.ParseSchema(const ASchemaName: String);
var
  i: Integer;
  Obj: TrmXMLItem;
  TopElement: ISchemaElement;
begin
  FSchema := FSchemaCache.getSchema(ASchemaName);
  try
    FXMLForm.Description := ParseAnnotation(FSchema);

    for i := 0 to FSchema.types.length - 1 do
      ParseSchemaType(FSchema.types[i]);

    for i := 0 to FSchema.attributes.length - 1 do
      ParseSchemaType(FSchema.attributes[i]);

    for i := 0 to FSchema.attributeGroups.length - 1 do
      ParseSchemaType(FSchema.attributeGroups[i]);

//    for i := 0 to FSchema.modelGroups.length - 1 do
//      ParseSchemaType(FSchema.modelGroups[i]);

    if FTopElementName <> '' then
    begin
      TopElement := FSchema.elements.itemByName(FTopElementName) as ISchemaElement;
      Obj := ParseElement(TopElement, FXMLForm);
      SetObjName(Obj, TopElement.name, GetRWTag(TopElement));
      (Obj as IrmDesignObject).SetDesigner(FXMLForm.Designer);
    end

    else
      for i := 0 to FSchema.elements.length - 1 do
      begin
        Obj := ParseElement(FSchema.elements[i] as ISchemaElement, FXMLForm);
        SetObjName(Obj, FSchema.elements[i].name, GetRWTag(FSchema.elements[i]));
        (Obj as IrmDesignObject).SetDesigner(FXMLForm.Designer);
      end;

  finally
    FSchema := nil;
  end;
end;


function TrmXMLxsdSchemaParser.CreateObjectByClass(const AXMLType: String; const ArwParentClass: String; const AOwner: TrmXMLNode): TrmXMLItem;
var
  BaseName: String;
  Cl: IrwClass;
begin
  BaseName := RemoveNSPrefix(AXMLType);

  Cl := rwRTTIInfo.FindClass(cXMLClassPrefix + BaseName);
  if not Assigned(Cl) then
    Cl := rwRTTIInfo.FindClass(cXMLUserClassPrefix + BaseName);

  if not Assigned(Cl) then
    Assert(False);

  if not Assigned(Cl) then
    rwError('Type definition "' + AXMLType + '" is not found');

  if not Cl.DescendantOf(ArwParentClass) then
    rwError('Inappropriate derivation type "' + AXMLType + '"');

  Result := Cl.CreateObjectInstance as TrmXMLItem;
  SetObjectOwner(Result, AOwner);
end;


function TrmXMLxsdSchemaParser.RemoveNSPrefix(const AText: String): String;
var
  i: Integer;
begin
  // temporary. Remove reference on name space
  Result := AText;
  i := Pos(':', Result);
  if i > 0 then
    Delete(Result, 1, i);
end;

function TrmXMLxsdSchemaParser.ParseSchemaItem(const ASchemaItem: ISchemaItem; const AObjOwner: TrmXMLNode): TrmXMLItem;
begin
  Result := nil;

  case ASchemaItem.itemType of
    SOMITEM_SIMPLETYPE:     Result := ParseSimpleType(ASchemaItem as ISchemaType, AObjOwner);
    SOMITEM_COMPLEXTYPE:    Result := ParseComplexType(ASchemaItem as ISchemaComplexType, AObjOwner);
    SOMITEM_ELEMENT:        Result := ParseElement(ASchemaItem as ISchemaElement, AObjOwner);
    SOMITEM_ATTRIBUTE:      Result := ParseAttribute(ASchemaItem as ISchemaAttribute, AObjOwner);
    SOMITEM_ATTRIBUTEGROUP: Result := ParseAttributeGroup(ASchemaItem as ISchemaAttributeGroup, AObjOwner);
    SOMITEM_ALL:            ParseAll(ASchemaItem as ISchemaModelGroup, AObjOwner);
    SOMITEM_CHOICE:         Result := ParseChoice(ASchemaItem as ISchemaModelGroup, AObjOwner);
    SOMITEM_SEQUENCE:       Result := ParseSequence(ASchemaItem as ISchemaModelGroup, AObjOwner);
    SOMITEM_GROUP:          Result := ParseGroup(ASchemaItem as ISchemaModelGroup, AObjOwner);
  end;

  if Assigned(Result) then
    Result.Description := ParseAnnotation(ASchemaItem);
end;

function TrmXMLxsdSchemaParser.ParseAttribute(const ASchemaItem: ISchemaAttribute; const AObjOwner: TrmXMLNode): TrmXMLAttribute;
var
  Obj: TrmXMLItem;
  i: Integer;
begin
  Result := nil;

  if Assigned(AObjOwner) then
  begin
    for i := 0 to AObjOwner.ComponentCount - 1 do
      if (AObjOwner.Components[i] is TrmXMLattribute) and AnsiSameText(TrmXMLattribute(AObjOwner.Components[i]).NSName, ASchemaItem.name) then
        Exit;
  end;

  if ASchemaItem.IsReference then
  begin
    ParseSchemaType(FSchema.attributes.itemByName(ASchemaItem.name));
    Result := CreateObjectByClass(ASchemaItem.name, 'TrmXMLattribute', AObjOwner) as TrmXMLattribute;
    SetObjName(Result, ASchemaItem.name, GetRWTag(ASchemaItem));
  end

  else
  begin
    if (ASchemaItem.type_.itemType >= SOMITEM_DATATYPE) and (ASchemaItem.type_.itemType < SOMITEM_DATATYPE_ANYSIMPLETYPE) then
    begin
      Result := CreateObjectByClass('attribute', 'TrmXMLattribute', AObjOwner) as TrmXMLattribute;
      SetObjName(Result, ASchemaItem.name, GetRWTag(ASchemaItem));

      Obj := CheckItemInheritence(ASchemaItem.type_, Result);
      if not Assigned(Obj) then
        Obj := ParseSimpleType(ASchemaItem.type_, Result);

      SetObjName(Obj, 'ValueHolder', GetRWTag(ASchemaItem));
    end

    else if ASchemaItem.type_.itemType = SOMITEM_SIMPLETYPE then
    begin
      Obj := CheckItemInheritence(ASchemaItem.type_, nil);

      if Obj is TrmXMLattribute then
      begin
        Result := Obj as TrmXMLattribute;
        SetObjectOwner(Result, AObjOwner);
      end
      else
      begin
        Result := CreateObjectByClass('attribute', 'TrmXMLattribute', AObjOwner) as TrmXMLattribute;
        SetObjName(Result, ASchemaItem.name, GetRWTag(ASchemaItem));

        if not Assigned(Obj) then
        begin
          Obj := CheckItemInheritence(ASchemaItem.type_, Result);
          if not Assigned(Obj) then
            Obj := ParseSimpleType(ASchemaItem.type_, Result);
        end
        else
          SetObjectOwner(Obj, Result);

        SetObjName(Obj, 'ValueHolder', GetRWTag(ASchemaItem));
      end;
    end;

    Result.NSName := ASchemaItem.name;
  end;
end;


procedure TrmXMLxsdSchemaParser.ParseAll(const ASchemaItem: ISchemaModelGroup; const AObjOwner: TrmXMLNode);
var
  i: Integer;
begin
  for i := 0 to ASchemaItem.particles.length - 1 do
    ParseSchemaItem(ASchemaItem.particles[i], AObjOwner);
end;


function TrmXMLxsdSchemaParser.ParseElement(const ASchemaItem: ISchemaElement; const AObjOwner: TrmXMLNode): TrmXMLelement;
var
  Obj: TrmXMLSimpleType;
  i: Integer;
  RefSchema: ISchema;
  SchemaItem: ISchemaItem;
begin
  Result := nil;

  if Assigned(AObjOwner) then
  begin
    for i := 0 to AObjOwner.ComponentCount - 1 do
      if (AObjOwner.Components[i] is TrmXMLelement) and AnsiSameText(TrmXMLelement(AObjOwner.Components[i]).NSName, ASchemaItem.name) then
        Exit;
  end;

  if ASchemaItem.IsReference then
  begin
    try
      SchemaItem := FSchema.elements.itemByName(ASchemaItem.name);
    except
      RefSchema := FSchemaCache.getSchema(ASchemaItem.namespaceURI);
      SchemaItem := RefSchema.elements.itemByName(ASchemaItem.name);
    end;

    if SchemaItem <> nil then
      ParseSchemaType(SchemaItem)
    else
      rwError('Cannot find reference item: ' + ASchemaItem.name);

    Result := CreateObjectByClass(ASchemaItem.name, 'TrmXMLelement', AObjOwner) as TrmXMLelement;
    SetObjName(Result, ASchemaItem.name, GetRWTag(ASchemaItem));
  end

  else
  begin
    if (ASchemaItem.type_.itemType >= SOMITEM_DATATYPE) and (ASchemaItem.type_.itemType < SOMITEM_DATATYPE_ANYSIMPLETYPE) then
    begin
      Result := CreateObjectByClass('element', 'TrmXMLelement', AObjOwner) as TrmXMLelement;
      SetObjName(Result, ASchemaItem.name, GetRWTag(ASchemaItem));

      Obj := CheckItemInheritence(ASchemaItem.type_, Result) as TrmXMLSimpleType;
      if not Assigned(Obj) then
        Obj := ParseSimpleType(ASchemaItem.type_, Result);

      SetObjName(Obj, 'ValueHolder', GetRWTag(ASchemaItem));
    end

    else if (ASchemaItem.type_.itemType = SOMITEM_SIMPLETYPE) then
    begin
      Result := CreateObjectByClass('element', 'TrmXMLelement', AObjOwner) as TrmXMLelement;
      SetObjName(Result, ASchemaItem.name, GetRWTag(ASchemaItem));

      Obj := CheckItemInheritence(ASchemaItem.type_, Result) as TrmXMLSimpleType;
      if not Assigned(Obj) then
      begin
        Obj := CheckItemInheritence(ASchemaItem.type_, Result) as TrmXMLSimpleType;
        if not Assigned(Obj) then
          Obj := ParseSimpleType(ASchemaItem.type_, Result);
      end;

      SetObjName(Obj, 'ValueHolder', GetRWTag(ASchemaItem));
    end

    else if (ASchemaItem.type_.itemType = SOMITEM_COMPLEXTYPE) then
    begin
      Result := CheckItemInheritence(ASchemaItem.type_, AObjOwner) as TrmXMLelement;
      if not Assigned(Result) then
        Result := ParseComplexType(ASchemaItem.type_ as ISchemaComplexType, AObjOwner);
      SetObjName(Result, ASchemaItem.name, GetRWTag(ASchemaItem));
    end;

    if Assigned(Result) then
      Result.NSName := ASchemaItem.name;
  end;
end;

procedure TrmXMLxsdSchemaParser.RegisterRwComponent(const rmObject: TrmXMLItem; const ATypeName: String);
var
  LibComp: TrwLocalComponent;
  Cl: IrwClass;
  CompList: TrwLocalComponents;
  lClassName: String;
begin
  lClassName := cXMLUserClassPrefix + ATypeName;

  Cl := rwRTTIInfo.FindClass(lClassName);

  if Assigned(Cl) then
    rwError('Class "' + lClassName + '" is registered already');

  if FXMLForm.LibDesignMode then
    CompList := FXMLForm.GlobalVarFunc.LocalComponents
  else
    CompList := (FXMLForm.Owner as TrwComponent).GlobalVarFunc.LocalComponents;

  LibComp := TrwLocalComponent(CompList.AddLibItem(lClassName, '', ''));
  LibComp.DisplayName := ATypeName;
  LibComp.ParentName := rmObject.PClassName;
  LibComp.Description := rmObject.Description;
  LibComp.Group := 'XML';

  if not Assigned(rmObject.GlobalVarFunc) then
    rmObject.InitGlobalVarFunc;

  LibComp.SaveComp(rmObject);
end;

function TrmXMLxsdSchemaParser.CheckTypeInheritence(const ASchemaType: ISchemaType; const AObjOwner: TrmXMLNode): TrmXMLItem;
var
  Cl: IrwClass;
begin
  Result := nil;

  if ASchemaType.baseTypes.length > 0 then
  begin
    Cl := rwRTTIInfo.FindClass(cXMLClassPrefix + ASchemaType.baseTypes[0].name);
    if Assigned(Cl) then
    begin
      Result := Cl.CreateObjectInstance as TrmXMLItem;
      SetObjectOwner(Result, AObjOwner);
    end
    else
    begin
      ParseSchemaType(FSchema.types.itemByName(ASchemaType.baseTypes[0].name));
      Result := CreateObjectByClass(ASchemaType.baseTypes[0].name, 'TrmXMLItem', AObjOwner) as TrmXMLItem;
    end;
  end

  else
  begin
    Cl := rwRTTIInfo.FindClass(cXMLClassPrefix + ASchemaType.name);
    if Assigned(Cl) then
    begin
      Result := Cl.CreateObjectInstance as TrmXMLItem;
      SetObjectOwner(Result, AObjOwner);
    end;
  end;
end;

procedure TrmXMLxsdSchemaParser.ParseSchemaType(const ASchemaType: ISchemaItem);
var
  Obj: TrmXMLItem;
begin
  if rwRTTIInfo.FindClass(cXMLUserClassPrefix + ASchemaType.name) <> nil then
    Exit;

  Obj := ParseSchemaItem(ASchemaType, nil);
  try
    RegisterRwComponent(Obj, ASchemaType.name);
  finally
    Obj.Free;
  end;
end;

function TrmXMLxsdSchemaParser.CheckItemInheritence(const ASchemaType: ISchemaType; const AObjOwner: TrmXMLNode): TrmXMLItem;
begin
  if ASchemaType.Name <> '' then
  begin
    if rwRTTIInfo.FindClass(cXMLClassPrefix + ASchemaType.name) = nil then
      ParseSchemaType(FSchema.types.itemByName(ASchemaType.name));
    Result := CreateObjectByClass(ASchemaType.name, 'TrmXMLItem', AObjOwner) as TrmXMLItem;
  end
  else
    Result := nil;
end;

function TrmXMLxsdSchemaParser.ParseSimpleType(const ASchemaItem: ISchemaType; const AObjOwner: TrmXMLNode): TrmXMLSimpleType;
var
  Obj: TrmXMLSimpleType;
  i: Integer;
begin
  Result := CheckTypeInheritence(ASchemaItem, nil) as TrmXMLSimpleType;

  if (ASchemaItem.derivedBy = SCHEMADERIVATIONMETHOD_LIST) and not (Result is TrmXMLSimpleTypeByList) then
  begin
    Obj := Result;
    Result := CreateObjectByClass('SimpleTypeByList', 'TrmXMLSimpleTypeByList', AObjOwner) as TrmXMLSimpleType;
    SetObjectOwner(Obj, Result);
    SetObjName(Obj, 'ListItem', GetRWTag(ASchemaItem));
  end

  else if (ASchemaItem.derivedBy = SCHEMADERIVATIONMETHOD_UNION) and not(Result is TrmXMLSimpleTypeByUnion)then
  begin
    FreeAndNil(Result);

    Result := CreateObjectByClass('SimpleTypeByUnion', 'TrmXMLSimpleTypeByUnion', AObjOwner) as TrmXMLSimpleType;
    for i := 0 to ASchemaItem.baseTypes.length - 1 do
    begin
      Obj := CheckItemInheritence(ASchemaItem.baseTypes[i] as ISchemaType, Result) as TrmXMLSimpleType;
      if not Assigned(Obj) then
        Obj := ParseSimpleType(ASchemaItem.baseTypes[i] as ISchemaType, Result);

      SetObjName(Obj, 'UnionItem' + IntToStr(i + 1), GetRWTag(ASchemaItem));
    end;
  end

  else
    SetObjectOwner(Result, AObjOwner);
end;

function TrmXMLxsdSchemaParser.ParseAttributeGroup(const ASchemaItem: ISchemaAttributeGroup; const AObjOwner: TrmXMLNode): TrmXMLattributeGroup;
var
  i: Integer;
begin
  Result := nil;

  if Assigned(AObjOwner) and (FindObjectByTag(GetRWTag(ASchemaItem), AObjOwner) <> nil) then
    Exit;

  // Microsoft doesn't provide inheritance for groups unlike RW

  Result := CreateObjectByClass('attributeGroup', 'TrmXMLattributeGroup', AObjOwner) as TrmXMLattributeGroup;
  SetObjName(Result, ASchemaItem.name, GetRWTag(ASchemaItem));

  for i := 0 to ASchemaItem.attributes.length - 1 do
    ParseSchemaItem(ASchemaItem.attributes[i] as ISchemaAttribute, Result);
end;

function TrmXMLxsdSchemaParser.ParseChoice(const ASchemaItem: ISchemaModelGroup; const AObjOwner: TrmXMLNode): TrmXMLchoice;
var
  i: Integer;
begin
  Result := nil;

  if Assigned(AObjOwner) and (FindObjectByTag(GetRWTag(ASchemaItem), AObjOwner) <> nil) then
    Exit;

  Result := CreateObjectByClass('choice', 'TrmXMLchoice', AObjOwner) as TrmXMLchoice;
  SetObjName(Result, 'Choice', GetRWTag(ASchemaItem));

  for i := 0 to ASchemaItem.particles.length - 1 do
    ParseSchemaItem(ASchemaItem.particles[i], Result);
end;


function TrmXMLxsdSchemaParser.ParseSequence(const ASchemaItem: ISchemaModelGroup; const AObjOwner: TrmXMLNode): TrmXMLsequence;
var
  i: Integer;
  O: TrmXMLNode;
  NoSequnce: Boolean;
begin
  Result := nil;

  NoSequnce := (ASchemaItem.minOccurs = 1) and (ASchemaItem.maxOccurs = 1);

  if Assigned(AObjOwner) then
    Result := FindObjectByTag(GetRWTag(ASchemaItem), AObjOwner) as TrmXMLsequence;

  if not Assigned(Result) then
  begin
    if NoSequnce then
      O := AObjOwner
    else
    begin
      Result := CreateObjectByClass('sequence', 'TrmXMLSequence', AObjOwner) as TrmXMLSequence;
      SetObjName(Result, 'Sequence', GetRWTag(ASchemaItem));
      O := Result;
    end;
  end

  else
    O := Result;

  for i := 0 to ASchemaItem.particles.length - 1 do
    ParseSchemaItem(ASchemaItem.particles[i], O);
end;

function TrmXMLxsdSchemaParser.ParseGroup(const ASchemaItem: ISchemaModelGroup; const AObjOwner: TrmXMLNode): TrmXMLgroup;
var
  i: Integer;
begin
  Result := nil;

  if Assigned(AObjOwner) and (FindObjectByTag(GetRWTag(ASchemaItem), AObjOwner) <> nil) then
    Exit;

  // Microsoft doesn't provide inheritance for groups unlike RW


  Result := CreateObjectByClass('group', 'TrmXMLGroup', AObjOwner) as TrmXMLGroup;
  SetObjName(Result, ASchemaItem.name, GetRWTag(ASchemaItem));

  for i := 0 to ASchemaItem.particles.length - 1 do
    ParseSchemaItem(ASchemaItem.particles[i], Result);
end;

function TrmXMLxsdSchemaParser.ParseAnnotation(const ASchemaItem: ISchemaItem): String;
var
  AnnotationDoc: IXMLDOMDocument2;
  N: IXMLDOMNode;
begin
  Result := '';
  AnnotationDoc := CreateXMLDoc;
  if ASchemaItem.writeAnnotation(AnnotationDoc) then
  begin
    AnnotationDoc.setProperty('SelectionNamespaces', 'xmlns:xsd="' + cXMLrwW3CNameSpace + '"');
    N := AnnotationDoc.selectSingleNode('//xsd:annotation/xsd:documentation');
    if Assigned(N) then
      Result := N.text;
  end;
end;

procedure TrmXMLxsdSchemaParser.SetObjectOwner(const AChild: TrmXMLItem; const AOwner: TrmXMLNode);
begin
  (AChild as IrmDesignObject).SetParent(AOwner);
end;

procedure TrmXMLxsdSchemaParser.SetObjName(const AObject: TrmXMLItem; const ANamePattern: String; ATag: Integer);
var
  i, n: Integer;
  s: String;
begin
  Assert(Assigned(AObject));

  i := FNameList.IndexOf(ANamePattern);
  if i = -1 then
  begin
    FNameList.AddObject(ANamePattern, nil);
    n := 0;
  end
  else
  begin
    n := Integer(FNameList.Objects[i]) + 1;
    FNameList.Objects[i] := Pointer(n);
  end;

  s := ANamePattern;

  if n <> 0 then
    s := s + IntToStr(n);

  AObject.Name := GetUniqueNameForComponent(AObject, False, s, '', True, True);
  AObject.Tag := ATag;
end;


function TrmXMLxsdSchemaParser.GetRWTag(const ASchemaItem: ISchemaItem): Integer;
begin
  if ASchemaItem.unhandledAttributes.length > 0 then
    Result:= StrToInt(ASchemaItem.unhandledAttributes.getValueFromQName(cXMLrwTypes + ':' + cXMLrwTag))
  else
    Result:= 0;
end;

function TrmXMLxsdSchemaParser.FindObjectByTag(const ATag: Integer; AOwner: TrmXMLNode): TrmXMLItem;
var
  i: Integer;
begin
  Result := nil;

  if ATag > 0 then
    for i := 0 to AOwner.ComponentCount - 1 do
      if AOwner.Components[i].Tag = ATag then
      begin
        Result := AOwner.Components[i] as TrmXMLItem;
        break;
      end;
end;

end.
