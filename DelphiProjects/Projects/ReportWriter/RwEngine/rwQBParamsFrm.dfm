object rwQBParams: TrwQBParams
  Left = 440
  Top = 225
  ActiveControl = grParams
  AutoScroll = False
  BorderIcons = [biSystemMenu]
  Caption = 'Query Parameters'
  ClientHeight = 250
  ClientWidth = 502
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    502
    250)
  PixelsPerInch = 96
  TextHeight = 13
  object grParams: TisRWDBGrid
    Left = 0
    Top = 0
    Width = 502
    Height = 217
    DisableThemesInTitle = False
    ControlType.Strings = (
      'Type;CustomEdit;evDBComboBox1;F'
      'Null;CheckBox;1;0')
    Selected.Strings = (
      'Description'#9'32'#9'Parameter'#9'F'#9
      'Type'#9'16'#9'Type'#9#9
      'Value'#9'32'#9'Value'#9#9)
    IniAttributes.Enabled = True
    IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
    IniAttributes.SectionName = 'TrwQBParams\grParams'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 1
    ShowHorzScrollBar = False
    ShowVertScrollBar = False
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = evDataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
    ParentFont = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    PaintOptions.AlternatingRowRegions = [arrDataColumns, arrActiveDataColumn]
    PaintOptions.AlternatingRowColor = clCream
    LocateDlg = False
    FilterDlg = False
    Sorting = False
  end
  object evDBComboBox1: TisRWDBComboBox
    Left = 216
    Top = 40
    Width = 65
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DataField = 'Type'
    DataSource = evDataSource1
    DropDownCount = 8
    ItemHeight = 0
    Items.Strings = (
      'Null'#9'0'
      'Date'#9'1'
      'Integer'#9'2'
      'Float'#9'3'
      'String'#9'4'
      'Array of Strings'#9'5'
      'Array of Numbers'#9'6')
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 3
    UnboundDataType = wwDefault
  end
  object btnOK: TButton
    Left = 355
    Top = 224
    Width = 65
    Height = 22
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    OnClick = btnOKClick
  end
  object btnCancel: TButton
    Left = 432
    Top = 224
    Width = 65
    Height = 22
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object dsParams: TisRWClientDataSet
    IndexFieldNames = 'NAME'
    ControlType.Strings = (
      'Type;CustomEdit;evDBComboBox1;T')
    FieldDefs = <
      item
        Name = 'Name'
        DataType = ftString
        Size = 32
      end
      item
        Name = 'Type'
        DataType = ftInteger
      end
      item
        Name = 'Value'
        DataType = ftString
        Size = 255
      end>
    BeforeDelete = dsParamsBeforeDelete
    OnFilterRecord = dsParamsFilter
    OnNewRecord = dsParamsNewRecord
    Left = 136
    Top = 104
    object dsParamsDescription: TStringField
      DisplayLabel = 'Parameter'
      DisplayWidth = 32
      FieldName = 'Description'
      Size = 64
    end
    object fldType: TIntegerField
      DisplayWidth = 16
      FieldName = 'Type'
      OnChange = fldTypeChange
    end
    object fldValue: TStringField
      DisplayWidth = 32
      FieldName = 'Value'
      Size = 255
    end
    object fldName: TStringField
      DisplayWidth = 32
      FieldName = 'Name'
      Visible = False
      Size = 32
    end
  end
  object evDataSource1: TDataSource
    DataSet = dsParams
    Left = 176
    Top = 104
  end
end
