unit rwLogViewerFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TrwLogViewer = class(TForm)
    memLog: TMemo;
  private
  public
  end;

  procedure ShowLog(AOwner: TComponent; const ALogText, ACaption: String);

implementation

{$R *.dfm}

procedure ShowLog(AOwner: TComponent; const ALogText, ACaption: String);
var
  F: TrwLogViewer;
begin
  F := TrwLogViewer.Create(AOwner);
  with F do
    try
      memLog.Lines.Text := ALogText;
      Caption := ACaption;
      ShowModal;
    finally
      F.Free;
    end;
end;

end.
