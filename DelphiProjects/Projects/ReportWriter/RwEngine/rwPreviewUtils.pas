unit rwPreviewUtils;

interface

uses Dialogs, rwUtils;

procedure PrvInitCachePath;
function  PrvGetCachePath: String;
procedure PrvClearCachePath;

procedure PrvStartWait(AMessage: String; AMaxProgress: Integer = -1);
procedure PrvUpdateWait(AMessage: String; AProgress: Integer = -1);
procedure PrvEndWait;
procedure PrvHideWait;

function  PrvMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons;
                     DefaultButton: TMsgDlgBtn = mbOK; HelpCtx: Integer = 0): Word; overload;
procedure PrvMessage(const Msg: string; HelpCtx: Integer = 0); overload;

function  PrvUnAttended: Boolean;

function  PrvDefaultDialog(const ACaption, APrompt, ADefault: string): string;
function  PrvPasswordDialog(const ACaption: String = ''; const APrompt: String = ''): string;

function  PrvNbrDialog(const ACaption, APrompt: string; var Value: Extended;
                     AIncrement: Extended = 1; AMinVal: Extended = 0; AMaxVal: Extended = 0): Boolean;


implementation

uses
     rwEngine, SysUtils, rwPasswordFrm;

procedure PrvInitCachePath;
begin
end;

function PrvGetCachePath: String;
begin
  Result := GetThreadTmpDir;
end;

procedure PrvClearCachePath;
begin
end;


procedure PrvStartWait(AMessage: String; AMaxProgress: Integer = -1);
begin
  RWEngineExt.AppAdapter.StartEngineProgressBar(AMessage, AMaxProgress);
end;


procedure PrvUpdateWait(AMessage: String; AProgress: Integer = -1);
begin
  RWEngineExt.AppAdapter.UpdateEngineProgressBar(AMessage, AProgress);
end;


procedure PrvEndWait;
begin
  RWEngineExt.AppAdapter.EndEngineProgressBar;
end;


procedure PrvHideWait;
begin
 RWEngineExt.AppAdapter.EndEngineProgressBar;
end;


function PrvMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons;
                     DefaultButton: TMsgDlgBtn = mbOK; HelpCtx: Integer = 0): Word;
begin
  Result := MessageDlg(Msg, DlgType, Buttons, HelpCtx);
end;


procedure PrvMessage(const Msg: string; HelpCtx: Integer = 0); overload;
begin
  MessageDlg(Msg, mtCustom, [mbOK], HelpCtx);
end;


function PrvUnAttended: Boolean;
begin
  Result := False;
end;

function PrvDefaultDialog(const ACaption, APrompt, ADefault: string): string;
begin
  Result := InputBox(ACaption, APrompt, ADefault);
end;


function PrvNbrDialog(const ACaption, APrompt: string; var Value: Extended;
 AIncrement: Extended = 1; AMinVal: Extended = 0; AMaxVal: Extended = 0): Boolean;
var
  s: String;
begin
  s := FloatToStr(Value);
  Result := InputQuery(ACaption, APrompt, s);
  if Result then
    Value := StrToFloat(s);
end;

function PrvPasswordDialog(const ACaption: String = ''; const APrompt: String = ''): string;
begin
  Result := PasswordDialog(ACaption, APrompt);
end;

end.
