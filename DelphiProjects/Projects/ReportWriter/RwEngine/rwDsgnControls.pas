unit rwDsgnControls;

interface

uses Classes, Windows, Messages, SysUtils, Controls, Graphics, Forms, Types, StdCtrls,
     ExtCtrls, Math, rwTransparentPanel, rwPrintElements, rwTypes, rwBasicClasses, rwCommonClasses,
     rwReport, rwReportControls, rwPCLCanvas, rwInputFormControls, rwExtendedControls,
     rwDesignClasses, wwFrame, Dialogs, rmReport, rwUtils, Buttons, Grids,
     rwGraphics, rmCommonClasses, rmTypes, Barcode1D, PDF417;

type

  {TrwNonVisualComponentDesignPanel is design panel for non-visual components}

  TrwNonVisualComponentDesignPanel = class(TPanel, IrwVisualControl)
  private
    FrwObject: TrwNoVisualComponent;
    FImage: TBitmap;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
    procedure Paint; override;
  public
    property Image: TBitmap read FImage;

    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;

  TrwTransparentContainerControl = class(TrwTransparentPanel, IrwVisualControl)
  private
    FrwObject: TrwPrintableContainer;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID); virtual;
    function  Container: TWinControl; virtual;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; virtual;
    procedure Notify(AEvent: TrmDesignEventType); virtual;
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;

  TrwFramedTransparentContainerControl = class(TrwTransparentContainerControl)
  private
    FrwObject: TrwFramedContainer;
  protected
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
  end;


  TrwFramedLabelControl = class(TGraphicControl,IrwVisualControl)
  private
    FrwObject: TrwCustomText;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID); virtual;
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  published
    property PopupMenu;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnDblClick;
  end;


  TrwShapeControl = class(TShape, IrwVisualControl)
  private
    FrwObject: TrwShape;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  published
    property PopupMenu;
    property OnDblClick;
  end;


  TrwFrameControl = class(TGraphicControl, IrwVisualControl)
  private
    FrwObject: TrwFrame;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property PopupMenu;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnDblClick;
  end;


  TrwFramedImageControl = class(TGraphicControl, IrwVisualControl)
  private
    FrwObject: TrwCustomImage;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  published
    property PopupMenu;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnDblClick;
  end;


  TrwPCLImageControl = class(TrwFramedImageControl)
  protected
    procedure Paint; override;
  end;


  TrwPCLLabelControl = class(TrwFramedLabelControl)
  protected
    procedure Paint; override;
  end;


  TrwBarcode1DControl = class(TBarcode1D, IrwVisualControl)
  private
    FrwObject: TrwBarcode1D;
    function  RealObject: TControl;
    function  Container: TWinControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  published
    property PopupMenu;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnDblClick;
  end;

  TrwBarcodePDF417Control = class(TPDF417, IrwVisualControl)
  private
    FrwObject: TrwBarcodePDF417;
    function  RealObject: TControl;
    function  Container: TWinControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  published
    property PopupMenu;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnDblClick;
  end;

  // Input form visual controls

  TrwInpFrmPanel = class(TPanel, IrwVisualControl)
  private
    FrwObject: TrwInputFormContainer;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   SetToDesigning;
  end;


  TrwTextControl = class(TLabel, IrwVisualControl)
  private
    FrwObject: TrwText;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwEditControl = class(TExtEdit, IrwVisualControl)
  private
    FrwObject: TrwEdit;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwCheckBoxControl = class(TExtCheckBox, IrwVisualControl)
  private
    FrwObject: TrwCheckBox;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwDateEditControl = class(TExtDateTimePicker, IrwVisualControl)
  private
    FrwObject: TrwDateEdit;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
    procedure CreateParams(var Params: TCreateParams); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwPanelControl = class(TPanel, IrwVisualControl)
  private
    FrwObject: TrwPanel;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwGroupBoxControl = class(TGroupBox, IrwVisualControl)
  private
    FrwObject: TrwGroupBox;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwRadioGroupControl = class(TExtRadioGroup, IrwVisualControl)
  private
    FrwObject: TrwRadioGroup;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwComboBoxControl = class(TExtComboBox, IrwVisualControl)
  private
    FrwObject: TrwComboBox;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwDBComboBoxControl = class(TExtDBComboBox, IrwVisualControl)
  private
    FrwObject: TrwDBComboBox;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwDBGridControl = class(TExtDBGrid, IrwVisualControl)
  private
    FrwObject: TrwDBGrid;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
    function  IsFiltered: Boolean; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwButtonControl = class(TExtButton, IrwVisualControl)
  private
    FrwObject: TrwButton;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwTabSheetControl = class(TExtTabSheet, IrwVisualControl)
  private
    FrwObject: TrwTabSheet;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwPageCtrlControl = class(TExtPageControl, IrwVisualControl)
  private
    FrwObject: TrwPageControl;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrwFileDialogControl = class(TisRWDBComboDlg, IrwVisualControl)
  private
    FrwObject: TrwFileDialog;
    FDialog:  TOpenDialog;
    FDialogType: TrwFileDialogType;
    function  RealObject: TControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    procedure SetDialogType(const Value: TrwFileDialogType);
    function  Container: TWinControl;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure Notify(AEvent: TrmDesignEventType);
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function    ShowDialog: Boolean;
    property DialogType: TrwFileDialogType read FDialogType write SetDialogType;
  end;


implementation

uses Wwdotdot, ComCtrls, Variants;

function IsSelectable(const AObject: TrwComponent): Boolean;
begin
  Result := (AObject.DsgnLockState = rwLSUnlocked) or
    Assigned((AObject as IrmDesignObject).GetDesigner) and (AObject as IrmDesignObject).GetDesigner.LibComponentDesigninig;
end;

procedure DrawDiagCrossBox(ACanvas: TCanvas; ARect: TRect);
begin
  with ACanvas do
  begin
    Brush.Style := bsDiagCross;
    Brush.Color := clSilver;
    SetBkMode(ACanvas.Handle, Opaque);
    SetBkColor(ACanvas.Handle, ColorToRGB(clWhite));
    FillRect(ARect);
  end;
end;

procedure DrawTextInBox(ACanvas: TCanvas; ARect: TRect; const Text: String);
var
  R: TRect;
  d: Integer;
begin
  with ACanvas do
  begin
    Font.Name := 'Arial';
    Font.Height := -11;
    Font.Style := [fsBold];
    Font.Color := clBlue;
    SetBkMode(ACanvas.Handle, TRANSPARENT);

    R := ARect;
    DrawText(ACanvas.Handle, PAnsiChar(Text), Length(Text), R, DT_CENTER or DT_WORDBREAK or DT_CALCRECT);
    d := ((ARect.Bottom - ARect.Top) - (R.Bottom - R.Top)) div 2;
    R := ARect;
    Inc(R.Top,  d);
    Dec(R.Bottom,  d);

    DrawText(ACanvas.Handle, PChar(Text), Length(Text), R, DT_CENTER or DT_WORDBREAK);
  end;
end;

procedure DrawDiagCrossBoxAndText(ACanvas: TCanvas; ARect: TRect; const Text: String);
begin
  DrawDiagCrossBox(ACanvas, ARect);
  DrawTextInBox(ACanvas, ARect, Text);
end;


{TrwNonVisualComponentDesignPanel}

constructor TrwNonVisualComponentDesignPanel.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwNoVisualComponent;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);

  FImage := TBitmap.Create;
  FImage.Transparent := True;
  Height := 25;
  Width := 25;
  Constraints.MaxHeight := Height;
  Constraints.MaxWidth := Width;
  Constraints.MinHeight := Height;
  Constraints.MinWidth := Width;

  if (FrwObject as IrmDesignObject).GetDesigner <> nil then
    (FrwObject as IrmDesignObject).GetDesigner.GetObjectPicture(FrwObject.PClassName, FImage);
end;


destructor TrwNonVisualComponentDesignPanel.Destroy;
begin
  FrwObject.VisualControl := nil;
  FreeAndNil(FImage);
  inherited;
end;

function TrwNonVisualComponentDesignPanel.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwNonVisualComponentDesignPanel.Paint;
var
  lDelta: Integer;
begin
  inherited;

  with Canvas do
  begin
    lDelta := (Width - FImage.Width) div 2;
    Draw(lDelta, lDelta, FImage);
  end;
end;

procedure TrwNonVisualComponentDesignPanel.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPPosition] then
    SetBounds(FrwObject.Left, FrwObject.Top, Width, Height);

  if APropType = rwCPInvalidate then
    Invalidate;
end;

function TrwNonVisualComponentDesignPanel.RealObject: TControl;
begin
  Result := Self;
end;

procedure TrwNonVisualComponentDesignPanel.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;


function TrwNonVisualComponentDesignPanel.Container: TWinControl;
begin
  Result := nil;
end;

function TrwNonVisualComponentDesignPanel.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := False;
  Result.AcceptedClases := '';
end;

procedure TrwNonVisualComponentDesignPanel.Notify(AEvent: TrmDesignEventType);
begin
end;


{ TrwTransparentContainerControl }

function TrwTransparentContainerControl.Container: TWinControl;
begin
  Result := Self;
end;

constructor TrwTransparentContainerControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwPrintableContainer;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);

  Color := clWhite;
  BevelInner := bvNone;
  BevelOuter := bvNone;
  BorderStyle := bsNone;
end;

function TrwTransparentContainerControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
end;

destructor TrwTransparentContainerControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwTransparentContainerControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwTransparentContainerControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType = rwCPInvalidate then
    Invalidate;

  if APropType in [rwCPAll, rwCPParent] then
    if Assigned(FrwObject.Container) then
      Parent := FrwObject.Container.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPAll, rwCPPosition] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    if (FrwObject as IrmDesignObject).GetDesigner <> nil then
      (FrwObject as IrmDesignObject).GetDesigner.Notify(rmdObjectResized, Integer(Pointer(FrwObject)));
  end;

  if APropType in [rwCPAll, rwCPTransparent] then
    Transparent := FrwObject.Transparent;

  if APropType in [rwCPAll, rwCPColor] then
    Color := FrwObject.Color;

  if APropType in [rwCPAll, rwCPAlign] then
    Align := FrwObject.DesignAlign;
end;

function TrwTransparentContainerControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwTransparentContainerControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;


{TContainer}

constructor TrwFramedTransparentContainerControl.Create(AOwner: TComponent);
begin
  inherited;
  FrwObject := AOwner as TrwFramedContainer;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

procedure TrwFramedTransparentContainerControl.Paint;
begin
  inherited;

  with Canvas do
  begin
    Pen.Mode := pmCopy;
    Pen.Color := clGray;
    Pen.Style := psDot;
    Brush.Color := Color;
    Brush.Style :=bsClear;
    Rectangle(0, 0, Width, Height);
    if FrwObject.CornerText <> '' then
    begin
      Font.Color := clGray;
      Font.Name := 'Arial';
      Font.Size := 7;
      Font.Style := [fsItalic];
      TextOut(2, Height - TextHeight(FrwObject.CornerText) - 2, FrwObject.CornerText);
    end;  
  end;

  DrawFrame(Canvas, FrwObject.BoundLines, FrwObject.BoundLinesColor, FrwObject.BoundLinesWidth, FrwObject.BoundLinesStyle, Width, Height);
end;

procedure TrwTransparentContainerControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwInpFrmPanel }

function TrwInpFrmPanel.Container: TWinControl;
begin
  Result := Self;
end;

constructor TrwInpFrmPanel.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwInputFormContainer;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);

  Name := 'InputFormContainer';
  Caption := '';
  BevelInner := bvNone;
  BevelOuter := bvNone;
  Align := alClient;
end;

function TrwInpFrmPanel.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [];
  Result.ResizingDirections := [];
  Result.Selectable := True;
  Result.SelectorType := rwDBISBNone;
  Result.StandaloneObject := False;
  Result.AcceptControls := True;
  Result.AcceptedClases := '';
end;

destructor TrwInpFrmPanel.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwInpFrmPanel.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwInpFrmPanel.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPCreation, rwCPAll, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    Width := FrwObject.Width;
    Height := FrwObject.Height;
    Color := FrwObject.Color;
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
  end;
end;

function TrwInpFrmPanel.RealObject: TControl;
begin
  Result := Self;
end;

function TrwInpFrmPanel.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwInpFrmPanel.SetToDesigning;
begin
  SetDesigning(True, False);
end;


procedure TrwInpFrmPanel.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{TrwFramedLabelControl}

function TrwFramedLabelControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwFramedLabelControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwCustomText;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);

  ControlStyle := ControlStyle + [csReplicatable];
  ParentColor := False;
  ParentFont := False;
end;

function TrwFramedLabelControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
end;

destructor TrwFramedLabelControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwFramedLabelControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwFramedLabelControl.Paint;
var
  Tinf: TrwTextDrawInfoRec;
begin
  if csOpaque in ControlStyle then
  begin
    Canvas.Brush.Color := FrwObject.Color;
    Canvas.Brush.Style  := bsSolid;
    Canvas.FillRect(Rect(0, 0, Width, Height));
  end;

  Tinf.CalcOnly := False;
  Tinf.ZoomRatio := 1;
  Tinf.Canvas := Canvas;
  Tinf.Text := FrwObject.GetTextForPrint;
  if (Tinf.Text = '') and FrwObject.IsDesignMode then
    Tinf.Text := '    ';
  Tinf.Font := FrwObject.Font;
  if FrwObject.Transparent then
    Tinf.Color := clNone
  else
    Tinf.Color := FrwObject.Color;
  Tinf.DigitNet := FrwObject.DigitNet.Text;
  Tinf.WordWrap := FrwObject.WordWrap;
  Tinf.Alignment := FrwObject.Alignment;
  Tinf.Layout := FrwObject.Layout;
  Tinf.TextRect := FrwObject.ClientRect;

  DrawTextImage(@Tinf);

  DrawFrame(Canvas, FrwObject.BoundLines, FrwObject.BoundLinesColor,
   FrwObject.BoundLinesWidth, FrwObject.BoundLinesStyle, Width, Height);

  if FrwObject.DsgnShowCorners then
    DrawCorners(Canvas, Width, Height)
end;

procedure TrwFramedLabelControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPPosition] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    if (FrwObject as IrmDesignObject).GetDesigner <> nil then
      (FrwObject as IrmDesignObject).GetDesigner.Notify(rmdObjectResized, Integer(Pointer(FrwObject)));
  end;

  if APropType in [rwCPAll, rwCPParent] then
    if Assigned(FrwObject.Container) then
      Parent := FrwObject.Container.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPAll, rwCPColor] then
    Color := FrwObject.Color;

  if APropType in [rwCPAll, rwCPFont] then
    FrwObject.Font.AssignTo(Font);

  if APropType in [rwCPAll, rwCPTransparent] then
  begin
    if FrwObject.Transparent then
      ControlStyle := ControlStyle - [csOpaque]
    else
      ControlStyle := ControlStyle + [csOpaque];

    if APropType = rwCPTransparent then
      Invalidate;
  end;

  if APropType = rwCPInvalidate then
    Invalidate;
end;

function TrwFramedLabelControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwFramedLabelControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwFramedLabelControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwShapeControl }

function TrwShapeControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwShapeControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwShape;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrwShapeControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
end;

destructor TrwShapeControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwShapeControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwShapeControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPPosition] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    if (FrwObject as IrmDesignObject).GetDesigner <> nil then
      (FrwObject as IrmDesignObject).GetDesigner.Notify(rmdObjectResized, Integer(Pointer(FrwObject)));
  end;

  if APropType in [rwCPAll, rwCPParent] then
    if Assigned(FrwObject.Container) then
      Parent := FrwObject.Container.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPAll, rwCPColor] then
    if FrwObject.Transparent then
      Brush.Style := bsClear
    else
    begin
      Brush.Style := bsSolid;
      Brush.Color := FrwObject.Color;
    end;

  if APropType in [rwCPAll, rwCPInvalidate] then
  begin
    Shape := FrwObject.ShapeType;
    Pen.Color := FrwObject.BoundLinesColor;
    Pen.Style := FrwObject.BoundLinesStyle;
    Pen.Width := FrwObject.BoundLinesWidth;
    if FrwObject.Transparent then
      Brush.Style := bsClear
    else
    begin
      Brush.Style := bsSolid;
      Brush.Color := FrwObject.Color;
    end;
  end;
end;

function TrwShapeControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwShapeControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwShapeControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{TrwFrameControl}

function TrwFrameControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwFrameControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwFrame;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);

  ControlStyle := ControlStyle + [csReplicatable];
end;

function TrwFrameControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
end;

destructor TrwFrameControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwFrameControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwFrameControl.Paint;
begin
  Canvas.Brush.Style := bsClear;
  DrawFrame(Canvas, FrwObject.BoundLines, FrwObject.BoundLinesColor, FrwObject.BoundLinesWidth,
    FrwObject.BoundLinesStyle, Width, Height);
  if FrwObject.DsgnShowCorners then
    DrawCorners(Canvas, Width, Height);
end;

procedure TrwFrameControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPPosition] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    if (FrwObject as IrmDesignObject).GetDesigner <> nil then
      (FrwObject as IrmDesignObject).GetDesigner.Notify(rmdObjectResized, Integer(Pointer(FrwObject)));
  end;

  if APropType in [rwCPAll, rwCPParent] then
    if Assigned(FrwObject.Container) then
      Parent := FrwObject.Container.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPInvalidate] then
    Invalidate;
end;

function TrwFrameControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwFrameControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwFrameControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{TrwFramedImageControl}

constructor TrwFramedImageControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwCustomImage;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);

  ControlStyle := ControlStyle + [csReplicatable];
end;

destructor TrwFramedImageControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwFramedImageControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPPosition] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    if (FrwObject as IrmDesignObject).GetDesigner <> nil then
      (FrwObject as IrmDesignObject).GetDesigner.Notify(rmdObjectResized, Integer(Pointer(FrwObject)));
  end;

  if APropType in [rwCPAll, rwCPParent] then
    if Assigned(FrwObject.Container) then
      Parent := FrwObject.Container.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPInvalidate] then
    Invalidate;
end;

function TrwFramedImageControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwFramedImageControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwFramedImageControl.Paint;
var
  R: TRect;
  h: String;

  function DestRect: TRect;
  var
    w, h, cw, ch: Integer;
    xyaspect: Double;
    Proportional: Boolean;
  begin
    Proportional := False;  //Will use some day

    w := FrwObject.Picture.Width;
    h := FrwObject.Picture.Height;
    cw := ClientWidth;
    ch := ClientHeight;
    if FrwObject.Stretch or (Proportional and ((w > cw) or (h > ch))) then
    begin
    if Proportional and (w > 0) and (h > 0) then
    begin
        xyaspect := w / h;
        if w > h then
        begin
          w := cw;
          h := Trunc(cw / xyaspect);
          if h > ch then  // woops, too big
          begin
            h := ch;
            w := Trunc(ch * xyaspect);
          end;
        end
        else
        begin
          h := ch;
          w := Trunc(ch * xyaspect);
          if w > cw then  // woops, too big
          begin
            w := cw;
            h := Trunc(cw / xyaspect);
          end;
        end;
      end
      else
      begin
        w := cw;
        h := ch;
      end;
    end;

    with Result do
    begin
      Left := 0;
      Top := 0;
      Right := w;
      Bottom := h;
    end;

    if True {Center} then
      OffsetRect(Result, (cw - w) div 2, (ch - h) div 2);
  end;

begin
  if not Assigned(FrwObject.Picture.Graphic) or FrwObject.Picture.Graphic.Empty then
  begin
    R :=Rect(0, 0, Width, Height);
    h := FrwObject.ClassName;
    Delete(h, 1 ,3);
    DrawDiagCrossBoxAndText(Canvas, R, h);
    DrawFrame(Canvas, FrwObject.BoundLines, FrwObject.BoundLinesColor, FrwObject.BoundLinesWidth, FrwObject.BoundLinesStyle, Width, Height);
  end

  else
  begin
    if Assigned(FrwObject.Picture.Graphic) then
      FrwObject.Picture.Graphic.Transparent := FrwObject.Transparent;

    if FrwObject.Stretch then
  	  Canvas.StretchDraw(DestRect, FrwObject.Picture.Graphic)
    else
    begin
      R := DestRect;
   	  Canvas.Draw(R.Left, R.Top, FrwObject.Picture.Graphic)
    end;
    R :=Rect(0, 0, Width, Height);
    DrawFrame(Canvas, FrwObject.BoundLines, FrwObject.BoundLinesColor, FrwObject.BoundLinesWidth, FrwObject.BoundLinesStyle,
      R.Right - R.Left, R.Bottom - R.Top, R.Left, R.Top);
  end;

  if FrwObject.DsgnShowCorners then
    DrawCorners(Canvas, Width, Height);
end;

procedure TrwFramedImageControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

function TrwFramedImageControl.Container: TWinControl;
begin
  Result := nil;
end;

function TrwFramedImageControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];

  if FrwObject.Stretch then
    Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom]
  else
    Result.ResizingDirections := [];

  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
end;


procedure TrwFramedImageControl.Notify(AEvent: TrmDesignEventType);
begin
end;

{ TrwPCLImageControl }

procedure TrwPCLImageControl.Paint;
var
  MF: TMetafile;
begin
  if TrwImage(FrwObject).PCLFile.Data <> '' then
  begin
    MF := TrwImage(FrwObject).PreparePCLPicture;
    DrawMetafile(MF, Canvas.Handle, TrwImage(FrwObject).PCLSize.X / MF.Width, Left, Top);

    if FrwObject.DsgnShowCorners then
      DrawCorners(Canvas, Width, Height);
  end
  else
    inherited;
end;


{ TrwPCLLabelControl }

procedure TrwPCLLabelControl.Paint;
var
  MF: TMetafile;
  R: TRect;
  h: String;
begin
  MF := TrwPCLLabel(FrwObject).PrepareImage(nil, Canvas.Handle);

  if (MF.Width = 0) and (MF.Height = 0) then
  begin
    h := FrwObject.ClassName;
    Delete(h, 1 ,3);
    DrawDiagCrossBoxAndText(Canvas, Rect(0, 0, Width, Height), h);
  end
  else
  begin
    R.TopLeft := Point(0, 0);
    R.BottomRight := TrwPCLLabel(FrwObject).PCLSize;
    DrawMetafile(MF, Canvas.Handle, (R.Right - R.Left)/ MF.Width, R.Left, R.Top);
    if FrwObject.DsgnShowCorners then
      DrawCorners(Canvas, Width, Height);
  end;
end;

{ TrwTextControl }

function TrwTextControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwTextControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwText;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);

  AutoSize := False;
  WordWrap := False;
end;

function TrwTextControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := False;
  Result.AcceptedClases := '';
end;

destructor TrwTextControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwTextControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwTextControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    Align := FrwObject.Align;
    Anchors := FrwObject.Anchors;
    Color := FrwObject.Color;
    FrwObject.Font.AssignTo(Font);
    WordWrap := FrwObject.WordWrap;
    Alignment := FrwObject.Alignment;
    Text := FrwObject.Text;
    Layout := FrwObject.Layout;
    Autosize := FrwObject.Autosize;
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
  end;
end;

function TrwTextControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwTextControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwTextControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwEditControl }

function TrwEditControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwEditControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwEdit;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrwEditControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := False;
  Result.AcceptedClases := '';
end;

destructor TrwEditControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwEditControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwEditControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    Anchors := FrwObject.Anchors;
    Color := FrwObject.Color;
    FrwObject.Font.AssignTo(Font);
    Text := FrwObject.Text;
    ReadOnly := FrwObject.ReadOnly;
    TabOrder := FrwObject.TabOrder;
    TabStop := FrwObject.TabStop;
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
  end;
end;

function TrwEditControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwEditControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwEditControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwCheckBoxControl }

function TrwCheckBoxControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwCheckBoxControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwCheckBox;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrwCheckBoxControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := False;
  Result.AcceptedClases := '';
end;

destructor TrwCheckBoxControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwCheckBoxControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwCheckBoxControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    Anchors := FrwObject.Anchors;
    Color := FrwObject.Color;
    FrwObject.Font.AssignTo(Font);
    Text := FrwObject.Text;
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
    AllowGrayed := FrwObject.AllowGrayed;
    State := FrwObject.State;
    TabOrder := FrwObject.TabOrder;
    TabStop := FrwObject.TabStop;
  end;
end;

function TrwCheckBoxControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwCheckBoxControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwCheckBoxControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwDateEditControl }

function TrwDateEditControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwDateEditControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwDateEdit;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

procedure TrwDateEditControl.CreateParams(var Params: TCreateParams);
begin
  inherited;
  if csDesigning in ComponentState then
    Params.WindowClass.Style :=  Params.WindowClass.Style or CS_DBLCLKS;
end;

function TrwDateEditControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := False;
  Result.AcceptedClases := '';
end;

destructor TrwDateEditControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwDateEditControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwDateEditControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    Anchors := FrwObject.Anchors;
    Color := FrwObject.Color;
    FrwObject.Font.AssignTo(Font);
    AllowClearKey := FrwObject.AllowClearKey;
    Date := FrwObject.Date;
    Kind := FrwObject.Kind;
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
    TabOrder := FrwObject.TabOrder;
    TabStop := FrwObject.TabStop;
  end;
end;

function TrwDateEditControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwDateEditControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwDateEditControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwPanelControl }

function TrwPanelControl.Container: TWinControl;
begin
  Result := Self;
end;

constructor TrwPanelControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwPanel;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrwPanelControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := True;
  Result.AcceptedClases := '';
end;

destructor TrwPanelControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwPanelControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwPanelControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    Align := FrwObject.Align;
    Anchors := FrwObject.Anchors;
    Color := FrwObject.Color;
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
    BevelInner := FrwObject.BevelInner;
    BevelOuter := FrwObject.BevelOuter;
    BevelWidth := FrwObject.BevelWidth;
    BorderStyle := FrwObject.BorderStyle;
    BorderWidth := FrwObject.BorderWidth;
    TabOrder := FrwObject.TabOrder;
    TabStop := FrwObject.TabStop;
  end;
end;

function TrwPanelControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwPanelControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwPanelControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwGroupBoxControl }

function TrwGroupBoxControl.Container: TWinControl;
begin
  Result := Self;
end;

constructor TrwGroupBoxControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwGroupBox;
  ParentColor := False;
  ParentFont := False;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrwGroupBoxControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := True;
  Result.AcceptedClases := '';
end;

destructor TrwGroupBoxControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwGroupBoxControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwGroupBoxControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    Align := FrwObject.Align;
    Anchors := FrwObject.Anchors;
    FrwObject.Font.AssignTo(Font);
    Text := FrwObject.Text;
    Color := FrwObject.Color;
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
    TabOrder := FrwObject.TabOrder;
    TabStop := FrwObject.TabStop;
  end;
end;

function TrwGroupBoxControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwGroupBoxControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwGroupBoxControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwRadioGroupControl }

function TrwRadioGroupControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwRadioGroupControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwRadioGroup;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrwRadioGroupControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := False;
  Result.AcceptedClases := '';
end;

destructor TrwRadioGroupControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwRadioGroupControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwRadioGroupControl.PropChanged(APropType: TrwChangedPropID);
var
 i: Integer;
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);

    Items.BeginUpdate;
    try
      Items.Clear;
      for i := 0 to FrwObject.Items.Count-1 do
        if FrwObject.Items.Names[i] = '' then
          Items.Add(FrwObject.Items[i])
        else
          Items.Add(FrwObject.Items.Names[i]);
    finally
      Items.EndUpdate;
    end;

    Anchors := FrwObject.Anchors;
    FrwObject.Font.AssignTo(Font);
    Text := FrwObject.Text;
    Color := FrwObject.Color;
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
    Columns := FrwObject.Columns;
    ItemIndex := FrwObject.ItemIndex;
    TabOrder := FrwObject.TabOrder;
    TabStop := FrwObject.TabStop;
  end;
end;

function TrwRadioGroupControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwRadioGroupControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwRadioGroupControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwComboBoxControl }

function TrwComboBoxControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwComboBoxControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwComboBox;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrwComboBoxControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := False;
  Result.AcceptedClases := '';
end;

destructor TrwComboBoxControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwComboBoxControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwComboBoxControl.PropChanged(APropType: TrwChangedPropID);
var
  i: Integer;
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    Anchors := FrwObject.Anchors;
    Color := FrwObject.Color;
    FrwObject.Font.AssignTo(Font);
    AllowClearKey := FrwObject.AllowClearKey;
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;

    if FrwObject.AllowEdit then
      Style := StdCtrls.csDropDown
    else
      Style := StdCtrls.csDropDownList;

    Items.BeginUpdate;
    try
      Items.Clear;
      for i := 0 to FrwObject.Items.Count-1 do
        if FrwObject.Items.Names[i] = '' then
          Items.Add(FrwObject.Items[i])
        else
          Items.Add(FrwObject.Items.Names[i]);
    finally
      Items.EndUpdate;
    end;

    ItemIndex := FrwObject.ItemIndex;

    TabOrder := FrwObject.TabOrder;
    TabStop := FrwObject.TabStop;
  end;
end;

function TrwComboBoxControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwComboBoxControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwComboBoxControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwDBComboBoxControl }

function TrwDBComboBoxControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwDBComboBoxControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwDBComboBox;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrwDBComboBoxControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := False;
  Result.AcceptedClases := '';
end;

destructor TrwDBComboBoxControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwDBComboBoxControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwDBComboBoxControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    Anchors := FrwObject.Anchors;
    Color := FrwObject.Color;
    FrwObject.Font.AssignTo(Font);
    AllowClearKey := FrwObject.AllowClearKey;
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
    TabOrder := FrwObject.TabOrder;
    TabStop := FrwObject.TabStop;

    if Assigned(FrwObject.DataSource) then
      LookupTable := FrwObject.DataSource.VCLDataSet
    else
      LookupTable := nil;

    SetDisplayFields(FrwObject.DisplayFields);
    LookupField := FrwObject.KeyField;
  end;
end;

function TrwDBComboBoxControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwDBComboBoxControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwDBComboBoxControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwDBGridControl }

function TrwDBGridControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwDBGridControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FUseCaseInsensitiveFilterExpression := True;  
  FrwObject := AOwner as TrwDBGrid;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrwDBGridControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := False;
  Result.AcceptedClases := '';
end;

destructor TrwDBGridControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

function TrwDBGridControl.IsFiltered: Boolean;
begin
  Result := Assigned(FrwObject) and Assigned(FrwObject.DataSource) and Boolean(FrwObject.DataSource.PActive) and (FrwObject.DataSource.Filter <> '');
end;

procedure TrwDBGridControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwDBGridControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    Align := FrwObject.Align;
    Anchors := FrwObject.Anchors;
    Color := FrwObject.Color;
    FrwObject.Font.AssignTo(Font);
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
    TabOrder := FrwObject.TabOrder;
    TabStop := FrwObject.TabStop;

    MultiSelection := FrwObject.MultiSelection;
    if Assigned(FrwObject.DataSource) then
      SortField := FrwObject.DataSource.Sort;
  end;
end;

function TrwDBGridControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwDBGridControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwDBGridControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwTabSheetControl }

function TrwTabSheetControl.Container: TWinControl;
begin
  Result := Self;
end;

constructor TrwTabSheetControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwTabSheet;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrwTabSheetControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [];
  Result.ResizingDirections := [];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := True;
  Result.AcceptedClases := '';
end;

destructor TrwTabSheetControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwTabSheetControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwTabSheetControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      PageControl := (FrwObject.Parent.VisualControl.RealObject as TPageControl)
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
    Caption := FrwObject.Caption;
    TabVisible := FrwObject.TabVisible;
  end;
end;

function TrwTabSheetControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwTabSheetControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwTabSheetControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwPageCtrlControl }

function TrwPageCtrlControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwPageCtrlControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwPageControl;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrwPageCtrlControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := True;
  Result.AcceptedClases := 'TrwTabSheet';
end;

destructor TrwPageCtrlControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwPageCtrlControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwPageCtrlControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    Align := FrwObject.Align;
    Anchors := FrwObject.Anchors;
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
    TabOrder := FrwObject.TabOrder;
    TabStop := FrwObject.TabStop;
  end;
end;

function TrwPageCtrlControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwPageCtrlControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwPageCtrlControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) or (Message.Msg = WM_LBUTTONDOWN) then
    inherited WndProc(Message);
end;

{ TrwButtonControl }

function TrwButtonControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwButtonControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwButton;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
end;

function TrwButtonControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
  Result.AcceptControls := False;
  Result.AcceptedClases := '';
end;

destructor TrwButtonControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwButtonControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwButtonControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    Anchors := FrwObject.Anchors;
    FrwObject.Font.AssignTo(Font);
    Caption := FrwObject.Caption;
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
    TabOrder := FrwObject.TabOrder;
    TabStop := FrwObject.TabStop;
  end;
end;

function TrwButtonControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwButtonControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwButtonControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwFileDialogControl }

function TrwFileDialogControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwFileDialogControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwFileDialog;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);
  
  ButtonStyle := cbsCustom;
end;

function TrwFileDialogControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;  
  Result.StandaloneObject := True;
  Result.AcceptControls := False;
  Result.AcceptedClases := '';
end;

destructor TrwFileDialogControl.Destroy;
begin
  FreeAndNil(FDialog);
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwFileDialogControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwFileDialogControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPCreation, rwCPParent] then
    if Assigned(FrwObject.Parent) then
      Parent := FrwObject.Parent.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPCreation] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    Anchors := FrwObject.Anchors;
    FrwObject.Font.AssignTo(Font);
    Visible := FrwObject.Visible;
    Enabled := FrwObject.Enabled;
    TabOrder := FrwObject.TabOrder;
    TabStop := FrwObject.TabStop;
    ReadOnly := FrwObject.ReadOnly;
    DialogType := FrwObject.DialogType;
  end;
end;

function TrwFileDialogControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwFileDialogControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

procedure TrwFileDialogControl.SetDialogType(const Value: TrwFileDialogType);
begin
  if (FDialogType = Value) and Assigned(FDialog) then
    Exit;

  FDialogType := Value;
  FreeAndNil(FDialog);
  if FDialogType = rfdOpenFile then
  begin
    FDialog := TOpenDialog.Create(nil);
    ButtonGlyph.LoadFromResourceName(HInstance, 'OPENFILE');
  end
  else
  begin
    FDialog := TSaveDialog.Create(nil);
    ButtonGlyph.LoadFromResourceName(HInstance, 'SAVEFILE');
  end;
end;

function TrwFileDialogControl.ShowDialog: Boolean;
begin
  FDialog.FileName := FrwObject.FileName;
  FDialog.DefaultExt := FrwObject.DefaultExt;
  FDialog.Filter := FrwObject.Filter;
  FDialog.FilterIndex := FrwObject.FilterIndex;
  FDialog.InitialDir := FrwObject.InitialDir;
  FDialog.Options := FrwObject.Options;
  FDialog.Title := FrwObject.Title;

  Result := FDialog.Execute;

  if Result then
    FrwObject.ParamValue := FDialog.FileName;
end;

procedure TrwFileDialogControl.WndProc(var Message: TMessage);
begin
  if not IsDesignMsg(FrwObject, Message) then
    inherited WndProc(Message);
end;

{ TrwBarcode1DControl }

function TrwBarcode1DControl.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwBarcode1DControl.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwBarcode1D;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);

  ControlStyle := ControlStyle + [csReplicatable];
  ParentColor := False;
  ParentFont := False;
  AutoSize := True;
  TopMargin := 10;
  LeftMargin := 10;
end;

function TrwBarcode1DControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
end;

destructor TrwBarcode1DControl.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwBarcode1DControl.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwBarcode1DControl.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPPosition] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    if (FrwObject as IrmDesignObject).GetDesigner <> nil then
      (FrwObject as IrmDesignObject).GetDesigner.Notify(rmdObjectResized, Integer(Pointer(FrwObject)));
    Autosize := FrwObject.Autosize;
  end;

  if APropType in [rwCPAll, rwCPParent] then
    if Assigned(FrwObject.Container) then
      Parent := FrwObject.Container.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPAll, rwCPColor] then
    if not FrwObject.Transparent then
      BackColor := FrwObject.Color;

  if APropType in [rwCPAll, rwCPFont] then
    FrwObject.Font.AssignTo(Font);

  if FrwObject.Transparent then
  begin
    ControlStyle := ControlStyle - [csOpaque];
    BackColor := clNone;
  end
  else
  begin
    ControlStyle := ControlStyle + [csOpaque];
    BackColor := FrwObject.Color;
  end;

  BarColor := FrwObject.BarColor;
  Code := FrwObject.Text;
  BarHeightPixels := FrwObject.BarHeight;
  barType := FrwObject.BarType;
  ShowText := FrwObject.ShowText;
  guardBars := FrwObject.GuardBars;
  CodeSup := FrwObject.SupplementalCode;
  checkCharacter := FrwObject.CheckCharacter;
  PostnetHeightTallBar := FrwObject.PostnetHeightTallBar;
  PostnetHeightShortBar := FrwObject.PostnetHeightShortBar;
  UPCEANSupplement2 := FrwObject.UPCEANSupplement2;
  UPCEANSupplement5 := FrwObject.UPCEANSupplement5;
  UPCESytem := FrwObject.UPCESytem;
  CODABARStartChar := FrwObject.CODABARStartChar;
  CODABARStopChar := FrwObject.CODABARStopChar;
  Code128Set := FrwObject.Code128Set;
  supSeparation := FrwObject.SupSeparation;
  VerticalOrientation := FrwObject.VerticalOrientation;
  TopMargin := FrwObject.TopMargin;
  LeftMargin := FrwObject.LeftMargin;

  Invalidate;
end;

function TrwBarcode1DControl.RealObject: TControl;
begin
  Result := Self;
end;

function TrwBarcode1DControl.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

{ TrwBarcodePDF417Control }

function TrwBarcodePDF417Control.Container: TWinControl;
begin
  Result := nil;
end;

constructor TrwBarcodePDF417Control.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FrwObject := AOwner as TrwBarcodePDF417;
  if FrwObject.IsDesignMode then
    SetDesigning(True, True);

  ControlStyle := ControlStyle + [csReplicatable];
  ParentColor := False;
  ParentFont := False;
  AutoSize := True;
  TopMargin := 10;
  LeftMargin := 10;
end;

function TrwBarcodePDF417Control.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
  Result.Selectable := IsSelectable(FrwObject);
  Result.SelectorType := rwDBISBDots;
  Result.StandaloneObject := True;
end;

destructor TrwBarcodePDF417Control.Destroy;
begin
  FrwObject.VisualControl := nil;
  inherited;
end;

procedure TrwBarcodePDF417Control.Notify(AEvent: TrmDesignEventType);
begin
end;

procedure TrwBarcodePDF417Control.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPPosition] then
  begin
    SetBounds(FrwObject.Left, FrwObject.Top, FrwObject.Width, FrwObject.Height);
    if (FrwObject as IrmDesignObject).GetDesigner <> nil then
      (FrwObject as IrmDesignObject).GetDesigner.Notify(rmdObjectResized, Integer(Pointer(FrwObject)));
    Autosize := FrwObject.Autosize;
  end;

  if APropType in [rwCPAll, rwCPParent] then
    if Assigned(FrwObject.Container) then
      Parent := FrwObject.Container.VisualControl.Container
    else
      Parent := nil;

  if APropType in [rwCPAll, rwCPColor] then
    if not FrwObject.Transparent then
      BackColor := FrwObject.Color;

  if APropType in [rwCPAll, rwCPFont] then
    FrwObject.Font.AssignTo(Font);

  if FrwObject.Transparent then
  begin
    ControlStyle := ControlStyle - [csOpaque];
    BackColor := clNone;
  end
  else
  begin
    ControlStyle := ControlStyle + [csOpaque];
    BackColor := FrwObject.Color;
  end;

  BarColor := FrwObject.BarColor;
  Code := FrwObject.Text;
  BarHeightPixels := FrwObject.BarHeightPixels;
  BarWidthPixels := FrwObject.BarWidthPixels;
  pdfColumns := FrwObject.ColumnsNumber;
  pdfRows := FrwObject.RowsNumber;
  pdfMaxRows := Frwobject.MaxRows;
  pdfECLevel := FrwObject.ECLevel;
  pdfMode := FrwObject.EncodingMode;
  pdfTruncated := FrwObject.Truncated;
  TopMargin := FrwObject.TopMargin;
  LeftMargin := FrwObject.LeftMargin;
  HorizontalPixelShaving := FrwObject.HorizontalPixelShaving;
  VerticalPixelShaving := FrwObject.VerticalPixelShaving;

  Invalidate;
end;

function TrwBarcodePDF417Control.RealObject: TControl;
begin
  Result := Self;
end;

function TrwBarcodePDF417Control.RWObject: TrwComponent;
begin
  Result := FrwObject;
end;

end.
