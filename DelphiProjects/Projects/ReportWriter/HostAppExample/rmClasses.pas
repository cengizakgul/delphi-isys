unit rmClasses;

interface

uses Classes, Variants, TypInfo;

type
   TrmTypeKind = (rmTKUnknown, rmTKInteger, rmTKFloat, rmTKCurrency, rmTKBoolean, rmTKString, rmTKDateTime, rmTKArray, rmTKVariant, rmTKClass, rmTKMethod);


   TrmTypeInfo = record
     Kind: TrmTypeKind;
     Name: ShortString;
   end;

   PTrmTypeInfo = ^TrmTypeInfo;

   PTrmTypeData = ^TrmTypeData;

   TTypeData = packed record
     case TrmTypeKind of
        rmTKUnknown, rmTKInteger, rmTKFloat, rmTKCurrency, rmTKBoolean, rmTKString, rmTKDateTime, rmTKVariant: ();
        tkClass: (
          ClassType: TrmClass;
          ParentInfo: PPTypeInfo;
          PropCount: SmallInt;
          UnitName: ShortStringBase;
       {PropData: TPropData});
      tkMethod: (
        MethodKind: TMethodKind;
        ParamCount: Byte;
        ParamList: array[0..1023] of Char
       {ParamList: array[1..ParamCount] of
          record
            Flags: TParamFlags;
            ParamName: ShortString;
            TypeName: ShortString;
          end;
        ResultType: ShortString});
      tkInterface: (
        IntfParent : PPTypeInfo; { ancestor }
        IntfFlags : TIntfFlagsBase;
        Guid : TGUID;
        IntfUnit : ShortStringBase;
       {PropData: TPropData});
      tkInt64: (
        MinInt64Value, MaxInt64Value: Int64);
	  tkDynArray: (
		elSize: Longint;
		elType: PPTypeInfo;       // nil if type does not require cleanup
		varType: Integer;         // Ole Automation varType equivalent
		elType2: PPTypeInfo;      // independent of cleanup
    DynUnitName: ShortStringBase);
  end;


   PrmPropList = ^TrmPropList;
   TrmPropList = array of PTrmPropInfo;




   IrmObject = interface
   ['{12BF7FF0-F6F9-4C3E-9CF0-9159E6B889BB}']
     function  GetPropertyInfo(const APropertyName: String): PrmTypeInfo;
     function  GetPropertyValue(const APropertyName: String): Variant;
     procedure SetPropertyValue(const APropertyName: String; const AValue: Variant);
   end;



   IrmComponent = interface(IrmObject)
   ['{4D31B074-7A7D-473C-B32D-F9E480DB9992}']
   end;


{   TrmObject = class(TInterfacedObject, IrmObject)
   end;


   TrmComponent = class(TrmObject, IrmComponent)
   private
   protected

   public
     constructor Create(AOwner: TrmComponent); virtual;
     destructor  Destroy; override;
   published
   end;
}


implementation


end.
