unit DemoDBdm;

interface

uses
  SysUtils, Classes, DB, DBTables, Forms;

type
  TDemoDM = class(TDataModule)
    dbDemo: TDatabase;
    tblInfo: TTable;
    Query: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
  public
  end;

var
  DemoDM: TDemoDM;

implementation

{$R *.dfm}

procedure TDemoDM.DataModuleCreate(Sender: TObject);
begin
  dbDemo.Open;
end;

procedure TDemoDM.DataModuleDestroy(Sender: TObject);
begin
  dbDemo.Close;
end;

initialization
  DemoDM := TDemoDM.Create(Application);

finalization
  FreeAndNil(DemoDM);  

end.
