object Main: TMain
  Left = 318
  Top = 177
  AutoScroll = False
  Caption = 'Report Writer Test'
  ClientHeight = 506
  ClientWidth = 727
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 167
    Height = 506
    Align = alLeft
    BevelOuter = bvLowered
    Color = 15794156
    TabOrder = 0
    object Button1: TButton
      Left = 6
      Top = 38
      Width = 153
      Height = 25
      Caption = 'Show Designer'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button4: TButton
      Left = 6
      Top = 7
      Width = 153
      Height = 25
      Caption = 'Report Master'
      TabOrder = 1
      OnClick = Button4Click
    end
  end
  object PageControl1: TPageControl
    Left = 167
    Top = 0
    Width = 560
    Height = 506
    ActivePage = tsSettings
    Align = alClient
    TabOrder = 1
    object tsSettings: TTabSheet
      Caption = 'Settings'
      ImageIndex = 2
      object chbModalInputForm: TCheckBox
        Left = 6
        Top = 5
        Width = 155
        Height = 17
        Caption = 'Show Modal Input Form'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = chbModalInputFormClick
      end
      object chbModalPreview: TCheckBox
        Left = 6
        Top = 26
        Width = 160
        Height = 17
        Caption = 'Show Modal Preview'
        TabOrder = 1
        OnClick = chbModalPreviewClick
      end
      object chbPrintResult: TCheckBox
        Left = 6
        Top = 56
        Width = 163
        Height = 17
        Caption = 'Print Result without preview'
        TabOrder = 2
      end
    end
    object tsInputForm: TTabSheet
      Caption = 'Input Form'
      object Panel2: TPanel
        Left = 0
        Top = 433
        Width = 552
        Height = 45
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          552
          45)
        object Button3: TButton
          Left = 444
          Top = 12
          Width = 97
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Run Report'
          TabOrder = 0
          OnClick = Button3Click
        end
      end
    end
    object tsPreview: TTabSheet
      Caption = 'Preview'
      ImageIndex = 1
      OnHide = tsPreviewHide
      OnResize = tsPreviewResize
    end
  end
end
