unit dmAdapterFrm;

interface

uses
  SysUtils, Classes, Forms, Controls, DB, rwCallback, rwEngineTypes, StdCtrls, Windows,
  AppCustomControlsFrm, Graphics, EvStreamUtils, Dialogs;

type
  TdmAdapter = class(TDataModule)
    rwAppAdapter: TrwAppAdapter;
    procedure rwAppAdapterDBCacheCleaning(
      const DataDictExternalTableName: String; var Accept: Boolean);
    procedure rwEngineCallBackEndProgressBar;
    procedure rwAppAdapterGetDDFieldInfo(const ATable, AField: String;
      out FieldInfo: TrwDDFieldInfoRec);
    procedure rwAppAdapterGetDDFields(const ATable: String;
      out Fields: String);
    procedure rwAppAdapterGetDDTables(out Tables: String);
    procedure rwAppAdapterGetTempDir(out TempDir: String);
    procedure rwAppAdapterLoadDataDictionary(Data: TStream);
    procedure rwAppAdapterLoadLibraryComponents(Data: TStream);
    procedure rwAppAdapterLoadLibraryFunctions(Data: TStream);
    procedure rwAppAdapterLoadQBTemplates(Data: TStream);
    procedure rwAppAdapterLoadSQLConstants(Data: TStream);
    procedure rwEngineCallBackStartProgressBar(const Title: String;
      MaxValue: Integer);
    procedure rwAppAdapterSubstDefaultClassName(
      var ADefaultClassName: String);
    procedure rwAppAdapterTableDataRequest(
      const CachedTable: IrwCachedTable; const sbTable: IrwSBTable;
      const Params: array of TrwParamRec);
    procedure rwAppAdapterUnloadDataDictionary(Data: TStream);
    procedure rwAppAdapterUnloadLibraryComponents(Data: TStream);
    procedure rwAppAdapterUnloadLibraryFunctions(Data: TStream);
    procedure rwAppAdapterUnloadQBTemplates(Data: TStream);
    procedure rwAppAdapterUnloadSQLConstants(Data: TStream);
    procedure rwDesignerCallBackGetSecurityDescriptor(
      var ASecurityDescriptor: TrwDesignerSecurityRec);
    procedure rwAppAdapterGetCustomControl(const Destination: String;
      var AContainer: TForm);
  private
    function  ConvertDataType(const ADataType: TFieldType): TrwDDType;
    function  GetAppPath: String;
  end;

var
  dmAdapter: TdmAdapter;

  procedure LoadFileIntoStream(const AFileName: String; const AStream: TStream);
  procedure StoreStreamIntoFile(const AFileName: String; const AStream: TStream);


implementation

uses DemoDBdm;

{$R *.dfm}

function ReportWriterAppAdapter: IrwAppAdapter; stdcall;
begin
  Result := dmAdapter.rwAppAdapter;
end;

exports ReportWriterAppAdapter;


procedure LoadFileIntoStream(const AFileName: String;
  const AStream: TStream);
var
  FS: TEvFileStream;
begin
  AStream.Size := 0;
  AStream.Position := 0;

  if FileExists(AFileName) then
  begin
    FS := TEvFileStream.Create(AFileName, fmOpenRead);
    try
      AStream.CopyFrom(FS, FS.Size);
    finally
      FS.Free
    end;
  end;
end;

procedure StoreStreamIntoFile(const AFileName: String;
  const AStream: TStream);
var
  FS: TEvFileStream;
begin
  FS := TEvFileStream.Create(AFileName, fmCreate);
  try
    AStream.Position := 0;
    FS.CopyFrom(AStream, AStream.Size);
  finally
    FS.Free
  end;
end;

function TdmAdapter.ConvertDataType(const ADataType: TFieldType): TrwDDType;
begin
  case ADataType of
    ftString, ftWideString:         Result := ddtString;
    ftSmallint, ftInteger,
    ftWord, ftAutoInc:              Result := ddtInteger;
    ftFloat, ftBCD:                 Result := ddtFloat;
    ftCurrency:                     Result := ddtCurrency;
    ftBoolean:                      Result := ddtBoolean;
    ftDate, ftTime, ftDateTime:     Result := ddtDateTime;
    ftBlob:                         Result := ddtBLOB;
    ftMemo:                         Result := ddtMemo;
    ftGraphic:                      Result := ddtGraphic;
    ftArray:                        Result := ddtArray;
  else
    Result := ddtUnknown;
  end;
end;

function TdmAdapter.GetAppPath: String;
begin
  Result := ExtractFilePath(Application.ExeName);
end;

procedure TdmAdapter.rwAppAdapterDBCacheCleaning(
  const DataDictExternalTableName: String; var Accept: Boolean);
begin
  Accept := True;
end;

procedure TdmAdapter.rwEngineCallBackEndProgressBar;
begin
  Screen.Cursor := crDefault;
end;

procedure TdmAdapter.rwAppAdapterGetDDFieldInfo(const ATable,
  AField: String; out FieldInfo: TrwDDFieldInfoRec);
begin
  DemoDM.tblInfo.TableName := ATable;
  DemoDM.tblInfo.Open;
  try
    FieldInfo.DisplayName := DemoDM.tblInfo.FieldByName(AField).DisplayName;
    FieldInfo.FieldType := ConvertDataType(DemoDM.tblInfo.FieldByName(AField).DataType);
    FieldInfo.Size := DemoDM.tblInfo.FieldByName(AField).Size;
    FieldInfo.FieldValues := '';
  finally
    DemoDM.tblInfo.Close;
  end;
end;

procedure TdmAdapter.rwAppAdapterGetDDFields(const ATable: String;
  out Fields: String);
var
  L: TStringList;
  i: Integer;
begin
  Fields := '';
  L := TStringList.Create;
  try
    DemoDM.dbDEMO.GetFieldNames(ATable, L);

    for i := 0 to L.Count - 1 do
    begin
      if i > 0 then
        Fields := Fields + ',';
      Fields := Fields + L[i]
    end;

  finally
    L.Free;
  end;
end;

procedure TdmAdapter.rwAppAdapterGetDDTables(out Tables: String);
var
  L: TStringList;
  i: Integer;
begin
  Tables := '';
  L := TStringList.Create;
  try
    DemoDM.dbDEMO.GetTableNames(L);
    for i := 0 to L.Count - 1 do
    begin
      if i > 0 then
        Tables := Tables + ',';
      Tables := Tables + L[i];
    end;
  finally
    L.Free;
  end;
end;

procedure TdmAdapter.rwAppAdapterGetTempDir(out TempDir: String);
begin
  TempDir := GetAppPath;
end;

procedure TdmAdapter.rwAppAdapterLoadDataDictionary(Data: TStream);
begin
  LoadFileIntoStream(GetAppPath + 'DataDictionary.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterLoadLibraryComponents(
  Data: TStream);
begin
  LoadFileIntoStream(GetAppPath + 'LibComponents.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterLoadLibraryFunctions(Data: TStream);
begin
  LoadFileIntoStream(GetAppPath + 'LibFunctions.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterLoadQBTemplates(Data: TStream);
begin
  LoadFileIntoStream(GetAppPath + 'QBTemplates.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterLoadSQLConstants(Data: TStream);
begin
  LoadFileIntoStream(GetAppPath + 'QBConstants.rwe', Data);
end;

procedure TdmAdapter.rwEngineCallBackStartProgressBar(
  const Title: String; MaxValue: Integer);
begin
  Screen.Cursor := crHourGlass;
end;

procedure TdmAdapter.rwAppAdapterSubstDefaultClassName(
  var ADefaultClassName: String);
begin
{  if ADefaultClassName = 'TrmTableCell' then
    ADefaultClassName := 'TrmCustomTableCell'
  else if ADefaultClassName = 'TrmDBTable' then
    ADefaultClassName := 'TrmCustomDBTable';}
end;

procedure TdmAdapter.rwAppAdapterTableDataRequest(
  const CachedTable: IrwCachedTable; const sbTable: IrwSBTable;
  const Params: array of TrwParamRec);
var
  FldVals: array of Variant;
  i: Integer;
  h, flt: String;
  Flds: TrwStrList;
begin
  with DemoDM.Query do
  begin
    Flds := CachedTable.GetExternalFields;
    flt := CachedTable.GetFilter;

    SetLength(FldVals, High(Flds) - Low(Flds) + 1);
    h := '';
    for i := Low(FldVals) to High(FldVals) do
    begin
      if i > 0 then
        h := h + ', ';
      h := h + Flds[i];
    end;
    h := 'Select ' + h + ' From ' + CachedTable.ddTable.GetTableExternalName;
    if flt <> '' then
      h := h + ' where ' + flt;
    SQL.Text := h;

    Open;
    try
      while not Eof do
      begin
        for i := Low(FldVals) to High(FldVals) do
          FldVals[i] := FieldByName(Flds[i]).Value;
        sbTable.AppendRecord(FldVals);
        Next;
      end;
    finally
      Close;
      SQL.Text := '';
    end;
  end;
end;

procedure TdmAdapter.rwAppAdapterUnloadDataDictionary(Data: TStream);
begin
  StoreStreamIntoFile(GetAppPath + 'DataDictionary.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterUnloadLibraryComponents(
  Data: TStream);
begin
  StoreStreamIntoFile(GetAppPath + 'LibComponents.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterUnloadLibraryFunctions(
  Data: TStream);
begin
  StoreStreamIntoFile(GetAppPath + 'LibFunctions.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterUnloadQBTemplates(Data: TStream);
begin
  StoreStreamIntoFile(GetAppPath + 'QBTemplates.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterUnloadSQLConstants(Data: TStream);
begin
  StoreStreamIntoFile(GetAppPath + 'QBConstants.rwe', Data);
end;

procedure TdmAdapter.rwDesignerCallBackGetSecurityDescriptor(var ASecurityDescriptor: TrwDesignerSecurityRec);
begin
  ASecurityDescriptor.DataDictionary.Modify := True;
  ASecurityDescriptor.QBTemplates.Modify := True;
  ASecurityDescriptor.QBConstants.Modify := True;
  ASecurityDescriptor.LibComponents.Modify := True;
  ASecurityDescriptor.LibFunctions.Modify := True;
end;


procedure TdmAdapter.rwAppAdapterGetCustomControl(const Destination: String; var AContainer: TForm);
begin
  if Destination = 'Query Builder ToolBar' then
  begin
    AContainer := TAppCustomControls.Create(Self);
  end;
end;

initialization
  dmAdapter := TdmAdapter.Create(Application);

finalization
  FreeAndNil(dmAdapter);

end.

