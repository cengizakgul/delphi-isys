unit MainFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ExtCtrls, Printers, ComCtrls, ActiveX,
  isRWDesigner_TLB, isRWEngine_TLB, isRWPreview_TLB, EvStreamUtils, XPMan,
  AppEvnts, madExcept;

type
  TMain = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    PageControl1: TPageControl;
    tsInputForm: TTabSheet;
    tsPreview: TTabSheet;
    Panel2: TPanel;
    Button3: TButton;
    tsSettings: TTabSheet;
    chbModalInputForm: TCheckBox;
    chbModalPreview: TCheckBox;
    chbPrintResult: TCheckBox;
    Button4: TButton;
    procedure FormCreate(Sender: TObject);
    procedure chbModalPreviewClick(Sender: TObject);
    procedure chbModalInputFormClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure tsPreviewHide(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure tsPreviewResize(Sender: TObject);
  private
    Preview: IrwPreviewApp;
    FReportResult: IEvDualStream;
  public
  end;


var
  Main: TMain;

implementation


{$R *.dfm}


procedure TMain.FormCreate(Sender: TObject);
begin
  chbModalInputForm.OnClick(nil);
  chbModalPreview.OnClick(nil);
end;

procedure TMain.chbModalPreviewClick(Sender: TObject);
begin
  tsPreview.TabVisible := not chbModalPreview.Checked;
end;

procedure TMain.chbModalInputFormClick(Sender: TObject);
begin
  tsInputForm.TabVisible := not chbModalInputForm.Checked;
end;

procedure TMain.Button4Click(Sender: TObject);
var
  Designer: IRWDesignerApp;
begin
  Designer := CoRWDesignerApp.Create;
  Designer.InitializeAppAdapter('rwAdapterExample.dll', '');
  Designer.OpenDesigner(nil, '', 1);
end;

procedure TMain.Button1Click(Sender: TObject);
var
  Engine: IrwEngineApp;
  ReportStructure: IrwReportStructure;
  ReportResult, PDF: IEvDualStream;
  Res: IrwReportResult;
  RunInfo: TrwRunReportSettings;
  n: Integer;
  ReportResultInfo: TrwReportResultInfo;
  PrintJobInfo: TrwPrintJobInfo;
  S: IStream;
  PDFInfo: TrwPDFFileInfo;
  v: Variant;
  PgInf: OleVariant;
  RWAInfo: TrwRWAFileInfo;
  i: Integer;

  procedure Test;
  begin
    ReportStructure := Engine.ReportStructureFromStream(S);
  end;

begin
  Engine := CoRWEngineApp.Create;
  try
    Engine.InitializeAppAdapter('rwAdapterExample.dll', '');

//    S := TEvDualStreamHolder.CreateFromFile('D:\Report.rwr').GetOleAdapter;
    S := TEvDualStreamHolder.CreateInMemory.GetOleAdapter;
    Engine.ReportStructureFromStream(S);

    for i := 1 to 1000 do
    begin
      Test;
    end;

    Exit;

    ReportResult := TEvDualStreamHolder.Create;

    RunInfo.ReturnParamsBack := True;
    RunInfo.Duplexing := False;
   {
    tsInputForm.Show;
    ReportStructure.ReportInputForm.Show(tsInputForm.Handle, False);
    ReportStructure.ReportInputForm.Left := 0;
    ReportStructure.ReportInputForm.Top := 0;
    ReportStructure.ReportInputForm.Width := tsInputForm.ClientWidth;
    ReportStructure.ReportInputForm.Height := tsInputForm.ClientHeight;

    while tsInputForm.Visible do
    begin
      Sleep(1);
      Application.ProcessMessages;
    end;

    ReportStructure.ReportInputForm.CloseOK;
 }
    Res := Engine.RunReport(ReportStructure, RunInfo);
    PgInf := Res.GetPageInfo;

    v := ReportStructure.ReportParameters.ParamByName('test').Value;

   Res.SaveToStream(ReportResult.GetOleAdapter);

   RWAInfo.PreviewPassword := 'a';
   RWAInfo.PrintPassword := 'a';
   RWAInfo.Compression := True;
   Engine.ChangeRWAInfo(ReportResult.GetOleAdapter, '', RWAInfo);
   ReportResult.SaveToFile('d:\test.rwa');


   n := Ord(Res.FormatType);

   PDF := TEvDualStreamHolder.Create;

   PDFInfo.Author := 'Aleksey';
   PDFInfo.Creator := '';
   PDFInfo.Subject := '';
   PDFInfo.Title := '';
   PDFInfo.Compression := False;
   PDFInfo.ProtectionOptions := 3;
   PDFInfo.OwnerPassword := 'a';
   PDFInfo.UserPassword := 'a';

   Engine.RWAtoPDF(ReportResult.GetOleAdapter, PDFInfo, PDF.GetOleAdapter);
   PDF.SaveToFile('d:\test.pdf');

  finally
    ReportStructure := nil;
    Engine := nil;
  end;

  if ReportResult.Size > 0 then
  begin
    if Assigned(Preview) then
      Preview := nil;

    tsPreview.Show;

    ReportResult.LoadFromFile('D:\LayersProblem.rwa');
    Preview := CoRWPreviewApp.Create;

//    ZeroMemory(@ReportResultInfo, SizeOf(ReportResultInfo));
//    ZeroMemory(@PrintJobInfo, SizeOf(PrintJobInfo));

    ReportResultInfo.Name :=  'Test';
    ReportResultInfo.SecuredMode := False;
    ReportResultInfo.PreviewPwd := '';
    ReportResultInfo.PrintPwd := '';
    ReportResultInfo.FormatType := 1;

{    PrintJobInfo.PrinterName := '\\ds2\HP LaserJet 4100 PCL 5e';
    PrintJobInfo.Copies := 2;
    Preview.ReportResultGroup.Add(ReportResult.GetOleAdapter, ReportResultInfo, PrintJobInfo);
    PrintJobInfo.Copies := 1;
}
    PrintJobInfo.PrinterName := '\\ds2\HP LaserJet 8150 PCL 5e';
    PrintJobInfo.Layers := '2';
    Preview.ReportResultGroup.Add(ReportResult.GetOleAdapter, ReportResultInfo, PrintJobInfo);

//    S := Preview.MergeGroup as IStream;

//    FReportResult := TEvDualStreamHolder.Create;
//    CopyStreamToStream(S, FReportResult);

//    PrintJobInfo.Copies := 1;
//    Preview.PrintGroup(PrintJobInfo);

    Preview.PreviewGroup(tsPreview.Handle);
  end;
end;

procedure TMain.tsPreviewHide(Sender: TObject);
begin
  Preview := nil;
end;

procedure TMain.Button3Click(Sender: TObject);
begin
  tsPreview.Show;
end;

procedure TMain.tsPreviewResize(Sender: TObject);
var
  FB: TrwBounds;
begin
  if Assigned(Preview) then
  begin
    FB.Left := 0;
    FB.Top := 0;
    FB.Width := tsPreview.ClientWidth;
    FB.Height := tsPreview.ClientHeight;

    Preview.FormBounds := FB;
  end;
end;


end.
