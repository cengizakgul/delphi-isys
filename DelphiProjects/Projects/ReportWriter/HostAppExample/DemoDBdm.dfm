object DemoDM: TDemoDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 507
  Top = 272
  Height = 150
  Width = 215
  object dbDemo: TDatabase
    AliasName = 'DBDEMOS'
    DatabaseName = 'dbDEMO'
    LoginPrompt = False
    ReadOnly = True
    SessionName = 'Default'
    Left = 24
    Top = 8
  end
  object tblInfo: TTable
    DatabaseName = 'dbDEMO'
    TableType = ttParadox
    Left = 64
    Top = 8
  end
  object Query: TQuery
    DatabaseName = 'dbDEMO'
    UniDirectional = True
    Left = 101
    Top = 8
  end
end
