program isRWWorkbench;

uses
  Forms,
  MainFrm in '..\rwWorkbench\MainFrm.pas' {frmMain},
  ReportsFrm in '..\rwWorkbench\ReportsFrm.pas' {frmReports: TFrame},
  SettingsFrm in '..\rwWorkbench\SettingsFrm.pas' {frmSettings: TFrame},
  Common in '..\rwWorkbench\Common.pas',
  rwAdapterUtils in '..\RwEngine\rwAdapterUtils.pas',
  rwInputFormContainerFrm in '..\rwWorkbench\rwInputFormContainerFrm.pas' {rwInputFormContainer: TFrame},
  rwPreviewContainerFrm in '..\rwWorkbench\rwPreviewContainerFrm.pas' {rwPreviewContainer: TFrame},
  rwModalInputFormFrm in '..\rwWorkbench\rwModalInputFormFrm.pas' {rwModalInputForm},
  ShellCtrls in '..\rwWorkbench\ShellCtrls.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'RW Workbench';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
