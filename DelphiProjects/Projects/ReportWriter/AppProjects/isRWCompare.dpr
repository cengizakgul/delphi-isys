program isRWCompare;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  Forms,
  rwCompareMainFrm in '..\RWCompare\rwCompareMainFrm.pas' {rwCompareMain},
  rwCompareUtils in '..\RWCompare\rwCompareUtils.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'RW Reports Compare';
  Application.CreateForm(TrwCompareMain, rwCompareMain);
  Application.Run;
end.
