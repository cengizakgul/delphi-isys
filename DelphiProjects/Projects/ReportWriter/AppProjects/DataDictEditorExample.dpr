program DataDictEditorExample;

uses
  Forms,
  ddParamsEditFrm in '..\DataDictEditExample\ddParamsEditFrm.pas' {ddParamsEdit: TFrame},
  ddTableEditorFrm in '..\DataDictEditExample\ddTableEditorFrm.pas' {ddTableEditor: TFrame},
  ddEditorFrm in '..\DataDictEditExample\ddEditorFrm.pas' {ddEditor},
  ddFieldsEditFrm in '..\DataDictEditExample\ddFieldsEditFrm.pas' {ddFieldsEdit: TFrame},
  ddForeignKeyEditFrm in '..\DataDictEditExample\ddForeignKeyEditFrm.pas' {ddForeignKeyEdit: TFrame},
  OldDataDictionary in '..\Crap\OldDataDictionary.pas',
  SFieldCodeValues in '..\Crap\SFieldCodeValues.pas';

{$R *.res}

var
  ddEditor: TddEditor;
  
begin
  Application.Initialize;
  Application.Title := 'Data Dictionary Editor';
  Application.CreateForm(TddEditor, ddEditor);
  Application.Run;
end.
