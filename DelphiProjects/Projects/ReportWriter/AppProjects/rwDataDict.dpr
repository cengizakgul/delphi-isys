program rwDataDict;

uses
  Forms,
  RwDataDictionary in '..\RwDataDict\rwDataDictionary.pas',
  rwDDFieldEditFrm in '..\RwDataDict\rwDDFieldEditFrm.pas' {rwDDFieldEdit: TFrame},
  rwDDTablePropEditFrm in '..\RwDataDict\rwDDTablePropEditFrm.pas' {rwDDTablePropEdit: TFrame},
  rwDDParamsEditFrm in '..\RwDataDict\rwDDParamsEditFrm.pas' {rwDDParamsEdit: TFrame},
  rwDDParamEditFrm in '..\RwDataDict\rwDDParamEditFrm.pas' {rwDDParamEdit: TFrame},
  rwDDTableEditorFrm in '..\RwDataDict\rwDDTableEditorFrm.pas' {rwDDTableEditor: TFrame},
  rwDDForeignKeyEditFrm in '..\RwDataDict\rwDDForeignKeyEditFrm.pas' {rwDDForeignKeyEdit: TFrame},
  rwDDFieldsEditFrm in '..\RwDataDict\rwDDFieldsEditFrm.pas' {rwDDFieldsEdit: TFrame},
  rwDDEditorFrm in '..\RwDataDict\rwDDEditorFrm.pas' {rwDDEditor};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TrwDDEditor, rwDDEditor);
  Application.Run;
end.
