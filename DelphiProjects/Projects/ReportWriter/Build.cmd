@ECHO OFF

SET pAppVersion=%1

IF "%pAppVersion%" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. Application Version
Pause
GOTO end

:start

FOR %%a IN (..\..\Bin\ReportWriter\*.*) DO DEL /F /Q /S ..\..\Bin\ReportWriter\*.* > nul

CALL Compile.cmd %pAppVersion%

ECHO *Move files to Common\RW folder
FOR %%a IN (..\..\Bin\ReportWriter\*.exe) DO COPY /Y %%a ..\..\Common\RW\*.* > nul

:end