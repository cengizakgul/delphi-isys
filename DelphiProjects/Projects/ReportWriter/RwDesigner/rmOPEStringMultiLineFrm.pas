unit rmOPEStringMultiLineFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls;

type
  TrmOPEStringMultiLine = class(TrmObjPropertyEditor)
    mString: TMemo;
    procedure mStringChange(Sender: TObject);
  public
    procedure ShowPropValue; override;
    function  NewPropertyValue: Variant; override;    
  end;

implementation

{$R *.dfm}

{ TrmObjPropertyEditor1 }

function TrmOPEStringMultiLine.NewPropertyValue: Variant;
begin
  Result := mString.Text;
end;

procedure TrmOPEStringMultiLine.ShowPropValue;
begin
  inherited;
  mString.Text := VarToStr(OldPropertyValue);
end;

procedure TrmOPEStringMultiLine.mStringChange(Sender: TObject);
begin
  NotifyOfChanges;
end;

end.
