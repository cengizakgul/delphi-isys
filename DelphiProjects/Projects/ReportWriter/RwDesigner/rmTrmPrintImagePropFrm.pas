unit rmTrmPrintImagePropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmPrintControlPropFrm, rmOPEBooleanFrm,
  rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, StdCtrls, ExtCtrls,
  ComCtrls, rmOPELoadFromFileFrm, rmOPEPictureFrm, rmOPEExpressionFrm,
  rmOPEBlockingControlFrm;

type
  TrmTrmPrintImageProp = class(TrmTrmPrintControlProp)
    frmTransparent: TrmOPEBoolean;
    rmOPEPicture1: TrmOPEPicture;
    frmStretch: TrmOPEBoolean;
  private
  public
    procedure GetPropsFromObject; override;  
  end;

implementation

{$R *.dfm}

{ TrmTrwImageProp }

procedure TrmTrmPrintImageProp.GetPropsFromObject;
begin
  inherited;
  frmStretch.PropertyName := 'Stretch';
  frmStretch.ShowPropValue;
  frmTransparent.PropertyName := 'Transparent';
  frmTransparent.ShowPropValue;
end;

end.
 