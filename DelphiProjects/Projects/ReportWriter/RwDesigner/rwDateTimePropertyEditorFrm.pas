unit rwDateTimePropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rwPropertyEditorFrm, StdCtrls, ComCtrls;

type
  TrwDateTimePropertyEditor = class(TrwPropertyEditor)
    Calendar: TMonthCalendar;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CalendarDblClick(Sender: TObject);
  private
  public
    function ShowModal: Integer; override;
  end;

implementation

{$R *.dfm}

{ TrwPropertyEditor1 }


procedure TrwDateTimePropertyEditor.FormShow(Sender: TObject);
begin
  inherited;
  Calendar.Date := FloatValue;
end;

procedure TrwDateTimePropertyEditor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FloatValue := Calendar.Date;
end;

function TrwDateTimePropertyEditor.ShowModal: Integer;
begin
  if not btnOK.Enabled then
    Result := mrCancel
  else
    Result := inherited ShowModal;
end;

procedure TrwDateTimePropertyEditor.CalendarDblClick(Sender: TObject);
begin
  btnOK.Click;
end;

initialization
  RegisterClass(TrwDateTimePropertyEditor);

end.
 