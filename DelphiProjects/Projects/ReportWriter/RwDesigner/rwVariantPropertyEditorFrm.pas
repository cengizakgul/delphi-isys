unit rwVariantPropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rwPropertyEditorFrm, StdCtrls, ExtCtrls;

type
  TrwVariantPropertyEditor = class(TrwPropertyEditor)
    rgType: TRadioGroup;
    Label1: TLabel;
    edString: TMemo;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure SetReadOnly; override;
  end;


implementation

{$R *.dfm}

{ TrwVariantPropertyEditor }

procedure TrwVariantPropertyEditor.SetReadOnly;
begin
  inherited;
  edString.ReadOnly := True;
  rgType.Enabled := False;
end;

procedure TrwVariantPropertyEditor.FormShow(Sender: TObject);
begin
  inherited;

  case VarType(VariantValue) of
    varEmpty,
    varNull:     rgType.ItemIndex := 6;

    varShortInt,
    varSmallint,
    varInteger,
    varByte,
    varWord,
    varLongWord,
    varInt64:    rgType.ItemIndex := 4;
    varSingle,
    varDouble:   rgType.ItemIndex := 3;

    varCurrency: rgType.ItemIndex := 1;

    varDate:     rgType.ItemIndex := 2;

    varBoolean:  rgType.ItemIndex := 0;

    varOleStr,
    varString:   rgType.ItemIndex := 5;
  else
    raise Exception.Create('Type is not supported');
  end;

  edString.Text := VarToStr(VariantValue);
end;

procedure TrwVariantPropertyEditor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if ModalResult = mrOK then
    case rgType.ItemIndex of
      0:  VariantValue := Boolean(StrToInt(Trim(edString.Text)));
      1:  VariantValue := StrToCurr(Trim(edString.Text));
      2:  VariantValue := StrToDateTime(Trim(edString.Text));
      3:  VariantValue := StrToFloat(Trim(edString.Text));
      4:  VariantValue := StrToInt(Trim(edString.Text));
      6:  VariantValue := Null;
    else
      VariantValue := edString.Text;
    end;
end;

initialization
  RegisterClass(TrwVariantPropertyEditor);

end.
