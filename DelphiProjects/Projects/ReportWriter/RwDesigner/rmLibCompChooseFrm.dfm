object rmLibCompChoose: TrmLibCompChoose
  Left = 397
  Top = 221
  Width = 463
  Height = 400
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'Library Components'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    455
    373)
  PixelsPerInch = 96
  TextHeight = 13
  object tbcCategories: TTabControl
    Left = 0
    Top = 0
    Width = 455
    Height = 373
    Align = alClient
    TabOrder = 2
    OnChange = tbcCategoriesChange
    DesignSize = (
      455
      373)
    object lDescription: TLabel
      Left = 6
      Top = 215
      Width = 74
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Item description'
    end
    object lGreeting: TLabel
      Left = 6
      Top = 30
      Width = 40
      Height = 13
      Caption = 'Greeting'
    end
    object lvLibComp: TListView
      Left = 5
      Top = 47
      Width = 444
      Height = 148
      Anchors = [akLeft, akTop, akRight, akBottom]
      Columns = <>
      HideSelection = False
      IconOptions.AutoArrange = True
      LargeImages = imlPictures
      ReadOnly = True
      SortType = stText
      TabOrder = 0
      OnDblClick = lvLibCompDblClick
      OnSelectItem = lvLibCompSelectItem
    end
    object memDescription: TMemo
      Left = 6
      Top = 231
      Width = 443
      Height = 98
      Anchors = [akLeft, akRight, akBottom]
      ReadOnly = True
      TabOrder = 1
    end
    object tvLibComp: TTreeView
      Left = 245
      Top = 6
      Width = 44
      Height = 35
      Indent = 19
      TabOrder = 2
      Visible = False
    end
  end
  object btnOK: TButton
    Left = 284
    Top = 340
    Width = 75
    Height = 23
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 371
    Top = 340
    Width = 75
    Height = 23
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object imlPictures: TImageList
    Height = 32
    Width = 32
    Left = 208
    Top = 104
  end
end
