unit rmTrmXMLchoicePropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmXMLNodePropFrm, rmOPEExpressionFrm,
  rmOPEBlockingControlFrm, StdCtrls, ExtCtrls, rmOPEBooleanFrm,
  rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, ComCtrls,
  rmOPEChoiceControlFrm;

type
  TrmTrmXMLchoiceProp = class(TrmTrmXMLNodeProp)
    frmProduceBranchIf: TrmOPEBlockingControl;
    Panel1: TPanel;
    Label3: TLabel;
    frmChoiceControl: TrmOPEChoiceControl;
  private
  public
    procedure GetPropsFromObject; override;
  end;


implementation

{$R *.dfm}

{ TrmTrmXMLNodeProp1 }

procedure TrmTrmXMLchoiceProp.GetPropsFromObject;
begin
  inherited;
  frmProduceBranchIf.BlockID := 'BranchCalc';
  frmProduceBranchIf.ShowPropValue;
end;

end.
 