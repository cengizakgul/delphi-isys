unit rwColorPropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwPropertyEditorFrm, StdCtrls;

type
  TrwColorPropertyEditor = class(TrwPropertyEditor)
    ColorDialog: TColorDialog;
  private
  public
    function ShowModal: Integer; override;
  end;

implementation

{$R *.DFM}

function TrwColorPropertyEditor.ShowModal: Integer;
begin
  ColorDialog.Color := OrdValue;

  if btnOK.Enabled and ColorDialog.Execute then
  begin
    Result := mrOK;
    OrdValue := ColorDialog.Color;
  end
  else
    Result := mrCancel;
end;

initialization
  RegisterClass(TrwColorPropertyEditor);

end.
