unit rmTrmlGRFieldValuesPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmlCustomControlPropFrm, StdCtrls, ExtCtrls,
  rmOPEReportParamFrm, rmOPEColorFrm, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls, rmOPEEnumFrm, rwDataDictionary,
  rwUtils, rwDsgnUtils, rwEngineTypes;

type
  TrmTrmlGRFieldValuesProp = class(TrmTrmlCustomControlProp)
    grbDDInfo: TGroupBox;
    frmTable: TrmOPEEnum;
    frmField: TrmOPEEnum;
    frmColumnTitle: TrmOPEStringSingleLine;
    procedure frmTablecbListChange(Sender: TObject);
  private
    procedure PrepareTableList;
    procedure PrepareFieldList(const ATable: String);
  protected
    procedure AssignParmType; override;
    procedure GetPropsFromObject; override;
  public
  end;

implementation

{$R *.dfm}

{ TrmTrmlCustomControlProp4 }

procedure TrmTrmlGRFieldValuesProp.GetPropsFromObject;
begin
  inherited;

  PrepareTableList;

  frmTable.PropertyName := 'ddTableName';
  frmField.PropertyName := 'ddFieldName';
  frmTable.ShowPropValue;
  frmTable.cbList.OnChange(nil);
  frmField.ShowPropValue;
  frmColumnTitle.PropertyName := 'ColumnTitle';
  frmColumnTitle.ShowPropValue;
end;

procedure TrmTrmlGRFieldValuesProp.PrepareFieldList(const ATable: String);
var
  ValList: TStringList;
  i: Integer;
  h: String;
  Tbl: TrwDataDictTable;
begin
  Tbl := TrwDataDictTable(DataDictionary.Tables.TableByName(ATable));

  if not Assigned(Tbl) then
    Exit;

  ValList := TStringList.Create;
  try
    for i := 0 to Tbl.Fields.Count - 1 do
    begin
      if Tbl.Fields[i].FieldValues.Text = '' then
        Continue;

      if DesignRMAdvancedMode then
        h := Tbl.Fields[i].Name
      else
      begin
        h := Tbl.Fields[i].DisplayName;
        if h = '' then
          Continue;
      end;

      h := Tbl.Fields[i].Name + '=' + h;
      ValList.Add(h);
    end;

    frmField.Values := ValList;
  finally
    FreeAndNil(ValList);
  end;
end;

procedure TrmTrmlGRFieldValuesProp.PrepareTableList;
var
  ValList: TStringList;
  i: Integer;
  h: String;
begin
  ValList := TStringList.Create;
  try
    for i := 0 to DataDictionary.Tables.Count - 1 do
    begin
      if DesignRMAdvancedMode then
        h := DataDictionary.Tables[i].Name
      else
      begin
        h := DataDictionary.Tables[i].DisplayName;
        if h = '' then
          Continue;
      end;

      h := DataDictionary.Tables[i].Name + '=' + h;
      ValList.Add(h);
    end;

    frmTable.Values := ValList;
  finally
    FreeAndNil(ValList);
  end;
end;

procedure TrmTrmlGRFieldValuesProp.frmTablecbListChange(Sender: TObject);
begin
  inherited;
  frmTable.cbListChange(Sender);
  PrepareFieldList(frmTable.NewPropertyValue);
end;

procedure TrmTrmlGRFieldValuesProp.AssignParmType;
begin
  frmParam1.ParamType := rwvArray;
end;

end.
                