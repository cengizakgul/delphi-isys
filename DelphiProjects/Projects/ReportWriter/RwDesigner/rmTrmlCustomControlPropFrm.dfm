inherited rmTrmlCustomControlProp: TrmTrmlCustomControlProp
  Height = 171
  inherited pcCategories: TPageControl
    Height = 171
    inherited tcAppearance: TTabSheet
      TabVisible = True
      inline frmColor: TrmOPEColor
        Left = 10
        Top = 10
        Width = 75
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
      end
    end
    object tsParam: TTabSheet [2]
      Caption = 'Parameters'
      ImageIndex = 2
      inline frmParam1: TrmOPEReportParam
        Left = 8
        Top = 10
        Width = 290
        Height = 82
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
        inherited edParamName: TEdit
          Width = 200
        end
        inherited edParamDescription: TEdit
          Width = 199
        end
      end
    end
  end
end
