unit rmTBMDIChildFrameFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmLocalToolBarFrm, ImgList, ActnList, Menus, ExtCtrls, rwDsgnUtils,
  rmDsgnTypes, ToolWin, ComCtrls;

type
  TrmTBMDIChildFrame = class(TrmLocalToolBar)
    miWindows: TMenuItem;
    miWindowsSeparator1: TMenuItem;
    miCloseWindow: TMenuItem;
    actCloseWindow: TAction;
    tlbMenu: TToolBar;
    procedure actCloseWindowExecute(Sender: TObject);
    procedure miWindowsClick(Sender: TObject);
  private
    procedure UpdateWindowsList;
    procedure FOnWindowsMenuClick(Sender: TObject);
  public
    procedure Initialize; override;
    procedure AfterConstruction; override;    
  end;

  TrmdWindowMenuItem = class(TMenuItem)
  private
    FChildFrame: TFrame;
  end;


implementation

{$R *.dfm}

{ TrmTBMDIChildFrame }

procedure TrmTBMDIChildFrame.FOnWindowsMenuClick(Sender: TObject);
begin
  Application.ProcessMessages;
  RMDesigner.ShowApartment(TrmdWindowMenuItem(Sender).FChildFrame as IrmDsgnApartment);
end;

procedure TrmTBMDIChildFrame.Initialize;
begin
  inherited;
  UpdateWindowsList;
end;

procedure TrmTBMDIChildFrame.UpdateWindowsList;
var
  L: TrmdApartmentList;
  i: Integer;
  MI: TrmdWindowMenuItem;
begin
  L := RMDesigner.GetApartmentList;

  while miWindows.Items[0] <> miWindowsSeparator1 do
    miWindows.Items[0].Free;

  for i := Low(L) to High(L) do
  begin
    MI := TrmdWindowMenuItem.Create(Self);
    MI.Caption := L[i].GetCaption;
    MI.Checked := L[i].RealObject = RMDesigner.GetActiveApartment.RealObject;
    MI.FChildFrame := L[i].RealObject;
    MI.OnClick := FOnWindowsMenuClick;
    miWindows.Insert(miWindows.IndexOf(miWindowsSeparator1), MI);
  end;
end;

procedure TrmTBMDIChildFrame.actCloseWindowExecute(Sender: TObject);
begin
  RMDesigner.DestroyCurrentApartment;
end;

procedure TrmTBMDIChildFrame.miWindowsClick(Sender: TObject);
begin
  UpdateWindowsList;
end;

procedure TrmTBMDIChildFrame.AfterConstruction;
begin
  inherited;
  tlbMenu.Menu := nil;
  tlbMenu.Menu := MainMenu;
end;

end.
