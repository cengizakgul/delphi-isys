inherited rmOPENumeric: TrmOPENumeric
  Width = 176
  Height = 21
  AutoSize = True
  OnResize = FrameResize
  object lPropName: TLabel
    Left = 0
    Top = 4
    Width = 39
    Height = 13
    Caption = 'Numeric'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object edNumeric: TisRWDBSpinEdit
    Left = 55
    Top = 0
    Width = 121
    Height = 21
    Increment = 1.000000000000000000
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 0
    UnboundDataType = wwDefault
    OnChange = edNumericChange
  end
end
