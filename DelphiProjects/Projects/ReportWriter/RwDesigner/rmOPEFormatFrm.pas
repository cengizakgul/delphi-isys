unit rmOPEFormatFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls;

type
  TrmOPEFormatType = (rmFTAll, rmFTNumeric, rmFTCurrency, rmFTDate);

  TrmOPEFormat = class(TrmObjPropertyEditor)
    lPropName: TLabel;
    cbFormat: TComboBox;
    procedure FrameResize(Sender: TObject);
    procedure cbFormatChange(Sender: TObject);
  private
    FFormatType: TrmOPEFormatType;
    procedure SetFormatType(const Value: TrmOPEFormatType);
  protected
    function  GetDefaultPropertyName: String; override;
  public
    procedure ShowPropValue; override;
    function  NewPropertyValue: Variant; override;    
    property  FormatType: TrmOPEFormatType read FFormatType write SetFormatType;
  end;

implementation

{$R *.dfm}

const
  cFormats: array [rmFTNumeric..rmFTDate] of String =
      ('0.00'#13 +
       '#,##0.00'#13 +
       '##.##'#13 +
       '#,###.##',

       '0.00'#13 +
       '#,##0.00',

       'mm/dd/yyyy'#13 +
       'mm/dd/yy'#13 +
       'mmm dd, yyyy'#13 +
       'mmmm dd, yyyy'#13 +
       'ddd, mmmm dd, yyyy'
      );

{ TrmOPEFormat }

function TrmOPEFormat.GetDefaultPropertyName: String;
begin
  Result := 'Format';
end;

procedure TrmOPEFormat.FrameResize(Sender: TObject);
begin
  inherited;
  cbFormat.Width := ClientWidth - cbFormat.Left;
end;

procedure TrmOPEFormat.ShowPropValue;
begin
  inherited;
  cbFormat.Text := OldPropertyValue;
end;

procedure TrmOPEFormat.SetFormatType(const Value: TrmOPEFormatType);
var
  h: String;
begin
  FFormatType := Value;

  if Value = rmFTAll then
    h := cFormats[rmFTNumeric] + #13 + cFormats[rmFTDate]
  else
    h := cFormats[FFormatType];

  cbFormat.Items.Text := h;
end;

function TrmOPEFormat.NewPropertyValue: Variant;
begin
  Result := cbFormat.Text;
end;

procedure TrmOPEFormat.cbFormatChange(Sender: TObject);
begin
  NotifyOfChanges;
end;

end.
