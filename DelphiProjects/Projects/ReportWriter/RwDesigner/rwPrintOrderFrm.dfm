object rwPrintOrder: TrwPrintOrder
  Left = 440
  Top = 207
  Width = 340
  Height = 356
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'Print Order'
  Color = clBtnFace
  Constraints.MinHeight = 220
  Constraints.MinWidth = 200
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    332
    329)
  PixelsPerInch = 96
  TextHeight = 13
  object lbControls: TListBox
    Left = 4
    Top = 3
    Width = 243
    Height = 322
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    TabOrder = 0
  end
  object btnUp: TButton
    Left = 254
    Top = 36
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Up'
    TabOrder = 2
    OnClick = btnUpClick
  end
  object btnDown: TButton
    Left = 254
    Top = 67
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Down'
    TabOrder = 3
    OnClick = btnDownClick
  end
  object btnTop: TButton
    Left = 254
    Top = 4
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Top'
    TabOrder = 1
    OnClick = btnTopClick
  end
  object btnBottom: TButton
    Left = 254
    Top = 100
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Bottom'
    TabOrder = 4
    OnClick = btnBottomClick
  end
  object btnOK: TButton
    Left = 254
    Top = 268
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 5
  end
  object btnCancel: TButton
    Left = 254
    Top = 300
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
  end
end
