unit rmOPEDBCellValueFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPEValueFrm, rmOPEDateFrm, rmOPENumericFrm, rmOPEFormatFrm,
  rmObjPropertyEditorFrm, rmOPEStringMultiLineFrm, StdCtrls, ComCtrls,
  rmOPEEnumFrm, rwQBInfo, rwReportControls, rwUtils, rwDsgnUtils,
  rmOPEEmptyFrm, rmTypes, rmOPEExpressionFrm, ExtCtrls, rmOPEFieldFrm;

type
  TrmOPEDBCellValue = class(TrmOPEValue)
    tsField: TTabSheet;
    frmFormatField: TrmOPEFormat;
    frmField: TrmOPEField;
    procedure tsFieldShow(Sender: TObject);
  private
    procedure ClearFieldInfo;  
  protected
    procedure SyncPages; override;
  public
    procedure ShowPropValue; override;
    procedure ApplyChanges; override;
    procedure AfterConstruction; override;        
  end;

implementation

{$R *.dfm}

{ TrmOPEDBCellValue }

procedure TrmOPEDBCellValue.ShowPropValue;
begin
  frmField.PropertyName := 'DataField';
  inherited;
end;

procedure TrmOPEDBCellValue.SyncPages;
begin
  if (TComponent(LinkedObject) as IrmQueryAwareObject).GetFieldName <> '' then
  begin
    pcTypes.ActivePage := tsField;
    tsField.OnShow(nil);
    PropagateShowPropValue(pcTypes.ActivePage);
  end
  else
    inherited;
end;

procedure TrmOPEDBCellValue.tsFieldShow(Sender: TObject);
begin
  frmField.RefreshFields;
end;

procedure TrmOPEDBCellValue.ClearFieldInfo;
begin
  if pcTypes.Tag = 0 then
    (TComponent(LinkedObject) as IrmQueryAwareObject).SetFieldName('');
end;

procedure TrmOPEDBCellValue.ApplyChanges;
begin
  if pcTypes.ActivePage <> tsField then
    ClearFieldInfo;

  inherited;
end;

procedure TrmOPEDBCellValue.AfterConstruction;
begin
  inherited;
  frmFormatField.FormatType := rmFTAll;
end;

end.
