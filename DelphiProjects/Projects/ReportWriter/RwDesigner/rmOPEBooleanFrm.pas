unit rmOPEBooleanFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls;

type
  TrmOPEBoolean = class(TrmObjPropertyEditor)
    chbProp: TCheckBox;
    procedure chbPropClick(Sender: TObject);
  private
    FInvertedValue: Boolean;
  protected
  public
    procedure ShowPropValue; override;
    function  NewPropertyValue: Variant; override;
    property  InvertedValue: Boolean read FInvertedValue write FInvertedValue;
  end;

implementation

{$R *.dfm}

{ TrmOPEBoolean }

function TrmOPEBoolean.NewPropertyValue: Variant;
begin
  Result := chbProp.Checked;
  if InvertedValue then
    Result := not Result;
end;

procedure TrmOPEBoolean.ShowPropValue;
begin
  inherited;
  try
    chbProp.Checked := OldPropertyValue;
    if InvertedValue then
      chbProp.Checked := not chbProp.Checked;
  except
    chbProp.Checked := False;
  end;
end;

procedure TrmOPEBoolean.chbPropClick(Sender: TObject);
begin
  NotifyOfChanges;
end;

end.
 