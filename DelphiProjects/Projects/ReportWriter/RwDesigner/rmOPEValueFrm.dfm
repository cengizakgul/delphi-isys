inherited rmOPEValue: TrmOPEValue
  Width = 291
  Height = 131
  object pcTypes: TPageControl
    Left = 0
    Top = 0
    Width = 291
    Height = 131
    ActivePage = tsEmpty
    Align = alClient
    Style = tsButtons
    TabOrder = 0
    OnChange = pcTypesChange
    object tsEmpty: TTabSheet
      Caption = 'Empty'
      inline frmEmpty: TrmOPEEmpty
        Left = 0
        Top = 0
        Width = 283
        Height = 100
        Align = alClient
        AutoScroll = False
        TabOrder = 0
        inherited Label1: TLabel
          Width = 283
          Height = 100
        end
      end
    end
    object tsText: TTabSheet
      Caption = 'Text'
      ImageIndex = 1
      object Label2: TLabel
        Left = 0
        Top = 0
        Width = 283
        Height = 15
        Align = alTop
        AutoSize = False
        Caption = 'Text'
      end
      inline frmText: TrmOPEStringMultiLine
        Left = 0
        Top = 15
        Width = 283
        Height = 85
        Align = alClient
        AutoScroll = False
        TabOrder = 0
        inherited mString: TMemo
          Width = 283
          Height = 85
        end
      end
    end
    object tsNumber: TTabSheet
      Caption = 'Number'
      ImageIndex = 2
      inline frmFormatNumber: TrmOPEFormat
        Left = 0
        Top = 10
        Width = 222
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
        inherited cbFormat: TComboBox
          Width = 167
        end
      end
      inline frmNumber: TrmOPENumeric
        Left = 0
        Top = 41
        Width = 222
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited edNumeric: TisRWDBSpinEdit
          Width = 167
        end
      end
    end
    object tsDateTime: TTabSheet
      Caption = 'Date'
      ImageIndex = 4
      inline frmFormatDate: TrmOPEFormat
        Left = 0
        Top = 10
        Width = 222
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
        inherited cbFormat: TComboBox
          Width = 167
        end
      end
      inline frmDate: TrmOPEDate
        Left = 0
        Top = 41
        Width = 223
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited deDate: TDateTimePicker
          Width = 168
        end
      end
    end
    object tsField: TTabSheet
      Caption = 'Field'
      ImageIndex = 5
      OnShow = tsFieldShow
      inline frmField: TrmOPEField
        Left = 0
        Top = 10
        Width = 271
        Height = 51
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
        inherited cbDataSource: TisRWDBComboBox
          Width = 216
        end
        inherited cbField: TisRWDBComboBox
          Width = 216
        end
      end
      inline frmFormatField: TrmOPEFormat
        Left = 0
        Top = 73
        Width = 222
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited cbFormat: TComboBox
          Width = 167
        end
      end
    end
    object tsCalc: TTabSheet
      Caption = 'Calc'
      ImageIndex = 4
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 283
        Height = 28
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        inline frmFormatCalc: TrmOPEFormat
          Left = 0
          Top = 3
          Width = 222
          Height = 21
          AutoScroll = False
          AutoSize = True
          TabOrder = 0
          inherited cbFormat: TComboBox
            Width = 167
          end
        end
      end
      inline frmExpression: TrmOPEExpression
        Left = 0
        Top = 28
        Width = 283
        Height = 72
        Align = alClient
        AutoScroll = False
        TabOrder = 1
        inherited frmExpression: TrmFMLExprPanel
          Width = 283
          Height = 72
          inherited pnlCanvas: TPanel
            Width = 283
            Height = 72
          end
        end
      end
    end
  end
end
