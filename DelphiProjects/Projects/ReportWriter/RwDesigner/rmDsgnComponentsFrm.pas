unit rmDsgnComponentsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDsgnAreaFrm, ExtCtrls, ImgList, ComCtrls, rmTypes, rwEngineTypes, rmDsgnTypes,
  StdCtrls, Menus, rmLibCompChooseFrm, rwEngine, rwUtils, ExtDlgs, rwDsgnUtils,
  rmCustomFrameFrm, EvStreamUtils, rwDsgnRes, rwBasicUtils, rwRTTI;

type
  TrmdTreeNodeComps = class;
  
  TrmDsgnComponents = class(TrmDsgnArea)
    tvObjects: TTreeView;
    ImageList: TImageList;
    Splitter1: TSplitter;
    pnlComponent: TPanel;
    Label1: TLabel;
    lClassNameTxt: TLabel;
    edClassName: TEdit;
    memDescription: TMemo;
    Label2: TLabel;
    edDisplayName: TEdit;
    Label3: TLabel;
    gbPalette: TGroupBox;
    chbShowOnPalette: TCheckBox;
    chbAdvancedMode: TCheckBox;
    pnlCompPictureEdit: TPanel;
    imgCompPictEdit: TImage;
    Label4: TLabel;
    btnLoadIcon: TButton;
    btnSaveIcon: TButton;
    btnEditComp: TButton;
    btnDeleteComp: TButton;
    Label5: TLabel;
    edAncestor: TEdit;
    pmLibTree: TPopupMenu;
    miNewComp: TMenuItem;
    miInputFormControl: TMenuItem;
    InputFormAncestor1: TMenuItem;
    ext1: TMenuItem;
    Panel2: TMenuItem;
    EditBox1: TMenuItem;
    DateTimeEdit1: TMenuItem;
    CheckBox1: TMenuItem;
    GroupBox1: TMenuItem;
    RadioGroup1: TMenuItem;
    Combobox1: TMenuItem;
    DBComboBox1: TMenuItem;
    DBGrid1: TMenuItem;
    PageControl1: TMenuItem;
    MenuItem1: TMenuItem;
    miPrintFormControl: TMenuItem;
    PrintForm1: TMenuItem;
    PrintText1: TMenuItem;
    Panel1: TMenuItem;
    Image1: TMenuItem;
    Shape1: TMenuItem;
    Table1: TMenuItem;
    TableCell1: TMenuItem;
    DBTable1: TMenuItem;
    miASCIIFormControl: TMenuItem;
    miASCIIFormAncestor: TMenuItem;
    miASCIIRecord: TMenuItem;
    miASCIIField: TMenuItem;
    miNonVisualComp: TMenuItem;
    miReportAncestor: TMenuItem;
    Query2: TMenuItem;
    Buffer1: TMenuItem;
    InheritFromLibrary1: TMenuItem;
    miSplitter1: TMenuItem;
    miAddLibGroup: TMenuItem;
    miDeleteLibGroup: TMenuItem;
    XMLForm1: TMenuItem;
    pnlComponents: TPanel;
    Label6: TLabel;
    pnlGroup: TPanel;
    Label7: TLabel;
    opdPicture: TOpenPictureDialog;
    svdPicture: TSavePictureDialog;
    pmPict: TPopupMenu;
    miLoadPict: TMenuItem;
    miSavePict: TMenuItem;
    N4: TMenuItem;
    miClearPict: TMenuItem;
    chbComponentEditMode: TCheckBox;
    miXMLFormAncestor: TMenuItem;
    miXMLSimpleTypes: TMenuItem;
    miXMLSimpleType: TMenuItem;
    miXMLSimpleTypeList: TMenuItem;
    miXMLSimpleTypeUnion: TMenuItem;
    miXMLAttribute: TMenuItem;
    miXMLElement: TMenuItem;
    miXMLGroup: TMenuItem;
    miXMLAttributeGroup: TMenuItem;
    btnDeleteGroup: TButton;
    Label8: TLabel;
    edGroupName: TEdit;
    btnAddGroup: TButton;
    miDeleteComponent: TMenuItem;
    procedure miReportAncestorClick(Sender: TObject);
    procedure tvObjectsChange(Sender: TObject; Node: TTreeNode);
    procedure edClassNameExit(Sender: TObject);
    procedure edClassNameKeyPress(Sender: TObject; var Key: Char);
    procedure edDisplayNameExit(Sender: TObject);
    procedure memDescriptionExit(Sender: TObject);
    procedure miLoadPictClick(Sender: TObject);
    procedure miSavePictClick(Sender: TObject);
    procedure miClearPictClick(Sender: TObject);
    procedure btnEditCompClick(Sender: TObject);
    procedure chbShowOnPaletteClick(Sender: TObject);
    procedure tvObjectsCompare(Sender: TObject; Node1, Node2: TTreeNode;
      Data: Integer; var Compare: Integer);
    procedure tvObjectsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure tvObjectsEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure miAddLibGroupClick(Sender: TObject);
    procedure tvObjectsChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure edGroupNameExit(Sender: TObject);
    procedure miDeleteLibGroupClick(Sender: TObject);
    procedure miDeleteComponentClick(Sender: TObject);
  private
    FUpdating: Boolean;

    function  GetTopNode: TrmdTreeNodeComps;
    function  SelectedComponent: IrmComponent;
    procedure UpdateCurrentItem;
    procedure AddComp(AClassName: String);
    procedure SetCompsDescription;
    procedure SetCompDescription;
    procedure SetGroupDescription;
    procedure NotifyOwnerAboutChanges;

    procedure DMRefreshComponents(var Message: TrmMessageRec); message mRefreshComponents;

  protected
    procedure Initialize; override;
  public
    constructor Create(AOwner: TComponent); override;
    function CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject; override;
  end;


  TrmdTreeNodeComp = class(TTreeNode)
  private
    FHandle:  IrmComponent;
    procedure Update;
    procedure UpdateGroupInfo;
  end;


  TrmdTreeNodeGroup = class(TTreeNode)
  private
    procedure UpdateGroupInfo;
    procedure CleanupFromComponents;
  end;


  TrmdTreeNodeComps = class(TTreeNode)
  private
    FHandle:  IrmComponents;
    function  AddComponentNode(const AComp: IrmComponent): TrmdTreeNodeComp;
    function  AddGroupNode(const AGroupName: String; AParentNode: TTreeNode): TrmdTreeNodeGroup;
    procedure Update;
  end;


implementation

uses rmDsgnLocalComponentFrm;

{$R *.dfm}


{ TrmdTreeNodeComps }

function TrmdTreeNodeComps.AddComponentNode(const AComp: IrmComponent): TrmdTreeNodeComp;
var
  Npar: TTreeNode;

  function CheckBranch(AGroup: String): TTreeNode;
  var
    h: String;
    i: Integer;
  begin
    Result := Self;

    while AGroup <> '' do
    begin
      h := GetNextStrValue(AGroup, '\');
      if Assigned(Result) then
      begin
        for i := 0 to Result.Count - 1 do
          if SameText(Result[i].Text, h) then
          begin
            Result := Result[i];
            h := '';
            break;
          end;
      end
      else
      begin
        for i := 0 to Owner.Count - 1 do
          if (Owner[i].Level = 0) and SameText(Owner[i].Text, h) then
          begin
            Result := Owner[i];
            h := '';
            break;
          end;
      end;

      if h <> '' then
        Result := AddGroupNode(h, Result);
    end;
  end;

begin
  Npar := CheckBranch(AComp.GetGroup);

  Result := TrmdTreeNodeComp.Create(Owner);
  Owner.AddNode(Result, Npar, '', nil, naAddChild);
  Result.FHandle := AComp;
  Result.Update;
end;

function TrmdTreeNodeComps.AddGroupNode(const AGroupName: String; AParentNode: TTreeNode): TrmdTreeNodeGroup;
begin
  Result := TrmdTreeNodeGroup.Create(Owner);
  Owner.AddNode(Result, AParentNode, AGroupName, nil, naAddChild);
end;

procedure TrmdTreeNodeComps.Update;
var
  i: Integer;
begin
  Text := 'Components';
  ImageIndex := 0;
  StateIndex := ImageIndex;
  SelectedIndex := ImageIndex;

  DeleteChildren;

  for i := 0 to FHandle.GetItemCount - 1 do
    AddComponentNode(FHandle.GetItemByIndex(i) as IrmComponent);
end;

{ TrmdTreeNodeComp }

procedure TrmdTreeNodeComp.Update;
begin
  Text := FHandle.GetName;
  if FHandle.GetDisplayName <> '' then
    Text := Text + ' (' + FHandle.GetDisplayName + ')';

  if FHandle.IsInheritedItem then
    ImageIndex := 2
  else
    ImageIndex := 1;

  StateIndex := ImageIndex;
  SelectedIndex := ImageIndex;
end;


procedure TrmdTreeNodeComp.UpdateGroupInfo;
var
  N: TTreeNode;
  Grp: String;
begin
  Grp := '';

  N := Parent;

  while N is TrmdTreeNodeGroup do
  begin
    if Grp <> '' then
      Grp := '\' + Grp;
    Grp := N.Text + Grp;
    N := N.Parent;
  end;

  FHandle.SetGroup(Grp);
end;


{ TrmDsgnComponents }

procedure TrmDsgnComponents.AddComp(AClassName: String);
var
  LibComp, NewComp: IrmComponent;
  Comp: IrmDesignObject;
  Grp: String;
  Cl: IrwClass;
begin
  if SameText(AClassName, 'LIBRARY') then
  begin
    LibComp := ChoiceLibComp(['TrwComponent'], DesignObject.GetObjectOwner, Self);
    if Assigned(LibComp) then
      AClassName := LibComp.GetName
    else
      Exit;
  end
  else
  begin
    AClassName := RWEngineExt.AppAdapter.SubstDefaultClassName(AClassName);
    Cl := rwRTTIInfo.FindClass(AClassName);
    if Assigned(Cl) and Cl.IsUserClass then
      LibComp := nil
    else
      LibComp := Cl.GetUserClass;
  end;

  NewComp := GetTopNode.FHandle.AddItem as IrmComponent;
  NewComp.SetClassName('TrmlNewComponent');
  NewComp.SetDisplayName('New Component');

  Comp := CreateRWComponentByClass(AClassName);
  Comp.InitializeForDesign;
  NewComp.SetComponentData(Comp);

  if tvObjects.Selected is TrmdTreeNodeGroup then
    Grp := TrmdTreeNodeGroup(tvObjects.Selected).Text
  else if tvObjects.Selected is TrmdTreeNodeComp then
    Grp := TrmdTreeNodeComp(tvObjects.Selected).FHandle.GetGroup
  else
    Grp := '';  

  NewComp.SetGroup(Grp);

  Comp := nil;

  GetTopNode.AddComponentNode(NewComp).Selected := True;
end;

constructor TrmDsgnComponents.Create(AOwner: TComponent);
begin
  inherited;
  FImageIndex := 2;
  FAreaType := rmDATInternalObject;
end;

function TrmDsgnComponents.CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject;
begin
// do nothing
end;

function TrmDsgnComponents.GetTopNode: TrmdTreeNodeComps;
begin
  Result := TrmdTreeNodeComps(tvObjects.Items[0]);
end;

procedure TrmDsgnComponents.Initialize;
var
  Npar: TrmdTreeNodeComps;
begin
  inherited;

  tvObjects.Items.BeginUpdate;
  try
    tvObjects.Items.Clear;

     Npar := TrmdTreeNodeComps.Create(tvObjects.Items);
     tvObjects.Items.AddNode(Npar, nil, '', nil, naAdd);
     Npar.FHandle := (DesignObject.RealObject as IrmVarFunctHolder).GetLocalComponents;
     Npar.Update;

     tvObjects.SortType := stNone;
     tvObjects.SortType := stText;

    GetTopNode.Expand(False);
  finally
     tvObjects.Items.EndUpdate;
  end;

  GetTopNode.Selected := True;

  btnAddGroup.Enabled := (Apartment as IrmDsgnComponent).GetSecurityInfo.EditComponents;
  btnDeleteGroup.Enabled := btnAddGroup.Enabled;
  miAddLibGroup.Enabled := btnAddGroup.Enabled;
  miDeleteLibGroup.Enabled := btnAddGroup.Enabled;
  miNewComp.Enabled := btnAddGroup.Enabled;
  btnDeleteComp.Enabled := btnAddGroup.Enabled;
  miDeleteComponent.Enabled := btnAddGroup.Enabled;
end;

procedure TrmDsgnComponents.miReportAncestorClick(Sender: TObject);
begin
  AddComp(TMenuItem(Sender).Hint);
end;

procedure TrmDsgnComponents.tvObjectsChange(Sender: TObject; Node: TTreeNode);
begin
  if Assigned(Node) then
  begin
    FUpdating := True;
    try
      if Node is TrmdTreeNodeComps then
        SetCompsDescription
      else if Node is TrmdTreeNodeComp then
        SetCompDescription
      else
        SetGroupDescription;
    finally
      FUpdating := False;
    end;
  end;
end;

procedure TrmDsgnComponents.SetCompDescription;
var
  DsgnPalette: TrwDsgnPaletteSettings;
  fl: Boolean;
begin
  edClassName.Text := SelectedComponent.GetClassName;
  edAncestor.Text := SelectedComponent.GetAncestorClassName;
  edDisplayName.Text := SelectedComponent.GetDisplayName;
  memDescription.Text := SelectedComponent.GetDescription;

  SelectedComponent.GetPicture(imgCompPictEdit.Picture.Bitmap);

  DsgnPalette := SelectedComponent.GetDsgnPaletteSettings;

  chbAdvancedMode.Checked := rwDPSAdvancedModeOnly in DsgnPalette;
  chbComponentEditMode.Checked := rwDPSComponentEditModeOnly in DsgnPalette;

  chbShowOnPalette.Enabled := not (SelectedComponent.IsInheritsFrom('TrmTableCell') or
                                   SelectedComponent.IsInheritsFrom('TrmReport') or
                                   SelectedComponent.IsInheritsFrom('TrmPrintForm') or
                                   SelectedComponent.IsInheritsFrom('TrmInputForm') or
                                   SelectedComponent.IsInheritsFrom('TrmASCIIForm') or
                                   SelectedComponent.IsInheritsFrom('TrmXMLForm'));

  chbShowOnPalette.Checked := chbShowOnPalette.Enabled and (rwDPSShowOnPalette in DsgnPalette);

  fl := not SelectedComponent.IsInheritedItem and (Apartment as IrmDsgnComponent).GetSecurityInfo.EditComponents;

  edClassName.Enabled := fl;
  edDisplayName.Enabled := fl;
  memDescription.Enabled := fl;
  chbShowOnPalette.Enabled := fl;
  chbAdvancedMode.Enabled := fl;
  chbComponentEditMode.Enabled := fl;
  btnLoadIcon.Enabled := fl;
  miLoadPict.Enabled := fl;
  miClearPict.Enabled := fl;
  btnDeleteComp.Enabled := fl;
  miDeleteComponent.Enabled := fl;

  pnlComponent.Show;
  pnlComponents.Hide;
  pnlGroup.Hide;
end;

procedure TrmDsgnComponents.SetCompsDescription;
begin
  pnlComponents.Show;
  pnlComponent.Hide;
  pnlGroup.Hide;
end;

procedure TrmDsgnComponents.SetGroupDescription;
var
  GrpNode: TrmdTreeNodeGroup;
begin
  GrpNode := tvObjects.Selected as TrmdTreeNodeGroup;
  edGroupName.Text := GrpNode.Text;

  edGroupName.Enabled := (Apartment as IrmDsgnComponent).GetSecurityInfo.EditComponents;

  pnlGroup.Show;
  pnlComponent.Hide;
  pnlComponents.Hide;
end;

procedure TrmDsgnComponents.edClassNameExit(Sender: TObject);
var
  OldName: String;
begin
  OldName := SelectedComponent.GetClassName;

  if OldName <> edClassName.Text then
  begin
    SelectedComponent.SetClassName(edClassName.Text);
    UpdateCurrentItem;
    NotifyOwnerAboutChanges;

    DsgnRes.UpdateClassName(OldName, SelectedComponent.GetClassName);
  end;
end;

procedure TrmDsgnComponents.edClassNameKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    TEdit(Sender).OnExit(nil);
end;

procedure TrmDsgnComponents.edDisplayNameExit(Sender: TObject);
begin
  if SelectedComponent.GetDisplayName <> edDisplayName.Text then
  begin
    SelectedComponent.SetDisplayName(edDisplayName.Text);
    UpdateCurrentItem;
    NotifyOwnerAboutChanges;
  end;    
end;

procedure TrmDsgnComponents.memDescriptionExit(Sender: TObject);
begin
  if SelectedComponent.GetDescription <> memDescription.Text then
  begin
    SelectedComponent.SetDescription(memDescription.Text);
    UpdateCurrentItem;
    NotifyOwnerAboutChanges;
  end;    
end;

procedure TrmDsgnComponents.miLoadPictClick(Sender: TObject);
var
  F: TEvFileStream;
begin
  if opdPicture.Execute then
  begin
    F := TEvFileStream.Create(opdPicture.FileName, fmOpenRead);
    try
      SelectedComponent.SetPictureData(F);
      NotifyOwnerAboutChanges;
    finally
      FreeAndNil(F);
    end;

    SelectedComponent.GetPicture(imgCompPictEdit.Picture.Bitmap);
    UpdateCurrentItem;

    DsgnRes.UpdateClassPicture(SelectedComponent.GetClassName, imgCompPictEdit.Picture.Bitmap);
  end;
end;

procedure TrmDsgnComponents.miSavePictClick(Sender: TObject);
begin
  if svdPicture.Execute then
    imgCompPictEdit.Picture.SaveToFile(svdPicture.FileName);
end;

procedure TrmDsgnComponents.miClearPictClick(Sender: TObject);
begin
  SelectedComponent.SetPictureData(nil);
  imgCompPictEdit.Picture.Bitmap.Assign(nil);
  UpdateCurrentItem;
  DsgnRes.UpdateClassPicture(SelectedComponent.GetClassName, imgCompPictEdit.Picture.Bitmap);  
end;

procedure TrmDsgnComponents.btnEditCompClick(Sender: TObject);
var
  ComponentEditor: IrmDsgnComponent;
begin
  ComponentEditor := RMDesigner.CreateApartment(TrmDsgnLocalComponent) as IrmDsgnComponent;
  try
    ComponentEditor.LoadComponentFromStream(SelectedComponent.GetComponentData.RealStream);
    if RMDesigner.ShowModalApartment(ComponentEditor as IrmDsgnApartment) = mrOK then
      if SelectedComponent.IsInheritedItem then
        raise ErwException.Create('Inherited component cannot be changed')

      else if not (Apartment as IrmDsgnComponent).GetSecurityInfo.EditComponents then
        raise ErwException.Create('You do not have permission to complete operation')

      else
      begin
        SelectedComponent.SetComponentData(ComponentEditor.DesigningObject);
        UpdateCurrentItem;
        SetCompDescription;
        NotifyOwnerAboutChanges;
      end;
  finally
    ComponentEditor := nil;
    RMDesigner.DestroyCurrentApartment;
  end
end;

function TrmDsgnComponents.SelectedComponent: IrmComponent;
begin
  Result := (tvObjects.Selected as TrmdTreeNodeComp).FHandle;
end;

procedure TrmDsgnComponents.UpdateCurrentItem;
begin
  if tvObjects.Selected is TrmdTreeNodeComp then
    (tvObjects.Selected as TrmdTreeNodeComp).Update

  else if tvObjects.Selected is TrmdTreeNodeGroup then
    (tvObjects.Selected as TrmdTreeNodeGroup).UpdateGroupInfo;
end;

procedure TrmDsgnComponents.NotifyOwnerAboutChanges;
begin
  tvObjects.SortType := stNone;
  tvObjects.SortType := stText;

  SendApartmentMessage(Self, mComponentsChanged, 0, 0);
end;

procedure TrmDsgnComponents.chbShowOnPaletteClick(Sender: TObject);
var
  DsgnPalette: TrwDsgnPaletteSettings;
  ChangeDsgnPalette: TrwDsgnPaletteSettings;
begin
  if FUpdating then Exit;

  DsgnPalette := SelectedComponent.GetDsgnPaletteSettings;

  if Sender = chbShowOnPalette then
    ChangeDsgnPalette := [rwDPSShowOnPalette]
  else if Sender = chbAdvancedMode then
    ChangeDsgnPalette := [rwDPSAdvancedModeOnly]
  else if Sender = chbComponentEditMode then
    ChangeDsgnPalette := [rwDPSComponentEditModeOnly];

  if TCheckBox(Sender).Checked then
    DsgnPalette := DsgnPalette + ChangeDsgnPalette
  else
    DsgnPalette := DsgnPalette - ChangeDsgnPalette;

  SelectedComponent.SetDsgnPaletteSettings(DsgnPalette);
  UpdateCurrentItem;
  NotifyOwnerAboutChanges;

  PostApartmentMessage(Self, mComponentPaletteInfoChanged, SelectedComponent.GetClassName, 0);
end;

procedure TrmDsgnComponents.tvObjectsCompare(Sender: TObject; Node1, Node2: TTreeNode; Data: Integer; var Compare: Integer);
begin
  if (Node1 is TrmdTreeNodeGroup) and (Node2 is TrmdTreeNodeComp) then
    Compare := -1
  else if (Node2 is TrmdTreeNodeGroup) and (Node1 is TrmdTreeNodeComp) then
    Compare := 1
  else
    Compare := CompareStr(Node1.Text, Node2.Text);
end;

procedure TrmDsgnComponents.tvObjectsDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (Apartment as IrmDsgnComponent).GetSecurityInfo.EditComponents;
end;

procedure TrmDsgnComponents.tvObjectsEndDrag(Sender, Target: TObject; X, Y: Integer);
var
  N: TTreeNode;
begin
  if Assigned(Target) then
  begin
    N := tvObjects.GetNodeAt(X, Y);
    if Assigned(N) then
    begin
      if N is TrmdTreeNodeComp then
        tvObjects.Selected.MoveTo(N, naInsert)
      else
        tvObjects.Selected.MoveTo(N, naAddChild);

      if tvObjects.Selected is TrmdTreeNodeComp then
        TrmdTreeNodeComp(tvObjects.Selected).UpdateGroupInfo
      else if tvObjects.Selected is TrmdTreeNodeGroup then
        TrmdTreeNodeGroup(tvObjects.Selected).UpdateGroupInfo;

      NotifyOwnerAboutChanges;
    end;
  end;
end;


procedure TrmDsgnComponents.miAddLibGroupClick(Sender: TObject);
var
  N: TTReeNode;
begin
  if tvObjects.Selected is TrmdTreeNodeComp then
    N := tvObjects.Selected.Parent
  else if tvObjects.Selected is TrmdTreeNodeGroup then
    N := tvObjects.Selected
  else
    N := GetTopNode;

  GetTopNode.AddGroupNode('New Group', N).Selected := True;

  NotifyOwnerAboutChanges;  
end;


procedure TrmDsgnComponents.DMRefreshComponents(var Message: TrmMessageRec);
begin
  inherited;
  Initialize;
end;

{ TrmdTreeNodeGroup }

procedure TrmdTreeNodeGroup.CleanupFromComponents;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    if Item[i] is TrmdTreeNodeGroup then
      TrmdTreeNodeGroup(Item[i]).CleanupFromComponents;

  for i := Count - 1 downto 0 do
    if Item[i] is TrmdTreeNodeComp then
      TrmdTreeNodeComp(Item[i]).MoveTo(Parent, naAddChild);
end;

procedure TrmdTreeNodeGroup.UpdateGroupInfo;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    if Item[i] is TrmdTreeNodeGroup then
      TrmdTreeNodeGroup(Item[i]).UpdateGroupInfo
    else if Item[i] is TrmdTreeNodeComp then
      TrmdTreeNodeComp(Item[i]).UpdateGroupInfo;
  end;
end;

procedure TrmDsgnComponents.tvObjectsChanging(Sender: TObject; Node: TTreeNode; var AllowChange: Boolean);
begin
  if tvObjects.CanFocus then
    tvObjects.SetFocus;
end;

procedure TrmDsgnComponents.edGroupNameExit(Sender: TObject);
var
  GrpNode: TrmdTreeNodeGroup;
begin
  GrpNode := tvObjects.Selected as TrmdTreeNodeGroup;

  if GrpNode.Text <> edGroupName.Text then
  begin
    GrpNode.Text := edGroupName.Text;
    UpdateCurrentItem;
    NotifyOwnerAboutChanges;
  end;  
end;

procedure TrmDsgnComponents.miDeleteLibGroupClick(Sender: TObject);
var
  N: TTreeNode;
begin
  if tvObjects.Selected is TrmdTreeNodeGroup then
  begin
    tvObjects.Items.BeginUpdate;
    try
      N :=tvObjects.Selected;
      TrmdTreeNodeGroup(N).CleanupFromComponents;
      N.Parent.Selected := True;
      N.Free;
      NotifyOwnerAboutChanges;
    finally
      tvObjects.Items.EndUpdate;
    end;
  end;
end;

procedure TrmDsgnComponents.miDeleteComponentClick(Sender: TObject);
var
  i: Integer;
begin
  if (tvObjects.Selected is TrmdTreeNodeComp) and AreYouSure then
  begin
    tvObjects.Items.BeginUpdate;
    try
      i := SelectedComponent.GetItemIndex;
      tvObjects.Selected.Free;
      GetTopNode.FHandle.DeleteItem(i);
      NotifyOwnerAboutChanges;
    finally
      tvObjects.Items.EndUpdate;
    end;
  end;
end;

end.
