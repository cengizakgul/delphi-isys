unit rmSBDsgnComponentFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmCustomStatusBarFrm, ComCtrls, rmDsgnTypes, rwDsgnUtils, rmTypes;

type
  TrmSBDsgnComponent = class(TrmCustomStatusBar)
  private
    procedure DMSelectedObjectListChanged(var Message: TrmMessageRec); message mSelectedObjectListChanged;
    procedure DMObjectPropertyChanged(var Message: TrmMessageRec); message mObjectPropertyChanged;

    procedure ShowSelectedObjects;
  protected
    procedure GetListeningMessages(const AMessages: TList); override;
    procedure DoRefresh; override;
  end;

implementation

{$R *.dfm}

{ TrmSBDsgnComponent }

procedure TrmSBDsgnComponent.DMObjectPropertyChanged(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;

procedure TrmSBDsgnComponent.DMSelectedObjectListChanged(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;  
end;

procedure TrmSBDsgnComponent.DoRefresh;
begin
  inherited;
  ShowSelectedObjects;
end;

procedure TrmSBDsgnComponent.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mSelectedObjectListChanged));
  AMessages.Add(Pointer(mObjectPropertyChanged));
end;

procedure TrmSBDsgnComponent.ShowSelectedObjects;
var
  SelObjects: TrmListOfObjects;
  h, h1: String;
begin
  SelObjects := (RMDesigner.GetActiveApartment as IrmDsgnComponent).GetSelectedObjects;

  if Length(SelObjects) = 0 then
    h := ''
  else if  Length(SelObjects) = 1 then
  begin
    h := SelObjects[0].GetPropertyValue('Name');
    h1 := SelObjects[0].GetPropertyValue('Description');
    if h1 <> '' then
      h := h + ' (' + h1 + ')';
  end
  else
    h := IntToStr(Length(SelObjects)) + ' Objects Selected';

  SetStatusPanelText(2, h);
end;

end.
