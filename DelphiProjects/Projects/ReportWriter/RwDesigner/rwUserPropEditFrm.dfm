object rwUserPropEdit: TrwUserPropEdit
  Left = 357
  Top = 274
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Register User Property'
  ClientHeight = 172
  ClientWidth = 345
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  DesignSize = (
    345
    172)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 11
    Width = 70
    Height = 13
    Caption = 'Property Name'
  end
  object Label2: TLabel
    Left = 8
    Top = 39
    Width = 66
    Height = 13
    Caption = 'Property Type'
  end
  object edName: TEdit
    Left = 92
    Top = 8
    Width = 245
    Height = 21
    TabOrder = 0
    OnExit = edNameExit
  end
  object cbType: TComboBox
    Left = 92
    Top = 37
    Width = 245
    Height = 21
    Style = csDropDownList
    DropDownCount = 16
    ItemHeight = 13
    TabOrder = 1
  end
  object btnOK: TButton
    Tag = 267
    Left = 178
    Top = 141
    Width = 75
    Height = 23
    Anchors = [akTop, akRight]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 4
  end
  object btnCancel: TButton
    Tag = 261
    Left = 262
    Top = 141
    Width = 75
    Height = 23
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 5
  end
  object chbDelegProp: TCheckBox
    Left = 8
    Top = 75
    Width = 132
    Height = 17
    Caption = 'Delegated Property For'
    TabOrder = 2
    OnClick = chbDelegPropClick
  end
  object edChildProp: TEdit
    Left = 8
    Top = 96
    Width = 329
    Height = 21
    Enabled = False
    TabOrder = 3
    OnExit = edChildPropExit
    OnKeyPress = edChildPropKeyPress
  end
end
