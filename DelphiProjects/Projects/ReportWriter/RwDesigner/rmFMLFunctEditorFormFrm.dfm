inherited rmFMLFunctEditorForm: TrmFMLFunctEditorForm
  Left = 575
  Top = 183
  BorderStyle = bsToolWindow
  Caption = 'Building Function'
  ClientHeight = 77
  ClientWidth = 382
  PixelsPerInch = 96
  TextHeight = 13
  object lContent: TLabel
    Left = 4
    Top = 2
    Width = 373
    Height = 45
    AutoSize = False
    Caption = 'lContent'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Layout = tlCenter
    WordWrap = True
    OnDragOver = lContentDragOver
  end
  object btnCancel: TButton
    Left = 218
    Top = 52
    Width = 75
    Height = 22
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 0
    OnClick = btnCancelClick
  end
  object btnOK: TButton
    Left = 302
    Top = 52
    Width = 75
    Height = 22
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    OnClick = btnCancelClick
  end
end
