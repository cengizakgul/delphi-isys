inherited rmDsgnASCIIForm: TrmDsgnASCIIForm
  Width = 694
  Height = 421
  object pmTrmASCIIForm: TPopupMenu
    AutoPopup = False
    Left = 84
    Top = 117
    object miAddASCIIRecord: TMenuItem
      Caption = 'Add ASCII Record'
      OnClick = miAddASCIIRecordClick
    end
  end
  object pmTrmASCIIRecord: TPopupMenu
    AutoPopup = False
    Left = 114
    Top = 117
    object miAddASCIIField: TMenuItem
      Caption = 'Add ASCII Field'
      OnClick = miAddASCIIFieldClick
    end
    object miAddSubDetailRecord: TMenuItem
      Caption = 'Add SubDetail Record'
      OnClick = miAddSubDetailRecordClick
    end
  end
  object pmTrmASCIIField: TPopupMenu
    AutoPopup = False
    Left = 144
    Top = 117
    object miAddASCIIField2: TMenuItem
      Caption = 'Add ASCII Field'
      OnClick = miAddASCIIField2Click
    end
  end
end
