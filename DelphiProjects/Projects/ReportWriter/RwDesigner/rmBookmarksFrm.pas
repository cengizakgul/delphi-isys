unit rmBookmarksFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDockableFrameFrm, ComCtrls, rmTypes, rmDsgnTypes, rwDsgnUtils;

type
  TrmBookmarksViewer = class(TrmDockableFrame)
    BookmarksListView: TListView;
    procedure BookmarksListViewDblClick(Sender: TObject);
    procedure BookmarksListViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BookmarksListViewEditing(Sender: TObject; Item: TListItem;
      var AllowEdit: Boolean);
    procedure DeleteAllBookmarks;
  private
    FBookmarksList : IInterfaceList;
    procedure AddBookMark(AObject : IrmDesignObject);
    procedure RefreshBookmarks;

    procedure DMObjectsWereDeleted(var Message: TrmMessageRec); message mObjectsWereDeleted;
    procedure DMBookmarkObject(var Message: TrmMessageRec); message mBookmarkObject;
    procedure DMtbNew(var Message: TrmMessageRec); message mtbNew;
    procedure DMtbOpen(var Message: TrmMessageRec); message mtbOpen;
  protected
    procedure BeforeShow; override;
    procedure GetListeningMessages(const AMessages: TList); override;
    procedure DoRefresh; override;

  public
    constructor Create(AOwner: TComponent); override;
  end;


implementation

uses rmDesignerFrm;

{$R *.dfm}

{ TrmBookmarks }

procedure TrmBookmarksViewer.AddBookMark(AObject: IrmDesignObject);
var
  ListItem: TListItem;
begin
  if Assigned(AObject) then
  begin
    BookmarksListView.Items.BeginUpdate;
    try
      ListItem := BookmarksListView.Items.Add;
      ListItem.Caption := AObject.GetPropertyValue('Name');
      ListItem.SubItems.Add(AObject.GetPropertyValue('Description'));
      ListItem.Data := Pointer(AObject);
    finally
      BookmarksListView.Items.EndUpdate;
    end;
  end;
end;

procedure TrmBookmarksViewer.BeforeShow;
begin
  inherited;
  Caption := 'Bookmarks';
end;

constructor TrmBookmarksViewer.Create(AOwner: TComponent);
var
  NewColumn: TListColumn;
begin
  inherited;
  FBookmarksList := TInterfaceList.Create;

  NewColumn := BookmarksListView.Columns.Add;
  NewColumn.Caption := 'Object name';
  NewColumn.AutoSize := true;
  NewColumn.Alignment := taCenter;
  NewColumn := BookmarksListView.Columns.Add;
  NewColumn.Caption := 'Object description';
  NewColumn.AutoSize := true;
  NewColumn.Alignment := taCenter;
end;

procedure TrmBookmarksViewer.DMBookmarkObject(var Message: TrmMessageRec);
var
  O: IrmDesignObject;
begin
  O := IrmDesignObject(Pointer(Integer(Message.Parameter1)));
  AddBookmark(O);
end;

procedure TrmBookmarksViewer.DMObjectsWereDeleted(
  var Message: TrmMessageRec);
begin
  RefreshBookmarks;
end;

procedure TrmBookmarksViewer.DoRefresh;
begin
  RefreshBookmarks;
  inherited;
end;

procedure TrmBookmarksViewer.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mObjectsWereDeleted));
  AMessages.Add(Pointer(mBookmarkObject));
  AMessages.Add(Pointer(mtbOpen));
  AMessages.Add(Pointer(mtbNew));
end;

procedure TrmBookmarksViewer.RefreshBookmarks;
var
  i : integer;
  tmpObject : IrmDesignObject;
  tmpString : Variant;
begin
  BookmarksListView.Items.BeginUpdate;
  try
    for i := BookmarksListView.Items.Count - 1 downto 0 do
      if Assigned(BookmarksListView.Items[i].Data) then
      begin
        try
          tmpObject := IrmDesignObject(BookmarksListView.Items[i].Data);
          tmpString := tmpObject.GetPropertyValue('Name');
        except
          on E:Exception do
            BookmarksListView.Items.Delete(i);
        end;
      end
      else
        BookmarksListView.Items.Delete(i);
  finally
    BookmarksListView.Items.EndUpdate;
  end;
end;

procedure TrmBookmarksViewer.BookmarksListViewDblClick(Sender: TObject);
begin
  inherited;
  if (BookmarksListView.Items.Count > 0) and (BookmarksListView.ItemIndex <> -1) then
    try
      (RMDesigner.GetActiveApartment as IrmDsgnComponent).UnselectAll;
      (RMDesigner.GetActiveApartment as IrmDsgnComponent).SelectObject(IrmDesignObject(BookmarksListView.Items[BookmarksListView.ItemIndex].Data));
       Application.ProcessMessages;
    except
      on E:Exception do
        BookmarksListView.Items.Delete(BookmarksListView.ItemIndex);
    end;
end;

procedure TrmBookmarksViewer.BookmarksListViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
    if (BookmarksListView.Items.Count > 0) and (BookmarksListView.ItemIndex <> -1) then
      BookmarksListView.Items.Delete(BookmarksListView.ItemIndex);
end;

procedure TrmBookmarksViewer.BookmarksListViewEditing(Sender: TObject;
  Item: TListItem; var AllowEdit: Boolean);
begin
  inherited;
  AllowEdit := false;
end;

procedure TrmBookmarksViewer.DeleteAllBookmarks;
begin
  BookmarksListView.Items.BeginUpdate;
  try
    BookmarksListView.Items.Clear;
  finally
    BookmarksListView.Items.EndUpdate;
  end;
end;

procedure TrmBookmarksViewer.DMtbNew(
  var Message: TrmMessageRec);
begin
  DeleteAllBookmarks;
end;

procedure TrmBookmarksViewer.DMtbOpen(
  var Message: TrmMessageRec);
begin
  DeleteAllBookmarks;
end;

initialization
  RegisterClass(TrmBookmarksViewer);
end.
