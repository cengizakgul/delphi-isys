unit rmOPEValueFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls, ExtCtrls, rmOPEStringMultiLineFrm,
  rmOPENumericFrm, rmOPEDateFrm, rmOPECurrencyFrm, rmOPEFormatFrm, ComCtrls,
  rmOPEEmptyFrm, rmFMLExprPanelFrm, rmOPEExpressionFrm, rmFMLExprEditorFormFrm,
  rmTypes, rmOPEFieldFrm;

type
  TrmOPEValue = class(TrmObjPropertyEditor)
    pcTypes: TPageControl;
    tsEmpty: TTabSheet;
    tsText: TTabSheet;
    tsNumber: TTabSheet;
    tsDateTime: TTabSheet;
    frmFormatDate: TrmOPEFormat;
    frmDate: TrmOPEDate;
    frmFormatNumber: TrmOPEFormat;
    frmNumber: TrmOPENumeric;
    frmText: TrmOPEStringMultiLine;
    Label2: TLabel;
    frmEmpty: TrmOPEEmpty;
    tsCalc: TTabSheet;
    Panel1: TPanel;
    frmFormatCalc: TrmOPEFormat;
    frmExpression: TrmOPEExpression;
    tsField: TTabSheet;
    frmField: TrmOPEField;
    frmFormatField: TrmOPEFormat;
    procedure pcTypesChange(Sender: TObject);
    procedure tsFieldShow(Sender: TObject);
  private
    procedure ClearFieldInfo;
  protected
    procedure SyncPages; virtual;
    procedure PropagateShowPropValue(APage: TTabSheet);
  public
    procedure  AfterConstruction; override;
    procedure  ShowPropValue; override;
    procedure  ApplyChanges; override;
  end;

implementation

{$R *.dfm}

{ TrmOPEValue }

procedure TrmOPEValue.AfterConstruction;
begin
  inherited;
  pcTypes.ActivePage := nil;

  frmEmpty.PropertyName := 'Value';
  frmText.PropertyName := 'Value';
  frmNumber.PropertyName := 'Value';
  frmFormatNumber.FormatType := rmFTNumeric;
  frmDate.PropertyName := 'Value';
  frmField.PropertyName := 'DataField';  
  frmFormatDate.FormatType := rmFTDate;
  frmFormatCalc.FormatType := rmFTAll;
  frmFormatField.FormatType := rmFTAll;  
end;

procedure TrmOPEValue.ShowPropValue;
begin
  inherited;
  SyncPages;
end;

procedure TrmOPEValue.SyncPages;
begin
  if (TComponent(LinkedObject) as IrmDesignObject).GetFormulas.GetFormulaByAction(fatSetProperty, frmEmpty.PropertyName) <> nil then
    pcTypes.ActivePage := tsCalc

  else if (TComponent(LinkedObject) as IrmQueryAwareObject).GetFieldName <> '' then
  begin
    pcTypes.ActivePage := tsField;
    tsField.OnShow(nil);
    PropagateShowPropValue(pcTypes.ActivePage);
  end

  else
    case VarType(OldPropertyValue) of
      varNull:      pcTypes.ActivePage := tsEmpty;

      varString:    pcTypes.ActivePage := tsText;

      varByte,
      varShortInt,
      varSmallint,
      varInteger,
      varInt64,
      varSingle,
      varDouble:    pcTypes.ActivePage := tsNumber;

      varDate:      pcTypes.ActivePage := tsDateTime;
    else
      pcTypes.ActivePage := tsEmpty;
    end;

  PropagateShowPropValue(pcTypes.ActivePage);
end;

procedure TrmOPEValue.ApplyChanges;

  procedure ApplyForContainer(ACont: TWinControl);
  var
    i: Integer;
  begin
    for i := 0 to ACont.ControlCount - 1 do
      if ACont.Controls[i] is TrmObjPropertyEditor then
        TrmObjPropertyEditor(ACont.Controls[i]).ApplyChanges
      else if ACont.Controls[i] is TWinControl then
         ApplyForContainer(TWinControl(ACont.Controls[i]));
  end;

begin
  if pcTypes.ActivePage <> tsField then
    ClearFieldInfo;

  if pcTypes.ActivePage <> tsCalc then
    frmExpression.ClearExpressionForObject;

  ApplyForContainer(pcTypes.ActivePage);
end;

procedure TrmOPEValue.pcTypesChange(Sender: TObject);
begin
  PropagateShowPropValue(pcTypes.ActivePage);
  NotifyOfChanges;
end;

procedure TrmOPEValue.PropagateShowPropValue(APage: TTabSheet);

  procedure PropogateForContainer(ACont: TWinControl);
  var
    i: Integer;
  begin
    for i := 0 to ACont.ControlCount - 1 do
      if ACont.Controls[i] is TrmObjPropertyEditor then
        TrmObjPropertyEditor(ACont.Controls[i]).ShowPropValue
      else if ACont.Controls[i] is TWinControl then
        PropogateForContainer(TWinControl(ACont.Controls[i]));
  end;

begin
  PropogateForContainer(APage);
end;

procedure TrmOPEValue.tsFieldShow(Sender: TObject);
begin
  frmField.RefreshFields;
end;

procedure TrmOPEValue.ClearFieldInfo;
begin
  if pcTypes.Tag = 0 then
    (TComponent(LinkedObject) as IrmQueryAwareObject).SetFieldName('');
end;

end.
