unit rwDsgnRes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList, mwKeyCmds, mwCustomEdit, rwCodeInsightFrm, Menus, rwDsgnUtils,
  rwBasicClasses, extctrls, rwBasicUtils, rwUtils, rmTypes, PJMenuSpeedButtons,
  rmDsgnTypes, Variants, rwRTTI;

const
  ecCodeInsight = ecUserFirst + 1;
  ecCodeInsParams = ecUserFirst + 2;
  ecHelp = ecUserFirst + 3;
  ecBreakPoint = ecUserFirst + 4;
  ecTraceIn = ecUserFirst + 5;
  ecTraceOver = ecUserFirst + 6;
  ecRun = ecUserFirst + 7;
  ecTerminate = ecUserFirst + 8;

type
  TrwExpertToolBarEvent = (eteQueryBuilder, etePrintOrder, eteReportWizard, eteLibrary,
    eteLocalVarFunc, eteAllEvents, eteASCIIPrintOrder, eteTabOrder);

  TrwRulerOrientationType = (roHorizontal, roVertical);

  TrwActionToolBarEvent = (ateAlignmentLeft, ateAlignmentTop, ateAlignmentBottom, ateAlignmentRight,
    ateAlignmentHorCenter, ateAlignmentVertCenter, ateAlignmentHorContainerCenter,
    ateAlignmentVertContainerCenter, ateAlignmentVertSpaces, ateAlignmentHorSpaces,
    ateGrowHeightToLargest, ateShrinkHeightToSmallest, ateGrowWidthToLargest,
    ateShrinkWidthToSmallest, ateBringToFront, ateSendToBack);

  TrwCodeInsightItemType = (citVar, citConst, citProperty, citProcedure, citFunction);


  TrwClassPictures = class(TImageList)
  private
    FClassNames: TStringList;

    procedure InitializePicts(ASize: Integer);
    procedure AddPicture(ABitmap: TBitmap; AIndex: Integer = -1);
    procedure UpdatePicture(const AClassName: String; ABitmap: TBitmap);
    procedure UpdateClassName(const AOldClassName, ANewClassName: String);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   GetClassPicture(const AClassName: String; ABitmap: TBitmap);
    function    GetClassPictureIndex(const AClassName: String): Integer;
  end;


  TDsgnRes = class(TDataModule)
    ilButtons: TImageList;
    ilPict: TImageList;
    ilRWClassesSmall: TImageList;
    ilRWClassesBig: TImageList;
    imlToolBar: TImageList;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FRWClassSmallPictures: TrwClassPictures;
    FRWClassBigPictures: TrwClassPictures;
  public
    procedure EventTextCommand(Sender: TObject; var Command: TmwEditorCommand; var AChar: char; Data: pointer);
    procedure InitEditor(AEditor: TmwCustomEdit);
    procedure UpdateClassName(const AOldClassName, ANewClassName: String);
    procedure UpdateClassPicture(const AClassName: String; ABitmap: TBitmap);

    property  RWClassSmallPictures: TrwClassPictures read FRWClassSmallPictures;
    property  RWClassBigPictures: TrwClassPictures read FRWClassBigPictures;
  end;


  TrwLineColorInfo = record
    LineNumber: Integer;
    ForegroundColor: TColor;
    BackgroundColor: TColor;
  end;

  PTrwLineColorInfo = ^TrwLineColorInfo;

  TrwLineColorList = class
  private
    FList: TList;
    function GetItem(Index: Integer): TrwLineColorInfo;
  public
    constructor Create;
    destructor  Destroy; override;
    procedure Clear;
    procedure Add(const ALineNumber: Integer; const AForegroundColor, ABackgroundColor: TColor);
    function  Count: Integer;
    function  IndexOfLine(const ALineNumber: Integer): Integer;
    property  Items[Index: Integer]: TrwLineColorInfo read GetItem; default;
  end;


  TrwEventTextEdit = class(TmwCustomEdit)
  private
    FEventOwner: TrwComponent;
    FTimer: TTimer;
    FLastChar: Char;
    FLineColorList: TrwLineColorList;
    procedure DoOnStatusChange(Sender: TObject; Changes: TmwStatusChanges);
    procedure FOnTimer(Sender: TObject);
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    function  SpecialLineColors(Line: integer; var Foreground, Background: TColor): boolean; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    property EventOwner: TrwComponent read FEventOwner write FEventOwner;
    property LineColorList: TrwLineColorList read FLineColorList;
  end;

function DsgnRes: TDsgnRes;

implementation

{$R *.DFM}

{$R rwClassPictures.res}

var
  FDsgnRes: TDsgnRes = nil;

function DsgnRes: TDsgnRes;
begin
  if not Assigned(FDsgnRes) then
    FDsgnRes := TDsgnRes.Create(Application);
  Result := FDsgnRes;
end;

procedure TDsgnRes.DataModuleDestroy(Sender: TObject);
begin
  FDsgnRes := nil;
end;


procedure TDsgnRes.EventTextCommand(Sender: TObject; var Command: TmwEditorCommand; var AChar: char; Data: pointer);
var
  h: String;
  i: Integer;
begin
  with TmwCustomEdit(Sender) do
  begin
    if (Command = ecCodeInsight) and not TmwCustomEdit(Sender).ReadOnly then
      ShowCodeInsight(TmwCustomEdit(Sender), (Integer(Data) = -1))

    else if (Command = ecCodeInsParams) and not TmwCustomEdit(Sender).ReadOnly then
      ShowCodeInsightParams(TmwCustomEdit(Sender), (Integer(Data) = -1))

    else if Command = ecHelp then
    begin
      h := '';
      for i := CaretX-1 downto 1 do
        if (Length(LineText) < i) or (LineText[i] in [#0..' ']) then
          break
        else
          h := LineText[i] + h;
      h := h + #0;
      Application.HelpCommand(HELP_KEY, Integer(PChar(@h[1])));
    end;
{
    else if Assigned(RMDesigner) then
      if Command = ecBreakPoint then
        RMDesigner.GetComponentDesigner.Notify(rmdTlbActBreakPointToggle, 0)

      else if Command = ecTraceIn then
        RMDesigner.GetComponentDesigner.Notify(rmdTlbActTraceIn, 0)

      else if Command = ecTraceOver then
        RMDesigner.GetComponentDesigner.Notify(rmdTlbActTraceOver, 0)

      else if Command = ecRun then
        RMDesigner.GetComponentDesigner.Notify(rmdTlbActRun, 0)

      else if Command = ecTerminate then
        RMDesigner.GetComponentDesigner.Notify(rmdTlbActStop, 0);
}        
  end;
end;


procedure TDsgnRes.InitEditor(AEditor: TmwCustomEdit);
var
  KS: TmwKeyStroke;
begin
  AEditor.TabWidth := 2;

  KS := AEditor.KeyStrokes.Add;
  KS.ShortCut :=  ShortCut(VK_SPACE, [ssCtrl]);
  KS.Command := ecCodeInsight;

  KS := AEditor.KeyStrokes.Add;
  KS.ShortCut :=  ShortCut(VK_SPACE, [ssCtrl, ssShift]);
  KS.Command := ecCodeInsParams;

  KS := AEditor.KeyStrokes.Add;
  KS.ShortCut :=  ShortCut(VK_F1, []);
  KS.Command := ecHelp;

  KS := AEditor.KeyStrokes.Add;
  KS.ShortCut :=  ShortCut(VK_F5, []);
  KS.Command := ecBreakpoint;

  KS := AEditor.KeyStrokes.Add;
  KS.ShortCut :=  ShortCut(VK_F7, []);
  KS.Command := ecTraceIn;

  KS := AEditor.KeyStrokes.Add;
  KS.ShortCut :=  ShortCut(VK_F8, []);
  KS.Command := ecTraceOver;

  KS := AEditor.KeyStrokes.Add;
  KS.ShortCut :=  ShortCut(VK_F9, []);
  KS.Command := ecRun;

  KS := AEditor.KeyStrokes.Add;
  KS.ShortCut :=  ShortCut(VK_F2, [ssCtrl]);
  KS.Command := ecTerminate;

  AEditor.OnProcessUserCommand := EventTextCommand;
  AEditor.HighLighter := GeneralSyn;
end;


{ TrwEventTextEdit }

constructor TrwEventTextEdit.Create(AOwner: TComponent);
begin
  inherited;
  FTimer := TTimer.Create(Self);
  FTimer.Enabled := False;
  FTimer.OnTimer := FOnTimer;
  FTimer.Interval := DsgnCodeInsightDelay;
  FEventOwner := nil;
  OnStatusChange := DoOnStatusChange;
  FLastChar := ' ';

  FLineColorList := TrwLineColorList.Create;
end;


destructor TrwEventTextEdit.Destroy;
begin
  FLineColorList.Free;
  inherited;
end;

procedure TrwEventTextEdit.DoOnStatusChange(Sender: TObject; Changes: TmwStatusChanges);
begin
  if (mwscLeftChar in Changes) or (mwscTopLine in Changes) then
    HideCodeInsightWnds;
end;


procedure TrwEventTextEdit.FOnTimer(Sender: TObject);
var
  Cmd: TmwEditorCommand;
  C: Char;
begin
  FTimer.Enabled := False;
  FTimer.Interval := DsgnCodeInsightDelay;

  Cmd := 0;
  case FLastChar of
    '.': if DsgnCodeCompletion then
           Cmd := ecCodeInsight;

    '(': if DsgnCodeParams then
           Cmd := ecCodeInsParams;
  end;

  if Cmd <> 0 then
    ProcessCommand(Cmd, C, Pointer(-1));
end;


procedure TrwEventTextEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if IsCodeInsightVisible then
    case Key of
      VK_UP:     CodeInsCommand(cicUp);
      VK_DOWN:   CodeInsCommand(cicDown);
      VK_PRIOR:  CodeInsCommand(cicPageUp);
      VK_NEXT:   CodeInsCommand(cicPageDown);
      VK_HOME:   CodeInsCommand(cicTop);
      VK_END:    CodeInsCommand(cicBottom);
      VK_RETURN: CodeInsCommand(cicSelect);
      VK_ESCAPE: CodeInsCommand(cicClose);
    else
      inherited;
    end
  else if IsCodeParamsVisible and (Key = VK_ESCAPE) then
    CodeInsCommand(cicClose)
  else
    inherited;
end;


procedure TrwEventTextEdit.KeyPress(var Key: Char);
begin
  inherited;
  if Key in ['.', '(']  then
  begin
    FLastChar := Key;
    FTimer.Enabled := True;
  end;
end;

procedure TDsgnRes.DataModuleCreate(Sender: TObject);
begin
  FRWClassSmallPictures := TrwClassPictures.Create(Self);
  FRWClassSmallPictures.InitializePicts(16);
  FRWClassBigPictures := TrwClassPictures.Create(Self);
  FRWClassBigPictures.InitializePicts(32);
end;


function TrwEventTextEdit.SpecialLineColors(Line: integer; var Foreground, Background: TColor): boolean;
var
  i: Integer;
begin
  i := FLineColorList.IndexOfLine(Line);
  if i <> -1 then
  begin
    Foreground := FLineColorList[i].ForegroundColor;
    Background := FLineColorList[i].BackgroundColor;
    Result := True;
    if Assigned(OnSpecialLineColors) then
      OnSpecialLineColors(Self, Line, Result, Foreground, Background);
  end
  else
    Result := inherited SpecialLineColors(Line, Foreground, Background);
end;


{ TrwClassPictures }

constructor TrwClassPictures.Create(AOwner: TComponent);
begin
  inherited;
  FClassNames := TStringList.Create;
end;

destructor TrwClassPictures.Destroy;
begin
  FreeAndNil(FClassNames);
  inherited;
end;


procedure TrwClassPictures.GetClassPicture(const AClassName: String; ABitmap: TBitmap);
var
  i: Integer;
begin
  ABitmap.Assign(nil);
  i := GetClassPictureIndex(AClassName);
  if i <> -1 then
    GetBitmap(i, ABitmap);
end;


procedure TrwClassPictures.InitializePicts(ASize: Integer);
var
 Bmp: TBitmap;
 i: Integer;
 Cl: IrwClass;

   procedure AddClass(const AClassName: String);
   begin
     if Bmp.Empty  or (FClassNames.IndexOf(UpperCase(AClassName)) <> -1) then
       Exit;

     AddPicture(Bmp);

     FClassNames.Add(UpperCase(AClassName));
   end;

   procedure AddStandardClass(const AClassName: String; APictIndex: Integer; ABigPicture: Boolean = False);
   begin
     Bmp.Assign(nil);
     if ABigPicture then
       TDsgnRes(Owner).ilRWClassesBig.GetBitmap(APictIndex, Bmp)
     else
       TDsgnRes(Owner).ilRWClassesSmall.GetBitmap(APictIndex, Bmp);
     AddClass(AClassName);
   end;

begin
  Clear;
  Width := ASize;
  Height := ASize;
  Masked := False;
  FClassNames.Clear;

  Bmp := TBitMap.Create;
  try
    AddStandardClass('TrwComponent', 0);
    AddStandardClass('TrwQuery', 1);
    AddStandardClass('TrwBuffer', 2);
    AddStandardClass('TrmPrintText', 3);
    AddStandardClass('TrmPrintImage', 6);
    AddStandardClass('TrmPrintContainer', 4);
    AddStandardClass('TrmTable', 14);
    AddStandardClass('TrmDBTable', 15);
    AddStandardClass('TrmPrintPageNumber', 17, True);

    AddStandardClass('TrwText', 3);
    AddStandardClass('TrwWinFormControl', 4);
    AddStandardClass('TrwEdit', 7);
    AddStandardClass('TrwComboBox', 8);
    AddStandardClass('TrwDBComboBox', 9);
    AddStandardClass('TrwCheckBox', 10);
    AddStandardClass('TrwDateEdit', 11);
    AddStandardClass('TrwGroupBox', 12);
    AddStandardClass('TrwRadioGroup', 13);
    AddStandardClass('TrwDBGrid', 15);
    AddStandardClass('TrwPageControl', 16);

    AddStandardClass('TrmASCIIRecord', 15, True);
    AddStandardClass('TrmASCIIField', 16, True);

    AddStandardClass('TrmXMLSimpleTypeByRestriction', 8, True);
    AddStandardClass('TrmXMLSimpleTypeByList', 9, True);
    AddStandardClass('TrmXMLSimpleTypeByUnion', 10, True);
    AddStandardClass('TrmXMLAttribute', 7, True);
    AddStandardClass('TrmXMLAttributeGroup', 14, True);
    AddStandardClass('TrmXMLElement', 6, True);
    AddStandardClass('TrmXMLGroup', 13, True);
    AddStandardClass('TrmXMLSequence', 11, True);
    AddStandardClass('TrmXMLChoice', 12, True);
    AddStandardClass('TrmXMLfragment', 18, True);

    AddStandardClass('TrmReport', 0, True);
    AddStandardClass('TrwGlobalVarFunct', 1, True);
    AddStandardClass('TrwInputFormContainer', 2, True);
    AddStandardClass('TrmPrintForm', 3, True);
    AddStandardClass('TrmASCIIForm', 4, True);
    AddStandardClass('TrmXMLForm', 5, True);

    for i := 0 to rwRTTIInfo.GetItemCount - 1 do
    begin
      Cl := rwRTTIInfo.GetItemByIndex(i);

      if Cl.IsUserClass then
      begin
        Bmp.Assign(nil);
        Cl.GetUserClass.GetPicture(Bmp);
        AddClass(Cl.GetClassName);
      end;
    end;

  finally
    FreeAndNil(Bmp);
  end;
end;

function TrwClassPictures.GetClassPictureIndex(const AClassName: String): Integer;
var
  Cl, h: String;
  i: Integer;
begin
  Result := -1;
  Cl := GetClassHierarchy(AClassName);
  while Cl <> '' do
  begin
    h := GetPrevStrValue(Cl, '.');
    i := FClassNames.IndexOf(UpperCase(h));
    if i <> -1 then
    begin
      Result := i;
      break;
    end;
  end;
end;

procedure TrwClassPictures.UpdatePicture(const AClassName: String; ABitmap: TBitmap);
var
  i: Integer;
begin
  i := GetClassPictureIndex(AClassName);
  if i <> -1 then
  begin
    if ABitmap.Empty then
    begin
      Delete(i);
      FClassNames.Delete(i);
    end
    else
      AddPicture(ABitmap, i);
  end

  else if not ABitmap.Empty then
  begin
    AddPicture(ABitmap);
    FClassNames.Add(UpperCase(AClassName));
    PostApartmentMessage(cAnonymousSender, mComponentPaletteInfoChanged, AClassName, 0);
  end;
end;

procedure TrwClassPictures.AddPicture(ABitmap: TBitmap; AIndex: Integer);
var
 BmpResized: TBitmap;
begin
   if (ABitmap.Width <> Width) or (ABitmap.Height <> Height) then
   begin
     BmpResized := TBitMap.Create;
     try
       BmpResized.Assign(nil);
       BmpResized.Width := Width;
       BmpResized.Height := Height;
       if (ABitmap.Width < Width) and (ABitmap.Height < Height) then
       begin
         BmpResized.Canvas.Brush.Style := bsSolid;
         BmpResized.Canvas.Brush.Color := ABitmap.TransparentColor;
         BmpResized.Canvas.FillRect(Rect(0, 0, Width, Height));
         BmpResized.Canvas.Draw((Width - ABitmap.Width) div 2, (Height - ABitmap.Height) div 2, ABitmap);
       end

       else
         BmpResized.Canvas.StretchDraw(Rect(0, 0, Width, Height), ABitmap);
       BmpResized.Transparent := True;
       BmpResized.TransparentMode := tmAuto;
       if AIndex = -1 then
         Add(BmpResized, nil)
       else
         Insert(AIndex, BmpResized, nil);
     finally
       FreeAndNil(BmpResized);
     end;
   end
   else
     if AIndex = -1 then
       Add(ABitmap, nil)
     else
       Insert(AIndex, ABitmap, nil);
end;

procedure TDsgnRes.UpdateClassPicture(const AClassName: String; ABitmap: TBitmap);
begin
  RWClassSmallPictures.UpdatePicture(AClassName, ABitmap);
  RWClassBigPictures.UpdatePicture(AClassName, ABitmap);

  PostApartmentMessage(cAnonymousSender, mComponentIconChanged, AClassName, 0);
end;

procedure TDsgnRes.UpdateClassName(const AOldClassName, ANewClassName: String);
begin
  RWClassSmallPictures.UpdateClassName(AOldClassName, ANewClassName);
  RWClassBigPictures.UpdateClassName(AOldClassName, ANewClassName);

  PostApartmentMessage(cAnonymousSender, mComponentClassNameChanged, AOldClassName, ANewClassName);
end;

procedure TrwClassPictures.UpdateClassName(const AOldClassName, ANewClassName: String);
var
  i: Integer;
begin
  i := FClassNames.IndexOf(UpperCase(AOldClassName));
  if i <> - 1 then
    FClassNames[i] := UpperCase(ANewClassName);
end;


{ TrwLineColorList }

constructor TrwLineColorList.Create;
begin
  FList := TList.Create;
end;

destructor TrwLineColorList.Destroy;
begin
  Clear;
  FList.Free;
  inherited;
end;

procedure TrwLineColorList.Add(const ALineNumber: Integer; const AForegroundColor, ABackgroundColor: TColor);
var
  NewLineInfo: PTrwLineColorInfo;
begin
  New(NewLineInfo);
  NewLineInfo.LineNumber := ALineNumber;
  NewLineInfo.ForegroundColor := AForegroundColor;
  NewLineInfo.BackgroundColor := ABackgroundColor;

  FList.Add(NewLineInfo);
end;

procedure TrwLineColorList.Clear;
var
  i: Integer;
begin
  for i := 0 to FList.Count - 1 do
  begin
    Dispose(PTrwLineColorInfo(FList[i]));
  end;

  FList.Clear;
end;

function TrwLineColorList.GetItem(Index: Integer): TrwLineColorInfo;
begin
  Result := PTrwLineColorInfo(FList[Index])^;
end;


function TrwLineColorList.Count: Integer;
begin
  Result := FList.Count;
end;

function TrwLineColorList.IndexOfLine(const ALineNumber: Integer): Integer;
var
  i: Integer;
begin
  Result := -1;
  
  for i := 0 to Count - 1 do
    if PTrwLineColorInfo(FList[i]).LineNumber = ALineNumber then
    begin
      Result := i;
      break;
    end;
end;

initialization

finalization
  FreeAndNil(FDsgnRes);

end.
