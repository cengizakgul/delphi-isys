unit rmLibCompChooseFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ImgList, ComCtrls, rwUtils, rmReport,
  rwCommonClasses, rwDsgnUtils, rwDsgnRes, rwEngineTypes, rmTypes, rwRTTI;

type

  TrmLibCompChoose = class(TForm)
    btnOK: TButton;
    btnCancel: TButton;
    tbcCategories: TTabControl;
    lvLibComp: TListView;
    memDescription: TMemo;
    lDescription: TLabel;
    lGreeting: TLabel;
    tvLibComp: TTreeView;
    imlPictures: TImageList;
    procedure lvLibCompDblClick(Sender: TObject);
    procedure lvLibCompSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure tbcCategoriesChange(Sender: TObject);
  private
    FComponentTypes: array of string;
    procedure Initialize(const Sender: IrmDesignObject);
    function  AddComponentToTree(AComp: IrmComponent): TTreeNode;
    procedure ShowComponentPalette(ANode: TTreeNode);
    function  ClassAccepted(ALibComponent: IrmComponent): Boolean;
  public
  end;

  function ChoiceLibComp(const AComponentTypes: array of string; const Sender: IrmDesignObject;  AOwner: TComponent): IrmComponent;

implementation

{$R *.dfm}

function ChoiceLibComp(const AComponentTypes: array of string; const Sender: IrmDesignObject; AOwner: TComponent): IrmComponent;
var
  Frm: TrmLibCompChoose;
  i: Integer;
begin
  Result := nil;
  Frm := TrmLibCompChoose.Create(AOwner);
  with Frm do
    try
      SetLength(FComponentTypes, Length(AComponentTypes));
      for i := Low(FComponentTypes) to High(FComponentTypes) do
        FComponentTypes[i] := AComponentTypes[i];

      Initialize(Sender);

      if tbcCategories.Tabs.Count > 0 then
      begin
        tbcCategories.TabIndex := 0;
        tbcCategories.OnChange(nil);
        if ShowModal = mrOK then
          if Assigned(lvLibComp.Selected) and (lvLibComp.Selected.Data <> nil) then
            Result := IInterface(lvLibComp.Selected.Data) as IrmComponent;
      end;
    finally
      Frm.Free;
    end;
end;


{ TrmLibCompChoose }

function TrmLibCompChoose.AddComponentToTree(AComp: IrmComponent): TTreeNode;
var
  N: TTreeNode;
  i: Integer;
begin
  Result := nil;
  N := nil;
  if Assigned(GetClass(AComp.GetAncestorClassName)) then
  begin
    for i := 0 to tvLibComp.Items[0].Count-1 do
      if AnsiSameText(TToolButton(tvLibComp.Items[0].Item[i].Data).Caption, AComp.GetAncestorClassName) then
      begin
        N := tvLibComp.Items[0].Item[i];
        break;
      end;

{###

    if not Assigned(N) then
      for i := 0 to ReportDesigner.ReportFormToolsBar.tlbComponents.ButtonCount - 1 do
        if ReportDesigner.ReportFormToolsBar.tlbComponents.Buttons[i].Caption = AComp.ParentName then
        begin
          N := tvLibComp.Items.AddChildObject(tvLibComp.Items[0],
            ReportDesigner.ReportFormToolsBar.tlbComponents.Buttons[i].Hint, ReportDesigner.ReportFormToolsBar.tlbComponents.Buttons[i]);
          N.ImageIndex := ReportDesigner.ReportFormToolsBar.tlbComponents.Buttons[i].ImageIndex;
          N.SelectedIndex := N.ImageIndex;
          N.StateIndex := N.ImageIndex;
          break;
        end;
}
  end

  else
  begin
    for i := 0 to tvLibComp.Items.Count - 1 do
      if Assigned(tvLibComp.Items[i].Data) and AnsiSameText(AComp.GetAncestorClassName, (IInterface(tvLibComp.Items[i].Data) as IrmComponent).GetClassName)  then
      begin
        N := tvLibComp.Items[i];
        break;
      end;
  end;

  if Assigned(N) then
  begin
//    AComp.DisplayName := AComp.Name;
    Result := tvLibComp.Items.AddChildObject(N, AComp.GetDisplayName, Pointer(AComp));
    Result.ImageIndex := N.ImageIndex;
    Result.SelectedIndex := N.ImageIndex;
    Result.StateIndex := N.ImageIndex;
  end;
end;


var Frm: TrmLibCompChoose;

procedure OnAddTreeNode(ANewNode: TTreeNode; var Accept: Boolean);
begin
  if Assigned(ANewNode.Data) then
    Accept := Frm.ClassAccepted(IInterface(ANewNode.Data) as IrmComponent);
end;


function TrmLibCompChoose.ClassAccepted(ALibComponent: IrmComponent): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(FComponentTypes) to High(FComponentTypes) do
  begin
    if ALibComponent.IsInheritsFrom(FComponentTypes[i]) and
       (RMDesigner.GetComponentDesigner.LibComponentDesigninig or
        not RMDesigner.GetComponentDesigner.LibComponentDesigninig and not (rwDPSComponentEditModeOnly in ALibComponent.GetDsgnPaletteSettings)) and
       (DesignRMAdvancedMode or
        not DesignRMAdvancedMode and not (rwDPSAdvancedModeOnly in ALibComponent.GetDsgnPaletteSettings)) then
    begin
      Result := True;
      break;
    end;
  end;
end;


procedure TrmLibCompChoose.Initialize(const Sender: IrmDesignObject);

  procedure InheritageData;
  var
    N: TTreeNode;
    i: Integer;
  begin
    N := tvLibComp.Items.AddChildObject(nil, 'Component Library', nil);
    N.ImageIndex := 46;
    N.SelectedIndex := N.ImageIndex;
    N.StateIndex := N.ImageIndex;

    for i := 0 to rwRTTIInfo.GetItemCount -1 do
      if rwRTTIInfo.GetItemByIndex(i).IsUserClass then
      if ClassAccepted(rwRTTIInfo.GetItemByIndex(i).GetUserClass) then
        AddComponentToTree(rwRTTIInfo.GetItemByIndex(i).GetUserClass);
  end;


  procedure CategoryData;
  var
    N: TTreeNode;
    i: Integer;
    L: TStringList;
  begin
    N := tvLibComp.Items.AddChildObject(nil, 'Categories', nil);
    N.ImageIndex := 35;
    N.SelectedIndex := N.ImageIndex;
    N.StateIndex := N.ImageIndex;

    Frm := Self;
    if Assigned(Sender) then
    begin
      L := TStringList.Create;
      try
        Sender.GetAvailableClassList(L);
        FillUpTreeViewCollection(L, tvLibComp, N, @OnAddTreeNode)
      finally
        FreeAndNil(L);
      end;
    end

    else
    begin
      FillUpTreeViewCollection(SystemLibComponents, tvLibComp, N, @OnAddTreeNode);
    end;

    for i := tvLibComp.Items.Count -1 downto 0 do
      if not Assigned(tvLibComp.Items[i].Data) and not tvLibComp.Items[i].HasChildren then
        tvLibComp.Items[i].Free;

{    if tvLibComp.Items.Count = 0 then
      Exit;

    N := tvLibComp.Items[0];
    while N.Count = 1 do
    begin
      if N.Item[0].Count = 1 then
        N := N.Item[0]
      else
        break;
    end;
}
    for i := 1 to tvLibComp.Items.Count -1 do
      if tvLibComp.Items[i].HasChildren {and
         Assigned(tvLibComp.Items[i].Parent) and (tvLibComp.Items[i].Parent = N)} then
        tbcCategories.Tabs.AddObject(tvLibComp.Items[i].Text, tvLibComp.Items[i]);
  end;


  function IsReportsList: Boolean;
  var
    i: Integer;
    Cl: IrwClass;
  begin
    Result := False;
    for i := Low(FComponentTypes) to High(FComponentTypes) do
    begin
      Cl := rwRTTIInfo.FindClass(FComponentTypes[i]);
      if not Assigned(Cl) then
        Result := False

      else if Cl.DescendantOf('TrmReport') then
        Result := True;

      if not Result then
        break;
    end;
  end;

begin
  tvLibComp.Items.BeginUpdate;
  tvLibComp.SortType := stNone;
  tvLibComp.Items.Clear;

  CategoryData;
  if tvLibComp.Items.Count = 0 then
    InheritageData;

  tvLibComp.SortType := stText;
  if tvLibComp.Items.Count > 0 then
    tvLibComp.Items[0].Expand(False);

  tvLibComp.Items.EndUpdate;


  if IsReportsList then
  begin
    lGreeting.Caption := 'Please select a type of report you want to create';
    Caption :=  'Report Type';
  end

  else
  begin
    lGreeting.Caption := 'Please select a component you want to create';
    Caption :=  'Library Component';
  end;
end;


procedure TrmLibCompChoose.lvLibCompDblClick(Sender: TObject);
begin
  btnOK.Click;
end;

procedure TrmLibCompChoose.lvLibCompSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
begin
  if Assigned(Item.Data) then
    memDescription.Text := (IInterface(Item.Data) as IrmComponent).GetDescription
  else
    memDescription.Text := '';
end;


procedure TrmLibCompChoose.ShowComponentPalette(ANode: TTreeNode);
var
  Bmp: TBitmap;
  BmpResized: TBitmap;

  procedure FillFromNode(ANode: TTreeNode);
  var
    LI: TListItem;
    N: TTreeNode;
    LC: IrmComponent;
  begin
    N := ANode.getFirstChild;
    while Assigned(N) do
    begin
      if Assigned(N.Data) then
      begin
        LC := (IInterface(N.Data) as IrmComponent);
        LI := lvLibComp.Items.Add;
        LI.Caption := LC.GetDisplayName;
        LI.Data := Pointer(LC);
        DsgnRes.RWClassBigPictures.GetClassPicture(LC.GetClassName, Bmp);
        if not Bmp.Empty then
        begin
          BmpResized.Assign(nil);
          BmpResized.Width := imlPictures.Width;
          BmpResized.Height := imlPictures.Height;
          BmpResized.Canvas.StretchDraw(Rect(0, 0, imlPictures.Width, imlPictures.Height), Bmp);
          BmpResized.Transparent := True;
          BmpResized.TransparentMode := tmAuto;
          LI.ImageIndex := imlPictures.AddMasked(BmpResized, BmpResized.TransparentColor);
        end
        else
          LI.ImageIndex := 0;
      end
      else
        FillFromNode(N);

      N := ANode.GetNextChild(N);
    end;
  end;

begin
  Bmp := TBitmap.Create;
  BmpResized := TBitmap.Create;
  lvLibComp.Items.BeginUpdate;
  try
    lvLibComp.SortType := stNone;
    lvLibComp.Items.Clear;
    FillFromNode(ANode);
    lvLibComp.SortType := stText;

  finally
    lvLibComp.Items.EndUpdate;
    FreeAndNil(Bmp);
    FreeAndNil(BmpResized);
  end;

  if lvLibComp.Items.Count > 0 then
    lvLibComp.Items[0].Selected := True;
end;

procedure TrmLibCompChoose.tbcCategoriesChange(Sender: TObject);
var
  N: TTreeNode;
begin
  N := TTreeNode(tbcCategories.Tabs.Objects[tbcCategories.TabIndex]);
  ShowComponentPalette(N);
end;

end.
