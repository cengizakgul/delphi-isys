unit rmDsgnFunctionsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDsgnAreaFrm, ExtCtrls, ImgList, ComCtrls, rmTypes, rmDsgnTypes,
  StdCtrls, rmCustomFrameFrm, rmFunctionTextFrm, Menus, rwDsgnUtils, rwBasicUtils;

type
  TrmdTreeNodeFuncts = class;

  TrmDsgnFunctions = class(TrmDsgnArea)
    tvObjects: TTreeView;
    ImageList: TImageList;
    Splitter1: TSplitter;
    pnlFunction: TPanel;
    Panel8: TPanel;
    Label6: TLabel;
    pnPos: TPanel;
    frmFunctionText: TrmFunctionText;
    pnlFunctions: TPanel;
    Label1: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    pnlGroup: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    edGroupName: TEdit;
    Panel3: TPanel;
    btnAddFunct2: TButton;
    btnDelFuct: TButton;
    btnDeleteGroup: TButton;
    pmObjTree: TPopupMenu;
    miSplitter1: TMenuItem;
    miAddGroup: TMenuItem;
    miDeleteGroup: TMenuItem;
    miAddFunction: TMenuItem;
    miDeleteFunction: TMenuItem;
    btnAddGroup: TButton;
    procedure tvObjectsChange(Sender: TObject; Node: TTreeNode);
    procedure tvObjectsChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure edGroupNameExit(Sender: TObject);
    procedure edGroupNameKeyPress(Sender: TObject; var Key: Char);
    procedure tvObjectsCompare(Sender: TObject; Node1, Node2: TTreeNode;
      Data: Integer; var Compare: Integer);
    procedure tvObjectsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure tvObjectsEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure miAddFunctionClick(Sender: TObject);
    procedure miDeleteFunctionClick(Sender: TObject);
    procedure miDeleteGroupClick(Sender: TObject);
    procedure miAddGroupClick(Sender: TObject);
  private
    function GetTopNode: TrmdTreeNodeFuncts;
    function SelectedFunction: IrmFunction;

    procedure SetFunctsDescription;
    procedure SetFunctDescription;
    procedure SetGroupDescription;
    procedure NotifyOwnerAboutChanges;
    procedure SyncRunTimeFunctionPosition;
    procedure OnSelectFunction;

    procedure DMRefreshComponent(var Message: TrmMessageRec); message mRefreshComponents;
    procedure DMFunctionTextChanged(var Message: TrmMessageRec); message mFunctionTextChanged;
    procedure DMRunningPaused(var Message: TrmMessageRec); message mRunningPaused;
    procedure DMRunningProceeded(var Message: TrmMessageRec); message mRunningProceeded;

  protected
    procedure Initialize; override;
    procedure GetListeningMessages(const AMessages: TList); override;
    procedure DoRefresh; override;

  public
    constructor Create(AOwner: TComponent); override;
    function CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject; override;
  end;


  TrmdTreeNodeGroup = class(TTreeNode)
  private
    procedure UpdateGroupInfo;
    procedure CleanupFromFunctions;
  end;

  TrmdTreeNodeFunct = class(TTreeNode)
  private
    FHandle:  IrmFunction;
    procedure Update;
    procedure UpdateGroupInfo;
  end;

  TrmdTreeNodeFuncts = class(TTreeNode)
  private
    FHandle:  IrmFunctions;
    function  AddFunctNode(const AFunct: IrmFunction): TrmdTreeNodeFunct;
    function  AddGroupNode(const AGroupName: String; AParentNode: TTreeNode): TrmdTreeNodeGroup;
    procedure Update;
  end;



implementation

{$R *.dfm}


{ TrmdTreeNodeFuncts }

function TrmdTreeNodeFuncts.AddFunctNode(const AFunct: IrmFunction): TrmdTreeNodeFunct;
var
  Npar: TTreeNode;

  function CheckBranch(AGroup: String): TTreeNode;
  var
    h: String;
    i: Integer;
  begin
    Result := Self;

    while AGroup <> '' do
    begin
      h := GetNextStrValue(AGroup, '\');
      if Assigned(Result) then
      begin
        for i := 0 to Result.Count - 1 do
          if SameText(Result[i].Text, h) then
          begin
            Result := Result[i];
            h := '';
            break;
          end;
      end
      else
      begin
        for i := 0 to Owner.Count - 1 do
          if (Owner[i].Level = 0) and SameText(Owner[i].Text, h) then
          begin
            Result := Owner[i];
            h := '';
            break;
          end;
      end;

      if h <> '' then
        Result := AddGroupNode(h, Result);
    end;
  end;

begin
  Npar := CheckBranch(AFunct.GetGroup);

  Result := TrmdTreeNodeFunct.Create(Owner);
  Owner.AddNode(Result, Npar, '', nil, naAddChild);
  Result.FHandle := AFunct;
  Result.Update;
end;

function TrmdTreeNodeFuncts.AddGroupNode(const AGroupName: String; AParentNode: TTreeNode): TrmdTreeNodeGroup;
begin
  Result := TrmdTreeNodeGroup.Create(Owner);
  Owner.AddNode(Result, AParentNode, AGroupName, nil, naAddChild);
end;

procedure TrmdTreeNodeFuncts.Update;
var
  i: Integer;
begin
  Text := 'Functions';
  ImageIndex := 0;
  StateIndex := ImageIndex;
  SelectedIndex := ImageIndex;

  DeleteChildren;

  for i := 0 to FHandle.GetItemCount - 1 do
    AddFunctNode(FHandle.GetItemByIndex(i) as IrmFunction);
end;

{ TrmdTreeNodeFunct }

procedure TrmdTreeNodeFunct.Update;
begin
  Text := FHandle.GetName;
  if FHandle.GetDisplayName <> '' then
    Text := Text + ' (' + FHandle.GetDisplayName + ')';

  if FHandle.IsInheritedItem then
    ImageIndex := 2
  else
    ImageIndex := 1;

  StateIndex := ImageIndex;
  SelectedIndex := ImageIndex;
end;

procedure TrmdTreeNodeFunct.UpdateGroupInfo;
var
  N: TTreeNode;
  Grp: String;
begin
  Grp := '';

  N := Parent;

  while N is TrmdTreeNodeGroup do
  begin
    if Grp <> '' then
      Grp := '\' + Grp;
    Grp := N.Text + Grp;
    N := N.Parent;
  end;

  FHandle.SetGroup(Grp);
end;


{ TrmDsgnFunctions }

constructor TrmDsgnFunctions.Create(AOwner: TComponent);
begin
  inherited;
  FImageIndex := 1;
  FAreaType := rmDATInternalObject;
end;

function TrmDsgnFunctions.CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject;
begin
// do nothing
end;

function TrmDsgnFunctions.GetTopNode: TrmdTreeNodeFuncts;
begin
  Result := TrmdTreeNodeFuncts(tvObjects.Items[0]);
end;

procedure TrmDsgnFunctions.Initialize;
var
  Npar: TrmdTreeNodeFuncts;
begin
  inherited;

  tvObjects.Items.BeginUpdate;
  try
    tvObjects.Items.Clear;

    Npar := TrmdTreeNodeFuncts.Create(tvObjects.Items);
    tvObjects.Items.AddNode(Npar, nil, '', nil, naAdd);
    Npar.FHandle := (DesignObject.RealObject as IrmVarFunctHolder).GetLocalFunctions;
    Npar.Update;

    tvObjects.SortType := stNone;
    tvObjects.SortType := stText;

    GetTopNode.Expand(False);
  finally
     tvObjects.Items.EndUpdate;
  end;

  GetTopNode.Selected := True;

  btnAddGroup.Enabled := (Apartment as IrmDsgnComponent).GetSecurityInfo.EditFunctions;
  btnAddFunct2.Enabled := btnAddGroup.Enabled;
  miAddGroup.Enabled := btnAddGroup.Enabled;
  miAddFunction.Enabled := btnAddGroup.Enabled;
  miDeleteGroup.Enabled := btnAddGroup.Enabled;
  btnDeleteGroup.Enabled := btnAddGroup.Enabled;
end;

procedure TrmDsgnFunctions.SetFunctDescription;
var
  fl: Boolean;
begin
  OnSelectFunction;

  fl :=  not SelectedFunction.IsInheritedItem and (Apartment as IrmDsgnComponent).GetSecurityInfo.EditFunctions;

  miDeleteFunction.Enabled := fl;
  btnDelFuct.Enabled := fl;

  pnlFunction.Show;
  pnlFunctions.Hide;
  pnlGroup.Hide;
end;

procedure TrmDsgnFunctions.SetFunctsDescription;
begin
  pnlFunctions.Show;
  pnlFunction.Hide;
  pnlGroup.Hide;
end;

procedure TrmDsgnFunctions.SetGroupDescription;
var
  GrpNode: TrmdTreeNodeGroup;
begin
  GrpNode := tvObjects.Selected as TrmdTreeNodeGroup;
  edGroupName.Text := GrpNode.Text;
  edGroupName.Enabled := (Apartment as IrmDsgnComponent).GetSecurityInfo.EditFunctions;

  pnlGroup.Show;
  pnlFunction.Hide;
  pnlFunctions.Hide;
end;

procedure TrmDsgnFunctions.tvObjectsChange(Sender: TObject; Node: TTreeNode);
begin
  if Assigned(Node) then
  begin
    if Node is TrmdTreeNodeFuncts then
      SetFunctsDescription
    else if Node is TrmdTreeNodeFunct then
      SetFunctDescription
    else
      SetGroupDescription;
  end;
end;

procedure TrmDsgnFunctions.NotifyOwnerAboutChanges;
begin
  tvObjects.SortType := stNone;
  tvObjects.SortType := stText;

  PostApartmentMessage(Self, mFunctionsChanged, 0, 0);
end;

procedure TrmDsgnFunctions.tvObjectsChanging(Sender: TObject; Node: TTreeNode; var AllowChange: Boolean);
begin
  if tvObjects.CanFocus then
    tvObjects.SetFocus;
end;

procedure TrmDsgnFunctions.DMRefreshComponent(var Message: TrmMessageRec);
begin
  inherited;
  Initialize;
end;

procedure TrmDsgnFunctions.DMFunctionTextChanged(var Message: TrmMessageRec);
begin
  inherited;

  if Message.SenderID = GetObjectID(frmFunctionText) then
  begin
    (tvObjects.Selected as TrmdTreeNodeFunct).Update;
    NotifyOwnerAboutChanges;
  end;
end;

procedure TrmDsgnFunctions.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mRefreshComponents));
  AMessages.Add(Pointer(mFunctionTextChanged));
  AMessages.Add(Pointer(mRunningPaused));
  AMessages.Add(Pointer(mRunningProceeded));
end;

function TrmDsgnFunctions.SelectedFunction: IrmFunction;
begin
  if tvObjects.Selected is TrmdTreeNodeFunct then
    Result := TrmdTreeNodeFunct(tvObjects.Selected).FHandle
  else
    Result := nil;
end;

procedure TrmDsgnFunctions.DMRunningPaused(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;

procedure TrmDsgnFunctions.DMRunningProceeded(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;


procedure TrmDsgnFunctions.DoRefresh;
begin
  inherited;
  SyncRunTimeFunctionPosition;
end;

procedure TrmDsgnFunctions.SyncRunTimeFunctionPosition;
var
  StopLine: TrmDebugStopLineInfo;
  i: Integer;
begin
  if Supports(Apartment, IrmDsgnReport) then
  begin
    StopLine := (Apartment as IrmDsgnReport).ReportStopLineInfo;

    if Assigned(StopLine.SorceCodeLineInfo) and ObjectsAreTheSame(StopLine.rmObject, DesignObject) then
    begin
      (Apartment as IrmDsgnReport).ChangeDesignContext(cContextNameFunctions);

      for i := 0 to tvObjects.Items.Count - 1 do
        if (tvObjects.Items[i] is TrmdTreeNodeFunct) and
           AnsiSameText(TrmdTreeNodeFunct(tvObjects.Items[i]).FHandle.GetName, StopLine.SorceCodeLineInfo.FunctionName) then
        begin
          if tvObjects.Items[i].Selected then
            OnSelectFunction
          else
            tvObjects.Items[i].Selected := True;
          break;
        end;
    end

    else
      OnSelectFunction;
  end;
end;

procedure TrmDsgnFunctions.OnSelectFunction;
var
  lClassName: String;
  lLine: Integer;
  StopLine: TrmDebugStopLineInfo;
begin
  if SelectedFunction = nil then
    Exit;
    
  lClassName := '';
  lLine := 0;

  if Supports(Apartment, IrmDsgnReport) then
  begin
    StopLine := (Apartment as IrmDsgnReport).ReportStopLineInfo;

    if Assigned(StopLine.SorceCodeLineInfo) and AnsiSameText(SelectedFunction.GetName, StopLine.SorceCodeLineInfo.FunctionName) then
    begin
      lClassName := StopLine.SorceCodeLineInfo.ClassName;
      lLine := StopLine.SorceCodeLineInfo.SourceCodeLineNbr;
    end
    else
    begin
      lClassName := frmFunctionText.AncestorClassName;
      lLine := frmFunctionText.CurrentEditor.CaretY;
    end;
  end;

  frmFunctionText.ShowFunction(SelectedFunction, lClassName, lLine);
end;


{ TrmdTreeNodeGroup }

procedure TrmdTreeNodeGroup.CleanupFromFunctions;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    if Item[i] is TrmdTreeNodeGroup then
      TrmdTreeNodeGroup(Item[i]).CleanupFromFunctions;

  for i := Count - 1 downto 0 do
    if Item[i] is TrmdTreeNodeFunct then
      TrmdTreeNodeFunct(Item[i]).MoveTo(Parent, naAddChild);
end;

procedure TrmdTreeNodeGroup.UpdateGroupInfo;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    if Item[i] is TrmdTreeNodeGroup then
      TrmdTreeNodeGroup(Item[i]).UpdateGroupInfo
    else if Item[i] is TrmdTreeNodeFunct then
      TrmdTreeNodeFunct(Item[i]).UpdateGroupInfo;
  end;
end;

procedure TrmDsgnFunctions.edGroupNameExit(Sender: TObject);
var
  GrpNode: TrmdTreeNodeGroup;
begin
  GrpNode := tvObjects.Selected as TrmdTreeNodeGroup;

  if GrpNode.Text <> edGroupName.Text then
  begin
    GrpNode.Text := edGroupName.Text;
    GrpNode.UpdateGroupInfo;
    NotifyOwnerAboutChanges;
  end;  
end;

procedure TrmDsgnFunctions.edGroupNameKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    TEdit(Sender).OnExit(nil);
end;

procedure TrmDsgnFunctions.tvObjectsCompare(Sender: TObject; Node1, Node2: TTreeNode; Data: Integer; var Compare: Integer);
begin
  if (Node1 is TrmdTreeNodeGroup) and (Node2 is TrmdTreeNodeFunct) then
    Compare := -1
  else if (Node2 is TrmdTreeNodeGroup) and (Node1 is TrmdTreeNodeFunct) then
    Compare := 1
  else
    Compare := CompareStr(Node1.Text, Node2.Text);
end;

procedure TrmDsgnFunctions.tvObjectsDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (Apartment as IrmDsgnComponent).GetSecurityInfo.EditFunctions;
end;

procedure TrmDsgnFunctions.tvObjectsEndDrag(Sender, Target: TObject; X, Y: Integer);
var
  N: TTreeNode;
begin
  if Assigned(Target) then
  begin
    N := tvObjects.GetNodeAt(X, Y);
    if Assigned(N) then
    begin
      if N is TrmdTreeNodeFunct then
        tvObjects.Selected.MoveTo(N, naInsert)
      else
        tvObjects.Selected.MoveTo(N, naAddChild);

      if tvObjects.Selected is TrmdTreeNodeFunct then
        TrmdTreeNodeFunct(tvObjects.Selected).UpdateGroupInfo
      else if tvObjects.Selected is TrmdTreeNodeGroup then
        TrmdTreeNodeGroup(tvObjects.Selected).UpdateGroupInfo;

      NotifyOwnerAboutChanges;
    end;
  end;
end;

procedure TrmDsgnFunctions.miAddFunctionClick(Sender: TObject);
var
  F: IrmFunction;
  Grp: String;
begin
  F := GetTopNode.FHandle.AddItem as IrmFunction;
  F.SetFunctionText('function MyFunction(MyParam: String): String;'#10#13'end');

  if tvObjects.Selected is TrmdTreeNodeGroup then
    Grp := TrmdTreeNodeGroup(tvObjects.Selected).Text
  else if tvObjects.Selected is TrmdTreeNodeFunct then
    Grp := TrmdTreeNodeFunct(tvObjects.Selected).FHandle.GetGroup
  else
    Grp := '';

  F.SetGroup(Grp);

  GetTopNode.AddFunctNode(F).Selected := True;
  NotifyOwnerAboutChanges;
end;

procedure TrmDsgnFunctions.miDeleteFunctionClick(Sender: TObject);
var
  i: Integer;
begin
   if tvObjects.Selected is TrmdTreeNodeFunct then
     if AreYouSure then
     begin
       i := SelectedFunction.GetItemIndex;
       tvObjects.Selected.Free;
       GetTopNode.FHandle.DeleteItem(i);
       NotifyOwnerAboutChanges;
     end;
end;

procedure TrmDsgnFunctions.miDeleteGroupClick(Sender: TObject);
var
  N: TTreeNode;
begin
  tvObjects.Items.BeginUpdate;
  try
    N :=tvObjects.Selected;
    TrmdTreeNodeGroup(N).CleanupFromFunctions;
    N.Parent.Selected := True;
    N.Free;
    NotifyOwnerAboutChanges;
  finally
    tvObjects.Items.EndUpdate;
  end;
end;

procedure TrmDsgnFunctions.miAddGroupClick(Sender: TObject);
var
  N: TTReeNode;
begin
  if tvObjects.Selected is TrmdTreeNodeFunct then
    N := tvObjects.Selected.Parent
  else if tvObjects.Selected is TrmdTreeNodeGroup then
    N := tvObjects.Selected
  else
    N := GetTopNode;

  GetTopNode.AddGroupNode('New Group', N).Selected := True;

  NotifyOwnerAboutChanges;  
end;

end.
