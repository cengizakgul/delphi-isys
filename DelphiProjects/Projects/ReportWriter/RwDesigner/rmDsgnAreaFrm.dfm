inherited rmDsgnArea: TrmDsgnArea
  Width = 360
  Height = 326
  VertScrollBar.Tracking = True
  AutoScroll = True
  Color = clAppWorkSpace
  ParentColor = False
  OnResize = FrameResize
  object pmObjectProperties: TPopupMenu
    AutoPopup = False
    OnPopup = pmObjectPropertiesPopup
    Left = 35
    Top = 34
    object miObjectProperties: TMenuItem
      Caption = 'Object Properties...'
      OnClick = miObjectPropertiesClick
    end
    object miLockObject: TMenuItem
      Caption = 'Lock Obect'
      object miUnlock: TMenuItem
        Caption = 'Unlock'
        Checked = True
        RadioItem = True
        OnClick = miUnlockClick
      end
      object miLock: TMenuItem
        Caption = 'Lock'
        RadioItem = True
        OnClick = miUnlockClick
      end
      object miHide: TMenuItem
        Caption = 'Hide'
        RadioItem = True
        OnClick = miUnlockClick
      end
    end
  end
end
