unit rmDockableFrameFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs,
  rmCustomFrameFrm, rwDsgnUtils, rmDsgnTypes;

type
  TrmDockForm = class(TCustomDockForm)
  protected
    FParent: TWinControl;
    procedure SetParent(AParent: TWinControl); override;
  public
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer);override;
    function  IsShortCut(var Message: TWMKey): Boolean; override;
    constructor Create(AOwner: TComponent); override;
  end;


  TrmDockableFrame = class(TrmCustomFrame)
  private
    procedure CMVisibleChanged(var Message: TMessage); message CM_VISIBLECHANGED;
    procedure CMRelease(var Message: TMessage); message CM_RELEASE;

  protected
    procedure OnActivateApartment; override;
    procedure OnDeactivateApartment; override;
    procedure StoreState; override;
    procedure RestoreState; override;
    procedure BeforeShow; virtual;
  public
    constructor Create(AOwner: TComponent); override;
  end;

  TrmDockableFrameClass = class of TrmDockableFrame;


  procedure ShowDockableForm(AFormClass: TrmDockableFrameClass);

implementation

{$R *.dfm}


procedure ShowDockableForm(AFormClass: TrmDockableFrameClass);
var
  Frm: TrmDockableFrame;
  Apt: IrmDsgnApartment;
begin
  Apt := RMDesigner.GetActiveApartment;

  Frm := Apt.RealObject.FindComponent(AFormClass.ClassName) as TrmDockableFrame;
  if Assigned(Frm) then
  begin
    if Frm.Visible then
      Exit;
  end

  else
  begin
    Frm := AFormClass.Create(Apt.RealObject);
    Frm.Name :=  AFormClass.ClassName;
    FRm.BeforeShow;
  end;
end;


{ TrmDockForm }

constructor TrmDockForm.Create(AOwner: TComponent);
begin
  inherited;
//  Parent := RMDesigner.GetActiveApartment.RealObject;
end;

function TrmDockForm.IsShortCut(var Message: TWMKey): Boolean;
begin
  Result := inherited IsShortCut(Message);

  if not Result then
    Result := TForm(RMDesigner.VCLObject).IsShortCut(Message);
end;

procedure TrmDockForm.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited;
{  ATop := ATop - (Top - ATop);
  ALeft := ALeft - (Left - ALeft);
  inherited;}
end;

procedure TrmDockForm.SetParent(AParent: TWinControl);
begin
  if csDestroying in Self.ComponentState then
    Exit;

  FParent := AParent;
    
  if Assigned(AParent) then
    Windows.SetParent(Handle, AParent.Handle)
  else
    Windows.SetParent(Handle, 0);
end;


{ TrmDockableFrame }

procedure TrmDockableFrame.BeforeShow;
begin
  RestoreState;
end;

procedure TrmDockableFrame.CMRelease(var Message: TMessage);
begin
  Free;
end;

procedure TrmDockableFrame.CMVisibleChanged(var Message: TMessage);
begin
  inherited;

  if not Visible and Apartment.IsActive then
  begin
    StoreState;
    PostApartmentMessage(Self, mDockingStateChanged, 0, 0);
    PostMessage(Handle, CM_RELEASE, 0, 0);
  end;
end;

constructor TrmDockableFrame.Create(AOwner: TComponent);
begin
  inherited;
  FloatingDockSiteClass := TrmDockForm;
  Left := 100;
  Top := 100;
end;



procedure TrmDockableFrame.OnActivateApartment;
begin
  inherited;
  Visible := True;
end;

procedure TrmDockableFrame.OnDeactivateApartment;
begin
  inherited;
  if Floating then
    Visible := False;
end;

procedure TrmDockableFrame.RestoreState;
var
  D: TWinControl;
  P: String;
  L,T,W,H: Integer;
begin
  inherited;

  P := Apartment.Settings.ReadData(ClassName + '\Parent', '');
  if P <> '' then
  begin
    D := Owner.FindComponent(P) as TWinControl;
    if Assigned(D) and (D <> HostDockSite) then
      ManualDock(D);
  end;

  if HostDockSite = nil then
    ManualFloat(BoundsRect);

  if Floating then
  begin
    L := Apartment.Settings.ReadData(ClassName + '\Left', Parent.Left);
    T := Apartment.Settings.ReadData(ClassName + '\Top', Parent.Top);
    W := Apartment.Settings.ReadData(ClassName + '\Width', UndockWidth);
    H := Apartment.Settings.ReadData(ClassName + '\Height', UndockHeight);
    Parent.SetBounds(L, T, W, H);
    Parent.ClientWidth := W;
    Parent.ClientHeight := H;
    UndockWidth := W;
    UndockHeight := H;
  end

  else
  begin
    UndockWidth := Apartment.Settings.ReadData(ClassName + '\Width', UndockWidth);
    UndockHeight := Apartment.Settings.ReadData(ClassName + '\Height', UndockHeight);
  end;
end;

procedure TrmDockableFrame.StoreState;
begin
  inherited;

  if Floating then
  begin
    Apartment.Settings.SaveData(ClassName + '\Parent', '');
    Apartment.Settings.SaveData(ClassName + '\Left', Parent.Left);
    Apartment.Settings.SaveData(ClassName + '\Top', Parent.Top);
  end

  else
    Apartment.Settings.SaveData(ClassName + '\Parent', Parent.Name);

  Apartment.Settings.SaveData(ClassName + '\Width', UndockWidth);
  Apartment.Settings.SaveData(ClassName + '\Height', UndockHeight);
end;

end.
