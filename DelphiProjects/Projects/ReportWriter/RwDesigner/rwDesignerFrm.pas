unit rwDesignerFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, Menus, ComCtrls, ExtCtrls, ToolWin, rwPageDesignerFrm, Tabs, rwDsgnRes,
  rwBasicClasses, rwCommonClasses, EvStreamUtils, rwTypes,
  rwBasicToolsBarFrm, rwReportFormToolsBarFrm, rwDesignPaperFrm, rwDesignAreaFrm,
  rwOneRepPreviewFrm, rwReport, rwBandsPropertyEditorFrm, rwInFormToolBarFrm, rwDesignInFormFrm,
  Clipbrd, rwLibraryFrm, rwDB, mwCustomEdit, stdctrls,
  rwCtrlResizer, rwLocalVarFunctFrm, TypInfo, rmTypes, isVCLBugFix,
  rwDebugInfo, rwParser, rwVirtualMachine, rwDsgnUtils, rwUtils, isBasicUtils,
  rwRegister, rwLogQuery, rwParamFileFrm, rwCodeInsightFrm, rwMessages,
  rwCommonToolsBarFrm, sbSQL, rwBasicUtils, rwEngineTypes, rwEngine, rwDesigner;

type
  {TrwDesigner is end class for report designing}

  TrwDesignerMainForm = class(TForm)
    stbStatus: TStatusBar;
    mmMenu: TMainMenu;
    aclDesignerActions: TActionList;
    actExit: TAction;
    miFile: TMenuItem;
    miExit: TMenuItem;
    pgcPages: TPageControl;
    tbsReportForm: TTabSheet;
    tbsInputForm: TTabSheet;
    tbsPreview: TTabSheet;
    tsReportSelector: TTabSet;
    miReport: TMenuItem;
    actPageHeader: TAction;
    actTitle: TAction;
    actHeader: TAction;
    actFooter: TAction;
    actSummary: TAction;
    actPageFooter: TAction;
    actPageStyle: TAction;
    miBands: TMenuItem;
    miPageHeader: TMenuItem;
    miPageFooter: TMenuItem;
    N2: TMenuItem;
    miTitle: TMenuItem;
    miSummary: TMenuItem;
    N3: TMenuItem;
    miHeader: TMenuItem;
    miFooter: TMenuItem;
    N4: TMenuItem;
    miPageStyle: TMenuItem;
    actNew: TAction;
    miNew: TMenuItem;
    actPageFormat: TAction;
    miPageFormat: TMenuItem;
    N5: TMenuItem;
    actDelSelection: TAction;
    actCopy: TAction;
    actCut: TAction;
    actPaste: TAction;
    miEdit: TMenuItem;
    miCut: TMenuItem;
    miCopy: TMenuItem;
    miPaste: TMenuItem;
    miDelete: TMenuItem;
    actExport: TAction;
    actImport: TAction;
    miExporttoFile: TMenuItem;
    miImportfromFile: TMenuItem;
    N6: TMenuItem;
    opdImport: TOpenDialog;
    svdExport: TSaveDialog;
    rwPreview: TrwOneRepPreview;
    actGroups: TAction;
    miGroups: TMenuItem;
    N7: TMenuItem;
    actUndo: TAction;
    miUndo: TMenuItem;
    N9: TMenuItem;
    actQueryBuilder: TAction;
    QueryBuilder1: TMenuItem;
    View1: TMenuItem;
    Help1: TMenuItem;
    actLibrary: TAction;
    Library1: TMenuItem;
    actSubDetail: TAction;
    actSubSummary: TAction;
    miSubDetail: TMenuItem;
    miSubSummary: TMenuItem;
    N11: TMenuItem;
    actCustom: TAction;
    miCustom: TMenuItem;
    actFromLib: TAction;
    miLoadFromLib: TMenuItem;
    SavetoFile1: TMenuItem;
    actQuickSave: TAction;
    N1: TMenuItem;
    actSaveRes: TAction;
    actLoadRes: TAction;
    opdOpenResult: TOpenDialog;
    svdSaveResult: TSaveDialog;
    N13: TMenuItem;
    miCompile: TMenuItem;
    actCompile: TAction;
    actPCode: TAction;
    actWatches: TAction;
    actStepOver: TAction;
    actTraceInto: TAction;
    actBrkPoint: TAction;
    actAddWatch: TAction;
    Run1: TMenuItem;
    AddWatch1: TMenuItem;
    BreakPointOnOff1: TMenuItem;
    TraceInto1: TMenuItem;
    StepOver1: TMenuItem;
    N14: TMenuItem;
    DebugerWindows1: TMenuItem;
    Watches1: TMenuItem;
    PCode1: TMenuItem;
    actRun: TAction;
    actPause: TAction;
    Run2: TMenuItem;
    actPause1: TMenuItem;
    ReportFormToolsBar: TrwReportFormToolsBar;
    frmReportForm: TrwPageDesigner;
    InFormToolsBar: TrwInFormToolsBar;
    frmInputForm: TrwPageDesigner;
    actTerminate: TAction;
    Terminate1: TMenuItem;
    actParamFile: TAction;
    actParamFile1: TMenuItem;
    actDSExplorer: TAction;
    DataSetExplorer1: TMenuItem;
    miReportStat: TMenuItem;
    actEnvOpt: TAction;
    EnvironmentOptionts1: TMenuItem;
    actEvaluate: TAction;
    Evaluate1: TMenuItem;
    pmLicencing: TMenuItem;
    actSecurity: TAction;
    procedure actExitExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure actNewExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tsReportSelectorChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure actSummaryExecute(Sender: TObject);
    procedure actPageFooterExecute(Sender: TObject);
    procedure actTitleExecute(Sender: TObject);
    procedure actPageHeaderExecute(Sender: TObject);
    procedure actHeaderExecute(Sender: TObject);
    procedure actFooterExecute(Sender: TObject);
    procedure actPageStyleExecute(Sender: TObject);
    procedure actPageFormatExecute(Sender: TObject);
    procedure actDelSelectionExecute(Sender: TObject);
    procedure pgcPagesChange(Sender: TObject);
    procedure actImportExecute(Sender: TObject);
    procedure actExportExecute(Sender: TObject);
    procedure miBandsClick(Sender: TObject);
    procedure actGroupsExecute(Sender: TObject);
    procedure actPasteExecute(Sender: TObject);
    procedure actCutExecute(Sender: TObject);
    procedure actCopyExecute(Sender: TObject);
    procedure actUndoExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actQueryBuilderExecute(Sender: TObject);
    procedure actLibraryExecute(Sender: TObject);
    procedure Help1Click(Sender: TObject);
    procedure actSubDetailExecute(Sender: TObject);
    procedure actSubSummaryExecute(Sender: TObject);
    procedure actCustomExecute(Sender: TObject);
    procedure rwPreviewactPrintExecute(Sender: TObject);
    procedure actFromLibExecute(Sender: TObject);
    procedure actQuickSaveExecute(Sender: TObject);
    procedure actCompileExecute(Sender: TObject);
    procedure actPCodeExecute(Sender: TObject);
    procedure actStepOverExecute(Sender: TObject);
    procedure actRunExecute(Sender: TObject);
    procedure actPauseExecute(Sender: TObject);
    procedure actTerminateExecute(Sender: TObject);
    procedure actBrkPointExecute(Sender: TObject);
    procedure actTraceIntoExecute(Sender: TObject);
    procedure actWatchesExecute(Sender: TObject);
    procedure actAddWatchExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure actParamFileExecute(Sender: TObject);
    procedure actDSExplorerExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure miReportStatClick(Sender: TObject);
    procedure ReportFormToolsBartbtEventsClick(Sender: TObject);
    procedure actEnvOptExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure actEvaluateExecute(Sender: TObject);
    procedure rwPreviewpnlPageNumClick(Sender: TObject);
    procedure tbsReportFormShow(Sender: TObject);
    procedure tbsReportFormHide(Sender: TObject);
    procedure tbsInputFormHide(Sender: TObject);
    procedure tbsInputFormShow(Sender: TObject);
    procedure actSecurityExecute(Sender: TObject);
    procedure tbsPreviewHide(Sender: TObject);

  private
    FIncomingStream: IEvDualStream;
    FOldHelpFile: string;
    FOldAppOnMessage: TMessageEvent;
    FChainClipBrdHWnd: HWnd;
    FDesignPapersReports: TList;
    FCurrentReportPaper: TrwDesignPaper;
    FInputForm: TrwDesignInForm;
    FCurrentPageDesigner: TrwPageDesigner;
    FDefaultReportName: String;
    FReportParamFile: String;
    FPreviewActions: TActionList;
    FRunningReport: TrwReport;
    FaComboIsOpened: Boolean;

    function GetDesignPaperReport(Index: Integer): TrwDesignPaper;
    function GetDesignPaperReportCount: Integer;
    procedure OnAppMessage(var Msg: TMsg; var Handled: Boolean);

    procedure CheckNeedForVisibleReportSelector;
    procedure OnPaperMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ClickComponentsToolBar(Sender: TObject; AComponentClassName: string; AMultiCreation: Boolean);
    procedure ClickActionToolBar(Sender: TObject; AAction: TrwActionToolBarEvent);
    procedure ClickExpertToolBar(Sender: TObject; AAction: TrwExpertToolBarEvent; AParam: TObject = nil);
    procedure ChangesOnFontToolBar(Sender: TObject; EventType: TrwFontToolBarEvent);
    procedure ChangesOnTextToolBar(Sender: TObject; AText: string);
    procedure AfterCreateNewComponent(Sender: TObject; AComponent: TrwComponent; AParent: TrwControl; X, Y: Integer);
    procedure BeforeDestroyComponent(Sender: TObject; AComponent: TrwComponent);
    procedure AfterDestroyContainer(Sender: TObject);
    procedure BandOnOff(Sender: TAction; ABandType: TrwBandType);
    procedure SyncDesignByCurrentReport;
    procedure ChangedComponentProperty(Sender: TObject; AComponent: TrwComponent);
    procedure ChangeSelectComponent(Sender: TObject; AComponent: TrwComponent);
    function  CheckBreakPoint(Sender: TObject; ALine: Integer): Boolean;
    procedure RefreshReportPropertyEditor(Sender: TObject; AComponent: TrwComponent);
    procedure RefreshInFormPropertyEditor(Sender: TObject; AComponent: TrwComponent);

    procedure DemandImageComponent(Sender: TObject; AClassName: string; AImage: TBitmap);
    procedure InitInputFormForDesign;
    procedure SyncInputFormForDesign;
    procedure WMChangeClipbrd(var M: TMessage); message WM_DRAWCLIPBOARD;
    procedure WMChangeClipbrdChain(var M: TMessage); message WM_CHANGECBCHAIN;
    procedure OnDestroyReport(Sender: TObject);
    procedure OnChangeReportName(Sender: TObject);
    procedure ShowDefProperty(AObject: TObject; APropName: string);
    procedure ExitFromControl;
    procedure ShowCompileError(AError: ErwParserError);
    procedure ShowRunTimeError(AError: ErwVMError);
    procedure RecreateBuffers;
    procedure RunReport;
    procedure RunDebugging(const AGoByStep: Boolean);
    procedure SetButtons;
    procedure SetRunButtons;
    function  SyncSourcePosition: Boolean;
    procedure SetCurrentDebEvent;
    function  SwitchToProperTab(const ACompPath: String): String;
    procedure RestoreDesignMode;
    procedure SetBrkPointsAddr(const AIndex: Integer = -1);
    procedure SyncEnvironment;
    procedure SetReadOnly(AValue: Boolean);

    procedure WMChangeCurrentAsmPosition(var Message: TMessage); message RW_CHANGE_DEBUG_ASM_POSITION;
    procedure WMChangeCurrentAsmCS(var Message: TMessage); message RW_CHANGE_DEBUG_ASM_CS;
    procedure WMBreakPointFound(var Message: TMessage); message RW_BREAKPOINT_FOUND;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;

  public
    constructor Create(AComponent: TComponent); override;
    destructor  Destroy; override;

    property CurrentReportPaper: TrwDesignPaper read FCurrentReportPaper;
    property InputForm: TrwDesignInForm read FInputForm;
    property CurrentPageDesigner: TrwPageDesigner read FCurrentPageDesigner;
    property DesignPaperReport[Index: Integer]: TrwDesignPaper read GetDesignPaperReport;
    property DesignPaperReportCount: Integer read GetDesignPaperReportCount;

    procedure SyncDesignByReport;
    function  GetComponentByPath(ACompPath: string): TrwComponent;
    function  GetDebSymbInfo(const ADebSymbInfo: PTrwDebInf; const AIndent: string; const AEvent: string): string;
    function  GetDebInfo(const ADebInfo: PTrwDebInf; ASymbInfo: string; const ALine: Integer): string;
    function  CreateReport(AOwner: TrwDesignPaper; AReport: TrwCustomReport = nil): TrwCustomReport;
    procedure DestroyReport;
    procedure LoadFromStream(AStream: TStream);
    procedure ShowCursorPos(Sender: TObject);
    procedure ShowMouseCursorPos(X, Y: Integer);
    procedure ComponentSelection(Sender: TObject; AComponent: TrwComponent; GroupSelection: Boolean);
    procedure OnChange;
  end;


  TrwDesignerFormHolder = class(TInterfacedObject, IrwDesignerForm)
  protected
    function  IsClosed: Boolean;
    function  GetData: IEvDualStream;
  public
    constructor Create(const AReportRes: IEvDualStream; const AReportName: String);
    destructor  Destroy; override;
  end;


var
  ReportDesigner: TrwDesignerMainForm;

  procedure ShowModalRWDesignerWithReport(const AReportRes: IEvDualStream; const AReportName: String);
  function  ShowRWDesigner(const AReportRes: IEvDualStream; const AReportName: String): IrwDesignerForm;

implementation

{$R *.DFM}

uses rwPagePropertyEditorFrm, rwDesignFormFrm,
     rwQueryBuilderFrm, rwLibCompChoiceFrm, rwVMCPUFrm, rwWatchListFrm,
     rwDataSetExplorerFrm, rwStatFrm, rwEnvOptionsFrm, rwEvaluateFrm,
     rwReportControls;

var
  ResultFileName: string;


function ShowRWDesigner(const AReportRes: IEvDualStream; const AReportName: String): IrwDesignerForm;
begin
  CheckCondition(not Assigned(ReportDesigner), 'Designer is already active');
  Result := TrwDesignerFormHolder.Create(AReportRes, AReportName);
end;


procedure ShowModalRWDesignerWithReport(const AReportRes: IEvDualStream; const AReportName: String);
begin
  ReportDesigner := TrwDesignerMainForm.Create(nil);

  with ReportDesigner do
  begin
    try
      FIncomingStream := AReportRes;
      stbStatus.Panels[0].Text := AReportName;
      ShowModal;
    finally
      Free;
      ReportDesigner := nil;
    end;
  end;
end;



procedure TrwDesignerMainForm.actExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TrwDesignerMainForm.FormDestroy(Sender: TObject);
begin
  try
    RWDesigning := False;
    DestroyReport;
    DsgnRes.Free;
    FreeDebugInfo;
    FPreviewActions.Free;
  finally
    FDesignPapersReports.Free;
    Application.OnMessage := FOldAppOnMessage;
    Application.HelpFile := FOldHelpFile;
  end;
end;

procedure TrwDesignerMainForm.DestroyReport;
begin
  if not (csDestroying in ComponentState) then
  begin
    frmInputForm.ShowComponentProperties(nil, False);
    frmReportForm.ShowComponentProperties(nil, False);
  end;

  if Assigned(FInputForm) then
    FInputForm.Container.ClearSelection;

  FCurrentReportPaper := nil;

  if DesignPaperReportCount > 0 then
    DesignPaperReport[0].Report.Free;

  FreeAndNil(FInputForm);
end;

procedure TrwDesignerMainForm.OnDestroyReport(Sender: TObject);
var
  i, j: Integer;
begin
  for i := 0 to DesignPaperReportCount - 1 do
    if DesignPaperReport[i].Report = Sender then
    begin
      with TrwCustomReport(Sender) do
        for j := 0 to ReportForm.ComponentCount - 1 do
          TrwComponent(ReportForm.Components[j]).OnDestroy := nil;

      DesignPaperReport[i].Report := nil;
      DesignPaperReport[i].Free;
      FDesignPapersReports.Delete(i);
      tsReportSelector.Tabs.Delete(i);
      CheckNeedForVisibleReportSelector;
      break;
    end;
end;

procedure TrwDesignerMainForm.OnChangeReportName(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to DesignPaperReportCount - 1 do
    if DesignPaperReport[i].Report = Sender then
    begin
      tsReportSelector.Tabs[i] := GetComponentPath(DesignPaperReport[i].Report);
      break;
    end;
end;

procedure TrwDesignerMainForm.CheckNeedForVisibleReportSelector;
begin
  tsReportSelector.Visible := (tsReportSelector.Tabs.Count > 1);
end;

procedure TrwDesignerMainForm.actNewExecute(Sender: TObject);
begin
  if Sender <> nil then
    if MessageDlg('Current opened report will be cleaned. Proceed operation?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
      AbortEx;

  DestroyReport;
  stbStatus.Panels[0].Text := '';
  CreateReport(nil);
  InitInputFormForDesign;
  tsReportSelector.TabIndex := 0;
end;

function TrwDesignerMainForm.CreateReport(AOwner: TrwDesignPaper; AReport: TrwCustomReport = nil): TrwCustomReport;
var
  lPaper: TrwDesignPaper;
begin
  if not Assigned(AReport) then
    if (AOwner = nil) then
    begin
      Result := TrwReport.Create(nil);
      Result.Name := 'Report';
    end
    else
      Result := TrwSubReport.Create(AOwner.Report.ReportForm)
  else
    Result := AReport;

  Result.OnDestroy := OnDestroyReport;
  Result.OnChangeName := OnChangeReportName;

  lPaper := TrwDesignPaper.Create(nil);
  lPaper.Parent := frmReportForm.sbxDsgnFrame;
  lPaper.Visible := False;
  lPaper.Report := Result;
  lPaper.pnlArea.OnMouseMove := OnPaperMouseMove;
  lPaper.OnSelectComponent := ComponentSelection;
  lPaper.OnCreateNewComponent := AfterCreateNewComponent;
  lPaper.OnDestroyComponent := BeforeDestroyComponent;
  lPaper.AfterDestroyContainer := AfterDestroyContainer;
  lPaper.OnDemandImageComponent := DemandImageComponent;
  lPaper.OnSetComponentProperty := RefreshReportPropertyEditor;
  lPaper.OnShowDefProperty := ShowDefProperty;
  lPaper.ClearSelection;

  FDesignPapersReports.Add(lPaper);
  tsReportSelector.Tabs.Add(GetComponentPath(Result));

  CheckNeedForVisibleReportSelector;
end;

procedure TrwDesignerMainForm.FormCreate(Sender: TObject);
begin
  FOldHelpFile := Application.HelpFile;
  Application.HelpFile := ExtractFilePath(Application.ExeName) + 'rwHelp.hlp';

  FaComboIsOpened := False;

  FOldAppOnMessage := Application.OnMessage;
  Application.OnMessage := OnAppMessage;

  DsgnRes;

  FRunningReport := nil;
  RWDesigning := True;
  ResultFileName :=  GetThreadTmpDir + 'Result.rwa';
  FDefaultReportName := 'RW_Report.rwr';
  FReportParamFile := '';
  actSecurity.Enabled := rwDesignerExt.AppAdapter.GetSecurityDescriptor.LicencedReports.Modify;

  FDesignPapersReports := TList.Create;
  ReportFormToolsBar.OnClickComponentsToolBar := ClickComponentsToolBar;
  ReportFormToolsBar.OnClickActionToolBar := ClickActionToolBar;
  ReportFormToolsBar.OnClickExpertToolBar := ClickExpertToolBar;
  ReportFormToolsBar.OnChangesFontToolBar := ChangesOnFontToolBar;
  ReportFormToolsBar.OnChangesTextToolBar := ChangesOnTextToolBar;
  frmReportForm.OnChangePropertySelectComponent := ChangedComponentProperty;
  frmReportForm.OnChangeSelectComponent := ChangeSelectComponent;
  frmReportForm.OnCheckBreakPoint := CheckBreakPoint;

  InFormToolsBar.OnClickComponentsToolBar := ClickComponentsToolBar;
  InFormToolsBar.OnClickActionToolBar := ClickActionToolBar;
  InFormToolsBar.OnClickExpertToolBar := ClickExpertToolBar;
  InFormToolsBar.OnChangesFontToolBar := ChangesOnFontToolBar;
  InFormToolsBar.OnChangesTextToolBar := ChangesOnTextToolBar;
  frmInputForm.OnChangePropertySelectComponent := ChangedComponentProperty;
  frmInputForm.OnChangeSelectComponent := ChangeSelectComponent;
  frmInputForm.OnCheckBreakPoint := CheckBreakPoint;

  frmInputForm.pnlVertRuler.Hide;
  frmInputForm.pnlHorRuler.Hide;
  DesignUnits := utScreenPixels;

  SetButtons;

  FCurrentPageDesigner := frmReportForm;
  actNewExecute(nil);
  pgcPages.OnChange(nil);

  InitDebugInfo(Self);
end;

function TrwDesignerMainForm.GetDesignPaperReport(Index: Integer): TrwDesignPaper;
begin
  Result := TrwDesignPaper(FDesignPapersReports[Index]);
end;

function TrwDesignerMainForm.GetDesignPaperReportCount: Integer;
begin
  Result := FDesignPapersReports.Count;
end;

procedure TrwDesignerMainForm.tsReportSelectorChange(Sender: TObject;
  NewTab: Integer; var AllowChange: Boolean);
begin
  if (tsReportSelector.TabIndex <> -1) then
    DesignPaperReport[tsReportSelector.TabIndex].Visible := False;

  DesignPaperReport[NewTab].Visible := True;

  ExitFromControl;
  if Assigned(FCurrentReportPaper) then
    FCurrentReportPaper.ClearSelection;
  FCurrentReportPaper := DesignPaperReport[NewTab];

  SyncDesignByCurrentReport;
end;

procedure TrwDesignerMainForm.OnPaperMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  frmReportForm.ShowPositionOnRuler(X, Y);
  ShowMouseCursorPos(X, Y);
end;

procedure TrwDesignerMainForm.ChangesOnTextToolBar(Sender: TObject; AText: string);
begin
  if (Sender = ReportFormToolsBar) then
    DesignPaperReport[tsReportSelector.TabIndex].SetPropertyForSelection(['Text'], AText)

  else
    if (Sender = InFormToolsBar) then
    FInputForm.Container.SetPropertyForSelection(['Text'], AText);

  TrwCommonToolsBar(Sender).edtText.SetFocus;
end;

procedure TrwDesignerMainForm.ClickComponentsToolBar(Sender: TObject; AComponentClassName: string; AMultiCreation: Boolean);
begin
  if (Sender = ReportFormToolsBar) then
    DesignPaperReport[tsReportSelector.TabIndex].SetNewComponentClassName(AComponentClassName, AMultiCreation)

  else if (Sender = InFormToolsBar) then
    FInputForm.Container.SetNewComponentClassName(AComponentClassName, AMultiCreation)
end;

procedure TrwDesignerMainForm.ClickActionToolBar(Sender: TObject; AAction: TrwActionToolBarEvent);
begin
  if (Sender = ReportFormToolsBar) then
    DesignPaperReport[tsReportSelector.TabIndex].ActionForSelection(AAction)

  else if (Sender = InFormToolsBar) then
    FInputForm.Container.ActionForSelection(AAction);
end;

procedure TrwDesignerMainForm.ClickExpertToolBar(Sender: TObject; AAction: TrwExpertToolBarEvent; AParam: TObject = nil);
var
  S: TStream;
begin
  if (Sender = ReportFormToolsBar) then
    if AAction = eteReportWizard then
    begin
      S := TStream(AParam);
      stbStatus.Panels[0].Text := '';
      actNew.Execute;
      S.Position := 0;
      TrwReport(DesignPaperReport[0].Report).LoadFromStream(S);
      SyncDesignByReport;
      actCompile.Execute;
    end
    else
      DesignPaperReport[tsReportSelector.TabIndex].ExpertForSelection(AAction)
  else
    if (Sender = InFormToolsBar) then
      FInputForm.Container.ExpertForSelection(AAction);
end;

procedure TrwDesignerMainForm.ChangesOnFontToolBar(Sender: TObject; EventType: TrwFontToolBarEvent);
begin
  if (Sender = ReportFormToolsBar) then
    frmReportForm.ChangeFontStatus(DesignPaperReport[tsReportSelector.TabIndex], ReportFormToolsBar, EventType)

  else if (Sender = InFormToolsBar) then
    frmInputForm.ChangeFontStatus(FInputForm.Container, InFormToolsBar, EventType);
end;

procedure TrwDesignerMainForm.BeforeDestroyComponent(Sender: TObject; AComponent: TrwComponent);
begin
{  if Assigned(DebugInfo.DebuggerForm) then
    with TrwDebugger(DebugInfo.DebuggerForm) do
      for i := BreakPointsList.Count - 1 downto 0 do
        if (BreakPointsList[i].ComponentName = AnsiUpperCase(AComponent.Name)) then
          BreakPointsList.Delete(i);
}
  if (Sender.ClassType = TrwDesignPaper) and (Sender = FCurrentReportPaper) then
    frmReportForm.DeleteComponentFromInspector(AComponent)
  else
    if (Sender.ClassType = TrwDesignForm) then
      frmInputForm.DeleteComponentFromInspector(AComponent);
end;

procedure TrwDesignerMainForm.AfterDestroyContainer(Sender: TObject);
begin
  SyncDesignByCurrentReport;
end;

procedure TrwDesignerMainForm.AfterCreateNewComponent(Sender: TObject; AComponent: TrwComponent; AParent: TrwControl; X, Y: Integer);
begin
  if (Sender.ClassType = TrwDesignPaper) then
    frmReportForm.AddNewComponentToInspector(AComponent, ReportFormToolsBar)

  else
    if (Sender.ClassType = TrwDesignForm) then
    frmInputForm.AddNewComponentToInspector(AComponent, InFormToolsBar);
end;

procedure TrwDesignerMainForm.ComponentSelection(Sender: TObject; AComponent: TrwComponent; GroupSelection: Boolean);
begin
  if AComponent is TrwReportForm then
    AComponent := TrwComponent(TrwReportForm(AComponent).Owner);

  if (Sender.ClassType = TrwDesignPaper) then
  begin
    ReportFormToolsBar.SetToolBarByComponentProperties(AComponent, GroupSelection);
    frmReportForm.ShowComponentProperties(AComponent, GroupSelection);
  end

  else if (Sender.ClassType = TrwDesignForm) then
  begin
    InFormToolsBar.SetToolBarByComponentProperties(AComponent, GroupSelection);
    frmInputForm.ShowComponentProperties(AComponent, GroupSelection);
  end;
end;

procedure TrwDesignerMainForm.BandOnOff(Sender: TAction; ABandType: TrwBandType);
begin
  if not FCurrentReportPaper.IsBandExists(ABandType) then
    FCurrentReportPaper.CreateBand(ABandType)
  else
    FCurrentReportPaper.DeleteBand(ABandType);
end;

procedure TrwDesignerMainForm.SyncDesignByReport;

  procedure SyncPaper(APaper: TrwDesignPaper);

    procedure SyncComponents(AOwner: TrwComponent);
    var
      i: Integer;
    begin
      for i := 0 to AOwner.ComponentCount - 1 do
      begin
        if not (AOwner.Components[i] is TrwComponent) or (AOwner.Components[i].InheritsFrom(TrwGlobalVarFunct)) then
          Continue;
        if AOwner.Components[i].InheritsFrom(TrwNoVisualComponent) then
          APaper.InitNoVisualComponent(TrwNoVisualComponent(AOwner.Components[i]));

        APaper.PrepDsgnComp(TrwComponent(AOwner.Components[i]));

        if (AOwner.Components[i] is TrwSubReport) then
        begin
          CreateReport(APaper, TrwSubReport(AOwner.Components[i]));
          SyncPaper(DesignPaperReport[tsReportSelector.Tabs.Count - 1]);
        end;
      end;
    end;

  begin
    APaper.SyncBandsByPaper;
    SyncComponents(APaper.Report.ReportForm);
  end;

  procedure SyncInputForm;
  var
    i: Integer;
  begin
    SyncInputFormForDesign;

    for i := 0 to TrwReport(FCurrentReportPaper.Report).InputForm.ComponentCount - 1 do
    begin
      if not TrwReport(FCurrentReportPaper.Report).InputForm.Components[i].InheritsFrom(TrwComponent) then
        Continue;

      if TrwReport(FCurrentReportPaper.Report).InputForm.Components[i].InheritsFrom(TrwNoVisualComponent) then
        FInputForm.Container.InitNoVisualComponent(TrwNoVisualComponent(
          TrwReport(FCurrentReportPaper.Report).InputForm.Components[i]));

      FInputForm.Container.PrepDsgnComp(TrwComponent(TrwReport(FCurrentReportPaper.Report).InputForm.Components[i]));
    end;
  end;

begin
  if (FCurrentReportPaper.Report is TrwReport) and
      TrwReport(FCurrentReportPaper.Report).IsLicenced  and
      not rwDesignerExt.AppAdapter.GetSecurityDescriptor.LicencedReports.Modify then
  begin
    try
      actNewExecute(nil);
    finally
      raise Exception.Create(ResErrAccessDenied);
    end;
  end;

  SyncPaper(FCurrentReportPaper);
  if FCurrentReportPaper.Report is TrwReport then
    SyncInputForm;
  tsReportSelector.TabIndex := 0;
  SyncDesignByCurrentReport;
end;

procedure TrwDesignerMainForm.SyncDesignByCurrentReport;
begin
  frmReportForm.SyncComponentsList(FCurrentReportPaper.Report, ReportFormToolsBar);
  frmReportForm.ShowComponentProperties(FCurrentReportPaper.Report);
end;

procedure TrwDesignerMainForm.InitInputFormForDesign;
begin
  if Assigned(FInputForm) then
    FInputForm.Free;

  FInputForm := TrwDesignInForm.Create(nil);
  FInputForm.Parent := frmInputForm.sbxDsgnFrame;
  FInputForm.Top := 10;
  FInputForm.Left := 10;
  FInputForm.Container.OnSelectComponent := ComponentSelection;
  FInputForm.Container.OnCreateNewComponent := AfterCreateNewComponent;
  FInputForm.Container.OnDestroyComponent := BeforeDestroyComponent;
  FInputForm.Container.AfterDestroyContainer := AfterDestroyContainer;
  FInputForm.Container.OnDemandImageComponent := DemandImageComponent;
  FInputForm.Container.OnSetComponentProperty := RefreshInFormPropertyEditor;
  FInputForm.Container.OnShowDefProperty := ShowDefProperty;

  FInputForm.Visible := True;

  SyncInputFormForDesign;
end;

procedure TrwDesignerMainForm.SyncInputFormForDesign;
begin
  FInputForm.Container.SetContainer(TrwReport(DesignPaperReport[0].Report).InputForm);
  frmInputForm.SyncComponentsList(TrwReport(DesignPaperReport[0].Report).InputForm, InFormToolsBar);
  frmInputForm.ShowComponentProperties(TrwReport(DesignPaperReport[0].Report).InputForm);
  TrwReport(DesignPaperReport[0].Report).InputForm.DesignParent := FInputForm.Container;
end;

procedure TrwDesignerMainForm.actSummaryExecute(Sender: TObject);
begin
  BandOnOff(actSummary, rbtSummary);
end;

procedure TrwDesignerMainForm.actPageFooterExecute(Sender: TObject);
begin
  BandOnOff(actPageFooter, rbtPageFooter);
end;

procedure TrwDesignerMainForm.actTitleExecute(Sender: TObject);
begin
  BandOnOff(actTitle, rbtTitle);
end;

procedure TrwDesignerMainForm.actPageHeaderExecute(Sender: TObject);
begin
  BandOnOff(actPageHeader, rbtPageHeader);
end;

procedure TrwDesignerMainForm.actHeaderExecute(Sender: TObject);
begin
  BandOnOff(actHeader, rbtHeader);
end;

procedure TrwDesignerMainForm.actFooterExecute(Sender: TObject);
begin
  BandOnOff(actFooter, rbtFooter);
end;

procedure TrwDesignerMainForm.actPageStyleExecute(Sender: TObject);
begin
  BandOnOff(actPageStyle, rbtPageStyle);
end;

procedure TrwDesignerMainForm.RefreshReportPropertyEditor(Sender: TObject; AComponent: TrwComponent);
begin
  FCurrentReportPaper.RefreshCompResizer;
  frmReportForm.RefreshComponentProperties;
end;

procedure TrwDesignerMainForm.RefreshInFormPropertyEditor(Sender: TObject; AComponent: TrwComponent);
begin
  FInputForm.Container.RefreshCompResizer;
  frmInputForm.RefreshComponentProperties;
end;

procedure TrwDesignerMainForm.ChangedComponentProperty(Sender: TObject; AComponent: TrwComponent);
begin
  if (Sender = frmReportForm) then
  begin
    ReportFormToolsBar.SetToolBarByComponentProperties(AComponent, False);
    FCurrentReportPaper.RefreshCompResizer;
    frmReportForm.RefreshComponentProperties;
  end

  else if (Sender = frmInputForm) then
  begin
    InFormToolsBar.SetToolBarByComponentProperties(AComponent, False);
    FInputForm.Container.RefreshCompResizer;
    frmInputForm.RefreshComponentProperties;
  end;
end;

procedure TrwDesignerMainForm.ChangeSelectComponent(Sender: TObject; AComponent: TrwComponent);
begin
  if (Sender = frmReportForm) then
    if (AComponent is TrwControl) or
       (AComponent is TrwNoVisualComponent) and not (AComponent is TrwReport) then
      FCurrentReportPaper.AddSelection(AComponent, False)
    else
      FCurrentReportPaper.ClearSelection
  else
    if (Sender = frmInputForm) then
      if (AComponent is TrwControl) or (AComponent is TrwNoVisualComponent) then
        FInputForm.Container.AddSelection(AComponent, False)
      else
        FInputForm.Container.ClearSelection;
end;

procedure TrwDesignerMainForm.actPageFormatExecute(Sender: TObject);
begin
  PageSetup(FCurrentReportPaper.Report.PageFormat);
end;

procedure TrwDesignerMainForm.actDelSelectionExecute(Sender: TObject);
begin
  if (ActiveControl is TrwControlResizer) then
    if pgcPages.ActivePage = tbsReportForm then
      FCurrentReportPaper.DestroySelectedComponents
    else if pgcPages.ActivePage = tbsInputForm then
      FInputForm.Container.DestroySelectedComponents;
end;


procedure TrwDesignerMainForm.pgcPagesChange(Sender: TObject);
begin
  FCurrentPageDesigner := nil;

  if pgcPages.ActivePage = tbsPreview then
  begin
    if not Assigned(rwPreview.aclActions) then
    begin
      rwPreview.InsertComponent(FPreviewActions);
      rwPreview.aclActions := FPreviewActions;
      FPreviewActions := nil;
    end;
  end

  else
  begin
    if Assigned(rwPreview.aclActions) then
    begin
      FPreviewActions := rwPreview.aclActions;
      rwPreview.RemoveComponent(FPreviewActions);
    end;

    if pgcPages.ActivePage = tbsReportForm then
    begin
      ShowCursorPos(frmReportForm.EventText);
      FCurrentPageDesigner := frmReportForm;
    end

    else if pgcPages.ActivePage = tbsInputForm then
    begin
      FCurrentPageDesigner := frmInputForm;
      ShowCursorPos(frmInputForm.EventText);
    end;

    if tbsPreview.TabVisible then
    begin
      tbsPreview.TabVisible := False;
      rwPreview.BeforeClosePreview;
      SetRunButtons;
      RestoreDesignMode;
    end;
  end;
end;


procedure TrwDesignerMainForm.actImportExecute(Sender: TObject);
begin
  if opdImport.Execute then
  begin
    Busy;
    try
      actNew.Execute;
      frmInputForm.ShowComponentProperties(nil, False);
      frmReportForm.ShowComponentProperties(nil, False);
      TrwReport(DesignPaperReport[0].Report).LoadFromFile(opdImport.FileName);
      stbStatus.Panels[0].Text := opdImport.FileName;
      stbStatus.Panels[1].Text := '';
      SyncDesignByReport;
      svdExport.FileName := opdImport.FileName;
    finally
      Ready;
    end;
  end;
end;

procedure TrwDesignerMainForm.DemandImageComponent(Sender: TObject; AClassName: string; AImage: TBitmap);
begin
  if (Sender.ClassType = TrwDesignPaper) then
    ReportFormToolsBar.AssignImageForComponent(AClassName, AImage)

  else if (Sender.ClassType = TrwDesignForm) then
    InFormToolsBar.AssignImageForComponent(AClassName, AImage);
end;

procedure TrwDesignerMainForm.actExportExecute(Sender: TObject);
var
  fl: Boolean;
  h: String;
begin
  if svdExport.FileName = '' then
  begin
    svdExport.FileName := FDefaultReportName;
    fl := True;
  end
  else
    fl := False;

  h := svdExport.FileName;
  svdExport.FileName := ChangeFileExt(ExtractFileName(h), '');
  svdExport.InitialDir := ExtractFileDir(h);
  svdExport.DefaultExt := ExtractFileExt(h);
  
  if svdExport.Execute then
  begin
    Busy;
    try
      ExitFromControl;
      TrwReport(DesignPaperReport[0].Report).SaveToFile(svdExport.FileName);
      if stbStatus.Panels[0].Text = '' then
        stbStatus.Panels[0].Text := svdExport.FileName;
      stbStatus.Panels[1].Text := '';
    finally
      Ready;
    end;
  end
  else if fl then
    svdExport.FileName := '';
end;

procedure TrwDesignerMainForm.miBandsClick(Sender: TObject);
var
  i: Integer;
begin
  miPageHeader.Checked := False;
  miTitle.Checked := False;
  miHeader.Checked := False;
  miFooter.Checked := False;
  miSummary.Checked := False;
  miPageFooter.Checked := False;
  miPageStyle.Checked := False;
  miSubSummary.Checked := False;
  miSubDetail.Checked := False;
  miCustom.Checked := False;

  for i := 0 to FCurrentReportPaper.BandCount - 1 do
    case FCurrentReportPaper.Bands[i].BandType of
      rbtPageHeader: miPageHeader.Checked := True;
      rbtTitle: miTitle.Checked := True;
      rbtHeader: miHeader.Checked := True;
      rbtFooter: miFooter.Checked := True;
      rbtSummary: miSummary.Checked := True;
      rbtPageFooter: miPageFooter.Checked := True;
      rbtPageStyle: miPageStyle.Checked := True;
      rbtSubSummary: miSubSummary.Checked := True;
      rbtSubDetail: miSubDetail.Checked := True;
      rbtCustom: miCustom.Checked := True;
    end;
end;

procedure TrwDesignerMainForm.actGroupsExecute(Sender: TObject);
begin
  if ReportBands(FCurrentReportPaper.Report.Bands) then
    SyncDesignByCurrentReport;
end;

procedure TrwDesignerMainForm.WMChangeClipbrd(var M: TMessage);
begin
  actPaste.Enabled :=  Clipboard.HasFormat(CF_RWDATA) or
                       Clipboard.HasFormat(CF_TEXT);
  if FChainClipBrdHWnd <> 0 then
    SendMessage(FChainClipBrdHWnd, WM_DRAWCLIPBOARD, M.wParam, M.lParam);
end;

procedure TrwDesignerMainForm.WMChangeClipbrdChain(var M: TMessage);
begin
  if HWND(M.wParam) = FChainClipBrdHWnd then
    FChainClipBrdHWnd := M.lParam
  else
    SendMessage(FChainClipBrdHWnd, WM_CHANGECBCHAIN, M.wParam, M.lParam);

  M.Result := 0;
end;

procedure TrwDesignerMainForm.actPasteExecute(Sender: TObject);
begin
  if Assigned(ActiveControl) and not (ActiveControl is TrwControlResizer) then
    ActiveControl.Perform(WM_PASTE, 0, 0)
  else if Clipboard.HasFormat(CF_RWDATA) then
    if pgcPages.ActivePage = tbsReportForm then
      FCurrentReportPaper.Paste
    else if pgcPages.ActivePage = tbsInputForm then
      FInputForm.Container.Paste;
end;

procedure TrwDesignerMainForm.actCutExecute(Sender: TObject);
begin
  if not (ActiveControl is TrwControlResizer) then
    ActiveControl.Perform(WM_CUT, 0, 0)
  else if pgcPages.ActivePage = tbsReportForm then
    FCurrentReportPaper.Cut
  else if pgcPages.ActivePage = tbsInputForm then
    FInputForm.Container.Cut;
end;

procedure TrwDesignerMainForm.actCopyExecute(Sender: TObject);
begin
  if not (ActiveControl is TrwControlResizer) then
    ActiveControl.Perform(WM_COPY, 0, 0)
  else if pgcPages.ActivePage = tbsReportForm then
    FCurrentReportPaper.Copy
  else if pgcPages.ActivePage = tbsInputForm then
    FInputForm.Container.Copy;
end;

procedure TrwDesignerMainForm.actUndoExecute(Sender: TObject);
begin
  if pgcPages.ActivePage = tbsReportForm then
    FCurrentReportPaper.UnDo
  else if pgcPages.ActivePage = tbsInputForm then
    FInputForm.Container.UnDo;
end;

procedure TrwDesignerMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ExitFromControl;
  if (IsCompLibInitialized and SystemLibComponents.Modified or
     IsFunctLibInitialized and  SystemLibFunctions.Modified) and
    (MessageDlg('RW Library has been changed. Do you want to save a changes to the database right now?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    if IsCompLibInitialized and SystemLibComponents.Modified then
      SystemLibComponents.Store;

    if IsFunctLibInitialized and SystemLibFunctions.Modified then
      SystemLibFunctions.Store;
  end;

  if pgcPages.ActivePage = tbsReportForm then
    ReportFormToolsBar.SaveState
  else if pgcPages.ActivePage = tbsInputForm then
    InFormToolsBar.SaveState;
  ReportFormToolsBar.UndockedBarsVisible(False);
  InFormToolsBar.UndockedBarsVisible(False);

  ChangeClipboardChain(Handle, FChainClipBrdHWnd);

  if Assigned(FIncomingStream) then
  begin
    FIncomingStream.Size := 0;
    TrwReport(DesignPaperReport[0].Report).SaveToStream(FIncomingStream.RealStream);
    FIncomingStream.Position := 0;
  end

  else
  begin
    if (svdExport.FileName = '') or (svdExport.FileName <> '') and
       (MessageDlg('Report will be saved to file "'+svdExport.FileName+'". Do you want to do this?',
         mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
      actQuickSave.Execute;
  end;
end;

procedure TrwDesignerMainForm.actQueryBuilderExecute(Sender: TObject);
begin
  ShowQueryBuilder(nil, False);
end;

procedure TrwDesignerMainForm.actLibraryExecute(Sender: TObject);
begin
  ShowLibrary;
end;

procedure TrwDesignerMainForm.Help1Click(Sender: TObject);
begin
  Application.HelpCommand(HELP_CONTENTS, 0);
end;

procedure TrwDesignerMainForm.actSubDetailExecute(Sender: TObject);
begin
  BandOnOff(actSubDetail, rbtSubDetail);
end;

procedure TrwDesignerMainForm.actSubSummaryExecute(Sender: TObject);
begin
  BandOnOff(actSubSummary, rbtSubSummary);
end;

procedure TrwDesignerMainForm.actCustomExecute(Sender: TObject);
begin
  BandOnOff(actSubSummary, rbtCustom);
end;

procedure TrwDesignerMainForm.ShowCursorPos(Sender: TObject);
begin
  stbStatus.Panels[2].Text := IntToStr(TmwCustomEdit(Sender).CaretY)+' : '+
    IntToStr(TmwCustomEdit(Sender).CaretX);
end;

procedure TrwDesignerMainForm.ShowMouseCursorPos(X, Y: Integer);
begin
  stbStatus.Panels[3].Text := FormatFloat('0.###', ConvertUnit(X, utScreenPixels, DesignUnits)) +
  ' : ' + FormatFloat('0.###', ConvertUnit(Y, utScreenPixels, DesignUnits));
end;

procedure TrwDesignerMainForm.rwPreviewactPrintExecute(Sender: TObject);
begin
  rwPreview.actPrintExecute(Sender);
end;

procedure TrwDesignerMainForm.actFromLibExecute(Sender: TObject);
var
  LibComp: TrwLibComponent;
  R: TrwReport;
  MS: TMemoryStream;
begin
  LibComp := ChoiceLibComp([rctReportAncestor]);
  if Assigned(LibComp) then
  begin
    Busy;
    stbStatus.Panels[0].Text := '';
    actNew.Execute;
    R := TrwReport(LibComp.CreateComp(nil, True));
    MS := TIsMemoryStream.Create;
    try
      R.SaveToStream(MS);
      MS.Position := 0;
      TrwReport(DesignPaperReport[0].Report).LoadFromStream(MS);
      TrwReport(DesignPaperReport[0].Report).Name := 'Report';
    finally
      MS.Free;
      R.Free;
    end;
    SyncDesignByReport;
    Ready;
  end;
end;

procedure TrwDesignerMainForm.actQuickSaveExecute(Sender: TObject);
var
  i: Integer;
  h1, h2: String;
  ext: String;
  hTemp: String;
begin
  if svdExport.FileName = '' then
    actExport.Execute
  else
  begin
    Busy;
    try
      ExitFromControl;
      CheckReadOnlyAttr(svdExport.FileName);

      hTemp := GetThreadTmpDir + 'temp' + ExtractFileExt(svdExport.FileName);
      TrwReport(DesignPaperReport[0].Report).SaveToFile(hTemp);

      for i := 9 downto 1 do
      begin
        h1 := GetTmpDir + 'rwQuickSave'+IntToStr(i) + '.';
        ext := 'rwr';
        if not FileExists(h1+ext) then
          ext := 'txt';
        if not  FileExists(h1+ext) then
          Continue;

        h1 := h1+ext+#0;
        h2 := GetTmpDir + 'rwQuickSave'+IntToStr(i+1)+'.'+ext+#0;
        Windows.CopyFile(@h1[1], @h2[1], False);
      end;

      h1 := svdExport.FileName;
      ext := ExtractFileExt(h1);
      h1 := h1 + #0;
      h2:= GetTmpDir+'rwQuickSave1'+ext+#0;
      Windows.CopyFile(@h1[1], @h2[1], False);

      h1 := hTemp + #0;
      h2:= svdExport.FileName+#0;
      Windows.CopyFile(@h1[1], @h2[1], False);
      DeleteFile(hTemp);

      if stbStatus.Panels[0].Text = '' then
        stbStatus.Panels[0].Text := svdExport.FileName;
      stbStatus.Panels[1].Text := '';
    finally
      Ready;
    end;
  end;
end;

procedure TrwDesignerMainForm.OnChange;
begin
  stbStatus.Panels[1].Text := 'Modified';
end;

procedure TrwDesignerMainForm.ShowDefProperty(AObject: TObject; APropName: string);
begin
  if AObject is TrwReportForm then
    AObject := TrwComponent(TrwReportForm(AObject).Owner);

  if pgcPages.ActivePage = tbsReportForm then
    frmReportForm.ShowDefProperty(TrwComponent(AObject), APropName)
  else if pgcPages.ActivePage = tbsInputForm then
    frmInputForm.ShowDefProperty(TrwComponent(AObject), APropName);
end;


procedure TrwDesignerMainForm.ExitFromControl;
var
  M: TMethod;
begin
  if Assigned(ActiveControl) and IsPublishedProp(ActiveControl, 'OnExit') then
  begin
    M := GetMethodProp(ActiveControl, 'OnExit');
    if Assigned(M.Code) then
      TNotifyEvent(M)(ActiveControl);
  end;
end;

procedure TrwDesignerMainForm.actCompileExecute(Sender: TObject);
begin
  ExitFromControl;

  if Length(TrwReport(DesignPaperReport[0].Report).Name) = 0 then
    raise ErwException.Create(ResErrEmptyName);

  try
    Busy;
    try
      actCompile.Tag := 1;
      TrwReport(DesignPaperReport[0].Report).Compile;
    finally
      Ready;
    end;
    actCompile.Tag := 0;
  except
    on E: ErwParserError do
      ShowCompileError(E);
    else
      raise;
  end;
end;


procedure TrwDesignerMainForm.WMChangeCurrentAsmCS(var Message: TMessage);
var
  F: TrwVMCPU;
  P1, P2: PTrwDebInf;
begin
  if IsVMCPUVisible then
  begin
    F := ShowVMCPU;
    F.SyncCodeSegment;
  end;

  case DebugInfo.CodeSegment of
    rcsReport:        begin
                        P1 := Addr(TrwReport(DesignPaperReport[0].Report).DebInf);
                        P2 := Addr(TrwReport(DesignPaperReport[0].Report).DebSymbInf);
                      end;

    rcsLibFunctions:  begin
                        P1 := SystemLibFunctions.GetDebInf;
                        P2 := SystemLibFunctions.GetDebSymbInf;
                      end;

    rcsLibComponents: begin
                        P1 := SystemLibComponents.GetDebInf;
                        P2 := SystemLibComponents.GetDebSymbInf;
                      end;
  else
    P1 := nil;
    P2 := nil;
  end;

  if P1 <> DebugInfo.PDebInfo then
  begin
    DebugInfo.CurDebLine := -1;
    DebugInfo.LineBegAddr := -1;
    DebugInfo.LineEndAddr := -1;
    DebugInfo.PDebInfo := P1;
    DebugInfo.PDebSymbInfo := P2;
  end;
end;


procedure TrwDesignerMainForm.WMChangeCurrentAsmPosition(var Message: TMessage);
var
  F: TrwVMCPU;
  fl: Boolean;
begin
  fl := SyncSourcePosition;

  if IsVMCPUVisible then
  begin
    F := ShowVMCPU;
    F.SyncPosition;
    SetCurrentDebEvent;
    Ready;
    DebugInfo.Waiting := True;
  end

  else if not fl then
    DebugInfo.Waiting := False
  else
  begin
    SetCurrentDebEvent;
    Ready;
    DebugInfo.Waiting := True;
  end;

  if DebugInfo.Waiting then
  begin
    if IsWatchVisible then
      ShowWatch.Initialize;

    if IsDSExplorerVisible then
      if IsWatchVisible then
        ShowDSExplorer.Synchronize
      else
        ShowDSExplorer.Initialize;
  end;

  SetRunButtons;
end;


procedure TrwDesignerMainForm.WMBreakPointFound(var Message: TMessage);
begin
  if (DebugInfo.BreakPointsList[Message.WParam].Line = -1) and not IsVMCPUVisible then
    actPCode.Execute;
end;



procedure TrwDesignerMainForm.actPCodeExecute(Sender: TObject);
var
  F: TrwVMCPU;
begin
  F := ShowVMCPU;
  F.SyncCodeSegment;
  F.SyncPosition;
  SetCurrentDebEvent;
  DebugInfo.GoByStep := True;
  DebugInfo.Waiting := True;
end;


procedure TrwDesignerMainForm.actStepOverExecute(Sender: TObject);
begin
  if Debugging then
  begin
    BusyBackground;
    DebugInfo.GoByStep := True;
    DebugInfo.TraceInto := False;
    DebugInfo.Waiting := False;
    SetRunButtons;
  end
  else
    RunDebugging(True);
end;


procedure TrwDesignerMainForm.ShowCompileError(AError: ErwParserError);
var
  i: Integer;
begin
  if (AError.LibComponentName <> '') and (AError.ComponentName <> '') then
  begin
    i := SystemLibComponents.IndexOf(AError.LibComponentName);
    if not SystemLibComponents[i].GetBaseClass.ClassNameIs('TrwReport') then
    begin
      pgcPages.ActivePage := tbsReportForm;
      ShowLibrary(AError.EventName, AError.LineNum, AError.ColNum + 1, AError.LibComponentName, AError.ComponentName, AError.Message);
    end;
  end

  else if AError.OwnerName <> '' then
  begin
    SwitchToProperTab(AError.ComponentPath);
    if AError.ComponentName = VAR_FUNCT_COMP_NAME then       // Global Functions error
    begin
      ShowError(AError.Message);
      ShowGlobalVarFunct(DesignPaperReport[0].Report.ReportForm, AError.EventName, AError.LibComponentName,
                         AError.LineNum, AError.ColNum + 1);
    end
    else
    begin
      FCurrentPageDesigner.ShowComponentEvent(AError.ComponentPath, AError.EventName, AError.LibComponentName);
      FCurrentPageDesigner.Position := Point(AError.ColNum + 1, AError.LineNum);
      ShowError(AError.Message);
    end;
  end

  else  // Function's error
  begin
    pgcPages.ActivePage := tbsReportForm;
    ShowLibrary(AError.EventName, AError.LineNum, AError.ColNum + 1, AError.Message);
  end;
end;


procedure TrwDesignerMainForm.ShowRunTimeError(AError: ErwVMError);
begin
  DebugInfo.ErrorHappend := True;
  DebugInfo.CodeSegment := AError.CS;
  DebugInfo.Waiting := False;
  DebugInfo.CurrentAsmPosition := AError.IP;
  if not DebugInfo.Waiting then
    SetCurrentDebEvent;
  CurrentPageDesigner.RunTimeErrorLine := DebugInfo.Line;
  CurrentPageDesigner.InvalidateEventLine(DebugInfo.Line);
  ShowError(AError.GetFullMessage);
end;


procedure TrwDesignerMainForm.RecreateBuffers;
var
  i, j: Integer;
begin
  for i := 0 to DesignPaperReportCount - 1 do
    for j := 0 to DesignPaperReport[i].Report.ReportForm.ComponentCount - 1 do
      if DesignPaperReport[i].Report.ReportForm.Components[j] is TrwBuffer then
        if (Length(TrwBuffer(DesignPaperReport[i].Report.ReportForm.Components[j]).TableName) > 0) and
           (TrwBuffer(DesignPaperReport[i].Report.ReportForm.Components[j]).Fields.Count > 0) then
          TrwBuffer(DesignPaperReport[i].Report.ReportForm.Components[j]).CreateBuffer;

  for j := 0 to FInputForm.Container.ComponentCount - 1 do
    if FInputForm.Container.Components[j] is TrwBuffer then
      if (Length(TrwBuffer(FInputForm.Container.Components[j]).TableName) > 0) and
         (TrwBuffer(FInputForm.Container.Components[j]).Fields.Count > 0) then
        TrwBuffer(FInputForm.Container.Components[j]).CreateBuffer;
end;


procedure TrwDesignerMainForm.RunReport;
var
  MS: TMemoryStream;
  s: String;

  procedure ReadParamsFromFile;
  var
    P: TrwReportParamList;
    F: TFileStream;
  begin
    try
      F := TFileStream.Create(FReportParamFile, fmOpenRead);
      try
        P := RWDesignerExt.AppAdapter.ReadReportParamsFromStream(F);
        FRunningReport.SetReportParams(@P);
      finally
        F.Free;
      end;
    except
      raise ErwException.Create('Error reading of external report parameter file');
    end;
  end;

begin
  Busy;

  FRunningReport := TrwReport.Create(nil);
  try
    SetReadOnly(True);
    SetBrkPointsAddr;

    MS := TIsMemoryStream.Create;
    try
      TrwReport(DesignPaperReport[0].Report).SaveToStream(MS);
      MS.Position := 0;
      FRunningReport.LoadFromStream(MS);
    finally
      MS.Free;
    end;

    if Length(FReportParamFile) > 0 then
      ReadParamsFromFile;

    BusyBackground;

    try
      FRunningReport.Prepare;
      FRunningReport.Print(ResultFileName, True);

      s := ResultFileName;
      if not FileExists(s) then
      begin
        s := ChangeFileExt(s, '.txt');
        if not FileExists(s) then
        begin
          //if the EXCEL ASCIIDelimeter is set, the extension
          //of the file was changed prior to reaching this stage
          //of the code. the above changefileext was looking for .rwa, which
          //was non-existen, thus the Excel preview did not show.
          //change it back, and test for it's existence.
          //Fixes Reso. 96694.
          s := ChangeFileExt(s, '.xls');
          if not FileExists(s) then
            s := '';
        end;

      end;

      if s <> '' then
      begin
        tbsPreview.TabVisible := True;
        pgcPages.ActivePage := tbsPreview;
        pgcPages.OnChange(nil);

        rwPreview.PreviewReport(s);
        FCurrentPageDesigner := nil;
      end;

    except
      on E: ErwVMError do
        ShowRunTimeError(E)
      else
        raise;
    end;

  finally
    try
      DebugInfo.Initialize;
      CloseVMCPU;
      CloseWatch;
      CloseDSExplorer;
      FRunningReport.Free;
      FRunningReport := nil;
      RecreateBuffers;
    finally
      SetButtons;
      Ready;

      SetReadOnly(False);
    end;
  end;
end;


procedure TrwDesignerMainForm.actRunExecute(Sender: TObject);
begin
  if Debugging then
  begin
    BusyBackground;
    DebugInfo.GoByStep := False;
    DebugInfo.TraceInto := False;
    DebugInfo.Waiting := False;
    DebugInfo.DontStop := True;
    SetRunButtons;
  end

  else
    RunDebugging(False);
end;


procedure TrwDesignerMainForm.SetButtons;
var
  fl: Boolean;
begin
  fl :=  not Debugging;
  with ReportFormToolsBar do
  begin
    tlbExperts.Enabled := fl;
    tlbComponents.Enabled := fl;
    tlbText.Enabled := fl;
    tlbFont.Enabled := fl;
    tlbSize.Enabled := fl;
    tlbAlignment.Enabled := fl;
  end;

  with InFormToolsBar do
  begin
    tlbExperts.Enabled := fl;
    tlbComponents.Enabled := fl;
    tlbText.Enabled := fl;
    tlbFont.Enabled := fl;
    tlbSize.Enabled := fl;
    tlbAlignment.Enabled := fl;
  end;

  actTerminate.Enabled := not fl;
  actPCode.Enabled := not fl;
  actWatches.Enabled := not fl;
  actEvaluate.Enabled := not fl;
  actDSExplorer.Enabled := not fl;
  actAddWatch.Enabled := not fl;

  actCut.Enabled := fl;
  actPaste.Enabled := fl;
  actDelSelection.Enabled := fl;
  actUndo.Enabled := fl;
  miBands.Enabled := fl;
  miPageFormat.Enabled := fl;

  SetRunButtons;
end;


procedure TrwDesignerMainForm.SetRunButtons;
begin
  if Debugging then
  begin
    actRun.Enabled := DebugInfo.Waiting;
    actPause.Enabled := not DebugInfo.Waiting;
    actStepOver.Enabled := DebugInfo.Waiting;
    actTraceInto.Enabled := DebugInfo.Waiting;
  end

  else
  begin
    actRun.Enabled := not tbsPreview.TabVisible;
    actPause.Enabled := False;
    actStepOver.Enabled := False;
    actTraceInto.Enabled := not tbsPreview.TabVisible;
  end;
end;


procedure TrwDesignerMainForm.actPauseExecute(Sender: TObject);
begin
  DebugInfo.GoByStep := True;
  DebugInfo.TraceInto := False;
  DebugInfo.Waiting := True;
end;


function TrwDesignerMainForm.SyncSourcePosition: Boolean;
var
  lAddr1, lAddr2, i: Integer;
  lLine, h1, h2: String;
  lStep: Smallint;
begin
  Result := False;

  with DebugInfo do
  begin
    if CurrentAsmPosition = -1 then
      Exit;

    if CurrentAsmPosition < LineBegAddr then
      lStep := -1
    else if CurrentAsmPosition > LineEndAddr then
      lStep := 1
    else if CurrentAsmPosition = LineBegAddr then
      lStep := 0
    else
      Exit;

    i := CurDebLine + lStep;
    while (i >= 0) and (i <= High(PDebInfo^)) do
    begin
      lLine := PDebInfo^[i];
      lAddr1 := StrToInt(GetNextStrValue(lLine, ';'));
      lAddr2 := StrToInt(GetNextStrValue(lLine, ';'));

      if (lStep = 1) and (CurrentAsmPosition < lAddr1) or
         (lStep = -1) and (CurrentAsmPosition > lAddr2) then
        break;

      if (CurrentAsmPosition >= lAddr1) and (CurrentAsmPosition <= lAddr2) then
      begin
        LineBegAddr := lAddr1;
        LineEndAddr := lAddr2;
        CurDebLine := i;
        Result := True;
        break;
      end;
      i := i + lStep;
    end;

    if Result then
    begin
      lLine := PDebInfo^[CurDebLine];
      GetNextStrValue(lLine, ';');
      GetNextStrValue(lLine, ';');
      h1 := Identif;

      Identif := GetNextStrValue(lLine, ';');
      case DebugInfo.CodeSegment of
        rcsLibComponents: CurrentLibClass := GetNextStrValue(Identif, '.');
        rcsLibFunctions:  CurrentLibClass := cParserLibFunct;
      else
        CurrentLibClass := '';
      end;

      Identif := GetComponentPath(TrwComponent(CurrentComponent));
      h2 := CurrentEvent;
      CurrentEvent := GetNextStrValue(lLine, ';');
      Line := StrToInt(GetNextStrValue(lLine, ';'));

      if (IsWatchVisible or IsDSExplorerVisible) and (not SameText(h1, Identif) or not SameText(h2, CurrentEvent)) then
        DebugInfo.WatchList.NeedCompileWatches := True;
    end;
  end;
end;

procedure TrwDesignerMainForm.SetCurrentDebEvent;
begin
  if DebugInfo.CurrentLibClass = cParserLibFunct then   //?????
    Exit;

  SwitchToProperTab(DebugInfo.Identif);

  FCurrentPageDesigner.ShowComponentEvent(DebugInfo.Identif, DebugInfo.CurrentEvent, DebugInfo.CurrentLibClass);
  DebugInfo.CurrentComponent := FCurrentPageDesigner.rwPropertiesEditor.CurrentComponent;
  try
    FCurrentPageDesigner.Position := Point(1, DebugInfo.Line);  //prevent focus to disable window
    FCurrentPageDesigner.CurrentHandler.TopLine := DebugInfo.Line - (FCurrentPageDesigner.CurrentHandler.LinesInWindow div 2);
  except
  end;
end;


function TrwDesignerMainForm.SwitchToProperTab(const ACompPath: String): String;
var
  h1, h2: string;
  i: Integer;
  fl: Boolean;
  TS: TTabSheet;
  TabInd: Integer;
begin
  TS := nil;
  TabInd := -1;
  Result := ACompPath;
  h2 := '';
  repeat
    h1 := GetNextStrValue(Result, '.');
    if h2 <> '' then
      h2 := h2 + '.';
    h2 := h2 + h1;
    fl := False;
    for i := 0 to DesignPaperReportCount - 1 do
      if SameText(GetComponentPath(DesignPaperReport[i].Report), h2) then
      begin
        TS := tbsReportForm;
        TabInd := i;
        fl := True;
        break;
      end;

    if not fl and SameText(GetComponentPath(TrwReport(DesignPaperReport[0].Report).InputForm), h2) then
    begin
      TS := tbsInputForm;
      TabInd := -1;
      fl := True;
    end;

  until not fl;

  if h1 <> '' then
  begin
    if Result <> '' then
      Result := '.' + Result;
    Result := h1 + Result;
  end;

  if not Assigned(TS) or
     (TS = tbsReportForm) and (pgcPages.ActivePage = TS) and (tsReportSelector.TabIndex = TabInd) or
     (TS = tbsInputForm) and (pgcPages.ActivePage = TS) then
    Exit;

  pgcPages.ActivePage := TS;
  if TabInd <> -1 then
  begin
    fl := True;
    tsReportSelector.TabIndex := TabInd;
    tsReportSelector.OnChange(nil, TabInd, fl);
  end;
  pgcPages.OnChange(nil);
end;


procedure TrwDesignerMainForm.actTerminateExecute(Sender: TObject);
begin
  DebugInfo.Abort;
end;

procedure TrwDesignerMainForm.RestoreDesignMode;
begin
  SwitchToProperTab(GetComponentPath(DesignPaperReport[0].Report));
  frmReportForm.ShowComponentEvent(GetComponentPath(DesignPaperReport[0].Report), DesignPaperReport[0].Report.EventsList[0].Name, '');
end;


procedure TrwDesignerMainForm.actBrkPointExecute(Sender: TObject);
var
  i: Integer;
begin
  i := DebugInfo.BreakPointsList.FindBreakPoint(FCurrentPageDesigner.rwPropertiesEditor.CurrentCompPath,
       FCurrentPageDesigner.rwPropertiesEditor.CurrentEventName, FCurrentPageDesigner.CurrentLibClass,
       FCurrentPageDesigner.Position.Y);
  if i = -1 then
  begin
    i := DebugInfo.BreakPointsList.Add(FCurrentPageDesigner.rwPropertiesEditor.CurrentCompPath,
      FCurrentPageDesigner.rwPropertiesEditor.CurrentEventName, FCurrentPageDesigner.CurrentLibClass,
      FCurrentPageDesigner.Position.Y, -1);
    if Debugging then
      SetBrkPointsAddr(i);
  end

  else
    DebugInfo.BreakPointsList.Delete(i);
  FCurrentPageDesigner.InvalidateEventLine(FCurrentPageDesigner.Position.Y);
  if Debugging and IsVMCPUVisible then
    ShowVMCPU.InvalidateEventHandler;
end;


function TrwDesignerMainForm.CheckBreakPoint(Sender: TObject; ALine: Integer): Boolean;
begin
  if Assigned(FCurrentPageDesigner) then
    Result := DebugInfo.BreakPointsList.FindBreakPoint(FCurrentPageDesigner.rwPropertiesEditor.CurrentCompPath,
       FCurrentPageDesigner.rwPropertiesEditor.CurrentEventName, FCurrentPageDesigner.CurrentLibClass, ALine) <> -1
  else
    Result := False;
end;


procedure TrwDesignerMainForm.SetBrkPointsAddr(const AIndex: Integer = -1);
var
  i, a, n, m: Integer;
  Psymb, Pdeb: PTrwDebInf;
  h, lCompPath: string;
  C, Own: TrwComponent;
  lBaseAddr: Integer;
begin
  if AIndex = -1 then
  begin
    n := 0;
    m := DebugInfo.BreakPointsList.Count - 1;
  end

  else
  begin
    n := AIndex;
    m := AIndex;
  end;

  for i := n to m do
  begin
    DebugInfo.BreakPointsList.SetAddress(i, -1);
    C := GetComponentByPath(DebugInfo.BreakPointsList[i].ComponentPath);
    if not Assigned(C) then
      continue;

    if Length(DebugInfo.BreakPointsList[i].LibClass) > 0 then
    begin
      Own := C;
      while Assigned(Own) and not Own.IsInheritsFrom(DebugInfo.BreakPointsList[i].LibClass) do
        Own := TrwComponent(Own.Owner);

      if Assigned(Own) then
      begin
        h := DebugInfo.BreakPointsList[i].ComponentPath;
        lCompPath := GetComponentPath(Own);
        Delete(h, 1, Length(lCompPath));
        lCompPath := DebugInfo.BreakPointsList[i].LibClass + h;
      end
      else
        lCompPath := '';

      Pdeb := SystemLibComponents.GetDebInf;
      Psymb := SystemLibComponents.GetDebSymbInf;
      lBaseAddr := GetOffSet(rcsLibComponents, Length(TrwReport(DesignPaperReport[0].Report).CompiledCode) + rwVMHeapSize);
    end
    else
    begin
      lCompPath := DebugInfo.BreakPointsList[i].ComponentPath;
      Pdeb := Addr(TrwReport(DesignPaperReport[0].Report).DebInf);
      Psymb := Addr(TrwReport(DesignPaperReport[0].Report).DebSymbInf);
      lBaseAddr := 0;
    end;

    h := GetDebSymbInfo(Psymb, lCompPath, DebugInfo.BreakPointsList[i].Event);
    if Length(h) = 0 then
      continue;

    h := GetDebInfo(Pdeb, h, DebugInfo.BreakPointsList[i].Line);
    if Length(h) = 0 then
      continue;
    a := StrToInt(GetNextStrValue(h, ';')) + lBaseAddr;
    DebugInfo.BreakPointsList.SetAddress(i, a);
  end;

  if AIndex = -1 then
  begin
    i := 0;
    while i <= m do
    begin
      if DebugInfo.BreakPointsList[i].Address = -1 then
      begin
       DebugInfo.BreakPointsList.Delete(i);
       Dec(m);
      end
      else
        Inc(i);
    end;
  end
  else
   if DebugInfo.BreakPointsList[AIndex].Address = -1 then
     DebugInfo.BreakPointsList.Delete(AIndex);
end;


procedure TrwDesignerMainForm.RunDebugging(const AGoByStep: Boolean);
begin
  ExitFromControl;

  if DsgnTraceLib then
  begin
    if (Length(SystemLibFunctions.GetDebInf^) = 0) or (Length(SystemLibComponents.GetDebInf^) = 0) then
    begin
      RWEngineExt.AppAdapter.StartDesignerProgressBar('Creating debugging information for library...');
      try
        SystemLibFunctions.CreateDebInf;
        SystemLibComponents.CreateDebInf;
      finally
        RWEngineExt.AppAdapter.EndDesignerProgressBar;
      end;
    end
  end
  else
  begin
    SystemLibFunctions.ClearDebInf;
    SystemLibComponents.ClearDebInf;
  end;

  if DsgnDontAskCompileConfirm or
     (Length(TrwReport(DesignPaperReport[0].Report).CompiledCode) = 0) or
     (Length(TrwReport(DesignPaperReport[0].Report).DebInf) > 0) or
     (Length(TrwReport(DesignPaperReport[0].Report).DebInf) = 0) and
     (MessageDlg('The report does not have debugging information. Do you want to compile for creating one?',
                 mtWarning, [mbYes, mbNo], 0) = mrYes) then
  begin
    actCompile.Execute;
    if actCompile.Tag <> 0 then
      Exit;
  end

  else
    ShowVMCPU;

  DebugInfo.Initialize;
  DebugInfo.Report := TrwReport(DesignPaperReport[0].Report);
  DebugInfo.GoByStep := AGoByStep;
  SetButtons;
  RunReport;
end;



procedure TrwDesignerMainForm.actTraceIntoExecute(Sender: TObject);
begin
  if Debugging then
  begin
    BusyBackground;
    DebugInfo.GoByStep := True;
    DebugInfo.TraceInto := True;
    DebugInfo.Waiting := False;
    SetRunButtons;
  end
  else
    RunDebugging(True);
end;


procedure TrwDesignerMainForm.actWatchesExecute(Sender: TObject);
begin
  ShowWatch;
end;


procedure TrwDesignerMainForm.actAddWatchExecute(Sender: TObject);
var
  F: TrwWatchList;
begin
  F := ShowWatch;
  F.AddWatch;
end;



procedure TrwDesignerMainForm.FormActivate(Sender: TObject);
begin
  if IsWatchVisible then
    ShowWatch;

  if IsDSExplorerVisible then
    ShowDSExplorer;

  if IsVMCPUVisible then
    ShowVMCPU;
end;


function TrwDesignerMainForm.GetComponentByPath(ACompPath: string): TrwComponent;
var
  h1, h2: string;
  i: Integer;
  fl: Boolean;
begin
  Result := nil;
  h2 := '';
  repeat
    h1 := GetNextStrValue(ACompPath, '.');
    if h2 <> '' then
      h2 := h2 + '.';
    h2 := h2 + h1;
    fl := False;
    for i := 0 to DesignPaperReportCount - 1 do
      if SameText(GetComponentPath(DesignPaperReport[i].Report), h2) then
      begin
        Result := DesignPaperReport[i].Report;
        fl := True;
        break;
      end;

    if not fl and SameText(GetComponentPath(TrwReport(DesignPaperReport[0].Report).InputForm), h2) then
    begin
      Result := TrwReport(DesignPaperReport[0].Report).InputForm;
      fl := True;
    end;

  until not fl;

  if not Assigned(Result) then
    Exit;

  if h1 <> '' then
  begin
    if ACompPath <> '' then
      ACompPath := '.' + ACompPath;
    ACompPath := h1 + ACompPath;
  end;

  while Length(ACompPath) > 0 do
  begin
    h2 := GetNextStrValue(ACompPath, '.');
    i := Result.FindVirtualChild(h2);
    if i = -1 then
    begin
      Result := nil;
      break;
    end
    else
      Result := TrwComponent(Result.GetVirtualChild(i));
  end;
end;


function TrwDesignerMainForm.GetDebSymbInfo(const ADebSymbInfo: PTrwDebInf; const AIndent: string; const AEvent: string): string;
var
  i: Integer;
  h1, h2: string;
begin
  Result := '';
  for i := 0 to High(ADebSymbInfo^) do
  begin
    h1 := ADebSymbInfo^[i];
    h2 := GetNextStrValue(h1, ';');
    if SameText(h2, AIndent) then
    begin
      h2 := GetNextStrValue(h1, ';');
      if SameText(h2, AEvent) then
      begin
        Result := ADebSymbInfo^[i];
        break;
      end;
    end;
  end;
end;


function TrwDesignerMainForm.GetDebInfo(const ADebInfo: PTrwDebInf; ASymbInfo: string; const ALine: Integer): string;
var
  i, j, lFirstAddr, lEndAddr: Integer;
  h: string;
begin
  Result := '';
  GetNextStrValue(ASymbInfo, ';');
  GetNextStrValue(ASymbInfo, ';');
  lFirstAddr := StrToInt(GetNextStrValue(ASymbInfo, ';'));
  lEndAddr := StrToInt(GetNextStrValue(ASymbInfo, ';'));

  for i := lFirstAddr to lEndAddr do
  begin
    h := ADebInfo^[i];
    GetNextStrValue(h, ';');
    GetNextStrValue(h, ';');
    GetNextStrValue(h, ';');
    GetNextStrValue(h, ';');
    j := StrToInt(GetNextStrValue(h, ';'));
    if j = ALine then
    begin
      Result := ADebInfo^[i];
      break;
    end
    else if j > ALine then
      break;
  end;
end;

procedure TrwDesignerMainForm.actParamFileExecute(Sender: TObject);
begin
  ShowParamFile(FReportParamFile);
end;



procedure TrwDesignerMainForm.actDSExplorerExecute(Sender: TObject);
begin
  ShowDSExplorer;
end;


procedure TrwDesignerMainForm.FormShow(Sender: TObject);
var
  RepName: String;
begin
  RepName := stbStatus.Panels[0].Text;

  if Assigned(FIncomingStream) and (FIncomingStream.Size > 0) then
  begin
    FIncomingStream.Position := 0;
    try
      TrwReport(DesignPaperReport[0].Report).LoadFromStream(FIncomingStream.RealStream);
      SyncDesignByReport;
    except
      on E: Exception do
        ShowMessage(E.Message);
    end;
  end;

  FDefaultReportName := NormalizeFileName(RepName)+'.rwr';

  FChainClipBrdHWnd := SetClipboardViewer(Handle);
  Perform(WM_DRAWCLIPBOARD, 0, 0);

  tbsReportForm.Show;
  SyncEnvironment;
end;


procedure TrwDesignerMainForm.miReportStatClick(Sender: TObject);
begin
  ShowReportStatistics;
end;

procedure TrwDesignerMainForm.ReportFormToolsBartbtEventsClick(Sender: TObject);
begin
  ExitFromControl;
  ReportFormToolsBar.tbtEventsClick(Sender);
end;


procedure TrwDesignerMainForm.WMSysCommand(var Message: TWMSysCommand);
begin
  HideCodeInsightWnds;
  inherited;
end;

procedure TrwDesignerMainForm.actEnvOptExecute(Sender: TObject);
begin
  if ShowEnvOptions then
    SyncEnvironment;
end;


procedure TrwDesignerMainForm.SyncEnvironment;
begin
  frmReportForm.SetUnits(DesignUnits);
end;

procedure TrwDesignerMainForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  pgcPages.ActivePage := tbsReportForm;
  pgcPages.OnChange(tbsReportForm);

  if DsgnAutoCompile then
  begin
    CanClose := False;
    actCompile.Execute;
    if actCompile.Tag = 0 then
      CanClose := True;
  end
  else
    CanClose := True;
end;


procedure TrwDesignerMainForm.OnAppMessage(var Msg: TMsg; var Handled: Boolean);
var
  C: TControl;
  W: TWinControl;
  PS: TPoint;

  procedure SendMsg(AControl: TControl; APoint: TPoint);
  var
    M: TMethod;
    ST: TShiftState;
    X, Y, N: Integer;
    pST: Pointer;
  begin
    M := GetMethodProp(AControl, 'OnMouseDown');
    if M.Code = nil then
      Exit;

    ST := KeysToShiftState(Msg.WParam);
    X := APoint.X;
    Y := APoint.Y;
    pST := Addr(ST);
    N := (PByte(pST))^;

    asm
      MOV   EDX, AControl;
      MOV   ECX, mbLeft;
      PUSH  N;
      PUSH  X;
      PUSH  Y;
      MOV   EAX, M.Data;
      CALL  M.Code;
    end;

    Handled := True;    
  end;


begin
  Handled := False;

  if Msg.Message = WM_LBUTTONDOWN then
  begin
    W := FindControl(Msg.hwnd);
    if not Assigned(W) or not Supports(W, IrwVisualControl) or not(csDesigning in W.ComponentState) then
      Exit;

    if (Msg.lParam > -1) and (Msg.lParam > -1) then
    begin
      PS.X := LoWord(Msg.lParam);
      PS.Y := HiWord(Msg.lParam);
      C := W.ControlAtPos(PS, True, True);
      PS := W.ClientToScreen(PS);
      if not Assigned(C) or not Supports(C, IrwVisualControl) then
        if C is TrwControlResizer then
          Exit
        else
          C := W;
      PS := C.ScreenToClient(PS);
      SendMsg(C, PS);
    end;
  end;
end;


constructor TrwDesignerMainForm.Create(AComponent: TComponent);
begin
  InitCacheInfo;
  InitializationSbVM;
  CreateRwVM(nil);

  inherited;
end;


destructor TrwDesignerMainForm.Destroy;
begin
  DestroyRwVM;
  DeInitializationSbVM;
  FreeCacheInfo;

  inherited;
end;

procedure TrwDesignerMainForm.actEvaluateExecute(Sender: TObject);
var
  h: String;
begin
  h := FCurrentPageDesigner.CurrentHandler.SelText;
  EvaluateExpr(h);
end;

procedure TrwDesignerMainForm.LoadFromStream(AStream: TStream);
begin
  Busy;
  stbStatus.Panels[0].Text := '';
  actNew.Execute;
  frmInputForm.ShowComponentProperties(nil, False);
  frmReportForm.ShowComponentProperties(nil, False);
  TrwReport(DesignPaperReport[0].Report).LoadFromStream(AStream);
  stbStatus.Panels[1].Text := '';
  SyncDesignByReport;
  Ready;
end;

procedure TrwDesignerMainForm.rwPreviewpnlPageNumClick(Sender: TObject);
begin
  rwPreview.pnlPageNumClick(Sender);
end;


procedure TrwDesignerMainForm.SetReadOnly(AValue: Boolean);
begin
  frmReportForm.ReadOnly := AValue;
  frmInputForm.ReadOnly := AValue;
  FCurrentReportPaper.RefreshCompResizer;
  FInputForm.Container.RefreshCompResizer;
end;

procedure TrwDesignerMainForm.tbsReportFormShow(Sender: TObject);
begin
  ReportFormToolsBar.RestoreState;
end;

procedure TrwDesignerMainForm.tbsReportFormHide(Sender: TObject);
begin
  ReportFormToolsBar.SaveState;
  ReportFormToolsBar.UndockedBarsVisible(False);
end;

procedure TrwDesignerMainForm.tbsInputFormHide(Sender: TObject);
begin
  InFormToolsBar.SaveState;
  InFormToolsBar.UndockedBarsVisible(False);
end;

procedure TrwDesignerMainForm.tbsInputFormShow(Sender: TObject);
begin
  InFormToolsBar.RestoreState;
end;

procedure TrwDesignerMainForm.actSecurityExecute(Sender: TObject);
var
  h: String;
begin
  h := TrwReport(DesignPaperReport[0].Report).SecurityInfo.Licence;
  if InputQuery('Licencing Info', 'Licence Key', h) then
    TrwReport(DesignPaperReport[0].Report).SecurityInfo.Licence := h;
end;

procedure TrwDesignerMainForm.tbsPreviewHide(Sender: TObject);
begin
  if rwPreview.FileName <> '' then
  begin
    rwPreview.BeforeClosePreview;
    DeleteFile(rwPreview.FileName);
  end;
end;

{ TrwDesignerFormHolder }

constructor TrwDesignerFormHolder.Create(const AReportRes: IEvDualStream; const AReportName: String);
begin
  ReportDesigner := TrwDesignerMainForm.Create(nil);

  with ReportDesigner do
  begin
    try
      FIncomingStream := AReportRes;
      stbStatus.Panels[0].Text := AReportName;
      Show;
    except
      FreeAndNil(ReportDesigner);
      raise;
    end;
  end;
end;

destructor TrwDesignerFormHolder.Destroy;
begin
  FreeAndNil(ReportDesigner);
  inherited;
end;

function TrwDesignerFormHolder.GetData: IEvDualStream;
begin
  if Assigned(ReportDesigner) then
    Result := ReportDesigner.FIncomingStream
  else
    Result := nil;
end;

function TrwDesignerFormHolder.IsClosed: Boolean;
begin
  Result := not (Assigned(ReportDesigner) and ReportDesigner.Visible);
end;

initialization

finalization

end.
