unit rmTrmDBSubTablePropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmDBTablePropFrm, StdCtrls, ExtCtrls, rmOPEFontFrm,
  rmOPEColorFrm, rmOPEQueryFrm, rmOPEBooleanFrm, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls, rmOPEMasterFieldsFrm,
  rmOPEDBTableGroupsFrm, rmOPEExpressionFrm, rmOPEBlockingControlFrm;

type
  TrmTrmDBSubTableProp = class(TrmTrmDBTableProp)
    frmMasterFields: TrmOPEMasterFields;
    procedure rmOPEQuery1btnQueryClick(Sender: TObject);
  private
  public
  end;


implementation

{$R *.dfm}

procedure TrmTrmDBSubTableProp.rmOPEQuery1btnQueryClick(Sender: TObject);
begin
  rmOPEQuery1.btnQueryClick(Sender);
  frmMasterFields.MakeLookups;
end;

end.
