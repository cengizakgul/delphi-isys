inherited rmOPEDBTableGroups: TrmOPEDBTableGroups
  Width = 341
  Height = 328
  object pnlGroupDetails: TPanel
    Left = 0
    Top = 141
    Width = 329
    Height = 182
    BevelOuter = bvNone
    TabOrder = 5
    object Label1: TLabel
      Left = 0
      Top = 7
      Width = 60
      Height = 13
      Caption = 'Group Name'
    end
    object Label2: TLabel
      Left = 0
      Top = 46
      Width = 63
      Height = 13
      Caption = 'Group Field 1'
    end
    object Label3: TLabel
      Left = 0
      Top = 75
      Width = 63
      Height = 13
      Caption = 'Group Field 2'
    end
    object Label4: TLabel
      Left = 0
      Top = 104
      Width = 63
      Height = 13
      Caption = 'Group Field 3'
    end
    object edName: TEdit
      Left = 77
      Top = 4
      Width = 172
      Height = 21
      TabOrder = 0
      OnExit = edNameExit
      OnKeyPress = edNameKeyPress
    end
    object cbField1: TwwDBComboBox
      Left = 76
      Top = 41
      Width = 176
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = True
      DropDownCount = 8
      ItemHeight = 0
      Sorted = True
      TabOrder = 1
      UnboundDataType = wwDefault
      OnChange = cbField1Change
    end
    object cbField2: TwwDBComboBox
      Left = 76
      Top = 70
      Width = 176
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = True
      DropDownCount = 8
      ItemHeight = 0
      Sorted = True
      TabOrder = 2
      UnboundDataType = wwDefault
      OnChange = cbField1Change
    end
    object cbField3: TwwDBComboBox
      Left = 76
      Top = 99
      Width = 176
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = True
      DropDownCount = 8
      ItemHeight = 0
      Sorted = True
      TabOrder = 3
      UnboundDataType = wwDefault
      OnChange = cbField1Change
    end
    object chbHeader: TCheckBox
      Left = 0
      Top = 138
      Width = 89
      Height = 17
      Caption = 'Header Band'
      TabOrder = 4
      OnClick = chbHeaderClick
    end
    object chbFooter: TCheckBox
      Left = 0
      Top = 163
      Width = 84
      Height = 17
      Caption = 'Footer Band'
      TabOrder = 5
      OnClick = chbFooterClick
    end
    object chbInherited: TCheckBox
      Left = 233
      Top = 163
      Width = 94
      Height = 17
      Caption = 'Inherited Group'
      Enabled = False
      TabOrder = 6
      OnClick = chbDisabledClick
    end
    object chbPageBreak: TCheckBox
      Left = 109
      Top = 138
      Width = 160
      Height = 17
      Caption = 'Break Page after this group'
      TabOrder = 7
      OnClick = chbPageBreakClick
    end
    object chbDisabled: TCheckBox
      Left = 109
      Top = 163
      Width = 98
      Height = 17
      Caption = 'Disabled Group'
      TabOrder = 8
      OnClick = chbDisabledClick
    end
  end
  object lvGroups: TListView
    Left = 0
    Top = 0
    Width = 341
    Height = 137
    Align = alTop
    Columns = <
      item
        Caption = 'Group Name'
        MinWidth = 80
        Width = 163
      end
      item
        AutoSize = True
        Caption = 'Group Fields'
        MinWidth = 80
      end>
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnSelectItem = lvGroupsSelectItem
  end
  object btnAdd: TButton
    Left = 267
    Top = 144
    Width = 74
    Height = 22
    Caption = 'Add Group'
    TabOrder = 1
    OnClick = btnAddClick
  end
  object btnDel: TButton
    Left = 267
    Top = 171
    Width = 74
    Height = 22
    Caption = 'Delete Group'
    TabOrder = 2
    OnClick = btnDelClick
  end
  object btnUp: TButton
    Left = 267
    Top = 206
    Width = 74
    Height = 22
    Caption = 'Move Up'
    TabOrder = 3
    OnClick = btnUpClick
  end
  object btnDown: TButton
    Left = 267
    Top = 233
    Width = 74
    Height = 22
    Caption = 'Move Down'
    TabOrder = 4
    OnClick = btnDownClick
  end
end
