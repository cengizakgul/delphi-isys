unit rmDsgnAreaFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTypes, rmDsgnTypes, rwGraphics, rmCustomFrameFrm, Menus, rwDsgnUtils,
  rwUtils, rwRTTI;

type

  TrmDsgnAreaScrollBox = class(TScrollBox)
  protected
    procedure AutoScrollInView(AControl: TControl); override;
  end;


  TrmDsgnArea = class(TrmCustomFrame)
    pmObjectProperties: TPopupMenu;
    miObjectProperties: TMenuItem;
    miLockObject: TMenuItem;
    miUnlock: TMenuItem;
    miLock: TMenuItem;
    miHide: TMenuItem;
    procedure FrameResize(Sender: TObject);
    procedure miUnlockClick(Sender: TObject);
    procedure miObjectPropertiesClick(Sender: TObject);
    procedure pmObjectPropertiesPopup(Sender: TObject);
  private
    FDesignObject: IrmDesignObject;
    FZoomFactor: Extended;
    FCanvasToScreenZoomRatio: Extended;
    FPopupMenuSender: Pointer;  // MUST BE a POINTER!
    procedure SetZoomFactor(const Value: Extended);
    procedure SetDesignObject(const Value: IrmDesignObject);
    function GetPopupMenuSender: IrmDesignObject;
  protected
    FImageIndex: Integer;
    FAreaType: TrmDsgnAreaType;
    procedure WndProc(var Message: TMessage); override;
    procedure AutoScrollInView(AControl: TControl); override;
    procedure Initialize; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    function    GetDsgnArea: TWinControl; virtual;
    function    CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject; virtual;
    function    GetObjectCanvasDPI: Integer; virtual;
    procedure   ShowPopupMenu(const Sender: IrmDesignObject; const X, Y: Integer);

    property DesignObject: IrmDesignObject read FDesignObject write SetDesignObject;
    property AreaType: TrmDsgnAreaType read FAreaType;
    property ZoomFactor: Extended read FZoomFactor write SetZoomFactor;
    property CanvasToScreenZoomRatio: Extended read FCanvasToScreenZoomRatio;
    property ImageIndex: Integer read FImageIndex;
    property PopupMenuSender: IrmDesignObject read GetPopupMenuSender;
  end;

  TrmDsgnAreaClass = class of TrmDsgnArea;

implementation

uses rmObjectPropFormFrm;

{$R *.dfm}


{ TrmDsgnArea }

procedure TrmDsgnArea.AutoScrollInView(AControl: TControl);
begin
  // Do nothing!
end;

procedure TrmDsgnArea.WndProc(var Message: TMessage);
var
  fl: Boolean;
  P: TPoint;
  lParam: DWORD;
  C: IrmDesignObject;
begin
  fl := False;

  if Message.Msg = WM_LBUTTONDOWN then
  begin
    if Assigned(FDesignObject) and (FDesignObject.GetDesigner <> nil) then
    begin
      P := Point(Smallint(Message.lParamLo), Smallint(Message.lParamHi));

      C := FDesignObject.GetParent;
      if  Assigned(C) then
      begin
        P := FDesignObject.ClientToScreen(P);
        P := C.ScreenToClient(P);
      end;

      lParam := Message.lParam;
      Message.lParam := MakeLong(Word(P.X), Word(P.Y));
      fl := FDesignObject.GetDesigner.DispatchDesignMsg(FDesignObject, Message);
      Message.lParam := lParam;
    end;
  end;


  if not fl then
    inherited WndProc(Message);
end;

procedure TrmDsgnArea.FrameResize(Sender: TObject);
begin
  // Do nothing here
end;

constructor TrmDsgnArea.Create(AOwner: TComponent);
begin
  inherited;
  FCanvasToScreenZoomRatio := GetObjectCanvasDPI / cScreenCanvasRes;
  FZoomFactor := 1 / CanvasToScreenZoomRatio;
end;

procedure TrmDsgnArea.SetZoomFactor(const Value: Extended);
begin
  FZoomFactor := Value;
  
  VertScrollBar.Position := 0;
  HorzScrollBar.Position := 0;

  DesignObject.Notify(rmdZoomChanged);
end;

function TrmDsgnArea.GetObjectCanvasDPI: Integer;
begin
  Result := cScreenCanvasRes;
end;

procedure TrmDsgnArea.SetDesignObject(const Value: IrmDesignObject);
begin
  FDesignObject := Value;
  Initialize;
end;

procedure TrmDsgnArea.Initialize;
begin
end;


function TrmDsgnArea.GetDsgnArea: TWinControl;
begin
  Result := Self;
end;

procedure TrmDsgnArea.ShowPopupMenu(const Sender: IrmDesignObject; const X, Y: Integer);
var
  aPM: array of TPopupMenu;
  PM: TPopupMenu;
  h: String;
  Cl: IrwClass;

  procedure MakePopupMenu;
  var
    i, j: Integer;
    MI: TMenuItem;
  begin
    for i := High(aPM) downto Low(aPM) do
      for j := 0 to aPM[i].Items.Count - 1 do
      begin
        MI := aPM[i].Items[0];
        MI.Tag := Integer(Pointer(aPM[i]));
        aPM[i].Items.Delete(0);
        pmObjectProperties.Items.Add(MI);
      end;

    for i := Low(aPM) to High(aPM) do
      if Assigned(aPM[i].OnPopup) then
        aPM[i].OnPopup(aPM[i]);
  end;

  procedure RestoreSourcePopupMenu;
  var
    i: Integer;
    MI: TMenuItem;
  begin
    for i := 2 to pmObjectProperties.Items.Count - 1 do
    begin
      MI := pmObjectProperties.Items[2];
      pmObjectProperties.Items.Delete(2);
      TPopupMenu(Pointer(MI.Tag)).Items.Add(MI);
    end;
  end;

begin
  FPopupMenuSender := Pointer(Sender);
  
  h := PopupMenuSender.GetClassName;
  SetLength(aPM, 0);
  while not AnsiSameText(h, 'TrwComponent') do
  begin
    PM := TPopupMenu(FindComponent('pm' + h));
    if Assigned(PM) then
    begin
      SetLength(aPM, Length(aPM) + 1);
      aPM[High(aPM)] := PM;
    end;

    Cl := rwRTTIInfo.FindClass(h);
    if Assigned(Cl) then
    begin
      Cl := Cl.GetAncestor;
      if Assigned(Cl) then
        h := Cl.GetClassName
      else
        break;
    end
    else
      break;
  end;

  try
    MakePopupMenu;
    if Length(RMDesigner.GetComponentDesigner.GetSelectedObjects) = 0 then
      RMDesigner.GetComponentDesigner.SelectObject(PopupMenuSender);

    pmObjectProperties.Popup(X, Y);
  finally
    RestoreSourcePopupMenu;
  end;
end;

function TrmDsgnArea.GetPopupMenuSender: IrmDesignObject;
begin
  Result := IrmDesignObject(FPopupMenuSender);
end;

function TrmDsgnArea.CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject;
begin
  Result := nil;
end;

{ TrmDsgnAreaScrollBox }

procedure TrmDsgnAreaScrollBox.AutoScrollInView(AControl: TControl);
begin
  // do nothing
end;

procedure TrmDsgnArea.miUnlockClick(Sender: TObject);
var
  ls: TrwDsgnLockState;
begin
  if Sender = miLock then
    ls := rwLSLocked
  else  if Sender = miHide then
    ls := rwLSHidden
  else
    ls := rwLSUnlocked;

  PopupMenuSender.SetPropertyValue('DsgnLockState', ls);
end;

procedure TrmDsgnArea.miObjectPropertiesClick(Sender: TObject);
begin
  if Assigned(PopupMenuSender) then
    ShowObjectProperties(PopupMenuSender, Self)
  else
    ShowObjectProperties(DesignObject, Self);
end;

procedure TrmDsgnArea.pmObjectPropertiesPopup(Sender: TObject);
begin
  case TrwDsgnLockState(Integer(PopupMenuSender.GetPropertyValue('DsgnLockState'))) of
    rwLSLocked: miLock.Checked := True;
    rwLSHidden: miHide.Checked := True;
  else
    miUnlock.Checked := True;
  end;
end;

end.
