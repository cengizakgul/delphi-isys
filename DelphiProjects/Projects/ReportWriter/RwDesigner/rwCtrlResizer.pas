unit rwCtrlResizer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TypInfo, Menus, ExtCtrls, StdCtrls, rwCommonClasses, rwReport, rwDebugInfo,
  ComCtrls, rwBasicClasses, rwReportControls;

type

  TrwMoveAct = (maNone, maSelected, maResize, maMove);

  TrwTypeSizePoint = (spClient, spTopLeft, spTop, spTopRight, spRight, spBottomRight,
    spBottom, spBottomLeft, spLeft);

  TrwControlResizer = class;

         {This is sizable border around control}

  TrwControlResizer = class(TCustomControl)
  private
    FMoveAct: TrwMoveAct;
    FSizePoint: TrwTypeSizePoint;
    FOldMousePos: TPoint;
    FContBounds: TRect;
    FGrouping: Boolean;
    FGrpMoving: Boolean;
    FOnAfterResize: TNotifyEvent;
    FOnAttach: TNotifyEvent;
    FOnDetach: TNotifyEvent;
    FChanging: Boolean;

    procedure WMGetDLGCode(var Message: TMessage); message WM_GETDLGCODE;
    procedure WMMouseMove(var Message: TWMMouseMove); message WM_MOUSEMOVE;
    procedure FOnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    function  WhereIsCursor(X, Y: LongInt): TrwTypeSizePoint;
    procedure DrawNewBounds(ANewBounds: TRect);
    procedure SetCursor(ARegion: TrwTypeSizePoint);
    procedure MoveControl(dX, dY: Integer);
    procedure ResizeControl(dX, dY: Integer);
    procedure SetGrouping(AValue: Boolean);
    procedure DoBeforeMove(X, Y: Integer);
    procedure DoMove(X, Y: Integer; Shift: TShiftState = []);
    procedure DoAfterMove;
    procedure FOnResize;
    function  GetRegion: THandle;
    procedure SetRegion;

  protected
    FVisualControl: TControl;
    FComponent: TrwComponent;
    procedure Paint; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure CreateWindowHandle(const Params: TCreateParams); override;

  public
    property Grouping: Boolean read FGrouping write SetGrouping;
    property VisualControl: TControl read FVisualControl;
    property Component: TrwComponent read FComponent;
    property OnAfterResize: TNotifyEvent read FOnAfterResize write FOnAfterResize;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure BringToFront;
    procedure SendToBack;
    procedure Attach(AComponent: TrwComponent);
    procedure Detach;
    procedure BoundsByControl;
    procedure BeginChanges;
    procedure EndChanges;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;

  published
    property OnDblClick;
    property PopupMenu;
    property OnMouseMove;
    property OnAttach: TNotifyEvent read FOnAttach write FOnAttach;
    property OnDetach: TNotifyEvent read FOnDetach write FOnDetach;
  end;


function LookChild(ACurrentControl: TControl; ACont: TWinControl; aP: TPoint; ARect: TRect): TControl;

implementation

uses
  rwDesignerFrm, rwDesignAreaFrm, Types;

const
  cSizePoint = 5;
  cGridSize = 8;



function LookChild(ACurrentControl: TControl; ACont: TWinControl; aP: TPoint; ARect: TRect): TControl;
var
  i: Integer;
  M1, M2: TMethod;
begin
  Result := nil;

  if not PtInRect(ARect, aP) then
    Exit;

  for i := 0 to ACont.ControlCount - 1 do
    if ACont.Controls[i].Visible and
{      (ACont.Controls[i].Owner <> ACont) and}
      ((ACont.Controls[i] is TCustomLabel) or
      PtInRect(ARect, ACont.Controls[i].BoundsRect.TopLeft) and PtInRect(ARect, ACont.Controls[i].BoundsRect.BottomRight)) and
      PtInRect(ACont.Controls[i].BoundsRect, aP) then
    begin
      M1 := GetMethodProp(ACurrentControl, 'OnMouseDown');
      M2 := GetMethodProp(ACont.Controls[i], 'OnMouseDown');
      if (M1.Code <> M2.Code) or (M1.Data <> M2.Data) then
        continue;

      if ACont.Controls[i] is TWinControl then
      begin
        Result := LookChild(ACurrentControl, TWinControl(ACont.Controls[i]),
          ACont.Controls[i].ScreenToClient(ACont.ClientToScreen(aP)),
          Rect(0, 0, ACont.Width, ACont.Height));
        if Result = nil then
          Result := ACont.Controls[i];
      end
      else if (ACurrentControl is TGraphicControl) and not (ACurrentControl is TCustomLabel) then
      begin
        Result := ACont.Controls[i];
        ARect := ACont.Controls[i].BoundsRect;
        continue;
      end
      else
        Result := ACont.Controls[i];

      break;
    end;
end;



              {TrwControlResizer}

constructor TrwControlResizer.Create(AOwner: TComponent);
begin
  inherited;

  BevelInner := bvNone;
  BevelOuter := bvNone;
  BorderWidth := 0;
  FGrouping := False;
  FGrpMoving := False;
  FChanging := False;
  FVisualControl := nil;
  FComponent := nil;
  FContBounds := Rect(0, 0, 0, 0);
  FMoveAct := maNone;
  FSizePoint := spClient;
  OnMouseDown := FOnMouseDown;
  OnMouseUp := FOnMouseUp;
end;

destructor TrwControlResizer.Destroy;
begin
  Detach;
  inherited;
end;

procedure TrwControlResizer.SetGrouping(AValue: Boolean);
begin
  if Debugging then
    Exit;
    
  if FGrouping <> AValue then
  begin
    FGrouping := AValue;
    Invalidate;
  end;
end;

procedure TrwControlResizer.Attach(AComponent: TrwComponent);
var
  i: Integer;
  lGrp: Boolean;
  PTypeInf: PTypeInfo;
  PPropInf: PPropInfo;
  Pm: LongInt;

begin
  Detach;

  //Group select
  lGrp := False;
  with TrwDesignArea(Owner).SelectedCompList do
    for i := 0 to Count - 1 do
    begin
      if Resizers[i].VisualControl <> nil then
      begin
        Resizers[i].Grouping := True;
        lGrp := True;
      end;
    end;

  FComponent := AComponent;
  FVisualControl := TrwControl(FComponent).VisualControl.RealObject;

  if not Assigned(FVisualControl) then
    Exit;

  PTypeInf := FVisualControl.ClassInfo;
  PPropInf := GetPropInfo(PTypeInf, 'PopupMenu');
  Pm := GetOrdProp(FVisualControl, PPropInf);
  if Pm <> 0 then
  begin
    PopupMenu := TPopupMenu(Pm);
    PopupMenu.AutoPopup := True;
  end
  else
    PopupMenu := nil;

  Grouping := lGrp;

  FMoveAct := maSelected;
  BoundsByControl;

  Parent := FVisualControl.Parent;

  if Component is TrwNoVisualComponent then
    BringToFront
  else
    if CanFocus then
      SetFocus;

  if not FChanging and Assigned(FOnAttach) then FOnAttach(Self);
end;

procedure TrwControlResizer.Detach;
var
  lRszH: TrwControlResizer;
  i: Integer;
begin
  Parent := nil;
  FComponent := nil;
  FVisualControl := nil;
  if Assigned(PopupMenu) then
    PopupMenu.AutoPopup := False;
  PopupMenu := nil;

  Grouping := False;

  lRszH := nil;

  with TrwDesignArea(Owner).SelectedCompList do
    for i := 0 to Count - 1 do
    begin
      if Resizers[i].VisualControl = nil then
        continue;

      if lRszH <> nil then
        Exit
      else
        lRszH := Resizers[i];
    end;

  if lRszH <> nil then
    lRszH.Grouping := False;

  FMoveAct := maNone;

  if not FChanging and Assigned(FOnDetach) then FOnDetach(Self);
end;

procedure TrwControlResizer.BringToFront;
begin
  if Assigned(FVisualControl) then
    FVisualControl.BringToFront;
  inherited;

  if CanFocus then
    SetFocus;
end;

procedure TrwControlResizer.SendToBack;
begin
  if FVisualControl <> nil then
    FVisualControl.SendToBack;

  inherited;

  if CanFocus then
    SetFocus;
end;

procedure TrwControlResizer.WMGetDLGCode(var Message: TMessage);
begin
  Message.Result := DLGC_WANTARROWS;
end;

procedure TrwControlResizer.Paint;
var
  Rgn: THandle;
begin
  with Canvas do
  begin
    if (FMoveAct = maSelected) and not FChanging then
    begin
      if Debugging and not DebugInfo.ErrorHappend then
        Brush.Color := clRed
      else
        if FGrouping then
          Brush.Color := clGray
        else
          Brush.Color := clBlack;
    end
    else
      Brush.Color := clGray;

    Brush.Style := bsSolid;
    Rgn := GetRegion;
    FillRgn(Canvas.Handle, Rgn, Brush.Handle);
    DeleteObject(Rgn);
  end;
end;

procedure TrwControlResizer.BoundsByControl;
begin
  SetBounds(FVisualControl.Left - cSizePoint div 2, FVisualControl.Top - cSizePoint div 2,
    FVisualControl.Width + cSizePoint, FVisualControl.Height + cSizePoint);
end;

procedure TrwControlResizer.FOnResize;
begin
  if Assigned(FVisualControl) then
  begin
    if FComponent is TrwControl then
      TrwControl(FComponent).SetBounds(Left + cSizePoint div 2, Top + cSizePoint div 2, Width - cSizePoint, Height - cSizePoint)
    else
    begin
      TrwNoVisualComponent(FComponent).Left := Left + cSizePoint div 2;
      TrwNoVisualComponent(FComponent).Top := Top + cSizePoint div 2;
    end;

    BoundsByControl;

    if CanFocus then
      SetFocus;

    if not FGrpMoving then
      if Assigned(FOnAfterResize) then
        FOnAfterResize(Self);
  end;
end;

function TrwControlResizer.WhereIsCursor(X, Y: LongInt): TrwTypeSizePoint;
var
  h1, h2, hp: Integer;
  P: TPoint;
begin
  h1 := Width div 2;
  h2 := Height div 2;
  hp := cSizePoint div 2;
  P := Point(X, Y);
  if PtInRect(Rect(0, 0, cSizePoint, cSizePoint), P) then
    Result := spTopLeft
  else if PtInRect(Rect(Width - cSizePoint, 0, Width, cSizePoint), P) then
    Result := spTopRight
  else if PtInRect(Rect(Width - cSizePoint, Height - cSizePoint, Width, Height), P) then
    Result := spBottomRight
  else if PtInRect(Rect(0, Height - cSizePoint, cSizePoint, Height), P) then
    Result := spBottomLeft
  else if PtInRect(Rect(h1 - hp, 0, h1 + hp, cSizePoint), P) then
    Result := spTop
  else if PtInRect(Rect(Width - cSizePoint, h2 - hp, Width, h2 + hp), P) then
    Result := spRight
  else if PtInRect(Rect(h1 - hp, Height - cSizePoint, h1 + hp, Height), P) then
    Result := spBottom
  else
    if PtInRect(Rect(0, h2 - hp, cSizePoint, h2 + hp), P) then
    Result := spLeft
  else
    Result := spClient;
end;

procedure TrwControlResizer.FOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  P: TMethod;
  R: TRect;
  sP: TPoint;
  Contr: TControl;
  OldCtrl: TControl;
  i: Integer;

begin
//  if (Shift = [ssLeft]) and (FVisualControl is TPageControl) then
//    SendMessage(TPageControl(FVisualControl).Handle, WM_LBUTTONDOWN, MK_LBUTTON, MakeLParam(X, Y));

  if (Button <> mbLeft) or (ssDouble in Shift) then Exit;

  if (Shift = [ssLeft]) and (FVisualControl is TPageControl) then
  begin
    i := TPageControl(FVisualControl).IndexOfTabAt(X, Y);
    if i <> -1 then
      TPageControl(FVisualControl).ActivePageIndex := i;
  end;

  FSizePoint := WhereIsCursor(X, Y);
  if (FSizePoint <> spClient) and not FGrouping then
    FMoveAct := maResize
  else
    FMoveAct := maMove;

  //Looking through child controls
  if FMoveAct = maMove then
  begin
    if FVisualControl is TWinControl then
      Contr := LookChild(FVisualControl, TWinControl(FVisualControl), Point(X, Y), Rect(-16000, -16000, 16000, 16000))

    else if not FGrpMoving and (FVisualControl is TGraphicControl) and not (FVisualControl is TCustomLabel) then
    begin
      sP := Point(X, Y);
      sP.X := FVisualControl.Left + X;
      sP.Y := FVisualControl.Top + Y;
      Contr := LookChild(FVisualControl, TWinControl(FVisualControl.Parent), sP, FVisualControl.BoundsRect);
      if FVisualControl = Contr then
        Contr := nil;
    end

    else
      Contr := nil;

    if Assigned(Contr) then
    begin
      FMoveAct := maSelected;
      sP := ClientToScreen(Point(X, Y));
      Attach((Contr as IrwVisualControl).RWObject);
      sP := ScreenToClient(sP);
      SendMessage(Self.Handle, WM_LBUTTONDOWN, MK_LBUTTON, MakeLong(sP.X, sP.Y));
      exit;
    end

    else
    begin
      OldCtrl := FVisualControl;

      P := GetMethodProp(Owner, 'OnMouseDown');
      if Assigned(P.Code) then
        TMouseEvent(P)(Self, Button, Shift, X, Y);

      if OldCtrl <> FVisualControl then
      begin
        FMoveAct := maSelected;
        FContBounds := Rect(0, 0, 0, 0);
        exit;
      end;
    end;
  end;


  FOldMousePos := ClientToScreen(Point(X, Y));

  DrawNewBounds(Rect(FVisualControl.Left, FVisualControl.Top,
    FVisualControl.Width + FVisualControl.Left, FVisualControl.Height + FVisualControl.Top));

  if not FGrpMoving and not (ssShift in Shift) then
  begin
    DoBeforeMove(X, Y);
    R := Parent.BoundsRect;

    if Parent.Parent <> nil then
    begin
      R.TopLeft := Parent.Parent.ClientToScreen(R.TopLeft);
      R.BottomRight := Parent.Parent.ClientToScreen(R.BottomRight);
    end;

    ClipCursor(@R);
    SetCaptureControl(Self);
  end;

  SetFocus;
end;

procedure TrwControlResizer.FOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  R: TRect;
  lApply: Boolean;
begin
  if (Button <> mbLeft) or (FContBounds.Left = FContBounds.Right) then
    Exit;

  lApply := FMoveAct <> maSelected;

  R := FContBounds;

  FMoveAct := maSelected;
  if lApply then
    SetBounds(R.Left - cSizePoint div 2, R.Top - cSizePoint div 2, R.Right - R.Left + cSizePoint, R.Bottom - R.Top + cSizePoint)
  else
    BoundsByControl;

  if not FGrpMoving then
  begin
    if lApply then
      DoAfterMove;

    ClipCursor(nil);
    ReleaseCapture;
  end;

  FOnResize;
end;

procedure TrwControlResizer.FOnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  Region: TrwTypeSizePoint;
  P: TPoint;
  NewBnd: TRect;
  lDelta: Integer;
  fl_exit: Boolean;
begin
  if Debugging then Exit;

  if ssShift in Shift then Exit;

  if (Shift = [ssAlt]) or (Shift = [ssAlt, ssLeft])  then
    lDelta := 1
  else
    lDelta := cGridSize;

  P := ClientToScreen(Point(X, Y));

  fl_exit := not((Abs(FOldMousePos.Y - P.Y) >= lDelta) or (Abs(FOldMousePos.X - P.X) >= lDelta));

  if not fl_exit then
    if (FMoveAct = maResize) then
    begin
      NewBnd := FContBounds;
      case FSizePoint of
        spTopLeft:
          begin
            NewBnd.Left := NewBnd.Left + P.X - FOldMousePos.X;
            NewBnd.Top := NewBnd.Top + P.Y - FOldMousePos.Y;
          end;

        spTop: NewBnd.Top := NewBnd.Top + P.Y - FOldMousePos.Y;

        spTopRight:
          begin
            NewBnd.Top := NewBnd.Top + P.Y - FOldMousePos.Y;
            NewBnd.Right := NewBnd.Right + P.X - FOldMousePos.X;
          end;

        spRight: NewBnd.Right := NewBnd.Right + P.X - FOldMousePos.X;

        spBottomRight:
          begin
            NewBnd.Bottom := NewBnd.Bottom + P.Y - FOldMousePos.Y;
            NewBnd.Right := NewBnd.Right + P.X - FOldMousePos.X;
          end;

        spBottom: NewBnd.Bottom := NewBnd.Bottom + P.Y - FOldMousePos.Y;

        spBottomLeft:
          begin
            NewBnd.Bottom := NewBnd.Bottom + P.Y - FOldMousePos.Y;
            NewBnd.Left := NewBnd.Left + P.X - FOldMousePos.X;
          end;

        spLeft: NewBnd.Left := NewBnd.Left + P.X - FOldMousePos.X;
      end;

      if FComponent is TrwBand then
      begin
        NewBnd.Top := FContBounds.Top;
        NewBnd.Left := FContBounds.Left;
        NewBnd.Right := FContBounds.Right;
      end;

      if FComponent is TrwNoVisualComponent then
        NewBnd := FContBounds;

      DrawNewBounds(NewBnd);
    end

    else if FMoveAct = maMove then
    begin
      if FComponent is TrwBand then
        NewBnd := FContBounds
      else
      begin
        NewBnd.Left := FContBounds.Left + P.X - FOldMousePos.X;
        NewBnd.Top := FContBounds.Top + P.Y - FOldMousePos.Y;
        NewBnd.Right := FContBounds.Right + P.X - FOldMousePos.X;
        NewBnd.Bottom := FContBounds.Bottom + P.Y - FOldMousePos.Y;
      end;

      DrawNewBounds(NewBnd);

      if not FGrpMoving then
        DoMove(X, Y, Shift);
    end;

  if (FMoveAct = maSelected) and not FGrouping then
  begin
    Region := WhereIsCursor(X, Y);
    SetCursor(Region);
  end;

  if not fl_exit then
    FOldMousePos := P
end;

procedure TrwControlResizer.DoBeforeMove(X, Y: Integer);
var
  i: Integer;
  lRszH: TrwControlResizer;
begin
  if FGrouping then
    with TrwDesignArea(Owner).SelectedCompList do
    begin
      for i := 0 to Count - 1 do
      begin
        lRszH := Resizers[i];
        if lRszH = Self then
          continue;
        lRszH.FGrpMoving := True;
        lRszH.FOnMouseDown(lRszH, mbLeft, [], X, Y);
        lRszH.FGrpMoving := False;
      end;
    end
end;

procedure TrwControlResizer.DoMove(X, Y: Integer; Shift: TShiftState = []);
var
  i: Integer;
  lRszH: TrwControlResizer;
begin
  if FGrouping then
    with TrwDesignArea(Owner).SelectedCompList do
    begin
      for i := 0 to Count - 1 do
      begin
        lRszH := Resizers[i];
        if lRszH = Self then
          continue;
        lRszH.FGrpMoving := True;
        lRszH.FOnMouseMove(lRszH, Shift, X, Y);
        lRszH.FGrpMoving := False;
      end;
      Parent.Update;
    end
end;

procedure TrwControlResizer.DoAfterMove;
var
  i: Integer;
  lRszH: TrwControlResizer;
begin
  if FGrouping then
    with TrwDesignArea(Owner).SelectedCompList do
    begin
      for i := 0 to Count - 1 do
      begin
        lRszH := Resizers[i];
        if lRszH = Self then
          continue;
        lRszH.FGrpMoving := True;
        lRszH.FOnMouseUp(lRszH, mbLeft, [], 0, 0);
        lRszH.FGrpMoving := False;
      end;
    end
end;

procedure TrwControlResizer.SetCursor(ARegion: TrwTypeSizePoint);
begin
  if ARegion = spClient then
    Cursor := crArrow
  else
    if ARegion in [spTopLeft, spBottomRight] then
    Cursor := crSizeNWSE
  else
    if ARegion in [spTopRight, spBottomLeft] then
    Cursor := crSizeNESW
  else
    if ARegion in [spTop, spBottom] then
    Cursor := crSizeNS
  else
    if ARegion in [spRight, spLeft] then
    Cursor := crSizeWE;
end;

procedure TrwControlResizer.DrawNewBounds(ANewBounds: TRect);
begin
  FContBounds := ANewBounds;
  SetBounds(FContBounds.Left, FContBounds.Top, FContBounds.Right - FContBounds.Left,
            FContBounds.Bottom - FContBounds.Top);
end;

procedure TrwControlResizer.MoveControl(dX, dY: Integer);
var
  i: Integer;
  lRszH: TrwControlResizer;
begin
  SetBounds(Left + dX, Top + dY, Width, Height);

  if FGrouping and not FGrpMoving then
    with TrwDesignArea(Owner).SelectedCompList do
    begin
      for i := 0 to Count - 1 do
      begin
        lRszH := Resizers[i];
        if lRszH = Self then
          continue;
        lRszH.FGrpMoving := True;
        lRszH.MoveControl(dX, dY);
        lRszH.FGrpMoving := False;
      end;
      Parent.Update;      
    end;

  FOnResize;
end;

procedure TrwControlResizer.ResizeControl(dX, dY: Integer);
var
  i: Integer;
  lRszH: TrwControlResizer;
begin
  SetBounds(Left, Top, Width + dX, Height + dY);

  if FGrouping and not FGrpMoving then
    with TrwDesignArea(Owner).SelectedCompList do
    begin
      for i := 0 to Count - 1 do
      begin
        lRszH := Resizers[i];
        if lRszH = Self then
          continue;
        lRszH.FGrpMoving := True;
        lRszH.ResizeControl(dX, dY);
        lRszH.FGrpMoving := False;
      end;
      Parent.Update;
    end;

  FOnResize;
end;

procedure TrwControlResizer.KeyDown(var Key: Word; Shift: TShiftState);
var
  lParent: TWinControl;
  h: string;
  lDelta: Integer;
  lRszH: TrwControlResizer;
  i: Integer;
  Hndl: THandle;
begin
  if (FMoveAct <> maSelected) and (Key = VK_ESCAPE) then
  begin
    FMoveAct := maSelected;
    PostMessage(Handle, WM_LBUTTONUP, 0, 0);

    if FGrouping then
      with TrwDesignArea(Owner).SelectedCompList do
      begin
        for i := 0 to Count - 1 do
        begin
          lRszH := Resizers[i];
          if lRszH = Self then
            continue;
          lRszH.FMoveAct := maSelected;
          PostMessage(lRszH.Handle, WM_LBUTTONUP, 0, 0);
        end;
      end;

    Exit;
  end;

  if Shift = [ssCtrl, ssShift] then
    lDelta := cGridSize
  else
    lDelta := 1;

  if ssCtrl in Shift then
    case Key of
      VK_UP: MoveControl(0, -lDelta);
      VK_DOWN: MoveControl(0, lDelta);
      VK_LEFT: MoveControl(-lDelta, 0);
      VK_RIGHT: MoveControl(lDelta, 0);
    end

  else if Shift = [ssShift] then
    case Key of
      VK_UP: ResizeControl(0, -1);
      VK_DOWN: ResizeControl(0, 1);
      VK_LEFT: ResizeControl(-1, 0);
      VK_RIGHT: ResizeControl(1, 0);
    end

  else if (Shift = []) and (Key = VK_ESCAPE) then
  begin
      if Supports(Parent, IrwVisualControl) and
         (Parent as IrwVisualControl).RWObject.InheritsFrom(TrwControl) and
         not ((FVisualControl.Parent as IrwVisualControl).RWObject is TrwInputFormContainer) then
      begin
        lParent := FVisualControl.Parent;
        Detach;
        Attach((lParent as IrwVisualControl).RWObject);
      end
      else
      begin
        if ReportDesigner.CurrentPageDesigner = ReportDesigner.frmInputForm then
          Hndl := ReportDesigner.InputForm.Container.pnlArea.Handle
        else
          Hndl := ReportDesigner.CurrentReportPaper.pnlArea.Handle;
        PostMessage(Hndl, WM_LBUTTONDOWN, -1, -1);
        PostMessage(Hndl, WM_LBUTTONUP, -1, -1)
      end;
  end

  else if (Shift = []) and (Key = VK_F1) then
  begin
    h := (FVisualControl as IrwVisualControl).RWObject.ClassName + #0;
    Application.HelpCommand(HELP_KEY, Integer(PChar(@h[1])));
  end;

  inherited;
end;


procedure TrwControlResizer.BeginChanges;
begin
  FChanging := True;
end;

procedure TrwControlResizer.EndChanges;
begin
  FChanging := False;
end;

procedure TrwControlResizer.WMMouseMove(var Message: TWMMouseMove);
begin
  inherited;

  with Message do
    FOnMouseMove(Self, KeysToShiftState(Keys), XPos, YPos);
end;

procedure TrwControlResizer.SetBounds(ALeft, ATop, AWidth,  AHeight: Integer);
var
  fl: Boolean;
begin
  fl := (AWidth = Width) and (AHeight = Height);
  inherited;
  if not fl then
    SetRegion;
end;

function TrwControlResizer.GetRegion: THandle;
var
  h, hp: Integer;
  Rgn: THandle;

  procedure AddSquare(X1, Y1, X2, Y2: Integer);
  begin
    Rgn := CreateRectRgn(X1, Y1, X2, Y2);
    CombineRgn(Result, Result, Rgn, RGN_OR);
    DeleteObject(Rgn);
  end;

begin
  if FMoveAct = maSelected then
  begin
    Result := CreateRectRgn(0, 0, cSizePoint, cSizePoint);
    AddSquare(0, Height - cSizePoint, cSizePoint, Height);
    AddSquare(Width - cSizePoint, 0, Width, cSizePoint);
    AddSquare(Width - cSizePoint, Height - cSizePoint, Width, Height);
    h := Width div 2;
    hp := cSizePoint div 2;
    AddSquare(h - hp, 0, h + hp + 1, cSizePoint);
    AddSquare(h - hp, Height - cSizePoint, h + hp + 1, Height);
    h := Height div 2;
    AddSquare(0, h - hp, cSizePoint, h + hp + 1);
    AddSquare(Width - cSizePoint, h - hp, Width, h + hp + 1);
  end
  
  else
  begin
    Result := CreateRectRgn(0, 0, Width, Height);
    Rgn := CreateRectRgn(2, 2, Width - 2, Height - 2);
    CombineRgn(Result, Result, Rgn, RGN_DIFF);
    DeleteObject(Rgn);
  end;
end;

procedure TrwControlResizer.SetRegion;
begin
  if HandleAllocated then
    SetWindowRgn(Handle, GetRegion, True);
end;

procedure TrwControlResizer.CreateWindowHandle(const Params: TCreateParams);
begin
  inherited;
  SetRegion;
end;

end.
