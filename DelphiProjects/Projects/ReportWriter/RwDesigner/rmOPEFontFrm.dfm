inherited rmOPEFont: TrmOPEFont
  Width = 75
  Height = 23
  AutoSize = True
  object btnFont: TButton
    Left = 0
    Top = 0
    Width = 75
    Height = 23
    Caption = 'Font'
    TabOrder = 0
    OnClick = btnFontClick
  end
  object FontDialog: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Left = 136
  end
end
