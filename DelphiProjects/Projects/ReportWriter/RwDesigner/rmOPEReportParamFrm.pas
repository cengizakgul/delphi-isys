unit rmOPEReportParamFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls, rwUtils, rwBasicClasses, rwTypes,
  rwEngineTypes, rwBasicUtils, rmTypes;

type
  TrmOPEReportParam = class(TrmObjPropertyEditor)
    lParamName: TLabel;
    edParamName: TEdit;
    chbDefault: TCheckBox;
    Label1: TLabel;
    edParamDescription: TEdit;
    chbShowInQB: TCheckBox;
    procedure edParamNameChange(Sender: TObject);
    procedure edParamNameExit(Sender: TObject);
  private
    FGlobalVariables: TrwGlobalVariables;
    FParamType: TrwVarTypeKind;
    FGlobalVar: TrwGlobalVariable;
    FParamNumber: Integer;
  protected
    property  GlobalVar: TrwGlobalVariable read FGlobalVar write FGlobalVar;
  public
    procedure ShowPropValue; override;
    procedure ApplyChanges; override;
    property  ParamType: TrwVarTypeKind read FParamType write FParamType;
    property  ParamNumber: Integer read FParamNumber write FParamNumber;
  end;

implementation

{$R *.dfm}

{ TrmObjPropertyEditor1 }

procedure TrmOPEReportParam.ShowPropValue;
var
  L: TStringList;
  VarFinct: TrwGlobalVarFunct;
  i: Integer;
  C: TrwComponent;
  h: String;
begin
  inherited;
  C := TrwComponent(LinkedObject);

  VarFinct := C.RootOwner.GlobalVarFunc;
  FGlobalVariables := VarFinct.Variables;

  L := TStringList(Pointer(Integer(C.PGetPropertyValue('ReportParams'))));
  if L.Count - 1 >= FParamNumber then
  begin
    h := L[FParamNumber];
    edParamName.Text := Trim(GetNextStrValue(h, ','));
    i := FGlobalVariables.IndexOf(edParamName.Text);
    if i <> -1 then
    begin
      GlobalVar := FGlobalVariables[i];
      edParamDescription.Text := GlobalVar.DisplayName;
      chbDefault.Checked := GlobalVar.Visibility = rvvPublished;
      chbShowInQB.Checked := GlobalVar.ShowInQB;
    end
    else
    begin
      GlobalVar := nil;
      edParamDescription.Text := '';
      chbDefault.Checked := False;
      chbShowInQB.Checked := False;
    end;
  end
  else
  begin
    GlobalVar := nil;
    edParamName.Text := '';
    edParamDescription.Text := '';
    chbDefault.Checked := True;
    chbShowInQB.Checked := True;
  end;
end;


procedure TrmOPEReportParam.ApplyChanges;
var
  L: TStringList;
  i: Integer;
begin
  edParamName.Text := Trim(edParamName.Text);
  edParamDescription.Text := Trim(edParamDescription.Text);

  if edParamName.Text = '' then
  begin
    FreeAndNil(FGlobalVar);
    Exit;
  end;

  if not Assigned(GlobalVar) then
  begin
    if FGlobalVariables.IndexOf(edParamName.Text) = -1 then
    begin
      if edParamName.Text <> '' then
      begin
        GlobalVar := FGlobalVariables.Add;
        GlobalVar.Name := edParamName.Text;
        GlobalVar.VarType := ParamType;
        GlobalVar.ShowInFmlEditor := True;
        case ParamType of
          rwvInteger,
          rwvFloat,
          rwvCurrency: GlobalVar.Value := 0;
          rwvBoolean:  GlobalVar.Value := False;
          rwvString:   GlobalVar.Value := '';
          rwvDate:     GlobalVar.Value := Now;
          rwvArray:    GlobalVar.Value := VarArrayOf([]);
        end;
      end;
    end
    else
      raise ErwException.Create('Parameter name "' + edParamName.Text + '" already in use');
  end

  else
  begin
    GlobalVar.Name := edParamName.Text;
    GlobalVar.ShowInFmlEditor := True;
  end;

  FGlobalVar.DisplayName := edParamDescription.Text;

  if chbDefault.Checked then
    FGlobalVar.Visibility := rvvPublished
  else
    FGlobalVar.Visibility := rvvPublic;

  FGlobalVar.ShowInQB := chbShowInQB.Checked;


  L := TStringList(Pointer(Integer(TrwComponent(LinkedObject).PGetPropertyValue('ReportParams'))));
  for i := 0 to FParamNumber - L.Count do
    L.Add('');

  L[FParamNumber] := edParamName.Text;
end;

procedure TrmOPEReportParam.edParamNameChange(Sender: TObject);
begin
  NotifyOfChanges;
end;

procedure TrmOPEReportParam.edParamNameExit(Sender: TObject);
begin
  if Assigned(GlobalVar) and AnsiSameStr(GlobalVar.Name, GlobalVar.DisplayName) then
    edParamDescription.Text := edParamName.Text;
end;

end.
