object rwVMCPU: TrwVMCPU
  Left = 547
  Top = 137
  Width = 675
  Height = 516
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'RW Virtual Machine CPU'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter4: TSplitter
    Left = 422
    Top = 0
    Height = 489
    Align = alRight
  end
  object Panel1: TPanel
    Left = 425
    Top = 0
    Width = 242
    Height = 489
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 0
      Top = 146
      Width = 242
      Height = 18
      Align = alTop
      AutoSize = False
      Caption = 'Stack content'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 242
      Height = 146
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 0
        Top = 0
        Width = 242
        Height = 18
        Align = alTop
        AutoSize = False
        Caption = 'Registers status'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbRegisters: TListBox
        Left = 0
        Top = 18
        Width = 242
        Height = 128
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 17
        Items.Strings = (
          'A'
          'B'
          'F'
          'SP'
          'CS'
          'IP'
          'Heap')
        ParentFont = False
        TabOrder = 0
      end
    end
    object lbStack: TListBox
      Left = 0
      Top = 164
      Width = 242
      Height = 325
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Courier New'
      Font.Style = []
      ItemHeight = 17
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 422
    Height = 489
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter5: TSplitter
      Left = 0
      Top = 307
      Width = 422
      Height = 3
      Cursor = crVSplit
      Align = alBottom
    end
    object Label4: TLabel
      Left = 0
      Top = 0
      Width = 422
      Height = 18
      Align = alTop
      AutoSize = False
      Caption = 'P-Code'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Panel5: TPanel
      Left = 0
      Top = 310
      Width = 422
      Height = 179
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object Label3: TLabel
        Left = 0
        Top = 0
        Width = 422
        Height = 18
        Align = alTop
        AutoSize = False
        Caption = 'Heap content'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbHeap: TListBox
        Left = 0
        Top = 18
        Width = 422
        Height = 161
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 17
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object aclDesignerActions: TActionList
    Images = DsgnRes.ilButtons
    Left = 288
    Top = 118
    object actStepOver: TAction
      Caption = 'Step Over'
      ImageIndex = 66
      ShortCut = 119
      OnExecute = actStepOverExecute
    end
    object actTraceInto: TAction
      Caption = 'Trace Into'
      ImageIndex = 67
      ShortCut = 118
      OnExecute = actTraceIntoExecute
    end
    object actAddWatch: TAction
      Caption = 'Add Watch...'
      ImageIndex = 63
      ShortCut = 16500
      OnExecute = actAddWatchExecute
    end
    object actWatches: TAction
      Caption = 'Watches'
      ImageIndex = 65
      ShortCut = 49239
      OnExecute = actWatchesExecute
    end
    object actRun: TAction
      Caption = 'actRun'
      ImageIndex = 69
      ShortCut = 120
      OnExecute = actRunExecute
    end
    object actTerminate: TAction
      Caption = 'Terminate'
      ImageIndex = 72
      ShortCut = 16497
      OnExecute = actTerminateExecute
    end
    object actBrkPoint: TAction
      Caption = 'actBrkPoint'
      ImageIndex = 68
      ShortCut = 116
      OnExecute = actBrkPointExecute
    end
  end
end
