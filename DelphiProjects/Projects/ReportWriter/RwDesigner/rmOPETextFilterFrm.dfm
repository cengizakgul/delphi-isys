inherited rmOPETextFilter: TrmOPETextFilter
  Width = 290
  Height = 243
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 290
    Height = 182
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object lHeader2: TLabel
      Left = 0
      Top = 164
      Width = 290
      Height = 18
      Align = alBottom
      AutoSize = False
      Caption = 'Parameters of Selected Filter'
      Color = 14737632
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
    end
    object lvFilters: TListView
      Left = 0
      Top = 0
      Width = 290
      Height = 121
      Align = alTop
      Columns = <
        item
          Caption = 'Filter'
          Width = 200
        end
        item
          AutoSize = True
          Caption = 'Parameters'
        end>
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
      OnSelectItem = lvFiltersSelectItem
    end
    object btnAdd: TButton
      Left = 0
      Top = 127
      Width = 75
      Height = 22
      Caption = 'Add...'
      TabOrder = 1
      OnClick = btnAddClick
    end
    object btnDelete: TButton
      Left = 81
      Top = 127
      Width = 75
      Height = 22
      Caption = 'Delete'
      TabOrder = 2
      OnClick = btnDeleteClick
    end
    object btnUp: TButton
      Left = 171
      Top = 127
      Width = 51
      Height = 22
      Caption = 'Up'
      TabOrder = 3
      OnClick = btnUpClick
    end
    object btnDown: TButton
      Left = 226
      Top = 127
      Width = 51
      Height = 22
      Caption = 'Down'
      TabOrder = 4
      OnClick = btnUpClick
    end
  end
  object pnlParams: TPanel
    Left = 0
    Top = 182
    Width = 290
    Height = 61
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
  end
end
