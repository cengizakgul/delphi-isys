unit rmFMLFunctEditorFormFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmActionFormFrm, StdCtrls, rmTypes;

type
  TrmFMLFunctEditorForm = class(TrmActionForm)
    btnCancel: TButton;
    btnOK: TButton;
    lContent: TLabel;
    procedure btnCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lContentDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);

  private
    FContent: TList;
    FPropertyName: String;
    FFunctionName: String;
    FMaxItemCount: Integer;

    procedure DisplayContent;

  protected
    procedure Initialize(const AParams: Variant); override;
    function  AllowDrag(const AObject: IrmDesignObject): Boolean; override;
    procedure EndDrag(const AObject: IrmDesignObject); override;

    //interface
    function  ObjectMouseDblClick(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean; override;
  end;

implementation

uses rmDesignerFrm;

{$R *.dfm}

function TrmFMLFunctEditorForm.AllowDrag(const AObject: IrmDesignObject): Boolean;
begin
  Result := (AObject <> FActiveObject) and AObject.PropertyExists(FPropertyName);
end;

procedure TrmFMLFunctEditorForm.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TrmFMLFunctEditorForm.DisplayContent;
var
  h: String;
  i: Integer;
begin
  h := FFunctionName + '(';

  for i := 0 to FContent.Count - 1 do
  begin
    if i > 0  then
      h := h + ', ';
    h := h + IrmDesignObject(FContent[i]).GetPropertyValue('Name');
  end;

  h := h + ')';
  lContent.Caption := h;
end;

procedure TrmFMLFunctEditorForm.EndDrag(const AObject: IrmDesignObject);
begin
  FContent.Add(Pointer(AObject));
  DisplayContent;

  if FContent.Count = FMaxItemCount then
    btnOK.Click;
end;

procedure TrmFMLFunctEditorForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Fml: IrmFMLFormula;

  procedure AddSumValExpression;
  var
    i: Integer;
  begin
    for i := 0 to FContent.Count - 1 do
    begin
      if i > 0  then
        Fml.AddItem(fitOperation, '+');
      Fml.AddItem(fitObject, IrmDesignObject(FContent[i]).GetPathFromRootOwner, 'Value');
    end;
  end;

  procedure AddSumFunction;
  begin
    Fml.AddItem(fitFunction, FFunctionName);
    AddSumValExpression;
    Fml.AddItem(fitSeparator, ',');
    Fml.AddItem(fitConst, True);
    Fml.AddItem(fitBracket, ')');
  end;

  procedure AddCountFunction;
  begin
    Fml.AddItem(fitFunction, FFunctionName);
    if FContent.Count > 0 then
     Fml.AddItem(fitObject, IrmDesignObject(FContent[0]).GetPathFromRootOwner);
    Fml.AddItem(fitSeparator, ',');
    Fml.AddItem(fitConst, True);
    Fml.AddItem(fitSeparator, ',');
    Fml.AddItem(fitConst, False);
    Fml.AddItem(fitBracket, ')');
  end;

begin
  inherited;

  if ModalResult = mrOK then
  begin
    Fml := FActiveObject.GetFormulas.GetFormulaByAction(fatSetProperty, FPropertyName);
    if not Assigned(Fml) then
    begin
      Fml := (FActiveObject.GetFormulas.AddItem as IrmFMLFormula);
      Fml.SetActionType(fatSetProperty);
      Fml.SetActionInfo(FPropertyName);
    end
    else
      Fml.Clear;

    if AnsiSameText(FFunctionName, 'Sum') then
      AddSumFunction
    else if AnsiSameText(FFunctionName, 'SumVal') then
      AddSumValExpression
    else if AnsiSameText(FFunctionName, 'Count') then
      AddCountFunction;


    FActiveObject.Refresh;
  end;

  Action := caFree;
end;

procedure TrmFMLFunctEditorForm.FormCreate(Sender: TObject);
begin
  inherited;
  FContent := TList.Create;
  FPropertyName := 'Value';
end;

procedure TrmFMLFunctEditorForm.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(FContent);
end;

procedure TrmFMLFunctEditorForm.Initialize(const AParams: Variant);
begin
  inherited;

  FFunctionName := AParams[0];
  if VarArrayHighBound(AParams, 1) >= 1 then
    FMaxItemCount := AParams[1]
  else
    FMaxItemCount := 10000;  

  DisplayContent;
end;

function TrmFMLFunctEditorForm.ObjectMouseDblClick(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean;
begin
  if AllowDrag(AObject) then
    EndDrag(AObject);
    
  Result := True;
end;

procedure TrmFMLFunctEditorForm.lContentDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := Source is TrmDragAndDropObject;
end;

end.
