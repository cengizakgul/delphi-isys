unit rmTrmDBTablePropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTypes, rmTrmPrintContainerPropFrm, rmOPEFontFrm, rmOPEColorFrm,
  rmOPEBooleanFrm, rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm,
  StdCtrls, ExtCtrls, ComCtrls, rmOPEQueryFrm, rmOPEDBTableGroupsFrm,
  rmOPEExpressionFrm, rmOPEBlockingControlFrm, rmOPEMasterFieldsFrm;

type
  TrmTrmDBTableProp = class(TrmTrmPrintContainerProp)
    rmOPEFont1: TrmOPEFont;
    rmOPEQuery1: TrmOPEQuery;
    tsGrouping: TTabSheet;
    frmGrouping: TrmOPEDBTableGroups;
    frmMasterFields: TrmOPEMasterFields;
    procedure rmOPEQuery1btnQueryClick(Sender: TObject);
  private
  protected
    procedure GetPropsFromObject; override;
  public
  end;

implementation

uses rmObjectPropsFrm;

{$R *.dfm}

procedure TrmTrmDBTableProp.GetPropsFromObject;
begin
  inherited;
  frmGrouping.ShowPropValue;
  frmPrintOnEachPage.Enabled := not (RWObject as IrmDBTableObject).IsSubTable;
  frmMultiPaged.Enabled := frmPrintOnEachPage.Enabled;
end;

procedure TrmTrmDBTableProp.rmOPEQuery1btnQueryClick(Sender: TObject);
begin
  rmOPEQuery1.btnQueryClick(Sender);
  frmGrouping.MakeFieldList;
  frmMasterFields.MakeLookups;
end;

end.
