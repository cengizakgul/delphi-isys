object rwGlobalVarFunction: TrwGlobalVarFunction
  Left = 319
  Top = 370
  AutoScroll = False
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Global Variables and Functions'
  ClientHeight = 443
  ClientWidth = 723
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pcPages: TPageControl
    Left = 0
    Top = 0
    Width = 723
    Height = 412
    ActivePage = tsVar
    Align = alClient
    TabOrder = 0
    object tsVar: TTabSheet
      Caption = 'Variables'
      object Splitter1: TSplitter
        Left = 230
        Top = 0
        Height = 384
      end
      object Panel2: TPanel
        Left = 233
        Top = 0
        Width = 482
        Height = 384
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object grbVar: TGroupBox
          Left = 8
          Top = 15
          Width = 278
          Height = 364
          Caption = 'Description'
          Enabled = False
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 24
            Width = 28
            Height = 13
            Caption = 'Name'
          end
          object Label2: TLabel
            Left = 8
            Top = 89
            Width = 24
            Height = 13
            Caption = 'Type'
          end
          object Label3: TLabel
            Left = 8
            Top = 177
            Width = 64
            Height = 13
            Caption = 'Initial value(s)'
          end
          object Label7: TLabel
            Left = 8
            Top = 121
            Width = 36
            Height = 13
            Caption = 'Visibility'
          end
          object Label8: TLabel
            Left = 8
            Top = 55
            Width = 53
            Height = 13
            Caption = 'Description'
          end
          object edtVarName: TEdit
            Left = 71
            Top = 22
            Width = 196
            Height = 21
            TabOrder = 0
            OnExit = edtVarNameExit
            OnKeyPress = edtVarNameKeyPress
          end
          object cbType: TComboBox
            Left = 71
            Top = 85
            Width = 195
            Height = 21
            DropDownCount = 10
            ItemHeight = 13
            TabOrder = 2
            OnChange = cbTypeChange
          end
          object memValue: TMemo
            Left = 8
            Top = 194
            Width = 260
            Height = 160
            ScrollBars = ssBoth
            TabOrder = 5
            WordWrap = False
            OnExit = memValueExit
            OnKeyPress = memValueKeyPress
          end
          object cbVisibility: TComboBox
            Left = 71
            Top = 117
            Width = 195
            Height = 21
            Style = csDropDownList
            DropDownCount = 10
            ItemHeight = 13
            TabOrder = 3
            OnChange = cbVisibilityChange
            Items.Strings = (
              'Published'
              'Public'
              'Protected'
              'Private')
          end
          object edtVarDescription: TEdit
            Left = 71
            Top = 53
            Width = 196
            Height = 21
            TabOrder = 1
            OnExit = edtVarDescriptionExit
            OnKeyPress = edtVarNameKeyPress
          end
          object chbShowInQB: TCheckBox
            Left = 136
            Top = 145
            Width = 128
            Height = 17
            Caption = 'Show In Query Builder'
            TabOrder = 4
            OnClick = chbShowInQBClick
          end
          object chbShowInFmlEditor: TCheckBox
            Left = 136
            Top = 169
            Width = 133
            Height = 17
            Caption = 'Show In Formula Editor'
            TabOrder = 6
            OnClick = chbShowInFmlEditorClick
          end
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 230
        Height = 384
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label4: TLabel
          Left = 0
          Top = 0
          Width = 230
          Height = 20
          Align = alTop
          AutoSize = False
          Caption = ' List of Variables'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lvVar: TListView
          Left = 0
          Top = 20
          Width = 230
          Height = 364
          Align = alClient
          Color = clWhite
          Columns = <
            item
              AutoSize = True
            end>
          HideSelection = False
          OwnerDraw = True
          ReadOnly = True
          ShowColumnHeaders = False
          SmallImages = DsgnRes.ilPict
          SortType = stText
          TabOrder = 0
          ViewStyle = vsReport
          OnChange = lvVarChange
          OnDrawItem = lvVarDrawItem
        end
      end
    end
    object tsFunct: TTabSheet
      Caption = 'Functions'
      ImageIndex = 1
      object Splitter2: TSplitter
        Left = 164
        Top = 0
        Height = 384
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 164
        Height = 384
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label5: TLabel
          Left = 0
          Top = 0
          Width = 164
          Height = 21
          Align = alTop
          AutoSize = False
          Caption = ' List of Functions'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lvFunct: TListView
          Left = 0
          Top = 21
          Width = 164
          Height = 363
          Align = alClient
          Columns = <
            item
              AutoSize = True
            end>
          HideSelection = False
          ReadOnly = True
          ShowColumnHeaders = False
          SortType = stText
          TabOrder = 0
          ViewStyle = vsReport
          OnChange = lvFunctChange
        end
      end
      object Panel5: TPanel
        Left = 167
        Top = 0
        Width = 548
        Height = 384
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 548
          Height = 18
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          DesignSize = (
            548
            18)
          object Label6: TLabel
            Left = 0
            Top = 0
            Width = 97
            Height = 18
            Align = alLeft
            AutoSize = False
            Caption = 'Text of Function'
          end
          object pnPos: TPanel
            Left = 480
            Top = 0
            Width = 66
            Height = 17
            Anchors = [akRight, akBottom]
            BevelOuter = bvLowered
            TabOrder = 0
          end
        end
        object pcFunctions: TPageControl
          Left = 0
          Top = 18
          Width = 548
          Height = 366
          Align = alClient
          TabOrder = 1
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 412
    Width = 723
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      723
      31)
    object btnOK: TButton
      Left = 558
      Top = 4
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 2
    end
    object btnCancel: TButton
      Left = 642
      Top = 4
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancel'
      ModalResult = 3
      TabOrder = 3
      OnClick = btnCancelClick
    end
    object btnAdd: TButton
      Left = 371
      Top = 4
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Add'
      TabOrder = 0
      OnClick = btnAddClick
    end
    object btnDel: TButton
      Left = 455
      Top = 4
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Delete'
      TabOrder = 1
      OnClick = btnDelClick
    end
  end
end
