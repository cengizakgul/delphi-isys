unit rmOPBasicFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjectPropsFrm, StdCtrls, ExtCtrls, ComCtrls, rwUtils, rmTypes,
  rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm;

type
  TrmOPBasic = class(TrmObjectProp)
    pcCategories: TPageControl;
    tsBasic: TTabSheet;
    tcAppearance: TTabSheet;
    frmObjectName: TrmOPEStringSingleLine;
    tsAbout: TTabSheet;
    imObject: TImage;
    mObjectDescription: TMemo;
    frmObjectDescription: TrmOPEStringSingleLine;
    ShowMultyLineEditorBtn: TButton;
    procedure ShowMultyLineEditorBtnClick(Sender: TObject);
  private
  protected
    procedure GetPropsFromObject; override;
    procedure InitObjectDescription; virtual;
    procedure InitObjectIcon; virtual;
  public
    procedure OnChangeProperty(Sender: TrmObjPropertyEditor); override;
  end;

implementation

uses rmObjectPropFormFrm, rwDsgnRes, rmMultyLineStringEditorFrmUnit;

{$R *.dfm}

procedure TrmOPBasic.GetPropsFromObject;
begin
  inherited;
  frmObjectName.PropertyName := 'Name';
  frmObjectName.ShowPropValue;
  frmObjectDescription.PropertyName := 'Description';
  frmObjectDescription.ShowPropValue;
  InitObjectIcon;
  InitObjectDescription;
end;

procedure TrmOPBasic.InitObjectDescription;
begin
  mObjectDescription.Text := RWObject.GetObjectClassDescription;
end;

procedure TrmOPBasic.InitObjectIcon;
begin
  imObject.Picture.Assign(nil);
  DsgnRes.RWClassBigPictures.GetClassPicture(RWObject.GetClassName, imObject.Picture.Bitmap);
end;

procedure TrmOPBasic.OnChangeProperty(Sender: TrmObjPropertyEditor);
var
  O: TWinControl;
begin
  inherited;

  O := Parent;
  while Assigned(O) and not (O is TrmObjectPropForm) do
    O := O.Parent;

  if O is TrmObjectPropForm then
    TrmObjectPropForm(O).NotifyOfChanges;
end;

procedure TrmOPBasic.ShowMultyLineEditorBtnClick(Sender: TObject);
var
  tmp : String;
begin
  if ShowRmMultyLineStringEditorFrm(frmObjectDescription.lPropName.Caption, frmObjectDescription.edString.Text, tmp) then
    frmObjectDescription.edString.Text := tmp;
end;

end.
