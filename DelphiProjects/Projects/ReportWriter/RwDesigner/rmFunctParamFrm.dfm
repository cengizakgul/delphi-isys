object rmFunctParam: TrmFunctParam
  Left = 0
  Top = 0
  Width = 314
  Height = 105
  AutoScroll = False
  TabOrder = 0
  object lParamName: TLabel
    Left = 0
    Top = 2
    Width = 61
    Height = 13
    Caption = 'Param Name'
  end
  object cbValues: TisRWDBComboBox
    Left = 153
    Top = 0
    Width = 159
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    DropDownCount = 8
    ItemHeight = 0
    Sorted = False
    TabOrder = 0
    UnboundDataType = wwDefault
    Visible = False
    OnChange = cbValuesChange
  end
  object chbValue: TCheckBox
    Left = 153
    Top = 22
    Width = 14
    Height = 17
    TabOrder = 1
    Visible = False
    OnClick = cbValuesChange
  end
  object eddValue: TwwDBDateTimePicker
    Left = 153
    Top = 40
    Width = 160
    Height = 21
    CalendarAttributes.Font.Charset = DEFAULT_CHARSET
    CalendarAttributes.Font.Color = clWindowText
    CalendarAttributes.Font.Height = -11
    CalendarAttributes.Font.Name = 'MS Sans Serif'
    CalendarAttributes.Font.Style = []
    Epoch = 1950
    ShowButton = True
    TabOrder = 2
    Visible = False
    OnChange = cbValuesChange
  end
  object ednValue: TisRWDBSpinEdit
    Left = 153
    Top = 62
    Width = 161
    Height = 21
    Increment = 1.000000000000000000
    TabOrder = 3
    UnboundDataType = wwDefault
    Visible = False
    OnChange = cbValuesChange
  end
  object edsValue: TEdit
    Left = 153
    Top = 84
    Width = 160
    Height = 21
    TabOrder = 4
    Visible = False
    OnChange = cbValuesChange
  end
end
