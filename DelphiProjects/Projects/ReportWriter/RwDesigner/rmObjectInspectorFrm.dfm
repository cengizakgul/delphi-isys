inherited rmObjectInspector: TrmObjectInspector
  Width = 239
  Height = 338
  object pmUserProps: TPopupMenu
    Left = 92
    Top = 180
    object miRegUserProp: TMenuItem
      Caption = 'Register New User Property...'
      OnClick = miRegUserPropClick
    end
    object miUnRegUserProp: TMenuItem
      Caption = 'Unregister User Property'
      OnClick = miUnRegUserPropClick
    end
  end
end
