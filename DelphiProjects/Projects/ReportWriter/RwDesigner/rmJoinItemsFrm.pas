unit rmJoinItemsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls, Mask, wwdbedit, Wwdotdot,
  Wwdbcomb, Buttons, ISBasicClasses;

type
  TrmJoinItems = class(TFrame)
    cbLeftItem: TwwDBComboBox;
    cbRightItem: TwwDBComboBox;
    lNumber: TSpeedButton;
    procedure cbLeftItemChange(Sender: TObject);
    procedure lNumberClick(Sender: TObject);
  private
    FControlDisableCount: Integer;
    FOnChange: TNotifyEvent;
    function  GetLeftValue: String;
    function  GetRightValue: String;
    procedure SetLeftValue(const Value: String);
    procedure SetRightValue(const Value: String);
  protected
    procedure SetEnabled(Value: Boolean); override;
    function  PropEditor: TrmObjPropertyEditor;
    procedure DoChange;
    procedure Clear;
  public
    procedure MakeLookups; virtual;
    procedure DisableControls;
    procedure EnableControls;
    property  LeftValue: String read GetLeftValue write SetLeftValue;
    property  RightValue: String read GetRightValue write SetRightValue;
    property  OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

implementation

{$R *.dfm}


{ TrmJoinItems }

procedure TrmJoinItems.DisableControls;
begin
  Inc(FControlDisableCount);
end;

procedure TrmJoinItems.DoChange;
begin
  if Assigned(FOnChange) and (FControlDisableCount = 0) then
    FOnChange(Self);
end;

procedure TrmJoinItems.EnableControls;
begin
  Dec(FControlDisableCount);
  if FControlDisableCount = -1 then
    FControlDisableCount := 0;
end;

function TrmJoinItems.GetLeftValue: String;
begin
  Result := cbLeftItem.Value;
end;

function TrmJoinItems.GetRightValue: String;
begin
  Result := cbRightItem.Value;
end;

procedure TrmJoinItems.MakeLookups;
begin

end;

function TrmJoinItems.PropEditor: TrmObjPropertyEditor;
begin
  Result := TrmObjPropertyEditor(Owner);
end;

procedure TrmJoinItems.SetEnabled(Value: Boolean);
var
  i: Integer;
begin
  inherited;

  for i := 0 to ComponentCount - 1 do
  begin
    if Components[i] is TControl then
      TControl(Components[i]).Enabled := Value;
  end;
end;

procedure TrmJoinItems.SetLeftValue(const Value: String);
begin
  cbLeftItem.Value := Value;
end;

procedure TrmJoinItems.SetRightValue(const Value: String);
begin
  cbRightItem.Value := Value;
end;

procedure TrmJoinItems.cbLeftItemChange(Sender: TObject);
begin
  DoChange;
end;

procedure TrmJoinItems.lNumberClick(Sender: TObject);
begin
  Clear;
end;

procedure TrmJoinItems.Clear;
begin
  cbLeftItem.Clear;
  cbRightItem.Clear;
end;

end.
