inherited rmTrmXMLelementProp: TrmTrmXMLelementProp
  Height = 384
  inherited pcCategories: TPageControl
    Height = 384
    inherited tsBasic: TTabSheet
      inherited frmQuery: TrmOPEQuery
        Top = 130
        TabOrder = 4
      end
      inherited frmMasterFields: TrmOPEMasterFields
        Top = 168
      end
      inline frmNSName: TrmOPEStringSingleLine
        Left = 8
        Top = 88
        Width = 290
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 2
        inherited lPropName: TLabel
          Width = 46
          Caption = 'NS Name'
        end
      end
    end
    inherited tcAppearance: TTabSheet
      inherited Panel1: TPanel
        Top = 72
        inherited frmProduceIf: TrmOPEBlockingControl
          Top = 82
        end
      end
    end
  end
end
