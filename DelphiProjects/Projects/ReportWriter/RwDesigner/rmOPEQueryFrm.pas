unit rmOPEQueryFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls, rwDsgnUtils, rwBasicClasses, rmTypes,
  rwEngineTypes;

type
  TrmOPEQuery = class(TrmObjPropertyEditor)
    btnQuery: TButton;
    procedure btnQueryClick(Sender: TObject);
  private
    FQuery: IrwQBQuery;
    FQueryEmpty: Boolean;
    procedure SetQueryEmpty(const Value: Boolean);
    procedure SyncQueryStatus;
  public
    procedure ShowPropValue; override;
    procedure ApplyChanges; override;

    property QueryEmpty: Boolean read FQueryEmpty write SetQueryEmpty;
  end;

implementation

{$R *.dfm}

procedure TrmOPEQuery.ApplyChanges;
begin
  if FQueryEmpty then
    FQuery.Clear;
end;

procedure TrmOPEQuery.btnQueryClick(Sender: TObject);
begin
  // Exception. Gets assigned immediately
  rmQueryBuilderForObject(TComponent(LinkedObject) as IrmDesignObject);
  SyncQueryStatus;
end;

procedure TrmOPEQuery.SetQueryEmpty(const Value: Boolean);
begin
  if FQueryEmpty <> Value then
  begin
    FQueryEmpty := Value;
    NotifyOfChanges;
  end;
end;

procedure TrmOPEQuery.ShowPropValue;
begin
  inherited;

  FQuery := (TComponent(LinkedObject) as IrmDesignObject).GetInterfacedPropByType('TrwQBQuery') as IrwQBQuery;
  Assert(Assigned(FQuery));

  SyncQueryStatus;
end;

procedure TrmOPEQuery.SyncQueryStatus;
begin
  FQueryEmpty := FQuery.GetFieldList = '';
  if FQueryEmpty then
    btnQuery.Caption := 'Build Query'
  else
    btnQuery.Caption := 'Edit Query';
end;

end.
