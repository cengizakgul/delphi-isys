object rwAppSelection: TrwAppSelection
  Left = 378
  Top = 207
  AutoScroll = False
  BorderIcons = [biSystemMenu]
  Caption = 'RW Aware Applications'
  ClientHeight = 212
  ClientWidth = 478
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000B07
    B080B70B07B0BBBB77077BBBBB77707B00700B707B00000BBB0BBB000BBB707B
    00700B707B00BBBB77077BBBBB770B07B000B70B07B00B070707070B07077070
    080800707000007070B070700000080B70B07B0800000077BBBBB77000000700
    B707B007000000BBB000BBB000000700B707B00700000077BBBBB77000000000
    00000000000000000000401000000000000002000000020000000200000018C7
    0000C01F0000800F0000800F0000800F0000820F0000800F0000C01F0000}
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    478
    212)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 4
    Width = 201
    Height = 13
    Caption = 'Please Select  the Application to work with'
  end
  object lFile: TLabel
    Left = 5
    Top = 186
    Width = 18
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'lFile'
    Visible = False
  end
  object btnOK: TButton
    Left = 278
    Top = 182
    Width = 93
    Height = 23
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Enabled = False
    ModalResult = 1
    TabOrder = 0
    OnClick = btnOKClick
  end
  object btnCancel: TButton
    Left = 380
    Top = 182
    Width = 93
    Height = 23
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object lvApplications: TListView
    Left = 0
    Top = 22
    Width = 478
    Height = 151
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = 'Application'
        Width = 150
      end
      item
        AutoSize = True
        Caption = 'RW Adapter Module'
      end
      item
        Caption = 'Version'
      end>
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    SortType = stText
    TabOrder = 2
    ViewStyle = vsReport
    OnDblClick = lvApplicationsDblClick
    OnSelectItem = lvApplicationsSelectItem
  end
  object pnlModeSelection: TPanel
    Left = 69
    Top = 177
    Width = 206
    Height = 33
    Anchors = [akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 3
    Visible = False
    object btnRW: TButton
      Left = 107
      Top = 5
      Width = 93
      Height = 23
      Caption = 'RW Designer'
      Enabled = False
      ModalResult = 1
      TabOrder = 0
      OnClick = btnRWClick
    end
    object btnRM: TButton
      Left = 4
      Top = 5
      Width = 93
      Height = 23
      Caption = 'RM Designer'
      Enabled = False
      ModalResult = 1
      TabOrder = 1
      OnClick = btnRMClick
    end
  end
end
