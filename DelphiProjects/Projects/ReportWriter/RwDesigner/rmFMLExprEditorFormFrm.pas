unit rmFMLExprEditorFormFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmFMLExprEditorFrm, StdCtrls, ExtCtrls, rmFormula, rmChildFormFrm,
  rwBasicClasses, rmTypes, rwBasicUtils, rmDsgnTypes, rmActionFormFrm;

type
  TrmFMLExprEditorForm = class(TrmActionForm)
    rmExprEditor: TrmFMLExprEditor;
    pnlButtons: TPanel;
    btnClear: TButton;
    btnCheck: TButton;
    btnOK: TButton;
    btnCancel: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure btnCheckClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FOriginalExpression: TrmFMLFormulaContent;
    FFormulas: TrmFMLFormulas;
    FExpression: TrmFMLFormulaContent;
    FSender: TWinControl;
    FFormSender: TForm;

    FPropertyName: String;
  protected
    procedure Initialize(const AParams: Variant); override;
    function  AllowDrag(const AObject: IrmDesignObject): Boolean; override;    
    procedure EndDrag(const AObject: IrmDesignObject); override;

    //interface
    function  ObjectMouseDblClick(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean; override;

  public
    class procedure ShowFMLEditor(AExpression: TrmFMLFormulaContent; Sender: TWinControl); overload;
  end;


implementation

uses rwDsgnUtils;

{$R *.dfm}



procedure TrmFMLExprEditorForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Fml: IrmFMLFormula;
begin
  inherited;

  if ModalResult = mrOK then
  begin
    if Assigned(FActiveObject) then
    begin
      Fml := FActiveObject.GetFormulas.GetFormulaByAction(fatSetProperty, FPropertyName);
      if not Assigned(Fml) then
      begin
        Fml := (FActiveObject.GetFormulas.AddItem as IrmFMLFormula);
        Fml.SetActionType(fatSetProperty);
        Fml.SetActionInfo(FPropertyName);
      end;

      FOriginalExpression := TrmFMLFormula(Fml.RealObject).Content;
    end;

    FOriginalExpression.Assign(FExpression);
    if Assigned(FSender) then
      FSender.Perform(WM_RMD_REFRESH, 0, 0);

    if Assigned(FActiveObject) then
      FActiveObject.Refresh;
  end;

  Action := caFree;

  if Assigned(FFormSender) then
    FFormSender.Visible := True;
end;

class procedure TrmFMLExprEditorForm.ShowFMLEditor(AExpression: TrmFMLFormulaContent; Sender: TWinControl);
var
  F: TWinControl;
  Frm: TrmFMLExprEditorForm;
begin
  F := Sender;
  while Assigned(F) and not (F is TForm) do
    F := F.Parent;

  Frm := TrmFMLExprEditorForm.Create(Sender);
  with Frm do
  begin
    FSender := Sender;
    FFormSender := TForm(F);
    Parent := TForm(RMDesigner.VCLObject);
    FOriginalExpression := AExpression;
    FExpression.Assign(FOriginalExpression);
    rmExprEditor.Expression := FExpression;
    Show;
  end;
end;

procedure TrmFMLExprEditorForm.FormShow(Sender: TObject);
begin
  if Assigned(FFormSender) then
    FFormSender.Visible := False;
end;

procedure TrmFMLExprEditorForm.btnOKClick(Sender: TObject);
begin
  Close;
end;

procedure TrmFMLExprEditorForm.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TrmFMLExprEditorForm.FormCreate(Sender: TObject);
begin
  inherited;
  FFormulas := TrmFMLFormulas.Create(TrwComponent(RMDesigner.GetComponentDesigner.DesigningObject.RealObject), TrmFMLFormula);
  FFormulas.Add;
  FExpression := FFormulas[0].Content;
end;

procedure TrmFMLExprEditorForm.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(FFormulas);
end;

procedure TrmFMLExprEditorForm.btnClearClick(Sender: TObject);
begin
  rmExprEditor.Clear;
end;

procedure TrmFMLExprEditorForm.btnCheckClick(Sender: TObject);
begin
  rmExprEditor.CheckErrors;
end;

procedure TrmFMLExprEditorForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOk then
  begin
    CanClose := False;
    btnCheck.OnClick(nil);
    CanClose := True;
  end;
end;


function TrmFMLExprEditorForm.ObjectMouseDblClick(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean;
begin
  if AllowDrag(AObject) then
    rmExprEditor.AddDrgDrpObject(AObject);
  Result := True;
end;

procedure TrmFMLExprEditorForm.Initialize(const AParams: Variant);
var
  Fml: IrmFMLFormula;
begin
  inherited;

  FPropertyName := 'Value';

  Fml := FActiveObject.GetFormulas.GetFormulaByAction(fatSetProperty, FPropertyName);
  if Assigned(Fml) then
  begin
    FOriginalExpression := TrmFMLFormula(Fml.RealObject).Content;
    FExpression.Assign(FOriginalExpression);
  end;

  rmExprEditor.Expression := FExpression;
end;

procedure TrmFMLExprEditorForm.EndDrag(const AObject: IrmDesignObject);
begin
  rmExprEditor.AddDrgDrpObject(AObject);
end;

function TrmFMLExprEditorForm.AllowDrag(const AObject: IrmDesignObject): Boolean;
begin
  Result := (AObject <> FActiveObject) and AObject.PropertyExists('Value');
end;

end.
