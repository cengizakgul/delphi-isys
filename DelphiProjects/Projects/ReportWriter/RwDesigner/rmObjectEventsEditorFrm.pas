unit rmObjectEventsEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDockableFrameFrm, rwDsgnUtils, rmDsgnTypes, rmTypes,
  rmCustomFrameFrm, rmFunctionTextFrm, ExtCtrls, Menus, StdCtrls, rwDsgnRes;

type
  TrmObjectEventsEditor = class(TrmDockableFrame)
    lbEvents: TListBox;
    pmUserEvent: TPopupMenu;
    miRegUserEvent: TMenuItem;
    miUnRegUserEvent: TMenuItem;
    Splitter1: TSplitter;
    rmEventText: TrmFunctionText;
    procedure lbEventsClick(Sender: TObject);
    procedure miRegUserEventClick(Sender: TObject);
    procedure miUnRegUserEventClick(Sender: TObject);
    procedure lbEventsDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
  private
    FRWObject: IrmDesignObject;

    procedure DMSelectedObjectListChanged(var Message: TrmMessageRec); message mSelectedObjectListChanged;
    procedure DMRunningPaused(var Message: TrmMessageRec); message mRunningPaused;
    procedure DMRunningProceeded(var Message: TrmMessageRec); message mRunningProceeded;
    procedure DMRunningStopped(var Message: TrmMessageRec); message mRunningStopped;
    procedure DMDesignObjectChanged(var Message: TrmMessageRec); message mDesignObjectChanged;

    procedure ShowSelectedObjectEvents;
    procedure SyncRunTimeEventPosition;
    procedure OnChangeEvent;
    procedure SetRWObject(const Value: IrmDesignObject);

  protected
    procedure StoreState; override;
    procedure RestoreState; override;
    procedure BeforeShow; override;
    procedure GetListeningMessages(const AMessages: TList); override;
    procedure DoRefresh; override;
  end;

implementation

{$R *.dfm}


{ TrmObjectEventsEditor }

procedure TrmObjectEventsEditor.BeforeShow;
begin
  ShowSelectedObjectEvents;
  inherited;
end;

procedure TrmObjectEventsEditor.DMRunningPaused(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;

procedure TrmObjectEventsEditor.DMRunningProceeded(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;

procedure TrmObjectEventsEditor.DMSelectedObjectListChanged(var Message: TrmMessageRec);
begin
  inherited;
  ShowSelectedObjectEvents;
end;

procedure TrmObjectEventsEditor.DoRefresh;
begin
  inherited;
  SyncRunTimeEventPosition;
end;

procedure TrmObjectEventsEditor.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mSelectedObjectListChanged));
  AMessages.Add(Pointer(mRunningPaused));
  AMessages.Add(Pointer(mRunningProceeded));
  AMessages.Add(Pointer(mRunningStopped));
  AMessages.Add(Pointer(mDesignObjectChanged));
end;

procedure TrmObjectEventsEditor.OnChangeEvent;
var
  lClassName: String;
  lLine: Integer;
  StopLine: TrmDebugStopLineInfo;
begin
  if lbEvents.ItemIndex <> -1 then
  begin
    lClassName := '';
    lLine := 0;

    if Supports(Apartment, IrmDsgnReport) then
    begin
      StopLine := (Apartment as IrmDsgnReport).ReportStopLineInfo;

      if Assigned(StopLine.SorceCodeLineInfo) and AnsiSameText(lbEvents.Items[lbEvents.ItemIndex], StopLine.SorceCodeLineInfo.FunctionName) then
      begin
        lClassName := StopLine.SorceCodeLineInfo.ClassName;
        lLine := StopLine.SorceCodeLineInfo.SourceCodeLineNbr;
      end
      else
      begin
        lClassName := rmEventText.AncestorClassName;
        lLine := rmEventText.CurrentEditor.CaretY;
      end;
    end;

    rmEventText.ShowFunction(IrmFunction(Pointer(lbEvents.Items.Objects[lbEvents.ItemIndex])), lClassName, lLine);
  end
  else
    rmEventText.ShowFunction(nil);
end;

procedure TrmObjectEventsEditor.ShowSelectedObjectEvents;
var
  SelObjects: TrmListOfObjects;
  h: String;
begin
  h := 'Object Events';
  SelObjects := (Apartment as IrmDsgnComponent).GetSelectedObjects;

  if Length(SelObjects) = 1 then
  begin
    SetRWObject(SelObjects[0]);
    h := h + ' - ' + SelObjects[0].GetPropertyValue('name');
  end
  else
    SetRWObject(nil);

  Caption := h;
end;

procedure TrmObjectEventsEditor.SyncRunTimeEventPosition;
var
  StopLine: TrmDebugStopLineInfo;
begin
  StopLine := (Apartment as IrmDsgnReport).ReportStopLineInfo;

  if Assigned(StopLine.SorceCodeLineInfo) then
    lbEvents.ItemIndex := lbEvents.Items.IndexOf(StopLine.SorceCodeLineInfo.FunctionName);

  OnChangeEvent;
end;

procedure TrmObjectEventsEditor.lbEventsClick(Sender: TObject);
begin
  OnChangeEvent;
end;

procedure TrmObjectEventsEditor.miRegUserEventClick(Sender: TObject);
begin
{  h := 'procedure NewEvent()';
  if InputQuery('Registration New User Event', 'Type in a header of the event', h) then
  begin
    TrwComponent(FRWObject.RealObject).RegisterEvents([h], True);
    RWObject := RWObject;
  end;
}  
end;

procedure TrmObjectEventsEditor.miUnRegUserEventClick(Sender: TObject);
begin
{  if lbEvents.ItemIndex = -1 then
    Exit;

  Ev := TrwEvent(lbEvents.Items.Objects[lbEvents.ItemIndex]);

  if not Ev.UserEvent then
    raise ErwException.CreateFmt(ResNotUserEvent, [Ev.Name]);

  if Ev.InheritedItem then
    raise ErwException.Create(ResErrInheritedUserEventDel);

  if MessageDlg('You are about unregister user event "' + Ev.Name + '". Proceed?',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    TrwComponent(FRWObject.RealObject).UnRegisterEvents([Ev.Name]);
    RWObject := RWObject;
  end;}
end;

procedure TrmObjectEventsEditor.SetRWObject(const Value: IrmDesignObject);
var
  lLastEvent: String;
  i: Integer;

  procedure AddFunctions(const AFuncts: IrmFunctions);
  var
    i: Integer;
    Funct: IrmFunction;
  begin
    if Assigned(AFuncts) then
      for i := 0 to AFuncts.GetItemCount - 1 do
      begin
        Funct := AFuncts.GetItemByIndex(i) as IrmFunction;
        lbEvents.Items.AddObject(Funct.GetName, Pointer(Funct));
      end;
  end;

begin
  if FRWObject = Value then
    Exit;

  FRWObject := Value;

  lbEvents.Items.BeginUpdate;
  if Assigned(FRWObject) then
  begin
    //try to keep last event to be selected
    if lbEvents.ItemIndex <> -1 then
      lLastEvent := lbEvents.Items[lbEvents.ItemIndex]
    else
      lLastEvent := '';

    lbEvents.Clear;

    lbEvents.Sorted := False;
    AddFunctions(FRWObject.GetEvents);
    if not ObjectsAreTheSame((Apartment as IrmDsgnComponent).DesigningObject, FRWObject) and Assigned(FRWObject.GetVarFunctHolder) then
      AddFunctions(FRWObject.GetVarFunctHolder.GetLocalFunctions);
    lbEvents.Sorted := True;      

    i := lbEvents.Items.IndexOf(lLastEvent);
    if (i = -1) and (lbEvents.Count > 0) then
      i := 0;
    lbEvents.ItemIndex := i;
  end

  else
  begin
    lbEvents.ItemIndex := -1;
    lbEvents.Clear;
  end;
  lbEvents.Items.EndUpdate;

  OnChangeEvent;
end;

procedure TrmObjectEventsEditor.lbEventsDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  Flags: Longint;
  Data: String;
begin
  with lbEvents do
  begin
    Canvas.FillRect(Rect);
    if Index < Count then
    begin
      Flags := DrawTextBiDiModeFlags(DT_SINGLELINE or DT_VCENTER or DT_NOPREFIX);
      if not UseRightToLeftAlignment then
        Inc(Rect.Left, 2)
      else
        Dec(Rect.Right, 2);
      Data := Items[Index];

      if ObjectsAreTheSame(IrmFunction(Pointer(Items.Objects[Index])).GetCollection.GetComponentOwner, FRWObject) then
        Canvas.Font.Style := []
      else
        Canvas.Font.Style := [fsBold];

      DrawText(Canvas.Handle, PChar(Data), Length(Data), Rect, Flags);
    end;
  end;
end;

procedure TrmObjectEventsEditor.DMRunningStopped(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;


procedure TrmObjectEventsEditor.RestoreState;
begin
  inherited;
  lbEvents.Width := Apartment.Settings.ReadData(ClassName + '\EventListWidth', lbEvents.Width);
end;

procedure TrmObjectEventsEditor.StoreState;
begin
  inherited;
  Apartment.Settings.SaveData(ClassName + '\EventListWidth', lbEvents.Width);
end;

procedure TrmObjectEventsEditor.DMDesignObjectChanged(var Message: TrmMessageRec);
begin
  inherited;
  ShowSelectedObjectEvents;
end;

initialization
  RegisterClass(TrmObjectEventsEditor);

end.


