unit rmOPEFontFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls, rwUtils, rmTypes;

type
  TrmOPEFont = class(TrmObjPropertyEditor)
    btnFont: TButton;
    FontDialog: TFontDialog;
    procedure btnFontClick(Sender: TObject);
  protected
    function  GetDefaultPropertyName: String; override;
  public
    procedure ShowPropValue; override;
    procedure ApplyChanges; override;
  end;

implementation

{$R *.dfm}

procedure TrmOPEFont.ApplyChanges;
begin
  TFont(Pointer(Integer(OldPropertyValue))).Assign(FontDialog.Font);
end;

procedure TrmOPEFont.btnFontClick(Sender: TObject);
var
  flPrintObject: Boolean;
begin
  flPrintObject := ClassObjectType(LinkedObject.ClassName) = rwDOTPrintForm;

  if flPrintObject then
    FontDialog.Options := FontDialog.Options + [fdTrueTypeOnly]
  else
    FontDialog.Options := FontDialog.Options - [fdTrueTypeOnly];


  if flPrintObject then
    FontDialog.Font.Size := Round(ConvertUnit(FontDialog.Font.Size, utLogicalPixels, utScreenPixels));

  if FontDialog.Execute then
    NotifyOfChanges;

  if flPrintObject then
    FontDialog.Font.Size := Round(ConvertUnit(FontDialog.Font.Size, utScreenPixels, utLogicalPixels));
end;

function TrmOPEFont.GetDefaultPropertyName: String;
begin
  Result := 'Font';
end;


procedure TrmOPEFont.ShowPropValue;
begin
  FontDialog.Font.Assign(TFont(Pointer(Integer(OldPropertyValue))));
end;

end.
