unit rmTrmTableCellPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjectPropsFrm, StdCtrls, ExtCtrls, Buttons,
  ComCtrls, rwTypes, rmOPBasicFrm, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, rmOPEValueFrm, rmOPEBorderLinesFrm,
  rmTrmPrintControlPropFrm, rmOPEBooleanFrm, rmOPEColorFrm,
  rmOPEFontFrm, rmTrmPrintContainerPropFrm, rmOPETextFilterFrm,
  rmOPEExpressionFrm, rmOPEBlockingControlFrm;

type
  TrmTrmTableCellProp = class(TrmTrmPrintContainerProp)
    tsBorderLines: TTabSheet;
    frmBorderLines: TrmOPEBorderLines;
    frmFont: TrmOPEFont;
    GroupBox2: TGroupBox;
    frmValue: TrmOPEValue;
    frmAutoHeight: TrmOPEBoolean;
    frmWordWrap: TrmOPEBoolean;
    frmFixedWidth: TrmOPEBoolean;
    tsTextFilters: TTabSheet;
    frmTextFilter: TrmOPETextFilter;
  private
  protected
    procedure GetPropsFromObject; override;
  public
  end;

implementation

{$R *.dfm}

{ TrmTrwTableCellProp }

procedure TrmTrmTableCellProp.GetPropsFromObject;
begin
  inherited;
  frmValue.PropertyName := 'Value';
  frmValue.ShowPropValue;
  frmAutoHeight.PropertyName := 'AutoHeight';
  frmAutoHeight.ShowPropValue;
  frmWordWrap.PropertyName := 'WordWrap';
  frmWordWrap.ShowPropValue;
  frmFixedWidth.PropertyName := 'FixedWidth';
  frmFixedWidth.ShowPropValue;
  frmBorderLines.ShowPropValue;
end;

end.
