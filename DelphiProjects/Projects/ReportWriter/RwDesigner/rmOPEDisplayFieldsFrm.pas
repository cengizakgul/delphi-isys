unit rmOPEDisplayFieldsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, Grids, Wwdbigrd, Wwdbgrid, DB,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, rwDesignClasses, rmTypes,
  rwEngineTypes, rwBasicUtils, ISDataAccessComponents;

type
  TrmOPEDisplayFields = class(TrmObjPropertyEditor)
    dsFields: TisRWClientDataSet;
    dsFieldsSelected: TStringField;
    dsFieldsFieldName: TStringField;
    dsFieldsSize: TIntegerField;
    ds: TDataSource;
    dsFieldsTitle: TStringField;
    grFields: TisRWDBGrid;
    procedure dsDataChange(Sender: TObject; Field: TField);
  private
    FFieldList: TIniString;
    FListType: Boolean;
    procedure PrepareDataSets;
    procedure MarkSelectedFields;
    procedure SetFieldList(const Value: TIniString);
  protected
    function  GetDefaultPropertyName: String; override;
  public
    procedure AfterConstruction; override;
    procedure ShowPropValue; override;
    procedure ApplyChanges; override;

    property  FieldList: TIniString read FFieldList write SetFieldList;
  end;

implementation

{$R *.dfm}

{ TrmOPEDisplayFields }

procedure TrmOPEDisplayFields.AfterConstruction;
begin
  inherited;
  grFields.IndicatorButton.Visible := False;
end;

procedure TrmOPEDisplayFields.ApplyChanges;
var
  h, res: String;
  L: TStringList;
begin
  if dsFields.State in [dsEdit, dsInsert] then
    dsFields.Post;

  res := '';

  if dsFields.Active then
  begin
    dsFields.DisableControls;
    try
      dsFields.First;
      while not dsFields.Eof do
      begin
        if dsFields.FieldByName('Selected').AsString = 'Y' then
        begin
          h := dsFields.FieldByName('FieldName').AsString;
          if (dsFields.FieldByName('Title').AsString <> '') or (dsFields.FieldByName('Size').AsString <> '') then
            h := h + ',' + dsFields.FieldByName('Title').AsString;
          if dsFields.FieldByName('Size').AsString <> '' then
            h := h + ',' + dsFields.FieldByName('Size').AsString;

          if res <> '' then
            res := res + #13;
          res := res + h;
        end;
        dsFields.Next;
      end;
      dsFields.First;
    finally
      dsFields.EnableControls;
    end;
  end;

  if FListType then
  begin
    L := TStringList.Create;
    try
      L.Text := res;
      TStringList(Pointer(Integer((TComponent(LinkedObject) as IrmDesignObject).GetPropertyValue(PropertyName)))).Assign(L);
    finally
      FreeAndNil(L);
    end;
  end
  else
    (TComponent(LinkedObject) as IrmDesignObject).SetPropertyValue(PropertyName, res);
end;

procedure TrmOPEDisplayFields.PrepareDataSets;
var
  L: TStringList;
  i: Integer;
  h: String;
begin
  L := TStringList.Create;
  try
    L.Text := FieldList;

    dsFields.DisableControls;
    try
      dsFields.FieldByName('FieldName').ReadOnly := False;
      dsFields.EmptyDataSet;
      dsFields.Open;
      // make field list
      dsFields.EmptyDataSet;
      for i := 0 to L.Count - 1 do
      begin
        h := L[i];
        dsFields.Append;
        dsFields.FieldByName('FieldName').AsString := GetNextStrValue(h, '=');
        dsFields.FieldByName('Title').AsString := h;
        dsFields.FieldByName('Selected').AsString := 'N';
        dsFields.Post;
      end;


      if dsFields.RecordCount > 0 then
        dsFields.First
      else
        dsFields.Close;

      dsFields.FieldByName('FieldName').ReadOnly := True;
    finally
      dsFields.EnableControls;
    end;

  finally
    FreeAndNil(L);
  end;
end;

procedure TrmOPEDisplayFields.ShowPropValue;
begin
  inherited;
  PrepareDataSets;
  MarkSelectedFields;
end;

procedure TrmOPEDisplayFields.dsDataChange(Sender: TObject; Field: TField);
begin
  if not dsFields.ControlsDisabled then
    NotifyOfChanges;
end;

procedure TrmOPEDisplayFields.MarkSelectedFields;
var
  h, h1, h2: String;
  L: TStringList;
  i: Integer;
  v: Variant;
begin
  L := TStringList.Create;
  dsFields.DisableControls;
  try
    v := (TComponent(LinkedObject) as IrmDesignObject).GetPropertyValue(PropertyName);
    if VarType(V) = varString then
      FListType := False
    else if TObject(Integer(v)) is TStrings then
      FListType := True
    else
      Assert(False, 'Unexpected property type');

    if FListType then
      L.Assign(TStrings(Pointer(Integer(v))))
    else
      L.Text := v;

    for i := 0 to L.Count - 1 do
    begin
      h := L[i];
      h1 := GetNextStrValue(h, ',');
      h2 := GetNextStrValue(h, ',');
      if dsFields.Locate('FieldName', h1, []) then
      begin
        dsFields.Edit;
        dsFields.FieldByName('Selected').AsString := 'Y';
        dsFields.FieldByName('Title').AsString := h2;
        if h = '' then
          dsFields.FieldByName('Size').Clear
        else
          dsFields.FieldByName('Size').AsInteger := StrToInt(h);
        dsFields.Post;
      end;
    end;
  finally
    FreeAndNil(L);
    dsFields.EnableControls;    
  end;
end;

function TrmOPEDisplayFields.GetDefaultPropertyName: String;
begin
  Result := 'DisplayFields';
end;

procedure TrmOPEDisplayFields.SetFieldList(const Value: TIniString);
begin
  FFieldList := Value;
  ShowPropValue;  
end;

end.
