unit rwLocalVarFunctFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls, TypInfo, mwCustomEdit, mwGeneralSyn, rwBasicClasses, rmTypes, rwTypes,
  rwMessages, rwParser, rwDsgnRes, rwReport, rwUtils, rwDsgnUtils, Variants, rwEngineTypes;

type
  TrwGlobalVarFunction = class(TForm)
    pcPages: TPageControl;
    tsVar: TTabSheet;
    tsFunct: TTabSheet;
    Panel1: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    Panel2: TPanel;
    grbVar: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtVarName: TEdit;
    cbType: TComboBox;
    memValue: TMemo;
    Panel3: TPanel;
    Label4: TLabel;
    lvVar: TListView;
    Splitter1: TSplitter;
    Panel4: TPanel;
    Label5: TLabel;
    lvFunct: TListView;
    Splitter2: TSplitter;
    Panel5: TPanel;
    btnAdd: TButton;
    btnDel: TButton;
    Panel8: TPanel;
    Label6: TLabel;
    pnPos: TPanel;
    pcFunctions: TPageControl;
    Label7: TLabel;
    cbVisibility: TComboBox;
    Label8: TLabel;
    edtVarDescription: TEdit;
    chbShowInQB: TCheckBox;
    chbShowInFmlEditor: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure lvVarChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure cbTypeChange(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure edtVarNameExit(Sender: TObject);
    procedure edtVarNameKeyPress(Sender: TObject; var Key: Char);
    procedure memValueExit(Sender: TObject);
    procedure memValueKeyPress(Sender: TObject; var Key: Char);
    procedure lvFunctChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure cbVisibilityChange(Sender: TObject);
    procedure lvVarDrawItem(Sender: TCustomListView; Item: TListItem;
      Rect: TRect; State: TOwnerDrawState);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnCancelClick(Sender: TObject);
    procedure edtVarDescriptionExit(Sender: TObject);
    procedure chbShowInQBClick(Sender: TObject);
    procedure chbShowInFmlEditorClick(Sender: TObject);

  private
    FunctionText: TrwEventTextEdit;
    FStoreGlobalVarFunc: TrwGlobalVarFunct;
    FGlobalVarFunc: TrwGlobalVarFunct;
    FComponent: TrwComponent;

    function  AddVarNode(AVar: TrwGlobalVariable): TListItem;
    function  AddFunctNode(AFunct: TrwEvent): TListItem;
    procedure FunctionTextExit(Sender: TObject);
    procedure ShowCursorPos(Sender: TObject);
    procedure ShowInheritedFunctions(AFunct: TrwEvent);
    function  IsRWProc(AName: String): Boolean;
  end;


function ShowGlobalVarFunct(AComp: TrwComponent; AFunctName: String = ''; ALibComponentName: string = '';
                            ALine: Integer = 0; ACol: Integer = 0): Boolean;


implementation

{$R *.DFM}


function ShowGlobalVarFunct(AComp: TrwComponent; AFunctName: String = ''; ALibComponentName: string = '';
                            ALine: Integer = 0; ACol: Integer = 0): Boolean;
var
  Frm: TrwGlobalVarFunction;
  i: Integer;
  Lst: TStringList;
  ce: TmwCustomEdit;
  fl_removeVarFnct: Boolean;
begin
  Result := False;
  fl_removeVarFnct := False;

  Frm := TrwGlobalVarFunction.Create(Application);
  with Frm do
    try
      FComponent := AComp;
      if FComponent  is TrwReportForm then
        FComponent := TrwComponent(FComponent.Owner);

      FunctionText.EventOwner := FComponent;
      while FunctionText.EventOwner is TrwSubReport do
      begin
        if FunctionText.EventOwner.Owner is TrwReportForm then
          FunctionText.EventOwner := TrwComponent(FunctionText.EventOwner.Owner.Owner);
      end;

      if Assigned(AComp.GlobalVarFunc) then
      begin
        FStoreGlobalVarFunc.Assign(AComp.GlobalVarFunc);
        for i := 0 to FStoreGlobalVarFunc.EventCount - 1 do
          FStoreGlobalVarFunc.EventsList[i].FCodeAddress := AComp.GlobalVarFunc.EventsList[i].FCodeAddress;
      end
      else
      begin
        AComp.InitGlobalVarFunc;
        fl_removeVarFnct := True;
      end;

      FGlobalVarFunc := AComp.GlobalVarFunc;

      for i := 0 to FGlobalVarFunc.Variables.Count-1 do
        if not FGlobalVarFunc.Variables[i].InheritedItem or
          FGlobalVarFunc.Variables[i].InheritedItem and
          (FGlobalVarFunc.Variables[i].Visibility in [rvvPublished, rvvPublic, rvvProtected])
        then
          AddVarNode(FGlobalVarFunc.Variables[i]);

      Lst := TStringList.Create;
      try
        FGlobalVarFunc.GetEventsList(Lst);
        for i := 0 to Lst.Count-1 do
          AddFunctNode(TrwEvent(Lst.Objects[i]));
      finally
        Lst.Free;
      end;

      if AFunctName <> '' then
      begin
        pcPages.ActivePage := tsFunct;
        lvFunct.Selected := lvFunct.FindCaption(0, AFunctName, False, False, True);
        ce := FunctionText;
        if ALibComponentName <> '' then
        begin
          for i := 0 to pcFunctions.PageCount - 1 do
            if pcFunctions.Pages[i].Caption = ALibComponentName then
            begin
              pcFunctions.ActivePage := pcFunctions.Pages[i];
              ce := TmwCustomEdit(pcFunctions.Pages[i].Controls[0]);
              break;
            end;
        end;

        ce.CaretY := ALine;
        ce.CaretX := ACol;
        ce.TopLine := 1;
        ce.LeftChar := 1;
        ActiveControl := ce;
      end;

      lvVar.Width := lvVar.Width + 1;

      Result := (ShowModal = mrOk);

    finally

      try
        if not Result then
        begin
          if fl_removeVarFnct then
            AComp.RemoveGlobalVarFunc
          else
          begin
            if Assigned(FGlobalVarFunc) then
            begin
              AComp.GlobalVarFunc.Assign(FStoreGlobalVarFunc);
              for i := 0 to FStoreGlobalVarFunc.EventCount - 1 do
                AComp.GlobalVarFunc.EventsList[i].FCodeAddress := FStoreGlobalVarFunc.EventsList[i].FCodeAddress;
            end;
          end;
        end;

      finally
        Free;
      end;
    end;
end;



procedure TrwGlobalVarFunction.FormCreate(Sender: TObject);
var
  i: Integer;
  PTypInf: PTypeInfo;
  PTypDat: PTypeData;
  h: String;
begin
  FunctionText := TrwEventTextEdit.Create(Self);
  DsgnRes.InitEditor(FunctionText);
  FunctionText.Align := alClient;
  FunctionText.Parent := Panel5;
  FunctionText.Enabled := False;
  FunctionText.OnExit := FunctionTextExit;
  FunctionText.OnSelectionChange := ShowCursorPos;

  FStoreGlobalVarFunc := TrwGlobalVarFunct.Create(nil);

  PTypInf := TypeInfo(TrwVarTypeKind);
  PTypDat := GetTypeData(PTypInf);

  for i := PTypDat^.MinValue to PTypDat^.MaxValue do
  begin
    h := GetEnumName(PTypInf, i);
    Delete(h, 1, 3);
    cbType.Items.Add(h);
  end;
end;

procedure TrwGlobalVarFunction.FormDestroy(Sender: TObject);
begin
  FStoreGlobalVarFunc.Free;
  FunctionText := nil;
end;


function TrwGlobalVarFunction.AddVarNode(AVar: TrwGlobalVariable): TListItem;
begin
  Result := lvVar.Items.Add;
  Result.Data := AVar;
  Result.Caption := AVar.Name;
end;


function TrwGlobalVarFunction.AddFunctNode(AFunct: TrwEvent): TListItem;
begin
  Result := lvFunct.Items.Add;
  Result.Data := AFunct;
  Result.Caption := AFunct.Name;
end;


procedure TrwGlobalVarFunction.btnAddClick(Sender: TObject);
var
  V: TrwGlobalVariable;
  F: TrwEvent;
begin
  if pcPages.ActivePage = tsVar then
  begin
    V := FGlobalVarFunc.Variables.Add;
    V.Name := 'NewVariable';
    V.VarType := rwvInteger;
    V.Visibility := rvvPrivate;
    V.Value := 0;
    lvVar.Selected := AddVarNode(V);
    lvVar.Invalidate;
  end
  else
  begin
    F := FGlobalVarFunc.EventsList[FGlobalVarFunc.EventsList.Add('NewProc', '', rwvUnknown, '')];
    F.HandlersText := '';
    lvFunct.Selected := AddFunctNode(F);
  end;
end;

procedure TrwGlobalVarFunction.lvVarChange(Sender: TObject;
  Item: TListItem; Change: TItemChange);
var
  PV: PVariant;
  i: Integer;
begin
  if csDestroying in ComponentState then exit;

  if (Change = ctState) then
    if Assigned(Item) and Assigned(Item.Data) then
    begin
      edtVarName.Text := TrwGlobalVariable(Item.Data).Name;
      edtVarDescription.Text := TrwGlobalVariable(Item.Data).DisplayName;
      cbType.ItemIndex := Ord(TrwGlobalVariable(Item.Data).VarType);
      if (TrwGlobalVariable(Item.Data).VarType = rwvPointer) and (Length(TrwGlobalVariable(Item.Data).PointerType) > 0) then
        cbType.Text := TrwGlobalVariable(Item.Data).PointerType
      else
        cbType.Text := cbType.Items[cbType.ItemIndex];

      cbVisibility.ItemIndex := Ord(TrwGlobalVariable(Item.Data).Visibility);
      chbShowInQB.Checked :=  TrwGlobalVariable(Item.Data).ShowInQB;
      chbShowInFmlEditor.Checked :=  TrwGlobalVariable(Item.Data).ShowInFmlEditor;

      if TrwGlobalVariable(Item.Data).VarType <> rwvArray then
        memValue.Text := TrwGlobalVariable(Item.Data).AsString
      else
      begin
        memValue.Clear;
        PV := TrwGlobalVariable(Item.Data).VarAddr;
        for i := VarArrayLowBound(PV^, 1) to VarArrayHighBound(PV^, 1) do
          memValue.Lines.Add(PV^[i]);
      end;

      grbVar.Enabled := True;
    end
    else
    begin
      edtVarName.Text := '';
      edtVarDescription.Text := '';
      cbType.ItemIndex := -1;
      cbVisibility.ItemIndex := -1;
      memValue.Text := '';
      grbVar.Enabled := False;
      chbShowInQB.Checked := False;
      chbShowInFmlEditor.Checked := False;
    end;
end;

procedure TrwGlobalVarFunction.cbTypeChange(Sender: TObject);
begin
  if Assigned(lvVar.Selected) then
  begin
    if TrwGlobalVariable(lvVar.Selected.Data).InheritedItem and
      (TrwVarTypeKind(cbType.ItemIndex) <> TrwGlobalVariable(lvVar.Selected.Data).VarType) then
    begin
      cbType.ItemIndex := Ord(TrwGlobalVariable(lvVar.Selected.Data).VarType);
      raise ErwException.Create(ResErrInheritedChange);
    end;

    if cbType.ItemIndex <> -1 then
      TrwGlobalVariable(lvVar.Selected.Data).VarType := TrwVarTypeKind(cbType.ItemIndex)
    else
    begin
      TrwGlobalVariable(lvVar.Selected.Data).VarType := rwvPointer;
      TrwGlobalVariable(lvVar.Selected.Data).PointerType := cbType.Text;
    end;
  end;
end;

procedure TrwGlobalVarFunction.btnDelClick(Sender: TObject);
var
  i: Integer;
begin
  if pcPages.ActivePage = tsVar then
    if Assigned(lvVar.Selected) then
    begin
      if TrwGlobalVariable(lvVar.Selected.Data).InheritedItem then
        raise ErwException.Create(ResErrInheritedDelete);
      TrwGlobalVariable(lvVar.Selected.Data).Free;
      lvVar.Selected.Data := nil;
      lvVar.Selected.Free;
      lvVar.Invalidate;
    end
    else
  else
    if Assigned(lvFunct.Selected) then
    begin
      i := FGlobalVarFunc.EventsList.IndexOf(TrwEvent(lvFunct.Selected.Data).Name);
      if FGlobalVarFunc.EventsList[i].InheritedItem then
        raise ErwException.Create(ResErrInheritedDelete);

      if IsRWProc(FGlobalVarFunc.EventsList[i].Name) then
        raise ErwException.Create(ResErrRWProcDelete);

      FGlobalVarFunc.EventsList.Delete(i);
      lvFunct.Selected.Data := nil;
      lvFunct.Selected.Free;
    end;
end;

procedure TrwGlobalVarFunction.edtVarNameExit(Sender: TObject);
begin
  if Assigned(lvVar.Selected) then
  begin
    if TrwGlobalVariable(lvVar.Selected.Data).InheritedItem and
      (edtVarName.Text <> TrwGlobalVariable(lvVar.Selected.Data).Name) then
    begin
      edtVarName.Text := TrwGlobalVariable(lvVar.Selected.Data).Name;
      raise ErwException.Create(ResErrInheritedChange);
    end;
    TrwGlobalVariable(lvVar.Selected.Data).Name := edtVarName.Text;
    lvVar.Selected.Caption := edtVarName.Text;
  end;
end;

procedure TrwGlobalVarFunction.edtVarNameKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    TEdit(Sender).OnExit(nil);
end;

procedure TrwGlobalVarFunction.memValueExit(Sender: TObject);
var
  i: Integer;
  PV: PVariant;
begin
  if Assigned(lvVar.Selected) then
  begin
    if TrwGlobalVariable(lvVar.Selected.Data).VarType <> rwvArray then
    begin
      TrwGlobalVariable(lvVar.Selected.Data).Value := memValue.Text;
      TrwGlobalVariable(lvVar.Selected.Data).Value :=
        InternalTypeToVariant(TrwGlobalVariable(lvVar.Selected.Data).VarType,
        TrwGlobalVariable(lvVar.Selected.Data).Value);
    end
    else
    begin
      PV := TrwGlobalVariable(lvVar.Selected.Data).VarAddr;
      VarArrayRedim(PV^, memValue.Lines.Count - 1 + VarArrayLowBound(PV^, 1));
      for i := 0 to memValue.Lines.Count - 1 do
        PV^[i] := memValue.Lines[i];
    end;
  end;
end;

procedure TrwGlobalVarFunction.memValueKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    memValue.OnExit(nil);
end;

procedure TrwGlobalVarFunction.lvFunctChange(Sender: TObject;
  Item: TListItem; Change: TItemChange);
begin
  if Assigned (FunctionText) then
    if (Change = ctState) then
      if Assigned(Item) and Assigned(Item.Data) then
        ShowInheritedFunctions(TrwEvent(Item.Data))
      else
        ShowInheritedFunctions(nil);
end;


procedure TrwGlobalVarFunction.FunctionTextExit(Sender: TObject);
var
  i, lBegPos, lEndPos, lEnd: Integer;
  lName: String;
  h: String;
begin
  if not Assigned(lvFunct.Selected) then Exit;

  try
    lName := ParseFunctionHeader(FunctionText.Text, TrwEvent(lvFunct.Selected.Data).Params, lBegPos, lEndPos, lEnd);

    while (lEnd <= Length(FunctionText.Text)) and (FunctionText.Text[lEnd] = ' ') do
      Inc(lEnd);

    if FunctionText.Text[lEnd] = #13 then
      Inc(lEnd, 2);

    i := Length(FunctionText.Text);
    while (FunctionText.Text[i] in [#10, #13, ' ']) and (i > lEnd) do
      Dec(i);

    if (i < lEnd) or not SameText(Copy(FunctionText.Text, i - 2, 3), 'END') then
      raise ErwException.Create(ResErrSyntaxError);

    Dec(i, 3);
    h := Copy(FunctionText.Text, lEnd, i-lEnd-1);
    i := Length(h);
    while (i > 0) and (h[i] in [#10, #13]) do
      Dec(i);
    Delete(h, i, Length(h) - i);
  except
    TrwEvent(lvFunct.Selected.Data).HandlersText := FunctionText.Text;
    raise;
  end;

  if not TrwEvent(lvFunct.Selected.Data).InheritedItem and
     not IsRWProc(TrwEvent(lvFunct.Selected.Data).Name) then
  begin
    lvFunct.Selected.Caption :=  lName;
    TrwEvent(lvFunct.Selected.Data).Name := lName;
    TrwEvent(lvFunct.Selected.Data).ParamStr := Trim(Copy(FunctionText.Text, lBegPos, lEndPos-lBegPos));
  end;

  TrwEvent(lvFunct.Selected.Data).HandlersText := h;
  if TrwEvent(lvFunct.Selected.Data).HandlersText = '' then
    TrwEvent(lvFunct.Selected.Data).HandlersText := ';';
end;


procedure TrwGlobalVarFunction.ShowCursorPos(Sender: TObject);
begin
  pnPos.Caption := IntToStr(TmwCustomEdit(Sender).CaretY)+' : '+IntToStr(TmwCustomEdit(Sender).CaretX);
end;


procedure TrwGlobalVarFunction.ShowInheritedFunctions(AFunct: TrwEvent);
var
  T: TTabSheet;
  i: Integer;
  FncText: TrwEventTextEdit;
  LibComp: TrwLibComponent;

  procedure ClearTabs;
  begin
    while pcFunctions.PageCount > 0 do
    begin
      while pcFunctions.Pages[0].ControlCount > 0 do
        pcFunctions.Pages[0].Controls[0].Free;
      pcFunctions.Pages[0].Free;
    end;
  end;


  procedure ShowOneEvent;
  begin
    FunctionText.Parent := Panel5;
    pcFunctions.Visible := False;
    ClearTabs;
  end;


  procedure CreateInheritedFuncts(ALibComp: TrwLibComponent);
  var
    i: Integer;
  begin
    i := ALibComp.VMT.IndexOf(VAR_FUNCT_COMP_NAME+'.'+AFunct.Name);
    if i = -1 then Exit;

    T := TTabSheet.Create(pcFunctions);
    T.Caption := ALibComp.Name;
    T.PageControl := pcFunctions;
    FncText := TrwEventTextEdit.Create(nil);
    FncText.TabWidth := 2;
    FncText.Align := alClient;
    FncText.Parent := T;
    FncText.HighLighter := GeneralSyn;
    FncText.Lines.Text := MakeEventHandlerHeader(AFunct) + #13 + #10 + ALibComp.VMT[i].Text+ #13 + #10 + 'end';
    FncText.Lines.Delete(0);
    FncText.Readonly := True;
    FncText.OnSelectionChange := ShowCursorPos;

    i := SystemLibComponents.IndexOf(ALibComp.ParentName);
    if i <> -1 then
      CreateInheritedFuncts(SystemLibComponents.Items[i]);
  end;

begin
  if Assigned(AFunct) then
  begin
    FunctionText.Text := MakeEventHandlerHeader(AFunct)+#13#10+AFunct.HandlersText+#13#10+'end';
    FunctionText.Enabled := True;
  end
  else
  begin
    FunctionText.Text := '';
    FunctionText.Enabled := False;
  end;


  if Assigned(AFunct) and AFunct.InheritedItem and FComponent.IsLibComponent then
  begin
    FunctionText.Parent := nil;
    ClearTabs;
    T := TTabSheet.Create(pcFunctions);
    T.Caption := 'Overriding';
    T.PageControl := pcFunctions;
    FunctionText.Parent := T;
    i := SystemLibComponents.IndexOf(FComponent._LibComponent);
    if i <> -1 then
    begin
      LibComp := SystemLibComponents.Items[i];
      CreateInheritedFuncts(LibComp);
    end;

    pcFunctions.Visible := True;
  end
  else
    ShowOneEvent;
end;


procedure TrwGlobalVarFunction.cbVisibilityChange(Sender: TObject);
begin
  if Assigned(lvVar.Selected) then
  begin
    if TrwGlobalVariable(lvVar.Selected.Data).InheritedItem and
      (cbVisibility.ItemIndex <> Ord(TrwGlobalVariable(lvVar.Selected.Data).Visibility)) then
    begin
      cbVisibility.ItemIndex := Ord(TrwGlobalVariable(lvVar.Selected.Data).Visibility);
      raise ErwException.Create(ResErrInheritedChange);
    end;
    TrwGlobalVariable(lvVar.Selected.Data).Visibility := TrwVarVisibility(cbVisibility.ItemIndex);
    lvVar.Invalidate;
  end;
end;

procedure TrwGlobalVarFunction.lvVarDrawItem(Sender: TCustomListView;
  Item: TListItem; Rect: TRect; State: TOwnerDrawState);
var
  Btmp: TBitmap;
begin
  if TrwGlobalVariable(Item.Data).InheritedItem then
  begin
    Btmp := TBitmap.Create;
    try
      DsgnRes.ilPict.GetBitmap(25, Btmp);
      lvVar.Canvas.Draw(Rect.Left + 2, Rect.Top, Btmp);
    finally
      Btmp.Free;
    end;
  end

  else
  begin
    lvVar.Canvas.Brush.Color := lvVar.Color;
    lvVar.Canvas.FillRect(Rect);
  end;

  Rect.Left := Rect.Left + 18;

  case TrwGlobalVariable(Item.Data).Visibility of
    rvvPublished:  lvVar.Canvas.Font.Color := clNavy;
    rvvPublic:     lvVar.Canvas.Font.Color := clBlack;
    rvvProtected:  lvVar.Canvas.Font.Color := clGray;
    rvvPrivate:    lvVar.Canvas.Font.Color := clSilver;
  end;

  lvVar.Canvas.Brush.Color := lvVar.Color;
  lvVar.Canvas.FillRect(Rect);

  Rect.Right := Rect.Left + lvVar.Canvas.TextWidth(Item.Caption + '  ');
  Rect.Bottom := Rect.Top + lvVar.Canvas.TextHeight(Item.Caption + '  ') + 2;

  if (odSelected in State) then
  begin
    if lvVar.Focused then
    begin
      lvVar.Canvas.Brush.Color := clHighlight;
      lvVar.Canvas.Font.Color := clWhite;
    end
    else
    begin
      lvVar.Canvas.Brush.Color := clInactiveBorder;
      lvVar.Canvas.Font.Color := clBlack;
    end;
    lvVar.Canvas.FillRect(Rect);
  end;

  lvVar.Canvas.TextOut(Rect.Left + 2, Rect.Top, Item.Caption);

  if (odSelected in State) and lvVar.Focused then
    lvVar.Canvas.DrawFocusRect(Rect);
end;

procedure TrwGlobalVarFunction.FormShow(Sender: TObject);
var
  ce: TmwCustomEdit;
begin
  if ActiveControl is TmwCustomEdit then
  begin
    ce := TmwCustomEdit(ActiveControl);
    if ce.TopLine + ce.LinesInWindow - 1 < ce.CaretY then
      ce.TopLine := ce.CaretY - ce.LinesInWindow + 1;
    if ce.LeftChar + ce.CharsInWindow - 1 < ce.CaretX then
      ce.LeftChar := ce.CaretX - ce.CharsInWindow + 1;
  end;    
end;

function TrwGlobalVarFunction.IsRWProc(AName: String): Boolean;
begin
  Result :=
    (AnsiSameText('Set', Copy(AName, 1, 3)) or AnsiSameText('Get', Copy(AName, 1, 3))) and
    (FComponent._UserProperties.IndexOf(Copy(AName, 4, Length(AName) - 3)) <> -1);
end;

procedure TrwGlobalVarFunction.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if not (ModalResult in [mrOK, mrAbort]) then
    case MessageDlg('Do you want to save changes you made?', mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
      mrYes: ModalResult := mrOK;
      mrNo:  ModalResult := mrCancel;
    else
      CanClose := False;
    end;
end;

procedure TrwGlobalVarFunction.btnCancelClick(Sender: TObject);
begin
  Close;
  ModalResult := mrAbort;
end;

procedure TrwGlobalVarFunction.edtVarDescriptionExit(Sender: TObject);
begin
  if Assigned(lvVar.Selected) then
  begin
    if TrwGlobalVariable(lvVar.Selected.Data).InheritedItem and
      (edtVarDescription.Text <> TrwGlobalVariable(lvVar.Selected.Data).DisplayName) then
    begin
      edtVarDescription.Text := TrwGlobalVariable(lvVar.Selected.Data).DisplayName;
      raise ErwException.Create(ResErrInheritedChange);
    end;
    TrwGlobalVariable(lvVar.Selected.Data).DisplayName := edtVarDescription.Text;
  end;
end;

procedure TrwGlobalVarFunction.chbShowInQBClick(Sender: TObject);
begin
  if Assigned(lvVar.Selected) then
  begin
    if TrwGlobalVariable(lvVar.Selected.Data).InheritedItem and
      (chbShowInQB.Checked <> TrwGlobalVariable(lvVar.Selected.Data).ShowInQB) then
    begin
      chbShowInQB.Checked := TrwGlobalVariable(lvVar.Selected.Data).ShowInQB;
      raise ErwException.Create(ResErrInheritedChange);
    end;
    TrwGlobalVariable(lvVar.Selected.Data).ShowInQB := chbShowInQB.Checked;
  end;
end;

procedure TrwGlobalVarFunction.chbShowInFmlEditorClick(Sender: TObject);
begin
  if Assigned(lvVar.Selected) then
  begin
    if TrwGlobalVariable(lvVar.Selected.Data).InheritedItem and
      (chbShowInFmlEditor.Checked <> TrwGlobalVariable(lvVar.Selected.Data).ShowInFmlEditor) then
    begin
      chbShowInFmlEditor.Checked := TrwGlobalVariable(lvVar.Selected.Data).ShowInFmlEditor;
      raise ErwException.Create(ResErrInheritedChange);
    end;
    TrwGlobalVariable(lvVar.Selected.Data).ShowInFmlEditor := chbShowInFmlEditor.Checked;
  end;
end;

end.
