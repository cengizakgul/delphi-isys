unit rwSetPropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwPropertyEditorFrm, StdCtrls, CheckLst;

type

  {TrwSetPropertyEditor is property editor for SET property}

  TrwSetPropertyEditor = class(TrwPropertyEditor)
    clbSetNames: TCheckListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    SetValues: TStringList;

  protected
    procedure SetReadOnly; override;
  end;

implementation

{$R *.DFM}

procedure TrwSetPropertyEditor.FormCreate(Sender: TObject);
begin
  inherited;
  SetValues := TStringList.Create;
  NamesList := clbSetNames.Items;
end;

procedure TrwSetPropertyEditor.FormShow(Sender: TObject);
var
  i, j: Integer;
begin
  inherited;

  if StrValue1 = '[]' then
  begin
    clbSetNames.AllowGrayed := True;
    for i := 0 to clbSetNames.Items.Count-1 do
      clbSetNames.State[i] := cbGrayed;
  end
  else
  begin
    SetValues.CommaText := StrValue1;
    for i := 0 to SetValues.Count - 1 do
    begin
      j := NamesList.IndexOf(SetValues[i]);
      clbSetNames.Checked[j] := True;
    end;
  end;
end;

procedure TrwSetPropertyEditor.FormDestroy(Sender: TObject);
begin
  SetValues.Free;
  inherited;
end;

procedure TrwSetPropertyEditor.btnOKClick(Sender: TObject);
var
  i, j: Integer;
begin
  inherited;

  StrValue1 := '';
  StrValue2 := '';
  for i := 0 to NamesList.Count - 1 do
  begin
    j := SetValues.IndexOf(NamesList[i]);

    if clbSetNames.State[i] = cbChecked then
      if not clbSetNames.AllowGrayed then
        if (j = -1) then
        begin
          if (StrValue1 <> '') then
            StrValue1 := StrValue1 + ',';
          StrValue1 := StrValue1 + NamesList[i];
        end
        else
      else
      begin
        if (StrValue1 <> '') then
          StrValue1 := StrValue1 + ',';
        StrValue1 := StrValue1 + NamesList[i];
      end

    else if clbSetNames.State[i] = cbUnChecked then
      if not clbSetNames.AllowGrayed then
        if (j <> -1) then
        begin
          if (StrValue2 <> '') then
            StrValue2 := StrValue2 + ',';
          StrValue2 := StrValue2 + NamesList[i];
        end
        else
      else
      begin
        if (StrValue2 <> '') then
          StrValue2 := StrValue2 + ',';
        StrValue2 := StrValue2 + NamesList[i];
      end;
   end

end;

procedure TrwSetPropertyEditor.SetReadOnly;
begin
  inherited;
  clbSetNames.Enabled := False;
end;

initialization
  RegisterClass(TrwSetPropertyEditor);

end.
