inherited rwLibToolsBar: TrwLibToolsBar
  Width = 1150
  inherited ctbBar: TControlBar
    Width = 1150
    inherited tlbComponents: TToolBar
      Width = 782
      inherited tbtSelect: TToolButton
        Tag = -1
        Visible = False
      end
      inherited tbtLabel: TToolButton
        Tag = 1
      end
      inherited tbtPCLLabel: TToolButton
        Tag = 1
      end
      inherited tbtMemo: TToolButton
        Tag = 1
      end
      inherited tbtRichText: TToolButton
        Tag = 1
      end
      inherited tbtSysLabel: TToolButton
        Tag = 1
      end
      inherited tbtImage: TToolButton
        Tag = 1
      end
      inherited tbtContainer: TToolButton
        Tag = 1
      end
      inherited tbtFrame: TToolButton
        Tag = 1
      end
      inherited tbtShape: TToolButton
        Tag = 1
      end
      inherited tbtSubReport: TToolButton
        Visible = False
      end
      inherited tbtDBText: TToolButton
        Tag = 1
      end
      inherited tbtDBImage: TToolButton
        Tag = 1
      end
      inherited ToolButton15: TToolButton
        Grouped = True
      end
      object ToolButton17: TToolButton
        Left = 414
        Top = 0
        Hint = 'RM Report'
        Caption = 'TrmReport'
        Grouped = True
        ImageIndex = 38
        Visible = False
      end
      object tbtTable: TToolButton
        Tag = 1
        Left = 437
        Top = 0
        Hint = 'Table'
        Caption = 'TrwTable'
        Grouped = True
        ImageIndex = 73
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tlbDBTable: TToolButton
        Tag = 1
        Left = 460
        Top = 0
        Hint = 'DBTable'
        Caption = 'TrwDBTable'
        Grouped = True
        ImageIndex = 92
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tlbFileDialog: TToolButton
        Left = 483
        Top = 0
        Hint = 'FileDialog'
        Caption = 'TrwFileDialog'
        Grouped = True
        ImageIndex = 87
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton18: TToolButton
        Tag = 2
        Left = 506
        Top = 0
        Hint = 'Text'
        Caption = 'TrwText'
        Grouped = True
        ImageIndex = 30
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtButton: TToolButton
        Tag = 2
        Left = 529
        Top = 0
        Hint = 'Button'
        Caption = 'TrwButton'
        Grouped = True
        ImageIndex = 85
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton29: TToolButton
        Tag = 2
        Left = 552
        Top = 0
        Hint = 'Panel'
        Caption = 'TrwPanel'
        Grouped = True
        ImageIndex = 36
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton25: TToolButton
        Tag = 2
        Left = 575
        Top = 0
        Hint = 'GroupBox'
        Caption = 'TrwGroupBox'
        Grouped = True
        ImageIndex = 53
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton19: TToolButton
        Tag = 2
        Left = 598
        Top = 0
        Hint = 'Edit'
        Caption = 'TrwEdit'
        Grouped = True
        ImageIndex = 48
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton24: TToolButton
        Tag = 2
        Left = 621
        Top = 0
        Hint = 'DateEdit'
        Caption = 'TrwDateEdit'
        Grouped = True
        ImageIndex = 52
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton22: TToolButton
        Tag = 2
        Left = 644
        Top = 0
        Hint = 'CheckBox'
        Caption = 'TrwCheckBox'
        Grouped = True
        ImageIndex = 50
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton23: TToolButton
        Tag = 2
        Left = 667
        Top = 0
        Hint = 'ComboBox'
        Caption = 'TrwComboBox'
        Grouped = True
        ImageIndex = 51
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton26: TToolButton
        Tag = 2
        Left = 690
        Top = 0
        Hint = 'RadioGroup'
        Caption = 'TrwRadioGroup'
        Grouped = True
        ImageIndex = 54
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton28: TToolButton
        Tag = 2
        Left = 713
        Top = 0
        Hint = 'PageControl'
        Caption = 'TrwPageControl'
        Grouped = True
        ImageIndex = 84
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton21: TToolButton
        Tag = 2
        Left = 736
        Top = 0
        Hint = 'DBComboBox'
        Caption = 'TrwDBComboBox'
        Grouped = True
        ImageIndex = 49
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton27: TToolButton
        Tag = 2
        Left = 759
        Top = 0
        Hint = 'DBGrid'
        Caption = 'TrwDBGrid'
        Grouped = True
        ImageIndex = 73
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
    end
    inherited tlbFont: TToolBar
      Left = 1111
    end
    inherited tlbExperts: TToolBar
      Left = 987
      inherited tbtLibrary: TToolButton
        Visible = False
      end
    end
    inherited tlbActions: TToolBar
      Visible = False
    end
  end
  inherited pmToolBars: TPopupMenu
    Left = 792
    Top = 18
  end
end
