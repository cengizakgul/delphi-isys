unit rmTrmlCaptionControlPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmlCustomControlPropFrm, rmOPEReportParamFrm,
  rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, StdCtrls, ExtCtrls,
  ComCtrls, rmOPEEnumFrm, rmOPEFontFrm, rmOPEColorFrm;

type
  TrmTrmlCaptionControlProp = class(TrmTrmlCustomControlProp)
    GroupBox2: TGroupBox;
    frmCaption: TrmOPEStringSingleLine;
    frmCaptionLayout: TrmOPEEnum;
    frmCaptionFont: TrmOPEFont;
  protected
    procedure GetPropsFromObject; override;
  end;

implementation

{$R *.dfm}

{ TrmTrmlCaptionControlProp }

procedure TrmTrmlCaptionControlProp.GetPropsFromObject;
begin
  inherited;
  frmCaption.PropertyName := 'Caption';
  frmCaption.ShowPropValue;

  frmCaptionLayout.MakeEnumList(['Left', 'Top'], [0, 1]);
  frmCaptionLayout.PropertyName := 'CaptionLayout';  
  frmCaptionLayout.ShowPropValue;

  frmCaptionFont.PropertyName := 'CaptionFont';
end;

end.
