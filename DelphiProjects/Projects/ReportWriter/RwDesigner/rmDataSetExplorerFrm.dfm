object rmDataSetExplorer: TrmDataSetExplorer
  Left = 0
  Top = 0
  Width = 688
  Height = 338
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 30
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 4
      Top = 7
      Width = 111
      Height = 13
      Caption = 'DataSet expression'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edDataSet: TEdit
      Left = 119
      Top = 4
      Width = 242
      Height = 21
      TabOrder = 0
      OnExit = edDataSetExit
      OnKeyPress = edDataSetKeyPress
    end
    object Panel2: TPanel
      Left = 398
      Top = 0
      Width = 340
      Height = 29
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object Label3: TLabel
        Left = 7
        Top = 7
        Width = 51
        Height = 13
        Caption = 'Rec Count'
      end
      object Label2: TLabel
        Left = 114
        Top = 7
        Width = 60
        Height = 13
        Caption = 'Rec Number'
      end
      object edRecCount: TEdit
        Left = 62
        Top = 4
        Width = 38
        Height = 21
        TabOrder = 0
      end
      object edRecNo: TEdit
        Left = 177
        Top = 4
        Width = 38
        Height = 21
        TabOrder = 1
      end
      object chbBOF: TCheckBox
        Left = 237
        Top = 7
        Width = 44
        Height = 17
        Caption = 'BOF'
        TabOrder = 2
      end
      object chbEOF: TCheckBox
        Left = 293
        Top = 7
        Width = 44
        Height = 17
        Caption = 'EOF'
        TabOrder = 3
      end
    end
  end
  object grData: TisRWDBGrid
    Left = 0
    Top = 30
    Width = 688
    Height = 308
    DisableThemesInTitle = False
    IniAttributes.Enabled = True
    IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
    IniAttributes.SectionName = 'TrmDataSetExplorer\grData'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = dsExpl
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    Sorting = False
    RecordCountHintEnabled = False
  end
  object DS: TisRWClientDataSet
    Left = 128
    Top = 112
  end
  object dsExpl: TwwDataSource
    AutoEdit = False
    DataSet = DS
    Left = 160
    Top = 112
  end
end
