unit rmTrmlCustomControlPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm,
  StdCtrls, ExtCtrls, ComCtrls, rmOPEReportParamFrm, rwEngineTypes,
  rmOPEColorFrm;

type
  TrmTrmlCustomControlProp = class(TrmOPBasic)
    tsParam: TTabSheet;
    frmParam1: TrmOPEReportParam;
    frmColor: TrmOPEColor;
  private
  protected
    procedure AssignParmType; virtual;
    procedure GetPropsFromObject; override;
  public
  end;

implementation

{$R *.dfm}

{ TrmTrmlCustomControlProp }

procedure TrmTrmlCustomControlProp.AssignParmType;
begin
  frmParam1.ParamType := rwvString; 
end;

procedure TrmTrmlCustomControlProp.GetPropsFromObject;
begin
  inherited;
  AssignParmType;
  frmParam1.ShowPropValue;
end;

end.
