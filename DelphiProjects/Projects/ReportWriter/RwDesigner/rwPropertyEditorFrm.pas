unit rwPropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwDebugInfo;

type
  {TrwPropertyEditor is abstract property editor}

  TrwPropertyEditor = class(TForm)
    btnOK: TButton;
    btnCancel: TButton;

  private
    FOrdValue: LongInt;
    FNamesList: TStrings;
    FStrValue1: string;
    FStrValue2: string;
    FFloatValue: Extended;
    FVariantValue: Variant;
    FCheckInheritProp: Boolean;
    FPrintableObject: boolean;

  private

  protected
    FValue: TPersistent;

    procedure SetReadOnly; virtual;

  public
    constructor Create(AComponent: TComponent; APrintableObject : boolean); reintroduce;

    property Value: TPersistent read FValue;
    property OrdValue: LongInt read FOrdValue write FOrdValue;
    property FloatValue: Extended read FFloatValue write FFloatValue;
    property StrValue1: string read FStrValue1 write FStrValue1;
    property StrValue2: string read FStrValue2 write FStrValue2;
    property VariantValue: Variant read FVariantValue write FVariantValue;
    property NamesList: TStrings read FNamesList write FNamesList;
    property CheckInheritProp: Boolean read FCheckInheritProp write FCheckInheritProp;
    property PrintableObject : boolean read FPrintableObject;
  end;

  TrwPropertyEditorClass = class of TrwPropertyEditor;

implementation

{$R *.DFM}

{ TrwPropertyEditor }

constructor TrwPropertyEditor.Create(AComponent: TComponent; APrintableObject : boolean);
begin
  inherited Create(AComponent);
  FCheckInheritProp := True;
  FPrintableObject := APrintableObject;

  if Debugging then
    SetReadOnly;
end;

procedure TrwPropertyEditor.SetReadOnly;
begin
  btnOK.Enabled := False;
end;

end.
