unit rmTrmXMLQueryElementPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmXMLNodePropFrm, rmOPEMasterFieldsFrm, rmOPEQueryFrm, StdCtrls,
  ExtCtrls, rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, ComCtrls,
  rmOPEExpressionFrm, rmOPEBlockingControlFrm, rmOPBasicFrm,
  rmOPEBooleanFrm;

type
  TrmTrmXMLQueryElementProp = class(TrmTrmXMLNodeProp)
    frmQuery: TrmOPEQuery;
    frmMasterFields: TrmOPEMasterFields;
    Panel1: TPanel;
    frmProduceBranchIf: TrmOPEBlockingControl;
    frmProduceIf: TrmOPEBlockingControl;
    procedure frmQuerybtnQueryClick(Sender: TObject);
  private
  public
    procedure GetPropsFromObject; override;
  end;

implementation

{$R *.dfm}

procedure TrmTrmXMLQueryElementProp.frmQuerybtnQueryClick(Sender: TObject);
begin
  frmQuery.btnQueryClick(Sender);
  frmMasterFields.ShowPropValue;
end;

procedure TrmTrmXMLQueryElementProp.GetPropsFromObject;
begin
  inherited;

  frmProduceBranchIf.BlockID := 'BranchCalc';
  frmProduceBranchIf.ShowPropValue;
  
  frmProduceIf.BlockID := 'Calc';
  frmProduceIf.ShowPropValue;

  frmQuery.ShowPropValue;
end;

end.
