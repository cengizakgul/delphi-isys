unit rwAppSelectionFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls;

type
  TrwAppSelection = class(TForm)
    btnOK: TButton;
    btnCancel: TButton;
    lvApplications: TListView;
    Label1: TLabel;
    pnlModeSelection: TPanel;
    btnRW: TButton;
    btnRM: TButton;
    lFile: TLabel;
    procedure lvApplicationsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure lvApplicationsDblClick(Sender: TObject);
    procedure btnRMClick(Sender: TObject);
    procedure btnRWClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FMode: Integer;
  public
  end;

var
  rwAppSelection: TrwAppSelection;

  function  ChooseAdapterFor(const AFileName: String; out AAdapterName: String): Integer;

implementation

{$R *.dfm}

uses rwEngineTypes, rwAdapterUtils;


function ChooseAdapterFor(const AFileName: String; out AAdapterName: String): Integer;
var
  AdapterPath: String;
  Frm: TrwAppSelection;
  LI: TListItem;
  Adapters: TrwAppAdaptersList;
  i: Integer;
begin
  Frm := TrwAppSelection.Create(nil);
  try
    AdapterPath := ExtractFilePath(Application.ExeName);
    Adapters := FindRWAdaptersInFolder(AdapterPath);

    for i := Low(Adapters) to High(Adapters) do
    begin
      LI := Frm.lvApplications.Items.Add;
      LI.Caption := Adapters[i].ApplicationName;
      LI.SubItems.Add(Adapters[i].ModuleName);
      LI.SubItems.Add(Adapters[i].Version);
    end;

    Frm.lFile.Caption := 'File: ' + AFileName;

    if ExtractFileExt(AFileName) = '.rwr' then
      Frm.FMode := 1
    else if ExtractFileExt(AFileName) = '.rwq' then
      Frm.FMode := 3
    else
      Frm.FMode := 0;

    if (Frm.lvApplications.Items.Count = 1) and (Frm.FMode > 0) then
      Frm.lvApplications.Items[0].Selected := True

    else if Frm.ShowModal <> mrOK then
      Frm.FMode := 0;

    Result := Frm.FMode;

    if (Result > 0) and (Frm.lvApplications.Selected <> nil) then
      AAdapterName := Frm.lvApplications.Selected.SubItems[0]

  finally
    Frm.Free;
  end;
end;


procedure TrwAppSelection.lvApplicationsSelectItem(Sender: TObject;Item: TListItem; Selected: Boolean);
begin
  btnOK.Enabled := lvApplications.Selected <> nil ;
  btnRW.Enabled := btnOK.Enabled;
  btnRM.Enabled := btnOK.Enabled;
end;

procedure TrwAppSelection.lvApplicationsDblClick(Sender: TObject);
begin
  if btnOK.Enabled then
    btnOK.Click;
end;

procedure TrwAppSelection.btnRMClick(Sender: TObject);
begin
  FMode := 2;
end;

procedure TrwAppSelection.btnRWClick(Sender: TObject);
begin
  FMode := 1;
end;

procedure TrwAppSelection.btnOKClick(Sender: TObject);
begin
  if pnlModeSelection.Visible then
    FMode := 3;
end;

procedure TrwAppSelection.FormShow(Sender: TObject);
begin
  if FMode = 0 then
  begin
    pnlModeSelection.Visible := True;
    btnOK.Caption := 'Query Builder';
  end
  else
    lFile.Visible := True;
end;

end.
