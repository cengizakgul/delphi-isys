unit rwTabOrderFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, rwCommonClasses, rwUtils;

type
  TrwTabOrder = class(TForm)
    lbControls: TListBox;
    btnTop: TButton;
    btnUp: TButton;
    btnDown: TButton;
    btnBottom: TButton;
    btnOK: TButton;
    btnCancel: TButton;
    procedure btnTopClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnBottomClick(Sender: TObject);
  private
    procedure MoveCurrentItem(NewPos: Integer);
  public
  end;

procedure ShowTabOrder(AContainer: TrwWinFormControl);

implementation

uses TypInfo;

{$R *.dfm}


procedure ShowTabOrder(AContainer: TrwWinFormControl);
var
  Frm: TrwTabOrder;
  i, j: Integer;
begin
  Frm := TrwTabOrder.Create(Application);
  with Frm do
    try
      Caption := 'Tab Order for ' + AContainer.Name;
      for i := 0 to AContainer.ControlCount - 1 do
        if IsPublishedProp(AContainer.Controls[i], 'TabOrder') then
          lbControls.Items.AddObject(AContainer.Controls[i].Name+' : '+AContainer.Controls[i].ClassName,
            AContainer.Controls[i]);

      for i := 0 to lbControls.Items.Count - 2 do
        for j := i + 1 to lbControls.Items.Count - 1 do
          if GetOrdProp(lbControls.Items.Objects[i], 'TabOrder') > GetOrdProp(lbControls.Items.Objects[j], 'TabOrder') then
            lbControls.Items.Exchange(i, j);

      if Frm.ShowModal = mrOK then
      begin
        CheckInheritedAndShowError(AContainer);

        for i := 0 to lbControls.Items.Count-1 do
          SetOrdProp(lbControls.Items.Objects[i], 'TabOrder', i);
      end;

    finally
      Frm.Free;
    end;
end;

procedure TrwTabOrder.btnTopClick(Sender: TObject);
begin
  MoveCurrentItem(0);
end;

procedure TrwTabOrder.btnUpClick(Sender: TObject);
begin
  MoveCurrentItem(lbControls.ItemIndex - 1);
end;

procedure TrwTabOrder.btnDownClick(Sender: TObject);
begin
  MoveCurrentItem(lbControls.ItemIndex + 1);
end;

procedure TrwTabOrder.btnBottomClick(Sender: TObject);
begin
  MoveCurrentItem(lbControls.Items.Count - 1);
end;

procedure TrwTabOrder.MoveCurrentItem(NewPos: Integer);
begin
  if (lbControls.ItemIndex = -1) or (NewPos < 0) or (NewPos > lbControls.Items.Count-1) then
    Exit;
  lbControls.Items.Move(lbControls.ItemIndex, NewPos);
  lbControls.ItemIndex := NewPos;
end;

end.
