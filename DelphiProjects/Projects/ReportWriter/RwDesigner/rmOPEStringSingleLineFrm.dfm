inherited rmOPEStringSingleLine: TrmOPEStringSingleLine
  Width = 290
  Height = 21
  AutoSize = True
  OnResize = FrameResize
  object lPropName: TLabel
    Left = 0
    Top = 4
    Width = 63
    Height = 13
    Caption = 'Text Property'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object edString: TEdit
    Left = 90
    Top = 0
    Width = 200
    Height = 21
    AutoSize = False
    TabOrder = 0
    OnChange = edStringChange
  end
end
