unit rwFieldsPropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwPropertyEditorFrm, StdCtrls, DB, rwDB, rwMessages, Mask, wwdbedit,
  Wwdbspin, rwTypes, rwEngineTypes;

type
  {TrwFieldsPropertyEditor is class for edition TrwFields property}

  TrwFieldsPropertyEditor = class(TrwPropertyEditor)
    GroupBox1: TGroupBox;
    lbFields: TListBox;
    btnAdd: TButton;
    btnDelete: TButton;
    btnUp: TButton;
    btnDown: TButton;
    gbField: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    edtName: TEdit;
    cmbType: TComboBox;
    chbKeyField: TCheckBox;
    Label3: TLabel;
    edtSize: TwwDBSpinEdit;
    Label4: TLabel;
    edtDisplayWidth: TwwDBSpinEdit;
    Label5: TLabel;
    edtDisplayLabel: TEdit;
    chbVisible: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure lbFieldsClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure edtNameExit(Sender: TObject);
    procedure edtNameKeyPress(Sender: TObject; var Key: Char);
    procedure cmbTypeChange(Sender: TObject);
    procedure chbKeyFieldClick(Sender: TObject);
    procedure edtSizeExit(Sender: TObject);
    procedure edtDisplayWidthExit(Sender: TObject);
    procedure chbVisibleClick(Sender: TObject);
    procedure edtDisplayLabelExit(Sender: TObject);
  protected
    procedure SetReadOnly; override;
  end;

implementation

{$R *.DFM}

procedure TrwFieldsPropertyEditor.FormCreate(Sender: TObject);
begin
  inherited;
  FValue := TrwFields.Create(nil, TrwField);
end;

procedure TrwFieldsPropertyEditor.FormDestroy(Sender: TObject);
begin
  FValue.Free;
  inherited;
end;

procedure TrwFieldsPropertyEditor.FormShow(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  lbFields.Clear;
  for i := 0 to TrwFields(FValue).Count - 1 do
    lbFields.Items.AddObject(TrwFields(FValue)[i].FieldName, TrwFields(FValue)[i]);
end;

procedure TrwFieldsPropertyEditor.btnAddClick(Sender: TObject);
var
  lField: TrwField;
begin
  lField := TrwField.Create(TrwFields(FValue));
  lField.FieldName := 'NewField';
  lField.FieldType := ftInteger;
  chbKeyField.Checked := False;
  lbFields.Items.AddObject(lField.FieldName, lField);
  lbFields.ItemIndex := lbFields.Items.Count - 1;
  lbFields.OnClick(nil);
end;

procedure TrwFieldsPropertyEditor.lbFieldsClick(Sender: TObject);
var
  lField: TrwField;
begin
  gbField.Enabled := (lbFields.ItemIndex <> -1);

  if gbField.Enabled then
  begin
    lField := TrwField(lbFields.Items.Objects[lbFields.ItemIndex]);
    edtName.Text := lField.FieldName;
    edtSize.Value := lField.FieldSize;
    chbKeyField.Checked := lField.KeyField;
    case lField.FieldType of
      ftBlob: cmbType.ItemIndex := 0;
      ftBoolean: cmbType.ItemIndex := 1;
      ftCurrency: cmbType.ItemIndex := 2;
      ftDate: cmbType.ItemIndex := 3;
      ftFloat: cmbType.ItemIndex := 4;
      ftInteger: cmbType.ItemIndex := 5;
      ftString: cmbType.ItemIndex := 6;
    else
      cmbType.ItemIndex := -1
    end;
    edtDisplayLabel.Text := lField.DisplayLabel;
    edtDisplayWidth.Value := lField.DisplayWidth;
    chbVisible.Checked := lField.Visible;
  end;
end;

procedure TrwFieldsPropertyEditor.btnDeleteClick(Sender: TObject);
var
  lField: TrwField;
begin
  if (lbFields.ItemIndex = -1) then Exit;

  lField := TrwField(lbFields.Items.Objects[lbFields.ItemIndex]);
  if lField.InheritedItem then
    raise ErwException.Create(ResErrInheritedDelete);

  lField.Free;
  lbFields.Items.Delete(lbFields.ItemIndex);
end;

procedure TrwFieldsPropertyEditor.btnUpClick(Sender: TObject);
var
  i: Integer;
begin
  i := lbFields.ItemIndex;

  if (i <= 0) then Exit;

  TrwFields(FValue)[i].Index := i - 1;
  lbFields.Items.Move(i, i - 1);
  lbFields.ItemIndex := i - 1;
end;

procedure TrwFieldsPropertyEditor.btnDownClick(Sender: TObject);
var
  i: Integer;
begin
  i := lbFields.ItemIndex;

  if (i = -1) or (i = lbFields.Items.Count - 1) then Exit;

  TrwFields(FValue)[i].Index := i + 1;
  lbFields.Items.Move(i, i + 1);
  lbFields.ItemIndex := i + 1;
end;

procedure TrwFieldsPropertyEditor.edtNameExit(Sender: TObject);
var
  lField: TrwField;
begin
  if (lbFields.ItemIndex = -1) then Exit;

  lField := TrwField(lbFields.Items.Objects[lbFields.ItemIndex]);
  if (edtName.Text <> lField.FieldName) and lField.InheritedItem then
  begin
    edtName.Text := lField.FieldName;
    raise ErwException.Create(ResErrInheritedChange);
  end;

  lField.FieldName := edtName.Text;
  lbFields.Items[lbFields.ItemIndex] := lField.FieldName;
end;

procedure TrwFieldsPropertyEditor.edtNameKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Ord(Key) = VK_RETURN) then
    TEdit(Sender).OnExit(Sender);
end;

procedure TrwFieldsPropertyEditor.cmbTypeChange(Sender: TObject);
var
  lField: TrwField;
begin
  if (lbFields.ItemIndex = -1) then Exit;

  lField := TrwField(lbFields.Items.Objects[lbFields.ItemIndex]);
  if lField.InheritedItem then
    raise ErwException.Create(ResErrInheritedChange);

  case cmbType.ItemIndex of
    0: lField.FieldType := ftBlob;
    1: lField.FieldType := ftBoolean;
    2: lField.FieldType := ftCurrency;
    3: lField.FieldType := ftDate;
    4: lField.FieldType := ftFloat;
    5: lField.FieldType := ftInteger;
    6: lField.FieldType := ftString;
  end;

  edtSize.Value := lField.FieldSize;
end;

procedure TrwFieldsPropertyEditor.chbKeyFieldClick(Sender: TObject);
var
  lField: TrwField;
begin
  lField := TrwField(lbFields.Items.Objects[lbFields.ItemIndex]);
  if (lField.KeyField <> chbKeyField.Checked) and lField.InheritedItem then
  begin
    chbKeyField.Checked := lField.KeyField;
    raise ErwException.Create(ResErrInheritedChange);
  end;

  lField.KeyField := chbKeyField.Checked;
end;

procedure TrwFieldsPropertyEditor.edtSizeExit(Sender: TObject);
var
  lField: TrwField;
begin
  if (lbFields.ItemIndex = -1) then Exit;

  lField := TrwField(lbFields.Items.Objects[lbFields.ItemIndex]);
  if (edtSize.Value <> lField.FieldSize) and lField.InheritedItem then
  begin
    edtSize.Value := lField.FieldSize;
    raise ErwException.Create(ResErrInheritedChange);
  end;

  lField.FieldSize := Trunc(edtSize.Value);
  edtSize.Value := lField.FieldSize;
end;


procedure TrwFieldsPropertyEditor.edtDisplayWidthExit(Sender: TObject);
var
  lField: TrwField;
begin
  if (lbFields.ItemIndex = -1) then Exit;

  lField := TrwField(lbFields.Items.Objects[lbFields.ItemIndex]);
  lField.DisplayWidth := Trunc(edtDisplayWidth.Value);
  edtDisplayWidth.Value := lField.DisplayWidth;
end;

procedure TrwFieldsPropertyEditor.chbVisibleClick(Sender: TObject);
var
  lField: TrwField;
begin
  lField := TrwField(lbFields.Items.Objects[lbFields.ItemIndex]);
  lField.Visible := chbVisible.Checked;
end;

procedure TrwFieldsPropertyEditor.edtDisplayLabelExit(Sender: TObject);
var
  lField: TrwField;
begin
  if (lbFields.ItemIndex = -1) then Exit;
  lField := TrwField(lbFields.Items.Objects[lbFields.ItemIndex]);
  lField.DisplayLabel := edtDisplayLabel.Text;
end;

procedure TrwFieldsPropertyEditor.SetReadOnly;
begin
  inherited;
  gbField.Enabled := False;
  btnAdd.Enabled := False;
  btnDelete.Enabled := False;
  btnUp.Enabled := False;
  btnDown.Enabled := False;
end;

initialization
  RegisterClass(TrwFieldsPropertyEditor);

end.
