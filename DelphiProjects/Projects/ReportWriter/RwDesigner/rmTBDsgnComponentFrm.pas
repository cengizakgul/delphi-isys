unit rmTBDsgnComponentFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmLocalToolBarFrm, rmCustomFrameFrm,
  rmTBComponentsFrm, Menus, rwDsgnUtils, rmTypes,
  Buttons, ExtCtrls, ActnList, ImgList, StdCtrls,
  ComCtrls, ToolWin, rmFMLExprPanelFrm, Mask, wwdbedit, Wwdbspin,
  ISBasicClasses, rwDesignClasses, ColorPicker, rmDsgnTypes, Math, rwUtils,
  rwGraphics, rwPrinters, rmFormula, rwBasicClasses, rmTBMDIChildFrameFrm, rwDebugInfo;

type
  TrmTBDsgnComponent = class(TrmTBMDIChildFrame)
    tbComponents: TrmTBComponents;
    ShowPaperGrid1: TMenuItem;
    miShowTableGrid: TMenuItem;
    miShowPaperGrid: TMenuItem;
    File1: TMenuItem;
    miOpen: TMenuItem;
    miSave: TMenuItem;

    actCopy: TAction;
    actCut: TAction;
    actPaste: TAction;
    actDelete: TAction;
    actOpen: TAction;
    actSave: TAction;
    actZoomIn: TAction;
    actZoomOut: TAction;
    actCompile: TAction;
    actShowTableGrid: TAction;
    actExpertMode: TAction;
    actShowPaperGrid: TAction;
    actQB: TAction;
    actFormulaEditor: TAction;
    actInpFormParams: TAction;
    actAggrFunctSUM: TAction;
    actAggrFunctSUMVal: TAction;
    actAggrFunctCount: TAction;
    actObjProps: TAction;
    tlbSettings: TToolBar;
    btnZoomIn: TToolButton;
    cbZoom: TComboBox;
    btnZoomOut: TToolButton;
    tlbExperts: TToolBar;
    ToolButton9: TToolButton;
    btnInpFormParams: TToolButton;
    btnQB: TToolButton;
    tlbTextStyle: TToolBar;
    cbFontName: TComboBox;
    cbFontSize: TComboBox;
    ToolButton4: TToolButton;
    tbtFontBold: TToolButton;
    tbtFontItalic: TToolButton;
    tbtFontUnderline: TToolButton;
    ToolButton1: TToolButton;
    tbtTextAlignLeft: TToolButton;
    tbtTextAlignCenter: TToolButton;
    tbtTextAlignRight: TToolButton;
    ToolButton10: TToolButton;
    tbtTopLayout: TToolButton;
    tbtCenterLayout: TToolButton;
    tbtBottomLayout: TToolButton;
    ToolButton23: TToolButton;
    tbtFontColor: TColorBtn;
    tbtBackgroundColor: TColorBtn;
    tlbPosSize: TToolBar;
    btnAlignLeft: TToolButton;
    btnAlignTop: TToolButton;
    btnAlignClient: TToolButton;
    btnAlignBottom: TToolButton;
    btnAlignRight: TToolButton;
    ToolBar2: TToolBar;
    tlbASCIIProps: TPanel;
    lASCIISize: TLabel;
    edASCIISize: TisRWDBSpinEdit;
    ToolBar1: TToolBar;
    lFormulaCaption: TLabel;
    pnlFormula: TrmFMLExprPanel;
    btnFormulaEditor: TToolButton;
    btnAggrFunct: TToolButton;
    pmAggrFunct: TPopupMenu;
    miAggrFunctSUMVal: TMenuItem;
    N4: TMenuItem;
    miAggrFunctSUM: TMenuItem;
    miAggrFunctCount: TMenuItem;
    tlbCopy: TToolBar;
    tbtCut: TToolButton;
    tbtCopy: TToolButton;
    tbtPaste: TToolButton;
    tlbCompile: TToolBar;
    tbtCompile: TToolButton;
    ToolBar3: TToolBar;
    tbtOpen: TToolButton;
    tbtSave: TToolButton;
    pmReportParams: TPopupMenu;
    miShowInputForm: TMenuItem;
    miSetParametersFile: TMenuItem;
    Edit1: TMenuItem;
    miCopy: TMenuItem;
    miCut: TMenuItem;
    miPaste: TMenuItem;
    miDelete: TMenuItem;
    miSep1: TMenuItem;
    miExpertMode: TMenuItem;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    actObjectTree: TAction;
    miObjectsTree: TMenuItem;
    miSep2: TMenuItem;
    actEventsEditor: TAction;
    miEventsEditor: TMenuItem;
    actObjectInspector: TAction;
    ObjectInspector1: TMenuItem;
    tlbComponntsAlignment: TToolBar;
    tbtAlignmentLeft: TToolButton;
    tbtAlignmentTop: TToolButton;
    tbtAlignmentBottom: TToolButton;
    tbtAlignmentRight: TToolButton;
    tblAlignCenter: TToolButton;
    tblAlignMiddle: TToolButton;
    tblAlignByContainerCenter: TToolButton;
    tblAlignByContainerMiddle: TToolButton;
    tblVerticalSpacesEqually: TToolButton;
    tblHorisontalSpacesEqually: TToolButton;
    miShowBookmarks: TMenuItem;
    actShowBookmarks: TAction;
    actEditDelimiter: TMenuItem;
    miBookmarkObject: TMenuItem;
    actToggleBookmark: TAction;
    btnShowAllEvents: TToolButton;
    actShowAllEvents: TAction;
    actShowAllFormulas: TAction;
    btnShowAllFormulas: TToolButton;

    procedure miShowPaperGridClick(Sender: TObject);
    procedure miShowTableGridClick(Sender: TObject);
    procedure actCompileExecute(Sender: TObject);
    procedure actCopyExecute(Sender: TObject);
    procedure actOpenExecute(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actCutExecute(Sender: TObject);
    procedure actPasteExecute(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
    procedure btnAlignLeftClick(Sender: TObject);
    procedure edASCIISizeChange(Sender: TObject);
    procedure cbFontNameChange(Sender: TObject);
    procedure cbFontSizeChange(Sender: TObject);
    procedure tbtFontBoldClick(Sender: TObject);
    procedure tbtFontItalicClick(Sender: TObject);
    procedure tbtFontUnderlineClick(Sender: TObject);
    procedure tbtTextAlignLeftClick(Sender: TObject);
    procedure tbtTopLayoutClick(Sender: TObject);
    procedure tbtFontColorClick(Sender: TObject);
    procedure tbtBackgroundColorClick(Sender: TObject);
    procedure btnAggrFunctClick(Sender: TObject);
    procedure actObjPropsExecute(Sender: TObject);
    procedure actQBExecute(Sender: TObject);
    procedure btnInpFormParamsClick(Sender: TObject);
    procedure miSetParametersFileClick(Sender: TObject);
    procedure actZoomInExecute(Sender: TObject);
    procedure cbZoomExit(Sender: TObject);
    procedure cbZoomKeyPress(Sender: TObject; var Key: Char);
    procedure cbZoomSelect(Sender: TObject);
    procedure actZoomOutExecute(Sender: TObject);
    procedure actInpFormParamsExecute(Sender: TObject);
    procedure actExpertModeExecute(Sender: TObject);
    procedure actAggrFunctSUMExecute(Sender: TObject);
    procedure actAggrFunctSUMValExecute(Sender: TObject);
    procedure actAggrFunctCountExecute(Sender: TObject);
    procedure actFormulaEditorExecute(Sender: TObject);
    procedure actObjectTreeExecute(Sender: TObject);
    procedure actEventsEditorExecute(Sender: TObject);
    procedure actObjectInspectorExecute(Sender: TObject);
    procedure tbtAlignmentLeftClick(Sender: TObject);
    procedure actShowBookmarksExecute(Sender: TObject);
    procedure actToggleBookmarkExecute(Sender: TObject);
    procedure actShowAllEventsExecute(Sender: TObject);
    procedure actShowAllFormulasExecute(Sender: TObject);
  private
    FTempFormulas: TrmFMLFormulas;
    FContext: TrmDsgnAreaType;
    FContextMask: Integer;

    procedure UpdateGridChecks;
    procedure PrepareContextButtons;
    function  GetComponentDesigner: IrmDsgnComponent;
    function  MakeSetParamValue(SetItemID: String; AddOper: Boolean): String;
    procedure UpdateAlignmentState;
    procedure UpdatePositionAlignmentState;
    procedure UpdateFontState;
    procedure UpdateFormulaState;
    procedure UpdateZoomState;
    procedure UpdateASCIIState;
    procedure ChangeContext(AContext: TrmDsgnAreaType);

    procedure DMSelectedObjectListChanged(var Message: TrmMessageRec); message mSelectedObjectListChanged;
    procedure DMObjectPropertyChanged(var Message: TrmMessageRec); message mObjectPropertyChanged;
    procedure DMDesignContextChanged(var Message: TrmMessageRec); message mDesignContextChanged;

  protected
    property  ComponentDesigner: IrmDsgnComponent read GetComponentDesigner;
    procedure DoUpdateState; override;
    procedure GetListeningMessages(const AMessages: TList); override;

  public
    procedure Initialize; override;
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    function  InContext(AFlag: Integer): Boolean;
  end;


implementation

{$R *.dfm}

{ TrmTBDsgnComponent }


procedure TrmTBDsgnComponent.PrepareContextButtons;
var
  Vis: Boolean;

  procedure CheckChildrenFor(AOwner: TComponent);
  var
    i: Integer;
    C: TComponent;
  begin
    for i := 0 to AOwner.ComponentCount - 1 do
    begin
      C := AOwner.Components[i];
      if C.Tag <> 0 then
      begin
        Vis := InContext(C.Tag);

        if C is TControl then
          TControl(C).Enabled := Vis

        else if C is TAction then
          TAction(C).Enabled := Vis

        else if C is TMenuItem then
          TMenuItem(C).Enabled := Vis;
      end;
    end;
  end;


begin
  // Mask bits:
  //    Library         000001
  //    PrintForm       000010
  //    InputForm       000100
  //    No Library      001000
  //    Advanced Mode   010000
  //    ASCIIForm       100000

  FContextMask := 0;

  if Assigned(ComponentDesigner.DesigningObject) then
    if FContext = rmDATInputForm then
      FContextMask := 4
    else if FContext = rmDATPrintForm then
      FContextMask := 2
    else if FContext = rmDATASCIIForm then
      FContextMask := 32
    else if FContext = rmDATXMLForm then
      FContextMask := 64
    else if ComponentDesigner.LibComponentDesigninig then
      FContextMask := 38;

  CheckChildrenFor(Self); // Disable inactive children

  if FContext = rmDATInputForm then
    cbFontName.Items.Assign(Screen.Fonts)
  else
    cbFontName.Items.Assign(rwPrinter.Fonts);

  tbComponents.PrepareComponentPallet;
end;

procedure TrmTBDsgnComponent.ChangeContext(AContext: TrmDsgnAreaType);
begin
  FContext := AContext;
  PrepareContextButtons;
  UpdateState;
end;

procedure TrmTBDsgnComponent.Initialize;
begin
  inherited;
  UpdateGridChecks;
end;

procedure TrmTBDsgnComponent.miShowPaperGridClick(Sender: TObject);
begin
  DesignRMShowPaperGrid := not DesignRMShowPaperGrid;
  ComponentDesigner.RealObject.Refresh;
  UpdateGridChecks;
end;

procedure TrmTBDsgnComponent.miShowTableGridClick(Sender: TObject);
begin
  DesignRMShowTableGrid := not DesignRMShowTableGrid;
  ComponentDesigner.RealObject.Refresh;
  UpdateGridChecks;
end;

procedure TrmTBDsgnComponent.UpdateGridChecks;
begin
  miShowPaperGrid.Checked := DesignRMShowPaperGrid;
  miShowTableGrid.Checked := DesignRMShowTableGrid;
  miExpertMode.Checked    := DesignRMAdvancedMode;
end;

procedure TrmTBDsgnComponent.DoUpdateState;
begin
  UpdateAlignmentState;
  UpdatePositionAlignmentState;
  UpdateFontState;
  UpdateASCIIState;
  UpdateFormulaState;
  UpdateZoomState;
end;

procedure TrmTBDsgnComponent.actCompileExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbCompile, 0, 0);
end;

procedure TrmTBDsgnComponent.actCopyExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbCopy, 0, 0);
end;

function TrmTBDsgnComponent.GetComponentDesigner: IrmDsgnComponent;
begin
  Result := Owner as IrmDsgnComponent;
end;

procedure TrmTBDsgnComponent.actOpenExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbOpen, 0, 0);
end;

procedure TrmTBDsgnComponent.actSaveExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbSave, 0, 0);
end;

procedure TrmTBDsgnComponent.actCutExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbCut, 0, 0);
end;

procedure TrmTBDsgnComponent.actPasteExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbPaste, 0, 0);
end;

procedure TrmTBDsgnComponent.actDeleteExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbDelete, 0, 0);
end;

procedure TrmTBDsgnComponent.btnAlignLeftClick(Sender: TObject);
var
  Btn: TToolButton;
  Al: TAlign;
begin
  if not UpdatingState then
  begin
    Al := alNone;

    Btn := TToolButton(Sender);
    if Btn.Down then
    begin
      if Btn = btnAlignTop then
        Al := alTop
      else if Btn = btnAlignBottom then
        Al := alBottom
      else if Btn = btnAlignLeft then
        Al := alLeft
      else if Btn = btnAlignRight then
        Al := alRight
      else if Btn = btnAlignClient then
        Al := alClient;
    end;

    SendApartmentMessage(Self, mtbChangeObjectProperty, 'Align', Al);
  end;
end;

procedure TrmTBDsgnComponent.UpdateAlignmentState;
var
  SelObject: IrmDesignObject;
  SelObjects: TrmListOfObjects;

  procedure InitialStateTBAlignment;
  begin
    btnAlignLeft.Enabled := False;
    btnAlignLeft.Down := False;
    btnAlignRight.Enabled := False;
    btnAlignRight.Down := False;
    btnAlignTop.Enabled := False;
    btnAlignTop.Down := False;
    btnAlignBottom.Enabled := False;
    btnAlignBottom.Down := False;
    btnAlignClient.Enabled := False;
    btnAlignClient.Down := False;
  end;

  procedure TBAlignment;
  var
    fl: Boolean;
    AlTlb: TToolButton;
  begin
    fl := SelObject.PropertyExists('Align');
    btnAlignLeft.Enabled := fl;
    btnAlignRight.Enabled := fl;
    btnAlignTop.Enabled := fl;
    btnAlignBottom.Enabled := fl;
    btnAlignClient.Enabled := fl;

    if fl then
    begin
      case TAlign(Integer(SelObject.GetPropertyValue('Align'))) of
        alTop:    AlTlb := btnAlignTop;
        alBottom: AlTlb := btnAlignBottom;
        alLeft:   AlTlb := btnAlignLeft;
        alRight:  AlTlb := btnAlignRight;
        alClient: AlTlb := btnAlignClient;
      else
        AlTlb := nil;
      end;

      if Assigned(AlTlb) then
        AlTlb.Down := True;
    end;
  end;

begin
  inherited;

  SelObjects := ComponentDesigner.GetSelectedObjects;

  InitialStateTBAlignment;

  if Length(SelObjects) = 1 then
  begin
    SelObject := SelObjects[0];
    TBAlignment;
  end;
end;

procedure TrmTBDsgnComponent.edASCIISizeChange(Sender: TObject);
begin
  if not UpdatingState then
    SendApartmentMessage(Self, mtbChangeObjectProperty, 'Size', edASCIISize.Value);
end;

procedure TrmTBDsgnComponent.cbFontNameChange(Sender: TObject);
begin
  if not UpdatingState then
    if cbFontName.ItemIndex <> -1 then
      SendApartmentMessage(Self, mtbChangeObjectProperty, 'Font.Name', cbFontName.Items[cbFontName.ItemIndex]);
end;

procedure TrmTBDsgnComponent.cbFontSizeChange(Sender: TObject);
var
  s: Double;
begin
  if not UpdatingState then
    if TryStrToFloat(cbFontSize.Text, s) then
    begin
      if ComponentDesigner.InputFormDesigninig then
        s := Round(s)
      else
        s := RoundTo(ConvertUnit(s, utScreenPixels, utLogicalPixels), - 1);

      SendApartmentMessage(Self, mtbChangeObjectProperty, 'Font.Size', s);
    end;
end;

procedure TrmTBDsgnComponent.tbtFontBoldClick(Sender: TObject);
begin
  if not UpdatingState then
    SendApartmentMessage(Self, mtbChangeObjectProperty, 'Font.Style', MakeSetParamValue('fsBold', TToolButton(Sender).Down));
end;

function TrmTBDsgnComponent.MakeSetParamValue(SetItemID: String; AddOper: Boolean): String;
begin
  if AddOper then
    Result := '+'
  else
    Result := '-';
  Result := Result + '[' + SetItemID + ']';
end;

procedure TrmTBDsgnComponent.tbtFontItalicClick(Sender: TObject);
begin
  if not UpdatingState then
    SendApartmentMessage(Self, mtbChangeObjectProperty, 'Font.Style', MakeSetParamValue('fsItalic', TToolButton(Sender).Down));
end;

procedure TrmTBDsgnComponent.tbtFontUnderlineClick(Sender: TObject);
begin
  if not UpdatingState then
    SendApartmentMessage(Self, mtbChangeObjectProperty, 'Font.Style', MakeSetParamValue('fsUnderline', TToolButton(Sender).Down));
end;

procedure TrmTBDsgnComponent.tbtTextAlignLeftClick(Sender: TObject);
var
  tAl: TAlignment;
begin
  if not UpdatingState then
  begin
    if tbtTextAlignCenter.Down then
      tAl := taCenter
    else if tbtTextAlignRight.Down then
      tAl := taRightJustify
    else
      tAl := taLeftJustify;

    SendApartmentMessage(Self, mtbChangeObjectProperty, 'Alignment', tAl);
  end;
end;

procedure TrmTBDsgnComponent.tbtTopLayoutClick(Sender: TObject);
var
  tl: TTextLayout;
begin
  if not UpdatingState then
  begin
    if tbtBottomLayout.Down then
      tl := tlBottom
    else if tbtCenterLayout.Down then
      tl := tlCenter
    else
      tl := tlTop;

    SendApartmentMessage(Self, mtbChangeObjectProperty, 'Layout', tl);
  end;
end;

procedure TrmTBDsgnComponent.tbtFontColorClick(Sender: TObject);
begin
  if not UpdatingState then
    SendApartmentMessage(Self, mtbChangeObjectProperty, 'Font.Color', Integer(tbtFontColor.ActiveColor));
end;

procedure TrmTBDsgnComponent.tbtBackgroundColorClick(Sender: TObject);
begin
  if not UpdatingState then
    SendApartmentMessage(Self, mtbChangeObjectProperty, 'Color', Integer(tbtBackgroundColor.ActiveColor));
end;

procedure TrmTBDsgnComponent.UpdateFontState;
var
  i: Integer;
  SelObject: IrmDesignObject;
  SelObjects: TrmListOfObjects;
  flFontEmpty: Boolean;
  flTextAlignEmpty: Boolean;
  flTextLayoutEmpty: Boolean;
  flColorEmpty: Boolean;

  procedure InitialStateTBTextStyle;
  begin
    flFontEmpty := True;
    flTextAlignEmpty := True;
    flTextLayoutEmpty := True;
    flColorEmpty := True;

    cbFontName.Enabled := True;
    cbFontSize.Enabled := True;
    tbtFontBold.Enabled := True;
    tbtFontItalic.Enabled := True;
    tbtFontUnderline.Enabled := True;
    tbtTextAlignLeft.Enabled := True;
    tbtTextAlignCenter.Enabled := True;
    tbtTextAlignRight.Enabled := True;
    tbtTopLayout.Enabled := True;
    tbtBottomLayout.Enabled := True;
    tbtCenterLayout.Enabled := True;
    tbtFontColor.Enabled := True;
    tbtBackgroundColor.Enabled := True;

    cbFontName.ItemIndex := -1;
    cbFontSize.Text := '';
    tbtFontBold.Down := False;
    tbtFontItalic.Down := False;
    tbtFontUnderline.Down := False;
    tbtTextAlignLeft.Down := False;
    tbtTextAlignCenter.Down := False;
    tbtTextAlignRight.Down := False;
    tbtTopLayout.Down := False;
    tbtBottomLayout.Down := False;
    tbtCenterLayout.Down := False;
  end;

  procedure CheckStateTBTextStyle;
  begin
    if flFontEmpty then
    begin
      cbFontName.Enabled := False;
      cbFontSize.Enabled := False;
      tbtFontBold.Enabled := False;
      tbtFontItalic.Enabled := False;
      tbtFontUnderline.Enabled := False;
      tbtFontColor.Enabled := False;
    end;

    if flTextAlignEmpty then
    begin
      tbtTextAlignLeft.Enabled := False;
      tbtTextAlignRight.Enabled := False;
      tbtTextAlignCenter.Enabled := False;
    end;

    if flTextLayoutEmpty then
    begin
      tbtTopLayout.Enabled := False;
      tbtBottomLayout.Enabled := False;
      tbtCenterLayout.Enabled := False;
    end;

    if flColorEmpty then
      tbtBackgroundColor.Enabled := False;
  end;

  procedure TBTextStyle;
  var
    fl: Boolean;
    AlTlb: TToolButton;
    F: TrwFont;

    function GetFontSize(AFontSize: Integer): String;
    var
      fs: Extended;
    begin
      if ComponentDesigner.InputFormDesigninig then
        fs := AFontSize
      else
      begin
        fs := ConvertUnit(AFontSize, utLogicalPixels, utScreenPixels);
        fs := Trunc((fs + 0.09) * 10) / 10;
      end;

      Result := FloatToStr(fs);
    end;

  begin
    if cbFontName.Enabled then
    begin
      fl := SelObject.PropertyExists('Font');
      cbFontName.Enabled := fl;
      cbFontSize.Enabled := fl;
      tbtFontBold.Enabled := fl;
      tbtFontItalic.Enabled := fl;
      tbtFontUnderline.Enabled := fl;
      tbtFontColor.Enabled := fl;

      if fl then
      begin
        F := TrwFont(Pointer(Integer(SelObject.GetPropertyValue('Font'))));
        if flFontEmpty then
        begin
          cbFontName.ItemIndex := cbFontName.Items.IndexOf(F.Name);
          cbFontSize.Text := GetFontSize(F.Size);
          tbtFontBold.Down := fsBold in F.Style;
          tbtFontItalic.Down := fsItalic in F.Style;
          tbtFontUnderline.Down := fsUnderline in F.Style;

          tbtFontColor.TargetColor := F.Color;
        end
        else
        begin
          if cbFontName.ItemIndex <> cbFontName.Items.IndexOf(F.Name) then
            cbFontName.ItemIndex := -1;

          if cbFontSize.Text <> GetFontSize(F.Size) then
            cbFontSize.Text := '';

          if tbtFontBold.Down <> (fsBold in F.Style) then
            tbtFontBold.Down := False;
          if tbtFontItalic.Down <> (fsItalic in F.Style) then
            tbtFontItalic.Down := False;
          if tbtFontUnderline.Down <> (fsUnderline in F.Style) then
            tbtFontUnderline.Down := False;
        end;

        flFontEmpty := False;
      end
      else
      begin
        cbFontName.ItemIndex := -1;
        cbFontSize.Text := '';
        tbtFontBold.Down := False;
        tbtFontItalic.Down := False;
        tbtFontUnderline.Down := False;
      end;
    end;


    if tbtTextAlignLeft.Enabled then
    begin
      fl := SelObject.PropertyExists('Alignment');
      tbtTextAlignLeft.Enabled := fl;
      tbtTextAlignRight.Enabled := fl;
      tbtTextAlignCenter.Enabled := fl;

      if fl then
      begin
        case TAlignment(Integer(SelObject.GetPropertyValue('Alignment'))) of
          taLeftJustify:  AlTlb := tbtTextAlignLeft;
          taRightJustify: AlTlb := tbtTextAlignRight;
          taCenter:       AlTlb := tbtTextAlignCenter;
        else
          AlTlb := nil;
        end;

        if flTextAlignEmpty then
          AlTlb.Down := True
        else
          if not AlTlb.Down then
          begin
            tbtTextAlignLeft.Down := False;
            tbtTextAlignCenter.Down := False;
            tbtTextAlignRight.Down := False;
          end;

        flTextAlignEmpty := False;
      end
      else
      begin
        tbtTextAlignLeft.Down := False;
        tbtTextAlignCenter.Down := False;
        tbtTextAlignRight.Down := False;
      end;
    end;

    if tbtTopLayout.Enabled then
    begin
      fl := SelObject.PropertyExists('Layout');
      tbtTopLayout.Enabled := fl;
      tbtBottomLayout.Enabled := fl;
      tbtCenterLayout.Enabled := fl;

      if fl then
      begin
        case TTextLayout(Integer(SelObject.GetPropertyValue('Layout'))) of
          tlTop:    AlTlb := tbtTopLayout;
          tlBottom: AlTlb := tbtBottomLayout;
          tlCenter: AlTlb := tbtCenterLayout;
        else
          AlTlb := nil;
        end;

        if flTextLayoutEmpty then
          AlTlb.Down := True
        else
          if not AlTlb.Down then
          begin
            tbtTopLayout.Down := False;
            tbtBottomLayout.Down := False;
            tbtCenterLayout.Down := False;
          end;

        flTextLayoutEmpty := False;
      end
      else
      begin
        tbtTopLayout.Down := False;
        tbtBottomLayout.Down := False;
        tbtCenterLayout.Down := False;
      end;
    end;

    if tbtBackgroundColor.Enabled then
    begin
      fl := SelObject.PropertyExists('Color');
      tbtBackgroundColor.Enabled := fl;
      if fl then
      begin
        if flColorEmpty then
           tbtBackgroundColor.TargetColor := TColor(Integer(SelObject.GetPropertyValue('Color')));
        flColorEmpty := False;
      end;
    end;
  end;

begin
  inherited;

  SelObjects := ComponentDesigner.GetSelectedObjects;

  InitialStateTBTextStyle;

  for i := Low(SelObjects) to High(SelObjects) do
  begin
    SelObject := SelObjects[i];
    TBTextStyle;
  end;

  CheckStateTBTextStyle;
end;

procedure TrmTBDsgnComponent.btnAggrFunctClick(Sender: TObject);
begin
  miAggrFunctSUM.Click;
end;

procedure TrmTBDsgnComponent.AfterConstruction;
begin
  inherited;
  pnlFormula.ReadOnly := True;
end;

procedure TrmTBDsgnComponent.BeforeDestruction;
begin
  inherited;
  FreeAndNil(FTempFormulas);
end;

procedure TrmTBDsgnComponent.UpdateFormulaState;
var
  SelObject: IrmDesignObject;
  SelObjects: TrmListOfObjects;

  procedure TBFormula;
  var
    Fml: IrmFMLFormula;
    Val: Variant;
  begin
    lFormulaCaption.Enabled := Assigned(SelObject);
    btnFormulaEditor.Enabled := Assigned(SelObject);
    btnAggrFunct.Enabled := Assigned(SelObject);

    FreeAndNil(FTempFormulas);

    if Assigned(SelObject) then
    begin
      Fml := SelObject.GetFormulas.GetFormulaByAction(fatSetProperty, 'Value');
      if not Assigned(Fml) then
      begin
        FTempFormulas := TrmFMLFormulas.Create(ComponentDesigner.DesigningObject.RealObject as TrwComponent, TrmFMLFormula);
        Fml := FTempFormulas.AddFormula(fatSetProperty, 'Value');

        if Supports(SelObject, IrmQueryAwareObject) then
          Val := (SelObject as IrmQueryAwareObject).GetDataFieldInfo.RootPath
        else
          Val := '';

        if Val = '' then
          Fml.AddItem(fitConst, SelObject.GetPropertyValue('Value'))
        else
          Fml.AddItem(fitField, Val);
      end;

      pnlFormula.Expression := TrmFMLFormula(Fml.RealObject).Content;
    end
    else
      pnlFormula.Expression := nil;
  end;

begin
  inherited;

  SelObjects := ComponentDesigner.GetSelectedObjects;

  if Length(SelObjects) = 1 then
  begin
    SelObject := SelObjects[0];
    if not SelObject.PropertyExists('Value') then
      SelObject := nil;
  end
  else
    SelObject := nil;

  TBFormula;
end;

procedure TrmTBDsgnComponent.actObjPropsExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbShowPropertyEditor, 0, 0);
end;

procedure TrmTBDsgnComponent.actQBExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbShowQueryBuilder, 0, 0);
end;

procedure TrmTBDsgnComponent.btnInpFormParamsClick(Sender: TObject);
begin
  miShowInputForm.Click;
end;

procedure TrmTBDsgnComponent.miSetParametersFileClick(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbSetParamFileName, '', 0);
end;

procedure TrmTBDsgnComponent.actZoomInExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbZoomChange, -1, 0);
  UpdateZoomState;
end;

procedure TrmTBDsgnComponent.UpdateZoomState;
var
  bEnabled: Boolean;
begin
  bEnabled := ComponentDesigner.PrintFormDesigninig or ComponentDesigner.ASCIIFormDesigninig or ComponentDesigner.XMLFormDesigninig;
  btnZoomIn.Enabled := bEnabled;
  btnZoomOut.Enabled := bEnabled;
  cbZoom.Enabled := bEnabled;
  cbZoom.Text := '100%';

  if bEnabled then
  begin
    cbZoom.Text := FloatToStr(RoundTo(ComponentDesigner.GetZoomFactor * 100 * ComponentDesigner.GetObjectCanvasDPI / cScreenCanvasRes, -2)) + '%';
    if cbZoom.Focused then
      cbZoom.Perform(WM_KILLFOCUS, cbZoom.Handle, 0);
  end;
end;

procedure TrmTBDsgnComponent.cbZoomExit(Sender: TObject);
var
  h: String;
  prc: Real;
  kf: Extended;
begin
  try
    h := Trim(cbZoom.Text);
    if (h <> '') and (h[Length(h)] = '%') then
      Delete(h, Length(h), 1);

    try
      if h = '' then
        Exit
      else
      begin
        prc := RoundTo(StrToFloat(h), -2);
        kf := prc / (100 * ComponentDesigner.GetObjectCanvasDPI / cScreenCanvasRes);
        cbZoom.Text := FLoatToStr(prc) + '%';
        SendApartmentMessage(Self, mtbZoomChange, kf, 0);
      end;
    except
    end;

  finally
    UpdateZoomState;
  end;
end;

procedure TrmTBDsgnComponent.cbZoomKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    cbZoom.OnExit(cbZoom);
end;

procedure TrmTBDsgnComponent.cbZoomSelect(Sender: TObject);
begin
  cbZoom.OnExit(cbZoom);
end;

procedure TrmTBDsgnComponent.actZoomOutExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbZoomChange, -2, 0);
  UpdateZoomState;
end;

procedure TrmTBDsgnComponent.UpdateASCIIState;
var
  SelObject: IrmDesignObject;
  SelObjects: TrmListOfObjects;

  procedure InitialStateTBASCIIProps;
  begin
    lASCIISize.Enabled := False;
    edASCIISize.Value := 0;
    edASCIISize.Enabled := False;
  end;

  procedure TBASCIIProps;
  var
    fl: Boolean;
  begin
    if not tlbASCIIProps.Visible then
      Exit;

    fl := SelObject.PropertyExists('Size') and
         (TrmASCIIResultType(Integer((SelObject as IrmASCIIItem).GetASCIIForm.GetPropertyValue('ResultType'))) = rmARTFixedLength);

    lASCIISize.Enabled := fl;
    edASCIISize.Enabled := fl;

    if fl then
      edASCIISize.Value := Integer(SelObject.GetPropertyValue('Size'));
  end;

begin
  inherited;

  SelObjects := ComponentDesigner.GetSelectedObjects;

  InitialStateTBASCIIProps;

  if Length(SelObjects) = 1 then
  begin
    SelObject := SelObjects[0];
    TBASCIIProps;
  end;
end;

procedure TrmTBDsgnComponent.actInpFormParamsExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbSetDefaultParams, 0, 0);
end;

function TrmTBDsgnComponent.InContext(AFlag: Integer): Boolean;
begin
  Result := (AFlag and FContextMask) <> 0;
  if ComponentDesigner.LibComponentDesigninig then
    Result := Result and ((AFlag and 8) = 0)
  else
    Result := Result and ((AFlag and 1) = 0);

  if not DesignRMAdvancedMode then
    Result := Result and ((AFlag and 16) = 0)
  else
    Result := Result or ((AFlag and 16) <> 0) and ((AFlag and 39) = 0);
end;

procedure TrmTBDsgnComponent.actExpertModeExecute(Sender: TObject);
begin
  DesignRMAdvancedMode := not DesignRMAdvancedMode;
  UpdateGridChecks;
  ChangeContext(FContext);
end;

procedure TrmTBDsgnComponent.actAggrFunctSUMExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbShowAggrFunctEditor, 'SUM', 0);
end;

procedure TrmTBDsgnComponent.actAggrFunctSUMValExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbShowAggrFunctEditor, 'SUMVAL', 0);
end;

procedure TrmTBDsgnComponent.actAggrFunctCountExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbShowAggrFunctEditor, 'COUNT', 0);
end;

procedure TrmTBDsgnComponent.actFormulaEditorExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbShowFormulaEditor, 0, 0);
end;

procedure TrmTBDsgnComponent.actObjectTreeExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbShowObjectsTree, 0, 0);
end;

procedure TrmTBDsgnComponent.DMSelectedObjectListChanged(var Message: TrmMessageRec);
begin
  inherited;
  UpdateState;
end;

procedure TrmTBDsgnComponent.DMDesignContextChanged(var Message: TrmMessageRec);
begin
  ChangeContext(TrmDsgnAreaType(Integer(Message.Parameter1)));
end;

procedure TrmTBDsgnComponent.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mSelectedObjectListChanged));
  AMessages.Add(Pointer(mDesignContextChanged));
  AMessages.Add(Pointer(mObjectPropertyChanged));
end;

procedure TrmTBDsgnComponent.actEventsEditorExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbShowEventsEditor, 0, 0);
end;

procedure TrmTBDsgnComponent.actObjectInspectorExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbShowObjectInspector, 0, 0);
end;

procedure TrmTBDsgnComponent.DMObjectPropertyChanged(var Message: TrmMessageRec);
begin
  inherited;
  UpdateState;
end;

procedure TrmTBDsgnComponent.tbtAlignmentLeftClick(Sender: TObject);
var
  Btn: TToolButton;
  Al: TPositionAlign;
begin
  if not UpdatingState then
  begin
    Al := alPositionNone;
    Btn := TToolButton(Sender);
    if Btn = tbtAlignmentLeft then
      Al := alPositionLeft
    else if Btn = tbtAlignmentTop then
      Al := alPositionTop
    else if Btn = tbtAlignmentBottom  then
      Al := alPositionBottom
    else if Btn = tbtAlignmentRight then
      Al := alPositionRight
    else if Btn = tblAlignCenter then
      Al := alPositionCenter
    else if Btn = tblAlignMiddle then
      Al := alPositionMiddle
    else if Btn = tblAlignByContainerCenter then
      Al := alPositionContainerCenter
    else if Btn = tblAlignByContainerMiddle then
      Al := alPositionContainerMiddle
    else if Btn = tblVerticalSpacesEqually then
      Al := alPositionVerticalSpaces
    else if Btn = tblHorisontalSpacesEqually then
      Al := alPositionHorisontalSpaces;
    if AL <> AlPositionNone then
      SendApartmentMessage(Self, mtbAlignPositions, 'AlignPositions', Al);
  end;
end;

procedure TrmTBDsgnComponent.UpdatePositionAlignmentState;
var
  SelObjects: TrmListOfObjects;

  procedure InitialStateTBPositionsAlignment;
  begin
    tbtAlignmentLeft.Enabled := false;
    tbtAlignmentTop.Enabled := false;
    tbtAlignmentBottom.Enabled := false;
    tbtAlignmentRight.Enabled := false;
    tblAlignCenter.Enabled := false;
    tblAlignMiddle.Enabled := false;
    tblAlignByContainerCenter.Enabled := false;
    tblAlignByContainerMiddle.Enabled := false;
    tblVerticalSpacesEqually.Enabled := false;
    tblHorisontalSpacesEqually.Enabled := false;
  end;

begin
  SelObjects := ComponentDesigner.GetSelectedObjects;

  InitialStateTBPositionsAlignment;

  if Debugging or (Length(SelObjects) = 0) then
    Exit;
  if Supports(SelObjects[0], IrmTableCell) then
    Exit;

  if not SelObjects[0].PropertyExists('Top') then
    Exit;

  if Length(SelObjects) >= 1 then
  begin
    tblAlignByContainerCenter.Enabled := true;
    tblAlignByContainerMiddle.Enabled := true;
  end;

  if Length(SelObjects) > 1 then
  begin
    tbtAlignmentLeft.Enabled := true;
    tbtAlignmentTop.Enabled := true;
    tbtAlignmentBottom.Enabled := true;
    tbtAlignmentRight.Enabled := true;
    tblAlignCenter.Enabled := true;
    tblAlignMiddle.Enabled := true;
  end;

  if Length(SelObjects) >= 3 then
  begin
    tblVerticalSpacesEqually.Enabled := true;
    tblHorisontalSpacesEqually.Enabled := true;
  end;
end;

procedure TrmTBDsgnComponent.actShowBookmarksExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbShowBookmarks, 0, 0);
end;

procedure TrmTBDsgnComponent.actToggleBookmarkExecute(Sender: TObject);
var
  SelObj: TrmListOfObjects;
begin
  if not UpdatingState then
  begin
    SelObj := RMDesigner.GetComponentDesigner.GetSelectedObjects;
    if Length(SelObj) >= 1 then
    begin
      if ((SelObj[0].ObjectType = rwDOTInternal) and Assigned(SelObj[0].GetObjectOwner)) or (TrwDsgnLockState(Integer(SelObj[0].GetPropertyValue('DsgnLockState'))) = rwLSHidden) then
        Exit;
      PostApartmentMessage(Self, mtbShowBookmarks, 0, 0);
      PostApartmentMessage(Self, mBookmarkObject, Integer(Pointer(SelObj[0])), 0);
    end;
  end;
end;

procedure TrmTBDsgnComponent.actShowAllEventsExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbShowAllEventsCode, 0, 0);
end;

procedure TrmTBDsgnComponent.actShowAllFormulasExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbShowAllFormulasCode, 0, 0);
end;

end.

