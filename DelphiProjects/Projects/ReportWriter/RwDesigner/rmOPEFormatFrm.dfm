inherited rmOPEFormat: TrmOPEFormat
  Width = 200
  Height = 21
  AutoSize = True
  OnResize = FrameResize
  object lPropName: TLabel
    Left = 0
    Top = 3
    Width = 32
    Height = 13
    Caption = 'Format'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object cbFormat: TComboBox
    Left = 55
    Top = 0
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    OnChange = cbFormatChange
  end
end
