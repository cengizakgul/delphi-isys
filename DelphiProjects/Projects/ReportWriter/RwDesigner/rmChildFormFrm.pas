unit rmChildFormFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, rmDsgnTypes;

type
  TrmChildForm = class(TForm, IrmMessageReceiver)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
  protected
    procedure SetParent(AParent: TWinControl); override;

  //interface
  protected
    function  VCLObject: TObject;
    function  Apartment: IrmDsgnApartment;
    procedure GetListeningMessages(const AMessages: TList); virtual;

  public
  end;

implementation

{$R *.dfm}

uses rwDsgnUtils;

{ TrmChildForm }

procedure TrmChildForm.SetParent(AParent: TWinControl);
begin
  if csDestroying in Self.ComponentState then
    Exit;

  if Assigned(AParent) then
    Windows.SetParent(Handle, AParent.Handle)
  else
    Windows.SetParent(Handle, 0);
end;

procedure TrmChildForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SetParent(nil);
end;

function TrmChildForm.Apartment: IrmDsgnApartment;
begin
  Result := GetComponentAppartment(Self);
end;

procedure TrmChildForm.GetListeningMessages(const AMessages: TList);
begin
end;

function TrmChildForm.VCLObject: TObject;
begin
  Result := Self;
end;

end.
