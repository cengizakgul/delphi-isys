inherited rmTrmXMLFormProp: TrmTrmXMLFormProp
  Width = 316
  Height = 370
  inherited pcCategories: TPageControl
    Width = 316
    Height = 370
    inherited tsBasic: TTabSheet
      inline frmDefaultFileName: TrmOPEStringSingleLine
        Left = 8
        Top = 88
        Width = 290
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 2
        inherited lPropName: TLabel
          Width = 84
          Caption = 'Default File Name'
        end
      end
      inline frmNameSpace: TrmOPEStringSingleLine
        Left = 8
        Top = 119
        Width = 290
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 3
        inherited lPropName: TLabel
          Width = 62
          Caption = 'Name Space'
        end
      end
      inline pnlSchemas: TrmOPEXMLFormSchemas
        Left = 8
        Top = 164
        Width = 291
        Height = 174
        AutoScroll = False
        TabOrder = 4
        inherited lvSchemas: TListView
          Width = 291
        end
      end
    end
    inherited tcAppearance: TTabSheet
      inherited frmBlockParentIfEmpty: TrmOPEBoolean
        Width = 267
        Visible = False
        inherited chbProp: TCheckBox
          Width = 267
          Caption = 'Do not produce XML result if no data on this form'
        end
      end
      inline frmProduceIf: TrmOPEBlockingControl [1]
        Left = 8
        Top = 37
        Width = 292
        Height = 71
        AutoScroll = False
        TabOrder = 1
        inherited lCaption: TLabel
          Width = 292
          Caption = 'Produce Data If'
        end
        inherited frmExpression: TrmFMLExprPanel
          Width = 292
          Height = 55
          inherited pnlCanvas: TPanel
            Width = 292
            Height = 55
          end
        end
      end
      inherited frmInactive: TrmOPEBoolean
        TabOrder = 2
      end
    end
  end
end
