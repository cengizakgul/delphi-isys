object rwASCIIPrintOrder: TrwASCIIPrintOrder
  Left = 356
  Top = 141
  Width = 553
  Height = 461
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'ASCII Print Order'
  Color = clBtnFace
  Constraints.MinHeight = 315
  Constraints.MinWidth = 540
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    545
    434)
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel
    Left = 4
    Top = 250
    Width = 100
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Available Objects'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnAdd: TSpeedButton
    Left = 408
    Top = 238
    Width = 23
    Height = 22
    Hint = 'Add'
    Anchors = [akRight, akBottom]
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDDDDDDDDDDDDDDDD77777DDDDDDDDDD444447DDDDDDDDDDCCCC47D
      DDDDDDDDDCCCC47DDDDDDDDDDCCCC47DDDDDDDDDDCCCC47DDDDDDDDDDCCCC477
      7DDDDDDCCCCCCCCCDDDDDDDDCCCCCCCDDDDDDDDDDCCCCCDDDDDDDDDDDDCCCDDD
      DDDDDDDDDDDCDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
    OnClick = btnAddClick
  end
  object btnRemove: TSpeedButton
    Left = 434
    Top = 238
    Width = 23
    Height = 22
    Hint = 'Remove'
    Anchors = [akRight, akBottom]
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDD7DDDDDDDDDDDDDD477DDDDDDDDDDDDCC477DDDDDDDDDDCCCC477
      DDDDDDDDCCCCCC477DDDDDDCCCCCC444DDDDDDDDDCCCC47DDDDDDDDDDCCCC47D
      DDDDDDDDDCCCC47DDDDDDDDDDCCCC47DDDDDDDDDDCCCC47DDDDDDDDDDCCCC4DD
      DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
    OnClick = btnRemoveClick
  end
  object tcLines: TTabControl
    Left = 4
    Top = 3
    Width = 454
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    Style = tsFlatButtons
    TabOrder = 0
    OnChange = tcLinesChange
  end
  object lvASCIIObj: TListView
    Left = 4
    Top = 31
    Width = 454
    Height = 199
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = '#'
        MaxWidth = 25
        MinWidth = 25
        Width = 25
      end
      item
        Caption = 'Object'
        Width = 125
      end
      item
        Caption = 'Class'
        Width = 125
      end
      item
        Caption = 'Indent'
        MaxWidth = 45
        MinWidth = 45
        Width = 45
      end
      item
        Caption = 'Length'
        MaxWidth = 45
        MinWidth = 45
        Width = 45
      end
      item
        Caption = 'Start'
        MaxWidth = 35
        MinWidth = 35
        Width = 35
      end
      item
        Caption = 'End'
        MaxWidth = 35
        MinWidth = 35
        Width = 35
      end>
    DragMode = dmAutomatic
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 1
    ViewStyle = vsReport
    OnChange = lvASCIIObjChange
    OnEndDrag = lvASCIIObjEndDrag
    OnDragOver = lvASCIIObjDragOver
    OnKeyDown = lvASCIIObjKeyDown
  end
  object lvAvailObj: TListView
    Left = 4
    Top = 267
    Width = 454
    Height = 163
    Anchors = [akLeft, akRight, akBottom]
    Columns = <
      item
        Caption = 'Object'
        Width = 145
      end
      item
        Caption = 'Class'
        Width = 145
      end
      item
        Caption = 'Container'
        Width = 145
      end>
    DragMode = dmAutomatic
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 2
    ViewStyle = vsReport
    OnEndDrag = lvAvailObjEndDrag
    OnDragOver = lvAvailObjDragOver
  end
  object Panel1: TPanel
    Left = 460
    Top = 0
    Width = 85
    Height = 434
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 3
    DesignSize = (
      85
      434)
    object Label3: TLabel
      Left = 7
      Top = 27
      Width = 70
      Height = 13
      Caption = 'Sequence #'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 7
      Top = 71
      Width = 37
      Height = 13
      Caption = 'Indent'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 7
      Top = 117
      Width = 56
      Height = 13
      Caption = 'Data Size'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 7
      Top = 164
      Width = 34
      Height = 13
      Caption = 'Total:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lTotalLength: TLabel
      Left = 47
      Top = 164
      Width = 6
      Height = 13
      Caption = '0'
    end
    object edNbr: TwwDBSpinEdit
      Left = 7
      Top = 42
      Width = 70
      Height = 21
      Increment = 1.000000000000000000
      MaxValue = 100000.000000000000000000
      MinValue = 1.000000000000000000
      TabOrder = 0
      UnboundDataType = wwDefault
      OnExit = edNbrExit
      OnKeyPress = edNbrKeyPress
      AfterUpClick = edLengthAfterDownClick
      AfterDownClick = edLengthAfterDownClick
    end
    object edIndent: TwwDBSpinEdit
      Left = 7
      Top = 86
      Width = 70
      Height = 21
      Increment = 1.000000000000000000
      MaxValue = 100000.000000000000000000
      TabOrder = 1
      UnboundDataType = wwDefault
      OnExit = edIndentExit
      OnKeyPress = edNbrKeyPress
      AfterUpClick = edLengthAfterDownClick
      AfterDownClick = edLengthAfterDownClick
    end
    object edLength: TwwDBSpinEdit
      Left = 7
      Top = 133
      Width = 70
      Height = 21
      Increment = 1.000000000000000000
      MaxValue = 100000.000000000000000000
      TabOrder = 2
      UnboundDataType = wwDefault
      OnExit = edLengthExit
      OnKeyPress = edLengthKeyPress
      AfterUpClick = edLengthAfterDownClick
      AfterDownClick = edLengthAfterDownClick
    end
    object btnOK: TButton
      Left = 6
      Top = 372
      Width = 75
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 3
    end
    object btnCancel: TButton
      Left = 6
      Top = 404
      Width = 75
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 4
    end
  end
end
