unit rwWatchListFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ActnList, rwDebugInfo, rwParser, rwTypes, rwBasicClasses, rwUtils,
  rwVirtualMachine, rwBasicUtils, rwEngineTypes;

type
  TrwWatchList = class(TForm)
    lbWatch: TListBox;
    aclDesignerActions: TActionList;
    actAddWatch: TAction;
    actDelWatch: TAction;
    actEditWatch: TAction;
    actStepOver: TAction;
    actTraceInto: TAction;
    actRun: TAction;
    actTerminate: TAction;
    actPCode: TAction;
    actAddWatch2: TAction;
    actClose: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure actAddWatchExecute(Sender: TObject);
    procedure actDelWatchExecute(Sender: TObject);
    procedure actEditWatchExecute(Sender: TObject);
    procedure actStepOverExecute(Sender: TObject);
    procedure actTraceIntoExecute(Sender: TObject);
    procedure actTerminateExecute(Sender: TObject);
    procedure actRunExecute(Sender: TObject);
    procedure actPCodeExecute(Sender: TObject);
    procedure actAddWatch2Execute(Sender: TObject);
    procedure actCloseExecute(Sender: TObject);
  public
    procedure AddWatch;
    procedure Initialize;
  end;


  function  ShowWatch: TrwWatchList;
  procedure CloseWatch;
  function  IsWatchVisible: Boolean;
  procedure CompileWatches;


implementation

{$R *.DFM}

uses rwDesignerFrm;

var
  FWatchList: TrwWatchList = nil;


procedure CompileWatches;
var
  h1, lVarInfo, lCompPath: string;
  i: Integer;
  C: TrwComponent;
begin
  DebugInfo.WatchList.SetLengthCompiledCode(0);
  SetCompiledCode(Addr(DebugInfo.WatchList.CompiledCode), cVMLibraryFunctionsOffset);
  SetCompilingPointer(0);
  SetDebInf(nil);
  SetDebSymbInf(nil);

  for i := 0 to DebugInfo.WatchList.Count - 1 do
  begin
    h1 := DebugInfo.WatchList[i].Expression + '; end'#0;
    DebugInfo.WatchList[i].Address := -1;

    C := ReportDesigner.GetComponentByPath(DebugInfo.Identif);
    if not Assigned(C) then
      continue;

    if Length(DebugInfo.CurrentLibClass) > 0 then
    begin
      if C.IsLibComponent and C.IsInheritsFrom(DebugInfo.CurrentLibClass) then
        lCompPath := DebugInfo.CurrentLibClass
      else
        lCompPath := DebugInfo.CurrentLibClass + '.' + C.Name;
    end
    else
      lCompPath := DebugInfo.Identif;

    lVarInfo := ReportDesigner.GetDebSymbInfo(DebugInfo.PDebSymbInfo, lCompPath, DebugInfo.CurrentEvent);
    if Length(lVarInfo) = 0 then
      Continue;

    GetNextStrValue(lVarInfo, ';');
    GetNextStrValue(lVarInfo, ';');
    GetNextStrValue(lVarInfo, ';');
    GetNextStrValue(lVarInfo, ';');
    lVarInfo := GetNextStrValue(lVarInfo, ';');

    try
      DebugInfo.WatchList[i].Result := '';
      DebugInfo.WatchList[i].PointerType := '';
      DebugInfo.WatchList[i].Address := GetCompilingPointer + cVMDsgnWatchListOffset;
      CompileWatchList(h1, lVarInfo, TrwComponent(DebugInfo.CurrentComponent));

      with GetCodeInsightInfo do
        if VarType = rwvPointer then
          if Assigned(ClassInf) then
            DebugInfo.WatchList[i].PointerType := ClassInf.GetClassName
          else
            DebugInfo.WatchList[i].PointerType := 'Pointer'

        else if VarType = rwvUnknown then
          raise ErwException.Create('Incorrect watch expression');

    except
      on E: Exception do
      begin
        DebugInfo.WatchList.SetLengthCompiledCode(DebugInfo.WatchList[i].Address - cVMDsgnWatchListOffset);
        if E is ErwParserError then
          DebugInfo.WatchList[i].Result := ErwParserError(E).GeneralMessage
        else
          DebugInfo.WatchList[i].Result := E.Message;
        SetCompilingPointer(DebugInfo.WatchList[i].Address - cVMDsgnWatchListOffset);
        DebugInfo.WatchList[i].Address := -1;
      end;
    end;
  end;

  DebugInfo.WatchList.SetLengthCompiledCode(GetCompilingPointer);

  DebugInfo.WatchList.NeedCompileWatches := False;
end;


function IsWatchVisible: Boolean;
begin
  Result :=  Assigned(FWatchList) and FWatchList.Visible;
end;


function ShowWatch: TrwWatchList;
begin
  if not Assigned(FWatchList) then
  begin
    FWatchList := TrwWatchList.Create(nil);
    DebugInfo.WatchList.NeedCompileWatches := True;
    FWatchList.Initialize;
    FWatchList.Show;
  end
  else
  begin
    if not IsWatchVisible then
      FWatchList.Show;
    SetWindowPos(FWatchList.Handle, HWND_TOP, 0, 0, 0, 0,
      SWP_NOACTIVATE or SWP_NOSIZE or SWP_NOMOVE);
  end;

  Result := FWatchList;
end;


procedure CloseWatch;
begin
  if Assigned(FWatchList) then
  begin
    FWatchList.Free;
    FWatchList := nil;
  end;
end;


procedure TrwWatchList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;


procedure TrwWatchList.FormDestroy(Sender: TObject);
begin
  FWatchList := nil;
end;


procedure TrwWatchList.actAddWatchExecute(Sender: TObject);
begin
  AddWatch;
end;


procedure TrwWatchList.actDelWatchExecute(Sender: TObject);
begin
  if (lbWatch.ItemIndex <> -1) and Assigned(lbWatch.Items.Objects[lbWatch.ItemIndex])  then
  begin
    DebugInfo.WatchList.Delete(PTrwWatch(lbWatch.Items.Objects[lbWatch.ItemIndex]));
    Initialize;
  end;
end;


procedure TrwWatchList.actEditWatchExecute(Sender: TObject);
var
  h: string;
begin
  if (lbWatch.ItemIndex <> -1) and Assigned(lbWatch.Items.Objects[lbWatch.ItemIndex]) then
  begin
    h := PTrwWatch(lbWatch.Items.Objects[lbWatch.ItemIndex]).Expression;
    if InputQuery('Edit Watch', 'Expression', h) then
    begin
      PTrwWatch(lbWatch.Items.Objects[lbWatch.ItemIndex]).Expression := h;
      DebugInfo.WatchList.NeedCompileWatches := True;
      Initialize;
    end;
  end
  else
    AddWatch;
end;


procedure TrwWatchList.AddWatch;
var
  h: string;
begin
  h := '';
  if InputQuery('Add Watch', 'Expression', h) then
  begin
    DebugInfo.WatchList.Add(h);
    DebugInfo.WatchList.NeedCompileWatches := True;
    Initialize;
  end;
end;

procedure TrwWatchList.actStepOverExecute(Sender: TObject);
begin
  ReportDesigner.actStepOver.Execute;
end;

procedure TrwWatchList.actTraceIntoExecute(Sender: TObject);
begin
  ReportDesigner.actTraceInto.Execute;
end;

procedure TrwWatchList.actTerminateExecute(Sender: TObject);
begin
  ReportDesigner.actTerminate.Execute;
end;

procedure TrwWatchList.actRunExecute(Sender: TObject);
begin
  ReportDesigner.actRun.Execute;
end;


procedure TrwWatchList.Initialize;
var
  i, j: Integer;
  h: String;
begin
  if DebugInfo.WatchList.NeedCompileWatches  then
    CompileWatches;

  VM.CalcWatches;

  j := lbWatch.ItemIndex;
  lbWatch.Clear;
  for i := 0 to DebugInfo.WatchList.Count - 1 do
    if not DebugInfo.WatchList[i].DataSet then
    begin
      h := DebugInfo.WatchList[i].Result;
      if not DebugInfo.WatchList[i].Error then
        if DebugInfo.WatchList[i].PointerType <> '' then
        begin
          if h = '0' then
            h := 'nil';
          h := DebugInfo.WatchList[i].PointerType + '(' + h + ')';
        end;

      lbWatch.Items.AddObject(DebugInfo.WatchList[i].Expression + ':  ' + h,
        Pointer(DebugInfo.WatchList[i]));
    end;
  lbWatch.Items.AddObject('', nil);
  lbWatch.ItemIndex := j;
end;


procedure TrwWatchList.actPCodeExecute(Sender: TObject);
begin
  ReportDesigner.actPCode.Execute;
end;


procedure TrwWatchList.actAddWatch2Execute(Sender: TObject);
begin
  actAddWatch.Execute;
end;


procedure TrwWatchList.actCloseExecute(Sender: TObject);
begin
  Close;
end;

initialization

finalization
  CloseWatch;


end.
