unit rmOPEPictureFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPELoadFromFileFrm, StdCtrls, ExtCtrls, ExtDlgs;

type
  TrmOPEPicture = class(TrmOPELoadFromFile)
    Panel1: TPanel;
    imPicture: TImage;
  protected
    function  GetDefaultPropertyName: String; override;
    procedure LoadPropertyFromFile(const AFileName: String); override;
  public
    procedure AfterConstruction; override;
    procedure ShowPropValue; override;
    function  NewPropertyValue: Variant; override;
  end;

implementation

uses rmObjPropertyEditorFrm;

{$R *.dfm}

{ TrmOPELoadFromFile1 }

procedure TrmOPEPicture.AfterConstruction;
begin
  inherited;
  dlgOpenFile.Filter := GraphicFilter(TGraphic);
end;

function TrmOPEPicture.GetDefaultPropertyName: String;
begin
  Result := 'Picture';
end;

procedure TrmOPEPicture.LoadPropertyFromFile(const AFileName: String);
begin
  imPicture.Picture.LoadFromFile(AFileName);
  inherited;
end;

function TrmOPEPicture.NewPropertyValue: Variant;
begin
  Result := Integer(imPicture.Picture);
end;

procedure TrmOPEPicture.ShowPropValue;
var
  P: TPicture;
begin
  inherited;
  P := TPicture(Pointer(Integer(OldPropertyValue)));

  if Assigned(P.Graphic) then
    imPicture.Picture.Assign(P)
  else
    imPicture.Picture.Assign(nil);
end;

end.
 