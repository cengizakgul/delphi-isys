unit rmFunctParamFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rwBasicClasses, StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb,
  ISBasicClasses, rwDesignClasses, Wwdbspin, wwdbdatetimepicker, rwEngineTypes;

type
  TrmFunctParam = class(TFrame)
    lParamName: TLabel;
    cbValues: TisRWDBComboBox;
    chbValue: TCheckBox;
    eddValue: TwwDBDateTimePicker;
    ednValue: TisRWDBSpinEdit;
    edsValue: TEdit;
    procedure cbValuesChange(Sender: TObject);
  private
    FParam: TrwFuncParam;
    FValueControl: TControl;
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
    procedure NotifyChanges;
  public
    procedure Initialize(AParam: TrwFuncParam);
    property  Value: Variant read GetValue write SetValue;
  end;

implementation

{$R *.dfm}


uses rmOPETextFilterFrm;

{ TrmFunctParam }

function TrmFunctParam.GetValue: Variant;
begin
  if FValueControl = cbValues then
    Result := FParam.StrToDomainValue(cbValues.Value)

  else if FValueControl = chbValue then
    Result := chbValue.Checked

  else if FValueControl = eddValue then
    Result := eddValue.DateTime

  else if FValueControl = ednValue then
    Result := ednValue.Value

  else if FValueControl = edsValue then
    Result := edsValue.Text

  else
    Result := '';
end;

procedure TrmFunctParam.Initialize(AParam: TrwFuncParam);
var
  i: Integer;
begin
  FParam := AParam;

  cbValues.Visible := False;
  chbValue.Visible := False;
  eddValue.Visible := False;
  ednValue.Visible := False;
  edsValue.Visible := False;

  lParamName.Caption := FParam.DisplayName;

  if Length(FParam.Domain) > 0 then
  begin
    FValueControl := cbValues;

    for i := Low(FParam.Domain) to High(FParam.Domain) do
      cbValues.Items.Add(FParam.Domain[i].Description + #9 + FParam.DomainValueToStr(FParam.Domain[i].Value));
  end

  else
  begin
    if FParam.VarType = rwvBoolean then
      FValueControl := chbValue
    else if FParam.VarType = rwvDate then
      FValueControl := eddValue
    else if FParam.VarType in [rwvInteger, rwvFloat, rwvCurrency] then
      FValueControl := ednValue
    else
      FValueControl := edsValue;    
  end;

  FValueControl.Top := 0;
  FValueControl.Visible := True;
  FValueControl.Width := ClientWidth - FValueControl.Left;
  ClientHeight := FValueControl.Height + 2;
end;

procedure TrmFunctParam.NotifyChanges;
begin
  TrmOPETextFilter(Parent.Owner).ParamValueChanged;
end;

procedure TrmFunctParam.SetValue(const AValue: Variant);
begin
  FValueControl.Tag := 1;
  try
    if FValueControl = cbValues then
      cbValues.Value := FParam.DomainValueToStr(AValue)

    else if FValueControl = chbValue then
      chbValue.Checked := Boolean(AValue)

    else if FValueControl = eddValue then
      eddValue.DateTime := AValue

    else if FValueControl = ednValue then
      ednValue.Value := AValue

    else if FValueControl = edsValue then
      edsValue.Text := AValue;
  finally
    FValueControl.Tag := 0;
  end;
end;

procedure TrmFunctParam.cbValuesChange(Sender: TObject);
begin
  if TControl(Sender).Tag = 0 then
    NotifyChanges;
end;

end.
