unit rmDsgnLibraryFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDsgnComponentFrm, Menus, ImgList, rmCustomFrameFrm,
  rmCustomStatusBarFrm, ComCtrls, ExtCtrls, rmTypes, rwDsgnUtils, rwEngineTypes,
  rwUtils, rwTypes, rmLocalToolbarFrm, rmDsgnTypes, rwDesigner, rmDummyDsgnComponent;

type
  TrmDsgnLibrary = class(TrmDsgnComponent, IrmDsgnLibrary)
  private
    FSecurityDiscriptor: TrwDesignerSecurityRec;

    function  LibraryHolder: TrmSystemLibraryHolder;

    procedure DMFunctionsChanged(var Message: TrmMessageRec); message mFunctionsChanged;
    procedure DMVariablesChanged(var Message: TrmMessageRec); message mVariablesChanged;
    procedure DMComponentsChanged(var Message: TrmMessageRec); message mComponentsChanged;
    procedure DMTBSaveChanges(var Message: TrmMessageRec); message mtbSaveChanges;
    procedure DMTBReload(var Message: TrmMessageRec); message mtbReload;
    procedure DMTBCompile(var Message: TrmMessageRec); message mtbCompile;

  protected
    procedure GetListeningMessages(const AMessages: TList); override;
    procedure NewDesignComponent(const AClassName: String); override;
    procedure DestroyDsgnComponent; override;
    procedure PrepareDesigner; override;
    function  GetToolbarClass: TrmLocalToolBarCalss; override;
    function  GetCaption: String; override;
    function  IsLibraryModified: Boolean;
    function  GetSecurityInfo: TrmdSecurityRec; override;
    procedure OpenFromFile(const AFileName: String); override;
    procedure SaveToFile(const AFileName: String); override;

  public
    constructor Create(AOwner: TComponent); override;
  end;

implementation

uses rmTBDsgnLibraryFrm;

{$R *.dfm}

{ TrmDsgnLibrary }

function TrmDsgnLibrary.GetCaption: String;
begin
  Result := 'System Library';
end;

function TrmDsgnLibrary.GetToolbarClass: TrmLocalToolBarCalss;
begin
  Result := TrmTBDsgnLibrary;
end;

function TrmDsgnLibrary.IsLibraryModified: Boolean;
begin
  Result := Assigned(FComponent) and (LibraryHolder.FunctsModified or LibraryHolder.VarsModified or LibraryHolder.CompsModified);
end;

function TrmDsgnLibrary.LibraryHolder: TrmSystemLibraryHolder;
begin
  Result := TrmSystemLibraryHolder(FComponent.RealObject);
end;

procedure TrmDsgnLibrary.NewDesignComponent(const AClassName: String);
begin
  DestroyDsgnComponent;
  FComponent := TrmSystemLibraryHolder.Create(nil);
  PrepareDesigner;
end;

procedure TrmDsgnLibrary.PrepareDesigner;
begin
  inherited;
  lvItems.PopupMenu := nil;
end;

procedure TrmDsgnLibrary.DMTBCompile(var Message: TrmMessageRec);
begin
  inherited;
  LibraryHolder.CompileLibrary;
end;

procedure TrmDsgnLibrary.DMTBReload(var Message: TrmMessageRec);
begin
  inherited;
  UnPrepareDesigner;
  LibraryHolder.ReloadLibrary;
  PostBroadcastMessage(mComponentPaletteInfoChanged, 0, 0);
  PrepareDesigner;
end;

procedure TrmDsgnLibrary.DMTBSaveChanges(var Message: TrmMessageRec);
begin
  inherited;
  LibraryHolder.StoreLibrary;
end;

procedure TrmDsgnLibrary.DMComponentsChanged(var Message: TrmMessageRec);
begin
  inherited;
  LibraryHolder.CompsModified := True;
  PostBroadcastMessage(mComponentPaletteInfoChanged, 0, 0);
end;

procedure TrmDsgnLibrary.DMFunctionsChanged(var Message: TrmMessageRec);
begin
  inherited;
  LibraryHolder.FunctsModified := True;
end;

procedure TrmDsgnLibrary.DMVariablesChanged(var Message: TrmMessageRec);
begin
  inherited;
  LibraryHolder.VarsModified := True;
end;

procedure TrmDsgnLibrary.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mFunctionsChanged));
  AMessages.Add(Pointer(mVariablesChanged));
  AMessages.Add(Pointer(mComponentsChanged));
  AMessages.Add(Pointer(mtbSaveChanges));
  AMessages.Add(Pointer(mtbReload));
  AMessages.Add(Pointer(mtbCompile));
end;

function TrmDsgnLibrary.GetSecurityInfo: TrmdSecurityRec;
begin
  Result.EditComponents := FSecurityDiscriptor.LibComponents.Modify;
  Result.EditFunctions := FSecurityDiscriptor.LibFunctions.Modify;
  Result.EditVariables := False;  
end;

constructor TrmDsgnLibrary.Create(AOwner: TComponent);
begin
  inherited;
  FSecurityDiscriptor := RWDesignerExt.AppAdapter.GetSecurityDescriptor;
end;

procedure TrmDsgnLibrary.OpenFromFile(const AFileName: String);
begin
  LibraryHolder.LoadFromFile(AFileName);
end;

procedure TrmDsgnLibrary.SaveToFile(const AFileName: String);
begin
  LibraryHolder.SaveToFile(AFileName);
end;

procedure TrmDsgnLibrary.DestroyDsgnComponent;
begin
  if IsLibraryModified and AreYouSure('System Library has been changed. Do you want to save changes?') then
    LibraryHolder.StoreLibrary;

  inherited;
end;


end.

