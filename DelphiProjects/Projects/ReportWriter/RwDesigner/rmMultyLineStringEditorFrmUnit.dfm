object rmMultyLineStringEditorFrm: TrmMultyLineStringEditorFrm
  Left = 243
  Top = 114
  Width = 641
  Height = 455
  Caption = 'rmMultyLineStringEditorFrm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    633
    421)
  PixelsPerInch = 96
  TextHeight = 13
  object btnOK: TButton
    Tag = 267
    Left = 552
    Top = 8
    Width = 75
    Height = 23
    Anchors = [akTop, akRight]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancel: TButton
    Tag = 261
    Left = 552
    Top = 38
    Width = 75
    Height = 23
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object memStrings: TMemo
    Left = 3
    Top = 0
    Width = 541
    Height = 421
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    ScrollBars = ssBoth
    TabOrder = 2
    WordWrap = False
    OnChange = memStringsClick
    OnClick = memStringsClick
    OnEnter = memStringsClick
    OnKeyDown = memStringsKeyUp
    OnKeyUp = memStringsKeyUp
  end
  object pnPos: TPanel
    Left = 547
    Top = 402
    Width = 85
    Height = 19
    Anchors = [akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 3
  end
end
