unit rmTBComponentsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, PJMenuSpeedButtons, ImgList, rmTypes,
  rmDsgnTypes, rwDsgnUtils, rwEngine, rwUtils, ComCtrls, rmCustomFrameFrm, rwRTTI,
  StdCtrls, ExtCtrls, Math;

type
  TrmComponentToolButton = class(TrwSpeedButton, IrmMessageReceiver)
  private
    FrwClassName: String;
    FDsgnPaletteSettings: TrwDsgnPaletteSettings;

    procedure UpdatePicture;
    procedure SetRwClassName(const Value: String);

    procedure DMComponentClassNameChanged(var Message: TrmMessageRec); message mComponentClassNameChanged;
    procedure DMComponentIconChanged(var Message: TrmMessageRec); message mComponentIconChanged;

  protected
  //interface
    function  VCLObject: TObject;
    function  Apartment: IrmDsgnApartment;
    procedure GetListeningMessages(const AMessages: TList);

  public
    function  GetAffiliationFlag: Integer;
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;

    property rwClassName: String read FrwClassName write SetRwClassName;
    property DsgnPaletteSettings: TrwDsgnPaletteSettings read FDsgnPaletteSettings write FDsgnPaletteSettings;
  end;


  TrmTBComponents = class(TrmCustomFrame)
    tbtSelect: TrwSpeedButton;
    pnlBtns: TPanel;
    tbCompLibrary: TrwSpeedButton;
    pscrlComponents: TPageScroller;
    procedure tbtSelectClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure pscrlComponentsScroll(Sender: TObject; Shift: TShiftState; X,
      Y: Integer; Orientation: TPageScrollerOrientation;
      var Delta: Integer);
    procedure tbCompLibraryClick(Sender: TObject);
  private
    procedure OnButtonClick(Sender: TObject);
    procedure CancelCreateComponentMode;

    procedure DMComponentPaletteInfoChanged(var Message: TrmMessageRec); message mComponentPaletteInfoChanged;
    procedure DMComponentCreationFinished(var Message: TrmMessageRec); message mComponentCreationFinished;

  protected
    procedure GetListeningMessages(const AMessages: TList); override;

  public
    procedure AfterConstruction; override;
    procedure PrepareComponentPallet;
  end;


implementation

uses rwDsgnRes, rmTBDsgnComponentFrm;

{$R *.dfm}

{ TrmTBComponents }

procedure TrmTBComponents.AfterConstruction;
begin
  inherited;
  PrepareComponentPallet;
end;

procedure TrmTBComponents.CancelCreateComponentMode;
var
  i: Integer;
begin
  tbtSelect.Down := True;

  for i := 0 to pnlBtns.ControlCount - 1 do
    (pnlBtns.Controls[i] as TrwSpeedButton).Down := False;
end;

procedure TrmTBComponents.OnButtonClick(Sender: TObject);
var
  rwClassName: String;
begin
  if Sender is TrmComponentToolButton then
    rwClassName := TrmComponentToolButton(Sender).rwClassName
  else if Sender = tbCompLibrary then
    rwClassName := 'LIBRARY'
  else
    rwClassName := '';

  if rwClassName <> '' then
    tbtSelect.Down := False;

  PostApartmentMessage(Self, mComponentCreationStarted, rwClassName, 0);
end;

procedure TrmTBComponents.PrepareComponentPallet;
var
  Xpos: Integer;
  i: Integer;
  Cl: IrwClass;
  FixedButtons: TList;

  procedure AddButton(const AClassName: String; const ADsgnPaletteSettings: TrwDsgnPaletteSettings; const AHintText: String);
  var
    TB: TrmComponentToolButton;
    i: Integer;
  begin
    TB := nil;

    for i := 0 to ComponentCount - 1 do
      if Components[i] is TrmComponentToolButton then
        if SameText(TrmComponentToolButton(Components[i]).rwClassName, AClassName) then
        begin
          TB := TrmComponentToolButton(Components[i]);
          break;
        end;

    if not Assigned(TB) then
    begin
      if rwDPSShowOnPalette in ADsgnPaletteSettings then
      begin
        TB := TrmComponentToolButton.Create(Self);
        TB.Visible := False;
        TB.Top := tbCompLibrary.Top;
        TB.Width := tbCompLibrary.Width;
        TB.Height := tbCompLibrary.Height;
        TB.Flat := True;
        TB.GroupIndex := 1;
        TB.AllowAllUp := True;
        TB.rwClassName := AClassName;
        TB.OnClick := OnButtonClick;
        TB.DsgnPaletteSettings := ADsgnPaletteSettings;
        TB.Parent := pnlBtns;
      end
      else
        Exit;
    end

    else if FixedButtons.IndexOf(TB) <> -1 then
      Exit

    else if not (rwDPSShowOnPalette in ADsgnPaletteSettings) then
    begin
      TB.Visible := False;
      Exit;
    end;

    TB.DsgnPaletteSettings := ADsgnPaletteSettings;
    TB.Visible := TrmTBDsgnComponent(Owner).InContext(TB.GetAffiliationFlag);

    if TB.Visible then
    begin
      TB.UpdatePicture;
      TB.Left := Xpos;
      Inc(Xpos, TB.Width);
      FixedButtons.Add(TB);      
    end;
  end;

  procedure AddStdButton(const AClassName: String; const ADsgnPaletteSettings: TrwDsgnPaletteSettings);
  var
    h: String;
  begin
    h := RWEngineExt.AppAdapter.SubstDefaultClassName(AClassName);
    AddButton(h, ADsgnPaletteSettings, GetRWClassDescription(GetClass(h)));
  end;

{  procedure HideAllButtons;
  var
    i: Integer;
  begin
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TrmComponentToolButton then
        TrmComponentToolButton(Components[i]).Visible := False;
  end;
}
begin
  FixedButtons := TList.Create;
  try
    Xpos := tbCompLibrary.Left + tbCompLibrary.Width;

    AddStdButton('TrwQuery',  [rwDPSShowOnPalette, rwDPSComponentEditModeOnly]);
    AddStdButton('TrwBuffer', [rwDPSShowOnPalette, rwDPSComponentEditModeOnly]);
    AddStdButton('TrwCustomObject', [rwDPSShowOnPalette, rwDPSAdvancedModeOnly, rwDPSComponentEditModeOnly]);

    AddStdButton('TrmPrintText',  [rwDPSShowOnPalette]);
    AddStdButton('TrmPrintPanel', [rwDPSShowOnPalette]);
    AddStdButton('TrmPrintImage', [rwDPSShowOnPalette]);
    AddStdButton('TrmTable',      [rwDPSShowOnPalette]);
    AddStdButton('TrmDBTable',    [rwDPSShowOnPalette]);
    AddStdButton('TrmPrintPageNumber', [rwDPSShowOnPalette]);

    AddStdButton('TrwText',       [rwDPSShowOnPalette]);
    AddStdButton('TrwPanel',      [rwDPSShowOnPalette]);
    AddStdButton('TrwGroupBox',   [rwDPSShowOnPalette]);
    AddStdButton('TrwPageControl',[rwDPSShowOnPalette]);
    AddStdButton('TrwEdit',       [rwDPSShowOnPalette, rwDPSComponentEditModeOnly]);
    AddStdButton('TrwDateEdit',   [rwDPSShowOnPalette, rwDPSComponentEditModeOnly]);
    AddStdButton('TrwComboBox',   [rwDPSShowOnPalette, rwDPSComponentEditModeOnly]);
    AddStdButton('TrwCheckBox',   [rwDPSShowOnPalette, rwDPSComponentEditModeOnly]);
    AddStdButton('TrwRadioGroup', [rwDPSShowOnPalette, rwDPSComponentEditModeOnly]);
    AddStdButton('TrwDBComboBox', [rwDPSShowOnPalette, rwDPSComponentEditModeOnly]);
    AddStdButton('TrwDBGrid',     [rwDPSShowOnPalette, rwDPSComponentEditModeOnly]);

    AddStdButton('TrmASCIIRecord', [rwDPSShowOnPalette]);
    AddStdButton('TrmASCIIField',  [rwDPSShowOnPalette]);

    AddStdButton('TrmXMLSimpleTypeByRestriction', [rwDPSShowOnPalette]);
    AddStdButton('TrmXMLSimpleTypeByList', [rwDPSShowOnPalette]);
    AddStdButton('TrmXMLSimpleTypeByUnion', [rwDPSShowOnPalette]);
    AddStdButton('TrmXMLAttribute', [rwDPSShowOnPalette]);
    AddStdButton('TrmXMLAttributeGroup', [rwDPSShowOnPalette]);
    AddStdButton('TrmXMLElement', [rwDPSShowOnPalette]);
    AddStdButton('TrmXMLGroup', [rwDPSShowOnPalette]);
    AddStdButton('TrmXMLSequence', [rwDPSShowOnPalette]);
    AddStdButton('TrmXMLChoice', [rwDPSShowOnPalette]);
    AddStdButton('TrmXMLfragment', [rwDPSShowOnPalette]);

    for i := 0 to rwRTTIInfo.GetItemCount - 1 do
    begin
      Cl := rwRTTIInfo.GetItemByIndex(i);
      if Cl.IsUserClass and
         not (Cl.DescendantOf('TrmReport') or Cl.DescendantOf('TrmPrintForm') or Cl.DescendantOf('TrmInputForm') or Cl.DescendantOf('TrmASCIIForm')) then
        AddButton(Cl.GetClassName, Cl.GetUserClass.GetDsgnPaletteSettings, Cl.GetUserClass.GetDisplayName);
    end;
  finally
    FreeAndNil(FixedButtons);
  end;

  CancelCreateComponentMode;
end;

procedure TrmTBComponents.tbtSelectClick(Sender: TObject);
begin
  if tbtSelect.Down then
    CancelCreateComponentMode;
  OnButtonClick(Sender);
end;

procedure TrmTBComponents.DMComponentPaletteInfoChanged(var Message: TrmMessageRec);
begin
  inherited;
  PrepareComponentPallet;
end;

procedure TrmTBComponents.DMComponentCreationFinished(var Message: TrmMessageRec);
begin
  inherited;
  CancelCreateComponentMode;
end;

procedure TrmTBComponents.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mComponentPaletteInfoChanged));
  AMessages.Add(Pointer(mComponentCreationFinished));
end;

{ TrmComponentToolButton }

procedure TrmComponentToolButton.AfterConstruction;
begin
  inherited;
  RegisterTalkativeApartmentMember(Self);
end;

function TrmComponentToolButton.Apartment: IrmDsgnApartment;
begin
  Result := GetComponentAppartment(Self);
end;

procedure TrmComponentToolButton.BeforeDestruction;
begin
  inherited;
  UnRegisterTalkativeApartmentMember(Self);
end;

function TrmComponentToolButton.GetAffiliationFlag: Integer;
var
  ObjType: TrwDsgnObjectType;
  Cl: IrwClass;
begin
  Result := 0;

  if rwDPSAdvancedModeOnly in FDsgnPaletteSettings then
    Result := Result or $10;

  if rwDPSComponentEditModeOnly in FDsgnPaletteSettings then
    Result := Result or $1;

  if rwDPSShowOnPalette in FDsgnPaletteSettings then
  begin
    Cl := rwRTTIInfo.FindClass(FrwClassName);
    ObjType := ClassObjectType(Cl.GetVCLClass.ClassName);

    case ObjType of
      rwDOTNotVisual:   Result := Result or $2 or $4 or $20 or $40;
      rwDOTPrintForm:   Result := Result or $2;
      rwDOTInputForm:   Result := Result or $4;
      rwDOTASCIIForm:   Result := Result or $20;
      rwDOTXMLForm:     Result := Result or $40;
    end;
  end;
end;

procedure TrmComponentToolButton.GetListeningMessages(const AMessages: TList);
begin
  AMessages.Add(Pointer(mComponentClassNameChanged));
  AMessages.Add(Pointer(mComponentIconChanged));
end;

function TrmComponentToolButton.VCLObject: TObject;
begin
  Result := Self;
end;

procedure TrmComponentToolButton.SetRwClassName(const Value: String);
begin
  FrwClassName := Value;
  Hint := FrwClassName;
end;

procedure TrmComponentToolButton.UpdatePicture;
begin
  DsgnRes.RWClassBigPictures.GetClassPicture(rwClassName, Glyph);
end;

procedure TrmTBComponents.FrameResize(Sender: TObject);
begin
  pscrlComponents.Width := ClientWidth - pscrlComponents.Left;
end;

procedure TrmTBComponents.pscrlComponentsScroll(Sender: TObject;
  Shift: TShiftState; X, Y: Integer; Orientation: TPageScrollerOrientation;
  var Delta: Integer);
begin
  if pscrlComponents.Position = 0 then
    Delta := (tbtSelect.Width - pscrlComponents.ButtonSize) * Sign(Delta)
  else
    Delta := tbtSelect.Width * Sign(Delta);  
end;

procedure TrmTBComponents.tbCompLibraryClick(Sender: TObject);
begin
  OnButtonClick(Sender);
end;

procedure TrmComponentToolButton.DMComponentClassNameChanged(var Message: TrmMessageRec);
begin
  inherited;
  if AnsiSameStr(Message.Parameter1, rwClassName) then
    rwClassName := Message.Parameter2;
end;

procedure TrmComponentToolButton.DMComponentIconChanged(var Message: TrmMessageRec);
begin
  inherited;
  if AnsiSameStr(Message.Parameter1, rwClassName) then
    UpdatePicture;
end;

end.

