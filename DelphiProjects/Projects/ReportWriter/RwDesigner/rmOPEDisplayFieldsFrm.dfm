inherited rmOPEDisplayFields: TrmOPEDisplayFields
  Width = 296
  Height = 173
  object grFields: TisRWDBGrid
    Left = 0
    Top = 0
    Width = 296
    Height = 173
    DisableThemesInTitle = False
    Selected.Strings = (
      'Selected'#9'4'#9'Show'#9#9
      'FieldName'#9'18'#9'Field Name'#9#9
      'Title'#9'19'#9'Title'#9#9
      'Size'#9'3'#9'Size'#9#9)
    IniAttributes.Enabled = True
    IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
    IniAttributes.SectionName = 'TrmOPEDisplayFields\grFields'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = False
    ShowVertScrollBar = False
    Align = alClient
    DataSource = ds
    Options = [dgEditing, dgTitles, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    UseTFields = False
    PaintOptions.AlternatingRowColor = clCream
    Sorting = False
    RecordCountHintEnabled = False
  end
  object dsFields: TisRWClientDataSet
    ControlType.Strings = (
      'Selected;CheckBox;Y;N')
    FieldDefs = <
      item
        Name = 'Selected'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FieldName'
        DataType = ftString
        Size = 64
      end
      item
        Name = 'Title'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Size'
        DataType = ftInteger
      end>
    Left = 96
    Top = 96
    object dsFieldsSelected: TStringField
      DisplayLabel = 'Show'
      DisplayWidth = 5
      FieldName = 'Selected'
      Size = 1
    end
    object dsFieldsFieldName: TStringField
      DisplayLabel = 'Field Name'
      DisplayWidth = 14
      FieldName = 'FieldName'
      Size = 64
    end
    object dsFieldsTitle: TStringField
      DisplayWidth = 15
      FieldKind = fkCalculated
      FieldName = 'Title'
      Size = 40
      Calculated = True
    end
    object dsFieldsSize: TIntegerField
      DisplayWidth = 3
      FieldName = 'Size'
    end
  end
  object ds: TDataSource
    DataSet = dsFields
    OnDataChange = dsDataChange
    Left = 160
    Top = 96
  end
end
