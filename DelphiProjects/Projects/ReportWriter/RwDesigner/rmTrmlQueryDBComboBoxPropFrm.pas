unit rmTrmlQueryDBComboBoxPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmlCaptionControlPropFrm, rmOPEQueryFrm, rmOPEReportParamFrm,
  rmOPEFontFrm, rmOPEEnumFrm, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, StdCtrls, ExtCtrls, ComCtrls, Grids, Wwdbigrd,
  Wwdbgrid, DB, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  rwDesignClasses, rwEngineTypes, rwUtils, rwQBInfo, rwDsgnUtils, rwBasicUtils,
  rmOPEColorFrm, rmOPEDisplayFieldsFrm;

type
  TrmTrmlQueryDBComboBoxProp = class(TrmTrmlCaptionControlProp)
    frmKeyField: TrmOPEEnum;
    tsData: TTabSheet;
    gbDisplayFields: TGroupBox;
    frmDisplayFields: TrmOPEDisplayFields;
    frmQuery: TrmOPEQuery;
    procedure frmQuerybtnQueryClick(Sender: TObject);
    procedure frmKeyFieldcbListClick(Sender: TObject);
  protected
    procedure AssignParmType; override;
    procedure GetPropsFromObject; override;
    procedure MakeFieldList;
  end;

implementation

uses rmTrmlCustomControlPropFrm;

{$R *.dfm}

{ TrmTrwlCaptionControlProp1 }

procedure TrmTrmlQueryDBComboBoxProp.AssignParmType;
begin
  frmParam1.ParamType := rwvString;
end;

procedure TrmTrmlQueryDBComboBoxProp.GetPropsFromObject;
begin
  inherited;
  frmKeyField.PropertyName := 'KeyField';
  frmDisplayFields.PropertyName := 'DisplayFields';
  MakeFieldList;
  frmKeyField.ShowPropValue;
end;

procedure TrmTrmlQueryDBComboBoxProp.MakeFieldList;
var
  PropList: TrwStrList;
  Q: TObject;
  L: TStringList;
begin
  PropList := RWObject.PropertiesByTypeName('TrwQBQuery');
  if Length(PropList) > 0 then
    Q := Pointer(Integer(RWObject.GetPropertyValue(PropList[0])))
  else
    Q := nil;

  if Assigned(Q) then
  begin
    L := TStringList.Create;
    try
      rmGetFieldListFromQBQuery(TrwQBQuery(Q), L);
      frmKeyField.Values := L;
      frmDisplayFields.FieldList := L.Text; 
    finally
      FreeAndNil(L);
    end;
  end

  else
  begin
    frmKeyField.Values := nil;
    frmDisplayFields.FieldList := '';
  end;
end;

procedure TrmTrmlQueryDBComboBoxProp.frmQuerybtnQueryClick(
  Sender: TObject);
begin
  inherited;
  frmQuery.btnQueryClick(Sender);
  GetPropsFromObject;
end;

procedure TrmTrmlQueryDBComboBoxProp.frmKeyFieldcbListClick(
  Sender: TObject);
begin
  inherited;
  if (frmParam1.edParamName.Text = '') and (frmKeyField.NewPropertyValue <> '') then
    frmParam1.edParamName.Text := frmKeyField.NewPropertyValue;
end;

end.
