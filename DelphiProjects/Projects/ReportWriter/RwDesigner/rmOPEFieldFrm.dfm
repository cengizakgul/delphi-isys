inherited rmOPEField: TrmOPEField
  Width = 248
  Height = 51
  AutoSize = True
  object Label1: TLabel
    Left = 0
    Top = 2
    Width = 34
    Height = 13
    Caption = 'Source'
  end
  object Label2: TLabel
    Left = 0
    Top = 32
    Width = 22
    Height = 13
    Caption = 'Field'
  end
  object cbDataSource: TisRWDBComboBox
    Left = 55
    Top = 0
    Width = 193
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    DropDownCount = 8
    ItemHeight = 0
    Sorted = False
    TabOrder = 0
    UnboundDataType = wwDefault
    OnChange = cbDataSourceChange
  end
  object cbField: TisRWDBComboBox
    Left = 55
    Top = 30
    Width = 193
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = False
    AllowClearKey = False
    DropDownCount = 8
    ItemHeight = 0
    Sorted = False
    TabOrder = 1
    UnboundDataType = wwDefault
    OnChange = cbFieldChange
  end
end
