unit rmSettingsHolder;

interface

uses Classes, SysUtils, IniFiles, Variants, rmDsgnTypes, EvStreamUtils;

type

  TrmCustomSettingsHolder = class(TInterfacedObject, IrmSettingsHolder)
  private
    FFileName: String;
  public
    constructor Create(const AFileName: String); virtual;
    procedure   SaveData(const APath: String; const AValue: Variant); virtual; abstract;
    function    ReadData(const APath: String; const ADefaultValue: Variant): Variant; virtual; abstract;
  end;

  TrmSettingsINIHolder = class(TrmCustomSettingsHolder)
  private
    FIniFile:  TIniFile;
  public
    constructor Create(const AFileName: String); override;
    destructor  Destroy; override;
    procedure   SaveData(const APath: String; const AValue: Variant); override;
    function    ReadData(const APath: String; const ADefaultValue: Variant): Variant; override;
  end;

implementation

uses rwBasicUtils;


{ TrmCustomSettingsHolder }

constructor TrmCustomSettingsHolder.Create(const AFileName: String);
begin
  FFileName := AFileName;

  if not FileExists(FFileName) then
  begin
    try
      TFileStream.Create(FFileName, fmCreate).Free;  // create an empty file if possible (no rights case)
    except
    end;
  end;
end;


{ TrmSettingsHolder }

constructor TrmSettingsINIHolder.Create(const AFileName: String);
begin
  inherited;
  FIniFile := TIniFile.Create(FFileName);
end;

destructor TrmSettingsINIHolder.Destroy;
begin
  FreeAndNil(FIniFile);
  inherited;
end;

function TrmSettingsINIHolder.ReadData(const APath: String; const ADefaultValue: Variant): Variant;
var
  Ident, Section, h: String;
  S: IEvDualStream;
begin
  Result := ADefaultValue;

  try
    Ident := APath;
    Section := GetNextStrValue(Ident, '\');

    if VarIsArray(ADefaultValue) then
    begin
      S := TEvDualStreamHolder.CreateInMemory;
      FIniFile.ReadBinaryStream(Section, Ident, S.RealStream);
      Result := S.AsVarArrayOfBytes;
    end

    else
    begin
      h := FIniFile.ReadString(Section, Ident, VarToStr(ADefaultValue));
      Result := VarAsType(h, VarType(ADefaultValue));
    end;
  except
    // in case if settings file is not accessible
  end;
end;

procedure TrmSettingsINIHolder.SaveData(const APath: String; const AValue: Variant);
var
  Ident, Section: String;
  S: IEvDualStream;
begin
  try
    Ident := APath;
    Section := GetNextStrValue(Ident, '\');

    if VarIsArray(AValue) then
    begin
      S := TEvDualStreamHolder.CreateFromVariant(AValue);
      S.Position := 0;
      FIniFile.WriteBinaryStream(Section, Ident, S.RealStream);
    end

    else
      FIniFile.WriteString(Section, Ident, VarToStr(AValue));
  except
    // in case if settings file is not accessible  
  end;
end;


end.
