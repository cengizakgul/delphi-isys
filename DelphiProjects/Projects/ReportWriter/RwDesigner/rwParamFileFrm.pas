unit rwParamFileFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TrwParamFile = class(TForm)
    GroupBox1: TGroupBox;
    edParams: TEdit;
    btnBrowse: TButton;
    btnOK: TButton;
    btnCancel: TButton;
    opdParams: TOpenDialog;
    procedure btnBrowseClick(Sender: TObject);
  private
  public
  end;

  procedure ShowParamFile(var AParamFile: String);

implementation

{$R *.DFM}

uses rwDesignerFrm;


procedure ShowParamFile(var AParamFile: String);
var
  Frm: TrwParamFile;
begin
  Frm := TrwParamFile.Create(ReportDesigner);
  with Frm do
    try
      edParams.Text := AParamFile;
      if ShowModal = mrOK then
        AParamFile := Trim(edParams.Text);
    finally
      Free;
    end;
end;


procedure TrwParamFile.btnBrowseClick(Sender: TObject);
begin
  opdParams.InitialDir := ExtractFileDir(edParams.Text);
  opdParams.FileName := ExtractFileName(edParams.Text);
  if opdParams.Execute then
    edParams.Text := opdParams.FileName;
end;

end.
