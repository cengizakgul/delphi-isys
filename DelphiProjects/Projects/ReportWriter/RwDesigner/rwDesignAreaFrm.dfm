object rwDesignArea: TrwDesignArea
  Left = 0
  Top = 0
  Width = 322
  Height = 308
  AutoScroll = False
  Color = clWindow
  ParentColor = False
  TabOrder = 0
  OnMouseDown = pnlAreaMouseDown
  object pmTrwPageControl: TPopupMenu
    AutoPopup = False
    Left = 88
    Top = 16
    object NewPage1: TMenuItem
      Caption = 'New Page'
      OnClick = NewPage1Click
    end
    object NextPage1: TMenuItem
      Caption = 'Next Page'
      OnClick = NextPage1Click
    end
    object PreviousPage1: TMenuItem
      Caption = 'Previous Page'
      OnClick = PreviousPage1Click
    end
  end
end
