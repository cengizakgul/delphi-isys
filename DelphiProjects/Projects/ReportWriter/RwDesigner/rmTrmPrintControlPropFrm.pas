unit rmTrmPrintControlPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, rmOPEBooleanFrm, rmOPEFontFrm,
  rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, StdCtrls, ExtCtrls,
  ComCtrls, rmOPEColorFrm, rmOPEExpressionFrm, rmTypes,
  rmOPEBlockingControlFrm, rmTrmXMLNodePropFrm;

type
  TrmTrmPrintControlProp = class(TrmOPBasic)
    tcPrint: TTabSheet;
    frmCutable: TrmOPEBoolean;
    frmPrintOnEachPage: TrmOPEBoolean;
    frmBlockParentIfEmpty: TrmOPEBoolean;
    pnlBlocking: TPanel;
    frmPrintIf: TrmOPEBlockingControl;
  protected
    procedure GetPropsFromObject; override;
  end;

implementation

uses TypInfo;

{$R *.dfm}

{ TrmOPBasic1 }

procedure TrmTrmPrintControlProp.GetPropsFromObject;
begin
  inherited;
  frmCutable.PropertyName := 'Cutable';
  frmCutable.ShowPropValue;
  frmPrintOnEachPage.PropertyName := 'PrintOnEachPage';
  frmPrintOnEachPage.ShowPropValue;
  frmBlockParentIfEmpty.PropertyName := 'BlockParentIfEmpty';
  frmBlockParentIfEmpty.ShowPropValue;

  frmPrintIf.BlockID := 'Calc';
  frmPrintIf.ShowPropValue;
end;

end.
