unit rmOPEExpressionFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, rmFMLExprPanelFrm, rmFormula, rwBasicClasses,
  rwDsgnUtils, rmTypes, rmFMLExprEditorFormFrm;

type
  TrmOPEExpression = class(TrmObjPropertyEditor)
    frmExpression: TrmFMLExprPanel;
    procedure frmExpressionpnlCanvasClick(Sender: TObject);
  private
    FFormulaActionType: TrmFMLActionType;
    FOriginalFormula: TrmFMLFormula;
    FExpression: TrmFMLFormulaContent;    
    procedure WMRefresh(var Message: TMessage); message WM_RMD_REFRESH;
  protected
    function GetDefaultPropertyName: String; override;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;

    function  NewPropertyValue: Variant; override;
    procedure ShowPropValue; override;
    procedure ApplyChanges; override;
    procedure ClearExpressionForObject;

    property  FormulaActionType: TrmFMLActionType read FFormulaActionType write FFormulaActionType;
  end;

implementation

{$R *.dfm}

{ TrmObjPropertyEditor1 }

procedure TrmOPEExpression.AfterConstruction;
begin
  inherited;
  FFormulaActionType := fatSetProperty;
  frmExpression.ReadOnly := True;
  FExpression := TrmFMLFormulaContent.Create(nil, TrmFMLFormulaItem);
  frmExpression.Expression := FExpression;
end;

procedure TrmOPEExpression.ApplyChanges;
var
  Fml: TrmFMLFormula;
begin
  inherited;

  if Assigned(FOriginalFormula) then
    Fml := FOriginalFormula
  else
    Fml := TrwComponent(LinkedObject).Formulas.AddFormula(FormulaActionType, PropertyName);

  Fml.Content.Assign(FExpression);

  if Fml.Content.Count = 0 then
    ClearExpressionForObject;
end;

procedure TrmOPEExpression.BeforeDestruction;
begin
  inherited;
  FreeAndNil(FExpression);
end;

procedure TrmOPEExpression.ClearExpressionForObject;
var
  Fml: TrmFMLFormula;
begin
  Fml := TrwComponent(LinkedObject).Formulas.FormulaByAction(FormulaActionType, PropertyName);
  FreeAndNil(Fml);
  FOriginalFormula := nil;
end;

function TrmOPEExpression.GetDefaultPropertyName: String;
begin
  Result := 'Value';
end;

function TrmOPEExpression.NewPropertyValue: Variant;
begin
  VarClear(Result);
end;

procedure TrmOPEExpression.ShowPropValue;
begin
  FOriginalFormula := TrwComponent(LinkedObject).Formulas.FormulaByAction(FormulaActionType, PropertyName);

  if Assigned(FOriginalFormula) then
    FExpression.Assign(FOriginalFormula.Content)
  else
    FExpression.Clear;

  frmExpression.ReDraw;
end;

procedure TrmOPEExpression.WMRefresh(var Message: TMessage);
begin
  NotifyOfChanges;
  frmExpression.ReDraw;
end;

procedure TrmOPEExpression.frmExpressionpnlCanvasClick(Sender: TObject);
begin
  TrmFMLExprEditorForm.ShowFMLEditor(frmExpression.Expression, Self);
end;

end.
