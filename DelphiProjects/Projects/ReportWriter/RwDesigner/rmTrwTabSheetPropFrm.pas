unit rmTrwTabSheetPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, StdCtrls, ExtCtrls, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls, rmOPEFontFrm;

type
  TrmTrwTabSheetProp = class(TrmOPBasic)
    frmCaption: TrmOPEStringSingleLine;
  protected
    procedure GetPropsFromObject; override;
  end;

implementation

{$R *.dfm}

{ TrmTrwTabSheetProp }

procedure TrmTrwTabSheetProp.GetPropsFromObject;
begin
  inherited;
  frmCaption.PropertyName := 'Caption';
  frmCaption.ShowPropValue;
end;

end.
 