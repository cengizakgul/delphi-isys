unit rmObjectPropsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, rwBasicClasses, StdCtrls, ExtCtrls, rwUtils, rmDsgnTypes, rmTypes,
  rwDsgnUtils, rmObjPropertyEditorFrm, rmCustomFrameFrm;

type
  TrmObjectProp = class(TrmCustomFrame)
  private
    FRWObject: IrmDesignObject;
    procedure SetRWObject(const Value: IrmDesignObject);

  protected
    procedure GetPropsFromObject; virtual;

  public
    procedure OnChangeProperty(Sender: TrmObjPropertyEditor); virtual;

    property RWObject: IrmDesignObject read FRWObject write SetRWObject;
  end;

  TrmObjectPropsClass = class of TrmObjectProp;

implementation

{$R *.dfm}

uses rmObjectPropFormFrm;

{ TrmObjectProp }

procedure TrmObjectProp.GetPropsFromObject;
begin
end;

procedure TrmObjectProp.OnChangeProperty(Sender: TrmObjPropertyEditor);
begin
end;

procedure TrmObjectProp.SetRWObject(const Value: IrmDesignObject);
begin
  FRWObject := Value;
  GetPropsFromObject;
  TrmObjectPropForm(Parent).Caption := 'Object Properties - ' + GetRWClassDescription(GetClass(Value.RealObject.ClassName));
  if DesignRMAdvancedMode then
    TrmObjectPropForm(Parent).Caption := TrmObjectPropForm(Parent).Caption + ' (' + Value.GetClassName + ')';
end;

end.
