inherited rmTrmPrintContainerProp: TrmTrmPrintContainerProp
  inherited pcCategories: TPageControl
    inherited tcAppearance: TTabSheet
      inline frmColor: TrmOPEColor
        Left = 10
        Top = 8
        Width = 75
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
      end
    end
    inherited tcPrint: TTabSheet
      inherited pnlBlocking: TPanel
        Top = 122
        TabOrder = 4
      end
      inline frmMultiPaged: TrmOPEBoolean
        Left = 8
        Top = 87
        Width = 213
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 3
        inherited chbProp: TCheckBox
          Width = 213
          Caption = 'Scroll overflowing content (Virtual Page)'
        end
      end
    end
  end
end
