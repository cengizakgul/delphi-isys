inherited rwCommonToolsBar: TrwCommonToolsBar
  Width = 987
  AutoScroll = False
  ParentShowHint = False
  ShowHint = True
  inherited ctbBar: TControlBar
    Width = 987
    inherited tlbComponents: TToolBar
      Left = 826
      Width = 92
      TabOrder = 4
      object tlbQuery: TToolButton
        Left = 23
        Top = 0
        Hint = 'Query'
        Caption = 'TrwQuery'
        Grouped = True
        ImageIndex = 14
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tlbBuffer: TToolButton
        Left = 46
        Top = 0
        Hint = 'Buffer'
        Caption = 'TrwBuffer'
        Grouped = True
        ImageIndex = 15
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tlbCustomObject: TToolButton
        Left = 69
        Top = 0
        Hint = 'CustomObject'
        Caption = 'TrwCustomObject'
        Grouped = True
        ImageIndex = 75
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
    end
    inherited tlbAlignment: TToolBar
      Left = 329
      TabOrder = 5
    end
    inherited tlbSize: TToolBar
      Left = 224
      TabOrder = 6
    end
    inherited tlbFont: TToolBar
      Left = 316
      Width = 471
    end
    object tlbText: TToolBar
      Left = 11
      Top = 28
      Width = 200
      Height = 21
      Align = alNone
      AutoSize = True
      ButtonHeight = 21
      Caption = 'Text'
      DragKind = dkDock
      EdgeBorders = []
      Flat = True
      TabOrder = 0
      Wrapable = False
      object edtText: TEdit
        Left = 0
        Top = 0
        Width = 200
        Height = 21
        Hint = 'Text'
        AutoSelect = False
        TabOrder = 0
        OnChange = edtTextChange
      end
    end
    object tlbExperts: TToolBar
      Left = 630
      Top = 28
      Width = 92
      Height = 22
      Align = alNone
      AutoSize = True
      Caption = 'Experts'
      DragKind = dkDock
      EdgeBorders = []
      Flat = True
      Images = DsgnRes.ilButtons
      TabOrder = 1
      Wrapable = False
      object tbtVarFunc: TToolButton
        Left = 0
        Top = 0
        Hint = 'Global Variables & Functions'
        Caption = 'tbtVarFunc'
        ImageIndex = 47
        OnClick = tbtVarFuncClick
      end
      object btnQB: TToolButton
        Tag = 10
        Left = 23
        Top = 0
        Hint = 'Query Builder'
        Caption = 'ToolButton2'
        ImageIndex = 39
        OnClick = btnQBClick
      end
      object tbtLibrary: TToolButton
        Left = 46
        Top = 0
        Hint = 'Library'
        Caption = 'tbtLibrary'
        ImageIndex = 46
        OnClick = tbtLibraryClick
      end
      object tbtEvents: TToolButton
        Left = 69
        Top = 0
        Hint = 'All object events'
        Caption = 'tbtEvents'
        ImageIndex = 61
        OnClick = tbtEventsClick
      end
    end
    object tlbActions: TToolBar
      Left = 11
      Top = 2
      Width = 292
      Height = 22
      Align = alNone
      AutoSize = True
      Caption = 'Actions'
      DragKind = dkDock
      EdgeBorders = []
      Flat = True
      Images = DsgnRes.ilButtons
      TabOrder = 2
      Wrapable = False
      object tbtOpen: TToolButton
        Left = 0
        Top = 0
        Hint = 'Open Report'
        Caption = 'tbtOpen'
        ImageIndex = 58
      end
      object tbtSave: TToolButton
        Left = 23
        Top = 0
        Hint = 'Save Report'
        Caption = 'tbtSave'
        ImageIndex = 59
      end
      object tbtCompile: TToolButton
        Left = 46
        Top = 0
        Hint = 'Compile Report'
        Caption = 'tbtCompile'
        ImageIndex = 60
      end
      object ToolButton20: TToolButton
        Left = 69
        Top = 0
        Width = 8
        Caption = 'ToolButton20'
        ImageIndex = 70
        Style = tbsSeparator
      end
      object tbtRun: TToolButton
        Left = 77
        Top = 0
        Caption = 'tbtRun'
        ImageIndex = 69
      end
      object tbtPause: TToolButton
        Left = 100
        Top = 0
        Caption = 'tbtPause'
        ImageIndex = 70
      end
      object tbtTerminate: TToolButton
        Left = 123
        Top = 0
        Caption = 'tbtTerminate'
        ImageIndex = 72
      end
      object tbtTraceInto: TToolButton
        Left = 146
        Top = 0
        Hint = 'Trace Into'
        Caption = 'tbtTraceInto'
        ImageIndex = 67
      end
      object tbtStepOver: TToolButton
        Left = 169
        Top = 0
        Hint = 'Step Over'
        Caption = 'tbtStepOver'
        ImageIndex = 66
      end
      object tbtBrkPoint: TToolButton
        Left = 192
        Top = 0
        Hint = 'Break Point On/Off'
        Caption = 'tbtBrkPoint'
        ImageIndex = 68
      end
      object ToolButton16: TToolButton
        Left = 215
        Top = 0
        Width = 8
        Caption = 'ToolButton16'
        ImageIndex = 0
        Style = tbsSeparator
      end
      object tbtCut: TToolButton
        Left = 223
        Top = 0
        Hint = 'Cut'
        Caption = 'tbtCut'
        ImageIndex = 76
      end
      object tbtCopy: TToolButton
        Left = 246
        Top = 0
        Hint = 'Copy'
        Caption = 'tbtCopy'
        ImageIndex = 77
      end
      object tbtPaste: TToolButton
        Left = 269
        Top = 0
        Hint = 'Paste'
        Caption = 'tbtPaste'
        ImageIndex = 78
      end
    end
  end
  inherited pmToolBars: TPopupMenu
    Left = 584
    Top = 8
  end
end
