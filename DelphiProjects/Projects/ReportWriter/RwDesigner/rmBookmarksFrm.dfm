inherited rmBookmarksViewer: TrmBookmarksViewer
  Width = 257
  Height = 85
  object BookmarksListView: TListView
    Left = 0
    Top = 0
    Width = 257
    Height = 85
    Align = alClient
    Columns = <>
    TabOrder = 0
    ViewStyle = vsReport
    OnDblClick = BookmarksListViewDblClick
    OnEditing = BookmarksListViewEditing
    OnKeyDown = BookmarksListViewKeyDown
  end
end
