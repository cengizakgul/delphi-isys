unit rwPropertiesEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls, rwExtendedControls, DBCGrids, TypInfo, Grids,
  rwPropertyEditorFrm, rwTypes, rwMessages, Variants,
  rwCommonClasses, rwBasicClasses, rwDebugInfo, rwBasicToolsBarFrm,
  rwUtils, rwDB, Menus, rwUserPropEditFrm, rwDsgnRes, rwDsgnUtils, rwBasicUtils,
  rwEngineTypes, Buttons;

type

  TrwGridInplaceEdit = class;

 {TrwPropList is class for store current objects RTTI}

  TrwPropList = class(TStringList)
  private
    FObjects: TList;
    FPropEditor: TComponent;
    FComponentsListOwner: TrwComponent;
  public
    constructor Create;
    destructor Destroy; override;

    procedure InitList(AObject: TObject; AGroup: Boolean = False);
    procedure ChangeProperty(Index: Integer; Value: string);
    function  GetStrValue(Index: Integer): string;
    procedure PrepareEditor(Index: Integer; AEditor: TrwGridInplaceEdit);
    procedure PrepareValueList(Index: Integer; AEditor: TrwGridInplaceEdit);
    function  IsItPropListOfValues(PropInfo: PPropInfo): Boolean;
    procedure CreateListOfValues(AObject: TObject; PropInfo: PPropInfo; AList: TStrings);
  end;

 {TPopupListbox is dropdown list for TrwGridInplaceEdit}

  TrwPopupListbox = class(TCustomListbox)
  private
    FSearchText: string;
    FSearchTickCount: Longint;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure KeyPress(var Key: Char); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  end;

 {TrwGridInplaceEdit is property editor for Inspector}

  TrwEditStyle = (resSimple, resEllipsis, resPickList);

  TrwGridInplaceEdit = class(TInplaceEdit)
  private
    FActiveList: TWinControl;
    FButtonWidth: Integer;
    FPickList: TrwPopupListbox;
    FEditStyle: TrwEditStyle;
    FListVisible: Boolean;
    FTracking: Boolean;
    FPressed: Boolean;
    procedure ListMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SetEditStyle(Value: TrwEditStyle);
    procedure StopTracking;
    procedure TrackButton(X, Y: Integer);
    procedure CMCancelMode(var Message: TCMCancelMode); message CM_CancelMode;
    procedure WMCancelMode(var Message: TMessage); message WM_CancelMode;
    procedure WMKillFocus(var Message: TMessage); message WM_KillFocus;
    procedure WMLButtonDblClk(var Message: TWMLButtonDblClk); message wm_LButtonDblClk;
    procedure WMPaint(var Message: TWMPaint); message wm_Paint;
    procedure WMSetCursor(var Message: TWMSetCursor); message WM_SetCursor;
    function OverButton(const P: TPoint): Boolean;
    function ButtonRect: TRect;
    function GetListItems: TStrings;
  protected
    procedure BoundsChanged; override;
    procedure CloseUp(Accept: Boolean);
    procedure DoDropDownKeys(var Key: Word; Shift: TShiftState);
    procedure DropDown;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure PaintWindow(DC: HDC); override;
    procedure WndProc(var Message: TMessage); override;
    property PickList: TrwPopupListbox read FPickList;
    property ActiveList: TWinControl read FActiveList write FActiveList;
    procedure CreateParams(var Params: TCreateParams); override;
  public
    property EditStyle: TrwEditStyle read FEditStyle write SetEditStyle;
    property ListItems: TStrings read GetListItems;

    constructor Create(Owner: TComponent); override;
  end;

 {TrwPropertiesPanel is panel for property list}

  TrwPropertiesGrid = class(TStringGrid)
  private
    FPropList: TrwPropList;
    FCellSize: Integer;
    FInitialization: Boolean;
    FReadOnly: Boolean;

    procedure SelectCell(Sender: TObject; ACol, ARow: Longint; var CanSelect: Boolean); reintroduce;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
    procedure WMCopy(var Msg: TMessage); message WM_COPY;
    procedure WMCut(var Msg: TMessage); message WM_CUT;
    procedure WMPaste(var Msg: TMessage); message WM_PASTE;

  protected
    function  CreateEditor: TInplaceEdit; override;
    procedure ChangePropertyValue;
    procedure EditButtonClick;
    procedure EditProperty(AObject: TObject; APropName: String);
    procedure BeforeDropDownList;
    function  PropertyIndex(APropName: string): Integer;
    function  CanEditModify: Boolean; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure CreatePropertyList(AComponent: TrwComponent; AGroup: Boolean = False);
    procedure RefreshPropertyList;
    property  ReadOnly: Boolean read FReadOnly write FReadOnly;

  end;

  {TrwPropertiesEditor is like Delphi Object Inspector}

  TOnPropertyChange = procedure(Sender: TObject; AComponent: TrwComponent) of object;

  TrwPropertiesEditor = class(TFrame)
    pgcInspector: TPageControl;
    tsProperties: TTabSheet;
    tbsObjTree: TTabSheet;
    tsEvents: TTabSheet;
    lbEvents: TListBox;
    tvObjTree: TTreeView;
    pmUserEvent: TPopupMenu;
    miRegUserEvent: TMenuItem;
    miUnRegUserEvent: TMenuItem;
    pmUserProps: TPopupMenu;
    miRegUserProp: TMenuItem;
    miUnRegUserProp: TMenuItem;
    Panel1: TPanel;
    pnlCompName: TPanel;
    Panel2: TPanel;
    btnPropRevert: TSpeedButton;
    procedure lbEventsClick(Sender: TObject);
    procedure tvObjTreeChange(Sender: TObject; Node: TTreeNode);
    procedure tvObjTreeDblClick(Sender: TObject);
    procedure miRegUserEventClick(Sender: TObject);
    procedure miUnRegUserEventClick(Sender: TObject);
    procedure miRegUserPropClick(Sender: TObject);
    procedure miUnRegUserPropClick(Sender: TObject);
    procedure pgcInspectorChange(Sender: TObject);
    procedure btnPropRevertClick(Sender: TObject);

  private
    FGroup: Boolean;
    FPropertiesGrid: TrwPropertiesGrid;
    FOnEventChange: TNotifyEvent;
    FCurrentComponent: TrwComponent;
    FCurrentEventName: string;
    FOnPropertyChange: TOnPropertyChange;
    FOnComponentChange: TOnPropertyChange;
    FCurrentCompPath: string;
    FReadOnly: Boolean;

    function  FindComponentInTree(const AComponent: TrwComponent): TTreeNode; overload;
    function  FindComponentInTree(const AComponentPath: String): TTreeNode; overload;
    procedure AddChildren(const AOwner: TrwComponent; const AToolBar: TrwBasicToolsBar);
    procedure SetCurrentComponent(const Value: TrwComponent);
    procedure UpdateComponentNamePanel;
    procedure SetReadOnly(const Value: Boolean);

  public
    property ReadOnly: Boolean read FReadOnly write SetReadOnly;
    property OnEventChange: TNotifyEvent read FOnEventChange write FOnEventChange;
    property CurrentComponent: TrwComponent read FCurrentComponent write SetCurrentComponent;
    property CurrentCompPath:  string read FCurrentCompPath;
    property CurrentEventName: string read FCurrentEventName write FCurrentEventName;
    property OnPropertyChange: TOnPropertyChange read FOnPropertyChange write FOnPropertyChange;
    property OnComponentChange: TOnPropertyChange read FOnComponentChange write FOnComponentChange;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AddNewComponentToComponentsList(const AComponent: TrwComponent; AToolBar: TrwBasicToolsBar;
       const AGroup: Boolean = False);
    procedure DeleteComponentFromComponentsList(AComponent: TrwComponent);
    procedure CreateComponentsList(AOwner: TrwComponent; AToolBar: TrwBasicToolsBar);
    procedure ShowComponentProperties(AComponent: TrwComponent; AGroup: Boolean = False);
    procedure EditProperty(AComponent: TrwComponent; APropName: string);
    procedure ShowComponentEvent(AComponentPath: string; AEventName: string);
    procedure RefreshComponentProperties;
    procedure RefreshComponentEvents;
  end;

implementation

{$R *.DFM}

uses rwDesignerFrm, rwReport,
     rwBandsPropertyEditorFrm, rwColorPropertyEditorFrm,
     rwDateTimePropertyEditorFrm, rwFieldsPropertyEditorFrm,
     rwFontPropertyEditorFrm, rwGlobalVariablesPropertyEditorFrm,
     rwPagePropertyEditorFrm, rwPCLFilePropertyEditorFrm,
     rwPicturePropertyEditorFrm, rwQBQueryPropertyEditorFrm,
     rwSetPropertyEditorFrm, rwStringsPropertyEditorFrm,
     rwVariantPropertyEditorFrm, rwBorderLinesPropertyEditorFrm;

const
  cDropDownRows = 8;



 {TrwPropList}


constructor TrwPropList.Create;
begin
  inherited Create;

  FObjects := TList.Create;
end;


destructor TrwPropList.Destroy;
begin
  FObjects.Free;

  inherited;
end;


procedure TrwPropList.InitList(AObject: TObject; AGroup: Boolean = False);
var
  ClassTypeInfo: PTypeInfo;
  ClassTypeData: PTypeData;
  PropList: PPropList;
  UProps: TrwUserProperties;

  procedure FillList(AUserProps: Boolean; ACount: Integer);
  var
    i, j: Integer;
    fl_exists: Boolean;    
  begin
    if AGroup then
    begin
      j := 0;
      while j < Count do
      begin
        fl_exists := False;
        for i := 0 to ACount - 1 do
          if (PropList[i]^.PropType^.Kind <> tkMethod) and (PropList[i]^.Name[1] <> '_') and
            (PropList[i]^.Name = Strings[j]) and
            (AUserProps or IsStoredProp(AObject, PropList[i]) or (UpperCase(PropList[i]^.Name) = 'NAME')) then
          begin
            fl_exists := True;
            break;
          end;

        if not fl_exists or (Strings[j] = 'Name') then
          Delete(j)
        else
          Inc(j);
      end;
    end

    else
      for i := 0 to ACount - 1 do
        if (PropList[i]^.PropType^.Kind <> tkMethod) and (PropList[i]^.Name[1] <> '_') and
           (AUserProps or IsStoredProp(AObject, PropList[i]) or (UpperCase(PropList[i]^.Name) = 'NAME')) then
          AddObject(PropList[i]^.Name, Pointer(PropList[i]));
  end;


begin
  if not AGroup then
  begin
    Clear;
    FObjects.Clear;
  end;

  if not Assigned(AObject) then
  begin
     FObjects.Clear;
     Exit;
  end;

  FObjects.Add(AObject);

  ClassTypeInfo := AObject.ClassInfo;
  ClassTypeData := GetTypeData(ClassTypeInfo);

  if (ClassTypeData.PropCount <> 0) then
  begin
    GetMem(PropList, SizeOf(PPropInfo) * ClassTypeData.PropCount);

    try
      GetPropInfos(AObject.ClassInfo, PropList);
      FillList(False, ClassTypeData.PropCount);
    finally
      FreeMem(PropList, SizeOf(PPropInfo) * ClassTypeData.PropCount);
    end;
  end;


// User defined properties
  if (AObject is TrwComponent) and (TrwComponent(AObject).UserPropertyIsNotEmpty) then
  begin
    UProps := TrwComponent(AObject)._UserProperties;
    if UProps.Count > 0 then
    begin
      GetMem(PropList, SizeOf(PPropInfo) * UProps.Count);

      try
        GetUserPropInfos(UProps, PropList);
        FillList(True, UProps.Count);
      finally
        FreeMem(PropList, SizeOf(PPropInfo) * UProps.Count);
      end;
    end;
  end;
end;

function TrwPropList.GetStrValue(Index: Integer): string;
var
  PropInfo: PPropInfo;
  PropObj: TObject;
  ClassProp: TClass;
  Val: Variant;
  h: String;
  i, j: Integer;
  d: TDateTime;

  function GetUserPropValue(AObj: TObject): Variant;
  begin
    try
      Result := TrwComponent(AObj)._UserProperties[j].Value;
    except
      on E: Exception do
      begin
        Result := 0;
        ShowMessage(E.Message);
      end;
    end;
  end;

  function lGetOrdProp(AObj: TObject; APropInfo: PPropInfo): Integer;
  begin
    if j = -1 then
      Result := GetOrdProp(AObj, APropInfo)
    else
      Result := GetUserPropValue(AObj);
  end;

  function lGetEnumProp(AObj: TObject; APropInfo: PPropInfo): String;
  begin
    if j = -1 then
      Result := GetEnumProp(AObj, APropInfo)
    else
      Result := GetUserPropValue(AObj);
  end;

  function lGetFloatProp(AObj: TObject; APropInfo: PPropInfo): Extended;
  begin
    if j = -1 then
      Result := GetFloatProp(AObj, APropInfo)
    else
      Result := GetUserPropValue(AObj);
  end;

  function lGetStrProp(AObj: TObject; APropInfo: PPropInfo): String;
  begin
    if j = -1 then
      Result := GetStrProp(AObj, APropInfo)
    else
      Result := GetUserPropValue(AObj);
  end;

  function lGetVariantProp(AObj: TObject; APropInfo: PPropInfo): Variant;
  begin
    if j = -1 then
      Result := GetVariantProp(AObj, APropInfo)
    else
      Result := GetUserPropValue(AObj);
  end;

  function lGetSetProp(AObj: TObject; APropName: String): String;
  begin
    if j = -1 then
      Result := GetSetProp(AObj, APropName, True)
    else
      Result := GetUserPropValue(AObj);
  end;

  function lGetObjectProp(AObj: TObject; APropInfo: PPropInfo): TObject;
  begin
    if j = -1 then
      Result := GetObjectProp(AObj, APropInfo)
    else
      Result := Pointer(Integer(GetUserPropValue(AObj)));
  end;

begin
  Result := '';

  for i := 0 to FObjects.Count-1 do
  begin
    PropInfo := GetPropInfo(TObject(FObjects[i]).ClassInfo, Strings[Index]);

    if not Assigned(PropInfo) then
    begin
      j := TrwComponent(FObjects[i])._UserProperties.IndexOf(Strings[Index]);
      PropInfo := TrwComponent(FObjects[i])._UserProperties[j].PPropInfo;
    end
    else
      j := -1;

    h := '';

    case PropInfo^.PropType^.Kind of
      tkInteger:
        if (AnsiUpperCase(PropInfo.Name) = 'LEFT') or
          (AnsiUpperCase(PropInfo.Name) = 'TOP') or
          (AnsiUpperCase(PropInfo.Name) = 'WIDTH') or
          (AnsiUpperCase(PropInfo.Name) = 'HEIGHT') or
          (AnsiUpperCase(PropInfo.Name) = 'BOUNDLINEWIDTH') then

            h := FloatToStr(Round(ConvertUnit(lGetOrdProp(TObject(FObjects[i]), PropInfo),
              utScreenPixels, DesignUnits)*1000)/1000)
        else
          h := IntToStr(lGetOrdProp(TObject(FObjects[i]), PropInfo));

      tkEnumeration: h := lGetEnumProp(TObject(FObjects[i]), PropInfo);

      tkFloat:
        begin
          if PropInfo^.PropType^.Name = 'TDateTime' then
          begin
            d := lGetFloatProp(TObject(FObjects[i]), PropInfo);
            if d - Int(d) <> 0 then
              h := DateTimeToStr(d)
            else
              h := DateToStr(d);
          end
          else
            h := FloatToStr(lGetFloatProp(TObject(FObjects[i]), PropInfo));
        end;

      tkChar,
        tkWChar,
        tkWString,
        tklString,
        tkString: h := lGetStrProp(TObject(FObjects[i]), PropInfo);

      tkVariant:
        begin
          Val := lGetVariantProp(TObject(FObjects[i]), PropInfo);
          if VarIsNull(Val) or VarIsEmpty(Val) then
            h := ''
          else
            h := VarAsType(Val, varString);
        end;

      tkSet: h := lGetSetProp(TObject(FObjects[i]), PropInfo^.Name);

      tkClass:
        begin
          if j = -1 then
            ClassProp := GetTypeData(PropInfo^.PropType^).ClassType
          else
            ClassProp := TrwComponent(FObjects[i])._UserProperties[j].GetPointerClass;

          if (ClassProp.InheritsFrom(TComponent)) then
          begin
            PropObj := lGetObjectProp(TObject(FObjects[i]), PropInfo);
            if Assigned(PropObj) then
              h := TComponent(PropObj).Name
            else
              h := ''
          end

          else
          begin
            if j = -1 then
              h := h + ClassProp.ClassName
            else
              h := TrwComponent(FObjects[i])._UserProperties[j].PointerType;
            h := '(' + h + ')';
          end;
        end;
    end;

    if (i > 0) and (Result <> h) then
    begin
      Result := '';
      break;
    end
    else
      Result := h;
  end;
end;

procedure TrwPropList.ChangeProperty(Index: Integer; Value: string);
var
  PropInfo: PPropInfo;
  lComp: TrwComponent;
  ClassProp: TClass;
  i, j: Integer;

  procedure SetUserPropValue(AObj: TObject; AValue: Variant);
  begin
    try
      TrwComponent(AObj)._UserProperties[j].Value := AValue;
    except
      on E: Exception do
        ShowMessage(E.Message);
    end;
  end;

  procedure lSetOrdProp(AObj: TObject; APropInfo: PPropInfo; AValue: Integer);
  begin
    if j = -1 then
      SetOrdProp(AObj, APropInfo, AValue)
    else
      SetUserPropValue(AObj, AValue);
  end;

  procedure lSetEnumProp(AObj: TObject; APropInfo: PPropInfo; AValue: String);
  begin
    if j = -1 then
      SetEnumProp(AObj, APropInfo, AValue)
    else
      SetUserPropValue(AObj, AValue);
  end;

  procedure lSetFloatProp(AObj: TObject; APropInfo: PPropInfo; AValue: Extended);
  begin
    if j = -1 then
      SetFloatProp(AObj, APropInfo, AValue)
    else
      SetUserPropValue(AObj, AValue);
  end;

  procedure lSetStrProp(AObj: TObject; APropInfo: PPropInfo; AValue: String);
  begin
    if j = -1 then
      SetStrProp(AObj, APropInfo, AValue)
    else
      SetUserPropValue(AObj, AValue);
  end;

  procedure lSetVariantProp(AObj: TObject; APropInfo: PPropInfo; AValue: Variant);
  begin
    if j = -1 then
      SetVariantProp(AObj, APropInfo, AValue)
    else
      SetUserPropValue(AObj, AValue);
  end;

  procedure lSetSetProp(AObj: TObject; APropName: String; AValue: String);
  begin
    if j = -1 then
      SetSetProp(AObj, APropName, AValue)
    else
      SetUserPropValue(AObj, AValue);
  end;

  procedure lSetObjectProp(AObj: TObject; APropInfo: PPropInfo; AValue: TObject);
  begin
    if j = -1 then
      SetObjectProp(AObj, APropInfo, AValue)
    else
      SetUserPropValue(AObj, Integer(Pointer(AValue)));
  end;

begin
  for i := 0 to FObjects.Count-1 do
  begin
    PropInfo := GetPropInfo(TObject(FObjects[i]).ClassInfo, Strings[Index]);

    if not Assigned(PropInfo) then
    begin
      j := TrwComponent(FObjects[i])._UserProperties.IndexOf(Strings[Index]);
      PropInfo := TrwComponent(FObjects[i])._UserProperties[j].PPropInfo;
    end
    else
    begin
      if not Assigned(PropInfo^.SetProc) then
        Continue;
      j := -1;
    end;

    case PropInfo^.PropType^.Kind of
      tkInteger:
        if (AnsiUpperCase(PropInfo.Name) = 'LEFT') or
          (AnsiUpperCase(PropInfo.Name) = 'TOP') or
          (AnsiUpperCase(PropInfo.Name) = 'WIDTH') or
          (AnsiUpperCase(PropInfo.Name) = 'HEIGHT') or
          (AnsiUpperCase(PropInfo.Name) = 'BOUNDLINEWIDTH') then

          lSetOrdProp(TObject(FObjects[i]), PropInfo,
            Round(ConvertUnit(StrToFloat(Value), DesignUnits,
            utScreenPixels)))
        else
          lSetOrdProp(TObject(FObjects[i]), PropInfo, StrToInt(Value));

      tkEnumeration: lSetEnumProp(TObject(FObjects[i]), PropInfo, Value);

      tkFloat:
        begin
          if PropInfo^.PropType^.Name = 'TDateTime' then
            lSetFloatProp(TObject(FObjects[i]), PropInfo, StrToDateTime(Value))          
          else
            lSetFloatProp(TObject(FObjects[i]), PropInfo, StrToFloat(Value));
        end;

      tkChar,
        tkWChar,
        tkWString,
        tklString,
        tkString: lSetStrProp(TObject(FObjects[i]), PropInfo, Value);

      tkVariant: lSetVariantProp(TObject(FObjects[i]), PropInfo, Value);

      tkSet: lSetSetProp(TObject(FObjects[i]), PropInfo^.Name, Value);

      tkClass:
        begin
          if j = -1 then
            ClassProp := GetTypeData(PropInfo^.PropType^).ClassType
          else
            ClassProp := TrwComponent(FObjects[i])._UserProperties[j].GetPointerClass;

          if (ClassProp.InheritsFrom(TComponent)) then
          begin
            lComp := TrwComponent(Pointer(Integer(FComponentsListOwner.PFindComponent(Value))));
            lSetOrdProp(TObject(FObjects[i]), PropInfo, LongInt(lComp));
          end;
        end;
    end;
  end;
end;

procedure TrwPropList.PrepareEditor(Index: Integer; AEditor: TrwGridInplaceEdit);
var
  PropInfo: PPropInfo;
  ClassProp: TClass;
  h: string;
  j: Integer;
begin
  if (FObjects.Count = 0) or (Count <= Index) then
    Exit;

  PropInfo := GetPropInfo(TObject(FObjects[0]).ClassInfo, Strings[Index]);

  if not Assigned(PropInfo) then
  begin
    j := TrwComponent(FObjects[0])._UserProperties.IndexOf(Strings[Index]);
    PropInfo := TrwComponent(FObjects[0])._UserProperties[j].PPropInfo;
  end
  else
    j := -1;

  AEditor.Tag := 1;

  case PropInfo^.PropType^.Kind of

      tkInteger,
      tkFloat,
      tkChar,
      tkWChar,
      tkWString,
      tklString,
      tkVariant,
      tkString:
      begin
        h := PropInfo^.PropType^.Name;
        if h[1] = 'T' then
          System.Delete(h, 1, 1);
        if (Copy(h, 1, 2) = 'rw') then
          System.Delete(h, 1, 2);
        if (GetClass('Trw' + h + 'PropertyEditor') <> nil) then
        begin
          if not(AnsiSameText(PropInfo^.PropType^.Name, 'TColor') or
                 AnsiSameText(PropInfo^.PropType^.Name, 'TDateTime') or
                 AnsiSameText(PropInfo^.PropType^.Name, 'Variant')) then
            AEditor.Tag := 0;
          AEditor.EditStyle := resEllipsis;
          Exit;
        end;

        if IsItPropListOfValues(PropInfo) then
          AEditor.EditStyle := resPickList
        else
          if PropInfo^.PropType^.Kind = tkWString then
            AEditor.EditStyle := resEllipsis
          else if (TObject(FObjects[0]) is TrwParamFormControl) and (Strings[Index] = 'ParamName') or
            (TObject(FObjects[0]) is TrwCustomObject) and (Strings[Index] = 'ObjectClassName') then
            AEditor.EditStyle := resPickList
          else
            AEditor.EditStyle := resSimple;
      end;

    tkSet: begin
             AEditor.Tag := 0;
             AEditor.EditStyle := resEllipsis;
           end;

    tkEnumeration: AEditor.EditStyle := resPickList;

    tkClass:
      begin
        if j = -1 then
          ClassProp := GetTypeData(PropInfo^.PropType^).ClassType
        else
          ClassProp := TrwComponent(FObjects[0])._UserProperties[j].GetPointerClass;
        if (ClassProp.InheritsFrom(TComponent)) then
          AEditor.EditStyle := resPickList
        else
        begin
          AEditor.Tag := 0;
          AEditor.EditStyle := resEllipsis;
        end;
      end;
  end;

  AEditor.Modified := False;
end;

procedure TrwPropList.PrepareValueList(Index: Integer; AEditor: TrwGridInplaceEdit);
var
  PropInfo: PPropInfo;
  ClassProp: TClass;
  i, j: Integer;
  OrdTypeData: PTypeData;
  MinVal, MaxVal: Integer;
  V: TrwGlobalVarFunct;
begin
  AEditor.PickList.Sorted := False;
  
  PropInfo := GetPropInfo(TObject(FObjects[0]).ClassInfo, Strings[Index]);

  if not Assigned(PropInfo) then
  begin
    j := TrwComponent(FObjects[0])._UserProperties.IndexOf(Strings[Index]);
    PropInfo := TrwComponent(FObjects[0])._UserProperties[j].PPropInfo;
  end
  else
    j := -1;

  if (TObject(FObjects[0]) is TrwParamFormControl) and (Strings[Index] = 'ParamName') then
  begin
    AEditor.ListItems.Clear;
    V := TrwComponent(TrwParamFormControl(FObjects[0]).Owner).GlobalVarFunc;
    if Assigned(V) then
      for i := 0 to V.Variables.Count-1 do
        AEditor.ListItems.AddObject(V.Variables[i].Name, V.Variables[i]);
  end

  else if (TObject(FObjects[0]) is TrwCustomObject) and (Strings[Index] = 'ObjectClassName') then
  begin
    AEditor.ListItems.Clear;
    for i := 0 to RWClassList.Count - 1 do
      AEditor.ListItems.AddObject(TPersistentClass(RWClassList[i]).ClassName, nil);
    for i := 0 to SystemLibComponents.Count - 1 do
      AEditor.ListItems.AddObject(SystemLibComponents[i].Name, nil);
  end

  else
    case PropInfo^.PropType^.Kind of
      tkWString,
        tklString,
        tkString: CreateListOfValues(TObject(FObjects[0]), PropInfo, AEditor.ListItems);

      tkSet: ;

      tkEnumeration:
        begin
          AEditor.ListItems.Clear;

          OrdTypeData := GetTypeData(PropInfo^.PropType^);
          MinVal := OrdTypeData^.MinValue;
          MaxVal := OrdTypeData^.MaxValue;

          for i := MinVal to MaxVal do
            AEditor.ListItems.AddObject(GetEnumName(PropInfo^.PropType^, i), Pointer(i));
        end;

      tkClass:
        begin
          if j = -1 then
            ClassProp := GetTypeData(PropInfo^.PropType^).ClassType
          else
            ClassProp := TrwComponent(FObjects[0])._UserProperties[j].GetPointerClass;

          if (ClassProp.InheritsFrom(TComponent)) then
          begin
            AEditor.ListItems.Clear;

            for i := 0 to FComponentsListOwner.VirtualComponentCount - 1 do
              if (FComponentsListOwner.GetVirtualChild(i).InheritsFrom(ClassProp)) then
                AEditor.ListItems.AddObject(FComponentsListOwner.GetVirtualChild(i).Name,
                  FComponentsListOwner.GetVirtualChild(i));
          end

          else
            CreateListOfValues(TObject(FObjects[0]), PropInfo, AEditor.ListItems);
        end;

    end;

  AEditor.PickList.Sorted := True;
end;

procedure TrwPropList.CreateListOfValues(AObject: TObject; PropInfo: PPropInfo; AList: TStrings);
var
  lObj: TObject;
begin
  AList.Clear;

  if (PropInfo^.Name = 'DataField') and IsPublishedProp(AObject, 'Datasource') then
  begin
    lObj := GetObjectProp(AObject, 'Datasource');

    if Assigned(lObj) then
      TrwDataSet(lObj).GetFieldList(AList);
  end;
end;


function TrwPropList.IsItPropListOfValues(PropInfo: PPropInfo): Boolean;
begin
  if (PropInfo^.Name = 'DataField') then
    Result := True
  else
    Result := False;
end;



{TrwPopupListBox}

procedure TrwPopupListBox.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  with Params do
  begin
    Style := Style or WS_BORDER;
    ExStyle := WS_EX_TOOLWINDOW or WS_EX_TOPMOST;
    AddBiDiModeExStyle(ExStyle);
    WindowClass.Style := CS_SAVEBITS;
  end;
end;

procedure TrwPopupListbox.CreateWnd;
begin
  inherited CreateWnd;
  Windows.SetParent(Handle, 0);
  CallWindowProc(DefWndProc, Handle, wm_SetFocus, 0, 0);
end;

procedure TrwPopupListbox.Keypress(var Key: Char);
var
  TickCount: Integer;
begin
  case Key of
    #8, #27: FSearchText := '';
    #32..#255:
      begin
        TickCount := GetTickCount;
        if TickCount - FSearchTickCount > 2000 then FSearchText := '';
        FSearchTickCount := TickCount;
        if Length(FSearchText) < 32 then FSearchText := FSearchText + Key;
        SendMessage(Handle, LB_SelectString, WORD(-1), Longint(PChar(FSearchText)));
        Key := #0;
      end;
  end;
  inherited Keypress(Key);
end;

procedure TrwPopupListbox.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited MouseUp(Button, Shift, X, Y);
  TrwGridInPlaceEdit(Owner).CloseUp((X >= 0) and (Y >= 0) and
    (X < Width) and (Y < Height));
end;

  {TrwInplaceEdit}

constructor TrwGridInplaceEdit.Create(Owner: TComponent);
begin
  inherited Create(Owner);
  FButtonWidth := GetSystemMetrics(SM_CXVSCROLL);
  FEditStyle := resSimple;
end;

procedure TrwGridInplaceEdit.BoundsChanged;
var
  R: TRect;
begin
  Top := Top-1;
  Left := Left-1;
  Height := Height+2;
  Width := Width+2;
  SetRect(R, 1, 1, Width - 4, Height);
  if FEditStyle <> resSimple then
    if not TCustomGrid(Owner).UseRightToLeftAlignment then
      Dec(R.Right, FButtonWidth)
    else
      Inc(R.Left, FButtonWidth - 1);
  SendMessage(Handle, EM_SETRECTNP, 0, LongInt(@R));
  SendMessage(Handle, EM_SCROLLCARET, 0, 0);
  if SysLocale.FarEast then
    SetImeCompositionWindow(Font, R.Left, R.Top);
end;

procedure TrwGridInplaceEdit.CloseUp(Accept: Boolean);
var
  ListValue: Variant;
begin
  if FListVisible then
  begin
    if GetCapture <> 0 then SendMessage(GetCapture, WM_CANCELMODE, 0, 0);
    if FPickList.ItemIndex <> -1 then
      ListValue := FPickList.Items[FPicklist.ItemIndex];
    SetWindowPos(FActiveList.Handle, 0, 0, 0, 0, 0, SWP_NOZORDER or
      SWP_NOMOVE or SWP_NOSIZE or SWP_NOACTIVATE or SWP_HIDEWINDOW);
    FListVisible := False;
    Invalidate;
    if Accept then
      if (not VarIsNull(ListValue)) and EditCanModify then
      begin
        Text := ListValue;
        TrwPropertiesGrid(Grid).ChangePropertyValue;
      end;
  end;
end;

procedure TrwGridInplaceEdit.DoDropDownKeys(var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_UP, VK_DOWN:
      if ssAlt in Shift then
      begin
        if FListVisible then
          CloseUp(True)
        else
          DropDown;
        Key := 0;
      end;
    VK_RETURN, VK_ESCAPE:
      if FListVisible and not (ssAlt in Shift) then
      begin
        CloseUp(Key = VK_RETURN);
        Key := 0;
      end;
  end;
end;

procedure TrwGridInplaceEdit.DropDown;
var
  P: TPoint;
  I, J, Y: Integer;
begin
  if not FListVisible and Assigned(FActiveList) then
  begin
    TrwPropertiesGrid(Grid).BeforeDropDownList;
    FActiveList.Width := Width-1;
    FPickList.Color := Color;
    FPickList.Font := Font;
    if FPickList.Items.Count >= cDropDownRows then
      FPickList.Height := cDropDownRows * FPickList.ItemHeight + 4
    else
      FPickList.Height := FPickList.Items.Count * FPickList.ItemHeight + 4;

//?
{    if Column.Field.IsNull then
      FPickList.ItemIndex := -1
    else
      FPickList.ItemIndex := FPickList.Items.IndexOf(Column.Field.Text);}
    J := FPickList.ClientWidth;
    for I := 0 to FPickList.Items.Count - 1 do
    begin
      Y := FPickList.Canvas.TextWidth(FPickList.Items[I]);
      if Y > J then J := Y;
    end;

    FPickList.ClientWidth := J;
    P := Parent.ClientToScreen(Point(Left, Top));
    Y := P.Y + Height;
    if Y + FActiveList.Height > Screen.Height then Y := P.Y - FActiveList.Height;
    SetWindowPos(FActiveList.Handle, HWND_TOP, P.X, Y, 0, 0,
      SWP_NOSIZE or SWP_NOACTIVATE or SWP_SHOWWINDOW);
    FListVisible := True;
    Invalidate;
    Windows.SetFocus(Handle);
  end;
end;

type
  TWinControlCracker = class(TWinControl)
  end;

procedure TrwGridInplaceEdit.KeyDown(var Key: Word; Shift: TShiftState);
var
  h: string;
begin
  if Key = VK_RETURN then
    TrwPropertiesGrid(Grid).ChangePropertyValue

  else if Key = VK_ESCAPE then
  begin
    Text := TrwPropertiesGrid(Grid).FPropList.GetStrValue(TrwPropertiesGrid(Grid).Row);
    Modified := False;
  end

  else if (Shift = []) and (Key = VK_F1) then
  begin
    h := TrwPropertiesGrid(Grid).Cells[0, TrwPropertiesGrid(Grid).Row] + #0;
    Application.HelpCommand(HELP_KEY, Integer(PChar(@h[1])));
  end

  else
    inherited KeyDown(Key, Shift);
end;

procedure TrwGridInplaceEdit.ListMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then
    CloseUp(PtInRect(FActiveList.ClientRect, Point(X, Y)));
end;

procedure TrwGridInplaceEdit.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  if (Button = mbLeft) and (FEditStyle <> resSimple) and
    OverButton(Point(X, Y)) then
  begin
    if FListVisible then
      CloseUp(False)
    else
    begin
      MouseCapture := True;
      FTracking := True;
      TrackButton(X, Y);
      if Assigned(FActiveList) then
        DropDown;
    end;
  end;
  inherited MouseDown(Button, Shift, X, Y);
end;

procedure TrwGridInplaceEdit.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  ListPos: TPoint;
  MousePos: TSmallPoint;
begin
  if FTracking then
  begin
    TrackButton(X, Y);
    if FListVisible then
    begin
      ListPos := FActiveList.ScreenToClient(ClientToScreen(Point(X, Y)));
      if PtInRect(FActiveList.ClientRect, ListPos) then
      begin
        StopTracking;
        MousePos := PointToSmallPoint(ListPos);
        SendMessage(FActiveList.Handle, WM_LBUTTONDOWN, 0, Integer(MousePos));
        Exit;
      end;
    end;
  end;
  inherited MouseMove(Shift, X, Y);
end;

procedure TrwGridInplaceEdit.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
var
  WasPressed: Boolean;
begin
  WasPressed := FPressed;
  StopTracking;

  if (Button = mbLeft) and (FEditStyle = resEllipsis) and WasPressed then
    TrwPropertiesGrid(Grid).EditButtonClick;

  inherited MouseUp(Button, Shift, X, Y);
end;

procedure TrwGridInplaceEdit.PaintWindow(DC: HDC);
var
  R: TRect;
  Flags: Integer;
  W, X, Y: Integer;
begin
  if FEditStyle <> resSimple then
  begin
    R := ButtonRect;
    Flags := 0;
    if FEditStyle in [resPickList] then
    begin
      if FActiveList = nil then
        Flags := DFCS_INACTIVE
      else
        if FPressed then
        Flags := DFCS_FLAT or DFCS_PUSHED;
      DrawFrameControl(DC, R, DFC_SCROLL, Flags or DFCS_SCROLLCOMBOBOX);
    end
    else { esEllipsis }
    begin
      if FPressed then Flags := BF_FLAT;
      DrawEdge(DC, R, EDGE_RAISED, BF_RECT or BF_MIDDLE or Flags);
      X := R.Left + ((R.Right - R.Left) shr 1) - 1 + Ord(FPressed);
      Y := R.Top + ((R.Bottom - R.Top) shr 1) - 1 + Ord(FPressed);
      W := FButtonWidth shr 3;
      if W = 0 then W := 1;
      PatBlt(DC, X, Y, W, W, BLACKNESS);
      PatBlt(DC, X - (W * 2), Y, W, W, BLACKNESS);
      PatBlt(DC, X + (W * 2), Y, W, W, BLACKNESS);
    end;
    ExcludeClipRect(DC, R.Left, R.Top, R.Right, R.Bottom);
  end;
  inherited PaintWindow(DC);
end;

procedure TrwGridInplaceEdit.SetEditStyle(Value: TrwEditStyle);
begin
  if Assigned(FPickList) then
    FPickList.Clear;

  if (Value = resEllipsis) and (Tag = 0) then
    ReadOnly := True
  else
    ReadOnly := False;

  if Value = FEditStyle then Exit;

  FEditStyle := Value;
  case Value of
    resPickList:
      begin
        if FPickList = nil then
        begin
          FPickList := TrwPopupListbox.Create(Self);
          FPickList.Visible := False;
          FPickList.Parent := Self;
          FPickList.OnMouseUp := ListMouseUp;
          FPickList.IntegralHeight := True;
          FPickList.ItemHeight := 11;
        end;
        FActiveList := FPickList;
      end;
  else { cbsNone, cbsEllipsis, or read only field }
    FActiveList := nil;
  end;

  Repaint;
end;

procedure TrwGridInplaceEdit.StopTracking;
begin
  if FTracking then
  begin
    TrackButton(-1, -1);
    FTracking := False;
    MouseCapture := False;
  end;
end;

procedure TrwGridInplaceEdit.TrackButton(X, Y: Integer);
var
  NewState: Boolean;
  R: TRect;
begin
  R := ButtonRect;
  NewState := PtInRect(R, Point(X, Y));
  if FPressed <> NewState then
  begin
    FPressed := NewState;
    InvalidateRect(Handle, @R, False);
  end;
end;

procedure TrwGridInplaceEdit.CMCancelMode(var Message: TCMCancelMode);
begin
  if (Message.Sender <> Self) and (Message.Sender <> FActiveList) then
    CloseUp(False);
end;

procedure TrwGridInplaceEdit.WMCancelMode(var Message: TMessage);
begin
  StopTracking;
  inherited;
end;

procedure TrwGridInplaceEdit.WMKillFocus(var Message: TMessage);
begin
  if not SysLocale.FarEast then
    inherited
  else
  begin
    ImeName := Screen.DefaultIme;
    ImeMode := imDontCare;
    inherited;
    if HWND(Message.WParam) <> TCustomGrid(Grid).Handle then
      ActivateKeyboardLayout(Screen.DefaultKbLayout, KLF_ACTIVATE);
  end;
  CloseUp(False);

  if Modified then
  begin
    Enabled := False;
    try
      TrwPropertiesGrid(Grid).ChangePropertyValue;
    finally
      Enabled := True;
      if TrwPropertiesGrid(Grid).CanFocus then
        TrwPropertiesGrid(Grid).SetFocus;
    end;  
  end;
end;

function TrwGridInplaceEdit.ButtonRect: TRect;
begin
  if not TCustomGrid(Owner).UseRightToLeftAlignment then
    Result := Rect(Width - FButtonWidth-4, 0, Width-3, Height-4)
  else
    Result := Rect(0, 0, FButtonWidth, Height);
end;

function TrwGridInplaceEdit.OverButton(const P: TPoint): Boolean;
begin
  Result := PtInRect(ButtonRect, P);
end;

procedure TrwGridInplaceEdit.WMLButtonDblClk(var Message: TWMLButtonDblClk);
var
  i: Integer;
begin
  with Message do
    if (FEditStyle <> resSimple) and OverButton(Point(XPos, YPos)) or not TrwPropertiesGrid(Owner).CanEditModify then
      Exit
    else  if (FEditStyle = resPickList) and not OverButton(Point(XPos, YPos)) then
    begin
      if FPickList.Items.Count = 0 then
        TrwPropertiesGrid(Grid).BeforeDropDownList;

      if FPickList.Items.Count = 0 then
        Exit;

      i := FPickList.Items.IndexOf(Text);

      if (i = FPickList.Items.Count-1) or (i = -1) then
        FPickList.ItemIndex := 0
      else
        FPickList.ItemIndex := i+1;

      Text := FPickList.Items[FPicklist.ItemIndex];
      TrwPropertiesGrid(Grid).ChangePropertyValue;
    end;

  inherited;
end;

procedure TrwGridInplaceEdit.WMPaint(var Message: TWMPaint);
begin
  PaintHandler(Message);
end;

procedure TrwGridInplaceEdit.WMSetCursor(var Message: TWMSetCursor);
var
  P: TPoint;
begin
  GetCursorPos(P);
  P := ScreenToClient(P);
  if (FEditStyle <> resSimple) and OverButton(P) then
    Windows.SetCursor(LoadCursor(0, idc_Arrow))
  else
    inherited;
end;

procedure TrwGridInplaceEdit.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  with Params do
  begin
//    Style := Style and not WS_BORDER;
    ExStyle := ExStyle or WS_EX_CLIENTEDGE;
  end;
end;


procedure TrwGridInplaceEdit.WndProc(var Message: TMessage);
begin
  case Message.Msg of
    wm_KeyDown, wm_SysKeyDown, wm_Char:
      if EditStyle in [resPickList] then
        with TWMKey(Message) do
        begin
          DoDropDownKeys(CharCode, KeyDataToShiftState(KeyData));
          if (CharCode <> 0) and FListVisible then
          begin
            with TMessage(Message) do
              SendMessage(FActiveList.Handle, Msg, WParam, LParam);
            Exit;
          end;
        end
  end;
  inherited;
end;

function TrwGridInplaceEdit.GetListItems: TStrings;
begin
  Result := FPickList.Items;
end;

  {TrwPropertiesGrid}

constructor TrwPropertiesGrid.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Options := Options - [goRangeSelect] + [goEditing, goAlwaysShowEditor];
  FCellSize := 18;
  RowCount := 1;
  ColCount := 2;
  RowHeights[0] := FCellSize;
  Color := clSilver; 

  FPropList := TrwPropList.Create;
  FPropList.FPropEditor := AOwner;
  FPropList.Sorted := True;

  OnSelectCell := SelectCell;
end;

destructor TrwPropertiesGrid.Destroy;
begin
  FPropList.Free;

  inherited;
end;

procedure TrwPropertiesGrid.RefreshPropertyList;
var
  i: Integer;
begin
  for i := 0 to FPropList.Count - 1 do
    Cells[1, i] := FPropList.GetStrValue(i);
end;

procedure TrwPropertiesGrid.CreatePropertyList(AComponent: TrwComponent; AGroup: Boolean = False);
var
  i, r: Integer;
  LastProp: String;
begin
  FInitialization := True;
  try
    LastProp := Cells[0, Row];
    FPropList.InitList(AComponent, AGroup);

    RowCount := FPropList.Count;

    Cells[0, 0] := '';
    Cells[1, 0] := '';

    r := -1;
    for i := 0 to FPropList.Count - 1 do
    begin
      Cells[0, i] := FPropList[i];
      if AnsiUpperCase(LastProp) = AnsiUpperCase(Cells[0, i]) then
        r := i;
      RowHeights[i] := FCellSize;
      Cells[1, i] := FPropList.GetStrValue(i);
    end;

    if r = -1 then
      r := 0;
    if r = 0 then
    begin
      if RowCount > 1 then
        Row := 1;
    end
    else
      Row := r-1;
    Col := 0;
    Row := r;
    Col := 1;
  finally
    FInitialization := False;
  end;
end;

procedure TrwPropertiesGrid.WMSize(var Message: TWMSize);
begin
  inherited;

  ColWidths[0] := ClientWidth div 2;
  ColWidths[1] := ClientWidth - ColWidths[0] - 1;
end;

procedure TrwPropertiesGrid.WMCopy(var Msg: TMessage);
begin
  InplaceEditor.CopyToClipboard;
end;

procedure TrwPropertiesGrid.WMCut(var Msg: TMessage);
begin
  InplaceEditor.CutToClipboard;
end;

procedure TrwPropertiesGrid.WMPaste(var Msg: TMessage);
begin
  InplaceEditor.PasteFromClipboard;
end;

procedure TrwPropertiesGrid.SelectCell(Sender: TObject; ACol, ARow: Longint; var CanSelect: Boolean);
begin
  if (ACol = 0) then
  begin
    Col := 1;
    Row := ARow;
    CanSelect := False;
  end
  else
  begin
    if not FInitialization and not TrwGridInplaceEdit(InplaceEditor).ReadOnly and
       TrwGridInplaceEdit(InplaceEditor).Modified then

    ChangePropertyValue;

    if Assigned(InplaceEditor) then
      FPropList.PrepareEditor(ARow, TrwGridInplaceEdit(InplaceEditor));
  end;
end;

procedure TrwPropertiesGrid.ChangePropertyValue;
var
  i: Integer;
  h: string;
  VO: TrwComponent;
begin
  if FPropList.Count = 0 then Exit;

  try
    if AnsiUpperCase(FPropList[Row]) = 'NAME' then
      h := GetComponentPath(TrwComponent(FPropList.FObjects[0]));

    if TrwComponent(FPropList.FObjects[0]).InheritedComponent and
     (AnsiUpperCase(FPropList[Row]) = 'NAME') then
      raise ErwException.Create(ResErrInheritedChangeName);

    if (Length(InplaceEditor.Text) = 0) and
       (AnsiUpperCase(FPropList[Row]) = 'NAME') then
      raise ErwException.Create(ResErrEmptyName);

    VO := TrwComponent(FPropList.FObjects[0]).VirtualOwner;
    if TrwComponent(FPropList.FObjects[0]).InheritedComponent and Assigned(VO) and
       not (VO.IsVirtualOwner or (VO.VirtualOwner = VO)) then
      raise ErwException.Create(ResErrInheritedChange);

    if not TrwGridInplaceEdit(InplaceEditor).ReadOnly then
      FPropList.ChangeProperty(Row, InplaceEditor.Text);

    with TrwPropertiesEditor(Owner) do
      if Assigned(OnPropertyChange) then
        for i := 0 to FPropList.FObjects.Count-1 do
          OnPropertyChange(Owner, TrwComponent(FPropList.FObjects[i]));

    if InplaceEditor.CanFocus then
      InplaceEditor.SetFocus;

  finally
    InplaceEditor.Modified := False;
    Cells[1, Row] := FPropList.GetStrValue(Row);
    if AnsiUpperCase(FPropList[Row]) = 'NAME' then
    begin
      TrwPropertiesEditor(Owner).UpdateComponentNamePanel;

      DebugInfo.BreakPointsList.ChangeComponentName(h,
        GetComponentPath(TrwComponent(FPropList.FObjects[0])));

      TrwPropertiesEditor(Owner).tvObjTree.Selected.Text := Cells[1, Row];
      TrwPropertiesEditor(Owner).tvObjTree.SortType := stNone;
      TrwPropertiesEditor(Owner).tvObjTree.SortType := stText;
    end;
  end;
end;

function TrwPropertiesGrid.CreateEditor: TInplaceEdit;
begin
  Result := TrwGridInplaceEdit.Create(Self);
  FPropList.PrepareEditor(Row, TrwGridInplaceEdit(Result));
end;

procedure TrwPropertiesGrid.BeforeDropDownList;
begin
  FPropList.PrepareValueList(Row, TrwGridInplaceEdit(InplaceEditor))
end;

procedure TrwPropertiesGrid.EditButtonClick;
begin
  EditProperty(TObject(FPropList.FObjects[0]), FPropList[Row]);
end;

procedure TrwPropertiesGrid.EditProperty(AObject: TObject; APropName: String);
var
  PropInfo: PPropInfo;
  TypeData: PTypeData;
  TypeInfo: PTypeInfo;
  ClassProp: TClass;
  h: string;
  lPropEdFrm: TrwPropertyEditor;
  lPropObj: TPersistent;
  i, j, k: Integer;
  flPrintObject : boolean;
begin
  PropInfo := GetPropInfo(AObject, APropName);
  flPrintObject := (TObject(FPropList.FObjects[0])) is TrwPrintableControl;
  if not Assigned(PropInfo) then
  begin
    j := TrwComponent(AObject)._UserProperties.IndexOf(APropName);
    PropInfo := TrwComponent(AObject)._UserProperties[j].PPropInfo;
  end
  else
    j := -1;

  if PropInfo^.PropType^.Kind = tkWString then
    lPropEdFrm := TrwPropertyEditorClass(FindClass('TrwStringsPropertyEditor')).Create(Self, flPrintObject)
  else if PropInfo^.PropType^.Kind = tkVariant then
    lPropEdFrm := TrwPropertyEditorClass(FindClass('TrwVariantPropertyEditor')).Create(Self, flPrintObject)
  else
  begin
    if PropInfo^.PropType^.Kind = tkSet then
      h := 'Set'
    else
    begin
      if j = -1 then
        h := PropInfo^.PropType^.Name
      else
        h := TrwComponent(AObject)._UserProperties[j].PointerType;
      Delete(h, 1, 1);
      if (Copy(h, 1, 2) = 'rw') then
        Delete(h, 1, 2);
    end;

    h := 'Trw' + h + 'PropertyEditor';
    ClassProp := FindClass(h);

    lPropEdFrm := TrwPropertyEditorClass(ClassProp).Create(Self, flPrintObject);
  end;

  if Assigned(lPropEdFrm) and (AObject is TrwComponent) then
    lPropEdFrm.CheckInheritProp := TrwComponent(AObject).InheritedComponent;

  Row := PropertyIndex(APropName);

  try
    if (PropInfo^.PropType^.Kind = tkClass) then
    begin
      if j = -1 then
        lPropObj := TPersistent(GetObjectProp(AObject, PropInfo))
      else
        lPropObj := Pointer(Integer(TrwComponent(AObject)._UserProperties[j].Value));

      lPropEdFrm.OrdValue := Integer(AObject);
      lPropEdFrm.Value.Assign(lPropObj);
      if (lPropEdFrm.ShowModal = mrOk) and not Debugging then
      begin
        for i := 0 to FPropList.FObjects.Count-1 do
        begin
          CheckInheritedAndShowError(TrwComponent(FPropList.FObjects[i]));

          if j = -1 then
          begin
            PropInfo := GetPropInfo(TObject(FPropList.FObjects[i]), APropName);
            lPropObj := TPersistent(GetObjectProp(TObject(FPropList.FObjects[i]), PropInfo));
            lPropObj.Assign(TPersistent(lPropEdFrm.Value));
          end
          else
          begin
            k := TrwComponent(TObject(FPropList.FObjects[i]))._UserProperties.IndexOf(APropName);
            TrwComponent(TObject(FPropList.FObjects[i]))._UserProperties[k].Value := Integer(Pointer(lPropEdFrm.Value));
          end;
        end;
        ChangePropertyValue;
      end;
    end

    else if (PropInfo^.PropType^.Kind = tkInteger) then
    begin
      lPropEdFrm.OrdValue := GetOrdProp(AObject, PropInfo);
      if (lPropEdFrm.ShowModal = mrOk) and not Debugging then
      begin
        for i := 0 to FPropList.FObjects.Count-1 do
        begin
          CheckInheritedAndShowError(TrwComponent(FPropList.FObjects[i]));

          if j = -1 then
          begin
            PropInfo := GetPropInfo(TObject(FPropList.FObjects[i]), APropName);
            SetOrdProp(TObject(FPropList.FObjects[i]), PropInfo, lPropEdFrm.OrdValue);
          end
          else
          begin
            k := TrwComponent(TObject(FPropList.FObjects[i]))._UserProperties.IndexOf(APropName);
            TrwComponent(TObject(FPropList.FObjects[i]))._UserProperties[k].Value := lPropEdFrm.OrdValue;
          end;
        end;
        InplaceEditor.Text := IntToStr(lPropEdFrm.OrdValue);
        ChangePropertyValue;
      end;
    end

    else if (PropInfo^.PropType^.Kind = tkFloat) then
    begin
      lPropEdFrm.FloatValue := GetFloatProp(AObject, PropInfo);
      if (lPropEdFrm.ShowModal = mrOk) and not Debugging then
      begin
        for i := 0 to FPropList.FObjects.Count-1 do
        begin
          CheckInheritedAndShowError(TrwComponent(FPropList.FObjects[i]));

          if j = -1 then
          begin
            PropInfo := GetPropInfo(TObject(FPropList.FObjects[i]), APropName);
            SetFloatProp(TObject(FPropList.FObjects[i]), PropInfo, lPropEdFrm.FloatValue);
          end
          else
          begin
            k := TrwComponent(TObject(FPropList.FObjects[i]))._UserProperties.IndexOf(APropName);
            TrwComponent(TObject(FPropList.FObjects[i]))._UserProperties[k].Value := lPropEdFrm.FloatValue;
          end;
        end;
        InplaceEditor.Text := FPropList.GetStrValue(Row);
        ChangePropertyValue;
      end;
    end

    else if (PropInfo^.PropType^.Kind = tkWString) then
    begin
      lPropObj := TStringList.Create;
      try
        if j = -1 then
          TStringList(lPropObj).Text := GetStrProp(AObject, PropInfo)
        else
          TStringList(lPropObj).Text := TrwComponent(AObject)._UserProperties[j].Value;
        lPropEdFrm.Value.Assign(lPropObj);
      finally
        lPropObj.Free;
      end;
      if (lPropEdFrm.ShowModal = mrOk) and not Debugging  then
      begin
        for i := 0 to FPropList.FObjects.Count-1 do
        begin
          CheckInheritedAndShowError(TrwComponent(FPropList.FObjects[i]));

          if j = -1 then
          begin
            PropInfo := GetPropInfo(TObject(FPropList.FObjects[i]), APropName);
            SetStrProp(TObject(FPropList.FObjects[i]), PropInfo, TrwStrings(lPropEdFrm.Value).Text);
          end
          else
          begin
            k := TrwComponent(TObject(FPropList.FObjects[i]))._UserProperties.IndexOf(APropName);
            TrwComponent(TObject(FPropList.FObjects[i]))._UserProperties[k].Value := TrwStrings(lPropEdFrm.Value).Text;
          end;
        end;
        InplaceEditor.Text := TrwStrings(lPropEdFrm.Value).Text;
        ChangePropertyValue;
      end;
    end

    else if (PropInfo^.PropType^.Kind = tkVariant) then
    begin
      lPropEdFrm.VariantValue := GetVariantProp(AObject, PropInfo);

      if (lPropEdFrm.ShowModal = mrOk) and not Debugging  then
      begin
        for i := 0 to FPropList.FObjects.Count-1 do
        begin
          CheckInheritedAndShowError(TrwComponent(FPropList.FObjects[i]));

          if j = -1 then
          begin
            PropInfo := GetPropInfo(TObject(FPropList.FObjects[i]), APropName);
            SetVariantProp(TObject(FPropList.FObjects[i]), PropInfo, lPropEdFrm.VariantValue);
          end
          else
          begin
            k := TrwComponent(TObject(FPropList.FObjects[i]))._UserProperties.IndexOf(APropName);
            TrwComponent(TObject(FPropList.FObjects[i]))._UserProperties[k].Value := lPropEdFrm.VariantValue;
          end;
        end;
        InplaceEditor.Text := VarToStr(lPropEdFrm.VariantValue);
      end;
    end

    else if (PropInfo^.PropType^.Kind = tkSet) then
    begin
      TypeData := GetTypeData(PropInfo^.PropType^);
      TypeInfo := TypeData.CompType^;
      TypeData := GetTypeData(TypeInfo);

      lPropEdFrm.NamesList.Clear;

      for i := TypeData^.MinValue to TypeData^.MaxValue do
        lPropEdFrm.NamesList.AddObject(GetEnumName(TypeInfo, i), Pointer(i));

      i := PropertyIndex(APropName);
      if Cells[1, i] = '' then
        lPropEdFrm.StrValue1 := '[]'
      else
        lPropEdFrm.StrValue1 := GetSetProp(AObject, PropInfo, False);

      if (lPropEdFrm.ShowModal = mrOk) and not Debugging then
      begin
        for i := 0 to FPropList.FObjects.Count-1 do
        begin
          CheckInheritedAndShowError(TrwComponent(FPropList.FObjects[i]));

          PropInfo := GetPropInfo(TObject(FPropList.FObjects[i]), APropName);
          if (lPropEdFrm.StrValue1 <> '') then
            SetComponentProperty(TObject(FPropList.FObjects[i]), [PropInfo^.Name], '+[' + lPropEdFrm.StrValue1 + ']');
          if (lPropEdFrm.StrValue2 <> '') then
            SetComponentProperty(TObject(FPropList.FObjects[i]), [PropInfo^.Name], '-[' + lPropEdFrm.StrValue2 + ']');
        end;
        ChangePropertyValue;
      end;
    end;

  finally
    lPropEdFrm.Free;
  end;
end;

function TrwPropertiesGrid.PropertyIndex(APropName: string): Integer;
var
  i: Integer;
begin
  Result := -1;
  APropName := UpperCase(APropName);
  for i := 0 to RowCount-1 do
    if UpperCase(Cells[0, i]) = APropName then
    begin
      Result := i;
      break;
    end;
end;

function TrwPropertiesGrid.CanEditModify: Boolean;
begin
  Result := inherited CanEditModify;
  Result := Result and not FReadOnly;
end;


{TrwPropertiesEditor}

constructor TrwPropertiesEditor.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FReadOnly := False;
  FPropertiesGrid := TrwPropertiesGrid.Create(Self);
  FPropertiesGrid.ScrollBars := ssVertical;
  FPropertiesGrid.FixedCols := 0;
  FPropertiesGrid.FixedRows := 0;
  FPropertiesGrid.ColCount := 2;
  FPropertiesGrid.RowCount := 0;
  FPropertiesGrid.BevelOuter := bvNone;
  FPropertiesGrid.BorderStyle := bsSingle;
  FPropertiesGrid.Align := alClient;
  FPropertiesGrid.Parent := tsProperties;
  FPropertiesGrid.PopupMenu := pmUserProps;
end;

destructor TrwPropertiesEditor.Destroy;
begin
  FPropertiesGrid.Free;

  inherited;
end;


procedure TrwPropertiesEditor.lbEventsClick(Sender: TObject);
begin
  if (lbEvents.ItemIndex <> -1) then
    FCurrentEventName := lbEvents.Items[lbEvents.ItemIndex]
  else
    FCurrentEventName := '';

  if {(FCurrentEventName <> '') and} Assigned(FOnEventChange) then
    FOnEventChange(Self);
end;

procedure TrwPropertiesEditor.AddNewComponentToComponentsList(const AComponent: TrwComponent; AToolBar: TrwBasicToolsBar;
   const AGroup: Boolean = False);
var
  N: TTreeNode;
  h: String;
begin
  if AComponent.InheritsFrom(TrwComponent) then
  begin
    if (AComponent is TrwSubReport) and (tvObjTree.Items.Count > 0) and
      (TrwComponent(tvObjTree.Items[0].Data) is TrwCustomReport) then
      N := tvObjTree.Items[0]
    else
      N := FindComponentInTree(AComponent.VirtualOwner);
    N := tvObjTree.Items.AddChildObject(N, AComponent.Name, AComponent);

    N.ImageIndex := -1;
    if AComponent is TrwCustomReport then
      h := 'TrwSubReport'
    else if AComponent is TrwBand then
      h := 'TrwContainer'
    else if AComponent is TrwInputFormContainer then
      N.ImageIndex := 44
    else if AComponent is TrwGlobalVarFunct then
      N.ImageIndex := 47
    else
      h := AComponent.ClassName;

    if N.ImageIndex = -1 then
      N.ImageIndex := AToolBar.GetImageIndexForComponent(h);
    N.StateIndex := N.ImageIndex;
    N.SelectedIndex := N.ImageIndex;

    if not AGroup then
    begin
      tvObjTree.SortType := stNone;
      tvObjTree.SortType := stText;
    end;

    if not (AComponent is TrwSubReport) then
      AddChildren(AComponent, AToolBar);
  end;
end;

procedure TrwPropertiesEditor.DeleteComponentFromComponentsList(AComponent: TrwComponent);
var
  N: TTreeNode;
begin
  N := FindComponentInTree(AComponent);
  if not Assigned(N) then
    Exit;

  if N = tvObjTree.Selected then
  begin
    tvObjTree.OnChange := nil;
    tvObjTree.Selected := nil;
    tvObjTreeChange(nil, nil);
    tvObjTree.OnChange := tvObjTreeChange;
  end;

  N.Free;
end;

procedure TrwPropertiesEditor.CreateComponentsList(AOwner: TrwComponent; AToolBar: TrwBasicToolsBar);
begin
  FPropertiesGrid.FPropList.FComponentsListOwner := AOwner;
  tvObjTree.Items.BeginUpdate;
  tvObjTree.Items.Clear;

  if Assigned(AOwner) then
  begin
    AddNewComponentToComponentsList(AOwner, AToolBar, True);
    if AOwner is TrwSubReport then
      AddChildren(AOwner, AToolBar);
  end;

  tvObjTree.SortType := stNone;
  tvObjTree.SortType := stText;
  if tvObjTree.Items.Count > 0 then
    tvObjTree.Items[0].Expand(False);
  tvObjTree.Items.EndUpdate;
end;

procedure TrwPropertiesEditor.ShowComponentProperties(AComponent: TrwComponent; AGroup: Boolean = False);
begin
  FGroup := AGroup;
  tvObjTree.OnChange := nil;
  tvObjTree.Selected := FindComponentInTree(AComponent);
  tvObjTreeChange(nil, nil);

  if FGroup then
    tvObjTree.Selected := nil;

  tvObjTree.OnChange := tvObjTreeChange;
end;

procedure TrwPropertiesEditor.RefreshComponentProperties;
var
  lRow: Integer;
begin
  lRow := FPropertiesGrid.Row;
  FPropertiesGrid.RefreshPropertyList;
  FPropertiesGrid.Row := lRow;
end;


procedure TrwPropertiesEditor.ShowComponentEvent(AComponentPath: string; AEventName: string);
var
  i: Integer;
begin
  tvObjTree.Selected := FindComponentInTree(AComponentPath);

  if Assigned(tvObjTree.Selected) then
  begin
    CurrentComponent := TrwComponent(tvObjTree.Selected.Data);
    FPropertiesGrid.CreatePropertyList(FCurrentComponent);
    FCurrentComponent.GetEventsList(lbEvents.Items);

    i := lbEvents.Items.IndexOf(AEventName);
    lbEvents.ItemIndex := i;
    lbEvents.OnClick(nil);
  end;
end;


procedure TrwPropertiesEditor.EditProperty(AComponent: TrwComponent;  APropName: string);
begin
  FPropertiesGrid.EditProperty(AComponent, APropName);
end;


procedure TrwPropertiesEditor.tvObjTreeChange(Sender: TObject; Node: TTreeNode);
var
  lLastEvent: String;
  i: Integer;
  ActCtrl: TWinControl;
begin
  ActCtrl := Screen.ActiveControl;

  if Assigned(Screen.ActiveForm) and (ActCtrl is TrwEventTextEdit) then
    Screen.ActiveForm.ActiveControl := nil;

  if not Assigned(tvObjTree.Selected) then
  begin
    CurrentComponent := nil;
    lbEvents.Clear;
    lbEvents.OnClick(nil);
    FPropertiesGrid.CreatePropertyList(FCurrentComponent);
  end

  else
  begin
    CurrentComponent := TrwComponent(tvObjTree.Selected.Data);
    FPropertiesGrid.CreatePropertyList(FCurrentComponent, FGroup);

    if lbEvents.ItemIndex <> -1 then
      lLastEvent := lbEvents.Items[lbEvents.ItemIndex]
    else
      lLastEvent := '';
    FCurrentComponent.GetEventsList(lbEvents.Items);

    with FCurrentComponent.DesignInfo do
    begin
      i := EventsPositions.IndexOfName(LastEvent);
      if i <> -1 then
        lbEvents.ItemIndex := lbEvents.Items.IndexOf(LastEvent)
      else
      begin
        lbEvents.ItemIndex := lbEvents.Items.IndexOf(lLastEvent);
        if (lbEvents.ItemIndex = -1) and (lbEvents.Items.Count > 0) then
          lbEvents.ItemIndex := 0;
      end;
    end;

    lbEvents.OnClick(nil);
  end;

  UpdateComponentNamePanel;

  if Assigned(Sender) and Assigned(FOnComponentChange) then
    FOnComponentChange(Self, FCurrentComponent);

  if ActCtrl = tvObjTree then
    tvObjTree.SetFocus;
end;


function TrwPropertiesEditor.FindComponentInTree(const AComponent: TrwComponent): TTreeNode;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to tvObjTree.Items.Count - 1 do
    if tvObjTree.Items[i].Data = AComponent then
    begin
      Result := tvObjTree.Items[i];
      break;
    end;
end;


function TrwPropertiesEditor.FindComponentInTree(const AComponentPath: String): TTreeNode;
var
  h, h1: string;

  function FindNode(const ANode: TTreeNode; const ACompName: string): TTreeNode;
  var
    i: Integer;
  begin
    Result := nil;
    for i := 0 to ANode.Count - 1 do
      if SameText(ANode.Item[i].Text, ACompName) then
      begin
        Result := ANode.Item[i];
        break;
      end;
  end;

begin
  Result := nil;
  h1 := AComponentPath;
  h := GetComponentPath(TrwComponent(tvObjTree.Items[0].Data));

  if Copy(h1, 1, Length(h)) = h then
  begin
    Delete(h1, 1, Length(h) + 1);
    Result := tvObjTree.Items[0];
  end
  else
    Exit;

  while Length(h1) > 0 do
  begin
    h := GetNextStrValue(h1, '.');
    Result := FindNode(Result, h);
    if not Assigned(Result) then
      break;
  end;
end;


procedure TrwPropertiesEditor.tvObjTreeDblClick(Sender: TObject);
begin
  pgcInspector.ActivePage := tsProperties;
end;

procedure TrwPropertiesEditor.AddChildren(const AOwner: TrwComponent; const AToolBar: TrwBasicToolsBar);
var
  i: Integer;
  C: TComponent;
begin
  for i := 0 to AOwner.VirtualComponentCount - 1 do
  begin
    C := AOwner.GetVirtualChild(i);
    if not (C is TrwComponent) or (C = AOwner) then
      Continue;
    AddNewComponentToComponentsList(TrwComponent(C), AToolBar, True);
  end;
end;


procedure TrwPropertiesEditor.SetCurrentComponent(const Value: TrwComponent);
begin
  FCurrentComponent := Value;
  if Assigned(FCurrentComponent) then
    FCurrentCompPath := GetComponentPath(FCurrentComponent)
  else
    FCurrentCompPath := '';

  if Assigned(FCurrentComponent) and
    (FCurrentComponent.LibDesignMode or (FCurrentComponent is TrwCustomReport) or
    (FCurrentComponent is TrwInputFormContainer)) then
    pmUserEvent.AutoPopup := True
  else
    pmUserEvent.AutoPopup := False;

  pmUserProps.AutoPopup := pmUserEvent.AutoPopup;

{
  if Assigned(FCurrentComponent) and FCurrentComponent.LibDesignMode then
    pmUserProps.AutoPopup := True
  else
    pmUserProps.AutoPopup := False;
}
end;


procedure TrwPropertiesEditor.miRegUserEventClick(Sender: TObject);
var
  h: String;
begin
  h := 'procedure NewEvent()';
  if InputQuery('Registration New User Event', 'Type in a header of the event', h) then
  begin
    FCurrentComponent.RegisterEvents([h], True);
    RefreshComponentEvents;
  end;
end;

procedure TrwPropertiesEditor.RefreshComponentEvents;
var
  i: Integer;
begin
  i := lbEvents.ItemIndex;
  FCurrentComponent.GetEventsList(lbEvents.Items);
  lbEvents.ItemIndex := i;
  lbEvents.OnClick(nil);
end;

procedure TrwPropertiesEditor.miUnRegUserEventClick(Sender: TObject);
var
  Ev: TrwEvent;
begin
  if lbEvents.ItemIndex = -1 then
    Exit;

  Ev := TrwEvent(lbEvents.Items.Objects[lbEvents.ItemIndex]);

  if not Ev.UserEvent then
    raise ErwException.CreateFmt(ResNotUserEvent, [Ev.Name]);

  if Ev.InheritedItem then
    raise ErwException.Create(ResErrInheritedUserEventDel);

  if MessageDlg('You are about unregister user event "' + Ev.Name + '". Proceed?',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    FCurrentComponent.UnRegisterEvents([Ev.Name]);
    RefreshComponentEvents;
  end;
end;


procedure TrwPropertiesEditor.miRegUserPropClick(Sender: TObject);
begin
  if AddUserProperty(FCurrentComponent) then
    ShowComponentProperties(FCurrentComponent);
end;

procedure TrwPropertiesEditor.miUnRegUserPropClick(Sender: TObject);
begin
  if FPropertiesGrid.Row = -1 then
    Exit;

  if DeleteUserProperty(FCurrentComponent, FPropertiesGrid.Cells[0, FPropertiesGrid.Row]) then
    ShowComponentProperties(FCurrentComponent);
end;


procedure TrwPropertiesEditor.UpdateComponentNamePanel;

  function GetOwnerPath(const AComp: TrwComponent): string;
  begin
    Result := '';
    if not Assigned(AComp) or (AComp.VirtualOwner = nil) or
      (AComp = AComp.VirtualOwner) or (AComp.VirtualOwner is TrwCustomReport) then
      Exit;

    Result := GetOwnerPath(AComp.VirtualOwner) + AComp.VirtualOwner.Name +'.';
  end;

begin
  if FGroup then
    pnlCompName.Caption := 'Multi-Selection'

  else
    if Assigned(FCurrentComponent) then
    begin
      pnlCompName.Caption := GetOwnerPath(FCurrentComponent) + FCurrentComponent.Name;
      if FCurrentComponent._LibComponent <> '' then
        pnlCompName.Caption := pnlCompName.Caption+': ' + FCurrentComponent._LibComponent
      else
        if not (FCurrentComponent is TrwGlobalVarFunct) then
          pnlCompName.Caption := pnlCompName.Caption+': '+FCurrentComponent.ClassName
    end
    else
      pnlCompName.Caption := '';
end;

procedure TrwPropertiesEditor.SetReadOnly(const Value: Boolean);
begin
  FReadOnly := Value;
  FPropertiesGrid.ReadOnly := FReadOnly;
end;

procedure TrwPropertiesEditor.pgcInspectorChange(Sender: TObject);
begin
  if (pgcInspector.ActivePageIndex = 0) or (pgcInspector.ActivePageIndex = 2) then
    btnPropRevert.Enabled := false
  else
    btnPropRevert.Enabled := not ReadOnly;
end;

procedure TrwPropertiesEditor.btnPropRevertClick(Sender: TObject);

  function PathFromRootOwner(Comp: TrwComponent): String;
  var
    RO, O: TComponent;
  begin
    Result := '';
    RO := Comp.RootOwner;
    O := Comp;
    Result := O.Name;
    while O <> RO do
    begin
      O := O.Owner;
      Result := O.Name + '.' + Result;
    end;
  end;

var
  RootComponent: TrwComponent;
  BaseComponent: TrwComponent;
  Own: TrwComponent;
  CompClass: String;
  PropName: String;
  PropVal: Variant;
  CompPath: String;
begin
  if Assigned(FCurrentComponent) and (MessageDlg('Do you want to revert property to its ancestor defaults?' + #13 +
                  'Current value of the property WILL BE LOST! Continue?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
     Own := FCurrentComponent.VirtualOwner;
     if FCurrentComponent.InheritedComponent then
     begin
       if (Own is TrwInputFormContainer) or (Own is TrwSubReport) then
         Own := Own.RootOwner;
       CompClass := Own.PClassName;
     end
     else
       CompClass := FCurrentComponent.PClassName;

     RootComponent := TrwComponent(CreateRWComponentByClass(CompClass, False).RealObject);
     try
       if FCurrentComponent.InheritedComponent then
       begin
         CompPath := PathFromRootOwner(FCurrentComponent);
         RootComponent.Name := Own.Name;
         BaseComponent := ComponentByPath(CompPath, RootComponent, False)
       end
       else
         BaseComponent := RootComponent;

       if Assigned(BaseComponent) then
       begin
         PropName := FPropertiesGrid.FPropList.Strings[FPropertiesGrid.Row];
         PropVal := BaseComponent.PGetPropertyValue(PropName);
         FCurrentComponent.PSetPropertyValue(PropName, PropVal);

         RefreshComponentProperties;

         ShowMessage('Property''s value has been reverted succesfully'); 
       end;

     finally
       RootComponent.Free;
     end;
  end;
end;

end.
