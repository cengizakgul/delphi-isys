unit rmTrmDBTableCellPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, rmOPEBorderLinesFrm, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, StdCtrls, ExtCtrls, ComCtrls, rmOPEValueFrm,
  rmOPEDBCellValueFrm, rmTrmPrintControlPropFrm, rmOPEBooleanFrm,
  rmOPEColorFrm, rmOPEFontFrm, rmTrmPrintContainerPropFrm,
  rmOPETextFilterFrm, rmOPEExpressionFrm, rmOPEBlockingControlFrm;

type
  TrmTrmDBTableCellProp = class(TrmTrmPrintContainerProp)
    tsBorderLines: TTabSheet;
    frmBorderLines: TrmOPEBorderLines;
    frmFont: TrmOPEFont;
    GroupBox2: TGroupBox;
    frmValue: TrmOPEDBCellValue;
    frmAutoHeight: TrmOPEBoolean;
    frmWordWrap: TrmOPEBoolean;
    frmFixedWidth: TrmOPEBoolean;
    tsTextFilters: TTabSheet;
    frmTextFilter: TrmOPETextFilter;
  private
  public
    procedure GetPropsFromObject; override;
  end;

implementation

{$R *.dfm}

{ TrmOPBasic1 }

procedure TrmTrmDBTableCellProp.GetPropsFromObject;
begin
  inherited;
  frmValue.PropertyName := 'Value';
  frmValue.ShowPropValue;
  frmAutoHeight.PropertyName := 'AutoHeight';
  frmAutoHeight.ShowPropValue;
  frmWordWrap.PropertyName := 'WordWrap';
  frmWordWrap.ShowPropValue;
  frmFixedWidth.PropertyName := 'WordWrap';
  frmFixedWidth.ShowPropValue;
  frmBorderLines.ShowPropValue;
end;

end.
