inherited rmDsgnLibrary: TrmDsgnLibrary
  inherited pnlList: TPanel
    inherited lvItems: TListView
      PopupMenu = nil
    end
  end
  inherited opdImport: TOpenDialog
    DefaultExt = 'rwl'
    Filter = 'ReportWriter Library (*.rwl)|*.rwl'
    Title = 'Import Library'
  end
  inherited svdExport: TSaveDialog
    DefaultExt = 'rwl'
    Filter = 'ReportWriter Library (*.rwl)|*.rwl'
    Title = 'Export Library'
  end
end
