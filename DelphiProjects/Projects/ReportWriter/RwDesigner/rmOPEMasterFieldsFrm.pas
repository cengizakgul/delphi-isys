unit rmOPEMasterFieldsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, ExtCtrls, DB, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, rwDesignClasses, Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, wwdblook, rmTypes, rwBasicUtils, rwDsgnUtils, Mask,
  wwdbedit, Wwdotdot, Wwdbcomb, ISDataAccessComponents, dbcgrids,
  rmJoinItemsFrm, rmJoinFieldsFrm;

type
  TrmOPEMasterFields = class(TrmObjPropertyEditor)
    frmJoin1: TrmJoinFields;
    frmJoin2: TrmJoinFields;
    frmJoin3: TrmJoinFields;
    frmJoin4: TrmJoinFields;
    frmJoin5: TrmJoinFields;
    Label3: TLabel;
    lHeader2: TLabel;
    Label1: TLabel;
  private
    procedure PrepareDataSets;
    procedure OnChangeItems(Sender: TObject);
  public
    procedure ShowPropValue; override;
    procedure ApplyChanges; override;
    procedure MakeLookups;
  end;

implementation

{$R *.dfm}

{ TrmOPEMasterFields }

procedure TrmOPEMasterFields.ApplyChanges;
var
  h: String;
  Frm: TrmJoinFields;
  i: Integer;
begin
  if not Visible then
    Exit;

  h := '';
  for i := 1 to 5 do
  begin
    Frm := TrmJoinFields(FindComponent('frmJoin' + IntToStr(i)));

    if (Frm.LeftValue <> '') and (Frm.RightValue <> '') then
    begin
      if h <> '' then
        h := h + #13;
      h := h + Frm.LeftValue + '=' + Frm.RightValue;
    end;  
  end;

  TStringList(Pointer(Integer((TComponent(LinkedObject) as IrmDesignObject).GetPropertyValue('MasterFields')))).Text := h;
end;

procedure TrmOPEMasterFields.PrepareDataSets;

  procedure MakeJoins;
  var
    i: Integer;
    L: TStringList;
    h: String;
    Frm: TrmJoinFields;
  begin
    for i := 1 to 5 do
    begin
      Frm := TrmJoinFields(FindComponent('frmJoin' + IntToStr(i)));
      Frm.DisableControls;
      Frm.LeftValue := '';
      Frm.RightValue :=  '';
      Frm.EnableControls;
    end;

    L := TStringList(Pointer(Integer((TComponent(LinkedObject) as IrmDesignObject).GetPropertyValue('MasterFields'))));
    for i := 1 to L.Count do
    begin
      Frm := TrmJoinFields(FindComponent('frmJoin' + IntToStr(i)));
      if not Assigned(Frm) then
        Continue;
        
      Frm.DisableControls;
      h := L[i - 1];
      Frm.LeftValue := Trim(GetNextStrValue(h, '='));
      if h = '' then
        h := Frm.LeftValue;
      Frm.RightValue := Trim(h);
      Frm.EnableControls;
    end;
  end;

begin
  if Visible then
  begin
    MakeLookUps;
    MakeJoins;
  end;
end;

procedure TrmOPEMasterFields.ShowPropValue;
var
  Frm: TrmJoinFields;
  i: Integer;
begin
  for i := 1 to 5 do
  begin
    Frm := TrmJoinFields(FindComponent('frmJoin' + IntToStr(i)));
    Frm.OnChange := OnChangeItems;
  end;

  inherited;

  Visible := (TComponent(LinkedObject) as IrmQueryDrivenObject).GetQuery.GetMasterQuery <> nil;
  PrepareDataSets;
end;

procedure TrmOPEMasterFields.MakeLookups;
var
  Frm: TrmJoinFields;
  i: Integer;
begin
  if Visible then
  begin
    for i := 1 to 5 do
    begin
      Frm := TrmJoinFields(FindComponent('frmJoin' + IntToStr(i)));
      Frm.MakeLookups;
    end;
  end;
end;

procedure TrmOPEMasterFields.OnChangeItems(Sender: TObject);
begin
  NotifyOfChanges;
end;

end.
