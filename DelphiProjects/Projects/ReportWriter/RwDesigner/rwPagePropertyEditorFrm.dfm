inherited rwPagePropertyEditor: TrwPagePropertyEditor
  Left = 342
  Top = 335
  VertScrollBar.Range = 0
  BorderStyle = bsToolWindow
  Caption = 'Page Setup'
  ClientHeight = 267
  ClientWidth = 519
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOK: TButton
    Left = 349
    Top = 238
    TabOrder = 1
    OnClick = btnOKClick
  end
  inherited btnCancel: TButton
    Left = 437
    Top = 238
    TabOrder = 2
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 519
    Height = 227
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object gbMargins: TGroupBox
      Tag = 186
      Left = 234
      Top = 6
      Width = 278
      Height = 215
      Caption = 'Margins'
      TabOrder = 3
      object pnPreview: TPanel
        Left = 63
        Top = 35
        Width = 150
        Height = 150
        BevelOuter = bvNone
        TabOrder = 0
        object pbPreview: TPaintBox
          Left = 10
          Top = 10
          Width = 130
          Height = 130
          Cursor = crHandPoint
          Color = clBtnFace
          ParentColor = False
          OnClick = pbPreviewClick
          OnPaint = pbPreviewPaint
        end
        object imDuplex: TImage
          Left = 116
          Top = 114
          Width = 25
          Height = 25
          Picture.Data = {
            07544269746D61700E070000424D0E0700000000000036040000280000001A00
            00001A0000000100080000000000D80200000000000000000000000100000000
            000000000000000080000080000000808000800000008000800080800000C0C0
            C000C0DCC000F0CAA6000020400000206000002080000020A0000020C0000020
            E00000400000004020000040400000406000004080000040A0000040C0000040
            E00000600000006020000060400000606000006080000060A0000060C0000060
            E00000800000008020000080400000806000008080000080A0000080C0000080
            E00000A0000000A0200000A0400000A0600000A0800000A0A00000A0C00000A0
            E00000C0000000C0200000C0400000C0600000C0800000C0A00000C0C00000C0
            E00000E0000000E0200000E0400000E0600000E0800000E0A00000E0C00000E0
            E00040000000400020004000400040006000400080004000A0004000C0004000
            E00040200000402020004020400040206000402080004020A0004020C0004020
            E00040400000404020004040400040406000404080004040A0004040C0004040
            E00040600000406020004060400040606000406080004060A0004060C0004060
            E00040800000408020004080400040806000408080004080A0004080C0004080
            E00040A0000040A0200040A0400040A0600040A0800040A0A00040A0C00040A0
            E00040C0000040C0200040C0400040C0600040C0800040C0A00040C0C00040C0
            E00040E0000040E0200040E0400040E0600040E0800040E0A00040E0C00040E0
            E00080000000800020008000400080006000800080008000A0008000C0008000
            E00080200000802020008020400080206000802080008020A0008020C0008020
            E00080400000804020008040400080406000804080008040A0008040C0008040
            E00080600000806020008060400080606000806080008060A0008060C0008060
            E00080800000808020008080400080806000808080008080A0008080C0008080
            E00080A0000080A0200080A0400080A0600080A0800080A0A00080A0C00080A0
            E00080C0000080C0200080C0400080C0600080C0800080C0A00080C0C00080C0
            E00080E0000080E0200080E0400080E0600080E0800080E0A00080E0C00080E0
            E000C0000000C0002000C0004000C0006000C0008000C000A000C000C000C000
            E000C0200000C0202000C0204000C0206000C0208000C020A000C020C000C020
            E000C0400000C0402000C0404000C0406000C0408000C040A000C040C000C040
            E000C0600000C0602000C0604000C0606000C0608000C060A000C060C000C060
            E000C0800000C0802000C0804000C0806000C0808000C080A000C080C000C080
            E000C0A00000C0A02000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0
            E000C0C00000C0C02000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0
            A000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF00FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD0000FDFD
            FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD0000FD0007070700
            00A4A40707FF07FF07FFFFFFFFFFFFFFFFFFFDFD0000FDFD0007FF07070000A4
            A4070707FF07FF07FFFFFFFFFFFFFDFD0000FDFDFD00FFFFFF07070000A4A407
            07FF07FF07FFFFFFFFFFFDFD0000FDFDFD00FFFFFFFFFF07A400A4A40707FF07
            FF07FFFFFFFFFDFD0000FDFDFD00FFFFFFFFFFFF07A400A4A40707FF07FF07FF
            FFFFFDFD0000FDFDFD0007FFFFFFFFFFFF07A400A4A40707FF07FF07FFFFFDFD
            0000FDFDFD0007FFFFFFFFFFFFFF07A400A4A40707FF07FF07FFFDFD0000FDFD
            FD0000FFFFFFFFFFFFFFFF07A400A4A40707FF07FFFFFDFD0000FDFDFDFD00FF
            FFFFFFFFFFFFFFFF070000A4A40707FF07FFFDFD0000FDFDFDFD00FFFFFFFFFF
            FFFFFFFFFF07A400A4A40707FF07FDFD0000FDFDFDFD00FFFFFFFFFFFF0700FF
            FF0700A400A4A40707FFFDFD0000FDFDFDFD00FFFFFFFF07A400A4FF0700A407
            A400A4A40707FDFD0000FDFDFDFD00FFFF07A40000A407A400A4FFFF07A400A4
            07FFFDFD0000FDFDFDFD00FF000000000000000007FFFFFFFF07A400A407FDFD
            0000FDFDFDFD00FFFF07A40000A407FFFFFFFFFFFFFF0700A407FDFD0000FDFD
            FDFD00FFFFFFFF07A400A4FFFFFFFFFFFFFFFF0700A4FDFD0000FDFDFDFD00FF
            FFFFFFFFFF0700FFFFFFFFFFFFFFFF0700A4FDFD0000FDFDFDFD00FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF0700FDFD0000FDFDFDFD00FFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0700FDFD0000FDFDFDFD0000000000000000000000000000
            07FFFFFFFF07FDFD0000FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD00000000
            0707FDFD0000FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD0007FDFD
            0000FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD00FDFD0000FDFD
            FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD0000}
          Transparent = True
          OnClick = imDuplexClick
        end
      end
      object speTopMargin: TwwDBSpinEdit
        Left = 111
        Top = 15
        Width = 55
        Height = 21
        Increment = 1.000000000000000000
        TabOrder = 2
        UnboundDataType = wwDefault
        OnChange = speWidthChange
      end
      object speBottomMargin: TwwDBSpinEdit
        Left = 111
        Top = 184
        Width = 55
        Height = 21
        Increment = 1.000000000000000000
        TabOrder = 4
        UnboundDataType = wwDefault
        OnChange = speWidthChange
      end
      object speRightMargin: TwwDBSpinEdit
        Left = 213
        Top = 97
        Width = 55
        Height = 21
        Increment = 1.000000000000000000
        TabOrder = 3
        UnboundDataType = wwDefault
        OnChange = speWidthChange
      end
      object speLeftMargin: TwwDBSpinEdit
        Left = 10
        Top = 97
        Width = 55
        Height = 21
        Increment = 1.000000000000000000
        TabOrder = 1
        UnboundDataType = wwDefault
        OnChange = speWidthChange
      end
    end
    object gbxOrientation: TGroupBox
      Tag = 217
      Left = 7
      Top = 140
      Width = 219
      Height = 46
      Caption = 'Paper Orientation'
      TabOrder = 1
      object rbPortrait: TRadioButton
        Tag = 148
        Left = 26
        Top = 18
        Width = 54
        Height = 17
        Caption = 'Portrait'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = rbPortraitClick
      end
      object rbLandscape: TRadioButton
        Tag = 147
        Left = 119
        Top = 18
        Width = 74
        Height = 17
        Caption = 'Landscape'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = rbPortraitClick
      end
    end
    object chbDuplexing: TCheckBox
      Left = 7
      Top = 201
      Width = 115
      Height = 17
      Caption = 'Print on Both Sides'
      TabOrder = 2
      OnClick = speWidthChange
    end
    object gbSize: TGroupBox
      Left = 7
      Top = 6
      Width = 219
      Height = 121
      Caption = 'Paper Size'
      TabOrder = 0
      object lblPaperSize: TLabel
        Tag = 219
        Left = 10
        Top = 26
        Width = 32
        Height = 13
        Caption = 'Format'
      end
      object lblWidth: TLabel
        Tag = 223
        Left = 10
        Top = 63
        Width = 28
        Height = 13
        Caption = 'Width'
      end
      object lblHeight: TLabel
        Tag = 215
        Left = 10
        Top = 92
        Width = 31
        Height = 13
        Caption = 'Height'
      end
      object cbPaperNames: TComboBox
        Left = 50
        Top = 22
        Width = 158
        Height = 21
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 0
        OnChange = cbPaperNamesChange
      end
      object speWidth: TwwDBSpinEdit
        Left = 50
        Top = 59
        Width = 72
        Height = 21
        Increment = 1.000000000000000000
        TabOrder = 1
        UnboundDataType = wwDefault
        OnChange = speWidthChange
      end
      object speHeight: TwwDBSpinEdit
        Left = 50
        Top = 88
        Width = 72
        Height = 21
        Increment = 1.000000000000000000
        TabOrder = 2
        UnboundDataType = wwDefault
        OnChange = speWidthChange
      end
    end
  end
end
