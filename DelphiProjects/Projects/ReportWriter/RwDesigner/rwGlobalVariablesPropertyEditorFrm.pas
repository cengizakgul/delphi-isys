unit rwGlobalVariablesPropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rwPropertyEditorFrm, StdCtrls, rwBasicClasses,
  ComCtrls, TypInfo, rwTypes, rmTypes;

type
  TrwGlobalVariablesPropertyEditor = class(TrwPropertyEditor)
    lvVar: TListView;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lvVarDrawItem(Sender: TCustomListView; Item: TListItem;
      Rect: TRect; State: TOwnerDrawState);
  private
  public
  end;

implementation

uses rwDsgnRes;

{$R *.dfm}


{ TrwGlobalVarFunctPropertyEditor }

procedure TrwGlobalVariablesPropertyEditor.FormCreate(Sender: TObject);
begin
  inherited;
  FValue := TrwGlobalVariables.Create(nil, TrwGlobalVariable);
end;


procedure TrwGlobalVariablesPropertyEditor.FormDestroy(Sender: TObject);
begin
  FValue.Free;
  inherited;
end;


procedure TrwGlobalVariablesPropertyEditor.FormShow(Sender: TObject);
var
  i: Integer;
  LI: TListItem;
begin
  inherited;

  for i := 0 to TrwGlobalVariables(FValue).Count-1 do
  begin
    LI := lvVar.Items.Add;
    LI.Data := TrwGlobalVariables(FValue)[i];
    LI.Caption := TrwGlobalVariables(FValue)[i].Name;
  end;
end;

procedure TrwGlobalVariablesPropertyEditor.lvVarDrawItem(
  Sender: TCustomListView; Item: TListItem; Rect: TRect;
  State: TOwnerDrawState);
var
  PTypInf: PTypeInfo;
  h: String;
begin
  lvVar.Canvas.Brush.Color := lvVar.Color;
  lvVar.Canvas.FillRect(Rect);

  case TrwGlobalVariable(Item.Data).Visibility of
    rvvPublished:  lvVar.Canvas.Font.Color := clNavy;
    rvvPublic:     lvVar.Canvas.Font.Color := clBlack;
    rvvProtected:  lvVar.Canvas.Font.Color := clGray;
    rvvPrivate:    lvVar.Canvas.Font.Color := clSilver;
  end;

  lvVar.Canvas.Brush.Color := lvVar.Color;
  lvVar.Canvas.FillRect(Rect);

  Rect.Right := Rect.Left + lvVar.Canvas.TextWidth(Item.Caption + '  ');
  Rect.Bottom := Rect.Top + lvVar.Canvas.TextHeight(Item.Caption + '  ') + 2;

  if (odSelected in State) then
  begin
    if lvVar.Focused then
    begin
      lvVar.Canvas.Brush.Color := clHighlight;
      lvVar.Canvas.Font.Color := clWhite;
    end
    else
    begin
      lvVar.Canvas.Brush.Color := clInactiveBorder;
      lvVar.Canvas.Font.Color := clBlack;
    end;
    lvVar.Canvas.FillRect(Rect);
  end;

  lvVar.Canvas.TextOut(Rect.Left + 2, Rect.Top, Item.Caption);

  if (odSelected in State) and lvVar.Focused then
    lvVar.Canvas.DrawFocusRect(Rect);

  PTypInf := TypeInfo(TrwVarTypeKind);
  h := GetEnumName(PTypInf, Ord(TrwGlobalVariable(Item.Data).VarType));
  Delete(h, 1, 3);
  lvVar.Canvas.Font.Color := clBlack;
  lvVar.Canvas.Brush.Color := lvVar.Color;
  Rect.Left := 180;
  lvVar.Canvas.TextOut(Rect.Left, Rect.Top, h);

  case TrwGlobalVariable(Item.Data).Visibility of
    rvvPublished:  h := 'Published';
    rvvPublic:     h := 'Public';
    rvvProtected:  h := 'Protected';
    rvvPrivate:    h := 'Private';
  else
    h := '';
  end;
  Rect.Left := 250;
  lvVar.Canvas.TextOut(Rect.Left, Rect.Top, h);
end;

initialization
  RegisterClass(TrwGlobalVariablesPropertyEditor);

end.
