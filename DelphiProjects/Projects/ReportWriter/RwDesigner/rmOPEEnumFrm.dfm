inherited rmOPEEnum: TrmOPEEnum
  Width = 241
  Height = 21
  AutoSize = True
  object lPropName: TLabel
    Left = 0
    Top = 4
    Width = 27
    Height = 13
    Caption = 'Enum'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object cbList: TComboBox
    Left = 55
    Top = 0
    Width = 186
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = cbListChange
  end
end
