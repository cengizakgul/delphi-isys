inherited rmTrmlCheckBoxProp: TrmTrmlCheckBoxProp
  Height = 263
  inherited pcCategories: TPageControl
    Height = 263
    inherited tsBasic: TTabSheet
      inline frmCaption: TrmOPEStringSingleLine
        Left = 8
        Top = 88
        Width = 290
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 2
        inherited lPropName: TLabel
          Width = 36
          Caption = 'Caption'
        end
      end
      object grbValues: TGroupBox
        Left = 8
        Top = 126
        Width = 291
        Height = 90
        Caption = 'State Values'
        TabOrder = 3
        inline frmValueChecked: TrmOPEStringSingleLine
          Left = 10
          Top = 21
          Width = 265
          Height = 21
          AutoScroll = False
          AutoSize = True
          TabOrder = 0
          inherited lPropName: TLabel
            Width = 43
            Caption = 'Checked'
          end
          inherited edString: TEdit
            Width = 175
          end
        end
        inline frmValueUnChecked: TrmOPEStringSingleLine
          Left = 10
          Top = 54
          Width = 265
          Height = 21
          AutoScroll = False
          AutoSize = True
          TabOrder = 1
          inherited lPropName: TLabel
            Width = 56
            Caption = 'Unchecked'
          end
          inherited edString: TEdit
            Width = 175
          end
        end
      end
    end
    inherited tcAppearance: TTabSheet
      TabVisible = False
    end
  end
end
