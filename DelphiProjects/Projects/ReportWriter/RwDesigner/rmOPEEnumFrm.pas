unit rmOPEEnumFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls, rwBasicUtils;

type

  TrmOPEEnumListType = (rmLTPlainList, rmLTValues, rmLTPointers);

  TrmOPEEnum = class(TrmObjPropertyEditor)
    lPropName: TLabel;
    cbList: TComboBox;
    procedure cbListChange(Sender: TObject);
  private
    FValues: TStrings;
    FListType: TrmOPEEnumListType;
    procedure SetValues(const AValue: TStrings);
    function GetAllowCustomValues: Boolean;
    procedure SetAllowCustomValues(const AValue: Boolean);
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    procedure ShowPropValue; override;
    procedure MakeEnumList(const ANames: array of string; const AValues: array of Integer);
    function  NewPropertyValue: Variant; override;

    property Values: TStrings read FValues write SetValues;
    property AllowCustomValues: Boolean read GetAllowCustomValues write SetAllowCustomValues;
  end;

implementation

{$R *.dfm}

{ TrmObjPropertyEditor1 }

procedure TrmOPEEnum.AfterConstruction;
begin
  inherited;
  FValues := TStringList.Create;
end;

procedure TrmOPEEnum.BeforeDestruction;
begin
  FreeAndNil(FValues);
  inherited;
end;

procedure TrmOPEEnum.SetValues(const AValue: TStrings);
var
  i: Integer;
  h: String;
begin
  if not Assigned(AValue) then
    FValues.Clear
  else
    FValues.Assign(AValue);

  cbList.Items.BeginUpdate;

  cbList.Sorted := True;
  cbList.Items.Clear;

  if FValues.Count = 0 then
    Exit;

  if Pos('=', FValues[0]) <> 0 then
  begin
    FListType := rmLTValues;
    for i := 0 to FValues.Count - 1 do
    begin
      h := FValues[i];
      GetNextStrValue(h, '=');
      cbList.Items.AddObject(h, Pointer(i));
    end;
  end

  else if FValues.Objects[FValues.Count - 1] <> nil then
  begin
    FListType := rmLTPointers;
    for i := 0 to FValues.Count - 1 do
      cbList.Items.AddObject(FValues[i], Pointer(i));
  end

  else
  begin
    FListType := rmLTPlainList;
    for i := 0 to FValues.Count - 1 do
      cbList.Items.AddObject(FValues[i], Pointer(i));
  end;

  cbList.Items.EndUpdate;
end;

procedure TrmOPEEnum.ShowPropValue;
var
  i: Integer;
begin
  inherited;

  if FListType = rmLTPlainList then
    i := FValues.IndexOf(OldPropertyValue)

  else if FListType = rmLTValues then
    i := FValues.IndexOfName(OldPropertyValue)

  else if FListType = rmLTPointers then
    i := FValues.IndexOfObject(Pointer(Integer(OldPropertyValue)))

  else
    i := -1;

  cbList.ItemIndex := cbList.Items.IndexOfObject(Pointer(i));

  if (cbList.Style = csDropDown) and (cbList.ItemIndex = -1) then
   cbList.Text := OldPropertyValue;
end;

procedure TrmOPEEnum.MakeEnumList(const ANames: array of string; const AValues: array of Integer);
var
  L: TStringList;
  i: Integer;
begin
  L := TStringList.Create;
  try
    for i := Low(ANames) to High(ANames) do
      L.Add(IntToStr(AValues[i]) + '=' + ANames[i]);
    Values := L;  
  finally
    FreeAndNil(L);
  end;
end;

function TrmOPEEnum.NewPropertyValue: Variant;
begin
  if cbList.ItemIndex <> - 1 then
  begin
    if FListType = rmLTPlainList then
      Result := FValues[Integer(Pointer(cbList.Items.Objects[cbList.ItemIndex]))]

    else if FListType = rmLTValues then
      Result := FValues.Names[Integer(Pointer(cbList.Items.Objects[cbList.ItemIndex]))]

    else if FListType = rmLTPointers then
      Result := Integer(Pointer(FValues.Objects[Integer(Pointer(cbList.Items.Objects[cbList.ItemIndex]))]))
  end

  else
    if cbList.Style = csDropDownList then
    begin
      if FListType = rmLTPlainList then
        Result := ''
      else if FListType = rmLTPointers then
        Result := Integer(Pointer(nil));
    end
    else
      Result := cbList.Text;
end;

procedure TrmOPEEnum.cbListChange(Sender: TObject);
begin
  NotifyOfChanges;
end;

function TrmOPEEnum.GetAllowCustomValues: Boolean;
begin
  Result := cbList.Style <> csDropDownList;
end;

procedure TrmOPEEnum.SetAllowCustomValues(const AValue: Boolean);
begin
  if aValue then
    CbList.Style := csDropDown
  else
    CbList.Style := csDropDownList;
end;

end.
 