unit rmOPEDBTableGroupsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rwEngineTypes, rmObjPropertyEditorFrm, StdCtrls, ComCtrls, Mask, wwdbedit,
  Wwdotdot, Wwdbcomb, rwDsgnUtils, rmTypes, rwBasicUtils, ExtCtrls;

type
  TrmdDBTableGroup = class(TCollectionItem)
  private
    FItemID: String;
    FInheritedItem: Boolean;
    FGroupName: String;
    FFields: TCommaDilimitedString;
    FPrintHeader: Boolean;
    FPrintFooter: Boolean;
    FDisabled: Boolean;

    procedure Initialize(const AGroup: IrmDBTableGroup);
    procedure ApplyChanges(const AGroup: IrmDBTableGroup);
  end;

  TrmdDBTableGroups = class(TCollection)
  private
    FPageBreakGroup: TrmdDBTableGroup;

    procedure Initialize(const AGroups: IrmDBTableGroups);
    procedure ApplyChanges(const AGroups: IrmDBTableGroups);
    function  FindItemByID(const AItemID: String): TrmdDBTableGroup;
  end;


  TrmOPEDBTableGroups = class(TrmObjPropertyEditor)
    lvGroups: TListView;
    btnAdd: TButton;
    btnDel: TButton;
    btnUp: TButton;
    btnDown: TButton;
    pnlGroupDetails: TPanel;
    Label1: TLabel;
    edName: TEdit;
    Label2: TLabel;
    cbField1: TwwDBComboBox;
    Label3: TLabel;
    cbField2: TwwDBComboBox;
    Label4: TLabel;
    cbField3: TwwDBComboBox;
    chbHeader: TCheckBox;
    chbFooter: TCheckBox;
    chbInherited: TCheckBox;
    chbPageBreak: TCheckBox;
    chbDisabled: TCheckBox;
    procedure btnAddClick(Sender: TObject);
    procedure lvGroupsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure edNameExit(Sender: TObject);
    procedure edNameKeyPress(Sender: TObject; var Key: Char);
    procedure cbField1Change(Sender: TObject);
    procedure chbHeaderClick(Sender: TObject);
    procedure chbFooterClick(Sender: TObject);
    procedure chbDisabledClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure chbPageBreakClick(Sender: TObject);
  private
    FGroups: TrmdDBTableGroups;
    FCurrentGroup: TrmdDBTableGroup;
    FUpdatingItem: Boolean;
    procedure MakeGroupList;
    procedure SyncGroupList;
    procedure UpdateItem(AGroup: TrmdDBTableGroup);
    procedure SetControlStatus;
  protected
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    procedure MakeFieldList;

    procedure ShowPropValue; override;
    procedure ApplyChanges; override;
  end;


implementation

{$R *.dfm}

{ TrmOPEDBTableGroups }

procedure TrmOPEDBTableGroups.AfterConstruction;
begin
  FGroups := TrmdDBTableGroups.Create(TrmdDBTableGroup);
  inherited;
end;

procedure TrmOPEDBTableGroups.ApplyChanges;
begin
  FGroups.ApplyChanges((TComponent(LinkedObject) as IrmDBTableObject).GetGroups);
end;

procedure TrmOPEDBTableGroups.BeforeDestruction;
begin
  FreeAndNil(FGroups);
  inherited;
end;

procedure TrmOPEDBTableGroups.MakeFieldList;
var
  h: String;
begin
  h := cbField1.Value;
  cbField1.Items.CommaText := (TComponent(LinkedObject) as IrmQueryDrivenObject).GetQuery.GetQBQuery.GetFieldList;
  cbField1.Value := h;

  h := cbField2.Value;
  cbField2.Items.Text := cbField1.Items.Text;
  cbField2.Value := h;

  h := cbField3.Value;
  cbField3.Items.Text := cbField1.Items.Text;
  cbField3.Value := h;
end;

procedure TrmOPEDBTableGroups.MakeGroupList;
begin
  FGroups.Initialize((TComponent(LinkedObject) as IrmDBTableObject).GetGroups);
  SyncGroupList;
end;

procedure TrmOPEDBTableGroups.ShowPropValue;
begin
  inherited;
  MakeFieldList;
  MakeGroupList;
  if lvGroups.Items.Count > 0 then
    lvGroups.Selected := lvGroups.Items[0]
  else
    lvGroups.OnSelectItem(nil, nil, False);
end;



procedure TrmOPEDBTableGroups.UpdateItem(AGroup: TrmdDBTableGroup);
var
  LI: TListItem;
begin
  FUpdatingItem := True;
  LI := lvGroups.FindData(0, AGroup, True, False);

  LI.Caption := AGroup.FGroupName;
  LI.SubItems.Clear;
  LI.SubItems.Add(AGroup.FFields);
  FUpdatingItem := False;

  if lvGroups.Tag = 0 then
    NotifyOfChanges;
end;

procedure TrmOPEDBTableGroups.SetControlStatus;
begin
  if Assigned(FCurrentGroup) then
  begin
    btnUp.Enabled := True;
    btnDown.Enabled := True;

    pnlGroupDetails.Enabled := not FCurrentGroup.FInheritedItem;
    btnDel.Enabled := not FCurrentGroup.FInheritedItem;
  end
  else
  begin
    pnlGroupDetails.Enabled := False;
    btnDel.Enabled := False;
    btnUp.Enabled := False;
    btnDown.Enabled := False;

    edName.Text := '';
    cbField1.Value := '';
    cbField2.Value := '';
    cbField3.Value := '';
    chbHeader.Checked := False;
    chbFooter.Checked := False;
    chbPageBreak.Checked := False;
    chbDisabled.Checked := False;
    chbInherited.Checked := False;
  end;
end;

procedure TrmOPEDBTableGroups.SyncGroupList;
var
  i: Integer;
  LI: TListItem;
begin
  lvGroups.Tag := 1;
  try
    lvGroups.Clear;
    for i := 0 to FGroups.Count -1 do
    begin
      LI := lvGroups.Items.Add;
      LI.Data := FGroups.Items[i];
      UpdateItem(TrmdDBTableGroup(LI.Data));
    end;
  finally
    lvGroups.Tag := 0;
  end;
end;


{ TrmdDBTableGroups }

procedure TrmdDBTableGroups.ApplyChanges(const AGroups: IrmDBTableGroups);
var
  i: Integer;
  Grp: TrmdDBTableGroup;
begin
  AGroups.BeginUpdate;
  try
    //Deleted and update groups
    for i := AGroups.GetItemCount - 1 downto 0 do
    begin
      Grp := FindItemByID(AGroups.GetItemByIndex(i).GetItemID);
      if Grp = nil then
        AGroups.DeleteItem(i)
      else
        Grp.ApplyChanges(AGroups.GetItemByIndex(i) as IrmDBTableGroup);
    end;

    //Add New groups
    for i := 0 to Count - 1 do
    begin
      Grp := TrmdDBTableGroup(Items[i]);
      if Grp.FItemID = '' then
        Grp.ApplyChanges(AGroups.AddItem as IrmDBTableGroup);
    end;

    //Update Group secuence
    for i := 0 to Count - 1 do
      if TrmdDBTableGroup(Items[i]).FItemID <> '' then
        AGroups.GetItemByID(TrmdDBTableGroup(Items[i]).FItemID).SetItemIndex(i);

  finally
    AGroups.EndUpdate;
  end;

  if Assigned(FPageBreakGroup) then
    AGroups.SetPageBreakGroup(FPageBreakGroup.FItemID)
  else
    AGroups.SetPageBreakGroup('');
end;

function TrmdDBTableGroups.FindItemByID(const AItemID: String): TrmdDBTableGroup;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count -1 do
    if AnsiSameText(TrmdDBTableGroup(Items[i]).FItemID, AItemID) then
    begin
      Result := TrmdDBTableGroup(Items[i]);
      break;
    end;
end;

procedure TrmdDBTableGroups.Initialize(const AGroups: IrmDBTableGroups);
var
  i: Integer;
begin
  Clear;
  for i := 0 to AGroups.GetItemCount - 1 do
    TrmdDBTableGroup(Add).Initialize(AGroups.GetItemByIndex(i) as IrmDBTableGroup);

  FPageBreakGroup := FindItemByID(AGroups.GetPageBreakGroupID);
end;


{ TrmdDBTableGroup }

procedure TrmdDBTableGroup.ApplyChanges(const AGroup: IrmDBTableGroup);
begin
  AGroup.SetGroupName(FGroupName);
  AGroup.SetFields(FFields);
  AGroup.SetDisabled(FDisabled);
  AGroup.SetPrintHeaderBand(FPrintHeader);
  AGroup.SetPrintFooterBand(FPrintFooter);
end;

procedure TrmdDBTableGroup.Initialize(const AGroup: IrmDBTableGroup);
begin
  FItemID := AGroup.GetItemID;
  FInheritedItem := AGroup.IsInheritedItem;
  FGroupName := AGroup.GetGroupName;
  FFields := AGroup.GetFields;
  FPrintHeader := AGroup.GetPrintHeaderBand;
  FPrintFooter := AGroup.GetPrintFooterBand;
  FDisabled := AGroup.GetDisabled;
end;

procedure TrmOPEDBTableGroups.btnAddClick(Sender: TObject);
var
  NewGroup: TrmdDBTableGroup;
  LI: TListItem;
begin
  NewGroup := TrmdDBTableGroup(FGroups.Add);
  NewGroup.FGroupName := 'New Group';
  NewGroup.FPrintHeader := True;
  NewGroup.FPrintFooter := True;

  LI := lvGroups.Items.Add;
  LI.Data := NewGroup;
  UpdateItem(NewGroup);
  lvGroups.Selected := LI;
end;

procedure TrmOPEDBTableGroups.lvGroupsSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
var
  Flds: TCommaDilimitedString;
begin
  lvGroups.Tag := 1;

  if not Selected then
    FCurrentGroup := nil

  else
  begin
    FCurrentGroup := TrmdDBTableGroup(Item.Data);

    edName.Text := FCurrentGroup.FGroupName;

    Flds := FCurrentGroup.FFields;

    cbField1.Value := GetNextStrValue(Flds, ',');
    cbField2.Value := GetNextStrValue(Flds, ',');
    cbField3.Value := GetNextStrValue(Flds, ',');

    cbField2.Enabled := cbField1.Value <> '';
    cbField3.Enabled := cbField2.Value <> '';

    chbHeader.Checked := FCurrentGroup.FPrintHeader;
    chbFooter.Checked := FCurrentGroup.FPrintFooter;
    chbPageBreak.Checked := FCurrentGroup = FGroups.FPageBreakGroup; 
    chbDisabled.Checked := FCurrentGroup.FDisabled;
    chbInherited.Checked := FCurrentGroup.FInheritedItem;
  end;

  SetControlStatus;
  lvGroups.Tag := 0;
end;

procedure TrmOPEDBTableGroups.edNameExit(Sender: TObject);
begin
  if not Assigned(FCurrentGroup) then
    Exit;

  FCurrentGroup.FGroupName := edName.Text;
  UpdateItem(FCurrentGroup);
end;

procedure TrmOPEDBTableGroups.edNameKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
  begin
    edName.Text := Trim(edName.Text);
    edName.OnExit(edName);
  end;
end;

procedure TrmOPEDBTableGroups.cbField1Change(Sender: TObject);
var
  Flds: TCommaDilimitedString;
  FldList: array [1..3] of String;
  FldNo: Integer;
  CB: TwwDBComboBox;
  i: Integer;
begin
  if FUpdatingItem or not Assigned(FCurrentGroup) then
    Exit;

  CB := TwwDBComboBox(Sender);

  Flds := FCurrentGroup.FFields;
  FldList[1] := GetNextStrValue(Flds, ',');
  FldList[2] := GetNextStrValue(Flds, ',');
  FldList[3] := GetNextStrValue(Flds, ',');

  FldNo := StrToInt(Copy(CB.Name, 8, 1));
  FldList[FldNo] := CB.Value;

  Flds := '';
  for i := 1 to 3 do
    if FldList[i] = '' then
      break
    else
    begin
      if Flds <> '' then
        Flds := Flds + ',';
      Flds := Flds + FldList[i];
    end;

  FCurrentGroup.FFields := Flds;
  UpdateItem(FCurrentGroup);
  lvGroups.OnSelectItem(lvGroups, lvGroups.Selected, True);
end;

procedure TrmOPEDBTableGroups.chbHeaderClick(Sender: TObject);
begin
  if not Assigned(FCurrentGroup) then
    Exit;

  FCurrentGroup.FPrintHeader := chbHeader.Checked;
  UpdateItem(FCurrentGroup);
end;

procedure TrmOPEDBTableGroups.chbFooterClick(Sender: TObject);
begin
  if not Assigned(FCurrentGroup) then
    Exit;

  FCurrentGroup.FPrintFooter := chbFooter.Checked;
  UpdateItem(FCurrentGroup);
end;

procedure TrmOPEDBTableGroups.chbDisabledClick(Sender: TObject);
begin
  if not Assigned(FCurrentGroup) then
    Exit;

  FCurrentGroup.FDisabled := chbDisabled.Checked;
  UpdateItem(FCurrentGroup);
end;

procedure TrmOPEDBTableGroups.btnDelClick(Sender: TObject);
begin
  FreeAndNil(FCurrentGroup);
  lvGroups.Selected.Free;
  NotifyOfChanges;
  SetControlStatus;
end;

procedure TrmOPEDBTableGroups.btnUpClick(Sender: TObject);
var
  i: Integer;
begin
  if FCurrentGroup.Index = 0 then
    Exit;

  FCurrentGroup.Index := FCurrentGroup.Index - 1;
  i := FCurrentGroup.Index;
  SyncGroupList;
  lvGroups.Selected := lvGroups.Items[i];
  NotifyOfChanges;
  SetControlStatus;
end;

procedure TrmOPEDBTableGroups.btnDownClick(Sender: TObject);
var
  i: Integer;
begin
  if FCurrentGroup.Index = FGroups.Count - 1  then
    Exit;

  FCurrentGroup.Index := FCurrentGroup.Index + 1;
  i := FCurrentGroup.Index;
  SyncGroupList;
  lvGroups.Selected := lvGroups.Items[i];
  NotifyOfChanges;
  SetControlStatus;
end;

procedure TrmOPEDBTableGroups.chbPageBreakClick(Sender: TObject);
begin
  if not Assigned(FCurrentGroup) then
    Exit;

  if chbPageBreak.Checked then
    FGroups.FPageBreakGroup := FCurrentGroup
  else
    FGroups.FPageBreakGroup := nil;
    
  UpdateItem(FCurrentGroup);
end;

end.
