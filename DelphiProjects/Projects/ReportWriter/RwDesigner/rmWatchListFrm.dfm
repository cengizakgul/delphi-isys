inherited rmWatchList: TrmWatchList
  Width = 378
  Height = 139
  object lvWatches: TListView
    Left = 0
    Top = 0
    Width = 378
    Height = 139
    Align = alClient
    Columns = <
      item
        Caption = 'Expression'
        MinWidth = 20
        Width = 100
      end
      item
        AutoSize = True
        Caption = 'Value'
      end>
    ColumnClick = False
    RowSelect = True
    PopupMenu = pmOperations
    TabOrder = 0
    ViewStyle = vsReport
  end
  object pmOperations: TPopupMenu
    Left = 160
    Top = 40
    object miAddWatch: TMenuItem
      Caption = 'Add Watch...'
      ShortCut = 45
      OnClick = miAddWatchClick
    end
    object miEditWatch: TMenuItem
      Caption = 'Edit Watch'
      ShortCut = 13
      OnClick = miEditWatchClick
    end
    object miDeleteWatch: TMenuItem
      Caption = 'Delete Watch'
      ShortCut = 46
      OnClick = miDeleteWatchClick
    end
  end
end
