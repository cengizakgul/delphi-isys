inherited rwPCLFilePropertyEditor: TrwPCLFilePropertyEditor
  Left = 433
  Top = 236
  Width = 429
  Height = 385
  Caption = 'PCL File'
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOK: TButton
    Left = 340
    Top = 75
    TabOrder = 3
  end
  inherited btnCancel: TButton
    Left = 340
    Top = 105
    TabOrder = 4
  end
  object memPCL: TMemo
    Left = -1
    Top = 0
    Width = 334
    Height = 358
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object btnLoad: TButton
    Tag = 267
    Left = 340
    Top = 5
    Width = 75
    Height = 23
    Anchors = [akTop, akRight]
    Caption = 'Load'
    TabOrder = 1
    OnClick = btnLoadClick
  end
  object btnSave: TButton
    Tag = 267
    Left = 340
    Top = 33
    Width = 75
    Height = 23
    Anchors = [akTop, akRight]
    Caption = 'Save'
    TabOrder = 2
    OnClick = btnSaveClick
  end
  object OpenDialog: TOpenDialog
    Filter = 
      'PCL File (*.pcl, *.hp, *.sfp, *.spl, *.rlj, *.rpj)|*.pcl;*.hp;*.' +
      'sfp;*.sfl;*.rlj;*.rpj|Soft Font Portrait (*.sfp, *.rpj)|*.sfp;*.' +
      'rpj|Soft Font Landscape (*.sfl, *.rlj)|*.sfl;*.rlj|All Files (*.' +
      '*)|*.*'
    Title = 'Open PCL File'
    Left = 272
    Top = 16
  end
  object SaveDialog: TSaveDialog
    Filter = 
      'PCL File (*.pcl, *.hp, *.sfp, *.spl, *.rlj, *.rpj)|*.pcl;*.hp;*.' +
      'sfp;*.sfl;*.rlj;*.rpj|Soft Font Portrait (*.sfp, *.rpj)|*.sfp;*.' +
      'rpj|Soft Font Landscape (*.sfl, *.rlj)|*.sfl;*.rlj|All Files (*.' +
      '*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save PCL File'
    Left = 272
    Top = 56
  end
end
