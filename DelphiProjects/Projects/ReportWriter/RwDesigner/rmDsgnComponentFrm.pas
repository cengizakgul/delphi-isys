unit rmDsgnComponentFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ImgList, ComCtrls, rmReport, rmDsgnAreaFrm,
  rmDsgnTypes, rmCtrlResizer, rwUtils, rwBasicUtils, Menus, rwQBInfo,
  rwDsgnUtils, rwDB, evStreamUtils,
  Clipbrd, rwGraphics, rmTypes, rmCommonClasses,
  ISBasicClasses, rwBasicClasses, rmLibCompChooseFrm, rwEngineTypes, rwEngine,
  rwDesigner, rwMessages, StrUtils, Math, rwDebugInfo,
  rmDsgnVariablesFrm, rmDsgnFunctionsFrm, rmDsgnComponentsFrm,
  rmDsgnApartmentFrm, rmCustomFrameFrm, rmLocalToolBarFrm,
  rmTBDsgnComponentFrm, rmCustomStatusBarFrm, rmFMLExprPanelFrm,
  rwDesignClasses, ColorPicker, ActnList;

type
  TrmDsgnComponent = class;

  TrwSelectedObjects = class(TList)
  private
    FDsgnComponent: TrmDsgnComponent;
    function  GetItem(Index: Integer): IrmDesignObject;
    function  GetResizer(Index: Integer): TrmControlResizer;
    procedure RepaintResizers;
  public
    property Items[Index: Integer]: IrmDesignObject read GetItem; default;
    property Resizers[Index: Integer]: TrmControlResizer read GetResizer;

    procedure Clear; override;
    function  Add(AComponent: IrmDesignObject): Integer;
    procedure Delete(Index: Integer); overload;
    procedure Delete(AResizer: TrmControlResizer); overload;
    function  IndexOfObject(AObject: IrmDesignObject): Integer;
    function  ListToVarArray: Variant;
  end;


  TrwPasteReader = class(TReader)
  private
    FSubstCompNamesOnPaste: TStringList;
    FContainer: IrmDesignObject;
    FCreatedObjects: TList;
    procedure ReaderOnSetName(Reader: TReader; Component: TComponent; var Name: string);
    procedure ReaderOnFindMethod(Reader: TReader; const MethodName: string; var Address: Pointer; var Error: Boolean);
    procedure ReaderOnReferenceName(Reader: TReader; var Name: string);
    procedure ReaderComponentReadCallback(Component: TComponent);
    procedure AncestorNotFound(Reader: TReader; const ComponentName: string; ComponentClass: TPersistentClass;
                               var Component: TComponent);
  public
    constructor Create(Data: TStream);
    destructor  Destroy; override;
    procedure   ApplyPaste(AOwner: IrmDesignObject; AContainer: IrmDesignObject);
    property    CreatedObjects: TList read FCreatedObjects;
  end;


  TrmDsgnComponent = class(TrmDsgnApartment, IrmDsgnComponent, IrmDesigner)
    imlMainForm: TImageList;
    pmReport: TPopupMenu;
    miAddPrintForm: TMenuItem;
    miReportProperties: TMenuItem;
    N1: TMenuItem;
    pnlList: TPanel;
    lvItems: TListView;
    miAddInputForm: TMenuItem;
    miDelete: TMenuItem;
    miAddFromLibrary: TMenuItem;
    opdImport: TOpenDialog;
    svdExport: TSaveDialog;
    miAddASCIIForm: TMenuItem;
    pnlWorkArea: TPanel;
    miAddXMLForm: TMenuItem;
    procedure lvItemsSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure miAddPrintFormClick(Sender: TObject);
    procedure miReportPropertiesClick(Sender: TObject);
    procedure miAddInputFormClick(Sender: TObject);
    procedure miDeleteClick(Sender: TObject);
    procedure miAddFromLibraryClick(Sender: TObject);
    procedure pmReportPopup(Sender: TObject);
    procedure miAddASCIIFormClick(Sender: TObject);
    procedure lvItemsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure lvItemsEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure lvItemsCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure miAddXMLFormClick(Sender: TObject);
    procedure lvItemsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    FCurrentDesignArea: TrmDsgnArea;
    FApplyingSizeChanges: Boolean;
    FCreatingComponentClass: String;
    FPrintFormDesignerClass: TrmDsgnAreaClass;
    FInputFormDesignerClass: TrmDsgnAreaClass;
    FASCIIFormDesignerClass: TrmDsgnAreaClass;
    FXMLFormDesignerClass:   TrmDsgnAreaClass;
    FNonVisualObjectDesignerClass: TrmDsgnAreaClass;
    FVariablesDesignerClass: TrmDsgnAreaClass;
    FFunctionsDesignerClass: TrmDsgnAreaClass;
    FComponentsDesignerClass: TrmDsgnAreaClass;
    procedure   SelectReportItem(const AName: String);

    procedure   ChangePropertyOfSelObjects(const APropName: String; const APropValue: Variant);
    procedure   OnDestroyResizer(AResizer: TObject);
    procedure   OnResizeRWObject(AObject: IrmDesignObject);
    procedure   OnQueryChanged(AQuery: IrmDesignObject);
    procedure   OnSelectObject(const AObjectList: Variant);
    procedure   ShowPropertyEditorForSelObjects;
    procedure   AddForm(const AClassName: String);
    procedure   DeleteForm;
    procedure   AddFormsFromLibrary;
    procedure   PrepareObjectForDesign(AObject: IrmDesignObject);
    procedure   QueryBuilder;
    procedure   CopySelectedObjectsToClipboard(const CutOperation: Boolean);
    procedure   PasteObjectsFromClipboard;
    procedure   ZoomAction(ZoomKf: Extended);
    procedure   AdjustInsertionIntoLibComponent(AComponent: IrmDesignObject; AParent: IrmDesignObject);
    procedure   SetCreateComponentMode(const AClassName: String);
    procedure   NotifySelectedObjectListChanged;
    procedure   CheckDesignContextFor(AObject: IrmDesignObject);
    procedure   PositionsAlign(const APropName: String; const APropValue: Variant);

    procedure   DMDockingStateChanged(var Message: TrmMessageRec); message  mDockingStateChanged;
    procedure   DMComponentCreationStarted(var Message: TrmMessageRec); message mComponentCreationStarted;
    procedure   DMObjectPropertyChanged(var Message: TrmMessageRec); message mObjectPropertyChanged;
    procedure   DMTBDelete(var Message: TrmMessageRec); message mtbDelete;
    procedure   DMTBCopy(var Message: TrmMessageRec); message mtbCopy;
    procedure   DMTBCut(var Message: TrmMessageRec); message mtbCut;
    procedure   DMTBPaste(var Message: TrmMessageRec); message mtbPaste;
    procedure   DMTBChangeObjectProperty(var Message: TrmMessageRec); message mtbChangeObjectProperty;
    procedure   DMTBOpen(var Message: TrmMessageRec); message mtbOpen;
    procedure   DMTBSave(var Message: TrmMessageRec); message mtbSave;
    procedure   DMTBShowPropertyEditor(var Message: TrmMessageRec); message mtbShowPropertyEditor;
    procedure   DMTBShowObjectsTree(var Message: TrmMessageRec); message mtbShowObjectsTree;
    procedure   DMTBShowEventsEditor(var Message: TrmMessageRec); message mtbShowEventsEditor;
    procedure   DMTBShowShowObjectInspector(var Message: TrmMessageRec); message mtbShowObjectInspector;
    procedure   DMTBCompile(var Message: TrmMessageRec); message mtbCompile;
    procedure   DMTBZoomChange(var Message: TrmMessageRec); message mtbZoomChange;
    procedure   DMTBShowQueryBuilder(var Message: TrmMessageRec); message mtbShowQueryBuilder;
    procedure   DMTBShowFormulaEditor(var Message: TrmMessageRec); message mtbShowFormulaEditor;
    procedure   DMTBShowAggrFunctEditor(var Message: TrmMessageRec); message mtbShowAggrFunctEditor;
    procedure   DMTBAlignPositions(var Message: TrmMessageRec); message mtbAlignPositions;
    procedure   DMTBShowBookmarks(var Message: TrmMessageRec); message mtbShowBookmarks;
    procedure   DMTBShowAllEventsCode(var Message: TrmMessageRec); message mtbShowAllEventsCode;
    procedure   DMTBShowAllFormulasCode(var Message: TrmMessageRec); message mtbShowAllFormulasCode;

  protected
    FSelectedObjects: TrwSelectedObjects;
    FComponent: IrmDesignObject;
    FReportWraper: IrmDesignObject;  // internal report-object for designing purpose
    FDefaultFileName: String;
    FActionHandler: IrmDsgnActionHandler;

    function  GetOwnerForNewComponent(ANewParent: IrmDesignObject): IrmDesignObject; virtual;
    function  GetParentForNewComponent(ANewParent: IrmDesignObject; const ANewObjClassName: String): IrmDesignObject; virtual;
    procedure OpenComponentFromFile; virtual;
    procedure SaveComponentToFile; virtual;
    procedure OpenFromFile(const AFileName: String); virtual;
    procedure SaveToFile(const AFileName: String); virtual;
    procedure SetUniqueName(AComponent: IrmDesignObject);
    function  IsInheritedInternalObjects(AObject: IrmDesignObject): Boolean; virtual;
    procedure Compile; virtual;
    function  GetToolbarClass: TrmLocalToolBarCalss; override;
    function  GetStatusBarClass: TrmCustomStatusBarCalss; override;
    function  GetCaption: String; override;
    procedure DestroyDsgnComponent; virtual;
    procedure PrepareDesigner; virtual;
    procedure UnPrepareDesigner;
    function  GetDesignParent(const AName: String): TWinControl;
    function  DispatchDesignMsg(Sender: IrmDesignObject; var Message: TMessage): Boolean;
    procedure DeleteSelection;
    function  DesignAreaType: TrmDsgnAreaType;
    function  IsObjectSelected(AObject: IrmDesignObject): Boolean;

    // Interface IrmDsgnComponent implementation
    procedure GetListeningMessages(const AMessages: TList); override;
    procedure NewDesignComponent(const AClassName: String); virtual;
    function  DesigningObject: IrmDesignObject;
    procedure Notify(const AEventType: TrmDesignEventType; const AParam: Variant); virtual;
    procedure RepaintCurrentObject;
    function  CurrentDesignArea: TrmDsgnArea;
    function  CreateRWObject(const AClassName: String; AParent: IrmDesignObject; APos: TPoint): IrmDesignObject;
    function  GroupSelection: Boolean;
    procedure ApplyResizerChanges(dLeft, dTop, dWidth, dHeight: Integer);
    procedure ApplyGroupResize(dLeft, dTop, dWidth, dHeight: Integer);
    procedure ApplyResizerState(AState: TrmResizerState);
    procedure RealignResizers;
    function  GetZoomFactor: Extended;
    function  GetObjectCanvasDPI: Integer;
    procedure SelectObject(AObject: IrmDesignObject);
    function  UnSelectObject(AObject: IrmDesignObject): Boolean;
    procedure UnselectAll;
    function  GetSelectedObjects: TrmListOfObjects;
    function  IsDragAndDropMode: Boolean;
    procedure LoadComponentFromStream(const AStream: TStream);
    procedure SaveComponentToStream(const AStream: TStream);
    function  GetSecurityInfo: TrmdSecurityRec; virtual;
    function  ReadOnlyMode: Boolean; virtual;
    procedure ChangeDesignContext(const ANewContextName: String);

   // Interface IrmDesigner implementation
    function  InputFormDesigninig: Boolean;
    function  PrintFormDesigninig: Boolean;
    function  ASCIIFormDesigninig: Boolean;
    function  XMLFormDesigninig: Boolean;
    function  LibComponentDesigninig: Boolean; virtual;
    procedure ObjectPropertyChanged(const AObject: IrmDesignObject; const APropertyName: String);
    procedure GetObjectPicture(const AClassName: String; AImage: TBitmap);
    function  GetDesignerSettings: TrmDesignerSettingsRec;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;

implementation

{$R *.dfm}

uses rmDsgnInputFormFrm, rmDsgnPrintFormFrm, rmDsgnASCIIFormFrm, rmDsgnXMLFormFrm, rmDsgnNonVisualObjectFrm,
     rwQueryBuilderFrm, rmFMLExprEditorFormFrm, rmFMLFunctEditorFormFrm,
     rwPagePropertyEditorFrm, rmObjectPropFormFrm, rmTrmPrintTextPropFrm,
     rmTrmTableCellPropFrm, rmTrmlCustomControlPropFrm,
     rmTrmlQueryDBGridPropFrm, rmTrmlCaptionControlPropFrm, rmTrmlQueryDBComboBoxPropFrm,
     rmTrmlDateTimeComboBoxPropFrm, rmTrmPrintControlPropFrm,
     rmTrmPrintContainerPropFrm, rmTrmTablePropFrm, rmTrmDBTablePropFrm,
     rmTrmPrintImagePropFrm, rmTrmPrintFormPropFrm, rmTrwTabSheetPropFrm, rmTrwGroupBoxPropFrm,
     rmTrwTextPropFrm, rmTrmlComboBoxPropFrm, rmTrmlCBFieldValuesPropFrm,
     rmTrmlGRFieldValuesPropFrm, rmTrmlCheckBoxPropFrm, rmTrmDBTableBandPropFrm,
     rmTrmlDateRangePropFrm, rmTrmASCIIFieldPropFrm, rmTrmASCIIRecordPropFrm, rmTrmASCIIFormPropFrm,
     rmTrmXMLFormPropFrm, rmTrmXMLelementPropFrm, rmTrmXMLattributePropFrm,
     rmTrmXMLSimpleTypeByRestrictionPropFrm, rmTrmXMLQueryElementPropFrm, rmTrmXMLchoicePropFrm,
     rwDsgnRes, rmDockableFrameFrm, rmObjectTreeViewerFrm, rmObjectEventsEditorFrm, rmSBDsgnComponentFrm,
     rmObjectInspectorFrm, rmBookmarksFrm, rmAllEventsFrm, rmAllFormulasFrm;


{ TrwSelectedObjects }

function TrwSelectedObjects.Add(AComponent: IrmDesignObject): Integer;
var
  lCtrlResizer: TrmControlResizer;
begin
  lCtrlResizer := TrmControlResizer.Create(FDsgnComponent);
  Result := inherited Add(lCtrlResizer);
  lCtrlResizer.Attach(AComponent);

  if Count > 1 then
    RepaintResizers;
end;

procedure TrwSelectedObjects.Clear;
var
  i: Integer;
begin
  for i := Count - 1 downto 0 do
    TrmControlResizer(inherited Items[i]).Free;

  inherited;
end;

procedure TrwSelectedObjects.Delete(Index: Integer);
begin
  TrmControlResizer(inherited Items[Index]).Free; //removes itself from the list. See OnDestroyResizer
  if Count = 1 then
    RepaintResizers;
end;

procedure TrwSelectedObjects.Delete(AResizer: TrmControlResizer);
var
  i: Integer;
begin
  i := IndexOf(AResizer);
  inherited Delete(i);
end;

function TrwSelectedObjects.GetItem(Index: Integer): IrmDesignObject;
begin
  Result := TrmControlResizer(inherited Items[Index]).AttachedObject;
end;

function TrwSelectedObjects.GetResizer(Index: Integer): TrmControlResizer;
begin
  Result := TrmControlResizer(inherited Items[Index]);
end;

function TrwSelectedObjects.IndexOfObject(AObject: IrmDesignObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to Count - 1 do
  begin
    if ObjectsAreTheSame(Items[i], AObject) then
    begin
      Result := i;
      break;
    end;
  end;
end;

function TrwSelectedObjects.ListToVarArray: Variant;
var
  i: Integer;
begin
  if Count = 0 then
    Result := VarArrayOf([])
  else
  begin
    Result := VarArrayCreate([0, Count - 1], varInteger);
    for i := 0 to Count - 1 do
      Result[i] := Integer(Pointer(Items[i]));
  end;
end;

procedure TrwSelectedObjects.RepaintResizers;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    Resizers[i].Invalidate;
end;


{ TrmDsgnComponent }

constructor TrmDsgnComponent.Create(AOwner: TComponent);
var
  PictBuf: TBitmap;

  procedure AddPict(const AClassName: String);
  begin
    DsgnRes.RWClassBigPictures.GetClassPicture(AClassName, PictBuf);
    PictBuf.Transparent := True;
    PictBuf.TransparentMode := tmAuto;
    imlMainForm.AddMasked(PictBuf, PictBuf.TransparentColor);
  end;

begin
  inherited;

  Name := Name + IntToStr(GetTickCount);

  FDefaultFileName := 'Component.rwr';

  FSelectedObjects := TrwSelectedObjects.Create;
  FSelectedObjects.FDsgnComponent := Self;

  FApplyingSizeChanges := False;

  FPrintFormDesignerClass := TrmDsgnPrintForm;
  FInputFormDesignerClass := TrmDsgnInputForm;
  FASCIIFormDesignerClass := TrmDsgnASCIIForm;
  FXMLFormDesignerClass   := TrmDsgnXMLForm;
  FNonVisualObjectDesignerClass := TrmDsgnNonVisualObject;
  FVariablesDesignerClass := TrmDsgnVariables;
  FFunctionsDesignerClass := TrmDsgnFunctions;
  FComponentsDesignerClass := TrmDsgnComponents;

  PictBuf := TBitmap.Create;
  try
    AddPict('TrwInputFormContainer');
    AddPict('TrmPrintForm');
    AddPict('TrmASCIIForm');
    AddPict('TrmXMLForm');    
    AddPict('TrwGlobalVarFunct');
  finally
    FreeAndNil(PictBuf);
  end;
end;

procedure TrmDsgnComponent.PrepareDesigner;
var
  Oown, Opar: IrmDesignObject;
  flRemoveFromOwner: Boolean;
  cPos: TPoint;

  procedure InternalPrepareReportMode(AComp: IrmDesignObject);
  var
    i: Integer;
    O: IrmDesignObject;
  begin
    for i := 0 to AComp.GetChildrenCount - 1 do
    begin
      O := AComp.GetChildByIndex(i);
      PrepareObjectForDesign(O);
    end;

    AComp.SetDesigner(Self);
  end;

  procedure InternalPrepareCompMode(AComp: IrmDesignObject);
  var
    i: Integer;
    O: IrmDesignObject;
  begin
    for i := 0 to AComp.GetChildrenCount - 1 do
    begin
      O := AComp.GetChildByIndex(i);
      if O.ObjectType = rwDOTInternal then
        PrepareObjectForDesign(O);
    end;

    AComp.SetDesigner(Self);
  end;

begin
  if DesigningObject.ObjectInheritsFrom('TrmReport') then
  begin
    FReportWraper := nil;
    InternalPrepareReportMode(DesigningObject);
    lvItems.PopupMenu := pmReport;
  end

  else
  begin
    lvItems.PopupMenu := nil;

    // Components live solely in TrmReport object, so we need to create a bogus one

    FReportWraper := CreateRWComponentByClass('TrmReport');
    FReportWraper.SetPropertyValue('Name', 'Report');
    FReportWraper.SetPropertyValue('Description', 'Component''s Dummy Report');

    if DesigningObject.ObjectInheritsFrom('TrwInputFormContainer') or DesigningObject.ObjectInheritsFrom('TrmPrintForm') or
       DesigningObject.ObjectInheritsFrom('TrmASCIIForm') or DesigningObject.ObjectInheritsFrom('TrmXMLForm') then
    begin
      // exception for Input Form, Print Form and ASCII Form
      DesigningObject.SetDesigner(Self);  //Needs for internal puposes of PrintForm (Zoom issue)
      FReportWraper.AddChildObject(DesigningObject);
      InternalPrepareReportMode(FReportWraper);
      FReportWraper.RemoveChildObject(DesigningObject);
      InternalPrepareCompMode(DesigningObject);
    end

    else
    begin
      // All other components go this way
      flRemoveFromOwner := True;
      if DesigningObject.ObjectType = rwDOTInputForm then
      begin
        Oown := FReportWraper.CreateChildObject('TrwInputFormContainer');
        Oown.SetPropertyValue('Name', 'InputForm');
        Opar := Oown;
        cPos := Point(20, 20);
      end

      else if DesigningObject.ObjectType = rwDOTPrintForm then
      begin
        Oown := FReportWraper.CreateChildObject('TrmPrintForm');
        Oown.SetPropertyValue('Name', 'PrintForm');
        cPos := Point(2000, 600);

        if DesigningObject.ObjectInheritsFrom('TrmTableCell') then
        begin
          flRemoveFromOwner := False;
          Opar := Oown.CreateChildObject('TrmTable');
          Opar.SetPropertyValue('Name', 'Table');
          Opar.SetParent(Oown);
          (Opar as IrmTable).ClearTable;
          Oown := Opar;
          Oown.SetLeft(cPos.X);
          Oown.SetTop(cPos.Y);
        end

        else
          Opar := Oown;
      end

      else if DesigningObject.ObjectType = rwDOTASCIIForm then
      begin
        Oown := FReportWraper.CreateChildObject('TrmASCIIForm');
        Oown.SetPropertyValue('Name', 'ASCIIForm');
        cPos := Point(20, 20);

        if DesigningObject.ObjectInheritsFrom('TrmASCIIField') then
        begin
          Opar := Oown.CreateChildObject('TrmASCIIRecord');
          Opar.SetPropertyValue('Name', 'ASCIIRecord');
        end
        else
          Opar := Oown;
      end

      else if DesigningObject.ObjectType = rwDOTXMLForm then
      begin
        Oown := FReportWraper.CreateChildObject('TrmXMLForm');
        Oown.SetPropertyValue('Name', 'XMLForm');
        cPos := Point(20, 20);
        Opar := Oown;
      end

      else
      begin
        Oown := FReportWraper;
        Opar := Oown;
        cPos := Point(100, 100);
      end;

      Oown.SetPropertyValue('Description', 'Component''s Dummy Parent');
      Opar.SetPropertyValue('Description', 'Component''s Dummy Parent');

      if DesigningObject.ObjectType = rwDOTInputForm then
        DesigningObject.SetParent(Opar);       // It needs to be done first, since for some Input Form controls the parent needs to be assigned

      Oown.AddChildObject(DesigningObject);
      InternalPrepareReportMode(FReportWraper);
      if flRemoveFromOwner then
        Oown.RemoveChildObject(DesigningObject);

      DesigningObject.SetLeft(cPos.X);
      DesigningObject.SetTop(cPos.Y);

      DesigningObject.SetParent(Opar);           // Do it one more time, since it is right place to do

      InternalPrepareCompMode(DesigningObject);
    end;
  end;


  if lvItems.Items.Count > 0 then
    SelectReportItem(lvItems.Items[0].Caption);

  if Assigned(FCurrentDesignArea) then
    FCurrentDesignArea.OnResize(nil);

  PostApartmentMessage(Self, mDesignObjectChanged, 0, 0);
end;

procedure TrmDsgnComponent.SelectReportItem(const AName: String);
var
  L: TListItem;
begin
  L := lvItems.FindCaption(0, AName, False, True, False);
  if Assigned(L) then
    L.Selected := True;
end;

procedure TrmDsgnComponent.UnPrepareDesigner;
var
  i: Integer;
begin
  UnselectAll;
  FCurrentDesignArea := nil;
  for i := 0 to lvItems.Items.Count - 1 do
    TrmDsgnArea(lvItems.Items[i].Data).Free;
  lvItems.Clear;
end;

function TrmDsgnComponent.GetDesignParent(const AName: String): TWinControl;
var
  L: TListItem;
begin
  Result := nil;

  L := lvItems.FindCaption(0, AName, False, True, False);
  if Assigned(L) then
  begin
    if Assigned(L.Data) then
      Result := TrmDsgnArea(L.Data).GetDsgnArea;
  end;
end;

function TrmDsgnComponent.DispatchDesignMsg(Sender: IrmDesignObject; var Message: TMessage): Boolean;
var
  ShState: TShiftState;
  Plog, Ppix: TPoint;
  fl: Boolean;

  procedure SendMessageToResizer;
  var
    i: Integer;
    R: TrmControlResizer;
  begin
    if Message.Msg = WM_LBUTTONDOWN then
    begin
      i := FSelectedObjects.IndexOfObject(Sender);
      R := FSelectedObjects.Resizers[i];

      if Sender.DesignBehaviorInfo.MovingDirections <> [] then
      begin
        Message.lParam := MakeLParam(Ppix.X, Ppix.Y);
        PostMessage(R.Handle, Message.Msg, Message.wParam, Message.lParam);
      end;

      if R.CanFocus then
        R.SetFocus;
    end;
  end;

  function DispatchInternalObjectMessage(var Handled: Boolean): Boolean;
  var
    P: TPoint;
    cParent: IrmDesignObject;
  begin
    if IsInheritedInternalObjects(Sender) then
    begin
      Handled := True;
      Result := True;
      cParent := Sender.GetParent;
      if Assigned(cParent) then
      begin
        P := Point(Smallint(Message.lParamLo), Smallint(Message.lParamHi));
        P := Sender.ClientToScreen(P);
        P := cParent.ScreenToClient(P);
        Message.lParam := MakeLong(Word(P.X), Word(P.Y));
        DispatchDesignMsg(cParent, Message);
      end;
    end

    else
      Result := False;
  end;

  procedure GetMousePositionInfo;
  begin
    Plog := Point(Smallint(Message.lParamLo), Smallint(Message.lParamHi));
    if Sender.ObjectType in [rwDOTPrintForm, rwDOTASCIIForm, rwDOTXMLForm] then
      Ppix := Zoomed(Plog)
    else
      Ppix := Plog;
  end;

  function CheckComponentCreation: Boolean;
  var
    P: TPoint;
    lParent, Obj: IrmDesignObject;
    R: TrmControlResizer;
  begin
    Result := False;

    if Message.Msg = WM_LBUTTONDOWN then
    begin
      // Create New RW Object
      if FCreatingComponentClass <> '' then
      begin
        P := Point(Smallint(Message.lParamLo), Smallint(Message.lParamHi));

        lParent := Sender;
        while Assigned(lParent) and not lParent.DesignBehaviorInfo.AcceptControls do
        begin
          Inc(P.X, lParent.GetLeft);
          Inc(P.Y, lParent.GetTop);
          lParent := lParent.GetParent;
        end;

        if Assigned(lParent) then
        begin
          try
            Obj := CreateRWObject(FCreatingComponentClass, lParent, P);
            R := FSelectedObjects.Resizers[0];
            if R.CanFocus then
              R.SetFocus;

            Result := Obj <> nil;
          finally
            SetCreateComponentMode('');
          end;  
        end;
      end;
    end;
  end;


begin
  Result := CheckComponentCreation;
  if Result then
    Exit;

  if Assigned(FCurrentDesignArea) then
  begin
//    if (Message.Msg = WM_LBUTTONDOWN) or (Message.Msg = WM_RBUTTONDOWN) then
//      FCurrentDesignArea.PopupMenuSender := nil;

    if (Message.Msg >= WM_MOUSEFIRST) and (Message.Msg <= WM_MOUSELAST) then
    begin
      if DispatchInternalObjectMessage(Result) then
        Exit;
      GetMousePositionInfo;
    end;


    if Message.Msg = WM_MOUSEMOVE then
    begin
      if Assigned(FActionHandler) then
      begin
        Result := FActionHandler.ObjectMouseMove(Ppix, Sender);
        if Result then
          Exit;
      end;
    end

    else if Message.Msg = WM_RBUTTONDOWN then
    begin
      if Assigned(FActionHandler) then
      begin
        Result := FActionHandler.ObjectMouseDown(mbRight, Ppix, Sender);
        if Result then
          Exit;
      end;
    end

    else if Message.Msg = WM_LBUTTONDOWN then
    begin
      if (Sender.ObjectType in [rwDOTPrintForm, rwDOTInputForm, rwDOTASCIIForm, rwDOTXMLForm]) and (Sender.GetParent = nil) then
      begin
        if not Assigned(FActionHandler) and not IsObjectSelected(Sender) then
          UnselectAll;
      end;

      if Assigned(FActionHandler) then
      begin
        Result := FActionHandler.ObjectMouseDown(mbLeft, Ppix, Sender);
        if Result then
          Exit;
      end;

      // Select Object
      if not Sender.DesignBehaviorInfo.Selectable then
        Exit;

      ShState := KeysToShiftState(Message.WParam);
      if ssShift in ShState then
      begin
        if not UnselectObject(Sender) then
          SelectObject(Sender);
      end
      else
      begin
        fl := IsObjectSelected(Sender);
        if not (GroupSelection and fl) then
        begin
          if not fl then
          begin
            UnselectAll;
            SelectObject(Sender);
          end;
        end;
        SendMessageToResizer;
      end;

      Result := True;
    end

    else if Message.Msg = WM_LBUTTONUP then
    begin
      if Assigned(FActionHandler) then
      begin
        Result := FActionHandler.ObjectMouseUp(mbLeft, Ppix, Sender);
        if Result then
          Exit;
      end;
    end

    else if (Message.Msg = WM_RBUTTONUP) or (Message.Msg = WM_LBUTTONDBLCLK) then
    begin
      if Assigned(FActionHandler) then
      begin
        if Message.Msg = WM_RBUTTONUP then
          Result := FActionHandler.ObjectMouseUp(mbRight, Ppix, Sender)
        else
          Result := FActionHandler.ObjectMouseDblClick(mbLeft, Ppix, Sender);
        if Result then
          Exit;
      end;

      if Message.Msg = WM_RBUTTONUP then
      begin
        if FSelectedObjects.Count = 1 then
          Sender := FSelectedObjects[0];  // temporary

        if Sender.ObjectType in [rwDOTPrintForm, rwDOTASCIIForm, rwDOTXMLForm] then
          Ppix := UnZoomed(Ppix);
        Ppix := Sender.ClientToScreen(Ppix);

        FCurrentDesignArea.ShowPopupMenu(Sender, Ppix.X, Ppix.Y);
      end

      else
        ShowObjectProperties(Sender, FCurrentDesignArea);
    end

    else if Message.Msg = WM_SETCURSOR then
    begin
      if Assigned(FActionHandler) then
      begin
        Result := FActionHandler.ObjectSetCursor(Sender);
        if Result then
          Exit;
      end;
    end;
  end;

  if Sender.ObjectInheritsFrom('TrmTableCell') then
    Result := False;
end;

function TrmDsgnComponent.CurrentDesignArea: TrmDsgnArea;
begin
  Result := FCurrentDesignArea;
end;

function TrmDsgnComponent.CreateRWObject(const AClassName: String; AParent: IrmDesignObject; APos: TPoint): IrmDesignObject;
begin
  UnselectAll;

  AParent := GetParentForNewComponent(AParent, AClassName);

  if Assigned(AParent) and not((TrwDsgnLockState(Integer(AParent.GetPropertyValue('DsgnLockState'))) = rwLSUnlocked) or LibComponentDesigninig) then
  begin
    Result := nil;
    Exit;
  end;

  Result := CurrentDesignArea.CreateRWObject(AClassName, GetOwnerForNewComponent(AParent));
  if not Assigned(Result) then
    Exit;

  if Result.ObjectType = rwDOTNotVisual then
  begin
    if Assigned(AParent) then
    begin
      APos := AParent.ClientToScreen(APos);
      APos := CurrentDesignArea.ScreenToClient(APos);
    end;
    Result.SetLeft(APos.X);
    Result.SetTop(APos.Y);
  end
  
  else if Result.GetParent = nil then
  begin
    Result.SetLeft(APos.X);
    Result.SetTop(APos.Y);
    AdjustInsertionIntoLibComponent(Result, AParent);  // In case when control gets inserted into lib component (an analog of Delphi's Frame)
    Result.SetParent(AParent);
  end;

  SelectObject(Result);
end;

destructor TrmDsgnComponent.Destroy;
begin
  FActionHandler := nil;
  DestroyDsgnComponent;
  FreeAndNil(FSelectedObjects);
  inherited;
end;

procedure TrmDsgnComponent.SelectObject(AObject: IrmDesignObject);
var
  i: Integer;
  P: IrmDesignObject;
begin
  CheckDesignContextFor(AObject);

  P := AObject.GetParent;
  for i := 0 to FSelectedObjects.Count - 1 do
    if not ObjectsAreTheSame(FSelectedObjects[i].GetParent, P) then
    begin
      UnselectAll;
      break;
    end;

  // Special selection logic for table cells
  if AObject.ObjectInheritsFrom('TrmTableCell') then
  begin
    if FSelectedObjects.Count = 2 then  // Allows to select only two table cells
      FSelectedObjects.Delete(1);
  end;

  FSelectedObjects.Add(AObject);
  OnSelectObject(FSelectedObjects.ListToVarArray);
end;

procedure TrmDsgnComponent.UnselectAll;
begin
  if FSelectedObjects.Count > 0 then
  begin
    FSelectedObjects.Clear;
    NotifySelectedObjectListChanged;
  end;    
end;

procedure TrmDsgnComponent.OnDestroyResizer(AResizer: TObject);
begin
  FSelectedObjects.Delete(TrmControlResizer(AResizer));
end;

function TrmDsgnComponent.GroupSelection: Boolean;
begin
  Result := FSelectedObjects.Count > 1;
end;


function TrmDsgnComponent.UnSelectObject(AObject: IrmDesignObject): Boolean;
var
  i: Integer;
begin
  i := FSelectedObjects.IndexOfObject(AObject);
  if i <> -1 then
  begin
    FSelectedObjects.Delete(i);
    Result := True;
    NotifySelectedObjectListChanged;
  end
  else
    Result := False;
end;

procedure TrmDsgnComponent.ApplyResizerChanges(dLeft, dTop, dWidth, dHeight: Integer);
var
  i, dL, dT, dW, dH: Integer;
  C: IrmDesignObject;
  R: TRect;
begin
  FApplyingSizeChanges := True;
  try
    for i := 0 to FSelectedObjects.Count - 1 do
    begin
      C := FSelectedObjects[i];

      if not C.DesignBehaviorInfo.Selectable then
        continue;

      if C.ObjectType in [rwDOTPrintForm, rwDOTASCIIForm, rwDOTXMLForm] then
      begin
        dL := UnZoomed(dLeft);
        dT := UnZoomed(dTop);
        dW := UnZoomed(dWidth);
        dH := UnZoomed(dHeight);
      end
      else
      begin
        dL := dLeft;
        dT := dTop;
        dW := dWidth;
        dH := dHeight;
      end;

      R := C.GetBoundsRect;
      Inc(R.Right, dW);
      Inc(R.Bottom, dH);
      OffsetRect(R, dL, dT);
      C.SetBoundsRect(R);
      PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(C)), '');
    end;

  finally
    FApplyingSizeChanges := False;
  end;
end;

procedure TrmDsgnComponent.ApplyGroupResize(dLeft, dTop, dWidth, dHeight: Integer);
var
  i: Integer;
  R: TrmControlResizer;
begin
  R := nil;

  for i := 0 to FSelectedObjects.Count - 1 do
  begin
    R := FSelectedObjects.Resizers[i];
    with R do
      SetBounds(Left + dLeft, Top + dTop, Width + dWidth, Height + dHeight);
  end;

  if Assigned(R) then
    R.Parent.Update;
end;

procedure TrmDsgnComponent.ApplyResizerState(AState: TrmResizerState);
var
  i: Integer;
begin
  for i := 0 to FSelectedObjects.Count - 1 do
    FSelectedObjects.Resizers[i].SetResizerState(AState);
end;


procedure TrmDsgnComponent.DeleteSelection;
var
  C: IrmDesignObject;
  O: TObject;
  i: Integer;
  fl: Boolean;
  SelObj: TrmListOfObjects;
begin
  SelObj := GetSelectedObjects;

  // marking inherited and internal objects
  fl := False;
  for i := High(SelObj) downto Low(SelObj) do
  begin
    C := SelObj[i];
    if not C.DesignBehaviorInfo.StandaloneObject or C.IsInheritedComponent or ObjectsAreTheSame(C, DesigningObject) then
    begin
      fl := True;
      SelObj[i] := nil;
    end
    else
      FSelectedObjects.Delete(FSelectedObjects.IndexOfObject(C));
  end;

  C := nil;

  SendApartmentMessage(Self, mSelectedObjectListChanged, 0, 0);

  // delete all not marked objects
  for i := High(SelObj) downto Low(SelObj) do
  begin
    if Assigned(SelObj[i]) then
    begin
      O := SelObj[i].RealObject;
      SelObj[i] := nil;
      FreeAndNil(O);
    end;
  end;

  if fl then
    raise ErwException.Create(ResErrInheritedDelete);

  SelectObject(DesigningObject);
end;

procedure TrmDsgnComponent.Notify(const AEventType: TrmDesignEventType; const AParam: Variant);
begin
  if IsDragAndDropMode and not (AEventType in [rmdActionStoped]) then
    Exit;

  case AEventType of
    rmdChildrenListChanged:
      if TOperation(AParam[1]) = opInsert then
        PostApartmentMessage(Self, mObjectChildrenListChanged, AParam[0], AParam[1])
      else
        SendApartmentMessage(Self, mObjectChildrenListChanged, AParam[0], AParam[1]);

    rmdQueryChanged:
      OnQueryChanged(IrmDesignObject(Pointer(Integer(AParam))));

    rmdDestroyResizer:
      OnDestroyResizer(Pointer(Integer(AParam)));

    rmdObjectResized:
      OnResizeRWObject(IrmDesignObject(Pointer(Integer(AParam))));

    rmdObjectTagMouseDown:
      begin
        if FSelectedObjects.Count > 0 then
          FSelectedObjects.Resizers[0].Perform(WM_LBUTTONDOWN, 0, MakeLong(Word(AParam[0]), Word(AParam[1])));
      end;

    rmdSelectObject:
      begin
        if not AParam[1] then
          UnselectAll;
        SelectObject(IrmDesignObject(Pointer(Integer(AParam[0]))));
      end;

    rmdObjectSelected:
      OnSelectObject(AParam);

    rmdActionStarted:
      FActionHandler := IrmDsgnActionHandler(Pointer(Integer(AParam)));

    rmdActionStoped:
    begin
      if ObjectsAreTheSame(FActionHandler, IrmDsgnActionHandler(Pointer(Integer(AParam)))) then
        FActionHandler := nil;
//???      UpdateToolBar;
    end;
  end;
end;


procedure TrmDsgnComponent.ChangePropertyOfSelObjects(const APropName: String; const APropValue: Variant);
var
  i: Integer;
  Cells: TrmListOfObjects;
  T: IrmTable;
begin
  if Debugging then
    Exit;

  SetLength(Cells, 0);


  if (FSelectedObjects.Count = 2) and Supports(FSelectedObjects[0], IrmTableCell) then
  begin
    T := (FSelectedObjects[0] as IrmTableCell).GetObjectOwner as IrmTable;
    Cells := T.GetCellsInRect(FSelectedObjects[0] as IrmTableCell, FSelectedObjects[1] as IrmTableCell);

    for i := Low(Cells) to High(Cells) do
    begin
      Cells[i].SetPropertyValue(APropName, APropValue);
      PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(Cells[i])), APropName);
    end;
  end

  else
    for i := 0 to FSelectedObjects.Count - 1 do
    begin
      FSelectedObjects[i].SetPropertyValue(APropName, APropValue);
      PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(FSelectedObjects[i])), APropName);
    end;
end;

procedure TrmDsgnComponent.AddForm(const AClassName: String);
var
  F: IrmDesignObject;
begin
  F := DesigningObject.CreateChildObject(AClassName);
  PrepareObjectForDesign(F);
  F.SetDesigner(Self);
  SelectReportItem(F.GetPropertyValue('Name'));
end;

procedure TrmDsgnComponent.DeleteForm;
var
  O: TObject;
begin
  if FCurrentDesignArea.DesignObject.IsInheritedComponent then
    raise ErwException.Create(ResErrInheritedDelete);

  UnselectAll;
  DesigningObject.RemoveChildObject(FCurrentDesignArea.DesignObject);
  O := FCurrentDesignArea.DesignObject.RealObject;
  FCurrentDesignArea.DesignObject := nil;
  FreeAndNil(O);
  TrmDsgnArea(lvItems.Selected.Data).Free;
  FCurrentDesignArea := nil;
  lvItems.Selected.Free;
end;

procedure TrmDsgnComponent.PrepareObjectForDesign(AObject: IrmDesignObject);

  procedure AddDesignArea(ADsgnClass: TrmDsgnAreaClass; ACaption: String = '');
  var
    F: TrmDsgnArea;
    L: TListItem;
    i, j: Integer;
  begin
    F := ADsgnClass.Create(Self);

    if AObject.ObjectType = rwDOTInternal then
      j := AObject.GetObjectIndex
    else
    begin
      j := 0;
      for i := lvItems.Items.Count - 1 downto 0 do
        if TrmDsgnArea(lvItems.Items[i].Data).AreaType = rmDATInternalObject then
        begin
          j := i;
          break;
        end;

      Inc(j, AObject.GetObjectIndex);
    end;

    L := lvItems.Items.Insert(j);

    if ACaption = '' then
      if AObject.ObjectType <> rwDOTInternal then
        L.Caption := AObject.GetPropertyValue('Name')
      else
        L.Caption := AObject.GetPropertyValue('Description')
    else
      L.Caption := ACaption;

    L.Data := F;
    L.ImageIndex := F.ImageIndex;

    F.Name := 'DesignArea' + IntToStr(Integer(Pointer(F)));
    F.Visible := False;
    F.Parent := pnlWorkArea;
    F.Align := alClient;
    F.DesignObject := AObject;
  end;

begin
  if AObject.ObjectType = rwDOTInternal then
  begin
    if Supports(AObject, IrmVarFunctHolder) then
    begin
      if Assigned((AObject as IrmVarFunctHolder).GetLocalComponents) then
        AddDesignArea(FComponentsDesignerClass, cContextNameComponents);

      if Assigned((AObject as IrmVarFunctHolder).GetLocalFunctions) then
        AddDesignArea(FFunctionsDesignerClass, cContextNameFunctions);

      if Assigned((AObject as IrmVarFunctHolder).GetVariables) then
        AddDesignArea(FVariablesDesignerClass, cContextNameVariables);
    end;
  end

  else if AObject.ObjectType = rwDOTNotVisual then
    AddDesignArea(FNonVisualObjectDesignerClass)

  else if AObject.ObjectInheritsFrom('TrwInputFormContainer') then
    AddDesignArea(FInputFormDesignerClass)

  else if AObject.ObjectInheritsFrom('TrmPrintForm') then
    AddDesignArea(FPrintFormDesignerClass)

  else if AObject.ObjectInheritsFrom('TrmASCIIForm') then
    AddDesignArea(FASCIIFormDesignerClass)

  else if AObject.ObjectInheritsFrom('TrmXMLForm') then
    AddDesignArea(FXMLFormDesignerClass);
end;


procedure TrmDsgnComponent.QueryBuilder;
begin
  if FSelectedObjects.Count = 1 then
    rmQueryBuilderForObject(FSelectedObjects[0]);
end;

procedure TrmDsgnComponent.lvItemsSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
begin
  if Selected and (TrmDsgnArea(Item.Data) <> FCurrentDesignArea) then
  begin
    if Assigned(FCurrentDesignArea) then
    begin
      UnselectAll;
      SetCreateComponentMode('');
      FCurrentDesignArea.HideInIdle;
      FCurrentDesignArea := nil;
    end;

    FCurrentDesignArea := TrmDsgnArea(Item.Data);

    if Assigned(FCurrentDesignArea) then
    begin
      FCurrentDesignArea.SendToBack;

      FCurrentDesignArea.HorzScrollBar.Position := 0;
      FCurrentDesignArea.VertScrollBar.Position := 0;
      FCurrentDesignArea.Show;
      SelectObject(FCurrentDesignArea.DesignObject);

      if Assigned(FCurrentDesignArea.DesignObject) then
        FCurrentDesignArea.DesignObject.Notify(rmdZoomChanged);
    end;

    PostApartmentMessage(Self, mDesignContextChanged, DesignAreaType, 0);
  end;
end;

procedure TrmDsgnComponent.miAddPrintFormClick(Sender: TObject);
begin
  AddForm('TrmPrintForm');
end;

function TrmDsgnComponent.IsInheritedInternalObjects(AObject: IrmDesignObject): Boolean;
var
  O: IrmDesignObject;
begin
  Result := TrwDsgnLockState(Integer(AObject.GetPropertyValue('DsgnLockState'))) <> rwLSUnlocked;

  if not Result then
  begin
    O := AObject.GetObjectOwner;
    Result := not ObjectsAreTheSame(AObject, FCurrentDesignArea.DesignObject) and not ObjectsAreTheSame(O, FCurrentDesignArea.DesignObject) and
              (not Assigned(O) or not O.IsVirtualOwner);
  end;
end;

procedure TrmDsgnComponent.OnResizeRWObject(AObject: IrmDesignObject);
var
  i: Integer;
begin
  if FApplyingSizeChanges then
    Exit;

  i := FSelectedObjects.IndexOfObject(AObject);
  if i <> -1 then
  begin
    FSelectedObjects.Resizers[i].BoundsByControl;
    PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(AObject)), '');
  end

  else
    for i := 0 to AObject.GetControlCount - 1 do
      OnResizeRWObject(AObject.GetControlByIndex(i));
end;

procedure TrmDsgnComponent.OnQueryChanged(AQuery: IrmDesignObject);

  procedure DBTableQueryChange;
  begin
    if MessageDlg('Do you want to create table cells automatically?' + #13 +
                  'All exesting cells WILL BE DELETED! Continue?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      AQuery.CallCustomFunction('UpdateCells', []);
  end;

begin
  if AQuery.ObjectInheritsFrom('TrmDBTableQuery') then
    DBTableQueryChange;
end;

procedure TrmDsgnComponent.miReportPropertiesClick(Sender: TObject);
begin
  ShowObjectProperties(TrmReport(DesigningObject.RealObject), FCurrentDesignArea);
end;

procedure TrmDsgnComponent.CopySelectedObjectsToClipboard(const CutOperation: Boolean);
var
  MS: TEvDualStreamHolder;

  procedure SelectionToStream;
  var
    i: Integer;
    lWriter: TWriter;
  begin
    MS.Clear;
    lWriter := TWriter.Create(MS.RealStream, 1024);
    lWriter.Root := TComponent(FCurrentDesignArea.DesignObject.RealObject); //??? probably need to use Owner of the first selected object 
    try
      for i := 0 to FSelectedObjects.Count - 1 do
      begin
        if IrmDesignObject(FSelectedObjects[i]).DesignBehaviorInfo.StandaloneObject then
          FSelectedObjects[i].WriteComp(lWriter);
      end;
      lWriter.WriteListEnd;
    finally
      lWriter.Free;
    end;
  end;

  procedure StreamToClipboard;
  var
    lhData: THandle;
    lpDataPtr: Pointer;
  begin
    lhData := GlobalAlloc(GMEM_MOVEABLE, MS.Size);
    try
      lpDataPtr := GlobalLock(lhData);

      try
        MS.Position := 0;
        MS.ReadBuffer(lpDataPtr^, MS.Size);
        ClipBoard.Open;
        ClipBoard.SetAsHandle(CF_RWDATA, lhData);
        ClipBoard.AsText := IntToStr(MS.Size);
        ClipBoard.Close;
      finally
        GlobalUnlock(lhData);
      end;

    except
      GlobalFree(lhData);
      raise;
    end;
  end;

begin
  if Assigned(Screen.ActiveControl) and not (Screen.ActiveControl is TrmControlResizer) then
  begin
    if CutOperation then
      Screen.ActiveControl.Perform(WM_CUT, 0, 0)
    else
      Screen.ActiveControl.Perform(WM_COPY, 0, 0);
    exit;
  end;

  MS := TEvDualStreamHolder.Create;
  SelectionToStream;
  StreamToClipboard;

  if CutOperation then
    DeleteSelection;
end;

procedure TrmDsgnComponent.PasteObjectsFromClipboard;
var
  FOwnerForPaste: IrmDesignObject;
  FContainerForPaste: IrmDesignObject;
  MS: TEvDualStreamHolder;

  procedure StreamFromClipboard;
  var
    lhData: THandle;
    lpDataPtr: Pointer;
    llSize: LongInt;
  begin
    MS.Clear;
    Clipboard.Open;
    lhData := Clipboard.GetAsHandle(CF_RWDATA);

    try
      if lhData = 0 then
        Exit;

      lpDataPtr := GlobalLock(lhData);
      llSize := StrToInt(ClipBoard.AsText);
      try
        MS.WriteBuffer(lpDataPtr^, llSize);
      finally
        GlobalUnlock(lhData);
        Clipboard.Close;
      end;

    except
      GlobalFree(lhData);
      raise;
    end;
  end;

  procedure ClipboardToSelection;
  var
    lReader: TrwPasteReader;
    C: IrmDesignObject;
    i: Integer;
  begin
    UnselectAll;
    MS.Position := 0;

    lReader := TrwPasteReader.Create(MS.RealStream);
    try
      lReader.ApplyPaste(FOwnerForPaste, FContainerForPaste);
      FOwnerForPaste.SetDesigner(Self);

      for i := 0 to lReader.CreatedObjects.Count - 1 do
      begin
        C := IrmDesignObject(lReader.CreatedObjects[i]);
        AdjustInsertionIntoLibComponent(C, FContainerForPaste);
        C.SetParent(FContainerForPaste);
        SelectObject(C);
      end;

    finally
      lReader.Free;
    end;
  end;


begin
  if not Clipboard.HasFormat(CF_RWDATA) then
  begin
    if Assigned(Screen.ActiveControl) then
      Screen.ActiveControl.Perform(WM_PASTE, 0, 0);
    Exit;
  end;

  if not(InputFormDesigninig or PrintFormDesigninig or ASCIIFormDesigninig or XMLFormDesigninig) then
    Exit;

  FContainerForPaste := nil;

  if (FSelectedObjects.Count > 0) and (FSelectedObjects[0].ObjectType in [rwDOTPrintForm, rwDOTInputForm, rwDOTASCIIForm, rwDOTXMLForm]) then
    FContainerForPaste := GetParentForNewComponent(FSelectedObjects[0], '');

  FOwnerForPaste := GetOwnerForNewComponent(FContainerForPaste);

  if not Assigned(FContainerForPaste) then
    FContainerForPaste := FOwnerForPaste;

  MS := TEvDualStreamHolder.Create;
  StreamFromClipboard;
  ClipboardToSelection;
end;


procedure TrmDsgnComponent.ZoomAction(ZoomKf: Extended);
var
  prc: Double;
begin
  if CurrentDesignArea = nil then
    Exit;

  with CurrentDesignArea do
  begin
    if ZoomKf < 0 then
    begin
      prc := RoundTo(ZoomFactor * 100 * CanvasToScreenZoomRatio, 1);

      if ZoomKf = -1 then
        prc := prc + 10
      else if ZoomKf = -2 then
        prc := prc - 10;

      ZoomKf := prc / (100 * CanvasToScreenZoomRatio);
    end
    else if ZoomKf = 0 then
      ZoomKf := 1 / CanvasToScreenZoomRatio;

    if ZoomKf > (800 / (100 * CanvasToScreenZoomRatio)) then  //800% max
      ZoomKf := 800 / (100 * CanvasToScreenZoomRatio)
    else if ZoomKf < (10 / (100 * CanvasToScreenZoomRatio)) then  //10% min
      ZoomKf := 10 / (100 * CanvasToScreenZoomRatio);

    if ZoomKf = ZoomFactor then
      Exit;
      
    ZoomFactor := ZoomKf;
  end;

  RealignResizers;
end;

function TrmDsgnComponent.GetZoomFactor: Extended;
begin
  if CurrentDesignArea <> nil then
    Result := CurrentDesignArea.ZoomFactor
  else
    Result := 0; 
end;

procedure TrmDsgnComponent.RealignResizers;
var
  i: Integer;
begin
  for i := 0 to FSelectedObjects.Count - 1 do
    FSelectedObjects.Resizers[i].BoundsByControl;
end;

procedure TrmDsgnComponent.DestroyDsgnComponent;
var
 O: TObject;
begin
  if Assigned(FComponent) then
    FComponent.SetDesigner(nil);

  UnPrepareDesigner;

  if Assigned(FReportWraper) then
    FReportWraper.SetDesigner(nil);

  svdExport.FileName := '';

  if Assigned(FComponent) then
  begin
    O := FComponent.RealObject;
    FComponent := nil;
    if not (csDestroying in ComponentState) then
      SendApartmentMessage(Self, mDesignObjectChanged, 0, 0);
    FreeAndNil(O);
  end;

  if Assigned(FReportWraper) then
  begin
    O := FReportWraper.RealObject;
    FReportWraper := nil;
    FreeAndNil(O);
  end;

  if not (csDestroying in ComponentState) then
  begin
    SetCreateComponentMode('');
    PostApartmentMessage(Self, mDesignContextChanged, DesignAreaType, 0);
  end;
end;

procedure TrmDsgnComponent.OpenComponentFromFile;
begin
  if opdImport.Execute then
  begin
    RWDesignerExt.AppAdapter.StartDesignerProgressBar('Loading...');
    try
     OpenFromFile(opdImport.FileName);
    finally
      RWDesignerExt.AppAdapter.EndDesignerProgressBar;
    end;
  end;
end;

procedure TrmDsgnComponent.SaveComponentToFile;
var
  fl: Boolean;
  h: String;
begin
  SendBroadcastMessage(mDoApplyPendingChanges, 0, 0);
  if svdExport.FileName = '' then
  begin
    svdExport.FileName := FDefaultFileName;
    fl := True;
  end
  else
    fl := False;

  h := svdExport.FileName;
  svdExport.FileName := ChangeFileExt(ExtractFileName(h), '');
  svdExport.InitialDir := ExtractFileDir(h);
  svdExport.DefaultExt := ExtractFileExt(h);

  if svdExport.Execute then
  begin
    RWDesignerExt.AppAdapter.StartDesignerProgressBar('Saving...');
    try
      SaveToFile(svdExport.FileName);
    finally
      RWDesignerExt.AppAdapter.EndDesignerProgressBar;
    end;
  end
  else if fl then
    svdExport.FileName := '';
end;

function TrmDsgnComponent.DesigningObject: IrmDesignObject;
begin
  Result := FComponent;
end;

procedure TrmDsgnComponent.NewDesignComponent(const AClassName: String);
begin
  DestroyDsgnComponent;
  FComponent := CreateRWComponentByClass(RWEngineExt.AppAdapter.SubstDefaultClassName(AClassName));
  SetUniqueName(FComponent);
  PrepareDesigner;
end;

function TrmDsgnComponent.GetOwnerForNewComponent(ANewParent: IrmDesignObject): IrmDesignObject;
begin
  if Assigned(ANewParent) then
    if ANewParent.IsVirtualOwner then
      Result := ANewParent
    else
      Result := ANewParent.GetObjectOwner
  else
    Result := nil;

  if not Assigned(Result) then
    Result := CurrentDesignArea.DesignObject;
end;

function TrmDsgnComponent.DesignAreaType: TrmDsgnAreaType;
begin
  if Assigned(FCurrentDesignArea) then
    Result := FCurrentDesignArea.AreaType
  else
    Result := rmDATNone;
end;

procedure TrmDsgnComponent.OpenFromFile(const AFileName: String);
begin
   DestroyDsgnComponent;
   svdExport.FileName := AFileName;
   FComponent := (LoadRWResFromFile(nil, AFileName) as IrmDesignObject);
   PrepareDesigner;
end;


procedure TrmDsgnComponent.SaveToFile(const AFileName: String);
begin
  SaveRWResToFile(FComponent.RealObject, AFileName);
end;

procedure TrmDsgnComponent.AddFormsFromLibrary;
var
  LibComp: IrmComponent;
begin
  LibComp := ChoiceLibComp(['TrmPrintForm', 'TrwInputFormContainer', 'TrmASCIIForm', 'TrmXMLForm'], DesigningObject, Self);
  if Assigned(LibComp) then
    AddForm(LibComp.GetName);
end;

procedure TrmDsgnComponent.SetUniqueName(AComponent: IrmDesignObject);
var
  h: String;
begin
  h := GetUniqueNameForComponent(TrwComponent(AComponent.RealObject));
  AComponent.SetPropertyValue('Name', h);
end;

procedure TrmDsgnComponent.AdjustInsertionIntoLibComponent(AComponent, AParent: IrmDesignObject);
var
  i: Integer;
  cName: String;
  fl: Boolean;
  C: IrmDesignObject;
begin
// Naturally Delphi's doesn't allow to insert control into Frame
// since it leads to complications later on streaming operations, but...
// Actually TWriter and TReader are written good enough to handle such sort of situations.
// There is only one important rule: names of controls must be unique within Frame
// (both in our case, Frame.Components[] and Frame.Controls[])

  if not AParent.IsLibComponent then
    Exit;

  fl := False;
  cName := AComponent.GetPropertyValue('Name');
  for i := 0 to AParent.GetChildrenCount - 1 do
  begin
    C := AParent.GetChildByIndex(i);
    if not ObjectsAreTheSame(C, AComponent) and  SameText(cName, C.GetPropertyValue('Name')) then
    begin
      fl := True;
      break;
    end;
  end;

  if not fl then
  begin
    for i := 0 to AParent.GetControlCount - 1 do
    begin
      C := AParent.GetControlByIndex(i);
      if not ObjectsAreTheSame(C, AComponent) and SameText(cName, C.GetPropertyValue('Name')) then
      begin
        fl := True;
        break;
      end;
    end;
  end;

  if fl then
  begin
    cName := cName + '_' + IntToStr(Random(MaxInt));
    AComponent.SetPropertyValue('Name', cName);
  end;
end;


function TrmDsgnComponent.GetSelectedObjects: TrmListOfObjects;
var
  i, n: Integer;
begin
  SetLength(Result, FSelectedObjects.Count);

  n := 0;
  for i := 0 to FSelectedObjects.Count - 1 do
    if FSelectedObjects.Resizers[i].ComponentState = [] then
    begin
      Result[n] := FSelectedObjects[i];
      Inc(n);
    end;

  if FSelectedObjects.Count <> n then
    SetLength(Result, n);
end;

procedure TrmDsgnComponent.LoadComponentFromStream(const AStream: TStream);
begin
  DestroyDsgnComponent;
  FComponent := (LoadRWResFromStream(nil, AStream) as IrmDesignObject);
  PrepareDesigner;
end;

procedure TrmDsgnComponent.SaveComponentToStream(const AStream: TStream);
begin
  SaveRWResToStream(FComponent.RealObject, AStream);
end;

procedure TrmDsgnComponent.RepaintCurrentObject;
begin
  CurrentDesignArea.DesignObject.Refresh;
end;

procedure TrmDsgnComponent.OnSelectObject(const AObjectList: Variant);
begin
  NotifySelectedObjectListChanged;

  if VarArrayHighBound(AObjectList, 1) >= 0 then
    ShowObjectPropertiesIfPossible(IrmDesignObject(Pointer(Integer(AObjectList[0]))));
end;

function TrmDsgnComponent.IsObjectSelected(AObject: IrmDesignObject): Boolean;
begin
  Result := FSelectedObjects.IndexOfObject(AObject) <> -1;
end;

function TrmDsgnComponent.IsDragAndDropMode: Boolean;
begin
  if Assigned(FActionHandler) then
    Result := True
  else
    Result := False;
end;

function TrmDsgnComponent.GetObjectCanvasDPI: Integer;
begin
  if CurrentDesignArea <> nil then
    Result := CurrentDesignArea.GetObjectCanvasDPI
  else
    Result := 0;
end;

function TrmDsgnComponent.GetParentForNewComponent(ANewParent: IrmDesignObject; const ANewObjClassName: String): IrmDesignObject;
var
  L: TStringList;
begin
  L := TStringList.Create;
  try
    Result := ANewParent;
    while Assigned(Result) do
      if not Result.DesignBehaviorInfo.AcceptControls then
        Result := Result.GetParent
      else
      begin
        L.CommaText := Result.DesignBehaviorInfo.AcceptedClases;
        if (L.Count = 0) or (L.IndexOf(ANewObjClassName) <> -1) then
          break;
        Result := Result.GetParent
      end;

  finally
    FreeAndNil(L);
  end;
end;

procedure TrmDsgnComponent.ShowPropertyEditorForSelObjects;
begin
  if FSelectedObjects.Count > 0 then
    ShowObjectProperties(FSelectedObjects[0], FCurrentDesignArea)
  else
    ShowObjectProperties(FCurrentDesignArea.DesignObject, FCurrentDesignArea);
end;


function TrmDsgnComponent.ASCIIFormDesigninig: Boolean;
begin
  Result := DesignAreaType = rmDATASCIIForm;
end;

function TrmDsgnComponent.InputFormDesigninig: Boolean;
begin
  Result := DesignAreaType = rmDATInputForm;
end;

function TrmDsgnComponent.LibComponentDesigninig: Boolean;
begin
  Result := False;
end;

function TrmDsgnComponent.PrintFormDesigninig: Boolean;
begin
  Result := DesignAreaType = rmDATPrintForm;
end;

procedure TrmDsgnComponent.ObjectPropertyChanged(const AObject: IrmDesignObject; const APropertyName: String);
begin

end;

procedure TrmDsgnComponent.GetObjectPicture(const AClassName: String; AImage: TBitmap);
begin
  DsgnRes.RWClassSmallPictures.GetClassPicture(AClassName, AImage);
end;

function TrmDsgnComponent.GetDesignerSettings: TrmDesignerSettingsRec;
begin
  Result.AdvancedMode := DesignRMAdvancedMode;
  Result.ShowPaperGrid := DesignRMShowPaperGrid;
  Result.ShowTableGrid := DesignRMShowTableGrid;
end;

procedure TrmDsgnComponent.SetCreateComponentMode(const AClassName: String);
begin
  if AClassName <> '' then
  begin
    FCreatingComponentClass := AClassName;
    Screen.Cursor := crHandPoint;
  end

  else if FCreatingComponentClass <> '' then
  begin
    PostApartmentMessage(Self, mComponentCreationFinished, FCreatingComponentClass, 0);
    Screen.Cursor := crDefault;
    FCreatingComponentClass := AClassName;
  end;
end;


function TrmDsgnComponent.GetToolbarClass: TrmLocalToolBarCalss;
begin
  Result := TrmTBDsgnComponent;
end;

function TrmDsgnComponent.GetCaption: String;
var
  h: String;
begin
  if Assigned(FComponent) then
  begin
    Result := svdExport.FileName;
    if Result = '' then
    begin
      h := FComponent.GetPropertyValue('Description');
      if h <> '' then
        Result := h + '   (' + FComponent.GetClassName + ')'
      else
        Result := FComponent.GetClassName;
    end;    
  end

  else
    Result := inherited GetCaption;
end;

function TrmDsgnComponent.XMLFormDesigninig: Boolean;
begin
  Result := DesignAreaType = rmDATXMLForm;
end;

procedure TrmDsgnComponent.Compile;
begin
  SendBroadcastMessage(mDoApplyPendingChanges, 0, 0);
end;

procedure TrmDsgnComponent.NotifySelectedObjectListChanged;
begin
  PostApartmentMessage(Self, mSelectedObjectListChanged, 0, 0);
end;

procedure TrmDsgnComponent.DMComponentCreationStarted(var Message: TrmMessageRec);
begin
  inherited;
  SetCreateComponentMode(Message.Parameter1);
end;

procedure TrmDsgnComponent.DMTBCopy(var Message: TrmMessageRec);
begin
  inherited;
  CopySelectedObjectsToClipboard(False);
end;

procedure TrmDsgnComponent.DMTBCut(var Message: TrmMessageRec);
begin
  inherited;
  CopySelectedObjectsToClipboard(True)
end;

procedure TrmDsgnComponent.DMTBDelete(var Message: TrmMessageRec);
begin
  inherited;
  DeleteSelection;
  SendApartmentMessage(Self, mObjectsWereDeleted, 0, 0);
end;

procedure TrmDsgnComponent.DMTBPaste(var Message: TrmMessageRec);
begin
  inherited;
  PasteObjectsFromClipboard;
end;

procedure TrmDsgnComponent.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mComponentCreationStarted));
  AMessages.Add(Pointer(mObjectPropertyChanged));
  AMessages.Add(Pointer(mtbDelete));
  AMessages.Add(Pointer(mtbCopy));
  AMessages.Add(Pointer(mtbCut));
  AMessages.Add(Pointer(mtbPaste));
  AMessages.Add(Pointer(mtbChangeObjectProperty));
  AMessages.Add(Pointer(mtbOpen));
  AMessages.Add(Pointer(mtbSave));
  AMessages.Add(Pointer(mtbShowPropertyEditor));
  AMessages.Add(Pointer(mtbShowObjectsTree));
  AMessages.Add(Pointer(mtbShowEventsEditor));
  AMessages.Add(Pointer(mtbShowObjectInspector));
  AMessages.Add(Pointer(mtbCompile));
  AMessages.Add(Pointer(mtbZoomChange));
  AMessages.Add(Pointer(mtbShowQueryBuilder));
  AMessages.Add(Pointer(mtbShowFormulaEditor));
  AMessages.Add(Pointer(mtbShowAggrFunctEditor));
  AMessages.Add(Pointer(mtbAlignPositions));
  AMessages.Add(Pointer(mtbShowBookmarks));
  AMessages.Add(Pointer(mtbShowAllEventsCode));
  AMessages.Add(Pointer(mtbShowAllFormulasCode));
end;

procedure TrmDsgnComponent.DMTBChangeObjectProperty(var Message: TrmMessageRec);
begin
  inherited;
  ChangePropertyOfSelObjects(Message.Parameter1, Message.Parameter2);
end;


procedure TrmDsgnComponent.DMTBSave(var Message: TrmMessageRec);
begin
  inherited;
  SaveComponentToFile;
end;

procedure TrmDsgnComponent.DMTBOpen(var Message: TrmMessageRec);
begin
  inherited;
  OpenComponentFromFile;
end;

procedure TrmDsgnComponent.DMTBShowPropertyEditor(var Message: TrmMessageRec);
begin
  inherited;
  ShowPropertyEditorForSelObjects;
end;

procedure TrmDsgnComponent.DMTBCompile(var Message: TrmMessageRec);
begin
  inherited;
  Compile;
end;

procedure TrmDsgnComponent.DMTBZoomChange(var Message: TrmMessageRec);
begin
  inherited;
  ZoomAction(Extended(Message.Parameter1));
end;

procedure TrmDsgnComponent.DMTBShowQueryBuilder(var Message: TrmMessageRec);
begin
  inherited;
  QueryBuilder;
end;

procedure TrmDsgnComponent.DMTBShowFormulaEditor(var Message: TrmMessageRec);
begin
  inherited;
  if not Assigned(FActionHandler) then
    TrmFMLExprEditorForm.StartAction(VarArrayOf([Message.Parameter1]));
end;

procedure TrmDsgnComponent.DMTBShowAggrFunctEditor(var Message: TrmMessageRec);
begin
  inherited;
  if not Assigned(FActionHandler) then
    TrmFMLFunctEditorForm.StartAction(VarArrayOf([Message.Parameter1]));
end;

function TrmDsgnComponent.GetSecurityInfo: TrmdSecurityRec;
begin
  Result.EditComponents := True;
  Result.EditFunctions := True;
  Result.EditVariables := True;  
end;

procedure TrmDsgnComponent.DMTBShowObjectsTree(var Message: TrmMessageRec);
begin
  inherited;
  ShowDockableForm(TrmObjectTreeViewer);
end;

procedure TrmDsgnComponent.DMDockingStateChanged(var Message: TrmMessageRec);
begin
  inherited;
  pnlList.Left := spltLeft.BoundsRect.Right + 1;
  StatusBar.Top := ClientHeight;
end;

procedure TrmDsgnComponent.DMTBShowEventsEditor(var Message: TrmMessageRec);
begin
  inherited;
  ShowDockableForm(TrmObjectEventsEditor);
end;

function TrmDsgnComponent.GetStatusBarClass: TrmCustomStatusBarCalss;
begin
  Result := TrmSBDsgnComponent;
end;


procedure TrmDsgnComponent.CheckDesignContextFor(AObject: IrmDesignObject);
var
  h: String;
begin
  h := AObject.GetPathFromRootOwner;

  if DesigningObject.GetPropertyValue('name') = GetNextStrValue(h, '.') then
  begin
    h := GetNextStrValue(h, '.');
    ChangeDesignContext(h);
  end;
end;

procedure TrmDsgnComponent.DMTBShowShowObjectInspector(var Message: TrmMessageRec);
begin
  inherited;
  ShowDockableForm(TrmObjectInspector);
end;

procedure TrmDsgnComponent.DMObjectPropertyChanged(var Message: TrmMessageRec);
var
  i: Integer;
  O: IrmDesignObject;
begin
  O := IrmDesignObject(Pointer(Integer(Message.Parameter1)));

  for i := 0 to lvItems.Items.Count - 1 do
    if ObjectsAreTheSame(TrmDsgnArea(lvItems.Items[i].Data).DesignObject, O) then
    begin
      lvItems.Items[i].Caption := O.GetPropertyValue('Name');
      break;
    end;
end;

function TrmDsgnComponent.ReadOnlyMode: Boolean;
begin
  Result := True;
end;

procedure TrmDsgnComponent.ChangeDesignContext(const ANewContextName: String);
var
  L: TListItem;
begin
  L := lvItems.FindCaption(0, ANewContextName, False, True, False);
  if Assigned(L) and (lvItems.Selected <> L) then
  begin
    L.Selected := True;
    UnselectAll;
  end;
end;


procedure TrmDsgnComponent.DMTBAlignPositions(var Message: TrmMessageRec);
begin
  inherited;
  PositionsAlign(Message.Parameter1, Message.Parameter2);
end;

procedure TrmDsgnComponent.PositionsAlign(const APropName: String; const APropValue: Variant);
var
  Al : TPositionAlign;

  procedure PositionsAlignLeftRight;
  var
    i : integer;
    Left : integer;
    X : integer;
  begin
    Left := FSelectedObjects[0].GetLeft;
    X := FSelectedObjects[0].GetLeft + FSelectedObjects[0].GetWidth;
    for i := 1 to FSelectedObjects.Count - 1 do
    begin
      Case AL of
        AlPositionLeft: FSelectedObjects[i].SetLeft(Left);
        AlPositionRight : FSelectedObjects[i].SetLeft(X - FSelectedObjects[i].GetWidth);
      end;
      PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(FSelectedObjects[i])), 'Left');
    end;
  end;

  procedure PositionsAlignTopBottom;
  var
    i : integer;
    Top, Y : integer;
  begin
    Top := FSelectedObjects[0].GetTop;
    Y := FSelectedObjects[0].GetTop + FSelectedObjects[0].GetHeight;
    for i := 1 to FSelectedObjects.Count - 1 do
    begin
      Case AL of
        alPositionTop: FSelectedObjects[i].SetTop(Top);
        alPositionBottom : FSelectedObjects[i].SetTop(Y - FSelectedObjects[i].GetHeight);
      end;
      PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(FSelectedObjects[i])), 'Top');
    end;
  end;

  procedure PositionsAlignCenter;
  var
    i: Integer;
    X, Y: Integer;
  begin
    X := FSelectedObjects[0].GetLeft + (FSelectedObjects[0].GetWidth div 2);
    Y := FSelectedObjects[0].GetTop + (FSelectedObjects[0].GetHeight div 2);
    for i := 1 to FSelectedObjects.Count - 1 do
    begin
      case Al of
        alPositionMiddle:
        begin
          FSelectedObjects[i].SetTop(Y - (FSelectedObjects[i].GetHeight div 2));
          PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(FSelectedObjects[i])), 'Top');
        end;
        alPositionCenter:
        begin
          FSelectedObjects[i].SetLeft(X - (FSelectedObjects[i].GetWidth div 2));
          PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(FSelectedObjects[i])), 'Left');
        end;
      end;
    end;
  end;

  procedure PositionsAlignContainerCenter;
  var
    i: Integer;
  begin
    for i := 0 to FSelectedObjects.Count - 1 do
      case Al of
        alPositionContainerMiddle :
          begin
            FSelectedObjects[i].SetTop((FSelectedObjects[i].GetObjectOwner.GetHeight div 2) - (FSelectedObjects[i].GetHeight div 2));
            PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(FSelectedObjects[i])), 'Top');
          end;

        alPositionContainerCenter :
          begin
            FSelectedObjects[i].SetLeft((FSelectedObjects[i].GetObjectOwner.GetWidth div 2) - (FSelectedObjects[i].GetWidth div 2));
            PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(FSelectedObjects[i])), 'Left');
          end;
      end;
  end;

  procedure PositionsAlignSpaces;
  var
    i, j: Integer;
    lAllSize, lCompSize, lSpaceSize: Integer;
    lList: TrmListOfObjects;
    tmp : IrmDesignObject;
  begin
    lList := GetSelectedObjects;

    case Al of
      alPositionVerticalSpaces:
        begin
          for i := 0 to Length(lList) - 1 do
            for j := i + 1 to Length(lList) - 1 do
              if lList[j].GetTop < lList[i].GetTop then
              begin
                tmp := lList[j];
                lList[j] := lList[i];
                lList[i] := tmp;
              end;

          lAllSize := lList[Length(lList) - 1].GetTop - lList[0].GetTop - lList[0].GetHeight;
          lCompSize := 0;

          for i := 1 to Length(lList) - 2 do
            lCompSize := lCompSize + lList[i].GetHeight;

          lSpaceSize := (lAllSize - lCompSize) div (Length(lList) - 1);

          for i := 1 to Length(lList) - 2 do
          begin
            lList[i].SetTop(lList[i - 1].GetTop + lList[i - 1].GetHeight + lSpaceSize);
            PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(lList[i])), 'Top');
          end;

        end;

      alPositionHorisontalSpaces:
        begin
          for i := 0 to Length(lList) - 1 do
            for j := i + 1 to Length(lList) - 1 do
              if (lList[j].GetLeft < lList[i].GetLeft) then
              begin
                tmp := lList[j];
                lList[j] := lList[i];
                lList[i] := tmp;
              end;

          lAllSize := lList[Length(lList) - 1].GetLeft - lList[0].GetLeft - lList[0].GetWidth;
          lCompSize := 0;

          for i := 1 to Length(lList) - 2 do
            lCompSize := lCompSize + lList[i].GetWidth;

          lSpaceSize := (lAllSize - lCompSize) div (Length(lList) - 1);

          for i := 1 to Length(lList) - 2 do
          begin
            lList[i].SetLeft(lList[i - 1].GetLeft + lList[i - 1].GetWidth + lSpaceSize);
            PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(lList[i])), 'Left');
          end;
        end;
    end;
  end;

begin
  if Debugging or (FSelectedObjects.Count = 0) then
    Exit;
  if Supports(FSelectedObjects[0], IrmTableCell) then
    Exit;

  Al := TPositionAlign(APropValue);
  if (Al in ([alPositionLeft, alPositionTop, alPositionBottom, alPositionRight, alPositionCenter, alPositionMiddle, alPositionVerticalSpaces, alPositionHorisontalSpaces]))
    and (FSelectedObjects.Count < 2) then Exit;

  Case Al of
    alPositionLeft : PositionsAlignLeftRight;
    alPositionRight : PositionsAlignLeftRight;
    alPositionTop : PositionsAlignTopBottom;
    alPositionBottom : PositionsAlignTopBottom;
    alPositionCenter : PositionsAlignCenter;
    alPositionMiddle : PositionsAlignCenter;
    alPositionContainerCenter : PositionsAlignContainerCenter;
    alPositionContainerMiddle : PositionsAlignContainerCenter;
    alPositionVerticalSpaces : PositionsAlignSpaces;
    alPositionHorisontalSpaces : PositionsAlignSpaces;
  end;
end;

procedure TrmDsgnComponent.DMTBShowBookmarks(var Message: TrmMessageRec);
begin
  ShowDockableForm(TrmBookmarksViewer);
end;

procedure TrmDsgnComponent.DMTBShowAllEventsCode(
  var Message: TrmMessageRec);
begin
  SendBroadcastMessage(mDoApplyPendingChanges, 0, 0);
  ShowRMAllEventOfReport((Apartment as IrmDsgnComponent).DesigningObject);
end;

procedure TrmDsgnComponent.DMTBShowAllFormulasCode(
  var Message: TrmMessageRec);
begin
  SendBroadcastMessage(mDoApplyPendingChanges, 0, 0);
  ShowRMAllFormulasOfReport((Apartment as IrmDsgnComponent).DesigningObject);
end;

{ TrwPasteReader }

constructor TrwPasteReader.Create(Data: TStream);
begin
  inherited Create(Data, 1024);
  FSubstCompNamesOnPaste := TStringList.Create;
  FCreatedObjects := TList.Create;
  OnSetName := ReaderOnSetName;
  OnFindMethod := ReaderOnFindMethod;
  OnReferenceName := ReaderOnReferenceName;
  OnAncestorNotFound := AncestorNotFound;
end;

destructor TrwPasteReader.Destroy;
begin
  FreeAndNil(FCreatedObjects);
  FreeAndNil(FSubstCompNamesOnPaste);
  inherited;
end;

procedure TrwPasteReader.AncestorNotFound(Reader: TReader;
  const ComponentName: string; ComponentClass: TPersistentClass; var Component: TComponent);
var
  h: String;
  C: IrmDesignObject;
begin
  C := (Reader.Owner as IrmDesignObject).CreateChildObject(ComponentClass.ClassName);
  Component := TComponent(C.RealObject);
  h := ComponentName;
  ReaderOnSetName(Reader, Component, h);
end;

procedure TrwPasteReader.ReaderComponentReadCallback(Component: TComponent);
var
  C: IrmDesignObject;
begin
  FCreatedObjects.Add(Pointer(Component as IrmDesignObject));

  if Assigned(FContainer) then
  begin
    C := Component as IrmDesignObject;
    if (C.GetLeft + C.GetWidth) > FContainer.GetWidth then
      C.SetLeft(FContainer.GetWidth - C.GetWidth);
    if (C.GetTop + C.GetHeight) > FContainer.GetHeight then
      C.SetTop(FContainer.GetHeight - C.GetHeight);
  end;
end;

procedure TrwPasteReader.ReaderOnFindMethod(Reader: TReader; const MethodName: string; var Address: Pointer; var Error: Boolean);
begin
  Error := False;
end;

procedure TrwPasteReader.ReaderOnReferenceName(Reader: TReader; var Name: string);
var
  h: string;
begin
  h := FSubstCompNamesOnPaste.Values[Name];
  if (h <> '') then
    Name := h;
end;

procedure TrwPasteReader.ReaderOnSetName(Reader: TReader; Component: TComponent; var Name: string);
var
  OldName: string;
  GlobOwner: TrwComponent;
begin
  OldName := Name;
  GlobOwner :=  TrwComponent(Component).GetGlobalOwner;

  if Assigned(GlobOwner) and (GlobOwner.GlobalFindComponent(Name) <> nil) then
    Name := GetUniqueNameForComponent(TrwComponent(Component))
  else if not Assigned(GlobOwner) and (Component.Owner.FindComponent(Name) <> nil) then
    Name := GetUniqueNameForComponent(TrwComponent(Component));

  FSubstCompNamesOnPaste.Add(OldName + '=' + Name);
  
  Component.Name := Name;

//  AfterCreationComponent(TrwComponent(Component), nil, 0, 0);
end;

procedure TrwPasteReader.ApplyPaste(AOwner: IrmDesignObject; AContainer: IrmDesignObject);
begin
  FCreatedObjects.Clear;
  FSubstCompNamesOnPaste.Clear;
  FContainer := AContainer;
  ReadComponents(TComponent(AOwner.RealObject), nil, ReaderComponentReadCallback);
end;


procedure TrmDsgnComponent.miAddInputFormClick(Sender: TObject);
begin
  AddForm('TrwInputFormContainer');
end;

procedure TrmDsgnComponent.miDeleteClick(Sender: TObject);
begin
  if Assigned(lvItems.Selected) then
    DeleteForm;
end;

procedure TrmDsgnComponent.miAddFromLibraryClick(Sender: TObject);
begin
  AddFormsFromLibrary;
end;

procedure TrmDsgnComponent.pmReportPopup(Sender: TObject);
begin
  miDelete.Visible := Assigned(lvItems.Selected) and (DesignAreaType <> rmDATInternalObject);

  if Assigned(lvItems.Selected) then
  begin
    if DesignAreaType = rmDATInputForm then
      miDelete.Caption := 'Delete Input Form'
    else if DesignAreaType = rmDATPrintForm then
      miDelete.Caption := 'Delete Print Form'
    else if DesignAreaType = rmDATASCIIForm then
      miDelete.Caption := 'Delete ASCII Form'
    else if DesignAreaType = rmDATXMLForm then
      miDelete.Caption := 'Delete XML Form';
  end;
end;

procedure TrmDsgnComponent.miAddASCIIFormClick(Sender: TObject);
begin
  AddForm('TrmASCIIForm');
end;

procedure TrmDsgnComponent.lvItemsDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
  DropItem: TListItem;

  function AcceptObject(Obj: IrmDesignObject): Boolean;
  begin
    Result := not(Obj.ObjectInheritsFrom('TrwInputFormContainer') or (Obj.ObjectType = rwDOTInternal) or  Obj.IsInheritedComponent);
  end;

begin
  DropItem := lvItems.GetItemAt(X, Y);
  Accept := Assigned(DropItem) and  AcceptObject(TrmDsgnArea(DropItem.Data).DesignObject) and
            AcceptObject(FCurrentDesignArea.DesignObject);
end;

procedure TrmDsgnComponent.lvItemsEndDrag(Sender, Target: TObject; X, Y: Integer);
var
  DropItem, DragItem, NewItem: TListItem;
  i: Integer;
  O: IrmDesignObject;
begin
  if Assigned(Target) then
  begin
    DropItem := lvItems.GetItemAt(X, Y);
    DragItem := lvItems.Selected;
    if Assigned(DropItem) and (DragItem <> DropItem) then
    begin
      O := TrmDsgnArea(DropItem.Data).DesignObject;
      i := DropItem.Index;
      if i > DragItem.Index then
        Inc(i);

      FCurrentDesignArea.DesignObject.SetObjectIndex(O.GetObjectIndex);

      NewItem := lvItems.Items.Insert(i);
      NewItem.Assign(DragItem);
      DragItem.Free;
      NewItem.Selected := True;
    end;
  end;
end;

procedure TrmDsgnComponent.lvItemsCompare(Sender: TObject; Item1, Item2: TListItem; Data: Integer; var Compare: Integer);
begin
  if Item1.Index < Item2.Index then
    Compare := -1
  else if Item1.Index > Item2.Index then
    Compare := 1
  else
    Compare := 0;
end;


procedure TrmDsgnComponent.miAddXMLFormClick(Sender: TObject);
begin
  AddForm('TrmXMLForm');
end;

procedure TrmDsgnComponent.lvItemsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Itm: TListItem;
begin
  Itm := lvItems.GetItemAt(X, Y);

  if Itm = nil then
  begin
    if not IsObjectSelected(DesigningObject) then
    begin
      UnselectAll;
      SelectObject(DesigningObject);
    end;
  end

  else if Itm = lvItems.Selected then
    if not IsObjectSelected(CurrentDesignArea.DesignObject) then
    begin
      UnselectAll;
      SelectObject(CurrentDesignArea.DesignObject);
    end;  
end;

initialization
  RegisterClasses([TrmTrmPrintTextProp, TrmTrmTableCellProp,
                   TrmTrmlCustomControlProp, TrmTrmlQueryDBGridProp,
                   TrmTrmlCaptionControlProp, TrmTrmlQueryDBComboBoxProp,
                   TrmTrmlDateTimeComboBoxProp, TrmTrmPrintControlProp,
                   TrmTrmPrintContainerProp, TrmTrmTableProp, TrmTrmDBTableProp, TrmTrmDBTableBandProp,
                   TrmTrmPrintImageProp, TrmTrmPrintFormProp,
                   TrmTrwTabSheetProp, TrmTrwGroupBoxProp, TrmTrwTextProp,
                   TrmTrmlComboBoxProp, TrmTrmlCBFieldValuesProp, TrmTrmlGRFieldValuesProp,
                   TrmTrmlCheckBoxProp, TrmTrmlDateRangeProp, TrmTrmASCIIFieldProp, TrmTrmASCIIRecordProp,
                   TrmTrmASCIIFormProp, TrmTrmXMLFormProp, TrmTrmXMLelementProp, TrmTrmXMLattributeProp,
                   TrmTrmXMLSimpleTypeByRestrictionProp, TrmTrmXMLQueryElementProp, TrmTrmXMLchoiceProp]);

end.
