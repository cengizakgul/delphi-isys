unit rmTrmlQueryDBGridPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmlCustomControlPropFrm, rmOPEReportParamFrm,
  rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, StdCtrls, ExtCtrls,
  ComCtrls, rwEngineTypes, rmOPEEnumFrm, rwUtils, rwQBInfo, rwDsgnUtils,
  Grids, ValEdit, rwBasicUtils, DB, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, rwDesignClasses, Wwdbigrd, Wwdbgrid, rmOPEQueryFrm,
  rmOPEColorFrm, rmOPEBooleanFrm, rmOPEDisplayFieldsFrm;

type
  TrmTrmlQueryDBGridProp = class(TrmTrmlCustomControlProp)
    frmKeyField: TrmOPEEnum;
    frmQuery: TrmOPEQuery;
    frmMultiSelection: TrmOPEBoolean;
    frmDisplayFields: TrmOPEDisplayFields;
    procedure frmKeyFieldcbListClick(Sender: TObject);
    procedure frmQuerybtnQueryClick(Sender: TObject);
  protected
    procedure AssignParmType; override;
    procedure GetPropsFromObject; override;
    procedure MakeFieldList;
  public
  end;

implementation

uses rmObjectPropsFrm, rwBasicClasses;

{$R *.dfm}

{ TrmTrwlQueryGridProp }

procedure TrmTrmlQueryDBGridProp.AssignParmType;
begin
  frmParam1.ParamType := rwvArray;
end;

procedure TrmTrmlQueryDBGridProp.GetPropsFromObject;
begin
  inherited;
  frmKeyField.PropertyName := 'KeyField';
  frmDisplayFields.PropertyName := 'VisibleFields';
  MakeFieldList;
  frmKeyField.ShowPropValue;
  frmMultiSelection.PropertyName := 'MultiSelection';
  frmMultiSelection.ShowPropValue;
end;

procedure TrmTrmlQueryDBGridProp.MakeFieldList;
var
  PropList: TrwStrList;
  Q: TObject;
  L: TStringList;
begin
  PropList := RWObject.PropertiesByTypeName('TrwQBQuery');
  if Length(PropList) > 0 then
    Q := Pointer(Integer(RWObject.GetPropertyValue(PropList[0])))
  else
    Q := nil;

  if Assigned(Q) then
  begin
    L := TStringList.Create;
    try
      rmGetFieldListFromQBQuery(TrwQBQuery(Q), L);
      frmKeyField.Values := L;
      frmDisplayFields.FieldList := L.Text;
    finally
      FreeAndNil(L);
    end;
  end

  else
  begin
    frmKeyField.Values := nil;
    frmDisplayFields.FieldList := '';
  end;
end;

procedure TrmTrmlQueryDBGridProp.frmKeyFieldcbListClick(Sender: TObject);
begin
  inherited;
  if (frmParam1.edParamName.Text = '') and (frmKeyField.NewPropertyValue <> '') then
    frmParam1.edParamName.Text := frmKeyField.NewPropertyValue;
end;

procedure TrmTrmlQueryDBGridProp.frmQuerybtnQueryClick(Sender: TObject);
begin
  inherited;
  frmQuery.btnQueryClick(Sender);
  GetPropsFromObject;
end;

end.

