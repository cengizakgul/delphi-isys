inherited rmTrmASCIIFormProp: TrmTrmASCIIFormProp
  Height = 332
  inherited pcCategories: TPageControl
    Height = 332
    inherited tsBasic: TTabSheet
      inline frmResultType: TrmOPEEnum
        Left = 8
        Top = 134
        Width = 236
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 3
        inherited lPropName: TLabel
          Width = 57
          Caption = 'Result Type'
        end
        inherited cbList: TComboBox
          Left = 90
          Width = 146
          OnChange = frmResultTypecbListChange
        end
      end
      inline frmDelimiter: TrmOPEEnum
        Left = 8
        Top = 166
        Width = 185
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 4
        inherited lPropName: TLabel
          Width = 40
          Caption = 'Delimiter'
        end
        inherited cbList: TComboBox
          Left = 90
          Width = 95
        end
      end
      inline frmQualifier: TrmOPEEnum
        Left = 8
        Top = 200
        Width = 185
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 5
        inherited lPropName: TLabel
          Width = 38
          Caption = 'Qualifier'
        end
        inherited cbList: TComboBox
          Left = 90
          Width = 95
        end
      end
      inline frmDefaultFileName: TrmOPEStringSingleLine
        Left = 8
        Top = 88
        Width = 290
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 2
        inherited lPropName: TLabel
          Width = 84
          Caption = 'Default File Name'
        end
      end
    end
    inherited tcAppearance: TTabSheet
      Caption = 'Processing'
      TabVisible = True
      inline frmBlockParentIfEmpty: TrmOPEBoolean
        Left = 8
        Top = 10
        Width = 264
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
        inherited chbProp: TCheckBox
          Width = 264
          Caption = 'Do not produce ASCII result if no data on this form'
        end
      end
      object pnlBlocking: TPanel
        Left = 8
        Top = 40
        Width = 290
        Height = 84
        BevelOuter = bvNone
        TabOrder = 1
        inline frmProduceRecordIf: TrmOPEBlockingControl
          Left = 0
          Top = 0
          Width = 290
          Height = 71
          AutoScroll = False
          TabOrder = 0
          inherited lCaption: TLabel
            Width = 290
            Caption = 'Produce Data If'
          end
          inherited frmExpression: TrmFMLExprPanel
            Width = 290
            Height = 55
            inherited pnlCanvas: TPanel
              Width = 290
              Height = 55
            end
          end
        end
      end
    end
  end
end
