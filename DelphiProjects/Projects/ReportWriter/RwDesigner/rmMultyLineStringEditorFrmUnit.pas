unit rmMultyLineStringEditorFrmUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TrmMultyLineStringEditorFrm = class(TForm)
    btnOK: TButton;
    btnCancel: TButton;
    memStrings: TMemo;
    pnPos: TPanel;
    procedure memStringsClick(Sender: TObject);
    procedure memStringsKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rmMultyLineStringEditorFrm: TrmMultyLineStringEditorFrm;

  // this function returns true if user pressed OK and false if Cancel
  function ShowRmMultyLineStringEditorFrm(ACaption : String; AOldValue : String; var ANewValue : String) : boolean;

implementation

{$R *.dfm}

procedure TrmMultyLineStringEditorFrm.memStringsClick(Sender: TObject);
begin
  pnPos.Caption := IntToStr(memStrings.CaretPos.Y+1)+' : '+
    IntToStr(memStrings.CaretPos.X+1);
end;

procedure TrmMultyLineStringEditorFrm.memStringsKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  memStrings.OnClick(nil);

  if Key = VK_ESCAPE then
    btnCancel.Click;
end;

// this function returns true if user pressed OK and false if Cancel
function ShowRmMultyLineStringEditorFrm(ACaption : String; AOldValue : String; var ANewValue : String) : boolean;
begin
  ANewValue := AOldValue;
  rmMultyLineStringEditorFrm := TrmMultyLineStringEditorFrm.Create(nil);
  try
    rmMultyLineStringEditorFrm.Caption := ACaption;
    rmMultyLineStringEditorFrm.memStrings.Text := AOldValue;
    rmMultyLineStringEditorFrm.ShowModal;
    result := rmMultyLineStringEditorFrm.ModalResult = mrOK;
    if result then ANewValue := rmMultyLineStringEditorFrm.memStrings.Text;
  finally
    rmMultyLineStringEditorFrm.Free;
    rmMultyLineStringEditorFrm := nil;
  end;
end;

initialization
  rmMultyLineStringEditorFrm := nil;
end.
