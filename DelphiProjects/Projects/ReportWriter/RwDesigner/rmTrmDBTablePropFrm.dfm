inherited rmTrmDBTableProp: TrmTrmDBTableProp
  Width = 364
  Height = 371
  inherited pcCategories: TPageControl
    Width = 364
    Height = 371
    inherited tsBasic: TTabSheet
      inline rmOPEQuery1: TrmOPEQuery
        Left = 8
        Top = 91
        Width = 91
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 2
        inherited btnQuery: TButton
          Width = 91
          OnClick = rmOPEQuery1btnQueryClick
        end
      end
      inline frmMasterFields: TrmOPEMasterFields
        Left = 8
        Top = 141
        Width = 340
        Height = 182
        AutoScroll = False
        TabOrder = 3
      end
    end
    object tsGrouping: TTabSheet [1]
      Caption = 'Grouping'
      ImageIndex = 3
      inline frmGrouping: TrmOPEDBTableGroups
        Left = 8
        Top = 7
        Width = 341
        Height = 328
        AutoScroll = False
        TabOrder = 0
      end
    end
    inherited tcAppearance: TTabSheet
      inherited frmColor: TrmOPEColor
        Top = 10
      end
      inline rmOPEFont1: TrmOPEFont
        Left = 226
        Top = 10
        Width = 75
        Height = 23
        AutoScroll = False
        AutoSize = True
        Enabled = False
        TabOrder = 1
        Visible = False
      end
    end
  end
end
