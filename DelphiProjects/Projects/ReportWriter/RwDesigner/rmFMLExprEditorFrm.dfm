object rmFMLExprEditor: TrmFMLExprEditor
  Left = 0
  Top = 0
  Width = 515
  Height = 332
  AutoScroll = False
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 460
    Height = 332
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 105
      Width = 460
      Height = 3
      Cursor = crVSplit
      Align = alTop
      AutoSnap = False
    end
    object pcItem: TPageControl
      Left = 0
      Top = 114
      Width = 460
      Height = 218
      ActivePage = tsObject
      Align = alClient
      Constraints.MinHeight = 100
      TabOrder = 0
      OnChanging = pcItemChanging
      OnResize = pcItemResize
      object tsObject: TTabSheet
        Caption = 'Object'
        OnShow = tsObjectShow
        object tvObjects: TTreeView
          Left = 0
          Top = 30
          Width = 452
          Height = 160
          Align = alClient
          HideSelection = False
          Images = DsgnRes.ilPict
          Indent = 19
          ReadOnly = True
          TabOrder = 0
          OnChange = tvObjectsChange
          OnDblClick = btnInsClick
        end
        object pnlObjProperty: TPanel
          Left = 0
          Top = 0
          Width = 452
          Height = 30
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label3: TLabel
            Left = 0
            Top = 7
            Width = 73
            Height = 13
            Caption = 'Object Property'
          end
          object cbObjectProperty: TComboBox
            Left = 89
            Top = 3
            Width = 152
            Height = 21
            Style = csDropDownList
            Enabled = False
            ItemHeight = 13
            ItemIndex = 0
            TabOrder = 0
            Text = 'Value'
            Items.Strings = (
              'Value')
          end
        end
      end
      object tsValue: TTabSheet
        Caption = 'Constant'
        ImageIndex = 1
        OnShow = tsValueShow
        DesignSize = (
          452
          190)
        object Label1: TLabel
          Left = 0
          Top = 6
          Width = 24
          Height = 13
          Caption = 'Type'
        end
        object cbConst: TComboBox
          Left = 29
          Top = 3
          Width = 127
          Height = 21
          Style = csDropDownList
          DropDownCount = 9
          ItemHeight = 0
          TabOrder = 0
          OnChange = cbConstChange
        end
        object pnlVal: TPanel
          Left = 170
          Top = 0
          Width = 281
          Height = 26
          Anchors = [akLeft, akTop, akRight]
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
          object Label2: TLabel
            Left = 0
            Top = 6
            Width = 27
            Height = 13
            Caption = 'Value'
          end
          object edValue: TEdit
            Left = 32
            Top = 3
            Width = 193
            Height = 21
            TabOrder = 0
            OnExit = edValueExit
            OnKeyPress = edValueKeyPress
          end
        end
      end
      object tsVariable: TTabSheet
        Caption = 'Variable'
        ImageIndex = 3
        OnShow = tsVariableShow
        object tvVariables: TTreeView
          Left = 0
          Top = 0
          Width = 452
          Height = 190
          Align = alClient
          HideSelection = False
          Images = DsgnRes.ilPict
          Indent = 19
          ReadOnly = True
          TabOrder = 0
          OnChange = tvVariablesChange
          OnDblClick = btnInsClick
        end
      end
      object tsFunction: TTabSheet
        Caption = 'Function'
        ImageIndex = 3
        OnShow = tsFunctionShow
        object tvFuncts: TTreeView
          Left = 0
          Top = 0
          Width = 452
          Height = 122
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HideSelection = False
          Images = DsgnRes.ilPict
          Indent = 19
          ParentFont = False
          ReadOnly = True
          ShowRoot = False
          TabOrder = 0
          OnChange = tvFunctsChange
          OnDblClick = tvFunctsDblClick
        end
        object Panel2: TPanel
          Left = 0
          Top = 128
          Width = 452
          Height = 62
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object lFuncDecl: TLabel
            Left = 0
            Top = 0
            Width = 452
            Height = 19
            Align = alTop
            AutoSize = False
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lFunctDescr: TLabel
            Left = 0
            Top = 19
            Width = 3
            Height = 13
            Align = alClient
            WordWrap = True
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 122
          Width = 452
          Height = 6
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
        end
      end
    end
    object grbExpression: TGroupBox
      Left = 0
      Top = 0
      Width = 460
      Height = 105
      Align = alTop
      Caption = 'Expression'
      Constraints.MinHeight = 50
      TabOrder = 1
      inline pnlExpr: TrmFMLExprPanel
        Left = 8
        Top = 15
        Width = 446
        Height = 81
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        inherited pnlCanvas: TPanel
          Width = 446
          Height = 81
        end
      end
    end
    object pnlSpace1: TPanel
      Left = 0
      Top = 108
      Width = 460
      Height = 6
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
    end
  end
  object Panel4: TPanel
    Left = 460
    Top = 0
    Width = 55
    Height = 332
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object btnPlus: TSpeedButton
      Left = 5
      Top = 67
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Caption = '+'
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold]
      Layout = blGlyphBottom
      Margin = 3
      ParentFont = False
      Transparent = False
      OnClick = btnPlusClick
    end
    object btnMinus: TSpeedButton
      Left = 29
      Top = 67
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Caption = '-'
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold]
      Layout = blGlyphBottom
      Margin = 3
      ParentFont = False
      Transparent = False
      OnClick = btnMinusClick
    end
    object btnMul: TSpeedButton
      Left = 5
      Top = 91
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Caption = #183
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold]
      Layout = blGlyphBottom
      Margin = 2
      ParentFont = False
      Transparent = False
      OnClick = btnMulClick
    end
    object btnDiv: TSpeedButton
      Left = 29
      Top = 91
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Caption = #184
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold]
      Layout = blGlyphBottom
      Margin = 3
      ParentFont = False
      Transparent = False
      OnClick = btnDivClick
    end
    object btnAND: TSpeedButton
      Left = 5
      Top = 198
      Width = 47
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Caption = 'AND'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clMaroon
      Font.Height = -9
      Font.Name = 'Small Fonts'
      Font.Style = [fsBold]
      Layout = blGlyphBottom
      Margin = 5
      ParentFont = False
      Transparent = False
      OnClick = btnANDClick
    end
    object btnOR: TSpeedButton
      Left = 5
      Top = 221
      Width = 47
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Caption = 'OR'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clMaroon
      Font.Height = -9
      Font.Name = 'Small Fonts'
      Font.Style = [fsBold]
      Layout = blGlyphBottom
      Margin = 5
      ParentFont = False
      Transparent = False
      OnClick = btnORClick
    end
    object btnGreat: TSpeedButton
      Left = 5
      Top = 121
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Caption = '>'
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold]
      Layout = blGlyphBottom
      Margin = 3
      ParentFont = False
      Transparent = False
      OnClick = btnGreatClick
    end
    object btnLess: TSpeedButton
      Left = 29
      Top = 121
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Caption = '<'
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold]
      Layout = blGlyphBottom
      Margin = 3
      ParentFont = False
      Transparent = False
      OnClick = btnLessClick
    end
    object btnEqual: TSpeedButton
      Left = 5
      Top = 144
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Caption = '='
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold]
      Layout = blGlyphBottom
      Margin = 3
      ParentFont = False
      Transparent = False
      OnClick = btnEqualClick
    end
    object btnNotEqual: TSpeedButton
      Left = 29
      Top = 144
      Width = 23
      Height = 22
      AllowAllUp = True
      BiDiMode = bdLeftToRight
      GroupIndex = 1
      Caption = #185
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold]
      Layout = blGlyphBottom
      Margin = 3
      ParentFont = False
      ParentBiDiMode = False
      Transparent = False
      OnClick = btnNotEqualClick
    end
    object btnGreatEqual: TSpeedButton
      Left = 5
      Top = 167
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Caption = '>'
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold, fsUnderline]
      Layout = blGlyphBottom
      Margin = 3
      ParentFont = False
      Transparent = False
      OnClick = btnGreatEqualClick
    end
    object btnLessEqual: TSpeedButton
      Left = 29
      Top = 167
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Caption = '<'
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold, fsUnderline]
      Layout = blGlyphBottom
      Margin = 3
      ParentFont = False
      Transparent = False
      OnClick = btnLessEqualClick
    end
    object btnBrackets: TSpeedButton
      Left = 6
      Top = 35
      Width = 47
      Height = 22
      Caption = '(.....)'
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clGreen
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold]
      Layout = blGlyphBottom
      Margin = 4
      ParentFont = False
      Transparent = False
      OnClick = btnBracketsClick
    end
    object btnIns: TSpeedButton
      Left = 6
      Top = 5
      Width = 23
      Height = 22
      Hint = 'Insert'
      AllowAllUp = True
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold]
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDD44444444
        44DDDDDD4FFFFFFFF4DDDDDD4F000000F4DDDDDD4FFFFFFFF4DDDDDD4F000F44
        44DDDDDD4FFFFF4F4DDDDDDD4FFFFF44DDDDDDDD4444444DDDDDDDDDDDD9DDDD
        DDDDDDDDDDD9DDDDDDDDDDDDD99999DDDDDDD0DDDD999DDDD000D0DD0DD9DD0D
        DD0DD0D000DDD000DDD000DD0DDDDD0DD0D0D0DDDDDDDDDDDD0D}
      Layout = blGlyphBottom
      Margin = 1
      ParentFont = False
      Transparent = False
      OnClick = btnInsClick
    end
    object btnDel: TSpeedButton
      Left = 30
      Top = 5
      Width = 23
      Height = 22
      Hint = 'Remove'
      AllowAllUp = True
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = [fsBold]
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00CCCCCCCCCCCC
        CCCCCCCCCCCCCCCCCCCCCC0CCCCCCCCCC0CCC000CCCCCCCCCCCCC0000CCCCCCC
        0CCCCC000CCCCCC0CCCCCCC000CCCC00CCCCCCCC000CC00CCCCCCCCCC00000CC
        CCCCCCCCCC000CCCCCCCCCCCC00000CCCCCCCCCC000CC00CCCCCCC0000CCCC00
        CCCCC0000CCCCCC00CCCC000CCCCCCCCC0CCCCCCCCCCCCCCCCCC}
      Layout = blGlyphBottom
      Margin = 1
      ParentFont = False
      Transparent = False
      OnClick = btnDelClick
    end
    object btnNot: TSpeedButton
      Left = 5
      Top = 244
      Width = 47
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Caption = 'NOT'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clMaroon
      Font.Height = -9
      Font.Name = 'Small Fonts'
      Font.Style = [fsBold]
      Layout = blGlyphBottom
      Margin = 5
      ParentFont = False
      Transparent = False
      OnClick = btnNotClick
    end
  end
end
