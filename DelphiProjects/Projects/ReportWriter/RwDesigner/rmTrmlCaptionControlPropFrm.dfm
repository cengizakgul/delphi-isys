inherited rmTrmlCaptionControlProp: TrmTrmlCaptionControlProp
  Height = 222
  inherited pcCategories: TPageControl
    Height = 222
    inherited tsBasic: TTabSheet
      object GroupBox2: TGroupBox
        Left = 8
        Top = 79
        Width = 290
        Height = 86
        Caption = 'Caption'
        TabOrder = 2
        inline frmCaption: TrmOPEStringSingleLine
          Left = 10
          Top = 20
          Width = 265
          Height = 21
          AutoScroll = False
          AutoSize = True
          TabOrder = 0
          inherited lPropName: TLabel
            Width = 21
            Caption = 'Text'
          end
          inherited edString: TEdit
            Left = 68
            Width = 197
          end
        end
        inline frmCaptionLayout: TrmOPEEnum
          Left = 10
          Top = 51
          Width = 155
          Height = 21
          AutoScroll = False
          AutoSize = True
          TabOrder = 1
          inherited lPropName: TLabel
            Width = 37
            Caption = 'Position'
          end
          inherited cbList: TComboBox
            Left = 68
            Width = 87
          end
        end
      end
    end
    inherited tcAppearance: TTabSheet
      inherited frmColor: TrmOPEColor
        Left = 100
        Top = 11
      end
      inline frmCaptionFont: TrmOPEFont
        Left = 10
        Top = 10
        Width = 75
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
      end
    end
    inherited tsParam: TTabSheet
      inherited frmParam1: TrmOPEReportParam
        inherited Label1: TLabel
          Width = 77
          Caption = 'Brief Description'
        end
      end
    end
  end
end
