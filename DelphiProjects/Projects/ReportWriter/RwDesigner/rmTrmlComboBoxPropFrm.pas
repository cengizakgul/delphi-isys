unit rmTrmlComboBoxPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmlCaptionControlPropFrm, rmOPEStringValuesFrm, StdCtrls,
  ExtCtrls, rmOPEReportParamFrm, rmOPEFontFrm, rmOPEColorFrm, rmOPEEnumFrm,
  rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, ComCtrls;

type
  TrmTrmlComboBoxProp = class(TrmTrmlCaptionControlProp)
    grbItems: TGroupBox;
    frmValues: TrmOPEStringValues;
  private
  protected
    procedure GetPropsFromObject; override;
  public
  end;

implementation

{$R *.dfm}

{ TrmTrmlCaptionControlProp1 }

procedure TrmTrmlComboBoxProp.GetPropsFromObject;
begin
  inherited;
  frmValues.PropertyName := 'Items';
  frmValues.ShowPropValue;
end;

end.
