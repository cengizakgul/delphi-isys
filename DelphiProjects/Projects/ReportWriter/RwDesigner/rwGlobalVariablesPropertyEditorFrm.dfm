inherited rwGlobalVariablesPropertyEditor: TrwGlobalVariablesPropertyEditor
  Left = 450
  Top = 558
  Width = 451
  Height = 370
  Caption = 'List of Variables'
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOK: TButton
    Left = 362
    ModalResult = 2
  end
  inherited btnCancel: TButton
    Left = 362
  end
  object lvVar: TListView
    Left = 0
    Top = 0
    Width = 353
    Height = 342
    Anchors = [akLeft, akTop, akRight, akBottom]
    Color = clWhite
    Columns = <
      item
        AutoSize = True
      end>
    HideSelection = False
    OwnerDraw = True
    ReadOnly = True
    ShowColumnHeaders = False
    SmallImages = DsgnRes.ilPict
    SortType = stText
    TabOrder = 2
    ViewStyle = vsReport
    OnDrawItem = lvVarDrawItem
  end
end
