inherited rmTrmTableProp: TrmTrmTableProp
  inherited pcCategories: TPageControl
    inherited tcAppearance: TTabSheet
      inline rmOPEFont1: TrmOPEFont [0]
        Left = 10
        Top = 10
        Width = 75
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
      end
      inherited frmColor: TrmOPEColor
        Left = 99
        Top = 10
        TabOrder = 1
      end
    end
    inherited tcPrint: TTabSheet
      inherited frmMultiPaged: TrmOPEBoolean [3]
        Top = 113
        Visible = False
        inherited chbProp: TCheckBox
          Enabled = False
        end
      end
      inherited pnlBlocking: TPanel [4]
        Top = 96
      end
    end
  end
end
