inherited rwDateTimePropertyEditor: TrwDateTimePropertyEditor
  Left = 545
  Top = 216
  VertScrollBar.Range = 0
  BorderStyle = bsToolWindow
  Caption = 'Date editor'
  ClientHeight = 187
  ClientWidth = 190
  OldCreateOrder = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOK: TButton
    Left = 14
    Top = 160
    TabOrder = 1
  end
  inherited btnCancel: TButton
    Left = 103
    Top = 160
    TabOrder = 2
  end
  object Calendar: TMonthCalendar
    Left = 0
    Top = 0
    Width = 190
    Height = 156
    Align = alTop
    Date = 37824.561850937500000000
    TabOrder = 0
    OnDblClick = CalendarDblClick
  end
end
