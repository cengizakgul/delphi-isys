unit rmOPEFieldFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls, Mask, wwdbedit, Wwdotdot,
  Wwdbcomb, ISBasicClasses, rwDesignClasses, rmTypes, rwBasicUtils;

type
  TrmOPEField = class(TrmObjPropertyEditor)
    Label1: TLabel;
    Label2: TLabel;
    cbDataSource: TisRWDBComboBox;
    cbField: TisRWDBComboBox;
    procedure cbDataSourceChange(Sender: TObject);
    procedure cbFieldChange(Sender: TObject);
  private
    procedure BuildDataSourceList;
    procedure ShowField;
  public
    procedure RefreshFields;
    procedure ShowPropValue; override;
    function  NewPropertyValue: Variant; override;
  end;

implementation

{$R *.dfm}

{ TrmOPEField }

procedure TrmOPEField.BuildDataSourceList;
var
  RootOwn, obj: IrmDesignObject;
  h: String;
  i: Integer;
begin
  cbDataSource.Tag := 1;
  try
    cbDataSource.Items.Clear;

    Obj := TComponent(LinkedObject) as IrmDesignObject;
    RootOwn := Obj.GetRootOwner;

    i := 0;
    while Obj <> RootOwn do
    begin
      if Supports(Obj.RealObject, IrmQueryDrivenObject) then
      begin
        if (Obj as IrmQueryDrivenObject).GetQuery.GetQBQuery.GetFieldList <> ''  then
        begin
          h := Obj.GetPropertyValue('Description');
          if h = '' then
            h := Obj.GetPropertyValue('Name');
          if i > 0 then
          begin
            h := h + ' (' + IntToStr(i) + ' Level';
            if i > 1 then
              h := h + 's';
            h := h + ' Up)';
          end;
          cbDataSource.Items.AddObject(h + #9 + Obj.GetPathFromRootOwner, Obj.RealObject);
        end;
        Inc(i);
      end;
      Obj := Obj.GetObjectOwner;
    end;
  finally
    cbDataSource.Tag := 0;
  end;
end;

procedure TrmOPEField.ShowPropValue;
begin
  inherited;
  ShowField;
end;

procedure TrmOPEField.cbDataSourceChange(Sender: TObject);
var
  i: Integer;
  Obj: IrmQueryDrivenObject;
begin
  if cbDataSource.Tag = 0 then
  begin
    i := cbDataSource.ItemIndex;
    cbField.Items.Clear;
    if i <> -1 then
    begin
      Obj := TComponent(cbDataSource.Items.Objects[i]) as IrmQueryDrivenObject;
      cbField.Items.CommaText := Obj.GetQuery.GetQBQuery.GetFieldList;
      cbField.Value := '';
    end;
    NotifyOfChanges;
  end;
end;

procedure TrmOPEField.ShowField;
var
  FldInfo: TrmDataFieldInfoRec;
begin
  BuildDataSourceList;

  if cbDataSource.Items.Count = 0 then
    Exit;

  FldInfo := (TComponent(LinkedObject) as IrmQueryAwareObject).GetDataFieldInfo;
  if Assigned(FldInfo.DataSource) then
  begin
    cbDataSource.Value := (TComponent(FldInfo.DataSource.VCLObject) as IrmDesignObject).GetPathFromRootOwner;
    cbField.Value := FldInfo.Name;
  end
  else
  begin
    if cbDataSource.Items.Count > 0 then
      cbDataSource.ItemIndex := 0
    else
      cbDataSource.Value := '';    

    cbField.Value := '';
  end;
end;

function TrmOPEField.NewPropertyValue: Variant;
begin
  if (cbDataSource.Value <> '') and (cbField.Value <> '') then
    Result := cbDataSource.Value + '.' + cbField.Value
  else
    Result := '';
end;

procedure TrmOPEField.RefreshFields;
var
  h: String;
begin
  h := cbField.Value;
  cbDataSource.OnChange(nil);
  cbField.Value := h;
end;

procedure TrmOPEField.cbFieldChange(Sender: TObject);
begin
  inherited;
  NotifyOfChanges;
end;

end.
