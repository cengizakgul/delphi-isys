unit rwDesignFormFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwDesignAreaFrm, ExtCtrls, StdCtrls, rwBasicClasses, rwReport, Menus,
  rwLibCompChoiceFrm, rwExtendedControls;

type

  {TrwDesignForm is input parameters form for designing}

  TrwDesignForm = class(TrwDesignArea)
  private
    FOnMouseDown: TMouseEvent;
  protected
    procedure Loaded; override;
    function SelectLibComp: TrwLibComponent; override;

  public
    procedure SetContainer(AContainer: TrwInputFormContainer);
    procedure DestroySelectedComponents; override;
  end;

implementation

{$R *.DFM}

procedure TrwDesignForm.Loaded;
begin
  inherited;

  FOnMouseDown := pnlArea.OnMouseDown;
//pnlArea.Free;
  pnlArea.Parent := nil;
  pnlArea := nil;
end;

procedure TrwDesignForm.SetContainer(AContainer: TrwInputFormContainer);
begin
  TrwArea(AContainer.VisualControl.RealObject).OnMouseDown := FOnMouseDown;
  pnlArea := TrwArea(AContainer.VisualControl.RealObject);
  Parent.ClientWidth := pnlArea.Width;
  Parent.ClientHeight := pnlArea.Height;
  pnlArea.Align := alClient;
  AContainer.VisualControl.RealObject.Parent := Self;
  ComponentsOwner := AContainer;
end;

procedure TrwDesignForm.DestroySelectedComponents;
var
  lComp: TrwComponent;
begin
  inherited;

  while SelectedCompList.Count > 0 do
  begin
    lComp := SelectedCompList[0];
    BeforeDestroyingComponent(lComp);
    lComp.Free;
  end;

  pnlAreaMouseDown(pnlArea, mbLeft, [], 0, 0);
end;


function TrwDesignForm.SelectLibComp: TrwLibComponent;
begin
  Result := ChoiceLibComp([rctInputFormComp, rctNoVisualComp]);
end;

end.
