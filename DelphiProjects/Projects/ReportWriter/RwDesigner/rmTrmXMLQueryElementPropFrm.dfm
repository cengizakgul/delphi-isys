inherited rmTrmXMLQueryElementProp: TrmTrmXMLQueryElementProp
  Width = 365
  Height = 343
  inherited pcCategories: TPageControl
    Width = 365
    Height = 343
    inherited tsBasic: TTabSheet
      inline frmQuery: TrmOPEQuery
        Left = 8
        Top = 95
        Width = 90
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 2
        inherited btnQuery: TButton
          OnClick = frmQuerybtnQueryClick
        end
      end
      inline frmMasterFields: TrmOPEMasterFields
        Left = 8
        Top = 131
        Width = 340
        Height = 178
        AutoScroll = False
        TabOrder = 3
      end
    end
    inherited tcAppearance: TTabSheet
      object Panel1: TPanel [1]
        Left = 8
        Top = 66
        Width = 343
        Height = 161
        BevelOuter = bvNone
        TabOrder = 1
        inline frmProduceBranchIf: TrmOPEBlockingControl
          Left = 0
          Top = 0
          Width = 340
          Height = 71
          AutoScroll = False
          TabOrder = 0
          inherited lCaption: TLabel
            Width = 340
            Caption = 'Produce Whole Branch If'
          end
          inherited frmExpression: TrmFMLExprPanel
            Width = 340
            Height = 55
            inherited pnlCanvas: TPanel
              Width = 340
              Height = 55
            end
          end
        end
        inline frmProduceIf: TrmOPEBlockingControl
          Left = 0
          Top = 84
          Width = 340
          Height = 71
          AutoScroll = False
          TabOrder = 1
          inherited lCaption: TLabel
            Width = 340
            Caption = 'Produce Items If'
          end
          inherited frmExpression: TrmFMLExprPanel
            Width = 340
            Height = 55
            inherited pnlCanvas: TPanel
              Width = 340
              Height = 55
            end
          end
        end
      end
      inherited frmInactive: TrmOPEBoolean
        TabOrder = 2
      end
    end
  end
end
