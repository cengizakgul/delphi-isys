unit rmTrmlDateRangePropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmlCustomControlPropFrm, StdCtrls, ExtCtrls,
  rmOPEReportParamFrm, rmOPEColorFrm, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls, rwEngineTypes;

type
  TrmTrmlDateRangeProp = class(TrmTrmlCustomControlProp)
    lHeader1: TLabel;
    lHeader2: TLabel;
    frmParam2: TrmOPEReportParam;
    frmCaption: TrmOPEStringSingleLine;
  protected
    procedure AssignParmType; override;
    procedure GetPropsFromObject; override;
  end;


implementation

{$R *.dfm}

{ TTrmTrmlDateRangeProp }

procedure TrmTrmlDateRangeProp.AssignParmType;
begin
  frmParam1.ParamType := rwvDate;
  frmParam1.ParamNumber := 0;
  frmParam2.ParamType := rwvDate;
  frmParam2.ParamNumber := 1;
end;

procedure TrmTrmlDateRangeProp.GetPropsFromObject;
begin
  inherited;
  frmCaption.PropertyName := 'Caption';
  frmCaption.ShowPropValue;
  frmParam2.ShowPropValue;
end;

end.
 