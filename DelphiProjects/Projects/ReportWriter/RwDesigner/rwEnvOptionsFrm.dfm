object rwEnvOptions: TrwEnvOptions
  Left = 461
  Top = 212
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Environment Options'
  ClientHeight = 290
  ClientWidth = 388
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object evPageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 388
    Height = 290
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'Designer'
      object GroupBox1: TGroupBox
        Left = 5
        Top = 5
        Width = 369
        Height = 113
        Caption = 'Code Insight'
        TabOrder = 0
        object Label2: TLabel
          Left = 199
          Top = 32
          Width = 30
          Height = 13
          Caption = 'Delay:'
        end
        object Label3: TLabel
          Left = 199
          Top = 72
          Width = 35
          Height = 13
          Caption = '0.5 sec'
        end
        object Label4: TLabel
          Left = 323
          Top = 72
          Width = 35
          Height = 13
          Caption = '1.5 sec'
        end
        object chbCodeCompl: TCheckBox
          Left = 10
          Top = 19
          Width = 107
          Height = 17
          Caption = 'Code completion'
          TabOrder = 0
        end
        object chbCodeParam: TCheckBox
          Left = 10
          Top = 42
          Width = 107
          Height = 17
          Caption = 'Code parameters'
          TabOrder = 1
        end
        object trkDelay: TTrackBar
          Left = 195
          Top = 47
          Width = 147
          Height = 25
          Max = 1500
          Min = 500
          ParentShowHint = False
          Frequency = 200
          Position = 500
          ShowHint = False
          TabOrder = 4
        end
        object chbExprEv: TCheckBox
          Left = 10
          Top = 65
          Width = 168
          Height = 17
          Caption = 'Tooltip expression evaluation'
          Enabled = False
          TabOrder = 2
        end
        object chbSymbInsight: TCheckBox
          Left = 10
          Top = 88
          Width = 161
          Height = 17
          Caption = 'Tooltip symbol insight'
          Enabled = False
          TabOrder = 3
        end
      end
      object GroupBox2: TGroupBox
        Left = 5
        Top = 123
        Width = 153
        Height = 91
        Caption = 'Compiler'
        TabOrder = 1
        object chbTraceLib: TCheckBox
          Left = 10
          Top = 19
          Width = 97
          Height = 17
          Caption = 'Trace Library'
          TabOrder = 0
        end
        object chbAutoCompile: TCheckBox
          Left = 10
          Top = 65
          Width = 134
          Height = 17
          Caption = 'Autocompile before exit'
          TabOrder = 2
        end
        object chbDontAskConfirm: TCheckBox
          Left = 10
          Top = 42
          Width = 134
          Height = 17
          Caption = 'Don'#39't ask confirmation'
          TabOrder = 1
        end
      end
      object GroupBox3: TGroupBox
        Left = 167
        Top = 123
        Width = 207
        Height = 91
        Caption = 'Misc'
        TabOrder = 2
        object Label1: TLabel
          Left = 10
          Top = 20
          Width = 24
          Height = 13
          Caption = 'Units'
        end
        object chbShowData: TCheckBox
          Left = 10
          Top = 53
          Width = 145
          Height = 17
          Caption = 'Show data in design-time'
          Enabled = False
          TabOrder = 1
        end
        object cbUnits: TComboBox
          Left = 47
          Top = 17
          Width = 147
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          Items.Strings = (
            'Inches'
            'Millimeters'
            'Thousandths MM'
            'Screen Pixels')
        end
      end
    end
  end
  object btnOK: TButton
    Tag = 267
    Left = 215
    Top = 252
    Width = 75
    Height = 23
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = btnOKClick
  end
  object btnCancel: TButton
    Tag = 261
    Left = 303
    Top = 252
    Width = 75
    Height = 23
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
end
