inherited rmTBMDIChildFrame: TrmTBMDIChildFrame
  Height = 25
  inherited ControlBar: TControlBar
    Height = 25
    object tlbMenu: TToolBar
      Left = 11
      Top = 2
      Width = 55
      Height = 21
      Align = alNone
      AutoSize = True
      ButtonHeight = 21
      ButtonWidth = 51
      Caption = 'Menu'
      DragKind = dkDock
      EdgeBorders = []
      Flat = True
      Menu = MainMenu
      ShowCaptions = True
      TabOrder = 0
      Wrapable = False
    end
  end
  inherited MainMenu: TMainMenu
    object miWindows: TMenuItem
      Caption = 'Windows'
      OnClick = miWindowsClick
      object miWindowsSeparator1: TMenuItem
        Caption = '-'
      end
      object miCloseWindow: TMenuItem
        Action = actCloseWindow
      end
    end
  end
  inherited ActionList: TActionList
    object actCloseWindow: TAction
      Caption = 'Close Active Window'
      Hint = 'Close Active Window'
      ShortCut = 16499
      OnExecute = actCloseWindowExecute
    end
  end
end
