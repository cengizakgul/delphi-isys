unit rmTrmPrintTextPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, StdCtrls, ExtCtrls, ComCtrls,
  rmObjPropertyEditorFrm, rmOPEBooleanFrm, rmOPEStringSingleLineFrm,
  rmOPEValueFrm, rmTrmPrintControlPropFrm, rmOPEColorFrm, rmOPEFontFrm,
  rmOPETextFilterFrm, rmOPEExpressionFrm, rmOPEBlockingControlFrm;

type
  TrmTrmPrintTextProp = class(TrmTrmPrintControlProp)
    frmFont: TrmOPEFont;
    frmColor: TrmOPEColor;
    grbValue: TGroupBox;
    frmValue: TrmOPEValue;
    frmAutosize: TrmOPEBoolean;
    frmWordWrap: TrmOPEBoolean;
    tcTextFilters: TTabSheet;
    frmTextFilter: TrmOPETextFilter;
  private
  protected
    procedure GetPropsFromObject; override;
  public
  end;


implementation

uses rmCommonClasses;

{$R *.dfm}

{ TrmTrwLabelProp }

procedure TrmTrmPrintTextProp.GetPropsFromObject;
begin
  inherited;
  frmValue.PropertyName := 'Value';
  frmValue.ShowPropValue;
  frmAutosize.PropertyName := 'Autosize';
  frmAutosize.ShowPropValue;
  frmWordWrap.PropertyName := 'WordWrap';
  frmWordWrap.ShowPropValue;
end;

end.
