object rwPropertyEditor: TrwPropertyEditor
  Left = 495
  Top = 244
  Width = 392
  Height = 235
  Anchors = [akRight, akBottom]
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'Property Editor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    384
    201)
  PixelsPerInch = 96
  TextHeight = 13
  object btnOK: TButton
    Tag = 267
    Left = 303
    Top = 8
    Width = 75
    Height = 23
    Anchors = [akTop, akRight]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancel: TButton
    Tag = 261
    Left = 303
    Top = 38
    Width = 75
    Height = 23
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
end
