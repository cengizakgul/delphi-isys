unit rwPrintOrderFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwCommonClasses, rwBasicClasses, rwUtils;

type
  TrwPrintOrder = class(TForm)
    lbControls: TListBox;
    btnUp: TButton;
    btnDown: TButton;
    btnTop: TButton;
    btnBottom: TButton;
    btnOK: TButton;
    btnCancel: TButton;
    procedure btnTopClick(Sender: TObject);
    procedure btnBottomClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);

  private
    procedure MoveCurrentItem(NewPos: Integer);
  public
  end;

procedure ShowPrintOrder(AContainer: TrwPrintableContainer);

implementation

{$R *.DFM}


procedure ShowPrintOrder(AContainer: TrwPrintableContainer);
var
  Frm: TrwPrintOrder;
  i: Integer;
begin
  Frm := TrwPrintOrder.Create(Application);
  with Frm do
    try
      Caption := 'Print order for '+AContainer.Name;
      for i := 0 to AContainer.ControlCount-1 do
        lbControls.Items.AddObject(AContainer.Controls[i].Name+' : '+AContainer.Controls[i].ClassName,
          AContainer.Controls[i]);

      if Frm.ShowModal = mrOK then
      begin
        CheckInheritedAndShowError(AContainer);

        for i := 0 to lbControls.Items.Count-1 do
        begin
          TrwChildComponent(lbControls.Items.Objects[i]).Container := nil;
          TrwChildComponent(lbControls.Items.Objects[i]).Container := AContainer;
        end;

        TrwComponent(AContainer.Owner).FixUpCompIndexes;
      end;
    finally
      Frm.Free;
    end;
end;

procedure TrwPrintOrder.btnTopClick(Sender: TObject);
begin
  MoveCurrentItem(0);
end;

procedure TrwPrintOrder.btnBottomClick(Sender: TObject);
begin
  MoveCurrentItem(lbControls.Items.Count-1);
end;

procedure TrwPrintOrder.btnUpClick(Sender: TObject);
begin
  MoveCurrentItem(lbControls.ItemIndex-1);
end;

procedure TrwPrintOrder.btnDownClick(Sender: TObject);
begin
  MoveCurrentItem(lbControls.ItemIndex+1);
end;

procedure TrwPrintOrder.MoveCurrentItem(NewPos: Integer);
begin
  if (lbControls.ItemIndex = -1) or (NewPos < 0) or (NewPos > lbControls.Items.Count-1) then
    Exit;
  lbControls.Items.Move(lbControls.ItemIndex, NewPos);
  lbControls.ItemIndex := NewPos;
end;



end.
