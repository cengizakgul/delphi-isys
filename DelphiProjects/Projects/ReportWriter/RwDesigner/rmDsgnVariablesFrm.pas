unit rmDsgnVariablesFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDsgnAreaFrm, ExtCtrls, ImgList, ComCtrls, rmTypes, rmDsgnTypes,
  rwEngineTypes, StdCtrls, TypInfo, rwMessages, rwUtils, Menus, rwDsgnUtils;

type
  TrmdTreeNodeVars = class;

  TrmDsgnVariables = class(TrmDsgnArea)
    tvObjects: TTreeView;
    ImageList: TImageList;
    Splitter1: TSplitter;
    pnlVariable: TPanel;
    lVarCaption: TLabel;
    Label2: TLabel;
    edVarName: TEdit;
    Label3: TLabel;
    edVarDescription: TEdit;
    rgVarGroup: TRadioGroup;
    Label4: TLabel;
    memVarValue: TMemo;
    Label5: TLabel;
    cbVarType: TComboBox;
    chbShowInQB: TCheckBox;
    chbShowInFmlEditor: TCheckBox;
    pnlDescription: TPanel;
    lDescrCaption: TLabel;
    lDescrText: TLabel;
    pnlButtons: TPanel;
    btnNewVar: TButton;
    btnDelVar: TButton;
    btnAddInGroup: TButton;
    procedure tvObjectsChange(Sender: TObject; Node: TTreeNode);
    procedure btnNewVarClick(Sender: TObject);
    procedure btnDelVarClick(Sender: TObject);
    procedure edVarNameExit(Sender: TObject);
    procedure edVarNameKeyPress(Sender: TObject; var Key: Char);
    procedure edVarDescriptionExit(Sender: TObject);
    procedure cbVarTypeChange(Sender: TObject);
    procedure rgVarGroupClick(Sender: TObject);
    procedure chbShowInQBClick(Sender: TObject);
    procedure chbShowInFmlEditorClick(Sender: TObject);
    procedure memVarValueExit(Sender: TObject);
    procedure tvObjectsChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
  private
    FReadingProperties: Boolean;
    function GetTopNode: TrmdTreeNodeVars;

    // notification
    procedure DMRefreshVariables(var Message: TrmMessageRec); message mRefreshVariables;

    procedure SetVarDescription;
    procedure SetVarsDescription;
    procedure SetGroupDescription;
    procedure NotifyOwnerAboutChanges;

  protected
    procedure Initialize; override;
    procedure GetListeningMessages(const AMessages: TList); override;
  public
    constructor Create(AOwner: TComponent); override;
    function CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject; override;
  end;


  TrmdTreeNodeVar = class(TTreeNode)
  private
    FHandle: IrmVariable;
    procedure Update;
  end;


  TrmdTreeNodeGroup = class(TTreeNode)
  private
    FVisibility: TrwVarVisibility;
    procedure Update;
  end;


  TrmdTreeNodeVars = class(TTreeNode)
  private
    FHandle:  IrmVariables;
    function  AddVarNode(const AVar: IrmVariable): TrmdTreeNodeVar;
    procedure Update;
    function  GroupPrivate: TrmdTreeNodeGroup;
    function  GroupProtected: TrmdTreeNodeGroup;
    function  GroupPublic: TrmdTreeNodeGroup;
    function  GroupPublished: TrmdTreeNodeGroup;
  end;


implementation

{$R *.dfm}


{ TrmdTreeNodeVars }

function TrmdTreeNodeVars.AddVarNode(const AVar: IrmVariable): TrmdTreeNodeVar;
var
  Npar: TTreeNode;
begin
  case AVar.GetVisibility of
    rvvPrivate:    Npar := GroupPrivate;
    rvvProtected:  Npar := GroupProtected;
    rvvPublic:     Npar := GroupPublic;
    rvvPublished:  Npar := GroupPublished;
  else
    Npar := nil;
  end;

  Result := TrmdTreeNodeVar.Create(Owner);
  Owner.AddNode(Result, Npar, '', nil, naAddChild);
  Result.FHandle := AVar;
  Result.Update;
end;

function TrmdTreeNodeVars.GroupPrivate: TrmdTreeNodeGroup;
begin
  Result := TrmdTreeNodeGroup(GetFirstChild);
end;

function TrmdTreeNodeVars.GroupProtected: TrmdTreeNodeGroup;
begin
  Result := TrmdTreeNodeGroup(GetFirstChild);
  Result := TrmdTreeNodeGroup(GetNextChild(Result));
end;

function TrmdTreeNodeVars.GroupPublic: TrmdTreeNodeGroup;
begin
  Result := TrmdTreeNodeGroup(GetFirstChild);
  Result := TrmdTreeNodeGroup(GetNextChild(Result));
  Result := TrmdTreeNodeGroup(GetNextChild(Result));
end;

function TrmdTreeNodeVars.GroupPublished: TrmdTreeNodeGroup;
begin
  Result := TrmdTreeNodeGroup(GetLastChild);
end;

procedure TrmdTreeNodeVars.Update;
var
  i: Integer;

  procedure AddGroup(const AVisibility: TrwVarVisibility);
  var
    N: TrmdTreeNodeGroup;
  begin
    N := TrmdTreeNodeGroup.Create(Owner);
    Owner.AddNode(N, Self, '', nil, naAddChild);
    N.FVisibility := AVisibility;
    N.Update;
  end;

begin
  Text := 'Variables';
  ImageIndex := 0;
  StateIndex := ImageIndex;
  SelectedIndex := ImageIndex;

  DeleteChildren;

  AddGroup(rvvPrivate);
  AddGroup(rvvProtected);
  AddGroup(rvvPublic);
  AddGroup(rvvPublished);

  for i := 0 to FHandle.GetItemCount - 1 do
    AddVarNode(FHandle.GetItemByIndex(i) as IrmVariable);
end;

{ TrmdTreeNodeVar }

procedure TrmdTreeNodeVar.Update;
begin
  with FHandle do
  begin
    Text := GetName;
    if GetDisplayName <> '' then
      Text := Text + ' (' + GetDisplayName + ')';
    if IsInheritedItem then
      ImageIndex := 2
    else
      ImageIndex := 1;
    StateIndex := ImageIndex;
    SelectedIndex := ImageIndex;
  end;
end;

{ TrmDsgnVariables }

constructor TrmDsgnVariables.Create(AOwner: TComponent);
begin
  inherited;
  FImageIndex := 0;
  FAreaType := rmDATInternalObject;
end;

function TrmDsgnVariables.CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject;
begin
// do nothing
end;

function TrmDsgnVariables.GetTopNode: TrmdTreeNodeVars;
begin
  Result := TrmdTreeNodeVars(tvObjects.Items[0]);
end;

procedure TrmDsgnVariables.Initialize;
var
  Npar: TrmdTreeNodeVars;
  i: Integer;
  PTypInf: PTypeInfo;
  PTypDat: PTypeData;
  h: String;
begin
  inherited;

  // VarType dropdown
  PTypInf := TypeInfo(TrwVarTypeKind);
  PTypDat := GetTypeData(PTypInf);

  cbVarType.Clear;
  for i := PTypDat^.MinValue to PTypDat^.MaxValue do
  begin
    h := GetEnumName(PTypInf, i);
    Delete(h, 1, 3);
    cbVarType.Items.Add(h);
  end;


  // var tree
  tvObjects.Items.BeginUpdate;
  try
    tvObjects.Items.Clear;

    Npar := TrmdTreeNodeVars.Create(tvObjects.Items);
    tvObjects.Items.AddNode(Npar, nil, '', nil, naAdd);
    Npar.FHandle := (DesignObject.RealObject as IrmVarFunctHolder).GetVariables;
    Npar.Update;

    GetTopNode.Expand(False);
  finally
     tvObjects.Items.EndUpdate;
  end;

  GetTopNode.Selected := True;  
end;

procedure TrmDsgnVariables.SetGroupDescription;
begin
  lDescrCaption.Caption := 'Group "' + tvObjects.Selected.Text + '"';
  lDescrText.Caption := '';
  
  pnlDescription.Show;
  pnlVariable.Hide;
end;

procedure TrmDsgnVariables.SetVarDescription;
var
  V: IrmVariable;
  i: Integer;
  ArrVal: Variant;
  NotInh: Boolean;
begin
  FReadingProperties := True;
  try
    V := TrmdTreeNodeVar(tvObjects.Selected).FHandle;

    edVarName.Text := V.GetName;
    edVarDescription.Text := V.GetDisplayName;

    cbVarType.ItemIndex := Ord(V.GetVarType);
    if (V.GetVarType = rwvPointer) and (V.GetPointerType <> '') then
      cbVarType.Text := V.GetPointerType
    else
      cbVarType.Text := cbVarType.Items[cbVarType.ItemIndex];

    case V.GetVisibility of
      rvvPublished:  i := 3;
      rvvPublic:     i := 2;
      rvvProtected:  i := 1;
      rvvPrivate:    i := 0;
    else
      i := -1;
    end;

    rgVarGroup.ItemIndex := i;
    chbShowInQB.Checked :=  V.GetShowInQB;
    chbShowInFmlEditor.Checked :=   V.GetShowInFmlEditor;

    if V.GetVarType <> rwvArray then
      memVarValue.Text := VarToStr(V.GetValue)
    else
    begin
      memVarValue.Clear;
      ArrVal := V.GetValue;
      for i := VarArrayLowBound(ArrVal, 1) to VarArrayHighBound(ArrVal, 1) do
        memVarValue.Lines.Add(VarToStr(ArrVal[i]));
    end;

    NotInh := not V.IsInheritedItem;

    edVarName.Enabled := NotInh;
    edVarDescription.Enabled := NotInh;
    cbVarType.Enabled := NotInh;
    rgVarGroup.Enabled := NotInh;
    chbShowInQB.Enabled := NotInh;
    chbShowInFmlEditor.Enabled := NotInh;
    btnDelVar.Enabled := NotInh;

    pnlVariable.Show;
    pnlDescription.Hide;
  finally
    FReadingProperties := False;
  end;
end;

procedure TrmDsgnVariables.SetVarsDescription;
begin
  lDescrCaption.Caption := 'Variables';
  lDescrText.Caption := '';

  pnlDescription.Show;
  pnlVariable.Hide;
end;

procedure TrmDsgnVariables.tvObjectsChange(Sender: TObject; Node: TTreeNode);
begin
  if Assigned(Node) then
  begin
    if Node is TrmdTreeNodeVar then
      SetVarDescription
    else if Node is TrmdTreeNodeVars then
      SetVarsDescription
    else
      SetGroupDescription;
  end;
end;

procedure TrmDsgnVariables.btnNewVarClick(Sender: TObject);
var
  V: IrmVariable;
begin
  V := GetTopNode.FHandle.AddItem as IrmVariable;

  V.SetName('New Variable');
  V.SetVarType(rwvString);

  if Sender = btnAddInGroup then
  begin
    if tvObjects.Selected is TrmdTreeNodeGroup then
      V.SetVisibility(TrmdTreeNodeGroup(tvObjects.Selected).FVisibility)
    else
      V.SetVisibility(rvvPrivate);
  end
  else
    V.SetVisibility(TrmdTreeNodeGroup(tvObjects.Selected.Parent).FVisibility);

  tvObjects.Selected := GetTopNode.AddVarNode(V);
  NotifyOwnerAboutChanges;  
end;

procedure TrmDsgnVariables.btnDelVarClick(Sender: TObject);
var
  i: Integer;
begin
   if tvObjects.Selected is TrmdTreeNodeVar then
   begin
     i := TrmdTreeNodeVar(tvObjects.Selected).FHandle.GetItemIndex;
     tvObjects.Selected.Free;
     GetTopNode.FHandle.DeleteItem(i);
     NotifyOwnerAboutChanges;     
   end;
end;

procedure TrmDsgnVariables.edVarNameExit(Sender: TObject);
var
  V: IrmVariable;
begin
  if tvObjects.Selected is TrmdTreeNodeVar then
  begin
    V := TrmdTreeNodeVar(tvObjects.Selected).FHandle;
    V.SetName(edVarName.Text);
    TrmdTreeNodeVar(tvObjects.Selected).Update;
    NotifyOwnerAboutChanges;    
  end;
end;

procedure TrmDsgnVariables.edVarNameKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    TEdit(Sender).OnExit(nil);
end;

procedure TrmDsgnVariables.edVarDescriptionExit(Sender: TObject);
var
  V: IrmVariable;
begin
  if tvObjects.Selected is TrmdTreeNodeVar then
  begin
    V := TrmdTreeNodeVar(tvObjects.Selected).FHandle;
    V.SetDisplayName(edVarDescription.Text);
    TrmdTreeNodeVar(tvObjects.Selected).Update;
    NotifyOwnerAboutChanges;
  end;
end;

procedure TrmDsgnVariables.cbVarTypeChange(Sender: TObject);
var
  V: IrmVariable;
begin
  if tvObjects.Selected is TrmdTreeNodeVar then
  begin
    V := TrmdTreeNodeVar(tvObjects.Selected).FHandle;
    if cbVarType.ItemIndex <> -1 then
      V.SetVarType(TrwVarTypeKind(cbVarType.ItemIndex))
    else
    begin
      V.SetVarType(rwvPointer);
      V.SetPointerType(cbVarType.Text);
    end;

    TrmdTreeNodeVar(tvObjects.Selected).Update;
    NotifyOwnerAboutChanges;
  end;
end;

procedure TrmDsgnVariables.rgVarGroupClick(Sender: TObject);
var
  V: IrmVariable;
  vis: TrwVarVisibility;
  N: TTreeNode;
begin
  if not FReadingProperties and (tvObjects.Selected is TrmdTreeNodeVar) then
  begin
    V := TrmdTreeNodeVar(tvObjects.Selected).FHandle;
    
    case rgVarGroup.ItemIndex of
      0:  begin
            vis := rvvPrivate;
            N := GetTopNode.GroupPrivate;
          end;

      1:  begin
            vis := rvvProtected;
            N := GetTopNode.GroupProtected;
          end;

      2:  begin
            vis := rvvPublic;
            N := GetTopNode.GroupPublic;
          end;

      3:  begin
            vis := rvvPublished;
            N := GetTopNode.GroupPublished;
          end;
    else
      vis := rvvPrivate;
      N := nil;
    end;

    V.SetVisibility(vis);

    TrmdTreeNodeVar(tvObjects.Selected).Update;
    TrmdTreeNodeVar(tvObjects.Selected).MoveTo(N, naAddChild);
    NotifyOwnerAboutChanges;
  end;
end;

procedure TrmDsgnVariables.chbShowInQBClick(Sender: TObject);
var
  V: IrmVariable;
begin
  if tvObjects.Selected is TrmdTreeNodeVar then
  begin
    V := TrmdTreeNodeVar(tvObjects.Selected).FHandle;
    V.SetShowInQB(chbShowInQB.Checked);
    TrmdTreeNodeVar(tvObjects.Selected).Update;
    NotifyOwnerAboutChanges;
  end;
end;

procedure TrmDsgnVariables.chbShowInFmlEditorClick(Sender: TObject);
var
  V: IrmVariable;
begin
  if tvObjects.Selected is TrmdTreeNodeVar then
  begin
    V := TrmdTreeNodeVar(tvObjects.Selected).FHandle;
    V.SetShowInFmlEditor(chbShowInFmlEditor.Checked);
    TrmdTreeNodeVar(tvObjects.Selected).Update;
    NotifyOwnerAboutChanges;    
  end;
end;

procedure TrmDsgnVariables.memVarValueExit(Sender: TObject);
var
  i: Integer;
  V: IrmVariable;
  ArrVal: Variant;
begin
  if tvObjects.Selected is TrmdTreeNodeVar then
  begin
    V := TrmdTreeNodeVar(tvObjects.Selected).FHandle;

    if V.GetVarType <> rwvArray then
      V.SetValue(InternalTypeToVariant(V.GetVarType, memVarValue.Text))
    else
    begin
      ArrVal := VarArrayCreate([0, memVarValue.Lines.Count - 1], varVariant);
      for i := 0 to memVarValue.Lines.Count - 1 do
        ArrVal[i] := memVarValue.Lines[i];
      V.SetValue(ArrVal);
    end;

    NotifyOwnerAboutChanges;
  end;
end;

procedure TrmDsgnVariables.NotifyOwnerAboutChanges;
begin
  SendApartmentMessage(Self, mVariablesChanged, 0, 0);
end;

procedure TrmDsgnVariables.DMRefreshVariables(var Message: TrmMessageRec);
begin
  inherited;
  Initialize;
end;

procedure TrmDsgnVariables.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mRefreshVariables));
end;

{ TrmdTreeNodeGroup }

procedure TrmdTreeNodeGroup.Update;
begin
  case FVisibility of
    rvvPublished: Text := 'Published';
    rvvPublic:    Text := 'Public';
    rvvProtected: Text := 'Protected';
    rvvPrivate:   Text := 'Private';
  end;

  ImageIndex := 3;
  StateIndex := ImageIndex;
  SelectedIndex := ImageIndex;
end;

procedure TrmDsgnVariables.tvObjectsChanging(Sender: TObject; Node: TTreeNode; var AllowChange: Boolean);
begin
  if tvObjects.CanFocus then
    tvObjects.SetFocus;
end;

end.
