inherited rmOPEDBCellValue: TrmOPEDBCellValue
  Width = 279
  inherited pcTypes: TPageControl
    Width = 279
    ActivePage = tsEmpty
    inherited tsEmpty: TTabSheet
      inherited frmEmpty: TrmOPEEmpty
        Width = 271
        inherited Label1: TLabel
          Width = 271
          Height = 100
        end
      end
    end
    inherited tsText: TTabSheet
      inherited Label2: TLabel
        Width = 271
      end
      inherited frmText: TrmOPEStringMultiLine
        Width = 271
        inherited mString: TMemo
          Width = 271
        end
      end
    end
    inherited tsNumber: TTabSheet
      inherited frmFormatNumber: TrmOPEFormat
        Top = 40
      end
      inherited frmNumber: TrmOPENumeric
        Top = 10
      end
    end
    inherited tsDateTime: TTabSheet
      inherited frmFormatDate: TrmOPEFormat
        Top = 40
      end
      inherited frmDate: TrmOPEDate
        Top = 10
      end
    end
    object tsField: TTabSheet [4]
      Caption = 'Field'
      ImageIndex = 4
      OnShow = tsFieldShow
      inline frmFormatField: TrmOPEFormat
        Left = 0
        Top = 73
        Width = 222
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
        inherited cbFormat: TComboBox
          Width = 167
        end
      end
      inline frmField: TrmOPEField
        Left = 0
        Top = 10
        Width = 271
        Height = 51
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited cbDataSource: TisRWDBComboBox
          Width = 216
        end
        inherited cbField: TisRWDBComboBox
          Width = 216
        end
      end
    end
    inherited tsCalc: TTabSheet
      inherited Panel1: TPanel
        Width = 271
      end
      inherited frmExpression: TrmOPEExpression
        Width = 271
        inherited frmExpression: TrmFMLExprPanel
          Width = 271
          inherited pnlCanvas: TPanel
            Width = 271
          end
        end
      end
    end
  end
end
