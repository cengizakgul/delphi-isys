unit rmDsgnLocalComponentFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDsgnComponentFrm, Menus, ImgList, ComCtrls, ExtCtrls, StdCtrls,
  rmDsgnTypes, rwDsgnUtils, rwUtils, rwTypes, rmTypes,
  rmLibCompChooseFrm, ExtDlgs, rwDsgnRes, EvStreamUtils, rwEngineTypes,
  rwEngine, rwDesigner, rmCustomFrameFrm,
  rmLocalToolBarFrm, rmTBDsgnComponentFrm, rmCustomStatusBarFrm, ActnList;

type
  TrmDsgnLocalComponent = class(TrmDsgnComponent)
    Panel1: TPanel;
    btnSave: TButton;
    btnCancel: TButton;
    procedure btnSaveClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);

  protected
    procedure PrepareDesigner; override;
    function  GetOwnerForNewComponent(ANewParent: IrmDesignObject): IrmDesignObject; override;
    function  IsInheritedInternalObjects(AObject: IrmDesignObject): Boolean; override;
    procedure Compile; override;
    function  LibComponentDesigninig: Boolean; override;
  end;

implementation

{$R *.dfm}

{ TrmDsgnLibrary }


function TrmDsgnLocalComponent.GetOwnerForNewComponent(ANewParent: IrmDesignObject): IrmDesignObject;
begin
  Result := inherited GetOwnerForNewComponent(ANewParent);

  if ObjectsAreTheSame(Result, CurrentDesignArea.DesignObject) and not (DesigningObject.ObjectInheritsFrom('TrmReport'))  then
    Result := DesigningObject;
end;

function TrmDsgnLocalComponent.IsInheritedInternalObjects(AObject: IrmDesignObject): Boolean;
var
  O: IrmDesignObject;
begin
  Result := AObject.IsInheritedComponent;

  if Result then
  begin
    O := AObject.GetObjectOwner;
    Result := Assigned(O) and not ObjectsAreTheSame(O, DesigningObject) and not O.IsVirtualOwner;
  end;
end;

procedure TrmDsgnLocalComponent.Compile;
begin
  DesigningObject.CallCustomFunction('CompileAsLibComp', []);
end;

procedure TrmDsgnLocalComponent.btnSaveClick(Sender: TObject);
begin
  SendDesignerMessage(Self, mModalDialogOK, 0, 0);
end;

procedure TrmDsgnLocalComponent.btnCancelClick(Sender: TObject);
begin
  SendDesignerMessage(Self, mModalDialogCancel, 0, 0);
end;

procedure TrmDsgnLocalComponent.PrepareDesigner;
begin
  DesigningObject.SetPropertyValue('Name', cParserLibComp);
  inherited;
end;

function TrmDsgnLocalComponent.LibComponentDesigninig: Boolean;
begin
  Result := True;
end;

end.
