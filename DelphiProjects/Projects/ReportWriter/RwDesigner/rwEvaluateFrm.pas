unit rwEvaluateFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwDebugInfo, rwVirtualMachine, rwParser, rwTypes, rwUtils,
  ActnList;

type
  TrwEvaluate = class(TForm)
    edExpression: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    meResult: TMemo;
    btnEvaluate: TButton;
    Label3: TLabel;
    edNewValue: TEdit;
    btnModify: TButton;
    ActionList1: TActionList;
    actClose: TAction;
    procedure edExpressionExit(Sender: TObject);
    procedure btnEvaluateClick(Sender: TObject);
    procedure btnModifyClick(Sender: TObject);
    procedure edNewValueExit(Sender: TObject);
    procedure edExpressionKeyPress(Sender: TObject; var Key: Char);
    procedure edNewValueKeyPress(Sender: TObject; var Key: Char);
    procedure actCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  public
  end;

  procedure EvaluateExpr(AExpr: string);

implementation

{$R *.DFM}

uses rwWatchListFrm;

procedure EvaluateExpr(AExpr: string);
var
  F: TrwEvaluate;
begin
  F := TrwEvaluate.Create(nil);
  with F do
    try
      edExpression.Text := AExpr;
      ShowModal;
    finally
      Free;
    end;
end;


procedure TrwEvaluate.edExpressionExit(Sender: TObject);
begin
  edExpression.Text := Trim(edExpression.Text);
end;

procedure TrwEvaluate.btnEvaluateClick(Sender: TObject);
var
  h: String;
begin
  meResult.Text := '';
  DebugInfo.WatchList.Add(edExpression.Text);
  DebugInfo.WatchList.NeedCompileWatches := True;
  try
    CompileWatches;
    if DebugInfo.WatchList[DebugInfo.WatchList.Count -  1].Address = -1 then
    begin
      h := DebugInfo.WatchList[DebugInfo.WatchList.Count -  1].Result;
      DebugInfo.WatchList[DebugInfo.WatchList.Count -  1].Expression := 'AsVariant(' + edExpression.Text + ')';
      DebugInfo.WatchList.NeedCompileWatches := True;
      CompileWatches;
      if DebugInfo.WatchList[DebugInfo.WatchList.Count -  1].Address = -1 then
        DebugInfo.WatchList[DebugInfo.WatchList.Count -  1].Result := h;
    end;

    VM.CalcWatches;
    DebugInfo.Waiting := True;
    meResult.Text := DebugInfo.WatchList[DebugInfo.WatchList.Count -  1].Result;

    if not DebugInfo.WatchList[DebugInfo.WatchList.Count -  1].Error then
    begin
      edNewValue.Text := meResult.Text;
      if DebugInfo.WatchList[DebugInfo.WatchList.Count -  1].PointerType <> '' then
      begin
        if meResult.Text = '0' then
          meResult.Text := 'nil';
        meResult.Text := DebugInfo.WatchList[DebugInfo.WatchList.Count -  1].PointerType +
          '(' + meResult.Text + ')';
      end;
    end
    else
      edNewValue.Text := '';

  finally
    DebugInfo.WatchList.Delete(DebugInfo.WatchList.Count -  1);
    DebugInfo.WatchList.NeedCompileWatches := True;
  end;
end;

procedure TrwEvaluate.btnModifyClick(Sender: TObject);
var
  Err: Boolean;
begin
  meResult.Text := '';
  DebugInfo.WatchList.Add(edExpression.Text + ' := ' + edNewValue.Text);
  DebugInfo.WatchList.NeedCompileWatches := True;
  try
    CompileWatches;
    VM.CalcWatches;
    DebugInfo.Waiting := True;
    meResult.Text := DebugInfo.WatchList[DebugInfo.WatchList.Count -  1].Result;
    Err := (DebugInfo.WatchList[DebugInfo.WatchList.Count -  1].Address = -1) or
            DebugInfo.ErrorHappend;
  finally
    DebugInfo.WatchList.Delete(DebugInfo.WatchList.Count -  1);
    DebugInfo.WatchList.NeedCompileWatches := True;
  end;

  if not Err then
    btnEvaluate.Click;
end;

procedure TrwEvaluate.edNewValueExit(Sender: TObject);
begin
  edNewValue.Text := Trim(edNewValue.Text);
end;

procedure TrwEvaluate.edExpressionKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    btnEvaluate.Click;
end;

procedure TrwEvaluate.edNewValueKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    btnModify.Click;
end;

procedure TrwEvaluate.actCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TrwEvaluate.FormShow(Sender: TObject);
begin
  if edExpression.Text <> '' then
    btnEvaluate.Click; 
end;

end.
