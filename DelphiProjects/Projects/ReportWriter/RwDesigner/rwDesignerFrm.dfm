object rwDesignerMainForm: TrwDesignerMainForm
  Left = 182
  Top = 155
  AutoScroll = False
  Caption = 'Report Writer Designer'
  ClientHeight = 557
  ClientWidth = 1028
  Color = clBtnFace
  Constraints.MinHeight = 550
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = mmMenu
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object stbStatus: TStatusBar
    Left = 0
    Top = 538
    Width = 1028
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Alignment = taCenter
        Width = 60
      end
      item
        Alignment = taCenter
        Width = 100
      end
      item
        Alignment = taCenter
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object pgcPages: TPageControl
    Left = 0
    Top = 0
    Width = 1028
    Height = 538
    ActivePage = tbsReportForm
    Align = alClient
    HotTrack = True
    TabOrder = 1
    OnChange = pgcPagesChange
    object tbsReportForm: TTabSheet
      Caption = 'Report'
      OnHide = tbsReportFormHide
      OnShow = tbsReportFormShow
      object tsReportSelector: TTabSet
        Left = 0
        Top = 489
        Width = 1020
        Height = 21
        Align = alBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        OnChange = tsReportSelectorChange
      end
      inline ReportFormToolsBar: TrwReportFormToolsBar
        Left = 0
        Top = 0
        Width = 1020
        Height = 52
        Align = alTop
        AutoScroll = False
        AutoSize = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        inherited ctbBar: TControlBar
          Width = 1020
          inherited tlbFont: TToolBar
            Left = 602
            Top = 28
          end
          inherited tlbExperts: TToolBar
            Left = 766
            Top = 2
            inherited tbtEvents: TToolButton
              OnClick = ReportFormToolsBartbtEventsClick
            end
          end
          inherited tlbActions: TToolBar
            inherited tbtOpen: TToolButton
              Action = actImport
            end
            inherited tbtSave: TToolButton
              Action = actExport
            end
            inherited tbtCompile: TToolButton
              Action = actCompile
            end
            inherited tbtRun: TToolButton
              Action = actRun
            end
            inherited tbtPause: TToolButton
              Action = actPause
            end
            inherited tbtTerminate: TToolButton
              Action = actTerminate
            end
            inherited tbtTraceInto: TToolButton
              Action = actTraceInto
            end
            inherited tbtStepOver: TToolButton
              Action = actStepOver
            end
            inherited tbtBrkPoint: TToolButton
              Action = actBrkPoint
            end
            inherited tbtCut: TToolButton
              Action = actCut
            end
            inherited tbtCopy: TToolButton
              Action = actCopy
            end
            inherited tbtPaste: TToolButton
              Action = actPaste
            end
          end
        end
        inherited pmToolBars: TPopupMenu
          Left = 824
        end
      end
      inline frmReportForm: TrwPageDesigner
        Left = 0
        Top = 52
        Width = 1020
        Height = 437
        Align = alClient
        AutoScroll = False
        TabOrder = 2
        inherited Splitter1: TSplitter
          Top = 148
          Width = 1020
        end
        inherited pnlVertRuler: TPanel
          Height = 125
        end
        inherited pnlHorRuler: TPanel
          Width = 1020
          PopupMenu = ReportFormToolsBar.pmToolBars
        end
        inherited Panel3: TPanel
          Top = 151
          Width = 1020
          inherited Panel1: TPanel
            Width = 751
            inherited pcEvents: TPageControl
              Width = 751
            end
          end
        end
      end
    end
    object tbsInputForm: TTabSheet
      Caption = 'Input Form'
      ImageIndex = 2
      OnHide = tbsInputFormHide
      OnShow = tbsInputFormShow
      inline InFormToolsBar: TrwInFormToolsBar
        Left = 0
        Top = 0
        Width = 1020
        Height = 52
        Align = alTop
        AutoScroll = False
        AutoSize = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        inherited ctbBar: TControlBar
          Width = 1020
          inherited tlbText: TToolBar
            Height = 22
          end
          inherited tlbExperts: TToolBar
            Left = 804
            Top = 2
            Width = 10
          end
          inherited tlbActions: TToolBar
            inherited tbtOpen: TToolButton
              Action = actImport
            end
            inherited tbtSave: TToolButton
              Action = actExport
            end
            inherited tbtCompile: TToolButton
              Action = actCompile
            end
            inherited tbtRun: TToolButton
              Action = actRun
            end
            inherited tbtPause: TToolButton
              Action = actPause
            end
            inherited tbtTerminate: TToolButton
              Action = actTerminate
            end
            inherited tbtTraceInto: TToolButton
              Action = actTraceInto
            end
            inherited tbtStepOver: TToolButton
              Action = actStepOver
            end
            inherited tbtBrkPoint: TToolButton
              Action = actBrkPoint
            end
            inherited tbtCut: TToolButton
              Action = actCut
            end
            inherited tbtCopy: TToolButton
              Action = actCopy
            end
            inherited tbtPaste: TToolButton
              Action = actPaste
            end
          end
        end
      end
      inline frmInputForm: TrwPageDesigner
        Left = 0
        Top = 52
        Width = 1020
        Height = 458
        Align = alClient
        AutoScroll = False
        TabOrder = 1
        inherited Splitter1: TSplitter
          Top = 169
          Width = 1020
        end
        inherited pnlVertRuler: TPanel
          Height = 146
        end
        inherited pnlHorRuler: TPanel
          Width = 1020
          PopupMenu = InFormToolsBar.pmToolBars
        end
        inherited Panel3: TPanel
          Top = 172
          Width = 1020
          inherited Panel1: TPanel
            Width = 751
            inherited pcEvents: TPageControl
              Width = 751
            end
          end
        end
      end
    end
    object tbsPreview: TTabSheet
      Caption = 'Preview'
      ImageIndex = 3
      TabVisible = False
      OnHide = tbsPreviewHide
      inline rwPreview: TrwOneRepPreview
        Left = 0
        Top = 0
        Width = 1020
        Height = 510
        Align = alClient
        AutoScroll = False
        Constraints.MinHeight = 230
        Constraints.MinWidth = 480
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = True
        inherited ctbTools: TControlBar
          Width = 1020
          inherited tlbTools: TToolBar
            Width = 952
            inherited pnlPages: TPanel
              inherited pnlPageNum: TPanel
                OnClick = rwPreviewpnlPageNumClick
              end
            end
          end
        end
        inherited sbxPreview: TScrollBox
          Width = 1020
          Height = 484
        end
        inherited aclActions: TActionList
          inherited actPrint: TAction
            OnExecute = rwPreviewactPrintExecute
          end
        end
      end
    end
  end
  object aclDesignerActions: TActionList
    Images = DsgnRes.ilButtons
    Left = 304
    Top = 134
    object actRun: TAction
      Category = 'Run'
      Hint = 'Run report'
      ImageIndex = 69
      ShortCut = 120
      OnExecute = actRunExecute
    end
    object actPause: TAction
      Category = 'Run'
      Caption = 'Report Pause'
      Hint = 'Stop running'
      ImageIndex = 70
      OnExecute = actPauseExecute
    end
    object actTerminate: TAction
      Category = 'Run'
      Caption = 'Terminate'
      Hint = 'Terminate running'
      ImageIndex = 72
      ShortCut = 16497
      OnExecute = actTerminateExecute
    end
    object actTraceInto: TAction
      Category = 'Run'
      Caption = 'Trace Into'
      Hint = 'Trace Into'
      ImageIndex = 67
      ShortCut = 118
      OnExecute = actTraceIntoExecute
    end
    object actPageFooter: TAction
      Category = 'Bands'
      Caption = 'Page Footer'
      OnExecute = actPageFooterExecute
    end
    object actSummary: TAction
      Category = 'Bands'
      Caption = 'Summary'
      OnExecute = actSummaryExecute
    end
    object actExit: TAction
      Category = 'File'
      Caption = 'Exit'
      OnExecute = actExitExecute
    end
    object actLoadRes: TAction
      Category = 'File'
      Caption = 'Load Saved Result...'
    end
    object actPageHeader: TAction
      Category = 'Bands'
      Caption = 'Page Header'
      OnExecute = actPageHeaderExecute
    end
    object actTitle: TAction
      Category = 'Bands'
      Caption = 'Title'
      OnExecute = actTitleExecute
    end
    object actHeader: TAction
      Category = 'Bands'
      Caption = 'Header'
      OnExecute = actHeaderExecute
    end
    object actFooter: TAction
      Category = 'Bands'
      Caption = 'Footer'
      OnExecute = actFooterExecute
    end
    object actPageStyle: TAction
      Category = 'Bands'
      Caption = 'Page Style'
      OnExecute = actPageStyleExecute
    end
    object actNew: TAction
      Category = 'File'
      Caption = 'Clear'
      OnExecute = actNewExecute
    end
    object actPageFormat: TAction
      Category = 'Report'
      Caption = 'Page Format...'
      OnExecute = actPageFormatExecute
    end
    object actUndo: TAction
      Category = 'Edit'
      Caption = 'UnDelete'
      Hint = 'Undelete last deleted object'
      ImageIndex = 80
      OnExecute = actUndoExecute
    end
    object actDelSelection: TAction
      Category = 'Edit'
      Caption = 'Delete'
      ImageIndex = 79
      ShortCut = 16430
      OnExecute = actDelSelectionExecute
    end
    object actCopy: TAction
      Category = 'Edit'
      Caption = 'Copy'
      ImageIndex = 77
      ShortCut = 16451
      OnExecute = actCopyExecute
    end
    object actCut: TAction
      Category = 'Edit'
      Caption = 'Cut'
      ImageIndex = 76
      ShortCut = 16472
      OnExecute = actCutExecute
    end
    object actPaste: TAction
      Category = 'Edit'
      Caption = 'Paste'
      ImageIndex = 78
      ShortCut = 16470
      OnExecute = actPasteExecute
    end
    object actExport: TAction
      Category = 'File'
      Caption = 'Export to File...'
      ImageIndex = 59
      OnExecute = actExportExecute
    end
    object actImport: TAction
      Category = 'File'
      Caption = 'Import from File...'
      ImageIndex = 58
      OnExecute = actImportExecute
    end
    object actGroups: TAction
      Category = 'Bands'
      Caption = 'Groups...'
      OnExecute = actGroupsExecute
    end
    object actQueryBuilder: TAction
      Category = 'View'
      Caption = 'Query Builder...'
      ImageIndex = 39
      OnExecute = actQueryBuilderExecute
    end
    object actLibrary: TAction
      Category = 'View'
      Caption = 'Library...'
      ImageIndex = 46
      OnExecute = actLibraryExecute
    end
    object actSubDetail: TAction
      Category = 'Bands'
      Caption = 'SubDetail'
      OnExecute = actSubDetailExecute
    end
    object actSubSummary: TAction
      Category = 'Bands'
      Caption = 'SubSummary'
      OnExecute = actSubSummaryExecute
    end
    object actCustom: TAction
      Category = 'Bands'
      Caption = 'Custom'
      OnExecute = actCustomExecute
    end
    object actFromLib: TAction
      Category = 'File'
      Caption = 'Inherit Report from Library...'
      OnExecute = actFromLibExecute
    end
    object actQuickSave: TAction
      Category = 'File'
      Caption = 'Quick Save to File'
      ShortCut = 16467
      OnExecute = actQuickSaveExecute
    end
    object actSaveRes: TAction
      Category = 'File'
      Caption = 'Save Result...'
    end
    object actCompile: TAction
      Category = 'Report'
      Caption = 'Compile'
      Hint = 'Compile report'
      ImageIndex = 60
      ShortCut = 16504
      OnExecute = actCompileExecute
    end
    object actPCode: TAction
      Category = 'View'
      Caption = 'P-Code'
      ImageIndex = 81
      ShortCut = 49219
      OnExecute = actPCodeExecute
    end
    object actWatches: TAction
      Category = 'View'
      Caption = 'Watches'
      ImageIndex = 65
      ShortCut = 49239
      OnExecute = actWatchesExecute
    end
    object actStepOver: TAction
      Category = 'Run'
      Caption = 'Step Over'
      Hint = 'Step Over'
      ImageIndex = 66
      ShortCut = 119
      OnExecute = actStepOverExecute
    end
    object actBrkPoint: TAction
      Category = 'Run'
      Caption = 'Break Point On/Off'
      Hint = 'Breakpoint On/Off'
      ImageIndex = 68
      ShortCut = 116
      OnExecute = actBrkPointExecute
    end
    object actAddWatch: TAction
      Category = 'Run'
      Caption = 'Add Watch...'
      Hint = 'Add Watch'
      ImageIndex = 63
      ShortCut = 16500
      OnExecute = actAddWatchExecute
    end
    object actParamFile: TAction
      Category = 'Run'
      Caption = 'External Parameter File...'
      Hint = 'Report parameters'
      OnExecute = actParamFileExecute
    end
    object actDSExplorer: TAction
      Category = 'View'
      Caption = 'DataSet Explorer'
      ImageIndex = 73
      ShortCut = 49220
      OnExecute = actDSExplorerExecute
    end
    object actEnvOpt: TAction
      Category = 'View'
      Caption = 'Environment Optionts...'
      ImageIndex = 74
      OnExecute = actEnvOptExecute
    end
    object actEvaluate: TAction
      Category = 'View'
      Caption = 'Evaluate/Modify...'
      ImageIndex = 86
      ShortCut = 16502
      OnExecute = actEvaluateExecute
    end
    object actSecurity: TAction
      Category = 'Report'
      Caption = 'Security Options...'
      ImageIndex = 91
      OnExecute = actSecurityExecute
    end
  end
  object mmMenu: TMainMenu
    Images = DsgnRes.ilButtons
    Left = 184
    Top = 108
    object miFile: TMenuItem
      Caption = 'File'
      object miNew: TMenuItem
        Action = actNew
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object miExporttoFile: TMenuItem
        Action = actExport
        ShortCut = 16453
      end
      object miImportfromFile: TMenuItem
        Action = actImport
        ShortCut = 16457
      end
      object SavetoFile1: TMenuItem
        Action = actQuickSave
      end
      object miLoadFromLib: TMenuItem
        Action = actFromLib
      end
      object N13: TMenuItem
        Caption = '-'
      end
      object miCompile: TMenuItem
        Action = actCompile
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object miExit: TMenuItem
        Action = actExit
      end
    end
    object miEdit: TMenuItem
      Caption = '&Edit'
      object miUndo: TMenuItem
        Action = actUndo
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object miCut: TMenuItem
        Action = actCut
      end
      object miCopy: TMenuItem
        Action = actCopy
      end
      object miPaste: TMenuItem
        Action = actPaste
      end
      object miDelete: TMenuItem
        Action = actDelSelection
      end
    end
    object View1: TMenuItem
      Caption = '&View'
      object EnvironmentOptionts1: TMenuItem
        Action = actEnvOpt
        Caption = 'Environment Options...'
      end
      object QueryBuilder1: TMenuItem
        Action = actQueryBuilder
      end
      object Library1: TMenuItem
        Action = actLibrary
      end
      object miReportStat: TMenuItem
        Caption = 'Report Statistics...'
        OnClick = miReportStatClick
      end
      object DebugerWindows1: TMenuItem
        Caption = 'Debug Windows'
        object Watches1: TMenuItem
          Action = actWatches
        end
        object Evaluate1: TMenuItem
          Action = actEvaluate
        end
        object DataSetExplorer1: TMenuItem
          Action = actDSExplorer
        end
        object PCode1: TMenuItem
          Action = actPCode
        end
      end
    end
    object miReport: TMenuItem
      Caption = '&Report'
      object miBands: TMenuItem
        Caption = 'Bands'
        OnClick = miBandsClick
        object miPageHeader: TMenuItem
          Action = actPageHeader
        end
        object miPageFooter: TMenuItem
          Action = actPageFooter
        end
        object N2: TMenuItem
          Caption = '-'
        end
        object miTitle: TMenuItem
          Action = actTitle
        end
        object miSummary: TMenuItem
          Action = actSummary
        end
        object N11: TMenuItem
          Caption = '-'
        end
        object miSubDetail: TMenuItem
          Action = actSubDetail
        end
        object miSubSummary: TMenuItem
          Action = actSubSummary
        end
        object N3: TMenuItem
          Caption = '-'
        end
        object miHeader: TMenuItem
          Action = actHeader
        end
        object miFooter: TMenuItem
          Action = actFooter
        end
        object N4: TMenuItem
          Caption = '-'
        end
        object miPageStyle: TMenuItem
          Action = actPageStyle
        end
        object miCustom: TMenuItem
          Action = actCustom
        end
        object N7: TMenuItem
          Caption = '-'
        end
        object miGroups: TMenuItem
          Action = actGroups
        end
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object miPageFormat: TMenuItem
        Action = actPageFormat
      end
      object pmLicencing: TMenuItem
        Action = actSecurity
      end
    end
    object Run1: TMenuItem
      Caption = 'Run'
      object Run2: TMenuItem
        Action = actRun
        Caption = 'Run'
      end
      object actPause1: TMenuItem
        Action = actPause
      end
      object Terminate1: TMenuItem
        Action = actTerminate
      end
      object TraceInto1: TMenuItem
        Action = actTraceInto
      end
      object StepOver1: TMenuItem
        Action = actStepOver
      end
      object N14: TMenuItem
        Caption = '-'
      end
      object BreakPointOnOff1: TMenuItem
        Action = actBrkPoint
      end
      object AddWatch1: TMenuItem
        Action = actAddWatch
      end
      object actParamFile1: TMenuItem
        Action = actParamFile
      end
    end
    object Help1: TMenuItem
      Caption = '&Help'
      OnClick = Help1Click
    end
  end
  object opdImport: TOpenDialog
    DefaultExt = 'rwr'
    Filter = 
      'ReportWriter Reports Files (*.rwr)|*.rwr|Text Files (*.txt)|*.tx' +
      't|All Files (*.*)|*.*'
    Options = [ofHideReadOnly, ofNoReadOnlyReturn, ofEnableSizing]
    Title = 'Import report'
    Left = 252
    Top = 184
  end
  object svdExport: TSaveDialog
    DefaultExt = 'rwr'
    Filter = 
      'ReportWriter Reports Files (*.rwr)|*.rwr|Text Files (*.txt)|*.tx' +
      't|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Export report'
    Left = 284
    Top = 184
  end
  object opdOpenResult: TOpenDialog
    DefaultExt = 'txt'
    Filter = 'Text Files (*.txt)|*.txt|All Files (*.*)|*.*'
    Title = 'Open Report Writer ASCII result file'
    Left = 252
    Top = 224
  end
  object svdSaveResult: TSaveDialog
    DefaultExt = 'txt'
    Filter = 'Text File (*.txt)|*.txt|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save Report Writer ASCII result file'
    Left = 284
    Top = 224
  end
end
