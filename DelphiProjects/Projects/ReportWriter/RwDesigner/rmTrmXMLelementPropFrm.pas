unit rmTrmXMLelementPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmXMLQueryElementPropFrm, StdCtrls, ExtCtrls, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls, rmOPEQueryFrm, rmOPEExpressionFrm,
  rmOPEBlockingControlFrm, rmOPEMasterFieldsFrm, rmOPBasicFrm,
  rmOPEBooleanFrm;

type
  TrmTrmXMLelementProp = class(TrmTrmXMLQueryElementProp)
    frmNSName: TrmOPEStringSingleLine;
  private
  public
    procedure GetPropsFromObject; override;
  end;


implementation

{$R *.dfm}

{ TrmOPBasic1 }

procedure TrmTrmXMLelementProp.GetPropsFromObject;
begin
  inherited;
  frmNSName.PropertyName := 'NSName';
  frmNSName.ShowPropValue;
end;

end.
 