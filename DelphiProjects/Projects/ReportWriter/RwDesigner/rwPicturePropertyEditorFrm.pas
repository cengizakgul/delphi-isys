unit rwPicturePropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwPropertyEditorFrm, ExtDlgs, StdCtrls, ExtCtrls;

type
  TrwPicturePropertyEditor = class(TrwPropertyEditor)
    OpenPictureDialog: TOpenPictureDialog;
    Panel1: TPanel;
    imgPicture: TImage;
    btnLoad: TButton;
    btnSave: TButton;
    SavePictureDialog: TSavePictureDialog;
    procedure FormCreate(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
  end;


implementation

{$R *.DFM}


procedure TrwPicturePropertyEditor.FormCreate(Sender: TObject);
begin
  inherited;

  FValue := imgPicture.Picture;
end;



procedure TrwPicturePropertyEditor.btnLoadClick(Sender: TObject);
begin
  if OpenPictureDialog.Execute then
    TPicture(FValue).LoadFromFile(OpenPictureDialog.FileName);
end;

procedure TrwPicturePropertyEditor.btnSaveClick(Sender: TObject);
begin
  if SavePictureDialog.Execute then
    TPicture(FValue).SaveToFile(SavePictureDialog.FileName);
end;

initialization
  RegisterClass(TrwPicturePropertyEditor);


end.
