inherited rmTrmDBSubTableProp: TrmTrmDBSubTableProp
  Width = 365
  Height = 361
  inherited pcCategories: TPageControl
    Width = 365
    Height = 361
    inherited tsBasic: TTabSheet
      inline frmMasterFields: TrmOPEMasterFields
        Left = 8
        Top = 141
        Width = 340
        Height = 182
        AutoScroll = False
        TabOrder = 3
      end
    end
    inherited tcPrint: TTabSheet
      inherited frmPrintOnEachPage: TrmOPEBoolean
        Top = 277
        Visible = False
      end
      inherited frmBlockParentIfEmpty: TrmOPEBoolean
        Top = 34
      end
      inherited pnlBlocking: TPanel
        Top = 71
      end
      inherited frmMultiPaged: TrmOPEBoolean
        Top = 300
        Visible = False
      end
    end
  end
end
