unit rmTrmlCBFieldValuesPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmlCaptionControlPropFrm, StdCtrls, ExtCtrls,
  rmOPEReportParamFrm, rmOPEFontFrm, rmOPEColorFrm, rmOPEEnumFrm,
  rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, ComCtrls, rwUtils,
  rwDsgnUtils, rwDataDictionary;

type
  TrmTrmlCBFieldValuesProp = class(TrmTrmlCaptionControlProp)
    grbDDInfo: TGroupBox;
    frmTable: TrmOPEEnum;
    frmField: TrmOPEEnum;
    procedure frmTablecbListChange(Sender: TObject);
  private
    procedure PrepareTableList;
    procedure PrepareFieldList(const ATable: String);
  protected
    procedure GetPropsFromObject; override;
  public
  end;

implementation

uses rwCustomDataDictionary;

{$R *.dfm}

{ TrmTrmlCBFieldValuesProp }

procedure TrmTrmlCBFieldValuesProp.GetPropsFromObject;
begin
  inherited;

  PrepareTableList;

  frmTable.PropertyName := 'ddTableName';
  frmField.PropertyName := 'ddFieldName';
  frmTable.ShowPropValue;
  frmTable.cbList.OnChange(nil);
  frmField.ShowPropValue;
end;

procedure TrmTrmlCBFieldValuesProp.PrepareFieldList(const ATable: String);
var
  ValList: TStringList;
  i: Integer;
  h: String;
  Tbl: TrwDataDictTable;
begin
  Tbl := TrwDataDictTable(DataDictionary.Tables.TableByName(ATable));

  if not Assigned(Tbl) then
    Exit;

  ValList := TStringList.Create;
  try
    for i := 0 to Tbl.Fields.Count - 1 do
    begin
      if Tbl.Fields[i].FieldValues.Text = '' then
        Continue;

      if DesignRMAdvancedMode then
        h := Tbl.Fields[i].Name
      else
      begin
        h := Tbl.Fields[i].DisplayName;
        if h = '' then
          Continue;
      end;

      h := Tbl.Fields[i].Name + '=' + h;
      ValList.Add(h);
    end;

    frmField.Values := ValList;
  finally
    FreeAndNil(ValList);
  end;
end;

procedure TrmTrmlCBFieldValuesProp.PrepareTableList;
var
  ValList: TStringList;
  i: Integer;
  h: String;
begin
  ValList := TStringList.Create;
  try
    for i := 0 to DataDictionary.Tables.Count - 1 do
    begin
      if DesignRMAdvancedMode then
        h := DataDictionary.Tables[i].Name
      else
      begin
        h := DataDictionary.Tables[i].DisplayName;
        if h = '' then
          Continue;
      end;

      h := DataDictionary.Tables[i].Name + '=' + h;
      ValList.Add(h);
    end;

    frmTable.Values := ValList;
  finally
    FreeAndNil(ValList);
  end;
end;

procedure TrmTrmlCBFieldValuesProp.frmTablecbListChange(Sender: TObject);
begin
  inherited;
  frmTable.cbListChange(Sender);
  PrepareFieldList(frmTable.NewPropertyValue);
end;

end.
