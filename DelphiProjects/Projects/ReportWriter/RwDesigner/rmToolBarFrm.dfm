inherited rmToolBar: TrmToolBar
  Width = 686
  Height = 48
  AutoSize = True
  object ctrbToolBar: TControlBar
    Left = 0
    Top = 21
    Width = 686
    Height = 27
    Align = alTop
    AutoSize = True
    BevelEdges = [beBottom]
    BevelOuter = bvNone
    TabOrder = 0
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 686
    Height = 21
    ButtonHeight = 21
    ButtonWidth = 51
    Caption = 'ToolBar1'
    EdgeBorders = []
    EdgeInner = esNone
    EdgeOuter = esNone
    Flat = True
    Menu = MainMenu
    ShowCaptions = True
    TabOrder = 1
    Transparent = False
    Wrapable = False
  end
  object MainMenu: TMainMenu
    Left = 297
    Top = 9
    object miFile: TMenuItem
      Caption = 'File'
      GroupIndex = 1
      object miExit: TMenuItem
        Caption = 'Exit'
        OnClick = miExitClick
      end
    end
    object View1: TMenuItem
      Caption = 'View'
      GroupIndex = 2
      object miAdvancedMode: TMenuItem
        AutoCheck = True
        Caption = 'Expert Mode'
        OnClick = miAdvancedModeClick
      end
    end
    object miWindows: TMenuItem
      Caption = 'Windows'
      GroupIndex = 100
    end
  end
end
