inherited rmTrmlDateRangeProp: TrmTrmlDateRangeProp
  Height = 302
  inherited pcCategories: TPageControl
    Height = 302
    inherited tsBasic: TTabSheet
      inline frmCaption: TrmOPEStringSingleLine
        Left = 8
        Top = 88
        Width = 290
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 2
        inherited lPropName: TLabel
          Width = 36
          Caption = 'Caption'
        end
      end
    end
    inherited tcAppearance: TTabSheet
      TabVisible = False
    end
    inherited tsParam: TTabSheet
      object lHeader1: TLabel [0]
        Left = 8
        Top = 10
        Width = 290
        Height = 18
        AutoSize = False
        Caption = 'Period Begin Date'
        Color = 14737632
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Layout = tlCenter
      end
      object lHeader2: TLabel [1]
        Left = 8
        Top = 147
        Width = 290
        Height = 18
        AutoSize = False
        Caption = 'Period End Date'
        Color = 14737632
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Layout = tlCenter
      end
      inherited frmParam1: TrmOPEReportParam
        Top = 37
      end
      inline frmParam2: TrmOPEReportParam
        Left = 8
        Top = 174
        Width = 290
        Height = 82
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited edParamName: TEdit
          Width = 200
        end
        inherited edParamDescription: TEdit
          Width = 199
        end
      end
    end
  end
end
