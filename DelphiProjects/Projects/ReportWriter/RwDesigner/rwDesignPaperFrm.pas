// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwDesignPaperFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwDesignAreaFrm, ExtCtrls, rwTypes, rwReport, StdCtrls,
  rwBasicClasses, rwCommonClasses, rwUtils, Menus, rwLibCompChoiceFrm,
  rwReportControls;

type
  TrwPaperArea = class(TrwArea)
  protected
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
  end;

  {TrwDesignPaper is report paper for designing}

  TrwDesignPaper = class(TrwDesignArea)
    procedure Shape2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    FReport: TrwCustomReport;

    function GetBand(Index: Integer): TrwBand;
    function GetBandCount: Integer;
    procedure ChangedPageFormat(Sender: TObject);
    procedure SetReport(Value: TrwCustomReport);
    procedure RWChangeBandList(var Message: TMessage); message RW_CHANGE_BAND_LIST;

  protected
    function SelectLibComp: TrwLibComponent; override;
    procedure PaintWindow(DC: HDC); override;

  public
    property Bands[Index: Integer]: TrwBand read GetBand;
    property BandCount: Integer read GetBandCount;
    property Report: TrwCustomReport read FReport write SetReport;

    constructor Create(AOwner: TComponent); override;
    procedure SyncBandsByPaper;
    procedure CreateBand(ABandType: TrwBandType);
    procedure DeleteBand(ABandType: TrwBandType);
    function IndexOfBand(ABandType: TrwBandType): Integer;
    function IsBandExists(ABandType: TrwBandType): Boolean;
    procedure DestroySelectedComponents; override;

  end;

implementation

{$R *.DFM}

constructor TrwDesignPaper.Create(AOwner: TComponent);
begin
  inherited;
  FReport := nil;
  FreeAndNil(pnlArea);
  pnlArea := TrwPaperArea.Create(Self);
  pnlArea.Parent := Self;
  pnlArea.OnMouseDown := pnlAreaMouseDown;
  pnlArea.Name := 'pnlArea';
end;

procedure TrwDesignPaper.SetReport(Value: TrwCustomReport);
begin
  if Assigned(FReport) then
  begin
    FReport.OnChangePageFormat := nil;
    FReport.ReportForm.DesignParent := nil;
    ComponentsOwner := nil;
  end;

  FReport := Value;

  if Assigned(FReport) then
  begin
    ChangedPageFormat(nil);

    FReport.OnChangePageFormat := ChangedPageFormat;
    FReport.ReportForm.DesignParent := pnlArea;
    ComponentsOwner := FReport.ReportForm;
    if not FReport.InheritedComponent then
      CreateBand(rbtDetail);
  end
end;

function TrwDesignPaper.GetBand(Index: Integer): TrwBand;
begin
  Result := Report.ReportForm.Bands[Index];
end;

function TrwDesignPaper.GetBandCount: Integer;
begin
  Result := Report.ReportForm.BandCount;
end;

procedure TrwDesignPaper.CreateBand(ABandType: TrwBandType);
begin
  Report.ReportForm.AddBand(ABandType);
//  AfterCreationComponent(lBand, nil, 0, 0);
end;

procedure TrwDesignPaper.SyncBandsByPaper;
begin
  ComponentsOwner := FReport.ReportForm;
  FReport.ReportForm.DesignParent := pnlArea;
end;

procedure TrwDesignPaper.DeleteBand(ABandType: TrwBandType);
begin
//  BeforeDestroyingComponent(lBand);
  Report.ReportForm.DeleteBand(ABandType);
end;

function TrwDesignPaper.IndexOfBand(ABandType: TrwBandType): Integer;
begin
  Result := Report.ReportForm.IndexOfBand(ABandType);
end;

function TrwDesignPaper.IsBandExists(ABandType: TrwBandType): Boolean;
begin
  Result := (IndexOfBand(ABandType) <> -1);
end;

procedure TrwDesignPaper.ChangedPageFormat(Sender: TObject);
begin
  Height := System.Round(ConvertUnit(FReport.PageFormat.Height/10, utMillimeters, utScreenPixels));
  Width := System.Round(ConvertUnit(FReport.PageFormat.Width/ 10, utMillimeters, utScreenPixels));

  pnlArea.Top := System.Round(ConvertUnit(FReport.PageFormat.TopMargin/10, utMillimeters, utScreenPixels));
  pnlArea.Height := Height - pnlArea.Top - System.Round(ConvertUnit(FReport.PageFormat.BottomMargin/10, utMillimeters, utScreenPixels));
  pnlArea.Left := System.Round(ConvertUnit(FReport.PageFormat.LeftMargin/10, utMillimeters, utScreenPixels));
  pnlArea.Width := Width - pnlArea.Left - System.Round(ConvertUnit(FReport.PageFormat.RightMargin/10, utMillimeters, utScreenPixels));
end;

procedure TrwDesignPaper.DestroySelectedComponents;
var
  lComp: TrwComponent;
begin
  inherited;

  while SelectedCompList.Count > 0 do
  begin
    lComp := SelectedCompList[0];
    if not (lComp is TrwBand) then
    begin
      BeforeDestroyingComponent(lComp);
      lComp.Free;
    end
    else
      DeleteSelectionFor(lComp);
  end;

  pnlAreaMouseDown(pnlArea, mbLeft, [], 0, 0);
end;

procedure TrwDesignPaper.RWChangeBandList(var Message: TMessage);
begin
  if Message.WParam = 1 then
    AfterCreationComponent(TrwBand(Pointer(Message.LParam)), nil, 0, 0)
  else
    BeforeDestroyingComponent(TrwBand(Pointer(Message.LParam)));
end;

function TrwDesignPaper.SelectLibComp: TrwLibComponent;
begin
  Result := ChoiceLibComp([rctReportComp, rctNoVisualComp]);
end;

procedure TrwDesignPaper.Shape2MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  pnlArea.OnMouseDown(pnlArea, Button, Shift, X, Y);
end;

procedure TrwDesignPaper.PaintWindow(DC: HDC);
var
  brh: HBRUSH;
begin
  inherited;

  Brh := CreateHatchBrush(HS_CROSS, $00F7F7F7);
  SelectObject(DC, Brh);
  FillRect(DC, ClientRect, Brh);
  DeleteObject(Brh);
end;


{ TrwPaperArea }

constructor TrwPaperArea.Create(AOwner: TComponent);
begin
  inherited;
  Transparent := False;
  Color := clWhite;
//  DoubleBuffered := True;
end;

procedure TrwPaperArea.Paint;
begin
  inherited;

  with Canvas do
  begin
    Brush.Color := $00F7F7F7;
    Brush.Style := bsCross;
    Pen.Color := clBlue;
    Pen.Mode := pmCopy;
    Pen.Style := psDot;
    Rectangle(ClientRect);
  end;
end;


end.
