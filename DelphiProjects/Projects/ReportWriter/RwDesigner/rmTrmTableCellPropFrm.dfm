inherited rmTrmTableCellProp: TrmTrmTableCellProp
  Width = 334
  Height = 400
  inherited pcCategories: TPageControl
    Width = 334
    Height = 400
    inherited tsBasic: TTabSheet
      object GroupBox2: TGroupBox
        Left = 8
        Top = 79
        Width = 310
        Height = 187
        Caption = 'Value'
        TabOrder = 2
        inline frmValue: TrmOPEValue
          Left = 10
          Top = 20
          Width = 289
          Height = 157
          AutoScroll = False
          TabOrder = 0
          inherited pcTypes: TPageControl
            Width = 289
            Height = 157
            inherited tsEmpty: TTabSheet
              inherited frmEmpty: TrmOPEEmpty
                Width = 281
                Height = 126
                inherited Label1: TLabel
                  Width = 281
                  Height = 126
                end
              end
            end
          end
        end
      end
    end
    object tsTextFilters: TTabSheet [1]
      Caption = 'Text Filter'
      ImageIndex = 5
      inline frmTextFilter: TrmOPETextFilter
        Left = 8
        Top = 10
        Width = 308
        Height = 335
        AutoScroll = False
        TabOrder = 0
        inherited Panel1: TPanel
          Width = 308
          inherited lHeader2: TLabel
            Width = 308
          end
          inherited lvFilters: TListView
            Width = 308
          end
        end
        inherited pnlParams: TPanel
          Width = 308
          Height = 153
        end
      end
    end
    inherited tcAppearance: TTabSheet
      inline frmFont: TrmOPEFont [0]
        Left = 10
        Top = 9
        Width = 75
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
      end
      inherited frmColor: TrmOPEColor
        Left = 98
        Top = 9
        TabOrder = 1
      end
    end
    object tsBorderLines: TTabSheet [3]
      Caption = 'Border Lines'
      ImageIndex = 2
      inline frmBorderLines: TrmOPEBorderLines
        Left = 7
        Top = 10
        Width = 303
        Height = 182
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
      end
    end
    inherited tcPrint: TTabSheet
      inherited frmPrintOnEachPage: TrmOPEBoolean
        Top = 301
        Visible = False
      end
      inherited frmBlockParentIfEmpty: TrmOPEBoolean
        Top = 278
        Enabled = False
        Visible = False
      end
      inherited pnlBlocking: TPanel
        Enabled = False
        TabOrder = 7
        Visible = False
      end
      inherited frmMultiPaged: TrmOPEBoolean
        Left = 6
        Top = 324
        TabOrder = 6
        Visible = False
        inherited chbProp: TCheckBox
          Enabled = False
        end
      end
      inline frmAutoHeight: TrmOPEBoolean
        Left = 8
        Top = 36
        Width = 152
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 3
        inherited chbProp: TCheckBox
          Width = 152
          Caption = 'Automatically adjust Height'
        end
      end
      inline frmWordWrap: TrmOPEBoolean
        Left = 8
        Top = 88
        Width = 119
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 4
        inherited chbProp: TCheckBox
          Width = 119
          Caption = 'Wrap text by Words'
        end
      end
      inline frmFixedWidth: TrmOPEBoolean
        Left = 8
        Top = 62
        Width = 83
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 5
        inherited chbProp: TCheckBox
          Width = 83
          Caption = 'Fixed Width'
        end
      end
    end
  end
end
