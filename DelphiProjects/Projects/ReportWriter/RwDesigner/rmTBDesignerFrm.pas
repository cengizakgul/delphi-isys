unit rmTBDesignerFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmLocalToolBarFrm, ActnList, ComCtrls, ToolWin, Menus, ImgList,
  ExtCtrls, rwDsgnUtils, rmTypes, rmDsgnTypes;

type
  TrmTBDesigner = class(TrmLocalToolBar)
    ToolBar3: TToolBar;
    tbtOpen: TToolButton;
    tbtNewReport: TToolButton;
    actOpenReport: TAction;
    actNewReport: TAction;
    miFile: TMenuItem;
    miOpenReport: TMenuItem;
    miNewReport: TMenuItem;
    N1: TMenuItem;
    miExit: TMenuItem;
    ToolBar1: TToolBar;
    actExit: TAction;
    actLibrary: TAction;
    OpenLibrary1: TMenuItem;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    N2: TMenuItem;
    procedure actOpenReportExecute(Sender: TObject);
    procedure actNewReportExecute(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure actLibraryExecute(Sender: TObject);
  private
  public
  end;

implementation

{$R *.dfm}

procedure TrmTBDesigner.actOpenReportExecute(Sender: TObject);
begin
  SendDesignerMessage(Self, mtbOpen, 0, 0);
end;

procedure TrmTBDesigner.actNewReportExecute(Sender: TObject);
begin
  SendDesignerMessage(Self, mtbNew, 0, 0);
end;

procedure TrmTBDesigner.actExitExecute(Sender: TObject);
begin
  SendDesignerMessage(Self, mtbClose, 0, 0);
end;

procedure TrmTBDesigner.actLibraryExecute(Sender: TObject);
begin
  SendDesignerMessage(Self, mtbOpenLibrary, 0, 0);
end;

end.
