unit rwStringsPropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwPropertyEditorFrm, StdCtrls, ExtCtrls, rwTypes;

type

  {TrwStringsPropertyEditor is class for edition TStrings property}

  TrwStringsPropertyEditor = class(TrwPropertyEditor)
    memStrings: TMemo;
    pnPos: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure memStringsClick(Sender: TObject);
    procedure memStringsKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  protected
    procedure SetReadOnly; override;
  end;

implementation

{$R *.DFM}

procedure TrwStringsPropertyEditor.FormCreate(Sender: TObject);
begin
  inherited;
  FValue := TrwStrings.Create;
end;

procedure TrwStringsPropertyEditor.memStringsClick(Sender: TObject);
begin
  pnPos.Caption := IntToStr(memStrings.CaretPos.Y+1)+' : '+
    IntToStr(memStrings.CaretPos.X+1);
end;

procedure TrwStringsPropertyEditor.memStringsKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  memStrings.OnClick(nil);

  if Key = VK_ESCAPE then
    btnCancel.Click;
end;


procedure TrwStringsPropertyEditor.FormDestroy(Sender: TObject);
begin
  FValue.Free;
  inherited;
end;

procedure TrwStringsPropertyEditor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if ModalResult = mrOk then
    FValue.Assign(memStrings.Lines);
  inherited;
end;

procedure TrwStringsPropertyEditor.FormShow(Sender: TObject);
begin
  memStrings.Lines.Assign(FValue);
  inherited;
end;

procedure TrwStringsPropertyEditor.SetReadOnly;
begin
  inherited;
  memStrings.ReadOnly := True;
end;

initialization
  RegisterClass(TrwStringsPropertyEditor);

end.
