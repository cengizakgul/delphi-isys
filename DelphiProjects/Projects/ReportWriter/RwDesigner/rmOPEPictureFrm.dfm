inherited rmOPEPicture: TrmOPEPicture
  Width = 180
  Height = 100
  object Panel1: TPanel [1]
    Left = 80
    Top = 0
    Width = 100
    Height = 100
    BevelOuter = bvLowered
    TabOrder = 1
    object imPicture: TImage
      Left = 1
      Top = 1
      Width = 98
      Height = 98
      Align = alClient
      Proportional = True
      Stretch = True
    end
  end
  inherited dlgOpenFile: TOpenDialog
    DefaultExt = '*.bmp'
    Title = 'Load Picture'
  end
end
