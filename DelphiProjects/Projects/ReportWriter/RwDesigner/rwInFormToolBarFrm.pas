unit rwInFormToolBarFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwCommonToolsBarFrm, ImgList, ComCtrls, StdCtrls, ToolWin, ExtCtrls, rwDsgnRes,
  Menus;

type

  {TrwInFormToolsBar is ToolBar for Input Form Designer}

  TrwInFormToolsBar = class(TrwCommonToolsBar)
    tbtText: TToolButton;
    tbtEdit: TToolButton;
    tbtDBComboBox: TToolButton;
    tbtCheckBox: TToolButton;
    tbtComboBox: TToolButton;
    tbtDateEdit: TToolButton;
    tbtGroupBox: TToolButton;
    tbtPanel: TToolButton;
    tbtRadioGroup: TToolButton;
    tbtGrid: TToolButton;
    tbtPageControl: TToolButton;
    ToolButton15: TToolButton;
    tbtButton: TToolButton;
    tlbFileDialog: TToolButton;
    tbtTabOrder: TToolButton;
    procedure tbtTabOrderClick(Sender: TObject);

  protected
    procedure InitFontList; override;
  end;

implementation

{$R *.DFM}

{ TrwInFormToolsBar }

procedure TrwInFormToolsBar.InitFontList;
begin
  inherited;
  cbFontName.Items.Assign(Screen.Fonts);
end;

procedure TrwInFormToolsBar.tbtTabOrderClick(Sender: TObject);
begin
  if Assigned(OnClickExpertToolBar) then
    OnClickExpertToolBar(Self, eteTabOrder);
end;

end.
