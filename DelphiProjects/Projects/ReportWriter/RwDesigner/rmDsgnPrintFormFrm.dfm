inherited rmDsgnPrintForm: TrmDsgnPrintForm
  Width = 401
  Height = 361
  object pmTrmTable: TPopupMenu
    AutoPopup = False
    OnPopup = pmTrmTablePopup
    Left = 75
    Top = 120
    object miClearTable: TMenuItem
      Caption = 'Clear from Cells'
      OnClick = miClearTableClick
    end
  end
  object pmTrmTableCell: TPopupMenu
    AutoPopup = False
    OnPopup = pmTrmTableCellPopup
    Left = 104
    Top = 120
    object N3: TMenuItem
      Caption = '-'
    end
    object miSplitCell: TMenuItem
      Caption = 'Split Cell...'
      OnClick = miSplitCellClick
    end
    object miMergeCells: TMenuItem
      Caption = 'Merge Cells'
      OnClick = miMergeCellsClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object miCreateSubTable: TMenuItem
      Caption = 'Create SubTable'
      OnClick = miCreateSubTableClick
    end
  end
  object pmTrmDBTable: TPopupMenu
    AutoPopup = False
    OnPopup = pmTrmDBTablePopup
    Left = 132
    Top = 121
    object miTableBands: TMenuItem
      Caption = 'Table Bands'
      object miHeaderBand: TMenuItem
        Caption = 'Table Header'
        OnClick = miHeaderBandClick
      end
      object miDetailBand: TMenuItem
        Caption = 'Detail'
        OnClick = miHeaderBandClick
      end
      object miTotalBand: TMenuItem
        Caption = 'Table Total'
        OnClick = miHeaderBandClick
      end
    end
    object miDestroySubTable: TMenuItem
      Caption = 'Destroy SubTable'
      OnClick = miDestroySubTableClick
    end
  end
end
