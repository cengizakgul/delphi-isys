unit rwLibraryFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, StdCtrls, mwCustomEdit, mwGeneralSyn,
  rwParser, Db, DBCtrls, Grids, DBGrids,
  rwCommonToolsBarFrm, rwReportFormToolsBarFrm, rwPageDesignerFrm,
  Menus, rwDesignAreaFrm, rwDesignPaperFrm, rwTypes, rwBasicClasses,
  rwDesignCompAreaFrm, rwCommonClasses, ActnList, rwCtrlResizer, Clipbrd,
  rwTypeCompFrm, rwReport, rmReport, TypInfo, rwMessages, rwUtils, rwDsgnUtils,
  rwDsgnRes, rwLibToolsBarFrm, rwPrinters, rwBasicToolsBarFrm, rwDesignClasses,
  ExtDlgs, rwEngineTypes, rwVirtualMachine, rmTypes,
  rwFunctParamDomainFrm, EvStreamUtils;

type

  {TrwLibrary form for editing library function}

  TrwDsgnLibrary = class(TForm)
    pgcPages: TPageControl;
    tsFunct: TTabSheet;
    Panel1: TPanel;
    pnlFunctList: TPanel;
    Label1: TLabel;
    Panel3: TPanel;
    Splitter1: TSplitter;
    tsComp: TTabSheet;
    Panel5: TPanel;
    CompDesigner: TrwPageDesigner;
    DesignCompArea: TrwDesignCompArea;
    aclDesignerActions: TActionList;
    actUndo: TAction;
    actDelSelection: TAction;
    actCopy: TAction;
    actCut: TAction;
    actPaste: TAction;
    actExportComp: TAction;
    actImportComp: TAction;
    actAddComp: TAction;
    actEditComp: TAction;
    actDelComp: TAction;
    actSaveComp: TAction;
    actCancelComp: TAction;
    pmComp: TPopupMenu;
    Add1: TMenuItem;
    Cancel1: TMenuItem;
    Copy1: TMenuItem;
    Cut1: TMenuItem;
    Delete1: TMenuItem;
    Delete2: TMenuItem;
    Edit1: TMenuItem;
    ExporttoFile1: TMenuItem;
    ImportfromFile1: TMenuItem;
    Paste1: TMenuItem;
    Save1: TMenuItem;
    Undo1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    Panel6: TPanel;
    Splitter2: TSplitter;
    N4: TMenuItem;
    pmCopyComp: TMenuItem;
    opdImport: TOpenDialog;
    svdExport: TSaveDialog;
    Label3: TLabel;
    pnlTree: TPanel;
    pcTrees: TPageControl;
    tsInheritance: TTabSheet;
    tsCategory: TTabSheet;
    tvLibComp: TTreeView;
    tvCategory: TTreeView;
    pmCategoryActions: TPopupMenu;
    actAddCategory: TAction;
    actDelCategory: TAction;
    AddCategory1: TMenuItem;
    DeleteCategory1: TMenuItem;
    Splitter3: TSplitter;
    Panel7: TPanel;
    pnlCompProp: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    edNameComp: TEdit;
    edClassIDComp: TEdit;
    memDescrComp: TMemo;
    tvFunct: TTreeView;
    pmFunctions: TPopupMenu;
    actAddFunctFolder: TAction;
    actDelFunctFolder: TAction;
    AddFolder1: TMenuItem;
    DeleteFolder1: TMenuItem;
    actAddFunc: TAction;
    actDelFunc: TAction;
    actEditFunc: TAction;
    actSaveFunc: TAction;
    actCancelFunc: TAction;
    Add2: TMenuItem;
    Delete3: TMenuItem;
    Edit2: TMenuItem;
    Save2: TMenuItem;
    Cancel2: TMenuItem;
    N5: TMenuItem;
    actCompile: TAction;
    N6: TMenuItem;
    Compile1: TMenuItem;
    Compile2: TMenuItem;
    actRefresh: TAction;
    RefreshLibrary1: TMenuItem;
    RefreshLibrary2: TMenuItem;
    actExpLibrary: TAction;
    actImpLibrary: TAction;
    ExportLibrary1: TMenuItem;
    ImportLibrary1: TMenuItem;
    N7: TMenuItem;
    ExportLibrary2: TMenuItem;
    ImportLibrary2: TMenuItem;
    opdImportLib: TOpenDialog;
    svdExportLib: TSaveDialog;
    Panel2: TPanel;
    btnAddComp: TButton;
    btnEditComp: TButton;
    btnDelComp: TButton;
    btnSaveComp: TButton;
    btnCancelComp: TButton;
    btnCompileComp: TButton;
    Panel4: TPanel;
    btnAdd: TButton;
    btnEdit: TButton;
    btnDel: TButton;
    btnSave: TButton;
    btnCancel: TButton;
    btnCompileFnct: TButton;
    lChangeComp: TLabel;
    lChangeFunct: TLabel;
    btnLoadPicture: TButton;
    opdPicture: TOpenPictureDialog;
    CompToolsBar: TrwLibToolsBar;
    chbShowInExprEditor: TCheckBox;
    chbDataFilter: TCheckBox;
    Splitter4: TSplitter;
    pcFunctProp: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    frmDomains: TrwFunctParamDomain;
    Label2: TLabel;
    edDescription: TEdit;
    Label6: TLabel;
    memNotes: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure actUndoExecute(Sender: TObject);
    procedure actDelSelectionExecute(Sender: TObject);
    procedure actCopyExecute(Sender: TObject);
    procedure actCutExecute(Sender: TObject);
    procedure actPasteExecute(Sender: TObject);
    procedure actAddCompExecute(Sender: TObject);
    procedure actEditCompExecute(Sender: TObject);
    procedure actDelCompExecute(Sender: TObject);
    procedure actSaveCompExecute(Sender: TObject);
    procedure actCancelCompExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure pmCopyCompClick(Sender: TObject);
    procedure actExportCompExecute(Sender: TObject);
    procedure actImportCompExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tvLibCompChange(Sender: TObject; Node: TTreeNode);
    procedure tvLibCompDblClick(Sender: TObject);
    procedure edNameCompExit(Sender: TObject);
    procedure actAddCategoryExecute(Sender: TObject);
    procedure tvCategoryExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure tvCategoryCollapsing(Sender: TObject; Node: TTreeNode;
      var AllowCollapse: Boolean);
    procedure tvCategoryCompare(Sender: TObject; Node1, Node2: TTreeNode;
      Data: Integer; var Compare: Integer);
    procedure tvCategoryEditing(Sender: TObject; Node: TTreeNode;
      var AllowEdit: Boolean);
    procedure actDelCategoryExecute(Sender: TObject);
    procedure tvCategoryDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure tvCategoryEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure tvLibCompEdited(Sender: TObject; Node: TTreeNode;
      var S: String);
    procedure tvFunctChange(Sender: TObject; Node: TTreeNode);
    procedure tvFunctDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure tvFunctEditing(Sender: TObject; Node: TTreeNode;
      var AllowEdit: Boolean);
    procedure tvFunctEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure actAddFunctFolderExecute(Sender: TObject);
    procedure actDelFunctFolderExecute(Sender: TObject);
    procedure actAddFuncExecute(Sender: TObject);
    procedure actEditFuncExecute(Sender: TObject);
    procedure actSaveFuncExecute(Sender: TObject);
    procedure actCancelFuncExecute(Sender: TObject);
    procedure actDelFuncExecute(Sender: TObject);
    procedure actRefreshExecute(Sender: TObject);
    procedure actCompileExecute(Sender: TObject);
    procedure actExpLibraryExecute(Sender: TObject);
    procedure actImpLibraryExecute(Sender: TObject);
    procedure tsCompShow(Sender: TObject);
    procedure tsCompHide(Sender: TObject);
    procedure btnLoadPictureClick(Sender: TObject);
    procedure chbShowInExprEditorClick(Sender: TObject);
    procedure chbDataFilterClick(Sender: TObject);
    procedure tsFunctShow(Sender: TObject);
    procedure edDescriptionExit(Sender: TObject);
    procedure memNotesExit(Sender: TObject);

  private
    FflCompAdd: Boolean;
    FflFuncAdd: Boolean;
    FSecurityRec: TrwDesignerSecurityRec;
    FChangeReason: String;

    procedure ClickComponentsToolBar(Sender: TObject; AComponentClassName: string; AMultiCreation: Boolean);
    procedure ClickActionToolBar(Sender: TObject; AAction: TrwActionToolBarEvent);
    procedure ClickExpertToolBar(Sender: TObject; AAction: TrwExpertToolBarEvent; AParam: TObject = nil);
    procedure ChangesOnFontToolBar(Sender: TObject; EventType: TrwFontToolBarEvent);
    procedure ChangesOnTextToolBar(Sender: TObject; AText: string);
    procedure ChangedComponentProperty(Sender: TObject; AComponent: TrwComponent);
    procedure ChangeSelectComponent(Sender: TObject; AComponent: TrwComponent);
    procedure OnPaperMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ComponentSelection(Sender: TObject; AComponent: TrwComponent; GroupSelection: Boolean);
    procedure AfterCreateNewComponent(Sender: TObject; AComponent: TrwComponent; AParent: TrwControl; X, Y: Integer);
    procedure BeforeDestroyComponent(Sender: TObject; AComponent: TrwComponent);
    procedure DemandImageComponent(Sender: TObject; AClassName: string; AImage: TBitmap);
    procedure RefreshReportPropertyEditor(Sender: TObject; AComponent: TrwComponent);
    procedure SyncDesignByComponent;
    procedure SetCompButtons(EditMode: Boolean);
    procedure SetFunctButtons(EditMode: Boolean);
    procedure DeleteComponent;
    procedure ShowDefProperty(AObject: TObject; APropName: string);
    procedure CreateFunctTree;
    procedure CreateCompInheritTree;
    procedure CreateCompCategoryTree;
    function  AddComponentToTree(AComp: TrwLibComponent): TTreeNode;
    function  AddComponentToCategoryTree(AComp: TrwLibComponent; AImageIndex: Integer): TTreeNode;
    function  AddFunctToTree(AFunct: TrwLibFunction; AImageIndex: Integer): TTreeNode;
    procedure ShowFunctError(const AFunctName: string; const ALine, ACol: Integer; const AError: string);
    procedure ShowCompError(const ALibComponentName, AComponentName, AEventName: string;
        const ALine, ACol: Integer; const AError: string);
    procedure RefreshLibrary(AsOfDate: TDateTime);
    procedure AssignChangingInfo(ALibItem: TrwLibCollectionItem);
    procedure ShowChangingInfo(ALibItem: TrwLibCollectionItem; ALabel: TLabel);
    procedure ExitFromControl;    

  public
    EventText: TrwEventTextEdit;
  end;

procedure ShowLibrary(AEventName: string = ''; ALine: Integer = 0; ACol: Integer = 0;
                      ALibComponentName: string = ''; AComponentName: string = ''; AError: String = '');

implementation

{$R *.DFM}

uses rwEngine, rwDesigner, rwDesignerFrm, rwLocalVarFunctFrm;

procedure ShowLibrary(AEventName: string = ''; ALine: Integer = 0; ACol: Integer = 0;
   ALibComponentName: string = ''; AComponentName: string = ''; AError: String = '');
var
  Frm: TrwDsgnLibrary;
begin
  Frm := TrwDsgnLibrary.Create(Application);
  with Frm do
  try
    if AEventName <> '' then
      if ALibComponentName = '' then
        ShowFunctError(AEventName, ALine, ACol, AError)
      else
        ShowCompError(ALibComponentName, AComponentName, AEventName, ALine, ACol, AError);
    ShowModal;
  finally
    Free;
  end;
end;

procedure TrwDsgnLibrary.FormCreate(Sender: TObject);
begin
  EventText := TrwEventTextEdit.Create(Self);
  EventText.Align := alClient;
  EventText.Parent := Panel3;
  EventText.HighLighter := GeneralSyn;
  EventText.EventOwner := nil;
  DsgnRes.InitEditor(EventText);

  DesignCompArea.Top := 0;
  DesignCompArea.Left := 0;
  DesignCompArea.Width := 4000;
  DesignCompArea.Height := 4000;
  DesignCompArea.pnlArea.OnMouseMove := OnPaperMouseMove;
  DesignCompArea.OnSelectComponent := ComponentSelection;
  DesignCompArea.OnCreateNewComponent := AfterCreateNewComponent;
  DesignCompArea.OnDestroyComponent := BeforeDestroyComponent;
  DesignCompArea.OnDemandImageComponent := DemandImageComponent;
  DesignCompArea.OnSetComponentProperty := RefreshReportPropertyEditor;
  DesignCompArea.OnShowDefProperty := ShowDefProperty;
  DesignCompArea.ClearSelection;

  DesignCompArea.Parent := CompDesigner.sbxDsgnFrame;

  ShowChangingInfo(nil, lChangeFunct);
  ShowChangingInfo(nil, lChangeComp);

  CompToolsBar.OnClickComponentsToolBar := ClickComponentsToolBar;
  CompToolsBar.OnClickActionToolBar := ClickActionToolBar;
  CompToolsBar.OnClickExpertToolBar := ClickExpertToolBar;
  CompToolsBar.OnChangesFontToolBar := ChangesOnFontToolBar;
  CompToolsBar.OnChangesTextToolBar := ChangesOnTextToolBar;
  CompDesigner.OnChangePropertySelectComponent := ChangedComponentProperty;
  CompDesigner.OnChangeSelectComponent := ChangeSelectComponent;

  FSecurityRec := RWDesignerExt.AppAdapter.GetSecurityDescriptor;

  if FSecurityRec.LibComponents.Modify then
    tvCategory.DragMode := dmAutomatic;

  if FSecurityRec.LibFunctions.Modify then
    tvFunct.DragMode := dmAutomatic;

  actExpLibrary.Enabled := FSecurityRec.LibComponents.Modify and FSecurityRec.LibFunctions.Modify;
  actImpLibrary.Enabled := actExpLibrary.Enabled;

  CreateCompInheritTree;
  CreateCompCategoryTree;
  CreateFunctTree;
  SetFunctButtons(False);
  SetCompButtons(False);
end;

procedure TrwDsgnLibrary.FormDestroy(Sender: TObject);
begin
  EventText.Free;
end;

procedure TrwDsgnLibrary.ClickComponentsToolBar(Sender: TObject; AComponentClassName: string; AMultiCreation: Boolean);
begin
  DesignCompArea.SetNewComponentClassName(AComponentClassName, AMultiCreation);
end;

procedure TrwDsgnLibrary.ClickActionToolBar(Sender: TObject; AAction: TrwActionToolBarEvent);
begin
  DesignCompArea.ActionForSelection(AAction);
end;

procedure TrwDsgnLibrary.ClickExpertToolBar(Sender: TObject; AAction: TrwExpertToolBarEvent; AParam: TObject = nil);
begin
  DesignCompArea.ExpertForSelection(AAction);
end;

procedure TrwDsgnLibrary.ChangesOnFontToolBar(Sender: TObject; EventType: TrwFontToolBarEvent);
begin
  CompDesigner.ChangeFontStatus(DesignCompArea, CompToolsBar, EventType);
end;

procedure TrwDsgnLibrary.ChangesOnTextToolBar(Sender: TObject; AText: string);
begin
  DesignCompArea.SetPropertyForSelection(['Text'], AText);
  TrwCommonToolsBar(Sender).edtText.SetFocus;
end;

procedure TrwDsgnLibrary.ChangedComponentProperty(Sender: TObject; AComponent: TrwComponent);
begin
  CompToolsBar.SetToolBarByComponentProperties(AComponent, False);
  DesignCompArea.RefreshCompResizer;
  CompDesigner.RefreshComponentProperties;
end;

procedure TrwDsgnLibrary.ChangeSelectComponent(Sender: TObject; AComponent: TrwComponent);
begin
  if AComponent <> DesignCompArea.Component then
    DesignCompArea.AddSelection(AComponent, False);
end;

procedure TrwDsgnLibrary.OnPaperMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  CompDesigner.ShowPositionOnRuler(X, Y);
end;

procedure TrwDsgnLibrary.ComponentSelection(Sender: TObject; AComponent: TrwComponent; GroupSelection: Boolean);
begin
  CompToolsBar.SetToolBarByComponentProperties(AComponent, GroupSelection);
  CompDesigner.ShowComponentProperties(AComponent, GroupSelection);
end;

procedure TrwDsgnLibrary.AfterCreateNewComponent(Sender: TObject; AComponent: TrwComponent; AParent: TrwControl; X, Y: Integer);
begin
  CompDesigner.AddNewComponentToInspector(AComponent, CompToolsBar);
end;

procedure TrwDsgnLibrary.BeforeDestroyComponent(Sender: TObject; AComponent: TrwComponent);
begin
  CompDesigner.DeleteComponentFromInspector(AComponent)
end;

procedure TrwDsgnLibrary.DemandImageComponent(Sender: TObject; AClassName: string; AImage: TBitmap);
begin
  CompToolsBar.AssignImageForComponent(AClassName, AImage);
end;

procedure TrwDsgnLibrary.RefreshReportPropertyEditor(Sender: TObject; AComponent: TrwComponent);
begin
  DesignCompArea.RefreshCompResizer;
  CompDesigner.RefreshComponentProperties;
end;


procedure TrwDsgnLibrary.SyncDesignByComponent;
var
  i: Integer;
begin
  if DesignCompArea.Component.InheritsFrom(TrwFormControl) then
    CompToolsBar.cbFontName.Items.Assign(Screen.Fonts)
  else
    CompToolsBar.cbFontName.Items.Assign(rwPrinter.Fonts);

  if not DesignCompArea.Component.InheritsFrom(TrwNoVisualComponent) then
    DesignCompArea.PrepDsgnComp(TrwComponent(DesignCompArea.Component));

  for i := 0 to DesignCompArea.Component.ComponentCount - 1 do
  begin
    if not DesignCompArea.Component.Components[i].InheritsFrom(TrwComponent) or
       (DesignCompArea.Component.Components[i] is TrwGlobalVarFunct) then
      Continue;
    if DesignCompArea.Component.Components[i].InheritsFrom(TrwNoVisualComponent) then
      DesignCompArea.InitNoVisualComponent(TrwNoVisualComponent(DesignCompArea.Component.Components[i]));

    DesignCompArea.PrepDsgnComp(TrwComponent(DesignCompArea.Component.Components[i]));
  end;

  CompDesigner.SyncComponentsList(DesignCompArea.Component, CompToolsBar);
  CompDesigner.ShowComponentProperties(DesignCompArea.Component);

  opdPicture.FileName := '';
end;


procedure TrwDsgnLibrary.actUndoExecute(Sender: TObject);
begin
  DesignCompArea.UnDo;
end;

procedure TrwDsgnLibrary.actDelSelectionExecute(Sender: TObject);
begin
  if (ActiveControl is TrwControlResizer) then
    DesignCompArea.DestroySelectedComponents;
end;

procedure TrwDsgnLibrary.actCopyExecute(Sender: TObject);
begin
  if not (ActiveControl is TrwControlResizer) then
    ActiveControl.Perform(WM_COPY, 0, 0)
  else
    DesignCompArea.Copy;
end;

procedure TrwDsgnLibrary.actCutExecute(Sender: TObject);
begin
  if not (ActiveControl is TrwControlResizer) then
    ActiveControl.Perform(WM_CUT, 0, 0)
  else
    DesignCompArea.Cut;
end;

procedure TrwDsgnLibrary.actPasteExecute(Sender: TObject);
begin
  if not (ActiveControl is TrwControlResizer) and (ActiveControl <> nil) then
    ActiveControl.Perform(WM_PASTE, 0, 0)
  else if Clipboard.HasFormat(CF_RWDATA) then
    DesignCompArea.Paste;
end;

procedure TrwDsgnLibrary.actAddCompExecute(Sender: TObject);
var
  CompClass: TObject;
begin
  if pcTrees.ActivePage = tsCategory then
  begin
    actAddCategory.Execute;
    SystemLibComponents.Modified := True;
  end
  
  else
  begin
    CompClass := ChoiceComponentType(CompToolsBar.tlbComponents);
    if not Assigned(CompClass) then
      Exit;

    FflCompAdd := True;
    edNameComp.Text := 'New Component';
    edClassIDComp.Text := 'New Class';
    if CompClass.ClassType = TrwLibComponent then
      DesignCompArea.Component := TrwComponent(TrwLibComponent(CompClass).CreateComp(nil, True))
    else
    begin
      DesignCompArea.Component := TrwComponentClass(CompClass).Create(nil);
      DesignCompArea.Component._LibComponent := DesignCompArea.Component.ClassName;

      if not Assigned(DesignCompArea.Component.GlobalVarFunc) then
        DesignCompArea.Component.InitGlobalVarFunc;
      DesignCompArea.Component.FixUpUserProperties;
    end;

    DesignCompArea.Component.Name := cParserLibComp;
    DesignCompArea.Component.InitializeForDesign;
    DesignCompArea.Component.LibDesignMode := True;
    SyncDesignByComponent;
    SetCompButtons(True);
  end;
end;

procedure TrwDsgnLibrary.SetCompButtons(EditMode: Boolean);
var
  t, i: Integer;
begin
  actAddComp.Enabled := not EditMode and FSecurityRec.LibComponents.Modify;
  actEditComp.Enabled := not EditMode;
  actDelComp.Enabled := not EditMode and FSecurityRec.LibComponents.Modify;
  pmCopyComp.Enabled := not EditMode and FSecurityRec.LibComponents.Modify;
  actSaveComp.Enabled := EditMode and FSecurityRec.LibComponents.Modify;
  actCancelComp.Enabled := EditMode;
  CompDesigner.Enabled := EditMode;
  CompToolsBar.Enabled := EditMode;
  actExportComp.Enabled := EditMode;
  actImportComp.Enabled := EditMode;
//  actCompile.Enabled := not EditMode and FEditLibrary;
  pnlCompProp.Enabled := EditMode;
  pnlTree.Enabled := not EditMode;

  if DesignCompArea.Component is TrwPrintableControl then
    t := 1
  else if DesignCompArea.Component is TrwFormControl then
    t := 2
  else
    t := 3;

  for i := 0 to CompToolsBar.tlbComponents.ButtonCount - 1 do
    if (CompToolsBar.tlbComponents.Buttons[i].Tag = t) or
       (CompToolsBar.tlbComponents.Buttons[i].Tag = -1) then
      CompToolsBar.tlbComponents.Buttons[i].Visible := True
    else if CompToolsBar.tlbComponents.Buttons[i].Tag <> 0 then
      CompToolsBar.tlbComponents.Buttons[i].Visible := False
end;


procedure TrwDsgnLibrary.SetFunctButtons(EditMode: Boolean);
begin
  actAddFunc.Enabled := not EditMode and FSecurityRec.LibFunctions.Modify;
  actEditFunc.Enabled := not EditMode and FSecurityRec.LibFunctions.Modify;
  actDelFunc.Enabled := not EditMode and FSecurityRec.LibFunctions.Modify;
  actSaveFunc.Enabled := EditMode and FSecurityRec.LibFunctions.Modify;
  actCancelFunc.Enabled := EditMode;
//  actCompile.Enabled := not EditMode and FEditLibrary;
  EventText.ReadOnly := not EditMode;
  actAddFunctFolder.Enabled := FSecurityRec.LibFunctions.Modify;
  actDelFunctFolder.Enabled := FSecurityRec.LibFunctions.Modify;
  tvFunct.ReadOnly := not FSecurityRec.LibFunctions.Modify;
  pnlFunctList.Enabled := not EditMode;
end;


procedure TrwDsgnLibrary.actEditCompExecute(Sender: TObject);
var
  i: Integer;
begin
  if (pcTrees.ActivePage = tsCategory) then
  begin
    if Assigned(tvCategory.Selected) and (TObject(tvCategory.Selected.Data) is TrwLibComponent) then
    begin
      for i := 0 to tvLibComp.Items.Count - 1 do
        if tvLibComp.Items[i].Data = tvCategory.Selected.Data then
        begin
          tvLibComp.Selected := tvLibComp.Items[i];
          break;
        end
     end
     else
       Exit;
  end;

  if not (Assigned(tvLibComp.Selected) and (TObject(tvLibComp.Selected.Data) is TrwLibComponent)) then
    Exit;

  FflCompAdd := False;
  i := SystemLibComponents.IndexOf(edClassIDComp.Text);
  DesignCompArea.Component := TrwComponent(SystemLibComponents[i].CreateComp);
  if not ((DesignCompArea.Component is TrwCustomReport) or (DesignCompArea.Component is TrmReport)) then
  begin
    DesignCompArea.Component.LibDesignMode := True;
    DesignCompArea.Component.Name := cParserLibComp;
    if not Assigned(DesignCompArea.Component.GlobalVarFunc) then
      DesignCompArea.Component.InitGlobalVarFunc;
    SyncDesignByComponent;
  end;
  SetCompButtons(True);
end;

procedure TrwDsgnLibrary.actDelCompExecute(Sender: TObject);
var
  i: Integer;
begin
  if pcTrees.ActivePage = tsCategory then
    actDelCategory.Execute
  else
  begin
    if not (Assigned(tvLibComp.Selected) and (TObject(tvLibComp.Selected.Data) is TrwLibComponent)) then
      Exit;

    if (MessageDlg('Do you want to delete the component "'+edNameComp.Text+'"', mtConfirmation, [mbYes, mbNo], 0) <> mrYes) then
      Exit;

    TrwLibComponent(tvLibComp.Selected.Data).Free;

    for i := 0 to tvCategory.Items.Count -1 do
      if tvCategory.Items[i].Data = tvLibComp.Selected.Data then
      begin
        tvCategory.Items[i].Free;
        break;
      end;
    tvLibComp.Selected.Free;
  end;
  SystemLibComponents.Modified := True;
end;

procedure TrwDsgnLibrary.actSaveCompExecute(Sender: TObject);
var
  Comp: TrwLibComponent;
  i: Integer;
  N: TTreeNode;
  F: TEvFileStream;
begin
  if not FflCompAdd then
    Comp := TrwLibComponent(tvLibComp.Selected.Data)
  else
  begin
    if SystemLibComponents.IndexOf(edClassIDComp.Text) <> -1 then
      raise ErwException.CreateFmt(ResErrLibCompExists, [edClassIDComp.Text]);
    Comp := SystemLibComponents.Add;
  end;

  Comp.Name := edClassIDComp.Text;
  Comp.DisplayName := edNameComp.Text;
  Comp.ParentName := DesignCompArea.Component._LibComponent;
  Comp.Description := memDescrComp.Text;

  if opdPicture.FileName <> '' then
  begin
    F := TEvFileStream.Create(opdPicture.FileName, fmOpenRead);
    try
      (Comp as IrmComponent).SetPictureData(F);
    finally
      FreeAndNil(F);
      opdPicture.FileName := '';
    end;
  end;

  DesignCompArea.Component.Name := '';
  Comp.SaveComp(DesignCompArea.Component);
  AssignChangingInfo(Comp);
  ShowChangingInfo(Comp, lChangeComp);
  SystemLibComponents.Modified := True;
  if FflCompAdd then
  begin
    N := AddComponentToTree(Comp);
    if tvCategory.Items.Count > 0 then
      AddComponentToCategoryTree(Comp, N.ImageIndex);
  end
  else
  begin
    tvLibComp.Selected.Text := Comp.DisplayName;
    for i := 0 to tvCategory.Items.Count -1 do
      if tvCategory.Items[i].Data = Comp then
      begin
        tvCategory.Items[i].Text := Comp.DisplayName;
        break;
      end;
  end;

  DeleteComponent;
  SetCompButtons(False);
end;

procedure TrwDsgnLibrary.actCancelCompExecute(Sender: TObject);
begin
  DeleteComponent;
  SetCompButtons(False);
end;

procedure TrwDsgnLibrary.FormShow(Sender: TObject);
begin
  CompToolsBar.RestoreState;  
end;

procedure TrwDsgnLibrary.pmCopyCompClick(Sender: TObject);
var
  Comp: TrwLibComponent;
begin
  if not (Assigned(tvLibComp.Selected) and (TObject(tvLibComp.Selected.Data) is TrwLibComponent)) then
    Exit;

  Comp := SystemLibComponents.Add;
  Comp.Assign(TrwLibComponent(tvLibComp.Selected.Data));
  AddComponentToTree(Comp);
  AssignChangingInfo(Comp);
  SystemLibComponents.Modified := True;
end;

procedure TrwDsgnLibrary.actExportCompExecute(Sender: TObject);
begin
  if not Assigned(DesignCompArea.Component) then Exit;
  if not svdExport.Execute then Exit;
  SaveRWResToFile(DesignCompArea.Component, svdExport.FileName);
end;

procedure TrwDsgnLibrary.actImportCompExecute(Sender: TObject);
var
  CompClass: TrwComponentClass;
  C: TrwComponent;
begin
  if not opdImport.Execute then Exit;
  DeleteComponent;
  C := TrwComponent(LoadRWResFromFile(nil, opdImport.FileName));
  if C is TrwReport then
  begin
    CompClass := TrwComponentClass(C.ClassType);
    C.Free;
    C := CompClass.Create(nil);
    TrwReport(C).LoadFromFile(opdImport.FileName);
    if not C.IsLibComponent then
      C._LibComponent := CompClass.ClassName;
    DesignCompArea.Component := C;
  end

  else if C is TrmReport then
  begin
    CompClass := TrwComponentClass(C.ClassType);
    C.Free;
    C := CompClass.Create(nil);
    TrmReport(C).LoadFromFile(opdImport.FileName);
    if not C.IsLibComponent then
      C._LibComponent := CompClass.ClassName;
    DesignCompArea.Component := C;
  end

  else
  begin
    DesignCompArea.Component := C;
    DesignCompArea.Component.LibDesignMode := True;
    DesignCompArea.Component.Name := cParserLibComp;
    if not Assigned(DesignCompArea.Component.GlobalVarFunc) then
      DesignCompArea.Component.InitGlobalVarFunc;
    SyncDesignByComponent;
  end;

  SetCompButtons(True);
end;

procedure TrwDsgnLibrary.DeleteComponent;
var
  Comp: TrwComponent;
begin
  DesignCompArea.ClearSelection;
  Comp := DesignCompArea.Component;
  DesignCompArea.Component := nil;
  Comp.Free;
  CompDesigner.ShowComponentProperties(nil, False);
  CompDesigner.SyncComponentsList(nil, CompToolsBar);
end;

procedure TrwDsgnLibrary.ShowDefProperty(AObject: TObject; APropName: string);
begin
  CompDesigner.ShowDefProperty(TrwComponent(AObject), APropName);
end;


procedure TrwDsgnLibrary.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ActiveControl := nil;
  tvFunct.Selected := nil;

  if FSecurityRec.LibComponents.Modify or FSecurityRec.LibFunctions.Modify then
    if (SystemLibComponents.Modified or SystemLibFunctions.Modified) and
       (MessageDlg('RW Library has been changed. Do you want to save changes to the database right now?',
          mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin
      if SystemLibComponents.Modified and FSecurityRec.LibComponents.Modify then
      begin
        UpdateCollectionGroupInfo(SystemLibComponents, tvCategory, tvCategory.Items[0]);
        SystemLibComponents.Store;
      end;

      if SystemLibFunctions.Modified and FSecurityRec.LibFunctions.Modify then
      begin
        UpdateCollectionGroupInfo(SystemLibFunctions, tvFunct, tvFunct.Items[0]);
        SystemLibFunctions.Store;
      end;
    end

    else
    begin
      UpdateCollectionGroupInfo(SystemLibComponents, tvCategory, tvCategory.Items[0]);
      UpdateCollectionGroupInfo(SystemLibFunctions, tvFunct, tvFunct.Items[0]);
    end;

  CompToolsBar.SaveState;
  CompToolsBar.UndockedBarsVisible(False);
end;


procedure TrwDsgnLibrary.CreateCompInheritTree;
var
  N: TTreeNode;
  i: Integer;
begin
  tvLibComp.Items.BeginUpdate;
  tvLibComp.Items.Clear;

  N := tvLibComp.Items.AddChildObject(nil, 'Component Library', nil);
  N.ImageIndex := 46;
  N.SelectedIndex := N.ImageIndex;
  N.StateIndex := N.ImageIndex;

  for i := 0 to SystemLibComponents.Count -1 do
    AddComponentToTree(SystemLibComponents[i]);

  tvLibComp.SortType := stNone;
  tvLibComp.SortType := stText;
  tvLibComp.Items[0].Expand(False);

  tvLibComp.Items.EndUpdate;
end;


function TrwDsgnLibrary.AddComponentToTree(AComp: TrwLibComponent): TTreeNode;
var
  N: TTreeNode;
  i: Integer;
begin
  Result := nil;
  N := nil;
  if Assigned(GetClass(AComp.ParentName)) then
  begin
    for i := 0 to tvLibComp.Items[0].Count-1 do
      if TToolButton(tvLibComp.Items[0].Item[i].Data).Caption = AComp.ParentName then
      begin
        N := tvLibComp.Items[0].Item[i];
        break;
      end;

    if not Assigned(N) then
      for i := 0 to CompToolsBar.tlbComponents.ButtonCount - 1 do
        if CompToolsBar.tlbComponents.Buttons[i].Caption = AComp.ParentName then
        begin
          N := tvLibComp.Items.AddChildObject(tvLibComp.Items[0],
            CompToolsBar.tlbComponents.Buttons[i].Hint, CompToolsBar.tlbComponents.Buttons[i]);
          N.ImageIndex := CompToolsBar.tlbComponents.Buttons[i].ImageIndex;
          N.SelectedIndex := N.ImageIndex;
          N.StateIndex := N.ImageIndex;
          break;
        end;
  end

  else
  begin
    for i := 0 to tvLibComp.Items.Count - 1 do
      if (TObject(tvLibComp.Items[i].Data) is TrwLibComponent) and
         (AComp.ParentName = TrwLibComponent(tvLibComp.Items[i].Data).Name)  then
      begin
        N := tvLibComp.Items[i];
        break;
      end;
  end;

  if Assigned(N) then
  begin
    Result := tvLibComp.Items.AddChildObject(N, AComp.DisplayName, AComp);
    Result.ImageIndex := N.ImageIndex;
    Result.SelectedIndex := N.ImageIndex;
    Result.StateIndex := N.ImageIndex;
  end;
end;



procedure TrwDsgnLibrary.tvLibCompChange(Sender: TObject; Node: TTreeNode);
begin
  if Assigned(Node) and (TObject(Node.Data) is TrwLibComponent) then
  begin
    ShowChangingInfo(TrwLibComponent(Node.Data), lChangeComp);
    edNameComp.Text := TrwLibComponent(Node.Data).DisplayName;
    edClassIDComp.Text := TrwLibComponent(Node.Data).Name;
    memDescrComp.Text := TrwLibComponent(Node.Data).Description;;
  end
  else
  begin
    ShowChangingInfo(nil, lChangeComp);
    edNameComp.Text := '';
    edClassIDComp.Text := '';
    memDescrComp.Text := '';
  end;
end;

procedure TrwDsgnLibrary.tvLibCompDblClick(Sender: TObject);
begin
  if Assigned(tvLibComp.Selected) and (TObject(tvLibComp.Selected.Data) is TrwLibComponent) then
    actEditComp.Execute;
end;

procedure TrwDsgnLibrary.edNameCompExit(Sender: TObject);
begin
  TEdit(Sender).Text := Trim(TEdit(Sender).Text);
end;



var Frm: TrwDsgnLibrary;

procedure OnAddCategoryNode(ANewNode: TTreeNode; var Accept: Boolean);
var
  lc: TrwLibComponent;
  C: TrwComponentClass;
  i: Integer;
begin
  if not Assigned(ANewNode.Data) then
    ANewNode.ImageIndex := 57

  else
  begin
    ANewNode.Data := TrwLibCollectionItem(IrmLibCollectionItem(ANewNode.Data).RealObject);
    ANewNode.ImageIndex := -1;
    lc := TrwLibComponent(ANewNode.Data);
    ANewNode.Text := lc.DisplayName;
    C := lc.GetBaseClass;
    for i := 0 to Frm.CompToolsBar.tlbComponents.ButtonCount - 1 do
      if Frm.CompToolsBar.tlbComponents.Buttons[i].Caption = C.ClassName then
      begin
        ANewNode.ImageIndex := Frm.CompToolsBar.tlbComponents.Buttons[i].ImageIndex;
        break;
      end;
  end;

  ANewNode.SelectedIndex := ANewNode.ImageIndex;
  ANewNode.StateIndex := ANewNode.ImageIndex;
end;

procedure TrwDsgnLibrary.CreateCompCategoryTree;
var
  N: TTreeNode;
begin
  Frm := Self;

  tvCategory.Items.BeginUpdate;
  tvCategory.Items.Clear;
  tvCategory.SortType := stNone;

  N := tvCategory.Items.AddChildObject(nil, 'Categories', nil);
  N.ImageIndex := 35;
  N.SelectedIndex := N.ImageIndex;
  N.StateIndex := N.ImageIndex;

  FillUpTreeViewCollection(SystemLibComponents, tvCategory, N, @OnAddCategoryNode);

  tvCategory.SortType := stText;
  tvCategory.Items.EndUpdate;
end;

procedure OnAddFunctNode(ANewNode: TTreeNode; var Accept: Boolean);
begin
  if not Assigned(ANewNode.Data) then
    ANewNode.ImageIndex := 57
  else
  begin
    ANewNode.Data := TrwLibCollectionItem(IrmLibCollectionItem(ANewNode.Data).RealObject);
    ANewNode.ImageIndex := 55;
  end;

  ANewNode.SelectedIndex := ANewNode.ImageIndex;
  ANewNode.StateIndex := ANewNode.ImageIndex;
end;

procedure TrwDsgnLibrary.CreateFunctTree;
var
  N: TTreeNode;
begin
  tvFunct.Items.BeginUpdate;
  tvFunct.Items.Clear;
  tvFunct.SortType := stNone;

  N := tvFunct.Items.AddChildObject(nil, 'Procedures and Functions', nil);
  N.ImageIndex := 46;
  N.SelectedIndex := N.ImageIndex;
  N.StateIndex := N.ImageIndex;

  FillUpTreeViewCollection(SystemLibFunctions, tvFunct, N, @OnAddFunctNode);

  tvFunct.SortType := stText;
  tvFunct.Items.EndUpdate;
end;

procedure TrwDsgnLibrary.actAddCategoryExecute(Sender: TObject);
var
  N: TTreeNode;
begin
  if not Assigned(tvCategory.Selected) or Assigned(tvCategory.Selected.Data) then
    Exit;

  N := tvCategory.Items.AddChildObject(tvCategory.Selected, 'New Category', nil);
  N.ImageIndex := 57;
  N.SelectedIndex := N.ImageIndex;
  N.StateIndex := N.ImageIndex;
  tvCategory.SortType := stNone;
  tvCategory.SortType := stText;
  tvCategory.Selected := N;

  SystemLibComponents.Modified := True;
end;

procedure TrwDsgnLibrary.tvCategoryExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.ImageIndex = 56 then
    Node.ImageIndex := 57;
end;

procedure TrwDsgnLibrary.tvCategoryCollapsing(Sender: TObject;
  Node: TTreeNode; var AllowCollapse: Boolean);
begin
  if Node.ImageIndex = 57 then
    Node.ImageIndex := 56;
end;

procedure TrwDsgnLibrary.tvCategoryCompare(Sender: TObject; Node1,
  Node2: TTreeNode; Data: Integer; var Compare: Integer);
begin
  if not Assigned(Node1.Data) and Assigned(Node2.Data) then
    Compare := -1
  else if Assigned(Node1.Data) and not Assigned(Node2.Data) then
    Compare := 1
  else
    Compare := CompareStr(Node1.Text, Node2.Text);
end;

procedure TrwDsgnLibrary.tvCategoryEditing(Sender: TObject;
  Node: TTreeNode; var AllowEdit: Boolean);
begin
  AllowEdit := not Assigned(Node.Data) and (Node <> tvCategory.Items[0]) and FSecurityRec.LibComponents.Modify;
end;

procedure TrwDsgnLibrary.actDelCategoryExecute(Sender: TObject);

  procedure DeleteNode(ANode: TTreeNode);
  var
    i: Integer;
  begin
    if Assigned(ANode.Data) then
      ANode.MoveTo(tvCategory.Items[0], naAddChild)
    else
    begin
      for i := 0 to ANode.Count - 1 do
        if Assigned(ANode.Item[0].Data) then
          ANode.Item[0].MoveTo(tvCategory.Items[0], naAddChild)
        else
          DeleteNode(ANode.Item[0]);
      ANode.Free;
    end;
  end;

begin
  if not Assigned(tvCategory.Selected) then
    Exit;

  tvCategory.Items.BeginUpdate;
  DeleteNode(tvCategory.Selected);
  tvCategory.SortType := stNone;
  tvCategory.SortType := stText;
  tvCategory.Items.EndUpdate;

  SystemLibComponents.Modified := True;
end;

procedure TrwDsgnLibrary.tvCategoryDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := True;
end;

procedure TrwDsgnLibrary.tvCategoryEndDrag(Sender, Target: TObject; X,
  Y: Integer);
var
  N: TTreeNode;
begin
  N := tvCategory.GetNodeAt(X, Y);
  if Assigned(N) then
  begin
    if Assigned(N.Data) then
      tvCategory.Selected.MoveTo(N, naInsert)
    else
      tvCategory.Selected.MoveTo(N, naAddChild);
    SystemLibComponents.Modified := True;
  end;
end;

function TrwDsgnLibrary.AddComponentToCategoryTree(AComp: TrwLibComponent; AImageIndex: Integer): TTreeNode;
begin
  Result := tvCategory.Items.AddChildObject(tvCategory.Items[0], AComp.DisplayName, AComp);
  Result.ImageIndex := AImageIndex;
  Result.SelectedIndex := Result.ImageIndex;
  Result.StateIndex := Result.ImageIndex;
end;


function TrwDsgnLibrary.AddFunctToTree(AFunct: TrwLibFunction;
  AImageIndex: Integer): TTreeNode;
begin
  Result := tvFunct.Items.AddChildObject(tvFunct.Items[0], AFunct.Name, AFunct);
  Result.ImageIndex := AImageIndex;
  Result.SelectedIndex := Result.ImageIndex;
  Result.StateIndex := Result.ImageIndex;
end;


procedure TrwDsgnLibrary.tvLibCompEdited(Sender: TObject; Node: TTreeNode;
  var S: String);
begin
  SystemLibComponents.Modified := True;
end;



procedure TrwDsgnLibrary.tvFunctChange(Sender: TObject; Node: TTreeNode);
begin
  if Assigned(Node) and (TObject(Node.Data) is TrwLibFunction) then
  begin
    ShowChangingInfo(TrwLibFunction(Node.Data), lChangeFunct);
    EventText.Text := TrwLibFunction(Node.Data).Text;
    chbShowInExprEditor.Checked := fdtShowInEditor in TrwLibFunction(Node.Data).DesignTimeInfo;
    chbDataFilter.Checked := fdtDataFilter in TrwLibFunction(Node.Data).DesignTimeInfo;
    edDescription.Text := TrwLibFunction(Node.Data).DisplayName;
    memNotes.Text := TrwLibFunction(Node.Data).Description;
    frmDomains.RWFunction := TrwLibFunction(Node.Data);
    pcFunctProp.Enabled := True;
  end
  else
  begin
    ShowChangingInfo(nil, lChangeFunct);
    EventText.Text := '';
    chbShowInExprEditor.Checked := False;
    chbDataFilter.Checked := False;
    memNotes.Text := '';
    edDescription.Text := '';
    frmDomains.RWFunction := nil;
    pcFunctProp.Enabled := False;
  end;
end;

procedure TrwDsgnLibrary.tvFunctDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := True;
end;

procedure TrwDsgnLibrary.tvFunctEditing(Sender: TObject; Node: TTreeNode;
  var AllowEdit: Boolean);
begin
  AllowEdit := not Assigned(Node.Data) and (Node <> tvFunct.Items[0]) and FSecurityRec.LibFunctions.Modify;
end;

procedure TrwDsgnLibrary.tvFunctEndDrag(Sender, Target: TObject; X,
  Y: Integer);
var
  N: TTreeNode;
begin
  N := tvFunct.GetNodeAt(X, Y);
  if Assigned(N) then
  begin
    if Assigned(N.Data) then
      tvFunct.Selected.MoveTo(N, naInsert)
    else
      tvFunct.Selected.MoveTo(N, naAddChild);
    SystemLibFunctions.Modified := True;
  end;
end;

procedure TrwDsgnLibrary.actAddFunctFolderExecute(Sender: TObject);
var
  N: TTreeNode;
begin
  if not Assigned(tvFunct.Selected) or Assigned(tvFunct.Selected.Data) then
    Exit;

  N := tvFunct.Items.AddChildObject(tvFunct.Selected, 'New Folder', nil);
  N.ImageIndex := 57;
  N.SelectedIndex := N.ImageIndex;
  N.StateIndex := N.ImageIndex;
  tvFunct.SortType := stNone;
  tvFunct.SortType := stText;
  tvFunct.Selected := N;

  SystemLibFunctions.Modified := True;
end;

procedure TrwDsgnLibrary.actDelFunctFolderExecute(Sender: TObject);

  procedure DeleteNode(ANode: TTreeNode);
  var
    i: Integer;
  begin
    if Assigned(ANode.Data) then
      ANode.MoveTo(tvFunct.Items[0], naAddChild)
    else
    begin
      for i := 0 to ANode.Count - 1 do
        if Assigned(ANode.Item[0].Data) then
          ANode.Item[0].MoveTo(tvFunct.Items[0], naAddChild)
        else
          DeleteNode(ANode.Item[0]);
      ANode.Free;
    end;
  end;

begin
  if not Assigned(tvFunct.Selected) then
    Exit;

  tvFunct.Items.BeginUpdate;
  DeleteNode(tvFunct.Selected);
  tvFunct.SortType := stNone;
  tvFunct.SortType := stText;
  tvFunct.Items.EndUpdate;

  SystemLibFunctions.Modified := True;
end;


procedure TrwDsgnLibrary.actAddFuncExecute(Sender: TObject);
begin
  tvFunct.Selected := nil;
  FflFuncAdd := True;
  EventText.Text := 'procedure NewProc()' + #13#10 + #13#10 + 'end';
  SetFunctButtons(True);
end;


procedure TrwDsgnLibrary.actEditFuncExecute(Sender: TObject);
begin
  if not (Assigned(tvFunct.Selected) and (TObject(tvFunct.Selected.Data) is TrwLibFunction)) then
    Exit;

  FflFuncAdd := False;
  EventText.Text := TrwLibFunction(tvFunct.Selected.Data).Text;
  SetFunctButtons(True);
end;


procedure TrwDsgnLibrary.actSaveFuncExecute(Sender: TObject);
var
  F: TrwLibFunction;
  Pb, Pe, P: Integer;
  h: String;
begin
  if not FflFuncAdd then
  begin
    F := TrwLibFunction(tvFunct.Selected.Data);
    F.Text := EventText.Text;
    F.Name := ParseFunctionHeader(EventText.Text, F.Params, Pb, Pe, P);
    tvFunct.Selected.Text := F.Name;
  end
  else
  begin
    h := ParseFunctionHeader(EventText.Text, nil, Pb, Pe, P);
    if SystemLibFunctions.IndexOf(h) <> -1 then
      raise ErwException.CreateFmt(ResErrLibFunctExists, [h]);
    F := SystemLibFunctions.AddFunc(EventText.Text);
    AddFunctToTree(F, 55);
  end;

  SetFunctButtons(False);
  AssignChangingInfo(F);
  SystemLibFunctions.Modified := True;

  tvFunct.OnChange(tvFunct, tvFunct.Selected);
end;


procedure TrwDsgnLibrary.actCancelFuncExecute(Sender: TObject);
begin
  if not FflFuncAdd then
    EventText.Text := TrwLibFunction(tvFunct.Selected.Data).Text
  else
    EventText.Text := '';
     
  SetFunctButtons(False);
end;

procedure TrwDsgnLibrary.actDelFuncExecute(Sender: TObject);
begin
  if not (Assigned(tvFunct.Selected) and (TObject(tvFunct.Selected.Data) is TrwLibFunction)) then
    Exit;

  TrwLibFunction(tvFunct.Selected.Data).Free;
  tvFunct.Selected.Free;
  SystemLibFunctions.Modified := True;
end;

procedure TrwDsgnLibrary.actRefreshExecute(Sender: TObject);
begin
  RefreshLibrary(0);
end;


procedure TrwDsgnLibrary.actCompileExecute(Sender: TObject);
var
  h: String;
  lCompCode: TrwCode;
  fl: Boolean;
begin
  ExitFromControl;
  
  Busy;
  try
    fl := actCancelComp.Enabled or actCancelFunc.Enabled;
    try
      if (pgcPages.ActivePage = tsFunct) and actCancelFunc.Enabled and not FflFuncAdd then
      begin
        SetLength(lCompCode, 0);
        SetCompiledCode(@lCompCode, cVMLibraryFunctionsOffset);
        SetCompilingPointer(0);
        SetDebInf(nil);
        SetDebSymbInf(nil);
        try
          CompileFunction(EventText.Text);
        finally
          SetLength(lCompCode, 0);
        end;
        Exit;
      end
      else if not fl then
        SystemLibFunctions.Compile;
    except
      on E:ErwParserError do
      begin
        ShowFunctError(E.EventName, E.LineNum, E.ColNum, E.Message);
        Exit;
      end
      else
        raise;
    end;

    try
      if (pgcPages.ActivePage = tsComp) and actCancelComp.Enabled and not FflCompAdd then
      begin
        try
          SetLength(lCompCode, 0);
          SetCompiledCode(@lCompCode, cVMLibraryComponentsOffset);
          SetCompilingPointer(0);
          SetDebInf(nil);
          SetDebSymbInf(nil);

          DesignCompArea.Component.Compile(True);
        finally
          SetLength(lCompCode, 0);
        end;
        Exit;
      end
      else if not fl then
        SystemLibComponents.Compile;

    except
      on E:ErwParserError do
      begin
        if not SameText(E.ComponentName, cParserLibComp) then
          if SameText(E.ComponentName, VAR_FUNCT_COMP_NAME) then
            h := E.ComponentName
          else
            h := cParserLibComp + '.' + E.ComponentName
        else
          h := cParserLibComp;
        ShowCompError(E.LibComponentName, h, E.EventName, E.LineNum, E.ColNum, E.Message);
      end
      else
        raise;
    end;

  finally
    Ready;
  end;
end;


procedure TrwDsgnLibrary.ShowFunctError(const AFunctName: string; const ALine, ACol: Integer; const AError: string);
var
  i: Integer;
begin
  for i := 0 to tvFunct.Items.Count - 1 do
  begin
    if Assigned(tvFunct.Items[i].Data) and (TObject(tvFunct.Items[i].Data) is TrwLibFunction) and
       SameText(TrwLibFunction(tvFunct.Items[i].Data).Name, AFunctName) then
    begin
      pgcPages.ActivePage := tsFunct;
      if not actCancelFunc.Enabled then
      begin
        tvFunct.Selected := tvFunct.Items[i];
        EventText.CaretY := ALine;
        EventText.CaretX := ACol;
        actEditFunc.Execute;
      end;
      ActiveControl := EventText;
      ShowError(AError);
      break;
    end;
  end;
end;


procedure TrwDsgnLibrary.ShowCompError(const ALibComponentName, AComponentName, AEventName: string;
   const ALine, ACol: Integer; const AError: string);
var
  i: Integer;
  C: TrwLibComponent;

  procedure Err;
  begin
    ShowError(AError);

    if SameText(AComponentName, VAR_FUNCT_COMP_NAME) then
      ShowGlobalVarFunct(DesignCompArea.Component, AEventName, DesignCompArea.Component._LibComponent, ALine, ACol+1)
    else
    begin
      CompDesigner.ShowComponentEvent(AComponentName, AEventName, ALibComponentName);
      CompDesigner.EventText.TopLine := 1;
      CompDesigner.EventText.LeftChar := 1;
      CompDesigner.EventText.CaretY := ALine;
      CompDesigner.EventText.CaretX := ACol;
      ActiveControl := CompDesigner.EventText;
    end;
  end;

begin
  if actCancelComp.Enabled then
  begin
    Err;
    Exit;
  end;

  i := SystemLibComponents.IndexOf(ALibComponentName);
  if i = -1 then
    Exit;

  pgcPages.ActivePage := tsComp;
  C := SystemLibComponents[i];
  for i := 0 to tvLibComp.Items.Count - 1 do
    if tvLibComp.Items[i].Data = C then
    begin
      tvLibComp.Selected := tvLibComp.Items[i];
      actEditComp.Execute;
      Err;
      break;
    end;
end;


procedure TrwDsgnLibrary.actExpLibraryExecute(Sender: TObject);
var
  F: TEvFileStream;
begin
  if not svdExportLib.Execute then
    Exit;

  F := TEvFileStream.Create(svdExportLib.FileName, fmCreate);
  try
    SystemLibFunctions.SaveToStream(F);
    SystemLibComponents.SaveToStream(F);
  finally
    F.Free;
  end;
end;


procedure TrwDsgnLibrary.actImpLibraryExecute(Sender: TObject);
var
  F: TEvFileStream;
begin
  if not opdImportLib.Execute then
    Exit;

  F := TEvFileStream.Create(opdImportLib.FileName, fmOpenRead);
  try
    SystemLibFunctions.LoadFromStream(F);
    SystemLibFunctions.InitializeHeaders;
    SystemLibFunctions.Modified := True;
    SystemLibComponents.LoadFromStream(F);
    SystemLibComponents.Modified := True;
  finally
    F.Free;
  end;

  CreateCompInheritTree;
  CreateCompCategoryTree;
  CreateFunctTree;
end;


procedure TrwDsgnLibrary.RefreshLibrary(AsOfDate: TDateTime);
begin
  if (SystemLibComponents.Modified or SystemLibFunctions.Modified) and
    (MessageDlg('RW Library has been changed. Operation will override all changes! Proceed?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes) then
    Exit;

  SystemLibComponents.Initialize;
  SystemLibFunctions.Initialize;

  CreateCompInheritTree;
  CreateCompCategoryTree;
  CreateFunctTree;
end;

procedure TrwDsgnLibrary.AssignChangingInfo(ALibItem: TrwLibCollectionItem);
begin
  ALibItem.LastChange := Now;
// ###  ALibItem.ChangedBy := ReportWriterEngine.AppAdapter.LoginInfoUserName;
  ALibItem.ChangeReason := FChangeReason;
end;

procedure TrwDsgnLibrary.ShowChangingInfo(ALibItem: TrwLibCollectionItem;
  ALabel: TLabel);
begin
  if Assigned(ALibItem) then
    ALabel.Caption := 'Last Change ' + DateTimeToStr(ALibItem.LastChange) +
      '  by ' + ALibItem.ChangedBy + '   Reason: ' + ALibItem.ChangeReason
  else
    ALabel.Caption := ''; 
end;

procedure TrwDsgnLibrary.tsCompShow(Sender: TObject);
begin
  actDelSelection.Enabled := True;
  CompToolsBar.RestoreState;
end;

procedure TrwDsgnLibrary.tsCompHide(Sender: TObject);
begin
  CompToolsBar.SaveState;
  CompToolsBar.UndockedBarsVisible(False);
end;


procedure TrwDsgnLibrary.btnLoadPictureClick(Sender: TObject);
begin
  if not opdPicture.Execute then
    opdPicture.FileName := '';
end;

procedure TrwDsgnLibrary.ExitFromControl;
var
  M: TMethod;
begin
  if Assigned(ActiveControl) and IsPublishedProp(ActiveControl, 'OnExit') then
  begin
    M := GetMethodProp(ActiveControl, 'OnExit');
    if Assigned(M.Code) then
      TNotifyEvent(M)(ActiveControl);
  end;
end;

procedure TrwDsgnLibrary.chbShowInExprEditorClick(Sender: TObject);
begin
  if Assigned(tvFunct.Selected) then
  begin
    if chbShowInExprEditor.Checked then
      TrwLibFunction(tvFunct.Selected.Data).DesignTimeInfo := TrwLibFunction(tvFunct.Selected.Data).DesignTimeInfo + [fdtShowInEditor]
    else
      TrwLibFunction(tvFunct.Selected.Data).DesignTimeInfo := TrwLibFunction(tvFunct.Selected.Data).DesignTimeInfo - [fdtShowInEditor];
  end;
end;

procedure TrwDsgnLibrary.chbDataFilterClick(Sender: TObject);
begin
  if Assigned(tvFunct.Selected) then
  begin
    if chbDataFilter.Checked then
      TrwLibFunction(tvFunct.Selected.Data).DesignTimeInfo := TrwLibFunction(tvFunct.Selected.Data).DesignTimeInfo + [fdtDataFilter]
    else
      TrwLibFunction(tvFunct.Selected.Data).DesignTimeInfo := TrwLibFunction(tvFunct.Selected.Data).DesignTimeInfo - [fdtDataFilter];
  end;
end;

procedure TrwDsgnLibrary.tsFunctShow(Sender: TObject);
begin
  actDelSelection.Enabled := False;
end;

procedure TrwDsgnLibrary.edDescriptionExit(Sender: TObject);
begin
  if tvFunct.Selected = nil then
    Exit;

  if TrwLibFunction(tvFunct.Selected.Data).DisplayName <> edDescription.Text then
    SystemLibFunctions.Modified := True;

  TrwLibFunction(tvFunct.Selected.Data).DisplayName := edDescription.Text;
end;

procedure TrwDsgnLibrary.memNotesExit(Sender: TObject);
begin
  if tvFunct.Selected = nil then
    Exit;

  if TrwLibFunction(tvFunct.Selected.Data).Description <> memNotes.Text then
    SystemLibFunctions.Modified := True;

  TrwLibFunction(tvFunct.Selected.Data).Description := memNotes.Text;
end;

end.
