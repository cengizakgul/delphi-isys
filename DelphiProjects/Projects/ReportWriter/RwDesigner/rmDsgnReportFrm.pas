unit rmDsgnReportFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, SyncObjs,
  Dialogs, rmDsgnComponentFrm, Menus, ImgList, ComCtrls, ExtCtrls, rmDsgnTypes,
  rmLibCompChooseFrm, rwEngineTypes, rmReport, rwDsgnUtils, rwUtils, rmTypes, rwTypes,
  rwBasicUtils, rwDesigner, rmCustomFrameFrm, rmLocalToolBarFrm, rwConnectionInt,
  rmTBDsgnComponentFrm, rmCustomStatusBarFrm, ActnList, rmTBDsgnReportFrm, rmChildFormFrm,
  rmDsgnInputFormContainerFrm, rwQBInfo, rwRTTI, rmDummyDsgnComponent, EvStreamUtils, rwEngine,
  rwConnection, isErrorUtils;

type
  TrmDsgnReport = class;


  TrwStatusThread = class(TThread)
  private
    FOwner:   TrmDsgnReport;
    procedure SyncUpdateStatus;
  protected
    procedure Execute; override;
  public
    constructor Create(AOwner: TrmDsgnReport);  
  end;


  TrwRunReportThread = class(TThread)
  private
    FOwner:   TrmDsgnReport;
    FReportData: IEvDualStream;
    FStatusThread: TrwStatusThread;
  protected
    procedure Execute; override;
    procedure DoTerminate; override;
  public
    constructor Create(AOwner: TrmDsgnReport);
  end;


  TrmDebugLineList = array of TrmDebugLineInfo;
  PTrmDebugLineList = ^TrmDebugLineList;

  TrmDebugSymbolsInfo = record
    Segment:            TrwVMCodeSegment;
    FirstAddress:       Integer;
    LastAddress:        Integer;
    DebSymbols:         String;
  end;

  TrmDebugSymbolsList =  array of TrmDebugSymbolsInfo;
  PTrmDebugSymbolsList = ^TrmDebugSymbolsList;


  TrmDebugSourceCodeInfo = record
    Report:                   TrmDebugLineList;
    ReportDebSymbols:         TrmDebugSymbolsList;
    LibFunctions:             TrmDebugLineList;
    LibFunctionsDebSymbols:   TrmDebugSymbolsList;
    LibComponents:            TrmDebugLineList;
    LibComponentsDebSymbols:  TrmDebugSymbolsList;
  end;

  TrmDebugInfoHolder = class;

  TrmBreakPoints = class(TInterfacedObject, IrmBreakPoints)
  private
    FOwner: TrmDebugInfoHolder;
    FList: TList;
    function    GetItem(Index: Integer): PTrmBreakPointInfo;
    procedure   SetBreakpointAddress(AIndex: Integer);
  protected
    procedure   ToggleBreakPoint(const AObjectLocation, AFunctionName: String; ASourceCodeLineNbr: Integer);
    function    Count: Integer;
    function    BreakPointByIndex(const AIndex: Integer): TrmBreakPointInfo;
    procedure   PostBreakpointsIntoRunningProcess;

  public
    constructor Create(AOwner: TrmDebugInfoHolder);
    destructor  Destroy; override;
    function    Add(const AObjectLocation, AFunctionName: String; ASourceCodeLineNbr: Integer): Integer;
    function    IndexOf(const AObjectLocation, AFunctionName: String; ASourceCodeLineNbr: Integer): Integer; overload;
    function    IndexOf(const AAddress: TrwVMAddress): Integer; overload;
    procedure   Delete(const AIndex: Integer);
    procedure   Clear;
    procedure   Prepare;
    function    GetAsVariant: Variant;
    property    Items[Index: Integer]: PTrmBreakPointInfo read GetItem; default;
  end;


  TrmWatches = class(TInterfacedObject, IrmWatchList)
  private
    FOwner: TrmDebugInfoHolder;
    FList: TList;
    function    GetItem(Index: Integer): PTrmWatchInfo;

  protected
    function    Add(const AExpression: String): Integer;
    procedure   Delete(const AIndex: Integer);
    procedure   Update(const AIndex: Integer; const AExpression: String);
    function    Count: Integer;
    function    WatchByIndex(const AIndex: Integer): TrmWatchInfo;
    procedure   Recalculate(const AIndex: Integer = -1);

  public
    constructor Create(AOwner: TrmDebugInfoHolder);
    destructor  Destroy; override;
    procedure   Clear;
    procedure   ClearResults;
    property    Items[Index: Integer]: PTrmWatchInfo read GetItem; default;
  end;


  TrmDebugInfoHolder = class
  private
    FOwner: TrmDsgnReport;
    FDebugSourceCode: TrmDebugSourceCodeInfo;
    FVMStatus: TrwVMStateType;
    FVMStatusExpired: Boolean;
    FVMRegisters: TrwVMRegisters;
    FVMCallStack: TrwVMAddressList;
    FDebugStopLineInfo: TrmDebugStopLineInfo;
    FBreakPointList: TrmBreakPoints;
    FWatches: TrmWatches;
    procedure Prepare;
    function  FindDebugLine(const AAddress: TrwVMAddress): PTrmDebugLineInfo; overload;
    function  FindDebugLine(const AObjectLocation, AFunctionName: String; ASourceCodeLineNbr: Integer): PTrmDebugLineInfo; overload;
    function  FindDebugSymbInfo(const AAddress: TrwVMAddress): String;
    procedure SetReportStopPoint;
    function  FindObjectByDebInfoLocation(const PDebLineInfo: PTrmDebugLineInfo): IrmDesignObject;
    procedure ClearReportStopPoint;
    procedure CleanUp;
    procedure UpdateVMStatus;
  public
    constructor Create(AOwner: TrmDsgnReport);
    destructor  Destroy; override;
  end;

  TrmRunAction = (rmRARun, rmRATraceOver, rmRATraceIn);

  TrmDsgnReport = class(TrmDsgnComponent, IrmDsgnReport)
  private
    FEngine: IrwEngineApp;
    FProcessController: IrwProcessController;
    FReportResult: IEvDualStream;
    FErrorMessage: String;

    FDebugInfoHolder: TrmDebugInfoHolder;
    FLastStackDepth: Integer;
    FCallStack: TrmDebugStopLineList;

    FParamFileName: String;

    FSystemLibrary: TrmSystemLibraryHolder;

    procedure DMTBSetDefaultParams(var Message: TrmMessageRec); message mtbSetDefaultParams;
    procedure DMTBSetParamFileName(var Message: TrmMessageRec); message mtbSetParamFileName;
    procedure DMTBRun(var Message: TrmMessageRec); message mtbRun;
    procedure DMTBStop(var Message: TrmMessageRec); message mtbStop;
    procedure DMTBPause(var Message: TrmMessageRec); message mtbPause;
    procedure DMTBTraceIn(var Message: TrmMessageRec); message mtbTraceIn;
    procedure DMTBTraceOver(var Message: TrmMessageRec); message mtbTraceOver;
    procedure DMTBShowCallStack(var Message: TrmMessageRec); message mtbShowCallStack;
    procedure DMTBShowWatches(var Message: TrmMessageRec); message mtbShowWatches;
    procedure DMTShowSourceCodeByAddress(var Message: TrmMessageRec); message mtbShowSourceCodeByAddress;
    procedure DMTBShowQueryBuilder(var Message: TrmMessageRec); message mtbShowQueryBuilder;

    function  ConnectedToRWEngine: Boolean;
    procedure ConnectToRWEngine;
    procedure DisconnectFromRWEngine;
    procedure DoRun(const RunAction: TrwRunAction);
    procedure DoStop;
    procedure WMRunTimeNotification(var Message: TMessage); message WM_RMDEBUG;
    procedure DoRunningPaused;
    procedure DoRunningProceeded;
    procedure DoRunningStopped;

    procedure SetDefaultParams;
    procedure SetParamFileName;
    procedure EngineQueryBuilder;

  protected
    function  GetToolbarClass: TrmLocalToolBarCalss; override;
    procedure NewDesignComponent(const AClassName: String); override;
    procedure OpenFromFile(const AFileName: String); override;
    procedure Compile; override;

    // Interface implementation
    procedure GetListeningMessages(const AMessages: TList); override;
    procedure DesignReport(const AData: TStream);
    function  ReportStopLineInfo: TrmDebugStopLineInfo;
    function  ReportCallStack: PTrmDebugStopLineList;
    function  ReadOnlyMode: Boolean; override;
    function  GetBreakPointList: IrmBreakPoints;
    function  GetWatchList: IrmWatchList;
    procedure SetReportFileName (AReportName : String);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;

implementation

uses rmObjectPropFormFrm, rwParamFileFrm, rmDockableFrameFrm, rmCallStackFrm, rmWatchListFrm;

{$R *.dfm}



{ TrmDsgnReport }

constructor TrmDsgnReport.Create(AOwner: TComponent);
begin
  inherited;
  FSystemLibrary := TrmSystemLibraryHolder.Create(Self);
  FDefaultFileName := 'Report.rwr';
  FDebugInfoHolder := TrmDebugInfoHolder.Create(Self);
end;

procedure TrmDsgnReport.NewDesignComponent(const AClassName: String);
var
  LibComp: IrmComponent;
  h: String;
begin
  LibComp := ChoiceLibComp(['TrmReport'], nil, Self);
  if Assigned(LibComp) then
    h := LibComp.GetName
  else
    h := RWEngineExt.AppAdapter.SubstDefaultClassName(AClassName);

  DestroyDsgnComponent;
  FComponent := CreateRWComponentByClass(h);
  if FComponent.GetPropertyValue('Description') = '' then
    FComponent.SetPropertyValue('Description', 'New Report');

  FComponent.InitializeForDesign;

  PrepareDesigner;
end;

procedure TrmDsgnReport.OpenFromFile(const AFileName: String);
begin
  DestroyDsgnComponent;
  FComponent := CreateRWComponentByClass('TrmReport');
  svdExport.FileName := AFileName;
  LoadRWResFromFile(FComponent.RealObject, AFileName);
  PrepareDesigner;
end;

procedure TrmDsgnReport.Compile;
begin
  inherited;
  (DesigningObject as IrmReportObject).Compile;
end;

procedure TrmDsgnReport.DesignReport(const AData: TStream);
begin
  LoadComponentFromStream(AData);
end;

procedure TrmDsgnReport.SetDefaultParams;
var
  S: IEvDualStream;
  Rep: IrwReportStructure;
  Params: TrwReportParamList;
begin
  ConnectToRWEngine;

  S := TEvDualStreamHolder.Create;
  (DesigningObject as IrmReportObject).SaveToStream(S.RealStream);
  FEngine.OpenReportFromStream(S);
  Rep := FEngine.GetActiveReport;
  S := nil;

  if Rep.ReportInputForm.Show(0, False) then
  begin
    Rep.ReportInputForm.CloseOK;
    S := Rep.ReportParameters.ParamsToStream;
    S.Position := 0;
    Params := rwDesignerExt.AppAdapter.ReadReportParamsFromStream(S.RealStream);
    (DesigningObject as IrmReportObject).SetReportParams(@Params);
  end;
end;

procedure TrmDsgnReport.SetParamFileName;
begin
  ShowParamFile(FParamFileName);
end;

function TrmDsgnReport.GetToolbarClass: TrmLocalToolBarCalss;
begin
  Result := TrmTBDsgnReport;
end;

procedure TrmDsgnReport.DMTBSetDefaultParams(var Message: TrmMessageRec);
begin
  inherited;
  SetDefaultParams;
end;

procedure TrmDsgnReport.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mtbSetDefaultParams));
  AMessages.Add(Pointer(mtbSetParamFileName));
  AMessages.Add(Pointer(mtbRun));
  AMessages.Add(Pointer(mtbStop));
  AMessages.Add(Pointer(mtbPause));
  AMessages.Add(Pointer(mtbTraceIn));
  AMessages.Add(Pointer(mtbTraceOver));
  AMessages.Add(Pointer(mtbShowCallStack));
  AMessages.Add(Pointer(mtbShowWatches));
  AMessages.Add(Pointer(mtbShowSourceCodeByAddress));
end;

procedure TrmDsgnReport.DMTBSetParamFileName(var Message: TrmMessageRec);
begin
  inherited;
  SetParamFileName;
end;

procedure TrmDsgnReport.DMTBPause(var Message: TrmMessageRec);
begin
 if ConnectedToRWEngine then
   FProcessController.Pause;
end;

procedure TrmDsgnReport.DMTBRun(var Message: TrmMessageRec);
begin
  try
    DoRun(rwRARun);
  except
    FDebugInfoHolder.CleanUp;
    PostApartmentMessage(Self, mRunningStopped, 0, 0);
    raise;
  end;
end;

procedure TrmDsgnReport.DMTBStop(var Message: TrmMessageRec);
begin
  DoStop;
end;

procedure TrmDsgnReport.DMTBTraceIn(var Message: TrmMessageRec);
begin
  DoRun(rwRATraceIn);
end;

procedure TrmDsgnReport.DMTBTraceOver(var Message: TrmMessageRec);
begin
  DoRun(rwRATraceOver);
end;

procedure TrmDsgnReport.DoRunningStopped;
var
  Preview: IrwPreviewApp;
begin
  FDebugInfoHolder.CleanUp;
  PostApartmentMessage(Self, mRunningStopped, 0, 0);

  if Assigned(FReportResult) then
  begin
    Preview := RWConnectionPool.GetRWConnection('RW Preview') as IrwPreviewApp;
    try
      Preview.Preview(FReportResult, 0);
    finally
      RWConnectionPool.ReturnRWConnection(Preview);
      Preview := nil;
      FReportResult := nil;
    end;
  end

  else if FErrorMessage <> '' then
    rwError(FErrorMessage)
  else
    ShowMessage('Report has not produced any result or result is empty');
end;

destructor TrmDsgnReport.Destroy;
begin
  try
    DisconnectFromRWEngine;
  except
  end;

  FreeAndNil(FDebugInfoHolder);
  inherited;
end;

function TrmDsgnReport.ReportStopLineInfo: TrmDebugStopLineInfo;
begin
  Result := FDebugInfoHolder.FDebugStopLineInfo;
end;


procedure TrmDsgnReport.DoRunningPaused;
begin
  if ConnectedToRWEngine then
  begin
    FDebugInfoHolder.SetReportStopPoint;

    if Assigned(FDebugInfoHolder.FDebugStopLineInfo.SorceCodeLineInfo) or (FErrorMessage <> '') then
    begin
      PostApartmentMessage(Self, mtbShowEventsEditor, 0, 0);

      if Assigned(FDebugInfoHolder.FDebugStopLineInfo.rmObject) and (not IsObjectSelected(FDebugInfoHolder.FDebugStopLineInfo.rmObject) or GroupSelection) then
      begin
        UnselectAll;
        SelectObject(FDebugInfoHolder.FDebugStopLineInfo.rmObject);
      end;

      PostApartmentMessage(Self, mRunningPaused, 0, 0);

      if FErrorMessage <> '' then
        rwError('Report execution has paused with exception!'#13 + FErrorMessage);
    end

    else
      DoRun(rwRATraceOver);  //Technically shouldn't get in there but let's do it just in case to helps to avoid the "stuck buttons"
  end;
end;


procedure TrmDsgnReport.DoRunningProceeded;
begin
  FDebugInfoHolder.ClearReportStopPoint;
  PostApartmentMessage(Self, mRunningProceeded, 0, 0);
end;


procedure TrmDsgnReport.WMRunTimeNotification(var Message: TMessage);
begin
  if Message.WParam = 1 then
    DoRunningPaused
  else if Message.WParam = 2 then
    DoRunningProceeded
  else if Message.WParam = 3 then
    DoRunningStopped;
end;


function TrmDsgnReport.ReadOnlyMode: Boolean;
begin
  Result := ConnectedToRWEngine;
end;

procedure TrmDsgnReport.DoRun(const RunAction: TrwRunAction);
var
  S: IEvDualStream;
begin
  if not ConnectedToRWEngine or (FDebugInfoHolder.FVMStatus = rwVMSTStopped)  then
  begin
    SendBroadcastMessage(mDoApplyPendingChanges, 0, 0);
    FReportResult := nil;
    FErrorMessage := '';

    FDebugInfoHolder.Prepare;
    FLastStackDepth := 0;

    ConnectToRWEngine;

    S := TEvDualStreamHolder.Create;
    SystemLibFunctions.StoreDebInfo := True;
    SystemLibFunctions.SaveToStream(S.RealStream);
    FEngine.SetExternalModuleData('SystemLibFunctions', S);

    S.Size := 0;
    SystemLibComponents.StoreDebInfo := True;
    SystemLibComponents.SaveToStream(S.RealStream);
    FEngine.SetExternalModuleData('SystemLibComponents', S);

    S.Size := 0;
    DataDictionary.SaveToStream(S.RealStream);
    FEngine.SetExternalModuleData('SystemDataDictionary', S);

    FDebugInfoHolder.FBreakPointList.PostBreakpointsIntoRunningProcess;

    TrwRunReportThread.Create(Self);  // run report in separate thread

    repeat
      Sleep(500);
      if FErrorMessage <> '' then
      begin
        DoStop;
        DoRunningStopped;
      end;
    until FProcessController.GetState.VMState <> rwVMSTStopped;
  end;

  FDebugInfoHolder.FVMStatusExpired := True;
  FProcessController.Run(RunAction);
end;

procedure TrmDsgnReport.DMTBShowCallStack(var Message: TrmMessageRec);
begin
  inherited;
  ShowDockableForm(TrmCallStack);
end;

function TrmDsgnReport.ReportCallStack: PTrmDebugStopLineList;
var
  i: Integer;
begin
  SetLength(FCallStack, Length(FDebugInfoHolder.FVMCallStack));

  for i := Low(FDebugInfoHolder.FVMCallStack) to High(FDebugInfoHolder.FVMCallStack) do
  begin
    FCallStack[i].Address := FDebugInfoHolder.FVMCallStack[i];
    FCallStack[i].SorceCodeLineInfo := FDebugInfoHolder.FindDebugLine(FCallStack[i].Address);
  end;

  Result := @FCallStack;
end;


function TrmDsgnReport.GetBreakPointList: IrmBreakPoints;
begin
  Result := FDebugInfoHolder.FBreakPointList;
end;

procedure TrmDsgnReport.DMTBShowWatches(var Message: TrmMessageRec);
begin
  inherited;
  ShowDockableForm(TrmWatchList);
end;

function TrmDsgnReport.GetWatchList: IrmWatchList;
begin
  Result := FDebugInfoHolder.FWatches;
end;

procedure TrmDsgnReport.DMTShowSourceCodeByAddress(var Message: TrmMessageRec);
var
  lAddress: TrwVMAddress;
  OldStopLine: TrmDebugStopLineInfo;
begin
  inherited;

  OldStopLine := FDebugInfoHolder.FDebugStopLineInfo;
  try
    lAddress := GetEntryPoint(Message.Parameter1);

    FDebugInfoHolder.FDebugStopLineInfo.Address :=lAddress;
    FDebugInfoHolder.FDebugStopLineInfo.SorceCodeLineInfo := FDebugInfoHolder.FindDebugLine(lAddress);

    if Assigned(FDebugInfoHolder.FDebugStopLineInfo.SorceCodeLineInfo) then
    begin
      FDebugInfoHolder.FDebugStopLineInfo.rmObject := FDebugInfoHolder.FindObjectByDebInfoLocation(FDebugInfoHolder.FDebugStopLineInfo.SorceCodeLineInfo);
      if Assigned(FDebugInfoHolder.FDebugStopLineInfo.rmObject) then
      begin
        PostApartmentMessage(Self, mtbShowEventsEditor, 0, 0);
        if not IsObjectSelected(FDebugInfoHolder.FDebugStopLineInfo.rmObject) or GroupSelection then
        begin
          UnselectAll;
          SelectObject(FDebugInfoHolder.FDebugStopLineInfo.rmObject);
        end;

        // SendApartmentMessage would not work correctly since refresh event in some frames uses Idle
        // will try to improve this some day
        PostApartmentMessage(Self, mRunningPaused, 0, 0);
        Application.ProcessMessages;
      end;
    end;

  finally
    FDebugInfoHolder.FDebugStopLineInfo := OldStopLine;
  end;
end;

procedure TrmDsgnReport.DoStop;
begin
  if ConnectedToRWEngine then
  begin
    try
      FProcessController.Stop;
    except
      try
        try
          DisconnectFromRWEngine;
        finally
          DoRunningStopped;
        end;
      except
      end;
    end;
  end
  
  else
    DoRunningStopped;
end;

procedure TrmDsgnReport.DisconnectFromRWEngine;
begin
  if ConnectedToRWEngine then
  begin
    FProcessController := nil;
    FEngine := nil;
  end;
end;

procedure TrmDsgnReport.ConnectToRWEngine;

  procedure Connect;
  begin
    FEngine := RWConnectionPool.GetRWConnection('RW Engine') as IrwEngineApp;
    try
      FEngine.InitializeAppAdapter(AppAdapterModuleName, AppAdapterAppCustomData);
      FProcessController := FEngine.ProcessController;
    except
      FProcessController := nil;
      FEngine := nil;
      raise;
    end;
  end;

begin
  if not ConnectedToRWEngine then
    Connect
  else
  begin
    try
      FEngine.QueryInfo('');
    except
      FEngine := nil;
      FProcessController := nil;
      Connect;
    end;
  end;
end;

function TrmDsgnReport.ConnectedToRWEngine: Boolean;
begin
  Result := Assigned(FEngine);
end;


procedure TrmDsgnReport.DMTBShowQueryBuilder(var Message: TrmMessageRec);
begin
  if ConnectedToRWEngine and (FDebugInfoHolder.FVMStatus = rwVMSTPaused) then
    EngineQueryBuilder
  else
    inherited;
end;

procedure TrmDsgnReport.EngineQueryBuilder;
var
  Q: TrwQBQuery;
  h: String;
  Stream: IEvDualStream;
begin
  if FSelectedObjects.Count = 1 then
  begin
    h := FindQBQueryProp(FSelectedObjects[0]);
    if h <> '' then
    begin
      Q := TObject(Pointer(Integer(FSelectedObjects[0].GetPropertyValue(h)))) as TrwQBQuery;
      Stream := TEvDualStreamHolder.Create;
      Q.SaveToStream(Stream.RealStream);
      Stream.Position := 0;
    end;
  end;

  FProcessController.OpenQueryBuilder(Stream, False);
end;

procedure TrmDsgnReport.SetReportFileName(AReportName: String);
begin
  svdExport.FileName := AReportName;
end;

{ TrmDebugInfoHolder }

procedure TrmDebugInfoHolder.ClearReportStopPoint;
begin
  ZeroMemory(@FDebugStopLineInfo, SizeOf(FDebugStopLineInfo));
  ZeroMemory(@FVMRegisters, SizeOf(FVMRegisters));
  SetLength(FVMCallStack, 0);
  FWatches.ClearResults;  
end;

constructor TrmDebugInfoHolder.Create(AOwner: TrmDsgnReport);
begin
  FOwner := AOwner;

  FBreakPointList := TrmBreakPoints.Create(Self);
  FBreakPointList._AddRef;

  FWatches := TrmWatches.Create(Self);
  FWatches._AddRef;
end;

function TrmDebugInfoHolder.FindDebugLine(const AAddress: TrwVMAddress): PTrmDebugLineInfo;
var
  PLines: PTrmDebugLineList;
  CurrPos, BegPos, EndPos: Integer;
begin
  Result := nil;

  if AAddress.Segment = rcsReport then
    PLines := Addr(FDebugSourceCode.Report)
  else if AAddress.Segment = rcsLibFunctions then
    PLines := Addr(FDebugSourceCode.LibFunctions)
  else if AAddress.Segment = rcsLibComponents then
    PLines := Addr(FDebugSourceCode.LibComponents)
  else
    exit;

  if Length(PLines^) = 0 then
    exit;

  // search
  BegPos := Low(PLines^);
  EndPos := High(PLines^);
  CurrPos := BegPos + (EndPos - BegPos) div 2;
  repeat
    if (PLines^[CurrPos].FirstAddress <= AAddress.Offset) and (PLines^[CurrPos].LastAddress >= AAddress.Offset) then
    begin
      Result := @PLines^[CurrPos];
      break;
    end

    else if PLines^[CurrPos].LastAddress < AAddress.Offset then
      BegPos := CurrPos + 1

    else if PLines^[CurrPos].FirstAddress > AAddress.Offset then
      EndPos := CurrPos - 1

    else
      break;

    CurrPos := BegPos + (EndPos - BegPos) div 2;

  until BegPos > EndPos;
end;


destructor TrmDebugInfoHolder.Destroy;
begin
  FWatches._Release;
  FBreakPointList._Release;
  inherited;
end;

function TrmDebugInfoHolder.FindDebugLine(const AObjectLocation, AFunctionName: String; ASourceCodeLineNbr: Integer): PTrmDebugLineInfo;

  procedure SearchWithinSegment(DebugLines: PTrmDebugLineList);
  var
    i: Integer;
  begin
    for i := Low(DebugLines^) to High(DebugLines^) do
      with DebugLines^[i] do
        if (ObjectLocation = AObjectLocation) and (FunctionName = AFunctionName) and (SourceCodeLineNbr = ASourceCodeLineNbr) then
        begin
          Result := Addr(DebugLines^[i]);
          break;
        end;
  end;

begin
  Result := nil;

  SearchWithinSegment(Addr(FDebugSourceCode.Report));
  if not Assigned(Result) then
  begin
    SearchWithinSegment(Addr(FDebugSourceCode.LibFunctions));
    if not Assigned(Result) then
      SearchWithinSegment(Addr(FDebugSourceCode.LibComponents));
  end;
end;

procedure TrmDebugInfoHolder.Prepare;

  procedure PrepareCodeSegment(ACodeSegment: TrwVMCodeSegment);
  var
    i: Integer;
    h: String;
    PDebInfoSource: PTrwDebInf;
    PDebInfoDest: PTrmDebugLineList;
    PDebSymbSource: PTrwDebInf;
    PDebSymbDest: PTrmDebugSymbolsList;
  begin
    case ACodeSegment of
      rcsReport:
        begin
          PDebInfoSource := (FOwner.DesigningObject as IrmReportObject).GetDebInf;
          PDebInfoDest := Addr(FDebugSourceCode.Report);
          PDebSymbSource := (FOwner.DesigningObject as IrmReportObject).GetDebSymbInf;
          PDebSymbDest := Addr(FDebugSourceCode.ReportDebSymbols);
        end;

      rcsLibFunctions:
        begin
          PDebInfoSource := SystemLibFunctions.GetDebInf;
          PDebInfoDest := Addr(FDebugSourceCode.LibFunctions);
          PDebSymbSource := SystemLibFunctions.GetDebSymbInf;
          PDebSymbDest := Addr(FDebugSourceCode.LibFunctionsDebSymbols);
        end;

      rcsLibComponents:
        begin
          PDebInfoSource := SystemLibComponents.GetDebInf;
          PDebInfoDest := Addr(FDebugSourceCode.LibComponents);
          PDebSymbSource := SystemLibComponents.GetDebSymbInf;
          PDebSymbDest := Addr(FDebugSourceCode.LibComponentsDebSymbols);
        end;
    else
      PDebInfoSource := nil;
      PDebInfoDest := nil;
      PDebSymbSource := nil;
      PDebSymbDest := nil;
    end;


    if Assigned(PDebInfoSource) then
    begin
      SetLength(PDebInfoDest^, Length(PDebInfoSource^));

      for i := Low(PDebInfoSource^) to High(PDebInfoSource^) do
      begin
        h:= PDebInfoSource^[i];
        with PDebInfoDest^[i] do
        begin
          Segment := ACodeSegment;
          FirstAddress := StrToInt(GetNextStrValue(h, ';'));
          LastAddress := StrToInt(GetNextStrValue(h, ';'));
          ObjectLocation := GetNextStrValue(h, ';');
          FunctionName := GetNextStrValue(h, ';');
          SourceCodeLineNbr := StrToInt(GetNextStrValue(h, ';'));
          SourceCodeLineTxt := GetNextStrValue(h, ';');

          h := ObjectLocation;
          h := GetNextStrValue(h, '.');
          if h <> FOwner.DesigningObject.GetPropertyValue('Name') then
            ClassName := h
          else
            ClassName := '';
        end;
      end;
    end

    else
      SetLength(PDebInfoDest^, 0);

      
    if Assigned(PDebSymbSource) then
    begin
      SetLength(PDebSymbDest^, Length(PDebSymbSource^));

      for i := Low(PDebSymbSource^) to High(PDebSymbSource^) do
      begin
        h:= PDebSymbSource^[i];
        if h <> '' then
          with PDebSymbDest^[i] do
          begin
            Segment := ACodeSegment;
            GetNextStrValue(h, ';');
            GetNextStrValue(h, ';');
            FirstAddress := PDebInfoDest^[StrToInt(GetNextStrValue(h, ';'))].FirstAddress;
            LastAddress := PDebInfoDest^[StrToInt(GetNextStrValue(h, ';'))].LastAddress;
            DebSymbols := h;
          end;
      end;
    end

    else
      SetLength(PDebSymbDest^, 0);
  end;

begin
  SystemLibFunctions.CreateDebInf;
  SystemLibComponents.CreateDebInf;

  (FOwner.DesigningObject as IrmReportObject ).Compile;

  PrepareCodeSegment(rcsReport);
  PrepareCodeSegment(rcsLibFunctions);
  PrepareCodeSegment(rcsLibComponents);

  FBreakPointList.Prepare;
end;

procedure TrmDebugInfoHolder.SetReportStopPoint;
var
  h, DebLoc: String;
  i: Integer;
  SCInfo: PTrmDebugLineInfo;
  RootObjectName: String;
  SelfExpr: TrwVMExpressionResult;
begin
  FVMRegisters := FOwner.FProcessController.GetVMRegisters;

  FDebugStopLineInfo.Address := VariantToVMAddress(FVMRegisters.CSIP);
  FDebugStopLineInfo.rmObject := nil;
  FDebugStopLineInfo.SorceCodeLineInfo := FindDebugLine(FDebugStopLineInfo.Address);

  FVMCallStack := VariantToVMAddrList(FOwner.FProcessController.GetVMCallStack);
  SetLength(FVMCallStack, Length(FVMCallStack) + 1);
  FVMCallStack[High(FVMCallStack)] := FDebugStopLineInfo.Address;

  if Assigned(FDebugStopLineInfo.SorceCodeLineInfo) then
  begin
    h := FDebugStopLineInfo.SorceCodeLineInfo.ObjectLocation;

    h := GetNextStrValue(h, '.');

    if AnsiSameStr(h, cParserLibFunct) then
    begin
      FDebugStopLineInfo.rmObject := FOwner.FSystemLibrary;
      FDebugStopLineInfo.DebugSymbolInfo := FindDebugSymbInfo(FDebugStopLineInfo.Address);
    end

    else
    begin
      RootObjectName := FOwner.DesigningObject.GetPropertyValue('Name');
      if AnsiSameText(h, RootObjectName) then
        DebLoc := FDebugStopLineInfo.SorceCodeLineInfo.ObjectLocation

      else if (FDebugStopLineInfo.SorceCodeLineInfo.ClassName <> '') and (FOwner.DesigningObject.ObjectInheritsFrom(FDebugStopLineInfo.SorceCodeLineInfo.ClassName)) then
      begin
        DebLoc := FDebugStopLineInfo.SorceCodeLineInfo.ObjectLocation;
        GetNextStrValue(DebLoc, '.');
        DebLoc := RootObjectName + '.' + DebLoc;
      end

      else
      begin
        DebLoc := FDebugStopLineInfo.SorceCodeLineInfo.ObjectLocation;
        h := DebLoc;
        i := High(FVMCallStack);
        while not AnsiSameText(GetNextStrValue(h, '.'), RootObjectName) and (i >= 0) do
        begin
          SCInfo := FindDebugLine(FVMCallStack[i]);
          if Assigned(SCInfo) then
            DebLoc := SCInfo.ObjectLocation
          else
            DebLoc := '';
          h := DebLoc;
          Dec(i);
        end;
      end;

      FDebugStopLineInfo.DebugSymbolInfo := FindDebugSymbInfo(FDebugStopLineInfo.Address);

      FDebugStopLineInfo.rmObject := ComponentByPath(DebLoc, FOwner.DesigningObject);

      if not Assigned(FDebugStopLineInfo.rmObject) then // If cannot find object via debug info then try to find it as "SELF" watch expression
      begin
        SelfExpr := FOwner.FProcessController.CalcExpression('Self.PathFromRootOwner', FDebugStopLineInfo.DebugSymbolInfo);
        if SelfExpr.ResultType <> rwvString then
          SelfExpr := FOwner.FProcessController.CalcExpression('LibComp.PathFromRootOwner', FDebugStopLineInfo.DebugSymbolInfo);  // try to do the same for "LIBCOMP"

        if (SelfExpr.ResultType = rwvString) and (SelfExpr.Result <> '') then
          try
            FDebugStopLineInfo.rmObject := ComponentByPath(SelfExpr.Result, FOwner.DesigningObject);
          except
          end;
      end;
    end;

    if not Assigned(FDebugStopLineInfo.rmObject) then
      FDebugStopLineInfo.SorceCodeLineInfo := nil
    else
      FWatches.Recalculate;
  end;
end;

procedure TrmDebugInfoHolder.CleanUp;
begin
  ZeroMemory(@FDebugSourceCode, SizeOf(FDebugSourceCode));
  FVMStatus := rwVMSTStopped;
  FVMStatusExpired := True;
  ClearReportStopPoint;
  FBreakPointList.Prepare;
end;


function TrmDebugInfoHolder.FindDebugSymbInfo(const AAddress: TrwVMAddress): String;
var
  PLines: PTrmDebugSymbolsList;
  CurrPos, BegPos, EndPos: Integer;
begin
  Result := '';

  if AAddress.Segment = rcsReport then
    PLines := Addr(FDebugSourceCode.ReportDebSymbols)
  else if AAddress.Segment = rcsLibFunctions then
    PLines := Addr(FDebugSourceCode.LibFunctionsDebSymbols)
  else if AAddress.Segment = rcsLibComponents then
    PLines := Addr(FDebugSourceCode.LibComponentsDebSymbols)
  else
    exit;

  if Length(PLines^) = 0 then
    exit;

  // search
  BegPos := Low(PLines^);
  EndPos := High(PLines^);
  CurrPos := BegPos + (EndPos - BegPos) div 2;
  repeat
    if (PLines^[CurrPos].FirstAddress <= AAddress.Offset) and (PLines^[CurrPos].LastAddress >= AAddress.Offset) then
    begin
      Result := PLines^[CurrPos].DebSymbols;
      break;
    end

    else if PLines^[CurrPos].LastAddress < AAddress.Offset then
      BegPos := CurrPos + 1

    else if PLines^[CurrPos].FirstAddress > AAddress.Offset then
      EndPos := CurrPos - 1

    else
      break;

    CurrPos := BegPos + (EndPos - BegPos) div 2;

  until BegPos > EndPos;
end;


function TrmDebugInfoHolder.FindObjectByDebInfoLocation(const PDebLineInfo: PTrmDebugLineInfo): IrmDesignObject;
var
  h, DebLoc: String;
  RootObjectName: String;
begin
  h := PDebLineInfo.ObjectLocation;
  h := GetNextStrValue(h, '.');

  if AnsiSameStr(h, cParserLibFunct) then
    Result := FOwner.FSystemLibrary

  else
  begin
    RootObjectName := FOwner.DesigningObject.GetPropertyValue('Name');
    if AnsiSameText(h, RootObjectName) then
      DebLoc := PDebLineInfo.ObjectLocation

    else if (PDebLineInfo.ClassName <> '') and (FOwner.DesigningObject.ObjectInheritsFrom(PDebLineInfo.ClassName)) then
    begin
      DebLoc := PDebLineInfo.ObjectLocation;
      GetNextStrValue(DebLoc, '.');
      DebLoc := RootObjectName + '.' + DebLoc;
    end

    else
    begin
      // need to find ANY object of this class
    end;

    Result := ComponentByPath(DebLoc, FOwner.DesigningObject);
  end;
end;


procedure TrmDebugInfoHolder.UpdateVMStatus;
var
  FNewStatus: TrwProcessState;

  procedure ReportStatusChange;
  begin
    case FVMStatus of
      rwVMSTRunning:  PostMessage(FOwner.Handle, WM_RMDEBUG, 2, 0);
      rwVMSTPaused:   PostMessage(FOwner.Handle, WM_RMDEBUG, 1, 0);
    end;
  end;

begin
  FNewStatus := FOwner.FProcessController.GetState;

  if (FNewStatus.VMState <> FVMStatus) or FVMStatusExpired then
  begin
    FVMStatus := FNewStatus.VMState;
    FOwner.FErrorMessage := FNewStatus.ExceptionText;    
    FVMStatusExpired := False;
    ReportStatusChange;
  end;
end;

{ TrmBreakPoints }

function TrmBreakPoints.Add(const AObjectLocation, AFunctionName: String; ASourceCodeLineNbr: Integer): Integer;
var
  BP: PTrmBreakPointInfo;
begin
  New(BP);
  Initialize(BP^);
  BP.ObjectLocation := AObjectLocation;
  BP.FunctionName := AFunctionName;
  BP.SourceCodeLineNbr :=ASourceCodeLineNbr;
  Result := FList.Add(BP);
end;

procedure TrmBreakPoints.Clear;
var
  BP: PTrmBreakPointInfo;
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    BP := PTrmBreakPointInfo(FList[i]);
    Dispose(BP);
  end;

  FList.Clear;

  PostBreakpointsIntoRunningProcess;  
end;

function TrmBreakPoints.Count: Integer;
begin
  Result := FList.Count;
end;

constructor TrmBreakPoints.Create(AOwner: TrmDebugInfoHolder);
begin
  FOwner := AOwner;
  FList := TList.Create;
end;

procedure TrmBreakPoints.Delete(const AIndex: Integer);
var
  BP: PTrmBreakPointInfo;
begin
  BP := PTrmBreakPointInfo(FList[AIndex]);
  Dispose(BP);
  FList.Delete(AIndex);
end;

destructor TrmBreakPoints.Destroy;
begin
  Clear;
  FList.Free;
  inherited;
end;

function TrmBreakPoints.GetItem(Index: Integer): PTrmBreakPointInfo;
begin
  Result := PTrmBreakPointInfo(FList[Index]);
end;

function TrmBreakPoints.IndexOf(const AObjectLocation, AFunctionName: String; ASourceCodeLineNbr: Integer): Integer;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to Count - 1 do
    with Items[i]^ do
      if (ObjectLocation = AObjectLocation) and (AFunctionName = AFunctionName) and (SourceCodeLineNbr = ASourceCodeLineNbr) then
      begin
        Result := i;
        break;
      end;
end;

function TrmBreakPoints.BreakPointByIndex(const AIndex: Integer): TrmBreakPointInfo;
begin
  Result := Items[AIndex]^; 
end;

procedure TrmBreakPoints.ToggleBreakPoint(const AObjectLocation, AFunctionName: String; ASourceCodeLineNbr: Integer);
var
  i: Integer;
begin
  i := IndexOf(AObjectLocation, AFunctionName, ASourceCodeLineNbr);
  if i = -1 then
    SetBreakpointAddress(Add(AObjectLocation, AFunctionName, ASourceCodeLineNbr))
  else
    Delete(i);

  PostBreakpointsIntoRunningProcess;
end;

function TrmBreakPoints.IndexOf(const AAddress: TrwVMAddress): Integer;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to Count - 1 do
    with Items[i]^ do
      if (Address.Segment = AAddress.Segment) and (Address.Offset = AAddress.Offset) then
      begin
        Result := i;
        break;
      end;
end;

procedure TrmBreakPoints.Prepare;
var
  i: Integer;
begin
  // setup breakpoint addresses
  for i := 0 to Count - 1 do
    SetBreakpointAddress(i);
end;

procedure TrmBreakPoints.SetBreakpointAddress(AIndex: Integer);
var
  PBreakPoint: PTrmBreakPointInfo;
  PDebugLine: PTrmDebugLineInfo;
begin
  PBreakPoint := Items[AIndex];
  PDebugLine := FOwner.FindDebugLine(PBreakPoint.ObjectLocation, PBreakPoint.FunctionName, PBreakPoint.SourceCodeLineNbr);

  if Assigned(PDebugLine) then
  begin
    PBreakPoint.SorceCodeLineInfo := PDebugLine;
    PBreakPoint.Address.Segment := PDebugLine.Segment;
    PBreakPoint.Address.Offset := PDebugLine.FirstAddress;
  end

  else
  begin
    PBreakPoint.SorceCodeLineInfo := nil;
    PBreakPoint.Address.Segment := rcsNone;
    PBreakPoint.Address.Offset := 0;
  end;
end;

function TrmBreakPoints.GetAsVariant: Variant;
var
  i: Integer;
  AddrList: TrwVMAddressList;
begin
  SetLength(AddrList, Count);

  for i := 0 to Count - 1 do
    AddrList[i] := PTrmBreakPointInfo(FList[i]).Address;

  Result := VMAddrListToVariant(AddrList);
end;


procedure TrmBreakPoints.PostBreakpointsIntoRunningProcess;
begin
  if FOwner.FOwner.ConnectedToRWEngine then
    FOwner.FOwner.FProcessController.SetBreakpoints(GetAsVariant);
end;

{ TrmWatches }

function TrmWatches.Add(const AExpression: String): Integer;
var
  W: PTrmWatchInfo;
begin
  New(W);
  Initialize(W^);
  W.Expression := AExpression;
  Result := FList.Add(W);
  Recalculate(Result);
end;

procedure TrmWatches.Clear;
var
  W: PTrmWatchInfo;
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    W := Items[i];
    Dispose(W);
  end;

  FList.Clear;
end;

procedure TrmWatches.ClearResults;
var
  W: PTrmWatchInfo;
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    W := Items[i];
    ZeroMemory(Addr(W.Result), SizeOf(W.Result));
  end;
end;

function TrmWatches.Count: Integer;
begin
  Result := FList.Count;
end;

constructor TrmWatches.Create(AOwner: TrmDebugInfoHolder);
begin
  FOwner := AOwner;
  FList := TList.Create;
end;

procedure TrmWatches.Delete(const AIndex: Integer);
var
  W: PTrmWatchInfo;
begin
  W := Items[AIndex];
  Dispose(W);
  FList.Delete(AIndex);
end;

destructor TrmWatches.Destroy;
begin
  Clear;
  FList.Free;
  inherited;
end;

function TrmWatches.GetItem(Index: Integer): PTrmWatchInfo;
begin
  Result := PTrmWatchInfo(FList[Index]);
end;

procedure TrmWatches.Recalculate(const AIndex: Integer);
var
  i: Integer;

  procedure Calc(W: PTrmWatchInfo);
  begin
    if FOwner.FOwner.ConnectedToRWEngine then
      W.Result := FOwner.FOwner.FProcessController.CalcExpression(W.Expression, FOwner.FDebugStopLineInfo.DebugSymbolInfo)

    else
    begin
      ZeroMemory(Addr(W.Result), SizeOf(W.Result));
      W.Result.ResultText := cProcessIsNotAccessible;
    end;
  end;

begin
  if AIndex = -1 then
  begin
    for i := 0 to Count - 1 do
      Calc(Items[i]);
  end

  else
    Calc(Items[AIndex]);
end;

procedure TrmWatches.Update(const AIndex: Integer; const AExpression: String);
var
  W: PTrmWatchInfo;
begin
  W := Items[AIndex];
  W.Expression := AExpression;
  Recalculate(AIndex);
end;

function TrmWatches.WatchByIndex(const AIndex: Integer): TrmWatchInfo;
begin
  Result := Items[AIndex]^;
end;



{ TrwRunReportThread }

constructor TrwRunReportThread.Create(AOwner: TrmDsgnReport);
begin
  FreeOnTerminate := True;
  FOwner := AOwner;
  FReportData  := TEvDualStreamHolder.Create;
 (FOwner.DesigningObject as IrmReportObject).SaveToStream(FReportData .RealStream);
  FStatusThread := TrwStatusThread.Create(AOwner);
  inherited Create(False);
end;

procedure TrwRunReportThread.DoTerminate;
begin
  FStatusThread.Terminate;
  PostMessage(FOwner.Handle, WM_RMDEBUG, 3, 0);  
  inherited;
end;

procedure TrwRunReportThread.Execute;
var
  rwEngine: IrwEngineApp;
  RunInfo: TrwRunReportSettings;
  Rep: IrwReportStructure;
  ReportResult: TrwReportResult;
begin
  try
    rwEngine := FOwner.FEngine;

    rwEngine.OpenReportFromStream(FReportData);
    Rep := rwEngine.GetActiveReport;


    if FOwner.FParamFileName <> '' then
    begin
      FReportData.LoadFromFile(FOwner.FParamFileName);
      Rep.ReportParameters.ParamsFromStream(FReportData);
    end;

    FReportData := nil;

    RunInfo.ReturnParamsBack := False;
    RunInfo.Duplexing := False;
    RunInfo.ShowInputForm := True;
    RunInfo.Suspended := True;

    ReportResult := rwEngine.RunActiveReport(RunInfo);

    if Assigned(ReportResult.Data) then
    begin
      FOwner.FReportResult := ReportResult.Data;
      if FOwner.FReportResult.Size = 0 then
         FOwner.FReportResult := nil;
    end;
  except
    on E: Exception do
      FOwner.FErrorMessage := MakeFullError(E);
  end;
end;

{ TrwStatusThread }

constructor TrwStatusThread.Create(AOwner: TrmDsgnReport);
begin
  FreeOnTerminate := True;
  FOwner := AOwner;
  inherited Create(False);
end;

procedure TrwStatusThread.Execute;
begin
  try
    while not Terminated do
    begin
      Synchronize(SyncUpdateStatus);
      Sleep(100);
    end;

  except
    on E: Exception do
      FOwner.FErrorMessage := ClassName + ': ' + E.Message;
  end;
end;

procedure TrwStatusThread.SyncUpdateStatus;
begin
  // main thread context
  FOwner.FDebugInfoHolder.UpdateVMStatus;
end;

end.
