inherited rmTrwGroupBoxProp: TrmTrwGroupBoxProp
  Height = 162
  inherited pcCategories: TPageControl
    Height = 162
    inherited tsBasic: TTabSheet
      inline frmCaption: TrmOPEStringSingleLine
        Left = 8
        Top = 82
        Width = 290
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 2
        inherited lPropName: TLabel
          Width = 36
          Caption = 'Caption'
        end
      end
    end
    inherited tcAppearance: TTabSheet
      TabVisible = True
      inline frmFont: TrmOPEFont
        Left = 10
        Top = 11
        Width = 75
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
      end
    end
  end
end
