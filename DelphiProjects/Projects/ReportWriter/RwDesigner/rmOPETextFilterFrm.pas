unit rmOPETextFilterFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls, ComCtrls, ExtCtrls, rmTextFilterSelectFrm,
  rwBasicClasses, rmTypes, rwTypes, rwUtils;

type
  TrmdTextFilter = class(TCollectionItem)
  private
    FFunction: TrwFunction;
    FParams: TVarArray;
    procedure SetFunction(AFunction: TrwFunction);
  end;

  TrmdTextFilters = class(TCollection)
  private
    FLinkedObject: IrmDesignObject;
    procedure Initialize(const ALinkedObject: IrmDesignObject);
    procedure ApplyChanges;
  end;



  TrmOPETextFilter = class(TrmObjPropertyEditor)
    Panel1: TPanel;
    lvFilters: TListView;
    pnlParams: TPanel;
    btnAdd: TButton;
    btnDelete: TButton;
    btnUp: TButton;
    btnDown: TButton;
    lHeader2: TLabel;
    procedure btnAddClick(Sender: TObject);
    procedure lvFiltersSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
  private
    FFilters: TrmdTextFilters;
    FCurrentFilter: TrmdTextFilter;
    procedure AddFunctionToList(AFilter: TrmdTextFilter);
    procedure MakeFilterList;
    procedure SyncFilterList;
    procedure BuildFilterParams;
    procedure UpdateFilterParams;
    procedure SetCurrentFilter(AFilter: TrmdTextFilter);
    procedure UpdateParamsText(AListItem: TListItem);
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    procedure ShowPropValue; override;
    procedure ApplyChanges; override;
    procedure ParamValueChanged;
  end;

implementation

uses rmFunctParamFrm;

//rmFMLExprEditorFormFrm, rmFormula;

{$R *.dfm}

{ TrmObjPropertyEditor1 }

procedure TrmrmOPETextFilterpplyChanges;
begin
end;

{ TrmOPETextFilter }

procedure TrmOPETextFilter.AddFunctionToList(AFilter: TrmdTextFilter);
var
  LI: TListItem;
begin
  LI := lvFilters.Items.Add;
  LI.Data := AFilter;
  LI.Caption := AFilter.FFunction.DisplayName;
  LI.SubItems.Add('');
  UpdateParamsText(LI);
end;

procedure TrmOPETextFilter.AfterConstruction;
begin
  inherited;
  FFilters := TrmdTextFilters.Create(TrmdTextFilter);
end;

procedure TrmOPETextFilter.ApplyChanges;
begin
  UpdateFilterParams;
  FFilters.ApplyChanges;
end;

procedure TrmOPETextFilter.BeforeDestruction;
begin
  inherited;
  FreeAndNil(FFilters);
end;

procedure TrmOPETextFilter.btnAddClick(Sender: TObject);
var
  Fnct: TrwFunction;
  Flt: TrmdTextFilter;
begin
  Fnct := SelectTextFilter;
  if Assigned(Fnct) then
  begin
    Flt := TrmdTextFilter(FFilters.Add);
    Flt.SetFunction(Fnct);

    AddFunctionToList(Flt);
    NotifyOfChanges;    
  end;
end;



procedure TrmOPETextFilter.MakeFilterList;
begin
  FCurrentFilter := nil;
  FFilters.Initialize(TComponent(LinkedObject) as IrmDesignObject);
  SyncFilterList;
end;

procedure TrmOPETextFilter.ShowPropValue;
begin
  inherited;
  MakeFilterList;
  if lvFilters.Items.Count > 0 then
    lvFilters.Selected := lvFilters.Items[0]
  else
    lvFilters.OnSelectItem(nil, nil, False);
end;

procedure TrmOPETextFilter.SyncFilterList;
var
  i: Integer;
begin
  lvFilters.Clear;

  for i := 0 to FFilters.Count -1 do
    AddFunctionToList(TrmdTextFilter(FFilters.Items[i]));
end;


procedure TrmOPETextFilter.BuildFilterParams;
var
  i, ParTop: Integer;
  ParFrm: TrmFunctParam;
begin
  pnlParams.DestroyComponents;

  if Assigned(FCurrentFilter) then
  begin
    ParTop := 8;
    for i := 1 to FCurrentFilter.FFunction.Params.Count - 1 do
    begin
      ParFrm := TrmFunctParam.Create(pnlParams);
      ParFrm.Name := 'Param' + IntToStr(i);
      ParFrm.Left := 0;
      ParFrm.Width := pnlParams.Width;
      ParFrm.Top := ParTop;
      ParFrm.Initialize(FCurrentFilter.FFunction.Params[i]);
      ParFrm.Value := FCurrentFilter.FParams[i - 1];      
      ParFrm.Parent := pnlParams;
      Inc(ParTop, ParFrm.Height + 8);
    end;
  end;  
end;

procedure TrmOPETextFilter.SetCurrentFilter(AFilter: TrmdTextFilter);
begin
  UpdateFilterParams;
  FCurrentFilter := AFilter;
  BuildFilterParams;
end;

procedure TrmOPETextFilter.UpdateFilterParams;
var
  i: Integer;
  ParFrm: TrmFunctParam;
begin
  if Assigned(FCurrentFilter) then
  begin
    for i := 0 to pnlParams.ControlCount - 1 do
    begin
      ParFrm := TrmFunctParam(pnlParams.Controls[i]);
      FCurrentFilter.FParams[i] := ParFrm.Value;
    end;

    UpdateParamsText(lvFilters.FindData(0, FCurrentFilter, True, False));
  end;
end;

procedure TrmOPETextFilter.ParamValueChanged;
begin
  NotifyOfChanges;
end;

procedure TrmOPETextFilter.UpdateParamsText(AListItem: TListItem);
var
  i: Integer;
  Flt: TrmdTextFilter;
  Params: String;
begin
  if not Assigned(AListItem) then
    exit;

  Flt := TrmdTextFilter(AListItem.Data);
  Params := '';

  for i := VarArrayLowBound(Flt.FParams, 1) to VarArrayHighBound(Flt.FParams, 1) do
  begin
    if i > 0 then
      Params := Params + ', ';

    Params := Params + Flt.FFunction.Params[i + 1].DomainValueToStr(Flt.FParams[i]);
  end;

  AListItem.SubItems[0] := Params;
end;


{ TrmdTextFilters }

procedure TrmdTextFilters.ApplyChanges;
var
  i, j: Integer;
  AFormula: IrmFMLFormula;
begin
  AFormula := FLinkedObject.GetFormulas.GetFormulaByAction(fatDataFilter, 'Text');

  if Count = 0 then
  begin
    if Assigned(AFormula) then
    begin
      i := AFormula.GetItemIndex;
      AFormula := nil;
      FLinkedObject.GetFormulas.DeleteItem(i);
    end;

    Exit;
  end;

  if not Assigned(AFormula) then
  begin
    AFormula := FLinkedObject.GetFormulas.AddItem as IrmFMLFormula;
    AFormula.SetActionType(fatDataFilter);
    AFormula.SetActionInfo('Text');
  end;

  AFormula.Clear;

  for i := 0 to Count - 1 do
  begin
    AFormula.AddItem(fitFunction, TrmdTextFilter(Items[i]).FFunction.Name, '', 0);
    if i = 0 then
      AFormula.AddItem(fitObject, FLinkedObject.GetPathFromRootOwner);

    for j := Low(TrmdTextFilter(Items[i]).FParams) to High(TrmdTextFilter(Items[i]).FParams) do
    begin
      AFormula.AddItem(fitSeparator, ',');    
      AFormula.AddItem(fitConst, TrmdTextFilter(Items[i]).FParams[j]);
    end;

    AFormula.AddItem(fitBracket, ')');
  end;

// for debugging only!  
//  TrmFMLExprEditorForm.ShowFMLEditor(TrmFMLFormula(AFormula.RealObject).Content, nil);
end;

procedure TrmdTextFilters.Initialize(const ALinkedObject: IrmDesignObject);
var
  AFormula: IrmFMLFormula;
  CurInd: Integer;  

  procedure AddFunction;
  var
    j: Integer;
    RecInf: TrmFMLItemInfoRec;
    Itm: TrmdTextFilter;
    Fnct: TrwFunction;
  begin
    RecInf := AFormula.GetItemInfo(CurInd);
    Fnct := GlobalLocateRWFunction(RecInf.Value);
    Itm := TrmdTextFilter(Add);
    Itm.Index := 0;
    Itm.SetFunction(Fnct);

    Inc(CurInd);
    RecInf := AFormula.GetItemInfo(CurInd);
    if RecInf.ItemType = fitFunction then
    begin
      AddFunction;
      Inc(CurInd);
      RecInf := AFormula.GetItemInfo(CurInd);
    end;

    j := 0;
    while RecInf.ItemType <> fitBracket do
    begin
      if RecInf.ItemType = fitConst then
      begin
        Itm.FParams[j] := RecInf.Value;
        Inc(j);
      end;

      Inc(CurInd);
      RecInf := AFormula.GetItemInfo(CurInd);
    end;
  end;

begin
  FLinkedObject := ALinkedObject;
  AFormula := FLinkedObject.GetFormulas.GetFormulaByAction(fatDataFilter, 'Text');

  Clear;

  if not Assigned(AFormula) or (AFormula.GetItemCount = 0) then
    Exit;

  CurInd := 0;
  AddFunction;
end;

procedure TrmOPETextFilter.lvFiltersSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
begin
  if not Selected then
    SetCurrentFilter(nil)
  else
    SetCurrentFilter(TrmdTextFilter(Item.Data));
end;

procedure TrmOPETextFilter.btnDeleteClick(Sender: TObject);
begin
  if Assigned(FCurrentFilter) then
  begin
    TrmdTextFilter(lvFilters.Selected.Data).Free;
    FCurrentFilter := nil;
    lvFilters.Selected.Free;
    NotifyOfChanges;    
  end;  
end;

procedure TrmOPETextFilter.btnUpClick(Sender: TObject);
var
  i: Integer;
begin
  if Assigned(FCurrentFilter) then
  begin
    if Sender = btnUp then
    begin
      if FCurrentFilter.Index = 0 then
        Exit;
      FCurrentFilter.Index := FCurrentFilter.Index - 1;
    end
    else
    begin
      if FCurrentFilter.Index = FFilters.Count - 1 then
        Exit;
      FCurrentFilter.Index := FCurrentFilter.Index + 1;
    end;

    i := FCurrentFilter.Index;
    SyncFilterList;
    lvFilters.Selected := lvFilters.Items[i];
    NotifyOfChanges;
  end;
end;

{ TrmdTextFilter }

procedure TrmdTextFilter.SetFunction(AFunction: TrwFunction);
var
  i: Integer;
begin
  FFunction := AFunction;
  SetLength(FParams, AFunction.Params.Count - 1);

  for i := 1 to AFunction.Params.Count - 1 do
    FParams[i - 1] := AFunction.Params[i].DefaultValue;
end;

end.
