unit rwTypeCompFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, rwBasicClasses, ImgList, ComCtrls, rwLibCompChoiceFrm;

type
  TrwTypeComp = class(TForm)
    btnOk: TButton;
    Button2: TButton;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    lvComps: TListView;
    imlComps: TImageList;
    procedure lvCompsDblClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FCompClass: TObject;
  public
  end;

function ChoiceComponentType(ACompToolBar: TToolBar): TObject;

implementation

{$R *.DFM}


function ChoiceComponentType(ACompToolBar: TToolBar): TObject;
var
  Frm: TrwTypeComp;
  i: Integer;
  LI: TListItem;
begin
  Result := nil;
  Frm := TrwTypeComp.Create(nil);
  with Frm do
    try
      imlComps.AddImages(ACompToolBar.Images);
      for i := 0 to ACompToolBar.ButtonCount-1 do
      begin
        if (ACompToolBar.Buttons[i].Tag in [1,2]) or
           (ACompToolBar.Buttons[i].Tag = 0) and ACompToolBar.Buttons[i].Visible then
        begin
          LI := lvComps.Items.Add;
          LI.Caption := ACompToolBar.Buttons[i].Hint;
          LI.ImageIndex := ACompToolBar.Buttons[i].ImageIndex;
          LI.Data := GetClass(ACompToolBar.Buttons[i].Caption);
        end;
      end;

      if Frm.ShowModal = mrOK then
        Result := FCompClass ;
    finally
      Frm.Free;
    end;
end;

procedure TrwTypeComp.lvCompsDblClick(Sender: TObject);
begin
  btnOK.Click;
end;

procedure TrwTypeComp.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult = mrOK then
    if not Assigned(lvComps.Selected.Data) then
    begin
      FCompClass := ChoiceLibComp([rctReportComp, rctInputFormComp, rctNoVisualComp]);
      CanClose := Assigned(FCompClass);
    end
    else
      FCompClass := lvComps.Selected.Data;
end;

end.
