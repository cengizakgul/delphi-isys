unit rmTrmXMLNodePropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, rmOPEBooleanFrm, StdCtrls, ExtCtrls,
  rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, ComCtrls;

type
  TrmTrmXMLNodeProp = class(TrmOPBasic)
    frmBlockParentIfEmpty: TrmOPEBoolean;
    frmInactive: TrmOPEBoolean;
  protected
    procedure GetPropsFromObject; override;
  public
  end;


implementation

{$R *.dfm}

{ TrmOPBasic1 }

procedure TrmTrmXMLNodeProp.GetPropsFromObject;
begin
  inherited;
  frmBlockParentIfEmpty.PropertyName := 'BlockParentIfEmpty';
  frmBlockParentIfEmpty.ShowPropValue;

  frmInactive.PropertyName := 'Enabled';
  frmInactive.InvertedValue := True;;
  frmInactive.ShowPropValue;
end;

end.
