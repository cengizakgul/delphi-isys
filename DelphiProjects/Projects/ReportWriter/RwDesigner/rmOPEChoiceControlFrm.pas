unit rmOPEChoiceControlFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, rmTypes, ExtCtrls;

type
  TrmOPEChoiceControl = class(TrmObjPropertyEditor)
    pnlChoice: TPanel;
  private
  public
    procedure ShowPropValue; override;
    procedure ApplyChanges; override;
  end;


implementation

{$R *.dfm}


uses rmOPEBlockingControlFrm;

{ TrmOPEChoiceControl }

procedure TrmOPEChoiceControl.ApplyChanges;
var
  i: Integer;
  Blck: TrmOPEBlockingControl;
begin
  inherited;

  if FEnabled then
    for i := 0 to pnlChoice.ControlCount - 1 do
      if pnlChoice.Controls[i] is TrmOPEBlockingControl then
      begin
        Blck := TrmOPEBlockingControl(pnlChoice.Controls[i]);
        Blck.ApplyChanges;
      end;
end;

procedure TrmOPEChoiceControl.ShowPropValue;
var
  Own, O: IrmDesignObject;
  i, h: Integer;
  Blck: TrmOPEBlockingControl;
  s: String;
begin
  inherited;

  if FEnabled then
  begin
    Own := (TComponent(LinkedObject) as IrmDesignObject);

    while pnlChoice.ControlCount > 0 do
      pnlChoice.Controls[pnlChoice.ControlCount - 1].Free;

    h := 0;
    for i := 0 to Own.GetChildrenCount - 1 do
    begin
      O := Own.GetChildByIndex(i);
      if O.ObjectInheritsFrom('TrmXMLanyElement') then
      begin
        s := O.GetPropertyValue('Name');
        Blck := TrmOPEBlockingControl.Create(Self);
        Blck.Name := s;
        Blck.Top := h;
        Blck.Height := 55;
        Blck.Align := alTop;
        Blck.lCaption.Caption := s;
        Blck.Parent := pnlChoice;
        Blck.BlockID := 'BranchCalc';
        Blck.LinkedObject := O.RealObject;

        h := h + Blck.Height;

        Blck.ShowPropValue;
      end;
    end;

    pnlChoice.Height := h + 5;
  end;  
end;

end.
