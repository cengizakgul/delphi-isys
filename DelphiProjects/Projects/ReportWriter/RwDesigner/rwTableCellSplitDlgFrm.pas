unit rwTableCellSplitDlgFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin;

type
  TrwTableCellSplitDlg = class(TForm)
    Label1: TLabel;
    edCols: TSpinEdit;
    Label2: TLabel;
    edRows: TSpinEdit;
    btnOK: TButton;
    btnCancel: TButton;
  private
  public
  end;

  function ShowTableCellSplitDlg(AOwner: TComponent; var ACols, ARows: Integer): Boolean;

implementation

{$R *.dfm}

function ShowTableCellSplitDlg(AOwner: TComponent; var ACols, ARows: Integer): Boolean;
begin
  with TrwTableCellSplitDlg.Create(AOwner) do
    try
      edCols.Value := ACols;
      edRows.Value := ARows;

      Result := ShowModal = mrOK;
      if Result then
      begin
        ACols := edCols.Value;
        ARows := edRows.Value;
      end;
    finally
      Free;
    end;
end;

end.
