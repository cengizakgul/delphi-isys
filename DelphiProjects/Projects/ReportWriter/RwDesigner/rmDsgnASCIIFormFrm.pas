unit rmDsgnASCIIFormFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDsgnAreaFrm, rmDsgnTypes, ExtCtrls, ComCtrls, rmTypes, rwBasicClasses,
  rmLibCompChooseFrm, rwUtils, Menus, rwDsgnUtils, rwRTTI;

type
  TrmDsgnASCIIForm = class(TrmDsgnArea)
    pmTrmASCIIForm: TPopupMenu;
    miAddASCIIRecord: TMenuItem;
    pmTrmASCIIRecord: TPopupMenu;
    miAddASCIIField: TMenuItem;
    miAddSubDetailRecord: TMenuItem;
    pmTrmASCIIField: TPopupMenu;
    miAddASCIIField2: TMenuItem;
    procedure FrameResize(Sender: TObject);
    procedure miAddASCIIRecordClick(Sender: TObject);
    procedure miAddASCIIFieldClick(Sender: TObject);
    procedure miAddSubDetailRecordClick(Sender: TObject);
    procedure miAddASCIIField2Click(Sender: TObject);
  private
  public
    constructor Create(AOwner: TComponent); override;
    function CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject; override;
  end;


implementation

{$R *.dfm}

{ TrmDsgnASCIIForm }

constructor TrmDsgnASCIIForm.Create(AOwner: TComponent);
begin
  inherited;
  FImageIndex := 5;
  FAreaType := rmDATASCIIForm;
end;

procedure TrmDsgnASCIIForm.FrameResize(Sender: TObject);
begin
  inherited;

  if Assigned(DesignObject) then
    DesignObject.Notify(rmdZoomChanged);
end;

function TrmDsgnASCIIForm.CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject;
var
  LibComp: IrmComponent;
  Cl: IrwClass;
begin
  if SameText(AClassName, 'LIBRARY') then
  begin
    LibComp := ChoiceLibComp(['TrmASCIIItem', 'TrwNoVisualComponent'], AOwner, Self);
    if Assigned(LibComp) then
      AClassName := LibComp.GetName
    else
      Exit;
  end;

  if not Assigned(AOwner) then
    AOwner := DesignObject;

  Cl := rwRTTIInfo.FindClass(AClassName);

  if AOwner.ObjectInheritsFrom('TrmASCIIForm') and Cl.DescendantOf('TrmASCIIField') then
  begin
    AOwner := AOwner.CreateChildObject('TrmASCIIRecord');
    AOwner.CreateChildObject(AClassName);
    Result := AOwner;
  end
  else
    Result := AOwner.CreateChildObject(AClassName);
end;

procedure TrmDsgnASCIIForm.miAddASCIIRecordClick(Sender: TObject);
var
  O: IrmDesignObject;
begin
  O := DesignObject.CreateChildObject('TrmASCIIRecord');
  RMDesigner.GetComponentDesigner.UnselectAll;
  RMDesigner.GetComponentDesigner.SelectObject(O);
end;

procedure TrmDsgnASCIIForm.miAddASCIIFieldClick(Sender: TObject);
var
  O: IrmDesignObject;
begin
  O := RMDesigner.GetComponentDesigner.GetSelectedObjects[0];
  O := O.CreateChildObject('TrmASCIIField');
  RMDesigner.GetComponentDesigner.UnselectAll;
  RMDesigner.GetComponentDesigner.SelectObject(O);
end;

procedure TrmDsgnASCIIForm.miAddSubDetailRecordClick(Sender: TObject);
var
  O: IrmDesignObject;
begin
  O := RMDesigner.GetComponentDesigner.GetSelectedObjects[0];
  O := O.CreateChildObject('TrmASCIIRecord');
  RMDesigner.GetComponentDesigner.UnselectAll;
  RMDesigner.GetComponentDesigner.SelectObject(O);
end;

procedure TrmDsgnASCIIForm.miAddASCIIField2Click(Sender: TObject);
var
  O, Fld: IrmDesignObject;
begin
  O := RMDesigner.GetComponentDesigner.GetSelectedObjects[0];
  Fld := O.GetParent.CreateChildObject('TrmASCIIField');
  Fld.SetPropertyValue('ItemIndex', O.GetPropertyValue('ItemIndex') + 1);
  RMDesigner.GetComponentDesigner.UnselectAll;
  RMDesigner.GetComponentDesigner.SelectObject(Fld);
end;

end.
