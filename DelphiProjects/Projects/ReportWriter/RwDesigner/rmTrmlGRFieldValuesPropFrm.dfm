inherited rmTrmlGRFieldValuesProp: TrmTrmlGRFieldValuesProp
  Height = 275
  inherited pcCategories: TPageControl
    Height = 275
    inherited tsBasic: TTabSheet
      object grbDDInfo: TGroupBox
        Left = 8
        Top = 79
        Width = 293
        Height = 91
        Caption = 'Data Dictionary Info'
        TabOrder = 2
        inline frmTable: TrmOPEEnum
          Left = 10
          Top = 23
          Width = 270
          Height = 21
          AutoScroll = False
          AutoSize = True
          TabOrder = 0
          inherited lPropName: TLabel
            Caption = 'Table'
          end
          inherited cbList: TComboBox
            Width = 215
            OnChange = frmTablecbListChange
          end
        end
        inline frmField: TrmOPEEnum
          Left = 10
          Top = 56
          Width = 270
          Height = 21
          AutoScroll = False
          AutoSize = True
          TabOrder = 1
          inherited lPropName: TLabel
            Width = 22
            Caption = 'Field'
          end
          inherited cbList: TComboBox
            Width = 215
          end
        end
      end
      inline frmColumnTitle: TrmOPEStringSingleLine
        Left = 8
        Top = 189
        Width = 292
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 3
        inherited lPropName: TLabel
          Width = 58
          Caption = 'Column Title'
        end
        inherited edString: TEdit
          Width = 202
        end
      end
    end
    inherited tcAppearance: TTabSheet
      TabVisible = False
    end
  end
end
