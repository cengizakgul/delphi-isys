unit rmObjectTreeViewerFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmChildFormFrm, rmObjectTreeFrm, rwDsgnUtils,
  rmDsgnTypes, rmDockableFrameFrm, rmCustomFrameFrm, rmTypes;

type
  TrmObjectTreeViewer = class(TrmDockableFrame)
    frmObjectTree: TrmObjectTree;
  private
    procedure DMSelectedObjectListChanged(var Message: TrmMessageRec); message mSelectedObjectListChanged;
    procedure DMObjectChildrenListChanged(var Message: TrmMessageRec); message mObjectChildrenListChanged;
    procedure DMDesignObjectChanged(var Message: TrmMessageRec); message mDesignObjectChanged;
    procedure DMObjectPropertyChanged(var Message: TrmMessageRec); message mObjectPropertyChanged;

    procedure ShowSelectedObject;
    procedure HookRootObject;

  protected
    procedure BeforeShow; override;
    procedure GetListeningMessages(const AMessages: TList); override;
    procedure DoRefresh; override;
  end;

implementation

{$R *.dfm}


{ TrmObjectTreeViewer }

procedure TrmObjectTreeViewer.BeforeShow;
begin
  Caption := 'Objects Tree';
  HookRootObject;
  inherited;
end;

procedure TrmObjectTreeViewer.DMDesignObjectChanged(var Message: TrmMessageRec);
begin
  inherited;
  frmObjectTree.RootObject := nil;
  HookRootObject;
end;

procedure TrmObjectTreeViewer.DMObjectChildrenListChanged( var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;

procedure TrmObjectTreeViewer.DMObjectPropertyChanged(
  var Message: TrmMessageRec);
var
  O: IrmDesignObject;
  i : integer;
begin
  inherited;
  O := IrmDesignObject(Pointer(Integer(Message.Parameter1)));
  for i := 0 to frmObjectTree.tvObjects.Items.Count - 1 do
    if ObjectsAreTheSame(IrmDesignObject(frmObjectTree.tvObjects.Items[i].Data), O) then
    begin
      frmObjectTree.tvObjects.Items[i].Text := O.GetPropertyValue('Name');
      break;
    end;
end;

procedure TrmObjectTreeViewer.DMSelectedObjectListChanged(var Message: TrmMessageRec);
begin
  inherited;
  ShowSelectedObject;
end;

procedure TrmObjectTreeViewer.DoRefresh;
begin
  inherited;
  frmObjectTree.Refresh;
  ShowSelectedObject;
end;

procedure TrmObjectTreeViewer.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mSelectedObjectListChanged));
  AMessages.Add(Pointer(mObjectChildrenListChanged));
  AMessages.Add(Pointer(mDesignObjectChanged));
  AMessages.Add(Pointer(mObjectPropertyChanged));
end;

procedure TrmObjectTreeViewer.HookRootObject;
begin
  frmObjectTree.RootObject := (Apartment as IrmDsgnComponent).DesigningObject;

  if frmObjectTree.tvObjects.TopItem <> nil then
    frmObjectTree.tvObjects.TopItem.Expand(False);
end;

procedure TrmObjectTreeViewer.ShowSelectedObject;
var
  SelObjects: TrmListOfObjects;
begin
  SelObjects := (RMDesigner.GetActiveApartment as IrmDsgnComponent).GetSelectedObjects;

  if Length(SelObjects) = 1 then
    frmObjectTree.RWObject := SelObjects[0]
  else
    frmObjectTree.RWObject := nil;
end;

initialization
  RegisterClass(TrmObjectTreeViewer);

end.
