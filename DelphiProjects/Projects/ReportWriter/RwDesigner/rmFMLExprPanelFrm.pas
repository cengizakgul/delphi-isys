unit rmFMLExprPanelFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, rmFormula, Menus, Clipbrd, rwDsgnUtils, rmTypes,
  rwUtils, rwBasicUtils, rwTypes;

type
  TrmFMLExprItemLabel = class;

  TrmFMLExprPanel = class(TFrame)
    pnlCanvas: TPanel;
    pmClipbrd: TPopupMenu;
    miCopy: TMenuItem;
    miPaste: TMenuItem;
    procedure pnlCanvasClick(Sender: TObject);
    procedure pnlCanvasDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure FrameResize(Sender: TObject);
    procedure pmClipbrdPopup(Sender: TObject);
    procedure miCopyClick(Sender: TObject);
    procedure miPasteClick(Sender: TObject);
  private
    FBlinkTimer: TTimer;
    FExpression: TrmFMLFormulaContent;
    FSelectedItem: TrmFMLExprItemLabel;
    FSelectedItemPair: TrmFMLExprItemLabel;
    FOnSelectItem: TNotifyEvent;
    FReadOnly: Boolean;
    FRedrawing: Boolean;
    FCurDragOverItem: TrmFMLExprItemLabel;
    procedure SetExpression(const Value: TrmFMLFormulaContent);
    procedure ItemMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ItemMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ItemEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure ItemCancelDrag(Sender, Target: TObject; X, Y: Integer);
    procedure ItemDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure CNKeyDown(var Message: TWMKeyDown); message CN_KEYDOWN;
    procedure SetSelectedItem(const Value: TrmFMLExprItemLabel);
    procedure SetReadOnly(const Value: Boolean);
    procedure OnBlinkTimer(Sender: TObject);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    procedure ReDraw;
    function  FindPairBracket(AItemIndex: Integer): TrmFMLExprItemLabel;
    function  FindFunction(AItemIndex: Integer): TrmFMLExprItemLabel;
    function  ItemByData(AExprItem: TrmFMLFormulaItem): TrmFMLExprItemLabel;
    property  Expression: TrmFMLFormulaContent read FExpression write SetExpression;
    property  SelectedItem: TrmFMLExprItemLabel read FSelectedItem write SetSelectedItem;
    property  CurDragOverItem: TrmFMLExprItemLabel read FCurDragOverItem write FCurDragOverItem;
    property  OnSelectItem: TNotifyEvent read FOnSelectItem write FOnSelectItem;
    property  ReadOnly: Boolean read FReadOnly write SetReadOnly;
  end;


  TrmFMLExprItemLabel = class(TLabel)
  private
    FExprItem: TrmFMLFormulaItem;
    procedure SetExprItem(const Value: TrmFMLFormulaItem);
    function  GetSelected: Boolean;
    procedure SetSelected(const Value: Boolean);
  public
    constructor Create(AOwner: TComponent); override;
    procedure ReDraw;

    property ExprItem: TrmFMLFormulaItem read FExprItem write SetExprItem;
    property Selected: Boolean read GetSelected write SetSelected;
  end;

implementation

{$R *.dfm}

uses rmActionFormFrm;

procedure TrmFMLExprPanel.CNKeyDown(var Message: TWMKeyDown);
begin
  if not FReadOnly and Assigned(FSelectedItem) then
    case Message.CharCode of
      VK_LEFT:
        begin
          if SelectedItem.ComponentIndex > 0 then
            SelectedItem := TrmFMLExprItemLabel(pnlCanvas.Components[SelectedItem.ComponentIndex - 1]);
        end;

      VK_RIGHT:
        begin
          if SelectedItem.ComponentIndex < pnlCanvas.ComponentCount - 1 then
            SelectedItem := TrmFMLExprItemLabel(pnlCanvas.Components[SelectedItem.ComponentIndex + 1]);
        end;
    end;

  inherited;
end;

constructor TrmFMLExprPanel.Create(AOwner: TComponent);
begin
  inherited;
  FReadOnly := True;
  FBlinkTimer := nil;
end;

destructor TrmFMLExprPanel.Destroy;
begin
  if Assigned(FBlinkTimer) then
  begin
    FBlinkTimer.Enabled := False;
    FreeAndNil(FBlinkTimer);
  end;
  inherited;
end;

function TrmFMLExprPanel.FindFunction(AItemIndex: Integer): TrmFMLExprItemLabel;
var
  i, lBrCount: Integer;
  L: TrmFMLExprItemLabel;
begin
  Result := nil;

  if TrmFMLExprItemLabel(pnlCanvas.Components[AItemIndex]).Caption <> ',' then
    Exit;

  lBrCount := 0;
  i := AItemIndex - 1;
  while i > 0 do
  begin
    L := TrmFMLExprItemLabel(pnlCanvas.Components[i]);
    if Assigned(L.ExprItem) and (L.ExprItem.ItemType = fitBracket) then
    begin
      if L.Caption = ')' then
        Dec(lBrCount)
      else
        Inc(lBrCount);
    end

    else if Assigned(L.ExprItem) and (L.ExprItem.ItemType = fitFunction) and (lBrCount = 0) then
    begin
      Result := L;
      break;
    end;

    Dec(i);
  end;
end;

function TrmFMLExprPanel.FindPairBracket(AItemIndex: Integer): TrmFMLExprItemLabel;
var
  i, lLimit,
  ADirect,
  lBrCount: Integer;
  L: TrmFMLExprItemLabel;
begin
  Result := nil;

  if (TrmFMLExprItemLabel(pnlCanvas.Components[AItemIndex]).Caption = '(') or
     (Assigned(TrmFMLExprItemLabel(pnlCanvas.Components[AItemIndex]).ExprItem) and
          (TrmFMLExprItemLabel(pnlCanvas.Components[AItemIndex]).ExprItem.ItemType = fitFunction)) then
  begin
    ADirect := 1;
    lLimit := pnlCanvas.ComponentCount - 1;
  end

  else if TrmFMLExprItemLabel(pnlCanvas.Components[AItemIndex]).Caption = ')' then
  begin
    ADirect := -1;
    lLimit := 0;
  end

  else
    Exit;

  lBrCount := 1;
  i := AItemIndex + ADirect;
  while i <> lLimit do
  begin
    L := TrmFMLExprItemLabel(pnlCanvas.Components[i]);
    if Assigned(L.ExprItem) and (L.ExprItem.ItemType in [fitBracket, fitFunction]) then
    begin
      if (L.Caption = '(') or
         (Assigned(L.ExprItem) and (L.ExprItem.ItemType = fitFunction)) then
        Inc(lBrCount, ADirect)
      else
        Dec(lBrCount, ADirect);

      if lBrCount = 0 then
      begin
        Result := L;
        break;
      end;
    end;

    Inc(i, ADirect);
  end;
end;

function TrmFMLExprPanel.ItemByData(AExprItem: TrmFMLFormulaItem): TrmFMLExprItemLabel;
var
  i: Integer;
  L: TrmFMLExprItemLabel;
begin
  Result := nil;
  for i := 0 to pnlCanvas.ComponentCount - 1 do
  begin
    L := TrmFMLExprItemLabel(pnlCanvas.Components[i]);
    if Assigned(L.ExprItem) and (L.ExprItem = AExprItem) then
    begin
      Result := L;
      break;
    end;
  end;
end;

procedure TrmFMLExprPanel.ItemCancelDrag(Sender, Target: TObject; X, Y: Integer);
begin
  if Assigned(FCurDragOverItem) then
  begin
    FCurDragOverItem.Selected := False;
    FCurDragOverItem := nil;
    CancelDrag;
  end;
end;

procedure TrmFMLExprPanel.ItemDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := not ReadOnly and ((Source is TrmFMLExprItemLabel) or (Source is TrmDragAndDropObject));

  if Source is TrmDragAndDropObject then
    SelectedItem := nil;

  if Accept then
  begin
    Accept := True;
    if Assigned(FCurDragOverItem) then
    begin
      FCurDragOverItem.Selected := False;
      FCurDragOverItem := nil;
    end;

    if Sender <> Source then
    begin
      FCurDragOverItem := TrmFMLExprItemLabel(Sender);
      FCurDragOverItem.Selected := True;
    end;
  end;
end;

procedure TrmFMLExprPanel.ItemEndDrag(Sender, Target: TObject; X,  Y: Integer);
var
  i, j: Integer;
  ExprItm, PairItem: TrmFMLFormulaItem;
  L: TrmFMLExprItemLabel;
begin
  SelectedItem.EndDrag(True);

  FCurDragOverItem := nil;
  ExprItm := TrmFMLExprItemLabel(Sender).ExprItem;

  if Target is TrmFMLExprItemLabel then
    if Assigned(TrmFMLExprItemLabel(Target).ExprItem) then
      i := TrmFMLExprItemLabel(Target).ExprItem.Index
    else
      if TrmFMLExprItemLabel(Target).ComponentIndex = pnlCanvas.ComponentCount - 1 then
        i := Expression.Count - 1
      else
      begin
        i := TrmFMLExprItemLabel(pnlCanvas.Components[TrmFMLExprItemLabel(Target).ComponentIndex + 1]).ExprItem.Index;
        if i > ExprItm.Index then
          Dec(i);
        if i = Expression.Count - 1 then
          Dec(i);
      end

  else if Target = pnlCanvas then
    i := Expression.Count - 1

  else
    Exit;

  j := ExprItm.Index;
  PairItem := nil;
  if ExprItm.ItemType in [fitBracket, fitFunction] then
  begin
    L := FindPairBracket(SelectedItem.ComponentIndex);
    if Assigned(L) then
      PairItem := L.ExprItem;
  end;

  ExprItm.Index := i;
  ReDraw;

  if ExprItm.ItemType in [fitBracket, fitFunction] then
  begin
    if not Assigned(FindPairBracket(SelectedItem.ComponentIndex)) then
    begin
      ExprItm.Index := j;
      ReDraw;
    end;

    if Assigned(PairItem) then
    begin
      L := ItemByData(PairItem);
      if not Assigned(FindPairBracket(L.ComponentIndex)) then
      begin
        ExprItm.Index := j;
        ReDraw;
      end;
    end;
  end;

  SetFocus;
end;

procedure TrmFMLExprPanel.ItemMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if not FReadOnly and (Button = mbLeft) then
  begin
    FCurDragOverItem := nil;
    SelectedItem := TrmFMLExprItemLabel(Sender);
    if Assigned(SelectedItem.ExprItem) then
    begin
      SelectedItem.Selected := True;
      SelectedItem.BeginDrag(False, 1);
    end;
    SetFocus;
  end;
end;

procedure TrmFMLExprPanel.ItemMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if FReadOnly and (Button = mbLeft) and Assigned(pnlCanvas.OnClick) then
    pnlCanvas.OnClick(pnlCanvas);
end;

procedure TrmFMLExprPanel.OnBlinkTimer(Sender: TObject);
begin
  if not HandleAllocated or SelectedItem.Dragging then
    Exit;

  if Assigned(FSelectedItem) then
    FSelectedItem.Selected := not FSelectedItem.Selected;
  if Assigned(FSelectedItemPair) then
    FSelectedItemPair.Selected := not FSelectedItemPair.Selected;
end;

procedure TrmFMLExprPanel.pnlCanvasClick(Sender: TObject);
begin
  if not ReadOnly then
  begin
    if pnlCanvas.ComponentCount > 0 then
      SelectedItem := TrmFMLExprItemLabel(pnlCanvas.Components[pnlCanvas.ComponentCount - 1]);
    SetFocus;
  end;
end;

{ TrmFMLExprItemLabel }

constructor TrmFMLExprItemLabel.Create(AOwner: TComponent);
begin
  inherited;
  ShowAccelChar := False;
  DragMode := dmManual;
end;

function TrmFMLExprItemLabel.GetSelected: Boolean;
begin
  Result := (Color <> clWhite);
end;

procedure TrmFMLExprItemLabel.ReDraw;
var
  C: IrmDesignObject;
  h: String;
  Fld: IrwQBShowingField;
  vfHolder: IrmVarFunctHolder;
  v: IrmVariable;

  procedure WrongObject(const AObjName: String);
  begin
    FExprItem.ItemType := fitError;
    FExprItem.Value := AObjName;
    Redraw;
  end;

begin
  Color := clWhite;
  if Assigned(Owner) then
    Font := TPanel(Owner).Font
  else
  begin
    Font.Name := 'MS Sans Serif';
    Font.Size := 10;
    Font.Style := [];
  end;

  if not Assigned(FExprItem) then
  begin
    Caption := ' ';
    exit;
  end;


  case FExprItem.ItemType of
    fitNone:      begin
                    Font.Color := clWhite;
                    Caption := '';
                  end;

    fitError:     begin
                    Font.Color := clRed;
                    Font.Style := [fsBold];
                    Caption := FExprItem.Value;
                  end;

    fitSeparator: begin
                    Font.Color := clGray;
                    Caption := FExprItem.Value;
                  end;

    fitOperation: begin
                    Font.Style := [fsBold];
                    Font.Color := clMaroon;
                    Caption := FExprItem.Value;
                  end;

    fitObject:    begin
                    Font.Color := clBlack;
                    if AnsiSameText(VarToStr(FExprItem.Value), cParserSelf) then
                      Caption := 'Self.' + FExprItem.AddInfo
                    else
                    begin
                      C := ComponentByPath(VarToStr(FExprItem.Value), RMDesigner.GetComponentDesigner.DesigningObject);
                      if Assigned(C) then
                        Caption := C.GetPropertyValue('Name') + '.' + FExprItem.AddInfo
                      else
                        WrongObject(VarToStr(FExprItem.Value));
                    end;
                  end;

    fitField:    begin
                    Font.Color := clBlack;
                    Fld := FExprItem.GetQBField(RMDesigner.GetComponentDesigner.DesigningObject);
                    if Assigned(Fld) then
                      Caption := Fld.GetName
                    else
                      WrongObject(h);
                  end;

    fitBracket:  begin
                    Font.Color := clGreen;
                    Caption := FExprItem.Value;
                 end;

    fitFunction: begin
                    Font.Color := clGreen;
                    Caption := FExprItem.Value + '(';
                 end;

    fitVariable: begin
                   Font.Color := clTeal;
                   vfHolder := RMDesigner.GetComponentDesigner.DesigningObject.GetVarFunctHolder;
                   if not Assigned(vfHolder) then
                     WrongObject(FExprItem.Value)
                   else
                   begin
                     v := vfHolder.GetVariables.GetVariableByName(FExprItem.Value);
                     if not Assigned(v) or not v.GetVarInfo.ShowInFmlEditor then
                       WrongObject(FExprItem.Value)
                     else
                     begin
                       h := v.GetVarInfo.DisplayName;
                       if h = '' then
                         h := FExprItem.Value;
                       Caption := h;  
                     end;
                   end;
                 end;

    fitConst:    begin
                   case VarType(FExprItem.Value) of
                     varDate,
                     varInteger,
                     varDouble:
                       begin
                         Caption := VarToStr(FExprItem.Value);
                         Font.Color := clFuchsia;
                       end;

                     varString:
                       begin
                         Font.Color := clBlue;
                         Caption := '''' + VarToStr(FExprItem.Value) + '''';
                       end;

                     varBoolean:
                       begin
                         Font.Color := clFuchsia;
                         if FExprItem.Value then
                           Caption := 'True'
                         else
                           Caption := 'False';
                       end;

                     varNull:
                       begin
                         Font.Color := clFuchsia;
                         Caption := 'Null';
                       end;
                   else
                     WrongObject('UNKNOWN CONSTANT');
                   end;  
                 end;
  end;
end;

procedure TrmFMLExprItemLabel.SetExprItem(const Value: TrmFMLFormulaItem);
begin
  FExprItem := Value;
  ReDraw;
end;

procedure TrmFMLExprItemLabel.SetSelected(const Value: Boolean);
begin
  if Selected <> Value then
    if Value then
    begin
      Font.Color := clYellow;
      Color := clNavy;
    end
    else
      SetExprItem(ExprItem);
end;

procedure TrmFMLExprPanel.ReDraw;
var
  i, x, y: Integer;
  ExprItm: TrmFMLFormulaItem;

  procedure AddLabel(AItem: TrmFMLFormulaItem);
  var
    L: TrmFMLExprItemLabel;
  begin
    L := TrmFMLExprItemLabel.Create(pnlCanvas);
    L.PopupMenu := PopupMenu;
    L.OnMouseDown := ItemMouseDown;
    L.OnMouseUp := ItemMouseUp;
    if not FReadOnly and Assigned(AItem) and not (AItem.ItemType = fitSeparator) then
      L.OnEndDrag := ItemEndDrag
    else
      L.OnEndDrag := ItemCancelDrag;

    L.OnDragOver := ItemDragOver;

    L.ExprItem := AItem;
    if x + L.Width  > pnlCanvas.ClientWidth then
    begin
      x := 0;
      y := y + 17;
    end;

    L.Left := x;
    L.Top := y;
    x := x + L.Width;

    L.Parent := pnlCanvas;

    if Assigned(ExprItm) and (ExprItm = AItem) then
      FSelectedItem := L;
  end;

begin
  if FRedrawing then
    Exit;

  FRedrawing := True;
  try
    if not FReadOnly then
      FBlinkTimer.Enabled := False;

    if Assigned(SelectedItem) then
      ExprItm := SelectedItem.ExprItem
    else
      ExprItm := nil;

    SelectedItem := nil;
    pnlCanvas.DestroyComponents;

    if not Assigned(FExpression) then
      Exit;

    x := 0;
    y := 0;
    for i := 0 to FExpression.Count - 1 do
    begin
      if not FReadOnly then
        AddLabel(nil);
      AddLabel(TrmFMLFormulaItem(FExpression.Items[i]));
    end;

    if not FReadOnly then
      AddLabel(nil);

    SetSelectedItem(FSelectedItem);

    if not FReadOnly then
      FBlinkTimer.Enabled := True;

  finally
    FRedrawing := False;
  end;
end;

procedure TrmFMLExprPanel.SetExpression(const Value: TrmFMLFormulaContent);
begin
  FExpression := Value;
  ReDraw;
end;

procedure TrmFMLExprPanel.SetReadOnly(const Value: Boolean);
begin
  FReadOnly := Value;
  if FReadOnly and Assigned(FBlinkTimer) then
    FreeAndNil(FBlinkTimer)

  else if not FReadOnly and not Assigned(FBlinkTimer) then
  begin
    FBlinkTimer := TTimer.Create(nil);
    FBlinkTimer.OnTimer := OnBlinkTimer;
    FBlinkTimer.Interval := GetCaretBlinkTime;
  end;

  ReDraw;
end;

procedure TrmFMLExprPanel.SetSelectedItem(const Value: TrmFMLExprItemLabel);
begin
  if not FReadOnly then
    FBlinkTimer.Enabled := False;

  if Assigned(FSelectedItem) then
    FSelectedItem.Selected := False;

  if Assigned(FSelectedItemPair) then
  begin
    FSelectedItemPair.Selected := False;
    FSelectedItemPair := nil;
  end;

  FSelectedItem := Value;
  if Assigned(FSelectedItem) then
  begin
    FSelectedItem.Selected := True;
    if Assigned(FSelectedItem.ExprItem) and (FSelectedItem.ExprItem.ItemType in [fitBracket, fitFunction]) then
    begin
      FSelectedItemPair := FindPairBracket(FSelectedItem.ComponentIndex);
      if Assigned(FSelectedItemPair) then
        FSelectedItemPair.Selected := True;
    end;
  end;

  if Assigned(FOnSelectItem) then
    FOnSelectItem(Self);

  if not FReadOnly then
    FBlinkTimer.Enabled := True;
end;

procedure TrmFMLExprPanel.pnlCanvasDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := not ReadOnly and ((Source is TrmFMLExprItemLabel) or (Source is TrmDragAndDropObject));

  if Source is TrmDragAndDropObject then
    SelectedItem := nil;

  if Accept and Assigned(FCurDragOverItem) then
  begin
    FCurDragOverItem.Selected := False;
    FCurDragOverItem := nil;
  end;
end;

procedure TrmFMLExprPanel.FrameResize(Sender: TObject);
begin
  ReDraw;
end;

procedure TrmFMLExprPanel.pmClipbrdPopup(Sender: TObject);
begin
  miPaste.Enabled := not ReadOnly and Clipboard.HasFormat(CF_FMLEXPRESSION);
end;

procedure TrmFMLExprPanel.miCopyClick(Sender: TObject);
begin
  if Assigned(Expression) then
    PutToClipboard(CF_FMLEXPRESSION, Expression.ContentToString);
end;

procedure TrmFMLExprPanel.miPasteClick(Sender: TObject);
begin
  Expression.ContentFromString(GetFromClipboard(CF_FMLEXPRESSION));
  ReDraw;
end;

end.
