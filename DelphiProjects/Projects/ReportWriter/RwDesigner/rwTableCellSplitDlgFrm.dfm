object rwTableCellSplitDlg: TrwTableCellSplitDlg
  Left = 471
  Top = 274
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Cell Splitting'
  ClientHeight = 117
  ClientWidth = 182
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 94
    Height = 13
    Caption = 'Quantity of Columns'
  end
  object Label2: TLabel
    Left = 8
    Top = 48
    Width = 81
    Height = 13
    Caption = 'Quantity of Rows'
  end
  object edCols: TSpinEdit
    Left = 112
    Top = 12
    Width = 65
    Height = 22
    MaxValue = 10000
    MinValue = 1
    TabOrder = 0
    Value = 2
  end
  object edRows: TSpinEdit
    Left = 112
    Top = 44
    Width = 65
    Height = 22
    MaxValue = 10000
    MinValue = 1
    TabOrder = 1
    Value = 1
  end
  object btnOK: TButton
    Left = 18
    Top = 88
    Width = 75
    Height = 22
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
  end
  object btnCancel: TButton
    Left = 102
    Top = 88
    Width = 75
    Height = 22
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
