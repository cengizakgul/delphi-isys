unit rmObjPropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rwUtils, StdCtrls, TypInfo, rmTypes;

type
  TrmObjPropertyEditor = class(TFrame)
  private
    FLinkedObject: TObject;
    FPropertyName: String;
    function  GetLinkedObject: TObject;
  protected
    FEnabled: Boolean;
    function  GetDefaultPropertyName: String; virtual;
    procedure NotifyOfChanges;
    procedure SetEnabled(Value: Boolean); override;

  public
    procedure AfterConstruction; override;

    function  OldPropertyValue: Variant;
    function  NewPropertyValue: Variant; virtual;
    procedure ShowPropValue; virtual;
    procedure ApplyChanges; virtual;

    property LinkedObject: TObject read GetLinkedObject write FLinkedObject;
    property PropertyName: String read FPropertyName write FPropertyName;
  end;

  TrmObjPropertyEditorClass = class of TrmObjPropertyEditor;

implementation

{$R *.dfm}

uses rmObjectPropsFrm;

{ TrmObjPropertyEditor }

procedure TrmObjPropertyEditor.AfterConstruction;
begin
  inherited;
  FEnabled := True;
  PropertyName := GetDefaultPropertyName;
end;

function TrmObjPropertyEditor.GetDefaultPropertyName: String;
begin
  Result := '';
end;

function TrmObjPropertyEditor.GetLinkedObject: TObject;
var
  O: TComponent;
begin
  if Assigned(FLinkedObject) then
    Result := FLinkedObject
  else
  begin
    O := Owner;
    while Assigned(O) and not (O is TrmObjectProp) do
      O := O.Owner;

    if Assigned(O) then
      Result := TrmObjectProp(O).RWObject.RealObject
    else
      Result := nil;
  end;
end;

function TrmObjPropertyEditor.OldPropertyValue: Variant;
begin
  if FEnabled then
  begin
    if LinkedObject is TComponent then
      Result := (TComponent(LinkedObject) as IrmDesignObject).GetPropertyValue(PropertyName)
    else
      Result := GetComponentProperty(LinkedObject, PropertyName);
   end
      
   else
     Result := Unassigned;
end;

procedure TrmObjPropertyEditor.ShowPropValue;
begin
  if PropertyName <> '' then
  begin
    if LinkedObject is TComponent then
      FEnabled := (TComponent(LinkedObject) as IrmDesignObject).PropertyExists(PropertyName)
    else
      FEnabled := IsPublishedProp(LinkedObject, PropertyName);

    if not FEnabled then
      SetEnabled(FEnabled);
  end;  
end;

procedure TrmObjPropertyEditor.ApplyChanges;
begin
  if FEnabled and (PropertyName <> '') then
    if LinkedObject is TComponent then
      (TComponent(LinkedObject) as IrmDesignObject).SetPropertyValue(PropertyName, NewPropertyValue)
    else
      SetComponentProperty(LinkedObject, PropertyName, NewPropertyValue);
end;

function TrmObjPropertyEditor.NewPropertyValue: Variant;
begin
end;

procedure TrmObjPropertyEditor.NotifyOfChanges;
var
  O: TWinControl;
begin
  O := Parent;
  while Assigned(O) and not (O is TrmObjectProp) do
    O := O.Parent;

  if O is TrmObjectProp then
    TrmObjectProp(O).OnChangeProperty(Self);
end;

procedure TrmObjPropertyEditor.SetEnabled(Value: Boolean);
var
  i: Integer;
begin
  inherited;

  for i := 0 to ComponentCount - 1 do
  begin
    if Components[i] is TControl then
      TControl(Components[i]).Enabled := Value;
  end;
end;

end.
