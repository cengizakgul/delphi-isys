inherited rmOPEBorderLines: TrmOPEBorderLines
  Width = 303
  Height = 182
  AutoSize = True
  object Label5: TLabel
    Left = 29
    Top = 0
    Width = 67
    Height = 13
    Caption = 'Lines Property'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 0
    Top = 16
    Width = 24
    Height = 13
    Caption = 'Color'
  end
  object Label3: TLabel
    Left = 0
    Top = 67
    Width = 47
    Height = 13
    Caption = 'Line Type'
  end
  object Label4: TLabel
    Left = 0
    Top = 115
    Width = 51
    Height = 13
    Caption = 'Line Width'
  end
  object Bevel1: TBevel
    Left = 116
    Top = 3
    Width = 2
    Height = 179
  end
  object Label6: TLabel
    Left = 206
    Top = 0
    Width = 40
    Height = 13
    Caption = 'Example'
  end
  object cbColor: TColorBox
    Left = 0
    Top = 32
    Width = 102
    Height = 22
    Style = [cbStandardColors, cbExtendedColors, cbCustomColor, cbPrettyNames]
    ItemHeight = 16
    TabOrder = 0
  end
  object cbStyle: TComboBox
    Left = 0
    Top = 83
    Width = 102
    Height = 22
    Style = csOwnerDrawFixed
    ItemHeight = 16
    ItemIndex = 0
    TabOrder = 1
    Text = '0'
    OnDrawItem = cbStyleDrawItem
    Items.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4')
  end
  object cbWidth: TComboBox
    Left = 0
    Top = 132
    Width = 102
    Height = 22
    Style = csOwnerDrawFixed
    DropDownCount = 9
    ItemHeight = 16
    TabOrder = 2
    OnDrawItem = cbWidthDrawItem
    Items.Strings = (
      '0.25'
      '0.5'
      '0.75'
      '1'
      '1.5'
      '2.25'
      '3'
      '4.5'
      '6')
  end
  object pnCellExample: TPanel
    Left = 129
    Top = 18
    Width = 174
    Height = 159
    BevelOuter = bvNone
    TabOrder = 3
    object pbCellExample: TPaintBox
      Left = 41
      Top = 8
      Width = 120
      Height = 120
      Color = clBtnFace
      ParentColor = False
      OnPaint = pbCellExamplePaint
    end
    object sbTopLine: TSpeedButton
      Left = 1
      Top = 22
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF7FFFFF7FFF
        FF7FFFFFFFFFFFFFFFFFFF7FFFFF7FFFFF7FFFFFFFFFFFFFFFFFFF7F7F7F7F7F
        7F7FFFFFFFFFFFFFFFFFFF7FFFFF7FFFFF7FFFFFFFFFFFFFFFFFFF7FFFFF7FFF
        FF7FFFFFFFFFFFFFFFFFFF0000000000000FFFFFFFFFFFFFFFFF}
      Margin = 0
      OnClick = sbTopLineClick
    end
    object sbBottomLine: TSpeedButton
      Left = 1
      Top = 85
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 2
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0000000000000FFFFFFFFFFFFFFFFFFF7FFFFF7FFF
        FF7FFFFFFFFFFFFFFFFFFF7FFFFF7FFFFF7FFFFFFFFFFFFFFFFFFF7F7F7F7F7F
        7F7FFFFFFFFFFFFFFFFFFF7FFFFF7FFFFF7FFFFFFFFFFFFFFFFFFF7FFFFF7FFF
        FF7FFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7FFFFFFFFFFFFFFFFF}
      Margin = 0
      OnClick = sbBottomLineClick
    end
    object sbLeftLine: TSpeedButton
      Left = 50
      Top = 137
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 3
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0F7F7F7F7F7F7FFF0FFFFFFFFFFFFFFF0FFFFF7FFF
        FF7FFF0FFFFFFFFFFFFFFF0FFFFF7FFFFF7FFF0FFFFFFFFFFFFFFF0F7F7F7F7F
        7F7FFF0FFFFFFFFFFFFFFF0FFFFF7FFFFF7FFF0FFFFFFFFFFFFFFF0FFFFF7FFF
        FF7FFF0FFFFFFFFFFFFFFF0F7F7F7F7F7F7FFFFFFFFFFFFFFFFF}
      Margin = 0
      OnClick = sbLeftLineClick
    end
    object sbRightLine: TSpeedButton
      Left = 132
      Top = 137
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 4
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F0FFFFFFFFFFFFFFF0FFF7FFFFF7FFF
        FF0FFFFFFFFFFFFFFF0FFF7FFFFF7FFFFF0FFFFFFFFFFFFFFF0FFF7F7F7F7F7F
        7F0FFFFFFFFFFFFFFF0FFF7FFFFF7FFFFF0FFFFFFFFFFFFFFF0FFF7FFFFF7FFF
        FF0FFFFFFFFFFFFFFF0FFF7F7F7F7F7F7F0FFFFFFFFFFFFFFFFF}
      Margin = 0
      OnClick = sbRightLineClick
    end
  end
end
