unit rwBandsPropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwPropertyEditorFrm, StdCtrls, rwReport, rwDB, rwMessages, rwTypes, rwEngineTypes;

type
  TrwBandsPropertyEditor = class(TrwPropertyEditor)
    gbBands: TGroupBox;
    cbPageHeader: TCheckBox;
    cbPageFooter: TCheckBox;
    cbPageStyle: TCheckBox;
    cbTitle: TCheckBox;
    cbSummary: TCheckBox;
    cbHeader: TCheckBox;
    cbFooter: TCheckBox;
    cbDetail: TCheckBox;
    gbGroups: TGroupBox;
    lbGroups: TListBox;
    btnAddGroup: TButton;
    btnDeleteGroup: TButton;
    btnUp: TButton;
    btnDown: TButton;
    gbGroup: TGroupBox;
    Label1: TLabel;
    cbFieldsList: TComboBox;
    cbGroupHeader: TCheckBox;
    cbGroupFooter: TCheckBox;
    cbSubDetail: TCheckBox;
    cbSubSummary: TCheckBox;
    cbCustom: TCheckBox;
    cbGroupDisabled: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAddGroupClick(Sender: TObject);
    procedure cbFieldsListChange(Sender: TObject);
    procedure cbGroupHeaderClick(Sender: TObject);
    procedure cbGroupFooterClick(Sender: TObject);
    procedure lbGroupsClick(Sender: TObject);
    procedure btnDeleteGroupClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure cbFieldsListExit(Sender: TObject);
    procedure cbGroupDisabledClick(Sender: TObject);
  protected
    procedure SetReadOnly; override;    
  end;

function ReportBands(ABands: TrwBands): Boolean;

implementation

{$R *.DFM}

uses rwDesignerFrm;

const
  cGrpStr = 'Group by:  ';

function ReportBands(ABands: TrwBands): Boolean;
var
  Frm: TrwBandsPropertyEditor;
begin
  Frm := TrwBandsPropertyEditor.Create(nil, true);

  with Frm do
  try
    Value.Assign(ABands);
    Result := (ShowModal = mrOk);
    if Result then
      ABands.Assign(TPersistent(Value));
  finally
    Free;
  end;
end;

procedure TrwBandsPropertyEditor.FormCreate(Sender: TObject);
var
  DS: TrwDataSet;
begin
  inherited;
  FValue := TrwBands.Create;

  DS := ReportDesigner.CurrentReportPaper.Report.MasterDataSource;
  if Assigned(DS) then
    try
      DS.GetFieldList(cbFieldsList.Items);
    except
    end;
end;

procedure TrwBandsPropertyEditor.FormDestroy(Sender: TObject);
begin
  inherited;
  FValue.Free;
end;

procedure TrwBandsPropertyEditor.FormShow(Sender: TObject);
var
  i: Integer;
begin
  inherited;

  with TrwBands(FValue) do
  begin
    cbPageHeader.Checked := PageHeader;
    cbTitle.Checked := Title;
    cbHeader.Checked := Header;
    cbFooter.Checked := Footer;
    cbSummary.Checked := Summary;
    cbPageFooter.Checked := PageFooter;
    cbPageStyle.Checked := PageStyle;
    cbSubDetail.Checked := SubDetail;
    cbSubSummary.Checked := SubSummary;
    cbCustom.Checked := Custom;

    lbGroups.Clear;
    for i := 0 to Groups.Count - 1 do
      lbGroups.Items.AddObject(cGrpStr + TrwGroup(Groups[i]).FieldName, Groups[i]);
  end;
end;

procedure TrwBandsPropertyEditor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  with TrwBands(FValue) do
  begin
    PageHeader := cbPageHeader.Checked;
    Title := cbTitle.Checked;
    Header := cbHeader.Checked;
    Footer := cbFooter.Checked;
    Summary := cbSummary.Checked;
    PageFooter := cbPageFooter.Checked;
    PageStyle := cbPageStyle.Checked;
    SubDetail := cbSubDetail.Checked;
    SubSummary := cbSubSummary.Checked;
    Custom := cbCustom.Checked;
  end;
end;

procedure TrwBandsPropertyEditor.btnAddGroupClick(Sender: TObject);
var
  lGrp: TrwGroup;
begin
  lGrp := TrwGroup.Create(TrwBands(FValue).Groups);
  lbGroups.Items.AddObject(cGrpStr, lGrp);
  lbGroups.ItemIndex := lbGroups.Items.Count - 1;
  lbGroups.OnClick(nil);
end;

procedure TrwBandsPropertyEditor.cbFieldsListChange(Sender: TObject);
var
  lGrp: TrwGroup;
begin
  if (lbGroups.ItemIndex = -1) then Exit;

  lGrp := TrwGroup(lbGroups.Items.Objects[lbGroups.ItemIndex]);
  if lGrp.InheritedItem and (lGrp.FieldName <> cbFieldsList.Text) then
  begin
    cbFieldsList.ItemIndex := cbFieldsList.Items.IndexOf(lGrp.FieldName);
    raise ErwException.Create(ResErrInheritedChange);
  end;

  lGrp.FieldName := cbFieldsList.Text;
  lbGroups.Items[lbGroups.ItemIndex] := cGrpStr + lGrp.FieldName;
end;

procedure TrwBandsPropertyEditor.cbGroupHeaderClick(Sender: TObject);
var
  lGrp: TrwGroup;
begin
  if (lbGroups.ItemIndex = -1) then Exit;

  lGrp := TrwGroup(lbGroups.Items.Objects[lbGroups.ItemIndex]);
  if lGrp.InheritedItem and (cbGroupHeader.Checked <> lGrp.GroupHeaderVisible) then
  begin
    cbGroupHeader.Checked := lGrp.GroupHeaderVisible;
    raise ErwException.Create(ResErrInheritedChange);
  end;

  lGrp.GroupHeaderVisible := cbGroupHeader.Checked;
end;

procedure TrwBandsPropertyEditor.cbGroupFooterClick(Sender: TObject);
var
  lGrp: TrwGroup;
begin
  if (lbGroups.ItemIndex = -1) then Exit;

  lGrp := TrwGroup(lbGroups.Items.Objects[lbGroups.ItemIndex]);
  if lGrp.InheritedItem and (cbGroupFooter.Checked <> lGrp.GroupFooterVisible) then
  begin
    cbGroupFooter.Checked := lGrp.GroupFooterVisible;
    raise ErwException.Create(ResErrInheritedChange);
  end;

  lGrp.GroupFooterVisible := cbGroupFooter.Checked;
end;

procedure TrwBandsPropertyEditor.lbGroupsClick(Sender: TObject);
var
  lGrp: TrwGroup;
begin
  gbGroup.Enabled := (lbGroups.ItemIndex <> -1);

  if gbGroup.Enabled then
  begin
    lGrp := TrwGroup(lbGroups.Items.Objects[lbGroups.ItemIndex]);
    cbFieldsList.ItemIndex := cbFieldsList.Items.IndexOf(lGrp.FieldName);
    if cbFieldsList.ItemIndex = -1 then
      cbFieldsList.Text := lGrp.FieldName;
    cbGroupHeader.Checked := lGrp.GroupHeaderVisible;
    cbGroupFooter.Checked := lGrp.GroupFooterVisible;
    cbGroupDisabled.Checked := lGrp.Disabled;
  end;
end;

procedure TrwBandsPropertyEditor.btnDeleteGroupClick(Sender: TObject);
var
  lGrp: TrwGroup;
begin
  if (lbGroups.ItemIndex = -1) then Exit;

  lGrp := TrwGroup(lbGroups.Items.Objects[lbGroups.ItemIndex]);
  if lGrp.InheritedItem then
    raise ErwException.Create(ResErrInheritedDelete);

  lGrp.Free;
  lbGroups.Items.Delete(lbGroups.ItemIndex);
end;

procedure TrwBandsPropertyEditor.btnUpClick(Sender: TObject);
var
  i: Integer;
begin
  i := lbGroups.ItemIndex;

  if (i <= 0) then Exit;

  TrwBands(FValue).Groups[i].Index := i - 1;
  lbGroups.Items.Move(i, i - 1);
  lbGroups.ItemIndex := i - 1;
end;

procedure TrwBandsPropertyEditor.btnDownClick(Sender: TObject);
var
  i: Integer;
begin
  i := lbGroups.ItemIndex;

  if (i = -1) or (i = lbGroups.Items.Count - 1) then Exit;

  TrwBands(FValue).Groups[i].Index := i + 1;
  lbGroups.Items.Move(i, i + 1);
  lbGroups.ItemIndex := i + 1;
end;

procedure TrwBandsPropertyEditor.cbFieldsListExit(Sender: TObject);
var
  lGrp: TrwGroup;
begin
  if (lbGroups.ItemIndex = -1) then
  begin
    lGrp := TrwGroup(lbGroups.Items.Objects[lbGroups.ItemIndex]);
    lGrp.FieldName := cbFieldsList.Text;
  end;
end;

procedure TrwBandsPropertyEditor.cbGroupDisabledClick(Sender: TObject);
var
  lGrp: TrwGroup;
begin
  if (lbGroups.ItemIndex = -1) then Exit;

  lGrp := TrwGroup(lbGroups.Items.Objects[lbGroups.ItemIndex]);
  lGrp.Disabled := cbGroupDisabled.Checked;
end;


procedure TrwBandsPropertyEditor.SetReadOnly;
begin
  inherited;
  gbBands.Enabled := False;
  gbGroup.Enabled := False;
  btnDeleteGroup.Enabled := False;
  btnAddGroup.Enabled := False;
  btnUp.Enabled := False;
  btnDown.Enabled := False;
end;

initialization
  RegisterClass(TrwBandsPropertyEditor);

end.
