// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwDesignAreaFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, rwBasicClasses, rwCtrlResizer,
  TypInfo, rwCommonClasses, rwMessages, rwReport, rwPropertiesEditorFrm,
  Clipbrd, rwDsgnRes, rwQueryBuilderFrm,
  rwQBInfo, rwUtils, rwDsgnUtils, rwDB, Menus, rwInputFormControls,
  ComCtrls, rwTypes, rwTransparentPanel, rwReportControls,
  rwDsgnControls, rwEngineTypes, EvStreamUtils;

type

  TrwArea = class(TrwTransparentPanel)
  public
    constructor Create(AOwner: TComponent); override;
  end;


  TrwDesignArea = class;

  TrwOnDemandImageComponent = procedure(Sender: TObject; AClassName: string; AImage: TBitmap) of object;
  TrwOnShowDefProperty = procedure(Sender: TObject; APropName: string) of object;

  {TSelectedCompList is list of selected components belong to TDesignArea}

  TrwSelectedCompList = class(TList)
  private
    FDesignArea: TrwDesignArea;

    function GetItem(Index: Integer): TrwComponent;
    function GetResizer(Index: Integer): TrwControlResizer;

  public
    property Items[Index: Integer]: TrwComponent read GetItem; default;
    property Resizers[Index: Integer]: TrwControlResizer read GetResizer;

    constructor Create(ADesignArea: TrwDesignArea);
    destructor Destroy; override;

    function  Add(AComponent: TrwComponent; ALock: Boolean = False): Integer;
    procedure Delete(Index: Integer);
    function  ResizerByControl(AControl: TControl): TrwControlResizer;
    procedure Clear; override;
  end;

  {TDesignArea is area for designing abstract form}

  TrwOnCreateNewComponent = procedure(Sender: TObject; AComponent: TrwComponent; AParent: TrwControl;
    X, Y: Integer) of object;

  TrwOnDestroyComponent = procedure(Sender: TObject; AComponent: TrwComponent) of object;

  TrwOnSelectComponent = procedure(Sender: TObject; AComponent: TrwComponent; GroupSelection: Boolean) of object;

  TrwDesignArea = class(TFrame)
    pmTrwPageControl: TPopupMenu;
    NewPage1: TMenuItem;
    NextPage1: TMenuItem;
    PreviousPage1: TMenuItem;
    procedure pnlAreaMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure InheritedCompsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ShowDefProperty(Sender: TObject);
    procedure NewPage1Click(Sender: TObject);
    procedure NextPage1Click(Sender: TObject);
    procedure PreviousPage1Click(Sender: TObject);
    procedure ShowRullerPos(Sender: TObject; Shift: TShiftState; X, Y: Integer);

  private
    FSelectedCompList: TrwSelectedCompList;
    FComponentsOwner: TComponent;
    FNewComponentClassName: string;
    FOnCreateNewComponent: TrwOnCreateNewComponent;
    FOnDestroyComponent: TrwOnDestroyComponent;
    FAfterDestroyContainer: TNotifyEvent;
    FOnSelectComponent: TrwOnSelectComponent;
    FOnDemandImageComponent: TrwOnDemandImageComponent;
    FOnShowDefProperty: TrwOnShowDefProperty;
    FOnSetProperty: TOnPropertyChange;
    FContainerForPaste: TrwControl;
    FUnDoMS: TMemoryStream;
    FMultiCreation: Boolean;

    procedure CreateNewComponent(AParent: TObject; X, Y: Integer; AIgnoreControlStyle: Boolean = False);
    procedure OnAttachResizer(Sender: TObject);
    procedure OnResizeComponent(Sender: TObject);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    property  ComponentsOwner: TComponent read FComponentsOwner write FComponentsOwner;
    procedure AfterCreationComponent(lComponent: TrwComponent; AParent: TrwControl; X: Integer; Y: Integer);
    procedure BeforeDestroyingComponent(Sender: TObject);

    procedure SelectionToMemStream(aMemoryStream: TMemoryStream);
    procedure SelectionFromMemStream(aMemoryStream: TMemoryStream);
    procedure MemStreamToClipboard(aMemoryStream: TMemoryStream);
    procedure MemStreamFromClipboard(aMemoryStream: TMemoryStream);
    procedure ReaderOnSetName(Reader: TReader; Component: TComponent; var Name: string);
    procedure ReaderOnFindMethod(Reader: TReader; const MethodName: string; var Address: Pointer; var Error: Boolean);
    procedure ReaderOnReferenceName(Reader: TReader; var Name: string);
    procedure ReaderComponentReadCallback(Component: TComponent);
    procedure AncestorNotFound(Reader: TReader; const ComponentName: string; ComponentClass: TPersistentClass;
                               var Component: TComponent);
    function SelectLibComp: TrwLibComponent; virtual; abstract;

    procedure SetComponentsMouseEvent(AComponent: TrwComponent; AClear: Boolean = False);
    procedure SetInheritedComponentMouseEvent(AComponent: TrwComponent; AClear: Boolean = False);
    procedure SetInheritedComponentsMouseEvent(AComponent: TrwComponent; AClear: Boolean = False);


  public
    pnlArea: TrwArea;

    property SelectedCompList: TrwSelectedCompList read FSelectedCompList;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure BeforeDestruction; override;
    procedure PrepDsgnComp(AComp: TrwComponent);
    procedure SetNewComponentClassName(AClassName: string; AMultiCreation: Boolean);
    function  AddSelection(AComponent: TrwComponent; GroupSelection: Boolean; ALock: Boolean = False): Boolean;
    procedure DeleteSelectionFor(AComponent: TrwComponent);
    procedure ClearSelection;
    procedure SetPropertyForSelection(const PropName: array of ShortString; Value: Variant);
    procedure ActionForSelection(AAction: TrwActionToolBarEvent);
    procedure ExpertForSelection(AAction: TrwExpertToolBarEvent);
    procedure RefreshCompResizer;
    procedure DestroySelectedComponents; virtual;
    procedure InitNoVisualComponent(AComponent: TrwNoVisualComponent);
    procedure Copy;
    procedure Paste;
    procedure Cut;
    procedure Undo;
    function  UndoIsNotEmpty: Boolean;

  published
    property OnCreateNewComponent: TrwOnCreateNewComponent read FOnCreateNewComponent write FOnCreateNewComponent;
    property OnDestroyComponent: TrwOnDestroyComponent read FOnDestroyComponent write FOnDestroyComponent;
    property AfterDestroyContainer: TNotifyEvent read FAfterDestroyContainer write FAfterDestroyContainer;
    property OnSelectComponent: TrwOnSelectComponent read FOnSelectComponent write FOnSelectComponent;
    property OnSetComponentProperty: TOnPropertyChange read FOnSetProperty write FOnSetProperty;
    property OnDemandImageComponent: TrwOnDemandImageComponent read FOnDemandImageComponent write FOnDemandImageComponent;
    property OnShowDefProperty: TrwOnShowDefProperty read FOnShowDefProperty write FOnShowDefProperty;
  end;

implementation

{$R *.DFM}

uses rwDesignerFrm, rwLibraryFrm, rwLocalVarFunctFrm, rwAllEventsFrm,
     rwPrintOrderFrm, rwASCIIPrintOrderFrm, rwTabOrderFrm, rwTableCellSplitDlgFrm;

var
  FClipbpardMS: TMemoryStream;
  FSubstCompNamesOnPaste: TStringList;

  {TSelectedCompList}

constructor TrwSelectedCompList.Create(ADesignArea: TrwDesignArea);
begin
  inherited Create;

  FDesignArea := ADesignArea;
end;

destructor TrwSelectedCompList.Destroy;
begin
  Clear;

  inherited;
end;

function TrwSelectedCompList.GetItem(Index: Integer): TrwComponent;
begin
  Result := TrwControlResizer(inherited Items[Index]).Component;
end;

function TrwSelectedCompList.GetResizer(Index: Integer): TrwControlResizer;
begin
  Result := TrwControlResizer(inherited Items[Index]);
end;

function TrwSelectedCompList.Add(AComponent: TrwComponent; ALock: Boolean = False): Integer;
var
  lCtrlResizer: TrwControlResizer;
begin
  lCtrlResizer := TrwControlResizer.Create(FDesignArea);
  if ALock then
    lCtrlResizer.BeginChanges;
  lCtrlResizer.OnAttach := FDesignArea.OnAttachResizer;
  lCtrlResizer.OnAfterResize := FDesignArea.OnResizeComponent;
  Result := inherited Add(lCtrlResizer);
  lCtrlResizer.Attach(AComponent);
end;

procedure TrwSelectedCompList.Delete(Index: Integer);
begin
  TrwControlResizer(inherited Items[Index]).Free;

//  inherited Delete(Index);
end;

procedure TrwSelectedCompList.Clear;
var
  i: Integer;
begin
  for i := Count - 1 downto 0 do
    TrwControlResizer(inherited Items[i]).Free;

  inherited Clear;
end;

function TrwSelectedCompList.ResizerByControl(AControl: TControl): TrwControlResizer;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if TrwControlResizer(inherited Items[i]).VisualControl = AControl then
    begin
      Result := TrwControlResizer(inherited Items[i]);
      break;
    end;
end;


  {TDesignArea}

constructor TrwDesignArea.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FSelectedCompList := TrwSelectedCompList.Create(Self);
  FUnDoMS := TIsMemoryStream.Create;

  pnlArea := TrwArea.Create(Self);
  pnlArea.Name := 'pnlArea';
  pnlArea.Parent := Self;
  pnlArea.OnMouseDown := pnlAreaMouseDown;
end;

procedure TrwDesignArea.BeforeDestruction;
begin
  ClearSelection;

  inherited;
end;

destructor TrwDesignArea.Destroy;
begin
  FUnDoMS.Free;
  FSelectedCompList.Free;

  inherited;
end;

function TrwDesignArea.AddSelection(AComponent: TrwComponent; GroupSelection: Boolean; ALock: Boolean = False): Boolean;

  procedure AddSel(AComponent: TrwComponent; ALock: Boolean = False);
  var
    i: Integer;
  begin
    if Assigned(AComponent) and ((AComponent is TrwControl) or (AComponent is TrwNoVisualComponent)) then
    begin
      i := SelectedCompList.Add(AComponent, ALock);
      SelectedCompList.GetResizer(i).OnMouseMove := ShowRullerPos;
    end;
  end;

begin
  if not GroupSelection then
    ClearSelection;
  AddSel(AComponent, ALock);
  Result := True;
end;

procedure TrwDesignArea.DeleteSelectionFor(AComponent: TrwComponent);
var
  i: Integer;
begin
  for i := 0 to SelectedCompList.Count - 1 do
    if (SelectedCompList[i] = AComponent) then
    begin
      SelectedCompList.Delete(i);
      break;
    end;

  if Assigned(FOnSelectComponent) and (SelectedCompList.Count > 0) then
  begin
    FOnSelectComponent(Self, SelectedCompList[0], False);
    for i := 1 to SelectedCompList.Count - 1 do
      FOnSelectComponent(Self, SelectedCompList[i], True);
  end;
end;

procedure TrwDesignArea.ClearSelection;
begin
  SelectedCompList.Clear;
end;

procedure TrwDesignArea.CreateNewComponent(AParent: TObject; X, Y: Integer; AIgnoreControlStyle: Boolean = False);
var
  lPersistentClass: TPersistentClass;
  lComponent, Own: TrwComponent;
  P: TPoint;
  LibComp: TrwLibComponent;
  i: Integer;
begin
  LibComp := nil;
  Own := nil;

  if AParent is TrwControl then
    Own := TrwComponent(TrwControl(AParent).Owner);

  if not Assigned(Own) then
    Own := TrwComponent(ComponentsOwner);

  if Assigned(AParent) and not AIgnoreControlStyle and
     (not (csAcceptsControls in TrwControl(AParent).VisualControl.RealObject.ControlStyle)) then
  begin
    X := TrwControl(AParent).Left + X;
    Y := TrwControl(AParent).Top + Y;
    if AParent is TrwChildComponent then
      AParent := TrwChildComponent(AParent).Container
    else
      AParent := TrwFormControl(AParent).Parent;
  end;

  lPersistentClass := GetClass(FNewComponentClassName);

  if not Assigned(lPersistentClass) then
  begin
    if FNewComponentClassName = 'Library' then
    begin
      LibComp := SelectLibComp;
      if not Assigned(LibComp) then
      begin
        if not FMultiCreation then
          FNewComponentClassName := '';
        Exit;
      end;
    end

    else
    begin
      i := SystemLibComponents.IndexOf(FNewComponentClassName);
      if i <> -1 then
        LibComp := SystemLibComponents[i]
      else
        raise ErwException.CreateFmt(ResErrWrongClass, [FNewComponentClassName]);
    end;
  end;

  if Assigned(LibComp) then
  begin
    lComponent := TrwComponent(LibComp.CreateComp(nil, True));

    if lComponent.InheritsFrom(TrwFormControl) and not Assigned(AParent) then
      AParent := Own;

    if lComponent.InheritsFrom(TrwControl) and not Assigned(AParent) then
    begin
      lComponent.Free;
      Exit;
    end;
    Own.InsertComponent(lComponent);
  end
  else
  begin
    if lPersistentClass.InheritsFrom(TrwChildComponent) and not Assigned(AParent) then
      Exit;

    if lPersistentClass.InheritsFrom(TrwFormControl) and not Assigned(AParent) then
      AParent := Own;

    if lPersistentClass = TrwSubReport then
      lComponent := ReportDesigner.CreateReport(ReportDesigner.CurrentReportPaper)
    else
      lComponent := TrwComponentClass(lPersistentClass).Create(Own);
  end;

  if (lComponent is TrwNoVisualComponent) and (Own <> ComponentsOwner) then
  begin
    Own.RemoveComponent(lComponent);
    ComponentsOwner.InsertComponent(lComponent);
    Own := TrwComponent(ComponentsOwner);
  end;

  lComponent.Name := GetUniqueNameForComponent(lComponent);

  if not Assigned(LibComp) then
    lComponent.InitializeForDesign;

  if (lComponent is TrwControl) then
  begin
    SetOrdProp(lComponent, 'Left', X);
    SetOrdProp(lComponent, 'Top', Y);

    if (AParent is TrwPrintableContainer) then
      SetObjectProp(lComponent, 'Container', TrwPrintableContainer(AParent))
    else
    begin
      SetObjectProp(lComponent, 'Parent', TrwWinFormControl(AParent));
      if IsPublishedProp(lComponent, 'TabOrder') then
        SetOrdProp(lComponent, 'TabOrder', High(TTabOrder));
    end;
  end

  else
  begin
    P := Point(X, Y);
    if Assigned(AParent) then
      P := TrwControl(AParent).VisualControl.RealObject.ClientToScreen(P)
    else
      P := pnlArea.ClientToScreen(P);
    P := Self.ScreenToClient(P);
    TrwNoVisualComponent(lComponent).Left := P.X;
    TrwNoVisualComponent(lComponent).Top := P.Y;

    InitNoVisualComponent(TrwNoVisualComponent(lComponent));
  end;

  TrwComponent(Own).FixUpCompIndexes;

  AfterCreationComponent(lComponent, TrwControl(AParent), X, Y);

  AddSelection(lComponent, False);

  if not FMultiCreation then
    FNewComponentClassName := '';
end;

procedure TrwDesignArea.InitNoVisualComponent(AComponent: TrwNoVisualComponent);
begin
  TrwNoVisualComponent(AComponent).DesignParent := Self;

  if Assigned(FOnDemandImageComponent) then
    FOnDemandImageComponent(Self, AComponent.ClassName, TrwNonVisualComponentDesignPanel(TrwNoVisualComponent(AComponent).VisualControl.RealObject).Image);
end;

procedure TrwDesignArea.AfterCreationComponent(lComponent: TrwComponent; AParent: TrwControl; X: Integer; Y: Integer);
var
  i: Integer;
  C: TComponent;
begin
  if Assigned(FOnCreateNewComponent) then
    FOnCreateNewComponent(Self, lComponent, AParent, X, Y);

  lComponent.OnDestroy := BeforeDestroyingComponent;

  PrepDsgnComp(lComponent);

  if not lComponent.IsLibComponent then
    for i := 0 to lComponent.ComponentCount - 1 do
    begin
      C := lComponent.Components[i];
      if C is TrwControl then
        PrepDsgnComp(TrwComponent(C));
    end;
end;

procedure TrwDesignArea.BeforeDestroyingComponent(Sender: TObject);
var
  lComponent: TrwComponent;
begin
  lComponent := TrwComponent(Sender);

  DeleteSelectionFor(lComponent);

  if Assigned(FOnDestroyComponent) then
    FOnDestroyComponent(Self, lComponent);
end;

procedure TrwDesignArea.pnlAreaMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  P: TPoint;
  lCtrlResizer: TrwControlResizer;
  M: TMethod;
  i: Integer;
begin
  if (Button <> mbLeft) or (Sender = Self) then Exit;

  if (Shift = [ssLeft]) and (Sender is TPageControl) then
  begin
    i := TPageControl(Sender).IndexOfTabAt(X, Y);
    if i <> -1 then
      TPageControl(Sender).ActivePageIndex := i;
  end;

  if Sender is TrwControlResizer then
  begin
    lCtrlResizer := TrwControlResizer(Sender);
    Sender := TrwControlResizer(Sender).VisualControl;
  end
  else if Sender is TControl then
  begin
    lCtrlResizer := SelectedCompList.ResizerByControl(TControl(Sender));
    if Assigned(lCtrlResizer) then
    begin
      P.X := X;
      P.Y := Y;
      P := TControl(Sender).ClientToScreen(P);
      P := lCtrlResizer.ScreenToClient(P);
      if ssShift in Shift then
        DeleteSelectionFor(lCtrlResizer.Component)
      else
        SendMessage(lCtrlResizer.Handle, WM_LBUTTONDOWN, MK_LBUTTON, MakeLong(P.X, P.Y));
      Exit;
    end;
  end
  else
    lCtrlResizer := nil;

  if (FNewComponentClassName <> '') then
  begin
    if TControl(Sender) is TrwNonVisualComponentDesignPanel then
      Exit;

    if (lCtrlResizer = nil) then
      ClearSelection;

    if Sender = pnlArea  then
      CreateNewComponent(nil, X, Y)
    else
      CreateNewComponent((TControl(Sender) as IrwVisualControl).RWObject, X, Y);
  end

  else if (Sender = pnlArea) then
  begin
    if Assigned(Screen.ActiveControl) and IsPublishedProp(Screen.ActiveControl, 'OnExit') then
    begin
      M := GetMethodProp(Screen.ActiveControl, 'OnExit');
      if Assigned(M.Code) then
        TNotifyEvent(M)(Screen.ActiveControl);
    end;
    ClearSelection;
    Screen.ActiveForm.ActiveControl := nil;
    if Assigned(FOnSelectComponent) then
      FOnSelectComponent(Self, TrwComponent(ComponentsOwner), False);
  end

  else
  begin
    if Assigned(lCtrlResizer) then
      Exit;

    if not AddSelection((TControl(Sender) as IrwVisualControl).RWObject, (ssShift in Shift), True) then
      Exit;

    if not (ssShift in Shift) then
    begin
      P.X := X;
      P.Y := Y;
      P := TControl(Sender).ClientToScreen(P);
      P := TrwControlResizer(Components[ComponentCount - 1]).ScreenToClient(P);

      SendMessage(TrwControlResizer(Components[ComponentCount - 1]).Handle,
        WM_LBUTTONDOWN, MK_LBUTTON, MakeLong(P.X, P.Y));
    end;

    TrwControlResizer(Components[ComponentCount - 1]).EndChanges;
    TrwControlResizer(Components[ComponentCount - 1]).OnAttach(TrwControlResizer(Components[ComponentCount - 1]));
  end;
end;

procedure TrwDesignArea.SetComponentsMouseEvent(AComponent: TrwComponent; AClear: Boolean = False);
var
  P, PD, PM: TMethod;
  M: TComponent;
  C: TControl;
begin
  if AClear then
  begin
    P.Data := nil;
    P.Code := nil;
    PD.Data := nil;
    PD.Code := nil;
    PM.Data := nil;
    PM.Code := nil;
  end
  else
  begin
    P.Data := Self;
    P.Code := MethodAddress('pnlAreaMouseDown');
    PD.Data := Self;
    PD.Code := MethodAddress('ShowDefProperty');
    PM.Data := Self;
    PM.Code := MethodAddress('ShowRullerPos');
  end;


  if (AComponent is TrwControl) then
  begin
    C := TrwControl(AComponent).VisualControl.RealObject;
    if IsPublishedProp(C, 'ShowCorners') then
      SetOrdProp(C, 'ShowCorners', Ord(True));
  end
  else if (AComponent is TrwNoVisualComponent) then
    C := TrwNoVisualComponent(AComponent).VisualControl.RealObject
  else
    C := nil;

  SetMethodProp(C, 'OnMouseDown', P);
  SetMethodProp(C, 'OnDblClick', PD);
  if Assigned(PM.Code) then
    SetMethodProp(C, 'OnMouseMove', PM);

  if AClear then
    SetObjectProp(C, 'PopupMenu', nil)
  else
  begin
    M := FindComponent('pm'+AComponent.ClassName);
    if M is TPopupMenu then
      SetObjectProp(C, 'PopupMenu', M);
  end;
end;


procedure TrwDesignArea.SetInheritedComponentMouseEvent(AComponent: TrwComponent; AClear: Boolean = False);
var
  P: TMethod;
begin
  if AClear then
  begin
    P.Data := nil;
    P.Code := nil;
  end

  else
  begin
    P.Data := Self;
    P.Code := MethodAddress('InheritedCompsMouseDown');
  end;

  if (AComponent is TrwControl) then
  begin
    if Assigned(TrwControl(AComponent).VisualControl.RealObject) then
    begin
      SetMethodProp(TrwControl(AComponent).VisualControl.RealObject, 'OnMouseDown', P);
      if IsPublishedProp(TrwControl(AComponent).VisualControl.RealObject, 'ShowCorners') then
        SetOrdProp(TrwControl(AComponent).VisualControl.RealObject, 'ShowCorners', Ord(False));
    end;
  end

  else if (AComponent is TrwNoVisualComponent) then
  begin
    if Assigned(TrwNoVisualComponent(AComponent).VisualControl.RealObject) then
      SetMethodProp(TrwNoVisualComponent(AComponent).VisualControl.RealObject, 'OnMouseDown', P);
  end;
end;



procedure TrwDesignArea.SetInheritedComponentsMouseEvent(AComponent: TrwComponent; AClear: Boolean = False);
var
  i: Integer;
begin
  for i := 0 to AComponent.ComponentCount-1 do
    if AComponent.Components[i] is TrwComponent and not AComponent.Components[i].InheritsFrom(TrwGlobalVarFunct) then
    begin
      SetInheritedComponentMouseEvent(TrwComponent(AComponent.Components[i]), AClear);
      SetInheritedComponentsMouseEvent(TrwComponent(AComponent.Components[i]), AClear);
    end;
end;


procedure TrwDesignArea.SetNewComponentClassName(AClassName: string; AMultiCreation: Boolean);
begin
  FNewComponentClassName := AClassName;
  FMultiCreation := AMultiCreation;
end;

procedure TrwDesignArea.SetPropertyForSelection(const PropName: array of ShortString; Value: Variant);
var
  i: Integer;
begin
  for i := 0 to SelectedCompList.Count - 1 do
    SetComponentProperty(SelectedCompList[i], PropName, Value);

  OnResizeComponent(nil);
end;

procedure TrwDesignArea.OnAttachResizer(Sender: TObject);
var
  P: TMethod;
begin
  P := GetMethodProp(TrwControlResizer(Sender).VisualControl, 'OnDblClick');
  SetMethodProp(Sender, 'OnDblClick', P);

  if Assigned(FOnSelectComponent) then
    FOnSelectComponent(Self, TrwComponent(TrwControlResizer(Sender).Component),
      TrwControlResizer(Sender).Grouping);
end;

procedure TrwDesignArea.OnResizeComponent(Sender: TObject);
var
  Comp: TrwComponent;
begin
  if (ComponentsOwner.ComponentState <> []) then Exit;

  if Assigned(FOnSetProperty) then
  begin
    if (SelectedCompList.Count = 1) then
      Comp := SelectedCompList[0]
    else
      Comp := nil;
    FOnSetProperty(Self, Comp);
  end;
end;

procedure TrwDesignArea.RefreshCompResizer;
var
  i: Integer;
begin
  for i := 0 to SelectedCompList.Count - 1 do
  begin
    SelectedCompList.Resizers[i].OnAfterResize := nil;
    try
      SelectedCompList.Resizers[i].BoundsByControl;
    finally
      SelectedCompList.Resizers[i].OnAfterResize := OnResizeComponent;
    end;
  end;
end;

procedure TrwDesignArea.DestroySelectedComponents;
var
  i: Integer;
begin
  for i := 0 to SelectedCompList.Count - 1 do
    if SelectedCompList[i].InheritedComponent then
      raise ErwException.Create(ResErrInheritedDelete);
  SelectionToMemStream(FUnDoMS);
end;

procedure TrwDesignArea.SelectionToMemStream(aMemoryStream: TMemoryStream);
var
  i: Integer;
  lWriter: TWriter;
begin
  aMemoryStream.Clear;
  lWriter := TWriter.Create(aMemoryStream, 1024);
  lWriter.Root := ComponentsOwner;

  try
    for i := 0 to SelectedCompList.Count - 1 do
    begin
      if SelectedCompList[i] is TrwBand then
        Continue;
      SelectedCompList[i].WriteComp(lWriter);
    end;
    lWriter.WriteListEnd;
  finally
    lWriter.Free;
  end;
end;

procedure TrwDesignArea.MemStreamToClipboard(aMemoryStream: TMemoryStream);
var
  lhData: THandle;
  lpDataPtr: Pointer;
begin
  lhData := GlobalAlloc(GMEM_MOVEABLE, aMemoryStream.Size);

  try
    lpDataPtr := GlobalLock(lhData);

    try
      Move(aMemoryStream.Memory^, lpDataPtr^, aMemoryStream.Size);
      ClipBoard.Open;
      ClipBoard.SetAsHandle(CF_RWDATA, lhData);
      ClipBoard.AsText := IntToStr(aMemoryStream.Size);
      ClipBoard.Close;

    finally
      GlobalUnlock(lhData);
    end;

  except
    GlobalFree(lhData);
    raise;
  end;
end;

procedure TrwDesignArea.MemStreamFromClipboard(aMemoryStream: TMemoryStream);
var
  lhData: THandle;
  lpDataPtr: Pointer;
  llSize: LongInt;
begin
  aMemoryStream.Clear;
  Clipboard.Open;

  lhData := Clipboard.GetAsHandle(CF_RWDATA);

  try
    if lhData = 0 then
      Exit;

    lpDataPtr := GlobalLock(lhData);
    llSize := StrToInt(ClipBoard.AsText);

    try
      aMemoryStream.Write(lpDataPtr^, llSize);
    finally
      GlobalUnlock(lhData);
      Clipboard.Close;
    end;

  except
    GlobalFree(lhData);
    raise;
  end;
end;

procedure TrwDesignArea.SelectionFromMemStream(aMemoryStream: TMemoryStream);
var
  lReader: TReader;
  i: Integer;
  C: TrwComponent;
  Own: TComponent;
  fl: Boolean;

  procedure CreateDesignPagesForSubReport(AReport: TrwSubReport);
  var
    i: Integer;
  begin
    ReportDesigner.CreateReport(nil, AReport);
    for i := 0 to AReport.ReportForm.ComponentCount - 1 do
      if AReport.ReportForm.Components[i] is TrwSubReport then
        CreateDesignPagesForSubReport(TrwSubReport(AReport.ReportForm.Components[i]));
  end;

begin
  ClearSelection;
  FSubstCompNamesOnPaste.Clear;
  aMemoryStream.Position := 0;

  lReader := TReader.Create(aMemoryStream, 1024);

  try
    lReader.OnSetName := ReaderOnSetName;
    lReader.OnFindMethod := ReaderOnFindMethod;
    lReader.OnReferenceName := ReaderOnReferenceName;
    lReader.OnAncestorNotFound := AncestorNotFound;

//    if Assigned(FContainerForPaste) and Assigned(FContainerForPaste.Owner) then
//      Own := FContainerForPaste.Owner
//    else
      Own := ComponentsOwner;

    lReader.ReadComponents(Own, nil, ReaderComponentReadCallback);

    for i := 0 to SelectedCompList.Count - 1 do
      if (SelectedCompList[i] is TrwChildComponent) then
      begin
        if Assigned(FContainerForPaste) then
        begin
          TrwChildComponent(SelectedCompList[i]).Container := TrwPrintableContainer(FContainerForPaste);
          SelectedCompList.Resizers[i].Parent := TWinControl(TrwPrintableContainer(FContainerForPaste).VisualControl.RealObject);
        end
        else if aMemoryStream <> FUnDoMS then
          TrwChildComponent(SelectedCompList[i]).Container := nil;
      end

      else if (SelectedCompList[i] is TrwFormControl) then
      begin
        if Assigned(FContainerForPaste) then
        begin
          TrwFormControl(SelectedCompList[i]).Parent := TrwWinFormControl(FContainerForPaste);
          SelectedCompList.Resizers[i].Parent := TWinControl(TrwWinFormControl(FContainerForPaste).VisualControl.RealObject);
        end
        else if aMemoryStream <> FUnDoMS then
          TrwFormControl(SelectedCompList[i]).Parent := nil;
      end

      else if SelectedCompList[i] is TrwSubReport then
        CreateDesignPagesForSubReport(TrwSubReport(SelectedCompList[i]));

  finally
    lReader.Free;
  end;

  fl := False;
  i := 0;
  while i < SelectedCompList.Count do
  begin
    if (SelectedCompList[i] is TrwChildComponent) and
        not Assigned(TrwChildComponent(SelectedCompList[i]).Container)
        or
       (SelectedCompList[i] is TrwFormControl) and
        not Assigned(TrwFormControl(SelectedCompList[i]).Parent) then
    begin
      C := TrwComponent(SelectedCompList[i]);
      SelectedCompList.Delete(i);
      C.Free;
      fl := True;
    end
    else
      Inc(i);
  end;

  TrwComponent(Own).FixUpCompIndexes;

  if fl then
    if SelectedCompList.Count > 0 then
    begin
      if Assigned(FOnSelectComponent) then
      begin
        FOnSelectComponent(Self, TrwComponent(SelectedCompList[0]), False);
        for i := 1 to SelectedCompList.Count - 1 do
          FOnSelectComponent(Self, TrwComponent(SelectedCompList[i]), True);
      end
    end
    else
    begin
      ClearSelection;
      if Assigned(FOnSelectComponent) then
        FOnSelectComponent(Self, TrwComponent(ComponentsOwner).VirtualOwner, False);
    end;
end;


procedure TrwDesignArea.AncestorNotFound(Reader: TReader;
  const ComponentName: string; ComponentClass: TPersistentClass;
  var Component: TComponent);
var
  h: String;
begin
  Component := TrwComponentClass(ComponentClass).Create(Reader.Owner);
  h := ComponentName;
  ReaderOnSetName(Reader, Component, h);
end;

procedure TrwDesignArea.ReaderComponentReadCallback(Component: TComponent);
begin
  AddSelection(TrwComponent(Component), True);

  if Assigned(FContainerForPaste) and Component.InheritsFrom(TrwControl) then
  begin
    if ((TrwControl(Component).Left + TrwControl(Component).Width) > FContainerForPaste.Width) then
      TrwControl(Component).Left := FContainerForPaste.Width - TrwControl(Component).Width;
    if ((TrwControl(Component).Top + TrwControl(Component).Height) > FContainerForPaste.Height) then
      TrwControl(Component).Top := FContainerForPaste.Height - TrwControl(Component).Height;
  end;
end;

procedure TrwDesignArea.ReaderOnReferenceName(Reader: TReader; var Name: string);
var
  h: string;
//  lComp: TComponent;
begin
//  lComp := ComponentsOwner.FindComponent(Name);

//  if ((lComp is TrwPrintableContainer) or (lComp is TrwWinFormControl)) then
//  begin
    h := FSubstCompNamesOnPaste.Values[Name];
    if (h <> '') then
      Name := h;
{    else if Assigned(FContainerForPaste) then
    begin
      if (FContainerForPaste = ComponentsOwner) then
        Name := 'Owner'
      else
        Name := FContainerForPaste.Name;
    end
    else
      Name := '';
  end;}
end;

procedure TrwDesignArea.ReaderOnSetName(Reader: TReader; Component: TComponent; var Name: string);
var
  OldName: string;
begin
  OldName := Name;
  if Component.Owner.FindComponent(Name) <> nil then
    Name := GetUniqueNameForComponent(TrwComponent(Component));
  FSubstCompNamesOnPaste.Add(OldName + '=' + Name);

  if Component.InheritsFrom(TrwNoVisualComponent) then
    InitNoVisualComponent(TrwNoVisualComponent(Component));

  Component.Name := Name;

  if not (Component is TrwReportForm) then
    AfterCreationComponent(TrwComponent(Component), nil, 0, 0);
end;

procedure TrwDesignArea.ReaderOnFindMethod(Reader: TReader; const MethodName: string;
  var Address: Pointer; var Error: Boolean);
begin
  Error := False;
end;

procedure TrwDesignArea.Copy;
begin
  SelectionToMemStream(FClipbpardMS);
  MemStreamToClipboard(FClipbpardMS);
end;

procedure TrwDesignArea.Paste;
begin
  FContainerForPaste := nil;

  if (SelectedCompList.Count > 0) then
  begin
    if (SelectedCompList[0].InheritsFrom(TrwControl) and
       (TrwControl(SelectedCompList[0]).VisualControl.RealObject is TWinControl)) then
      FContainerForPaste := TrwControl(SelectedCompList[0])

    else if SelectedCompList[0].InheritsFrom(TrwChildComponent) then
      FContainerForPaste := TrwChildComponent(SelectedCompList[0]).Container

    else if SelectedCompList[0].InheritsFrom(TrwFormControl) then
      FContainerForPaste := TrwFormControl(SelectedCompList[0]).Parent;
  end

  else if ComponentsOwner.InheritsFrom(TrwControl) then
    FContainerForPaste := TrwControl(ComponentsOwner);

  if Assigned(FContainerForPaste) and
    (not (csAcceptsControls in FContainerForPaste.VisualControl.RealObject.ControlStyle)) then
    if FContainerForPaste is TrwChildComponent then
      FContainerForPaste := TrwChildComponent(FContainerForPaste).Container
    else
      FContainerForPaste := TrwFormControl(FContainerForPaste).Parent;

  MemStreamFromClipboard(FClipbpardMS);
  SelectionFromMemStream(FClipbpardMS);
  RefreshCompResizer;
end;

procedure TrwDesignArea.Cut;
begin
  Copy;
  DestroySelectedComponents;
end;

procedure TrwDesignArea.Undo;
begin
  FContainerForPaste := nil;
  if UndoIsNotEmpty then
  begin
    SelectionFromMemStream(FUnDoMS);
    FUnDoMS.Clear;
  end;  
end;

procedure TrwDesignArea.ExpertForSelection(AAction: TrwExpertToolBarEvent);

  procedure QueryBuilder;
  var
   QBI: TrwQBQuery;
  begin
    if SelectedCompList.Count = 1 then
    begin
      if not (SelectedCompList[0] is TrwQuery) then Exit;
      QBI := TrwQuery(SelectedCompList[0]).QueryBuilderInfo;
    end
    else if ComponentsOwner is TrwQuery then
      QBI := TrwQuery(ComponentsOwner).QueryBuilderInfo
    else
      QBI := nil;

    ShowQueryBuilder(QBI, False);
  end;

  procedure PrintOrder;
  begin
    if (SelectedCompList.Count = 1) and
       SelectedCompList[0].InheritsFrom(TrwPrintableContainer) then
      ShowPrintOrder(TrwPrintableContainer(SelectedCompList[0]));
  end;

  procedure ASCIIPrintOrder;
  var
    C: TrwChildComponent;
  begin
    if (SelectedCompList.Count > 0) and SelectedCompList[0].InheritsFrom(TrwChildComponent) then
    begin
      C := TrwChildComponent(SelectedCompList[0]);
      while  not (C is TrwBand) do
        C := C.Container;
      ShowASCIIPrintOrder(TrwBand(C));
    end;
  end;

  procedure TabOrder;
  var
    C: TrwFormControl;
  begin
    if SelectedCompList.Count > 0 then
    begin
      if not SelectedCompList[0].InheritsFrom(TrwFormControl) then
        Exit;

      C := TrwFormControl(SelectedCompList[0]);
      while  not (C is TrwWinFormControl) do
        C := C.Parent;
    end
    else
      C := TrwReport(ReportDesigner.DesignPaperReport[0].Report).InputForm;

    ShowTabOrder(TrwWinFormControl(C));
    ReportDesigner.frmInputForm.RefreshComponentProperties;
  end;

  procedure ShowVarFunct;
  begin
    if Owner = nil  then
      ShowGlobalVarFunct(ReportDesigner.CurrentReportPaper.Report.ReportForm)
    else
      ShowGlobalVarFunct(TrwComponent(ComponentsOwner));
  end;

  procedure ShowAllEvents;
  var
    C: TrwComponent;
  begin
    C := TrwComponent(ComponentsOwner);
    if C is TrwReportForm then
      C := C.VirtualOwner;
    ShowAllEventOfReport(C);
  end;

begin
  case AAction of
    eteQueryBuilder: QueryBuilder;
    etePrintOrder:   PrintOrder;
    eteASCIIPrintOrder:   ASCIIPrintOrder;
    eteLibrary:      ShowLibrary;
    eteLocalVarFunc: ShowVarFunct;
    eteAllEvents:    ShowAllEvents;
    eteTabOrder:     TabOrder;
  end;
end;

procedure TrwDesignArea.ActionForSelection(AAction: TrwActionToolBarEvent);

  procedure Alignment;
  var
    i: Integer;
    X1, Y1, X2, Y2: Integer;
  begin
    if SelectedCompList.Count <= 1 then Exit;

    if not SelectedCompList[0].InheritsFrom(TrwControl) then Exit;

    X1 := TrwControl(SelectedCompList[0]).Left;
    Y1 := TrwControl(SelectedCompList[0]).Top;
    X2 := TrwControl(SelectedCompList[0]).Left + TrwControl(SelectedCompList[0]).Width;
    Y2 := TrwControl(SelectedCompList[0]).Top + TrwControl(SelectedCompList[0]).Height;

    for i := 1 to SelectedCompList.Count - 1 do
    begin
      if not SelectedCompList[i].InheritsFrom(TrwControl) then Continue;

      case AAction of
        ateAlignmentLeft: TrwControl(SelectedCompList[i]).Left := X1;

        ateAlignmentBottom: TrwControl(SelectedCompList[i]).Top := Y2 - TrwControl(SelectedCompList[i]).Height;

        ateAlignmentTop: TrwControl(SelectedCompList[i]).Top := Y1;

        ateAlignmentRight: TrwControl(SelectedCompList[i]).Left := X2 - TrwControl(SelectedCompList[i]).Width;
      end;
    end;
  end;

  procedure AlignmentCenter;
  var
    i: Integer;
    X, Y: Integer;
  begin
    if SelectedCompList.Count <= 1 then Exit;

    if not SelectedCompList[0].InheritsFrom(TrwControl) then Exit;

    X := TrwControl(SelectedCompList[0]).Left + (TrwControl(SelectedCompList[0]).Width div 2);
    Y := TrwControl(SelectedCompList[0]).Top + (TrwControl(SelectedCompList[0]).Height div 2);

    for i := 1 to SelectedCompList.Count - 1 do
    begin
      if not SelectedCompList[i].InheritsFrom(TrwControl) then Continue;

      case AAction of
        ateAlignmentVertCenter: TrwControl(SelectedCompList[i]).Top := Y - (TrwControl(SelectedCompList[i]).Height div 2);

        ateAlignmentHorCenter: TrwControl(SelectedCompList[i]).Left := X - (TrwControl(SelectedCompList[i]).Width div 2);
      end;
    end;
  end;

  procedure AlignmentContainerCenter;
  var
    i: Integer;
  begin
    for i := 0 to SelectedCompList.Count - 1 do
    begin
      if not SelectedCompList[i].InheritsFrom(TrwControl) then Continue;

      if SelectedCompList[i].InheritsFrom(TrwChildComponent) then
        case AAction of
          ateAlignmentVertContainerCenter:
            TrwChildComponent(SelectedCompList[i]).Top := (TrwChildComponent(SelectedCompList[i]).Container.Height div 2) -
              (TrwChildComponent(SelectedCompList[i]).Height div 2);

          ateAlignmentHorContainerCenter:
            TrwFormControl(SelectedCompList[i]).Left := (TrwChildComponent(SelectedCompList[i]).Container.Width div 2) -
              (TrwChildComponent(SelectedCompList[i]).Width div 2);
        end

      else
        case AAction of
          ateAlignmentVertContainerCenter:
            TrwFormControl(SelectedCompList[i]).Top := (TrwFormControl(SelectedCompList[i]).Parent.Height div 2) -
              (TrwFormControl(SelectedCompList[i]).Height div 2);

          ateAlignmentHorContainerCenter:
            TrwFormControl(SelectedCompList[i]).Left := (TrwFormControl(SelectedCompList[i]).Parent.Width div 2) -
              (TrwFormControl(SelectedCompList[i]).Width div 2);
        end;
    end;

  end;

  procedure AlignmentSpaces;
  var
    i, j: Integer;
    lAllSize, lCompSize, lSpaceSize: Integer;
    lList: TList;
  begin
    if SelectedCompList.Count <= 2 then Exit;

    lList := TList.Create;

    try
      for i := 0 to SelectedCompList.Count - 1 do
      begin
        if not SelectedCompList[i].InheritsFrom(TrwControl) then Continue;
        lList.Add(SelectedCompList[i]);
      end;

      case AAction of
        ateAlignmentVertSpaces:
          begin
            for i := 0 to lList.Count - 1 do
              for j := i + 1 to lList.Count - 1 do
                if (TrwControl(lList[j]).Top < TrwControl(lList[i]).Top) then
                  lList.Exchange(j, i);

            lAllSize := TrwControl(lList[lList.Count - 1]).Top - TrwControl(lList[0]).Top - TrwControl(lList[0]).Height;
            lCompSize := 0;

            for i := 1 to lList.Count - 2 do
              lCompSize := lCompSize + TrwControl(lList[i]).Height;

            lSpaceSize := (lAllSize - lCompSize) div (lList.Count - 1);

            for i := 1 to lList.Count - 2 do
              TrwControl(lList[i]).Top := TrwControl(lList[i - 1]).Top + TrwControl(lList[i - 1]).Height + lSpaceSize;

          end;

        ateAlignmentHorSpaces:
          begin
            for i := 0 to lList.Count - 1 do
              for j := i + 1 to lList.Count - 1 do
                if (TrwControl(lList[j]).Left < TrwControl(lList[i]).Left) then
                  lList.Exchange(j, i);

            lAllSize := TrwControl(lList[lList.Count - 1]).Left - TrwControl(lList[0]).Left - TrwControl(lList[0]).Width;
            lCompSize := 0;

            for i := 1 to lList.Count - 2 do
              lCompSize := lCompSize + TrwControl(lList[i]).Width;

            lSpaceSize := (lAllSize - lCompSize) div (lList.Count - 1);

            for i := 1 to lList.Count - 2 do
              TrwControl(lList[i]).Left := TrwControl(lList[i - 1]).Left + TrwControl(lList[i - 1]).Width + lSpaceSize;
          end;
      end;

    finally
      lList.Free;
    end;
  end;

  procedure GrowShrinkSize;
  var
    i: Integer;
    lSize: Integer;
  begin
    if SelectedCompList.Count <= 1 then Exit;

    lSize := 0;

    case AAction of
      ateGrowHeightToLargest:
        begin
          lSize := 0;
          for i := 0 to SelectedCompList.Count - 1 do
          begin
            if not SelectedCompList[i].InheritsFrom(TrwControl) then Continue;
            if (TrwControl(SelectedCompList[i]).Height > lSize) then
              lSize := TrwControl(SelectedCompList[i]).Height;
          end;
        end;

      ateShrinkHeightToSmallest:
        begin
          lSize := 16000;
          for i := 0 to SelectedCompList.Count - 1 do
          begin
            if not SelectedCompList[i].InheritsFrom(TrwControl) then Continue;
            if (TrwControl(SelectedCompList[i]).Height < lSize) then
              lSize := TrwControl(SelectedCompList[i]).Height;
          end;
        end;

      ateGrowWidthToLargest:
        begin
          lSize := 0;
          for i := 0 to SelectedCompList.Count - 1 do
          begin
            if not SelectedCompList[i].InheritsFrom(TrwControl) then Continue;
            if (TrwControl(SelectedCompList[i]).Width > lSize) then
              lSize := TrwControl(SelectedCompList[i]).Width;
          end;
        end;

      ateShrinkWidthToSmallest:
        begin
          lSize := 16000;
          for i := 0 to SelectedCompList.Count - 1 do
          begin
            if not SelectedCompList[i].InheritsFrom(TrwControl) then Continue;
            if (TrwControl(SelectedCompList[i]).Width < lSize) then
              lSize := TrwControl(SelectedCompList[i]).Width;
          end;
        end;

    end;

    case AAction of
      ateGrowHeightToLargest,
        ateShrinkHeightToSmallest:
        for i := 0 to SelectedCompList.Count - 1 do
        begin
          if not SelectedCompList[i].InheritsFrom(TrwControl) then Continue;
          TrwControl(SelectedCompList[i]).Height := lSize;
        end;

      ateGrowWidthToLargest,
        ateShrinkWidthToSmallest:
        for i := 0 to SelectedCompList.Count - 1 do
        begin
          if not SelectedCompList[i].InheritsFrom(TrwControl) then Continue;
          TrwControl(SelectedCompList[i]).Width := lSize;
        end;
    end;
  end;


  procedure ZOrder;
  var
    i: Integer;
  begin
    if SelectedCompList.Count = 0 then Exit;

    for i := 0 to SelectedCompList.Count - 1 do
    begin
      if not SelectedCompList[i].InheritsFrom(TrwControl) then Continue;

      case AAction of
        ateBringToFront: SelectedCompList.Resizers[i].BringToFront;
        ateSendToBack:   SelectedCompList.Resizers[i].SendToBack;
      end;
    end;
  end;

begin
  case AAction of
    ateAlignmentLeft,
      ateAlignmentBottom,
      ateAlignmentTop,
      ateAlignmentRight: Alignment;

    ateAlignmentVertCenter,
      ateAlignmentHorCenter: AlignmentCenter;

    ateAlignmentHorContainerCenter,
      ateAlignmentVertContainerCenter: AlignmentContainerCenter;

    ateAlignmentVertSpaces,
      ateAlignmentHorSpaces: AlignmentSpaces;

    ateGrowHeightToLargest,
      ateShrinkHeightToSmallest,
      ateGrowWidthToLargest,
      ateShrinkWidthToSmallest: GrowShrinkSize;

    ateBringToFront,
    ateSendToBack: ZOrder;
  end;

  if not (AAction in [ateBringToFront, ateSendToBack]) then
    RefreshCompResizer;
end;

procedure TrwDesignArea.InheritedCompsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  P: TPoint;
  M: TMethod;
begin
  P.X := X;
  P.Y := Y;
  P := TControl(Sender).ClientToScreen(P);
  P := TControl(Sender).Parent.ScreenToClient(P);

  M := GetMethodProp(TControl(Sender).Parent, 'OnMouseDown');
  if Assigned(M.Code) then
    TMouseEvent(M)(TControl(Sender).Parent, Button, Shift, P.X, P.Y);
end;

procedure TrwDesignArea.ShowDefProperty(Sender: TObject);
var
  i: Integer;
  lCtrlResizer: TrwControlResizer;
begin
  lCtrlResizer := nil;
  
  if Sender is TrwControlResizer then
    Sender := TrwControlResizer(Sender).Component
  else if Sender is TControl then
  begin
    lCtrlResizer := SelectedCompList.ResizerByControl(TControl(Sender));
    if Assigned(lCtrlResizer) then
    begin
      Sender := lCtrlResizer.Component;
    end
    else
      Exit;
  end
  else
    Exit;

  i := DefPropComponents.IndexOf(Sender.ClassName);
  if (i <> -1) and Assigned(FOnShowDefProperty) then
  begin
    FOnShowDefProperty(Sender, DefPropComponents[i].PropName);
    if Assigned(lCtrlResizer) then
      PostMessage(lCtrlResizer.Handle, WM_LBUTTONUP, MK_LBUTTON, MakeLong(0, 0));
  end;
end;


procedure TrwDesignArea.NewPage1Click(Sender: TObject);
var
  PC: TrwPageControl;
begin
  PC := TrwPageControl(SelectedCompList[0]);
  FNewComponentClassName := 'TrwTabSheet';
  CreateNewComponent(PC, 0, 0, True);
end;


procedure TrwDesignArea.NextPage1Click(Sender: TObject);
var
  PC: TrwPageControl;
begin
  PC := TrwPageControl(SelectedCompList[0]);
  if PC.ActivePageIndex = PC.PageCount - 1 then
    PC.ActivePageIndex := 0
  else
    PC.ActivePageIndex := PC.ActivePageIndex + 1;
  FOnSetProperty(Self, PC);
end;

procedure TrwDesignArea.PreviousPage1Click(Sender: TObject);
var
  PC: TrwPageControl;
begin
  PC := TrwPageControl(SelectedCompList[0]);
  if PC.ActivePageIndex = 0 then
    PC.ActivePageIndex := PC.PageCount - 1
  else
    PC.ActivePageIndex := PC.ActivePageIndex - 1;
  FOnSetProperty(Self, PC);
end;


procedure TrwDesignArea.ShowRullerPos(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  P: TPoint;
begin
  if Assigned(pnlArea.OnMouseMove) then
  begin
    P := Point(X, Y);
    P := TControl(Sender).ClientToScreen(P);
    P := pnlArea.ScreenToClient(P);
    pnlArea.OnMouseMove(pnlArea, [], P.X, P.Y);
  end;
end;

function TrwDesignArea.UndoIsNotEmpty: Boolean;
begin
  Result := FUnDoMS.Size > 0;
end;



{ TrwArea }

constructor TrwArea.Create(AOwner: TComponent);
begin
  inherited;
end;

procedure TrwDesignArea.PrepDsgnComp(AComp: TrwComponent);
begin
  SetComponentsMouseEvent(AComp);
  if (AComp is TrwControl) then
    SetInheritedComponentsMouseEvent(AComp);
  AComp.OnDestroy := BeforeDestroyingComponent;
end;

procedure TrwDesignArea.Notification(AComponent: TComponent; Operation: TOperation);
var
  i: Integer;
begin
  if (Operation = opRemove) and (AComponent is TrwControlResizer) then
  begin
    i := FSelectedCompList.IndexOf(AComponent);
    if i <> - 1 then
      TList(FSelectedCompList).Delete(i);
  end;

  inherited;
end;

initialization
  FClipbpardMS := TIsMemoryStream.Create;
  FSubstCompNamesOnPaste := TStringList.Create;

finalization
  FClipbpardMS.Free;
  FSubstCompNamesOnPaste.Free;

end.
