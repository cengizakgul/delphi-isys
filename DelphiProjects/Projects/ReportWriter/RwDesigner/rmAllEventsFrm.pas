unit rmAllEventsFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwReport, mwCustomEdit, rwBasicClasses, rwTypes, rwDsgnUtils, rwUtils, rmTypes,
  EvStreamUtils, rmReport;

type
  TrmAllEvents = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    EventText: TmwCustomEdit;
    FStream: TStream;
    procedure FillCompEvents(AComponent: TrwComponent; AOwnerName: String);
    procedure FillCompChildren(AIrmDesignObject: IrmDesignObject; AOwnerName: String);
  public
  end;

  procedure ShowRMAllEventOfReport(AIrmDesignObject : IrmDesignObject);

implementation

var Frm: TrmAllEvents;

{$R *.DFM}

procedure ShowRMAllEventOfReport(AIrmDesignObject: IrmDesignObject);
var
  h: String;
begin
  if Assigned(Frm) or (not Assigned(AIrmDesignObject)) then
    Exit;

  Frm := TrmAllEvents.Create(nil);
  with Frm do
    try

      h := GetThreadTmpDir + '~tmpEvents.txt';
      FStream := TEvFileStream.Create(h, fmCreate);
      EventText.BeginUpdate;
      try
        FillCompChildren(AIrmDesignObject, AIrmDesignObject.RealObject.Name);
        FStream.Position := 0;
        EventText.Lines.LoadFromStream(FStream);
      finally
        EventText.EndUpdate;
        FStream.Free;
        DeleteFile(h);
      end;

      ShowModal;
    finally
      Free;
      Frm := nil;
    end;
end;

procedure TrmAllEvents.FormCreate(Sender: TObject);
begin
  EventText := TmwCustomEdit.Create(Self);
  EventText.Align := alClient;
  EventText.Parent := Self;
  EventText.ReadOnly := True;
  EventText.HighLighter := GeneralSyn;
end;


procedure TrmAllEvents.FillCompEvents(AComponent: TrwComponent; AOwnerName: String);
var
  i:  Integer;
  Ev: TrwEvent;
  h:  string;
  A:  PChar;
begin
  for i := 0 to AComponent.EventCount-1 do
  begin
    Ev := AComponent.EventByIndex(i);
    if Ev.HandlersText <> '' then
    begin
      if FStream.Size > 0 then
        FStream.Write(#13#10#13#10+'//*******************************************'+#13#10, 51);
      if VAR_FUNCT_COMP_NAME <> AComponent.Name then
      begin
        h := AComponent.Name;
        if (AOwnerName <> '') then
          h := AOwnerName+'.'+h;
      end
      else
        h := '';
      h := MakeEventHandlerHeader(Ev, h) + #13 + #10 + Ev.HandlersText + #13 + #10 + 'end';
      A := AllocMem(Length(h)+1);
      try
        StrPCopy(A, h);
        FStream.Write(A^, Length(h));
      finally
        FreeMem(A);
      end;
    end;
  end;
end;


procedure TrmAllEvents.FillCompChildren(AIrmDesignObject: IrmDesignObject; AOwnerName: String);
var
  i: Integer;
  C: IrmDesignObject;
begin
  for i := 0 to AIrmDesignObject.GetChildrenCount - 1 do
  begin
    C := AIrmDesignObject.GetChildByIndex(i);
    if (C.RealObject is TrwComponent) then
    begin
      FillCompEvents((C.RealObject as TrwComponent), AOwnerName);
//        if not (TrwComponent(C.RealObject).InheritedComponent and TrwComponent(AIrmDesignObject.RealObject).IsLibComponent) then
      FillCompChildren(TrwComponent(C.RealObject), AOwnerName + '.' + C.RealObject.Name)
    end;
  end;
end;


procedure TrmAllEvents.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_ESCAPE then
    Close;
end;

initialization
  Frm := nil;

end.
