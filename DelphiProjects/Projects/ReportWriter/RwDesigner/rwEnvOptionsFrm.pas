unit rwEnvOptionsFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, rwDsgnUtils, rwUtils;

type
  TrwEnvOptions = class(TForm)
    btnOK: TButton;
    btnCancel: TButton;
    evPageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    chbCodeCompl: TCheckBox;
    chbCodeParam: TCheckBox;
    trkDelay: TTrackBar;
    chbExprEv: TCheckBox;
    chbSymbInsight: TCheckBox;
    GroupBox2: TGroupBox;
    chbTraceLib: TCheckBox;
    chbAutoCompile: TCheckBox;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    chbShowData: TCheckBox;
    cbUnits: TComboBox;
    chbDontAskConfirm: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
  public
  end;

  function ShowEnvOptions: Boolean;

implementation

{$R *.DFM}


function ShowEnvOptions: Boolean;
var
  Frm: TrwEnvOptions;
begin
  Frm := TrwEnvOptions.Create(Application);
  try
    Result := Frm.ShowModal = mrOk;
  finally
    Frm.Free;
  end;
end;



procedure TrwEnvOptions.FormShow(Sender: TObject);
begin
  chbCodeCompl.Checked := DsgnCodeCompletion;
  chbCodeParam.Checked := DsgnCodeParams;
  chbExprEv.Checked := DsgnCodeEvaluation;
  chbSymbInsight.Checked := DsgnSymbInsight;
  trkDelay.Position := DsgnCodeInsightDelay;

  chbTraceLib.Checked := DsgnTraceLib;
  chbDontAskConfirm.Checked := DsgnDontAskCompileConfirm;
  chbAutoCompile.Checked := DsgnAutoCompile;

  chbShowData.Checked := DsgnShowData;
  cbUnits.ItemIndex := Ord(DesignUnits);
end;

procedure TrwEnvOptions.btnOKClick(Sender: TObject);
begin
  DsgnCodeCompletion := chbCodeCompl.Checked;
  DsgnCodeParams := chbCodeParam.Checked;
  DsgnCodeEvaluation := chbExprEv.Checked;
  DsgnSymbInsight := chbSymbInsight.Checked;
  DsgnCodeInsightDelay := trkDelay.Position;

  DsgnTraceLib := chbTraceLib.Checked;
  DsgnDontAskCompileConfirm := chbDontAskConfirm.Checked;
  DsgnAutoCompile := chbAutoCompile.Checked;

  DsgnShowData := chbShowData.Checked;
  DesignUnits := TrwUnitType(cbUnits.ItemIndex);

  WriteDsgnEnv;
end;

end.
