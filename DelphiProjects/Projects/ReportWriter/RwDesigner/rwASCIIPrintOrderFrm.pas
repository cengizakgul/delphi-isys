unit rwASCIIPrintOrderFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, rwCommonClasses, Mask, ComCtrls, Buttons, rwDsgnRes,
  rwBasicClasses, rwReport, ExtCtrls, rwUtils, wwdbedit, Wwdbspin;

type

  TrwObjInfo = class(TCollectionItem)
  public
    Instance: TrwCustomText;
    LineNbr:  Integer;
    ColNbr:   Integer;
    Indent:   Integer;
    Length:   Integer;
    StartPos: Integer;
    EndPos:   Integer;
  end;


  TrwObjList = class (TCollection)
  private
    function GetItem(Index: Integer): TrwObjInfo;
    procedure SetItem(Index: Integer; const Value: TrwObjInfo);
  public
    property Items[Index: Integer]: TrwObjInfo read GetItem write SetItem; default;
  end;


  TrwASCIIPrintOrder = class(TForm)
    tcLines: TTabControl;
    lvASCIIObj: TListView;
    Label6: TLabel;
    lvAvailObj: TListView;
    btnAdd: TSpeedButton;
    btnRemove: TSpeedButton;
    Panel1: TPanel;
    Label3: TLabel;
    edNbr: TwwDBSpinEdit;
    Label1: TLabel;
    edIndent: TwwDBSpinEdit;
    Label2: TLabel;
    edLength: TwwDBSpinEdit;
    Label4: TLabel;
    btnOK: TButton;
    btnCancel: TButton;
    lTotalLength: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure tcLinesChange(Sender: TObject);
    procedure edNbrExit(Sender: TObject);
    procedure edIndentExit(Sender: TObject);
    procedure edLengthExit(Sender: TObject);
    procedure edNbrKeyPress(Sender: TObject; var Key: Char);
    procedure lvASCIIObjChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure btnAddClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
    procedure lvAvailObjEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure lvASCIIObjDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure lvAvailObjDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure lvASCIIObjEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure edLengthAfterDownClick(Sender: TObject);
    procedure lvASCIIObjKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edLengthKeyPress(Sender: TObject; var Key: Char);
  private
    FObjList: TrwObjList;
    FBand: TrwBand;

    procedure CreateAvailableList;
    procedure ApplayChanges;
    procedure UpdateAvailListView;
    procedure UpdateASCIIListView;
    procedure UpdateASCIIInfo;
    procedure AddObjToAvailList(AInfo: TrwObjInfo);
    procedure AddObjToASCIIList(AInfo: TrwObjInfo);
    procedure CheckEditMode;
    procedure SelectItem(AListView: TListView; AData: Pointer);
    procedure MoveItem(AObjInfo: TrwObjInfo; ANewColValue: Integer);

  public
  end;

  procedure ShowASCIIPrintOrder(ABand: TrwBand);

implementation

{$R *.dfm}


procedure ShowASCIIPrintOrder(ABand: TrwBand);
var
  Frm: TrwASCIIPrintOrder;
  C: TrwComponent;
begin
  Frm := TrwASCIIPrintOrder.Create(Application);
  with Frm do
    try
      FBand := ABand;
      C := FBand.VirtualOwner;
      while not (C is TrwReport) do
      begin
        C := C.VirtualOwner;
        if C is TrwSubReport then
          C := TrwComponent(C.Owner.Owner);
      end;

      if TrwReport(C).ASCIIDelimiter <> '' then
      begin
        lvASCIIObj.Columns[6].Free;
        lvASCIIObj.Columns[5].Free;
        lvASCIIObj.Columns[4].Free;

        Label4.Visible := False;
        lTotalLength.Visible := False;
        Label2.Visible := False;
        edLength.Visible := False;
      end;

      Caption := 'ASCII print order for ' + ABand.Name;
      CreateAvailableList;

      if Frm.ShowModal = mrOK then
      begin
        CheckInheritedAndShowError(ABand);
        ApplayChanges;
      end;

    finally
      Frm.Free;
    end;
end;



{ TrwObjList }

function TrwObjList.GetItem(Index: Integer): TrwObjInfo;
begin
  Result := TrwObjInfo(inherited Items[Index]);
end;


procedure TrwObjList.SetItem(Index: Integer; const Value: TrwObjInfo);
begin
  TrwObjInfo(inherited Items[Index]).Assign(Value);
end;



{ TrwASCIIPrintOrder }

procedure TrwASCIIPrintOrder.CreateAvailableList;
var
  i, MaxLine: Integer;

  procedure AddInfo(AContainer: TrwPrintableContainer);
  var
    i: Integer;
    C: TrwPrintableControl;
    ObjInfo: TrwObjInfo;
  begin
    for i := 0 to AContainer.ControlCount - 1 do
    begin
      C := AContainer.Controls[i];
      if C.InheritsFrom(TrwCustomText) then
      begin
        ObjInfo := TrwObjInfo(FObjList.Add);
        ObjInfo.Instance := TrwCustomText(C);
        ObjInfo.LineNbr := TrwCustomText(C).ASCIIPrintLineSequence;
        ObjInfo.ColNbr := TrwCustomText(C).ASCIIPrintColSequence;
        ObjInfo.Indent := TrwCustomText(C).ASCIIIndent;
        ObjInfo.Length := TrwCustomText(C).ASCIISize;
        if MaxLine < ObjInfo.LineNbr then
          MaxLine := ObjInfo.LineNbr;
      end
      else if C.InheritsFrom(TrwPrintableContainer) then
        AddInfo(TrwPrintableContainer(C));
    end;
  end;

begin
  MaxLine := 0;
  FObjList.Clear;
  AddInfo(FBand);

  tcLines.Tabs.Clear;
  for i := 1 to MaxLine + 1 do
    tcLines.Tabs.Add('Line #' + IntToStr(i));

  UpdateAvailListView;
  UpdateASCIIListView;
end;


procedure TrwASCIIPrintOrder.ApplayChanges;
var
  i: Integer;
begin
  for i := 0 to FObjList.Count - 1 do
  begin
    FObjList[i].Instance.ASCIIPrintLineSequence := FObjList[i].LineNbr;
    FObjList[i].Instance.ASCIIIndent := FObjList[i].Indent;
    FObjList[i].Instance.ASCIISize := FObjList[i].Length;
    FObjList[i].Instance.ASCIIPrintColSequence := FObjList[i].ColNbr;
  end;
end;


procedure TrwASCIIPrintOrder.FormCreate(Sender: TObject);
begin
  FObjList := TrwObjList.Create(TrwObjInfo);
end;


procedure TrwASCIIPrintOrder.FormDestroy(Sender: TObject);
begin
  FObjList.Free;
end;


procedure TrwASCIIPrintOrder.UpdateAvailListView;
var
  i: Integer;
begin
  lvAvailObj.Items.BeginUpdate;

  try
    lvAvailObj.Clear;

    for i := 0 to FObjList.Count - 1 do
    begin
      if FObjList[i].LineNbr = 0 then
        AddObjToAvailList(FObjList[i]);
    end;

  finally
    lvAvailObj.Items.EndUpdate;
  end;
end;


procedure TrwASCIIPrintOrder.UpdateASCIIListView;
var
  i, j, l: Integer;
  SI, P: Pointer;
  A: Array of TrwObjInfo;
begin
  l := tcLines.TabIndex + 1;

  j := 0;
  SetLength(A, FObjList.Count);
  for i := 0 to FObjList.Count - 1 do
  begin
    if FObjList[i].LineNbr = l then
    begin
      A[j] := FObjList[i];
      Inc(j);
    end;
  end;

  SetLength(A, j);
  for i := Low(A) to High(A) - 1 do
    for j := i + 1 to High(A) do
      if A[i].ColNbr > A[j].ColNbr then
      begin
        P := A[i];
        A[i] := A[j];
        A[j] := P;
      end;

  for i := Low(A) to High(A) do
  begin
    A[i].ColNbr := i + 1;
    if i = 0 then
      A[i].StartPos := 1 + A[i].Indent
    else
      A[i].StartPos := A[i - 1].EndPos + 1 + A[i].Indent;
    A[i].EndPos := A[i].StartPos + A[i].Length - 1;
  end;

  if High(A) > -1 then
    lTotalLength.Caption := IntToStr(A[High(A)].EndPos)
  else
    lTotalLength.Caption := '';


  lvASCIIObj.Items.BeginUpdate;
  lvASCIIObj.Tag := 1;
  try
    if Assigned(lvASCIIObj.Selected) then
      SI := lvASCIIObj.Selected.Data
    else
      SI := nil;
    lvASCIIObj.Selected := nil;
    lvASCIIObj.Clear;

    for i := Low(A) to High(A) do
      AddObjToASCIIList(A[i]);

    if Assigned(SI) then
      SelectItem(lvASCIIObj, SI)
    else if lvASCIIObj.Items.Count > 0 then
      lvASCIIObj.Selected := lvASCIIObj.Items[0];

    if Assigned(lvASCIIObj.Selected) then
      lvASCIIObj.Selected.MakeVisible(False);

  finally
    lvASCIIObj.Items.EndUpdate;
    lvASCIIObj.Tag := 0;
  end;

  if (lvASCIIObj.Items.Count > 0) and (l = tcLines.Tabs.Count) then
    tcLines.Tabs.Add('Line #' + IntToStr(l + 1));

  CheckEditMode;
end;


procedure TrwASCIIPrintOrder.AddObjToAvailList(AInfo: TrwObjInfo);
var
  LI: TListItem;
begin
  LI := lvAvailObj.Items.Add;
  LI.Caption := AInfo.Instance.Name;
  LI.Data := AInfo;
  LI.SubItems.Add(AInfo.Instance.PClassName);
  LI.SubItems.Add(AInfo.Instance.Container.Name);
end;


procedure TrwASCIIPrintOrder.AddObjToASCIIList(AInfo: TrwObjInfo);
var
  LI: TListItem;
begin
  LI := lvASCIIObj.Items.Add;
  LI.Caption := IntToStr(AInfo.ColNbr);
  LI.Data := AInfo;
  LI.SubItems.Add(AInfo.Instance.Name);
  LI.SubItems.Add(AInfo.Instance.PClassName);
  LI.SubItems.Add(IntToStr(AInfo.Indent));
  LI.SubItems.Add(IntToStr(AInfo.Length));
  LI.SubItems.Add(IntToStr(AInfo.StartPos));
  LI.SubItems.Add(IntToStr(AInfo.EndPos));
end;


procedure TrwASCIIPrintOrder.tcLinesChange(Sender: TObject);
begin
  UpdateASCIIListView;
end;

procedure TrwASCIIPrintOrder.edNbrExit(Sender: TObject);
var
  n: Integer;
begin
  if Assigned(lvASCIIObj.Selected) then
  begin
    n := Trunc(edNbr.Value);
    if (n > TrwObjInfo(lvASCIIObj.Items[lvASCIIObj.Items.Count - 1].Data).ColNbr) or (n < 0) then
      edNbr.Value := TrwObjInfo(lvASCIIObj.Selected.Data).ColNbr
    else
      MoveItem(TrwObjInfo(lvASCIIObj.Selected.Data), n);
  end;
end;

procedure TrwASCIIPrintOrder.edIndentExit(Sender: TObject);
begin
  if Assigned(lvASCIIObj.Selected) then
  begin
    TrwObjInfo(lvASCIIObj.Selected.Data).Indent := Trunc(edIndent.Value);
    lvASCIIObj.Selected.SubItems[2] := IntToStr(TrwObjInfo(lvASCIIObj.Selected.Data).Indent);
    UpdateASCIIInfo;
  end;
end;

procedure TrwASCIIPrintOrder.edLengthExit(Sender: TObject);
begin
  if Assigned(lvASCIIObj.Selected) then
  begin
    TrwObjInfo(lvASCIIObj.Selected.Data).Length := Trunc(edLength.Value);
    lvASCIIObj.Selected.SubItems[3] := IntToStr(TrwObjInfo(lvASCIIObj.Selected.Data).Length);
    UpdateASCIIInfo;
  end;
end;

procedure TrwASCIIPrintOrder.edNbrKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    TwwDBSpinEdit(Sender).OnExit(Sender);
end;

procedure TrwASCIIPrintOrder.CheckEditMode;
begin
  edNbr.Enabled := Assigned(lvASCIIObj.Selected);
  edIndent.Enabled := edNbr.Enabled;
  edLength.Enabled := edNbr.Enabled;

  if not edNbr.Enabled then
  begin
    edNbr.Value := 0;
    edIndent.Value := 0;
    edLength.Value := 0;
  end
  else
  begin
    edNbr.Value := TrwObjInfo(lvASCIIObj.Selected.Data).ColNbr;
    edIndent.Value := TrwObjInfo(lvASCIIObj.Selected.Data).Indent;
    edLength.Value := TrwObjInfo(lvASCIIObj.Selected.Data).Length;
  end;
end;

procedure TrwASCIIPrintOrder.lvASCIIObjChange(Sender: TObject;
  Item: TListItem; Change: TItemChange);
begin
  if lvASCIIObj.Tag = 0 then
    CheckEditMode;
end;

procedure TrwASCIIPrintOrder.btnAddClick(Sender: TObject);
var
  OI: TrwObjInfo;
  i: Integer;
begin
  if Assigned(lvAvailObj.Selected) then
  begin
    OI := TrwObjInfo(lvAvailObj.Selected.Data);
    OI.LineNbr := tcLines.TabIndex + 1;

    if Sender is TListItem then
    begin
      OI.ColNbr := 10000000;
      MoveItem(OI, TrwObjInfo(TListItem(Sender).Data).ColNbr);
    end

    else
    begin
      if lvASCIIObj.Items.Count = 0 then
        OI.ColNbr := 1
      else
        OI.ColNbr := TrwObjInfo(lvASCIIObj.Items[lvASCIIObj.Items.Count - 1].Data).ColNbr + 1;

      UpdateASCIIListView;
      SelectItem(lvASCIIObj, OI);
    end;

    i := lvAvailObj.Selected.Index;
    UpdateAvailListView;
    if i < lvAvailObj.Items.Count then
      lvAvailObj.Selected := lvAvailObj.Items[i]
    else if lvAvailObj.Items.Count > 0 then
      lvAvailObj.Selected := lvAvailObj.Items[lvAvailObj.Items.Count - 1];

    if Assigned(lvAvailObj.Selected) then
      lvAvailObj.Selected.MakeVisible(False);
  end;
end;


procedure TrwASCIIPrintOrder.SelectItem(AListView: TListView; AData: Pointer);
var
  i: Integer;
begin
  for i := 0 to AListView.Items.Count - 1 do
  begin
    if AListView.Items[i].Data = AData then
    begin
      AListView.Selected := AListView.Items[i];
      if Assigned(AListView.Selected) then
        AListView.Selected.MakeVisible(False);
      break;
    end;
  end
end;


procedure TrwASCIIPrintOrder.btnRemoveClick(Sender: TObject);
var
  OI: TrwObjInfo;
  i, l: Integer;
begin
  if Assigned(lvASCIIObj.Selected) then
  begin
    OI := TrwObjInfo(lvASCIIObj.Selected.Data);
    OI.LineNbr := 0;

    for i := lvASCIIObj.Selected.Index + 1 to lvASCIIObj.Items.Count - 1 do
      TrwObjInfo(lvASCIIObj.Items[i].Data).ColNbr := TrwObjInfo(lvASCIIObj.Items[i].Data).ColNbr - 1;

    if lvASCIIObj.Items.Count = 1 then
    begin
      l := tcLines.TabIndex + 1;
      for i := 0 to FObjList.Count - 1 do
        if FObjList[i].LineNbr > l then
          FObjList[i].LineNbr := FObjList[i].LineNbr - 1;
    end;

    if tcLines.TabIndex < tcLines.Tabs.Count - 1 then
    begin
      for i := tcLines.TabIndex + 1 to tcLines.Tabs.Count - 1 do
        tcLines.Tabs[i] := 'Line #' + IntToStr(i);

      tcLines.Tabs.Delete(tcLines.TabIndex);
      tcLines.TabIndex := 0;
    end;

    l := lvASCIIObj.Selected.Index;
    UpdateASCIIListView;
    if l < lvASCIIObj.Items.Count then
      lvASCIIObj.Selected := lvASCIIObj.Items[l]
    else if lvASCIIObj.Items.Count > 0 then
      lvASCIIObj.Selected := lvASCIIObj.Items[lvASCIIObj.Items.Count - 1];

    if Assigned(lvASCIIObj.Selected) then
      lvASCIIObj.Selected.MakeVisible(False);

    UpdateAvailListView;
    SelectItem(lvAvailObj, OI);
  end;
end;

procedure TrwASCIIPrintOrder.lvAvailObjEndDrag(Sender, Target: TObject; X,
  Y: Integer);
begin
  if Target = lvASCIIObj then
    btnAdd.OnClick(lvASCIIObj.GetItemAt(X, Y));
end;

procedure TrwASCIIPrintOrder.lvASCIIObjDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (Source = lvAvailObj) or (Source = lvASCIIObj);
end;

procedure TrwASCIIPrintOrder.lvAvailObjDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := Source = lvASCIIObj;
end;

procedure TrwASCIIPrintOrder.lvASCIIObjEndDrag(Sender, Target: TObject; X,
  Y: Integer);
var
  LI: TListItem;
  n: Integer;
begin
  if Target = lvAvailObj then
    btnRemove.Click

  else if (Target = lvASCIIObj) and Assigned(lvASCIIObj.Selected) then
  begin
    LI := lvASCIIObj.GetItemAt(X, Y);
    if Assigned(LI) then
      n := TrwObjInfo(LI.Data).ColNbr
    else
    begin
      if lvASCIIObj.Selected.Index =  lvASCIIObj.Items.Count - 1 then
        Exit;
      n :=  TrwObjInfo(lvASCIIObj.Items[lvASCIIObj.Items.Count - 1].Data).ColNbr + 1;
    end;
    
    MoveItem(TrwObjInfo(lvASCIIObj.Selected.Data), n);
  end;
end;

procedure TrwASCIIPrintOrder.MoveItem(AObjInfo: TrwObjInfo; ANewColValue: Integer);
var
 i: Integer;
begin
  if AObjInfo.ColNbr = ANewColValue then
    Exit

  else if AObjInfo.ColNbr < ANewColValue then
  begin
    for i := 0 to FObjList.Count - 1 do
      if (FObjList[i].LineNbr = AObjInfo.LineNbr) and
         (FObjList[i].ColNbr > AObjInfo.ColNbr) and
         (FObjList[i].ColNbr <= ANewColValue)  then
        FObjList[i].ColNbr := FObjList[i].ColNbr - 1
  end

  else if AObjInfo.ColNbr > ANewColValue then
  begin
    for i := 0 to FObjList.Count - 1 do
      if (FObjList[i].LineNbr = AObjInfo.LineNbr) and
         (FObjList[i].ColNbr >= ANewColValue) and
         (FObjList[i].ColNbr < AObjInfo.ColNbr)  then
        FObjList[i].ColNbr := FObjList[i].ColNbr + 1
  end;

  AObjInfo.ColNbr := ANewColValue;
  UpdateASCIIListView;
  SelectItem(lvASCIIObj, AObjInfo);
end;


procedure TrwASCIIPrintOrder.edLengthAfterDownClick(Sender: TObject);
begin
  TwwDBSpinEdit(Sender).OnExit(Sender);
end;

procedure TrwASCIIPrintOrder.lvASCIIObjKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key= VK_DELETE then
    btnRemove.Click;
end;

procedure TrwASCIIPrintOrder.UpdateASCIIInfo;
var
  i: Integer;
  A: TrwObjInfo;
begin
  A := nil;
  for i := 0 to lvASCIIObj.Items.Count - 1 do
  begin
    A := TrwObjInfo(lvASCIIObj.Items[i].Data);
    if i = 0 then
      A.StartPos := 1 + A.Indent
    else
      A.StartPos := TrwObjInfo(lvASCIIObj.Items[i - 1].Data).EndPos + 1 + A.Indent;
    A.EndPos := A.StartPos + A.Length - 1;

    lvASCIIObj.Items[i].Caption := IntToStr(A.ColNbr);
    lvASCIIObj.Items[i].SubItems[2] := IntToStr(A.Indent);
    lvASCIIObj.Items[i].SubItems[3] := IntToStr(A.Length);
    lvASCIIObj.Items[i].SubItems[4] := IntToStr(A.StartPos);
    lvASCIIObj.Items[i].SubItems[5] := IntToStr(A.EndPos);
  end;

  if Assigned(A) then
    lTotalLength.Caption := IntToStr(A.EndPos)
  else
    lTotalLength.Caption := '';
end;

procedure TrwASCIIPrintOrder.edLengthKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
  begin
    edLength.OnExit(edLength);
    if lvASCIIObj.Selected.Index < lvASCIIObj.Items.Count - 1 then
    begin
      lvASCIIObj.Selected := lvASCIIObj.Items[lvASCIIObj.Selected.Index + 1];
      lvASCIIObj.Selected.MakeVisible(False);
      edLength.SetFocus;
    end;
  end;
end;

end.
