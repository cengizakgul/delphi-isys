unit rmLocalToolBarFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmCustomFrameFrm, ExtCtrls, rwDsgnUtils, Menus, ComCtrls,
  ActnList, ImgList, rmDsgnTypes, EvStreamUtils;

type
  TrmLocalToolBar = class(TrmCustomFrame)
    MainMenu: TMainMenu;
    ControlBar: TControlBar;
    ActionList: TActionList;
    ImageList: TImageList;
    pmToolBars: TPopupMenu;
    procedure pmToolBarsPopup(Sender: TObject);
  private
    FUpdatingState: Boolean;
    procedure CreateToolBarMenu;
    procedure OnClickToolBarMenu(Sender: TObject);
  protected
    procedure DoUpdateState; virtual;
    function  UpdatingState: Boolean;
    function  FrameOwner: IrmDsgnApartment;
    procedure UpdateState;
    procedure StoreState; override;
    procedure RestoreState; override;
  public
    procedure Initialize; virtual;
    procedure Deinitialize; virtual;
    constructor Create(AOwner: TComponent); override;
  end;

  TrmLocalToolBarCalss = class of TrmLocalToolBar;

  TrmTBMenuItem = class(TMenuItem)
  private
    FToolBar: TToolBar;
  end;

implementation

{$R *.dfm}

{ TrmLocalToolBar }

procedure TrmLocalToolBar.Initialize;
begin
  UpdateState;
end;

procedure TrmLocalToolBar.Deinitialize;
begin
end;

procedure TrmLocalToolBar.UpdateState;
begin
  try
    FUpdatingState := True;
    DoUpdateState;
  finally
    FUpdatingState := False;
  end;
end;


procedure TrmLocalToolBar.DoUpdateState;
begin
end;

function TrmLocalToolBar.UpdatingState: Boolean;
begin
  Result := FUpdatingState;
end;

function TrmLocalToolBar.FrameOwner: IrmDsgnApartment;
var
  O: TComponent;
begin
  Result := nil;

  O := Owner;

  while Assigned(O) do
    if Supports(O, IrmDsgnApartment) then
    begin
      Result := O as IrmDsgnApartment;
      break;
    end
    else
      O := O.Owner;

//  Result := RMDesigner.GetActiveFrame;
end;

procedure TrmLocalToolBar.RestoreState;
var
  S: IEvDualStream;
  V: Variant;
  TBname: String;
  TBvisible, TBfloating: Boolean;
  TBleft, TBtop: Integer;
  TB: TToolBar;
begin
  inherited;
  V := VarArrayOf([]);
  V := Apartment.Settings.ReadData(ClassName + '\DockingInfo', V);

  if VarArrayHighBound(V, 1) <> -1 then
  begin
    S := TEvDualStreamHolder.CreateFromVariant(V);

    // hide or undock TB
    S.Position := 0;
    while S.Position < S.Size do
    begin
      TBname := S.ReadString;
      TBvisible := S.ReadBoolean;
      TBfloating := S.ReadBoolean;
      TBleft := S.ReadInteger;
      TBtop := S.ReadInteger;

      TB := FindComponent(TBname) as TToolBar;
      if Assigned(TB) then
      begin
        TB.Visible := TBvisible;

        if TBfloating and TBvisible then
          TB.ManualFloat(Rect(TBleft, TBtop, TBleft + TB.Width, TBleft + TB.Height))
        else
          TB.Parent := ControlBar;
      end;
    end;


    // arrange TB
    S.Position := 0;
    while S.Position < S.Size do
    begin
      TBname := S.ReadString;
      TBvisible := S.ReadBoolean;
      TBfloating := S.ReadBoolean;
      TBleft := S.ReadInteger;
      TBtop := S.ReadInteger;

      TB := FindComponent(TBname) as TToolBar;
      if Assigned(TB) and TBvisible and not TBfloating then
      begin
        TB.SetBounds(TBleft, TBtop, TB.Width, TB.Height);
        TB.Visible := TBvisible;
      end;
    end;
  end;
end;

procedure TrmLocalToolBar.StoreState;
var
  i: Integer;
  C: TToolBar;
  S: IEvDualStream;
  V: Variant;
begin
  inherited;

  S := TEvDualStreamHolder.CreateInMemory;

  for i := 0 to ComponentCount - 1 do
    if Components[i] is TToolBar then
    begin
      C := TToolBar(Components[i]);
      S.WriteString(C.Name);
      S.WriteBoolean(C.Visible);
      S.WriteBoolean(C.Floating);

      if C.Floating then
      begin
        S.WriteInteger(C.Parent.Left);
        S.WriteInteger(C.Parent.Top);
      end
      else
      begin
        S.WriteInteger(C.Left);
        S.WriteInteger(C.Top);
      end;
    end;

  V := S.AsVarArrayOfBytes;
  Apartment.Settings.SaveData(ClassName + '\DockingInfo', V);
end;


constructor TrmLocalToolBar.Create(AOwner: TComponent);
begin
  inherited;
  CreateToolBarMenu;
end;

procedure TrmLocalToolBar.CreateToolBarMenu;
var
  i: Integer;
  MI: TrmTBMenuItem;
begin
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TToolBar then
    begin
      MI := TrmTBMenuItem.Create(Self);
      MI.Caption := TToolBar(Components[i]).Caption;
      MI.FToolBar := TToolBar(Components[i]);
      MI.OnClick := OnClickToolBarMenu;
      pmToolBars.Items.Add(MI);
    end;
end;

procedure TrmLocalToolBar.OnClickToolBarMenu(Sender: TObject);
begin
  TMenuItem(Sender).Checked := not TMenuItem(Sender).Checked;
  if TMenuItem(Sender).Checked then
  begin
    TrmTBMenuItem(Sender).FToolBar.Left := 0;
    TrmTBMenuItem(Sender).FToolBar.Top := ControlBar.Height;
    TrmTBMenuItem(Sender).FToolBar.Parent := ControlBar;
    TrmTBMenuItem(Sender).FToolBar.Visible := True;
  end
  else
    TrmTBMenuItem(Sender).FToolBar.Visible := False;
end;

procedure TrmLocalToolBar.pmToolBarsPopup(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to pmToolBars.Items.Count - 1 do
    pmToolBars.Items[i].Checked := TrmTBMenuItem(pmToolBars.Items[i]).FToolBar.Visible;
end;

end.
