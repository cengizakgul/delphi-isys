unit rwQBQueryPropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwPropertyEditorFrm, StdCtrls, rwQBInfo, rwQueryBuilderFrm, rwDB;

type

  TrwQBQueryPropertyEditor = class(TrwPropertyEditor)
    procedure FormCreate(Sender: TObject);
  public
    function ShowModal: Integer; override;
  end;

implementation

{$R *.DFM}

function TrwQBQueryPropertyEditor.ShowModal: Integer;
begin
  if ShowQueryBuilder(TrwQBQuery(FValue), not CheckInheritProp) then
    Result := mrOK
  else
    Result := mrCancel;
end;

procedure TrwQBQueryPropertyEditor.FormCreate(Sender: TObject);
begin
  inherited;

  FValue := TrwQBQuery.Create(nil);
end;


initialization
  RegisterClass(TrwQBQueryPropertyEditor);

end.
