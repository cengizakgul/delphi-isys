inherited rmTrmlQueryDBComboBoxProp: TrmTrmlQueryDBComboBoxProp
  Height = 252
  inherited pcCategories: TPageControl
    Height = 252
    inherited tsBasic: TTabSheet
      inherited GroupBox2: TGroupBox
        Top = 78
      end
    end
    object tsData: TTabSheet [1]
      Caption = 'Data'
      ImageIndex = 4
      object gbDisplayFields: TGroupBox
        Left = 8
        Top = 41
        Width = 291
        Height = 153
        Caption = 'Visible Fields'
        TabOrder = 0
        inline frmDisplayFields: TrmOPEDisplayFields
          Left = 10
          Top = 16
          Width = 270
          Height = 125
          AutoScroll = False
          TabOrder = 0
          inherited grFields: TisRWDBGrid
            Width = 270
            Height = 125
            Selected.Strings = (
              'Selected'#9'4'#9'Show'#9#9
              'FieldName'#9'34'#9'Field Name'#9'F'
              'Size'#9'3'#9'Size'#9#9)
          end
          inherited dsFields: TisRWClientDataSet
            inherited dsFieldsTitle: TStringField
              Visible = False
            end
          end
        end
      end
      inline frmQuery: TrmOPEQuery
        Left = 8
        Top = 8
        Width = 75
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited btnQuery: TButton
          OnClick = frmQuerybtnQueryClick
        end
      end
    end
    inherited tcAppearance: TTabSheet
      inherited frmColor: TrmOPEColor
        Top = 10
        TabOrder = 1
      end
      inherited frmCaptionFont: TrmOPEFont
        TabOrder = 0
      end
    end
    inherited tsParam: TTabSheet
      inherited frmParam1: TrmOPEReportParam
        Top = 41
      end
      inline frmKeyField: TrmOPEEnum
        Left = 8
        Top = 10
        Width = 291
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited lPropName: TLabel
          Width = 43
          Caption = 'Key Field'
        end
        inherited cbList: TComboBox
          Left = 90
          Width = 201
          OnClick = frmKeyFieldcbListClick
        end
      end
    end
  end
end
