unit rmTBDsgnLibraryFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTBDsgnComponentFrm, Menus, ImgList, ActnList,
  rmFMLExprPanelFrm, StdCtrls, Mask, wwdbedit, Wwdbspin, ISBasicClasses,
  rwDesignClasses, ExtCtrls, ColorPicker, ComCtrls, ToolWin,
  rmCustomFrameFrm, rmTBMDIChildFrameFrm, rmTBComponentsFrm, rmDsgnTypes,
  rwDsgnUtils, rmTypes;

type
  TrmTBDsgnLibrary = class(TrmTBMDIChildFrame)
    File1: TMenuItem;
    tbFile: TToolBar;
    tbtOpen: TToolButton;
    tbtSave: TToolButton;
    tbtNewReport: TToolButton;
    tlbCompile: TToolBar;
    tbtCompile: TToolButton;
    actOpen: TAction;
    actSave: TAction;
    actNewReport: TAction;
    actCompile: TAction;
    ToolButton1: TToolButton;
    tbtSaveChanges: TToolButton;
    tbtReload: TToolButton;
    actSaveChanges: TAction;
    acrReload: TAction;
    ToolButton2: TToolButton;
    LoadLibrary1: TMenuItem;
    SaveLibrary1: TMenuItem;
    NewReport1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    SaveChanges1: TMenuItem;
    ReloadLibrary1: TMenuItem;
    N3: TMenuItem;
    procedure actCompileExecute(Sender: TObject);
    procedure actOpenExecute(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actNewReportExecute(Sender: TObject);
    procedure actSaveChangesExecute(Sender: TObject);
    procedure acrReloadExecute(Sender: TObject);

  private
    procedure DMFunctionsChanged(var Message: TrmMessageRec); message mFunctionsChanged;
    procedure DMVariablesChanged(var Message: TrmMessageRec); message mVariablesChanged;
    procedure DMComponentsChanged(var Message: TrmMessageRec); message mComponentsChanged;

  protected
    procedure DoUpdateState; override;
    procedure GetListeningMessages(const AMessages: TList); override;
  end;

implementation

{$R *.dfm}

{ TrmTBDsgnLibrary }

procedure TrmTBDsgnLibrary.actCompileExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbCompile,  0, 0);
  UpdateState;
end;

procedure TrmTBDsgnLibrary.actOpenExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbOpen,  0, 0);
end;

procedure TrmTBDsgnLibrary.actSaveExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbSave,  0, 0);
end;

procedure TrmTBDsgnLibrary.actNewReportExecute(Sender: TObject);
begin
  SendDesignerMessage(Self, mtbNew, 0, 0);
end;

procedure TrmTBDsgnLibrary.actSaveChangesExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbSaveChanges, 0, 0);
  UpdateState;
end;

procedure TrmTBDsgnLibrary.acrReloadExecute(Sender: TObject);
begin
  SendApartmentMessage(Self, mtbReload, 0, 0);
  UpdateState;
end;

procedure TrmTBDsgnLibrary.DoUpdateState;
begin
  inherited;
  actSaveChanges.Enabled := (FrameOwner as IrmDsgnLibrary).IsLibraryModified;
end;

procedure TrmTBDsgnLibrary.DMComponentsChanged(var Message: TrmMessageRec);
begin
  inherited;
  UpdateState;
end;

procedure TrmTBDsgnLibrary.DMFunctionsChanged(var Message: TrmMessageRec);
begin
  inherited;
  UpdateState;
end;

procedure TrmTBDsgnLibrary.DMVariablesChanged(var Message: TrmMessageRec);
begin
  inherited;
  UpdateState;
end;

procedure TrmTBDsgnLibrary.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mFunctionsChanged));
  AMessages.Add(Pointer(mVariablesChanged));
  AMessages.Add(Pointer(mComponentsChanged));
end;

end.
