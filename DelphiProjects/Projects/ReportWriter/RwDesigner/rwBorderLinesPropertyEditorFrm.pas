unit rwBorderLinesPropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rwPropertyEditorFrm, StdCtrls, ExtCtrls, rwTypes, Buttons;

type
  TrwBorderLinesPropertyEditor = class(TrwPropertyEditor)
    cbColor: TColorBox;
    cbStyle: TComboBox;
    cbWidth: TComboBox;
    pnCellExample: TPanel;
    pbCellExample: TPaintBox;
    sbTopLine: TSpeedButton;
    sbBottomLine: TSpeedButton;
    sbLeftLine: TSpeedButton;
    sbRightLine: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Bevel1: TBevel;
    procedure pbCellExamplePaint(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cbStyleDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure cbWidthDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure sbTopLineClick(Sender: TObject);
    procedure sbBottomLineClick(Sender: TObject);
    procedure sbLeftLineClick(Sender: TObject);
    procedure sbRightLineClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure AssignLineProps(ALine: TrwBorderLine);
    procedure SetLine(ALine: TrwBorderLine; Visible: Boolean);
  public
  end;

implementation

uses Types;

{$R *.dfm}

procedure TrwBorderLinesPropertyEditor.pbCellExamplePaint(Sender: TObject);
const
  cCornerWidth = 10;
  cTextHeight = 3;
var
  R, R1: TRect;
  i, d: Integer;

  procedure DrawCorner(X1, Y1, X2, Y2, X3, Y3: Integer);
  begin
    with pbCellExample.Canvas do
    begin
      MoveTo(X1, Y1);
      LineTo(X2, Y2);
      LineTo(X3, Y3);
    end;
  end;

  procedure DrawLine(BL: TrwBorderLine; X1, Y1, X2, Y2: Integer);
  begin
    if not BL.Visible then
      Exit;
    with pbCellExample.Canvas do
    begin
      Pen.Mode := pmCopy;
      Pen.Style := BL.Style;
      Pen.Width := BL.Width;
      Pen.Color := BL.Color;

      MoveTo(X1, Y1);
      LineTo(X2, Y2);
    end;  
  end;

begin
  with pbCellExample, pbCellExample.Canvas do
  begin
    Brush.Color := clWhite;
    Brush.Style := bsSolid;
    FillRect(pbCellExample.ClientRect);

    // Corners
    Pen.Mode := pmCopy;
    Pen.Style := psSolid;
    Pen.Width := 1;
    Pen.Color := clGray;

    DrawCorner(cCornerWidth, cCornerWidth * 2,
               cCornerWidth * 2, cCornerWidth * 2,
               cCornerWidth * 2, cCornerWidth);

    DrawCorner(Width - cCornerWidth * 2, cCornerWidth,
               Width - cCornerWidth * 2, cCornerWidth * 2,
               Width - cCornerWidth, cCornerWidth * 2);

    DrawCorner(Width - cCornerWidth * 2, Height - cCornerWidth,
               Width - cCornerWidth * 2, Height - cCornerWidth * 2,
               Width - cCornerWidth, Height - cCornerWidth * 2);

    DrawCorner(cCornerWidth, Height - cCornerWidth * 2,
               cCornerWidth * 2, Height - cCornerWidth * 2,
               cCornerWidth * 2, Height - cCornerWidth);

    // Lines
    DrawLine(TrwBorderLines(FValue).LeftLine, cCornerWidth * 2, cCornerWidth * 2, cCornerWidth * 2, Height - cCornerWidth * 2);
    DrawLine(TrwBorderLines(FValue).TopLine, cCornerWidth * 2, cCornerWidth * 2, Width - cCornerWidth * 2, cCornerWidth * 2);
    DrawLine(TrwBorderLines(FValue).RightLine, Width - cCornerWidth * 2, cCornerWidth * 2, Width - cCornerWidth * 2, Height - cCornerWidth * 2);
    DrawLine(TrwBorderLines(FValue).BottomLine, cCornerWidth * 2, Height - cCornerWidth * 2, Width - cCornerWidth * 2, Height - cCornerWidth * 2);

    // "Text"
    R := Rect(cCornerWidth * 2 + 3, cCornerWidth * 2, Width - cCornerWidth * 2 - 3, Height - cCornerWidth * 2 - 3);

    if TrwBorderLines(FValue).LeftLine.Visible then
      R.Left := R.Left + TrwBorderLines(FValue).LeftLine.Width;

    if TrwBorderLines(FValue).TopLine.Visible then
      R.Top := R.Top + TrwBorderLines(FValue).TopLine.Width;

    if TrwBorderLines(FValue).RightLine.Visible then
      R.Right := R.Right - TrwBorderLines(FValue).RightLine.Width;

    if TrwBorderLines(FValue).BottomLine.Visible then
      R.Bottom := R.Bottom - TrwBorderLines(FValue).BottomLine.Width;

    d := (R.Bottom - R.Top) div (cTextHeight * 2);
    Brush.Color := clSilver;
    Brush.Style := bsSolid;
    for i := 1 to d do
    begin
      R1 := Rect(R.Left, R.Top + i * cTextHeight * 2 - cTextHeight, R.Right, R.Top + i * cTextHeight * 2);
      if i = 1 then
        R1.Left := R1.Left + (R.Right - R.Left) div 3
      else if i = d then
        R1.Right := R1.Right - (R.Right - R.Left) div 3;

      FillRect(R1);
    end;

  end;
end;


procedure TrwBorderLinesPropertyEditor.FormCreate(Sender: TObject);
begin
  inherited;
  FValue := TrwBorderLines.Create(nil);
end;

procedure TrwBorderLinesPropertyEditor.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FValue);
  inherited;
end;

procedure TrwBorderLinesPropertyEditor.cbStyleDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  d: Integer;
begin
  with cbStyle.Canvas do
  begin
    Brush.Style := bsSolid;
    Brush.Color := clWhite;
    FillRect(Rect);

    Pen.Style := TPenStyle(Index);
    Pen.Mode := pmCopy;
    Pen.Width := 1;
    Pen.Color := clBlack;
    d := (Rect.Bottom - Rect.Top) div 2;
    MoveTo(10, Rect.Top + d);
    LineTo(Rect.Right - Rect.Left - 10, Rect.Top + d);
  end;
end;

procedure TrwBorderLinesPropertyEditor.cbWidthDrawItem(Control: TWinControl; Index: Integer; Rect: TRect;
  State: TOwnerDrawState);
var
  d: Integer;
begin
  with cbWidth.Canvas do
  begin
    Brush.Style := bsSolid;
    Brush.Color := clWhite;
    FillRect(Rect);

    Pen.Style := psSolid;
    Pen.Mode := pmCopy;
    Pen.Width := StrToInt(cbWidth.Items[Index]);
    Pen.Color := clBlack;
    d := (Rect.Bottom - Rect.Top) div 2;
    MoveTo(10, Rect.Top + d);
    LineTo(Rect.Right - Rect.Left - 10, Rect.Top + d);
  end;
end;

procedure TrwBorderLinesPropertyEditor.sbTopLineClick(Sender: TObject);
begin
  SetLine(TrwBorderLines(FValue).TopLine, TSpeedButton(Sender).Down);
end;

procedure TrwBorderLinesPropertyEditor.AssignLineProps(ALine: TrwBorderLine);
begin
  ALine.Style := TPenStyle(cbStyle.ItemIndex);
  ALine.Width := cbWidth.ItemIndex + 1;
  ALine.Color := cbColor.Selected;
end;

procedure TrwBorderLinesPropertyEditor.SetLine(ALine: TrwBorderLine; Visible: Boolean);
begin
  ALine.Visible := Visible;
  if Visible then
    AssignLineProps(ALine);
  pbCellExample.Invalidate;
end;

procedure TrwBorderLinesPropertyEditor.sbBottomLineClick(Sender: TObject);
begin
  SetLine(TrwBorderLines(FValue).BottomLine, TSpeedButton(Sender).Down);
end;

procedure TrwBorderLinesPropertyEditor.sbLeftLineClick(Sender: TObject);
begin
  SetLine(TrwBorderLines(FValue).LeftLine, TSpeedButton(Sender).Down);
end;

procedure TrwBorderLinesPropertyEditor.sbRightLineClick(Sender: TObject);
begin
  SetLine(TrwBorderLines(FValue).RightLine, TSpeedButton(Sender).Down);
end;

procedure TrwBorderLinesPropertyEditor.FormShow(Sender: TObject);
begin
  inherited;

  with TrwBorderLines(FValue) do
  begin
    sbTopLine.Down := TopLine.Visible;
    sbBottomLine.Down := BottomLine.Visible;
    sbLeftLine.Down := LeftLine.Visible;
    sbRightLine.Down := RightLine.Visible;

    if TopLine.Visible and BottomLine.Visible and LeftLine.Visible and RightLine.Visible then
    begin
      if (TopLine.Color = BottomLine.Color) and (TopLine.Color = LeftLine.Color) and
         (TopLine.Color = RightLine.Color) then
        cbColor.Selected := TopLine.Color;

      if (TopLine.Style = BottomLine.Style) and (TopLine.Style = LeftLine.Style) and
         (TopLine.Style = RightLine.Style) then
        cbStyle.ItemIndex := Ord(TopLine.Style);

      if (TopLine.Width = BottomLine.Width) and (TopLine.Width = LeftLine.Width) and
         (TopLine.Width = RightLine.Width) then
        cbWidth.ItemIndex := TopLine.Width - 1;
    end;
  end;
end;

initialization
  RegisterClass(TrwBorderLinesPropertyEditor);

end.

