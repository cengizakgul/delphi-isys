unit rmFunctionTextFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmCustomFrameFrm, ComCtrls, rmTypes, rwDsgnRes, rwDsgnUtils, rmDsgnTypes, rwTypes,
  rwBasicClasses;

type
  TrmFunctionText = class(TrmCustomFrame)
    pcFunctions: TPageControl;
    procedure pcFunctionsChange(Sender: TObject);
  private
    FFunction: IrmFunction;
    FFunctionText: TrwEventTextEdit;
    procedure FunctionTextExit(Sender: TObject);
    function  GetRWObject: IrmDesignObject;

    procedure DMBreakPointToggle(var Message: TrmMessageRec); message mtbBreakPointToggle;
    procedure DMDesignObjectChanged(var Message: TrmMessageRec); message mDesignObjectChanged;
    procedure DMDoApplyPendingChanges(var Message: TrmMessageRec); message mDoApplyPendingChanges;
    procedure DMApartmentDestroying(var Message: TrmMessageRec); message mApartmentDestroying;

  protected
    procedure GetListeningMessages(const AMessages: TList); override;

  public
    procedure ShowFunction(const AFunction: IrmFunction; const AClassName: String = ''; const ALineNbr: Integer = 0);
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    function  CurrentEditor: TrwEventTextEdit;
    function  AncestorClassName: String;
    procedure RefreshLineColors;
    function  GetDebugLocation: String;
    property  RMFunction: IrmFunction read FFunction;
  public
    destructor Destroy; override;
  end;

implementation

{$R *.dfm}


procedure TrmFunctionText.AfterConstruction;
begin
  inherited;

  FFunctionText := TrwEventTextEdit.Create(Self);
  DsgnRes.InitEditor(FFunctionText);
  FFunctionText.Align := alClient;
  FFunctionText.Parent := Self;
  FFunctionText.Enabled := False;
  FFunctionText.OnExit := FunctionTextExit;
//  FunctionText.OnSelectionChange := ShowCursorPos;
end;

function TrmFunctionText.AncestorClassName: String;
begin
 if pcFunctions.Visible and Assigned(pcFunctions.ActivePage) and (pcFunctions.ActivePage.TabIndex > 0) then
   Result := pcFunctions.ActivePage.Caption
 else
   Result := ''
end;

function TrmFunctionText.CurrentEditor: TrwEventTextEdit;
begin
 if pcFunctions.Visible and Assigned(pcFunctions.ActivePage) then
   Result := (pcFunctions.ActivePage.Controls[0] as TrwEventTextEdit)
 else
   Result := FFunctionText;
end;

procedure TrmFunctionText.RefreshLineColors;
var
  StopLine: TrmDebugStopLineInfo;
  ALineColorList: TrwLineColorList;

  procedure RefreshBreakPoints;
  var
    BP: IrmBreakPoints;
    i: Integer;
    DebLoc: String;
  begin
    BP := (Apartment as IrmDsgnReport).GetBreakPointList;
    DebLoc := GetDebugLocation;

    for i := 0 to BP.Count - 1 do
    begin
      with BP.BreakPointByIndex(i) do
        if Assigned(FFunction) and AnsiSameText(ObjectLocation, DebLoc) and AnsiSameText(FunctionName, FFunction.GetName) then
          ALineColorList.Add(SourceCodeLineNbr, clWhite, clRed);
    end;
  end;

begin
  ALineColorList := CurrentEditor.LineColorList;

  ALineColorList.Clear;

  if Supports(Apartment, IrmDsgnReport) then
  begin
    StopLine := (Apartment as IrmDsgnReport).ReportStopLineInfo;
    if Assigned(StopLine.SorceCodeLineInfo) and Assigned(FFunction) and
{       ObjectsAreTheSame(StopLine.rmObject, GetRWObject) and}
       AnsiSameText(StopLine.SorceCodeLineInfo.ObjectLocation, GetDebugLocation) and
       AnsiSameText(StopLine.SorceCodeLineInfo.FunctionName, FFunction.GetName) then
    begin
      ALineColorList.Add(StopLine.SorceCodeLineInfo.SourceCodeLineNbr, clWhite, clNavy);
    end;

    RefreshBreakPoints;
  end;

  CurrentEditor.Invalidate;
end;

procedure TrmFunctionText.FunctionTextExit(Sender: TObject);
var
  Aprt: IrmDsgnApartment;
begin
  Aprt := Apartment;
  if Assigned(FFunction) and Assigned(Aprt) and Aprt.isActive and (Aprt as IrmDsgnComponent).GetSecurityInfo.EditFunctions then
  begin
    FFunction.SetFunctionText(FFunctionText.Text);
    PostApartmentMessage(Self, mFunctionTextChanged, 0, 0);
  end;
end;

procedure TrmFunctionText.ShowFunction(const AFunction: IrmFunction; const AClassName: String = ''; const ALineNbr: Integer = 0);
var
  T: TTabSheet;
  i: Integer;
  FncText: TrwEventTextEdit;
  ArrInhInfo: TrmInheritedFunctInfoList;

  procedure ClearTabs;
  begin
    while pcFunctions.PageCount > 0 do
    begin
      while pcFunctions.Pages[0].ControlCount > 0 do
        pcFunctions.Pages[0].Controls[0].Free;
      pcFunctions.Pages[0].Free;
    end;
  end;


  procedure ShowOneEvent;
  begin
    FFunctionText.Parent := Self;
    pcFunctions.Visible := False;
    FFunctionText.Align := alClient;
    ClearTabs;
    RefreshLineColors;
  end;

  procedure CreateInheritedFuncts(const AFuncInfo: TrmInheritedFunctInfoRec);
  begin
    if AFuncInfo.ThroughInheritance then
      Exit;

    T := TTabSheet.Create(pcFunctions);
    T.Caption := AFuncInfo.ClassName;
    T.PageControl := pcFunctions;

    FncText := TrwEventTextEdit.Create(nil);
    FncText.TabWidth := 2;
    FncText.Align := alClient;
    FncText.Parent := T;
    FncText.HighLighter := GeneralSyn;
    FncText.Lines.Text := AFuncInfo.FunctText;
    FncText.Lines.Delete(0);
    FncText.Readonly := True;
//    FncText.OnSelectionChange := ShowCursorPos;
  end;

  procedure SetClassTabAndPosition;
  var
    i: Integer;
  begin
    if Assigned(FFunction) then
    begin
      if pcFunctions.Visible then
        if AClassName <> '' then
        begin
           for i := 0 to pcFunctions.PageCount - 1 do
             if AnsiSameText(pcFunctions.Pages[i].Caption, AClassName) then
             begin
               pcFunctions.Pages[i].Show;
               break;
             end;
        end
        else
          pcFunctions.Pages[0].Show;

      if ALineNbr > 0 then
        CurrentEditor.CaretY := ALineNbr;
    end;    

    RefreshLineColors;  
  end;

begin
  if ObjectsAreTheSame(FFunction, AFunction) then
  begin
    SetClassTabAndPosition;
    Exit;
  end;

  FFunctionText.OnExit(nil);

  FFunction := AFunction;
  SetLength(ArrInhInfo, 0);

  if Assigned(FFunction) then
  begin
    if GetRWObject <> nil then
      FFunctionText.EventOwner := GetRWObject.VCLObject as TrwComponent
    else
      FFunctionText.EventOwner := nil;

    FFunctionText.Text := FFunction.GetFunctionText;
    FFunctionText.Enabled := True;
    FFunctionText.ReadOnly := not (Apartment as IrmDsgnComponent).GetSecurityInfo.EditFunctions;

    ArrInhInfo := FFunction.GetInheritanceInfo;

    if Length(ArrInhInfo) > 0 then
    begin
      FFunctionText.Parent := nil;
      ClearTabs;
      T := TTabSheet.Create(pcFunctions);
      T.Caption := 'Overriding';
      T.PageControl := pcFunctions;
      FFunctionText.Parent := T;
      FFunctionText.Align := alClient;

      for i := Low(ArrInhInfo) to High(ArrInhInfo) do
        CreateInheritedFuncts(ArrInhInfo[i]);

      pcFunctions.Visible := True;
    end
    else
      ShowOneEvent;
  end

  else
  begin
    FFunctionText.Text := '';
    ShowOneEvent;
    FFunctionText.Enabled := False;
  end;

  SetClassTabAndPosition;
end;


procedure TrmFunctionText.pcFunctionsChange(Sender: TObject);
begin
  RefreshLineColors;
end;

function TrmFunctionText.GetDebugLocation: String;
var
  Own: IrmDesignObject;
begin
  Result := '';

  if Assigned(FFunction) then
    if AncestorClassName = '' then
    begin
      if GetRWObject <> nil then
        Result := GetRWObject.GetPathFromRootOwner
      else
        Result := cParserLibFunct;
    end

    else
    begin
      Own := GetRWObject;
      while not Own.ObjectInheritsFrom(AncestorClassName) do
      begin
        if Result <> '' then
          Result := '.' + Result;
        Result := Own.GetPropertyValue('Name') + Result;
        Own := Own.GetObjectOwner;
      end;

      if Result <> '' then
        Result := '.' + Result;

      Result := AncestorClassName + Result;
    end;
end;

function TrmFunctionText.GetRWObject: IrmDesignObject;
begin
  if Assigned(FFunction) then
    Result := FFunction.GetCollection.GetComponentOwner
  else
    Result := nil;
end;

procedure TrmFunctionText.DMBreakPointToggle(var Message: TrmMessageRec);
begin
  if Assigned(FFunction) and Supports(Apartment, IrmDsgnReport) and IsChild(Handle, Screen.ActiveControl.Handle) then
  begin
    FunctionTextExit(nil);
    (Apartment as IrmDsgnReport).GetBreakPointList.ToggleBreakPoint(GetDebugLocation, FFunction.GetName, CurrentEditor.CaretY);
    RefreshLineColors;
  end;
end;

procedure TrmFunctionText.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mtbBreakPointToggle));
  AMessages.Add(Pointer(mDesignObjectChanged));
  AMessages.Add(Pointer(mDoApplyPendingChanges));
  AMessages.Add(Pointer(mApartmentDestroying));
end;

procedure TrmFunctionText.BeforeDestruction;
begin
  FFunctionText.OnExit(nil);
  inherited;
end;

procedure TrmFunctionText.DMDesignObjectChanged(var Message: TrmMessageRec);
begin
  inherited;
  ShowFunction(nil);
end;

procedure TrmFunctionText.DMDoApplyPendingChanges(
  var Message: TrmMessageRec);
begin
  FunctionTextExit(Self);
end;

destructor TrmFunctionText.Destroy;
begin
  FFunction := nil;
  inherited;
end;

procedure TrmFunctionText.DMApartmentDestroying(var Message: TrmMessageRec);
begin
  ShowFunction(nil);
end;

end.
