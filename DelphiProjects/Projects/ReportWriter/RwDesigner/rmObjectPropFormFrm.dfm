inherited rmObjectPropForm: TrmObjectPropForm
  Left = 416
  Top = 147
  BorderStyle = bsDialog
  ClientHeight = 263
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pnlButtons: TPanel
    Left = 0
    Top = 221
    Width = 279
    Height = 42
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object btnOK: TButton
      Left = 5
      Top = 14
      Width = 75
      Height = 22
      Caption = 'Close'
      TabOrder = 0
      OnClick = btnOKClick
    end
    object Panel1: TPanel
      Left = 112
      Top = 0
      Width = 167
      Height = 42
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object btnCancel: TButton
        Left = 1
        Top = 14
        Width = 75
        Height = 22
        Caption = 'Cancel'
        TabOrder = 0
        OnClick = btnCancelClick
      end
      object btnApply: TButton
        Left = 88
        Top = 14
        Width = 75
        Height = 22
        Caption = 'Apply'
        TabOrder = 1
        OnClick = btnApplyClick
      end
    end
  end
end
