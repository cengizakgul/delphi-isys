object rwParamFile: TrwParamFile
  Left = 459
  Top = 243
  ActiveControl = edParams
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Run Parameters File'
  ClientHeight = 99
  ClientWidth = 382
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 5
    Top = 6
    Width = 372
    Height = 55
    Caption = 'File Name'
    TabOrder = 0
    object edParams: TEdit
      Left = 8
      Top = 20
      Width = 273
      Height = 21
      TabOrder = 0
    end
    object btnBrowse: TButton
      Left = 288
      Top = 18
      Width = 75
      Height = 25
      Caption = 'Browse'
      TabOrder = 1
      OnClick = btnBrowseClick
    end
  end
  object btnOK: TButton
    Left = 217
    Top = 70
    Width = 74
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object btnCancel: TButton
    Left = 303
    Top = 70
    Width = 74
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object opdParams: TOpenDialog
    DefaultExt = 'rwp'
    Filter = 'RW report parameters (*.rwp)|*.rwp|All Files (*.*)|*.*'
    Title = 'External report parameters file'
    Left = 124
    Top = 64
  end
end
