inherited rmTrmXMLchoiceProp: TrmTrmXMLchoiceProp
  Height = 308
  inherited pcCategories: TPageControl
    Height = 308
    inherited tsBasic: TTabSheet
      object Panel1: TPanel
        Left = 8
        Top = 77
        Width = 325
        Height = 197
        BevelOuter = bvNone
        TabOrder = 2
        object Label3: TLabel
          Left = 0
          Top = 0
          Width = 325
          Height = 13
          Align = alTop
          Caption = 'Choice Logic'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        inline frmChoiceControl: TrmOPEChoiceControl
          Left = 0
          Top = 13
          Width = 325
          Height = 180
          Align = alTop
          TabOrder = 0
        end
      end
    end
    inherited tcAppearance: TTabSheet
      inherited frmInactive: TrmOPEBoolean
        TabOrder = 2
      end
      inline frmProduceBranchIf: TrmOPEBlockingControl
        Left = 8
        Top = 35
        Width = 325
        Height = 71
        AutoScroll = False
        TabOrder = 1
        inherited lCaption: TLabel
          Width = 325
          Caption = 'Produce Whole Branch If'
        end
        inherited frmExpression: TrmFMLExprPanel
          Width = 325
          Height = 55
          inherited pnlCanvas: TPanel
            Width = 325
            Height = 55
          end
        end
      end
    end
  end
end
