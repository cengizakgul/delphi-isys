object rwPageDesigner: TrwPageDesigner
  Left = 0
  Top = 0
  Width = 709
  Height = 479
  AutoScroll = False
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 0
    Top = 190
    Width = 709
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    AutoSnap = False
    Beveled = True
    MinSize = 70
  end
  object pnlVertRuler: TPanel
    Left = 0
    Top = 23
    Width = 23
    Height = 167
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
  end
  object pnlHorRuler: TPanel
    Left = 0
    Top = 0
    Width = 709
    Height = 23
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object pnlUnitName: TPanel
      Left = 0
      Top = 0
      Width = 23
      Height = 23
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'Inch'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Small Fonts'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 193
    Width = 709
    Height = 286
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Splitter2: TSplitter
      Left = 266
      Top = 0
      Height = 286
      AutoSnap = False
      Beveled = True
      MinSize = 100
    end
    inline rwPropertiesEditor: TrwPropertiesEditor
      Left = 0
      Top = 0
      Width = 266
      Height = 286
      Align = alLeft
      AutoScroll = False
      Color = clBtnFace
      ParentColor = False
      TabOrder = 0
      inherited pgcInspector: TPageControl
        Width = 266
        Height = 268
      end
      inherited pnlCompName: TPanel
        Top = 268
        Width = 266
      end
    end
    object Panel1: TPanel
      Left = 269
      Top = 0
      Width = 440
      Height = 286
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object pcEvents: TPageControl
        Left = 0
        Top = 0
        Width = 440
        Height = 286
        Align = alClient
        TabOrder = 0
        Visible = False
        OnChange = pcEventsChange
      end
    end
  end
end
