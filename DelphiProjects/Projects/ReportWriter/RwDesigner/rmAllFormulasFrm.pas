unit rmAllFormulasFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwReport, mwCustomEdit, rwBasicClasses, rwTypes, rwDsgnUtils, rwUtils, rmTypes,
  EvStreamUtils, rmReport, ComCtrls, rmFormula, Variants;

type
  TrmAllFormulas = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    FormulasText: TmwCustomEdit;
    FStream: TStream;
    procedure FillCompFormulas(AComponent: TrwComponent; AOwnerName: String);
    procedure FillCompChildren(AIrmDesignObject: IrmDesignObject; AOwnerName: String);
  public
  end;

  procedure ShowRMAllFormulasOfReport(AIrmDesignObject : IrmDesignObject);

implementation

var Frm: TrmAllFormulas;

{$R *.DFM}

function  MakeFormulaHandlerHeader(AFormula: TrmFMLFormula; AObjectName: String = ''): string;
begin
  Result := '// Name of object : '+ AObjectName + #13#10 + '// ActionInfo : ' + AFormula.ActionInfo + #13#10 +'// ActionType : ';
  case AFormula.ActionType of
    fatNone : result := result + 'None';
    fatSetProperty : result := result + 'SetProperty';
    fatDataFilter : result := result + 'DataFilter';
    fatCalcControl : result := result + 'CalcControl';
  end;
end;

procedure ShowRMAllFormulasOfReport(AIrmDesignObject: IrmDesignObject);
var
  h: String;
begin
  if Assigned(Frm) or (not Assigned(AIrmDesignObject)) then
    Exit;

  Frm := TrmAllFormulas.Create(nil);
  with Frm do
    try

      h := GetThreadTmpDir + '~tmpEvents.txt';
      FStream := TEvFileStream.Create(h, fmCreate);
      FormulasText.BeginUpdate;
      try
        FillCompChildren(AIrmDesignObject, AIrmDesignObject.RealObject.Name);
        FStream.Position := 0;
        FormulasText.Lines.LoadFromStream(FStream);
      finally
        FormulasText.EndUpdate;
        FStream.Free;
        DeleteFile(h);
      end;

      ShowModal;
    finally
      Free;
      Frm := nil;
    end;
end;

procedure TrmAllFormulas.FormCreate(Sender: TObject);
begin
  FormulasText := TmwCustomEdit.Create(Self);
  FormulasText.Align := alClient;
  FormulasText.Parent := Self;
  FormulasText.ReadOnly := True;
  FormulasText.HighLighter := GeneralSyn;
end;


procedure TrmAllFormulas.FillCompFormulas(AComponent: TrwComponent; AOwnerName: String);
var
  i, j, k:  Integer;
  Formula: TrmFMLFormula;
  h, h1:  string;
  A:  PChar;
  AggregateFlag : boolean;
begin
  for i := 0 to AComponent.Formulas.Count-1 do
  begin
    Formula := AComponent.Formulas.Items[i];
    if Formula.RWExpression <> '' then
    begin
      if FStream.Size > 0 then
        FStream.Write(#13#10#13#10+'//*******************************************'+#13#10, 51);
      if VAR_FUNCT_COMP_NAME <> AComponent.Name then
      begin
        h := AComponent.Name;
        if (AOwnerName <> '') then
          h := AOwnerName+'.'+h;
      end
      else
        h := '';

      // formula's header
      h := MakeFormulaHandlerHeader(Formula, h) + #13#10;

      // formula's aggregate subfunctions
      AggregateFlag := False;
      for j := 0 to Formula.Content.Count - 1 do
        if Formula.Content[j].IsAggregateFunction then
        begin
          AggregateFlag := True;
          if j = 0 then
            h := h + '//-------------------------------------------' + #13#10;
          k := TrmFMLFormulaContent(Formula.Content[j].Collection).AggrFunctSkipThrough(Formula.Content[j]);
          h1 := TrmFMLFormulaContent(Formula.Content[j].Collection).RWExpression(Formula.Content[j].Index + 1, k - 2);
          if h1 <> '' then
          begin
            if VarIsStr(Formula.Content[j].Value) then
              h := h + '// Aggregate function: ' + Formula.Content[j].Value + #13#10;
            h := h + '// FunctionID: ' + Formula.Content[j].AggrFunctHolder.FunctionID + #13#10;
            h := h + 'function CalcAggrFunctParams(): Array' + #13#10 +
                  '  Result := [' + h1 + '];' + #13#10 +
                  'end;' + #13#10;
          end;
        end;

      // formula's body
      h1 := '';
      if Formula.ActionType = fatDataFilter then
      begin
        if AnsiSameText(Formula.ActionInfo, 'Text') then
          h1 := 'AData: String';
      end
      else
        h1 := '';
      if AggregateFlag then
        h := h + '// Calculation of result' + #13#10;
      h := h + 'function CalcFormula(' + h1 + '): Variant' + #13#10;
      h := h + '  ' + Formula.RWExpression + #13 + #10 + 'end;';

      A := AllocMem(Length(h)+1);
      try
        StrPCopy(A, h);
        FStream.Write(A^, Length(h));
      finally
        FreeMem(A);
      end;
    end;
  end;
end;


procedure TrmAllFormulas.FillCompChildren(AIrmDesignObject: IrmDesignObject; AOwnerName: String);
var
  i: Integer;
  C: IrmDesignObject;
begin
  for i := 0 to AIrmDesignObject.GetChildrenCount - 1 do
  begin
    C := AIrmDesignObject.GetChildByIndex(i);
    if (C.RealObject is TrwComponent) then
    begin
      FillCompFormulas((C.RealObject as TrwComponent), AOwnerName);
//        if not (TrwComponent(C.RealObject).InheritedComponent and TrwComponent(AIrmDesignObject.RealObject).IsLibComponent) then
      FillCompChildren(TrwComponent(C.RealObject), AOwnerName + '.' + C.RealObject.Name)
    end;
  end;
end;


procedure TrmAllFormulas.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_ESCAPE then
    Close;
end;

initialization
  Frm := nil;

end.
