object rwBasicToolsBar: TrwBasicToolsBar
  Left = 0
  Top = 0
  Width = 888
  Height = 52
  AutoSize = True
  TabOrder = 0
  OnResize = FrameResize
  object ctbBar: TControlBar
    Left = 0
    Top = 0
    Width = 888
    Height = 52
    Align = alTop
    AutoSize = True
    BevelEdges = []
    DragKind = dkDock
    PopupMenu = pmToolBars
    TabOrder = 0
    object tlbComponents: TToolBar
      Left = 11
      Top = 2
      Width = 23
      Height = 22
      Align = alNone
      AutoSize = True
      Caption = 'Components'
      DragKind = dkDock
      EdgeBorders = []
      Flat = True
      Images = DsgnRes.ilButtons
      TabOrder = 0
      Wrapable = False
      object tbtSelect: TToolButton
        Left = 0
        Top = 0
        Hint = 'Select Mode'
        Down = True
        Grouped = True
        ImageIndex = 12
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtSelectMouseDown
      end
    end
    object tlbAlignment: TToolBar
      Left = 124
      Top = 28
      Width = 260
      Height = 22
      Align = alNone
      AutoSize = True
      ButtonWidth = 26
      Caption = 'Alignment'
      DragKind = dkDock
      EdgeBorders = []
      Flat = True
      Images = DsgnRes.ilButtons
      TabOrder = 1
      Wrapable = False
      object tbtAlignmentLeft: TToolButton
        Left = 0
        Top = 0
        Hint = 'Align left'
        Caption = 'tbtAlignmentLeft'
        ImageIndex = 16
        OnClick = ToolButton2Click
      end
      object tbtAlignmentTop: TToolButton
        Tag = 1
        Left = 26
        Top = 0
        Hint = 'Align Top'
        Caption = 'tbtAlignmentTop'
        ImageIndex = 18
        OnClick = ToolButton2Click
      end
      object tbtAlignmentBottom: TToolButton
        Tag = 2
        Left = 52
        Top = 0
        Hint = 'Align Bottom'
        Caption = 'tbtAlignmentBottom'
        ImageIndex = 17
        OnClick = ToolButton2Click
      end
      object tbtAlignmentRight: TToolButton
        Tag = 3
        Left = 78
        Top = 0
        Hint = 'Align Right'
        Caption = 'tbtAlignmentRight'
        ImageIndex = 19
        OnClick = ToolButton2Click
      end
      object ToolButton7: TToolButton
        Tag = 4
        Left = 104
        Top = 0
        Hint = 'Align Center'
        Caption = 'ToolButton7'
        ImageIndex = 24
        OnClick = ToolButton2Click
      end
      object ToolButton14: TToolButton
        Tag = 5
        Left = 130
        Top = 0
        Hint = 'Align Middle'
        Caption = 'ToolButton14'
        ImageIndex = 25
        OnClick = ToolButton2Click
      end
      object ToolButton3: TToolButton
        Tag = 6
        Left = 156
        Top = 0
        Hint = 'Align by Container Center'
        Caption = 'ToolButton3'
        ImageIndex = 23
        OnClick = ToolButton2Click
      end
      object ToolButton5: TToolButton
        Tag = 7
        Left = 182
        Top = 0
        Hint = 'Align by Container Middle'
        Caption = 'ToolButton5'
        ImageIndex = 22
        OnClick = ToolButton2Click
      end
      object ToolButton9: TToolButton
        Tag = 8
        Left = 208
        Top = 0
        Hint = 'Vertical Spaces Equally'
        Caption = 'ToolButton9'
        ImageIndex = 20
        OnClick = ToolButton2Click
      end
      object ToolButton8: TToolButton
        Tag = 9
        Left = 234
        Top = 0
        Hint = 'Horisontal Spaces Equally'
        Caption = 'ToolButton8'
        ImageIndex = 21
        OnClick = ToolButton2Click
      end
    end
    object tlbSize: TToolBar
      Left = 11
      Top = 28
      Width = 92
      Height = 22
      Align = alNone
      AutoSize = True
      Caption = 'Size'
      DragKind = dkDock
      EdgeBorders = []
      Flat = True
      Images = DsgnRes.ilButtons
      TabOrder = 2
      Wrapable = False
      object ToolButton2: TToolButton
        Tag = 10
        Left = 0
        Top = 0
        Hint = 'Grow Height to Largest'
        Caption = 'ToolButton2'
        ImageIndex = 26
        OnClick = ToolButton2Click
      end
      object ToolButton6: TToolButton
        Tag = 11
        Left = 23
        Top = 0
        Hint = 'Shrink Height to Smallest'
        Caption = '`'
        ImageIndex = 27
        OnClick = ToolButton2Click
      end
      object ToolButton12: TToolButton
        Tag = 12
        Left = 46
        Top = 0
        Hint = 'Grow Width to Largest'
        Caption = 'ToolButton12'
        ImageIndex = 28
        OnClick = ToolButton2Click
      end
      object ToolButton13: TToolButton
        Tag = 13
        Left = 69
        Top = 0
        Hint = 'Shrink Width to Smallest'
        Caption = 'ToolButton13'
        ImageIndex = 29
        OnClick = ToolButton2Click
      end
    end
    object tlbFont: TToolBar
      Left = 56
      Top = 2
      Width = 497
      Height = 22
      Align = alNone
      AutoSize = True
      Caption = 'Text style'
      DragKind = dkDock
      EdgeBorders = []
      Flat = True
      Images = DsgnRes.ilButtons
      TabOrder = 3
      Wrapable = False
      object cbFontName: TComboBox
        Left = 0
        Top = 0
        Width = 155
        Height = 21
        Hint = 'Font Name'
        Style = csDropDownList
        DropDownCount = 16
        ItemHeight = 13
        Sorted = True
        TabOrder = 0
        OnChange = cbFontNameChange
      end
      object cbFontSize: TComboBox
        Left = 155
        Top = 0
        Width = 54
        Height = 21
        Hint = 'Font Size'
        DropDownCount = 16
        ItemHeight = 13
        TabOrder = 1
        OnChange = cbFontSizeChange
        Items.Strings = (
          '8'
          '9'
          '10'
          '11'
          '12'
          '14'
          '16'
          '18'
          '20'
          '22'
          '24'
          '26'
          '28'
          '36'
          '48'
          '72')
      end
      object ToolButton4: TToolButton
        Left = 209
        Top = 0
        Width = 8
        Caption = 'ToolButton4'
        ImageIndex = 3
        Style = tbsSeparator
      end
      object tbtFontBold: TToolButton
        Left = 217
        Top = 0
        Hint = 'Bold'
        AllowAllUp = True
        Caption = 'tbtFontBold'
        ImageIndex = 0
        Style = tbsCheck
        OnClick = tbtFontBoldClick
      end
      object tbtFontItalic: TToolButton
        Left = 240
        Top = 0
        Hint = 'Italic'
        AllowAllUp = True
        Caption = 'tbtFontItalic'
        ImageIndex = 1
        Style = tbsCheck
        OnClick = tbtFontItalicClick
      end
      object tbtFontUnderline: TToolButton
        Left = 263
        Top = 0
        Hint = 'Underlined'
        AllowAllUp = True
        Caption = 'tbtFontUnderline'
        ImageIndex = 2
        Style = tbsCheck
        OnClick = tbtFontUnderlineClick
      end
      object ToolButton1: TToolButton
        Left = 286
        Top = 0
        Width = 8
        Caption = 'ToolButton1'
        ImageIndex = 12
        Style = tbsSeparator
      end
      object tbtTextAlignLeft: TToolButton
        Left = 294
        Top = 0
        Hint = 'Left Justify'
        Caption = 'tbtTextAlignLeft'
        Grouped = True
        ImageIndex = 3
        Style = tbsCheck
        OnClick = tbtTextAlignLeftClick
      end
      object tbtTextAlignCenter: TToolButton
        Left = 317
        Top = 0
        Hint = 'Center'
        Caption = 'tbtTextAlignCenter'
        Grouped = True
        ImageIndex = 4
        Style = tbsCheck
        OnClick = tbtTextAlignCenterClick
      end
      object tbtTextAlignRight: TToolButton
        Left = 340
        Top = 0
        Hint = 'Right Justify'
        Caption = 'tbtTextAlignRight'
        Grouped = True
        ImageIndex = 5
        Style = tbsCheck
        OnClick = tbtTextAlignRightClick
      end
      object ToolButton10: TToolButton
        Left = 363
        Top = 0
        Width = 8
        Caption = 'ToolButton10'
        ImageIndex = 6
        Style = tbsSeparator
      end
      object tbtFontColor: TToolButton
        Left = 371
        Top = 0
        Hint = 'Font Color'
        Caption = 'tbtFontColor'
        ImageIndex = 6
        Style = tbsDropDown
        OnClick = tbtFontColorClick
      end
      object tbtHighlightColor: TToolButton
        Left = 407
        Top = 0
        Hint = 'Background Color'
        Caption = 'tbtHighlightColor'
        ImageIndex = 8
        Style = tbsDropDown
        OnClick = tbtHighlightColorClick
      end
      object ToolButton11: TToolButton
        Left = 443
        Top = 0
        Width = 8
        Caption = 'ToolButton11'
        ImageIndex = 7
        Style = tbsSeparator
      end
      object tbtBringToFront: TToolButton
        Tag = 14
        Left = 451
        Top = 0
        Hint = 'Bring to Front'
        Caption = 'tbtBringToFront'
        ImageIndex = 10
        OnClick = ToolButton2Click
      end
      object tbtSendToBack: TToolButton
        Tag = 15
        Left = 474
        Top = 0
        Hint = 'Send to Back'
        Caption = 'tbtSendToBack'
        ImageIndex = 11
        OnClick = ToolButton2Click
      end
    end
  end
  object pmToolBars: TPopupMenu
    OnPopup = pmToolBarsPopup
    Left = 608
    Top = 16
  end
end
