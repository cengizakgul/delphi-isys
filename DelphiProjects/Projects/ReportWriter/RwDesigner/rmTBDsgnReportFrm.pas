unit rmTBDsgnReportFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTBDsgnComponentFrm, Menus, ImgList, ActnList,
  rmFMLExprPanelFrm, StdCtrls, Mask, wwdbedit, Wwdbspin, ISBasicClasses,
  rwDesignClasses, ExtCtrls, ColorPicker, ComCtrls, ToolWin,
  rmCustomFrameFrm, rmTBComponentsFrm, rwDsgnUtils, rmTypes, rmDsgnTypes,
  rwDebugInfo;

type
  TrmTBDsgnReport = class(TrmTBDsgnComponent)
    tbtNewReport: TToolButton;
    actNewReport: TAction;
    actNewReport1: TMenuItem;
    ToolButton2: TToolButton;
    tbtRun: TToolButton;
    tbtPauseRunning: TToolButton;
    tbtTraceIn: TToolButton;
    tbtTraceOver: TToolButton;
    actRun: TAction;
    actPauseRunning: TAction;
    actTraceIn: TAction;
    actTraceOver: TAction;
    actBrkPointToggle: TAction;
    tbtBrkPointToggle: TToolButton;
    ToolButton11: TToolButton;
    actStopRunning: TAction;
    tbtStopRunning: TToolButton;
    actExit: TAction;
    N1: TMenuItem;
    Exit1: TMenuItem;
    actLibrary: TAction;
    SystemLibrary1: TMenuItem;
    N2: TMenuItem;
    actCallStack: TAction;
    miDebugWindows: TMenuItem;
    miCallStack: TMenuItem;
    actWatches: TAction;
    miWatches: TMenuItem;
    actFindError: TAction;
    miSerach: TMenuItem;
    FindErrorbyAddress1: TMenuItem;
    procedure actNewReportExecute(Sender: TObject);
    procedure actRunExecute(Sender: TObject);
    procedure actPauseRunningExecute(Sender: TObject);
    procedure actTraceInExecute(Sender: TObject);
    procedure actTraceOverExecute(Sender: TObject);
    procedure actBrkPointToggleExecute(Sender: TObject);
    procedure actStopRunningExecute(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure actLibraryExecute(Sender: TObject);
    procedure actCallStackExecute(Sender: TObject);
    procedure actWatchesExecute(Sender: TObject);
    procedure actFindErrorExecute(Sender: TObject);
  private
    procedure DisableRunButtons;
    procedure DMRunningProceeded(var Message: TrmMessageRec); message mRunningProceeded;
    procedure DMRunningPaused(var Message: TrmMessageRec); message mRunningPaused;
    procedure DMRunningStopped(var Message: TrmMessageRec); message mRunningStopped;
  protected
    procedure GetListeningMessages(const AMessages: TList); override;
  end;


implementation


{$R *.dfm}

procedure TrmTBDsgnReport.actNewReportExecute(Sender: TObject);
begin
  SendDesignerMessage(Self, mtbNew, 0, 0);
end;

procedure TrmTBDsgnReport.actRunExecute(Sender: TObject);
begin
  DisableRunButtons;
  PostApartmentMessage(Self, mtbRun, 0, 0);
end;

procedure TrmTBDsgnReport.actPauseRunningExecute(Sender: TObject);
begin
  DisableRunButtons;
  PostApartmentMessage(Self, mtbPause, 0, 0);
end;

procedure TrmTBDsgnReport.actTraceInExecute(Sender: TObject);
begin
  DisableRunButtons;
  PostApartmentMessage(Self, mtbTraceIn, 0, 0);
end;

procedure TrmTBDsgnReport.actTraceOverExecute(Sender: TObject);
begin
  DisableRunButtons;
  PostApartmentMessage(Self, mtbTraceOver, 0, 0);
end;

procedure TrmTBDsgnReport.actBrkPointToggleExecute(Sender: TObject);
begin
  PostApartmentMessage(Self, mtbBreakPointToggle, 0, 0);
end;

procedure TrmTBDsgnReport.actStopRunningExecute(Sender: TObject);
begin
  PostApartmentMessage(Self, mtbStop, 0, 0);
end;

procedure TrmTBDsgnReport.actExitExecute(Sender: TObject);
begin
  SendDesignerMessage(Self, mtbClose, 0, 0);
end;

procedure TrmTBDsgnReport.actLibraryExecute(Sender: TObject);
begin
  SendDesignerMessage(Self, mtbOpenLibrary, 0, 0);
end;

procedure TrmTBDsgnReport.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mRunningProceeded));
  AMessages.Add(Pointer(mRunningPaused));
  AMessages.Add(Pointer(mRunningStopped));
end;

procedure TrmTBDsgnReport.DMRunningPaused(var Message: TrmMessageRec);
begin
  actRun.Enabled := True;
  actPauseRunning.Enabled := False;
  actTraceOver.Enabled := True;
  actTraceIn.Enabled := True;
  actStopRunning.Enabled := True;
end;

procedure TrmTBDsgnReport.DMRunningProceeded(var Message: TrmMessageRec);
begin
  actRun.Enabled := False;
  actPauseRunning.Enabled := True;
  actTraceOver.Enabled := False;
  actTraceIn.Enabled := False;
  actStopRunning.Enabled := True;
end;

procedure TrmTBDsgnReport.DMRunningStopped(var Message: TrmMessageRec);
begin
  actRun.Enabled := True;
  actPauseRunning.Enabled := False;
  actTraceOver.Enabled := True;
  actTraceIn.Enabled := True;
  actStopRunning.Enabled := False;
end;

procedure TrmTBDsgnReport.actCallStackExecute(Sender: TObject);
begin
  PostApartmentMessage(Self, mtbShowCallStack, 0, 0);
end;

procedure TrmTBDsgnReport.DisableRunButtons;
begin
  actRun.Enabled := False;
  actPauseRunning.Enabled := False;
  actTraceOver.Enabled := False;
  actTraceIn.Enabled := False;
  actStopRunning.Enabled := False;
end;

procedure TrmTBDsgnReport.actWatchesExecute(Sender: TObject);
begin
  PostApartmentMessage(Self, mtbShowWatches, 0, 0);
end;

procedure TrmTBDsgnReport.actFindErrorExecute(Sender: TObject);
var
  ErrAddr: String;
begin
  ErrAddr := '';
  if InputQuery('Find Error by Address', 'Address', ErrAddr) then
    PostApartmentMessage(Self, mtbShowSourceCodeByAddress, StrToInt(ErrAddr), 0);
end;

end.
