inherited rwPicturePropertyEditor: TrwPicturePropertyEditor
  Left = 463
  Top = 473
  Width = 391
  Height = 295
  Caption = 'Picture'
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOK: TButton
    Left = 302
    Top = 80
  end
  inherited btnCancel: TButton
    Left = 302
    Top = 110
  end
  object Panel1: TPanel
    Left = 6
    Top = 8
    Width = 286
    Height = 253
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 2
    object imgPicture: TImage
      Left = 1
      Top = 1
      Width = 284
      Height = 251
      Align = alClient
      Stretch = True
    end
  end
  object btnLoad: TButton
    Tag = 267
    Left = 302
    Top = 8
    Width = 75
    Height = 23
    Anchors = [akTop, akRight]
    Caption = 'Load'
    TabOrder = 3
    OnClick = btnLoadClick
  end
  object btnSave: TButton
    Tag = 267
    Left = 302
    Top = 36
    Width = 75
    Height = 23
    Anchors = [akTop, akRight]
    Caption = 'Save'
    TabOrder = 4
    OnClick = btnSaveClick
  end
  object OpenPictureDialog: TOpenPictureDialog
    Title = 'Load Picture'
    Left = 224
    Top = 48
  end
  object SavePictureDialog: TSavePictureDialog
    Title = 'Save Picture'
    Left = 230
    Top = 80
  end
end
