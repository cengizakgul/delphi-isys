unit rwPagePropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwPropertyEditorFrm, StdCtrls, ExtCtrls, ComCtrls, rwReport,
  Mask, wwdbedit, Wwdbspin, rwTypes, rwUtils, rwDsgnUtils;

type

  {TrwPagePropertyEditor is class for edition TrwPage property}

  TrwPagePropertyEditor = class(TrwPropertyEditor)
    Panel1: TPanel;
    gbMargins: TGroupBox;
    pnPreview: TPanel;
    pbPreview: TPaintBox;
    speTopMargin: TwwDBSpinEdit;
    speBottomMargin: TwwDBSpinEdit;
    speRightMargin: TwwDBSpinEdit;
    speLeftMargin: TwwDBSpinEdit;
    gbxOrientation: TGroupBox;
    rbPortrait: TRadioButton;
    rbLandscape: TRadioButton;
    chbDuplexing: TCheckBox;
    gbSize: TGroupBox;
    lblPaperSize: TLabel;
    lblWidth: TLabel;
    lblHeight: TLabel;
    cbPaperNames: TComboBox;
    speWidth: TwwDBSpinEdit;
    speHeight: TwwDBSpinEdit;
    imDuplex: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure cbPaperNamesChange(Sender: TObject);
    procedure pbPreviewPaint(Sender: TObject);
    procedure pbPreviewClick(Sender: TObject);
    procedure rbPortraitClick(Sender: TObject);
    procedure imDuplexClick(Sender: TObject);
    procedure speWidthChange(Sender: TObject);
  private
    procedure UpdatePreview;
    procedure UpdateOrientation;
  protected
    procedure SetReadOnly; override;
  end;

procedure PageSetup(APage: TrwPage);

implementation

{$R *.DFM}

procedure PageSetup(APage: TrwPage);
var
  Frm: TrwPagePropertyEditor;
begin
  Frm := TrwPagePropertyEditor.Create(nil, true);

  with Frm do
  try
    Value.Assign(APage);
    if (ShowModal = mrOk) then
      APage.Assign(TPersistent(Value));
  finally
    Free;
  end;
end;

procedure TrwPagePropertyEditor.FormCreate(Sender: TObject);
begin
  inherited;
  FValue := TrwPage.Create;
end;

procedure TrwPagePropertyEditor.FormDestroy(Sender: TObject);
begin
  inherited;
  FValue.Free;
end;

procedure TrwPagePropertyEditor.FormShow(Sender: TObject);
var
  lPage: TrwPage;
  i: Integer;
begin
  inherited;

  for i := Ord(Low(cPaperInfo)) to Ord(High(cPaperInfo)) do
    cbPaperNames.Items.AddObject(cPaperInfo[TrwPaperSize(i)].Name, Pointer(i));

  lPage := TrwPage(Value);

  speHeight.Value := Round(ConvertUnit(lPage.Height / 10, utMillimeters, DesignUnits)*1000)/1000;
  speWidth.Value := Round(ConvertUnit(lPage.Width / 10, utMillimeters, DesignUnits)*1000)/1000;
  speTopMargin.Value := Round(ConvertUnit(lPage.TopMargin / 10, utMillimeters, DesignUnits)*1000)/1000;
  speBottomMargin.Value := Round(ConvertUnit(lPage.BottomMargin / 10, utMillimeters, DesignUnits)*1000)/1000;
  speLeftMargin.Value := Round(ConvertUnit(lPage.LeftMargin / 10, utMillimeters, DesignUnits)*1000)/1000;
  speRightMargin.Value := Round(ConvertUnit(lPage.RightMargin / 10, utMillimeters, DesignUnits)*1000)/1000;

  chbDuplexing.Checked := lPage.Duplexing;

  if (lPage.PaperOrientation = rpoPortrait) then
    rbPortrait.Checked := True
  else
    rbLandscape.Checked := True;
  cbPaperNames.ItemIndex := Ord(lPage.PaperSize);
  cbPaperNames.OnChange(nil);
end;

procedure TrwPagePropertyEditor.btnOKClick(Sender: TObject);
var
  lPage: TrwPage;
  h: Integer;
begin
  inherited;

  lPage := TrwPage(Value);

  lPage.PaperSize := TrwPaperSize(cbPaperNames.ItemIndex);
  lPage.Height := Round(ConvertUnit(speHeight.Value, DesignUnits, utMillimeters) * 10);
  lPage.Width := Round(ConvertUnit(speWidth.Value, DesignUnits, utMillimeters) * 10);
  lPage.TopMargin := Round(ConvertUnit(speTopMargin.Value, DesignUnits, utMillimeters) * 10);
  lPage.BottomMargin := Round(ConvertUnit(speBottomMargin.Value, DesignUnits, utMillimeters) * 10);
  lPage.LeftMargin := Round(ConvertUnit(speLeftMargin.Value, DesignUnits, utMillimeters) * 10);
  lPage.RightMargin := Round(ConvertUnit(speRightMargin.Value, DesignUnits, utMillimeters) * 10);

  if (rbPortrait.Checked) then
    lPage.PaperOrientation := rpoPortrait
  else
  begin
    lPage.PaperOrientation := rpoLandscape;
    h := lPage.Height;
    lPage.Height := lPage.Width;
    lPage.Width := h;
  end;

  lPage.Duplexing := chbDuplexing.Checked;    
end;

procedure TrwPagePropertyEditor.cbPaperNamesChange(Sender: TObject);
begin
  if cbPaperNames.ItemIndex <> Ord(psCustom) then
  begin
    speHeight.Value :=
      Round(ConvertUnit(cPaperInfo[TrwPaperSize(Integer(cbPaperNames.Items.Objects[cbPaperNames.ItemIndex]))].Height / 10,
      utMillimeters, DesignUnits)*1000)/1000;

    speWidth.Value :=
      Round(ConvertUnit(cPaperInfo[TrwPaperSize(Integer(cbPaperNames.Items.Objects[cbPaperNames.ItemIndex]))].Width / 10,
      utMillimeters, DesignUnits)*1000)/1000;
  end;

  speHeight.Enabled := Integer(cbPaperNames.Items.Objects[cbPaperNames.ItemIndex]) = Ord(psCustom);
  speWidth.Enabled := speHeight.Enabled;

  if rbLandscape.Checked then
    UpdateOrientation;

  UpdatePreview;
end;

procedure TrwPagePropertyEditor.SetReadOnly;
begin
  inherited;
  Panel1.Enabled := False;
end;


procedure TrwPagePropertyEditor.UpdatePreview;
begin
  if speHeight.Value > speWidth.Value then
  begin
    pbPreview.Height := pnPreview.ClientHeight - 20;
    pbPreview.Width := Round(pbPreview.Height * (speWidth.Value / speHeight.Value));
  end
  else
  begin
    pbPreview.Width := pnPreview.ClientHeight - 20;
    pbPreview.Height := Round(pbPreview.Width * (speHeight.Value / speWidth.Value));
  end;

  pbPreview.Top := Round((pnPreview.ClientHeight - pbPreview.Height) / 2);
  pbPreview.Left := Round((pnPreview.ClientWidth - pbPreview.Width) / 2);

  pbPreview.Invalidate;
  if chbDuplexing.Checked then
  begin
    imDuplex.Top := pbPreview.Top + pbPreview.Height - imDuplex.Height;
    imDuplex.Left := pbPreview.Left + pbPreview.Width - imDuplex.Width;
    imDuplex.Visible := True;
  end
  else
    imDuplex.Visible := False;
end;


procedure TrwPagePropertyEditor.pbPreviewPaint(Sender: TObject);
var
  R, R1:  TRect;
  i, d: Integer;
begin
  with pbPreview.Canvas do
  begin
    Brush.Color := clWhite;
    Brush.Style := bsSolid;
    Pen.Width := 1;
    FillRect(pbPreview.ClientRect);

    R := pbPreview.ClientRect;
    R.Top := Round(R.Bottom * speTopMargin.Value / speHeight.Value);
    R.Bottom := R.Bottom - Round(R.Bottom * speBottomMargin.Value / speHeight.Value);
    R.Left := Round(R.Right * speLeftMargin.Value / speWidth.Value);
    R.Right := R.Right - Round(R.Right * speRightMargin.Value / speWidth.Value);

    d := 10;
    Pen.Color := clGray;
    Brush.Style := bsClear;
    Rectangle(R);
    R1 := R;
    R1.Bottom := R1.Top + d * 2;
    Brush.Style := bsSolid;
    Brush.Color := 16704474;
    Rectangle(R1);

    i := R.Top + d * 3;
    while i <= R.Bottom - d do
    begin
      MoveTo(R.Left, i);
      LineTo(R.Right, i);
      Inc(i, d);
    end;

    d := 20;
    i := R.Left + d;
    while i <= R.Right - d do
    begin
      MoveTo(i, R.Top);
      LineTo(i, R.Bottom);
      Inc(i, d);
    end;

    Brush.Style := bsClear;
    Pen.Color := clBlack;
    Rectangle(pbPreview.ClientRect);
  end;
end;

procedure TrwPagePropertyEditor.pbPreviewClick(Sender: TObject);
begin
  rbPortrait.Checked := not rbPortrait.Checked;
  rbLandscape.Checked := not rbPortrait.Checked;
end;

procedure TrwPagePropertyEditor.UpdateOrientation;
var
  h: Extended;
begin
  h := speHeight.Value;
  speHeight.Value := speWidth.Value;
  speWidth.Value := h;
end;

procedure TrwPagePropertyEditor.rbPortraitClick(Sender: TObject);
begin
  UpdateOrientation;
  UpdatePreview;
end;

procedure TrwPagePropertyEditor.imDuplexClick(Sender: TObject);
begin
  pbPreview.OnClick(nil);
end;

procedure TrwPagePropertyEditor.speWidthChange(Sender: TObject);
begin
  UpdatePreview;
end;

initialization
  RegisterClass(TrwPagePropertyEditor);

end.
