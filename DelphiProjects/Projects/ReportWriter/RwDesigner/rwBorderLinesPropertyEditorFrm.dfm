inherited rwBorderLinesPropertyEditor: TrwBorderLinesPropertyEditor
  Left = 425
  Top = 184
  VertScrollBar.Range = 0
  BorderStyle = bsToolWindow
  Caption = 'Border Lines'
  ClientHeight = 207
  ClientWidth = 478
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 7
    Top = 20
    Width = 24
    Height = 13
    Caption = 'Color'
  end
  object Label2: TLabel [1]
    Left = 7
    Top = 71
    Width = 47
    Height = 13
    Caption = 'Line Type'
  end
  object Label3: TLabel [2]
    Left = 7
    Top = 119
    Width = 51
    Height = 13
    Caption = 'Line Width'
  end
  object Label4: TLabel [3]
    Left = 255
    Top = 4
    Width = 40
    Height = 13
    Caption = 'Example'
  end
  object Label5: TLabel [4]
    Left = 39
    Top = 4
    Width = 67
    Height = 13
    Caption = 'Lines Property'
  end
  object Bevel1: TBevel [5]
    Left = 160
    Top = 7
    Width = 2
    Height = 221
  end
  inherited btnOK: TButton
    Left = 397
  end
  inherited btnCancel: TButton
    Left = 397
  end
  object cbColor: TColorBox
    Left = 7
    Top = 36
    Width = 145
    Height = 22
    Style = [cbStandardColors, cbExtendedColors, cbCustomColor, cbPrettyNames]
    ItemHeight = 16
    TabOrder = 2
  end
  object cbStyle: TComboBox
    Left = 7
    Top = 87
    Width = 145
    Height = 22
    Style = csOwnerDrawFixed
    ItemHeight = 16
    ItemIndex = 0
    TabOrder = 3
    Text = '0'
    OnDrawItem = cbStyleDrawItem
    Items.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4')
  end
  object cbWidth: TComboBox
    Left = 7
    Top = 136
    Width = 145
    Height = 22
    Style = csOwnerDrawFixed
    ItemHeight = 16
    ItemIndex = 0
    TabOrder = 4
    Text = '1'
    OnDrawItem = cbWidthDrawItem
    Items.Strings = (
      '1'
      '2'
      '3'
      '4'
      '5')
  end
  object pnCellExample: TPanel
    Left = 167
    Top = 22
    Width = 186
    Height = 171
    BevelOuter = bvNone
    TabOrder = 5
    object pbCellExample: TPaintBox
      Left = 48
      Top = 8
      Width = 120
      Height = 120
      Color = clBtnFace
      ParentColor = False
      OnPaint = pbCellExamplePaint
    end
    object sbTopLine: TSpeedButton
      Left = 8
      Top = 22
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF7FFFFF7FFF
        FF7FFFFFFFFFFFFFFFFFFF7FFFFF7FFFFF7FFFFFFFFFFFFFFFFFFF7F7F7F7F7F
        7F7FFFFFFFFFFFFFFFFFFF7FFFFF7FFFFF7FFFFFFFFFFFFFFFFFFF7FFFFF7FFF
        FF7FFFFFFFFFFFFFFFFFFF0000000000000FFFFFFFFFFFFFFFFF}
      Margin = 0
      OnClick = sbTopLineClick
    end
    object sbBottomLine: TSpeedButton
      Left = 8
      Top = 85
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 2
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0000000000000FFFFFFFFFFFFFFFFFFF7FFFFF7FFF
        FF7FFFFFFFFFFFFFFFFFFF7FFFFF7FFFFF7FFFFFFFFFFFFFFFFFFF7F7F7F7F7F
        7F7FFFFFFFFFFFFFFFFFFF7FFFFF7FFFFF7FFFFFFFFFFFFFFFFFFF7FFFFF7FFF
        FF7FFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7FFFFFFFFFFFFFFFFF}
      Margin = 0
      OnClick = sbBottomLineClick
    end
    object sbLeftLine: TSpeedButton
      Left = 57
      Top = 137
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 3
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0F7F7F7F7F7F7FFF0FFFFFFFFFFFFFFF0FFFFF7FFF
        FF7FFF0FFFFFFFFFFFFFFF0FFFFF7FFFFF7FFF0FFFFFFFFFFFFFFF0F7F7F7F7F
        7F7FFF0FFFFFFFFFFFFFFF0FFFFF7FFFFF7FFF0FFFFFFFFFFFFFFF0FFFFF7FFF
        FF7FFF0FFFFFFFFFFFFFFF0F7F7F7F7F7F7FFFFFFFFFFFFFFFFF}
      Margin = 0
      OnClick = sbLeftLineClick
    end
    object sbRightLine: TSpeedButton
      Left = 139
      Top = 137
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 4
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F0FFFFFFFFFFFFFFF0FFF7FFFFF7FFF
        FF0FFFFFFFFFFFFFFF0FFF7FFFFF7FFFFF0FFFFFFFFFFFFFFF0FFF7F7F7F7F7F
        7F0FFFFFFFFFFFFFFF0FFF7FFFFF7FFFFF0FFFFFFFFFFFFFFF0FFF7FFFFF7FFF
        FF0FFFFFFFFFFFFFFF0FFF7F7F7F7F7F7F0FFFFFFFFFFFFFFFFF}
      Margin = 0
      OnClick = sbRightLineClick
    end
  end
end
