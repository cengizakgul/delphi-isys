unit rmTrwGroupBoxPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, StdCtrls, ExtCtrls, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls, rmOPEColorFrm, rmOPEFontFrm;

type
  TrmTrwGroupBoxProp = class(TrmOPBasic)
    frmCaption: TrmOPEStringSingleLine;
    frmFont: TrmOPEFont;
  protected
    procedure GetPropsFromObject; override;
  end;


implementation

{$R *.dfm}

{ TrmTrwGroupBoxProp }

procedure TrmTrwGroupBoxProp.GetPropsFromObject;
begin
  inherited;
  frmCaption.PropertyName := 'Text';
  frmCaption.ShowPropValue;
end;

end.
