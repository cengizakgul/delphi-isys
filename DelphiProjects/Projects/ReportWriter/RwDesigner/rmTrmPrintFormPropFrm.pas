unit rmTrmPrintFormPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmPrintControlPropFrm, StdCtrls, ExtCtrls,
  rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, ComCtrls,
  rmOPEPageSetupFrm, rmOPEBooleanFrm, rmOPEExpressionFrm,
  rmOPEBlockingControlFrm;

type
  TrmTrmPrintFormProp = class(TrmTrmPrintControlProp)
    rmOPEPageSetup1: TrmOPEPageSetup;
  private
  public
  end;

implementation

{$R *.dfm}

end.
 