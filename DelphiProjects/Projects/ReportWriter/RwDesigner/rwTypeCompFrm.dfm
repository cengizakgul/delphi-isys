object rwTypeComp: TrwTypeComp
  Left = 360
  Top = 207
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Ancestor for Component'
  ClientHeight = 284
  ClientWidth = 370
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 3
    Width = 343
    Height = 26
    Caption = 
      'Choose ancestor from wich New Component will derived. This will ' +
      'define behavior and appearance of your component.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object btnOk: TButton
    Left = 196
    Top = 255
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object Button2: TButton
    Left = 285
    Top = 255
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 6
    Top = 37
    Width = 359
    Height = 207
    Caption = 'Existing Components'
    TabOrder = 0
    object lvComps: TListView
      Left = 9
      Top = 17
      Width = 340
      Height = 181
      Anchors = [akLeft, akTop, akRight, akBottom]
      Columns = <>
      HideSelection = False
      LargeImages = imlComps
      ReadOnly = True
      SmallImages = imlComps
      TabOrder = 0
      ViewStyle = vsSmallIcon
      OnDblClick = lvCompsDblClick
    end
  end
  object imlComps: TImageList
    Left = 230
    Top = 132
  end
end
