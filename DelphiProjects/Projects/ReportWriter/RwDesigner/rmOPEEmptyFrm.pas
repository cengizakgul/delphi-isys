unit rmOPEEmptyFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls;

type
  TrmOPEEmpty = class(TrmObjPropertyEditor)
    Label1: TLabel;
  private
  public
    function  NewPropertyValue: Variant; override;
  end;


implementation

{$R *.dfm}

{ TrmObjPropertyEditor1 }

function TrmOPEEmpty.NewPropertyValue: Variant;
begin
  Result := Null;
end;

end.
 