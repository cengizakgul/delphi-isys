unit rmOPEStringValuesFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, Grids, ValEdit;

type
  TrmOPEStringValues = class(TrmObjPropertyEditor)
    vlValues: TValueListEditor;
    procedure vlValuesStringsChange(Sender: TObject);
  private
  public
    procedure ShowPropValue; override;
    function  NewPropertyValue: Variant; override;
  end;

implementation

{$R *.dfm}

function TrmOPEStringValues.NewPropertyValue: Variant;
begin
  Result := Integer(Pointer(vlValues.Strings));
end;

procedure TrmOPEStringValues.ShowPropValue;
begin
  inherited;
  vlValues.Strings := TStrings(Pointer(Integer(OldPropertyValue)));
end;

procedure TrmOPEStringValues.vlValuesStringsChange(Sender: TObject);
begin
  NotifyOfChanges;
end;

end.
