unit rwFontPropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwPropertyEditorFrm, StdCtrls, rwGraphics, rwUtils, rwDsgnUtils;

type
  TrwFontPropertyEditor = class(TrwPropertyEditor)
    FontDialog: TFontDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
  public
    function ShowModal: Integer; override;
  end;

implementation

{$R *.DFM}

function TrwFontPropertyEditor.ShowModal: Integer;
begin
  TrwFont(FValue).AssignTo(FontDialog.Font);

{ TODO : analyze true type }
{  if PrintableObject then
    FontDialog.Options := FontDialog.Options + [fdTrueTypeOnly]
  else
    FontDialog.Options := FontDialog.Options - [fdTrueTypeOnly];}


  if (PrintableObject) and (Assigned(RMDesigner)) then
    FontDialog.Font.Size := Round(ConvertUnit(FontDialog.Font.Size, utLogicalPixels, utScreenPixels));

  if btnOK.Enabled and FontDialog.Execute then
  begin
    if PrintableObject and (Assigned(RMDesigner)) then
      FontDialog.Font.Size := Round(ConvertUnit(FontDialog.Font.Size, utScreenPixels, utLogicalPixels));
    TrwFont(FValue).Assign(FontDialog.Font);
    Result := mrOK;
  end
  else
    Result := mrCancel;
end;

procedure TrwFontPropertyEditor.FormCreate(Sender: TObject);
begin
  inherited;

  FValue := TrwFont.Create(Self);
end;

procedure TrwFontPropertyEditor.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FValue);
  inherited;
end;

initialization
  RegisterClass(TrwFontPropertyEditor);

end.
