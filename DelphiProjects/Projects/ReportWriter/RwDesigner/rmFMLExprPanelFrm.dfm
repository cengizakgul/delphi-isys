object rmFMLExprPanel: TrmFMLExprPanel
  Left = 0
  Top = 0
  Width = 294
  Height = 94
  TabOrder = 0
  OnResize = FrameResize
  object pnlCanvas: TPanel
    Left = 0
    Top = 0
    Width = 294
    Height = 94
    Align = alClient
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clWhite
    PopupMenu = pmClipbrd
    TabOrder = 0
    OnClick = pnlCanvasClick
    OnDragOver = pnlCanvasDragOver
  end
  object pmClipbrd: TPopupMenu
    OnPopup = pmClipbrdPopup
    Left = 218
    Top = 42
    object miCopy: TMenuItem
      Caption = 'Copy'
      ShortCut = 16451
      OnClick = miCopyClick
    end
    object miPaste: TMenuItem
      Caption = 'Paste'
      ShortCut = 16470
      OnClick = miPasteClick
    end
  end
end
