unit rmOPENumericFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls, Mask, wwdbedit, Wwdbspin,
  ISBasicClasses, rwDesignClasses;

type
  TrmOPENumeric = class(TrmObjPropertyEditor)
    lPropName: TLabel;
    edNumeric: TisRWDBSpinEdit;
    procedure FrameResize(Sender: TObject);
    procedure edNumericChange(Sender: TObject);
  public
    procedure ShowPropValue; override;
    function  NewPropertyValue: Variant; override;    
  end;

implementation

{$R *.dfm}

{ TrmOPENumeric }

procedure TrmOPENumeric.ShowPropValue;
begin
  inherited;
  try
    edNumeric.Value := OldPropertyValue;
  except
    edNumeric.Value := 0; 
  end;
end;

procedure TrmOPENumeric.FrameResize(Sender: TObject);
begin
  inherited;
  edNumeric.Width := ClientWidth - edNumeric.Left;
end;

function TrmOPENumeric.NewPropertyValue: Variant;
begin
  Result := edNumeric.Value;
end;

procedure TrmOPENumeric.edNumericChange(Sender: TObject);
begin
  NotifyOfChanges;
end;

end.
