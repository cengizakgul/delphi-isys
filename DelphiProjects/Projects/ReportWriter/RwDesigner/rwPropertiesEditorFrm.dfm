object rwPropertiesEditor: TrwPropertiesEditor
  Left = 0
  Top = 0
  Width = 261
  Height = 454
  AutoScroll = False
  Color = clBtnFace
  ParentColor = False
  TabOrder = 0
  object pgcInspector: TPageControl
    Left = 0
    Top = 0
    Width = 261
    Height = 430
    ActivePage = tsProperties
    Align = alClient
    Images = DsgnRes.ilButtons
    MultiLine = True
    Style = tsButtons
    TabOrder = 0
    OnChange = pgcInspectorChange
    object tbsObjTree: TTabSheet
      Caption = 'Objects Tree'
      ImageIndex = 71
      object tvObjTree: TTreeView
        Left = 0
        Top = 0
        Width = 253
        Height = 399
        Align = alClient
        HideSelection = False
        Images = DsgnRes.ilButtons
        Indent = 19
        ReadOnly = True
        TabOrder = 0
        OnChange = tvObjTreeChange
        OnDblClick = tvObjTreeDblClick
      end
    end
    object tsProperties: TTabSheet
      Caption = 'Properties'
      ImageIndex = 35
    end
    object tsEvents: TTabSheet
      Caption = 'Events'
      ImageIndex = 55
      object lbEvents: TListBox
        Left = 0
        Top = 0
        Width = 253
        Height = 399
        Align = alClient
        ItemHeight = 13
        PopupMenu = pmUserEvent
        TabOrder = 0
        OnClick = lbEventsClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 430
    Width = 261
    Height = 24
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object pnlCompName: TPanel
      Left = 28
      Top = 0
      Width = 233
      Height = 24
      Align = alClient
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 28
      Height = 24
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object btnPropRevert: TSpeedButton
        Left = 1
        Top = 1
        Width = 23
        Height = 23
        Hint = 'Revert property to its ancestor defaults'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDD0DDDDD00000DDDDD0DDDDD0000DDDDDDD0DDDD000DDDDDDD
          D0DDDD00D0DDDDDDD0DDDD0DDD00DDDD0DDDDDDDDDDD0000DDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        Layout = blGlyphBottom
        Margin = 1
        ParentShowHint = False
        ShowHint = True
        OnClick = btnPropRevertClick
      end
    end
  end
  object pmUserEvent: TPopupMenu
    Left = 92
    Top = 124
    object miRegUserEvent: TMenuItem
      Caption = 'Register New User Event...'
      OnClick = miRegUserEventClick
    end
    object miUnRegUserEvent: TMenuItem
      Caption = 'Unregister User Event'
      OnClick = miUnRegUserEventClick
    end
  end
  object pmUserProps: TPopupMenu
    Left = 92
    Top = 180
    object miRegUserProp: TMenuItem
      Caption = 'Register New User Property...'
      OnClick = miRegUserPropClick
    end
    object miUnRegUserProp: TMenuItem
      Caption = 'Unregister User Property'
      OnClick = miUnRegUserPropClick
    end
  end
end
