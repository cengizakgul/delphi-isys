inherited rmTrmlCBFieldValuesProp: TrmTrmlCBFieldValuesProp
  Height = 321
  inherited pcCategories: TPageControl
    Height = 321
    inherited tsBasic: TTabSheet
      object grbDDInfo: TGroupBox
        Left = 8
        Top = 184
        Width = 293
        Height = 91
        Caption = 'Data Dictionary Info'
        TabOrder = 3
        inline frmTable: TrmOPEEnum
          Left = 10
          Top = 23
          Width = 270
          Height = 21
          AutoScroll = False
          AutoSize = True
          TabOrder = 0
          inherited lPropName: TLabel
            Caption = 'Table'
          end
          inherited cbList: TComboBox
            Width = 215
            OnChange = frmTablecbListChange
          end
        end
        inline frmField: TrmOPEEnum
          Left = 10
          Top = 56
          Width = 270
          Height = 21
          AutoScroll = False
          AutoSize = True
          TabOrder = 1
          inherited lPropName: TLabel
            Width = 22
            Caption = 'Field'
          end
          inherited cbList: TComboBox
            Width = 215
          end
        end
      end
    end
  end
end
