object rwLibCompChoice: TrwLibCompChoice
  Left = 339
  Top = 180
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Library of Components'
  ClientHeight = 345
  ClientWidth = 655
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 6
    Top = 7
    Width = 305
    Height = 333
    Caption = 'Components'
    TabOrder = 0
    object tvLibComp: TTreeView
      Left = 9
      Top = 16
      Width = 286
      Height = 307
      HideSelection = False
      Images = DsgnRes.ilButtons
      Indent = 19
      TabOrder = 0
      OnChange = tvLibCompChange
      OnCompare = tvLibCompCompare
      OnDblClick = tvLibCompDblClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 320
    Top = 7
    Width = 329
    Height = 218
    Caption = 'Description'
    TabOrder = 1
    object memDescript: TMemo
      Left = 9
      Top = 16
      Width = 310
      Height = 193
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
      WordWrap = False
    end
  end
  object btnOK: TButton
    Left = 476
    Top = 314
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object btnCancel: TButton
    Left = 568
    Top = 314
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
