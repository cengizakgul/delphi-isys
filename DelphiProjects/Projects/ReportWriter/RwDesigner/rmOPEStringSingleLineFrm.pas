unit rmOPEStringSingleLineFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls;

type
  TrmOPEStringSingleLine = class(TrmObjPropertyEditor)
    lPropName: TLabel;
    edString: TEdit;
    procedure FrameResize(Sender: TObject);
    procedure edStringChange(Sender: TObject);
  public
    procedure ShowPropValue; override;
    function  NewPropertyValue: Variant; override;
  end;

implementation

{$R *.dfm}

{ TrmOPEName }


procedure TrmOPEStringSingleLine.ShowPropValue;
begin
  inherited;
  edString.Text := VarToStr(OldPropertyValue);
end;

procedure TrmOPEStringSingleLine.FrameResize(Sender: TObject);
begin
  inherited;
  edString.Width := ClientWidth - edString.Left;
end;

function TrmOPEStringSingleLine.NewPropertyValue: Variant;
begin
  Result := edString.Text;
end;

procedure TrmOPEStringSingleLine.edStringChange(Sender: TObject);
begin
  NotifyOfChanges;
end;

end.
