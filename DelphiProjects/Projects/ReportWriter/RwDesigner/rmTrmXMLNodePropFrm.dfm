inherited rmTrmXMLNodeProp: TrmTrmXMLNodeProp
  Width = 350
  Height = 126
  inherited pcCategories: TPageControl
    Width = 350
    Height = 126
    ActivePage = tsBasic
    inherited tcAppearance: TTabSheet
      Caption = 'Processing'
      TabVisible = True
      inline frmBlockParentIfEmpty: TrmOPEBoolean
        Left = 8
        Top = 36
        Width = 329
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
        inherited chbProp: TCheckBox
          Width = 329
          Caption = 'Do not produce parent element if nothing produced (Inner Join)'
        end
      end
      inline frmInactive: TrmOPEBoolean
        Left = 8
        Top = 10
        Width = 77
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited chbProp: TCheckBox
          Width = 77
          Caption = 'Inactive'
        end
      end
    end
  end
end
