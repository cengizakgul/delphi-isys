unit rmDsgnNonVisualObjectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDsgnAreaFrm, rmTypes, rwBasicClasses, rmLibCompChooseFrm, rmDsgnTypes,
  Menus;

type
  TrmDsgnNonVisualObject = class(TrmDsgnArea)
  public
    constructor Create(AOwner: TComponent); override;
    function GetDsgnArea: TWinControl; override;
    function CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject; override;
  end;

implementation

{$R *.dfm}

{ TrmDsgnNonVisualObject }

constructor TrmDsgnNonVisualObject.Create(AOwner: TComponent);
begin
  inherited;
  FImageIndex := 1000;
  FAreaType := rmDATNotVisualComp;
end;

function TrmDsgnNonVisualObject.CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject;
var
  LibComp: IrmComponent;
begin
  if AClassName = 'LIBRARY' then
  begin
    LibComp := ChoiceLibComp(['TrwNoVisualComponent'], AOwner, Self);
    if Assigned(LibComp) then
      AClassName := LibComp.GetName
    else
      Exit;
  end;

  if not Assigned(AOwner) then
    AOwner := DesignObject;

  Result := AOwner.CreateChildObject(AClassName);
end;

function TrmDsgnNonVisualObject.GetDsgnArea: TWinControl;
begin
  Result := Self;
end;

end.
 