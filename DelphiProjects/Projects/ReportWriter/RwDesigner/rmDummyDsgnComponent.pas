unit rmDummyDsgnComponent;

interface

uses
  Classes, Windows, SysUtils, Controls, Variants, rwUtils, Dialogs,
  rwEngineTypes, rmTypes, rwTypes, EvStreamUtils;

type
  TrmDummyDsgnComponent = class(TComponent, IrmDesignObject)
  protected
    function  VCLObject: TObject;
    function  GetDesigner: IrmDesigner;
    procedure SetDesigner(ADesigner: IrmDesigner);
    function  RealObject: TComponent;
    function  ObjectType: TrwDsgnObjectType; virtual;
    function  IsVirtualOwner: Boolean;
    function  IsInheritedComponent: Boolean;
    function  GetWinControlParent: TWinControl;
    procedure Notify(AEvent: TrmDesignEventType);
    function  GetParentExtRect: TRect;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; virtual;
    function  ObjectAtPos(APos: TPoint): IrmDesignObject;
    function  GetParent: IrmDesignObject;
    procedure SetParent(AParent: IrmDesignObject);
    function  GetObjectIndex: Integer;
    procedure SetObjectIndex(const AIndex: Integer);
    function  ClientToScreen(APos: TPoint): TPoint;
    function  ScreenToClient(APos: TPoint): TPoint;
    procedure WriteComp(Writer: TWriter);
    function  GetVarFunctHolder: IrmVarFunctHolder;
    function  GetFormulas: IrmFMLFormulas;
    function  GetBoundsRect: TRect;
    procedure SetBoundsRect(ARect: TRect);
    function  GetLeft: Integer;
    procedure SetLeft(AValue: Integer);
    function  GetTop: Integer;
    procedure SetTop(AValue: Integer);
    function  GetWidth: Integer;
    procedure SetWidth(AValue: Integer);
    function  GetHeight: Integer;
    procedure SetHeight(AValue: Integer);
    procedure Refresh;

    function  GetClassName: String;
    function  IsLibComponent: Boolean;
    function  ObjectInheritsFrom(const AClassName: String): Boolean;
    function  PropertyExists(const APropertyName: String): Boolean;
    function  PropertiesByTypeName(const ATypeName: String): TrwStrList;
    procedure SetPropertyValue(const APropertyName: String; Value: Variant);
    function  GetPropertyValue(const APropertyName: String): Variant;
    function  GetRootOwner: IrmDesignObject;
    function  GetPathFromRootOwner: String;
    function  GetPathFromRootParent: String;
    function  GetInterfacedPropByType(const APropertyType: String): IrmInterfacedObject;

    function  GetLibComponentByClass(const AClassName: String): IrmComponent;
    procedure GetAvailableClassList(const AList: TStringList);

    function  CreateChildObject(const AClassName: String): IrmDesignObject;
    procedure AddChildObject(AObject: IrmDesignObject);
    procedure RemoveChildObject(AObject: IrmDesignObject);
    procedure InitializeForDesign;
    function  GetObjectOwner: IrmDesignObject;
    function  GetChildrenCount: Integer;
    function  GetChildByIndex(AIndex: Integer): IrmDesignObject;
    function  GetControlCount: Integer;
    function  GetControlByIndex(AIndex: Integer): IrmDesignObject;
    function  GetObjectClassDescription: String; virtual;
    function  GetEvents: IrmFunctions;
    function  CallCustomFunction(const AFunctionName: String; const AParams: array of Variant): Variant;
  end;


  TrmSystemLibraryHolder = class(TrmDummyDsgnComponent)
  private
    FVarsModified: Boolean;
    FFunctsModified: Boolean;
    FCompsModified: Boolean;

  protected
    function  GetObjectClassDescription: String; override;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;

  public
    property VarsModified: Boolean read FVarsModified write FVarsModified;
    property FunctsModified: Boolean read FFunctsModified write FFunctsModified;
    property CompsModified: Boolean read FCompsModified write FCompsModified;

    procedure StoreLibrary;
    procedure ReloadLibrary;
    procedure SaveToFile(const AFileName: String);
    procedure LoadFromFile(const AFileName: String);
    procedure CompileLibrary;

    procedure AfterConstruction; override;
  end;


  TrwdLibVarFunctHolder = class(TrmDummyDsgnComponent, IrmVarFunctHolder)
  protected
    function  ObjectType: TrwDsgnObjectType; override;
    function  GetVariables: IrmVariables;
    function  GetLocalFunctions:  IrmFunctions;
    function  GetLocalComponents: IrmComponents;
  end;


implementation


{ TrmDummyDsgnComponent }

procedure TrmDummyDsgnComponent.AddChildObject(AObject: IrmDesignObject);
begin
end;

function TrmDummyDsgnComponent.CallCustomFunction(const AFunctionName: String; const AParams: array of Variant): Variant;
begin
  Result := Unassigned;
end;

function TrmDummyDsgnComponent.ClientToScreen(APos: TPoint): TPoint;
begin
  Result := Point(0, 0);
end;

function TrmDummyDsgnComponent.CreateChildObject(const AClassName: String): IrmDesignObject;
begin
  Result := nil;
end;

function TrmDummyDsgnComponent.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  ZeroMemory(@Result, SizeOf(Result));
end;

procedure TrmDummyDsgnComponent.GetAvailableClassList(const AList: TStringList);
var
  i: Integer;
begin
  AList.Capacity := GetVarFunctHolder.GetLocalComponents.GetItemCount;

  for i := 0 to GetVarFunctHolder.GetLocalComponents.GetItemCount - 1 do
    AList.AddObject(UpperCase(GetVarFunctHolder.GetLocalComponents.GetComponentByIndex(i).GetClassName), Pointer(GetVarFunctHolder.GetLocalComponents.GetComponentByIndex(i)));
end;

function TrmDummyDsgnComponent.GetBoundsRect: TRect;
begin
  Result := Rect(0, 0, 0, 0);
end;

function TrmDummyDsgnComponent.GetChildByIndex(AIndex: Integer): IrmDesignObject;
begin
  Result := Components[AIndex] as IrmDesignObject;
end;

function TrmDummyDsgnComponent.GetChildrenCount: Integer;
begin
  Result := ComponentCount;
end;

function TrmDummyDsgnComponent.GetClassName: String;
begin
  Result := 'LIBRARY';
end;

function TrmDummyDsgnComponent.GetControlByIndex(AIndex: Integer): IrmDesignObject;
begin
  Result := nil;
end;

function TrmDummyDsgnComponent.GetControlCount: Integer;
begin
  Result := 0;
end;

function TrmDummyDsgnComponent.GetDesigner: IrmDesigner;
begin
  Result := nil;
end;

function TrmDummyDsgnComponent.GetEvents: IrmFunctions;
begin
  Result := nil;
end;

function TrmDummyDsgnComponent.GetFormulas: IrmFMLFormulas;
begin
  Result := nil;
end;

function TrmDummyDsgnComponent.GetHeight: Integer;
begin
  Result := 0;
end;

function TrmDummyDsgnComponent.GetInterfacedPropByType(const APropertyType: String): IrmInterfacedObject;
begin
  Result := nil;
end;

function TrmDummyDsgnComponent.GetLeft: Integer;
begin
  Result := 0;
end;

function TrmDummyDsgnComponent.GetLibComponentByClass(const AClassName: String): IrmComponent;
begin
  GetVarFunctHolder.GetLocalComponents.GetComponentByClassName(AClassName);
end;

function TrmDummyDsgnComponent.GetObjectClassDescription: String;
begin
  Result := '';
end;

function TrmDummyDsgnComponent.GetObjectIndex: Integer;
begin
  Result := -1;
end;

function TrmDummyDsgnComponent.GetObjectOwner: IrmDesignObject;
begin
  Result := nil;
end;

function TrmDummyDsgnComponent.GetParent: IrmDesignObject;
begin
  Result := nil;
end;

function TrmDummyDsgnComponent.GetParentExtRect: TRect;
begin
  Result := Rect(0, 0, 0, 0);
end;

function TrmDummyDsgnComponent.GetPathFromRootOwner: String;
begin
  Result := '';
end;

function TrmDummyDsgnComponent.GetPathFromRootParent: String;
begin
  Result := '';
end;

function TrmDummyDsgnComponent.GetPropertyValue(const APropertyName: String): Variant;
begin
  Result := Unassigned;
end;

function TrmDummyDsgnComponent.GetRootOwner: IrmDesignObject;
begin
  Result := nil;
end;

function TrmDummyDsgnComponent.GetTop: Integer;
begin
  Result := 0;
end;

function TrmDummyDsgnComponent.GetVarFunctHolder: IrmVarFunctHolder;
begin
  Result := Components[0] as IrmVarFunctHolder;
end;

function TrmDummyDsgnComponent.GetWidth: Integer;
begin
  Result := 0;
end;

function TrmDummyDsgnComponent.GetWinControlParent: TWinControl;
begin
  Result := nil;
end;

procedure TrmDummyDsgnComponent.InitializeForDesign;
begin
end;

function TrmDummyDsgnComponent.IsInheritedComponent: Boolean;
begin
  Result := False;
end;

function TrmDummyDsgnComponent.IsLibComponent: Boolean;
begin
  Result := False;
end;

function TrmDummyDsgnComponent.IsVirtualOwner: Boolean;
begin
  Result := True;
end;

procedure TrmDummyDsgnComponent.Notify(AEvent: TrmDesignEventType);
begin
end;

function TrmDummyDsgnComponent.ObjectAtPos(APos: TPoint): IrmDesignObject;
begin
  Result := nil;
end;

function TrmDummyDsgnComponent.ObjectInheritsFrom(const AClassName: String): Boolean;
begin
  Result := SameText(AClassName, 'TrmReport');
end;

function TrmDummyDsgnComponent.ObjectType: TrwDsgnObjectType;
begin
  Result := rwDOTNotVisual; 
end;

function TrmDummyDsgnComponent.PropertiesByTypeName(const ATypeName: String): TrwStrList;
begin
  SetLength(Result, 0);
end;

function TrmDummyDsgnComponent.PropertyExists(const APropertyName: String): Boolean;
begin
  Result := False;
end;

function TrmDummyDsgnComponent.RealObject: TComponent;
begin
  Result := Self;
end;

procedure TrmDummyDsgnComponent.Refresh;
begin
end;

procedure TrmDummyDsgnComponent.RemoveChildObject(AObject: IrmDesignObject);
begin
end;

function TrmDummyDsgnComponent.ScreenToClient(APos: TPoint): TPoint;
begin
  Result := Point(0, 0);
end;

procedure TrmDummyDsgnComponent.SetBoundsRect(ARect: TRect);
begin
end;

procedure TrmDummyDsgnComponent.SetDesigner(ADesigner: IrmDesigner);
begin
end;

procedure TrmDummyDsgnComponent.SetHeight(AValue: Integer);
begin
end;

procedure TrmDummyDsgnComponent.SetLeft(AValue: Integer);
begin
end;

procedure TrmDummyDsgnComponent.SetObjectIndex(const AIndex: Integer);
begin
end;

procedure TrmDummyDsgnComponent.SetParent(AParent: IrmDesignObject);
begin
end;

procedure TrmDummyDsgnComponent.SetPropertyValue(const APropertyName: String; Value: Variant);
begin
end;

procedure TrmDummyDsgnComponent.SetTop(AValue: Integer);
begin
end;

procedure TrmDummyDsgnComponent.SetWidth(AValue: Integer);
begin
end;

function TrmDummyDsgnComponent.VCLObject: TObject;
begin
  Result := Self;
end;

procedure TrmDummyDsgnComponent.WriteComp(Writer: TWriter);
begin
end;

{ TrwdLibVarFunctHolder }

function TrwdLibVarFunctHolder.GetLocalComponents: IrmComponents;
begin
  Result := SystemLibComponents;
end;

function TrwdLibVarFunctHolder.GetLocalFunctions: IrmFunctions;
begin
  Result := SystemLibFunctions;
end;

function TrwdLibVarFunctHolder.GetVariables: IrmVariables;
begin
  Result := nil; // future logic for global constants
end;

function TrwdLibVarFunctHolder.ObjectType: TrwDsgnObjectType;
begin
  Result := rwDOTInternal;
end;


{ TrmSystemLibraryHolder }

procedure TrmSystemLibraryHolder.AfterConstruction;
var
  C: TrwdLibVarFunctHolder;
begin
  inherited;

  Name := 'SystemLibrary';
  C := TrwdLibVarFunctHolder.Create(Self);
  C.Name := VAR_FUNCT_COMP_NAME;
end;

procedure TrmSystemLibraryHolder.CompileLibrary;
var
  h: String;
begin
  SystemLibFunctions.Compile;

  try
    SystemLibComponents.Compile;
  except
    on E: ErwParserError do
    begin
      if not SameText(E.ComponentName, cParserLibComp) then
        if SameText(E.ComponentName, VAR_FUNCT_COMP_NAME) then
          h := E.ComponentName
        else
          h := cParserLibComp + '.' + E.ComponentName
      else
        h := cParserLibComp;
//      ShowCompError(E.LibComponentName, h, E.EventName, E.LineNum, E.ColNum, E.Message);

{      i := LibComponents.IndexOf(E.LibComponentName);
      LI := LibComponents[i];
      N := NodeByLibItem(LI);
      if Assigned(N) then
        N.Selected := True;
}
      raise;
    end

    else
      raise;
  end;


  ShowMessage('Library has been compiled successfully');
end;

function TrmSystemLibraryHolder.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.Selectable := True;
end;

function TrmSystemLibraryHolder.GetObjectClassDescription: String;
begin
  Result := 'System Library';
end;

procedure TrmSystemLibraryHolder.LoadFromFile(const AFileName: String);
var
  F: TEvFileStream;
begin
  F := TEvFileStream.Create(AFileName, fmOpenRead);
  try
    SystemLibFunctions.LoadFromStream(F);
    SystemLibComponents.LoadFromStream(F);
  finally
    F.Free;
  end;
end;

procedure TrmSystemLibraryHolder.ReloadLibrary;
begin
  SystemLibComponents.Initialize;
  SystemLibFunctions.Initialize;

  FFunctsModified := False;
  FCompsModified := False;
end;

procedure TrmSystemLibraryHolder.SaveToFile(const AFileName: String);
var
  F: TEvFileStream;
begin
  F := TEvFileStream.Create(AFileName, fmCreate);
  try
    SystemLibFunctions.StoreDebInfo := False;
    SystemLibFunctions.SaveToStream(F);
    SystemLibComponents.StoreDebInfo := False;
    SystemLibComponents.SaveToStream(F);
  finally
    F.Free;
  end;
end;

procedure TrmSystemLibraryHolder.StoreLibrary;
begin
  if FFunctsModified then
  begin
    SystemLibFunctions.Store;
    FFunctsModified := False;
  end;

  if FCompsModified then
  begin
    SystemLibComponents.Store;
    FCompsModified := False;
  end;
end;

end.
