inherited rmDsgnComponents: TrmDsgnComponents
  Width = 733
  Height = 630
  object Splitter1: TSplitter [0]
    Left = 313
    Top = 0
    Height = 630
    Color = clBtnFace
    ParentColor = False
  end
  object pnlComponents: TPanel [1]
    Left = 316
    Top = 0
    Width = 417
    Height = 630
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 2
    object Label6: TLabel
      Left = 10
      Top = 10
      Width = 70
      Height = 13
      Caption = 'Components'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object pnlGroup: TPanel [2]
    Left = 316
    Top = 0
    Width = 417
    Height = 630
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 3
    object Label7: TLabel
      Left = 10
      Top = 10
      Width = 123
      Height = 13
      Caption = 'Group of Components'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 20
      Top = 50
      Width = 60
      Height = 13
      Caption = 'Group Name'
    end
    object btnDeleteGroup: TButton
      Left = 174
      Top = 116
      Width = 132
      Height = 23
      Caption = 'Delete Group'
      TabOrder = 0
      OnClick = miDeleteLibGroupClick
    end
    object edGroupName: TEdit
      Left = 114
      Top = 46
      Width = 233
      Height = 21
      TabOrder = 1
      OnExit = edGroupNameExit
      OnKeyPress = edClassNameKeyPress
    end
    object btnAddGroup: TButton
      Left = 20
      Top = 116
      Width = 132
      Height = 23
      Caption = 'Add Child Group'
      TabOrder = 2
      OnClick = miAddLibGroupClick
    end
  end
  object pnlComponent: TPanel [3]
    Left = 316
    Top = 0
    Width = 417
    Height = 630
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 10
      Top = 10
      Width = 132
      Height = 13
      Caption = 'Component Description'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lClassNameTxt: TLabel
      Left = 20
      Top = 50
      Width = 56
      Height = 13
      Caption = 'Class Name'
    end
    object Label2: TLabel
      Left = 20
      Top = 117
      Width = 77
      Height = 13
      Caption = 'Brief Description'
    end
    object Label3: TLabel
      Left = 21
      Top = 146
      Width = 53
      Height = 13
      Caption = 'Description'
    end
    object Label5: TLabel
      Left = 20
      Top = 82
      Width = 70
      Height = 13
      Caption = 'Ancestor Class'
    end
    object edClassName: TEdit
      Left = 114
      Top = 46
      Width = 233
      Height = 21
      TabOrder = 0
      OnExit = edClassNameExit
      OnKeyPress = edClassNameKeyPress
    end
    object memDescription: TMemo
      Left = 20
      Top = 166
      Width = 328
      Height = 116
      ScrollBars = ssVertical
      TabOrder = 1
      OnExit = memDescriptionExit
    end
    object edDisplayName: TEdit
      Left = 114
      Top = 113
      Width = 233
      Height = 21
      TabOrder = 2
      OnExit = edDisplayNameExit
      OnKeyPress = edClassNameKeyPress
    end
    object gbPalette: TGroupBox
      Left = 20
      Top = 293
      Width = 328
      Height = 135
      Caption = 'Designer Settings'
      TabOrder = 3
      object Label4: TLabel
        Left = 254
        Top = 23
        Width = 59
        Height = 13
        Caption = 'Icon (32x32)'
      end
      object chbShowOnPalette: TCheckBox
        Left = 20
        Top = 23
        Width = 158
        Height = 17
        Caption = 'Show on Component Palette'
        TabOrder = 0
        OnClick = chbShowOnPaletteClick
      end
      object chbAdvancedMode: TCheckBox
        Left = 20
        Top = 73
        Width = 139
        Height = 17
        Caption = 'Advanced Mode Only'
        TabOrder = 1
        OnClick = chbShowOnPaletteClick
      end
      object pnlCompPictureEdit: TPanel
        Left = 279
        Top = 46
        Width = 34
        Height = 34
        BevelOuter = bvLowered
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object imgCompPictEdit: TImage
          Left = 1
          Top = 1
          Width = 32
          Height = 32
          Hint = 'Component'#39's Icon'
          Align = alClient
          Center = True
          PopupMenu = pmPict
          Proportional = True
          Transparent = True
        end
      end
      object btnLoadIcon: TButton
        Left = 160
        Top = 100
        Width = 70
        Height = 23
        Caption = 'Load...'
        TabOrder = 3
        OnClick = miLoadPictClick
      end
      object btnSaveIcon: TButton
        Left = 243
        Top = 100
        Width = 70
        Height = 23
        Caption = 'Save...'
        TabOrder = 4
        OnClick = miSavePictClick
      end
      object chbComponentEditMode: TCheckBox
        Left = 20
        Top = 48
        Width = 150
        Height = 17
        Caption = 'Component Editor Only'
        TabOrder = 5
        OnClick = chbShowOnPaletteClick
      end
    end
    object btnEditComp: TButton
      Left = 20
      Top = 458
      Width = 132
      Height = 23
      Caption = 'Edit Component...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btnEditCompClick
    end
    object btnDeleteComp: TButton
      Left = 170
      Top = 458
      Width = 132
      Height = 23
      Caption = 'Delete Component'
      TabOrder = 5
      OnClick = miDeleteComponentClick
    end
    object edAncestor: TEdit
      Left = 114
      Top = 78
      Width = 233
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 6
    end
  end
  object tvObjects: TTreeView [4]
    Left = 0
    Top = 0
    Width = 313
    Height = 630
    Align = alLeft
    DragMode = dmAutomatic
    HideSelection = False
    Images = ImageList
    Indent = 19
    PopupMenu = pmLibTree
    ReadOnly = True
    SortType = stData
    TabOrder = 0
    OnChange = tvObjectsChange
    OnChanging = tvObjectsChanging
    OnCompare = tvObjectsCompare
    OnDragOver = tvObjectsDragOver
    OnEndDrag = tvObjectsEndDrag
  end
  object ImageList: TImageList
    Left = 128
    Top = 72
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000042
      4200004242000042420000424200004242000042420000424200004242000042
      4200004242000042420000424200004242000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000A5A50000A5
      A50000A5A50000A5A50000A5A50000A5A50000A5A50000A5A50000A5A50000A5
      A50000A5A50000A5A50000424200004242000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000A5A5000000
      000084C6E70084E7E70084C6E70084E7E70084C6E70084C6E70084C6E70084C6
      E70084C6C60000A5A50000424200004242000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000A5A5000000000084E7
      E70084E7E70084C6E70084E7E70084C6E70084E7E70084C6E70084C6E70084C6
      E70084C6C6000042420000A5A500004242000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008484840000000000848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000A5A5000000000084E7
      E70084E7E70084E7E70084E7E70084E7E70084C6E70084E7E70084C6E70084C6
      E70000A5A5000042420000A5A500004242000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      000000FF000000FF000000840000000000008484840000FF000000FF000000FF
      000000FF00008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000A5A5000000000084E7E70084E7
      E70084E7E70084E7E70084C6E70084E7E70084E7E70084C6E70084E7E70084C6
      C60000424200F7CEA50000000000004242000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00848484000000000000000000000000000000000000FF
      000000FF000000FF00000084000000000000848484000084000000FF000000FF
      000000FF00008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000A5A5000000000084E7E70084E7
      E70084E7E70084E7E70084E7E70084E7E70084C6E70084E7E70084C6E70084C6
      C60000424200F7CEA500F7CEA500000000000000000084848400FFFFFF00FF00
      0000FF000000FF000000FF000000FF000000FFFFFF00FF000000FF000000FF00
      0000FF000000FFFFFF008484840000000000000000008484840000FF000000FF
      000000FF0000848484000084000000000000000000000000000000FF000000FF
      000000FF00008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000A5A50000A5A50000A5A50000A5
      A50000A5A50000A5A50000A5A50000A5A50000A5A50000A5A50000A5A50000A5
      A500F7CEA50000000000F7CEA500000000000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00848484000000000000000000000000008484840000FF
      0000848484008484840000840000008400000084000000FF000000FF000000FF
      000000FF00008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000848400846363000000
      0000F7CEA50084004200F7CEA500F7CEA50084004200F7CEA500F7CEA500F7CE
      A500F7CEA500F7CEA50000000000004242000000000084848400FFFFFF00FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFFF
      FF00FFFFFF00FFFFFF0084848400000000000000000000000000000000008484
      84000000000084848400008400000084000000FF000000FF000000FF00000000
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000084840084C6C6008463
      63000000000000000000C6424200F7CEA50000000000C6424200000000000000
      0000F7CEA5000000000084C6C600004242000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00848484008484840084848400000000000000000000000000000000000000
      000000000000848484000084000000FF000000FF000000FF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000084840084C6C60084C6
      C600846363000000000000000000C6630000F7CEA5000000000000000000F7CE
      A5000000000000A5A50000A5A500000000000000000084848400FFFFFF00FF00
      0000FF000000FF000000FFFFFF00FF000000FF000000FFFFFF00FFFFFF00FFFF
      FF0084848400FFFFFF0084848400000000000000000000000000000000000000
      0000000000008484840000FF000000FF000000FF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000084840084C6
      C60084C6C600846363000000000000000000C6A5420000000000F7CEA5000000
      0000000000000000000000000000000000000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00848484008484840000000000000000000000000000000000000000000000
      000000000000000000008484840000FF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      84000084840000848400846363000000000000000000F7CEA500000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000846363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008463630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000E000FFFFFFFF0000
      C000FFFFFFFF0000D000FFFFF8FF0000A000FFFFF0430000A0008001E0030000
      40008001C0030000400080018003000000048001C003000090008001E8170000
      8CB08001F83F000086618001F87F0000C34F8003FCFF0000E19F8007FFFF0000
      FEBFFFFFFFFF0000FF7FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object pmLibTree: TPopupMenu
    Left = 130
    Top = 120
    object miNewComp: TMenuItem
      Caption = 'New Component'
      object miInputFormControl: TMenuItem
        Caption = 'Input Form'
        object InputFormAncestor1: TMenuItem
          Caption = 'Input Form Ancestor'
          Hint = 'TrwInputFormContainer'
          OnClick = miReportAncestorClick
        end
        object ext1: TMenuItem
          Caption = 'Text'
          Hint = 'TrwText'
          OnClick = miReportAncestorClick
        end
        object Panel2: TMenuItem
          Caption = 'Panel'
          Hint = 'TrwPanel'
          OnClick = miReportAncestorClick
        end
        object EditBox1: TMenuItem
          Caption = 'Edit Box'
          Hint = 'TrwEdit'
          OnClick = miReportAncestorClick
        end
        object DateTimeEdit1: TMenuItem
          Caption = 'Date Edit'
          Hint = 'TrwDateEdit'
          OnClick = miReportAncestorClick
        end
        object CheckBox1: TMenuItem
          Caption = 'Check Box'
          Hint = 'TrwCheckBox'
          OnClick = miReportAncestorClick
        end
        object GroupBox1: TMenuItem
          Caption = 'Group Box'
          Hint = 'TrwGroupBox'
          OnClick = miReportAncestorClick
        end
        object RadioGroup1: TMenuItem
          Caption = 'Radio Group'
          Hint = 'TrwRadioGroup'
          OnClick = miReportAncestorClick
        end
        object Combobox1: TMenuItem
          Caption = 'Combo Box'
          Hint = 'TrwComboBox'
          OnClick = miReportAncestorClick
        end
        object DBComboBox1: TMenuItem
          Caption = 'DB Combo Box'
          Hint = 'TrwDBComboBox'
          OnClick = miReportAncestorClick
        end
        object DBGrid1: TMenuItem
          Caption = 'DB Grid'
          Hint = 'TrwDBGrid'
          OnClick = miReportAncestorClick
        end
        object PageControl1: TMenuItem
          Caption = 'Page Control'
          Hint = 'TrwPageControl'
          OnClick = miReportAncestorClick
        end
        object MenuItem1: TMenuItem
          Caption = 'Button'
          Hint = 'TrwButton'
          OnClick = miReportAncestorClick
        end
      end
      object miPrintFormControl: TMenuItem
        Caption = 'Print Form'
        object PrintForm1: TMenuItem
          Caption = 'Print Form Ancestor'
          Hint = 'TrmPrintForm'
          OnClick = miReportAncestorClick
        end
        object PrintText1: TMenuItem
          Caption = 'Text'
          Hint = 'TrmPrintText'
          OnClick = miReportAncestorClick
        end
        object Panel1: TMenuItem
          Caption = 'Panel'
          Hint = 'TrmPrintPanel'
          OnClick = miReportAncestorClick
        end
        object Image1: TMenuItem
          Caption = 'Picture'
          Hint = 'TrmPrintImage'
          OnClick = miReportAncestorClick
        end
        object Shape1: TMenuItem
          Caption = 'Shape'
          Visible = False
          OnClick = miReportAncestorClick
        end
        object Table1: TMenuItem
          Caption = 'Table'
          Hint = 'TrmTable'
          OnClick = miReportAncestorClick
        end
        object TableCell1: TMenuItem
          Caption = 'Table Cell'
          Hint = 'TrmTableCell'
          OnClick = miReportAncestorClick
        end
        object DBTable1: TMenuItem
          Caption = 'DB Table'
          Hint = 'TrmDBTable'
          OnClick = miReportAncestorClick
        end
      end
      object miASCIIFormControl: TMenuItem
        Caption = 'ASCII Form'
        object miASCIIFormAncestor: TMenuItem
          Caption = 'ASCII Form Ancestor'
          Hint = 'TrmASCIIForm'
          OnClick = miReportAncestorClick
        end
        object miASCIIRecord: TMenuItem
          Caption = 'Record'
          Hint = 'TrmASCIIRecord'
          OnClick = miReportAncestorClick
        end
        object miASCIIField: TMenuItem
          Caption = 'Field'
          Hint = 'TrmASCIIField'
          OnClick = miReportAncestorClick
        end
      end
      object XMLForm1: TMenuItem
        Caption = 'XML Form'
        object miXMLFormAncestor: TMenuItem
          Caption = 'XML Form Ancestor'
          Hint = 'TrmXMLForm'
          OnClick = miReportAncestorClick
        end
        object miXMLSimpleTypes: TMenuItem
          Caption = 'Simple Types'
          object miXMLSimpleType: TMenuItem
            Caption = 'by Restriction'
            Hint = 'TrmXMLSimpleTypeByRestriction'
            OnClick = miReportAncestorClick
          end
          object miXMLSimpleTypeList: TMenuItem
            Caption = 'by List'
            Hint = 'TrmXMLSimpleTypeByList'
            OnClick = miReportAncestorClick
          end
          object miXMLSimpleTypeUnion: TMenuItem
            Caption = 'by Union'
            Hint = 'TrmXMLSimpleTypeByUnion'
            OnClick = miReportAncestorClick
          end
        end
        object miXMLAttribute: TMenuItem
          Caption = 'Attributes'
          Hint = 'TrmXMLAttribute'
          OnClick = miReportAncestorClick
        end
        object miXMLAttributeGroup: TMenuItem
          Caption = 'Group of Attributes'
          Hint = 'TrmXMLAttributeGroup'
          OnClick = miReportAncestorClick
        end
        object miXMLElement: TMenuItem
          Caption = 'Element'
          Hint = 'TrmXMLElement'
          OnClick = miReportAncestorClick
        end
        object miXMLGroup: TMenuItem
          Caption = 'Group'
          Hint = 'TrmXMLGroup'
          OnClick = miReportAncestorClick
        end
      end
      object miNonVisualComp: TMenuItem
        Caption = 'Misc'
        object miReportAncestor: TMenuItem
          Caption = 'Report Ancestor'
          Hint = 'TrmReport'
          OnClick = miReportAncestorClick
        end
        object Query2: TMenuItem
          Caption = 'Query'
          Hint = 'TrwQuery'
          OnClick = miReportAncestorClick
        end
        object Buffer1: TMenuItem
          Caption = 'Buffer'
          Hint = 'TrwBuffer'
          OnClick = miReportAncestorClick
        end
      end
      object InheritFromLibrary1: TMenuItem
        Caption = 'Library Inheritance...'
        Hint = 'LIBRARY'
        OnClick = miReportAncestorClick
      end
    end
    object miDeleteComponent: TMenuItem
      Caption = 'Delete Component'
      OnClick = miDeleteComponentClick
    end
    object miSplitter1: TMenuItem
      Caption = '-'
    end
    object miAddLibGroup: TMenuItem
      Caption = 'Add Group'
      OnClick = miAddLibGroupClick
    end
    object miDeleteLibGroup: TMenuItem
      Caption = 'Delete Group'
      OnClick = miDeleteLibGroupClick
    end
  end
  object opdPicture: TOpenPictureDialog
    Filter = 'All (*.bmp)|*.bmp|Bitmaps (*.bmp)|*.bmp'
    Title = 'Component Picture'
    Left = 505
    Top = 342
  end
  object svdPicture: TSavePictureDialog
    Filter = 'All (*.bmp)|*.bmp|Bitmaps (*.bmp)|*.bmp'
    Title = 'Component Picture'
    Left = 536
    Top = 342
  end
  object pmPict: TPopupMenu
    Left = 568
    Top = 344
    object miLoadPict: TMenuItem
      Caption = 'Load From File...'
      OnClick = miLoadPictClick
    end
    object miSavePict: TMenuItem
      Caption = 'Save To File...'
      OnClick = miSavePictClick
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object miClearPict: TMenuItem
      Caption = 'Clear Picture'
      OnClick = miClearPictClick
    end
  end
end
