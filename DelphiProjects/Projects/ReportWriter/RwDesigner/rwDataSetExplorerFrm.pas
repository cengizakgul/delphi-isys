unit rwDataSetExplorerFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, ExtCtrls, StdCtrls, Db,
  rwDB, rwDebugInfo, rwVirtualMachine, sbAPI, ActnList,
  rwDesignClasses, Wwdatsrc, Wwdbigrd, Wwdbgrid,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses;

type

  TrwDataSetExplorer = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    edDataSet: TEdit;
    dsExpl: TwwDataSource;
    aclDesignerActions: TActionList;
    actStepOver: TAction;
    actTraceInto: TAction;
    actRun: TAction;
    actTerminate: TAction;
    actPCode: TAction;
    grData: TisRWDBGrid;
    Panel2: TPanel;
    Label3: TLabel;
    edRecCount: TEdit;
    Label2: TLabel;
    edRecNo: TEdit;
    chbBOF: TCheckBox;
    chbEOF: TCheckBox;
    DS: TISRWClientDataSet;
    actClose: TAction;
    procedure FormCreate(Sender: TObject);
    procedure edDataSetExit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edDataSetKeyPress(Sender: TObject; var Key: Char);
    procedure actStepOverExecute(Sender: TObject);
    procedure actTraceIntoExecute(Sender: TObject);
    procedure actRunExecute(Sender: TObject);
    procedure actTerminateExecute(Sender: TObject);
    procedure actPCodeExecute(Sender: TObject);
    procedure actCloseExecute(Sender: TObject);
  private
    FWatch: PTrwWatch;
    FDataSet: TrwDataSet;

    procedure FOnChangeDataSet(Sender: TObject);
    procedure FAfterPost(Sender: TObject);
    procedure SyncContent;
    procedure SyncControls;

  public
    procedure Initialize;
    procedure Synchronize;
  end;


  function  ShowDSExplorer(ADSExpression: string = ''): TrwDataSetExplorer;
  procedure CloseDSExplorer;
  function  IsDSExplorerVisible: Boolean;

implementation

{$R *.DFM}

uses rwDesignerFrm, rwEvaluateFrm, rwWatchListFrm;

var
  FDataSetExplorer: TrwDataSetExplorer;


function ShowDSExplorer(ADSExpression: string): TrwDataSetExplorer;
begin
  if not Assigned(FDataSetExplorer) then
  begin
    FDataSetExplorer := TrwDataSetExplorer.Create(nil);
    DebugInfo.WatchList.NeedCompileWatches := True;
    if Length(ADSExpression) = 0 then
      FDataSetExplorer.Initialize;
    FDataSetExplorer.Show;
  end
  else
  begin
    if not IsDSExplorerVisible then
      FDataSetExplorer.Show;
    SetWindowPos(FDataSetExplorer.Handle, HWND_TOP, 0, 0, 0, 0,
      SWP_NOACTIVATE or SWP_NOSIZE or SWP_NOMOVE);
    FDataSetExplorer.FOnChangeDataSet(nil);
  end;

  if Length(ADSExpression) <> 0 then
  begin
    FDataSetExplorer.edDataSet.Text := ADSExpression;
    FDataSetExplorer.edDataSet.OnExit(nil);
  end;

  Result := FDataSetExplorer;
end;


procedure CloseDSExplorer;
begin
  if Assigned(FDataSetExplorer) then
  begin
    FDataSetExplorer.Free;
    FDataSetExplorer := nil;
  end;
end;


function IsDSExplorerVisible: Boolean;
begin
  Result := Assigned(FDataSetExplorer) and FDataSetExplorer.Visible;
end;


{ TrwDataSetExplorer }


procedure TrwDataSetExplorer.Initialize;
begin
  if DebugInfo.WatchList.NeedCompileWatches then
    CompileWatches;

  VM.CalcWatches;
  Synchronize;
end;



procedure TrwDataSetExplorer.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  FDataSet := nil;
  FWatch := nil;

  for i := 0 to DebugInfo.WatchList.Count - 1 do
    if DebugInfo.WatchList[i].DataSet then
    begin
      FWatch := DebugInfo.WatchList[i];
      edDataSet.Text := FWatch.Expression;
      break;
    end;
    
  SyncControls;
end;

procedure TrwDataSetExplorer.edDataSetExit(Sender: TObject);
begin
  if not Assigned(FWatch) then
    FWatch := DebugInfo.WatchList[DebugInfo.WatchList.Add(edDataSet.Text)]
  else
  begin
    FWatch.Expression := Trim(edDataSet.Text);
    DebugInfo.WatchList.NeedCompileWatches := True;
  end;

  if Length(FWatch.Expression) = 0 then
  begin
    DebugInfo.WatchList.Delete(FWatch);
    FWatch := nil;
    if Assigned(FDataSet) then
    begin
       FDataSet.OnDataChange := nil;
       FDataSet.AfterPost := nil;
       FDataSet := nil;
    end;
    DS.Close;
    SyncControls;
  end

  else
  begin
    FWatch.DataSet := True;
    Initialize;
  end;
end;


procedure TrwDataSetExplorer.Synchronize;
var
  P: Integer;
begin
  if Assigned(FWatch) then
  begin
    try
      if FWatch.Error then
      begin
        FDataSet := nil;
        DS.Close;
        SyncControls;
      end

      else
      begin
        P := StrToInt(FWatch.Result);
        if P <> Integer(Pointer(FDataSet)) then
        begin
          if Assigned(FDataSet) then
          begin
             FDataSet.OnDataChange := nil;
             FDataSet.AfterPost := nil;
             FDataSet := nil;
          end;

          FDataSet := TrwDataSet(Pointer(P));
          FDataSet.OnDataChange := FOnChangeDataSet;
          FDataSet.AfterPost := FAfterPost;
          FDataSet.OnChangeFilter := FAfterPost;
          FDataSet.OnChangeSort := FAfterPost;
          SyncContent;
        end;
      end;

    except
      DS.Close;
      SyncControls;
      Exit;
    end;
  end;
end;


procedure TrwDataSetExplorer.FOnChangeDataSet(Sender: TObject);
begin
  if IsDSExplorerVisible then
    if not DS.Active or not Assigned(FDataSet) or not Boolean(FDataSet.PActive) or
       (FDataSet.PRecordCount <> DS.RecordCount) then
      SyncContent
    else
      SyncControls;
end;

procedure TrwDataSetExplorer.FormDestroy(Sender: TObject);
begin
  if Assigned(FDataSet) then
  begin
     FDataSet.AfterPost := nil;
     FDataSet.OnDataChange := nil;
     FDataSet := nil;
  end;
end;


procedure TrwDataSetExplorer.edDataSetKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    edDataSet.OnExit(nil);
end;

procedure TrwDataSetExplorer.actStepOverExecute(Sender: TObject);
begin
  ReportDesigner.actStepOver.Execute;
end;

procedure TrwDataSetExplorer.actTraceIntoExecute(Sender: TObject);
begin
  ReportDesigner.actTraceInto.Execute;
end;

procedure TrwDataSetExplorer.actRunExecute(Sender: TObject);
begin
  ReportDesigner.actRun.Execute;
end;

procedure TrwDataSetExplorer.actTerminateExecute(Sender: TObject);
begin
  ReportDesigner.actTerminate.Execute;
end;

procedure TrwDataSetExplorer.actPCodeExecute(Sender: TObject);
begin
  ReportDesigner.actPCode.Execute;
end;

procedure TrwDataSetExplorer.SyncContent;
var
  i: Integer;
begin
  DS.DisableControls;
  try
    grData.DataSource := nil;

    if not Assigned(FDataSet) or not Boolean(FDataSet.PActive) then
    begin
      DS.Close;
      Exit;
    end;  

    if FDataSet.DataSet.State in [sbcInactive, sbcBrowse] then
      DS.Close;

    if FDataSet.DataSet.State = sbcBrowse then
    begin
      sbSBCursorToClientDataSet(FDataSet.DataSet, DS);
      DS.Open;
      for i := 0 to DS.Fields.Count - 1 do
      begin
        if DS.Fields[i] is TFloatField then
          TFloatField(DS.Fields[i]).DisplayFormat := '';
      end;
    end;

  finally
    DS.EnableControls;
    SyncControls;
    grData.DataSource := dsExpl;
  end;
end;


procedure TrwDataSetExplorer.FAfterPost(Sender: TObject);
begin
  if IsDSExplorerVisible then
    SyncContent;
end;


procedure TrwDataSetExplorer.SyncControls;
begin
  if Assigned(FDataSet) and DS.Active then
  begin
    if FDataSet.PRecordNumber > 0 then
      DS.RecNo := FDataSet.PRecordNumber;
    edRecCount.Text := IntToStr(FDataSet.PRecordCount);
    edRecNo.Text := IntToStr(FDataSet.PRecordNumber);
    chbBOF.Checked := FDataSet.PBOF;
    chbEOF.Checked := FDataSet.PEOF;
  end

  else
  begin
    edRecCount.Text := '';
    edRecNo.Text := '';
    chbBOF.State := cbGrayed;
    chbEOF.State := cbGrayed;
  end
end;

procedure TrwDataSetExplorer.actCloseExecute(Sender: TObject);
begin
  Close;
end;

initialization
  FDataSetExplorer := nil;

finalization
  CloseDSExplorer;


end.
