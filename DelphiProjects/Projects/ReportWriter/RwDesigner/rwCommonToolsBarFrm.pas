unit rwCommonToolsBarFrm;

interface

uses     
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, ExtCtrls, ImgList, StdCtrls, rwBasicClasses,
  rwDsgnRes, rwUtils, rwBasicToolsBarFrm, Variants, Menus;

type

  {TrwCommonToolsBar is tool bar with common tools}

  TrwOnChangesTextToolBar = procedure(Sender: TObject; AText: string) of object;

  TrwOnClickExpertToolBar = procedure(Sender: TObject; AAction: TrwExpertToolBarEvent; AParam: TObject = nil) of object;


  TrwCommonToolsBar = class(TrwBasicToolsBar)
    tlbText: TToolBar;
    edtText: TEdit;
    tlbExperts: TToolBar;
    btnQB: TToolButton;
    tbtLibrary: TToolButton;
    tbtVarFunc: TToolButton;
    tbtEvents: TToolButton;
    tlbActions: TToolBar;
    tbtOpen: TToolButton;
    tbtSave: TToolButton;
    tbtCompile: TToolButton;
    tbtStepOver: TToolButton;
    tbtBrkPoint: TToolButton;
    ToolButton20: TToolButton;
    tbtTraceInto: TToolButton;
    tbtRun: TToolButton;
    tbtPause: TToolButton;
    tbtTerminate: TToolButton;
    ToolButton16: TToolButton;
    tbtCut: TToolButton;
    tbtCopy: TToolButton;
    tbtPaste: TToolButton;
    tlbQuery: TToolButton;
    tlbBuffer: TToolButton;
    tlbCustomObject: TToolButton;
    procedure edtTextChange(Sender: TObject);
    procedure btnQBClick(Sender: TObject);
    procedure tbtLibraryClick(Sender: TObject);
    procedure tbtVarFuncClick(Sender: TObject);
    procedure tbtEventsClick(Sender: TObject);

  private
    FOnChangesTextToolBar: TrwOnChangesTextToolBar;
    FOnClickExpertToolBar: TrwOnClickExpertToolBar;

  public
    procedure SetToolBarByComponentProperties(AComponent: TrwComponent; AUnion: Boolean); override;

    property OnChangesTextToolBar: TrwOnChangesTextToolBar read FOnChangesTextToolBar write FOnChangesTextToolBar;
    property OnClickExpertToolBar: TrwOnClickExpertToolBar read FOnClickExpertToolBar write FOnClickExpertToolBar;
  end;

implementation


{$R *.DFM}


procedure TrwCommonToolsBar.edtTextChange(Sender: TObject);
begin
  if not FUpdating and Assigned(FOnChangesTextToolBar) then
    FOnChangesTextToolBar(Self, edtText.Text);
end;

procedure TrwCommonToolsBar.btnQBClick(Sender: TObject);
begin
  if Assigned(FOnClickExpertToolBar) then
    FOnClickExpertToolBar(Self, eteQueryBuilder);
end;

procedure TrwCommonToolsBar.tbtLibraryClick(Sender: TObject);
begin
  if Assigned(FOnClickExpertToolBar) then
    FOnClickExpertToolBar(Self, eteLibrary);
end;

procedure TrwCommonToolsBar.tbtVarFuncClick(Sender: TObject);
begin
  if Assigned(FOnClickExpertToolBar) then
    FOnClickExpertToolBar(Self, eteLocalVarFunc);
end;

procedure TrwCommonToolsBar.tbtEventsClick(Sender: TObject);
begin
  if Assigned(FOnClickExpertToolBar) then
    FOnClickExpertToolBar(Self, eteAllEvents);
end;



procedure TrwCommonToolsBar.SetToolBarByComponentProperties(AComponent: TrwComponent; AUnion: Boolean);
var
  Value: Variant;
begin
  inherited;

  FUpdating := True;
  
  if not AUnion then
  begin
    Value := GetComponentProperty(AComponent, ['Text']);
    if (Value <> Null) then
      edtText.Text := Value
    else
      edtText.Text := '';
  end

  else
    edtText.Text := '';

  FUpdating := False;
end;

end.
