object rwDataSetExplorer: TrwDataSetExplorer
  Left = 325
  Top = 230
  Width = 778
  Height = 355
  BorderIcons = [biSystemMenu]
  Caption = 'DataSet Explorer'
  Color = clBtnFace
  Constraints.MinHeight = 110
  Constraints.MinWidth = 760
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 770
    Height = 30
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 4
      Top = 7
      Width = 111
      Height = 13
      Caption = 'DataSet expression'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edDataSet: TEdit
      Left = 119
      Top = 4
      Width = 242
      Height = 21
      TabOrder = 0
      OnExit = edDataSetExit
      OnKeyPress = edDataSetKeyPress
    end
    object Panel2: TPanel
      Left = 398
      Top = 0
      Width = 340
      Height = 29
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object Label3: TLabel
        Left = 7
        Top = 7
        Width = 51
        Height = 13
        Caption = 'Rec Count'
      end
      object Label2: TLabel
        Left = 114
        Top = 7
        Width = 60
        Height = 13
        Caption = 'Rec Number'
      end
      object edRecCount: TEdit
        Left = 62
        Top = 4
        Width = 38
        Height = 21
        TabOrder = 0
      end
      object edRecNo: TEdit
        Left = 177
        Top = 4
        Width = 38
        Height = 21
        TabOrder = 1
      end
      object chbBOF: TCheckBox
        Left = 237
        Top = 7
        Width = 44
        Height = 17
        Caption = 'BOF'
        TabOrder = 2
      end
      object chbEOF: TCheckBox
        Left = 293
        Top = 7
        Width = 44
        Height = 17
        Caption = 'EOF'
        TabOrder = 3
      end
    end
  end
  object grData: TisRWDBGrid
    Left = 0
    Top = 30
    Width = 770
    Height = 297
    IniAttributes.Enabled = True
    IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
    IniAttributes.SectionName = 'TrwDataSetExplorer\grData'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = dsExpl
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    Sorting = False
    RecordCountHintEnabled = False
  end
  object dsExpl: TwwDataSource
    AutoEdit = False
    DataSet = DS
    Left = 160
    Top = 112
  end
  object aclDesignerActions: TActionList
    Images = DsgnRes.ilButtons
    Left = 240
    Top = 78
    object actStepOver: TAction
      Caption = 'actStepOver'
      ImageIndex = 66
      ShortCut = 119
      OnExecute = actStepOverExecute
    end
    object actTraceInto: TAction
      Caption = 'actTraceInto'
      ImageIndex = 67
      ShortCut = 118
      OnExecute = actTraceIntoExecute
    end
    object actRun: TAction
      Caption = 'actRun'
      ImageIndex = 69
      ShortCut = 120
      OnExecute = actRunExecute
    end
    object actTerminate: TAction
      Caption = 'actTerminate'
      ImageIndex = 72
      ShortCut = 16497
      OnExecute = actTerminateExecute
    end
    object actPCode: TAction
      Caption = 'actPCode'
      ShortCut = 49219
      OnExecute = actPCodeExecute
    end
    object actClose: TAction
      Caption = 'Close'
      ShortCut = 27
      OnExecute = actCloseExecute
    end
  end
  object DS: TisRWClientDataSet
    Left = 128
    Top = 112
  end
end
