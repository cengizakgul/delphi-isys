unit rmOPEBorderLinesFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls, StdCtrls, rmObjPropertyEditorFrm, rwTypes,
  rwReportControls, rwGraphics, rwUtils;

type
  TrmOPEBorderLines = class(TrmObjPropertyEditor)
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Bevel1: TBevel;
    Label6: TLabel;
    cbColor: TColorBox;
    cbStyle: TComboBox;
    cbWidth: TComboBox;
    pnCellExample: TPanel;
    pbCellExample: TPaintBox;
    sbTopLine: TSpeedButton;
    sbBottomLine: TSpeedButton;
    sbLeftLine: TSpeedButton;
    sbRightLine: TSpeedButton;
    procedure cbStyleDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure cbWidthDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure sbTopLineClick(Sender: TObject);
    procedure sbBottomLineClick(Sender: TObject);
    procedure sbLeftLineClick(Sender: TObject);
    procedure sbRightLineClick(Sender: TObject);
    procedure pbCellExamplePaint(Sender: TObject);
  private
    FBorderLines: TrwBorderLines;
    procedure SetLine(ALine: TrwBorderLine; Visible: Boolean);
    procedure AssignLineProps(ALine: TrwBorderLine);
    procedure FOnChangeBorderLines(Sender: TObject);
  protected
    function  GetDefaultPropertyName: String; override;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    procedure ShowPropValue; override;
    procedure ApplyChanges; override;
  end;

implementation

{$R *.dfm}

procedure TrmOPEBorderLines.cbStyleDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  d: Integer;
begin
  with cbStyle.Canvas do
  begin
    Brush.Style := bsSolid;
    Brush.Color := clWhite;
    FillRect(Rect);

    Pen.Style := TPenStyle(Index);
    Pen.Mode := pmCopy;
    Pen.Width := 1;
    Pen.Color := clBlack;
    d := (Rect.Bottom - Rect.Top) div 2;
    MoveTo(10, Rect.Top + d);
    LineTo(Rect.Right - Rect.Left - 10, Rect.Top + d);
  end;
end;

procedure TrmOPEBorderLines.cbWidthDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
const
  WidthFracts: array [0..8] of String = (#188, #189, #190, '1', '1'#189, '2'#188, '3', '4'#189, '6');
var
  d: Integer;
begin
  with cbWidth.Canvas do
  begin
    Brush.Style := bsSolid;
    Brush.Color := clWhite;
    FillRect(Rect);

    Pen.Style := psSolid;
    Pen.Mode := pmCopy;
    Pen.Width := Round(StrToFloat(cbWidth.Items[Index]));
    if Pen.Width = 0 then
      Pen.Width := 1;
    Pen.Color := clBlack;
    d := (Rect.Bottom - Rect.Top) div 2;
    Font.Name := 'Arial';
    Font.Charset  := ANSI_CHARSET;
    Font.Size := 8;
    Font.Color := clBlack;
    TextRect(Rect, 8, Rect.Top, WidthFracts[Index] + ' pt');
    MoveTo(40, Rect.Top + d);
    LineTo(Rect.Right - Rect.Left - 10, Rect.Top + d);
  end;
end;

procedure TrmOPEBorderLines.sbTopLineClick(Sender: TObject);
begin
  SetLine(FBorderLines.TopLine, TSpeedButton(Sender).Down);
end;

procedure TrmOPEBorderLines.sbBottomLineClick(Sender: TObject);
begin
  SetLine(FBorderLines.BottomLine, TSpeedButton(Sender).Down);
end;

procedure TrmOPEBorderLines.sbLeftLineClick(Sender: TObject);
begin
  SetLine(FBorderLines.LeftLine, TSpeedButton(Sender).Down);
end;

procedure TrmOPEBorderLines.sbRightLineClick(Sender: TObject);
begin
  SetLine(FBorderLines.RightLine, TSpeedButton(Sender).Down);
end;

procedure TrmOPEBorderLines.pbCellExamplePaint(Sender: TObject);
const
  cCornerWidth = 10;
  cTextHeight = 3;
var
  R, R1: TRect;
  i, d: Integer;

  procedure DrawCorner(X1, Y1, X2, Y2, X3, Y3: Integer);
  begin
    with pbCellExample.Canvas do
    begin
      MoveTo(X1, Y1);
      LineTo(X2, Y2);
      LineTo(X3, Y3);
    end;
  end;

  function LogToScreen(AValue: Integer): Integer;
  begin
    Result := Trunc(ConvertUnit(AValue, utLogicalPixels, utScreenPixels) + 0.49);
  end;

  procedure DrawLine(BL: TrwBorderLine; X1, Y1, X2, Y2: Integer);
  begin
    if not BL.Visible then
      Exit;
    with pbCellExample.Canvas do
    begin
      Pen.Mode := pmCopy;
      Pen.Style := BL.Style;
      Pen.Width := LogToScreen(BL.Width);
      Pen.Color := BL.Color;

      MoveTo(X1, Y1);
      LineTo(X2, Y2);
    end;  
  end;

begin
  with pbCellExample, pbCellExample.Canvas do
  begin
    Brush.Color := clWhite;
    Brush.Style := bsSolid;
    FillRect(pbCellExample.ClientRect);

    // Corners
    Pen.Mode := pmCopy;
    Pen.Style := psSolid;
    Pen.Width := 1;
    Pen.Color := clGray;

    DrawCorner(cCornerWidth, cCornerWidth * 2,
               cCornerWidth * 2, cCornerWidth * 2,
               cCornerWidth * 2, cCornerWidth);

    DrawCorner(Width - cCornerWidth * 2, cCornerWidth,
               Width - cCornerWidth * 2, cCornerWidth * 2,
               Width - cCornerWidth, cCornerWidth * 2);

    DrawCorner(Width - cCornerWidth * 2, Height - cCornerWidth,
               Width - cCornerWidth * 2, Height - cCornerWidth * 2,
               Width - cCornerWidth, Height - cCornerWidth * 2);

    DrawCorner(cCornerWidth, Height - cCornerWidth * 2,
               cCornerWidth * 2, Height - cCornerWidth * 2,
               cCornerWidth * 2, Height - cCornerWidth);

    // Lines
    DrawLine(FBorderLines.LeftLine, cCornerWidth * 2, cCornerWidth * 2, cCornerWidth * 2, Height - cCornerWidth * 2);
    DrawLine(FBorderLines.TopLine, cCornerWidth * 2, cCornerWidth * 2, Width - cCornerWidth * 2, cCornerWidth * 2);
    DrawLine(FBorderLines.RightLine, Width - cCornerWidth * 2, cCornerWidth * 2, Width - cCornerWidth * 2, Height - cCornerWidth * 2);
    DrawLine(FBorderLines.BottomLine, cCornerWidth * 2, Height - cCornerWidth * 2, Width - cCornerWidth * 2, Height - cCornerWidth * 2);

    // "Text"
    R := Rect(cCornerWidth * 2 + 3, cCornerWidth * 2, Width - cCornerWidth * 2 - 3, Height - cCornerWidth * 2 - 3);

    if FBorderLines.LeftLine.Visible then
      R.Left := R.Left + LogToScreen(FBorderLines.LeftLine.Width);

    if FBorderLines.TopLine.Visible then
      R.Top := R.Top + LogToScreen(FBorderLines.TopLine.Width);

    if FBorderLines.RightLine.Visible then
      R.Right := R.Right - LogToScreen(FBorderLines.RightLine.Width);

    if FBorderLines.BottomLine.Visible then
      R.Bottom := R.Bottom - LogToScreen(FBorderLines.BottomLine.Width);

    d := (R.Bottom - R.Top) div (cTextHeight * 2);
    Brush.Color := clSilver;
    Brush.Style := bsSolid;
    for i := 1 to d do
    begin
      R1 := Rect(R.Left, R.Top + i * cTextHeight * 2 - cTextHeight, R.Right, R.Top + i * cTextHeight * 2);
      if i = 1 then
        R1.Left := R1.Left + (R.Right - R.Left) div 3
      else if i = d then
        R1.Right := R1.Right - (R.Right - R.Left) div 3;

      FillRect(R1);
    end;

  end;
end;

procedure TrmOPEBorderLines.AssignLineProps(ALine: TrwBorderLine);
const
  WidthFracts: array [0..8] of Integer = (1, 3, 5, 6, 9, 14, 19, 28, 38);
begin
  ALine.Style := TPenStyle(cbStyle.ItemIndex);
  ALine.Width := WidthFracts[cbWidth.ItemIndex];
  ALine.Color := cbColor.Selected;
end;

procedure TrmOPEBorderLines.SetLine(ALine: TrwBorderLine; Visible: Boolean);
begin
  ALine.Visible := Visible;
  if Visible then
    AssignLineProps(ALine);
  pbCellExample.Invalidate;
end;

procedure TrmOPEBorderLines.ShowPropValue;
begin
  inherited;
  FBorderLines.OnChange := nil;
  FBorderLines.Assign(TrwBorderLines(Pointer(Integer(OldPropertyValue))));
  FBorderLines.OnChange := FOnChangeBorderLines;

  with FBorderLines do
  begin
    sbTopLine.Down := TopLine.Visible;
    sbBottomLine.Down := BottomLine.Visible;
    sbLeftLine.Down := LeftLine.Visible;
    sbRightLine.Down := RightLine.Visible;
    pbCellExample.Invalidate;

    if TopLine.Visible and BottomLine.Visible and LeftLine.Visible and RightLine.Visible then
    begin
      if (TopLine.Color = BottomLine.Color) and (TopLine.Color = LeftLine.Color) and
         (TopLine.Color = RightLine.Color) then
        cbColor.Selected := TopLine.Color;

      if (TopLine.Style = BottomLine.Style) and (TopLine.Style = LeftLine.Style) and
         (TopLine.Style = RightLine.Style) then
        cbStyle.ItemIndex := Ord(TopLine.Style);

      if (TopLine.Width = BottomLine.Width) and (TopLine.Width = LeftLine.Width) and
         (TopLine.Width = RightLine.Width) then
        cbWidth.ItemIndex := TopLine.Width - 1;
    end;
  end;
end;


procedure TrmOPEBorderLines.AfterConstruction;
begin
  FBorderLines := TrwBorderLines.Create(nil);
  inherited;
end;

procedure TrmOPEBorderLines.BeforeDestruction;
begin
  FreeAndNil(FBorderLines);
  inherited;
end;

procedure TrmOPEBorderLines.ApplyChanges;
begin
  TrwBorderLines(Pointer(Integer(OldPropertyValue))).Assign(FBorderLines);
end;

function TrmOPEBorderLines.GetDefaultPropertyName: String;
begin
  Result := 'BorderLines';
end;

procedure TrmOPEBorderLines.FOnChangeBorderLines(Sender: TObject);
begin
  NotifyOfChanges;  
end;

end.
