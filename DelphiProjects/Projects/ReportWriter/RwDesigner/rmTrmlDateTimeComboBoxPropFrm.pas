unit rmTrmlDateTimeComboBoxPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmlCaptionControlPropFrm, rmOPEReportParamFrm, rmOPEFontFrm,
  rmOPEEnumFrm, rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, StdCtrls,
  ExtCtrls, ComCtrls, rwEngineTypes, rmOPEColorFrm;

type
  TrmTrmlDateTimeComboBoxProp = class(TrmTrmlCaptionControlProp)
    frmDateTimeType: TrmOPEEnum;
  protected
    procedure GetPropsFromObject; override;
    procedure AssignParmType; override;
  end;

implementation

uses rmTrmlCustomControlPropFrm;

{$R *.dfm}

{ TrmTrmlDateTimeComboBoxProp }

procedure TrmTrmlDateTimeComboBoxProp.AssignParmType;
begin
  frmParam1.ParamType := rwvDate;
end;

procedure TrmTrmlDateTimeComboBoxProp.GetPropsFromObject;
begin
  inherited;
  frmDateTimeType.MakeEnumList(['Date and Time', 'Date only', 'Time only'], [0, 1, 2]);
  frmDateTimeType.PropertyName := 'DateTimeType';
  frmDateTimeType.ShowPropValue;
end;

end.
