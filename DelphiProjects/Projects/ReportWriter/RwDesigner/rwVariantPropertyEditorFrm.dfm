inherited rwVariantPropertyEditor: TrwVariantPropertyEditor
  Left = 350
  Top = 206
  VertScrollBar.Range = 0
  BorderStyle = bsToolWindow
  Caption = 'Variant Property Editor'
  ClientHeight = 224
  ClientWidth = 320
  OldCreateOrder = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 6
    Top = 83
    Width = 27
    Height = 13
    Caption = 'Value'
  end
  inherited btnOK: TButton
    Left = 156
    Top = 193
    TabOrder = 2
  end
  inherited btnCancel: TButton
    Left = 239
    Top = 193
    TabOrder = 3
  end
  object rgType: TRadioGroup
    Left = 6
    Top = 5
    Width = 308
    Height = 71
    Caption = 'Value Type'
    Columns = 4
    Items.Strings = (
      'Boolean'
      'Currency'
      'Data'
      'Float'
      'Integer'
      'String'
      'Null')
    TabOrder = 0
  end
  object edString: TMemo
    Left = 6
    Top = 101
    Width = 307
    Height = 83
    TabOrder = 1
  end
end
