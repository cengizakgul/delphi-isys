unit rmDsgnInputFormContainerFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, rmChildFormFrm;

type
  TrmDsgnInputFormContainer = class(TrmChildForm)
    pnlBtns: TPanel;
    bvlBottom: TBevel;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    Edit1: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  end;

implementation

{$R *.dfm}

{ TrmDsgnInputFormContainer }

procedure TrmDsgnInputFormContainer.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Sender = Self then
    ModalResult := mrCancel;
    
  SetParent(nil);
end;

procedure TrmDsgnInputFormContainer.btnOKClick(Sender: TObject);
begin
  ModalResult := mrOK;
  Close;
end;

procedure TrmDsgnInputFormContainer.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
  Close;
end;

end.
 