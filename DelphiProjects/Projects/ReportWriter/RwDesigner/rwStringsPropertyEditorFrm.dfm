inherited rwStringsPropertyEditor: TrwStringsPropertyEditor
  Left = 308
  Top = 168
  Width = 585
  Height = 378
  Caption = 'Strings Property Editor'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOK: TButton
    Left = 496
    TabOrder = 2
  end
  inherited btnCancel: TButton
    Left = 496
    TabOrder = 3
  end
  object memStrings: TMemo
    Left = 3
    Top = 4
    Width = 485
    Height = 344
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    ScrollBars = ssBoth
    TabOrder = 0
    WordWrap = False
    OnChange = memStringsClick
    OnClick = memStringsClick
    OnEnter = memStringsClick
    OnKeyDown = memStringsKeyUp
    OnKeyUp = memStringsKeyUp
  end
  object pnPos: TPanel
    Left = 491
    Top = 329
    Width = 85
    Height = 19
    Anchors = [akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 1
  end
end
