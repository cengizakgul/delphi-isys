unit rmObjectTreeFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ComCtrls, rmTypes, rwDsgnUtils, rwUtils, rmCustomFrameFrm,
  rmDsgnTypes;

type
  TrmObjectTree = class(TrmCustomFrame)
    tvObjects: TTreeView;
    imlIcons: TImageList;
    procedure tvObjectsChange(Sender: TObject; Node: TTreeNode);
  private
    FRootObject: IrmDesignObject;
    FExternalSelection: Boolean;
    FInternalSelection: Boolean;
    procedure SetRootObject(const Value: IrmDesignObject);
    procedure BuildTree;
    function  GetRWObject: IrmDesignObject;
    procedure SetRWObject(const Value: IrmDesignObject);
  public
    procedure Refresh;
    property  RootObject: IrmDesignObject read FRootObject write SetRootObject;
    property  RWObject: IrmDesignObject read GetRWObject write SetRWObject;
  end;

implementation

uses rwDsgnRes;

{$R *.dfm}

{ TrmObjectTree }

procedure TrmObjectTree.BuildTree;
var
  PictBuf: TBitmap;
  Picts: TStringList;

  procedure SetNodePicture(ANode: TTreeNode; const AClassName: String);
  var
    i: Integer;
  begin
    i := Picts.IndexOf(UpperCase(AClassName));
    if i = -1 then
    begin
      DsgnRes.RWClassSmallPictures.GetClassPicture(AClassName, PictBuf);
      if not PictBuf.Empty then
      begin
        PictBuf.Transparent := True;
        PictBuf.TransparentMode := tmAuto;
        i := imlIcons.AddMasked(PictBuf, PictBuf.TransparentColor);
        Picts.AddObject(UpperCase(AClassName), Pointer(i));
      end
      else
        i := -1;
    end
    else
      i := Integer(Picts.Objects[i]);

    ANode.ImageIndex := i;
    ANode.SelectedIndex := i;
    ANode.StateIndex := i;
  end;

  procedure AddSelfAndChildren(AObject: IrmDesignObject; AParentNode: TTreeNode);
  var
    N: TTreeNode;
    i: Integer;
  begin
    if ((AObject.ObjectType = rwDOTInternal) and Assigned(AObject.GetObjectOwner)) or (TrwDsgnLockState(Integer(AObject.GetPropertyValue('DsgnLockState'))) = rwLSHidden) then
      Exit;

    N := tvObjects.Items.AddChildObject(AParentNode, AObject.GetPropertyValue('Name'), Pointer(AObject));
    SetNodePicture(N, AObject.GetClassName);

    for i := 0 to AObject.GetChildrenCount - 1 do
      AddSelfAndChildren(AObject.GetChildByIndex(i), N);
  end;

begin
  PictBuf := TBitmap.Create;
  Picts := TStringList.Create;

  tvObjects.Items.BeginUpdate;
  try
    tvObjects.Items.Clear;
    tvObjects.SortType := stNone;

    if Assigned(FRootObject) then
      AddSelfAndChildren(FRootObject, nil);

    tvObjects.SortType := stText;
  finally
    FreeAndNil(PictBuf);
    FreeAndNil(Picts);
    tvObjects.Items.EndUpdate;
  end;
end;

function TrmObjectTree.GetRWObject: IrmDesignObject;
begin
  if Assigned(tvObjects.Selected) then
    Result := IrmDesignObject(tvObjects.Selected.Data)
  else
    Result := nil;  
end;

procedure TrmObjectTree.Refresh;
begin
  FExternalSelection := True;
  BuildTree;
  FExternalSelection := False;
end;

procedure TrmObjectTree.SetRootObject(const Value: IrmDesignObject);
begin
  if FRootObject <> Value then
  begin
    FRootObject := Value;
    BuildTree;
  end;
end;

procedure TrmObjectTree.SetRWObject(const Value: IrmDesignObject);
var
  i: Integer;
  N: TTreeNode;
begin
  if FInternalSelection then
    Exit;

  N := nil;
  if Assigned(Value) then
    for i := 0 to tvObjects.Items.Count - 1 do
      if ObjectsAreTheSame(IrmDesignObject(tvObjects.Items[i].Data), Value) then
      begin
        N := tvObjects.Items[i];
        break;
      end;

  FExternalSelection := True;
  tvObjects.Selected := N;
  FExternalSelection := False;
end;

procedure TrmObjectTree.tvObjectsChange(Sender: TObject; Node: TTreeNode);
begin
  if not FExternalSelection and Assigned(Node) then
  begin
     FInternalSelection := True;
    (RMDesigner.GetActiveApartment as IrmDsgnComponent).UnselectAll;
    (RMDesigner.GetActiveApartment as IrmDsgnComponent).SelectObject(IrmDesignObject(Node.Data));
     Application.ProcessMessages;
     FInternalSelection := False;    
  end;
end;

end.
