unit rwAllEventsFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwReport, mwCustomEdit, rwBasicClasses, rwTypes, rwDsgnUtils, rwUtils,
  EvStreamUtils;

type
  TrwAllEvents = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    EventText: TmwCustomEdit;
    FStream: TStream;
    procedure FillCompEvents(AComponent: TrwComponent; AOwnerName: String);
    procedure FillCompChildren(AComponent: TrwComponent; AOwnerName: String);
  public
  end;

  procedure ShowAllEventOfReport(AComponent: TrwComponent);

implementation

var Frm: TrwAllEvents;

{$R *.DFM}

procedure ShowAllEventOfReport(AComponent: TrwComponent);
var
  h: String;
begin
  if Assigned(Frm) then
    Exit;

  Frm := TrwAllEvents.Create(nil);
  with Frm do
    try

      h := GetThreadTmpDir + '~tmpEvents.txt';
      FStream := TEvFileStream.Create(h, fmCreate);
      EventText.BeginUpdate;
      try
        FillCompEvents(AComponent, '');
        FillCompChildren(AComponent, AComponent.Name);
        if AComponent is TrwReport then
        begin
          FillCompEvents(TrwReport(AComponent).InputForm, '');
          FillCompChildren(TrwReport(AComponent).InputForm, TrwReport(AComponent).InputForm.Name);
        end;
        FStream.Position := 0;
        EventText.Lines.LoadFromStream(FStream);
      finally
        EventText.EndUpdate;
        FStream.Free;
        DeleteFile(h);
      end;

      ShowModal;
    finally
      Free;
      Frm := nil;
    end;
end;

procedure TrwAllEvents.FormCreate(Sender: TObject);
begin
  EventText := TmwCustomEdit.Create(Self);
  EventText.Align := alClient;
  EventText.Parent := Self;
  EventText.ReadOnly := True;
  EventText.HighLighter := GeneralSyn;
end;


procedure TrwAllEvents.FillCompEvents(AComponent: TrwComponent; AOwnerName: String);
var
  i:  Integer;
  Ev: TrwEvent;
  h:  string;
  A:  PChar;
begin
  for i := 0 to AComponent.EventCount-1 do
  begin
    Ev := AComponent.EventByIndex(i);
    if Ev.HandlersText <> '' then
    begin
      if FStream.Size > 0 then
        FStream.Write(#13#10#13#10+'//*******************************************'+#13#10, 51);
      if VAR_FUNCT_COMP_NAME <> AComponent.Name then
      begin
        h := AComponent.Name;
        if (AOwnerName <> '') then
          h := AOwnerName+'.'+h;
      end
      else
        h := '';
      h := MakeEventHandlerHeader(Ev, h) + #13 + #10 + Ev.HandlersText + #13 + #10 + 'end';
      A := AllocMem(Length(h)+1);
      try
        StrPCopy(A, h);
        FStream.Write(A^, Length(h));
      finally
        FreeMem(A);
      end;
    end;
  end;
end;


procedure TrwAllEvents.FillCompChildren(AComponent: TrwComponent; AOwnerName: String);
var
  i: Integer;
  C: TComponent;
begin
  for i := 0 to AComponent.VirtualComponentCount -1 do
  begin
    C := AComponent.GetVirtualChild(i);
    if (C is TrwComponent) and not (C is TrwReportForm) then
    begin
      FillCompEvents(TrwComponent(C), AOwnerName);
      if not (TrwComponent(C).InheritedComponent and AComponent.IsLibComponent) then
        FillCompChildren(TrwComponent(C), AOwnerName + '.' + TrwComponent(C).VirtualOwner.Name);
    end;
  end;
end;


procedure TrwAllEvents.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_ESCAPE then
    Close;
end;

initialization
  Frm := nil;

end.
