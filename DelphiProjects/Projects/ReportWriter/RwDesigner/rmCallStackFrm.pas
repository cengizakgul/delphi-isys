unit rmCallStackFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDockableFrameFrm, StdCtrls, rmDsgnTypes, rwUtils, rwEngineTypes, rwDsgnUtils;

type
  TrmCallStack = class(TrmDockableFrame)
    lbCallStack: TListBox;
    procedure lbCallStackDblClick(Sender: TObject);
  private
    procedure DMRunningStopped(var Message: TrmMessageRec); message mRunningStopped;
    procedure DMRunningPaused(var Message: TrmMessageRec); message mRunningPaused;
    procedure DMRunningProceeded(var Message: TrmMessageRec); message mRunningProceeded;

    procedure ShowCallStack;

  protected
    procedure GetListeningMessages(const AMessages: TList); override;
    procedure BeforeShow; override;
    procedure DoRefresh; override;
  end;

implementation

uses rmCustomFrameFrm;

{$R *.dfm}

{ TrmCallStack }

procedure TrmCallStack.BeforeShow;
begin
  Caption := 'Call Stack';
  ShowCallStack;
  inherited;
end;

procedure TrmCallStack.DMRunningPaused(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;

procedure TrmCallStack.DMRunningProceeded(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;

procedure TrmCallStack.DMRunningStopped(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;

procedure TrmCallStack.DoRefresh;
begin
  inherited;
  ShowCallStack;
end;

procedure TrmCallStack.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mRunningStopped));
  AMessages.Add(Pointer(mRunningPaused));
  AMessages.Add(Pointer(mRunningProceeded));
end;

procedure TrmCallStack.ShowCallStack;
var
  PCallStack: PTrmDebugStopLineList;
  i: Integer;
  h: String;
  A: TrwVMAddress;
begin
  lbCallStack.Items.BeginUpdate;
  try
    lbCallStack.Clear;
    PCallStack := (Apartment as IrmDsgnReport).ReportCallStack;

    for i := High(PCallStack^) downto Low(PCallStack^) do
    begin
      A.Segment := TrwVMCodeSegment(Ord(PCallStack^[i].Address.Segment));
      A.Offset := PCallStack^[i].Address.Offset;
      h := VMAddressToString(A);

      if Assigned(PCallStack^[i].SorceCodeLineInfo) then
        h := h + '   ' + PCallStack^[i].SorceCodeLineInfo^.ObjectLocation + '.' + PCallStack^[i].SorceCodeLineInfo^.FunctionName + '   (' + IntToStr(PCallStack^[i].SorceCodeLineInfo^.SourceCodeLineNbr) + ')';

      lbCallStack.Items.AddObject(h, Pointer(GetAbsoluteAddress(A)));
    end;

  finally
    lbCallStack.Items.EndUpdate;
  end;

  if lbCallStack.Count > 0 then
    lbCallStack.ItemIndex := 0;
end;

procedure TrmCallStack.lbCallStackDblClick(Sender: TObject);
var
  i: Integer;
begin
  if lbCallStack.ItemIndex <> -1 then
  begin
    i := lbCallStack.ItemIndex;
    SendApartmentMessage(Self, mtbShowSourceCodeByAddress, Integer(Pointer(lbCallStack.Items.Objects[lbCallStack.ItemIndex])), 0);
    lbCallStack.ItemIndex := i;
  end;
end;

initialization
  RegisterClass(TrmCallStack);


end.
