inherited rmTrmlDateTimeComboBoxProp: TrmTrmlDateTimeComboBoxProp
  Height = 261
  inherited pcCategories: TPageControl
    Height = 261
    inherited tsBasic: TTabSheet
      inherited GroupBox2: TGroupBox
        inherited frmCaptionLayout: TrmOPEEnum
          Width = 138
          inherited cbList: TComboBox
            Width = 70
          end
        end
      end
      inline frmDateTimeType: TrmOPEEnum
        Left = 8
        Top = 179
        Width = 198
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 3
        inherited lPropName: TLabel
          Width = 60
          Caption = 'Control Type'
        end
        inherited cbList: TComboBox
          Left = 78
          Width = 120
        end
      end
    end
    inherited tcAppearance: TTabSheet
      inherited frmColor: TrmOPEColor
        TabOrder = 1
      end
      inherited frmCaptionFont: TrmOPEFont
        TabOrder = 0
      end
    end
  end
end
