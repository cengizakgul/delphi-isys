inherited rmObjectEventsEditor: TrmObjectEventsEditor
  Width = 486
  Height = 201
  object Splitter1: TSplitter
    Left = 102
    Top = 0
    Height = 201
  end
  object lbEvents: TListBox
    Left = 0
    Top = 0
    Width = 102
    Height = 201
    Style = lbOwnerDrawFixed
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 15
    ParentFont = False
    PopupMenu = pmUserEvent
    TabOrder = 0
    OnClick = lbEventsClick
    OnDrawItem = lbEventsDrawItem
  end
  inline rmEventText: TrmFunctionText
    Left = 105
    Top = 0
    Width = 381
    Height = 201
    Align = alClient
    AutoScroll = False
    TabOrder = 1
    inherited pcFunctions: TPageControl
      Width = 381
      Height = 201
    end
  end
  object pmUserEvent: TPopupMenu
    Left = 36
    Top = 92
    object miRegUserEvent: TMenuItem
      Caption = 'Register New User Event...'
      OnClick = miRegUserEventClick
    end
    object miUnRegUserEvent: TMenuItem
      Caption = 'Unregister User Event'
      OnClick = miUnRegUserEventClick
    end
  end
end
