unit rmObjectPropFormFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rwBasicClasses, rmObjectPropsFrm, rmOPBasicFrm, TypInfo, ComCtrls,
  rwUtils, rwDsgnUtils, ExtCtrls, rwDebugInfo,
  Buttons, StdCtrls, rmTypes, rmChildFormFrm, rmDsgnTypes, rwRTTI;

type
  TrmObjectPropForm = class(TrmChildForm)
    pnlButtons: TPanel;
    btnOK: TButton;
    Panel1: TPanel;
    btnCancel: TButton;
    btnApply: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    FPropertyFrame: TrmObjectProp;
    procedure Initialize(AObject: IrmDesignObject);
    procedure ExitFromActiveControl;
  public
    procedure ApplyChanges;
    procedure RefreshPropertyValues;    
    procedure NotifyOfChanges;
  end;

  procedure ShowObjectProperties(AObject: IrmDesignObject; AOwner: TComponent);
  procedure ShowObjectPropertiesIfPossible(AObject: IrmDesignObject = nil);
  procedure CloseObjectProperties;

implementation

{$R *.dfm}

uses rmObjPropertyEditorFrm;

var
  Frm: TrmObjectPropForm = nil;

procedure ShowObjectProperties(AObject: IrmDesignObject; AOwner: TComponent);
var
  P: TForm;
  SelObjects: TrmListOfObjects;
  i : integer;
begin
  { for issue #53199 }
  SelObjects := RMDesigner.GetComponentDesigner.GetSelectedObjects;
  if Length(SelObjects) > 1 then
    for i := High(SelObjects) downto Low(SelObjects) do
      if not ObjectsAreTheSame(SelObjects[i], AObject) then
        RMDesigner.GetComponentDesigner.UnSelectObject(SelObjects[i]);

  if not Assigned(Frm) then
  begin
    Frm := TrmObjectPropForm.Create(AOwner);
    P := TForm(RMDesigner.VCLObject);
    Frm.Left := (P.Width - Frm.Width) div 2;
    Frm.Top := (P.Height - Frm.Height) div 2;
  end
  else
    Frm.ExitFromActiveControl;

  ShowObjectPropertiesIfPossible(AObject);

  P := TForm(RMDesigner.VCLObject);
  Frm.Parent := P;
end;

procedure ShowObjectPropertiesIfPossible(AObject: IrmDesignObject = nil);
begin
  if Assigned(Frm) then
  begin
    if not Assigned(AObject) then
      AObject := Frm.FPropertyFrame.RWObject;

    if Debugging and Assigned(AObject) and (DebugInfo.Report <> AObject.GetRootOwner) then
      AObject := ComponentByPath(AObject.GetPathFromRootOwner, DebugInfo.Report);

    if Assigned(AObject) then
    begin
      Frm.Initialize(AObject);
      Frm.Show;
    end
    else
      CloseObjectProperties;
  end;
end;

procedure CloseObjectProperties;
begin
  FreeAndNil(Frm);
end;

procedure TrmObjectPropForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  ExitFromActiveControl;
  Action := caFree;
end;


procedure TrmObjectPropForm.Initialize(AObject: IrmDesignObject);
var
  h: String;
  Cl: TrmObjectPropsClass;
  oCl: TClass;
  lTop, lWidth, lHeight: Integer;
  C: TComponent;
  rwCl: IrwClass;
begin
  BorderStyle := bsToolWindow;
  pnlButtons.Visible := True;

  h := AObject.GetClassName;
  Cl := nil;
  while not AnsiSameText(h, 'TrwComponent') do
  begin
    Cl := TrmObjectPropsClass(GetClass('Trm' + h + 'Prop'));
    if Assigned(Cl) then
      break

    else
    begin
      oCl := GetClass(h);
      if Assigned(oCl) then
        h := oCl.ClassParent.ClassName
      else
      begin
        rwCl := rwRTTIInfo.FindClass(h);
        if not Assigned(rwCl) then
          break;
        h := rwCl.GetAncestor.GetClassName;
      end;
    end;
  end;

  if not Assigned(Cl) then
    Cl := TrmOPBasic;

  if not Assigned(FPropertyFrame) or not (FPropertyFrame.ClassInfo = Cl.ClassInfo) then
  begin
    if Assigned(FPropertyFrame) and (FPropertyFrame is TrmOPBasic) then
      h := TrmOPBasic(FPropertyFrame).pcCategories.ActivePage.Name
    else
      h := 'tsBasic';

    FreeAndNil(FPropertyFrame);

    FPropertyFrame := Cl.Create(Self);
    FPropertyFrame.Top := 4;
    FPropertyFrame.Left := 4;
    FPropertyFrame.Parent := Self;

    lTop := Top - GetSystemMetrics(SM_CYCAPTION);
    lWidth := Width + FPropertyFrame.Width + FPropertyFrame.Left * 2 - ClientWidth;
    lHeight := Height + FPropertyFrame.Height + FPropertyFrame.Top + pnlButtons.Height - ClientHeight;
    SetBounds(Left, lTop, lWidth, lHeight);

    if DesignRMAdvancedMode then
      FPropertyFrame.Align := alClient;

    if FPropertyFrame is TrmOPBasic then
    begin
      C := TrmOPBasic(FPropertyFrame).FindComponent(h);
      if Assigned(C) and (C is TTabSheet) then
        TrmOPBasic(FPropertyFrame).pcCategories.ActivePage := TTabSheet(C);
    end;
  end;

  FPropertyFrame.RWObject := AObject;
  RefreshPropertyValues;
end;

procedure TrmObjectPropForm.FormDestroy(Sender: TObject);
begin
  Frm := nil
end;

procedure TrmObjectPropForm.ApplyChanges;

  procedure LookForPropFrames(AParent: TWinControl);
  var
    i: Integer;
  begin
    for i := 0 to AParent.ControlCount - 1 do
      if AParent.Controls[i].Enabled then
        if AParent.Controls[i] is TrmObjPropertyEditor then
          TrmObjPropertyEditor(AParent.Controls[i]).ApplyChanges
        else if AParent.Controls[i] is TWinControl then
          LookForPropFrames(TWinControl(AParent.Controls[i]));
  end;

begin
  LookForPropFrames(Self);

  PostApartmentMessage(Self, mObjectPropertyChanged, Integer(Pointer(FPropertyFrame.RWObject)), 0);

  RefreshPropertyValues;

  FPropertyFrame.RWObject.Refresh;
end;

procedure TrmObjectPropForm.btnApplyClick(Sender: TObject);
begin
  ApplyChanges;
end;

procedure TrmObjectPropForm.btnOKClick(Sender: TObject);
begin
  if btnApply.Enabled then
    ApplyChanges;
  Close;
end;

procedure TrmObjectPropForm.NotifyOfChanges;
begin
  btnApply.Enabled := True;
  btnCancel.Enabled := True;
end;

procedure TrmObjectPropForm.btnCancelClick(Sender: TObject);
begin
  RefreshPropertyValues;
end;

procedure TrmObjectPropForm.RefreshPropertyValues;

  procedure LookForPropFrames(AParent: TWinControl);
  var
    i: Integer;
  begin
    for i := 0 to AParent.ControlCount - 1 do
      if AParent.Controls[i].Enabled then
        if AParent.Controls[i] is TrmObjPropertyEditor then
          TrmObjPropertyEditor(AParent.Controls[i]).ShowPropValue
        else if AParent.Controls[i] is TWinControl then
          LookForPropFrames(TWinControl(AParent.Controls[i]));
  end;

begin
  LookForPropFrames(Self);
  btnApply.Enabled := False;
  btnCancel.Enabled := False;
end;

procedure TrmObjectPropForm.ExitFromActiveControl;
var
  M: TMethod;
begin
  if Assigned(ActiveControl) and IsPublishedProp(ActiveControl, 'OnExit') then
  begin
    M := GetMethodProp(ActiveControl, 'OnExit');
    if Assigned(M.Code) then
      TNotifyEvent(M)(ActiveControl);
  end;
end;

end.
