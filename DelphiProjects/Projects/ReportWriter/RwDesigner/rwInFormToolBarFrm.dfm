inherited rwInFormToolsBar: TrwInFormToolsBar
  Width = 1041
  inherited ctbBar: TControlBar
    Width = 1041
    inherited tlbComponents: TToolBar
      Left = 602
      Top = 28
      Width = 414
      object tlbFileDialog: TToolButton
        Left = 92
        Top = 0
        Hint = 'FileDialog'
        Caption = 'TrwFileDialog'
        Grouped = True
        ImageIndex = 87
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtText: TToolButton
        Left = 115
        Top = 0
        Hint = 'Text'
        Caption = 'TrwText'
        Grouped = True
        ImageIndex = 30
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtButton: TToolButton
        Left = 138
        Top = 0
        Hint = 'Button'
        Caption = 'TrwButton'
        Grouped = True
        ImageIndex = 85
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtPanel: TToolButton
        Left = 161
        Top = 0
        Hint = 'Panel'
        Caption = 'TrwPanel'
        Grouped = True
        ImageIndex = 36
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtGroupBox: TToolButton
        Left = 184
        Top = 0
        Hint = 'GroupBox'
        Caption = 'TrwGroupBox'
        Grouped = True
        ImageIndex = 53
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtEdit: TToolButton
        Left = 207
        Top = 0
        Hint = 'Edit'
        Caption = 'TrwEdit'
        Grouped = True
        ImageIndex = 48
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtDateEdit: TToolButton
        Left = 230
        Top = 0
        Hint = 'DateEdit'
        Caption = 'TrwDateEdit'
        Grouped = True
        ImageIndex = 52
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtCheckBox: TToolButton
        Left = 253
        Top = 0
        Hint = 'CheckBox'
        Caption = 'TrwCheckBox'
        Grouped = True
        ImageIndex = 50
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtComboBox: TToolButton
        Left = 276
        Top = 0
        Hint = 'ComboBox'
        Caption = 'TrwComboBox'
        Grouped = True
        ImageIndex = 51
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtRadioGroup: TToolButton
        Left = 299
        Top = 0
        Hint = 'RadioGroup'
        Caption = 'TrwRadioGroup'
        Grouped = True
        ImageIndex = 54
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtPageControl: TToolButton
        Left = 322
        Top = 0
        Hint = 'PageControl'
        Caption = 'TrwPageControl'
        Grouped = True
        ImageIndex = 84
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtDBComboBox: TToolButton
        Left = 345
        Top = 0
        Hint = 'DBComboBox'
        Caption = 'TrwDBComboBox'
        Grouped = True
        ImageIndex = 49
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtGrid: TToolButton
        Left = 368
        Top = 0
        Hint = 'DBGrid'
        Caption = 'TrwDBGrid'
        Grouped = True
        ImageIndex = 73
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton15: TToolButton
        Left = 391
        Top = 0
        Hint = 'Library Component'
        Caption = 'Library'
        Grouped = True
        ImageIndex = 45
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
    end
    inherited tlbExperts: TToolBar
      Left = 1029
      Width = 115
      object tbtTabOrder: TToolButton
        Left = 92
        Top = 0
        Hint = 'Tab Order'
        Caption = 'tbtTabOrder'
        ImageIndex = 43
        OnClick = tbtTabOrderClick
      end
    end
  end
end
