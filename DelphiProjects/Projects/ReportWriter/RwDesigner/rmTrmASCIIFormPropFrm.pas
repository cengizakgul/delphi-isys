unit rmTrmASCIIFormPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, StdCtrls, ExtCtrls, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls, rmOPEEnumFrm, rmTypes,
  rmOPEBooleanFrm, rmOPEExpressionFrm, rmOPEBlockingControlFrm;

type
  TrmTrmASCIIFormProp = class(TrmOPBasic)
    frmResultType: TrmOPEEnum;
    frmDelimiter: TrmOPEEnum;
    frmQualifier: TrmOPEEnum;
    frmDefaultFileName: TrmOPEStringSingleLine;
    frmBlockParentIfEmpty: TrmOPEBoolean;
    pnlBlocking: TPanel;
    frmProduceRecordIf: TrmOPEBlockingControl;
    procedure frmResultTypecbListChange(Sender: TObject);
  private
    procedure UpdateContextControls;
  public
    procedure GetPropsFromObject; override;
  end;

implementation

{$R *.dfm}

{ TrmTrmASCIIFormProp }

procedure TrmTrmASCIIFormProp.GetPropsFromObject;
var
  Vals: TStringList;
begin
  inherited;
  frmDefaultFileName.PropertyName := 'DefaultFileName';
  frmDefaultFileName.ShowPropValue;

  frmBlockParentIfEmpty.PropertyName := 'BlockParentIfEmpty';
  frmBlockParentIfEmpty.ShowPropValue;

  frmProduceRecordIf.BlockID := 'Calc';
  frmProduceRecordIf.ShowPropValue;

  frmResultType.MakeEnumList(['Fixed Length',
                              'Delimited', 'Excel (xls)'], [Ord(rmARTFixedLength), Ord(rmARTDelimited), Ord(rmARTXls)]);
  frmResultType.PropertyName := 'ResultType';
  frmResultType.ShowPropValue;

  Vals := TStringList.Create;
  try
    frmDelimiter.AllowCustomValues := True;
    Vals.Add(',=Comma');
    Vals.Add(';=Semicolumn');
    Vals.Add(#9'=Tab');
    Vals.Add(' =Space');
    frmDelimiter.Values :=Vals;
    frmDelimiter.PropertyName := 'Delimiter';
    frmDelimiter.ShowPropValue;

    Vals.Clear;
    Vals.Add('=<No>');
    Vals.Add('"=Double Quote');
    Vals.Add('''=Single Quote');
    frmQualifier.AllowCustomValues := True;
    frmQualifier.Values :=Vals;
    frmQualifier.PropertyName := 'Qualifier';
    frmQualifier.ShowPropValue;
  finally
    FreeAndNil(Vals);
  end;

  UpdateContextControls;
end;

procedure TrmTrmASCIIFormProp.frmResultTypecbListChange(Sender: TObject);
begin
  frmResultType.cbListChange(Sender);
  UpdateContextControls;
end;

procedure TrmTrmASCIIFormProp.UpdateContextControls;
begin
  if (TrmASCIIResultType(Integer(frmResultType.NewPropertyValue)) = rmARTFixedLength) or
    (TrmASCIIResultType(Integer(frmResultType.NewPropertyValue)) = rmARTXls) then
  begin
    frmDelimiter.Enabled := False;
    frmQualifier.Enabled := False;
  end
  else
  begin
    frmDelimiter.Enabled := True;
    frmQualifier.Enabled := True;
  end;
end;

end.
