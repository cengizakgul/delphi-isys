object rwWatchList: TrwWatchList
  Left = 337
  Top = 136
  Width = 444
  Height = 169
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'Watch List'
  Color = clBtnFace
  UseDockManager = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object lbWatch: TListBox
    Left = 0
    Top = 0
    Width = 436
    Height = 142
    Align = alClient
    ItemHeight = 13
    TabOrder = 0
    OnDblClick = actEditWatchExecute
  end
  object aclDesignerActions: TActionList
    Left = 224
    Top = 46
    object actAddWatch: TAction
      Caption = 'Add Watch...'
      ImageIndex = 63
      ShortCut = 16500
      OnExecute = actAddWatchExecute
    end
    object actDelWatch: TAction
      Caption = 'Watches'
      ImageIndex = 65
      ShortCut = 46
      OnExecute = actDelWatchExecute
    end
    object actEditWatch: TAction
      Caption = 'actEditWatch'
      ShortCut = 13
      OnExecute = actEditWatchExecute
    end
    object actStepOver: TAction
      Caption = 'actStepOver'
      ImageIndex = 66
      ShortCut = 119
      OnExecute = actStepOverExecute
    end
    object actTraceInto: TAction
      Caption = 'actTraceInto'
      ImageIndex = 67
      ShortCut = 118
      OnExecute = actTraceIntoExecute
    end
    object actRun: TAction
      Caption = 'actRun'
      ImageIndex = 69
      ShortCut = 120
      OnExecute = actRunExecute
    end
    object actTerminate: TAction
      Caption = 'actTerminate'
      ImageIndex = 72
      ShortCut = 16497
      OnExecute = actTerminateExecute
    end
    object actPCode: TAction
      Caption = 'actPCode'
      ShortCut = 49219
      OnExecute = actPCodeExecute
    end
    object actAddWatch2: TAction
      Caption = 'actAddWatch2'
      ShortCut = 45
      OnExecute = actAddWatch2Execute
    end
    object actClose: TAction
      Caption = 'Close'
      ShortCut = 27
      OnExecute = actCloseExecute
    end
  end
end
