inherited rmDsgnLocalComponent: TrmDsgnLocalComponent
  Width = 681
  Height = 450
  inherited spltLeft: TSplitter
    Height = 397
  end
  inherited spltRight: TSplitter
    Left = 628
    Height = 397
  end
  inherited spltBottom: TSplitter
    Top = 397
    Width = 681
  end
  inherited pnlDockBottom: TPanel
    Top = 400
    Width = 681
  end
  inherited pnlDockLeft: TPanel
    Height = 397
  end
  inherited pnlDockRight: TPanel
    Left = 631
    Height = 397
  end
  inherited pnlList: TPanel
    Height = 397
    inherited lvItems: TListView
      Height = 328
    end
    object Panel1: TPanel
      Left = 0
      Top = 328
      Width = 100
      Height = 69
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object btnSave: TButton
        Left = 5
        Top = 38
        Width = 75
        Height = 23
        Caption = 'Save'
        TabOrder = 0
        OnClick = btnSaveClick
      end
      object btnCancel: TButton
        Left = 5
        Top = 8
        Width = 75
        Height = 23
        Caption = 'Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  inherited pnlWorkArea: TPanel
    Width = 475
    Height = 397
  end
  inherited opdImport: TOpenDialog
    Filter = 
      'Report Master Component (*.rwr)|*.rwr|Report Master Component (*' +
      '.txt)|*.txt|All Files (*.*)|*.*'
    Title = 'Import Component'
    Left = 316
    Top = 224
  end
  inherited svdExport: TSaveDialog
    Filter = 
      'Report Master Component (*.rwr)|*.rwr|Report Master Component (*' +
      '.txt)|*.txt|All Files (*.*)|*.*'
    Title = 'Export Component'
  end
end
