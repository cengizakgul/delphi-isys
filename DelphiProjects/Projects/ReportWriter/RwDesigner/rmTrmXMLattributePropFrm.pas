unit rmTrmXMLattributePropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, StdCtrls, ExtCtrls, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls;

type
  TrmTrmXMLattributeProp = class(TrmOPBasic)
    frmNSName: TrmOPEStringSingleLine;
  private
  public
    procedure GetPropsFromObject; override;  
  end;

implementation

{$R *.dfm}

{ TrmTrmXMLattributeProp }

procedure TrmTrmXMLattributeProp.GetPropsFromObject;
begin
  inherited;
  frmNSName.PropertyName := 'NSName';
  frmNSName.ShowPropValue;
end;

end.
 