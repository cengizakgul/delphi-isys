unit rmTrmXMLFormPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmXMLNodePropFrm, StdCtrls, ExtCtrls, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls, rmOPEXMLFormSchemasFrm, rmDsgnTypes, rwDsgnUtils,
  rmOPBasicFrm, rmOPEBooleanFrm, rmOPEExpressionFrm,
  rmOPEBlockingControlFrm;

type
  TrmTrmXMLFormProp = class(TrmTrmXMLNodeProp)
    frmDefaultFileName: TrmOPEStringSingleLine;
    frmNameSpace: TrmOPEStringSingleLine;
    pnlSchemas: TrmOPEXMLFormSchemas;
    frmProduceIf: TrmOPEBlockingControl;
  private
  public
    procedure GetPropsFromObject; override;
    procedure OnChangeProperty(Sender: TrmObjPropertyEditor); override;
  end;

implementation

{$R *.dfm}

uses rmObjectPropFormFrm;

{ TrmTrmXMLFormProp }

procedure TrmTrmXMLFormProp.GetPropsFromObject;
begin
  inherited;
  frmDefaultFileName.PropertyName := 'DefaultFileName';
  frmDefaultFileName.ShowPropValue;

  frmNameSpace.PropertyName := 'NameSpace';
  frmNameSpace.ShowPropValue;

  frmProduceIf.BlockID := 'Calc';
  frmProduceIf.ShowPropValue;
end;


procedure TrmTrmXMLFormProp.OnChangeProperty(Sender: TrmObjPropertyEditor);
begin
  inherited;

  if Sender = pnlSchemas then
  begin
    SendApartmentMessage(cAnonymousSender, mRefreshComponents, 0, 0);
    (Parent as TrmObjectPropForm).RefreshPropertyValues;
  end;
end;

end.



