inherited rmTrmPrintFormProp: TrmTrmPrintFormProp
  Width = 532
  Height = 259
  inherited pcCategories: TPageControl
    Width = 532
    Height = 259
    inherited tcAppearance: TTabSheet
      Caption = 'Page Setup'
      inline rmOPEPageSetup1: TrmOPEPageSetup
        Left = 0
        Top = 0
        Width = 524
        Height = 231
        Align = alClient
        AutoScroll = False
        TabOrder = 0
        inherited gbSize: TGroupBox
          Left = 8
        end
        inherited gbxOrientation: TGroupBox
          Left = 8
        end
        inherited chbDuplexing: TCheckBox
          Left = 8
        end
        inherited gbMargins: TGroupBox
          Left = 239
        end
      end
    end
    inherited tcPrint: TTabSheet
      inherited frmCutable: TrmOPEBoolean
        Top = 178
        Visible = False
      end
      inherited frmPrintOnEachPage: TrmOPEBoolean
        Top = 204
        Visible = False
      end
      inherited frmBlockParentIfEmpty: TrmOPEBoolean
        Top = 10
        Width = 221
        inherited chbProp: TCheckBox
          Width = 221
          Caption = 'Do not produce result if no data on form'
        end
      end
      inherited pnlBlocking: TPanel
        Top = 38
      end
    end
  end
end
