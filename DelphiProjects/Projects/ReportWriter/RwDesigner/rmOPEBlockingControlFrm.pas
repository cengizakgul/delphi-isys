unit rmOPEBlockingControlFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, StdCtrls,
  Dialogs, rmOPEExpressionFrm, rmFMLExprPanelFrm, rmTypes, rwBasicClasses, rmFormula;

type
  TrmOPEBlockingControl = class(TrmOPEExpression)
    lCaption: TLabel;
  private
    FBlockID: String;
    FOriginalBlock: String;
  public
    property  BlockID: String read FBlockID write FBlockID; //Calc, BranchCalc etc. (omit Before and After prefixes)
    procedure AfterConstruction; override;
    procedure ShowPropValue; override;
    procedure ApplyChanges; override;
  end;

implementation

{$R *.dfm}

{ TrmOPEBlockingControl }

procedure TrmOPEBlockingControl.AfterConstruction;
begin
  inherited;
  FormulaActionType := fatCalcControl;
end;

procedure TrmOPEBlockingControl.ApplyChanges;
var
  SelfRootPath: String;
  Fml: TrmFMLFormula;
begin
  PropertyName := FOriginalBlock;

  inherited;

  Fml := TrwComponent(LinkedObject).Formulas.FormulaByAction(FormulaActionType, FOriginalBlock);
  if Assigned(Fml) then
  begin
    SelfRootPath := (TComponent(LinkedObject) as IrmDesignObject).GetPathFromRootParent;

    if (Fml as IrmFMLFormula).ThereAreObjectsBelongingTo(SelfRootPath) then
      PropertyName := 'BlockAfter' + FBlockID
    else
      PropertyName := 'BlockBefore' + FBlockID;

    Fml.ActionInfo := PropertyName;
  end;  
end;

procedure TrmOPEBlockingControl.ShowPropValue;
begin
  FOriginalBlock := 'BlockBefore' + FBlockID;
  if TrwComponent(LinkedObject).Formulas.FormulaByAction(FormulaActionType, FOriginalBlock) = nil then
  begin
    FOriginalBlock := 'BlockAfter' + FBlockID;
    if TrwComponent(LinkedObject).Formulas.FormulaByAction(FormulaActionType, FOriginalBlock) = nil then
      FOriginalBlock := '';
  end;

  PropertyName := FOriginalBlock;

  inherited;
end;

end.
