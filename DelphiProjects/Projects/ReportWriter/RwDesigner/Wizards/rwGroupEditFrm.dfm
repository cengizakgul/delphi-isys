object rwGroupEdit: TrwGroupEdit
  Left = 483
  Top = 429
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Group property'
  ClientHeight = 150
  ClientWidth = 251
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 5
    Top = 4
    Width = 241
    Height = 107
    Caption = 'Group'
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 19
      Width = 38
      Height = 13
      Caption = 'Column:'
    end
    object lColName: TLabel
      Left = 56
      Top = 19
      Width = 45
      Height = 13
      Caption = 'lColName'
    end
    object chbHeader: TCheckBox
      Left = 9
      Top = 48
      Width = 97
      Height = 17
      Caption = 'Group Header'
      TabOrder = 0
    end
    object chbFooter: TCheckBox
      Left = 9
      Top = 72
      Width = 97
      Height = 17
      Caption = 'Group Footer'
      TabOrder = 1
    end
    object chbBreakPage: TCheckBox
      Left = 130
      Top = 72
      Width = 88
      Height = 17
      Caption = 'Break Page'
      TabOrder = 2
    end
  end
  object btnOk: TButton
    Left = 86
    Top = 119
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
  end
  object btnCancel: TButton
    Left = 170
    Top = 119
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
