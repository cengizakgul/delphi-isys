// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWPParStrFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWPParamFrm, StdCtrls;

type
  TrwWPParStr = class(TrwWPParam)
    edStr: TEdit;
    procedure edStrExit(Sender: TObject);
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
  end;

implementation

{$R *.DFM}

{ TrwWPParStr }

function TrwWPParStr.GetValue: string;
begin
  Result := edStr.Text;
end;

procedure TrwWPParStr.SetValue(const Value: string);
begin
  edStr.Text := Value;
end;

procedure TrwWPParStr.edStrExit(Sender: TObject);
begin
  DoChange;
end;

end.
