object rwFieldAdd: TrwFieldAdd
  Left = 606
  Top = 223
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Add Fields'
  ClientHeight = 423
  ClientWidth = 632
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object btnAdd: TSpeedButton
    Left = 273
    Top = 98
    Width = 24
    Height = 25
    Caption = '>'
    OnClick = btnAddClick
  end
  object btnDel: TSpeedButton
    Left = 273
    Top = 130
    Width = 24
    Height = 25
    Caption = '<'
    OnClick = btnDelClick
  end
  object lPath: TLabel
    Left = 6
    Top = 398
    Width = 24
    Height = 13
    Caption = 'lPath'
  end
  object btnOk: TButton
    Left = 468
    Top = 394
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 552
    Top = 394
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object grbFields: TGroupBox
    Left = 6
    Top = 5
    Width = 259
    Height = 380
    Caption = 'Available Fields'
    TabOrder = 2
    object lvFields: TListView
      Left = 9
      Top = 18
      Width = 240
      Height = 351
      Columns = <
        item
          Width = 217
        end>
      ColumnClick = False
      HideSelection = False
      LargeImages = WizDM.ImageList
      ReadOnly = True
      ShowColumnHeaders = False
      SmallImages = WizDM.ImageList
      SortType = stText
      TabOrder = 0
      ViewStyle = vsReport
      OnChange = lvFieldsChange
      OnDblClick = lvFieldsDblClick
      OnEnter = lvFieldsEnter
      OnKeyPress = lvFieldsKeyPress
    end
  end
  object grbSelFields: TGroupBox
    Left = 304
    Top = 5
    Width = 323
    Height = 243
    Caption = 'Selected Fields'
    TabOrder = 3
    object lvSelFields: TTreeView
      Left = 11
      Top = 16
      Width = 302
      Height = 204
      HideSelection = False
      Images = WizDM.ImageList
      Indent = 19
      ReadOnly = True
      SortType = stText
      TabOrder = 0
      OnChange = lvSelFieldsChange
      OnEnter = lvFieldsEnter
    end
    object chbOuter: TCheckBox
      Left = 11
      Top = 223
      Width = 304
      Height = 17
      Caption = 'If there is no data do not show information from parent level'
      TabOrder = 1
      Visible = False
      OnClick = chbOuterClick
    end
  end
  object grbDescr: TGroupBox
    Left = 304
    Top = 256
    Width = 323
    Height = 130
    Caption = 'Description'
    TabOrder = 4
    object meDescr: TMemo
      Left = 11
      Top = 17
      Width = 302
      Height = 102
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
end
