// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWPParDateFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWPParamFrm, ComCtrls, StdCtrls;

type
  TrwWPParDate = class(TrwWPParam)
    deDate: TDateTimePicker;
    chbCurDate: TCheckBox;
    procedure deDateExit(Sender: TObject);
    procedure chbCurDateClick(Sender: TObject);
  protected
    function  GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
  end;
implementation

{$R *.DFM}

{ TrwWPParDate }

function TrwWPParDate.GetValue: string;
begin
  if chbCurDate.Checked then
    Result := 'now'  
  else
    Result := DateToStr(deDate.Date);
end;

procedure TrwWPParDate.SetValue(const Value: string);
begin
  if (UpperCase(Value) = 'NOW') or (UpperCase(Value) = 'TODAY') then
  begin
    chbCurDate.Checked := True;
    chbCurDate.OnClick(nil);
  end
  else
    deDate.Date := StrToDate(Value)
end;

procedure TrwWPParDate.deDateExit(Sender: TObject);
begin
  DoChange;
end;

procedure TrwWPParDate.chbCurDateClick(Sender: TObject);
begin
  if chbCurDate.Checked then
  begin
    deDate.Date := 0;
    deDate.Enabled := False;
  end
  else
  begin
    deDate.Date := Date;
    deDate.Enabled := True;
  end;
  DoChange;
end;


end.
