// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWizardPrEditEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWizardFrm, StdCtrls, Buttons, ComCtrls, rwWizardInfo, ExtCtrls,
  wwdblook, Db, CheckLst, sbAPI, rwUtils, rwDB,
  rwBasicClasses, rwCommonClasses, rwDataDictionary, rwReport, rwTypes,
  rwReportControls, rwInputFormControls, rwQBInfo, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, rwDesignClasses, rwEngineTypes, rwLogQuery,
  rwEngine, ISDataAccessComponents;

type
  TrwWizInfPayroll = class(TrwWizardInfo)
  private
    FPayroll: String;

  protected
    procedure CreateReportObject; override;
    procedure CustomQueryCondition(ATable: TrwWizardQueryTable; var sFrom, sWhere: string); override;
    procedure CheckQueryParams(AQuery: TrwQuery; var AEventHandler: string); override;

  public
    procedure CreateReport; override;
    procedure Assign(Source: TPersistent); override;
    class function FormEditor: TClass; override;
    class function WizardName: string; override;
  published
    property Payroll: String read FPayroll write FPayroll;
  end;


  TrwWizardPrEdit = class(TrwWizard)
    GroupBox11: TGroupBox;
    clPayrolls: TCheckListBox;
    procedure cbClientsCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure btnAddTblClick(Sender: TObject);
  private
    procedure InitPayrollList;
  protected
    procedure Initialize; override;
    procedure Finalize; override;
  public
  end;

implementation

{$R *.DFM}

uses rwTableAddFrm;


{ TrwWizInfPayroll }

procedure TrwWizInfPayroll.Assign(Source: TPersistent);
begin
  inherited;
  Payroll := TrwWizInfPayroll(Source).Payroll;
end;


procedure TrwWizInfPayroll.CheckQueryParams(AQuery: TrwQuery; var AEventHandler: string);
begin
  inherited;
  CheckQueryParam(AQuery, 'MY_DATE', '''now''', AEventHandler);
end;


procedure TrwWizInfPayroll.CreateReport;
var
  Q1, Q2: TrwQuery;
  h: String;
  C, C1: TrwComponent;
  Col: TrwWizTblColumn;
  i,j,k,m: Integer;
  lWidth, nW, dW: Integer;
  Buf: TrwBuffer;
  DS: TrwDataSet;
  Fld: TrwWizardQueryTableField;
  Cont: TrwControl;
  V: TrwGlobalVariable;
  SF: TrwQBShowingField;
  Attr: TrwDatadictField;
begin
  inherited;

  if MainTable.Columns.Count = 0 then
    Exit;

  //Global variables
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('Clients')];
  AssignArrValue(V, Client);
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('Companies')];
  AssignArrValue(V, Company);
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('Payrolls')];
  AssignArrValue(V, Payroll);
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('ReportName')];
  V.Value := 'Payroll report';
  V:= Report.ReportForm.GlobalVarFunc.Variables.Add;
  V.Name := 'Company';
  V.VarType := rwvInteger;
  V.Value := 0;


  //Create Queries and Final Buffer
  FInFormParamList.Clear;
  Q1 := BuildQuery(MainQuery, 'Query');
  Buf := BuildBuffer(MainQuery, 'bMain', 'Tmp1');

  h := '';
  for i := 0 to Buf.Fields.Count - 1 do
  begin
    h := h + #13 + '     ' + Buf.Fields[i].FieldName;
    if i < Buf.Fields.Count - 1 then
      h := h + ',';
  end;
  h := 'INSERT INTO ' + Buf.TableName + ' (' + h + ')';
  Q1.SQL.Text := h + #13 + Q1.SQL.Text;


  Q1 := BufQuery(Buf, MainTable.Columns);
  Q2 := TrwQuery(Report.ReportForm.FindComponent('qMain'));
  Q2.SQL.Text := Q1.SQL.Text;

  for i := 0 to FObjList.Count-1 do
  begin
    TrwNoVisualComponent(FObjList[i]).Top := 20;
    TrwNoVisualComponent(FObjList[i]).Left := 20+i*40;
  end;
  Q1.Free;

  h :=       '  Company := Companies[0];';
  h := h+#13+'  Query1.BeforeOpen;';
  h := h+#13+'  Query1.Execute;';

  Report.Events['BeforePrint'].HandlersText := Report.Events['BeforePrint'].HandlersText + #13 + h;


  //Create Headers of Columns
  i := Report.ReportForm.AddBand(rbtHeader);
  FDefContainer := Report.ReportForm.Bands[i];
  FDefContainer.Height := 23;
  j := 0;
  k := 0;
  C := nil;
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    if Assigned(MainTable.Columns[i].Group) and MainTable.Columns[i].GroupHeader then
      Continue;
    j := j+MainTable.Columns[i].WidthPix;
    Inc(k);
  end;
  lWidth := Report.PageFormat.Width-Report.PageFormat.LeftMargin-Report.PageFormat.RightMargin;
  lWidth := Trunc(ConvertUnit(lWidth*100, utMMThousandths, utScreenPixels));
  if j < lWidth then
    dW := (lWidth-j) div k
  else
    dW := 0;

  j := 0;
  nW := 0;
  Col := nil;
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    if Assigned(MainTable.Columns[i].Group) and MainTable.Columns[i].GroupHeader then
      Continue;
    Col := MainTable.Columns[i];
    C := CreateLabel('lColHeader'+IntToStr(i), Col.Title, j, 0, Col.WidthPix+dW, 10, alLeft, taCenter);
    j := TrwLabel(C).Left+TrwLabel(C).Width+1;
    TrwLabel(C).Font.Style := TrwLabel(C).Font.Style+[fsBold];
    TrwLabel(C).WordWrap := Col.WrapTitle;
    TrwLabel(C).BoundLines := [rclLeft, rclTop, rclBottom];
    TrwLabel(C).Color := 15461355;
    TrwLabel(C).Transparent := False;
    TrwLabel(C).Layout := tlCenter;
    Col.FRealPixWidth := TrwLabel(C).Width;
    nW := nW + Col.FRealPixWidth;
  end;
  TrwLabel(C).BoundLines := TrwLabel(C).BoundLines + [rclRight];
  Col.FRealPixWidth := Col.FRealPixWidth + (lWidth-nW);
  TrwLabel(C).Width := Col.FRealPixWidth;


  //Create Columns
  i := Report.ReportForm.IndexOfBand(rbtDetail);
  FDefContainer := Report.ReportForm.Bands[i];
  FDefContainer.Height := 17;
  j := 0;
  m := 1;
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    if Assigned(MainTable.Columns[i].Group) and MainTable.Columns[i].GroupHeader then
      Continue;

    Col := MainTable.Columns[i];
    if MainTable.Columns[i].Calculated then
    begin
      C := CreateLabel('lCol'+IntToStr(i), 'lCol'+IntToStr(i), j, 0, Col.FRealPixWidth, 10, alLeft, Col.Alignment);

      h := '  lCol'+IntToStr(i);
      if Col.Format <> '' then
      begin
        TrwLabel(C).TextByValue := True;
        TrwLabel(C).Format := Col.Format;
        TrwLabel(C).Value := C.Name;
        h := h+'.Value';
      end
      else
        h := h+'.Text';

      h := h+' := ';
      for k := 0 to Col.Expression.Count-1 do
        if Pos('=', Col.Expression[k]) = 0 then
          h := h+Col.Expression[k]
        else
        begin
          Fld := MainQuery.FieldByAlias(Col.Expression.Names[k]);
          h := h+Report.MasterDataSource.Name+'.FieldValue('''+Fld.Alias+''')';
        end;

      C.Events['OnPrint'].HandlersText := h+';';
    end

    else
    begin
      DS := Report.MasterDataSource;
      C := CreateDBText('dbCol'+IntToStr(i), j, 0, Col.FRealPixWidth, 10, alLeft, Col.Alignment, DS, Col.DataField);
      if Assigned(MainQuery.Table.QBQuery) then
      begin
        SF := MainQuery.Table.QBQuery.ShowingFields.FieldByInternalName(Col.Field.FieldName);
        Attr := SF.Attribute;
        if Assigned(Attr) then
        begin
          TrwDBText(C).SubstID := TrwDataDictTable(Attr.Table).Name + '.' + Attr.Name;
          TrwDBText(C).Substitution := Col.SubstValue;
        end;
      end

      else
      begin
        TrwDBText(C).SubstID := TrwDataDictTable(Col.Field.Field.Table).Name + '.' + Col.Field.Field.Name;
        TrwDBText(C).Substitution := Col.SubstValue;
      end;

      TrwDBText(C).Format := Col.Format;
    end;
    if GenerateASCIIFile then
    begin
      TrwCustomText(C).ASCIIPrintLineSequence := 1;
      TrwCustomText(C).ASCIIPrintColSequence := m;
      TrwCustomText(C).ASCIISize := Col.ASCIISize;
    end;
    TrwCustomText(C).BoundLines := [rclLeft];
    j := TrwControl(C).Left+TrwControl(C).Width+1;
    Col.Control := TrwCustomText(C);
    Inc(m);
  end;


  //Create Groups
  i := Report.ReportForm.IndexOfBand(rbtDetail);
  Cont := Report.ReportForm.Bands[i];
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    Col := MainTable.Columns[i];
    if Assigned(Col.Group) then
    begin
      if Col.GroupHeader then
      begin
        FDefContainer := Col.Group.GroupHeader;

        C := CreateDBText('dbGrpHeader'+IntToStr(i), 0, 0, lWidth, FDefContainer.Height, alLeft,
          Col.Alignment, Report.MasterDataSource, Col.DataField);
        if Assigned(MainQuery.Table.QBQuery) then
        begin
          SF := MainQuery.Table.QBQuery.ShowingFields.FieldByInternalName(Col.Field.FieldName);
          Attr := SF.Attribute;
          if Assigned(Attr) then
          begin
            TrwDBText(C).SubstID := TrwDataDictTable(Attr.Table).Name + '.' + Attr.Name;
            TrwDBText(C).Substitution := Col.SubstValue;
          end
        end

        else
        begin
          TrwDBText(C).SubstID := TrwDataDictTable(Col.Field.Field.Table).Name + '.' + Col.Field.Field.Name;
          TrwDBText(C).Substitution := Col.SubstValue;
        end;

        TrwDBText(C).Format := Col.Format;
        TrwCustomText(C).Font.Style := TrwCustomText(C).Font.Style + [fsItalic];
      end
      else
      begin
        h := 'lColSumTotal'+IntToStr(i);
        j := Report.ReportForm.IndexOfBand(rbtSummary);
        if j <> -1 then
        begin
          C1 := CreateLabel(h, '', Col.Control.Left, 0, Col.Control.Width, 10, alLeft, taCenter);
          TrwLabel(C1).BoundLines := [rclLeft];
          TrwLabel(C1).Container := Report.ReportForm.Bands[j];
        end;
      end;

      if Col.GroupFooter then
        for j := 0 to MainTable.Columns.Count-1 do
        begin
          if Assigned(MainTable.Columns[j].Group) and MainTable.Columns[j].GroupHeader then
            Continue;

          h := 'lColSum'+IntToStr(j)+'Grp'+IntToStr(i);
          if MainTable.Columns[j].GroupSummary then
          begin
            Col.Group.GroupFooter.Height := 17;
            Col.Group.GroupFooter.BoundLines := [rclLeft, rclRight, rclTop, rclBottom];
            CreateGroupSumLabel(MainTable.Columns[j], h, TrwBand(Cont), Col.Group.GroupHeader.Events['OnPrint'],
              Col.Group.GroupFooter);
          end
          else
          begin
            C1 := CreateLabel(h, '', MainTable.Columns[j].Control.Left, 0, MainTable.Columns[j].Control.Width, 10, alLeft, taCenter);
            TrwLabel(C1).BoundLines := [rclLeft];
            TrwLabel(C1).Container := Col.Group.GroupFooter;
          end;
        end;
    end

    else
    begin
      h := 'lColSumTotal'+IntToStr(i);
      if Col.GroupSummary then
        CreateGroupSumLabel(Col, h, TrwBand(Cont), Report.Events['BeforePrint'],
          Report.ReportForm.Bands[Report.ReportForm.IndexOfBand(rbtSummary)])
      else
      begin
        j := Report.ReportForm.IndexOfBand(rbtSummary);
        if j <> -1 then
        begin
          C1 := CreateLabel(h, '', Col.Control.Left, 0, Col.Control.Width, 10, alLeft, taCenter);
          TrwLabel(C1).BoundLines := [rclLeft];
          TrwLabel(C1).Container := Report.ReportForm.Bands[j];
        end;
      end;
    end;
  end;

  i := Report.ReportForm.AddBand(rbtFooter);
  FDefContainer := Report.ReportForm.Bands[i];
  FDefContainer.Height := 1;
  TrwFramedContainer(FDefContainer).BoundLines := [rclTop];

  RealignBands;

  CreateInputForm;
end;


procedure TrwWizInfPayroll.CreateReportObject;
begin
  FReport := TrwReport(SystemLibComponents[SystemLibComponents.IndexOf('TrwlPayrollReport')].CreateComp(nil, True));
end;


procedure TrwWizInfPayroll.CustomQueryCondition(ATable: TrwWizardQueryTable; var sFrom, sWhere: string);
begin
  inherited;
  if not Assigned(ATable.QBQuery) and (ATable.Table.Fields.FieldByName('PR_NBR') <> nil) then
  begin
    if Pos(', tmp_pr tpr', sFrom) = 0 then
      sFrom := sFrom +', tmp_pr tpr';
    sWhere := sWhere+' and'+#13+'   '+ATable.Alias+'.PR_NBR = tpr.PR_NBR';
  end;
end;


class function TrwWizInfPayroll.FormEditor: TClass;
begin
  Result := TrwWizardPrEdit;
end;


class function TrwWizInfPayroll.WizardName: string;
begin
  Result := 'Payroll report';
end;


procedure TrwWizardPrEdit.InitPayrollList;
var
  Q: TrwLQQuery;
  Pars: array [0..0] of Variant;
  Res: Variant;
begin
  Busy;
  clPayrolls.Clear;
  Q := TrwLQQuery.Create;
  try
    Pars[0] := FClient;
    RWEngineExt.AppAdapter.ExecuteExternalFunction(1, Pars, Res); // Open Client
    Q.SQL.Text := 'SELECT p.check_date, p.run_number, p.pr_nbr FROM Pr("now") p WHERE p.co_nbr = :co_nbr ORDER BY 1, 2';
    Q.Params[0].Value := FCompany;

    Q.OpenSQL;
    while not Q.Eof do
    begin
      clPayrolls.Items.AddObject(Q.Fields[0].AsString+ '-' + Q.Fields[1].AsString, Pointer(Q.Fields[2].AsInteger));
      Q.Next;
    end;
    Q.Close;
  finally
    Q.Free;
    Ready;
  end;
end;


procedure TrwWizardPrEdit.cbClientsCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if Modified then
    InitPayrollList;
end;

procedure TrwWizardPrEdit.Initialize;
var
  L: TStringList;
  i, j: Integer;
begin
  inherited;
  InitPayrollList;

  L := TStringList.Create;
  try
    L.Text := TrwWizInfPayroll(WizardInfo).Payroll;
    for i := 0 to L.Count-1 do
    begin
      j := clPayrolls.Items.IndexOfObject(Pointer(StrToInt(L[i])));
      if j <> -1 then
        clPayrolls.Checked[j] := True;
    end;
  finally
    L.Free;
  end;
end;


procedure TrwWizardPrEdit.Finalize;
var
  i: Integer;
begin
  inherited;

  TrwWizInfPayroll(WizardInfo).Payroll := '';
  for i := 0 to clPayrolls.Items.Count-1 do
    if clPayrolls.Checked[i] then
    begin
      if TrwWizInfPayroll(WizardInfo).Payroll <> '' then
        TrwWizInfPayroll(WizardInfo).Payroll := TrwWizInfPayroll(WizardInfo).Payroll + #13;
      TrwWizInfPayroll(WizardInfo).Payroll := TrwWizInfPayroll(WizardInfo).Payroll + IntToStr(Integer(clPayrolls.Items.Objects[i]));
    end;
end;


procedure TrwWizardPrEdit.btnAddTblClick(Sender: TObject);
var
  Tbl: TrwWizardQueryTable;
  f: string;
begin
  if not Assigned(lvTables.Selected) then
  begin
    Tbl := WizardInfo.MainQuery.Table;
    f := 'PR_NBR';
  end
  else
  begin
    Tbl := TrwWizardQueryTable(lvTables.Selected.Data);
    f := '';
  end;

  if AddTable(Tbl, f) then
   InitializeQuery;
end;


initialization
  RegisterClass(TrwWizInfPayroll);
  WizardsLst.Add(TrwWizInfPayroll);

finalization
  UnRegisterClass(TrwWizInfPayroll);
  WizardsLst.Remove(TrwWizInfPayroll);

end.
