// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWizardInfo;

interface

uses Windows, Classes, Sysutils, rwDataDictionary, rwReportControls, rwDB,
     rwCommonClasses, Graphics, rwTypes, Controls, rwReport, rwBasicClasses,
     DB, Forms, StdCtrls, rwInputFormControls, rwQBInfo, TypInfo, rwUtils, Variants,
     sbAPI, Contnrs, Dialogs, rwBasicUtils, rwEngineTypes, rwGraphics;

type
  TrwWizCollection = class (TCollection)
  public
    procedure Assign(Source: TPersistent); override;
  end;


  TrwWizardQueryTableParam = class (TCollectionItem)
  private
    FParamName: string;
    FParam: TrwDataDictParam;
    FParamValue: string;
    FForInputForm: Boolean;
    FEmptyParam: Boolean;
    procedure SetParamName(const Value: string);
  public
    property Param: TrwDataDictParam read FParam;
    constructor Create(Collection: TCollection); override;
    procedure Assign(Source: TPersistent); override;
  published
    property ParamName: string read FParamName write SetParamName;
    property ParamValue: string read FParamValue write FParamValue;
    property ForInputForm: Boolean read FForInputForm write FForInputForm;
    property EmptyParam: Boolean read FEmptyParam write FEmptyParam;
  end;

  TrwWizardQueryTable = class;

  TrwWizardQueryTableParams = class (TrwWizCollection)
  private
    FQueryTable: TrwWizardQueryTable;
    function  GetItem(Index: Integer): TrwWizardQueryTableParam;
    procedure SetItem(Index: Integer; Value: TrwWizardQueryTableParam);
  public
    function FindParam(AParamName: string): TrwWizardQueryTableParam;
    function Add: TrwWizardQueryTableParam;
    property Items[Index: Integer]: TrwWizardQueryTableParam read GetItem write SetItem; default;
    property QueryTable: TrwWizardQueryTable read FQueryTable;
  end;


  TrwWizardQueryTableField = class (TCollectionItem)
  private
    FFieldName: string;
    FAlias: string;
    FNote: string;
    FField: TrwDataDictField;
    FDataSet: TrwDataSet;
    FLookUpTable: TrwWizardQueryTable;
    procedure SetFieldName(const Value: string);
    procedure SetLookUpTable(const Value: TrwWizardQueryTable);
  public
    property  Field: TrwDataDictField read FField;
    property  DataSet: TrwDataSet read FDataSet;
    procedure Assign(Source: TPersistent); override;
    function  Path: String;
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
  published
    property Alias: string read FAlias write FAlias;
    property FieldName: string read FFieldName write SetFieldName;
    property Note: string read FNote write FNote;
    property LookUpTable: TrwWizardQueryTable read FLookUpTable write SetLookUpTable;
  end;


  TrwWizardQueryTableFields = class (TrwWizCollection)
  private
    FQueryTable: TrwWizardQueryTable;
    function  GetItem(Index: Integer): TrwWizardQueryTableField;
    procedure SetItem(Index: Integer; Value: TrwWizardQueryTableField);
  public
    function Add: TrwWizardQueryTableField;
    property Items[Index: Integer]: TrwWizardQueryTableField read GetItem write SetItem; default;
    property QueryTable: TrwWizardQueryTable read FQueryTable;
    function FindField(AField: TrwDataDictField): Integer;
    function RootTable: TrwWizardQueryTable;
  end;


  TrwWizardQueryTblFltCond = class (TCollectionItem)
  private
    FFieldName: string;
    FField: TrwDataDictField;
    FAndOr: TrwQBFilterNodeType;
    FInverse: Boolean;
    FOperation: string;
    FExpression: string;
    FValue1: String;
    FValue2: String;
    FDisplayExpression: string;
    procedure SetFieldName(const Value: string);
  public
    property  Field: TrwDataDictField read FField;
    procedure Assign(Source: TPersistent); override;
  published
    property Inverse: Boolean read FInverse write FInverse;
    property FieldName: string read FFieldName write SetFieldName;
    property AndOr: TrwQBFilterNodeType read FAndOr write FAndOr;
    property Operation: string read FOperation write FOperation;
    property Value1: string read FValue1 write FValue1;
    property Value2: string read FValue2 write FValue2;
    property DisplayExpression: string read FDisplayExpression write FDisplayExpression;
    property Expression: string read FExpression write FExpression;
  end;


  TrwWizardQueryTblFilter = class (TrwWizCollection)
  private
    FQueryTable: TrwWizardQueryTable;
    function  GetItem(Index: Integer): TrwWizardQueryTblFltCond;
    procedure SetItem(Index: Integer; Value: TrwWizardQueryTblFltCond);
  public
    property Items[Index: Integer]: TrwWizardQueryTblFltCond read GetItem write SetItem; default;
    property QueryTable: TrwWizardQueryTable read FQueryTable write FQueryTable;
    function Add: TrwWizardQueryTblFltCond;
  end;


  TrwWizardQueryTables = class;

  TrwWizardQueryTable = class (TCollectionItem)
  private
    FTables: TrwWizardQueryTables;
    FTable: TrwDataDictTable;
    FTableName: string;
    FParams: TrwWizardQueryTableParams;
    FFields: TrwWizardQueryTableFields;
    FOuter: Boolean;
    FFilter: TrwWizardQueryTblFilter;
    FLookUpField: TrwWizardQueryTableField;
    FAlias: String;
    FQBQuery: TrwQBQuery;
    procedure SetTableName(const Value: string);
    procedure SetFields(const Value: TrwWizardQueryTableFields);
    procedure SetParams(const Value: TrwWizardQueryTableParams);
    procedure SetFilter(const Value: TrwWizardQueryTblFilter);
    procedure SetTables(const Value: TrwWizardQueryTables);
    function  QBQueryIsNotEmpty: Boolean;
    procedure SetQBQuery(const Value: TrwQBQuery);
    procedure OnAssignQBQuery(Sender: TObject);

  public
    property Table: TrwDataDictTable read FTable;
    procedure Assign(Source: TPersistent); override;
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure InitQBQuery;

  published
    property Alias: string read FAlias write FAlias;
    property TableName: string read FTableName write SetTableName;
    property Params: TrwWizardQueryTableParams read FParams write SetParams;
    property Fields: TrwWizardQueryTableFields read FFields write SetFields;
    property Outer: Boolean read FOuter write FOuter;
    property Filter: TrwWizardQueryTblFilter read FFilter write SetFilter;
    property Tables: TrwWizardQueryTables read FTables write SetTables;
    property QBQuery: TrwQBQuery read FQBQuery write SetQBQuery stored QBQueryIsNotEmpty;
  end;

  TrwWizardQuery = class;

  TrwWizardQueryTables = class (TrwWizCollection)
  private
    FWizQuery: TrwWizardQuery;
    function  GetItem(Index: Integer): TrwWizardQueryTable;
    procedure SetItem(Index: Integer; Value: TrwWizardQueryTable);
  public
    function Add: TrwWizardQueryTable;
    property Items[Index: Integer]: TrwWizardQueryTable read GetItem write SetItem; default;
    property WizQuery: TrwWizardQuery read FWizQuery;
  end;

  TrwWizardTable = class;

  TrwWizardQuery = class(TPersistent)
  private
    FTable: TrwWizardQueryTable;
    FWizardTable: TrwWizardTable;
    procedure SetTable(const Value: TrwWizardQueryTable);
    function  GetUniqueFieldAlias(Name: string): string;
    function  GetUniqueTableAlias(Name: string): string;
    procedure SyncReferences;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function FieldByAlias(FldName: string): TrwWizardQueryTableField;
    function TableByAlias(TblName: string): TrwWizardQueryTable;
  published
    property Table: TrwWizardQueryTable read FTable write SetTable;
  end;


              {Columns}

  TrwWizTblColumn = class (TCollectionItem)
  private
    FTitle: string;
    FCalculated: Boolean;
    FSummarized: Boolean;
    FWrapTitle: Boolean;
    FWidth: Real;
    FFormat: string;
    FAlignment: TAlignment;
    FDataField: string;
    FSubstValue: Boolean;
    FField: TrwWizardQueryTableField;
    FExpression: TStringList;
    FSortNumber: Integer;
    FGroup: TrwGroup;
    FGroupNumber: Integer;
    FGroupSummary: Boolean;
    FGroupHeader: Boolean;
    FGroupFooter: Boolean;
    FGroupPageBreak: Boolean;
    FControl: TrwCustomText;
    FASCIISize: Integer;
    procedure SetDataField(const Value: string);
    procedure SetExpression(const Value: TStringList);
  public
    FRealPixWidth: Integer;
    property  Field: TrwWizardQueryTableField read FField;
    property  Group: TrwGroup read FGroup write FGroup;
    property  Control: TrwCustomText read FControl write FControl;
    procedure Assign(Source: TPersistent); override;
    function  WidthPix: Integer;
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
  published
    property Title: string read FTitle write FTitle;
    property Calculated: Boolean read FCalculated write FCalculated;
    property Summarized: Boolean read FSummarized write FSummarized;
    property WrapTitle: Boolean read FWrapTitle write FWrapTitle;
    property Width: Real read FWidth write FWidth;
    property Format: string read FFormat write FFormat;
    property Alignment: TAlignment read FAlignment write FAlignment;
    property DataField: string read FDataField write SetDataField;
    property SubstValue: Boolean read FSubstValue write FSubstValue;
    property Expression: TStringList read FExpression write SetExpression;
    property SortNumber: Integer read FSortNumber write FSortNumber;
    property GroupNumber: Integer read FGroupNumber write FGroupNumber;
    property GroupSummary: Boolean read FGroupSummary write FGroupSummary;
    property GroupPageBreak: Boolean read FGroupPageBreak write FGroupPageBreak;
    property GroupHeader: Boolean read FGroupHeader write FGroupHeader;
    property GroupFooter: Boolean read FGroupFooter write FGroupFooter;
    property ASCIISize: Integer read FASCIISize write FASCIISize;
  end;


  TrwWizTblColumns = class (TrwWizCollection)
  private
    FTable: TrwWizardTable;
    function  GetItem(Index: Integer): TrwWizTblColumn;
    procedure SetItem(Index: Integer; Value: TrwWizTblColumn);
  public
    function Add: TrwWizTblColumn;
    function FindColumnByFieldName(AFieldName: String): Integer;
    property Items[Index: Integer]: TrwWizTblColumn read GetItem write SetItem; default;
  end;


  TrwWizardTable = class(TPersistent)
  private
    FColumns: TrwWizTblColumns;
    FQuery: TrwWizardQuery;
    FCountRecords: Boolean;
    procedure SetColumns(const Value: TrwWizTblColumns);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Columns: TrwWizTblColumns read FColumns write SetColumns;
    property CountRecords: Boolean read FCountRecords write FCountRecords;
  end;


  {Structure for storing wizard's information}

  TrwWizardInfo = class(TComponent)
  private
    FMainQuery: TrwWizardQuery;
    FMainTable: TrwWizardTable;
    FClient: String;
    FCompany: String;

    FGenerateASCIIFile: Boolean;
    FASCIIDelimiter: string;
    FDescription: WideString;
    FSurroundChar: String;

    procedure SetMainQuery(const Value: TrwWizardQuery);
    procedure SetMainTable(const Value: TrwWizardTable);
    procedure SetPaper(const Value: TrwPage);
    function  GetPaper: TrwPage;

  protected
    FReport: TrwReport;
    FObjList: TList;
    FInFormParamList: TStringList;
    FDefOwner: TrwComponent;
    FDefContainer: TrwControl;
    FDefFont: TFont;
    FDefColor: TColor;
    FDefBoundsLines: TrwSetColumnLines;

    procedure Loaded; override;
    procedure CreateReportObject; virtual;
    function  CreateLabel(AName, AText: String; ALeft, ATop, AWidth, AHeight: Integer;
                          AAlign: TAlign; AAlignment: TAlignment): TrwLabel;
    function  CreateDBText(AName: String; ALeft, ATop, AWidth, AHeight: Integer;
                           AAlign: TAlign; AAlignment: TAlignment; ADataSet: TrwDataSet;
                           AFieldName: String): TrwDBText;
    function  CreateLibComponent(AName, ALibName: String; ALeft, ATop: Integer; AAlign: TAlign):TrwComponent;
    function  CreateLabelIf(AName, AText: String; ALeft, ATop, AWidth, AHeight: Integer): TrwText;

    function  CreateQuery(AName: String; ALeft, ATop: Integer): TrwQuery;
    function  BuildQuery(AQuery: TrwWizardQuery; AName: string): TrwQuery;
    function  BuildBuffer(AQuery: TrwWizardQuery; AName: string; ATableName: String): TrwBuffer;
    function  AlignBand(ABand: TrwBand; ATop: Integer): Integer;
    procedure RealignBands;
    function  ReorderBand(ABandType: TrwBandType; ATop: Integer): Integer;
    procedure CreateGroupSumLabel(AColumn: TrwWizTblColumn; AName: string; ADetailBand: TrwBand;
      AClearEvent: TrwEvent; AGroupBandF: TrwBand);
   function BufQuery(ABuf: TrwBuffer; AColumns: TrwWizTblColumns): TrwQuery;
   procedure CreateInputForm;
   function  CheckQueryParam(AQuery: TrwQuery; AParName: string; AParValue: string; var AEventHandler: String): Boolean;
   procedure CheckQueryParams(AQuery: TrwQuery; var AEventHandler: String); virtual;
   procedure CustomQueryCondition(ATable: TrwWizardQueryTable; var sFrom, sWhere: string); virtual;

  public
    property   Report: TrwReport read FReport;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure  CreateReport; virtual;
    procedure AssignArrValue(AVar: TrwGlobalVariable; Value: Variant);    
    procedure  Assign(Source: TPersistent); override;
    class function FormEditor: TClass; virtual;
    class function WizardName: string; virtual;

  published
    property MainQuery: TrwWizardQuery read FMainQuery write SetMainQuery;
    property MainTable: TrwWizardTable read FMainTable write SetMainTable;
    property Client: String read FClient write FClient;
    property Company: String read FCompany write FCompany;
    property GenerateASCIIFile: Boolean read FGenerateASCIIFile write FGenerateASCIIFile;
    property ASCIIDelimiter: string read FASCIIDelimiter write FASCIIDelimiter;
    property SurroundChar: String read FSurroundChar write FSurroundChar;
    property Description: WideString read FDescription write FDescription;
    property Paper: TrwPage read GetPaper write SetPaper;
  end;

  TrwWizardInfoClass = class of TrwWizardInfo;

  function WizardsLst: TClassList;
  function RunWizard(AWizardNum: Integer; AResStream: TStream): Boolean;
  function WizardsList: String;

implementation

uses rwWizardFrm, rwCustomDataDictionary;

var
  FWizardsList: TClassList;

function WizardsLst: TClassList;
begin
  if not Assigned(FWizardsList) then
    FWizardsList := TClassList.Create;
  Result := FWizardsList;
end;


procedure ReCreateReport(AResStream: TStream);
var
  WI: TrwWizardInfo;
begin
  AResStream.Position := 0;
  WI := TrwWizardInfo(AResStream.ReadComponent(nil));
  try
    WI.CreateReport;
    AResStream.Size := 0;
    AResStream.Position := 0;
    WI.Report.WizardInfo.Clear;
    WI.Report.WizardInfo.WriteComponent(WI);
    WI.Report.SaveToStream(AResStream);
  finally
    WI.Free;
  end;
end;


function RunWizard(AWizardNum: Integer; AResStream: TStream): Boolean;
var
  Frm: TrwWizard;
  W: TrwWizardInfo;
begin
  Result := False;

  if (AResStream.Size > 0) and
     (MessageDlg('Show stored wizard''s information?', mtCustom, [mbYes, mbNo], 0) = mrYes) then
  begin
    Frm := nil;
    AResStream.Position := 0;
    W := TrwWizardInfo(AResStream.ReadComponent(nil));
    try
      Frm := TrwWizardClass(W.FormEditor).Create(Application);
      Frm.WizardInfo := W;
      if Frm.ShowModal = mrOK then
      begin
        AResStream.Size := 0;
        AResStream.Position := 0;
        Frm.WizardInfo.Report.SaveToStream(AResStream);
        Result := True;
      end;
    finally
      Frm.Free;
    end;
  end

  else
  begin
{    Frm := TrwWizardInfoEdit.Create(Application);
    try
      TrwWizardInfoEdit(Frm).WizardInfo := TrwWizInfOneTable.Create(nil);
      if Frm.ShowModal = mrOK then
      begin
        AResStream.Size := 0;
        AResStream.Position := 0;
        TrwWizardInfoEdit(Frm).WizardInfo.Report.SaveToStream(AResStream);
        Result := True;
      end;
    finally
      Frm.Free;
    end;
}
    Frm := TrwWizardClass(TrwWizardInfoClass(WizardsLst[AWizardNum]).FormEditor).Create(Application);
    Frm.WizardInfo := TrwWizardInfoClass(WizardsLst[AWizardNum]).Create(nil);
    try
      if Frm.ShowModal = mrOK then
      begin
        AResStream.Size := 0;
        AResStream.Position := 0;
        TrwWizard(Frm).WizardInfo.Report.SaveToStream(AResStream);
        Result := True;
      end;
    finally
      Frm.Free;
    end;
  end;
end;


function WizardsList: String;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to WizardsLst.Count-1 do
    Result := Result+#13+TrwWizardInfoClass(WizardsLst[i]).WizardName;
  Delete(Result, 1, 1);
end;




{ TrwWizardQueryTableParam }

procedure TrwWizardQueryTableParam.Assign(Source: TPersistent);
begin
  ParamName := TrwWizardQueryTableParam(Source).ParamName;
  ParamValue := TrwWizardQueryTableParam(Source).ParamValue;
  ForInputForm := TrwWizardQueryTableParam(Source).ForInputForm;
  EmptyParam := TrwWizardQueryTableParam(Source).EmptyParam;
end;

constructor TrwWizardQueryTableParam.Create(Collection: TCollection);
begin
  inherited;

  FParamValue := '';
  FForInputForm := False;
  FEmptyParam := False;
end;

procedure TrwWizardQueryTableParam.SetParamName(const Value: string);
begin
  FParamName := Value;
  FParam := TrwDataDictParam(TrwWizardQueryTableParams(Collection).QueryTable.Table.Params.ParamByName(FParamName));

  if not Assigned(FParam) then
    if AnsiSameText(Value, 'MY_DATE') or AnsiSameText(Value, 'AsOfDate') then
    begin
      FParamName := 'Effective_Date';
      FParam := TrwDataDictParam(TrwWizardQueryTableParams(Collection).QueryTable.Table.Params.ParamByName(FParamName));
      Assert(Assigned(FParam),  'Cannot create parameter ' + Value);
    end;
end;


{ TrwWizardQueryTableParams }

function TrwWizardQueryTableParams.Add: TrwWizardQueryTableParam;
begin
  Result := TrwWizardQueryTableParam(inherited Add);
end;

function TrwWizardQueryTableParams.FindParam(AParamName: string): TrwWizardQueryTableParam;
var
  i: Integer;
begin
  Result := nil;
  AParamName := AnsiUpperCase(AParamName);
  for i := 0 to Count-1 do
    if AnsiUpperCase(Items[i].ParamName) = AParamName then
    begin
      Result := Items[i];
      break;
    end;
end;

function TrwWizardQueryTableParams.GetItem(Index: Integer): TrwWizardQueryTableParam;
begin
  Result := TrwWizardQueryTableParam(inherited items[Index]);
end;

procedure TrwWizardQueryTableParams.SetItem(Index: Integer; Value: TrwWizardQueryTableParam);
begin
  TrwWizardQueryTableParam(inherited items[Index]).Assign(Value);
end;



{ TrwWizardQueryTableField }

constructor TrwWizardQueryTableField.Create(Collection: TCollection);
begin
  inherited;
  FLookUpTable := TrwWizardQueryTable.Create(nil);
  FLookUpTable.FLookUpField := Self;
  FLookUpTable.Tables.FWizQuery := TrwWizardQueryTableFields(Collection).QueryTable.Tables.FWizQuery;
end;

destructor TrwWizardQueryTableField.Destroy;
var i: Integer;
begin
  if Assigned(FLookUpTable.Tables.WizQuery.FWizardTable) then
    with FLookUpTable.Tables.WizQuery.FWizardTable do
    begin
      i := 0;
      while i < Columns.Count do
        if Assigned(Columns[i].Field) and (Columns[i].Field = Self) then
          Columns[i].Free
        else
          Inc(i);
    end;

  FLookUpTable.Free;
  inherited;
end;

procedure TrwWizardQueryTableField.Assign(Source: TPersistent);
begin
  Alias := TrwWizardQueryTableField(Source).Alias;
  FieldName := TrwWizardQueryTableField(Source).FieldName;
  Note := TrwWizardQueryTableField(Source).Note;
  LookUpTable := TrwWizardQueryTableField(Source).LookUpTable;
end;

procedure TrwWizardQueryTableField.SetFieldName(const Value: string);
begin
  FFieldName := Value;

  if not TrwWizardQueryTableFields(Collection).QueryTable.QBQueryIsNotEmpty then
  begin
    FField := TrwDataDictField(TrwWizardQueryTableFields(Collection).QueryTable.Table.Fields.FieldByName(Value));

    if FAlias = '' then
      FAlias := TrwWizardQueryTableFields(Collection).QueryTable.Tables.FWizQuery.
        GetUniqueFieldAlias(Value);
  end;
end;

procedure TrwWizardQueryTableField.SetLookUpTable(const Value: TrwWizardQueryTable);
begin
  FLookUpTable.Assign(Value);
end;

function TrwWizardQueryTableField.Path: String;
var
  h: String;
begin
  if not Assigned(Field) then
  begin
    Result := '';
    Exit;
  end;
  Result := Field.DisplayName;
  if not Assigned(TrwWizardQueryTableFields(Collection).QueryTable.FLookUpField) then
    Result := TrwWizardQueryTableFields(Collection).QueryTable.Table.DisplayName+'\'+Result
  else
  begin
    h := TrwWizardQueryTableFields(Collection).QueryTable.FLookUpField.Path;
    if h <> '' then
      Result := h+'\'+Result;
  end;
end;


{ TrwWizardQueryTableFields }

function TrwWizardQueryTableFields.Add: TrwWizardQueryTableField;
begin
  Result := TrwWizardQueryTableField(inherited Add);
end;

function TrwWizardQueryTableFields.FindField(AField: TrwDataDictField): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to Count-1 do
    if TrwWizardQueryTableField(Items[i]).Field = AField then
    begin
      Result := i;
      break;
    end;
end;

function TrwWizardQueryTableFields.GetItem(Index: Integer): TrwWizardQueryTableField;
begin
  Result := TrwWizardQueryTableField(inherited items[Index]);
end;

function TrwWizardQueryTableFields.RootTable: TrwWizardQueryTable;
begin
  Result := QueryTable;
  if Assigned(Result.FLookUpField) then
    Result := TrwWizardQueryTableFields(Result.FLookUpField.Collection).RootTable;
end;

procedure TrwWizardQueryTableFields.SetItem(Index: Integer; Value: TrwWizardQueryTableField);
begin
  TrwWizardQueryTableField(inherited items[Index]).Assign(Value);
end;


{ TrwWizardQueryTable }

constructor TrwWizardQueryTable.Create(Collection: TCollection);
begin
  inherited;
  FParams := TrwWizardQueryTableParams.Create(TrwWizardQueryTableParam);
  FParams.FQueryTable := Self;
  FFields := TrwWizardQueryTableFields.Create(TrwWizardQueryTableField);
  FFields.FQueryTable := Self;
  FFilter := TrwWizardQueryTblFilter.Create(TrwWizardQueryTblFltCond);
  FFilter.FQueryTable := Self;
  FTables := TrwWizardQueryTables.Create(TrwWizardQueryTable);
end;

destructor TrwWizardQueryTable.Destroy;
begin
  FParams.Free;
  FFields.Free;
  FFilter.Free;
  FTables.Free;
  if Assigned(FQBQuery) then
    FQBQuery.Free;
  inherited;
end;

procedure TrwWizardQueryTable.Assign(Source: TPersistent);
begin
  if QBQueryIsNotEmpty then
    QBQuery := TrwWizardQueryTable(Source).QBQuery
    
  else
  begin
    Alias := TrwWizardQueryTable(Source).Alias;
    TableName := TrwWizardQueryTable(Source).TableName;
    Params := TrwWizardQueryTable(Source).Params;
    Fields := TrwWizardQueryTable(Source).Fields;
    Outer := TrwWizardQueryTable(Source).Outer;
    Filter := TrwWizardQueryTable(Source).Filter;
    Tables := TrwWizardQueryTable(Source).Tables;
  end;
end;

procedure TrwWizardQueryTable.SetTableName(const Value: string);
begin
  if QBQueryIsNotEmpty then
  begin
    if FAlias = '' then
      FAlias := Tables.FWizQuery.GetUniqueTableAlias('T');
    Exit;
  end;
    
  FTable := TrwDataDictTable(DataDictionary.Tables.TableByName(Value));
  if FTable <> nil then
  begin
    FTableName := Value;
    if FAlias = '' then
      FAlias := Tables.FWizQuery.GetUniqueTableAlias('T');
  end
  else
  begin
    FParams.Clear;
    FFields.Clear;
    FFilter.Clear;
    FTables.Clear;
    FTableName := '';
    FAlias := '';
  end;
end;

procedure TrwWizardQueryTable.SetFields(const Value: TrwWizardQueryTableFields);
begin
  FFields.Assign(Value);
end;

procedure TrwWizardQueryTable.SetParams(const Value: TrwWizardQueryTableParams);
begin
  FParams.Assign(Value);
end;

procedure TrwWizardQueryTable.SetFilter(
  const Value: TrwWizardQueryTblFilter);
begin
  FFilter.Assign(Value);
end;

procedure TrwWizardQueryTable.SetTables(const Value: TrwWizardQueryTables);
begin
  FTables.Assign(Value);
end;

function TrwWizardQueryTable.QBQueryIsNotEmpty: Boolean;
begin
  Result := Assigned(FQBQuery);
end;

procedure TrwWizardQueryTable.SetQBQuery(const Value: TrwQBQuery);
begin
  FQBQuery := Value;
end;

procedure TrwWizardQueryTable.InitQBQuery;
begin
  if Assigned(FQBQuery) then
    FQBQuery.Free;
  FQBQuery := TrwQBQuery.Create(nil);
  FQBQuery.OnAssign := OnAssignQBQuery;
end;

procedure TrwWizardQueryTable.OnAssignQBQuery(Sender: TObject);
var
  i, j: Integer;
  F: TrwWizardQueryTableField;
  fl: Boolean;
begin
  TableName := FQBQuery.Description;


  j := 0;
  while j < Fields.Count do
    if not Assigned(FQBQuery.ShowingFields.FieldByInternalName(Fields[j].FieldName)) then
      Fields[j].Free
    else
      Inc(j);


  for i := 0 to FQBQuery.ShowingFields.Count -1 do
  begin
    fl := False;
    for j := 0 to Fields.Count - 1 do
      if AnsiSameText(Fields[j].FieldName, FQBQuery.ShowingFields[i].InternalName) then
      begin
        fl := True;
        break;
      end;

    if not fl then
    begin
      F := Fields.Add;
      F.FieldName := FQBQuery.ShowingFields[i].InternalName;
      F.FAlias := Tables.FWizQuery.GetUniqueFieldAlias(FQBQuery.ShowingFields[i].GetFieldName(False, False));
    end;
  end;
end;


{ TrwWizardQueryTables }

function TrwWizardQueryTables.Add: TrwWizardQueryTable;
begin
  Result := TrwWizardQueryTable(inherited Add);
  Result.Tables.FWizQuery := FWizQuery;
end;

function TrwWizardQueryTables.GetItem(Index: Integer): TrwWizardQueryTable;
begin
  Result := TrwWizardQueryTable(inherited items[Index]);
end;

procedure TrwWizardQueryTables.SetItem(Index: Integer;
  Value: TrwWizardQueryTable);
begin
  TrwWizardQueryTable(inherited items[Index]).Assign(Value);
end;

{ TrwWizardInfo }

constructor TrwWizardInfo.Create(AOwner: TComponent);
begin
  inherited;
  FObjList := TList.Create;
  FInFormParamList := TStringList.Create;
  FMainQuery := TrwWizardQuery.Create;
  FMainTable := TrwWizardTable.Create;
  FMainTable.FQuery := FMainQuery;
  FMainQuery.FWizardTable := FMainTable;
  FGenerateASCIIFile := False;
  FASCIIDelimiter := '';
  FSurroundChar := '';
  FClient := '0';
  FCompany := '0';

  FDefFont := TFont.Create;
  FDefFont.Name := 'Arial';
  FDefFont.Size := 8;
  FDefFont.Style := [];
  
  FDefColor := clWhite;

  CreateReportObject;
end;

destructor TrwWizardInfo.Destroy;
begin
  FInFormParamList.Free;
  FObjList.Free;
  FMainQuery.Free;
  FMainTable.Free;
  FReport.Free;
  FDefFont.Free;
  inherited;
end;

procedure TrwWizardInfo.SetMainQuery(const Value: TrwWizardQuery);
begin
  FMainQuery.Assign(Value);
end;


function TrwWizardInfo.CreateLabel(AName, AText: String; ALeft, ATop, AWidth, AHeight: Integer;
  AAlign: TAlign; AAlignment: TAlignment): TrwLabel;
begin
  Result := TrwLabel.Create(FDefOwner);
  with Result do
  begin
    if AName = '' then
      Name := GetUniqueNameForComponent(Result)
    else
      Name := AName;

    Font.Assign(FDefFont);
    Color := FDefColor;
    BoundLines := FDefBoundsLines;
    Container := TrwPrintableContainer(FDefContainer);
    Text := AText;

    Left := ALeft;
    Top := ATop;
    Width := AWidth;
    Height := AHeight;

    Align := AAlign;
    Alignment := AAlignment;
    Transparent := True;
  end;
end;


function TrwWizardInfo.CreateDBText(AName: String; ALeft, ATop, AWidth, AHeight: Integer;
  AAlign: TAlign; AAlignment: TAlignment; ADataSet: TrwDataSet; AFieldName: String): TrwDBText;
begin
  Result := TrwDBText.Create(FDefOwner);
  with Result do
  begin
    if AName = '' then
      Name := GetUniqueNameForComponent(Result)
    else
      Name := AName;

    Font.Assign(FDefFont);
    Color := FDefColor;
    BoundLines := FDefBoundsLines;
    Container := TrwPrintableContainer(FDefContainer);

    Left := ALeft;
    Top := ATop;
    Width := AWidth;
    Height := AHeight;

    Align := AAlign;
    Alignment := AAlignment;

    DataSource := ADataSet;
    DataField := AFieldName;
    Transparent := True;
  end;
end;


function TrwWizardInfo.CreateLibComponent(AName, ALibName: String; ALeft,
  ATop: Integer; AAlign: TAlign): TrwComponent;
var
  i: Integer;
begin
  i := SystemLibComponents.IndexOf(ALibName);
  Result := SystemLibComponents[i].CreateComp(nil, True);
  FDefOwner.InsertComponent(Result);
  if AName = '' then
    Result.Name := GetUniqueNameForComponent(Result)
  else
    Result.Name := AName;

  if Result.InheritsFrom(TrwControl) then
  begin
    TrwControl(Result).Align := AAlign;
    if Result.InheritsFrom(TrwChildComponent) then
      TrwChildComponent(Result).Container := TrwPrintableContainer(FDefContainer);
    TrwControl(Result).Left := ALeft;
    TrwControl(Result).Top := ATop;
  end
  else if Result.InheritsFrom(TrwNoVisualComponent) then
  begin
    TrwNoVisualComponent(Result).Left := ALeft;
    TrwNoVisualComponent(Result).Top := ATop;
  end;
end;


function TrwWizardInfo.CreateLabelIf(AName, AText: String; ALeft, ATop, AWidth, AHeight: Integer): TrwText;
begin
  Result := TrwText.Create(FReport.InputForm);
  with Result do
  begin
    if AName = '' then
      Name := GetUniqueNameForComponent(Result)
    else
      Name := AName;

    Font.Assign(FDefFont);
    Color := FDefColor;
    Parent := TrwWinFormControl(FDefContainer);
    Text := AText;

    Left := ALeft;
    Top := ATop;
    Width := AWidth;
    Height := AHeight;
  end;
end;


function TrwWizardInfo.CreateQuery(AName: String; ALeft, ATop: Integer): TrwQuery;
begin
  Result := TrwQuery.Create(FDefOwner);
  with Result do
  begin
    if AName = '' then
      Name := GetUniqueNameForComponent(Result)
    else
      Name := AName;

    Left := ALeft;
    Top := ATop;
  end;
end;


procedure TrwWizardInfo.SetMainTable(const Value: TrwWizardTable);
begin
  FMainTable.Assign(Value);
end;


function TrwWizardInfo.BuildQuery(AQuery: TrwWizardQuery; AName: string): TrwQuery;
var
  NameIndex: Integer;

  //Creating New Query object
  function CreateNewQueryObject(ATable: TrwWizardQueryTable): TrwQuery;
  var
    Q: TrwQuery;
    sSelect, sFrom, sWhere: string;
    j: Integer;
    EventHandler: String;
    V: TrwGlobalVariable;


    function CreateQBSQL(AQuery: TrwQBQuery): string;
    var
      Q: TrwQBQuery;

      procedure GoThroughTables(AQuery: TrwQBQuery); forward;

      procedure AddParameter(AParam: String; ATable: TrwQBSelectedTable);
      var
        Cond: TrwQBFilterCondition;
        Expr: TrwQBExpression;
      begin
        if ATable.Filter.NodeType = rntOr then
          Exit;

        Cond := TrwQBFilterCondition(ATable.Filter.Conditions.Add);
        Cond.Operation := '=';
        Cond.LeftPart.AddItem(eitField, 'CO_NBR', ATable.InternalName);
        Expr := TrwQBExpression(Cond.RightPart.Add);
        Expr.AddItem(eitParam, AParam);
      end;

      procedure CreateTableFilter(ATable: TrwQBSelectedTable);
      var
        O: TrwDataDictTable;
        i: Integer;
        J: TrwQBJoins;
      begin
        if ATable.IsSUBQuery then
          GoThroughTables(ATable.SubQuery)

        else
        begin
          J := TrwQBSelectedTables(ATable.Collection).QBQuery.Joins;
          for i := 0 to J.Count - 1 do
            if (J[i].JoinType = rjtLeftJoin) and (J[i].RightTable = ATable) or
               (J[i].JoinType = rjtRightJoin) and (J[i].LeftTable = ATable) then
              Exit;

          O := ATable.lqObject;
          for i := 0 to O.Fields.Count - 1 do
            if AnsiSameText(O.Fields[i].Name, 'CO_NBR') then
            begin
              AddParameter('Company', ATable);
              break;
            end;
        end;
      end;

      procedure GoThroughTables(AQuery: TrwQBQuery);
      var
        i: Integer;
      begin
        for i := 0 to AQuery.SelectedTables.Count - 1 do
          CreateTableFilter(AQuery.SelectedTables[i])
      end;

    begin
      Q := TrwQBQuery.Create(nil);
      try
        Q.Assign(AQuery);
        GoThroughTables(Q);
        Result := Q.SQL;
      finally
        Q.Free;
      end;
    end;


    //Analyse of table and forming SQL
    procedure AnalyseTable(ATable: TrwWizardQueryTable; AQuery: TrwQuery; AParentOuter: Boolean);
    var
      i, k: Integer;
      Par: TrwWizardQueryTableParam;
      qw:  String[1];
      op,
      sD1, sM1, SY1,
      sD2, sM2, SY2,
      h, fld, v1, v2, h1: String;
      SF: TrwQBShowingField;
      FK: TDataDictForeignKey;
    begin
      //Clause FROM
      if ATable.QBQueryIsNotEmpty then
        sFrom := sFrom+','+#13+'   ('+CreateQBSQL(ATable.QBQuery)+')'

      else
      begin
        sFrom := sFrom+','+#13+'   '+ATable.Table.Name;
        if ATable.Table.Params.Count > 0 then
        begin
          sFrom := sFrom+'(';
          for i := 0 to ATable.Table.Params.Count-1 do
          begin
            if i > 0 then
              sFrom := sFrom+', ';
            Par := ATable.Params.FindParam(ATable.Table.Params[i].Name);
            if not Assigned(Par) or Par.ForInputForm or (ATable.Table.Params[i].DisplayName = '') then
            begin
              sFrom := sFrom+':'+ATable.Table.Params[i].Name;
              if Assigned(Par) and Par.ForInputForm then
                if FInFormParamList.IndexOf(Par.ParamName) = -1 then
                  FInFormParamList.AddObject(Par.ParamName, Par);
            end
            else
              if Par.EmptyParam and not AnsiSameText(Par.ParamName, 'Effective_Date') and not AnsiSameText(Par.ParamName, 'AsOfDate') then
                sFrom := sFrom+''''''
              else
                sFrom := sFrom+''''+Par.ParamValue+'''';
          end;
          sFrom := sFrom+')';
        end;
      end;

      sFrom := sFrom+' '+ATable.Alias;

      //Clause WHERE
      if ATable.Filter.Count > 0 then
        sWhere := sWhere+' and (';

      for i := 0 to ATable.Filter.Count-1 do
      begin

        fld := ATable.Alias+'.'+ATable.Filter[i].FieldName;
        v1 := ATable.Filter[i].Value1;
        v2 := ATable.Filter[i].Value2;
        qw := '';

        if ATable.Filter[i].Field.FieldType = ddtString then
          qw := '"'

        else if ATable.Filter[i].Field.FieldType = ddtDateTime then
          try
            StrToDate(Trim(ATable.Filter[i].Value1));
            qw := '"';
          except
            h := ATable.Filter[i].Value1;
            sM1 := Trim(GetNextStrValue(h, '/'));
            sD1 := Trim(GetNextStrValue(h, '/'));
            sY1 := Trim(GetNextStrValue(h, '/'));
            h := ATable.Filter[i].Value2;
            sM2 := Trim(GetNextStrValue(h, '/'));
            sD2 := Trim(GetNextStrValue(h, '/'));
            sY2 := Trim(GetNextStrValue(h, '/'));

            if sM1 <> '' then
            begin
              fld := 'Month(' + fld + ')';
              v1 := sM1;
              v2 := sM2;
            end
            else if sD1 <> '' then
            begin
              fld := 'Day(' + fld + ')';
              v1 := sD1;
              v2 := sD2;
            end
            else if sY1 <> '' then
            begin
              fld := 'Year(' + fld + ')';
              v1 := sY1;
              v2 := sY2;
            end;
          end;

        h1 := fld+' '+ATable.Filter[i].Operation+' ';
        if AnsiUpperCase(ATable.Filter[i].Operation) = 'BETWEEN' then
          h1 := h1+qw+v1+qw+' and '+qw+v2+qw

        else if AnsiUpperCase(ATable.Filter[i].Operation) = 'IN' then
          h1 := h1+'('+qw+ StringReplace(v1, ',', qw+' , '+qw, [rfReplaceAll])+qw+')'

        else if AnsiUpperCase(ATable.Filter[i].Operation) = 'LIKE' then
          h1 := h1+'"'+v1+'"'

        else
          h1 := h1+qw+v1+qw;

        if ATable.Outer or AParentOuter then
          h1 := '(' + h1 + ' or ' + fld + ' is null)';

        if i < ATable.Filter.Count-1 then
          if ATable.Filter[i].AndOr = rntAnd then
            h1 := h1 + ' and '
          else
            h1 := h1 + ' or ';

        sWhere := sWhere + h1;
      end;
      if ATable.Filter.Count > 0 then
        sWhere := sWhere+')';

      //Clause SELECT
      if ATable.QBQueryIsNotEmpty then
        for i := 0 to ATable.Fields.Count-1 do
        begin
          SF := ATable.QBQuery.ShowingFields.FieldByInternalName(ATable.Fields[i].FieldName);
          sSelect := sSelect+','+#13+'   '+ATable.Alias+'.'+SF.GetFieldName(False, False)+' '+ATable.Fields[i].Alias;
          ATable.Fields[i].FDataSet := AQuery;
        end


      else
      begin
        for i := 0 to ATable.Fields.Count-1 do
        begin
          sSelect := sSelect+','+#13+'   '+ATable.Alias+'.'+ATable.Fields[i].FieldName+' '+ATable.Fields[i].Alias;
          ATable.Fields[i].FDataSet := AQuery;

          if Assigned(ATable.Fields[i].LookUpTable.Table) then
          begin
            if ATable.Fields[i].LookUpTable.Outer then
              op := ' += '
            else
              op := ' = ';

            FK := ATable.Table.ForeignKeys.ForeignKeyByField(ATable.Fields[i].Field);
            Assert(Assigned(FK), 'Field ' + ATable.Table.Name + '.' + ATable.Fields[i].Field.Name + ' does not refer to anywhere');
            for k := 0 to FK.Fields.Count-1 do
              sWhere := sWhere+' and '+#13+'   '+ATable.Alias+'.'+ FK.Fields[k].Field.Name +
                op + ATable.Fields[i].LookUpTable.Alias+'.'+FK.Table.PrimaryKey[k].Field.Name;

            AnalyseTable(ATable.Fields[i].LookUpTable, AQuery, ATable.Outer or AParentOuter);      //RECURSION!!!
          end;
        end;

        //Looking through child tables
        for i := 0 to ATable.Tables.Count-1 do
        begin
          if ATable.Tables[i].Outer then
            op := ' =+ '
          else
            op := ' = ';


          FK := ATable.Tables[i].Table.ForeignKeys.ForeignKeyByTable(ATable.Table);
          Assert(Assigned(FK), 'Cannot find FK: ' + ATable.Tables[i].Table.Name + ' -> ' + ATable.Table.Name);
          for k := 0 to FK.Fields.Count-1 do
            sWhere := sWhere+' and'+#13+'   '+ATable.Alias+'.'+ ATable.Table.PrimaryKey[k].Field.Name +
              op + ATable.Tables[i].Alias+'.'+ FK.Fields[k].Field.Name;

          AnalyseTable(ATable.Tables[i], AQuery, ATable.Outer or AParentOuter);      //RECURSION!!!
        end;

        if not ATable.Outer and (ATable.Table.Fields.FieldByName('CO_NBR') <> nil) then
          sWhere := sWhere+' and'+#13+'   '+ATable.Alias+'.CO_NBR = :Company';
      end;

      CustomQueryCondition(ATable, sFrom, sWhere);

      if (Length(sSelect) >= 1) and (sSelect[1] = ',') then
        Delete(sSelect, 1, 1);
      if (Length(sWhere) >= 4) and (UpperCase(Copy(sWhere, 1, 4)) = ' AND') then
        Delete(sWhere, 1, 4);
      if (Length(sFrom) >= 1) and (sFrom[1] = ',') then
        Delete(sFrom, 1, 1);
    end;

  begin
    Inc(NameIndex);
    Q := CreateQuery(AName+IntToStr(NameIndex), 0, 0);
    FObjList.Add(Q);

    sSelect := '';
    sFrom := '';
    sWhere := '';

    AnalyseTable(ATable, Q, False);

    Q.SQL.Text := 'SELECT'+sSelect+#13+'FROM'+sFrom+#13;
    if sWhere <> '' then
      Q.SQL.Text := Q.SQL.Text+'WHERE'+sWhere;


    //Parameters of SQL
    EventHandler := '';

    CheckQueryParams(Q, EventHandler);

    for j := 0 to FInFormParamList.Count-1 do
    begin
      if CheckQueryParam(Q, FInFormParamList[j], FInFormParamList[j], EventHandler) and
         not TrwWizardQueryTableParam(FInFormParamList.Objects[j]).EmptyParam then
      begin
        V:= Report.ReportForm.GlobalVarFunc.Variables.Add;
        V.Name := FInFormParamList[j];
        V.VarType := DDTypeToRWType(TrwWizardQueryTableParam(FInFormParamList.Objects[j]).Param.ParamType);
        if (V.VarType = rwvDate) and AnsiSameText(TrwWizardQueryTableParam(FInFormParamList.Objects[j]).ParamValue, 'now') then
          V.Value := Now
        else
          V.Value := TrwWizardQueryTableParam(FInFormParamList.Objects[j]).ParamValue;
        V.UpdateValue;
      end;
    end;

    if EventHandler <> '' then
      Q.Events['BeforeOpen'].HandlersText := '//Assignin query parameters for opening'+EventHandler;

    Result := Q;
  end;

begin
  NameIndex := 0;
  Result := CreateNewQueryObject(AQuery.Table);
end;


function TrwWizardInfo.BuildBuffer(AQuery: TrwWizardQuery; AName: string; ATableName: String): TrwBuffer;
var
  B: TrwBuffer;

  procedure CreateStructure(ATable: TrwWizardQueryTable);
  var
    i: Integer;
    Fld: TrwField;
    SF: TrwQBShowingField;
  begin
    for i := 0 to ATable.Fields.Count-1 do
    begin
      Fld := TrwField(B.Fields.Add);
      Fld.FieldName := ATable.Fields[i].Alias;

      if ATable.QBQueryIsNotEmpty then
      begin
        SF := ATable.QBQuery.ShowingFields.FieldByInternalName(ATable.Fields[i].FieldName);
        case SF.ExprType of
          rwvDate:     Fld.FieldType := ftDate;
          rwvInteger:  Fld.FieldType := ftInteger;
          rwvFloat:    Fld.FieldType := ftFloat;
          rwvCurrency: Fld.FieldType := ftCurrency;
          rwvPointer:  Fld.FieldType := ftBLOB;
          rwvString,
          rwvVariant:   Fld.FieldType := ftString;
        end
      end

      else
        case ATable.Fields[i].Field.FieldType of
          ddtDateTime: Fld.FieldType := ftDate;
          ddtInteger:  Fld.FieldType := ftInteger;
          ddtFloat:    Fld.FieldType := ftFloat;
          ddtString:   Fld.FieldType := ftString;
          ddtBLOB,
          ddtMemo,
          ddtGraphic:  Fld.FieldType := ftBLOB;
        end;

      if Assigned(ATable.Fields[i].LookUpTable.Table) then
        CreateStructure(ATable.Fields[i].LookUpTable);
    end;

    for i := 0 to ATable.Tables.Count-1 do
      CreateStructure(ATable.Tables[i]);
  end;

begin
  B := TrwBuffer.Create(FDefOwner);
  FObjList.Add(B);
  B.Name := AName;
  CreateStructure(AQuery.Table);
  B.TableName := ATableName;
  Result := B;
end;



procedure TrwWizardInfo.Assign(Source: TPersistent);
begin
  Client := TrwWizardInfo(Source).Client;
  Company := TrwWizardInfo(Source).Company;
  MainQuery := TrwWizardInfo(Source).MainQuery;
  MainTable := TrwWizardInfo(Source).MainTable;
  FGenerateASCIIFile := TrwWizardInfo(Source).GenerateASCIIFile;
  FASCIIDelimiter := TrwWizardInfo(Source).ASCIIDelimiter;
  FSurroundChar := TrwWizardInfo(Source).SurroundChar;
end;

procedure TrwWizardInfo.Loaded;
begin
  inherited;
  MainQuery.SyncReferences;
end;

procedure TrwWizardInfo.SetPaper(const Value: TrwPage);
begin
  FReport.PageFormat.Assign(Value);
end;

function TrwWizardInfo.GetPaper: TrwPage;
begin
  Result := FReport.PageFormat;
end;


class function TrwWizardInfo.FormEditor: TClass;
begin
  Result := nil;
end;


class function TrwWizardInfo.WizardName: string;
begin
  Result := '';
end;


function TrwWizardInfo.AlignBand(ABand: TrwBand; ATop: Integer): Integer;
begin
  ABand.Top := ATop;
  Result := ABand.Top+ABand.Height+1;
end;


function TrwWizardInfo.ReorderBand(ABandType: TrwBandType; ATop: Integer): Integer;
var
  i: Integer;
begin
  i := Report.ReportForm.IndexOfBand(ABandType);
  if i <> -1 then
    Result := AlignBand(Report.ReportForm.Bands[i], ATop)
  else
    Result := ATop;
end;


procedure TrwWizardInfo.AssignArrValue(AVar: TrwGlobalVariable; Value: Variant);
var
  i: Integer;
  L: TStringList;
begin
  L := TStringList.Create;
  try
    L.Text := Value;
    Value := VarArrayCreate([0, L.Count - 1], varVariant);
    for i := 0 to L.Count - 1 do
      Value[i] := L[i];
  finally
    L.Free;
  end;

  AVar.Value := Value;
end;



procedure TrwWizardInfo.CreateGroupSumLabel(AColumn: TrwWizTblColumn; AName: string;
  ADetailBand: TrwBand; AClearEvent: TrwEvent; AGroupBandF: TrwBand);
var
  C: TrwComponent;
  OldCont: TrwControl;
begin
  OldCont := FDefContainer;
  FDefContainer := AGroupBandF;
  C := CreateLabel(AName, AName, AColumn.Control.Left, 2, AColumn.Control.Width, 20, alLeft, AColumn.Control.Alignment);
  TrwLabel(C).Format := GetStrProp(AColumn.Control, 'Format');
  TrwLabel(C).TextByValue := True;
  TrwLabel(C).Value := TrwLabel(C).Name;
  TrwLabel(C).BoundLines := AColumn.Control.BoundLines;

  if ADetailBand.Events['AfterPrint'].HandlersText <> '' then
    ADetailBand.Events['AfterPrint'].HandlersText := ADetailBand.Events['AfterPrint'].HandlersText+#13;
  ADetailBand.Events['AfterPrint'].HandlersText := ADetailBand.Events['AfterPrint'].HandlersText+'  '+
    AName+'.Value := ' +AName+'.Value +'+AColumn.Control.Name+'.Value;';

  if AClearEvent.HandlersText <> '' then
    AClearEvent.HandlersText :=
      AClearEvent.HandlersText + #13;
  AClearEvent.HandlersText :=
    AClearEvent.HandlersText + '  '+AName+'.Value := 0;';
  FDefContainer := OldCont;
end;

procedure TrwWizardInfo.CreateReportObject;
begin
  FReport := TrwReport.Create(nil);
  FReport.ReportForm.AddBand(rbtDetail);
end;


procedure TrwWizardInfo.CreateReport;
var
  i: Integer;
begin
  FObjList.Clear;
  Report.Description := Description;
  Report.Name := 'rptMain';
  FDefOwner := Report.ReportForm;

  //Is this ASCII report?
  i := Report.ReportForm.AddBand(rbtDetail);
  if GenerateASCIIFile then
  begin
    Report.ASCIIDelimiter := ASCIIDelimiter;
    if SurroundChar <> '' then
      Report.ASCIIDelimiter := Report.ASCIIDelimiter + #0 + SurroundChar;
    Report.ReportForm.Bands[i].PrintAsASCII := True;
  end;
  Report.ReportForm.Bands[i].BoundLines := [rclLeft, rclRight];
end;


function TrwWizardInfo.BufQuery(ABuf: TrwBuffer; AColumns: TrwWizTblColumns): TrwQuery;
var
  i, j, k, n: Integer;
  flGrp: Boolean;
  Sel, Grp: String;
  SortArr: array of Pointer;
  h: String;
  SelList: TStringList;

begin
  Result := TrwQuery.Create(FDefOwner);
  FObjList.Add(Result);
  flGrp := False;
  Grp := '';
  Sel := '';
  SelList := TStringList.Create;

  for i := 0 to AColumns.Count-1 do
  begin
    if AColumns[i].Summarized and Assigned(AColumns[i].Field) and
      (AColumns[i].Field.Field.FieldType in [ddtFloat, ddtInteger]) then
    begin
      Sel := Sel +','+#13+'      SUM(t.'+AColumns[i].DataField+') '+AColumns[i].DataField;
      SelList.Add(AColumns[i].DataField);
      flGrp := True;
    end
    else
      if AColumns[i].Expression.Count > 0 then
      begin
        for k := 0 to AColumns[i].Expression.Count-1 do
          if Pos('=', AColumns[i].Expression[k]) <> 0 then
          begin
            Sel := Sel + ','+#13+'      t.'+ AColumns[i].Expression.Names[k];
            SelList.Add(AColumns[i].Expression.Names[k]);
            Grp := Grp + ', ' + IntToStr(SelList.Count);
          end;
      end
      else
      begin
        Sel := Sel+','+#13+'      t.'+AColumns[i].DataField;
        SelList.Add(AColumns[i].DataField);        
        Grp := Grp + ', ' + IntToStr(SelList.Count);
      end;
  end;
  Delete(Sel, 1, 1);

  h := 'SELECT'+#13+Sel+#13+'FROM   '+ABuf.TableName+' t';
  if flGrp and (Grp <> '') then
  begin
    Delete(Grp, 1, 1);
    h := h+#13+'GROUP BY   '+Grp;
  end;
  Result.SQL.Text := h;

  h := '';

  for i := 0 to MainTable.Columns.Count-1 do
    if MainTable.Columns[i].GroupNumber > 0 then
    begin
      if Length(SortArr) < MainTable.Columns[i].GroupNumber then
        SetLength(SortArr, MainTable.Columns[i].GroupNumber);
      SortArr[MainTable.Columns[i].GroupNumber-1] := MainTable.Columns[i];
    end;

  for i := 0 to MainTable.Columns.Count-1 do
    if MainTable.Columns[i].GroupSummary then
    begin
      j := Report.ReportForm.AddBand(rbtSummary);
      Report.ReportForm.Bands[j].Height := 17;
      Report.ReportForm.Bands[j].Color := 15597054;
      Report.ReportForm.Bands[j].BoundLines := [rclLeft, rclRight, rclTop, rclBottom];
      break;
    end;


  for i := Low(SortArr) to High(SortArr) do
  begin
    TrwWizTblColumn(SortArr[i]).Group := TrwGroup(Report.Bands.Groups.Add);
    with TrwWizTblColumn(SortArr[i]).Group do
    begin
      FieldName := TrwWizTblColumn(SortArr[i]).DataField;

      GroupHeaderVisible := TrwWizTblColumn(SortArr[i]).GroupHeader;

      if not GroupHeaderVisible then
        for k := 0 to MainTable.Columns.Count-1 do
        begin
          if Assigned(MainTable.Columns[k].Group) then
            Continue;
          if MainTable.Columns[k].GroupSummary then
          begin
            GroupHeaderVisible := True;
            GroupHeader.BoundLines := [];
            GroupHeader.Height := 0;
            break;
          end
        end
      else
      begin
        GroupHeader.BoundLines := [rclLeft, rclRight, rclBottom];
        GroupHeader.Height := 17;
        GroupHeader.Color := 16704474;
      end;

      GroupFooterVisible := True;
      GroupFooter.Height := 1;
      GroupFooter.BoundLines := [rclTop];
      GroupFooter.Color := 15597054;

      if TrwWizTblColumn(SortArr[i]).GroupPageBreak then
        GroupFooter.Events['AfterPrint'].HandlersText := '  if not rptMain.MasterDataSource.Eof then' + #13 +
                                                         '    SkipPage;' + #13 +
                                                         '  endIf;';
    end;
  end;

  n := Length(SortArr);

  for i := 0 to MainTable.Columns.Count-1 do
    if MainTable.Columns[i].SortNumber > 0 then
    begin
      if Length(SortArr) < MainTable.Columns[i].SortNumber+n then
        SetLength(SortArr, MainTable.Columns[i].SortNumber+n);
      SortArr[MainTable.Columns[i].SortNumber+n-1] := MainTable.Columns[i];
    end;

  for i := Low(SortArr) to High(SortArr) do
    if TrwWizTblColumn(SortArr[i]).Calculated then
    begin
      for k := 0 to TrwWizTblColumn(SortArr[i]).Expression.Count-1 do
        if Pos('=', TrwWizTblColumn(SortArr[i]).Expression[k]) <> 0 then
        begin
          n := SelList.IndexOf(TrwWizTblColumn(SortArr[i]).Expression.Names[k]);
          if n <> - 1 then
            h := h + ', ' + IntToStr(n + 1);
        end;
    end
    else
    begin
      n := SelList.IndexOf(TrwWizTblColumn(SortArr[i]).DataField);
      if n <> - 1 then
        h := h + ', ' + IntToStr(n + 1);
    end;

  if h <> '' then
  begin
    Delete(h, 1, 1);
    Result.SQL.Text := Result.SQL.Text+#13+'ORDER BY '+h;
  end;

  FreeAndNil(SelList);
end;


procedure TrwWizardInfo.RealignBands;
var
  i, j: Integer;
begin
  i := ReorderBand(rbtPageHeader, 0);
  i := ReorderBand(rbtTitle, i);
  i := ReorderBand(rbtHeader, i);
  for j := 0 to Report.ReportForm.Groups.Count-1 do
    if Report.ReportForm.Groups[j].GroupHeaderVisible then
      i := AlignBand(Report.ReportForm.Groups[j].GroupHeader, i);
  i := ReorderBand(rbtDetail, i);
  i := ReorderBand(rbtSubDetail, i);
  for j := 0 to Report.ReportForm.Groups.Count-1 do
    i := AlignBand(Report.ReportForm.Groups[j].GroupFooter, i);
  i := ReorderBand(rbtFooter, i);
  i := ReorderBand(rbtSubSummary, i);
  i := ReorderBand(rbtSummary, i);
  i := ReorderBand(rbtPageFooter, i);
  i := ReorderBand(rbtCustom, i);
  ReorderBand(rbtPageStyle, i);
end;

procedure TrwWizardInfo.CreateInputForm;
var
  j, i: Integer;
  Par: TrwDataDictParam;
  C: TrwComponent;
begin
  FDefColor := Report.InputForm.Color;
  FDefContainer := Report.InputForm;
  j := 8;
  for i := 0 to FInFormParamList.Count-1 do
  begin
    if TrwWizardQueryTableParam(FInFormParamList.Objects[i]).EmptyParam then
      Continue;

    Par := TrwWizardQueryTableParam(FInFormParamList.Objects[i]).Param;
    CreateLabelIf('l'+FInFormParamList[i], Par.DisplayName, 8, j, 100, 10);

    C := nil;
    if Par.RefField <> nil then
{### ????      if Par.FieldName <> '' then
      begin
        //Reference to table
      end
      else
      begin
        //List of const values
      end
}
    else if Par.ParamType = ddtDateTime then
      C := TrwDateEdit.Create(Report.InputForm)
    else if Pos('=', Par.ParamValues.Text) > 0 then
    begin
      //List of const values
    end
    else
      C := TrwEdit.Create(Report.InputForm);

    TrwParamFormControl(C).Name := GetUniqueNameForComponent(C);
    TrwParamFormControl(C).ParamName := Par.Name;
    TrwParamFormControl(C).Top := j-2;
    TrwParamFormControl(C).Left := 110;
    TrwParamFormControl(C).Width := 150;
    TrwParamFormControl(C).Parent := TrwWinFormControl(FDefContainer);

    j := j+30;
  end;
  if j > 8 then
  begin
    Report.InputForm.Height := j;
    Report.InputForm.Width := 265;
  end;
end;


procedure TrwWizardInfo.CheckQueryParams(AQuery: TrwQuery; var AEventHandler: String);
begin
  CheckQueryParam(AQuery, 'Company', 'Company', AEventHandler);
end;


function TrwWizardInfo.CheckQueryParam(AQuery: TrwQuery; AParName, AParValue: string; var AEventHandler: String): Boolean;
var
  Par: TsbParam;
begin
  Par := AQuery.GetParamByName(AParName);
  Result := Assigned(Par);
  if Result then
    AEventHandler := AEventHandler+#13+#10+'  Self.SetParameterValue('''+AParName+''', '+AParValue+');'
end;


procedure TrwWizardInfo.CustomQueryCondition(ATable: TrwWizardQueryTable; var sFrom, sWhere: string);
begin
end;


{ TrwWizTblColumns }

function TrwWizTblColumns.Add: TrwWizTblColumn;
begin
  Result := TrwWizTblColumn(inherited Add);
end;

function TrwWizTblColumns.FindColumnByFieldName(AFieldName: String): Integer;
var
  i: Integer;
begin
  AFieldName := AnsiUpperCase(AFieldName);
  Result := -1;
  for i := 0 to Count-1 do
    if AFieldName = AnsiUpperCase(Items[i].DataField) then
    begin
      Result := i;
      Exit;
    end;
end;

function TrwWizTblColumns.GetItem(Index: Integer): TrwWizTblColumn;
begin
  Result := TrwWizTblColumn(inherited GetItem(Index));
end;

procedure TrwWizTblColumns.SetItem(Index: Integer; Value: TrwWizTblColumn);
begin
  inherited SetItem(Index, Value);
end;


{ TrwWizTblColumn }

procedure TrwWizTblColumn.Assign(Source: TPersistent);
begin
  Title := TrwWizTblColumn(Source).Title;
  Calculated := TrwWizTblColumn(Source).Calculated;
  Summarized := TrwWizTblColumn(Source).Summarized;
  WrapTitle := TrwWizTblColumn(Source).WrapTitle;
  Width := TrwWizTblColumn(Source).Width;
  Format := TrwWizTblColumn(Source).Format;
  Alignment := TrwWizTblColumn(Source).Alignment;
  DataField := TrwWizTblColumn(Source).DataField;
  SubstValue := TrwWizTblColumn(Source).SubstValue;
  Expression := TrwWizTblColumn(Source).Expression;
  SortNumber := TrwWizTblColumn(Source).SortNumber;
  GroupNumber := TrwWizTblColumn(Source).GroupNumber;
  GroupSummary := TrwWizTblColumn(Source).GroupSummary;
  ASCIISize := TrwWizTblColumn(Source).ASCIISize;
end;

constructor TrwWizTblColumn.Create(Collection: TCollection);
begin
  inherited;
  FExpression := TStringList.Create;
  FGroup := nil;
  FControl := nil;
  SortNumber := 0;
  GroupNumber := 0;
  FASCIISize := 0;
end;

destructor TrwWizTblColumn.Destroy;
begin
  FExpression.Free;
  inherited;
end;

procedure TrwWizTblColumn.SetDataField(const Value: string);
begin
  FField := TrwWizTblColumns(Collection).FTable.FQuery.FieldByAlias(Value);
  FDataField := Value;
end;

procedure TrwWizTblColumn.SetExpression(const Value: TStringList);
begin
  FExpression.Assign(Value);
end;

function TrwWizTblColumn.WidthPix: Integer;
begin
  Result := Trunc(cScreenCanvasRes*FWidth);
end;


{ TrwWizardQuery }

constructor TrwWizardQuery.Create;
begin
  inherited;
  FTable := TrwWizardQueryTable.Create(nil);
  FTable.Tables.FWizQuery := Self;
end;

destructor TrwWizardQuery.Destroy;
begin
  FTable.Free;
  inherited;
end;

procedure TrwWizardQuery.Assign(Source: TPersistent);
begin
  FTable.Assign(TrwWizardQuery(Source).Table);
end;

procedure TrwWizardQuery.SetTable(const Value: TrwWizardQueryTable);
begin
  FTable.Assign(Value);
end;

function TrwWizardQuery.GetUniqueFieldAlias(Name: string): string;
var
  i: Integer;
  h: String;
begin
  Result := '';
  Name := Copy(Name, 1, 23);
  for i := 1 to 100 do
  begin
    h := Name+IntToStr(i);
    if not Assigned(FieldByAlias(h)) then
    begin
      Result := h;
      break
    end
  end;
end;

function TrwWizardQuery.FieldByAlias(FldName: string): TrwWizardQueryTableField;

  function CheckTable(ATbl: TrwWizardQueryTable): TrwWizardQueryTableField;
  var
    i: Integer;
  begin
    Result := nil;
    for i := 0 to ATbl.Fields.Count-1 do
      if FldName = AnsiUpperCase(ATbl.Fields[i].Alias) then
      begin
        Result := ATbl.Fields[i];
        Exit;
      end
      else if Assigned(ATbl.Fields[i].LookUpTable.Table) then
      begin
        Result := CheckTable(ATbl.Fields[i].LookUpTable);
        if Assigned(Result) then
          Exit;
      end;

    for i := 0 to ATbl.Tables.Count-1 do
    begin
      Result := CheckTable(ATbl.Tables[i]);
      if Assigned(Result) then
        Break;
    end;
  end;

begin
  FldName := AnsiUpperCase(FldName);
  Result := CheckTable(Table);
end;

function TrwWizardQuery.GetUniqueTableAlias(Name: string): string;
var
  i: Integer;
  h: String;
begin
  Result := '';
  Name := Copy(Name, 1, 23);
  for i := 1 to 100 do
  begin
    h := Name+IntToStr(i);
    if not Assigned(TableByAlias(h)) then
    begin
      Result := h;
      break
    end
  end;
end;

function TrwWizardQuery.TableByAlias(TblName: string): TrwWizardQueryTable;

  function CheckTable(ATbl: TrwWizardQueryTable): TrwWizardQueryTable;
  var
    i: Integer;
  begin
    if TblName = ATbl.Alias then
    begin
      Result := ATbl;
      Exit;
    end;

    Result := nil;
    for i := 0 to ATbl.Fields.Count-1 do
      if Assigned(ATbl.Fields[i].LookUpTable.Table) then
      begin
        Result := CheckTable(ATbl.Fields[i].LookUpTable);
        if Assigned(Result) then
          Exit;
      end;

    for i := 0 to ATbl.Tables.Count-1 do
    begin
      Result := CheckTable(ATbl.Tables[i]);
      if Assigned(Result) then
        Break;
    end;
  end;

begin
  TblName := AnsiUpperCase(TblName);
  Result := CheckTable(Table);
end;




procedure TrwWizardQuery.SyncReferences;

  procedure Sync(ATable: TrwWizardQueryTable);
  var
    i: Integer;
  begin
    ATable.Tables.FWizQuery := Self;
    for i := 0 to ATable.Tables.Count-1 do
      Sync(ATable.Tables[i]);

    for i := 0 to ATable.Fields.Count-1 do
      Sync(ATable.Fields[i].LookUpTable);
  end;

begin
  Sync(Table);
end;




{ TrwWizTable }

constructor TrwWizardTable.Create;
begin
  inherited;
  FColumns := TrwWizTblColumns.Create(TrwWizTblColumn);
  FColumns.FTable := Self;
  FCountRecords := False;
end;

destructor TrwWizardTable.Destroy;
begin
  FColumns.Free;
  inherited;
end;

procedure TrwWizardTable.Assign(Source: TPersistent);
begin
  Columns := TrwWizardTable(Source).Columns;
  FCountRecords := TrwWizardTable(Source).FCountRecords;
end;

procedure TrwWizardTable.SetColumns(const Value: TrwWizTblColumns);
begin
  FColumns.Assign(Value);
end;



{ TrwWizardQueryTblFilter }

function TrwWizardQueryTblFilter.Add: TrwWizardQueryTblFltCond;
begin
  Result := TrwWizardQueryTblFltCond(inherited Add);
end;

function TrwWizardQueryTblFilter.GetItem(
  Index: Integer): TrwWizardQueryTblFltCond;
begin
  Result := TrwWizardQueryTblFltCond(inherited items[Index]);
end;

procedure TrwWizardQueryTblFilter.SetItem(Index: Integer;
  Value: TrwWizardQueryTblFltCond);
begin
  TrwWizardQueryTblFltCond(inherited items[Index]).Assign(Value);
end;


{ TrwWizardQueryTblFltCond }

procedure TrwWizardQueryTblFltCond.Assign(Source: TPersistent);
begin
  Inverse := TrwWizardQueryTblFltCond(Source).FInverse;
  FieldName :=  TrwWizardQueryTblFltCond(Source).FieldName;
  AndOr := TrwWizardQueryTblFltCond(Source).AndOr;
  Operation := TrwWizardQueryTblFltCond(Source).Operation;
  Value1 := TrwWizardQueryTblFltCond(Source).Value1;
  Value2 := TrwWizardQueryTblFltCond(Source).Value2;
  DisplayExpression := TrwWizardQueryTblFltCond(Source).DisplayExpression;
  Expression := TrwWizardQueryTblFltCond(Source).Expression;
end;


procedure TrwWizardQueryTblFltCond.SetFieldName(const Value: string);
begin
  FField := TrwDataDictField(TrwWizardQueryTblFilter(Collection).QueryTable.Table.Fields.FieldByName(Value));
  FFieldName := Value;
end;


{ TrwWizCollection }

procedure TrwWizCollection.Assign(Source: TPersistent);
begin
  Clear;
  inherited;
end;


initialization

finalization
  FreeAndNil(FWizardsList)

end.
