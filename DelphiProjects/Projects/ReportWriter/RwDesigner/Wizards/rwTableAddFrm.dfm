object rwTableAdd: TrwTableAdd
  Left = 384
  Top = 169
  AutoScroll = False
  Caption = 'Add Table'
  ClientHeight = 510
  ClientWidth = 526
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object btnOk: TButton
    Left = 362
    Top = 481
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 446
    Top = 481
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object grbTbls: TGroupBox
    Left = 8
    Top = 5
    Width = 512
    Height = 467
    Caption = 'Child Tables'
    TabOrder = 2
    object lDescr: TLabel
      Left = 12
      Top = 306
      Width = 53
      Height = 13
      Caption = 'Description'
    end
    object Label3: TLabel
      Left = 12
      Top = 16
      Width = 32
      Height = 13
      Caption = 'Tables'
    end
    object lFields: TLabel
      Left = 261
      Top = 16
      Width = 27
      Height = 13
      Caption = 'Fields'
    end
    object lvTables: TListView
      Left = 12
      Top = 32
      Width = 239
      Height = 268
      Columns = <
        item
          Width = 217
        end>
      ColumnClick = False
      HideSelection = False
      LargeImages = WizDM.ImageList
      ReadOnly = True
      ShowColumnHeaders = False
      SmallImages = WizDM.ImageList
      SortType = stText
      TabOrder = 0
      ViewStyle = vsReport
      OnChange = lvTablesChange
      OnDblClick = lvTablesDblClick
      OnEnter = lvTablesEnter
      OnKeyPress = lvTablesKeyPress
    end
    object meDescr: TMemo
      Left = 12
      Top = 324
      Width = 487
      Height = 132
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 1
    end
    object lvFields: TListView
      Left = 261
      Top = 32
      Width = 239
      Height = 268
      Columns = <
        item
          Width = 217
        end>
      ColumnClick = False
      LargeImages = WizDM.ImageList
      ReadOnly = True
      ShowColumnHeaders = False
      SmallImages = WizDM.ImageList
      SortType = stText
      TabOrder = 2
      ViewStyle = vsReport
      OnChange = lvFieldsChange
      OnEnter = lvFieldsEnter
      OnKeyPress = lvTablesKeyPress
    end
  end
end
