object rwWPParam: TrwWPParam
  Left = 0
  Top = 0
  Width = 443
  Height = 34
  Align = alTop
  AutoScroll = False
  AutoSize = True
  TabOrder = 0
  Visible = False
  object Label1: TLabel
    Left = 0
    Top = 21
    Width = 3
    Height = 13
  end
  object lName: TLabel
    Left = 21
    Top = 2
    Width = 30
    Height = 13
    Caption = 'lName'
    OnClick = lNameClick
  end
  object chbInFrm: TCheckBox
    Left = 192
    Top = 0
    Width = 137
    Height = 17
    Caption = 'Create on the Input form'
    TabOrder = 0
    OnExit = chbInFrmExit
  end
  object chbUse: TCheckBox
    Left = 5
    Top = 0
    Width = 16
    Height = 17
    TabOrder = 1
    OnClick = chbUseClick
  end
end
