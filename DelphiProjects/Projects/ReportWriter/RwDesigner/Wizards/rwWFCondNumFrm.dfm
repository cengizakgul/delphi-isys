inherited rwWFCondNum: TrwWFCondNum
  inherited Panel1: TPanel
    inherited cbOper: TComboBox
      OnChange = cbOperChange
    end
    object edNum1: TwwDBSpinEdit
      Left = 100
      Top = 1
      Width = 89
      Height = 21
      Increment = 0.1
      TabOrder = 1
      UnboundDataType = wwDefault
    end
    object pnNum: TPanel
      Left = 197
      Top = -1
      Width = 132
      Height = 25
      BevelOuter = bvNone
      TabOrder = 2
      object Label1: TLabel
        Left = 5
        Top = 4
        Width = 19
        Height = 13
        Caption = 'And'
      end
      object edNum2: TwwDBSpinEdit
        Left = 36
        Top = 1
        Width = 89
        Height = 21
        Increment = 0.1
        TabOrder = 0
        UnboundDataType = wwDefault
      end
    end
  end
end
