// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWizardQBPrEditFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWizardPrEditEdit, Db, Buttons, StdCtrls, ComCtrls, ExtCtrls,
  CheckLst, wwdblook, rwWizardInfo, rwQBInfo, rwUtils,
  rwDataDictionary, rwDB, rwBasicClasses, rwCommonClasses, rwTypes,
  rwReportControls, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  rwDesignClasses, rwQueryBuilderFrm, rwVirtualMachine,
  ISDataAccessComponents;

type
  TrwWizInfQBPayroll = class(TrwWizInfPayroll)
  private
  protected
    procedure CreateReportObject; override;

  public
    constructor Create(AOwner: TComponent); override;
    class function FormEditor: TClass; override;
    class function WizardName: string; override;
  end;


  TrwWizardQBPrEdit = class(TrwWizardPrEdit)
    TabSheet1: TTabSheet;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
  private
  public
  end;


implementation


{$R *.DFM}


{ TrwWizInfQBPayroll }

constructor TrwWizInfQBPayroll.Create(AOwner: TComponent);
begin
  inherited;
  MainQuery.Table.InitQBQuery;
end;

procedure TrwWizInfQBPayroll.CreateReportObject;
var
  O: TrwDataDictTable;
  i: Integer;
begin
  inherited;
  O := TrwDataDictTable(DataDictionary.Tables.TableByName('TMP_PR'));
  O.DisplayName := 'Selected Payrolls';

  for i := 0 to O.Fields.Count - 1 do
  begin
    if AnsiSameText(O.Fields[i].Name, 'PR_NBR') then
    begin
      O.Fields[i].DisplayName := 'Payroll';
      O.PrimaryKey.AddKeyField(O.Fields[i]);
    end

    else if AnsiSameText(O.Fields[i].Name, 'check_date') then
      O.Fields[i].DisplayName := 'Check Date'
    else if AnsiSameText(O.Fields[i].Name, 'Batch_date_b') then
      O.Fields[i].DisplayName := 'Batch Begin Date'
    else if AnsiSameText(O.Fields[i].Name, 'Batch_date_e') then
      O.Fields[i].DisplayName := 'Batch End Date'
    else if AnsiSameText(O.Fields[i].Name, 'run_nbr') then
      O.Fields[i].DisplayName := 'Run number'
    else if AnsiSameText(O.Fields[i].Name, 'process_date') then
      O.Fields[i].DisplayName := 'Process Date'
  end;
end;

class function TrwWizInfQBPayroll.FormEditor: TClass;
begin
  Result := TrwWizardQBPrEdit;
end;


class function TrwWizInfQBPayroll.WizardName: string;
begin
  Result := 'Payroll report (QB)';
end;


procedure TrwWizardQBPrEdit.Button2Click(Sender: TObject);
var
  i, j: Integer;
  fl: Boolean;
  C: TrwWizTblColumn;
  SF: TrwQBShowingField;
  V: TrwGlobalVariable;
begin
  try
    Finalize;
    V := WizardInfo.Report.ReportForm.GlobalVarFunc.Variables[WizardInfo.Report.ReportForm.GlobalVarFunc.Variables.IndexOf('Clients')];
    WizardInfo.AssignArrValue(V, WizardInfo.Client);
    V := WizardInfo.Report.ReportForm.GlobalVarFunc.Variables[WizardInfo.Report.ReportForm.GlobalVarFunc.Variables.IndexOf('Companies')];
    WizardInfo.AssignArrValue(V, WizardInfo.Company);
    V := WizardInfo.Report.ReportForm.GlobalVarFunc.Variables[WizardInfo.Report.ReportForm.GlobalVarFunc.Variables.IndexOf('Payrolls')];
    WizardInfo.AssignArrValue(V, TrwWizInfQBPayroll(WizardInfo).Payroll);

    WizardInfo.Report.Compile;
    WizardInfo.Report.Print('c:\tmp.rwa');
  except
  end;
  DeleteFile('c:\tmp.rwa');

  ShowQueryBuilder(WizardInfo.MainQuery.Table.QBQuery, False, True);

  for i := 0 to WizardInfo.MainQuery.Table.Fields.Count -1 do
  begin
    fl := False;
    for j := 0 to WizardInfo.MainTable.Columns.Count -1 do
      if WizardInfo.MainTable.Columns[j].Field = WizardInfo.MainQuery.Table.Fields[i] then
      begin
        fl := True;
        break;
      end;

    if not fl then
    begin
      SF := WizardInfo.MainQuery.Table.QBQuery.ShowingFields.FieldByInternalName(WizardInfo.MainQuery.Table.Fields[i].FieldName);
      C := WizardInfo.MainTable.Columns.Add;
      C.Title := SF.GetFieldName(True, False);
      C.Width := 0.5;
      C.Calculated := False;
      C.DataField := WizardInfo.MainQuery.Table.Fields[i].Alias;
      C.SubstValue := False;
    end;
  end;
end;


initialization
  RegisterClass(TrwWizInfQBPayroll);
  WizardsLst.Add(TrwWizInfQBPayroll);

finalization
  UnRegisterClass(TrwWizInfQBPayroll);
  WizardsLst.Remove(TrwWizInfQBPayroll);


end.
