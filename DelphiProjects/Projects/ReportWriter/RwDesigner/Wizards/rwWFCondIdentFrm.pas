// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWFCondIdentFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWFCondFrm, StdCtrls, ExtCtrls, CheckLst, Db, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  rwDesignClasses, rwUtils;

type
  TrwWFCondIdent = class(TrwWFCond)
    dsValues: TisRWClientDataSet;
    dsValuesName: TStringField;
    dsValuesValue: TStringField;
    chbValues: TCheckListBox;
  private
  public
    procedure PutCond; override;
    procedure GetCond; override;
    procedure AfterConstruction; override;
    procedure Initialize(AValues: string); virtual;
  end;


implementation

{$R *.DFM}

procedure TrwWFCondIdent.AfterConstruction;
begin
  inherited;
  cbOper.ItemIndex := cbOper.Items.Add('In');
end;

procedure TrwWFCondIdent.GetCond;
var
  i: Integer;
  h, h1: String;
begin
  Condition.Operation := 'In';
  h := '';
  h1 := '';
  dsValues.DisableControls;
  for i := 0 to chbValues.Items.Count-1 do
    if chbValues.Checked[i] then
    begin
      dsValues.RecNo := i+1;
      h := h+','+dsValues.FieldByName('value').AsString;
      h1 := h1+', '+chbValues.Items[i];
    end;
  Delete(h, 1, 1);
  Delete(h1, 1, 1);
  dsValues.EnableControls;

  Condition.Value1 := h;
  Condition.DisplayExpression := Condition.Field.DisplayName+' '+Condition.Operation+
    ' '+'('+h1+')';
end;


procedure TrwWFCondIdent.PutCond;
var
  S: TStringList;
  i: Integer;
begin
  dsValues.DisableControls;
  S := TStringList.Create;
  try
    S.CommaText := Condition.Value1;
    for i := 0 to S.Count-1 do
    begin
      dsValues.Locate('Value', S[i], []);
      chbValues.Checked[dsValues.RecNo-1] := True;
    end;
  finally
    S.Free;
    dsValues.EnableControls;
  end;
end;

procedure TrwWFCondIdent.Initialize(AValues: string);
var
  L: TStringList;
  i: Integer;
begin
  dsValues.Close;
  dsValues.CreateDataSet;
  dsValues.Open;

  L := TStringList.Create;
  try
    L.Text := AValues;

    chbValues.Items.Clear;
    for i := 0 to L.Count - 1 do
    begin
      dsValues.Append;
      dsValues.FieldByName('Value').AsString := L.Names[i];
      dsValues.FieldByName('Name').AsString := L.Values[L.Names[i]];
      chbValues.Items.Add(dsValues.FieldByName('Name').AsString);
      dsValues.Post;
    end;

  finally
    L.Free;
  end;
end;


end.
