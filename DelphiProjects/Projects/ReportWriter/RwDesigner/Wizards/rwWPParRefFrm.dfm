inherited rwWPParRef: TrwWPParRef
  Height = 56
  object sbOr: TSpeedButton [2]
    Left = 425
    Top = 0
    Width = 23
    Height = 22
    AllowAllUp = True
    GroupIndex = 1
    Caption = 'OR'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = sbOrClick
  end
  object chbValues: TCheckListBox [3]
    Left = 180
    Top = 0
    Width = 241
    Height = 56
    ItemHeight = 13
    TabOrder = 2
    Visible = False
    OnExit = cbValuesExit
  end
  inherited chbInFrm: TCheckBox
    Left = 463
    Top = 3
  end
  inherited chbUse: TCheckBox
    TabOrder = 3
  end
  object cbValues: TwwDBLookupCombo
    Left = 180
    Top = 1
    Width = 241
    Height = 21
    DropDownAlignment = taLeftJustify
    LookupTable = dsValues
    LookupField = 'Val'
    Options = [loColLines]
    Style = csDropDownList
    DropDownCount = 16
    TabOrder = 1
    AutoDropDown = False
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = True
    ShowMatchText = True
    OnExit = cbValuesExit
  end
  object dsValues: TisRWClientDataSet
    Left = 79
    Top = 2
  end
end
