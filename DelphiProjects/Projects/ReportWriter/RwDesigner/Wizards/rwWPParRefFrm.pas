// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWPParRefFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWPParamFrm, StdCtrls, wwdblook, Db, rwDataDictionary, Buttons,
  CheckLst, rwUtils, sbAPI, Variants, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, rwDesignClasses, rwLogQuery, rwEngine,
  ISDataAccessComponents;

type
  TrwWPParRef = class(TrwWPParam)
    cbValues: TwwDBLookupCombo;
    dsValues: TisRWClientDataSet;
    chbValues: TCheckListBox;
    sbOr: TSpeedButton;
    procedure sbOrClick(Sender: TObject);
    procedure cbValuesExit(Sender: TObject);
  protected
    function  GetValue: string; override;
    procedure SetValue(const Value: string); override;
    procedure TurnOnOff(AStatus: Boolean); override;
  public
    procedure Initialize(ATable: string; AField: string; AClient: Integer; ACompany: Integer);
  end;


implementation

uses rwCustomDataDictionary;


{$R *.DFM}

{ TrwWPParRef }

function TrwWPParRef.GetValue: string;
var
  i: Integer;
  h: String;
begin
  if sbOr.Down then
  begin
    h := '';
    dsValues.DisableControls;
    for i := 0 to chbValues.Items.Count-1 do
      if chbValues.Checked[i] then
      begin
        dsValues.First;
        dsValues.MoveBy(i);
        h := h+','+dsValues.FieldByName('val').AsString;
      end;
    Delete(h, 1, 1);
    dsValues.EnableControls;
    Result := h;
  end
  else
    Result := cbValues.Value;
end;

procedure TrwWPParRef.SetValue(const Value: string);
var
  L: TStringList;
  i: Integer;
begin
  if Pos(',', Value) > 0 then
  begin
    sbOr.Down := True;
    sbOr.Click;
    L := TStringList.Create;
    try
      L.CommaText := Value;
      for i := 0 to L.Count-1 do
        if dsValues.Locate('val', VarArrayOf([L[i]]), []) then
          chbValues.Checked[dsValues.RecNo-1] := True;
    finally
      L.Free;
    end;
  end
  else
    cbValues.Value := Value;
end;

procedure TrwWPParRef.Initialize(ATable, AField: string; AClient: Integer; ACompany: Integer);
var
  SQL: string;
  i: Integer;
  Obj: TrwDataDictTable;
  h: String;
  Q: TrwLQQuery;
  Pars: array [0..0] of Variant;
  Res: Variant;
begin
  Obj := TrwDataDictTable(DataDictionary.Tables.TableByName(ATable));

  if AField = '' then
    AField := Obj.PrimaryKey[0].Field.Name;

  SQL := 'select t.'+AField+' val, ';

  h := '';
  for i := 0 to Obj.LogicalKey.Count - 1 do
    h := h+'||"  "||t.'+Obj.LogicalKey[i].Field.Name;
  if h = '' then
    h := 't.'+AField
  else
    Delete(h, 1, 8);

  SQL := SQL+h+' name from '+ATable;
  if TrwDataDictParams(Obj.Params).ParamValueStr <> '' then
    SQL := SQL+'('+TrwDataDictParams(Obj.Params).ParamValueStr+')';
  SQL := SQL+' t';

  if Obj.Fields.FieldByName('CO_NBR') <> nil then
    SQL := SQL + ' where t.co_nbr = '+IntToStr(ACompany);

  SQL := SQL+' order by 2';

  dsValues.Close;
  Q := TrwLQQuery.Create;
  try
    Pars[0] := AClient;
    RWEngineExt.AppAdapter.ExecuteExternalFunction(1, Pars, Res); //OpenClient
    Q.SQL.Text := SQL;
    Q.OpenSQL;
    sbSBCursorToClientDataSet(Q, dsValues);
    dsValues.Open;
  finally
    Q.Free;
  end;

  cbValues.Selected.Clear;
  cbValues.Selected.Add('Name'+#9+IntToStr(dsValues.FieldByName('name').Size)+#9+'Name'+#9+'F');

  chbValues.Items.Clear;
  while not dsValues.Eof do
  begin
    chbValues.Items.Add(dsValues.FieldByName('name').AsString);
    dsValues.Next;
  end;
  dsValues.First;
end;

procedure TrwWPParRef.sbOrClick(Sender: TObject);
begin
  cbValues.Visible := not sbOr.Down;
  chbValues.Visible := sbOr.Down;
  if sbOr.Down then
    Label1.Top := chbValues.Top+chbValues.Height
  else
    Label1.Top := cbValues.Top+cbValues.Height
end;


procedure TrwWPParRef.cbValuesExit(Sender: TObject);
begin
  DoChange;
end;

procedure TrwWPParRef.TurnOnOff(AStatus: Boolean);
begin
  inherited;
  if AStatus then
    sbOr.Click;
end;

end.
