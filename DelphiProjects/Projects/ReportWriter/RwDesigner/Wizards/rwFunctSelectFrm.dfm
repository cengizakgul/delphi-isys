object rwFunctSelect: TrwFunctSelect
  Left = 270
  Top = 285
  Width = 758
  Height = 272
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'Functions'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  DesignSize = (
    750
    245)
  PixelsPerInch = 96
  TextHeight = 13
  object btnOk: TButton
    Left = 585
    Top = 216
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 669
    Top = 216
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object lvFunct: TListView
    Left = 0
    Top = 0
    Width = 751
    Height = 211
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = 'Function'
        Width = 350
      end
      item
        AutoSize = True
        Caption = 'Description'
      end>
    HideSelection = False
    Items.Data = {
      A80100000800000000000000FFFFFFFFFFFFFFFF01000000000000000C546F64
      6179203A20446174651452657475726E732063757272656E7420646174650000
      0000FFFFFFFFFFFFFFFF01000000000000002155707065724361736520284154
      6578743A20537472696E67293A20537472696E670000000000FFFFFFFFFFFFFF
      FF0100000000000000214C6F77657243617365202841546578743A2053747269
      6E67293A20537472696E670000000000FFFFFFFFFFFFFFFF0100000000000000
      1F5472696D4C656674202841546578743A20537472696E67293A537472696E67
      0000000000FFFFFFFFFFFFFFFF0100000000000000205472696D526967687420
      2841546578743A20537472696E67293A537472696E670000000000FFFFFFFFFF
      FFFFFF01000000000000001B5472696D202841546578743A20537472696E6729
      3A537472696E670000000000FFFFFFFFFFFFFFFF010000000000000019416273
      202856616C75653A20466C6F6174293A20466C6F61740000000000FFFFFFFFFF
      FFFFFF01000000000000000B54696D65203A204461746500FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF}
    ReadOnly = True
    TabOrder = 2
    ViewStyle = vsReport
    OnDblClick = lvFunctDblClick
  end
end
