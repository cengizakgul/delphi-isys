inherited rwWizardPrEdit: TrwWizardPrEdit
  Left = 394
  Top = 194
  Caption = 'rwWizardPrEdit'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pcPages: TPageControl
    inherited tsPrimary: TTabSheet
      Caption = 'Primary Information'
      inherited GroupBox12: TGroupBox
        inherited memDescr: TMemo
          Height = 207
        end
      end
      inherited GroupBox1: TGroupBox
        Top = 2
      end
      inherited rgGenASCII: TRadioGroup
        Top = 97
        Width = 234
        Height = 77
      end
      inherited chbGenASCII: TCheckBox
        Top = 94
      end
      inherited edDelimiter: TEdit
        Left = 120
        Top = 144
      end
      inherited GroupBox14: TGroupBox
        inherited cbClients: TwwDBLookupCombo
          OnCloseUp = cbClientsCloseUp
        end
      end
      inherited chbQuoted: TCheckBox
        Left = 120
        Top = 115
        TabOrder = 7
      end
      object GroupBox11: TGroupBox
        Left = 246
        Top = 56
        Width = 376
        Height = 181
        Caption = 'Payrolls'
        TabOrder = 6
        object clPayrolls: TCheckListBox
          Left = 9
          Top = 17
          Width = 357
          Height = 154
          Columns = 3
          ItemHeight = 13
          TabOrder = 0
        end
      end
    end
    inherited tsQuery: TTabSheet
      inherited grbParams: TGroupBox
        Height = 237
        inherited ScrollBox1: TScrollBox
          Height = 171
        end
      end
    end
  end
  inherited BitBtn1: TBitBtn
    Top = 511
  end
  inherited BitBtn2: TBitBtn
    Top = 511
  end
end
