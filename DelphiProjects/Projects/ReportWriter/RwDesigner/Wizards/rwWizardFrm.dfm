object rwWizard: TrwWizard
  Left = 345
  Top = 180
  BorderStyle = bsDialog
  Caption = 'Wizard'
  ClientHeight = 545
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pcPages: TPageControl
    Left = 0
    Top = 0
    Width = 635
    Height = 545
    ActivePage = tsPrimary
    Align = alClient
    TabOrder = 2
    OnChange = pcPagesChange
    object tsPrimary: TTabSheet
      Caption = 'Primary information'
      object GroupBox12: TGroupBox
        Left = 4
        Top = 241
        Width = 617
        Height = 237
        Caption = 'Notes'
        TabOrder = 0
        object memDescr: TMemo
          Left = 10
          Top = 17
          Width = 597
          Height = 210
          ScrollBars = ssVertical
          TabOrder = 0
          WantReturns = False
        end
      end
      object GroupBox1: TGroupBox
        Left = 4
        Top = 70
        Width = 234
        Height = 83
        Caption = 'Page Format'
        TabOrder = 1
        object Label1: TLabel
          Left = 10
          Top = 20
          Width = 51
          Height = 13
          Caption = 'Paper Size'
        end
        object imgLandScape: TImage
          Left = 18
          Top = 48
          Width = 32
          Height = 26
          AutoSize = True
          Picture.Data = {
            07544269746D617016020000424D160200000000000076000000280000002000
            00001A0000000100040000000000A0010000CE0E0000C40E0000100000000000
            000000000000000080000080000000808000800000008000800080800000C0C0
            C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF00000000000000000000000000000000008FFFF77777777777777777777777
            77708FFFFFFFFFFFFFFFFFFFFFFFFFFFFF708FFFFFFFFFFFFFFFFFFFFFFFFFFF
            FF708FFFFFFFFFFFFFFFFFFFFFFFFFFFFF708FFFFFFFFFFFFFFFFFFFFFFFFFFF
            FF708FFFFFFFFFFFFFFFFFFFFFFFFFFFFF708FFFFFFFF8888FFFF888888FFFFF
            FF708FFFFFFFFF78FFFFFF7887FFFFFFFF708FFFFFFFFFF87FFFFF888FFFFFFF
            FF708FFFFFFFFFF787FFF7887FFFFFFFFF708FFFFFFFFFFF88888888FFFFFFFF
            FF708FFFFFFFFFFF87FF7887FFFFFFFFFF708FFFFFFFFFFF78FF888FFFFFFFFF
            FF708FFFFFFFFFFFF87F887FFFFFFFFFFF708FFFFFFFFFFFF78888FFFFFFFFFF
            FF708FFFFFFFFFFFFF8887FFFFFFFFFFFF708FFFFFFFFFFFFF788FFFFFFFFFFF
            FF708FFFFFFFFFFFFFF87FFFFFFFFFFFFF708FFFFFFFFFFFFFFFFFFFFFFFF000
            00088FFFFFFFFFFFFFFFFFFFFFFFF07FFF878FFFFFFFFFFFFFFFFFFFFFFFF07F
            F8778FFFFFFFFFFFFFFFFFFFFFFFF07F87778FFFFFFFFFFFFFFFFFFFFFFFF0F8
            77778FFFFFFFFFFFFFFFFFFFFFFFF08777778888888888888888888888888077
            7777}
        end
        object imgPortrait: TImage
          Left = 21
          Top = 44
          Width = 26
          Height = 32
          AutoSize = True
          Picture.Data = {
            07544269746D617076020000424D760200000000000076000000280000001A00
            000020000000010004000000000000020000CE0E0000C40E0000100000000000
            000000000000000080000080000000808000800000008000800080800000C0C0
            C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF00000000000000000000000000000000008777777777777777777777777000
            00008FFFFFFFFFFFFFFFFFFFFFFF700000008FFFFFFFFFFFFFFFFFFFFFFF7000
            00008FFFFFFFFFFFFFFFFFFFFFFF700000008FFFFFFFFFFFFFFFFFFFFFFF7000
            00008FFFFFFFFFFFFFFFFFFFFFFF700000008FFFFFFFFFFFFFFFFFFFFFFF7000
            00008FFFFFFFFFFFFFFFFFFFFFFF700000008FFFFFFFFFFFFFFFFFFFFFFF7000
            00008FFFFF8888FFFF888888FFFF700000008FFFFFF78FFFFFF7887FFFFF7000
            00008FFFFFFF87FFFFF888FFFFFF700000008FFFFFFF787FFF7887FFFFFF7000
            00008FFFFFFFF88888888FFFFFFF700000008FFFFFFFF87FF7887FFFFFFF7000
            00008FFFFFFFF78FF888FFFFFFFF700000008FFFFFFFFF87F887FFFFFFFF7000
            00008FFFFFFFFF78888FFFFFFFFF700000008FFFFFFFFFF8887FFFFFFFFF7000
            00008FFFFFFFFFF788FFFFFFFFFF700000008FFFFFFFFFFF87FFFFFFFFFF7000
            00008FFFFFFFFFFFFFFFFFFFFFFF700000008FFFFFFFFFFFFFFFFFFFFFFF7000
            00008FFFFFFFFFFFFFFFFFFFFFFF700000008FFFFFFFFFFFFFFFFFF800000000
            00008FFFFFFFFFFFFFFFFFF8FF78070000008FFFFFFFFFFFFFFFFFF8F7807700
            00008FFFFFFFFFFFFFFFFFF87807770000008FFFFFFFFFFFFFFFFFF880777700
            00008FFFFFFFFFFFFFFFFFF80777770000008888888888888888888877777700
            0000}
        end
        object cbPaperNames: TComboBox
          Left = 68
          Top = 17
          Width = 155
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
        end
        object rbPortrait: TRadioButton
          Left = 80
          Top = 56
          Width = 55
          Height = 17
          Caption = 'Portrait'
          TabOrder = 1
          OnClick = rbPortraitClick
        end
        object rbLandscape: TRadioButton
          Left = 150
          Top = 56
          Width = 74
          Height = 17
          Caption = 'Landscape'
          TabOrder = 2
          OnClick = rbPortraitClick
        end
      end
      object rgGenASCII: TRadioGroup
        Left = 4
        Top = 160
        Width = 161
        Height = 76
        Enabled = False
        ItemIndex = 0
        Items.Strings = (
          'Fixed Length'
          'Delimited')
        TabOrder = 2
        OnClick = rgGenASCIIClick
      end
      object chbGenASCII: TCheckBox
        Left = 13
        Top = 157
        Width = 109
        Height = 17
        Caption = 'Generate ASCII file'
        TabOrder = 3
        OnClick = chbGenASCIIClick
      end
      object edDelimiter: TEdit
        Left = 103
        Top = 207
        Width = 33
        Height = 21
        Enabled = False
        TabOrder = 4
        Text = ';'
        OnExit = edDelimiterExit
      end
      object GroupBox14: TGroupBox
        Left = 245
        Top = 2
        Width = 377
        Height = 49
        Caption = 'Company'
        TabOrder = 5
        object cbClients: TwwDBLookupCombo
          Left = 9
          Top = 17
          Width = 358
          Height = 21
          DropDownAlignment = taLeftJustify
          LookupTable = dsClients
          LookupField = 'name'
          Options = [loColLines]
          Style = csDropDownList
          DropDownCount = 16
          TabOrder = 0
          AutoDropDown = False
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
          OnChange = cbClientsChange
        end
      end
      object chbQuoted: TCheckBox
        Left = 103
        Top = 180
        Width = 58
        Height = 17
        Caption = 'Quoted'
        Enabled = False
        TabOrder = 6
        OnClick = chbQuotedClick
      end
    end
    object tsQuery: TTabSheet
      Caption = 'Main query'
      ImageIndex = 1
      object grbParams: TGroupBox
        Left = 9
        Top = 241
        Width = 612
        Height = 236
        Caption = 'Property of selected Table'
        TabOrder = 0
        object ScrollBox1: TScrollBox
          Left = 2
          Top = 64
          Width = 608
          Height = 170
          Align = alClient
          BorderStyle = bsNone
          TabOrder = 0
          object pnCont: TPanel
            Left = 0
            Top = 0
            Width = 608
            Height = 25
            Align = alTop
            AutoSize = True
            BevelOuter = bvNone
            TabOrder = 0
          end
        end
        object Panel1: TPanel
          Left = 2
          Top = 15
          Width = 608
          Height = 49
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 608
            Height = 37
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object chbOuter: TCheckBox
              Left = 9
              Top = 11
              Width = 300
              Height = 15
              Caption = 'If there is no data do not show information from parent level'
              TabOrder = 0
              Visible = False
              OnClick = chbOuterClick
            end
            object btnFltrTbl: TButton
              Left = 524
              Top = 6
              Width = 75
              Height = 25
              Caption = 'Extra Filter'
              TabOrder = 1
              OnClick = btnFltrTblClick
            end
          end
        end
      end
      object GroupBox10: TGroupBox
        Left = 9
        Top = 5
        Width = 300
        Height = 229
        Caption = 'Selected Tables'
        TabOrder = 1
        object btnAddTbl: TButton
          Left = 12
          Top = 198
          Width = 75
          Height = 25
          Caption = 'Add'
          TabOrder = 0
          OnClick = btnAddTblClick
        end
        object btnDelTbl: TButton
          Left = 95
          Top = 198
          Width = 75
          Height = 25
          Caption = 'Delete'
          TabOrder = 1
          OnClick = btnDelTblClick
        end
        object lvTables: TTreeView
          Left = 12
          Top = 16
          Width = 275
          Height = 177
          HideSelection = False
          Images = WizDM.ImageList
          Indent = 19
          ReadOnly = True
          ShowRoot = False
          StateImages = WizDM.ImageList
          TabOrder = 2
          OnChange = lvTablesChange
          OnEnter = lvTablesEnter
        end
      end
      object grbFields: TGroupBox
        Left = 319
        Top = 5
        Width = 300
        Height = 229
        Caption = 'Selected Fields'
        TabOrder = 2
        object lvFields: TTreeView
          Left = 11
          Top = 16
          Width = 277
          Height = 177
          HideSelection = False
          Images = WizDM.ImageList
          Indent = 19
          ReadOnly = True
          TabOrder = 0
          OnChange = lvFieldsChange
        end
        object Button1: TButton
          Left = 213
          Top = 198
          Width = 75
          Height = 25
          Caption = 'Edit'
          TabOrder = 1
          OnClick = Button1Click
        end
        object pnNote: TPanel
          Left = 2
          Top = 196
          Width = 207
          Height = 31
          BevelOuter = bvNone
          TabOrder = 2
          Visible = False
          object Label4: TLabel
            Left = 11
            Top = 8
            Width = 23
            Height = 13
            Caption = 'Note'
          end
          object edNote: TEdit
            Left = 38
            Top = 4
            Width = 166
            Height = 21
            TabOrder = 0
            OnChange = edNoteChange
          end
        end
      end
    end
    object tsColumns: TTabSheet
      Caption = 'Columns'
      ImageIndex = 2
      object GroupBox2: TGroupBox
        Left = 6
        Top = 3
        Width = 615
        Height = 474
        Caption = 'Columns'
        TabOrder = 0
        object lvColumn: TListView
          Left = 10
          Top = 17
          Width = 511
          Height = 445
          Columns = <
            item
              Caption = 'Title'
              Width = 200
            end
            item
              AutoSize = True
              Caption = 'Field'
            end
            item
              Caption = 'Sum'
              Width = 40
            end>
          HideSelection = False
          ReadOnly = True
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
          OnDblClick = btnEditColumnClick
        end
        object btnAddColumn: TButton
          Left = 530
          Top = 19
          Width = 75
          Height = 25
          Caption = 'Add'
          TabOrder = 1
          OnClick = btnAddColumnClick
        end
        object btnEditColumn: TButton
          Left = 530
          Top = 47
          Width = 75
          Height = 25
          Caption = 'Edit'
          TabOrder = 2
          OnClick = btnEditColumnClick
        end
        object btnDeleteColumn: TButton
          Left = 530
          Top = 75
          Width = 75
          Height = 25
          Caption = 'Delete'
          TabOrder = 3
          OnClick = btnDeleteColumnClick
        end
        object btnUpColumn: TButton
          Left = 530
          Top = 115
          Width = 37
          Height = 25
          Caption = 'Up'
          TabOrder = 4
          OnClick = btnUpColumnClick
        end
        object btnDownColumn: TButton
          Left = 568
          Top = 115
          Width = 37
          Height = 25
          Caption = 'Down'
          TabOrder = 5
          OnClick = btnDownColumnClick
        end
      end
    end
    object tsGroup: TTabSheet
      Caption = 'Grouping and Sorting'
      ImageIndex = 3
      object GroupBox4: TGroupBox
        Left = 7
        Top = 3
        Width = 614
        Height = 308
        Caption = 'Grouping'
        TabOrder = 0
        object SpeedButton1: TSpeedButton
          Left = 303
          Top = 34
          Width = 24
          Height = 25
          Hint = 'Add Group'
          Caption = '>'
          OnClick = SpeedButton1Click
        end
        object btnDel: TSpeedButton
          Left = 303
          Top = 66
          Width = 24
          Height = 25
          Hint = 'Remove Group'
          Caption = '<'
          OnClick = btnDelClick
        end
        object SpeedButton3: TSpeedButton
          Left = 303
          Top = 99
          Width = 24
          Height = 25
          Hint = 'Group Property'
          Caption = '...'
          OnClick = SpeedButton3Click
        end
        object GroupBox5: TGroupBox
          Left = 11
          Top = 15
          Width = 284
          Height = 282
          Caption = 'Columns'
          TabOrder = 0
          object lbColumns1: TListBox
            Left = 9
            Top = 16
            Width = 265
            Height = 253
            ItemHeight = 13
            Sorted = True
            TabOrder = 0
          end
        end
        object GroupBox6: TGroupBox
          Left = 334
          Top = 15
          Width = 268
          Height = 120
          Caption = 'Groups list'
          TabOrder = 1
          object lbGroup: TListBox
            Left = 10
            Top = 16
            Width = 249
            Height = 94
            ItemHeight = 13
            TabOrder = 0
          end
        end
        object GroupBox9: TGroupBox
          Left = 334
          Top = 141
          Width = 268
          Height = 156
          Caption = 'Group Summary and Total Summary'
          TabOrder = 2
          object Label3: TLabel
            Left = 8
            Top = 46
            Width = 86
            Height = 13
            Caption = 'Summary Columns'
          end
          object chbCount: TCheckBox
            Left = 8
            Top = 19
            Width = 97
            Height = 17
            Caption = 'Count Records'
            TabOrder = 0
            Visible = False
            OnClick = chbCountClick
          end
          object chlSum: TCheckListBox
            Left = 10
            Top = 63
            Width = 248
            Height = 81
            OnClickCheck = chlSumClickCheck
            ItemHeight = 13
            TabOrder = 1
          end
        end
      end
      object GroupBox8: TGroupBox
        Left = 7
        Top = 318
        Width = 614
        Height = 158
        Caption = 'Sorting'
        TabOrder = 1
        object btnAdd: TSpeedButton
          Left = 303
          Top = 61
          Width = 24
          Height = 25
          Caption = '>'
          OnClick = btnAddClick
        end
        object SpeedButton2: TSpeedButton
          Left = 303
          Top = 93
          Width = 24
          Height = 25
          Caption = '<'
          OnClick = SpeedButton2Click
        end
        object GroupBox3: TGroupBox
          Left = 11
          Top = 16
          Width = 284
          Height = 131
          Caption = 'Columns'
          TabOrder = 0
          object lbColumns2: TListBox
            Left = 9
            Top = 16
            Width = 265
            Height = 104
            ItemHeight = 13
            Sorted = True
            TabOrder = 0
          end
        end
        object GroupBox7: TGroupBox
          Left = 334
          Top = 16
          Width = 268
          Height = 131
          Caption = 'Sort list'
          TabOrder = 1
          object lbSort: TListBox
            Left = 10
            Top = 16
            Width = 248
            Height = 103
            ItemHeight = 13
            TabOrder = 0
          end
        end
      end
    end
  end
  object BitBtn1: TBitBtn
    Left = 468
    Top = 510
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
    OnClick = BitBtn1Click
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object BitBtn2: TBitBtn
    Left = 551
    Top = 510
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333000033338833333333333333333F333333333333
      0000333911833333983333333388F333333F3333000033391118333911833333
      38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
      911118111118333338F3338F833338F3000033333911111111833333338F3338
      3333F8330000333333911111183333333338F333333F83330000333333311111
      8333333333338F3333383333000033333339111183333333333338F333833333
      00003333339111118333333333333833338F3333000033333911181118333333
      33338333338F333300003333911183911183333333383338F338F33300003333
      9118333911183333338F33838F338F33000033333913333391113333338FF833
      38F338F300003333333333333919333333388333338FFF830000333333333333
      3333333333333333333888330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object dsClients: TisRWClientDataSet
    Left = 367
    Top = 34
  end
end
