inherited rwWFCondIdent: TrwWFCondIdent
  Width = 442
  Height = 61
  inherited lName: TLabel
    Height = 61
  end
  inherited Panel1: TPanel
    Width = 412
    Height = 61
    inherited cbOper: TComboBox
      Width = 45
      Enabled = False
    end
    object chbValues: TCheckListBox
      Left = 69
      Top = 0
      Width = 236
      Height = 57
      ItemHeight = 13
      TabOrder = 1
    end
  end
  object dsValues: TisRWClientDataSet
    Active = True
    FieldDefs = <
      item
        Name = 'Name'
        DataType = ftString
        Size = 64
      end
      item
        Name = 'Value'
        DataType = ftString
        Size = 8
      end>
    Left = 63
    Top = 26
    object dsValuesName: TStringField
      DisplayWidth = 64
      FieldName = 'Name'
      Size = 64
    end
    object dsValuesValue: TStringField
      DisplayWidth = 8
      FieldName = 'Value'
      Visible = False
      Size = 8
    end
  end
end
