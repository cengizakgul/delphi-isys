// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWFCondDateFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWFCondFrm, ComCtrls, StdCtrls, ExtCtrls, Mask;

type
  TrwWFCondDate = class(TrwWFCond)
    pnDatE: TPanel;
    Label1: TLabel;
    deDatB: TMaskEdit;
    deDatE: TMaskEdit;
    procedure cbOperChange(Sender: TObject);
  public
    procedure PutCond; override;
    procedure GetCond; override;
    procedure AfterConstruction; override;
  end;

implementation

{$R *.DFM}


procedure TrwWFCondDate.AfterConstruction;
begin
  inherited;
  cbOper.Items.Add('Between');
end;

procedure TrwWFCondDate.GetCond;
begin
  Condition.Value1 := deDatB.Text;
  Condition.Value2 := deDatE.Text;
  Condition.Operation := cbOper.Text;
  Condition.DisplayExpression := Condition.Field.DisplayName+' '+Condition.Operation+
    ' '+'"'+Condition.Value1+'"';
  if Condition.Operation = 'Between' then
    Condition.DisplayExpression := Condition.DisplayExpression + '  and  '+ '"'+Condition.Value2+'"';
end;


procedure TrwWFCondDate.PutCond;
begin
  cbOper.ItemIndex := cbOper.Items.IndexOf(Condition.Operation);
  if cbOper.ItemIndex = -1 then
    cbOper.ItemIndex := 0;
  cbOper.OnChange(nil);

  if Condition.Value1 = '' then
    deDatB.Text := DateToStr(Date)
  else
    deDatB.Text := Condition.Value1;

  if Condition.Value2 = '' then
    deDatE.Text := DateToStr(Date)
  else
    deDatE.Text := Condition.Value2;
end;


procedure TrwWFCondDate.cbOperChange(Sender: TObject);
begin
  inherited;
  pnDatE.Visible := (cbOper.Text = 'Between');
end;

end.
