// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWPParListFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWPParamFrm, Db, StdCtrls, wwdblook, Buttons, CheckLst,
  rwDataDictionary, Variants, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  rwDesignClasses, rwUtils;

type
  TrwWPParList = class(TrwWPParam)
    cbValues: TwwDBLookupCombo;
    dsValues: TisRWClientDataSet;
    dsValuesName: TStringField;
    dsValuesValue: TStringField;
    chbValues: TCheckListBox;
    sbOr: TSpeedButton;
    procedure sbOrClick(Sender: TObject);
    procedure cbValuesExit(Sender: TObject);
  protected
    function  GetValue: string; override;
    procedure SetValue(const Value: string); override;
    procedure  TurnOnOff(AStatus: Boolean); override;
  public
    procedure Initialize(AValues: string);
  end;

implementation

{$R *.DFM}

{ TrwWPParam1 }

function TrwWPParList.GetValue: string;
var
  i: Integer;
  h: String;
begin
  if sbOr.Down then
  begin
    h := '';
    dsValues.DisableControls;
    for i := 0 to chbValues.Items.Count-1 do
      if chbValues.Checked[i] then
      begin
        dsValues.First;
        dsValues.MoveBy(i);
        h := h+','+dsValues.FieldByName('value').AsString;
      end;
    Delete(h, 1, 1);
    dsValues.EnableControls;
    Result := h;
  end
  else
    Result := cbValues.Value;
end;

procedure TrwWPParList.SetValue(const Value: string);
var
  L: TStringList;
  i: Integer;
begin
  if Pos(',', Value) > 0 then
  begin
    sbOr.Down := True;
    sbOr.Click;
    L := TStringList.Create;
    try
      L.CommaText := Value;
      for i := 0 to L.Count-1 do
        if dsValues.Locate('value', VarArrayOf([L[i]]), []) then
          chbValues.Checked[dsValues.RecNo-1] := True;
    finally
      L.Free;
    end;
  end
  else
    cbValues.Value := Value;
end;

procedure TrwWPParList.Initialize(AValues: string);
var
  L: TStringList;
  i: Integer;
begin
  dsValues.Close;
  dsValues.CreateDataSet;
  dsValues.Open;

  L := TStringList.Create;
  try
    L.Text := AValues;

    chbValues.Items.Clear;
    for i := 0 to L.Count - 1 do
    begin
      dsValues.Append;
      dsValues.FieldByName('Value').AsString := L.Names[i];
      dsValues.FieldByName('Name').AsString := L.Values[L.Names[i]];
      chbValues.Items.Add(dsValues.FieldByName('Name').AsString);
      dsValues.Post;
    end;

  finally
    L.Free;
  end;
end;

procedure TrwWPParList.sbOrClick(Sender: TObject);
begin
  cbValues.Visible := not sbOr.Down;
  chbValues.Visible := sbOr.Down;
  if sbOr.Down then
    Label1.Top := chbValues.Top+chbValues.Height
  else
    Label1.Top := cbValues.Top+cbValues.Height
end;

procedure TrwWPParList.cbValuesExit(Sender: TObject);
begin
  DoChange;
end;

procedure TrwWPParList.TurnOnOff(AStatus: Boolean);
begin
  inherited;
  if AStatus then
    sbOr.Click;
end;

end.
