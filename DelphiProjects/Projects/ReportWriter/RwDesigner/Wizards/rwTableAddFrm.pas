// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwTableAddFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwDataDictionary, rwTypes, ComCtrls, ImgList,
  rwWizardInfo, rwWizDM, rwUtils;

type
  TrwTableAdd = class(TForm)
    btnOk: TButton;
    btnCancel: TButton;
    grbTbls: TGroupBox;
    lvTables: TListView;
    meDescr: TMemo;
    lDescr: TLabel;
    Label3: TLabel;
    lFields: TLabel;
    lvFields: TListView;
    procedure lvTablesChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure lvTablesDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lvTablesKeyPress(Sender: TObject; var Key: Char);
    procedure lvFieldsChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure lvTablesEnter(Sender: TObject);
    procedure lvFieldsEnter(Sender: TObject);
  private
    FPreviosObjects: TList;
    FCurrentParent: TrwDatadictTable;
    FFields: String;
    procedure CreateTableList(AObject: TrwDatadictTable = nil);
    procedure ShowPath;
    procedure ShowFields(AObject: TrwDatadictTable);
  public
  end;

  function AddTable(ATable: TrwWizardQueryTable; AFields: String = ''): Boolean; overload;

implementation

uses rwCustomDataDictionary;

{$R *.DFM}


function AddTable(ATable: TrwWizardQueryTable; AFields: String = ''): Boolean;
var
  Frm: TrwTableAdd;
  WT: TrwWizardQueryTable;
  i: Integer;

  function AddTbl(T1: TrwDatadictTable; T2: TrwWizardQueryTable): TrwWizardQueryTable;
  begin
    if not Assigned(T2.Table) then
    begin
      WizDM.InitTable(T1, T2);
      Result := T2;
    end
    else
    begin
      Result := T2.Tables.Add;
      WizDM.InitTable(T1, Result);
    end;
  end;

begin
  Frm := TrwTableAdd.Create(nil);
  with Frm do
    try
      FCurrentParent := ATable.Table;
      FFields :=  AFields;
      CreateTableList(ATable.Table);
      ShowPath;
      Result := (ShowModal = mrOK) and Assigned(lvTables.Selected);
      if Result then
      begin
        WT := ATable;
        for i := 1 to FPreviosObjects.Count-1 do
          if Assigned(FPreviosObjects[i]) then
            WT := AddTbl(TrwDatadictTable(FPreviosObjects[i]), WT);

        if (FPreviosObjects.Count > 0) and Assigned(FCurrentParent) then
          WT := AddTbl(FCurrentParent, WT);

        if not((FPreviosObjects.Count > 0) and (FPreviosObjects[FPreviosObjects.Count-1] = lvTables.Selected.Data) or
               (lvTables.Selected.Data = FCurrentParent)) then
          AddTbl(TrwDatadictTable(lvTables.Selected.Data), WT);
      end;
    finally
      Free;
    end;
end;



procedure TrwTableAdd.CreateTableList(AObject: TrwDatadictTable = nil);
var
  i, j: Integer;
  LI: TListItem;
  L: TStringList;
  f: Boolean;
  FK: TDataDictForeignKey;
begin
  lvTables.Items.BeginUpdate;
  L := TStringList.Create;
  try
    L.CommaText := FFields;
    lvTables.Items.Clear;
    if not Assigned(AObject) then
    begin
      for i :=0 to DataDictionary.Tables.Count-1 do
        if (DataDictionary.Tables[i].DisplayName <> '') and
           not Assigned(lvTables.FindData(-1, DataDictionary.Tables[i], False, False)) then
        begin
          if L.Count > 0 then
          begin
            f := False;
            for j := 0 to L.Count - 1 do
            begin
              if DataDictionary.Tables[i].Fields.FieldByName(L[j]) <> nil then
              begin
                f := True;
                break;
              end;
            end;

            if not f then
              Continue;
          end;

          LI := lvTables.Items.Add;
          LI.Caption := DataDictionary.Tables[i].DisplayName;
          if not AnsiSameText(LI.Caption, TrwDataDictTable(DataDictionary.Tables[i]).GroupName) then
            LI.Caption := LI.Caption + '  [' +  TrwDataDictTable(DataDictionary.Tables[i]).GroupName + ']';

          LI.Data := DataDictionary.Tables[i];
          LI.ImageIndex := 0;
        end;
    end

    else
    begin
      if FPreviosObjects.Count > 0 then
      begin
        LI := lvTables.Items.Add;
        LI.Caption := '..';
        LI.Data := AObject;
        LI.ImageIndex := 1;
      end;

      for i :=0 to DataDictionary.Tables.Count-1 do
        if DataDictionary.Tables[i].DisplayName <> '' then
        begin
          FK := DataDictionary.Tables[i].ForeignKeys.ForeignKeyByTable(AObject);
          if (FK <> nil) and (DataDictionary.Tables[i].ForeignKeys.ForeignKeyByTable(AObject, FK.Index + 1) = nil) and
              not Assigned(lvTables.FindData(-1, DataDictionary.Tables[i], False, False)) then
          begin
            LI := lvTables.Items.Add;
            LI.Caption := DataDictionary.Tables[i].DisplayName;
            LI.Data := DataDictionary.Tables[i];
            LI.ImageIndex := 0;
          end;
        end;
    end;

  finally
    L.Free;
    lvTables.Items.EndUpdate;
  end;
end;

procedure TrwTableAdd.lvTablesChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  if lvTables.Focused then
  begin
    lDescr.Caption := 'Description of table ';
    if Assigned(lvTables.Selected) then
    begin
      meDescr.Text := TrwDataDictTable(lvTables.Selected.Data).Notes;
      lDescr.Caption := lDescr.Caption+TrwDataDictTable(lvTables.Selected.Data).DisplayName;
    end
    else
      meDescr.Text := '';
  end;

  if Assigned(lvTables.Selected) then
    ShowFields(TrwDataDictTable(lvTables.Selected.Data));
end;

procedure TrwTableAdd.lvTablesDblClick(Sender: TObject);
var
  P: TrwDataDictTable;
begin
  if lvTables.Selected.Caption = '..' then
  begin
    P := FCurrentParent;
    FCurrentParent := FPreviosObjects[FPreviosObjects.Count-1];
    CreateTableList(FCurrentParent);
    FPreviosObjects.Delete(FPreviosObjects.Count-1);
    lvTables.Selected := lvTables.FindData(-1, P, False, False);
    lvTables.ItemFocused := lvTables.Selected;
  end
  else
  begin
    FPreviosObjects.Add(FCurrentParent);
    FCurrentParent := TrwDataDictTable(lvTables.Selected.Data);
    CreateTableList(FCurrentParent);
    lvTables.Selected := lvTables.TopItem;
    lvTables.ItemFocused := lvTables.Selected;
  end;

  ShowPath;
end;

procedure TrwTableAdd.FormCreate(Sender: TObject);
begin
  FPreviosObjects := TList.Create;
end;

procedure TrwTableAdd.FormDestroy(Sender: TObject);
begin
  FPreviosObjects.Free;
end;

procedure TrwTableAdd.lvTablesKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    lvTables.OnDblClick(nil);
end;

procedure TrwTableAdd.ShowPath;
var
  i: Integer;
begin
  grbTbls.Caption := 'Child Tables of \';
  for i := 0 to FPreviosObjects.Count -1 do
    if Assigned(FPreviosObjects[i]) then
      grbTbls.Caption := grbTbls.Caption+TrwDataDictTable(FPreviosObjects[i]).DisplayName+'\';

  if Assigned(FCurrentParent) then
    grbTbls.Caption := grbTbls.Caption+FCurrentParent.DisplayName+'\';
end;

procedure TrwTableAdd.ShowFields(AObject: TrwDatadictTable);
var
  i, j: Integer;
  LI: TListItem;
  O: TrwDataDictTable;
begin
  lvFields.Items.BeginUpdate;
  lvFields.Items.Clear;
  for i :=0 to AObject.Fields.Count-1 do
  begin
    if AObject.Fields[i].DisplayName = '' then
      Continue;
      
    LI := lvFields.Items.Add;
    LI.Caption := AObject.Fields[i].DisplayName;
    LI.Data := AObject.Fields[i];
    LI.ImageIndex := 2;
    for j := 0 to AObject.ForeignKeys.Count-1 do
    begin
      if AObject.ForeignKeys[j].Fields.FindField(AObject.Fields[i]) <> nil then
      begin
        O := TrwDataDictTable(AObject.ForeignKeys[j].Table);
        if not AnsiSameText(LI.Caption, O.GroupName) then
          LI.Caption := LI.Caption + '  [' + O.GroupName + ']';
        LI.ImageIndex := 3;
        break;
      end;
    end;
  end;
  lFields.Caption := 'Fields of '+AObject.DisplayName;
  lvFields.Items.EndUpdate;
end;

procedure TrwTableAdd.lvFieldsChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  if lvFields.Focused then
  begin
    lDescr.Caption := 'Description of field ';
    if Assigned(lvFields.Selected) then
    begin
      meDescr.Text := TrwDataDictField(lvFields.Selected.Data).Notes;
      lDescr.Caption := lDescr.Caption+TrwDataDictField(lvFields.Selected.Data).DisplayName;
    end
    else
      meDescr.Text := '';
  end;
end;

procedure TrwTableAdd.lvTablesEnter(Sender: TObject);
begin
  lvTables.OnChange(lvTables, lvTables.Selected, ctState);
end;

procedure TrwTableAdd.lvFieldsEnter(Sender: TObject);
begin
  lvFields.OnChange(lvFields, lvFields.Selected, ctState);
end;

end.
