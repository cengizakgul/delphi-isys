// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWPParamFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwDataDictionary;

type
  TrwWPParam = class(TFrame)
    chbInFrm: TCheckBox;
    Label1: TLabel;
    lName: TLabel;
    chbUse: TCheckBox;
    procedure chbInFrmExit(Sender: TObject);
    procedure chbUseClick(Sender: TObject);
    procedure lNameClick(Sender: TObject);
  private
    FParam: TrwDataDictParam;
    FOnChange: TNotifyEvent;
    function GetForInputForm: Boolean;
    function GetEmptyParam: Boolean;
  protected
    function   GetValue: string; virtual; abstract;
    procedure  SetValue(const Value: string); virtual; abstract;
    procedure  TurnOnOff(AStatus: Boolean); virtual;
  public
    property Param: TrwDataDictParam read FParam write FParam;
    property Value: String read GetValue write SetValue;
    property ForInputForm: Boolean read GetForInputForm;
    property EmptyParam: Boolean read GetEmptyParam;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    procedure DoChange;
  end;

implementation

{$R *.DFM}


{ TrwWPParam }

procedure TrwWPParam.DoChange;
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

function TrwWPParam.GetForInputForm: Boolean;
begin
  Result := chbInFrm.Checked;
end;

procedure TrwWPParam.chbInFrmExit(Sender: TObject);
begin
  DoChange;
end;

procedure TrwWPParam.chbUseClick(Sender: TObject);
begin
  TurnOnOff(chbUse.Checked);
end;

procedure TrwWPParam.TurnOnOff(AStatus: Boolean);
var
  i: Integer;
begin
  for i := 0 to ControlCount - 1 do
    if (Controls[i] <> chbUse) and (Controls[i] <> lName) then
      Controls[i].Visible := AStatus;
end;

function TrwWPParam.GetEmptyParam: Boolean;
begin
  Result := not chbUse.Checked;
end;

procedure TrwWPParam.lNameClick(Sender: TObject);
begin
  chbUse.Checked := not chbUse.Checked;
  chbUse.OnClick(nil);
end;

end.
