object rwFilterEdit: TrwFilterEdit
  Left = 274
  Top = 111
  BorderStyle = bsDialog
  Caption = 'Extra Filter'
  ClientHeight = 339
  ClientWidth = 642
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    642
    339)
  PixelsPerInch = 96
  TextHeight = 13
  object btnAdd: TSpeedButton
    Left = 258
    Top = 101
    Width = 26
    Height = 25
    Caption = '>'
    OnClick = btnAddClick
  end
  object btnDel: TSpeedButton
    Left = 258
    Top = 132
    Width = 26
    Height = 25
    Caption = '<'
    OnClick = btnDelClick
  end
  object btnEdit: TSpeedButton
    Left = 258
    Top = 162
    Width = 26
    Height = 25
    Caption = '...'
    OnClick = btnEditClick
  end
  object lName: TLabel
    Left = 0
    Top = 6
    Width = 641
    Height = 17
    Alignment = taCenter
    AutoSize = False
    Caption = 'lName'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object grbFields: TGroupBox
    Left = 8
    Top = 31
    Width = 242
    Height = 273
    Caption = 'Fields'
    TabOrder = 0
    object lbFields: TListBox
      Left = 9
      Top = 16
      Width = 222
      Height = 247
      ItemHeight = 13
      Sorted = True
      TabOrder = 0
    end
  end
  object GroupBox3: TGroupBox
    Left = 292
    Top = 31
    Width = 345
    Height = 273
    Caption = 'Filter'
    TabOrder = 1
    object lbFilter: TListBox
      Left = 9
      Top = 16
      Width = 328
      Height = 247
      ItemHeight = 13
      Sorted = True
      TabOrder = 0
      OnDblClick = btnEditClick
    end
  end
  object btnOk: TButton
    Left = 478
    Top = 310
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
  end
  object btnCancel: TButton
    Left = 562
    Top = 310
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
