// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwGroupEditFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwWizardInfo;

type
  TrwGroupEdit = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    chbHeader: TCheckBox;
    chbFooter: TCheckBox;
    chbBreakPage: TCheckBox;
    lColName: TLabel;
    btnOk: TButton;
    btnCancel: TButton;
  private
  public
  end;

  function EditGroup(AColumn: TrwWizTblColumn; AOwner: TForm): Boolean;

implementation

{$R *.DFM}


function EditGroup(AColumn: TrwWizTblColumn; AOwner: TForm): Boolean;
var
  F: TrwGroupEdit;
begin
  F := TrwGroupEdit.Create(AOwner);
  with F do
    try
      lColName.Caption := AColumn.Title;
      chbHeader.Checked := AColumn.GroupHeader;
      chbFooter.Checked := AColumn.GroupFooter;
      chbBreakPage.Checked := AColumn.GroupPageBreak;

      Result := (ShowModal = mrOK);
      if Result then
      begin
        AColumn.GroupHeader := chbHeader.Checked;
        AColumn.GroupFooter := chbFooter.Checked;
        AColumn.GroupPageBreak := chbBreakPage.Checked;
      end;
    finally
      Free;
    end;
end;

end.
