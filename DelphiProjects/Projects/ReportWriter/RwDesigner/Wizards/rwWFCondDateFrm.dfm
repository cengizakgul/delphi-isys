inherited rwWFCondDate: TrwWFCondDate
  Width = 390
  Height = 23
  inherited lName: TLabel
    Height = 23
    Layout = tlCenter
  end
  inherited Panel1: TPanel
    Width = 360
    Height = 23
    inherited cbOper: TComboBox
      Left = 13
      OnChange = cbOperChange
    end
    object pnDatE: TPanel
      Left = 187
      Top = 0
      Width = 131
      Height = 23
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 9
        Top = 4
        Width = 19
        Height = 13
        Caption = 'And'
      end
      object deDatE: TMaskEdit
        Left = 41
        Top = 1
        Width = 84
        Height = 21
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        TabOrder = 0
        Text = '  /  /    '
      end
    end
    object deDatB: TMaskEdit
      Left = 100
      Top = 1
      Width = 84
      Height = 21
      EditMask = '99/99/9999;1; '
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
  end
end
