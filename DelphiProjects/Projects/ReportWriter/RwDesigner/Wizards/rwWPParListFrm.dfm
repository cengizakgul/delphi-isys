inherited rwWPParList: TrwWPParList
  Height = 56
  inherited Label1: TLabel
    Top = 20
  end
  object sbOr: TSpeedButton [2]
    Left = 425
    Top = 0
    Width = 23
    Height = 22
    AllowAllUp = True
    GroupIndex = 1
    Caption = 'OR'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = sbOrClick
  end
  inherited chbInFrm: TCheckBox
    Left = 463
    Top = 2
  end
  inherited chbUse: TCheckBox
    TabOrder = 3
  end
  object chbValues: TCheckListBox
    Left = 180
    Top = 0
    Width = 241
    Height = 56
    ItemHeight = 13
    TabOrder = 2
    Visible = False
    OnExit = cbValuesExit
  end
  object cbValues: TwwDBLookupCombo
    Left = 180
    Top = 0
    Width = 241
    Height = 21
    ControlInfoInDataset = False
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'Name'#9'64'#9'Name'#9'F')
    LookupTable = dsValues
    LookupField = 'Value'
    Options = [loColLines]
    Style = csDropDownList
    DropDownCount = 16
    TabOrder = 1
    AutoDropDown = False
    ShowButton = True
    UseTFields = False
    PreciseEditRegion = False
    AllowClearKey = True
    ShowMatchText = True
    OnExit = cbValuesExit
  end
  object dsValues: TisRWClientDataSet
    Active = True
    FieldDefs = <
      item
        Name = 'Name'
        DataType = ftString
        Size = 64
      end
      item
        Name = 'Value'
        DataType = ftString
        Size = 8
      end>
    Left = 79
    Top = 2
    object dsValuesName: TStringField
      DisplayWidth = 64
      FieldName = 'Name'
      Size = 64
    end
    object dsValuesValue: TStringField
      DisplayWidth = 8
      FieldName = 'Value'
      Visible = False
      Size = 8
    end
  end
end
