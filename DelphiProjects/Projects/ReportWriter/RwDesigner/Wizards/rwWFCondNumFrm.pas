// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWFCondNumFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWFCondFrm, StdCtrls, Mask, wwdbedit, Wwdbspin, ExtCtrls;

type
  TrwWFCondNum = class(TrwWFCond)
    edNum1: TwwDBSpinEdit;
    pnNum: TPanel;
    Label1: TLabel;
    edNum2: TwwDBSpinEdit;
    procedure cbOperChange(Sender: TObject);
  private
  public
    procedure PutCond; override;
    procedure GetCond; override;
    procedure AfterConstruction; override;
  end;

implementation

{$R *.DFM}

{ TrwWFCond1 }

procedure TrwWFCondNum.AfterConstruction;
begin
  inherited;
  cbOper.Items.Add('Between');
end;

procedure TrwWFCondNum.GetCond;
begin
  Condition.Value1 := FloatToStr(edNum1.Value);
  Condition.Value2 := FloatToStr(edNum2.Value);
  Condition.Operation := cbOper.Text;
  Condition.DisplayExpression := Condition.Field.DisplayName+' '+Condition.Operation+
    ' '+Condition.Value1;
  if Condition.Operation = 'Between' then
    Condition.DisplayExpression := Condition.DisplayExpression + '  and  '+Condition.Value2;
end;

procedure TrwWFCondNum.PutCond;
begin
  cbOper.ItemIndex := cbOper.Items.IndexOf(Condition.Operation);
  if cbOper.ItemIndex = -1 then
    cbOper.ItemIndex := 0;
  cbOper.OnChange(nil);

  if Condition.Value1 = '' then
    edNum1.Value := 0
  else
    edNum1.Value := StrToFloat(Condition.Value1);

  if Condition.Value2 = '' then
    edNum2.Value := 0
  else
    edNum2.Value := StrToFloat(Condition.Value2);
end;

procedure TrwWFCondNum.cbOperChange(Sender: TObject);
begin
  inherited;
  pnNum.Visible := (cbOper.Text = 'Between');
end;

end.
