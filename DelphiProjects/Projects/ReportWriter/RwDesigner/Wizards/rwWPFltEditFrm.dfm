object rwWPFltEdit: TrwWPFltEdit
  Left = 285
  Top = 220
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Edit Condition'
  ClientHeight = 137
  ClientWidth = 467
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 298
    Top = 105
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object Button2: TButton
    Left = 386
    Top = 105
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object rgrOper: TRadioGroup
    Left = 5
    Top = 96
    Width = 226
    Height = 37
    Caption = 'Operation for joining with next condition'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'AND'
      'OR')
    TabOrder = 2
  end
  object grbCond: TGroupBox
    Left = 5
    Top = 4
    Width = 457
    Height = 86
    Caption = 'Condition'
    TabOrder = 3
  end
end
