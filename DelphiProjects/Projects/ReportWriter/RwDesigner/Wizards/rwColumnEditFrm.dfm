object rwColumnEdit: TrwColumnEdit
  Left = 347
  Top = 185
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Column'
  ClientHeight = 461
  ClientWidth = 650
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pnBtn: TPanel
    Left = 0
    Top = 420
    Width = 650
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      650
      41)
    object btnOk: TButton
      Left = 484
      Top = 7
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 568
      Top = 7
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 339
    Top = 0
    Width = 311
    Height = 420
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      311
      420)
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 305
      Height = 418
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Data Field'
      TabOrder = 0
      object Label6: TLabel
        Left = 9
        Top = 14
        Width = 27
        Height = 13
        Caption = 'Table'
      end
      object Label7: TLabel
        Left = 9
        Top = 153
        Width = 22
        Height = 13
        Caption = 'Field'
      end
      object lvTables: TTreeView
        Left = 9
        Top = 28
        Width = 287
        Height = 114
        HideSelection = False
        Images = WizDM.ImageList
        Indent = 19
        ReadOnly = True
        ShowRoot = False
        StateImages = WizDM.ImageList
        TabOrder = 0
        OnChange = lvTablesChange
      end
      object lvFields: TTreeView
        Left = 9
        Top = 168
        Width = 287
        Height = 239
        HideSelection = False
        Images = WizDM.ImageList
        Indent = 19
        ReadOnly = True
        TabOrder = 1
        OnChange = lvFieldsChange
        OnEnter = lvFieldsEnter
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 339
    Height = 420
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 2
    object grbExpr: TGroupBox
      Left = 4
      Top = 259
      Width = 329
      Height = 159
      TabOrder = 0
      object Label5: TLabel
        Left = 8
        Top = 14
        Width = 51
        Height = 13
        Caption = 'Expression'
      end
      object btnAdd: TSpeedButton
        Left = 274
        Top = 37
        Width = 23
        Height = 22
        Caption = '+'
        Font.Charset = ANSI_CHARSET
        Font.Color = clMaroon
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnAddClick
      end
      object btnSub: TSpeedButton
        Left = 298
        Top = 37
        Width = 23
        Height = 22
        Caption = '-'
        Font.Charset = ANSI_CHARSET
        Font.Color = clMaroon
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnAddClick
      end
      object btnMul: TSpeedButton
        Left = 274
        Top = 61
        Width = 23
        Height = 22
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clMaroon
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnAddClick
      end
      object btnDiv: TSpeedButton
        Left = 298
        Top = 61
        Width = 23
        Height = 22
        Caption = '/'
        Font.Charset = ANSI_CHARSET
        Font.Color = clMaroon
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnAddClick
      end
      object btnLPar: TSpeedButton
        Left = 274
        Top = 85
        Width = 23
        Height = 22
        Caption = '('
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnAddClick
      end
      object btnRPar: TSpeedButton
        Left = 298
        Top = 85
        Width = 23
        Height = 22
        Caption = ')'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnAddClick
      end
      object btnBack: TSpeedButton
        Left = 274
        Top = 12
        Width = 47
        Height = 22
        Caption = 'Back'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnBackClick
      end
      object btnPut: TSpeedButton
        Left = 274
        Top = 131
        Width = 47
        Height = 22
        Caption = 'Put'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnPutClick
      end
      object btnFunct: TSpeedButton
        Left = 274
        Top = 108
        Width = 47
        Height = 22
        Caption = 'Funct'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnFunctClick
      end
      object memExpr: TMemo
        Left = 8
        Top = 29
        Width = 261
        Height = 100
        ReadOnly = True
        TabOrder = 0
      end
      object edExpr: TEdit
        Left = 8
        Top = 132
        Width = 261
        Height = 21
        TabOrder = 1
        OnChange = edExprChange
      end
    end
    object grbTitleFormat: TGroupBox
      Left = 4
      Top = 1
      Width = 329
      Height = 65
      Caption = 'Title Format'
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 17
        Width = 54
        Height = 13
        Caption = 'Column title'
      end
      object edTitle: TEdit
        Left = 68
        Top = 15
        Width = 251
        Height = 21
        TabOrder = 0
      end
      object chbTitleWrap: TCheckBox
        Left = 8
        Top = 41
        Width = 123
        Height = 17
        Caption = 'Wrap title by words'
        TabOrder = 1
      end
    end
    object grbColFormat: TGroupBox
      Left = 4
      Top = 73
      Width = 329
      Height = 128
      Caption = 'Column Format'
      TabOrder = 2
      object Label2: TLabel
        Left = 8
        Top = 19
        Width = 49
        Height = 13
        Caption = 'Width (In.)'
      end
      object Label4: TLabel
        Left = 8
        Top = 45
        Width = 46
        Height = 13
        Caption = 'Alignment'
      end
      object Format: TLabel
        Left = 8
        Top = 74
        Width = 32
        Height = 13
        Caption = 'Format'
      end
      object lSize: TLabel
        Left = 8
        Top = 99
        Width = 48
        Height = 13
        Caption = 'ASCII size'
      end
      object Label3: TLabel
        Left = 187
        Top = 16
        Width = 85
        Height = 13
        Caption = 'Background Color'
      end
      object SpeedButton1: TSpeedButton
        Left = 298
        Top = 12
        Width = 23
        Height = 22
        Enabled = False
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00559999999995
          5555557777777775F5555559999999505555555777777757FFF5555555555550
          0955555555555FF7775F55555555995501955555555577557F75555555555555
          01995555555555557F5755555555555501905555555555557F57555555555555
          0F905555555555557FF75555555555500005555555555557777555555555550F
          F05555555555557F57F5555555555008F05555555555F775F755555555570000
          05555555555775577555555555700007555555555F755F775555555570000755
          55555555775F77555555555700075555555555F75F7755555555570007555555
          5555577F77555555555500075555555555557777555555555555}
        NumGlyphs = 2
      end
      object Shape1: TShape
        Left = 277
        Top = 15
        Width = 16
        Height = 16
      end
      object Label8: TLabel
        Left = 225
        Top = 40
        Width = 48
        Height = 13
        Caption = 'Text Color'
      end
      object Shape2: TShape
        Left = 277
        Top = 39
        Width = 16
        Height = 16
        Brush.Color = clBlack
      end
      object SpeedButton2: TSpeedButton
        Left = 298
        Top = 36
        Width = 23
        Height = 22
        Enabled = False
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00559999999995
          5555557777777775F5555559999999505555555777777757FFF5555555555550
          0955555555555FF7775F55555555995501955555555577557F75555555555555
          01995555555555557F5755555555555501905555555555557F57555555555555
          0F905555555555557FF75555555555500005555555555557777555555555550F
          F05555555555557F57F5555555555008F05555555555F775F755555555570000
          05555555555775577555555555700007555555555F755F775555555570000755
          55555555775F77555555555700075555555555F75F7755555555570007555555
          5555577F77555555555500075555555555557777555555555555}
        NumGlyphs = 2
      end
      object edWidth: TwwDBSpinEdit
        Left = 68
        Top = 16
        Width = 79
        Height = 21
        Increment = 0.100000000000000000
        Value = 1.000000000000000000
        TabOrder = 0
        UnboundDataType = wwDefault
      end
      object cbAlignment: TComboBox
        Left = 68
        Top = 43
        Width = 80
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        Items.Strings = (
          'Left'
          'Right'
          'Center')
      end
      object cbFormat: TComboBox
        Left = 68
        Top = 70
        Width = 116
        Height = 21
        ItemHeight = 13
        TabOrder = 2
        Items.Strings = (
          '0'
          '0.00'
          '#,##0.00'
          'mm/dd/yyyy'
          'mmm dd yyyy')
      end
      object edSize: TwwDBSpinEdit
        Left = 68
        Top = 97
        Width = 62
        Height = 21
        Increment = 1.000000000000000000
        TabOrder = 3
        UnboundDataType = wwDefault
      end
    end
    object chbCalcCol: TCheckBox
      Left = 12
      Top = 257
      Width = 106
      Height = 15
      Caption = 'Calculated column'
      TabOrder = 3
      OnClick = chbCalcColClick
    end
    object GroupBox2: TGroupBox
      Left = 4
      Top = 207
      Width = 329
      Height = 44
      Caption = 'Misc'
      TabOrder = 4
      object chbSummarized: TCheckBox
        Left = 8
        Top = 16
        Width = 122
        Height = 17
        Caption = 'Summarize column'
        TabOrder = 0
      end
      object chbSubstAbbr: TCheckBox
        Left = 153
        Top = 16
        Width = 168
        Height = 17
        Caption = 'Print text insted of abbreviation'
        Enabled = False
        TabOrder = 1
      end
    end
  end
end
