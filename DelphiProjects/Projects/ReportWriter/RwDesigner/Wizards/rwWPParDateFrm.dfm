inherited rwWPParDate: TrwWPParDate
  Height = 33
  inherited Label1: TLabel
    Top = 20
  end
  inherited chbInFrm: TCheckBox
    Left = 463
    Top = 3
  end
  object deDate: TDateTimePicker [3]
    Left = 180
    Top = 0
    Width = 129
    Height = 21
    Date = 36762.464966620400000000
    Time = 36762.464966620400000000
    TabOrder = 1
    OnExit = deDateExit
  end
  object chbCurDate: TCheckBox [4]
    Left = 316
    Top = 3
    Width = 114
    Height = 17
    Caption = 'Always current date'
    TabOrder = 2
    OnClick = chbCurDateClick
  end
  inherited chbUse: TCheckBox
    TabOrder = 3
  end
end
