// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWPConditionFrm;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwDataDictionary;

type
  TrwWPCondition = class(TFrame)
    lName: TLabel;
  private
    FField: TrwDatadictField;
    function GetValue: String;
    procedure SetValue(const Value: String);
  public
    property Field: TrwDatadictField read FField write FField;
    property Value: String read GetValue write SetValue;
  end;

implementation

{$R *.DFM}

{ TrwWPCondition }

function TrwWPCondition.GetValue: String;
begin

end;

procedure TrwWPCondition.SetValue(const Value: String);
begin

end;

end.
