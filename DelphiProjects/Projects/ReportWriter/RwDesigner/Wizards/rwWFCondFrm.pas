// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWFCondFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwWizardInfo, ExtCtrls;

type
  TrwWFCond = class(TFrame)
    lName: TLabel;
    Panel1: TPanel;
    cbOper: TComboBox;
  private
    FCondition: TrwWizardQueryTblFltCond;
    FOnChange: TNotifyEvent;
//    procedure DoChange;
    procedure SetCondition(const Value: TrwWizardQueryTblFltCond);
  public
    procedure PutCond; virtual; abstract;
    procedure GetCond; virtual; abstract;
    property  Condition: TrwWizardQueryTblFltCond read FCondition write SetCondition;
    property  OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

implementation

{$R *.DFM}

{ TrwWFCond }
{
procedure TrwWFCond.DoChange;
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
end;
}
procedure TrwWFCond.SetCondition(const Value: TrwWizardQueryTblFltCond);
begin
  FCondition := Value;
  lName.Caption := FCondition.Field.DisplayName;
end;

end.
