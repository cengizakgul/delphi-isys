// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwFieldAddFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, rwWizardInfo, rwWizDM, rwDataDictionary, rwTypes,
  ExtCtrls, rwEngineTypes;

type
  TrwFieldAdd = class(TForm)
    btnOk: TButton;
    btnCancel: TButton;
    grbFields: TGroupBox;
    grbSelFields: TGroupBox;
    lvSelFields: TTreeView;
    btnAdd: TSpeedButton;
    btnDel: TSpeedButton;
    grbDescr: TGroupBox;
    meDescr: TMemo;
    lPath: TLabel;
    lvFields: TListView;
    chbOuter: TCheckBox;
    procedure lvFieldsDblClick(Sender: TObject);
    procedure lvFieldsKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lvFieldsChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure btnAddClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure lvSelFieldsChange(Sender: TObject; Node: TTreeNode);
    procedure lvFieldsEnter(Sender: TObject);
    procedure chbOuterClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FPreviosObjects: TList;
    FPreviosAttributes: TList;
    FCurrentParent: TrwDataDictTable;
    FFirstTable: TrwDataDictTable;
    FCurrentAttrParent: TrwDataDictField;
    FPrevStateNodes: TStringList;
    FFieldsForDelete: TStringList;
    procedure CreateFieldList(AObject: TrwDataDictTable);
    procedure ShowPath;
    procedure ShowDescr(AFld: TrwDataDictField);
    procedure SyncSelectedFields(ATbl: TrwWizardQueryTable);
  public
  end;

  function AddField(ATable: TrwWizardQueryTable; AColumns: TrwWizTblColumns; AOwner: TForm): Boolean;

implementation

uses rwCustomDataDictionary;

{$R *.DFM}

function AddField(ATable: TrwWizardQueryTable; AColumns: TrwWizTblColumns; AOwner: TForm): Boolean;
var
  Frm: TrwFieldAdd;
  Fld: TrwWizardQueryTableField;
  i: Integer;
  lCreateColumns: Boolean;
  lAscCreateColumns: Boolean;

  procedure AddFld(ATbl: TrwWizardQueryTable; ANode: TTreeNode);
  var
    i, j, k: Integer;
    F: TrwWizardQueryTableField;
    Obj: TrwDataDictTable;
    Col: TrwWizTblColumn;
  begin
    for i := 0 to ANode.Count-1 do
    begin
      k := Frm.FPrevStateNodes.IndexOfObject(ANode.Item[i]);
      if k = -1 then
      begin
        F := ATbl.Fields.Add;
        F.FieldName := TrwDataDictField(ANode.Item[i].Data).Name;
        if lAscCreateColumns then
        begin
          lCreateColumns := (MessageDlg('Do you want to create columns according selected fields?',
             mtConfirmation, [mbYes, mbNo], 0) = mrYes);
          lAscCreateColumns := False;
        end;

        if lCreateColumns and not((ANode.Item[i].ImageIndex = 3) and ANode.Item[i].HasChildren) then
        begin
          Col := AColumns.Add;
          Col.DataField := F.Alias;
          Col.Title :=  StringReplace(F.Path, '\', ' ', [rfReplaceAll, rfIgnoreCase]);
          Col.Width := 1;
          if F.Field.FieldValues.Text <> '' then
            Col.SubstValue := True;
          case F.Field.FieldType of
            ddtDateTime: Col.Format := 'mm/dd/yyyy';
            ddtInteger:  Col.Format := '0';
            ddtFloat:    begin
                            Col.Format := '0.00';
                            Col.Summarized := True;
                          end;
          end;

          if F.Field.FieldType in [ddtInteger, ddtFloat] then
            Col.Alignment := taRightJustify
          else
            Col.Alignment := taLeftJustify;
        end;

      end
      else
        F := ATbl.Tables.WizQuery.FieldByAlias(Frm.FPrevStateNodes[k]);

      if ANode.Item[i].HasChildren then
      begin
        Obj := TrwDataDictTable(F.Field.Table);
        for j := 0 to Obj.ForeignKeys.Count-1 do
          if Obj.ForeignKeys[j].Fields.FindField(F.Field) <> nil then
          begin
            if k = -1 then
              WizDM.InitTable(TrwDataDictTable(Obj.ForeignKeys[j].Table), F.LookUpTable);
            F.LookUpTable.Outer := ANode.Item[i].Cut;
            break;
          end;
        AddFld(F.LookUpTable, ANode.Item[i]);
      end;
    end;
  end;

begin
  Frm := TrwFieldAdd.Create(nil);
  with Frm do
    try
      FFirstTable := ATable.Table;
      FCurrentParent := ATable.Table;
      grbFields.Caption := 'Fields of '+ATable.Table.DisplayName;
      lvSelFields.Items.AddChildObject(nil, ATable.Table.DisplayName, nil);
      SyncSelectedFields(ATable);
      CreateFieldList(ATable.Table);
      ShowPath;
      Result := (ShowModal = mrOK);
      if Result then
      begin
        for i := 0 to FFieldsForDelete.Count-1 do
        begin
          Fld := ATable.Tables.WizQuery.FieldByAlias(FFieldsForDelete[i]);
          Fld.Free;
        end;

        lAscCreateColumns := True;
        lCreateColumns := True;
        AddFld(ATable, lvSelFields.Items.GetFirstNode);
      end;
    finally
      Free;
    end;
end;

{ TrwFieldAdd }

procedure TrwFieldAdd.CreateFieldList(AObject: TrwDataDictTable);
var
  i, j: Integer;
  LI: TListItem;
begin
  lvFields.Items.BeginUpdate;
  lvFields.Items.Clear;

  if FPreviosObjects.Count > 0 then
  begin
    LI := lvFields.Items.Add;
    LI.Caption := '..';
    LI.Data := FCurrentAttrParent;
    LI.ImageIndex := 1;
  end;

  for i :=0 to AObject.Fields.Count-1 do
  begin
    if AObject.Fields[i].DisplayName = '' then
      Continue;

    LI := lvFields.Items.Add;
    LI.Caption := AObject.Fields[i].DisplayName;
    LI.Data := AObject.Fields[i];
    LI.ImageIndex := 2;
    for j := 0 to AObject.ForeignKeys.Count-1 do
      if AObject.ForeignKeys[j].Fields.FindField(AObject.Fields[i]) <> nil then
      begin
        LI.ImageIndex := 3;
        break;
      end;
  end;
  lvFields.Items.EndUpdate;
end;

procedure TrwFieldAdd.lvFieldsDblClick(Sender: TObject);
var
  P: TrwDataDictTable;
  A: TrwDataDictField;
  j: Integer;
begin
  if not Assigned(lvFields.Selected) then
    Exit;

  if lvFields.Selected.Caption = '..' then
  begin
    A := FCurrentAttrParent;
    FCurrentParent := FPreviosObjects[FPreviosObjects.Count-1];
    FCurrentAttrParent := FPreviosAttributes[FPreviosAttributes.Count-1];
    FPreviosObjects.Delete(FPreviosObjects.Count-1);
    FPreviosAttributes.Delete(FPreviosAttributes.Count-1);
    CreateFieldList(FCurrentParent);
    lvFields.Selected := lvFields.FindData(-1, A, False, False);
    lvFields.ItemFocused := lvFields.Selected;
  end
  else if lvFields.Selected.ImageIndex = 3 then
  begin
    FPreviosObjects.Add(FCurrentParent);
    FPreviosAttributes.Add(FCurrentAttrParent);
    P := TrwDataDictTable(TrwDataDictField(lvFields.Selected.Data).Table);
    for j := 0 to P.ForeignKeys.Count-1 do
      if P.ForeignKeys[j].Fields.FindField(TrwDataDictField(lvFields.Selected.Data)) <> nil then
      begin
        FCurrentParent := TrwDataDictTable(P.ForeignKeys[j].Table);
        FCurrentAttrParent := TrwDataDictField(lvFields.Selected.Data);
        CreateFieldList(FCurrentParent);
        lvFields.Selected := lvFields.Items[0];
        lvFields.ItemFocused := lvFields.Selected;
        break;
      end;
  end
  else
    btnAdd.Click;

  ShowPath;
end;

procedure TrwFieldAdd.lvFieldsKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    lvFields.OnDblClick(nil);
end;

procedure TrwFieldAdd.FormCreate(Sender: TObject);
begin
  FPreviosObjects := TList.Create;
  FPreviosAttributes := TList.Create;
  FFieldsForDelete := TStringList.Create;
  FPrevStateNodes := TStringList.Create;
end;

procedure TrwFieldAdd.FormDestroy(Sender: TObject);
begin
  FPreviosObjects.Free;
  FPreviosAttributes.Free;
  FFieldsForDelete.Free;
  FPrevStateNodes.Free;
end;

procedure TrwFieldAdd.ShowPath;
var
  i: Integer;
begin
  lPath.Caption := '';
  for i := 0 to FPreviosAttributes.Count -1 do
    if Assigned(FPreviosAttributes[i]) then
      lPath.Caption := lPath.Caption+TrwDataDictField(FPreviosAttributes[i]).DisplayName+'\';

  if Assigned(FCurrentAttrParent) then
    lPath.Caption := lPath.Caption+FCurrentAttrParent.DisplayName+'\';

  if lPath.Caption <> '' then
    lPath.Caption := 'Path: \'+lPath.Caption;
end;

procedure TrwFieldAdd.lvFieldsChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  if lvFields.Focused and Assigned(lvFields.Selected) then
    ShowDescr(TrwDataDictField(lvFields.Selected.Data));
end;

procedure TrwFieldAdd.btnAddClick(Sender: TObject);
var
  lTree: TTreeNode;
  i: Integer;
  fl: Boolean;

  procedure CheckNode(Attr: TrwDataDictField);
  var
    j: Integer;
    fl: Boolean;
  begin
    fl := False;
    for j := 0 to lTree.Count-1 do
      if lTree[j].Data = Attr then
      begin
         lTree := lTree[j];
         fl := True;
         break;
      end;

    if not fl then
    begin
      lTree := lvSelFields.Items.AddChildObject(lTree, Attr.DisplayName, Attr);
      lTree.ImageIndex := 3;
      lTree.SelectedIndex := lTree.ImageIndex;
      lTree.StateIndex := lTree.ImageIndex;
    end;
    lTree.Expand(False);
  end;

begin
  if not Assigned(lvFields.Selected) or (lvFields.Selected.Caption = '..') then
    Exit;

  if not Assigned(lvSelFields.Selected) or not Assigned(lvSelFields.Selected.Data) or
     not((FCurrentAttrParent = lvSelFields.Selected.Data) or (FCurrentAttrParent = lvSelFields.Selected.Parent.Data)) then
  begin
    lTree := lvSelFields.Items.GetFirstNode;
    for i := 0 to FPreviosAttributes.Count -1 do
      if Assigned(FPreviosAttributes[i]) then
        CheckNode(TrwDataDictField(FPreviosAttributes[i]));

    if Assigned(FCurrentAttrParent) then
      CheckNode(FCurrentAttrParent);
  end
  else
    if FCurrentAttrParent = lvSelFields.Selected.Data then
      lTree := lvSelFields.Selected
    else
      lTree := lvSelFields.Selected.Parent;

  fl := False;
  for i := 0 to lTree.Count-1 do
    if lTree.Item[i].Data = lvFields.Selected.Data then
    begin
      fl := True;
      lvSelFields.Selected := lTree.Item[i];
      break;
    end;
  if fl and (lvFields.Selected.ImageIndex = 3) then
    fl := False;

  if not fl then
  begin
    lTree := lvSelFields.Items.AddChildObject(lTree, TrwDataDictField(lvFields.Selected.Data).DisplayName, lvFields.Selected.Data);
    lTree.ImageIndex := lvFields.Selected.ImageIndex;
    lTree.SelectedIndex := lTree.ImageIndex;
    lTree.StateIndex := lTree.ImageIndex;
    lvSelFields.Selected := lTree;
  end;

  if Assigned(lTree.Parent) then
    lTree.Parent.Expand(False);
  lvSelFields.Items.GetFirstNode.Expand(False);
end;

procedure TrwFieldAdd.btnDelClick(Sender: TObject);
var
  j: Integer;
begin
  if not Assigned(lvSelFields.Selected) or (lvSelFields.Selected.Data = nil) then
    Exit;
  j := FPrevStateNodes.IndexOfObject(lvSelFields.Selected);
  if j <> -1 then
    FFieldsForDelete.Add(FPrevStateNodes[j]);
  lvSelFields.Selected.Free;
end;

procedure TrwFieldAdd.lvSelFieldsChange(Sender: TObject; Node: TTreeNode);
var
  N: TTreeNode;
  P: TrwDataDictTable;
  j: Integer;
  C: Pointer;
begin
  if  Assigned(lvSelFields.Selected) then
  begin
    chbOuter.Checked := not lvSelFields.Selected.Cut;
    chbOuter.Visible := ((lvSelFields.Selected.ImageIndex = 3) and lvSelFields.Selected.HasChildren);
    if lvSelFields.Focused then
    begin
      ShowDescr(TrwDataDictField(lvSelFields.Selected.Data));

      C := FCurrentParent;
      FPreviosAttributes.Clear;
      FPreviosObjects.Clear;
      if lvSelFields.Selected.ImageIndex = 3 then
        N := lvSelFields.Selected
      else
        N := lvSelFields.Selected.Parent;

      If Assigned(N) then
        N := N.Parent;
      while Assigned(N) do
      begin
        if not Assigned(N.Data) then
        begin
          FPreviosObjects.Insert(0, FFirstTable);
          FPreviosAttributes.Insert(0, nil);
        end
        else
        begin
          FPreviosObjects.Insert(0, TrwDataDictField(N.Item[0].Data).Table);
          FPreviosAttributes.Insert(0, TrwDataDictField(N.Data));
        end;
        N := N.Parent;
      end;

      if lvSelFields.Selected.ImageIndex = 3 then
      begin
        P := TrwDataDictTable(TrwDataDictField(lvSelFields.Selected.Data).Table);
        for j := 0 to P.ForeignKeys.Count-1 do
          if P.ForeignKeys[j].Fields.FindField(TrwDataDictField(lvSelFields.Selected.Data)) <> nil then
          begin
            FCurrentParent := TrwDataDictTable(P.ForeignKeys[j].Table);
            FCurrentAttrParent := TrwDataDictField(lvSelFields.Selected.Data);
            break;
          end;
      end
      else if (lvSelFields.Selected.ImageIndex = 2) and Assigned(lvSelFields.Selected.Parent.Data) then
      begin
        FCurrentAttrParent := TrwDataDictField(lvSelFields.Selected.Parent.Data);
        FCurrentParent := TrwDataDictTable(TrwDataDictField(lvSelFields.Selected.Data).Table);
      end
      else
      begin
        FCurrentParent := FFirstTable;
        FCurrentAttrParent := nil;
      end;

      if not (FCurrentParent = C) then
      begin
        CreateFieldList(FCurrentParent);
        ShowPath;
      end;  
    end;
  end;
end;

procedure TrwFieldAdd.ShowDescr(AFld: TrwDataDictField);
begin
  grbDescr.Caption := 'Description of ';
  if Assigned(AFld) then
  begin
    meDescr.Text := AFld.Notes;
    grbDescr.Caption := grbDescr.Caption+AFld.DisplayName;
  end
  else
    meDescr.Text := '';
end;

procedure TrwFieldAdd.lvFieldsEnter(Sender: TObject);
begin
  if Sender is TTreeView then
    TTreeView(Sender).OnChange(Sender, TTreeView(Sender).Selected);
end;

procedure TrwFieldAdd.SyncSelectedFields(ATbl: TrwWizardQueryTable);

  procedure AddFlds(T: TrwWizardQueryTable; ANode: TTreeNode);
  var
    i: Integer;
    N: TTreeNode;
  begin
    for i := 0 to T.Fields.Count-1 do
    begin
      N := lvSelFields.Items.AddChildObject(ANode, T.Fields[i].Field.DisplayName, T.Fields[i].Field);
      FPrevStateNodes.AddObject(T.Fields[i].Alias, N);
      N.ImageIndex := 2;
      if Assigned(T.Fields[i].LookUpTable.Table) then
      begin
        N.ImageIndex := 3;
        N.Cut := T.Fields[i].LookUpTable.Outer;
        AddFlds(T.Fields[i].LookUpTable, N);
      end
      else
        N.ImageIndex := 2;
      N.SelectedIndex := N.ImageIndex;
      N.StateIndex := N.ImageIndex;
    end;
  end;

begin
   lvSelFields.Items.BeginUpdate;
   AddFlds(ATbl, lvSelFields.Items.GetFirstNode);
   lvSelFields.FullExpand;
   lvSelFields.Items.EndUpdate;
end;

procedure TrwFieldAdd.chbOuterClick(Sender: TObject);
begin
  lvSelFields.Selected.Cut := not chbOuter.Checked;
end;

procedure TrwFieldAdd.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if (ModalResult = mrOK) and (FFieldsForDelete.Count > 0) then
    CanClose := (MessageDlg('Probably some columns related with deleted fields will be deleted! Proceed?',
       mtWarning, [mbYes, mbNo], 0) = mrYes);
end;

end.
