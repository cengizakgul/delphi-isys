// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWizardFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWizardInfo, StdCtrls, Buttons, ComCtrls, ExtCtrls, rwTypes, wwdblook,
  Db, sbAPI, rwWPParRefFrm, rwWPParListFrm, rwWPParDateFrm,
  rwWPParStrFrm, rwWPParamFrm, rwDataDictionary, CheckLst, rwUtils,
  rwQBInfo, Variants, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  rwDesignClasses, rwLogQuery, rwEngineTypes, ISDataAccessComponents;

type
  TrwWizard = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    pcPages: TPageControl;
    tsPrimary: TTabSheet;
    GroupBox12: TGroupBox;
    memDescr: TMemo;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    imgLandScape: TImage;
    imgPortrait: TImage;
    cbPaperNames: TComboBox;
    rbPortrait: TRadioButton;
    rbLandscape: TRadioButton;
    rgGenASCII: TRadioGroup;
    chbGenASCII: TCheckBox;
    edDelimiter: TEdit;
    dsClients: TisRWClientDataSet;
    GroupBox14: TGroupBox;
    cbClients: TwwDBLookupCombo;
    tsQuery: TTabSheet;
    grbParams: TGroupBox;
    ScrollBox1: TScrollBox;
    pnCont: TPanel;
    Panel1: TPanel;
    Panel2: TPanel;
    chbOuter: TCheckBox;
    btnFltrTbl: TButton;
    GroupBox10: TGroupBox;
    btnAddTbl: TButton;
    btnDelTbl: TButton;
    lvTables: TTreeView;
    grbFields: TGroupBox;
    lvFields: TTreeView;
    Button1: TButton;
    pnNote: TPanel;
    Label4: TLabel;
    edNote: TEdit;
    tsColumns: TTabSheet;
    GroupBox2: TGroupBox;
    lvColumn: TListView;
    btnAddColumn: TButton;
    btnEditColumn: TButton;
    btnDeleteColumn: TButton;
    btnUpColumn: TButton;
    btnDownColumn: TButton;
    tsGroup: TTabSheet;
    GroupBox4: TGroupBox;
    SpeedButton1: TSpeedButton;
    btnDel: TSpeedButton;
    SpeedButton3: TSpeedButton;
    GroupBox5: TGroupBox;
    lbColumns1: TListBox;
    GroupBox6: TGroupBox;
    lbGroup: TListBox;
    GroupBox9: TGroupBox;
    Label3: TLabel;
    chbCount: TCheckBox;
    chlSum: TCheckListBox;
    GroupBox8: TGroupBox;
    btnAdd: TSpeedButton;
    SpeedButton2: TSpeedButton;
    GroupBox3: TGroupBox;
    lbColumns2: TListBox;
    GroupBox7: TGroupBox;
    lbSort: TListBox;
    chbQuoted: TCheckBox;
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rbPortraitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rgGenASCIIClick(Sender: TObject);
    procedure chbGenASCIIClick(Sender: TObject);
    procedure cbClientsChange(Sender: TObject);
    procedure chbOuterClick(Sender: TObject);
    procedure btnFltrTblClick(Sender: TObject);
    procedure lvTablesChange(Sender: TObject; Node: TTreeNode);
    procedure lvTablesEnter(Sender: TObject);
    procedure btnAddTblClick(Sender: TObject);
    procedure btnDelTblClick(Sender: TObject);
    procedure lvFieldsChange(Sender: TObject; Node: TTreeNode);
    procedure edNoteChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnAddColumnClick(Sender: TObject);
    procedure btnEditColumnClick(Sender: TObject);
    procedure btnDeleteColumnClick(Sender: TObject);
    procedure btnUpColumnClick(Sender: TObject);
    procedure btnDownColumnClick(Sender: TObject);
    procedure pcPagesChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure chbCountClick(Sender: TObject);
    procedure chlSumClickCheck(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure chbQuotedClick(Sender: TObject);
    procedure edDelimiterExit(Sender: TObject);
  private
    FParamTable: TrwWizardQueryTable;
    FParamNode: TTreeNode;
    FParamTree: TTreeView;
    procedure ClearFilterPanel;
    procedure InitTableParams;
    procedure InitFieldParams(ANode: TTreeNode);
    procedure CreateTableParams(ATable: TrwWizardQueryTable; ATree: TTreeView);
    procedure ChangeParam(Sender: TObject);
    procedure SyncSelectedFields(ATbl: TrwWizardQueryTable);    
  protected
    FClient: Integer;
    FCompany: Integer;
    procedure Initialize; virtual;
    procedure Finalize; virtual;
    procedure InitializeQuery;
    procedure SyncColumnList; virtual;
    procedure InitializeGroup; virtual;
  public
    WizardInfo: TrwWizardInfo;
  end;

  TrwWizardClass = class of TrwWizard;


implementation

{$R *.DFM}

uses rwColumnEditFrm, rwFilterEditFrm, rwTableAddFrm, rwFieldAddFrm, rwGroupEditFrm,
  rwWizDM;

{ TrwWizard }

procedure TrwWizard.FormDestroy(Sender: TObject);
begin
  if Assigned(WizardInfo) then
    WizardInfo.Free;
end;

procedure TrwWizard.BitBtn1Click(Sender: TObject);
begin
  Finalize;
  try
    WizardInfo.CreateReport;
  finally
    WizardInfo.Report.WizardInfo.Clear;
    WizardInfo.Report.WizardInfo.WriteComponent(WizardInfo);
  end;
end;

procedure TrwWizard.FormShow(Sender: TObject);
begin
  Caption := WizardInfo.WizardName;
  Initialize;
end;

procedure TrwWizard.rbPortraitClick(Sender: TObject);
begin
  imgPortrait.Visible := rbPortrait.Checked;
  imgLandScape.Visible := rbLandScape.Checked;
end;

procedure TrwWizard.FormCreate(Sender: TObject);
var
  i: Integer;
  Q: TrwLQQuery;
begin
  for i := Ord(Low(cPaperInfo)) to Ord(High(cPaperInfo)) do
    cbPaperNames.Items.AddObject(cPaperInfo[TrwPaperSize(i)].Name, Pointer(i));

    
  dsClients.Close;
  Q := TrwLQQuery.Create;
  try
    Q.SQL.Text := 'SELECT Tmp_Co.CUSTOM_COMPANY_NUMBER || ''  '' ||Tmp_Co.NAME NAME, Tmp_Co.CL_NBR, Tmp_Co.CO_NBR FROM Tmp_Co ';

//###    if not rwAllowChangeClient then
//###     Q.SQL.Text := Q.SQL.Text + ' WHERE CL_NBR = ' + IntToStr(ClientID);

    Q.SQL.Text := Q.SQL.Text + ' ORDER BY 1 ';
    Q.OpenSQL;
    sbSBCursorToClientDataSet(Q, dsClients);
    Q.Close;
  finally
    Q.Free;
  end;
  dsClients.Open;
  cbClients.Selected.Add('Name'+#9+IntToStr(dsClients.FieldByName('name').Size)+#9+'Name'+#9+'F');
end;

procedure TrwWizard.rgGenASCIIClick(Sender: TObject);
begin
  edDelimiter.Enabled := rgGenASCII.ItemIndex = 1;
  if rgGenASCII.ItemIndex = 0 then
    TrwWizardInfo(WizardInfo).ASCIIDelimiter := ''
  else
    TrwWizardInfo(WizardInfo).ASCIIDelimiter := edDelimiter.Text;
end;

procedure TrwWizard.chbGenASCIIClick(Sender: TObject);
begin
  rgGenASCII.Enabled := chbGenASCII.Checked;
  chbQuoted.Enabled := chbGenASCII.Checked;
  if not chbGenASCII.Checked then
    edDelimiter.Enabled := False;
  TrwWizardInfo(WizardInfo).GenerateASCIIFile := chbGenASCII.Checked;
  if rgGenASCII.ItemIndex = 0 then
    TrwWizardInfo(WizardInfo).ASCIIDelimiter := ''
  else
    TrwWizardInfo(WizardInfo).ASCIIDelimiter := edDelimiter.Text;
end;

procedure TrwWizard.cbClientsChange(Sender: TObject);
begin
  FClient := dsClients.FieldByName('CL_NBR').AsInteger;
  FCompany := dsClients.FieldByName('CO_NBR').AsInteger;
end;

procedure TrwWizard.chbOuterClick(Sender: TObject);
begin
  if Assigned(FParamTable) then
  begin
    FParamTable.Outer := not chbOuter.Checked;
    FParamNode.Cut := not chbOuter.Checked;
  end;
end;

procedure TrwWizard.btnFltrTblClick(Sender: TObject);
begin
  EditFilter(WizardInfo, FParamTable);
end;

procedure TrwWizard.lvTablesChange(Sender: TObject; Node: TTreeNode);
begin
  if not Assigned(Node) then
  begin
    ClearFilterPanel;
    grbParams.Enabled := False;
    Exit;
  end
  else
  begin
    grbParams.Enabled := True;
    WizDM.CreateFieldTree(lvFields, TrwWizardQueryTable(Node.Data));
    InitTableParams;
  end;
end;

procedure TrwWizard.lvTablesEnter(Sender: TObject);
begin
  TTreeView(Sender).OnChange(nil, TTreeView(Sender).Selected);
end;

procedure TrwWizard.btnAddTblClick(Sender: TObject);
var
  Tbl: TrwWizardQueryTable;
begin
  if not Assigned(lvTables.Selected) then
    Tbl := WizardInfo.MainQuery.Table
  else
    Tbl := TrwWizardQueryTable(lvTables.Selected.Data);

  if AddTable(Tbl) then
    InitializeQuery;
end;

procedure TrwWizard.btnDelTblClick(Sender: TObject);
var
  i: Integer;

  procedure DelTable(Node: TTreeNode);
  begin
    if Node <> lvTables.Items.GetFirstNode then
      TrwWizardQueryTable(Node.Data).Free
    else
      TrwWizardQueryTable(Node.Data).TableName := '';
    Node.Free;
  end;

begin
  if not Assigned(lvTables.Selected) then
    Exit;

  if lvTables.Selected = lvTables.Items.GetFirstNode then
  begin
     if MessageDlg('All tables will be deleted! Proceed?', mtWarning, [mbYes, mbNo], 0) = mrYes then
       for i := lvTables.Items.Count - 1 downto 0 do
         DelTable(lvTables.Items[i]);
     SyncSelectedFields(nil);
  end

  else if MessageDlg('Probably some fields and columns related with that table will be deleted! Proceed?',
    mtWarning, [mbYes, mbNo], 0) = mrYes then
  begin
    DelTable(lvTables.Selected);
    lvTables.Selected := lvTables.Items.GetFirstNode;
    SyncSelectedFields(TrwWizardQueryTable(lvTables.Selected.Data));
  end;
end;


procedure TrwWizard.ClearFilterPanel;
begin
  while pnCont.ControlCount > 0 do
  begin
    TrwWPParam(pnCont.Controls[0]).DoChange;
    pnCont.Controls[0].Free;
  end;
  FParamTree := nil;
  FParamTable := nil;
end;

procedure TrwWizard.lvFieldsChange(Sender: TObject; Node: TTreeNode);
begin
  if not Assigned(Node) then
  begin
    ClearFilterPanel;
    grbParams.Enabled := False;
    Exit;
  end;

  if (Node.ImageIndex = 3) and Node.HasChildren then
    InitFieldParams(Node)
  else if Assigned(Node.Parent) and (Node.Parent.ImageIndex = 3) then
    InitFieldParams(Node.Parent)
  else
    InitTableParams;

  if Node.ImageIndex = 2 then
  begin
    pnNote.Visible := (Node <> lvFields.Items.GetFirstNode);
    if pnNote.Visible then
      edNote.Text := TrwWizardQueryTableField(Node.Data).Note;
  end;
end;

procedure TrwWizard.edNoteChange(Sender: TObject);
begin
  TrwWizardQueryTableField(lvFields.Selected.Data).Note := Trim(edNote.Text);
  lvFields.Selected.Text := TrwWizardQueryTableField(lvFields.Selected.Data).Field.DisplayName;
  if TrwWizardQueryTableField(lvFields.Selected.Data).Note <> '' then
    lvFields.Selected.Text := lvFields.Selected.Text+' ('+TrwWizardQueryTableField(lvFields.Selected.Data).Note+')';
end;

procedure TrwWizard.Button1Click(Sender: TObject);
begin
  if Assigned(lvTables.Selected) and
     AddField(TrwWizardQueryTable(lvTables.Selected.Data), WizardInfo.MainTable.Columns, Self) then
    SyncSelectedFields(TrwWizardQueryTable(lvTables.Selected.Data));
end;

procedure TrwWizard.InitTableParams;
begin
  FParamNode := lvTables.Selected;
  CreateTableParams(TrwWizardQueryTable(lvTables.Selected.Data), lvTables);
  chbOuter.Checked := not TrwWizardQueryTable(lvTables.Selected.Data).Outer;
  chbOuter.Visible := (lvTables.Selected <> lvTables.Items.GetFirstNode);
  grbParams.Caption := 'Property of table '+TrwWizardQueryTable(lvTables.Selected.Data).Table.DisplayName;
end;

procedure TrwWizard.InitializeQuery;
begin
  WizDM.CreateTableTree(lvTables, WizardInfo.MainQuery.Table);
end;

procedure TrwWizard.InitFieldParams(ANode: TTreeNode);
begin
  FParamNode := ANode;
  CreateTableParams(TrwWizardQueryTable(TrwWizardQueryTableField(ANode.Data).LookUpTable), lvFields);
  chbOuter.Checked := not TrwWizardQueryTable(TrwWizardQueryTableField(ANode.Data).LookUpTable).Outer;
  chbOuter.Visible := True;
  pnNote.Visible := False;
  grbParams.Caption := 'Property of look up field '+TrwWizardQueryTableField(ANode.Data).Field.DisplayName;
end;


procedure TrwWizard.SyncSelectedFields(ATbl: TrwWizardQueryTable);
begin
  WizDM.CreateFieldTree(lvFields, ATbl);
end;

procedure TrwWizard.CreateTableParams(ATable: TrwWizardQueryTable; ATree: TTreeView);
var
  i: Integer;
  y: Integer;
  C: TrwWPParam;
  AObject: TrwDataDictTable;
  h: String;
begin
  if FParamTable = ATable then Exit;

  Busy;
  AObject := ATable.Table;
  ClearFilterPanel;
  FParamTree := ATree;
  FParamTable := ATable;

  y := 40;
  C := nil;
  for i := 0 to AObject.Params.Count-1 do
  begin
    if AObject.Params[i].DisplayName = '' then Continue;

    if TrwDataDictParam(AObject.Params[i]).RefField <> nil then
    begin
      h := TrwDataDictParam(AObject.Params[i]).RefField.FieldValues.Text;

      if h = '' then
      begin
        C := TrwWPParRef.Create(Self);
        TrwWPParRef(C).Initialize(TrwDataDictParam(AObject.Params[i]).RefField.Table.Name,
          TrwDataDictParam(AObject.Params[i]).RefField.Name, FClient, FCompany);
      end
      else
      begin
        C := TrwWPParList.Create(Self);
        TrwWPParList(C).Initialize(h);
      end;

      C.Parent := pnCont;
      TrwWPParam(C).Value := ATable.Params[i].ParamValue;
    end

    else if AObject.Params[i].ParamType = ddtDateTime then
    begin
      C := TrwWPParDate.Create(Self);
      TrwWPParDate(C).Value := ATable.Params[i].ParamValue;
    end
    else if Pos('=', AObject.Params[i].ParamValues.Text) > 0 then
    begin
      C := TrwWPParList.Create(Self);
      TrwWPParList(C).Initialize(AObject.Params[i].ParamValues.Text);
      C.Parent := pnCont;
      TrwWPParList(C).Value := ATable.Params[i].ParamValue;
    end
    else
    begin
      C := TrwWPParStr.Create(Self);
      TrwWPParStr(C).Value := ATable.Params[i].ParamValue;
    end;

    C.Param := TrwDataDictParam(AObject.Params[i]);
    C.Top := y;
    C.chbUse.Checked := not ATable.Params[i].EmptyParam;
    C.chbUse.OnClick(nil);
    C.Visible := True;    
    C.Parent := pnCont;
    C.lName.Caption := AObject.Params[i].DisplayName;
    C.Name := 'par' + ATable.Params[i].ParamName;
    C.chbInFrm.Checked := ATable.Params[i].ForInputForm;
    C.OnChange := ChangeParam;
    y := C.Top+C.Height+200;
  end;

  if not Assigned(C) then
    pnCont.Height := 0;
    
  Ready;
end;

procedure TrwWizard.ChangeParam(Sender: TObject);
var
  TP: TrwWizardQueryTableParam;
  ParFrm: TrwWPParam;
begin
  ParFrm := TrwWPParam(Sender);
  TP := FParamTable.Params.FindParam(Copy(ParFrm.Name, 4, Length(ParFrm.Name)-3));
  TP.ParamName := ParFrm.Param.Name;
  TP.ParamValue := ParFrm.Value;
  TP.ForInputForm := ParFrm.ForInputForm;
  TP.EmptyParam := ParFrm.EmptyParam;
end;

procedure TrwWizard.btnAddColumnClick(Sender: TObject);
var
  LI: TListItem;
  SF: TrwQBShowingField;
begin
  if AddColumn(WizardInfo) then
  begin
    LI := lvColumn.Items.Add;
    LI.Data := WizardInfo.MainTable.Columns[WizardInfo.MainTable.Columns.Count-1];
    LI.Caption := WizardInfo.MainTable.Columns[WizardInfo.MainTable.Columns.Count-1].Title;
    if TrwWizTblColumn(LI.Data).Calculated then
      LI.SubItems.Add('CALCULATED')

    else
      if Assigned(WizardInfo.MainQuery.Table.QBQuery) then
      begin
        SF := WizardInfo.MainQuery.Table.QBQuery.ShowingFields.FieldByInternalName(WizardInfo.MainTable.Columns[WizardInfo.MainTable.Columns.Count-1].Field.FieldName);
        LI.SubItems.Add(SF.GetFieldName(True));
      end
      else
        if WizardInfo.MainTable.Columns[WizardInfo.MainTable.Columns.Count-1].Field.Note <> '' then
          LI.SubItems.Add(WizardInfo.MainTable.Columns[WizardInfo.MainTable.Columns.Count-1].Field.Note)
        else
          LI.SubItems.Add(WizardInfo.MainTable.Columns[WizardInfo.MainTable.Columns.Count-1].Field.Path);
          
    if TrwWizTblColumn(LI.Data).Summarized then
      LI.SubItems.Add('Yes')
    else
      LI.SubItems.Add('');
  end;
end;

procedure TrwWizard.btnEditColumnClick(Sender: TObject);
var
  LI: TListItem;
  SF: TrwQBShowingField;
begin
  if not Assigned(lvColumn.Selected) then
    Exit;
  LI := lvColumn.Selected;
  if EditColumn(WizardInfo, TrwWizTblColumn(LI.Data)) then
  begin
    LI.Caption := TrwWizTblColumn(LI.Data).Title;
    if TrwWizTblColumn(LI.Data).Calculated then
      LI.SubItems[0] := 'CALCULATED'
    else
      if Assigned(WizardInfo.MainQuery.Table.QBQuery) then
      begin
        SF := WizardInfo.MainQuery.Table.QBQuery.ShowingFields.FieldByInternalName(TrwWizTblColumn(LI.Data).Field.FieldName);
        LI.SubItems[0] := SF.GetFieldName(True);
      end

      else
        if TrwWizTblColumn(LI.Data).Field.Note <> '' then
          LI.SubItems[0] := TrwWizTblColumn(LI.Data).Field.Note
        else
          LI.SubItems[0] := TrwWizTblColumn(LI.Data).Field.Path;

    if TrwWizTblColumn(LI.Data).Summarized then
      LI.SubItems[1] := 'Yes'
    else
      LI.SubItems[1] := '';
  end;
end;

procedure TrwWizard.btnDeleteColumnClick(Sender: TObject);
var
  Col: TrwWizTblColumn;
begin
  if not Assigned(lvColumn.Selected) then
    Exit;
    
  Col := TrwWizTblColumn(lvColumn.Selected.Data);
  Col.Free;
  lvColumn.Selected.Free;
end;

procedure TrwWizard.btnUpColumnClick(Sender: TObject);
var
  i: Integer;
begin
  if not Assigned(lvColumn.Selected) then
    Exit;
  i := TrwWizTblColumn(lvColumn.Selected.Data).Index-1;
  if i >= 0 then
  begin
    TrwWizTblColumn(lvColumn.Selected.Data).Index := i;
    SyncColumnList;
    lvColumn.Selected := lvColumn.Items[i];
  end;
end;

procedure TrwWizard.btnDownColumnClick(Sender: TObject);
var
  i: Integer;
begin
  if not Assigned(lvColumn.Selected) then
    Exit;
  i := TrwWizTblColumn(lvColumn.Selected.Data).Index+1;
  if i < lvColumn.Items.Count then
  begin
    TrwWizTblColumn(lvColumn.Selected.Data).Index := i;
    SyncColumnList;
    lvColumn.Selected := lvColumn.Items[i];
  end;
end;

procedure TrwWizard.SyncColumnList;
var
  i: Integer;
  LI: TListItem;
  SF: TrwQBShowingField;
begin
  lvColumn.Items.Clear;
  for i := 0 to WizardInfo.MainTable.Columns.Count-1 do
  begin
    LI := lvColumn.Items.Add;
    LI.Data := WizardInfo.MainTable.Columns[i];
    LI.Caption := WizardInfo.MainTable.Columns[i].Title;
    if WizardInfo.MainTable.Columns[i].Calculated then
      LI.SubItems.Add('Calculated')
    else
      if Assigned(WizardInfo.MainQuery.Table.QBQuery) then
      begin
        SF := WizardInfo.MainQuery.Table.QBQuery.ShowingFields.FieldByInternalName(WizardInfo.MainTable.Columns[i].Field.FieldName);
        LI.SubItems.Add(SF.GetFieldName(True));
      end

      else
        if WizardInfo.MainTable.Columns[i].Field.Note = '' then
          LI.SubItems.Add(WizardInfo.MainTable.Columns[i].Field.Path)
        else
          LI.SubItems.Add(WizardInfo.MainTable.Columns[i].Field.Note);

    if WizardInfo.MainTable.Columns[i].Summarized then
      LI.SubItems.Add('Yes')
    else
      LI.SubItems.Add('');
  end;
end;

procedure TrwWizard.pcPagesChange(Sender: TObject);
begin
  if pcPages.ActivePage = tsColumns then
    SyncColumnList
  else if pcPages.ActivePage = tsQuery then
    InitializeQuery
  else if pcPages.ActivePage = tsGroup then
    InitializeGroup;
end;

procedure TrwWizard.Initialize;
var
  h: String;
begin
  memDescr.Text := WizardInfo.Description;
  if WizardInfo.Paper.PaperOrientation = rpoPortrait then
    rbPortrait.Checked := True
  else
    rbLandscape.Checked := True;
  rbPortrait.OnClick(nil);
  cbPaperNames.ItemIndex := Ord(WizardInfo.Paper.PaperSize);

  if Pos(#13, WizardInfo.Company) = 0 then
  begin
    dsClients.Locate('cl_nbr;co_nbr', VarArrayOf([StrToInt(WizardInfo.Client), StrToInt(WizardInfo.Company)]), []);
    cbClients.Value := dsClients.FieldByName('name').AsString;
  end;

  h := WizardInfo.ASCIIDelimiter;
  chbGenASCII.Checked := WizardInfo.GenerateASCIIFile;
  if h = '' then
    rgGenASCII.ItemIndex := 0
  else
    rgGenASCII.ItemIndex := 1;
  WizardInfo.ASCIIDelimiter := h;
  edDelimiter.Text := h;

  chbQuoted.Checked := (TrwWizardInfo(WizardInfo).SurroundChar <> '');

  SyncColumnList;
end;

procedure TrwWizard.SpeedButton1Click(Sender: TObject);
var
  i: Integer;
begin
  if lbColumns1.ItemIndex = -1 then Exit;

  i := lbGroup.Items.AddObject(lbColumns1.Items[lbColumns1.ItemIndex], lbColumns1.Items.Objects[lbColumns1.ItemIndex]);
  TrwWizTblColumn(lbGroup.Items.Objects[i]).GroupNumber := i+1;
  TrwWizTblColumn(lbGroup.Items.Objects[i]).GroupHeader := True;
  TrwWizTblColumn(lbGroup.Items.Objects[i]).GroupFooter := True;
  TrwWizTblColumn(lbGroup.Items.Objects[i]).GroupPageBreak := False;
  lbColumns1.Items.Delete(lbColumns1.ItemIndex);
end;

procedure TrwWizard.btnDelClick(Sender: TObject);
begin
  if lbGroup.ItemIndex = -1 then Exit;

  lbColumns1.Items.AddObject(lbGroup.Items[lbGroup.ItemIndex], lbGroup.Items.Objects[lbGroup.ItemIndex]);
  TrwWizTblColumn(lbGroup.Items.Objects[lbGroup.ItemIndex]).GroupNumber := 0;
  lbGroup.Items.Delete(lbGroup.ItemIndex);
end;

procedure TrwWizard.SpeedButton3Click(Sender: TObject);
begin
  if lbGroup.ItemIndex = -1 then Exit;

  EditGroup(TrwWizTblColumn(lbGroup.Items.Objects[lbGroup.ItemIndex]), Self);
end;

procedure TrwWizard.chbCountClick(Sender: TObject);
begin
  WizardInfo.MainTable.CountRecords := chbCount.Checked;
end;

procedure TrwWizard.chlSumClickCheck(Sender: TObject);
begin
  TrwWizTblColumn(chlSum.Items.Objects[chlSum.ItemIndex]).GroupSummary := chlSum.Checked[chlSum.ItemIndex];
end;

procedure TrwWizard.btnAddClick(Sender: TObject);
var
  i: Integer;
begin
  if lbColumns2.ItemIndex = -1 then Exit;

  i := lbSort.Items.AddObject(lbColumns2.Items[lbColumns2.ItemIndex], lbColumns2.Items.Objects[lbColumns2.ItemIndex]);
  TrwWizTblColumn(lbSort.Items.Objects[i]).SortNumber := i+1;
  lbColumns2.Items.Delete(lbColumns2.ItemIndex);
end;

procedure TrwWizard.SpeedButton2Click(Sender: TObject);
var
  i: Integer;
begin
  if lbSort.ItemIndex = -1 then Exit;

  lbColumns2.Items.AddObject(lbSort.Items[lbSort.ItemIndex], lbSort.Items.Objects[lbSort.ItemIndex]);
  TrwWizTblColumn(lbSort.Items.Objects[lbSort.ItemIndex]).SortNumber := 0;
  lbSort.Items.Delete(lbSort.ItemIndex);
  for i := 0 to lbSort.Items.Count - 1 do
    TrwWizTblColumn(lbSort.Items.Objects[i]).SortNumber := i + 1;
end;

procedure TrwWizard.InitializeGroup;
var
  i, j: Integer;
  SF: TrwQBShowingField;
begin
  lbColumns1.Items.Clear;
  lbColumns2.Items.Clear;
  lbGroup.Items.Clear;
  chlSum.Items.Clear;
  lbSort.Items.Clear;
  with WizardInfo.MainTable do
  begin
    for i := 0 to Columns.Count-1 do
      if not Columns[i].Calculated then
      begin
        if Columns[i].GroupNumber = 0 then
          lbColumns1.Items.AddObject(Columns[i].Title, Columns[i])
        else
          lbGroup.Items.AddObject(Columns[i].Title, Columns[i]);
        if Columns[i].SortNumber = 0 then
          lbColumns2.Items.AddObject(Columns[i].Title, Columns[i])
        else
          lbSort.Items.AddObject(Columns[i].Title, Columns[i]);
      end;


    if Assigned(WizardInfo.MainQuery.Table.QBQuery) then
    begin
      for i := 0 to Columns.Count-1 do
      begin
        if not Columns[i].Calculated then
          SF := WizardInfo.MainQuery.Table.QBQuery.ShowingFields.FieldByInternalName(WizardInfo.MainTable.Columns[i].Field.FieldName)
        else
          SF := nil;  
        if Columns[i].Calculated or Assigned(SF) and (SF.ExprType in [rwvFloat, rwvInteger]) then
        begin
          j := chlSum.Items.AddObject(Columns[i].Title, Columns[i]);
          if Columns[i].GroupSummary then
            chlSum.Checked[j] := True;
        end;
      end;
    end

    else
      for i := 0 to Columns.Count-1 do
        if Columns[i].Calculated or
           Assigned(Columns[i].Field) and
           (Columns[i].Field.Field.FieldType in [ddtFloat, ddtInteger]) then
        begin
          j := chlSum.Items.AddObject(Columns[i].Title, Columns[i]);
          if Columns[i].GroupSummary then
            chlSum.Checked[j] := True;
        end;
  end;

  chbCount.Checked := WizardInfo.MainTable.CountRecords;
end;


procedure TrwWizard.Finalize;
begin
  WizardInfo.Description := memDescr.Text;

  WizardInfo.Paper.PaperSize := TrwPaperSize(cbPaperNames.ItemIndex);
  if rbPortrait.Checked then
    WizardInfo.Paper.PaperOrientation := rpoPortrait
  else
    WizardInfo.Paper.PaperOrientation := rpoLandscape;

  WizardInfo.Client := dsClients.FieldByName('CL_NBR').AsString;
  WizardInfo.Company := dsClients.FieldByName('CO_NBR').AsString;
end;

procedure TrwWizard.chbQuotedClick(Sender: TObject);
begin
  if chbQuoted.Checked then
    TrwWizardInfo(WizardInfo).SurroundChar := '"'
  else
    TrwWizardInfo(WizardInfo).SurroundChar := '';
end;

procedure TrwWizard.edDelimiterExit(Sender: TObject);
begin
  TrwWizardInfo(WizardInfo).ASCIIDelimiter := edDelimiter.Text;
end;

end.
