// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWizDM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList, ComCtrls, rwWizardInfo, rwDataDictionary, rwQBInfo;

type
  TWizDM = class(TDataModule)
    ImageList: TImageList;
  private
  public
    procedure CreateTableTree(TV: TTreeView; Tbl: TrwWizardQueryTable);
    procedure CreateFieldTree(TV: TTreeView; Tbl: TrwWizardQueryTable);
    procedure InitTable(T1: TrwDataDictTable; ATbl: TrwWizardQueryTable);
  end;

var
  WizDM: TWizDM;

implementation

{$R *.DFM}

procedure TWizDM.CreateTableTree(TV: TTreeView; Tbl: TrwWizardQueryTable);

  procedure AddTbl(ATbl: TrwWizardQueryTable; ANode: TTreeNode);
  var
    N: TTreeNode;
    i: Integer;
    h: String;
  begin
    if Assigned(ATbl.QBQuery) then
    begin
      N := TV.Items.AddChildObject(ANode, ATbl.QBQuery.Description, ATbl);
      N.ImageIndex := 0;
      N.SelectedIndex := N.ImageIndex;
      N.StateIndex := N.ImageIndex;
    end

    else
    begin
      h := ATbl.Table.DisplayName;
      if not AnsiSameText(h, ATbl.Table.GroupName) then
        h := h + '  [' + ATbl.Table.GroupName + ']';
      N := TV.Items.AddChildObject(ANode, h, ATbl);
      N.Cut := ATbl.Outer;
      N.ImageIndex := 0;
      N.SelectedIndex := N.ImageIndex;
      N.StateIndex := N.ImageIndex;
      for i := 0 to ATbl.Tables.Count-1 do
        AddTbl(ATbl.Tables[i], N);
    end;
  end;

begin
  TV.Items.BeginUpdate;
  TV.Items.Clear;
  if Assigned(Tbl.Table) or Assigned(Tbl.QBQuery) then
  begin
    AddTbl(Tbl, nil);
    TV.Selected := TV.Items.GetFirstNode;
    TV.FullExpand;
  end;
  TV.Items.EndUpdate;
end;

procedure TWizDM.CreateFieldTree(TV: TTreeView; Tbl: TrwWizardQueryTable);

  procedure AddFlds(T: TrwWizardQueryTable; ANode: TTreeNode);
  var
    i: Integer;
    N: TTreeNode;
    h: String;
    SF: TrwQBShowingField;
  begin
    for i := 0 to T.Fields.Count-1 do
    begin
      if Assigned(T.QBQuery) then
      begin
        SF := T.QBQuery.ShowingFields.FieldByInternalName(T.Fields[i].FieldName);
        h := SF.GetFieldName(True);
      end

      else
        h := T.Fields[i].Field.DisplayName;
      if T.Fields[i].Note <> '' then
        h :=  h+' ('+T.Fields[i].Note+')';

      N := TV.Items.AddChildObject(ANode, h, T.Fields[i]);
      N.ImageIndex := 2;
      if Assigned(T.Fields[i].LookUpTable.Table) then
      begin
        N.ImageIndex := 3;
        N.Cut := T.Fields[i].LookUpTable.Outer;
        AddFlds(T.Fields[i].LookUpTable, N);
      end
      else
        N.ImageIndex := 2;
      N.SelectedIndex := N.ImageIndex;
      N.StateIndex := N.ImageIndex;
    end;
  end;

begin
   TV.Items.BeginUpdate;
   TV.Items.Clear;
   if Assigned(Tbl) or Assigned(Tbl.QBQuery) then
   begin
     if Assigned(Tbl.QBQuery) then
     begin
       TV.Items.AddChildObject(nil, Tbl.QBQuery.Description, nil);
       AddFlds(Tbl, TV.Items.GetFirstNode);
     end

     else
     begin
       TV.Items.AddChildObject(nil, Tbl.Table.DisplayName, nil);
       AddFlds(Tbl, TV.Items.GetFirstNode);
     end;

     if not TV.Items.GetFirstNode.HasChildren then
       TV.Items.Clear
     else
       TV.FullExpand;
   end;
   TV.Items.EndUpdate;
end;


procedure TWizDM.InitTable(T1: TrwDataDictTable; ATbl: TrwWizardQueryTable);
var
  TP: TrwWizardQueryTableParam;
  i: Integer;
begin
  ATbl.TableName := T1.Name;
  for i := 0 to T1.Params.Count-1 do
  begin
    TP := ATbl.Params.Add;
    TP.ParamName := T1.Params[i].Name;
    if TrwDataDictParam(T1.Params[i]).DefaultValue <> '' then
      TP.ParamValue := TrwDataDictParam(T1.Params[i]).DefaultValue
    else
      TP.ParamValue := '';
    TP.ForInputForm := False;
    TP.EmptyParam := True;
  end;
end;


initialization
  WizDM := TWizDM.Create(nil);

finalization
  WizDM.Free;

end.
