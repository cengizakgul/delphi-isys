// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwFunctSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, rwUtils, rwBasicUtils, rwBasicClasses;

type
  TrwFunctSelect = class(TForm)
    btnOk: TButton;
    btnCancel: TButton;
    lvFunct: TListView;
    procedure FormCreate(Sender: TObject);
    procedure lvFunctDblClick(Sender: TObject);
  private
  public
  end;

  function SelectFunction(AOwner: TComponent): String;

implementation

{$R *.DFM}

const
  RW_WIZARD_SIGN = '//Wizard';
  RW_WIZARD_DEFAULTS_SIGN = '//Defaults';


function SelectFunction(AOwner: TComponent): String;
var
  F: TrwFunctSelect;
  h, h1: String;
  Fnct: TrwLibFunction;
  j, n: Integer;
begin
  Result := '';
  F := TrwFunctSelect.Create(AOwner);
  with F do
    try
      if (ShowModal = mrOk) and Assigned(lvFunct.Selected) then
      begin
        if Assigned(lvFunct.Selected.Data) then
        begin
          Fnct := TrwLibFunction(lvFunct.Selected.Data);
          Result := Fnct.Name + '(';

          j := Pos(RW_WIZARD_DEFAULTS_SIGN, Fnct.Text);
          if j > 0 then
          begin
            h := Copy(Fnct.Text, j, Length(Fnct.Text) - j);
            GetNextStrValue(h, #13#10);
            n := 0;
            while True do
            begin
              h1 := GetNextStrValue(h, #13#10);
              if Copy(h1, 1, 2) = '//' then
              begin
                Delete(h1, 1, 2);
                if Length(h1) = 0 then
                  break;
                Result := Result + h1 + ', ';
                Inc(n);
              end
              else
                Break;
            end;

            if (n = Fnct.Params.Count) and (Copy(Result, Length(Result)-1, 2) = ', ') then
            begin
              Delete(Result, Length(Result)-1, 2);
              Result := Result +')';
            end;
          end;
        end

        else
        begin
          j := Pos(' ', lvFunct.Selected.Caption);
          Result := Copy(lvFunct.Selected.Caption, 1, j - 1);
          if lvFunct.Selected.Caption[j + 1] = '(' then
            Result := Result + '(';
        end;
      end;

    finally
      Free;
    end;
end;


procedure TrwFunctSelect.FormCreate(Sender: TObject);
var
  LI: TListItem;
  i, j: Integer;
  h, h1, h2: String;
begin
//  lvFunct.Items.Clear;
  for i := 0 to SystemLibFunctions.Count - 1 do
  begin
    j := Pos(RW_WIZARD_SIGN, TrwLibFunction(SystemLibFunctions[i]).Text);
    if j > 0 then
    begin
      LI := lvFunct.Items.Add;
      LI.Caption := SystemLibFunctions[i].Name;
      LI.Data := SystemLibFunctions[i];
      h := Copy(TrwLibFunction(SystemLibFunctions[i]).Text, j, Length(TrwLibFunction(SystemLibFunctions[i]).Text) - j);
      GetNextStrValue(h, #13#10);
      h1 := GetNextStrValue(h, #13#10);
      if Copy(h1, 1, 2) = '//' then
        Delete(h1, 1, 2)
      else
        h1 := '';
      h2 := GetNextStrValue(h, #13#10);
      if Copy(h2, 1, 2) = '//' then
        Delete(h2, 1, 2)
      else
        h2 := '';
      if Length(h2) > 0 then
        LI.Caption := h2;
      LI.SubItems.Add(h1);
    end;
  end;

  lvFunct.SortType := stText; 
end;

procedure TrwFunctSelect.lvFunctDblClick(Sender: TObject);
begin
  btnOk.Click;
end;

end.
