inherited rwWizardInfoEdit: TrwWizardInfoEdit
  Left = 355
  Top = 169
  BorderIcons = [biSystemMenu]
  Caption = 'Wizard'#39's Information'
  ClientHeight = 543
  ClientWidth = 634
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pcPages: TPageControl
    Width = 634
    Height = 543
    TabOrder = 0
    inherited tsPrimary: TTabSheet
      inherited rgGenASCII: TRadioGroup [0]
        Width = 166
        TabOrder = 7
      end
      object GroupBox13: TGroupBox [1]
        Left = 4
        Top = 3
        Width = 233
        Height = 63
        Caption = 'Title'
        TabOrder = 0
        object edTitle: TEdit
          Left = 9
          Top = 15
          Width = 215
          Height = 21
          TabOrder = 0
        end
        object chbPrntTitleOnEachPage: TCheckBox
          Left = 9
          Top = 40
          Width = 129
          Height = 17
          Caption = 'Print on the all pages'
          TabOrder = 1
        end
      end
      object chbSBInfo: TCheckBox [2]
        Left = 344
        Top = 188
        Width = 284
        Height = 17
        Caption = 'Print Service Bureau information on the bottom of page'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
      object chbCOInfo: TCheckBox [3]
        Left = 344
        Top = 165
        Width = 241
        Height = 17
        Caption = 'Print Company information on the top of page'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
      object GroupBox11: TGroupBox [4]
        Left = 180
        Top = 160
        Width = 153
        Height = 77
        TabOrder = 4
        object dtAsOfDate: TDateTimePicker
          Left = 9
          Top = 31
          Width = 134
          Height = 21
          Date = 36920.398190173600000000
          Time = 36920.398190173600000000
          Enabled = False
          TabOrder = 0
        end
      end
      object chbAsOfDate: TCheckBox [5]
        Left = 189
        Top = 157
        Width = 71
        Height = 17
        Caption = 'As Of Date'
        TabOrder = 3
        OnClick = chbAsOfDateClick
      end
      inherited GroupBox12: TGroupBox [6]
        Height = 238
        TabOrder = 5
        inherited memDescr: TMemo
          Height = 211
        end
      end
      inherited GroupBox1: TGroupBox [7]
        TabOrder = 11
      end
      inherited chbGenASCII: TCheckBox
        TabOrder = 8
      end
      inherited edDelimiter: TEdit
        Left = 107
        TabOrder = 9
      end
      inherited GroupBox14: TGroupBox
        Height = 151
        TabOrder = 10
        object clClients: TCheckListBox [0]
          Left = 9
          Top = 18
          Width = 358
          Height = 109
          ItemHeight = 13
          TabOrder = 3
          Visible = False
        end
        object rbCoReport: TRadioButton
          Left = 9
          Top = 40
          Width = 113
          Height = 17
          Caption = 'Company report'
          TabOrder = 1
          OnClick = rbMClReportClick
        end
        object rbMClReport: TRadioButton
          Left = 125
          Top = 40
          Width = 113
          Height = 17
          Caption = 'Multiclient report'
          TabOrder = 2
          OnClick = rbMClReportClick
        end
      end
      inherited chbQuoted: TCheckBox
        Left = 107
      end
    end
    inherited tsQuery: TTabSheet
      inherited grbParams: TGroupBox
        Height = 237
        inherited ScrollBox1: TScrollBox
          Height = 171
        end
      end
    end
    inherited tsColumns: TTabSheet
      inherited GroupBox2: TGroupBox
        Height = 473
        inherited lvColumn: TListView
          OnDblClick = nil
        end
      end
    end
    inherited tsGroup: TTabSheet
      inherited GroupBox4: TGroupBox
        Height = 300
        inherited GroupBox5: TGroupBox
          Height = 273
          inherited lbColumns1: TListBox
            Height = 245
          end
        end
        inherited GroupBox9: TGroupBox
          Height = 147
          inherited chlSum: TCheckListBox
            Height = 72
          end
        end
      end
      inherited GroupBox8: TGroupBox
        Top = 315
        Height = 163
        inherited GroupBox3: TGroupBox
          Height = 135
          inherited lbColumns2: TListBox
            Top = 18
            Height = 105
          end
        end
        inherited GroupBox7: TGroupBox
          Height = 135
          inherited lbSort: TListBox
            Height = 107
          end
        end
      end
    end
  end
  inherited BitBtn1: TBitBtn
    TabOrder = 1
  end
  inherited BitBtn2: TBitBtn
    TabOrder = 2
  end
end
