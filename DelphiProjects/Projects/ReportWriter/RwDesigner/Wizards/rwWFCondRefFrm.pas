// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWFCondRefFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWFCondIdentFrm, Db, StdCtrls, CheckLst, ExtCtrls,
  rwDataDictionary, rwUtils, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  rwDesignClasses;

type
  TrwWFCondRef = class(TrwWFCondIdent)
  private
  public
    procedure Initialize(ATable, AField: string); reintroduce;
  end;

implementation

uses rwCustomDataDictionary;

{$R *.DFM}


procedure TrwWFCondRef.Initialize(ATable, AField: string);
var
  SQL: string;
  i: Integer;
  Obj: TrwDataDictTable;
  h: String;
begin
  Obj := TrwDataDictTable(DataDictionary.Tables.TableByName(ATable));

  if AField = '' then
    AField := Obj.PrimaryKey[0].Field.Name;

  SQL := 'select t.'+AField+' val, ';

  h := '';
  for i := 0 to Obj.LogicalKey.Count-1 do
    h := h+'||"  "||t.'+Obj.LogicalKey[i].Field.Name;
  if h = '' then
    h := 't.'+AField
  else
    Delete(h, 1, 8);

  SQL := SQL+h+' name from '+ATable;
  if TrwDataDictParams(Obj.Params).ParamValueStr <> '' then
    SQL := SQL+'('+TrwDataDictParams(Obj.Params).ParamValueStr+')';

  SQL := SQL+' t order by 2';

  dsValues.Close;
//  dsValues.Data := ReportServerObj.ImplementFunction([rsfLogicalQuery,
//    WizardFrm.WizardInfo.Client, SQL, LongInt(dsValues.Params)]);
  dsValues.Open;

  while not dsValues.Eof do
  begin
    chbValues.Items.Add(dsValues.FieldByName('name').AsString);
    dsValues.Next;
  end;
  dsValues.First;
end;

end.
 