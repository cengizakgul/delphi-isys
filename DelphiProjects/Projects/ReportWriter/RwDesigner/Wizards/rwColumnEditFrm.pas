// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwColumnEditFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, wwdbedit, Wwdbspin, ExtCtrls, rwWizardInfo, rwTypes,
  wwdblook, Wwdbdlg, ComCtrls, rwWizDM, Menus, rwUtils, rwBasicClasses, rwQBInfo,
  rwEngineTypes;

type
  TrwColumnEdit = class(TForm)
    pnBtn: TPanel;
    btnOk: TButton;
    btnCancel: TButton;
    Panel1: TPanel;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    lvTables: TTreeView;
    Label6: TLabel;
    lvFields: TTreeView;
    Label7: TLabel;
    grbExpr: TGroupBox;
    grbTitleFormat: TGroupBox;
    Label1: TLabel;
    edTitle: TEdit;
    chbTitleWrap: TCheckBox;
    grbColFormat: TGroupBox;
    Label2: TLabel;
    edWidth: TwwDBSpinEdit;
    Label4: TLabel;
    cbAlignment: TComboBox;
    Format: TLabel;
    cbFormat: TComboBox;
    lSize: TLabel;
    edSize: TwwDBSpinEdit;
    Label5: TLabel;
    memExpr: TMemo;
    edExpr: TEdit;
    btnAdd: TSpeedButton;
    btnSub: TSpeedButton;
    btnMul: TSpeedButton;
    btnDiv: TSpeedButton;
    btnLPar: TSpeedButton;
    btnRPar: TSpeedButton;
    btnBack: TSpeedButton;
    btnPut: TSpeedButton;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    Shape1: TShape;
    Label8: TLabel;
    Shape2: TShape;
    SpeedButton2: TSpeedButton;
    chbCalcCol: TCheckBox;
    GroupBox2: TGroupBox;
    chbSummarized: TCheckBox;
    chbSubstAbbr: TCheckBox;
    btnFunct: TSpeedButton;
    procedure chbCalcColClick(Sender: TObject);
    procedure btnPutClick(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lvTablesChange(Sender: TObject; Node: TTreeNode);
    procedure lvFieldsChange(Sender: TObject; Node: TTreeNode);
    procedure edExprChange(Sender: TObject);
    procedure lvFieldsEnter(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnFunctClick(Sender: TObject);
  private
    Expr: TStringList;
    FWizardInfo: TrwWizardInfo;
    FColumn: TrwWizTblColumn;
    procedure SyncExpression;
    procedure Initialize;
    procedure AssignValue;
    procedure SyncTree(TV: TTreeView; D: TObject);
  public
  end;

  function AddColumn(AWizardInfo: TrwWizardInfo): Boolean;
  function EditColumn(AWizardInfo: TrwWizardInfo; AColumn: TrwWizTblColumn): Boolean;

implementation

{$R *.DFM}

uses rwFunctSelectFrm;


function AddColumn(AWizardInfo: TrwWizardInfo): Boolean;
var
  Frm: TrwColumnEdit;
begin
  Frm := TrwColumnEdit.Create(nil);
  with Frm do
    try
      FWizardInfo := AWizardInfo;
      Initialize;
      Result := ShowModal = mrOK;
      if Result then
      begin
        FColumn := FWizardInfo.MainTable.Columns.Add;
        AssignValue;
      end;
    finally
      Free;
    end;
end;


function EditColumn(AWizardInfo: TrwWizardInfo; AColumn: TrwWizTblColumn): Boolean;
var
  Frm: TrwColumnEdit;
begin
  Frm := TrwColumnEdit.Create(nil);
  with Frm do
    try
      FWizardInfo := AWizardInfo;
      FColumn := AColumn;
      Initialize;

      if Assigned(FColumn.Field) then
      begin
        SyncTree(lvTables, TrwWizardQueryTableFields(FColumn.Field.Collection).RootTable);
        SyncTree(lvFields, FColumn.Field);
      end;

      edTitle.Text := FColumn.Title;
      chbCalcCol.Checked := FColumn.Calculated;
      chbTitleWrap.Checked := FColumn.WrapTitle;
      edWidth.Value := FColumn.Width;
      cbFormat.Text := FColumn.Format;
      edSize.Value := FColumn.ASCIISize;
      cbAlignment.ItemIndex := Ord(FColumn.Alignment);

      chbSubstAbbr.Checked := FColumn.SubstValue;
      Expr.Assign(FColumn.Expression);
      chbCalcCol.OnClick(nil);
      SyncExpression;

      chbSummarized.Checked := FColumn.Summarized;

      Result := ShowModal = mrOK;
      if Result then
        AssignValue;
    finally
      Free;
    end;
end;


procedure TrwColumnEdit.chbCalcColClick(Sender: TObject);
begin
  grbExpr.Enabled := chbCalcCol.Checked;
  chbSubstAbbr.Enabled := not chbCalcCol.Checked;
  if chbCalcCol.Checked then
  begin
    chbSubstAbbr.Checked := False;
    lvFields.OnChange(lvFields, lvFields.Selected);
  end
  else
  begin
    memExpr.Text := '';
    edExpr.Text := '';
  end;
end;

procedure TrwColumnEdit.btnPutClick(Sender: TObject);
begin
  if edExpr.Tag = 0 then
  begin
    Expr.Add(edExpr.Text);
    memExpr.Text := memExpr.Text + edExpr.Text;
  end
  else
  begin
    Expr.Add(TrwWizardQueryTableField(lvFields.Selected.Data).Alias+'='+edExpr.Text);
    memExpr.Text := memExpr.Text + edExpr.Text;
  end;
  edExpr.Text := '';
end;

procedure TrwColumnEdit.btnBackClick(Sender: TObject);
begin
  Expr.Delete(Expr.Count-1);
  SyncExpression;
end;

procedure TrwColumnEdit.btnAddClick(Sender: TObject);
begin
  Expr.Add(TSpeedButton(Sender).Caption);
  memExpr.Text := memExpr.Text + TSpeedButton(Sender).Caption;
end;

procedure TrwColumnEdit.FormCreate(Sender: TObject);
begin
  Expr := TStringList.Create;
end;

procedure TrwColumnEdit.FormDestroy(Sender: TObject);
begin
  Expr.Free;
end;

procedure TrwColumnEdit.SyncExpression;
var
  i: Integer;
begin
 memExpr.Clear;
 for i := 0 to Expr.Count-1 do
   if Pos('=', Expr[i]) = 0 then
     memExpr.Text := memExpr.Text+Expr[i]
   else
     memExpr.Text := memExpr.Text+Expr.Values[Expr.Names[i]];
end;

procedure TrwColumnEdit.Initialize;
begin
  chbCalcCol.OnClick(nil);
  WizDM.CreateTableTree(lvTables, FWizardInfo.MainQuery.Table);
  lSize.Visible := FWizardInfo.GenerateASCIIFile and (FWizardInfo.ASCIIDelimiter = '');
  edSize.Visible := lSize.Visible;
  cbAlignment.ItemIndex := 0;
end;

procedure TrwColumnEdit.AssignValue;
begin
  FColumn.Title := edTitle.Text;
  FColumn.Calculated := chbCalcCol.Checked;
  FColumn.WrapTitle := chbTitleWrap.Checked;
  FColumn.Width := edWidth.Value;
  FColumn.Format := cbFormat.Text;
  FColumn.Alignment := TAlignment(cbAlignment.ItemIndex);
  if Assigned(lvFields.Selected) then
    FColumn.DataField := TrwWizardQueryTableField(lvFields.Selected.Data).Alias;
  FColumn.SubstValue := chbSubstAbbr.Checked;
  FColumn.Expression := Expr;
  FColumn.ASCIISize := Trunc(edSize.Value);
  FColumn.Summarized := chbSummarized.Checked;
end;

procedure TrwColumnEdit.SyncTree(TV: TTreeView; D: TObject);
var
  i: Integer;
begin
  for i := 0 to TV.Items.Count-1 do
    if TV.Items[i].Data = D  then
    begin
      TV.Selected := TV.Items[i];
      break;
    end;
end;

procedure TrwColumnEdit.lvTablesChange(Sender: TObject; Node: TTreeNode);
begin
  WizDM.CreateFieldTree(lvFields, TrwWizardQueryTable(lvTables.Selected.Data));
end;

procedure TrwColumnEdit.lvFieldsChange(Sender: TObject; Node: TTreeNode);
var
  SF: TrwQBShowingField;
begin
  if not Assigned(Node) or (Node = lvFields.Items.GetFirstNode) then
    Exit;

  if Assigned(FWizardInfo.MainQuery.Table.QBQuery) then
  begin
    if edTitle.Text = '' then
      edTitle.Text := Node.Text;

    SF := FWizardInfo.MainQuery.Table.QBQuery.ShowingFields.FieldByInternalName(TrwWizardQueryTableField(Node.Data).FieldName);

    if SF.ExprType = rwvFloat then
    begin
      cbAlignment.ItemIndex := 1;
      cbFormat.ItemIndex := 1;
      chbSummarized.Checked := True;
    end
    else if SF.ExprType = rwvDate then
    begin
      cbAlignment.ItemIndex := 0;
      cbFormat.ItemIndex := 3;
    end;

    if chbCalcCol.Checked then
    begin
      edExpr.Text := Node.Text;
      edExpr.Tag := 1;
    end;
  end

  else
  begin
    if edTitle.Text = '' then
      edTitle.Text := TrwWizardQueryTableField(Node.Data).Field.DisplayName;

    if TrwWizardQueryTableField(Node.Data).Field.FieldValues.Text <> '' then
    begin
      chbSubstAbbr.Checked := True;
      chbSubstAbbr.Enabled := True;
    end
    else
    begin
      chbSubstAbbr.Checked := False;
      chbSubstAbbr.Enabled := False;
    end;

    if TrwWizardQueryTableField(Node.Data).Field.FieldType = ddtFloat  then
    begin
      cbAlignment.ItemIndex := 1;
      cbFormat.ItemIndex := 1;
      chbSummarized.Checked := True;
    end
    else if TrwWizardQueryTableField(Node.Data).Field.FieldType = ddtDateTime then
    begin
      cbAlignment.ItemIndex := 0;
      cbFormat.ItemIndex := 3;
    end;

    if chbCalcCol.Checked then
    begin
      if TrwWizardQueryTableField(Node.Data).Note = '' then
        edExpr.Text := TrwWizardQueryTableField(Node.Data).Path
      else
        edExpr.Text := TrwWizardQueryTableField(Node.Data).Note;
      edExpr.Tag := 1;
    end;
  end;
end;

procedure TrwColumnEdit.edExprChange(Sender: TObject);
begin
  edExpr.Tag := 0;
end;

procedure TrwColumnEdit.lvFieldsEnter(Sender: TObject);
begin
  lvFields.OnChange(lvFields, lvFields.Selected);
end;

procedure TrwColumnEdit.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult = mrOK then
  begin
    CanClose := False;

    if edTitle.Text = '' then
      raise ErwException.Create('Column title is empty');

    if edWidth.Value = 0 then
      raise ErwException.Create('Width of column is 0');

    if not chbCalcCol.Checked and not (Assigned(lvFields.Selected) and (lvFields.Selected.ImageIndex = 2)) then
      raise ErwException.Create('Field is not selected');

    if chbCalcCol.Checked and (Expr.Text = '') then
      raise ErwException.Create('Expression of calculated column is empty');

    CanClose := True;
  end;
end;


procedure TrwColumnEdit.btnFunctClick(Sender: TObject);
var
  h: string;
begin
  h := SelectFunction(Self);
  if Length(h) > 0 then
    edExpr.Text := h;
end;

end.
