object rwWFCond: TrwWFCond
  Left = 0
  Top = 0
  Width = 437
  Height = 24
  AutoScroll = False
  TabOrder = 0
  object lName: TLabel
    Left = 0
    Top = 0
    Width = 30
    Height = 24
    Align = alLeft
    Caption = 'lName'
  end
  object Panel1: TPanel
    Left = 30
    Top = 0
    Width = 407
    Height = 24
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object cbOper: TComboBox
      Left = 12
      Top = 1
      Width = 77
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        '='
        '<>'
        '>'
        '>='
        '<'
        '<=')
    end
  end
end
