// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwFilterEditFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, rwDataDictionary, rwWizardInfo;

type
  TrwFilterEdit = class(TForm)
    grbFields: TGroupBox;
    lbFields: TListBox;
    btnAdd: TSpeedButton;
    btnDel: TSpeedButton;
    btnEdit: TSpeedButton;
    GroupBox3: TGroupBox;
    lbFilter: TListBox;
    lName: TLabel;
    btnOk: TButton;
    btnCancel: TButton;
    procedure btnAddClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FObj: TrwDataDictTable;
    FFlt: TrwWizardQueryTblFilter;
    procedure SyncFilter;
    procedure CreateFieldsList(AObject: TrwDataDictTable);
  public
  end;

function EditFilter(AWizardInfo: TrwWizardInfo; ATable: TrwWizardQueryTable): Boolean;

implementation

{$R *.DFM}

uses rwWPFltEditFrm, rwCustomDataDictionary;

function EditFilter(AWizardInfo: TrwWizardInfo; ATable: TrwWizardQueryTable): Boolean;
var
  Frm: TrwFilterEdit;
begin
  Frm := TrwFilterEdit.Create(nil);
  with Frm do
    try
      FFlt.QueryTable := ATable;
      FFlt.Assign(ATable.Filter);
      CreateFieldsList(ATable.Table);
      SyncFilter;
      Result := ShowModal = mrOK;
      if Result then
        ATable.Filter.Assign(FFlt);
    finally
      Free;
    end;
end;


procedure TrwFilterEdit.CreateFieldsList(AObject: TrwDataDictTable);
var
  i: Integer;
begin
  lbFields.Clear;
  FObj := AObject;
  lName.Caption := FObj.DisplayName;
  for i :=0 to FObj.Fields.Count-1 do
  begin
    if FObj.Fields[i].DisplayName = '' then
      Continue;

    if FObj.PrimaryKey.FieldByName(FObj.Fields[i].Name) <> nil then
      Continue;

    if FObj.ForeignKeys.ForeignKeyByField(FObj.Fields[i]) <> nil then
      Continue;

    lbFields.Items.AddObject(FObj.Fields[i].DisplayName, FObj.Fields[i]);
  end;
end;

procedure TrwFilterEdit.SyncFilter;
var
  i: Integer;
begin
  lbFilter.Items.Clear;
  for i := 0 to FFlt.Count-1 do
    lbFilter.Items.AddObject(FFlt[i].DisplayExpression, FFlt[i]);
end;


procedure TrwFilterEdit.btnAddClick(Sender: TObject);
var
  FltCond: TrwWizardQueryTblFltCond;
begin
  FltCond := FFlt.Add;
  FltCond.FieldName := TrwDataDictField(lbFields.Items.Objects[lbFields.ItemIndex]).Name;
  if AddCondition(FltCond) then
    SyncFilter
  else
    FltCond.Free;
end;

procedure TrwFilterEdit.btnDelClick(Sender: TObject);
var
  FltCond: TrwWizardQueryTblFltCond;
begin
  FltCond := TrwWizardQueryTblFltCond(lbFilter.Items.Objects[lbFilter.ItemIndex]);
  FltCond.Free;
  SyncFilter;
end;

procedure TrwFilterEdit.btnEditClick(Sender: TObject);
var
  FltCond: TrwWizardQueryTblFltCond;
begin
  FltCond := TrwWizardQueryTblFltCond(lbFilter.Items.Objects[lbFilter.ItemIndex]);
  if AddCondition(FltCond) then
    SyncFilter;
end;

procedure TrwFilterEdit.FormCreate(Sender: TObject);
begin
  FFlt := TrwWizardQueryTblFilter.Create(TrwWizardQueryTblFltCond);
end;

procedure TrwFilterEdit.FormDestroy(Sender: TObject);
begin
  FFlt.Free;
end;

end.
