inherited rwWizardDBDTEdit: TrwWizardDBDTEdit
  Left = 397
  Top = 188
  Caption = 'rwWizardDBDTEdit'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pcPages: TPageControl
    inherited tsPrimary: TTabSheet
      inherited GroupBox12: TGroupBox
        Top = 244
        Width = 618
        Height = 235
        inherited memDescr: TMemo
          Left = 11
          Top = 18
          Width = 596
          Height = 206
        end
      end
      inherited rgGenASCII: TRadioGroup
        Visible = False
      end
      inherited chbGenASCII: TCheckBox
        Visible = False
      end
      inherited chbQuoted: TCheckBox
        TabOrder = 8
      end
      inherited GroupBox11: TGroupBox
        Height = 182
        inherited clPayrolls: TCheckListBox
          Height = 155
        end
      end
      object GroupBox13: TGroupBox
        Left = 4
        Top = 96
        Width = 235
        Height = 142
        Caption = 'DBDT Settings'
        TabOrder = 7
        object Label17: TLabel
          Left = 8
          Top = 24
          Width = 58
          Height = 13
          Caption = 'Group Level'
        end
        object Label18: TLabel
          Left = 8
          Top = 54
          Width = 56
          Height = 13
          Caption = 'Group Type'
        end
        object Label19: TLabel
          Left = 8
          Top = 84
          Width = 51
          Height = 13
          Caption = 'Group Sort'
        end
        object Label20: TLabel
          Left = 8
          Top = 114
          Width = 49
          Height = 13
          Caption = 'Detail Sort'
        end
        object cmbxGroupLevel: TisRWDBComboBox
          Left = 121
          Top = 21
          Width = 105
          Height = 21
          HelpContext = 50502
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = True
          AutoDropDown = True
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 0
          UnboundDataType = wwDefault
        end
        object cmbxGroupType: TisRWDBComboBox
          Left = 121
          Top = 51
          Width = 105
          Height = 21
          HelpContext = 50502
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 1
          UnboundDataType = wwDefault
        end
        object cmbxGroupSort: TisRWDBComboBox
          Left = 121
          Top = 111
          Width = 105
          Height = 21
          HelpContext = 50502
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 2
          UnboundDataType = wwDefault
        end
        object cmbxGroupDetailSort: TisRWDBComboBox
          Left = 121
          Top = 81
          Width = 105
          Height = 21
          HelpContext = 50502
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 3
          UnboundDataType = wwDefault
        end
      end
    end
    inherited tsColumns: TTabSheet
      inherited GroupBox2: TGroupBox
        Height = 478
        inherited lvColumn: TListView
          Height = 448
        end
      end
      object chbEEHeader: TCheckBox
        Left = 6
        Top = 489
        Width = 107
        Height = 17
        Caption = 'Print EE header'
        TabOrder = 1
      end
      object chbEEFooter: TCheckBox
        Left = 121
        Top = 489
        Width = 88
        Height = 17
        Caption = 'Print EE total'
        TabOrder = 2
      end
    end
    inherited tsGroup: TTabSheet
      TabVisible = False
    end
  end
end
