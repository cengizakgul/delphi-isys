// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWPFltEditFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, rwWPParListFrm, rwWPParamFrm, rwWPParDateFrm, rwWPParStrFrm,
  rwWPParRefFrm, rwWizardInfo, rwTypes, rwDataDictionary, rwWFCondFrm,
  rwWFCondDateFrm, rwWFCondIdentFrm, rwWFCondRefFrm, rwWFCondStrFrm, rwWFCondNumFrm,
  rwQBInfo, rwEngineTypes;

type
  TrwWPFltEdit = class(TForm)
    Button1: TButton;
    Button2: TButton;
    rgrOper: TRadioGroup;
    grbCond: TGroupBox;
    procedure FormShow(Sender: TObject);
  private
    FCond: TrwWizardQueryTblFltCond;
    FCondFrm: TrwWFCond;
  public
  end;

function AddCondition(Cond: TrwWizardQueryTblFltCond): Boolean;

implementation

uses rwCustomDataDictionary;

{$R *.DFM}


function AddCondition(Cond: TrwWizardQueryTblFltCond): Boolean;
var
  Frm: TrwWPFltEdit;
begin
  Frm := TrwWPFltEdit.Create(nil);

  with Frm do
    try
      FCond := Cond;
      Result := (ShowModal = mrOK);
      if Result then
      begin
        FCondFrm.GetCond;
        FCond.AndOr := TrwQBFilterNodeType(rgrOper.ItemIndex);
        if FCond.AndOr = rntAnd then
          FCond.DisplayExpression := FCond.DisplayExpression + '  AND'
        else
          FCond.DisplayExpression := FCond.DisplayExpression + '  OR';
      end;
    finally
      FCondFrm.Free;
      Frm.Free;
    end;
end;


procedure TrwWPFltEdit.FormShow(Sender: TObject);
var
  RefObj: TrwDataDictTable;
  FK: TDataDictForeignKey;
begin
  RefObj := nil;

  FK := FCond.Field.Table.ForeignKeys.ForeignKeyByField(FCond.Field, 0);
  if Assigned(FK) then
    RefObj := TrwDataDictTable(FK.Table);

  FCondFrm := nil;

  if Assigned(RefObj) then
  begin
    FCondFrm := TrwWFCondRef.Create(Self);
    TrwWFCondRef(FCondFrm).Initialize(RefObj.Name, '');
  end

  else if FCond.Field.FieldValues.Text <> '' then
  begin
    FCondFrm := TrwWFCondIdent.Create(Self);
//    TrwWFCondIdent(FCondFrm).Initialize(FCond.Field.FieldValues.Text); // issue 53260
    TrwWFCondIdent(FCondFrm).Initialize(FCond.Field.FieldValuesReal);   // issue 53260
  end

  else if FCond.Field.FieldType = ddtDateTime then
    FCondFrm := TrwWFCondDate.Create(Self)

  else if Pos('=', FCond.Value1) > 0 then
  begin
    FCondFrm := TrwWFCondIdent.Create(Self);
    TrwWFCondIdent(FCondFrm).Initialize(FCond.Value1);
  end
  else if FCond.Field.FieldType = ddtString then
    FCondFrm := TrwWFCondStr.Create(Self)
  else
    FCondFrm := TrwWFCondNum.Create(Self);

  FCondFrm.Condition := FCond;
  FCondFrm.PutCond;
  FCondFrm.Top := 20;
  FCondFrm.Left := 8;
  FCondFrm.Parent := grbCond;

  rgrOper.ItemIndex := Ord(FCond.AndOr);
end;

end.
