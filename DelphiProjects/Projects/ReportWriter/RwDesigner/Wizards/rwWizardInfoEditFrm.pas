// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWizardInfoEditFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TypInfo, StdCtrls, Buttons, rwWizardInfo, wwdblook, Db, ComCtrls,
  CheckLst, rwTypes, ExtCtrls, rwDataDictionary, Variants,
  rwWizDM, rwWizardFrm, rwDB, rwBasicClasses, rwCommonClasses, rwReport,
  rwReportControls, rwInputFormControls, rwUtils, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, rwDesignClasses, rwEngineTypes,
  ISDataAccessComponents;

type

  TrwWizInfOneTable = class(TrwWizardInfo)
  private
    FReportTitle: string;
    FPrintTitleOnEachPage: Boolean;
    FMultiClient: Boolean;
    FPrintSBInfo: Boolean;
    FPrintCOInfo: Boolean;
    FAsOfDate: TDateTime;

  protected
    procedure CheckQueryParams(AQuery: TrwQuery; var AEventHandler: string); override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure CreateReport; override;
    procedure Assign(Source: TPersistent); override;
    class function FormEditor: TClass; override;
    class function WizardName: string; override;
  published
    property ReportTitle: string read FReportTitle write FReportTitle;
    property PrintTitleOnEachPage: Boolean read FPrintTitleOnEachPage write FPrintTitleOnEachPage;
    property MultiClient: Boolean read FMultiClient write FMultiClient;
    property PrintSBInfo: Boolean read FPrintSBInfo write FPrintSBInfo;
    property PrintCOInfo: Boolean read FPrintCOInfo write FPrintCOInfo;
    property AsOfDate: TDateTime read FAsOfDate write FAsOfDate;
  end;



  TrwWizardInfoEdit = class(TrwWizard)
    GroupBox13: TGroupBox;
    edTitle: TEdit;
    chbPrntTitleOnEachPage: TCheckBox;
    chbSBInfo: TCheckBox;
    chbCOInfo: TCheckBox;
    chbAsOfDate: TCheckBox;
    GroupBox11: TGroupBox;
    dtAsOfDate: TDateTimePicker;
    rbCoReport: TRadioButton;
    rbMClReport: TRadioButton;
    clClients: TCheckListBox;
    procedure chbAsOfDateClick(Sender: TObject);
    procedure rbMClReportClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Initialize; override;
    procedure Finalize; override;
  end;


implementation

uses rwColumnEditFrm, rwFilterEditFrm, rwTableAddFrm, rwFieldAddFrm, rwGroupEditFrm;

{$R *.DFM}



{ TrwWizInfOneTable }

constructor TrwWizInfOneTable.Create(AOwner: TComponent);
begin
  inherited;
  FPrintSBInfo := True;
  FPrintCOInfo := True;
end;



class function TrwWizInfOneTable.FormEditor: TClass;
begin
  Result := TrwWizardInfoEdit;
end;


procedure TrwWizInfOneTable.Assign(Source: TPersistent);
begin
  inherited;
  ReportTitle := TrwWizInfOneTable(Source).ReportTitle;
  PrintTitleOnEachPage := TrwWizInfOneTable(Source).PrintTitleOnEachPage;
  MultiClient := TrwWizInfOneTable(Source).MultiClient;
  PrintSBInfo := TrwWizInfOneTable(Source).PrintSBInfo;
  PrintCOInfo := TrwWizInfOneTable(Source).PrintCOInfo;
  AsOfDate := TrwWizInfOneTable(Source).AsOfDate;
end;


procedure TrwWizInfOneTable.CreateReport;
var
  Q1: TrwQuery;
  h: String;
  C, C1: TrwComponent;
  Col: TrwWizTblColumn;
  i,j,k,m: Integer;
  lWidth, nW, dW: Integer;
  Buf: TrwBuffer;
  DS: TrwDataSet;
  SR: TrwSubReport;
  Fld: TrwWizardQueryTableField;
  Cont: TrwControl;
  V: TrwGlobalVariable;

begin
  inherited;

  if MainTable.Columns.Count = 0 then
    Exit;

  //Global variables
  if Self.AsOfDate <> 0 then
  begin
    V:= Report.ReportForm.GlobalVarFunc.Variables.Add;
    V.Name := 'AsOfDate';
    V.VarType := rwvDate;
    V.Value := Self.AsOfDate;
  end;

  V:= Report.ReportForm.GlobalVarFunc.Variables.Add;
  V.Name := 'Clients';
  V.VarType := rwvArray;
  AssignArrValue(V, Client);

  V:= Report.ReportForm.GlobalVarFunc.Variables.Add;
  V.Name := 'Companies';
  V.VarType := rwvArray;
  AssignArrValue(V, Company);

  V:= Report.ReportForm.GlobalVarFunc.Variables.Add;
  V.Name := 'Client';
  V.VarType := rwvInteger;
  V.Value := 0;

  V:= Report.ReportForm.GlobalVarFunc.Variables.Add;
  V.Name := 'Company';
  V.VarType := rwvInteger;
  V.Value := 0;


  //Create Title
  if ReportTitle <> '' then
  begin
    if PrintTitleOnEachPage then
    begin
      i := Report.ReportForm.AddBand(rbtPageHeader);
    end
    else
      i := Report.ReportForm.AddBand(rbtTitle);
    FDefContainer := Report.ReportForm.Bands[i];

    C := CreateLabel('lTitle', ReportTitle, 0, 0, 10, 30, alTop, taCenter);
    TrwLabel(C).Font.Size := 18;
    TrwLabel(C).Font.Style := TrwLabel(C).Font.Style+[fsBold];
    TrwLabel(C).Font.Color := clNavy;
  end
  else
  begin
    i := Report.ReportForm.AddBand(rbtTitle);
    FDefContainer := Report.ReportForm.Bands[i];
  end;
  FDefContainer.Height := 50;


  //Information about company(companies)
  if not MultiClient then
  begin
    if PrintCOInfo then
    begin
      C := CreateLibComponent('pnCompInfo', 'TrwlCoInfoContainer', 0, 0, alBottom);
      FDefContainer.Height := FDefContainer.Height+TrwPanel(C).Height;
    end
  end
  else
  begin
    if PrintCOInfo then
    begin
      C := CreateLibComponent('bCompany', 'TrwlCoBuffer', 0, 0, alBottom);
      FObjList.Add(C);

      SR := TrwSubReport.Create(FDefOwner);
      SR.Name := 'sbrCompanies';
      FDefOwner := SR.ReportForm;
      FObjList.Add(SR);
      i := SR.ReportForm.AddBand(rbtTitle);
      FDefContainer := SR.ReportForm.Bands[i];
      C := CreateLabel('lCompany', 'Companies:', 0, 0, 100, 16, alLeft, taLeftJustify);
      TrwLabel(C).Font.Style := TrwLabel(C).Font.Style+[fsItalic];
      TrwLabel(C).Font.Size := 10;
      FDefContainer.Height := 16;
      i := SR.ReportForm.AddBand(rbtDetail);
      FDefContainer := SR.ReportForm.Bands[i];
      FDefContainer.Top := 30;
      FDefContainer.Height := 16;
      C := CreateDBText('dbtCompany', 10, 0, 450, 16, alNone, taLeftJustify, nil, 'NAME');
      TrwDBText(C).Font.Size := 10;
      C.Events['OnSetText'].HandlersText := ' Text := ''#''+sbrCompanies.MasterDataSource.FieldValue(''custom_nbr'')+'' ''+Text;';
      SR.Events['BeforePrint'].HandlersText := '  dbtCompany.DataSource := sbrCompanies.MasterDataSource;';
      FDefOwner := Report.ReportForm;
    end;
  end;

  //Create Pattern parts
  if PrintSBInfo then
  begin
    i := Report.ReportForm.AddBand(rbtPageFooter);
    FDefContainer := Report.ReportForm.Bands[i];
    FDefContainer.Height := 73;
    TrwFramedContainer(FDefContainer).BoundLines := [rclTop];
    C := CreateLibComponent('pnSBInfo', 'TrwlSBInfoContainer', 0, 0, alClient);
    if not MultiClient then
      C.Events['OnPrint'].HandlersText := '  Self.Company := Company;'+#13+
                                          '  inherited;';
  end;


  //Create Queries and Final Buffer
  FInFormParamList.Clear;
  Q1 := BuildQuery(MainQuery, 'Query');
  Buf := BuildBuffer(MainQuery, 'bMain', 'Tmp1');

  h := '';
  for i := 0 to Buf.Fields.Count - 1 do
  begin
    h := h + #13 + '     ' + Buf.Fields[i].FieldName;
    if i < Buf.Fields.Count - 1 then
      h := h + ',';
  end;
  h := 'INSERT INTO ' + Buf.TableName + ' (' + h + ')';
  Q1.SQL.Text := h + #13 + Q1.SQL.Text;

  Q1 := BufQuery(Buf, MainTable.Columns);
  Q1.Name := 'qBufSum';

  for i := 0 to FObjList.Count-1 do
  begin
    TrwNoVisualComponent(FObjList[i]).Top := 20;
    TrwNoVisualComponent(FObjList[i]).Left := 20+i*40;
  end;

  Report.MasterDataSource := Q1;

  h := '//Loop by companies and filling of buffer';
  h := h+#13+'var N: Integer;';
  h := h+#13+'var i: Integer;';
  h := h+#13+'  N := ArrLength(Companies)-1;';
  h := h+#13+'  for i := 0 to N do';
  h := h+#13+'    Client := Integer(Clients[i]);';
  h := h+#13+'    Company := Integer(Companies[i]);';
  h := h+#13+'    OpenClient(Client);';
  h := h+#13+'    Query1.BeforeOpen;';
  h := h+#13+'    Query1.Execute;';
  h := h+#13+'  endfor;';

  if MultiClient and PrintCOInfo then
  begin
    h := h+#13#13+'  bCompany.FillCoInfo := True;' +
              #13+'  bCompany.Initialize(Clients, Companies);';
    h := h+#13+'  sbrCompanies.MasterDataSource := bCompany;';

    if not Report.Bands.Title then
    begin
      i := Report.ReportForm.AddBand(rbtTitle);
      Report.ReportForm.Bands[i].Height :=  3;
    end
    else
      i := Report.ReportForm.IndexOfBand(rbtTitle);
    Report.ReportForm.Bands[i].Events['AfterPrint'].HandlersText := '  sbrCompanies.Print;';
  end;

  Report.Events['BeforePrint'].HandlersText := h;


  //Create Headers of Columns
  i := Report.ReportForm.AddBand(rbtHeader);
  FDefContainer := Report.ReportForm.Bands[i];
  FDefContainer.Height := 23;
  j := 0;
  k := 0;
  C := nil;
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    if Assigned(MainTable.Columns[i].Group) and MainTable.Columns[i].GroupHeader then
      Continue;
    j := j+MainTable.Columns[i].WidthPix;
    Inc(k);
  end;
  lWidth := Report.PageFormat.Width-Report.PageFormat.LeftMargin-Report.PageFormat.RightMargin;
  lWidth := Trunc(ConvertUnit(lWidth*100, utMMThousandths, utScreenPixels));
  if j < lWidth then
    dW := (lWidth-j) div k
  else
    dW := 0;

  j := 0;
  nW := 0;
  Col := nil;
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    if Assigned(MainTable.Columns[i].Group) and MainTable.Columns[i].GroupHeader then
      Continue;
    Col := MainTable.Columns[i];
    C := CreateLabel('lColHeader'+IntToStr(i), Col.Title, j, 0, Col.WidthPix+dW, 10, alLeft, taCenter);
    j := TrwLabel(C).Left+TrwLabel(C).Width+1;
    TrwLabel(C).Font.Style := TrwLabel(C).Font.Style+[fsBold];
    TrwLabel(C).WordWrap := Col.WrapTitle;
    TrwLabel(C).BoundLines := [rclLeft, rclTop, rclBottom];
    TrwLabel(C).Color := 15461355;
    TrwLabel(C).Transparent := False;
    TrwLabel(C).Layout := tlCenter;
    Col.FRealPixWidth := TrwLabel(C).Width;
    nW := nW + Col.FRealPixWidth;
  end;
  TrwLabel(C).BoundLines := TrwLabel(C).BoundLines + [rclRight];
  Col.FRealPixWidth := Col.FRealPixWidth + (lWidth-nW);
  TrwLabel(C).Width := Col.FRealPixWidth;


  //Create Columns
  i := Report.ReportForm.IndexOfBand(rbtDetail);
  FDefContainer := Report.ReportForm.Bands[i];
  FDefContainer.Height := 17;
  j := 0;
  m := 1;
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    if Assigned(MainTable.Columns[i].Group) and MainTable.Columns[i].GroupHeader then
      Continue;

    Col := MainTable.Columns[i];
    if MainTable.Columns[i].Calculated then
    begin
      C := CreateLabel('lCol'+IntToStr(i), 'lCol'+IntToStr(i), j, 0, Col.FRealPixWidth, 10, alLeft, Col.Alignment);

      h := '  lCol'+IntToStr(i);
      if Col.Format <> '' then
      begin
        TrwLabel(C).TextByValue := True;
        TrwLabel(C).Format := Col.Format;
        TrwLabel(C).Value := C.Name;
        h := h+'.Value';
      end
      else
        h := h+'.Text';

      h := h+' := ';
      for k := 0 to Col.Expression.Count-1 do
        if Pos('=', Col.Expression[k]) = 0 then
          h := h+Col.Expression[k]
        else
        begin
          Fld := MainQuery.FieldByAlias(Col.Expression.Names[k]);
          h := h+Report.MasterDataSource.Name+'.FieldValue('''+Fld.Alias+''')';
        end;

      C.Events['OnPrint'].HandlersText := h+';';
    end
    else
    begin
      DS := Report.MasterDataSource;
      C := CreateDBText('dbCol'+IntToStr(i), j, 0, Col.FRealPixWidth, 10, alLeft, Col.Alignment, DS, Col.DataField);
      if TrwDataDictTable(Col.Field.Field.Table).TableType = rwtDDExternal then
        TrwDBText(C).SubstID := TrwDataDictTable(Col.Field.Field.Table).Name + '.' + Col.Field.Field.Name
      else
        TrwDBText(C).SubstID := '';
      TrwDBText(C).Substitution := Col.SubstValue;
      TrwDBText(C).Format := Col.Format;
    end;
    if GenerateASCIIFile then
    begin
      TrwCustomText(C).ASCIIPrintLineSequence := 1;
      TrwCustomText(C).ASCIIPrintColSequence := m;
      TrwCustomText(C).ASCIISize := Col.ASCIISize;
    end;
    TrwCustomText(C).BoundLines := [rclLeft];
    j := TrwControl(C).Left+TrwControl(C).Width+1;
    Col.Control := TrwCustomText(C);
    Inc(m);
  end;


  //Create Groups
  i := Report.ReportForm.IndexOfBand(rbtDetail);
  Cont := Report.ReportForm.Bands[i];
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    Col := MainTable.Columns[i];
    if Assigned(Col.Group) then
    begin
      if Col.GroupHeader then
      begin
        FDefContainer := Col.Group.GroupHeader;

        C := CreateDBText('dbGrpHeader'+IntToStr(i), 0, 0, lWidth, FDefContainer.Height, alLeft,
          Col.Alignment, Report.MasterDataSource, Col.DataField);

        if TrwDataDictTable(Col.Field.Field.Table).TableType = rwtDDExternal then
          TrwDBText(C).SubstID := TrwDataDictTable(Col.Field.Field.Table).Name + '.' + Col.Field.Field.Name
        else
          TrwDBText(C).SubstID := '';
        TrwDBText(C).Substitution := Col.SubstValue;
        TrwDBText(C).Format := Col.Format;
        TrwCustomText(C).Font.Style := TrwCustomText(C).Font.Style + [fsItalic];
      end
      else
      begin
        h := 'lColSumTotal'+IntToStr(i);
        j := Report.ReportForm.IndexOfBand(rbtSummary);
        if j <> -1 then
        begin
          C1 := CreateLabel(h, '', Col.Control.Left, 0, Col.Control.Width, 10, alLeft, taCenter);
          TrwLabel(C1).BoundLines := [rclLeft];
          TrwLabel(C1).Container := Report.ReportForm.Bands[j];
        end;
      end;

      if Col.GroupFooter then
        for j := 0 to MainTable.Columns.Count-1 do
        begin
          if Assigned(MainTable.Columns[j].Group) and MainTable.Columns[j].GroupHeader then
            Continue;

          h := 'lColSum'+IntToStr(j)+'Grp'+IntToStr(i);
          if MainTable.Columns[j].GroupSummary then
          begin
            Col.Group.GroupFooter.Height := 17;
            Col.Group.GroupFooter.BoundLines := [rclLeft, rclRight, rclTop, rclBottom];
            CreateGroupSumLabel(MainTable.Columns[j], h, TrwBand(Cont), Col.Group.GroupHeader.Events['OnPrint'],
              Col.Group.GroupFooter);
          end
          else
          begin
            C1 := CreateLabel(h, '', MainTable.Columns[j].Control.Left, 0, MainTable.Columns[j].Control.Width, 10, alLeft, taCenter);
            TrwLabel(C1).BoundLines := [rclLeft];
            TrwLabel(C1).Container := Col.Group.GroupFooter;
          end;
        end;
    end

    else
    begin
      h := 'lColSumTotal'+IntToStr(i);
      if Col.GroupSummary then
        CreateGroupSumLabel(Col, h, TrwBand(Cont), Report.Events['BeforePrint'],
          Report.ReportForm.Bands[Report.ReportForm.IndexOfBand(rbtSummary)])
      else
      begin
        j := Report.ReportForm.IndexOfBand(rbtSummary);
        if j <> -1 then
        begin
          C1 := CreateLabel(h, '', Col.Control.Left, 0, Col.Control.Width, 10, alLeft, taCenter);
          TrwLabel(C1).BoundLines := [rclLeft];
          TrwLabel(C1).Container := Report.ReportForm.Bands[j];
        end;
      end;
    end;
  end;


  i := Report.ReportForm.AddBand(rbtFooter);
  FDefContainer := Report.ReportForm.Bands[i];
  FDefContainer.Height := 1;
  TrwFramedContainer(FDefContainer).BoundLines := [rclTop];

  RealignBands;

  CreateInputForm;
end;


class function TrwWizInfOneTable.WizardName: string;
begin
  Result := 'One table report';
end;


procedure TrwWizInfOneTable.CheckQueryParams(AQuery: TrwQuery; var AEventHandler: string);
begin
  inherited;
  if Self.AsOfDate = 0 then
    CheckQueryParam(AQuery, 'MY_DATE', '''now''', AEventHandler)
  else
    CheckQueryParam(AQuery, 'MY_DATE', 'AsOfDate', AEventHandler);
end;


{TrwWizardInfoEdit}

procedure TrwWizardInfoEdit.Initialize;
var
  L1, L2: TStringList;
  i: Integer;
begin
  inherited;

  edTitle.Text := TrwWizInfOneTable(WizardInfo).ReportTitle;
  chbPrntTitleOnEachPage.Checked := TrwWizInfOneTable(WizardInfo).PrintTitleOnEachPage;
  rbMClReport.Checked := TrwWizInfOneTable(WizardInfo).MultiClient;
  rbMClReport.OnClick(nil);
  chbSBInfo.Checked := TrwWizInfOneTable(WizardInfo).PrintSBInfo;
  chbCOInfo.Checked := TrwWizInfOneTable(WizardInfo).PrintCOInfo;

  if TrwWizInfOneTable(WizardInfo).MultiClient then
  begin
    L1 := TStringList.Create;
    L2 := TStringList.Create;
    try
      L1.Text := TrwWizInfOneTable(WizardInfo).Client;
      L2.Text := TrwWizInfOneTable(WizardInfo).Company;

      for i := 0 to L1.Count-1 do
        if dsClients.Locate('cl_nbr;co_nbr', VarArrayOf([StrToInt(L1[i]), StrToInt(L2[i])]), []) then
          clClients.Checked[dsClients.RecNo-1] := True;
    finally
      L1.Free;
      L2.Free;
    end;
  end;

  if TrwWizInfOneTable(WizardInfo).AsOfDate = 0 then
    chbAsOfDate.Checked := False
  else
    chbAsOfDate.Checked := True;
  chbAsOfDate.OnClick(nil);
  dtAsOfDate.Date := TrwWizInfOneTable(WizardInfo).AsOfDate;
end;

procedure TrwWizardInfoEdit.Finalize;
var
  i: Integer;
begin
  inherited;

  TrwWizInfOneTable(WizardInfo).ReportTitle := edTitle.Text;
  TrwWizInfOneTable(WizardInfo).PrintTitleOnEachPage := chbPrntTitleOnEachPage.Checked;
  TrwWizInfOneTable(WizardInfo).PrintSBInfo := chbSBInfo.Checked;
  TrwWizInfOneTable(WizardInfo).PrintCOInfo := chbCOInfo.Checked;

  if chbAsOfDate.Checked then
    TrwWizInfOneTable(WizardInfo).AsOfDate := dtAsOfDate.Date
  else
    TrwWizInfOneTable(WizardInfo).AsOfDate := 0;

  if rbMClReport.Checked then
  begin
    TrwWizInfOneTable(WizardInfo).MultiClient := True;
    TrwWizInfOneTable(WizardInfo).Client := '';
    TrwWizInfOneTable(WizardInfo).Company := '';
    for i := 0 to clClients.Items.Count-1 do
      if clClients.Checked[i] then
      begin
        dsClients.RecNo := i+1;
        if TrwWizInfOneTable(WizardInfo).Client <> '' then
        begin
          TrwWizInfOneTable(WizardInfo).Client := TrwWizInfOneTable(WizardInfo).Client+#13;
          TrwWizInfOneTable(WizardInfo).Company := TrwWizInfOneTable(WizardInfo).Company+#13;
        end;
        TrwWizInfOneTable(WizardInfo).Client := TrwWizInfOneTable(WizardInfo).Client+dsClients.FieldByName('CL_NBR').AsString;
        TrwWizInfOneTable(WizardInfo).Company := TrwWizInfOneTable(WizardInfo).Company+dsClients.FieldByName('CO_NBR').AsString;
      end;
  end
  else
    TrwWizInfOneTable(WizardInfo).MultiClient := False;
end;

procedure TrwWizardInfoEdit.chbAsOfDateClick(Sender: TObject);
begin
  dtAsOfDate.Enabled := chbAsOfDate.Checked;
  if not chbAsOfDate.Checked then
    dtAsOfDate.Date := 0
  else
    dtAsOfDate.Date := Date;
end;

procedure TrwWizardInfoEdit.rbMClReportClick(Sender: TObject);
var
  i: Integer;
begin
  rbCoReport.Checked := not rbMClReport.Checked;
  cbClients.Visible := not rbMClReport.Checked;
  clClients.Visible := rbMClReport.Checked;
  if rbMClReport.Checked then
  begin
    rbMClReport.Top := clClients.Top+clClients.Height+3;
    for i := 0 to clClients.Items.Count-1 do
      clClients.Checked[i] := False;
    if cbClients.Text <> '' then
      clClients.Checked[dsClients.RecNo-1] := True;
  end
  else
    rbMClReport.Top := cbClients.Top+cbClients.Height+3;

  rbCoReport.Top := rbMClReport.Top;
end;



procedure TrwWizardInfoEdit.FormCreate(Sender: TObject);
begin
  inherited;
  
  dsClients.DisableControls;
  try
    dsClients.First;
    while not dsClients.Eof do
    begin
      clClients.Items.Add(dsClients.FieldByName('Name').AsString);
      dsClients.Next;
    end;
    dsClients.First;
  finally
    dsClients.EnableControls;
  end;
end;

initialization
  RegisterClass(TrwWizInfOneTable);
  WizardsLst.Add(TrwWizInfOneTable);

finalization
  UnRegisterClass(TrwWizInfOneTable);
  WizardsLst.Remove(TrwWizInfOneTable);

end.
