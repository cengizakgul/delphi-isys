// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWizardDBDTEditFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWizardPrEditEdit, Db, Buttons, StdCtrls, ComCtrls, ExtCtrls,
  CheckLst, wwdblook, rwWizardInfo, rwReport, rwUtils, Mask, wwdbedit,
  Wwdotdot, Wwdbcomb, rwBasicClasses, rwDB, rwCommonClasses,
  rwTypes, rwReportControls, ISBasicClasses, kbmMemTable,
  ISKbmMemDataSet, rwDesignClasses, rwEngineTypes, rwDataDictionary,
  ISDataAccessComponents;

type
  TrwWizInfDBDT = class(TrwWizInfPayroll)
  private
    FGroupLevel: Integer;
    FGroupType: Integer;
    FGroupSort: Integer;
    FGroupDetailSort: Integer;
    FPrintEEHeader: Boolean;
    FPrintEEFooter: Boolean;
  protected
    procedure CreateReportObject; override;
    procedure CustomQueryCondition(ATable: TrwWizardQueryTable; var sFrom, sWhere: string); override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure CreateReport; override;
    procedure Assign(Source: TPersistent); override;
    class function FormEditor: TClass; override;
    class function WizardName: string; override;
  published
    property GroupLevel: Integer read FGroupLevel write FGroupLevel;
    property GroupType: Integer read FGroupType write FGroupType;
    property GroupSort: Integer read FGroupSort write FGroupSort;
    property GroupDetailSort: Integer read FGroupDetailSort write FGroupDetailSort;
    property PrintEEHeader: Boolean read FPrintEEHeader write FPrintEEHeader;
    property PrintEEFooter: Boolean read FPrintEEFooter write FPrintEEFooter;
  end;


  TrwWizardDBDTEdit = class(TrwWizardPrEdit)
    GroupBox13: TGroupBox;
    Label17: TLabel;
    cmbxGroupLevel: TisRWDBComboBox;
    Label18: TLabel;
    cmbxGroupType: TisRWDBComboBox;
    Label19: TLabel;
    cmbxGroupSort: TisRWDBComboBox;
    Label20: TLabel;
    cmbxGroupDetailSort: TisRWDBComboBox;
    chbEEHeader: TCheckBox;
    chbEEFooter: TCheckBox;
    procedure btnAddTblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Initialize; override;
    procedure Finalize; override;
  public
  end;

implementation

{$R *.DFM}

uses rwTableAddFrm;

{ TrwWizInfDBDT }

procedure TrwWizInfDBDT.Assign(Source: TPersistent);
begin
  inherited;
  GroupLevel := TrwWizInfDBDT(Source).GroupLevel;
  GroupType := TrwWizInfDBDT(Source).GroupType;
  GroupSort := TrwWizInfDBDT(Source).GroupSort;
  GroupDetailSort := TrwWizInfDBDT(Source).GroupDetailSort;
  PrintEEHeader := TrwWizInfDBDT(Source).PrintEEHeader;
  PrintEEFooter := TrwWizInfDBDT(Source).PrintEEFooter;
end;

constructor TrwWizInfDBDT.Create(AOwner: TComponent);
begin
  inherited;
  FGroupLevel := 1;
  FGroupType := 1;
  FGroupSort := 1;
  FGroupDetailSort := 1;
  PrintEEHeader := False;
  PrintEEFooter := False;
end;

procedure TrwWizInfDBDT.CreateReport;
var
  Q1: TrwQuery;
  h, h1: String;
  C, C1: TrwComponent;
  Col: TrwWizTblColumn;
  i,j,k,m: Integer;
  lWidth, nW, dW: Integer;
  Buf: TrwBuffer;
  DS: TrwDataSet;
  Fld: TrwWizardQueryTableField;
  Cont: TrwControl;
  V: TrwGlobalVariable;
  F: TrwWizardQueryTableField;
begin
  if MainTable.Columns.Count = 0 then
    Exit;

  FObjList.Clear;
  Report.Description := Description;
  Report.Name := 'rptMain';
  FDefOwner := Report.ReportForm;


  //Global variables
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('Clients')];
  AssignArrValue(V, Client);
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('Companies')];
  AssignArrValue(V, Company);
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('Payrolls')];
  AssignArrValue(V, Payroll);
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('ReportName')];
  V.Value := 'Payroll report';
  V:= Report.ReportForm.GlobalVarFunc.Variables.Add;
  V.Name := 'Company';
  V.VarType := rwvInteger;
  V.Value := 0;
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('GroupIndex')];
  V.Value := GroupLevel;
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('GroupType')];
  V.Value := GroupType;
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('GroupSort')];
  V.Value := GroupSort;
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('DetailSort')];
  V.Value := GroupDetailSort;
  V := Report.ReportForm.GlobalVarFunc.Variables[Report.ReportForm.GlobalVarFunc.Variables.IndexOf('DetailGap')];
  V.Value := False;


  //Create Queries and Final Buffer
  FInFormParamList.Clear;
  F := MainQuery.Table.Fields.Add;
  F.FieldName := 'EE_NBR';
  Q1 := BuildQuery(MainQuery, 'Query');
  Buf := BuildBuffer(MainQuery, 'bMain', 'TmpMain');

  h := '';
  for i := 0 to Buf.Fields.Count - 1 do
  begin
    h := h + #13 + '     ' + Buf.Fields[i].FieldName;
    if i < Buf.Fields.Count - 1 then
      h := h + ',';
  end;
  h := 'INSERT INTO ' + Buf.TableName + ' (' + h + ')';
  Q1.SQL.Text := h + #13 + Q1.SQL.Text;

  Q1 := BufQuery(Buf, MainTable.Columns);
  Q1.Name := 'qRes';
  i := Pos(#13#10'FROM',  Q1.SQL.Text);
  k := Pos(#13#10'GROUP BY',  Q1.SQL.Text);
  Q1.SQL.Text := Copy(Q1.SQL.Text, 1, i - 1) + ', t.ee_nbr1' + Copy(Q1.SQL.Text, i, Length(Q1.SQL.Text) - i - 1);
  if k = 0 then
    Q1.SQL.Text := Q1.SQL.Text + #13 + 'GROUP BY  t.ee_nbr1'
  else
    Q1.SQL.Text := Q1.SQL.Text + #13 + '  , t.ee_nbr1';



  for i := 0 to FObjList.Count-1 do
  begin
    TrwNoVisualComponent(FObjList[i]).Top := 20;
    TrwNoVisualComponent(FObjList[i]).Left := 20+i*40;
  end;

  h :=       '  Query1.BeforeOpen;';
  h := h+#13+'  Query1.Execute;';
  h := h+#13+'';
  h := h+#13+'  qRes.Open;';
  h := h+#13+'  bMain.Clear;';
  h := h+#13+'  bMain.Open;';
  h := h+#13+'  while not qRes.Eof do';
  h := h+#13+'    bMain.Append;';
  h1 := '';
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    if MainTable.Columns[i].Expression.Count > 0 then
      Continue;
    h := h+#13+'    bMain.SetFieldValue('''+MainTable.Columns[i].DataField+''', qRes.FieldValue('''+MainTable.Columns[i].DataField+''')'+');';
    h1 := h1 + ', ' + #13 + '  bres.' + MainTable.Columns[i].DataField;
  end;
  h := h+#13+'    bMain.SetFieldValue(''EE_NBR1'', qRes.FieldValue(''EE_NBR1''));';
  h1 := h1 + ', ' + #13 + '  bres.EE_NBR1';
  h := h+#13+'    bMain.Post;';
  h := h+#13+'    qRes.Next;';
  h := h+#13+'  endwhile;';

  h := h+#13+'  qRes.Close;';
  h := h+#13+'  bMain.Close;';

  Report.Events['BeforePrint'].HandlersText := Report.Events['BeforePrint'].HandlersText + #13 + h;

  Q1 := TrwQuery(Report.ReportForm.FindComponent('qMain'));
  i := Pos(#13#10'FROM',  Q1.SQL.Text);
  j := Pos(#13#10'WHERE', Q1.SQL.Text);
  k := Pos(#13#10'   %sort',  Q1.SQL.Text);
  Q1.SQL.Text := Copy(Q1.SQL.Text, 1, i) + h1 + Copy(Q1.SQL.Text, i, j - i) + '  ,TmpMain bres'  +
    Copy(Q1.SQL.Text, j, k - j - 1) + ' and e.ee_nbr = bres.ee_nbr1 ' + Copy(Q1.SQL.Text, k, Length(Q1.SQL.Text) - k + 1);


  //Create Headers of Columns
  i := Report.ReportForm.AddBand(rbtHeader);
  FDefContainer := Report.ReportForm.Bands[i];
  FDefContainer.Height := 23;
  j := 0;
  k := 0;
  C := nil;
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    if Assigned(MainTable.Columns[i].Group) and MainTable.Columns[i].GroupHeader then
      Continue;
    j := j+MainTable.Columns[i].WidthPix;
    Inc(k);
  end;
  lWidth := Report.PageFormat.Width-Report.PageFormat.LeftMargin-Report.PageFormat.RightMargin;
  lWidth := Trunc(ConvertUnit(lWidth*100, utMMThousandths, utScreenPixels));
  if j < lWidth then
    dW := (lWidth-j) div k
  else
    dW := 0;

  j := 0;
  nW := 0;
  Col := nil;
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    if Assigned(MainTable.Columns[i].Group) and MainTable.Columns[i].GroupHeader then
      Continue;
    Col := MainTable.Columns[i];
    C := CreateLabel('lColHeader'+IntToStr(i), Col.Title, j, 0, Col.WidthPix+dW, 10, alLeft, taCenter);
    j := TrwLabel(C).Left+TrwLabel(C).Width+1;
    TrwLabel(C).Font.Style := TrwLabel(C).Font.Style+[fsBold];
    TrwLabel(C).WordWrap := Col.WrapTitle;
    TrwLabel(C).BoundLines := [rclLeft, rclTop, rclBottom];
    TrwLabel(C).Color := 15461355;
    TrwLabel(C).Transparent := False;
    TrwLabel(C).Layout := tlCenter;
    Col.FRealPixWidth := TrwLabel(C).Width;
    nW := nW + Col.FRealPixWidth;
  end;
  TrwLabel(C).BoundLines := TrwLabel(C).BoundLines + [rclRight];
  Col.FRealPixWidth := Col.FRealPixWidth + (lWidth-nW);
  TrwLabel(C).Width := Col.FRealPixWidth;


  //Create Columns
  i := Report.ReportForm.IndexOfBand(rbtDetail);
  FDefContainer := Report.ReportForm.Bands[i];
  FDefContainer.Height := 17;
  j := 0;
  m := 1;
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    if Assigned(MainTable.Columns[i].Group) and MainTable.Columns[i].GroupHeader then
      Continue;

    Col := MainTable.Columns[i];
    if MainTable.Columns[i].Calculated then
    begin
      C := CreateLabel('lCol'+IntToStr(i), 'lCol'+IntToStr(i), j, 0, Col.FRealPixWidth, 10, alLeft, Col.Alignment);

      h := '  lCol'+IntToStr(i);
      if Col.Format <> '' then
      begin
        TrwLabel(C).TextByValue := True;
        TrwLabel(C).Format := Col.Format;
        TrwLabel(C).Value := C.Name;
        h := h+'.Value';
      end
      else
        h := h+'.Text';

      h := h+' := ';
      for k := 0 to Col.Expression.Count-1 do
        if Pos('=', Col.Expression[k]) = 0 then
          h := h+Col.Expression[k]
        else
        begin
          Fld := MainQuery.FieldByAlias(Col.Expression.Names[k]);
          h := h+Report.MasterDataSource.Name+'.FieldValue('''+Fld.Alias+''')';
        end;

      C.Events['OnPrint'].HandlersText := h+';';
    end
    else
    begin
      DS := Report.MasterDataSource;
      C := CreateDBText('dbCol'+IntToStr(i), j, 0, Col.FRealPixWidth, 10, alLeft, Col.Alignment, DS, Col.DataField);
      TrwDBText(C).SubstID := TrwDataDictTable(Col.Field.Field.Table).Name + '.' + Col.Field.Field.Name;
      TrwDBText(C).Substitution := Col.SubstValue;
      TrwDBText(C).Format := Col.Format;
    end;
    if GenerateASCIIFile then
    begin
      TrwCustomText(C).ASCIIPrintLineSequence := 1;
      TrwCustomText(C).ASCIIPrintColSequence := m;
      TrwCustomText(C).ASCIISize := Col.ASCIISize;
    end;
    TrwCustomText(C).BoundLines := [rclLeft];
    j := TrwControl(C).Left+TrwControl(C).Width+1;
    Col.Control := TrwCustomText(C);
    Inc(m);
  end;
  TrwCustomText(C).BoundLines := TrwCustomText(C).BoundLines + [rclRight];

  //Create Groups
  i := Report.ReportForm.IndexOfBand(rbtDetail);
  Cont := Report.ReportForm.Bands[i];
  for i := 0 to MainTable.Columns.Count-1 do
  begin
    Col := MainTable.Columns[i];
    if Assigned(Col.Group) then
    begin
      if Col.GroupHeader then
      begin
        FDefContainer := Col.Group.GroupHeader;

        C := CreateDBText('dbGrpHeader'+IntToStr(i), 0, 0, lWidth, FDefContainer.Height, alLeft,
          Col.Alignment, Report.MasterDataSource, Col.DataField);
        TrwDBText(C).SubstID := TrwDataDictTable(Col.Field.Field.Table).Name + '.' + Col.Field.Field.Name;
        TrwDBText(C).Substitution := Col.SubstValue;
        TrwDBText(C).Format := Col.Format;
        TrwCustomText(C).Font.Style := TrwCustomText(C).Font.Style + [fsItalic];
      end
      else
      begin
        h := 'lColSumTotal'+IntToStr(i);
        j := Report.ReportForm.IndexOfBand(rbtSummary);
        if j <> -1 then
        begin
          C1 := CreateLabel(h, '', Col.Control.Left, 0, Col.Control.Width, 10, alLeft, taCenter);
          TrwLabel(C1).BoundLines := [rclLeft];
          TrwLabel(C1).Container := Report.ReportForm.Bands[j];
        end;
      end;

      if Col.GroupFooter then
        for j := 0 to MainTable.Columns.Count-1 do
        begin
          if Assigned(MainTable.Columns[j].Group) and MainTable.Columns[j].GroupHeader then
            Continue;

          h := 'lColSum'+IntToStr(j)+'Grp'+IntToStr(i);
          if MainTable.Columns[j].GroupSummary then
          begin
            Col.Group.GroupFooter.Height := 17;
            Col.Group.GroupFooter.BoundLines := [rclLeft, rclRight, rclTop, rclBottom];
            CreateGroupSumLabel(MainTable.Columns[j], h, TrwBand(Cont), Col.Group.GroupHeader.Events['OnPrint'],
              Col.Group.GroupFooter);
          end
          else
          begin
            C1 := CreateLabel(h, '', MainTable.Columns[j].Control.Left, 0, MainTable.Columns[j].Control.Width, 10, alLeft, taCenter);
            TrwLabel(C1).BoundLines := [rclLeft];
            TrwLabel(C1).Container := Col.Group.GroupFooter;
          end;
        end;
    end

    else
    begin
      h := 'lColSumTotal'+IntToStr(i);
      if Col.GroupSummary then
        CreateGroupSumLabel(Col, h, TrwBand(Cont), Report.Events['BeforePrint'],
          Report.ReportForm.Bands[Report.ReportForm.IndexOfBand(rbtSummary)])
      else
      begin
        j := Report.ReportForm.IndexOfBand(rbtSummary);
        if j <> -1 then
        begin
          C1 := CreateLabel(h, '', Col.Control.Left, 0, Col.Control.Width, 10, alLeft, taCenter);
          TrwLabel(C1).BoundLines := [rclLeft];
          TrwLabel(C1).Container := Report.ReportForm.Bands[j];
        end;
      end;
    end;
  end;

  i := Report.ReportForm.AddBand(rbtFooter);
  FDefContainer := Report.ReportForm.Bands[i];
  FDefContainer.Height := 1;
  TrwFramedContainer(FDefContainer).BoundLines := [rclTop];

  FDefContainer := TrwBand(Report.ReportForm.FindComponent('gbhDivision'));
  TrwFramedContainer(FDefContainer).BoundLines := [rclLeft, rclRight, rclBottom];
  TrwFramedContainer(FDefContainer).BoundLinesStyle := psSolid;
  FDefContainer := TrwBand(Report.ReportForm.FindComponent('gbhBranch'));
  TrwFramedContainer(FDefContainer).BoundLines := [rclLeft, rclRight, rclBottom];
  TrwFramedContainer(FDefContainer).BoundLinesStyle := psSolid;
  FDefContainer := TrwBand(Report.ReportForm.FindComponent('gbhDepartment'));
  TrwFramedContainer(FDefContainer).BoundLines := [rclLeft, rclRight, rclBottom];
  TrwFramedContainer(FDefContainer).BoundLinesStyle := psSolid;
  FDefContainer := TrwBand(Report.ReportForm.FindComponent('gbhTeam'));
  TrwFramedContainer(FDefContainer).BoundLines := [rclLeft, rclRight, rclBottom];
  TrwFramedContainer(FDefContainer).BoundLinesStyle := psSolid;
  FDefContainer := TrwBand(Report.ReportForm.FindComponent('gbhEE'));
  TrwFramedContainer(FDefContainer).BoundLines := [rclLeft, rclRight, rclBottom];
  TrwFramedContainer(FDefContainer).BoundLinesStyle := psSolid;
  FDefContainer.Color := 15461355;
  FDefContainer.Visible := PrintEEHeader;
  FDefContainer := TrwBand(Report.ReportForm.FindComponent('gbfDivision'));
  TrwFramedContainer(FDefContainer).BoundLines := [rclLeft, rclRight, rclBottom];
  TrwFramedContainer(FDefContainer).BoundLinesStyle := psSolid;
  FDefContainer := TrwBand(Report.ReportForm.FindComponent('gbfBranch'));
  TrwFramedContainer(FDefContainer).BoundLines := [rclLeft, rclRight, rclBottom];
  TrwFramedContainer(FDefContainer).BoundLinesStyle := psSolid;
  FDefContainer := TrwBand(Report.ReportForm.FindComponent('gbfDepartment'));
  TrwFramedContainer(FDefContainer).BoundLines := [rclLeft, rclRight, rclBottom];
  TrwFramedContainer(FDefContainer).BoundLinesStyle := psSolid;
  FDefContainer := TrwBand(Report.ReportForm.FindComponent('gbfTeam'));
  TrwFramedContainer(FDefContainer).BoundLines := [rclLeft, rclRight, rclBottom];
  TrwFramedContainer(FDefContainer).BoundLinesStyle := psSolid;
  FDefContainer := TrwBand(Report.ReportForm.FindComponent('gbfEE'));
  TrwFramedContainer(FDefContainer).BoundLines := [rclLeft, rclRight, rclTop, rclBottom];
  TrwFramedContainer(FDefContainer).BoundLinesStyle := psSolid;
  FDefContainer.Visible := PrintEEFooter;
  FDefContainer := TrwBand(Report.ReportForm.FindComponent('SummaryBand'));
  TrwFramedContainer(FDefContainer).BoundLines := [rclLeft, rclRight, rclBottom];
  TrwFramedContainer(FDefContainer).BoundLinesStyle := psSolid;
  FDefContainer := TrwBand(Report.ReportForm.FindComponent('DetailBand'));
  TrwFramedContainer(FDefContainer).BoundLines := [];
  TrwFramedContainer(FDefContainer).BoundLinesStyle := psSolid;

  RealignBands;

  CreateInputForm;
  F.Free;
end;


procedure TrwWizInfDBDT.CreateReportObject;
begin
  FReport := TrwReport(SystemLibComponents[SystemLibComponents.IndexOf('TrwlDBDTReport')].CreateComp(nil, True));
end;

procedure TrwWizInfDBDT.CustomQueryCondition(ATable: TrwWizardQueryTable;
  var sFrom, sWhere: string);
begin
  inherited;
  if ATable.Table.Fields.FieldByName('EE_NBR') <> nil then
  begin
    if Pos(', tmp_ee tee', sFrom) = 0 then
      sFrom := sFrom + ', tmp_ee tee';
    sWhere := sWhere+' and'+#13+'   '+ATable.Alias+'.EE_NBR = tee.EE_NBR';
  end;
end;

class function TrwWizInfDBDT.FormEditor: TClass;
begin
  Result := TrwWizardDBDTEdit;
end;

class function TrwWizInfDBDT.WizardName: string;
begin
  Result := 'DBDT report';
end;

procedure TrwWizardDBDTEdit.btnAddTblClick(Sender: TObject);
var
  Tbl: TrwWizardQueryTable;
  f: string;
begin
  if not Assigned(lvTables.Selected) then
  begin
    Tbl := WizardInfo.MainQuery.Table;
    f := 'EE_NBR';
  end
  else
  begin
    Tbl := TrwWizardQueryTable(lvTables.Selected.Data);
    f := '';
  end;

  if AddTable(Tbl, f) then
   InitializeQuery;
end;

procedure TrwWizardDBDTEdit.Finalize;
begin
  inherited;
  TrwWizInfDBDT(WizardInfo).GroupLevel := StrToInt(cmbxGroupLevel.Value);
  TrwWizInfDBDT(WizardInfo).GroupType := StrToInt(cmbxGroupType.Value);
  TrwWizInfDBDT(WizardInfo).GroupSort := StrToInt(cmbxGroupSort.Value);
  TrwWizInfDBDT(WizardInfo).GroupDetailSort := StrToInt(cmbxGroupDetailSort.Value);

  TrwWizInfDBDT(WizardInfo).PrintEEHeader := chbEEHeader.Checked;
  TrwWizInfDBDT(WizardInfo).PrintEEFooter := chbEEFooter.Checked;
end;

procedure TrwWizardDBDTEdit.FormCreate(Sender: TObject);
begin
  inherited;
  cmbxGroupLevel.Items.Text := 'Company' + #9 + '1' + #13 +
                               'Division' + #9 + '2' + #13 +
                               'Branch' + #9 + '3' + #13 +
                               'Department' + #9 + '4' + #13 +
                               'Team' + #9 + '5';

  cmbxGroupType.Items.Text := 'Summary Detail' + #9 + '3' + #13 +
                              'Summary' + #9 + '1' + #13 +
                              'Detail' + #9 + '2';

  cmbxGroupSort.Items.Text := 'Group Code' + #9 + '1' + #13 +
                              'Group Name' + #9 + '2' + #13 +
                              'Group None' + #9 + '0';

  cmbxGroupDetailSort.Items.Text := 'Last Name' + #9 + '3' + #13 +
                                    'Social Number' + #9 + '1' + #13 +
                                    'Employee Code' + #9 + '2';

end;

procedure TrwWizardDBDTEdit.Initialize;
begin
  inherited;
  cmbxGroupLevel.Value := IntToStr(TrwWizInfDBDT(WizardInfo).GroupLevel);
  cmbxGroupType.Value := IntToStr(TrwWizInfDBDT(WizardInfo).GroupType);
  cmbxGroupSort.Value := IntToStr(TrwWizInfDBDT(WizardInfo).GroupSort);
  cmbxGroupDetailSort.Value := IntToStr(TrwWizInfDBDT(WizardInfo).GroupDetailSort);

  chbEEHeader.Checked := TrwWizInfDBDT(WizardInfo).PrintEEHeader;
  chbEEFooter.Checked := TrwWizInfDBDT(WizardInfo).PrintEEFooter;
end;

initialization
  RegisterClass(TrwWizInfDBDT);
  WizardsLst.Add(TrwWizInfDBDT);

finalization
  UnRegisterClass(TrwWizInfDBDT);
  WizardsLst.Remove(TrwWizInfDBDT);

end.
