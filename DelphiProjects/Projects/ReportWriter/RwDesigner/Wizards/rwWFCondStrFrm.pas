// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwWFCondStrFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwWFCondFrm, StdCtrls, ExtCtrls;

type
  TrwWFCondStr = class(TrwWFCond)
    edPattern: TEdit;
  private
  public
    procedure PutCond; override;
    procedure GetCond; override;
    procedure AfterConstruction; override;
  end;


implementation

{$R *.DFM}

{ TrwWFCond1 }

procedure TrwWFCondStr.AfterConstruction;
begin
  inherited;
  cbOper.ItemIndex := cbOper.Items.Add('Like');
end;

procedure TrwWFCondStr.GetCond;
begin
  Condition.Value1 := edPattern.Text;
  Condition.Operation := cbOper.Text;
  Condition.DisplayExpression := Condition.Field.DisplayName+' '+Condition.Operation+
    ' '+'"'+Condition.Value1+'"';
end;

procedure TrwWFCondStr.PutCond;
begin
  edPattern.Text := Condition.Value1;
end;

end.
