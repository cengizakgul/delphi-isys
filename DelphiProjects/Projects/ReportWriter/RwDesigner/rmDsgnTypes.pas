unit rmDsgnTypes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, rmTypes, Forms, Menus,
  rwEngineTypes, rwConnectionInt;


// messages
const
      WM_DESIGNER_PROCESS_POSTED_MESSAGES = WM_USER + 100;

      mModalDialogOK = WM_USER + 100 + 1;
      mModalDialogCancel = WM_USER + 100 + 2;

      mFunctionTextChanged = WM_USER + 100 + 3;
      mFunctionsChanged =  WM_USER + 100 + 4;
      mVariablesChanged =  WM_USER + 100 + 5;
      mComponentsChanged = WM_USER + 100 + 6;
      mComponentClassNameChanged = WM_USER + 100 + 7;
      mComponentIconChanged = WM_USER + 100 + 8;
      mComponentPaletteInfoChanged = WM_USER + 100 + 9;
      mRefreshFunctions = WM_USER + 100 + 10;
      mRefreshComponents = WM_USER + 100 + 11;
      mRefreshVariables = WM_USER + 100 + 12;
      mComponentCreationStarted = WM_USER + 100 + 13;
      mComponentCreationFinished = WM_USER + 100 + 14;
      mSelectedObjectListChanged = WM_USER + 100 + 14;
      mDesignContextChanged = WM_USER + 100 + 17;
      mApartmentCreated = WM_USER + 100 + 18;
      mApartmentDestroyed = WM_USER + 100 + 19;
      mNewReportCreating = WM_USER + 100 + 20;
      mDockingStateChanged = WM_USER + 100 + 21;
      mObjectChildrenListChanged = WM_USER + 100 + 22;
      mObjectPropertyChanged = WM_USER + 100 + 33;
      mRunningStopped = WM_USER + 100 + 34;
      mRunningPaused = WM_USER + 100 + 35;
      mRunningProceeded = WM_USER + 100 + 36;
      mApartmentActivated = WM_USER + 100 + 37;
      mApartmentDeactivated = WM_USER + 100 + 38;
      mDesignObjectChanged = WM_USER + 100 + 39;
      mDoApplyPendingChanges = WM_USER + 100 + 40;
      mObjectsWereDeleted = WM_USER + 100 + 41;
      mBookmarkObject = WM_USER + 100 + 42;
      mApartmentDestroying = WM_USER + 100 + 43;


      mtbOpen = WM_USER + 300 + 20;
      mtbSave = WM_USER + 300 + 21;
      mtbNew = WM_USER + 300 + 22;
      mtbClose = WM_USER + 300 + 23;
      mtbOpenLibrary = WM_USER + 300 + 24;
      mtbCompile = WM_USER + 300 + 25;
      mtbSaveChanges = WM_USER + 300 + 26;
      mtbReload = WM_USER + 300 + 27;
      mtbDelete = WM_USER + 300 + 28;
      mtbCopy = WM_USER + 300 + 29;
      mtbPaste = WM_USER + 300 + 30;
      mtbCut = WM_USER + 300 + 31;
      mtbChangeObjectProperty = WM_USER + 300 + 32;
      mtbShowPropertyEditor = WM_USER + 300 + 33;
      mtbZoomChange = WM_USER + 300 + 34;
      mtbShowQueryBuilder = WM_USER + 300 + 35;
      mtbShowFormulaEditor = WM_USER + 300 + 36;
      mtbShowAggrFunctEditor = WM_USER + 300 + 37;
      mtbSetDefaultParams = WM_USER + 300 + 38;
      mtbSetParamFileName = WM_USER + 300 + 39;
      mtbShowObjectsTree = WM_USER + 300 + 40;
      mtbShowEventsEditor = WM_USER + 300 + 41;
      mtbShowObjectInspector = WM_USER + 300 + 42;
      mtbRun = WM_USER + 300 + 43;
      mtbStop = WM_USER + 300 + 44;
      mtbPause = WM_USER + 300 + 45;
      mtbTraceIn = WM_USER + 300 + 46;
      mtbTraceOver = WM_USER + 300 + 47;
      mtbBreakPointToggle = WM_USER + 300 + 48;
      mtbShowCallStack = WM_USER + 300 + 49;
      mtbShowWatches = WM_USER + 300 + 50;
      mtbShowSourceCodeByAddress = WM_USER + 300 + 51;
      mtbAlignPositions = WM_USER + 300 + 52;
      mtbShowBookmarks = WM_USER + 300 + 53;
      mtbShowAllEventsCode = WM_USER + 300 + 54;
      mtbShowAllFormulasCode = WM_USER + 300 + 55;

      cContextNameFunctions = 'Functions';
      cContextNameVariables = 'Variables';
      cContextNameComponents = 'Components';

      cProcessIsNotAccessible = 'Process is not accessible';

type
  TrmResizerState = (maNone, maSelected, maResize, maMove);
  TrmDsgnAreaType = (rmDATNone, rmDATNotVisualComp, rmDATPrintForm, rmDATInputForm, rmDATASCIIForm, rmDATXMLForm, rmDATInternalObject);


  TrmObjectID = Cardinal;
  TrmDsgnMessageID = Word;

  TrmMessageRec = record
    MessageID: TrmDsgnMessageID;
    SenderID:  TrmObjectID;
    Parameter1: Variant;
    Parameter2: Variant;
  end;

  IrmDsgnApartment = interface;

  IrmMessageReceiver = interface(IrmInterfacedObject)
  ['{8E40F2C8-68F1-4282-B5EC-79C8E309B0B8}']
    function   Apartment: IrmDsgnApartment;
    procedure  GetListeningMessages(const AMessages: TList);
  end;


  IrmDesignerEventManager = interface
  ['{50E6A589-3AFE-4594-BDD8-E32C26BB8F5A}']
    procedure SetOwnerWndHandle(const AWndHandle: HWND);
    procedure RegisterObject(const AObject: IrmMessageReceiver);
    procedure UnregisterObject(const AObject: IrmMessageReceiver);
    procedure SendBroadcastMessage(var Message: TrmMessageRec);
    procedure PostBroadcastMessage(var Message: TrmMessageRec);
    procedure ProcessMessages;
    function  QueueIsEmpty: Boolean;
  end;


  IrmDsgnActionHandler = interface(IrmInterfacedObject)
  ['{678DB118-FC13-4B9A-879D-53FDFE30A90D}']
    function ObjectMouseDown(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean;
    function ObjectMouseUp(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean;
    function ObjectMouseDblClick(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean;
    function ObjectMouseMove(const AMousePos: TPoint; AObject: IrmDesignObject): Boolean;
    function ObjectSetCursor(AObject: IrmDesignObject): Boolean;
  end;


  IrmSettingsHolder = interface
  ['{55C659FE-50BB-445D-9935-27053AF72FA2}']
    procedure   SaveData(const APath: String; const AValue: Variant);
    function    ReadData(const APath: String; const ADefaultValue: Variant): Variant;
  end;


  IrmCustomFrame = interface
  ['{1B7D0CFE-7606-44C0-84ED-CE26FF8E1CB7}']
    function  RealObject: TFrame;
  end;


  IrmDsgnApartment  = interface(IrmCustomFrame)
  ['{CDB994F4-71AF-4D73-884D-9E02D2F140A6}']
    function  EventManager: IrmDesignerEventManager;
    procedure Activate;
    procedure Deactivate;
    function  IsActive: Boolean;
    function  GetCaption: String;
    function  Settings: IrmSettingsHolder;
  end;


  TrmdSecurityRec = record
    EditComponents: Boolean;
    EditFunctions: Boolean;
    EditVariables: Boolean;
  end;

  
  IrmDsgnComponent = interface(IrmDsgnApartment)
  ['{AE9B3F26-30DF-479E-A738-967A27066B78}']
    function  DesigningObject: IrmDesignObject;
    procedure NewDesignComponent(const AClassName: String);
    procedure Notify(const AEventType: TrmDesignEventType; const AParam: Variant);
    function  CreateRWObject(const AClassName: String; AParent: IrmDesignObject; APos: TPoint): IrmDesignObject;
    function  GroupSelection: Boolean;
    procedure ApplyResizerChanges(dLeft, dTop, dWidth, dHeight: Integer);
    procedure ApplyGroupResize(dLeft, dTop, dWidth, dHeight: Integer);
    procedure ApplyResizerState(AState: TrmResizerState);
    function  GetZoomFactor: Extended;
    function  GetObjectCanvasDPI: Integer;
    function  GetSelectedObjects: TrmListOfObjects;
    procedure SelectObject(AObject: IrmDesignObject);
    function  UnSelectObject(AObject: IrmDesignObject): Boolean;
    procedure UnselectAll;
    function  IsDragAndDropMode: Boolean;
    procedure LoadComponentFromStream(const AStream: TStream);
    procedure SaveComponentToStream(const AStream: TStream);
    function  GetSecurityInfo: TrmdSecurityRec;    
    function  InputFormDesigninig: Boolean;
    function  PrintFormDesigninig: Boolean;
    function  ASCIIFormDesigninig: Boolean;
    function  XMLFormDesigninig:   Boolean;
    function  LibComponentDesigninig: Boolean;
    function  ReadOnlyMode: Boolean;
    procedure ChangeDesignContext(const ANewContextName: String);
  end;


  TrmDebugLineInfo = record
    Segment:            TrwVMCodeSegment;
    FirstAddress:       Integer;
    LastAddress:        Integer;
    ClassName:          String;
    ObjectLocation:     String;
    FunctionName:       String;
    SourceCodeLineNbr:  Integer;
    SourceCodeLineTxt:  String;
  end;

  PTrmDebugLineInfo = ^TrmDebugLineInfo;

  TrmDebugStopLineInfo = record
    Address:            TrwVMAddress;
    rmObject:           IrmDesignObject;
    SorceCodeLineInfo:  PTrmDebugLineInfo;
    DebugSymbolInfo:    String;
  end;

  TrmDebugStopLineList = array of TrmDebugStopLineInfo;
  PTrmDebugStopLineList = ^TrmDebugStopLineList;

  TrmBreakPointInfo = record
    ObjectLocation:     String;
    FunctionName:       String;
    SourceCodeLineNbr:  Integer;
    Address:            TrwVMAddress;
    SorceCodeLineInfo:  PTrmDebugLineInfo;
  end;

  PTrmBreakPointInfo = ^TrmBreakPointInfo;

  IrmBreakPoints = interface
  ['{A707B60C-B89C-486B-832E-A853E4CB91A6}']
    procedure   ToggleBreakPoint(const AObjectLocation, AFunctionName: String; ASourceCodeLineNbr: Integer);
    function    Count: Integer;
    function    BreakPointByIndex(const AIndex: Integer): TrmBreakPointInfo;
  end;

  TrmWatchInfo = record
    Expression: String;
    Result:     TrwVMExpressionResult;
  end;

  PTrmWatchInfo = ^TrmWatchInfo;


  IrmWatchList  = interface
  ['{97C734F0-F3FA-446D-971F-A48E26311F83}']
    function    Add(const AExpression: String): Integer;
    procedure   Delete(const AIndex: Integer);
    procedure   Update(const AIndex: Integer; const AExpression: String);
    function    Count: Integer;
    function    WatchByIndex(const AIndex: Integer): TrmWatchInfo;
    procedure   Recalculate(const AIndex: Integer = -1);
  end;


  IrmDsgnReport = interface (IrmDsgnComponent)
  ['{AFA01B3C-8B59-4160-8E8C-9EA5AC05FEF2}']
    procedure DesignReport(const AData: TStream);
    function  ReportStopLineInfo: TrmDebugStopLineInfo;
    function  ReportCallStack: PTrmDebugStopLineList;
    function  GetBreakPointList: IrmBreakPoints;
    function  GetWatchList: IrmWatchList;
    procedure SetReportFileName (AReportName : String);
  end;

  
  IrmDsgnLibrary = interface (IrmDsgnComponent)
  ['{BF12E75F-BC8B-4134-B0AE-CE8222B1C5E0}']
    function IsLibraryModified: Boolean;
  end;


  TrmdApartmentList = array of IrmDsgnApartment;

  IrmDesignerGUI = interface (IrmInterfacedObject)
  ['{BC705F8D-8520-42AE-8A71-EB2BEE23701C}']
    function  GetComponentDesigner: IrmDsgnComponent;
    function  GetActiveApartment: IrmDsgnApartment;
    function  CreateApartment(ApartmentClass: TComponentClass): IrmDsgnApartment;
    procedure ShowApartment(Apartment: IrmDsgnApartment);
    function  ShowModalApartment(Apartment: IrmDsgnApartment): TModalResult;
    procedure DestroyCurrentApartment;
    function  GetApartmentList: TrmdApartmentList;
    function  EventManager: IrmDesignerEventManager;
    function  CurrentApartment: IrmDsgnApartment;
  end;

implementation

end.
