unit rmToolBarFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, ToolWin, StdCtrls, ExtCtrls, ImgList, rmTypes,
  rmDsgnTypes, ActnList, rwPrinters, rwGraphics, rwUtils, rwDsgnUtils,
  rwDsgnRes, rwDebugInfo, ColorPicker, rmFMLExprPanelFrm, rmFormula, rwBasicClasses,
  Mask, wwdbedit, Wwdbspin, ISBasicClasses, rwDesignClasses, rwEngine, rmCustomFrameFrm,
  rmCustomTBGroupFrm;

type
  TrmComponentToolButton = class(TToolButton)
  end;

  TrmToolBar = class(TrmCustomFrame, IrmToolBar)
    MainMenu: TMainMenu;
    miFile: TMenuItem;
    miExit: TMenuItem;
    View1: TMenuItem;
    miAdvancedMode: TMenuItem;
    miWindows: TMenuItem;
    ctrbToolBar: TControlBar;
    ToolBar1: TToolBar;
    procedure miExitClick(Sender: TObject);
    procedure miAdvancedModeClick(Sender: TObject);
  private
    procedure UpdateCheckMenuItems;
    procedure AddReportDesignerToMenu(AReportDesigner: IrmDsgnComponent);
    procedure RemoveReportDesignerFromMenu(AReportDesigner: IrmDsgnComponent);
    procedure OnSwitchReportDesigner(Sender: TObject);

    // Interface implementation
    procedure Notify(const AEventType: TrmDesignEventType; const AParam: Variant);
    function  GetVCLContainer: TWinControl;
    function  GetMainMenu: TMainMenu;

  public
   procedure AfterConstruction; override;
  end;

implementation

uses TypInfo, Math;

{$R *.dfm}

const
  cDesignWindowMIName = 'miDesignWindow';

{ TrmToolBar }

procedure TrmToolBar.miExitClick(Sender: TObject);
begin
  RMDesigner.Notify(rmdTlbActExit, 0);
end;

procedure TrmToolBar.Notify(const AEventType: TrmDesignEventType; const AParam: Variant);
begin
{  case AEventType of
    rmdComponentDesignerOpen:
      AddReportDesignerToMenu(IrmDsgnComponent(Pointer(Integer(AParam))));

    rmdComponentDesignerClose:
      RemoveReportDesignerFromMenu(IrmDsgnComponent(Pointer(Integer(AParam))));
  end;}
end;

procedure TrmToolBar.AddReportDesignerToMenu(AReportDesigner: IrmDsgnComponent);
var
  MI: TMenuItem;
  i: Integer;
  fl: Boolean;

  function GetUniqueName: String;
  var
    i: Integer;
  begin
    for i := 1 to 100 do
    begin
      Result := cDesignWindowMIName + IntToStr(i);
      if FindComponent(Result) = nil then
        break;
    end;
  end;

begin
  fl := False;
  for i := 0 to miWindows.Count - 1 do
    if miWindows.Items[i].Tag = Integer(Pointer(AReportDesigner)) then
    begin
      miWindows.Items[i].Checked := True;
      fl := True;
      break;
    end;

  if not fl then
  begin
    MI := TMenuItem.Create(Self);
    MI.Name := GetUniqueName;
    MI.Caption := 'Report';
    MI.Tag := Integer(Pointer(AReportDesigner));
    MI.RadioItem := True;
    MI.OnClick := OnSwitchReportDesigner;
    miWindows.Insert(0, MI);
    MI.Checked := True;
  end;
end;

procedure TrmToolBar.RemoveReportDesignerFromMenu(AReportDesigner: IrmDsgnComponent);
var
  i: Integer;
begin
  for i := 0 to miWindows.Count - 1 do
    if miWindows.Items[i].Tag = Integer(Pointer(AReportDesigner)) then
      miWindows.Items[i].Free;
end;

procedure TrmToolBar.OnSwitchReportDesigner(Sender: TObject);
var
  p: Variant;
begin
{###  if Sender = miLibrary then
    p := 0
  else
    p := TMenuItem(Sender).Tag;

  RMDesigner.Notify(rmdComponentDesignerOpen, p);
  TMenuItem(Sender).Checked := True;}
end;

procedure TrmToolBar.AfterConstruction;
begin
  inherited;
  UpdateCheckMenuItems;
end;

function TrmToolBar.GetVCLContainer: TWinControl;
begin
  Result := ctrbToolBar;
end;

procedure TrmToolBar.miAdvancedModeClick(Sender: TObject);
begin
  DesignRMAdvancedMode := not DesignRMAdvancedMode;
  UpdateCheckMenuItems;
  RMDesigner.Notify(rmdTlbActChangeAdvancedMode, 0);
end;

procedure TrmToolBar.UpdateCheckMenuItems;
begin
  miAdvancedMode.Checked  := DesignRMAdvancedMode;
end;

function TrmToolBar.GetMainMenu: TMainMenu;
begin
  Result := MainMenu;
end;

end.
