object rwStatistics: TrwStatistics
  Left = 359
  Top = 114
  Width = 600
  Height = 574
  BorderStyle = bsSizeToolWin
  Caption = 'Report Statistics'
  Color = clBtnFace
  Constraints.MinHeight = 530
  Constraints.MinWidth = 520
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 592
    Height = 547
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Histogram'
      object Chart: TChart
        Left = 0
        Top = 0
        Width = 584
        Height = 483
        AllowPanning = pmNone
        AllowZoom = False
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        BackWall.Pen.Visible = False
        Title.Text.Strings = (
          '')
        AxisVisible = False
        ClipPoints = False
        Frame.Visible = False
        Legend.Alignment = laBottom
        View3DOptions.Elevation = 315
        View3DOptions.Orthogonal = False
        View3DOptions.Perspective = 0
        View3DOptions.Rotation = 360
        View3DWalls = False
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        AutoSize = True
        object Series1: TPieSeries
          Marks.ArrowLength = 8
          Marks.Style = smsLabelPercent
          Marks.Visible = True
          SeriesColor = clRed
          Title = 'Report Statistics'
          ValueFormat = '#,##0'
          CustomXRadius = 135
          CustomYRadius = 165
          OtherSlice.Text = 'Other'
          PieValues.DateTime = False
          PieValues.Name = 'Pie'
          PieValues.Multiplier = 1.000000000000000000
          PieValues.Order = loNone
          RotationAngle = 45
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 483
        Width = 584
        Height = 36
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object lTotalTime: TLabel
          Left = 2
          Top = 1
          Width = 88
          Height = 13
          Caption = 'Total Time(ms):'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lTotal: TLabel
          Left = 103
          Top = 1
          Width = 26
          Height = 13
          Caption = 'lTotal'
        end
        object Label1: TLabel
          Left = 330
          Top = 20
          Width = 130
          Height = 13
          Caption = 'Cached Data Size(Kb):'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lDataSize: TLabel
          Left = 461
          Top = 20
          Width = 32
          Height = 13
          Caption = 'Label2'
        end
        object Label3: TLabel
          Left = 2
          Top = 20
          Width = 99
          Height = 13
          Caption = 'Physical Queries:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lRQ: TLabel
          Left = 103
          Top = 20
          Width = 18
          Height = 13
          Caption = 'lRQ'
        end
        object Label4: TLabel
          Left = 167
          Top = 20
          Width = 93
          Height = 13
          Caption = 'Logical Queries:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lLQ: TLabel
          Left = 262
          Top = 20
          Width = 16
          Height = 13
          Caption = 'lLQ'
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Data'
      ImageIndex = 1
      DesignSize = (
        584
        519)
      object evDBGrid1: TISRWDBGrid
        Left = 0
        Top = 33
        Width = 584
        Height = 486
        Selected.Strings = (
          'DateTimeStamp'#9'18'#9'Date'
          'ReportName'#9'32'#9'Report'
          'TotalTime'#9'8'#9'Total (ms)'
          'RemoteQueriesTime'#9'10'#9'Physical Queries (ms)'
          'CacheDataTime'#9'10'#9'Caching Data (ms)'
          'LogicalQueriesTime'#9'10'#9'Logical Queries (ms)'
          'CacheDataSize'#9'10'#9'Caching Data Size (Kb)'
          'RemoteConnections'#9'10'#9'DB Connections'
          'RemoteQueries'#9'10'#9'Remote Queries'
          'LogicalQueries'#9'10'#9'Logical Queries')
        IniAttributes.Enabled = True
        IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
        IniAttributes.SectionName = 'TrwStatistics\evDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataSource = evDataSource1
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
    end
  end
  object Panel2: TPanel
    Left = 3
    Top = 24
    Width = 133
    Height = 33
    BevelOuter = bvNone
    TabOrder = 1
    object sbOpenLog: TSpeedButton
      Left = 3
      Top = 3
      Width = 25
      Height = 25
      Hint = 'Open Logfile'
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00CCCCCCCCCCCC
        CCCC00000000000CCCCC003333333330CCCC0B03333333330CCC0FB033333333
        30CC0BFB03333333330C0FBFB000000000000BFBFBFBFB0CCCCC0FBFBFBFBF0C
        CCCC0BFB0000000CCCCCC000CCCCCCCC000CCCCCCCCCCCCCC00CCCCCCCCC0CCC
        0C0CCCCCCCCCC000CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC}
      OnClick = sbOpenLogClick
    end
    object DBNavigator1: TDBNavigator
      Left = 28
      Top = 3
      Width = 100
      Height = 25
      DataSource = evDataSource1
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      Flat = True
      TabOrder = 0
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = '.txt'
    FileName = 'rwStatLog.txt'
    Title = 'Load Logfile'
    Left = 48
    Top = 96
  end
  object evDataSource1: TDataSource
    AutoEdit = False
    DataSet = dsStat
    OnDataChange = evDataSource1DataChange
    Left = 260
    Top = 304
  end
  object dsStat: TISRWClientDataSet
    FieldDefs = <
      item
        Name = 'DateTimeStamp'
        DataType = ftDateTime
      end
      item
        Name = 'ReportName'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'TotalTime'
        DataType = ftInteger
      end
      item
        Name = 'RemoteQueriesTime'
        DataType = ftInteger
      end
      item
        Name = 'CacheDataTime'
        DataType = ftInteger
      end
      item
        Name = 'CacheDataSize'
        DataType = ftInteger
      end
      item
        Name = 'LogicalQueriesTime'
        DataType = ftInteger
      end
      item
        Name = 'RemoteQueries'
        DataType = ftInteger
      end
      item
        Name = 'LogicalQueries'
        DataType = ftInteger
      end>
    Left = 220
    Top = 304
    object dsStatDateTimeStamp: TDateTimeField
      DisplayLabel = 'Date'
      DisplayWidth = 18
      FieldName = 'DateTimeStamp'
    end
    object dsStatReportName: TStringField
      DisplayLabel = 'Report'
      DisplayWidth = 32
      FieldName = 'ReportName'
      Size = 255
    end
    object dsStatTotalTime: TIntegerField
      DisplayLabel = 'Total (ms)'
      DisplayWidth = 8
      FieldName = 'TotalTime'
    end
    object dsStatRemoteQueriesTime: TIntegerField
      DisplayLabel = 'Physical Queries (ms)'
      DisplayWidth = 10
      FieldName = 'RemoteQueriesTime'
    end
    object dsStatCacheDataTime: TIntegerField
      DisplayLabel = 'Caching Data (ms)'
      DisplayWidth = 10
      FieldName = 'CacheDataTime'
    end
    object dsStatLogicalQueriesTime: TIntegerField
      DisplayLabel = 'Logical Queries (ms)'
      DisplayWidth = 10
      FieldName = 'LogicalQueriesTime'
    end
    object dsStatCacheDataSize: TIntegerField
      DisplayLabel = 'Caching Data Size (Kb)'
      DisplayWidth = 10
      FieldName = 'CacheDataSize'
    end
    object dsStatRemoteQueries: TIntegerField
      DisplayLabel = 'Remote Queries'
      DisplayWidth = 10
      FieldName = 'RemoteQueries'
    end
    object dsStatLogicalQueries: TIntegerField
      DisplayLabel = 'Logical Queries'
      DisplayWidth = 10
      FieldName = 'LogicalQueries'
    end
  end
end
