inherited rmTrmXMLSimpleTypeByRestrictionProp: TrmTrmXMLSimpleTypeByRestrictionProp
  Width = 334
  Height = 380
  inherited pcCategories: TPageControl
    Width = 334
    Height = 380
    inherited tsBasic: TTabSheet
      object grbValue: TGroupBox
        Left = 8
        Top = 78
        Width = 311
        Height = 188
        Caption = 'Field Content'
        TabOrder = 2
        inline frmValue: TrmOPEValue
          Left = 10
          Top = 20
          Width = 292
          Height = 157
          AutoScroll = False
          TabOrder = 0
          inherited pcTypes: TPageControl
            Width = 292
            Height = 157
            inherited tsEmpty: TTabSheet
              inherited frmEmpty: TrmOPEEmpty
                Width = 284
                Height = 126
                inherited Label1: TLabel
                  Width = 284
                  Height = 126
                end
              end
            end
          end
        end
      end
    end
    inherited tcAppearance: TTabSheet
      Caption = 'Text Filters'
      TabVisible = True
      inline frmTextFilter: TrmOPETextFilter
        Left = 8
        Top = 10
        Width = 308
        Height = 335
        AutoScroll = False
        TabOrder = 0
        inherited Panel1: TPanel
          Width = 308
          inherited lHeader2: TLabel
            Width = 308
          end
          inherited lvFilters: TListView
            Width = 308
          end
        end
        inherited pnlParams: TPanel
          Width = 308
          Height = 153
        end
      end
    end
  end
end
