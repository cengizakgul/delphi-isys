unit rwUserPropEditFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwBasicClasses, TypInfo, rmTypes, rwTypes, rwMessages, rwUtils, rwBasicUtils,
  rwEngineTypes;

type
  TrwUserPropEdit = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    cbType: TComboBox;
    btnOK: TButton;
    btnCancel: TButton;
    edName: TEdit;
    chbDelegProp: TCheckBox;
    edChildProp: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure edNameExit(Sender: TObject);
    procedure chbDelegPropClick(Sender: TObject);
    procedure edChildPropExit(Sender: TObject);
    procedure edChildPropKeyPress(Sender: TObject; var Key: Char);
  private
    FComponent: TrwComponent;
    procedure DelegatedPropertyInfo(var AObject: TObject; AProp: PTPropNamesArray);    
  public
  end;

  function AddUserProperty(AComponent: TrwComponent): Boolean;
  function DeleteUserProperty(AComponent: TrwComponent; APropName: String): Boolean;

implementation

{$R *.DFM}


function AddUserProperty(AComponent: TrwComponent): Boolean;
var
  Frm: TrwUserPropEdit;
  Prop: TrwUserProperty;
  F: TrwEvent;
begin
  Frm := TrwUserPropEdit.Create(nil);
  with Frm do
    try
      FComponent := AComponent;
      Result := ShowModal = mrOK;

      if Result then
      begin
        Prop := AComponent._UserProperties.Add;
        Prop.Name := edName.Text;
        Prop.PropType := TrwVarTypeKind(cbType.Items.Objects[cbType.ItemIndex]);
        if Prop.PropType = rwvPointer then
          Prop.PointerType := cbType.Items[cbType.ItemIndex]
        else
          Prop.PointerType := '';

        if not chbDelegProp.Checked then
        begin
          F := AComponent.GlobalVarFunc.EventsList[
            AComponent.GlobalVarFunc.EventsList.Add('Get' + Prop.Name, '', Prop.PropType,
            Prop.PointerType)];
          F.HandlersText := '';

          F := AComponent.GlobalVarFunc.EventsList[
            AComponent.GlobalVarFunc.EventsList.Add('Set' + Prop.Name, 'AValue: ' + Trim(cbType.Text),
            rwvUnknown, '')];
          F.HandlersText := '';
        end

        else
        begin
          Prop.DelegatedName := edChildProp.Text;

          F := AComponent.GlobalVarFunc.EventsList[
            AComponent.GlobalVarFunc.EventsList.Add('OnSet' + Prop.Name, '', rwvUnknown, '')];
          F.HandlersText := '';
        end;
      end;

    finally
      Free;
    end;
end;


function DeleteUserProperty(AComponent: TrwComponent; APropName: String): Boolean;
var
  i, j: Integer;
begin
  Result := False;

  i := AComponent._UserProperties.IndexOf(APropName);
  if i = -1 then
    Exit;

  if MessageDlg('You are about unregister user property "' + APropName + '". Proceed?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
    Exit;

  if AComponent._UserProperties[i].InheritedItem then
    raise ErwException.Create(ResErrInheritedUserPropDel);

  j := AComponent.GlobalVarFunc.EventsList.IndexOf('Set' + AComponent._UserProperties[i].Name);
  if j <> -1 then
    AComponent.GlobalVarFunc.EventsList.Delete(j);
  j := AComponent.GlobalVarFunc.EventsList.IndexOf('Get' + AComponent._UserProperties[i].Name);
  if j <> -1 then
    AComponent.GlobalVarFunc.EventsList.Delete(j);
  AComponent._UserProperties[i].Free;
  Result := True;
end;


procedure TrwUserPropEdit.FormCreate(Sender: TObject);
var
  i: Integer;
  PTypInf: PTypeInfo;
  PTypDat: PTypeData;
  h: String;
begin
  PTypInf := TypeInfo(TrwVarTypeKind);
  PTypDat := GetTypeData(PTypInf);

  for i := PTypDat^.MinValue to PTypDat^.MaxValue do
  begin
    if TrwVarTypeKind(i) in [rwvUnknown, rwvArray, rwvPointer, rwvVariant] then
      Continue;
    h := GetEnumName(PTypInf, i);
    Delete(h, 1, 3);
    cbType.Items.AddObject(h, Pointer(i));
  end;

  cbType.Items.AddObject('', Pointer(-1));

  for i := 0 to RWClassList.Count - 1 do
    cbType.Items.AddObject(TPersistentClass(RWClassList.Objects[i]).ClassName, Pointer(rwvPointer));

  cbType.Items.AddObject('', Pointer(-1));

  for i := 0 to SystemLibComponents.Count - 1 do
    cbType.Items.AddObject(SystemLibComponents[i].Name, Pointer(rwvPointer));
end;

procedure TrwUserPropEdit.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult = mrOk then
  begin
    CanClose := (cbType.ItemIndex <> -1) and (cbType.Items.Objects[cbType.ItemIndex] <> Pointer(-1));
    if FComponent._UserProperties.IndexOf(edName.Text) <> -1 then
    begin
      CanClose := False;
      raise ErwException.CreateFmt(ResErrPropertyExists, [edName.Text]);
    end;
  end;
end;

procedure TrwUserPropEdit.edNameExit(Sender: TObject);
begin
  edName.Text := Trim(edName.Text);
end;

procedure TrwUserPropEdit.chbDelegPropClick(Sender: TObject);
begin
  if chbDelegProp.Checked then
  begin
    edChildProp.Text := '';
    edChildProp.Enabled := True;
    cbType.Enabled := False;
    cbType.ItemIndex := -1;
  end

  else
  begin
    edChildProp.Text := '';
    edChildProp.Enabled := False;
    cbType.Enabled := True;
    cbType.ItemIndex := -1;
  end;
end;

procedure TrwUserPropEdit.edChildPropExit(Sender: TObject);
var
  Obj: TObject;
  P: TPropNamesArray;
  Prop: PPropInfo;
  h: String;
begin
  h := 'Unknown';
  edChildProp.Text := Trim(edChildProp.Text);
  DelegatedPropertyInfo(Obj, @P);
  Prop := GetComponentPropertyInfo(Obj, P);
  if Assigned(Prop) and Assigned(Obj) then
    case Prop^.PropType^.Kind of
      tkClass:
        begin
          Obj := Pointer(GetOrdProp(Obj, Prop));
          if Obj is TrwComponent then
            h := TrwComponent(Obj).PClassName
          else
            h := Obj.ClassName;
        end;

      tkInteger,
      tkEnumeration,
      tkSet:
        if Prop^.PropType^.Name = 'Boolean' then
          h := 'Boolean'        
        else
          h := 'Integer';

      tkFloat:
        h := 'Float';

      tkString,
      tkLString,
      tkWString:
        h := 'String';

      tkVariant:
        h := 'Variant';
    end;

  cbType.ItemIndex := cbType.Items.IndexOf(h);
end;


procedure TrwUserPropEdit.DelegatedPropertyInfo(var AObject: TObject; AProp: PTPropNamesArray);
var
  h1, h2: String;
  i: Integer;
begin
  h1 := edChildProp.Text;
  h2 := GetNextStrValue(h1, '.');
  AObject := FComponent.FindComponent(h2);

  SetLength(AProp^, 10);  // max hierarchy depth of the delegated property
  i := 0;
  while h1 <> '' do
  begin
    AProp^[i] := GetNextStrValue(h1, '.');
    Inc(i);
  end;
  SetLength(AProp^, i);
end;

procedure TrwUserPropEdit.edChildPropKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    edChildProp.OnExit(nil);
end;

end.
