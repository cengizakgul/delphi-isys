unit rwLibCompChoiceFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwBasicClasses, ComCtrls, rwCommonClasses, rwUtils,
  rwReport, rwDesignClasses, rwDsgnUtils, rmTypes;

type
  TrwChoiceLibCompType = (rctReportComp, rctInputFormComp, rctNoVisualComp, rctReportAncestor);
  TrwChoiceLibCompTypes = set of TrwChoiceLibCompType;


  TrwLibCompChoice = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    memDescript: TMemo;
    btnOK: TButton;
    btnCancel: TButton;
    tvLibComp: TTreeView;
    procedure tvLibCompChange(Sender: TObject; Node: TTreeNode);
    procedure tvLibCompDblClick(Sender: TObject);
    procedure tvLibCompCompare(Sender: TObject; Node1, Node2: TTreeNode;
      Data: Integer; var Compare: Integer);
  private
    FChoiceType: TrwChoiceLibCompTypes;
    procedure Initialize;
    function  AddComponentToTree(AComp: TrwLibComponent): TTreeNode;
    function  ClassAccepted(AClass: TrwComponentClass): Boolean;
  public
  end;


  function ChoiceLibComp(AType: TrwChoiceLibCompTypes): TrwLibComponent;

implementation

{$R *.DFM}

uses rwDesignerFrm;


function ChoiceLibComp(AType: TrwChoiceLibCompTypes): TrwLibComponent;
var
  Frm: TrwLibCompChoice;
begin
  Result := nil;
  Frm := TrwLibCompChoice.Create(nil);
  with Frm do
    try
      FChoiceType := AType;
      Initialize;
      if ShowModal = mrOK then
        if Assigned(tvLibComp.Selected) and (TObject(tvLibComp.Selected.Data) is TrwLibComponent) then
          Result := TrwLibComponent(tvLibComp.Selected.Data);
    finally
      Frm.Free;
    end;
end;


function TrwLibCompChoice.AddComponentToTree(AComp: TrwLibComponent): TTreeNode;
var
  N: TTreeNode;
  i: Integer;
begin
  Result := nil;
  N := nil;
  if Assigned(GetClass(AComp.ParentName)) then
  begin
    for i := 0 to tvLibComp.Items[0].Count-1 do
      if TToolButton(tvLibComp.Items[0].Item[i].Data).Caption = AComp.ParentName then
      begin
        N := tvLibComp.Items[0].Item[i];
        break;
      end;

    if not Assigned(N) then
      for i := 0 to ReportDesigner.ReportFormToolsBar.tlbComponents.ButtonCount - 1 do
        if ReportDesigner.ReportFormToolsBar.tlbComponents.Buttons[i].Caption = AComp.ParentName then
        begin
          N := tvLibComp.Items.AddChildObject(tvLibComp.Items[0],
            ReportDesigner.ReportFormToolsBar.tlbComponents.Buttons[i].Hint, ReportDesigner.ReportFormToolsBar.tlbComponents.Buttons[i]);
          N.ImageIndex := ReportDesigner.ReportFormToolsBar.tlbComponents.Buttons[i].ImageIndex;
          N.SelectedIndex := N.ImageIndex;
          N.StateIndex := N.ImageIndex;
          break;
        end;
  end

  else
  begin
    for i := 0 to tvLibComp.Items.Count - 1 do
      if (TObject(tvLibComp.Items[i].Data) is TrwLibComponent) and
         (AComp.ParentName = TrwLibComponent(tvLibComp.Items[i].Data).Name)  then
      begin
        N := tvLibComp.Items[i];
        break;
      end;
  end;

  if Assigned(N) then
  begin
    AComp.DisplayName := AComp.Name;
    Result := tvLibComp.Items.AddChildObject(N, AComp.DisplayName, AComp);
    Result.ImageIndex := N.ImageIndex;
    Result.SelectedIndex := N.ImageIndex;
    Result.StateIndex := N.ImageIndex;
  end;
end;


function TrwLibCompChoice.ClassAccepted(AClass: TrwComponentClass): Boolean;
begin
  Result := False;
  if (rctReportComp in FChoiceType) and AClass.InheritsFrom(TrwPrintableControl) then
    Result := Result or True;

  if (rctInputFormComp in FChoiceType) and AClass.InheritsFrom(TrwFormControl) then
    Result := Result or True;

  if (rctReportAncestor in FChoiceType) and AClass.InheritsFrom(TrwReport) then
    Result := Result or True;

  if (rctNoVisualComp in FChoiceType) and AClass.InheritsFrom(TrwNoVisualComponent) and
     not AClass.InheritsFrom(TrwReport) then
    Result := Result or True;
end;

var Frm: TrwLibCompChoice;

procedure OnAddCategoryNode(ANewNode: TTreeNode; var Accept: Boolean);
var
  lc: TrwLibComponent;
  C: TrwComponentClass;
  i: Integer;
begin
  if not Assigned(ANewNode.Data) then
    ANewNode.ImageIndex := 57

  else
  begin
    ANewNode.Data := TrwLibCollectionItem(IrmLibCollectionItem(ANewNode.Data).RealObject);
    lc := TrwLibComponent(ANewNode.Data);
    C := lc.GetBaseClass;
    if Frm.ClassAccepted(C) then
    begin
      ANewNode.Text := lc.DisplayName;
      for i := 0 to ReportDesigner.ReportFormToolsBar.tlbComponents.ButtonCount - 1 do
        if ReportDesigner.ReportFormToolsBar.tlbComponents.Buttons[i].Caption = C.ClassName then
        begin
          ANewNode.ImageIndex := ReportDesigner.ReportFormToolsBar.tlbComponents.Buttons[i].ImageIndex;
          break;
        end;
    end
    else
      ANewNode.Data := nil;
  end;

  ANewNode.SelectedIndex := ANewNode.ImageIndex;
  ANewNode.StateIndex := ANewNode.ImageIndex;
end;

procedure TrwLibCompChoice.Initialize;

  procedure InheritageData;
  var
    N: TTreeNode;
    i: Integer;
  begin
    N := tvLibComp.Items.AddChildObject(nil, 'Component Library', nil);
    N.ImageIndex := 46;
    N.SelectedIndex := N.ImageIndex;
    N.StateIndex := N.ImageIndex;

    for i := 0 to SystemLibComponents.Count -1 do
      if ClassAccepted(SystemLibComponents[i].GetBaseClass) then
        AddComponentToTree(SystemLibComponents[i]);
  end;

  procedure CategoryData;
  var
    N: TTreeNode;
    i: Integer;
  begin
    Frm := Self;
    tvLibComp.Items.BeginUpdate;
    tvLibComp.Items.Clear;
    tvLibComp.SortType := stNone;

    N := tvLibComp.Items.AddChildObject(nil, 'Categories', nil);
    N.ImageIndex := 35;
    N.SelectedIndex := N.ImageIndex;
    N.StateIndex := N.ImageIndex;

    FillUpTreeViewCollection(SystemLibComponents, tvLibComp, N, @OnAddCategoryNode);

    for i := tvLibComp.Items.Count -1 downto 0 do
      if not Assigned(tvLibComp.Items[i].Data) and not tvLibComp.Items[i].HasChildren then
        tvLibComp.Items[i].Free;

    tvLibComp.SortType := stText;
    tvLibComp.Items.EndUpdate;
  end;

begin
  tvLibComp.Items.BeginUpdate;
  tvLibComp.SortType := stNone;
  tvLibComp.Items.Clear;

  CategoryData;
  if tvLibComp.Items.Count = 0 then
    InheritageData;

  tvLibComp.SortType := stText;
  if tvLibComp.Items.Count > 0 then
    tvLibComp.Items[0].Expand(False);

  tvLibComp.Items.EndUpdate;
end;


procedure TrwLibCompChoice.tvLibCompChange(Sender: TObject; Node: TTreeNode);
begin
  if Assigned(Node) and (TObject(Node.Data) is TrwLibComponent) then
    memDescript.Text := TrwLibComponent(Node.Data).Description
  else
    memDescript.Text := '';
end;


procedure TrwLibCompChoice.tvLibCompDblClick(Sender: TObject);
begin
  if Assigned(tvLibComp.Selected) and (TObject(tvLibComp.Selected.Data) is TrwLibComponent) then
    btnOK.Click;
end;

procedure TrwLibCompChoice.tvLibCompCompare(Sender: TObject; Node1,
  Node2: TTreeNode; Data: Integer; var Compare: Integer);
begin
  if not Assigned(Node1.Data) and Assigned(Node2.Data) then
    Compare := -1
  else if Assigned(Node1.Data) and not Assigned(Node2.Data) then
    Compare := 1
  else
    Compare := CompareStr(Node1.Text, Node2.Text);
end;

end.
