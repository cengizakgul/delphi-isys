unit rwDesignerComm;

interface

uses Forms, rwDesignerApp, rwConnectionInt, EvStreamUtils, evTransportInterfaces,
     isThreadManager, isBaseClasses, ISErrorUtils, SysUtils, isLogFile;

procedure DesignerSrv;

implementation

type
  TrwDesignerServer = class(TrwServer)
  protected
    function  CreateDispatcher: IevDatagramDispatcher; override;
    procedure Terminate;    
  end;


  TrDesignerDatagramDispatcher = class(TrwServerDatagramDispatcher)
  private
    procedure dmOpenDesigner(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmOpenQueryBuilder(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmInitializeAppAdapter(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmCallAppAdapterFunction(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRunQuery(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmQueryInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmOpenDesigner2(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmIsDesignerFormClosed(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmOpenQueryBuilder2(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmIsQueryBuilderFormClosed(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    procedure DoOnConstruction; override;
    procedure RegisterHandlers; override;
  end;


var
  rwDesignerServer: IrwServer;
  FDesignerwApp: IrwDesignerApp;


procedure DesignerSrv;
begin
  SetUnhandledErrorSettings(uesLogAllShowNothing);
  try
    Application.CreateForm(TrwDesignerAppFrm, rwDesignerAppFrm);
    rwDesignerServer := TrwDesignerServer.Create;
  except
    on E: Exception do
    begin
      if GlobalLogFile <> nil then
        GlobalLogFile.AddEvent(etFatalError, 'Server', E.Message, GetStack(E, ExceptAddr));
      raise;  
    end
  end;
end;

{ TrwDesignerServer }

function TrwDesignerServer.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TrDesignerDatagramDispatcher.Create;
end;

procedure TrwDesignerServer.Terminate;
begin
  inherited;
  TrwDesignerApp((FDesignerwApp as IisInterfacedObject).GetImplementation).Terminate;
end;

{ TrDesignerDatagramDispatcher }

procedure TrDesignerDatagramDispatcher.dmCallAppAdapterFunction(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Variant;
  begin
    Res := FDesignerwApp.CallAppAdapterFunction(ADatagram.Params.Value['AFunctionName'],
      ADatagram.Params.Value['AParams']);
    AResult.Params.AddValue(METHOD_RESULT, Res);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrDesignerDatagramDispatcher.dmInitializeAppAdapter(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FDesignerwApp.InitializeAppAdapter(ADatagram.Params.Value['AModuleName'],
      ADatagram.Params.Value['AAppCustomData']);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrDesignerDatagramDispatcher.dmIsDesignerFormClosed(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Rep: IEvDualStream;
    Res: Boolean;
  begin
    Res := FDesignerwApp.IsDesignerFormClosed(Rep);
    AResult.Params.AddValue(METHOD_RESULT, Res);
    AResult.Params.AddValue('AReportData', Rep);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrDesignerDatagramDispatcher.dmIsQueryBuilderFormClosed(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Boolean;
    Query: IevDualStream;
  begin
    Res := FDesignerwApp.IsQueryBuilderFormClosed(Query);
    AResult.Params.AddValue(METHOD_RESULT, Res);
    AResult.Params.AddValue('AQueryData', Query);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrDesignerDatagramDispatcher.dmOpenDesigner(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Boolean;
  begin
    Res := FDesignerwApp.OpenDesigner(IInterface(ADatagram.Params.Value['AReportData']) as IevDualStream,
      ADatagram.Params.Value['AReportName'],
      ADatagram.Params.Value['ADefaultReportType']);
    AResult.Params.AddValue(METHOD_RESULT, Res);      
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrDesignerDatagramDispatcher.dmOpenDesigner2(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Boolean;
  begin
    Res := FDesignerwApp.OpenDesigner2(IInterface(ADatagram.Params.Value['AReportData']) as IevDualStream,
      ADatagram.Params.Value['AReportName'],
      ADatagram.Params.Value['ADefaultReportType']);
    AResult.Params.AddValue(METHOD_RESULT, Res);      
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrDesignerDatagramDispatcher.dmOpenQueryBuilder(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Boolean;
  begin
    Res := FDesignerwApp.OpenQueryBuilder(IInterface(ADatagram.Params.Value['AQueryData']) as IevDualStream,
      ADatagram.Params.Value['ADescriptionView']);
    AResult.Params.AddValue(METHOD_RESULT, Res);      
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrDesignerDatagramDispatcher.dmOpenQueryBuilder2(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Boolean;
  begin
    Res := FDesignerwApp.OpenQueryBuilder2(
      IInterface(ADatagram.Params.Value['AQueryData']) as IevDualStream,
      ADatagram.Params.Value['ADescriptionView']);
    AResult.Params.AddValue(METHOD_RESULT, Res);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrDesignerDatagramDispatcher.dmQueryInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Variant;
  begin
    Res := FDesignerwApp.QueryInfo(ADatagram.Params.Value['AItemName']);
    AResult.Params.AddValue(METHOD_RESULT, Res);    
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrDesignerDatagramDispatcher.dmRunQuery(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Variant;
  begin
    Res := FDesignerwApp.RunQuery(IInterface(ADatagram.Params.Value['AQueryData']) as IevDualStream,
      ADatagram.Params.Value['AParams']);
    AResult.Params.AddValue(METHOD_RESULT, Res);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrDesignerDatagramDispatcher.DoOnConstruction;
begin
  inherited;
  FDesignerwApp := TrwDesignerApp.Create;
end;

procedure TrDesignerDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_RWDESIGNER_OpenDesigner, dmOpenDesigner);
  AddHandler(METHOD_RWDESIGNER_OpenQueryBuilder, dmOpenQueryBuilder);
  AddHandler(METHOD_RWDESIGNER_InitializeAppAdapter, dmInitializeAppAdapter);
  AddHandler(METHOD_RWDESIGNER_CallAppAdapterFunction, dmCallAppAdapterFunction);
  AddHandler(METHOD_RWDESIGNER_RunQuery, dmRunQuery);
  AddHandler(METHOD_RWDESIGNER_QueryInfo, dmQueryInfo);
  AddHandler(METHOD_RWDESIGNER_OpenDesigner2, dmOpenDesigner2);
  AddHandler(METHOD_RWDESIGNER_IsDesignerFormClosed, dmIsDesignerFormClosed);
  AddHandler(METHOD_RWDESIGNER_OpenQueryBuilder2, dmOpenQueryBuilder2);
  AddHandler(METHOD_RWDESIGNER_IsQueryBuilderFormClosed, dmIsQueryBuilderFormClosed);
end;

end.
