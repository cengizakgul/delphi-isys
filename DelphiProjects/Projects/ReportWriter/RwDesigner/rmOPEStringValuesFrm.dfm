inherited rmOPEStringValues: TrmOPEStringValues
  Width = 283
  Height = 98
  object vlValues: TValueListEditor
    Left = 0
    Top = 0
    Width = 283
    Height = 98
    Align = alClient
    KeyOptions = [keyEdit, keyAdd, keyDelete]
    TabOrder = 0
    TitleCaptions.Strings = (
      'Description'
      'Value')
    OnStringsChange = vlValuesStringsChange
    ColWidths = (
      233
      44)
  end
end
