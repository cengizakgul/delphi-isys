object rmTextFilterSelect: TrmTextFilterSelect
  Left = 416
  Top = 225
  Width = 536
  Height = 259
  BorderStyle = bsSizeToolWin
  Caption = 'Text Filters'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    528
    232)
  PixelsPerInch = 96
  TextHeight = 13
  object lvFilters: TListView
    Left = 0
    Top = 0
    Width = 528
    Height = 199
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = 'Filter Name'
        Width = 200
      end
      item
        AutoSize = True
        Caption = 'Description'
      end>
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    SortType = stText
    TabOrder = 0
    ViewStyle = vsReport
    OnDblClick = lvFiltersDblClick
  end
  object btnOK: TButton
    Left = 363
    Top = 207
    Width = 75
    Height = 22
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object btnCancel: TButton
    Left = 449
    Top = 207
    Width = 75
    Height = 22
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
