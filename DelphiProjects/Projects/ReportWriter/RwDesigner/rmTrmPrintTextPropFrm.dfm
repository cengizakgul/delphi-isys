inherited rmTrmPrintTextProp: TrmTrmPrintTextProp
  Width = 334
  Height = 378
  inherited pcCategories: TPageControl
    Width = 334
    Height = 378
    inherited tsBasic: TTabSheet
      object grbValue: TGroupBox
        Left = 8
        Top = 82
        Width = 303
        Height = 159
        Caption = 'Value'
        TabOrder = 2
        inline frmValue: TrmOPEValue
          Left = 10
          Top = 20
          Width = 283
          Height = 130
          AutoScroll = False
          TabOrder = 0
          inherited pcTypes: TPageControl
            Width = 283
            Height = 130
            inherited tsEmpty: TTabSheet
              inherited frmEmpty: TrmOPEEmpty
                Width = 275
                Height = 99
                inherited Label1: TLabel
                  Width = 275
                  Height = 99
                end
              end
            end
          end
        end
      end
    end
    object tcTextFilters: TTabSheet [1]
      Caption = 'Text Filters'
      ImageIndex = 4
      inline frmTextFilter: TrmOPETextFilter
        Left = 8
        Top = 10
        Width = 308
        Height = 335
        AutoScroll = False
        TabOrder = 0
        inherited Panel1: TPanel
          Width = 308
          inherited lHeader2: TLabel
            Width = 308
          end
          inherited lvFilters: TListView
            Width = 308
          end
        end
        inherited pnlParams: TPanel
          Width = 308
          Height = 153
        end
      end
    end
    inherited tcAppearance: TTabSheet
      inline frmFont: TrmOPEFont
        Left = 10
        Top = 11
        Width = 75
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
      end
      inline frmColor: TrmOPEColor
        Left = 104
        Top = 11
        Width = 75
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
      end
    end
    inherited tcPrint: TTabSheet
      inherited pnlBlocking: TPanel
        Top = 149
        TabOrder = 5
      end
      inline frmAutosize: TrmOPEBoolean
        Left = 8
        Top = 87
        Width = 140
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 3
        inherited chbProp: TCheckBox
          Width = 140
          Caption = 'Automatically adjust size '
        end
      end
      inline frmWordWrap: TrmOPEBoolean
        Left = 8
        Top = 113
        Width = 116
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 4
        inherited chbProp: TCheckBox
          Width = 116
          Caption = 'Wrap text by words'
        end
      end
    end
  end
end
