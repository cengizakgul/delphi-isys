inherited rmTrmlQueryDBGridProp: TrmTrmlQueryDBGridProp
  Height = 358
  inherited pcCategories: TPageControl
    Height = 358
    inherited tsBasic: TTabSheet
      inline frmQuery: TrmOPEQuery
        Left = 8
        Top = 89
        Width = 91
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 2
        inherited btnQuery: TButton
          Width = 91
          OnClick = frmQuerybtnQueryClick
        end
      end
    end
    inherited tcAppearance: TTabSheet
      inherited frmColor: TrmOPEColor
        Visible = False
      end
      inline frmMultiSelection: TrmOPEBoolean
        Left = 8
        Top = 300
        Width = 110
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited chbProp: TCheckBox
          Caption = 'Multi-Selection'
        end
      end
      inline frmDisplayFields: TrmOPEDisplayFields
        Left = 8
        Top = 7
        Width = 296
        Height = 283
        AutoScroll = False
        TabOrder = 2
        inherited grFields: TisRWDBGrid
          Height = 283
        end
      end
    end
    inherited tsParam: TTabSheet
      inherited frmParam1: TrmOPEReportParam
        Top = 41
      end
      inline frmKeyField: TrmOPEEnum
        Left = 8
        Top = 10
        Width = 291
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited lPropName: TLabel
          Width = 43
          Caption = 'Key Field'
        end
        inherited cbList: TComboBox
          Left = 90
          Width = 201
          OnClick = frmKeyFieldcbListClick
        end
      end
    end
  end
end
