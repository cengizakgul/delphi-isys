object rwEvaluate: TrwEvaluate
  Left = 493
  Top = 229
  Width = 431
  Height = 285
  ActiveControl = edExpression
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'Evaluate/Modify'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    423
    258)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 9
    Width = 51
    Height = 13
    Caption = 'Expression'
  end
  object Label2: TLabel
    Left = 6
    Top = 34
    Width = 30
    Height = 13
    Caption = 'Result'
  end
  object Label3: TLabel
    Left = 6
    Top = 229
    Width = 52
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'New Value'
  end
  object edExpression: TEdit
    Left = 64
    Top = 7
    Width = 271
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    OnExit = edExpressionExit
    OnKeyPress = edExpressionKeyPress
  end
  object meResult: TMemo
    Left = 6
    Top = 50
    Width = 410
    Height = 160
    Anchors = [akLeft, akTop, akRight, akBottom]
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object btnEvaluate: TButton
    Left = 341
    Top = 5
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Evaluate'
    TabOrder = 1
    OnClick = btnEvaluateClick
  end
  object edNewValue: TEdit
    Left = 64
    Top = 227
    Width = 271
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 3
    OnExit = edNewValueExit
    OnKeyPress = edNewValueKeyPress
  end
  object btnModify: TButton
    Left = 341
    Top = 225
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Modify'
    TabOrder = 4
    OnClick = btnModifyClick
  end
  object ActionList1: TActionList
    Left = 232
    Top = 88
    object actClose: TAction
      Caption = 'Close'
      ShortCut = 27
      OnExecute = actCloseExecute
    end
  end
end
