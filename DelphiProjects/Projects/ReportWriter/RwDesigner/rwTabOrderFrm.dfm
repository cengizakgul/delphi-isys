object rwTabOrder: TrwTabOrder
  Left = 668
  Top = 179
  Width = 336
  Height = 380
  Caption = 'Tab Order'
  Color = clBtnFace
  Constraints.MinHeight = 220
  Constraints.MinWidth = 200
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    328
    353)
  PixelsPerInch = 96
  TextHeight = 13
  object lbControls: TListBox
    Left = 4
    Top = 3
    Width = 239
    Height = 348
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    TabOrder = 0
  end
  object btnTop: TButton
    Left = 250
    Top = 4
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Top'
    TabOrder = 1
    OnClick = btnTopClick
  end
  object btnUp: TButton
    Left = 250
    Top = 36
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Up'
    TabOrder = 2
    OnClick = btnUpClick
  end
  object btnDown: TButton
    Left = 250
    Top = 67
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Down'
    TabOrder = 3
    OnClick = btnDownClick
  end
  object btnBottom: TButton
    Left = 250
    Top = 100
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Bottom'
    TabOrder = 4
    OnClick = btnBottomClick
  end
  object btnOK: TButton
    Left = 250
    Top = 294
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 5
  end
  object btnCancel: TButton
    Left = 250
    Top = 326
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
  end
end
