inherited rwReportFormToolsBar: TrwReportFormToolsBar
  Width = 1021
  inherited ctbBar: TControlBar
    Width = 1021
    inherited tlbComponents: TToolBar
      Left = 316
      Width = 460
      object tbtLabel: TToolButton
        Left = 92
        Top = 0
        Hint = 'Label'
        Caption = 'TrwLabel'
        Grouped = True
        ImageIndex = 30
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtPCLLabel: TToolButton
        Left = 115
        Top = 0
        Hint = 'PCL Label'
        Caption = 'TrwPCLLabel'
        Grouped = True
        ImageIndex = 41
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtBarcode1D: TToolButton
        Left = 138
        Top = 0
        Hint = 'Barcode 1D'
        Caption = 'TrwBarcode1D'
        Grouped = True
        ImageIndex = 93
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtPDF417: TToolButton
        Left = 161
        Top = 0
        Hint = '2D Barcode PDF417'
        Caption = 'TrwBarcodePDF417'
        Grouped = True
        ImageIndex = 94
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtMemo: TToolButton
        Left = 184
        Top = 0
        Hint = 'Memo'
        Grouped = True
        ImageIndex = 31
        Style = tbsCheck
        Visible = False
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtRichText: TToolButton
        Left = 207
        Top = 0
        Hint = 'RichMemo'
        Grouped = True
        ImageIndex = 32
        Style = tbsCheck
        Visible = False
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtSysLabel: TToolButton
        Left = 230
        Top = 0
        Hint = 'SysLabel'
        Caption = 'TrwSysLabel'
        Grouped = True
        ImageIndex = 34
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtImage: TToolButton
        Left = 253
        Top = 0
        Hint = 'Image'
        Caption = 'TrwImage'
        Grouped = True
        ImageIndex = 40
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtContainer: TToolButton
        Left = 276
        Top = 0
        Hint = 'Container'
        Caption = 'TrwContainer'
        Grouped = True
        ImageIndex = 37
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtFrame: TToolButton
        Left = 299
        Top = 0
        Hint = 'Frame'
        Caption = 'TrwFrame'
        Grouped = True
        ImageIndex = 36
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtShape: TToolButton
        Left = 322
        Top = 0
        Hint = 'Shape'
        Caption = 'TrwShape'
        Grouped = True
        ImageIndex = 35
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtSubReport: TToolButton
        Left = 345
        Top = 0
        Hint = 'SubReport'
        Caption = 'TrwSubReport'
        Grouped = True
        ImageIndex = 38
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtDBText: TToolButton
        Left = 368
        Top = 0
        Hint = 'DBText'
        Caption = 'TrwDBText'
        Grouped = True
        ImageIndex = 33
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtDBImage: TToolButton
        Left = 391
        Top = 0
        Hint = 'DBImage'
        Caption = 'TrwDBImage'
        Grouped = True
        ImageIndex = 42
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object tbtLibComp: TToolButton
        Left = 414
        Top = 0
        Hint = 'Library Component'
        Caption = 'Library'
        Grouped = True
        ImageIndex = 45
        Style = tbsCheck
        OnClick = tbtQueryClick
        OnMouseDown = tbtQueryMouseDown
      end
      object ToolButton15: TToolButton
        Left = 437
        Top = 0
        Hint = 'Report'
        Caption = 'TrwReport'
        ImageIndex = 38
        Visible = False
      end
    end
    inherited tlbFont: TToolBar
      Left = 789
      Width = 497
    end
    inherited tlbExperts: TToolBar
      Left = 668
      Width = 161
      object btnPrintOrder: TToolButton
        Left = 92
        Top = 0
        Hint = 'Print Order'
        Caption = 'btnPrintOrder'
        ImageIndex = 43
        OnClick = btnPrintOrderClick
      end
      object btnASCIIOrder: TToolButton
        Left = 115
        Top = 0
        Hint = 'ASCII Print Order'
        Caption = 'btnASCIIOrder'
        ImageIndex = 88
        OnClick = btnASCIIOrderClick
      end
      object btnWizards: TToolButton
        Left = 138
        Top = 0
        Caption = 'Wizards'
        DropdownMenu = pmWizards
        ImageIndex = 44
      end
    end
  end
  inherited pmToolBars: TPopupMenu
    Left = 696
    Top = 24
  end
  object pmWizards: TPopupMenu
    OnPopup = pmWizardsPopup
    Left = 864
    Top = 26
    object miEmptyWiz: TMenuItem
      Caption = 'There are no any Wizards'
    end
  end
end
