unit rwStatFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeEngine, Series, ExtCtrls, TeeProcs, Chart, StdCtrls, rwUtils, Buttons,
  DBCtrls, Db, Grids,ComCtrls, rwDesignClasses, Wwdatsrc,
  Wwdbigrd, Wwdbgrid, kbmMemTable, ISKbmMemDataSet;

type
  TrwStatistics = class(TForm)
    OpenDialog: TOpenDialog;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Chart: TChart;
    Series1: TPieSeries;
    Panel1: TPanel;
    lTotalTime: TLabel;
    lTotal: TLabel;
    Label1: TLabel;
    lDataSize: TLabel;
    Label3: TLabel;
    lRQ: TLabel;
    Label4: TLabel;
    lLQ: TLabel;
    evDBGrid1: TisRWDBGrid;
    evDataSource1: TDataSource;
    dsStat: TISRWClientDataSet;
    Panel2: TPanel;
    sbOpenLog: TSpeedButton;
    DBNavigator1: TDBNavigator;
    dsStatDateTimeStamp: TDateTimeField;
    dsStatReportName: TStringField;
    dsStatTotalTime: TIntegerField;
    dsStatRemoteQueriesTime: TIntegerField;
    dsStatCacheDataTime: TIntegerField;
    dsStatCacheDataSize: TIntegerField;
    dsStatLogicalQueriesTime: TIntegerField;
    dsStatRemoteQueries: TIntegerField;
    dsStatLogicalQueries: TIntegerField;
    procedure sbOpenLogClick(Sender: TObject);
    procedure evDataSource1DataChange(Sender: TObject; Field: TField);
  private
    procedure PrepareData(ARec: TrwStatisticRec);
//    procedure LoadFromFile(AFileName: String);
  public
  end;

  procedure ShowReportStatistics;

implementation

{$R *.DFM}

uses rwEngine;

procedure ShowReportStatistics;
var
  Frm: TrwStatistics;
begin
  Frm := TrwStatistics.Create(Application);
  with Frm do
    try
      PrepareData(RwStatRec^);
      ShowModal;
    finally
      Frm.Free;
    end;
end;

{ TrwStatistics }

procedure TrwStatistics.PrepareData(ARec: TrwStatisticRec);
var
  h: Cardinal;
begin
  Chart.Title.Text.Text := ARec.ReportName+#13+DateTimeToStr(ARec.DateTimeStamp);
  with ARec do
  begin
    Chart.Series[0].Clear;
    Chart.Series[0].AddXY(0, TotalTime - RemoteQueriesTime -
      CacheDataTime - LogicalQueriesTime, 'Calculations', clWhite);
    Chart.Series[0].AddXY(0, LogicalQueriesTime, 'Logical Queries', clBlue);
    Chart.Series[0].AddXY(0, RemoteQueriesTime, 'Remote Queries', clGreen);
    Chart.Series[0].AddXY(0, CacheDataTime, 'Caching Data', clYellow);

    lTotal.Caption := '';
    h := TotalTime div (1000 * 60 *60);
    TotalTime := TotalTime - h * 1000 * 60 * 60;
    if h <> 0 then
      lTotal.Caption :=  IntToStr(h) + ' h ';
    h := TotalTime div (1000 * 60);
    TotalTime := TotalTime - h * 1000 * 60;
    if (lTotal.Caption <> '') or (h <> 0) then
      lTotal.Caption := lTotal.Caption + IntToStr(h) + ' min ';
    h := TotalTime div 1000;
    TotalTime := TotalTime - h * 1000;
    if (lTotal.Caption <> '') or (h <> 0) then
      lTotal.Caption := lTotal.Caption + IntToStr(h) + ' sec ';
    if (lTotal.Caption = '') or (lTotal.Caption <> '') and (TotalTime <> 0) then
      lTotal.Caption := lTotal.Caption + IntToStr(TotalTime) + ' ms';

    lDataSize.Caption := IntToStr(CacheDataSize);
    lRQ.Caption := IntToStr(RemoteQueries);
    lLQ.Caption := IntToStr(LogicalQueries);
  end;
end;


procedure TrwStatistics.sbOpenLogClick(Sender: TObject);
begin
{  OpenDialog.FileName := RwStatLogFile;
  if OpenDialog.Execute then
    LoadFromFile(OpenDialog.FileName);}
end;


{
procedure TrwStatistics.LoadFromFile(AFileName: String);
var
  F: TextFile;
  h: String;
begin
  Update;
  Busy;
  dsStat.DisableControls;
  dsStat.Close;
  dsStat.CreateDataSet;

  AssignFile(F, AFileName);
  try
    Reset(F);

    while not Eof(F) do
    begin
      Readln(F, h);

      dsStat.Append;
      dsStat.FieldByName('DateTimeStamp').AsDateTime := StrToDateTime(GetNextStrValue(h, ';'));
      dsStat.FieldByName('ReportName').AsString := GetNextStrValue(h, ';');
      dsStat.FieldByName('TotalTime').AsInteger := StrToInt(GetNextStrValue(h, ';'));
      GetNextStrValue(h, ';');
      dsStat.FieldByName('RemoteQueriesTime').AsInteger := StrToInt(GetNextStrValue(h, ';'));
      dsStat.FieldByName('CacheDataTime').AsInteger := StrToInt(GetNextStrValue(h, ';'));
      dsStat.FieldByName('CacheDataSize').AsInteger := StrToInt(GetNextStrValue(h, ';'));
      dsStat.FieldByName('LogicalQueriesTime').AsInteger := StrToInt(GetNextStrValue(h, ';'));
      GetNextStrValue(h, ';');
      dsStat.FieldByName('RemoteQueries').AsInteger := StrToInt(GetNextStrValue(h, ';'));
      dsStat.FieldByName('LogicalQueries').AsInteger := StrToInt(GetNextStrValue(h, ';'));
      dsStat.Post;
    end;

  finally
    CloseFile(F);
  end;
  dsStat.First;
  dsStat.EnableControls;
  Ready;
end;
}

procedure TrwStatistics.evDataSource1DataChange(Sender: TObject; Field: TField);
var
  R: TrwStatisticRec;
begin
  R.DateTimeStamp := dsStat.FieldByName('DateTimeStamp').AsDateTime;
  R.ReportName := dsStat.FieldByName('ReportName').AsString;
  R.TotalTime := dsStat.FieldByName('TotalTime').AsInteger;
  R.RemoteQueriesTime := dsStat.FieldByName('RemoteQueriesTime').AsInteger;
  R.CacheDataTime := dsStat.FieldByName('CacheDataTime').AsInteger;
  R.CacheDataSize := dsStat.FieldByName('CacheDataSize').AsInteger;
  R.LogicalQueriesTime := dsStat.FieldByName('LogicalQueriesTime').AsInteger;
  R.RemoteQueries := dsStat.FieldByName('RemoteQueries').AsInteger;
  R.LogicalQueries := dsStat.FieldByName('LogicalQueries').AsInteger;
  PrepareData(R);
end;

end.
