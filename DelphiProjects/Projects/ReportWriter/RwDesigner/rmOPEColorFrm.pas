unit rmOPEColorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls;

type
  TrmOPEColor = class(TrmObjPropertyEditor)
    btnColor: TButton;
    ColorDialog: TColorDialog;
    procedure btnColorClick(Sender: TObject);
  protected
    function  GetDefaultPropertyName: String; override;
  public
    procedure ShowPropValue; override;
    function  NewPropertyValue: Variant; override;
  end;

implementation

{$R *.dfm}

{ TrmObjPropertyEditor1 }

function TrmOPEColor.GetDefaultPropertyName: String;
begin
  Result := 'Color';
end;

procedure TrmOPEColor.btnColorClick(Sender: TObject);
begin
  if ColorDialog.Execute then
    NotifyOfChanges;
end;

function TrmOPEColor.NewPropertyValue: Variant;
begin
  Result := ColorDialog.Color;
end;

procedure TrmOPEColor.ShowPropValue;
begin
  ColorDialog.Color := TColor(Integer(OldPropertyValue));
end;

end.
 