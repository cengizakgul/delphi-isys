inherited rmLocalToolBar: TrmLocalToolBar
  Width = 443
  Height = 71
  Align = alTop
  AutoSize = True
  ParentBackground = False
  ParentColor = False
  object ControlBar: TControlBar
    Left = 0
    Top = 0
    Width = 443
    Height = 71
    Align = alTop
    AutoDock = False
    AutoDrag = False
    AutoSize = True
    BevelEdges = []
    DockSite = False
    PopupMenu = pmToolBars
    TabOrder = 0
  end
  object MainMenu: TMainMenu
    Left = 352
  end
  object ActionList: TActionList
    Images = ImageList
    Left = 382
  end
  object ImageList: TImageList
    Left = 412
  end
  object pmToolBars: TPopupMenu
    OnPopup = pmToolBarsPopup
    Left = 304
  end
end
