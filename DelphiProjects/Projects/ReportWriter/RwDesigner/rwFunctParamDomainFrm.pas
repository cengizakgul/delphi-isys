unit rwFunctParamDomainFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ValEdit, ComCtrls, rwTypes, rwBasicClasses, rwEngineTypes,
  rwBasicUtils, rwUtils, ExtCtrls, Menus;

type
  TrwFunctParamDomain = class(TFrame)
    lvParams: TListView;
    Label1: TLabel;
    Panel1: TPanel;
    vleValues: TValueListEditor;
    Panel2: TPanel;
    Label2: TLabel;
    edDefaultValue: TEdit;
    Label3: TLabel;
    edDescription: TEdit;
    procedure lvParamsChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure vleValuesStringsChange(Sender: TObject);
  private
    FRWFunction: TrwFunction;
    FActiveParam: Integer;
    FModified: Boolean;
    procedure SetRWFunction(const Value: TrwFunction);
    procedure Syncronize;
    procedure SyncDomainValues;
    procedure UpdateDomain;
  public
    property RWFunction: TrwFunction read FRWFunction write SetRWFunction;
  end;

implementation

{$R *.dfm}

{ TrwFunctParamDomain }

procedure TrwFunctParamDomain.SetRWFunction(const Value: TrwFunction);
begin
  UpdateDomain;
  FRWFunction := Value;
  Syncronize;
end;

procedure TrwFunctParamDomain.Syncronize;
var
  i: Integer;
  LI: TListItem;
begin
  FActiveParam := -1;

  lvParams.Clear;

  if Assigned(FRWFunction) and (FRWFunction.Params.Count > 0) then
  begin
    for i := 0 to FRWFunction.Params.Count - 1 do
    begin
      LI := lvParams.Items.Add;
      LI.Caption := FRWFunction.Params[i].Name;
      LI.SubItems.Add(FRWFunction.Params[i].DisplayName);
    end;

    lvParams.Selected := lvParams.Items[0];

    Enabled := True;
  end

  else
    Enabled := False;
end;

procedure TrwFunctParamDomain.lvParamsChange(Sender: TObject; Item: TListItem; Change: TItemChange);
begin
  SyncDomainValues;
end;

procedure TrwFunctParamDomain.SyncDomainValues;
var
  i: Integer;
begin
  UpdateDomain;

  if lvParams.Selected = nil then
    FActiveParam := -1
  else
    FActiveParam := lvParams.Selected.Index;

  vleValues.Strings.Clear;
  edDefaultValue.Text := '';
  edDescription.Text := '';

  if FActiveParam <> -1 then
  begin
    for i := Low(RWFunction.Params[FActiveParam].Domain) to High(RWFunction.Params[FActiveParam].Domain) do
      vleValues.InsertRow(RWFunction.Params[FActiveParam].Domain[i].Description,
        RWFunction.Params[FActiveParam].DomainValueToStr(RWFunction.Params[FActiveParam].Domain[i].Value), True);

    edDefaultValue.Text := RWFunction.Params[FActiveParam].DomainValueToStr(RWFunction.Params[FActiveParam].DefaultValue);
    edDescription.Text := RWFunction.Params[FActiveParam].DisplayName;
  end;

  vleValues.Enabled := FActiveParam <> -1;
  edDefaultValue.Enabled := FActiveParam <> -1;

  FModified := False;
end;

procedure TrwFunctParamDomain.UpdateDomain;
var
  i: Integer;
  Vals: TrwDomain;
  h: String;
begin
  if not FModified or not Assigned(FRWFunction) or (FActiveParam = -1) then
    Exit;

  RWFunction.Params[FActiveParam].DisplayName := edDescription.Text;
  lvParams.Selected.SubItems[0] := edDescription.Text;

  try
    SetLength(Vals, vleValues.Strings.Count);

    for i := 0 to vleValues.Strings.Count - 1 do
    begin
      h := vleValues.Strings[i];
      Vals[i].Description := GetNextStrValue(h, '=');
      try
        Vals[i].Value := RWFunction.Params[FActiveParam].StrToDomainValue(h);
      except
        Vals[i].Value := '';
      end;
    end;

    RWFunction.Params[FActiveParam].Domain := Vals;
    if edDefaultValue.Text = '' then
      RWFunction.Params[FActiveParam].DefaultValue := ''
    else
      RWFunction.Params[FActiveParam].DefaultValue := RWFunction.Params[FActiveParam].StrToDomainValue(edDefaultValue.Text);
  finally
    SystemLibFunctions.Modified := True;
  end;
end;

procedure TrwFunctParamDomain.vleValuesStringsChange(Sender: TObject);
begin
  FModified := True;
end;

end.
