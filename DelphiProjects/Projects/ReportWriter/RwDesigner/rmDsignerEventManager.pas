unit rmDsignerEventManager;

interface

uses SysUtils, Classes, Contnrs, Windows, rmDsgnTypes, SyncObjs;

type
  TrmMessageRegistry = class(TObject)
  private
    FList: TStringList;
  public
    constructor Create;
    destructor  Destroy; override;
    function    Count: Integer;
    procedure   Clear;
    function    IndexOf(const AMessageID: TrmDsgnMessageID): Integer;
    procedure   Add(const AMessageID: TrmDsgnMessageID; const AObject: IrmMessageReceiver);
    procedure   Delete(const AMessageID: TrmDsgnMessageID; const AObject: IrmMessageReceiver);
    function    GetObjectsByMessage(const AMessageID: TrmDsgnMessageID): TList;
    function    GetMessageByIndex(const AIndex: Integer): TrmDsgnMessageID;
  end;



  TrmDsignerEventManager = class(TInterfacedObject, IrmDesignerEventManager)
  private
    FOwnerWndHandle: HWND;
    FRegObjects: TrmMessageRegistry;
    FQueue: TQueue;
    CS: TCriticalSection;
    function  GetMessage: TrmMessageRec;
    function  QueueIsNotEmpty: Boolean;
    procedure ClearQueue;
  protected
    procedure SetOwnerWndHandle(const AOwnerWndHandle: HWND);
    procedure RegisterObject(const AObject: IrmMessageReceiver);
    procedure UnregisterObject(const AObject: IrmMessageReceiver);
    procedure SendBroadcastMessage(var Message: TrmMessageRec);
    procedure PostBroadcastMessage(var Message: TrmMessageRec);
    procedure ProcessMessages;
    function  QueueIsEmpty: Boolean;
  public
    constructor Create;
    destructor  Destroy; override;
  end;


implementation

{ TrmDsignerEventManager }

constructor TrmDsignerEventManager.Create;
begin
  CS := TCriticalSection.Create;
  FRegObjects := TrmMessageRegistry.Create;
  FQueue := TQueue.Create;
end;

destructor TrmDsignerEventManager.Destroy;
begin
  FreeAndNil(FRegObjects);
  ClearQueue;
  FreeAndNil(FQueue);
  FreeAndNil(CS);  
  inherited;
end;

procedure TrmDsignerEventManager.RegisterObject(const AObject: IrmMessageReceiver);
var
  i: Integer;
  L: TList;
begin
  L := TList.Create;
  try
    L.Capacity := 20;
    AObject.GetListeningMessages(L);
    for i := 0 to L.Count - 1 do
      FRegObjects.Add(TrmDsgnMessageID(L[i]), AObject);
  finally
    L.Free;
  end;
end;

procedure TrmDsignerEventManager.SendBroadcastMessage(var Message: TrmMessageRec);
var
  i: Integer;
  Obj: IrmMessageReceiver;
  ObjList: TList;
  CurrentReceivers: TList;
begin
  ObjList := FRegObjects.GetObjectsByMessage(Message.MessageID);
  if not Assigned(ObjList) then
    Exit;

  CurrentReceivers := TList.Create;
  try
    CurrentReceivers.Assign(ObjList, laCopy);

    for i := 0 to CurrentReceivers.Count - 1 do
      if Assigned(CurrentReceivers[i]) then
      begin
        Obj := IrmMessageReceiver(CurrentReceivers[i]);
        Obj.VCLObject.Dispatch(Message);
      end;

  finally
    FreeAndNil(CurrentReceivers);
  end;  

  ProcessMessages;
end;

procedure TrmDsignerEventManager.UnregisterObject(const AObject: IrmMessageReceiver);
var
  i: Integer;
begin
  for i := FRegObjects.Count - 1 downto 0 do
    FRegObjects.Delete(FRegObjects.GetMessageByIndex(i), AObject);
end;

function TrmDsignerEventManager.GetMessage: TrmMessageRec;
var
  Msg: ^TrmMessageRec;
begin
  Msg := FQueue.Pop;
  Result := Msg^;
  Dispose(Msg);
end;

procedure TrmDsignerEventManager.ClearQueue;
begin
  while QueueIsNotEmpty do
    GetMessage;
end;

function TrmDsignerEventManager.QueueIsNotEmpty: Boolean;
begin
  Result := FQueue.Count > 0;
end;

procedure TrmDsignerEventManager.PostBroadcastMessage(var Message: TrmMessageRec);
var
  Msg: ^TrmMessageRec;
begin
  CS.Enter;
  try
    if FOwnerWndHandle = 0 then
      Exit;

    New(Msg);
    Msg^ := Message;
    FQueue.Push(Msg);

    PostMessage(FOwnerWndHandle, WM_DESIGNER_PROCESS_POSTED_MESSAGES, 0, 0);
  finally
    CS.Leave;
  end;
end;

procedure TrmDsignerEventManager.ProcessMessages;
var
  Msg: TrmMessageRec;
begin
  if QueueIsNotEmpty then
  begin
    Msg := GetMessage;
    SendBroadcastMessage(Msg);
  end;
end;

procedure TrmDsignerEventManager.SetOwnerWndHandle(const AOwnerWndHandle: HWND);
begin
  FOwnerWndHandle := AOwnerWndHandle;

  if FOwnerWndHandle = 0 then
    ClearQueue;
end;

function TrmDsignerEventManager.QueueIsEmpty: Boolean;
begin
  Result := FQueue.Count = 0; 
end;

{ TrmMessageRegistry }

constructor TrmMessageRegistry.Create;
begin
  FList := TStringList.Create;
  FList.Sorted := True;
end;

destructor TrmMessageRegistry.Destroy;
begin
  Clear;
  FreeAndNil(FList);
  inherited;
end;

procedure TrmMessageRegistry.Clear;
var
  i: Integer;
begin
  for i := 0 to FList.Count - 1 do
    Flist.Objects[i].Free;
  FList.Clear;  
end;


procedure TrmMessageRegistry.Add(const AMessageID: TrmDsgnMessageID; const AObject: IrmMessageReceiver);
var
  ObjList: TList;
  P: Pointer;
begin
  ObjList := GetObjectsByMessage(AMessageID);

  if not Assigned(ObjList) then
  begin
    ObjList := TList.Create;
    FList.AddObject(IntToStr(AMessageID), ObjList);
  end;

  P := Pointer(AObject);
  if ObjList.IndexOf(P) = -1 then
    ObjList.Add(P);
end;

procedure TrmMessageRegistry.Delete(const AMessageID: TrmDsgnMessageID; const AObject: IrmMessageReceiver);
var
  i: Integer;
  ObjList: TList;
  P: Pointer;
begin
  ObjList := GetObjectsByMessage(AMessageID);
  if Assigned(ObjList) then
  begin
    P := Pointer(AObject);
    i := ObjList.IndexOf(P);
    if i <> -1 then
      ObjList.Delete(i);

    if ObjList.Count = 0 then
    begin
      FreeAndNil(ObjList);
      FList.Delete(IndexOf(AMessageID));
    end;
  end;
end;

function TrmMessageRegistry.IndexOf(const AMessageID: TrmDsgnMessageID): Integer;
begin
  Result := FList.IndexOf(IntToStr(AMessageID));
end;

function TrmMessageRegistry.GetObjectsByMessage(const AMessageID: TrmDsgnMessageID): TList;
var
  i: Integer;
begin
  i := IndexOf(AMessageID);

  if i = -1 then
    Result := nil
  else
    Result := TList(FList.Objects[i]);
end;

function TrmMessageRegistry.Count: Integer;
begin
  Result := FList.Count;
end;

function TrmMessageRegistry.GetMessageByIndex(const AIndex: Integer): TrmDsgnMessageID;
begin
  Result := StrToInt(FList[AIndex]);
end;

end.
