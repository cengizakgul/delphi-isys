object rwDsgnLibrary: TrwDsgnLibrary
  Left = 216
  Top = 135
  AutoScroll = False
  Caption = 'Report Writer Library'
  ClientHeight = 698
  ClientWidth = 924
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pgcPages: TPageControl
    Left = 0
    Top = 0
    Width = 924
    Height = 698
    ActivePage = tsComp
    Align = alClient
    TabOrder = 0
    object tsComp: TTabSheet
      Caption = 'Components'
      PopupMenu = pmComp
      OnHide = tsCompHide
      OnShow = tsCompShow
      object Splitter2: TSplitter
        Left = 630
        Top = 78
        Height = 561
        Align = alRight
      end
      object Panel5: TPanel
        Left = 0
        Top = 639
        Width = 916
        Height = 31
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object lChangeComp: TLabel
          Left = 0
          Top = 11
          Width = 66
          Height = 13
          Caption = 'lChangeComp'
        end
        object Panel2: TPanel
          Left = 368
          Top = 0
          Width = 548
          Height = 31
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object btnAddComp: TButton
            Left = 3
            Top = 4
            Width = 75
            Height = 25
            Action = actAddComp
            TabOrder = 0
          end
          object btnEditComp: TButton
            Left = 88
            Top = 4
            Width = 75
            Height = 25
            Action = actEditComp
            TabOrder = 1
          end
          object btnDelComp: TButton
            Left = 172
            Top = 4
            Width = 75
            Height = 25
            Action = actDelComp
            TabOrder = 2
          end
          object btnSaveComp: TButton
            Left = 278
            Top = 4
            Width = 75
            Height = 25
            Action = actSaveComp
            TabOrder = 3
          end
          object btnCancelComp: TButton
            Left = 363
            Top = 4
            Width = 75
            Height = 25
            Action = actCancelComp
            TabOrder = 4
          end
          object btnCompileComp: TButton
            Left = 469
            Top = 4
            Width = 75
            Height = 25
            Action = actCompile
            TabOrder = 5
          end
        end
      end
      inline CompDesigner: TrwPageDesigner
        Left = 0
        Top = 78
        Width = 630
        Height = 561
        Align = alClient
        AutoScroll = False
        TabOrder = 1
        inherited Splitter1: TSplitter
          Top = 176
          Width = 630
        end
        inherited pnlVertRuler: TPanel
          Height = 153
        end
        inherited pnlHorRuler: TPanel
          Width = 630
        end
        inherited Panel3: TPanel
          Top = 179
          Width = 630
          Height = 382
          inherited Splitter2: TSplitter
            Height = 382
          end
          inherited rwPropertiesEditor: TrwPropertiesEditor
            Height = 382
            inherited pgcInspector: TPageControl
              Height = 364
            end
            inherited pnlCompName: TPanel
              Top = 364
            end
          end
          inherited Panel1: TPanel
            Width = 361
            Height = 382
            inherited pcEvents: TPageControl
              Width = 361
              Height = 382
              OnChange = actAddCategoryExecute
            end
          end
        end
      end
      inline DesignCompArea: TrwDesignCompArea
        Left = 184
        Top = 66
        Width = 298
        Height = 49
        AutoScroll = False
        Color = clWindow
        ParentColor = False
        TabOrder = 2
      end
      object Panel6: TPanel
        Left = 633
        Top = 78
        Width = 283
        Height = 561
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object Splitter3: TSplitter
          Left = 0
          Top = 369
          Width = 283
          Height = 3
          Cursor = crVSplit
          Align = alBottom
        end
        object pnlTree: TPanel
          Left = 0
          Top = 0
          Width = 283
          Height = 369
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object pcTrees: TPageControl
            Left = 0
            Top = 0
            Width = 283
            Height = 369
            ActivePage = tsInheritance
            Align = alClient
            MultiLine = True
            TabOrder = 0
            object tsInheritance: TTabSheet
              Caption = 'Inheritance'
              object tvLibComp: TTreeView
                Left = 0
                Top = 0
                Width = 275
                Height = 341
                Align = alClient
                HideSelection = False
                Images = DsgnRes.ilButtons
                Indent = 19
                ReadOnly = True
                TabOrder = 0
                OnChange = tvLibCompChange
                OnDblClick = tvLibCompDblClick
                OnEdited = tvLibCompEdited
              end
            end
            object tsCategory: TTabSheet
              Caption = 'Category'
              ImageIndex = 1
              object tvCategory: TTreeView
                Left = 0
                Top = 0
                Width = 275
                Height = 341
                Align = alClient
                HideSelection = False
                Images = DsgnRes.ilButtons
                Indent = 19
                PopupMenu = pmCategoryActions
                SortType = stText
                TabOrder = 0
                OnChange = tvLibCompChange
                OnCollapsing = tvCategoryCollapsing
                OnCompare = tvCategoryCompare
                OnDblClick = tvLibCompDblClick
                OnDragOver = tvCategoryDragOver
                OnEditing = tvCategoryEditing
                OnEndDrag = tvCategoryEndDrag
                OnExpanding = tvCategoryExpanding
              end
            end
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 372
          Width = 283
          Height = 189
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object pnlCompProp: TPanel
            Left = 0
            Top = 0
            Width = 283
            Height = 189
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            DesignSize = (
              283
              189)
            object Label4: TLabel
              Left = 3
              Top = 6
              Width = 28
              Height = 13
              Caption = 'Name'
            end
            object Label5: TLabel
              Left = 3
              Top = 32
              Width = 39
              Height = 13
              Caption = 'Class ID'
            end
            object edNameComp: TEdit
              Left = 48
              Top = 4
              Width = 235
              Height = 21
              Anchors = [akLeft, akTop, akRight]
              TabOrder = 0
              OnExit = edNameCompExit
            end
            object edClassIDComp: TEdit
              Left = 48
              Top = 30
              Width = 153
              Height = 21
              Anchors = [akLeft, akTop, akRight]
              TabOrder = 1
              OnExit = edNameCompExit
            end
            object memDescrComp: TMemo
              Left = 0
              Top = 55
              Width = 283
              Height = 134
              Anchors = [akLeft, akTop, akRight, akBottom]
              ScrollBars = ssBoth
              TabOrder = 2
              WordWrap = False
            end
            object btnLoadPicture: TButton
              Left = 205
              Top = 30
              Width = 78
              Height = 21
              Anchors = [akTop, akRight]
              Caption = 'Load Picture'
              TabOrder = 3
              OnClick = btnLoadPictureClick
            end
          end
        end
      end
      inline CompToolsBar: TrwLibToolsBar
        Left = 0
        Top = 0
        Width = 916
        Height = 78
        Align = alTop
        AutoScroll = False
        AutoSize = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        inherited ctbBar: TControlBar
          Width = 916
          Height = 78
          inherited tlbComponents: TToolBar
            Left = 12
            Top = 54
          end
          inherited tlbFont: TToolBar
            Left = 11
            Width = 471
          end
          inherited tlbExperts: TToolBar
            Left = 602
            inherited btnASCIIOrder: TToolButton
              Visible = False
            end
          end
          inherited tlbActions: TToolBar
            Left = 502
          end
        end
        inherited pmWizards: TPopupMenu
          Left = 856
        end
      end
    end
    object tsFunct: TTabSheet
      Caption = 'Functions'
      OnShow = tsFunctShow
      object Splitter1: TSplitter
        Left = 276
        Top = 0
        Height = 639
      end
      object Panel1: TPanel
        Left = 0
        Top = 639
        Width = 916
        Height = 31
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object lChangeFunct: TLabel
          Left = 0
          Top = 11
          Width = 66
          Height = 13
          Caption = 'lChangeComp'
        end
        object Panel4: TPanel
          Left = 369
          Top = 0
          Width = 547
          Height = 31
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object btnAdd: TButton
            Left = 2
            Top = 4
            Width = 75
            Height = 25
            Action = actAddFunc
            TabOrder = 0
          end
          object btnEdit: TButton
            Left = 87
            Top = 4
            Width = 75
            Height = 25
            Action = actEditFunc
            TabOrder = 1
          end
          object btnDel: TButton
            Left = 171
            Top = 4
            Width = 75
            Height = 25
            Action = actDelFunc
            TabOrder = 2
          end
          object btnSave: TButton
            Left = 277
            Top = 4
            Width = 75
            Height = 25
            Action = actSaveFunc
            TabOrder = 3
          end
          object btnCancel: TButton
            Left = 362
            Top = 4
            Width = 75
            Height = 25
            Action = actCancelFunc
            TabOrder = 4
          end
          object btnCompileFnct: TButton
            Left = 468
            Top = 4
            Width = 75
            Height = 25
            Action = actCompile
            TabOrder = 5
          end
        end
      end
      object pnlFunctList: TPanel
        Left = 0
        Top = 0
        Width = 276
        Height = 639
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label1: TLabel
          Left = 0
          Top = 0
          Width = 276
          Height = 16
          Align = alTop
          AutoSize = False
          Caption = ' List of Functions'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object tvFunct: TTreeView
          Left = 0
          Top = 16
          Width = 276
          Height = 623
          Align = alClient
          HideSelection = False
          Images = DsgnRes.ilButtons
          Indent = 19
          PopupMenu = pmFunctions
          TabOrder = 0
          OnChange = tvFunctChange
          OnCollapsing = tvCategoryCollapsing
          OnCompare = tvCategoryCompare
          OnDragOver = tvFunctDragOver
          OnEditing = tvFunctEditing
          OnEndDrag = tvFunctEndDrag
          OnExpanding = tvCategoryExpanding
        end
      end
      object Panel3: TPanel
        Left = 279
        Top = 0
        Width = 637
        Height = 639
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        DesignSize = (
          637
          639)
        object Label3: TLabel
          Left = 0
          Top = 0
          Width = 637
          Height = 17
          Align = alTop
          AutoSize = False
          Caption = 'Text of Function'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Splitter4: TSplitter
          Left = 0
          Top = 406
          Width = 637
          Height = 3
          Cursor = crVSplit
          Align = alBottom
        end
        object chbShowInExprEditor: TCheckBox
          Left = 336
          Top = 0
          Width = 163
          Height = 17
          Anchors = [akTop, akRight]
          Caption = 'Available In Expression Editor'
          TabOrder = 0
          OnClick = chbShowInExprEditorClick
        end
        object chbDataFilter: TCheckBox
          Left = 519
          Top = 0
          Width = 118
          Height = 17
          Anchors = [akTop, akRight]
          Caption = 'Data Filter Function'
          TabOrder = 1
          OnClick = chbDataFilterClick
        end
        object pcFunctProp: TPageControl
          Left = 0
          Top = 409
          Width = 637
          Height = 230
          ActivePage = TabSheet1
          Align = alBottom
          TabOrder = 2
          object TabSheet1: TTabSheet
            Caption = 'Function Info'
            DesignSize = (
              629
              202)
            object Label2: TLabel
              Left = 8
              Top = 14
              Width = 81
              Height = 13
              Caption = 'Short Description'
            end
            object Label6: TLabel
              Left = 8
              Top = 48
              Width = 28
              Height = 13
              Caption = 'Notes'
            end
            object edDescription: TEdit
              Left = 103
              Top = 11
              Width = 314
              Height = 21
              TabOrder = 0
              OnExit = edDescriptionExit
            end
            object memNotes: TMemo
              Left = 104
              Top = 48
              Width = 519
              Height = 147
              Anchors = [akLeft, akTop, akRight, akBottom]
              TabOrder = 1
              OnExit = memNotesExit
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Parameters Domain'
            ImageIndex = 1
            inline frmDomains: TrwFunctParamDomain
              Left = 0
              Top = 0
              Width = 629
              Height = 202
              Align = alClient
              AutoScroll = False
              TabOrder = 0
              inherited Label1: TLabel
                Height = 202
              end
              inherited lvParams: TListView
                Height = 202
              end
              inherited Panel1: TPanel
                Width = 398
                Height = 202
                inherited vleValues: TValueListEditor
                  Width = 398
                  Height = 133
                  ColWidths = (
                    223
                    169)
                end
                inherited Panel2: TPanel
                  Top = 133
                  Width = 398
                end
              end
            end
          end
        end
      end
    end
  end
  object aclDesignerActions: TActionList
    Images = DsgnRes.ilButtons
    Left = 496
    Top = 112
    object actUndo: TAction
      Category = 'Edit'
      Caption = 'Undo'
      ImageIndex = 80
      OnExecute = actUndoExecute
    end
    object actDelSelection: TAction
      Category = 'Edit'
      Caption = 'Delete Selection'
      ImageIndex = 79
      ShortCut = 16430
      OnExecute = actDelSelectionExecute
    end
    object actCopy: TAction
      Category = 'Edit'
      Caption = 'Copy'
      ImageIndex = 77
      ShortCut = 16451
      OnExecute = actCopyExecute
    end
    object actCut: TAction
      Category = 'Edit'
      Caption = 'Cut'
      ImageIndex = 76
      ShortCut = 16472
      OnExecute = actCutExecute
    end
    object actPaste: TAction
      Category = 'Edit'
      Caption = 'Paste'
      ImageIndex = 78
      ShortCut = 16470
      OnExecute = actPasteExecute
    end
    object actExportComp: TAction
      Category = 'Components'
      Caption = 'Export Component to File...'
      OnExecute = actExportCompExecute
    end
    object actImportComp: TAction
      Category = 'Components'
      Caption = 'Import Component from File...'
      OnExecute = actImportCompExecute
    end
    object actAddComp: TAction
      Category = 'Components'
      Caption = 'Add'
      OnExecute = actAddCompExecute
    end
    object actEditComp: TAction
      Category = 'Components'
      Caption = 'Edit'
      OnExecute = actEditCompExecute
    end
    object actDelComp: TAction
      Category = 'Components'
      Caption = 'Delete'
      OnExecute = actDelCompExecute
    end
    object actSaveComp: TAction
      Category = 'Components'
      Caption = 'Save'
      OnExecute = actSaveCompExecute
    end
    object actCancelComp: TAction
      Category = 'Components'
      Caption = 'Cancel'
      OnExecute = actCancelCompExecute
    end
    object actAddCategory: TAction
      Category = 'Components'
      Caption = 'Add Category'
      OnExecute = actAddCategoryExecute
    end
    object actDelCategory: TAction
      Category = 'Components'
      Caption = 'Delete Category'
      OnExecute = actDelCategoryExecute
    end
    object actAddFunctFolder: TAction
      Category = 'Functions'
      Caption = 'Add Folder'
      OnExecute = actAddFunctFolderExecute
    end
    object actDelFunctFolder: TAction
      Category = 'Functions'
      Caption = 'Delete Folder'
      OnExecute = actDelFunctFolderExecute
    end
    object actAddFunc: TAction
      Category = 'Functions'
      Caption = 'Add'
      OnExecute = actAddFuncExecute
    end
    object actDelFunc: TAction
      Category = 'Functions'
      Caption = 'Delete'
      OnExecute = actDelFuncExecute
    end
    object actEditFunc: TAction
      Category = 'Functions'
      Caption = 'Edit'
      OnExecute = actEditFuncExecute
    end
    object actSaveFunc: TAction
      Category = 'Functions'
      Caption = 'Save'
      OnExecute = actSaveFuncExecute
    end
    object actCancelFunc: TAction
      Category = 'Functions'
      Caption = 'Cancel'
      OnExecute = actCancelFuncExecute
    end
    object actCompile: TAction
      Caption = 'Compile'
      ImageIndex = 60
      ShortCut = 16504
      OnExecute = actCompileExecute
    end
    object actRefresh: TAction
      Caption = 'Refresh Library'
      OnExecute = actRefreshExecute
    end
    object actExpLibrary: TAction
      Caption = 'Export Library to File...'
      ImageIndex = 83
      OnExecute = actExpLibraryExecute
    end
    object actImpLibrary: TAction
      Caption = 'Import Library from File...'
      ImageIndex = 82
      OnExecute = actImpLibraryExecute
    end
  end
  object pmComp: TPopupMenu
    Images = DsgnRes.ilPict
    Left = 436
    Top = 112
    object Add1: TMenuItem
      Action = actAddComp
    end
    object Edit1: TMenuItem
      Action = actEditComp
    end
    object Delete1: TMenuItem
      Action = actDelComp
    end
    object Save1: TMenuItem
      Action = actSaveComp
    end
    object Cancel1: TMenuItem
      Action = actCancelComp
    end
    object pmCopyComp: TMenuItem
      Caption = 'Copy Component'
      OnClick = pmCopyCompClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Copy1: TMenuItem
      Action = actCopy
    end
    object Cut1: TMenuItem
      Action = actCut
    end
    object Paste1: TMenuItem
      Action = actPaste
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Delete2: TMenuItem
      Action = actDelSelection
    end
    object Undo1: TMenuItem
      Action = actUndo
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object ExporttoFile1: TMenuItem
      Action = actExportComp
    end
    object ImportfromFile1: TMenuItem
      Action = actImportComp
    end
    object ExportLibrary1: TMenuItem
      Action = actExpLibrary
    end
    object ImportLibrary1: TMenuItem
      Action = actImpLibrary
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Compile2: TMenuItem
      Action = actCompile
    end
    object RefreshLibrary1: TMenuItem
      Action = actRefresh
    end
  end
  object opdImport: TOpenDialog
    DefaultExt = 'txt'
    Filter = 
      'ReportWriter Component Files (*.rwc)|*.rwc|Text Files (*.txt)|*.' +
      'txt|All Files (*.*)|*.*'
    Title = 'Import Component'
    Left = 444
    Top = 160
  end
  object svdExport: TSaveDialog
    DefaultExt = 'txt'
    Filter = 
      'ReportWriter Component Files (*.rwc)|*.rwc|Text Files (*.txt)|*.' +
      'txt|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Export Component'
    Left = 492
    Top = 160
  end
  object pmCategoryActions: TPopupMenu
    Left = 798
    Top = 172
    object AddCategory1: TMenuItem
      Action = actAddCategory
    end
    object DeleteCategory1: TMenuItem
      Action = actDelCategory
    end
  end
  object pmFunctions: TPopupMenu
    Images = DsgnRes.ilPict
    Left = 86
    Top = 252
    object Add2: TMenuItem
      Action = actAddFunc
    end
    object Delete3: TMenuItem
      Action = actDelFunc
    end
    object Edit2: TMenuItem
      Action = actEditFunc
    end
    object Save2: TMenuItem
      Action = actSaveFunc
    end
    object Cancel2: TMenuItem
      Action = actCancelFunc
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object AddFolder1: TMenuItem
      Action = actAddFunctFolder
    end
    object DeleteFolder1: TMenuItem
      Action = actDelFunctFolder
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object ExportLibrary2: TMenuItem
      Action = actExpLibrary
    end
    object ImportLibrary2: TMenuItem
      Action = actImpLibrary
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object Compile1: TMenuItem
      Action = actCompile
    end
    object RefreshLibrary2: TMenuItem
      Action = actRefresh
    end
  end
  object opdImportLib: TOpenDialog
    DefaultExt = 'rwl'
    Filter = 'ReportWriter Library Files (*.rwl)|*.rwl|All Files (*.*)|*.*'
    Title = 'Import Library'
    Left = 444
    Top = 200
  end
  object svdExportLib: TSaveDialog
    DefaultExt = 'rwl'
    Filter = 'ReportWriter Library Files (*.rwl)|*.rwl|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Export Library'
    Left = 492
    Top = 200
  end
  object opdPicture: TOpenPictureDialog
    Filter = 'All (*.bmp)|*.bmp|Bitmaps (*.bmp)|*.bmp'
    Title = 'Component Picture'
    Left = 905
    Top = 478
  end
end
