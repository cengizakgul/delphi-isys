unit rwDesignCompAreaFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwDesignAreaFrm, ExtCtrls, rwBasicClasses, rwCommonClasses, Menus,
  rwLibCompChoiceFrm, rwReport;

type
  TrwDesignCompArea = class(TrwDesignArea)
  private
    FComponent: TrwComponent;
    procedure SetComponent(Value: TrwComponent);

  protected
    function SelectLibComp: TrwLibComponent; override;  

  public
    constructor Create(AOwner: TComponent); override;

    property Component: TrwComponent read FComponent write SetComponent;
    procedure DestroySelectedComponents; override;
  end;

implementation

{$R *.DFM}


procedure TrwDesignCompArea.SetComponent(Value: TrwComponent);
begin
  if Assigned(FComponent) then
  begin
    FComponent.DesignParent := nil;
    ComponentsOwner := nil;
  end;

  FComponent := Value;

  if Assigned(FComponent) then
  begin
    FComponent.DesignParent := pnlArea;
    ComponentsOwner := FComponent;
  end
end;

procedure TrwDesignCompArea.DestroySelectedComponents;
var
  lComp: TrwComponent;
  fl: Boolean;
begin
  inherited;

  fl := False;
  while SelectedCompList.Count > 0 do
  begin
    lComp := SelectedCompList[0];
    BeforeDestroyingComponent(lComp);
    fl := fl or (lComp is TrwPrintableContainer);
    lComp.Free;
  end;

  if fl and Assigned(AfterDestroyContainer) then
    AfterDestroyContainer(Self);
end;


function TrwDesignCompArea.SelectLibComp: TrwLibComponent;
var
  t:  TrwChoiceLibCompTypes;
begin
  Result := nil;

  if FComponent is TrwFormControl then
    t := [rctInputFormComp, rctNoVisualComp]
  else if FComponent is TrwPrintableControl then
    t := [rctReportComp, rctNoVisualComp]
  else if FComponent is TrwReport then
    Exit
  else
    t := [rctNoVisualComp];

  Result := ChoiceLibComp(t);
end;

constructor TrwDesignCompArea.Create(AOwner: TComponent);
begin
  inherited;
  pnlArea.Transparent := False;
  pnlArea.BevelOuter := bvNone;
  pnlArea.BevelInner := bvNone;
  pnlArea.BorderStyle := bsNone;
  pnlArea.Align := alClient;
  pnlArea.Color := clAppWorkSpace;
end;

end.
