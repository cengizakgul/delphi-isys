inherited rmDsgnApartment: TrmDsgnApartment
  object spltLeft: TSplitter
    Left = 50
    Top = 0
    Height = 222
    AutoSnap = False
    Color = clBtnFace
    ParentColor = False
    Visible = False
    OnCanResize = spltLeftCanResize
    OnMoved = spltLeftMoved
  end
  object spltRight: TSplitter
    Left = 440
    Top = 0
    Height = 222
    Align = alRight
    AutoSnap = False
    Color = clBtnFace
    ParentColor = False
    Visible = False
    OnCanResize = spltRightCanResize
    OnMoved = spltRightMoved
  end
  object spltBottom: TSplitter
    Left = 0
    Top = 222
    Width = 493
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    AutoSnap = False
    Color = clBtnFace
    MinSize = 20
    ParentColor = False
    Visible = False
    OnCanResize = spltBottomCanResize
    OnMoved = spltBottomMoved
  end
  object pnlDockBottom: TPanel
    Left = 0
    Top = 225
    Width = 493
    Height = 50
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    DockSite = True
    TabOrder = 0
    OnDockDrop = pnlDockBottomDockDrop
    OnDockOver = pnlDockBottomDockOver
    OnUnDock = pnlDockBottomUnDock
  end
  object pnlDockLeft: TPanel
    Left = 0
    Top = 0
    Width = 50
    Height = 222
    Align = alLeft
    AutoSize = True
    BevelOuter = bvNone
    DockSite = True
    TabOrder = 1
    OnDockDrop = pnlDockBottomDockDrop
    OnDockOver = pnlDockLeftDockOver
    OnUnDock = pnlDockBottomUnDock
  end
  object pnlDockRight: TPanel
    Left = 443
    Top = 0
    Width = 50
    Height = 222
    Align = alRight
    AutoSize = True
    BevelOuter = bvNone
    DockSite = True
    TabOrder = 2
    OnDockDrop = pnlDockBottomDockDrop
    OnDockOver = pnlDockRightDockOver
    OnUnDock = pnlDockBottomUnDock
  end
end
