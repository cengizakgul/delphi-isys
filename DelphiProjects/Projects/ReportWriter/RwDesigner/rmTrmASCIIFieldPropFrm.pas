unit rmTrmASCIIFieldPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, rmOPEValueFrm, StdCtrls,
  ExtCtrls, rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, ComCtrls,
  rmOPENumericFrm, rmTypes, rmOPETextFilterFrm;

type
  TrmTrmASCIIFieldProp = class(TrmOPBasic)
    grbValue: TGroupBox;
    frmValue: TrmOPEValue;
    frmSize: TrmOPENumeric;
    frmTextFilter: TrmOPETextFilter;
  private
  public
    procedure GetPropsFromObject; override;
  end;

implementation

{$R *.dfm}

{ TrmTrmASCIIFieldProp }

procedure TrmTrmASCIIFieldProp.GetPropsFromObject;
begin
  inherited;
  frmValue.PropertyName := 'Value';
  frmValue.ShowPropValue;
  frmSize.PropertyName := 'Size';
  frmSize.ShowPropValue;

  if TrmASCIIResultType(Integer((RWObject as IrmASCIIItem).GetASCIIForm.GetPropertyValue('ResultType'))) = rmARTFixedLength then
   frmSize.Enabled := True
  else
   frmSize.Enabled := False;
end;

end.
