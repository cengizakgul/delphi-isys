unit rwReportFormToolsBarFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwCommonToolsBarFrm, ImgList, ComCtrls, StdCtrls, ToolWin, ExtCtrls, rwBasicClasses,
  rwTypes, Menus, rwReport, rwPrinters, rwWizardInfo, EvStreamUtils;

type

  {TrwReportFormToolsBar is ToolBar for designing report form}

  TrwReportFormToolsBar = class(TrwCommonToolsBar)
    tbtLabel: TToolButton;
    tbtMemo: TToolButton;
    tbtRichText: TToolButton;
    tbtDBText: TToolButton;
    tbtSysLabel: TToolButton;
    tbtShape: TToolButton;
    tbtContainer: TToolButton;
    tbtFrame: TToolButton;
    tbtSubReport: TToolButton;
    tbtImage: TToolButton;
    tbtPCLLabel: TToolButton;
    tbtDBImage: TToolButton;
    tbtLibComp: TToolButton;
    btnPrintOrder: TToolButton;
    ToolButton15: TToolButton;
    btnASCIIOrder: TToolButton;
    btnWizards: TToolButton;
    pmWizards: TPopupMenu;
    miEmptyWiz: TMenuItem;
    tbtBarcode1D: TToolButton;
    tbtPDF417: TToolButton;
    procedure btnPrintOrderClick(Sender: TObject);
    procedure btnASCIIOrderClick(Sender: TObject);
    procedure pmWizardsPopup(Sender: TObject);

  private
    procedure OnClickWizardsMenu(Sender: TObject);

  protected
    procedure InitFontList; override;

  public
    procedure AssignImageForComponent(const AComponentClassName: string; AImage: TBitmap); override;
  end;

implementation

uses rwDsgnRes, rwDesignerFrm;

{$R *.DFM}


procedure TrwReportFormToolsBar.AssignImageForComponent(const AComponentClassName: string; AImage: TBitmap);
begin
  inherited;
  if AImage.Empty then
    DsgnRes.ilButtons.GetBitmap(tbtLibComp.ImageIndex, AImage);
end;

procedure TrwReportFormToolsBar.btnPrintOrderClick(Sender: TObject);
begin
  if Assigned(OnClickExpertToolBar) then
    OnClickExpertToolBar(Self, etePrintOrder);
end;


procedure TrwReportFormToolsBar.InitFontList;
begin
  inherited;
  cbFontName.Items.Assign(rwPrinter.Fonts);
end;

procedure TrwReportFormToolsBar.btnASCIIOrderClick(Sender: TObject);
begin
  if Assigned(OnClickExpertToolBar) then
    OnClickExpertToolBar(Self, eteASCIIPrintOrder);
end;

procedure TrwReportFormToolsBar.OnClickWizardsMenu(Sender: TObject);
var
  MS: TMemoryStream;

  function _RunWizard: Boolean;
  begin
    with TrwReport(ReportDesigner.DesignPaperReport[0].Report) do
    begin
      WizardInfo.Position := 0;
      MS.CopyFrom(WizardInfo.RealStream, WizardInfo.Size);
    end;  
    Result := RunWizard(TMenuItem(Sender).Tag, MS);
  end;

begin
  MS := TIsMemoryStream.Create;
  try
    if _RunWizard and (MS.Size > 0) and Assigned(OnClickExpertToolBar) then
      OnClickExpertToolBar(Self, eteReportWizard, MS);
  finally
    MS.Free;
  end;
end;

procedure TrwReportFormToolsBar.pmWizardsPopup(Sender: TObject);
var
  i: Integer;
  mItm: TMenuItem;
  WzList: TStringList;
begin
  if pmWizards.Tag = 1 then Exit;

  miEmptyWiz.Visible := False;
  WzList := TStringList.Create;
  try
    WzList.Text := WizardsList;
    for i := 0 to WzList.Count-1 do
    begin
      mItm := TMenuItem.Create(pmWizards);
      mItm.Caption := WzList[i];
      mItm.Tag := i;
      mItm.OnClick := OnClickWizardsMenu;
      pmWizards.Items.Add(mItm);
    end;
    pmWizards.Tag := 1;

  finally
    FreeAndNil(WzList);
  end;
end;

end.
