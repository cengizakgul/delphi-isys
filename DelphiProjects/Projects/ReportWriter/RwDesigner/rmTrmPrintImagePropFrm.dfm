inherited rmTrmPrintImageProp: TrmTrmPrintImageProp
  Height = 293
  inherited pcCategories: TPageControl
    Height = 293
    inherited tsBasic: TTabSheet
      inline rmOPEPicture1: TrmOPEPicture
        Left = 8
        Top = 91
        Width = 290
        Height = 166
        AutoScroll = False
        AutoSize = True
        TabOrder = 2
        inherited Panel1: TPanel
          Left = 92
          Width = 198
          Height = 166
          inherited imPicture: TImage
            Width = 196
            Height = 164
          end
        end
      end
    end
    inherited tcAppearance: TTabSheet
      inline frmTransparent: TrmOPEBoolean
        Left = 8
        Top = 35
        Width = 83
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
        inherited chbProp: TCheckBox
          Width = 83
          Caption = 'Transparent'
        end
      end
      inline frmStretch: TrmOPEBoolean
        Left = 8
        Top = 10
        Width = 62
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited chbProp: TCheckBox
          Width = 62
          Caption = 'Stretch'
        end
      end
    end
    inherited tcPrint: TTabSheet
      inherited frmCutable: TrmOPEBoolean
        Top = 185
        Visible = False
      end
      inherited frmPrintOnEachPage: TrmOPEBoolean
        Top = 10
      end
      inherited frmBlockParentIfEmpty: TrmOPEBoolean
        Top = 35
      end
      inherited pnlBlocking: TPanel
        Top = 68
      end
    end
  end
end
