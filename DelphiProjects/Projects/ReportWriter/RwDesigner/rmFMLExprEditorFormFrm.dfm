inherited rmFMLExprEditorForm: TrmFMLExprEditorForm
  Left = 451
  Top = 299
  Width = 527
  Height = 374
  Caption = 'Formula Editor'
  Constraints.MinHeight = 328
  Constraints.MinWidth = 430
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inline rmExprEditor: TrmFMLExprEditor
    Left = 0
    Top = 0
    Width = 519
    Height = 313
    Align = alClient
    AutoScroll = False
    TabOrder = 1
    inherited Panel1: TPanel
      Width = 464
      Height = 313
      inherited Splitter1: TSplitter
        Width = 464
      end
      inherited pcItem: TPageControl
        Width = 464
        Height = 199
        inherited tsObject: TTabSheet
          inherited tvObjects: TTreeView
            Width = 456
            Height = 141
          end
          inherited pnlObjProperty: TPanel
            Width = 456
          end
        end
        inherited tsValue: TTabSheet
          inherited pnlVal: TPanel
            Width = 278
          end
        end
        inherited tsFunction: TTabSheet
          inherited Panel2: TPanel
            inherited lFuncDecl: TLabel
              Width = 395
            end
          end
        end
      end
      inherited grbExpression: TGroupBox
        Width = 464
      end
      inherited pnlSpace1: TPanel
        Width = 464
      end
    end
    inherited Panel4: TPanel
      Left = 464
      Height = 313
    end
  end
  object pnlButtons: TPanel
    Left = 0
    Top = 313
    Width = 519
    Height = 34
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      519
      34)
    object btnClear: TButton
      Left = 4
      Top = 6
      Width = 75
      Height = 22
      Anchors = [akLeft, akBottom]
      Caption = 'Clear'
      TabOrder = 0
      OnClick = btnClearClick
    end
    object btnCheck: TButton
      Left = 88
      Top = 6
      Width = 75
      Height = 22
      Anchors = [akLeft, akBottom]
      Caption = 'Check Errors'
      TabOrder = 1
      OnClick = btnCheckClick
    end
    object btnOK: TButton
      Left = 441
      Top = 7
      Width = 75
      Height = 22
      Anchors = [akTop, akRight]
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 2
      OnClick = btnOKClick
    end
    object btnCancel: TButton
      Left = 357
      Top = 7
      Width = 75
      Height = 22
      Anchors = [akTop, akRight]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 3
      OnClick = btnCancelClick
    end
  end
end
