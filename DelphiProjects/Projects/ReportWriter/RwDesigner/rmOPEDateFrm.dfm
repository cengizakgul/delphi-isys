inherited rmOPEDate: TrmOPEDate
  Width = 208
  Height = 21
  AutoSize = True
  OnResize = FrameResize
  object lPropName: TLabel
    Left = 0
    Top = 4
    Width = 23
    Height = 13
    Caption = 'Date'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object deDate: TDateTimePicker
    Left = 55
    Top = 0
    Width = 153
    Height = 21
    Date = 38144.963744386570000000
    Time = 38144.963744386570000000
    TabOrder = 0
    OnChange = deDateChange
  end
end
