unit rwDesignerApp;

interface

uses
  Windows, Classes, Messages, SysUtils,
  isLocale, rwEngineTypes, EvStreamUtils, Controls, StdCtrls, Forms, Dialogs, Variants,
  Menus, CoolTrayIcon, rwBasicUtils, ISErrorUtils, ISUtils, rwAdapterUtils,
  rwDesigner, rwEngine, isBasicUtils, rwConnectionInt, isBaseClasses, isExceptions;

type
  TrwDesignerApp = class(TisInterfacedObject, IrwDesignerApp)
  private
    FAdapterModule: THandle;
    FAdapter: IrwAppAdapter;
    FDesignerFormHolder: IrwDesignerForm;
    FQueryBuilderFormHolder: IrwDesignerForm;
    procedure ShowDesignException(Sender: TObject; E: Exception);

    procedure LoadAdapter;
    procedure UnloadAdapter;

    function  OpenDesigner(const AReportData: IevDualStream; const AReportName: String;
                           const ADefaultReportType: Integer): Boolean;
    function  OpenQueryBuilder(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
    procedure InitializeAppAdapter(const AModuleName: String; const AAppCustomData: Variant);
    function  CallAppAdapterFunction(const AFunctionName: String; const AParams: Variant): Variant;
    function  RunQuery(const AQueryData: IevDualStream; const AParams: Variant): Variant;
    function  QueryInfo(const AItemName: String): Variant;
    function  OpenDesigner2(const AReportData: IevDualStream; const AReportName: String;
                            const ADefaultReportType: Integer): Boolean;
    function  IsDesignerFormClosed(out AReportData: IevDualStream): Boolean;
    function  OpenQueryBuilder2(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
    function  IsQueryBuilderFormClosed(out AQueryData: IevDualStream): Boolean;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    procedure Terminate;    
  end;

  TrwDesignerAppFrm = class(TForm)
    TrayIcon: TCoolTrayIcon;
    PopupMenu: TPopupMenu;
    miTerminate: TMenuItem;
    procedure miTerminateClick(Sender: TObject);
  public
    procedure UpdateTrayHint;
  end;

  procedure DesignerApp;

var rwDesignerAppFrm: TrwDesignerAppFrm;

implementation

{$R *.dfm}

uses rwAppSelectionFrm, ISDataAccessComponents, rwUtils, rwDsgnUtils;


var
  Designer: IRWDesignerApp = nil;

procedure ShowReport(const FileName: String; const DefaultReportType: Integer);
var
  RepData: IEvDualStream;
  WorkData: IEvDualStream;
  WorkFile: String;
begin
  if FileName <> '' then
  begin
    WorkFile := GetUniqueFileName(GetTmpDir);
    WorkData := TEvDualStreamHolder.CreateFromFile(WorkFile, True, False);
    RepData := TEvDualStreamHolder.CreateFromFile(FileName, False, False);
    WorkData.CopyFrom(RepData, 0);
    RepData := nil;
    WorkData.Position := 0;
  end

  else
  begin
    WorkData := TEvDualStreamHolder.Create;
    RepData := nil;
  end;

  Designer.OpenDesigner(WorkData, FileName, DefaultReportType);

  if FileName <> '' then
  begin
    if AreYouSure('Do you want to save changes back into "' + FileName + '"?') then
      WorkData.SaveToFile(FileName);
  end;
end;


procedure ShowQuery(const FileName: String);
var
  QueryData: IEvDualStream;
begin
  if FileName <> '' then
    QueryData := TEvDualStreamHolder.CreateFromFile(FileName, False, False)
  else
    QueryData := TEvDualStreamHolder.Create;

  Designer.OpenQueryBuilder(QueryData, False);
  QueryData.Position := 0;
end;


procedure DesignerApp;
var
  Mode: Integer;
  sAdapterName: String;
begin
  Designer := TrwDesignerApp.Create;
  try
    try
      Mode := ChooseAdapterFor(ParamStr(1), sAdapterName);

      if Mode > 0 then
      begin
        Designer.InitializeAppAdapter(sAdapterName, varEmpty);

        if Mode = 1 then
          ShowReport(ParamStr(1), 0)

        else if Mode = 2 then
          ShowReport(ParamStr(1), 1)

        else if Mode = 3 then
          ShowQuery(ParamStr(1))
      end;
    except
      on E: Exception do ApplicationShowException(E);
    end;
  finally
    Designer := nil;
  end;
end;


{ TrwDesignerApp }
procedure TrwDesignerApp.AfterConstruction;
begin
  Application.OnException := ShowDesignException;
  inherited;
end;

procedure TrwDesignerApp.BeforeDestruction;
begin
  UnloadAdapter;
  inherited;
end;

function TrwDesignerApp.CallAppAdapterFunction(const AFunctionName: String; const AParams: Variant): Variant;
begin
  Result := RWEngineExt.AppAdapter.DispatchCustomFunction(AFunctionName, AParams);
end;

procedure TrwDesignerApp.InitializeAppAdapter(const AModuleName: String; const AAppCustomData: Variant);
begin
  // Allow initialization only one time!
  if AppAdapterModuleName = '' then
  begin
    AppAdapterModuleName := AModuleName;
    AppAdapterAppCustomData := AAppCustomData;
    LoadAdapter;
  end;
end;

function TrwDesignerApp.IsDesignerFormClosed(out AReportData: IevDualStream): Boolean;
begin
  Result := True;
  AReportData := nil;

  if Assigned(FDesignerFormHolder) then
    if FDesignerFormHolder.IsClosed then
    begin
      AReportData := FDesignerFormHolder.GetData;
      FDesignerFormHolder := nil;
    end
    else
      Result := False;
end;

function TrwDesignerApp.IsQueryBuilderFormClosed(out AQueryData: IevDualStream): Boolean;
begin
  Result := True;
  AQueryData := nil;

  if Assigned(FQueryBuilderFormHolder) then
    if FQueryBuilderFormHolder.IsClosed then
    begin
      AQueryData := FQueryBuilderFormHolder.GetData;
      FQueryBuilderFormHolder := nil;
    end
    else
      Result := False;
end;

procedure TrwDesignerApp.LoadAdapter;
var
  GetRWAppAdapterProc: TGetRWAppAdapterProc;
  RWInitializeProc: TrwInitializeProc;
begin
  if AppAdapterModuleName = '' then
    raise ErwException.Create('AppAdapter is not defined');
  try
    FAdapterModule := LoadLibrary(PChar(AppAdapterModuleName));
  except
    on E: Exception do
    begin
      E.Message := 'Cannot load Application RW Adapter "' + AppAdapterModuleName + '"'#13 + E.Message ;
      raise;
    end;
  end;

  if FAdapterModule <> 0 then
  begin
    RWInitializeProc := GetProcAddress(FAdapterModule, cInitialize);
    if not Assigned(RWInitializeProc) then
      raise ErwException.Create('Module "' + AppAdapterModuleName + '" does not contain an exported function "' + cInitialize + '"');
    try
      RWInitializeProc;
    except
      on E: Exception do
      begin
        E.Message := 'Cannot initialize Application RW Adapter "' + AppAdapterModuleName + '"'#13 + E.Message ;
        raise;
      end;
    end;

    AppAdapterInfo := GetAppAdapterInfo(AppAdapterModuleName);

    if Assigned(rwDesignerAppFrm) then
      rwDesignerAppFrm.UpdateTrayHint;

    GetRWAppAdapterProc := GetProcAddress(FAdapterModule, cReportWriterAppAdapter);
    if not Assigned(GetRWAppAdapterProc) then
      raise ErwException.Create('Module "' + AppAdapterModuleName + '" does not contain an exported function "' + cReportWriterAppAdapter + '"');

    FAdapter := GetRWAppAdapterProc;
    InitializeRWEngine(FAdapter);
    InitializeRWDesigner;
  end

  else
    raise ErwException.Create('Cannot load Application RW Adapter "' + AppAdapterModuleName + '"');
end;

function TrwDesignerApp.OpenDesigner(const AReportData: IevDualStream; const AReportName: String;
  const ADefaultReportType: Integer): Boolean;
var
  ClName: String;
begin
  if ADefaultReportType = 0 then
    ClName := 'TrwReport'
  else
    ClName := 'TrmReport';

  if Assigned(AReportData) then
  begin
    RWDesignerExt.ShowDesigner(AReportData, AReportName, ClName);
  end
  else
    RWDesignerExt.ShowDesigner(nil, AReportName, ClName);

  Result := True;
end;


function TrwDesignerApp.OpenDesigner2(const AReportData: IevDualStream; const AReportName: String;
  const ADefaultReportType: Integer): Boolean;
var
  ClName: String;
begin
  if ADefaultReportType = 0 then
    ClName := 'TrwReport'
  else
    ClName := 'TrmReport';

  FDesignerFormHolder := RWDesignerExt.ShowDesigner2(AReportData, AReportName, ClName);
  Result := Assigned(FDesignerFormHolder);
end;

function TrwDesignerApp.OpenQueryBuilder(const AQueryData: IevDualStream;
  const ADescriptionView: Boolean): Boolean;
begin
  if Assigned(AQueryData) then
    Result := RWEngineExt.ShowQueryBuilder(AQueryData.RealStream, ADescriptionView)
  else
    Result := RWEngineExt.ShowQueryBuilder(nil, ADescriptionView);
end;

function TrwDesignerApp.OpenQueryBuilder2(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
var
  QData: IEvDualStream;
begin
  if Assigned(AQueryData) then
    QData := AQueryData
  else
    QData := TEvDualStreamHolder.CreateInMemory;

  FQueryBuilderFormHolder := RWEngineExt.ShowQueryBuilder(QData, ADescriptionView);
  Result := Assigned(FQueryBuilderFormHolder);
end;

function TrwDesignerApp.QueryInfo(const AItemName: String): Variant;

  procedure AddValue(const AItem, AValue: String);
  begin
    if Result <> '' then
      Result := Result + #13;
    Result := Result + AItem + '=' + AValue;
  end;

begin
  Result := '';

  if AnsiSameText(AItemName, 'Version') then
    AddValue('Version', GetFileVersion(GetModuleName(0)));

  if AnsiSameText(AItemName, 'AdapterVersion') and (FAdapterModule <> 0) then
    AddValue('AdapterVersion', GetModuleName(FAdapterModule));
end;


function TrwDesignerApp.RunQuery(const AQueryData: IevDualStream; const AParams: Variant): Variant;
var
  QueryParams: TrwParamList;
  Res: TISBasicClientDataSet;
  n, i, j: Integer;
begin
  Res := TISBasicClientDataSet.Create(nil);
  try
    if VarIsArray(AParams) then
    begin
      n := (VarArrayHighBound(AParams, 1) - VarArrayLowBound(AParams, 1) + 1) div 2;
      Assert((VarArrayHighBound(AParams, 1) - VarArrayLowBound(AParams, 1) + 1) mod 2 = 0);
      SetLength(QueryParams, n);

      for i := Low(QueryParams) to High(QueryParams) do
      begin
        j := i * 2;
        QueryParams[i].Name := AParams[j];
//        QueryParams[i].ParamType := RWTypeToDDType(VariantToInternalType(AParams[j + 1]));     do not need
        QueryParams[i].Value := AParams[j + 1];
      end;
    end

    else
      SetLength(QueryParams, 0);

    RWDesignerExt.RunQBQuery(AQueryData.RealStream, QueryParams, Res);
    Result := Res.Data;
  finally
    FreeAndNil(Res);
  end;
end;

procedure TrwDesignerApp.ShowDesignException(Sender: TObject; E: Exception);
begin
  ISExceptMessage(E, True);
end;

procedure TrwDesignerApp.Terminate;
begin
  if Assigned(FAdapter) then
    FAdapter.Terminate;
end;

procedure TrwDesignerApp.UnloadAdapter;
var
  RWFinalizeProc: TrwFinalizeProc;
begin
  if Assigned(FAdapter) then
  begin
    FAdapter := nil;
    DestroyRWDesigner;
    DestroyRWEngine;
  end;

  if FAdapterModule <> 0 then
  begin
    RWFinalizeProc := GetProcAddress(FAdapterModule, cFinalize);
    if not Assigned(RWFinalizeProc) then
      raise ErwException.Create('Module "' + AppAdapterModuleName + '" does not contain an exported function "' + cFinalize + '"');
    try
      RWFinalizeProc;
    except
      on E: Exception do
      begin
        E.Message := 'Cannot finalize Application RW Adapter "' + AppAdapterModuleName + '"'#13 + E.Message ;
        raise;
      end;
    end;

    AppAdapterModuleName := '';
    AppAdapterAppCustomData := varEmpty;

    FreeLibrary(FAdapterModule);
    FAdapterModule := 0;
  end;

  ZeroMemory(@AppAdapterInfo, SizeOf(AppAdapterInfo));

  if Assigned(rwDesignerAppFrm) and (not Application.Terminated) then
    rwDesignerAppFrm.UpdateTrayHint;
end;

{ TrwDesignerAppFrm }

procedure TrwDesignerAppFrm.miTerminateClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TrwDesignerAppFrm.UpdateTrayHint;
begin
  TrayIcon.Hint := 'Report Writer Designer'#13 +
                   'Application: ' + AppAdapterInfo.ApplicationName + #13 +
                   'Location:    ' + AppAdapterInfo.ModuleName + #13 +
                   'Version:     ' + AppAdapterInfo.Version;
end;

end.
