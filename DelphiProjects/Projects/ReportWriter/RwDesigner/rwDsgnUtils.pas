unit rwDsgnUtils;

interface

uses Windows, SysUtils, ExtCtrls, Classes, Dialogs, mwGeneralSyn, rwTypes, Graphics, rwParser,
     TypInfo, rwVirtualMachine, rwUtils, isRegistryFunctions, sbSQL, Clipbrd, Variants,
     rwBasicClasses, rwMessages, rwEngineTypes, rwBasicUtils, rwQBInfo, rmReport, rmTypes,
     Messages, rmDsgnTypes, Forms, ComCtrls, Controls;

const
  cUnexpectedError = 'Unexpected Error';
  WM_RMD_REFRESH = WM_USER + 100;

type

  TrwMessageTimer = class (TTimer)
  private
    FMessage: string;
    FException: Boolean;
    procedure FOnTimer(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure   ShowMessage(const AMessage: string; const AExceptionType: Boolean = False);
  end;


  TrwFillUpTreeViewCallBack = procedure (ANewNode: TTreeNode; var Accept: Boolean);


  function  GeneralSyn: TmwGeneralSyn;
  procedure DisassembleCompiledCode(const AFileName: string; const ACode: PTrwCode; const ADebInfo: PTrwDebInf = nil; const ABaseAddress: Integer = 0; const AEndAddress: Integer = 0);
  procedure ShowError(AMessage: String);

  procedure rmGetFieldListFromQBQuery(AQuery: TrwQBQuery; AList: TStringList);
  procedure rmQueryBuilderForObject(const AComponent: IrmDesignObject);

  procedure ReadDsgnEnv;
  procedure WriteDsgnEnv;

  procedure FillUpTreeViewCollection(ACollection: IrmLibCollection; ATreeView: TTreeView; ARootNode: TTreeNode; ACallBack: TrwFillUpTreeViewCallBack = nil); overload;
  procedure FillUpTreeViewCollection(AList: TStringList; ATreeView: TTreeView; ARootNode: TTreeNode; ACallBack: TrwFillUpTreeViewCallBack = nil); overload;
  function  AddCollectionItemToTreeView(ACollectionItem: TrwLibCollectionItem; ATreeView: TTreeView; AParentNode: TTreeNode; ACallBack: TrwFillUpTreeViewCallBack = nil): TTreeNode;
  procedure UpdateCollectionGroupInfo(ACollection: TrwLibCollection; ATreeView: TTreeView; ARootNode: TTreeNode);

  function  MakeComboBoxFieldList(AFieldList: TCommaDilimitedString): String;

  function  GetUniqueComponentName(AOwner: TComponent; const APriffix: String): String;

  function AreYouSure(const AQuestion: String = 'Are you sure you want to delete this item?'): Boolean;

  function  GetComponentAppartment(AObject: TComponent): IrmDsgnApartment;
  procedure RegisterTalkativeApartmentMember(const AReceiver: IrmMessageReceiver);
  procedure UnRegisterTalkativeApartmentMember(const AReceiver: IrmMessageReceiver);
  procedure RegisterTalkativeDesignerMember(const AReceiver: IrmMessageReceiver);
  procedure UnRegisterTalkativeDesignerMember(const AReceiver: IrmMessageReceiver);
  function  GetObjectID(AObject: TObject): TrmObjectID;
  procedure SendApartmentMessage(const ASender: IrmMessageReceiver; const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
  procedure SendDesignerMessage(const ASender: IrmMessageReceiver; const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
  procedure SendBroadcastMessage(const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
  procedure PostApartmentMessage(const ASender: IrmMessageReceiver; const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
  procedure PostDesignerMessage(const ASender: IrmMessageReceiver; const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
  procedure PostBroadcastMessage(const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
  function  IsEventQueueEmpty(const ASender: IrmMessageReceiver): Boolean;
  function  ObjectsAreTheSame(const AObject1, AObject2: IrmInterfacedObject): Boolean;

const cAnonymousSender = nil; 

var
  CF_RWDATA: Word;
  CF_FMLEXPRESSION: Word;

  DsgnCodeCompletion: Boolean;
  DsgnCodeParams: Boolean;
  DsgnCodeEvaluation: Boolean;
  DsgnSymbInsight: Boolean;
  DsgnCodeInsightDelay: Integer;
  DsgnTraceLib: Boolean;
  DsgnAutoCompile: Boolean;
  DesignUnits: TrwUnitType;
  DsgnShowData: Boolean;
  DsgnDontAskCompileConfirm: Boolean;

  DesignRMAdvancedMode: Boolean = False;
  DesignRMShowTableGrid: Boolean = False;
  DesignRMShowPaperGrid: Boolean = True;
  RMDesigner: IrmDesignerGUI;

implementation

uses rwQueryBuilderFrm, rmFMLExprEditorFormFrm;

var
  FGeneralSyn: TmwGeneralSyn;
  FErrTimer: TrwMessageTimer;


function GetUniqueComponentName(AOwner: TComponent; const APriffix: String): String;
var
  i: Integer;
begin
  for i := 1 to 1000 do
  begin
    Result := APriffix + IntToStr(i);
    if AOwner.FindComponent(Result) = nil then
      break;
  end;
end;


function GeneralSyn: TmwGeneralSyn;
var
  i: Integer;
begin
  GlobalNameSpace.BeginWrite;
  try
    if not Assigned(FGeneralSyn) then
    begin
      FGeneralSyn := TmwGeneralSyn.Create(nil);
      FGeneralSyn.SymbolAttri.Foreground := clGray;
      FGeneralSyn.Comments := [csPasStyle];
      FGeneralSyn.CommentAttri.Foreground := clNavy;
      FGeneralSyn.CommentAttri.Style := [fsItalic];
      FGeneralSyn.NumberAttri.Foreground := clPurple;
      FGeneralSyn.StringAttri.Foreground := clBlue;
      for i := 0 to KeyWords.Count - 1 do
        if KeyWords[i].Code > rwkPoint then
          FGeneralSyn.KeyWords.Add(KeyWords[i].Text);
    end;
    Result := FGeneralSyn;
  finally
    GlobalNameSpace.EndWrite;
  end;
end;


procedure ShowError(AMessage: String);
begin
  if not Assigned(FErrTimer) then
    FErrTimer := TrwMessageTimer.Create(nil);

  FErrTimer.ShowMessage(AMessage, True);
end;


{ TrwMessageTimer }

constructor TrwMessageTimer.Create(AOwner: TComponent);
begin
  inherited;
  Enabled := False;
  Interval := 250;
  OnTimer := FOnTimer;
end;


procedure TrwMessageTimer.FOnTimer(Sender: TObject);
begin
  Enabled := False;
  if FException then
    raise ErwException.Create(FMessage)
  else
    MessageDlg(FMessage, mtError, [mbOK], 0);
end;


procedure TrwMessageTimer.ShowMessage(const AMessage: string; const AExceptionType: Boolean = False);
begin
  FMessage := AMessage;
  FException := AExceptionType;
  Enabled := True;
end;


procedure DisassembleCompiledCode(const AFileName: string; const ACode: PTrwCode; const ADebInfo: PTrwDebInf = nil; const ABaseAddress: Integer = 0; const AEndAddress: Integer = 0);
var
  i, k, n, j,
  lCurDebInfLine,
  lLineEndAddr,
  lAddr1, lAddr2: Integer;
  lDebInfLine: String;
  lSourceText: String;
  L: TStringList;

  function DecodeConst(const AQuotes: Boolean = True): string;
  begin
    Inc(i);
    try
      case VarType(ACode^[i]) of
        varString,
        varOleStr:  begin
                      Result := ACode^[i];
                      if AQuotes then
                        Result := '"'+Result+'"';
                    end;
        varNull:  Result := '<Null>';
      else
        if VarIsArray(ACode^[i]) then
          Result := '<Array>'
        else
          Result := VarAsType(ACode^[i], varString);
      end;
    except
      Result := '??????';
    end;
  end;


  function DecodeVarType: string;
  begin
    Inc(i);
    try
      case TrwVarTypeKind(ACode^[i]) of
        rwvUnknown:  Result := 'Unknown';
        rwvInteger:  Result := 'Integer';
        rwvBoolean:  Result := 'Boolean';
        rwvString:   Result := 'String';
        rwvFloat:    Result := 'Float';
        rwvDate:     Result := 'Date';
        rwvArray:    Result := 'Array';
        rwvPointer:   Result := 'Pointer';
        rwvCurrency: Result := 'Currency';
        rwvVariant:  Result := 'Variant';
      end;
    except
      Result := '??????';
    end;
  end;


  function DecodeProc: string;
  begin
    Inc(i);
    try
      Result := GetEnumName(TypeInfo(TrwVMEmbeddedProc), Ord(TrwVMEmbeddedProc(ACode^[i])));
      Delete(Result, 1, 3);
    except
      Result := '??????';
    end;
  end;


  function DecodeFunct: string;
  begin
    Inc(i);
    try
      Result := GetEnumName(TypeInfo(TrwVMEmbeddedFunc), Ord(TrwVMEmbeddedFunc(ACode^[i])));
      Delete(Result, 1, 3);
    except
      Result := '??????';
    end;
  end;


  function DecodeInstruction: string;
  var
    c: variant;
    a: Integer;
  begin
    a := i;
    c := ACode^[i];
    try
      case TrwVMOperation(c) of
        rmoNope:        Result := 'NOPE';
        rmoMOVAconst:   Result := 'MOV   A, '+DecodeConst;
        rmoMOVAlocVar:  Result := 'MOV   A, LocalVar['+DecodeConst+']';
        rmoMOVAglobVar: Result := 'MOV   A, GlobalVar['+DecodeConst+']';
        rmoMOVBconst:   Result := 'MOV   B, '+DecodeConst;
        rmoMOVBlocVar:  Result := 'MOV   B, LocalVar['+DecodeConst+']';
        rmoMOVBglobVar: Result := 'MOV   B, GlobalVar['+DecodeConst+']';
        rmoMOVlocVarA:  Result := 'MOV   LocalVar['+DecodeConst+'], A';
        rmoMOVglobVarA: Result := 'MOV   GlobalVar['+DecodeConst+'], A';
        rmoMOVlocVarB:  Result := 'MOV   LocalVar['+DecodeConst+'], B';
        rmoMOVglobVarB: Result := 'MOV   GlobalVar['+DecodeConst+'], B';
        rmoMOVAB:       Result := 'MOV   A, B';
        rmoMOVBA:       Result := 'MOV   B, A';
        rmoMOVArefB:    Result := 'MOV   A, [B]';
        rmoMOVrefBA:    Result := 'MOV   [B], A';
        rmoReverseSignA: Result := 'NEG   A';
        rmoAdd:         Result := 'ADD   A, B';
        rmoSubstract:   Result := 'SUB   A, B';
        rmoDivide:      Result := 'DIV   A, B';
        rmoDiv:         Result := 'DIVI  A, B';
        rmoMod:         Result := 'MOD   A, B';
        rmoMultiply:    Result := 'MUL   A, B';
        rmoAexpB:       Result := 'EXP   A, B';
        rmoIncA:        Result := 'INC   A';
        rmoDecA:        Result := 'DEC   A';
        rmoIncB:        Result := 'INC   B';
        rmoDecB:        Result := 'DEC   B';
        rmoAandB:       Result := 'AND   A, B';
        rmoAorB:        Result := 'OR    A, B';
        rmoNotA:        Result := 'NOT   A';
        rmoPushA:       Result := 'PUSH  A';
        rmoPushB:       Result := 'PUSH  B';
        rmoPushFlag:    Result := 'PUSH  F';
        rmoPushConst:   Result := 'PUSH  '+DecodeConst;
        rmoPopA:        Result := 'POP   A';
        rmoPopB:        Result := 'POP   B';
        rmoPopFlag:     Result := 'POP   F';
        rmoSwapAB:      Result := 'SWAP  A, B';
        rmoCmpAlB:      Result := 'CMPL  A, B';
        rmoCmpAeB:      Result := 'CMPE  A, B';
        rmoCmpAneB:     Result := 'CMPNE A, B';
        rmoCmpAgB:      Result := 'CMPG  A, B';
        rmoCmpAleB:     Result := 'CMPLE A, B';
        rmoCmpAgeB:     Result := 'CMPGE A, B';
        rmoCall:        Result := 'CALL  '+FormatFloat('0000000', StrToInt(DecodeConst));
        rmoRet:         Result := 'RET';
        rmoNewVar:      Result := 'NEWVAR '+ DecodeVarType;
        rmoJump:        Result := 'JMP   '+FormatFloat('0000000', ABaseAddress + a + StrToInt(DecodeConst)+1);
        rmoJumpIfTrue:  Result := 'JMPT  '+FormatFloat('0000000', ABaseAddress + a + StrToInt(DecodeConst)+1);
        rmoJumpIfFalse: Result := 'JMPF  '+FormatFloat('0000000', ABaseAddress + a +StrToInt(DecodeConst)+1);

        rmoCallEmbProc: Result := 'PROC  '+DecodeProc;
        rmoCallEmbFunc: Result := 'FNCT  '+DecodeFunct;
        rmoCallEvent:   Result := 'EVNT  '+DecodeConst;
        rmoCallLibFunc: Result := 'LFNCT '+DecodeConst;
        rmoCallLibCompEvent: Result := 'LFNCT ' + '"' + DecodeConst(False) + '.' + DecodeConst(False) + '"';
      end;

    except
      Result := '??????';
    end;
    Result := FormatFloat('0000000', ABaseAddress + a)+'          '+Result;
  end;


begin
  L := TStringList.Create;
  try
    if AEndAddress = 0 then
      n := High(ACode^)
    else
      n := AEndAddress;

    i := Low(ACode^);
    lLineEndAddr := -1;
    lSourceText := '';
    lCurDebInfLine := 0;
    while i <= n do
    begin
      if Assigned(ADebInfo) and (i > lLineEndAddr) then
      begin
        for j := lCurDebInfLine to High(ADebInfo^) do
        begin
          lDebInfLine := ADebInfo^[j];
          lAddr1 := StrToInt(GetNextStrValue(lDebInfLine, ';'));
          lAddr2 := StrToInt(GetNextStrValue(lDebInfLine, ';'));
          if i < lAddr1 then
            break;

          if (i >= lAddr1) and (i <= lAddr2) then
          begin
            lLineEndAddr := lAddr2;
            lSourceText := GetNextStrValue(lDebInfLine, ';') + '.' +
              GetNextStrValue(lDebInfLine, ';') + ' ' + GetNextStrValue(lDebInfLine, ';') + ': ';
            for k := 1 to Length(lDebInfLine) do
              if lDebInfLine[k] in [#0, #10, #13] then
                lDebInfLine[k] := ' ';
            lSourceText := lSourceText + lDebInfLine;
            lCurDebInfLine := j + 1;
            break;
          end;
        end;
      end;

      if L.Count = L.Capacity then
        L.Capacity := L.Capacity + 512;

      if Length(lSourceText) > 0 then
      begin
        L.Add(lSourceText);
        lSourceText := '';
      end;

      if L.Count = L.Capacity then
        L.Capacity := L.Capacity + 512;

      L.Add(DecodeInstruction);

      Inc(i);
    end;

    L.SaveToFile(AFileName);
  finally
    L.Free;
  end;
end;


procedure ReadDsgnEnv;
begin
  DsgnCodeCompletion := Boolean(StrToInt(ReadFromRegistry('Misc\rwDesigner\Options', 'CodeCompletion', '1')));
  DsgnCodeParams := Boolean(StrToInt(ReadFromRegistry('Misc\rwDesigner\Options', 'CodeParams', '1')));
  DsgnCodeEvaluation := Boolean(StrToInt(ReadFromRegistry('Misc\rwDesigner\Options', 'ExprEvaluation', '0')));
  DsgnSymbInsight := Boolean(StrToInt(ReadFromRegistry('Misc\rwDesigner\Options', 'SymbInsight', '0')));
  DsgnCodeInsightDelay := StrToInt(ReadFromRegistry('Misc\rwDesigner\Options', 'CodeInsightDelay', '500'));

  DsgnTraceLib := Boolean(StrToInt(ReadFromRegistry('Misc\rwDesigner\Options', 'TraceLibrary', '0')));
  DsgnDontAskCompileConfirm := Boolean(StrToInt(ReadFromRegistry('Misc\rwDesigner\Options', 'DontAskCompileConfirm', '1')));
  DsgnAutoCompile := Boolean(StrToInt(ReadFromRegistry('Misc\rwDesigner\Options', 'AutoCompile', '1')));

  DsgnShowData := Boolean(StrToInt(ReadFromRegistry('Misc\rwDesigner\Options', 'ShowData', '0')));
  DesignUnits := TrwUnitType(StrToInt(ReadFromRegistry('Misc\rwDesigner\Options', 'Units', '0')));

  DesignRMAdvancedMode := Boolean(StrToInt(ReadFromRegistry('Misc\rmDesigner\Options', 'AdvancedMode', '0')));
  DesignRMShowPaperGrid := Boolean(StrToInt(ReadFromRegistry('Misc\rmDesigner\Options', 'ShowPaperGrid', '1')));
  DesignRMShowTableGrid := Boolean(StrToInt(ReadFromRegistry('Misc\rmDesigner\Options', 'ShowTableGrid', '0')));
end;


procedure WriteDsgnEnv;
begin
  WriteToRegistry('Misc\rwDesigner\Options', 'CodeCompletion', IntToStr(Integer(DsgnCodeCompletion)));
  WriteToRegistry('Misc\rwDesigner\Options', 'CodeParams', IntToStr(Integer(DsgnCodeParams)));
  WriteToRegistry('Misc\rwDesigner\Options', 'ExprEvaluation', IntToStr(Integer(DsgnCodeEvaluation)));
  WriteToRegistry('Misc\rwDesigner\Options', 'SymbInsight', IntToStr(Integer(DsgnSymbInsight)));
  WriteToRegistry('Misc\rwDesigner\Options', 'CodeInsightDelay', IntToStr(DsgnCodeInsightDelay));

  WriteToRegistry('Misc\rwDesigner\Options', 'TraceLibrary', IntToStr(Integer(DsgnTraceLib)));
  WriteToRegistry('Misc\rwDesigner\Options', 'DontAskCompileConfirm', IntToStr(Integer(DsgnDontAskCompileConfirm)));
  WriteToRegistry('Misc\rwDesigner\Options', 'AutoCompile', IntToStr(Integer(DsgnAutoCompile)));

  WriteToRegistry('Misc\rwDesigner\Options', 'ShowData', IntToStr(Integer(DsgnShowData)));
  WriteToRegistry('Misc\rwDesigner\Options', 'Units', IntToStr(Ord(DesignUnits)));
  WriteToRegistry('Misc\rmDesigner\Options', 'AdvancedMode', IntToStr(Ord(DesignRMAdvancedMode)));
  WriteToRegistry('Misc\rmDesigner\Options', 'ShowPaperGrid', IntToStr(Ord(DesignRMShowPaperGrid)));
  WriteToRegistry('Misc\rmDesigner\Options', 'ShowTableGrid', IntToStr(Ord(DesignRMShowTableGrid)));
end;


procedure rmGetFieldListFromQBQuery(AQuery: TrwQBQuery; AList: TStringList);
var
  i: Integer;
begin
  AList.Clear;
  AList.Capacity := AQuery.ShowingFields.Count;
  for i := 0 to AQuery.ShowingFields.Count - 1 do
    AList.Add(AQuery.ShowingFields[i].GetFieldName(False, False));
end;


procedure rmQueryBuilderForObject(const AComponent: IrmDesignObject);
var
  Q: TObject;
  Vars: IrmVariables;
  cRoot: IrmDesignObject;
  h: String;
begin
  h := FindQBQueryProp(AComponent);
  if h <> '' then
  begin
    Q := Pointer(Integer(AComponent.GetPropertyValue(h)));

    cRoot := AComponent.GetRootOwner;

    if Assigned(cRoot) and (cRoot.GetVarFunctHolder <> nil) then
      Vars := cRoot.GetVarFunctHolder.GetVariables
    else
      Vars := nil;

    ShowQueryBuilderExt(TrwQBQuery(Q), True, False, Vars);
  end;
end;


procedure FillUpTreeViewCollection(AList: TStringList; ATreeView: TTreeView; ARootNode: TTreeNode; ACallBack: TrwFillUpTreeViewCallBack = nil); overload;
var
  i: Integer;
  cItem: IrmLibCollectionItem;
  N: TTreeNode;
  h: String;

  function CallBack(ANode: TTreeNode): Boolean;
  begin
    Result := True;
    if Assigned(ACallBack) then
      ACallBack(ANode, Result);
  end;

  function CheckBranch(AGroup: String): TTreeNode;
  var
    h: String;
    i: Integer;
  begin
    Result := ARootNode;

    while AGroup <> '' do
    begin
      h := GetNextStrValue(AGroup, '\');
      if Assigned(Result) then
      begin
        for i := 0 to Result.Count - 1 do
          if SameText(Result[i].Text, h) then
          begin
            Result := Result[i];
            h := '';
            break;
          end;
      end
      else
      begin
        for i := 0 to ATreeView.Items.Count - 1 do
          if (ATreeView.Items[i].Level = 0) and SameText(ATreeView.Items[i].Text, h) then
          begin
            Result := ATreeView.Items[i];
            h := '';
            break;
          end;
      end;

      if h <> '' then
      begin
        Result := ATreeView.Items.AddChildObject(Result, h, nil);
        if not CallBack(Result) then
        begin
          FreeAndNil(Result);
          break;
        end;
      end;
    end;
  end;

begin
  for i := 0 to AList.Count - 1 do
  begin
    cItem := IInterface(Pointer(AList.Objects[i])) as IrmLibCollectionItem;
    N := CheckBranch(cItem.GetGroup);
    if Assigned(N) then
    begin
      h := cItem.GetName;
      N := ATreeView.Items.AddChildObject(N, h, Pointer(cItem));
      if not CallBack(N) then
        FreeAndNil(N);
    end;
  end;
end;

procedure FillUpTreeViewCollection(ACollection: IrmLibCollection; ATreeView: TTreeView; ARootNode: TTreeNode; ACallBack: TrwFillUpTreeViewCallBack = nil);
var
  L: TStringList;
  i: Integer;
begin
  L := TStringList.Create;
  try
    L.Capacity := ACollection.GetItemCount;
    for i := 0 to  ACollection.GetItemCount - 1 do
      L.AddObject(UpperCase((ACollection.GetItemByIndex(i) as IrmLibCollectionItem).GetName), Pointer(ACollection.GetItemByIndex(i) as IrmLibCollectionItem));

    FillUpTreeViewCollection(L, ATreeView, ARootNode, ACallBack);

  finally
    FreeAndNil(L);
  end;
end;

function AddCollectionItemToTreeView(ACollectionItem: TrwLibCollectionItem; ATreeView: TTreeView; AParentNode: TTreeNode; ACallBack: TrwFillUpTreeViewCallBack = nil): TTreeNode;

  function CallBack(ANode: TTreeNode): Boolean;
  begin
    Result := True;
    if Assigned(ACallBack) then
      ACallBack(ANode, Result);
  end;

begin
  Result := ATreeView.Items.AddChildObject(AParentNode, ACollectionItem.DisplayName, ACollectionItem);
  if not CallBack(Result) then
    FreeAndNil(Result);
end;

procedure UpdateCollectionGroupInfo(ACollection: TrwLibCollection; ATreeView: TTreeView; ARootNode: TTreeNode);
var
  i: Integer;
  N: TTreeNode;

  function GetGroupName(ANode: TTreeNode): String;
  begin
    N := ANode.Parent;
    Result := '';
    while N <> ARootNode do
    begin
      if Result <> '' then
        Result := '\' + Result;
      Result := N.Text + Result;
      N := N.Parent;
    end;

  end;

begin
  for i := 0 to ACollection.Count - 1 do
    TrwLibCollectionItem(ACollection.Items[i]).Group := '';

  for i := 0 to ATreeView.Items.Count - 1 do
    if Assigned(ATreeView.Items[i].Data) then
      TrwLibCollectionItem(ATreeView.Items[i].Data).Group := GetGroupName(ATreeView.Items[i]);
end;


function MakeComboBoxFieldList(AFieldList: TCommaDilimitedString): String;
var
  h: String;
begin
  Result := '';
  while AFieldList <> '' do
  begin
    h := GetNextStrValue(AFieldList, ',');

    if Result <> '' then
      Result := Result + #13;

    Result := Result + h + #9 + h;
  end;
end;


procedure RegisterTalkativeApartmentMember(const AReceiver: IrmMessageReceiver);
var
  Apt: IrmDsgnApartment;
begin
  Apt := AReceiver.Apartment;
  if Assigned(Apt) then
    Apt.EventManager.RegisterObject(AReceiver);
end;


procedure UnRegisterTalkativeApartmentMember(const AReceiver: IrmMessageReceiver);
var
  Apt: IrmDsgnApartment;
begin
  Apt := AReceiver.Apartment;
  if Assigned(Apt) then
    Apt.EventManager.UnRegisterObject(AReceiver);
end;

procedure RegisterTalkativeDesignerMember(const AReceiver: IrmMessageReceiver);
begin
  if Assigned(RMDesigner) then
    RMDesigner.EventManager.RegisterObject(AReceiver);
end;


procedure UnRegisterTalkativeDesignerMember(const AReceiver: IrmMessageReceiver);
begin
  if Assigned(RMDesigner) then
    RMDesigner.EventManager.UnRegisterObject(AReceiver);
end;


procedure SendApartmentMessageA(const Queued: Boolean; const ASender: IrmMessageReceiver; const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
var
  Apt: IrmDsgnApartment;
  Msg: TrmMessageRec;
begin
  if Assigned(ASender) then
  begin
    Apt := ASender.Apartment;
    if Assigned(Apt) then
    begin
      Msg.MessageID := AMessageID;
      Msg.SenderID := GetObjectID(ASender.VCLObject);
      Msg.Parameter1 := AParam1;
      Msg.Parameter2 := AParam2;

      if Queued then
        Apt.EventManager.PostBroadcastMessage(Msg)
      else
        Apt.EventManager.SendBroadcastMessage(Msg);
    end;
  end;
end;

procedure SendApartmentMessage(const ASender: IrmMessageReceiver; const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
begin
  SendApartmentMessageA(False, ASender, AMessageID, AParam1, AParam2);
end;

procedure PostApartmentMessage(const ASender: IrmMessageReceiver; const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
begin
  SendApartmentMessageA(True, ASender, AMessageID, AParam1, AParam2);
end;

procedure SendDesignerMessageA(const Queued: Boolean; const ASender: IrmMessageReceiver; const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
var
  Msg: TrmMessageRec;
begin
  if Assigned(RMDesigner) then
  begin
    Msg.MessageID := AMessageID;
    Msg.SenderID := GetObjectID(ASender.VCLObject);
    Msg.Parameter1 := AParam1;
    Msg.Parameter2 := AParam2;

    if Queued then
      RMDesigner.EventManager.PostBroadcastMessage(Msg)
    else
      RMDesigner.EventManager.SendBroadcastMessage(Msg);
  end;
end;

procedure SendDesignerMessage(const ASender: IrmMessageReceiver; const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
begin
  SendDesignerMessageA(False, ASender, AMessageID, AParam1, AParam2);
end;

procedure PostDesignerMessage(const ASender: IrmMessageReceiver; const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
begin
  SendDesignerMessageA(True, ASender, AMessageID, AParam1, AParam2);
end;

procedure SendBroadcastMessageA(const Queued: Boolean; const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
var
  Apt: TrmdApartmentList;
  i: Integer;
  Msg: TrmMessageRec;
begin
  Apt := nil;
  if Assigned(RMDesigner) then
  begin
    Msg.MessageID := AMessageID;
    Msg.SenderID := GetObjectID(RMDesigner.VCLObject);
    Msg.Parameter1 := AParam1;
    Msg.Parameter2 := AParam2;

    if Queued then
      RMDesigner.EventManager.PostBroadcastMessage(Msg)
    else
      RMDesigner.EventManager.SendBroadcastMessage(Msg);
    Apt := RMDesigner.GetApartmentList;

    for i := Low(Apt) to High(Apt) do
    begin
      Msg.MessageID := AMessageID;
      Msg.SenderID := GetObjectID(RMDesigner.VCLObject);
      Msg.Parameter1 := AParam1;
      Msg.Parameter2 := AParam2;

      if Queued then
        Apt[i].EventManager.PostBroadcastMessage(Msg)
      else
        Apt[i].EventManager.SendBroadcastMessage(Msg);
    end;
  end;
end;


procedure SendBroadcastMessage(const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
begin
  SendBroadcastMessageA(False, AMessageID, AParam1, AParam2);
end;


procedure PostBroadcastMessage(const AMessageID: TrmDsgnMessageID; const AParam1, AParam2: Variant);
begin
  SendBroadcastMessageA(True, AMessageID, AParam1, AParam2);
end;

function IsEventQueueEmpty(const ASender: IrmMessageReceiver): Boolean;
var
  Apt: IrmDsgnApartment;
begin
  Result := True;
  if Assigned(ASender) then
  begin
    Apt := ASender.Apartment;
    if Assigned(Apt) then
    begin
      Result := Apt.EventManager.QueueIsEmpty;
      if Result then
        Result := RMDesigner.EventManager.QueueIsEmpty;
    end;
  end;
end;

function GetObjectID(AObject: TObject): TrmObjectID;
begin
  Result := Cardinal(Pointer(AObject));
end;


function GetComponentAppartment(AObject: TComponent): IrmDsgnApartment;
var
  O: TComponent;
begin
  Result := nil;
  O := AObject;
  while Assigned(O) do
  begin
    if Supports(O, IrmDsgnApartment) then
    begin
      Result := O as IrmDsgnApartment;
      break;
    end;
    O := O.Owner;
  end;
end;


function AreYouSure(const AQuestion: String): Boolean;
begin
  Result := MessageDlg(AQuestion,  mtConfirmation, [mbYes, mbNo], 0) = mrYes;
end;


function ObjectsAreTheSame(const AObject1, AObject2: IrmInterfacedObject): Boolean;
begin
  Result := not Assigned(AObject1) and not Assigned(AObject2) or
           Assigned(AObject1) and Assigned(AObject2) and (AObject1.VCLObject = AObject2.VCLObject);
end;

initialization
  CF_RWDATA := RegisterClipboardFormat('CF_RWDATA');
  CF_FMLEXPRESSION := RegisterClipboardFormat('CF_FMLEXPRESSION');

  FGeneralSyn := nil;
  FErrTimer := nil;
  ReadDsgnEnv;

finalization
  WriteDsgnEnv;

  FreeAndNil(FGeneralSyn);

  if Assigned(FErrTimer) then
    FErrTimer.Free;


end.
