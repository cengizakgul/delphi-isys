unit rmWatchListFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDockableFrameFrm, StdCtrls, Menus, ComCtrls, rmDsgnTypes;

type
  TrmWatchList = class(TrmDockableFrame)
    lvWatches: TListView;
    pmOperations: TPopupMenu;
    miAddWatch: TMenuItem;
    miEditWatch: TMenuItem;
    miDeleteWatch: TMenuItem;
    procedure miAddWatchClick(Sender: TObject);
    procedure miEditWatchClick(Sender: TObject);
    procedure miDeleteWatchClick(Sender: TObject);
  private
    procedure DMRunningStopped(var Message: TrmMessageRec); message mRunningStopped;
    procedure DMRunningPaused(var Message: TrmMessageRec); message mRunningPaused;
    procedure DMRunningProceeded(var Message: TrmMessageRec); message mRunningProceeded;

    procedure RefreshWatchList;
    procedure AddWatch;
    procedure EditWatch;
    procedure DeleteWatch;
    procedure ReCalculateWatches;

  protected
    procedure GetListeningMessages(const AMessages: TList); override;
    procedure BeforeShow; override;
    procedure DoRefresh; override;
  public
  end;

implementation

{$R *.dfm}

procedure TrmWatchList.miAddWatchClick(Sender: TObject);
begin
  AddWatch;
end;

procedure TrmWatchList.miEditWatchClick(Sender: TObject);
begin
  EditWatch;
end;

procedure TrmWatchList.miDeleteWatchClick(Sender: TObject);
begin
  DeleteWatch;
end;

procedure TrmWatchList.AddWatch;
var
  h: string;
  WL: IrmWatchList;
begin
  h := '';
  if InputQuery('Add Watch', 'Expression', h) then
  begin
    WL := (Apartment as IrmDsgnReport).GetWatchList;
    WL.Add(h);
    RefreshWatchList;
  end;
end;

procedure TrmWatchList.DeleteWatch;
var
  WL: IrmWatchList;
begin
  if lvWatches.ItemIndex <> - 1 then
  begin
    WL := (Apartment as IrmDsgnReport).GetWatchList;
    WL.Delete(lvWatches.ItemIndex);
    RefreshWatchList;
  end;  
end;

procedure TrmWatchList.EditWatch;
var
  h: string;
  WL: IrmWatchList;
begin
  if lvWatches.ItemIndex <> - 1 then
  begin
    WL := (Apartment as IrmDsgnReport).GetWatchList;
    h := WL.WatchByIndex(lvWatches.ItemIndex).Expression;
    if InputQuery('Edit Watch', 'Expression', h) then
    begin
      WL.Update(lvWatches.ItemIndex, h);
      RefreshWatchList;
    end;
  end

  else
    AddWatch;
end;

procedure TrmWatchList.ReCalculateWatches;
begin
  RefreshWatchList;
end;

procedure TrmWatchList.DMRunningPaused(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;

procedure TrmWatchList.DMRunningProceeded(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;

procedure TrmWatchList.DMRunningStopped(var Message: TrmMessageRec);
begin
  inherited;
  RefreshInIdle;
end;

procedure TrmWatchList.BeforeShow;
begin
  Caption := 'Watch List';
  ReCalculateWatches;
  inherited;
end;

procedure TrmWatchList.DoRefresh;
begin
  inherited;
  ReCalculateWatches;
end;

procedure TrmWatchList.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mRunningStopped));
  AMessages.Add(Pointer(mRunningPaused));
  AMessages.Add(Pointer(mRunningProceeded));
end;

procedure TrmWatchList.RefreshWatchList;
var
  i: Integer;
  WL: IrmWatchList;
  LI: TListItem;
begin
  WL := (Apartment as IrmDsgnReport).GetWatchList;

  lvWatches.Items.BeginUpdate;
  try
    lvWatches.Items.Clear;
    for i := 0 to WL.Count - 1 do
      with WL.WatchByIndex(i) do
      begin
        LI := lvWatches.Items.Add;
        LI.Caption := Expression;
        LI.SubItems.Add(Result.ResultText);
      end;
  finally
    lvWatches.Items.EndUpdate;
  end;  
end;

initialization
  RegisterClass(TrmWatchList);


end.
