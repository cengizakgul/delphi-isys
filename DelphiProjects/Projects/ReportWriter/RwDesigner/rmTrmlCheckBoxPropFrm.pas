unit rmTrmlCheckBoxPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmlCustomControlPropFrm, StdCtrls, ExtCtrls,
  rmOPEReportParamFrm, rmOPEColorFrm, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls;

type
  TrmTrmlCheckBoxProp = class(TrmTrmlCustomControlProp)
    frmCaption: TrmOPEStringSingleLine;
    grbValues: TGroupBox;
    frmValueChecked: TrmOPEStringSingleLine;
    frmValueUnChecked: TrmOPEStringSingleLine;
  protected
    procedure GetPropsFromObject; override;
  public
  end;


implementation

{$R *.dfm}

{ TrmTrmlCheckBoxProp }

procedure TrmTrmlCheckBoxProp.GetPropsFromObject;
begin
 inherited;
 frmCaption.PropertyName := 'Caption';
 frmCaption.ShowPropValue;
 frmValueChecked.PropertyName := 'ValueChecked';
 frmValueChecked.ShowPropValue;
 frmValueUnChecked.PropertyName := 'ValueUnChecked';
 frmValueUnChecked.ShowPropValue;
end;

end.
