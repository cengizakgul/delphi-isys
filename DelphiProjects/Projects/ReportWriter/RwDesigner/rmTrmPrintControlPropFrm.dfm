inherited rmTrmPrintControlProp: TrmTrmPrintControlProp
  Height = 366
  inherited pcCategories: TPageControl
    Height = 366
    inherited tcAppearance: TTabSheet
      TabVisible = True
    end
    object tcPrint: TTabSheet [2]
      Caption = 'Processing'
      ImageIndex = 3
      inline frmCutable: TrmOPEBoolean
        Left = 8
        Top = 10
        Width = 158
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
        inherited chbProp: TCheckBox
          Width = 158
          Caption = 'Allow to cut between Pages'
        end
      end
      inline frmPrintOnEachPage: TrmOPEBoolean
        Left = 8
        Top = 36
        Width = 118
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited chbProp: TCheckBox
          Width = 118
          Caption = 'Print on each Page'
        end
      end
      inline frmBlockParentIfEmpty: TrmOPEBoolean
        Left = 8
        Top = 62
        Width = 223
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 2
        inherited chbProp: TCheckBox
          Width = 223
          Caption = 'Do not print parent container if not printed'
        end
      end
      object pnlBlocking: TPanel
        Left = 8
        Top = 99
        Width = 290
        Height = 78
        BevelOuter = bvNone
        TabOrder = 3
        inline frmPrintIf: TrmOPEBlockingControl
          Left = 1
          Top = 0
          Width = 289
          Height = 71
          AutoScroll = False
          TabOrder = 0
          inherited lCaption: TLabel
            Width = 289
          end
          inherited frmExpression: TrmFMLExprPanel
            Width = 289
            Height = 55
            inherited pnlCanvas: TPanel
              Width = 289
              Height = 55
            end
          end
        end
      end
    end
  end
end
