unit rmCtrlResizer;

interface

uses
  Windows, Messages, Classes, Graphics, Controls, Forms, rmTypes, rmDsgnTypes,
  rwDebugInfo, Variants, SysUtils;

type
  TrwTypeSizePoint = (spClient, spTopLeft, spTop, spTopRight, spRight, spBottomRight,
    spBottom, spBottomLeft, spLeft);


         {This is sizable border around control}

  TrmControlResizer = class(TCustomControl)
  private
    FDsgnComp: IrmDsgnComponent;
    FState: TrmResizerState;
    FSizePoint: TrwTypeSizePoint;
    FOldMousePos: TPoint;
    FMouseCaptured: Boolean;

    function  GetRegion: THandle;
    procedure SetRegion;
    procedure SetCursor(ARegion: TrwTypeSizePoint);
    function  WhereIsCursor(X, Y: LongInt): TrwTypeSizePoint;
    procedure FOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FOnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure WMMouseMove(var Message: TWMMouseMove); message WM_MOUSEMOVE;

    procedure WMGetDLGCode(var Message: TMessage); message WM_GETDLGCODE;
    procedure ResizeControl(dX, dY, dW, dH: Integer);
    procedure GoToNextTableCell(const ADirection: Char);

  protected
    FAttachedObject: IrmDesignObject;
    procedure Paint; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure CreateWindowHandle(const Params: TCreateParams); override;
    procedure ShowIfNotInView;

  public
    property AttachedObject: IrmDesignObject read FAttachedObject;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Attach(AComponent: IrmDesignObject);
    procedure Detach;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure SetResizerState(AState: TrmResizerState);
    procedure BoundsByControl;

  published
    property OnDblClick;
    property PopupMenu;
    property OnMouseMove;
  end;


implementation

uses
  rwDesignerFrm, rwDesignAreaFrm;

const
  cSizePoint = 5;
  cGridSize = 8;


              {TrmControlResizer}

constructor TrmControlResizer.Create(AOwner: TComponent);
begin
  inherited Create(nil);

  FDsgnComp := (AOwner as IrmDsgnComponent);

  BevelInner := bvNone;
  BevelOuter := bvNone;
  BorderWidth := 0;
  FAttachedObject := nil;
  FState := maNone;
  FSizePoint := spClient;
  OnMouseDown := FOnMouseDown;
  OnMouseUp := FOnMouseUp;
end;

destructor TrmControlResizer.Destroy;
begin
  Detach;
  FDsgnComp.Notify(rmdDestroyResizer, Integer(Pointer(Self)));
  FDsgnComp := nil;
  inherited;
end;

procedure TrmControlResizer.Attach(AComponent: IrmDesignObject);
begin
  Detach;

  if not Assigned(AComponent) then
    Exit;

  FAttachedObject := AComponent;


  if FAttachedObject.DesignBehaviorInfo.SelectorType <> rwDBISBNone then
    Parent := FAttachedObject.GetWinControlParent;

  SetResizerState(maSelected);

  FAttachedObject.Notify(rmdSelectObject);

  ShowIfNotInView;
end;

procedure TrmControlResizer.Detach;
begin
  Parent := nil;

  if Assigned(FAttachedObject) then
    FAttachedObject.Notify(rmdDestroyResizer);

  FAttachedObject := nil;
  SetResizerState(maNone);
end;

procedure TrmControlResizer.WMGetDLGCode(var Message: TMessage);
begin
  Message.Result := DLGC_WANTARROWS;
end;

procedure TrmControlResizer.Paint;
var
  Rgn: THandle;
begin
  with Canvas do
  begin
    if Debugging then
      Brush.Color := clRed
    else
      if FState = maSelected then
        if FDsgnComp.GroupSelection then
          Brush.Color := clGray
        else
          Brush.Color := clBlack
      else
        Brush.Color := clGray;

    Brush.Style := bsSolid;
    Rgn := CreateRectRgn(0,0,0,0);
    GetWindowRgn(Self.Handle, Rgn);
    FillRgn(Canvas.Handle, Rgn, Brush.Handle);
    DeleteObject(Rgn);
  end;
end;

procedure TrmControlResizer.BoundsByControl;
var
  d: Integer;
  R: TRect;
begin
  if not Assigned(Parent) then
    Exit;

  if FState = maSelected then
    d := cSizePoint div 2
  else if FState in [maResize, maMove] then
    d := 0
  else
    Exit;

  R := Rect(FAttachedObject.ClientToScreen(Point(0, 0)), FAttachedObject.ClientToScreen(Point(FAttachedObject.GetWidth, FAttachedObject.GetHeight)));
  InflateRect(R, d, d);

  R.TopLeft := Parent.ScreenToClient(R.TopLeft);
  R.BottomRight := Parent.ScreenToClient(R.BottomRight);

  SetBounds(R.Left, R.Top, R.Right - R.Left, R.Bottom - R.Top);
end;

function TrmControlResizer.WhereIsCursor(X, Y: LongInt): TrwTypeSizePoint;
var
  h1, h2, hp: Integer;
  P: TPoint;
  DsgnInfo: TrwDesignBehaviorInfoRec;  
begin
  if FDsgnComp.GroupSelection then
  begin
    Result := spClient;
    Exit;
  end;

  h1 := Width div 2;
  h2 := Height div 2;
  hp := cSizePoint div 2;
  P := Point(X, Y);
  if PtInRect(Rect(0, 0, cSizePoint, cSizePoint), P) then
    Result := spTopLeft
  else if PtInRect(Rect(Width - cSizePoint, 0, Width, cSizePoint), P) then
    Result := spTopRight
  else if PtInRect(Rect(Width - cSizePoint, Height - cSizePoint, Width, Height), P) then
    Result := spBottomRight
  else if PtInRect(Rect(0, Height - cSizePoint, cSizePoint, Height), P) then
    Result := spBottomLeft
  else if PtInRect(Rect(h1 - hp, 0, h1 + hp, cSizePoint), P) then
    Result := spTop
  else if PtInRect(Rect(Width - cSizePoint, h2 - hp, Width, h2 + hp), P) then
    Result := spRight
  else if PtInRect(Rect(h1 - hp, Height - cSizePoint, h1 + hp, Height), P) then
    Result := spBottom
  else
    if PtInRect(Rect(0, h2 - hp, cSizePoint, h2 + hp), P) then
    Result := spLeft
  else
    Result := spClient;

  DsgnInfo := FAttachedObject.DesignBehaviorInfo;
  case Result of
    spTopLeft:
      if [rwDBIMDTop, rwDBIMDLeft] * DsgnInfo.ResizingDirections = [] then
        Result := spClient;
    spTop:
      if not (rwDBIMDTop in DsgnInfo.ResizingDirections) then
        Result := spClient;
    spTopRight:
      if [rwDBIMDTop, rwDBIMDRight] * DsgnInfo.ResizingDirections = [] then
        Result := spClient;
    spRight:
      if not (rwDBIMDRight in DsgnInfo.ResizingDirections) then
        Result := spClient;
    spBottomRight:
      if [rwDBIMDBottom, rwDBIMDRight] * DsgnInfo.ResizingDirections = [] then
        Result := spClient;
    spBottom:
      if not (rwDBIMDBottom in DsgnInfo.ResizingDirections) then
        Result := spClient;
    spBottomLeft:
      if [rwDBIMDBottom, rwDBIMDLeft] * DsgnInfo.ResizingDirections = [] then
        Result := spClient;
    spLeft:
      if not (rwDBIMDLeft in DsgnInfo.ResizingDirections) then
        Result := spClient;
  end;
end;

procedure TrmControlResizer.FOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  R: TRect;
  DsgnInfo: TrwDesignBehaviorInfoRec;
  P: TWinControl;
begin
  if (Button <> mbLeft) or (ssDouble in Shift) or FDsgnComp.IsDragAndDropMode then
    Exit;

  P := FAttachedObject.GetWinControlParent;
  R := P.ClientRect;
  R.TopLeft := P.ClientToScreen(R.TopLeft);
  R.BottomRight := P.ClientToScreen(R.BottomRight);
  DsgnInfo := FAttachedObject.DesignBehaviorInfo;

  if DsgnInfo.MovingDirections <> [] then
  begin
    ClipCursor(@R);
    SetCaptureControl(Self);
    FMouseCaptured := True;
  end;

  FSizePoint := WhereIsCursor(X, Y);
  if (FSizePoint <> spClient) then
    SetResizerState(maResize)
  else
  begin
    if DsgnInfo.MovingDirections <> [] then
      FDsgnComp.ApplyResizerState(maMove);
  end;

  FOldMousePos := ClientToScreen(Point(X, Y));

  if CanFocus then
    SetFocus;
end;

procedure TrmControlResizer.FOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  R1, R2: TRect;
  P: TWinControl;
begin
  if FDsgnComp.IsDragAndDropMode then
    Exit;

  if FState <> maSelected then
  begin
    P := FAttachedObject.GetWinControlParent;
    R1 := FAttachedObject.GetParentExtRect;

    R2 := BoundsRect;
    R2.TopLeft := Parent.ClientToScreen(R2.TopLeft);
    R2.BottomRight := Parent.ClientToScreen(R2.BottomRight);
    R2.TopLeft := P.ScreenToClient(R2.TopLeft);
    R2.BottomRight := P.ScreenToClient(R2.BottomRight);

    FDsgnComp.ApplyResizerChanges(R2.Left - R1.Left, R2.Top - R1.Top,
                                    (R2.Right - R2.Left) - (R1.Right - R1.Left),
                                    (R2.Bottom - R2.Top) - (R1.Bottom - R1.Top));
  end;

  if FMouseCaptured then
  begin
    ClipCursor(nil);
    ReleaseCapture;
  end;  

  FDsgnComp.ApplyResizerState(maSelected);
  SetCursor(spClient);
end;

procedure TrmControlResizer.FOnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  Region: TrwTypeSizePoint;
  P: TPoint;
  lDelta: Integer;
  fl_exit: Boolean;
  dLeft, dTop, dWidth, dHeight: Integer;
  DsgnInfo: TrwDesignBehaviorInfoRec;
begin
  if ssShift in Shift then
    Exit;

  if FDsgnComp.IsDragAndDropMode then
    SetCursor(spClient)

  else if FState = maSelected then
  begin
    Region := WhereIsCursor(X, Y);
    SetCursor(Region);
  end

  else
  begin
    if (Shift = [ssAlt]) or (Shift = [ssAlt, ssLeft])  then
      lDelta := 1
    else
      lDelta := cGridSize;

    P := ClientToScreen(Point(X, Y));

    fl_exit := not((Abs(FOldMousePos.Y - P.Y) >= lDelta) or (Abs(FOldMousePos.X - P.X) >= lDelta));

    if not fl_exit then
    begin
      dLeft := 0;
      dTop := 0;
      dWidth := 0;
      dHeight := 0;

      if (FState = maResize) and not (FAttachedObject.ObjectType = rwDOTNotVisual) then
      begin
        case FSizePoint of
          spTopLeft:
            begin
              dLeft := P.X - FOldMousePos.X;
              dTop := P.Y - FOldMousePos.Y;
              dWidth := - dLeft;
              dHeight := - dTop;
            end;

          spTop:
            begin
              dTop := P.Y - FOldMousePos.Y;
              dHeight := - dTop;
            end;

          spTopRight:
            begin
              dTop := P.Y - FOldMousePos.Y;
              dWidth := P.X - FOldMousePos.X;
              dHeight := - dTop;
            end;

          spRight:
              dWidth := P.X - FOldMousePos.X;

          spBottomRight:
            begin
              dHeight := P.Y - FOldMousePos.Y;
              dWidth := P.X - FOldMousePos.X;
            end;

          spBottom:
              dHeight := P.Y - FOldMousePos.Y;

          spBottomLeft:
            begin
              dLeft := P.X - FOldMousePos.X;
              dHeight := P.Y - FOldMousePos.Y;
              dWidth := - dLeft;
            end;

          spLeft:
            begin
              dLeft := P.X - FOldMousePos.X;
              dWidth := -dLeft;
            end;
        end;
      end

      else if FState = maMove then
      begin
        DsgnInfo := FAttachedObject.DesignBehaviorInfo;

        if not (rwDBIMDLeft in DsgnInfo.MovingDirections) and (P.X < FOldMousePos.X) then
          dLeft := 0
        else if not (rwDBIMDRight in DsgnInfo.MovingDirections) and (P.X > FOldMousePos.X) then
          dLeft := 0
        else
          dLeft := P.X - FOldMousePos.X;

        if not (rwDBIMDTop in DsgnInfo.MovingDirections) and (P.Y < FOldMousePos.Y) then
          dTop := 0
        else if not (rwDBIMDBottom in DsgnInfo.MovingDirections) and (P.Y > FOldMousePos.Y) then
          dTop := 0
        else
          dTop := P.Y - FOldMousePos.Y;

        SetCursor(spClient);
      end;

      FDsgnComp.ApplyGroupResize(dLeft, dTop, dWidth, dHeight);
      FOldMousePos := P;
    end;
  end;
end;

procedure TrmControlResizer.SetCursor(ARegion: TrwTypeSizePoint);
begin
  if ARegion = spClient then
    Cursor := crArrow
  else if ARegion in [spTopLeft, spBottomRight] then
    Cursor := crSizeNWSE
  else if ARegion in [spTopRight, spBottomLeft] then
    Cursor := crSizeNESW
  else if ARegion in [spTop, spBottom] then
    Cursor := crSizeNS
  else if ARegion in [spRight, spLeft] then
    Cursor := crSizeWE;

  if FState = maMove then
    Screen.Cursor := crSizeAll
  else
    Screen.Cursor := crDefault;
end;


procedure TrmControlResizer.ResizeControl(dX, dY, dW, dH: Integer);
begin
  FDsgnComp.ApplyResizerState(maMove);
  FDsgnComp.ApplyResizerChanges(dX, dY, dW, dH);
  FDsgnComp.ApplyResizerState(maSelected);
end;

procedure TrmControlResizer.KeyDown(var Key: Word; Shift: TShiftState);
var
  P: IrmDesignObject;
  lDelta: Integer;
  h: String;
begin
  if FDsgnComp.IsDragAndDropMode then
    Exit;

  if (FState <> maSelected) and (Key = VK_ESCAPE) then
  begin
    SetResizerState(maSelected);
    PostMessage(Handle, WM_LBUTTONUP, 0, 0);

    Exit;
  end;

  if Shift = [ssCtrl, ssShift] then
    lDelta := cGridSize
  else
    lDelta := 1;

  if ssCtrl in Shift then
    case Key of
      VK_UP:    ResizeControl(0, -lDelta, 0, 0);
      VK_DOWN:  ResizeControl(0, lDelta, 0, 0);
      VK_LEFT:  ResizeControl(-lDelta, 0, 0, 0);
      VK_RIGHT: ResizeControl(lDelta, 0, 0, 0);
    end

  else if Shift = [ssShift] then
    case Key of
      VK_UP:    ResizeControl(0, 0, 0, -1);
      VK_DOWN:  ResizeControl(0, 0, 0, 1);
      VK_LEFT:  ResizeControl(0, 0, -1, 0);
      VK_RIGHT: ResizeControl(0, 0, 1, 0);
    end

  else if (Shift = []) and (Key = VK_ESCAPE) then
  begin
    P := AttachedObject.GetParent;
    if Assigned(P) and (P.DesignBehaviorInfo.Selectable) then
    begin
      Attach(P);
      if CanFocus then
        SetFocus;
      FDsgnComp.Notify(rmdObjectSelected, VarArrayOf([Integer(Pointer(P))]));
    end;
  end

  else if (Shift = []) and (Key = VK_F1) then
  begin
    h := AttachedObject.GetClassName + #0;
    Application.HelpCommand(HELP_KEY, Integer(@h[1]));
  end

  else if (Shift = []) and not FDsgnComp.GroupSelection and Supports(FAttachedObject, IrmTableCell) then
    case Key of
      VK_UP:    GoToNextTableCell('U');
      VK_DOWN:  GoToNextTableCell('D');
      VK_LEFT:  GoToNextTableCell('L');
      VK_RIGHT: GoToNextTableCell('R');
    end;

  inherited;
end;


procedure TrmControlResizer.WMMouseMove(var Message: TWMMouseMove);
begin
  inherited;

  with Message do
    FOnMouseMove(Self, KeysToShiftState(Keys), XPos, YPos);
end;

procedure TrmControlResizer.SetBounds(ALeft, ATop, AWidth,  AHeight: Integer);
var
  fl: Boolean;
begin
  fl := (AWidth = Width) and (AHeight = Height);

  inherited;

  if not fl then
    SetRegion;
end;

function TrmControlResizer.GetRegion: THandle;
var
  DsgnInfo: TrwDesignBehaviorInfoRec;

  procedure GenericRegion;
  var
    h, hp: Integer;
    Rgn: THandle;

    procedure AddSquare(X1, Y1, X2, Y2: Integer);
    begin
      Rgn := CreateRectRgn(X1, Y1, X2, Y2);
      CombineRgn(Result, Result, Rgn, RGN_OR);
      DeleteObject(Rgn);
    end;

  begin
    if FState = maSelected then
    begin
      Result := CreateRectRgn(0, 0, cSizePoint, cSizePoint);
      AddSquare(0, Height - cSizePoint, cSizePoint, Height);
      AddSquare(Width - cSizePoint, 0, Width, cSizePoint);
      AddSquare(Width - cSizePoint, Height - cSizePoint, Width, Height);
      h := Width div 2;
      hp := cSizePoint div 2;
      AddSquare(h - hp, 0, h + hp + 1, cSizePoint);
      AddSquare(h - hp, Height - cSizePoint, h + hp + 1, Height);
      h := Height div 2;
      AddSquare(0, h - hp, cSizePoint, h + hp + 1);
      AddSquare(Width - cSizePoint, h - hp, Width, h + hp + 1);
    end

    else
    begin
      Result := CreateRectRgn(0, 0, Width, Height);
      Rgn := CreateRectRgn(2, 2, Width - 2, Height - 2);
      CombineRgn(Result, Result, Rgn, RGN_DIFF);
      DeleteObject(Rgn);
    end;
  end;

  procedure SolidFrameRegion;
  var
    Rgn: THandle;
  begin
    Result := CreateRectRgn(1, 1, Width, Height);
    Rgn := CreateRectRgn(4, 4, Width - 3, Height - 3);
    CombineRgn(Result, Result, Rgn, RGN_DIFF);
    DeleteObject(Rgn);
  end;

begin
  DsgnInfo := FAttachedObject.DesignBehaviorInfo;

  if DsgnInfo.SelectorType = rwDBISBNone then
    Result := NULLREGION
  else
    GenericRegion;
end;

procedure TrmControlResizer.SetRegion;
begin
  if HandleAllocated then
    SetWindowRgn(Handle, GetRegion, True);
end;

procedure TrmControlResizer.CreateWindowHandle(const Params: TCreateParams);
begin
  inherited;
  SetRegion;
end;

procedure TrmControlResizer.SetResizerState(AState: TrmResizerState);
begin
  if AState <> FState then
  begin
    FState := AState;
    BoundsByControl;
    SetRegion;
  end;
end;

procedure TrmControlResizer.GoToNextTableCell(const ADirection: Char);
var
  NewCell: IrmTableCell;
begin
  NewCell := (FAttachedObject as IrmTableCell).GetClosestCell(ADirection);

  if Assigned(NewCell) then
  begin
    Attach(NewCell);
    if CanFocus then
      SetFocus;
    FDsgnComp.Notify(rmdObjectSelected, VarArrayOf([Integer(Pointer(NewCell))]));
  end;
end;

procedure TrmControlResizer.ShowIfNotInView;
var
  Frm: TWinControl;
begin
  Frm := Parent;
  while Assigned(Frm) and not (Frm is TScrollingWinControl) do
    Frm := Frm.Parent;

  if Assigned(Frm) then
    TScrollingWinControl(Frm).ScrollInView(Self);
end;

end.
