unit rwDesignInFormFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwDesignAreaFrm, rwDesignFormFrm, Menus;

type

  {TrwDesignInForm is visual form for design Input Form}

  TrwDesignInForm = class(TForm)
    Container: TrwDesignForm;
  private
  public
    procedure BeforeDestruction; override;
  end;

implementation

{$R *.DFM}

procedure TrwDesignInForm.BeforeDestruction;
begin
  Container.ClearSelection;

  inherited;
end;

end.
