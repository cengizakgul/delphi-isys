unit rmTrmPrintContainerPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmPrintControlPropFrm, rmOPEBooleanFrm, rmOPEColorFrm,
  rmOPEFontFrm, rmObjPropertyEditorFrm, rmOPEStringSingleLineFrm, StdCtrls,
  ExtCtrls, ComCtrls, rmOPEExpressionFrm, rmOPEBlockingControlFrm;

type
  TrmTrmPrintContainerProp = class(TrmTrmPrintControlProp)
    frmColor: TrmOPEColor;
    frmMultiPaged: TrmOPEBoolean;
  protected
    procedure GetPropsFromObject; override;
  end;

implementation

{$R *.dfm}

{ TrmTrmPrintableControlProp1 }

procedure TrmTrmPrintContainerProp.GetPropsFromObject;
begin
  inherited;
  frmMultiPaged.PropertyName := 'MultiPaged';
  frmMultiPaged.ShowPropValue;
end;

end.
