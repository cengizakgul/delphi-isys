unit rmTrmASCIIRecordPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, StdCtrls, ExtCtrls, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls, rmOPEMasterFieldsFrm, rmOPEQueryFrm,
  rmOPEBooleanFrm, rmOPEExpressionFrm, rmTypes, rmOPEBlockingControlFrm;

type
  TrmTrmASCIIRecordProp = class(TrmOPBasic)
    frmQuery: TrmOPEQuery;
    frmMasterFields: TrmOPEMasterFields;
    chbQueryDriven: TCheckBox;
    frmBlockParentIfEmpty: TrmOPEBoolean;
    pnlBlocking: TPanel;
    frmProduceBranchIf: TrmOPEBlockingControl;
    frmProduceRecordIf: TrmOPEBlockingControl;
    procedure frmQuerybtnQueryClick(Sender: TObject);
    procedure chbQueryDrivenClick(Sender: TObject);
  private
  protected
    procedure GetPropsFromObject; override;
  public
  end;


implementation

{$R *.dfm}

procedure TrmTrmASCIIRecordProp.frmQuerybtnQueryClick(Sender: TObject);
begin
  frmQuery.btnQueryClick(Sender);
  frmMasterFields.ShowPropValue;
end;

procedure TrmTrmASCIIRecordProp.chbQueryDrivenClick(Sender: TObject);
begin
  if chbQueryDriven.Tag = 0 then
  begin
    frmQuery.btnQuery.Enabled := chbQueryDriven.Checked;
    frmQuery.QueryEmpty := not chbQueryDriven.Checked;
  end;
end;

procedure TrmTrmASCIIRecordProp.GetPropsFromObject;
begin
  inherited;
  frmBlockParentIfEmpty.PropertyName := 'BlockParentIfEmpty';
  frmBlockParentIfEmpty.ShowPropValue;

  frmQuery.ShowPropValue;

  chbQueryDriven.Tag := 1;
  chbQueryDriven.Checked := not frmQuery.QueryEmpty;
  frmQuery.btnQuery.Enabled := chbQueryDriven.Checked;
  chbQueryDriven.Tag := 0;

  frmProduceBranchIf.BlockID := 'BranchCalc';
  frmProduceBranchIf.ShowPropValue;

  frmProduceRecordIf.BlockID := 'Calc';
  frmProduceRecordIf.ShowPropValue;
end;

end.
