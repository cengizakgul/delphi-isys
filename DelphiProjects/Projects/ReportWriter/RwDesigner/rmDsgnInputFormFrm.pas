unit rmDsgnInputFormFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, rmDsgnAreaFrm, Menus, Variants, Graphics, Controls, Forms,
  Dialogs, rwInputParamsFormFrm, rmLibCompChooseFrm, rmTypes, rwBasicClasses, rmDsgnTypes,
  rwDsgnUtils;

type
  TrmDsgnInputForm = class(TrmDsgnArea)
    pmTrwPageControl: TPopupMenu;
    pmAddTabSheet: TMenuItem;
    procedure pmAddTabSheetClick(Sender: TObject);
  private
    FForm: TrwInputParamsForm;

  public
    constructor Create(AOwner: TComponent); override;
    procedure   BeforeDestruction; override;

    function GetDsgnArea: TWinControl; override;
    function CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject; override;
  end;

implementation


{$R *.dfm}

{ TrmDsgnInputForm }

procedure TrmDsgnInputForm.BeforeDestruction;
begin
  FForm.Close;
  FreeAndNil(FForm);
  inherited;
end;

constructor TrmDsgnInputForm.Create(AOwner: TComponent);
begin
  inherited;
  FImageIndex := 3;
  FAreaType := rmDATInputForm;

  FForm := TrwInputParamsForm.Create(nil);
  FForm.Position := poDesigned;
  FForm.BorderIcons := [];
  FForm.Parent := Self;
  FForm.Top := 20;
  FForm.Left := 20;
  FForm.Show;
end;

function TrmDsgnInputForm.CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject;
var
  LibComp: IrmComponent;
begin
  if SameText(AClassName, 'LIBRARY') then
  begin
    LibComp := ChoiceLibComp(['TrwFormControl', 'TrwNoVisualComponent'], AOwner, Self);
    if Assigned(LibComp) then
      AClassName := LibComp.GetName
    else
      Exit;
  end;

  if not Assigned(AOwner) then
    AOwner := DesignObject;

  Result := AOwner.CreateChildObject(AClassName);
end;

function TrmDsgnInputForm.GetDsgnArea: TWinControl;
begin
  Result := FForm;
end;

procedure TrmDsgnInputForm.pmAddTabSheetClick(Sender: TObject);
begin
  RMDesigner.GetComponentDesigner.CreateRWObject('TrwTabSheet', PopupMenuSender, Point(0, 0));
end;

end.
