unit rwDesigner;

interface

uses
  Classes, SysUtils, rwEngineTypes, rwEngine, rwQueryBuilderFrm, rwQBInfo,
  rwDB, rwQBParamsFrm, sbAPI, ISBasicClasses, rwLogQuery, rwReport, rwWizardInfo, rwUtils,
  isUtils, ISDataAccessComponents, EvStreamUtils;

type
    //Common interface to access RW Designer
    IrwDesigner = interface
    ['{C56539DF-0103-499D-B1DC-4E5A7F4D461F}']
      function AppAdapter: IrwAppAdapter;

      // If AReport stream is not empty it opens designer and show this report
      // and returns modified report into this stream
      // Otherwise just open designer with the blank report
      procedure ShowDesigner(const AReport: IEvDualStream; const AReportName, ADefaultClassName: String);
      function  ShowDesigner2(const AReport: IEvDualStream; const AReportName, ADefaultClassName: String): IrwDesignerForm;

      // Query Builder
      function  RunQBQuery(const AQuery: TStream; const Params: TrwParamList; const AClientDataSet: TISBasicClientDataSet): IrwSBTable;

      function  ReCreateWizardReport(const pReportInfo: PTrwReportRec): Boolean;
    end;


    TrwDesigner = class(TInterfacedObject, IrwDesigner)
    protected
      function  AppAdapter: IrwAppAdapter;
      procedure ShowDesigner(const AReport: IEvDualStream; const AReportName, ADefaultClassName: String);
      function  ShowDesigner2(const AReport: IEvDualStream; const AReportName, ADefaultClassName: String): IrwDesignerForm;
      function  RunQBQuery(const AQuery: TStream; const Params: TrwParamList; const AClientDataSet: TISBasicClientDataSet): IrwSBTable;
      function  ReCreateWizardReport(const pReportInfo: PTrwReportRec): Boolean;
    end;


    // You can have only ONE instance of the Designer in application.
    procedure InitializeRWDesigner;
    procedure DestroyRWDesigner;
    function  rwDesignerExt: IrwDesigner;


implementation

uses rwDesignerFrm, rmDesignerFrm;

var
  vDesigner: IrwDesigner = nil;


procedure InitializeRWDesigner;
var
  D: TrwDesigner;
begin
  D := TrwDesigner.Create;
  vDesigner := D;
end;

procedure DestroyRWDesigner;
begin
  vDesigner := nil;
end;

function rwDesignerExt: IrwDesigner;
begin
  Result := vDesigner;
  if not Assigned(Result) then
    raise ErwException.Create('AppAdapter is not initialized');
end;

{ TrwDesigner }

function TrwDesigner.AppAdapter: IrwAppAdapter;
begin
  Result := RWEngineExt.AppAdapter;
end;

function TrwDesigner.ReCreateWizardReport(const pReportInfo: PTrwReportRec): Boolean;
var
  Report: TrwReport;
  WI: TrwWizardInfo;
begin
  Result := False;

  Report := TrwReport.Create(nil);
  try
    pReportInfo^.Data.Position := 0;
    Report.LoadFromStream(pReportInfo^.Data);

    if Report.WizardInfo.Size = 0 then
      Exit;

    pReportInfo.Data.Size := 0;
    Report.WizardInfo.Position := 0;
    pReportInfo.Data.CopyFrom(Report.WizardInfo.RealStream, Report.WizardInfo.Size);
  finally
    FreeAndNil(Report);
  end;

  pReportInfo.Data.Position := 0;
  WI := TrwWizardInfo(pReportInfo.Data.ReadComponent(nil));
  try
    WI.CreateReport;
    pReportInfo.Data.Size := 0;

    WI.Report.WizardInfo.Clear;
    WI.Report.WizardInfo.WriteComponent(WI);
    WI.Report.SaveToStream(pReportInfo.Data);
  finally
    WI.Free;
  end;

  Result := True;
end;


function TrwDesigner.RunQBQuery(const AQuery: TStream; const Params: TrwParamList; const AClientDataSet: TISBasicClientDataSet): IrwSBTable;
var
  Q: TrwQBQuery;
  SQL: String;
  i: Integer;
  Prm: TsbParam;
begin
  Q := TrwQBQuery.Create(nil);
  try
    AQuery.Position := 0;
    Q.ReadFromStream(AQuery);
    SQL := Q.SQL;
  finally
    FreeAndNil(Q);
  end;


  Result := TrwLQQuery.Create;
  TrwLQQuery(Result.RealObject).SQL.Text := SQL;

  for i := Low(Params) to High(Params) do
  begin
    Prm := TrwLQQuery(Result.RealObject).Params.ParamByName(Params[i].Name);
    if Assigned(Prm) then
      Prm.Value := Params[i].Value;
  end;

  TrwLQQuery(Result.RealObject).OpenSQL;

  if Assigned(AClientDataSet) then
    sbSBCursorToClientDataSet(TsbTableCursor(Result.RealObject), AClientDataSet);
end;

procedure TrwDesigner.ShowDesigner(const AReport: IEvDualStream; const AReportName, ADefaultClassName: String);
begin
  if not Assigned(AReport) then

    if ADefaultClassName = 'TrmReport' then
      ShowModalRMDesignerWithReport(AReport, AReportName)
    else
      ShowModalRWDesignerWithReport(AReport, AReportName)

  else
    if AReport.Size > 0 then
      if IsRMReport(AReport.RealStream) then
        ShowModalRMDesignerWithReport(AReport, AReportName)
      else
        ShowModalRWDesignerWithReport(AReport, AReportName)
    else
      if ADefaultClassName = 'TrmReport' then
        ShowModalRMDesignerWithReport(AReport, AReportName)
      else
        ShowModalRWDesignerWithReport(AReport, AReportName);
end;


function TrwDesigner.ShowDesigner2(const AReport: IEvDualStream; const AReportName, ADefaultClassName: String): IrwDesignerForm;
var
  ClName: String;
begin
  if Assigned(AReport) and (AReport.Size > 0) then
    if IsRMReport(AReport.RealStream) then
      ClName := 'TrmReport'
    else
      ClName := 'TrwReport'
  else
    ClName := ADefaultClassName;

  if ClName = 'TrmReport' then
    Result := ShowRMDesigner(AReport, AReportName)
  else
    Result := ShowRWDesigner(AReport, AReportName);
end;

end.
