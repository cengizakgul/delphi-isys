unit rmActionFormFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmChildFormFrm, rmDsgnTypes, rmTypes, ExtCtrls;

type
  TrmDragAndDropObject = class(TPanel)
  private
    FRMObject: IrmDesignObject;
  public
    property RMObject: IrmDesignObject read FRMObject;
    procedure StartDrag(const AObject: IrmDesignObject);
  end;


  TrmActionForm = class(TrmChildForm, IrmDsgnActionHandler)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    FDragAndDropObject: TrmDragAndDropObject;
    procedure FOnEndDrag(Sender, Target: TObject; X, Y: Integer);

  protected
    FActiveObject: IrmDesignObject;

    procedure Initialize(const AParams: Variant); virtual;
    function  AllowDrag(const AObject: IrmDesignObject): Boolean; virtual;
    procedure EndDrag(const AObject: IrmDesignObject); virtual;

    //interface
    function  VCLObject: TObject;
    function  ObjectMouseDown(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean; virtual;
    function  ObjectMouseUp(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean; virtual;
    function  ObjectMouseDblClick(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean; virtual;
    function  ObjectMouseMove(const AMousePos: TPoint; AObject: IrmDesignObject): Boolean; virtual;
    function  ObjectSetCursor(AObject: IrmDesignObject): Boolean;
  public
    class procedure StartAction(const AParams: Variant); overload;
    class procedure StartAction(const AActiveObject: IrmDesignObject; const AParams: Variant); overload;
  end;


implementation

{$R *.dfm}

uses
  rwDsgnUtils;

{ TrmActionForm }

function TrmActionForm.ObjectMouseDblClick(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean;
begin
  Result := True;
end;

function TrmActionForm.ObjectMouseDown(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean;
begin
  if AllowDrag(AObject) then
  begin
    FDragAndDropObject := TrmDragAndDropObject.Create(self);
    FDragAndDropObject.OnEndDrag := FOnEndDrag;
    FDragAndDropObject.StartDrag(AObject);
  end;
    
  Result := True;
end;

function TrmActionForm.ObjectMouseUp(const AButton: TMouseButton; const AMousePos: TPoint; AObject: IrmDesignObject): Boolean;
begin
  Result := True;
end;


procedure TrmActionForm.FormCreate(Sender: TObject);
begin
  inherited;
  RMDesigner.GetComponentDesigner.Notify(rmdActionStarted, Integer(Pointer(Self as IrmDsgnActionHandler)));
end;


procedure TrmActionForm.FormDestroy(Sender: TObject);
begin
  inherited;
  FActiveObject := nil;
  RMDesigner.GetComponentDesigner.Notify(rmdActionStoped, Integer(Pointer(Self as IrmDsgnActionHandler)));
end;

class procedure TrmActionForm.StartAction(const AActiveObject: IrmDesignObject; const AParams: Variant);
var
  Frm: TrmActionForm;
begin
  Frm := Create(Application);
  with Frm do
  begin
    FActiveObject := AActiveObject;
    Parent := TForm(RMDesigner.VCLObject);
    Initialize(AParams);
    Show;
  end;
end;


class procedure TrmActionForm.StartAction(const AParams: Variant);
var
  L: TrmListOfObjects;
begin
  L := RMDesigner.GetComponentDesigner.GetSelectedObjects;
  if Length(L) = 1 then
    StartAction(L[0], AParams);
end;

procedure TrmActionForm.Initialize(const AParams: Variant);
begin
end;

function TrmActionForm.ObjectMouseMove(const AMousePos: TPoint; AObject: IrmDesignObject): Boolean;
begin
  Result := True;
end;

procedure TrmActionForm.FOnEndDrag(Sender, Target: TObject; X, Y: Integer);
begin
  FDragAndDropObject.EndDrag(True);
  if Assigned(Target) then
    EndDrag(FDragAndDropObject.RMObject);
  FreeAndNil(FDragAndDropObject);
end;

procedure TrmActionForm.EndDrag(const AObject: IrmDesignObject);
begin
end;

function TrmActionForm.ObjectSetCursor(AObject: IrmDesignObject): Boolean;
var
  Cr: TCursor;
begin
  Cr := crDefault;
  Windows.SetCursor(Screen.Cursors[Cr]);
  Result := True;
end;


function TrmActionForm.AllowDrag(const AObject: IrmDesignObject): Boolean;
begin
  Result := False;
end;

function TrmActionForm.VCLObject: TObject;
begin
  Result := Self;
end;

{ TrmDragAndDropObject }

procedure TrmDragAndDropObject.StartDrag(const AObject: IrmDesignObject);
begin
  FRMObject := AObject;
  BeginDrag(False, 1);
end;


end.
