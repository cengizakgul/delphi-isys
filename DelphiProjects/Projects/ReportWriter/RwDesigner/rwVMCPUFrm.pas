unit rwVMCPUFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, mwCustomEdit, rwVirtualMachine, rwDsgnUtils, rwUtils,
  rwBasicClasses, rwDebugInfo, ActnList, rwTypes, Variants, rwEngineTypes;

type
  TrwVMCPU = class(TForm)
    Panel1: TPanel;
    Panel6: TPanel;
    lbRegisters: TListBox;
    lbStack: TListBox;
    Splitter4: TSplitter;
    Panel4: TPanel;
    Splitter5: TSplitter;
    Panel5: TPanel;
    lbHeap: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    aclDesignerActions: TActionList;
    actWatches: TAction;
    actStepOver: TAction;
    actTraceInto: TAction;
    actAddWatch: TAction;
    actRun: TAction;
    actTerminate: TAction;
    actBrkPoint: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure actStepOverExecute(Sender: TObject);
    procedure actTraceIntoExecute(Sender: TObject);
    procedure actAddWatchExecute(Sender: TObject);
    procedure actWatchesExecute(Sender: TObject);
    procedure actRunExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actTerminateExecute(Sender: TObject);
    procedure actBrkPointExecute(Sender: TObject);
  private
    HandlerAsm: TmwCustomEdit;
    FCurrentAsmLine: Integer;
    FCurrenAsmCS: TrwVMCodeSegment;
    FCurrentAsmPosition: Integer;
    FBaseAddress: Integer;
    FReportAsmText: String;
    FLibFunctAsmText: String;
    FLibCompAsmText: String;

    procedure OnSpecialAsmLineColors(Sender: TObject; Line: integer; var Special: boolean; var FG, BG: TColor);
    procedure SetCurrentAsmCS(const Value: TrwVMCodeSegment);
    procedure SetCurrentAsmPosition(Value: Integer);
    procedure UpdateRegistersInfo;
    procedure UpdateStackInfo;
    procedure UpdateHeapInfo;
    function  ConvertValueToStr(const AValue: Variant): string;
  public
    property CurrentAsmPosition: Integer read FCurrentAsmPosition write SetCurrentAsmPosition;
    property CurrentAsmLine: Integer read FCurrentAsmLine;
    property CurrentAsmCS: TrwVMCodeSegment read FCurrenAsmCS write SetCurrentAsmCS;

    procedure SyncPosition;
    procedure SyncCodeSegment;
    procedure InvalidateEventHandler;
  end;


  function  ShowVMCPU: TrwVMCPU;
  procedure CloseVMCPU;
  function  IsVMCPUVisible: Boolean;

implementation

{$R *.DFM}
uses rwDesignerFrm, rwReport;

var
  FVMCPU: TrwVMCPU;


function IsVMCPUVisible: Boolean;
begin
  Result :=  Assigned(FVMCPU) and FVMCPU.Visible;
end;


function ShowVMCPU: TrwVMCPU;
begin
  if not Assigned(FVMCPU) then
  begin
    FVMCPU := TrwVMCPU.Create(nil);
    FVMCPU.Show;
  end
  else
  begin
    if not IsVMCPUVisible then
      FVMCPU.Show;
    SetWindowPos(FVMCPU.Handle, HWND_TOP, 0, 0, 0, 0,
      SWP_NOACTIVATE or SWP_NOSIZE or SWP_NOMOVE);
  end;

  Result := FVMCPU;
end;


procedure CloseVMCPU;
begin
  if Assigned(FVMCPU) then
  begin
    FVMCPU.Free;
    FVMCPU := nil;
  end;
end;


function TrwVMCPU.ConvertValueToStr(const AValue: Variant): string;
begin
  if VarIsNull(AValue) then
    Result := '<null>'

  else if VarIsArray(AValue) then
    Result := '<Array>'

  else
    Result := VarToStr(AValue);
end;


procedure TrwVMCPU.FormCreate(Sender: TObject);
begin
  HandlerAsm := TmwCustomEdit.Create(Self);
  HandlerAsm.Align := alClient;
  HandlerAsm.Parent := Panel4;
//  HandlerAsm.HighLighter := GeneralSyn;
  HandlerAsm.ReadOnly := True;
  HandlerAsm.OnSpecialLineColors := OnSpecialAsmLineColors;
end;


procedure TrwVMCPU.OnSpecialAsmLineColors(Sender: TObject; Line: integer;
  var Special: boolean; var FG, BG: TColor);
var
  a: integer;
  brkp: Boolean;
begin
  if (Length(HandlerAsm.Lines[Line-1]) < 17) or (HandlerAsm.Lines[Line-1][1] <> '0') then
  begin
    FG := clBlue;
    Special := True;
    Exit;
  end;

  a := StrToInt(Copy(HandlerAsm.Lines[Line-1], 1, 7));
  brkp := DebugInfo.BreakPointsList.FindBreakPoint(a) <> -1;

  if brkp then
  begin
    BG := clRed;
    FG := clWhite;
    Special := True;
  end
  else
    brkp := False;

  if Line = CurrentAsmLine then
  begin
    if brkp then
      BG := clMaroon
    else
      BG := clNavy;
    FG := clWhite;
    Special := True;
  end;
end;


procedure TrwVMCPU.SetCurrentAsmCS(const Value: TrwVMCodeSegment);
begin
  if FCurrenAsmCS = Value then
    Exit;

  FCurrenAsmCS := Value;
  FBaseAddress := GetOffSet(FCurrenAsmCS, Length(TrwReport(ReportDesigner.DesignPaperReport[0].Report).CompiledCode) + rwVMHeapSize);
  case FCurrenAsmCS of
    rcsReport:       begin
                        if Length(FReportAsmText) = 0 then
                        begin
                          FReportAsmText := GetThreadTmpDir + '~tmpASM1.txt';
                          DisassembleCompiledCode(FReportAsmText, VM.ReportCode,
                            Addr(TrwReport(ReportDesigner.DesignPaperReport[0].Report).DebInf), FBaseAddress, VM.HeapTop - 1);
                        end;
                        HandlerAsm.Lines.LoadFromFile(FReportAsmText);
                     end;

    rcsLibFunctions:  begin
                        if Length(FLibFunctAsmText) = 0 then
                        begin
                          FLibFunctAsmText := GetThreadTmpDir + '~tmpASM2.txt';
                          DisassembleCompiledCode(FLibFunctAsmText, Addr(SystemLibFunctions.CompiledCode), SystemLibFunctions.GetDebInf, FBaseAddress);
                        end;
                        HandlerAsm.Lines.LoadFromFile(FLibFunctAsmText);
                      end;

    rcsLibComponents: begin
                        if Length(FLibCompAsmText) = 0 then
                        begin
                          FLibCompAsmText := GetThreadTmpDir + '~tmpASM3.txt';
                          DisassembleCompiledCode(FLibCompAsmText, SystemLibComponents.GetCompiledCode, SystemLibComponents.GetDebInf, FBaseAddress);
                        end;
                        HandlerAsm.Lines.LoadFromFile(FLibCompAsmText);
                      end;
  end;
end;


procedure TrwVMCPU.SetCurrentAsmPosition(Value: Integer);
var
  i, a, addr: Integer;
begin
  FCurrentAsmPosition := Value;

  if Value = -1 then
  begin
    Visible := False;
    Exit;
  end;

  addr := 0;
  Value := Value + FBaseAddress;
  for i := 0 to HandlerAsm.Lines.Count - 1 do
  begin
    if (Length(HandlerAsm.Lines[i]) < 17) or (HandlerAsm.Lines[i][1] <> '0') then
      Continue;
    a := StrToInt(Copy(HandlerAsm.Lines[i], 1, 7));
    if a = Value then
    begin
      addr := i+1;
      break;
    end;
  end;

  if addr = 0 then
    raise ErwException.Create('Can not syncronize debug info');

  HandlerAsm.BeginUpdate;
  i := FCurrentAsmLine;
  FCurrentAsmLine := -1;
  HandlerAsm.InvalidateLine(i);
  FCurrentAsmLine := addr;
  HandlerAsm.EndUpdate;

  UpdateRegistersInfo;
  UpdateStackInfo;

  UpdateHeapInfo;
  HandlerAsm.InvalidateLine(FCurrentAsmLine);
  HandlerAsm.CaretY := FCurrentAsmLine;
  HandlerAsm.CaretX := 1;
  Ready;
end;


procedure TrwVMCPU.SyncCodeSegment;
begin
  CurrentAsmCS := DebugInfo.CodeSegment;
end;


procedure TrwVMCPU.SyncPosition;
begin
  CurrentAsmPosition := DebugInfo.CurrentAsmPosition;
end;


procedure TrwVMCPU.UpdateHeapInfo;
var
  i: Integer;
begin
  lbHeap.Clear;
  for i := VM.HeapTop to VM.HeapPointer - 1 do
    lbHeap.Items.Add(FormatFloat('0000000', i) + '          ' + ConvertValueToStr(VM.ReportCode^[i]));
end;


procedure TrwVMCPU.UpdateRegistersInfo;
var
  lCS: string;
begin
  lbRegisters.Clear;
  lbRegisters.Items.Add('A           ' + ConvertValueToStr(VM.RegA));
  lbRegisters.Items.Add('B           ' + ConvertValueToStr(VM.RegB));
  lbRegisters.Items.Add('F           ' + ConvertValueToStr(VM.Flag));
  lbRegisters.Items.Add('SP          ' + FormatFloat('0000000', VM.StackPointer));

  case FCurrenAsmCS of
    rcsReport:        lCS := 'Report';
    rcsLibFunctions:  lCS := 'Library Functions';
    rcsLibComponents: lCS := 'Library Components';
  else
    lCS := 'Unknown';
  end;
  lbRegisters.Items.Add('CS          ' + lCS);

  lbRegisters.Items.Add('IP          ' + FormatFloat('0000000', VM.InstrPointer + FBaseAddress));
  lbRegisters.Items.Add('HeapTop     ' + FormatFloat('0000000', VM.CurrHeapTop));
end;


procedure TrwVMCPU.UpdateStackInfo;
var
  i: Integer;
begin
  lbStack.Clear;
  for i := VM.StackPointer to High(VM.ReportCode^) do
    lbStack.Items.Add(FormatFloat('0000000', i) + '          ' + ConvertValueToStr(VM.ReportCode^[i]));
end;

procedure TrwVMCPU.FormDestroy(Sender: TObject);
begin
  FVMCPU := nil;
  if Length(FReportAsmText) > 0 then
    DeleteFile(FReportAsmText);
  if Length(FLibFunctAsmText) > 0 then
    DeleteFile(FLibFunctAsmText);
  if Length(FLibCompAsmText) > 0 then
    DeleteFile(FLibCompAsmText);
end;


procedure TrwVMCPU.actStepOverExecute(Sender: TObject);
begin
  ReportDesigner.actStepOver.Execute;
end;


procedure TrwVMCPU.actTraceIntoExecute(Sender: TObject);
begin
  ReportDesigner.actTraceInto.Execute;
end;


procedure TrwVMCPU.actAddWatchExecute(Sender: TObject);
begin
  ReportDesigner.actAddWatch.Execute;
end;


procedure TrwVMCPU.actWatchesExecute(Sender: TObject);
begin
  ReportDesigner.actWatches.Execute;
end;

procedure TrwVMCPU.actRunExecute(Sender: TObject);
begin
  ReportDesigner.actRun.Execute;
end;


procedure TrwVMCPU.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;


procedure TrwVMCPU.actTerminateExecute(Sender: TObject);
begin
  ReportDesigner.actTerminate.Execute;
end;


procedure TrwVMCPU.actBrkPointExecute(Sender: TObject);
var
  i, a: Integer;
begin
  a := StrToInt(Copy(HandlerAsm.LineText, 1, 7));

  i := DebugInfo.BreakPointsList.FindBreakPoint(a);
  if i = -1 then
    DebugInfo.BreakPointsList.Add('', '', '', -1, a)
  else
    DebugInfo.BreakPointsList.Delete(i);
  HandlerAsm.InvalidateLine(HandlerAsm.CaretY);
end;


procedure TrwVMCPU.InvalidateEventHandler;
begin
  HandlerAsm.Invalidate;
end;


initialization
  FVMCPU := nil;

finalization
  CloseVMCPU;

end.
