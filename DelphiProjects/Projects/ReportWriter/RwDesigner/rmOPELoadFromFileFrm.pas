unit rmOPELoadFromFileFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls;

type
  TrmOPELoadFromFile = class(TrmObjPropertyEditor)
    btnImage: TButton;
    dlgOpenFile: TOpenDialog;
    procedure btnImageClick(Sender: TObject);
  protected
    procedure LoadPropertyFromFile(const AFileName: String); virtual;
  end;

implementation

{$R *.dfm}

procedure TrmOPELoadFromFile.btnImageClick(Sender: TObject);
begin
  if dlgOpenFile.Execute then
    LoadPropertyFromFile(dlgOpenFile.FileName);
end;

procedure TrmOPELoadFromFile.LoadPropertyFromFile(const AFileName: String);
begin
  NotifyOfChanges;
end;

end.
