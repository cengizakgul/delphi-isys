inherited rwBandsPropertyEditor: TrwBandsPropertyEditor
  Left = 300
  Top = 184
  VertScrollBar.Range = 0
  BorderStyle = bsToolWindow
  Caption = 'Report Bands'
  ClientHeight = 221
  ClientWidth = 576
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOK: TButton
    Left = 495
    TabOrder = 2
  end
  inherited btnCancel: TButton
    Left = 495
    TabOrder = 3
  end
  object gbBands: TGroupBox
    Left = 7
    Top = 2
    Width = 218
    Height = 213
    Caption = 'General Bands'
    TabOrder = 0
    object cbPageHeader: TCheckBox
      Left = 10
      Top = 18
      Width = 97
      Height = 17
      Caption = 'Page Header'
      TabOrder = 0
    end
    object cbPageFooter: TCheckBox
      Left = 10
      Top = 137
      Width = 97
      Height = 17
      Caption = 'Page Footer'
      TabOrder = 5
    end
    object cbPageStyle: TCheckBox
      Left = 117
      Top = 89
      Width = 97
      Height = 17
      Caption = 'Page Style'
      TabOrder = 9
    end
    object cbTitle: TCheckBox
      Left = 10
      Top = 42
      Width = 97
      Height = 17
      Caption = 'Title'
      TabOrder = 1
    end
    object cbSummary: TCheckBox
      Left = 10
      Top = 113
      Width = 97
      Height = 17
      Caption = 'Summary'
      TabOrder = 4
    end
    object cbHeader: TCheckBox
      Left = 10
      Top = 66
      Width = 97
      Height = 17
      Caption = 'Header'
      TabOrder = 2
    end
    object cbFooter: TCheckBox
      Left = 10
      Top = 89
      Width = 97
      Height = 17
      Caption = 'Footer'
      TabOrder = 3
    end
    object cbDetail: TCheckBox
      Left = 117
      Top = 18
      Width = 97
      Height = 17
      Caption = 'Detail'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 6
    end
    object cbSubDetail: TCheckBox
      Left = 117
      Top = 42
      Width = 97
      Height = 17
      Caption = 'SubDetail'
      TabOrder = 7
    end
    object cbSubSummary: TCheckBox
      Left = 117
      Top = 66
      Width = 97
      Height = 17
      Caption = 'SubSummary'
      TabOrder = 8
    end
    object cbCustom: TCheckBox
      Left = 117
      Top = 113
      Width = 71
      Height = 17
      Caption = 'Custom'
      TabOrder = 10
    end
  end
  object gbGroups: TGroupBox
    Left = 235
    Top = 2
    Width = 252
    Height = 213
    Caption = 'Groups'
    TabOrder = 1
    DesignSize = (
      252
      213)
    object lbGroups: TListBox
      Left = 10
      Top = 18
      Width = 147
      Height = 82
      ItemHeight = 13
      TabOrder = 0
      OnClick = lbGroupsClick
    end
    object btnAddGroup: TButton
      Tag = 267
      Left = 166
      Top = 18
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Add'
      TabOrder = 1
      OnClick = btnAddGroupClick
    end
    object btnDeleteGroup: TButton
      Tag = 267
      Left = 166
      Top = 47
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Delete'
      TabOrder = 2
      OnClick = btnDeleteGroupClick
    end
    object btnUp: TButton
      Tag = 267
      Left = 166
      Top = 76
      Width = 36
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Up'
      TabOrder = 3
      OnClick = btnUpClick
    end
    object btnDown: TButton
      Tag = 267
      Left = 205
      Top = 76
      Width = 36
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Down'
      TabOrder = 4
      OnClick = btnDownClick
    end
    object gbGroup: TGroupBox
      Left = 10
      Top = 108
      Width = 231
      Height = 94
      Caption = 'Group Description'
      Enabled = False
      TabOrder = 5
      object Label1: TLabel
        Left = 10
        Top = 21
        Width = 53
        Height = 13
        Caption = 'Field Name'
      end
      object cbFieldsList: TComboBox
        Left = 75
        Top = 18
        Width = 146
        Height = 21
        DropDownCount = 16
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbFieldsListChange
        OnExit = cbFieldsListExit
      end
      object cbGroupHeader: TCheckBox
        Left = 10
        Top = 47
        Width = 97
        Height = 17
        Caption = 'Group Header'
        TabOrder = 1
        OnClick = cbGroupHeaderClick
      end
      object cbGroupFooter: TCheckBox
        Left = 10
        Top = 70
        Width = 97
        Height = 17
        Caption = 'Group Footer'
        TabOrder = 2
        OnClick = cbGroupFooterClick
      end
      object cbGroupDisabled: TCheckBox
        Left = 124
        Top = 58
        Width = 97
        Height = 17
        Caption = 'Group Disabled'
        TabOrder = 3
        OnClick = cbGroupDisabledClick
      end
    end
  end
end
