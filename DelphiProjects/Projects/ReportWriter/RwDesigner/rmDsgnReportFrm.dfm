inherited rmDsgnReport: TrmDsgnReport
  Width = 607
  Height = 325
  inherited spltLeft: TSplitter
    Height = 272
  end
  inherited spltRight: TSplitter
    Left = 554
    Height = 272
  end
  inherited spltBottom: TSplitter
    Top = 272
    Width = 607
  end
  inherited pnlDockBottom: TPanel
    Top = 275
    Width = 607
  end
  inherited pnlDockLeft: TPanel
    Height = 272
  end
  inherited pnlDockRight: TPanel
    Left = 557
    Height = 272
  end
  inherited pnlList: TPanel
    Height = 272
    inherited lvItems: TListView
      Height = 272
    end
  end
  inherited pnlWorkArea: TPanel
    Width = 401
    Height = 272
  end
  inherited opdImport: TOpenDialog
    Filter = 
      'Report Master Report (*.rwr)|*.rwr|Report Master Report (*.txt)|' +
      '*.txt|All Files (*.*)|*.*'
    Title = 'Import Report'
    Left = 236
    Top = 112
  end
  inherited svdExport: TSaveDialog
    Filter = 
      'Report Master Report (*.rwr)|*.rwr|Report Master Report (*.txt)|' +
      '*.txt|All Files (*.*)|*.*'
    Title = 'Export Report'
    Left = 276
    Top = 112
  end
end
