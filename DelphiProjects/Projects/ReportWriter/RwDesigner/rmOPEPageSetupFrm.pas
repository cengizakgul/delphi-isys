unit rmOPEPageSetupFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, StdCtrls, Mask, wwdbedit, Wwdbspin,
  ExtCtrls, rwTypes, rwUtils, rwDsgnUtils, rmTypes;



type
  TrmOPEPageSetup = class(TrmObjPropertyEditor)
    gbSize: TGroupBox;
    lblPaperSize: TLabel;
    lblWidth: TLabel;
    lblHeight: TLabel;
    cbPaperNames: TComboBox;
    speWidth: TwwDBSpinEdit;
    speHeight: TwwDBSpinEdit;
    gbxOrientation: TGroupBox;
    rbPortrait: TRadioButton;
    rbLandscape: TRadioButton;
    chbDuplexing: TCheckBox;
    gbMargins: TGroupBox;
    pnPreview: TPanel;
    pbPreview: TPaintBox;
    imDuplex: TImage;
    speTopMargin: TwwDBSpinEdit;
    speBottomMargin: TwwDBSpinEdit;
    speRightMargin: TwwDBSpinEdit;
    speLeftMargin: TwwDBSpinEdit;
    procedure cbPaperNamesChange(Sender: TObject);
    procedure rbPortraitClick(Sender: TObject);
    procedure pbPreviewClick(Sender: TObject);
    procedure imDuplexClick(Sender: TObject);
    procedure pbPreviewPaint(Sender: TObject);
    procedure speWidthChange(Sender: TObject);
  private
    procedure Initialize;
    procedure UpdatePreview;
    procedure UpdateOrientation;
    procedure SetPaperNameBySize(AWidth, AHeight: Integer);
  public
    procedure ShowPropValue; override;
    procedure ApplyChanges; override;
  end;

implementation

{$R *.dfm}

procedure TrmOPEPageSetup.cbPaperNamesChange(Sender: TObject);
begin
  if Tag = 0 then
  begin
    if cbPaperNames.ItemIndex <> Ord(psCustom) then
    begin
      speHeight.Value :=
        Round(ConvertUnit(cPaperInfo[TrwPaperSize(Integer(cbPaperNames.Items.Objects[cbPaperNames.ItemIndex]))].Height / 10,
        utMillimeters, DesignUnits)*1000)/1000;

      speWidth.Value :=
        Round(ConvertUnit(cPaperInfo[TrwPaperSize(Integer(cbPaperNames.Items.Objects[cbPaperNames.ItemIndex]))].Width / 10,
        utMillimeters, DesignUnits)*1000)/1000;
    end;

    if rbLandscape.Checked then
      UpdateOrientation;
  end;

  speHeight.Enabled := Integer(cbPaperNames.Items.Objects[cbPaperNames.ItemIndex]) = Ord(psCustom);
  speWidth.Enabled := speHeight.Enabled;

  UpdatePreview;
end;

procedure TrmOPEPageSetup.rbPortraitClick(Sender: TObject);
begin
  if Tag = 0 then
  begin
    UpdateOrientation;
    UpdatePreview;
  end;  
end;

procedure TrmOPEPageSetup.pbPreviewClick(Sender: TObject);
begin
  rbPortrait.Checked := not rbPortrait.Checked;
  rbLandscape.Checked := not rbPortrait.Checked;
end;

procedure TrmOPEPageSetup.imDuplexClick(Sender: TObject);
begin
  pbPreview.OnClick(nil);
end;

procedure TrmOPEPageSetup.pbPreviewPaint(Sender: TObject);
var
  R, R1:  TRect;
  i, d: Integer;
begin
  with pbPreview.Canvas do
  begin
    Brush.Color := clWhite;
    Brush.Style := bsSolid;
    Pen.Width := 1;
    FillRect(pbPreview.ClientRect);

    R := pbPreview.ClientRect;
    R.Top := Round(R.Bottom * speTopMargin.Value / speHeight.Value);
    R.Bottom := R.Bottom - Round(R.Bottom * speBottomMargin.Value / speHeight.Value);
    R.Left := Round(R.Right * speLeftMargin.Value / speWidth.Value);
    R.Right := R.Right - Round(R.Right * speRightMargin.Value / speWidth.Value);

    d := 10;
    Pen.Color := clGray;
    Brush.Style := bsClear;
    Rectangle(R);
    R1 := R;
    R1.Bottom := R1.Top + d * 2;
    Brush.Style := bsSolid;
    Brush.Color := 16704474;
    Rectangle(R1);

    i := R.Top + d * 3;
    while i <= R.Bottom - d do
    begin
      MoveTo(R.Left, i);
      LineTo(R.Right, i);
      Inc(i, d);
    end;

    d := 20;
    i := R.Left + d;
    while i <= R.Right - d do
    begin
      MoveTo(i, R.Top);
      LineTo(i, R.Bottom);
      Inc(i, d);
    end;

    Brush.Style := bsClear;
    Pen.Color := clBlack;
    Rectangle(pbPreview.ClientRect);
  end;
end;

procedure TrmOPEPageSetup.UpdateOrientation;
var
  h: Extended;
begin
  if Tag = 0 then
  begin
    h := speHeight.Value;
    speHeight.Value := speWidth.Value;
    speWidth.Value := h;
  end;  
end;

procedure TrmOPEPageSetup.UpdatePreview;
begin
  if speHeight.Value > speWidth.Value then
  begin
    pbPreview.Height := pnPreview.ClientHeight - 20;
    pbPreview.Width := Round(pbPreview.Height * (speWidth.Value / speHeight.Value));
  end
  else
  begin
    pbPreview.Width := pnPreview.ClientHeight - 20;
    pbPreview.Height := Round(pbPreview.Width * (speHeight.Value / speWidth.Value));
  end;

  pbPreview.Top := Round((pnPreview.ClientHeight - pbPreview.Height) / 2);
  pbPreview.Left := Round((pnPreview.ClientWidth - pbPreview.Width) / 2);

  pbPreview.Invalidate;
  if chbDuplexing.Checked then
  begin
    imDuplex.Top := pbPreview.Top + pbPreview.Height - imDuplex.Height;
    imDuplex.Left := pbPreview.Left + pbPreview.Width - imDuplex.Width;
    imDuplex.Visible := True;
  end
  else
    imDuplex.Visible := False;

  if Tag = 0 then
    NotifyOfChanges;
end;

procedure TrmOPEPageSetup.Initialize;
var
  i: Integer;
  C: IrmDesignObject;
begin
  Tag := 1;

  C := TComponent(LinkedObject) as IrmDesignObject;

  for i := Ord(Low(cPaperInfo)) to Ord(High(cPaperInfo)) do
    cbPaperNames.Items.AddObject(cPaperInfo[TrwPaperSize(i)].Name, Pointer(i));

  speHeight.Value := Round(ConvertUnit(C.GetHeight, utLogicalPixels, DesignUnits)*1000)/1000;
  speWidth.Value := Round(ConvertUnit(C.GetWidth, utLogicalPixels, DesignUnits)*1000)/1000;
  speTopMargin.Value := Round(ConvertUnit(C.GetPropertyValue('TopMargin'), utLogicalPixels, DesignUnits)*1000)/1000;
  speBottomMargin.Value := Round(ConvertUnit(C.GetPropertyValue('BottomMargin'), utLogicalPixels, DesignUnits)*1000)/1000;
  speLeftMargin.Value := Round(ConvertUnit(C.GetPropertyValue('LeftMargin'), utLogicalPixels, DesignUnits)*1000)/1000;
  speRightMargin.Value := Round(ConvertUnit(C.GetPropertyValue('RightMargin'), utLogicalPixels, DesignUnits)*1000)/1000;

  chbDuplexing.Checked := C.GetPropertyValue('Duplexing');

  if C.GetPropertyValue('PaperOrientation') = rpoPortrait then
    rbPortrait.Checked := True
  else
    rbLandscape.Checked := True;

  SetPaperNameBySize(Round(speWidth.Value), Round(speHeight.Value));

  Tag := 0;
end;

procedure TrmOPEPageSetup.ApplyChanges;
var
  C: IrmDesignObject;
begin
  C := TComponent(LinkedObject) as IrmDesignObject;

  C.SetPropertyValue('TopMargin', Round(ConvertUnit(speTopMargin.Value, DesignUnits, utLogicalPixels)));
  C.SetPropertyValue('BottomMargin', Round(ConvertUnit(speBottomMargin.Value, DesignUnits, utLogicalPixels)));
  C.SetPropertyValue('LeftMargin', Round(ConvertUnit(speLeftMargin.Value, DesignUnits, utLogicalPixels)));
  C.SetPropertyValue('RightMargin', Round(ConvertUnit(speRightMargin.Value, DesignUnits, utLogicalPixels)));

  if (rbPortrait.Checked) then
    C.SetPropertyValue('PaperOrientation', rpoPortrait)
  else
    C.SetPropertyValue('PaperOrientation', rpoLandscape);

  C.SetHeight(Round(ConvertUnit(speHeight.Value, DesignUnits, utLogicalPixels)));
  C.SetWidth(Round(ConvertUnit(speWidth.Value, DesignUnits, utLogicalPixels)));

  C.SetPropertyValue('Duplexing', chbDuplexing.Checked);
end;

procedure TrmOPEPageSetup.ShowPropValue;
begin
  Initialize;
end;

procedure TrmOPEPageSetup.speWidthChange(Sender: TObject);
begin
  UpdatePreview;
end;

procedure TrmOPEPageSetup.SetPaperNameBySize(AWidth, AHeight: Integer);
var
  i, j, W, H: Integer;
  R: TrwPaperRec;
begin
  j := cbPaperNames.Items.IndexOfObject(Pointer(Ord(psCustom)));
  for i := 0 to cbPaperNames.Items.Count - 1 do
  begin
    R := cPaperInfo[TrwPaperSize(Integer(cbPaperNames.Items.Objects[i]))];
    H := Round(Round(ConvertUnit(R.Height / 10, utMillimeters, DesignUnits)*1000)/1000);
    W := Round(Round(ConvertUnit(R.Width / 10, utMillimeters, DesignUnits)*1000)/1000);

    if (AWidth = W) and (AHeight = H) or (AWidth = H) and (AHeight = W) then
    begin
      j := i;
      break;
    end;
  end;

  cbPaperNames.ItemIndex := j;
  cbPaperNames.OnChange(nil);
end;

end.
