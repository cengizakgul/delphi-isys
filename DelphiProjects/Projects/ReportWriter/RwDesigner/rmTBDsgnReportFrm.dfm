inherited rmTBDsgnReport: TrmTBDsgnReport
  Width = 443
  inherited tbComponents: TrmTBComponents
    Width = 443
  end
  inherited ControlBar: TControlBar
    Width = 443
    inherited tlbSettings: TToolBar
      Left = 311
    end
    inherited tlbExperts: TToolBar
      Left = 402
    end
    inherited tlbPosSize: TToolBar
      Left = 441
    end
    inherited ToolBar2: TToolBar
      Left = 441
    end
    inherited tlbCopy: TToolBar
      Left = 116
    end
    inherited tlbCompile: TToolBar
      Left = 428
      Width = 177
      inherited tbtCompile: TToolButton
        Tag = 0
      end
      object ToolButton2: TToolButton
        Left = 23
        Top = 0
        Width = 8
        Caption = 'ToolButton2'
        ImageIndex = 50
        Style = tbsSeparator
      end
      object tbtRun: TToolButton
        Left = 31
        Top = 0
        Action = actRun
      end
      object tbtPauseRunning: TToolButton
        Left = 54
        Top = 0
        Action = actPauseRunning
      end
      object tbtStopRunning: TToolButton
        Left = 77
        Top = 0
        Action = actStopRunning
      end
      object tbtTraceIn: TToolButton
        Left = 100
        Top = 0
        Action = actTraceIn
      end
      object tbtTraceOver: TToolButton
        Left = 123
        Top = 0
        Action = actTraceOver
      end
      object ToolButton11: TToolButton
        Left = 146
        Top = 0
        Width = 8
        Caption = 'ToolButton11'
        ImageIndex = 54
        Style = tbsSeparator
      end
      object tbtBrkPointToggle: TToolButton
        Left = 154
        Top = 0
        Action = actBrkPointToggle
      end
    end
    inherited ToolBar3: TToolBar
      Left = 229
      Width = 69
      object tbtNewReport: TToolButton
        Left = 46
        Top = 0
        Action = actNewReport
      end
    end
    inherited tlbMenu: TToolBar
      Width = 145
    end
    inherited tlbComponntsAlignment: TToolBar
      Left = 415
    end
  end
  inherited MainMenu: TMainMenu
    inherited File1: TMenuItem
      object actNewReport1: TMenuItem [0]
        Action = actNewReport
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object SystemLibrary1: TMenuItem
        Action = actLibrary
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Action = actExit
      end
    end
    object miSerach: TMenuItem [2]
      Caption = 'Search'
      GroupIndex = 100
      object FindErrorbyAddress1: TMenuItem
        Action = actFindError
      end
    end
    inherited ShowPaperGrid1: TMenuItem
      object miDebugWindows: TMenuItem [6]
        Caption = 'Debug Windows'
        object miCallStack: TMenuItem
          Action = actCallStack
        end
        object miWatches: TMenuItem
          Action = actWatches
        end
      end
    end
    inherited miWindows: TMenuItem
      Visible = True
    end
  end
  inherited ActionList: TActionList
    inherited actOpen: TAction
      Caption = 'Open Report...'
      Hint = 'Open Report from File'
    end
    inherited actSave: TAction
      Caption = 'Save Report...'
      Hint = 'Save Report into File'
    end
    inherited actCompile: TAction
      Tag = 0
    end
    object actNewReport: TAction [19]
      Category = 'File'
      Caption = 'New Report...'
      Hint = 'Create New Report'
      ImageIndex = 52
      OnExecute = actNewReportExecute
    end
    object actRun: TAction [20]
      Category = 'Execution'
      Caption = 'Run Report'
      Hint = 'Run Report'
      ImageIndex = 24
      ShortCut = 120
      OnExecute = actRunExecute
    end
    object actPauseRunning: TAction [21]
      Category = 'Execution'
      Caption = 'actPauseRunning'
      Enabled = False
      ImageIndex = 25
      OnExecute = actPauseRunningExecute
    end
    object actTraceIn: TAction [22]
      Category = 'Execution'
      Caption = 'actTraceIn'
      ImageIndex = 57
      ShortCut = 118
      OnExecute = actTraceInExecute
    end
    object actTraceOver: TAction [23]
      Category = 'Execution'
      Caption = 'actTraceOver'
      ImageIndex = 58
      ShortCut = 119
      OnExecute = actTraceOverExecute
    end
    object actBrkPointToggle: TAction [24]
      Category = 'Execution'
      Caption = 'actBrkPointToggle'
      ImageIndex = 53
      ShortCut = 116
      OnExecute = actBrkPointToggleExecute
    end
    object actStopRunning: TAction [25]
      Category = 'Execution'
      Caption = 'Stop'
      Enabled = False
      ImageIndex = 56
      ShortCut = 16497
      OnExecute = actStopRunningExecute
    end
    object actExit: TAction [26]
      Category = 'File'
      Caption = 'Exit'
      Hint = 'Close Designer'
      OnExecute = actExitExecute
    end
    inherited actCloseWindow: TAction
      Enabled = True
    end
    inherited actObjectTree: TAction
      ShortCut = 41082
    end
    inherited actEventsEditor: TAction
      ShortCut = 113
    end
    inherited actObjectInspector: TAction
      ShortCut = 122
    end
    object actLibrary: TAction
      Category = 'File'
      Caption = 'Open System Library'
      Hint = 'Open System Library'
      ImageIndex = 61
      OnExecute = actLibraryExecute
    end
    object actCallStack: TAction
      Category = 'View'
      Caption = 'Call Stack'
      ShortCut = 49235
      OnExecute = actCallStackExecute
    end
    object actWatches: TAction
      Category = 'View'
      Caption = 'Watches'
      ShortCut = 49239
      OnExecute = actWatchesExecute
    end
    object actFindError: TAction
      Category = 'Search'
      Caption = 'Find Error by Address...'
      OnExecute = actFindErrorExecute
    end
  end
end
