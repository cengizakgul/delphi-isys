unit rwLibToolsBarFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwReportFormToolsBarFrm, Menus, StdCtrls, ComCtrls, ToolWin, ExtCtrls;

type
  TrwLibToolsBar = class(TrwReportFormToolsBar)
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton21: TToolButton;
    ToolButton22: TToolButton;
    ToolButton23: TToolButton;
    ToolButton24: TToolButton;
    ToolButton25: TToolButton;
    ToolButton26: TToolButton;
    ToolButton27: TToolButton;
    ToolButton28: TToolButton;
    ToolButton29: TToolButton;
    tbtButton: TToolButton;
    tlbFileDialog: TToolButton;
    tbtTable: TToolButton;
    tlbDBTable: TToolButton;
    ToolButton17: TToolButton;
  protected
    function  AllowedToolBar(AToolBar: TToolBar): Boolean; override;
  end;

implementation

{$R *.DFM}

{ TrwLibToolsBar }

function TrwLibToolsBar.AllowedToolBar(AToolBar: TToolBar): Boolean;
begin
  Result := (AToolBar = tlbFont) or
            (AToolBar = tlbText) or
            (AToolBar = tlbSize) or
            (AToolBar = tlbAlignment) or
            (AToolBar = tlbComponents) or
            (AToolBar = tlbExperts);
end;

end.
