unit rmTextFilterSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, rwUtils, rwBasicClasses, rwParser, rmTypes;

type
  TrmTextFilterSelect = class(TForm)
    lvFilters: TListView;
    btnOK: TButton;
    btnCancel: TButton;
    procedure FormShow(Sender: TObject);
    procedure lvFiltersDblClick(Sender: TObject);
  private
  public
  end;

  function SelectTextFilter: TrwFunction;

implementation

{$R *.dfm}


function SelectTextFilter: TrwFunction;
var
  Frm: TrmTextFilterSelect;
begin
  Frm := TrmTextFilterSelect.Create(Application);
  with Frm do
    try
      if (ShowModal = mrOK) and (lvFilters.Selected <> nil)then
        Result := TrwFunction(lvFilters.Selected.Data)
      else
        Result := nil;
    finally
      Free;
    end;
end;

procedure TrmTextFilterSelect.FormShow(Sender: TObject);

  procedure FillFunctions(AFuncts: TrwFunctions);
  var
    i: Integer;
    Fnct: TrwFunction;
    LI: TListItem;
  begin
    for i := 0 to AFuncts.Count - 1 do
    begin
      Fnct := AFuncts[i];
      if not (fdtDataFilter in Fnct.DesignTimeInfo) then
        continue;

      LI := lvFilters.Items.Add;
      if Fnct.DisplayName <> '' then
        LI.Caption := Fnct.DisplayName
      else
        LI.Caption := Fnct.Name;

      LI.SubItems.Add(Fnct.Description);

      LI.Data := Fnct;
    end;
  end;

begin
  FillFunctions(RWFunctions);
  FillFunctions(SystemLibFunctions);
end;

procedure TrmTextFilterSelect.lvFiltersDblClick(Sender: TObject);
begin
  btnOK.Click;
end;

end.
