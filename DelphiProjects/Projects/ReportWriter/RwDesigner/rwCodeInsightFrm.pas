unit rwCodeInsightFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, rwParser, TypInfo, rwBasicClasses, rwCommonClasses,
  rwReport, rwTypes, rwUtils, mwCustomEdit, isRegistryFunctions, rwBasicUtils,
  rwEngineTypes, rmTypes;

type
  TrwCodeInsightItemType = (citProperty, citVariable, citObject, citFunction, citProcedure, citType, citClass);
  TrwCodeInsightCommand = (cicUp, cicDown, cicPageUp, cicPageDown, cicTop, cicBottom,
                           cicSelect, cicClose);


  TrwCodeInsightItem = class(TCollectionItem)
  private
    FName: string;
    FItemType: TrwCodeInsightItemType;
    FDescription: string;
  public
    property Name: string read FName write FName;
    property ItemType: TrwCodeInsightItemType read FItemType write FItemType;
    property Description: string read FDescription write FDescription;
  end;

  TrwCodeInsightItems = class(TCollection)
  end;


  TrwParHintWindow = class(THintWindow)
  private
    FCurrentParam: Integer;
    procedure SetCurrentParam(const Value: Integer);
  protected
    procedure Paint; override;
  public
    property CurrentParam: Integer read FCurrentParam write SetCurrentParam;
  end;


  TrwCodeInsight = class(TForm)
    lbCode: TListBox;
    procedure lbCodeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbCodeDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lbCodeDblClick(Sender: TObject);
    procedure lbCodeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    FItems: TrwCodeInsightItems;
    FFilter: String;
    FEdit: TmwCustomEdit;
    FBegPos: Integer;
    FAutoPopUp: Boolean;
    procedure FillList(AText: String);
    procedure FillListBox;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
  end;

function  ShowCodeInsight(AOwner: TmwCustomEdit; AAutoPopUp: Boolean): string;
function  ShowCodeInsightParams(AOwner: TmwCustomEdit; AAutoPopUp: Boolean): string;
procedure HideCodeInsightWnds;
function  IsCodeInsightVisible: Boolean;
function  IsCodeParamsVisible: Boolean;
procedure CodeInsCommand(ACommand: TrwCodeInsightCommand);


implementation

{$R *.DFM}

uses rwDsgnRes;

const
  CITypeWords: array [citProperty .. citClass] of string =
    ('property', 'var', 'object', 'function', 'procedure', 'type', 'class');

var
  FHintWindow: TrwParHintWindow = nil;
  FCodeInsight: TrwCodeInsight = nil;



function  IsCodeInsightVisible: Boolean;
begin
  Result := Assigned(FCodeInsight) and FCodeInsight.Visible;
end;

function IsCodeParamsVisible: Boolean;
begin
  Result := Assigned(FHintWindow) and FHintWindow.Visible;
end;

procedure HideCodeInsightWnds;
begin
  if Assigned(FHintWindow) then
  begin
    FHintWindow.Visible := False;
    ShowWindow(FHintWindow.Handle, SW_HIDE);
  end;

  if Assigned(FCodeInsight) then
    FCodeInsight.Visible := False;
end;


procedure CodeInsCommand(ACommand: TrwCodeInsightCommand);
begin
  if ACommand = cicClose then
    HideCodeInsightWnds

  else if IsCodeInsightVisible then
    case ACommand of
      cicUp:        SendMessage(FCodeInsight.lbCode.Handle, WM_KEYDOWN, VK_UP, 0);
      cicDown:      SendMessage(FCodeInsight.lbCode.Handle, WM_KEYDOWN, VK_DOWN, 0);
      cicPageUp:    SendMessage(FCodeInsight.lbCode.Handle, WM_KEYDOWN, VK_PRIOR, 0);
      cicPageDown:  SendMessage(FCodeInsight.lbCode.Handle, WM_KEYDOWN, VK_NEXT, 0);
      cicTop:       SendMessage(FCodeInsight.lbCode.Handle, WM_KEYDOWN, VK_HOME, 0);
      cicBottom:    SendMessage(FCodeInsight.lbCode.Handle, WM_KEYDOWN, VK_END, 0);
      cicSelect:    SendMessage(FCodeInsight.lbCode.Handle, WM_KEYDOWN, VK_RETURN, 0);
//      cicClose:     SendMessage(FCodeInsight.lbCode.Handle, WM_KEYDOWN, VK_ESCAPE, 0);
    end
end;


function ShowCodeInsight(AOwner: TmwCustomEdit; AAutoPopUp: Boolean): string;
var
  CI: TrwCodeInsightItem;
  n: Integer;
  P: TPoint;
  h: String;
  lOldFilter: String;
begin
  if Assigned(FCodeInsight) then
    Exit;

  HideCodeInsightWnds;
  Busy;
  FCodeInsight := TrwCodeInsight.Create(AOwner);
  with FCodeInsight do
    try
      FAutoPopUp := AAutoPopUp;
      h := ReadFromRegistry('Misc\rwDesigner\Options', 'CodeInsightSize', '');
      if Length(h) > 0 then
      begin
        Width := StrToInt(GetNextStrValue(h, ','));
        Height := StrToInt(GetNextStrValue(h, ','));
      end;

      P := AOwner.ClientToScreen(Point(AOwner.CaretXPix, AOwner.CaretYPix));
      if Screen.Height < (P.Y+Height) then
        Top := P.Y-Height
      else
        Top := P.Y + 17;
      if Screen.Width < (P.X + Width) then
        Left := P.X - Width
      else
        Left := P.X;

      n := AOwner.CoorToPos(AOwner.CaretXY) - 1;
      if Length(AOwner.LineText) < AOwner.CaretX - 1 then
        h := Copy(AOwner.Text, 1, n - (AOwner.CaretX - 1 - Length(AOwner.LineText))) + StringOfChar(' ', (AOwner.CaretX - 1) - Length(AOwner.LineText))
      else
        h := Copy(AOwner.Text, 1, n);

      FEdit := AOwner;
      FillList(h);
      FillListBox;
      Ready;

      if lbCode.Items.Count > 0 then
      begin
        ModalResult := mrCancel;
        Show;
        AOwner.SetFocus;

        SetWindowPos(FCodeInsight.Handle,
                      HWND_TOPMOST,
                      0, 0, 0, 0,
                      SWP_NOMOVE or SWP_NOSIZE or SWP_SHOWWINDOW or SWP_NOACTIVATE);
        lOldFilter := FFilter;

        while Visible do
        begin
          Application.ProcessMessages;
          Sleep(1);

          if (AOwner.CaretX < FBegPos) or (lbCode.Items.Count = 0) or
             (Screen.ActiveControl <> AOwner) and (Screen.ActiveControl <> lbCode) or
              not Application.Active  then
            Visible := False
          else
            if (lbCode.Items.Count = 1) and (Length(FFilter) > 0) and
               AnsiSameText(TrwCodeInsightItem(lbCode.Items.Objects[0]).Name, FFilter) then
            begin
              Visible := False;
              ModalResult := mrOK;
            end
            else
            begin
              h := Copy(AOwner.LineText, FBegPos, AOwner.CaretX - FBegPos);
              if (Length(FFilter) > 0) and (Length(AOwner.LineText) < AOwner.CaretX - 1) then
                h := h + StringOfChar(' ', (AOwner.CaretX - 1) - Length(AOwner.LineText));
              if not AnsiSameText(h, lOldFilter) then
              begin
                FFilter := h;
                FillListBox;
                lOldFilter := FFilter;
              end;
            end;
        end;

        if (ModalResult = mrOK) and (lbCode.ItemIndex <> -1) then
        begin
          CI := TrwCodeInsightItem(lbCode.Items.Objects[lbCode.ItemIndex]);
          Result := CI.Name;

          with AOwner do
          begin
            if Length(LineText) < CaretX - 1 then
              LineText := LineText + StringOfChar(' ', (CaretX - 1) - Length(LineText));

            LineText := Copy(LineText, 1, CaretX - 1 - Length(FFilter)) + Result +
              Copy(LineText, CaretX, Length(LineText) - CaretX + 1);
            CaretX := CaretX - Length(FFilter) + Length(Result);
          end;
        end;
      end;

      WriteToRegistry('Misc\rwDesigner\Options', 'CodeInsightSize', IntToStr(Width) + ',' + IntToStr(Height));

    finally
      Free;
      FCodeInsight := nil;
      Ready;
    end;
end;


function ShowCodeInsightParams(AOwner: TmwCustomEdit; AAutoPopUp: Boolean): string;
var
  n, BegPos: Integer;
  lPrevScrPos: TPoint;
  lText, h: String;
  HintWinRect: TRect;
  fl: Boolean;
  lPrevPos: Integer;

  function MakeHintText: string;
  var
    i, j: Integer;
    lCompCode: TrwCode;
    Params: TrwFuncParams;
    Err: String;

  begin
    Result := '';
    Params := TrwFuncParams.Create(TrwFuncParam);
    try
      Params.Tag := 0;
      SetLength(lCompCode, 0);
      SetCompiledCode(@lCompCode, 0);
      SetCompilingPointer(0);
      SetDebInf(nil);
      SetDebSymbInf(nil);
      SetCodeInsightFuncParams(Params);

      j := -1;
      try
        if Assigned(TrwEventTextEdit(AOwner).EventOwner) then
          CompileHandler(TrwEventTextEdit(AOwner).EventOwner, lText + #0)
        else
          CompileFunction(lText + #0);
      except
        on E: ErwParserError do
        begin
          j := E.Position;
          Err := E.Message;
        end;
      end;
      SetCodeInsightFuncParams(nil);
      SetLength(lCompCode, 0);

      if (j > 0) and (j <= Length(lText)) then
      begin
        if not AAutoPopUp then
          MessageDlg('Unable to invoke Code Completion due to errors in source code'#13#13 + Err, mtError, [mbOk], 0);
        Exit;
      end;

      if Params.Tag = 0 then
        Exit;

      if Params.Count = 0 then
      begin
        Result := '* No parameters expected *';
        Exit;
      end;

      for i := 0 to Params.Count - 1 do
      begin
        if Params[i].Descriptor.Reference then
          Result := Result + 'var ';
        Result := Result + Params[i].Descriptor.Name + ': ' + VarTypeToString(Params[i].Descriptor^);
        if i < (Params.Count - 1) then
          Result := Result + '; '
        else
          Result := Result + ';';
      end;

    finally
      Params.Free;
    end;
  end;


  function CheckCurrParams(const ACurrPos: Integer): Integer;
  var
    h: String;
    i, j, k: Integer;
    fl: Boolean;
  begin
    Result := 1;

    h := Copy(AOwner.Text, BegPos + 1, ACurrPos - BegPos - 1);
    j := 0;
    k := 0;
    fl := False;
    for i := 1 to Length(h) do
    begin
      if h[i] = '''' then
        fl := not fl;

      if not fl then
      begin
        case h[i] of
          '(': begin
                 Inc(j);
                 continue;
               end;

          ')': begin
                 Dec(j);
                 continue;
               end;

          '[': Inc(k);

          ']': Dec(k);
        end;

        if (j = 0) and (k = 0) then
        begin
          if h[i] = ',' then
            Inc(Result);
        end
        else if (j < 0) or (k < 0) then
        begin
          Result := 0;
          Exit;
        end;
      end;
    end;

    if j <> 0 then
      Result := 0;
  end;


begin
  if Assigned(FHintWindow) then
    Exit;

  Busy;
  HideCodeInsightWnds;

  try
    lText := Copy(AOwner.Text, 1, AOwner.CoorToPos(AOwner.CaretXY) - 1);
    BegPos := Length(lText);
    n := 1;
    fl := False;
    while (BegPos > 0) and (n > 0) do
    begin
      if lText[BegPos] = '''' then
        fl := not fl;

      if not fl then
        case lText[BegPos] of
          '(': Dec(n);
          ')': Inc(n);
        end;
      Dec(BegPos);
    end;

    if BegPos = 0 then
      Exit;

    Inc(BegPos);
    lText := Copy(AOwner.Text, 1, BegPos);
    h := MakeHintText;
    if Length(h) = 0 then
      Exit;

    lPrevScrPos := AOwner.ClientToScreen(Point(AOwner.CaretXPix, AOwner.CaretYPix));

    FHintWindow := TrwParHintWindow.Create(Application);
    FHintWindow.Color := Application.HintColor;
    FHintWindow.Canvas.Font.Style := FHintWindow.Canvas.Font.Style + [fsBold];
    HintWinRect := FHintWindow.CalcHintRect(Screen.Width, h, nil);
    HintWinRect.Left := lPrevScrPos.X;
    HintWinRect.Top := lPrevScrPos.Y + 17;
    HintWinRect.Right := HintWinRect.Right + lPrevScrPos.X + 4;
    HintWinRect.Bottom := HintWinRect.Bottom + lPrevScrPos.Y + 17;

    if Screen.Width < HintWinRect.Right then
    begin
      HintWinRect.Left := HintWinRect.Left - (HintWinRect.Right - Screen.Width);
      HintWinRect.Right := HintWinRect.Right - (HintWinRect.Right - Screen.Width);
    end;

    FHintWindow.ActivateHintData(HintWinRect, h, nil);
    Ready;
    lPrevPos := AOwner.CoorToPos(AOwner.CaretXY);

    n := CheckCurrParams(AOwner.CoorToPos(AOwner.CaretXY));
    if n > 0 then
    begin
      FHintWindow.CurrentParam := n;

      while FHintWindow.Visible do
      begin
        Application.ProcessMessages;
        Sleep(1);
        n := AOwner.CoorToPos(AOwner.CaretXY);

        if (n - 1 < BegPos) or (Screen.ActiveControl <> AOwner) or not Application.Active  then
          FHintWindow.Visible := False

        else if lPrevPos <> n then
        begin
          lPrevPos := n;
          n := CheckCurrParams(n);
          if n = 0 then
            FHintWindow.Visible := False
          else
            FHintWindow.CurrentParam := n;
        end;
      end;
    end;

    FHintWindow.Free;
    FHintWindow := nil;

  finally
    Ready;
  end;
end;


procedure TrwCodeInsight.FillList(AText: String);
var
  i, j: Integer;
  h: string;
  t: TrwCodeInsightItemType;
  ClassTypeInfo: PTypeInfo;
  ClassTypeData: PTypeData;
  PropList: PPropList;
  L: TList;
  lCompCode: TrwCode;
  V: TrwVariables;
  fl: Boolean;
  C: TComponent;
  Err: String;
  lTempObject: TrwComponent;

  procedure FillItem(AName: String; AType: TrwCodeInsightItemType; ADescription: String);
  var
    CI: TrwCodeInsightItem;
  begin
    CI := TrwCodeInsightItem(FItems.Add);
    CI.Name := AName;
    CI.ItemType := AType;
    CI.Description := ADescription;
  end;


  procedure FillEvents(AEvents: TrwEventsList);
  var
    i, j: Integer;
    t: TrwCodeInsightItemType;
    h: String;
  begin
    for i := 0 to AEvents.Count-1 do
    begin
      if AEvents[i].Params.Result.VarType = rwvUnknown then
        t := citProcedure
      else
        t := citFunction;

      h := MakeEventHandlerHeader(AEvents[i]);
      j := Pos('(', h);
      if j = 0 then
      begin
        j := Pos(':', h);
        if j > 0 then
          Delete(h, 1, j - 1)
        else
          h := '';
      end
      else
      begin
        Delete(h, 1, j - 1);
        j := Pos('()', h);
        if j > 0 then
          Delete(h, j, 2);
      end;
      FillItem(AEvents[i].Name, t, h + ';');
    end;
  end;


  procedure FillVars(AVars: TrwVariables);
  var
    i: Integer;
  begin
    for i := 0 to AVars.Count-1 do
      FillItem(AVars[i].Name, citVariable, VarTypeToString(AVars[i]^) + ';');
  end;


  procedure FillGlVarFnct(AGlVar: TrwGlobalVarFunct; AType: TrwCodeInsightItemType;
     ANonVis: TrwVarVisibilities; AInhNonVis: TrwVarVisibilities);
  var
    i: Integer;
    h: String;
  begin
    if Assigned(AGlVar) then
    begin
      for i := 0 to AGlVar.Variables.Count-1 do
      begin
        if not AGlVar.Variables[i].InheritedItem and (AGlVar.Variables[i].Visibility in ANonVis) or
           AGlVar.Variables[i].InheritedItem and (AGlVar.Variables[i].Visibility in AInhNonVis) then
          Continue;

        if (AGlVar.Variables[i].VarType = rwvPointer) and
           (Length(AGlVar.Variables[i].PointerType) > 0) then
          h := AGlVar.Variables[i].PointerType
        else
          h := NameOfVarType(AGlVar.Variables[i].VarType);
        FillItem(AGlVar.Variables[i].Name, AType, h + ';');
      end;
      FillEvents(AGlVar.EventsList);
    end;
  end;

  procedure FillUserProperties(AComp: TrwComponent);
  var
    i: Integer;
    h: String;
  begin
    for i := 0 to AComp._UserProperties.Count -1 do
    begin
      if (AComp._UserProperties[i].PropType = rwvPointer) and
         (Length(AComp._UserProperties[i].PointerType) > 0) then
        h := AComp._UserProperties[i].PointerType
      else
        h := NameOfVarType(AComp._UserProperties[i].PropType);
      FillItem(AComp._UserProperties[i].Name, citProperty, h + ';');
    end;
  end;


  procedure FillFncts(AFnct: TrwFunctions);
  var
    i, j: Integer;
    t: TrwCodeInsightItemType;
    Dscr: string;
  begin
    for i := 0 to AFnct.Count-1 do
    begin
      Dscr := '';
      for j := 0 to AFnct[i].Params.Count-1 do
      begin
        Dscr := Dscr + AFnct[i].Params[j].Descriptor.Name+': '+VarTypeToString(AFnct[i].Params[j].Descriptor^);
        if j < AFnct[i].Params.Count-1 then
          Dscr := Dscr + '; ';
      end;
      if AFnct[i].Params.Count > 0 then
        Dscr := '(' + Dscr + ')';

      if AFnct[i].Params.Result.VarType = rwvUnknown then
        t := citProcedure
      else
      begin
        t := citFunction;
        if (AFnct[i].Params.Result.VarType = rwvPointer) and
           (Length(AFnct[i].Params.Result.PointerType) > 0) then
          Dscr := Dscr+': ' + AFnct[i].Params.Result.PointerType
        else
          Dscr := Dscr+': ' + VarTypeToString(AFnct[i].Params.Result^);
      end;

      FillItem(AFnct[i].Name, t, Dscr + ';');
    end;
  end;


  procedure AddObject(AComp: TrwComponent);
  var
    h: String;
  begin
    if AComp.IsLibComponent then
      h := AComp._LibComponent
    else
      h := AComp.ClassName;
    FillItem(AComp.Name, citObject, h + ';');
  end;

  procedure FillChildren(AComp: TrwComponent);
  var
    i: Integer;
    C: TComponent;
  begin
    for i := 0 to AComp.VirtualComponentCount - 1 do
    begin
      C := AComp.GetVirtualChild(i);
      if (C is TrwNoVisualComponent) or (C is TrwControl) then
        AddObject(TrwComponent(C));
    end;
  end;

  procedure FillTypes;
  var
    t: TrwVarTypeKind;
    i: Integer;
    h: String;
  begin
    for t := Succ(Low(TrwVarTypeKind)) to High(TrwVarTypeKind) do
      FillItem(NameOfVarType(t), citType, '');

    for i := 0 to RWClassList.Count -1 do
    begin
      if not TPersistentClass(RWClassList.Objects[i]).InheritsFrom(TrwComponent) or
        (TPersistentClass(RWClassList.Objects[i]) = TrwComponent) then
        h := ''
      else
        h := 'inherits from ' + TPersistentClass(RWClassList.Objects[i]).ClassParent.ClassName;
      FillItem(TPersistentClass(RWClassList.Objects[i]).ClassName, citClass, h);
    end;

    for i := 0 to SystemLibComponents.Count -1 do
    begin
      h := 'inherits from ' + SystemLibComponents[i].ParentName;
      FillItem(SystemLibComponents[i].Name, citClass, h);
    end;
  end;


begin
  V := TrwVariables.Create;
  try
    FItems.Clear;

    SetLength(lCompCode, 0);
    SetCompiledCode(@lCompCode, 0);
    SetCompilingPointer(0);
    SetDebInf(nil);
    SetDebSymbInf(nil);
    SetCodeInsightLocVars(V);
    j := -1;
    try
      if Assigned(TrwEventTextEdit(FEdit).EventOwner) then
        CompileHandler(TrwEventTextEdit(FEdit).EventOwner, AText + #0)
      else
        CompileFunction(AText + #0);
    except
      on E: ErwParserError do
      begin
        j := E.Position;
        Err := E.Message;
      end;
    end;
    SetCodeInsightLocVars(nil);
    SetLength(lCompCode, 0);

    FFilter := '';
    fl := False;
    h := AText;
    i := Length(AText);
    while i > 0 do
    begin
      if AText[i] = '.' then
      begin
        fl := True;
        break
      end

      else if Pos(AText[i], ' [(;:=<>+-/*},'#13#10) <> 0 then
      begin
        AText := '';
        break;
      end

      else if AText[i] in [')', ']'] then
        break;

      FFilter := AText[i] + FFilter;
      Dec(i);
    end;

    if (j > 0) and (j <= Length(AText)) then
    begin
      if not FAutoPopUp then
        MessageDlg('Unable to invoke Code Completion due to errors in source code'#13#13 + Err, mtError, [mbOk], 0);
      Exit;
    end;

    if (Length(AText) > 0) and (GetCodeInsightInfo.VarType <> rwvPointer) then
      Exit;

    i := LastDelimiter(#10, h);
    if i <> -1 then
      FBegPos := Length(h) - i + 1 - Length(FFilter);
    if FBegPos = 0 then
      FBegPos := 1;

    C := TrwEventTextEdit(FEdit).EventOwner;
    if Assigned(C) then
      while not TrwComponent(C).IsVirtualOwner and (TrwComponent(C).VirtualOwner <> nil) and (TrwComponent(C).VirtualOwner <> C) do
        C := TrwComponent(C).VirtualOwner;

    if not fl and (AText = '') then
    begin
      FillFncts(RWFunctions);
      FillFncts(SystemLibFunctions);
      FillTypes;
      if Assigned(C) then
      begin
        FillGlVarFnct(TrwComponent(C).GlobalVarFunc, citVariable, [], [rvvPrivate]);
        FillUserProperties(TrwComponent(C));
        FillChildren(TrwComponent(C));
        AddObject(TrwComponent(C));
      end;
      FillVars(V);
    end

    else if not fl and (AText <> '') then
      Exit

    else
    begin
      if Assigned(GetCodeInsightInfo.ClassInf) then
      begin
        ClassTypeInfo := GetCodeInsightInfo.ClassInf.GetVCLClass.ClassInfo;
        ClassTypeData := GetTypeData(ClassTypeInfo);

        if (ClassTypeData.PropCount <> 0) then
        begin
          GetMem(PropList, SizeOf(PPropInfo) * ClassTypeData.PropCount);

          try
            GetPropInfos(ClassTypeInfo, PropList);
            for i := 0 to ClassTypeData.PropCount - 1 do
              if (PropList[i]^.PropType^.Kind <> tkMethod) and (PropList[i]^.Name[1] <> '_') then
                FillItem(PropList[i]^.Name, citProperty, PropList[i]^.PropType^.Name + ';');
          finally
            FreeMem(PropList, SizeOf(PPropInfo) * ClassTypeData.PropCount);
          end;
        end;


        //Published methods
        L := TList.Create;
        try
          PublishedMethods.ListOfFunction(GetCodeInsightInfo.ClassInf.GetVCLClass, L);
          for i := 0  to L.Count -1 do
          begin
            if TrwPublishedMethod(L[i]).Params.Result.VarType = rwvUnknown then
              t := citProcedure
            else
              t := citFunction;
            h := TrwPublishedMethod(L[i]).HeaderText;
            j := Pos('(', h);
            if j = 0 then
            begin
              j := Pos(':', h);
              if j > 0 then
                Delete(h, 1, j - 1)
              else
                h := '';
            end
            else
            begin
              Delete(h, 1, j - 1);
              j := Pos('()', h);
              if j > 0 then
                Delete(h, j, 2);
            end;
            FillItem(TrwPublishedMethod(L[i]).Name, t, h + ';');
          end;
        finally
          L.Free;
        end;


        //Global variables and functions
        if Assigned(GetCodeInsightInfo.Obj) and (GetCodeInsightInfo.Obj is TrwComponent) then
          with TrwComponent(GetCodeInsightInfo.Obj) do
          begin
            if TrwComponent(GetCodeInsightInfo.Obj) = C then
              FillGlVarFnct(GlobalVarFunc, citProperty, [], [rvvPrivate])
            else
              FillGlVarFnct(GlobalVarFunc, citProperty,
                [rvvProtected, rvvPrivate], [rvvProtected, rvvPrivate]);
            FillUserProperties(TrwComponent(GetCodeInsightInfo.Obj));
            FillEvents(EventsList);
            FillChildren(TrwComponent(GetCodeInsightInfo.Obj));
          end

        //RW Library info
        else if GetCodeInsightInfo.ClassInf.IsUserClass then
        begin
          lTempObject := GetCodeInsightInfo.ClassInf.CreateObjectInstance(False, True) as TrwComponent;
          try
            with TrwComponent(lTempObject) do
            begin
              FillGlVarFnct(GlobalVarFunc, citProperty,
                [rvvProtected, rvvPrivate], [rvvProtected, rvvPrivate]);
              FillUserProperties(TrwComponent(lTempObject));
              FillEvents(EventsList);
              FillChildren(lTempObject);
            end
          finally
            lTempObject.Free;
          end;
        end;
      end;
    end;

  finally
    V.Free;
  end;
end;

procedure TrwCodeInsight.lbCodeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Ord(Key) = VK_RETURN then
  begin
    Close;
    ModalResult := mrOK;
  end
  else if Ord(Key) in [VK_ESCAPE, VK_LEFT, VK_RIGHT] then
  begin
    Close;
    ModalResult := mrCancel;
  end;
end;

procedure TrwCodeInsight.lbCodeDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  CI: TrwCodeInsightItem;
  x: Integer;
  R: TRect;
  h: String[2];
begin
  if odSelected in State then
  begin
    lbCode.Canvas.Font.Color := clWhite;
    lbCode.Canvas.Brush.Color := clNavy;
    lbCode.Canvas.FillRect(Rect);
  end
  else
  begin
    lbCode.Canvas.Font.Color := clBlack;
    lbCode.Canvas.Brush.Color := lbCode.Color;
    lbCode.Canvas.FillRect(Rect);
  end;

  CI := TrwCodeInsightItem(lbCode.Items.Objects[Index]);
  x := Rect.Left;
  lbCode.Canvas.Font.Style := lbCode.Canvas.Font.Style - [fsBold];
  if not (odSelected in State) then
    case CI.ItemType of
      citProperty:  lbCode.Canvas.Font.Color := clMaroon;
      citVariable:  lbCode.Canvas.Font.Color := clRed;
      citObject:    lbCode.Canvas.Font.Color := clGreen;
      citFunction:  lbCode.Canvas.Font.Color := clNavy;
      citProcedure: lbCode.Canvas.Font.Color := clBlue;
      citType,
      citClass:     lbCode.Canvas.Font.Color := clOlive;
    end;

  lbCode.Canvas.TextOut(x, Rect.Top, CITypeWords[CI.ItemType]);

  x := x+80;
  lbCode.Canvas.Font.Style := lbCode.Canvas.Font.Style + [fsBold];
  if not (odSelected in State) then
    lbCode.Canvas.Font.Color := clBlack;
  lbCode.Canvas.TextOut(x, Rect.Top, CI.Name);

  DrawText(lbCode.Canvas.Handle, PChar(CI.Name), Length(CI.Name), R, DT_CALCRECT);
  x := X+(R.Right-R.Left)+2;
  lbCode.Canvas.Font.Style := lbCode.Canvas.Font.Style - [fsBold];
  if CI.ItemType in [citProperty, citVariable, citObject] then
    h := ' :'
  else
    h := ' ';
  lbCode.Canvas.TextOut(x, Rect.Top, h + CI.Description);

  if odSelected in State then
    lbCode.Hint := CITypeWords[CI.ItemType] + '   ' + CI.Name + h +CI.Description;
end;

procedure TrwCodeInsight.FormCreate(Sender: TObject);
begin
  FItems := TrwCodeInsightItems.Create(TrwCodeInsightItem);
end;

procedure TrwCodeInsight.FormDestroy(Sender: TObject);
begin
  FItems.Free;
end;

procedure TrwCodeInsight.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.Style := WS_POPUP OR WS_THICKFRAME;
end;

procedure TrwCodeInsight.lbCodeDblClick(Sender: TObject);
begin
  Close;
  ModalResult := mrOK;
end;



{ TrwParHintWindow }

procedure TrwParHintWindow.Paint;
var
  cR, R: TRect;
  h, p: String;
  i, n: Integer;
  c: String[1];
begin
  R := ClientRect;
  Inc(R.Left, 2);
  Inc(R.Top, 2);
  Canvas.Font.Color := clInfoText;

  if Caption[Length(Caption)] = ';' then
    c := ';'
  else
    c := '';
  h := Caption;
  i := 0;
  while Length(h) > 0 do
  begin
    p := GetNextStrValue(h, ';') + c;
    Inc(i);

    if i = CurrentParam then
      Canvas.Font.Style := Canvas.Font.Style + [fsBold]
    else
      Canvas.Font.Style := Canvas.Font.Style - [fsBold];

    cR := Rect(0,0,0,0);
    DrawText(Canvas.Handle, PChar(p), -1, cR, DT_LEFT or DT_NOPREFIX or DrawTextBiDiModeFlagsReadingOnly or DT_CALCRECT);
    n := cR.Right;
    cR := Rect(R.Left, R.Top, cR.Right + R.Left, cR.Bottom + R.Top);
    R.Left := R.Left + n;

    DrawText(Canvas.Handle, PChar(p), -1, cR, DT_LEFT or DT_NOPREFIX or DrawTextBiDiModeFlagsReadingOnly);
  end;
end;


procedure TrwParHintWindow.SetCurrentParam(const Value: Integer);
begin
  if FCurrentParam <> Value then
  begin
    FCurrentParam := Value;
    Invalidate;
  end;
end;

procedure TrwCodeInsight.FillListBox;
var
  CI: TrwCodeInsightItem;
  i: Integer;
begin
  lbCode.Items.BeginUpdate;
  lbCode.Items.Clear;
  lbCode.Sorted := False;

  for i := 0 to FItems.Count - 1 do
  begin
    CI := TrwCodeInsightItem(FItems.Items[i]);
    if (Length(FFilter) > 0) and not AnsiSameText(Copy(CI.Name, 1, Length(FFilter)), FFilter) then
      continue;
    lbCode.Items.AddObject(CI.Name, CI);
  end;

  lbCode.Sorted := True;
  if lbCode.Items.Count > 0 then
    lbCode.ItemIndex := 0;

  lbCode.Items.EndUpdate;    
end;

procedure TrwCodeInsight.lbCodeMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FEdit.SetFocus;
  SetWindowPos(FCodeInsight.Handle,
              HWND_TOPMOST,
              0, 0, 0, 0,
              SWP_NOMOVE or SWP_NOSIZE or SWP_SHOWWINDOW or SWP_NOACTIVATE);end;

end.
