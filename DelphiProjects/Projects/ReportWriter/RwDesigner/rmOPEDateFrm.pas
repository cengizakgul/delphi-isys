unit rmOPEDateFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, ComCtrls, StdCtrls;

type
  TrmOPEDate = class(TrmObjPropertyEditor)
    lPropName: TLabel;
    deDate: TDateTimePicker;
    procedure FrameResize(Sender: TObject);
    procedure deDateChange(Sender: TObject);
  public
    procedure ShowPropValue; override;
    function  NewPropertyValue: Variant; override;    
  end;

implementation

{$R *.dfm}

procedure TrmOPEDate.FrameResize(Sender: TObject);
begin
  inherited;
  deDate.Width := ClientWidth - deDate.Left;
end;

procedure TrmOPEDate.ShowPropValue;
begin
  inherited;
  try
    deDate.Date := OldPropertyValue;
  except
    deDate.Date := Date;
  end;
end;

function TrmOPEDate.NewPropertyValue: Variant;
begin
  Result := VarAsType(deDate.Date, varDate);
end;

procedure TrmOPEDate.deDateChange(Sender: TObject);
begin
  NotifyOfChanges;
end;

end.
 