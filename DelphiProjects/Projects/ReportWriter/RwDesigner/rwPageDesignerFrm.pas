unit rwPageDesignerFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ToolWin, ComCtrls, ExtCtrls, StdCtrls, rwRuler, Menus, rwTypes, rwBasicClasses,
  rwPropertiesEditorFrm, mwCustomEdit, rwDesignAreaFrm,
  rwBasicToolsBarFrm, rwDsgnRes, rwReport, rwDebugInfo, rwUtils, rwDsgnUtils, rwDB;

type

  TrwDsgnScrollAreaEvent = procedure(Sender: TObject; DeltaX: Integer; DeltaY: Integer) of object;
  TrwOnCheckBreakPoint = function(Sender: TObject; ALine: Integer): Boolean of object;

  {TDsgnScrollArea is scrolled area for designing}
  TrwDsgnScrollArea = class(TScrollBox)
  private
    FOnScroll: TrwDsgnScrollAreaEvent;

    procedure WMHScroll(var Message: TWMHScroll); message WM_HSCROLL;
    procedure WMVScroll(var Message: TWMVScroll); message WM_VSCROLL;

  protected
    procedure AutoScrollInView(AControl: TControl); override;

  published
    property OnScroll: TrwDsgnScrollAreaEvent read FOnScroll write FOnScroll;
  end;

  TrwPageDesigner = class(TFrame)
    pnlVertRuler: TPanel;
    pnlHorRuler: TPanel;
    Panel3: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    pnlUnitName: TPanel;
    rwPropertiesEditor: TrwPropertiesEditor;
    Panel1: TPanel;
    pcEvents: TPageControl;
    procedure pcEventsChange(Sender: TObject);

  private
    FOnChangePropertySelectComponent: TOnPropertyChange;
    FOnChangeSelectComponent: TOnPropertyChange;
    FOnCheckBreakPoint: TrwOnCheckBreakPoint;
    FCurrentLibClass: String;
    FReadOnly: Boolean;

    procedure OnScrollArea(Sender: TObject; DeltaX: Integer; DeltaY: Integer);
    procedure OnEventChange(Sender: TObject);
    procedure OnPropertyChange(Sender: TObject; AComponent: TrwComponent);
    procedure OnComponentChange(Sender: TObject; AComponent: TrwComponent);
    function  GetPosition: TPoint;
    procedure SetPosition(Value: TPoint);
    procedure EventTextExit(Sender: TObject);
    procedure OnChangeCursPos(Sender: TObject);
    procedure ShowInheritedEvent(ALibComp: TrwLibComponent; AChildComponent: TrwComponent; AEvent: TrwEvent; AClear: Boolean = True);
    procedure OnSpecialLineColors(Sender: TObject; Line: integer; var Special: boolean; var FG, BG: TColor);
    procedure SetReadOnly(const Value: Boolean);
  public
    sbxDsgnFrame: TrwDsgnScrollArea;
    VertRuler: TrwRuler;
    HorRuler: TrwRuler;
    EventText: TrwEventTextEdit;
    RunTimeErrorLine: Integer;

    property OnChangePropertySelectComponent: TOnPropertyChange read FOnChangePropertySelectComponent write
      FOnChangePropertySelectComponent;
    property OnChangeSelectComponent: TOnPropertyChange read FOnChangeSelectComponent write FOnChangeSelectComponent;
    property OnCheckBreakPoint: TrwOnCheckBreakPoint read FOnCheckBreakPoint write FOnCheckBreakPoint;
    property Position: TPoint read GetPosition write SetPosition;
    property CurrentLibClass: String read FCurrentLibClass;
    property ReadOnly: Boolean read FReadOnly write SetReadOnly;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ShowPositionOnRuler(X, Y: Integer);
    procedure SetUnits(Value: TrwUnitType);

    procedure AddNewComponentToInspector(AComponent: TrwComponent; AToolBar: TrwBasicToolsBar);
    procedure DeleteComponentFromInspector(AComponent: TrwComponent);

    procedure SyncComponentsList(AOwner: TrwComponent; AToolBar: TrwBasicToolsBar);
    procedure ShowComponentProperties(AComponent: TrwComponent; AGroup: Boolean = False);
    procedure ShowDefProperty(AComponent: TrwComponent; APropName: string);
    procedure ShowComponentEvent(AComponentPath: string; AEventName: string; ALibClass: String);
    procedure RefreshComponentProperties;
    procedure ChangeFontStatus(Area: TrwDesignArea; AToolBar: TrwBasicToolsBar; EventType: TrwFontToolBarEvent);
    procedure SetPos(const APos: Integer);
    procedure InvalidateEventLine(const ALine: Integer);
    function  CurrentHandler: TmwCustomEdit;
  end;

implementation

uses rwDesignerFrm, rwDataSetExplorerFrm;

{$R *.DFM}


const
 lcOverriding = 'Overriding';

  {TDsgnScrollArea}

procedure TrwDsgnScrollArea.WMHScroll(var Message: TWMHScroll);
var
  lOldPosition, lDelta: Integer;
begin
  lOldPosition := HorzScrollBar.ScrollPos;

  inherited;

  lDelta := HorzScrollBar.ScrollPos - lOldPosition;

  if Assigned(FOnScroll) and (lDelta <> 0) then
    FOnScroll(Self, lDelta, 0);

end;

procedure TrwDsgnScrollArea.WMVScroll(var Message: TWMVScroll);
var
  lOldPosition, lDelta: Integer;
begin
  lOldPosition := VertScrollBar.ScrollPos;

  inherited;

  lDelta := VertScrollBar.ScrollPos - lOldPosition;

  if Assigned(FOnScroll) and (lDelta <> 0) then
    FOnScroll(Self, 0, lDelta);

end;

procedure TrwDsgnScrollArea.AutoScrollInView(AControl: TControl);
begin
end;


  {TrwReportFormDesigner}

constructor TrwPageDesigner.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FReadOnly := False;

  EventText := TrwEventTextEdit.Create(nil);
  EventText.OnSpecialLineColors := OnSpecialLineColors;
  DsgnRes.InitEditor(EventText);
  EventText.Align := alClient;
  EventText.OnExit := EventTextExit;
  EventText.OnSelectionChange := OnChangeCursPos;
  EventText.Parent := Panel1;

  VertRuler := TrwRuler.Create(Self);
  VertRuler.Orientation := roVertical;
  VertRuler.Parent := pnlVertRuler;
  VertRuler.Align := alClient;

  HorRuler := TrwRuler.Create(Self);
  HorRuler.Orientation := roHorizontal;
  HorRuler.Parent := pnlHorRuler;
  HorRuler.Align := alClient;

  sbxDsgnFrame := TrwDsgnScrollArea.Create(Self);
  sbxDsgnFrame.Color := clGray;
  sbxDsgnFrame.Align := alClient;
  sbxDsgnFrame.Parent := Self;
  sbxDsgnFrame.OnScroll := OnScrollArea;

  rwPropertiesEditor.OnEventChange := OnEventChange;
  rwPropertiesEditor.OnPropertyChange := OnPropertyChange;
  rwPropertiesEditor.OnComponentChange := OnComponentChange;

  RunTimeErrorLine := 0;
end;

destructor TrwPageDesigner.Destroy;
begin
  EventText.HighLighter := nil;
  EventText.Free;
  inherited;
end;

procedure TrwPageDesigner.OnScrollArea(Sender: TObject; DeltaX: Integer; DeltaY: Integer);
begin
  HorRuler.Scroll(DeltaX);
  VertRuler.Scroll(DeltaY);
end;

procedure TrwPageDesigner.SetUnits(Value: TrwUnitType);
begin
  DesignUnits := Value;
  VertRuler.Units := Value;
  HorRuler.Units := Value;

  case Value of
    utScreenPixels: pnlUnitName.Caption := 'Pixel';
    utInches: pnlUnitName.Caption := 'Inch';
    utMillimeters: pnlUnitName.Caption := 'mm';
    utMMThousandths: pnlUnitName.Caption := 'Tmm';
  end;

  RefreshComponentProperties;
end;

procedure TrwPageDesigner.ShowPositionOnRuler(X, Y: Integer);
begin
  HorRuler.SetGuides(X, 0);
  VertRuler.SetGuides(Y, 0);
end;

procedure TrwPageDesigner.AddNewComponentToInspector(AComponent: TrwComponent; AToolBar: TrwBasicToolsBar);
begin
  rwPropertiesEditor.AddNewComponentToComponentsList(AComponent, AToolBar);
end;

procedure TrwPageDesigner.DeleteComponentFromInspector(AComponent: TrwComponent);
begin
  rwPropertiesEditor.DeleteComponentFromComponentsList(AComponent);
end;

procedure TrwPageDesigner.SyncComponentsList(AOwner: TrwComponent; AToolBar: TrwBasicToolsBar);
begin
  rwPropertiesEditor.CreateComponentsList(AOwner, AToolBar);
end;

procedure TrwPageDesigner.ShowComponentProperties(AComponent: TrwComponent; AGroup: Boolean = False);
begin
  rwPropertiesEditor.ShowComponentProperties(AComponent, AGroup);
end;

procedure TrwPageDesigner.RefreshComponentProperties;
begin
  rwPropertiesEditor.RefreshComponentProperties;
end;

procedure TrwPageDesigner.OnEventChange(Sender: TObject);
var
  lEvent: TrwEvent;
  LibComp: TrwLibComponent;
  Ownr: TrwComponent;
  i: Integer;
  h: String;
  P: TPoint;
begin
  try
    EventText.Tag := Ord(True);
    ShowInheritedEvent(nil, nil, nil);

    with rwPropertiesEditor do
      if (CurrentEventName = '') then
      begin
        EventText.Lines.Clear;
        EventText.Enabled := False;
      end
      else
      begin
        EventText.EventOwner := CurrentComponent;
        EventText.Enabled := True;
        lEvent := CurrentComponent.Events[CurrentEventName];

        Ownr := TrwComponent(CurrentComponent.Owner);

        while Assigned(Ownr) and not (Ownr.IsLibComponent or (Ownr is TrwReport)) do
          Ownr := TrwComponent(Ownr.Owner);

        if Assigned(Ownr) and Ownr.IsLibComponent then
        begin
          i := SystemLibComponents.IndexOf(Ownr._LibComponent);
          if i <> -1 then
          begin
            LibComp := SystemLibComponents.Items[i];
            ShowInheritedEvent(LibComp, CurrentComponent, lEvent);
          end;
        end;

        if CurrentComponent.IsLibComponent then
        begin
          i := SystemLibComponents.IndexOf(CurrentComponent._LibComponent);
          if i <> -1 then
          begin
            LibComp := SystemLibComponents.Items[i];
            ShowInheritedEvent(LibComp, nil, lEvent,
              not (Assigned(Ownr) and Ownr.IsLibComponent and CurrentComponent.InheritedComponent));
          end;
        end
        else
          if not (Assigned(Ownr) and Ownr.IsLibComponent) then
            ShowInheritedEvent(nil, nil, nil);

        EventText.Lines.Clear;
        EventText.Lines[0] := MakeEventHandlerHeader(lEvent);
        if (lEvent.HandlersText = '') then
          EventText.Lines.Add('')
        else
        begin
          EventText.Lines.Text := EventText.Lines.Text + lEvent.HandlersText;
          EventText.Lines.Delete(0);
        end;
        EventText.Lines.Add('end');

        h := CurrentComponent.DesignInfo.EventsPositions.Values[lEvent.Name];
        if h <> '' then
        begin
          i := Pos(',', h);
          P.X := StrToInt(Copy(h, 1, i-1));
          Delete(h, 1, i);
          i := Pos(',', h);
          P.Y := StrToInt(Copy(h, 1, i-1));
          Delete(h, 1, i);
          EventText.CaretY := P.Y;
          EventText.CaretX := P.X;
          i := Pos(',', h);
          EventText.TopLine := StrToInt(Copy(h, 1, i-1));
          Delete(h, 1, i);
          EventText.LeftChar := StrToInt(h);
        end;
      end;
  finally
    EventText.Tag := Ord(False);
    pcEvents.OnChange(nil);
  end;
end;

procedure TrwPageDesigner.OnPropertyChange(Sender: TObject; AComponent: TrwComponent);
begin
  if Assigned(FOnChangePropertySelectComponent) then
    FOnChangePropertySelectComponent(Self, AComponent);
end;

procedure TrwPageDesigner.OnComponentChange(Sender: TObject; AComponent: TrwComponent);
begin
  if Assigned(FOnChangeSelectComponent) then
    FOnChangeSelectComponent(Self, AComponent);
end;

procedure TrwPageDesigner.SetPosition(Value: TPoint);
var
  ce: TmwCustomEdit;
begin
  ce := CurrentHandler;
  ce.CaretY := Value.Y;
  ce.CaretX := Value.X;
  ce.SetFocus;
end;

function TrwPageDesigner.GetPosition: TPoint;
var
  ce: TmwCustomEdit;
begin
  ce := CurrentHandler;
  Result := Point(ce.CaretX, ce.CaretY);
end;


procedure TrwPageDesigner.ShowComponentEvent(AComponentPath: string; AEventName: string; ALibClass: String);
var
  i: Integer;
begin
  rwPropertiesEditor.ShowComponentEvent(AComponentPath, AEventName);

  if (Length(ALibClass) > 0) and pcEvents.Visible then
  begin
    for i := 0 to pcEvents.PageCount -1 do
      if SameText(pcEvents.Pages[i].Caption, ALibClass) then
      begin
        pcEvents.ActivePage := pcEvents.Pages[i];
        pcEvents.OnChange(nil);
        break;
      end;
  end;
end;


procedure TrwPageDesigner.EventTextExit(Sender: TObject);
var
  i, j: Integer;
  h: String;
  fl: Boolean;
begin
  with rwPropertiesEditor do
    if Assigned(CurrentComponent) and (CurrentEventName <> '') then
    begin
      i := Length(EventText.Lines[0]) + 3;
      j := Length(EventText.Lines.Text) - Length(EventText.Lines[EventText.Lines.Count - 1]) - 3 - i;
      h := TrimRight(Copy(EventText.Lines.Text, i, j));
      if (Length(h) = 0) and CurrentComponent.Events[CurrentEventName].InheritedItem and
         (pcEvents.PageCount > 1)  then
      begin
        h := '  inherited;';
        fl := True;
      end
      else
        fl := False;
      CurrentComponent.Events[CurrentEventName].HandlersText := h;

      with CurrentComponent.DesignInfo do
      begin
        i := EventsPositions.IndexOfName(CurrentEventName);
        if i <> -1 then
          EventsPositions.Delete(i);
        EventsPositions.Add(CurrentEventName+'='+IntToStr(EventText.CaretX)+','+IntToStr(EventText.CaretY)+','+
          IntToStr(EventText.TopLine)+','+IntToStr(EventText.LeftChar));
        LastEvent := CurrentEventName;
      end;

      if fl then
        OnEventChange(nil);
    end;
end;

procedure TrwPageDesigner.OnChangeCursPos(Sender: TObject);
begin
  ReportDesigner.ShowCursorPos(TmwCustomEdit(Sender));
end;


procedure TrwPageDesigner.ChangeFontStatus(Area: TrwDesignArea; AToolBar: TrwBasicToolsBar; EventType: TrwFontToolBarEvent);

  function SetOper(ABtn: TToolButton): String;
  begin
   if ABtn.Down then
     Result := '+'
   else
     Result := '-';
  end;

begin
  with Area, AToolBar do
    case EventType of
      fteFontName: SetPropertyForSelection(['Font', 'Name'], cbFontName.Text);

      fteFontSize: SetPropertyForSelection(['Font', 'Size'], cbFontSize.Text);

      fteFontBold: SetPropertyForSelection(['Font', 'Style'], SetOper(tbtFontBold) + '[fsBold]');

      fteFontItalic: SetPropertyForSelection(['Font', 'Style'], SetOper(tbtFontItalic) + '[fsItalic]');

      fteFontUnderline: SetPropertyForSelection(['Font', 'Style'], SetOper(tbtFontUnderline) + '[fsUnderline]');

      fteTextAlignLeft: SetPropertyForSelection(['Alignment'], taLeftJustify);

      fteTextAlignCenter: SetPropertyForSelection(['Alignment'], taCenter);

      fteTextAlignRight: SetPropertyForSelection(['Alignment'], taRightJustify);

{        fteFontColor:          SetFontColor(TColor(tbtFontColor.Tag));
      fteFontHighlightColor: SetFontHighlightColor(TColor(tbtFontHighlightColor.Tag));}
    end;
end;

procedure TrwPageDesigner.ShowDefProperty(AComponent: TrwComponent; APropName: string);
begin
  if Debugging then
  begin
    if AComponent is TrwDataSet then
      ShowDSExplorer(AComponent.Name);
  end
  else
    rwPropertiesEditor.EditProperty(AComponent, APropName);
end;

procedure TrwPageDesigner.ShowInheritedEvent(ALibComp: TrwLibComponent; AChildComponent: TrwComponent; AEvent: TrwEvent; AClear: Boolean = True);
var
  T: TTabSheet;
  i: Integer;
  EvText: TmwCustomEdit;
  h: String;
  Ownr: TrwComponent;

  function ShowOverridenEvent: Boolean;
  begin
    with rwPropertiesEditor do
      Result := not CurrentComponent.InheritedComponent or
                CurrentComponent.InheritedComponent and
                ((CurrentComponent.VirtualOwner is TrwCustomReport) or (CurrentComponent.VirtualOwner is TrwInputFormContainer));
  end;


  procedure ClearTabs;
  begin
    while pcEvents.PageCount > 0 do
    begin
      while pcEvents.Pages[0].ControlCount > 0 do
        pcEvents.Pages[0].Controls[0].Free;
      pcEvents.Pages[0].Free;
    end;
    FCurrentLibClass := '';
  end;

  procedure ShowOneEvent;
  begin
    EventText.Parent := Panel1;
    pcEvents.Visible := False;
    EventText.ReadOnly := FReadOnly;
    ClearTabs;
  end;

  procedure CreateInheritedEvent(LC: TrwLibComponent);
  var
    i: Integer;
    h: String;
    Ownr: TrwComponent;
  begin
    h := AEvent.Name;
    if Assigned(AChildComponent) then
    begin
      h := AChildComponent.Name + '.' + h;

      Ownr := TrwComponent(AChildComponent.Owner);
      while Assigned(Ownr) and not (Ownr.IsLibComponent or (Ownr is TrwReport)) do
      begin
        if not (Ownr is TrwReportForm) then
          h := Ownr.Name + '.' + h;
        Ownr := TrwComponent(Ownr.Owner);
      end;
    end;

    i := LC.VMT.IndexOf(h);

    if i = -1 then Exit;

    T := TTabSheet.Create(pcEvents);
    T.Caption := LC.Name;
    T.PageControl := pcEvents;
    EvText := TrwEventTextEdit.Create(nil);
    EvText.OnSpecialLineColors := OnSpecialLineColors;
    EvText.TabWidth := 2;
    EvText.Align := alClient;
    EvText.Parent := T;
    EvText.HighLighter := GeneralSyn;
    EvText.OnSelectionChange := OnChangeCursPos;
    EvText.Lines.Text := MakeEventHandlerHeader(AEvent) + #13 + #10 + LC.VMT[i].Text+ #13 + #10 + 'end';
    EvText.Lines.Delete(0);
    EvText.Readonly := True;

    i := SystemLibComponents.IndexOf(LC.ParentName);
    if i <> -1 then
      CreateInheritedEvent(SystemLibComponents.Items[i]);
  end;

begin
  if not Assigned(ALibComp) then
    ShowOneEvent

  else
  begin
    h := AEvent.Name;
    if Assigned(AChildComponent) then
    begin
      h := AChildComponent.Name + '.' + h;

      Ownr := TrwComponent(AChildComponent.Owner);
      while Assigned(Ownr) and not (Ownr.IsLibComponent or (Ownr is TrwReport)) do
      begin
        if not (Ownr is TrwReportForm) then
          h := Ownr.Name + '.' + h;
        Ownr := TrwComponent(Ownr.Owner);
      end;
    end;

    i := ALibComp.VMT.IndexOf(h);

    if i = -1 then
    begin
      ShowOneEvent;
      EventText.ReadOnly := not ShowOverridenEvent or FReadOnly;
      Exit;
    end;

    if AClear then
    begin
      EventText.Parent := nil;
      ClearTabs;

      if ShowOverridenEvent then
      begin
        T := TTabSheet.Create(pcEvents);
        T.Caption := lcOverriding;
        T.PageControl := pcEvents;
        EventText.Parent := T;
      end;
    end;

    CreateInheritedEvent(ALibComp);

    pcEvents.Visible := True;
  end;
end;


procedure TrwPageDesigner.OnSpecialLineColors(Sender: TObject; Line: integer; var Special: boolean; var FG, BG: TColor);
var
  brkp: Boolean;
begin
  if Assigned(FOnCheckBreakPoint) then
  begin
    brkp := OnCheckBreakPoint(Self, Line);
    if brkp then
    begin
      BG := clRed;
      FG := clWhite;
      Special := True;
    end
  end
  else
    brkp := False;

  if Debugging and (DebugInfo.CurrentComponent = rwPropertiesEditor.CurrentComponent) and
     SameText(DebugInfo.CurrentEvent, rwPropertiesEditor.CurrentEventName) and
     SameText(DebugInfo.CurrentLibClass, CurrentLibClass) then
  begin
    if Line = DebugInfo.Line then
    begin
      if brkp then
        BG := clMaroon
      else
        BG := clNavy;
      FG := clWhite;
      Special := True;
    end;
  end;

  if Line = RunTimeErrorLine then
  begin
    BG := clLime;
    FG := clBlack;
    Special := True;
    RunTimeErrorLine := 0;
  end;
end;


procedure TrwPageDesigner.SetPos(const APos: Integer);
begin
  SetPosition(CurrentHandler.PosToCoor(APos));
end;


procedure TrwPageDesigner.InvalidateEventLine(const ALine: Integer);
begin
  CurrentHandler.InvalidateLine(ALine);
end;


procedure TrwPageDesigner.pcEventsChange(Sender: TObject);
begin
  FCurrentLibClass := pcEvents.ActivePage.Caption;
  if FCurrentLibClass = lcOverriding then
    FCurrentLibClass := '';
  CurrentHandler.Invalidate;    
end;


function TrwPageDesigner.CurrentHandler: TmwCustomEdit;
begin
 if pcEvents.Visible then
   Result := TmwCustomEdit(pcEvents.ActivePage.Controls[0])
 else
   Result := EventText;
end;


procedure TrwPageDesigner.SetReadOnly(const Value: Boolean);
begin
  FReadOnly := Value;
  EventText.ReadOnly := FReadOnly;
  rwPropertiesEditor.ReadOnly := FReadOnly;
end;


end.
