inherited rmOPEMasterFields: TrmOPEMasterFields
  Width = 342
  Height = 178
  object Label3: TLabel
    Left = 0
    Top = 0
    Width = 103
    Height = 13
    Caption = 'Master-Detail Join'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lHeader2: TLabel
    Left = 16
    Top = 19
    Width = 159
    Height = 18
    AutoSize = False
    Caption = 'Parent Query Field'
    Color = 14737632
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Layout = tlCenter
  end
  object Label1: TLabel
    Left = 182
    Top = 19
    Width = 159
    Height = 18
    AutoSize = False
    Caption = 'Query Field'
    Color = 14737632
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Layout = tlCenter
  end
  inline frmJoin1: TrmJoinFields
    Left = 0
    Top = 41
    Width = 341
    Height = 21
    AutoScroll = False
    AutoSize = True
    TabOrder = 0
  end
  inline frmJoin2: TrmJoinFields
    Left = 0
    Top = 69
    Width = 341
    Height = 21
    AutoScroll = False
    AutoSize = True
    TabOrder = 1
    inherited lNumber: TSpeedButton
      Caption = '2'
    end
  end
  inline frmJoin3: TrmJoinFields
    Left = 0
    Top = 98
    Width = 341
    Height = 21
    AutoScroll = False
    AutoSize = True
    TabOrder = 2
    inherited lNumber: TSpeedButton
      Caption = '3'
    end
  end
  inline frmJoin4: TrmJoinFields
    Left = 0
    Top = 126
    Width = 341
    Height = 21
    AutoScroll = False
    AutoSize = True
    TabOrder = 3
    inherited lNumber: TSpeedButton
      Caption = '4'
    end
  end
  inline frmJoin5: TrmJoinFields
    Left = 0
    Top = 155
    Width = 341
    Height = 21
    AutoScroll = False
    AutoSize = True
    TabOrder = 4
    inherited lNumber: TSpeedButton
      Caption = '5'
    end
  end
end
