unit rmOPEXMLFormSchemasFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmObjPropertyEditorFrm, ComCtrls, StdCtrls, rmTypes,
  rmObjectPropFormFrm;

type
  TrmOPEXMLFormSchemas = class(TrmObjPropertyEditor)
    lvSchemas: TListView;
    btnAdd: TButton;
    btnDel: TButton;
    odlgXMLSchema: TOpenDialog;
    btnSave: TButton;
    saveXMLSchema: TSaveDialog;
    btnUpdate: TButton;
    udlgXMLSchema: TOpenDialog;
    btnAddNoParse: TButton;
    adlgXMLSchema: TOpenDialog;
    procedure btnAddClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure lvSchemasSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure btnSaveClick(Sender: TObject);
    procedure btnUpdateClick(Sender: TObject);
    procedure btnAddNoParseClick(Sender: TObject);
  private
    procedure MakeSchemaList;
  public
    procedure ShowPropValue; override;
  end;

implementation

{$R *.dfm}

{ TrmOPEXMLFormSchemas }

procedure TrmOPEXMLFormSchemas.MakeSchemaList;
var
  Schemas: IrmXMLSchemas;
  Schema: IrmXMLSchema;
  i: Integer;
  Itm: TListItem;
begin
  Schemas := (TComponent(LinkedObject) as IrmXMLFormObject).GetSchemas;

  lvSchemas.Items.BeginUpdate;
  try
    lvSchemas.Items.Clear;

    for i := 0 to Schemas.GetItemCount - 1 do
    begin
      Schema := Schemas.GetItemByIndex(i) as IrmXMLSchema;
      Itm := lvSchemas.Items.Add;
      Itm.Caption := Schema.GetName;
      Itm.SubItems.Add(Schema.GetDescription);
    end;

  finally
    lvSchemas.Items.EndUpdate;
  end;
end;

procedure TrmOPEXMLFormSchemas.ShowPropValue;
begin
  inherited;
  MakeSchemaList;
end;

procedure TrmOPEXMLFormSchemas.btnAddClick(Sender: TObject);
begin
  if odlgXMLSchema.Execute then
  begin
    (TComponent(LinkedObject) as IrmXMLFormObject).LoadXSDSchema(odlgXMLSchema.FileName);
    NotifyOfChanges;
  end;
end;

procedure TrmOPEXMLFormSchemas.btnDelClick(Sender: TObject);
var
  Schemas: IrmXMLSchemas;
  DlgRes: integer;
begin
  DlgRes := MessageDlg('Do you want to delete selected schema?', mtConfirmation, [mbYes, mbCancel], 0);
  if DlgRes = mrYes then
  begin
    Schemas := (TComponent(LinkedObject) as IrmXMLFormObject).GetSchemas;
    Schemas.DeleteItem(lvSchemas.Selected.Index);
    NotifyOfChanges;
  end;
end;

procedure TrmOPEXMLFormSchemas.lvSchemasSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
var
  Schemas: IrmXMLSchemas;
  Schema: IrmXMLSchema;
begin
  if not Assigned(lvSchemas.Selected) then
  begin
    btnDel.Enabled := False;
    btnSave.Enabled := False;
    btnUpdate.Enabled := False;
    Exit;
  end;

  Schemas := (TComponent(LinkedObject) as IrmXMLFormObject).GetSchemas;
  Schema := Schemas.GetItemByIndex(lvSchemas.Selected.Index) as IrmXMLSchema;

  btnDel.Enabled := not Schema.IsInheritedItem;
  btnUpdate.Enabled := not Schema.IsInheritedItem;
  btnAddNoParse.Enabled := True;
  btnSave.Enabled := True;
end;

procedure TrmOPEXMLFormSchemas.btnSaveClick(Sender: TObject);
var
  Form: IrmXMLFormObject;
begin
  if Assigned(lvSchemas.Selected) then
  begin
    Form := TComponent(LinkedObject) as IrmXMLFormObject;
    saveXMLSchema.FileName := lvSchemas.Selected.Caption + '.xsd';
    if saveXMLSchema.Execute then
      Form.SaveXSDSchema(lvSchemas.Selected.Caption, saveXMLSchema.FileName);
  end;
end;

procedure TrmOPEXMLFormSchemas.btnUpdateClick(Sender: TObject);
var
  Form: IrmXMLFormObject;
  DlgRes: integer;
  sFileName: String;
begin
  DlgRes := MessageDlg('Do you want to update selected schema? Report will have to be updated manually!', mtConfirmation, [mbYes, mbCancel], 0);
  if DlgRes = mrYes then
  begin
    sFileName := lvSchemas.Selected.Caption + '.xsd';
    udlgXMLSchema.Filter := sFileName + '|' + sFileName;
    udlgXMLSchema.FileName := sFileName;
    if udlgXMLSchema.Execute then
    begin
      Form := TComponent(LinkedObject) as IrmXMLFormObject;
      Form.UpdateXSDSchema(lvSchemas.Selected.Caption, udlgXMLSchema.FileName);
    end;
  end;
end;

procedure TrmOPEXMLFormSchemas.btnAddNoParseClick(Sender: TObject);
var
  Form: IrmXMLFormObject;
  DlgRes: integer;
  sFileName: String;
begin
  DlgRes := MessageDlg('Do you want to load a new schema file without parsing? Report will have to be updated manually!', mtConfirmation, [mbYes, mbCancel], 0);
  if DlgRes = mrYes then
  begin
    if adlgXMLSchema.Execute then
    begin
      sFileName := adlgXMLSchema.FileName;
      Form := TComponent(LinkedObject) as IrmXMLFormObject;
      Form.AddXSDSchema(adlgXMLSchema.FileName);
      NotifyOfChanges;
    end;
  end;
end;

end.
