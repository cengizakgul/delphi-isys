unit rwPCLFilePropertyEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rwPropertyEditorFrm, StdCtrls, rwPrintElements;

type
  TrwPCLFilePropertyEditor = class(TrwPropertyEditor)
    OpenDialog: TOpenDialog;
    memPCL: TMemo;
    btnLoad: TButton;
    btnSave: TButton;
    SaveDialog: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
  protected
    procedure SetReadOnly; override; 
  end;


implementation

{$R *.DFM}



procedure TrwPCLFilePropertyEditor.FormCreate(Sender: TObject);
begin
  inherited;

  FValue := TrwPCLFile.Create;
end;

procedure TrwPCLFilePropertyEditor.btnLoadClick(Sender: TObject);
begin
  if OpenDialog.Execute then
  begin
    TrwPCLFile(FValue).LoadFromFile(OpenDialog.FileName);
    memPCL.Text := TrwPCLFile(FValue).Data;
  end;
end;

procedure TrwPCLFilePropertyEditor.FormShow(Sender: TObject);
begin
  inherited;
  memPCL.Text := TrwPCLFile(FValue).Data;
end;

procedure TrwPCLFilePropertyEditor.btnSaveClick(Sender: TObject);
begin
  if SaveDialog.Execute then
    TrwPCLFile(FValue).SaveToFile(SaveDialog.FileName);
end;

procedure TrwPCLFilePropertyEditor.SetReadOnly;
begin
  inherited;
  memPCL.ReadOnly := True;
  btnLoad.Enabled := False;
end;

initialization
  RegisterClass(TrwPCLFilePropertyEditor);

end.
