unit rwBasicToolsBarFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ToolWin, ExtCtrls, isRegistryFunctions, rwDsgnRes, rwBasicClasses,
  rwUtils, rwBasicUtils, Variants, Menus;

type

  TrwFontToolBarEvent = (fteFontName, fteFontSize, fteFontBold, fteFontItalic, fteFontUnderline,
    fteTextAlignLeft, fteTextAlignCenter, fteTextAlignRight, fteFontColor,
    fteFontHighlightColor);

  TrwOnClickComponentsToolBar = procedure(Sender: TObject; AComponentClassName: string; AMultiCreation: Boolean) of object;

  TrwOnClickActionToolBar = procedure(Sender: TObject; AAction: TrwActionToolBarEvent) of object;

  TrwOnChangesFontToolBar = procedure(Sender: TObject; EventType: TrwFontToolBarEvent) of object;


  TrwBasicToolsBar = class(TFrame)
    ctbBar: TControlBar;
    tlbComponents: TToolBar;
    tbtSelect: TToolButton;
    tlbAlignment: TToolBar;
    tbtAlignmentLeft: TToolButton;
    tbtAlignmentTop: TToolButton;
    tbtAlignmentBottom: TToolButton;
    tbtAlignmentRight: TToolButton;
    ToolButton7: TToolButton;
    ToolButton14: TToolButton;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton9: TToolButton;
    ToolButton8: TToolButton;
    tlbSize: TToolBar;
    ToolButton2: TToolButton;
    ToolButton6: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    tlbFont: TToolBar;
    cbFontName: TComboBox;
    cbFontSize: TComboBox;
    ToolButton4: TToolButton;
    tbtFontBold: TToolButton;
    tbtFontItalic: TToolButton;
    tbtFontUnderline: TToolButton;
    ToolButton1: TToolButton;
    tbtTextAlignLeft: TToolButton;
    tbtTextAlignCenter: TToolButton;
    tbtTextAlignRight: TToolButton;
    ToolButton10: TToolButton;
    tbtFontColor: TToolButton;
    tbtHighlightColor: TToolButton;
    ToolButton11: TToolButton;
    tbtBringToFront: TToolButton;
    tbtSendToBack: TToolButton;
    pmToolBars: TPopupMenu;

    procedure tbtQueryClick(Sender: TObject);
    procedure tbtQueryMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure tbtSelectMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ToolButton2Click(Sender: TObject);
    procedure tbtHighlightColorClick(Sender: TObject);
    procedure tbtFontColorClick(Sender: TObject);
    procedure tbtTextAlignRightClick(Sender: TObject);
    procedure tbtTextAlignCenterClick(Sender: TObject);
    procedure tbtTextAlignLeftClick(Sender: TObject);
    procedure tbtFontUnderlineClick(Sender: TObject);
    procedure tbtFontItalicClick(Sender: TObject);
    procedure tbtFontBoldClick(Sender: TObject);
    procedure cbFontSizeChange(Sender: TObject);
    procedure cbFontNameChange(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure pmToolBarsPopup(Sender: TObject);

  private
    FOnChangesFontToolBar: TrwOnChangesFontToolBar;
    FOnClickComponentsToolBar: TrwOnClickComponentsToolBar;
    FOnClickActionToolBar: TrwOnClickActionToolBar;
    FMultiCreation: Boolean;

    procedure ChangingState(AEvent: TrwFontToolBarEvent);
    procedure SetSelectMode;
    procedure ClearMarks;
    procedure OnClickToolBarMenu(Sender: TObject);
    procedure CreateToolBarMenu;

  protected
    FUpdating: Boolean;

    procedure DoCreateComponent(AToolButton: TToolButton);
    procedure DoActionToolBar(AToolButton: TToolButton);
    procedure InitFontList; virtual;
    function  AllowedToolBar(AToolBar: TToolBar): Boolean; virtual;

  public
    constructor Create(AOwner: TComponent); override;
    procedure SetToolBarByComponentProperties(AComponent: TrwComponent; AUnion: Boolean); virtual;
    procedure SaveState;
    procedure RestoreState;
    procedure UndockedBarsVisible(AVisible: Boolean);
    function  GetImageIndexForComponent(const AComponentClassName: string): Integer; virtual;
    procedure AssignImageForComponent(const AComponentClassName: string; AImage: TBitmap); virtual;

    property OnChangesFontToolBar: TrwOnChangesFontToolBar read FOnChangesFontToolBar write FOnChangesFontToolBar;
    property OnClickComponentsToolBar: TrwOnClickComponentsToolBar read FOnClickComponentsToolBar write FOnClickComponentsToolBar;
    property OnClickActionToolBar: TrwOnClickActionToolBar read FOnClickActionToolBar write FOnClickActionToolBar;
  end;

implementation


{$R *.DFM}

{ TFrame1 }

procedure TrwBasicToolsBar.ChangingState(AEvent: TrwFontToolBarEvent);
begin
  if Assigned(FOnChangesFontToolBar) then
    FOnChangesFontToolBar(Self, AEvent);
end;

procedure TrwBasicToolsBar.ClearMarks;
var
  i: Integer;
begin
  for i := 0  to tlbComponents.ButtonCount-1 do
    tlbComponents.Buttons[i].Marked := False;
end;

procedure TrwBasicToolsBar.DoActionToolBar(AToolButton: TToolButton);
begin
  if Assigned(FOnClickActionToolBar) then
    FOnClickActionToolBar(Self, TrwActionToolBarEvent(AToolButton.Tag));
end;

procedure TrwBasicToolsBar.DoCreateComponent(AToolButton: TToolButton);
begin
  if Assigned(FOnClickComponentsToolBar) then
    FOnClickComponentsToolBar(Self, AToolButton.Caption, FMultiCreation);
end;

procedure TrwBasicToolsBar.SetSelectMode;
begin
  ClearMarks;
  tbtSelect.Down := True;
end;

procedure TrwBasicToolsBar.tbtQueryClick(Sender: TObject);
begin
  DoCreateComponent(TToolButton(Sender));
end;

procedure TrwBasicToolsBar.tbtQueryMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if not TToolButton(Sender).Down then
  begin
    ClearMarks;
    FMultiCreation := (Shift = [ssShift, ssLeft]);
    TToolButton(Sender).Marked := FMultiCreation;
  end;
end;

procedure TrwBasicToolsBar.tbtSelectMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FMultiCreation := False;
  ClearMarks;
end;

procedure TrwBasicToolsBar.ToolButton2Click(Sender: TObject);
begin
  DoActionToolBar(TToolButton(Sender));
end;

procedure TrwBasicToolsBar.tbtHighlightColorClick(Sender: TObject);
begin
  ChangingState(fteFontHighlightColor);
end;

procedure TrwBasicToolsBar.tbtFontColorClick(Sender: TObject);
begin
  ChangingState(fteFontColor);
end;

procedure TrwBasicToolsBar.tbtTextAlignRightClick(Sender: TObject);
begin
  ChangingState(fteTextAlignRight);
end;

procedure TrwBasicToolsBar.tbtTextAlignCenterClick(Sender: TObject);
begin
  ChangingState(fteTextAlignCenter);
end;

procedure TrwBasicToolsBar.tbtTextAlignLeftClick(Sender: TObject);
begin
  ChangingState(fteTextAlignLeft);
end;

procedure TrwBasicToolsBar.tbtFontUnderlineClick(Sender: TObject);
begin
  ChangingState(fteFontUnderline);
end;

procedure TrwBasicToolsBar.tbtFontItalicClick(Sender: TObject);
begin
  ChangingState(fteFontItalic);
end;

procedure TrwBasicToolsBar.tbtFontBoldClick(Sender: TObject);
begin
  ChangingState(fteFontBold);
end;

procedure TrwBasicToolsBar.cbFontSizeChange(Sender: TObject);
begin
  ChangingState(fteFontSize);
end;

procedure TrwBasicToolsBar.cbFontNameChange(Sender: TObject);
begin
  ChangingState(fteFontName);
end;

procedure TrwBasicToolsBar.SetToolBarByComponentProperties(AComponent: TrwComponent; AUnion: Boolean);
var
  Value: Variant;
begin
  FUpdating := True;
  if not FMultiCreation then
    SetSelectMode;

  if not AUnion then
  begin
    Value := GetComponentProperty(AComponent, ['Font', 'Name']);
    if (Value <> Null) then
      cbFontName.ItemIndex := cbFontName.Items.IndexOf(Value)
    else
      cbFontName.ItemIndex := -1;

    Value := GetComponentProperty(AComponent, ['Font', 'Size']);
    if (Value <> Null) then
      cbFontSize.Text := Value
    else
      cbFontSize.Text := '';

    Value := GetComponentProperty(AComponent, ['Font', 'Style']);
    if (Value <> Null) then
    begin
      tbtFontBold.Down := (LongInt(fsBold) in TIntegerSet(LongInt(Value)));
      tbtFontItalic.Down := (LongInt(fsItalic) in TIntegerSet(LongInt(Value)));
      tbtFontUnderline.Down := (LongInt(fsUnderline) in TIntegerSet(LongInt(Value)));
    end
    else
    begin
      tbtFontBold.Down := False;
      tbtFontItalic.Down := False;
      tbtFontUnderline.Down := False;
    end;

    Value := GetComponentProperty(AComponent, ['Alignment']);
    if (Value <> Null) then
    begin
      tbtTextAlignLeft.Down := TAlignment(LongInt(Value)) = taLeftJustify;
      tbtTextAlignCenter.Down := TAlignment(LongInt(Value)) = taCenter;
      tbtTextAlignRight.Down := TAlignment(LongInt(Value)) = taRightJustify;
    end
    else
    begin
      tbtTextAlignLeft.Down := False;
      tbtTextAlignCenter.Down := False;
      tbtTextAlignRight.Down := False;
    end;
  end

  else
  begin
    cbFontName.ItemIndex := -1;

    cbFontSize.Text := '';

    tbtFontBold.Down := False;
    tbtFontItalic.Down := False;
    tbtFontUnderline.Down := False;

    tbtTextAlignLeft.Down := False;
    tbtTextAlignCenter.Down := False;
    tbtTextAlignRight.Down := False;
  end;

  FUpdating := False;
end;

constructor TrwBasicToolsBar.Create(AOwner: TComponent);
begin
  inherited;

  FUpdating := False;
  FMultiCreation := False;
  InitFontList;
  CreateToolBarMenu;
end;

procedure TrwBasicToolsBar.RestoreState;
var
  i: Integer;
  h: String;
  mh, L, T: Integer;
  D, V: Boolean;
  R: TRect;
  C: TToolBar;
begin
  ctbBar.Height := 400;

  for i := 0 to ComponentCount - 1 do
    if (Components[i] is TToolBar) and AllowedToolBar(TToolBar(Components[i])) then
    begin
      C := TToolBar(Components[i]);
      h := ReadFromRegistry('Misc\'+Owner.Name+'\'+Name, C.Name, '');
      if Length(h) > 0 then
        try
          D := Boolean(StrToInt(GetNextStrValue(h, ',')));
          L := StrToInt(GetNextStrValue(h, ','));
          T := StrToInt(GetNextStrValue(h, ','));
          V := GetNextStrValue(h, ',') <> '0';

          if D then
          begin
            C.Left := L;
            C.Top := T;
            if not V then
            begin
              C.Parent := nil;
              C.Visible := False;
              C.Parent := ctbBar;
            end;
          end
          else
          begin
            if V then
            begin
              R.Left := L;
              R.Top  := T;
              R.Right := R.Left + C.Width*2;
              R.Bottom := R.Top + C.Height*2;
              C.ManualFloat(R);
            end
            else
            begin
              C.Parent := nil;
              C.Visible := False;
              C.Parent := ctbBar;
            end;
          end;
        except
        end;
    end;


  mh := 0;
  for i := 0 to ComponentCount - 1 do
    if (Components[i] is TToolBar) and AllowedToolBar(TToolBar(Components[i])) then
    begin
      C := TToolBar(Components[i]);
      h := ReadFromRegistry('Misc\'+Owner.Name+'\'+Name, C.Name, '');
      if Length(h) > 0 then
        try
          D := Boolean(StrToInt(GetNextStrValue(h, ',')));
          L := StrToInt(GetNextStrValue(h, ','));
          T := StrToInt(GetNextStrValue(h, ','));
          if D then
          begin
            C.Left := L;
            C.Top  := T;
            if C.Visible and (mh < C.Top + C.Height) then
              mh := C.Top + C.Height;
          end;
        except
        end;
    end;

   ctbBar.Height := mh;
   Height := mh;
end;

procedure TrwBasicToolsBar.SaveState;
var
  i: Integer;
  C: TToolBar;
  h: String;
begin
  for i := 0 to ComponentCount - 1 do
    if (Components[i] is TToolBar) and AllowedToolBar(TToolBar(Components[i])) then
    begin
      C := TToolBar(Components[i]);
      if C.Visible then
        if C.Parent is TControlBar then
          h := '1,' + IntToStr(C.Left) + ',' + IntToStr(C.Top) + ',1'
        else
          h := '0,' + IntToStr(C.Parent.Left) + ',' + IntToStr(C.Parent.Top) + ',1'
      else
        h := '1,' + IntToStr(C.Left) + ',' + IntToStr(C.Top) + ',0';
      WriteToRegistry('Misc\'+Owner.Name+'\'+Name, C.Name, h);
    end;
end;

procedure TrwBasicToolsBar.UndockedBarsVisible(AVisible: Boolean);
var
  i: Integer;
  C: TToolBar;
begin
  for i := 0 to ComponentCount - 1 do
    if (Components[i] is TToolBar) and AllowedToolBar(TToolBar(Components[i])) then
    begin
      C := TToolBar(Components[i]);
      if C.Parent is FloatingDockSiteClass then
        C.Parent.Visible := AVisible;
    end;
end;

procedure TrwBasicToolsBar.FrameResize(Sender: TObject);
begin
  ctbBar.Width := ClientWidth;
  ctbBar.Height := ClientHeight;
end;

function TrwBasicToolsBar.GetImageIndexForComponent(
  const AComponentClassName: string): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to ComponentCount - 1 do
    if (Components[i] is TToolButton) and
      SameText(TToolButton(Components[i]).Caption, AComponentClassName) then
    begin
      Result := TToolButton(Components[i]).ImageIndex;
      break;
    end;
end;


procedure TrwBasicToolsBar.AssignImageForComponent(
  const AComponentClassName: string; AImage: TBitmap);
var
  i: Integer;
begin
  i := GetImageIndexForComponent(AComponentClassName);
  if i <> -1 then
    DsgnRes.ilButtons.GetBitmap(i, AImage);
end;

procedure TrwBasicToolsBar.InitFontList;
begin
  cbFontName.Items.Clear;
end;

procedure TrwBasicToolsBar.CreateToolBarMenu;
var
  i: Integer;
  MI: TMenuItem;
begin
  for i := 0 to ComponentCount - 1 do
    if (Components[i] is TToolBar) and AllowedToolBar(TToolBar(Components[i])) then
    begin
      MI := TMenuItem.Create(Self);
      MI.Caption := TToolBar(Components[i]).Caption;
      MI.Tag := Integer(Pointer(Components[i]));
      MI.OnClick := OnClickToolBarMenu;
      pmToolBars.Items.Add(MI);
    end;
end;

procedure TrwBasicToolsBar.pmToolBarsPopup(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to pmToolBars.Items.Count - 1 do
    pmToolBars.Items[i].Checked := TToolBar(Pointer(pmToolBars.Items[i].Tag)).Visible;
end;

procedure TrwBasicToolsBar.OnClickToolBarMenu(Sender: TObject);
begin
  TMenuItem(Sender).Checked := not TMenuItem(Sender).Checked;
  if TMenuItem(Sender).Checked then
  begin
    TToolBar(Pointer(TMenuItem(Sender).Tag)).Left := 0;
    TToolBar(Pointer(TMenuItem(Sender).Tag)).Top := ctbBar.Height;
    TToolBar(Pointer(TMenuItem(Sender).Tag)).Visible := True;
  end
  else
    TToolBar(Pointer(TMenuItem(Sender).Tag)).Visible := False;
end;

function TrwBasicToolsBar.AllowedToolBar(AToolBar: TToolBar): Boolean;
begin
  Result := True;
end;

end.
