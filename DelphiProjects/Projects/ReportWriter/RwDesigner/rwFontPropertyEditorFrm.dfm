inherited rwFontPropertyEditor: TrwFontPropertyEditor
  Left = 401
  Top = 136
  Width = 200
  Height = 140
  Caption = 'rwFontPropertyEditor'
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOK: TButton
    Left = 111
  end
  inherited btnCancel: TButton
    Left = 111
  end
  object FontDialog: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [fdTrueTypeOnly, fdEffects]
    Left = 16
    Top = 32
  end
end
