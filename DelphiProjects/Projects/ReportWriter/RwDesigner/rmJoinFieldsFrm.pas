unit rmJoinFieldsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmJoinItemsFrm, StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb,
  rmTypes, rwDsgnUtils, Buttons;

type
  TrmJoinFields = class(TrmJoinItems)
  private
  public
    procedure MakeLookups; override;  
  end;

implementation

{$R *.dfm}

{ TrmJoinFields }

procedure TrmJoinFields.MakeLookups;
begin
  cbLeftItem.Items.Text := MakeComboBoxFieldList((TComponent(PropEditor.LinkedObject) as IrmQueryDrivenObject).GetQuery.GetMasterQuery.GetQBQuery.GetFieldList);
  cbRightItem.Items.Text := MakeComboBoxFieldList((TComponent(PropEditor.LinkedObject) as IrmQueryDrivenObject).GetQuery.GetQBQuery.GetFieldList);
end;

end.
 