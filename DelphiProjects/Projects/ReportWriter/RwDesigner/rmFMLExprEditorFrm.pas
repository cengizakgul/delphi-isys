unit rmFMLExprEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, rmFMLExprPanelFrm, Buttons, rmTypes,
  rmFormula, rwBasicClasses, rwParser, rmDsgnTypes, rwBasicUtils, rwUtils,
  rwEngineTypes, rwDsgnRes, rwTypes;

type
  TrmFMLExprEditor = class(TFrame)
    Panel1: TPanel;
    pcItem: TPageControl;
    tsObject: TTabSheet;
    tvObjects: TTreeView;
    tsValue: TTabSheet;
    Label1: TLabel;
    cbConst: TComboBox;
    pnlVal: TPanel;
    Label2: TLabel;
    edValue: TEdit;
    tsFunction: TTabSheet;
    tvFuncts: TTreeView;
    Panel2: TPanel;
    lFuncDecl: TLabel;
    lFunctDescr: TLabel;
    Panel3: TPanel;
    grbExpression: TGroupBox;
    pnlExpr: TrmFMLExprPanel;
    Splitter1: TSplitter;
    pnlSpace1: TPanel;
    Panel4: TPanel;
    btnPlus: TSpeedButton;
    btnMinus: TSpeedButton;
    btnMul: TSpeedButton;
    btnDiv: TSpeedButton;
    btnAND: TSpeedButton;
    btnOR: TSpeedButton;
    btnGreat: TSpeedButton;
    btnLess: TSpeedButton;
    btnEqual: TSpeedButton;
    btnNotEqual: TSpeedButton;
    btnGreatEqual: TSpeedButton;
    btnLessEqual: TSpeedButton;
    btnBrackets: TSpeedButton;
    btnIns: TSpeedButton;
    btnDel: TSpeedButton;
    btnNot: TSpeedButton;
    pnlObjProperty: TPanel;
    Label3: TLabel;
    cbObjectProperty: TComboBox;
    tsVariable: TTabSheet;
    tvVariables: TTreeView;
    procedure pcItemResize(Sender: TObject);
    procedure btnInsClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnPlusClick(Sender: TObject);
    procedure btnMinusClick(Sender: TObject);
    procedure btnMulClick(Sender: TObject);
    procedure btnDivClick(Sender: TObject);
    procedure btnANDClick(Sender: TObject);
    procedure btnORClick(Sender: TObject);
    procedure btnBracketsClick(Sender: TObject);
    procedure cbConstChange(Sender: TObject);
    procedure tvObjectsChange(Sender: TObject; Node: TTreeNode);
    procedure tsObjectShow(Sender: TObject);
    procedure tsValueShow(Sender: TObject);
    procedure tsFunctionShow(Sender: TObject);
    procedure edValueExit(Sender: TObject);
    procedure edValueKeyPress(Sender: TObject; var Key: Char);
    procedure tvFunctsChange(Sender: TObject; Node: TTreeNode);
    procedure tvFunctsDblClick(Sender: TObject);
    procedure pcItemChanging(Sender: TObject; var AllowChange: Boolean);
    procedure btnGreatClick(Sender: TObject);
    procedure btnLessClick(Sender: TObject);
    procedure btnEqualClick(Sender: TObject);
    procedure btnNotEqualClick(Sender: TObject);
    procedure btnGreatEqualClick(Sender: TObject);
    procedure btnLessEqualClick(Sender: TObject);
    procedure btnNotClick(Sender: TObject);
    procedure tvVariablesChange(Sender: TObject; Node: TTreeNode);
    procedure tsVariableShow(Sender: TObject);

  private
    FExpression: TrmFMLFormulaContent;
    FCurrentItemType: TrmFMLItemType;
    FCurrentItemValue: Variant;
    FCurrentItemAddInfo: String;
    FInsertion: Boolean;

    procedure SetExpression(const Value: TrmFMLFormulaContent);
    procedure AddItem(AType: TrmFMLItemType; const AValue: Variant; const AddInfo: String);
    procedure UpdateItem(AType: TrmFMLItemType; const AValue: Variant; const AddInfo: String);
    procedure RemoveItem;
    procedure Initialize;
    procedure CheckValue;
    procedure SyncSelection(AItem: TrmFMLFormulaItem);
    procedure SetButtons;
    procedure OnSelectItem(Sender: TObject);
    procedure AssignChValues(APage: TTabSheet);

  public
    procedure Clear;
    procedure CheckErrors;
    procedure AddDrgDrpObject(const AObject: IrmDesignObject);

    property Expression: TrmFMLFormulaContent read FExpression write SetExpression;
  end;

implementation

uses rwDsgnUtils;

{$R *.dfm}

const
  cFolderImageIndex = 0;
  cCustomObjectImageIndex = 31;
  cDataSetFieldImageIndex = 1;
  
procedure TrmFMLExprEditor.AddItem(AType: TrmFMLItemType; const AValue: Variant; const AddInfo: String);
var
  Expr: TrmFMLFormulaItem;
  i, j, n: Integer;
  Fnct: TrwFunction;
  V: Variant;
begin
  if not Assigned(pnlExpr.SelectedItem) then
    Exit;

  if (AType = fitOperation) and not FInsertion then
    UpdateItem(AType, AValue, AddInfo)

  else
  begin
    Expr := FExpression.AddItem(AType, AValue, AddInfo);

    n := pnlExpr.SelectedItem.ComponentIndex;
    if n > 0 then
      Expr.Index := TrmFMLExprItemLabel(pnlExpr.pnlCanvas.Components[n - 1]).ExprItem.Index + 1
    else
      Expr.Index := 0;

    if AType = fitFunction then
    begin
      j := Expr.Index + 1;
      Fnct := GlobalFindFunction(AValue);
      if not Assigned(Fnct) then
        Exit;

      for i := 1 to Fnct.Params.Count do
      begin
        V := Fnct.Params[i - 1].DefaultValue;
        if not VarIsEmpty(V) then
        begin
          Expr := FExpression.AddItem(fitConst, V);
          Expr.Index := j;
          Inc(j);
        end;

        if i < Fnct.Params.Count then
        begin
          Expr := FExpression.AddItem(fitSeparator, ',');
          Expr.Index := j;
          Inc(j);
        end;
      end;

      Expr := FExpression.AddItem(fitBracket, ')');
      Expr.Index := j;
    end;

    pnlExpr.ReDraw;
    pnlExpr.SelectedItem := TrmFMLExprItemLabel(pnlExpr.pnlCanvas.Components[n + 2]);
  end;
end;

procedure TrmFMLExprEditor.Initialize;

  procedure CreateObjectList;
  var
    R: IrmDesignObject;
    i: Integer;
    ObjType: TrwDsgnObjectType;
    SelObj: TrmListOfObjects;
    SelfObject: IrmDesignObject;
    h: String;

    function CreateObjectsFor(AParent: IrmDesignObject; ANode: TTreeNode): Boolean;
    var
      N, Nf: TTreeNode;
      h, h1: String;
      i: Integer;
      Q: IrmQuery;
      flSelf: Boolean;
      Obj: IrmDesignObject;
    begin
      Result := False;

      if AParent.ObjectType <> ObjType then
        Exit;

      flSelf := AParent =SelfObject;

      Result := AParent.PropertyExists('Value');  //so far

      if not(Result or (AParent.GetChildrenCount > 0)) then
        Exit;

      if flSelf then
        h := 'Self'
      else
      begin
        h := AParent.GetPropertyValue('Name');
        h1 := AParent.GetPropertyValue('Description');
        if h1 <> '' then
          h := h + ' (' + h1 + ')';
      end;

      N := tvObjects.Items.AddChildObject(ANode, h, Pointer(AParent));
      N.ImageIndex := cCustomObjectImageIndex;
      N.SelectedIndex := N.ImageIndex;
      N.StateIndex := N.ImageIndex;

      for i := 0 to AParent.GetChildrenCount - 1 do
      begin
        Obj := AParent.GetChildByIndex(i);
        if (Obj <> SelfObject) and CreateObjectsFor(Obj, N) then
          Result := True;
      end;

      // QB Fields
      if Supports(AParent.RealObject, IrmQueryDrivenObject) then
      begin
        Q := (AParent as IrmQueryDrivenObject).GetQuery;
        h := Q.GetQBQuery.GetFieldList;
        Result := Result or (h <> '');
        while h <> '' do
        begin
          h1 := GetNextStrValue(h, ',');
          Nf := tvObjects.Items.AddChildObject(N, h1 + ' (Field)', Pointer(Q.GetQBQuery.GetQBFieldByName(h1)));
          Nf.ImageIndex := cDataSetFieldImageIndex;
          Nf.SelectedIndex := Nf.ImageIndex;
          Nf.StateIndex := Nf.ImageIndex;
        end;
      end;

      if not Result then
        N.Free;
    end;

  begin
    tvObjects.Items.Clear;

    if RMDesigner.GetComponentDesigner.InputFormDesigninig then
      ObjType := rwDOTInputForm
    else if RMDesigner.GetComponentDesigner.PrintFormDesigninig then
      ObjType := rwDOTPrintForm
    else if RMDesigner.GetComponentDesigner.ASCIIFormDesigninig then
      ObjType := rwDOTASCIIForm
    else if RMDesigner.GetComponentDesigner.XMLFormDesigninig then
      ObjType := rwDOTXMLForm;

    SelObj := RMDesigner.GetComponentDesigner.GetSelectedObjects;
    if Length(SelObj) = 1 then
    begin
      SelfObject := SelObj[0];
      CreateObjectsFor(SelfObject, nil);
      if Parent is TForm then
      begin
        h := SelfObject.GetPropertyValue('Description');
        if h = '' then
          h := SelfObject.GetPropertyValue('Name');
        TForm(Parent).Caption := TForm(Parent).Caption + ' for "' + h + '"'; 
      end;
    end
    else
      SelfObject := nil;

    R := RMDesigner.GetComponentDesigner.DesigningObject;
    for i := 0 to R.GetChildrenCount - 1 do
      if R.GetChildByIndex(i).ObjectType = ObjType then
        CreateObjectsFor(R.GetChildByIndex(i), nil);

    tsObject.TabVisible := tvObjects.Items.Count > 0;
  end;


  procedure CreateListOfVariables;
  var
    nParams, nOthers, N: TTreeNode;
    vfHolder: IrmVarFunctHolder;
    i: Integer;
    h: String;
    VarInfoRec: TrmVFHVarInfoRec;
  begin
    tvVariables.Items.BeginUpdate;
    tvVariables.Items.Clear;
    tvVariables.SortType := stNone;

    vfHolder := RMDesigner.GetComponentDesigner.DesigningObject.GetVarFunctHolder;

    if not Assigned(vfHolder) then
      Exit;

    nParams := tvVariables.Items.AddChildObject(nil, 'Input Form Parameters', Pointer(-1));
    nParams.ImageIndex := cFolderImageIndex;
    nParams.SelectedIndex := nParams.ImageIndex;
    nParams.StateIndex := nParams.ImageIndex;

    nOthers := tvVariables.Items.AddChildObject(nil, 'Worksheet Variables', Pointer(-1));
    nOthers.ImageIndex := cFolderImageIndex;
    nOthers.SelectedIndex := nOthers.ImageIndex;
    nOthers.StateIndex := nOthers.ImageIndex;

    for i := 0 to vfHolder.GetVariables.GetItemCount - 1 do
    begin
      VarInfoRec := (vfHolder.GetVariables.GetItemByIndex(i) as IrmVariable).GetVarInfo;

      if VarInfoRec.ShowInFmlEditor then
      begin
        if VarInfoRec.Visibility in [rvvPublished, rvvPublic] then
          N := nParams
        else
          N := nOthers;

        h := VarInfoRec.DisplayName;
        if h = '' then
          h := VarInfoRec.Name;
        h := h + '   (' + NameOfVarType(VarInfoRec.VarType) + ')';
        N := tvVariables.Items.AddChildObject(N, h, Pointer(i));
        N.ImageIndex := -1;
        N.SelectedIndex := N.ImageIndex;
        N.StateIndex := N.ImageIndex;
      end;
    end;

    if not nParams.HasChildren then
      FreeAndNil(nParams);

    if not nOthers.HasChildren then
      FreeAndNil(nOthers);

    tvVariables.SortType := stText;
    tvVariables.Items.EndUpdate;

    tsVariable.TabVisible := tvVariables.Items.Count > 0;
 end;


  procedure CreateListOfConstant;
  begin
    cbConst.Items.Clear;
    cbConst.Items.AddObject('Date',     Pointer(Ord(varDate)));
    cbConst.Items.AddObject('Float',    Pointer(Ord(varDouble)));
    cbConst.Items.AddObject('Integer',  Pointer(Ord(varInteger)));
    cbConst.Items.AddObject('String',   Pointer(Ord(varString)));
    cbConst.Items.AddObject('Null',     Pointer(Ord(varNull)));
    cbConst.Items.AddObject('True',     Pointer(Ord(varBoolean)));
    cbConst.Items.AddObject('False',    Pointer(Ord(varBoolean)));
  end;


  procedure CreateListOfFunctions;
  var
    i: Integer;

    procedure OnAddFunctNode(ANewNode: TTreeNode; var Accept: Boolean);
    begin
      if Assigned(ANewNode.Data) then
      begin
        ANewNode.Data := TrwLibCollectionItem(IrmLibCollectionItem(ANewNode.Data).RealObject);
        Accept := fdtShowInEditor in TrwFunction(ANewNode.Data).DesignTimeInfo;
        ANewNode.ImageIndex := -1;
      end
      else
        ANewNode.ImageIndex := cFolderImageIndex;
      ANewNode.SelectedIndex := ANewNode.ImageIndex;
      ANewNode.StateIndex := ANewNode.ImageIndex;
    end;

  begin
    tvFuncts.Items.BeginUpdate;
    tvFuncts.Items.Clear;
    tvFuncts.SortType := stNone;

    FillUpTreeViewCollection(RWFunctions, tvFuncts, nil, @OnAddFunctNode);
    FillUpTreeViewCollection(AggrFunctions, tvFuncts, nil, @OnAddFunctNode);
    FillUpTreeViewCollection(SystemLibFunctions, tvFuncts, nil, @OnAddFunctNode);

    for i := tvFuncts.Items.Count - 1 downto 0 do
      if (tvFuncts.Items[i].Level = 0) and not tvFuncts.Items[i].HasChildren then
        tvFuncts.Items[i].Free;

    tvFuncts.SortType := stText;
    tvFuncts.Items.EndUpdate;

    tsFunction.TabVisible := tvFuncts.Items.Count > 0;
  end;

begin
  CreateObjectList;
  CreateListOfConstant;
  CreateListOfVariables;
  CreateListOfFunctions;

  pnlExpr.Expression := FExpression;
  pnlExpr.OnSelectItem := OnSelectItem;
  pnlExpr.ReadOnly := False;
  pnlExpr.ReDraw;

  if FExpression.Count = 0 then
  begin
    pnlExpr.SelectedItem := TrmFMLExprItemLabel(pnlExpr.pnlCanvas.Components[0]);
    SetButtons;
  end

  else if FExpression.Count > 0 then
  begin
    pnlExpr.SelectedItem := TrmFMLExprItemLabel(pnlExpr.pnlCanvas.Components[1]);
    SyncSelection(pnlExpr.SelectedItem.ExprItem);
  end;
end;

procedure TrmFMLExprEditor.pcItemResize(Sender: TObject);
begin
  pnlExpr.SetBounds(8, 15, grbExpression.Width - 16, grbExpression.ClientHeight - 24);
  edValue.Width := tsValue.ClientWidth - edValue.Left;
end;

procedure TrmFMLExprEditor.RemoveItem;
var
  L1, L2: TrmFMLExprItemLabel;
  n: Integer;

  procedure DeleteFunction(ALeftExpr: TrmFMLFormulaItem; ARightExpr: TrmFMLFormulaItem);
  var
    i: Integer;
  begin
    i := ALeftExpr.Index;
    while TrmFMLFormulaItem(FExpression.Items[i]) <> ARightExpr do
      TrmFMLFormulaItem(FExpression.Items[i]).Free;
    ARightExpr.Free;
  end;

begin
  if not (Assigned(pnlExpr.SelectedItem) and Assigned(pnlExpr.SelectedItem.ExprItem))  then
    Exit;

  if pnlExpr.SelectedItem.ExprItem.ItemType = fitSeparator then
    Exit;

  n := pnlExpr.SelectedItem.ComponentIndex;
  L1 := pnlExpr.SelectedItem;
  pnlExpr.SelectedItem := nil;

  if L1.ExprItem.ItemType = fitBracket then
  begin
    L2 := pnlExpr.FindPairBracket(L1.ComponentIndex);
    if Assigned(L2) then
      if Assigned(L2.ExprItem) and (L2.ExprItem.ItemType = fitFunction) then
        DeleteFunction(L2.ExprItem, L1.ExprItem)
      else
      begin
        L2.ExprItem.Free;
        L1.ExprItem.Free;
      end;
  end

  else if L1.ExprItem.ItemType = fitFunction then
  begin
    L2 := pnlExpr.FindPairBracket(L1.ComponentIndex);
    if Assigned(L2) then
      DeleteFunction(L1.ExprItem, L2.ExprItem);
  end

  else
    L1.ExprItem.Free;

  pnlExpr.ReDraw;

  if n > pnlExpr.pnlCanvas.ComponentCount - 1 then
    if pnlExpr.pnlCanvas.ComponentCount - 2 < 0 then
      pnlExpr.SelectedItem := TrmFMLExprItemLabel(pnlExpr.pnlCanvas.Components[pnlExpr.pnlCanvas.ComponentCount - 1])
    else
      pnlExpr.SelectedItem := TrmFMLExprItemLabel(pnlExpr.pnlCanvas.Components[pnlExpr.pnlCanvas.ComponentCount - 2])
  else
    pnlExpr.SelectedItem := TrmFMLExprItemLabel(pnlExpr.pnlCanvas.Components[n]);    
end;

procedure TrmFMLExprEditor.UpdateItem(AType: TrmFMLItemType; const AValue: Variant; const AddInfo: String);
var
  Fnct1, Fnct2: TrwFunction;
begin
  if not Assigned(pnlExpr.SelectedItem) then
    Exit;

  if ((AType = fitOperation) and (pnlExpr.SelectedItem.ExprItem.ItemType = AType) or
     (pnlExpr.SelectedItem.ExprItem.ItemType >= fitObject) and (AType >= fitObject) or
     (AType = fitFunction) and (pnlExpr.SelectedItem.ExprItem.ItemType = AType)) and
     ((pnlExpr.SelectedItem.ExprItem.ItemType <> AType) or
      (VarType(pnlExpr.SelectedItem.ExprItem.Value) <> VarType(AValue)) or
      (VarToStr(pnlExpr.SelectedItem.ExprItem.Value) <> VarToStr(AValue)) or
      (pnlExpr.SelectedItem.ExprItem.AddInfo <> AddInfo)) then
  begin
    if (AType <> fitConst) and (AValue = '') then
      Exit;

    if AType = fitFunction then
    begin
      Fnct1 := GlobalFindFunction(pnlExpr.SelectedItem.ExprItem.Value);
      Fnct2 := GlobalFindFunction(AValue);
      if Fnct1.Params.Count <> Fnct2.Params.Count then
        Exit
      else
        pnlExpr.SelectedItem.ExprItem.Value := AValue;
    end

    else
    begin
      pnlExpr.SelectedItem.ExprItem.ItemType := AType;
      pnlExpr.SelectedItem.ExprItem.Value := AValue;
      pnlExpr.SelectedItem.ExprItem.AddInfo := AddInfo;
    end;

    pnlExpr.ReDraw;
  end;
end;

procedure TrmFMLExprEditor.btnInsClick(Sender: TObject);
begin
  if pcItem.ActivePage = tsValue then
    CheckValue;

  if FInsertion then
    AddItem(FCurrentItemType, FCurrentItemValue, FCurrentItemAddInfo)
  else
    UpdateItem(FCurrentItemType, FCurrentItemValue, FCurrentItemAddInfo);
end;

procedure TrmFMLExprEditor.CheckValue;
var
  vt: TVarType;
  h: String;
begin
  if cbConst.ItemIndex = -1 then
    raise ErwException.Create('Value type is empty')
  else
    FCurrentItemType := fitConst;

  vt := TVarType(Integer(cbConst.Items.Objects[cbConst.ItemIndex]));
  h := cbConst.Items[cbConst.ItemIndex];
  FCurrentItemAddInfo := '';

  case vt of
    varInteger:     begin
                      edValue.Text := Trim(edValue.Text);
                      FCurrentItemValue := StrToInt(edValue.Text);
                    end;

    varDouble:      begin
                      edValue.Text := Trim(edValue.Text);
                      FCurrentItemValue := StrToFloat(edValue.Text);
                    end;

    varString:      begin
                      edValue.Text := StringReplace(edValue.Text, '''', '', [rfReplaceAll]);
                      FCurrentItemValue := edValue.Text;
                    end;

    varDate:        begin
                      edValue.Text := Trim(edValue.Text);
                      FCurrentItemValue := StrToDateTime(edValue.Text);
                    end;

    varBoolean:     if SameText(h, 'True') then
                      FCurrentItemValue := True
                    else
                      FCurrentItemValue := False;

    varNull:        FCurrentItemValue := Null;
  end;
end;

procedure TrmFMLExprEditor.btnDelClick(Sender: TObject);
begin
  RemoveItem;
end;

procedure TrmFMLExprEditor.btnPlusClick(Sender: TObject);
begin
  AddItem(fitOperation, '+', '');
end;

procedure TrmFMLExprEditor.btnMinusClick(Sender: TObject);
begin
  AddItem(fitOperation, '-', '');
end;

procedure TrmFMLExprEditor.btnMulClick(Sender: TObject);
begin
  AddItem(fitOperation, '*', '');
end;

procedure TrmFMLExprEditor.btnDivClick(Sender: TObject);
begin
  AddItem(fitOperation, '/', '');
end;

procedure TrmFMLExprEditor.btnANDClick(Sender: TObject);
begin
  AddItem(fitOperation, 'AND', '');
end;

procedure TrmFMLExprEditor.btnORClick(Sender: TObject);
begin
  AddItem(fitOperation, 'OR', '');
end;

procedure TrmFMLExprEditor.btnBracketsClick(Sender: TObject);
var
  n: Integer;
begin
  AddItem(fitBracket, '(', '');
  n := pnlExpr.SelectedItem.ComponentIndex;
  AddItem(fitBracket, ')', '');
  pnlExpr.SelectedItem := TrmFMLExprItemLabel(pnlExpr.pnlCanvas.Components[n]);
end;

procedure TrmFMLExprEditor.SyncSelection(AItem: TrmFMLFormulaItem);
var
  L: TrmFMLExprItemLabel;
  vt: TVarType;
  h: string;

  procedure SelectFunction;
  var
    i: Integer;
  begin
    pcItem.ActivePage := tsFunction;
    for i := 0 to tvFuncts.Items.Count - 1 do
      if AnsiSameText(tvFuncts.Items[i].Text, AItem.Value) then
      begin
        tvFuncts.Items[i].Selected := True;
        break;
      end;
  end;


  procedure SelectObject;
  var
    i: Integer;
    C: IrmDesignObject;
  begin
    tvObjects.Selected := nil;
    pcItem.ActivePage := tsObject;
    C := ComponentByPath(AItem.Value, RMDesigner.GetComponentDesigner.DesigningObject);

    for i := 0 to tvObjects.Items.Count - 1 do
      if ObjectsAreTheSame(IrmDesignObject(tvObjects.Items[i].Data), C) then
      begin
        tvObjects.Items[i].Selected := True;
        break;
      end;
  end;

  procedure SelectVariable;
  var
    i: Integer;
    V: IrmVariable;
    vfHolder: IrmVarFunctHolder;
  begin
    tvVariables.Selected := nil;
    pcItem.ActivePage := tsVariable;

    vfHolder := RMDesigner.GetComponentDesigner.DesigningObject.GetVarFunctHolder;
    if Assigned(vfHolder) then
    begin
      V := vfHolder.GetVariables.GetVariableByName(AItem.Value);
      for i := 0 to tvVariables.Items.Count - 1 do
        if Integer(tvVariables.Items[i].Data) = V.GetItemIndex then
        begin
          tvVariables.Items[i].Selected := True;
          break;
        end;
    end;
  end;

  procedure SelectField;
  var
    i: Integer;
    Fld: IrwQBShowingField;
  begin
    tvObjects.Selected := nil;
    pcItem.ActivePage := tsObject;
    Fld := AItem.GetQBField;

    for i := 0 to tvObjects.Items.Count - 1 do
      if IrwQBShowingField(tvObjects.Items[i].Data) = Fld then
      begin
        tvObjects.Items[i].Selected := True;
        break;
      end;
  end;


begin
  btnPlus.Down := False;
  btnMinus.Down := False;
  btnMul.Down := False;
  btnDiv.Down := False;
  btnAND.Down := False;
  btnOR.Down := False;
  btnNot.Down := False;
  btnGreat.Down := False;
  btnLess.Down := False;
  btnEqual.Down := False;
  btnNotEqual.Down := False;
  btnGreatEqual.Down := False;
  btnLessEqual.Down := False;

  tvObjects.Selected := nil;
  tvVariables.Selected := nil;
  cbConst.ItemIndex := -1;
  cbConst.OnChange(nil);
  edValue.Text := '';

  if Assigned(AItem) then
  begin
     case AItem.ItemType of
       fitOperation:
         begin
           case String(AItem.Value)[1] of
             '+':   btnPlus.Down := True;
             '-':   btnMinus.Down := True;
             '*':   btnMul.Down := True;
             '/':   btnDiv.Down := True;
            else
              if AItem.Value = 'AND' then
                btnAND.Down := True
              else if AItem.Value = 'OR' then
                btnOR.Down := True
              else if AItem.Value = 'NOT' then
                btnNOT.Down := True
              else if AItem.Value = '>' then
                btnGreat.Down := True
              else if AItem.Value = '<' then
                btnLess.Down := True
              else if AItem.Value = '=' then
                btnEqual.Down := True
              else if AItem.Value = '<>' then
                btnNotEqual.Down := True
              else if AItem.Value = '>=' then
                btnGreatEqual.Down := True
              else if AItem.Value = '<=' then
                btnLessEqual.Down := True;
           end;
         end;

       fitBracket:
         begin
           L := pnlExpr.FindPairBracket(pnlExpr.SelectedItem.ComponentIndex);
           if Assigned(L) and (L.ExprItem.ItemType = fitFunction) then
           begin
             AItem := L.ExprItem;
             SelectFunction;
           end;
         end;

       fitSeparator:
         begin
           L := pnlExpr.FindFunction(pnlExpr.SelectedItem.ComponentIndex);
           if Assigned(L) and (L.ExprItem.ItemType = fitFunction) then
           begin
             AItem := L.ExprItem;
             SelectFunction;
           end;
         end;

       fitFunction:
         SelectFunction;

       fitObject:
         SelectObject;

       fitField:
         SelectField;

       fitVariable:
         SelectVariable;

       fitConst:
         begin
          pcItem.ActivePage := tsValue;
          vt := VarType(AItem.Value);
          if vt = varBoolean then
          begin
            if AItem.Value then
              h := 'True'
            else
              h := 'False';
            cbConst.ItemIndex := cbConst.Items.IndexOf(h);
          end
          else
            cbConst.ItemIndex := cbConst.Items.IndexOfObject(Pointer(Ord(vt)));

          cbConst.OnChange(nil);

          if not (vt in [varBoolean, varNull]) then
            edValue.Text := VarToStr(AItem.Value);
         end;
     end;

     FInsertion := False;
   end
   else
     FInsertion := True;

   SetButtons;
end;

procedure TrmFMLExprEditor.SetButtons;

  procedure EnableButtons(AEnable: Boolean);
  begin
    btnPlus.Enabled := AEnable;
    btnMinus.Enabled := AEnable;
    btnMul.Enabled := AEnable;
    btnDiv.Enabled := AEnable;
    btnAND.Enabled := AEnable;
    btnOR.Enabled := AEnable;
    btnNOT.Enabled := AEnable;
    btnGreat.Enabled := AEnable;
    btnLess.Enabled := AEnable;
    btnEqual.Enabled := AEnable;
    btnNotEqual.Enabled := AEnable;
    btnGreatEqual.Enabled := AEnable;
    btnLessEqual.Enabled := AEnable;
  end;

begin
  if FInsertion or not Assigned(pnlExpr.SelectedItem.ExprItem) then
  begin
    btnDel.Enabled := False;
    btnBrackets.Enabled := True;
    EnableButtons(True);
  end

  else
  begin
    btnDel.Enabled := True;
    btnBrackets.Enabled := False;

    case pnlExpr.SelectedItem.ExprItem.ItemType of
      fitOperation: EnableButtons(True);

      fitBracket:   EnableButtons(False);

      fitSeparator: begin
                      btnDel.Enabled := False;
                      btnBrackets.Enabled := False;
                      EnableButtons(False);
                    end;
    else
      EnableButtons(False);
    end;
  end;

  btnIns.Enabled := FInsertion;
end;

procedure TrmFMLExprEditor.SetExpression(const Value: TrmFMLFormulaContent);
begin
  FExpression := Value;
  Initialize;
end;

procedure TrmFMLExprEditor.OnSelectItem(Sender: TObject);
begin
  if Assigned(pnlExpr.SelectedItem) then
    SyncSelection(pnlExpr.SelectedItem.ExprItem);
end;

procedure TrmFMLExprEditor.cbConstChange(Sender: TObject);
var
  vt: TVarType;
begin
  if cbConst.ItemIndex = -1 then
  begin
    pnlVal.Visible := False;
    Exit;
  end;

  vt := TVarType(Integer(cbConst.Items.Objects[cbConst.ItemIndex]));
  pnlVal.Visible := not (vt in [varNull, varBoolean]);

  if vt in [varNull, varBoolean] then
  begin
    if not FInsertion then
      btnIns.Click;
  end;
end;

procedure TrmFMLExprEditor.tvObjectsChange(Sender: TObject; Node: TTreeNode);
begin
   if not Assigned(tvObjects.Selected) then
     Exit;

   tvObjects.Selected.MakeVisible;
   AssignChValues(tsObject);

   if not FInsertion then
     btnIns.Click;
end;

procedure TrmFMLExprEditor.AssignChValues(APage: TTabSheet);
var
  O: IrmDesignObject;
  vfHolder: IrmVarFunctHolder;
begin
  if APage = tsObject then
  begin
    if not Assigned(tvObjects.Selected) then
      Exit;

    if tvObjects.Selected.ImageIndex = cCustomObjectImageIndex then
    begin
      FCurrentItemType := fitObject;
      O := IrmDesignObject(tvObjects.Selected.Data);
      if not O.PropertyExists('Value') then  // so far
        Exit;
      if AnsiSameText(tvObjects.Selected.Text, cParserSelf) then
        FCurrentItemValue := cParserSelf
      else
        FCurrentItemValue := O.GetPathFromRootOwner;
      FCurrentItemAddInfo := cbObjectProperty.Text;
    end

    else
    begin
      FCurrentItemType := fitField;
      O := IrmDesignObject(tvObjects.Selected.Parent.Data);
      FCurrentItemValue := O.GetPathFromRootOwner + '.' + IrwQBShowingField(tvObjects.Selected.Data).GetID;
      FCurrentItemAddInfo := '';
    end;
  end

  else if APage = tsVariable then
  begin
    if Assigned(tvVariables.Selected) and (Integer(tvVariables.Selected.Data) <> -1) then
    begin
      vfHolder := RMDesigner.GetComponentDesigner.DesigningObject.GetVarFunctHolder;
      FCurrentItemValue := (vfHolder.GetVariables.GetItemByIndex(Integer(tvVariables.Selected.Data)) as IrmVariable).GetVarInfo.Name;
      FCurrentItemAddInfo := '';
    end
  end

  else if APage = tsFunction then
    if Assigned(tvFuncts.Selected) and Assigned(tvFuncts.Selected.Data) then
    begin
      FCurrentItemValue := TrwFunction(tvFuncts.Selected.Data).Name;
      FCurrentItemAddInfo := '';      
    end;  
end;

procedure TrmFMLExprEditor.tsObjectShow(Sender: TObject);
begin
   FCurrentItemType := fitObject;
   AssignChValues(tsObject);
end;

procedure TrmFMLExprEditor.tsValueShow(Sender: TObject);
begin
   FCurrentItemType := fitConst;
   AssignChValues(tsValue);
end;

procedure TrmFMLExprEditor.tsFunctionShow(Sender: TObject);
begin
   FCurrentItemType := fitFunction;
   AssignChValues(tsFunction);
end;


procedure TrmFMLExprEditor.AddDrgDrpObject(const AObject: IrmDesignObject);
var
  i: Integer;
  O: IrmDesignObject;
  h: String;
begin
  for i := 0 to tvObjects.Items.Count - 1 do
  begin
    if tvObjects.Items[i].ImageIndex <> cCustomObjectImageIndex then
      Continue;

    O := IrmDesignObject(tvObjects.Items[i].Data);

    if O = AObject then
    begin
      if pnlExpr.CurDragOverItem = nil then
        pnlExpr.SelectedItem := TrmFMLExprItemLabel(pnlExpr.pnlCanvas.Components[pnlExpr.pnlCanvas.ComponentCount - 1])
      else
        pnlExpr.SelectedItem := pnlExpr.CurDragOverItem;

      h := O.GetPathFromRootOwner;
      if not Assigned(pnlExpr.SelectedItem.ExprItem) then
        AddItem(fitObject, h, 'Value')
      else if pnlExpr.SelectedItem.ExprItem.ItemType in [fitObject, fitConst, fitVariable] then
        UpdateItem(fitObject, h, 'Value');

      pnlExpr.CurDragOverItem := nil;

      break;
    end;
  end;
end;


procedure TrmFMLExprEditor.edValueExit(Sender: TObject);
begin
  if not FInsertion and pnlVal.Visible then
    btnIns.Click;
end;

procedure TrmFMLExprEditor.edValueKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    btnIns.Click;
end;

procedure TrmFMLExprEditor.CheckErrors;
var
  n: Integer;
  Err: String;
begin
  Err := FExpression.CheckErrors(n);
  if n <> -1 then
  begin
    pnlExpr.SelectedItem := pnlExpr.ItemByData(FExpression[n]);
    raise ErwException.Create(Err);
  end;
end;

procedure TrmFMLExprEditor.Clear;
begin
  FExpression.Clear;
  pnlExpr.Redraw;
  pnlExpr.pnlCanvasClick(nil);
end;

procedure TrmFMLExprEditor.tvFunctsChange(Sender: TObject; Node: TTreeNode);
begin
  AssignChValues(tsFunction);

  if Assigned(tvFuncts.Selected) and Assigned(tvFuncts.Selected.Data) then
  begin
   lFuncDecl.Caption := TrwFunction(tvFuncts.Selected.Data).GetFunctionHeader;
   lFunctDescr.Caption := TrwFunction(tvFuncts.Selected.Data).Description;

   tvFuncts.Selected.MakeVisible;
   if not FInsertion then
     btnIns.Click;
  end
  else
  begin
    lFuncDecl.Caption := '';
    lFunctDescr.Caption := '';
  end;
end;

procedure TrmFMLExprEditor.tvFunctsDblClick(Sender: TObject);
begin
  if Assigned(tvFuncts.Selected) and Assigned(tvFuncts.Selected.Data) then
    btnIns.Click;
end;

procedure TrmFMLExprEditor.pcItemChanging(Sender: TObject; var AllowChange: Boolean);
begin
  FCurrentItemType := fitNone;
  FCurrentItemValue := '';
end;

procedure TrmFMLExprEditor.btnGreatClick(Sender: TObject);
begin
  AddItem(fitOperation, '>', '');
end;

procedure TrmFMLExprEditor.btnLessClick(Sender: TObject);
begin
  AddItem(fitOperation, '<', '');
end;

procedure TrmFMLExprEditor.btnEqualClick(Sender: TObject);
begin
  AddItem(fitOperation, '=', '');
end;

procedure TrmFMLExprEditor.btnNotEqualClick(Sender: TObject);
begin
  AddItem(fitOperation, '<>', '');
end;

procedure TrmFMLExprEditor.btnGreatEqualClick(Sender: TObject);
begin
  AddItem(fitOperation, '>=', '');
end;

procedure TrmFMLExprEditor.btnLessEqualClick(Sender: TObject);
begin
  AddItem(fitOperation, '<=', '');
end;

procedure TrmFMLExprEditor.btnNotClick(Sender: TObject);
begin
  AddItem(fitOperation, 'NOT', '');
end;

procedure TrmFMLExprEditor.tvVariablesChange(Sender: TObject; Node: TTreeNode);
begin
   if not Assigned(tvVariables.Selected) then
     Exit;
   tvVariables.Selected.MakeVisible;
   AssignChValues(tsVariable);

   if not FInsertion then
     btnIns.Click;
end;

procedure TrmFMLExprEditor.tsVariableShow(Sender: TObject);
begin
   FCurrentItemType := fitVariable;
   AssignChValues(tsVariable);
end;

end.
