unit rmOPECurrencyFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPENumericFrm, StdCtrls, Mask, wwdbedit, Wwdbspin,
  ISBasicClasses, rwDesignClasses;

type
  TrmOPECurrency = class(TrmOPENumeric)
  public
    function  NewPropertyValue: Variant; override;
  end;

implementation

{$R *.dfm}

function TrmOPECurrency.NewPropertyValue: Variant;
begin
  Result := VarAsType(Round(VarAsType(edNumeric.Value, varCurrency)*100)/100, varCurrency);
end;

end.
