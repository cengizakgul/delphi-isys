unit rmCustomStatusBarFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmCustomFrameFrm, ComCtrls;

type
  TrmCustomStatusBar = class(TrmCustomFrame)
    sbStatus: TStatusBar;
  private
  public
    procedure SetStatusPanelText(const AIndex: Integer; const AText: String);
  end;

  TrmCustomStatusBarCalss = class of TrmCustomStatusBar;

implementation

{$R *.dfm}

{ TrmCustomStatusBar }

procedure TrmCustomStatusBar.SetStatusPanelText(const AIndex: Integer; const AText: String);
begin
  sbStatus.Panels[AIndex].Text := AText;
end;

end.
