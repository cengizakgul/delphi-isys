inherited rmTrmASCIIRecordProp: TrmTrmASCIIRecordProp
  Width = 365
  Height = 362
  inherited pcCategories: TPageControl
    Width = 365
    Height = 362
    inherited tsBasic: TTabSheet
      inline frmQuery: TrmOPEQuery
        Left = 146
        Top = 90
        Width = 90
        Height = 23
        AutoScroll = False
        AutoSize = True
        TabOrder = 2
        inherited btnQuery: TButton
          OnClick = frmQuerybtnQueryClick
        end
      end
      inline frmMasterFields: TrmOPEMasterFields
        Left = 8
        Top = 140
        Width = 340
        Height = 178
        AutoScroll = False
        TabOrder = 3
      end
      object chbQueryDriven: TCheckBox
        Left = 8
        Top = 93
        Width = 127
        Height = 17
        Caption = 'Query Driven Record'
        TabOrder = 4
        OnClick = chbQueryDrivenClick
      end
    end
    inherited tcAppearance: TTabSheet
      Caption = 'Processing'
      TabVisible = True
      inline frmBlockParentIfEmpty: TrmOPEBoolean
        Left = 8
        Top = 10
        Width = 329
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
        inherited chbProp: TCheckBox
          Width = 329
          Caption = 'Do not produce parent record if no record produced (Inner Join)'
        end
      end
      object pnlBlocking: TPanel
        Left = 8
        Top = 45
        Width = 340
        Height = 154
        BevelOuter = bvNone
        TabOrder = 1
        inline frmProduceBranchIf: TrmOPEBlockingControl
          Left = 0
          Top = 0
          Width = 340
          Height = 71
          AutoScroll = False
          TabOrder = 0
          inherited lCaption: TLabel
            Width = 340
            Caption = 'Produce Whole Branch If'
          end
          inherited frmExpression: TrmFMLExprPanel
            Width = 340
            Height = 55
            inherited pnlCanvas: TPanel
              Width = 340
              Height = 55
            end
          end
        end
        inline frmProduceRecordIf: TrmOPEBlockingControl
          Left = 0
          Top = 82
          Width = 340
          Height = 71
          AutoScroll = False
          TabOrder = 1
          inherited lCaption: TLabel
            Width = 340
            Caption = 'Produce Record If'
          end
          inherited frmExpression: TrmFMLExprPanel
            Width = 340
            Height = 55
            inherited pnlCanvas: TPanel
              Width = 340
              Height = 55
            end
          end
        end
      end
    end
  end
end
