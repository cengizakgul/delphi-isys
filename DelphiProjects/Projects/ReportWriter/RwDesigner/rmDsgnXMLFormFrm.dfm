inherited rmDsgnXMLForm: TrmDsgnXMLForm
  object pmTrmXMLForm: TPopupMenu
    AutoPopup = False
    Left = 48
    Top = 90
    object miLoadXMLSchema: TMenuItem
      Caption = 'Load XML Schema'
      OnClick = miLoadXMLSchemaClick
    end
  end
  object odlgXMLSchema: TOpenDialog
    Filter = 'XSD Schema files (*.xsd)|*.xsd|All Files (*.*)|*.*'
    Title = 'Open XML Schema'
    Left = 80
    Top = 96
  end
end
