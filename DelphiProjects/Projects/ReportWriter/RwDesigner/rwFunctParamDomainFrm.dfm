object rwFunctParamDomain: TrwFunctParamDomain
  Left = 0
  Top = 0
  Width = 659
  Height = 265
  AutoScroll = False
  TabOrder = 0
  object Label1: TLabel
    Left = 228
    Top = 0
    Width = 3
    Height = 265
    Align = alLeft
    AutoSize = False
  end
  object lvParams: TListView
    Left = 0
    Top = 0
    Width = 228
    Height = 265
    Align = alLeft
    Columns = <
      item
        Caption = 'Parameter'
        Width = 100
      end
      item
        AutoSize = True
        Caption = 'Description'
      end>
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnChange = lvParamsChange
  end
  object Panel1: TPanel
    Left = 231
    Top = 0
    Width = 428
    Height = 265
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object vleValues: TValueListEditor
      Left = 0
      Top = 0
      Width = 428
      Height = 196
      Align = alClient
      KeyOptions = [keyEdit, keyAdd, keyDelete, keyUnique]
      TabOrder = 0
      TitleCaptions.Strings = (
        'Description'
        'Value')
      OnStringsChange = vleValuesStringsChange
      ColWidths = (
        223
        199)
    end
    object Panel2: TPanel
      Left = 0
      Top = 196
      Width = 428
      Height = 69
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 10
        Top = 12
        Width = 64
        Height = 13
        Caption = 'Default Value'
      end
      object Label3: TLabel
        Left = 10
        Top = 42
        Width = 53
        Height = 13
        Caption = 'Description'
      end
      object edDefaultValue: TEdit
        Left = 82
        Top = 9
        Width = 201
        Height = 21
        TabOrder = 0
        OnChange = vleValuesStringsChange
      end
      object edDescription: TEdit
        Left = 82
        Top = 39
        Width = 201
        Height = 21
        TabOrder = 1
        OnChange = vleValuesStringsChange
      end
    end
  end
end
