unit rmDsgnXMLFormFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDsgnAreaFrm, Menus, rmLibCompChooseFrm, rwUtils, rwDsgnUtils, rwRTTI,
  rmTypes, rmDsgnTypes;

type
  TrmDsgnXMLForm = class(TrmDsgnArea)
    pmTrmXMLForm: TPopupMenu;
    miLoadXMLSchema: TMenuItem;
    odlgXMLSchema: TOpenDialog;
    procedure miLoadXMLSchemaClick(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    function CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject; override;
  end;

implementation

{$R *.dfm}

{ TrmDsgnArea1 }

constructor TrmDsgnXMLForm.Create(AOwner: TComponent);
begin
  inherited;
  FImageIndex := 6;
  FAreaType := rmDATXMLForm;
end;

function TrmDsgnXMLForm.CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject;
var
  LibComp: IrmComponent;
begin
  if SameText(AClassName, 'LIBRARY') then
  begin
    LibComp := ChoiceLibComp(['TrmXMLItem', 'TrwNoVisualComponent'], AOwner, Self);
    if Assigned(LibComp) then
      AClassName := LibComp.GetName
    else
      Exit;
  end;

  if not Assigned(AOwner) then
    AOwner := DesignObject;

  Result := AOwner.CreateChildObject(AClassName);
end;

procedure TrmDsgnXMLForm.miLoadXMLSchemaClick(Sender: TObject);
begin
  if odlgXMLSchema.Execute then
  begin
    (DesignObject as IrmXMLFormObject).LoadXSDSchema(odlgXMLSchema.FileName);
    SendApartmentMessage(Self, mRefreshComponents,  0, 0);
    ShowMessage('XML Schema has been loaded successfully');
  end;
end;

end.
