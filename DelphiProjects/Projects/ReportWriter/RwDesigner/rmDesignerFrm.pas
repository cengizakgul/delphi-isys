unit rmDesignerFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rwTypes, ImgList, ComCtrls, ExtCtrls, rmReport, rwReport,
  StdCtrls, rwBasicClasses, rwCommonClasses, Contnrs, ActnList,
  rmDsgnTypes, rwEngine, rwEngineTypes, Menus, rwDesigner,
  rmTypes, rmCommonClasses, rmDsgnReportFrm, rwDebugInfo,
  rwGraphics, rmDsgnApartmentFrm, rmCustomFrameFrm, rmLocalToolBarFrm,
  rmTBDesignerFrm, rmDsgnLibraryFrm, rmDsignerEventManager, EvStreamUtils;

type
  TrmModalResultInfo = record
    Data: IEvDualStream;
    ReportName: String;
    Notes: String;
    ClassName: String;
    AncestorName: String;
  end;


  TrmDesigner = class(TForm, IrmDesignerGUI, IrmMessageReceiver)
    opdOpen: TOpenDialog;
    ToolBar: TrmTBDesigner;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure miExitClick(Sender: TObject);

  private
    FApartmentList: TList;
    FCurrentApartmentIndex: Integer;
    FModalResultInfo: TrmModalResultInfo;
    FModalDlgStack: TStack;
    FEventManager: IrmDesignerEventManager;
    FModalDialogResult: TModalResult;
    FPrevOnAppIdle: TIdleEvent;

    function  CurrentApartment: IrmDsgnApartment;
    procedure NewReport;
    procedure OpenReport;
    procedure OpenSystemLibrary;
    procedure WMProcessPostedMessages(var Message: TMessage); message WM_DESIGNER_PROCESS_POSTED_MESSAGES;
    procedure UpdateActionLists;
    procedure UpdateCaption;
    procedure AfterCreateApartment(Apt: TrmDsgnApartment);
    procedure BeforeDestroyApartment(Apt: TrmDsgnApartment);
    function  WaitForModalDialogResult: TModalResult;
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);

    procedure DMNewReportCreating(var Message: TMessage); message mNewReportCreating;
    procedure DMApartmentCreated(var Message: TrmMessageRec); message mApartmentCreated;
    procedure DMApartmentDestroyed(var Message: TrmMessageRec); message mApartmentDestroyed;
    procedure DMTBClose(var Message: TrmMessageRec); message mtbClose;
    procedure DMTBOpen(var Message: TrmMessageRec); message mtbOpen;
    procedure DMTBNew(var Message: TrmMessageRec); message mtbNew;
    procedure DMTBOpenLibrary(var Message: TrmMessageRec); message mtbOpenLibrary;
    procedure DMModalDialogCancel(var Message: TrmMessageRec); message mModalDialogCancel;
    procedure DMModalDialogOK(var Message: TrmMessageRec); message mModalDialogOK;

    // Interface implementation
    function  VCLObject: TObject;
    function  GetComponentDesigner: IrmDsgnComponent;
    function  GetActiveApartment: IrmDsgnApartment;
    function  CreateApartment(ApartmentClass: TComponentClass): IrmDsgnApartment;
    procedure ShowApartment(Apartment: IrmDsgnApartment);
    function  ShowModalApartment(Apartment: IrmDsgnApartment): TModalResult;
    procedure DestroyCurrentApartment;
    function  GetApartmentList: TrmdApartmentList;
    function  EventManager: IrmDesignerEventManager;
    function  Apartment: IrmDsgnApartment;
    procedure GetListeningMessages(const AMessages: TList);

  protected
    procedure CreateWindowHandle(const Params: TCreateParams); override;
    procedure DestroyWindowHandle; override;

  public
    constructor Create(AOwner: TComponent); override;
  end;


  TrwDesignerFormHolder = class(TInterfacedObject, IrwDesignerForm)
  private
    FReportDesigner: TrmDesigner;
  protected
    function  IsClosed: Boolean;
    function  GetData: IEvDualStream;
  public
    constructor Create(const AReportRes: IEvDualStream; const AReportName: String);
    destructor  Destroy; override;
  end;


  procedure ShowModalRMDesignerWithReport(const AReportRes: IEvDualStream; const AReportName: String);
  function  ShowRMDesigner(const AReportRes: IEvDualStream; const AReportName: String): IrwDesignerForm;


implementation


uses rwDsgnUtils, rwDsgnRes;

{$R *.dfm}


procedure ShowModalRMDesignerWithReport(const AReportRes: IEvDualStream; const AReportName: String);
var
  ReportDesigner: TrmDesigner;
begin
  Application.CreateForm(TrmDesigner, ReportDesigner);

  with ReportDesigner do
  begin
    try
      if Assigned(AReportRes) then
        AReportRes.Position := 0;

      FModalResultInfo.Data := AReportRes;
      FModalResultInfo.ReportName := AReportName;

      ShowModal;
    finally
      FreeAndNil(ReportDesigner);
    end;
  end;
end;


function ShowRMDesigner(const AReportRes: IEvDualStream; const AReportName: String): IrwDesignerForm;
begin
  Result := TrwDesignerFormHolder.Create(AReportRes, AReportName);
end;


{ TrmDesigner }

constructor TrmDesigner.Create(AOwner: TComponent);
begin
  RMDesigner := Self;
  FEventManager := TrmDsignerEventManager.Create;
  FEventManager.RegisterObject(Self);
  inherited;
end;

procedure TrmDesigner.FormCreate(Sender: TObject);
begin
  FPrevOnAppIdle := Application.OnIdle;
  Application.OnIdle := OnAppIdle;
  RWDesigning := True;
  FApartmentList := TList.Create;
  FModalDlgStack := TStack.Create;
  FCurrentApartmentIndex := -1;
end;

function TrmDesigner.CreateApartment(ApartmentClass: TComponentClass): IrmDsgnApartment;
begin
  Result := ApartmentClass.Create(Self) as TrmDsgnApartment;
end;

function TrmDesigner.ShowModalApartment(Apartment: IrmDsgnApartment): TModalResult;
begin
  FModalDlgStack.Push(Pointer(CurrentApartment));
  ShowApartment(Apartment);
  Result := WaitForModalDialogResult;
end;

procedure TrmDesigner.NewReport;
begin
  ShowApartment(CreateApartment(TrmDsgnReport));
  GetComponentDesigner.NewDesignComponent('TrmReport');
  UpdateCaption;
end;


procedure TrmDesigner.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i: Integer;
  O: TObject;
begin
  if Assigned(FModalResultInfo.Data) and (GetComponentDesigner <> nil) then
  begin
    FModalResultInfo.Notes := GetComponentDesigner.DesigningObject.GetPropertyValue('Description');
    FModalResultInfo.AncestorName := GetComponentDesigner.DesigningObject.GetPropertyValue('_LibComponent');
    FModalResultInfo.ClassName := GetComponentDesigner.DesigningObject.GetPropertyValue('_RwClassName');
    FModalResultInfo.Data.Size := 0;
    GetComponentDesigner.SaveComponentToStream(FModalResultInfo.Data.RealStream);
    FModalResultInfo.Data.Position := 0;
  end;

  ShowApartment(nil);

  RMDesigner := nil;

  for i := 0 to FApartmentList.Count - 1 do
  begin
    O := IrmDsgnApartment(FApartmentList[i]).RealObject;
    O.Free;
  end;
end;

procedure TrmDesigner.DMNewReportCreating(var Message: TMessage);
begin
  NewReport;
end;

procedure TrmDesigner.FormShow(Sender: TObject);
begin
  if Assigned(FModalResultInfo.Data) and (FModalResultInfo.Data.Size > 0) then
    try
      ShowApartment(CreateApartment(TrmDsgnReport));
      (GetComponentDesigner as IrmDsgnReport).DesignReport(FModalResultInfo.Data.RealStream);
      if FModalResultInfo.ReportName <> '' then
        (GetComponentDesigner as IrmDsgnReport).SetReportFileName(FModalResultInfo.ReportName);
      Self.WindowState := wsNormal;
      Self.WindowState := wsMaximized;
    except
      on E: Exception do
        ShowMessage(E.Message);
    end;

  if CurrentApartment = nil then
    PostDesignerMessage(Self, mNewReportCreating, 0, 0);
end;

function TrmDesigner.VCLObject: TObject;
begin
  Result := Self;
end;

procedure TrmDesigner.FormDestroy(Sender: TObject);
begin
  DsgnRes.Free;
  FreeAndNil(FModalDlgStack);
  FreeAndNil(FApartmentList);
  RWDesigning := False;
  Application.OnIdle := FPrevOnAppIdle;
end;

function TrmDesigner.GetComponentDesigner: IrmDsgnComponent;
begin
  if (CurrentApartment <> nil) and Supports(CurrentApartment, IrmDsgnComponent) then
    Result := CurrentApartment as IrmDsgnComponent
  else
    Result := nil;
end;

procedure TrmDesigner.miExitClick(Sender: TObject);
begin
  Close;
end;

procedure TrmDesigner.UpdateActionLists;
var
  i: Integer;

  procedure UpdateActionListFor(AFrame: TFrame; AOperation: TOperation);
  var
    i, j: Integer;
  begin
    for i := 0 to AFrame.ComponentCount - 1 do
      if AFrame.Components[i] is TCustomActionList then
      begin
        j := FActionLists.IndexOf(AFrame.Components[i]);
        case AOperation of
          opInsert:
            begin
              if j = -1 then
                FActionLists.Add(AFrame.Components[i]);
            end;

          opRemove:
            begin
              if j <> -1 then
                FActionLists.Delete(j);
            end;
        end;
      end

      else if AFrame.Components[i] is TFrame then
        UpdateActionListFor(TFrame(AFrame.Components[i]), AOperation);
  end;

begin
  // Here I'm saying "THANK YOU VERY MUCH!" to Borland's programmer who implemented ActionLists for frames.
  // This is a good example how you NOT supposed to program ever !!! (See TForm.FActionLists variable)
  // However thanks God he(she?) did it as a protected variable. So, here is a workaround:

 if FActionLists = nil then FActionLists := TList.Create;  // to be consistent with VCL implementation. :)

 FActionLists.Clear;

  for i := 0 to ComponentCount - 1 do
    if Components[i] is TFrame then
    begin
      if TFrame(Components[i]).Visible then
        UpdateActionListFor(TFrame(Components[i]), opInsert);
    end;
end;


procedure TrmDesigner.ShowApartment(Apartment: IrmDsgnApartment);
begin
  if Assigned(Apartment) and Apartment.IsActive then
    Exit;

  if (CurrentApartment <> nil) and CurrentApartment.IsActive then
  begin
    CurrentApartment.Deactivate;
    FCurrentApartmentIndex := -1;
  end;

  if Assigned(Apartment) then
  begin
    if ToolBar.Visible then
    begin
      ToolBar.Deinitialize;
      ToolBar.Hide;
    end;

    FCurrentApartmentIndex := FApartmentList.IndexOf(Pointer(Apartment));
    CurrentApartment.Activate;

    UpdateActionLists;

    UpdateCaption;
  end;

  if CurrentApartment = nil then
  begin
    ToolBar.Show;
    ToolBar.Initialize;
    UpdateActionLists;
    UpdateCaption;
  end;
end;

procedure TrmDesigner.DestroyCurrentApartment;
var
  i, j: Integer;
  NewActiveChildFrame: IrmDsgnApartment;
  L: TrmdApartmentList;
  AptObj: TObject;
begin
  if CurrentApartment = nil then
    Exit;

  AptObj := CurrentApartment.RealObject;

  if FModalDlgStack.Count > 0 then
    NewActiveChildFrame := IrmDsgnApartment(FModalDlgStack.Pop)

  else
  begin
    L := GetApartmentList;

    if Length(L) <= 1 then
      NewActiveChildFrame := nil

    else
    begin
      j := -1;
      for i := Low(L) to High(L) do
        if L[i].RealObject = AptObj then
        begin
          if i = Low(L) then
            j := i + 1
          else if i > Low(L) then
            j := i - 1;
        end;

      if j <> -1 then
        NewActiveChildFrame := L[j]
      else
        NewActiveChildFrame := nil;
    end;

    SetLength(L, 0);
  end;

  ShowApartment(NewActiveChildFrame);

  AptObj.Free;
end;

procedure TrmDesigner.OpenReport;
var
  FS: TEvFileStream;
begin
  if opdOpen.Execute then
  begin
    FS := TEvFileStream.Create(opdOpen.FileName, fmOpenRead);
    try
      try
         ShowApartment(CreateApartment(TrmDsgnReport));
        (GetComponentDesigner as IrmDsgnReport).DesignReport(FS);
      except
        on E: Exception do
        begin
          try
            DestroyCurrentApartment;
          except
          end;
          ShowMessage(E.Message);
        end;
      end;
    finally
      FreeAndNil(FS);
    end;
  end;
end;

function TrmDesigner.GetApartmentList: TrmdApartmentList;
var
  i: Integer;
begin
  SetLength(Result, FApartmentList.Count);

  for i := 0 to FApartmentList.Count - 1 do
    Result[i] := IrmDsgnApartment(FApartmentList[i]);
end;

procedure TrmDesigner.OpenSystemLibrary;
var
  L: TrmdApartmentList;
  i: Integer;
  Frm: IrmDsgnApartment;
begin
  L := GetApartmentList;

  Frm := nil;
  for i := Low(L) to High(L) do
    if L[i].RealObject is TrmDsgnLibrary then
    begin
      Frm := L[i];
      break;
    end;

  if Assigned(Frm) then
    ShowApartment(Frm)
  else
  begin
    ShowApartment(CreateApartment(TrmDsgnLibrary));
    GetComponentDesigner.NewDesignComponent('SystemLibrary');
  end;  
end;


function TrmDesigner.GetActiveApartment: IrmDsgnApartment;
begin
  Result := CurrentApartment;
end;

procedure TrmDesigner.UpdateCaption;
var
  h: String;
begin
  h := 'Report Master';
  if CurrentApartment <> nil then
    h := h +  ' - ' + CurrentApartment.GetCaption;
  Caption := h;
end;

function TrmDesigner.EventManager: IrmDesignerEventManager;
begin
  Result := FEventManager;
end;


function TrmDesigner.Apartment: IrmDsgnApartment;
begin
  Result := nil;
end;

procedure TrmDesigner.AfterCreateApartment(Apt: TrmDsgnApartment);
begin
  RegisterTalkativeDesignerMember(Apt);

  FApartmentList.Add(Pointer(Apt as IrmDsgnApartment));
  Apt.Align := alClient;
  Apt.Parent := Self;
end;

procedure TrmDesigner.BeforeDestroyApartment(Apt: TrmDsgnApartment);
var
  i: Integer;
begin
  UnRegisterTalkativeDesignerMember(Apt);
  i := FApartmentList.IndexOf(Pointer(Apt as IrmDsgnApartment));
  FApartmentList.Delete(i);
end;

function TrmDesigner.CurrentApartment: IrmDsgnApartment;
begin
  if (FCurrentApartmentIndex < 0) or (FApartmentList.Count = 0) then
  begin
    Result := nil;
    exit;
  end

  else if FCurrentApartmentIndex >= FApartmentList.Count then
    FCurrentApartmentIndex := FApartmentList.Count - 1;

  Result := IrmDsgnApartment(Pointer(FApartmentList[FCurrentApartmentIndex]));
end;

function TrmDesigner.WaitForModalDialogResult: TModalResult;
begin
  FModalDialogResult := mrNone;
  
  while FModalDialogResult = mrNone do
  begin
    WaitMessage;
    Application.ProcessMessages;
  end;

  Result := FModalDialogResult;
  FModalDialogResult := mrNone;
end;

procedure TrmDesigner.GetListeningMessages(const AMessages: TList);
begin
  AMessages.Add(Pointer(mNewReportCreating));
  AMessages.Add(Pointer(mApartmentCreated));
  AMessages.Add(Pointer(mApartmentDestroyed));
  AMessages.Add(Pointer(mtbClose));
  AMessages.Add(Pointer(mtbOpen));
  AMessages.Add(Pointer(mtbNew));
  AMessages.Add(Pointer(mtbOpenLibrary));
  AMessages.Add(Pointer(mModalDialogCancel));
  AMessages.Add(Pointer(mModalDialogOK));
end;

procedure TrmDesigner.DMApartmentCreated(var Message: TrmMessageRec);
begin
  AfterCreateApartment(TrmDsgnApartment(Pointer(Integer(Message.Parameter1))));
end;

procedure TrmDesigner.DMApartmentDestroyed(var Message: TrmMessageRec);
begin
  BeforeDestroyApartment(TrmDsgnApartment(Pointer(Integer(Message.Parameter1))));
end;

procedure TrmDesigner.DMModalDialogCancel(var Message: TrmMessageRec);
begin
  FModalDialogResult := mrCancel;
end;

procedure TrmDesigner.DMModalDialogOK(var Message: TrmMessageRec);
begin
  FModalDialogResult := mrOK;
end;

procedure TrmDesigner.DMTBClose(var Message: TrmMessageRec);
begin
  Close;
end;

procedure TrmDesigner.DMTBNew(var Message: TrmMessageRec);
begin
  NewReport;
end;

procedure TrmDesigner.DMTBOpen(var Message: TrmMessageRec);
begin
  OpenReport;
end;

procedure TrmDesigner.DMTBOpenLibrary(var Message: TrmMessageRec);
begin
  OpenSystemLibrary;
end;

procedure TrmDesigner.WMProcessPostedMessages(var Message: TMessage);
begin
  FEventManager.ProcessMessages;
end;

procedure TrmDesigner.CreateWindowHandle(const Params: TCreateParams);
begin
  inherited;
  FEventManager.SetOwnerWndHandle(Handle);
end;

procedure TrmDesigner.DestroyWindowHandle;
begin
  inherited;
  FEventManager.SetOwnerWndHandle(0);
end;

procedure TrmDesigner.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  if GetCurrentThreadId = MainThreadID then
    CheckSynchronize;
end;

{ TrwDesignerFormHolder }

constructor TrwDesignerFormHolder.Create(const AReportRes: IEvDualStream; const AReportName: String);
begin
  Application.CreateForm(TrmDesigner, FReportDesigner);

  with FReportDesigner do
  begin
    try
      if Assigned(AReportRes) then
          AReportRes.Position := 0;

      FModalResultInfo.Data := AReportRes;
      FModalResultInfo.ReportName := AReportName;

      Show;
    except
      FreeAndNil(FReportDesigner);
      raise;
    end;
  end;
end;


destructor TrwDesignerFormHolder.Destroy;
begin
  FreeAndNil(FReportDesigner);
  inherited;
end;

function TrwDesignerFormHolder.GetData: IEvDualStream;
begin
  if Assigned(FReportDesigner) then
    Result := FReportDesigner.FModalResultInfo.Data
  else
    Result := nil;
end;

function TrwDesignerFormHolder.IsClosed: Boolean;
begin
  Result := not (Assigned(FReportDesigner) and FReportDesigner.Visible);
end;

end.
