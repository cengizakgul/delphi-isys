inherited rwFieldsPropertyEditor: TrwFieldsPropertyEditor
  Left = 388
  Top = 202
  VertScrollBar.Range = 0
  BorderStyle = bsToolWindow
  Caption = 'Fields'
  ClientHeight = 357
  ClientWidth = 378
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOK: TButton
    Left = 297
    TabOrder = 1
  end
  inherited btnCancel: TButton
    Left = 297
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 2
    Width = 284
    Height = 348
    Caption = 'Fields'
    TabOrder = 0
    DesignSize = (
      284
      348)
    object lbFields: TListBox
      Left = 9
      Top = 18
      Width = 181
      Height = 143
      ItemHeight = 13
      TabOrder = 0
      OnClick = lbFieldsClick
    end
    object btnAdd: TButton
      Tag = 267
      Left = 199
      Top = 18
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Add'
      TabOrder = 1
      OnClick = btnAddClick
    end
    object btnDelete: TButton
      Tag = 267
      Left = 199
      Top = 47
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Delete'
      TabOrder = 2
      OnClick = btnDeleteClick
    end
    object btnUp: TButton
      Tag = 267
      Left = 199
      Top = 76
      Width = 36
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Up'
      TabOrder = 3
      OnClick = btnUpClick
    end
    object btnDown: TButton
      Tag = 267
      Left = 238
      Top = 76
      Width = 36
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Down'
      TabOrder = 4
      OnClick = btnDownClick
    end
    object gbField: TGroupBox
      Left = 9
      Top = 168
      Width = 266
      Height = 170
      Caption = 'Field Description'
      Enabled = False
      TabOrder = 5
      object Label1: TLabel
        Left = 10
        Top = 21
        Width = 28
        Height = 13
        Caption = 'Name'
      end
      object Label2: TLabel
        Left = 10
        Top = 48
        Width = 24
        Height = 13
        Caption = 'Type'
      end
      object Label3: TLabel
        Left = 10
        Top = 75
        Width = 20
        Height = 13
        Caption = 'Size'
      end
      object Label4: TLabel
        Left = 10
        Top = 143
        Width = 65
        Height = 13
        Caption = 'Display Width'
      end
      object Label5: TLabel
        Left = 10
        Top = 114
        Width = 63
        Height = 13
        Caption = 'Display Label'
      end
      object edtName: TEdit
        Left = 56
        Top = 18
        Width = 201
        Height = 21
        TabOrder = 0
        OnExit = edtNameExit
        OnKeyPress = edtNameKeyPress
      end
      object cmbType: TComboBox
        Left = 56
        Top = 48
        Width = 201
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        OnChange = cmbTypeChange
        Items.Strings = (
          'Blob'
          'Boolean'
          'Currency'
          'Date'
          'Float'
          'Integer'
          'String')
      end
      object chbKeyField: TCheckBox
        Left = 144
        Top = 77
        Width = 67
        Height = 18
        Caption = 'Key Field'
        TabOrder = 3
        OnClick = chbKeyFieldClick
      end
      object edtSize: TwwDBSpinEdit
        Left = 56
        Top = 75
        Width = 66
        Height = 21
        Increment = 1.000000000000000000
        MaxValue = 255.000000000000000000
        TabOrder = 2
        UnboundDataType = wwDefault
        OnExit = edtSizeExit
        OnKeyPress = edtNameKeyPress
      end
      object edtDisplayWidth: TwwDBSpinEdit
        Left = 94
        Top = 140
        Width = 66
        Height = 21
        Increment = 1.000000000000000000
        MaxValue = 255.000000000000000000
        TabOrder = 5
        UnboundDataType = wwDefault
        OnExit = edtDisplayWidthExit
        OnKeyPress = edtNameKeyPress
      end
      object edtDisplayLabel: TEdit
        Left = 94
        Top = 110
        Width = 163
        Height = 21
        TabOrder = 4
        OnExit = edtDisplayLabelExit
        OnKeyPress = edtNameKeyPress
      end
      object chbVisible: TCheckBox
        Left = 182
        Top = 142
        Width = 52
        Height = 18
        Caption = 'Visible'
        TabOrder = 6
        OnClick = chbVisibleClick
      end
    end
  end
end
