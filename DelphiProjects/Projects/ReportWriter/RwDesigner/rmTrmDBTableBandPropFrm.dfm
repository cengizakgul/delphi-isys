inherited rmTrmDBTableBandProp: TrmTrmDBTableBandProp
  inherited pcCategories: TPageControl
    inherited tcPrint: TTabSheet
      inherited frmPrintOnEachPage: TrmOPEBoolean
        Width = 256
        inherited chbProp: TCheckBox
          Width = 256
        end
      end
      inherited frmMultiPaged: TrmOPEBoolean
        Top = 313
        TabOrder = 5
      end
      inherited pnlBlocking: TPanel
        Top = 123
      end
      inline frmPrintIfNoData: TrmOPEBoolean
        Left = 8
        Top = 87
        Width = 127
        Height = 17
        AutoScroll = False
        AutoSize = True
        TabOrder = 3
        inherited chbProp: TCheckBox
          Width = 127
          Caption = 'Print if Query is empty'
        end
      end
    end
  end
end
