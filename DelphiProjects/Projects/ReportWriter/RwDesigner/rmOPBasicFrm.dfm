inherited rmOPBasic: TrmOPBasic
  Width = 330
  Height = 122
  object pcCategories: TPageControl
    Left = 0
    Top = 0
    Width = 330
    Height = 122
    ActivePage = tsBasic
    Align = alClient
    MultiLine = True
    TabOrder = 0
    object tsBasic: TTabSheet
      Caption = 'General'
      inline frmObjectName: TrmOPEStringSingleLine
        Left = 8
        Top = 10
        Width = 290
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 0
        inherited lPropName: TLabel
          Width = 62
          Caption = 'Object Name'
        end
      end
      inline frmObjectDescription: TrmOPEStringSingleLine
        Left = 8
        Top = 40
        Width = 290
        Height = 21
        AutoScroll = False
        AutoSize = True
        TabOrder = 1
        inherited lPropName: TLabel
          Width = 77
          Caption = 'Brief Description'
        end
      end
      object ShowMultyLineEditorBtn: TButton
        Left = 300
        Top = 40
        Width = 17
        Height = 23
        Caption = '...'
        TabOrder = 2
        OnClick = ShowMultyLineEditorBtnClick
      end
    end
    object tcAppearance: TTabSheet
      Caption = 'Appearance'
      ImageIndex = 2
      TabVisible = False
    end
    object tsAbout: TTabSheet
      Caption = 'About'
      ImageIndex = 2
      object imObject: TImage
        Left = 6
        Top = 6
        Width = 16
        Height = 16
        AutoSize = True
        Transparent = True
      end
      object mObjectDescription: TMemo
        Left = 48
        Top = 8
        Width = 250
        Height = 97
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        ParentColor = True
        ReadOnly = True
        TabOrder = 0
      end
    end
  end
end
