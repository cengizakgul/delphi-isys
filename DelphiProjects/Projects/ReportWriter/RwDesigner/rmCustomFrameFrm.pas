unit rmCustomFrameFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDsgnTypes, rwDsgnUtils, rmSettingsHolder;

const
  WM_HIDE_IN_IDLE = WM_USER + 200;
  WM_REFRESH_IN_IDLE = WM_USER + 201;

type
  TrmCustomFrame = class(TFrame, IrmCustomFrame, IrmMessageReceiver)
  private
    FRefreshMessagePosted: Boolean;

    procedure  WMHideInIdle(var Message: TMessage); message WM_HIDE_IN_IDLE;
    procedure  WMRefresh(var Message: TrmMessageRec); message WM_REFRESH_IN_IDLE;

    procedure  DMApartmentActivated(var Message: TrmMessageRec); message  mApartmentActivated;
    procedure  DMApartmentDeactivated(var Message: TrmMessageRec); message  mApartmentDeactivated;

  protected
    procedure OnActivateApartment; virtual;
    procedure OnDeactivateApartment; virtual;
    procedure StoreState; virtual;
    procedure RestoreState; virtual;

  //interface
  protected
    function  VCLObject: TObject;
    function  Apartment: IrmDsgnApartment;
    procedure GetListeningMessages(const AMessages: TList); virtual;
    function  RealObject: TFrame;
    procedure DoRefresh; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   HideInIdle;
    procedure   RefreshInIdle;
  end;

implementation

{$R *.dfm}


{ TrmCustomFrame }

constructor TrmCustomFrame.Create(AOwner: TComponent);
begin
  inherited;
  RegisterTalkativeApartmentMember(Self);
end;

destructor TrmCustomFrame.Destroy;
begin
  UnRegisterTalkativeApartmentMember(Self);
  inherited;
end;

function TrmCustomFrame.RealObject: TFrame;
begin
  Result := Self;
end;

procedure TrmCustomFrame.HideInIdle;
begin
  if HandleAllocated then
    PostMessage(Self.Handle, WM_HIDE_IN_IDLE, 0, 0)
  else
    Hide;
end;

procedure TrmCustomFrame.WMHideInIdle(var Message: TMessage);
begin
  Hide;
end;

function TrmCustomFrame.VCLObject: TObject;
begin
  Result := Self;
end;

function TrmCustomFrame.Apartment: IrmDsgnApartment;
begin
  Result := GetComponentAppartment(Self);
end;

procedure TrmCustomFrame.GetListeningMessages(const AMessages: TList);
begin
  AMessages.Add(Pointer(mApartmentActivated));
  AMessages.Add(Pointer(mApartmentDeactivated));  
end;

procedure TrmCustomFrame.DoRefresh;
begin
end;

procedure TrmCustomFrame.WMRefresh(var Message: TrmMessageRec);
begin
  FRefreshMessagePosted := False;
  if IsEventQueueEmpty(Self) then
    DoRefresh
  else
    RefreshInIdle;
end;

procedure TrmCustomFrame.RefreshInIdle;
begin
  if HandleAllocated then
  begin
    if not FRefreshMessagePosted then
    begin
      PostMessage(Self.Handle, WM_REFRESH_IN_IDLE, 0, 0);
      FRefreshMessagePosted := True;
    end;
  end

  else
    DoRefresh;
end;

procedure TrmCustomFrame.RestoreState;
begin
end;

procedure TrmCustomFrame.StoreState;
begin
end;

procedure TrmCustomFrame.DMApartmentActivated(var Message: TrmMessageRec);
begin
  OnActivateApartment;
end;

procedure TrmCustomFrame.DMApartmentDeactivated(var Message: TrmMessageRec);
begin
  OnDeactivateApartment;
end;

procedure TrmCustomFrame.OnActivateApartment;
begin
  RestoreState;
end;

procedure TrmCustomFrame.OnDeactivateApartment;
begin
  StoreState;
end;

end.
