unit rmTrmXMLSimpleTypeByRestrictionPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, StdCtrls, ExtCtrls, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls, rmOPEValueFrm, rmOPETextFilterFrm;

type
  TrmTrmXMLSimpleTypeByRestrictionProp = class(TrmOPBasic)
    grbValue: TGroupBox;
    frmValue: TrmOPEValue;
    frmTextFilter: TrmOPETextFilter;
  private
  public
    procedure GetPropsFromObject; override;
  end;

implementation

{$R *.dfm}

{ TrmXMLSimpleTypeByRestrictionProp }

procedure TrmTrmXMLSimpleTypeByRestrictionProp.GetPropsFromObject;
begin
  inherited;
  frmValue.PropertyName := 'Value';
  frmValue.ShowPropValue;
end;

end.
 