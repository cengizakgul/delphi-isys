unit rmDsgnApartmentFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmCustomFrameFrm, rmLocalToolBarFrm, rmDsgnTypes, rwDsgnUtils, rmDsignerEventManager,
  ExtCtrls, ToolWin, ComCtrls, rmCustomStatusBarFrm, EvStreamUtils, rwBasicUtils, isRegistryFunctions,
  rmSettingsHolder, rwUtils;

type
  TrmDsgnApartment = class(TrmCustomFrame, IrmDsgnApartment)
    pnlDockBottom: TPanel;
    pnlDockLeft: TPanel;
    pnlDockRight: TPanel;
    spltLeft: TSplitter;
    spltRight: TSplitter;
    spltBottom: TSplitter;
    procedure pnlDockBottomDockOver(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure pnlDockLeftDockOver(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure pnlDockRightDockOver(Sender: TObject;
      Source: TDragDockObject; X, Y: Integer; State: TDragState;
      var Accept: Boolean);
    procedure pnlDockBottomDockDrop(Sender: TObject;
      Source: TDragDockObject; X, Y: Integer);
    procedure pnlDockBottomUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure spltBottomCanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure spltBottomMoved(Sender: TObject);
    procedure spltLeftCanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure spltLeftMoved(Sender: TObject);
    procedure spltRightCanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure spltRightMoved(Sender: TObject);
  private
    FEventManager: IrmDesignerEventManager;
    FToolBar: TrmLocalToolBar;
    FStatusBar: TrmCustomStatusBar;
    FSettings: IrmSettingsHolder;

    procedure WMProcessPostedMessages(var Message: TMessage); message WM_DESIGNER_PROCESS_POSTED_MESSAGES;

    procedure DMDockingStateChanged(var Message: TrmMessageRec); message  mDockingStateChanged;

    procedure StoreDockableParts;
    procedure RestoreDockableParts;

  protected
    procedure CreateWindowHandle(const Params: TCreateParams); override;
    procedure DestroyWindowHandle; override;

    function  GetToolbarClass: TrmLocalToolBarCalss; virtual;
    function  GetStatusBarClass: TrmCustomStatusBarCalss; virtual;

    procedure StoreState; override;
    procedure RestoreState; override;

    //Interface
    function  EventManager: IrmDesignerEventManager;
    function  GetCaption: String; virtual;
    procedure Activate; virtual;
    procedure Deactivate; virtual;
    function  IsActive: Boolean;
    function  Settings: IrmSettingsHolder;
    procedure GetListeningMessages(const AMessages: TList); override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;

    property  ToolBar: TrmLocalToolBar read FToolBar;
    property  StatusBar: TrmCustomStatusBar read FStatusBar;
  end;

  TrmDsgnApartmentClass = class of TrmDsgnApartment;

implementation

{$R *.dfm}

uses rmDockableFrameFrm;


{ TrmDsgnCustomFrame }

procedure TrmDsgnApartment.Activate;
begin
  if Assigned(ToolBar) then
    ToolBar.Initialize;
  BringToFront;
  Visible := True;

  SendApartmentMessage(Self, mApartmentActivated, 0, 0);
end;

procedure TrmDsgnApartment.AfterConstruction;
begin
  inherited;

  if GetToolbarClass <> nil then
  begin
    FToolBar := GetToolbarClass.Create(Self);
    FToolBar.Parent := Self;
    FToolBar.Color := clBtnFace;
  end;

  if GetStatusBarClass <> nil then
  begin
    FStatusBar := GetStatusBarClass.Create(Self);
    FStatusBar.Parent := Self;
  end;

  SendDesignerMessage(Self, mApartmentCreated, Integer(Pointer(Self)), 0);
end;

procedure TrmDsgnApartment.BeforeDestruction;
begin
  SendApartmentMessage(Self, mApartmentDestroying, Integer(Pointer(Self)), 0);
  SendDesignerMessage(Self, mApartmentDestroyed, Integer(Pointer(Self)), 0);
  inherited;
  FreeAndNil(FStatusBar);
  FreeAndNil(FToolBar);
end;

constructor TrmDsgnApartment.Create(AOwner: TComponent);
begin
  FEventManager := TrmDsignerEventManager.Create;
  FSettings := TrmSettingsINIHolder.Create(ExtractFilePath(Application.ExeName) + 'isRWDesignerSettings.ini');
  inherited;
  Visible := False;
end;

procedure TrmDsgnApartment.CreateWindowHandle(const Params: TCreateParams);
begin
  inherited;
  FEventManager.SetOwnerWndHandle(Handle);
end;

procedure TrmDsgnApartment.Deactivate;
begin
  Visible := False;

  SendApartmentMessage(Self, mApartmentDeactivated, 0, 0);
    
  if Assigned(ToolBar) then
    ToolBar.Deinitialize;
end;

procedure TrmDsgnApartment.DestroyWindowHandle;
begin
  inherited;
  FEventManager.SetOwnerWndHandle(0);
end;

function TrmDsgnApartment.EventManager: IrmDesignerEventManager;
begin
  Result := FEventManager;
end;

function TrmDsgnApartment.GetCaption: String;
begin
  Result := Name;
end;

function TrmDsgnApartment.GetToolbarClass: TrmLocalToolBarCalss;
begin
  Result := nil;
end;

function TrmDsgnApartment.IsActive: Boolean;
begin
  Result := Visible;
end;

procedure TrmDsgnApartment.WMProcessPostedMessages(var Message: TMessage);
begin
  FEventManager.ProcessMessages;
end;

procedure TrmDsgnApartment.pnlDockBottomDockOver(Sender: TObject; Source: TDragDockObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
  R: TRect;
begin
  if pnlDockBottom.Height = 0 then
  begin
    R := Source.DockRect;
    Dec(R.Top, 100);
    Source.DockRect := R;
  end;  
end;

procedure TrmDsgnApartment.pnlDockLeftDockOver(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
var
  R: TRect;
begin
  if pnlDockLeft.Width = 0 then
  begin
    R := Source.DockRect;
    Inc(R.Right, 100);
    Source.DockRect := R;
  end;
end;

procedure TrmDsgnApartment.pnlDockRightDockOver(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
var
  R: TRect;
begin
  if pnlDockRight.Width = 0 then
  begin
    R := Source.DockRect;
    Dec(R.Left, 100);
    Source.DockRect := R;
  end;
end;

procedure TrmDsgnApartment.pnlDockBottomDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
begin
  PostApartmentMessage(Self, mDockingStateChanged, 0, 0);
end;

procedure TrmDsgnApartment.pnlDockBottomUnDock(Sender: TObject; Client: TControl; NewTarget: TWinControl; var Allow: Boolean);
begin
  PostApartmentMessage(Self, mDockingStateChanged, 0, 0);
end;

procedure TrmDsgnApartment.GetListeningMessages(const AMessages: TList);
begin
  inherited;
  AMessages.Add(Pointer(mDockingStateChanged));
end;

procedure TrmDsgnApartment.DMDockingStateChanged(var Message: TrmMessageRec);
begin
  if pnlDockBottom.Height = 0 then
    spltBottom.Visible := False
  else
  begin
    spltBottom.Visible := True;
    spltBottom.Top := pnlDockBottom.Top - spltBottom.Height;
  end;

  if pnlDockLeft.Width = 0 then
    spltLeft.Visible := False
  else
  begin
    spltLeft.Visible := True;
    spltLeft.Left := pnlDockLeft.BoundsRect.Right + spltLeft.Width;
  end;

  if pnlDockRight.Width = 0 then
    spltRight.Visible := False
  else
  begin
    spltRight.Visible := True;
    spltRight.Left := pnlDockRight.Left - spltRight.Width;
  end;
end;

procedure TrmDsgnApartment.spltBottomCanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
  pnlDockBottom.AutoSize := False;
end;

procedure TrmDsgnApartment.spltBottomMoved(Sender: TObject);
begin
  pnlDockBottom.AutoSize := True;
end;

procedure TrmDsgnApartment.spltLeftCanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
  pnlDockLeft.AutoSize := False;
end;

procedure TrmDsgnApartment.spltLeftMoved(Sender: TObject);
begin
  pnlDockLeft.AutoSize := True;
end;

procedure TrmDsgnApartment.spltRightCanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
  pnlDockRight.AutoSize := False;
end;

procedure TrmDsgnApartment.spltRightMoved(Sender: TObject);
begin
  pnlDockRight.AutoSize := True;
end;

function TrmDsgnApartment.GetStatusBarClass: TrmCustomStatusBarCalss;
begin
  Result := nil;
end;

{
procedure TrmDsgnApartment.RestoreDockInfo;
var
  S: IEvDualStream;
  h, FrmName: String;
  Frm: TrmDockableFrame;
  i: Integer;
begin
  try
    S := TEvDualStreamHolder.CreateFromFile('d:\Dock.txt');

    h := S.ReadString;
    while h <> '' do
    begin
      FrmName := GetNextStrValue(h, ',');
      ShowDockableForm(TrmDockableFrameClass(FindClass(FrmName)));
      Frm := FindComponent(FrmName) as TrmDockableFrame;
    end;

    pnlDockBottom.DockManager.LoadFromStream(S.RealStream);
    pnlDockLeft.DockManager.LoadFromStream(S.RealStream);
    pnlDockRight.DockManager.LoadFromStream(S.RealStream);
  except
  end;
end;

procedure TrmDsgnApartment.StoreDockInfo;
var
  S: IEvDualStream;
  i: Integer;
  h: String;
begin
  h := '';
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TrmDockableFrame then
    begin
      if h <> '' then
        h := h + ',';
      h := h + Components[i].ClassName;
    end;

  S := TEvDualStreamHolder.CreateFromNewFile('d:\Dock.txt');
  S.WriteString(h);

  pnlDockBottom.DockManager.SaveToStream(S.RealStream);
  pnlDockLeft.DockManager.SaveToStream(S.RealStream);
  pnlDockRight.DockManager.SaveToStream(S.RealStream);
end;
}

procedure TrmDsgnApartment.StoreState;
begin
  inherited;
  StoreDockableParts;
end;


function TrmDsgnApartment.Settings: IrmSettingsHolder;
begin
  Result := FSettings;
end;

procedure TrmDsgnApartment.RestoreDockableParts;
var
  h, FrmName: String;

  procedure RestoreDockInfo;
  var
    S: IEvDualStream;
    V: Variant;
  begin
    try
      V := VarArrayOf([]);
      V := Settings.ReadData(ClassName + '\DockingInfo', V);

      if VarArrayHighBound(V, 1) <> -1 then
      begin
        S := TEvDualStreamHolder.CreateFromVariant(V);
        S.Position := 0;
        pnlDockBottom.DockManager.LoadFromStream(S.RealStream);
        pnlDockLeft.DockManager.LoadFromStream(S.RealStream);
        pnlDockRight.DockManager.LoadFromStream(S.RealStream);
      end;
    except
    end;
  end;

begin
  h := Settings.ReadData(ClassName + '\ToolWindows', '');
  while h <> '' do
  begin
    FrmName := GetNextStrValue(h, ',');
    ShowDockableForm(TrmDockableFrameClass(FindClass(FrmName)));
  end;

  RestoreDockInfo;  
end;


procedure TrmDsgnApartment.StoreDockableParts;
var
  i: Integer;
  h: String;

  procedure StoreDockInfo;
  var
    S: IEvDualStream;
    V: Variant;
  begin
    S := TEvDualStreamHolder.Create;
    pnlDockBottom.DockManager.SaveToStream(S.RealStream);
    pnlDockLeft.DockManager.SaveToStream(S.RealStream);
    pnlDockRight.DockManager.SaveToStream(S.RealStream);
    V := S.AsVarArrayOfBytes;
    Settings.SaveData(ClassName + '\DockingInfo', V);
  end;

begin
  h := '';
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TrmDockableFrame then
    begin
      if h <> '' then
        h := h + ',';
      h := h + Components[i].ClassName;
    end;

  Settings.SaveData(ClassName + '\ToolWindows', h);
  StoreDockInfo;  
end;


procedure TrmDsgnApartment.RestoreState;
begin
  inherited;
  RestoreDockableParts;
end;

end.
