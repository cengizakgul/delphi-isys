unit rmTrwTextPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmOPBasicFrm, StdCtrls, ExtCtrls, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls, rmOPEColorFrm, rmOPEFontFrm;

type
  TrmTrwTextProp = class(TrmOPBasic)
    frmCaption: TrmOPEStringSingleLine;
    frmFont: TrmOPEFont;
    frmColor: TrmOPEColor;
  protected
    procedure GetPropsFromObject; override;
  end;

implementation

{$R *.dfm}

{ TrmTrwTextProp }

procedure TrmTrwTextProp.GetPropsFromObject;
begin
  inherited;
  frmCaption.PropertyName := 'Text';
  frmCaption.ShowPropValue;
end;

end.
 