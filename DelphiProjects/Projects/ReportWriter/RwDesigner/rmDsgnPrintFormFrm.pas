unit rmDsgnPrintFormFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmDsgnAreaFrm, ExtCtrls, rmLibCompChooseFrm,  StdCtrls, rmTypes,
  rwBasicClasses, rmDsgnTypes, rwGraphics, Menus, rwDsgnUtils, rwEngineTypes,
  rwMessages;

type
  TrmDsgnPrintForm = class(TrmDsgnArea)
    pmTrmTable: TPopupMenu;
    miClearTable: TMenuItem;
    pmTrmTableCell: TPopupMenu;
    N3: TMenuItem;
    miSplitCell: TMenuItem;
    miMergeCells: TMenuItem;
    N2: TMenuItem;
    miCreateSubTable: TMenuItem;
    pmTrmDBTable: TPopupMenu;
    miTableBands: TMenuItem;
    miHeaderBand: TMenuItem;
    miDetailBand: TMenuItem;
    miTotalBand: TMenuItem;
    miDestroySubTable: TMenuItem;
    procedure FrameResize(Sender: TObject);
    procedure miClearTableClick(Sender: TObject);
    procedure pmTrmTablePopup(Sender: TObject);
    procedure miSplitCellClick(Sender: TObject);
    procedure miMergeCellsClick(Sender: TObject);
    procedure miCreateSubTableClick(Sender: TObject);
    procedure pmTrmTableCellPopup(Sender: TObject);
    procedure pmTrmDBTablePopup(Sender: TObject);
    procedure miHeaderBandClick(Sender: TObject);
    procedure miDestroySubTableClick(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    function  GetDsgnArea: TWinControl; override;
    function  CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject; override;
    function  GetObjectCanvasDPI: Integer; override;
  end;

implementation

uses rwTableCellSplitDlgFrm;

{$R *.dfm}

{ TrmDsgnPrintForm }

function TrmDsgnPrintForm.GetDsgnArea: TWinControl;
begin
  Result := Self;
end;

procedure TrmDsgnPrintForm.FrameResize(Sender: TObject);
begin
  inherited;

  if Assigned(DesignObject) then
    DesignObject.Notify(rmdZoomChanged);
end;

function TrmDsgnPrintForm.CreateRWObject(AClassName: String; AOwner: IrmDesignObject): IrmDesignObject;
var
  LibComp: IrmComponent;
begin
  if SameText(AClassName, 'LIBRARY') then
  begin
    LibComp := ChoiceLibComp(['TrmPrintControl', 'TrwNoVisualComponent'], AOwner, Self);
    if Assigned(LibComp) then
      AClassName := LibComp.GetName
    else
      Exit;
  end;

  if not Assigned(AOwner) then
    AOwner := DesignObject;

  Result := AOwner.CreateChildObject(AClassName);
end;


constructor TrmDsgnPrintForm.Create(AOwner: TComponent);
begin
  inherited;
  FAreaType := rmDATPrintForm;
  FImageIndex := 4;
end;

function TrmDsgnPrintForm.GetObjectCanvasDPI: Integer;
begin
  Result := cVirtualCanvasRes;
end;

procedure TrmDsgnPrintForm.miClearTableClick(Sender: TObject);
var
  Tbl: IrmTable;
begin
  Tbl := RMDesigner.GetComponentDesigner.GetSelectedObjects[0] as IrmTable;
  Tbl.ClearTable;
end;

procedure TrmDsgnPrintForm.pmTrmTablePopup(Sender: TObject);
begin
  miClearTable.Visible := RMDesigner.GetComponentDesigner.LibComponentDesigninig;
end;

procedure TrmDsgnPrintForm.miSplitCellClick(Sender: TObject);
var
  i: Integer;
  Cols, Rows: Integer;
  Cells: TrmListOfObjects;
  T: IrmTable;
begin
  Cols := 2;
  Rows := 1;

  SetLength(Cells, 0);

  if ShowTableCellSplitDlg(Self, Cols, Rows) then
  begin
    if Length(RMDesigner.GetComponentDesigner.GetSelectedObjects) > 1 then
    begin
      T := (RMDesigner.GetComponentDesigner.GetSelectedObjects[0] as IrmTableCell).GetObjectOwner as IrmTable;
      Cells := T.GetCellsInRect(RMDesigner.GetComponentDesigner.GetSelectedObjects[0] as IrmTableCell, RMDesigner.GetComponentDesigner.GetSelectedObjects[1] as IrmTableCell);
      for i := Low(Cells) to High(Cells) do
        (Cells[i] as IrmTableCell).SplitCell(Cols, Rows);
    end
    else
      (RMDesigner.GetComponentDesigner.GetSelectedObjects[0] as IrmTableCell).SplitCell(Cols, Rows);
  end;
end;

procedure TrmDsgnPrintForm.miMergeCellsClick(Sender: TObject);
var
  C1, C2: IrmDesignObject;
  Tbl: IrmTable;
  Cell1, Cell2: String;
begin
  if Length(RMDesigner.GetComponentDesigner.GetSelectedObjects) <> 2 then
    Exit;

  C1 := RMDesigner.GetComponentDesigner.GetSelectedObjects[0];
  C2 := RMDesigner.GetComponentDesigner.GetSelectedObjects[1];
  RMDesigner.GetComponentDesigner.UnselectAll;
  Cell1 := C1.GetPropertyValue('Name');
  Cell2 := C2.GetPropertyValue('Name');
  Tbl := C1.GetObjectOwner as IrmTable;
  C1 := nil;
  C2 := nil;
  RMDesigner.GetComponentDesigner.SelectObject(Tbl.MergeCells(Cell1, Cell2));
end;

procedure TrmDsgnPrintForm.miCreateSubTableClick(Sender: TObject);
begin
  (PopupMenuSender as IrmTableCell).CreateSubTable;
end;

procedure TrmDsgnPrintForm.pmTrmTableCellPopup(Sender: TObject);
begin
  miMergeCells.Enabled := Length(RMDesigner.GetComponentDesigner.GetSelectedObjects) > 1;
  miCreateSubTable.Enabled := Length(RMDesigner.GetComponentDesigner.GetSelectedObjects) <= 1;
end;

procedure TrmDsgnPrintForm.pmTrmDBTablePopup(Sender: TObject);
var
  T: IrmDesignObject;
begin
  T := PopupMenuSender;
  miDetailBand.Enabled := RMDesigner.GetComponentDesigner.LibComponentDesigninig;
  miDetailBand.Checked := T.CallCustomFunction('GetBandByType', ['Detail']) <> 0;
  miHeaderBand.Checked := T.CallCustomFunction('GetBandByType', ['Header']) <> 0;
  miTotalBand.Checked :=  T.CallCustomFunction('GetBandByType', ['Total']) <> 0;
  miTableBands.Visible := Length(RMDesigner.GetComponentDesigner.GetSelectedObjects) <= 1;
  miDestroySubTable.Visible := (T as IrmDBTableObject).IsSubTable;
end;

procedure TrmDsgnPrintForm.miHeaderBandClick(Sender: TObject);
var
  bt: String;
  Bnd, Tbl: IrmDesignObject;
begin
  Tbl := PopupMenuSender;

  if Sender = miHeaderBand then
    bt := 'Header'
  else if Sender = miTotalBand then
    bt := 'Total'
  else if Sender = miDetailBand then
    bt := 'Detail'
  else
    Exit;

  TMenuItem(Sender).Checked := not TMenuItem(Sender).Checked;
  RMDesigner.GetComponentDesigner.UnselectAll;

  Bnd := IrmDesignObject(Pointer(Integer(Tbl.CallCustomFunction('ChangeBands', [bt, TMenuItem(Sender).Checked]))));

  if TMenuItem(Sender).Checked and Assigned(Bnd) then
    RMDesigner.GetComponentDesigner.SelectObject(Bnd)
  else
    RMDesigner.GetComponentDesigner.SelectObject(Tbl);
end;

procedure TrmDsgnPrintForm.miDestroySubTableClick(Sender: TObject);
var
  C: IrmDesignObject;
begin
  if PopupMenuSender.IsInheritedComponent then
    raise ErwException.Create(ResErrInheritedDelete);

  C := PopupMenuSender.GetParent;

  RMDesigner.GetComponentDesigner.UnselectAll;
  (C as IrmTableCell).DestroySubTable;
end;

end.
