unit rmDataSetExplorerFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, Wwdbigrd, Wwdbgrid, ISBasicClasses, rwDesignClasses, DB,
  Wwdatsrc, kbmMemTable, ISKbmMemDataSet, StdCtrls, ExtCtrls, rwDebugInfo,
  rwDB, rwVirtualMachine, sbAPI, ISDataAccessComponents;

type
  TrmDataSetExplorer = class(TFrame)
    Panel1: TPanel;
    Label1: TLabel;
    edDataSet: TEdit;
    Panel2: TPanel;
    Label3: TLabel;
    Label2: TLabel;
    edRecCount: TEdit;
    edRecNo: TEdit;
    chbBOF: TCheckBox;
    chbEOF: TCheckBox;
    DS: TisRWClientDataSet;
    dsExpl: TwwDataSource;
    grData: TisRWDBGrid;
    procedure edDataSetExit(Sender: TObject);
    procedure edDataSetKeyPress(Sender: TObject; var Key: Char);
  private
    FWatch: PTrwWatch;
    FDataSet: TrwDataSet;

    procedure FOnChangeDataSet(Sender: TObject);
    procedure FAfterPost(Sender: TObject);
    procedure SyncContent;
    procedure SyncControls;
  public
    procedure Initialize;
    procedure Synchronize;
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
  end;

implementation

{$R *.dfm}

procedure TrmDataSetExplorer.AfterConstruction;
var
  i: Integer;
begin
  inherited;

  FDataSet := nil;
  FWatch := nil;

  for i := 0 to DebugInfo.WatchList.Count - 1 do
    if DebugInfo.WatchList[i].DataSet then
    begin
      FWatch := DebugInfo.WatchList[i];
      edDataSet.Text := FWatch.Expression;
      break;
    end;

  SyncControls;
end;

procedure TrmDataSetExplorer.BeforeDestruction;
begin
  if Assigned(FDataSet) then
  begin
     FDataSet.AfterPost := nil;
     FDataSet.OnDataChange := nil;
     FDataSet := nil;
  end;

  inherited;  
end;

procedure TrmDataSetExplorer.edDataSetExit(Sender: TObject);
begin
  if not Assigned(FWatch) then
    FWatch := DebugInfo.WatchList[DebugInfo.WatchList.Add(edDataSet.Text)]
  else
  begin
    FWatch.Expression := Trim(edDataSet.Text);
    DebugInfo.WatchList.NeedCompileWatches := True;
  end;

  if Length(FWatch.Expression) = 0 then
  begin
    DebugInfo.WatchList.Delete(FWatch);
    FWatch := nil;
    if Assigned(FDataSet) then
    begin
       FDataSet.OnDataChange := nil;
       FDataSet.AfterPost := nil;
       FDataSet := nil;
    end;
    DS.Close;
    SyncControls;
  end

  else
  begin
    FWatch.DataSet := True;
    Initialize;
  end;
end;

procedure TrmDataSetExplorer.edDataSetKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    edDataSet.OnExit(nil);
end;

procedure TrmDataSetExplorer.FAfterPost(Sender: TObject);
begin
  SyncContent;
end;

procedure TrmDataSetExplorer.FOnChangeDataSet(Sender: TObject);
begin
  if not DS.Active or not Assigned(FDataSet) or not Boolean(FDataSet.PActive) or
     (FDataSet.PRecordCount <> DS.RecordCount) then
    SyncContent
  else
    SyncControls;
end;

procedure TrmDataSetExplorer.Initialize;
begin
  VM.CalcWatches;
  Synchronize;
end;

procedure TrmDataSetExplorer.SyncContent;
var
  i: Integer;
begin
  DS.DisableControls;
  try
    grData.DataSource := nil;

    if not Assigned(FDataSet) or not Boolean(FDataSet.PActive) then
    begin
      DS.Close;
      Exit;
    end;  

    if FDataSet.DataSet.State in [sbcInactive, sbcBrowse] then
      DS.Close;

    if FDataSet.DataSet.State = sbcBrowse then
    begin
      sbSBCursorToClientDataSet(FDataSet.DataSet, DS);
      DS.Open;
      for i := 0 to DS.Fields.Count - 1 do
      begin
        if DS.Fields[i] is TFloatField then
          TFloatField(DS.Fields[i]).DisplayFormat := '';
      end;
    end;

  finally
    DS.EnableControls;
    SyncControls;
    grData.DataSource := dsExpl;
  end;
end;

procedure TrmDataSetExplorer.SyncControls;
begin
  if Assigned(FDataSet) and DS.Active then
  begin
    if FDataSet.PRecordNumber > 0 then
      DS.RecNo := FDataSet.PRecordNumber;
    edRecCount.Text := IntToStr(FDataSet.PRecordCount);
    edRecNo.Text := IntToStr(FDataSet.PRecordNumber);
    chbBOF.Checked := FDataSet.PBOF;
    chbEOF.Checked := FDataSet.PEOF;
  end

  else
  begin
    edRecCount.Text := '';
    edRecNo.Text := '';
    chbBOF.State := cbGrayed;
    chbEOF.State := cbGrayed;
  end
end;

procedure TrmDataSetExplorer.Synchronize;
var
  P: Integer;
begin
  if Assigned(FWatch) then
  begin
    try
      if FWatch.Error then
      begin
        FDataSet := nil;
        DS.Close;
        SyncControls;
      end

      else
      begin
        P := StrToInt(FWatch.Result);
        if P <> Integer(Pointer(FDataSet)) then
        begin
          if Assigned(FDataSet) then
          begin
             FDataSet.OnDataChange := nil;
             FDataSet.AfterPost := nil;
             FDataSet := nil;
          end;

          FDataSet := TrwDataSet(Pointer(P));
          FDataSet.OnDataChange := FOnChangeDataSet;
          FDataSet.AfterPost := FAfterPost;
          FDataSet.OnChangeFilter := FAfterPost;
          FDataSet.OnChangeSort := FAfterPost;
          SyncContent;
        end;
      end;

    except
      DS.Close;
      SyncControls;
      Exit;
    end;
  end;
end;

end.
