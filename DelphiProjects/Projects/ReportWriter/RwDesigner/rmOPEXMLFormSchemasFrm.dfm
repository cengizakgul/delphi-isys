inherited rmOPEXMLFormSchemas: TrmOPEXMLFormSchemas
  Width = 285
  Height = 182
  object lvSchemas: TListView
    Left = 0
    Top = 0
    Width = 285
    Height = 137
    Align = alTop
    Columns = <
      item
        Caption = 'Schema Name'
        MinWidth = 80
        Width = 100
      end
      item
        AutoSize = True
        Caption = 'Description'
        MinWidth = 80
      end>
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnSelectItem = lvSchemasSelectItem
  end
  object btnAdd: TButton
    Left = 3
    Top = 147
    Width = 85
    Height = 22
    Caption = 'Load and Parse'
    TabOrder = 1
    OnClick = btnAddClick
  end
  object btnDel: TButton
    Left = 234
    Top = 147
    Width = 45
    Height = 22
    Caption = 'Delete'
    Enabled = False
    TabOrder = 2
    OnClick = btnDelClick
  end
  object btnSave: TButton
    Left = 182
    Top = 147
    Width = 45
    Height = 22
    Caption = 'Save'
    Enabled = False
    TabOrder = 3
    OnClick = btnSaveClick
  end
  object btnUpdate: TButton
    Left = 94
    Top = 147
    Width = 45
    Height = 22
    Caption = 'Update'
    Enabled = False
    TabOrder = 4
    OnClick = btnUpdateClick
  end
  object btnAddNoParse: TButton
    Left = 145
    Top = 147
    Width = 30
    Height = 22
    Caption = 'Add'
    TabOrder = 5
    OnClick = btnAddNoParseClick
  end
  object odlgXMLSchema: TOpenDialog
    Filter = 'Schema files (*.xsd)|*.xsd|All Files (*.*)|*.*'
    Title = 'Open XML Schema for loading with parsing'
    Left = 16
    Top = 96
  end
  object saveXMLSchema: TSaveDialog
    Filter = 'Schema files (*.xsd)|*.xsd'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save XML Schema'
    Left = 120
    Top = 96
  end
  object udlgXMLSchema: TOpenDialog
    Filter = 'Schema files (*.xsd)|*.xsd'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Open XML Schema for updating'
    Left = 56
    Top = 96
  end
  object adlgXMLSchema: TOpenDialog
    Filter = 'Schema files (*.xsd)|*.xsd'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Open XML Schema for adding without parsing'
    Left = 88
    Top = 96
  end
end
