unit rmTrmDBTableBandPropFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rmTrmTablePropFrm, StdCtrls, ExtCtrls, rmOPEBooleanFrm,
  rmOPEColorFrm, rmOPEFontFrm, rmObjPropertyEditorFrm,
  rmOPEStringSingleLineFrm, ComCtrls, rmOPEExpressionFrm,
  rmOPEBlockingControlFrm;

type
  TrmTrmDBTableBandProp = class(TrmTrmTableProp)
    frmPrintIfNoData: TrmOPEBoolean;
  protected
    procedure GetPropsFromObject; override;
  end;

implementation

{$R *.dfm}

{ TrmTrmDBTableBandProp }

procedure TrmTrmDBTableBandProp.GetPropsFromObject;
begin
  inherited;
  frmPrintIfNoData.Visible := RWObject.ObjectInheritsFrom('TrmDBTableHeaderBand') or RWObject.ObjectInheritsFrom('TrmDBTableTotalBand');
  frmPrintIfNoData.PropertyName := 'PrintIfNoData';
  frmPrintIfNoData.ShowPropValue;

  if RWObject.ObjectInheritsFrom('TrmDBTableHeaderBand') then
  begin
    frmPrintOnEachPage.Visible := True;
    frmPrintOnEachPage.chbProp.Caption := 'Print on new Page if Table breaks up';
    frmPrintIfNoData.Top := 61;
  end
  else if RWObject.ObjectInheritsFrom('TrmDBTableGroupHeaderBand') then
  begin
    frmPrintOnEachPage.Visible := True;
    frmPrintOnEachPage.chbProp.Caption := 'Print on new Page if Group breaks up';
  end
  else
  begin
    frmPrintOnEachPage.Visible := False;
    frmPrintIfNoData.Top :=  frmPrintOnEachPage.Top;
  end;
end;

end.
