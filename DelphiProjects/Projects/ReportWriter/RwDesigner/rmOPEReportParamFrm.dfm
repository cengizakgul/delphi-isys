inherited rmOPEReportParam: TrmOPEReportParam
  Width = 239
  Height = 82
  AutoSize = True
  object lParamName: TLabel
    Left = 0
    Top = 4
    Width = 79
    Height = 13
    Caption = 'Parameter Name'
  end
  object Label1: TLabel
    Left = 0
    Top = 35
    Width = 53
    Height = 13
    Caption = 'Description'
  end
  object edParamName: TEdit
    Left = 90
    Top = 0
    Width = 149
    Height = 21
    TabOrder = 0
    OnChange = edParamNameChange
    OnExit = edParamNameExit
  end
  object chbDefault: TCheckBox
    Left = 0
    Top = 65
    Width = 100
    Height = 17
    Caption = 'Value by default'
    TabOrder = 1
    OnClick = edParamNameChange
  end
  object edParamDescription: TEdit
    Left = 90
    Top = 31
    Width = 149
    Height = 21
    TabOrder = 2
    OnChange = edParamNameChange
  end
  object chbShowInQB: TCheckBox
    Left = 113
    Top = 65
    Width = 126
    Height = 17
    Caption = 'Show in Query Builder'
    TabOrder = 3
    OnClick = edParamNameChange
  end
end
