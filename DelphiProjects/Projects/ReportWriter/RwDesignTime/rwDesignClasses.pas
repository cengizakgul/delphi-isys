unit rwDesignClasses;

interface

uses
  Classes, rwCallback, ISBasicClasses, ISDataAccessComponents;

type
  TisRWClientDataSet = class(TISBasicClientDataSet)
  end;

  TisRWDBGrid = class(TISDBGrid)
  end;

  TisRWDBCheckGrid = class(TISDBCheckGrid)
  end;

  TisRWDBLookupCombo = class(TisDBLookupCombo)
  end;

  TisRWDBComboBox = class(TisDBComboBox)
  end;

  TisRWDBComboDlg = class(TisDBComboDlg)
  end;

  TisRWDBSpinEdit = class(TisDBSpinEdit);

  procedure Register;

implementation

procedure Register;
begin
  rwCallback.Register;
  ISBasicClasses.Register;

  RegisterComponents('Report Writer', [TisRWClientDataSet, TisRWDBGrid, TisRWDBCheckGrid, TisRWDBLookupCombo, TisRWDBComboBox,
    TisRWDBComboDlg, TisRWDBSpinEdit]);
end;

end.
