unit isProjectExpFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ImgList, CommCtrl, ToolWin, StdCtrls, ExtCtrls, isSettings,
  isBasicUtils, isBaseClasses, BaseDockForm;

type
  TFileActionEvent = procedure (const AFileName: String) of object;

  TisProjectExplorer = class(TBaseDockableForm)
    ListView: TListView;
    ImageList1: TImageList;
    Panel1: TPanel;
    cbFilters: TComboBox;
    Label1: TLabel;
    procedure ListViewColumnClick(Sender: TObject; Column: TListColumn);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbFiltersClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ListViewDblClick(Sender: TObject);
    procedure ListViewKeyPress(Sender: TObject; var Key: Char);
  private
    FUpArrow: TBitmap;
    FDownArrow: TBitmap;
    FFilters: IisList;
    FOnFileAction: TFileActionEvent;
    FProjectPath: String;
    procedure CreateFilters;
    procedure SetProjectFile(const AValue: String);
    property  OnFileAction: TFileActionEvent read FOnFileAction write FOnFileAction;
    procedure DoFileAction;
    procedure ApplyFilterChange;
  protected
    FProjectFile: String;
    procedure AddFiles(const APath: String);
    procedure Clear;
    procedure DefaultSort;
    procedure RepaintHeader;
    procedure WMNotify(var Msg: TMessage); message WM_NOTIFY;
  public
    procedure ShowDockForm;
    property  ProjectFile: String read FProjectFile write SetProjectFile;
  end;

  procedure CreateProjectExplorerForm(const AFileActionEvent: TFileActionEvent);
  function  GetProjectExplorerForm: TisProjectExplorer;
  procedure DestroyProjectExplorerForm;

implementation

{$R *.dfm}

var
  FormInstance: TBaseDockableForm;

procedure CreateProjectExplorerForm(const AFileActionEvent: TFileActionEvent);
begin
  if not Assigned(FormInstance) then
  begin
    CreateDockableForm(FormInstance, TisProjectExplorer);
    GetProjectExplorerForm.OnFileAction := AFileActionEvent;
  end;
end;

procedure DestroyProjectExplorerForm;
begin
  FreeDockableForm(FormInstance);
end;

function GetProjectExplorerForm: TisProjectExplorer;
begin
  Result := TisProjectExplorer(FormInstance);
end;


function SortFirstSubItemAsString(Item1, Item2: TListItem; ParamSort: Integer): integer; stdcall;
begin
  Result := 0;
  if AnsiUpperCase(Item1.SubItems[0]) > AnsiUpperCase(Item2.SubItems[0]) then
    Result := ParamSort
  else
  if AnsiUpperCase(Item1.SubItems[0]) < AnsiUpperCase(Item2.SubItems[0]) then
    Result := -ParamSort;
end;

function SortSecondSubItemAsString(Item1, Item2: TListItem; ParamSort: Integer): integer; stdcall;
begin
  Result := 0;
  if AnsiUpperCase(Item1.SubItems[1]) > AnsiUpperCase(Item2.SubItems[1]) then
    Result := ParamSort
  else
  if AnsiUpperCase(Item1.SubItems[1]) < AnsiUpperCase(Item2.SubItems[1]) then
    Result := -ParamSort;
end;

function SortCaptionAsString(Item1, Item2: TListItem; ParamOwner: Integer): integer; stdcall;
var
  LV: TListView;
begin
  LV := TListView(ParamOwner);
  Result := 0;

  if LV.Columns[0].Tag = 1 then
  begin
    if AnsiUpperCase(Item1.Caption) > AnsiUpperCase(Item2.Caption) then
       Result := LV.Columns[0].Tag
    else if AnsiUpperCase(Item1.Caption) < AnsiUpperCase(Item2.Caption) then
       Result := -LV.Columns[0].Tag;
  end else
  if LV.Columns[1].Tag = 1 then
  begin
    if Result = 0 then
      Result := SortFirstSubItemAsString(Item1, Item2, 1);
    if Result = 0 then
    begin
      if AnsiUpperCase(Item1.Caption) > AnsiUpperCase(Item2.Caption) then
         Result := LV.Columns[0].Tag
      else if AnsiUpperCase(Item1.Caption) < AnsiUpperCase(Item2.Caption) then
         Result := -LV.Columns[0].Tag;
    end;
  end else
  if LV.Columns[2].Tag = 1 then
  begin
    if Result = 0 then
      Result := SortSecondSubItemAsString(Item1, Item2, 1);
    if Result = 0 then
    begin
      if AnsiUpperCase(Item1.Caption) > AnsiUpperCase(Item2.Caption) then
         Result := LV.Columns[0].Tag
      else if AnsiUpperCase(Item1.Caption) < AnsiUpperCase(Item2.Caption) then
         Result := -LV.Columns[0].Tag;
    end;
  end;
end;

procedure TisProjectExplorer.FormCreate(Sender: TObject);

  procedure MakeBitmap(var Bmp: TBitmap; ImageIndex: Integer );
  begin
    Bmp:= TBitmap.Create;
    Bmp.Width := Imagelist1.Width;
    Bmp.Height := Imagelist1.Height;
    with Bmp.Canvas do
    begin
      Brush.COlor := clBtnface;
      Brush.Style := bsSolid;
      FillRect(Cliprect);
    end;
    ImageList1.Draw(Bmp.canvas, 0, 0, ImageIndex );
  end;

begin
  inherited;
  MakeBitmap(FUpArrow, 1);
  MakeBitmap(FDownArrow, 0);
  Clear;
  CreateFilters;
end;

procedure TisProjectExplorer.AddFiles(const APath: String);

  procedure FindFiles(ListView: TListView; StartDir, FileMask: string);
  var
    SR: TSearchRec;
    DirList: TStringList;
    Found: Boolean;
    i: Integer;
    DfmName, Folder: String;
  begin
    if StartDir[Length(StartDir)] <> '\' then
      StartDir := StartDir + '\';

    Found := FindFirst(StartDir + FileMask, faAnyFile - faDirectory, SR) = 0;
    try
      while Found do
      begin
        ListView.AddItem(SR.Name, nil);
        Folder := ExtractFilePath(StartDir + SR.Name);
        GetPrevStrValue(Folder, '\');
        Folder := GetPrevStrValue(Folder, '\');
        ListView.Items[ListView.Items.Count-1].SubItems.Add(Folder);
        ListView.Items[ListView.Items.Count-1].SubItems.Add(ExtractFilePath(StartDir + SR.Name));
        DfmName := SR.Name;
        DfmName := copy(DfmName, 1, Length(DfmName) - Length(ExtractFileExt(DfmName)))+'.dfm';
        if FileExists(ExtractFilePath(StartDir + SR.Name)+DfmName) then
        begin
          ListView.Items[ListView.Items.Count-1].ImageIndex := 2;
          ListView.Items[ListView.Items.Count-1].StateIndex := 2;
        end
        else
        begin
          ListView.Items[ListView.Items.Count-1].ImageIndex := 3;
          ListView.Items[ListView.Items.Count-1].StateIndex := 3;
        end;
        Found := FindNext(SR) = 0;
      end;
    finally
      FindClose(SR);
    end;

    DirList := TStringList.Create;
    try
      Found := FindFirst(StartDir + '*.*', faAnyFile, SR) = 0;
      while Found do
      begin
        if ((SR.Attr and faDirectory) <> 0) and
            (SR.Name[1] <> '.') then
          DirList.Add(StartDir + SR.Name);
        Found := FindNext(SR) = 0;
      end;
      FindClose(SR);

      for i := 0 to DirList.Count - 1 do
        FindFiles(ListView, DirList[i], FileMask);
    finally
      DirList.Free;
    end;
  end;

begin
  FindFiles(ListView, APath, '*.pas');
end;

procedure TisProjectExplorer.ListViewColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  case Column.Index of
  0: begin
       ListView.Columns[0].Tag := 1;
       ListView.Columns[1].Tag := 0;
       ListView.Columns[2].Tag := 0;
     end;
  1: begin
       ListView.Columns[0].Tag := 0;
       ListView.Columns[1].Tag := 1;
       ListView.Columns[2].Tag := 0;
     end;
  2: begin
       ListView.Columns[0].Tag := 0;
       ListView.Columns[1].Tag := 0;
       ListView.Columns[2].Tag := 1;
     end;
  end;
  ListView.CustomSort(@SortCaptionAsString, Integer(ListView));
  RepaintHeader;
end;

procedure TisProjectExplorer.FormShow(Sender: TObject);
begin
  inherited;
  ListView.Columns[0].Tag := 1;
  ListView.Columns[1].Tag := 0;
  ListView.Columns[2].Tag := 0;
end;

procedure TisProjectExplorer.Clear;
begin
  ListView.Clear;
end;

procedure TisProjectExplorer.DefaultSort;
begin
  ListView.Columns[0].Tag := 1;
  ListView.Columns[1].Tag := 0;
  ListView.Columns[2].Tag := 0;
  if ListView.Items.Count > 0 then
    ListView.CustomSort(@SortCaptionAsString, Integer(ListView));
  Application.ProcessMessages;  
  RepaintHeader;
end;

procedure TisProjectExplorer.CreateFilters;
var
  IniFile: IisSettings;
  s, sFlt, sFltVal, sFilterName, sIdent: String;
  Filters: IisStringListRO;
  FltInfo: IisStringList;
  sCongigFile: String;
  i: Integer;
begin
  cbFilters.Clear;
  FFilters := TisList.Create;

  sCongigFile := ChangeFileExt(FProjectFile, '.pes');
  if (FProjectFile <> '') and FileExists(sCongigFile) then
  begin
    IniFile := TisSettingsINI.Create(sCongigFile);
    Filters := IniFile.GetValueNames('Views');

    for i := 0 to Filters.Count - 1 do
    begin
      sFilterName := Filters[i];
      sFlt := IniFile.AsString['Views\' + sFilterName];

      FltInfo := TisStringList.Create;
      FFilters.Add(FltInfo);

      while sFlt <> '' do
      begin
        sFltVal := GetNextStrValue(sFlt, ';');
        FltInfo.Add(sFltVal);
      end;

      s := sFilterName;
      sIdent := '';
      while s <> '' do
      begin
        sFilterName := GetNextStrValue(s, '.');
        if s <> '' then
          sIdent := '    ' + sIdent;
      end;

      cbFilters.AddItem(sIdent + sFilterName, nil);
    end;
  end;
end;

procedure TisProjectExplorer.cbFiltersClick(Sender: TObject);
begin
  ApplyFilterChange;
end;

procedure TisProjectExplorer.SetProjectFile(const AValue: String);
begin
  if FProjectFile <> AValue then
  begin
    Clear;
    FProjectFile := AValue;
    if FProjectFile <> '' then
    begin
      FProjectPath := NormalizePath(ExtractFileDir(FProjectFile));
      Caption := ExtractFileName(FProjectFile);
    end
    else
    begin
      FProjectPath := '';
      Caption :='IS Project Explorer';
    end;

    CreateFilters;

    if cbFilters.Items.Count > 0 then
    begin
      cbFilters.ItemIndex := 0;
      ApplyFilterChange;
    end;
  end;
end;

procedure TisProjectExplorer.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caHide;
end;

procedure TisProjectExplorer.ListViewDblClick(Sender: TObject);
begin
  DoFileAction;
end;

procedure TisProjectExplorer.RepaintHeader;
var
  hdr: HWND;
  hdritem: HD_ITEM;

  procedure RedrawHeader(Index: Integer);
  begin
    if not (ListView.Columns.Count > Index) then Exit; 
    Header_GetItem(hdr, Index, hdritem);
    hdritem.Mask := HDI_FORMAT or HDI_BITMAP;
    hdritem.hbm := 0;

    if ListView.Columns[Index].Tag = -1 then
      hdritem.hbm := FUpArrow.Handle
    else if ListView.Columns[Index].Tag = 1 then
      hdritem.hbm := FDownArrow.Handle;

    hdritem.fmt := hdritem.fmt or HDF_BITMAP_ON_RIGHT or HDF_BITMAP;
    Header_SetItem(hdr, Index, hdritem);
  end;

begin

  Exit;

  hdr := ListView_GetHeader(ListView.Handle);
  FillChar(hdritem, SizeOf(hdritem), 0);
  hdritem.Mask := HDI_FORMAT;

  RedrawHeader(0);
  RedrawHeader(1);
  RedrawHeader(2);
end;

procedure TisProjectExplorer.WMNotify(var Msg: TMessage);
begin
  inherited;
//  if Msg.LParam = 1241552 then
//    RepaintHeader;
end;

procedure TisProjectExplorer.ShowDockForm;
begin
  ShowDockableForm(Self);
end;

procedure TisProjectExplorer.ListViewKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    DoFileAction;
end;

procedure TisProjectExplorer.DoFileAction;
var
  sFileName: String;
begin
  if Assigned(FOnFileAction) and (ListView.Selected <> nil) then
  begin
    sFileName := NormalizePath(ListView.Selected.SubItems[1]) + ListView.Selected.Caption;
    FOnFileAction(sFileName);
  end;
end;

procedure TisProjectExplorer.ApplyFilterChange;
var
  Flt: IisStringList;
  i: Integer;
  sLastCurrDir: String;
begin
  Flt := FFilters[cbFilters.ItemIndex] as IisStringList;

  ListView.Items.BeginUpdate;
  try
    Clear;
    sLastCurrDir := GetCurrentDir;
    try
      for i := 0 to Flt.Count - 1 do
      begin
        SetCurrentDir(FProjectPath + Flt[i]);
        AddFiles(GetCurrentDir);
      end;
    finally
      SetCurrentDir(sLastCurrDir);
    end;

    DefaultSort;
  finally
    ListView.Items.EndUpdate;
  end;
end;

end.
