unit IDE_Experts;

interface

uses
   Windows, Classes, SysUtils, EditIntf, Forms, Dialogs,
   EvStreamUtils, isBasicUtils, Math, isSettings, isBaseClasses, ShellApi,
   isProjectExpFrm, ToolsAPI,
{$WARNINGS OFF}
   ExptIntf, ToolIntf;
{$WARNINGS ON}


procedure Register;

implementation

type
  TFileEventNotifier = class(TNotifierObject, IOTANotifier, IOTAIDENotifier)
  private
    FDepth: Integer;
    procedure CheckAncestorsAndComp(const AFileName: String);
    function  OpenFileByExtension(const AFileName: String): Boolean;
    procedure InitProjectExplorer;
  protected
    procedure FileNotification(NotifyCode: TOTAFileNotification; const FileName: string; var Cancel: Boolean);
    procedure BeforeCompile(const Project: IOTAProject; var Cancel: Boolean); overload;
    procedure AfterCompile(Succeeded: Boolean); overload;
  end;


  TProjectExplorerExpert = class(TNotifierObject, IOTANotifier, IOTAWizard, IOTAMenuWizard)
  private
    procedure OnFileAction(const AFileName: String);
  public
    function    GetIDString: string;
    function    GetName: string;
    function    GetState: TWizardState;
    function    GetMenuText: string;
    procedure   Execute;
    constructor Create;
  end;

var
  FileEventNotifierIndex: Integer;

procedure Register;
begin
  RegisterPackageWizard(TProjectExplorerExpert.Create);
  FileEventNotifierIndex := (BorlandIDEServices as IOTAServices).AddNotifier(TFileEventNotifier.Create);
end;

{ TFileEventNotifier }

procedure TFileEventNotifier.AfterCompile(Succeeded: Boolean);
begin
end;

procedure TFileEventNotifier.BeforeCompile(const Project: IOTAProject; var Cancel: Boolean);
begin
end;

procedure TFileEventNotifier.CheckAncestorsAndComp(const AFileName: String);
var
  s: String;
  F: IevDualStream;
  sSearchPath, sUnit, sUses, sCurrentUnit: String;
  SearchPathList: IisStringList;
  i: Integer;
  M: TIModuleInterface;
  sCurDir: String;
begin
  if not AnsiSameText(ExtractFileExt(AFileName), '.pas') then
    Exit;

  Inc(FDepth);
  sCurDir := GetCurrentDir;
  try
    s := ExtractFileName(AFileName);
    s := ChangeFileExt(AFileName, '.dfm');
    if FileExists(s) and (GetActiveProject <> nil) then
    begin
      s := GetActiveProject.FileName;
      s := ChangeFileExt(s, '.dof');
      SetCurrentDir(ExtractFilePath(s));
      sSearchPath := ReadFromIniFile(s, 'Directories\SearchPath', '');
      SearchPathList := TisStringList.Create;
      SearchPathList.Delimiter := ';';
      SearchPathList.DelimitedText := sSearchPath;

      F := TEvDualStreamHolder.CreateFromFile(AFileName);
      SetLength(sUses, Min(4096, F.Size));
      F.ReadBuffer(sUses[1], Length(sUses));
      F := nil;

      GetNextStrValue(sUses, 'interface');
      GetNextStrValue(sUses, 'uses');
      sUses := GetNextStrValue(sUses, ';');
      while sUses <> '' do
      begin
        sUnit := Trim(GetNextStrValue(sUses, ','));
        sUnit := StringReplace(sUnit, #13, '', [rfReplaceAll]);
        sUnit := StringReplace(sUnit, #10, '', [rfReplaceAll]);
        if sUnit <> '' then
          for i := 0 to SearchPathList.Count - 1 do
          begin
            sCurrentUnit := NormalizePath(SearchPathList[i]) + sUnit + '.pas';
            if FileExists(ChangeFileExt(sCurrentUnit, '.dfm')) then
            begin
              M := ToolServices.GetModuleInterface(sCurrentUnit);
              if not Assigned(M) then
              begin
                ToolServices.CreateModule(sCurrentUnit, nil, nil, [cmExisting]);
                M := ToolServices.GetModuleInterface(sCurrentUnit);
              end;
              M.Release;
              break;
            end;
          end;
      end;
    end;
  finally
    SetCurrentDir(sCurDir);
    Dec(FDepth);
  end;
end;

procedure TFileEventNotifier.FileNotification(NotifyCode: TOTAFileNotification; const FileName: string; var Cancel: Boolean);
begin
  if NotifyCode = ofnFileOpening then
  begin
    if OpenFileByExtension(FileName) then
      Cancel := True
    else
      CheckAncestorsAndComp(FileName);
  end

  else if NotifyCode = ofnActiveProjectChanged then
     InitProjectExplorer;
end;

procedure TFileEventNotifier.InitProjectExplorer;
begin
  if (GetProjectExplorerForm <> nil) and (GetActiveProject <> nil) then
    GetProjectExplorerForm.ProjectFile := GetActiveProject.FileName;
end;

function TFileEventNotifier.OpenFileByExtension(const AFileName: String): Boolean;
var
  sExt: String;
begin
  sExt := AnsiUpperCase(ExtractFileExt(AFileName));
  if (sExt = '.JPG') or (sExt = '.BMP') or (sExt = '.DOC') then
  begin
    ShellExecute(0, 'open', PAnsiChar(AFileName), nil, nil, SW_SHOWNORMAL);
    Result := True;
  end
  else
    Result := False;
end;


{ TProjectExplorerExpert }

constructor TProjectExplorerExpert.Create;
begin
  CreateProjectExplorerForm(OnFileAction);
end;

procedure TProjectExplorerExpert.Execute;
begin
  if GetActiveProject <> nil then
    GetProjectExplorerForm.ProjectFile := GetActiveProject.FileName
  else
    GetProjectExplorerForm.ProjectFile := '';
  GetProjectExplorerForm.ShowDockForm;
end;

function TProjectExplorerExpert.GetIDString: string;
begin
  Result := 'iSystems.ProjectExplorer';
end;

function TProjectExplorerExpert.GetMenuText: string;
begin
  Result := 'IS Project Explorer';
end;

function TProjectExplorerExpert.GetName: string;
begin
  Result := 'IS Project Explorer';
end;

function TProjectExplorerExpert.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

procedure TProjectExplorerExpert.OnFileAction(const AFileName: String);
begin
  (BorlandIDEServices as  IOTAActionServices).OpenFile(AFileName);
end;

initialization
finalization
  DestroyProjectExplorerForm;
  (BorlandIDEServices as IOTAServices).RemoveNotifier(FileEventNotifierIndex);

end.
