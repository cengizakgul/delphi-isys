unit pbClientDLL;

interface

uses Windows, SysUtils, pbInterfaces;


function CreateProjectBuilder(const AHost: String; const APort: Integer = 9498;
                              const AUser: String = ''; const APassword: String = ''): IpbProjectBuilder;



implementation

const
  pbClientDLLName = 'isPBClient.dll';
  CreatePBProcName = 'CreateProjectBuilder';

type
  TCreateProjectBuilderProc = function (const AHost: String; const APort: Integer;
                                        const AUser, APassword: String): IpbProjectBuilder; stdcall;

var
  FpbClientDLL: THandle;


function CreateProjectBuilder(const AHost: String; const APort: Integer; const AUser, APassword: String): IpbProjectBuilder;
var
  CreatePBProc: TCreateProjectBuilderProc;
begin
  if FpbClientDLL <> 0 then
  begin
    CreatePBProc := GetProcAddress(FpbClientDLL, CreatePBProcName);
    if not Assigned(CreatePBProc) then
      raise Exception.CreateFmt('Function %s is not found in %s', [CreatePBProcName, pbClientDLLName]);

    Result := CreatePBProc(AHost, APort, AUser, APassword);
  end
  else
    Result := nil;
end;


initialization
  FpbClientDLL := LoadLibrary(pbClientDLLName);

finalization
  if FpbClientDLL <> 0 then
  begin
    FreeLibrary(FpbClientDLL);
    FpbClientDLL := 0;
  end;

end.
