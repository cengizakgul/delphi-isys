{-------------------------------------------------------------------------------
 
 Copyright (c) 1999-2011 Ralf Junker, The Delphi Inspiration
 Internet: http://www.yunqa.de/delphi/
 E-Mail:   delphi@yunqa.de

-------------------------------------------------------------------------------}

unit DISQLite3Reg;

{$I DI.inc}
{$I DISQLite3.inc}

interface

procedure Register;

implementation

uses
  DISystemCompat,
  {$IFDEF HAS_UNITSCOPE}
  System.SysUtils, System.TypInfo, System.Classes, Vcl.Dialogs, Data.DB,
  {$ELSE HAS_UNITSCOPE}
  SysUtils, TypInfo, Classes, Dialogs, DB,
  {$ENDIF HAS_UNITSCOPE}
  {$IFDEF COMPILER_6_UP}DesignIntf, DesignEditors, {$ELSE }DsgnIntf, {$ENDIF}
  DISQLite3Api, DISQLite3Database, DISQLite3DataSet;

type
  TDISQLite3Database_Connected_Editor = class(TEnumProperty)
  public
    procedure SetValue(const AValue: string); override;
  end;

procedure TDISQLite3Database_Connected_Editor.SetValue(const AValue: string);
var
  c: TPersistent;
begin
  if CompareText(AValue, BooleanIdents[True]) = 0 then
    begin
      c := GetComponent(0);
      if c is TDISQLite3Database then
        with c as TDISQLite3Database do
          begin
            if DatabaseName = '' then
              MessageDlg(
                'DatabaseName is empty. This creates a temporary disk-based database.',
                mtInformation, [mbOK], 0)
            else
              if sqlite3_is_memory_name(DatabaseName) then
                MessageDlg(
                  'DatabaseName is '':memory:''. This creates a temporary in-memory database.',
                  mtInformation, [mbOK], 0);
          end;
    end;
  inherited;
end;

type
  TDISQLite3Database_DatabaseName_Editor = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure Edit; override;
    procedure SetValue(const Value: string); override;
  end;

function TDISQLite3Database_DatabaseName_Editor.GetAttributes: TPropertyAttributes;
begin
  if GetComponent(0) is TDISQLite3Database then
    Result := [paDialog];
end;

procedure TDISQLite3Database_DatabaseName_Editor.SetValue(const Value: string);
var
  c: TPersistent;
begin
  c := GetComponent(0);
  if c is TDISQLite3Database then
    with c as TDISQLite3Database do
      if Connected then Connected := False;
  SetStrValue(Value);
end;

procedure TDISQLite3Database_DatabaseName_Editor.Edit;
var
  c: TPersistent;
begin
  c := GetComponent(0);
  if c is TDISQLite3Database then
    with TOpenDialog.Create(nil) do
      try
        Filter := 'Any File (*.*)|*.*|DISQLite3 Database (*.db;*.db3)|*.db;*.db3';
        InitialDir := ExtractFilePath((c as TDISQLite3Database).DatabaseName);
        if Execute then
          (c as TDISQLite3Database).DatabaseName := FileName;
      finally
        Free;
      end
  else
    inherited;
end;

procedure Register;
begin
  RegisterComponents('DISQLite', [
    TDISQLite3Database,
      TDISQLite3DataSetImporter,
      TDISQLite3UniDirQuery]);

  RegisterPropertyEditor(
    TypeInfo(Boolean),
    TDISQLite3Database,
    'Connected',
    TDISQLite3Database_Connected_Editor);
  RegisterPropertyEditor(
    TypeInfo(UnicodeString),
    TDISQLite3Database,
    'DatabaseName',
    TDISQLite3Database_DatabaseName_Editor);

  RegisterFields([TDISQLite3MemoField]);
  RegisterFields([TDISQLite3WideStringField]);

  {$IFNDEF COMPILER_10_UP}
  RegisterFields([TDISQLite3BlobField]);
  {$ENDIF !COMPILER_10_UP}
end;

end.

