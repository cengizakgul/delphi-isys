{-------------------------------------------------------------------------------
 
 Copyright (c) 1999-2011 Ralf Junker, The Delphi Inspiration
 Internet: http://www.yunqa.de/delphi/
 E-Mail:   delphi@yunqa.de

-------------------------------------------------------------------------------}

unit DISQLite3IniFile;

{$I DI.inc}

interface

uses
  DISystemCompat,
  {$IFDEF HAS_UNITSCOPE}
  System.Classes, System.IniFiles,
  {$ELSE HAS_UNIT_SCOPE}
  Classes, IniFiles,
  {$ENDIF HAS_UNIT_SCOPE}
  DISQLite3IniFileDb;

type

  TDISQLite3IniFile = class(TCustomIniFile)
  private
    FDb: TDISQLite3IniFileDb;
  public
    constructor Create(
      const AFileName: string);
    constructor CreateW(
      const AFileName: UnicodeString);
    destructor Destroy; override;
    procedure Clear;
    procedure DeleteKey(
      const ASection: string;
      const AKey: string); override;
    procedure EraseSection(
      const ASection: string); override;

    procedure Import(
      const AIniFile: TCustomIniFile);
    function ReadInteger(
      const ASection: string;
      const AKey: string;
      ADefault: Integer): Integer; override;
    procedure ReadSection(
      const ASection: string;
      AStrings: TStrings); override;
    procedure ReadSections(
      AStrings: TStrings); override;
    procedure ReadSectionValues(
      const ASection: string;
      AStrings: TStrings); override;
    function ReadString(
      const ASection, AKey: string;
      const ADefault: string): string; override;
    function SectionExists(
      const ASection: string): Boolean;
    procedure UpdateFile; override;
    function ValueExists(
      const ASection: string;
      const AKey: string): Boolean; {$IFDEF COMPILER_9_UP} override; {$ENDIF}
    procedure WriteInteger(
      const ASection: string;
      const AKey: string;
      AValue: Integer); override;
    procedure WriteString(
      const ASection: string;
      const AKey: string;
      const AValue: string); override;

    property DB: TDISQLite3IniFileDb read FDb;
  end;

implementation

constructor TDISQLite3IniFile.Create(const AFileName: string);
begin
  CreateW(AFileName);
end;

constructor TDISQLite3IniFile.CreateW(const AFileName: UnicodeString);
begin
  inherited Create(AFileName);
  FDb := TDISQLite3IniFileDb.Create(AFileName);
end;

destructor TDISQLite3IniFile.Destroy;
begin
  FDb.Free;
  inherited;
end;

procedure TDISQLite3IniFile.Clear;
begin
  FDb.Clear;
end;

procedure TDISQLite3IniFile.DeleteKey(const ASection, AKey: string);
begin
  FDb.DeleteKey(ASection, AKey);
end;

procedure TDISQLite3IniFile.EraseSection(const ASection: string);
begin
  FDb.EraseSection(ASection);
end;

procedure TDISQLite3IniFile.Import(const AIniFile: TCustomIniFile);
var
  iSection: Integer;
  Section: string;
  Sections: TStringList;
  iKey: Integer;
  Key: string;
  Keys: TStringList;
  Value: string;
begin
  FDb.BeginUpdate;
  try
    Sections := TStringList.Create;
    Keys := TStringList.Create;
    try
      AIniFile.ReadSections(Sections);
      for iSection := 0 to Sections.Count - 1 do
        begin
          Section := Sections[iSection];
          Keys.Clear;
          AIniFile.ReadSection(Section, Keys);
          for iKey := 0 to Keys.Count - 1 do
            begin
              Key := Keys[iKey];
              Value := AIniFile.ReadString(Section, Key, '');
              FDb.WriteString(Section, Key, Value);
            end;
        end;
    finally
      Keys.Free;
      Sections.Free;
    end;
  finally
    FDb.EndUpdate;
  end;
end;

function TDISQLite3IniFile.ReadInteger(const ASection, AKey: string; ADefault: Integer): Integer;
begin
  Result := FDb.ReadInteger(ASection, AKey, ADefault);
end;

procedure TDISQLite3IniFile.ReadSection(const ASection: string; AStrings: TStrings);
begin
  FDb.ReadSection(ASection, AStrings);
end;

procedure TDISQLite3IniFile.ReadSections(AStrings: TStrings);
begin
  FDb.ReadSections(AStrings);
end;

procedure TDISQLite3IniFile.ReadSectionValues(const ASection: string; AStrings: TStrings);
begin
  FDb.ReadSectionValues(ASection, AStrings);
end;

function TDISQLite3IniFile.ReadString(const ASection, AKey, ADefault: string): string;
begin
  Result := FDb.ReadString(ASection, AKey, ADefault);
end;

function TDISQLite3IniFile.SectionExists(const ASection: string): Boolean;
begin
  Result := FDb.SectionExists(ASection);
end;

procedure TDISQLite3IniFile.UpdateFile;
begin

end;

function TDISQLite3IniFile.ValueExists(const ASection, AKey: string): Boolean;
begin
  Result := FDb.KeyExists(ASection, AKey);
end;

procedure TDISQLite3IniFile.WriteInteger(const ASection, AKey: string; AValue: Integer);
begin
  FDb.WriteInteger(ASection, AKey, AValue);
end;

procedure TDISQLite3IniFile.WriteString(const ASection, AKey, AValue: string);
begin
  FDb.WriteString(ASection, AKey, AValue);
end;

end.

