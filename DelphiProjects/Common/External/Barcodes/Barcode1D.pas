unit Barcode1D;

interface

uses
  WinProcs, WinTypes, Messages, SysUtils, Classes, Graphics, Dialogs,Controls,Math;

type
  TVectorString103 = array[0..102] of String;
  TVectorString10 = array[0..9] of String;
  TVectorString128 = array[0..127] of String;
  TMatrix10x2String = array[0..9,0..1] of String;
  TMatrix50x2String = array[0..49,0..1] of String;
  TVectorByte = array of byte;

	TSymbology =(CODE39,CODE39EXT,INTERLEAVED25,CODE11,CODABAR,MSI,UPCA,IND25,MAT25,CODE93,EAN13,EAN8,UPCE,CODE128,CODE93EXT,POSTNET,EAN128);

	{ code 39 table}   {44x2}
	const set39:TMatrix50x2String=(('0','nnnwwnwnn'),('1','wnnwnnnnw'),('2','nnwwnnnnw'),('3','wnwwnnnnn'),('4','nnnwwnnnw'),('5','wnnwwnnnn'),('6','nnwwwnnnn'),('7','nnnwnnwnw'),('8','wnnwnnwnn'),('9','nnwwnnwnn'),('A','wnnnnwnnw'),('B','nnwnnwnnw'),('C','wnwnnwnnn'),('D','nnnnwwnnw'),('E','wnnnwwnnn'),('F','nnwnwwnnn'),('G','nnnnnwwnw'),('H','wnnnnwwnn'),('I','nnwnnwwnn'),('J','nnnnwwwnn'),('K','wnnnnnnww'),('L','nnwnnnnww'),('M','wnwnnnnwn'),('N','nnnnwnnww'),('O','wnnnwnnwn'),('P','nnwnwnnwn'),('Q','nnnnnnwww'),('R','wnnnnnwwn'),('S','nnwnnnwwn'),('T','nnnnwnwwn'),('U','wwnnnnnnw'),('V','nwwnnnnnw'),('W','wwwnnnnnn'),('X','nwnnwnnnw'),('Y','wwnnwnnnn'),('Z','nwwnwnnnn'),('-','nwnnnnwnw'),('.','wwnnnnwnn'),(' ','nwwnnnwnn'),('$','nwnwnwnnn'),('/','nwnwnnnwn'),('+','nwnnnwnwn'),('%','nnnwnwnwn'),('*','nwnnwnwnn'),('',''),('',''),('',''),('',''),('',''),('',''));
	{ code 2 of 5 table. }
	const set25:TMatrix10x2String=(('0','nnwwn'),('1','wnnnw'),('2','nwnnw'),('3','wwnnn'),('4','nnwnw'),('5','wnwnn'),('6','nwwnn'),('7','nnnww'),('8','wnnwn'),('9','nwnwn'));
	{ code MSI table. }
	const setMSI:TMatrix10x2String=(('0','nwnwnwnw'),('1','nwnwnwwn'),('2','nwnwwnnw'),('3','nwnwwnwn'),('4','nwwnnwnw'),('5','nwwnnwwn'),('6','nwwnwnnw'),('7','nwwnwnwn'),('8','wnnwnwnw'),('9','wnnwnwwn'));
	{ code 11 table.} {11x2}
	const set11:TMatrix50x2String=(('0','nnnnw'),('1','wnnnw'),('2','nwnnw'),('3','wwnnn'),('4','nnwnw'),('5','wnwnn'),('6','nwwnn'),('7','nnnww'),('8','wnnwn'),('9','wnnnn'),('-','nnwnn'),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''));
	{ CODABAR table.}  {20x2}
  const setCODABAR:TMatrix50x2String=(('0','nnnnnww'),('1','nnnnwwn'),('2','nnnwnnw'),('3','wwnnnnn'),('4','nnwnnwn'),('5','wnnnnwn'),('6','nwnnnnw'),('7','nwnnwnn'),('8','nwwnnnn'),('9','wnnwnnn'),('-','nnnwwnn'),('$','nnwwnnn'),(':','wnnnwnw'),('/','wnwnnnw'),('.','wnwnwnn'),('+','nnwnwnw'),('A','nnwwnwn'),('B','nwnwnnw'),('C','nnnwnww'),('D','nnnwwwn'),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''),('',''));
	{ code 93 table.}    {47x2}
	const set93:TMatrix50x2String=(('0','131112'),('1','111213'),('2','111312'),('3','111411'),('4','121113'),('5','121212'),('6','121311'),('7','111114'),('8','131211'),('9','141111'),('A','211113'),('B','211212'),('C','211311'),('D','221112'),('E','221211'),('F','231111'),('G','112113'),('H','112212'),('I','112311'),('J','122112'),('K','132111'),('L','111123'),('M','111222'),('N','111321'),('O','121122'),('P','131121'),('Q','212112'),('R','212211'),('S','211122'),('T','211221'),('U','221121'),('V','222111'),('W','112122'),('X','112221'),('Y','112121'),('Z','123111'),('-','121131'),('.','311112'),(' ','311211'),('$','321111'),('/','112131'),('+','113121'),('%','211131'),('_1','121211'),('_2','312111'),('_3','311121'),('_4','122211'),('',''),('',''),('',''));
	{ code UPCA left table.}
  const setUPCALeft:TMatrix10x2String=(('0','3211'),('1','2221'),('2','2122'),('3','1411'),('4','1132'),('5','1231'),('6','1114'),('7','1312'),('8','1213'),('9','3112'));
	{ code UPCA right table.}
  const setUPCARight:TMatrix10x2String=(('0','3211'),('1','2221'),('2','2122'),('3','1411'),('4','1132'),('5','1231'),('6','1114'),('7','1312'),('8','1213'),('9','3112'));
	{ code UPCE odd table.}
  const setUPCEOdd:TMatrix10x2String=(('0','3211'),('1','2221'),('2','2122'),('3','1411'),('4','1132'),('5','1231'),('6','1114'),('7','1312'),('8','1213'),('9','3112'));
	{ code UPCE even table.}
  const setUPCEEven:TMatrix10x2String=(('0','1123'),('1','1222'),('2','2212'),('3','1141'),('4','2311'),('5','1321'),('6','4111'),('7','2131'),('8','3121'),('9','2113'));
	{ code 39 extended table.}
  const set39Ext:TVectorString128 =('%U','$A','$B','$C','$D','$E','$F','$G','$H','$I','$J','$K','$L','$M','$N','$O','$P','$Q','$R','$S','$T','$U','$V','$W','$X','$Y','$Z','%A','%B','%C','%D','%E',' ','/A','/B','/C','/D','/E','/F','/G','/H','/I','/J','/K','/L','-','.','/O','0','1','2','3','4','5','6','7','8','9','/Z','%F','%G','%H','%I','%J','%V','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','%K','%L','%M','%N','%O','%W','+A','+B','+C','+D','+E','+F','+G','+H','+I','+J','+K','+L','+M','+N','+O','+P','+Q','+R','+S','+T','+U','+V','+W','+X','+Y','+Z','%P','%Q','%R','%S','%T');
  {code 93 extended table.}
  const set93Ext:TVectorString128 =('_2U','_1A','_1B','_1C','_1D','_1E','_1F','_1G','_1H','_1I','_1J','_1K','_1L','_1M','_1N','_1O','_1P','_1Q','_1R','_1S','_1T','_1U','_1V','_1W','_1X','_1Y','_1Z','_2A','_2B','_2C','_2D','_2E',' ','_3A','_3B','_3C','_3D','_3E','_3F','_3G','_3H','_3I','_3J','_3K','_3L','-','.','_3O','0','1','2','3','4','5','6','7','8','9','_3Z','_2F','_2G','_2H','_2I','_2J','_2V','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','_2K','_2L','_2M','_2N','_2O','_2W','_4A','_4B','_4C','_4D','_4E','_4F','_4G','_4H','_4I','_4J','_4K','_4L','_4M','_4N','_4O','_4P','_4Q','_4R','_4S','_4T','_4U','_4V','_4W','_4X','_4Y','_4Z','_2P','_2Q','_2R','_2S','_2T');
  { code UPCE system 0 table.}
  const UPCESystem0:TVectorString10 =('EEEOOO','EEOEOO','EEOOEO','EEOOOE','EOEEOO','EOOEEO','EOOOEE','EOEOEO','EOEOOE','EOOEOE');
  { code UPCE system 1 table.}
  const UPCESystem1:TVectorString10=('OOOEEE','OOEOEE','OOEEOE','OOEEEO','OEOOEE','OEEOOE','OEEEOO','OEOEOE','OEOEEO','OEEOEO');
  { code EAN left A table.}
  const setEANLeftA:TMatrix10x2String=(('0','3211'),('1','2221'),('2','2122'),('3','1411'),('4','1132'),('5','1231'),('6','1114'),('7','1312'),('8','1213'),('9','3112'));
  { code EAN left B table.}
  const setEANLeftB:TMatrix10x2String=(('0','1123'),('1','1222'),('2','2212'),('3','1141'),('4','2311'),('5','1321'),('6','4111'),('7','2131'),('8','3121'),('9','2113'));
  { code EAN right table.}
  const setEANRight:TMatrix10x2String=(('0','3211'),('1','2221'),('2','2122'),('3','1411'),('4','1132'),('5','1231'),('6','1114'),('7','1312'),('8','1213'),('9','3112'));
  { code EAN  table.}
  const setEANCode:TVectorString10=('AAAAA','ABABB','ABBAB','ABBBA','BAABB','BBAAB','BBBAA','BABAB','BABBA','BBABA');
  { 5 digit supplement table.}
  const fiveSuplement:TVectorString10=('EEOOO','EOEOO','EOOEO','EOOOE','OEEOO','OOEEO','OOOEE','OEOEO','OEOOE','OOEOE');
  { code 128 table.}
	const set128:TVectorString103=('212222','222122','222221','121223','121322','131222','122213','122312','132212','221213','221312','231212','112232','122132','122231','113222','123122','123221','223211','221132','221231','213212','223112','312131','311222','321122','321221','312212','322112','322211','212123','212321','232121','111323','131123','131321','112313','132113','132311','211313','231113','231311','112133','112331','132131','113123','113321','133121','313121','211331','231131','213113','213311','213131','311123','311321','331121','312113','312311','332111','314111','221411','431111','111224','111422','121124','121421','141122','141221','112214','112412','122114','122411','142112','142211','241211','221114','413111','241112','134111','111242','121142','121241','114212','124112','124211','411212','421112','421211','212141','214121','412121','111143','111341','131141','114113','114311','411113','411311','113141','114131','311141','411131');
  { code 128, character set A}
	const set128A:TVectorString103=(' ','!','\','#','$','%','&','''','(',')','*','+',',','-','.','/','0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?','@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','[','\',']','^','_',chr(0),chr(1),chr(2),chr( 3),chr(4),chr(5),chr(6),chr(7),chr(8),chr(9),chr(10),chr(11),chr(12),chr(13),chr(14),chr(15),chr(16),chr(17),chr(18),chr(19),chr(20),chr(21),chr(22),chr(23),chr(24),chr(25),chr(26),chr( 27),chr(28),chr(29),chr(30),chr(31),'_96','_97','_98','_99','_100','_101','_102');
  { code 128, character set B}
	const set128B:TVectorString103=(' ','!','\','#','$','%','&','''','(',')','*','+',',','-','.','/','0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?','@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','[','\',']','^','_','`','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','(','_92',')','~','_95','_96','_97','_98','_99','_100','_101','_102');
  { code 128, character set C }
  const set128C:TVectorString103=('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64','65','66','67','68','69','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89','90','91','92','93','94','95','96','97','98','99','_100','_101','_102');
  { code POSTNET table}
  const setPOSTNET:TMatrix10x2String=(('0','11000'),('1','00011'),('2','00101'),('3','00110'),('4','01001'),('5','01010'),('6','01100'),('7','10001'),('8','10010'),('9','10100'));


	type TBarcode1D = class(TGraphicControl)
	private
    {resize the image when painting on the screen to fit the size of the control}
    FAutoFit:boolean;
	  { Private Declarations }
    FCurrentY:Integer;
    FCurrentX:Integer;
    FTopMargin:Integer;
    FLeftMargin:Integer;
    FOffsetX:integer;
    FOffsetY:integer;
    FBarHeightPixels:Integer;
    FcurrentCanvas:TCanvas;
	  FBackColor: TColor;
	  FBarColor:TColor;
    FProcessTilde:boolean;
    FSizeCalculated: boolean;

    { where to put the text}
    FtextOnTop:boolean;
	  { type of barcode.}
	  FbarType:TSymbology;
	  { text to be painted as barcode.}
    Fcode:String;
    { user defined 2 or 5 digit supplement for EAN or UPC codes.}
	  FcodeSup:String;
    { if true, the checksum character will be calculated and appended to the code.}
	  FcheckCharacter:boolean;
    { height in pixels of POSTNET's tall bars.}
	  FpostnetHeightTallBar:integer;
    { height in pixels of POSTNET's short bars.}
    FpostnetHeightShortBar:integer;
    { left margin of the barcode (also know as quite zone). The default is 05. CM. }
    FleftGuardBar:Integer;
    FcenterGuardBarStart:Integer;
    FcenterGuardBarEnd:Integer;
    FrightGuardBar:Integer;
    FendOfCode:Integer;
    FstartSuplement:Integer;
    FendSuplement:Integer;
    FsuplementTopMargin:Integer;
    Fsupplement:String; // user defined supplement
    { if true (default), guardbars in EAN and UPC codes will be longer than data bars.}
    FguardBars:boolean;

    { text after encoding. It will contain the appenden checksum, if calculated.}
    FcodeText:String;
    FnarrowBarPixels:Integer;
    FwidthBarPixels:Integer;

    { font of the text.}
    FFont:TFont;
    { color of the text}
    FextraHeight:Integer;

    { system to be used in UPCE. It can be "0" or "1". The default is "1". }
    FUPCESytem:String;
    { start character for CODABAR. The default is A. }
    FCODABARStartChar:String;
    {stop character for CODABAR. The default is B. }
    FCODABARStopChar:String;
    { 2 digit supplement for EAN or UPC codes. }
    FUPCEANSupplement2:boolean;
    { 5 digit supplement for EAN or UPC codes. }
    FUPCEANSupplement5:boolean;
    { set of character to be used in code 128. Possible values are "A", "B" or "C" (only for numeric codes). The default is "B"     }
    FCode128Set:String;

    { size in pixels of modules (narrow bars or spaces). The resolution is used to converto to pixels. }
    FX:integer; //  , X-dimension
    { multiplicator value for width bars. A value of 2 (default) means that wide bars will be 2*N CM width. }
    FN:integer; // multiple value for width bars
    { space between 2 characters (code 39). This a multiplicator of X. The default is 1. }
    FI:integer; // space between 2 chars, multiple of X
    { height of bars.  This a multiplicator of X. The default is 0.45. }
    FH:double; // height, multiple of the length
    {	 Symbol lentgh in pixels . This is calculated by RBarcode and the user can read it. }
    FL:integer;// symbol length
    { Separation in pixels between the barcode and the supplement }
    FsupSeparation:integer;
    { height of the supplement. This is a multiplicator of the height of the code. The default is 0.8 (80%).}
    FsupHeight:double;
    FShowText: boolean;
    FVerticalOrientation: boolean;
    procedure SetVerticalOrientation(const Value: boolean);
	protected

//          procedure addBarWithHeight(w:Integer;black:boolean;Y:Integer;Height:Integer);
//          procedure paintCharWithHeight(patternColor:String;patternBars:String;Y:integer;Height:integer);
    function findChar10(table:TMatrix10x2String; c:String):integer;
    function findChar50(table:TMatrix50x2String; c:String):integer;

    procedure addBar(w:integer;black:boolean;bartopmargin:integer);
    procedure paintPostNetChar(pattern:String);
    procedure paintPOSTNET();
    procedure paintInterleaved25();
    procedure paintIND25();
    function toString(i:integer):String;
    function toInteger( s:String):integer;
    function UPCEANCheck(s:String):String;
    procedure paintUPCA();
    procedure paintEAN13();
    function findInArray103(s1:TVectorString103;c:string):integer;
    function convertCode128ControlChar(c:String):string;
    function isDigit(c:String):boolean;
    function getNextLowerCase(s:string;i:integer):integer;
    function  getNextControlChar(s:string;i:integer):integer;
    function  getEvenNumberDigits(s:string;i:integer):boolean;
    function calculateNextSet(s:string;i:integer):String;
    procedure paintCode128();
    procedure paintEAN128();
    procedure paintEAN8();
    procedure paintUPCE();
    procedure paintSup2(chars:String);
    procedure paintSup5(chars:String);
    procedure paintMAT25();
    procedure paintCODE11();
    procedure paintBAR39();
    procedure paintCODABAR();
    procedure paintMSI();
    procedure paintBAR39Ext();
    procedure paintBAR93();
    procedure paintBAR93Ext();
    procedure paintChar(patternColor:String;patternBars:String);
    procedure paintChar2(patternColor:String;patternBars:String;bartopmargin:integer);
    procedure paintGuardChar(patternColor:String;patternBars:String;bartopmargin:integer);
    procedure calculateSizes();
    procedure fillRect(ca:TCanvas;x:integer;y:integer;w:integer;h:integer);
    function applyTilde(code:String):String;
    procedure Paint; override;
//          procedure Print(OfsX,OfsY:Integer); override;
    function makeLength(c:String;l:integer):String;
	public
	published
	  { Published  Declarations }
	  constructor Create(Owner:TComponent); override;
    property AutoFit:boolean read FAutoFit write FAutoFit;
    property offsetX:Integer read FoffsetX write FoffsetX;
    property offsetY:Integer read FoffsetY write FoffsetY;
    property Code : String read FCode write FCode;
    property ProcessTilde:boolean read FProcessTilde write FProcessTilde;
    property TopMargin : integer read FTopMargin write FTopMargin default 0;
    property LeftMargin : integer read FLeftMargin write FLeftMargin default 0;
    property BarHeightPixels : integer read FBarHeightPixels write FBarHeightPixels default 7;
    property BackColor:TColor read FBackColor write FBackColor default clWhite;
	  property BarColor:TColor read FBarColor write FBarColor default clBlack;

    procedure paintBarcode(C: TCanvas);
    procedure paintBarcodeAutoSize(c:TCanvas);
    procedure PaintToCanvas(c:TCanvas);

    function GetMetafilePicture(ReferenceDevice: HDC): TMetafile;
    function GetActualSize(ReferenceDevice: HDC): TSize;

    { where to put the text}
    property textOnTop:boolean read FtextOnTop write FtextOnTop;
	  { type of barcode.}
	  property barType:TSymbology read FbarType write FbarType;
	  { text to be painted as barcode.}
    { user defined 2 or 5 digit supplement for EAN or UPC codes.}
	  property codeSup:String read FcodeSup write FcodeSup;
    { if true, the checksum character will be calculated and appended to the code.}
	  property checkCharacter:boolean read FcheckCharacter write FcheckCharacter;
    { height in pixels of POSTNET's tall bars. }
	  property postnetHeightTallBar:integer read FpostnetHeightTallBar write FpostnetHeightTallBar;
    { height in pixels of POSTNET's short bars. }
    property postnetHeightShortBar:integer read FpostnetHeightShortBar write FpostnetHeightShortBar;
{ left margin of the barcode (also know as quite zone). The default is 05. CM. }
//         property leftGuardBar:Integer read FleftGuardBar write FleftGuardBar;
//	property centerGuardBarStart:Integer read FcenterGuardBarStart write FcenterGuardBarStart;
//	property centerGuardBarEnd:Integer read FcenterGuardBarEnd write FcenterGuardBarEnd;
//	property rightGuardBar:Integer read FrightGuardBar write FrightGuardBar;
//	property endOfCode:Integer read FendOfCode write FendOfCode;
//	property startSuplement:Integer read FstartSuplement write FstartSuplement;
//	property endSuplement:Integer read FendSuplement write FendSuplement;
//	property suplementTopMargin:Integer read FsuplementTopMargin write FsuplementTopMargin;
    property supplement:String read Fsupplement write Fsupplement; { user defined supplement}
    { if true (default), guardbars in EAN and UPC codes will be longer than data bars.}
    property guardBars:boolean read FguardBars write FguardBars;
   { text after encoding. It will contain the appenden checksum, if calculated.}
    property codeText:String read FcodeText write FcodeText;
  //	property narrowBarPixels:Integer read FnarrowBarPixels write FnarrowBarPixels;
  //	property widthBarPixels:Integer read FwidthBarPixels write FwidthBarPixels;
    { font of the text.}
    property Font:TFont read FFont write FFont;
    { color of the text}
    property extraHeight:Integer read FextraHeight write FextraHeight;

    { system to be used in UPCE. It can be "0" or "1". The default is "1". }
    property UPCESytem:String read FUPCESytem write FUPCESytem;
    { start character for CODABAR. The default is A. }
    property CODABARStartChar:String read FCODABARStartChar write FCODABARStartChar;
    {stop character for CODABAR. The default is B. }
    property CODABARStopChar:String read FCODABARStopChar write FCODABARStopChar;
    { 2 digit supplement for EAN or UPC codes. }
    property UPCEANSupplement2:boolean read FUPCEANSupplement2 write FUPCEANSupplement2;
    { 5 digit supplement for EAN or UPC codes. }
    property UPCEANSupplement5:boolean read FUPCEANSupplement5 write FUPCEANSupplement5;
    { set of character to be used in code 128. Possible values are "A", "B" or "C" (only for numeric codes).
    The default is "B" }
    property Code128Set:String read FCode128Set write FCode128Set;

    { size in pixels of modules (narrow bars or spaces). The resolution is used to converto to pixels.}
    property X:integer read FX write FX; //  X-dimension
    { multiplicator value for width bars. A value of 2 (default) means that wide bars will be 2*N width.}
    property N:integer read FN write FN; // multiple value for width bars
    { space between 2 characters (code 39). This a multiplicator of X. The default is 1.}
    property I:integer read FI write FI; // space between 2 chars, multiple of X
    { height of bars.  This a multiplicator of L. The default is 0.45. }
    property H:double read FH write FH; // height, multiple of the length
    {	 Symbol lentgh . This is calculated by RBarcode and the user can read it. }
    property L:integer read FL write FL;// symbol length


    { Separation in CN between the barcode and the supplement }
    property supSeparation:integer read FsupSeparation write FsupSeparation;
    { height of the supplement. This is a multiplicator of the height of the code. The default is 0.8 (80%).}
    property supHeight:double read FsupHeight write FsupHeight;

		property CurrentX : integer read FCurrentX;
		property CurrentY : integer read FCurrentY;

    property ShowText: boolean read FShowText write FShowText;
    property VerticalOrientation: boolean read FVerticalOrientation write SetVerticalOrientation;
	end;

procedure Register;

implementation

{$R *.dcr}

uses rwGraphics;

procedure Register;
begin
  RegisterComponents('ISClasses', [TBarcode1D]);
end;

procedure show1(s:String;i:integer);
var
 s1:String;
begin

 str(i,s1);
 s1:=s+' '+s1;

 MessageDlg(s1, mtInformation, [mbOk], 0);

end;
{
procedure TBarcode1D.Print(OfsX,OfsY:Integer);
var
w:integer;
h:integer;
u:integer;
begin

     offsetX:=ParentReport.QRPrinter.XPos(OfsX+Size.Left);
     offsety:=ParentReport.QRPrinter.Ypos(OfsY+Size.Top);

      w:= round(size.width);
      //show1('sw ',w);
      h:= round(size.height);
      //show1('sh ',h);
      w:= width;
      //show1('w ',w);
      h:= height;
      //show1('h ',h);
      u:=ord(ParentReport.Units);
     // show1('unit ',u);

     // this is a preview
     //if (ParentReport.state<>qrPrint) then  paintBarcodeAutosize(ParentReport.QRPrinter.Canvas)
     // printing
     //else     begin

              paintBarcode(ParentReport.QRPrinter.Canvas);
           //   MessageDlg('print', mtInformation, [mbOk], 0);
     //end;

     offsetX:= 0;
     offsety:=0;

    // MessageDlg('PPP', mtInformation, [mbOk], 0);

end;
}
procedure TBarcode1D.paintBarcodeAutosize(c:TCanvas);
var
  MetaFile, RotatedMetafile: TMetafile;
  MetafileCanvas: TMetafileCanvas;
  requiredWith:integer;
  requiredHeight:integer;
  rect:TRect;
  oldVertical: boolean;
  oldLeftMargin, oldTopMargin: integer;
  oldBackColor: TColor;
begin
   // test paint to find out required size
   oldVertical := FVerticalOrientation;
   oldBackColor := BackColor;
   FVerticalOrientation := false;
   try
     MetaFile:=TMetafile.create();
     try
       MetaFile.width:=100;
       MetaFile.height:=100;

       MetafileCanvas := TMetafileCanvas.Create(MetaFile, c.handle);
       try
         paintBarcode(MetafileCanvas);
       finally
         FreeAndNil(MetafileCanvas);
       end;
       requiredWith:=FcurrentX;
       requiredHeight:=FcurrentY;
     finally
       FreeAndNil(MetaFile);
     end;

     // paint now on metafile of exact size
     MetaFile:=TMetafile.create();
     try
       MetaFile.width:=requiredWith + FLeftMargin;
       MetaFile.height:=requiredHeight + FTopMargin;

       MetafileCanvas := TMetafileCanvas.Create(MetaFile, c.handle);
       try
         oldLeftMargin := FLeftMargin;
         oldTopMargin := FTopMargin;
         if oldVertical then
         begin
           FTopMargin := 0;
           FLeftMargin := 0;
           BackColor := clNone;
         end;
         try
           paintBarcode(MetafileCanvas);
         finally
           FTopMargin := oldTopMargin;
           FLeftMargin := oldLeftMargin;
           if oldVertical then
           begin
             FCurrentY := FCurrentY + FTopMargin;
             FCurrentX := FCurrentX + FLeftMargin;
           end;
         end;
       finally
         FreeAndNil(MetafileCanvas);
       end;

       rect.top:=0;
       rect.left:=0;
       rect.bottom:=height;
       rect.right:=width;

       if not oldVertical then
         C.StretchDraw(rect, MetaFile)
       else
       begin
         RotatedMetafile := NTRotateMetafile90ClockWise(MetaFile.Handle, Metafile.Width, Metafile.Height, C.Handle);
         try
           if oldBackColor <> clNone then
           begin
             C.Brush.Color:= oldBackColor;
             C.Brush.Style := bsSolid;
             C.FillRect(Rect);
           end;
           Rect.Left := Rect.Left + FTopMargin;
           Rect.Right := Rect.Right + FTopMargin;
           Rect.Top := Rect.Top + FLeftMargin;
           Rect.Bottom := Rect.Bottom + FLeftMargin;
           C.StretchDraw(rect, RotatedMetafile);
         finally
           FreeAndNil(RotatedMetafile);
         end;
       end;
     finally
       FreeAndNil(MetaFile);
     end;
   finally
     FVerticalOrientation := oldVertical;
     BackColor := oldBackColor;
   end;
end;

procedure TBarcode1D.Paint;
begin
  PaintToCanvas(Canvas);
end;

{ adds a bar to the bar code at the currentX position.}
procedure TBarcode1D.addBar(w:integer;black:boolean;bartopmargin:integer);
var
a,b,c,d:TPoint;
h,x,y:integer;
begin

		if (black) then begin
                     FCurrentCanvas.pen.width:=1;
                     FCurrentCanvas.pen.Color:=barColor;
                     FCurrentCanvas.brush.color:=barColor;
                     FCurrentCanvas.brush.style:=bsSolid;
                     x:=FCurrentX;
                     y:=(topMargin+offsetY)+bartopmargin;
                     h:=barHeightPixels+extraHeight-bartopmargin;

                     a.x:=x;
                     a.y:=y;
                     b.x:=x+w-1;
                     b.y:=y;
                     c.x:=x+w-1;
                     c.y:=y+h;
                     d.x:=x;
                     d.y:=y+h;
                     FCurrentCanvas.polygon([a,b,c,d]);

		end;

		{ move pointer   }
		FCurrentX:=FCurrentX+w;

end;

procedure TBarcode1D.fillRect(ca:TCanvas;x:integer;y:integer;w:integer;h:integer);
var
a,b,c,d:TPoint;
begin
     ca.pen.width:=1;
     ca.brush.style:=bsSolid;
     a.x:=x;
     a.y:=y;
     b.x:=x+w-1;
     b.y:=y;
     c.x:=x+w-1;
     c.y:=y+h;
     d.x:=x;
     d.y:=y+h;
     ca.polygon([a,b,c,d]);
end;

{ paints a Postnet char.}
procedure TBarcode1D.paintPostNetChar(pattern:String);
var
cBar:String;
shortBarPixels:integer;
tallBarPixels:integer;
diff:integer;
i:integer;
begin

		shortBarPixels:=postnetHeightShortBar;
		tallBarPixels:=postnetHeightTallBar;
                diff:=(tallBarPixels-shortBarPixels);


                 FCurrentCanvas.pen.Color:=barColor;
                 FCurrentCanvas.brush.color:=barColor;

		for i:=0 to length(pattern)-1 do begin

			cBar:=pattern[i+1];

			if (cBar='0') then fillRect(FCurrentCanvas,FCurrentX,(topMargin+offsetY)+diff,FnarrowBarPixels,shortBarPixels+extraHeight);
			if (cBar='1') then fillRect(FCurrentCanvas,FCurrentX,(topMargin+offsetY),FnarrowBarPixels,tallBarPixels+extraHeight);

			{ bar}
                        FCurrentX:=FCurrentX+FnarrowBarPixels;

			{ space     }
			FCurrentX:=FCurrentX+FwidthBarPixels;
		end;

end;


{ paints a Postnet code.}
procedure TBarcode1D.paintPOSTNET();
var
pos:integer;
i:integer;
c:String;
sum:integer;
codetmp:String;
checksum:integer;
tmpStr:string;
begin
{ Postnet codes have a trailing checksum that is the Modulo 10 of the sum of the digits.
So to get the checksum, add all the digits in the code and subtract that sum from the
	next multiple of 10. The checksum for 32501 would be 9 (3+2+5+0+1=11; 20-11=9).}



	  codetmp:=code;
          sum:=0;

	  { paint start char}
	  paintPostNetChar('1');

	  // calculate checksum
	  for i:=length(code)-1 downto 0 do begin
		  c:=''+code[i+1];
		  sum:=sum+findChar10(setPOSTNET,c);
	  end;

	  checksum:=(sum mod 10);
	  if (checksum<>0) then checksum:=10-checksum;

	  { add checksum char}
	  if (checkCharacter) then begin
                               str(checksum,tmpStr);
                              codetmp:=codetmp+tmpStr;
          end;

	  { paint char in string}
	  for i:=0 to length(codetmp)-1 do begin
		  c:=codetmp[i+1];
		  pos:=findChar10(setPOSTNET,c);
		  paintPostNetChar(setPOSTNET[pos,1]);
	  end;


	  { paint stop char}
	  paintPostNetChar('1');

	end;


{ find a char in a charset table}
function TBarcode1D.findChar10(table:TMatrix10x2String; c:String):integer;
var
i:integer;

begin

		for i:=0 to length(table)-1 do
			if (c=table[i,0]) then begin
                           result:=i;
                           exit;
                        end;


		result:=-1;
end;


{ find a char in a charset table}
function TBarcode1D.findChar50(table:TMatrix50x2String; c:String):integer;
var
i:integer;

begin

		for i:=0 to length(table)-1  do
			if (c=table[i,0]) then begin
                           result:=i;
                           exit;
                        end;


		result:=-1;
end;

{ paints an interleaved 2 of 5 code.}
procedure TBarcode1D.paintInterleaved25();
var
pos:integer;
pos2:integer;
c:string;
c2:string;
codetmp:string;
l:string;
sumeven:integer;
sumodd:integer;
even:boolean;
checksum:integer;
i:integer;
j:integer;
tmpstr:String;
begin

{    Interleaved 2 of 5 code is a numeric only bar code.
    Each character of this code is represented by five elements, two
    wide and three narrow. Wide elements are decoded as binary one (1),
    and narrow elements are decoded as binary zero (0). The wide to narrow
    element ratio should be between two and three.
    Whether or not the elements used to encode a character are bars or
    spaces depends upon the location
    of the character within the message. The first character of the message is encoded
    into the bars immediately following the start character. The second character
    of the message is encoded into the spaces between the bars of the first character,
    thus eliminating the intercharacter space. Because of this, Interleaved 2 of 5 is
    a continuous bar code.

    Due to the interleaving of the characters, the number of characters in an
    Interleaved 2 of 5 message must be even. The check character, if used, must
    be included in the character count. If the message has an odd number of characters, add a leading zero (0) to the message.


    Interleaved 2 of 5 has an optional modulus 10 check character, which is printed at the end of the message. The value of the check character is determined by the following six step procedure.

    Identify even and odd positioned characters in the message with the rightmost data character always defined as an even positioned character.
    - Sum the numeric values of the odd positioned characters.
    - Sum the numeric values of the even positioned characters and multiply this total by three.
    - Sum the odd and even totals from steps two and three.
    - Determine the smallest number which, when added to the sum in step four, will result in a multiple of ten. This number is the value of the check character.
    - Determine if the number of characters (message plus check character) is even or odd. If it is odd, add a leading, nonsignificant zero to the message to produce an even number of characters.
    }



    codetmp:=code;

    { paint start char}
    paintChar('bwbw','nnnn');


     { leading 0 needed?}
     l:='';
     if ((length(code) mod 2)=0) and (checkCharacter) then codetmp:='0'+code;
     if ((length(code) mod 2)=1) and (not checkCharacter) then codetmp:='0'+code;

     {	   calculate checksum }
     sumeven:=0;
     sumodd:=0;
     even:=true;
     for i:=length(codetmp)-1 downto 0 do begin
		  c:=''+codetmp[i+1];
		  if (even) then sumeven:=sumeven+findChar10(set25,c)
		  else sumodd:=sumodd+findChar10(set25,c);
		  even:=(not even)
     end;

     checksum:=(sumeven*3)+sumodd;
     checksum:=(checksum mod 10);
     if (checksum<>0) then checksum:=10-checksum;

     { 48 is the asc value of the char 0}
     if (checkCharacter) then begin
        str(checksum,tmpstr);
        codetmp:=codetmp+tmpstr;
     end;

	  { paint char in string}
          i:=0;
          while( i<length(codetmp)) do begin
		  c:=''+codetmp[i+1];
		  c2:=''+codetmp[i+2];
		  pos:=findChar10(set25,c);
		  pos2:=findChar10(set25,c2);

		  { draw bar of first char and space of second char, all 5 components per char}
                  if ((pos>=0) and (pos2>=0)) then begin
                    for j:=0 to 4 do begin
			   paintChar('b',''+set25[pos,1][j+1]);
			   paintChar('w',''+set25[pos2,1][j+1]);
                    end;  {for}
		  end; {if}

           i:=i+2;
	  end; {while}


	  { paint stop char}
	  paintChar('bwb','wnn');

	  codeText:=codetmp;

end;

{ paints an industrial 2 of 5 code.}
procedure TBarcode1D.paintIND25();
{ The value of the checksum character is determined by the following six step
procedure:

 1. Identify even and odd positioned characters in the message with the
    right-hand message character ALWAYS defined as an even positioned character.
 2. Sum the numeric values of the odd positioned characters.
 3. Sum the numeric values of the even positioned characters and multiply the total
    value by three.
 4. Sum the odd and even totals from steps 2 and 3.
 5. Determine the smallest number which, when added to the sum in step 4, will result
    in a multiple of 10. This number is the value of the checksum character.
}
var
pos:integer;
c:string;
codetmp:string;
sumeven:integer;
sumodd:integer;
even:boolean;
checksum:integer;
i:integer;
j:integer;
tmpstr:String;
begin


    codetmp:=code;

    { paint start char}
    paintChar('bwbwbw','wwwwnw');


    {calculate checksum}
    sumeven:=0;
    sumodd:=0;
    even:=true;
     for i:=length(codetmp)-1 downto 0 do begin
		  c:=''+codetmp[i];
		  if (even) then sumeven:=sumeven+findChar10(set25,c)
		  else sumodd:=sumodd+findChar10(set25,c);
		  even:=(not even);
     end;

     checksum:=(sumeven*3)+sumodd;
     checksum:=(checksum mod 10);
     if (checksum<>0) then checksum:=10-checksum;

     { 48 is the asc value of the char 0 }
     if (checkCharacter) then begin
        str(checksum,tmpstr);
        codetmp:=codetmp+tmpstr;
     end;

     { paint char in string}
     for i:=0 to length(codetmp)-1 do begin
		  c:=''+codetmp[i+1];
		  pos:=findChar10(set25,c);
		  if (pos>=0) then begin
			  for j:=0 to length(set25[pos,1])-1 do begin
			   paintChar('b',''+set25[pos,1][j+1]); { bar}
			   paintChar('w','w'); { narrow space}
			  end;
		  end;

	end;


  { paint stop char}
  paintChar('bwbwb','wwnww');

end;

{convert str to int}
function TBarcode1D.toInteger( s:String):integer;
var
dummy:integer;
outInt:integer;
begin
     val(s,outInt,dummy);
     result:=outInt;
end;

{convert int to str}
function TBarcode1D.toString(i:integer):String;
var
s:string;

begin
     str(i,s);
     result:=s;
end;



{  caculates the UPC/EAN check character.}
function TBarcode1D.UPCEANCheck(s:String):String;
var
odd:boolean;
sumodd:integer;
sum:integer;
c:integer;
i:integer;
begin

{ The check character for the UPC/EAN codes is generated by the following method:

 1. Designate the rightmost character odd.
 2. Sum all of the characters in the odd positions and multiply the result by three.
 3. Sum all of the characters in the even positions
 4. Add the odd and even totals from steps two and three.
 5. Determine the smallest number that when added to the result from step four,
 will result in a multiple of 10. This is the check character.
}

        sumodd:=0;
        sum:=0;
        odd:=true;

	for i:=(length(s)-1) downto 0 do begin
		if (odd) then sumodd:=sumodd+toInteger(s[i+1])
		else sum:=sum+toInteger(s[i+1]);
		odd:=(not odd);
	end;

	sum:=(sumodd*3)+sum;

	c:=(sum mod 10);

	if (c<>0) then  c:=10-c;

	result:=toString(c);

end;

function TBarcode1D.makeLength(c:String;l:integer):String;
var
i:integer;
begin

          // fill up to 1l chars
          if (length(c)<l) then
                               for i:= length(c) to l do c:=c+'0';

          if (length(code)>l) then c:=copy(c,1,l);

          result:=c;

end;

{ paints a UPCA code.}
procedure TBarcode1D.paintUPCA();
var
pos:integer;
c:string;
i:integer;
begin
{ The UPC-A symbol is physically constructed as follows:

 Left hand guard bars, encoded 101
 Number system character, encoded as a left hand character
 First five data characters, encoded as left hand characters
 Center guard pattern, encoded 01010
 Last five data characters, encoded as right hand characters
 Check character, encoded as a right hand character
 Right hand guard bars, encoded 101
}


	  if ((length(code)=11) and (checkCharacter)) then code:=code+UPCEANCheck(code);

          code:=makeLength(code,12);

	  { paint start char}
	  paintGuardChar('bwb','nnn',0);

	  FleftGuardBar:=FCurrentX;

	  { paint char in string}
	  for i:=0 to length(code)-1 do    begin
		  c:=''+code[i+1];

		  if (i<=5) then begin
			pos:=findChar10(setUPCALeft,c);
			paintChar('wbwb',setUPCALeft[pos,1]);
		  end
		  else begin
			pos:=findChar10(setUPCARight,c);
			paintChar('bwbw',setUPCARight[pos,1]);
		  end;

		  { center guard Pattern}
		  if (i=5) then begin

			   FcenterGuardBarStart:=FCurrentX;

			   paintGuardChar('wbwbw','nnnnn',0);


			   FcenterGuardBarEnd:=FCurrentX;
		  end;

	  end;

	  FrightGuardBar:=FCurrentX;

	  { paint stop char}
	  paintGuardChar('bwb','nnn',0);

	  FendOfCode:=FCurrentX;

	  { supplement}
	  if (UPCEANSupplement2) then paintSup2(copy(code,2,2))
	  else if (UPCEANSupplement5) then paintSup5(copy(code,2,5));

end;

{ paints a EAN13 code.}
procedure TBarcode1D.paintEAN13();
{	The physical structure of the EAN-13 is as follows:

 - Left hand guard bars, encoded 101
 - Second flag character, encoded as in the following tables
 - First five data characters, encoded as in the following tables
 - Center guard pattern, encoded 01010
 - Last five data characters, encoded as right hand characters
 - Check character, encoded as a right hand character
 - Right hand guard bars, encoded 101
 The thirteenth digit is encoded in the pattern of parity in the second flag character and the
 first five data characters. The left hand A and the left hand B character sets are used.
}
var
pos:integer;
i:integer;
c:string;
leftset:TMatrix10x2String;
sets:String;
begin


	  if ((length(code)=12) and (checkCharacter)) then begin
                  code:=code+UPCEANCheck(code);
                  codeText:=code;
	  end;

          code:=makeLength(code,13);

	  { paint start char}
	  paintGuardChar('bwb','nnn',0);

	  FleftGuardBar:=FCurrentX;

	 {second flag char}
	  sets:=setEANCode[toInteger(code[1])];
          pos:=findChar10(setEANLeftA,code[2]);
	  paintChar('wbwb',setEANLeftA[pos,1]);

	  { paint char in string}
	  for i:=2 to 11 do begin
		  c:=''+code[i+1];

		  if (i<=6) then begin

			{ use set A or B?}
			leftset:=setEANLeftA;
			if (sets[(i-2)+1]='B') then leftset:=setEANLeftB;

			pos:=findChar10(leftset,c);
			paintChar('wbwb',leftset[pos,1]);
		  end
		  else begin
			pos:=findChar10(setEANRight,c);
			paintChar('bwbw',setEANRight[pos,1]);
		  end;

		  { center guard Pattern}
		  if (i=6) then begin
			   FcenterGuardBarStart:=FCurrentX;
			   paintGuardChar('wbwbw','nnnnn',0);
			   FcenterGuardBarEnd:=FCurrentX;
		  end;

	  end;

	  { check character}
          pos:=findChar10(setEANRight,code[13]);
	  paintChar('bwbw',setEANRight[pos,1]);

	  FrightGuardBar:=FCurrentX;

	  {paint stop char}
	  paintGuardChar('bwb','nnn',0);

	  FendOfCode:=FCurrentX;

	   {supplement}
	  if (UPCEANSupplement2) then paintSup2(copy(code,3,2))
	  else if (UPCEANSupplement5) then paintSup5(copy(code,3,4));

end;



function TBarcode1D.findInArray103(s1:TVectorString103;c:string):integer;
var
j:integer;
begin

  for j:=0 to length(s1)-1 do
	  if (s1[j]=c) then begin
              result:=j;
              exit;
          end;


  result:=-1;

end;




function TBarcode1D.convertCode128ControlChar(c:String):string;
var
FNC1:String;
FNC2:String;
FNC3:String;
FNC4A:String;
FNC4B:String;
begin
    FNC1:=chr(202);
    FNC2:=chr(197);
    FNC3:=chr(196);
    FNC4A:=chr(201);
    FNC4B:=chr(200);

    if (c=FNC1) then  c:='_102';
    if (c=FNC4A) then c:='_101';
    if (c=FNC4B) then c:='_100';
    if (c=FNC2) then c:='_97';
    if (c=FNC3) then c:='_96';

    result:= c;
end;


{ help functions for code128}
function TBarcode1D.isDigit(c:String):boolean;
begin
      if (length(c)>1) then begin
         result:=false;
         exit;
       end;

      result:=((c[1]>='0') and (c[1]<='9'));
end;


function TBarcode1D.getNextLowerCase(s:string;i:integer):integer;
var
j:integer;
begin

    for j:=i to length(s)-1 do
       if ((s[j+1]='a') and (s[j+1]<='z')) then begin
                        result:=j;
                        exit;
       end;

    result:=9999;
end;

function  TBarcode1D.getNextControlChar(s:string;i:integer):integer;
var
j:integer;
begin

    for j:=i to length(s)-1 do
       if (s[j+1]<' ') then begin
                        result:=j;
                        exit;
       end;

    result:=9999;
end;

function  TBarcode1D.getEvenNumberDigits(s:string;i:integer):boolean;
var
j:integer;
even:boolean;
begin


    even:=true;

    for j:=i to length(s)-1 do
       if (isDigit(s[j+1])) then  even:=not even
       else break;

    result:=even;
end;

function TBarcode1D.calculateNextSet(s:string;i:integer):String;
begin

   { SET C}
    if (length(s)>=(i+4)) then
      if (isDigit(s[i+1])) and (isDigit(s[i+2])) and (isDigit(s[i+3])) and (isDigit(s[i+4]))  then begin
         result:='C';
         exit;
      end;

    if (getNextControlChar(s,i)<getNextLowerCase(s,i)) then begin
       result:='A';
       exit;
    end;

    result:='B';

end;

{ paints a code 128}
procedure TBarcode1D.paintCode128();
{ Code 128 is a continuous, multilevel, full ASCII code. Each of the Code 128
	characters consists of three bars and three spaces. The bars and spaces may be
	one, two, three, or four modules wide. The total length of each code 128 character
	is eleven modules, with the total length of the bar modules odd, and the total
	length of the space modules even.
 The character set consists of 103 different characters, three different
	start characters, and one unique stop character


 A check character is mandatory for Code 128. The check character is a modulus
	103 sum of the character values, weighted by position in the message. The check
	character is calculated by summing the value for the start character with the
	products from the value of the message characters times their position in the
	message, with the character that follows the start character in the first position.
	The stop character is not included in the calculation. The result is divided by 103,
	and the character that corresponds to the remainder is the check character.
}
var
pos:integer;
sum:integer;
i:integer;
w:integer;
check:integer;
c:String;
setc:TVectorString103;
cChar:String;
begin


	  setc:=set128A;
	  if (Code128Set='B') then setc:=set128B;

	  sum:=103;
	  if (Code128Set='B') then begin
           setc:=set128B;
           sum:=104;
          end;
	  if (Code128Set='C') then begin
             setc:=set128C;
             sum:=105;
          end;


	  { paint start char}
	  if (Code128Set='B') then paintChar('bwbwbw','211214');
	  if (Code128Set='C') then paintChar('bwbwbw','211232');
          if ((Code128Set<>'B') and (Code128Set<>'C'))	then paintChar('bwbwbw','211412');


	 { paint char in string    }
	  w:=1; { weight for checksum        }
          i:=0;
          while i< length(code) do begin
		  c:=''+code[i+1];

		  if (Code128Set='C') then begin
			  { charater set C encodes two characters at a time}
			  cChar:=''+c;
			  i:=i+1;

                          if (i<length(code)) then cChar:=cChar+code[i+1]
			  else begin
				  { odd number of digits, shift to B}
				  pos:=findInArray103(set128C,'_100');
				  sum:=sum+(pos*w);
				  w:=w+1;
				  paintChar('bwbwbw',set128[pos]);

				  cChar:= toString(toInteger(cChar)+16);
			  end;

			  pos:=findInArray103(set128C,cChar);
			  if (pos>=0) then begin
                                      paintChar('bwbwbw',set128[pos]);
			               sum:=sum+(pos*w);
                                      end;

		  end
		  else begin
		      { set A or B}
		      pos:=findInArray103(setc,c);
		       if (pos>=0) then begin
                          paintChar('bwbwbw',set128[pos]);
                      	  sum:=sum+(pos*w);
                          end;
		  end;
		  w:=w+1;

            i:=i+1;
	  end;     {while}


	  if (checkCharacter) then begin
	    check:=(sum mod 103);
	    paintChar('bwbwbw',set128[check]);
	  end;

	  { paint stop char}
	  paintChar('bwbwbwb','2331112');

end;


{ paints a ean 128}

procedure TBarcode1D.paintEAN128();
{ Code 128 is a continuous, multilevel, full ASCII code. Each of the Code 128
	characters consists of three bars and three spaces. The bars and spaces may be
	one, two, three, or four modules wide. The total length of each code 128 character
	is eleven modules, with the total length of the bar modules odd, and the total
	length of the space modules even.
 The character set consists of 103 different characters, three different
	start characters, and one unique stop character


 A check character is mandatory for Code 128. The check character is a modulus
	103 sum of the character values, weighted by position in the message. The check
	character is calculated by summing the value for the start character with the
	products from the value of the message characters times their position in the
	message, with the character that follows the start character in the first position.
	The stop character is not included in the calculation. The result is divided by 103,
	and the character that corresponds to the remainder is the check character.
}
var
pos:integer;
pos2:integer;
sum:integer;
i:integer;
currentSet:String;
previousSet:String;
w:integer;
check:integer;
c:String;
setc:TVectorString103;
done:boolean;
begin


          { EAN128 is always mode C}
          if (barType=EAN128) then Code128Set:='C';


	  setc:=set128A;
	  if (Code128Set='B') then setc:=set128B;

	  sum:=103;
	  if (Code128Set='B') then begin
           setc:=set128B;
           sum:=104;
          end;
	  if (Code128Set='C') then begin
             setc:=set128C;
             sum:=105;
          end;

          currentSet :=Code128Set;
          previousSet:=currentSet;

	  { paint start char}
	  if (Code128Set='B') then paintChar('bwbwbw','211214');
	  if (Code128Set='C') then paintChar('bwbwbw','211232');
          if ((Code128Set<>'B') and (Code128Set<>'C'))	then paintChar('bwbwbw','211412');


	 { paint char in string    }
	  w:=1; { weight for checksum        }
          i:=0;
	  while i<length(code) do begin
	     c:=''+code[i+1];
             previousSet:=currentSet;
             { select chars array}
	      setc:=set128A;
	      if (currentSet='B') then setc:=set128B;
	      if (currentSet='C') then setc:=set128C;

              c:=convertCode128ControlChar(c);

              { ean 128, first char is FNC1}
              if ((barType=EAN128) and (i=0)) then begin
                 pos:=findInArray103(set128C,'_102');
                 paintChar('bwbwbw',set128[pos]);
                 sum:=sum+(pos*w);
                 w:=w+1;
              end;

	      if (currentSet='C') then begin

                done:=false;

               { case 1, we have 2 digits}
               if (i<length(code)-1) then
                 if (isDigit(c)) and (isDigit(code[i+2])) then begin
                    c:=c+code[i+2];
		    pos:=findInArray103(set128C,c);
		    if (pos>=0) then begin
                      paintChar('bwbwbw',set128[pos]);
		      sum:=sum+(pos*w);
                    end;
                    done:=true;
                    i:=i+1;
                 end;

              { case 2, this is not a digit, it could be a FNC1}
              if ((not done) and (not isDigit(c) ))  then begin
			        pos:=findInArray103(set128C,c);
			        if (pos>=0)  then begin
                                   paintChar('bwbwbw',set128[pos]);
			           sum:=sum+(pos*w);
                                    done:=true;
                                 end;
              end;

              { case 3 we have only 1 digit      }
              if ((not done) and (isDigit(c))) then begin
                  { do we shift to B or A}
                  currentSet:='B';
                  { there is another char}
                  if (i<length(code)-1) then
                    if (getNextControlChar(code,i+1)<getNextLowerCase(code,i+1)) then currentSet:='A';

				  { odd number of digits, shift to B or A}
				  pos:=findInArray103(set128C,'_100');
                                  if (currentSet='A') then pos:=findInArray103(set128C,'_101');
				  sum:=sum+(pos*w);
				  w:=w+1;
				  paintChar('bwbwbw',set128[pos]);

				  //c:=toString( toInteger(c)+16);

			          pos:=findInArray103(set128B,c);
			          if (pos>=0)  then begin
                                      paintChar('bwbwbw',set128[pos]);
			               sum:=sum+(pos*w);
                                  end;
                                  done:=true;
                end; { done }

                if ((not done) and (not isDigit(c))) then begin
                    currentSet:=calculateNextSet(code,i);
                    i:=i-1;

                    pos2:=0;
                    if (currentSet='A') then pos2:=findInArray103(set128C,'_101');
                    if (currentSet='B') then pos2:=findInArray103(set128C,'_100');
                    paintChar('bwbwbw',set128[pos2]);
		    sum:=sum+(pos2*w);
                end;

		end { END MODE C}
		  else begin

              { select mode C if  we have now 4 digits}
              if (calculateNextSet(code,i)='C') then begin

                  pos2:=findInArray103(set128B,'_99');
                  if (currentSet='A') then pos2:=findInArray103(set128A,'_99');
                  paintChar('bwbwbw',set128[pos2]);
		  sum:=sum+(pos2*w);

                 { next mode is C}
                 currentSet:='C';
                 i:=i-1;
              end
              else begin

                { set A or B}
                pos:=findInArray103(setc,c);

                { we are in mode A and we did not find the chartm try mode B}
                if ((currentSet='A') and (pos=-1)) then
                  if (findInArray103(set128B,c)>=0) then begin
                      { only shift?}
                      if (getNextControlChar(code,i)<getNextLowerCase(code,i)) then
                          { only shift to B for this char}
                          pos2:=findInArray103(setc,'_98')

                      else begin
                        { change to code B}
                        pos2:=findInArray103(setc,'_100');
                        currentSet:='B';
                      end;

                      paintChar('bwbwbw',set128[pos2]);
                      sum:=sum+(pos2*w);
                      w:=w+1;
                      setc:=set128B;
                  end;

                { we are in mode B and we did not find the char try mode A}
                if ((currentSet='B') and (pos=-1)) then
                  if (findInArray103(set128A,c)>=0) then begin

                      // only shift?
                      if (getNextControlChar(code,i)>getNextLowerCase(code,i)) then
                          { only shift to A for this char                           }
                          pos2:=findInArray103(setc,'_98')

                      else begin
                        { change to code A}
                        pos2:=findInArray103(setc,'_101');
                        currentSet:='A';
                      end;

                      { paint SHIFT or SET change      }
                      paintChar('bwbwbw',set128[pos2]);
                      sum:=sum+(pos2*w);
                      w:=w+1;
                      setc:=set128A;

                  end;


                pos:=findInArray103(setc,c);
			    if (pos>=0) then begin
                                        paintChar('bwbwbw',set128[pos]);
                                        sum:=sum+(pos*w);
                                        end;

              end; { not C}

	    end; { not C}
            w:=w+1;
            i:=i+1;
        end; {while}


	  if (checkCharacter) then begin
	    check:=(sum mod 103);
	    paintChar('bwbwbw',set128[check]);
	  end;

	  { paint stop char}
	  paintChar('bwbwbwb','2331112');

end;


{ a EAN8 code}
procedure TBarcode1D.paintEAN8();
{
 The EAN 8 symbol encodes two flag characters, five data characters, and a check character. The physical structure of EAN 8 is as follows:

 - Left hand guard bars, encoded 101
 - Two flag characters, encoded as left hand A characters
 - First two data characters, encoded as left hand A characters
 - Center guard pattern, encoded 01010
 - Last three data characters, encoded as right hand characters
 - Check character, encoded as a right hand character
 - Right hand guard bars, encoded 101
}
var
pos:integer;
i:integer;
c:String;
begin

	  if (length(code)=7) and (checkCharacter) then code:=code+UPCEANCheck(code);

          code:=makeLength(code,8);

	  { paint start char}
	  paintGuardChar('bwb','nnn',0);

	  FleftGuardBar:=FCurrentX;


	  { paint char in string   }
	  for i:=0 to 7 do begin
		  c:=code[i+1];

		  if (i<=3) then begin

			pos:=findChar10(setEANLeftA,c);
			paintChar('wbwb',setEANLeftA[pos,1]);
		  end
		  else begin
			pos:=findChar10(setEANRight,c);
			paintChar('bwbw',setEANRight[pos,1]);
		  end;

		  { center guard Pattern  }
		  if (i=3)  then begin
			   FcenterGuardBarStart:=FCurrentX;
			   paintGuardChar('wbwbw','nnnnn',0);
			   FcenterGuardBarEnd:=FCurrentX;
		  end;

	  end;

	  FrightGuardBar:=FCurrentX;

	  { paint stop char }
	  paintGuardChar('bwb','nnn',0);

	  FendOfCode:=FCurrentX;

	  if (UPCEANSupplement2) then paintSup2(copy(code,3,2))
	  else if (UPCEANSupplement5) then paintSup5(copy(code,3,7));

end;

{ paints a UPCE code    }
procedure TBarcode1D.paintUPCE();
{
 To convert a UPC A number to UPC E there must be at least four zeros in the data characters.

 The physical structure of the UPC-E symbol is as follows:

 Left hand guard bars, encoded 101
 Six explicit data characters, encoded from the parity table.
 Right hand guard bars, encoded 010101
 To determine how to encode the data characters, first the UPC-A check character must be found.
 	Once known, it is used to set the pattern of even and odd parity to be used to encode the six explicit
 	characters from the UPC E Parity Table.

 Determining the explicit characters
  1- If the manufacturers number ends in 000, 100, or 200, valid item numbers are 00000 to 00999.
	  The six explicit characters are the first two characters of the manufacturer's number, the last three
	  characters of the item number, and the third character of the manufacturer's number.
  2- If the manufacturers number ends in 300, 400, 500, 600, 700, 800 or 900, valid item numbers are
     00000 to 00099. The six explicit characters are the first three characters of the manufacturer's
	    number, the last two characters of the item number, and the character ``3''.
  3- If the manufacturers number ends in 10, 20, 30, 40, 50, 60, 70, 80, or 90, valid item numbers are 00000 to
	     00009. The six explicit characters are the first four characters of the manufacturer's number, the
	    last character of the item number, and the character ``4''.
  4- If the manufacturers number does not end in zero, the valid numbers are 00005 to 00009. The six explicit
      	characters are all five characters of the manufacturer's number, and the last character of the item
      	number
}
var
pos:integer;
i:integer;
j:integer;
checkchar:integer;
codetmp:String;
c:String;
setLeft:TMatrix10x2String;
System:String;
inverted:String;
begin



	  codetmp:='';

	  if ((length(code)=11) and (checkCharacter)) then code:=code+UPCEANCheck(code);

          code:=makeLength(code,12);

	  checkchar:=toInteger(code[12]);

	  { case one   }
	  if ((copy(code,4,3)='000') or (copy(code,4,3)='100') or (copy(code,4,3)='200')) then
		  codetmp:=copy(code,2,2)+copy(code,9,3)+code[4];

	  { case two }
	  if ((copy(code,4,3)='300') or (copy(code,4,3)='400') or (copy(code,4,3)='500') or (copy(code,4,3)='600') or (copy(code,4,3)='700') or (copy(code,4,3)='800') or (copy(code,4,3)='900')) then
                  codetmp:=copy(code,2,3)+copy(code,10,2)+'3';

	  { case three    }
	  if ((copy(code,5,2)='10') or (copy(code,5,2)='20') or (copy(code,5,2)='30') or (copy(code,5,2)='40') or (copy(code,5,2)='50') or (copy(code,5,2)='60') or (copy(code,5,2)='70') or (copy(code,5,2)='80') or (copy(code,5,2)='90')) then
                  codetmp:=copy(code,2,4)+copy(code,11,2)+'4';

	  { case four   }
	  if (copy(code,6,1)<>'0') then
                  codetmp:=copy(code,2,5)+copy(code,11,1);


	  codeText:=codetmp;

	  { paint start char   }
	  paintGuardChar('bwb','nnn',0);

	  FleftGuardBar:=FCurrentX;

	  System:=UPCESystem0[checkchar];
	  if (UPCESytem='1') then System:=UPCESystem1[checkchar];

	  { paint char in string   }
	  for i:=0 to length(codetmp)-1 do begin
		  c:=codetmp[i+1];

		  setLeft:=setUPCEOdd;
		  if (System[i+1]='E') then setLeft:=setUPCEEven;

                  inverted:='';
		  pos:=findChar10(setLeft,c);

                  for j := 0 to  length(setLeft[pos][1])-1  do begin
                      inverted := setLeft[pos][1][j+1] + inverted;
                  end;

		  paintChar('wbwb',inverted);

	  end;


	  FrightGuardBar:=FCurrentX;

	  { paint stop char   }
	  paintGuardChar('wbwbwb','nnnnnn',0);

	  FendOfCode:=FCurrentX;

	  {supplement  }
	  if (UPCEANSupplement2) then paintSup2(copy(codetmp,1,2))
	  else if (UPCEANSupplement5) then paintSup5(copy(codetmp,1,5));

	end;


{ paints a 2 digit supplement  }
procedure TBarcode1D.paintSup2(chars:String);
var
i:integer;
Parity:String;
setc:TMatrix10x2String;
pos:integer;
begin

	  if (length(supplement)>0) then chars:=supplement;


	  FsuplementTopMargin:=round(barHeightPixels*(1-supHeight));
	  if (length(codeSup)=0) then codeSup:=chars;

	  if (length(chars)<>2) then exit;

	  FCurrentX:=FCurrentX+supSeparation; { separation between code bar and supplement}

	  FstartSuplement:=FCurrentX;


          i:=toInteger(chars);

	  Parity:='OO';
	  if (i mod 4)=1 then Parity:='OE';
	  if (i mod 4)=2 then Parity:='EO';
	  if (i mod 4)=3 then Parity:='EE';

	  { start}
	  paintGuardChar('bwb','112',FsuplementTopMargin);

	  { first char  }
	  setc:=setUPCEOdd;
	  if (Parity[1]='E') then setc:=setUPCEEven;
	  pos:=findChar10(setc,chars[1]);
	  paintGuardChar('wbwb',setc[pos,1],FsuplementTopMargin);

	 { delineator   }
	  paintGuardChar('wb','11',FsuplementTopMargin);

	  {second char    }
	  setc:=setUPCEOdd;
	  if (Parity[2]='E') then setc:=setUPCEEven;
	  pos:=findChar10(setc,chars[2]);
	  paintGuardChar('wbwb',setc[pos,1],FsuplementTopMargin);

	  FendSuplement:=FCurrentX;

end;

{ paints a 5 digit supplement}
procedure TBarcode1D.paintSup5(chars:String);
var
odd:boolean;
sumodd:integer;
sum:integer;
c:integer;
i:integer;
j:integer;
pos:integer;
setc:TMatrix10x2String;
sumStr:String;
Parity:String;
begin
	  if (length(supplement)>0) then chars:=supplement;

	  FsuplementTopMargin:=round(barHeightPixels*(1-supHeight));
	  if (length(codeSup)=0) then  codeSup:=chars;

	  if (length(chars)<>5) then  exit;

          {
       To determine the parity pattern for the five digit supplementals, use the following steps:

       Designate the rightmost character odd
       Sum all of the characters in the odd positions and multiply the result by three.
       Sum all of the characters in the even positions and multiply the result by nine.
       Add the results from steps two and three. The number in the units position is the number of the parity pattern.
}

	 odd:=true;
	 sumodd:=0;
	 sum:=0;

	 for i:=length(chars)-1 downto 0 do begin
		if (odd) then sumodd:=sumodd+toInteger(chars[i+1])
		else sum:=sum+toInteger(chars[i+1]);
		odd:=(not odd);
	 end;

	 sum:=(sumodd*3)+(sum*9);

	 sumstr:=toString(sum); { convert result to string}

	 { take last digit}
         c:=toInteger(sumstr[length(sumstr)]);

	 Parity:=fiveSuplement[c];

	 FCurrentX:= (FCurrentX+supSeparation);  {separation between code bar and supplement}

	 FstartSuplement:=FCurrentX;

	  { start}
	  paintGuardChar('bwb','112',FsuplementTopMargin);

	  for j:=0 to 4 do begin

	    { first char}
	    setc:=setUPCEOdd;
	    if (Parity[j+1]='E') then setc:=setUPCEEven;
	    pos:=findChar10(setc,chars[j+1]);
	    paintGuardChar('wbwb',setc[pos,1],FsuplementTopMargin);

	    { delineator}
	    if (j<4) then paintGuardChar('wb','11',FsuplementTopMargin);

	  end;

	  FendSuplement:=FCurrentX;

	end;


{ paints a matrix 2 of 5 code }
procedure TBarcode1D.paintMAT25();
var
pos:integer;
i:integer;
codetmp:String;
c:String;
begin


	  codetmp:=code;

	  { paint start char}
	  paintChar('bwbwbw','wnnnnn');

	  { paint char in string}
	  for i:=0 to length(codetmp)-1 do begin
		  c:=code[i+1];
		  pos:=findChar10(set25,c);
		  if (pos>=0) then  paintChar('bwbwbw',set25[pos,1]+'n');
	  end;

	  { paint stop char}
	  paintChar('bwbwbw','wnnnnn');

end;

{ paints a code 39 code.}
procedure TBarcode1D.paintBAR39();
var
pos:integer;
sum:integer;
i:integer;
c:String;
inter:integer;
begin
    { Code 39 is an alphanumeric bar code that can encode decimal numbers,
    the upper case alphabet, and the following special symbols:

                   _    .    *    $    /    %    +

    Code 39 characters are constructed using nine elements, five bars and
    four spaces. Of these nine elements, two of the bars and one of the spaces are
    wider than the rest. Wide elements represent binary ones (1), and narrow elements
    represent binary zeros (0). The character set table shows each of the available
    characters with their corresponding check character values.

    To enable a decoder to distinguish between the wide and narrow elements a minimum
    wide to narrow ratio is needed. Depending upon which resolution has been used for
    the printing of the bar code, the width of the wide element should be at least two
    times greater than the narrow element. A ratio of three to one is better. All
    elements of the same type should be printed the same size. (The width of a narrow
    bar should be the same as a narrow space.)


    The Code 39 check character is a modulus 43 sum of all of the message character
    values and is printed as the last character in the message. The check character
    values are given in the above table. The check chararacter is computed by adding
    up all of the values, dividing by 43, and using the remainder as the value of the
    check character.
    }

	  sum:=0;

	  { paint *}
	  paintChar('bwbwbwbwb',set39[findChar50(set39,'*')][1]);

	  { interchar separator}
          inter:=(FI*X);
          if (inter=0) then inter:=1;

	  FCurrentX:=FCurrentX+inter;

	  { paint char in string}
	  for i:=0 to length(code)-1 do begin
		  c:=code[i+1];
		  pos:=findChar50(set39,c);
		  if (pos>-1) then begin
			  sum:=sum+pos;
			  paintChar('bwbwbwbwb',set39[pos,1]);
	                  { interchar separator}
			  FCurrentX:=FCurrentX+inter;
                   end;
	  end;

	  { check character   }
	  if (checkCharacter) then begin
		   pos:=(sum mod 43);
		   paintChar('bwbwbwbwb',set39[pos,1]);
	           { interchar separator  }
		   FCurrentX:=FCurrentX+inter;

		   codeText:=code+set39[pos,0];
	  end;

	  { paint *}
	  paintChar('bwbwbwbwb',set39[findChar50(set39,'*'),1]);

end;


{ paints code 11 code}
procedure TBarcode1D.paintCODE11();

      {  Code 11 uses two check digits, C and K. The first check digit C is the modulo
        11 sum of the weighted products with the weights changing from 1 to 10.
       The second check digit K is also the modulo 11 sum of the weighted products
       with the weights changing from 1 to 9. The character - is assigned the value of 10.
       As a rule of thumb, when the message length is 10 digits or less, usually only the C check digit is used.
      }
var
pos:integer;
sum:integer;
i:integer;
c:String;
ch1:integer;
ch2:integer;
w:integer;
begin


	  { paint start/Stop}
	  paintChar('bwbwbw','nnwwnn');

	  { calculate checksum C}
	  w:=1; {weight}
	  sum:=0;
	  for i:=length(code)-1 downto 0 do begin
		  sum:=sum+(findChar50(set11,code[i+1])*w);
		  w:=w+1;
		  if (w=11) then w:=1;
	  end;

	  ch1:=(sum mod 11);

	  { calculate checksum K}
	  w:=2; {weight}
	  sum:=ch1;
	  for i:=length(code)-1 downto 0 do begin
		  sum:=sum+(findChar50(set11,code[i+1])*w);
		  w:=w+1;
		  if (w=10) then w:=1;
	  end;

	  ch2:=(sum mod 11);

	  { paint char in string}
	  for i:=0 to length(code)-1 do begin
		  c:=code[i+1];
		  pos:=findChar50(set11,c);
		  if (pos>-1) then
			  paintChar('bwbwbw',set11[pos,1]+'n');

	  end;

	  if (checkCharacter) then begin
	  { check character C}
	  paintChar('bwbwbw',set11[ch1,1]+'n');

	  codeText:=code+set11[ch1,0];

	  { check character K, if requested or lentgh > 10}
	  if (length(code)>10) then begin
		   paintChar('bwbwbw',set11[ch2,1]+'n');

		   codeText:=codeText+set11[ch2,0];
	  end;
	 end;

	  { paint start/Stop}
	  paintChar('bwbwb','nnwwn');


end;


{ paints a CODABAR code}
procedure TBarcode1D.paintCODABAR();
{
	  Codabar is a discrete, numeric code with special characters and four different
	  start/stop characters. Each character is encoded as seven elements, with two or
	  three of the elements wide. While the Codabar check character is undefined, AIM
	  has a recommended check character. The following table shows the Codabar character encodation.
	  The numbers in the table correspond to the width of the element. A 0 means the
	  element is narrow, and a 1 means the element is wide.

	 While Codabar has no check character, AIM has a recommended check character.
	 	Each Codabar character has a value assigned to it. (See above table).
	 	The check character is computed as follows.

        1. The sum of all character values is taken, including the Start and the Stop
	 	characters.
       2. The data character whose value that when added to the total from step one
	 	equals a multiple of 16 is the check character.
}
var
pos:integer;
sum:integer;
i:integer;
c:String;
ch1:integer;

begin



	  {paint start/Stop, A}
	  paintChar('bwbwbwbw',setCODABAR[findChar50(setCODABAR,CODABARStartChar),1]+'n');

	  { calculate checksum C}
	  sum:=findChar50(setCODABAR,CODABARStartChar)+findChar50(setCODABAR,CODABARStopChar);
	  for i:=(length(code)-1) downto 0 do
		  sum:=sum+(findChar50(setCODABAR,code[i+1]));


	  ch1:=(sum mod 16);
	  if (ch1<>0) then ch1:=16-ch1;

	  { paint char in string�}
	  for i:=0 to length(code)-1 do begin
		  c:=code[i+1];
		  pos:=findChar50(setCODABAR,c);
		  if (pos>-1) then
			  paintChar('bwbwbwbw',setCODABAR[pos,1]+'n');

	  end;

	  { check character K, if requested or lentgh > 10}
	  if (checkCharacter)  then begin
		   codeText:=code+setCODABAR[ch1,0];
		   paintChar('bwbwbwbw',setCODABAR[ch1,1]+'n');
	  end;

	  { paint start/Stop, B}
	  paintChar('bwbwbwb',setCODABAR[findChar50(setCODABAR,CODABARStopChar),1]);


end;

{ paints a MSI code}
procedure TBarcode1D.paintMSI();
{

	  MSI Code is a numeric, continuous code. Each character consists of four bits,
	 	using a straight binary encodation. Each bit is encoded in the following way:
	 	if the bit is a 1, the pattern to be used is a wide bar followed by a narrow
	 	space. If the bit is to be a 0, the pattern is a narrow bar followed by a
	 	wide space. The following table is the bit patterns:

	  The start character is a single ``1'' bit. (wide bar/narrow space).
	 	The stop character is a narrow bar/wide space/narrow bar.
      The check digit is calculated by using the following steps.

      1. Starting from the units position, create a new number with all of the odd position digits in their original sequence.
      2. Multiply this new number by 2.
      3. Add all of the digits of the product from step two.
      4. Add all of the digits not used in step one to the result in step three.
      5. Determine the smallest number which when added to the result in step four will result in a multiple of 10. This is the check character.
}
var
pos:integer;
sum:integer;
i:integer;
c:String;
ch1:integer;
oddNumber:String;
odd:boolean;
begin


	  { paint start}
	  paintChar('bw','wn');

	  { calculate checksum C }
	  sum:=0;
	  oddNumber:='';
	  odd:=true;
	  for i:=(length(code)-1) downto 0 do begin
		  if (not odd) then sum:=sum+(findChar10(setMSI,code[i+1]));
		  if (odd) then oddNumber:=toString(findChar10(setMSI,code[i+1]))+oddNumber;
		  odd:=(not odd);
	  end;

	 oddNumber:=toString(toInteger(oddNumber)*2);

	 for i:=(length(oddNumber)-1) downto 0 do
		 sum:=sum+(findChar10(setMSI,oddNumber[i+1]));

	  ch1:=(sum mod 10);
	  if (ch1<>0) then ch1:=10-ch1;

	  { paint char in string  }
	  for i:=0 to length(code)-1 do begin
		  c:=code[i+1];
		  pos:=findChar10(setMSI,c);
		  if (pos>-1) then
			  paintChar('bwbwbwbw',setMSI[pos,1]);
	  end;

	  {check character K, if requested or lentgh > 10 }
	  if (checkCharacter) then begin
		   paintChar('bwbwbwbw',setMSI[ch1,1]);

		   codeText:=code+setMSI[ch1,0];
	  end;

	  {paint stop  }
	  paintChar('bwb','nwn');


end;

{ paints a code 39 extended code}
procedure TBarcode1D.paintBAR39Ext();
var
pos:integer;
sum:integer;
i:integer;
j:integer;
c:String;
encoded:String;
b:byte;
inter:integer;
begin

	  sum:=0;

	  { paint *}
	  paintChar('bwbwbwbwb',set39[findChar50(set39,'*'),1]);

	  { interchar separator  }
          inter:=(FI*FX);
          if (inter=0) then inter:=1;
	  FCurrentX:=FCurrentX+inter;

	  { paint char in string }
	  for i:=0 to length(code)-1 do begin
		  b:=ord(code[i+1]);
		  if (b<=128) then begin
			encoded:=set39Ext[b];
		    for j:=0 to length(encoded)-1 do begin
		     c:=encoded[j+1];
		     pos:=findChar50(set39,c);
		     if (pos>-1) then begin
			   sum:=sum+pos;
			   paintChar('bwbwbwbwb',set39[pos,1]);
	                   { interchar separator}
			   FCurrentX:=FCurrentX+inter;
		     end;
		  end;
	    end;
	  end;

	  { check character}
	  if (checkCharacter) then begin
		   pos:=(sum mod 43);
		   paintChar('bwbwbwbwb',set39[pos,1]);
	           {  interchar separator}
		   FCurrentX:=FCurrentX+inter;

		   codeText:=code+set39[pos,0];
	  end;

	  {paint *   }
	  paintChar('bwbwbwbwb',set39[findChar50(set39,'*'),1]);

end;


{ paints a code 93 barcode}
procedure TBarcode1D.paintBAR93();
var
pos:integer;
sum:integer;
i:integer;
w:integer;
c:String;
ch1:integer;
ch2:integer;

begin


	  { paint start}
	  paintChar('bwbwbw','111141');

	  { paint char in string}
	  for i:=0 to length(code)-1 do begin
		    c:= code[i+1];
		    pos:=findChar50(set93,c);

		    if (pos>-1) then begin
			  paintChar('bwbwbw',set93[pos,1]);
                    end;
	        { interchar separator
			//currentX=currentX+((int) (I*X*resolution));}
	  end;

         { calculate checksum C}
	  w:=1; {weight}
	  sum:=0;
	  for i:=(length(code)-1) downto 0 do begin
		  sum:=sum+(findChar50(set93,code[i+1])*w);
		  w:=w+1;
		  if (w=21) then w:=1;
	  end;

	  ch1:=(sum mod 47);

	  { calculate checksum K}
	  w:=2; {weight  }
	  sum:=ch1;
	  for i:=length(code)-1 downto 0 do begin
		  sum:=sum+(findChar50(set93,code[i+1])*w);
		  w:=w+1;
		  if (w=16) then w:=1;
	  end;

	  ch2:=(sum mod 47);

	  { check character}
	  if (checkCharacter) then begin

		   paintChar('bwbwbw',set93[ch1,1]);
		   paintChar('bwbwbw',set93[ch2,1]);

		   codeText:=code+set93[ch1,0][1]+set93[ch2,0][1];
	  end;

       { paint stop  }
	  paintChar('bwbwbwb','1111411');

end;


{ paints a code 93 extended barcode}
procedure TBarcode1D.paintBAR93Ext();
var
pos:integer;
sum:integer;
i:integer;
w:integer;
c:String;
ch1:integer;
ch2:integer;
encoded:String;
b:byte;
begin

	  { paint start    }
	  paintChar('bwbwbw','111141');

	 {paint char in string   }
	  for i:=0 to length(code)-1 do begin
		  b:=ord(code[i+1]);
		  if (b<=128) then begin
			encoded:=set93Ext[b];

			if (length(encoded)=3) then begin
			   { control char, _1,_2,_3, or _4}
		           c:=encoded[1]+encoded[2];
		           pos:=findChar50(set93,c);
			   paintChar('bwbwbw',set93[pos,1]);

	                   {// interchar separator
			   //currentX=currentX+((int) (I*X*resolution));}

			   c:=encoded[3];
			end
			else c:=encoded[1];

		        pos:=findChar50(set93,c);
			paintChar('bwbwbw',set93[pos,1]);
	                {// interchar separator
			//currentX=currentX+((int) (I*X*resolution));}
	    end;
	  end;


          { calculate checksum C}
	  w:=1; {weight}
	  sum:=0;
	  for i:=(length(code)-1) downto 0 do begin
	          b:=ord(code[i+1]);
		  if (b<=128) then begin
			encoded:=set93Ext[b];

			if (length(encoded)=3) then begin
			   { control char, _1,_2,_3, or _4}
		           c:=encoded[1]+encoded[2];
		           pos:=findChar50(set93,c);
			   sum:=sum+(pos*(w+1));

			   c:=encoded[3];
			   pos:=findChar50(set93,c);
			   sum:=sum+(pos*w);
		           w:=w+1;
			   if (w=21) then w:=1;
			   w:=w+1;
			   if (w=21) then w:=1;

			end
			else begin
			    c:=encoded[1];
		            pos:=findChar50(set93,c);
			    sum:=sum+(pos*w);
			    w:=w+1;
			    if (w=21) then w:=1;
			end;
		  end;
	  end;

	  ch1:=(sum mod 47);

	  { calculate checksum K}
	  w:=2; {weight  }
	  sum:=ch1;
	  for i:=(length(code)-1) downto 0 do begin
	      b:=ord(code[i+1]);
		  if (b<=128) then begin
			encoded:=set93Ext[b];

			if (length(encoded)=3) then begin
			   { control char, _1,_2,_3, or _4        }
		           c:=encoded[1]+encoded[2];
		           pos:=findChar50(set93,c);
			   sum:=sum+(pos*(w+1));

			   c:=encoded[3];
			   pos:=findChar50(set93,c);
			   sum:=sum+(pos*w);
                           w:=w+1;
			   if (w=16) then w:=1;
                           w:=w+1;
			   if (w=16) then w:=1;

			end
			else begin
                           c:=encoded[1];
		           pos:=findChar50(set93,c);
			   sum:=sum+(pos*w);
                           w:=w+1;
			   if (w=16) then w:=1;
			end;
		  end;
	  end;

	  ch2:=(sum mod 47);

	  { check character  }
	  if (checkCharacter) then begin

		   paintChar('bwbwbw',set93[ch1,1]);
		   paintChar('bwbwbw',set93[ch2,1]);

		   codeText:=code+set93[ch1,0][1]+set93[ch2,0][1];
	  end;

       { paint stop  }
	  paintChar('bwbwbwb','1111411');

end;

{ paints a character}
procedure TBarcode1D.paintChar(patternColor:String;patternBars:String);
begin

	paintChar2(patternColor,patternBars,0);

end;

{ paints a character}
procedure TBarcode1D.paintChar2(patternColor:String;patternBars:String;bartopmargin:integer);
var
cColor:String;
cBar:String;
i:integer;
begin
            if (length(patternColor)<>length(patternBars)) then exit;


		for i:=0 to length(patternColor)-1 do begin
			cColor:=patternColor[i+1];
			cBar:=patternBars[i+1];

			if (cBar='n') then addBar(FnarrowBarPixels,(cColor='b'),bartopmargin);
			if (cBar='w') then addBar(FwidthBarPixels,(cColor='b'),bartopmargin);

			if (cBar='1') then addBar(FnarrowBarPixels,(cColor='b'),bartopmargin);
			if (cBar='2') then addBar( (FnarrowBarPixels*2),(cColor='b'),bartopmargin);
			if (cBar='3') then addBar( (FnarrowBarPixels*3),(cColor='b'),bartopmargin);
			if (cBar='4') then addBar( (FnarrowBarPixels*4),(cColor='b'),bartopmargin);

                end;

end;

{paints a guard character}
procedure TBarcode1D.paintGuardChar(patternColor:String;patternBars:String;bartopmargin:integer);
begin

	{ Guard bars are longer that the other}
	if (FFont<>nil) and (guardBars) then begin
                        FCurrentCanvas.Font:=Font;

                        extraHeight:=FCurrentCanvas.textHeight(code);
	  end;

	paintChar2(patternColor,patternBars, bartopMargin);
	extraHeight:=0;

end;

{ calculates L and converts size in CM to pixels}
procedure TBarcode1D.calculateSizes();
var
c:integer;
b:integer;
j:integer;
encoded:String;
begin

        C := Length(fcode); {lentgh of m_code in chars}

        FnarrowBarPixels:= FX;
        FwidthBarPixels := FX * FN;

         { length}
        If (barType = INTERLEAVED25) Then begin
            { leading 0 needed??  }
            If ((C Mod 2) = 0) And (checkCharacter) Then C := C + 1;
            If ((C Mod 2) = 1) And (Not checkCharacter) Then C := C + 1;

            If (checkCharacter) Then C := C + 1;

            L := ((C div 2) * (3 + (2 * N)) * X) + (7 * X);
        End;


        If (barType = UPCA) Then  begin
            If (checkCharacter) Then C := C + 1;
            L := (C * 7 * X) + ((3 + 3 + 5) * X);
        End;

        If (barType = EAN13) Then
            L := (C * 7 * X) + ((3 + 3 + 5) * X);


        If (barType = EAN8) Then
            L := (C * 7 * X) + ((3 + 3 + 5) * X);


        If ((barType = CODE128) or (barType = EAN128)) Then  begin
            If (checkCharacter) Then C := C + 1;

            If (Code128Set = 'C') Then
               L := ((11 * C) + 35) * X
            Else
               L := round(((5.5 * C) + 35) * X);

        End;

        If (barType = UPCE) Then
            L := (6 * 7 * X) + ((3 + 6) * X);



        If (barType = IND25) Then begin
            If (checkCharacter) Then C := C + 1;

            L := (C * (3 + (2 * N)) * X) + (7 * X);
        End;

        If (barType = MAT25) Then begin
            If (checkCharacter) Then C := C + 1   ;

            L := (C * (3 + (2 * N)) * X) + (7 * X);
        End;


        If (barType = MSI) Then begin
            If (checkCharacter) Then C := C + 1;

            L := (C * (4 + (4 * N)) * X) + ((1 + N) * X) + ((2 + N) * X) ;
        End ;

        If (barType = CODABAR) Then begin
            If (checkCharacter) Then C := C + 1;

            { almost all char have 3 narrow, 2 wide. (3+(2*N))}
            L := ((C + 2) * (4 + (3 * N)) * X);
        End;

        If (barType = CODE11) Then begin

            If ((checkCharacter) Or (Length(code) > 10)) Then C := C + 1;

            { almost all char have 3 narrow, 2 wide. (3+(2*N))}
            L := ((C + 2 + 1) * (3 + (2 * N)) * X);

        End ;


        If (barType = POSTNET) Then begin
             If (checkCharacter) Then C := C + 1;

             L := (X * 10) ;
        End ;

        If (barType = CODE39) Then begin
             If (checkCharacter) Then C := C + 1;

             L := ((C + 2) * ((3 * N) + 6) * X) + ((C + 1) * I * X);

        End;

        If (barType = CODE39EXT) Then begin

            C := 0;
            If (checkCharacter) Then C := C + 1;
             { count chars needed to encode}
             
             
            For j := 0 To Length(code) - 1 do begin
                b := ord(code[j+1]);
                If (b <= 128) Then begin
                    encoded := set39Ext[b + 2];
                    C := C + Length(encoded);
                End;

            end;

             L := ((C + 2) * ((3 * N) + 6) * X) + ((C + 1) * I * X);

        End;

        If ((barType = CODE93) Or (barType = CODE93EXT)) Then  begin
            

            C := 0;
            If (checkCharacter) Then C := C + 1 ;
            { count chars needed to encode }
            For j := 0 To Length(code) - 1 do begin
                b := ord(code[j+1]);
                If (b <= 128) Then begin
                    encoded := set39Ext[b * 2];
                    If (Length(encoded) = 1) Then
                       C := C + 1
                    Else
                      C := C + 2;

                End;

            end;

             L := ((C + 2) * (9 * X)) + ((C + 1) * I * X);

        End;



        { calculate dimensions of height}
         If (BarHeightPixels = 0) Then
           BarHeightPixels := round(L * H)


end;

{ paints the barcode}
procedure TBarcode1D.paintBarcode(c:TCanvas);
var
  toCenterX: integer;
  tmpCode: String;
  textH: integer;
  charW: integer;
  textW: integer;
  textW2: integer;
  groupCenterX: integer;
  rect: TRect;
begin
  codeText:='';

	calculateSizes();

{		// if demo
		if ((d<>1) && (topMarginCM<1)) topMarginCM=1;
 }
  FCurrentX := LeftMargin+offsetX;

  FCurrentCanvas := c;

  { background color}
  if backColor = clNone then
  begin
    c.Brush.Style := bsClear;
    c.pen.Mode:= pmNop;
  end
  else
  begin
    c.pen.Color:=backColor;
    c.Brush.Color:=backColor;
    c.Brush.Style := bsSolid;
    Rect.Left := offsetX;
    Rect.Top := offsetY;
    if not FVerticalOrientation then
    begin
      Rect.Bottom := offsetY+height;
      Rect.Right := offsetX+width;
    end
    else
    begin
      Rect.Right := offsetY+height;
      Rect.Bottom := offsetX+width;
    end;
    c.FillRect(Rect);
//          fillRect(c,offsetX,offsetY,offsetX+width,offsetY+height);
  end;

//        fillRect(c,c.Cliprect.left,c.Cliprect.top,c.Cliprect.Right,c.Cliprect.bottom);

  c.pen.Mode:= pmCopy;
  FendOfCode:=0;

  { process tilde}
  tmpCode:=code;
  if (processTilde) then code:=applyTilde(code);

	 { draw bars}
	if (barType=CODE11) then  paintCODE11();
	if (barType=MSI) then  paintMSI();
	if (barType=CODABAR) then  paintCODABAR();
	if (barType=CODE39) then  paintBAR39();
	if (barType=CODE39EXT) then  paintBAR39Ext();
	if (barType=INTERLEAVED25) then paintInterleaved25();
	if (barType=CODE93) then paintBAR93();
	if (barType=EAN8) then paintEAN8();
	if (barType=EAN13) then paintEAN13();
	if (barType=UPCA) then paintUPCA();
	if (barType=UPCE) then paintUPCE();
	if (barType=CODE128) then paintCode128();
        if (barType=EAN128) then paintEAN128();
	if (barType=CODE93EXT) then paintBAR93Ext();
	if (barType=IND25) then paintIND25();
	if (barType=MAT25) then paintMAT25();
	if (barType=POSTNET) then paintPOSTNET();

  code:=tmpCode;

	if (FendOfCode=0) then FendOfCode := FCurrentX;
	if (length(codeText)=0) then  codeText := code;
	FCurrentY := barHeightPixels + (topMargin + offsetY);


  { draw text}
	if (font<>nil) then
  begin
    FCurrentCanvas.Font:=Font;
    FCurrentCanvas.Brush.Color:=backColor;
    if backColor = clNone then
      FCurrentCanvas.Brush.Style := bsClear
    else
      FCurrentCanvas.Brush.Style := bsSolid;

    //TextH:=Font.height;
    TextH:=FCurrentCanvas.textHeight(codeText);
    TextW:= FCurrentCanvas.textWidth(codeText);
    charW:= FCurrentCanvas.textWidth('8');


    { draw supplement text}
    if ((UPCEANSupplement2) or (UPCEANSupplement5)) then
      if ((barType=EAN8) or (barType=UPCA) or (barType=UPCE) or (barType=EAN13)) then
      begin
        TextW2:= FCurrentCanvas.textWidth(codeSup);
        groupCenterX:=((FendSuplement-FstartSuplement-TextW2) div 2);
        if (groupCenterX<0) then groupCenterX:=0;
        FCurrentCanvas.TextOut(FstartSuplement+groupCenterX, (topMargin+offsetY)+FsuplementTopMargin-2-TextH,codeSup);
      end;

    { draw text for POSTNET}
    if (barType=POSTNET) and FShowText then
    begin
      toCenterX := ((FendOfCode - (leftMargin + offsetX) - TextW) div 2);
      if (toCenterX<0) then toCenterX:=0;
      FCurrentCanvas.TextOut((leftMargin+offsetX)+toCenterX,postnetHeightTallBar+TextH+1+(topMargin+offsetY),codeText);
      FcurrentY:=barHeightPixels+TextH+1+(topMargin+offsetY);
      exit;
    end;

    { draw text for EAN 13}
    if ((barType=EAN13) and (guardBars) and (length(codeText)>=13))  and FShowText then
    begin
      { first flag     }
      FCurrentCanvas.TextOut((leftMargin+offsetX)-charW-2,barHeightPixels+1+(topMargin+offsetY),copy(codeText,1,1));
      TextW2:= FCurrentCanvas.textWidth(copy(codeText,2,6));
      groupCenterX:=(((FcenterGuardBarStart-FleftGuardBar)-textW2) div 2);
      if (groupCenterX<0) then groupCenterX:=0;

      { next 6 chars   }
      FCurrentCanvas.TextOut(FleftGuardBar+groupCenterX,barHeightPixels+1+(topMargin+offsetY),copy(codeText,2,6));
      TextW2:= FCurrentCanvas.textWidth(copy(codeText,8,6));
      groupCenterX:=((FrightGuardBar-FcenterGuardBarEnd)-TextW2) div 2;
      if (groupCenterX<0) then groupCenterX:=0;

      { last 6 chars    }
      FCurrentCanvas.TextOut(FcenterGuardBarEnd+groupCenterX,barHeightPixels+1+(topMargin+offsetY),copy(codeText,8,6));

      FcurrentY:=barHeightPixels+TextH+1+(topMargin+offsetY);

      exit;
    end;

    { draw text for UPCA  }
    if ((barType=UPCA) and (guardBars) and (length(codeText)>=12))  and FShowText then
    begin
      {first flag  }
      FCurrentCanvas.TextOut((leftMargin+offsetX)-charW-2,barHeightPixels+1+(topMargin+offsetY),copy(codeText,1,1));
      TextW2:= FCurrentCanvas.textWidth(copy(codeText,2,5));
      groupCenterX:= (((FcenterGuardBarStart-FleftGuardBar)-textW2) div 2);
      if (groupCenterX<0) then groupCenterX:=0;

      { next 5 chars }
      FCurrentCanvas.TextOut(FleftGuardBar+groupCenterX,barHeightPixels+1+(topMargin+offsetY),copy(codeText,2,5));

      TextW2:= FCurrentCanvas.textWidth(copy(codeText,7,5));
      groupCenterX:=(((FrightGuardBar-FcenterGuardBarEnd)-textW2) div 2);
      if (groupCenterX<0) then groupCenterX:=0;

      { last 5 chars  }
      FCurrentCanvas.TextOut(FcenterGuardBarEnd+groupCenterX,barHeightPixels+1+(topMargin+offsetY),copy(codeText,7,5));

      { last char  }
      FCurrentCanvas.TextOut(FendOfCode+3,barHeightPixels+1+(topMargin+offsetY),copy(codeText,12,1));

      FcurrentY:=barHeightPixels+TextH+1+(topMargin+offsetY);
      exit;
    end;

    { draw text for EAN 8    }
    if ((barType=EAN8) and (guardBars) and (length(codeText)>=8))  and FShowText then
    begin
      TextW2:= FCurrentCanvas.textWidth(copy(codeText,1,4));
      groupCenterX:=((FcenterGuardBarStart-FleftGuardBar)-textW2) div 2;
      if (groupCenterX<0) then groupCenterX:=0;

      { first 4 chars}
      FCurrentCanvas.TextOut(FleftGuardBar+groupCenterX,barHeightPixels+1+(topMargin+offsetY),copy(codeText,1,4));

                        TextW2:= FCurrentCanvas.textWidth(copy(codeText,5,4));
      groupCenterX:=((FrightGuardBar-FcenterGuardBarEnd)-textW2) div 2;
      if (groupCenterX<0) then groupCenterX:=0;

      { last 4 chars   }
      FCurrentCanvas.TextOut(FcenterGuardBarEnd+groupCenterX,barHeightPixels+1+(topMargin+offsetY),copy(codeText,5,4));

      FcurrentY:=barHeightPixels+TextH+1+(topMargin+offsetY);

      exit;
    end;

    { draw text for UPCE   }
    if ((barType=UPCE) and (guardBars))  and FShowText then
    begin
      { first flag}
      FCurrentCanvas.TextOut((leftMargin+offsetX)-charW-2,barHeightPixels+1+(topMargin+offsetY),copy(code,1,1));


      groupCenterX:=((FrightGuardBar-FleftGuardBar)-textW) div 2;
      if (groupCenterX<0) then groupCenterX:=0;

      { last 4 chars   }
      FCurrentCanvas.TextOut(FleftGuardBar+groupCenterX,barHeightPixels+1+(topMargin+offsetY),codeText);

      FcurrentY:=barHeightPixels+TextH+1+(topMargin+offsetY);

      exit;
    end;

    toCenterX:=(FendOfCode-(leftMargin+offsetX)-TextW) div 2;

		if (toCenterX<0) then toCenterX:=0;

    if FShowText then
    begin
      if (not textOnTop) then
        FCurrentCanvas.TextOut((leftMargin+offsetX)+toCenterX,BarHeightPixels+(textH div 2)+(topMargin+offsetY), codeText)
      else
        FCurrentCanvas.TextOut((leftMargin+offsetX)+toCenterX,(topMargin+offsetY)-(textH div 2)-textH,codeText);

      FcurrentY := BarHeightPixels + TextH+(textH div 2)+(topMargin+offsetY);
    end;
  end;
end;

{ apply tilde}
function TBarcode1D.applyTilde(code:String):String;
var
i:integer;
c:String;
longi:integer;
nextc:String;
ascString:String;
asc:integer;
dummy:integer;
begin


	   longi:=length(code);
	   result:='';

	   i:=0;
           while (i<longi) do begin

		   c:=code[i+1];

		   if (c='~') then begin
			   { process special cases}

			   if (i<(longi-1)) then begin

			   nextc:=code[i+1+1];

			   { quoting character}
			   if (nextc='~') then begin
				  result:=result+'~';
				  i:=i+1;
			   end else begin

			   { dNNN}
			    if (i<(longi-3)) then begin
				   ascString:=copy(code,i+1,3);

				   val(ascString,asc,dummy);

				   if (asc>255) then asc:=255;

				   { the ascii value}
				   result:=result+chr(asc);

				   i:=i+3;
			   end;
                           end; { not escape    }

			  end; { we have two chars at least   }

		   end else begin
			   { no tilde   }
			   result:=result+c;
		   end;
           i:=i+1;
	   end; {while}

end;



constructor TBarcode1D.Create(Owner: TComponent);
begin
  inherited Create(Owner);

  backColor:=clWhite;
  barColor:=clBlack;
  topMargin:=20;
  leftMargin:=20;
  FbarType:=CODE39;
  postnetHeightTallBar:=20;
  postnetHeightShortBar:=10;
  FguardBars := True;
  UPCESytem := '1';
  CODABARStartChar := 'A';
  CODABARStopChar := 'B';
  UPCEANSupplement2 := False;
  UPCEANSupplement5 := False;
  Code128Set := 'B' ;
  checkCharacter := True ;
  X := 1 ;
  N := 2 ;
  I := 1 ;
  H := 0.45;
  L := 0  ;
  supSeparation := 10 ;
  supHeight := 0.8  ;
  extraHeight := 0  ;
  code := '1234567';

  FFont:=TFont.create();
  FFont.Color := clBlack;
  FFont.Height := 10;
  FFont.Size := 10;
  FFont.Name := 'MS Sans Serif';

  height := 200;
  width := 200;
  OffsetX := 0;
  OffsetY := 0;

  FAutoFit := true;
  FShowText := true;
  //drawText := True;
  //FontColor := clBlack;
end;

procedure TBarcode1D.PaintToCanvas(c: TCanvas);
begin
  if AutoFit then
    paintBarcodeAutoSize(c)
  else
    paintBarcode(c);
end;

function TBarcode1D.GetActualSize(ReferenceDevice: HDC): TSize;
var
  MetaFile: TMetaFile;
begin
  if not FSizeCalculated then
  begin
    MetaFile := GetMetafilePicture(ReferenceDevice);
    FreeAndNil(Metafile);
    FSizeCalculated := true;
  end;

  if not FVerticalOrientation then
  begin
    Result.cx := CurrentX + LeftMargin;
    Result.cy := CurrentY + TopMargin;
  end  
  else
  begin
    Result.cy := CurrentX + LeftMargin;
    Result.cx := CurrentY + TopMargin;
  end;
end;

function TBarcode1D.GetMetafilePicture(ReferenceDevice: HDC): TMetafile;
var
  Canvas: TMetafileCanvas;
begin
  Result := TMetafile.Create;

  if not FSizeCalculated then
  begin
    Result.Width := 100;
    Result.Height := 100;
    Canvas := TMetafileCanvas.Create(Result, ReferenceDevice);
    try
      paintBarcodeAutoSize(Canvas);
    finally
      FreeAndNil(Canvas);
    end;

    FreeAndNil(Result);
    Result := TMetafile.Create;
  end;

  Result.Width := CurrentX + LeftMargin;
  Result.Height := CurrentY + TopMargin;

  Canvas := TMetafileCanvas.Create(Result, ReferenceDevice);
  try
    PaintToCanvas(Canvas);
  finally
    FreeAndNil(Canvas);
  end;
end;

procedure TBarcode1D.SetVerticalOrientation(const Value: boolean);
begin
  FVerticalOrientation := Value;
end;

end.
