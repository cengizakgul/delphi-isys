
{*******************************************************}
{							}
{       Delphi SQLDirect Component Library		}
{       SQLDirect include file				}
{                                                       }
{       Copyright (c) 1997,2002 by Yuri Sheino		}
{                                                       }
{*******************************************************}

{$A+}	{+ Aligned record fields }
{$B-}	{- Complete boolean eval }
{$P+}	{- Open string params }
{$R-}	{- Range-Checking }
{$V-}	{- Var-String Checking }
{$X+}	{- Extended syntax }
{$IFDEF WIN32}
{$J+}	{- Writeable structured consts }
{$H+}	{- Use long strings by default }
{$ENDIF}

{ Include it for DEBUGGING }
{ $D+}	{ DEBUGINFO ON }
{ $L+}	{ LOCALSYMBOLS ON }
{ $O-}	{ OPTIMIZATION OFF }

{$IFDEF VER100} { Borland Delphi 3.0 }
  {$DEFINE SD_D3}
{$ENDIF}

{$IFDEF VER110} { Borland C++Builder 3.0 }
  {$DEFINE SD_C3}
  { $ObjExportAll On}
{$ENDIF}

{$IFDEF VER120} { Borland Delphi 4.0 }
  {$DEFINE SD_D4}
  {$DEFINE SD_VCL4}
{$ENDIF}

{$IFDEF VER125} { Borland C++Builder 4.0 }
  {$DEFINE SD_C4}
  {$DEFINE SD_VCL4}
  { $ObjExportAll On}
{$ENDIF}

{$IFDEF VER130} { Borland Delphi 5.0 & Borland C++Builder 5.0 }
  {$DEFINE SD_D4}
  {$DEFINE SD_VCL4}
  {$DEFINE SD_VCL5}
{$ENDIF}

{$IFDEF VER140} { Borland Delphi 6.0 & Borland C++Builder 6.0 }
  {$DEFINE SD_D4}
  {$DEFINE SD_VCL4}
  {$DEFINE SD_VCL5}
  {$DEFINE SD_VCL6}
{$ENDIF}

{$IFDEF VER150} { Borland Delphi 7.0 }
  {$DEFINE SD_D4}
  {$DEFINE SD_VCL4}
  {$DEFINE SD_VCL5}
  {$DEFINE SD_VCL6}
{$ENDIF}