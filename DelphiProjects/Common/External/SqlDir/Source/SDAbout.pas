
{*******************************************************}
{							}
{       Delphi SQLDirect Component Library		}
{       SQLDirect About Box    				}
{                                                       }
{       Copyright (c) 1997,2002 by Yuri Sheino		}
{                                                       }
{*******************************************************}

unit SDAbout;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ShellApi, SDConsts;

type
  { TSDAboutDlg }
  TSDAboutDlg = class(TForm)
    imLogo: TImage;
    lblCopyright: TLabel;
    bvCopyright: TBevel;
    btOk: TButton;
    lblHome: TLabel;
    lblOrder: TLabel;
    lblProductName: TLabel;
    lblVersion: TLabel;
    lblSupport: TLabel;
    Label1: TLabel;
    lblOrderSQLDirectStd: TLabel;
    lblOrderSQLDirectPro: TLabel;
    procedure lblHomeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure lblOrderMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblSupportMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SDAboutDlg: TSDAboutDlg;

procedure ShowAboutBox;
procedure ReadFileVersInfo(const FileName: string; var ProductName, VersStr: string);

implementation

{$R *.DFM}

procedure ShowAboutBox;
var
  Dlg: TSDAboutDlg;
begin
  Application.CreateForm(TSDAboutDlg, Dlg);
  try
    Dlg.ShowModal;
  finally
    Dlg.Free;
  end;
end;

procedure ReadFileVersInfo(const FileName: string; var ProductName, VersStr: string);
type
  TTransTable = array[0..0,0..1] of Word;
var
  bOk: Boolean;
  InfoSize, Len: DWORD;
  lpInfoData: Pointer;
  szStringValue: PChar;
  pFileInfo: ^TVSFixedFileInfo;
  sBeta, sLangCharSet: string;
  pTransTable: ^TTransTable;
begin
  sBeta := '';
  InfoSize := GetFileVersionInfoSize( PChar(FileName), Len );

  if InfoSize <= 0 then Exit;

  lpInfoData := StrAlloc(InfoSize);
  try
    bOk := GetFileVersionInfo( PChar(FileName), 0, InfoSize, lpInfoData );
    bOk := bOk and VerQueryValue( lpInfoData, '\', Pointer(pFileInfo), Len );

    if not bOk then Exit;

    if (pFileInfo.dwFileFlags and VS_FF_PRERELEASE) <> 0 then
      sBeta := 'beta';

    VersStr := Format('%d.%d.%d%s',
    	[pFileInfo.dwFileVersionMS shr 16,
         pFileInfo.dwFileVersionMS and $0000FFFF,
	 pFileInfo.dwFileVersionLS shr 16,
         sBeta
        ]);

    if VerQueryValue(lpInfoData, '\VarFileInfo\Translation', Pointer(pTransTable), Len) then
      sLangCharSet := IntToHex(pTransTable^[0,0], 4) + IntToHex(pTransTable^[0,1], 4);

    if VerQueryValue(lpInfoData, PChar('\StringFileInfo\'+sLangCharSet+'\ProductName'), Pointer(szStringValue), Len) then
      ProductName := szStringValue;
  finally
    StrDispose(lpInfoData);
  end;
end;

{ TSDAboutDlg }
procedure TSDAboutDlg.FormCreate(Sender: TObject);
var
  sFileName, sProductName, sProductVers: string;
begin
  sProductName := 'SQLDirect';
  sProductVers := '';

  SetLength(sFileName, 260);
  GetModuleFileName(HInstance, PChar(sFileName), 260);

  ReadFileVersInfo(PChar(sFileName), sProductName, sProductVers);

  lblProductName.Caption:= sProductName;
  lblVersion.Caption	:= Format('Version %s', [sProductVers]);
end;

procedure TSDAboutDlg.lblHomeMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Inst: THandle;
begin
  if mbLeft = Button then begin
    Inst := ShellExecute(0, 'open', PChar(SLinkSQLDirectHome), nil, nil, 0);
    if Inst <= 32 then
      MessageDlg(Format(SErrOpenWwwSite, [SLinkSQLDirectHome]), mtError, [mbOK], 0);
  end;
end;

procedure TSDAboutDlg.lblOrderMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Inst: THandle;
  sLink: string;
begin
  if mbLeft = Button then begin
    if Sender = lblOrderSQLDirectStd then
      sLink := SLinkSQLDirectStdOrder
    else
      sLink := SLinkSQLDirectProOrder;

    Inst := ShellExecute(0, 'open', PChar(sLink), nil, nil, 0);
    if Inst <= 32 then
      MessageDlg(Format(SErrOpenWwwSite, [sLink]), mtError, [mbOK], 0);
  end;
end;

procedure TSDAboutDlg.lblSupportMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Inst: THandle;
begin
  if mbLeft = Button then begin
    Inst := ShellExecute(0, 'open', PChar(SLinkSQLDirectSupport), nil, nil, 0);
    if Inst <= 32 then
      MessageDlg( Format(SErrOpenWwwSite, [SLinkSQLDirectSupport]), mtError, [mbOK], 0 );
  end;
end;

end.
