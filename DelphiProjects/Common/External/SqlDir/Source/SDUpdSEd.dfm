�
 TSDUPDATESQLEDITFORM 0X	  TPF0TSDUpdateSQLEditFormSDUpdateSQLEditFormLeft� Top� ActiveControlcbTableNameBorderStylebsDialogCaptionUpdateSQL EditorClientHeightClientWidth�Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterOnCloseQueryFormCloseQueryOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TPageControlpcUpdateSQLLeft Top Width�Height� 
ActivePage	tsOptionsAlignalClientTabOrder  	TTabSheet	tsOptionsCaptionOptions 	TGroupBox	gbxGenSQLLeftTop Width�Height� CaptionSQL GenerationTabOrder  TLabellblTableNameLeft	TopWidth=HeightCaptionTable &Name:  TLabellblKeyFieldsLeft� TopWidth3HeightCaption&Key Fields:  TLabellblUpdateFieldsLeft.TopWidthDHeightCaptionUpdate &Fields:  	TComboBoxcbTableNameLeft	Top!Width� Height
ItemHeightTabOrder   TListBoxlbKeyFieldsLeft� Top!Width� Height� 
ItemHeightMultiSelect		PopupMenupmFieldsTabOrder  TListBoxlbUpdateFieldsLeft.Top!Width� Height� 
ItemHeightMultiSelect		PopupMenupmFieldsTabOrder  TButtonbtDataSetDefaultsLeftToprWidthxHeightCaption&DataSet DefaultsTabOrderOnClickDataSetDefaultsClick  TButtonbtGenerateSQLLeftTop� WidthxHeightCaption&Generate SQLTabOrderOnClickGenerateSQLClick  TButtonbtGetTableFieldsLeftTopTWidthxHeightCaptionGet &Table FieldsTabOrderOnClickGetTableFieldsClick    	TTabSheettsSQLCaptionSQL TLabel
lblSQLTextLeftTop0Width0HeightCaption
S&QL Text:  TMemo	meSQLTextLeftTop?Width�Height� 
ScrollBarsssBothTabOrder OnExitSQLTextExit  TRadioGrouprgrStatementTypeLeftTopWidth�Height%CaptionStatement TypeColumns	ItemIndex Items.Strings&Modify&Insert&Delete TabOrderOnClickStatementTypeClick    TPanel	pnlButtonLeft Top� Width�Height!AlignalBottom
BevelOuterbvNoneTabOrder TButtonbtOkLeft<TopWidthKHeightCaption&OKDefault	ModalResultTabOrder   TButtonbtCancelLeft�TopWidthKHeightCancel	CaptionCancelModalResultTabOrder   
TPopupMenupmFieldsLeft� Topc 	TMenuItemmiSelectAllCaption&Select AllOnClickSelectAllClick  	TMenuItem
miClearAllCaption
&Clear AllOnClickClearAllClick    