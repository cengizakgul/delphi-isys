
{*******************************************************}
{							}
{       Delphi SQLDirect Component Library		}
{       SQLDirect Components Registration		}
{                                                       }
{       Copyright (c) 1997,2002 by Yuri Sheino		}
{                                                       }
{*******************************************************}

unit SDReg;

interface

{$I SqlDir.inc}

uses
  Windows, Messages, SysUtils, Classes,
{$IFNDEF SD_VCL6}
  DsgnIntf,
{$ELSE}
  DesignIntf, DesignEditors,
{$ENDIF}
{$IFDEF SD_VCL5}
  DBReg,
{$ENDIF}
  DSDesign, Dialogs, TypInfo,
  StdCtrls, Controls, Graphics, Forms;

type
  { TSDStringProperty }
  TSDStringProperty = class(TStringProperty)
    procedure GetValueList(List: TStrings); virtual; abstract;
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;


  { TSDSessionNameProperty }
  TSDSessionNameProperty = class(TSDStringProperty)
    procedure GetValueList(List: TStrings); override;
  end;

  { TSDDatabaseNameProperty }
  TSDDatabaseNameProperty = class(TSDStringProperty)
    procedure GetValueList(List: TStrings); override;
  end;

  { TSDStoredProcNameProperty }
  TSDStoredProcNameProperty = class(TSDStringProperty)
    procedure GetValueList(List: TStrings); override;
  end;


  { TSDParamsProperty }
  TSDParamsProperty = class(TPropertyEditor)
  public
    function GetValue: string; override;
    function GetAttributes: TPropertyAttributes; override;
  end;

  { TSDQueryParamsProperty }
  TSDQueryParamsProperty = class(TSDParamsProperty)
    procedure Edit; override;
  end;

  { TSPStoredProcParamsProperty }
  TSPStoredProcParamsProperty = class(TSDParamsProperty)
    procedure Edit; override;
  end;

{ TSDDefaultEditor }

  TSDDefaultEditor = class(TComponentEditor)
    procedure Edit; override;
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

{ TSDDataSetEditor }

  TSDDataSetEditor = class(
  {$IFDEF SD_VCL5}      TDataSetEditor
  {$ELSE}               TComponentEditor{$ENDIF} )
  public
    procedure Edit; override;
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

{ TSDQueryEditor }

  TSDQueryEditor = class(TSDDataSetEditor)
  public
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

{ TSDStoredProcEditor }

  TSDStoredProcEditor = class(TSDDataSetEditor)
  public
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

{ TSDUpdateSQLEditor }

  TSDUpdateSQLEditor = class(TSDDefaultEditor)
  public
    procedure Edit; override;
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

procedure Register;

implementation

uses
  Db,
  SDEngine, SDConsts, SDCsbSrv,
{$IFDEF SD_VCL4}
  ColnEdit,
{$ELSE}
  SDQPDlg, SDSpPDlg,
{$ENDIF}
{$IFDEF EVAL}SDRemind,{$ENDIF}
  SDAbout, SDUpdSEd;


type
{$IFDEF SD_VCL6}
  TSDHelperDesigner  	= IDesigner;
{$ELSE}
 {$IFDEF SD_VCL5}
  TSDHelperDesigner  	= IFormDesigner;
 {$ELSE}
  {$IFDEF SD_VCL4}
  TSDHelperDesigner  	= IDesigner;
  {$ELSE}
  TSDHelperDesigner  	= TDesigner;
  {$ENDIF}
 {$ENDIF}
{$ENDIF}

function GetAboutVerbName: string;
var
  sFileName, sProductName, sProductVers: string;
begin
  sProductName := 'SQLDirect';
  sProductVers := '';

  SetLength(sFileName, 260);
  GetModuleFileName(HInstance, PChar(sFileName), 260);

  ReadFileVersInfo(PChar(sFileName), sProductName, sProductVers);

  Result := sProductName + ' ' + sProductVers;
end;

function ShowQueryParamsEditor(Designer: TSDHelperDesigner; Query: TSDQuery): Boolean;
{$IFNDEF SD_VCL4}
var
  List: TSDHelperParams;
{$ENDIF}
begin
  Result := False;
{$IFDEF SD_VCL4}
  ColnEdit.ShowCollectionEditorClass(Designer, TCollectionEditor, Query, Query.Params, 'Params', [] );
{$ELSE}
  List := TSDHelperParams.Create;
  try
    List.Assign(Query.Params);
    if EditQueryParams(Query, List) and not(List.IsEqual(Query.Params)) then begin
      Query.Close;
      Query.Params := List;
      Result := True
    end;
  finally
    List.Free;
  end;
{$ENDIF}
end;

function ShowStoredProcParamsEditor(Designer: TSDHelperDesigner; StoredProc: TSDStoredProc): Boolean;
{$IFNDEF SD_VCL4}
var
  List: TSDHelperParams;
{$ENDIF}
begin
  Result := False;
{$IFDEF SD_VCL4}
  if StoredProc.DescriptionsAvailable then begin
    if (StoredProc.ParamCount = 0) and (not StoredProc.Prepared) then
      StoredProc.Prepare;
    ColnEdit.ShowCollectionEditorClass(Designer, TCollectionEditor, StoredProc, StoredProc.Params, 'Params', [] )
  end else
    ColnEdit.ShowCollectionEditorClass(Designer, TCollectionEditor, StoredProc, StoredProc.Params, 'Params', [coAdd, coDelete] );
{$ELSE}
  List := TSDHelperParams.Create;
  try
    if (StoredProc.ParamCount > 0) or StoredProc.Prepared or
       (not StoredProc.DescriptionsAvailable)
    then
      List.Assign(StoredProc.Params)
    else begin
      StoredProc.Prepare;
      try
        List.Assign(StoredProc.Params);
      finally
        StoredProc.UnPrepare;
      end;
    end;
    if EditStoredProcParams(StoredProc, List) and
       not(List.IsEqual(StoredProc.Params))
    then begin
      StoredProc.Close;
      StoredProc.Params := List;
      Result := True;
    end;
  finally
    List.Free;
  end;
{$ENDIF}
end;

function ShowUpdateSQLDesigner(Designer: TSDHelperDesigner; AUpdateSQL: TSDUpdateSQL): Boolean;
begin
  Result := False;
  if EditUpdateSQL(AUpdateSQL) then begin
    if Assigned(Designer) then Designer.Modified;
    Result := True;
  end;
end;


{ TSDDefaultEditor }
procedure TSDDefaultEditor.Edit;
begin
  ExecuteVerb(0);
end;

procedure TSDDefaultEditor.ExecuteVerb(Index: Integer);
begin
  case Index of
    0: ShowAboutBox;
  end;
end;

function TSDDefaultEditor.GetVerb(Index: Integer): string;
begin
  case Index of
    0: Result := GetAboutVerbName;
  end;
end;

function TSDDefaultEditor.GetVerbCount: Integer;
begin
  Result := 1;
end;

{ TSDDataSetEditor }
procedure TSDDataSetEditor.Edit;
begin
  ExecuteVerb(2);
end;

procedure TSDDataSetEditor.ExecuteVerb(Index: Integer);
begin
  case Index of
    0: ShowAboutBox;
    2:
{$IFDEF SD_VCL5}
       DSDesign.ShowFieldsEditor(Designer, TSDDataSet(Component), GetDSDesignerClass); // or inherited ExecuteVerb(Index)
{$ELSE}
       DSDesign.ShowDatasetDesigner(Designer, TSDDataSet(Component));
{$ENDIF}
  end;
end;

function TSDDataSetEditor.GetVerb(Index: Integer): string;
begin
  case Index of
    0: Result := GetAboutVerbName;
    1: Result := '-';
    2: Result := SDatasetDesigner; // // or Result := inherited GetVerb(Index)
  end;
end;

function TSDDataSetEditor.GetVerbCount: Integer;
begin
  Result := 3;
end;


{ TSDQueryEditor }
procedure TSDQueryEditor.ExecuteVerb(Index: Integer);
var
  q: TSDQuery;
begin
  q := Component as TSDQuery;
  case Index of
    0,
    2: inherited ExecuteVerb(Index);
    3: if ShowQueryParamsEditor(Designer, q) and Assigned(Designer) then
         Designer.Modified;
  end;
end;

function TSDQueryEditor.GetVerb(Index: Integer): string;
begin
  case Index of
    0,
    1,
    2: Result := inherited GetVerb(Index);
    3: Result := SBindVerb;
  end;
end;

function TSDQueryEditor.GetVerbCount: Integer;
begin
  Result := 4;
end;


{ TSDStoredProcEditor }
procedure TSDStoredProcEditor.ExecuteVerb(Index: Integer);
var
  sp: TSDStoredProc;
begin
  sp := Component as TSDStoredProc;
  case Index of
    0,
    2: inherited ExecuteVerb(Index);
    3: if ShowStoredProcParamsEditor(Designer, sp) and Assigned(Designer) then
         Designer.Modified;
  end;
end;

function TSDStoredProcEditor.GetVerb(Index: Integer): string;
begin
  case Index of
    0,
    1,
    2: Result := inherited GetVerb(Index);
    3: Result := SBindVerb;
  end;
end;

function TSDStoredProcEditor.GetVerbCount: Integer;
begin
  Result := 4;
end;

{ TSDUpdateSQLEditor }
procedure TSDUpdateSQLEditor.Edit;
begin
  ExecuteVerb(2);
end;

procedure TSDUpdateSQLEditor.ExecuteVerb(Index: Integer);
begin
  case Index of
    0: ShowAboutBox;
    2: ShowUpdateSQLDesigner(Designer, TSDUpdateSQL(Component));
  end;
end;

function TSDUpdateSQLEditor.GetVerb(Index: Integer): string;
begin
  case Index of
    0: Result := GetAboutVerbName;
    1: Result := '-';
    2: Result := SUpdateSQLEditor;
  end;
end;

function TSDUpdateSQLEditor.GetVerbCount: Integer;
begin
  Result := 3;
end;


{ TSDStringProperty }
function TSDStringProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList, paMultiSelect];
end;

procedure TSDStringProperty.GetValues(Proc: TGetStrProc);
var
  I: Integer;
  Values: TStringList;
begin
  Values := TStringList.Create;
  try
    GetValueList(Values);
    for I := 0 to Values.Count - 1 do Proc(Values[I]);
  finally
    Values.Free;
  end;
end;


{ TSDSessionNameProperty }
procedure TSDSessionNameProperty.GetValueList(List: TStrings);
var
  i: Integer;
begin
  for i:=0 to Sessions.Count-1 do
    List.Add( Sessions[i].SessionName );
end;

{ TSDDatabaseNameProperty }
procedure TSDDatabaseNameProperty.GetValueList(List: TStrings);
begin
  (GetComponent(0) as TSDDataSet).DBSession.GetDatabaseNames(List);
end;

{ TSDStoredProcNameProperty }
procedure TSDStoredProcNameProperty.GetValueList(List: TStrings);
var
  sp: TSDStoredProc;
  db: TSDDatabase;
begin
  sp := (GetComponent(0) as TSDStoredProc);

  db := Sessions.FindDatabase( sp.DatabaseName );

  if (db = nil) and ((sp.Database = nil) or (not sp.Database.Connected)) then
    db := sp.OpenDatabase;
  ASSERT( db <> nil );
  Sessions.List[db.SessionName].GetStoredProcNames( db.DatabaseName, List );
end;


{ TSDParamsProperty }
function TSDParamsProperty.GetValue: string;
begin
  Result := Format('(%s)', [TSDHelperParams.ClassName]);
end;

function TSDParamsProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paDialog];
end;


{ TSDQueryParamsProperty }
procedure TSDQueryParamsProperty.Edit;
var
  q: TSDQuery;
begin
  q := GetComponent(0) as TSDQuery;
  if ShowQueryParamsEditor(Designer, q) then
    Modified;
end;

{ TSPStoredParamsProperty }
procedure TSPStoredProcParamsProperty.Edit;
var
  sp: TSDStoredProc;
begin
  sp := GetComponent(0) as TSDStoredProc;
  if ShowStoredProcParamsEditor(Designer, sp) then
    Modified;
end;


procedure Register;
begin
  RegisterComponents(srSQLDirect, [TSDSession, TSDDatabase, TSDQuery, TSDStoredProc, TSDUpdateSQL, TSDSQLBaseServer]);

  RegisterPropertyEditor(TypeInfo(string),	TSDDatabase, 'SessionName', TSDSessionNameProperty);
  RegisterPropertyEditor(TypeInfo(string),	TSDDataSet, 'DatabaseName', TSDDatabaseNameProperty);
  RegisterPropertyEditor(TypeInfo(string),	TSDDataSet, 'SessionName', TSDSessionNameProperty);
  RegisterPropertyEditor(TypeInfo(string), 	TSDStoredProc, 'StoredProcName', TSDStoredProcNameProperty);
{$IFDEF SD_VCL4}
  RegisterPropertyEditor(TypeInfo(TParams),	TSDQuery, 'Params', TSDQueryParamsProperty);
  RegisterPropertyEditor(TypeInfo(TParams),	TSDStoredProc, 'Params', TSPStoredProcParamsProperty);
{$ELSE}
  RegisterPropertyEditor(TypeInfo(TSDParams),	TSDQuery, 'Params', TSDQueryParamsProperty);
  RegisterPropertyEditor(TypeInfo(TSDParams),	TSDStoredProc, 'Params', TSPStoredProcParamsProperty);
{$ENDIF}
  RegisterComponentEditor(TSDSession, TSDDefaultEditor);
  RegisterComponentEditor(TSDDatabase, TSDDefaultEditor);

  RegisterComponentEditor(TSDDataSet, TSDDataSetEditor);
  RegisterComponentEditor(TSDQuery, TSDQueryEditor);
  RegisterComponentEditor(TSDStoredProc, TSDStoredProcEditor);
  RegisterComponentEditor(TSDUpdateSQL, TSDUpdateSQLEditor);

  RegisterComponentEditor(TSDSQLBaseServer, TSDDefaultEditor);
{$IFDEF EVAL}
  ShowReminderBox;
{$ENDIF}

{$IFDEF SD_VCL5}
    { Property Category registration }
  RegisterPropertiesInCategory(
        {$IFDEF SD_VCL6}sDatabaseCategoryName{$ELSE}TDatabaseCategory{$ENDIF},
        TSDDatabase,
        ['ServerType', 'SessionName', 'DatabaseName', 'RemoteDatabase']);

  RegisterPropertiesInCategory(
        {$IFDEF SD_VCL6}sDatabaseCategoryName{$ELSE}TDatabaseCategory{$ENDIF},
        TSDDataSet,
        ['DatabaseName', 'Preservation']);

  RegisterPropertiesInCategory(
        {$IFDEF SD_VCL6}sDatabaseCategoryName{$ELSE}TDatabaseCategory{$ENDIF},
        TSDDataSetUpdateObject,
        ['*SQL']);
{$ENDIF}
end;

end.
