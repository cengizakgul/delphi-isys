
{**********************************************************}
{							   }
{       Delphi SQLDirect Component Library		   }
{	ODBC 3.0 Interface Unit		 		   }
{                                                          }
{       Copyright (c) 1997,2002 by Yuri Sheino		   }
{                                                          }
{**********************************************************}

unit SDOdbc;

interface

{$I SqlDir.inc}
                                
uses
  Windows, SysUtils, Classes, Db, Dialogs, Forms, SyncObjs,
{$IFDEF SD_VCL6}
  Variants,
{$ENDIF}
  SDEngine, SDConsts;

const

{*****************************************************************
** SQL.H - This is the the main include for ODBC Core functions.
**
**      Updated 5/12/93 for 2.00 specification
**      Updated 5/23/94 for 2.01 specification
**      Updated 11/10/94 for 2.10 specification
**      Updated 04/10/95 for 2.50 specification
**      Updated 6/6/95  for 3.00 specification
*********************************************************************}

// special length/indicator values
  SQL_NULL_DATA       	= -1;
  SQL_DATA_AT_EXEC    	= -2;

// return values from functions - RETCODE values
  SQL_SUCCESS           = 0;
  SQL_SUCCESS_WITH_INFO = 1;
  SQL_STILL_EXECUTING   = 2;
  SQL_NEED_DATA         = 99;
  SQL_NO_DATA           = 100;
  SQL_NO_DATA_FOUND    	= SQL_NO_DATA;
  SQL_ERROR             = -1;
  SQL_INVALID_HANDLE    = -2;

// flags for null-terminated string
  SQL_NTS             	= -3;      	// NTS = Null Terminated String
  SQL_NTSL            	= Longint(-3);	// NTS = Null Terminated String

// maximum message length
  SQL_MAX_MESSAGE_LENGTH= 512;

// date/time length constants
  SQL_DATE_LEN          = 10;
  SQL_TIME_LEN          = 8;  	// add P+1 if precision is nonzero
  SQL_TIMESTAMP_LEN	= 19;	// add P+1 if precision is nonzero

// handle type identifiers
  SQL_HANDLE_ENV        = 1;
  SQL_HANDLE_DBC        = 2;
  SQL_HANDLE_STMT       = 3;
  SQL_HANDLE_DESC       = 4;

// Environment attributes; note SQL_CONNECTTYPE, SQL_SYNC_POINT are also
// environment attributes that are settable at the connection level
  SQL_ATTR_OUTPUT_NTS  	= 10001;

// connection attributes
  SQL_ATTR_AUTO_IPD            	= 10001;
  SQL_ATTR_METADATA_ID         	= 10014;

// statement attributes for SQLGetStmtOption/SQLSetStmtOption
  SQL_ATTR_APP_ROW_DESC        	= 10010;
  SQL_ATTR_APP_PARAM_DESC      	= 10011;
  SQL_ATTR_IMP_ROW_DESC        	= 10012;
  SQL_ATTR_IMP_PARAM_DESC      	= 10013;
  SQL_ATTR_CURSOR_SCROLLABLE   	= (-1);
  SQL_ATTR_CURSOR_SENSITIVITY  	= (-2);

// SQL_ATTR_CURSOR_SCROLLABLE values
  SQL_NONSCROLLABLE            	= 0;
  SQL_SCROLLABLE               	= 1;

// identifiers of fields in the SQL descriptor
  SQL_DESC_COUNT                = 1001;
  SQL_DESC_TYPE                 = 1002;
  SQL_DESC_LENGTH               = 1003;
  SQL_DESC_OCTET_LENGTH_PTR     = 1004;
  SQL_DESC_PRECISION            = 1005;
  SQL_DESC_SCALE                = 1006;
  SQL_DESC_DATETIME_INTERVAL_CODE=1007;
  SQL_DESC_NULLABLE             = 1008;
  SQL_DESC_INDICATOR_PTR        = 1009;
  SQL_DESC_DATA_PTR             = 1010;
  SQL_DESC_NAME                 = 1011;
  SQL_DESC_UNNAMED              = 1012;
  SQL_DESC_OCTET_LENGTH         = 1013;
  SQL_DESC_ALLOC_TYPE           = 1099;

// identifiers of fields in the diagnostics area
  SQL_DIAG_RETURNCODE       	= 1;
  SQL_DIAG_NUMBER           	= 2;
  SQL_DIAG_ROW_COUNT        	= 3;
  SQL_DIAG_SQLSTATE         	= 4;
  SQL_DIAG_NATIVE           	= 5;
  SQL_DIAG_MESSAGE_TEXT     	= 6;
  SQL_DIAG_DYNAMIC_FUNCTION 	= 7;
  SQL_DIAG_CLASS_ORIGIN     	= 8;
  SQL_DIAG_SUBCLASS_ORIGIN  	= 9;
  SQL_DIAG_CONNECTION_NAME  	= 10;
  SQL_DIAG_SERVER_NAME     	= 11;
  SQL_DIAG_DYNAMIC_FUNCTION_CODE= 12;

// dynamic function codes
  SQL_DIAG_ALTER_DOMAIN		=  3;
  SQL_DIAG_ALTER_TABLE         	=  4;
  SQL_DIAG_CALL			=  7;
  SQL_DIAG_CREATE_ASSERTION	=  6;
  SQL_DIAG_CREATE_CHARACTER_SET	=  8;
  SQL_DIAG_CREATE_COLLATION	= 10;
  SQL_DIAG_CREATE_DOMAIN 	= 23;
  SQL_DIAG_CREATE_SCHEMA 	= 64;
  SQL_DIAG_CREATE_INDEX        	= -1;
  SQL_DIAG_CREATE_TABLE        	= 77;
  SQL_DIAG_CREATE_TRANSLATION 	= 79;
  SQL_DIAG_CREATE_VIEW         	= 84;
  SQL_DIAG_DELETE_WHERE        	= 19;
  SQL_DIAG_DROP_ASSERTION 	= 24;
  SQL_DIAG_DROP_CHARACTER_SET 	= 25;
  SQL_DIAG_DROP_COLLATION      	= 26;
  SQL_DIAG_DROP_DOMAIN		= 27;
  SQL_DIAG_DROP_INDEX          	= -2;
  SQL_DIAG_DROP_SCHEMA	    	= 31;
  SQL_DIAG_DROP_TABLE          	= 32;
  SQL_DIAG_DROP_TRANSLATION     = 33;
  SQL_DIAG_DROP_VIEW           	= 36;
  SQL_DIAG_DYNAMIC_DELETE_CURSOR= 38;
  SQL_DIAG_DYNAMIC_UPDATE_CURSOR= 81;
  SQL_DIAG_GRANT               	= 48;
  SQL_DIAG_INSERT              	= 50;
  SQL_DIAG_REVOKE              	= 59;
  SQL_DIAG_SELECT_CURSOR       	= 85;
  SQL_DIAG_UNKNOWN_STATEMENT   	=  0;
  SQL_DIAG_UPDATE_WHERE        	= 82;

// Standard SQL data type codes
  SQL_UNKNOWN_TYPE     	= 0;
  SQL_CHAR             	= 1;
  SQL_NUMERIC          	= 2;
  SQL_DECIMAL          	= 3;
  SQL_INTEGER          	= 4;
  SQL_SMALLINT         	= 5;
  SQL_FLOAT            	= 6;
  SQL_REAL             	= 7;
  SQL_DOUBLE           	= 8;
  SQL_DATETIME         	= 9;
  SQL_VARCHAR          	=12;

// One-parameter shortcuts for date/time data types
  SQL_TYPE_DATE     	= 91;
  SQL_TYPE_TIME     	= 92;
  SQL_TYPE_TIMESTAMP	= 93;

// Statement attribute values for cursor sensitivity
  SQL_UNSPECIFIED    	= 0;
  SQL_INSENSITIVE    	= 1;
  SQL_SENSITIVE      	= 2;

// SQLGetTypeInfo() request for all data types
  SQL_ALL_TYPES        	= 0;

// Default conversion code for SQLBindCol(), SQLBindParam() and SQLGetData()
  SQL_DEFAULT       	= 99;

// SQLGetData() code indicating that the application row descriptor specifies the data type
  SQL_ARD_TYPE      	= (-99);

// SQL date/time type subcodes
  SQL_CODE_DATE      	= 1;
  SQL_CODE_TIME      	= 2;
  SQL_CODE_TIMESTAMP 	= 3;

// CLI attribute/option values
  SQL_FALSE            	= 0;
  SQL_TRUE             	= 1;

// values of NULLABLE field in descriptor
  SQL_NO_NULLS        	= 0;
  SQL_NULLABLE        	= 1;

// NULL status defines; these are used in SQLColAttributes, SQLDescribeCol,
// to describe the nullability of a column in a table.
  SQL_NULLABLE_UNKNOWN	= 2;

// Values returned by SQLGetTypeInfo() to show WHERE clause supported
  SQL_PRED_NONE  	= 0;
  SQL_PRED_CHAR        	= 1;
  SQL_PRED_BASIC       	= 2;

// values of UNNAMED field in descriptor used in SQLColAttribute
  SQL_NAMED            	= 0;
  SQL_UNNAMED          	= 1;

// values of ALLOC_TYPE field in descriptor
  SQL_DESC_ALLOC_AUTO	= 1;
  SQL_DESC_ALLOC_USER	= 2;

// SQLFreeStmt option values
  SQL_CLOSE            	= 0;
  SQL_DROP             	= 1;
  SQL_UNBIND           	= 2;
  SQL_RESET_PARAMS     	= 3;

// Codes used for FetchOrientation in SQLFetchScroll(), and in SQLDataSources()
  SQL_FETCH_NEXT       	= 1;
  SQL_FETCH_FIRST      	= 2;

// Other codes used for FetchOrientation in SQLFetchScroll()
  SQL_FETCH_LAST     	= 3;
  SQL_FETCH_PRIOR    	= 4;
  SQL_FETCH_ABSOLUTE 	= 5;
  SQL_FETCH_RELATIVE 	= 6;

// SQLEndTran() and SQLTransact() option values
  SQL_COMMIT           	= 0;
  SQL_ROLLBACK         	= 1;

// null handles returned by SQLAllocHandle()
  SQL_NULL_HENV      	= 0;
  SQL_NULL_HDBC      	= 0;
  SQL_NULL_HSTMT     	= 0;
  SQL_NULL_HDESC     	= 0;

// null handle used in place of parent handle when allocating HENV
  SQL_NULL_HANDLE	= 0;

// Values that may appear in the result set of SQLSpecialColumns()
  SQL_SCOPE_CURROW     	= 0;
  SQL_SCOPE_TRANSACTION	= 1;
  SQL_SCOPE_SESSION    	= 2;

// Defines for SQLSpecialColumns (returned in the result set)
  SQL_PC_UNKNOWN       	= 0;
  SQL_PC_NON_PSEUDO    	= 1;
  SQL_PC_PSEUDO        	= 2;

// Reserved value for the IdentifierType argument of SQLSpecialColumns()
  SQL_ROW_IDENTIFIER 	= 1;

// Reserved values for UNIQUE argument of SQLStatistics()
  SQL_INDEX_UNIQUE     	= 0;
  SQL_INDEX_ALL        	= 1;

// Values that may appear in the result set of SQLStatistics()
  SQL_INDEX_CLUSTERED  	= 1;
  SQL_INDEX_HASHED     	= 2;
  SQL_INDEX_OTHER      	= 3;

// SQLGetFunctions() values to identify ODBC APIs - supported functions
  SQL_API_SQLALLOCCONNECT	=  1;
  SQL_API_SQLALLOCENV          	=  2;
  SQL_API_SQLALLOCHANDLE       	= 1001;
  SQL_API_SQLALLOCSTMT         	=  3;
  SQL_API_SQLBINDCOL           	=  4;
  SQL_API_SQLBINDPARAM         	=  1002;
  SQL_API_SQLCANCEL            	=  5;
  SQL_API_SQLCLOSECURSOR       	= 1003;
  SQL_API_SQLCOLATTRIBUTE      	= 6;
  SQL_API_SQLCOLUMNS           	= 40;
  SQL_API_SQLCONNECT           	=  7;
  SQL_API_SQLCOPYDESC          	=  1004;
  SQL_API_SQLDATASOURCES       	= 57;
  SQL_API_SQLDESCRIBECOL       	=  8;
  SQL_API_SQLDISCONNECT        	=  9;
  SQL_API_SQLENDTRAN           	= 1005;
  SQL_API_SQLERROR             	= 10;
  SQL_API_SQLEXECDIRECT        	= 11;
  SQL_API_SQLEXECUTE           	= 12;
  SQL_API_SQLFETCH             	= 13;
  SQL_API_SQLFETCHSCROLL       	= 1021;
  SQL_API_SQLFREECONNECT       	= 14;
  SQL_API_SQLFREEENV           	= 15;
  SQL_API_SQLFREEHANDLE        	= 1006;
  SQL_API_SQLFREESTMT          	= 16;
  SQL_API_SQLGETCONNECTATTR    	= 1007;
  SQL_API_SQLGETCONNECTOPTION  	= 42;
  SQL_API_SQLGETCURSORNAME     	= 17;
  SQL_API_SQLGETDATA           	= 43;
  SQL_API_SQLGETDESCFIELD      	= 1008;
  SQL_API_SQLGETDESCREC        	= 1009;
  SQL_API_SQLGETDIAGFIELD      	= 1010;
  SQL_API_SQLGETDIAGREC        	= 1011;
  SQL_API_SQLGETENVATTR        	= 1012;
  SQL_API_SQLGETFUNCTIONS      	= 44;
  SQL_API_SQLGETINFO           	= 45;
  SQL_API_SQLGETSTMTATTR       	= 1014;
  SQL_API_SQLGETSTMTOPTION     	= 46;
  SQL_API_SQLGETTYPEINFO       	= 47;
  SQL_API_SQLNUMRESULTCOLS     	= 18;
  SQL_API_SQLPARAMDATA         	= 48;
  SQL_API_SQLPREPARE           	= 19;
  SQL_API_SQLPUTDATA           	= 49;
  SQL_API_SQLROWCOUNT          	= 20;
  SQL_API_SQLSETCONNECTATTR    	= 1016;
  SQL_API_SQLSETCONNECTOPTION  	= 50;
  SQL_API_SQLSETCURSORNAME     	= 21;
  SQL_API_SQLSETDESCFIELD      	= 1017;
  SQL_API_SQLSETDESCREC        	= 1018;
  SQL_API_SQLSETENVATTR        	= 1019;
  SQL_API_SQLSETPARAM          	= 22;
  SQL_API_SQLSETSTMTATTR       	= 1020;
  SQL_API_SQLSETSTMTOPTION     	= 51;
  SQL_API_SQLSPECIALCOLUMNS    	= 52;
  SQL_API_SQLSTATISTICS        	= 53;
  SQL_API_SQLTABLES            	= 54;
  SQL_API_SQLTRANSACT          	= 23;

// Information requested by SQLGetInfo()
  SQL_MAX_DRIVER_CONNECTIONS    	= 0;
  SQL_MAXIMUM_DRIVER_CONNECTIONS       	= SQL_MAX_DRIVER_CONNECTIONS;
  SQL_MAX_CONCURRENT_ACTIVITIES       	= 1;
  SQL_MAXIMUM_CONCURRENT_ACTIVITIES    	= SQL_MAX_CONCURRENT_ACTIVITIES;
  SQL_DATA_SOURCE_NAME              	=  2;
  SQL_FETCH_DIRECTION                  	=  8;
  SQL_SERVER_NAME                      	= 13;
  SQL_SEARCH_PATTERN_ESCAPE            	= 14;
  SQL_DBMS_NAME                        	= 17;
  SQL_DBMS_VER                         	= 18;
  SQL_ACCESSIBLE_TABLES                	= 19;
  SQL_ACCESSIBLE_PROCEDURES            	= 20;
  SQL_CURSOR_COMMIT_BEHAVIOR           	= 23;
  SQL_DATA_SOURCE_READ_ONLY            	= 25;
  SQL_DEFAULT_TXN_ISOLATION            	= 26;
  SQL_IDENTIFIER_CASE                  	= 28;
  SQL_IDENTIFIER_QUOTE_CHAR            	= 29;
  SQL_MAX_COLUMN_NAME_LEN              	= 30;
  SQL_MAXIMUM_COLUMN_NAME_LENGTH       	= SQL_MAX_COLUMN_NAME_LEN;
  SQL_MAX_CURSOR_NAME_LEN              	= 31;
  SQL_MAXIMUM_CURSOR_NAME_LENGTH       	= SQL_MAX_CURSOR_NAME_LEN;
  SQL_MAX_SCHEMA_NAME_LEN        	= 32;
  SQL_MAXIMUM_SCHEMA_NAME_LENGTH	= SQL_MAX_SCHEMA_NAME_LEN;
  SQL_MAX_CATALOG_NAME_LEN           	= 34;
  SQL_MAXIMUM_CATALOG_NAME_LENGTH	= SQL_MAX_CATALOG_NAME_LEN;
  SQL_MAX_TABLE_NAME_LEN               	= 35;
  SQL_SCROLL_CONCURRENCY               	= 43;
  SQL_TXN_CAPABLE                      	= 46;
  SQL_TRANSACTION_CAPABLE              	= SQL_TXN_CAPABLE;
  SQL_USER_NAME                        	= 47;
  SQL_TXN_ISOLATION_OPTION             	= 72;
  SQL_TRANSACTION_ISOLATION_OPTION     	= SQL_TXN_ISOLATION_OPTION;
  SQL_INTEGRITY                      	= 73;
  SQL_GETDATA_EXTENSIONS               	= 81;
  SQL_NULL_COLLATION                   	= 85;
  SQL_ALTER_TABLE                      	= 86;
  SQL_ORDER_BY_COLUMNS_IN_SELECT       	= 90;
  SQL_SPECIAL_CHARACTERS               	= 94;
  SQL_MAX_COLUMNS_IN_GROUP_BY          	= 97;
  SQL_MAXIMUM_COLUMNS_IN_GROUP_BY      	= SQL_MAX_COLUMNS_IN_GROUP_BY;
  SQL_MAX_COLUMNS_IN_INDEX             	= 98;
  SQL_MAXIMUM_COLUMNS_IN_INDEX         	= SQL_MAX_COLUMNS_IN_INDEX;
  SQL_MAX_COLUMNS_IN_ORDER_BY          	= 99;
  SQL_MAXIMUM_COLUMNS_IN_ORDER_BY      	= SQL_MAX_COLUMNS_IN_ORDER_BY;
  SQL_MAX_COLUMNS_IN_SELECT           	= 100;
  SQL_MAXIMUM_COLUMNS_IN_SELECT       	= SQL_MAX_COLUMNS_IN_SELECT;
  SQL_MAX_COLUMNS_IN_TABLE            	= 101;
  SQL_MAX_INDEX_SIZE                  	= 102;
  SQL_MAXIMUM_INDEX_SIZE              	= SQL_MAX_INDEX_SIZE;
  SQL_MAX_ROW_SIZE                    	= 104;
  SQL_MAXIMUM_ROW_SIZE                	= SQL_MAX_ROW_SIZE;
  SQL_MAX_STATEMENT_LEN               	= 105;
  SQL_MAXIMUM_STATEMENT_LENGTH        	= SQL_MAX_STATEMENT_LEN;
  SQL_MAX_TABLES_IN_SELECT            	= 106;
  SQL_MAXIMUM_TABLES_IN_SELECT        	= SQL_MAX_TABLES_IN_SELECT;
  SQL_MAX_USER_NAME_LEN               	= 107;
  SQL_MAXIMUM_USER_NAME_LENGTH        	= SQL_MAX_USER_NAME_LEN;

  SQL_OJ_CAPABILITIES                	= 115;
  SQL_OUTER_JOIN_CAPABILITIES        	= SQL_OJ_CAPABILITIES;

  SQL_XOPEN_CLI_YEAR               	= 10000;
  SQL_CURSOR_SENSITIVITY           	= 10001;
  SQL_DESCRIBE_PARAMETER           	= 10002;
  SQL_CATALOG_NAME                 	= 10003;
  SQL_COLLATION_SEQ                	= 10004;
  SQL_MAX_IDENTIFIER_LEN           	= 10005;
  SQL_MAXIMUM_IDENTIFIER_LENGTH    	= SQL_MAX_IDENTIFIER_LEN;

// SQL_ALTER_TABLE bitmasks
  SQL_AT_ADD_COLUMN           	= $00000001;
  SQL_AT_DROP_COLUMN           	= $00000002;
  SQL_AT_ADD_CONSTRAINT        	= $00000008;

// SQL_ASYNC_MODE values
  SQL_AM_NONE       	= 0;
  SQL_AM_CONNECTION	= 1;
  SQL_AM_STATEMENT 	= 2;

// SQL_CURSOR_COMMIT_BEHAVIOR and SQL_CURSOR_ROLLBACK_BEHAVIOR values
  SQL_CB_DELETE                	= $0000;
  SQL_CB_CLOSE                 	= $0001;
  SQL_CB_PRESERVE              	= $0002;

// SQL_FETCH_DIRECTION bitmasks

// SQL_FETCH_DIRECTION masks
  SQL_FD_FETCH_NEXT           	= $00000001;
  SQL_FD_FETCH_FIRST          	= $00000002;
  SQL_FD_FETCH_LAST           	= $00000004;
  SQL_FD_FETCH_PRIOR          	= $00000008;
  SQL_FD_FETCH_ABSOLUTE       	= $00000010;
  SQL_FD_FETCH_RELATIVE       	= $00000020;

// SQL_GETDATA_EXTENSIONS bitmasks
  SQL_GD_ANY_COLUMN            	= $00000001;
  SQL_GD_ANY_ORDER             	= $00000002;

// SQL_IDENTIFIER_CASE values
  SQL_IC_UPPER       	 	= $0001;
  SQL_IC_LOWER        		= $0002;
  SQL_IC_SENSITIVE    		= $0003;
  SQL_IC_MIXED        		= $0004;

// SQL_OJ_CAPABILITIES bitmasks. NB: this means 'outer join', not what  you may be thinking
  SQL_OJ_LEFT                  	= $00000001;
  SQL_OJ_RIGHT                 	= $00000002;
  SQL_OJ_FULL                  	= $00000004;
  SQL_OJ_NESTED                	= $00000008;
  SQL_OJ_NOT_ORDERED           	= $00000010;
  SQL_OJ_INNER                 	= $00000020;
  SQL_OJ_ALL_COMPARISON_OPS    	= $00000040;

// SQL_SCROLL_CONCURRENCY bitmasks
  SQL_SCCO_READ_ONLY           	= $00000001;
  SQL_SCCO_LOCK                	= $00000002;
  SQL_SCCO_OPT_ROWVER          	= $00000004;
  SQL_SCCO_OPT_VALUES          	= $00000008;

// SQL_TXN_CAPABLE values
  SQL_TC_NONE                  	= $0000;
  SQL_TC_DML                   	= $0001;
  SQL_TC_ALL                   	= $0002;
  SQL_TC_DDL_COMMIT            	= $0003;
  SQL_TC_DDL_IGNORE            	= $0004;

// SQL_TXN_ISOLATION_OPTION bitmasks
  SQL_TXN_READ_UNCOMMITTED        	= $00000001;
  SQL_TRANSACTION_READ_UNCOMMITTED	= SQL_TXN_READ_UNCOMMITTED;
  SQL_TXN_READ_COMMITTED          	= $00000002;
  SQL_TRANSACTION_READ_COMMITTED  	= SQL_TXN_READ_COMMITTED;
  SQL_TXN_REPEATABLE_READ         	= $00000004;
  SQL_TRANSACTION_REPEATABLE_READ 	= SQL_TXN_REPEATABLE_READ;
  SQL_TXN_SERIALIZABLE            	= $00000008;
  SQL_TRANSACTION_SERIALIZABLE    	= SQL_TXN_SERIALIZABLE;

// SQL_NULL_COLLATION values
  SQL_NC_HIGH                 	= 0;
  SQL_NC_LOW                  	= 1;


{*****************************************************************
** SQLTYPES.H - This file defines the types used in ODBC
**
**		Created 04/10/95 for 2.50 specification
**		Updated 12/11/95 for 3.00 specification
*********************************************************************}
type
  UCHAR		= Byte;
  ULONG		= DWORD;
  USHORT	= Word;

  SCHAR		= Char;
  SDWORD	= Longint;
  SWORD		= Smallint;
  UDWORD	= DWORD;
  UWORD		= Word;
  SDOUBLE	= Double;
  SFLOAT	= Single;
  SLONG		= Longint;
  SSHORT	= Smallint;
  LDOUBLE	= Extended;

  PTR		= Pointer;

type
// API declaration data types
  SQLCHAR	= UCHAR;
  SQLSCHAR	= SCHAR;
  SQLDATE	= Byte;		//C type: unsigned char
  SQLDECIMAL	= Byte;		//C type: unsigned char
  SQLDOUBLE	= SDOUBLE;
  SQLFLOAT	= SDOUBLE;
  SQLINTEGER	= SDWORD;
  SQLUINTEGER	= UDWORD;
  SQLNUMERIC	= SQLDECIMAL;
  SQLPOINTER	= PTR;
  SQLREAL	= SFLOAT;
  SQLSMALLINT	= SWORD;
  SQLUSMALLINT	= UWORD;
  SQLTIME	= SQLDATE;
  SQLTIMESTAMP	= SQLDATE;
  SQLVARCHAR	= UCHAR;


// function return type
  SQLRETURN	= SQLSMALLINT;

// generic data structures
  SQLHANDLE	= Pointer;

  SQLHENV	= SQLHANDLE;
  SQLHDBC	= SQLHANDLE;
  SQLHSTMT	= SQLHANDLE;
  SQLHDESC	= SQLHANDLE;

  PSQLHANDLE	= ^SQLHANDLE;
  PSQLHENV	= ^SQLHENV;
  PSQLHDBC	= ^SQLHDBC;
  PSQLHSTMT	= ^SQLHSTMT;


  RETCODE	= Smallint;

  PSQLINTEGER	= ^SQLINTEGER;
  PSQLSMALLINT	= ^SQLSMALLINT;
  PSQLUSMALLINT	= ^SQLUSMALLINT;
  PSQLUINTEGER	= ^SQLUINTEGER;
  PSQLPOINTER	= ^SQLPOINTER;

  SQLHWND	= HWND;

// transfer types for DATE, TIME, TIMESTAMP
  TDATE_STRUCT	= record
    year:	SQLSMALLINT;
    month:	SQLUSMALLINT;
    day:	SQLUSMALLINT;
  end;
  TSQL_DATE_STRUCT = TDATE_STRUCT;

  TTIME_STRUCT	= record
    hour:	SQLUSMALLINT;
    minute:	SQLUSMALLINT;
    second:	SQLUSMALLINT;
  end;
  TSQL_TIME_STRUCT = TTIME_STRUCT;

  TTIMESTAMP_STRUCT = record
    year:	SQLSMALLINT;
    month:	SQLUSMALLINT;
    day:	SQLUSMALLINT;
    hour:	SQLUSMALLINT;
    minute:	SQLUSMALLINT;
    second:	SQLUSMALLINT;
    fraction:	SQLUINTEGER;	// fraction of a second
  end;
  TSQL_TIMESTAMP_STRUCT	= TTIMESTAMP_STRUCT;

  TSQL_YEAR_MONTH_STRUCT = record
    year:	SQLUINTEGER;
    month:	SQLUINTEGER;
  end;

  TSQL_DAY_SECOND_STRUCT = record
    day:	SQLUINTEGER;
    hour:	SQLUINTEGER;
    minute:	SQLUINTEGER;
    second:	SQLUINTEGER;
    fraction:	SQLUINTEGER;
  end;

  TSQL_BOOKMARK	= SQLUINTEGER;

{*******************************************************************************
** SQLEXT.H - This is the include for applications using
**             the Microsoft SQL Extensions
**
**		Updated 07/25/95 for 3.00 specification
**		Updated 01/12/96 for 3.00 preliminary release
** 		Updated 09/16/96 for 3.00 SDK release
**		Updated 11/21/96 for bug #4436
********************************************************************************}
const
  SQL_SQLSTATE_SIZE	= 5;	// size of SQLSTATE
  SQL_MAX_DSN_LENGTH	= 32;	// maximum data source name size

  SQL_MAX_OPTION_STRING_LENGTH	= 256;

//#if (ODBCVER >= 0x0300)
	// an end handle type
  SQL_HANDLE_SENV	= 5;

	// env attribute
  SQL_ATTR_ODBC_VERSION		= 200;
  SQL_ATTR_CONNECTION_POOLING	= 201;
  SQL_ATTR_CP_MATCH		= 202;

	// values for SQL_ATTR_CONNECTION_POOLING
 SQL_CP_OFF	      	= 0;
 SQL_CP_ONE_PER_DRIVER	= 1;
 SQL_CP_ONE_PER_HENV  	= 2;
 SQL_CP_DEFAULT	      	= SQL_CP_OFF;

	// values for SQL_ATTR_CP_MATCH
 SQL_CP_STRICT_MATCH 	= 0;
 SQL_CP_RELAXED_MATCH	= 1;
 SQL_CP_MATCH_DEFAULT	= SQL_CP_STRICT_MATCH;

	// values for SQL_ATTR_ODBC_VERSION
 SQL_OV_ODBC2	= 2;
 SQL_OV_ODBC3	= 3;
//#endif  /* ODBCVER >= 0x0300 */

	// connection attributes
  SQL_ACCESS_MODE              	= 101;
  SQL_AUTOCOMMIT               	= 102;
  SQL_LOGIN_TIMEOUT            	= 103;
  SQL_OPT_TRACE                	= 104;
  SQL_OPT_TRACEFILE            	= 105;
  SQL_TRANSLATE_DLL            	= 106;
  SQL_TRANSLATE_OPTION         	= 107;
  SQL_TXN_ISOLATION            	= 108;
  SQL_CURRENT_QUALIFIER        	= 109;
  SQL_ODBC_CURSORS             	= 110;
  SQL_QUIET_MODE               	= 111;
  SQL_PACKET_SIZE              	= 112;

//#if (ODBCVER >= 0x0300)
	// connection attributes with new names
  SQL_ATTR_ACCESS_MODE         	= SQL_ACCESS_MODE;
  SQL_ATTR_AUTOCOMMIT          	= SQL_AUTOCOMMIT;
  SQL_ATTR_CONNECTION_TIMEOUT	= 113;
  SQL_ATTR_CURRENT_CATALOG     	= SQL_CURRENT_QUALIFIER;
  SQL_ATTR_DISCONNECT_BEHAVIOR 	= 114;
  SQL_ATTR_ENLIST_IN_DTC       	= 1207;
  SQL_ATTR_ENLIST_IN_XA        	= 1208;
  SQL_ATTR_LOGIN_TIMEOUT       	= SQL_LOGIN_TIMEOUT;
  SQL_ATTR_ODBC_CURSORS        	= SQL_ODBC_CURSORS;
  SQL_ATTR_PACKET_SIZE         	= SQL_PACKET_SIZE;
  SQL_ATTR_QUIET_MODE          	= SQL_QUIET_MODE;
  SQL_ATTR_TRACE               	= SQL_OPT_TRACE;
  SQL_ATTR_TRACEFILE           	= SQL_OPT_TRACEFILE;
  SQL_ATTR_TRANSLATE_LIB       	= SQL_TRANSLATE_DLL;
  SQL_ATTR_TRANSLATE_OPTION    	= SQL_TRANSLATE_OPTION;
  SQL_ATTR_TXN_ISOLATION       	= SQL_TXN_ISOLATION;
//#endif  /* ODBCVER >= 0x0300 */

	// SQL_CONNECT_OPT_DRVR_START is not meaningful for 3.0 driver
//#if (ODBCVER < 0x0300)
  SQL_CONNECT_OPT_DRVR_START 	= 1000;
//#endif  /* ODBCVER < 0x0300 */

//#if (ODBCVER < 0x0300)
  SQL_CONN_OPT_MAX             	= SQL_PACKET_SIZE;
  SQL_CONN_OPT_MIN             	= SQL_ACCESS_MODE;
//#endif /* ODBCVER < 0x0300 */

	// SQL_ACCESS_MODE options
  SQL_MODE_READ_WRITE          	= 0;
  SQL_MODE_READ_ONLY           	= 1;
  SQL_MODE_DEFAULT             	= SQL_MODE_READ_WRITE;

	// SQL_AUTOCOMMIT options
  SQL_AUTOCOMMIT_OFF           	= 0;
  SQL_AUTOCOMMIT_ON            	= 1;
  SQL_AUTOCOMMIT_DEFAULT       	= SQL_AUTOCOMMIT_ON;

	// SQL_LOGIN_TIMEOUT options
  SQL_LOGIN_TIMEOUT_DEFAULT    	= 15;

	// SQL_OPT_TRACE options
  SQL_OPT_TRACE_OFF            	= 0;
  SQL_OPT_TRACE_ON             	= 1;
  SQL_OPT_TRACE_DEFAULT        	= SQL_OPT_TRACE_OFF;
  SQL_OPT_TRACE_FILE_DEFAULT   	= '\SQL.LOG';

	// SQL_ODBC_CURSORS options
  SQL_CUR_USE_IF_NEEDED        	= 0;
  SQL_CUR_USE_ODBC             	= 1;
  SQL_CUR_USE_DRIVER           	= 2;
  SQL_CUR_DEFAULT              	= SQL_CUR_USE_DRIVER;

//#if (ODBCVER >= 0x0300)
	// values for SQL_ATTR_DISCONNECT_BEHAVIOR
  SQL_DB_RETURN_TO_POOL        	= 0;
  SQL_DB_DISCONNECT            	= 1;
  SQL_DB_DEFAULT               	= SQL_DB_RETURN_TO_POOL;

	// values for SQL_ATTR_ENLIST_IN_DTC
  SQL_DTC_DONE                 	= 0;
//#endif  /* ODBCVER >= 0x0300 */

	// statement attributes
  SQL_QUERY_TIMEOUT            	= 0;
  SQL_MAX_ROWS                 	= 1;
  SQL_NOSCAN                   	= 2;
  SQL_MAX_LENGTH               	= 3;
  SQL_ASYNC_ENABLE             	= 4;       // same as SQL_ATTR_ASYNC_ENABLE
  SQL_BIND_TYPE                	= 5;
  SQL_CURSOR_TYPE              	= 6;
  SQL_CONCURRENCY              	= 7;
  SQL_KEYSET_SIZE              	= 8;
  SQL_ROWSET_SIZE              	= 9;
  SQL_SIMULATE_CURSOR          	= 10;
  SQL_RETRIEVE_DATA            	= 11;
  SQL_USE_BOOKMARKS            	= 12;
  SQL_GET_BOOKMARK             	= 13;      // GetStmtOption Only
  SQL_ROW_NUMBER               	= 14;      // GetStmtOption Only

//#if (ODBCVER >= 0x0300)
	// statement attributes for ODBC 3.0
  SQL_ATTR_ASYNC_ENABLE        	= 4;
  SQL_ATTR_CONCURRENCY         	= SQL_CONCURRENCY;
  SQL_ATTR_CURSOR_TYPE         	= SQL_CURSOR_TYPE;
  SQL_ATTR_ENABLE_AUTO_IPD     	= 15;
  SQL_ATTR_FETCH_BOOKMARK_PTR  	= 16;
  SQL_ATTR_KEYSET_SIZE         	= SQL_KEYSET_SIZE;
  SQL_ATTR_MAX_LENGTH          	= SQL_MAX_LENGTH;
  SQL_ATTR_MAX_ROWS            	= SQL_MAX_ROWS;
  SQL_ATTR_NOSCAN              	= SQL_NOSCAN;
  SQL_ATTR_PARAM_BIND_OFFSET_PTR= 17;
  SQL_ATTR_PARAM_BIND_TYPE     	= 18;
  SQL_ATTR_PARAM_OPERATION_PTR 	= 19;
  SQL_ATTR_PARAM_STATUS_PTR    	= 20;
  SQL_ATTR_PARAMS_PROCESSED_PTR	= 21;
  SQL_ATTR_PARAMSET_SIZE       	= 22;
  SQL_ATTR_QUERY_TIMEOUT       	= SQL_QUERY_TIMEOUT;
  SQL_ATTR_RETRIEVE_DATA       	= SQL_RETRIEVE_DATA;
  SQL_ATTR_ROW_BIND_OFFSET_PTR 	= 23;
  SQL_ATTR_ROW_BIND_TYPE       	= SQL_BIND_TYPE;
  SQL_ATTR_ROW_NUMBER          	= SQL_ROW_NUMBER;	// GetStmtAttr
  SQL_ATTR_ROW_OPERATION_PTR   	= 24;
  SQL_ATTR_ROW_STATUS_PTR      	= 25;
  SQL_ATTR_ROWS_FETCHED_PTR    	= 26;
  SQL_ATTR_ROW_ARRAY_SIZE      	= 27;
  SQL_ATTR_SIMULATE_CURSOR     	= SQL_SIMULATE_CURSOR;
  SQL_ATTR_USE_BOOKMARKS       	= SQL_USE_BOOKMARKS;
//#endif  /* ODBCVER >= 0x0300 */

//#if (ODBCVER < 0x0300)
  SQL_STMT_OPT_MAX             	= SQL_ROW_NUMBER;
  SQL_STMT_OPT_MIN       	= SQL_QUERY_TIMEOUT;
//#endif    	/* ODBCVER < 0x0300 */

//#if (ODBCVER >= 0x0300)
	// whether an attribute is a pointer or not
  SQL_IS_POINTER	= (-4);
  SQL_IS_UINTEGER  	= (-5);
  SQL_IS_INTEGER  	= (-6);
  SQL_IS_USMALLINT	= (-7);
  SQL_IS_SMALLINT 	= (-8);

	// the value of SQL_ATTR_PARAM_BIND_TYPE
  SQL_PARAM_BIND_BY_COLUMN	= 0;
  SQL_PARAM_BIND_TYPE_DEFAULT	= SQL_PARAM_BIND_BY_COLUMN;
//#endif  /* ODBCVER >= 0x0300 */

	// SQL_QUERY_TIMEOUT options
  SQL_QUERY_TIMEOUT_DEFAULT    	= 0;

	// SQL_MAX_ROWS options
  SQL_MAX_ROWS_DEFAULT         	= 0;

	// SQL_NOSCAN options
  SQL_NOSCAN_OFF               	= 0;     //      1.0 FALSE
  SQL_NOSCAN_ON                	= 1;     //      1.0 TRUE
  SQL_NOSCAN_DEFAULT           	= SQL_NOSCAN_OFF;

	// SQL_MAX_LENGTH options
  SQL_MAX_LENGTH_DEFAULT      	= 0;

	// values for SQL_ATTR_ASYNC_ENABLE
  SQL_ASYNC_ENABLE_OFF         	= 0;
  SQL_ASYNC_ENABLE_ON          	= 1;
  SQL_ASYNC_ENABLE_DEFAULT     	= SQL_ASYNC_ENABLE_OFF;

	// SQL_BIND_TYPE options
  SQL_BIND_BY_COLUMN           	= 0;
  SQL_BIND_TYPE_DEFAULT        	= SQL_BIND_BY_COLUMN;  // Default value

	// SQL_CONCURRENCY options
  SQL_CONCUR_READ_ONLY         	= 1;
  SQL_CONCUR_LOCK              	= 2;
  SQL_CONCUR_ROWVER            	= 3;
  SQL_CONCUR_VALUES            	= 4;
  SQL_CONCUR_DEFAULT           	= SQL_CONCUR_READ_ONLY; // Default value

	// SQL_CURSOR_TYPE options
  SQL_CURSOR_FORWARD_ONLY      	= 0;
  SQL_CURSOR_KEYSET_DRIVEN     	= 1;
  SQL_CURSOR_DYNAMIC           	= 2;
  SQL_CURSOR_STATIC            	= 3;
  SQL_CURSOR_TYPE_DEFAULT      	= SQL_CURSOR_FORWARD_ONLY; // Default value

	// SQL_ROWSET_SIZE options
  SQL_ROWSET_SIZE_DEFAULT     	= 1;

	// SQL_KEYSET_SIZE options
  SQL_KEYSET_SIZE_DEFAULT      	= 0;

	// SQL_SIMULATE_CURSOR options
  SQL_SC_NON_UNIQUE            	= 0;
  SQL_SC_TRY_UNIQUE            	= 1;
  SQL_SC_UNIQUE                	= 2;

	// SQL_RETRIEVE_DATA options
  SQL_RD_OFF                  	= 0;
  SQL_RD_ON                   	= 1;
  SQL_RD_DEFAULT              	= SQL_RD_ON;

	// SQL_USE_BOOKMARKS options
  SQL_UB_OFF                   	= 0;
  SQL_UB_ON                    	= 1;
  SQL_UB_DEFAULT               	= SQL_UB_OFF;

//#if (ODBCVER >= 0x0300)
	// New values for SQL_USE_BOOKMARKS attribute
  SQL_UB_FIXED               	= SQL_UB_ON;
  SQL_UB_VARIABLE            	= 2;
//#endif  /* ODBCVER >= 0x0300 */

	// SQL extended datatypes
  SQL_DATE                    	= 9;
//#if (ODBCVER >= 0x0300)
  SQL_INTERVAL                	= 10;
//#endif  /* ODBCVER >= 0x0300 */
  SQL_TIME                    	= 10;
  SQL_TIMESTAMP               	= 11;
  SQL_LONGVARCHAR             	= (-1);
  SQL_BINARY                  	= (-2);
  SQL_VARBINARY               	= (-3);
  SQL_LONGVARBINARY           	= (-4);
  SQL_BIGINT                  	= (-5);
  SQL_TINYINT                 	= (-6);
  SQL_BIT                     	= (-7);

//#if (ODBCVER >= 0x0300)
	// interval code
  SQL_CODE_YEAR               	= 1;
  SQL_CODE_MONTH              	= 2;
  SQL_CODE_DAY                	= 3;
  SQL_CODE_HOUR               	= 4;
  SQL_CODE_MINUTE             	= 5;
  SQL_CODE_SECOND             	= 6;
  SQL_CODE_YEAR_TO_MONTH      	= 7;
  SQL_CODE_DAY_TO_HOUR        	= 8;
  SQL_CODE_DAY_TO_MINUTE      	= 9;
  SQL_CODE_DAY_TO_SECOND      	= 10;
  SQL_CODE_HOUR_TO_MINUTE     	= 11;
  SQL_CODE_HOUR_TO_SECOND     	= 12;
  SQL_CODE_MINUTE_TO_SECOND   	= 13;

  SQL_INTERVAL_YEAR           	= (100 + SQL_CODE_YEAR);
  SQL_INTERVAL_MONTH          	= (100 + SQL_CODE_MONTH);
  SQL_INTERVAL_DAY            	= (100 + SQL_CODE_DAY);
  SQL_INTERVAL_HOUR           	= (100 + SQL_CODE_HOUR);
  SQL_INTERVAL_MINUTE         	= (100 + SQL_CODE_MINUTE);
  SQL_INTERVAL_SECOND         	= (100 + SQL_CODE_SECOND);
  SQL_INTERVAL_YEAR_TO_MONTH  	= (100 + SQL_CODE_YEAR_TO_MONTH);
  SQL_INTERVAL_DAY_TO_HOUR    	= (100 + SQL_CODE_DAY_TO_HOUR);
  SQL_INTERVAL_DAY_TO_MINUTE  	= (100 + SQL_CODE_DAY_TO_MINUTE);
  SQL_INTERVAL_DAY_TO_SECOND  	= (100 + SQL_CODE_DAY_TO_SECOND);
  SQL_INTERVAL_HOUR_TO_MINUTE 	= (100 + SQL_CODE_HOUR_TO_MINUTE);
  SQL_INTERVAL_HOUR_TO_SECOND 	= (100 + SQL_CODE_HOUR_TO_SECOND);
  SQL_INTERVAL_MINUTE_TO_SECOND	= (100 + SQL_CODE_MINUTE_TO_SECOND);

{#else
#define SQL_INTERVAL_YEAR                       (-80)
#define SQL_INTERVAL_MONTH                      (-81)
#define SQL_INTERVAL_YEAR_TO_MONTH              (-82)
#define SQL_INTERVAL_DAY                        (-83)
#define SQL_INTERVAL_HOUR                       (-84)
#define SQL_INTERVAL_MINUTE                     (-85)
#define SQL_INTERVAL_SECOND                     (-86)
#define SQL_INTERVAL_DAY_TO_HOUR                (-87)
#define SQL_INTERVAL_DAY_TO_MINUTE              (-88)
#define SQL_INTERVAL_DAY_TO_SECOND              (-89)
#define SQL_INTERVAL_HOUR_TO_MINUTE             (-90)
#define SQL_INTERVAL_HOUR_TO_SECOND             (-91)
#define SQL_INTERVAL_MINUTE_TO_SECOND           (-92)
}

//#endif  /* ODBCVER >= 0x0300 */


  SQL_UNICODE                  	= (-95);
  SQL_UNICODE_VARCHAR          	= (-96);
  SQL_UNICODE_LONGVARCHAR      	= (-97);
  SQL_UNICODE_CHAR             	= SQL_UNICODE;

//#if (ODBCVER < 0x0300)
  SQL_TYPE_DRIVER_START        	= SQL_INTERVAL_YEAR;
  SQL_TYPE_DRIVER_END          	= SQL_UNICODE_LONGVARCHAR;
//#endif  /* ODBCVER < 0x0300 */

	// C datatype to SQL datatype mapping      	SQL types
  SQL_C_CHAR   		= SQL_CHAR;             // CHAR, VARCHAR, DECIMAL, NUMERIC
  SQL_C_LONG 	  	= SQL_INTEGER;          // INTEGER
  SQL_C_SHORT  		= SQL_SMALLINT;         // SMALLINT
  SQL_C_FLOAT  		= SQL_REAL;             // REAL
  SQL_C_DOUBLE 		= SQL_DOUBLE;           // FLOAT, DOUBLE
//#if (ODBCVER >= 0x0300)
  SQL_C_NUMERIC		= SQL_NUMERIC;
//#endif  /* ODBCVER >= 0x0300 */
  SQL_C_DEFAULT	= 99;

  SQL_SIGNED_OFFSET   	= (-20);
  SQL_UNSIGNED_OFFSET 	= (-22);

	// C datatype to SQL datatype mapping
  SQL_C_DATE     	=  SQL_DATE;
  SQL_C_TIME     	=  SQL_TIME;
  SQL_C_TIMESTAMP	=  SQL_TIMESTAMP;
//#if (ODBCVER >= 0x0300)
  SQL_C_TYPE_DATE              	= SQL_TYPE_DATE;
  SQL_C_TYPE_TIME              	= SQL_TYPE_TIME;
  SQL_C_TYPE_TIMESTAMP         	= SQL_TYPE_TIMESTAMP;
  SQL_C_INTERVAL_YEAR          	= SQL_INTERVAL_YEAR;
  SQL_C_INTERVAL_MONTH         	= SQL_INTERVAL_MONTH;
  SQL_C_INTERVAL_DAY           	= SQL_INTERVAL_DAY;
  SQL_C_INTERVAL_HOUR          	= SQL_INTERVAL_HOUR;
  SQL_C_INTERVAL_MINUTE        	= SQL_INTERVAL_MINUTE;
  SQL_C_INTERVAL_SECOND        	= SQL_INTERVAL_SECOND;
  SQL_C_INTERVAL_YEAR_TO_MONTH 	= SQL_INTERVAL_YEAR_TO_MONTH;
  SQL_C_INTERVAL_DAY_TO_HOUR   	= SQL_INTERVAL_DAY_TO_HOUR;
  SQL_C_INTERVAL_DAY_TO_MINUTE 	= SQL_INTERVAL_DAY_TO_MINUTE;
  SQL_C_INTERVAL_DAY_TO_SECOND 	= SQL_INTERVAL_DAY_TO_SECOND;
  SQL_C_INTERVAL_HOUR_TO_MINUTE	= SQL_INTERVAL_HOUR_TO_MINUTE;
  SQL_C_INTERVAL_HOUR_TO_SECOND	= SQL_INTERVAL_HOUR_TO_SECOND;
  SQL_C_INTERVAL_MINUTE_TO_SECOND= SQL_INTERVAL_MINUTE_TO_SECOND;
//#endif  /* ODBCVER >= 0x0300 */

  SQL_C_BINARY    	= SQL_BINARY;
  SQL_C_BIT       	= SQL_BIT;

//#if (ODBCVER >= 0x0300)
  SQL_C_SBIGINT   	= (SQL_BIGINT+SQL_SIGNED_OFFSET);     	// SIGNED BIGINT
  SQL_C_UBIGINT  	= (SQL_BIGINT+SQL_UNSIGNED_OFFSET);   	// UNSIGNED BIGINT
//#endif  /* ODBCVER >= 0x0300 */

  SQL_C_TINYINT   	= SQL_TINYINT;
  SQL_C_SLONG     	= (SQL_C_LONG+SQL_SIGNED_OFFSET);    	// SIGNED INTEGER
  SQL_C_SSHORT    	= (SQL_C_SHORT+SQL_SIGNED_OFFSET);   	// SIGNED SMALLINT
  SQL_C_STINYINT  	= (SQL_TINYINT+SQL_SIGNED_OFFSET);   	// SIGNED TINYINT
  SQL_C_ULONG     	= (SQL_C_LONG+SQL_UNSIGNED_OFFSET);  	// UNSIGNED INTEGER
  SQL_C_USHORT    	= (SQL_C_SHORT+SQL_UNSIGNED_OFFSET); 	// UNSIGNED SMALLINT
  SQL_C_UTINYINT  	= (SQL_TINYINT+SQL_UNSIGNED_OFFSET); 	// UNSIGNED TINYINT
  SQL_C_BOOKMARK  	= SQL_C_ULONG;                       	// BOOKMARK

  SQL_TYPE_NULL   	= 0;

//#if (ODBCVER < 0x0300)
  SQL_TYPE_MIN 		= SQL_BIT;
  SQL_TYPE_MAX 		= SQL_VARCHAR;
//#endif

//#if (ODBCVER >= 0x0300)
  SQL_C_VARBOOKMARK 	= SQL_C_BINARY;
//#endif  /* ODBCVER >= 0x0300 */

	// define for SQL_DIAG_ROW_NUMBER and SQL_DIAG_COLUMN_NUMBER
//#if (ODBCVER >= 0x0300)
  SQL_NO_ROW_NUMBER       	= (-1);
  SQL_NO_COLUMN_NUMBER    	= (-1);
  SQL_ROW_NUMBER_UNKNOWN	= (-2);
  SQL_COLUMN_NUMBER_UNKNOWN	= (-2);
//#endif  /* ODBCVER >= 0x0300 */

	// SQLBindParameter extensions
  SQL_DEFAULT_PARAM           	= (-5);
  SQL_IGNORE                  	= (-6);
//#if (ODBCVER >= 0x0300)
  SQL_COLUMN_IGNORE           	= SQL_IGNORE;
//#endif  /* ODBCVER >= 0x0300 */
  SQL_LEN_DATA_AT_EXEC_OFFSET 	= (-100);
//  SQL_LEN_DATA_AT_EXEC(length)	= (-(length)+SQL_LEN_DATA_AT_EXEC_OFFSET);
function SQL_LEN_DATA_AT_EXEC(ALength: SQLINTEGER): SQLINTEGER;

const
	// binary length for driver specific attributes
  SQL_LEN_BINARY_ATTR_OFFSET   	= (-100);
//  SQL_LEN_BINARY_ATTR(length)  	= (-(length)+SQL_LEN_BINARY_ATTR_OFFSET);

	// Defines used by Driver Manager when mapping SQLSetParam to SQLBindParameter
{  SQL_PARAM_TYPE_DEFAULT      	= SQL_PARAM_INPUT_OUTPUT; }
  SQL_SETPARAM_VALUE_MAX       	= (-1);

	// SQLColAttributes defines
  SQL_COLUMN_COUNT             	= 0;
  SQL_COLUMN_NAME              	= 1;
  SQL_COLUMN_TYPE              	= 2;
  SQL_COLUMN_LENGTH            	= 3;
  SQL_COLUMN_PRECISION         	= 4;
  SQL_COLUMN_SCALE             	= 5;
  SQL_COLUMN_DISPLAY_SIZE      	= 6;
  SQL_COLUMN_NULLABLE          	= 7;
  SQL_COLUMN_UNSIGNED          	= 8;
  SQL_COLUMN_MONEY             	= 9;
  SQL_COLUMN_UPDATABLE         	= 10;
  SQL_COLUMN_AUTO_INCREMENT    	= 11;
  SQL_COLUMN_CASE_SENSITIVE    	= 12;
  SQL_COLUMN_SEARCHABLE        	= 13;
  SQL_COLUMN_TYPE_NAME         	= 14;
  SQL_COLUMN_TABLE_NAME        	= 15;
  SQL_COLUMN_OWNER_NAME        	= 16;
  SQL_COLUMN_QUALIFIER_NAME    	= 17;
  SQL_COLUMN_LABEL             	= 18;
  SQL_COLATT_OPT_MAX           	= SQL_COLUMN_LABEL;
//#if (ODBCVER < 0x0300)
  SQL_COLUMN_DRIVER_START      	= 1000;
//#endif  /* ODBCVER < 0x0300 */

  SQL_COLATT_OPT_MIN           	= SQL_COLUMN_COUNT;

	// SQLColAttributes subdefines for SQL_COLUMN_UPDATABLE
  SQL_ATTR_READONLY            	= 0;
  SQL_ATTR_WRITE               	= 1;
  SQL_ATTR_READWRITE_UNKNOWN   	= 2;

// SQLColAttributes subdefines for SQL_COLUMN_SEARCHABLE
// These are also used by SQLGetInfo
  SQL_UNSEARCHABLE             	= 0;
  SQL_LIKE_ONLY                	= 1;
  SQL_ALL_EXCEPT_LIKE          	= 2;
  SQL_SEARCHABLE               	= 3;
  SQL_PRED_SEARCHABLE          	= SQL_SEARCHABLE;

// Special return values for SQLGetData
  SQL_NO_TOTAL                	= (-4);

	// New defines for SEARCHABLE column in SQLGetTypeInfo
//#if (ODBCVER >= 0x0300)
  SQL_COL_PRED_CHAR		= SQL_LIKE_ONLY;
  SQL_COL_PRED_BASIC		= SQL_ALL_EXCEPT_LIKE;
//#endif /* ODBCVER >= 0x0300 */

//#if (ODBCVER >= 0x0300)
	// extended descriptor field
  SQL_DESC_ARRAY_SIZE          	= 20;
  SQL_DESC_ARRAY_STATUS_PTR    	= 21;
  SQL_DESC_AUTO_UNIQUE_VALUE   	= SQL_COLUMN_AUTO_INCREMENT;
  SQL_DESC_BASE_COLUMN_NAME    	= 22;
  SQL_DESC_BASE_TABLE_NAME     	= 23;
  SQL_DESC_BIND_OFFSET_PTR     	= 24;
  SQL_DESC_BIND_TYPE           	= 25;
  SQL_DESC_CASE_SENSITIVE      	= SQL_COLUMN_CASE_SENSITIVE;
  SQL_DESC_CATALOG_NAME        	= SQL_COLUMN_QUALIFIER_NAME;
  SQL_DESC_CONCISE_TYPE        	= SQL_COLUMN_TYPE;
  SQL_DESC_DATETIME_INTERVAL_PRECISION = 26;
  SQL_DESC_DISPLAY_SIZE        	= SQL_COLUMN_DISPLAY_SIZE;
  SQL_DESC_FIXED_PREC_SCALE    	= SQL_COLUMN_MONEY;
  SQL_DESC_LABEL               	= SQL_COLUMN_LABEL;
  SQL_DESC_LITERAL_PREFIX      	= 27;
  SQL_DESC_LITERAL_SUFFIX      	= 28;
  SQL_DESC_LOCAL_TYPE_NAME     	= 29;
  SQL_DESC_MAXIMUM_SCALE       	= 30;
  SQL_DESC_MINIMUM_SCALE       	= 31;
  SQL_DESC_NUM_PREC_RADIX      	= 32;
  SQL_DESC_PARAMETER_TYPE      	= 33;
  SQL_DESC_ROWS_PROCESSED_PTR  	= 34;
  SQL_DESC_SCHEMA_NAME         	= SQL_COLUMN_OWNER_NAME;
  SQL_DESC_SEARCHABLE          	= SQL_COLUMN_SEARCHABLE;
  SQL_DESC_TYPE_NAME           	= SQL_COLUMN_TYPE_NAME;
  SQL_DESC_TABLE_NAME          	= SQL_COLUMN_TABLE_NAME;
  SQL_DESC_UNSIGNED            	= SQL_COLUMN_UNSIGNED;
  SQL_DESC_UPDATABLE           	= SQL_COLUMN_UPDATABLE;

	// defines for diagnostics fields
  SQL_DIAG_CURSOR_ROW_COUNT    	= (-1249);
  SQL_DIAG_ROW_NUMBER          	= (-1248);
  SQL_DIAG_COLUMN_NUMBER       	= (-1247);

//#endif /* ODBCVER >= 0x0300 */


{********************************************
 * SQLGetFunctions: additional values for   *
 * fFunction to represent functions that    *
 * are not in the X/Open spec.		    *
 ********************************************}
//#if (ODBCVER >= 0x0300)
  SQL_API_SQLALLOCHANDLESTD	= 73;
  SQL_API_SQLBULKOPERATIONS    	= 24;
//#endif /* ODBCVER >= 0x0300 */
  SQL_API_SQLBINDPARAMETER   	= 72;
  SQL_API_SQLBROWSECONNECT   	= 55;
  SQL_API_SQLCOLATTRIBUTES   	=  6;
  SQL_API_SQLCOLUMNPRIVILEGES	= 56;
  SQL_API_SQLDESCRIBEPARAM   	= 58;
  SQL_API_SQLDRIVERCONNECT   	= 41;
  SQL_API_SQLDRIVERS         	= 71;
  SQL_API_SQLEXTENDEDFETCH   	= 59;
  SQL_API_SQLFOREIGNKEYS     	= 60;
  SQL_API_SQLMORERESULTS     	= 61;
  SQL_API_SQLNATIVESQL       	= 62;
  SQL_API_SQLNUMPARAMS       	= 63;
  SQL_API_SQLPARAMOPTIONS    	= 64;
  SQL_API_SQLPRIMARYKEYS     	= 65;
  SQL_API_SQLPROCEDURECOLUMNS	= 66;
  SQL_API_SQLPROCEDURES      	= 67;
  SQL_API_SQLSETPOS          	= 68;
  SQL_API_SQLSETSCROLLOPTIONS	= 69;
  SQL_API_SQLTABLEPRIVILEGES 	= 70;

{*-------------------------------------------*
 * SQL_EXT_API_LAST is not useful with ODBC  *
 * version 3.0 because some of the values    *
 * from X/Open are in the 10000 range.       *
 *-------------------------------------------*}
//#if (ODBCVER < 0x0300)
  SQL_EXT_API_LAST           	= SQL_API_SQLBINDPARAMETER;
  SQL_NUM_FUNCTIONS          	= 23;
  SQL_EXT_API_START          	= 40;
  SQL_NUM_EXTENSIONS 		= (SQL_EXT_API_LAST-SQL_EXT_API_START+1);
//#endif

{*--------------------------------------------*
 * SQL_API_ALL_FUNCTIONS returns an array     *
 * of 'booleans' representing whether a       *
 * function is implemented by the driver.     *
 *                                            *
 * CAUTION: Only functions defined in ODBC    *
 * version 2.0 and earlier are returned, the  *
 * new high-range function numbers defined by *
 * X/Open break this scheme.   See the new    *
 * method -- SQL_API_ODBC3_ALL_FUNCTIONS      *
 *--------------------------------------------*}

  SQL_API_ALL_FUNCTIONS   	= 0;	// See CAUTION above

{*----------------------------------------------*
 * 2.X drivers export a dummy function with  	*
 * ordinal number SQL_API_LOADBYORDINAL to speed*
 * loading under the windows operating system.  *
 * 						*
 * CAUTION: Loading by ordinal is not supported *
 * for 3.0 and above drivers.			*
 *----------------------------------------------*}

  SQL_API_LOADBYORDINAL		= 199;	// See CAUTION above	

{*----------------------------------------------*
 * SQL_API_ODBC3_ALL_FUNCTIONS                  *
 * This returns a bitmap, which allows us to    *
 * handle the higher-valued function numbers.   *
 * Use  SQL_FUNC_EXISTS(bitmap,function_number) *
 * to determine if the function exists.         *
 *----------------------------------------------*}

//#if (ODBCVER >= 0x0300)
  SQL_API_ODBC3_ALL_FUNCTIONS		= 999;
  SQL_API_ODBC3_ALL_FUNCTIONS_SIZE	= 250;	// array of 250 words

{#define SQL_FUNC_EXISTS(pfExists, uwAPI) \
				((*(((UWORD*) (pfExists)) + ((uwAPI) >> 4)) \
					& (1 << ((uwAPI) & 0x000F)) \
 				 ) ? SQL_TRUE : SQL_FALSE \
				)
}
//#endif  /* ODBCVER >= 0x0300 */

{************************************************/
/* Extended definitions for SQLGetInfo          */
/************************************************}

	// Values in ODBC 2.0 that are not in the X/Open spec
  SQL_INFO_FIRST        	=  0;
  SQL_ACTIVE_CONNECTIONS       	=  0;  		// MAX_DRIVER_CONNECTIONS
  SQL_ACTIVE_STATEMENTS        	=  1;  		// MAX_CONCURRENT_ACTIVITIES
  SQL_DRIVER_HDBC              	=  3;
  SQL_DRIVER_HENV              	=  4;
  SQL_DRIVER_HSTMT             	=  5;
  SQL_DRIVER_NAME              	=  6;
  SQL_DRIVER_VER               	=  7;
  SQL_ODBC_API_CONFORMANCE     	=  9;
  SQL_ODBC_VER                 	= 10;
  SQL_ROW_UPDATES              	= 11;
  SQL_ODBC_SAG_CLI_CONFORMANCE 	= 12;
  SQL_ODBC_SQL_CONFORMANCE     	= 15;
  SQL_PROCEDURES               	= 21;
  SQL_CONCAT_NULL_BEHAVIOR     	= 22;
  SQL_CURSOR_ROLLBACK_BEHAVIOR 	= 24;
  SQL_EXPRESSIONS_IN_ORDERBY   	= 27;
  SQL_MAX_OWNER_NAME_LEN       	= 32;		// MAX_SCHEMA_NAME_LEN
  SQL_MAX_PROCEDURE_NAME_LEN   	= 33;
  SQL_MAX_QUALIFIER_NAME_LEN   	= 34;  		// MAX_CATALOG_NAME_LEN
  SQL_MULT_RESULT_SETS         	= 36;
  SQL_MULTIPLE_ACTIVE_TXN      	= 37;
  SQL_OUTER_JOINS              	= 38;
  SQL_OWNER_TERM               	= 39;
  SQL_PROCEDURE_TERM           	= 40;
  SQL_QUALIFIER_NAME_SEPARATOR 	= 41;
  SQL_QUALIFIER_TERM           	= 42;
  SQL_SCROLL_OPTIONS           	= 44;
  SQL_TABLE_TERM               	= 45;
  SQL_CONVERT_FUNCTIONS        	= 48;
  SQL_NUMERIC_FUNCTIONS        	= 49;
  SQL_STRING_FUNCTIONS         	= 50;
  SQL_SYSTEM_FUNCTIONS         	= 51;
  SQL_TIMEDATE_FUNCTIONS       	= 52;
  SQL_CONVERT_BIGINT           	= 53;
  SQL_CONVERT_BINARY           	= 54;
  SQL_CONVERT_BIT              	= 55;
  SQL_CONVERT_CHAR             	= 56;
  SQL_CONVERT_DATE             	= 57;
  SQL_CONVERT_DECIMAL          	= 58;
  SQL_CONVERT_DOUBLE           	= 59;
  SQL_CONVERT_FLOAT            	= 60;
  SQL_CONVERT_INTEGER          	= 61;
  SQL_CONVERT_LONGVARCHAR      	= 62;
  SQL_CONVERT_NUMERIC          	= 63;
  SQL_CONVERT_REAL             	= 64;
  SQL_CONVERT_SMALLINT         	= 65;
  SQL_CONVERT_TIME             	= 66;
  SQL_CONVERT_TIMESTAMP        	= 67;
  SQL_CONVERT_TINYINT          	= 68;
  SQL_CONVERT_VARBINARY        	= 69;
  SQL_CONVERT_VARCHAR          	= 70;
  SQL_CONVERT_LONGVARBINARY    	= 71;
  SQL_ODBC_SQL_OPT_IEF         	= 73;		// SQL_INTEGRITY
  SQL_CORRELATION_NAME         	= 74;
  SQL_NON_NULLABLE_COLUMNS     	= 75;
  SQL_DRIVER_HLIB              	= 76;
  SQL_DRIVER_ODBC_VER          	= 77;
  SQL_LOCK_TYPES               	= 78;
  SQL_POS_OPERATIONS           	= 79;
  SQL_POSITIONED_STATEMENTS    	= 80;
  SQL_BOOKMARK_PERSISTENCE     	= 82;
  SQL_STATIC_SENSITIVITY       	= 83;
  SQL_FILE_USAGE               	= 84;
  SQL_COLUMN_ALIAS             	= 87;
  SQL_GROUP_BY                 	= 88;
  SQL_KEYWORDS                 	= 89;
  SQL_OWNER_USAGE              	= 91;
  SQL_QUALIFIER_USAGE          	= 92;
  SQL_QUOTED_IDENTIFIER_CASE   	= 93;
  SQL_SUBQUERIES               	= 95;
  SQL_UNION                    	= 96;
  SQL_MAX_ROW_SIZE_INCLUDES_LONG= 103;
  SQL_MAX_CHAR_LITERAL_LEN     	= 108;
  SQL_TIMEDATE_ADD_INTERVALS   	= 109;
  SQL_TIMEDATE_DIFF_INTERVALS  	= 110;
  SQL_NEED_LONG_DATA_LEN       	= 111;
  SQL_MAX_BINARY_LITERAL_LEN   	= 112;
  SQL_LIKE_ESCAPE_CLAUSE       	= 113;
  SQL_QUALIFIER_LOCATION       	= 114;

{
#if (ODBCVER >= 0x0201 && ODBCVER < 0x0300)
#define SQL_OJ_CAPABILITIES         65003  /* Temp value until ODBC 3.0 */
#endif  /* ODBCVER >= 0x0201 && ODBCVER < 0x0300 */
}

{*----------------------------------------------*
 * SQL_INFO_LAST and SQL_INFO_DRIVER_START are  *
 * not useful anymore, because  X/Open has      *
 * values in the 10000 range.   You  		*
 * must contact X/Open directly to get a range	*
 * of numbers for driver-specific values.	*
 *----------------------------------------------*}

//#if (ODBCVER < 0x0300)
  SQL_INFO_LAST			= SQL_QUALIFIER_LOCATION;
  SQL_INFO_DRIVER_START		= 1000;
//#endif /* ODBCVER < 0x0300 */

// Defines for SQLBindParameter and SQLProcedureColumns (returned in the result set)
  SQL_PARAM_TYPE_UNKNOWN       	= 0;
  SQL_PARAM_INPUT              	= 1;
  SQL_PARAM_INPUT_OUTPUT       	= 2;
  SQL_RESULT_COL               	= 3;
  SQL_PARAM_OUTPUT             	= 4;
  SQL_RETURN_VALUE             	= 5;

// Defines for SQLProcedures (returned in the result set)
  SQL_PT_UNKNOWN	       	= 0;
  SQL_PT_PROCEDURE      	= 1;
  SQL_PT_FUNCTION              	= 2;


	// Defines for SQLTables
//#if (ODBCVER >= 0x0300)
  SQL_ALL_CATALOGS	= '%';
  SQL_ALL_SCHEMAS	= '%';
  SQL_ALL_TABLE_TYPES	= '%';
//#endif  /* ODBCVER >= 0x0300 */

	// Options for SQLDriverConnect
  SQL_DRIVER_NOPROMPT   	= 0;
  SQL_DRIVER_COMPLETE         	= 1;
  SQL_DRIVER_PROMPT            	= 2;
  SQL_DRIVER_COMPLETE_REQUIRED 	= 3;


{********************************************************************
** SQLUCODE.H - This is the the unicode include for ODBC Core functions.
**
**		Created 6/20 for 3.00 specification
*********************************************************************}
const
  SQL_WCHAR		= (-8);
  SQL_WVARCHAR		= (-9);
  SQL_WLONGVARCHAR	= (-10);
  SQL_C_WCHAR		= SQL_WCHAR;

  SQL_C_TCHAR		= SQL_C_WCHAR;

  SQL_SQLSTATE_SIZEW	= 10;		// size of SQLSTATE for unicode

  
type
//  ESDDb2Error = class(ESDEngineError);
  TSQLAllocConnect=
	  	function (henv :SQLHENV; phdbc: PSQLHDBC): SQLRETURN; stdcall;
  TSQLAllocEnv=
  		function (phenv: PSQLHENV): SQLRETURN; stdcall;
  TSQLAllocHandle=
  		function(fHandleType: SQLSMALLINT; hInput: SQLHANDLE;
                    hOutput: PSQLHANDLE): SQLRETURN; stdcall;
  TSQLAllocStmt=
  		function (hdbc: SQLHDBC; phstmt: PSQLHSTMT): SQLRETURN; stdcall;
  TSQLBindCol=	function (hstmt: SQLHSTMT; icol: SQLUSMALLINT; fCType: SQLSMALLINT;
                    rgbValue: SQLPOINTER; cbValueMax: SQLINTEGER;
                    pcbValue: PSQLINTEGER): SQLRETURN; stdcall;
  TSQLCancel=	function (hstmt: SQLHSTMT): SQLRETURN; stdcall;
  TSQLCloseCursor=
  		function(hStmt: SQLHSTMT): SQLRETURN; stdcall;
  TSQLColAttribute=
  		function (hstmt: SQLHSTMT; icol, fDescType: SQLUSMALLINT;
                    rgbDesc: SQLPOINTER; cbDescMax: SQLSMALLINT;
                    var cbDesc: SQLSMALLINT; pfDesc: SQLPOINTER): SQLRETURN; stdcall;
  TSQLColumns=	function (hStmt: SQLHSTMT;
                    szCatalogName: PChar; cbCatalogName: SQLSMALLINT;
                    szSchemaName: PChar; cbSchemaName: SQLSMALLINT;
                    szTableName: PChar; cbTableName: SQLSMALLINT;
                    szColumnName: PChar; cbColumnName: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLConnect=	function (hdbc: SQLHDBC; szDSN: PChar; cbDSN: SQLSMALLINT;
                    szUID: PChar; cbUID: SQLSMALLINT;
                    szAuthStr: PChar; cbAuthStr: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLCopyDesc=
  		function (hDescSource: SQLHDESC; hDescTarget: SQLHDESC): SQLRETURN; stdcall;
  TSQLDataSources =
  		function (hEnv: SQLHENV; fDirection: SQLUSMALLINT;
                    szDSN: PChar; cbDSNMax: SQLSMALLINT; var cbDSN: SQLSMALLINT;
                    szDescription: PChar; cbDescriptionMax: SQLSMALLINT;
                    var cbDescription: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLDescribeCol =
  		function (hstmt: SQLHSTMT; icol: SQLUSMALLINT;
                    szColName: PChar; cbColNameMax: SQLSMALLINT; var cbColName, fSqlType: SQLSMALLINT;
                    var cbColDef: SQLUINTEGER; var ibScale, fNullable: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLDisconnect =
  		function (hdbc: SQLHDBC): SQLRETURN; stdcall;
  TSQLEndTran =	function (fHandleType: SQLSMALLINT; hHandle: SQLHANDLE; fType: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLError =	function (henv: SQLHENV; hdbc: SQLHDBC; hstmt: SQLHSTMT;
                    szSqlState: PChar; pfNativeError: PSQLINTEGER;
                    szErrorMsg: PChar; cbErrorMsgMax: SQLSMALLINT;
                    pcbErrorMsg: PSQLSMALLINT): SQLRETURN; stdcall;
  TSQLExecDirect =
  		function (hstmt: SQLHSTMT; szSqlStr: PChar; cbSqlStr: SQLINTEGER): SQLRETURN; stdcall;
  TSQLExecute =	function (hstmt: SQLHSTMT): SQLRETURN; stdcall;
  TSQLFetch =	function (hstmt: SQLHSTMT): SQLRETURN; stdcall;
  TSQLFetchScroll =
  		function (hStmt: SQLHSTMT;
  		    FetchOrientation: SQLSMALLINT; FetchOffset: SQLINTEGER): SQLRETURN; stdcall;
  TSQLFreeConnect =
  		function (hdbc: SQLHDBC): SQLRETURN; stdcall;
  TSQLFreeEnv =	function (henv: SQLHENV): SQLRETURN; stdcall;
  TSQLFreeHandle =
  		function (fHandleType: SQLSMALLINT; hHandle: SQLHANDLE): SQLRETURN; stdcall;
  TSQLFreeStmt =
  		function (hstmt: SQLHSTMT; fOption: SQLUSMALLINT): SQLRETURN; stdcall;
  TSQLGetConnectAttr =
  		function (hDbc: SQLHDBC; Attribute: SQLINTEGER; Value: SQLPOINTER;
                    BufferLength: SQLINTEGER; var StringLength: SQLINTEGER): SQLRETURN; stdcall;
  TSQLGetConnectOption =
  		function (hDbc: SQLHDBC; fOption: SQLUSMALLINT; pvParam: SQLPOINTER): SQLRETURN; stdcall;
  TSQLGetCursorName =
  		function (hStmt: SQLHSTMT; szCursor: PChar; cbCursorMax: SQLSMALLINT;
                    var cbCursor: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLGetData =	function (hStmt: SQLHSTMT; icol: SQLUSMALLINT; fCType: SQLSMALLINT;
                    rgbValue: SQLPOINTER; cbValueMax: SQLINTEGER;
                    pcbValue: PSQLINTEGER): SQLRETURN; stdcall;
  TSQLGetDescField =
  		function (DescriptorHandle: SQLHDESC; RecNumber, FieldIdentifier: SQLSMALLINT;
                    Value: SQLPOINTER; BufferLength: SQLINTEGER;
                    var StringLength: SQLINTEGER): SQLRETURN; stdcall;
  TSQLGetDescRec =
  		function (DescriptorHandle: SQLHDESC; RecNumber: SQLSMALLINT;
                    Name: PChar; BufferLength: SQLSMALLINT; var StringLength, RecType, RecSubType: SQLSMALLINT;
                    var Length: SQLINTEGER; var Precision, Scale, Nullable: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLGetDiagField =
  		function (fHandleType: SQLSMALLINT; hHandle: SQLHANDLE; iRecNumber: SQLSMALLINT;
                    fDiagIdentifier: SQLSMALLINT; pDiagInfo: SQLPOINTER;
                    cbDiagInfoMax: SQLSMALLINT; pcbDiagInfo: PSQLSMALLINT): SQLRETURN; stdcall;
  TSQLGetDiagRec =
  		function (fHandleType: SQLSMALLINT; hHandle: SQLHANDLE; iRecNumber: SQLSMALLINT;
                    szSqlState: PChar; pfNativeError: PSQLINTEGER;
                    szErrorMsg: PChar; cbErrorMsgMax: SQLSMALLINT;
                    pcbErrorMsg: PSQLSMALLINT): SQLRETURN; stdcall;
  TSQLGetEnvAttr =
  		function(hEnv: SQLHENV; Attr: SQLINTEGER; Value: SQLPOINTER;
         		BufLen: SQLINTEGER; StringLength: PSQLINTEGER): SQLRETURN; stdcall;
  TSQLGetFunctions =
  		function (hDbc: SQLHDBC; fFunction: SQLUSMALLINT; var fExists: SQLUSMALLINT): SQLRETURN; stdcall;
  TSQLGetInfo =	function (hDbc: SQLHDBC; fInfoType: SQLUSMALLINT; rgbInfoValue: SQLPOINTER;
                    cbInfoValueMax: SQLSMALLINT; pcbInfoValue: PSQLSMALLINT): SQLRETURN; stdcall;
  TSQLGetStmtAttr =
  		function (hStmt: SQLHSTMT; Attribute: SQLINTEGER; Value: SQLPOINTER;
                    BufferLength: SQLINTEGER; var StringLength: SQLINTEGER): SQLRETURN; stdcall;
  TSQLGetStmtOption =
  		function (hStmt: SQLHSTMT; fOption: SQLUSMALLINT; pvParam: SQLPOINTER): SQLRETURN; stdcall;
  TSQLGetTypeInfo =
  		function (hStmt: SQLHSTMT; fSqlType: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLNumResultCols =
  		function (hStmt: SQLHSTMT; var ccol: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLParamData =
  		function (hStmt: SQLHSTMT; rgbValue: PSQLPOINTER): SQLRETURN; stdcall;
  TSQLPrepare =	function (hStmt: SQLHSTMT; szSqlStr: PChar; cbSqlStr: SQLINTEGER): SQLRETURN; stdcall;
  TSQLPutData =	function (hStmt: SQLHSTMT; rgbValue: SQLPOINTER; cbValue: SQLINTEGER): SQLRETURN; stdcall;
  TSQLRowCount =
  		function (hStmt: SQLHSTMT; var crow: SQLINTEGER): SQLRETURN; stdcall;
  TSQLSetConnectAttr =
  		function (hDbc: SQLHDBC; fOption: SQLINTEGER;
                    pvParam: SQLPOINTER; fStrLen: SQLINTEGER): SQLRETURN; stdcall;
  TSQLSetConnectOption =
  		function (hDbc: SQLHDBC; fOption: SQLUSMALLINT; vParam: SQLUINTEGER): SQLRETURN; stdcall;
  TSQLSetCursorName =
  		function (hStmt: SQLHSTMT; szCursor: PChar; cbCursor: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLSetDescField =
  		function (DescriptorHandle: SQLHDESC; RecNumber, FieldIdentifier: SQLSMALLINT;
                    Value: SQLPOINTER; BufferLength: SQLINTEGER): SQLRETURN; stdcall;
  TSQLSetDescRec =
  		function (DescriptorHandle: SQLHDESC; RecNumber, RecType, RecSubType: SQLSMALLINT;
                    Length: SQLINTEGER; Precision, Scale: SQLSMALLINT;
                    Data: SQLPOINTER; var StringLength, Indicator: SQLINTEGER): SQLRETURN; stdcall;
  TSQLSetEnvAttr =
  		function(hEnv: SQLHENV; Attr: SQLINTEGER; Value: SQLPOINTER;
         		StringLength: SQLINTEGER): SQLRETURN; stdcall;
  TSQLSetParam =function (hStmt: SQLHSTMT; ipar: SQLUSMALLINT; fCType, fSqlType: SQLSMALLINT;
                    cbParamDef: SQLUINTEGER; ibScale: SQLSMALLINT;
                    rgbValue: SQLPOINTER; var cbValue: SQLINTEGER): SQLRETURN; stdcall;
  TSQLSetStmtAttr =
  		function (hStmt: SQLHSTMT; fOption: SQLINTEGER;
                    pvParam: SQLPOINTER; fStrLen: SQLINTEGER): SQLRETURN; stdcall;
  TSQLSetStmtOption =
  		function (hStmt: SQLHSTMT; fOption: SQLUSMALLINT; vParam: SQLUINTEGER): SQLRETURN; stdcall;
  TSQLSpecialColumns =
  		function (hStmt: SQLHSTMT; fColType: SQLUSMALLINT;
                    szCatalogName: PChar; cbCatalogName: SQLSMALLINT;
                    szSchemaName: PChar; cbSchemaName: SQLSMALLINT;
                    szTableName: PChar; cbTableName: SQLSMALLINT;
                    fScope, fNullable: SQLUSMALLINT): SQLRETURN; stdcall;
  TSQLStatistics =
  		function (hStmt: SQLHSTMT;
                    szCatalogName: PChar; cbCatalogName: SQLSMALLINT;
                    szSchemaName: PChar; cbSchemaName: SQLSMALLINT;
                    szTableName: PChar; cbTableName: SQLSMALLINT;
                    fUnique, fAccuracy: SQLUSMALLINT): SQLRETURN; stdcall;
  TSQLTables =	function (hStmt: SQLHSTMT;
                    szCatalogName: PChar; cbCatalogName: SQLSMALLINT;
                    szSchemaName: PChar; cbSchemaName: SQLSMALLINT;
                    szTableName: PChar; cbTableName: SQLSMALLINT;
                    szTableType: PChar; cbTableType: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLTransact =function (hEnv: SQLHENV; hDbc: SQLHDBC; fType: SQLUSMALLINT): SQLRETURN; stdcall;

{*******************************************************************************
** SQLEXT.H - This is the include for applications using
**             the Microsoft SQL Extensions
**
**		Updated 07/25/95 for 3.00 specification
**		Updated 01/12/96 for 3.00 preliminary release
** 		Updated 09/16/96 for 3.00 SDK release
**		Updated 11/21/96 for bug #4436
********************************************************************************}
  TSQLBrowseConnect =
  		function(hDbc: SQLHDBC; szConnStrIn: PChar; cbConnStrIn: SQLSMALLINT;
              	szConnStrOut: PChar; cbConnStrOutMax: SQLSMALLINT;
                      pcbConnStrOut: PSQLSMALLINT): SQLRETURN; stdcall;
  TSQLBulkOperations =
  		function(hStmt: SQLHSTMT; Operation: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLColAttributes =
  		function(hStmt: SQLHSTMT; icol, fDescType: SQLUSMALLINT; rgbDesc: SQLPOINTER;
              	cbDescMax: SQLSMALLINT; pcbDesc: PSQLSMALLINT; pfDesc: PSQLINTEGER): SQLRETURN; stdcall;
  TSQLColumnPrivileges =
  		function(hStmt: SQLHSTMT; szCatalogName: PChar; cbCatalogName: SQLSMALLINT;
              	szSchemaName: PChar; cbSchemaName: SQLSMALLINT;
                      szTableName: PChar; cbTableName: SQLSMALLINT;
                      szColumnName: PChar; cbColumnName: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLDescribeParam =
  		function(hStmt: SQLHSTMT; ipar: SQLUSMALLINT; pfSqlType: PSQLSMALLINT;
              	pcbParamDef: PSQLUINTEGER; pibScale, pfNullable: PSQLSMALLINT): SQLRETURN; stdcall;
  TSQLExtendedFetch =
  		function(hStmt: SQLHSTMT; fFetchType: SQLUSMALLINT; irow: SQLINTEGER;
              	pcrow: PSQLUINTEGER; rgfRowStatus: PSQLUSMALLINT): SQLRETURN; stdcall;
  TSQLForeignKeys =
  		function(hStmt: SQLHSTMT; szPkCatalogName: PChar; cbPkCatalogName: SQLSMALLINT;
              	szPkSchemaName: PChar; cbPkSchemaName: SQLSMALLINT;
                      szPkTableName: PChar; cbPkTableName: SQLSMALLINT;
                      szFkCatalogName: PChar; cbFkCatalogName: SQLSMALLINT;
                      szFkSchemaName: PChar; cbFkSchemaName: SQLSMALLINT;
                      szFkTableName: PChar; cbFkTableName: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLMoreResults =
  		function(hStmt: SQLHSTMT): SQLRETURN; stdcall;
  TSQLNativeSql =
  		function(hDbc: SQLHDBC; szSqlStrIn: PChar; cbSqlStrIn: SQLINTEGER;
  			szSqlStr: PChar; cbSqlStrMax: SQLINTEGER; pcbSqlStr: PSQLINTEGER): SQLRETURN; stdcall;
  TSQLNumParams =
  		function(hStmt: SQLHSTMT; pcpar: PSQLSMALLINT): SQLRETURN; stdcall;
  TSQLParamOptions =
  		function(hStmt: SQLHSTMT; crow: SQLUINTEGER; pirow: PSQLUINTEGER): SQLRETURN; stdcall;
  TSQLPrimaryKeys =
  		function(hStmt: SQLHSTMT; szCatalogName: PChar; cbCatalogName: SQLSMALLINT;
  			szSchemaName: PChar; cbSchemaName: SQLSMALLINT;
  			szTableName: PChar; cbTableName: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLProcedureColumns =
  		function(hStmt: SQLHSTMT; szCatalogName: PChar; cbCatalogName: SQLSMALLINT;
  			szSchemaName: PChar; cbSchemaName: SQLSMALLINT;
                      szProcName: PChar; cbProcName: SQLSMALLINT;
  			szColumnName: PChar; cbColumnName: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLProcedures =
  		function(hStmt: SQLHSTMT; szCatalogName: PChar; cbCatalogName: SQLSMALLINT;
  			szSchemaName: PChar; cbSchemaName: SQLSMALLINT;
  			szProcName: PChar; cbProcName: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLSetPos =
  		function(hStmt: SQLHSTMT; irow, fOption, fLock: SQLUSMALLINT): SQLRETURN; stdcall;
  TSQLTablePrivileges =
  		function(hStmt: SQLHSTMT; szCatalogName: PChar; cbCatalogName: SQLSMALLINT;
              	szSchemaName: PChar; cbSchemaName: SQLSMALLINT;
                      szTableName: PChar; cbTableName: SQLSMALLINT): SQLRETURN; stdcall;
  TSQLDrivers =
  		function(hEnv: SQLHENV; fDirection: SQLUSMALLINT; szDriverDesc: PChar;
              	cbDriverDescMax: SQLSMALLINT; pcbDriverDesc: PSQLSMALLINT;
                      szDriverAttributes: PChar; cbDrvrAttrMax: SQLSMALLINT; pcbDrvrAttr: PSQLSMALLINT): SQLRETURN; stdcall;
  TSQLBindParameter =
  		function(hStmt: SQLHSTMT; ipar: SQLUSMALLINT;
  			fParamType, fCType, fSqlType: SQLSMALLINT;
  			cbColDef: SQLUINTEGER; ibScale: SQLSMALLINT;
  			rgbValue: SQLPOINTER; cbValueMax: SQLINTEGER;
                      pcbValue: PSQLINTEGER): SQLRETURN; stdcall;
  TSQLDriverConnect =
  		function(hDbc: SQLHDBC; hWnd: SQLHWND;
                	szConnStrIn: PChar; cbConnStrIn: SQLSMALLINT;
                        szConnStrOut: PChar; cbConnStrOutMax: SQLSMALLINT;
                        pcbConnStrOut: PSQLSMALLINT; fDriverCompletion: SQLUSMALLINT): SQLRETURN; stdcall;


{ TOdbcFunctions }
  TOdbcFunctions = class
  private
    FSQLAllocConnect:	TSQLAllocConnect;
    FSQLAllocEnv:	TSQLAllocEnv;
    FSQLAllocHandle:	TSQLAllocHandle;
    FSQLAllocStmt:	TSQLAllocStmt;
    FSQLBindCol:	TSQLBindCol;
    FSQLCancel:		TSQLCancel;
    FSQLCloseCursor:	TSQLCloseCursor;
    FSQLColAttribute:	TSQLColAttribute;
    FSQLColumns:	TSQLColumns;
    FSQLConnect:	TSQLConnect;
    FSQLCopyDesc:	TSQLCopyDesc;
    FSQLDataSources:	TSQLDataSources;
    FSQLDescribeCol:	TSQLDescribeCol;
    FSQLDisconnect:	TSQLDisconnect;
    FSQLEndTran:	TSQLEndTran;
    FSQLError:		TSQLError;
    FSQLExecDirect:	TSQLExecDirect;
    FSQLExecute:	TSQLExecute;
    FSQLFetch:		TSQLFetch;
    FSQLFetchScroll:	TSQLFetchScroll;
    FSQLFreeConnect:	TSQLFreeConnect;
    FSQLFreeEnv:	TSQLFreeEnv;
    FSQLFreeHandle:	TSQLFreeHandle;
    FSQLFreeStmt:	TSQLFreeStmt;
    FSQLGetConnectAttr:	TSQLGetConnectAttr;
    FSQLGetConnectOption:TSQLGetConnectOption;
    FSQLGetCursorName:	TSQLGetCursorName;
    FSQLGetData:	TSQLGetData;
    FSQLGetDescField:	TSQLGetDescField;
    FSQLGetDescRec:	TSQLGetDescRec;
    FSQLGetDiagField:	TSQLGetDiagField;
    FSQLGetDiagRec:	TSQLGetDiagRec;
    FSQLGetEnvAttr:	TSQLGetEnvAttr;
    FSQLGetFunctions:	TSQLGetFunctions;
    FSQLGetInfo:	TSQLGetInfo;
    FSQLGetStmtAttr:	TSQLGetStmtAttr;
    FSQLGetStmtOption:	TSQLGetStmtOption;
    FSQLGetTypeInfo:	TSQLGetTypeInfo;
    FSQLNumResultCols:	TSQLNumResultCols;
    FSQLParamData:	TSQLParamData;
    FSQLPrepare:	TSQLPrepare;
    FSQLPutData:	TSQLPutData;
    FSQLRowCount:	TSQLRowCount;
    FSQLSetConnectAttr:	TSQLSetConnectAttr;
    FSQLSetConnectOption:TSQLSetConnectOption;
    FSQLSetCursorName:	TSQLSetCursorName;
    FSQLSetDescField:	TSQLSetDescField;
    FSQLSetDescRec:	TSQLSetDescRec;
    FSQLSetEnvAttr:	TSQLSetEnvAttr;
    FSQLSetParam:	TSQLSetParam;
    FSQLSetStmtAttr:	TSQLSetStmtAttr;
    FSQLSetStmtOption:	TSQLSetStmtOption;
    FSQLSpecialColumns:	TSQLSpecialColumns;
    FSQLStatistics:	TSQLStatistics;
    FSQLTables:		TSQLTables;
    FSQLTransact:	TSQLTransact;
	// from SQLEXT.H
    FSQLBrowseConnect:	TSQLBrowseConnect;
    FSQLBulkOperations:	TSQLBulkOperations;
    FSQLColAttributes:	TSQLColAttributes;
    FSQLColumnPrivileges:TSQLColumnPrivileges;
    FSQLDescribeParam:	TSQLDescribeParam;
    FSQLExtendedFetch:	TSQLExtendedFetch;
    FSQLForeignKeys:	TSQLForeignKeys;
    FSQLMoreResults:	TSQLMoreResults;
    FSQLNativeSql:	TSQLNativeSql;
    FSQLNumParams:	TSQLNumParams;
    FSQLParamOptions:	TSQLParamOptions;
    FSQLPrimaryKeys:	TSQLPrimaryKeys;
    FSQLProcedureColumns:TSQLProcedureColumns;
    FSQLProcedures:	TSQLProcedures;
    FSQLSetPos:		TSQLSetPos;
    FSQLTablePrivileges:TSQLTablePrivileges;
    FSQLDrivers:     	TSQLDrivers;
    FSQLBindParameter:	TSQLBindParameter;
    FSQLDriverConnect:	TSQLDriverConnect;
  public
    property SQLAllocConnect:	TSQLAllocConnect 	read FSQLAllocConnect;
    property SQLAllocEnv:	TSQLAllocEnv		read FSQLAllocEnv;
    property SQLAllocHandle:	TSQLAllocHandle		read FSQLAllocHandle;
    property SQLAllocStmt:	TSQLAllocStmt		read FSQLAllocStmt;
    property SQLBindCol:	TSQLBindCol		read FSQLBindCol;
    property SQLCancel:		TSQLCancel		read FSQLCancel;
    property SQLCloseCursor:	TSQLCloseCursor		read FSQLCloseCursor;
    property SQLColAttribute:	TSQLColAttribute	read FSQLColAttribute;
    property SQLColumns:	TSQLColumns		read FSQLColumns;
    property SQLConnect:	TSQLConnect		read FSQLConnect;
    property SQLCopyDesc:	TSQLCopyDesc		read FSQLCopyDesc;
    property SQLDataSources:	TSQLDataSources		read FSQLDataSources;
    property SQLDescribeCol:	TSQLDescribeCol		read FSQLDescribeCol;
    property SQLDisconnect:	TSQLDisconnect		read FSQLDisconnect;
    property SQLEndTran:	TSQLEndTran		read FSQLEndTran;
    property SQLError:		TSQLError		read FSQLError;
    property SQLExecDirect:	TSQLExecDirect		read FSQLExecDirect;
    property SQLExecute:	TSQLExecute		read FSQLExecute;
    property SQLFetch:		TSQLFetch		read FSQLFetch;
    property SQLFetchScroll:	TSQLFetchScroll		read FSQLFetchScroll;
    property SQLFreeConnect:	TSQLFreeConnect		read FSQLFreeConnect;
    property SQLFreeEnv:	TSQLFreeEnv		read FSQLFreeEnv;
    property SQLFreeHandle:	TSQLFreeHandle		read FSQLFreeHandle;
    property SQLFreeStmt:	TSQLFreeStmt		read FSQLFreeStmt;
    property SQLGetConnectAttr:	TSQLGetConnectAttr    	read FSQLGetConnectAttr;
    property SQLGetConnectOption:TSQLGetConnectOption 	read FSQLGetConnectOption;
    property SQLGetCursorName:	TSQLGetCursorName     	read FSQLGetCursorName;
    property SQLGetData:	TSQLGetData		read FSQLGetData;
    property SQLGetDescField:	TSQLGetDescField      	read FSQLGetDescField;
    property SQLGetDescRec:	TSQLGetDescRec		read FSQLGetDescRec;
    property SQLGetDiagField:	TSQLGetDiagField      	read FSQLGetDiagField;
    property SQLGetDiagRec:	TSQLGetDiagRec		read FSQLGetDiagRec;
    property SQLGetEnvAttr:	TSQLGetEnvAttr		read FSQLGetEnvAttr;
    property SQLGetFunctions:	TSQLGetFunctions      	read FSQLGetFunctions;
    property SQLGetInfo:	TSQLGetInfo		read FSQLGetInfo;
    property SQLGetStmtAttr:	TSQLGetStmtAttr		read FSQLGetStmtAttr;
    property SQLGetStmtOption:	TSQLGetStmtOption     	read FSQLGetStmtOption;
    property SQLGetTypeInfo:	TSQLGetTypeInfo		read FSQLGetTypeInfo;
    property SQLNumResultCols:	TSQLNumResultCols     	read FSQLNumResultCols;
    property SQLParamData:	TSQLParamData		read FSQLParamData;
    property SQLPrepare:	TSQLPrepare		read FSQLPrepare;
    property SQLPutData:	TSQLPutData		read FSQLPutData;
    property SQLRowCount:	TSQLRowCount		read FSQLRowCount;
    property SQLSetConnectAttr:	TSQLSetConnectAttr    	read FSQLSetConnectAttr;
    property SQLSetConnectOption:TSQLSetConnectOption	read FSQLSetConnectOption;
    property SQLSetCursorName:	TSQLSetCursorName  	read FSQLSetCursorName;
    property SQLSetDescField:	TSQLSetDescField   	read FSQLSetDescField;
    property SQLSetDescRec:	TSQLSetDescRec		read FSQLSetDescRec;
    property SQLSetEnvAttr:	TSQLSetEnvAttr		read FSQLSetEnvAttr;
    property SQLSetParam:	TSQLSetParam		read FSQLSetParam;
    property SQLSetStmtAttr:	TSQLSetStmtAttr		read FSQLSetStmtAttr;
    property SQLSetStmtOption:	TSQLSetStmtOption  	read FSQLSetStmtOption;
    property SQLSpecialColumns:	TSQLSpecialColumns 	read FSQLSpecialColumns;
    property SQLStatistics:	TSQLStatistics		read FSQLStatistics;
    property SQLTables:		TSQLTables		read FSQLTables;
    property SQLTransact:	TSQLTransact		read FSQLTransact;
	// from SQLEXT.H
    property SQLBrowseConnect:	TSQLBrowseConnect    	read FSQLBrowseConnect;
    property SQLBulkOperations:	TSQLBulkOperations   	read FSQLBulkOperations;
    property SQLColAttributes:	TSQLColAttributes    	read FSQLColAttributes;
    property SQLColumnPrivileges:TSQLColumnPrivileges	read FSQLColumnPrivileges;
    property SQLDescribeParam:	TSQLDescribeParam    	read FSQLDescribeParam;
    property SQLExtendedFetch:	TSQLExtendedFetch    	read FSQLExtendedFetch;
    property SQLForeignKeys:	TSQLForeignKeys		read FSQLForeignKeys;
    property SQLMoreResults:	TSQLMoreResults		read FSQLMoreResults;
    property SQLNativeSql:	TSQLNativeSql		read FSQLNativeSql;
    property SQLNumParams:	TSQLNumParams		read FSQLNumParams;
    property SQLParamOptions:	TSQLParamOptions     	read FSQLParamOptions;
    property SQLPrimaryKeys:	TSQLPrimaryKeys		read FSQLPrimaryKeys;
    property SQLProcedureColumns:TSQLProcedureColumns	read FSQLProcedureColumns;
    property SQLProcedures:	TSQLProcedures		read FSQLProcedures;
    property SQLSetPos:		TSQLSetPos		read FSQLSetPos;
    property SQLTablePrivileges:TSQLTablePrivileges	read FSQLTablePrivileges;
    property SQLDrivers:	TSQLDrivers		read FSQLDrivers;
    property SQLBindParameter:	TSQLBindParameter	read FSQLBindParameter;
    property SQLDriverConnect:	TSQLDriverConnect	read FSQLDriverConnect;
  end;

  ESDOdbcError = class(ESDEngineError)
  private
    FSqlState: string;
  public
    constructor CreateWithSqlState(AErrorCode, ANativeError: TSDEResult; const AMsg, ASqlState: string);
    property SqlState: string read FSqlState;
  end;

{ TICustomOdbcDatabase }
  TOdbcSrvInfo	= record
    hEnv: SQLHENV;		// environment handle
    hDbc: SQLHDBC;   		// connection handle
  end;
  POdbcSrvInfo	= ^TOdbcSrvInfo;


  TICustomOdbcDatabase = class(TISrvDatabase)
  private
    FHandle: PSDHandleRec;
    FDriverOdbcVer: LongInt;		// version of ODBC that driver supports
    FIsRTrimChar: Boolean;		// whether to trim trailing spaces in the output of CHAR datatype ?
    FIsTransSupported: Boolean;		// Is transaction supported for ODBC datasource ?

    FIsSingleConn: Boolean;		// Whether or not a database FHandle is used for opened TSDQuery
    FCurrDataSet: TSDDataSet;		// a dataset, which uses a database handle currently (when FIsSingleConn is True)

    function GetCalls: TOdbcFunctions;
    function GetDriverOdbcVersion: LongInt;
    function GetDriverOdbcMajor: Word;
    function GetDriverOdbcMinor: Word;
    function GetIsNeedLongDataLen: Boolean;
    function GetEnvHandle: SQLHENV;
    function GetDbcHandle: SQLHDBC;
    procedure SetAutoCommitOption(Value: Boolean);
  protected
    FCalls: TOdbcFunctions;

    procedure AllocHandle;
    procedure AllocOdbcHandles;
    procedure FreeHandle;
    procedure FreeOdbcHandles;

    function OdbcAllocHandle(fHandleType: SQLSMALLINT; hInput: SQLHANDLE; hOutput: PSQLHANDLE): SQLRETURN;
    function OdbcFreeHandle(fHandleType: SQLSMALLINT; hHandle: SQLHANDLE): SQLRETURN;
    function OdbcSetConnectAttr(hDbc: SQLHDBC; fOption: SQLINTEGER; pvParam: SQLPOINTER; fStrLen: SQLINTEGER): SQLRETURN;
    function OdbcSetStmtAttr(hStmt: SQLHSTMT; fOption: SQLINTEGER; pvParam: SQLPOINTER; fStrLen: SQLINTEGER): SQLRETURN;
    function OdbcTransact(fEndType: SQLSMALLINT): SQLRETURN;
    function OdbcGetMaxCatalogNameLen: SQLUSMALLINT;
    function OdbcGetMaxFieldNameLen: SQLUSMALLINT;
    function OdbcGetMaxProcNameLen: SQLUSMALLINT;
    function OdbcGetMaxSchemaNameLen: SQLUSMALLINT;
    function OdbcGetMaxTableNameLen: SQLUSMALLINT;

    procedure CheckHandle(AHandleType: SQLSMALLINT; AHandle: SQLHANDLE; AStatus: TSDEResult);
    procedure CheckHDBC(AHandle: SQLHDBC; Status: TSDEResult);
    procedure CheckHSTMT(AHandle: SQLHSTMT; Status: TSDEResult);
    procedure Check(Status: TSDEResult);
    procedure FreeSqlLib; virtual; abstract;
    procedure LoadSqlLib; virtual; abstract;
    procedure RaiseSDEngineError(AErrorCode, ANativeError: TSDEResult; const AMsg, ASqlState: string); virtual; abstract;

    function GetClientVersion: LongInt; override;
    function GetServerVersion: LongInt; override;
    function GetVersionString: string; override;
    procedure GetStmtResult(const Stmt: string; List: TStrings);

    procedure Commit; override;
    function GetHandle: PSDHandleRec; override;
    procedure GetStoredProcNames(List: TStrings); override;
    procedure GetTableFieldNames(const TableName: string; List: TStrings); override;
    procedure GetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings); override;
    procedure Logon(const sRemoteDatabase, sUserName, sPassword: string); override;
    procedure Logoff(Force: Boolean); override;
    function FieldDataType(ExtDataType: Integer): TFieldType; override;
    function NativeDataSize(FieldType: TFieldType): Word; override;
    function NativeDataType(FieldType: TFieldType): Integer; override;
    function SqlDataType(FieldType: TFieldType): Integer; virtual;
    function RequiredCnvtFieldType(FieldType: TFieldType): Boolean; override;
    procedure Rollback; override;
    procedure SetDefaultParams; override;
    procedure SetHandle(AHandle: PSDHandleRec); override;
    procedure SetTransIsolation(Value: TSDTransIsolation); override;
    procedure StartTransaction; override;
    function SPDescriptionsAvailable: Boolean; override;
    property IsRTrimChar: Boolean read FIsRTrimChar;
    property IsSingleConn: Boolean read FIsSingleConn;
  public
    constructor Create(ADatabase: TSDDatabase); override;
    destructor Destroy; override;
    procedure ReleaseDBHandle(ADataSet: TSDDataSet; IsFetchAll: Boolean);
    property CurrDataSet: TSDDataSet read FCurrDataSet;
    property Calls: TOdbcFunctions read GetCalls;
    property EnvHandle: SQLHENV read GetEnvHandle;
    property DbcHandle: SQLHDBC read GetDbcHandle;
    property DriverOdbcMajor: Word read GetDriverOdbcMajor;
    property DriverOdbcMinor: Word read GetDriverOdbcMinor;
    property IsNeedLongDataLen: Boolean read GetIsNeedLongDataLen;
  end;

{ TICustomOdbcDataSet }
  TICustomOdbcDataSet = class(TISrvDataSet)
  private
    FHandle: SQLHSTMT;		// statement handle
    FStmt: string;		// statement with parameter markers '?'
    FNextResults: Boolean;	// if one of multiple result sets processing
    FIsSingleConn: Boolean;	// True, if it's required to use a database handle always
    FInfoExecuted: Boolean;	// a statement was executed in GetFieldDesc method
    FNoDataExecuted: Boolean;	// a statement was executed with SQL_NO_DATA return code

    function GetCalls: TOdbcFunctions;
    function GetSrvDatabase: TICustomOdbcDatabase;
    function GetIsCallStmt: Boolean;
    function GetIsExecDirect: Boolean;

    function CnvtDBDateTime2DateTimeRec(ADataType: TFieldType; Buffer: PChar; BufSize: Integer): TDateTimeRec;
    procedure InternalQExecute(InfoQuery: Boolean);
    procedure InternalSpExecute(InfoQuery: Boolean);
  protected
    procedure AcquireDBHandle;
    procedure ReleaseDBHandle;

    function CnvtDateTime2DBDateTime(ADataType: TFieldType; Value: TDateTime; Buffer: Pointer; BufSize: Integer): Integer; override;
    function CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean; override;

    procedure Check(Status: TSDEResult);
    procedure CheckPrepared;
    procedure CloseResultSet; override;
    procedure Connect; virtual;
    procedure Disconnect(Force: Boolean); override;
    procedure Execute; override;
    function FetchNextRow: Boolean; override;
    procedure GetFieldDescs(Descs: TSDFieldDescList); override;
    function GetHandle: PSDCursor; override;
    function ResultSetExists: Boolean; override;
    procedure SetSelectBuffer; override;
    function ReadBlob(FieldNo: Integer; var BlobData: string): Longint; override;
    function WriteBlob(FieldNo: Integer; const Buffer; Count: LongInt): Longint; override;
	// Query methods
    procedure QBindParams; override;
    function QGetRowsAffected: Integer; override;
    procedure QPrepareSQL(Value: PChar); override;
    procedure QExecute; override;
	// StoredProc methods
    procedure SpBindParams; override;
    procedure SpCreateParamDesc; override;
    procedure SpPrepareProc; override;
    procedure SpExecute; override;
    procedure SpExecProc; override;
    procedure SpGetParams;
    procedure SpGetResults; override;
    function SpNextResultSet: Boolean; override;

    property SrvDatabase: TICustomOdbcDatabase read GetSrvDatabase;
  public
    constructor Create(ADataSet: TSDDataSet); override;
    destructor Destroy; override;
    property Calls: TOdbcFunctions read GetCalls;
    property IsCallStmt: Boolean read GetIsCallStmt;
    property IsExecDirect: Boolean read GetIsExecDirect;
  end;


{ TIOdbcDatabase }
  TIOdbcDatabase = class(TICustomOdbcDatabase)
  protected
    procedure FreeSqlLib; override;
    procedure LoadSqlLib; override;
    procedure RaiseSDEngineError(AErrorCode, ANativeError: TSDEResult; const AMsg, ASqlState: string); override;
  public
    function CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet; override;
  end;

{ TIOdbcDataSet }
  TIOdbcDataSet = class(TICustomOdbcDataSet)
  end;

var
  OdbCalls: TOdbcFunctions;

const
  DefSqlApiDLL	= 'ODBC32.DLL';

(*******************************************************************************
			Load/Unload Sql-library
********************************************************************************)
procedure LoadSqlLib;
procedure FreeSqlLib;

procedure SetApiCalls(ASqlLibModule: HINST; ACalls: TOdbcFunctions);
procedure ResetApiCalls(ACalls: TOdbcFunctions);


implementation


resourcestring
  SErrLibLoading 	= 'Error loading library ''%s''';
  SErrLibUnloading	= 'Error unloading library ''%s''';

const
  ParamPrefix	= ':';
  ParamMarker	= '?';
// consider a field as blob/memo, even if it has char/binary type (for example, char(30000))
  MinBlobSize	= 256;

var
  hSqlLibModule: THandle;
  SqlLibRefCount: Integer;
  SqlLibLock: TCriticalSection;  


// implementation of SQL_LEN_DATA_AT_EXEC macro
function SQL_LEN_DATA_AT_EXEC(ALength: SQLINTEGER): SQLINTEGER;
begin
  Result := (-(ALength)+SQL_LEN_DATA_AT_EXEC_OFFSET);
end;

procedure SetApiCalls(ASqlLibModule: HINST; ACalls: TOdbcFunctions);
begin
  @ACalls.FSQLAllocConnect    	:= GetProcAddress(ASqlLibModule, 'SQLAllocConnect');
  @ACalls.FSQLAllocEnv        	:= GetProcAddress(ASqlLibModule, 'SQLAllocEnv');
  @ACalls.FSQLAllocHandle     	:= GetProcAddress(ASqlLibModule, 'SQLAllocHandle');
  @ACalls.FSQLAllocStmt       	:= GetProcAddress(ASqlLibModule, 'SQLAllocStmt');
  @ACalls.FSQLBindCol         	:= GetProcAddress(ASqlLibModule, 'SQLBindCol');
  @ACalls.FSQLCancel          	:= GetProcAddress(ASqlLibModule, 'SQLCancel');
  @ACalls.FSQLCloseCursor     	:= GetProcAddress(ASqlLibModule, 'SQLCloseCursor');
  @ACalls.FSQLColAttribute    	:= GetProcAddress(ASqlLibModule, 'SQLColAttribute');
  @ACalls.FSQLColumns         	:= GetProcAddress(ASqlLibModule, 'SQLColumns');
  @ACalls.FSQLConnect         	:= GetProcAddress(ASqlLibModule, 'SQLConnect');
  @ACalls.FSQLCopyDesc        	:= GetProcAddress(ASqlLibModule, 'SQLCopyDesc');
  @ACalls.FSQLDataSources     	:= GetProcAddress(ASqlLibModule, 'SQLDataSources');
  @ACalls.FSQLDescribeCol     	:= GetProcAddress(ASqlLibModule, 'SQLDescribeCol');
  @ACalls.FSQLDisconnect      	:= GetProcAddress(ASqlLibModule, 'SQLDisconnect');
  @ACalls.FSQLEndTran         	:= GetProcAddress(ASqlLibModule, 'SQLEndTran');
  @ACalls.FSQLError           	:= GetProcAddress(ASqlLibModule, 'SQLError');
  @ACalls.FSQLExecDirect      	:= GetProcAddress(ASqlLibModule, 'SQLExecDirect');
  @ACalls.FSQLExecute         	:= GetProcAddress(ASqlLibModule, 'SQLExecute');
  @ACalls.FSQLFetch           	:= GetProcAddress(ASqlLibModule, 'SQLFetch');
  @ACalls.FSQLFetchScroll     	:= GetProcAddress(ASqlLibModule, 'SQLFetchScroll');
  @ACalls.FSQLFreeConnect     	:= GetProcAddress(ASqlLibModule, 'SQLFreeConnect');
  @ACalls.FSQLFreeEnv         	:= GetProcAddress(ASqlLibModule, 'SQLFreeEnv');
  @ACalls.FSQLFreeHandle      	:= GetProcAddress(ASqlLibModule, 'SQLFreeHandle');
  @ACalls.FSQLFreeStmt        	:= GetProcAddress(ASqlLibModule, 'SQLFreeStmt');
  @ACalls.FSQLGetConnectAttr  	:= GetProcAddress(ASqlLibModule, 'SQLGetConnectAttr');
  @ACalls.FSQLGetConnectOption	:= GetProcAddress(ASqlLibModule, 'SQLGetConnectOption');
  @ACalls.FSQLGetCursorName   	:= GetProcAddress(ASqlLibModule, 'SQLGetCursorName');
  @ACalls.FSQLGetData         	:= GetProcAddress(ASqlLibModule, 'SQLGetData');
  @ACalls.FSQLGetDescField    	:= GetProcAddress(ASqlLibModule, 'SQLGetDescField');
  @ACalls.FSQLGetDescRec      	:= GetProcAddress(ASqlLibModule, 'SQLGetDescRec');
  @ACalls.FSQLGetDiagField    	:= GetProcAddress(ASqlLibModule, 'SQLGetDiagField');
  @ACalls.FSQLGetDiagRec      	:= GetProcAddress(ASqlLibModule, 'SQLGetDiagRec');
  @ACalls.FSQLGetEnvAttr      	:= GetProcAddress(ASqlLibModule, 'SQLGetEnvAttr');
  @ACalls.FSQLGetFunctions    	:= GetProcAddress(ASqlLibModule, 'SQLGetFunctions');
  @ACalls.FSQLGetInfo         	:= GetProcAddress(ASqlLibModule, 'SQLGetInfo');
  @ACalls.FSQLGetStmtAttr     	:= GetProcAddress(ASqlLibModule, 'SQLGetStmtAttr');
  @ACalls.FSQLGetStmtOption   	:= GetProcAddress(ASqlLibModule, 'SQLGetStmtOption');
  @ACalls.FSQLGetTypeInfo     	:= GetProcAddress(ASqlLibModule, 'SQLGetTypeInfo');
  @ACalls.FSQLNumResultCols   	:= GetProcAddress(ASqlLibModule, 'SQLNumResultCols');
  @ACalls.FSQLParamData       	:= GetProcAddress(ASqlLibModule, 'SQLParamData');
  @ACalls.FSQLPrepare         	:= GetProcAddress(ASqlLibModule, 'SQLPrepare');
  @ACalls.FSQLPutData         	:= GetProcAddress(ASqlLibModule, 'SQLPutData');
  @ACalls.FSQLRowCount        	:= GetProcAddress(ASqlLibModule, 'SQLRowCount');
  @ACalls.FSQLSetConnectAttr  	:= GetProcAddress(ASqlLibModule, 'SQLSetConnectAttr');
  @ACalls.FSQLSetConnectOption	:= GetProcAddress(ASqlLibModule, 'SQLSetConnectOption');
  @ACalls.FSQLSetCursorName   	:= GetProcAddress(ASqlLibModule, 'SQLSetCursorName');
  @ACalls.FSQLSetDescField    	:= GetProcAddress(ASqlLibModule, 'SQLSetDescField');
  @ACalls.FSQLSetDescRec      	:= GetProcAddress(ASqlLibModule, 'SQLSetDescRec');
  @ACalls.FSQLSetEnvAttr      	:= GetProcAddress(ASqlLibModule, 'SQLSetEnvAttr');
  @ACalls.FSQLSetParam        	:= GetProcAddress(ASqlLibModule, 'SQLSetParam');
  @ACalls.FSQLSetStmtAttr     	:= GetProcAddress(ASqlLibModule, 'SQLSetStmtAttr');
  @ACalls.FSQLSetStmtOption   	:= GetProcAddress(ASqlLibModule, 'SQLSetStmtOption');
  @ACalls.FSQLSpecialColumns  	:= GetProcAddress(ASqlLibModule, 'SQLSpecialColumns');
  @ACalls.FSQLStatistics      	:= GetProcAddress(ASqlLibModule, 'SQLStatistics');
  @ACalls.FSQLTables          	:= GetProcAddress(ASqlLibModule, 'SQLTables');
  @ACalls.FSQLTransact        	:= GetProcAddress(ASqlLibModule, 'SQLTransact');
		// Function definitions of APIs in both X/Open CLI and ODBC ((sqlcli1.h))
		// APIs defined only by Microsoft SQL Extensions (sqlext.h)
  @ACalls.FSQLBrowseConnect   	:= GetProcAddress(ASqlLibModule, 'SQLBrowseConnect');
  @ACalls.FSQLBulkOperations  	:= GetProcAddress(ASqlLibModule, 'SQLBulkOperations');
  @ACalls.FSQLColAttributes   	:= GetProcAddress(ASqlLibModule, 'SQLColAttributes');
  @ACalls.FSQLColumnPrivileges	:= GetProcAddress(ASqlLibModule, 'SQLColumnPrivileges');
  @ACalls.FSQLDescribeParam   	:= GetProcAddress(ASqlLibModule, 'SQLDescribeParam');
  @ACalls.FSQLExtendedFetch   	:= GetProcAddress(ASqlLibModule, 'SQLExtendedFetch');
  @ACalls.FSQLForeignKeys   	:= GetProcAddress(ASqlLibModule, 'SQLForeignKeys');
  @ACalls.FSQLMoreResults     	:= GetProcAddress(ASqlLibModule, 'SQLMoreResults');
  @ACalls.FSQLNativeSql       	:= GetProcAddress(ASqlLibModule, 'SQLNativeSql');
  @ACalls.FSQLNumParams       	:= GetProcAddress(ASqlLibModule, 'SQLNumParams');
  @ACalls.FSQLParamOptions    	:= GetProcAddress(ASqlLibModule, 'SQLParamOptions');
  @ACalls.FSQLPrimaryKeys     	:= GetProcAddress(ASqlLibModule, 'SQLPrimaryKeys');
  @ACalls.FSQLProcedureColumns	:= GetProcAddress(ASqlLibModule, 'SQLProcedureColumns');
  @ACalls.FSQLProcedures      	:= GetProcAddress(ASqlLibModule, 'SQLProcedures');
  @ACalls.FSQLSetPos   		:= GetProcAddress(ASqlLibModule, 'SQLSetPos');
  @ACalls.FSQLTablePrivileges 	:= GetProcAddress(ASqlLibModule, 'SQLTablePrivileges');
  @ACalls.FSQLDrivers  		:= GetProcAddress(ASqlLibModule, 'SQLDrivers');
  @ACalls.FSQLBindParameter   	:= GetProcAddress(ASqlLibModule, 'SQLBindParameter');
  @ACalls.FSQLDriverConnect   	:= GetProcAddress(ASqlLibModule, 'SQLDriverConnect');
end;

procedure ResetApiCalls(ACalls: TOdbcFunctions);
begin
  @ACalls.FSQLAllocConnect    	:= nil;
  @ACalls.FSQLAllocEnv        	:= nil;
  @ACalls.FSQLAllocHandle     	:= nil;
  @ACalls.FSQLAllocStmt       	:= nil;
  @ACalls.FSQLBindCol         	:= nil;
  @ACalls.FSQLCancel          	:= nil;
  @ACalls.FSQLCloseCursor     	:= nil;
  @ACalls.FSQLColAttribute    	:= nil;
  @ACalls.FSQLColumns         	:= nil;
  @ACalls.FSQLConnect         	:= nil;
  @ACalls.FSQLCopyDesc        	:= nil;
  @ACalls.FSQLDataSources     	:= nil;
  @ACalls.FSQLDescribeCol     	:= nil;
  @ACalls.FSQLDisconnect      	:= nil;
  @ACalls.FSQLEndTran         	:= nil;
  @ACalls.FSQLError           	:= nil;
  @ACalls.FSQLExecDirect      	:= nil;
  @ACalls.FSQLExecute         	:= nil;
  @ACalls.FSQLFetch           	:= nil;
  @ACalls.FSQLFetchScroll     	:= nil;
  @ACalls.FSQLFreeConnect     	:= nil;
  @ACalls.FSQLFreeEnv         	:= nil;
  @ACalls.FSQLFreeHandle      	:= nil;
  @ACalls.FSQLFreeStmt        	:= nil;
  @ACalls.FSQLGetConnectAttr  	:= nil;
  @ACalls.FSQLGetConnectOption	:= nil;
  @ACalls.FSQLGetCursorName   	:= nil;
  @ACalls.FSQLGetData         	:= nil;
  @ACalls.FSQLGetDescField    	:= nil;
  @ACalls.FSQLGetDescRec      	:= nil;
  @ACalls.FSQLGetDiagField    	:= nil;
  @ACalls.FSQLGetDiagRec      	:= nil;
  @ACalls.FSQLGetEnvAttr      	:= nil;
  @ACalls.FSQLGetFunctions    	:= nil;
  @ACalls.FSQLGetInfo         	:= nil;
  @ACalls.FSQLGetStmtAttr     	:= nil;
  @ACalls.FSQLGetStmtOption   	:= nil;
  @ACalls.FSQLGetTypeInfo     	:= nil;
  @ACalls.FSQLNumResultCols   	:= nil;
  @ACalls.FSQLParamData       	:= nil;
  @ACalls.FSQLPrepare         	:= nil;
  @ACalls.FSQLPutData         	:= nil;
  @ACalls.FSQLRowCount        	:= nil;
  @ACalls.FSQLSetConnectAttr  	:= nil;
  @ACalls.FSQLSetConnectOption	:= nil;
  @ACalls.FSQLSetCursorName   	:= nil;
  @ACalls.FSQLSetDescField    	:= nil;
  @ACalls.FSQLSetDescRec      	:= nil;
  @ACalls.FSQLSetEnvAttr      	:= nil;
  @ACalls.FSQLSetParam        	:= nil;
  @ACalls.FSQLSetStmtAttr     	:= nil;
  @ACalls.FSQLSetStmtOption   	:= nil;
  @ACalls.FSQLSpecialColumns  	:= nil;
  @ACalls.FSQLStatistics      	:= nil;
  @ACalls.FSQLTables          	:= nil;
  @ACalls.FSQLTransact        	:= nil;
		// APIs defined only by Microsoft SQL Extensions (sqlext.h)
  @ACalls.FSQLBrowseConnect   	:= nil;
  @ACalls.FSQLBulkOperations  	:= nil;
  @ACalls.FSQLColAttributes   	:= nil;
  @ACalls.FSQLColumnPrivileges	:= nil;
  @ACalls.FSQLDescribeParam   	:= nil;
  @ACalls.FSQLExtendedFetch   	:= nil;
  @ACalls.FSQLForeignKeys     	:= nil;
  @ACalls.FSQLMoreResults     	:= nil;
  @ACalls.FSQLNativeSql       	:= nil;
  @ACalls.FSQLNumParams       	:= nil;
  @ACalls.FSQLParamOptions    	:= nil;
  @ACalls.FSQLPrimaryKeys     	:= nil;
  @ACalls.FSQLProcedureColumns	:= nil;
  @ACalls.FSQLProcedures      	:= nil;
  @ACalls.FSQLSetPos          	:= nil;
  @ACalls.FSQLTablePrivileges 	:= nil;
  @ACalls.FSQLDrivers         	:= nil;
  @ACalls.FSQLBindParameter   	:= nil;
  @ACalls.FSQLDriverConnect   	:= nil;
end;

procedure SetProcAddresses;
begin
  SetApiCalls( hSqlLibModule, OdbCalls );
end;

procedure ResetProcAddresses;
begin
  ResetApiCalls( OdbCalls );
end;

procedure LoadSqlLib;
begin
  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 0) then begin
      hSqlLibModule := LoadLibrary( PChar( SrvApiDLLs[stODBC] ) );
      if (hSqlLibModule = 0) then
        raise Exception.CreateFmt(SErrLibLoading, [ SrvApiDLLs[stODBC] ]);
      Inc(SqlLibRefCount);
      SetProcAddresses;
    end else
      Inc(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;

procedure FreeSqlLib;
begin
  if SqlLibRefCount = 0 then
    Exit;

  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 1) then begin
      if FreeLibrary(hSqlLibModule) then
        hSqlLibModule := 0
      else
        raise Exception.CreateFmt(SErrLibUnloading, [ SrvApiDLLs[stODBC] ]);
      Dec(SqlLibRefCount);
      ResetProcAddresses;
    end else
      Dec(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;


{ ESDOdbcError }
constructor ESDOdbcError.CreateWithSqlState(AErrorCode, ANativeError: TSDEResult; const AMsg, ASqlState: string);
begin
  inherited CreateDefPos(AErrorCode, ANativeError, AMsg);
  FSqlState	:= ASqlState;
end;

{ TICustomOdbcDatabase }
constructor TICustomOdbcDatabase.Create(ADatabase: TSDDatabase);
begin
  inherited Create(ADatabase);

  FHandle 	:= nil;
  FDriverOdbcVer:= 0;
  FIsTransSupported	:= False;
	// by default IsRTrimChar = True
  FIsRTrimChar := not(AnsiUpperCase( Trim( ADatabase.Params.Values[szRTRIMCHAROUTPUT] ) ) = SFalseString);
	// by default IsSingleConn = False
  FIsSingleConn := AnsiUpperCase( Trim( ADatabase.Params.Values[szSINGLECONN] ) ) = STrueString;
  FCurrDataSet	:= nil;
end;

destructor TICustomOdbcDatabase.Destroy;
begin
  FreeHandle;

  if AcquiredHandle then
    FreeSqlLib;

  inherited Destroy;
end;

function TICustomOdbcDatabase.GetCalls: TOdbcFunctions;
begin
  Result := FCalls;
end;

procedure TICustomOdbcDatabase.CheckHDBC(AHandle: SQLHDBC; Status: TSDEResult);
begin
  CheckHandle( SQL_HANDLE_DBC, SQLHANDLE(AHandle), Status );
end;

procedure TICustomOdbcDatabase.CheckHSTMT(AHandle: SQLHSTMT; Status: TSDEResult);
begin
  CheckHandle( SQL_HANDLE_STMT, SQLHANDLE(AHandle), Status );
end;

procedure TICustomOdbcDatabase.Check(Status: TSDEResult);
begin
  CheckHDBC( DbcHandle, Status );
end;

procedure TICustomOdbcDatabase.CheckHandle(AHandleType: SQLSMALLINT; AHandle: SQLHANDLE; AStatus: TSDEResult);
const
  MAXERRMSG = 1000;
var
  rcd: RETCODE;
  sqlstate: array[0..5] of Char;
  Msg: array[0..MAXERRMSG] of Char;
  ErrCode: SQLINTEGER;
  MsgLen: SQLSMALLINT;
begin
  ResetIdleTimeOut;

  if (AStatus = SQL_SUCCESS) or
     (AStatus = SQL_SUCCESS_WITH_INFO) or
     (AStatus = SQL_NO_DATA)
  then
    Exit;

  ErrCode := -1;
  FillChar( msg, MAXERRMSG, #$0 );
  if Assigned( Calls.SQLGetDiagRec ) then
    rcd := Calls.SQLGetDiagRec( AHandleType, AHandle, 1, sqlstate, @ErrCode, msg, MAXERRMSG, @MsgLen )
  else begin
    if AHandleType = SQL_HANDLE_STMT then
      rcd := Calls.SQLError( EnvHandle, DbcHandle, AHandle, sqlstate, @ErrCode, msg, MAXERRMSG, @MsgLen )
    else
      rcd := Calls.SQLError( EnvHandle, AHandle, SQLHSTMT(SQL_NULL_HSTMT), sqlstate, @ErrCode, msg, MAXERRMSG, @MsgLen );
  end;
  if rcd = SQL_INVALID_HANDLE then
    StrFmt( Msg, PChar(SFatalError), ['Invalid handle in TICustomOdbcDatabase.Check'] )
  else if rcd = SQL_NO_DATA then
    StrFmt( Msg, PChar(SFatalError), ['No diagnostic data in TICustomOdbcDatabase.Check'] );

  ResetBusyState;

  RaiseSDEngineError( ErrCode, ErrCode, Msg, SqlState );
end;

function TICustomOdbcDatabase.GetHandle: PSDHandleRec;
begin
  Result := FHandle;
end;

function TICustomOdbcDatabase.GetEnvHandle: SQLHENV;
begin
  ASSERT( Assigned(FHandle) and Assigned(POdbcSrvInfo(FHandle^.SrvInfo)),
  	'TICustomOdbcDatabase.GetEnvHandle' );
  Result := POdbcSrvInfo(FHandle^.SrvInfo)^.hEnv;
end;

function TICustomOdbcDatabase.GetDbcHandle: SQLHDBC;
begin
  ASSERT( Assigned(FHandle) and Assigned(POdbcSrvInfo(FHandle^.SrvInfo)),
  	'TICustomOdbcDatabase.GetDbcHandle' );
  Result := POdbcSrvInfo(FHandle^.SrvInfo)^.hDbc;
end;

procedure TICustomOdbcDatabase.AllocHandle;
var
  s: POdbcSrvInfo;
begin
  New(FHandle);
  FillChar( FHandle^, SizeOf(FHandle^), $0 );
  FHandle^.SrvType := Ord( Database.ServerType );

  New(s);
  FillChar( s^, SizeOf(s^), $0 );
  FHandle^.SrvInfo := s;
end;

procedure TICustomOdbcDatabase.FreeHandle;
begin
  if Assigned(FHandle) then begin
    if Assigned(FHandle^.SrvInfo) then begin
      Dispose( POdbcSrvInfo(FHandle^.SrvInfo) );
      POdbcSrvInfo(FHandle^.SrvInfo) := nil;
    end;
    Dispose( FHandle );
    FHandle := nil;
  end;
end;

procedure TICustomOdbcDatabase.AllocOdbcHandles;
begin
  ASSERT( not Assigned(FHandle), 'TICustomOdbcDatabase.AllocHandles' );

  AllocHandle;

  OdbcAllocHandle( SQL_HANDLE_ENV, SQLHANDLE(SQL_NULL_HANDLE),
  		@POdbcSrvInfo(FHandle^.SrvInfo)^.hEnv );
	// Declaring the application's ODBC version, it's necessary after environment handle allocating
  if Assigned(Calls.SQLSetEnvAttr) then
    Calls.SQLSetEnvAttr( EnvHandle, SQL_ATTR_ODBC_VERSION, SQLPOINTER(SQL_OV_ODBC3), 0 );

  CheckHandle( SQL_HANDLE_ENV, EnvHandle,
  	OdbcAllocHandle( SQL_HANDLE_DBC, SQLHANDLE(EnvHandle),
        		 @POdbcSrvInfo(FHandle^.SrvInfo)^.hDbc )
  );
end;

procedure TICustomOdbcDatabase.FreeOdbcHandles;
begin
  if Assigned(FHandle) then begin
    if Assigned(FHandle^.SrvInfo) then begin

      if Assigned( POdbcSrvInfo(FHandle^.SrvInfo)^.hDbc ) then
        OdbcFreeHandle( SQL_HANDLE_DBC, SQLHANDLE(POdbcSrvInfo(FHandle^.SrvInfo)^.hDbc) );
      POdbcSrvInfo(FHandle^.SrvInfo)^.hDbc := nil;

      if Assigned( POdbcSrvInfo(FHandle^.SrvInfo)^.hEnv ) then
        OdbcFreeHandle( SQL_HANDLE_ENV, SQLHANDLE(POdbcSrvInfo(FHandle^.SrvInfo)^.hEnv) );
      POdbcSrvInfo(FHandle^.SrvInfo)^.hEnv := nil;
    end;
  end;
  FreeHandle;
end;

procedure TICustomOdbcDatabase.SetHandle(AHandle: PSDHandleRec);
begin
  LoadSqlLib;

  AllocHandle;

  POdbcSrvInfo(FHandle^.SrvInfo)^.hEnv	:=
  	POdbcSrvInfo(PSDHandleRec(AHandle)^.SrvInfo)^.hEnv;
  POdbcSrvInfo(FHandle^.SrvInfo)^.hDbc	:=
  	POdbcSrvInfo(PSDHandleRec(AHandle)^.SrvInfo)^.hDbc;
end;

function TICustomOdbcDatabase.OdbcAllocHandle(fHandleType: SQLSMALLINT; hInput: SQLHANDLE; hOutput: PSQLHANDLE): SQLRETURN;
begin
  with Calls do begin
    if Assigned(FSQLAllocHandle) then
      Result := FSQLAllocHandle(fHandleType, hInput, hOutput)
    else begin
      case fHandleType of
        SQL_HANDLE_ENV:	Result := FSQLAllocEnv(PSQLHENV(hOutput));
        SQL_HANDLE_DBC:	Result := FSQLAllocConnect(hInput, PSQLHDBC(hOutput));
        SQL_HANDLE_STMT:Result := FSQLAllocStmt(hInput, PSQLHSTMT(hOutput));
      else
        Result := SQL_INVALID_HANDLE;
      end;
    end;
  end;
end;

function TICustomOdbcDatabase.OdbcFreeHandle(fHandleType: SQLSMALLINT; hHandle: SQLHANDLE): SQLRETURN;
begin
  with Calls do begin
    if Assigned(FSQLFreeHandle) then
      Result := FSQLFreeHandle(fHandleType, hHandle)
    else begin
      case fHandleType of
        SQL_HANDLE_ENV:	Result := FSQLFreeEnv(hHandle);
        SQL_HANDLE_DBC:	Result := FSQLFreeConnect(hHandle);
        SQL_HANDLE_STMT:Result := FSQLFreeStmt(hHandle, SQL_DROP);
      else
        Result := SQL_INVALID_HANDLE;
      end;
    end;
  end;
end;

function TICustomOdbcDatabase.OdbcGetMaxCatalogNameLen: SQLUSMALLINT;
begin
  CheckHDBC( DbcHandle,
   	Calls.SQLGetInfo( DbcHandle, SQL_MAX_CATALOG_NAME_LEN, @Result, SizeOf(Result), nil )
  );
end;

function TICustomOdbcDatabase.OdbcGetMaxSchemaNameLen: SQLUSMALLINT;
begin
  CheckHDBC( DbcHandle,
   	Calls.SQLGetInfo( DbcHandle, SQL_MAX_SCHEMA_NAME_LEN, @Result, SizeOf(Result), nil )
  );
end;

function TICustomOdbcDatabase.OdbcGetMaxTableNameLen: SQLUSMALLINT;
begin
  CheckHDBC( DbcHandle,
    	Calls.SQLGetInfo( DbcHandle, SQL_MAX_TABLE_NAME_LEN, @Result, SizeOf(Result), nil )
  );
end;

function TICustomOdbcDatabase.OdbcGetMaxFieldNameLen: SQLUSMALLINT;
begin
  CheckHDBC( DbcHandle,
    	Calls.SQLGetInfo( DbcHandle, SQL_MAX_COLUMN_NAME_LEN, @Result, SizeOf(Result), nil )
  );
end;

function TICustomOdbcDatabase.OdbcGetMaxProcNameLen: SQLUSMALLINT;
begin
  CheckHDBC( DbcHandle,
    	Calls.SQLGetInfo( DbcHandle, SQL_MAX_PROCEDURE_NAME_LEN, @Result, SizeOf(Result), nil )
  );
end;

function TICustomOdbcDatabase.OdbcSetConnectAttr(hDbc: SQLHDBC; fOption: SQLINTEGER; pvParam: SQLPOINTER; fStrLen: SQLINTEGER): SQLRETURN;
var
  fExists: SQLUSMALLINT;
begin
  with Calls do begin
    fExists := SQL_FALSE;
    if Assigned(FSQLGetFunctions) then
      Check( FSQLGetFunctions(hDbc, SQL_API_SQLSETCONNECTATTR, fExists) );
    if (fExists = SQL_FALSE) and (DriverOdbcMajor > 2) then
      fExists := SQL_TRUE;

    if (fExists = SQL_TRUE) and Assigned(FSQLSetConnectAttr) then
      Result := FSQLSetConnectAttr(hDbc, fOption, pvParam, fStrLen)
    else
      Result := FSQLSetConnectOption(hDbc, fOption, SQLUINTEGER(pvParam));
  end;
end;

function TICustomOdbcDatabase.OdbcSetStmtAttr(hStmt: SQLHSTMT; fOption: SQLINTEGER; pvParam: SQLPOINTER; fStrLen: SQLINTEGER): SQLRETURN;
begin
  with Calls do begin
    if Assigned(FSQLSetStmtAttr) then
      Result := FSQLSetStmtAttr(hStmt, fOption, pvParam, fStrLen)
    else
      Result := FSQLSetStmtOption(hStmt, fOption, SQLUINTEGER(pvParam));
  end;
end;

function TICustomOdbcDatabase.OdbcTransact(fEndType: SQLSMALLINT): SQLRETURN;
begin
  with Calls do begin
    if Assigned(FSQLEndTran) then
      Result := FSQLEndTran(SQL_HANDLE_DBC, DbcHandle, fEndType)
    else
      Result := FSQLTransact(EnvHandle, DbcHandle, fEndType)
  end;
end;

procedure TICustomOdbcDatabase.GetStmtResult(const Stmt: string; List: TStrings);
var
  q: TSDQuery;
begin
  q := TSDQuery.Create(nil);
  try
    q.DatabaseName := Self.Database.DatabaseName;
    q.SQL.Add( Stmt );
    q.Open;
    while not q.EOF do begin
      List.Add( q.Fields[0].AsString );

      q.Next;
    end;
  finally
    q.Free;
  end;
end;

function TICustomOdbcDatabase.GetDriverOdbcMajor: Word;
begin
  if FDriverOdbcVer <= 0 then
    FDriverOdbcVer := GetDriverODBCVersion;

  Result := HiWord( FDriverOdbcVer );
end;

function TICustomOdbcDatabase.GetDriverOdbcMinor: Word;
begin
  if FDriverOdbcVer <= 0 then
    FDriverOdbcVer := GetDriverODBCVersion;

  Result := LoWord( FDriverOdbcVer );
end;

function TICustomOdbcDatabase.GetDriverOdbcVersion: LongInt;
const
  MaxVerLen = 512;
var
  szVer: PChar;
  RetBytes: SQLSMALLINT;
begin
  RetBytes := 0;
  szVer := StrAlloc( MaxVerLen );
  try
    FillChar( szVer^, MaxVerLen, 0 );
	// get SQL_DRIVER_ODBC_VER parameter
    Check( Calls.SQLGetInfo(DbcHandle, SQL_DRIVER_ODBC_VER, szVer, MaxVerLen, @RetBytes) );
    Result := VersionStringToDWORD( StrPas(szVer) );
  finally
    StrDispose( szVer );
  end;
end;

function TICustomOdbcDatabase.GetClientVersion: LongInt;
const
  MaxVerLen = 512;
var
  szVer: PChar;
  RetBytes: SQLSMALLINT;
begin
  RetBytes := 0;
  szVer := StrAlloc( MaxVerLen );
  try
    FillChar( szVer^, MaxVerLen, 0 );
	// get SQL_DRIVER_VER parameter
    Check( Calls.SQLGetInfo(DbcHandle, SQL_DRIVER_VER, szVer, MaxVerLen, @RetBytes) );
    Result := VersionStringToDWORD( StrPas(szVer) );
  finally
    StrDispose( szVer );
  end;
end;

function TICustomOdbcDatabase.GetServerVersion: LongInt;
const
  MaxVerLen = 512;
var
  szVer: PChar;
  RetBytes: SQLSMALLINT;
begin
  RetBytes := 0;
  szVer := StrAlloc( MaxVerLen );
  try
    FillChar( szVer^, MaxVerLen, 0 );
	// get SQL_DRIVER_VER parameter
    Check( Calls.SQLGetInfo(DbcHandle, SQL_DBMS_VER, szVer, MaxVerLen, @RetBytes) );
    Result := VersionStringToDWORD( StrPas(szVer) );
  finally
    StrDispose( szVer );
  end;
end;

function TICustomOdbcDatabase.GetVersionString: string;
const
  MaxVerLen = 512;
var
  szVer: PChar;
  RetBytes: SQLSMALLINT;
begin
  RetBytes := 0;
  szVer := StrAlloc( MaxVerLen );
  try
    FillChar( szVer^, MaxVerLen, 0 );
	// get SQL_DRIVER_VER parameter
    Check( Calls.SQLGetInfo(DbcHandle, SQL_DBMS_NAME, szVer, MaxVerLen, @RetBytes) );
    Result := szVer;
    Result := Format('%s Release %d.%d', [Result, Database.ServerMajor, Database.ServerMinor]);
  finally
    StrDispose( szVer );
  end;
end;

function TICustomOdbcDatabase.GetIsNeedLongDataLen: Boolean;
const
  MaxBufLen = 10;
var
  szValue: array[0..MaxBufLen] of Char;
  RetBytes: SQLSMALLINT;
begin
  RetBytes := 0;
	// get SQL_NEED_LONG_DATA_LEN parameter
  Check( Calls.SQLGetInfo(DbcHandle, SQL_NEED_LONG_DATA_LEN, PChar(@szValue), MaxBufLen, @RetBytes) );
  Result := (RetBytes > 0) and (szValue[0] = 'Y');
end;

procedure TICustomOdbcDatabase.Logon(const sRemoteDatabase, sUserName, sPassword: string);
var
  LoginTimeout: Integer;
  sConnStrOut: string;
  ConnStrOut: SQLSMALLINT;
begin
  ConnStrOut := 0;
  SetLength(sConnStrOut, 1024);
  try
    LoadSqlLib;
    AllocOdbcHandles;

	// set login timeout before connect
    if Trim( Database.Params.Values[szLOGINTIMEOUT] ) <> '' then begin
      LoginTimeout := StrToIntDef( Trim( Database.Params.Values[szLOGINTIMEOUT] ), 0 );
	// Sequence error on call OdbcSetConnectAttr now
      if Assigned(Calls.FSQLSetConnectAttr) then
        Check( Calls.FSQLSetConnectAttr(DbcHandle, SQL_ATTR_LOGIN_TIMEOUT, SQLPOINTER(LoginTimeout), 0) )
      else
        Check( Calls.FSQLSetConnectOption(DbcHandle, SQL_ATTR_LOGIN_TIMEOUT, LoginTimeout) );
    end;

    	// if DSN string with options
    if AnsiPos('=', sRemoteDatabase) > 0 then
      Check( Calls.SQLDriverConnect( DbcHandle, 0, PChar(sRemoteDatabase), SQL_NTS,
      			PChar(sConnStrOut), Length(sConnStrOut), @ConnStrOut, SQL_DRIVER_NOPROMPT )
      )
    else	// if sRemoteDatabase contains one word, which is datasource name (from ODBC config)
      Check( Calls.SQLConnect( DbcHandle, PChar(sRemoteDatabase), SQL_NTS,
    			PChar(sUserName), SQL_NTS, PChar(sPassword), SQL_NTS )
      );

      	// get and save(in private field) ODBC version, which is supported by driver
    GetDriverOdbcMajor;
  except
    FreeOdbcHandles;
    FreeSqlLib;

    raise;
  end;
end;

procedure TICustomOdbcDatabase.Logoff(Force: Boolean);
begin
	// commit in case of successful disconnect, else SQLDisconnect returns SQLSTATE=25000 sometimes(noted for DB2 v6.1)
  if FIsTransSupported then
    if Database.InTransaction then
      OdbcTransact( SQL_ROLLBACK )
    else
      OdbcTransact( SQL_COMMIT );
  Calls.SQLDisconnect( DbcHandle );

  FreeOdbcHandles;
  FreeSqlLib;

  FDriverOdbcVer:= 0;
end;

procedure TICustomOdbcDatabase.ReleaseDBHandle(ADataSet: TSDDataSet; IsFetchAll: Boolean);
var
  ds: TSDDataSet;
begin
  if Assigned(FCurrDataSet) and (ADataSet <> FCurrDataSet) then begin
    ds := FCurrDataSet;
    FCurrDataSet := nil;
    try
      if IsFetchAll then begin
        if ds is TSDStoredProc then
          TSDStoredProc(ds).GetResults
        else if ds is TSDQuery then
          TSDQuery(ds).FetchAll
        else
          raise Exception.Create( Format(SFatalError, ['TICustomOdbcDatabase.ReleaseDBHandle']) );
      end;
    except
      FCurrDataSet := ds;
      raise;
    end;
  end;
  FCurrDataSet := ADataSet;
end;

procedure TICustomOdbcDatabase.StartTransaction;
begin
  if AutoCommit then
    SetAutoCommitOption(False);
end;

procedure TICustomOdbcDatabase.Commit;
begin
  Check( OdbcTransact( SQL_COMMIT ) );

  if AutoCommit then
    SetAutoCommitOption(True);
end;

procedure TICustomOdbcDatabase.Rollback;
begin
  Check( OdbcTransact( SQL_ROLLBACK ) );

  if AutoCommit then
    SetAutoCommitOption(True);
end;

procedure TICustomOdbcDatabase.SetAutoCommitOption(Value: Boolean);
begin
  if not FIsTransSupported then
    Exit;

  if Value then
    Check( OdbcSetConnectAttr( DbcHandle, SQL_ATTR_AUTOCOMMIT , SQLPOINTER(SQL_AUTOCOMMIT_ON), 0 ) )
  else
    Check( OdbcSetConnectAttr( DbcHandle, SQL_ATTR_AUTOCOMMIT , SQLPOINTER(SQL_AUTOCOMMIT_OFF), 0 ) );
end;

procedure TICustomOdbcDatabase.SetDefaultParams;
var
  sValue: string;
  V: SQLUINTEGER;
  TxnProp, DescValue: SQLUSMALLINT;
begin
  with Calls do begin
    Check( SQLGetInfo( DbcHandle, SQL_CURSOR_COMMIT_BEHAVIOR, @DescValue, SizeOf(SQLUSMALLINT), nil ) );
    FCursorPreservedOnCommit := DescValue = SQL_CB_PRESERVE;
    Check( SQLGetInfo( DbcHandle, SQL_CURSOR_ROLLBACK_BEHAVIOR, @DescValue, SizeOf(SQLUSMALLINT), nil ) );
    FCursorPreservedOnRollback := DescValue = SQL_CB_PRESERVE;

	// set command timeout value, if it's set
    sValue := Trim( Database.Params.Values[szCMDTIMEOUT] );
    if sValue <> '' then begin
      V := StrToIntDef( sValue, 0 );
      	// Set timeout for a situation not associated with query execution or login
	// Exclude Check call, because the feature isn't supported for MS Access, for example
      OdbcSetConnectAttr( DbcHandle, SQL_ATTR_CONNECTION_TIMEOUT, SQLPOINTER( V ), SQL_IS_UINTEGER );
    end;

    TxnProp := SQL_TC_NONE;
    Check( SQLGetInfo( DbcHandle, SQL_TXN_CAPABLE, @TxnProp, SizeOf(SQLUSMALLINT), nil ) );
    FIsTransSupported := TxnProp <> SQL_TC_NONE;
    	// if driver supports transactions
    if FIsTransSupported then begin
    	// process an autocommit option
      if not AutoCommitDef then
        SetAutoCommitOption( AutoCommit );

	// set default isolation level
      SetTransIsolation(Database.TransIsolation);
    end;
  end;
end;

procedure TICustomOdbcDatabase.SetTransIsolation(Value: TSDTransIsolation);
const
  IsolLevel: array[TSDTransIsolation] of SQLINTEGER =
  	(SQL_TXN_READ_UNCOMMITTED,
         SQL_TXN_READ_COMMITTED,
         SQL_TXN_REPEATABLE_READ
        );
begin
  Check( OdbcSetConnectAttr( DbcHandle, SQL_ATTR_TXN_ISOLATION, SQLPOINTER(IsolLevel[Value]), 0 ) );
end;

// Returns True if field type required converting from internal database format
function TICustomOdbcDatabase.RequiredCnvtFieldType(FieldType: TFieldType): Boolean;
begin
  Result := IsDateTimeType( FieldType );
end;

function TICustomOdbcDatabase.FieldDataType(ExtDataType: Integer): TFieldType;
begin
  case ExtDataType of
	// Standard SQL data types
    SQL_CHAR:		Result := ftString;
    SQL_NUMERIC,
    SQL_DECIMAL:	Result := ftFloat;
    SQL_INTEGER:	Result := ftInteger;
    SQL_SMALLINT:	Result := ftSmallInt;
    SQL_FLOAT,
    SQL_REAL,
    SQL_DOUBLE:		Result := ftFloat;
    SQL_VARCHAR:	Result := ftString;
	// One-parameter shortcuts for date/time data types
    SQL_TYPE_DATE:	Result := ftDate;
    SQL_TIME,
    SQL_TYPE_TIME:	Result := ftTime;
    SQL_DATETIME,
    SQL_TIMESTAMP,
    SQL_TYPE_TIMESTAMP:	Result := ftDateTime;
	// SQL extended data types
    SQL_BIGINT: 	Result := {$IFDEF SD_VCL4} ftLargeInt {$ELSE} ftInteger {$ENDIF};
    SQL_TINYINT:	Result := ftSmallInt;
    SQL_BIT:		Result := ftBoolean;
    SQL_BINARY:		Result := ftBytes;
    SQL_VARBINARY:	Result := ftVarBytes;
    SQL_LONGVARBINARY:	Result := ftBlob;
    SQL_LONGVARCHAR:	Result := ftMemo;
    	// unicode data types
    SQL_WCHAR,
    SQL_WVARCHAR:	Result := ftString;
    SQL_WLONGVARCHAR:	Result := ftMemo;
  else
    Result := ftUnknown;
  end;
end;

function TICustomOdbcDatabase.NativeDataSize(FieldType: TFieldType): Word;
const
  { Converting from TFieldType to Program Data Type(ODBC) }
  OdbcDataSizeMap: array[TFieldType] of Word = ( 0,	// ftUnknown
	// ftString, ftSmallint, ftInteger, 	ftWord, 	ftBoolean
	0, SizeOf(SQLSMALLINT), SizeOf(SQLINTEGER), SizeOf(SQLINTEGER), SizeOf(SQLSMALLINT),
	// ftFloat, 	ftCurrency, 		ftBCD, ftDate, 		ftTime
        SizeOf(SQLDOUBLE), SizeOf(SQLDOUBLE),	0, SizeOf(TDATE_STRUCT), SizeOf(TTIME_STRUCT),
        // ftDateTime, ftBytes, ftVarBytes, ftAutoInc, ftBlob
        SizeOf(TTIMESTAMP_STRUCT), 0, 0, SizeOf(SQLINTEGER), 0,
        // ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle
        0,	0,	0,   	0,	0,
        // ftTypedBinary, ftCursor
        0, 	0
{$IFDEF SD_VCL4},
	// ftFixedChar, ftWideString, ftLargeint,
        0,	0,	SizeOf(Int64),
        // ftADT, ftArray, ftReference, ftDataSet
        0,	0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL5},
        // ftOraBlob, ftOraClob, ftVariant,
        0,	0,	0,
        // ftInterface, ftIDispatch, ftGuid
        0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL6},
        // ftTimeStamp, ftFMTBcd
        0,      0
{$ENDIF}
        );
begin
  Result := OdbcDataSizeMap[FieldType];
end;

function TICustomOdbcDatabase.NativeDataType(FieldType: TFieldType): Integer;
const
  { Converting from TFieldType to C(Program) Data Type(ODBC) }
  OdbcDataTypeMap: array[TFieldType] of Integer = ( 0,	// ftUnknown
	// ftString, ftSmallint, ftInteger, ftWord, ftBoolean (TBooleanField.GetDataSize = 2)
	SQL_C_CHAR, SQL_C_SHORT, SQL_C_LONG, SQL_C_LONG, SQL_C_SHORT,
	// ftFloat, ftCurrency, ftBCD, 	ftDate, 	ftTime
        SQL_C_DOUBLE, SQL_C_DOUBLE, 0, SQL_C_TYPE_DATE, SQL_C_TYPE_TIME,
        // ftDateTime, 		ftBytes, ftVarBytes, ftAutoInc, ftBlob
        SQL_C_TYPE_TIMESTAMP, SQL_C_BINARY, SQL_C_BINARY, SQL_C_LONG, SQL_C_BINARY,
        // ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle
        SQL_C_CHAR, SQL_C_BINARY, SQL_C_CHAR,	0,	0,
        // ftTypedBinary, ftCursor
        0,	0
{$IFDEF SD_VCL4},
	// ftFixedChar, ftWideString, ftLargeint,
        0,	0,	SQL_C_SBIGINT,
        // ftADT, ftArray, ftReference, ftDataSet
        0,	0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL5},
        // ftOraBlob, ftOraClob, ftVariant,
        0,	0,	0,
        // ftInterface, ftIDispatch, ftGuid
        0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL6},
        // ftTimeStamp, ftFMTBcd
        0,      0
{$ENDIF}
        );
begin
  Result := OdbcDataTypeMap[FieldType];
  if DriverOdbcMajor < 3 then
    case FieldType of
      ftDate:		Result := SQL_C_DATE;
      ftTime:		Result := SQL_C_TIME;
      ftDateTime:	Result := SQL_C_TIMESTAMP;
    end;
end;

// It's used in SQLBindParameter
function TICustomOdbcDatabase.SqlDataType(FieldType: TFieldType): Integer;
const
  { Converting from TFieldType to SQL Data Type(ODBC) }
  OdbcSqlDataTypeMap: array[TFieldType] of Integer = ( 0,	// ftUnknown
	// ftString, ftSmallint, ftInteger, ftWord, ftBoolean
	SQL_VARCHAR, SQL_SMALLINT, SQL_INTEGER, SQL_INTEGER, SQL_SMALLINT,
	// ftFloat, ftCurrency, ftBCD, 	ftDate, 	ftTime
        SQL_DOUBLE, SQL_DOUBLE, 0, SQL_TYPE_DATE, SQL_TYPE_TIME,
        // ftDateTime, 		ftBytes, ftVarBytes, ftAutoInc, ftBlob
        SQL_TYPE_TIMESTAMP, SQL_BINARY, SQL_VARBINARY, SQL_INTEGER, SQL_LONGVARBINARY,
        // ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle
        SQL_LONGVARCHAR, SQL_LONGVARBINARY, SQL_LONGVARCHAR, 	0,	0,
        // ftTypedBinary, ftCursor
        0,	0
{$IFDEF SD_VCL4},
	// ftFixedChar, ftWideString, ftLargeint,
        0,	0,	SQL_BIGINT,
        // ftADT, ftArray, ftReference, ftDataSet
        0,	0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL5},
        // ftOraBlob, ftOraClob, ftVariant,
        0,	0,	0,
        // ftInterface, ftIDispatch, ftGuid
        0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL6},
        // ftTimeStamp, ftFMTBcd
        0,      0
{$ENDIF}
        );
begin
  Result := OdbcSqlDataTypeMap[FieldType];
  if DriverOdbcMajor < 3 then
    case FieldType of
      ftDate:		Result := SQL_DATE;
      ftTime:		Result := SQL_TIME;
      ftDateTime:	Result := SQL_TIMESTAMP;
    end;
end;

function TICustomOdbcDatabase.SPDescriptionsAvailable: Boolean;
begin
  Result := True;
end;

procedure TICustomOdbcDatabase.GetStoredProcNames(List: TStrings);
const
  MaxCharColLen = 255;
var
  hStmt: SQLHSTMT;
  rcd: SQLRETURN;
  szSchName, szProcName: PChar;
  MaxProcNameLen, MaxSchNameLen: SQLUSMALLINT;
  iSchName, iProcName: SQLINTEGER;
  sSchName, sProcName: string;
  szCatName, szRemarks: array[0..MaxCharColLen] of Char;
  iCatName, iRemarks, iProcType, iInParamNum, iOutParamNum, iResultSetNum: SQLINTEGER;
  ProcType, InParamNum, OutParamNum, ResultSetNum: SQLSMALLINT;
begin
  hStmt := nil;
  szSchName := nil;
  szProcName := nil;
  List.Clear;

  with Calls do try
  	// get max length for a schema(owner) name
    MaxSchNameLen := OdbcGetMaxSchemaNameLen;
    if MaxSchNameLen <= 0 then
      MaxSchNameLen := 128;
    szSchName := StrAlloc( MaxSchNameLen + 1 );
  	// get max length for a procedure name
    MaxProcNameLen := OdbcGetMaxProcNameLen;
    if MaxProcNameLen <= 0 then
      MaxProcNameLen := 128;
    szProcName := StrAlloc( MaxProcNameLen + 1 );
	// allocate a statement handle
    rcd := OdbcAllocHandle( SQL_HANDLE_STMT, SQLHANDLE(DbcHandle), @hStmt );
    CheckHSTMT( hStmt, rcd );
    	// get required result set
    rcd := Calls.SQLProcedures( hStmt, nil, 0, SQL_ALL_SCHEMAS, SQL_NTS, '%', SQL_NTS );
	// Error 'Option feature not implemented'
        // driver(MS Access) can't support catalog, schema or string pattern
    if rcd <> SQL_SUCCESS then
      rcd := Calls.SQLProcedures( hStmt, nil, 0, nil, 0, '%', SQL_NTS );

    CheckHSTMT( hStmt, rcd );
	// bind the required columns
	// bind 2 - PROCEDURE_SCHEM
    rcd := Calls.SQLBindCol( hStmt, 2, SQL_C_CHAR, SQLPOINTER(PChar(szSchName)), MaxSchNameLen+1, @iSchName );
    CheckHSTMT( hStmt, rcd );
	// bind 3 - PROCEDURE_NAME
    rcd := Calls.SQLBindCol( hStmt, 3, SQL_C_CHAR, SQLPOINTER(PChar(szProcName)), MaxProcNameLen+1, @iProcName );
    CheckHSTMT( hStmt, rcd );

	// it's necessary to bind all columns for some(it seems old) drivers
    rcd := Calls.SQLBindCol( hStmt, 1, SQL_C_CHAR, SQLPOINTER(PChar(@szCatName)), MaxCharColLen+1, @iCatName );
    CheckHSTMT( hStmt, rcd );
    rcd := Calls.SQLBindCol( hStmt, 4, SQL_C_SSHORT, SQLPOINTER(@InParamNum), SizeOf(InParamNum), @iInParamNum );
    CheckHSTMT( hStmt, rcd );
    rcd := Calls.SQLBindCol( hStmt, 5, SQL_C_SSHORT, SQLPOINTER(@OutParamNum), SizeOf(OutParamNum), @iOutParamNum );
    CheckHSTMT( hStmt, rcd );
    rcd := Calls.SQLBindCol( hStmt, 6, SQL_C_SSHORT, SQLPOINTER(@ResultSetNum), SizeOf(ResultSetNum), @iResultSetNum );
    CheckHSTMT( hStmt, rcd );
    rcd := Calls.SQLBindCol( hStmt, 7, SQL_C_CHAR, SQLPOINTER(PChar(@szRemarks)), MaxCharColLen, @iRemarks );
    CheckHSTMT( hStmt, rcd );
    rcd := Calls.SQLBindCol( hStmt, 8, SQL_C_SSHORT, SQLPOINTER(@ProcType), SizeOf(ProcType), @iProcType );
    CheckHSTMT( hStmt, rcd );

	// fetch records
    rcd := Calls.SQLFetch( hStmt );
    if rcd = SQL_ERROR then
      CheckHSTMT( hStmt, rcd );
    while rcd = SQL_SUCCESS do begin
      if iSchName <> SQL_NULL_DATA then
        sSchName := Copy( szSchName, 1, iSchName );
      if iProcName <> SQL_NULL_DATA then
        sProcName := Copy( szProcName, 1, iProcName );
      if Length(Trim(sSchName)) > 0 then
        sProcName := sSchName + '.' + sProcName;
      List.Add( sProcName );
      rcd := Calls.SQLFetch( hStmt );
      if rcd = SQL_ERROR then
        CheckHSTMT( hStmt, rcd );
    end;

  finally
    if hStmt <> nil then
      OdbcFreeHandle( SQL_HANDLE_STMT, SQLHANDLE(hStmt) );
    if szSchName <> nil then
      StrDispose( szSchName );
    if szProcName <> nil then
      StrDispose( szProcName );
  end;
end;

procedure TICustomOdbcDatabase.GetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings);
const
  MaxCharColLen = 255;
var
  hStmt: SQLHSTMT;
  rcd: SQLRETURN;
  szSchName, szTblName, szTblType: PChar;
  MaxSchNameLen, MaxTblNameLen: SQLUSMALLINT;
  iSchName, iTblName, iTblType: SQLINTEGER;
  sSchName, sTblName: string;
  sRqrdTblTypes: string;
  szCatName, szRemarks: array[0..MaxCharColLen] of Char;
  iCatName, iRemarks: SQLINTEGER;
begin
  hStmt := nil;
  szSchName := nil;
  szTblName := nil;
  szTblType := nil;
  List.Clear;

  with Calls do try
  	// get max length for a schema(owner) name
    MaxSchNameLen := OdbcGetMaxSchemaNameLen;
    if MaxSchNameLen <= 0 then
      MaxSchNameLen := 128;
    szSchName := StrAlloc( MaxSchNameLen + 1 );
  	// get max length for a table name
    MaxTblNameLen := OdbcGetMaxTableNameLen;
    if MaxTblNameLen <= 0 then
      MaxTblNameLen := 128;
    szTblName := StrAlloc( MaxTblNameLen + 1 );
    szTblType := StrAlloc( MaxTblNameLen + 1 );
	// allocate a statement handle
    rcd := OdbcAllocHandle( SQL_HANDLE_STMT, SQLHANDLE(DbcHandle), @hStmt );
    CheckHSTMT( hStmt, rcd );
    	// get required result set
    if Length(Trim(Pattern)) = 0 then Pattern := '%';
    sRqrdTblTypes := 'TABLE,VIEW';	// it's necessary a separate variable for avooid AV for some drivers(for example: Informix CLI v2.8)
    rcd := SQLTables( hStmt, nil, 0, SQL_ALL_SCHEMAS, SQL_NTS,
    		PChar(Pattern), SQL_NTS, PChar(@sRqrdTblTypes[1]), SQL_NTS );
	// Error 'Option feature not implemented'
        // driver(MS Access) can't support catalog, schema or string pattern                
    if rcd <> SQL_SUCCESS then
      rcd := SQLTables( hStmt, nil, 0, nil, 0,
    		PChar(Pattern), SQL_NTS, PChar(@sRqrdTblTypes[1]), SQL_NTS );

    CheckHSTMT( hStmt, rcd );

	// bind the required columns
	// bind 2 - TABLE_SCHEM
    rcd := SQLBindCol( hStmt, 2, SQL_C_CHAR, SQLPOINTER(PChar(szSchName)), MaxSchNameLen+1, @iSchName );
    CheckHSTMT( hStmt, rcd );
	// bind 3 - TABLE_NAME
    rcd := SQLBindCol( hStmt, 3, SQL_C_CHAR, SQLPOINTER(PChar(szTblName)), MaxTblNameLen+1, @iTblName );
    CheckHSTMT( hStmt, rcd );
	// bind 4 - TABLE_TYPE
    rcd := SQLBindCol( hStmt, 4, SQL_C_CHAR, SQLPOINTER(PChar(szTblType)), MaxTblNameLen+1, @iTblType );
    CheckHSTMT( hStmt, rcd );
	// it's necessary to bind all columns for some(it seems old) drivers
    rcd := Calls.SQLBindCol( hStmt, 1, SQL_C_CHAR, SQLPOINTER(PChar(@szCatName)), MaxCharColLen+1, @iCatName );
    CheckHSTMT( hStmt, rcd );
    rcd := Calls.SQLBindCol( hStmt, 5, SQL_C_CHAR, SQLPOINTER(PChar(@szRemarks)), MaxCharColLen+1, @iRemarks );
    CheckHSTMT( hStmt, rcd );

	// fetch records
    rcd := SQLFetch( hStmt );
    if rcd = SQL_ERROR then
      CheckHSTMT( hStmt, rcd );
    while rcd = SQL_SUCCESS do begin
      if iSchName <> SQL_NULL_DATA then
        sSchName := Copy( szSchName, 1, iSchName );
      if iTblName <> SQL_NULL_DATA then
        sTblName := Copy( szTblName, 1, iTblName );
      if Length(Trim(sSchName)) > 0 then
        sTblName := sSchName + '.' + sTblName;
      szTblType[iTblType] := #$0;
	// if SystemTables is equal False, then do not add system tables
      if not(szTblType = 'SYSTEM TABLE') or SystemTables then
        List.Add( sTblName );
      rcd := SQLFetch( hStmt );
      if rcd = SQL_ERROR then
        CheckHSTMT( hStmt, rcd );
    end;

  finally
    if hStmt <> nil then
      OdbcFreeHandle( SQL_HANDLE_STMT, SQLHANDLE(hStmt) );
    if szSchName <> nil then
      StrDispose( szSchName );
    if szTblName <> nil then
      StrDispose( szTblName );
    if szTblType <> nil then
      StrDispose( szTblType );
  end;
end;

procedure TICustomOdbcDatabase.GetTableFieldNames(const TableName: string; List: TStrings);
const
  MaxCharColLen = 255;
var
  hStmt: SQLHSTMT;
  rcd: SQLRETURN;
  szFldName: PChar;
  MaxFldNameLen: SQLUSMALLINT;
  iFldName: SQLINTEGER;
  szCatName, szSchName, szTblName: array[0..MaxCharColLen] of Char;
  iCatName, iSchName, iTblName: SQLINTEGER;
  sSchName, sTblName: string;
  i: Integer;
begin
  hStmt := nil;
  szFldName := nil;
  List.Clear;

  with Calls do try
  	// get max length for a column name
    MaxFldNameLen := OdbcGetMaxFieldNameLen;
    if MaxFldNameLen <= 0 then
      MaxFldNameLen := 128;
    szFldName := StrAlloc( MaxFldNameLen + 1 );
	// allocate a statement handle
    rcd := OdbcAllocHandle( SQL_HANDLE_STMT, SQLHANDLE(DbcHandle), @hStmt );
    CheckHSTMT( hStmt, rcd );
    	// get required result set
    sTblName := TableName;
    i := AnsiPos('.', TableName);
    if i = 0 then
      sSchName := SQL_ALL_SCHEMAS
    else begin
      sSchName := Copy(TableName, 1, i-1);
      sTblName := Copy(TableName, i+1, Length(TableName)-i);
    end;
    rcd := SQLColumns( hStmt, nil, 0, PChar(sSchName), SQL_NTS, PChar(sTblName), SQL_NTS, nil, 0 );
	// Error 'Option feature not implemented'
        // driver(MS Access) does not support catalog, schema or string pattern
    if rcd <> SQL_SUCCESS then
      rcd := SQLColumns( hStmt, nil, 0, nil, 0,	PChar(sTblName), SQL_NTS, nil, 0 );

    CheckHSTMT( hStmt, rcd );

	// bind the required columns
	// bind 4 - COLUMN_NAME
    rcd := SQLBindCol( hStmt, 4, SQL_C_CHAR, SQLPOINTER(PChar(szFldName)), MaxFldNameLen+1, @iFldName );
    CheckHSTMT( hStmt, rcd );
	// it's necessary to bind all(or almost all) columns for some(it seems old) drivers
    rcd := Calls.SQLBindCol( hStmt, 1, SQL_C_CHAR, SQLPOINTER(PChar(@szCatName)), MaxCharColLen+1, @iCatName );
    CheckHSTMT( hStmt, rcd );
    rcd := Calls.SQLBindCol( hStmt, 2, SQL_C_CHAR, SQLPOINTER(PChar(@szSchName)), MaxCharColLen+1, @iSchName );
    CheckHSTMT( hStmt, rcd );
    rcd := Calls.SQLBindCol( hStmt, 3, SQL_C_CHAR, SQLPOINTER(PChar(@szTblName)), MaxCharColLen+1, @iTblName );
    CheckHSTMT( hStmt, rcd );

	// fetch records
    rcd := SQLFetch( hStmt );
    if rcd = SQL_ERROR then
      CheckHSTMT( hStmt, rcd );
    while rcd = SQL_SUCCESS do begin
      if iFldName <> SQL_NULL_DATA then
        List.Add( Copy( szFldName, 1, iFldName ) );

      rcd := SQLFetch( hStmt );
      if rcd = SQL_ERROR then
        CheckHSTMT( hStmt, rcd );
    end;

  finally
    if hStmt <> nil then
      OdbcFreeHandle( SQL_HANDLE_STMT, SQLHANDLE(hStmt) );
    if szFldName <> nil then
      StrDispose( szFldName );
  end;
end;


{ TICustomOdbcDataSet }
constructor TICustomOdbcDataSet.Create(ADataSet: TSDDataSet);
begin
  inherited Create(ADataSet);

  FHandle := nil;
  FStmt	:= '';
  FNextResults := False;
  FInfoExecuted:= False;

  FIsSingleConn := SrvDatabase.IsSingleConn;
end;

destructor TICustomOdbcDataSet.Destroy;
begin
  Disconnect(False);

  inherited;
end;

function TICustomOdbcDataSet.GetCalls: TOdbcFunctions;
begin
  Result := SrvDatabase.FCalls;
end;

function TICustomOdbcDataSet.GetSrvDatabase: TICustomOdbcDatabase;
begin
  Result := (inherited SrvDatabase) as TICustomOdbcDatabase;
end;

function TICustomOdbcDataSet.GetHandle: PSDCursor;
begin
  Result := FHandle;
end;

{ Marks a database handle as used by the current dataset }
procedure TICustomOdbcDataSet.AcquireDBHandle;
begin
  if FIsSingleConn then
    SrvDatabase.ReleaseDBHandle(DataSet, True);
end;

{ Releases a database handle, which was used by the current dataset }
procedure TICustomOdbcDataSet.ReleaseDBHandle;
begin
  if SrvDatabase.CurrDataSet = DataSet then
    SrvDatabase.ReleaseDBHandle(nil, False);
end;

procedure TICustomOdbcDataSet.Check(Status: TSDEResult);
begin
  SrvDatabase.CheckHSTMT( FHandle, Status );
end;

procedure TICustomOdbcDataSet.Connect;
var
  sValue: string;
  V: SQLUINTEGER;
begin
  try
    Check(
    	SrvDatabase.OdbcAllocHandle( SQL_HANDLE_STMT, SQLHANDLE(SrvDatabase.DbcHandle), @FHandle )
        );

    Check(
  	SrvDatabase.OdbcSetStmtAttr( FHandle, SQL_ATTR_CURSOR_TYPE,
        	SQLPOINTER( SQL_CURSOR_FORWARD_ONLY ), 0 )
        );

	// default value for SQL_ATTR_MAX_ROWS is zero: the driver returs all rows
    Check( SrvDatabase.OdbcSetStmtAttr( FHandle, SQL_ATTR_MAX_ROWS, SQLPOINTER( 0 ), 0 ) );

	// set command timeout value, if it's set
    sValue := Trim( SrvDatabase.Database.Params.Values[szCMDTIMEOUT] );
    if sValue <> '' then begin
      V := StrToIntDef( sValue, 0 );
      	// Set timeout to the number of seconds to wait for an SQL statement to execute before returning to the application
      SrvDatabase.OdbcSetStmtAttr( FHandle, SQL_ATTR_QUERY_TIMEOUT, SQLPOINTER( V ), SQL_IS_UINTEGER );
    end;

  except
    if FHandle <> nil then
      SrvDatabase.OdbcFreeHandle( SQL_HANDLE_STMT, SQLHANDLE(FHandle) );
    FHandle := nil;

    raise;
  end;
end;

procedure TICustomOdbcDataSet.Disconnect(Force: Boolean);
begin
  if FSelectBuffer <> nil then
    FreeSelectBuffer;

  ReleaseDBHandle;
      
  if FHandle = nil then Exit;

  if FHandle <> nil then begin
    Calls.SQLFreeStmt( FHandle, SQL_CLOSE );
    SrvDatabase.OdbcFreeHandle( SQL_HANDLE_STMT, SQLHANDLE(FHandle) );
  end;
  FHandle := nil;
end;

procedure TICustomOdbcDataSet.CloseResultSet;
begin
  if FNextResults then
    Exit;

  FStmt := '';

  if FHandle <> nil then
    Calls.SQLFreeStmt( FHandle, SQL_CLOSE );
    
	// it's need in case of close a result set without full fetch
  ReleaseDBHandle;
end;

procedure TICustomOdbcDataSet.CheckPrepared;
begin
  if FHandle <> nil then
    Exit;

  if (DataSet is TSDQuery) and TSDQuery(DataSet).Prepared then
    QPrepareSQL( PChar(TSDQuery(DataSet).SQL.Text) )
  else if (DataSet is TSDStoredProc) and TSDStoredProc(DataSet).Prepared then
    SpPrepareProc;
end;

function TICustomOdbcDataSet.ResultSetExists: Boolean;
var
  ColNum: SQLSMALLINT;
begin
  if DataSet is TSDQuery then begin
    AcquireDBHandle;
    CheckPrepared;
    
    Check( Calls.SQLNumResultCols( Handle, ColNum ) );
    Result := ColNum > 0;
    if not Result and IsCallStmt then
      Result := True;
  end else
    Result := True;
end;

procedure TICustomOdbcDataSet.GetFieldDescs(Descs: TSDFieldDescList);
const
  MaxColName	= 255;
var
  pFieldDesc: PSDFieldDesc;
  ft: TFieldType;
  Col, NumCols: SQLSMALLINT;
  szColName: PChar;
  ColNameLen, ColType, ColScale, ColNullable: SQLSMALLINT;
  ColSize: SQLUINTEGER;
begin
  CheckPrepared;

  Check( Calls.SQLNumResultCols( Handle, NumCols ) );
  if (NumCols = 0) and IsCallStmt then begin
    if DataSet is TSDQuery then
      InternalQExecute(True)
    else
      InternalSpExecute(True);
    Check( Calls.SQLNumResultCols( Handle, NumCols ) );
  end;
  szColName := StrAlloc( MaxColName );
  try
    for Col:=1 to NumCols do begin

      Check( Calls.SQLDescribeCol( Handle, Col, szColName, MaxColName, ColNameLen,
    		ColType, ColSize, ColScale, ColNullable )
      );
      szColName[ColNameLen] := #0;

      ft := SrvDatabase.FieldDataType(ColType);
	// That's applied for a column with size more then 255 (for example: char(30000) or varbinary(10000))
      if (ft = ftString) and (ColSize >= MinBlobSize) then
        ft := ftMemo;
      if (ft in [ftBytes, ftVarBytes]) and (ColSize >= MinBlobSize) then
        ft := ftBlob;

      if ft = ftUnknown then
        DatabaseErrorFmt( SBadFieldType, [szColName] );

      New( pFieldDesc );
      with pFieldDesc^ do begin
        FieldName	:= szColName;
        FieldNo		:= Col;
        DataType	:= ft;
        if IsRequiredSizeTypes( ft )
        then Size	:= ColSize
        else Size	:= 0;
        Precision	:= ColScale;
    	// if NullOk = 0 then null values are not permitted for the column (Required = True)
        Required	:= ColNullable = SQL_NO_NULLS;
      end;

      Descs.Add( pFieldDesc );

    end;
  finally
    StrDispose( szColName );
  end;
end;

// Setting buffers for data, which selected from database
procedure TICustomOdbcDataSet.SetSelectBuffer;
var
  i, nOffset: Integer;
  BindDataSize: SQLINTEGER;
  BindDataType: SQLSMALLINT;
  BindDataBuffer: PChar;
  TmpField: TField;
begin
  nOffset := 0;		// pointer to the TFieldInfo

  for i:=0 to DataSet.FieldDefs.Count-1 do begin
    TmpField := FieldByNumber(DataSet.FieldDefs[i].FieldNo);
    if Assigned(TmpField) then with TmpField do begin
    	// skips calculated fields
      if Calculated then Continue;

      BindDataType := SrvDatabase.NativeDataType(DataType);
      if BindDataType = 0 then
        DatabaseErrorFmt( SUnknownFieldType, [FieldName] );

      BindDataSize := NativeFieldDataSize(TmpField);
      BindDataBuffer := PChar( Integer(FSelectBuffer) + nOffset + SizeOf(TFieldInfo) );

      if not IsBlobType(DataType) then
        Check( Calls.SQLBindCol( Handle, FieldNo, BindDataType,
        	SQLPOINTER(BindDataBuffer), BindDataSize,
                @PFieldInfo( Integer(BindDataBuffer) - SizeOf(TFieldInfo) )^.DataSize )
        );

      Inc(nOffset, SizeOf(TFieldInfo) + BindDataSize);
    end;
  end;
end;

procedure TICustomOdbcDataSet.Execute;
begin
  AcquireDBHandle;
  CheckPrepared;  
  
  if DataSet is TSDQuery then
    QExecute
  else if DataSet is TSDStoredProc then
    SpExecute
  else
    Check( Calls.SQLExecute( Handle ) );
end;

function TICustomOdbcDataSet.FetchNextRow: Boolean;
var
  rcd: SQLRETURN;
begin
  Result := False;
  rcd := Calls.SQLFetch( Handle );
  SrvDatabase.ResetIdleTimeOut;  
  if (rcd = SQL_SUCCESS) or (rcd = SQL_SUCCESS_WITH_INFO) or (rcd = SQL_NO_DATA_FOUND) then
    Result := rcd <> SQL_NO_DATA_FOUND
  else
    Check( rcd );

  if Result then begin
    if BlobFieldCount > 0 then
      FetchBlobFields;
  end else
    ReleaseDBHandle;
end;

function TICustomOdbcDataSet.CnvtDateTime2DBDateTime(ADataType: TFieldType; Value: TDateTime; Buffer: Pointer; BufSize: Integer): Integer;
var
  Year, Month, Day, Hour, Min, Sec, MSec: Word;
begin
  Result := 0;
  
  if SrvDatabase.NativeDataSize(ADataType) > BufSize then
    DatabaseError(SInsufficientIDateTimeBufSize);
	// it's necessary for ftDate
  Hour	:= 0;
  Min	:= 0;
  Sec	:= 0;

  if ADataType in [ftTime, ftDateTime] then
    DecodeTime(Value, Hour, Min, Sec, MSec);
  if ADataType in [ftDate, ftDateTime] then
    DecodeDate(Value, Year, Month, Day);

  case ADataType of
    ftTime:
      begin
	TTIME_STRUCT(Pointer(Buffer)^).hour	:= Hour;
	TTIME_STRUCT(Pointer(Buffer)^).minute	:= Min;
        TTIME_STRUCT(Pointer(Buffer)^).second	:= Sec;
        Result := SizeOf(TTIME_STRUCT);
      end;
    ftDate:
      begin
	TDATE_STRUCT(Pointer(Buffer)^).year	:= Year;
	TDATE_STRUCT(Pointer(Buffer)^).month	:= Month;
        TDATE_STRUCT(Pointer(Buffer)^).day	:= Day;
        Result := SizeOf(TDATE_STRUCT);
      end;
    ftDateTime:
      begin
	TTIMESTAMP_STRUCT(Pointer(Buffer)^).hour	:= Hour;
	TTIMESTAMP_STRUCT(Pointer(Buffer)^).minute	:= Min;
        TTIMESTAMP_STRUCT(Pointer(Buffer)^).second	:= Sec;
        TTIMESTAMP_STRUCT(Pointer(Buffer)^).fraction	:= MSec*1000*1000;
	TTIMESTAMP_STRUCT(Pointer(Buffer)^).year	:= Year;
	TTIMESTAMP_STRUCT(Pointer(Buffer)^).month	:= Month;
        TTIMESTAMP_STRUCT(Pointer(Buffer)^).day		:= Day;
        Result := SizeOf(TTIMESTAMP_STRUCT);
      end
  end;
end;

function TICustomOdbcDataSet.CnvtDBDateTime2DateTimeRec(ADataType: TFieldType; Buffer: PChar; BufSize: Integer): TDateTimeRec;
var
  dtDate, dtTime: TDateTime;
begin
  case ADataType of
    ftTime:
      begin
        dtTime := EncodeTime(
    			TTIME_STRUCT(Pointer(Buffer)^).hour,
    			TTIME_STRUCT(Pointer(Buffer)^).minute,
                        TTIME_STRUCT(Pointer(Buffer)^).second, 0 );
        Result.Time := DateTimeToTimeStamp(dtTime).Time;
      end;
    ftDate:
      begin
        dtDate := EncodeDate(
    			TDATE_STRUCT(Pointer(Buffer)^).year,
    			TDATE_STRUCT(Pointer(Buffer)^).month,
                        TDATE_STRUCT(Pointer(Buffer)^).day );
        Result.Date := DateTimeToTimeStamp(dtDate).Date;
      end;
    ftDateTime:
      begin
        dtTime := EncodeTime(
    			TTIMESTAMP_STRUCT(Pointer(Buffer)^).hour,
    			TTIMESTAMP_STRUCT(Pointer(Buffer)^).minute,
                        TTIMESTAMP_STRUCT(Pointer(Buffer)^).second, 0 );
    	dtDate := EncodeDate(
    			TTIMESTAMP_STRUCT(Pointer(Buffer)^).year,
    			TTIMESTAMP_STRUCT(Pointer(Buffer)^).month,
                        TTIMESTAMP_STRUCT(Pointer(Buffer)^).day );
        Result.DateTime := TimeStampToMSecs( DateTimeToTimeStamp(dtDate + dtTime) ) +
        			TTIMESTAMP_STRUCT(Pointer(Buffer)^).fraction div 1000000;	// plus milliseconds
      end
  else
    Result.DateTime := 0.0;
  end;
end;

function TICustomOdbcDataSet.CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean;
var
  InData, OutData: Pointer;
  dtDateTime: TDateTimeRec;
begin
  if PFieldInfo(InBuf)^.DataSize = SQL_NULL_DATA then
    PFieldInfo(OutBuf)^.FetchStatus := indNULL
  else
    PFieldInfo(OutBuf)^.FetchStatus := indVALUE;

  if PFieldInfo(OutBuf)^.FetchStatus <> indNULL then begin
    InData	:= Pointer( Integer(InBuf) + SizeOf(TFieldInfo) );
    OutData	:= Pointer( Integer(OutBuf) + SizeOf(TFieldInfo) );
    	// DateTime fields
    if SrvDatabase.RequiredCnvtFieldType(AField.DataType) then begin
      if AField is TDateTimeField then begin
        dtDateTime := CnvtDBDateTime2DateTimeRec(AField.DataType, InData, PFieldInfo(InBuf)^.DataSize);
        TDateTimeRec(OutData^) := dtDateTime;
        PFieldInfo(OutBuf)^.DataSize := SizeOf(TDateTimeRec);
      end;
    end else begin
      if AField.DataType = ftString then begin
        Move( InData^, OutData^, PFieldInfo(InBuf)^.DataSize + 1 );
        if SrvDatabase.IsRTrimChar then
          StrRTrim( OutData );
        PFieldInfo(OutBuf)^.DataSize := StrLen( OutData );
      end else begin
        Move( InData^, OutData^, PFieldInfo(InBuf)^.DataSize );
        PFieldInfo(OutBuf)^.DataSize := PFieldInfo(InBuf)^.DataSize;
      end;
    end;
  end;
  Result := True;
end;

function TICustomOdbcDataSet.ReadBlob(FieldNo: Integer; var BlobData: string): Longint;
  function IsLongCharType(AFieldType: TFieldType): Boolean;
  begin
    Result := AFieldType in [ftMemo, ftFmtMemo];
  end;
var
  ft: TFieldType;
  CurPtr: PChar;			// pointer to current buffer
  BlobSize, Len: SQLINTEGER;
  rcd: SQLRETURN;
begin
  Result := 0;
  ft := FieldByNumber(FieldNo).DataType;
	// get data size
  Len := 0;
  BlobSize := 0;
  	// it's necessary for zero at the end of char data
  if IsLongCharType(ft) then
    Inc(BlobSize);
  if BlobSize > 0 then
    SetLength(BlobData, BlobSize)
  else
    SetLength(BlobData, 1);
  CurPtr := @BlobData[1];	// CurPtr must have a valid pointer
  rcd := Calls.SQLGetData( Handle, FieldNo,
    		SrvDatabase.NativeDataType( ft ),
                CurPtr, BlobSize, @Len );
  if (rcd <> SQL_SUCCESS) and (rcd <> SQL_SUCCESS_WITH_INFO) and
     (rcd <> SQL_NO_DATA)
  then
    Check( rcd );
  if Len <= 0 then begin	// SQL_NO_TOTAL value is not processed now (if a server can not define data size)
    SetLength(BlobData, 0);
    Exit;
  end;
	// get data
  BlobSize := Len;
  if IsLongCharType(ft) then
    Inc(BlobSize);
  SetLength(BlobData, BlobSize);
  CurPtr := @BlobData[1];
  rcd := Calls.SQLGetData( Handle, FieldNo,
    		SrvDatabase.NativeDataType( ft ),
                CurPtr, BlobSize, @Len );
  if (rcd <> SQL_SUCCESS) and (rcd <> SQL_SUCCESS_WITH_INFO) and
     (rcd <> SQL_NO_DATA)
  then
    Check( rcd );

	// set(shrink) really Blob size - without null-terminator for text fields
  if IsLongCharType(ft) then begin
    Dec(BlobSize);
    SetLength(BlobData, BlobSize);
  end;
  Result := BlobSize;
end;

function TICustomOdbcDataSet.WriteBlob(FieldNo: Integer; const Buffer; Count: LongInt): Longint;
var
  Len: Word;
  Buf: PChar;
begin
  Result := 0;

  Buf := @Buffer;
  while Count > 0 do begin
    if Count > $7FFF
    then Len := $7FFF
    else Len := Count;
    Check( Calls.SQLPutData( Handle, Buf, Len) );
    Inc(Result, Len);
    Dec(Count, Len);
    Buf := PChar(Buf + Len);
  end;
end;

		{ Query methods }

procedure TICustomOdbcDataSet.QBindParams;
const
  MaxBlobSize = MaxInt;	// 2147483647
var
  CurPtr, DataPtr: Pointer;
  i: Integer;
  DataLen, ColSize: SQLINTEGER;
begin
  if FBindBuffer = nil then begin
    if Query.ParamCount = 0 then
      Exit
    else
      AllocBindBuffer;
  end;
  CurPtr := FBindBuffer;

  CheckPrepared;

  for i:=0 to Query.ParamCount-1 do
    with Query.Params[i] do begin
      TFieldInfo(CurPtr^).DataSize := 0;
      DataLen := QParamBindSize( Query.Params[i] );
      DataPtr := Pointer(Integer(CurPtr) + SizeOf(TFieldInfo));

      if IsNull then
        TFieldInfo(CurPtr^).FetchStatus := SQL_NULL_DATA
      else
        if DataType = ftString then
          TFieldInfo(CurPtr^).FetchStatus := SQL_NTS
        else
          TFieldInfo(CurPtr^).FetchStatus := DataLen;

      case DataType of
        ftString:
          if DataLen > 0 then StrMove(DataPtr, PChar(AsString), DataLen);
        ftBytes,
        ftVarBytes:
          if DataLen > 0 then begin
            FillChar( DataPtr^, DataLen, 0 );
            Query.Params[i].GetData( DataPtr );
          end;
        ftInteger:
          if DataLen > 0 then DWORD(DataPtr^) := AsInteger;
        ftSmallInt:
          if DataLen > 0 then WORD(DataPtr^) := AsInteger;
        ftDate, ftTime, ftDateTime:
          if DataLen > 0 then CnvtDateTime2DBDateTime(DataType, AsDateTime, DataPtr, SrvDatabase.NativeDataSize(DataType));
        ftFloat:
          if DataLen > 0 then Double(DataPtr^) := AsFloat;
        else
          if not IsSupportedBlobTypes(DataType) then
            raise EDatabaseError.CreateFmt(SNoParameterDataType, [Name]);
      end;

      if IsBlobType(DataType) then begin
        if TVarData(Value).VType <> varString then
          VarAsType(Value, varString);
        if PChar(TVarData(Value).VString) = nil then
          TFieldInfo(CurPtr^).FetchStatus := SQL_NULL_DATA
        else
          if SrvDatabase.IsNeedLongDataLen
          then TFieldInfo(CurPtr^).FetchStatus := SQL_LEN_DATA_AT_EXEC( Length(AsBlob) )
          else TFieldInfo(CurPtr^).FetchStatus := SQL_DATA_AT_EXEC;

        Check( Calls.SQLBindParameter( Handle, i+1, SQL_PARAM_INPUT,
      			SrvDatabase.NativeDataType(DataType),
      			SrvDatabase.SqlDataType(DataType),
      			MaxBlobSize, 0, PChar(i), 0,
                        @TFieldInfo(CurPtr^).FetchStatus ) );
      end else begin
        ColSize := DataLen;
        if DataType in [ftDateTime] then begin
		// max display string size, which can differs from DataLen, for example, for date/time columns
          if not Assigned( @Calls.SQLDescribeParam ) or
             (Calls.SQLDescribeParam(Handle, i+1, nil, @ColSize, nil, nil) <> SQL_SUCCESS)
          then	// in case of error, for example: <Invalid descriptor index> when datetime parameter is not the first parameter
            ColSize := Length( DateTimeToStr(AsDateTime) );
        end;
        Check( Calls.SQLBindParameter( Handle, i+1, SQL_PARAM_INPUT,
      			SrvDatabase.NativeDataType(DataType),
      			SrvDatabase.SqlDataType(DataType),
      			ColSize, 0, DataPtr, DataLen,
                        @TFieldInfo(CurPtr^).FetchStatus ) );
      end;

      Inc( Integer(CurPtr), SizeOf(TFieldInfo) + DataLen );
    end;
end;

{ Check whether the statement contains CALL procedure
 Some ODBC function returns info only after SQLExecute call, for example:
 SQLGetDiagField (for SQL_DIAG_DYNAMIC_FUNCTION),
 SQLNumResultCols for DB2 (not for MSSQL)) }
function TICustomOdbcDataSet.GetIsCallStmt: Boolean;
var
  i: Integer;
  fc: SQLINTEGER;
begin
  if Assigned(Calls.SQLGetDiagField) and
     (Calls.SQLGetDiagField( SQL_HANDLE_STMT, Handle, 1,
    		SQL_DIAG_DYNAMIC_FUNCTION_CODE, @fc, SQL_IS_INTEGER, nil ) = SQL_SUCCESS)
  then
    Result := fc = SQL_DIAG_CALL
  else begin
    i := AnsiPos( 'CALL ', AnsiUpperCase(FStmt) );
    Result := (i = 1) or ( (i > 1) and (FStmt[i-1] in [#$20, '{', '=']) );
  end;
end;

function TICustomOdbcDataSet.GetIsExecDirect: Boolean;
begin
  Result := not(dsfPrepared in DSFlags) and (dsfExecSQL in DSFlags);
end;

procedure TICustomOdbcDataSet.QPrepareSQL(Value: PChar);

  function ChangeParamMarkers( OldStmt: PChar ): string;
  var
    i, ParamPos: Integer;
    sFullParamName: string;
  begin
    Result := OldStmt;
    with Query do
      for i:=0 to Query.ParamCount-1 do begin
        sFullParamName := ParamPrefix + Params[i].Name;
        ParamPos := AnsiTextPos( sFullParamName, Result );
        if ParamPos = 0 then
          Continue;
      	// remove parameter name with prefix(':')
        System.Delete( Result, ParamPos, Length(sFullParamName) );
      	// set parameter marker
        System.Insert( ParamMarker, Result, ParamPos );
    end;
  end;

begin
  FStmt := ChangeParamMarkers( Value );

  if FHandle = nil then Connect;

  if not IsExecDirect then
    Check( Calls.SQLPrepare( FHandle, PChar(FStmt), SQL_NTS ) );
end;

procedure TICustomOdbcDataSet.QExecute;
begin
  InternalQExecute(False);
  
	// if procedure was called, it's possible to describe a result set after SQLExecute only
  if (dsfOpened in DSFlags) and IsCallStmt then
    InitResultSet;
end;

procedure TICustomOdbcDataSet.InternalQExecute(InfoQuery: Boolean);
var
  rcd: SQLRETURN;
  pValue: SQLPOINTER;
  sBlob: string;
begin
  FNoDataExecuted := False;

  if InfoQuery then
    QBindParams;

  if not FInfoExecuted then begin
    if not IsExecDirect then
      rcd := Calls.SQLExecute( Handle )
    else
      rcd := Calls.SQLExecDirect( Handle, PChar(FStmt), SQL_NTS );
    if rcd = SQL_NEED_DATA then begin
      rcd := Calls.SQLParamData( Handle, @pValue );
      while rcd = SQL_NEED_DATA do
        with Query.Params[Integer(pValue)] do begin
          sBlob := AsBlob;
          WriteBlob( Integer(pValue), PChar(sBlob)^, Length(sBlob) );

          rcd := Calls.SQLParamData( Handle, @pValue );
        end;
      Check( rcd );
    end else
      Check( rcd );

	// nothing was updated/deleted
    FNoDataExecuted := rcd = SQL_NO_DATA;
  end;

  FInfoExecuted := InfoQuery;
end;

function TICustomOdbcDataSet.QGetRowsAffected: Integer;
begin
  if FNoDataExecuted then
    Result := 0
  else
    Check( Calls.SQLRowCount( Handle, Result ) );
end;

	{ StoredProc methods }

procedure TICustomOdbcDataSet.SpBindParams;
var
  CurPtr, DataPtr: Pointer;
  i: Integer;
  DataLen: SQLINTEGER;
  InputOutputType: SQLSMALLINT;
begin
  if FBindBuffer = nil then Exit;
  CurPtr := FBindBuffer;

  CheckPrepared;

  for i:=0 to StoredProc.ParamCount-1 do
    with StoredProc.Params[i] do begin
      TFieldInfo(CurPtr^).DataSize := 0;
      DataLen := SpParamBindSize( StoredProc.Params[i] );
      DataPtr := Pointer(Integer(CurPtr) + SizeOf(TFieldInfo));

      if IsNull then
        TFieldInfo(CurPtr^).FetchStatus := SQL_NULL_DATA
      else
        if DataType = ftString then
          TFieldInfo(CurPtr^).FetchStatus := SQL_NTS
        else
          TFieldInfo(CurPtr^).FetchStatus := DataLen;

      case ParamType of
        ptInput:
          InputOutputType := SQL_PARAM_INPUT;
        ptInputOutput:
          InputOutputType := SQL_PARAM_INPUT_OUTPUT;
        ptOutput,
        ptResult:
          InputOutputType := SQL_PARAM_OUTPUT;
      else
        InputOutputType := SQL_PARAM_TYPE_UNKNOWN;
      end;

      case DataType of
        ftString:
          if not IsNull then begin
          	// DataLen is always equal 255(including '\0'), but string length may be less or more then 255
            if DataLen > (Length(AsString) + 1)
            then StrMove(DataPtr, PChar(AsString), Length(AsString)+1)
            else StrMove(DataPtr, PChar(AsString), DataLen);
            PChar(DataPtr)[DataLen-1] := #$0;	// DataLen is buffer size including zero-terminator
          end;
        ftInteger:
          if not IsNull then DWORD(DataPtr^) := AsInteger;
        ftSmallInt:
          if not IsNull then WORD(DataPtr^) := AsInteger;
        ftDate, ftTime, ftDateTime:
          if not IsNull then CnvtDateTime2DBDateTime(DataType, AsDateTime, DataPtr, SrvDatabase.NativeDataSize(DataType));
        ftFloat:
          if not IsNull then Double(DataPtr^) := AsFloat;
        else
          if not IsSupportedBlobTypes(DataType) then
            raise EDatabaseError.CreateFmt(SNoParameterDataType, [Name]);
      end;

      if IsBlobType(DataType) then begin
        if TVarData(Value).VType <> varString then
          VarAsType(Value, varString);
        if PChar(TVarData(Value).VString) = nil then
          TFieldInfo(CurPtr^).FetchStatus := SQL_NULL_DATA
        else
          TFieldInfo(CurPtr^).FetchStatus := SQL_DATA_AT_EXEC;

        Check( Calls.SQLBindParameter( Handle, i+1, InputOutputType,
      			SrvDatabase.NativeDataType(DataType),
      			SrvDatabase.SqlDataType(DataType),
      			0, 0, PChar(i),
                        0,
                        @TFieldInfo(CurPtr^).FetchStatus ) );
      end else begin
        Check( Calls.SQLBindParameter( Handle, i+1, InputOutputType,
      			SrvDatabase.NativeDataType(DataType),
      			SrvDatabase.SqlDataType(DataType),
      			DataLen, 0, DataPtr, DataLen,
                        @TFieldInfo(CurPtr^).FetchStatus ) );
      end;

      Inc( Integer(CurPtr), SizeOf(TFieldInfo) + DataLen );
    end;
end;

procedure TICustomOdbcDataSet.SpCreateParamDesc;
const
  MaxColName	= 128;
  MaxCharColLen = 255;
var
  hStmt: SQLHSTMT;
  rcd: SQLRETURN;
  i: Integer;
  szColName: PChar;
  ColType, ColDataType: SQLSMALLINT;
  iColName, iColType, iColDataType: SQLINTEGER;
  ft: TFieldType;
  pt: TSDHelperParamType;
  sParamName, sPrcName, sSchName: string;
  szCatName, szSchName, szPrcName: array[0..MaxCharColLen] of Char;
  iCatName, iSchName, iPrcName: SQLINTEGER;
begin
  hStmt := nil;
  pt := ptUnknown;
  szColName := StrAlloc( MaxColName + 1 );
  with SrvDatabase, Calls do try
    rcd := OdbcAllocHandle( SQL_HANDLE_STMT, SQLHANDLE(SrvDatabase.DbcHandle), @hStmt );
    CheckHSTMT( hStmt, rcd );
    	// get required result set
    sPrcName := StoredProc.StoredProcName;
    i := AnsiPos('.', sPrcName);
    if (i > 0) and (i < Length(sPrcName)) then begin
      sSchName := Copy(sPrcName, 1, i-1);	// get owner name
      sPrcName := Copy(sPrcName, i+1, Length(sPrcName)-i);
    end else
      sSchName := SQL_ALL_SCHEMAS;
{ !!! NOTE: if szColumnName = '', then Informix v.2.80 returns only columns with empty name }
    rcd := SQLProcedureColumns( hStmt, nil, 0, PChar(sSchName), SQL_NTS,
    		PChar(sPrcName), SQL_NTS, nil, SQL_NTS );

    CheckHSTMT( hStmt, rcd );
	// bind the required columns
	// bind 4 - COLUMN_NAME
    rcd := SQLBindCol( hStmt, 4, SQL_C_CHAR, SQLPOINTER(PChar(szColName)), MaxColName+1, @iColName );
    CheckHSTMT( hStmt, rcd );
    	// bind 5 - COLUMN_TYPE
    rcd := SQLBindCol( hStmt, 5, SQL_C_SSHORT, @ColType, 0, @iColType);
    CheckHSTMT( hStmt, rcd );
	// bind 6 - DATA_TYPE
    rcd := SQLBindCol( hStmt, 6, SQL_C_SSHORT, @ColDataType, 0, @iColDataType);
    CheckHSTMT( hStmt, rcd );
	// it's necessary to bind all(or almost all) columns for some(it seems old) drivers
    rcd := Calls.SQLBindCol( hStmt, 1, SQL_C_CHAR, SQLPOINTER(PChar(@szCatName)), MaxCharColLen+1, @iCatName );
    CheckHSTMT( hStmt, rcd );
    rcd := Calls.SQLBindCol( hStmt, 2, SQL_C_CHAR, SQLPOINTER(PChar(@szSchName)), MaxCharColLen+1, @iSchName );
    CheckHSTMT( hStmt, rcd );
    rcd := Calls.SQLBindCol( hStmt, 3, SQL_C_CHAR, SQLPOINTER(PChar(@szPrcName)), MaxCharColLen+1, @iPrcName );
    CheckHSTMT( hStmt, rcd );

    repeat
	// fetch
      rcd := SQLFetch( hStmt );
      if rcd = SQL_ERROR then
        CheckHSTMT( hStmt, rcd )
      else if rcd = SQL_NO_DATA then
        Break;

      if iColName <> SQL_NULL_DATA
      then sParamName := Copy( szColName, 1, iColName )
      else sParamName := '';
      ft := SrvDatabase.FieldDataType( ColDataType );
      if ft = ftUnknown then
        DatabaseErrorFmt( SBadFieldType, [sParamName] );
      case ColType of
        SQL_PARAM_INPUT:
          pt := ptInput;
        SQL_PARAM_INPUT_OUTPUT:
          pt := ptInputOutput;
        SQL_PARAM_OUTPUT:
          pt := ptOutput;
        SQL_RESULT_COL:
          Continue;		// it is field of a result set (not parameter)
        SQL_RETURN_VALUE:
          pt := ptResult; 	// this is not returned
      else
        DatabaseErrorFmt( SBadParameterType, [sParamName] );
      end;
      if ((iColName = 0) or (iColName = SQL_NULL_DATA)) and (pt = ptOutput) then
        pt := ptResult;
      if pt = ptResult then
        sParamName := SResultName;

      SpAddParam( sParamName, ft, pt );
    until rcd = SQL_NO_DATA;

  finally
    if szColName <> nil then
      StrDispose( szColName );
    if hStmt <> nil then
      OdbcFreeHandle( SQL_HANDLE_STMT, SQLHANDLE(hStmt) );
  end;
end;

procedure TICustomOdbcDataSet.SpPrepareProc;
var
  i: Integer;
  sParams, sResult: string;
begin
  if FHandle = nil then Connect;

  with StoredProc do begin
    if Params.Count = 0 then
      SpCreateParamDesc;

    sParams := '';
    sResult := '';
    for i:=0 to Params.Count-1 do begin
      if Params[i].ParamType in [ptResult] then
        sResult := ParamMarker + '=';
      if not (Params[i].ParamType in [ptInput, ptInputOutput, ptOutput]) then
        Continue;

      if Length(sParams) > 0 then
        sParams := sParams + ', ';
      sParams := sParams + ParamMarker;
    end;

    FStmt := Format( '{%sCALL %s(%s)}', [sResult, LowerCase( StoredProcName ), sParams] );
  end;

  Check( Calls.SQLPrepare( FHandle, PChar(FStmt), SQL_NTS ) );
end;

{ InfoQuery is True, when GetFieldDescs is called.
 It's necessary to avoid to call SQLExecute more once.
 InternalSpExecute can be called before SpExecute(Open) or after(ExecProc) }
procedure TICustomOdbcDataSet.InternalSpExecute(InfoQuery: Boolean);
begin
  if InfoQuery then
    SpBindParams;
  if not FInfoExecuted then
    Check( Calls.SQLExecute( Handle ) );
  FInfoExecuted := InfoQuery;
end;

procedure TICustomOdbcDataSet.SpExecute;
begin
  if not FNextResults then begin
    InternalSpExecute(False);
    SpGetParams;
        // to avoid call SQLExecute twice in InitResultSet
    FInfoExecuted := True;
  end;

  try
	// field description is accessible only after execute
    InitResultSet;
  finally
    FInfoExecuted := False;
  end;
end;

procedure TICustomOdbcDataSet.SpExecProc;
begin
  SpBindParams;
  SpExecute;
end;

{
NOTE from ODBC Reference:
  For some drivers, output parameters and return values are not available
until all result sets and row counts have been processed. For such drivers,
output parameters and return values become available when SQLMoreResults returns SQL_NO_DATA.
}
procedure TICustomOdbcDataSet.SpGetResults;
begin
  DataSet.FetchAll;
  SpGetParams;
end;

procedure TICustomOdbcDataSet.SpGetParams;
var
  I: Integer;
  FldBuf, OutBuf, OutData: PChar;
  TmpField: TField;
  FieldCls: TFieldClass;
begin
  if FBindBuffer = nil then Exit;
	// Alloc buffer for parameter of max size
  OutBuf := StrAlloc(255 + SizeOf(TFieldInfo));

  try
    FldBuf := FBindBuffer;
    for I := 0 to StoredProc.ParamCount - 1 do with StoredProc.Params[I] do begin
      if ParamType in [ptResult, ptInputOutput, ptOutput] then begin
        if PFieldInfo(FldBuf)^.FetchStatus <> SQL_NULL_DATA then
          PFieldInfo(FldBuf)^.DataSize := SpParamBindSize(StoredProc.Params[I]);
          // DataSize has to contain length w/o null-terminator in in CnvtDBField2Field method
        if DataType = ftString then
          Dec( PFieldInfo(FldBuf)^.DataSize );

        FieldCls := DefaultFieldClasses[DataType];
        TmpField := FieldCls.Create(nil);
        try
          CnvtDBField2Field(TmpField, FldBuf, OutBuf);
        finally
          TmpField.Free;
        end;

        if FieldIsNull(OutBuf) then
          Clear
        else begin
          OutData := OutBuf + SizeOf(TFieldInfo);
          SetData(OutData);
        end;
      end;
      Inc( FldBuf, SpParamBindSize(StoredProc.Params[I]) + SizeOf(TFieldInfo) );
    end;

  finally
    StrDispose(OutBuf);
  end;
end;

function TICustomOdbcDataSet.SpNextResultSet: Boolean;
var
  rcd: SQLRETURN;
begin
  Result := False;

  rcd := Calls.SQLMoreResults( Handle );
  if rcd = SQL_NO_DATA_FOUND then
    Exit;
  Check( rcd );

  FNextResults := True;
  try
    if not(dsfStoredProc in DSFlags) then
      DatabaseError('It is necessary to call Prepare method before open');
    StoredProc.Close;

    DataSet.FieldDefs.Clear;

    StoredProc.Open;
  finally
    FNextResults := False;
  end;

  Result := True;
end;

{ TIODBCDatabase }
procedure TIOdbcDatabase.FreeSqlLib;
begin
  SDOdbc.FreeSqlLib;
end;

procedure TIOdbcDatabase.LoadSqlLib;
begin
  SDOdbc.LoadSqlLib;

  FCalls := OdbCalls;
end;

procedure TIOdbcDatabase.RaiseSDEngineError(AErrorCode, ANativeError: TSDEResult; const AMsg, ASqlState: string);
begin
  raise ESDOdbcError.CreateWithSqlState(AErrorCode, ANativeError, AMsg, ASqlState);
end;

function TIOdbcDatabase.CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet;
begin
  Result := TIOdbcDataSet.Create( ADataSet );
end;


initialization
  hSqlLibModule	:= 0;
  SqlLibRefCount:= 0;
  OdbCalls 	:= TOdbcFunctions.Create;
  SqlLibLock 	:= TCriticalSection.Create;
finalization
  while SqlLibRefCount > 0 do
    FreeSqlLib;
  SqlLibLock.Free;
  OdbCalls.Free;
end.
