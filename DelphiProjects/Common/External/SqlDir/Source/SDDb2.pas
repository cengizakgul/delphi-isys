
{**********************************************************}
{							   }
{       Delphi SQLDirect Component Library		   }
{	IBM DB2 API (CAE v5.2) Interface Unit 		   }
{                                                          }
{       Copyright (c) 1997,2002 by Yuri Sheino		   }
{                                                          }
{**********************************************************}

unit SDDb2;

interface

{$I SqlDir.inc}

uses
  Windows, SysUtils, Classes, Db, Dialogs, SyncObjs,
  SDEngine, SDConsts, SDOdbc;



{******************************************************************************
 *
 * Source File Name = sqlcli.h
 *
 * Function = Include File defining:
 *              DB2 CLI Interface - Constants
 *              DB2 CLI Interface - Data Structures
 *              DB2 CLI Interface - Function Prototypes
 *
 * Operating System = Common C Include File
 *
 *****************************************************************************}


const
// generally useful constants
  SQL_MAX_MESSAGE_LENGTH	= 1024; // message buffer size
  SQL_MAX_ID_LENGTH        	= 18;   // maximum identifier name size, e.g. cursor names

(* test for SQL_SUCCESS or SQL_SUCCESS_WITH_INFO
#define SQL_SUCCEEDED(rc) (((rc)&(~1))==0)
*)


// SQL extended data types
  SQL_GRAPHIC           = -95;
  SQL_VARGRAPHIC        = -96;
  SQL_LONGVARGRAPHIC    = -97;
  SQL_BLOB              = -98;
  SQL_CLOB              = -99;
  SQL_DBCLOB            = -350;

// C data type to SQL data type mapping
  SQL_C_DBCHAR        	= SQL_DBCLOB;
  SQL_C_DECIMAL_IBM   	= SQL_DECIMAL;

// locator type identifier
  SQL_BLOB_LOCATOR      = 31;
  SQL_CLOB_LOCATOR      = 41;
  SQL_DBCLOB_LOCATOR    = -351;

// C Data Type for the LOB locator types
  SQL_C_BLOB_LOCATOR    = SQL_BLOB_LOCATOR;
  SQL_C_CLOB_LOCATOR    = SQL_CLOB_LOCATOR;
  SQL_C_DBCLOB_LOCATOR  = SQL_DBCLOB_LOCATOR;

// SQLColAttributes defines
  SQL_COLUMN_SCHEMA_NAME     	= 16;
  SQL_COLUMN_CATALOG_NAME    	= 17;
  SQL_COLUMN_DISTINCT_TYPE   	= 1250;
  SQL_DESC_DISTINCT_TYPE     	= SQL_COLUMN_DISTINCT_TYPE;

// SQLColAttribute defines for SQL_COLUMN_UPDATABLE condition
  SQL_UPDT_READONLY           	= 0;
  SQL_UPDT_WRITE              	= 1;
  SQL_UPDT_READWRITE_UNKNOWN  	= 2;

// SQL_DIAG_ROW_NUMBER values
  SQL_ROW_NO_ROW_NUMBER 	= (-1);
  SQL_ROW_NUMBER_UNKNOWN        = (-2);

// SQL_DIAG_COLUMN_NUMBER values
  SQL_COLUMN_NO_COLUMN_NUMBER   = (-1);
  SQL_COLUMN_NUMBER_UNKNOWN     = (-2);

// internal representation of numeric data type
const
  SQL_MAX_NUMERIC_LEN	= 16;
type
  TSQL_NUMERIC_STRUCT	= record
    precision:	SQLCHAR;
    scale:	SQLSCHAR;
    sign:	SQLCHAR;	// 1 if positive, 0 if negative
    val:	array[0..SQL_MAX_NUMERIC_LEN-1] of SQLCHAR;
  end;


{******************************************************************************
 *
 * Source File Name = sqlcli1.h
 *
 * Function = Include File defining:
 *              DB2 CLI Interface - Constants
 *              DB2 CLI Interface - Function Prototypes
 *
 * Operating System = Common C Include File
 *
 *****************************************************************************}

const

// SQLGetFunction defines  - supported functions
  SQL_API_SQLBINDFILETOCOL     	= 1250;
  SQL_API_SQLBINDFILETOPARAM   	= 1251;
  SQL_API_SQLSETCOLATTRIBUTES  	= 1252;
  SQL_API_SQLGETSQLCA          	= 1253;

  SQL_API_SQLGETLENGTH         	= 1022;
  SQL_API_SQLGETPOSITION       	= 1023;
  SQL_API_SQLGETSUBSTRING      	= 1024;

  SQL_API_SQLSETCONNECTION     	= 1254;

// SQL_FETCH_DIRECTION masks
  SQL_FD_FETCH_RESUME         	= $00000040;

// SQL_TXN_ISOLATION_OPTION masks
  SQL_TXN_NOCOMMIT             	= $00000020;
  SQL_TRANSACTION_NOCOMMIT     	= SQL_TXN_NOCOMMIT;

// Options for SQLGetStmtOption/SQLSetStmtOption extensions
  SQL_CURSOR_HOLD             	= 1250;
  SQL_ATTR_CURSOR_HOLD        	= 1250;
  SQL_NODESCRIBE_OUTPUT       	= 1251;
  SQL_ATTR_NODESCRIBE_OUTPUT  	= 1251;

  SQL_NODESCRIBE_INPUT        	= 1264;
  SQL_ATTR_NODESCRIBE_INPUT   	= 1264;
  SQL_NODESCRIBE              	= SQL_NODESCRIBE_OUTPUT;
  SQL_ATTR_NODESCRIBE         	= SQL_NODESCRIBE_OUTPUT;
  SQL_CLOSE_BEHAVIOR          	= 1257;
  SQL_ATTR_CLOSE_BEHAVIOR     	= 1257;
  SQL_ATTR_CLOSEOPEN          	= 1265;
  SQL_ATTR_CURRENT_PACKAGE_SET	= 1276;
  SQL_ATTR_DEFERRED_PREPARE   	= 1277;
  SQL_ATTR_EARLYCLOSE         	= 1268;
  SQL_ATTR_PROCESSCTL         	= 1278;

// SQL_CLOSE_BEHAVIOR values.
  SQL_CC_NO_RELEASE            	= 0;
  SQL_CC_RELEASE               	= 1;
  SQL_CC_DEFAULT               	= SQL_CC_NO_RELEASE;

// SQL_ATTR_DEFERRED_PREPARE values
  SQL_DEFERRED_PREPARE_ON      	= 1;
  SQL_DEFERRED_PREPARE_OFF     	= 0;
  SQL_DEFERRED_PREPARE_DEFAULT 	= SQL_DEFERRED_PREPARE_ON;

// SQL_ATTR_EARLYCLOSE values
  SQL_EARLYCLOSE_ON            	= 1;
  SQL_EARLYCLOSE_OFF           	= 0;
  SQL_EARLYCLOSE_DEFAULT       	= SQL_EARLYCLOSE_ON;

// SQL_ATTR_PROCESSCTL masks
  SQL_PROCESSCTL_NOTHREAD      	= $00000001;
  SQL_PROCESSCTL_NOFORK        	= $00000002;

// Options for SQL_CURSOR_HOLD
  SQL_CURSOR_HOLD_ON       	= 1;
  SQL_CURSOR_HOLD_OFF      	= 0;
  SQL_CURSOR_HOLD_DEFAULT  	= SQL_CURSOR_HOLD_ON;


// Options for SQL_NODESCRIBE_INPUT/SQL_NODESCRIBE_OUTPUT
  SQL_NODESCRIBE_ON         	= 1;
  SQL_NODESCRIBE_OFF        	= 0;
  SQL_NODESCRIBE_DEFAULT    	= SQL_NODESCRIBE_OFF;


// Options for SQLGetConnectOption/SQLSetConnectOption extensions
  SQL_WCHARTYPE               	= 1252;
  SQL_LONGDATA_COMPAT         	= 1253;
  SQL_CURRENT_SCHEMA          	= 1254;
  SQL_DB2EXPLAIN              	= 1258;
  SQL_DB2ESTIMATE             	= 1259;
  SQL_PARAMOPT_ATOMIC         	= 1260;
  SQL_STMTTXN_ISOLATION       	= 1261;
  SQL_MAXCONN                 	= 1262;

  SQL_ATTR_WCHARTYPE         	= SQL_WCHARTYPE;
  SQL_ATTR_LONGDATA_COMPAT   	= SQL_LONGDATA_COMPAT;
  SQL_ATTR_CURRENT_SCHEMA    	= SQL_CURRENT_SCHEMA;
  SQL_ATTR_DB2EXPLAIN        	= SQL_DB2EXPLAIN;
  SQL_ATTR_DB2ESTIMATE       	= SQL_DB2ESTIMATE;
  SQL_ATTR_PARAMOPT_ATOMIC   	= SQL_PARAMOPT_ATOMIC;
  SQL_ATTR_STMTTXN_ISOLATION 	= SQL_STMTTXN_ISOLATION;
  SQL_ATTR_MAXCONN           	= SQL_MAXCONN;

// Options for SQLSetConnectOption, SQLSetEnvAttr
  SQL_CONNECTTYPE             	= 1255;
  SQL_SYNC_POINT              	= 1256;
  SQL_MINMEMORY_USAGE         	= 1263;
  SQL_CONN_CONTEXT            	= 1269;
  SQL_ATTR_INHERIT_NULL_CONNECT	= 1270;
  SQL_ATTR_FORCE_CONVERSION_ON_CLIENT	= 1275;

  SQL_ATTR_CONNECTTYPE        	= SQL_CONNECTTYPE;
  SQL_ATTR_SYNC_POINT         	= SQL_SYNC_POINT;
  SQL_ATTR_MINMEMORY_USAGE    	= SQL_MINMEMORY_USAGE;
  SQL_ATTR_CONN_CONTEXT       	= SQL_CONN_CONTEXT;

// Options for SQL_LONGDATA_COMPAT
  SQL_LD_COMPAT_YES           	= 1;
  SQL_LD_COMPAT_NO            	= 0;
  SQL_LD_COMPAT_DEFAULT       	= SQL_LD_COMPAT_NO;

// Options for SQL_PARAMOPT_ATOMIC
  SQL_ATOMIC_YES              	= 1;
  SQL_ATOMIC_NO               	= 0;
  SQL_ATOMIC_DEFAULT          	= SQL_ATOMIC_YES;

// Options for SQL_CONNECT_TYPE
  SQL_CONCURRENT_TRANS        	= 1;
  SQL_COORDINATED_TRANS       	= 2;
  SQL_CONNECTTYPE_DEFAULT     	= SQL_CONCURRENT_TRANS;

// Options for SQL_SYNCPOINT
  SQL_ONEPHASE                	= 1;
  SQL_TWOPHASE                	= 2;
  SQL_SYNCPOINT_DEFAULT       	= SQL_ONEPHASE;

// Options for SQL_DB2ESTIMATE
  SQL_DB2ESTIMATE_ON          	= 1;
  SQL_DB2ESTIMATE_OFF         	= 0;
  SQL_DB2ESTIMATE_DEFAULT     	= SQL_DB2ESTIMATE_OFF;

// Options for SQL_DB2EXPLAIN
  SQL_DB2EXPLAIN_OFF            = $00000000;
  SQL_DB2EXPLAIN_SNAPSHOT_ON   	= $00000001;
  SQL_DB2EXPLAIN_MODE_ON       	= $00000002;
  SQL_DB2EXPLAIN_SNAPSHOT_MODE_ON= SQL_DB2EXPLAIN_SNAPSHOT_ON+SQL_DB2EXPLAIN_MODE_ON;
  SQL_DB2EXPLAIN_ON            	= SQL_DB2EXPLAIN_SNAPSHOT_ON;
  SQL_DB2EXPLAIN_DEFAULT       	= SQL_DB2EXPLAIN_OFF;

{/* Options for SQL_WCHARTYPE
 * Note that you can only specify SQL_WCHARTYPE_CONVERT if you have an
 * external compile flag SQL_WCHART_CONVERT defined
 */
#ifdef SQL_WCHART_CONVERT
#define SQL_WCHARTYPE_CONVERT        1
#endif
#define SQL_WCHARTYPE_NOCONVERT      0
#define SQL_WCHARTYPE_DEFAULT        SQL_WCHARTYPE_NOCONVERT
}

//  LOB file reference options
  SQL_FILE_READ      	=  2;	// Input file to read from
  SQL_FILE_CREATE    	=  8;	// Output file - new file to be created
  SQL_FILE_OVERWRITE 	= 16;  	// Output file - overwrite existing file or create a new file if it doesn't exist
  SQL_FILE_APPEND    	= 32;	// Output file - append to an existing file or create a new file if it doesn't exist

// Source of string for SQLGetLength(), SQLGetPosition(),
// and SQLGetSubstring().
  SQL_FROM_LOCATOR     	= 2;
  SQL_FROM_LITERAL     	= 3;


type
  ESDDb2Error = class(ESDOdbcError);

{ TDB2Functions }
{*******************************************************************************
 * DB2 specific CLI APIs (sqlcli1.h)
 ******************************************************************************}
  TSQLBindFileToCol =
  		function  (hStmt: SQLHSTMT; icol: SQLUSMALLINT;
                   FileName: PChar; var FileNameLength: SQLSMALLINT;
                   var  FileOptions: SQLUINTEGER; MaxFileNameLength: SQLSMALLINT;
                   var StringLength, IndicatorValue: SQLINTEGER): SQLRETURN; stdcall;
  TSQLBindFileToParam =
  		function (hStmt: SQLHSTMT; ipar: SQLUSMALLINT; fSqlType: SQLSMALLINT;
                   FileName: PChar; var FileNameLength: SQLSMALLINT;
                   var  FileOptions: SQLUINTEGER; MaxFileNameLength: SQLSMALLINT;
                   var IndicatorValue: SQLINTEGER): SQLRETURN; stdcall;
  TSQLGetLength =
  		function (hStmt: SQLHSTMT; LocatorCType: SQLSMALLINT; Locator: SQLINTEGER;
                   var StringLength, IndicatorValue: SQLINTEGER): SQLRETURN; stdcall;
  TSQLGetPosition =
  		function (hStmt: SQLHSTMT; LocatorCType: SQLSMALLINT;
                   SourceLocator, SearchLocator: SQLINTEGER;
                   SearchLiteral: PChar; SearchLiteralLength: SQLINTEGER;
                   FromPosition: SQLUINTEGER; var LocatedAt: SQLUINTEGER;
                   var IndicatorValue: SQLINTEGER): SQLRETURN; stdcall;
  TSQLGetSubString =
  		function (hStmt: SQLHSTMT; LocatorCType: SQLSMALLINT;
                   SourceLocator: SQLINTEGER; FromPosition, ForLength: SQLUINTEGER;
                   TargetCType: SQLSMALLINT; rgbValue: SQLPOINTER;
                   cbValueMax: SQLINTEGER; var StringLength, IndicatorValue: SQLINTEGER): SQLRETURN; stdcall;
  TSQLSetColAttributes =
  		function (hStmt: SQLHSTMT; icol: SQLUSMALLINT;
                   szColName: PChar; cbColName, fSQLType: SQLSMALLINT;
                   cbColDef: SQLUINTEGER; ibScale, fNullable: SQLSMALLINT): SQLRETURN; stdcall;

{*******************************************************************************
 * APIs defined only by X/Open CLI (sqlcli1.h)
 ******************************************************************************}
  TSQLBindParam =
    		function(hStmt: SQLHSTMT; ParameterNumber: SQLUSMALLINT;
                     ValueType, ParameterType: SQLSMALLINT; LengthPrecision: SQLUINTEGER;
                     ParameterScale: SQLSMALLINT; ParameterValue: SQLPOINTER;
                     var StrLen_or_Ind: SQLINTEGER): SQLRETURN; stdcall;
	// adding DB2 specific CLI APIs
  TDB2Functions = class(TOdbcFunctions)
  private
    FSQLBindFileToCol:	TSQLBindFileToCol;
    FSQLBindFileToParam:TSQLBindFileToParam;
    FSQLGetLength:	TSQLGetLength;
    FSQLGetPosition:	TSQLGetPosition;
    FSQLGetSubString:	TSQLGetSubString;
    FSQLSetColAttributes:TSQLSetColAttributes;
    FSQLBindParam:	TSQLBindParam;
  public
    property SQLBindFileToCol:	TSQLBindFileToCol 	read FSQLBindFileToCol;
    property SQLBindFileToParam:TSQLBindFileToParam	read FSQLBindFileToParam;
    property SQLGetLength:	TSQLGetLength		read FSQLGetLength;
    property SQLGetPosition:	TSQLGetPosition		read FSQLGetPosition;
    property SQLGetSubString:	TSQLGetSubString	read FSQLGetSubString;
    property SQLSetColAttributes:TSQLSetColAttributes	read FSQLSetColAttributes;
    property SQLBindParam:	TSQLBindParam		read FSQLBindParam;
  end;

{ TIDB2Database }
  TIDB2Database = class(TICustomOdbcDatabase)
  private
    function GetCalls: TDB2Functions;
  protected
    procedure FreeSqlLib; override;
    procedure LoadSqlLib; override;
    procedure RaiseSDEngineError(AErrorCode, ANativeError: TSDEResult; const AMsg, ASqlState: string); override;

    function FieldDataType(ExtDataType: Integer): TFieldType; override;
    function SqlDataType(FieldType: TFieldType): Integer; override;
  public
    function CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet; override;
    property Calls: TDB2Functions read GetCalls;
  end;

{ TIDB2DataSet }
  TIDB2DataSet = class(TICustomOdbcDataSet)
  private
    function GetSrvDatabase: TIDB2Database;
  protected
    procedure Connect; override;

    property SrvDatabase: TIDB2Database read GetSrvDatabase;
  end;

const
  DefSqlApiDLL	= 'DB2CLI.DLL';

var
  DB2Calls: TDB2Functions;


(*******************************************************************************
			Load/Unload Sql-library
********************************************************************************)
procedure LoadSqlLib;
procedure FreeSqlLib;


implementation

resourcestring
  SErrLibLoading 	= 'Error loading library ''%s''';
  SErrLibUnloading	= 'Error unloading library ''%s''';

var
  hSqlLibModule: THandle;
  SqlLibRefCount: Integer;
  SqlLibLock: TCriticalSection;  

procedure SetProcAddresses;
begin
  SDOdbc.SetApiCalls( hSqlLibModule, DB2Calls );
		// DB2 specific CLI APIs (sqlcli1.h)
  @DB2Calls.FSQLBindFileToCol  	:= GetProcAddress(hSqlLibModule, 'SQLBindFileToCol');
  @DB2Calls.FSQLBindFileToParam	:= GetProcAddress(hSqlLibModule, 'SQLBindFileToParam');
  @DB2Calls.FSQLGetLength      	:= GetProcAddress(hSqlLibModule, 'SQLGetLength');
  @DB2Calls.FSQLGetPosition    	:= GetProcAddress(hSqlLibModule, 'SQLGetPosition');
  @DB2Calls.FSQLGetSubString   	:= GetProcAddress(hSqlLibModule, 'SQLGetSubString');
  @DB2Calls.FSQLSetColAttributes:= GetProcAddress(hSqlLibModule, 'SQLSetColAttributes');
		// APIs defined only by X/Open CLI (sqlcli1.h)
  @DB2Calls.FSQLBindParam       := GetProcAddress(hSqlLibModule, 'SQLBindParam');
end;

procedure ResetProcAddresses;
begin
  SDOdbc.ResetApiCalls( DB2Calls );
		// DB2 specific CLI APIs (sqlcli1.h)
  @DB2Calls.FSQLBindFileToCol  	:= nil;
  @DB2Calls.FSQLBindFileToParam	:= nil;
  @DB2Calls.FSQLGetLength      	:= nil;
  @DB2Calls.FSQLGetPosition    	:= nil;
  @DB2Calls.FSQLGetSubString   	:= nil;
  @DB2Calls.FSQLSetColAttributes:= nil;
		// APIs defined only by X/Open CLI (sqlcli1.h)
  @DB2Calls.FSQLBindParam      	:= nil;
end;

procedure LoadSqlLib;
begin
  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 0) then begin
      hSqlLibModule := LoadLibrary( PChar( SrvApiDLLs[stDB2] ) );
      if (hSqlLibModule = 0) then
        raise Exception.CreateFmt(SErrLibLoading, [ SrvApiDLLs[stDB2] ]);
      Inc(SqlLibRefCount);
      SetProcAddresses;
    end else
      Inc(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;

procedure FreeSqlLib;
begin
  if SqlLibRefCount = 0 then
    Exit;

  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 1) then begin
      if FreeLibrary(hSqlLibModule) then
        hSqlLibModule := 0
      else
        raise Exception.CreateFmt(SErrLibUnloading, [ SrvApiDLLs[stDB2] ]);
      Dec(SqlLibRefCount);
      ResetProcAddresses;
    end else
      Dec(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;

{ TIDB2Database }
function TIDB2Database.CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet;
begin
  Result := TIDB2DataSet.Create( ADataSet );
end;

function TIDB2Database.GetCalls: TDB2Functions;
begin
  Result := FCalls as TDB2Functions;
end;

procedure TIDB2Database.FreeSqlLib;
begin
  SDDb2.FreeSqlLib;
end;

procedure TIDB2Database.LoadSqlLib;
begin
  SDDb2.LoadSqlLib;

  FCalls := DB2Calls;
end;

procedure TIDB2Database.RaiseSDEngineError(AErrorCode, ANativeError: TSDEResult; const AMsg, ASqlState: string);
begin
  raise ESDDb2Error.CreateWithSqlState(AErrorCode, ANativeError, AMsg, ASqlState);
end;

function TIDB2Database.FieldDataType(ExtDataType: Integer): TFieldType;
begin
  case ExtDataType of
	// DB2 data types
    SQL_GRAPHIC,
    SQL_VARGRAPHIC:	Result := ftVarBytes;
    SQL_LONGVARGRAPHIC,
    SQL_BLOB:		Result := ftBlob;
    SQL_CLOB,
    SQL_DBCLOB:		Result := ftMemo;
  else
    Result := inherited FieldDataType( ExtDataType );
  end;
end;

function TIDB2Database.SqlDataType(FieldType: TFieldType): Integer;
const
  { Converting from TFieldType to SQL Data Type(DB2) }
  Db2SqlDataTypeMap: array[TFieldType] of Integer = ( 0,	// ftUnknown
	// ftString, ftSmallint, ftInteger, ftWord, ftBoolean
	SQL_CHAR, SQL_SMALLINT, SQL_INTEGER, SQL_INTEGER, SQL_SMALLINT,
	// ftFloat, ftCurrency, ftBCD, 	ftDate, 	ftTime
        SQL_DOUBLE, SQL_DOUBLE, 0, SQL_TYPE_DATE, SQL_TYPE_TIME,
        // ftDateTime, 		ftBytes, ftVarBytes, ftAutoInc, ftBlob
        SQL_TYPE_TIMESTAMP, SQL_BINARY, SQL_BINARY, SQL_INTEGER, SQL_BLOB,
        // ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle
        SQL_CLOB, SQL_BLOB, SQL_CLOB, 	0,	0,
        // ftTypedBinary, ftCursor
        0,	0
{$IFDEF SD_VCL4},
	// ftFixedChar, ftWideString, ftLargeint,
        0,	0,	SQL_BIGINT,
        // ftADT, ftArray, ftReference, ftDataSet
        0,	0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL5},
        // ftOraBlob, ftOraClob, ftVariant,
        0,	0,	0,
        // ftInterface, ftIDispatch, ftGuid
        0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL6},
        // ftTimeStamp, ftFMTBcd
        0,      0
{$ENDIF}
        );
begin
  Result := Db2SqlDataTypeMap[FieldType];
end;


{ TIDB2DataSet }
function TIDB2DataSet.GetSrvDatabase: TIDB2Database;
begin
  Result := (inherited SrvDatabase) as TIDB2Database;
end;

procedure TIDB2DataSet.Connect;
begin
  inherited;

	// specifies the number of values for each parameter (DB2 CLI v5)
  Check( SrvDatabase.OdbcSetStmtAttr( Handle, SQL_ATTR_PARAMSET_SIZE, SQLPOINTER( 1 ), 0 ) );
	// (DB2 CLI v5)
  Check( SrvDatabase.OdbcSetStmtAttr( Handle, SQL_ATTR_USE_BOOKMARKS, SQLPOINTER( SQL_UB_OFF ), 0 ) );

	// Note: This option is an IBM extension
  Check(
  	SrvDatabase.OdbcSetStmtAttr( Handle, SQL_ATTR_CURSOR_HOLD,
        	SQLPOINTER( SQL_CURSOR_HOLD_ON ), 0 )
        );
	// Note: This is an IBM defined extension. (DB2 CLI v5)
  Check(
  	SrvDatabase.OdbcSetStmtAttr( Handle, SQL_ATTR_DEFERRED_PREPARE,
        	SQLPOINTER( SQL_DEFERRED_PREPARE_OFF ), 0 )
        );
end;


initialization
  hSqlLibModule	:= 0;
  SqlLibRefCount:= 0;
  DB2Calls 	:= TDB2Functions.Create;
  SqlLibLock 	:= TCriticalSection.Create;
finalization
  while SqlLibRefCount > 0 do
    FreeSqlLib;
  SqlLibLock.Free;
  DB2Calls.Free;    
end.
