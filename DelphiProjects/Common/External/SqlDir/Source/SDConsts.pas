
{*******************************************************}
{							}
{       Delphi SQLDirect Component Library		}
{       SQLDirect string constans    			}
{                                                       }
{       Copyright (c) 1997,2002 by Yuri Sheino		}
{                                                       }
{*******************************************************}

unit SDConsts;

interface

const
  SLinkSQLDirectHome	= 'http://www.sqldirect-soft.com';
  SLinkSQLDirectStdOrder= 'http://www.shareit.com/programs/101656.htm';
  SLinkSQLDirectProOrder= 'http://www.shareit.com/programs/101657.htm';
  SLinkSQLDirectSupport	= 'mailto:support@sqldirect-soft.com';
  SErrOpenWwwSite	= 'Error opening following address: %s';
  srSQLDirect		= 'SQLDirect';

resourcestring
  SFuncNotImplemented 	= 'Function %s not implemented';

  SAutoSessionExclusive	= 'Cannot enable AutoSessionName property with more than one session on a form or data-module';
  SAutoSessionExists	= 'Cannot add a session to the form or data-module while session ''%s'' has AutoSessionName enabled';
  SAutoSessionActive	= 'Cannot modify SessionName while AutoSessionName is enabled';

  SDuplicateDatabaseName= 'Duplicate database name ''%s''';
  SDuplicateSessionName = 'Duplicate session name ''%s''';
  SInvalidSessionName 	= 'Invalid session name %s';
  SDatabaseNameMissing 	= 'Database name missing';
  SRemoteDatabaseNameMissing = 'Name of remote database missing';
  SSessionNameMissing 	= 'Session name missing';
  SDatabaseOpen 	= 'Cannot perform this operation on an open database';
  SDatabaseClosed 	= 'Cannot perform this operation on a closed database';
  SDatabaseHandleSet 	= 'Database handle owned by a different session';
  SSessionActive 	= 'Cannot perform this operation on an active session';
  SHandleError 		= 'Error creating cursor handle';
  SInvalidFloatField 	= 'Cannot convert field ''%s'' to a floating point value';
  SInvalidIntegerField 	= 'Cannot convert field ''%s'' to an integer value';
  STableMismatch 	= 'Source and destination tables are incompatible';
  SFieldAssignError 	= 'Fields ''%s'' and ''%s'' are not assignment compatible';
  SUnknownFieldType	= 'Field ''%s'' is of an unknown type';
  SBadFieldType		= 'Field ''%s'' is of an unsupported type';
  SCompositeIndexError 	= 'Cannot use array of Field values with Expression Indices';
  SInvalidBatchMove 	= 'Invalid batch move parameters';
  SEmptySQLStatement 	= 'No SQL statement available';
  SNoParameterValue 	= 'No value for parameter ''%s''';
  SNoParameterType 	= 'No parameter type for parameter ''%s''';
  SNoParameterDataType	= 'Unknown data type for parameter ''%s''';
  SBadParameterType	= 'Unknown parameter type for parameter ''%s''';
  SParameterNotFound 	= 'Parameter ''%s'' not found';
  SParamTooBig 		= 'Parameter ''%s'', cannot save data larger than %d bytes';
  SLoginError 		= 'Cannot connect to database ''%s''';
  SInitError 		= 'An error occurred while attempting to initialize the Borland Database Engine (error $%.4x)';
  SFKInternalCalc 	= '&InternalCalc';
{$IFDEF SD_VCL5}        // there is local menu item 'Scal&e' in Delphi 5 IDE
  SDatasetDesigner 	= 'Fields Edi&tor...';
  SDatabaseEditor 	= 'Da&tabase Editor...';
{$ELSE}
  SDatasetDesigner 	= 'Fields &Editor...';
  SDatabaseEditor 	= 'Database &Editor...';
{$ENDIF}  
  SExplore 		= 'E&xplore';
  SLinkDesigner 	= 'Field ''%s'', from the Detail Fields list, must be linked';
  SLinkDetail 		= 'Table ''%s'' cannot be opened';
  SLinkMasterSource 	= 'The MasterSource property of ''%s'' must be linked to a DataSource';
  SLinkMaster 		= 'Unable to open the MasterSource Table';
  SQBEVerb 		= '&Query Builder...';
  SBindVerb 		= 'Define &Parameters...';
  SDisconnectDatabase 	= 'Database is currently connected. Disconnect and continue?';
  SLookupSourceError 	= 'Unable to use duplicate DataSource and LookupSource';
  SLookupTableError 	= 'LookupSource must be connected to TTable component';
  SLookupIndexError 	= '%s must be the lookup table''s active index';
  SParameterTypes 	= ';Input;Output;Input/Output;Result';
  SInvalidParamFieldType= 'Must have a valid field type selected';
  STruncationError 	= 'Parameter ''%s'' truncated on output';
  SInvalidVersion 	= 'Unable to load bind parameters';
  SDataTypes 		= ';String;SmallInt;Integer;Word;Boolean;Float;Currency;BCD;Date;Time;DateTime;;;;Blob;Memo;Graphic;;;;;Cursor;';
  SResultName 		= 'Result';
  SDBCaption 		= '%s.%s Database';
  SParamEditor 		= '%s.%s Parameters';
  SDatasetEditor 	= '%s.%s';
  SIndexFilesEditor 	= '%s.%s Index Files';
  SNoIndexFiles 	= '(None)';
  SIndexDoesNotExist 	= 'Index does not exist. Index: %s';
  SNoTableName 		= 'Missing TableName property';
  SBatchExecute 	= 'Execute';
  SNoCachedUpdates 	= 'Not in cached update mode';
  SInvalidAliasName 	= 'Invalid alias name %s';
  SDBGridColEditor 	= 'Co&lumns Editor...';
  SExprTermination 	= 'Filter expression incorrectly terminated';
  SExprNameError 	= 'Unterminated field name';
  SExprStringError 	= 'Unterminated string constant';
  SExprInvalidChar 	= 'Invalid filter expression character: ''%s''';
  SExprNoRParen 	= ''')'' expected but %s found';
  SExprExpected 	= 'Expression expected but %s found';
  SExprBadCompare 	= 'Relational operators require a field and a constant';
  SExprBadField 	= 'Field ''%s'' cannot be used in a filter expression';
  SExprBadNullTest 	= 'NULL only allowed with ''='' and ''<>''';
  SExprRangeError 	= 'Constant out of range';
  SExprNotBoolean 	= 'Field ''%s'' is not of type Boolean';
  SExprIncorrect 	= 'Incorrectly formed filter expression';
  SExprNothing 		= 'nothing';
  SNoFieldAccess 	= 'Cannot access field ''%s'' in a filter';
  SUpdateSQLEditor 	= 'UpdateSQL &Editor...';
  SNoDataSet 		= 'No dataset association';
  SUntitled 		= 'Untitled Application';
  SUpdateWrongDB 	= 'Cannot update, %s is not owned by %s';
  SUpdateFailed 	= 'Update failed';
  SSQLGenSelect 	= 'Must select at least one key field and one update field';
  SSQLNotGenerated 	= 'Update SQL statements not generated, exit anyway?';
  SSQLDataSetOpen 	= 'Unable to determine field names for %s';
  SLocalTransDirty 	= 'The transaction isolation level must be dirty read for local databases';
  SPrimary 		= 'Primary';
  SClientDSAssignData 	= 'Assign &Local Data...';
  SClientDSClearData 	= '&Clear Data';
  SClientDataSetEditor 	= '%s.%s Data';
  SMissingDataSet 	= 'Missing DataSet property';

  SFatalError		= 'Fatal error: %s';
  SMismatchFieldSize 	= 'Mismatch field size for type %s';
  SNoStoredProcName 	= 'Empty stored procedure name';
  SInsufficientColumnBufSize 	= 'Insufficient column buffer size';
  SInsufficientIDateTimeBufSize = 'Insufficient buffer size for internal datetime type';
  SUpdateImpossible 	= 'Update impossible (set property UpdateObject or event handler for OnUpdateRecord)';
  SNotInTransaction	= 'Transaction is not active';
  SInTransaction	= 'Transaction is active';

  SServerOpen 		= 'Cannot perform this operation on an open server';
  SServerClosed 	= 'Cannot perform this operation on a closed server';
  SServerNameMissing	= 'Server name missing';
  SServerLoginError	= 'Cannot connect to server ''%s''';
  SNoServerType		= 'SQL-server not supported';
  SServerTypeMismatch	= 'ServerType mismatch, expecting: %s actual: %s';
  SBadServerType	= 'Invalid value for ServerType property';
  SFieldDescNotFound 	= 'Description of field ''%s'' not found';

  SInvalidOperation	= 'Operation not applicable';
  SNoCapability		= 'Capability not supported';
  SFuncFailed		= 'Function "%s" failed';
//  SOwnerTableError 	= 'Unable to resolve owner table for field ''%s''';

  SFalseString		= 'FALSE';
  STrueString		= 'TRUE';
implementation

end.

