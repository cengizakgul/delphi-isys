
{*******************************************************}
{							}
{       Delphi SQLDirect Component Library		}
{	PostgreSQL API (ver.7.1) Interface Unit	}
{       PostgreSQL API Date: 22.03.2001        		}
{       		                                }
{       Copyright (c) 1997,2002 by Yuri Sheino		}
{                                                       }
{*******************************************************}

unit SDPgSQL;

interface

{$I SqlDir.inc}

uses
  Windows, SysUtils, Classes, Db, Registry, SyncObjs,
  SDEngine, SDConsts;


{*******************************************************************************
*	pg_type.h - The OIDs of the built-in datatypes are defined in          *
*			system table pg_type and                               *
* 			src/include/catalog/pg_type.h in the source tree.      *
*******************************************************************************}

const
  BOOLOID    	= 16;
  BYTEAOID   	= 17;
  CHAROID    	= 18;
  NAMEOID    	= 19;
  INT8OID    	= 20;
  INT2OID    	= 21;
  INT2VECTOROID	= 22;
  INT4OID    	= 23;
  REGPROCOID 	= 24;
  TEXTOID    	= 25;
  OIDOID     	= 26;
  TIDOID     	= 27;
  XIDOID 	= 28;
  CIDOID 	= 29;
  OIDVECTOROID	= 30;
  POINTOID   	= 600;
  LSEGOID    	= 601;
  PATHOID    	= 602;
  BOXOID     	= 603;
  POLYGONOID 	= 604;
  LINEOID    	= 628;
  FLOAT4OID	= 700;
  FLOAT8OID	= 701;
  ABSTIMEOID	= 702;
  RELTIMEOID	= 703;
  TINTERVALOID	= 704;
  UNKNOWNOID  	= 705;
  CIRCLEOID   	= 718;
  CASHOID 	= 790;
  INETOID 	= 869;
  CIDROID 	= 650;
  ACLITEMSIZE	= 8;
  BPCHAROID  	= 1042;
  VARCHAROID 	= 1043;
  DATEOID    	= 1082;
  TIMEOID    	= 1083;
  TIMESTAMPOID	= 1184;
  INTERVALOID 	= 1186;
  TIMETZOID   	= 1266;
  ZPBITOID    	= 1560;
  VARBITOID   	= 1562;
  NUMERICOID  	= 1700;
	// for PostgreSQL v.7.2
  TIMESTAMPOID_v72= 1114;

{*******************************************************************************
*	postgres_ext.h - This file is only for fundamental Postgres declarations *
*******************************************************************************}

{*
 * Object ID is a fundamental type in Postgres.
 *}
type
  Oid	= Cardinal;	// object id

const
  InvalidOid	= 0;

{*
 * NAMEDATALEN is the max length for system identifiers (e.g. table names,
 * attribute names, function names, etc.)
 *
 * NOTE that databases with different NAMEDATALEN's cannot interoperate!
 *}
  NAMEDATALEN 	= 32;

{*******************************************************************************
*	libpq-fe.h - Common definition used by frontend postgres applications  *
*******************************************************************************}

const
  {	Application-visible enum types	}
	// enum ConnStatusType
  CONNECTION_OK 	= 0;
  CONNECTION_BAD	= 1;
  // Non-blocking mode only below here
  CONNECTION_STARTED	= 2;		// Waiting for connection to be made
  CONNECTION_MADE 	= 3;		// Connection OK; waiting to send
  CONNECTION_AWAITING_RESPONSE	= 4;	// Waiting for a response from the postmaster
  CONNECTION_AUTH_OK	= 5;		// Received authentication; waiting for backend startup
  CONNECTION_SETENV	= 6;		// Negotiating environment

	// enum PostgresPollingStatusType
  PGRES_POLLING_FAILED	= 0;
  PGRES_POLLING_READING	= 1;		// These two indicate that one may
  PGRES_POLLING_WRITING	= 2;		// use select before polling again
  PGRES_POLLING_OK	= 3;
  PGRES_POLLING_ACTIVE	= 4;		// Can call poll function immediately

	// enum ExecStatusType
  PGRES_EMPTY_QUERY	= 0;
  PGRES_COMMAND_OK	= 1;		// a query command that doesn't return anything was executed properly by the backend
  PGRES_TUPLES_OK	= 2;		// a query command that returns tuples was executed properly by the backend, PGresult contains the result tuples
  PGRES_COPY_OUT	= 3;		// Copy Out data transfer in progress
  PGRES_COPY_IN		= 4;		// Copy In data transfer in progress
  PGRES_BAD_RESPONSE	= 5;		// an unexpected response was recv'd from the backend
  PGRES_NONFATAL_ERROR	= 6;
  PGRES_FATAL_ERROR	= 7;

type
  TVoid = Pointer;

{* PGconn encapsulates a connection to the backend.
 * The contents of this struct are not supposed to be known to applications.
 *}
  PPGconn = TVoid;

  TConnStatusType 		= Integer;	// enum
  TPostgresPollingStatusType	= Integer;	// enum
  TExecStatusType		= Integer;	// enum
  PSSL				= Pointer;
    
{* PGresult encapsulates the result of a query (or more precisely, of a single
 * SQL command --- a query string given to PQsendQuery can contain multiple
 * commands and thus return multiple PGresult objects).
 * The contents of this struct are not supposed to be known to applications.
 *}
  PPGresult = TVoid;

{* PGnotify represents the occurrence of a NOTIFY message.
 * Ideally this would be an opaque typedef, but it's so simple that it's
 * unlikely to change.
 * NOTE: in Postgres 6.4 and later, the be_pid is the notifying backend's,
 * whereas in earlier versions it was always your own backend's PID.
 *}
  TPGnotify	= record
    szRelName: array[0..NAMEDATALEN] of Char;	// name of relation containing data
    be_pid: Integer;				// process id of backend
  end;
  PPGnotify	= ^TPGnotify;

{* PQnoticeProcessor is the function type for the notice-message callback.
 *}
  TPQnoticeProcessor	= procedure (arg: Pointer; szMessage: PChar); cdecl;

	{ Print options for PQprint() }
  TPQBool	= Char;

  TPQprintOpt	= record
    header,				// print output field headings and row count
    align,				// fill align the fields
    standard,				// old brain dead format
    html3,				// output html tables
    expanded,				// expand tables
    pager:	TPQBool;		// use pager for output if needed
    fieldSep,				// field separator
    tableOpt,				// insert to HTML <table ...>
    caption:	PChar;			// HTML <caption>
    fieldName:	array[0..0] of PChar;	// null terminated array of repalcement field names
  end;
  PPQprintOpt	= ^TPQprintOpt;


{* ----------------
 * Structure for the conninfo parameter definitions returned by PQconndefaults
 *
 * All fields except "val" point at static strings which must not be altered.
 * "val" is either NULL or a malloc'd current-value string.  PQconninfoFree()
 * will release both the val strings and the PQconninfoOption array itself.
 * ----------------
 *}
  TPQconninfoOption	= record
    szKeyword,		// The keyword of the option
    szEnvVar,		// Fallback environment variable name
    szCompiled,		// Fallback compiled in default value
    szVal,		// Option's current value, or NULL
    szLabel,		// Label for field in connect dialog
    szDispChar:	PChar;	// Character to display for this field in a connect dialog.
    			// Values are: "" Display entered value as is "*" Password field - hide value "D"
                        // Debug option - don't show by default
    dispsize: Integer;	// Field size in characters for dialog
  end;
  PPQconninfoOption	= ^TPQconninfoOption;


{* ----------------
 * PQArgBlock -- structure for PQfn() arguments
 * ----------------
 *}
  TPQArgBlock	= record
    len,
    isint:	Integer;
    IntOrPtr:	Integer;	// union {int *ptr; int	integer;} u;
  end;


{* ----------------
 * Exported functions of libpq
 * ----------------
 *}
var
	// make a new client connection to the backend
  // Asynchronous (non-blocking)
  PQconnectStart: 	function(szConnInfo: PChar): PPGconn; cdecl;
  PQconnectPoll:	function(conn: PPGconn): TPostgresPollingStatusType; cdecl;

  // Synchronous (blocking)
  PQconnectdb: 		function(szConnInfo: PChar): PPGconn; cdecl;
  PQsetdbLogin:		function(pghost, pgport, pgoptions, pgtty, dbName, login, pwd: PChar): PPGconn; cdecl;

  // close the current connection and free the PGconn data structure
   PQfinish:		procedure(conn: PPGconn); cdecl;

  // get info about connection options known to PQconnectdb
  PQconndefaults:	function: PPQconninfoOption; cdecl;

  // free the data structure returned by PQconndefaults()
  PQconninfoFree:	procedure(connOptions: PPQconninfoOption); cdecl;

  	// close the current connection and restablish a new one with the same parameters

  // Asynchronous (non-blocking)
  PQresetStart:	function(conn: PPGconn): Integer; cdecl;
  PQresetPoll:	function(conn: PPGconn): TPostgresPollingStatusType; cdecl;
  // Synchronous (blocking)
  PQreset:	procedure(conn: PPGconn); cdecl;

  // issue a cancel request
  PQrequestCancel:	function(conn: PPGconn): Integer; cdecl;

	// Accessor functions for PGconn objects
  PQdb:		function(const conn: PPGconn): PChar; cdecl;
  PQuser:	function(const conn: PPGconn): PChar; cdecl;
  PQpass:	function(const conn: PPGconn): PChar; cdecl;
  PQhost:	function(const conn: PPGconn): PChar; cdecl;
  PQport:	function(const conn: PPGconn): PChar; cdecl;
  PQtty:	function(const conn: PPGconn): PChar; cdecl;
  PQoptions:	function(const conn: PPGconn): PChar; cdecl;
  PQstatus:	function(const conn: PPGconn): TConnStatusType; cdecl;
  PQerrorMessage:function(const conn: PPGconn): PChar; cdecl;
  PQsocket:	function(const conn: PPGconn): Integer; cdecl;
  PQbackendPID:	function(const conn: PPGconn): Integer; cdecl;
  PQclientEncoding:
  		function(const conn: PPGconn): Integer; cdecl;
  PQsetClientEncoding:
  		function(conn: PPGconn; const encoding: PChar): Integer; cdecl;

  // Get the SSL structure associated with a connection
  PQgetssl:	function(conn: PPGconn): PSSL; cdecl;


  // Enable/disable tracing
  PQtrace:	procedure(conn: PPGconn; debug_port: Pointer); cdecl;
  PQuntrace:	procedure(conn: PPGconn); cdecl;

  // Override default notice processor
  PQsetNoticeProcessor:
  		function(conn: PPGconn; proc: TPQnoticeProcessor; arg: Pointer): TPQnoticeProcessor;


  // Simple synchronous query
  PQexec:	function(conn: PPGconn; const query: PChar): PPGresult; cdecl;
  PQnotifies:	function(conn: PPGconn): PPGnotify; cdecl;

  // Interface for multiple-result or asynchronous queries
  PQsendQuery:	function(conn: PPGconn; const query: PChar): Integer; cdecl;
  PQgetResult:	function(conn: PPGconn): PPGresult; cdecl;

  // Routines for managing an asychronous query
  PQisBusy: 	function(conn: PPGconn): Integer; cdecl;
  PQconsumeInput:function(conn: PPGconn): Integer; cdecl;

  // Routines for copy in/out
  PQgetline: 	function(conn: PPGconn; str: PChar; length: Integer): Integer; cdecl;
  PQputline: 	function(conn: PPGconn; const str: PChar): Integer; cdecl;
  PQgetlineAsync:function(conn: PPGconn; buffer: PChar; bufsize: Integer): Integer; cdecl;
  PQputnbytes: 	function(conn: PPGconn; const buffer: PChar; nbytes: Integer): Integer; cdecl;
  PQendcopy: 	function(conn: PPGconn): Integer; cdecl;

  // Set blocking/nonblocking connection to the backend
  PQsetnonblocking:
  		function(conn: PPGconn; arg: Integer): Integer; cdecl;
  PQisnonblocking:
  		function(const conn: PPGconn): Integer; cdecl;

  // Force the write buffer to be written (or at least try)
  PQflush:	function(conn: PPGconn): Integer; cdecl;


  	// Accessor functions for PGresult objects
  PQresultStatus:
  	 	function(const res: PPGresult): TExecStatusType; cdecl;
  PQresStatus: 	function(status: TExecStatusType): PChar; cdecl;
  PQresultErrorMessage:
  	 	function(const res: PPGresult): PChar; cdecl;
  PQntuples: 	function(const res: PPGresult): Integer; cdecl;
  PQnfields: 	function(const res: PPGresult): Integer; cdecl;
  PQbinaryTuples:
  	 	function(const res: PPGresult): Integer; cdecl;
  PQfname: 	function(const res: PPGresult; field_num: Integer): PChar; cdecl;
  PQfnumber: 	function(const res: PPGresult; const field_name: PChar): Integer; cdecl;
  PQftype: 	function(const res: PPGresult; field_num: Integer): Oid; cdecl;
  PQfsize: 	function(const res: PPGresult; field_num: Integer): Integer; cdecl;
  PQfmod: 	function(const res: PPGresult; field_num: Integer): Integer; cdecl;
  PQcmdStatus: 	function(res: PPGresult): PChar; cdecl;
  PQoidStatus: 	function(const res: PPGresult): PChar; cdecl;	       	// old and ugly
  PQoidValue: 	function(const res: PPGresult): Oid; cdecl;		// new and improved
  PQcmdTuples: 	function(res: PPGresult): PChar; cdecl;
  PQgetvalue: 	function(const res: PPGresult; tup_num, field_num: Integer): PChar; cdecl;
  PQgetlength: 	function(const res: PPGresult; tup_num, field_num: Integer): Integer; cdecl;
  PQgetisnull: 	function(const res: PPGresult; tup_num, field_num: Integer): Integer; cdecl;

  // Delete a PGresult
  PQclear:	procedure(res: PPGresult); cdecl;

  // Make an empty PGresult with given status (some apps find this useful).
  //If conn is not NULL and status indicates an error, the conn's errorMessage is copied.
  PQmakeEmptyPGresult:
  		function(conn: PPGconn; status: TExecStatusType): PPGresult; cdecl;


  PQprint:	procedure(fout: Pointer; const res: PPGresult; const ps: PPQprintOpt); cdecl;


	// Large-object access routines
  lo_open: 	function(conn: PPGconn; lobjId: Oid; mode: Integer): Integer; cdecl;
  lo_close: 	function(conn: PPGconn; fd: Integer): Integer; cdecl;
  lo_read: 	function(conn: PPGconn; fd: Integer; buf: PChar; len: Integer): Integer; cdecl;
  lo_write: 	function(conn: PPGconn; fd: Integer; buf: PChar; len: Integer): Integer; cdecl;
  lo_lseek: 	function(conn: PPGconn; fd: Integer; offset, whence: Integer): Integer; cdecl;
  lo_creat: 	function(conn: PPGconn; mode: Integer): Oid; cdecl;
  lo_tell: 	function(conn: PPGconn; fd: Integer): Integer; cdecl;
  lo_unlink: 	function(conn: PPGconn; lobjId: Oid): Integer; cdecl;
  lo_import: 	function(conn: PPGconn; const filename: PChar): Oid; cdecl;
  lo_export: 	function(conn: PPGconn; lobjId: Oid; const filename: PChar): Integer; cdecl;


  // Determine length of multibyte encoded char at *s
  PQmblen:	function(const s: PChar; encoding: Integer): Integer; cdecl;

  // Get encoding id from environment variable PGCLIENTENCODING
  PQenv2encoding:
  		function: Integer; cdecl;


type
  ESDPgSQLError = class(ESDEngineError);

{ TIPsDatabase }
  TPgSrvInfo	= record
    PgConn:	PPGconn;			// pointer to TMYSQL structure for communication with the MySQL
  end;
  PPgSrvInfo	= ^TPgSrvInfo;

  TIPgDatabase = class(TISrvDatabase)
  private
    FHandle: PSDHandleRec;

    procedure Check(pgconn: PPGconn);
    procedure CheckRes(pgres: PPGresult);
    procedure AllocHandle;
    procedure FreeHandle;
    procedure ExecStmt(const sStmt: string);
    function GetPgConn: PPGconn;
    procedure GetStmtResult(const Stmt: string; List: TStrings);
  protected
    function GetClientVersion: LongInt; override;
    function GetServerVersion: LongInt; override;
    function GetVersionString: string; override;
    procedure Commit; override;
    function FieldDataType(ExtDataType: Integer): TFieldType; override;
    function GetHandle: PSDHandleRec; override;
    procedure GetStoredProcNames(List: TStrings); override;
    procedure GetTableFieldNames(const TableName: string; List: TStrings); override;
    procedure GetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings); override; 
    procedure Logon(const sRemoteDatabase, sUserName, sPassword: string); override;
    procedure Logoff(Force: Boolean); override;
    function NativeDataSize(FieldType: TFieldType): Word; override;
    function RequiredCnvtFieldType(FieldType: TFieldType): Boolean; override;
    procedure Rollback; override;
    procedure SetDefaultParams; override;
    procedure SetHandle(AHandle: PSDHandleRec); override;
    procedure SetTransIsolation(Value: TSDTransIsolation); override;
    procedure StartTransaction; override;
//    function SPDescriptionsAvailable: Boolean; override;
  public
    constructor Create(ADatabase: TSDDatabase); override;
    destructor Destroy; override;
    function CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet; override;
    property PgConn: PPGconn read GetPgConn;
  end;


{ TIPgDataSet }
  TIPgDataSet = class(TISrvDataSet)
  private
    FHandle: PSDCursor;
    FRecCount,			// record count in a result set
    FCurrRec: Integer;		// the current record number (0..FRecCount-1)

    FStmt,			// w/o bind data
    FBindStmt: string;		// with bind parameter data, it's used to check whether the statement was executed
    FRowsAffected: Integer;	// number of rows affected by the last query

    function GetExecuted: Boolean;
    function GetSrvDatabase: TIPgDatabase;
    function CnvtDateTime2SqlString(Value: TDateTime): string;
    function CnvtFloat2SqlString(Value: Double): string;
    function CnvtByteString2String(Value: string): string;
    function CnvtString2ByteString(Value: string): string;
    function CnvtString2EscapeString(Value: string): string;
    procedure InternalQBindParams;
    procedure InternalQExecute;
  protected
    procedure Check(pgres: PPGresult);

    procedure CloseResultSet; override;
    function CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean; override;
    procedure Disconnect(Force: Boolean); override;
    procedure Execute; override;
    function FetchNextRow: Boolean; override;
    procedure GetFieldDescs(Descs: TSDFieldDescList); override;
    function GetHandle: PSDCursor; override;
    procedure SetSelectBuffer; override;
    function ResultSetExists: Boolean; override;
    function ReadBlob(FieldNo: Integer; var BlobData: string): Longint; override;
	// Query methods
    procedure QBindParams; override;
    function QGetRowsAffected: Integer; override;
    procedure QPrepareSQL(Value: PChar); override;
    procedure QExecute; override;
{	// StoredProc methods
    procedure SpBindParams; override;
    procedure SpCreateParamDesc; override;
    procedure SpPrepareProc; override;
    procedure SpExecute; override;
    procedure SpExecProc; override;
    procedure SpGetResults; override;
    function SpNextResultSet: Boolean; override;
}
    property Executed: Boolean read GetExecuted;
    property SrvDatabase: TIPgDatabase read GetSrvDatabase;
  public
    constructor Create(ADataSet: TSDDataSet); override;
    destructor Destroy; override;
  end;


const
  DefSqlApiDLL	= 'libpq.dll';

{*******************************************************************************
		Load/Unload Sql-library
*******************************************************************************}
procedure LoadSqlLib;
procedure FreeSqlLib;


implementation

resourcestring
  SErrLibLoading 	= 'Error loading library ''%s''';
  SErrLibUnloading	= 'Error unloading library ''%s''';
  SErrFuncNotFound	= 'Function ''%s'' not found in PostgreSQL library';

var
  hSqlLibModule: THandle;
  SqlLibRefCount: Integer;
  SqlLibLock: TCriticalSection;  
  dwLoadedFileVer: LongInt;     // version of client DLL used

(*******************************************************************************
			Load/Unload Sql-library
********************************************************************************)
procedure SetProcAddresses;
begin
	// checked function availability in v.6-7.1
  @PQconnectStart	:= GetProcAddress(hSqlLibModule, 'PQconnectStart');  		// ASSERT( @PQconnectStart		<>nil, Format(SErrFuncNotFound, ['PQconnectStart']) );
  @PQconnectPoll 	:= GetProcAddress(hSqlLibModule, 'PQconnectPoll');  		// ASSERT( @PQconnectPoll 		<>nil, Format(SErrFuncNotFound, ['PQconnectPoll']) );
  @PQconnectdb   	:= GetProcAddress(hSqlLibModule, 'PQconnectdb');  		ASSERT( @PQconnectdb   		<>nil, Format(SErrFuncNotFound, ['PQconnectdb']) );
  @PQsetdbLogin  	:= GetProcAddress(hSqlLibModule, 'PQsetdbLogin');  		ASSERT( @PQsetdbLogin  		<>nil, Format(SErrFuncNotFound, ['PQsetdbLogin']) );
  @PQfinish      	:= GetProcAddress(hSqlLibModule, 'PQfinish');  			ASSERT( @PQfinish      		<>nil, Format(SErrFuncNotFound, ['PQfinish']) );
  @PQconndefaults 	:= GetProcAddress(hSqlLibModule, 'PQconndefaults');  		ASSERT( @PQconndefaults		<>nil, Format(SErrFuncNotFound, ['PQconndefaults']) );
  @PQconninfoFree 	:= GetProcAddress(hSqlLibModule, 'PQconninfoFree');  		ASSERT( @PQconninfoFree		<>nil, Format(SErrFuncNotFound, ['PQconninfoFree']) );
  @PQresetStart  	:= GetProcAddress(hSqlLibModule, 'PQresetStart');  		// ASSERT( @PQresetStart  		<>nil, Format(SErrFuncNotFound, ['PQresetStart']) );
  @PQresetPoll   	:= GetProcAddress(hSqlLibModule, 'PQresetPoll');    		// ASSERT( @PQresetPoll   		<>nil, Format(SErrFuncNotFound, ['PQresetPoll']) );
  @PQreset       	:= GetProcAddress(hSqlLibModule, 'PQreset');  			ASSERT( @PQreset       		<>nil, Format(SErrFuncNotFound, ['PQreset']) );
  @PQrequestCancel   	:= GetProcAddress(hSqlLibModule, 'PQrequestCancel');		ASSERT( @PQrequestCancel    	<>nil, Format(SErrFuncNotFound, ['PQrequestCancel']) );
  @PQdb              	:= GetProcAddress(hSqlLibModule, 'PQdb');  			ASSERT( @PQdb               	<>nil, Format(SErrFuncNotFound, ['PQdb']) );
  @PQuser            	:= GetProcAddress(hSqlLibModule, 'PQuser');  			ASSERT( @PQuser             	<>nil, Format(SErrFuncNotFound, ['PQuser']) );
  @PQpass            	:= GetProcAddress(hSqlLibModule, 'PQpass');  			ASSERT( @PQpass             	<>nil, Format(SErrFuncNotFound, ['PQpass']) );
  @PQhost            	:= GetProcAddress(hSqlLibModule, 'PQhost');  			ASSERT( @PQhost             	<>nil, Format(SErrFuncNotFound, ['PQhost']) );
  @PQport            	:= GetProcAddress(hSqlLibModule, 'PQport');  			ASSERT( @PQport             	<>nil, Format(SErrFuncNotFound, ['PQport']) );
  @PQtty             	:= GetProcAddress(hSqlLibModule, 'PQtty');  			ASSERT( @PQtty              	<>nil, Format(SErrFuncNotFound, ['PQtty']) );
  @PQoptions         	:= GetProcAddress(hSqlLibModule, 'PQoptions');  		ASSERT( @PQoptions          	<>nil, Format(SErrFuncNotFound, ['PQoptions']) );
  @PQstatus          	:= GetProcAddress(hSqlLibModule, 'PQstatus');  			ASSERT( @PQstatus           	<>nil, Format(SErrFuncNotFound, ['PQstatus']) );
  @PQerrorMessage    	:= GetProcAddress(hSqlLibModule, 'PQerrorMessage');  		ASSERT( @PQerrorMessage     	<>nil, Format(SErrFuncNotFound, ['PQerrorMessage']) );
  @PQsocket          	:= GetProcAddress(hSqlLibModule, 'PQsocket');  			ASSERT( @PQsocket           	<>nil, Format(SErrFuncNotFound, ['PQsocket']) );
  @PQbackendPID      	:= GetProcAddress(hSqlLibModule, 'PQbackendPID');  		ASSERT( @PQbackendPID       	<>nil, Format(SErrFuncNotFound, ['PQbackendPID']) );
  @PQclientEncoding  	:= GetProcAddress(hSqlLibModule, 'PQclientEncoding');  		ASSERT( @PQclientEncoding   	<>nil, Format(SErrFuncNotFound, ['PQclientEncoding']) );
  @PQsetClientEncoding	:= GetProcAddress(hSqlLibModule, 'PQsetClientEncoding');	// ASSERT( @PQsetClientEncoding	<>nil, Format(SErrFuncNotFound, ['PQsetClientEncoding']) );
  @PQtrace            	:= GetProcAddress(hSqlLibModule, 'PQtrace');  			ASSERT( @PQtrace            	<>nil, Format(SErrFuncNotFound, ['PQtrace']) );
  @PQuntrace          	:= GetProcAddress(hSqlLibModule, 'PQuntrace');  		ASSERT( @PQuntrace          	<>nil, Format(SErrFuncNotFound, ['PQuntrace']) );
  @PQsetNoticeProcessor	:= GetProcAddress(hSqlLibModule, 'PQsetNoticeProcessor');  	ASSERT( @PQsetNoticeProcessor	<>nil, Format(SErrFuncNotFound, ['PQsetNoticeProcessor']) );
  @PQexec              	:= GetProcAddress(hSqlLibModule, 'PQexec');  			ASSERT( @PQexec              	<>nil, Format(SErrFuncNotFound, ['PQexec']) );
  @PQnotifies          	:= GetProcAddress(hSqlLibModule, 'PQnotifies');  		ASSERT( @PQnotifies          	<>nil, Format(SErrFuncNotFound, ['PQnotifies']) );
  @PQsendQuery         	:= GetProcAddress(hSqlLibModule, 'PQsendQuery');  		ASSERT( @PQsendQuery         	<>nil, Format(SErrFuncNotFound, ['PQsendQuery']) );
  @PQgetResult         	:= GetProcAddress(hSqlLibModule, 'PQgetResult');  		ASSERT( @PQgetResult         	<>nil, Format(SErrFuncNotFound, ['PQgetResult']) );
  @PQisBusy            	:= GetProcAddress(hSqlLibModule, 'PQisBusy');  			ASSERT( @PQisBusy            	<>nil, Format(SErrFuncNotFound, ['PQisBusy']) );
  @PQconsumeInput      	:= GetProcAddress(hSqlLibModule, 'PQconsumeInput');  		ASSERT( @PQconsumeInput      	<>nil, Format(SErrFuncNotFound, ['PQconsumeInput']) );
  @PQgetline           	:= GetProcAddress(hSqlLibModule, 'PQgetline');  		ASSERT( @PQgetline           	<>nil, Format(SErrFuncNotFound, ['PQgetline']) );
  @PQputline           	:= GetProcAddress(hSqlLibModule, 'PQputline');  		ASSERT( @PQputline           	<>nil, Format(SErrFuncNotFound, ['PQputline']) );
  @PQgetlineAsync      	:= GetProcAddress(hSqlLibModule, 'PQgetlineAsync');  		ASSERT( @PQgetlineAsync      	<>nil, Format(SErrFuncNotFound, ['PQgetlineAsync']) );
  @PQputnbytes         	:= GetProcAddress(hSqlLibModule, 'PQputnbytes');  		ASSERT( @PQputnbytes         	<>nil, Format(SErrFuncNotFound, ['PQputnbytes']) );
  @PQendcopy           	:= GetProcAddress(hSqlLibModule, 'PQendcopy');  		ASSERT( @PQendcopy           	<>nil, Format(SErrFuncNotFound, ['PQendcopy']) );
  @PQsetnonblocking    	:= GetProcAddress(hSqlLibModule, 'PQsetnonblocking');  		// ASSERT( @PQsetnonblocking    	<>nil, Format(SErrFuncNotFound, ['PQsetnonblocking']) );
  @PQisnonblocking     	:= GetProcAddress(hSqlLibModule, 'PQisnonblocking');  		// ASSERT( @PQisnonblocking     	<>nil, Format(SErrFuncNotFound, ['PQisnonblocking']) );
  @PQflush             	:= GetProcAddress(hSqlLibModule, 'PQflush');  			// ASSERT( @PQflush             	<>nil, Format(SErrFuncNotFound, ['PQflush']) );
  @PQresultStatus      	:= GetProcAddress(hSqlLibModule, 'PQresultStatus');  		ASSERT( @PQresultStatus      	<>nil, Format(SErrFuncNotFound, ['PQresultStatus']) );
  @PQresStatus         	:= GetProcAddress(hSqlLibModule, 'PQresStatus');  		ASSERT( @PQresStatus         	<>nil, Format(SErrFuncNotFound, ['PQresStatus']) );
  @PQresultErrorMessage	:= GetProcAddress(hSqlLibModule, 'PQresultErrorMessage');  	ASSERT( @PQresultErrorMessage 	<>nil, Format(SErrFuncNotFound, ['PQresultErrorMessage']) );
  @PQntuples           	:= GetProcAddress(hSqlLibModule, 'PQntuples');  		ASSERT( @PQntuples           	<>nil, Format(SErrFuncNotFound, ['PQntuples']) );
  @PQnfields           	:= GetProcAddress(hSqlLibModule, 'PQnfields');  		ASSERT( @PQnfields           	<>nil, Format(SErrFuncNotFound, ['PQnfields']) );
  @PQbinaryTuples      	:= GetProcAddress(hSqlLibModule, 'PQbinaryTuples');  		ASSERT( @PQbinaryTuples      	<>nil, Format(SErrFuncNotFound, ['PQbinaryTuples']) );
  @PQfname             	:= GetProcAddress(hSqlLibModule, 'PQfname');  			ASSERT( @PQfname             	<>nil, Format(SErrFuncNotFound, ['PQfname']) );
  @PQfnumber           	:= GetProcAddress(hSqlLibModule, 'PQfnumber');  		ASSERT( @PQfnumber           	<>nil, Format(SErrFuncNotFound, ['PQfnumber']) );
  @PQftype             	:= GetProcAddress(hSqlLibModule, 'PQftype');  			ASSERT( @PQftype             	<>nil, Format(SErrFuncNotFound, ['PQftype']) );
  @PQfsize             	:= GetProcAddress(hSqlLibModule, 'PQfsize');  			ASSERT( @PQfsize             	<>nil, Format(SErrFuncNotFound, ['PQfsize']) );
  @PQfmod              	:= GetProcAddress(hSqlLibModule, 'PQfmod');  			ASSERT( @PQfmod              	<>nil, Format(SErrFuncNotFound, ['PQfmod']) );
  @PQcmdStatus         	:= GetProcAddress(hSqlLibModule, 'PQcmdStatus');  		ASSERT( @PQcmdStatus         	<>nil, Format(SErrFuncNotFound, ['PQcmdStatus']) );
  @PQoidStatus         	:= GetProcAddress(hSqlLibModule, 'PQoidStatus');  		ASSERT( @PQoidStatus         	<>nil, Format(SErrFuncNotFound, ['PQoidStatus']) );
  @PQoidValue          	:= GetProcAddress(hSqlLibModule, 'PQoidValue');  		ASSERT( @PQoidValue          	<>nil, Format(SErrFuncNotFound, ['PQoidValue']) );
  @PQcmdTuples         	:= GetProcAddress(hSqlLibModule, 'PQcmdTuples');  		ASSERT( @PQcmdTuples         	<>nil, Format(SErrFuncNotFound, ['PQcmdTuples']) );
  @PQgetvalue          	:= GetProcAddress(hSqlLibModule, 'PQgetvalue');  		ASSERT( @PQgetvalue          	<>nil, Format(SErrFuncNotFound, ['PQgetvalue']) );
  @PQgetlength         	:= GetProcAddress(hSqlLibModule, 'PQgetlength');  		ASSERT( @PQgetlength         	<>nil, Format(SErrFuncNotFound, ['PQgetlength']) );
  @PQgetisnull         	:= GetProcAddress(hSqlLibModule, 'PQgetisnull');  		ASSERT( @PQgetisnull         	<>nil, Format(SErrFuncNotFound, ['PQgetisnull']) );
  @PQclear             	:= GetProcAddress(hSqlLibModule, 'PQclear');  			ASSERT( @PQclear             	<>nil, Format(SErrFuncNotFound, ['PQclear']) );
  @PQmakeEmptyPGresult 	:= GetProcAddress(hSqlLibModule, 'PQmakeEmptyPGresult');  	ASSERT( @PQmakeEmptyPGresult 	<>nil, Format(SErrFuncNotFound, ['PQmakeEmptyPGresult']) );
  @PQprint             	:= GetProcAddress(hSqlLibModule, 'PQprint');  			ASSERT( @PQprint             	<>nil, Format(SErrFuncNotFound, ['PQprint']) );
  @PQmblen             	:= GetProcAddress(hSqlLibModule, 'PQmblen');  			ASSERT( @PQmblen             	<>nil, Format(SErrFuncNotFound, ['PQmblen']) );
  @PQenv2encoding      	:= GetProcAddress(hSqlLibModule, 'PQenv2encoding');  		ASSERT( @PQenv2encoding      	<>nil, Format(SErrFuncNotFound, ['PQenv2encoding']) );

  @lo_open             	:= GetProcAddress(hSqlLibModule, 'lo_open');      		ASSERT( @lo_open     		<>nil, Format(SErrFuncNotFound, ['lo_open']) );
  @lo_close            	:= GetProcAddress(hSqlLibModule, 'lo_close');      		ASSERT( @lo_close      		<>nil, Format(SErrFuncNotFound, ['lo_close']) );
  @lo_read             	:= GetProcAddress(hSqlLibModule, 'lo_read');  	    		ASSERT( @lo_read       		<>nil, Format(SErrFuncNotFound, ['lo_read']) );
  @lo_write            	:= GetProcAddress(hSqlLibModule, 'lo_write');   	   	ASSERT( @lo_write              	<>nil, Format(SErrFuncNotFound, ['lo_write']) );
  @lo_lseek            	:= GetProcAddress(hSqlLibModule, 'lo_lseek');      		ASSERT( @lo_lseek              	<>nil, Format(SErrFuncNotFound, ['lo_lseek']) );
  @lo_creat            	:= GetProcAddress(hSqlLibModule, 'lo_creat');   	   	ASSERT( @lo_creat              	<>nil, Format(SErrFuncNotFound, ['lo_creat']) );
  @lo_tell             	:= GetProcAddress(hSqlLibModule, 'lo_tell');      		ASSERT( @lo_tell               	<>nil, Format(SErrFuncNotFound, ['lo_tell']) );
  @lo_unlink           	:= GetProcAddress(hSqlLibModule, 'lo_unlink');    	  	ASSERT( @lo_unlink             	<>nil, Format(SErrFuncNotFound, ['lo_unlink']) );
  @lo_import           	:= GetProcAddress(hSqlLibModule, 'lo_import');      		ASSERT( @lo_import             	<>nil, Format(SErrFuncNotFound, ['lo_import']) );
  @lo_export           	:= GetProcAddress(hSqlLibModule, 'lo_export');     	 	ASSERT( @lo_export             	<>nil, Format(SErrFuncNotFound, ['lo_export']) );

  @PQgetssl           	:= GetProcAddress(hSqlLibModule, 'PQgetssl');
end;

procedure ResetProcAddresses;
begin
  @PQconnectStart	:= nil;
  @PQconnectPoll 	:= nil;
  @PQconnectdb   	:= nil; 
  @PQsetdbLogin  	:= nil;
  @PQfinish      	:= nil; 
  @PQconndefaults	:= nil; 
  @PQconninfoFree	:= nil; 
  @PQresetStart  	:= nil;
  @PQresetPoll   	:= nil;
  @PQreset       	:= nil; 
  @PQrequestCancel   	:= nil; 
  @PQdb              	:= nil; 
  @PQuser            	:= nil; 
  @PQpass            	:= nil; 
  @PQhost            	:= nil; 
  @PQport            	:= nil; 
  @PQtty             	:= nil; 
  @PQoptions         	:= nil; 
  @PQstatus          	:= nil; 
  @PQerrorMessage    	:= nil; 
  @PQsocket          	:= nil; 
  @PQbackendPID      	:= nil;
  @PQclientEncoding  	:= nil; 
  @PQsetClientEncoding	:= nil;
  @PQgetssl           	:= nil; 
  @PQtrace            	:= nil; 
  @PQuntrace          	:= nil;
  @PQsetNoticeProcessor	:= nil; 
  @PQexec              	:= nil; 
  @PQnotifies          	:= nil; 
  @PQsendQuery         	:= nil;
  @PQgetResult         	:= nil; 
  @PQisBusy            	:= nil; 
  @PQconsumeInput      	:= nil; 
  @PQgetline           	:= nil; 
  @PQputline           	:= nil; 
  @PQgetlineAsync      	:= nil; 
  @PQputnbytes         	:= nil; 
  @PQendcopy           	:= nil; 
  @PQsetnonblocking    	:= nil;
  @PQisnonblocking     	:= nil;
  @PQflush             	:= nil;
  @PQresultStatus      	:= nil; 
  @PQresStatus         	:= nil; 
  @PQresultErrorMessage	:= nil;
  @PQntuples           	:= nil; 
  @PQnfields           	:= nil; 
  @PQbinaryTuples      	:= nil; 
  @PQfname             	:= nil; 
  @PQfnumber           	:= nil; 
  @PQftype             	:= nil; 
  @PQfsize             	:= nil; 
  @PQfmod              	:= nil; 
  @PQcmdStatus         	:= nil;
  @PQoidStatus         	:= nil; 
  @PQoidValue          	:= nil; 
  @PQcmdTuples         	:= nil; 
  @PQgetvalue          	:= nil;
  @PQgetlength         	:= nil; 
  @PQgetisnull         	:= nil; 
  @PQclear             	:= nil; 
  @PQmakeEmptyPGresult 	:= nil; 
  @PQprint             	:= nil;
  @PQmblen             	:= nil; 
  @PQenv2encoding      	:= nil; 
                          
  @lo_open             	:= nil;
  @lo_close            	:= nil;
  @lo_read             	:= nil;
  @lo_write            	:= nil;
  @lo_lseek            	:= nil;
  @lo_creat            	:= nil;
  @lo_tell             	:= nil;
  @lo_unlink           	:= nil;
  @lo_import           	:= nil;
  @lo_export           	:= nil;
end;

procedure LoadSqlLib;
var
  sFileName:string;
begin
  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 0) then begin
      sFileName := SrvApiDLLs[stPostgreSQL];
      hSqlLibModule := LoadLibrary( PChar( sFileName ) );
      if (hSqlLibModule = 0) then
        raise Exception.CreateFmt(SErrLibLoading, [ sFileName ]);
      Inc(SqlLibRefCount);
      SetProcAddresses;
      dwLoadedFileVer := GetFileVersion(sFileName);
    end else
      Inc(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;

procedure FreeSqlLib;
begin
  if SqlLibRefCount = 0 then
    Exit;

  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 1) then begin
      if FreeLibrary(hSqlLibModule) then
        hSqlLibModule := 0
      else
        raise Exception.CreateFmt(SErrLibUnloading, [ SrvApiDLLs[stPostgreSQL] ]);
      Dec(SqlLibRefCount);
      ResetProcAddresses;
      dwLoadedFileVer := 0;
    end else
      Dec(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;


{ TIPgDatabase }
constructor TIPgDatabase.Create(ADatabase: TSDDatabase);
begin
  inherited Create(ADatabase);
end;

destructor TIPgDatabase.Destroy;
begin
  inherited;
end;

function TIPgDatabase.CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet;
begin
  Result := TIPgDataSet.Create( ADataSet );
end;

procedure TIPgDatabase.Check(pgconn: PPGconn);
var
  E: ESDPgSQLError;
  szErrMsg: PChar;
begin
  ResetIdleTimeOut;
  if PQstatus( pgconn ) = CONNECTION_OK then
    Exit;
  ResetBusyState;

  szErrMsg := PQerrorMessage( pgconn );

  E := ESDPgSQLError.Create(-1, -1, szErrMsg, 0);
  raise E;
end;

procedure TIPgDatabase.CheckRes(pgres: PPGresult);
var
  E: ESDPgSQLError;
  szErrMsg: PChar;
  rs: Integer;
begin
  ResetIdleTimeOut;
  rs := PQresultStatus( pgres );
  if (rs = PGRES_EMPTY_QUERY) or (rs = PGRES_COMMAND_OK) or (rs = PGRES_TUPLES_OK) then
    Exit;
  ResetBusyState;

  szErrMsg := PQresultErrorMessage( pgres );

  E := ESDPgSQLError.Create(-1, -1, szErrMsg, 0);
  raise E;
end;

procedure TIPgDatabase.ExecStmt(const sStmt: string);
var
  res: PPGresult;begin
  res := PQexec( PgConn, PChar(sStmt) );
  try
    CheckRes( res );
  finally
    PQclear( res );
  end;
end;

procedure TIPgDatabase.GetStmtResult(const Stmt: string; List: TStrings);
var
  res: PPGresult;
  st: TExecStatusType;
  RecCount, RecNo: Integer;
  szValue: PChar;
begin
  RecCount := 0;
  RecNo    := 0;

  res := PQexec( PgConn, PChar(Stmt) );
  try
    CheckRes( res );

    st := PQresultStatus( res );
    if st = PGRES_TUPLES_OK then begin
      RecCount := PQntuples( res );
      RecNo  := 0;
    end;

    while RecNo < RecCount do begin
      szValue := PQgetvalue( res, RecNo, 0 );
      List.Add( StrPas(szValue) );
      Inc( RecNo );
    end;
  finally
    PQclear( res );
  end;
end;

function TIPgDatabase.GetClientVersion: LongInt;
begin
  Result := dwLoadedFileVer;
end;

function TIPgDatabase.GetServerVersion: LongInt;
begin
  Result := VersionStringToDWORD( GetVersionString );
end;

function TIPgDatabase.GetVersionString: string;
var
  res: PPGresult;
  szVer: PChar;
begin
  res := PQexec( PgConn, 'select version()');
  try
    CheckRes( res );
    szVer := PQgetvalue( res, 0, 0 );

    Result := szVer;
  finally
    PQclear( res );
  end;
end;

function TIPgDatabase.GetPgConn: PPGconn;
begin
  ASSERT( Assigned(FHandle) and Assigned(PPgSrvInfo(FHandle^.SrvInfo)),
  	'TIPgDatabase.GetPgConn' );

  Result := PPgSrvInfo(FHandle^.SrvInfo)^.PgConn;
end;

function TIPgDatabase.GetHandle: PSDHandleRec;
begin
  Result := FHandle;
end;

procedure TIPgDatabase.SetHandle(AHandle: PSDHandleRec);
begin
  LoadSqlLib;

  AllocHandle;

  PPgSrvInfo(FHandle^.SrvInfo)^.PgConn :=
  	PPgSrvInfo(AHandle^.SrvInfo)^.PgConn;
end;

procedure TIPgDatabase.AllocHandle;
var
  s: PPgSrvInfo;
begin
  ASSERT( not Assigned(FHandle), 'TIPgDatabase.AllocHandle' );

  New(FHandle);
  FillChar( FHandle^, SizeOf(FHandle^), $0 );
  FHandle^.SrvType := Ord( Database.ServerType );

  New(s);
  FillChar( s^, SizeOf(s^), $0 );
  FHandle^.SrvInfo := s;
end;

procedure TIPgDatabase.FreeHandle;
begin
  if Assigned(FHandle) then begin
    if Assigned(FHandle^.SrvInfo) then begin
      Dispose( PPgSrvInfo(FHandle^.SrvInfo) );
      PPgSrvInfo(FHandle^.SrvInfo) := nil;
    end;
    Dispose( FHandle );
    FHandle := nil;
  end;
end;

procedure TIPgDatabase.Logon(const sRemoteDatabase, sUserName, sPassword: string);
var
  sSrvName, sDbName, sPortNo: string;
begin
  try
    LoadSqlLib;

    AllocHandle;
	// sRemoteDatabase can be equal 'srv:port:db'
    sSrvName := ExtractServerName(sRemoteDatabase);
    sDbName := ExtractDatabaseName(sRemoteDatabase);
    sPortNo := ExtractServerName(sDbName);
    if sPortNo = sDbName then
      sPortNo := '';

    PPgSrvInfo(FHandle^.SrvInfo)^.PgConn :=
    	PQsetdblogin(PChar(sSrvName), PChar(sPortNo), nil, nil, PChar(sDbName),
        	PChar(sUserName), PChar(sPassword));
    Check( PPgSrvInfo(FHandle^.SrvInfo)^.PgConn );

  except
    Logoff(False);

    raise;
  end;
end;

procedure TIPgDatabase.Logoff(Force: Boolean);
begin
  if Assigned(FHandle) and Assigned(FHandle^.SrvInfo) and
     Assigned( PPgSrvInfo(FHandle^.SrvInfo)^.PgConn )
  then begin
    if Database.InTransaction then
      PQclear( PQexec( PgConn, 'ROLLBACK' ) );

    PQfinish( PPgSrvInfo(FHandle^.SrvInfo)^.PgConn );
    PPgSrvInfo(FHandle^.SrvInfo)^.PgConn := nil;
  end;

  FreeHandle;
  FreeSqlLib;
end;

procedure TIPgDatabase.SetDefaultParams;
begin

end;

procedure TIPgDatabase.StartTransaction;
begin
  ExecStmt( 'BEGIN' );
end;

procedure TIPgDatabase.Commit;
begin
  ExecStmt( 'COMMIT' );
end;

procedure TIPgDatabase.Rollback;
begin
  ExecStmt( 'ROLLBACK' );
end;

procedure TIPgDatabase.SetTransIsolation(Value: TSDTransIsolation);
var
  sStmt: string;
begin
  sStmt := 'SET SESSION CHARACTERISTICS AS TRANSACTION ISOLATION LEVEL ';

  if Value = tiRepeatableRead then
    sStmt := sStmt + 'SERIALIZABLE'
  else
    sStmt := sStmt + 'READ COMMITTED';
  ExecStmt( sStmt );
end;

procedure TIPgDatabase.GetStoredProcNames(List: TStrings);
begin
  GetStmtResult('select distinct proname from pg_proc', List);
end;

procedure TIPgDatabase.GetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings);
var
  sTblTypes: string;
begin
  sTblTypes := '''r'', ''v''';
  if SystemTables then
    sTblTypes := sTblTypes + ',''t''';
  GetStmtResult(
  	Format('select relname from pg_class where relkind in (%s)', [sTblTypes]), List);
end;

procedure TIPgDatabase.GetTableFieldNames(const TableName: string; List: TStrings);
begin
  GetStmtResult(
  	'select attname from pg_class tbl, pg_attribute col '+
	'where tbl.oid = col.attrelid and col.attnum > 0 and'+
        ' upper(relname) = upper(''' + TableName + ''')',
        List);
end;

function TIPgDatabase.FieldDataType(ExtDataType: Integer): TFieldType;
begin
  case ExtDataType of
    BOOLOID: 	// BOOLEAN|BOOL
      Result := ftBoolean;
    BYTEAOID: 	// BYTEA - variable-length string, binary values escaped
      Result := ftBlob;
    TEXTOID: 	// TEXT - variable-length string, no limit specified
      Result := ftMemo;
    CHAROID, 	// CHAR - single character
    BPCHAROID, 	// BPCHAR - char(length), blank-padded string, fixed storage length
    NAMEOID, 	// NAME - 31-character type for storing system identifiers
    VARCHAROID: // VARCHAR(?) - varchar(length), non-blank-padded string, variable storage length
      Result := ftString;
    INT8OID: 	// INT8|BIGINT - ~18 digit integer, 8-byte storage
{$IFDEF SD_VCL4}
      Result := ftLargeInt;
{$ELSE}
      Result := ftInteger;
{$ENDIF}
    FLOAT4OID, 	// FLOAT4 - single-precision floating point number, 4-byte storage
    FLOAT8OID, 	// FLOAT8 - double-precision floating point number, 8-byte storage
    CASHOID, 	// $d,ddd.cc, money
    NUMERICOID: // NUMERIC(x,y) - numeric(precision, decimal), arbitrary precision number
      Result := ftFloat;
    INT2OID: 	// INT2|SMALLINT - -32 thousand to 32 thousand, 2-byte storage
      Result := ftSmallInt;
    INT4OID, 	// INT4|INTEGER - -2 billion to 2 billion integer, 4-byte storage
    XIDOID, 	// transaction id
    CIDOID, 	// command identifier type, sequence in transaction id
    OIDOID: 	// OID - object identifier(oid), maximum 4 billion
      Result := ftInteger;
    DATEOID:    // DATE - ANSI SQL date
      Result := ftDate;
    TIMEOID: 	// TIME - hh:mm:ss, ANSI SQL time
      Result := ftTime;
    ABSTIMEOID, // ABSTIME - absolute, limited-range date and time (Unix system time)
    TIMESTAMPOID,// TIMESTAMP - date and time
    TIMESTAMPOID_v72,
    TIMETZOID: 	// TIME WITH TIMEZONE - hh:mm:ss, ANSI SQL time
      Result := ftDateTime;
    UNKNOWNOID, // Unknown
    INT2VECTOROID, 	// array of INDEX_MAX_KEYS int2 integers, used in system tables
    REGPROCOID, // registered procedure
    TIDOID, 	// (Block, offset), physical location of tuple
    OIDVECTOROID, 	// array of INDEX_MAX_KEYS oids, used in system tables
    POINTOID, 	// geometric point '(x, y)'
    LSEGOID, 	// geometric line segment '(pt1,pt2)'
    PATHOID, 	// geometric path '(pt1,...)'
    BOXOID, 	// geometric box '(lower left,upper right)'
    POLYGONOID, // geometric polygon '(pt1,...)'
    LINEOID, 	// geometric line '(pt1,pt2)'
    RELTIMEOID, // relative, limited-range time interval (Unix delta time)
    TINTERVALOID, // (abstime,abstime), time interval
    INTERVALOID,// @ <number> <units>, time interval
    CIRCLEOID, 	// geometric circle '(center,radius)'
	// locale specific
    INETOID, 	// IP address/netmask, host address, netmask optional
    CIDROID, 	// network IP address/netmask, network address
    829, 	// XX:XX:XX:XX:XX:XX, MAC address
    ZPBITOID, 	// BIT(?) - fixed-length bit string
    VARBITOID: 	// BIT VARING(?) - variable-length bit string
      Result := ftString;
    else
      Result := ftString;
  end;
end;

function TIPgDatabase.NativeDataSize(FieldType: TFieldType): Word;
begin
  Result := 0;
end;

function TIPgDatabase.RequiredCnvtFieldType(FieldType: TFieldType): Boolean;
begin
  Result := FieldType in
  	[ftInteger, {$IFDEF SD_VCL4}ftLargeInt,{$ENDIF} ftSmallInt,
         ftFloat,
         ftDate, ftTime, ftDateTime,
         ftBoolean];
end;


{ TIPgDataSet }

constructor TIPgDataSet.Create(ADataSet: TSDDataSet);
begin
  inherited Create(ADataSet);

  FStmt		:= '';
  FBindStmt	:= '';
  FRowsAffected := -1;

  FRecCount	:= 0;
  FCurrRec	:= -1;

  FHandle 	:= nil;
end;

destructor TIPgDataSet.Destroy;
begin
  Disconnect(False);

  inherited;
end;

procedure TIPgDataSet.Check(pgres: PPGresult);
begin
  SrvDatabase.CheckRes( pgres );
end;

function TIPgDataSet.GetExecuted: Boolean;
begin
  Result := Assigned( FHandle );
end;

function TIPgDataSet.GetHandle: PSDCursor;
begin
  Result := FHandle;
end;

function TIPgDataSet.GetSrvDatabase: TIPgDatabase;
begin
  Result := (inherited SrvDatabase) as TIPgDatabase;
end;

procedure TIPgDataSet.Disconnect(Force: Boolean);
begin
  // nothing
end;

procedure TIPgDataSet.Execute;
begin
  if DataSet is TSDStoredProc
  then SpExecute
  else QExecute;
end;

procedure TIPgDataSet.QBindParams;
begin
  // nothing
end;

procedure TIPgDataSet.QExecute;
begin
  if not Executed then
    InternalQExecute;
end;

function TIPgDataSet.QGetRowsAffected: Integer;
begin
  Result := FRowsAffected;
end;

procedure TIPgDataSet.QPrepareSQL(Value: PChar);
begin
  FStmt		:= Value;
  FBindStmt	:= '';
end;

function TIPgDataSet.ResultSetExists: Boolean;
begin
  Result := True;
end;

procedure TIPgDataSet.SetSelectBuffer;
begin
  // nothing
end;

{ Convert TDateTime to string for SQL statement }
function TIPgDataSet.CnvtDateTime2SqlString(Value: TDateTime): string;
  { Format date in ISO format 'yyyy-mm-dd' }
  function FormatSqlDate(Value: TDateTime): string;
  var
    Year, Month, Day: Word;
  begin
    DecodeDate(Value, Year, Month, Day);
    Result := Format('%.4d-%.2d-%.2d', [Year, Month, Day]);
  end;
  { Format time in ISO format 'hh:nn:ss' }
  function FormatSqlTime(Value: TDateTime): string;
  var
    Hour, Min, Sec, MSec: Word;
  begin
    DecodeTime(Value, Hour, Min, Sec, MSec);
    Result := Format('%.2d:%.2d:%.2d', [Hour, Min, Sec]);
  end;

begin
  if Trunc(Value) <> 0 then
    Result := FormatSqlDate(Value)
  else
    Result := '1899-12-30';

  if Frac(Value) <> 0 then begin
    if Result <> '' then
      Result := Result + ' ';
    Result := Result + FormatSqlTime(Value);
  end;
  Result := '''' + Result + '''';
end;

{ Convert float value to string with '.' delimiter }
function TIPgDataSet.CnvtFloat2SqlString(Value: Double): string;
var
  i: Integer;
begin
  Result := FloatToStr(Value);
  if DecimalSeparator <> '.' then begin
    i := AnsiPos(DecimalSeparator,Result);
    if i <> 0 then Result[i] := '.';
  end;
end;

{ Characters are converted as (tested with PostgreSQL 7.1)
  <'>  -> <\'>
  <$0> -> <\\000>
  <\>  -> <\\\\> 	 - it's probably this (and previous) character is converted twice
  <X>  -> <X>            - printable characters
  <X>  -> <\nnn> (octal) - non-printable characters
}
function TIPgDataSet.CnvtByteString2String(Value: string): string;
  function IsPrint(Ch: Char): Boolean;
  begin
    Result := Byte(Ch) in [$20..$7E];
  end;
var
  i, BufSize: Integer;
  CurPtr: PChar;
begin
  BufSize := 0;

  i := 1;
  while i <= Length(Value) do begin
    if Value[i] = '''' then
      BufSize := BufSize + 2
    else if Value[i] = #$00 then
      BufSize := BufSize + 5
    else if IsPrint(Value[i]) and (Value[i] <> '\') then
      BufSize := BufSize + 1
    else
      BufSize := BufSize + 4;
    Inc(i);
  end;

  SetLength( Result, BufSize );
  FillChar( PChar(Result)^, BufSize, $0 );
  CurPtr := PChar(Result);

  for i:=1 to Length(Value) do begin
    if Value[i] = '''' then begin
      CurPtr[0] := '\';
      CurPtr[1] := '''';
      CurPtr := CurPtr + 2;
    end else if Value[i] = #$00 then begin
      CurPtr[0] := '\';
      CurPtr[1] := '\';
      CurPtr[2] := '0';
      CurPtr[3] := '0';
      CurPtr[4] := '0';
      CurPtr := CurPtr + 5;
    end else if Value[i] = '\' then begin
      CurPtr[0] := '\';
      CurPtr[1] := '\';
      CurPtr[2] := '\';
      CurPtr[3] := '\';
      CurPtr := CurPtr + 4;
    end else if IsPrint(Value[i]) then begin
      CurPtr[0] := Value[i];
      Inc(CurPtr);
    end else begin
      CurPtr[0] := '\';
      CurPtr[1] := Char( Byte('0') + ((Byte(Value[i]) and $C0) shr 6) );
      CurPtr[2] := Char( Byte('0') + ((Byte(Value[i]) and $38) shr 3) );
      CurPtr[3] := Char( Byte('0') + ((Byte(Value[i]) and $07)) );
      CurPtr := CurPtr + 4;
    end;
  end;

  Result := '''' + Result + '''';
end;


{ Non-printable and <\> characters are inserted as <\nnn> (octal)
  and <'> as <\'>
}
function TIPgDataSet.CnvtString2ByteString(Value: string): string;
  function IsDigit(Ch: Char): Boolean;
  begin
    Result := Ch in ['0'..'9'];
  end;
var
  i, CharCount: Integer;
  CurPtr: PChar;
begin
  CharCount := 0;
  i := 1;
  while i <= Length(Value) do begin
    if Value[i] = '\' then begin
      Inc( i );

      if i > Length(Value) then
        Break;

      	// <\\> = <\>, <\'> = <'>
      if Value[i] in ['\', ''''] then
        Inc( CharCount )
      else if IsDigit(Value[i]) and ((i+2)<=Length(Value)) and
              IsDigit(Value[i+1]) and IsDigit(Value[i+2])
      then begin
        i := i + 2;
        Inc( CharCount );
      end else
        ASSERT(False, Format('Invalid octal number format(offset:%d) in Blob field', [i]));

    end else	// any other character
      Inc( CharCount );

    Inc( i );
  end;

  SetLength( Result, CharCount );
  FillChar( PChar(Result)^, CharCount, $0 );
  CurPtr := PChar(Result);
  i := 1;
  while i <= Length(Value) do begin
    if Value[i] = '\' then begin
      Inc( i );

      if i > Length(Value) then
        Break;

      	// <\\> = <\>, <\'> = <'>
      if Value[i] in ['\', ''''] then
        CurPtr^ := Value[i]
      else if IsDigit(Value[i]) and ((i+2)<=Length(Value)) and
              IsDigit(Value[i+1]) and IsDigit(Value[i+2])
      then begin
        CurPtr^ := Char(
        		( (Byte(Value[i])   - Byte('0')) shl 6 ) or
                	( (Byte(Value[i+1]) - Byte('0')) shl 3 ) or
                	(  Byte(Value[i+2]) - Byte('0') )
        	);
        i := i + 2
      end;
    end else	// any other character
      CurPtr^ := Value[i];

    Inc( i );
    Inc( CurPtr );
  end;
end;

{ <\> and <'> are inserted as <\\> and <\'> }
function TIPgDataSet.CnvtString2EscapeString(Value: string): string;
var
  i: Integer;
begin
  Result := Value;

  i := 1;
  while i <= Length(Result) do begin
    if Result[i] in ['''', '\'] then begin
      System.Insert('\', Result, i);
      Inc( i );
    end;

    Inc( i );
  end;

  Result := '''' + Result + '''';
end;

procedure TIPgDataSet.InternalQBindParams;
const
  ParamPrefix	= ':';
  SqlNullValue	= 'NULL';
  QuoteChar	= '"';	// for surroundings of the parameter's name, which can include, for example, spaces
var
  i: Integer;
  sFullParamName, sValue: string;
begin
  FBindStmt := FStmt;

  with Query do
    for i:=Query.ParamCount-1 downto 0 do begin
      	// set parameter value
      if Params[i].IsNull then begin
        sValue := SqlNullValue
      end else begin
        case Params[i].DataType of
{$IFDEF SD_VCL4}
          ftLargeInt,
{$ENDIF}
          ftInteger,
          ftSmallint,
          ftWord:	sValue := Params[i].AsString;
          ftBoolean:
            if Params[i].AsBoolean
            then sValue := '1' else sValue := '0';
          ftBytes,
          ftVarBytes,
          ftBlob:
            sValue := CnvtByteString2String(Params[i].Value);
          ftDate,
          ftTime,
          ftDateTime:	sValue := CnvtDateTime2SqlString(Params[i].AsDateTime);
          ftCurrency,
          ftFloat:	sValue := CnvtFloat2SqlString(Params[i].AsFloat);
        else
          sValue := CnvtString2EscapeString(Params[i].AsString);
        end;
      end;
	// change a parameter's name on a value of the parameter
      sFullParamName := ParamPrefix + Params[i].Name;
      if not ReplaceString( False, sFullParamName, sValue, FBindStmt ) then begin
      	// if parameter's name is enclosed in double quotation marks
        sFullParamName := Format( '%s%s%s%s', [ParamPrefix, QuoteChar, Params[i].Name, QuoteChar] );
        ReplaceString( False, sFullParamName, sValue, FBindStmt );
      end;
    end;
end;

procedure TIPgDataSet.InternalQExecute;
var
  szRowsAffected: PChar;
  st: Integer;
begin
  FRowsAffected := -1;
	// set parameter's values
  InternalQBindParams;

  FHandle := PQexec( SrvDatabase.PgConn, PChar(FBindStmt) );
  Check( FHandle );

  st := PQresultStatus( FHandle );
	// if a command does not return data: INSERT, UPDATE
  if st = PGRES_COMMAND_OK then begin
    szRowsAffected := PQcmdTuples( FHandle );
    FRowsAffected  := StrToIntDef( szRowsAffected, 0 );
  end else if st = PGRES_TUPLES_OK then begin
    FRecCount := PQntuples( FHandle );
    FCurrRec  := -1;
  end;
end;

function TIPgDataSet.FetchNextRow: Boolean;
begin
  Inc( FCurrRec );
  Result := FCurrRec < FRecCount;

  if Result and (BlobFieldCount > 0) then
    FetchBlobFields;
end;

procedure TIPgDataSet.CloseResultSet;
begin
  if Assigned( FHandle ) then
    PQclear( FHandle );

  FBindStmt := '';
  FHandle := nil;

  FRecCount	:= 0;
  FCurrRec	:= -1;
end;

procedure TIPgDataSet.GetFieldDescs(Descs: TSDFieldDescList);
var
  Col, NumCols: Integer;
  pFieldDesc: PSDFieldDesc;
  ft: TFieldType;
  szFldName: PChar;
  FldSize: Integer;
begin
  if not Executed then
    InternalQExecute;

  NumCols := PQnfields( FHandle );
  for Col:=0 to NumCols-1 do begin
    szFldName := PQfname( FHandle, Col );
    ft := SrvDatabase.FieldDataType( PQftype(FHandle, Col) );
    if ft = ftUnknown then
      DatabaseErrorFmt( SBadFieldType, [StrPas(szFldName)] );

    FldSize := PQfsize( FHandle, Col );
    if (ft = ftString) and (FldSize = -1) then begin
      FldSize := PQfmod( FHandle, Col );

      if FldSize <= 0 then
        FldSize := dsMaxStringSize;
    end;

    New( pFieldDesc );
    with pFieldDesc^ do begin
      FieldName	:= StrPas( szFldName );
      FieldNo	:= Col+1;
      DataType	:= ft;
      if FldSize > 0 then
        Size	:= FldSize;
//      Precision	:= FldPtr^.decimals;
  	// null values are not permitted for the column (Required = True)
//      Required	:= (FldPtr^.flags and NOT_NULL_FLAG) <> 0;
    end;
    Descs.Add( pFieldDesc );
  end;
end;

function TIPgDataSet.CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean;
var
  OutData: Pointer;
  szValue: PChar;
  FldSize: Integer;
  dtValue: TDateTime;
begin
  if PQgetisnull( FHandle, FCurrRec, AField.FieldNo-1 ) = 0 then
    PFieldInfo(OutBuf)^.FetchStatus := indVALUE
  else
    PFieldInfo(OutBuf)^.FetchStatus := indNULL;

  if PFieldInfo(OutBuf)^.FetchStatus <> indNULL then begin
    OutData	:= Pointer( Integer(OutBuf) + SizeOf(TFieldInfo) );
    szValue 	:= PQgetvalue( FHandle, FCurrRec, AField.FieldNo-1 );
    FldSize	:= PQgetlength( FHandle, FCurrRec, AField.FieldNo-1 );
    if SrvDatabase.RequiredCnvtFieldType(AField.DataType) then begin
      case AField.DataType of
{$IFDEF SD_VCL4}
      ftLargeInt:
         begin
           Int64(OutData^) := StrToInt64Def( szValue, 0 );
           PFieldInfo(OutBuf)^.DataSize := SizeOf(Int64);
         end;
{$ENDIF}
      ftInteger, ftSmallInt:
        begin
          Integer(OutData^) := StrToIntDef( szValue, 0 );
          PFieldInfo(OutBuf)^.DataSize := SizeOf(Integer);
        end;
      ftFloat:
        begin
          Double(OutData^) := SqlStrToFloatDef(szValue, 0);
          PFieldInfo(OutBuf)^.DataSize := SizeOf(Double);
        end;
      ftBoolean:
        begin
          Word(OutData^) := Word( SqlStrToBoolean(szValue) );
          PFieldInfo(OutBuf)^.DataSize := SizeOf(Word);
        end;
      ftTime:
        begin
          dtValue := SqlTimeToDateTime(szValue);
          TDateTimeRec(OutData^).Time := DateTimeToTimeStamp( dtValue ).Time;
          PFieldInfo(OutBuf)^.DataSize := SizeOf(TDateTimeRec);
        end;
      ftDate, ftDateTime:
        begin
          dtValue := SqlDateToDateTime(szValue);
          if AField.DataType in [ftDate] then
            TDateTimeRec(OutData^).Date := DateTimeToTimeStamp(dtValue).Date
          else
            TDateTimeRec(OutData^).DateTime := TimeStampToMSecs( DateTimeToTimeStamp(dtValue) );
          PFieldInfo(OutBuf)^.DataSize := SizeOf(TDateTimeRec);
        end;
      end;	// end of case
    end else begin
      if IsBlobType(AField.DataType) then begin
        Result := False;
        Exit;
      end;

      if AField.DataType = ftString
      then Move( szValue^, OutData^, FldSize + 1 )
      else Move( szValue^, OutData^, FldSize );
      PFieldInfo(OutBuf)^.DataSize := FldSize;
    end;
  end;

  Result := True;
end;

function TIPgDataSet.ReadBlob(FieldNo: Integer; var BlobData: string): Longint;
var
  BlobSize: LongInt;
  DataPtr: PChar;
begin
  BlobData := '';

  BlobSize:= PQgetlength( FHandle, FCurrRec, FieldNo-1 );

  if BlobSize > 0 then begin
    DataPtr := PQgetvalue( FHandle, FCurrRec, FieldNo-1 );
    if Assigned(DataPtr) then
      if FieldByNumber(FieldNo).DataType = ftMemo then begin
        SetLength(BlobData, BlobSize);
        System.Move(DataPtr^, PChar(BlobData)^, BlobSize);
      end else	// Blob
        BlobData := CnvtString2ByteString(DataPtr);
  end;

  Result := Length(BlobData);
end;

initialization
  hSqlLibModule := 0;
  SqlLibRefCount:= 0;
  SqlLibLock 	:= TCriticalSection.Create;
finalization
  while SqlLibRefCount > 0 do
    FreeSqlLib;
  SqlLibLock.Free;
end.

