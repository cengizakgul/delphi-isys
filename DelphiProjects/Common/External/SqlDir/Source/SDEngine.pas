
{*******************************************************}
{							}
{       Delphi SQLDirect Component Library		}
{       SQLDirect Data Access				}
{                                                       }
{       Copyright (c) 1997,2002 by Yuri Sheino		}
{                                                       }
{*******************************************************}

unit SDEngine;

interface

{$I SqlDir.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Db, SyncObjs, DbCommon,
{$IFDEF SD_VCL6}
  Variants,
{$ENDIF}
  SDConsts;

const
	{ constants for database's parameters }
  szUSERNAME    = 'USER NAME';
  szPASSWORD    = 'PASSWORD';

  szAPPNAME     = 'APPLICATION NAME';
  szHOSTNAME    = 'HOST NAME';
  szMAXCURSORS  = 'MAX CURSORS';
  szQUOTEDIDENT	= 'QUOTED IDENTIFIER';		// enable or disable to use of quoted identifiers (for MSSQL and Sybase)
  szSINGLECONN	= 'SINGLE CONNECTION';		// whether to use a single database process/connection (for MSSQL)
  szTDSPACKETSIZE   = 'TDS PACKET SIZE';	// TDS packet size (for Sybase)
  szRTRIMCHAROUTPUT = 'RTRIM CHAR OUTPUT';	// trim trailing spaces in the output of CHAR datatype for Oracle
  szENABLEINTS	= 'ENABLE INTEGERS';		// converts NUMERIC database field with no scale into integer field in an application
  szENABLEBCD	= 'ENABLE BCD';			// converts NUMERIC database field with scale (1-4) into BCD field in an application to exclude the rounding errors
  szSQLDialect	= 'SQL Dialect';		// client SQL Dialect(1,2,3) for Interbase
  szLOCALCHARSET= 'LOCAL CHARSET';		// local charset for Interbase
  szROLENAME	= 'ROLE NAME';			// role name for Interbase and Oracle (connect as SYSDBA, SYSOPER)
  szAUTOCOMMIT	= 'AUTOCOMMIT';			// whether to use autocommit when transaction processing is not used explicitly
  szLOGINTIMEOUT= 'LOGIN TIMEOUT';		// number of seconds to wait for a login request
  szCMDTIMEOUT	= 'COMMAND TIMEOUT';		// number of seconds to wait for any request on the connection to complete
  szNEWPASSWORD	= 'NEW PASSWORD';		// it is used when a server returns 'Password expired' error
  szSERVERPORT	= 'SERVER PORT';		// number of a server port(non-default) for MySQL
  szXACONN	= 'XA CONNECTION';   		// whether to use RemoteDatabase as a service name of transaction monitor
  szMAXFIELDNAMELEN = 'MAXFIELDNAMELEN'; 	// max length of field names

  	{ aren't used now }
  szSERVERNAME	= 'SERVER NAME';
  szDATABASENAME= 'DATABASE NAME';

  SDE_ERR_NONE		= 0;
  SDE_ERR_UPDATEABORT	= -1;

  MAXFIELDNAMELEN	= 50;	// max field name length, which is stored by SQLDirect components 

  { TSDDataSet flags }

  dsfOpened     = 0;
  dsfPrepared   = 1;
  dsfExecSQL    = 2;
  dsfTable      = 3;
  dsfFieldList  = 4;
  dsfStoredProc = 5;
  dsfExecProc	= 6;
  dsfProcDesc   = 7;
  dsfProvider   = 10;

  { Field status codes (see TFieldInfo.FetchStatus) }
  indTRUNC	= -2;                   { Value has been truncated }
  indNULL	= -1;                   { Value is NULL }
  indVALUE	= 0;

  { BLOB subtypes }
  fldstUNKNOWN	= $00;
  fldstMEMO	= $10;              	{ Text Memo }
  fldstBINARY	= $11;              	{ Binary data }
  fldstHMEMO	= $12;              	{ CLOB }
  fldstHBINARY	= $13;              	{ BLOB }
  fldstHCFILE	= $14;              	{ CFILE }
  fldstHBFILE	= $15;              	{ BFILE }

  ServerDelimiters	= ':@';		{ server delimiters for TSDDatabase.RemoteDatabase property }

type
	{ Cursor types }
  PSDCursor	= Pointer;

  TSDEResult		= SmallInt;         // result of the C/API-functions

  TBlobData 		= string;
  TBlobDataArray 	= array[0..0] of TBlobData;
  PBlobDataArray 	= ^TBlobDataArray;

  TSDServerType = (stSQLBase, stOracle, stSQLServer, stSybase,
  		   stDB2, stInformix, stODBC, stInterbase,
                   stMySQL, stPostgreSQL);

  TSDSession 		= class;  
  TSDDatabase 		= class;
  TSDDataSet  		= class;
  TSDQuery  		= class;
  
  TSDFieldDescList 	= class;

  TISrvDatabase 	= class;
  TISrvDataSet 		= class;

{$IFDEF SD_VCL4}
  TSDHelperParam  	= TParam;
  TSDHelperParams  	= TParams;
  TSDHelperParamType  	= TParamType;
{$ELSE}
  TSDParam  	= class;
  TSDParams  	= class;
  TSDParamType	= (ptUnknown, ptInput, ptOutput, ptInputOutput, ptResult);

  TSDHelperParam  	= TSDParam;
  TSDHelperParams  	= TSDParams;
  TSDHelperParamType  	= TSDParamType;
{$ENDIF}

{ ESDNoResultSet }

  ESDNoResultSet = class(EDatabaseError);

{ ESDEngineError }

  ESDEngineError = class(EDatabaseError)
  private
    FNativeError,
    FErrorCode,
    FErrorPos: LongInt;
  public
    constructor Create(AErrorCode, ANativeError: LongInt; const Msg: string; AErrorPos: LongInt);
    constructor CreateDefPos(AErrorCode, ANativeError: LongInt; const Msg: string);
    property ErrorCode:   LongInt read FErrorCode;
    property NativeError: LongInt read FNativeError;
    property ErrorPos: LongInt read FErrorPos;
  end;

{ TSessionList }

  TSDSessionList = class(TObject)
  private
    FSessions: TThreadList;
    FSessionNumbers: TBits;
    procedure AddSession(ASession: TSDSession);
    procedure CloseAll;
    function GetCount: Integer;
    function GetSession(Index: Integer): TSDSession;
    function GetSessionByName(const SessionName: string): TSDSession;
  public
    constructor Create;
    destructor Destroy; override;
    function FindDatabase(const DatabaseName: string): TSDDatabase;
    function FindSession(const SessionName: string): TSDSession;
    procedure GetSessionNames(List: TStrings);
    function OpenSession(const SessionName: string): TSDSession;
    property Count: Integer read GetCount;
    property List[const SessionName: string]: TSDSession read GetSessionByName;
    property Sessions[Index: Integer]: TSDSession read GetSession; default;
  end;

{ TSDSession }

  TSDSession = class(TComponent)
  private
    FDatabases: TList;				// active or inactive database components, which have an associated SessionName
    FDefault: Boolean;
    FKeepConnections: Boolean;			// for temporary database objects in run-time
    FDBParams: TList;				// remote database parameters could be used, for example, for TSDDatabase with an empty Params property 
    FAutoSessionName: Boolean;
    FUpdatingAutoSessionName: Boolean;
    FSessionName: string;
    FSessionNumber: Integer;
    FLockCount: Integer;

    FActive: Boolean;
    FStreamedActive: Boolean;
    FSQLHourGlass: Boolean;
    FSQLWaitCursor: TCursor;
    procedure AddDatabase(Value: TSDDatabase);
    procedure CheckInactive;
    procedure ClearDBParams;
    function DoFindDatabase(const DatabaseName: string; AOwner: TComponent): TSDDatabase;
    function DoOpenDatabase(const DatabaseName: string; AOwner: TComponent): TSDDatabase;
    function GetActive: Boolean;
    function GetDatabase(Index: Integer): TSDDatabase;
    function GetDatabaseCount: Integer;
    procedure LockSession;
    procedure MakeCurrent;
    procedure RemoveDatabase(Value: TSDDatabase);
    function SessionNameStored: Boolean;
    procedure SetActive(Value: Boolean);
    procedure SetAutoSessionName(Value: Boolean);
    procedure SetSessionName(const Value: string);
    procedure SetSessionNames;
    procedure StartSession(Value: Boolean);
    procedure UnlockSession;
    procedure InternalAddDatabase(const ARemoteDatabase: string; AServerType: TSDServerType; List: TStrings);
    procedure UpdateAutoSessionName;
    procedure ValidateAutoSession(AOwner: TComponent; AllSessions: Boolean);
  protected
    procedure Loaded; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetName(const NewName: TComponentName); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Close;
    procedure CloseDatabase(Database: TSDDatabase);
    function FindDatabase(const DatabaseName: string): TSDDatabase;
    procedure GetDatabaseNames(List: TStrings);
    procedure GetDatabaseParams(const ARemoteDatabase: string; AServerType: TSDServerType; List: TStrings);
    procedure GetStoredProcNames(const DatabaseName: string; List: TStrings);
    procedure GetTableFieldNames(const DatabaseName, TableName: string; List: TStrings);
    procedure GetTableNames(const DatabaseName, Pattern: string; SystemTables: Boolean; List: TStrings);
    procedure Open;
    function OpenDatabase(const DatabaseName: string): TSDDatabase;
    property DatabaseCount: Integer read GetDatabaseCount;
    property Databases[Index: Integer]: TSDDatabase read GetDatabase;
    property SQLWaitCursor: TCursor read FSQLWaitCursor write FSQLWaitCursor;
  published
    property Active: Boolean read GetActive write SetActive default False;
    property AutoSessionName: Boolean read FAutoSessionName write SetAutoSessionName default False;
    property KeepConnections: Boolean read FKeepConnections write FKeepConnections default True;
    property SessionName: string read FSessionName write SetSessionName stored SessionNameStored;
    property SQLHourGlass: Boolean read FSQLHourGlass write FSQLHourGlass default True;
  end;

{ TSDDatabase }

  TSDDatabaseParam = (spMaxBindName, spMaxClientName, spMaxConnectString,
  	spMaxDatabaseName, spMaxErrorMessage, spMaxErrorText, spMaxExtErrorMessage,
        spMaxJoinedTables, spLongIdentifiers, spShortIdentifiers,
        spMaxUserName, spMaxPasswordString, spMaxServerName,
        spMaxFieldName, spMaxTableName, spMaxSPName, spMaxFullTableName, spMaxFullSPName);

  TSDTransIsolation = (tiDirtyRead, tiReadCommitted, tiRepeatableRead);

  TSDLoginEvent = procedure(Database: TSDDatabase; LoginParams: TStrings) of object;

  TSDHandleRec	= record
    SrvType: Integer;
    SrvInfo: PSDCursor;
  end;

  PSDHandleRec	= ^TSDHandleRec;


{ TSDCustomDatabase }
{$IFNDEF SD_VCL5}
  TSDCustomDatabase = class(TComponent)
  private
    FDataSets: TList;
    FStreamedConnected: Boolean;	// Connected value while it's reading from stream
    FAfterConnect: TNotifyEvent;
    FAfterDisconnect: TNotifyEvent;
    FBeforeConnect: TNotifyEvent;
    FBeforeDisconnect: TNotifyEvent;
  protected
    FLoginPrompt: Boolean;  
    procedure DoConnect; virtual;
    procedure DoDisconnect; virtual;
    function GetConnected: Boolean; virtual;
    function GetDataSet(Index: Integer): TDataSet; virtual;
    function GetDataSetCount: Integer; virtual;
    procedure Loaded; override;
    procedure RegisterClient(Client: TObject); virtual;
    procedure SetConnected(Value: Boolean); virtual;
    property StreamedConnected: Boolean read FStreamedConnected write FStreamedConnected;
    procedure UnRegisterClient(Client: TObject); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Open; virtual;
    procedure Close;
    property Connected: Boolean read GetConnected write SetConnected default False;
    property DataSets[Index: Integer]: TDataSet read GetDataSet;
    property DataSetCount: Integer read GetDataSetCount;
    property LoginPrompt: Boolean read FLoginPrompt write FLoginPrompt default False;
    property AfterConnect: TNotifyEvent read FAfterConnect write FAfterConnect;
    property BeforeConnect: TNotifyEvent read FBeforeConnect write FBeforeConnect;
    property AfterDisconnect: TNotifyEvent read FAfterDisconnect write FAfterDisconnect;
    property BeforeDisconnect: TNotifyEvent read FBeforeDisconnect write FBeforeDisconnect;
  end;
{$ELSE}
  TSDCustomDatabase = class(TCustomConnection);
{$ENDIF}

  TSDThreadTimer = class(TThread)
  private
    FDatabase: TSDDatabase;
    FInterval: Integer;
    FOnTimer: TNotifyEvent;
    procedure SetInterval(Value: Integer);
    procedure SetOnTimer(Value: TNotifyEvent);
  protected
    procedure Timer; dynamic;
    procedure Execute; override;
  public
    constructor Create(ADatabase: TSDDatabase; CreateSuspended: Boolean);
    destructor Destroy; override;
    property Interval: Integer read FInterval write SetInterval default 1000;
    property OnTimer: TNotifyEvent read FOnTimer write SetOnTimer;
  end;

  TSDDatabase = class(TSDCustomDatabase)
  private
    FTransIsolation: TSDTransIsolation;
    FKeepConnection: Boolean;
    FTemporary: Boolean;
    FAcquiredHandle: Boolean;
    FParams: TStrings;
    FRefCount: Integer;			// dataset's reference count(connected dataset) to the current database
    FSession: TSDSession;
    FSessionName: string;
    FDatabaseName: string;

    FInTransaction: Boolean;
    FRemoteDatabase: string;          	// real database name on server
    FServerType: TSDServerType;		// server type
    FSrvDatabase: TISrvDatabase;

    FIdleTimeOut: Integer;
    FTimer: TSDThreadTimer;		// to process disconnecting when idle time has been elapsed
    FIdleTimeoutStarted: Boolean;
    FIsConnectionBusy: Boolean;		// True, in case of server long-running process (for example: a complex SQL statement is executed)

    FClientVersion: LongInt;		// client version as MajorVer.MinorVer (-1 and 0 - unloaded and undefined)
    FServerVersion: LongInt;		// server version as MajorVer.MinorVer
    FVersion: string;			// server info with full version and name

    FOnLogin: TSDLoginEvent;
    procedure CheckActive;
    procedure CheckInactive;
    procedure CheckInTransaction;
    procedure CheckNotInTransaction;
    procedure CheckDatabaseName;
    procedure CheckRemoteDatabase(var Password: string);
    procedure CheckSessionName(Required: Boolean);
    function GetHandle: PSDCursor;
    function GetIdleTimeOut: Integer;
    function GetIsSQLBased: Boolean;
    function GetVersion: string;
    function GetServerMajor: Word;
    function GetServerMinor: Word;
    function GetClientMajor: Word;
    function GetClientMinor: Word;
    procedure IdleTimerHandler(Sender: TObject);
    procedure Login(LoginParams: TStrings);
    procedure ResetServerInfo;
    procedure SetDatabaseName(const Value: string);
    procedure SetHandle(Value: PSDCursor);
    procedure SetIdleTimeOut(Value: Integer);
    procedure SetKeepConnection(Value: Boolean);
    procedure SetParams(Value: TStrings);
    procedure SetRemoteDatabase(const Value: string);
    procedure SetServerType(Value: TSDServerType);
    procedure SetSessionName(const Value: string);
    procedure SetTransIsolation(Value: TSDTransIsolation);
    procedure UpdateTimer(NewTimeout: Integer);
  protected
    property SrvDatabase: TISrvDatabase read FSrvDatabase;
    property AcquiredHandle: Boolean read FAcquiredHandle;
    procedure InitSrvDatabase(const ADatabaseName, AUserName, APassword: string);
    procedure DoneSrvDatabase;
    procedure InternalClose(Force: Boolean);
  	// call server API
    function ISrvFieldDataType(ExtDataType: Integer): TFieldType;
    procedure ISrvGetStoredProcNames(List: TStrings);
    procedure ISrvGetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings);
    procedure ISrvGetTableFieldNames(const TableName: string; List: TStrings);
    function ISrvNativeDataSize(FieldType: TFieldType): Word;
    function ISrvNativeDataType(FieldType: TFieldType): Byte;
    function ISrvNumberDataType(FieldType: TFieldType; Precision, Scale: Integer): TFieldType;
    function ISrvParamValue(Value: TSDDatabaseParam): Integer;
    function ISrvRequiredCnvtFieldType(FieldType: TFieldType): Boolean;
  protected
    procedure DoConnect; override;
    procedure DoDisconnect; override;
    function GetConnected: Boolean; override;
    function GetDataSet(Index: Integer): TDataSet; override;
    function GetSDDataSet(Index: Integer): TSDDataSet; virtual;
    procedure Loaded; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ApplyUpdates(const DataSets: array of TSDDataSet);
    procedure CloseDataSets;
    procedure Commit;
    procedure ForceClose;
    procedure Rollback;
    procedure StartTransaction;
    procedure ValidateName(const Name: string);
    property DataSets[Index: Integer]: TSDDataSet read GetSDDataSet;
    property Handle: PSDCursor read GetHandle write SetHandle;
    property InTransaction: Boolean read FInTransaction;
    property IsSQLBased: Boolean read GetIsSQLBased;
    property Session: TSDSession read FSession;
    property Temporary: Boolean read FTemporary write FTemporary;
    property ClientMajor: Word read GetClientMajor;
    property ClientMinor: Word read GetClientMinor;
    property ServerMajor: Word read GetServerMajor;
    property ServerMinor: Word read GetServerMinor;
    property Version: string read GetVersion;
  published
{$IFDEF SD_VCL5}
    property Connected;
    property LoginPrompt default True;
{$ELSE}
    property Connected: Boolean read GetConnected write SetConnected default False;
    property LoginPrompt: Boolean read FLoginPrompt write FLoginPrompt default True;
{$ENDIF}
    property DatabaseName: string read FDatabaseName write SetDatabaseName;
    property IdleTimeOut: Integer read GetIdleTimeOut write SetIdleTimeOut;    
    property KeepConnection: Boolean read FKeepConnection write SetKeepConnection default True;
    property Params: TStrings read FParams write SetParams;
    property RemoteDatabase: string read FRemoteDatabase write SetRemoteDatabase;
    property ServerType: TSDServerType read FServerType write SetServerType default stSQLBase;
    property SessionName: string read FSessionName write SetSessionName;
    property TransIsolation: TSDTransIsolation read FTransIsolation write SetTransIsolation default tiReadCommitted;
    property AfterConnect;
    property AfterDisconnect;
    property BeforeConnect;
    property BeforeDisconnect;
    property OnLogin: TSDLoginEvent read FOnLogin write FOnLogin;
  end;

{ TSDResultSet }

  PCacheRecInfo = ^TCacheRecInfo;
  TCacheRecInfo = record
    Applied: Boolean;
    CurRec: PChar;
    OldRec: PChar;
  end;

  TSDResultSet = class(TList)
  private
    FDataSet: TSDDataSet;
    FPosition: Integer;		// current record index
    FAllInCache: Boolean;
    FDeletedCount: Integer;	// number of rescords, which are marked as deleted
    function GetAppliedRecords(Index: Integer): Boolean;
    function GetCurRecords(Index: Integer): PChar;
    function GetOldRecords(Index: Integer): PChar;
    function GetUpdateStatusRecords(Index: Integer): TUpdateStatus;
    function GetFilterActivated: Boolean;
    function GetRecordCount: Integer;
    procedure SetAppliedRecords(Index: Integer; Value: Boolean);
    procedure SetCurRecords(Index: Integer; Value: PChar);
    procedure SetOldRecords(Index: Integer; Value: PChar);
    procedure SetUpdateStatusRecords(Index: Integer; Value: TUpdateStatus);
  private
    function AddRecord(RecBuf: PChar): Integer;
    procedure CopyRecBuf(var Source, Dest);
    procedure ClearCacheItem(Index: Integer);
    procedure DeleteCacheItem(Index: Integer);
    function FetchRecord: Boolean;
    function GetCurrRecord: Boolean;
    function GetNextRecord: Boolean;
    function GetPriorRecord: Boolean;
    function GetRecord(Buffer: PChar; GetMode: TGetMode): Boolean;
    function GotoNextRecord: Boolean;
    function IsVisibleRecord(Index: Integer): Boolean;
    procedure DeleteRecord(RecBuf: PChar);
    function IsInsertedRecord(Index: Integer): Boolean;
    procedure InsertRecord(RecBuf: PChar; Append, SetCurrent: Boolean);
    procedure ModifyRecord(RecBuf: PChar);
    procedure ModifyBlobData(Field: TField; RecBuf: PChar; const Value: TBlobData);
    function RecordFilter(RecBuf: Pointer): Boolean;
    function UpdatesCancel: Integer;
    function UpdatesCancelCurrent: Integer;
    function UpdatesCommit: Integer;
    function UpdatesPrepare: Integer;
    function UpdatesRollback: Integer;
  public
    constructor Create(ADataSet: TSDDataSet);
    destructor Destroy; override;
    procedure Clear; {$IFDEF SD_VCL4} override; {$ENDIF} {$IFDEF SD_C3} override; {$ENDIF}
    procedure FetchAll;
    function FindNextRecord: Boolean;
    function FindPriorRecord: Boolean;
    procedure SetToBegin;
    procedure SetToEnd;
    property AllInCache: Boolean read FAllInCache;
    property AppliedRecords[Index: Integer]: Boolean read GetAppliedRecords write SetAppliedRecords;
    property CurRecords[Index: Integer]: PChar read GetCurRecords write SetCurRecords; default;
    property OldRecords[Index: Integer]: PChar read GetOldRecords write SetOldRecords;
    property UpdateStatusRecords[Index: Integer]: TUpdateStatus read GetUpdateStatusRecords write SetUpdateStatusRecords;
    property RecNo: Integer read FPosition;
    property RecordCount: Integer read GetRecordCount;
    property FilterActivated: Boolean read GetFilterActivated;
  end;

{ TSDDataSet }

  PRecInfo = ^TRecInfo;
  TRecInfo = record
    RecordNumber: LongInt;
    UpdateStatus: TUpdateStatus; 	// (usUnmodified, usModified, usInserted, usDeleted)
    BookmarkFlag: TBookmarkFlag;
  end;

  PFieldInfo = ^TFieldInfo;
  TFieldInfo = record
    FetchStatus:LongInt;		{ Fetch Status Code of field (signed 16-bit) }
    DataSize: 	LongInt;		{ Length of the data received (for Oracle, it uses for BLOB-field) }
  end;

  TIntArray	= array[0..0] of Integer;
  PIntArray	= ^TIntArray;

  TDelayedUpdCmd = (                 { Op types for Delayed Update cursor }
    DelayedUpdCommit,                { Commit the updates }
    DelayedUpdCancel,                { Cancel the updates (all record changes) }
    DelayedUpdCancelCurrent,         { Cancel the Current Rec Change }
    DelayedUpdPrepare,               { Phase1 of 2 phase commit }
    DelayedUpdRollback               { Rollback the updates, but keep the changes: Phase2 of 2 phase rollback }
  );

  TDSFlags = set of 0..15;		{ set of TSDDataSet flags}

{$IFNDEF SD_VCL5}	// to exclude ambiguous reference in BCB5+, when set OnUpdateRecord event handler
  TUpdateAction = (uaFail, uaAbort, uaSkip, uaRetry, uaApplied);
{$ENDIF}
  TUpdateKinds	= set of TUpdateKind;
  TUpdateRecordTypes = set of (rtModified, rtInserted, rtDeleted, rtUnmodified);
  TUpdateErrorEvent = procedure(DataSet: TDataSet; E: EDatabaseError;
    UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction) of object;
  TUpdateRecordEvent = procedure(DataSet: TDataSet; UpdateKind: TUpdateKind;
    var UpdateAction: TUpdateAction) of object;

  TSDDataSetUpdateObject = class(TComponent)
  protected
    function GetDataSet: TSDDataSet; virtual; abstract;
    procedure SetDataSet(ADataSet: TSDDataSet); virtual; abstract;
    procedure Apply(UpdateKind: TUpdateKind); virtual; abstract;
    property DataSet: TSDDataSet read GetDataSet write SetDataSet;
  end;


  TSDDataSet = class(TDataSet)
  private
    FDSFlags: TDSFlags;
    FDatabase: TSDDatabase;
    FDatabaseName: string;
    FSessionName: string;
    FSrvDataSet: TISrvDataSet;
    FRecCache: TSDResultSet;
    FFieldDescs: TSDFieldDescList;
    FFieldBufOffs: PIntArray;		// Offsets to field's buffer in a record buffer

    FPreservation: Boolean;		// Context cursor preservation (only for SQLBase)

    FCachedUpdates: Boolean;
    FCacheBlobs: Boolean;
    FUniDirectional: Boolean;
    FDetachOnFetchAll: Boolean;		// drop a server cursor, when entire result set is on client side, to minimize server resources
    FFilterActivated: Boolean;		// if Filter active
    FFilterBuffer: PChar;
    FForceClosing: Boolean;		// hide database exceptions in this case
    FRecordSize: Integer;  		// record size (without additional info and BLOB-pointers)
    FRecBufSize: Integer;  		// record size with additional info
    FBlobCacheOffs: Integer;		// BLOB-cache offset in record buffer
    FRecInfoOffs: Integer; 		// TRecInfo offset in record buffer
    FBookmarkOffs: Integer;             // TBookmark offset in record buffer
    FEnableUpdateKinds: TUpdateKinds;	// what updates(modify, insert, delete) are enabled
    FUpdateObject: TSDDataSetUpdateObject;
    FUpdateRecordTypes: TUpdateRecordTypes;	// set of record types, which are visible in a dataset in CachedUpdates mode
    FOnUpdateError: TUpdateErrorEvent;
    FOnUpdateRecord: TUpdateRecordEvent;
    procedure CheckDBSessionName;
    procedure ClearBlobCache(RecBuf: PChar);
    function FieldIsNull(FieldBuf: Pointer): Boolean;
    procedure FieldSetNull(FieldBuf: Pointer; bNull: Boolean);
    function GetActiveRecBuf(var RecBuf: PChar): Boolean;
    function GetBlobData(Field: TField; Buffer: PChar): TBlobData;
    function GetFieldBufOffs(Index: Integer): Integer;
    function GetHandle: PSDCursor;
    function GetOldRecord: PChar;
    function GetServerType: TSDServerType;
    function GetDBSession: TSDSession;
    function GetEnableUpdateKinds: TUpdateKinds;
    function GetUpdatesPending: Boolean;
    function GetUpdateRecordSet: TUpdateRecordTypes;
    procedure InitBufferPointers;
    function RecordFilter(RecBuf: Pointer): Boolean;
    procedure SetBlobData(Field: TField; Buffer: PChar; Value: TBlobData);
    procedure SetDatabaseName(const Value: string);
    procedure SetDetachOnFetchAll(Value: Boolean);
    procedure SetSessionName(const Value: string);
    procedure SetPreservation(Value: Boolean);
    procedure SetEnableUpdateKinds(Value: TUpdateKinds);
    procedure SetUpdateRecordSet(RecordTypes: TUpdateRecordTypes);
    procedure SetUpdateObject(Value: TSDDataSetUpdateObject);
  protected
    property RecCache: TSDResultSet read FRecCache;
    property SrvDataSet: TISrvDataSet read FSrvDataSet;

    procedure ISrvCloseResultSet;
    procedure ISrvExecute;
    procedure ISrvFreeSelectBuffer;
    procedure ISrvInitFieldDefs;
    procedure ISrvSetSelectBuffer;
    procedure ISrvSetPreservation(Value: Boolean);
    function ISrvReadBlob(FieldNo: Integer; var BlobData: string; Count: LongInt): Longint;
    function ISrvWriteBlob(FieldNo: Integer; const Buffer; Count: LongInt): Longint;
    function ISrvWriteBlobByName(Name: string; const Buffer; Count: LongInt): Longint;
  private
    procedure CacheInit;
    procedure CacheDone;
{$IFDEF SD_VCL4}
    function CacheTempBuffer: PChar;	// it's used in BlockReadNext method only(now)
{$ENDIF}
  protected
{$IFDEF SD_VCL5}
    { IProviderSupport }
    procedure PSEndTransaction(Commit: Boolean); override;
    function PSExecuteStatement(const ASQL: string; AParams: TParams; ResultSet: Pointer = nil): Integer; override;
    function PSGetQuoteChar: string; override;
    function PSGetUpdateException(E: Exception; Prev: EUpdateError): EUpdateError; override;
    function PSInTransaction: Boolean; override;
    function PSIsSQLBased: Boolean; override;
    function PSIsSQLSupported: Boolean; override;
    procedure PSReset; override;
    procedure PSStartTransaction; override;
    function PSUpdateRecord(UpdateKind: TUpdateKind; Delta: TDataSet): Boolean; override;
{$ENDIF}
    procedure ActivateFilters;
    function AllocRecordBuffer: PChar; override;
    procedure DestroySrvDataSet(Force: Boolean);
    procedure ForceClose;
    procedure FreeRecordBuffer(var Buffer: PChar); override;
{$IFNDEF SD_VCL5}
    function BCDToCurr(BCD: Pointer; var Curr: Currency): Boolean; override;
    function CurrToBCD(const Curr: Currency; BCD: Pointer; Precision, Decimals: Integer): Boolean; override;
{$ENDIF}
    procedure CheckCachedUpdateMode;
    procedure CheckDatabaseName;
    procedure ClearCalcFields(Buffer: PChar); override;
    procedure CloseCursor; override;
    procedure ClearFieldDefs;
    function CreateHandle: TISrvDataSet; virtual;
    procedure DataEvent(Event: TDataEvent; Info: Longint); override;
    procedure DeactivateFilters;
    procedure DestroyHandle; virtual;
    function FindRecord(Restart, GoForward: Boolean): Boolean; override;
    procedure GetBookmarkData(Buffer: PChar; Data: Pointer); override;
    function GetBookmarkFlag(Buffer: PChar): TBookmarkFlag; override;
    function GetCanModify: Boolean; override;
    function GetFieldBuffer(Field: TField; RecBuf: Pointer): Pointer;
    function GetFieldDataSize(Field: TField): Integer;
{$IFNDEF SD_VCL4}
    function GetFieldData(Field: TField; Buffer: Pointer): Boolean; override;
{$ENDIF}
    function GetRecNo: Integer; override;
    function GetRecord(Buffer: PChar; GetMode: TGetMode; DoCheck: Boolean): TGetResult; override;
    function GetRecordCount: LongInt; override;
    function GetRecordSize: Word; override;
    procedure InitRecord(Buffer: PChar); override;
    procedure InitResultSet;
    procedure InternalAddRecord(Buffer: Pointer; Append: Boolean); override;
    procedure InternalClose; override;
    procedure InternalDelete; override;
    procedure InternalFirst; override;
    procedure InternalGotoBookmark(Bookmark: TBookmark); override;
    procedure InternalHandleException; override;
    procedure InternalInitFieldDefs; override;
    procedure InternalInitRecord(Buffer: PChar); override;
    procedure InternalLast; override;
    procedure InternalOpen; override;
    procedure InternalPost; override;
    procedure InternalRefresh; override;
    procedure InternalSetToRecord(Buffer: PChar); override;
    function IsCursorOpen: Boolean; override;
    procedure DoneResultSet;
    function LocateRecord(const KeyFields: string; const KeyValues: Variant;
      Options: TLocateOptions; SyncCursor: Boolean): Integer;
    function MapsToIndex(Fields: TList; CaseInsensitive: Boolean): Boolean;
    procedure SetConnectionState(IsBusy: Boolean);
    procedure SetBookmarkData(Buffer: PChar; Data: Pointer); override;
    procedure SetBookmarkFlag(Buffer: PChar; Value: TBookmarkFlag); override;
{$IFDEF SD_VCL4}
    procedure BlockReadNext; override;
    procedure SetBlockReadSize(Value: Integer); override;
{$ENDIF}
    procedure SetFieldData(Field: TField; Buffer: Pointer); override;
    procedure SetFilterData(const Text: string; Options: TFilterOptions);
    procedure SetFiltered(Value: Boolean); override;
    procedure SetFilterOptions(Value: TFilterOptions); override;
    procedure SetFilterText(const Value: string); override;
    procedure OpenCursor(InfoQuery: Boolean); override;
    procedure PrepareCursor; virtual; abstract;
    procedure ExecuteCursor; virtual;
    function ProcessUpdates(UpdCmd: TDelayedUpdCmd): Integer;
    function NativeFieldDataSize(Field: TField): Word;
    function SetDSFlag(Flag: Integer; Value: Boolean): Boolean; virtual;
    procedure SetOnFilterRecord(const Value: TFilterRecordEvent); override;
    property DSFlags: TDSFlags read FDSFlags;
    property ServerType: TSDServerType read GetServerType;
    property FieldBufOffs[Index: Integer]: Integer read GetFieldBufOffs;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Disconnect; virtual;
    procedure ApplyUpdates;
    function BookmarkValid(Bookmark: TBookmark): Boolean; override;
    procedure CancelUpdates;
    procedure CommitUpdates;
    procedure RollbackUpdates;
    function CompareBookmarks(Bookmark1, Bookmark2: TBookmark): Integer; override;
    function CreateBlobStream(Field: TField; Mode: TBlobStreamMode): TStream; override;
    procedure Detach;
    procedure FetchAll;
    function OpenDatabase: TSDDatabase;
    procedure CloseDatabase(Database: TSDDatabase);
{$IFDEF SD_VCL4}
    function GetFieldData(Field: TField; Buffer: Pointer): Boolean; overload; override;
    function GetFieldData(FieldNo: Integer; Buffer: Pointer): Boolean; overload; override;
{$ENDIF}
    function IsSequenced: Boolean; override;
    function Locate(const KeyFields: string; const KeyValues: Variant;
      Options: TLocateOptions): Boolean; override;
    function Lookup(const KeyFields: string; const KeyValues: Variant;
      const ResultFields: string): Variant; override;
    procedure RevertRecord;
    function UpdateStatus: TUpdateStatus; {$IFDEF SD_VCL4} override; {$ENDIF}
    property CacheBlobs: Boolean read FCacheBlobs write FCacheBlobs default True;
    property Database: TSDDatabase read FDatabase;
    property DBSession: TSDSession read GetDBSession;
    property EnableUpdateKinds: TUpdateKinds read GetEnableUpdateKinds write SetEnableUpdateKinds;
    property Handle: PSDCursor read GetHandle;
    property UniDirectional: Boolean read FUniDirectional write FUniDirectional default False;
    property UpdateObject: TSDDataSetUpdateObject read FUpdateObject write SetUpdateObject;
    property UpdatesPending: Boolean read GetUpdatesPending;
    property UpdateRecordTypes: TUpdateRecordTypes read GetUpdateRecordSet write SetUpdateRecordSet;
  published
    property DatabaseName: string read FDatabaseName write SetDatabaseName;
    property DetachOnFetchAll: Boolean read FDetachOnFetchAll write SetDetachOnFetchAll default False;
    property Preservation: Boolean read FPreservation write SetPreservation default True;
    property SessionName: string read FSessionName write SetSessionName;
    property OnUpdateError: TUpdateErrorEvent read FOnUpdateError write FOnUpdateError;
    property OnUpdateRecord: TUpdateRecordEvent read FOnUpdateRecord write FOnUpdateRecord;
  published
    property Active;
    property AutoCalcFields;
    property Filter;
    property Filtered;
    property FilterOptions;
    property BeforeOpen;
    property AfterOpen;
    property BeforeClose;
    property AfterClose;
    property BeforeInsert;
    property AfterInsert;
    property BeforeEdit;
    property AfterEdit;
    property BeforePost;
    property AfterPost;
    property BeforeCancel;
    property AfterCancel;
    property BeforeDelete;
    property AfterDelete;
    property BeforeScroll;
    property AfterScroll;
    property OnCalcFields;
    property OnDeleteError;
    property OnEditError;
    property OnFilterRecord;
    property OnNewRecord;
    property OnPostError;
  end;

{ TSDQuery }

  TSDQuery = class(TSDDataSet)
  private
    FDataLink: TDataLink;
    FParams: TSDHelperParams;
    FPrepared: Boolean;
    FSQL: TStrings;
    FText: string;
    FParamCheck: Boolean;
    FRowsAffected: Integer;
    function BindBufferSize(Param: TSDHelperParam): Integer;
    procedure CreateParams(List: TSDHelperParams; const Value: PChar);
    function GetRowsAffected: Integer;
    procedure QueryChanged(Sender: TObject);
    procedure RefreshParams;
    procedure SetDataSource(Value: TDataSource);
    procedure SetQuery(Value: TStrings);
    procedure SetParamsFromCursor;
    procedure SetParamsList(Value: TSDHelperParams);
    procedure SetPrepare(Value: Boolean);
    procedure SetPrepared(Value: Boolean);
{$IFDEF SD_VCL4}
  private
    procedure ReadParamData(Reader: TReader);
    procedure WriteParamData(Writer: TWriter);
  protected
    procedure DefineProperties(Filer: TFiler); override;
{$ENDIF}
  protected
    procedure ISrvAllocBindBuffer;
    procedure ISrvBindParams;
    procedure ISrvFreeBindBuffer;
    function ISrvGetRowsAffected: Integer;
    procedure ISrvPrepareSQL(Value: PChar);
  protected
{$IFDEF SD_VCL5}
    { IProviderSupport }
    procedure PSExecute; override;
    function PSGetParams: TParams; override;
    function PSGetTableName: string; override;
    procedure PSSetCommandText(const CommandText: string); override;
    procedure PSSetParams(AParams: TParams); override;
{$ENDIF}
    procedure BindParams;
    procedure DoBeforeOpen; override;
    function GetDataSource: TDataSource; override;
    function GetParamsCount: Word;
    procedure InternalClose; override;
    procedure InternalOpen; override;
    procedure PrepareCursor; override;
    procedure ExecuteCursor; override;
    procedure UnPrepareCursor;
    function SetDSFlag(Flag: Integer; Value: Boolean): Boolean; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Disconnect; override;
    procedure ExecSQL;
{$IFDEF SD_VCL4}
    procedure GetDetailLinkFields(MasterFields, DetailFields: TList); override;
{$ENDIF}
    procedure ParseSQL(FldInfo, TblInfo: TStrings);
    function ParamByName(const Value: string): TSDHelperParam;
    procedure Prepare;
    procedure UnPrepare;
    property Prepared: Boolean read FPrepared write SetPrepared;
    property ParamCount: Word read GetParamsCount;
    property Text: string read FText;
    property RowsAffected: Integer read GetRowsAffected;
  published
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property ParamCheck: Boolean read FParamCheck write FParamCheck default True;
    property Params: TSDHelperParams read FParams write SetParamsList {$IFDEF SD_VCL4} stored False {$ENDIF};
    property SQL: TStrings read FSQL write SetQuery;
    property UniDirectional;
    property UpdateObject;
  end;

{ TSDUpdateSQL }

  TSDUpdateSQL = class(TSDDataSetUpdateObject)
  private
    FDataSet: TSDDataSet;
    FQueries: array[TUpdateKind] of TSDQuery;
    FSQLText: array[TUpdateKind] of TStrings;
    function GetQuery(UpdateKind: TUpdateKind): TSDQuery;
    function GetSQL(UpdateKind: TUpdateKind): TStrings;
    function GetSQLIndex(Index: Integer): TStrings;
    procedure SetSQL(UpdateKind: TUpdateKind; Value: TStrings);
    procedure SetSQLIndex(Index: Integer; Value: TStrings);
  protected
    function GetDataSet: TSDDataSet; override;
    procedure SetDataSet(ADataSet: TSDDataSet); override;
    procedure SQLChanged(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Apply(UpdateKind: TUpdateKind); override;
    procedure ExecSQL(UpdateKind: TUpdateKind);
    procedure SetParams(UpdateKind: TUpdateKind);
    property DataSet;
    property Query[UpdateKind: TUpdateKind]: TSDQuery read GetQuery;
    property SQL[UpdateKind: TUpdateKind]: TStrings read GetSQL write SetSQL;
  published
    property ModifySQL: TStrings index 0 read GetSQLIndex write SetSQLIndex;
    property InsertSQL: TStrings index 1 read GetSQLIndex write SetSQLIndex;
    property DeleteSQL: TStrings index 2 read GetSQLIndex write SetSQLIndex;
  end;

{$IFNDEF SD_VCL4}		{ for Delphi 3 & C++ Builder 3 }

{ TSDParam }

  TSDParam = class(TPersistent)
  private
    FParamList: TSDParams;
    FData: Variant;
    FName: string;
    FDataType: TFieldType;
    FNull: Boolean;
    FBound: Boolean;
    FParamType: TSDParamType;
    procedure InitValue;
  protected
    procedure AssignParam(Param: TSDParam);
    procedure AssignTo(Dest: TPersistent); override;
    function GetAsBCD: Currency;
    function GetAsBoolean: Boolean;
    function GetAsDateTime: TDateTime;
    function GetAsFloat: Double;
    function GetAsInteger: Longint;
    function GetAsMemo: string;
    function GetAsString: string;
    function GetAsVariant: Variant;
    function GetParamName: string;
    function IsEqual(Value: TSDParam): Boolean;
    procedure SetAsBCD(Value: Currency);
    procedure SetAsBlob(Value: TBlobData);
    procedure SetAsBoolean(Value: Boolean);
    procedure SetAsCurrency(Value: Double);
    procedure SetAsDate(Value: TDateTime);
    procedure SetAsDateTime(Value: TDateTime);
    procedure SetAsFloat(Value: Double);
    procedure SetAsInteger(Value: Longint);
    procedure SetAsMemo(const Value: string);
    procedure SetAsSmallInt(Value: LongInt);
    procedure SetAsString(const Value: string);
    procedure SetAsTime(Value: TDateTime);
    procedure SetAsVariant(Value: Variant);
    procedure SetAsWord(Value: LongInt);
    procedure SetDataType(Value: TFieldType);
    procedure SetParamName(const Value: string);
    procedure SetText(const Value: string);
  public
    constructor Create(AParamList: TSDParams; AParamType: TSDParamType);
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure AssignField(Field: TField);
    procedure AssignFieldValue(Field: TField; const Value: Variant);
    procedure Clear;
    procedure GetData(Buffer: Pointer);
    function GetDataSize: Integer;
    procedure SetData(Buffer: Pointer);
    procedure SetBlobData(Buffer: Pointer; Size: Integer);
    procedure LoadFromFile(const FileName: string; BlobType: TBlobType);
    procedure LoadFromStream(Stream: TStream; BlobType: TBlobType);
    property Name: string read GetParamName write SetParamName;
    property DataType: TFieldType read FDataType write SetDataType;
    property AsBCD: Currency read GetAsBCD write SetAsBCD;
    property AsBlob: TBlobData read GetAsString write SetAsBlob;
    property AsBoolean: Boolean read GetAsBoolean write SetAsBoolean;
    property AsCurrency: Double read GetAsFloat write SetAsCurrency;
    property AsDate: TDateTime read GetAsDateTime write SetAsDate;
    property AsDateTime: TDateTime read GetAsDateTime write SetAsDateTime;
    property AsFloat: Double read GetAsFloat write SetAsFloat;
    property AsInteger: LongInt read GetAsInteger write SetAsInteger;
    property AsMemo: string read GetAsMemo write SetAsMemo;
    property AsString: string read GetAsString write SetAsString;
    property AsSmallInt: LongInt read GetAsInteger write SetAsSmallInt;
    property AsTime: TDateTime read GetAsDateTime write SetAsTime;
    property AsWord: LongInt read GetAsInteger write SetAsWord;
    property IsNull: Boolean read FNull;
    property ParamType: TSDParamType read FParamType write FParamType;
    property Bound: Boolean read FBound write FBound;
    property Text: string read GetAsString write SetText;
    property Value: Variant read GetAsVariant write SetAsVariant;
  end;

{ TSDParams }

  TSDParams = class(TPersistent)
  private
    FItems: TList;
    function GetParam(Index: Word): TSDParam;
    function GetParamValue(const ParamName: string): Variant;
    function GetVersion: Word;
    procedure ReadBinaryData(Stream: TStream);
    procedure SetParamValue(const ParamName: string; const Value: Variant);
    procedure WriteBinaryData(Stream: TStream);
  protected
    procedure AssignTo(Dest: TPersistent); override;
    procedure DefineProperties(Filer: TFiler); override;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure AssignValues(Value: TSDParams);
    procedure AddParam(Value: TSDParam);
    procedure RemoveParam(Value: TSDParam);
    function CreateParam(FldType: TFieldType; const ParamName: string; ParamType: TSDParamType): TSDParam;
    function Count: Integer;
    procedure Clear;
    procedure GetParamList(List: TList; const ParamNames: string);
    function IsEqual(Value: TSDParams): Boolean;
    function ParamByName(const Value: string): TSDParam;
    property Items[Index: Word]: TSDParam read GetParam; default;
    property ParamValues[const ParamName: string]: Variant read GetParamValue write SetParamValue;
  end;
{$ENDIF}	{ for Delphi 3 & C++ Builder }

{ TSDBlobStream }

  TSDBlobStream = class(TStream)
  private
    FField: TBlobField;
    FDataSet: TSDDataSet;
    FBuffer: PChar;    		// record buffer
    FMode: TBlobStreamMode;
    FFieldNo: Integer;
    FOpened: Boolean;
    FModified: Boolean;
    FPosition: LongInt;
    FBlobData: TBlobData;
    FBlobSize: LongInt;
    FCached: Boolean;		// if BLOB into memory
    FCacheSize: LongInt;
    function GetBlobSize: LongInt;
  public
    constructor Create(Field: TBlobField; Mode: TBlobStreamMode);
    destructor Destroy; override;
    function Read(var Buffer; Count: Longint): Longint; override;
    function Write(const Buffer; Count: Longint): Longint; override;
    function Seek(Offset: Longint; Origin: Word): Longint; override;
    procedure Truncate;
  end;

{ TSDStoredProc }

  TSDStoredProc = class(TSDDataSet)
  private
    FProcName: string;
    FParams: TSDHelperParams;
    FOverLoad: Word;
    FPrepared: Boolean;
    FQueryMode: Boolean;
    function BindBufferSize(Param: TSDHelperParam): Integer;
    procedure PrepareProc;
    procedure SetParamsList(Value: TSDHelperParams);
{$IFDEF SD_VCL4}
  private
    procedure ReadParamData(Reader: TReader);
    procedure WriteParamData(Writer: TWriter);
  protected
    procedure DefineProperties(Filer: TFiler); override;
{$ENDIF}
  protected
{$IFDEF SD_VCL5}
    { IProviderSupport }
    procedure PSExecute; override;
    function PSGetTableName: string; override;
    function PSGetParams: TParams; override;
    procedure PSSetCommandText(const CommandText: string); override;
    procedure PSSetParams(AParams: TParams); override;
{$ENDIF}
    procedure BindParams;
    procedure CreateParamDesc;
    procedure InternalOpen; override;
    procedure PrepareCursor; override;
    procedure UnPrepareCursor;
    function GetParamsCount: Word;
    function SetDSFlag(Flag: Integer; Value: Boolean): Boolean; override;
    procedure SetOverLoad(Value: Word);
    procedure SetProcName(const Value: string);
    procedure SetPrepared(Value: Boolean);
    procedure SetPrepare(Value: Boolean);
  protected
    procedure ISrvAllocBindBuffer;
    procedure ISrvBindParams;
    procedure ISrvFreeBindBuffer;
    function ISrvDescriptionsAvailable: Boolean;
    function ISrvNextResultSet: Boolean;
    procedure ISrvPrepareProc;
    procedure ISrvExecProc;
    procedure ISrvGetResults;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Disconnect; override;
    procedure CopyParams(Value: TSDHelperParams);
    function DescriptionsAvailable: Boolean;
    procedure ExecProc;
    procedure GetResults;
    function NextResultSet: Boolean;
    function ParamByName(const Value: string): TSDHelperParam;
    procedure Prepare;
    procedure UnPrepare;
    property ParamCount: Word read GetParamsCount;
    property Prepared: Boolean read FPrepared write SetPrepared;
  published
    property StoredProcName: string read FProcName write SetProcName;
    property Overload: Word read FOverload write SetOverload default 0;
    property Params: TSDHelperParams read FParams write SetParamsList {$IFDEF SD_VCL4} stored False {$ENDIF};
    property UpdateObject;
  end;

{	abstract server API	}

  TISrvDatabaseClass = class of TISrvDatabase;
  TISrvDataSetClass = class of TISrvDataSet;

{ TISrvDatabase }
  TISrvDatabase = class(TObject)
  private
    FAutoCommitDef: Boolean;			// whether autocommit option is present in TSDDatabase.Params
    FAutoCommit	  : Boolean;
    FDatabase: TSDDatabase;
    FMaxFieldNameLen: Integer;
    function GetAcquiredHandle: Boolean;
  protected
    FCursorPreservedOnCommit,
    FCursorPreservedOnRollback: Boolean;	// whether a server preserves all cursor in the same position after COMMIT, ROLLBACK

    procedure Commit; virtual;
    function FieldDataType(ExtDataType: Integer): TFieldType; virtual;
    function FieldSubType(ExtDataType: Integer): Word; virtual;
    function GetHandle: PSDHandleRec; virtual;
    procedure GetStoredProcNames(List: TStrings); virtual;
    procedure GetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings); virtual;
    procedure GetTableFieldNames(const TableName: string; List: TStrings); virtual;
    function GetClientVersion: LongInt; virtual; abstract;
    function GetServerVersion: LongInt; virtual; abstract;
    function GetVersionString: string; virtual; abstract;
    procedure Logon(const sDatabaseName, sUserName, sPassword: string); virtual;
    procedure Logoff(Force: Boolean); virtual;
    function NativeDataSize(FieldType: TFieldType): Word; virtual;
    function NativeDataType(FieldType: TFieldType): Integer; virtual;
    function ParamValue(Value: TSDDatabaseParam): Integer; virtual;
    function RequiredCnvtFieldType(FieldType: TFieldType): Boolean; virtual;
    procedure Rollback; virtual;
    procedure SetDefaultParams; virtual;
    procedure SetHandle(AHandle: PSDHandleRec); virtual;
    procedure SetTransIsolation(Value: TSDTransIsolation); virtual;
    procedure StartTransaction; virtual;
    function SPDescriptionsAvailable: Boolean; virtual;
  public
    constructor Create(ADatabase: TSDDatabase); virtual;
    class function CreateConnected(ADatabase: TSDDatabase; const ADatabaseName, AUserName, APassword: string): TISrvDatabase; virtual;
    class function CreateWithHandle(ADatabase: TSDDatabase; const AHandle: PSDCursor): TISrvDatabase; virtual;
    function CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet; virtual; abstract;
    procedure FetchAllDataSets;
    procedure ResetIdleTimeOut;
    property AcquiredHandle: Boolean read GetAcquiredHandle;
    property AutoCommit: Boolean read FAutoCommit;
    property AutoCommitDef: Boolean read FAutoCommitDef;
    property Database: TSDDatabase read FDatabase;
    property Handle: PSDHandleRec read GetHandle write SetHandle;
    property CursorPreservedOnCommit: Boolean read FCursorPreservedOnCommit;
    property CursorPreservedOnRollback: Boolean read FCursorPreservedOnRollback;
    property MaxFieldNameLen: Integer read FMaxFieldNameLen;    
  end;

  { TSDFieldDescList }
  PSDFieldDesc = ^TSDFieldDesc;
  TSDFieldDesc = record
    FieldName: string[MAXFIELDNAMELEN];	// Field name or alias (for example, <select F as Alias...>)
    FieldNo : Integer;			// Field number
    DataType: TFieldType;		// Field type
    FldType,				// Native field type
    SubType: Word;			// Field subtype (if applicable)
    Size: SmallInt;			// Size in bytes
    Precision: SmallInt;		// total number of digits
    Required: Boolean;			// is not Null
  end;

  TSDFieldDescList = class(TList)
  private
    function GetFieldDescs(Index: Integer): TSDFieldDesc;
  public
    destructor Destroy; override;
    procedure Clear; {$IFDEF SD_VCL4} override; {$ENDIF} {$IFDEF SD_C3} override; {$ENDIF}
    function FieldDescByName(const AFieldName: string): TSDFieldDesc;
    function FieldDescByNumber(AFieldNo: Integer): TSDFieldDesc;
    property FieldDescs[Index: Integer]: TSDFieldDesc read GetFieldDescs;
  end;

{ TISrvDataSet }
  TISrvDataSet = class(TObject)
  private
    FDataSet: TSDDataSet;
    function GetBlobFieldCount: Integer;
    function GetDataSetComponentState: TComponentState;
    function GetDSFlags: TDSFlags;
    function GetFieldBufOffs(Index: Integer): Integer;
    function GetForceClosing: Boolean;
    function GetResultSet: TSDResultSet;
    function GetSrvDatabase: TISrvDatabase;
    function GetStoredProc: TSDStoredProc;
    function GetQuery: TSDQuery;
  protected
    FSelectBuffer: PChar;
    FBindBuffer: PChar;
    FBlobCacheOffs: Integer;                    // offset to blob data in a select buffer
    FPreservedOnCommit,
    FPreservedOnRollback: Boolean;		// whether a result set is preserved a cursor in the same position as before COMMIT, ROLLBACK, else it's need to reopen cursor
    function BindBufferSize: Integer; virtual;
    procedure CloseResultSet; virtual;
    function CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean; virtual;
    function CnvtDateTime2DBDateTime(ADataType: TFieldType; Value: TDateTime; Buffer: Pointer; BufSize: Integer): Integer; virtual;
    procedure Disconnect(Force: Boolean); virtual;
    procedure MoveBlobFields(Buffer: PChar); virtual;
    procedure InitFieldDefs; virtual;
    procedure InitResultSet;
    procedure Execute; virtual;
    procedure FetchBlobFields; virtual;
    function FetchNextRow: Boolean; virtual;
    function FieldByNumber(FieldNo: Integer): TField;
    function FieldIsNull(FieldBuf: Pointer): Boolean;
    function FieldDescByName(const AFieldName: string): TSDFieldDesc;
    procedure GetFieldDescs(Descs: TSDFieldDescList); virtual;
    function GetHandle: PSDCursor; virtual;
    function GetFieldBuffer(Field: TField; RecBuf: Pointer): Pointer;
    function NativeFieldDataSize(Field: TField): Word; virtual;
    procedure ParseFieldNames(const theSelect: string; FldInfo: TStrings);
    procedure ParseSQLText(const theSelect: string; FldInfo, TblInfo: TStrings);
    function ResultSetExists: Boolean; virtual;
    function SelectBufferSize: Integer; virtual;
    function SetDSFlag(Flag: Integer; Value: Boolean): Boolean;
    procedure SetPreservation(Value: Boolean); virtual;
    procedure SetSelectBuffer; virtual;
    function ReadBlob(FieldNo: Integer; var BlobData: string): Longint; virtual;
    function WriteBlob(FieldNo: Integer; const Buffer; Count: LongInt): Longint; virtual;
    function WriteBlobByName(Name: string; const Buffer; Count: LongInt): Longint; virtual;
	// Query methods
    procedure QBindParams; virtual;
    function QGetRowsAffected: Integer; virtual;
    procedure QPrepareSQL(Value: PChar); virtual;
    procedure QExecute; virtual;
    function QParamBindSize(Param: TSDHelperParam): Integer;
	// StoredProc methods
    procedure SpAddParam(AParamName: string; ADataType: TFieldType; AParamType: TSDHelperParamType);
    procedure SpBindParams; virtual;
    procedure SpCreateParamDesc; virtual;
    procedure SpPrepareProc; virtual;
    procedure SpExecute; virtual;
    procedure SpExecProc; virtual;
    procedure SpGetResults; virtual;
    function SpNextResultSet: Boolean; virtual;
    function SpParamBindSize(Param: TSDHelperParam): Integer; virtual;

    property BlobFieldCount: Integer read GetBlobFieldCount;
    property DSFlags: TDSFlags read GetDSFlags;
    property DataSet: TSDDataSet read FDataSet;
    property Query: TSDQuery read GetQuery;
    property StoredProc: TSDStoredProc read GetStoredProc;
    property DataSetComponentState: TComponentState read GetDataSetComponentState;
    property FieldBufOffs[Index: Integer]: Integer read GetFieldBufOffs;
    property Handle: PSDCursor read GetHandle;
    property ResultSet: TSDResultSet read GetResultSet;
    property SrvDatabase: TISrvDatabase read GetSrvDatabase;
    property ForceClosing: Boolean read GetForceClosing;
  public
    constructor Create(ADataSet: TSDDataSet); virtual;
    destructor Destroy; override;
    procedure AllocBindBuffer; virtual;
    procedure AllocSelectBuffer; virtual;
    procedure ConvertSelectBuffer(Buffer: PChar); virtual;
    procedure FreeBindBuffer; virtual;
    procedure FreeSelectBuffer; virtual;

    property BindBuffer: PChar read FBindBuffer;
//    property RecordCount: Longint read GetRecordCount;
    property SelectBuffer: PChar read FSelectBuffer;
    property PreservedOnCommit: Boolean read FPreservedOnCommit;
    property PreservedOnRollback: Boolean read FPreservedOnRollback;
  end;

var
  ForceOCI7: Boolean = False;		// force OCI7 (SQL*Net 2.x) mode
  
{ Error and exception handling routines }
procedure SDEError(ErrorCode: TSDEResult);
procedure SDECheck(Status: TSDEResult);

function IsBlobType(FieldType: TFieldType): Boolean;
function IsDateTimeType(FieldType: TFieldType): Boolean;
function IsSupportedBlobTypes(FieldType: TFieldType): Boolean;
function IsRequiredSizeTypes(FieldType: TFieldType): Boolean;

function GetAppName: string;
function GetHostName: string;
function VersionStringToDWORD(const VerStr: string): LongInt;
function RepeatChar(Ch: Char; S: string): string;
procedure StrRTrim( S: PChar );

function ExtractLibName(const LibNames: string; Sep: Char; var Pos: Integer): string;
function ExtractStoredProcName(const sFullProcName: string): string;
procedure SetBusyState;
procedure ResetBusyState;
{ functions to parsing TSDDatabase.RemoteDatabase property. They're used in SDMss.pas, SDMySQL.pas }
function ExtractServerName(const sRemoteDatabase: string): string;
function ExtractDatabaseName(const sRemoteDatabase: string): string;

function GetFileVersion(const FileName: string): LongInt;

{ it is used in SDMss and SDSyb units for convert binary type to hex string }
function CnvtVarBytesToHexString(Value: Variant): string;
{ ------- The following functions are used in SDMySQL and SDPgSQL units ------ }
function SqlDateToDateTime(Value: string): TDateTime;
function SqlTimeToDateTime(Value: string): TDateTime;
function SqlStrToFloatDef(Value: string; Default: Double): Double;
function SqlStrToBoolean(Value: string): Boolean;
{ ------- The previous functions are used in SDMySQL and SDPgSQL units ------- }

{ Locates the position of a sub-string within a string like AnsiPos, but AnsiTextPos is case-insensitive }
function AnsiTextPos(const Substr, S: string): Integer;
{ Replaces OldStr (as word, separated delimiters) and returns True when the replacing is successful }
function ReplaceString( Once: Boolean; OldStr, NewStr: string; var ResultStr: string ): Boolean;

{ For compatibility with all Delphi version: exclude using of Math unit }
function MaxIntValue(A, B: Integer): Integer;
function MinIntValue(A, B: Integer): Integer;


{$IFNDEF SD_VCL5}	// for TBCDField support
type
  PBcd = ^TBcd;
  TBcd  = packed record
    Precision: Byte;                        { 1..64 }
    SignSpecialPlaces: Byte;                { Sign:1, Special:1, Places:6 }
    Fraction: packed array [0..31] of Byte; { BCD Nibbles, 00..99 per Byte, high Nibble 1st }
  end;

function CurrToBCD(Curr: Currency; var BCD: TBcd; Precision, Decimals: Integer): Boolean;
function BCDToCurr(const BCD: TBcd; var Curr: Currency): Boolean;
{$ENDIF}

var
  Session: TSDSession;
  Sessions: TSDSessionList;

{ Global variable for parameter binding to Oracle8 CLOB & BLOB field types.
  Set this to True before executing a query on that uses these types for parameters.
  Assign the data using TParam.AsMemo for CLOB and TParam.AsBlob for BLOB.
  Note you cannot mix CLOB/BLOB and RAW/LONG RAW in the binding of the same query. }
  Oracle8Blobs: Boolean;
{ Global variable defines minimum size of a buffer for string parameters }
  MinStrParamSize: Integer;

  SrvApiDLLs: array[TSDServerType] of string;
  SrvDatabaseClasses: array[TSDServerType] of TISrvDatabaseClass;

implementation

uses
  TypInfo,
  DBConsts, DBLogDlg,
{$IFDEF EVAL}SDRemind,{$ENDIF}
  SDCsb, SDMss, SDOra, SDSyb, SDDb2, SDInf, SDOdbc, SDInt,
  SDMySQL, SDPgSQL;

const
  ParamPrefix		= ':';
  
  DateTimeTypes		= [ftDate, ftTime, ftDateTime];
  { Field types with changeable size (require size defining) }
  RequiredSizeTypes	= [{ftBCD,} ftBytes, ftVarBytes, ftString, ftParadoxOle, ftDBaseOle, ftTypedBinary];
  {   in DB.pas : TBlobType = ftBlob..ftTypedBinary; (D3-4, CB3-4),
                  TBlobType = ftBlob..ftOraClob (D5) includes not valid blob types (for example, ftCursor)  }
  BlobTypes		=
  	[ftBlob..ftTypedBinary] {$IFDEF SD_VCL5} + [ftOraBlob, ftOraClob] {$ENDIF};
  SupportedBlobTypes	=
        [ftBlob, ftMemo {$IFDEF SD_VCL5}, ftOraBlob, ftOraClob{$ENDIF}];


function IsBlobType(FieldType: TFieldType): Boolean;
begin
  Result := FieldType in BlobTypes;
end;

function IsSupportedBlobTypes(FieldType: TFieldType): Boolean;
begin
  Result := FieldType in SupportedBlobTypes;
end;

function IsRequiredSizeTypes(FieldType: TFieldType): Boolean;
begin
  Result := FieldType in RequiredSizeTypes;
end;

function IsDateTimeType(FieldType: TFieldType): Boolean;
begin
  Result := FieldType in DateTimeTypes;
end;

{ find first digits in the string sInit and convert them to number }
function VersionStringToDWORD(const VerStr: string): LongInt;
const
  DigitSet = ['0'..'9'];
var
  i: Integer;
  HVer, LVer: LongInt;
  sVer: string;
begin
  HVer := 0;
  LVer := 0;
  i := 1;
  sVer := VerStr;
  	// find first digit
  while (i <= Length(sVer)) and not(sVer[i] in DigitSet) do
    Inc(i);
  if i > 1 then
    System.Delete(sVer, 1, i-1);
    	// find major version number
  for i:=1 to Length(sVer) do
    if not(sVer[i] in ['0'..'9']) then begin
      HVer := StrToIntDef( Copy(sVer, 1, i-1), 0 );
      System.Delete(sVer, 1, i);
      Break;
    end;
    	// find minor version number
  for i:=1 to Length(sVer) do
    if not(sVer[i] in ['0'..'9']) then begin
      LVer := StrToIntDef( Copy(sVer, 1, i-1), 0 );
      sVer := '';      
      Break;
    end;
	// if digits follow till the end of the string
  if sVer <> '' then
    LVer := StrToIntDef(sVer, 0);
  Result := MakeLong( LVer, HVer );
end;

{ Insert repetable char Ch into string S: if find Ch then insert Ch after one }
function RepeatChar(Ch: Char; S: string): string;
var
  i: Integer;
begin
  i := 1;
  Result := S;
  while i <= Length(Result) do begin
    if Result[i] = Ch then begin
      Insert( Ch, Result, i+1 );
      Inc(i);
    end;
    Inc(i);
  end;
end;

{ Removes trailing spaces }
procedure StrRTrim( S: PChar );
const
  SP	= #$20;
  ZS	= #$00;
var
  i: Integer;
begin
  i := -1;
  if AnsiStrComp(S, '') <> 0 then
    i := StrLen( S ) - 1;

  while i >= 0 do begin
    if S[i] in [SP] then
      S[i] := ZS
    else
      Break;
    Dec( i );
  end;
end;

{ ------- The following functions are used in SDMySQL and SDPgSQL units ------ }

{ To avoid using Math unit for compatibility with all Delphi version (maybe, Math unit isn't included with some Delphi version) }
function Max(A, B: Integer): Integer;
begin
  if A >= B then
    Result := A
  else
    Result := B;
end;

function MaxIntValue(A, B: Integer): Integer;
begin
  Result := Max(A, B);
end;

function MinIntValue(A, B: Integer): Integer;
begin
  if A <= B then
    Result := A
  else
    Result := B;
end;

{ Define last day of a month }
function LastDay(Month, Year: Word): Word;
begin
  Result := 30;
  case Month of
    1,3,5,7,8,10,12:
      Result := 31;
    2: begin
        Result := 28;
        if (((Year mod 4) = 0) or ((Year mod 400) = 0)) and
           not((Year mod 100) = 0)
        then
          Result := 29;
      end;
  end;
end;

{ Convert SQL Date (YYYY-MM-DD HH:NN:SS) to TDateTime with constant date part }
function SqlDateToDateTime(Value: string): TDateTime;
var
  Year, Month, Day, Hour, Min, Sec: Word;
  Temp: string;
begin
  Temp  := Value;
  Year  := StrToIntDef(Copy(Temp, 1, 4), 1);
  Year  := MaxIntValue(Year, 1);
  Month := StrToIntDef(Copy(Temp, 6, 2), 1);
  Month := MinIntValue(MaxIntValue(Month, 1), 12);
  Day   := StrToIntDef(Copy(Temp, 9, 2), 1);
  Day   := MinIntValue(MaxIntValue(Day, 1), LastDay(Month, Year));
  Result := EncodeDate(Year, Month, Day);

  if Length(Temp) > 11 then begin
    Temp := Copy(Temp, 12, 8);
    Hour := StrToIntDef(Copy(Temp, 1, 2), 0);
    Hour := MinIntValue(MaxIntValue(Hour, 0), 23);
    Min  := StrToIntDef(Copy(Temp, 4, 2), 0);
    Min  := MinIntValue(MaxIntValue(Min, 0), 59);
    Sec  := StrToIntDef(Copy(Temp, 7, 2), 0);
    Sec  := MinIntValue(MaxIntValue(Sec, 0), 59);
	// if date value is before "12/30/1899 12:00", then Result < 0
    if Result >= 0 then
      Result := Result + EncodeTime(Hour, Min, Sec, 0)
    else
      Result := Result - EncodeTime(Hour, Min, Sec, 0);    
  end;
end;

{ Convert SQL Time (YYYY-MM-DD HH:NN:SS) to TDateTime
 Any part of date could be equal zero or out of range, because it's need additional processing }
function SqlTimeToDateTime(Value: string): TDateTime;
var
  Year, Month, Day, Hour, Min, Sec: Word;
  Temp: string;
begin
  Temp   := Value;
  Result := 0;
  	// if date part is present
  if Length(Temp) >= 10 then begin
    Year  := StrToIntDef(Copy(Temp, 1, 4), 1);
    Year := Max(Year, 1);
    Month := StrToIntDef(Copy(Temp, 6, 2), 1);
    Month := MinIntValue(MaxIntValue(Month, 1), 12);
    Day   := StrToIntDef(Copy(Temp, 9, 2), 1);
    Day   := MinIntValue(MaxIntValue(Day, 1), LastDay(Month, Year));

    Result := EncodeDate(Year, Month, Day);
    Temp := Copy(Temp, 12, 8);
  end;
  if Length(Temp) >= 8 then begin
    Hour := StrToIntDef(Copy(Temp, 1, 2), 0);
    Hour := MinIntValue(MaxIntValue(Hour, 0), 23);
    Min  := StrToIntDef(Copy(Temp, 4, 2), 0);
    Min  := MinIntValue(MaxIntValue(Min, 0), 59);
    Sec  := StrToIntDef(Copy(Temp, 7, 2), 0);
    Sec  := MinIntValue(MaxIntValue(Sec, 0), 59);
	// if date value is before "12/30/1899 12:00", then Result < 0
    if Result >= 0 then
      Result := Result + EncodeTime(Hour, Min, Sec, 0)
    else
      Result := Result - EncodeTime(Hour, Min, Sec, 0);
  end;
end;

{ Convert string value to float with '.'(Delphi) delimiter with default value }
function SqlStrToFloatDef(Value: string; Default: Double): Double;
var
  Ptr: PChar;
begin
  Ptr := PChar(Value);
  while Ptr^ <> #0 do begin
  	// change decimal delimiter to one, which is recognized by Delphi
    if Ptr^ in ['.',','] then
      Ptr^ := DecimalSeparator;
    Inc(Ptr);
  end;
  if Value <> '' then
    try
      Result := StrToFloat(Value);
    except
      Result := Default;
    end
  else
    Result := 0;
end;

{ Check valid literal values for the "true" state (from PostreSQL docs)  }
function SqlStrToBoolean(Value: string): Boolean;
begin
  Result :=
    (AnsiCompareText(Value, 'true') = 0) or
    (AnsiCompareText(Value, 'yes') = 0 ) or
    ((Length(Value) = 1) and (Value[1] in ['1', 'y', 'Y', 't', 'T']));
end;

{ ------- The previous functions are used in SDMySQL and SDPgSQL units ------ }


{ it is used in SDMss and SDSyb units for convert binary/blob type to hex string }
function CnvtVarBytesToHexString(Value: Variant): string;
var
  i, h: Integer;
  PData: PChar;
  bStr: Boolean;
begin
  bStr := VarType(Value) = varString;
  if bStr then begin
    PData := TVarData(Value).VString;
    i := 0;
    h := Length( VarToStr( Value ) ) - 1;
  end else begin
    h := VarArrayHighBound(Value, 1);
    i := VarArrayLowBound(Value, 1);
    PData := VarArrayLock(Value);
  end;
  try
    Result := '0x';
    while i <= h do begin
      Result := Result + Format('%.2x', [Ord(PData[i])]);
      Inc(i);
    end;
  finally
    if not bStr then
      VarArrayUnLock(Value);
  end;
  if Length(Result) = 2 then
    Result := 'NULL';
end;

{ functions to parsing TSDDatabase.RemoteDatabase property. They're used in SDMss.pas, SDMySQL.pas }
function ExtractServerName(const sRemoteDatabase: string): string;
var
  i: Integer;
begin
  Result := sRemoteDatabase;
	// looking delimiter
  i := 1;
  while i <= Length(Result) do begin
    if IsDelimiter(ServerDelimiters, Result, i) then
      Break;
    Inc(i);
  end;
	// extract substring before delimiter
  if i <= Length(Result) then
    Result := Copy(Result, 1, i-1);
end;

function ExtractDatabaseName(const sRemoteDatabase: string): string;
var
  i: Integer;
begin
  Result := '';
	// looking delimiter
  i := 1;
  while i <= Length(sRemoteDatabase) do begin
    if IsDelimiter(ServerDelimiters, sRemoteDatabase, i) then
      Break;
    Inc(i);
  end;
	// extract substring after delimiter
  if i < Length(sRemoteDatabase) then begin
    Result := Copy(sRemoteDatabase, i+1, Length(sRemoteDatabase)-i);
    	// exclude repeated delimiters, in case of presence    
    while Length(Result) > 0 do
      if IsDelimiter(ServerDelimiters, Result, 1) then begin
        if Length(Result) = 1 then
          Result := ''
        else
          Result := Copy(Result, 2, Length(Result)-1)
      end else
        Break;
  end;
end;

{ Returns file version, which is encrypted in integer number }
function GetFileVersion(const FileName: string): LongInt;
type
  TTransTable = array[0..0,0..1] of Word;
var
  bOk: Boolean;
  InfoSize, Len: DWORD;
  lpInfoData: Pointer;
  pFileInfo: ^TVSFixedFileInfo;
  pTransTable: ^TTransTable;
  szStringValue: PChar;
  sLangCharSet: string;
begin
  Result := 0;
  InfoSize := GetFileVersionInfoSize( PChar(FileName), Len );
  if InfoSize <= 0 then
    Exit;

  lpInfoData := StrAlloc(InfoSize);
  try
    bOk := GetFileVersionInfo( PChar(FileName), 0, InfoSize, lpInfoData );
    bOk := bOk and VerQueryValue( lpInfoData, '\', Pointer(pFileInfo), Len );
    if not bOk then
      Exit;
    if VerQueryValue(lpInfoData, '\VarFileInfo\Translation', Pointer(pTransTable), Len) then
      sLangCharSet := IntToHex(pTransTable^[0,0], 4) + IntToHex(pTransTable^[0,1], 4);
    if VerQueryValue(lpInfoData, PChar('\StringFileInfo\'+sLangCharSet+'\FileVersion'), Pointer(szStringValue), Len) then
      Result := VersionStringToDWORD(StrPas(szStringValue));
  finally
    StrDispose(lpInfoData);
  end;
end;

{ Parse statement utility (later move to SDEngine or other common unit), it'll be useful for macro support }

function IsNameDelimiter(C: Char): Boolean;
begin
  Result := (C in [' ', ',', ';', ')', #13, #10])
end;

function IsLiteral(C: Char): Boolean;
begin
  Result := C in ['''', '"'];
end;

function AnsiTextPos(const Substr, S: string): Integer;
begin
  Result := AnsiPos( Substr, S );
  	// when strings are present in different cases
  if Result = 0 then
    Result := AnsiPos( AnsiUpperCase(Substr), AnsiUpperCase(S) );
end;

function StrFindFromPos(const Substr, S: string; StartPos: Integer): Integer;
var
  Str: string;
begin
  if StartPos > 1 then
  	// Copy returns empty string when StartPos > Length(S)
    Str := System.Copy(S, StartPos, Length(S) - StartPos + 1)
  else begin
    Str := S;
    StartPos := 1;
  end;
  Result := AnsiTextPos( Substr, Str );
  if Result > 0 then
    Result := StartPos + (Result - 1);	// return an index of Pascal string
end;

{ Returns False, when nothing is changed. Once parameter specifies only one changing }
function ReplaceString( Once: Boolean; OldStr, NewStr: string; var ResultStr: string ): Boolean;
var
  i, FoundPos, Literals: Integer;
  bFound: Boolean;
begin
  Result := False;

  repeat
    FoundPos := 0;
    repeat
    	// pass the first char of the similar string, which was found earlier
      if FoundPos > 0 then Inc(FoundPos);
      FoundPos := StrFindFromPos( OldStr, ResultStr, FoundPos );
    	// check whether OldStr at the end of ResultStr or has a delimiter after itself
      bFound :=
    	(FoundPos > 0) and
    	((Length(ResultStr) = FoundPos + Length(OldStr) - 1) or
          IsNameDelimiter( ResultStr[FoundPos + Length(OldStr)] )
        );

      if bFound then begin
        Literals := 0;
        for i := 1 to FoundPos - 1 do
          if IsLiteral(ResultStr[i]) then Inc(Literals);
        bFound := Literals mod 2 = 0;	// OldStr has not to be quoted
        if bFound then begin
          System.Delete( ResultStr, FoundPos, Length(OldStr) );
          System.Insert( NewStr, ResultStr, FoundPos );
          FoundPos := FoundPos + Length(NewStr) - 1;
          Result := True;
          if Once then
            Exit;
        end;
      end;
    until FoundPos = 0;
  until not bFound;

end;

{----------------------- END OF UTILITY FUNCTIONS/PROCEDURES ------------------}

type

  { TBookmarkRec }
  PBookmarkRec = ^TBookmarkRec;
  TBookmarkRec = packed record		{ Bookmark structure }
    iPos   : Integer;               { Position in given order - position in the cache(FRecCache) }
{    iState : Integer;               { State of cursor }
//    iRecNo : Integer;               { Physical Record number }
{    iSeqNo : Integer;               { Version number of order }
{    iOrderID : Integer;             { Defines Order }
  end;

  { TDBParams - remote database parameters that stored in the Session object }
  PDBParams	= ^TDBParams;
  TDBParams	= record
    RemoteDatabase: string;
    ServerType: TSDServerType;
    Params: TStrings;
  end;

{ Busy state cursor function }
var
  BusyCount: Integer;
  SaveCursor: TCursor;
  FCSect: TRTLCriticalSection;

function GetAppName: string;
begin
  Result := ExtractFileName( Application.ExeName );
end;

function GetHostName: string;
var
  szCompName: PChar;
  nSize: {$IFDEF SD_VCL4}Cardinal{$ELSE}Integer{$ENDIF};
begin
  Result := '';
  nSize := MAX_COMPUTERNAME_LENGTH + 1;
  szCompName := StrAlloc( nSize );
  try
    if GetComputerName(szCompName, nSize) then
      if nSize > 0 then
        Result := StrPas(szCompName);
  finally
    StrDispose( szCompName );
  end;
end;

function ExtractLibName(const LibNames: string; Sep: Char; var Pos: Integer): string;
var
  I: Integer;
begin
  I := Pos;
  while (I <= Length(LibNames)) and (LibNames[I] <> Sep) do
    Inc(I);
  Result := Trim( Copy(LibNames, Pos, I - Pos) );
  if (I <= Length(LibNames)) and (LibNames[I] = Sep) then
    Inc(I);
  Pos := I;
end;

{
  Extracts only stored procedure name(with owner) from full name ( <owner.procname;group_no> )
  Used by SDMss and SDSyb units
}
function ExtractStoredProcName(const sFullProcName: string): string;
var
  i: Integer;
begin
  Result := sFullProcName;

  i := AnsiPos(';', Result);
	// if group number separator (;) found
  if i > 0 then
    Result := Copy( Result, 1, i-1 );
end;

procedure SetBusyState;
begin
  if GetCurrentThreadID <> MainThreadID then
    Exit;
	// if main thread, that change cursor
  if Session.FSQLHourGlass then begin
    if BusyCount = 0 then begin
      SaveCursor := Screen.Cursor;
      Screen.Cursor := Session.FSQLWaitCursor;
    end;
    Inc(BusyCount);
  end;
end;

procedure ResetBusyState;
begin
  if Session.FSQLHourGlass then
    if BusyCount > 0 then begin
      Dec(BusyCount);
      if BusyCount = 0 then
        Screen.Cursor := SaveCursor;
    end;
end;

{ comparison values of the following types: Variant or Array of Variant,
 where V2 is key value, which can be coincided partially with V1 if PartialKey is True }
function VarIsEqual(V1, V2: Variant; CaseInsensitive, PartialKey: Boolean): Boolean;
	// compares using options
  function ExtCompareText(const S1, S2: string; CaseInsensitive, PartialKey: Boolean): Integer;
  var
    MinLen: Integer;	// partial key length
  begin
    MinLen := 0;
    if PartialKey then begin
      if Length(S2) < Length(S1)
      then MinLen := Length(S2)
    end;
	// partial compare
    if MinLen > 0 then
      if CaseInsensitive
      then Result := AnsiCompareText( Copy(S1, 1, MinLen), Copy(S2, 1, MinLen) )
      else Result := AnsiCompareStr( Copy(S1, 1, MinLen), Copy(S2, 1, MinLen) )
    else
      if CaseInsensitive
      then Result := AnsiCompareText( S1, S2 )
      else Result := AnsiCompareStr( S1, S2 );
  end;
var
  i: Integer;
begin
  if ((VarType(V1) and not varTypeMask) <> varArray) and
     ((VarType(V2) and not varTypeMask) <> varArray)
  then			// V1 and V2 are not arrays
    if ((VarType(V1) = varString) or (VarType(V1) = varOleStr)) and
       ((VarType(V2) = varString) or (VarType(V2) = varOleStr))
    then
      Result := ExtCompareText(V1, V2, CaseInsensitive, PartialKey) = 0
    else
      Result := V1 = V2
  else begin			// V1 and V2 are arrays
    Result := True;
    for i:= VarArrayLowBound(V1, 1) to VarArrayHighBound(V1, 1) do begin
      if ((VarType(V1[i]) = varString) or (VarType(V1[i]) = varOleStr)) and
         ((VarType(V2[i]) = varString) or (VarType(V2[i]) = varOleStr))
      then
        Result := ExtCompareText(V1[i], V2[i], CaseInsensitive, PartialKey) = 0
      else
        Result := Result and (V1[i] = V2[i]);
      if not Result then Break;
    end;
  end;
end;

function DateTimeRecToDateTime(DataType: TFieldType; DateTimeRec: TDateTimeRec): TDateTime;
var
  TimeStamp: TTimeStamp;
begin
  case DataType of
    ftDate:
      begin
        TimeStamp.Time := 0;
        TimeStamp.Date := DateTimeRec.Date;
      end;
    ftTime:
      begin
        TimeStamp.Time := DateTimeRec.Time;
        TimeStamp.Date := DateDelta;
      end;
    ftDateTime:
      try
        TimeStamp := MSecsToTimeStamp(DateTimeRec.DateTime);
      except
        TimeStamp.Time := 0;
        TimeStamp.Date := 0;
      end;
    else
      raise Exception.Create('Unknown data type in function DateTimeRecToDateTime');
    end;
  Result := TimeStampToDateTime(TimeStamp);
end;

{ MIDAS uses '?' parameter marker, which is supported in Interbase, DB2, Informix and ODBC only.
 I.e. for some servers it is necessary to change a parameter marker from '?' to ':NAME' }
function ReplaceQuestionParamMarker(AServerType: TSDServerType; ASQL :string; AParams :TSDHelperParams): string;
var
  i: Integer;
begin
  Result := ASQL;

  if (AServerType in [stDB2, stInformix, stODBC, stInterbase]) or (AParams.Count = 0) then
    Exit;
  if Pos('?', ASQL) = 0 then
    Exit;

  for i:=0 to AParams.Count-1 do begin
    if AParams[i].Name = '' then
      AParams[i].Name := IntToStr( i+1 );
      // it is necessary to change only one '?' marker per parameter
    ReplaceString( True, '?', ParamPrefix + AParams[i].Name, Result);
  end;
end;

procedure SetUnknownParamTypeToInput(P: TSDHelperParams);
var
  i: Integer;
begin
  for i:=0 to P.Count-1 do
    if P[i].ParamType = ptUnknown then
      P[i].ParamType := ptInput;
end;


type

{ TSDQueryDataLink }
  TSDCustomQueryDataLink = {$IFDEF SD_VCL4} TDetailDataLink {$ELSE} TDataLink {$ENDIF};

  TSDQueryDataLink = class( TSDCustomQueryDataLink )
  private
    FQuery: TSDQuery;
  protected
    procedure ActiveChanged; override;
    procedure RecordChanged(Field: TField); override;
{$IFDEF SD_VCL4}
    function GetDetailDataSet: TDataSet; override;
{$ENDIF}
    procedure CheckBrowseMode; override;
  public
    constructor Create(AQuery: TSDQuery);
  end;


{ Utility routines }
  
function DefaultSession: TSDSession;
begin
  Result := SDEngine.Session;
end;

{ Error and exception handling routines }

procedure SDEError(ErrorCode: TSDEResult);
begin
  ResetBusyState;
  raise ESDEngineError.CreateDefPos(ErrorCode, ErrorCode, '');
end;

procedure SDECheck(Status: TSDEResult);
begin
  if Status <> 0 then SDEError(Status);
end;


{ ESDEngineError }
constructor ESDEngineError.Create(AErrorCode, ANativeError: LongInt; const Msg: string; AErrorPos: LongInt);
begin
  inherited Create(Msg);
  FErrorCode := AErrorCode;
  FNativeError := ANativeError;
  FErrorPos := AErrorPos;
end;

constructor ESDEngineError.CreateDefPos(AErrorCode, ANativeError: LongInt; const Msg: string);
begin
  inherited Create(Msg);
  FErrorCode := AErrorCode;
  FNativeError := ANativeError;
  FErrorPos := -1;
end;


{*******************************************************************************
				TSDSessionList
*******************************************************************************}
constructor TSDSessionList.Create;
begin
  inherited Create;
  FSessions := TThreadList.Create;
  FSessionNumbers := TBits.Create;
  InitializeCriticalSection(FCSect);
end;

destructor TSDSessionList.Destroy;
begin
  CloseAll;
  DeleteCriticalSection(FCSect);
  FSessionNumbers.Free;
  FSessions.Free;
  inherited Destroy;
end;

procedure TSDSessionList.AddSession(ASession: TSDSession);
var
  List: TList;
begin
  List := FSessions.LockList;
  try
    if List.Count = 0 then
      ASession.FDefault := True;
    List.Add(ASession);
  finally
    FSessions.UnlockList;
  end;
end;

procedure TSDSessionList.CloseAll;
var
  I: Integer;
  List: TList;
begin
  List := FSessions.LockList;
  try
    for I := List.Count-1 downto 0 do
      TSDSession(List[I]).Free;
    SDEngine.Session := nil;	// default session is destroyed now
  finally
    FSessions.UnlockList;
  end;
end;

function TSDSessionList.GetCount: Integer;
var
  List: TList;
begin
  List := FSessions.LockList;
  try
    Result := List.Count;
  finally
    FSessions.UnlockList;
  end;
end;

function TSDSessionList.GetSession(Index: Integer): TSDSession;
var
  List: TList;
begin
  List := FSessions.LockList;
  try
    Result := TSDSession( List[Index] );
  finally
    FSessions.UnlockList;
  end;
end;

function TSDSessionList.GetSessionByName(const SessionName: string): TSDSession;
begin
  if SessionName = '' then
    Result := Session
  else
    Result := FindSession(SessionName);
  if Result = nil then
    DatabaseErrorFmt(SInvalidSessionName, [SessionName]);
end;

function TSDSessionList.FindDatabase(const DatabaseName: string): TSDDatabase;
var
  i: Integer;
  List: TList;
begin
  Result := nil;

  List := FSessions.LockList;
  try
    for i:=0 to List.Count-1 do begin
      Result := TSDSession( List[i] ).FindDatabase( DatabaseName );
      if Result <> nil then
        Break;
    end;
  finally
    FSessions.UnlockList;
  end;
end;

function TSDSessionList.FindSession(const SessionName: string): TSDSession;
var
  I: Integer;
  List: TList;
begin
  if SessionName = '' then
    Result := Session
  else begin
    List := FSessions.LockList;
    try
      for I := 0 to List.Count - 1 do begin
        Result := List[I];
        if AnsiCompareText( Result.SessionName, SessionName ) = 0 then
          Exit;
      end;
      Result := nil;
    finally
      FSessions.UnlockList;
    end;
  end;
end;

procedure TSDSessionList.GetSessionNames(List: TStrings);
var
  I: Integer;
  SList: TList;
begin
  List.BeginUpdate;
  try
    List.Clear;
    SList := FSessions.LockList;
    try
      for I := 0 to SList.Count - 1 do
        with TSDSession( SList[I] ) do
          List.Add(SessionName);
    finally
      FSessions.UnlockList;
    end;
  finally
    List.EndUpdate;
  end;
end;

function TSDSessionList.OpenSession(const SessionName: string): TSDSession;
begin
  Result := FindSession(SessionName);
  if Result = nil then begin
    Result := TSDSession.Create(nil);
    Result.SessionName := SessionName;
  end;
  Result.SetActive(True);
end;


{*******************************************************************************
				TSDSession
*******************************************************************************}
constructor TSDSession.Create(AOwner: TComponent);
begin
  ValidateAutoSession(AOwner, False);
  inherited Create(AOwner);
  FDatabases	:= TList.Create;
  FDefault	:= False;
  FDBParams	:= TList.Create;
  FKeepConnections := True;
  FSQLHourGlass := True;
  FSQLWaitCursor:= crHourGlass;
  FLockCount	:= 0;

  Sessions.AddSession(Self);
end;

destructor TSDSession.Destroy;
begin
  SetActive(False);
  if Assigned(Sessions) then
    Sessions.FSessions.Remove(Self);

  FDatabases.Free;
  FDatabases := nil;

  if Assigned(FDBParams) then
    ClearDBParams;
  FDBParams.Free;
  FDBParams := nil;

  inherited Destroy;
end;

procedure TSDSession.AddDatabase(Value: TSDDatabase);
begin
  FDatabases.Add(Value);
end;

procedure TSDSession.CheckInactive;
begin
  if Active then
    DatabaseError(SSessionActive);
end;

procedure TSDSession.Close;
begin
  SetActive(False);
end;

procedure TSDSession.Open;
begin
  SetActive(True);
end;

function TSDSession.GetActive: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i:=0 to DatabaseCount-1 do
    if Databases[i].Connected then begin
      Result := True;
      Break;
    end;
  if not Result then
    Result := FActive;
end;

procedure TSDSession.SetActive(Value: Boolean);
begin
  if csReading in ComponentState then
    FStreamedActive := Value
  else
    if Active <> Value then
      StartSession(Value);
end;

procedure TSDSession.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if AutoSessionName and (Operation = opInsert) then
    if AComponent is TSDDataSet then
      TSDDataSet(AComponent).FSessionName := Self.SessionName
    else if AComponent is TSDDatabase then
      TSDDatabase(AComponent).FSession := Self;
end;

procedure TSDSession.SetName(const NewName: TComponentName);
begin
  inherited SetName(NewName);
  if FAutoSessionName then UpdateAutoSessionName;
end;

function TSDSession.SessionNameStored: Boolean;
begin
  Result := not FAutoSessionName;
end;

procedure TSDSession.SetAutoSessionName(Value: Boolean);
begin
  if Value <> FAutoSessionName then begin
    if Value then begin
      CheckInActive;
      ValidateAutoSession(Owner, True);
      FSessionNumber := -1;
      EnterCriticalSection(FCSect);
      try
        with Sessions do begin
          FSessionNumber := FSessionNumbers.OpenBit;
          FSessionNumbers[FSessionNumber] := True;
        end;
      finally
        LeaveCriticalSection(FCSect);
      end;
      UpdateAutoSessionName;
    end
    else begin
      if FSessionNumber > -1 then begin
        EnterCriticalSection(FCSect);
        try
          Sessions.FSessionNumbers[FSessionNumber] := False;
        finally
          LeaveCriticalSection(FCSect);
        end;
      end;
    end;
    FAutoSessionName := Value;
  end;
end;

procedure TSDSession.SetSessionName(const Value: string);
var
  Ses: TSDSession;
begin
  if FAutoSessionName and not FUpdatingAutoSessionName then
    DatabaseError(SAutoSessionActive);
  CheckInActive;
  if Value <> '' then begin
    Ses := Sessions.FindSession(Value);
    if not ((Ses = nil) or (Ses = Self)) then
      DatabaseErrorFmt(SDuplicateSessionName, [Value]);
  end;
  FSessionName := Value
end;

procedure TSDSession.SetSessionNames;
var
  I: Integer;
  Component: TComponent;
begin
  if Owner <> nil then
    for I := 0 to Owner.ComponentCount - 1 do begin
      Component := Owner.Components[I];
      if (Component is TSDDataBase) and
         (AnsiCompareText(TSDDatabase(Component).SessionName, Self.SessionName) <> 0)
      then
        TSDDataBase(Component).SessionName := Self.SessionName
      else if (Component is TSDDataSet) and
              (AnsiCompareText(TSDDataSet(Component).SessionName, Self.SessionName) <> 0)
      then
        TSDDataSet(Component).SessionName := Self.SessionName;
    end;
end;

procedure TSDSession.StartSession(Value: Boolean);
var
  i: Integer;
begin
  EnterCriticalSection(FCSect);
  try
    if Value then begin
      if FSessionName = '' then
        DatabaseError(SSessionNameMissing);
      if (DefaultSession <> Self) then
        DefaultSession.Active := True;
    end else begin
      for i := FDatabases.Count - 1 downto 0 do
        with TSDDatabase(FDatabases[i]) do
          if Temporary
          then Free
          else Close;
    end;
    FActive := Value;
  finally
    LeaveCriticalSection(FCSect);
  end;
end;

procedure TSDSession.CloseDatabase(Database: TSDDatabase);
begin
  with Database do begin
    if FRefCount <> 0 then Dec(FRefCount);
    if (FRefCount = 0) and not KeepConnection then
      if not Temporary then
        Close
      else if not (csDestroying in ComponentState) then
        Free;
  end;
end;

function TSDSession.FindDatabase(const DatabaseName: string): TSDDatabase;
var
  i: Integer;
begin
  for i := 0 to FDatabases.Count - 1 do begin
    Result := FDatabases[i];
    if ((Result.DatabaseName <> '') or Result.Temporary) and
        (AnsiCompareText(Result.DatabaseName, DatabaseName) = 0)
    then
      Exit;
  end;
  Result := nil;
end;

function TSDSession.DoFindDatabase(const DatabaseName: string; AOwner: TComponent): TSDDatabase;
var
  i: Integer;
begin
  if AOwner <> nil then
    for i := 0 to FDatabases.Count - 1 do begin
      Result := FDatabases[i];
      if (Result.Owner = AOwner) and
         (AnsiCompareText(Result.DatabaseName, DatabaseName) = 0)
      then
        Exit;
    end;
  Result := FindDatabase(DatabaseName);
end;

function TSDSession.GetDatabase(Index: Integer): TSDDatabase;
begin
  Result := FDatabases[Index];
end;

function TSDSession.GetDatabaseCount: Integer;
begin
  if Assigned(FDatabases) then
    Result := FDatabases.Count
  else
    Result := 0;
end;

procedure TSDSession.GetDatabaseNames(List: TStrings);
var
  I: Integer;
  Names: TStringList;
begin
  Names := TStringList.Create;
  try
    Names.Sorted := True;

    for I := 0 to FDatabases.Count - 1 do
      with TSDDatabase(FDatabases[I]) do
        Names.Add( AnsiLowerCase(DatabaseName) );

    List.Assign(Names);
  finally
    Names.Free;
  end;
end;

procedure TSDSession.GetDatabaseParams(const ARemoteDatabase: string; AServerType: TSDServerType; List: TStrings);
var
  i: Integer;
  pp: PDBParams;
begin
  for i:=0 to FDBParams.Count-1 do begin
    pp := FDBParams[i];
    if Assigned(pp) and
       (pp^.ServerType = AServerType) and
       (AnsiCompareText(pp^.RemoteDatabase, ARemoteDatabase) = 0)
    then begin
      if Assigned(pp^.Params) then
        List.Assign(pp^.Params);
      Exit;
    end;
  end;
end;

{ Stores database Params to Session.FDBParams, when a database is opening.
If a database is closing, then it's params don't be removed from Session.FDBParams }
procedure TSDSession.InternalAddDatabase(const ARemoteDatabase: string; AServerType: TSDServerType; List: TStrings);
var
  i: Integer;
  pp: PDBParams;
begin
  pp := nil;
  for i:=0 to FDBParams.Count-1 do
	// if RemoteDatabase and database Params exist, then don't add,
        // but it is necessary to change Params to new values
    if Assigned( FDBParams[i] ) and
       (PDBParams(FDBParams[i])^.ServerType = AServerType) and
       (AnsiCompareText( PDBParams(FDBParams[i])^.RemoteDatabase, ARemoteDatabase ) = 0)
    then begin
      pp := PDBParams(FDBParams[i]);
      Break;
    end;
	// if RemoteDatabase and database Params don't exist
  if pp = nil then begin
    New(pp);
    pp^.RemoteDatabase	:= ARemoteDatabase;
    pp^.ServerType	:= AServerType;    
    pp^.Params		:= TStringList.Create;
    FDBParams.Add(pp);
  end;

  pp^.Params.Assign(List);
end;

procedure TSDSession.ClearDBParams;
var
  i: Integer;
  pp: PDBParams;
begin
  for i:=FDBParams.Count-1 downto 0 do begin
    pp := FDBParams[i];
    if Assigned(pp) then begin
      pp^.Params.Free;
      Dispose(pp);
      FDBParams.Delete(i);
    end;
  end;
end;

procedure TSDSession.Loaded;
begin
  inherited Loaded;
  try
    if AutoSessionName then SetSessionNames;
    if FStreamedActive then SetActive(True);
  except
    if csDesigning in ComponentState then
      Application.HandleException(Self)
    else
      raise;
  end;
end;

procedure TSDSession.LockSession;
begin
  if FLockCount = 0 then begin
    EnterCriticalSection(FCSect);
    Inc(FLockCount);
    MakeCurrent;
  end else
    Inc(FLockCount);
end;

procedure TSDSession.UnLockSession;
begin
  Dec(FLockCount);
  if FLockCount = 0 then
    LeaveCriticalSection(FCSect);
end;

procedure TSDSession.MakeCurrent;
begin
  SetActive(True);
end;

function TSDSession.DoOpenDatabase(const DatabaseName: string; AOwner: TComponent): TSDDatabase;
var
  TempDatabase: TSDDatabase;
begin
  Result := nil;
  TempDatabase := nil;
  LockSession;
  try
    try
      Result := DoFindDatabase(DatabaseName, AOwner);
      if Result = nil then begin
        TempDatabase := TSDDatabase.Create(Self);
        TempDatabase.RemoteDatabase	:= DatabaseName;
        TempDatabase.DatabaseName	:= DatabaseName;
        TempDatabase.KeepConnection := not(csDesigning in ComponentState) and FKeepConnections;
        TempDatabase.Temporary		:= True;
        Result := TempDatabase;
      end;
      Result.Open;
      Inc(Result.FRefCount);
    except
      TempDatabase.Free;
      raise;
    end;
  finally
    UnLockSession;
  end;
end;

function TSDSession.OpenDatabase(const DatabaseName: string): TSDDatabase;
begin
  Result := DoOpenDatabase(DatabaseName, nil);
end;

procedure TSDSession.GetStoredProcNames(const DatabaseName: string; List: TStrings);
var
  db: TSDDatabase;
begin
  db := FindDatabase(DatabaseName);
  if Assigned(db) then
    db.ISrvGetStoredProcNames(List);
end;

procedure TSDSession.GetTableNames(const DatabaseName, Pattern: string; SystemTables: Boolean; List: TStrings);
var
  db: TSDDatabase;
begin
  db := FindDatabase(DatabaseName);
  if Assigned(db) then
    db.ISrvGetTableNames(Pattern, SystemTables, List);
end;

{ It's necessary for UpdateSQL Editor }
procedure TSDSession.GetTableFieldNames(const DatabaseName, TableName: string; List: TStrings);
var
  db: TSDDatabase;
begin
  db := FindDatabase(DatabaseName);
  if Assigned(db) then
    db.ISrvGetTableFieldNames(TableName, List);
end;

procedure TSDSession.RemoveDatabase(Value: TSDDatabase);
begin
	// it's necessary, when TSDDatabase instance is disposing after it's FSession component
  if not Assigned(FDatabases) then Exit;
  FDatabases.Remove(Value);
end;

procedure TSDSession.UpdateAutoSessionName;
begin
  FUpdatingAutoSessionName := True;
  try
    SessionName := Format('%s_%d', [Name, FSessionNumber + 1]);
  finally
    FUpdatingAutoSessionName := False;
  end;
  SetSessionNames;
end;

procedure TSDSession.ValidateAutoSession(AOwner: TComponent; AllSessions: Boolean);
var
  I: Integer;
  Component: TComponent;
begin
  if AOwner <> nil then
    for I := 0 to AOwner.ComponentCount - 1 do begin
      Component := AOwner.Components[I];
      if (Component <> Self) and (Component is TSDSession) then
        if AllSessions then
          DatabaseError(SAutoSessionExclusive)
        else if TSDSession(Component).AutoSessionName then
          DatabaseErrorFmt(SAutoSessionExists, [Component.Name]);
    end;
end;

{$IFNDEF SD_VCL5}

{ TSDCustomDatabase }

constructor TSDCustomDatabase.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDataSets := TList.Create;
end;

destructor TSDCustomDatabase.Destroy;
begin
  inherited Destroy;
  SetConnected(False);
  FDataSets.Free;
end;

procedure TSDCustomDatabase.Loaded;
begin
  inherited Loaded;

  try
    if FStreamedConnected then
      SetConnected(True);
  except
    if csDesigning in ComponentState then
      Application.HandleException(Self)
    else
      raise;
  end;
end;

procedure TSDCustomDatabase.Open;
begin
  SetConnected(True);
end;

procedure TSDCustomDatabase.Close;
begin
  SetConnected(False);
end;

function TSDCustomDatabase.GetConnected: Boolean;
begin
  Result := False;
end;

procedure TSDCustomDatabase.SetConnected(Value: Boolean);
begin
  if (csReading in ComponentState) and Value then
    FStreamedConnected := True
  else begin
    if Value = GetConnected then
      Exit;
    if Value then begin
      if Assigned(BeforeConnect) then BeforeConnect(Self);
      DoConnect;
      if Assigned(AfterConnect) then AfterConnect(Self);
    end else begin
      if Assigned(BeforeDisconnect) then BeforeDisconnect(Self);
      DoDisconnect;
      if Assigned(AfterDisconnect) then AfterDisconnect(Self);
    end;
  end;
end;

procedure TSDCustomDatabase.DoConnect;
begin
end;

procedure TSDCustomDatabase.DoDisconnect;
begin
end;

procedure TSDCustomDatabase.RegisterClient(Client: TObject);
begin
  if Client is TDataSet then
    FDataSets.Add(Client);
end;

procedure TSDCustomDatabase.UnRegisterClient(Client: TObject);
begin
  if Client is TDataSet then
    FDataSets.Remove(Client);
end;

function TSDCustomDatabase.GetDataSet(Index: Integer): TDataSet;
begin
  Result := FDataSets[Index];
end;

function TSDCustomDatabase.GetDataSetCount: Integer;
begin
  Result := FDataSets.Count;
end;
{$ENDIF}

{*******************************************************************************
				TSDDatabase
*******************************************************************************}
constructor TSDDatabase.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  if FSession = nil then
    if AOwner is TSDSession then
      FSession := TSDSession(AOwner) else
      FSession := DefaultSession;
  SessionName := FSession.SessionName;
  FSession.AddDatabase(Self);

  FParams := TStringList.Create;
  FRefCount := 0;

  FTransIsolation := tiReadCommitted;
  LoginPrompt	:= True;

  FServerType	:= stSQLBase;
  FSrvDatabase	:= nil;

  FClientVersion:= 0;
  FServerVersion:= 0;
  FVersion	:= '';

  FKeepConnection := True;
  FInTransaction := False;
  FIsConnectionBusy:= False;
end;

destructor TSDDatabase.Destroy;
begin
  IdleTimeOut := 0;
  
  Close;

  if FSession <> nil then
    FSession.RemoveDatabase(Self);
  FParams.Free;
  
  inherited Destroy;
end;

procedure TSDDatabase.Loaded;
begin
  inherited Loaded;

  if not StreamedConnected then CheckSessionName(False);
end;

procedure TSDDatabase.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (AComponent = FSession) and
     (FSession <> DefaultSession)
  then begin
    Close;
    SessionName := '';
  end;
end;

procedure TSDDatabase.ApplyUpdates(const DataSets: array of TSDDataSet);
var
  I: Integer;
  DS: TSDDataSet;
begin
  StartTransaction;
  try
    for I := 0 to High(DataSets) do begin
      DS := DataSets[I];
      if not Assigned( DS ) then
        Continue;
      if DS.Database <> Self then
        DatabaseError(Format(SUpdateWrongDB, [DS.Name, Name]));
      DS.ApplyUpdates;
    end;
    Commit;
  except
    Rollback;
	// mark records as not saved
    for I := 0 to High(DataSets) do
      if Assigned( DataSets[I] ) then
        DataSets[I].RollbackUpdates;
    raise;
  end;
  for I := 0 to High(DataSets) do
    if Assigned( DataSets[I] ) then
      DataSets[I].CommitUpdates;
end;

procedure TSDDatabase.CheckActive;
begin
  if not GetConnected then
    DatabaseError(SDatabaseClosed);
end;

procedure TSDDatabase.CheckInactive;
begin
  if GetConnected then
    if csDesigning in ComponentState then
      Close
    else
      DatabaseError(SDatabaseOpen);
end;

procedure TSDDatabase.CheckInTransaction;
begin
  if not FInTransaction then
    DatabaseError( SNotInTransaction );
end;

procedure TSDDatabase.CheckNotInTransaction;
begin
  if FInTransaction then
    DatabaseError( SInTransaction );
end;

procedure TSDDatabase.CheckDatabaseName;
begin
  if (FDatabaseName = '') and not Temporary then
    DatabaseError(SDatabaseNameMissing);
end;

{ Sets FSession property and sets on FSession.Active in case of Required is True }
procedure TSDDatabase.CheckSessionName(Required: Boolean);
var
  NewSession: TSDSession;
begin
  if Required then
    NewSession := Sessions.List[FSessionName]
  else
    NewSession := Sessions.FindSession(FSessionName);
  if (NewSession <> nil) and (NewSession <> FSession) then begin
    if (FSession <> nil) then FSession.RemoveDatabase(Self);
    FSession := NewSession;
    FSession.FreeNotification(Self);
    FSession.AddDatabase(Self);
    try
      ValidateName(FDatabaseName);
    except
      FDatabaseName := '';
      raise;
    end;
  end;
  if Required then
    FSession.Active := True;
end;

procedure TSDDatabase.CloseDataSets;
begin
  while DataSetCount <> 0 do
    TSDDataSet( DataSets[DataSetCount-1] ).Disconnect;
end;

procedure TSDDatabase.StartTransaction;
begin
  CheckActive;
  CheckNotInTransaction;

  FSrvDatabase.StartTransaction;

  FInTransaction := True;
end;

procedure TSDDatabase.Commit;
var
  i: Integer;
begin
  CheckActive;
  CheckInTransaction;

  if Assigned(SrvDatabase) and not SrvDatabase.CursorPreservedOnCommit then begin
    for i:=0 to DataSetCount - 1 do
      if DataSets[i].Active and
         Assigned(DataSets[i].SrvDataSet) and
         not DataSets[i].SrvDataSet.PreservedOnCommit
      then
        DataSets[i].FetchAll;
  end;
  FSrvDatabase.Commit;

  FInTransaction := False;
end;

{ After rollback, for example, DB2, SQLBase could be destroyed }
procedure TSDDatabase.Rollback;
var
  i: Integer;
  bPrepared: Boolean;
begin
  CheckActive;
  CheckInTransaction;

  if Assigned(SrvDatabase) and not SrvDatabase.CursorPreservedOnRollback then
    for i:=0 to DataSetCount - 1 do begin
      bPrepared := DataSets[i].Active or
      	((DataSets[i] is TSDQuery) and (TSDQuery(DataSets[i]).Prepared)) or
        ((DataSets[i] is TSDStoredProc) and (TSDStoredProc(DataSets[i]).Prepared));
      if bPrepared and
         Assigned( DataSets[i].SrvDataSet ) and
         not DataSets[i].SrvDataSet.PreservedOnRollback
      then begin
        if DataSets[i].Active then begin
          DataSets[i].FetchAll;     
          DataSets[i].ISrvCloseResultSet;
        end;
		// it is necessary to destroy cursor explicitly before rollback
        DataSets[i].Detach;	// dataset must stay active
      end;
    end;
  FSrvDatabase.Rollback;

  FInTransaction := False;
end;

function TSDDatabase.GetConnected: Boolean;
begin
  Result := FSrvDatabase <> nil;
end;

function TSDDatabase.GetDataSet(Index: Integer): TDataSet;
begin
  Result := inherited GetDataSet(Index) as TSDDataSet;
end;

function TSDDatabase.GetSDDataSet(Index: Integer): TSDDataSet;
begin
  Result := GetDataSet(Index) as TSDDataSet;
end;

function TSDDatabase.GetHandle: PSDCursor;
begin
  Result := nil;
  if Assigned(FSrvDatabase) then
    Result := FSrvDatabase.Handle;
end;

procedure TSDDatabase.SetHandle(Value: PSDCursor);
var
  ISrvDatabaseClass: TISrvDatabaseClass;
begin
	// check server type
  if (PSDHandleRec(Value)^.SrvType < Ord(Low(ServerType))) or
     (PSDHandleRec(Value)^.SrvType > Ord(High(ServerType)))
  then
    DatabaseError( SBadServerType );

  if Ord(ServerType) <> PSDHandleRec(Value)^.SrvType then
    DatabaseErrorFmt(
    	SServerTypeMismatch,
    	[GetEnumName( TypeInfo(TSDServerType), PSDHandleRec(Value)^.SrvType ),
         GetEnumName( TypeInfo(TSDServerType), Ord(ServerType) )
        ]
    );

  if Connected then Close;
  if Value <> nil then begin
    CheckDatabaseName;
    CheckSessionName(True);

    ISrvDatabaseClass := SrvDatabaseClasses[ ServerType ];
    if ISrvDatabaseClass = nil then
      DatabaseError( SNoServerType );

    FSrvDatabase := ISrvDatabaseClass.CreateWithHandle( Self, Value );

    FAcquiredHandle := True;
  end;
end;

function TSDDatabase.GetIsSQLBased: Boolean;
begin
  Result := True;
end;

{ server info with full version and name }
function TSDDatabase.GetVersion: string;
begin
  if Assigned(FSrvDatabase) and (Length(FVersion) = 0) then
    FVersion := FSrvDatabase.GetVersionString;

  Result := FVersion;
end;

{ server version as MajorVer.MinorVer }
function TSDDatabase.GetServerMajor: Word;
begin
  if Assigned(FSrvDatabase) and (FServerVersion <= 0) then
    FServerVersion := FSrvDatabase.GetServerVersion;

  Result := HiWord( FServerVersion );
end;

function TSDDatabase.GetServerMinor: Word;
begin
  if Assigned(FSrvDatabase) and (FServerVersion <= 0) then
    FServerVersion := FSrvDatabase.GetServerVersion;

  Result := LoWord( FServerVersion );
end;

{ client version as MajorVer.MinorVer (-1 and 0 - unloaded and undefined) }
function TSDDatabase.GetClientMajor: Word;
begin
  if Assigned(FSrvDatabase) and (FClientVersion <= 0) then
    FClientVersion := FSrvDatabase.GetClientVersion;

  Result := HiWord( FClientVersion );
end;

{ client version as MajorVer.MinorVer (-1 and 0 - unloaded and undefined) }
function TSDDatabase.GetClientMinor: Word;
begin
  if Assigned(FSrvDatabase) and (FClientVersion <= 0) then
    FClientVersion := FSrvDatabase.GetClientVersion;

  Result := LoWord( FClientVersion );
end;

procedure TSDDatabase.Login(LoginParams: TStrings);
var
  UserName, Password: string;
  LoginProc: TSDLoginEvent;
begin
  LoginProc := OnLogin;
  if Assigned(LoginProc) then
    LoginProc(Self, LoginParams)
  else begin
    UserName := LoginParams.Values[szUSERNAME];
    if not LoginDialog(DatabaseName, UserName, Password) then
      DatabaseErrorFmt(SLoginError, [DatabaseName]);
    LoginParams.Values[szUSERNAME] := UserName;
    LoginParams.Values[szPASSWORD] := Password;
  end;
end;

procedure TSDDatabase.CheckRemoteDatabase(var Password: string);
var
  RDbParams, LoginParams: TStrings;
begin
  Password := '';

  if (RemoteDatabase = '') then begin
    DatabaseError(SRemoteDatabaseNameMissing);
    Exit;
  end;

  LoginParams := TStringList.Create;
  RDbParams := TStringList.Create;
  try
    Session.GetDatabaseParams(FRemoteDatabase, ServerType, RDbParams);

    if LoginPrompt then begin
      if FParams.Values[szUSERNAME] = '' then
        FParams.Values[szUSERNAME] := RDbParams.Values[szUSERNAME];

      LoginParams.Values[szUSERNAME] := FParams.Values[szUSERNAME];
      Login(LoginParams);

      Password := LoginParams.Values[szPASSWORD];
      FParams.Values[szUSERNAME] := LoginParams.Values[szUSERNAME];
    end else begin
      if FParams.Count > 0 then
        Password := FParams.Values[szPASSWORD]
      else begin
        FParams.Assign(RDbParams);
        Password := FParams.Values[szPASSWORD];
      end;
    end;

    if LoginPrompt then
      RDbParams.Assign(LoginParams)
    else
      RDbParams.Assign(FParams);
	// store the last parameters for the following connects
    if RDbParams.Count > 0 then
      Session.InternalAddDatabase(FRemoteDatabase, ServerType, RDbParams);
  finally
    LoginParams.Free;
    RDbParams.Free;
  end;
end;

procedure TSDDatabase.InitSrvDatabase(const ADatabaseName, AUserName, APassword: string);
var
  ISrvDatabaseClass: TISrvDatabaseClass;
begin
  ASSERT( FSrvDatabase = nil );

  ISrvDatabaseClass := SrvDatabaseClasses[ ServerType ];
  if ISrvDatabaseClass = nil then
    DatabaseError( SNoServerType );

  FSrvDatabase := ISrvDatabaseClass.CreateConnected(
  				Self, ADatabaseName, AUserName, APassword );

  FSrvDatabase.SetDefaultParams;
end;

procedure TSDDatabase.DoneSrvDatabase;
begin
  if FSrvDatabase = nil then Exit;

  FSrvDatabase.Free;
  FSrvDatabase := nil;

  ResetServerInfo;
end;

procedure TSDDatabase.ResetServerInfo;
begin
  FVersion	:= '';
  FServerVersion:= 0;
  FClientVersion:= 0;
end;

procedure TSDDatabase.DoConnect;
{$IFDEF EVAL}
  procedure ShowRegReminder;
  var
    i: Integer;
  begin
    for i:=0 to Session.DatabaseCount-1 do
      if Session.Databases[i].Connected then
        Exit;
    ShowReminderBox;
  end;
{$ENDIF}
var
  DBPassword: string;
begin
  if Connected then
    Exit;
{$IFDEF EVAL}
  ShowRegReminder;
{$ENDIF}
  CheckDatabaseName;
	// here used Login method
  CheckRemoteDatabase(DBPassword);

  try
    InitSrvDatabase( RemoteDatabase, FParams.Values[szUSERNAME], DBPassword );
	// The property can be True, when database is disconnected during transaction
    FInTransaction := False;
  except
    DoneSrvDatabase;
    raise;
  end;
end;

procedure TSDDatabase.DoDisconnect;
begin
  CloseDataSets;
  FRefCount := 0;

  InternalClose( False );
end;

procedure TSDDatabase.InternalClose(Force: Boolean);
begin
  if GetConnected then begin
    if not FAcquiredHandle then begin
      FSrvDatabase.Logoff( Force );
      DoneSrvDatabase;
    end else begin
      DoneSrvDatabase;
      FAcquiredHandle := False;
    end;
  end;
end;

procedure TSDDatabase.ForceClose;
begin
  while DataSetCount <> 0 do
    TSDDataSet( DataSets[DataSetCount-1] ).ForceClose;
  FRefCount := 0;

  InternalClose( True );
end;

procedure TSDDatabase.SetRemoteDatabase(const Value: string);
begin
  if FRemoteDatabase <> Value then begin
    CheckInactive;
    FRemoteDatabase := Value;
  end;
end;

procedure TSDDatabase.SetDatabaseName(const Value: string);
begin
  if csReading in ComponentState then
    FDatabaseName := Value
  else if FDatabaseName <> Value then begin
    CheckInactive;
    ValidateName(Value);
    FDatabaseName := Value;
  end;
end;

procedure TSDDatabase.SetKeepConnection(Value: Boolean);
begin
  if FKeepConnection <> Value then begin
    FKeepConnection := Value;
    if not Value and (FRefCount = 0) then
      Close;
  end;
end;

procedure TSDDatabase.SetParams(Value: TStrings);
begin
  FParams.Assign(Value);
end;

procedure TSDDatabase.SetServerType(Value: TSDServerType);
begin
  if Value <> FServerType then begin
    CheckInactive;
    FServerType := Value;
  end;
end;

procedure TSDDatabase.SetSessionName(const Value: string);
begin
  if csReading in ComponentState then
    FSessionName := Value
  else begin
    CheckInactive;
    if FSessionName <> Value then begin
      FSessionName := Value;
      CheckSessionName(False);
    end;
  end;
end;

procedure TSDDatabase.SetTransIsolation(Value: TSDTransIsolation);
begin
  if FTransIsolation = Value then Exit;

  if GetConnected then
    FSrvDatabase.SetTransIsolation(Value);
  FTransIsolation := Value;
end;

procedure TSDDatabase.ValidateName(const Name: string);
var
  Database: TSDDatabase;
begin
  if Name <> '' then begin
    Database := Session.FindDatabase(Name);
    if (Database <> nil) and (Database <> Self) then begin
      if (not Database.Temporary) or (Database.FRefCount <> 0) then
        DatabaseErrorFmt(SDuplicateDatabaseName, [Name]);
      Database.Free;
    end;
  end;
end;

{		SQL-servers C/API calls		}
function TSDDatabase.ISrvParamValue(Value: TSDDatabaseParam): Integer;
begin
  Result := FSrvDatabase.ParamValue(Value);
end;

function TSDDatabase.ISrvFieldDataType(ExtDataType: Integer): TFieldType;
begin
  Result := FSrvDatabase.FieldDataType(ExtDataType);
end;

procedure TSDDatabase.ISrvGetStoredProcNames(List: TStrings);
begin
  if not Connected then
    Open;

  FSrvDatabase.GetStoredProcNames(List);
end;

procedure TSDDatabase.ISrvGetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings);
begin
  if not Connected then
    Open;

  FSrvDatabase.GetTableNames(Pattern, SystemTables, List);
end;

procedure TSDDatabase.ISrvGetTableFieldNames(const TableName: string; List: TStrings);
begin
  if not Connected then
    Open;

  FSrvDatabase.GetTableFieldNames(TableName, List);
end;

function TSDDatabase.ISrvNativeDataSize(FieldType: TFieldType): Word;
begin
  Result := FSrvDatabase.NativeDataSize(FieldType);
end;

function TSDDatabase.ISrvNativeDataType(FieldType: TFieldType): Byte;
begin
  Result := FSrvDatabase.NativeDataType(FieldType);
end;

{
	for Oracle only:
if possible then convert ftFloat(NUMBER) to ftInteger ( Precision <= 9 (10 digits >= 1 billion )
}
function TSDDatabase.ISrvNumberDataType(FieldType: TFieldType; Precision, Scale: Integer): TFieldType;
const
  MAX_INT_PRECISION = 9;
begin
  Result := FieldType;
  if (FieldType = ftFloat) and (Scale = 0) and
     (Precision >= 1) and (Precision <= (MAX_INT_PRECISION-1))
  then
    Result := ftInteger;
end;

{ Returns True if field type requires converting from internal database format }
function TSDDatabase.ISrvRequiredCnvtFieldType(FieldType: TFieldType): Boolean;
begin
  Result := FSrvDatabase.RequiredCnvtFieldType(FieldType);
end;

{ IdleTimer processing }
function TSDDatabase.GetIdleTimeOut: Integer;
begin
  Result := FIdleTimeOut;
end;

procedure TSDDatabase.SetIdleTimeOut(Value: Integer);
begin
  if Value <> FIdleTimeOut then begin
    if not (csDesigning in ComponentState) then
      UpdateTimer( Value );
    FIdleTimeOut := Value;
  end;
end;

procedure TSDDatabase.UpdateTimer(NewTimeout: Integer);
begin
  if NewTimeout > 0 then begin
    if not Assigned( FTimer ) then begin
      FTimer := TSDThreadTimer.Create(Self, True);
      FTimer.FreeOnTerminate := False;
      FTimer.OnTimer := IdleTimerHandler;
      FTimer.Interval := NewTimeout;
      FTimer.Resume;
    end else
      FTimer.Interval := NewTimeout;
  end else begin
    FTimer.Terminate;
    FTimer.WaitFor;
    FTimer.Free;
    FTimer := nil;
  end;
end;

procedure TSDDatabase.IdleTimerHandler(Sender: TObject);
begin
  if Connected then begin
    if FIdleTimeoutStarted then begin
      if not FIsConnectionBusy
      then Close
      else FIdleTimeoutStarted := False		// connection is processed at server-side
    end else
      FIdleTimeoutStarted := True;
  end;
end;


(*******************************************************************************
	TSDThreadTimer - to implement TSDDatabase.IdleTimeOut property
*******************************************************************************)

constructor TSDThreadTimer.Create(ADatabase: TSDDatabase; CreateSuspended: Boolean);
begin
  inherited Create(CreateSuspended);

  FDatabase := ADatabase;
  FInterval := 1000;
end;

destructor TSDThreadTimer.Destroy;
begin
  inherited Destroy;
end;

procedure TSDThreadTimer.Execute;
const
  MinInterval = 500;
var
  ev: TEvent;
  i: Integer;	// the variable can be negative (<=0)
begin
  ev := TEvent.Create(nil, False, False, '');
  try
    while not Terminated do begin
    	// start count down
      i := Interval;
      	// to minimize an application shutdown, it's necessary to check Terminated more often
      while not Terminated and (i > 0) do begin
        ev.WaitFor( MinInterval );
        i := i - MinInterval;
      	// timeout is broken
        if not FDatabase.FIdleTimeoutStarted  then
          Break;
      end;
      Timer;
    end;
  finally
    ev.Free;
  end;
end;

procedure TSDThreadTimer.SetInterval(Value: Integer);
begin
  if Value <> FInterval then
    FInterval := Value;
end;

procedure TSDThreadTimer.SetOnTimer(Value: TNotifyEvent);
begin
  FOnTimer := Value;
end;

procedure TSDThreadTimer.Timer;
begin
  if not Terminated and Assigned(FOnTimer) then FOnTimer(Self);
end;


(*******************************************************************************
  				TSDResultSet
*******************************************************************************)
constructor TSDResultSet.Create(ADataSet: TSDDataSet);
begin
  inherited Create;

  FDataSet := ADataSet;
  FAllInCache := False;
  FPosition := -1;
  FDeletedCount := 0;
end;

destructor TSDResultSet.Destroy;
begin
  Clear;
  inherited Destroy;
end;

procedure TSDResultSet.Clear;
var
  i: Integer;
begin
  FPosition := -1;

  for i:=0 to Count-1 do
    ClearCacheItem(i);

  FDeletedCount := 0;
  FAllInCache := False;

  inherited Clear;
end;

{ Empties a cache item, but it does not remove the item }
procedure TSDResultSet.ClearCacheItem(Index: Integer);
begin
  if Items[Index] = nil then
    Exit;

  if PCacheRecInfo(Items[Index])^.CurRec <> nil then begin
    FDataSet.ClearBlobCache( PCacheRecInfo(Items[Index])^.CurRec );
    FDataSet.FreeRecordBuffer( PCacheRecInfo(Items[Index])^.CurRec );
  end;

  if PCacheRecInfo(Items[Index])^.OldRec <> nil then begin
    FDataSet.ClearBlobCache( PCacheRecInfo(Items[Index])^.OldRec );
    FDataSet.FreeRecordBuffer( PCacheRecInfo(Items[Index])^.OldRec );
  end;

  Dispose( PCacheRecInfo(Items[Index]) );
  Items[Index] := nil;
end;

procedure TSDResultSet.SetToBegin;
begin
  FPosition := -1;
end;

procedure TSDResultSet.SetToEnd;
begin
  FPosition := Count;
end;

function TSDResultSet.GetFilterActivated: Boolean;
begin
  Result := FDataSet.FFilterActivated;
end;

function TSDResultSet.RecordFilter(RecBuf: Pointer): Boolean;
begin
  Result := FDataSet.RecordFilter(RecBuf);
end;

{ Gets the prior visible record }
function TSDResultSet.GetPriorRecord: Boolean;
begin
  Result := False;
  	// if the record was deleted, then begin from the last record in the cache
  if FPosition >= Count then
    FPosition := Count;
  if FPosition >= 0 then begin
    Dec(FPosition);
    while FPosition >= 0 do begin
      if IsVisibleRecord(FPosition) then begin
        Result := True;
        Break;
      end;
      Dec(FPosition);
    end;
  end;
end;

{ Gets the current visible record, in case of necessity it will be fetched }
function TSDResultSet.GetCurrRecord: Boolean;
begin
  Result := False;

  if FPosition >= Count then
    FPosition := Count - 1;

  while (FPosition >= 0) and (FPosition < Count) do begin
    if IsVisibleRecord(FPosition) then begin
      Result := True;
      Break;
    end;
    if not GotoNextRecord then
      Break;	// the current record is last
  end;
end;

{ Gets the next visible record, in case of necessity it will be fetched }
function TSDResultSet.GetNextRecord: Boolean;
begin
  Result := False;

  repeat
    if not GotoNextRecord then
      Break;	// the current record is last

    if IsVisibleRecord(FPosition) then
      Result := True;
  until Result;
end;

{ Go to the next record in result set, in case of necessity it will be fetched }
function TSDResultSet.GotoNextRecord: Boolean;
begin
	// if the next record is in cache
  if (FPosition + 1) < Count then begin
    Inc(FPosition);
    Result := True;
  end else begin
  	// if FPosition exceeds Count, for example, the last visible rows were deleted
    if FPosition >= Count then
      FPosition := Count - 1;
        // FetchRecord returns True and increases FPosition in case of success, else
        // it returns False, if all records were fetched (all in cache).
    Result := FetchRecord;
  end;
end;

{ The following 2 methods are used by filtered dataset (with property Filtered=True) }
function TSDResultSet.FindNextRecord: Boolean;
begin
  Result := GetNextRecord;
end;

function TSDResultSet.FindPriorRecord: Boolean;
begin
  Result := False;
  while FPosition > 0 do begin
    Dec(FPosition);
    if IsVisibleRecord( FPosition ) then begin
      Result := True;
      Break;
    end;
  end;
end;

procedure TSDResultSet.FetchAll;
var
  SavePos: Integer;
begin
  if FAllInCache then Exit;

  SavePos := FPosition;
  while FetchRecord do
    ;
  FPosition := SavePos;
end;

{ Method fetchs the next record, which is cached, and increments FPosition in case of success }
function TSDResultSet.FetchRecord: Boolean;
var
  RecBuf: PChar;
begin
	// if full result set is fetched or cursor was detached (offline)
  if FAllInCache or not Assigned(FDataSet.FSrvDataSet) then begin
    Result := False;
    Exit;
  end;

  Result := FDataSet.FSrvDataSet.FetchNextRow;
  if Result then begin
    RecBuf := FDataSet.AllocRecordBuffer;

    with PRecInfo(RecBuf + FDataSet.FRecInfoOffs)^ do begin
      UpdateStatus := usUnmodified;
      RecordNumber := FPosition+1;
    end;
    with PBookmarkRec(RecBuf + FDataSet.FBookmarkOffs)^ do
      iPos := FPosition+1;

    AddRecord(RecBuf);
    Inc(FPosition);

	// get data from select buffer
    FDataSet.FSrvDataSet.ConvertSelectBuffer( RecBuf );
  end else
	// drop server cursor
    if FDataSet.FDetachOnFetchAll then
      FDataSet.DestroyHandle;

  FAllInCache := not Result;
end;

function TSDResultSet.GetCurRecords(Index: Integer): PChar;
begin
  ASSERT( (Index >= 0) and (Index < Count), '' );

  if not Assigned(Items[Index]) then
    DatabaseError(SInvalidOperation);
  Result := PCacheRecInfo(Items[Index])^.CurRec;
end;

procedure TSDResultSet.SetCurRecords(Index: Integer; Value: PChar);
begin
  ASSERT( (Index >= 0) and (Index < Count), '' );

  if not Assigned(Items[Index]) then
    DatabaseError(SInvalidOperation);
  PCacheRecInfo(Items[Index])^.CurRec := Value;
end;

function TSDResultSet.GetOldRecords(Index: Integer): PChar;
begin
  ASSERT( (Index >= 0) and (Index < Count), '' );

  if not Assigned(Items[Index]) then
    DatabaseError(SInvalidOperation);
  Result := PCacheRecInfo(Items[Index])^.OldRec;
end;

procedure TSDResultSet.SetOldRecords(Index: Integer; Value: PChar);
begin
  ASSERT( (Index >= 0) and (Index < Count), '' );

  if not Assigned(Items[Index]) then
    DatabaseError(SInvalidOperation);
  PCacheRecInfo(Items[Index])^.OldRec := Value;
end;

function TSDResultSet.GetAppliedRecords(Index: Integer): Boolean;
begin
  ASSERT( (Index >= 0) and (Index < Count), '' );

  if Items[Index] = nil then
    Result := True
  else
    Result := PCacheRecInfo(Items[Index])^.Applied;
end;

procedure TSDResultSet.SetAppliedRecords(Index: Integer; Value: Boolean);
begin
  ASSERT( (Index >= 0) and (Index < Count), '' );

  if Items[Index] = nil then
    Exit;
  if Value <> PCacheRecInfo(Items[Index])^.Applied then
    PCacheRecInfo(Items[Index])^.Applied := Value;
end;

function TSDResultSet.GetUpdateStatusRecords(Index: Integer): TUpdateStatus;
begin
  ASSERT( (Index >= 0) and (Index < Count), '' );

  if Items[Index] = nil then
    Result := usUnModified
  else
    Result := PRecInfo( CurRecords[Index] + FDataSet.FRecInfoOffs )^.UpdateStatus;
end;

procedure TSDResultSet.SetUpdateStatusRecords(Index: Integer; Value: TUpdateStatus);
var
  CurStatus: TUpdateStatus;
begin
  ASSERT( (Index >= 0) and (Index < Count), '' );

  if Items[Index] = nil then
    Exit;
  CurStatus := PRecInfo( CurRecords[Index] + FDataSet.FRecInfoOffs )^.UpdateStatus;
  if Value = CurStatus then
    Exit;

  if CurStatus = usDeleted then
    Dec( FDeletedCount );
	// save update status for the required record
  PRecInfo( CurRecords[Index] + FDataSet.FRecInfoOffs )^.UpdateStatus := Value;

  if Value = usDeleted then
    Inc( FDeletedCount );
end;

{ If the record is exist in cache only(not in database), then returns True }
function TSDResultSet.IsInsertedRecord(Index: Integer): Boolean;
begin
  if Items[Index] = nil then
    Result := False
  else
    Result := PRecInfo( CurRecords[Index] + FDataSet.FRecInfoOffs)^.BookmarkFlag = bfInserted;
end;

{ In a filter mode check what record is accessible }
function TSDResultSet.GetRecordCount: Integer;
var
  i, n: Integer;
begin
  if FDataSet.Filtered then begin
    i := 0;
    n := 0;
    while i < Count do begin
      if IsVisibleRecord(i) then
        Inc(n);
      Inc(i);
    end;
    Result := n;
  end else begin
    Result := Count - FDeletedCount;
  	// It can't be never (for checking)
    if Result < 0 then
      Result := 0;
  end;
end;

{ Copies a record buffer including Blob part }
procedure TSDResultSet.CopyRecBuf(var Source, Dest);
var
  i: Integer;
begin
  with FDataSet do begin
	// copy non-blob fields
    System.Move( Source, Dest, FBlobCacheOffs );
    	// copy TRecInfo and TBookmarkRec fields (at the end of buffer)
    System.Move( Pointer(PChar(@Source) + FRecInfoOffs)^, Pointer(PChar(@Dest) + FRecInfoOffs)^, FRecBufSize - FRecInfoOffs );
	// copy data of Blob fields
    if BlobFieldCount > 0 then
      for i:=0 to FieldCount-1 do
        if Fields[i].IsBlob then
          PBlobDataArray(PChar(@Dest) + FBlobCacheOffs)[Fields[i].Offset] :=
      		PBlobDataArray(PChar(@Source) + FBlobCacheOffs)[Fields[i].Offset];
  end;
end;

function TSDResultSet.GetRecord(Buffer: PChar; GetMode: TGetMode): Boolean;
begin
  Result := False;

  case GetMode of
    gmCurrent:
      Result := GetCurrRecord;
    gmNext:
      Result := GetNextRecord;
    gmPrior:
      Result := GetPriorRecord;
  end;
  if Result then
    CopyRecBuf(CurRecords[FPosition]^, Buffer^);
end;

{  Checks record visibility for user (for example, in TDBGrid) }
function TSDResultSet.IsVisibleRecord(Index: Integer): Boolean;
begin
  ASSERT( (Index >= 0) and (Index < Count), '' );
  Result := False;
  case UpdateStatusRecords[Index] of
    usUnmodified:Result := rtUnmodified in FDataSet.FUpdateRecordTypes;
    usModified:	Result := rtModified in FDataSet.FUpdateRecordTypes;
    usInserted:	Result := rtInserted in FDataSet.FUpdateRecordTypes;
    usDeleted:	Result := rtDeleted in FDataSet.FUpdateRecordTypes;
  end;

  if FilterActivated and Result then
    Result := RecordFilter(CurRecords[Index]);
end;

{ Inserts(Saves) the selected(from result set) record RecBuf to the cache }
function TSDResultSet.AddRecord(RecBuf: PChar): Integer;
var
  pRecInfo: PCacheRecInfo;
  i: Integer;
begin
  New(pRecInfo);
  pRecInfo^.CurRec := RecBuf;
  pRecInfo^.OldRec := nil;
  pRecInfo^.Applied := True;
  Result := Add(pRecInfo);
	// clear cache in case of necessity
  if FDataSet.Unidirectional then begin
	// BufferCount is equal 1 (when the dataset is not associated with visual components) or depends from datalinks
    i := Count - FDataSet.BufferCount - 1;
    if (i >= 0) and (Items[i] <> nil) and (UpdateStatusRecords[i] = usUnModified) then
      ClearCacheItem(i);
  end;
end;

{
  If a record still not exist in database, then remove it from the cache.
Else only marks the record in the cache as deleted
}
procedure TSDResultSet.DeleteRecord(RecBuf: PChar);
var
  Idx: Integer;
begin
  Idx := PBookmarkRec(RecBuf + FDataSet.FBookmarkOffs)^.iPos;
  	// if record still not in database
  if UpdateStatusRecords[Idx] = usInserted then
    DeleteCacheItem( Idx )
  else begin
  	// else mark for delete in database
    UpdateStatusRecords[Idx] := usDeleted;
    AppliedRecords[Idx] := False;
  end;
end;

{ Adds inserted (of the user) record at the end of cache }
procedure TSDResultSet.InsertRecord(RecBuf: PChar; Append, SetCurrent: Boolean);
var
  Buf: PChar;
  pRec: PCacheRecInfo;
  i: Integer;
begin
  New(pRec);
  Buf := FDataSet.AllocRecordBuffer;
  CopyRecBuf(RecBuf^, Buf^);	// copy with Blob-fields

  PRecInfo( Buf + FDataSet.FRecInfoOffs)^.UpdateStatus := usInserted;
  PRecInfo( Buf + FDataSet.FRecInfoOffs)^.BookmarkFlag := bfCurrent;

  pRec^.CurRec := Buf;
  pRec^.OldRec := nil;
  pRec^.Applied := False;
  	// if SetBookmarkData method still not called and TBookmarkRec structure still not initialized
  if PBookmarkRec( Buf + FDataSet.FBookmarkOffs)^.iPos < 0 then begin
    if Append then
      PBookmarkRec( Buf + FDataSet.FBookmarkOffs)^.iPos := Count
    else
      if FPosition >= 0
      then PBookmarkRec( Buf + FDataSet.FBookmarkOffs)^.iPos := FPosition
      else PBookmarkRec( Buf + FDataSet.FBookmarkOffs)^.iPos := 0;
  end;
  	// insert at the cache in required position
  Insert(PBookmarkRec( Buf + FDataSet.FBookmarkOffs)^.iPos, pRec);
	// if new record is inserted at center of the cache, then update the following record info
  for i:= PBookmarkRec( Buf + FDataSet.FBookmarkOffs)^.iPos + 1 to Count-1 do begin
    PBookmarkRec( CurRecords[i] + FDataSet.FBookmarkOffs)^.iPos := i;
    if OldRecords[i] <> nil then
      PBookmarkRec( OldRecords[i] + FDataSet.FBookmarkOffs)^.iPos := i;
  end;
  	// if need to set new record to current record
  if SetCurrent then
    FPosition := PBookmarkRec( Buf + FDataSet.FBookmarkOffs)^.iPos;
end;

{ Changes the record in the cache (without blob part) }
procedure TSDResultSet.ModifyRecord(RecBuf: PChar);
var
  Idx: Integer;
  Buf: PChar;
begin
  Idx := PBookmarkRec(RecBuf + FDataSet.FBookmarkOffs)^.iPos;
  if (UpdateStatusRecords[Idx] in [usUnmodified, usModified]) and
     (OldRecords[Idx] = nil )
  then with FDataSet do begin
    Buf := AllocRecordBuffer;
    OldRecords[Idx] := Buf;
    CopyRecBuf(CurRecords[Idx]^, OldRecords[Idx]^);

    UpdateStatusRecords[Idx] := usModified;
    AppliedRecords[Idx] := False;
  end;
  System.Move( RecBuf^, CurRecords[Idx]^, FDataSet.FBlobCacheOffs );
end;

{ Stores Blob data in the record cache for the passed record }
procedure TSDResultSet.ModifyBlobData(Field: TField; RecBuf: PChar; const Value: TBlobData);
var
  Idx: Integer;
begin
	// if the record temporary and still not present in the cache now
  if PRecInfo(RecBuf + FDataSet.FRecInfoOffs)^.BookmarkFlag = bfInserted then
    Exit;
    	// get a position in a record cache
  Idx := PBookmarkRec(RecBuf + FDataSet.FBookmarkOffs)^.iPos;
	// If the record is only inserted and still not present in the cache. It will be in the cache after call Post method
  if Idx < 0 then
    Exit;
  if (UpdateStatusRecords[Idx] in [usUnmodified, usModified]) and
     (OldRecords[Idx] = nil )
  then
    ModifyRecord(RecBuf);
	// save new Blob-data in the current record buffer
  PBlobDataArray(CurRecords[Idx] + FDataSet.FBlobCacheOffs)[Field.Offset] := Value;
end;

{ Frees record buffers and removes the record from the cache.
 Changes ordinal number for the rest of the records }
procedure TSDResultSet.DeleteCacheItem(Index: Integer);
var
  i: Integer;
begin
  ClearCacheItem(Index);

  Delete(Index);
	// change current position for records in the cache (before deleted position)
  for i:=Index to Count-1 do begin
    PBookmarkRec( CurRecords[i] + FDataSet.FBookmarkOffs)^.iPos := i;
    if OldRecords[i] <> nil then
      PBookmarkRec( OldRecords[i] + FDataSet.FBookmarkOffs)^.iPos := i;
  end;
end;

{ Cancel modifications for the current record }
function TSDResultSet.UpdatesCancelCurrent: Integer;
var
  i: Integer;
begin
  i := PBookmarkRec(FDataSet.ActiveBuffer + FDataSet.FBookmarkOffs)^.iPos;
  if not AppliedRecords[i] then begin
    if UpdateStatusRecords[i] = usModified then begin
  	// delete current record and restore data from old record buffer
      FDataSet.FreeRecordBuffer( PCacheRecInfo(Items[i])^.CurRec );
      CurRecords[i] := OldRecords[i];
      OldRecords[i] := nil;
      AppliedRecords[i] := True;
    end else if UpdateStatusRecords[i] = usInserted then
      DeleteCacheItem(i)
    else if UpdateStatusRecords[i] = usDeleted then begin
      UpdateStatusRecords[i] := usUnmodified;
      AppliedRecords[i] := True;
    end;

  end;
  Result := SDE_ERR_NONE;
end;

{ Cancel all modifications in the cache }
function TSDResultSet.UpdatesCancel: Integer;
var
  i: Integer;
begin
  i := 0;
  while i < Count do begin
    if UpdateStatusRecords[i] <> usUnmodified then
      if UpdateStatusRecords[i] = usModified then begin			// if record was modified
        FDataSet.FreeRecordBuffer( PCacheRecInfo(Items[i])^.CurRec );
        CurRecords[i] := OldRecords[i];
        OldRecords[i] := nil;
        UpdateStatusRecords[i] := usUnmodified;
        AppliedRecords[i] := True;
      end else if UpdateStatusRecords[i] = usInserted  then begin	// if record was inserted
        DeleteCacheItem(i);
        Dec(i);
      end else if UpdateStatusRecords[i] = usDeleted  then begin	// if record was deleted
        UpdateStatusRecords[i] := usUnmodified;
        AppliedRecords[i] := True;
      end;

    Inc(i);
  end;
  Result := SDE_ERR_NONE;
end;

{   Commits all changes in the cache, which was successfully saved in database(UpdateStatus=unUnmodified),
i.e. if AppliedRecords[i]=True, then old record buffers is released and set UpdateStatus:=usUnmodified
}
function TSDResultSet.UpdatesCommit: Integer;
var
  i: Integer;
  nDeletedRows: Integer;	// deleted row before the current position
begin
  i := 0;
  nDeletedRows := 0;
  while i < Count do begin
    if AppliedRecords[i] then
      case UpdateStatusRecords[i] of
        usModified:
          begin
            FDataSet.FreeRecordBuffer( PCacheRecInfo(Items[i])^.OldRec );
            UpdateStatusRecords[i] := usUnmodified;
            AppliedRecords[i] := True;
          end;
        usInserted:
          begin
            UpdateStatusRecords[i] := usUnmodified;
            AppliedRecords[i] := True;
          end;
        usDeleted:
          begin
            DeleteCacheItem(i);
            	// the deleted row is removed from the cache
            if FDeletedCount > 0 then
              Dec(FDeletedCount);
		// how many record was deleted before the current record
            if i <= FPosition then
              Inc(nDeletedRows);

            Dec(i);
          end;
      end;
    Inc(i);
  end;
	// a cache position of the current row is changed,
        // but the current visible row is not changed
  FPosition := FPosition - nDeletedRows;
  Result := SDE_ERR_NONE;
end;

{
 Mark modifications in local cache as not saved in a database.
 If saving was successful, AppliedRecords[i]=True,
 but if transaction is rolled back, changes are actually not saved
}
function TSDResultSet.UpdatesRollback: Integer;
var
  i: Integer;
begin
  i := 0;
  while i < Count do begin
    if AppliedRecords[i] and (UpdateStatusRecords[i] <> usUnmodified) then
      AppliedRecords[i] := False;
    Inc(i);
  end;
  Result := SDE_ERR_NONE;
end;

{ Apply(save) changes in database. If saving is successful, then set AppliedRecords[i]:=True }
function TSDResultSet.UpdatesPrepare: Integer;
  function IsEnableUpdateRecord( AUpdateKinds: TUpdateKinds; Value: TUpdateStatus): Boolean;
  begin
    Result :=
    	(Value = usModified) and (ukModify in AUpdateKinds) or
    	(Value = usInserted) and (ukInsert in AUpdateKinds) or
	(Value = usDeleted) and (ukDelete in AUpdateKinds);
  end;
var
  i: Integer;
  UpdateAction: TUpdateAction;
  UpdateKind: TUpdateKind;
  EnableUpdKinds: TUpdateKinds;
  rtSave: TUpdateRecordTypes;
  evBeforeScroll, evAfterScroll: TDataSetNotifyEvent;
  SavePos: Integer;
  FilterState: Boolean;
begin
  Result := SDE_ERR_UPDATEABORT;
  FDataSet.DisableControls;
  SavePos := FPosition;			// save current record position
	// store dataset's states
  rtSave := FDataSet.FUpdateRecordTypes;
  FilterState := FDataSet.Filtered;
  EnableUpdKinds := FDataSet.EnableUpdateKinds;
  evBeforeScroll := FDataSet.BeforeScroll;
  evAfterScroll	 := FDataSet.AfterScroll;
  FDataSet.BeforeScroll := nil;
  FDataSet.AfterScroll  := nil;

  try
	// it sets all records visible when we go through result set (for Next, GetRecord methods)
    FDataSet.FUpdateRecordTypes := [rtModified, rtInserted, rtDeleted, rtUnModified];
    FDataSet.Filtered		:= False;
    FDataSet.First;
    while not FDataSet.EOF do begin
      i := PBookmarkRec(FDataSet.ActiveBuffer + FDataSet.FBookmarkOffs)^.iPos;
      UpdateAction := uaFail;
	// apply the record ?
      if (not AppliedRecords[i]) and
         (not ( (UpdateStatusRecords[i] in [usDeleted]) and IsInsertedRecord(i) )) and
         IsEnableUpdateRecord( EnableUpdKinds, UpdateStatusRecords[i] )
      then begin
        try

          ASSERT( UpdateStatusRecords[i] in [usModified, usInserted, usDeleted], 'UpdateStatus out of range' );

          UpdateKind := TUpdateKind( Ord(UpdateStatusRecords[i])-1 );
          try
            if Assigned(FDataSet.FOnUpdateRecord) then
              FDataSet.FOnUpdateRecord(FDataSet, UpdateKind, UpdateAction)
            else if Assigned(FDataSet.FUpdateObject) then begin
              FDataSet.FUpdateObject.Apply(UpdateKind);
              UpdateAction := uaApplied;
            end else
              DatabaseError(SUpdateImpossible);
          except
            on E: Exception do begin
              if (E is EDatabaseError) and Assigned(FDataSet.FOnUpdateError) then
                FDataSet.FOnUpdateError(FDataSet, EDatabaseError(E), UpdateKind, UpdateAction)
              else begin	// handle all other exceptions
                Application.HandleException(Self);
                UpdateAction := uaAbort;
              end;
            end;
          end;
          if UpdateAction in [uaAbort, uaFail] then begin
            if UpdateAction = uaAbort then
              Result := SDE_ERR_UPDATEABORT;
            Exit;
          end;
          if UpdateAction = uaApplied then
            AppliedRecords[i] := True;
        except
		// default handling of exceptions(show errors) and return back to the following line
          Application.HandleException(Self);
        end;
      end;		// end of (IF THEN) apply the record
      	// if it is not necessary to apply record again, then go to the following
      if UpdateAction <> uaRetry then begin
      	// if the next record is not cached at present, then break from cycle
        if (i + 1) >= Count then
          Break
        else
          FDataSet.Next;
      end;
    end;	// WHILE not FDataSet.EOF do
  finally
    FDataSet.BeforeScroll := evBeforeScroll;
    FDataSet.AfterScroll  := evAfterScroll;
    FDataSet.FUpdateRecordTypes := rtSave;
    if FDataSet.Filtered <> FilterState then
      FDataSet.Filtered	:= FilterState;
    FPosition := SavePos;		// restore current record position after filter mode is restored
    FDataSet.EnableControls;
  end;
  Result := SDE_ERR_NONE;
end;


(*******************************************************************************
				TSDDataSet
*******************************************************************************)
constructor TSDDataSet.Create(AOwner: TComponent);
begin
  Inherited Create(AOwner);
  FCachedUpdates := True;
  FCacheBlobs := True;
  FUniDirectional  := False;
  FDetachOnFetchAll:= False;
  FFilterActivated := False;
  FForceClosing	   := False;
  FRecordSize := 0;
  FRecBufSize := 0;
  FFieldDescs	:= TSDFieldDescList.Create;
  FFieldBufOffs := nil;

  FPreservation := True;
  FEnableUpdateKinds	:= [];
  FUpdateRecordTypes	:= [];
end;

destructor TSDDataSet.Destroy;
begin
  FFieldDescs.Clear;
  FFieldDescs.Free;

  if FFieldBufOffs <> nil then
    ReallocMem( FFieldBufOffs, 0 );

  if UpdateObject <> nil then
    SetUpdateObject(nil);
    
  inherited Destroy;
end;

procedure TSDDataSet.CheckDatabaseName;
begin
  if FDatabaseName = '' then
    DatabaseError(SDatabaseNameMissing);
end;

procedure TSDDataSet.CheckDBSessionName;
var
  S: TSDSession;
  Database: TSDDatabase;
begin
  if (SessionName <> '') and (DatabaseName <> '') then begin
    S := Sessions.FindSession(SessionName);
    if Assigned(S) and not Assigned(S.DoFindDatabase(DatabaseName, Self)) then begin
      Database := DefaultSession.DoFindDatabase(DatabaseName, Self);
      if Assigned(Database) then
        Database.CheckSessionName(True);
    end;
  end;
end;

function TSDDataSet.CreateHandle: TISrvDataSet;
begin
  ASSERT( FDatabase <> nil, Format(SFatalError, ['TSDDataSet.CreateHandle']) );
  ASSERT( FDatabase.FSrvDatabase <> nil, Format(SFatalError, ['TSDDataSet.CreateHandle']) );

  Result := FDatabase.FSrvDatabase.CreateSrvDataSet( Self );
end;

procedure TSDDataSet.DestroyHandle;
begin
  DestroySrvDataSet( FForceClosing );
end;

procedure TSDDataSet.Disconnect;
begin
  Close;
  DestroyHandle;
end;

{ Sets a result set to offline: close cursor and keep result set active }
procedure TSDDataSet.Detach;
begin
  DestroyHandle;
end;


procedure TSDDataSet.ForceClose;
begin
  FForceClosing := True;
  try
    try
    	// ISrvCloseResultSet method could raise a database exception (on some server), which is need to hide
      Close;
	// to exclude to parse and set off all flags
      FDSFlags := [dsfOpened];
      	// set off the last flag to call Disconnect and UnregisterClient methods
      SetDSFlag(dsfOpened, False);
    except
      on E: Exception do
        if not (E is ESDEngineError) then
          raise;
    end;
  finally
    FForceClosing := False;
  end;
end;

procedure TSDDataSet.DestroySrvDataSet(Force: Boolean);
begin
  if FSrvDataSet <> nil then begin
    FSrvDataSet.Disconnect(Force);

    FSrvDataSet.Free;
    FSrvDataSet := nil;
  end;
end;

function TSDDataSet.GetServerType: TSDServerType;
var
  db: TSDDatabase;
begin
  if FDatabase <> nil then
    Result := FDatabase.FServerType
  else begin
    db := Sessions.FindDatabase(DatabaseName);
    if db = nil then
      db := Session.OpenDatabase(DatabaseName);
    Result := db.ServerType
  end;
end;

procedure TSDDataSet.CloseCursor;
begin
  SetBusyState;
  try
    inherited CloseCursor;
    SetDSFlag(dsfOpened, False);
  finally
    ResetBusyState;
  end;
end;

procedure TSDDataSet.OpenCursor(InfoQuery: Boolean);
begin
  SetBusyState;
  try
    SetDSFlag(dsfOpened, True);
    if FSrvDataSet = nil then
      FSrvDataSet := CreateHandle;
    PrepareCursor;
    if not FSrvDataSet.ResultSetExists then begin
      DestroyHandle;
      ResetBusyState;
      raise ESDNoResultSet.Create(SHandleError);
    end;
    inherited OpenCursor(InfoQuery);
  finally
    ResetBusyState;
  end;
end;

procedure TSDDataSet.ExecuteCursor;
begin
  SetConnectionState( True );
  try
    ISrvExecute;
  finally
    SetConnectionState( False );
  end;
end;

{ Marks that a statement is running on server-side }
procedure TSDDataSet.SetConnectionState(IsBusy: Boolean);
begin
  ASSERT( Assigned(Database) );

  Database.FIsConnectionBusy := IsBusy;
end;

function TSDDataSet.IsCursorOpen: Boolean;
begin
  Result := dsfOpened in DSFlags;
end;

procedure TSDDataSet.InternalHandleException;
begin
  Application.HandleException(Self)
end;

procedure TSDDataSet.InternalInitFieldDefs;
begin
  if not Active then begin
    ClearFieldDefs;

    ISrvInitFieldDefs;
  end;
end;

procedure TSDDataSet.ClearFieldDefs;
begin
  FieldDefs.Clear;
  FFieldDescs.Clear;
end;

{ It is necessary for initialization of the result set without or after execute }
procedure TSDDataSet.InitResultSet;
begin
	// private field FieldDefs.FUpdated = False if modified properties of component(DataEvent(dePropertyChanged))
  if FieldDefs.Count = 0 then InternalInitFieldDefs;
  if DefaultFields and (FieldCount = 0) then CreateFields;

	// calculates BlobFieldCount and CalcFieldsSize, field numeration (set FieldNo property)
  BindFields(True);
	// sets FFieldBufOffs, FRecordSize and other pointers in the record buffer 
  InitBufferPointers;

  ISrvSetSelectBuffer;
end;

procedure TSDDataSet.DoneResultSet;
begin
  if Assigned( FFieldBufOffs ) then begin
    ISrvCloseResultSet;
	// resets field's offsets in the record buffer
    ReallocMem( FFieldBufOffs, 0 );
    FFieldBufOffs := nil;
  end;
	// resets BlobFieldCount and CalcFieldsSize
  BindFields(False);
  
  if DefaultFields then DestroyFields;
end;

procedure TSDDataSet.InternalOpen;
begin
	// create field definitions, fields, init record size and set select buffer
  InitResultSet;

  CacheInit;
  ExecuteCursor;
  FEnableUpdateKinds	:= [ukModify, ukInsert, ukDelete];
  FUpdateRecordTypes	:= [rtModified, rtInserted, rtUnmodified];
  if Filtered then ActivateFilters;
end;

procedure TSDDataSet.InternalClose;
begin
  CacheDone;

  DoneResultSet;

  FEnableUpdateKinds	:= [];
  FUpdateRecordTypes	:= [];
end;

function TSDDataSet.NativeFieldDataSize(Field: TField): Word;
begin
  with Field do
    if IsRequiredSizeTypes( DataType ) then
      Result := DataSize
    else
      Result := FDatabase.ISrvNativeDataSize(DataType);
end;

function TSDDataSet.GetDBSession: TSDSession;
begin
  if (FDatabase <> nil) then
    Result := FDatabase.Session
  else
    Result := Sessions.FindSession(SessionName);
  if Result = nil then
    Result := DefaultSession;
end;

function TSDDataSet.OpenDatabase: TSDDatabase;
begin
  with Sessions.List[FSessionName] do
    Result := DoOpenDatabase(FDatabasename, Self.Owner);
end;

procedure TSDDataSet.CloseDatabase(Database: TSDDatabase);
begin
  if Assigned(Database) then
    Database.Session.CloseDatabase(Database);
end;

procedure TSDDataSet.SetDatabaseName(const Value: string);
begin
  if csReading in ComponentState then
    FDatabaseName := Value
  else if FDatabaseName <> Value then begin
    CheckInactive;
    if FDatabase <> nil then DatabaseError(SDatabaseOpen);
    FDatabaseName := Value;
    DataEvent(dePropertyChange, 0);
  end;
end;

procedure TSDDataSet.SetSessionName(const Value: string);
begin
  if FSessionName <> Value then begin
    CheckInactive;
    FSessionName := Value;
    DataEvent(dePropertyChange, 0);
  end;
end;

function TSDDataSet.SetDSFlag(Flag: Integer; Value: Boolean): Boolean;
begin
  Result := Flag in FDSFlags;
  if Value then begin
    if not (Flag in FDSFlags) then begin
      if FDSFlags = [] then begin
        CheckDBSessionName;      
        if Trim(DatabaseName) = '' then
          DatabaseError(SDatabaseNameMissing);
        FDatabase := OpenDatabase;
        FDatabase.RegisterClient(Self);
      end;
      Include(FDSFlags, Flag);
    end;
  end else begin
    if Flag in FDSFlags then begin
      Exclude(FDSFlags, Flag);
      if FDSFlags = [] then begin
        Disconnect;
        FDatabase.UnregisterClient(Self);
        FDatabase.Session.CloseDatabase(FDatabase);
        FDatabase := nil;
      end;
    end;
  end;
end;

procedure TSDDataSet.SetDetachOnFetchAll(Value: Boolean);
begin
  if Value <> FDetachOnFetchAll then begin
    if Active then
      Detach;
    FDetachOnFetchAll := Value;
  end;
end;

procedure TSDDataSet.SetPreservation(Value: Boolean);
begin
  if Value <> FPreservation then begin
    if (FSrvDataSet <> nil) and not (csReading in ComponentState) then
      ISrvSetPreservation(Value);
    FPreservation := Value;
  end;
end;

{ Record Cache Functions }
procedure TSDDataSet.CacheInit;
begin
  FRecCache := TSDResultSet.Create(Self);
end;

procedure TSDDataSet.CacheDone;
begin
  if FRecCache <> nil then begin
    FRecCache.Free;
    FRecCache := nil;
  end;
end;

{$IFDEF SD_VCL4}
{ current record buffer in cache }
function TSDDataSet.CacheTempBuffer: PChar;
begin
  Result := FRecCache.CurRecords[FRecCache.RecNo];
end;
{$ENDIF}

{ Record Functions }
procedure TSDDataSet.InitBufferPointers;
var
  i: Integer;
  f: TField;
begin
  BookmarkSize := SizeOf(TBookmarkRec);
  FRecordSize := 0;
	// Allocate and fill up FFieldBufOffs array, which must contain FieldDefs.Count items(-1 for calculated and omitted fields)
  ReallocMem( FFieldBufOffs, (FieldDefs.Count)*SizeOf(Integer) );
  FillChar( FFieldBufOffs^, (FieldDefs.Count)*SizeOf(Integer), $FF );
	// FieldDefs contains a complete column's definition in contrast to Fields array
  for i:=0 to FieldDefs.Count-1 do begin
    f := FieldByNumber(FieldDefs[i].FieldNo);
    	// Skip calculated fields
    if (not Assigned(f)) or (f.Calculated) then
      Continue;
    	// Field.FieldNo starts from 1
    FFieldBufOffs[f.FieldNo-1] := FRecordSize;
    Inc( FRecordSize, SizeOf(TFieldInfo) + GetFieldDataSize(f) );
  end;

//    -------------------------------------------------------------
//    | Record | CalcFields | BlobCache | TRecInfo | TBookmarkRec |
//     <-------------------- FRecBufSize ----------------------->
//
  FBlobCacheOffs := FRecordSize + CalcFieldsSize;
  FRecInfoOffs := FBlobCacheOffs + BlobFieldCount * SizeOf(Pointer);
  FBookmarkOffs := FRecInfoOffs + SizeOf(TRecInfo);
  FRecBufSize := FBookmarkOffs + BookmarkSize;
end;

function TSDDataSet.AllocRecordBuffer: PChar;
begin
  Result := StrAlloc(FRecBufSize);
  FillChar(Result^,FRecBufSize,0);
  if BlobFieldCount > 0 then
    Initialize(PBlobDataArray(Result + FBlobCacheOffs)[0], BlobFieldCount);
end;

procedure TSDDataSet.FreeRecordBuffer(var Buffer: PChar);
begin
  ASSERT( Buffer <> nil );

  if BlobFieldCount > 0 then
    Finalize(PBlobDataArray(Buffer + FBlobCacheOffs)[0], BlobFieldCount);
  StrDispose(Buffer);
  Buffer := nil;
end;

procedure TSDDataSet.ClearBlobCache(RecBuf: PChar);
var
  I: Integer;
begin
  ASSERT( RecBuf <> nil );

  if FCacheBlobs then
    for I := 0 to BlobFieldCount - 1 do
      PBlobDataArray(RecBuf + FBlobCacheOffs)[I] := '';
end;

function TSDDataSet.FieldIsNull(FieldBuf: Pointer): Boolean;
begin
  Result := PFieldInfo(FieldBuf)^.FetchStatus = indNULL;
end;

procedure TSDDataSet.FieldSetNull(FieldBuf: Pointer; bNull: Boolean);
begin
  if bNull then begin
    PFieldInfo(FieldBuf)^.FetchStatus	:= indNULL;
    PFieldInfo(FieldBuf)^.DataSize	:= 0;
  end else
    PFieldInfo(FieldBuf)^.FetchStatus	:= indVALUE;
end;

function TSDDataSet.GetActiveRecBuf(var RecBuf: PChar): Boolean;
begin
  case State of
{$IFDEF SD_VCL4}
    dsBlockRead: RecBuf := ActiveBuffer;
{$ENDIF}    
    dsBrowse: if IsEmpty then RecBuf := nil else RecBuf := ActiveBuffer;
    dsEdit,
    dsInsert: RecBuf := ActiveBuffer;
    dsCalcFields: RecBuf := CalcBuffer;
    dsFilter: RecBuf := FFilterBuffer;
    dsNewValue: RecBuf := ActiveBuffer;
    dsOldValue: RecBuf := GetOldRecord;
  else
    RecBuf := nil;
  end;
  Result := RecBuf <> nil;
end;

function TSDDataSet.GetFieldDataSize(Field: TField): Integer;
begin
  if IsDateTimeType( Field.DataType ) then
    Result := SizeOf(TDateTimeRec)  // DateTime fields stored as TDateTimeRec
  else
    Result := Field.DataSize;
end;

{ Returns an offset to TFieldInfo structure for a fields (Index = FieldNo-1) }
function TSDDataSet.GetFieldBufOffs(Index: Integer): Integer;
begin
  Result := FFieldBufOffs[Index];
end;

{ Returns pointer to the field buffer from the record buffer RecBuf (with TFieldInfo structure) }
function TSDDataSet.GetFieldBuffer(Field: TField; RecBuf: Pointer): Pointer;
begin
  ASSERT( Field.FieldNo > 0 );				// Field.FieldNo starts from 1
  ASSERT( FFieldBufOffs[Field.FieldNo-1] >= 0 );	// offset must be 0 or more

  Result := Pointer(Integer(RecBuf) + FFieldBufOffs[Field.FieldNo-1]);
end;

{ Returns field data in Buffer, if Result = True
If parameter Buffer = nil, then checks field data and if field is empty, then returns False,
else returns True and field data (if Buffer <> nil) }
function TSDDataSet.GetFieldData(Field: TField; Buffer: Pointer): Boolean;
const
{$IFDEF SD_VCL4}
  TempStateSet = [dsBrowse, dsEdit, dsInsert, dsCalcFields, dsBlockRead];
{$ELSE}
  TempStateSet = [dsBrowse, dsEdit, dsInsert, dsCalcFields];
{$ENDIF}
var
  RecBuf, FieldBuf: PChar;
begin
  Result := False;
  if not GetActiveRecBuf(RecBuf) then Exit;

  ASSERT( Field <> nil );
  with Field do begin
    if FieldNo > 0 then begin		// FieldNo(1,2..) can be not ordered at sequential Fields[i]
      FieldBuf := GetFieldBuffer(Field, RecBuf);

      ASSERT( FieldBuf<>nil, 'Field buffer = nil' );
      ASSERT( FDatabase<>nil );

	// check IsNull only
      if Buffer = nil then begin
        Result := not FieldIsNull(FieldBuf);
        Exit;                           	// field data is not required
      end;
	// if not IsNull then return filed data
      if FieldIsNull(FieldBuf) then
        Result := False
      else begin
        if DataType = ftString then begin
          if PFieldInfo(FieldBuf)^.DataSize > 0 then
            Move( (FieldBuf + SizeOf(TFieldInfo))^, Buffer^,
              PFieldInfo(FieldBuf)^.DataSize + 1)	// copy with null-terminator
          else
            PChar(Buffer)[0] := #$00;			// set with null-terminator
        end else
          Move( (FieldBuf + SizeOf(TFieldInfo))^, Buffer^,
              PFieldInfo(FieldBuf)^.DataSize );
        Result := True;
      end
    end else
      if State in TempStateSet then begin
        Inc(RecBuf, FRecordSize + Offset);
        Result := Boolean(RecBuf[0]);
        if Result and (Buffer <> nil) then
          Move(RecBuf[1], Buffer^, DataSize);
      end;
  end;
end;

{$IFDEF SD_VCL4}
function TSDDataSet.GetFieldData(FieldNo: Integer; Buffer: Pointer): Boolean;
begin
  Result := GetFieldData( FieldByNumber(FieldNo), Buffer );
end;
{$ENDIF}

{ Sets new data(Buffer) for field(Field). If Buffer = nil than field is cleared }
procedure TSDDataSet.SetFieldData(Field: TField; Buffer: Pointer);
var
  RecBuf: PChar;

  procedure PutField(AField: TField; ARecBuf, ABuffer: PChar);
  var
    FldBuf: PChar;
  begin
    with AField do begin
      FldBuf := GetFieldBuffer(AField, ARecBuf);

      ASSERT( FDatabase<>nil );
	// Copy(assign) new value of the field
      if ABuffer <> nil then begin
        if DataType = ftString then begin
		// Set data size in record buffer
          PFieldInfo(FldBuf)^.DataSize := StrLen(ABuffer);
          Move( ABuffer^, (FldBuf + SizeOf(TFieldInfo))^, PFieldInfo(FldBuf)^.DataSize );
          (FldBuf + SizeOf(TFieldInfo) + PFieldInfo(FldBuf)^.DataSize)^ := #0;
        end else begin
		// Set data size in record buffer
          PFieldInfo(FldBuf)^.DataSize := GetFieldDataSize(AField);
          Move( ABuffer^, (FldBuf + SizeOf(TFieldInfo))^, PFieldInfo(FldBuf)^.DataSize );
        end;
        FieldSetNull( FldBuf, False );
      end else
        FieldSetNull( FldBuf, True );

	// Set modification flag for record
      if PRecInfo(ARecBuf + FRecInfoOffs)^.UpdateStatus = usUnmodified then
        PRecInfo(ARecBuf + FRecInfoOffs)^.UpdateStatus := usModified;
    end;
  end;
begin
  with Field do begin
    if not (State in dsWriteModes) then DatabaseError(SNotEditing);
    GetActiveRecBuf(RecBuf);

    if FieldNo > 0 then begin
      if State = dsCalcFields then DatabaseError(SNotEditing);
      if ReadOnly and not (State in [dsSetKey, dsFilter]) then
        DatabaseErrorFmt(SFieldReadOnly, [DisplayName]);
      Validate(Buffer);
      if FieldKind <> fkInternalCalc then
        PutField(Field, RecBuf, Buffer);
    end else begin	{fkCalculated, fkLookup}
      Inc(RecBuf, FRecordSize + Offset);
      Boolean(RecBuf[0]) := LongBool(Buffer);
      if Boolean(RecBuf[0]) then
        Move(Buffer^, RecBuf[1], DataSize);
    end;
    if not (State in [dsCalcFields, dsFilter, dsNewValue]) then
      DataEvent(deFieldChange, Longint(Field));
  end;
end;

{$IFNDEF SD_VCL5}
function CurrToBCD(Curr: Currency; var BCD: TBcd; Precision, Decimals: Integer): Boolean;
const
  Power10: array[0..3] of Single = (10000, 1000, 100, 10);
var
  Digits: array[0..63] of Byte;
asm
        PUSH    EBX
        PUSH    ESI
        PUSH    EDI
        MOV     ESI,EAX
        XCHG    ECX,EDX
        MOV     [ESI].TBcd.Precision,CL
        MOV     [ESI].TBcd.SignSpecialPlaces,DL
@@1:    SUB     EDX,4
        JE      @@3
        JA      @@2
        FILD    Curr
        FDIV    Power10.Single[EDX*4+16]
        FISTP   Curr
        JMP     @@3
@@2:    DEC     ECX
        MOV     Digits.Byte[ECX],0
        DEC     EDX
        JNE     @@2
@@3:    MOV     EAX,Curr.Integer[0]
        MOV     EBX,Curr.Integer[4]
        OR      EBX,EBX
        JNS     @@4
        NEG     EBX
        NEG     EAX
        SBB     EBX,0
        OR      [ESI].TBcd.SignSpecialPlaces,80H
@@4:    MOV     EDI,10
@@5:    MOV     EDX,EAX
        OR      EDX,EBX
        JE      @@7
        XOR     EDX,EDX
        OR      EBX,EBX
        JE      @@6
        XCHG    EAX,EBX
        DIV     EDI
        XCHG    EAX,EBX
@@6:    DIV     EDI
@@7:    MOV     Digits.Byte[ECX-1],DL
        DEC     ECX
        JNE     @@5
        OR      EAX,EBX
        MOV     AL,0
        JNE     @@9
        MOV     CL,[ESI].TBcd.Precision
        INC     ECX
        SHR     ECX,1
@@8:    MOV     AX,Digits.Word[ECX*2-2]
        SHL     AL,4
        OR      AL,AH
        MOV     [ESI].TBcd.Fraction.Byte[ECX-1],AL
        DEC     ECX
        JNE     @@8
        MOV     AL,1
@@9:    POP     EDI
        POP     ESI
        POP     EBX
end;

function BCDToCurr(const BCD: TBcd; var Curr: Currency): Boolean;
const
  FConst10: Single = 10;
  CWNear: Word = $133F;
var
  CtrlWord: Word;
  Temp: Integer;
  Digits: array[0..63] of Byte;
asm
        PUSH    EBX
        PUSH    ESI
        MOV     EBX,EAX
        MOV     ESI,EDX
        MOV     AL,0
        MOVZX   EDX,[EBX].TBcd.Precision
        OR      EDX,EDX
        JE      @@8
        LEA     ECX,[EDX+1]
        SHR     ECX,1
@@1:    MOV     AL,[EBX].TBcd.Fraction.Byte[ECX-1]
        MOV     AH,AL
        SHR     AL,4
        AND     AH,0FH
        MOV     Digits.Word[ECX*2-2],AX
        DEC     ECX
        JNE     @@1
        XOR     EAX,EAX
@@2:    MOV     AL,Digits.Byte[ECX]
        OR      AL,AL
        JNE     @@3
        INC     ECX
        CMP     ECX,EDX
        JNE     @@2
        FLDZ
        JMP     @@7
@@3:    MOV     Temp,EAX
        FILD    Temp
@@4:    INC     ECX
        CMP     ECX,EDX
        JE      @@5
        FMUL    FConst10
        MOV     AL,Digits.Byte[ECX]
        MOV     Temp,EAX
        FIADD   Temp
        JMP     @@4
@@5:    MOV     AL,[EBX].TBcd.SignSpecialPlaces
        OR      AL,AL
        JNS     @@6
        FCHS
@@6:    AND     EAX,3FH
        SUB     EAX,4
        NEG     EAX
        CALL    FPower10
@@7:    FSTCW   CtrlWord
        FLDCW   CWNear
        FISTP   [ESI].Currency
        FSTSW   AX
        NOT     AL
        AND     AL,1
        FCLEX
        FLDCW   CtrlWord
        FWAIT
@@8:    POP     ESI
        POP     EBX
end;

function TSDDataSet.BCDToCurr(BCD: Pointer; var Curr: Currency): Boolean;
begin
  Result := SDEngine.BCDToCurr( TBcd(BCD^), Curr );
end;

function TSDDataSet.CurrToBCD(const Curr: Currency; BCD: Pointer; Precision, Decimals: Integer): Boolean;
begin
  Result := SDEngine.CurrToBCD( Curr, TBcd(BCD^), Precision, Decimals );
end;
{$ENDIF}

function TSDDataSet.CreateBlobStream(Field: TField; Mode: TBlobStreamMode): TStream;
begin
  Result := TSDBlobStream.Create(Field as TBlobField, Mode);
end;

function TSDDataSet.GetBlobData(Field: TField; Buffer: PChar): TBlobData;
begin
  Result := PBlobDataArray(Buffer + FBlobCacheOffs)[Field.Offset];
end;

{ Stores a blob value in the passed record buffer and in the record cache }
procedure TSDDataSet.SetBlobData(Field: TField; Buffer: PChar; Value: TBlobData);
var
  FldBuf: PChar;
begin
  if Buffer = ActiveBuffer then begin
  	// stores in the record cache
    FRecCache.ModifyBlobData(Field, Buffer, Value);
    	// stores in the passed buffer
    PBlobDataArray(Buffer + FBlobCacheOffs)[Field.Offset] := Value;
    FldBuf := GetFieldBuffer(Field, Buffer);
	// Checking: if Length(Value) = 0, then IsNull:=True
    FieldSetNull( FldBuf, not(Length(Value) > 0) );
  end;
end;

function TSDDataSet.GetRecNo: Integer;
var
  BufPtr: PChar;
begin
  CheckActive;
  if State = dsCalcFields
  then BufPtr := CalcBuffer
  else BufPtr := ActiveBuffer;
  Result := PRecInfo(BufPtr + FRecInfoOffs).RecordNumber;
end;

{
  Returns the total number of record (except, deleted record with UpdateRecordTypes=rtDeleted),
  when a dataset is filtered, then return number of the filtered(visible) records.
}
function TSDDataSet.GetRecordCount: LongInt;
begin
  Result := -1;
  if UniDirectional then
    Exit;
    	// for example, after ExecSQL/ExecProc a result set is not present at all
  if not Assigned(RecCache) then
    Exit;

  SetBusyState;
  try
    if not RecCache.AllInCache then
      FetchAll;

    Result := RecCache.RecordCount;
  finally
    ResetBusyState;
  end;
end;

function TSDDataSet.GetRecord(Buffer: PChar; GetMode: TGetMode; DoCheck: Boolean): TGetResult;
var
  Status: Boolean;
begin
  if Assigned(FRecCache)
  then Status := FRecCache.GetRecord(Buffer, GetMode)
  else Status := False;

  case Status of
    True:
      begin
        with PRecInfo(Buffer + FRecInfoOffs)^ do
          BookmarkFlag := bfCurrent;
        GetCalcFields(Buffer);
        Result := grOK;
      end;
    False:
      if GetMode = gmPrior
      then Result := grBOF
      else Result := grEOF;
  else
    Result := grError;
  end;
end;

function TSDDataSet.GetOldRecord: PChar;
var
  Idx: Integer;
begin
  Result := ActiveBuffer;
  Idx := PBookmarkRec(Result + FBookmarkOffs)^.iPos;
  case PRecInfo(Result + FRecInfoOffs)^.UpdateStatus of
    usInserted: Result := nil;
    usModified:
      if FRecCache.OldRecords[Idx] = nil		// modifications have not been written in cache (before call Post)
      then Result := FRecCache.CurRecords[Idx]
      else Result := FRecCache.OldRecords[Idx];
  end;
end;

function TSDDataSet.GetRecordSize: Word;
begin
  Result := FRecordSize;
end;

procedure TSDDataSet.FetchAll;
begin
	// EOF is not equal FRecCache.AllInCache, for example, after Append call
  if not FRecCache.AllInCache then begin
    CheckActive;
    SetBusyState;
    try
	// CheckBrowseMode is excluded. It sets dsBrowse mode always.
      FRecCache.FetchAll;
      if FDetachOnFetchAll then
        Detach;
    finally
      ResetBusyState;
    end;
  end;
end;

procedure TSDDataSet.InitRecord(Buffer: PChar);
begin
  inherited InitRecord(Buffer);
  ClearBlobCache(Buffer);

  with PBookmarkRec(Buffer + FBookmarkOffs)^ do
    iPos := -1;

  with PRecInfo(Buffer + FRecInfoOffs)^ do begin
    UpdateStatus := TUpdateStatus(usInserted);
    BookmarkFlag := bfInserted;
    RecordNumber := -1;
  end;
end;

procedure TSDDataSet.InternalInitRecord(Buffer: PChar);
var
  i: Integer;
  FieldBuf: PChar;
begin
  for i:=0 to FieldCount-1 do
  	// exclude fields, which have FieldNo = -1: FieldKind in [fkCalculated, fkLookup, fkAggregate]
    if Fields[i].FieldNo > 0 then begin
      FieldBuf := GetFieldBuffer(Fields[i], Buffer);

      ASSERT( FieldBuf<>nil, 'Field buffer = nil' );
      ASSERT( FDatabase<>nil );

      FieldSetNull( FieldBuf, True );
    end;
end;

{ Clears a buffer of calculated fields }
procedure TSDDataSet.ClearCalcFields(Buffer: PChar);
begin
  inherited;
  
  FillChar( PChar(Buffer+FRecordSize)^, CalcFieldsSize, 0 );
end;


{ Navigation / Editing }

{ Is DataSet sequenced ? (it is not sequenced for all SQL database) }
function TSDDataSet.IsSequenced: Boolean;
begin
  Result := False;
end;

function TSDDataSet.GetCanModify: Boolean;
begin
  Result := Assigned(FOnUpdateRecord) or Assigned(FUpdateObject);
end;

{ This method is used by the following methods: AppendRecord, InsertRecord }
procedure TSDDataSet.InternalAddRecord(Buffer: Pointer; Append: Boolean);
begin
  FRecCache.InsertRecord( Buffer, Append, True );
end;

{ Marks(as deleted) or deletes(if record still not in database) the record in the cache }
procedure TSDDataSet.InternalDelete;
begin
  FRecCache.DeleteRecord(ActiveBuffer);
end;

procedure TSDDataSet.InternalFirst;
begin
  FRecCache.SetToBegin;
end;

procedure TSDDataSet.InternalLast;
begin
  SetBusyState;
  try
    FRecCache.FetchAll;
    FRecCache.SetToEnd;
  finally
    ResetBusyState;
  end;
end;

{ Modify data in cache }
procedure TSDDataSet.InternalPost;
begin
  if State = dsEdit then
    FRecCache.ModifyRecord( ActiveBuffer )
  else
    FRecCache.InsertRecord( ActiveBuffer, True, False );
end;

{ refetch records without Close/Open methods and without OnClose/OnOpen events,
NOTE: but OnBeforeScroll/OnAfterScroll handlers are called (maybe in ApplyUpdates) ??? }
procedure TSDDataSet.InternalRefresh;
var
  SavePos: Integer;
begin
  if FRecCache = nil then Exit;	// dataset is inactive
  SetBusyState;
  try
    SavePos := FRecCache.RecNo;
    ISrvCloseResultSet;
    BindFields(False);	// resets BlobFieldCount and CalcFieldsSize
    FRecCache.Clear;

	// --- reexecute and refetch ---

	// if cursor was detached, but result set is stay active
    if not Assigned(FSrvDataSet) then
      OpenCursor(False)
    else begin 	// cursor is connected, just execute it again
      InitResultSet;
      if Self is TSDQuery then
        TSDQuery(Self).BindParams
      else if Self is TSDStoredProc then
        TSDStoredProc(Self).BindParams;
      ExecuteCursor;
    end;
	// fetch until the saved record
    while FRecCache.FetchRecord do
      if (SavePos+1) = FRecCache.Count then begin
        FRecCache.FPosition := SavePos;
        Break;
      end;
  finally
    ResetBusyState;
  end;
end;

procedure TSDDataSet.InternalGotoBookmark(Bookmark: TBookmark);
begin
	// There are not all records in cache
  if PBookmarkRec(Bookmark)^.iPos >= FRecCache.Count then
    FRecCache.FetchAll;
  FRecCache.FPosition := PBookmarkRec(Bookmark)^.iPos;
end;

procedure TSDDataSet.InternalSetToRecord(Buffer: PChar);
begin
  InternalGotoBookmark(Buffer + FBookmarkOffs);
end;

function TSDDataSet.GetBookmarkFlag(Buffer: PChar): TBookmarkFlag;
begin
  Result := PRecInfo(Buffer + FRecInfoOffs).BookmarkFlag;
end;

procedure TSDDataSet.SetBookmarkFlag(Buffer: PChar; Value: TBookmarkFlag);
begin
  PRecInfo(Buffer + FRecInfoOffs).BookmarkFlag := Value;
end;

procedure TSDDataSet.GetBookmarkData(Buffer: PChar; Data: Pointer);
begin
  Move(Buffer[FBookmarkOffs], Data^, BookmarkSize);
end;

procedure TSDDataSet.SetBookmarkData(Buffer: PChar; Data: Pointer);
begin
  Move(Data^, Buffer[FBookmarkOffs], BookmarkSize);
end;

function TSDDataSet.BookmarkValid(Bookmark: TBookmark): Boolean;
var
  InCacheNo: Integer;
begin
  Result := False;
  if Handle = nil then
    Exit;

  InCacheNo := PBookmarkRec(Bookmark)^.iPos;
  if (InCacheNo >= FRecCache.Count) or (InCacheNo < 0) then
    Exit;
	// If row was deleted
  if FRecCache.UpdateStatusRecords[InCacheNo] = usDeleted then
    Exit;

  Result := True;
end;

function TSDDataSet.CompareBookmarks(Bookmark1, Bookmark2: TBookmark): Integer;
const
  BkmLess           = -1;             { Bkm1 < Bkm2 }
  BkmEql            = 0;              { BookMarks are exactly the same }
  BkmGtr            = 1;              { Bkm1 > Bkm2 }
  BkmKeyEql         = 2;              { Only Bkm1.key_val = Bkm2.key_val }
  RetCodes: array[Boolean, Boolean] of ShortInt = ((BkmKeyEql, BkmLess), (BkmGtr, BkmEql));
begin
  { Check for uninitialized bookmarks }
  Result := RetCodes[Bookmark1 = nil, Bookmark2 = nil];
  if Result = BkmKeyEql then begin
    if Handle <> nil then begin
      if PBookmarkRec(Bookmark1)^.iPos = PBookmarkRec(Bookmark2)^.iPos then
        Result := BkmEql
      else if PBookmarkRec(Bookmark1)^.iPos < PBookmarkRec(Bookmark2)^.iPos then
        Result := BkmLess
      else
        Result := BkmGtr;
    end;
  end;
end;

{$IFDEF SD_VCL4}
{ Set record count in one readed block }
procedure TSDDataSet.SetBlockReadSize(Value: Integer);

  function CanBlockRead: Boolean;
  var
    i: Integer;
  begin
    Result := (BufferCount <= 1) and (DataSetField = nil);
{$IFDEF SD_VCL4}
    if Result then
      for i := 0 to FieldCount - 1 do
        if (Fields[i].DataType in [ftDataSet, ftReference]) then
        begin
          Result := False;
          Break;
        end;
{$ENDIF}
  end;

begin
  if Value <> BlockReadSize then begin
    if Value > 0 then begin

      Exit;	// at preset BlockRead is not supported

      if EOF or not CanBlockRead then Exit;
      UpdateCursorPos;
      inherited;
      BlockReadNext;
    end else
      inherited;
  end;
end;

{ Read BlockReadSize record at once in cache }
procedure TSDDataSet.BlockReadNext;
begin
  MoveBy(1);

  if CalcFieldsSize > 0 then begin
    GetCalcFields( CacheTempBuffer );
  end;
  DataEvent(deDataSetScroll, -1);
end;
{$ENDIF}

{ Datasource/Datalink Interaction }

procedure TSDDataSet.DataEvent(Event: TDataEvent; Info: Longint);
begin
  inherited DataEvent(Event, Info);
  case Event of
    dePropertyChange:	ClearFieldDefs;
  end;
end;

{ Index / Ranges }

{ Checking: whether correspond the field list to any index (by field number, by field order and other.) }
function TSDDataSet.MapsToIndex(Fields: TList; CaseInsensitive: Boolean): Boolean;
begin
  Result := False;
end;

{ Filters }

procedure TSDDataSet.ActivateFilters;
begin
  FFilterActivated := True;
end;

procedure TSDDataSet.DeactivateFilters;
begin
  FFilterActivated := False;
end;

procedure TSDDataSet.SetFilterData(const Text: string; Options: TFilterOptions);
begin
  if Active then
  begin
    CheckBrowseMode;
    if (Filter <> Text) or (FilterOptions <> Options) then
    begin
{      if Text <> '' then
        HFilter := CreateExprFilter(Text, Options, 0) else
        HFilter := nil;
      SetFilterHandle(FExprFilter, HFilter);}
    end;
  end;
  inherited SetFilterText(Text);
  inherited SetFilterOptions(Options);
  if Active and Filtered then First;
end;

procedure TSDDataSet.SetFilterOptions(Value: TFilterOptions);
begin
  SetFilterData(Filter, Value);
end;

procedure TSDDataSet.SetFilterText(const Value: string);
begin
  SetFilterData(Value, FilterOptions);
end;

procedure TSDDataSet.SetFiltered(Value: Boolean);
begin
  if Active then begin
    CheckBrowseMode;
    if Filtered <> Value then begin
      FRecCache.SetToBegin;
      if Value then ActivateFilters else DeactivateFilters;
      inherited SetFiltered(Value);
    end;
    First;
  end else
    inherited SetFiltered(Value);
end;

procedure TSDDataSet.SetOnFilterRecord(const Value: TFilterRecordEvent);
begin
  if Active then begin
    CheckBrowseMode;
    inherited SetOnFilterRecord(Value);
    if Filtered then First;
  end else
    inherited SetOnFilterRecord(Value);
end;

function TSDDataSet.FindRecord(Restart, GoForward: Boolean): Boolean;
var
  Status: Boolean;
begin
  CheckBrowseMode;
  DoBeforeScroll;
  SetFound(False);
  UpdateCursorPos;
  CursorPosChanged;
  if not Filtered then ActivateFilters;
  try
    if GoForward then begin
      if Restart then FRecCache.SetToBegin;
      Status := FRecCache.FindNextRecord;
    end else begin
      if Restart then FRecCache.SetToEnd;
      Status := FRecCache.FindPriorRecord;
    end;
  finally
    if not Filtered then DeactivateFilters;
  end;
  if Status then begin
    Resync([rmExact, rmCenter]);
    SetFound(True);
  end;
  Result := Found;
  if Result then DoAfterScroll;
end;

function TSDDataSet.RecordFilter(RecBuf: Pointer): Boolean;
var
  Accept: Boolean;
  SaveState: TDataSetState;
begin
  SaveState := SetTempState(dsFilter);
  FFilterBuffer := RecBuf;
  try
    Accept := True;
    if Assigned(OnFilterRecord) then
      OnFilterRecord(Self, Accept);
  except
    Application.HandleException(Self);
  end;
  RestoreState(SaveState);
  Result := Accept;
end;

{
  Returns position of the 1-st satisfying record in the cache (FResultSet)
}
function TSDDataSet.LocateRecord(const KeyFields: string; const KeyValues: Variant;
  Options: TLocateOptions; SyncCursor: Boolean): Integer;
var
  SaveState: TDataSetState;
  i: Integer;
begin
  CheckBrowseMode;
  CursorPosChanged;
  Result := -1;
	// temporary turn in Filter mode
  SaveState := SetTempState( dsFilter );
  SetBusyState;
  try
    i := 0;
    while i < FRecCache.Count do begin
        // in filter mode field values are got from FFilterBuffer
      FFilterBuffer := FRecCache[i];
      	// if a record is visible
      if FRecCache.IsVisibleRecord(i) then begin
        if VarIsEqual(FieldValues[KeyFields], KeyValues, loCaseInsensitive in Options, loPartialKey in Options) then begin
          Result := i;
          Break;
        end;
      end;
      Inc(i);
      if i = FRecCache.Count then begin
	// for avoiding fetch problem (especially, with Blob-fields)
        SetTempState( dsBrowse );
        try
          FRecCache.FetchAll;
        finally
          RestoreState( dsFilter );
        end;
      end;
    end;
  finally
    ResetBusyState;
    RestoreState( SaveState );
  end;
  if (Result >= 0) and SyncCursor then
    FRecCache.FPosition := Result;
end;

function TSDDataSet.Locate(const KeyFields: string;
  const KeyValues: Variant; Options: TLocateOptions): Boolean;
begin
  Result := False;
  DoBeforeScroll;
  if LocateRecord(KeyFields, KeyValues, Options, True) >= 0 then begin
    Resync([rmExact, rmCenter]);
    DoAfterScroll;
    Result := True;
  end;
end;

function TSDDataSet.Lookup(const KeyFields: string; const KeyValues: Variant;
  const ResultFields: string): Variant;
var
  Pos: Integer;
begin
  Result := Null;
  Pos := LocateRecord(KeyFields, KeyValues, [], False);
  if Pos >= 0 then begin
    SetTempState(dsCalcFields);
    try
      CalculateFields(FRecCache[Pos]);
      Result := FieldValues[ResultFields];
    finally
      RestoreState(dsBrowse);
    end;
  end;
end;


{ Cached Updates }

procedure TSDDataSet.CheckCachedUpdateMode;
begin
  if not FCachedUpdates then DatabaseError(SNoCachedUpdates);
end;

procedure TSDDataSet.ApplyUpdates;
var
  Status: Integer;
begin
  if State <> dsBrowse then Post;
  Status := ProcessUpdates(DelayedUpdPrepare);
  if Status <> SDE_ERR_NONE then
    if Status = SDE_ERR_UPDATEABORT then SysUtils.Abort
    else SDEError(Status);
end;

procedure TSDDataSet.CommitUpdates;
begin
  SDECheck( ProcessUpdates(DelayedUpdCommit) );
end;

procedure TSDDataSet.RollbackUpdates;
begin
  SDECheck( ProcessUpdates(DelayedUpdRollback) );
end;

procedure TSDDataSet.CancelUpdates;
begin
  Cancel;
  ProcessUpdates(DelayedUpdCancel);
end;

procedure TSDDataSet.RevertRecord;
var
  Status: Integer;
begin
  if State in dsEditModes then Cancel;
  Status := ProcessUpdates(DelayedUpdCancelCurrent);
  if Status <> SDE_ERR_NONE then
    SDECheck(Status);
end;

function TSDDataSet.ProcessUpdates(UpdCmd: TDelayedUpdCmd): Integer;
begin
  CheckActive;
  CheckCachedUpdateMode;
  UpdateCursorPos;
  case UpdCmd of
    DelayedUpdCancel: 		Result := FRecCache.UpdatesCancel;
    DelayedUpdCommit: 		Result := FRecCache.UpdatesCommit;
    DelayedUpdPrepare: 		Result := FRecCache.UpdatesPrepare;
    DelayedUpdCancelCurrent: 	Result := FRecCache.UpdatesCancelCurrent;
    DelayedUpdRollback: 	Result := FRecCache.UpdatesRollback;
  else
    Result := SDE_ERR_UPDATEABORT;
  end;
  Resync([]);
end;

function TSDDataSet.GetUpdatesPending: Boolean;
var
  i: Integer;
begin
  Result := False;
  if not Active then
    Exit;
  if State in (dsEditModes - [dsSetKey]) then begin
    Result := True;
    Exit;
  end;
  for i:=0 to FRecCache.Count-1 do
    if PRecInfo(FRecCache[i] + FRecInfoOffs)^.UpdateStatus <> usUnmodified then begin
      Result := True;
      Break;
    end;
end;

function TSDDataSet.GetEnableUpdateKinds: TUpdateKinds;
begin
  if Active then begin
    CheckCachedUpdateMode;
    Result := FEnableUpdateKinds;
  end else
    Result := [];
end;

procedure TSDDataSet.SetEnableUpdateKinds(Value: TUpdateKinds);
begin
  CheckCachedUpdateMode;
  CheckActive;
  FEnableUpdateKinds := Value;
end;

function TSDDataSet.GetUpdateRecordSet: TUpdateRecordTypes;
begin
  if Active then begin
    CheckCachedUpdateMode;
    Result := FUpdateRecordTypes;
  end else
    Result := [];
end;

procedure TSDDataSet.SetUpdateRecordSet(RecordTypes: TUpdateRecordTypes);
begin
  CheckCachedUpdateMode;
  CheckBrowseMode;
  UpdateCursorPos;
  FUpdateRecordTypes := RecordTypes;
  Resync([]);
end;

procedure TSDDataSet.SetUpdateObject(Value: TSDDataSetUpdateObject);
begin
  if Value <> FUpdateObject then begin
    if Assigned(FUpdateObject) and (FUpdateObject.DataSet = Self) then
      FUpdateObject.DataSet := nil;
    FUpdateObject := Value;
    if Assigned(FUpdateObject) then begin
      { If another dataset already references this updateobject, then remove the reference }
      if Assigned(FUpdateObject.DataSet) and (FUpdateObject.DataSet <> Self) then
        FUpdateObject.DataSet.UpdateObject := nil;
      FUpdateObject.DataSet := Self;
    end;
  end;
end;

function TSDDataSet.UpdateStatus: TUpdateStatus;
var
  BufPtr: PChar;
begin
  CheckCachedUpdateMode;
  if State = dsCalcFields
  then BufPtr := CalcBuffer
  else BufPtr := ActiveBuffer;
  Result := PRecInfo(BufPtr + FRecInfoOffs).UpdateStatus;
end;

{-------------------------------------------------------------------------------
			Common SQL server C/API Functions
-------------------------------------------------------------------------------}
function TSDDataSet.GetHandle: PSDCursor;
begin
  Result := nil;
  if FSrvDataSet <> nil then
    Result := FSrvDataSet.Handle;
end;

procedure TSDDataSet.ISrvInitFieldDefs;
begin
  ASSERT( Assigned(FSrvDataSet) );
  FSrvDataSet.InitFieldDefs;
end;

procedure TSDDataSet.ISrvExecute;
begin
  ASSERT( Assigned(FSrvDataSet) );
  FSrvDataSet.Execute;
end;

procedure TSDDataSet.ISrvCloseResultSet;
begin
  if Assigned(FSrvDataSet) then
    FSrvDataSet.CloseResultSet;
end;

procedure TSDDataSet.ISrvSetPreservation(Value: Boolean);
begin
  ASSERT( Assigned(FSrvDataSet) );
  FSrvDataSet.SetPreservation(Value);
end;

function TSDDataSet.ISrvReadBlob(FieldNo: Integer; var BlobData: string; Count: LongInt): Longint;
begin
  ASSERT( Assigned(FSrvDataSet) );
  Result := FSrvDataSet.ReadBlob(FieldNo, BlobData);
end;

function TSDDataSet.ISrvWriteBlob(FieldNo: Integer; const Buffer; Count: LongInt): Longint;
begin
  ASSERT( Assigned(FSrvDataSet) );
  Result := FSrvDataSet.WriteBlob(FieldNo, Buffer, Count);
end;

function TSDDataSet.ISrvWriteBlobByName(Name: string; const Buffer; Count: LongInt): Longint;
begin
  ASSERT( Assigned(FSrvDataSet) );
  Result := FSrvDataSet.WriteBlobByName(Name, Buffer, Count);
end;

{ Sets select buffers for data from a database }
procedure TSDDataSet.ISrvSetSelectBuffer;
begin
  FSrvDataSet.AllocSelectBuffer;

  FSrvDataSet.SetSelectBuffer;
end;

procedure TSDDataSet.ISrvFreeSelectBuffer;
begin
  if FSrvDataSet <> nil then
    FSrvDataSet.FreeSelectBuffer;
end;

{$IFDEF SD_VCL5}

{ TSDDataSet.IProviderSupport }

function TSDDataSet.PSIsSQLBased: Boolean;
var
  InProvider: Boolean;
begin
  InProvider := SetDSFlag(dsfProvider, True);
  try
    Result := Database.IsSQLBased;
  finally
    SetDSFlag(dsfProvider, InProvider);
  end;
end;

function TSDDataSet.PSIsSQLSupported: Boolean;
begin
  Result := True;
end;

procedure TSDDataSet.PSReset;
begin
  inherited PSReset;

  InternalRefresh;
end;

function TSDDataSet.PSGetQuoteChar: string;
var
  InProvider: Boolean;
begin
  InProvider := SetDSFlag(dsfProvider, True);
  try
	// some servers don't support, for example, quoted table names  
    Result := '';
  finally
    SetDSFlag(dsfProvider, InProvider);
  end;
end;

function TSDDataSet.PSInTransaction: Boolean;
var
  s: TSDSession;
  db: TSDDatabase;
begin
  Result := False;
  s := Sessions.List[SessionName];
  if Assigned(s) then begin
    db := s.DoFindDatabase(DatabaseName, Owner);
    Result := Assigned(db) and db.InTransaction;
  end;
end;

procedure TSDDataSet.PSStartTransaction;
begin
  SetDSFlag(dsfProvider, True);
  try
    Database.StartTransaction;
  except
    SetDSFlag(dsfProvider, False);
    raise;
  end;
end;

procedure TSDDataSet.PSEndTransaction(Commit: Boolean);
begin
  try
    if Commit then begin
      try
        Database.Commit;
      except
        Database.Rollback;
        raise;
      end;
    end else
      Database.Rollback;
  finally
    SetDSFlag(dsfProvider, False);
  end;
end;

function TSDDataSet.PSExecuteStatement(const ASQL: string; AParams: TParams; ResultSet: Pointer = nil): Integer;
var
  InProvider: Boolean;
  qryStat: TSDQuery;
begin
  Result := -1;
  InProvider := SetDSFlag(dsfProvider, True);
  qryStat := TSDQuery.Create(nil);
  try
    try
      qryStat.SessionName  := FSessionName;
      qryStat.DatabaseName := FDatabaseName;

      ASSERT( Assigned(FDatabase), Format(SFatalError, ['TSDDataSet.PSExecuteStatement']) );

      qryStat.SQL.Text := ReplaceQuestionParamMarker(FDatabase.ServerType, ASQL, AParams);
      qryStat.Params.Assign( AParams );
      SetUnknownParamTypeToInput( qryStat.Params );

      if Assigned( ResultSet ) then
        qryStat.Open
      else
        qryStat.ExecSQL;
      Result := qryStat.RowsAffected;
    except
      qryStat.Free;
      qryStat := nil;
      raise;
    end;
  finally
    SetDSFlag(dsfProvider, InProvider);
    if Assigned( ResultSet ) then
      TDataSet( ResultSet^ ) := qryStat
    else
      qryStat.Free;
  end;
end;

function TSDDataSet.PSGetUpdateException(E: Exception; Prev: EUpdateError): EUpdateError;
var
  PrevErr: Integer;
begin
  if E is ESDEngineError then begin
    if Prev <> nil
    then PrevErr := Prev.ErrorCode
    else PrevErr := 0;

    Result := EUpdateError.Create(E.Message, '', ESDEngineError(E).ErrorCode, PrevErr, E);
  end else
    Result := inherited PSGetUpdateException(E, Prev);
end;

function TSDDataSet.PSUpdateRecord(UpdateKind: TUpdateKind; Delta: TDataSet): Boolean;

  procedure AssignParams(DataSet: TDataSet; Params: TParams);
  var
    i: Integer;
    Old: Boolean;
    Param: TParam;
    PName: string;
    Field: TField;
    Value: Variant;
  begin
    for i := 0 to Params.Count - 1 do begin
      Param := Params[i];
      PName := Param.Name;
      Old := CompareText(Copy(PName, 1, 4), 'OLD_') = 0;
      if Old then System.Delete(PName, 1, 4);
      Field := DataSet.FindField(PName);
      if not Assigned(Field) then Continue;
      if Old then
        Param.AssignFieldValue(Field, Field.OldValue)
      else begin
        Value := Field.NewValue;
        if VarIsEmpty(Value) then Value := Field.OldValue;
        Param.AssignFieldValue(Field, Value);
      end;
    end;
  end;

var
  SQL, sTempSQL: string;
  Params: TParams;
  UpdateAction: TUpdateAction;
begin
  Result := False;
  if Assigned(OnUpdateRecord) then begin
    UpdateAction := uaFail;
    if Assigned(FOnUpdateRecord) then begin
      FOnUpdateRecord(Delta, UpdateKind, UpdateAction);
      Result := UpdateAction = uaApplied;
    end;
  end else if Assigned(UpdateObject) then begin
    if UpdateObject is TSDUpdateSQL then
      SQL := TSDUpdateSQL(FUpdateObject).GetSQL(UpdateKind).Text;
    if SQL <> '' then begin
      Params := TParams.Create;
      try
        SetString( sTempSQL, PChar(SQL), Length(SQL) );
        Params.ParseSQL(sTempSQL, True);
        AssignParams(Delta, Params);
        if PSExecuteStatement(SQL, Params) = 0 then
          DatabaseError(SRecordChanged);
        Result := True;
      finally
        Params.Free;
      end;
    end;
  end;
end;

{$ENDIF}

{*******************************************************************************
				TSDQueryDataLink
*******************************************************************************}
constructor TSDQueryDataLink.Create(AQuery: TSDQuery);
begin
  inherited Create;
  FQuery := AQuery;
end;

procedure TSDQueryDataLink.ActiveChanged;
begin
  if FQuery.Active then
    FQuery.RefreshParams;
end;

{$IFDEF SD_VCL4}
function TSDQueryDataLink.GetDetailDataSet: TDataSet;
begin
  Result := FQuery;
end;
{$ENDIF}

procedure TSDQueryDataLink.RecordChanged(Field: TField);
begin
  if (Field = nil) and FQuery.Active then
    FQuery.RefreshParams;
end;

procedure TSDQueryDataLink.CheckBrowseMode;
begin
  if FQuery.Active then FQuery.CheckBrowseMode;
end;


{*******************************************************************************
				TSDQuery
*******************************************************************************}
constructor TSDQuery.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FSQL := TStringList.Create;
  TStringList(SQL).OnChange := QueryChanged;
  FParamCheck := True;
  FParams := TSDHelperParams.Create {$IFDEF SD_VCL4} (Self) {$ENDIF};
  FDataLink := TSDQueryDataLink.Create(Self);
  FRowsAffected := -1;
end;

destructor TSDQuery.Destroy;
begin
  Destroying;
  Disconnect;
  if Assigned(FSQL) then FSQL.Free;
  if Assigned(FParams) then FParams.Free;
  if Assigned(FDataLink) then FDataLink.Free;
  inherited Destroy;
end;

procedure TSDQuery.Disconnect;
begin
  Close;
  UnPrepare;
  DestroyHandle;
end;

{ Size of parameter's buffer for binding }
function TSDQuery.BindBufferSize(Param: TSDHelperParam): Integer;
begin
  with Param do
    if DataType in [ftBlob..ftDBaseOLE] then
      Result := 0		//SizeOf(BlobParamDesc)
    else if FDatabase.ISrvRequiredCnvtFieldType(DataType) then
      Result := FDatabase.ISrvNativeDataSize(DataType)
    else
      Result := GetDataSize;
end;

procedure TSDQuery.PrepareCursor;
begin
  FRowsAffected := -1;
  if FSrvDataSet = nil then
    FSrvDataSet := CreateHandle;
  if Length(Text) > 1 then begin
    if not Prepared then
      ISrvPrepareSQL( PChar( FText ) )
  end else
    DatabaseError(SEmptySQLStatement);
end;

procedure TSDQuery.UnPrepareCursor;
begin
	// after prepare may be changed properties: Params and Field(type, count)
  ISrvFreeBindBuffer;
  if DefaultFields then DestroyFields;
  ISrvFreeSelectBuffer;
end;

procedure TSDQuery.Prepare;
begin
  SetDSFlag(dsfPrepared, True);
  try
    SetPrepare(True);
  except
    SetDSFlag(dsfPrepared, False);
    raise;
  end;
end;

procedure TSDQuery.ParseSQL(FldInfo, TblInfo: TStrings);
var
  SavePrepared: Boolean;
begin
  SavePrepared := Prepared;
  try
    if not Prepared then
      Prepare;

    FSrvDataSet.ParseSQLText( FText, FldInfo, TblInfo );

  finally
    if not SavePrepared then
      UnPrepare;
  end;
end;

procedure TSDQuery.ExecuteCursor;
begin
  SetConnectionState( True );
  try
    ISrvExecute;
  finally
    SetConnectionState( False );
  end;
end;

procedure TSDQuery.ExecSQL;
begin
  FRowsAffected := -1;
  CheckInActive;

  SetBusyState;
  SetDSFlag(dsfExecSQL, True);
  try
    if not Prepared then
      SetPrepare(True);
    if not Assigned(SrvDataSet.BindBuffer) then
      ISrvAllocBindBuffer;
    BindParams;
    ExecuteCursor;

    FRowsAffected := ISrvGetRowsAffected;

    ISrvFreeBindBuffer;
  finally
    SetDSFlag(dsfExecSQL, False);
    ResetBusyState;
  end;
end;

procedure TSDQuery.DoBeforeOpen;
begin
	// make sure that all params have a type set
  SetUnknownParamTypeToInput( Params );

  inherited;
end;

procedure TSDQuery.InternalOpen;
begin
  FRowsAffected := -1;

  if FDataLink.DataSource <> nil then
    SetParamsFromCursor;

  ISrvAllocBindBuffer;
  BindParams;

  inherited;
end;

procedure TSDQuery.InternalClose;
begin
  inherited;

  ISrvFreeBindBuffer;
end;

procedure TSDQuery.BindParams;
begin
  ISrvBindParams;
end;

procedure TSDQuery.SetPrepared(Value: Boolean);
begin
  if Value
  then Prepare
  else UnPrepare;
end;

procedure TSDQuery.SetPrepare(Value: Boolean);
begin
  if Active then DatabaseError(SDataSetOpen);
  if (Prepared <> Value) or
     (Prepared and not Assigned(FSrvDataSet))	// FSrvDataSet(cursor) was destroyed after Commit/Rollback or Detach
  then begin
    if Value
    then PrepareCursor
    else UnPrepareCursor;
    FPrepared := Value;
  end;
end;

procedure TSDQuery.UnPrepare;
begin
  SetPrepare(False);
  SetDSFlag(dsfPrepared, False);
end;

function TSDQuery.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

procedure TSDQuery.SetDataSource(Value: TDataSource);
begin
  if IsLinkedTo(Value) then DatabaseError(SCircularDataLink);
  FDataLink.DataSource := Value;
end;

function TSDQuery.SetDSFlag(Flag: Integer; Value: Boolean): Boolean;
begin
  if (not Value) then
    if DSFlags - [Flag] = [] then
      SetPrepare(False);
  Result := inherited SetDSFlag(Flag, Value);
end;

function TSDQuery.GetParamsCount: Word;
begin
  Result := FParams.Count;
end;

function TSDQuery.GetRowsAffected: Integer;
begin
  Result := FRowsAffected;
end;

function TSDQuery.ParamByName(const Value: string): TSDHelperParam;
begin
  Result := FParams.ParamByName(Value);
end;

procedure TSDQuery.SetParamsFromCursor;
var
  I: Integer;
  DataSet: TDataSet;
begin
  if FDataLink.DataSource <> nil then begin
    DataSet := FDataLink.DataSource.DataSet;
    if DataSet <> nil then begin
      DataSet.FieldDefs.Update;
      for I := 0 to FParams.Count - 1 do
        with FParams[I] do
          if not Bound then begin
            AssignField(DataSet.FieldByName(Name));
            Bound := False;
          end;
    end;
  end;
end;

procedure TSDQuery.SetParamsList(Value: TSDHelperParams);
begin
  FParams.AssignValues(Value);
end;

{ It is necessary to reopen the query, for example, when a master record is changed }
procedure TSDQuery.RefreshParams;
	// Returns False if the field value differs from the parameter value
  function ParamValueOK(Param: TSDHelperParam; Field: TField): Boolean;
  begin
    Result := False;
    if not Assigned( Field ) then
      Exit;
    if (Param.IsNull xor Field.IsNull) or
       (Param.DataType <> Field.DataType)
    then
      Exit;
    Result := (Param.IsNull and Field.IsNull) or
              (Param.Value = Field.Value);
  end;

var
  DataSet: TDataSet;
  i: Integer;
  IsValueEqual: Boolean; // whether parameter and the corresponded field values are equal
begin
  DisableControls;
  try
    if FDataLink.DataSource <> nil then begin
    	// get a master dataset
      DataSet := FDataLink.DataSource.DataSet;
      if DataSet <> nil then
        if DataSet.Active and (DataSet.State <> dsSetKey) then begin
          IsValueEqual := not UniDirectional;
          for I := 0 to FParams.Count - 1 do begin
            if not IsValueEqual then
              Break;
            IsValueEqual := IsValueEqual and ParamValueOK( FParams[I], DataSet.FindField(FParams[I].Name) );
          end;
          	// if field values were changed or the dataset is unidirectional (else it is impossible to go to the first record)
          if not IsValueEqual then begin
            Close;
            Open;
          end;
        end;
    end;
  finally
    EnableControls;
  end;
end;

{ Parse SQL-statement and create instances of TSDParam }
procedure TSDQuery.CreateParams(List: TSDHelperParams; const Value: PChar);
var
  CurPos, StartPos: PChar;
  CurChar: Char;
  Literal: Char;	// #0, when the current character is not quoted, else it's equal the last significant quote
  EmbeddedLiteral: Boolean;
  Name: string;

  function NameDelimiter: Boolean;
  begin
    Result := CurChar in [' ', ',', ';', ')', #13, #10];
  end;

  function IsLiteral: Boolean;
  begin
    Result := CurChar in ['''', '"'];
  end;

  function StripLiterals(Buffer: PChar): string;
  var
    Len: Word;
    TempBuf: PChar;

    procedure StripChar(Value: Char);
    begin
      if TempBuf^ = Value then
        StrMove(TempBuf, TempBuf + 1, Len - 1);
      if TempBuf[StrLen(TempBuf) - 1] = Value then
        TempBuf[StrLen(TempBuf) - 1] := #0;
    end;

  begin
    Len := StrLen(Buffer) + 1;
    TempBuf := AllocMem(Len);
    Result := '';
    try
      StrCopy(TempBuf, Buffer);
      StripChar('''');
      StripChar('"');
      Result := StrPas(TempBuf);
    finally
      FreeMem(TempBuf, Len);
    end;
  end;

begin
  CurPos := Value;
  Literal := #0;	// #0 - not inside a quoted string
  EmbeddedLiteral := False;
  repeat
    CurChar := CurPos^;
    	// Is it parameter name ?
    if (CurChar = ':') and not Boolean(Literal) and ((CurPos + 1)^ <> ':') then begin
      StartPos := CurPos;
      while (CurChar <> #0) and (Boolean(Literal) or not NameDelimiter) do begin
        Inc(CurPos);
        CurChar := CurPos^;
        if IsLiteral then begin
		// To process a quote, which is inside a parameter name like :"param's name"        
          if not Boolean(Literal) then
            Literal := CurChar
          else if Literal = CurChar then	// the quoted string has to be finished by the same quote character
            Literal := #0;
            	// if a parameter name is quoted
          if CurPos = StartPos + 1 then EmbeddedLiteral := True;
        end;
      end;
      CurPos^ := #0;
      if EmbeddedLiteral then begin
        Name := StripLiterals(StartPos + 1);
        EmbeddedLiteral := False;
      end else
        Name := StrPas(StartPos + 1);
      if Assigned(List) then begin
{$IFDEF SD_VCL4}
        TParam(List.Add).Name := Name;
        List.ParamByName(Name).ParamType := ptInput;
{$ELSE}
        List.CreateParam(ftUnknown, Name, ptInput);
{$ENDIF}
      end;
      CurPos^ := CurChar;
    end else if (CurChar = ':') and not Boolean(Literal) and ((CurPos + 1)^ = ':') then
      StrMove(CurPos, CurPos + 1, StrLen(CurPos) + 1)
    else if IsLiteral then begin
	// To process a quote, which is inside a quoted string like "param's :name"
      if not Boolean(Literal) then
        Literal := CurChar
      else if Literal = CurChar then
        Literal := #0;
    end;
    
    Inc(CurPos);
  until CurChar = #0;
end;

procedure TSDQuery.SetQuery(Value: TStrings);
begin
  if SQL.Text <> Value.Text then begin
    Disconnect;
    SQL.BeginUpdate;
    try
      SQL.Assign(Value);
    finally
      SQL.EndUpdate;
    end;
  end;
end;

procedure TSDQuery.QueryChanged(Sender: TObject);
var
  List: TSDHelperParams;
begin
  FText := SQL.Text;
  if not (csReading in ComponentState) then begin
    if Active then Close;
    UnPrepare;
    if ParamCheck or (csDesigning in ComponentState) then begin
      List := TSDHelperParams.Create {$IFDEF SD_VCL4}(Self){$ENDIF};
      try
        CreateParams(List, PChar(Text));
        List.AssignValues(FParams);
        FParams.Clear;
        FParams.Assign(List);
      finally
        List.Free;
      end;
    end;
    DataEvent(dePropertyChange, 0);	// FieldDefs.Updated := False - not valid
  end else
    CreateParams(nil, PChar(Text));
end;

{$IFDEF SD_VCL4}
procedure TSDQuery.GetDetailLinkFields(MasterFields, DetailFields: TList);

  function AddFieldToList(const FieldName: string; DataSet: TDataSet; List: TList): Boolean;
  var
    Field: TField;
  begin
    Field := DataSet.FindField(FieldName);
    if (Field <> nil) then
      List.Add(Field);
    Result := Field <> nil;
  end;

var
  i: Integer;
begin
  MasterFields.Clear;
  DetailFields.Clear;
  if (DataSource <> nil) and (DataSource.DataSet <> nil) then
    for i := 0 to Params.Count - 1 do
      if AddFieldToList(Params[i].Name, DataSource.DataSet, MasterFields) then
        AddFieldToList(Params[i].Name, Self, DetailFields);
end;

procedure TSDQuery.DefineProperties(Filer: TFiler);

  function WriteData: Boolean;
  begin
    if Filer.Ancestor <> nil
    then Result := not FParams.IsEqual(TSDQuery(Filer.Ancestor).FParams)
    else Result := FParams.Count > 0;
  end;

begin
  inherited DefineProperties(Filer);

  Filer.DefineProperty('ParamData', ReadParamData, WriteParamData, WriteData);
end;

procedure TSDQuery.ReadParamData(Reader: TReader);
begin
  Reader.ReadValue;
  Reader.ReadCollection(FParams);
end;

procedure TSDQuery.WriteParamData(Writer: TWriter);
begin
  Writer.WriteCollection(Params);
end;
{$ENDIF}

{$IFDEF SD_VCL5}

{ TSDQuery.IProviderSupport }

function TSDQuery.PSGetParams: TParams;
begin
  Result := Params;
end;

procedure TSDQuery.PSSetParams(AParams: TParams);
var
  InProvider: Boolean;
  i: Integer;
  SavedNames: TStrings;
begin
  if Active then
    Close;

  InProvider := SetDSFlag(dsfProvider, True);
  try
    ASSERT( Assigned(FDatabase), Format(SFatalError, ['TSDDataSet.PSSetParams']) );

    SQL.Text := ReplaceQuestionParamMarker(FDatabase.ServerType, SQL.Text, AParams);
  finally
    SetDSFlag(dsfProvider, InProvider);
  end;
	//  Params.Assign(AParams) is imposible to use, because it changes parameter name, which is used to bind a value
  if AParams.Count <> 0 then begin
    Params.BeginUpdate;
    SavedNames := TStringList.Create;
    try
    	// save old parameter names
      for i:=0 to Params.Count-1 do
        SavedNames.Add(Params[i].Name);
      Params.Clear;
      for i:=0 to AParams.Count-1 do begin
        Params.Add.Assign(AParams[i]);
	// restore an old parameter name
        if i < SavedNames.Count then
          Params[Params.Count-1].Name := SavedNames[i];
      end;

      SetUnknownParamTypeToInput(Params);
    finally
      Params.EndUpdate;
      SavedNames.Free;
    end;
  end;
end;

function TSDQuery.PSGetTableName: string;
begin
	// Return tablename in SQL
  Result := GetTableNameFromSQL(SQL.Text);
end;

procedure TSDQuery.PSExecute;
begin
  ExecSQL;
end;

procedure TSDQuery.PSSetCommandText(const CommandText: string);
begin
  if CommandText <> '' then
    SQL.Text := CommandText;
end;
{$ENDIF}

{-------------------------------------------------------------------------------
			SQL server C/API Functions
-------------------------------------------------------------------------------}
procedure TSDQuery.ISrvAllocBindBuffer;
begin
  FSrvDataSet.AllocBindBuffer;
end;

procedure TSDQuery.ISrvFreeBindBuffer;
begin
  if FSrvDataSet <> nil then
    FSrvDataSet.FreeBindBuffer;
end;

procedure TSDQuery.ISrvBindParams;
begin
  FSrvDataSet.QBindParams;
end;

procedure TSDQuery.ISrvPrepareSQL(Value: PChar);
begin
  FSrvDataSet.QPrepareSQL(Value);
end;

function TSDQuery.ISrvGetRowsAffected: Integer;
begin
  Result := FSrvDataSet.QGetRowsAffected;
end;


{*******************************************************************************
 					TSDUpdateSQL
*******************************************************************************}
constructor TSDUpdateSQL.Create(AOwner: TComponent);
var
  UpdateKind: TUpdateKind;
begin
  inherited Create(AOwner);

  for UpdateKind := Low(TUpdateKind) to High(TUpdateKind) do begin
    FSQLText[UpdateKind] := TStringList.Create;
    TStringList(FSQLText[UpdateKind]).OnChange := SQLChanged;
  end;
end;

destructor TSDUpdateSQL.Destroy;
var
  UpdateKind: TUpdateKind;
begin
  if Assigned(FDataSet) and (FDataSet.UpdateObject = Self) then
    FDataSet.UpdateObject := nil;
  for UpdateKind := Low(TUpdateKind) to High(TUpdateKind) do
    FSQLText[UpdateKind].Free;
    
  inherited Destroy;
end;

procedure TSDUpdateSQL.ExecSQL(UpdateKind: TUpdateKind);
begin
  with Query[UpdateKind] do begin
    ExecSQL;
    if RowsAffected <> 1 then DatabaseError(SUpdateFailed);
  end;
end;

function TSDUpdateSQL.GetQuery(UpdateKind: TUpdateKind): TSDQuery;
begin
  if not Assigned(FQueries[UpdateKind]) then begin
    FQueries[UpdateKind] := TSDQuery.Create(Self);
    FQueries[UpdateKind].SQL.Assign(FSQLText[UpdateKind]);

    ASSERT( FDataSet <> nil, 'Property TSPUpdateObject.DataSet = nil !!!' );

    if (FDataSet is TSDDataSet) then begin
      FQueries[UpdateKind].SessionName  := TSDDataSet(FDataSet).SessionName;
      FQueries[UpdateKind].DatabaseName := TSDDataSet(FDataSet).DatabaseName;
    end;
  end;
  Result := FQueries[UpdateKind];
end;

function TSDUpdateSQL.GetSQL(UpdateKind: TUpdateKind): TStrings;
begin
  Result := FSQLText[UpdateKind];
end;

function TSDUpdateSQL.GetSQLIndex(Index: Integer): TStrings;
begin
  Result := FSQLText[TUpdateKind(Index)];
end;

function TSDUpdateSQL.GetDataSet: TSDDataSet;
begin
  Result := FDataSet;
end;

procedure TSDUpdateSQL.SetDataSet(ADataSet: TSDDataSet);
var
  UpdateKind: TUpdateKind;
begin
  if FDataSet = ADataSet then
    Exit;

  for UpdateKind := Low(TUpdateKind) to High(TUpdateKind) do begin
    if Assigned(FQueries[UpdateKind]) then begin
      FQueries[UpdateKind].UnPrepare;
      FQueries[UpdateKind].Free;
      FQueries[UpdateKind] := nil;
    end;
  end;

  FDataSet := ADataSet;
end;

procedure TSDUpdateSQL.SetSQL(UpdateKind: TUpdateKind; Value: TStrings);
begin
  FSQLText[UpdateKind].Assign(Value);
end;

procedure TSDUpdateSQL.SetSQLIndex(Index: Integer; Value: TStrings);
begin
  SetSQL(TUpdateKind(Index), Value);
end;

procedure TSDUpdateSQL.SQLChanged(Sender: TObject);
var
  UpdateKind: TUpdateKind;
begin
  for UpdateKind := Low(TUpdateKind) to High(TUpdateKind) do
    if Sender = FSQLText[UpdateKind] then begin
      if Assigned(FQueries[UpdateKind]) then begin
        FQueries[UpdateKind].Params.Clear;
        FQueries[UpdateKind].SQL.Assign(FSQLText[UpdateKind]);
      end;
      Break;
    end;
end;

procedure TSDUpdateSQL.SetParams(UpdateKind: TUpdateKind);
var
  I: Integer;
  Old: Boolean;
  Param: TSDHelperParam;
  PName: string;
  Field: TField;
begin
  if not Assigned(FDataSet) then Exit;
  with Query[UpdateKind] do begin
    for I := 0 to Params.Count - 1 do begin
      Param := Params[I];
      PName := Param.Name;
      Old := CompareText( Copy(PName, 1, 4), 'OLD_' ) = 0;
      if Old then System.Delete(PName, 1, 4);
      Field := FDataSet.FindField(PName);
      if not Assigned(Field) then Continue;
      if Old
      then Param.AssignFieldValue(Field, Field.OldValue)
      else Param.AssignFieldValue(Field, Field.NewValue);
    end;
  end;
end;

procedure TSDUpdateSQL.Apply(UpdateKind: TUpdateKind);
begin
  SetParams(UpdateKind);
  ExecSQL(UpdateKind);
end;

{$IFNDEF SD_VCL4}

{*******************************************************************************
					TSDParam
*******************************************************************************}
constructor TSDParam.Create(AParamList: TSDParams; AParamType: TSDParamType);
begin
  if AParamList <> nil then AParamList.AddParam(Self);
  FName := '';
  DataType := ftUnknown;
  FParamType := AParamType;
  FBound := False;
  FNull := True;
end;

destructor TSDParam.Destroy;
begin
  if FParamList <> nil then FParamList.RemoveParam(Self);
end;

function TSDParam.IsEqual(Value: TSDParam): Boolean;
begin
  Result :=
  	(VarType(FData) = VarType(Value.FData)) and
    	(FData = Value.FData) and (Name = Value.Name) and
    	(DataType = Value.DataType) and (IsNull = Value.IsNull) and
    	(Bound = Value.Bound) and (ParamType = Value.ParamType);
end;

procedure TSDParam.SetDataType(Value: TFieldType);
begin
  FData := 0;
  FDataType := Value;
end;

procedure TSDParam.SetParamName(const Value: string);
begin
  FName := Value;
end;

function TSDParam.GetParamName: string;
begin
  Result := FName;
end;

function TSDParam.GetDataSize: Integer;
begin
  case DataType of
    ftString,
    ftMemo:     Result := Length(AsString) + 1;		// in Delphi 3: VarToStr(FData) returns '0'(and Length(FData)=1) in case of empty string
    ftBoolean: 	Result := SizeOf(WordBool);
    ftBCD,
    ftDateTime,
    ftCurrency,
    ftFloat: 	Result := SizeOf(Double);
    ftTime,
    ftDate,
    ftInteger: 	Result := SizeOf(LongInt);
    ftSmallint: Result := SizeOf(Integer);
    ftWord: 	Result := SizeOf(Word);
    ftBlob,
    ftGraphic..ftDBaseOLE: Result := Length(FData);
  else
    if DataType = ftUnknown
    then DatabaseErrorFmt(SUnknownFieldType, [Name])
    else DatabaseErrorFmt(SBadFieldType, [Name]);
    Result := 0;
  end;
end;

procedure TSDParam.GetData(Buffer: Pointer);
begin
  case DataType of
    ftUnknown: 	DatabaseErrorFmt(SUnknownFieldType, [FName]);
    ftString,
    ftMemo: 	StrMove(Buffer, PChar(string(FData)), Length(FData) + 1);
    ftSmallint: Integer(Buffer^) := FData;
    ftWord: 	Word(Buffer^) := FData;
    ftInteger: 	LongInt(Buffer^) := FData;
	{ DateTime fields stored in Buffer as TDateTimeRec }
    ftTime: 	Integer(Buffer^) := DateTimeToTimeStamp(AsDateTime).Time;
    ftDate: 	Integer(Buffer^) := DateTimeToTimeStamp(AsDateTime).Date;
    ftDateTime: Double(Buffer^)  := TimeStampToMSecs(DateTimeToTimeStamp(AsDateTime));
    ftBCD,
    ftCurrency,
    ftFloat: 	Double(Buffer^) := FData;
    ftBoolean: 	WordBool(Buffer^) := FData;
    ftBlob,
    ftGraphic..ftTypedBinary: Move(PChar(string(FData))^, Buffer^, Length(FData));
  else
    DatabaseError( Format(SBadFieldType, [FName]) );
  end;
end;

procedure TSDParam.SetBlobData(Buffer: Pointer; Size: Integer);
var
  DataStr: string;
begin
  SetLength(DataStr, Size);
  Move(Buffer^, PChar(DataStr)^, Size);
  AsBlob := DataStr;
end;

procedure TSDParam.SetData(Buffer: Pointer);
var
  TimeStamp: TTimeStamp;
begin
  case DataType of
    ftUnknown:	DatabaseError( Format(SUnknownFieldType, [FName]) );
    ftString:	AsString := StrPas(Buffer);
    ftWord:	AsWord := Integer(Buffer^);
    ftSmallint:	AsSmallInt := Integer(Buffer^);
    ftInteger: 	AsInteger := Integer(Buffer^);
	{ DateTime fields received from Buffer as TDateTimeRec }
    ftTime:
      begin
        TimeStamp.Time := LongInt(Buffer^);
        TimeStamp.Date := DateDelta;
        AsTime := TimeStampToDateTime(TimeStamp);
      end;
    ftDate:
      begin
        TimeStamp.Time := 0;
        TimeStamp.Date := Integer(Buffer^);
        AsDate := TimeStampToDateTime(TimeStamp);
      end;
    ftDateTime:
      begin
        TimeStamp.Time := 0;
        TimeStamp.Date := Integer(Buffer^);
        AsDateTime := TimeStampToDateTime(MSecsToTimeStamp(Double(Buffer^)));
      end;
    ftBCD: 	AsBCD := Double(Buffer^);
    ftCurrency: AsCurrency := Double(Buffer^);
    ftFloat: 	AsFloat := Double(Buffer^);
    ftBoolean: 	AsBoolean := WordBool(Buffer^);
  else
    DatabaseError( Format(SBadFieldType, [FName]) );
  end;
end;

procedure TSDParam.SetText(const Value: string);
begin
  InitValue;
  if DataType = ftUnknown then DataType := ftString;
  FData := Value;
  case DataType of
    ftDateTime, ftTime, ftDate: FData := VarToDateTime(FData);
    ftBCD: FData := Currency(FData);
    ftCurrency, ftFloat: FData := Single(FData);
    ftInteger, ftSmallInt, ftWord: FData := Integer(FData);
    ftBoolean: FData := Boolean(FData);
  end;
end;

procedure TSDParam.Assign(Source: TPersistent);
begin
  if Source is TSDParam then
    AssignParam(TSDParam(Source))
  else if Source is TField then
    AssignField(TField(Source))
  else
    inherited Assign(Source);
end;

procedure TSDParam.AssignTo(Dest: TPersistent);
begin
  if Dest is TField then
    TField(Dest).Value := FData
  else
    inherited AssignTo(Dest);
end;

procedure TSDParam.AssignParam(Param: TSDParam);
begin
  if Param <> nil then begin
    DataType := Param.DataType;
    if Param.IsNull then Clear
    else begin
      InitValue;
      FData := Param.FData;
    end;
    FBound := Param.Bound;
    Name := Param.Name;
    if ParamType = ptUnknown then FParamType := Param.ParamType;
  end;
end;

procedure TSDParam.AssignField(Field: TField);
begin
  if Field <> nil then begin
    if (Field.DataType = ftMemo) and (Field.Size > 255)
    then DataType := ftString
    else DataType := Field.DataType;
    if Field.IsNull then Clear
    else begin
      InitValue;
      FData := Field.Value;
    end;
    FBound := True;
    Name := Field.FieldName;
  end;
end;

procedure TSDParam.AssignFieldValue(Field: TField; const Value: Variant);
begin
  if Field <> nil then begin
    if (Field.DataType = ftMemo) and (Field.Size > 255)
    then DataType := ftString
    else DataType := Field.DataType;
    if VarIsNull(Value) then Clear
    else begin
      InitValue;
      FData := Value;
    end;
    FBound := True;
  end;
end;

procedure TSDParam.Clear;
begin
  FNull := True;
  FData := 0;
end;

procedure TSDParam.InitValue;
begin
  FBound := True;
  FNull := False;
end;

procedure TSDParam.LoadFromFile(const FileName: string; BlobType: TBlobType);
var
  Stream: TStream;
begin
  Stream := TFileStream.Create(FileName, fmOpenRead);
  try
    LoadFromStream(Stream, BlobType);
  finally
    Stream.Free;
  end;
end;

procedure TSDParam.LoadFromStream(Stream: TStream; BlobType: TBlobType);
var
  DataStr: string;
  Len: Integer;
begin
  with Stream do begin
    InitValue;
    FDataType := BlobType;
    Len := Size;
    SetLength(DataStr, Len);
    ReadBuffer(Pointer(DataStr)^, Len);
    FData := DataStr;
  end;
end;

function TSDParam.GetAsBoolean: Boolean;
begin
  Result := FData;
end;

procedure TSDParam.SetAsBoolean(Value: Boolean);
begin
  InitValue;
  DataType := ftBoolean;
  FData := Value;
end;

function TSDParam.GetAsFloat: Double;
begin
  Result := FData;
end;

procedure TSDParam.SetAsFloat(Value: Double);
begin
  InitValue;
  DataType := ftFloat;
  FData := Value;
end;

function TSDParam.GetAsBCD: Currency;
begin
  Result := FData;
end;

procedure TSDParam.SetAsBCD(Value: Currency);
begin
  InitValue;
  FData := Value;
  FDataType := ftBCD;
end;

procedure TSDParam.SetAsCurrency(Value: Double);
begin
  SetAsFloat(Value);
  FDataType := ftCurrency;
end;

function TSDParam.GetAsInteger: Longint;
begin
  Result := FData;
end;

procedure TSDParam.SetAsInteger(Value: Longint);
begin
  InitValue;
  DataType := ftInteger;
  FData := Value;
end;

procedure TSDParam.SetAsWord(Value: LongInt);
begin
  SetAsInteger(Value);
  FDataType := ftWord;
end;

procedure TSDParam.SetAsSmallInt(Value: LongInt);
begin
  SetAsInteger(Value);
  FDataType := ftSmallint;
end;

function TSDParam.GetAsString: string;
begin
  if not IsNull then
    case DataType of
      ftBoolean:
        if FData then Result := STextTrue
        else Result := STextFalse;
      ftDateTime,
      ftDate,
      ftTime:	Result := VarFromDateTime(FData)
    else
      Result := FData;
    end
  else
    Result := ''
end;

procedure TSDParam.SetAsString(const Value: string);
begin
  InitValue;
  DataType := ftString;
  FData := Value;
end;

function TSDParam.GetAsMemo: string;
begin
  Result := FData;
end;

procedure TSDParam.SetAsMemo(const Value: string);
begin
  InitValue;
  DataType := ftMemo;
  FData := Value;
end;

procedure TSDParam.SetAsBlob(Value: TBlobData);
begin
  InitValue;
  DataType := ftBlob;
  FData := Value;
end;

procedure TSDParam.SetAsDate(Value: TDateTime);
begin
  InitValue;
  DataType := ftDate;
  FData := Value;
end;

procedure TSDParam.SetAsTime(Value: TDateTime);
begin
  InitValue;
  FDataType := ftTime;
  FData := Value;
end;

procedure TSDParam.SetAsDateTime(Value: TDateTime);
begin
  InitValue;
  FDataType := ftDateTime;
  FData := Value;
end;

function TSDParam.GetAsDateTime: TDateTime;
begin
  if IsNull
  then Result := 0
  else Result := VarToDateTime(FData);
end;

procedure TSDParam.SetAsVariant(Value: Variant);
begin
  InitValue;
  case VarType(Value) of
    varSmallint:DataType := ftSmallInt;
    varInteger:	DataType := ftInteger;
    varCurrency:DataType := ftBCD;
    varSingle,
    varDouble: 	DataType := ftFloat;
    varDate: 	DataType := ftDateTime;
    varBoolean:	DataType := ftBoolean;
    varString:	DataType := ftString;
  else
    DataType := ftUnknown;
  end;
  FData := Value;
end;

function TSDParam.GetAsVariant: Variant;
begin
  Result := FData;
end;


{*******************************************************************************
					TSDParams
*******************************************************************************}
constructor TSDParams.Create;
begin
  FItems := TList.Create;
end;

destructor TSDParams.Destroy;
begin
  Clear;
  FItems.Free;
  inherited Destroy;
end;

procedure TSDParams.Assign(Source: TPersistent);
var
  I: Integer;
begin
  if Source is TSDParams then begin
    Clear;
    for I := 0 to TSDParams(Source).Count - 1 do
      with TSDParam.Create(Self, ptUnknown) do
        AssignParam(TSDParams(Source)[I]);
  end else
    inherited Assign(Source);
end;

procedure TSDParams.AssignTo(Dest: TPersistent);
begin
  if Dest is TSDParams
  then TSDParams(Dest).Assign(Self)
  else inherited AssignTo(Dest);
end;

procedure TSDParams.AssignValues(Value: TSDParams);
var
  I, J: Integer;
begin
  for I := 0 to Count - 1 do
    for J := 0 to Value.Count - 1 do
      if Items[I].Name = Value[J].Name then begin
        Items[I].AssignParam(Value[J]);
        Break;
      end;
end;

procedure TSDParams.AddParam(Value: TSDParam);
begin
  FItems.Add(Value);
  Value.FParamList := Self;
end;

procedure TSDParams.RemoveParam(Value: TSDParam);
begin
  FItems.Remove(Value);
  Value.FParamList := nil;
end;

function TSDParams.CreateParam(FldType: TFieldType; const ParamName: string; ParamType: TSDParamType): TSDParam;
begin
  Result := TSDParam.Create(Self, ParamType);
  with Result do begin
    Name := ParamName;
    DataType :=  FldType;
  end;
end;

function TSDParams.Count: Integer;
begin
  Result := FItems.Count;
end;

procedure TSDParams.Clear;
begin
  while FItems.Count > 0 do TSDParam(FItems.Last).Free;
end;

function TSDParams.IsEqual(Value: TSDParams): Boolean;
var
  I: Integer;
begin
  Result := Count = Value.Count;
  if Result then
    for I := 0 to Count - 1 do begin
      Result := Items[I].IsEqual(Value.Items[I]);
      if not Result then Break;
    end
end;

function TSDParams.GetParam(Index: Word): TSDParam;
begin
  Result := ParamByName(TSDParam(FItems[Index]).Name);
end;

function TSDParams.ParamByName(const Value: string): TSDParam;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to FItems.Count - 1 do begin
    Result := FItems[I];
    if AnsiCompareText(Result.Name, Value) = 0 then Exit;
  end;
  DatabaseError( Format(SParameterNotFound, [Value]) );
end;

procedure TSDParams.DefineProperties(Filer: TFiler);

  function WriteData: Boolean;
  begin
    if Filer.Ancestor <> nil
    then Result := not IsEqual(TSDParams(Filer.Ancestor))
    else Result := Count > 0;
  end;

begin
  inherited DefineProperties(Filer);
  Filer.DefineBinaryProperty('Data', ReadBinaryData, WriteBinaryData, WriteData);
end;

procedure TSDParams.ReadBinaryData(Stream: TStream);
var
  I, Temp, NumItems: Integer;
  Buffer: array[0..2047] of Char;
  TempStr: string;
  Version: Word;
begin
  Clear;
  with Stream do begin
    ReadBuffer(Version, SizeOf(Version));
    if Version > 2 then DatabaseError(SInvalidVersion);
    NumItems := 0;
    if Version = 2
    then ReadBuffer(NumItems, SizeOf(NumItems))
    else ReadBuffer(NumItems, 2);
    for I := 0 to NumItems - 1 do
      with TSDParam.Create(Self, ptUnknown) do begin
        Temp := 0;
        if Version = 2
        then ReadBuffer(Temp, SizeOf(Temp))
        else ReadBuffer(Temp, 1);
        SetLength(TempStr, Temp);
        ReadBuffer(PChar(TempStr)^, Temp);
        Name := TempStr;
        ReadBuffer(FParamType, SizeOf(FParamType));
        ReadBuffer(FDataType, SizeOf(FDataType));
        if DataType <> ftUnknown then begin
          Temp := 0;
          if Version = 2
          then ReadBuffer(Temp, SizeOf(Temp))
          else ReadBuffer(Temp, 2);
          ReadBuffer(Buffer, Temp);
          if DataType in [ftBlob, ftGraphic..ftDBaseOLE]
          then SetBlobData(@Buffer, Temp)
          else SetData(@Buffer);
        end;
        ReadBuffer(FNull, SizeOf(FNull));
        ReadBuffer(FBound, SizeOf(FBound));
      end;
  end;
end;

procedure TSDParams.WriteBinaryData(Stream: TStream);
var
  I: Integer;
  Temp: SmallInt;
  Buffer: array[0..2047] of Char;
  Version: Word;
begin
  with Stream do begin
    Version := GetVersion;
    WriteBuffer(Version, SizeOf(Version));
    Temp := Count;
    WriteBuffer(Temp, SizeOf(Temp));
    for I := 0 to Count - 1 do
      with Items[I] do begin
        Temp := Length(FName);
        WriteBuffer(Temp, 1);
        WriteBuffer(PChar(FName)^, Length(FName));
        WriteBuffer(FParamType, SizeOf(FParamType));
        WriteBuffer(FDataType, SizeOf(FDataType));
        if (DataType <> ftUnknown) then begin
          if GetDataSize > SizeOf(Buffer) then
            DatabaseErrorFmt(SParamTooBig, [Name, SizeOf(Buffer)]);
          Temp := GetDataSize;
          GetData(@Buffer);
          WriteBuffer(Temp, SizeOf(Temp));
          WriteBuffer(Buffer, Temp);
        end;
        WriteBuffer(FNull, SizeOf(FNull));
        WriteBuffer(FBound, SizeOf(FBound));
      end;
  end;
end;

function TSDParams.GetVersion: Word;
begin
  Result := 1;
end;

function TSDParams.GetParamValue(const ParamName: string): Variant;
var
  I: Integer;
  Params: TList;
begin
  if Pos(';', ParamName) <> 0 then begin
    Params := TList.Create;
    try
      GetParamList(Params, ParamName);
      Result := VarArrayCreate([0, Params.Count - 1], varVariant);
      for I := 0 to Params.Count - 1 do
        Result[I] := TSDParam(Params[I]).Value;
    finally
      Params.Free;
    end;
  end else
    Result := ParamByName(ParamName).Value
end;

procedure TSDParams.SetParamValue(const ParamName: string; const Value: Variant);
var
  I: Integer;
  Params: TList;
begin
  if Pos(';', ParamName) <> 0 then begin
    Params := TList.Create;
    try
      GetParamList(Params, ParamName);
      for I := 0 to Params.Count - 1 do
        TSDParam(Params[I]).Value := Value[I];
    finally
      Params.Free;
    end;
  end else
    ParamByName(ParamName).Value := Value;
end;

procedure TSDParams.GetParamList(List: TList; const ParamNames: string);
var
  Pos: Integer;
begin
  Pos := 1;
  while Pos <= Length(ParamNames) do
    List.Add(ParamByName(ExtractFieldName(ParamNames, Pos)));
end;
{$ENDIF}


{*******************************************************************************
					TSDBlobStream
*******************************************************************************}
constructor TSDBlobStream.Create(Field: TBlobField; Mode: TBlobStreamMode);
begin
  FMode := Mode;
  FField := Field;
  FDataSet := FField.DataSet as TSDDataSet;
  FCached := FDataSet.FCacheBlobs;
  FFieldNo := FField.FieldNo;
  if not FDataSet.GetActiveRecBuf(FBuffer) then Exit;
  if FDataSet.State = dsFilter then
    DatabaseErrorFmt(SNoFieldAccess, [FField.DisplayName]);
  if not FField.Modified then
    if Mode = bmWrite then begin
      if FField.ReadOnly then
        DatabaseErrorFmt(SFieldReadOnly, [FField.DisplayName]);
      if not (FDataSet.State in [dsEdit, dsInsert]) then
        DatabaseError(SNotEditing);
    end;
  FOpened := True;
  FBlobSize := -1;
  if Mode = bmWrite then Truncate;
end;

destructor TSDBlobStream.Destroy;
begin
  if FOpened then
    if FModified then FField.Modified := True;
  if FModified then
  try
    FDataSet.DataEvent(deFieldChange, Longint(FField));
  except
    Application.HandleException(Self);
  end;
end;

function TSDBlobStream.GetBlobSize: Longint;
begin
  Result := 0;
  if FOpened then
    if FCached then begin
      ASSERT( FBuffer<>nil, Format(SFatalError, ['TSDBlobStream.GetBlobSize']) );

      FBlobSize := Length( FDataSet.GetBlobData(FField, FBuffer) );
      Result := FBlobSize;
    end else
      Result := FBlobSize;
end;

function TSDBlobStream.Read(var Buffer; Count: LongInt): Longint;
var
  sBlob: TBlobData;
begin
  Result := 0;
  if FOpened then begin
    if FCached then begin	// If cached field then data is copied from the record cache
      if Count > Size - FPosition
      then Result := Size - FPosition
      else Result := Count;
      if Result > 0 then begin
        sBlob := FDataSet.GetBlobData(FField, FBuffer);
        Move( PChar(sBlob)[FPosition], Buffer, Result );
        Inc(FPosition, Result);
      end;
    end else begin		// else data is received from the cache stream FBlobData
	// save in the record cache
      if FDataset.FCacheBlobs and
        (FMode = bmRead) and not FField.Modified and
        ((FPosition = FCacheSize) or (FPosition = 0))
      then begin
        FCacheSize := FBlobSize;
        Result := FBlobSize;
        if FCacheSize = Size then begin
          PBlobDataArray(FBuffer + FDataSet.FBlobCacheOffs)[FField.Offset] := FBlobData;
          FBlobData := '';
          FCached := True;
        end;
      end;
    end;
  end;
end;

function TSDBlobStream.Write(const Buffer; Count: Longint): Longint;
var
  sBlob: TBlobData;
begin
  if (FMode = bmWrite) and (not FCached) then begin
    Result := FDataSet.ISrvWriteBlob(FFieldNo, Buffer, Count);
  end else
    Result := Count;
	// get current blob value
  sBlob := FDataSet.GetBlobData(FField, FBuffer);
  	// set the corresponded buffer size
  SetLength(sBlob, FPosition + Count);
    	// move new data(Buffer) in current position(FPosition)
  Move( PChar(@Buffer)^, PChar(@sBlob[FPosition+1])^, Count );
  	// save new blob value
  FDataSet.SetBlobData(FField, FBuffer, sBlob);

  Inc(FPosition, Count);
  FModified := True;
end;

function TSDBlobStream.Seek(Offset: Longint; Origin: Word): Longint;
begin
  case Origin of
    0: FPosition := Offset;
    1: Inc(FPosition, Offset);
    2: FPosition := GetBlobSize + Offset;
  end;
  Result := FPosition;
end;

{ Sets in zero(begin) position. Data will be overwritten really in Write method }
procedure TSDBlobStream.Truncate;
begin
  if FOpened then begin
    FPosition := 0;
    FModified := True;

    FDataSet.SetBlobData(FField, FBuffer, '');
  end;
end;


(*******************************************************************************
				TSDStoredProc
*******************************************************************************)
constructor TSDStoredProc.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FParams := TSDHelperParams.Create {$IFDEF SD_VCL4} (Self) {$ENDIF};
end;

destructor TSDStoredProc.Destroy;
begin
  Destroying;
  Disconnect;
  FParams.Free;

  inherited Destroy;
end;

{$IFDEF SD_VCL4}
procedure TSDStoredProc.DefineProperties(Filer: TFiler);

  function WriteData: Boolean;
  begin
    if Filer.Ancestor <> nil
    then Result := not FParams.IsEqual(TSDStoredProc(Filer.Ancestor).FParams)
    else Result := FParams.Count > 0;
  end;

begin
  inherited DefineProperties(Filer);
  Filer.DefineProperty('ParamData', ReadParamData, WriteParamData, WriteData);
end;

procedure TSDStoredProc.WriteParamData(Writer: TWriter);
begin
  Writer.WriteCollection(Params);
end;

procedure TSDStoredProc.ReadParamData(Reader: TReader);
begin
  Reader.ReadValue;
  Reader.ReadCollection(Params);
end;
{$ENDIF}

procedure TSDStoredProc.Disconnect;
begin
  Close;
  UnPrepare;
  DestroyHandle;
end;

{ Size of parameter's buffer for binding.
 NOTE: for ftString returns 255 always for placing of output parameters }
function TSDStoredProc.BindBufferSize(Param: TSDHelperParam): Integer;
begin
  with Param do
    if DataType in [ftBlob..ftDBaseOLE] then
      Result := 0		//SizeOf(BlobParamDesc)
    else if FDatabase.ISrvRequiredCnvtFieldType(DataType) then
      Result := FDatabase.ISrvNativeDataSize(DataType)
    else begin
      Result := GetDataSize;
	// it's necessary for output and result parameters      
      if (DataType in [ftBytes, ftVarBytes, ftString]) and (Result < (MinStrParamSize+1)) then
        Result := MinStrParamSize+1;		// with zero-terminator
    end;
end;

procedure TSDStoredProc.ExecProc;
begin
  CheckInActive;
  SetBusyState;
  SetDSFlag(dsfExecProc, True);
  SetConnectionState( True );
  try
    SetPrepare(True);

    ISrvExecProc;
  finally
    SetConnectionState( False );
    SetDSFlag(dsfExecProc, False);
    ResetBusyState;
  end;
end;

procedure TSDStoredProc.GetResults;
begin
  ISrvGetResults;
end;

function TSDStoredProc.NextResultSet: Boolean;
begin
  Result := ISrvNextResultSet;
end;

procedure TSDStoredProc.SetProcName(const Value: string);
begin
  if not (csReading in ComponentState) then begin
    CheckInactive;
    if Value <> FProcName then begin
      FProcName := Value;
      UnPrepare;
      ISrvFreeBindBuffer;
      FParams.Clear;
    end;
  end else
    FProcName := Value;
end;

procedure TSDStoredProc.SetOverLoad(Value: Word);
begin
  if not (csReading in ComponentState) then begin
    CheckInactive;
    if Value <> OverLoad then begin
      FOverLoad := Value;
      UnPrepare;      
      ISrvFreeBindBuffer;
      FParams.Clear;
    end
  end else
    FOverLoad := Value;
end;

function TSDStoredProc.GetParamsCount: Word;
begin
  Result := FParams.Count;
end;

procedure TSDStoredProc.CreateParamDesc;
begin
  PrepareProc;
end;

function TSDStoredProc.DescriptionsAvailable: Boolean;
begin
  SetDSFlag(dsfProcDesc, True);
  try
    Result := ISrvDescriptionsAvailable;
  finally
    SetDSFlag(dsfProcDesc, False);
  end;
end;

procedure TSDStoredProc.InternalOpen;
begin
  BindParams;
  
  inherited;
end;

procedure TSDStoredProc.BindParams;
var
  i: Integer;
begin
  for i:=0 to ParamCount-1 do
    if Params[i].ParamType = ptOutput then
      Params[i].Clear;

  ISrvBindParams;
end;

procedure TSDStoredProc.PrepareCursor;
begin
  try
    if FSrvDataSet = nil then
      FSrvDataSet := CreateHandle;
    if not Prepared then
      PrepareProc; 	// compile and get parameters(or vice versa) of stored procedure
    if not Assigned(SrvDataSet.BindBuffer) then
      ISrvAllocBindBuffer;
  except
    ISrvFreeBindBuffer;
    raise;
  end
end;

procedure TSDStoredProc.UnPrepareCursor;
begin
  ISrvFreeBindBuffer;
  if DefaultFields then DestroyFields;
  ISrvFreeSelectBuffer;
end;

procedure TSDStoredProc.PrepareProc;
begin
  ISrvPrepareProc;
end;

procedure TSDStoredProc.SetPrepare(Value: Boolean);
begin
  if Active then DatabaseError(SDataSetOpen);
  if (Prepared <> Value) or
     (Prepared and not Assigned(FSrvDataSet))	// FSrvDataSet(cursor) was destroyed after Commit/Rollback or Detach
  then begin
    if Value
    then PrepareCursor
    else UnPrepareCursor;
    FPrepared := Value;
  end;
end;

procedure TSDStoredProc.Prepare;
begin
  SetDSFlag(dsfStoredProc, True);
  SetPrepare(True);
end;

procedure TSDStoredProc.UnPrepare;
begin
  SetPrepare(False);
  SetDSFlag(dsfStoredProc, False);
end;

procedure TSDStoredProc.SetPrepared(Value: Boolean);
begin
  if Value
  then Prepare
  else UnPrepare;
end;

function TSDStoredProc.SetDSFlag(Flag: Integer; Value: Boolean): Boolean;
begin
  if not Value and (DSFlags - [Flag] = []) then
    SetPrepare(False);
  Result := inherited SetDSFlag(Flag, Value);
end;

procedure TSDStoredProc.CopyParams(Value: TSDHelperParams);
begin
  if not Prepared and (FParams.Count = 0) then
    try
      FQueryMode := True;
      Prepare;
      Value.Assign(FParams);
    finally
      UnPrepare;
      FQueryMode := False;
    end
  else
    Value.Assign(FParams);
end;

procedure TSDStoredProc.SetParamsList(Value: TSDHelperParams);
begin
  CheckInactive;
  if Prepared then begin
    SetPrepare(False);
    FParams.Assign(Value);
    SetPrepare(True);
  end else
    FParams.Assign(Value);
end;

function TSDStoredProc.ParamByName(const Value: string): TSDHelperParam;
begin
  Result := FParams.ParamByName(Value);
end;

{$IFDEF SD_VCL5}

{ TSDStoredProc.IProviderSupport }

function TSDStoredProc.PSGetParams: TParams;
begin
  Result := Params;
end;

procedure TSDStoredProc.PSSetParams(AParams: TParams);
begin
  if AParams.Count > 0 then
    Params.Assign(AParams);
  Close;
end;

function TSDStoredProc.PSGetTableName: string;
begin
  Result := inherited PSGetTableName;
end;

procedure TSDStoredProc.PSExecute;
begin
  ExecProc;
end;

procedure TSDStoredProc.PSSetCommandText(const CommandText: string);
begin
  if CommandText <> '' then
    StoredProcName := CommandText;
end;
{$ENDIF}

{-------------------------------------------------------------------------------
			Common SQL server C/API Functions
-------------------------------------------------------------------------------}
procedure TSDStoredProc.ISrvAllocBindBuffer;
begin
  FSrvDataSet.AllocBindBuffer;
end;

procedure TSDStoredProc.ISrvFreeBindBuffer;
begin
  if Assigned(FSrvDataSet) then
    FSrvDataSet.FreeBindBuffer;
end;

procedure TSDStoredProc.ISrvBindParams;
begin
  FSrvDataSet.SpBindParams;
end;

function TSDStoredProc.ISrvDescriptionsAvailable: Boolean;
var
  db: TSDDatabase;
begin
	// Can not use FSrvDataSet, because FSrvDataSet=nil, when Prepared=False;
  if FDatabase <> nil then
    db := FDatabase
  else
    db := Session.OpenDatabase(DatabaseName);
  if not db.Connected then
    db.Open;
  ASSERT( db.FSrvDatabase<>nil, 'TSDStoredProc.ISrvDescriptionsAvailable: FSrvDatabase=nil' );
  Result := db.FSrvDatabase.SPDescriptionsAvailable;
end;

function TSDStoredProc.ISrvNextResultSet: Boolean;
begin
  Result := False;
  if Assigned(FSrvDataSet) then
    Result := FSrvDataSet.SpNextResultSet;
end;

procedure TSDStoredProc.ISrvPrepareProc;
begin
  FSrvDataSet.SpPrepareProc;
end;

procedure TSDStoredProc.ISrvExecProc;
begin
  FSrvDataSet.SpExecProc;
end;

procedure TSDStoredProc.ISrvGetResults;
begin
  if Assigned(FSrvDataSet) then
    FSrvDataSet.SpGetResults;
end;


{ TISrvDatabase }
constructor TISrvDatabase.Create(ADatabase: TSDDatabase);
begin
  inherited Create;

  FDatabase := ADatabase;

  FCursorPreservedOnCommit	:= True;
  FCursorPreservedOnRollback	:= True;

	// by default AutoCommit = False
  FAutoCommitDef:= Trim( ADatabase.Params.Values[szAUTOCOMMIT] ) = '';
  FAutoCommit 	:= AnsiUpperCase( Trim( ADatabase.Params.Values[szAUTOCOMMIT] ) ) = STrueString;

  FMaxFieldNameLen	:= StrToIntDef( Trim( ADatabase.Params.Values[szMAXFIELDNAMELEN] ), SDEngine.MAXFIELDNAMELEN );
end;

class function TISrvDatabase.CreateConnected(ADatabase: TSDDatabase; const ADatabaseName, AUserName, APassword: string): TISrvDatabase;
var
  ISrvDatabaseClass: TISrvDatabaseClass;
  isrvdb: TISrvDatabase;
begin
  ASSERT( ADatabase <> nil );

  ISrvDatabaseClass := SrvDatabaseClasses[ADatabase.ServerType];
  if ISrvDatabaseClass = nil then
    DatabaseError( SNoServerType );

  isrvdb := ISrvDatabaseClass.Create( ADatabase );
  isrvdb.Logon( ADatabaseName, AUserName, APassword );

  Result := isrvdb;
end;

class function TISrvDatabase.CreateWithHandle(ADatabase: TSDDatabase; const AHandle: PSDCursor): TISrvDatabase;
var
  ISrvDatabaseClass: TISrvDatabaseClass;
  isrvdb: TISrvDatabase;
begin
  ASSERT( ADatabase <> nil );

  ISrvDatabaseClass := SrvDatabaseClasses[ADatabase.ServerType];
  if ISrvDatabaseClass = nil then
    DatabaseError( SNoServerType );

  isrvdb := ISrvDatabaseClass.Create( ADatabase );
  isrvdb.Handle := AHandle;

  Result := isrvdb;
end;

function TISrvDatabase.GetAcquiredHandle: Boolean;
begin
  Result := FDatabase.AcquiredHandle;
end;

{ It's necessary to call after the each network trip/database API call, except an error processing.
It marks that a database connection still active }
procedure TISrvDatabase.ResetIdleTimeOut;
begin
  FDatabase.FIdleTimeOutStarted := False;
end;

{ FetchAll for all active datasets }
procedure TISrvDatabase.FetchAllDataSets;
var
  i: Integer;
begin
  for i:=0 to Database.DataSetCount - 1 do begin
    if Database.DataSets[i].Active then
      Database.DataSets[i].FetchAll;
  end;
end;

{ There are dummy methods }

function TISrvDatabase.FieldDataType(ExtDataType: Integer): TFieldType;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.FieldDataType']) );
  Result := ftUnknown;
end;

function TISrvDatabase.FieldSubType(ExtDataType: Integer): Word;
begin
  Result := fldstUNKNOWN;
end;

function TISrvDatabase.GetHandle: PSDHandleRec;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.GetHandle']) );
  Result := nil;
end;

{ procedure must load API library, if it isn't loaded }
procedure TISrvDatabase.SetHandle(AHandle: PSDHandleRec);
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.SetHandle']) );
end;

procedure TISrvDatabase.GetStoredProcNames(List: TStrings);
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.GetStoredProcNames']) );
end;

function TISrvDatabase.SPDescriptionsAvailable: Boolean;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.SPDescriptionsAvailable']) );
  Result := False;
end;

procedure TISrvDatabase.GetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings);
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.GetTableNames']) );
end;

{ It's used in UpdateSQL Editor }
procedure TISrvDatabase.GetTableFieldNames(const TableName: string; List: TStrings);
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.GetTableFieldNames']) );
end;

procedure TISrvDatabase.Logon(const sDatabaseName, sUserName, sPassword: string);
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.Logon']) );
end;

procedure TISrvDatabase.Logoff;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.Logoff']) );
end;

function TISrvDatabase.NativeDataSize(FieldType: TFieldType): Word;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.NativeDataSize']) );
  Result := 0;
end;

function TISrvDatabase.NativeDataType(FieldType: TFieldType): Integer;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.NativeDataType']) );
  Result := 0;
end;

function TISrvDatabase.ParamValue(Value: TSDDatabaseParam): Integer;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.ParamValue']) );
  Result := -1;
end;

function TISrvDatabase.RequiredCnvtFieldType(FieldType: TFieldType): Boolean;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.RequiredCnvtFieldType']) );
  Result := False;
end;

procedure TISrvDatabase.SetDefaultParams;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.SetDefaultParams']) );
end;

procedure TISrvDatabase.SetTransIsolation(Value: TSDTransIsolation);
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.SetTransIsolation']) );
end;

{ set off autocommit when it's necessary: explicit transaction }
procedure TISrvDatabase.StartTransaction;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.StartTransaction']) );
end;

{ set on autocommit when it's necessary: explicit transaction is finished }
procedure TISrvDatabase.Commit;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.Commit']) );
end;

procedure TISrvDatabase.Rollback;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDatabase.Rollback']) );
end;

{ TISrvDataSet }
constructor TISrvDataSet.Create(ADataSet: TSDDataSet);
begin
  inherited Create;

  FDataSet := ADataSet;
  FBindBuffer	:= nil;
  FSelectBuffer := nil;

  FPreservedOnCommit	:= SrvDatabase.CursorPreservedOnCommit;
  FPreservedOnRollback	:= SrvDatabase.CursorPreservedOnRollback;
end;

destructor TISrvDataSet.Destroy;
begin
  if FBindBuffer <> nil then
    FreeBindBuffer;

  if FSelectBuffer <> nil then
    FreeSelectBuffer;

  inherited;
end;

function TISrvDataSet.BindBufferSize: Integer;
var
  i, NumBytes: Integer;
begin
  NumBytes := 0;
  if DataSet is TSDQuery then begin
    for i := 0 to Query.Params.Count - 1 do
      with Query.Params[I] do begin
        if DataType = ftUnknown then
          DatabaseErrorFmt( SNoParameterDataType, [Name] );
        if ParamType = ptUnknown then
          DatabaseErrorFmt( SNoParameterType, [Name] );
        Inc(NumBytes, SizeOf(TFieldInfo) + QParamBindSize(Query.Params[I]));
      end;
  end else if DataSet is TSDStoredProc then begin
    for i := 0 to StoredProc.Params.Count - 1 do
      with StoredProc.Params[I] do begin
        if DataType = ftUnknown then
          DatabaseErrorFmt( SNoParameterDataType, [Name] );
        if ParamType = ptUnknown then
          DatabaseErrorFmt( SNoParameterType, [Name] );
        Inc(NumBytes, SizeOf(TFieldInfo) + SpParamBindSize(StoredProc.Params[I]));
      end;
  end else
    raise Exception.Create( Format(SFatalError, ['BindBufferSize']) );
  Result := NumBytes;
end;

{ Allocates memory for bind(parameter) buffer }
procedure TISrvDataSet.AllocBindBuffer;
var
  nSize: Integer;
begin
  if FBindBuffer <> nil then
    FreeBindBuffer;

  nSize := BindBufferSize;

  	// allocate memory for bind(parameter) buffer
  FBindBuffer := StrAlloc(nSize);
  FillChar(FBindBuffer^, nSize, 0);
end;

procedure TISrvDataSet.FreeBindBuffer;
begin
  if FBindBuffer <> nil then begin
    StrDispose(FBindBuffer);
    FBindBuffer := nil;
  end;
end;

{ Calculates a size of select buffer }
function TISrvDataSet.SelectBufferSize: Integer;
var
  i: Integer;
begin
  Result := 0;
  with FDataSet do begin
    for i:=0 to FieldCount-1 do begin
      if Fields[i].FieldKind = fkCalculated then
        Continue;
      Inc( Result , SizeOf(TFieldInfo) + Self.NativeFieldDataSize(Fields[i]) );
    end;
    Self.FBlobCacheOffs := Result;

    Inc( Result, Self.BlobFieldCount * SizeOf(Pointer) );
  end;
end;

{ Allocates memory for data, which selects from a database }
procedure TISrvDataSet.AllocSelectBuffer;
var
  nSize: Integer;
begin
  if FSelectBuffer <> nil then
    FreeSelectBuffer;

  nSize := SelectBufferSize;

  	// allocate memory for select buffer
  FSelectBuffer := StrAlloc(nSize);
  FillChar(FSelectBuffer^, nSize, 0);
end;

{ Record conversion from internal database format to program format }
procedure TISrvDataSet.ConvertSelectBuffer(Buffer: PChar);
var
  nSelOffset, nFldOffset, i: Integer;
  NativeDataSize: Integer;
  TmpField: TField;
begin
  nSelOffset := 0;	// offset on TFieldInfo structure in internal buffer(selected data)
  nFldOffset := 0;	// offset on TFieldInfo structure in program buffer(converted data)
	// copy data from internal(database) to program(required) format
  for i:=1 to DataSet.FieldDefs.Count do begin
    TmpField := FieldByNumber(i);
    if not Assigned(TmpField) then Continue;
    with TmpField do begin
    	// omit calculated field
      if (FieldKind = fkCalculated) then
        Continue;
	// maximum data size in internal database format
      NativeDataSize := NativeFieldDataSize(TmpField);

      ASSERT( SrvDatabase<>nil );
    	// convert TFieldInfo structure and data(if it's necessary)
      CnvtDBField2Field(TmpField, FSelectBuffer + nSelOffset, Buffer + nFldOffset);

      Inc( nSelOffset, SizeOf(TFieldInfo) + NativeDataSize );
      Inc( nFldOffset, SizeOf(TFieldInfo) + Self.DataSet.GetFieldDataSize(TmpField) );
    end;
  end;
  MoveBlobFields(Buffer);
end;

{ Move blob data from select buffer to cache }
procedure TISrvDataSet.MoveBlobFields(Buffer: PChar);
var
  i: Integer;
  Src, Dst, FldBuf: PChar;
  sBlobData: string;
begin
  Src := FSelectBuffer;
  Dst := Buffer;

  if BlobFieldCount > 0 then with DataSet do
    for i:=0 to FieldCount-1 do
      if Fields[i].IsBlob then begin
        sBlobData := PBlobDataArray(Src + Self.FBlobCacheOffs)[Fields[i].Offset];
        PBlobDataArray(Dst + DataSet.FBlobCacheOffs)[Fields[i].Offset] := sBlobData;
	// free a reference to blob data in the select buffer
        PBlobDataArray(Src + Self.FBlobCacheOffs)[Fields[i].Offset] := '';

        FldBuf := DataSet.GetFieldBuffer(Fields[i], Buffer);
        DataSet.FieldSetNull( FldBuf, Length(sBlobData) <= 0 );
      end;
end;

{ Gets data of blob fields to select buffer }
procedure TISrvDataSet.FetchBlobFields;
var
  i, BlobSize: Integer;
  sBlobData: string;
begin
  ASSERT( FDataSet <> nil );
  with DataSet do begin
    for i:=0 to DataSet.FieldCount-1 do
      if Fields[i].IsBlob then begin
        try
          BlobSize := ReadBlob(Fields[i].FieldNo, sBlobData);
          if BlobSize > 0 then begin
            PBlobDataArray(FSelectBuffer + Self.FBlobCacheOffs)[Fields[i].Offset] := sBlobData;
          end else
            PBlobDataArray(FSelectBuffer + Self.FBlobCacheOffs)[Fields[i].Offset] := '';
        finally
          sBlobData := '';	// it's need in case of exceptions in ReadBlob call
        end;
      end;
  end;
end;

{ Frees the select buffer }
procedure TISrvDataSet.FreeSelectBuffer;

begin
  if FSelectBuffer <> nil then begin
    StrDispose(FSelectBuffer);
    FSelectBuffer := nil;
  end;
end;

function TISrvDataSet.GetBlobFieldCount: Integer;
begin
  Result := FDataSet.BlobFieldCount;
end;

function TISrvDataSet.GetDataSetComponentState: TComponentState;
begin
  Result := FDataSet.ComponentState;
end;

function TISrvDataSet.GetDSFlags: TDSFlags;
begin
  Result := FDataSet.DSFlags;
end;

function TISrvDataSet.GetFieldBufOffs(Index: Integer): Integer;
begin
  Result := FDataSet.FieldBufOffs[Index];
end;

function TISrvDataSet.GetForceClosing: Boolean;
begin
  if Assigned(FDataSet) then
    Result := FDataSet.FForceClosing
  else
    Result := False;
end;

function TISrvDataSet.GetResultSet: TSDResultSet;
begin
  Result := FDataSet.FRecCache;
end;

function TISrvDataSet.GetSrvDatabase: TISrvDatabase;
begin
  ASSERT( Assigned(FDataSet) );
  ASSERT( Assigned(FDataSet.FDatabase) );

  Result := FDataSet.FDatabase.FSrvDatabase;
end;

function TISrvDataSet.GetStoredProc: TSDStoredProc;
begin
  Result := (FDataSet as TSDStoredProc);
end;

function TISrvDataSet.GetQuery: TSDQuery;
begin
  Result := (FDataSet as TSDQuery);
end;

procedure TISrvDataSet.InitResultSet;
begin
  DataSet.InitResultSet;
end;

function TISrvDataSet.FieldByNumber(FieldNo: Integer): TField;
begin
  Result := DataSet.FieldByNumber(FieldNo);
end;

function TISrvDataSet.FieldIsNull(FieldBuf: Pointer): Boolean;
begin
  Result := DataSet.FieldIsNull(FieldBuf);
end;

function TISrvDataSet.GetFieldBuffer(Field: TField; RecBuf: Pointer): Pointer;
begin
  Result := DataSet.GetFieldBuffer(Field, RecBuf);
end;

function TISrvDataSet.NativeFieldDataSize(Field: TField): Word;
begin
  Result := DataSet.NativeFieldDataSize(Field);
end;

function TISrvDataSet.SetDSFlag(Flag: Integer; Value: Boolean): Boolean;
begin
  Result := DataSet.SetDSFlag(Flag, Value);
end;

procedure TISrvDataSet.SetPreservation(Value: Boolean);
begin
end;

function TISrvDataSet.FieldDescByName(const AFieldName: string): TSDFieldDesc;
begin
  Result := DataSet.FFieldDescs.FieldDescByName( AFieldName );
end;

// It's dummy methods

procedure TISrvDataSet.CloseResultSet;
begin
end;

function TISrvDataSet.CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.CnvtDBField2Field']) );
  Result := False;
end;

function TISrvDataSet.CnvtDateTime2DBDateTime(ADataType: TFieldType; Value: TDateTime; Buffer: Pointer; BufSize: Integer): Integer;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.CnvtDateTime2DBDateTime']) );
  Result := 0;
end;

procedure TISrvDataSet.Disconnect;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.Disconnect']) );
end;

(*
	Processes theSelect and returns field info in FldInfo
For example: FldInfo.Strings[i] = "FieldName=[Owner.]Table.Column",
             TblInfo.Strings[i] = "Alias=TableName"
where
	FieldName (column heading) is equal Field[i].FieldName,
        Column - a really column name in table with name <Table>

Syntax:
SELECT [ALL|DISTINCT|field_list]
	[FROM table_name[,table_name2]]
	[WHERE ...]
	[GROUP BY ...]
	[ORDER BY ...]
where
	field_list	= *|field[, field]
        	field	= column_name | column_name AS heading | heading=column_name | expression
        table_name	= simple_table_name | derived_table | joined_table
        	simple_table_name = [[database.]owner.]{table|view}[[AS]alias_name]
                derived_table	= (select_statement) [AS] alias_name
                joined_table	= {table1 CROSS JOIN table2 | table1 [join_type] JOIN table2 ON search_conditions}
                	join_type = {INNER | LEFT [OUTER] | RIGHT [OUTER] | FULL [OUTER]}

*)
procedure TISrvDataSet.ParseSQLText(const theSelect: string; FldInfo, TblInfo: TStrings);
const
  Literals	= ['''', '"', '`'];
  ItemDelimiters= [','];
  NameDelimiters= [' ', ',', ';', ')', #$00, #$0A, #$0D];

  kwSelect 	= 1;
  kwAll		= 2;
  kwDistinct	= 3;
  kwFrom	= 4;
  KeyWords: array[1..9] of string = (
  		'SELECT', 'ALL', 'DISTINCT', 'FROM',
                'WHERE', 'ORDER', 'GROUP', 'HAVING', 'UNION');
  jwJoin	= 1;
  jwOn		= 2;
  JoinWords: array[1..7] of string = (
  		'JOIN', 'ON', 'LEFT', 'RIGHT',
                'INNER', 'OUTER', 'FULL');

  function IsKeyWord(const S: string): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i:=Low(KeyWords) to High(KeyWords) do
      if AnsiUpperCase(S) = AnsiUpperCase(KeyWords[i]) then begin
        Result := True;
        Break;
      end;
  end;

  function IsJoinWord(const S: string): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i:=Low(JoinWords) to High(JoinWords) do
      if AnsiUpperCase(S) = AnsiUpperCase(JoinWords[i]) then begin
        Result := True;
        Break;
      end;
  end;

  procedure SkipBlanks( Stmt: string; var i: Integer );
  begin
    while (i <= Length(Stmt)) and (Stmt[i] in NameDelimiters) do
      Inc(i);
  end;

  function StripLiterals(Str: string): string;
  var
    Len: Word;
  begin
    Result := Str;
    Len := Length(Str);
    if (Len > 0) and (Str[1] in Literals) then
      Result := Copy( Str, 2, Len-1 );
    Len := Length(Result);
    if Result[Len] in Literals then
      Result := Copy( Result, 1, Len-1 );
  end;

  function GetCurWord( Stmt: string; var i: Integer ): string;
  var
    StartPos: Integer;
    Quoted: Boolean;
  begin
    StartPos := i;
    Quoted := False;
        	// while not NameDelimiter
    while i <= Length(Stmt) do begin
      if not Quoted and (Stmt[i] in NameDelimiters) then
        Break;
      Quoted := Quoted xor (Stmt[i] in Literals);
      Inc(i);
    end;
    Result := Copy(Stmt, StartPos, i-StartPos);
  end;
	// Cols.Strings[i] must be: Heading=[Alias.]ColumnName (as since Heading is unique in query)
  procedure ParseFieldItem( FieldItem: string; Cols: TStrings );
  const
    SepEq	= '=';
    SepAS	= 'AS';
    SepSpace	= ' ';
  var
    i, n: Integer;
    sHeading, sColName, sUniqueName: string;
  begin
    if FieldItem = '*' then begin
      with DataSet do
        for i:=0 to FieldDefs.Count-1 do
          Cols.Values[FieldDefs[i].Name] := FieldDefs[i].Name;
    end else begin
      i := 1;
      while (i <= Length(FieldItem)) and
      	    not(FieldItem[i] in NameDelimiters + [SepEq]) do
        Inc(i);
	// may be column name is first
      sColName := Copy( FieldItem, 1, i-1 );
      if i > Length(FieldItem) then
        sHeading := sColName
      else begin
        SkipBlanks( FieldItem, i );
        if FieldItem[i] = SepEq then begin		// Heading = ColumnName
          Inc(i);
          SkipBlanks( FieldItem, i );
          sHeading := sColName;
          sColName := GetCurWord( FieldItem, i );
        end else begin
          sHeading := GetCurWord( FieldItem, i );
          SkipBlanks( FieldItem, i );
          if UpperCase(sHeading) = SepAS then		// if   ColumnName AS Heading
            sHeading := GetCurWord( FieldItem, i );    	// else ColumnName Heading
        end;
      end;
	// if sHeading is equal sColName (for example T1.COL1), then sHeading must be equal COL1
      i := Length(sHeading);
      while (i > 0) and (sHeading[i] <> '.') do Dec(i);
      if i > 0 then
        sHeading := Copy(sHeading, i+1, Length(sHeading)-i);
      sHeading := StripLiterals(sHeading);
	// if the column heading(sHeading) exists in Cols list
      n := 0;
      sUniqueName := sHeading;
      while Cols.IndexOfName( sUniqueName ) >= 0 do begin
        Inc( n );
        sUniqueName := Format( '%s_%d', [sHeading, n] );
      end;
      	// if sHeading <> sUniqueName
      if n > 0 then
        sHeading := sUniqueName;

      Cols.Values[sHeading] := sColName;
    end;
  end;

  function ParseSelectList( Stmt: string; var i: Integer; Cols: TStrings ): string;
  var
    sWord, CurSentence: string;
  begin
    CurSentence := '';

    while i <= Length(Stmt) do begin
      SkipBlanks( Stmt, i );
      sWord := GetCurWord( Stmt, i );
      if Length(sWord) <= 0 then
        Break;
      if IsKeyWord(sWord) then begin
        if (UpperCase(sWord) <> KeyWords[kwAll]) and
           (UpperCase(sWord) <> KeyWords[kwDistinct])
        then
          Break;
      end else begin
        if Length(CurSentence) > 0 then
          CurSentence := CurSentence + ' ';
        CurSentence := CurSentence + sWord;

        if Stmt[i] in ItemDelimiters then begin
          ParseFieldItem( CurSentence, Cols );
          CurSentence := '';
        end;

      end;
    end;
    if Length(CurSentence) > 0 then
      ParseFieldItem( CurSentence, Cols );
    if Length(sWord) > 0 then
      Result := sWord;		// return the last passed word
  end;
	// stored in ATblList as the following: AliasName=TableName (as since AliasName is unique in query)
  procedure ParseTableItem(const AItem: string; ATblList: TStrings);
  var
    i: Integer;
    tbl, alias: string;
  begin
    i := 1;
    	// untill not blank or delimiter
    while (i <= Length(AItem)) and not(AItem[i] in NameDelimiters) do Inc(i);
    tbl := Copy(AItem, 1, i-1);

    SkipBlanks( AItem, i );
    if i > Length(AItem)
    then alias	:= tbl
    else alias := Copy(AItem, i, Length(AItem)-i+1);
    ATblList.Values[AnsiUpperCase(alias)] := tbl;
  end;

  function ParseFromClause( Stmt: string; var i: Integer; Tbls: TStrings ): string;
  var
    sWord, CurSentence: string;
    bJoinON: Boolean;
  begin
    CurSentence := '';
    bJoinON	:= False;

    while i <= Length(Stmt) do begin
      SkipBlanks( Stmt, i );
      sWord := GetCurWord( Stmt, i );
      if Length(sWord) <= 0 then
        Break;
      if IsKeyWord(sWord) then
        Break
      else begin
        if not IsJoinWord(sWord) then begin
          if Length(CurSentence) > 0 then
            CurSentence := CurSentence + ' ';
          CurSentence := CurSentence + sWord;
        end;

        if (Stmt[i] in ItemDelimiters) or IsJoinWord(sWord) then begin
          if Length(CurSentence) > 0 then
            if not bJoinON then
              ParseTableItem( CurSentence, Tbls );
          CurSentence := '';
        	// if search conditions(ON) in JOIN operator, then set flag
          bJoinON := IsJoinWord(sWord) and (UpperCase(sWord) = JoinWords[jwOn]);
        end;

      end;
    end;
    if Length(CurSentence) > 0 then
      if not bJoinON then
        ParseTableItem( CurSentence, Tbls );
    if Length(sWord) > 0 then
      Result := sWord;	// return the last passed word
  end;
	// Heading=[Alias.]ColumnName + Alias=TableName -> TableName.ColumnName
  procedure ProcessColumnInfo(AColList, ATblList, AColInfo: TStrings);
  var
    i, j, ColPos, SepPos: Integer;
    sColName, sAlias, sTblName: string;
  begin
    AColInfo.Clear;

    with DataSet do
      for i:=0 to FieldDefs.Count-1 do begin
        ColPos := AColList.IndexOfName(FieldDefs[i].Name);
        if ColPos < 0 then Continue;
	// returns [Alias.]ColName from "Heading=[Alias.]ColumnName"
        sColName := AColList.Values[AColList.Names[ColPos]];
        j := Length(sColName);
        while (j > 0) and (sColName[j] <> '.') do
          Dec(j);
        SepPos := j;
	// if sColName = ColumnName (w/o Alias), else sColName = Alias.ColumnName
        if SepPos = 0 then begin
          if ATblList.Count > 0
          then sTblName := ATblList.Values[ ATblList.Names[0] ]
          else sTblName := '';
        end else begin
        	// extract Alias from "Alias.ColumnName"
          sAlias := Copy(sColName, 1, SepPos-1);
        	// extract ColName from "Alias.ColumnName"
          sColName:= Copy(sColName, SepPos+1, Length(sColName)-SepPos);

          j := ATblList.IndexOfName(sAlias);
		// if alias founded
          if j >= 0 then
            sTblName := ATblList.Values[ ATblList.Names[j] ]
          else
            sTblName := sAlias;
        end;
	// saves as FieldName=Table.Column
        AColInfo.Values[FieldDefs[i].Name] := Format('%s.%s', [sTblName, sColName]);
      end;
  end;

var
  CurPos: Integer;
  CurWord: string;
  ColList, TblList: TStrings;

begin
  ASSERT( DataSet <> nil );

  FldInfo.Clear;
  TblInfo.Clear;

  if DataSet.FieldDefs.Count = 0 then
    DataSet.FieldDefs.Update;

  ColList := TStringList.Create;
  TblList := TStringList.Create;

  try
    CurPos := 1;
    CurWord := '';

    while (CurPos <= Length(theSelect)) do begin
      CurWord := GetCurWord( theSelect, CurPos );
      if (Length(CurWord) <= 0) then begin
        Inc(CurPos);	// move to the following symbol, if the current one is a delimiter
        Continue;
      end;
      if UpperCase(CurWord) = KeyWords[kwSelect] then
	// stored in column info in string list as: Heading=[Alias.]ColumnName
        CurWord := ParseSelectList( theSelect, CurPos, ColList );
      if UpperCase(CurWord) = KeyWords[kwFrom] then
	// stored in table info in string list as: TableName=AliasName
        CurWord := ParseFromClause( theSelect, CurPos, TblList );
    end;

	// Fields[i].FieldName = Heading -> [Alias.]ColName -> TableName = TableName.ColName
    ProcessColumnInfo( ColList, TblList, FldInfo );

    TblInfo.Assign( TblList );
  finally
    ColList.Free;
    TblList.Free;
  end;
end;

procedure TISrvDataSet.ParseFieldNames(const theSelect: string; FldInfo: TStrings);
var
  TblInfo: TStrings;
begin
  TblInfo := TStringList.Create;
  try
    ParseSQLText( theSelect, FldInfo, TblInfo );
  finally
    TblInfo.Free;
  end;
end;

procedure TISrvDataSet.InitFieldDefs;
var
  TmpFieldDefs: TFieldDefs;
{$IFDEF SD_VCL4}  fd: TFieldDef; {$ENDIF}
  Descs: TSDFieldDescList;
  i, n: Integer;
  sFieldName, sName: string;
begin
  Descs := TSDFieldDescList.Create;
  TmpFieldDefs := TFieldDefs.Create( DataSet );
  try
    GetFieldDescs( Descs );

    for i:=0 to Descs.Count-1 do begin
      sFieldName := Descs.FieldDescs[i].FieldName;
      if Length(sFieldName) = 0 then
        sFieldName := Format('COLUMN%d', [i+1]);

      n := 0;
      sName := sFieldName;
      while TmpFieldDefs.IndexOf( sName ) >= 0 do begin
        Inc( n );
        sName := Format( '%s_%d', [sFieldName, n] );
      end;
	// in case of duplicate field name store the changed name in FieldDesc structure too
      if AnsiCompareText(sName, sFieldName) <> 0 then
        sFieldName := sName;
      PSDFieldDesc(Descs.Items[i])^.FieldName := sFieldName;

      with Descs.FieldDescs[i] do begin
{$IFDEF SD_VCL4}
        if IsRequiredSizeTypes( DataType )
        then fd := TFieldDef.Create(TmpFieldDefs, sName, DataType, Size, Required, FieldNo)
        else fd := TFieldDef.Create(TmpFieldDefs, sName, DataType, 0, Required, FieldNo);

        if DataType in [ftBCD, ftFloat] then
          fd.Precision := Precision;
{$ELSE}
        if IsRequiredSizeTypes( DataType )
        then TmpFieldDefs.Add( sName, DataType, Size, Required )
        else TmpFieldDefs.Add( sName, DataType, 0, Required );

        if DataType in [ftBCD, ftFloat] then
          TmpFieldDefs.Items[TmpFieldDefs.Count-1].Precision := Precision;
{$ENDIF}
      end;

    end;

    DataSet.FieldDefs := TmpFieldDefs;
  finally
  	// it's need here to exclude a memory leak when GetFieldDescs( Descs ) raises an exception
    DataSet.FFieldDescs.Free;
    DataSet.FFieldDescs := Descs;

    TmpFieldDefs.Free;
  end;
end;

procedure TISrvDataSet.GetFieldDescs(Descs: TSDFieldDescList);
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.GetFieldDescs']) );
end;

procedure TISrvDataSet.Execute;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.Execute']) );
end;

function TISrvDataSet.FetchNextRow: Boolean;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.FetchNextRow']) );
  Result := False;
end;

function TISrvDataSet.GetHandle: PSDCursor;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.GetHandle']) );
  Result := nil;
end;

function TISrvDataSet.ResultSetExists: Boolean;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.ResultSetExists']) );
  Result := False;
end;

procedure TISrvDataSet.SetSelectBuffer;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.SetSelectBuffer']) );
end;

function TISrvDataSet.ReadBlob(FieldNo: Integer; var BlobData: string): Longint;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.ReadBlob']) );
  Result := 0;
end;

function TISrvDataSet.WriteBlob(FieldNo: Integer; const Buffer; Count: LongInt): Longint;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.WriteBlob']) );
  Result := 0;
end;

function TISrvDataSet.WriteBlobByName(Name: string; const Buffer; Count: LongInt): Longint;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.WriteBlobByName']) );
  Result := 0;
end;

	// Query methods
function TISrvDataSet.QParamBindSize(Param: TSDHelperParam): Integer;
begin
  Result := Query.BindBufferSize(Param);
end;

procedure TISrvDataSet.QBindParams;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.QBindParams']) );
end;

function TISrvDataSet.QGetRowsAffected: Integer;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.QGetRowsAffected']) );
  Result := -1;
end;

procedure TISrvDataSet.QPrepareSQL(Value: PChar);
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.QPrepareSQL']) );
end;

procedure TISrvDataSet.QExecute;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.QExecute']) );
end;

	// StoredProc methods

procedure TISrvDataSet.SpAddParam(AParamName: string; ADataType: TFieldType; AParamType: TSDHelperParamType);
begin
  with StoredProc do begin
{$IFDEF SD_VCL4}
    with TParam(Params.Add) do begin
      ParamType := AParamType;
      DataType := ADataType;
      Name := AParamName;
    end;
{$ELSE}
    Params.CreateParam( ADataType, AParamName, AParamType);
{$ENDIF}
  end;
end;

function TISrvDataSet.SpParamBindSize(Param: TSDHelperParam): Integer;
begin
  Result := StoredProc.BindBufferSize(Param);
end;

procedure TISrvDataSet.SpBindParams;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.SpBindParams']) );
end;

procedure TISrvDataSet.SpCreateParamDesc;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.SpCreateParamDesc']) );
end;

procedure TISrvDataSet.SpPrepareProc;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.SpPrepareProc']) );
end;

procedure TISrvDataSet.SpExecute;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.SpExecute']) );
end;

procedure TISrvDataSet.SpExecProc;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.SpExecProc']) );
end;

procedure TISrvDataSet.SpGetResults;
begin
  ASSERT( False, Format(SFuncNotImplemented, ['TISrvDataSet.SpGetResults']) );
end;

function TISrvDataSet.SpNextResultSet: Boolean;
begin
  Result := False;
end;

{ TSDFieldDescList }
destructor TSDFieldDescList.Destroy;
begin
  Clear;

  inherited;
end;

procedure TSDFieldDescList.Clear;
var
  i: Integer;
begin
  for i:=0 to Count-1 do
    Dispose( PSDFieldDesc(Items[i]) );

  inherited Clear;
end;

function TSDFieldDescList.GetFieldDescs(Index: Integer): TSDFieldDesc;
begin
  Result := PSDFieldDesc( Items[Index] )^;
end;

function TSDFieldDescList.FieldDescByName(const AFieldName: string): TSDFieldDesc;
var
  i: Integer;
begin
  for i:=0 to Count-1 do
    if AnsiUpperCase(PSDFieldDesc(Items[i])^.FieldName) = AnsiUpperCase( AFieldName ) then begin
      Result := PSDFieldDesc( Items[i] )^;
      Break;
    end else if i = (Count-1) then
      DatabaseErrorFmt( SFieldDescNotFound, [AFieldName] );
end;

function TSDFieldDescList.FieldDescByNumber(AFieldNo: Integer): TSDFieldDesc;
var
  i: Integer;
begin
  for i:=0 to Count-1 do
    if PSDFieldDesc(Items[i])^.FieldNo = AFieldNo then begin
      Result := PSDFieldDesc( Items[i] )^;
      Break;
    end else if i = (Count-1) then
      DatabaseErrorFmt( SFieldDescNotFound, ['FieldNo='+IntToStr(AFieldNo)] );
end;


procedure InitSrvVar;
var
  i: TSDServerType;
begin
  for i:=Low(TSDServerType) to High(TSDServerType) do begin
    SrvApiDLLs[i] := '';
    SrvDatabaseClasses[i] := nil;
  end;

  SrvDatabaseClasses[stSQLBase] := TICsbDatabase;
  SrvDatabaseClasses[stOracle]	:= TIOraDatabase;
  SrvDatabaseClasses[stSQLServer]:=TIMssDatabase;
  SrvDatabaseClasses[stSybase]	:= TISybDatabase;
  SrvDatabaseClasses[stDB2]	:= TIDB2Database;
  SrvDatabaseClasses[stInformix]:= TIInfDatabase;
  SrvDatabaseClasses[stODBC]	:= TIOdbcDatabase;
  SrvDatabaseClasses[stInterbase]:=TIIntDatabase;
  SrvDatabaseClasses[stMySQL]	:= TIMyDatabase;
  SrvDatabaseClasses[stPostgreSQL]:= TIPgDatabase;

  SrvApiDLLs[stSQLBase]	:= SDCsb.DefSqlApiDLL;
  SrvApiDLLs[stOracle]	:= SDOra.DefSqlApiDLL;
  SrvApiDLLs[stSQLServer]:=SDMss.DefSqlApiDLL;
  SrvApiDLLs[stSybase]	:= SDSyb.DefSqlApiDLL;
  SrvApiDLLs[stDB2]	:= SDDb2.DefSqlApiDLL;
  SrvApiDLLs[stInformix]:= SDInf.DefSqlApiDLL;
  SrvApiDLLs[stODBC]	:= SDOdbc.DefSqlApiDLL;
  SrvApiDLLs[stInterbase]:=SDInt.DefSqlApiDLL;
  SrvApiDLLs[stMySQL]	:= SDMySQL.DefSqlApiDLL;
  SrvApiDLLs[stPostgreSQL]:=SDPgSQL.DefSqlApiDLL;
end;



initialization
  BusyCount := 0;
  ForceOCI7	:= False;
  Oracle8Blobs	:= False;
  MinStrParamSize:= 255;
  InitSrvVar;
  Sessions := TSDSessionList.Create;
  Session := TSDSession.Create(nil);
  Session.SessionName := 'Default';
finalization
  Sessions.Free;
  Sessions := nil;
end.

