
{**********************************************************}
{							   }
{       Delphi SQLDirect Component Library		   }
{	Informix CLI v2.8 Interface Unit 		   }
{                                                          }
{       Copyright (c) 1997,2002 by Yuri Sheino		   }
{                                                          }
{**********************************************************}

unit SDInf;

interface

{$I SqlDir.inc}

uses
  Windows, SysUtils, Classes, Db, Dialogs, SyncObjs,
  SDEngine, SDConsts, SDOdbc;

{********************************************************************
** INFXCLI.H - This is the the main include for Informix-CLI
**             applications.
**
*********************************************************************}
const

	// For extended errors
  SQL_DIAG_ISAM_ERROR	= 13;
  SQL_DIAG_XA_ERROR  	= 14;

// START -- Q+E Software's SQLSetStmtOption extensions (1040 to 1139)
// defines here for backwards compatibility

  SQL_STMTOPT_START	= 1040;

	// Get the rowid for the last row inserted
  SQL_GET_ROWID		= (SQL_STMTOPT_START+8);

	// Get the value for the serial column in the last row inserted
  SQL_GET_SERIAL_VALUE	= (SQL_STMTOPT_START+9);

// END -- Q+E Software's SQLSetStmtOption extensions (1040 to 1139)

{*
**    Informix extensions
*}

	// Informix Column Attributes Flags Definitions
  FDNULLABLE	= $0001;	// null allowed in field
  FDDISTINCT   	= $0002;	// distinct of all
  FDDISTLVARCHAR= $0004;	// distinct of SQLLVARCHAR
  FDDISTBOOLEAN	= $0008;	// distinct of SQLBOOL
  FDDISTSIMP   	= $0010;	// distinct of simple type
  FDCSTTYPE    	= $0020;	// constructor type
  FDNAMED      	= $0040;	// named row type

{
#define ISNULLABLE( flags )       ( flags & FDNULLABLE ? 1 : 0)
#define ISDISTINCT( flags )       ( flags & FDDISTINCT ? 1 : 0)
}

	// Informix Type Estensions
  SQL_INFX_UDT_FIXED  		= -100;
  SQL_INFX_UDT_VARYING 		= -101;
  SQL_INFX_UDT_BLOB    		= -102;
  SQL_INFX_UDT_CLOB    		= -103;
  SQL_INFX_UDT_LVARCHAR		= -104;
  SQL_INFX_RC_ROW      		= -105;
  SQL_INFX_RC_COLLECTION	= -106;
  SQL_INFX_RC_LIST     		= -107;
  SQL_INFX_RC_SET      		= -108;
  SQL_INFX_RC_MULTISET 		= -109;
  SQL_INFX_UNSUPPORTED 		= -110;

	// Informix Connect Attributes Extensions
  SQL_OPT_LONGID                    	= 2251;
  SQL_INFX_ATTR_LONGID                 	= SQL_OPT_LONGID;
  SQL_INFX_ATTR_LEAVE_TRAILING_SPACES  	= 2252;
  SQL_INFX_ATTR_DEFAULT_UDT_FETCH_TYPE 	= 2253;
  SQL_INFX_ATTR_ENABLE_SCROLL_CURSORS  	= 2254;
  SQL_ENABLE_INSERT_CURSOR             	= 2255;
  SQL_INFX_ATTR_ENABLE_INSERT_CURSORS  	= SQL_ENABLE_INSERT_CURSOR;
  SQL_INFX_ATTR_OPTIMIZE_AUTOCOMMIT    	= 2256;
  SQL_INFX_ATTR_ODBC_TYPES_ONLY        	= 2257;

	// Informix Descriptor Extensions
  SQL_INFX_ATTR_FLAGS                  	= 1900; // UDWORD
  SQL_INFX_ATTR_EXTENDED_TYPE_CODE     	= 1901; // UDWORD
  SQL_INFX_ATTR_EXTENDED_TYPE_NAME     	= 1902; // UCHAR ptr
  SQL_INFX_ATTR_EXTENDED_TYPE_OWNER    	= 1903; // UCHAR ptr
  SQL_INFX_ATTR_EXTENDED_TYPE_ALIGNMENT	= 1904; // UDWORD
  SQL_INFX_ATTR_SOURCE_TYPE_CODE       	= 1905;	// UDWORD

	// Informix Statement Attributes Extensions
  SQL_VMB_CHAR_LEN		= 2325;
  SQL_INFX_ATTR_VMB_CHAR_LEN	= SQL_VMB_CHAR_LEN;

	// Informix fOption, SQL_VMB_CHAR_LEN vParam
  SQL_VMB_CHAR_EXACT     	= 0;
  SQL_VMB_CHAR_ESTIMATE        	= 1;

	// Informix row/collection traversal constants
  SQL_INFX_RC_NEXT  		= 1;
  SQL_INFX_RC_PRIOR 		= 2;
  SQL_INFX_RC_FIRST 		= 3;
  SQL_INFX_RC_LAST    		= 4;
  SQL_INFX_RC_ABSOLUTE		= 5;
  SQL_INFX_RC_RELATIVE		= 6;
  SQL_INFX_RC_CURRENT 		= 7;

{*******************************************************************************
 * Large Object (LO) related structures
 *
 * LO_SPEC: Large object spec structure
 * It is used for creating smartblobs. The user may examin and/or set certain
 * fields of LO_SPEC by using ifx_lo_spec[set|get]_* accessor functions.
 *
 * LO_PTR: Large object pointer structure
 * Identifies the LO and provides ancillary, security-related information.
 *
 * LO_STAT: Large object stat structure
 * It is used in querying attribtes of smartblobs. The user may examin fields
 * herein by using ifx_lo_stat[set|get]_* accessor functions.
 *
 * These structures are opaque to the user. Accessor functions are provided
 * for these structures.
 ******************************************************************************}

	// Informix GetInfo Extensions to obtain length of LO related structures
  SQL_INFX_LO_SPEC_LENGTH	= 2250; 	// UWORD
  SQL_INFX_LO_PTR_LENGTH       	= 2251; 	// UWORD
  SQL_INFX_LO_STAT_LENGTH      	= 2252; 	// UWORD

{******************************************************************************
 * LO Open flags: (see documentation for further explanation)
 *
 * LO_APPEND   - Positions the seek position to end-of-file + 1. By itself,
 *               it is equivalent to write only mode followed by a seek to the
 *               end of large object. Read opeartions will fail.
 *               You can OR the LO_APPEND flag with another access mode.
 * LO_WRONLY   - Only write operations are valid on the data.
 * LO_RDONLY   - Only read operations are valid on the data.
 * LO_RDWR     - Both read and write operations are valid on the data.
 *
 * LO_RANDOM   - If set overrides optimizer decision. Indicates that I/O is
 *               random and that the system should not read-ahead.
 * LO_SEQUENTIAL - If set overrides optimizer decision. Indicates that
 *               reads are sequential in either forward or reverse direction.
 *
 * LO_FORWARD  - Only used for sequential access. Indicates that the sequential
 *               access will be in a forward direction, i.e. from low offset
 *               to higher offset.
 * LO_REVERSE  - Only used for sequential access. Indicates that the sequential
 *               access will be in a reverse direction.
 *
 * LO_BUFFER   - If set overrides optimizer decision. I/O goes through the
 *               buffer pool.
 * LO_NOBUFFER - If set then I/O does not use the buffer pool.
 ******************************************************************************}

  LO_APPEND   		= $01;
  LO_WRONLY    		= $02;
  LO_RDONLY    		= $04;		// default
  LO_RDWR      		= $08;

  LO_RANDOM    		= $20;    	// default is determined by optimizer
  LO_SEQUENTIAL		= $40;    	// default is determined by optimizer

  LO_FORWARD   		= $080;    	// default
  LO_REVERSE   		= $100;

  LO_BUFFER    		= $200;   	// default is determined by optimizer
  LO_NOBUFFER  		= $400;   	// default is determined by optimizer

  LO_DIRTY_READ		= $010;
  LO_NODIRTY_READ	= $800;

{*******************************************************************************
 * LO create-time flags:
 *
 * Bitmask - Set/Get via ifx_lo_specset_flags() on LO_SPEC.
 ******************************************************************************}

  LO_ATTR_LOG                 	= $0001;
  LO_ATTR_NOLOG               	= $0002;
  LO_ATTR_DELAY_LOG           	= $0004;
  LO_ATTR_KEEP_LASTACCESS_TIME	= $0008;
  LO_ATTR_NOKEEP_LASTACCESS_TIME= $0010;
  LO_ATTR_HIGH_INTEG          	= $0020;
  LO_ATTR_MODERATE_INTEG      	= $0040;

{*******************************************************************************
 * Symbolic constants for the "lseek" routine
 ******************************************************************************}

  LO_SEEK_SET	= 0;	// Set curr. pos. to "offset"           
  LO_SEEK_CUR 	= 1;   	// Set curr. pos. to current + "offset" 
  LO_SEEK_END 	= 2;   	// Set curr. pos. to EOF + "offset"

{*******************************************************************************
 * Intersolv specific infoTypes for SQLGetInfo
 ******************************************************************************}

  SQL_RESERVED_WORDS		= 1011;
  SQL_PSEUDO_COLUMNS	       	= 1012;
  SQL_FROM_RESERVED_WORDS      	= 1013;
  SQL_WHERE_CLAUSE_TERMINATORS 	= 1014;
  SQL_COLUMN_FIRST_CHARS       	= 1015;
  SQL_COLUMN_MIDDLE_CHARS      	= 1016;
  SQL_TABLE_FIRST_CHARS	       	= 1018;
  SQL_TABLE_MIDDLE_CHARS       	= 1019;
  SQL_FAST_SPECIAL_COLUMNS     	= 1021;
  SQL_ACCESS_CONFLICTS	       	= 1022;
  SQL_LOCKING_SYNTAX	       	= 1023;
  SQL_LOCKING_DURATION	       	= 1024;
  SQL_RECORD_OPERATIONS	       	= 1025;
  SQL_QUALIFIER_SYNTAX	       	= 1026;


type
  HINFX_RC	= SQLPOINTER;		// row & collection handle


type
  ESDInfError = class(ESDOdbcError);

{ TIInfDatabase }
  TIInfDatabase = class(TICustomOdbcDatabase)
  protected
    procedure FreeSqlLib; override;
    procedure LoadSqlLib; override;
    procedure RaiseSDEngineError(AErrorCode, ANativeError: TSDEResult; const AMsg, ASqlState: string); override;

    procedure Logon(const sRemoteDatabase, sUserName, sPassword: string); override;
  public
    function CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet; override;
  end;

{ TIInfDataSet }
  TIInfDataSet = class(TICustomOdbcDataSet)
  private
    function GetSrvDatabase: TIInfDatabase;
  protected
    property SrvDatabase: TIInfDatabase read GetSrvDatabase;
  end;

const
  DefSqlApiDLL	= 'ICLIT09B.DLL;ICLIT09A.DLL';

var
  InfCalls: TOdbcFunctions;

(*******************************************************************************
			Load/Unload Sql-library
********************************************************************************)
procedure LoadSqlLib;
procedure FreeSqlLib;

implementation

resourcestring
  SErrLibLoading 	= 'Error loading library ''%s''';
  SErrLibUnloading	= 'Error unloading library ''%s''';

var
  hSqlLibModule: THandle;
  SqlLibRefCount: Integer;
  SqlLibLock: TCriticalSection;  

procedure SetProcAddresses;
begin
  SDOdbc.SetApiCalls( hSqlLibModule, InfCalls );
end;

procedure ResetProcAddresses;
begin
  SDOdbc.ResetApiCalls( InfCalls );
end;

procedure LoadSqlLib;
const
  SqlApiDLLSep	= ';';	// delimiters of API-library names
var
  sLibName, sSqlApiDLL: string;
  CurPos: Integer;
begin
  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 0) then begin
      sSqlApiDLL := SrvApiDLLs[stInformix];

      CurPos := 1;
      repeat
        sLibName := ExtractLibName(sSqlApiDLL, SqlApiDLLSep, CurPos);
        hSqlLibModule := LoadLibrary( PChar(sLibName) );
        if hSqlLibModule <> 0 then
          Break;
      until CurPos > Length(sSqlApiDLL);
      if (hSqlLibModule = 0) then
        raise Exception.CreateFmt(SErrLibLoading, [sSqlApiDLL]);

      Inc(SqlLibRefCount);
      SetProcAddresses;
    end else
      Inc(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;

procedure FreeSqlLib;
begin
  if SqlLibRefCount = 0 then
    Exit;
    
  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 1) then begin
      if FreeLibrary(hSqlLibModule) then
        hSqlLibModule := 0
      else
        raise Exception.CreateFmt(SErrLibUnloading, [ SrvApiDLLs[stInformix] ]);
      Dec(SqlLibRefCount);
      ResetProcAddresses;
    end else
      Dec(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;


{ TIInfDatabase }
function TIInfDatabase.CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet;
begin
  Result := TIInfDataSet.Create( ADataSet );
end;

procedure TIInfDatabase.FreeSqlLib;
begin
  SDInf.FreeSqlLib;
end;

procedure TIInfDatabase.LoadSqlLib;
begin
  SDInf.LoadSqlLib;

  FCalls := InfCalls;
end;

procedure TIInfDatabase.RaiseSDEngineError(AErrorCode, ANativeError: TSDEResult; const AMsg, ASqlState: string);
begin
  raise ESDInfError.CreateWithSqlState(AErrorCode, ANativeError, AMsg, ASqlState);
end;

procedure TIInfDatabase.Logon(const sRemoteDatabase, sUserName, sPassword: string);
  function ParseRemoteDatabase(Str, Delimiters: string; var sServer, sDatabase: string) :Boolean;
  var
    i: Integer;
  begin
    sServer := '';
    sDatabase := '';
    i := 1;
    while i <= Length(Str) do begin
      if IsDelimiter(Delimiters, Str, i) then
        Break;
      Inc(i);
    end;
    if i <= Length(Str) then begin
      sServer := System.Copy(Str, 1, i-1);
      while i <= Length(Str) do begin
        if not IsDelimiter(Delimiters, Str, i) then
          Break;
        Inc(i);
      end;
      sDatabase := System.Copy(Str, i, Length(Str)-i+1);
      Result := True;
    end else begin
      sServer := Str;
      Result := False;
    end;
  end;
var
  rcd: SQLRETURN;
  sConnStr, sSrv, sDb: string;
  sConnStrOut: string;
  ConnStrOut: SQLSMALLINT;
begin
  ConnStrOut := 0;
  SetLength(sConnStrOut, 1024);

  try
    LoadSqlLib;
    AllocOdbcHandles;

	// if sRemoteDatabase contains server and database name, like srv:db1
    if ParseRemoteDatabase(sRemoteDatabase, ServerDelimiters, sSrv, sDb) then begin
      sConnStr := Format('SERVICE=%s;HOST=test;PROTOCOL=OLSOCTCP;SERVER=%s;DATABASE=%s;UID=%s;PWD=%s',
      			[sSrv, sSrv, sDb, sUserName, sPassword]);
      rcd := Calls.SQLDriverConnect( DbcHandle, 0, PChar(sConnStr), SQL_NTS,
      			PChar(sConnStrOut), Length(sConnStrOut), @ConnStrOut, SQL_DRIVER_NOPROMPT );
    end else
    	// if DSN string with parameters (for example: username and password)
      if (AnsiPos('=', sRemoteDatabase) > 0) then
        rcd := Calls.SQLDriverConnect( DbcHandle, 0, PChar(sRemoteDatabase), SQL_NTS,
        		PChar(sConnStrOut), Length(sConnStrOut), @ConnStrOut, SQL_DRIVER_NOPROMPT )
      else	// if sRemoteDatabase contains one word, which is a datasource name (from ODBC config)
    	rcd := Calls.SQLConnect( DbcHandle, PChar(sRemoteDatabase), SQL_NTS,
    			PChar(sUserName), SQL_NTS, PChar(sPassword), SQL_NTS );

    Check( rcd );

  except
    FreeOdbcHandles;
    FreeSqlLib;

    raise;
  end;
end;


{ TIInfDataSet }
function TIInfDataSet.GetSrvDatabase: TIInfDatabase;
begin
  Result := (inherited SrvDatabase) as TIInfDatabase;
end;


initialization
  hSqlLibModule	:= 0;
  SqlLibRefCount:= 0;
  InfCalls 	:= TOdbcFunctions.Create;
  SqlLibLock 	:= TCriticalSection.Create;
finalization
  while SqlLibRefCount > 0 do
    FreeSqlLib;
  SqlLibLock.Free;    
  InfCalls.Free;
end.
