
{*******************************************************}
{							}
{       Delphi SQLDirect Component Library		}
{	Oracle API (SQLNet 2.3+) Interface Unit		}
{       OCI Date: 23.05.1997 (updated from OCI8)        }
{       		                                }
{       Copyright (c) 1997,2002 by Yuri Sheino		}
{                                                       }
{*******************************************************}

unit SDOra;

interface

{$I SqlDir.inc}

uses
  Windows, SysUtils, Classes, Db, Registry, SyncObjs,
{$IFDEF SD_VCL6}
  FmtBcd, Variants,
{$ENDIF}
  SDEngine, SDConsts;

const
	(* 	FETCH RETURN CODES 	*)
  FetRTru = 1;			// data truncated
  FetRSin = 2;			// signed number fetched
  FetRDnn = 3;			// data is not numeric
  FetRNof = 4;			// numeric overflow
  FetRDtn = 5;			// data type not supported
  FetRDnd = 6;			// data is not in date format
  FetRNul = -1;			// data is null

{*******************************************************************************
*	limits.h - implementation dependent values                             *
*Purpose:                                                                      *
*       Contains defines for a number of implementation dependent values       *
*       which are commonly used in C programs.                                 *
*******************************************************************************}
const
  CHAR_BIT	= 8;         	// number of bits in a char
  SCHAR_MIN   	= -128;      	// minimum signed char value
  SCHAR_MAX     = 127;       	// maximum signed char value
  UCHAR_MAX     = $FF;      	// maximum unsigned char value
  CHAR_MIN      = 0;
  CHAR_MAX    	= UCHAR_MAX;

  MB_LEN_MAX    = 2;            	// max. # bytes in multibyte char
  SHRT_MIN     	= -32768;     		// minimum (signed) short value
  SHRT_MAX      = 32767;        	// maximum (signed) short value
  USHRT_MAX     = $FFFF;        	// maximum unsigned short value
  INT_MIN     	= (-2147483647 - 1); 	// minimum (signed) int value
  INT_MAX       = 2147483647;		// maximum (signed) int value
  UINT_MAX      = $FFFFFFFF;     	// maximum unsigned int value
  LONG_MIN    	= (-2147483647 - 1); 	// minimum (signed) long value
  LONG_MAX      = 2147483647;     	// maximum (signed) long value
  ULONG_MAX     = $FFFFFFFF;  		// maximum unsigned long value

{*******************************************************************************
*				OraTypes.h                                     *
*******************************************************************************}
//const
// #define boolean int
//  TRUE	= 1;
//  FALSE = 0;

type
  eword		= Integer;			// use where sign not important
  uword		= Integer;			// use where unsigned important
  sword		= Integer;			// use where   signed important
  size_t	= Integer;
  OraBoolean	= LongBool;			// 4 byte
const
  EWORDMAXVAL  	= INT_MAX;
  EWORDMINVAL  	= 0;
  UWORDMAXVAL  	= UINT_MAX;
  UWORDMINVAL  	= 0;
  SWORDMAXVAL  	= INT_MAX;
  SWORDMINVAL  	= INT_MIN;
  MINEWORDMAXVAL= 32767;
  MAXEWORDMINVAL= 0;
  MINUWORDMAXVAL= 65535;
  MAXUWORDMINVAL= 0;
  MINSWORDMAXVAL= 32767;
  MAXSWORDMINVAL= -32767;

type
  eb1		= ShortInt;		// use where sign not important
  ub1		= Byte;			// use where unsigned important
  sb1		= ShortInt;		// use where   signed important
  eb1p		= ^eb1;
  ub1p		= ^ub1;
  sb1p		= ^sb1;
  
const
  EB1MAXVAL 	= SCHAR_MAX;
  EB1MINVAL 	= 0;
  UB1MAXVAL 	= UCHAR_MAX;
  UB1MINVAL 	= 0;
  SB1MAXVAL 	= SCHAR_MAX;
  SB1MINVAL 	= SCHAR_MIN;
  MINEB1MAXVAL  = 127;
  MAXEB1MINVAL  = 0;
  MINUB1MAXVAL  = 255;
  MAXUB1MINVAL  = 0;
  MINSB1MAXVAL  = 127;
  MAXSB1MINVAL  = -127;
// number of bits in a byte
  UB1BITS       = CHAR_BIT;
  UB1MASK       = $FF;

type
// human readable (printable) characters
  PText		= PChar;
  PPText	= ^PText;

  eb2		= SmallInt;		// use where sign not important
  ub2		= Word;			// use where unsigned important
  sb2		= SmallInt;		// use where   signed important
  eb2p		= ^eb2;
  ub2p		= ^ub2;
  ub2pp		= ^ub2p;
  sb2p		= ^sb2;

const
  EB2MAXVAL 	= SHRT_MAX;
  EB2MINVAL 	= 0;
  UB2MAXVAL 	= USHRT_MAX;
  UB2MINVAL 	= 0;
  SB2MAXVAL 	= SHRT_MAX;
  SB2MINVAL 	= SHRT_MIN;
  MINEB2MAXVAL	= 32767;
  MAXEB2MINVAL  = 0;
  MINUB2MAXVAL 	= 65535;
  MAXUB2MINVAL  = 0;
  MINSB2MAXVAL 	= 32767;
  MAXSB2MINVAL	= -32767;

type
  eb4		= LongInt;		// use where sign not important
  ub4		= LongInt;		// use where unsigned important
  sb4		= LongInt;		// use where   signed important
  eb4p		= ^eb4;
  ub4p		= ^ub4;
  ub4pp		= ^ub4p;  
  sb4p		= ^sb4;

const
  EB4MAXVAL 	= INT_MAX;
  EB4MINVAL     = 0;
  UB4MAXVAL 	= UINT_MAX;
  UB4MINVAL     = 0;
  SB4MAXVAL  	= INT_MAX;
  SB4MINVAL  	= INT_MIN;
  MINEB4MAXVAL	= 2147483647;
  MAXEB4MINVAL  = 0;
  MINUB4MAXVAL 	= UINT_MAX;
  MAXUB4MINVAL  = 0;
  MINSB4MAXVAL 	= 2147483647;
  MAXSB4MINVAL 	= -2147483647;

type
  ubig_ora	= LongInt;		// use where unsigned important
  sbig_ora	= LongInt;		// use where   signed important
const
  UBIG_ORAMAXVAL 	= ULONG_MAX;
  UBIG_ORAMINVAL        = 0;
  SBIG_ORAMAXVAL  	= LONG_MAX;
  SBIG_ORAMINVAL  	= LONG_MIN;
  MINUBIG_ORAMAXVAL 	= 2147483647;
  MAXUBIG_ORAMINVAL     = 0;
  MINSBIG_ORAMAXVAL  	= 2147483647;
  MAXSBIG_ORAMINVAL 	= -2147483647;

type
  dvoid 		= Pointer;
  Pdvoid 		= ^dvoid;
  PPdvoid		= ^Pdvoid;  

const
  SIZE_TMAXVAL		= UB4MAXVAL;		// This case applies for others
  MINSIZE_TMAXVAL 	= 65535;


{*******************************************************************************
*	ocidfn.h
*   Common header file for OCI C sample programs.                              *
*   This header declares the cursor and logon data area structure.             *
*   The types used are defined in <oratypes.h>.                                *
*******************************************************************************}
type
// 	The cda_head struct is strictly PRIVATE.  It is used
//   internally only. Do not use this struct in OCI programs.
  cda_head = record
    v2_rc:	sb2;
    ft:   	ub2;
    rpc:  	ub4;
    peo:  	ub2;
    fc:   	ub1;
    rcs1: 	ub1;
    rc:   	ub2;
    wrn:  	ub1;
    rcs2: 	ub1;
    rcs3:	sword;
    rcs4:	ub4;
    rcs5:   	ub2;
    rcs6:   	ub1;
    rcs7:	ub4;
    rcs8:     	ub2;
    ose:        sword;
    chk:        ub1;
    rcsp:      	dvoid;
  end;

const
  CDA_SIZE 	= 64;
  HDA_SIZE 	= 256;

type
	 // rowid structures
  rd = record
    rcs4: ub4;
    rcs5: ub2;
    rcs6: ub1;
  end;
  rid = record
    r: 	  rd;
    rcs7: ub4;
    rcs8: ub2;
  end;

// the real CDA, padded to CDA_SIZE bytes in size
  cda_def = record
    v2_rc:	sb2;         	// V2 return code
    ft:         ub2;      	// SQL function type
    rpc:        ub4;   		// rows processed count
    peo:        ub2;     	// parse error offset
    fc:         ub1;      	// OCI function code
    rcs1:       ub1;            // filler area
    rc:         ub2;         	// V7 return code
    wrn:        ub1;          	// warning flags
    rcs2:       ub1;            // reserved
    rcs3:       sword;      	// reserved
    rowid:	rid;		// rowid structure
    ose:	sword;		// OSD dependent error
    chk:	ub1;
    rcsp:	dvoid;		// pointer to reserved area
    rcs9:	array[1..CDA_SIZE - SizeOf(cda_head)] of ub1; // filler
  end;


// the logon data area (LDA) is the same shape as the CDA
  TCdaDef	= cda_def;
  PCdaDef	= ^TCdaDef;
  Lda_Def	= Cda_Def;
  TLdaDef	= Lda_Def;
  PLdaDef	= ^TLdaDef;
  THdaDef	= array[1..HDA_SIZE div 4] of ub4;
  PHdaDef	= ^THdaDef;

const
	// OCI Environment Modes for opinit call
  OCI_EV_DEF 	= 0; 		// default single-threaded environment
  OCI_EV_TSF 	= 1;		// thread-safe environment

	// OCI Logon Modes for olog call
  OCI_LM_DEF 	= 0;  		// default login
  OCI_LM_NBL 	= 1;		// non-blocking logon

// OCI_*_PIECE defines the piece types that are returned or set
  OCI_ONE_PIECE  = 0;		// there or this is the only piece
  OCI_FIRST_PIECE= 1;    	// the first of many pieces
  OCI_NEXT_PIECE = 2;     	// the next of many pieces
  OCI_LAST_PIECE = 3;		// the last piece of this column

	// input data types
  SQLT_CHR	= 1;     	// (ORANET TYPE) character string
  SQLT_NUM	= 2;       	// (ORANET TYPE) oracle numeric
  SQLT_INT	= 3;            // (ORANET TYPE) integer
  SQLT_FLT	= 4;   		// (ORANET TYPE) Floating point number
  SQLT_STR	= 5;            // zero terminated string
  SQLT_VNU	= 6;     	// NUM with preceding length byte
  SQLT_PDN	= 7;  		// (ORANET TYPE) Packed Decimal Numeric
  SQLT_LNG	= 8;            // long
  SQLT_VCS	= 9;          	// Variable character string
  SQLT_NON	= 10;   	// Null/empty PCC Descriptor entry
  SQLT_RID	= 11;           // rowid
  SQLT_DAT	= 12;           // date in oracle format
  SQLT_VBI	= 15;           // binary in VCS format
  SQLT_BIN	= 23;           // binary data(DTYBIN)
  SQLT_LBI	= 24;           // long binary
  SQLT_UIN	= 68;           // unsigned integer
  SQLT_SLS	= 91;     	// Display sign leading separate
  SQLT_LVC	= 94;           // Longer longs (char)
  SQLT_LVB	= 95;           // Longer long binary
  SQLT_AFC	= 96;           // Ansi fixed char
  SQLT_AVC	= 97;           // Ansi Var char
  SQLT_CUR	= 102;          // cursor  type
  SQLT_RDD	= 104;		// rowid descriptor
  SQLT_LAB	= 105;          // label type
  SQLT_OSL	= 106;          // oslabel type

  SQLT_NTY 	= 108;          // named object type
  SQLT_REF 	= 110;          // ref type
  SQLT_CLOB	= 112;          // character lob
  SQLT_BLOB	= 113;          // binary lob
  SQLT_BFILE	= 114;          // binary file lob
  SQLT_CFILE	= 115;          // character file lob
  SQLT_RSET	= 116;          // result set type
  SQLT_NCO 	= 122;          // named collection type (varray or nested table)
  SQLT_VST 	= 155;          // OCIString type
  SQLT_ODT 	= 156;          // OCIDate type

// CHAR/NCHAR/VARCHAR2/NVARCHAR2/CLOB/NCLOB char set "form" information
  SQLCS_IMPLICIT= 1;		// for CHAR, VARCHAR2, CLOB w/o a specified set
  SQLCS_NCHAR  	= 2; 		// for NCHAR, NCHAR VARYING, NCLOB
  SQLCS_EXPLICIT= 3; 		// for CHAR, etc, with "CHARACTER SET ..." syntax
  SQLCS_FLEXIBLE= 4; 		// for PL/SQL "flexible" parameters
  SQLCS_LIT_NULL= 5; 		// for typecheck of NULL and empty_clob() lits 


{*******************************************************************************
*   Declare the OCI functions.                                                 *
*   Prototype information is included.                                         *
*   Use this header for ANSI C compilers.                                      *
*******************************************************************************}

var
	{ OCI BIND (Piecewise or with Skips) }
  obindps: function(var cursor: TCdaDef; opcode: ub1; sqlvar: PText;
	       sqlvl: sb4; pvctx: Pointer; progvl: sb4; ftype, scale: sword;
	       ind, alen, arcode: ub2p;
	       pv_skip, ind_skip, alen_skip, rc_skip: sb4;
	       maxsiz: ub4; cursiz: ub4p;
	       fmt: PText; fmtl: sb4; fmtt: sword): sword; cdecl;
  obreak: function(var lda: TCdaDef): sword; cdecl;
  ocan: function(var cursor: TCdaDef): sword; cdecl;
  oclose: function(var cursor: TCdaDef): sword; cdecl;
  ocof: function(var lda: TCdaDef): sword; cdecl;
  ocom: function(var lda: TCdaDef): sword; cdecl;
  ocon: function(var lda: TCdaDef): sword; cdecl;


	{ OCI DEFINe (Piecewise or with Skips) }
  odefinps: function(var cursor: TCdaDef; opcode: ub1; pos: sword; bufctx: PText;
		bufl: sb4; ftype, scale: sword;	indp: sb2p;
                fmt: PText; fmtl: sb4; fmtt: sword; rlen, rcode: ub2p;
		pv_skip, ind_skip, alen_skip, rc_skip: sb4): sword; cdecl;
  odessp: function(var cursor: TCdaDef; objnam: PText; onlen: size_t;
              rsv1: ub1p; rsv1ln: size_t; rsv2: ub1p; rsv2ln: size_t;
              ovrld, pos, level: ub2p; text: PText;
              arnlen, dtype: ub2p; defsup, mode: ub1p;
              dtsiz: ub4p; prec, scale: sb2p; radix: ub1p;
              spare, arrsiz: ub4p): sword; cdecl;
  odescr: function(var cursor: TCdaDef; pos: sword; var dbsize: sb4;
                 var dbtype: sb2; cbuf: PText; var cbufl, dsize: sb4;
                 var prec, scale, nullok: sb2): sword; cdecl;
  oerhms: function(var lda: TLdaDef; rcode: sb2; buf: PText;
                 bufsiz: sword): sword; cdecl;
  oermsg: function(rcode: sb2; buf: PText): sword; cdecl;
  oexec : function(var cursor: TCdaDef): sword; cdecl;
  oexfet: function(var cursor: TCdaDef; nrows: ub4;
                 cancel, exact: sword): sword; cdecl;
  oexn: function(var cursor: TCdaDef; iters, rowoff: sword): sword; cdecl;
  ofen: function(var cursor: TCdaDef; nrows: sword): sword; cdecl;
  ofetch: function(var cursor: TCdaDef): sword; cdecl;
  oflng: function(var cursor: TCdaDef; pos: sword; buf: PText;
                 bufl: sb4; dtype: sword; var retl: ub4; offset: sb4): sword; cdecl;
  ogetpi: function(var cursor: TCdaDef; var piece: ub1; var ctxp: dvoid;
                 var iter, index: ub4): sword; cdecl;
  oopt: function(var cursor: TCdaDef; rbopt, waitopt: sword): sword; cdecl;
  opinit: function(mode: ub4): sword; cdecl;
  olog: function(var lda: TLdaDef; var hda: THdaDef;
                 uid: PText; uidl: sword;
                 pswd: PText; pswdl: sword;
                 conn: PText; connl: sword;
                 mode: ub4): sword; cdecl;
  ologof: function(var lda: TLdaDef): sword; cdecl;
  oopen: function(var cursor: TCdaDef; var lda: TCdaDef;
                 dbn: PText; dbnl, arsize: sword;
                 uid: PText; uidl: sword): sword; cdecl;
  oparse: function(var cursor: TCdaDef; sqlstm: PText; sqllen: sb4;
            defflg: sword; lngflg: ub4): sword; cdecl;
  orol: function(var lda: TCdaDef): sword; cdecl;
  osetpi: function(var cursor: TCdaDef; piece: ub1; bufp: dvoid; var len: ub4): sword; cdecl;

	{ non-blocking functions }
  onbset: function(var lda: TCdaDef): sword; cdecl;
  onbtst: function(var lda: TCdaDef): sword; cdecl;
  onbclr: function(var lda: TCdaDef): sword; cdecl;
//  ognfd: function(var lda: TCdaDef; fdp: dvoid): sword; cdecl; Function is not exist in OCI 7.2 and some OCI 7.3

{************************* OBSOLETE CALLS ********************************}

	{ OBSOLETE BIND CALLS }
  obndra: function(var cursor: TCdaDef; sqlvar: PText; sqlvl: sword;
  		progv: PText; progvl, ftype, scale: sword;
                indp: sb2p; alen, arcode: ub2p; maxsiz: ub4;
                cursiz: ub4p; fmt: PText; fmtl, fmtt: sword): sword; cdecl;
  obndrn: function(var cursor: TCdaDef; sqlvn: sword;
  		progv: PText; progvl, ftype, scale: sword;
                indp: sb2p; fmt: PText; fmtl, fmtt: sword): sword; cdecl;
  obndrv: function(var cursor: TCdaDef; sqlvar: PText; sqlvl: sword;
  		progv: PText; progvl, ftype, scale: sword;
                indp: sb2p; fmt: PText; fmtl, fmtt: sword): sword; cdecl;

	{ OBSOLETE DEFINE CALLS }
  odefin: function(var cursor: TCdaDef; pos: sword; buf: PText;
  		bufl, ftype, scale: sword; indp: sb2p;
                fmt: PText; fmtl, fmtt: sword; rlen, rcode: ub2p): sword; cdecl;


const
  MAXERRMSG		= 1000;
  DEFERRED_PARSE	= 1;
  ORACLE7_PARSE		= 2;
	// Oracle command type
  ORA_CT_CTB		= 1;	//  CREATE TABLE
  ORA_CT_SRL		= 2;	//  SET ROLE
  ORA_CT_INS		= 3;	//  INSERT
  ORA_CT_SEL		= 4;	//  SELECT
  ORA_CT_UPD		= 5;	//  UPDATE
  ORA_CT_DRL		= 6;	//  DROP ROLE
  ORA_CT_DVW		= 7;	//  DROP VIEW
  ORA_CT_DTB		= 8;	//  DROP TABLE
  ORA_CT_DEL		= 9;	//  DELETE
  ORA_CT_CVW		= 10;	//  CREATE VIEW
  ORA_CT_DUS		= 11;	//  DROP USER
  ORA_CT_CRL		= 12;	//  CREATE ROLE
  ORA_CT_CSQ		= 13;	//  CREATE SEQUENCE
  ORA_CT_ASQ		= 14;	//  ALTER SEQUENCE
//  ORA_CT_		= 15;	  (not used)
  ORA_CT_DSQ		= 16;	//  DROP SEQUENCE
  ORA_CT_CSC		= 17;	//  CREATE SCHEMA
  ORA_CT_CCL		= 18;	//  CREATE CLUSTER
  ORA_CT_CUS		= 19;	//  CREATE USER
  ORA_CT_CIN		= 20;	//  CREATE INDEX
  ORA_CT_DIN		= 21;	//  DROP INDEX
  ORA_CT_DCL		= 22;	//  DROP CLUSTER
  ORA_CT_VIN		= 23;	//  VALIDATE INDEX
  ORA_CT_CPR		= 24;	//  CREATE PROCEDURE
  ORA_CT_APR		= 25;	//  ALTER PROCEDURE
  ORA_CT_ATB		= 26;	//  ALTER TABLE
  ORA_CT_EXP		= 27;	//  EXPLAIN
  ORA_CT_GRT		= 28;	//  GRANT
  ORA_CT_RVK		= 29;	//  REVOKE
  ORA_CT_CSY		= 30;	//  CREATE SYNONYM
  ORA_CT_DSY		= 31;	//  DROP SYNONYM
//  ORA_CT_		= 32;	//  ALTER SYSTEM SWITCH LOG
  ORA_CT_STR		= 33;	//  SET TRANSACTION
  ORA_CT_PEX		= 34;	//  PL/SQL EXECUTE
  ORA_CT_LTB		= 35;	//  LOCK TABLE
//  ORA_CT_		= 36;  (not used)
  ORA_CT_REN		= 37;	//  RENAME
  ORA_CT_COM		= 38;	//  COMMENT
  ORA_CT_AUD		= 39;	//  AUDIT
  ORA_CT_NAU		= 40;	//  NOAUDIT
  ORA_CT_AIN		= 41;	//  ALTER INDEX
//  ORA_CT_		= 42;	//  CREATE EXTERNAL DATABASE
//  ORA_CT_		= 43;	//  DROP EXTERNAL DATABASE
  ORA_CT_CDB		= 44;	//  CREATE DATABASE
  ORA_CT_ADB		= 45;	//  ALTER DATABASE
//  ORA_CT_		= 46;	//  CREATE ROLLBACK SEGMENT
//  ORA_CT_		= 47;	//  ALTER ROLLBACK SEGMENT
//  ORA_CT_		= 48;	//  DROP ROLLBACK SEGMENT
  ORA_CT_CTS		= 49;	//  CREATE TABLESPACE
  ORA_CT_ATS		= 50;	//  ALTER TABLESPACE
  ORA_CT_DTS		= 51;	//  DROP TABLESPACE
  ORA_CT_ASE		= 52;	//  ALTER SESSION
  ORA_CT_AUS		= 53;	//  ALTER USER
  ORA_CT_CMT		= 54;	//  COMMIT
  ORA_CT_RBK		= 55;	//  ROLLBACK
  ORA_CT_SVP		= 56;	//  SAVEPOINT
//  ORA_CT_		= 57;	//  CREATE CONTROL FILE
//  ORA_CT_ATRC		= 58;	//  ALTER TRACING
  ORA_CT_CTR		= 59;	//  CREATE TRIGGER
  ORA_CT_ATR		= 60;	//  ALTER TRIGGER
  ORA_CT_DTR		= 61;	//  DROP TRIGGER
//  ORA_CT_		= 62;	//  ANALYZE TABLE
//  ORA_CT_		= 63;	//  ANALYZE INDEX
//  ORA_CT_		= 64;	//  ANALYZE CLUSTER
  ORA_CT_CPF		= 65;	//  CREATE PROFILE
  ORA_CT_DPF		= 66;	//  DROP PROFILE
  ORA_CT_APF		= 67;	//  ALTER PROFILE
  ORA_CT_DPR		= 68;	//  DROP PROCEDURE
//  ORA_CT_		= 69;	  (not used)
//  ORA_CT_		= 70;	//  ALTER RESOURCE COST
//  ORA_CT_		= 71;	//  CREATE SNAPSHOT LOG
//  ORA_CT_		= 72;	//  ALTER SNAPSHOT LOG
//  ORA_CT_		= 73;	//  DROP SNAPSHOT LOG
  ORA_CT_CSN		= 74;	//  CREATE SNAPSHOT
  ORA_CT_ASN		= 75;	//  ALTER SNAPSHOT
  ORA_CT_DSN		= 76;	//  DROP SNAPSHOT

  	// Oracle Internal Datatypes Sizes
  ORA_DTS_VARCHAR2 	= 2000;	// 2000 bytes
  ORA_DTS_NUMBER 	= 21;	// 21 bytes
  ORA_DTS_LONG 		= 0;	// 2^31-1 bytes
  ORA_DTS_ROWID 	= 6;	// 6 bytes
  ORA_DTS_DATE 		= 7;	// 7 bytes
  ORA_DTS_RAW 		= 255;	// 255 bytes
  ORA_DTS_LONG_RAW 	= 0;	// 2^31-1 bytes
  ORA_DTS_CHAR 		= 255;	// 255 bytes
  ORA_DTS_MLSLABEL 	= 255;	// 255 bytes

const
  ORA_ERR_VarNotInSel	= 1007;	// "variable not in select list" (ORA-01007)
  ORA_ERR_INVALID_OCI_OP= 1010;	// 'Invalid OCI Operation'
  ORA_ERR_PASSWORD_EXPIRED = 28001;	// "the password has expired" (ORA-28001)

  OCI_MORE_INSERT_PIECES= 3129;
  OCI_MORE_FETCH_PIECES	= 3130;
  	//   exit flags
  OCI_EXIT_FAILURE 	= 1;
  OCI_EXIT_SUCCESS	= 0;

  MAX_LONG_COL_SIZE	= SB4MAXVAL;

// OCI8 begin

{*******************************************************************************
*	OCI.h - V8 Oracle Call Interface definitions
*Purpose:
*     This file defines all the constants and structures required by a V8
*     OCI programmer.
*******************************************************************************}
const
//	Modes
  OCI_DEFAULT		= $00;		// the default value for parameters and attributes
  OCI_THREADED		= $01;      	// the application is in threaded environment
  OCI_OBJECT  		= $02;        	// the application is in object environment
  OCI_NON_BLOCKING	= $04;          // non blocking mode of operation
  OCI_ENV_NO_MUTEX	= $08; 		// the environment handle will not be protected by a mutex internally

//	Handle Types (handle types range from 1 - 49)
  OCI_HTYPE_FIRST	= 1;   		// start value of handle type
  OCI_HTYPE_ENV 	= 1;            // environment handle
  OCI_HTYPE_ERROR	= 2;   		// error handle
  OCI_HTYPE_SVCCTX	= 3;   		// service handle
  OCI_HTYPE_STMT	= 4;   		// statement handle
  OCI_HTYPE_BIND	= 5;   		// bind handle
  OCI_HTYPE_DEFINE	= 6;   		// define handle
  OCI_HTYPE_DESCRIBE	= 7;   		// describe handle
  OCI_HTYPE_SERVER	= 8;   		// server handle
  OCI_HTYPE_SESSION	= 9;   		// authentication handle
  OCI_HTYPE_TRANS	= 10;  		// transaction handle
  OCI_HTYPE_COMPLEXOBJECT= 11; 		// complex object retrieval handle
  OCI_HTYPE_SECURITY	= 12;  		// security handle
  OCI_HTYPE_LAST	= 12;  		// last value of a handle type

//	Descriptor Types (descriptor values range from 50 - 255)
  OCI_DTYPE_FIRST	= 50;    	// start value of descriptor type
  OCI_DTYPE_LOB		= 50;           // lob  locator
  OCI_DTYPE_SNAP	= 51;   	// snapshot descriptor
  OCI_DTYPE_RSET	= 52; 		// result set descriptor
  OCI_DTYPE_PARAM	= 53;  		// a parameter descriptor obtained from ocigparm
  OCI_DTYPE_ROWID	= 54;    	// rowid descriptor
  OCI_DTYPE_COMPLEXOBJECTCOMP=55; 	// complex object retrieval descriptor
  OCI_DTYPE_FILE	= 56;      	// File Lob locator
  OCI_DTYPE_LAST	= 56;    	// last value of a descriptor type

//	Object Ptr Types
  OCI_OTYPE_NAME	= 1;		// object name
  OCI_OTYPE_REF 	= 2; 		// REF to TDO
  OCI_OTYPE_PTR 	= 3;		// PTR to TDO

//	Attribute Types
  OCI_ATTR_FNCODE  		= 1;
  OCI_ATTR_OBJECT   		= 2;
  OCI_ATTR_NONBLOCKING_MODE 	= 3;
  OCI_ATTR_SQLCODE  		= 4;
  OCI_ATTR_ENV  		= 5;
  OCI_ATTR_SERVER 		= 6;
  OCI_ATTR_SESSION 		= 7;
  OCI_ATTR_TRANS   		= 8;
  OCI_ATTR_ROW_COUNT   		= 9;
  OCI_ATTR_SQLFNCODE 		= 10;
  OCI_ATTR_PREFETCH_ROWS	= 11;
  OCI_ATTR_NESTED_PREFETCH_ROWS = 12;
  OCI_ATTR_PREFETCH_MEMORY 	= 13;
  OCI_ATTR_NESTED_PREFETCH_MEMORY=14;
  OCI_ATTR_CHAR_COUNT 		= 15;	// this specifies the bind and define size in characters
  OCI_ATTR_PDSCL    		= 16;
  OCI_ATTR_PDFMT    		= 17;
  OCI_ATTR_PARAM_COUNT  	= 18;
  OCI_ATTR_ROWID    		= 19;
  OCI_ATTR_CHARSET   		= 20;
  OCI_ATTR_NCHAR    		= 21;
  OCI_ATTR_USERNAME  		= 22;	// username attribute
  OCI_ATTR_PASSWORD  		= 23;	// password attribute
  OCI_ATTR_STMT_TYPE    	= 24;	// statement type
  OCI_ATTR_INTERNAL_NAME    	= 25;
  OCI_ATTR_EXTERNAL_NAME    	= 26;
  OCI_ATTR_XID      		= 27;
  OCI_ATTR_TRANS_LOCK  		= 28;
  OCI_ATTR_TRANS_NAME       	= 29;
  OCI_ATTR_HEAPALLOC  		= 30;	// memory allocated on the heap
  OCI_ATTR_CHARSET_ID  		= 31;	// Character Set ID
  OCI_ATTR_CHARSET_FORM  	= 32;   // Character Set Form
  OCI_ATTR_MAXDATA_SIZE  	= 33;   // Maximumsize of data on the server
  OCI_ATTR_CACHE_OPT_SIZE	= 34;   // object cache optimal size
  OCI_ATTR_CACHE_MAX_SIZE	= 35;   // object cache maximum size percentage
  OCI_ATTR_PINOPTION  		= 36;   // object cache default pin option
  OCI_ATTR_ALLOC_DURATION	= 37;   // object cache default allocation duration
  OCI_ATTR_PIN_DURATION		= 38;   // object cache default pin duration
  OCI_ATTR_FDO         		= 39;   // Format Descriptor object attribute
  OCI_ATTR_POSTPROCESSING_CALLBACK=40;  // Callback to process outbind data
  OCI_ATTR_POSTPROCESSING_CONTEXT=41;  	// Callback context to process outbind data
  OCI_ATTR_ROWS_RETURNED	= 42;   // Number of rows returned in current iter - for Bind handles
  OCI_ATTR_FOCBK       		= 43;   // Failover Callback attribute
  OCI_ATTR_IN_V8_MODE  		= 44; 	// is the server/service context in V8 mode
  OCI_ATTR_LOBEMPTY    		= 45;
  OCI_ATTR_SESSLANG    		= 46;	// session language handle

//	Parameter Attribute Types
  OCI_ATTR_UNK     		= 101;	// unknown attribute
  OCI_ATTR_NUM_COLS    		= 102;  // number of columns
  OCI_ATTR_LIST_COLUMNS		= 103;  // parameter of the column list
  OCI_ATTR_RDBA    		= 104;  // DBA of the segment header
  OCI_ATTR_CLUSTERED   		= 105;  // whether the table is clustered
  OCI_ATTR_PARTITIONED     	= 106;  // whether the table is partitioned
  OCI_ATTR_INDEX_ONLY      	= 107;  // whether the table is index only
  OCI_ATTR_LIST_ARGUMENTS  	= 108;  // parameter of the argument list
  OCI_ATTR_LIST_SUBPROGRAMS	= 109;  // parameter of the subprogram list
  OCI_ATTR_REF_TDO         	= 110;  // REF to the type descriptor
  OCI_ATTR_LINK            	= 111;  // the database link name
  OCI_ATTR_MIN             	= 112;  // minimum value
  OCI_ATTR_MAX             	= 113;  // maximum value
  OCI_ATTR_INCR            	= 114;  // increment value
  OCI_ATTR_CACHE           	= 115;  // number of sequence numbers cached
  OCI_ATTR_ORDER           	= 116;  // whether the sequence is ordered
  OCI_ATTR_HW_MARK         	= 117;  // high-water mark
  OCI_ATTR_TYPE_SCHEMA     	= 118;  // type's schema name
  OCI_ATTR_TIMESTAMP       	= 119;  // timestamp of the object
  OCI_ATTR_NUM_ATTRS       	= 120;  // number of sttributes
  OCI_ATTR_NUM_PARAMS      	= 121;  // number of parameters
  OCI_ATTR_OBJID           	= 122;  // object id for a table or view
  OCI_ATTR_PTYPE           	= 123;  // type of info described by
  OCI_ATTR_PARAM           	= 124;  // parameter descriptor
  OCI_ATTR_OVERLOAD_ID     	= 125;  // overload ID for funcs and procs
  OCI_ATTR_TABLESPACE      	= 126;  // table name space
  OCI_ATTR_TDO                 	= 127;  // TDO of a type
  OCI_ATTR_LTYPE               	= 128;  // list type
  OCI_ATTR_PARSE_ERROR_OFFSET	= 129;  // Parse Error offset
  OCI_ATTR_IS_TEMPORARY     	= 130;  // whether table is temporary
  OCI_ATTR_IS_TYPED        	= 131;  // whether table is typed
  OCI_ATTR_DURATION        	= 132;  // duration of temporary table
  OCI_ATTR_IS_INVOKER_RIGHTS	= 133;  // is invoker rights
  OCI_ATTR_OBJ_NAME       	= 134;	// top level schema obj name
  OCI_ATTR_OBJ_SCHEMA     	= 135;  // schema name
  OCI_ATTR_OBJ_ID         	= 136;  // top level schema object id

  OCI_ATTR_DIRPATH_SORTED_INDEX	= 137; 	// index that data is sorted on

     { direct path index maint method (see oci8dp.h) }
  OCI_ATTR_DIRPATH_INDEX_MAINT_METHOD 	= 138;

    { parallel load: db file, initial and next extent sizes }
  OCI_ATTR_DIRPATH_FILE    		= 139;	// DB file to load into
  OCI_ATTR_DIRPATH_STORAGE_INITIAL 	= 140;	// initial extent size
  OCI_ATTR_DIRPATH_STORAGE_NEXT		= 141;	// next extent size

  OCI_ATTR_TRANS_TIMEOUT	= 142;	// transaction timeout
  OCI_ATTR_SERVER_STATUS	= 143; 	// state of the server handle
  OCI_ATTR_STATEMENT  		= 144; 	// statement txt in stmt hdl

{ ----- Temporary attribute value for UCS2 character set ID -------- }
  OCI_UCS2ID			= 1000;	// UCS2 charset ID

{============================== End OCI Attribute Types ====================}

{---------------- Server Handle Attribute Values ---------------------------}

	// OCI_ATTR_SERVER_STATUS
  OCI_SERVER_NOT_CONNECTED	= $00;
  OCI_SERVER_NORMAL      	= $01;

{---------------------------------------------------------------------------}

{------------------------- Supported Namespaces  ---------------------------}
  OCI_SUBSCR_NAMESPACE_ANONYMOUS= 0;	// Anonymous Namespace 
  OCI_SUBSCR_NAMESPACE_AQ  	= 1;	// Advanced Queues
  OCI_SUBSCR_NAMESPACE_MAX   	= 2;	// Max Name Space Number


//	Credential Types
  OCI_CRED_RDBMS 	= 1;		// database username/password
  OCI_CRED_EXT     	= 2;		// externally provided credentials

//	Error Return Values
  OCI_SUCCESS		= 0;    	// maps to SQL_SUCCESS of SAG CLI
  OCI_SUCCESS_WITH_INFO	= 1;            // maps to SQL_SUCCESS_WITH_INFO
  OCI_RESERVED_FOR_INT_USE=200;		//* reserved for internal use 
  OCI_NO_DATA		= 100;          // maps to SQL_NO_DATA
  OCI_ERROR		= -1;           // maps to SQL_ERROR
  OCI_INVALID_HANDLE	= -2;           // maps to SQL_INVALID_HANDLE
  OCI_NEED_DATA		= 99;           // maps to SQL_NEED_DATA
  OCI_STILL_EXECUTING	= -3123;        // OCI would block error
  OCI_CONTINUE		= -24200;       // Continue with the body of the OCI function


//	Parsing Syntax Types
  OCI_V7_SYNTAX		= 2;	// V7 language
  OCI_V8_SYNTAX		= 3;	// V8 language
  OCI_NTV_SYNTAX	= 1;	// Use what so ever is the native lang of server these values must match the values defined in kpul.h

//	Scrollable Cursor Options
  OCI_FETCH_NEXT	= $02;
  OCI_FETCH_FIRST	= $04;
  OCI_FETCH_LAST	= $08;
  OCI_FETCH_PRIOR	= $10;
  OCI_FETCH_ABSOLUTE	= $20;
  OCI_FETCH_RELATIVE	= $40;

//	Bind and Define Options
  OCI_SB2_IND_PTR  	= $01;
  OCI_DATA_AT_EXEC 	= $02;
  OCI_DYNAMIC_FETCH	= $02;
  OCI_PIECEWISE    	= $04;

//	Execution Modes
  OCI_BATCH_MODE       	= $01;
  OCI_EXACT_FETCH      	= $02;
  OCI_KEEP_FETCH_STATE 	= $04;
  OCI_SCROLLABLE_CURSOR	= $08;
  OCI_DESCRIBE_ONLY    	= $10;
  OCI_COMMIT_ON_SUCCESS	= $20;

//	Authentication Modes
  OCI_MIGRATE    	= $0001;	// migratable auth context
  OCI_SYSDBA     	= $0002;	// for SYSDBA authorization
  OCI_SYSOPER    	= $0004;	// for SYSOPER authorization
  OCI_PRELIM_AUTH	= $0008;	// for preliminary authorization

//	Piece Information
  OCI_PARAM_IN		= $01;
  OCI_PARAM_OUT		= $02;

//	Transaction Start Flags
  OCI_TRANS_OLD        	= $00000000;
  OCI_TRANS_NEW        	= $00000001;
  OCI_TRANS_JOIN       	= $00000002;
  OCI_TRANS_RESUME     	= $00000004;
  OCI_TRANS_STARTMASK  	= $000000FF;

  OCI_TRANS_READONLY   	= $00000100;
  OCI_TRANS_READWRITE  	= $00000200;
  OCI_TRANS_SERIALIZABLE= $00000400;
  OCI_TRANS_ISOLMASK   	= $0000FF00;

  OCI_TRANS_LOOSE      	= $00010000;
  OCI_TRANS_TIGHT      	= $00020000;
  OCI_TRANS_TYPEMASK   	= $000F0000;

  OCI_TRANS_NOMIGRATE  	= $00100000;

//	Transaction End Flags
  OCI_TRANS_TWOPHASE   	= $01000000;

//	Object Types
  OCI_OTYPE_UNK        	= 0;
  OCI_OTYPE_TABLE      	= 1;
  OCI_OTYPE_VIEW       	= 2;
  OCI_OTYPE_SYN        	= 3;
  OCI_OTYPE_PROC       	= 4;
  OCI_OTYPE_FUNC       	= 5;
  OCI_OTYPE_PKG        	= 6;
  OCI_OTYPE_STMT       	= 7;

//	Describe Handle Parameter Attributes

// attributes common to Columns and Stored Procs
  OCI_ATTR_DATA_SIZE   	 	= 1;
  OCI_ATTR_DATA_TYPE   	 	= 2;
  OCI_ATTR_DISP_SIZE   	 	= 3;
  OCI_ATTR_NAME        	 	= 4;
  OCI_ATTR_PRECISION   	 	= 5;
  OCI_ATTR_SCALE       	 	= 6;
  OCI_ATTR_IS_NULL     	 	= 7;
  OCI_ATTR_TYPE_NAME   	 	= 8;
  OCI_ATTR_SCHEMA_NAME 	 	= 9;
  OCI_ATTR_SUB_NAME    	 	= 10;
  OCI_ATTR_POSITION    	 	= 11;

// complex object retrieval parameter attributes
  OCI_ATTR_COMPLEXOBJECTCOMP_TYPE	= 50;
  OCI_ATTR_COMPLEXOBJECTCOMP_TYPE_LEVEL	= 51;
  OCI_ATTR_COMPLEXOBJECT_LEVEL		= 52;
  OCI_ATTR_COMPLEXOBJECT_COLL_OUTOFLINE	= 53;

// only Columns
  OCI_ATTR_DISP_NAME   	 	= 100;

// only Stored Procs
  OCI_ATTR_OVERLOAD    	 	= 210;
  OCI_ATTR_LEVEL       	 	= 211;
  OCI_ATTR_HAS_DEFAULT 	 	= 212;
  OCI_ATTR_IOMODE      	 	= 213;
  OCI_ATTR_RADIX       	 	= 214;
  OCI_ATTR_NUM_ARGS    	 	= 215;

// only user-defined Type's
  OCI_ATTR_TYPECODE           	= 216;
  OCI_ATTR_COLLECTION_TYPECODE	= 217;
  OCI_ATTR_VERSION            	= 218;
  OCI_ATTR_IS_INCOMPLETE_TYPE 	= 219;
  OCI_ATTR_IS_SYSTEM_TYPE     	= 220;
  OCI_ATTR_IS_PREDEFINED_TYPE 	= 221;
  OCI_ATTR_IS_TRANSIENT_TYPE  	= 222;
  OCI_ATTR_IS_SYSTEM_GENERATED_TYPE=223;
  OCI_ATTR_HAS_NESTED_TABLE   	= 224;
  OCI_ATTR_HAS_LOB             	= 225;
  OCI_ATTR_HAS_FILE            	= 226;
  OCI_ATTR_COLLECTION_ELEMENT  	= 227;
  OCI_ATTR_NUM_TYPE_ATTRS      	= 228;
  OCI_ATTR_LIST_TYPE_ATTRS     	= 229;
  OCI_ATTR_NUM_TYPE_METHODS    	= 230;
  OCI_ATTR_LIST_TYPE_METHODS   	= 231;
  OCI_ATTR_MAP_METHOD          	= 232;
  OCI_ATTR_ORDER_METHOD        	= 233;

// only collection element
  OCI_ATTR_NUM_ELEMS           	= 234;

// only type methods
  OCI_ATTR_ENCAPSULATION       	= 235;
  OCI_ATTR_IS_SELFISH          	= 236;
  OCI_ATTR_IS_VIRTUAL          	= 237;
  OCI_ATTR_IS_INLINE           	= 238;
  OCI_ATTR_IS_CONSTANT         	= 239;
  OCI_ATTR_HAS_RESULT          	= 240;
  OCI_ATTR_IS_CONSTRUCTOR      	= 241;
  OCI_ATTR_IS_DESTRUCTOR       	= 242;
  OCI_ATTR_IS_OPERATOR         	= 243;
  OCI_ATTR_IS_MAP              	= 244;
  OCI_ATTR_IS_ORDER            	= 245;
  OCI_ATTR_IS_RNDS             	= 246;
  OCI_ATTR_IS_RNPS             	= 247;
  OCI_ATTR_IS_WNDS             	= 248;
  OCI_ATTR_IS_WNPS             	= 249;

//	ocicpw Modes
  OCI_AUTH	= $08;			// Change the password but donot login

//	Other Constants
  OCI_MAX_FNS		= 100;		// max number of OCI Functions
  OCI_SQLSTATE_SIZE	= 5;
  OCI_ERROR_MAXMSG_SIZE = 1024;
  OCI_LOBMAXSIZE	= MINUB4MAXVAL;
  OCI_ROWID_LEN 	= 23;

//	Fail Over Events
  OCI_FO_END          	= $00000001;
  OCI_FO_ABORT        	= $00000002;
  OCI_FO_REAUTH       	= $00000004;
  OCI_FO_BEGIN        	= $00000008;

//	Fail Over Types
  OCI_FO_NONE          	= $00000001;
  OCI_FO_SESSION       	= $00000002;
  OCI_FO_SELECT        	= $00000004;
  OCI_FO_TXNAL         	= $00000008;

//	Function Codes
  OCI_FNCODE_INITIALIZE 	= 1;
  OCI_FNCODE_HANDLEALLOC	= 2;
  OCI_FNCODE_HANDLEFREE 	= 3;
  OCI_FNCODE_DESCRIPTORALLOC	= 4;
  OCI_FNCODE_DESCRIPTORFREE 	= 5;
  OCI_FNCODE_ENVINIT  		= 6;
  OCI_FNCODE_SERVERATTACH  	= 7;
  OCI_FNCODE_SERVERDETACH  	= 8;
  // unused         9
  OCI_FNCODE_SESSIONBEGIN 	= 10;
  OCI_FNCODE_SESSIONEND  	= 11;
  OCI_FNCODE_PASSWORDCHANGE  	= 12;
  OCI_FNCODE_STMTPREPARE  	= 13;
  OCI_FNCODE_STMTBINDBYPOS 	= 14;
  OCI_FNCODE_STMTBINDBYNAME 	= 15;
  // unused        16
  OCI_FNCODE_BINDDYNAMIC  	= 17;
  OCI_FNCODE_BINDOBJECT 	= 18;
  OCI_FNCODE_BINDARRAYOFSTRUCT 	= 20;
  OCI_FNCODE_STMTEXECUTE 	= 21;
  // unused 22, 23
  OCI_FNCODE_DEFINE 		= 24;
  OCI_FNCODE_DEFINEOBJECT 	= 25;
  OCI_FNCODE_DEFINEDYNAMIC  	= 26;
  OCI_FNCODE_DEFINEARRAYOFSTRUCT= 27;
  OCI_FNCODE_STMTFETCH  	= 28;
  OCI_FNCODE_STMTGETBIND  	= 29;
  // 30, 31 unused
  OCI_FNCODE_DESCRIBEANY 	= 32;
  OCI_FNCODE_TRANSSTART 	= 33;
  OCI_FNCODE_TRANSDETACH 	= 34;
  OCI_FNCODE_TRANSCOMMIT 	= 35;
  // 36 unused
  OCI_FNCODE_ERRORGET  		= 37;
  OCI_FNCODE_LOBOPENFILE 	= 38;
  OCI_FNCODE_LOBCLOSEFILE 	= 39;
  OCI_FNCODE_LOBCREATEFILE 	= 40;
  OCI_FNCODE_LOBDELFILE 	= 41;
  OCI_FNCODE_LOBCOPY 		= 42;
  OCI_FNCODE_LOBAPPEND 		= 43;
  OCI_FNCODE_LOBERASE 		= 44;
  OCI_FNCODE_LOBLENGTH	 	= 45;
  OCI_FNCODE_LOBTRIM 		= 46;
  OCI_FNCODE_LOBREAD 		= 47;
  OCI_FNCODE_LOBWRITE 		= 48;
  // 49 unused
  OCI_FNCODE_SVCCTXBREAK	= 50;
  OCI_FNCODE_SERVERVERSION 	= 51;
  // unused 52, 53
  OCI_FNCODE_ATTRGET		= 54;
  OCI_FNCODE_ATTRSET		= 55;
  OCI_FNCODE_PARAMSET		= 56;
  OCI_FNCODE_PARAMGET		= 57;
  OCI_FNCODE_STMTGETPIECEINFO  	= 58;
  OCI_FNCODE_LDATOSVCCTX	= 59;
  OCI_FNCODE_RESULTSETTOSTMT	= 60;
  OCI_FNCODE_STMTSETPIECEINFO  	= 61;
  OCI_FNCODE_TRANSFORGET	= 62;
  OCI_FNCODE_TRANSPREPARE	= 63;
  OCI_FNCODE_TRANSROLLBACK 	= 64;

  OCI_FNCODE_DEFINEBYPOS	= 65;
  OCI_FNCODE_BINDBYPOS		= 66;
  OCI_FNCODE_BINDBYNAME		= 67;

  OCI_FNCODE_LOBASSIGN 		= 68;
  OCI_FNCODE_LOBISEQUAL 	= 69;
  OCI_FNCODE_LOBISINIT 		= 70;
  OCI_FNCODE_LOBLOCATORSIZE 	= 71;
  OCI_FNCODE_LOBCHARSETID 	= 72;
  OCI_FNCODE_LOBCHARSETFORM 	= 73;
  OCI_FNCODE_LOBFILESETNAME 	= 74;
  OCI_FNCODE_LOBFILEGETNAME 	= 75;

  OCI_FNCODE_LOGON		= 76;
  OCI_FNCODE_LOGOFF		= 77;

type
//	Handle Definitions
  OCIEnv           	= Pointer;	// OCI environment handle
  OCIError         	= Pointer;    	// OCI error handle
  OCISvcCtx        	= Pointer; 	// OCI service handle
  OCIStmt          	= Pointer; 	// OCI statement handle
  OCIBind          	= Pointer;      // OCI bind handle
  OCIDefine        	= Pointer;  	// OCI Define handle
  OCIDescribe      	= Pointer;  	// OCI Describe handle
  OCIServer        	= Pointer;  	// OCI Server handle
  OCISession     	= Pointer;     	// OCI Authentication handle
  OCIComplexObject 	= Pointer; 	// OCI COR handle
  OCITrans         	= Pointer; 	// OCI Transaction handle
  OCISecurity      	= Pointer;	// OCI Security handle

  POCIEnv		= ^OCIEnv;
  POCIError		= ^OCIError;
  POCISvcCtx		= ^OCISvcCtx;
  POCIStmt		= ^OCIStmt;
  POCIBind		= ^OCIBind;
  PPOCIBind		= ^POCIBind;
  POCIDefine		= ^OCIDefine;
  POCIDescribe		= ^OCIDescribe;
  POCIServer        	= ^OCIServer;
  POCISession     	= ^OCISession;



//	Descriptor Definitions
  OCISnapshot     	= Pointer;      // OCI snapshot descriptor
  OCIResult       	= Pointer;      // OCI Result Set Descriptor
  OCILobLocator   	= Pointer; 	// OCI Lob Locator descriptor
  OCIParam        	= Pointer;      // OCI PARameter descriptor
  OCIComplexObjectComp	= Pointer;	// OCI COR descriptor
  OCIRowid		= Pointer;	// OCI ROWID descriptor

  POCISnapshot		= ^OCISnapshot;
  POCILobLocator	= ^OCILobLocator;
  POCIResult		= ^OCIResult;
  POCIParam		= ^OCIParam;
  POCIComplexObjectComp	= ^OCIComplexObjectComp;
  POCIRowid		= ^OCIRowid;

// Lob typedefs for Pro*C
  OCIClobLocator	= OCILobLocator;	// OCI Character LOB Locator
  OCIBlobLocator	= OCILobLocator;       	// OCI Binary LOB Locator
  OCIBFileLocator	= OCILobLocator; 	// OCI Binary LOB File Locator

//	Lob-specific Definitions

// ociloff - OCI Lob OFFset
  OCILobOffset 		= ubig_ora;

// ocillen - OCI Lob LENgth (of lob data)
  OCILobLength		= ubig_ora;

// ocilmo - OCI Lob open MOdes
//enum OCILobMode{ OCI_LOBMODE_READONLY = 1 /* read-only */ };
  OCILobMode		= Integer;
const
  OCI_LOBMODE_READONLY 	= 1;

//	FILE open modes
  OCI_FILE_READONLY	= 1;	// readonly mode open for FILE types

//	LOB Buffering Flush Flags
  OCI_LOB_BUFFER_FREE  	= 1;
  OCI_LOB_BUFFER_NOFREE	= 2;

//	OCI Statement Types
  OCI_STMT_SELECT 	= 1;	// select statement
  OCI_STMT_UPDATE 	= 2;	// update statement
  OCI_STMT_DELETE 	= 3;	// delete statement
  OCI_STMT_INSERT 	= 4;	// Insert Statement
  OCI_STMT_CREATE 	= 5;	// create statement
  OCI_STMT_DROP   	= 6;  	// drop statement
  OCI_STMT_ALTER  	= 7; 	// alter statement
  OCI_STMT_BEGIN  	= 8;   	// begin ... (pl/sql statement)
  OCI_STMT_DECLARE	= 9;	// declare .. (pl/sql statement )

//	OCI Parameter Types
  OCI_PTYPE_UNK        	= 0;	// unknown
  OCI_PTYPE_TABLE      	= 1;	// table
  OCI_PTYPE_VIEW       	= 2;	// view
  OCI_PTYPE_PROC       	= 3;	// procedure
  OCI_PTYPE_FUNC       	= 4;	// function
  OCI_PTYPE_PKG	       	= 5;	// package
  OCI_PTYPE_TYPE       	= 6; 	// user-defined type
  OCI_PTYPE_SYN	       	= 7; 	// synonym
  OCI_PTYPE_SEQ	       	= 8; 	// sequence
  OCI_PTYPE_COL	       	= 9; 	// column
  OCI_PTYPE_ARG	       	= 10;	// argument
  OCI_PTYPE_LIST       	= 11;	// list
  OCI_PTYPE_TYPE_ATTR  	= 12; 	// user-defined type's attribute
  OCI_PTYPE_TYPE_COLL  	= 13; 	// collection type's element
  OCI_PTYPE_TYPE_METHOD	= 14;  	// user-defined type's method
  OCI_PTYPE_TYPE_ARG   	= 15;  	// user-defined type method's argument
  OCI_PTYPE_TYPE_RESULT	= 16;   // user-defined type method's result

//	OCI List Types
  OCI_LTYPE_UNK        	= 0;	// unknown   
  OCI_LTYPE_COLUMN     	= 1;	// column list
  OCI_LTYPE_ARG_PROC   	= 2;	// procedure argument list
  OCI_LTYPE_ARG_FUNC   	= 3;	// function argument list
  OCI_LTYPE_SUBPRG     	= 4;	// subprogram list
  OCI_LTYPE_TYPE_ATTR  	= 5;	// type attribute
  OCI_LTYPE_TYPE_METHOD	= 6;	// type method
  OCI_LTYPE_TYPE_ARG_PROC=7;	// type method w/o result argument list
  OCI_LTYPE_TYPE_ARG_FUNC=8;	// type method w/result argument list

{*******************************************************************************
*	ORO.H - ORacle Object Interface for External/Internal/Kernel Clients
*Purpose:
*    This header file contains Oracle object interface definitions which
*    can be included by external user applications, tools, as well as
*    the kernel.  It defines types and constants that are common to all
*    object interface which is being defined in several other header files
*    (e.g., ori.h, ort.h, and orl.h).
*******************************************************************************}
type
// OCIRef - OCI object REFerence
  OCIRef 	= Pointer;
  POCIRef	= ^OCIRef;

// OCIInd -- a variable of this type contains (null) indicator information
  OCIInd	= sb2;
  POCIInd	= ^OCIInd;

const
  OCI_IND_NOTNULL	= OCIInd( 0);	// not NULL
  OCI_IND_NULL		= OCIInd(-1);	// NULL
  OCI_IND_BADNULL	= OCIInd(-2);	// BAD NULL
  OCI_IND_NOTNULLABLE	= OCIInd(-3);	// not NULLable

//	OBJECT CACHE

type

// OCIPinOpt - OCI object Pin Option: enum OCIPinOpt{ /* 0 = uninitialized */ OCI_PIN_DEFAULT = 1, OCI_PIN_ANY = 3, OCI_PIN_RECENT = 4, OCI_PIN_LATEST = 5 };
  OCIPinOpt 	= Integer;
  POCIPinOpt	= ^OCIPinOpt;

const
  OCI_PIN_DEFAULT	= 1;		// default pin option
  OCI_PIN_ANY		= 3;		// pin any copy of the object
  OCI_PIN_RECENT	= 4;		// pin recent copy of the object
  OCI_PIN_LATEST	= 5;		// pin latest copy of the object

type
// OCILockOpt - OCI object LOCK Option: enum OCILockOpt{  /* 0 = uninitialized */ OCI_LOCK_NONE = 1, OCI_LOCK_X = 2 };
  OCILockOpt 	= Integer;
  POCILockOpt	= ^OCILockOpt;

const
  OCI_LOCK_NONE	= 1;			// null (same as no lock)
  OCI_LOCK_X	= 2;			// exclusive lock

type

// OCIMarkOpt - OCI object Mark option: enum OCIMarkOpt{ /* 0 = uninitialized */ OCI_MARK_DEFAULT = 1, OCI_MARK_NONE = OCI_MARK_DEFAULT, OCI_MARK_UPDATE };
  OCIMarkOpt	= Integer;
  POCIMarkOpt	= ^OCIMarkOpt;

const
  OCI_MARK_DEFAULT	= 1;        		// default (the same as OCI_MARK_NONE)
  OCI_MARK_NONE		= OCI_MARK_DEFAULT;	// object has not been modified
  OCI_MARK_UPDATE	= 2;			// object is to be updated

type
// OCIDuration - OCI object duration
  OCIDuration	= ub2;
  POCIDuration	= ^OCIDuration;

const
  OCI_DURATION_BEGIN	= 10;           	// beginning sequence of duration 
  OCI_DURATION_NULL	= OCI_DURATION_BEGIN-1;	// null duration
  OCI_DURATION_DEFAULT	= OCI_DURATION_BEGIN-2;	// default
  OCI_DURATION_NEXT	= OCI_DURATION_BEGIN-3;	// next special duration
  OCI_DURATION_SESSION	= OCI_DURATION_BEGIN;	// the end of user session
  OCI_DURATION_TRANS	= OCI_DURATION_BEGIN+1;	// the end of user transaction

type
// OCIRefreshOpt - OCI cache Refresh Option: enum OCIRefreshOpt{ /* 0 = uninitialized */ OCI_REFRESH_LOADED = 1 };
  OCIRefreshOpt		= Integer;
  POCIRefreshOpt	= ^OCIRefreshOpt;

const
  OCI_REFRESH_LOADED	= 1;		// refresh objects loaded in the transaction

// OCIObjectCopyFlag - Object copy flag
  OCI_OBJECTCOPY_NOREF	= ub1( $01 );

// OCIObjectFreeFlag - Object free flag
  OCI_OBJECTFREE_FORCE	= ub2( $0001 );
  OCI_OBJECTFREE_NONULL	= ub2( $0002 );

// Type manager typecodes
  OCI_TYPECODE_REF     	= SQLT_REF;     // SQL/OTS OBJECT REFERENCE
  OCI_TYPECODE_DATE    	= SQLT_DAT;     // SQL DATE  OTS DATE
  OCI_TYPECODE_SIGNED8 	= 27;     	// SQL SIGNED INTEGER(8)  OTS SINT8
  OCI_TYPECODE_SIGNED16	= 28;    	// SQL SIGNED INTEGER(16)  OTS SINT16
  OCI_TYPECODE_SIGNED32	= 29;    	// SQL SIGNED INTEGER(32)  OTS SINT32
  OCI_TYPECODE_REAL    	= 21;           // SQL REAL  OTS SQL_REAL
  OCI_TYPECODE_DOUBLE  	= 22;  		// SQL DOUBLE PRECISION  OTS SQL_DOUBLE
  OCI_TYPECODE_FLOAT   	= SQLT_FLT;     // SQL FLOAT(P)  OTS FLOAT(P)
  OCI_TYPECODE_NUMBER  	= SQLT_NUM;	// SQL NUMBER(P S)  OTS NUMBER(P S)
  OCI_TYPECODE_DECIMAL 	= SQLT_PDN;	// SQL DECIMAL(P S)  OTS DECIMAL(P S)
  OCI_TYPECODE_UNSIGNED8= SQLT_BIN;	// SQL UNSIGNED INTEGER(8)  OTS UINT8
  OCI_TYPECODE_UNSIGNED16= 25;  	// SQL UNSIGNED INTEGER(16)  OTS UINT16
  OCI_TYPECODE_UNSIGNED32= 26;  	// SQL UNSIGNED INTEGER(32)  OTS UINT32
  OCI_TYPECODE_OCTET   	= 245;          // SQL ???  OTS OCTET
  OCI_TYPECODE_SMALLINT	= 246;          // SQL SMALLINT  OTS SMALLINT
  OCI_TYPECODE_INTEGER 	= SQLT_INT;     // SQL INTEGER  OTS INTEGER
  OCI_TYPECODE_RAW     	= SQLT_LVB;     // SQL RAW(N)  OTS RAW(N)
  OCI_TYPECODE_PTR     	= 32;           // SQL POINTER  OTS POINTER
  OCI_TYPECODE_VARCHAR2	= SQLT_VCS; 	// SQL VARCHAR2(N)  OTS SQL_VARCHAR2(N)
  OCI_TYPECODE_CHAR    	= SQLT_AFC;    	// SQL CHAR(N)  OTS SQL_CHAR(N)
  OCI_TYPECODE_VARCHAR 	= SQLT_CHR; 	// SQL VARCHAR(N)  OTS SQL_VARCHAR(N)
  OCI_TYPECODE_MLSLABEL	= SQLT_LAB;     // OTS MLSLABEL
  OCI_TYPECODE_VARRAY  	= 247;         	// SQL VARRAY  OTS PAGED VARRAY
  OCI_TYPECODE_TABLE   	= 248;          // SQL TABLE  OTS MULTISET
  OCI_TYPECODE_OBJECT  	= SQLT_NTY;  	// SQL/OTS NAMED OBJECT TYPE
  OCI_TYPECODE_NAMEDCOLLECTION= SQLT_NCO;// SQL/OTS NAMED COLLECTION TYPE
  OCI_TYPECODE_BLOB    	= SQLT_BLOB;    // SQL/OTS BINARY LARGE OBJECT
  OCI_TYPECODE_BFILE   	= SQLT_BFILE;   // SQL/OTS BINARY FILE OBJECT
  OCI_TYPECODE_CLOB    	= SQLT_CLOB; 	// SQL/OTS CHARACTER LARGE OBJECT
  OCI_TYPECODE_CFILE   	= SQLT_CFILE; 	// SQL/OTS CHARACTER FILE OBJECT

  OCI_TYPECODE_OTMFIRST	= 228;     	// first Open Type Manager typecode
  OCI_TYPECODE_OTMLAST 	= 320;       	// last OTM typecode
  OCI_TYPECODE_SYSFIRST	= 228;     	// first OTM system type (internal)
  OCI_TYPECODE_SYSLAST 	= 235;      	// last OTM system type (internal)

	// the following are PL/SQL-only internal. They should not be used
//  OCI_TYPECODE_ITABLE  	= SQLT_TAB;	// PLSQL indexed table
//  OCI_TYPECODE_RECORD  	= SQLT_REC;     // PLSQL record
//  OCI_TYPECODE_BOOLEAN 	= SQLT_BOL;     // PLSQL boolean 

type
// The OCITypeCode type is interchangeable with the existing SQLT type which is a ub2
  OCITypeCode	= ub2;

// GET OPTIONS FOR TDO : enum OCITypeGetOpt{ OCI_TYPEGET_HEADER, OCI_TYPEGET_ALL };
  OCITypeGetOpt	= Integer;
  POCITypeGetOpt= ^OCITypeGetOpt;

const
  OCI_TYPEGET_HEADER	= 0;	// load only the header portion of the TDO when getting type
  OCI_TYPEGET_ALL 	= 1;	// load all attribute and method descriptors as well

type
// OCITypeEncap - OCI Encapsulation Level: enum OCITypeEncap{ /* 0 = uninitialized */ OCI_TYPEENCAP_PRIVATE, OCI_TYPEENCAP_PUBLIC };
  OCITypeEncap 	= Integer;
  POCITypeEncap	= ^OCITypeEncap;

const
  OCI_TYPEENCAP_PRIVATE	= 1;	// private: only internally visible
  OCI_TYPEENCAP_PUBLIC	= 2;	// public: visible to both internally and externally

type
// TYPE METHOD FLAGS: enum OCITypeMethodFlag{ ... }
  OCITypeMethodFlag	= Integer;
  POCITypeMethodFlag	= ^OCITypeMethodFlag;

const
  OCI_TYPEMETHOD_INLINE		= $0001;	// inline
  OCI_TYPEMETHOD_CONSTANT	= $0002;	// constant
  OCI_TYPEMETHOD_VIRTUAL	= $0004;       	// virtual
  OCI_TYPEMETHOD_CONSTRUCTOR	= $0008;	// constructor
  OCI_TYPEMETHOD_DESTRUCTOR	= $0010; 	// destructor
  OCI_TYPEMETHOD_OPERATOR 	= $0020;    	// operator
  OCI_TYPEMETHOD_SELFISH	= $0040;    	// selfish method (generic otherwise)
	// OCI_TYPEMETHOD_MAP and OCI_TYPEMETHOD_ORDER are mutually exclusive 
  OCI_TYPEMETHOD_MAP		= $0080;	// map (relative ordering)
  OCI_TYPEMETHOD_ORDER 		= $0100;	// order (relative ordering)

  OCI_TYPEMETHOD_RNDS		= $0200;	// Read no Data State (default)
  OCI_TYPEMETHOD_WNDS		= $0400;	// Write no Data State
  OCI_TYPEMETHOD_RNPS		= $0800;	// Read no Process State
  OCI_TYPEMETHOD_WNPS		= $1000;	// Write no Process State

type
// TYPE PARAMETER MODE: enum OCITypeParamMode{ /* PL/SQL starts this from 0 */ OCI_TYPEPARAM_IN = 0, OCI_TYPEPARAM_OUT, OCI_TYPEPARAM_INOUT, OCI_TYPEPARAM_BYREF };
  OCITypeParamMode	= Integer;
  POCITypeParamMode	= ^OCITypeParamMode;

const
  OCI_TYPEPARAM_IN	= 0;		// in
  OCI_TYPEPARAM_OUT	= 1;		// out
  OCI_TYPEPARAM_INOUT	= 2;		// in-out
  OCI_TYPEPARAM_BYREF	= 3;		// call by reference (implicitly in-out)

//	DEFAULTS

// default binary and decimal precision and scale
  OCI_NUMBER_DEFAULTPREC	= ub1( 0 );            	// no precision specified
  OCI_NUMBER_DEFAULTSCALE	= sb1( MAXSB1MINVAL );	// no binary/decimal scale specified 

// default maximum length for varrays and vstrings (used in sql.bsq)
  OCI_VARRAY_MAXSIZE	= 4000;		// default maximum number of elements for a varray
  OCI_STRING_MAXLEN 	= 4000;		// default maximum length of a vstring

// Special duration for allocating memory only. No instance can be allocated given these durations.
type
  OCICoherency 	= OCIRefreshOpt;
const
  OCI_COHERENCY_NONE 	= OCIRefreshOpt( 2 );
  OCI_COHERENCY_NULL  	= OCIRefreshOpt( 4 );
  OCI_COHERENCY_ALWAYS	= OCIRefreshOpt( 5 );

  
{*******************************************************************************
*	ORT.H - ORacle's external open Type interface to the open type manager (OTM)
*Purpose:
*    The open type manager interface includes dynamic type operations to
*    create, delete, update, and access types.
*******************************************************************************}

type
// OCIType - OCI Type Description Object
  OCIType	= Pointer;
  POCIType	= ^OCIType;

// OCITypeElem - OCI Type Element object
  OCITypeElem	= Pointer;
  POCITypeElem	= ^OCITypeElem;

// OCITypeMethod - OCI Method Description object
  OCITypeMethod	= Pointer;
  POCITypeMethod= ^OCITypeMethod;

  PPChar	= ^PChar;

var
// OCITypeArrayByName - OCI Get array of TYPes by name.
  OCITypeArrayByName:
  	function (var hEnv: OCIEnv; var hError: OCIError; const phSvc: POCISvcCtx; array_len: ub4;
                const arr_schema_name: PPChar; arr_s_length: ub4p;
                const arr_type_name: PPChar; arr_t_length: ub4p;
                const arr_version_name: PPChar; arr_v_length: ub4p;
                pin_duration: OCIDuration; get_option: OCITypeGetOpt; arr_tdo: POCIType ): sword; cdecl;

// OCITypeArrayByRef - OCI Get array of TYPes by REF.
  OCITypeArrayByRef:
  	function (var hEnv: OCIEnv; var hError: OCIError;
        	array_len: ub4;	const arr_type_ref: POCIRef;
                pin_duration: OCIDuration; get_option: OCITypeGetOpt; arr_tdo: POCIType): sword; cdecl;


{*******************************************************************************
*									       *
*	OCIAP.H - Oracle Call Interface - Ansi Prototypes                      *
*									       *
*******************************************************************************}

//	Dynamic Callback Function Pointers
type
  TOCICallbackInBind =
  	function(ictxp: Pdvoid; phBind: POCIBind; iter, index: ub4;
  		bufpp: PPdvoid; alenp: ub4p; piecep: ub1p; indpp: PPdvoid): sb4; cdecl;

  TOCICallbackOutBind =
  	function(octxp: Pdvoid; phBind: POCIBind; iter, index: ub4;
        	bufpp: PPdvoid; alenpp: ub4pp; piecep: ub1p;
                indpp: PPdvoid; rcodepp: ub2pp): sb4; cdecl;

  TOCICallbackDefine =
  	function(octxp: Pdvoid; phDefn: POCIDefine; iter: ub4;
        	var bufp: Pdvoid; var alenp: ub4p; piecep: ub1p;
                indpp: PPdvoid; rcodepp: ub2pp): sb4; cdecl;

  TOCICallbackLobRead =
  	function(ctxp: Pdvoid; const bufp: Pdvoid; len: ub4; piece: ub1): sb4; cdecl;

  TOCICallbackLobWrite =
  	function(ctxp: Pdvoid; bufp: Pdvoid; lenp: ub4p; piece: ub1p): sb4; cdecl;

//	Failover Callback Structure
  TOCICallbackFailover =
  	function(svcctx, envctx, fo_ctx: Pdvoid; fo_type, fo_event: ub4): sb4; cdecl;

  TOCIFocbkStruct = record
    callback_function: TOCICallbackFailover;
    fo_ctx: Pdvoid;
  end;

type
  Tmalocfp = function(ctxp: Pdvoid; size: size_t): Pdvoid; cdecl;
  Tralocfp = function(ctxp: Pdvoid; memptr: Pdvoid; newsize: size_t): Pdvoid; cdecl;
  Tmfreefp = procedure(ctxp: Pdvoid; memptr: Pdvoid ); cdecl;
	// callback types for OCILobRead and OCI LobWrite procedure
  TCBLobRead	= function(ctxp: Pdvoid; const bufp: Pdvoid; len: ub4; piece: ub1): sb4; cdecl;
  TCBLobWrite	= function(ctxp: Pdvoid; bufp: Pdvoid; len: ub4p; piece: ub1p): sb4; cdecl;

var
  OCIInitialize:
  	function (mode: ub4; ctxp: Pdvoid;
        	malocfp: Tmalocfp; ralocfp: Tralocfp; mfreefp: Tmfreefp): sword; cdecl;
  OCITerminate:
   	function (mode: ub4): sword; cdecl;

  OCIHandleAlloc:
  	function (const phParenEnv: Pdvoid; hndlpp: PPdvoid; htype: ub4;
        	xtramem_sz: size_t; usrmemp: PPdvoid): sword; cdecl;

  OCIHandleFree:
  	function (hndlp: Pdvoid; htype: ub4): sword; cdecl;

  OCIDescriptorAlloc:
  	function (const phParenEnv: Pdvoid; descpp: PPdvoid; htype: ub4;
        	xtramem_sz: size_t; usrmemp: PPdvoid): sword; cdecl;

  OCIDescriptorFree:
  	function (descp: Pdvoid; htype: ub4): sword; cdecl;

  OCIEnvCreate:
  	function(var phEnv: POCIEnv; mode: ub4; ctxp: Pdvoid;
        	malocfp: Tmalocfp; ralocfp: Tralocfp; mfreefp: Tmfreefp;
                xtramem_sz: size_t; usrmemp: Pdvoid): sword; cdecl;

  OCIEnvInit:
  	function (var phEnv: POCIEnv; mode: ub4;
        	xtramem_sz: size_t; usrmemp: Pdvoid): sword; cdecl;

  OCIServerAttach:
  	function (phSrv: POCIServer; phError: POCIError;
        	const dblink: PChar; dblink_len: sb4; mode: ub4): sword; cdecl;

  OCIServerDetach:
  	function (phSrv: POCIServer; phError: POCIError; mode: ub4): sword; cdecl;

  OCISessionBegin:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	phUsr: POCISession; credt, mode: ub4): sword; cdecl;

  OCISessionEnd:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	phUsr: POCISession; mode: ub4): sword; cdecl;

  OCILogon:
  	function (phEnv: POCIEnv; phError: POCIError; var svchp: POCISvcCtx;
        	const username: PChar; uname_len: ub4; const password: PChar; passwd_len: ub4;
                const dbname: PChar; dbname_len: ub4): sword; cdecl;

  OCILogoff:
  	function (phSvc: POCISvcCtx; phError: POCIError): sword; cdecl;

  OCIPasswordChange:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	const user_name: PChar; usernm_len: ub4; const opasswd: PChar; opasswd_len: ub4;
                const npasswd: PChar; npasswd_len, mode: ub4): sword; cdecl;

  OCIStmtPrepare:
  	function (phStmt: POCIStmt; phError: POCIError;
        	const stmt: PChar; stmt_len, language, mode: ub4): sword; cdecl;

  OCIBindByPos:
  	function (phStmt: POCIStmt; var phBind: POCIBind; phError: POCIError;
        	position: ub4; valuep: Pdvoid; value_sz: sb4; dty: ub2; indp: Pdvoid;
                alenp, rcodep: ub2p; maxarr_len: ub4; curelep: ub4p; mode: ub4): sword; cdecl;

  OCIBindByName:
  	function (phStmt: POCIStmt; var phBind: POCIBind; phError: POCIError;
        	const placeholder: PChar; placeh_len: sb4; valuep: Pdvoid; value_sz: sb4; dty: ub2; indp: Pdvoid;
                alenp, rcodep: ub2p; maxarr_len: ub4; curelep: ub4p; mode: ub4): sword; cdecl;

  OCIBindObject:
  	function (phBind: POCIBind; phError: POCIError; const otype: POCIType;
        	var pgvp: Pdvoid; pvszsp: ub4; var indp: Pdvoid; indszp: ub4p): sword; cdecl;

  OCIBindDynamic:
  	function (phBind: POCIBind; phError: POCIError;
        	ictxp: Pdvoid; icbfp: TOCICallbackInBind;
                octxp: Pdvoid; ocbfp: TOCICallbackOutBind): sword; cdecl;

  OCIBindArrayOfStruct:
  	function (phBind: POCIBind; phError: POCIError;
        	pvskip, indskip, alskip, rcskip: ub4): sword; cdecl;

  OCIStmtGetPieceInfo:
  	function (phStmt: POCIStmt; phError: POCIError; var hndlp: Pdvoid; typep: ub4p;
        	in_outp: ub1p; iterp, idxp: ub4p; piecep: ub1p): sword; cdecl;

  OCIStmtSetPieceInfo:
  	function (hndlp: Pdvoid; HandleType: ub4; phError: POCIError;
        	const bufp: dvoid; alenp: ub4p; piece: ub1;
                const indp: Pdvoid; rcodep: ub2p): sword; cdecl;

  OCIStmtExecute:
  	function (phSvc: POCISvcCtx; phStmt: POCIStmt; phError: POCIError; iters, rowoff: ub4;
        	const snap_in: POCISnapshot; snap_out: POCISnapshot; mode: ub4): sword; cdecl;

  OCIDefineByPos:
  	function (phStmt: POCIStmt; var phDefine: POCIDefine; phError: POCIError;
  		position: ub4; pValue: dvoid; value_sz: sb4; dty: ub2;
                indp: Pdvoid; rlenp, rcodep: ub2p; mode: ub4): sword; cdecl;

  OCIDefineObject:
  	function (phDefine: POCIDefine; phError: POCIError; const phType: POCIType;
        	var pgvp: Pdvoid; pvszsp: ub4p; var indp: Pdvoid; indszp: ub4p): sword; cdecl;

  OCIDefineDynamic:
  	function (phDefine: POCIDefine; phError: POCIError;
        	octxp: Pdvoid; ocbfp: TOCICallbackDefine): sword; cdecl;

  OCIDefineArrayOfStruct:
  	function (phDefine: POCIDefine; phError: POCIError;
        	pvskip, indskip, rlskip, rcskip: ub4): sword; cdecl;

  OCIStmtFetch:
  	function (phStmt: POCIStmt; phError: POCIError;
        	nrows: ub4; orientation: ub2; mode: ub4): sword; cdecl;

  OCIStmtGetBindInfo:
  	function (phStmt: POCIStmt; phError: POCIError;
        	size, startloc: ub4; found: sb4p; arr_bvn: PPChar; arr_bvnl: ub1p;
                arr_inv: PPChar; arr_inpl, arr_dupl: ub1p; arr_hndl: PPOCIBind): sword; cdecl;

  OCIDescribeAny:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	objptr: Pdvoid;	objnm_len: ub4; objptr_typ, info_level, objtyp: ub1;
                phDsc: POCIDescribe): sword; cdecl;

  OCIParamGet:
  	function (const pHndl: Pdvoid; htype: ub4; phError: POCIError;
        	var pParmd: Pdvoid; pos: ub4): sword; cdecl;

  OCIParamSet:
  	function (pHdl: Pdvoid; htyp: ub4; phError: POCIError;
        	const pDsc: Pdvoid; dtyp, pos: ub4): sword; cdecl;

  OCITransStart:
  	function (phSvc: POCISvcCtx; phError: POCIError; timeout: uword; flags: ub4): sword; cdecl;

  OCITransDetach:
  	function (phSvc: POCISvcCtx; phError: POCIError; flags: ub4): sword; cdecl;

  OCITransCommit:
  	function (phSvc: POCISvcCtx; phError: POCIError; flags: ub4): sword; cdecl;

  OCITransRollback:
  	function (phSvc: POCISvcCtx; phError: POCIError; flags: ub4): sword; cdecl;

  OCITransPrepare:
  	function (phSvc: POCISvcCtx; phError: POCIError; flags: ub4): sword; cdecl;

  OCITransForget:
  	function (phSvc: POCISvcCtx; phError: POCIError; flags: ub4): sword; cdecl;

  OCIErrorGet:
  	function (pHndl: Pdvoid; RecordNo: ub4; SqlState: PChar;
        	var ErrCode: sb4; buf: PChar; bufsiz, typ: ub4): sword; cdecl;

  OCILobAppend:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	dst_locp, src_locp: POCILobLocator): sword; cdecl;

  OCILobAssign:
  	function (phEnv: POCIEnv; phError: POCIError;
        	const src_locp: POCILobLocator; var dst_locp: POCILobLocator): sword; cdecl;

  OCILobCharSetForm:
  	function (phEnv: POCIEnv; phError: POCIError;
        	const locp: POCILobLocator; csfrm: ub1p): sword; cdecl;

  OCILobCharSetId:
  	function (phEnv: POCIEnv; phError: POCIError;
        	const locp: POCILobLocator; csid: ub2p): sword; cdecl;

  OCILobCopy:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	dst_locp, src_locp: OCILobLocator;
                amount, dst_offset, src_offset: ub4): sword; cdecl;

  OCILobDisableBuffering:
  	function (phSvc: POCISvcCtx; phError: POCIError; locp: POCILobLocator): sword; cdecl;

  OCILobEnableBuffering:
  	function (phSvc: POCISvcCtx; phError: POCIError; locp: POCILobLocator): sword; cdecl;

  OCILobErase:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	locp: POCILobLocator; amount: ub4p; offset: ub4): sword; cdecl;

  OCILobFileClose:
  	function (phSvc: POCISvcCtx; phError: POCIError; filep: POCILobLocator): sword; cdecl;

  OCILobFileCloseAll:
  	function (phSvc: POCISvcCtx; phError: POCIError): sword; cdecl;

  OCILobFileExists:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	filep: POCILobLocator; var flag: OraBoolean): sword; cdecl;

  OCILobFileGetName:
  	function (phEnv: POCIEnv; phError: POCIError;
        	const filep: POCILobLocator; dir_alias: PChar; d_length: ub2p;
                filename: PChar; f_length: ub2p): sword; cdecl;

  OCILobFileIsOpen:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	filep: POCILobLocator; var flag: OraBoolean): sword; cdecl;

  OCILobFileOpen:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	filep: POCILobLocator; mode: ub1): sword; cdecl;

  OCILobFileSetName:
  	function (phEnv: POCIEnv; phError: POCIError;
        	var filep: POCILobLocator; const dir_alias: PChar; d_length: ub2;
                const filename: PChar; f_length: ub2): sword; cdecl;

  OCILobFlushBuffer:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	locp: POCILobLocator; flag: ub4): sword; cdecl;

  OCILobGetLength:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	locp: POCILobLocator; lenp: ub4p): sword; cdecl;

  OCILobIsEqual:
  	function (phEnv: POCIEnv;
        	const x, y: POCILobLocator; var is_equal: OraBoolean): sword; cdecl;

  OCILobLoadFromFile:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	dst_locp, src_filep: POCILobLocator;
                amount, dst_offset, src_offset: ub4): sword; cdecl;

  OCILobLocatorIsInit:
  	function (phEnv: POCIEnv; phError: POCIError;
        	const locp: POCILobLocator; var is_initialized: OraBoolean): sword; cdecl;

  OCILobRead:
  	function (phSvc: POCISvcCtx; phError: POCIError; locp: POCILobLocator;
        	amtp: ub4p; offset: ub4; bufp: dvoid; bufl: ub4;
                ctxp: Pdvoid; cbfp: TCBLobRead; csid: ub2; csfrm: ub1): sword; cdecl;

  OCILobTrim:
  	function (phSvc: POCISvcCtx; phError: POCIError;
        	locp: POCILobLocator; newlen: ub4): sword; cdecl;

  OCILobWrite:
  	function (phSvc: POCISvcCtx; phError: POCIError; locp: POCILobLocator;
        	amtp: ub4p; offset: ub4; bufp: dvoid; buflen: ub4; piece: ub1;
                ctxp: Pdvoid; cbfp: TCBLobWrite; csid: ub2; csfrm: ub1): sword; cdecl;

  OCIBreak:
  	function (pHndl: Pdvoid; phError: POCIError): sword; cdecl;

  OCIServerVersion:
  	function (pHndl: Pdvoid; phError: POCIError;
        	buf: PChar; bufsz: ub4; hndltype: ub1): sword; cdecl;

  OCIAttrGet:
  	function (const trgthndlp: Pdvoid; trghndltyp: ub4;
        	attributep: dvoid; sizep: ub4p; attrtype: ub4; phError: POCIError): sword; cdecl;

  OCIAttrSet:
  	function (trgthndlp: Pdvoid; trghndltyp: ub4;
        	attributep: dvoid; size, attrtype: ub4; phError: POCIError): sword; cdecl;

  OCISvcCtxToLda:
  	function (phSvc: POCISvcCtx; phError: POCIError; ldap: PLdaDef): sword; cdecl;

  OCILdaToSvcCtx:
  	function (var phSvc: POCISvcCtx; phError: POCIError; ldap: PLdaDef): sword; cdecl;

  OCIResultSetToStmt:
  	function (phRset: POCIResult; phError: POCIError): sword; cdecl;

  xaoEnv:
  	function (dbname: PChar): POCIEnv; cdecl;

  xaoSvcCtx:
  	function (dbname: PChar): POCISvcCtx; cdecl;

{------------------------- OCI8 END - V8 Oracle Call Interface definitions ------------------}

type
	// Oracle cursor types for OCI7
  TOraLogon	= record
    Lda:	TLdaDef;
    Hda:	THdaDef;
  end;
  POraLogon     = ^TOraLogon;
  POraCursor	= PCdaDef;
  ESDOraError = class(ESDEngineError);

{ TIOraDatabase }
  TOraSrvInfo	= record
    pL: POraLogon;
    pC: POraCursor;
  end;
  POraSrvInfo	= ^TOraSrvInfo;

  TIOraDatabase = class(TISrvDatabase)
  private
    FHandle: PSDCursor;		// POraLogon - pointer to LDA
    FCurHandle: PSDCursor;	// POraCursor- pointer to CDA
    FIsRTrimChar: Boolean;	// whether to trim trailing spaces in the output of CHAR datatype ?
    FIsEnableBCD,
    FIsEnableInts,
    FIsXAConn: Boolean;
    FSharedInfo: PSDHandleRec;	// this structure was shared for other database components,but it must be destroyed by the current one

    procedure Check(Status: TSDEResult);
    function CnvtDBDateTime2DateTimeRec(ADataType: TFieldType; Buffer: PChar; BufSize: Integer): TDateTimeRec;
    procedure AllocHandles;
    procedure FreeHandles;
    procedure FreeSharedInfo;
    procedure SetAutoCommitOption(Value: Boolean);
  protected
    function CnvtDateTime2DBDateTime(ADataType: TFieldType; Value: TDateTime; Buffer: Pointer; BufSize: Integer): Integer;
    function CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean;

    procedure GetStmtResult(const Stmt: string; List: TStrings); virtual;
    function GetClientVersion: LongInt; override;
    function GetServerVersion: LongInt; override;
    function GetVersionString: string; override;

    procedure Commit; override;
    function ExactNumberDataType(FieldType: TFieldType; Precision, Scale: Integer): TFieldType;
    function FieldDataType(ExtDataType: Integer): TFieldType; override;
    function FieldSubType(ExtDataType: Integer): Word; override;
    function GetHandle: PSDHandleRec; override;
    procedure GetStoredProcNames(List: TStrings); override;
    procedure GetTableFieldNames(const TableName: string; List: TStrings); override;
    procedure GetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings); override;
    procedure Logon(const ARemoteDatabase, AUserName, APassword: string); override;
    procedure Logoff(Force: Boolean); override;
    function NativeDataSize(FieldType: TFieldType): Word; override;
    function NativeDataType(FieldType: TFieldType): Integer; override;
    function ParamValue(Value: TSDDatabaseParam): Integer; override;
    function RequiredCnvtFieldType(FieldType: TFieldType): Boolean; override;
    procedure Rollback; override;
    procedure SetDefaultParams; override;
    procedure SetHandle(AHandle: PSDHandleRec); override;
    procedure SetTransIsolation(Value: TSDTransIsolation); override;
    procedure StartTransaction; override;
    function SPDescriptionsAvailable: Boolean; override;
    property IsEnableBCD: Boolean read FIsEnableBCD;
    property IsEnableInts: Boolean read FIsEnableInts;
    property IsRTrimChar: Boolean read FIsRTrimChar;
    property IsXAConn: Boolean read FIsXAConn;    
  public
    constructor Create(ADatabase: TSDDatabase); override;
    destructor Destroy; override;
    class function CreateConnected(ADatabase: TSDDatabase; const ADatabaseName, AUserName, APassword: string): TISrvDatabase; override;
    class function CreateWithHandle(ADatabase: TSDDatabase; const AHandle: PSDCursor): TISrvDatabase; override;
    function CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet; override;
  end;

{ TIOraDataSet }
  TIOraDataSet = class(TISrvDataSet)
  private
    FHandle: PSDCursor;
    FStmt: string;		// for Oracle deferred mode required save statement between calls 'oparse' and 'oexec'
    FParamHandle: PSDCursor;	// pointer to cursor, returned by PL/SQL procedure
    FBoundParams: Boolean;	// SpBindParams method was called
    FIsCursorParamExists: Boolean;
    function GetSrvDatabase: TIOraDatabase;
    procedure Connect;
    procedure CloseParamHandle;
  protected
    procedure Check(Status: TSDEResult);
    procedure CloseResultSet; override;

    function CnvtDateTime2DBDateTime(ADataType: TFieldType; Value: TDateTime; Buffer: Pointer; BufSize: Integer): Integer; override;
    function CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean; override;

    procedure Disconnect(Force: Boolean); override;
    procedure Execute; override;
    function FetchNextRow: Boolean; override;
    procedure GetFieldDescs(Descs: TSDFieldDescList); override;
    function GetHandle: PSDCursor; override;
    procedure SetSelectBuffer; override;
    function ResultSetExists: Boolean; override;
    function ReadBlob(FieldNo: Integer; var BlobData: string): Longint; override;
    function WriteBlob(FieldNo: Integer; const Buffer; Count: LongInt): Longint; override;
    function WriteBlobByName(Name: string; const Buffer; Count: LongInt): Longint; override;
	// Query methods
    procedure QBindParams; override;
    function QGetRowsAffected: Integer; override;
    procedure QPrepareSQL(Value: PChar); override;
    procedure QExecute; override;
    procedure QGetParams;
	// StoredProc methods
    procedure SpBindParams; override;
    procedure SpCreateParamDesc; override;
    procedure SpPrepareProc; override;
    procedure SpExecute; override;
    procedure SpExecProc; override;
    procedure SpGetParams;
    procedure SpGetResults; override;

    property SrvDatabase: TIOraDatabase read GetSrvDatabase;
  public
    constructor Create(ADataSet: TSDDataSet); override;
    destructor Destroy; override;
    procedure FreeBindBuffer; override;
  end;

{ TIOra8Database }
  TOCI8Logon	= record
    phSvc: POCISvcCtx;
    phSrv: POCIServer;
    phUsr: POCISession;
  end;
  POCI8Logon     = ^TOCI8Logon;

  TOra8SrvInfo	= record
    phEnv: POCIEnv;
    phErr: POCIError;
    phSvc: POCISvcCtx;
    phSrv: POCIServer;
    phUsr: POCISession;
  end;
  POra8SrvInfo	= ^TOra8SrvInfo;

  TIOra8Database = class(TIOraDatabase)
  private
    FEnvHandle: POCIEnv;
    FErrHandle: POCIError;
    FHandle: POCI8Logon;		// POCI8Logon - pointer to handles
    FSharedInfo: PSDHandleRec;
    procedure AllocHandle;
    procedure FreeHandles(Force: Boolean);
    procedure FreeSharedInfo;
  protected
    procedure Check(Status: TSDEResult);
    procedure CheckExt(Status: TSDEResult; AStmt: POCIStmt);

    procedure GetStmtResult(const Stmt: string; List: TStrings); override;
    procedure Commit; override;
    function FieldDataType(ExtDataType: Integer): TFieldType; override;
    function FieldSubType(ExtDataType: Integer): Word; override;
    function GetHandle: PSDHandleRec; override;
    procedure Logon(const sRemoteDatabase, sUserName, sPassword: string); override;
    procedure Logoff(Force: Boolean); override;
    function NativeDataSize(FieldType: TFieldType): Word; override;
    function NativeDataType(FieldType: TFieldType): Integer; override;
    function RequiredCnvtFieldType(FieldType: TFieldType): Boolean; override;
    procedure Rollback; override;
    procedure SetDefaultParams; override;
    procedure SetHandle(AHandle: PSDHandleRec); override;
    procedure StartTransaction; override;
  public
    constructor Create(ADatabase: TSDDatabase); override;
    destructor Destroy; override;
    function CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet; override;
    function IsOra8BlobMode: Boolean;
    function IsOra8BlobType(ADataType: TFieldType): Boolean;
    function IsLocFieldSubType(ASubType: Word): Boolean;
    function IsFileLocFieldSubType(ASubType: Word): Boolean;
  end;

{ TIOra8DataSet }
  TIOra8DataSet = class(TISrvDataSet)
  private
    FHandle: POCIStmt;
    FStmt: string;		// for Oracle8 it's need to save statement untill the statement will be executed or data will be fetched
    FParamHandle: POCIStmt;	// pointer to cursor, returned by PL/SQL procedure
    FBoundParams: Boolean;	// SpBindParams method was called

    FUpdatedLobInfo: Pointer;
    function GetphError: POCIError;
    function GetphSvcCtx: POCISvcCtx;
    function GetSrvDatabase: TIOra8Database;
    function GetStmtExecuteMode: ub4;
    procedure Connect;
    procedure CloseParamHandle;
    function IsCursorParamExists: Boolean;
    function IsOra8BlobType(ADataType: TFieldType): Boolean;
    function IsPLSQLBlock: Boolean;
    function IsSelectStmt: Boolean;
    procedure FreeSelectHLob;
    procedure SetUpdatedLobArray(ACols, ARows: Integer);
    procedure WriteLobParams;
  protected
    procedure Check(Status: TSDEResult);
    procedure CheckStmt(Status: TSDEResult);
    procedure CloseResultSet; override;

    function CnvtDateTime2DBDateTime(ADataType: TFieldType; Value: TDateTime; Buffer: Pointer; BufSize: Integer): Integer; override;
    function CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean; override;

    procedure Disconnect(Force: Boolean); override;
    procedure Execute; override;
    function FetchNextRow: Boolean; override;
    procedure GetFieldDescs(Descs: TSDFieldDescList); override;
    function GetHandle: PSDCursor; override;
    function NativeFieldDataSize(Field: TField): Word; override;
    function NativeFieldOffset(FieldNo: Integer): Integer;
    procedure SetSelectBuffer; override;
    function ResultSetExists: Boolean; override;
    function ReadBlob(FieldNo: Integer; var BlobData: string): Longint; override;
    function ReadHLobField(FieldNo: Integer; var BlobData: string): Longint;
    function ReadLobData(phLob: POCILobLocator; var BlobData: string): Longint;
    function WriteBlob(FieldNo: Integer; const Buffer; Count: LongInt): Longint; override;
    function WriteBlobByName(Name: string; const Buffer; Count: LongInt): Longint; override;
    function WriteLobData(phLob: POCILobLocator; const Buffer; Count: LongInt): Longint;
	// Query methods
    procedure QBindParams; override;
    function QGetRowsAffected: Integer; override;
    procedure QPrepareSQL(Value: PChar); override;
    procedure QExecute; override;
    procedure QGetParams;
	// StoredProc methods
    procedure SpBindParams; override;
    procedure SpCreateParamDesc; override;
    procedure SpPrepareProc; override;
    procedure SpExecute; override;
    procedure SpExecProc; override;
    procedure SpGetParams;
    procedure SpGetResults; override;

    property phError: POCIError read GetphError;
    property phSvcCtx: POCISvcCtx read GetphSvcCtx;
    property SrvDatabase: TIOra8Database read GetSrvDatabase;
  public
    constructor Create(ADataSet: TSDDataSet); override;
    destructor Destroy; override;
    procedure FreeBindBuffer; override;
    property UpdatedLobInfo: Pointer read FUpdatedLobInfo;
  end;


const
	// Old library names: Oracle 7.0 - ORANT7.DLL; Oracle 7.1 - ORA7NT.DLL(for Win95), ORANT71.DLL
  DefSqlApiDLL	= 'ORACLIENT9.DLL;ORACLIENT8.DLL;ORA805.DLL;ORA804.DLL;ORA803.DLL;OCI.DLL;ORA73.DLL;ORA72.DLL;OCIW32.DLL';

var
  UsePiecewiseCalls: Boolean;	// LONG columns are processed by piecewise calls (bindps, definps, ogetpi, osetpi)


{*******************************************************************************
		Load/Unload Sql-library
*******************************************************************************}
function LoadSqlLib: LongInt;
procedure FreeSqlLib;

implementation


const
  SqlApiDLLSep	= ';';	// delimiters of API-library names
  QuoteChar	= '"';	// for surroundings of the parameter's name, which can include, for example, spaces

  UnknownOCI	= 0;	// unknown OCI-version

const
  ParamPrefix	= ':';
  ZS	= #$00;
  CR	= #$0D;
  LF	= #$0A;
  SP	= #$20;

resourcestring
  SErrLibLoading 	= 'Error loading library ''%s''';
  SErrLibUnloading	= 'Error unloading library ''%s''';
  SErrFuncNotFound	= 'Function ''%s'' not found in OCI library';
  SErrInvalidHandle	= 'Invalid OCI handle';

var
  hSqlLibModule: THandle;
  SqlLibRefCount: Integer;
  SqlLibLock: TCriticalSection;

  dwLoadedOCI: LongInt;		// version of the loaded OCI-library (HiWord-Major, LoWord-Monor)

  bDetectedOCI72: Boolean;
  bDetectedOCI73: Boolean;


procedure InitOCI7; forward;
procedure DoneOCI7; forward;

procedure InitOCI8; forward;
procedure DoneOCI8; forward;

procedure OraErrorOCI7(lda: TLdaDef; rcode: sb2; peo: LongInt);
var
  E: ESDOraError;
  Msg: array[0..MAXERRMSG] of Char;
begin
  if rcode = 0 then
    Exit;
  oerhms( lda, rcode, @Msg, MAXERRMSG);
  E := ESDOraError.Create(rcode, rcode, Msg, peo);
  raise E;
end;

procedure OraErrorOCI8( AErrHandle: POCIError; rcode: TSDEResult;  AStmt: POCIStmt );
var
  E: ESDOraError;
  Msg: array[0..MAXERRMSG] of Char;
  ErrCode, ErrOffset: Integer;
  peo: sb4;
begin
  case rcode of
    OCI_INVALID_HANDLE:
      begin
        StrPCopy( Msg, SErrInvalidHandle );
        ErrCode := rcode;
      end
{	implement, when it will be necessary
    OCI_STILL_EXECUTE:
    OCI_CONTINUE: 	}
  else
    if Assigned( AErrHandle ) then
      OCIErrorGet( Pdvoid(AErrHandle), 1, nil, ErrCode, Msg, MAXERRMSG, OCI_HTYPE_ERROR )
    else
      OCIErrorGet( Pdvoid(AErrHandle), 1, nil, ErrCode, Msg, MAXERRMSG, OCI_HTYPE_ENV );
  end;

  ErrOffset := -1;
  peo := 0;
  if (AStmt <> nil) and
     (OCIAttrGet( Pdvoid(AStmt), OCI_HTYPE_STMT, @peo, nil, OCI_ATTR_PARSE_ERROR_OFFSET, AErrHandle ) = OCI_SUCCESS)
  then
    ErrOffset := peo;

  E := ESDOraError.Create(ErrCode, ErrCode, Msg, ErrOffset);
  raise E;
end;

function OCI8ErrorCode( AErrHandle: POCIError; rcode: TSDEResult): Integer;
var
  Msg: array[0..MAXERRMSG] of Char;
  ErrCode: Integer;
begin
  case rcode of
    OCI_INVALID_HANDLE:
      begin
        StrPCopy( Msg, SErrInvalidHandle );
        ErrCode := rcode;
      end
{	implement, when it will be necessary
    OCI_STILL_EXECUTE:
    OCI_CONTINUE: 	}
  else
    if Assigned( AErrHandle ) then
      OCIErrorGet( Pdvoid(AErrHandle), 1, nil, ErrCode, Msg, MAXERRMSG, OCI_HTYPE_ERROR )
    else
      OCIErrorGet( Pdvoid(AErrHandle), 1, nil, ErrCode, Msg, MAXERRMSG, OCI_HTYPE_ENV );
  end;

  Result := ErrCode;
end;

function DetectedOCI72: Boolean;
begin
  Result := bDetectedOCI72;
end;

function DetectedOCI73: Boolean;
begin
  Result := bDetectedOCI73;
end;

function IsLoadedOCI7: Boolean;
begin
  Result := @oexec <> nil;
end;

function IsLoadedOCI8: Boolean;
begin
  Result := @OCIStmtExecute <> nil;
end;

// For example:	name/password@service_name, if password is empty, then OS authentication is used (username is ignored)
function CreateConnectString(const sDatabase, sUserName, sPassword: string): string;
begin
  if Length(sPassword) > 0 then begin
    if Length(sUserName) > 0 then
      Result := Trim(sUserName);
    Result := Result + '/' + Trim(sPassword);
  end;
  if Length(sDatabase) > 0 then
    Result := Result + '@' + Trim(sDatabase);
end;

function CreateCursorString(const sDatabase, sUserName, sPassword: string): string;
begin
  if Length(sUserName) > 0 then
    Result := Trim(sUserName);
  if Length(sPassword) > 0 then
    Result := Result + '/' + Trim(sPassword);
end;

// remove invalid symbols from statement
//and change parameter's name from Params[i].Name to number (:i+1), for example :p1 -> :1
function MakeValidStatement( AStmt: string; AQuery: TSDQuery ): string;
var
  i, ParamPos: Integer;
  sStmt, sFullParamName: string;
begin
  sStmt := AStmt;
	// remove the symbols #$00, #$0A, #$0D in the end of statement
  for i:=Length(sStmt) downto 1 do
    if sStmt[i] in [ZS, LF, CR]
    then sStmt[i] := ZS
    else Break;
	// remove CR(#$0D). It's necessary for PL/SQL blocks (error: PLS-00103) (for example: begin <CRLF> NULL; <CRLF> end;)
  i := 1;
  while i <= Length(sStmt) do begin
    if sStmt[i] in [CR] then
      System.Delete(sStmt, i, 1)
    else
      Inc(i);
  end;
	// change parameter's name from Params[i].Name to number (:i+1), for example :p1 -> :1
  with AQuery do
    for i:=0 to AQuery.ParamCount-1 do begin
      sFullParamName := ParamPrefix + Params[i].Name;
      ParamPos := AnsiTextPos( sFullParamName, sStmt );
      	// if parameter's name is quoted
      if ParamPos = 0 then begin
        sFullParamName := ParamPrefix + QuoteChar + Params[i].Name + QuoteChar;
        ParamPos := AnsiTextPos( sFullParamName, sStmt );
        if ParamPos = 0 then
          Continue;
      end;
      	// change, for example, :Param1 -> :1
      System.Delete( sStmt, ParamPos, Length(sFullParamName) );
      System.Insert( ParamPrefix + IntToStr(i+1), sStmt, ParamPos );
    end;

  Result := sStmt;
end;

// Create PL/SQL block for processing of a stored procedure
function MakeStoredProcStmt( AStoredProc: TSDStoredProc ): string;
var
  i: Integer;
  sParams, sResult: string;
begin
  sResult := '';
  sParams := '';
  if AStoredProc.Params.Count > 0 then with AStoredProc do begin
    	// add parameters
    for i:=0 to Params.Count-1 do begin
  	// if this stored function
      if Params[i].ParamType = ptResult then begin
        sResult := Format(':%s := ', [Params[i].Name]);
        Continue;
      end;
      if Length(sParams) > 0 then
        sParams := sParams + ', ';
      sParams := sParams + Format('%s => :%0:s', [AnsiUpperCase(Params[i].Name)]);
    end;
	// if parameters of stored procedure exists, that adding '('
    if Length(sParams) > 0 then
      sParams := '(' + sParams + ')';
  end;
	// for example, 'begin :Result = TESTPROC(P1 => :P1, P2 => :P2); end;'
        // 		or 'begin :Result = TESTPROC(:P1, :P2); end;'

  Result := Format('begin %s%s %s; end;', [sResult, AnsiUpperCase(AStoredProc.StoredProcName), sParams]);
end;

// Returns a real length of string data (GetDataSize includes '\0' symbol)
//It is not necessery to include '\0' for long data
function RealParamDataSize(AParam: TSDHelperParam): Integer;
begin
  Result := AParam.GetDataSize;
  if AParam.DataType in [ftString {$IFDEF SD_VCL4}, ftFixedChar {$ENDIF}, ftMemo] then
    Dec(Result);
end;


// for OCI8 callback functions
var
  null_ind: sb2 = -1;

type
  TBindLobInfo = record
    phBind: POCIBind;
    ParamIdx: Integer;
  end;
  TBindLobArray	= array [0..0] of TBindLobInfo;
  PBindLobArray	= ^TBindLobArray;
  TLobInfo = record
    phLoc: POCILobLocator;
    Len: ub4;
    ind: sb2;
    rcode: ub2;
  end;
  PLobInfo	= ^TLobInfo;
  PLobArray	= ^TLobArray;
  TLobArray	= array[0..0, 0..0] of TLobInfo;
  TOutLocStruct = record
    ColCount,
    RowCount: Integer;
    Row: PLobArray;
    BindInfo: PBindLobArray; 	// index in Params
  end;


// IN-bind callback function, which provides nothing
function cbf_no_data(ictxp: Pdvoid; phBind: POCIBind; iter, index: ub4;
  		bufpp: PPdvoid; alenp: ub4p; piecep: ub1p; indpp: PPdvoid): sb4; cdecl;
begin
  bufpp^	:= nil;
  alenp^	:= 0;
  Null_ind 	:= OCI_IND_NULL;
  indpp^	:= @Null_ind;
  piecep^ 	:= OCI_ONE_PIECE;

  Result := OCI_CONTINUE;
end;

{ OUT-bind callback function, which provides buffers for LOB locators.
 It's necessary to bind a DML statement with RETURNING clause } 
function cbf_get_lob(octxp: Pdvoid; phBind: POCIBind; iter, index: ub4;
        	bufpp: PPdvoid; alenpp: ub4pp; piecep: ub1p;
                indpp: PPdvoid; rcodepp: ub2pp): sb4; cdecl;
var
  RetRows: ub4;
  i, ParamNo: Integer;
begin
  with TIOra8DataSet(octxp) do begin
    if index = 0 then begin
      Check( OCIAttrGet( Pdvoid(phBind), OCI_HTYPE_BIND, @RetRows, nil, OCI_ATTR_ROWS_RETURNED, phError) );

      SetUpdatedLobArray( TOutLocStruct(UpdatedLobInfo^).ColCount, RetRows );
    end;

    with TOutLocStruct(UpdatedLobInfo^) do begin
      ParamNo := -1;
      for i:=0 to  TOutLocStruct(UpdatedLobInfo^).ColCount-1 do
        if BindInfo[i].phBind = phBind then begin
          ParamNo := i;
          Break;
        end;
      ASSERT( ParamNo >= 0 );

      Row[index, ParamNo].Len := SizeOf(OCILobLocator);
      bufpp^ 	:= Pointer( Row[index, ParamNo].phLoc );
      alenpp^	:= @Row[index, ParamNo].Len;

      piecep^ 	:= OCI_ONE_PIECE;
    end;

  end;

  Result := OCI_CONTINUE;
end;

{ Pack version number in 4 byte (DOUBLE WORD) }
function MakeVer(MajorVer, MinorVer: Integer): LongInt;
begin
  Result := MakeLong( MinorVer, MajorVer );
end;


(*******************************************************************************
			Load/Unload Sql-library
********************************************************************************)
function LoadSqlLib: LongInt;
  function GetOracleHome: string;
  const
    OraHomeKey	= 'SOFTWARE\ORACLE';
    OraHomeVal	= 'ORACLE_HOME';
  var
    rg: TRegistry;
  begin
    rg := TRegistry.Create;
    try
      rg.RootKey := HKEY_LOCAL_MACHINE;
{$IFDEF SD_VCL4}
      if rg.OpenKeyReadOnly(OraHomeKey) then begin
{$ELSE}
      if rg.OpenKey(OraHomeKey, False) then begin
{$ENDIF}
        Result := rg.ReadString(OraHomeVal);
        rg.CloseKey;        
      end;
    finally
      rg.Free;
    end;
  end;

  function DetectVersion(const ALibName: string): LongInt;
  begin
    if (ALibName = 'ORA805.DLL') or
       (ALibName = 'ORA804.DLL') or
       (ALibName = 'ORA803.DLL') or
       (ALibName = 'OCI.DLL')		// this library can have different version 8.x-9.x
    then
      Result := MakeVer(8,0)
    else if (ALibName = 'ORACLIENT9.DLL') then
      Result := MakeVer(9, 0)
    else if (ALibName = 'ORACLIENT8.DLL') then
      Result := MakeVer(8, 1)
    else if ALibName = 'ORA73.DLL' then
      Result := MakeVer(7, 3)
    else if ALibName = 'ORA72.DLL' then
      Result := MakeVer(7, 2)
    else if ALibName = 'OCIW32.DLL' then
      Result := MakeVer(7, 3)		// or more
    else
      Result := UnknownOCI;
  end;

var
  sLibName, sSqlApiDLL, sOraBinDir: string;
  CurPos: Integer;
begin
  Result := 0;
  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 0) then begin
      sSqlApiDLL := SrvApiDLLs[stOracle];
      sOraBinDir := GetOracleHome;
      if Trim(sOraBinDir) <> '' then
        sOraBinDir := sOraBinDir + '\BIN\';

       	// first it's need to looking in global path, which is set by Oracle Home Selector
      CurPos := 1;
      while (CurPos <= Length(sSqlApiDLL)) and (hSqlLibModule = 0) do begin
        sLibName := ExtractLibName(sSqlApiDLL, SqlApiDLLSep, CurPos);
        hSqlLibModule := LoadLibrary( PChar(sLibName) );
      end;
       	// then look in ORANT\BIN or ORA95\BIN directory
      CurPos := 1;
      if Trim(sOraBinDir) <> '' then
        while (CurPos <= Length(sSqlApiDLL)) and (hSqlLibModule = 0) do begin
          sLibName := ExtractLibName(sSqlApiDLL, SqlApiDLLSep, CurPos);
          hSqlLibModule := LoadLibrary( PChar(sOraBinDir+sLibName) );
        end;

      if (hSqlLibModule = 0) then
        raise Exception.CreateFmt(SErrLibLoading, [sSqlApiDLL]);
      Inc(SqlLibRefCount);
	// detect OCI-version
      dwLoadedOCI := DetectVersion( sLibName );

    end else
      Inc(SqlLibRefCount);

    if (not ForceOCI7) and (HiWord(dwLoadedOCI) >= 8) then begin
      if not IsLoadedOCI8 then
        InitOCI8;
    end else begin
      if not IsLoadedOCI7 then
        InitOCI7;
    end;

    Result := dwLoadedOCI;
  finally
    SqlLibLock.Release;
  end;
end;

procedure FreeSqlLib;
begin
  if SqlLibRefCount = 0 then
    Exit;
    
  SqlLibLock.Acquire;
  try
    if SqlLibRefCount = 1 then begin
      if FreeLibrary(hSqlLibModule) then
        hSqlLibModule := 0
      else
        raise Exception.CreateFmt(SErrLibUnloading, [ SrvApiDLLs[stOracle] ]);
      Dec(SqlLibRefCount);

      DoneOCI7;
      DoneOCI8;
      dwLoadedOCI := 0;
    end else
      Dec(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;

procedure InitOCI7;
begin
  @obindps      := GetProcAddress(hSqlLibModule, 'obindps');	ASSERT( (dwLoadedOCI < MakeVer(7, 3)) or (@obindps<>nil),	Format(SErrFuncNotFound, ['obindps<']) );
  @obreak      	:= GetProcAddress(hSqlLibModule, 'obreak');  	ASSERT( @obreak<>nil,	Format(SErrFuncNotFound, ['obreak']) );
  @ocan        	:= GetProcAddress(hSqlLibModule, 'ocan');  	ASSERT( @ocan<>nil,	Format(SErrFuncNotFound, ['ocan']) );
  @oclose	:= GetProcAddress(hSqlLibModule, 'oclose');  	ASSERT( @oclose<>nil,	Format(SErrFuncNotFound, ['oclose']) );
  @ocof		:= GetProcAddress(hSqlLibModule, 'ocof');  	ASSERT( @ocof<>nil,	Format(SErrFuncNotFound, ['ocof']) );
  @ocom		:= GetProcAddress(hSqlLibModule, 'ocom');  	ASSERT( @ocom<>nil,	Format(SErrFuncNotFound, ['ocom']) );
  @ocon 	:= GetProcAddress(hSqlLibModule, 'ocon');  	ASSERT( @ocon<>nil,	Format(SErrFuncNotFound, ['ocon']) );
  @odefinps	:= GetProcAddress(hSqlLibModule, 'odefinps');  	ASSERT( (dwLoadedOCI < MakeVer(7, 3)) or (@odefinps<>nil),Format(SErrFuncNotFound, ['odefinps']) );
  @odessp  	:= GetProcAddress(hSqlLibModule, 'odessp');  	ASSERT( @odessp<>nil,	Format(SErrFuncNotFound, ['odessp']) );
  @odescr  	:= GetProcAddress(hSqlLibModule, 'odescr');  	ASSERT( @odescr<>nil,	Format(SErrFuncNotFound, ['odescr']) );
  @oerhms  	:= GetProcAddress(hSqlLibModule, 'oerhms');  	ASSERT( @oerhms<>nil,	Format(SErrFuncNotFound, ['oerhms']) );
  @oermsg  	:= GetProcAddress(hSqlLibModule, 'oermsg');  	ASSERT( @oermsg<>nil,	Format(SErrFuncNotFound, ['oermsg']) );
  @oexec   	:= GetProcAddress(hSqlLibModule, 'oexec');  	ASSERT( @oexec<>nil,	Format(SErrFuncNotFound, ['oexec']) );
  @oexfet  	:= GetProcAddress(hSqlLibModule, 'oexfet');  	ASSERT( @oexfet<>nil,	Format(SErrFuncNotFound, ['oexfet']) );
  @oexn    	:= GetProcAddress(hSqlLibModule, 'oexn');  	ASSERT( @oexn<>nil,	Format(SErrFuncNotFound, ['oexn']) );
  @ofen    	:= GetProcAddress(hSqlLibModule, 'ofen');  	ASSERT( @ofen<>nil,	Format(SErrFuncNotFound, ['ofen']) );
  @ofetch  	:= GetProcAddress(hSqlLibModule, 'ofetch');  	ASSERT( @ofetch<>nil,	Format(SErrFuncNotFound, ['ofetch']) );
  @oflng   	:= GetProcAddress(hSqlLibModule, 'oflng');  	ASSERT( @oflng<>nil,	Format(SErrFuncNotFound, ['oflng']) );
  @ogetpi  	:= GetProcAddress(hSqlLibModule, 'ogetpi');  	ASSERT( (dwLoadedOCI < MakeVer(7, 3)) or (@ogetpi<>nil),	Format(SErrFuncNotFound, ['ogetpi']) );
  @oopt    	:= GetProcAddress(hSqlLibModule, 'oopt');  	ASSERT( @oopt<>nil,	Format(SErrFuncNotFound, ['oopt']) );
  @opinit  	:= GetProcAddress(hSqlLibModule, 'opinit');  	ASSERT( (dwLoadedOCI < MakeVer(7, 3)) or (@opinit<>nil),	Format(SErrFuncNotFound, ['opinit']) );
  @olog    	:= GetProcAddress(hSqlLibModule, 'olog');  	ASSERT( @olog<>nil,	Format(SErrFuncNotFound, ['olog']) );
  @ologof  	:= GetProcAddress(hSqlLibModule, 'ologof');  	ASSERT( @ologof<>nil,	Format(SErrFuncNotFound, ['ologof']) );
  @oopen   	:= GetProcAddress(hSqlLibModule, 'oopen');  	ASSERT( @oopen<>nil,	Format(SErrFuncNotFound, ['oopen']) );
  @oparse  	:= GetProcAddress(hSqlLibModule, 'oparse');  	ASSERT( @oparse<>nil,	Format(SErrFuncNotFound, ['oparse']) );
  @orol    	:= GetProcAddress(hSqlLibModule, 'orol');  	ASSERT( @orol<>nil,	Format(SErrFuncNotFound, ['orol']) );
  @osetpi  	:= GetProcAddress(hSqlLibModule, 'osetpi');  	ASSERT( (dwLoadedOCI < MakeVer(7, 3)) or (@osetpi<>nil),	Format(SErrFuncNotFound, ['osetpi']) );
  @onbset  	:= GetProcAddress(hSqlLibModule, 'onbset');  	ASSERT( @onbset<>nil,	Format(SErrFuncNotFound, ['onbset']) );
  @onbtst  	:= GetProcAddress(hSqlLibModule, 'onbtst');  	ASSERT( @onbtst<>nil,	Format(SErrFuncNotFound, ['onbtst']) );
  @onbclr  	:= GetProcAddress(hSqlLibModule, 'onbclr');  	ASSERT( @onbclr<>nil,	Format(SErrFuncNotFound, ['onbclr']) );
//  @ognfd   	:= GetProcAddress(hSqlLibModule, 'ognfd');  	ASSERT( @ognfd<>nil,	Format(SErrFuncNotFound, ['ognfd']) );
	{ OBSOLETE CALLS }
  @obndra	:= GetProcAddress(hSqlLibModule, 'obndra');  	ASSERT( @obndra<>nil,	Format(SErrFuncNotFound, ['obndra']) );
  @obndrn	:= GetProcAddress(hSqlLibModule, 'obndrn');  	ASSERT( @obndrn<>nil,	Format(SErrFuncNotFound, ['obndrn']) );
  @obndrv	:= GetProcAddress(hSqlLibModule, 'obndrv');  	ASSERT( @obndrv<>nil,	Format(SErrFuncNotFound, ['obndrv']) );
  @odefin	:= GetProcAddress(hSqlLibModule, 'odefin');  	ASSERT( @odefin<>nil,	Format(SErrFuncNotFound, ['odefin']) );

  bDetectedOCI72 := Assigned(@onbclr) and Assigned(@onbset) and Assigned(@onbtst);
  bDetectedOCI73 := Assigned(@obindps) and Assigned(@odefinps) and
    		Assigned(@opinit) and Assigned(@ogetpi) and Assigned(@osetpi);
	// init a threadsafe mode
  if bDetectedOCI73 then
    opinit(OCI_EV_TSF);
end;

procedure DoneOCI7;
begin
  @obindps      := nil;
  @obreak      	:= nil;
  @ocan        	:= nil;
  @oclose	:= nil;
  @ocof		:= nil;
  @ocom		:= nil;
  @ocon 	:= nil;
  @odefinps	:= nil;
  @odessp  	:= nil;
  @odescr  	:= nil;
  @oerhms  	:= nil;
  @oermsg  	:= nil;
  @oexec   	:= nil;
  @oexfet  	:= nil;
  @oexn    	:= nil;
  @ofen    	:= nil;
  @ofetch  	:= nil;
  @oflng   	:= nil;
  @ogetpi  	:= nil;
  @oopt    	:= nil;
  @opinit  	:= nil;
  @olog    	:= nil;
  @ologof  	:= nil;
  @oopen   	:= nil;
  @oparse  	:= nil;
  @orol    	:= nil;
  @osetpi  	:= nil;
  @onbset  	:= nil;
  @onbtst  	:= nil;
  @onbclr  	:= nil;
//  @ognfd   	:= nil;
	{ OBSOLETE CALLS }
  @obndra	:= nil;
  @obndrn	:= nil;
  @obndrv	:= nil;
  @odefin	:= nil;

  bDetectedOCI72 := False;
  bDetectedOCI73 := False;
end;

procedure InitOCI8;
begin
  @OCIInitialize	:= GetProcAddress(hSqlLibModule, 'OCIInitialize');  	ASSERT( @OCIInitialize		<>nil,	Format(SErrFuncNotFound, ['OCIInitialize']) );
  @OCITerminate		:= GetProcAddress(hSqlLibModule, 'OCIInitialize');  	ASSERT( @OCIInitialize		<>nil,	Format(SErrFuncNotFound, ['OCITerminate']) );
  @OCIHandleAlloc	:= GetProcAddress(hSqlLibModule, 'OCIHandleAlloc');  	ASSERT( @OCIHandleAlloc		<>nil,	Format(SErrFuncNotFound, ['OCIHandleAlloc']) );
  @OCIHandleFree	:= GetProcAddress(hSqlLibModule, 'OCIHandleFree');  	ASSERT( @OCIHandleFree		<>nil,	Format(SErrFuncNotFound, ['OCIHandleFree']) );
  @OCIDescriptorAlloc	:= GetProcAddress(hSqlLibModule, 'OCIDescriptorAlloc'); ASSERT( @OCIDescriptorAlloc	<>nil,	Format(SErrFuncNotFound, ['OCIDescriptorAlloc']) );
  @OCIDescriptorFree	:= GetProcAddress(hSqlLibModule, 'OCIDescriptorFree');  ASSERT( @OCIDescriptorFree	<>nil,	Format(SErrFuncNotFound, ['OCIDescriptorFree']) );
	// OCIEnvCreate was implemented at OCI v8.1.5
  @OCIEnvCreate		:= GetProcAddress(hSqlLibModule, 'OCIEnvCreate');
  @OCIEnvInit		:= GetProcAddress(hSqlLibModule, 'OCIEnvInit');  	ASSERT( @OCIEnvInit		<>nil,	Format(SErrFuncNotFound, ['OCIEnvInit']) );
  @OCIServerAttach	:= GetProcAddress(hSqlLibModule, 'OCIServerAttach');  	ASSERT( @OCIServerAttach	<>nil,	Format(SErrFuncNotFound, ['OCIServerAttach']) );
  @OCIServerDetach	:= GetProcAddress(hSqlLibModule, 'OCIServerDetach');  	ASSERT( @OCIServerDetach	<>nil,	Format(SErrFuncNotFound, ['OCIServerDetach']) );
  @OCISessionBegin	:= GetProcAddress(hSqlLibModule, 'OCISessionBegin');  	ASSERT( @OCISessionBegin	<>nil,	Format(SErrFuncNotFound, ['OCISessionBegin']) );
  @OCISessionEnd	:= GetProcAddress(hSqlLibModule, 'OCISessionEnd');	ASSERT( @OCISessionEnd		<>nil,	Format(SErrFuncNotFound, ['OCISessionEnd']) );
  @OCILogon		:= GetProcAddress(hSqlLibModule, 'OCILogon');  		ASSERT( @OCILogon		<>nil,	Format(SErrFuncNotFound, ['OCILogon']) );
  @OCILogoff		:= GetProcAddress(hSqlLibModule, 'OCILogoff');  	ASSERT( @OCILogoff		<>nil,	Format(SErrFuncNotFound, ['OCILogoff']) );
  @OCIPasswordChange	:= GetProcAddress(hSqlLibModule, 'OCIPasswordChange');  ASSERT( @OCIPasswordChange	<>nil,	Format(SErrFuncNotFound, ['OCIPasswordChange']) );
  @OCIStmtPrepare	:= GetProcAddress(hSqlLibModule, 'OCIStmtPrepare');  	ASSERT( @OCIStmtPrepare		<>nil,	Format(SErrFuncNotFound, ['OCIStmtPrepare']) );
  @OCIBindByPos		:= GetProcAddress(hSqlLibModule, 'OCIBindByPos');  	ASSERT( @OCIBindByPos  		<>nil,	Format(SErrFuncNotFound, ['OCIBindByPos']) );
  @OCIBindByName	:= GetProcAddress(hSqlLibModule, 'OCIBindByName');  	ASSERT( @OCIBindByName 		<>nil,	Format(SErrFuncNotFound, ['OCIBindByName']) );
  @OCIBindObject	:= GetProcAddress(hSqlLibModule, 'OCIBindObject');	ASSERT( @OCIBindObject 		<>nil,	Format(SErrFuncNotFound, ['OCIBindObject']) );
  @OCIBindDynamic	:= GetProcAddress(hSqlLibModule, 'OCIBindDynamic');  	ASSERT( @OCIBindDynamic		<>nil,	Format(SErrFuncNotFound, ['OCIBindDynamic']) );
  @OCIBindArrayOfStruct	:= GetProcAddress(hSqlLibModule, 'OCIBindArrayOfStruct');ASSERT( @OCIBindArrayOfStruct	<>nil,	Format(SErrFuncNotFound, ['OCIBindArrayOfStruct']) );
  @OCIStmtGetPieceInfo	:= GetProcAddress(hSqlLibModule, 'OCIStmtGetPieceInfo');ASSERT( @OCIStmtGetPieceInfo	<>nil,	Format(SErrFuncNotFound, ['OCIStmtGetPieceInfo']) );
  @OCIStmtSetPieceInfo	:= GetProcAddress(hSqlLibModule, 'OCIStmtSetPieceInfo');ASSERT( @OCIStmtSetPieceInfo	<>nil,	Format(SErrFuncNotFound, ['OCIStmtSetPieceInfo']) );
  @OCIStmtExecute	:= GetProcAddress(hSqlLibModule, 'OCIStmtExecute');  	ASSERT( @OCIStmtExecute		<>nil,	Format(SErrFuncNotFound, ['OCIStmtExecute']) );
  @OCIDefineByPos	:= GetProcAddress(hSqlLibModule, 'OCIDefineByPos');  	ASSERT( @OCIDefineByPos		<>nil,	Format(SErrFuncNotFound, ['OCIDefineByPos']) );
  @OCIDefineObject	:= GetProcAddress(hSqlLibModule, 'OCIDefineObject');  	ASSERT( @OCIDefineObject	<>nil,	Format(SErrFuncNotFound, ['OCIDefineObject']) );
  @OCIDefineDynamic	:= GetProcAddress(hSqlLibModule, 'OCIDefineDynamic');  	ASSERT( @OCIDefineDynamic	<>nil,	Format(SErrFuncNotFound, ['OCIDefineDynamic']) );
  @OCIDefineArrayOfStruct:=GetProcAddress(hSqlLibModule, 'OCIDefineArrayOfStruct');ASSERT( @OCIDefineArrayOfStruct<>nil,Format(SErrFuncNotFound, ['OCIDefineArrayOfStruct']) );
  @OCIStmtFetch		:= GetProcAddress(hSqlLibModule, 'OCIStmtFetch');  	ASSERT( @OCIStmtFetch		<>nil,	Format(SErrFuncNotFound, ['OCIStmtFetch']) );
  @OCIStmtGetBindInfo	:= GetProcAddress(hSqlLibModule, 'OCIStmtGetBindInfo'); ASSERT( @OCIStmtGetBindInfo	<>nil,	Format(SErrFuncNotFound, ['OCIStmtGetBindInfo']) );
  @OCIDescribeAny	:= GetProcAddress(hSqlLibModule, 'OCIDescribeAny');  	ASSERT( @OCIDescribeAny		<>nil,	Format(SErrFuncNotFound, ['OCIDescribeAny']) );
  @OCIParamGet		:= GetProcAddress(hSqlLibModule, 'OCIParamGet');  	ASSERT( @OCIParamGet		<>nil,	Format(SErrFuncNotFound, ['OCIParamGet']) );
  @OCIParamSet		:= GetProcAddress(hSqlLibModule, 'OCIParamSet');  	ASSERT( @OCIParamSet		<>nil,	Format(SErrFuncNotFound, ['OCIParamSet']) );
  @OCITransStart	:= GetProcAddress(hSqlLibModule, 'OCITransStart');   	ASSERT( @OCITransStart		<>nil,	Format(SErrFuncNotFound, ['OCITransStart']) );
  @OCITransDetach	:= GetProcAddress(hSqlLibModule, 'OCITransDetach');  	ASSERT( @OCITransDetach		<>nil,	Format(SErrFuncNotFound, ['OCITransDetach']) );
  @OCITransCommit	:= GetProcAddress(hSqlLibModule, 'OCITransCommit');  	ASSERT( @OCITransCommit		<>nil,	Format(SErrFuncNotFound, ['OCITransCommit']) );
  @OCITransRollback	:= GetProcAddress(hSqlLibModule, 'OCITransRollback');  	ASSERT( @OCITransRollback	<>nil,	Format(SErrFuncNotFound, ['OCITransRollback']) );
  @OCITransPrepare	:= GetProcAddress(hSqlLibModule, 'OCITransPrepare');  	ASSERT( @OCITransPrepare	<>nil,	Format(SErrFuncNotFound, ['OCITransPrepare']) );
  @OCITransForget	:= GetProcAddress(hSqlLibModule, 'OCITransForget');  	ASSERT( @OCITransForget		<>nil,	Format(SErrFuncNotFound, ['OCITransForget']) );
  @OCIErrorGet		:= GetProcAddress(hSqlLibModule, 'OCIErrorGet');  	ASSERT( @OCIErrorGet		<>nil,	Format(SErrFuncNotFound, ['OCIErrorGet']) );
  @OCILobAppend		:= GetProcAddress(hSqlLibModule, 'OCILobAppend');  	ASSERT( @OCILobAppend		<>nil,	Format(SErrFuncNotFound, ['OCILobAppend']) );
  @OCILobAssign		:= GetProcAddress(hSqlLibModule, 'OCILobAssign');  	ASSERT( @OCILobAssign		<>nil,	Format(SErrFuncNotFound, ['OCILobAssign']) );
  @OCILobCharSetForm	:= GetProcAddress(hSqlLibModule, 'OCILobCharSetForm');  ASSERT( @OCILobCharSetForm	<>nil,	Format(SErrFuncNotFound, ['OCILobCharSetForm']) );
  @OCILobCharSetId	:= GetProcAddress(hSqlLibModule, 'OCILobCharSetId');  	ASSERT( @OCILobCharSetId	<>nil,	Format(SErrFuncNotFound, ['OCILobCharSetId']) );
  @OCILobCopy		:= GetProcAddress(hSqlLibModule, 'OCILobCopy');  	ASSERT( @OCILobCopy		<>nil,	Format(SErrFuncNotFound, ['OCILobCopy']) );
  @OCILobDisableBuffering:=GetProcAddress(hSqlLibModule, 'OCILobDisableBuffering');ASSERT( @OCILobDisableBuffering<>nil,Format(SErrFuncNotFound, ['OCILobDisableBuffering']) );
  @OCILobEnableBuffering:= GetProcAddress(hSqlLibModule, 'OCILobEnableBuffering');ASSERT( @OCILobEnableBuffering<>nil,	Format(SErrFuncNotFound, ['OCILobEnableBuffering']) );
  @OCILobErase		:= GetProcAddress(hSqlLibModule, 'OCILobErase');  	ASSERT( @OCILobErase		<>nil,	Format(SErrFuncNotFound, ['OCILobErase']) );
  @OCILobFileClose	:= GetProcAddress(hSqlLibModule, 'OCILobFileClose');  	ASSERT( @OCILobFileClose	<>nil,	Format(SErrFuncNotFound, ['OCILobFileClose']) );
  @OCILobFileCloseAll	:= GetProcAddress(hSqlLibModule, 'OCILobFileCloseAll'); ASSERT( @OCILobFileCloseAll	<>nil,	Format(SErrFuncNotFound, ['OCILobFileCloseAll']) );
  @OCILobFileExists	:= GetProcAddress(hSqlLibModule, 'OCILobFileExists');  	ASSERT( @OCILobFileExists	<>nil,	Format(SErrFuncNotFound, ['OCILobFileExists']) );
  @OCILobFileGetName	:= GetProcAddress(hSqlLibModule, 'OCILobFileGetName');  ASSERT( @OCILobFileGetName	<>nil,	Format(SErrFuncNotFound, ['OCILobFileGetName']) );
  @OCILobFileIsOpen	:= GetProcAddress(hSqlLibModule, 'OCILobFileIsOpen');  	ASSERT( @OCILobFileIsOpen	<>nil,	Format(SErrFuncNotFound, ['OCILobFileIsOpen']) );
  @OCILobFileOpen	:= GetProcAddress(hSqlLibModule, 'OCILobFileOpen');  	ASSERT( @OCILobFileOpen		<>nil,	Format(SErrFuncNotFound, ['OCILobFileOpen']) );
  @OCILobFileSetName	:= GetProcAddress(hSqlLibModule, 'OCILobFileSetName');  ASSERT( @OCILobFileSetName	<>nil,	Format(SErrFuncNotFound, ['OCILobFileSetName']) );
  @OCILobFlushBuffer	:= GetProcAddress(hSqlLibModule, 'OCILobFlushBuffer');  ASSERT( @OCILobFlushBuffer	<>nil,	Format(SErrFuncNotFound, ['OCILobFlushBuffer']) );
  @OCILobGetLength	:= GetProcAddress(hSqlLibModule, 'OCILobGetLength');  	ASSERT( @OCILobGetLength	<>nil,	Format(SErrFuncNotFound, ['OCILobGetLength']) );
  @OCILobIsEqual       	:= GetProcAddress(hSqlLibModule, 'OCILobIsEqual');  	ASSERT( @OCILobIsEqual		<>nil,	Format(SErrFuncNotFound, ['OCILobIsEqual']) );
  @OCILobLoadFromFile	:= GetProcAddress(hSqlLibModule, 'OCILobLoadFromFile'); ASSERT( @OCILobLoadFromFile	<>nil,	Format(SErrFuncNotFound, ['OCILobLoadFromFile']) );
  @OCILobLocatorIsInit	:= GetProcAddress(hSqlLibModule, 'OCILobLocatorIsInit');ASSERT( @OCILobLocatorIsInit	<>nil,	Format(SErrFuncNotFound, ['OCILobLocatorIsInit']) );
  @OCILobRead		:= GetProcAddress(hSqlLibModule, 'OCILobRead');  	ASSERT( @OCILobRead		<>nil,	Format(SErrFuncNotFound, ['OCILobRead']) );
  @OCILobTrim		:= GetProcAddress(hSqlLibModule, 'OCILobTrim');  	ASSERT( @OCILobTrim		<>nil,	Format(SErrFuncNotFound, ['OCILobTrim']) );
  @OCILobWrite		:= GetProcAddress(hSqlLibModule, 'OCILobWrite');  	ASSERT( @OCILobWrite		<>nil,	Format(SErrFuncNotFound, ['OCILobWrite']) );
  @OCIBreak		:= GetProcAddress(hSqlLibModule, 'OCIBreak');  		ASSERT( @OCIBreak		<>nil,	Format(SErrFuncNotFound, ['OCIBreak']) );
  @OCIServerVersion	:= GetProcAddress(hSqlLibModule, 'OCIServerVersion');  	ASSERT( @OCIServerVersion	<>nil,	Format(SErrFuncNotFound, ['OCIServerVersion']) );
  @OCIAttrGet		:= GetProcAddress(hSqlLibModule, 'OCIAttrGet');  	ASSERT( @OCIAttrGet		<>nil,	Format(SErrFuncNotFound, ['OCIAttrGet']) );
  @OCIAttrSet		:= GetProcAddress(hSqlLibModule, 'OCIAttrSet');  	ASSERT( @OCIAttrSet		<>nil,	Format(SErrFuncNotFound, ['OCIAttrSet']) );
  @OCISvcCtxToLda	:= GetProcAddress(hSqlLibModule, 'OCISvcCtxToLda');  	ASSERT( @OCISvcCtxToLda		<>nil,	Format(SErrFuncNotFound, ['OCISvcCtxToLda']) );
  @OCILdaToSvcCtx	:= GetProcAddress(hSqlLibModule, 'OCILdaToSvcCtx');  	ASSERT( @OCILdaToSvcCtx		<>nil,	Format(SErrFuncNotFound, ['OCILdaToSvcCtx']) );
  @OCIResultSetToStmt	:= GetProcAddress(hSqlLibModule, 'OCIResultSetToStmt'); ASSERT( @OCIResultSetToStmt	<>nil,	Format(SErrFuncNotFound, ['OCIResultSetToStmt']) );
	// XA library support: xaoOCIEnv, xaoSvcCtx were implemented at OCI v8.1
  @xaoEnv		:= GetProcAddress(hSqlLibModule, 'xaoEnv');
  @xaoSvcCtx		:= GetProcAddress(hSqlLibModule, 'xaoSvcCtx');

  if not Assigned( @OCIEnvCreate ) and Assigned( @OCIInitialize ) then
    OCIInitialize( OCI_THREADED or OCI_OBJECT, nil, nil, nil, nil );
end;

procedure DoneOCI8;
begin
  @OCIInitialize	:= nil;
  @OCIHandleAlloc	:= nil;
  @OCIHandleFree	:= nil;
  @OCIDescriptorAlloc	:= nil;
  @OCIDescriptorFree	:= nil;
  @OCIEnvCreate		:= nil;
  @OCIEnvInit		:= nil;
  @OCIServerAttach	:= nil;
  @OCIServerDetach	:= nil;
  @OCISessionBegin	:= nil;
  @OCISessionEnd	:= nil;
  @OCILogon		:= nil;
  @OCILogoff		:= nil;
  @OCIPasswordChange	:= nil;
  @OCIStmtPrepare	:= nil;
  @OCIBindByPos		:= nil;
  @OCIBindByName	:= nil;
  @OCIBindObject	:= nil;
  @OCIBindDynamic	:= nil;
  @OCIBindArrayOfStruct	:= nil;
  @OCIStmtGetPieceInfo	:= nil;
  @OCIStmtSetPieceInfo	:= nil;
  @OCIStmtExecute	:= nil;
  @OCIDefineByPos	:= nil;
  @OCIDefineObject	:= nil;
  @OCIDefineDynamic	:= nil;
  @OCIDefineArrayOfStruct:=nil;
  @OCIStmtFetch		:= nil;
  @OCIStmtGetBindInfo	:= nil;
  @OCIDescribeAny	:= nil;
  @OCIParamGet		:= nil;
  @OCIParamSet		:= nil;
  @OCITransStart	:= nil;
  @OCITransDetach	:= nil;
  @OCITransCommit	:= nil;
  @OCITransRollback	:= nil;
  @OCITransPrepare	:= nil;
  @OCITransForget	:= nil;
  @OCIErrorGet		:= nil;
  @OCILobAppend		:= nil;
  @OCILobAssign		:= nil;
  @OCILobCharSetForm	:= nil;
  @OCILobCharSetId	:= nil;
  @OCILobCopy		:= nil;
  @OCILobDisableBuffering:=nil;
  @OCILobEnableBuffering:= nil;
  @OCILobErase		:= nil;
  @OCILobFileClose	:= nil;
  @OCILobFileCloseAll	:= nil;
  @OCILobFileExists	:= nil;
  @OCILobFileGetName	:= nil;
  @OCILobFileIsOpen	:= nil;
  @OCILobFileOpen	:= nil;
  @OCILobFileSetName	:= nil;
  @OCILobFlushBuffer	:= nil;
  @OCILobGetLength	:= nil;
  @OCILobIsEqual       	:= nil;
  @OCILobLoadFromFile	:= nil;
  @OCILobLocatorIsInit	:= nil;
  @OCILobRead		:= nil;
  @OCILobTrim		:= nil;
  @OCILobWrite		:= nil;
  @OCIBreak		:= nil;
  @OCIServerVersion	:= nil;
  @OCIAttrGet		:= nil;
  @OCIAttrSet		:= nil;
  @OCISvcCtxToLda	:= nil;
  @OCILdaToSvcCtx	:= nil;
  @OCIResultSetToStmt	:= nil;

  @xaoEnv		:= nil;
  @xaoSvcCtx		:= nil;
end;

{-------------------------------------------------------------------------------
 			call Oracle API (OCI)
-------------------------------------------------------------------------------}
constructor TIOraDatabase.Create(ADatabase: TSDDatabase);
begin
  inherited Create(ADatabase);

  FHandle 	:= nil;
  FCurHandle	:= nil;
  FSharedInfo	:= nil;
	// by default IsEnableBCD = False
  FIsEnableBCD := AnsiUpperCase( Trim( ADatabase.Params.Values[szENABLEBCD] ) ) = STrueString;
	// by default IsEnableInts = False
  FIsEnableInts := AnsiUpperCase( Trim( ADatabase.Params.Values[szENABLEINTS] ) ) = STrueString;
	// by default IsRTrimChar = True
  FIsRTrimChar := not(AnsiUpperCase( Trim( ADatabase.Params.Values[szRTRIMCHAROUTPUT] ) ) = SFalseString);
	// by default IsXAConn = False
  FIsXAConn := AnsiUpperCase( Trim( ADatabase.Params.Values[szXACONN] ) ) = STrueString;
end;

destructor TIOraDatabase.Destroy;
begin
  FreeSharedInfo;

  if AcquiredHandle then
    FreeSqlLib;

  inherited Destroy;
end;

class function TIOraDatabase.CreateConnected(ADatabase: TSDDatabase; const ADatabaseName, AUserName, APassword: string): TISrvDatabase;
var
  isrvdb: TIOraDatabase;
  CliVer: LongInt;
begin
  ASSERT( ADatabase <> nil );

  CliVer := LoadSqlLib;
  try
    if (not ForceOCI7) and (HiWord(CliVer) >= 8) then
      isrvdb := TIOra8Database.Create( ADatabase )
    else
      isrvdb := TIOraDatabase.Create( ADatabase );

    isrvdb.Logon( ADatabaseName, AUserName, APassword );
  finally
    FreeSqlLib;	// decrement a library ref count, Logon calls LoadSqlLib too
  end;

  Result := isrvdb;
end;

class function TIOraDatabase.CreateWithHandle(ADatabase: TSDDatabase; const AHandle: PSDCursor): TISrvDatabase;
var
  isrvdb: TIOraDatabase;
  CliVer: LongInt;
begin
  ASSERT( ADatabase <> nil );

  CliVer := LoadSqlLib;
  try
    if (not ForceOCI7) and (HiWord(CliVer) >= 8) then
      isrvdb := TIOra8Database.Create( ADatabase )
    else
      isrvdb := TIOraDatabase.Create( ADatabase );

    isrvdb.Handle := AHandle;
  finally
    FreeSqlLib;	// decrement a library ref count, SetHandle call LoadSqlLib too
  end;
  Result := isrvdb;
end;

function TIOraDatabase.CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet;
begin
  Result := TIOraDataSet.Create( ADataSet );
end;

procedure TIOraDatabase.Check(Status: TSDEResult);
begin
  ResetIdleTimeOut;
  if Status = 0 then
    Exit;
  ResetBusyState;
  OraErrorOCI7( POraLogon(FHandle)^.Lda, POraLogon(FHandle)^.Lda.rc, -1 );
end;

function TIOraDatabase.GetHandle: PSDHandleRec;
var
  s: POraSrvInfo;
begin
  Result := nil;
  if not Assigned(FHandle) then
    Exit;

  New(FSharedInfo);
  FillChar( FSharedInfo^, SizeOf(FSharedInfo^), $0 );
  FSharedInfo^.SrvType := Ord( Database.ServerType );
  New(s);
  FillChar( s^, SizeOf(s^), $0 );
  FSharedInfo^.SrvInfo := s;
  s^.pL := FHandle;
  s^.pC := FCurHandle;

  Result := FSharedInfo;
end;

procedure TIOraDatabase.FreeSharedInfo;
begin
  if Assigned(FSharedInfo) then begin
    if Assigned(FSharedInfo^.SrvInfo) then
      Dispose( POraSrvInfo(FSharedInfo^.SrvInfo) );
    Dispose( FSharedInfo );
    FSharedInfo := nil;
  end;
end;

procedure TIOraDatabase.SetHandle(AHandle: PSDHandleRec);
var
  s: POraSrvInfo;
begin
  if not Assigned(AHandle) then
    Exit;

  s := PSDHandleRec(AHandle)^.SrvInfo;

  FHandle	:= s^.pL;
  FCurHandle	:= s^.pC;
end;

procedure TIOraDatabase.Logon(const ARemoteDatabase, AUserName, APassword: string);
var
  sConnect: string;
begin
	// OS Authentication, when a password is empty
  sConnect := SDOra.CreateConnectString(ARemoteDatabase, AUserName, APassword);
  try
    AllocHandles;

    LoadSqlLib;

    Check( SDOra.olog( POraLogon(FHandle)^.Lda, POraLogon(FHandle)^.Hda, PChar(sConnect), -1, nil, -1, nil, -1, OCI_LM_DEF  ) );
    Check( SDOra.oopen( POraCursor(FCurHandle)^, POraLogon(FHandle)^.Lda,
    				nil, -1, -1, nil, 0  ) );
  except
    FreeSqlLib;
    FreeHandles;

    raise;
  end;
end;

procedure TIOraDatabase.Logoff(Force: Boolean);
var
  rcd: TSDEResult;
begin
  rcd := SDOra.oclose( POraCursor(FCurHandle)^ );
  if not Force then
    Check( rcd );

  if Database.InTransaction then
    SDOra.orol( POraLogon(FHandle)^.Lda );
	// commit is automatically issued on a successful ologof call
  rcd := SDOra.ologof( POraLogon(FHandle)^.Lda );
  if not Force then
    Check( rcd );

  FreeHandles;

  FreeSqlLib;
end;

procedure TIOraDatabase.AllocHandles;
begin
  New(POraLogon(FHandle));
  New(POraCursor(FCurHandle));

  FillChar( POraLogon(FHandle)^, SizeOf(POraLogon(FHandle)^), $0 );
  FillChar( POraCursor(FCurHandle)^, SizeOf(POraCursor(FHandle)^), $0 );
end;

procedure TIOraDatabase.FreeHandles;
begin
  if FCurHandle <> nil then
    Dispose( POraCursor(FCurHandle) );
  FCurHandle := nil;
  if FHandle <> nil then
    Dispose( POraLogon(FHandle) );
  FHandle := nil;
end;

function TIOraDatabase.SPDescriptionsAvailable: Boolean;
begin
  Result := True;
end;

procedure TIOraDatabase.StartTransaction;
begin
  if AutoCommit then
    SetAutoCommitOption(False);
end;

procedure TIOraDatabase.Commit;
begin
  Check( SDOra.ocom( POraLogon(FHandle)^.Lda ) );

  SetTransIsolation(Database.TransIsolation);

  if AutoCommit then
    SetAutoCommitOption(True);
end;

procedure TIOraDatabase.Rollback;
begin
  Check( SDOra.orol( POraLogon(FHandle)^.Lda ) );

  SetTransIsolation(Database.TransIsolation);

  if AutoCommit then
    SetAutoCommitOption(True);
end;

procedure TIOraDatabase.SetAutoCommitOption(Value: Boolean);
begin
  if Value then
    Check( ocon( POraLogon(FHandle)^.Lda ) )	// enable autocommit
  else
    Check( ocof( POraLogon(FHandle)^.Lda ) );	// disable autocommit
end;

procedure TIOraDatabase.SetDefaultParams;
begin
  if not AutoCommitDef then
    SetAutoCommitOption( AutoCommit );
end;

procedure TIOraDatabase.SetTransIsolation(Value: TSDTransIsolation);
const
  IsolLevel: array[TSDTransIsolation] of string =
  	('set transaction READ WRITE',
         'set transaction READ WRITE',
         'set transaction READ ONLY'
        );
begin
  Check( SDOra.oparse(POraCursor(FCurHandle)^, PChar(IsolLevel[Value]), -1, DEFERRED_PARSE, ORACLE7_PARSE) );
  Check( SDOra.oexec(POraCursor(FCurHandle)^) );
end;

{ if possible then convert ftFloat(NUMBER) to ftInteger ( Precision <= 9 (10 digits >= 1 billion ) }
function TIOraDatabase.ExactNumberDataType(FieldType: TFieldType; Precision, Scale: Integer): TFieldType;
const
  MAX_INT_PRECISION	= 10;	// 10 is limit in BDE for ftInteger
  MAX_SINT_PRECISION	= 5;	// 5 is limit in BDE for ftSmallInt
  MAX_BCDFIELD_PREC	= 20;
  MAX_BCDFIELD_SCALE	= 4;
begin
  Result := FieldType;
  if (not IsEnableInts) and (not IsEnableBCD) then
    Exit;

  if (FieldType = ftFloat) and (Precision >= 1) then
    if Scale = 0 then begin
      if Precision <= (MAX_SINT_PRECISION) then
        Result := ftSmallInt
      else if Precision <= (MAX_INT_PRECISION) then
        Result := ftInteger;
    end else
      if IsEnableBCD and (Precision <= MAX_BCDFIELD_PREC) and
                (Scale > 0) and (Scale <= MAX_BCDFIELD_SCALE)
      then
        Result := ftBCD;
end;

function TIOraDatabase.FieldDataType(ExtDataType: Integer): TFieldType;
begin
  case ExtDataType of
    SQLT_CHR:	Result := ftString;	// (ORANET TYPE) character string
    SQLT_NUM:   Result := ftFloat;	// NUMBER
    SQLT_INT:	Result := ftInteger;	// (ORANET TYPE) integer
    SQLT_LNG:   Result := ftMemo;	// LONG
    SQLT_RID:   Result := ftString;	// ROWID
    SQLT_DAT:	Result := ftDateTime;	// DATE
    SQLT_BIN:	Result := ftVarBytes;	// RAW
    SQLT_LBI:	Result := ftBlob;	// LONG RAW
    SQLT_AFC,
    SQLT_AVC:	Result := ftString;	// Ansi fixed and var char
    SQLT_CUR:	Result := ftCursor;	// cursor  type
    SQLT_LAB,
    SQLT_OSL:	Result := ftString;	// label type and oslabel type
  else
    Result := ftUnknown;
  end;
end;

function TIOraDatabase.FieldSubType(ExtDataType: Integer): Word;
begin
  case ExtDataType of
    SQLT_LNG:   Result := fldstMEMO;	// LONG
    SQLT_LBI:	Result := fldstBINARY;	// LONG RAW
  else
    Result := inherited FieldSubType( ExtDataType );
  end;
end;

function TIOraDatabase.NativeDataSize(FieldType: TFieldType): Word;
const
  { Converting from TFieldType to Program Data Type(Oracle) }
  OraDataSizeMap: array[TFieldType] of Word = ( 0,	// ftUnknown
	// ftString, ftSmallint, ftInteger, 	ftWord, 	ftBoolean
	0,	SizeOf(Integer), SizeOf(Integer), SizeOf(Integer), 0,
	// ftFloat, 	ftCurrency, 	ftBCD, ftDate, ftTime
        SizeOf(Double),	SizeOf(Double),	SizeOf(Double), ORA_DTS_DATE, ORA_DTS_DATE,
        // ftDateTime, ftBytes, ftVarBytes, ftAutoInc, ftBlob
        ORA_DTS_DATE, 	0, 	0, 		0, 	0,
        // ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle
        0,		0,	0,		0,	0,
        // ftTypedBinary, ftCursor
        0,		SizeOf(TCdaDef)
{$IFDEF SD_VCL4},
	// ftFixedChar, ftWideString, ftLargeint,
        0,	0,	0,
        // ftADT, ftArray, ftReference, ftDataSet
        0,	0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL5},
        // ftOraBlob, ftOraClob, ftVariant,
        SizeOf( POCILobLocator ), SizeOf( POCILobLocator ), 0,
        // ftInterface, ftIDispatch, ftGuid
        0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL6},
        // ftTimeStamp, ftFMTBcd
        0,      0
{$ENDIF}        
        );
begin
  Result := OraDataSizeMap[FieldType];
end;

function TIOraDatabase.NativeDataType(FieldType: TFieldType): Integer;
const
  { Converting from TFieldType to Program Data Type(Oracle) }
  OraDataTypeMap: array[TFieldType] of Integer = ( 0,	// ftUnknown
	// ftString, ftSmallint, ftInteger, ftWord, ftBoolean
	SQLT_AVC, 	SQLT_INT, SQLT_INT, SQLT_INT, 0,
	// ftFloat, ftCurrency, ftBCD, ftDate, ftTime
        SQLT_FLT, 	SQLT_FLT, SQLT_FLT, SQLT_DAT, SQLT_DAT,
        // ftDateTime, ftBytes, ftVarBytes, ftAutoInc, ftBlob
        SQLT_DAT, 	SQLT_LBI, SQLT_LBI, 	0, 	SQLT_LBI,
        // ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle
        SQLT_LNG, 	SQLT_LBI, SQLT_LNG,	0,	0,
        // ftTypedBinary, ftCursor
        0,		SQLT_CUR
{$IFDEF SD_VCL4},
	// ftFixedChar, ftWideString, ftLargeint,
        0,	0,	0,
        // ftADT, ftArray, ftReference, ftDataSet
        0,	0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL5},
        // ftOraBlob, ftOraClob, ftVariant,
        SQLT_BLOB,	SQLT_CLOB,	0,
        // ftInterface, ftIDispatch, ftGuid
        0,	        0,	        0
{$ENDIF}
{$IFDEF SD_VCL6},
        // ftTimeStamp, ftFMTBcd
        0,      0
{$ENDIF}
        );
begin
  Result := OraDataTypeMap[FieldType];
end;

function TIOraDatabase.ParamValue(Value: TSDDatabaseParam): Integer;
begin
  case Value of
    spMaxFieldName,
    spMaxTableName,
    spMaxSPName:	Result := 30;
    spMaxFullTableName,
    spMaxFullSPName:	Result := 2*30+1;
  else
    Result := 0;
  end;
end;

function TIOraDatabase.GetClientVersion: LongInt;
begin
  Result := dwLoadedOCI;
end;

function TIOraDatabase.GetServerVersion: LongInt;
var
  ResultSet: TStringList;
begin
  Result := 0;
  ResultSet := TStringList.Create;
  try
    GetStmtResult('select VERSION from PRODUCT_COMPONENT_VERSION where PRODUCT like ''%Oracle%''', ResultSet);

    if ResultSet.Count > 0 then
      Result := VersionStringToDWORD( Trim( ResultSet.Strings[0] ) );
  finally
    ResultSet.Free;
  end;
end;

function TIOraDatabase.GetVersionString: string;
var
  ResultSet: TStringList;
begin
  ResultSet := TStringList.Create;
  try
    GetStmtResult(
    	'select PRODUCT || '' Release '' || VERSION || '' - '' || STATUS '+
    	'from PRODUCT_COMPONENT_VERSION where PRODUCT like ''%Oracle%''',
    	ResultSet);

    if ResultSet.Count > 0 then
      Result := ResultSet.Strings[0];
  finally
    ResultSet.Free;
  end;
end;

procedure TIOraDatabase.GetStmtResult(const Stmt: string; List: TStrings);
var
  dbtype, prec, scale, nullok: sb2;
  dbsize, dsize: sb4;
  szColHeading: PChar;
  ColHeadingL: sb4;
  MaxColumnName: Integer;
  szData: PChar;
begin
  szData := nil;
  MaxColumnName := ParamValue(spMaxFieldName)*5;	// column name can appear with function name
  szColHeading := StrAlloc( MaxColumnName + 1 );

  try
    Check( oparse( POraCursor(FCurHandle)^, PChar(Stmt), -1, DEFERRED_PARSE, ORACLE7_PARSE ) );
    ColHeadingL := MaxColumnName - 1;
    Check( odescr(POraCursor(FCurHandle)^, 1, dbsize, dbtype,
     			szColHeading, ColHeadingL, dsize, prec, scale, nullok) );
    szData := StrAlloc( dbsize );
    Check( SDOra.oexec( POraCursor(FCurHandle)^ ) );

    Check( odefin(POraCursor(FCurHandle)^, 1,
          	szData, dbsize, SQLT_STR, 0,
                nil, nil, -1, -1, @dbsize, nil) );

    while ofetch( POraCursor(FCurHandle)^ ) = 0 do
      if dbsize > 0 then
        List.Add( StrPas(szData) );

  finally
    StrDispose(szColHeading);
    if szData <> nil then StrDispose(szData);
  end;
end;

procedure TIOraDatabase.GetStoredProcNames(List: TStrings);
var
  sStmt: string;
begin
  sStmt := 'select OWNER || ''.'' || OBJECT_NAME from SYS.ALL_OBJECTS ' +
	   'where OBJECT_TYPE in (''PROCEDURE'', ''FUNCTION'') order by 1';
  List.BeginUpdate;
  try
    List.Clear;

    GetStmtResult( sStmt, List );
  finally
    List.EndUpdate;
  end;
end;

procedure TIOraDatabase.GetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings);
var
  sStmt: string;
begin
  sStmt := 'select OWNER || ''.'' || OBJECT_NAME from SYS.ALL_OBJECTS ' +
	   'where OBJECT_TYPE in (''TABLE'', ''VIEW'') ';
  if not SystemTables then
    sStmt := sStmt + 'and OWNER != ''SYS'' ';
  if Trim(Pattern) <> '' then
    sStmt := sStmt + 'and OBJECT_NAME like ''' + Pattern + ''' ';
  sStmt := sStmt + 'order by 1';

  List.BeginUpdate;
  try
    List.Clear;

    GetStmtResult( sStmt, List );
  finally
    List.EndUpdate;
  end;
end;

procedure TIOraDatabase.GetTableFieldNames(const TableName: string; List: TStrings);
var
  sStmt: string;
begin
  sStmt := Format( 'select COLUMN_NAME from SYS.ALL_TAB_COLUMNS ' +
	   	   'where UPPER(TABLE_NAME) = UPPER(''%s'')',	[TableName] );

  List.BeginUpdate;
  try
    List.Clear;

    GetStmtResult( sStmt, List );
  finally
    List.EndUpdate;
  end;
end;

// Returns True if field type required converting from internal database format
function TIOraDatabase.RequiredCnvtFieldType(FieldType: TFieldType): Boolean;
begin
  Result := FieldType in [ftDate, ftTime, ftDateTime, ftCursor];
end;

function TIOraDatabase.CnvtDateTime2DBDateTime(ADataType: TFieldType; Value: TDateTime; Buffer: Pointer; BufSize: Integer): Integer;
var
  Year, Month, Day, Hour, Min, Sec, MSec: Word;
begin
  if NativeDataSize(ADataType) > BufSize then
    DatabaseError(SInsufficientIDateTimeBufSize);
	// it's necessary for ftDate
  Hour	:= 0;
  Min	:= 0;
  Sec	:= 0;

  if ADataType in [ftTime, ftDateTime] then
    DecodeTime(Value, Hour, Min, Sec, MSec);
  if ADataType in [ftDate, ftDateTime] then
    DecodeDate(Value, Year, Month, Day);

  Byte( Pointer(Integer(Buffer)+0)^ ) := Year div 100 + 100;
  Byte( Pointer(Integer(Buffer)+1)^ ) := Year mod 100 + 100;
  Byte( Pointer(Integer(Buffer)+2)^ ) := Month;
  Byte( Pointer(Integer(Buffer)+3)^ ) := Day;
	// default time value = midnight = (1, 1, 1)
  Byte( Pointer(Integer(Buffer)+4)^ ) := Hour + 1;
  Byte( Pointer(Integer(Buffer)+5)^ ) := Min + 1;
  Byte( Pointer(Integer(Buffer)+6)^ ) := Sec + 1;

  Result := BufSize;
end;

function TIOraDatabase.CnvtDBDateTime2DateTimeRec(ADataType: TFieldType; Buffer: PChar; BufSize: Integer): TDateTimeRec;
var
  Year, Month, Day, Hour, Min, Sec: Word;
  dtDate, dtTime: TDateTime;
begin
  Year := (Byte( Buffer[0] ) - 100) * 100 + (Byte( Buffer[1] ) - 100);
  Month := Byte( Buffer[2] );
  Day := Byte( Buffer[3] );

  Hour := Byte(Buffer[4]) - 1;
  Min := Byte(Buffer[5]) - 1;
  Sec := Byte(Buffer[6]) - 1;
  dtDate := EncodeDate(Year, Month, Day);
  dtTime := EncodeTime(Hour, Min, Sec, 0);

  case (ADataType) of
    ftTime: 	Result.Time := DateTimeToTimeStamp(dtTime).Time;
    ftDate: 	Result.Date := DateTimeToTimeStamp(dtDate).Date;
    ftDateTime: Result.DateTime := TimeStampToMSecs( DateTimeToTimeStamp(dtDate + dtTime) );
  else
    Result.DateTime := 0.0;
  end;
end;

// TDateTimeField selects as TDateTimeRec and converts to TDateTime
function TIOraDatabase.CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean;
var
  dtDateTime: TDateTimeRec;
  InData, OutData: Pointer;
begin
    	// data of BLOB-fields stored in other place and FetchStatus and DataSize set after fetch(read BLOB column)
  if AField.DataType in [Low(TBlobType)..High(TBlobType)] then begin
    PFieldInfo(OutBuf)^.FetchStatus := indNULL;
    PFieldInfo(OutBuf)^.DataSize := 0;
    Result := True;
    Exit;
  end;

  if sb2( PFieldInfo(InBuf)^.FetchStatus ) = 0 then
    PFieldInfo(OutBuf)^.FetchStatus := indVALUE
  else if sb2( PFieldInfo(InBuf)^.FetchStatus ) = FetRNul then
    PFieldInfo(OutBuf)^.FetchStatus := indNULL
  else
    PFieldInfo(OutBuf)^.FetchStatus := indTRUNC;

  if PFieldInfo(OutBuf)^.FetchStatus = indNULL then
    PFieldInfo(OutBuf)^.DataSize := 0
  else begin
    InData := Pointer( Integer(InBuf) + SizeOf(TFieldInfo) );
    OutData := Pointer( Integer(OutBuf) + SizeOf(TFieldInfo) );
    	// DateTime fields
    if RequiredCnvtFieldType(AField.DataType) and (AField is TDateTimeField) then begin
      dtDateTime := CnvtDBDateTime2DateTimeRec(AField.DataType, InData, PFieldInfo(InBuf)^.DataSize);
      TDateTimeRec(OutData^) := dtDateTime;
      PFieldInfo(OutBuf)^.DataSize := SizeOf(TDateTimeRec);
    end else begin
      if AField.DataType = ftString then begin
      	// PFieldInfo(InBuf)^.DataSize must include null-terminator, but Oracle 8.0.5(and 8i) does not include it.
        if PFieldInfo(InBuf)^.DataSize > AField.Size then
          Move( InData^, OutData^, PFieldInfo(InBuf)^.DataSize)	// to exclude overwriting OutData buffer
        else
          Move( InData^, OutData^, PFieldInfo(InBuf)^.DataSize + 1);
        if IsRTrimChar then
          StrRTrim( OutData );
        PFieldInfo(OutBuf)^.DataSize := StrLen( OutData );
      end else if AField.DataType = ftBCD then begin
        Currency( InData^ ) := Double( InData^ );
        CurrToBCD( Currency( InData^ ), TBcd(OutData^), 32, 4 );
        PFieldInfo(OutBuf)^.DataSize := SizeOf(TBcd);
      end else begin
        Move( InData^, OutData^, PFieldInfo(InBuf)^.DataSize);
        PFieldInfo(OutBuf)^.DataSize := PFieldInfo(InBuf)^.DataSize;
      end;
    end;
  end;
  Result := True;
end;

{ TIOraDataSet }
constructor TIOraDataSet.Create(ADataSet: TSDDataSet);
begin
  inherited Create(ADataSet);

  FHandle := nil;
  FParamHandle := nil;
  FBoundParams := False;
    
  FIsCursorParamExists := False;
end;

destructor TIOraDataSet.Destroy;
begin
  Disconnect(False);

  inherited;
end;

function TIOraDataSet.GetHandle: PSDCursor;
begin
  Result := FHandle;
  if FParamHandle <> nil then
    Result := FParamHandle;
end;

function TIOraDataSet.GetSrvDatabase: TIOraDatabase;
begin
  Result := (inherited SrvDatabase) as TIOraDatabase;
end;

procedure TIOraDataSet.Check(Status: TSDEResult);
var
  peo: Integer;
begin
  SrvDatabase.ResetIdleTimeOut;
  if Status = 0 then
    Exit;
  ResetBusyState;
  if POraCursor(Handle)^.rc <> 0 then begin
    peo := -1;
    	// after oparse call
    if POraCursor(Handle)^.fc = 54 then
      peo := POraCursor(Handle)^.peo;
    OraErrorOCI7( POraLogon(SrvDatabase.FHandle)^.Lda, POraCursor(Handle)^.rc, peo )
  end else
    OraErrorOCI7( POraLogon(SrvDatabase.FHandle)^.Lda, POraLogon(SrvDatabase.FHandle)^.Lda.rc, -1 );
end;

procedure TIOraDataSet.Connect;
begin
  New(POraCursor(FHandle));
  try
    FillChar( POraCursor(FHandle)^, SizeOf(POraCursor(FHandle)^), $0 );
    Check( SDOra.oopen( POraCursor(FHandle)^, POraLogon(SrvDatabase.FHandle)^.Lda,
    				nil, -1, -1, nil, 0  ) );
  except
    Dispose(FHandle);
    FHandle := nil;
    raise;
  end;
end;

procedure TIOraDataSet.Disconnect(Force: Boolean);
begin
  if FSelectBuffer <> nil then
    FreeSelectBuffer;

  if FHandle = nil then Exit;
  	// In case of hang up returns an error (for example, ORA-00028).
        //However it's a successful disconnect, so the error is not raised
  SDOra.oclose( POraCursor(FHandle)^ );

  Dispose(FHandle);
  FHandle := nil;
end;

function TIOraDataSet.ResultSetExists: Boolean;
var
  i: Integer;
begin
  Result := POraCursor(Handle)^.ft in [ORA_CT_SEL];
  if DataSet is TSDQuery then begin
    for i:=0 to Query.ParamCount-1 do
    	// if PL/SQL block with cursor parameter is executes ('begin open_proc(:v_cur); end;')
      if Query.Params[i].DataType = ftCursor then begin
        FIsCursorParamExists := True;
        Break;
      end;
  end else if DataSet is TSDStoredProc then begin
    for i:=0 to StoredProc.ParamCount-1 do
	// if PL/SQL procedure returns cursor parameters
      if StoredProc.Params[i].DataType = ftCursor then begin
        FIsCursorParamExists := True;
        Break;
      end;
  end;
  if not Result then
    Result := FIsCursorParamExists;
end;

procedure TIOraDataSet.GetFieldDescs(Descs: TSDFieldDescList);
var
  pFieldDesc: PSDFieldDesc;
  rcd, pos: sword;
  dbtype, prec, scale, nullok: sb2;
  dbsize, dsize: sb4;
  szColHeading: PChar;
  ColHeadingL: sb4;
  MaxColumnName: Integer;
  ft: TFieldType;
begin
  ASSERT( FHandle <> nil );

  if FIsCursorParamExists then begin
    if (DataSet is TSDStoredProc) and not FBoundParams then
      SpBindParams;  
    Execute;
    if DataSet is TSDStoredProc then
      SpGetParams;
    if DataSet is TSDQuery then
      QGetParams;
  end;

  MaxColumnName := SrvDatabase.MaxFieldNameLen;
  szColHeading := StrAlloc( MaxColumnName + 1 );	// +1 - include space for zero-terminator
  try
    pos := 1;
    while True do begin
      ColHeadingL := MaxColumnName;
      rcd := odescr(POraCursor(Handle)^, pos, dbsize, dbtype,
     			szColHeading, ColHeadingL, dsize, prec, scale, nullok);
      if rcd <> 0 then
        if POraCursor(Handle)^.rc = ORA_ERR_VarNotInSel
        then Break
        else Check( rcd );
      if ColHeadingL > MaxColumnName then DatabaseError(SInsufficientColumnBufSize);
      szColHeading[ColHeadingL] := #0;

      ft := SrvDatabase.FieldDataType(dbtype);
      if ft = ftUnknown then
        DatabaseErrorFmt( SBadFieldType, [szColHeading] );
	// for ROWID use dsize (display size), as since ROWID binds as string
      if dbtype = SQLT_RID then dbsize := dsize;
      ft := SrvDatabase.ExactNumberDataType( ft, prec, scale );

      New( pFieldDesc );
      with pFieldDesc^ do begin
        FieldName	:= szColHeading;
        FieldNo		:= pos;
        DataType	:= ft;
        if ft = ftBCD then	// TBCDField.Size indicates the number digits after the decimal place
          Size		:= scale
        else
          Size		:= dbsize;
        Precision	:= prec;
    	// if NullOk = 0 then null values are not permitted for the column (Required = True)
        Required	:= NullOk = 0;
      end;
      Descs.Add( pFieldDesc );

      Inc(pos);
    end;

  finally
    StrDispose(szColHeading);
  end;
end;

// Setting buffers for data, which selected from database
procedure TIOraDataSet.SetSelectBuffer;
var
  i, nOffset: Integer;
  PrgDataSize: Word;
  PrgDataType: Integer;
  PrgDataBuffer: PChar;
  TmpField: TField;
begin
  nOffset := 0;		// pointer to the TFieldInfo

  for i:=0 to DataSet.FieldDefs.Count-1 do begin
    TmpField := FieldByNumber(DataSet.FieldDefs[i].FieldNo);
    if Assigned(TmpField) then with TmpField do begin
    	// Skip calculated fields
      if Calculated then Continue;

      PrgDataType := SrvDatabase.NativeDataType(DataType);
      if PrgDataType = 0 then
        DatabaseErrorFmt( SUnknownFieldType, [FieldName] );

      PrgDataSize := NativeFieldDataSize(TmpField);
      PrgDataBuffer := PChar( Integer(FSelectBuffer) + nOffset + SizeOf(TFieldInfo) );
      if IsSupportedBlobTypes(DataType)  then begin
        if DetectedOCI73 and UsePiecewiseCalls then
          Check( odefinps(POraCursor(Handle)^, 0, FieldNo,
      		nil, MAX_LONG_COL_SIZE, PrgDataType, 0,
     		@PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.FetchStatus,
        	nil, 0, 0,
     		@PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.DataSize,
        	nil, 0, 0, 0, 0) )
        else
          Check( odefin(POraCursor(Handle)^, FieldNo,
          	PrgDataBuffer, 0{MAX_LONG_COL_SIZE}, PrgDataType, 0,
                @PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.FetchStatus,
                nil, -1, -1,
                @PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.DataSize,
                nil) );
      end else begin
        if DetectedOCI73 then
          Check( odefinps(POraCursor(Handle)^, 1, FieldNo,
      		PrgDataBuffer, PrgDataSize, PrgDataType, 0,
     		@PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.FetchStatus,
        	nil, 0, 0,
     		@PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.DataSize,
        	nil, 0, 0, 0, 0) )
        else
          Check( odefin(POraCursor(Handle)^, FieldNo,
          	PrgDataBuffer, PrgDataSize, PrgDataType, 0,
                @PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.FetchStatus,
                nil, -1, -1,
                @PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.DataSize,
                nil) );
      end;	{ if DataType in SupportedBlobFields then }
      Inc(nOffset, SizeOf(TFieldInfo) + PrgDataSize);
    end;
  end;
end;

procedure TIOraDataSet.Execute;
begin
  if DataSet is TSDQuery then begin
    if FParamHandle = nil then
      QExecute
  end else if DataSet is TSDStoredProc then begin
    if FParamHandle = nil then
      SpExecute
  end else
    Check( SDOra.oexec(POraCursor(Handle)^) );
end;

function TIOraDataSet.FetchNextRow: Boolean;
var
  rcd: sword;
begin
  rcd := SDOra.ofetch( POraCursor(Handle)^ );

  SrvDatabase.ResetIdleTimeOut;

  if rcd <> 0 then
    rcd := POraCursor(Handle)^.rc;
	// get Blob field data
  if BlobFieldCount > 0 then begin
  	// use piecewise fetch for Oracle 7.3, if it's necessary
    if DetectedOCI73 and (rcd = OCI_MORE_FETCH_PIECES) then begin
      FetchBlobFields;
      Result := True;
    end else begin
      if rcd = 0 then
        FetchBlobFields;
      Result := rcd = 0;
    end;
  end else
    Result := rcd = 0;

	// if EOF, then close Cursor, returned by PL/SQL procedure
  if (not Result) and (FParamHandle <> nil) then
    CloseParamHandle;
end;

function TIOraDataSet.CnvtDateTime2DBDateTime(ADataType: TFieldType; Value: TDateTime; Buffer: Pointer; BufSize: Integer): Integer;
begin
  Result := SrvDatabase.CnvtDateTime2DBDateTime( ADataType, Value, Buffer, BufSize );
end;

// TDateTimeField selects as TDateTimeRec and converts to TDateTime
function TIOraDataSet.CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean;
begin
  Result := SrvDatabase.CnvtDBField2Field(AField, InBuf, OutBuf);
end;

function TIOraDataSet.ReadBlob(FieldNo: Integer; var BlobData: string): LongInt;
var
  Len: ub4;       		// count of really read bytes
  Buf: PChar;			// current pointer for read of data
  BlobSize, GrowDelta: Integer;	// current buffer size and increment size
  Piece: Byte;
  rcd: sword;
  iter, index: ub4;
  ctxp: Pointer;
  dbtype: Integer;
begin
  Result := 0;
  GrowDelta := $7FFF;
  BlobSize := GrowDelta;
  if DetectedOCI73 and UsePiecewiseCalls then begin
    while True do begin
      SetLength(BlobData, BlobSize);
      Buf := @PChar(BlobData)[BlobSize-GrowDelta];
      Check( SDOra.ogetpi(POraCursor(Handle)^, Piece, ctxp, iter, index) );
	// read from database
      Len := GrowDelta;
      Check( SDOra.osetpi(POraCursor(Handle)^, Piece, Buf, Len) );
      rcd := SDOra.ofetch(POraCursor(Handle)^);
      Inc(Result, Len);
      if Abs(rcd) <> OCI_MORE_FETCH_PIECES then
        if rcd = 0 then
          Break
        else
          Check( rcd );
      Inc(BlobSize, GrowDelta);
    end;
  end else begin
    while True do begin
      SetLength(BlobData, BlobSize);
      Buf := @PChar(BlobData)[BlobSize-GrowDelta];
	// read from database
      Len := GrowDelta;
      dbtype := SrvDatabase.NativeDataType(FieldByNumber(FieldNo).DataType);
      Check( SDOra.oflng(POraCursor(Handle)^, FieldNo, Buf, Len, dbtype, Len, BlobSize-GrowDelta) );
      Inc(Result, Len);
      if (Len = 0) or (Len < GrowDelta) then
        Break;
      Inc(BlobSize, GrowDelta);
    end;
  end;

  SetLength(BlobData, Result);
end;

function TIOraDataSet.WriteBlob(FieldNo: Integer; const Buffer; Count: LongInt): Longint;
begin
  raise EDatabaseError.CreateFmt(SFuncNotImplemented, ['TIOraDataSet.WriteBlob']);
end;

function TIOraDataSet.WriteBlobByName(Name: string; const Buffer; Count: LongInt): Longint;
var
  Piece: Byte;
  rcd: sword;
  iter, index: ub4;
  ctxp: Pointer;
begin
  Result := 0;
  if not DetectedOCI73 then
    Exit;
  while True do begin
    Check( SDOra.ogetpi(POraCursor(Handle)^, Piece, ctxp, iter, index) );
	// write a block in the database as one piece
    Piece := OCI_LAST_PIECE;
    	// if bufp will equally @Buffer, and not nil(for empty buffer),
        //then for the following insert of the non-empty buffer the exception "Access violation" will be raised
        //in ORA73.dll in the time of binding of this parameter (obindps)
    if Count = 0 then
      Check( SDOra.osetpi(POraCursor(Handle)^, Piece, nil, Count) )
    else
      Check( SDOra.osetpi(POraCursor(Handle)^, Piece, @Buffer, Count) );
    rcd := SDOra.oexec(POraCursor(Handle)^);
    if rcd = 0 then
      Break
    else if rcd <> OCI_MORE_INSERT_PIECES then
      Check( rcd );
  end;
  Result := Count;
end;

		{ Query methods }

procedure TIOraDataSet.QBindParams;
var
  CurPtr, DataPtr: Pointer;
  i: Integer;
  sqlvar: string;
  DataLen: Word;
begin
  if FBindBuffer = nil then Exit;
  CurPtr := FBindBuffer;

  for i:=0 to Query.ParamCount-1 do
    with Query.Params[i] do begin
      if IsNull then
        TFieldInfo(CurPtr^).FetchStatus := -1
      else
        TFieldInfo(CurPtr^).FetchStatus := 0;
      TFieldInfo(CurPtr^).DataSize := 0;
      DataLen := QParamBindSize( Query.Params[i] );
      DataPtr := Pointer(Integer(CurPtr) + SizeOf(TFieldInfo));

      sqlvar := IntToStr(i+1);

      case DataType of
        ftString:
          if DataLen > 0 then StrMove(DataPtr, PChar(AsString), DataLen);
        ftInteger:
          if DataLen > 0 then DWORD(DataPtr^) := AsInteger;
        ftSmallInt:
          if DataLen > 0 then WORD(DataPtr^) := AsInteger;
        ftDate, ftTime, ftDateTime:
          if DataLen > 0 then CnvtDateTime2DBDateTime(DataType, AsDateTime, DataPtr, SrvDatabase.NativeDataSize(DataType));
        ftFloat:
          if DataLen > 0 then Double(DataPtr^) := AsFloat;
        ftCursor:
		// for avoid "invalid cursor" error, because the Oracle server
                //doesn't accept NULL as an input value for a REF Cursor type.
          if IsNull then
            TFieldInfo(CurPtr^).FetchStatus := 0;
        else
          if not IsSupportedBlobTypes(DataType) then
            raise EDatabaseError.CreateFmt(SNoParameterDataType, [Name]);
      end;
      if IsBlobType(DataType) then begin
        if DetectedOCI73 and UsePiecewiseCalls then
          Check( obindps(POraCursor(Handle)^, 0, PChar(sqlvar), -1, nil, MAX_LONG_COL_SIZE,
        		SrvDatabase.NativeDataType(DataType), 0,
                        @TFieldInfo(CurPtr^).FetchStatus, nil, nil, 0, 0, 0, 0, 0, nil, nil, 0, 0) )
        else begin
          if TVarData(Value).VType <> varString then
            VarAsType(Value, varString);
          Check( obndra(POraCursor(Handle)^, PChar(sqlvar), -1,
          		PChar(TVarData(Value).VString), RealParamDataSize( Query.Params[i] ),
        		SrvDatabase.NativeDataType(DataType), -1,
                        @TFieldInfo(CurPtr^).FetchStatus, nil, nil, 0, nil, nil, 0, 0) );
        end;
      end else begin
        if DetectedOCI73 then
          Check( obindps(POraCursor(Handle)^, 1, PChar(sqlvar), -1, DataPtr, DataLen,
        		SrvDatabase.NativeDataType(DataType), 0,
                        @TFieldInfo(CurPtr^).FetchStatus, nil, nil, 0, 0, 0, 0, 0, nil, nil, 0, 0) )
        else
          Check( obndra(POraCursor(Handle)^, PChar(sqlvar), -1, DataPtr, DataLen,
        		SrvDatabase.NativeDataType(DataType), -1,
                        @TFieldInfo(CurPtr^).FetchStatus, nil, nil, 0, nil, nil, 0, 0) );
      end;

      Inc( Integer(CurPtr), SizeOf(TFieldInfo) + DataLen );
    end;
end;

procedure TIOraDataSet.QPrepareSQL(Value: PChar);
begin
  if FHandle = nil then Connect;

  FStmt := MakeValidStatement( Value, Query );

	// it's not use deferred parse, which does not recognize
        // a statement type(SELECT or other; used in ResultSetExists) and syntax errors
  Check( SDOra.oparse(POraCursor(Handle)^, PChar(FStmt), -1, 0, ORACLE7_PARSE) );
end;

procedure TIOraDataSet.QExecute;
var
  rcd: sword;
  i: Integer;
  sBlob: string;
begin
  rcd := SDOra.oexec(POraCursor(Handle)^);
  if rcd <> 0 then
    rcd := POraCursor(Handle)^.rc;
    	// if required Blob-field data
  if rcd = OCI_MORE_INSERT_PIECES then begin
    for i:=0 to Query.ParamCount-1 do
      if IsSupportedBlobTypes(Query.Params[i].DataType) then begin
        sBlob := Query.Params[i].AsBlob;
        WriteBlobByName(Query.Params[i].Name, PChar(sBlob)^, Length(sBlob));
      end;
  end else if rcd <> 0 then
    Check( rcd );
	// if PL/SQL block is executed
  if POraCursor(Handle)^.ft = ORA_CT_PEX then
    QGetParams;
end;

function TIOraDataSet.QGetRowsAffected: Integer;
begin
  Result := POraCursor(Handle)^.rpc;
end;

procedure TIOraDataSet.QGetParams;
var
  I: Integer;
  FldBuf, OutBuf, OutData: PChar;
  TmpField: TField;
  FieldCls: TFieldClass;
begin
  if FBindBuffer = nil then Exit;
	// Alloc buffer for parameter of max size
  OutBuf := StrAlloc( MinStrParamSize + SizeOf(TFieldInfo) );

  try
    FldBuf := FBindBuffer;
    for I := 0 to Query.ParamCount - 1 do with Query.Params[I] do begin
      if ParamType in [ptResult, ptInputOutput, ptOutput] then begin
        if PFieldInfo(FldBuf)^.DataSize = 0 then
          PFieldInfo(FldBuf)^.DataSize := QParamBindSize(Query.Params[I]);

        if DataType = ftCursor then begin
          if FParamHandle = nil then
            FParamHandle := PCdaDef( FldBuf + SizeOf(TFieldInfo) );
        end else begin
          FieldCls := DefaultFieldClasses[DataType];
          TmpField := FieldCls.Create(nil);
          try
            CnvtDBField2Field(TmpField, FldBuf, OutBuf);
          finally
            TmpField.Free;
          end;

          if FieldIsNull(OutBuf) then
            Clear
          else begin
            OutData := OutBuf + SizeOf(TFieldInfo);
            SetData(OutData);
          end;
        end;
      end;
      Inc( FldBuf, PFieldInfo(FldBuf)^.DataSize + SizeOf(TFieldInfo) );
    end;

  finally
    StrDispose(OutBuf);
  end;
end;

	{ StoredProc methods }

procedure TIOraDataSet.SpCreateParamDesc;
const
  MaxSPParamCount = 50;
  MaxSPParamName = 30;
var
  rcd: sword;
  ovrld:	array[0..MaxSPParamCount-1] of ub2;
  pos:		array[0..MaxSPParamCount-1] of ub2;
  level:	array[0..MaxSPParamCount-1] of ub2;
  argname:	array[0..MaxSPParamCount-1, 0..MaxSPParamName-1] of Char;
  arnlen:	array[0..MaxSPParamCount-1] of ub2;
  dtype:	array[0..MaxSPParamCount-1] of ub2;
  defsup:	array[0..MaxSPParamCount-1] of ub1;
  mode: 	array[0..MaxSPParamCount-1] of ub1;
  dtsize:	array[0..MaxSPParamCount-1] of ub4;
  prec: 	array[0..MaxSPParamCount-1] of sb2;
  scale:	array[0..MaxSPParamCount-1] of sb2;
  radix:	array[0..MaxSPParamCount-1] of ub1;
  spare:	array[0..MaxSPParamCount-1] of ub4;
  arrsize: ub4;
  i: Integer;
  ft: TFieldType;
  pt: TSDHelperParamType;
  sParamName: string;
begin
  arrsize := MaxSPParamCount;

{ ATTENTION !!!
  if parameter of the stored procedure is declared as
	OutField2 out NUMBER, then function parameters dtsize, prec, scale is equal 0(zero)
  if parameter of the stored procedure is declared as
  	OutField2 out TEST_TBL.FSTR%TYPE, then returns actual values of the function parameters
}
  rcd := SDOra.odessp( POraLogon(SrvDatabase.FHandle)^.Lda, PChar(StoredProc.StoredProcName), -1, nil, 0, nil, 0,
  	@ovrld[0], @pos[0], @level[0], Pointer(@argname[0]), @arnlen,
        @dtype, @defsup, @mode, @dtsize[0], @prec, @scale,
        @radix, @spare, @arrsize );
  if rcd <> 0 then
    Check( POraLogon(SrvDatabase.FHandle)^.Lda.rc );
	// In this case procedure has no parameters. It's meaningful for procedures from packages
  if (arrsize = 1) and (dtype[0] = 0) then
    Exit;

  for i:=0 to arrsize-1 do with StoredProc do begin
    sParamName := Copy(argname[i], 0, arnlen[i]);
    ft := SrvDatabase.FieldDataType(dtype[i]);
    if ft = ftUnknown then
      DatabaseErrorFmt( SBadFieldType, [sParamName] );
    ft := SrvDatabase.ExactNumberDataType(ft, prec[i], scale[i]);
    pt := ptUnknown;
    if pos[i] = 0 then
      pt := ptResult
    else
      case mode[i] of
        0: pt := ptInput;
        1: pt := ptOutput;
        2: pt := ptInputOutput;
      else
        DatabaseErrorFmt( SBadParameterType, [sParamName] );
      end;

    if (pt = ptResult) and (sParamName = '') then
      sParamName := SResultName;

    SpAddParam( sParamName, ft, pt );
  end;
  	// move Result in the end of parameters list
  with StoredProc do begin
    for i:=0 to ParamCount-1 do with StoredProc do
      if (Params[i].ParamType = ptResult) and (i < (ParamCount-1)) then begin
{$IFDEF SD_VCL4}
        Params.Items[i].Index := ParamCount-1;
{$ELSE}
        //Params.Items.Move(i, ParamCount-1);
{$ENDIF}
        Break;
      end;
  end;
  FBoundParams := False;  
end;

procedure TIOraDataSet.SpBindParams;
var
  CurPtr, DataPtr: Pointer;
  i: Integer;
  sqlvar: string;
  DataLen: Word;
begin
  if FBindBuffer = nil then Exit;
  CurPtr := FBindBuffer;

  for i:=0 to StoredProc.ParamCount-1 do
    with StoredProc.Params[i] do begin
      if IsNull then
        TFieldInfo(CurPtr^).FetchStatus := -1
      else
        TFieldInfo(CurPtr^).FetchStatus := 0;
      DataLen := SpParamBindSize( StoredProc.Params[i] );
      TFieldInfo(CurPtr^).DataSize := 0;
      DataPtr := Pointer(Integer(CurPtr) + SizeOf(TFieldInfo));

      sqlvar := ':' + Name;

      case DataType of
        ftString:
          if not IsNull then begin
          	// DataLen is always equal MinStrParamSize+1(with '\0'), but string length may be less or more
            if DataLen > (Length(AsString) + 1) then
              StrMove(DataPtr, PChar(AsString), Length(AsString)+1)	// includes '\0'
            else begin
              StrMove(DataPtr, PChar(AsString), DataLen);
              PChar(DataPtr)[DataLen-1] := #$0;	// DataLen is buffer size including zero-terminator
            end;
          end;
        ftInteger:
          if not IsNull then DWORD(DataPtr^) := AsInteger;
        ftSmallInt:
          if not IsNull then WORD(DataPtr^) := AsInteger;
        ftDate, ftTime, ftDateTime:
          if not IsNull then CnvtDateTime2DBDateTime(DataType, AsDateTime, DataPtr, SrvDatabase.NativeDataSize(DataType));
        ftFloat:
          if not IsNull then Double(DataPtr^) := AsFloat;
        ftCursor:
		// for avoid "invalid cursor" error, because the Oracle server
                //doesn't accept NULL as an input value for a REF Cursor type.
          if IsNull then
            TFieldInfo(CurPtr^).FetchStatus := 0
          else
            GetData( DataPtr );	// GetData returns nothing for DataType = ftCursor in D3, C3, D4
        else
          if not IsSupportedBlobTypes(DataType) then
            raise EDatabaseError.CreateFmt(SNoParameterDataType, [Name]);
      end;
      if IsBlobType(DataType) then begin
      	// if OCI ver. >= 7.3
        if dwLoadedOCI >= MakeVer(7, 3) then
          Check( obindps(POraCursor(Handle)^, 0, PChar(sqlvar), -1, nil, MAX_LONG_COL_SIZE,
        		SrvDatabase.NativeDataType(DataType), 0,
                        @TFieldInfo(CurPtr^).FetchStatus, nil, nil, 0, 0, 0, 0, 0, nil, nil, 0, 0) )
        else begin
          if TVarData(Value).VType <> varString then
            VarAsType(Value, varString);
          Check( obndra(POraCursor(Handle)^, PChar(sqlvar), -1,
          		PChar(TVarData(Value).VString), GetDataSize,
        		SrvDatabase.NativeDataType(DataType), -1,
                        @TFieldInfo(CurPtr^).FetchStatus, nil, nil, 0, nil, nil, 0, 0) );
        end;
      end else begin
      	// if OCI ver. >= 7.3
        if dwLoadedOCI >= MakeVer(7, 3) then
          Check( obindps(POraCursor(Handle)^, 1, PChar(sqlvar), -1, DataPtr, DataLen,
        		SrvDatabase.NativeDataType(DataType), 0,
                        @TFieldInfo(CurPtr^).FetchStatus, nil, nil, 0, 0, 0, 0, 0, nil, nil, 0, 0) )
        else
          Check( obndra(POraCursor(Handle)^, PChar(sqlvar), -1, DataPtr, DataLen,
        		SrvDatabase.NativeDataType(DataType), 0,
                        @TFieldInfo(CurPtr^).FetchStatus,
                        nil, nil, 0, nil, nil, 0, 0) );
      end;
      Inc( Integer(CurPtr), SizeOf(TFieldInfo) + DataLen );
    end;

  FBoundParams := True;
end;

procedure TIOraDataSet.SpPrepareProc;
begin
  if FHandle = nil then Connect;

  if StoredProc.Params.Count = 0 then
    SpCreateParamDesc;

  FStmt := MakeStoredProcStmt( StoredProc );
  Check( SDOra.oparse(POraCursor(Handle)^, PChar(FStmt), -1, DEFERRED_PARSE, ORACLE7_PARSE) );
end;

procedure TIOraDataSet.SpExecute;
var
  rcd: sword;
  i: Integer;
  sBlob: string;
begin
  rcd := SDOra.oexec(POraCursor(Handle)^);
  if rcd <> 0 then
    rcd := POraCursor(Handle)^.rc;
    	// It's writing of the blob parameter
  if rcd = OCI_MORE_INSERT_PIECES then with StoredProc do begin
    for i:=0 to ParamCount-1 do
      if IsSupportedBlobTypes(Params[i].DataType) then begin
        sBlob := Params[i].AsBlob;
        WriteBlobByName(Params[i].Name, PChar(sBlob)^, Length(sBlob));
      end;
  end else if rcd = OCI_MORE_FETCH_PIECES then begin	// It's reading of the blob-parameter
    DatabaseError(SNoCapability);
  end else if rcd <> 0 then
    Check( rcd );
end;

procedure TIOraDataSet.SpExecProc;
begin
  SpBindParams;
  SpExecute;
  SpGetParams;
  
  CloseParamHandle;
end;

procedure TIOraDataSet.SpGetResults;
begin
end;

// Coping from FParamsBuffer to Params
procedure TIOraDataSet.SpGetParams;
var
  I: Integer;
  FldBuf, OutBuf, OutData: PChar;
  TmpField: TField;
  FieldCls: TFieldClass;
begin
  if FBindBuffer = nil then Exit;
	// Alloc buffer for parameter of max size
  OutBuf := StrAlloc(MinStrParamSize + SizeOf(TFieldInfo));

  try
    FldBuf := FBindBuffer;
    for I := 0 to StoredProc.ParamCount - 1 do with StoredProc.Params[I] do begin
      if ParamType in [ptResult, ptInputOutput, ptOutput] then begin
        if PFieldInfo(FldBuf)^.DataSize = 0 then
          PFieldInfo(FldBuf)^.DataSize := SpParamBindSize(StoredProc.Params[I]);

        if DataType = ftCursor then begin
          if FParamHandle = nil then
            FParamHandle := PCdaDef( FldBuf+SizeOf(TFieldInfo) );
        end else begin
          FieldCls := DefaultFieldClasses[DataType];
          TmpField := FieldCls.Create(nil);
          try
            CnvtDBField2Field(TmpField, FldBuf, OutBuf);
          finally
            TmpField.Free;
          end;

          if FieldIsNull(OutBuf) then
            Clear
          else begin
            OutData := OutBuf + SizeOf(TFieldInfo);
            SetData(OutData);
          end;
        end;
      end;
      Inc( FldBuf, SpParamBindSize(StoredProc.Params[I]) + SizeOf(TFieldInfo) );
    end;

  finally
    StrDispose(OutBuf);
  end;
end;

// for stored procedure with Cursor parameter
procedure TIOraDataSet.CloseParamHandle;
begin
  if FParamHandle = nil then Exit;
	// closing cursor, returned from stored procedure
  Check( SDOra.oclose( POraCursor(FParamHandle)^ ) );
  FParamHandle := nil;
end;

procedure TIOraDataSet.CloseResultSet;
begin
  FIsCursorParamExists := False;

  CloseParamHandle;
	// reset FieldDefs for stored procedure, which returns result set (has cursor parameter)
  if (DataSet is TSDStoredProc) and (DataSet.FieldDefs.Count > 0) then
    DataSet.FieldDefs.Clear;
end;

procedure TIOraDataSet.FreeBindBuffer;
begin
  FBoundParams := False;
  
  CloseParamHandle;

  inherited;
end;


{ TIOra8Database }
constructor TIOra8Database.Create(ADatabase: TSDDatabase);
begin
  inherited Create(ADatabase);

  FHandle 	:= nil;
  FEnvHandle	:= nil;
  FErrHandle	:= nil;

  FSharedInfo	:= nil;    
end;

destructor TIOra8Database.Destroy;
begin
  FreeSharedInfo;

  inherited Destroy;
end;

function TIOra8Database.CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet;
begin
  Result := TIOra8DataSet.Create( ADataSet );
end;

procedure TIOra8Database.Check(Status: TSDEResult);
begin
  CheckExt(Status, nil);
end;

procedure TIOra8Database.CheckExt(Status: TSDEResult; AStmt: POCIStmt);
begin
  ResetIdleTimeOut;

  if (Status = OCI_SUCCESS) or (Status = OCI_SUCCESS_WITH_INFO) then
    Exit;
  ResetBusyState;

  if Assigned( FErrHandle ) then
    OraErrorOCI8( FErrHandle, Status, AStmt )
  else
    OraErrorOCI8( Pointer(FEnvHandle), Status, AStmt );	// for OCIEnvInit and OCIHandleAlloc calls
end;

function TIOra8Database.IsOra8BlobMode: Boolean;
begin
  Result := Oracle8Blobs;
end;

{ If a datatype is accessible through LOB locator }
function TIOra8Database.IsOra8BlobType(ADataType: TFieldType): Boolean;
begin
  Result := (IsBlobType(ADataType) and IsOra8BlobMode)
{$IFDEF SD_VCL5} or (ADataType in [ftOraBlob, ftOraClob]) {$ENDIF};
end;

function TIOra8Database.GetHandle: PSDHandleRec;
var
  s: POra8SrvInfo;
begin
  Result := nil;
  if not Assigned(FHandle) then
    Exit;

  New(FSharedInfo);
  FillChar( FSharedInfo^, SizeOf(FSharedInfo^), $0 );
  FSharedInfo^.SrvType := Ord( Database.ServerType );
  New(s);
  FillChar( s^, SizeOf(s^), $0 );
  FSharedInfo^.SrvInfo := s;
  s^.phEnv := FEnvHandle;
  s^.phErr := FErrHandle;
  s^.phSrv := FHandle^.phSrv;
  s^.phSvc := FHandle^.phSvc;
  s^.phUsr := FHandle^.phUsr;

  Result := FSharedInfo;
end;

procedure TIOra8Database.FreeSharedInfo;
begin
  if Assigned(FSharedInfo) then begin
    if Assigned(FSharedInfo^.SrvInfo) then
      Dispose( POra8SrvInfo(FSharedInfo^.SrvInfo) );
    Dispose( FSharedInfo );
    FSharedInfo := nil;
  end;
	// if Database.FAcquiredHandle = True
  if AcquiredHandle and (FHandle <> nil) then begin
    Dispose( FHandle );
    FHandle := nil;
  end;
end;

procedure TIOra8Database.SetHandle(AHandle: PSDHandleRec);
var
  s: POra8SrvInfo;
begin
  if not Assigned(AHandle) then
    Exit;

  s := PSDHandleRec(AHandle)^.SrvInfo;

  FEnvHandle	:= s^.phEnv;
  FErrHandle	:= s^.phErr;

  AllocHandle;

  FHandle^.phSrv:= s^.phSrv;
  FHandle^.phSvc:= s^.phSvc;
  FHandle^.phUsr:= s^.phUsr;
end;

procedure TIOra8Database.GetStmtResult(const Stmt: string; List: TStrings);
var
  ft: TFieldType;
  hStmt: POCIStmt;
  phCol: POCIParam;
  phDefn: POCIDefine;
  NumCols: ub4;
  dtype, dsize: ub2;
  dispsize: sb4;
  szData: PChar;
begin
  szData := nil;
  Check( OCIHandleAlloc( Pdvoid(FEnvHandle), PPdvoid(@hStmt), OCI_HTYPE_STMT, 0, nil ) );
  try
  	// prepare & execute
    Check( OCIStmtPrepare( hStmt, FErrHandle, PChar(Stmt), Length(Stmt), OCI_NTV_SYNTAX, OCI_DEFAULT ) );
    Check( OCIStmtExecute( FHandle^.phSvc, hStmt, FErrHandle, 0, 0, nil, nil, OCI_DEFAULT ) );
	// get column number
    Check( OCIAttrGet( Pdvoid(hStmt), OCI_HTYPE_STMT, @NumCols, nil, OCI_ATTR_PARAM_COUNT, FErrHandle ) );
	// get info for the 1st column only and set select buffer for it
    if NumCols > 0 then begin
      Check( OCIParamGet( Pdvoid(hStmt), OCI_HTYPE_STMT, FErrHandle, Pdvoid(phCol), 1 ) );

      Check( OCIAttrGet( Pdvoid(phCol), OCI_DTYPE_PARAM,
    			 @dtype, nil, OCI_ATTR_DATA_TYPE, FErrHandle ) );
      Check( OCIAttrGet( Pdvoid(phCol), OCI_DTYPE_PARAM,
    			 @dsize, nil, OCI_ATTR_DATA_SIZE, FErrHandle ) );

      ft := FieldDataType(dtype);
      dispsize := 0;
      if (ft <> ftString) or (ft = ftUnknown) then begin
        Check( OCIAttrGet( Pdvoid(phCol), OCI_DTYPE_PARAM,
      			 @dispsize, nil, OCI_ATTR_DISP_SIZE, FErrHandle ) );
      end;
      if dispsize < dsize then
        dispsize := dsize;
      szData := StrAlloc( dispsize );

      Check( OCIDefineByPos( hStmt, phDefn, FErrHandle, 1,
          	szData, dispsize, NativeDataType(ftString),
                nil, @dsize, nil, OCI_DEFAULT ) );
    end;
	// fetch
    dsize := 0;
    while OCIStmtFetch( hStmt, FErrHandle, 1, OCI_FETCH_NEXT, OCI_DEFAULT ) = OCI_SUCCESS do begin
      if dsize > 0 then
        List.Add( StrPas(szData) );
    end;

  finally
    if szData <> nil then
      StrDispose(szData);
    Check( OCIHandleFree( Pdvoid(hStmt), OCI_HTYPE_STMT ) );
  end;
end;

procedure TIOra8Database.AllocHandle;
begin
  New( POCI8Logon(FHandle) );
  FillChar( POCI8Logon(FHandle)^, SizeOf(POCI8Logon(FHandle)^), $0 );
end;

procedure TIOra8Database.FreeHandles(Force: Boolean);
var
  rcd: TSDEResult;
begin
	// if IsXAConn is True, then phUsr and phSrv are not assigned
  if FHandle^.phUsr <> nil then begin
  	// In case of hang up returns an error (for example, ORA-00028).
        //However it's a successful disconnect, so the error is not raised
    rcd := OCISessionEnd( FHandle^.phSvc, FErrHandle, FHandle^.phUsr, OCI_DEFAULT );
    if not Force then
      Check( rcd );
    Check( OCIHandleFree( Pdvoid(FHandle^.phUsr), OCI_HTYPE_SESSION ) );
    FHandle^.phUsr := nil;
  end;
  if FHandle^.phSrv <> nil then begin
    rcd := OCIServerDetach( FHandle^.phSrv, FErrHandle, OCI_DEFAULT );
    if not Force then
      Check( rcd );
    Check( OCIHandleFree( Pdvoid(FHandle^.phSrv), OCI_HTYPE_SERVER ) );
    FHandle^.phSrv := nil;
  end;
  if not IsXAConn and (FHandle^.phSvc <> nil) then begin
    Check( OCIHandleFree( Pdvoid(FHandle^.phSvc), OCI_HTYPE_SVCCTX ) );
    FHandle^.phSvc := nil;
  end;
	// OCI will deallocate all child handles (including FErrHandle)
  if not IsXAConn and (FEnvHandle <> nil) then begin
    Check( OCIHandleFree( Pdvoid(FEnvHandle), OCI_HTYPE_ENV ) );
    FEnvHandle := nil;
    FErrHandle := nil;
  end;
end;

procedure TIOra8Database.Logon(const sRemoteDatabase, sUserName, sPassword: string);
var
  AuthentType, SessMode: ub4;
  rc: sword;
  sRole, sNewPass: string;
begin
  try
    AllocHandle;

    LoadSqlLib;

    if IsXAConn then begin
      if @xaoEnv = nil then
        DatabaseErrorFmt( SErrFuncNotFound, ['xaoEnv'] );
      if @xaoSvcCtx = nil then
        DatabaseErrorFmt( SErrFuncNotFound, ['xaoSvcCtx'] );

      FEnvHandle 	:= xaoEnv( PChar(sRemoteDatabase) );
      if FEnvHandle = nil then
        DatabaseErrorFmt( SFuncFailed, ['xaoEnv'] );
      FHandle^.phSvc 	:= xaoSvcCtx( PChar(sRemoteDatabase) );
      if FHandle^.phSvc = nil then
        DatabaseErrorFmt( SFuncFailed, ['xaoSvcCtx'] );
        
      Check( OCIHandleAlloc( Pdvoid(FEnvHandle), PPdvoid(@FErrHandle), OCI_HTYPE_ERROR, 0, nil ) );
    end else begin
      if not Assigned( @OCIEnvCreate ) then
        Check( OCIEnvInit( FEnvHandle, OCI_DEFAULT, 0, nil ) )
      else
        Check( OCIEnvCreate( FEnvHandle, OCI_THREADED or OCI_OBJECT, nil, nil, nil, nil, 0, nil ) );

      Check( OCIHandleAlloc( Pdvoid(FEnvHandle), PPdvoid(@FErrHandle), OCI_HTYPE_ERROR, 0, nil ) );
      Check( OCIHandleAlloc( Pdvoid(FEnvHandle), PPdvoid(@FHandle^.phSvc), OCI_HTYPE_SVCCTX, 0, nil ) );
      Check( OCIHandleAlloc( Pdvoid(FEnvHandle), PPdvoid(@FHandle^.phSrv), OCI_HTYPE_SERVER, 0, nil ) );

      Check( OCIServerAttach( FHandle^.phSrv, FErrHandle, PChar(sRemoteDatabase), Length(sRemoteDatabase), OCI_DEFAULT ) );
      Check( OCIAttrSet( Pdvoid(FHandle^.phSvc), OCI_HTYPE_SVCCTX, Pdvoid(FHandle^.phSrv), 0, OCI_ATTR_SERVER, FErrHandle ) );

      Check( OCIHandleAlloc( Pdvoid(FEnvHandle), PPdvoid(@FHandle^.phUsr), OCI_HTYPE_SESSION, 0, nil ) );
      Check( OCIAttrSet( Pdvoid(FHandle^.phUsr), OCI_HTYPE_SESSION, Pdvoid( PChar(sUserName) ), Length(sUserName), OCI_ATTR_USERNAME, FErrHandle ) );
      Check( OCIAttrSet( Pdvoid(FHandle^.phUsr), OCI_HTYPE_SESSION, Pdvoid( PChar(sPassword) ), Length(sPassword), OCI_ATTR_PASSWORD, FErrHandle ) );
	// OS Authentication, when a password is empty (name of the current OS user is used)
      if Trim(sPassword) = ''
      then AuthentType := OCI_CRED_EXT
      else AuthentType := OCI_CRED_RDBMS;
      sRole := Trim( Database.Params.Values[szROLENAME] );
      if sRole = 'SYSDBA' then
        SessMode := OCI_SYSDBA
      else if sRole = 'SYSOPER' then
        SessMode := OCI_SYSOPER
      else
        SessMode := OCI_DEFAULT;
      Check( OCIAttrSet( Pdvoid(FHandle^.phSvc), OCI_HTYPE_SVCCTX, Pdvoid(FHandle^.phUsr), 0, OCI_ATTR_SESSION, FErrHandle ) );
      rc := OCISessionBegin( FHandle^.phSvc, FErrHandle, FHandle^.phUsr, AuthentType, SessMode );

      if (rc = OCI_ERROR) and (OCI8ErrorCode(FErrHandle, rc) = ORA_ERR_PASSWORD_EXPIRED) and (Trim( Database.Params.Values[szNEWPASSWORD] ) <> '') then begin
        sNewPass := Trim( Database.Params.Values[szNEWPASSWORD] );
        Check( OCIPasswordChange( FHandle^.phSvc, FErrHandle, PChar(sUserName), Length(sUserName),
                PChar(sPassword), Length(sPassword), PChar(sNewPass), Length(sNewPass), OCI_AUTH )
        );
      end else
        Check( rc );
    end;
  except
    FreeHandles( True );
    FreeSqlLib;

    if FHandle <> nil then Dispose( FHandle );
    FHandle := nil;

    raise;
  end;
end;

procedure TIOra8Database.Logoff(Force: Boolean);
begin
  if FHandle <> nil then begin
    if Database.InTransaction then
      OCITransRollback( FHandle^.phSvc, FErrHandle, OCI_DEFAULT );
	// the current transaction is implicitly committed by OCISessionEnd call          
    FreeHandles( Force );
    Dispose( FHandle );
  end;
  FHandle := nil;
  FreeSharedInfo;

  FreeSqlLib;
end;

function TIOra8Database.IsLocFieldSubType(ASubType: Word): Boolean;
begin
  Result := ASubType in [fldstHMEMO, fldstHBINARY, fldstHCFILE, fldstHBFILE];
end;

function TIOra8Database.IsFileLocFieldSubType(ASubType: Word): Boolean;
begin
  Result := ASubType in [fldstHCFILE, fldstHBFILE];
end;

function TIOra8Database.FieldDataType(ExtDataType: Integer): TFieldType;
begin
  case ExtDataType of
  	// Oracle8 ROWID
    SQLT_RDD: Result := ftString;
                // character lob
    SQLT_CLOB,
    SQLT_CFILE:	Result :=
        {$IFDEF SD_VCL5}ftOraClob
        {$ELSE}         ftMemo {$ENDIF};
                // binary lob
    SQLT_BLOB,
    SQLT_BFILE:  Result :=
        {$IFDEF SD_VCL5}ftOraBlob
        {$ELSE}         ftBlob {$ENDIF};
  else
    Result := inherited FieldDataType( ExtDataType );
  end;
end;

function TIOra8Database.FieldSubType(ExtDataType: Integer): Word;
begin
  case ExtDataType of
    SQLT_CLOB:	Result := fldstHMEMO;	// character lob
    SQLT_BLOB:	Result := fldstHBINARY;	// binary lob
    SQLT_CFILE:	Result := fldstHCFILE;	// locator to a character file
    SQLT_BFILE:	Result := fldstHBFILE;	// locator to a binary file
  else
    Result := inherited FieldSubType( ExtDataType );
  end;
end;

function TIOra8Database.NativeDataSize(FieldType: TFieldType): Word;
begin
	// REF CURSOR datatype for OCI8
  if FieldType = ftCursor then
    Result := SizeOf( POCIStmt )
  else
    Result := inherited NativeDataSize(FieldType);
end;

function TIOra8Database.NativeDataType(FieldType: TFieldType): Integer;
begin
  case FieldType of
    ftCursor:
      Result := SQLT_RSET;
    ftBlob:
      if IsOra8BlobMode
      then Result := SQLT_BLOB
      else Result := inherited NativeDataType(FieldType);
    ftMemo:
      if IsOra8BlobMode
      then Result := SQLT_CLOB
      else Result := inherited NativeDataType(FieldType);
  else
    Result := inherited NativeDataType(FieldType);
  end;
end;

function TIOra8Database.RequiredCnvtFieldType(FieldType: TFieldType): Boolean;
begin
  if IsOra8BlobType(FieldType) then
    Result := True
  else
    Result := inherited RequiredCnvtFieldType(FieldType);
end;

procedure TIOra8Database.StartTransaction;
begin

end;

procedure TIOra8Database.Commit;
begin
  Check( OCITransCommit( FHandle^.phSvc, FErrHandle, OCI_DEFAULT ) );
end;

procedure TIOra8Database.Rollback;
begin
  Check( OCITransRollback( FHandle^.phSvc, FErrHandle, OCI_DEFAULT ) );
end;

procedure TIOra8Database.SetDefaultParams;
begin

end;


{ TIOra8DataSet }
constructor TIOra8DataSet.Create(ADataSet: TSDDataSet);
begin
  inherited Create(ADataSet);

  FHandle	:= nil;
  FParamHandle	:= nil;
  FStmt		:= '';
  FBoundParams	:= False;  
end;

destructor TIOra8DataSet.Destroy;
begin
  inherited;
end;

function TIOra8DataSet.GetHandle: PSDCursor;
begin
  Result := FHandle;
  if FParamHandle <> nil then
    Result := FParamHandle;
end;

function TIOra8DataSet.GetphError: POCIError;
begin
  Result := SrvDatabase.FErrHandle;
end;

function TIOra8DataSet.GetphSvcCtx: POCISvcCtx;
begin
  Result := POCI8Logon(SrvDatabase.FHandle)^.phSvc;
end;

function TIOra8DataSet.GetSrvDatabase: TIOra8Database;
begin
  Result := (inherited SrvDatabase) as TIOra8Database;
end;

procedure TIOra8DataSet.Check(Status: TSDEResult);
begin
  SrvDatabase.Check( Status );
end;

procedure TIOra8DataSet.CheckStmt(Status: TSDEResult);
begin
  SrvDatabase.CheckExt( Status, Handle );
end;

procedure TIOra8DataSet.Connect;
begin
  Check( OCIHandleAlloc( Pdvoid(SrvDatabase.FEnvHandle), PPdvoid(@FHandle), OCI_HTYPE_STMT, 0, nil ) );
end;

procedure TIOra8DataSet.Disconnect(Force: Boolean);
begin
  if FHandle = nil then
    Exit;

  Check( OCIHandleFree( Pdvoid(FHandle), OCI_HTYPE_STMT ) );
  FHandle := nil;
end;

procedure TIOra8DataSet.CloseParamHandle;
begin
  if FParamHandle = nil then
    Exit;

  Check( OCIHandleFree( Pdvoid(FParamHandle), OCI_HTYPE_STMT ) );
  FParamHandle := nil;
end;

procedure TIOra8DataSet.CloseResultSet;
begin
  CloseParamHandle;
  FreeSelectHLob;
	// reset FieldDefs for stored procedure, which returns result set (has cursor parameter)
  if (DataSet is TSDStoredProc) and (DataSet.FieldDefs.Count > 0) then
    DataSet.FieldDefs.Clear;
end;

procedure TIOra8DataSet.FreeBindBuffer;
begin
  FBoundParams := False;

  CloseParamHandle;

  inherited;
end;

procedure TIOra8DataSet.GetFieldDescs(Descs: TSDFieldDescList);
var
  pFieldDesc: PSDFieldDesc;
  ft: TFieldType;
  i, NumCols: ub4;
  phCol: POCIParam;
  dtype, dsize: ub2;
  dispsize: sb4;
  scale: sb1;
  prec, NullOk: ub1;
  szColName: PChar;
  ColNameLen, MaxColNameLen: ub4;
begin
	// get describe info
  if IsSelectStmt then
    CheckStmt( OCIStmtExecute( phSvcCtx, FHandle, phError, 0, 0, nil, nil, OCI_DESCRIBE_ONLY ) )
  else if IsCursorParamExists then begin
    if (DataSet is TSDStoredProc) and not FBoundParams then
      SpBindParams;
    Execute;
    if DataSet is TSDStoredProc then
      SpGetParams;
  end;

	// get column number
  Check( OCIAttrGet( Pdvoid(Handle), OCI_HTYPE_STMT, @NumCols, nil, OCI_ATTR_PARAM_COUNT, phError ) );
  MaxColNameLen := SrvDatabase.MaxFieldNameLen;

  for i:=1 to NumCols do begin
    Check( OCIParamGet( Pdvoid(Handle), OCI_HTYPE_STMT, phError, Pdvoid(phCol), i ) );

    szColName := nil;
    Check( OCIAttrGet( Pdvoid(phCol), OCI_DTYPE_PARAM,
    			 dvoid(@szColName), @ColNameLen, OCI_ATTR_NAME, phError ) );
    Check( OCIAttrGet( Pdvoid(phCol), OCI_DTYPE_PARAM,
    			 @dtype, nil, OCI_ATTR_DATA_TYPE, phError ) );
    Check( OCIAttrGet( Pdvoid(phCol), OCI_DTYPE_PARAM,
    			 @dsize, nil, OCI_ATTR_DATA_SIZE, phError ) );
    Check( OCIAttrGet( Pdvoid(phCol), OCI_DTYPE_PARAM,
    			 @prec, nil, OCI_ATTR_PRECISION, phError ) );
    Check( OCIAttrGet( Pdvoid(phCol), OCI_DTYPE_PARAM,
    			 @scale, nil, OCI_ATTR_SCALE, phError ) );
    Check( OCIAttrGet( Pdvoid(phCol), OCI_DTYPE_PARAM,
    			 @NullOk, nil, OCI_ATTR_IS_NULL, phError ) );

    ft := SrvDatabase.FieldDataType(dtype);
    if ft = ftUnknown then
      DatabaseErrorFmt( SBadFieldType, [szColName] );
    ft := SrvDatabase.ExactNumberDataType(ft, prec, scale);
	// in case of Oracle8 ROWID, get size of string form, so ROWID is binded as string
    if dtype = SQLT_RDD then begin
      Check( OCIAttrGet( Pdvoid(phCol), OCI_DTYPE_PARAM,
    			 @dispsize, nil, OCI_ATTR_DISP_SIZE, phError ) );
      dsize := dispsize;
    end;

    New( pFieldDesc );
    with pFieldDesc^ do begin
      FieldName	:= Copy( szColName, 1, MinIntValue(ColNameLen, MaxColNameLen) );
      FieldNo	:= i;
      DataType	:= ft;
      FldType	:= dtype;
      SubType	:= SrvDatabase.FieldSubType(dtype);
      if ft = ftBCD then	// TBCDField.Size indicates the number digits after the decimal place
        Size	:= scale
      else
        Size   	:= dsize;
      Precision	:= prec;
  	// if NullOk = 0 then null values are not permitted for the column (Required = True)
      Required	:= NullOk = 0;
    end;
    Descs.Add( pFieldDesc );
  end;
end;

function TIOra8DataSet.NativeFieldDataSize(Field: TField): Word;
begin
  with Field do begin
    if IsSupportedBlobTypes(DataType) and
       (FieldDescByName(FieldName).SubType in [fldstHMEMO, fldstHBINARY, fldstHCFILE, fldstHBFILE])
    then begin
      Result := SizeOf( POCILobLocator )
    end else
      Result := inherited NativeFieldDataSize(Field);
  end;
end;

function TIOra8DataSet.NativeFieldOffset(FieldNo: Integer): Integer;
var
  i, nOffset: Integer;
  TmpField: TField;
  PrgDataSize: Word;
begin
  Result := -1;
  nOffset := 0;		// offset to the TFieldInfo

  for i:=0 to DataSet.FieldDefs.Count-1 do begin
    TmpField := FieldByNumber(DataSet.FieldDefs[i].FieldNo);
    if Assigned(TmpField) then begin
    	// Skip calculated fields
      if TmpField.Calculated then Continue;

      if TmpField.FieldNo = FieldNo then begin
        Result := nOffset;
        Break;
      end;

      PrgDataSize := NativeFieldDataSize(TmpField);

      Inc(nOffset, SizeOf(TFieldInfo) + PrgDataSize);
    end;
  end;
end;

procedure TIOra8DataSet.SetSelectBuffer;
var
  i, nOffset: Integer;
  TmpField: TField;
  PrgDataSize: Word;
  PrgDataType: Integer;
  PrgDataBuffer: PChar;
  phDefn: POCIDefine;
  phLob: POCILobLocator;
begin
  nOffset := 0;		// pointer to the TFieldInfo

  for i:=0 to DataSet.FieldDefs.Count-1 do begin
    TmpField := FieldByNumber(DataSet.FieldDefs[i].FieldNo);
    if Assigned(TmpField) then with TmpField do begin
    	// Skip calculated fields
      if Calculated then Continue;

      PrgDataType := SrvDatabase.NativeDataType(DataType);
      if PrgDataType = 0 then
        DatabaseErrorFmt( SUnknownFieldType, [FieldName] );

      PrgDataSize := NativeFieldDataSize(TmpField);
      PrgDataBuffer := PChar( Integer(FSelectBuffer) + nOffset + SizeOf(TFieldInfo) );

      if IsSupportedBlobTypes(DataType)  then begin
      		// if LONG/LONG RAW column type
        if FieldDescByName(FieldName).SubType in [fldstMEMO, fldstBINARY] then
          Check( OCIDefineByPos( Handle, phDefn, phError, FieldNo,
          	nil, MAX_LONG_COL_SIZE, PrgDataType,
                @PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.FetchStatus,
                @PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.DataSize,
                nil, OCI_DYNAMIC_FETCH ) )
        else begin
          if SrvDatabase.IsFileLocFieldSubType( FieldDescByName(FieldName).SubType ) then
            Check( OCIDescriptorAlloc(Pdvoid(SrvDatabase.FEnvHandle), @phLob, OCI_DTYPE_FILE, 0, nil) )
          else
            Check( OCIDescriptorAlloc(Pdvoid(SrvDatabase.FEnvHandle), @phLob, OCI_DTYPE_LOB, 0, nil) );
          POCILobLocator(Pointer(PrgDataBuffer)^) := phLob;
          phLob	:= nil;

          Check( OCIDefineByPos( Handle, phDefn, phError, FieldNo,
          	PrgDataBuffer, PrgDataSize, FieldDescByName(FieldName).FldType,
                @PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.FetchStatus,
                @PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.DataSize,
                nil, OCI_DEFAULT ) )
        end;
      end else begin
        Check( OCIDefineByPos( Handle, phDefn, phError, FieldNo,
          	PrgDataBuffer, PrgDataSize, PrgDataType,
                @PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.FetchStatus,
                @PFieldInfo( Integer(PrgDataBuffer) - SizeOf(TFieldInfo) )^.DataSize,
                nil, OCI_DEFAULT ) )
      end;	{ if DataType in SupportedBlobFields then }

      Inc(nOffset, SizeOf(TFieldInfo) + PrgDataSize);
    end;
  end;
end;

procedure TIOra8DataSet.FreeSelectHLob;
var
  i, nOffset: Integer;
  FldDesc: TSDFieldDesc;
  DataPtr: Pointer;
  phLob: POCILobLocator;
begin
	// free Lob descriptors in select buffer
  for i:=0 to DataSet.FieldDefs.Count-1 do with DataSet.FieldDefs[i] do begin
    if not IsSupportedBlobTypes(DataType) then
      Continue;

    FldDesc := FieldDescByName(Name);
    if (FldDesc.SubType in [fldstHMEMO, fldstHBINARY, fldstHCFILE, fldstHBFILE]) then begin
      nOffset := NativeFieldOffset( FldDesc.FieldNo );
      ASSERT( nOffset >= 0 );   // ???

      DataPtr := FSelectBuffer + nOffset + SizeOf(TFieldInfo);
      phLob := POCILobLocator(DataPtr)^;
      Check( OCIDescriptorFree( Pdvoid(phLob), OCI_DTYPE_LOB ) );
      POCILobLocator(DataPtr)^ := nil;
    end;
  end;
end;

{ If a datatype is accessible through LOB locator }
function TIOra8DataSet.IsOra8BlobType(ADataType: TFieldType): Boolean;
begin
  Result := SrvDatabase.IsOra8BlobType(ADataType);
end;

function TIOra8DataSet.IsSelectStmt: Boolean;
var
  StmtType: ub2;
begin
  Result := False;
  if not Assigned(FHandle) then
    Exit;
  Check( OCIAttrGet( Pdvoid(FHandle), OCI_HTYPE_STMT, @StmtType, nil, OCI_ATTR_STMT_TYPE, phError ) );
  Result := StmtType = OCI_STMT_SELECT;
end;

function TIOra8DataSet.IsPLSQLBlock: Boolean;
var
  StmtType: ub2;
begin
  Check( OCIAttrGet( Pdvoid(FHandle), OCI_HTYPE_STMT, @StmtType, nil, OCI_ATTR_STMT_TYPE, phError ) );
  Result := (StmtType = OCI_STMT_BEGIN) or (StmtType = OCI_STMT_DECLARE);
end;

function TIOra8DataSet.IsCursorParamExists: Boolean;
var
  i: Integer;
begin
  Result := False;
  if DataSet is TSDQuery then begin
    for i:=0 to Query.ParamCount-1 do begin
    	// if PL/SQL block with cursor parameter is executes ('begin open_proc(:v_cur); end;')
      Result := Query.Params[i].DataType = ftCursor;
      if Result then
        Break;
    end;
  end else if DataSet is TSDStoredProc then begin
    for i:=0 to StoredProc.ParamCount-1 do begin
	// if PL/SQL procedure returns cursor parameters
      Result := StoredProc.Params[i].DataType = ftCursor;
      if Result then
        Break;
    end;
  end;
end;

function TIOra8DataSet.ResultSetExists: Boolean;
begin
  Result := IsSelectStmt;
  if Result then Exit;

  Result := IsCursorParamExists;
end;

procedure TIOra8DataSet.Execute;
begin
  if DataSet is TSDQuery then begin
    if FParamHandle = nil then
      QExecute
  end else if DataSet is TSDStoredProc then begin
    if FParamHandle = nil then
      SpExecute
  end else
    Check( OCIStmtExecute( phSvcCtx, Handle, phError, 0, 0, nil, nil, GetStmtExecuteMode ) );
end;

function TIOra8DataSet.FetchNextRow: Boolean;
var
  rcd: sword;
begin
  rcd := OCIStmtFetch( Handle, phError, 1, OCI_FETCH_NEXT, OCI_DEFAULT );

  SrvDatabase.ResetIdleTimeOut;

  if (BlobFieldCount > 0) and
     ((rcd = OCI_NEED_DATA) or (rcd = OCI_SUCCESS) or (rcd = OCI_SUCCESS_WITH_INFO))
  then begin
    FetchBlobFields;
    rcd := OCI_SUCCESS;
  end;

  if (rcd <> OCI_SUCCESS) and
     (rcd <> OCI_SUCCESS_WITH_INFO) and
     (rcd <> OCI_NO_DATA)
  then
    Check( rcd );

  Result := (rcd = OCI_SUCCESS) or (rcd = OCI_SUCCESS_WITH_INFO);
	// if EOF, then close Cursor, returned by PL/SQL procedure
  if (not Result) and (FParamHandle <> nil) then
    CloseParamHandle;
end;

function TIOra8DataSet.CnvtDateTime2DBDateTime(ADataType: TFieldType; Value: TDateTime; Buffer: Pointer; BufSize: Integer): Integer;
begin
  Result := SrvDatabase.CnvtDateTime2DBDateTime( ADataType, Value, Buffer, BufSize );
end;

// TDateTimeField selects as TDateTimeRec and converts to TDateTime
function TIOra8DataSet.CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean;
begin
  Result := SrvDatabase.CnvtDBField2Field(AField, InBuf, OutBuf);
end;

function TIOra8DataSet.ReadBlob(FieldNo: Integer; var BlobData: string): Longint;
var
  Len: ub4;       		// count of really read bytes
  Buf: PChar;			// current pointer for read of data
  BlobSize, GrowDelta: Integer;	// current buffer size and increment size
  rcd: sword;
  phDefn: POCIDefine;
  Piece, in_out: ub1;
  ind: sb2;
  htype, iter, idx: ub4;
begin
  Result := 0;
  if FieldDescByName(FieldByNumber(FieldNo).FieldName).SubType in [fldstHMEMO, fldstHBINARY, fldstHCFILE, fldstHBFILE] then begin
    Result := ReadHLobField( FieldNo, BlobData );
    Exit;
  end;
  GrowDelta := $7FFF;
  BlobSize := GrowDelta;

  while True do begin
    SetLength(BlobData, BlobSize);
    Buf := @PChar(BlobData)[BlobSize-GrowDelta];
	// return: in_out = OCI_PARAM_OUT(2), htype = OCI_HTYPE_DEFINE(6)
    Check( OCIStmtGetPieceInfo( FHandle, phError, Pdvoid(phDefn), @htype,
    		@in_out, @iter, @idx, @Piece) );

	// read from database
    Len := GrowDelta;
    Check( OCIStmtSetPieceInfo( Pdvoid(phDefn), htype, phError,
    		Buf, @Len, Piece, @ind, nil ) );
    rcd := OCIStmtFetch( FHandle, phError, 1, OCI_FETCH_NEXT, OCI_DEFAULT );
    Inc(Result, Len);

    if rcd <> OCI_NEED_DATA then
      if rcd = OCI_SUCCESS then
        Break
      else
        Check( rcd );
    Inc(BlobSize, GrowDelta);
  end;

  SetLength(BlobData, Result);
end;

{ Read field data using a locator(BFILE or LOB) }
function TIOra8DataSet.ReadHLobField(FieldNo: Integer; var BlobData: string): Longint;
var
  nOffset: Integer;
  DataPtr: Pointer;
  phLob: POCILobLocator;
  LobSize, ReadBytes: ub4;
  IsFileLoc: Boolean;
begin
  Result := 0;
  nOffset := NativeFieldOffset( FieldNo );
  if nOffset < 0 then Exit;

  DataPtr := FSelectBuffer + nOffset + SizeOf(TFieldInfo);
  if sb2(PFieldInfo(PChar(DataPtr)-SizeOf(TFieldInfo))^.FetchStatus) = OCI_IND_NOTNULL then begin
    phLob := POCILobLocator(DataPtr^);
	// whether a file locator it
    IsFileLoc := SrvDatabase.IsFileLocFieldSubType( FieldDescByName(FieldByNumber(FieldNo).FieldName).SubType );
    	// in case of file locator to open a file
    if IsFileLoc then
      Check( OCILobFileOpen( phSvcCtx, phError, phLob, OCI_FILE_READONLY ) );
    try
      Check( OCILobGetLength( phSvcCtx, phError, phLob, @LobSize ) );
      if LobSize > 0 then begin
        SetLength(BlobData, LobSize);
        ReadBytes := LobSize;
        Check( OCILobRead( phSvcCtx, phError, phLob, @ReadBytes, 1,
    		Pointer(PChar(BlobData)), LobSize, nil, nil, 0, SQLCS_IMPLICIT) );
        Result := LobSize;
      end;
    finally
    	// in case of file locator to close a file
      if IsFileLoc then
        Check( OCILobFileClose( phSvcCtx, phError, phLob ) );
    end;
  end;
end;

{ Get LOB data using a LOB locator, it's used to get LOB parameters from a database }
function TIOra8DataSet.ReadLobData(phLob: POCILobLocator; var BlobData: string): Longint;
var
  LobSize, ReadBytes: ub4;
begin
  Result := 0;

  Check( OCILobGetLength( phSvcCtx, phError, phLob, @LobSize ) );
  if LobSize > 0 then begin
    SetLength(BlobData, LobSize);
    ReadBytes := LobSize;
    Check( OCILobRead( phSvcCtx, phError, phLob, @ReadBytes, 1,
		Pointer(PChar(BlobData)), LobSize, nil, nil, 0, SQLCS_IMPLICIT) );
    Result := LobSize;
  end;
end;

{ Set LOB data using a LOB locator, it's used to pass LOB data in a database }
function TIOra8DataSet.WriteLobData(phLob: POCILobLocator; const Buffer; Count: LongInt): Longint;
begin
  Check( OCILobWrite(phSvcCtx, phError, phLob, @Count, 1, @Buffer, Count, OCI_ONE_PIECE,
  			nil, nil, 0, SQLCS_IMPLICIT) );
  Result := Count;
end;

function TIOra8DataSet.WriteBlob(FieldNo: Integer; const Buffer; Count: LongInt): Longint;
begin
  raise EDatabaseError.CreateFmt(SFuncNotImplemented, ['TIOra8DataSet.WriteBlob']);
end;

{ Writes LONG/LONG RAW datatype }
function TIOra8DataSet.WriteBlobByName(Name: string; const Buffer; Count: LongInt): Longint;
var
  rcd: sword;
  phBind: POCIBind;
  Piece, in_out: ub1;
  ind: sb2;
  htype, iter, idx: ub4;
begin

	// return: in_out = OCI_PARAM_IN(1)/OCI_PARAM_OUT(2), htype = OCI_HTYPE_BIND(5)
  Check( OCIStmtGetPieceInfo( FHandle, phError, Pdvoid(phBind), @htype,
  		@in_out, @iter, @idx, @Piece) );
	// write a block in the database as one piece
  Piece := OCI_LAST_PIECE;
  if Count = 0 then
    Check( OCIStmtSetPieceInfo( Pdvoid(phBind), htype, phError,
  		nil, @Count, Piece, @ind, nil ) )
  else
    Check( OCIStmtSetPieceInfo( Pdvoid(phBind), htype, phError,
  		@Buffer, @Count, Piece, @ind, nil ) );
  rcd := OCIStmtExecute( phSvcCtx, FHandle, phError, 1, 0, nil, nil, OCI_DEFAULT );

  if rcd <> OCI_NEED_DATA then
    Check( rcd );

  Result := Count;
end;

procedure TIOra8DataSet.SetUpdatedLobArray(ACols, ARows: Integer);
var
  i, j: Integer;
  IsColCountChanged, IsRowCountChanged: Boolean;
begin
	// free TOutLocStruct structure
  if (ARows = 0) and (ACols = 0)  then begin
    if FUpdatedLobInfo <> nil then with TOutLocStruct(FUpdatedLobInfo^) do begin
	// free arrays
      for j:=0 to RowCount-1 do
        for i:=0 to ColCount-1 do
          Check( OCIDescriptorFree( Pdvoid(Row[j, i].phLoc), OCI_DTYPE_LOB ) );
      ReallocMem( Row, 0 );
      if Assigned( BindInfo ) then
        ReallocMem( BindInfo, 0 );
    end;

    ReallocMem( FUpdatedLobInfo, 0 );
    FUpdatedLobInfo := nil;
    Exit;
  end;
	// alloc TOutLocStruct structure
  if (FUpdatedLobInfo = nil) and ((ARows > 0) or (ACols > 0))  then begin
    ReallocMem( FUpdatedLobInfo, SizeOf(TOutLocStruct) );
    FillChar( FUpdatedLobInfo^, SizeOf(TOutLocStruct), 0 );
  end;

  ASSERT( FUpdatedLobInfo <> nil );
  with TOutLocStruct(FUpdatedLobInfo^) do begin
  	// it's impossible to reside arrays (only alloc or free)
    ASSERT(
    	( (ACols = 0) or ((ACols > 0) and ((ColCount = 0) or (ColCount = ACols))) ) and
      	( (ARows = 0) or ((ARows > 0) and ((RowCount = 0) or (RowCount = ARows))) )
    );

    IsRowCountChanged := ARows <> RowCount;
    if ARows <> RowCount then
      RowCount := ARows;

    IsColCountChanged := ACols <> ColCount;
    if ACols <> ColCount then begin
      ColCount := ACols;
      ReallocMem( BindInfo, ColCount * SizeOf(TBindLobInfo) );
      	// in case of ColCount = 0
      if BindInfo <> nil then
        FillChar( BindInfo^, ColCount * SizeOf(TBindLobInfo), 0 );
    end;
	// alloc array for descriptors, if any array size was changed
    if (ColCount > 0) and (RowCount > 0) and
       (IsColCountChanged or IsRowCountChanged)
    then begin
      ReallocMem( Row, RowCount*ColCount*SizeOf(TLobInfo) );
      FillChar( Row^, RowCount*ColCount*SizeOf(TLobInfo), 0 );
      for j:=0 to RowCount-1 do
        for i:=0 to ColCount-1 do
          Check( OCIDescriptorAlloc( Pdvoid(SrvDatabase.FEnvHandle), @Row[j, i].phLoc, OCI_DTYPE_LOB, 0, nil ) );
    end;

  end;
end;

procedure TIOra8DataSet.WriteLobParams;
var
  i, j: Integer;
  szData: PChar;
begin
  ASSERT( FUpdatedLobInfo <> nil );
  
  with TOutLocStruct(FUpdatedLobInfo^), Query do begin
    for j:=0 to RowCount-1 do
      for i:=0 to ColCount-1 do begin
        if TVarData(Params[BindInfo[i].ParamIdx].Value).VType <> varString then
          VarAsType(Params[BindInfo[i].ParamIdx].Value, varString);

        szData := TVarData( Params[BindInfo[i].ParamIdx].Value ).VString;
        if szData <> nil then
          WriteLobData( Row[j, i].phLoc, szData^, Params[BindInfo[i].ParamIdx].GetDataSize );
      end;
  end;
end;

		{ Query methods }

procedure TIOra8DataSet.QBindParams;
var
  CurPtr, DataPtr: Pointer;
  i, Ora8LobCount: Integer;
  ArrLobBind: PBindLobArray; 	// index in Params
  sqlvar: string;
  DataLen: sb4;
  phBind: POCIBind;
begin
  if FBindBuffer = nil then Exit;
  CurPtr := FBindBuffer;
  Ora8LobCount := 0;
  ArrLobBind := nil;

  try

  for i:=0 to Query.ParamCount-1 do
    with Query.Params[i] do begin
      phBind := nil;
      if IsNull then
        TFieldInfo(CurPtr^).FetchStatus := OCI_IND_NULL		// = -1
      else
        TFieldInfo(CurPtr^).FetchStatus := OCI_IND_NOTNULL;	// = 0
      TFieldInfo(CurPtr^).DataSize := 0;
      DataLen := QParamBindSize( Query.Params[i] );
      DataPtr := Pointer(Integer(CurPtr) + SizeOf(TFieldInfo));

      sqlvar := IntToStr(i+1);

      case DataType of
        ftString:
          if DataLen > 0 then StrMove(DataPtr, PChar(AsString), DataLen);
        ftInteger:
          if DataLen > 0 then DWORD(DataPtr^) := AsInteger;
        ftSmallInt:
          if DataLen > 0 then WORD(DataPtr^) := AsInteger;
        ftDate, ftTime, ftDateTime:
          if DataLen > 0 then CnvtDateTime2DBDateTime(DataType, AsDateTime, DataPtr, SrvDatabase.NativeDataSize(DataType));
        ftFloat:
          if DataLen > 0 then Double(DataPtr^) := AsFloat;
        ftCursor:
          if FParamHandle = nil then begin
            TFieldInfo(CurPtr^).FetchStatus := OCI_IND_NOTNULL;
            Check( OCIHandleAlloc( Pdvoid(SrvDatabase.FEnvHandle), PPdvoid(@FParamHandle), OCI_HTYPE_STMT, 0, nil ) );
            POCIStmt(DataPtr^)	:= FParamHandle;
            FParamHandle	:= nil;
          end;
        else
          if not IsSupportedBlobTypes(DataType) then
            raise EDatabaseError.CreateFmt(SNoParameterDataType, [Name]);
      end;
      if IsBlobType(DataType) then begin
        if TVarData(Value).VType <> varString then
          VarAsType(Value, varString);
        if IsOra8BlobType(DataType) then begin
          Check( OCIBindByName( Handle, phBind, phError, PChar(sqlvar), Length(sqlvar),
        	nil, SizeOf(OCILobLocator), SrvDatabase.NativeDataType(DataType),
                nil, nil, nil, 0, nil, OCI_DATA_AT_EXEC ) );
          Check( OCIBindDynamic( phBind, phError, nil, cbf_no_data, Pdvoid(Self), cbf_get_lob ) );
          	// save Bind Handle and parameter's index for Lob parameter
          if ArrLobBind = nil then begin
            GetMem( ArrLobBind, Query.ParamCount * SizeOf(TBindLobInfo) );
            FillChar( ArrLobBind^, Query.ParamCount * SizeOf(TBindLobInfo), 0 );
          end;
          ArrLobBind[Ora8LobCount].phBind := phBind;
          ArrLobBind[Ora8LobCount].ParamIdx := i;
		// count of Lob parameters
          Inc( Ora8LobCount );
        end else
          Check( OCIBindByName( Handle, phBind, phError, PChar(sqlvar), Length(sqlvar),
        	Pointer(PChar(TVarData(Value).VString)), RealParamDataSize(Query.Params[i]),
                SrvDatabase.NativeDataType(DataType), @TFieldInfo(CurPtr^).FetchStatus,
                nil, nil, 0, nil, OCI_DEFAULT ) )
      end else begin
        Check( OCIBindByName( Handle, phBind, phError, PChar(sqlvar), Length(sqlvar),
        	DataPtr, Datalen, SrvDatabase.NativeDataType(DataType),
                @TFieldInfo(CurPtr^).FetchStatus,
                nil, nil, 0, nil, OCI_DEFAULT ) )
      end;

      Inc( Integer(CurPtr), SizeOf(TFieldInfo) + DataLen );
    end;
	// save info of Lob parameter
  if Ora8LobCount > 0 then begin
    SetUpdatedLobArray( Ora8LobCount, 0 );
    for i:=0 to Ora8LobCount-1 do with TOutLocStruct(UpdatedLobInfo^) do begin
      BindInfo[i].phBind   := ArrLobBind[i].phBind;
      BindInfo[i].ParamIdx := ArrLobBind[i].ParamIdx;
    end;
  end;

  finally
    if ArrLobBind <> nil then
      FreeMem( ArrLobBind, Query.ParamCount * SizeOf(TBindLobInfo));
  end;
end;

procedure TIOra8DataSet.QPrepareSQL(Value: PChar);
	// S must be INSERT or UPDATE statement
  function IsRequiredStmt(const S: string): Boolean;
  var
    StmtType: ub2;
  begin
    Check( OCIAttrGet( Pdvoid(FHandle), OCI_HTYPE_STMT, @StmtType, nil, OCI_ATTR_STMT_TYPE, phError ) );
    Result := (StmtType = OCI_STMT_INSERT) or (StmtType = OCI_STMT_UPDATE);
  end;
	// check RETURNING keyword
  function CheckReturning(const S: string): Boolean;
  const
    sRet = 'RETURNING';
    WordSep = [#$10, #$13, #$20];
  var
    i: Integer;
  begin
    i := AnsiPos( sRet, AnsiUpperCase(S) );
    	// if found a separate word, which has a delimiter before and after
    Result := (i > 0) and
    	      (
    		( ((i+Length(sRet)) <= Length(S)) and (S[i+Length(sRet)] in WordSep) )
                	or
                ( ((i-1) > 0) and (S[i-1] in WordSep) )
    	      );
  end;
	// for example: change 'FCLOB = :FLONG' -> 'FCLOB = empty_clob()' and add 'returning FCLOB into :FLONG'
  function AddReturning(const AStmt: string): string;
  var
    sRetStmt, sFields, sParams, sFullParamName, sEmptyFunc: string;
    i, ParamPos: Integer;
  begin
    sRetStmt := PChar(AStmt);
    with Query do
      for i:=0 to Query.ParamCount-1 do begin
        if not IsOra8BlobType(Params[i].DataType) then
          Continue;
        sFullParamName := ParamPrefix + IntToStr(i+1);
        ParamPos := AnsiPos( sFullParamName, sRetStmt );
        if ParamPos = 0 then
          Continue;
	// it's necessary to parse column name for UPDATE and INSERT statement
        if Length(sFields) > 0 then
          sFields := sFields + ', ';
        sFields := sFields + Params[i].Name;

        if Length(sParams) > 0 then
          sParams := sParams + ', ';
        sParams := sParams + sFullParamName;
      	// change, for example, ':FLONG' -> 'empty_blob()' or 'empty_clob()'
        System.Delete( sRetStmt, ParamPos, Length(sFullParamName) );
        if SrvDatabase.NativeDataType(Params[i].DataType) = SQLT_BLOB
        then sEmptyFunc := 'empty_blob()'
        else sEmptyFunc := 'empty_clob()';	// else SQLT_CLOB
        System.Insert( sEmptyFunc, sRetStmt, ParamPos );
    end;

    if Length(sFields) > 0 then
      sRetStmt := sRetStmt + Format(' returning %s into %s', [sFields, sParams]);
    Result := sRetStmt;
  end;
var
  i: Integer;
begin
  if FHandle = nil then Connect;

  FStmt := MakeValidStatement( Value, Query );

  CheckStmt( OCIStmtPrepare( FHandle, phError, PChar(FStmt), Length(FStmt), OCI_NTV_SYNTAX, OCI_DEFAULT ) );

  if {$IFDEF SD_VCL5} True {$ELSE} SrvDatabase.IsOra8BlobMode {$ENDIF}
  then with Query do begin
    i := 0;
    while i < ParamCount do begin
      if IsOra8BlobType(Params[i].DataType) then
        Break;
      Inc( i );
    end;
    if i = ParamCount then
      Exit;	// Query have not Oracle8 blob parameters
	// Check UPDATE or INSERT statement
    if not IsRequiredStmt( FStmt ) then
      Exit;
	// Check RETURNING clause in the statament
    if not CheckReturning( FStmt ) then
      FStmt := AddReturning( FStmt );  // add modifucations in the statement. If BLOB is empty, then it's not necessary to bind and add it to returning clause

    CheckStmt( OCIStmtPrepare( FHandle, phError, PChar(FStmt), Length(FStmt), OCI_NTV_SYNTAX, OCI_DEFAULT ) );
  end;

end;

function TIOra8DataSet.GetStmtExecuteMode: ub4;
begin
  Result := OCI_DEFAULT;
  if SrvDatabase.AutoCommit and not SrvDatabase.Database.InTransaction then
    Result := Result or OCI_COMMIT_ON_SUCCESS;
end;

procedure TIOra8DataSet.QExecute;
var
  iters, PrefetchRows: ub4;
  rcd: sword;
begin
  if IsSelectStmt
  then iters := 0
  else iters := 1;

	// in case of select, it improves fetch perfomance
  if iters = 0 then begin
    PrefetchRows := 3;		// by default 1, big value can decrease perfomance of complex queries
    Check( OCIAttrSet( Pdvoid(Handle), OCI_HTYPE_STMT, @PrefetchRows, SizeOf(PrefetchRows), OCI_ATTR_PREFETCH_ROWS, phError ) );
  end;

  rcd := OCIStmtExecute( phSvcCtx, Handle, phError, iters, 0, nil, nil, GetStmtExecuteMode );

	// in case of update(insert ..) error,
        //it's necessary to prepare and bind a statement anew. It isn't necessary to do for SELECT
  if (rcd = OCI_ERROR) and
     (dsfExecSQL in DSFlags) and (dsfPrepared in DSFlags)
  then
    Query.UnPrepare;

  CheckStmt( rcd );

	// if Oracle8 Lob parameters are processed
  if Assigned(UpdatedLobInfo) then begin
    WriteLobParams;
    SetUpdatedLobArray( 0, 0 );
  end;
	// if PL/SQL block is executed
  if IsPLSQLBlock then
    QGetParams;
end;

procedure TIOra8DataSet.QGetParams;
var
  I: Integer;
  FldBuf, OutBuf, OutData: PChar;
  TmpField: TField;
  FieldCls: TFieldClass;
begin
  if FBindBuffer = nil then Exit;
	// Alloc buffer for parameter of max size
  OutBuf := StrAlloc( MinStrParamSize + SizeOf(TFieldInfo) );

  try
    FldBuf := FBindBuffer;
    for I := 0 to Query.ParamCount - 1 do with Query.Params[I] do begin
      if PFieldInfo(FldBuf)^.DataSize = 0 then
        PFieldInfo(FldBuf)^.DataSize := QParamBindSize( Query.Params[I] );
      if ParamType in [ptResult, ptInputOutput, ptOutput] then begin
	// REF CURSOR processing, if it's need
        if DataType = ftCursor then begin
          if FParamHandle = nil then
            FParamHandle := POCIStmt(Pointer(FldBuf + SizeOf(TFieldInfo))^);
        end else begin
          FieldCls := DefaultFieldClasses[DataType];
          TmpField := FieldCls.Create(nil);
          try
            CnvtDBField2Field(TmpField, FldBuf, OutBuf);
          finally
            TmpField.Free;
          end;

          if FieldIsNull(OutBuf) then
            Clear
          else begin
            OutData := OutBuf + SizeOf(TFieldInfo);
            SetData(OutData);
          end;
        end;
      end;
      Inc( FldBuf, PFieldInfo(FldBuf)^.DataSize + SizeOf(TFieldInfo) );
    end;

  finally
    StrDispose(OutBuf);
  end;
end;

function TIOra8DataSet.QGetRowsAffected: Integer;
var
  rows: ub4;
begin
  Result := -1;
  if IsSelectStmt or not Assigned(FHandle) then
    Exit;
    
  Check( OCIAttrGet( Pdvoid(FHandle), OCI_HTYPE_STMT, @rows, nil, OCI_ATTR_ROW_COUNT, phError ) );
  Result := rows;
end;

	{ StoredProc methods }

procedure TIOra8DataSet.SpCreateParamDesc;
var
  sObjName, sParamName, sPkgName: string;
  ft: TFieldType;
  pt: TSDHelperParamType;
  IsFunc: Boolean;
  i: Integer;
  rcd: sword;
  phDesc: POCIDescribe;
  phParm, phSubLst, phArgLst, phArg: POCIParam;
  NumSubs, NumArgs, dtype: ub2;
  iomode: OCITypeParamMode;
  prec, SubType: ub1;
  scale: sb1;
  szName: PChar;
  NameLen: ub4;
begin
  IsFunc := False;
  sObjName := StoredProc.StoredProcName;
  for i:=Length(sObjName) downto 1 do
    if sObjName[i] = '.' then begin
    	// package name can include a owner name, for example: 'owner.package_name'
      sPkgName := Copy( sObjName, 1, i-1 );
      if AnsiPos( '.', sPkgName ) = 0 then
        sPkgName := ''
      else
        sObjName := Copy( sObjName, i+1, Length(sObjName)-i );
      Break;
    end;
  pt := ptInput;
  phDesc := nil;
  	// get the describe handle for the procedure or function
  Check( OCIHandleAlloc( Pdvoid(SrvDatabase.FEnvHandle), @phDesc, OCI_HTYPE_DESCRIBE, 0, nil ) );
  try
  	// describe a package and find the required procedure/function in a package list
    if Trim(sPkgName) <> '' then begin
      Check( OCIDescribeAny( phSvcCtx, phError, Pdvoid(PChar(sPkgName)), Length(sPkgName),
  		OCI_OTYPE_NAME, OCI_DEFAULT, OCI_PTYPE_PKG, phDesc ) );
	// Get the parameter for the package
      Check( OCIAttrGet( Pdvoid(phDesc), OCI_HTYPE_DESCRIBE, @phParm, nil, OCI_ATTR_PARAM, phError ) );
      	// get parameter for list of subprograms
      Check( OCIAttrGet( Pdvoid(phParm), OCI_DTYPE_PARAM, @phSubLst, nil, OCI_ATTR_LIST_SUBPROGRAMS, phError ) );
	//  How many subroutines are in this package ?
      Check( OCIAttrGet( Pdvoid(phSubLst), OCI_DTYPE_PARAM, @NumSubs, nil, OCI_ATTR_NUM_PARAMS, phError ) );
      i := 0;
      while i < NumSubs do begin
        Check( OCIParamGet( Pdvoid(phSubLst), OCI_DTYPE_PARAM, phError, Pdvoid(phParm), i ) );
	// Get the routine type and name...
        Check( OCIAttrGet( Pdvoid(phParm), OCI_DTYPE_PARAM, @SubType, nil, OCI_ATTR_PTYPE, phError ) );
        IsFunc := SubType = OCI_PTYPE_FUNC;
        szName := nil;
        Check( OCIAttrGet( Pdvoid(phParm), OCI_DTYPE_PARAM, @szName, @NameLen, OCI_ATTR_NAME, phError ) );
       	// the routine's found
        if AnsiUpperCase(sObjName) = AnsiUpperCase( Copy(szName, 1, NameLen) ) then
          Break;
        Inc( i );
      end;
      	// the procedure/function is not exist in the package
      if i = NumSubs then begin
        sObjName := StoredProc.StoredProcName;
        Check( OCIDescribeAny( phSvcCtx, phError, Pdvoid(PChar(sObjName)), Length(sObjName),
  		OCI_OTYPE_NAME, OCI_DEFAULT, OCI_PTYPE_FUNC, phDesc ) );
      end;
    end else begin
      rcd := OCIDescribeAny( phSvcCtx, phError, Pdvoid(PChar(sObjName)), Length(sObjName),
  		OCI_OTYPE_NAME, OCI_DEFAULT, OCI_PTYPE_PROC, phDesc );
      	// if the procedure sObjName is not exist, try to get it as function
      if (rcd = OCI_ERROR) then begin
        Check( OCIDescribeAny( phSvcCtx, phError, Pdvoid(PChar(sObjName)), Length(sObjName),
        	OCI_OTYPE_NAME, OCI_DEFAULT, OCI_PTYPE_FUNC, phDesc ) );
        IsFunc := True;
      end else
        Check( rcd );
	// get the parameter handle
      Check( OCIAttrGet( Pdvoid(phDesc), OCI_HTYPE_DESCRIBE, @phParm, nil, OCI_ATTR_PARAM, phError ) );
    end;

	// Get the number of arguments and the arg list
    Check( OCIAttrGet( Pdvoid(phParm), OCI_DTYPE_PARAM, @phArgLst, nil,
    			OCI_ATTR_LIST_ARGUMENTS, phError ) );
    Check( OCIAttrGet( Pdvoid(phArgLst), OCI_DTYPE_PARAM, @NumArgs, nil,
    			OCI_ATTR_NUM_PARAMS, phError ) );
	// For a procedure, we begin with i = 1; for a function, we begin with i = 0. NumArgs = Result(1, for function) + Parameters
    if IsFunc then begin
      i := 0;
      Dec( NumArgs );
    end else
      i := 1;

    while i <= NumArgs do begin
      Check( OCIParamGet( Pdvoid(phArgLst), OCI_DTYPE_PARAM, phError, Pdvoid(phArg), i ) );
      szName := nil;
      Check( OCIAttrGet( Pdvoid(phArg), OCI_DTYPE_PARAM, @szName, @NameLen,
      				OCI_ATTR_NAME, phError ) );
      Check( OCIAttrGet( Pdvoid(phArg), OCI_DTYPE_PARAM, @dtype, nil,
      				OCI_ATTR_DATA_TYPE, phError ) );
      Check( OCIAttrGet( Pdvoid(phArg), OCI_DTYPE_PARAM, @iomode, nil,
      				OCI_ATTR_IOMODE, phError ) );
      Check( OCIAttrGet( Pdvoid(phArg), OCI_DTYPE_PARAM, @prec, nil,
      				OCI_ATTR_PRECISION, phError ) );
      Check( OCIAttrGet( Pdvoid(phArg), OCI_DTYPE_PARAM, @scale, nil,
      				OCI_ATTR_SCALE, phError ) );

      if szName <> nil
      then sParamName := Copy( szName, 0, NameLen )
      else sParamName := SResultName;

      ft := SrvDatabase.FieldDataType( dtype );
      if ft = ftUnknown then
        DatabaseErrorFmt( SBadFieldType, [sParamName]);
      ft := SrvDatabase.ExactNumberDataType(ft, prec, scale);
      case iomode of
        OCI_TYPEPARAM_IN:	pt := ptInput;
        OCI_TYPEPARAM_OUT:	pt := ptOutput;
        OCI_TYPEPARAM_INOUT:	pt := ptInputOutput;
      else
        DatabaseErrorFmt( SBadParameterType, [sParamName] );
      end;
      if (szName = nil) and (pt in [ptOutput]) then
        pt := ptResult;

      SpAddParam( sParamName, ft, pt );

      Inc( i );
    end;
    FBoundParams := False;
  finally
    Check( OCIHandleFree( Pdvoid(phDesc), OCI_HTYPE_DESCRIBE ) );
  end;
end;

procedure TIOra8DataSet.SpBindParams;
var
  CurPtr, DataPtr: Pointer;
  i: Integer;
  sqlvar: string;
  DataLen: sb4;
  phBind: POCIBind;
  phLob: POCILobLocator;
  LobEmpty: ub4;
begin
  if FBindBuffer = nil then Exit;
  CurPtr := FBindBuffer;

  for i:=0 to StoredProc.ParamCount-1 do
    with StoredProc.Params[i] do begin
      if IsNull then
        TFieldInfo(CurPtr^).FetchStatus := OCI_IND_NULL
      else
        TFieldInfo(CurPtr^).FetchStatus := OCI_IND_NOTNULL;
      DataLen := SpParamBindSize( StoredProc.Params[i] );
      TFieldInfo(CurPtr^).DataSize := 0;
      DataPtr := Pointer(Integer(CurPtr) + SizeOf(TFieldInfo));

      sqlvar := ':' + Name;

      case DataType of
        ftString:
          if not IsNull then begin
          	// DataLen is always equal MinStrParamSize+1(with '\0'), but string length may be less or more
            if DataLen > (Length(AsString) + 1) then
              StrMove(DataPtr, PChar(AsString), Length(AsString)+1)	// includes '\0'
            else begin
              StrMove(DataPtr, PChar(AsString), DataLen);
              PChar(DataPtr)[DataLen-1] := #$0;	// DataLen is buffer size including zero-terminator
            end;
          end;
        ftInteger:
          if not IsNull then DWORD(DataPtr^) := AsInteger;
        ftSmallInt:
          if not IsNull then WORD(DataPtr^) := AsInteger;
        ftDate, ftTime, ftDateTime:
          if not IsNull then CnvtDateTime2DBDateTime(DataType, AsDateTime, DataPtr, SrvDatabase.NativeDataSize(DataType));
        ftFloat:
          if not IsNull then Double(DataPtr^) := AsFloat;
        ftCursor:
          if FParamHandle = nil then begin
            TFieldInfo(CurPtr^).FetchStatus := OCI_IND_NOTNULL;
            Check( OCIHandleAlloc( Pdvoid(SrvDatabase.FEnvHandle), PPdvoid(@FParamHandle), OCI_HTYPE_STMT, 0, nil ) );
            POCIStmt(DataPtr^)	:= FParamHandle;
            FParamHandle	:= nil;
          end;
        else
          if not IsSupportedBlobTypes(DataType) then
            raise EDatabaseError.CreateFmt(SNoParameterDataType, [Name]);
      end;
      phBind := nil;
      if IsBlobType(DataType) then begin
        if IsOra8BlobType(DataType) then begin
          Check( OCIDescriptorAlloc(Pdvoid(SrvDatabase.FEnvHandle), @phLob, OCI_DTYPE_LOB, 0, nil) );
          if ParamType in [ptInput, ptInputOutput] then begin
          	// it will be set BLOB/CLOB field to empty (assign this locator is equal to assign empty_lob() function )
            LobEmpty := 0;
            Check( OCIAttrSet(Pdvoid(phLob), OCI_DTYPE_LOB, @LobEmpty, SizeOf(LobEmpty), OCI_ATTR_LOBEMPTY, phError) );
          end;
          POCILobLocator(DataPtr^) := phLob;
          phLob	:= nil;
          Check( OCIBindByName( Handle, phBind, phError, PChar(sqlvar), Length(sqlvar),
        	DataPtr, DataLen, SrvDatabase.NativeDataType(DataType),
                @TFieldInfo(CurPtr^).FetchStatus,
                nil, nil, 0, nil, OCI_DEFAULT ) )
        end else
          Check( OCIBindByName( Handle, phBind, phError, PChar(sqlvar), Length(sqlvar),
        	nil, MAX_LONG_COL_SIZE, SrvDatabase.NativeDataType(DataType),
                @TFieldInfo(CurPtr^).FetchStatus,
                nil, nil, 0, nil, OCI_DATA_AT_EXEC ) )
      end else begin
        Check( OCIBindByName( Handle, phBind, phError, PChar(sqlvar), Length(sqlvar),
        	DataPtr, DataLen, SrvDatabase.NativeDataType(DataType),
                @TFieldInfo(CurPtr^).FetchStatus,
                nil, nil, 0, nil, OCI_DEFAULT ) )
      end;
      Inc( Integer(CurPtr), SizeOf(TFieldInfo) + DataLen );
    end;

  FBoundParams := True;    
end;

procedure TIOra8DataSet.SpPrepareProc;
begin
  if FHandle = nil then Connect;

  if StoredProc.Params.Count = 0 then
    SpCreateParamDesc;

  FStmt := MakeStoredProcStmt( StoredProc );
  Check( OCIStmtPrepare( Handle, phError, PChar(FStmt), Length(FStmt), OCI_NTV_SYNTAX, OCI_DEFAULT ) );
end;

procedure TIOra8DataSet.SpExecute;
var
  rcd: sword;
  i: Integer;
  sBlob: string;
begin
  rcd := OCIStmtExecute( phSvcCtx, FHandle, phError, 1, 0, nil, nil, GetStmtExecuteMode );

    	// if required Blob-field data
  if rcd = OCI_NEED_DATA then with StoredProc do begin
    for i:=0 to ParamCount-1 do
      if IsSupportedBlobTypes( Params[i].DataType ) then begin
        sBlob := Params[i].AsBlob;
        WriteBlobByName( Params[i].Name, PChar(sBlob)^, Length(sBlob) );
      end;
  end else
    Check( rcd );
end;

procedure TIOra8DataSet.SpExecProc;
begin
  SpBindParams;
  SpExecute;
  SpGetParams;

  CloseParamHandle;
end;

procedure TIOra8DataSet.SpGetResults;
begin
  // nothing
end;

procedure TIOra8DataSet.SpGetParams;
var
  I: Integer;
  FldBuf, OutBuf, OutData: PChar;
  TmpField: TField;
  FieldCls: TFieldClass;
  sBlob: string;
  phLob: POCILobLocator;
begin
  if FBindBuffer = nil then Exit;
	// Alloc buffer for parameter of max size
  OutBuf := StrAlloc( MinStrParamSize + SizeOf(TFieldInfo) );

  try
    FldBuf := FBindBuffer;
    for I := 0 to StoredProc.ParamCount - 1 do with StoredProc.Params[I] do begin
      PFieldInfo(FldBuf)^.DataSize := SpParamBindSize( StoredProc.Params[I] );
      if ParamType in [ptResult, ptInputOutput, ptOutput] then begin
	// REF CURSOR processing, if it's need
        if DataType = ftCursor then begin
          if FParamHandle = nil then
            FParamHandle := POCIStmt(Pointer(FldBuf + SizeOf(TFieldInfo))^);
        end else if IsOra8BlobType(DataType) then begin
          if sb2(PFieldInfo(FldBuf)^.FetchStatus) = OCI_IND_NOTNULL then begin
            phLob := POCILobLocator(Pointer(FldBuf + SizeOf(TFieldInfo))^);
            if ParamType = ptInputOutput then begin
              sBlob := AsString;
              	// set BLOB/CLOB data using the returned locator
              if Length(sBlob) > 0 then
                WriteLobData(phLob, PChar(sBlob)^, Length(sBlob));
            end else
              if ReadLobData(phLob, sBlob) > 0 then
                Value := sBlob;
		// free the LOB locator                
            Check( OCIDescriptorFree( Pdvoid(phLob), OCI_DTYPE_LOB ) );
          end;
        end else begin
          FieldCls := DefaultFieldClasses[DataType];
          TmpField := FieldCls.Create(nil);
          try
            CnvtDBField2Field(TmpField, FldBuf, OutBuf);
          finally
            TmpField.Free;
          end;

          if FieldIsNull(OutBuf) then begin
          	// reset if it's necessary
            if not IsNull then
              Clear;
          end else begin
            OutData := OutBuf + SizeOf(TFieldInfo);
            SetData(OutData);
          end;
        end;
      end;
      Inc( FldBuf, PFieldInfo(FldBuf)^.DataSize + SizeOf(TFieldInfo) );
    end;

  finally
    StrDispose(OutBuf);
  end;
end;


initialization
  bDetectedOCI72:= False;
  bDetectedOCI73:= False;
  dwLoadedOCI	:= 0;

  UsePiecewiseCalls := False;

  hSqlLibModule := 0;
  SqlLibRefCount:= 0;
  SqlLibLock 	:= TCriticalSection.Create;
finalization
  while SqlLibRefCount > 0 do
    FreeSqlLib;
  SqlLibLock.Free;
end.

