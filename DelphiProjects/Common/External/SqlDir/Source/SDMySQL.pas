
{*******************************************************}
{							}
{       Delphi SQLDirect Component Library		}
{	MySQL API (ver.3.23.36) Interface Unit		}
{       MySQL API Date: 23.03.2001        		}
{       		                                }
{       Copyright (c) 1997,2002 by Yuri Sheino		}
{                                                       }
{*******************************************************}

unit SDMySql;

interface

{$I SqlDir.inc}

uses
  Windows, SysUtils, Classes, Db, Registry, SyncObjs,
  SDEngine, SDConsts;

{*******************************************************************************
*	mysql_com.h - Common definition between mysql server & client          *
*******************************************************************************}

const
  NAME_LEN		= 64;		// Field/table name length
  HOSTNAME_LENGTH  	= 60;
  USERNAME_LENGTH	= 16;
  SERVER_VERSION_LENGTH = 60;

  LOCAL_HOST		= 'localhost';
  LOCAL_HOST_NAMEDPIPE 	= '.';

  MYSQL_NAMEDPIPE     	= 'MySQL';
  MYSQL_SERVICENAME	= 'MySql';

type
  TMyBool	= Char;
  TMySocket	= Integer;
  TGPtr		= PChar;
  PCHARSET_INFO	= PChar;

  TServerCommand	= (
  	COM_SLEEP,COM_QUIT,COM_INIT_DB,COM_QUERY,
        COM_FIELD_LIST,COM_CREATE_DB,COM_DROP_DB,COM_REFRESH,
        COM_SHUTDOWN,COM_STATISTICS,
        COM_PROCESS_INFO,COM_CONNECT,COM_PROCESS_KILL,
        COM_DEBUG,COM_PING,COM_TIME,COM_DELAYED_INSERT,
        COM_CHANGE_USER, COM_BINLOG_DUMP,
        COM_TABLE_DUMP, COM_CONNECT_OUT
  );

const
  NOT_NULL_FLAG		= 1;		// Field can't be NULL
  PRI_KEY_FLAG		= 2;		// Field is part of a primary key
  UNIQUE_KEY_FLAG	= 4;		// Field is part of a unique key
  MULTIPLE_KEY_FLAG	= 8;		// Field is part of a key
  BLOB_FLAG    		= 16;		// Field is a blob
  UNSIGNED_FLAG		= 32;		// Field is unsigned
  ZEROFILL_FLAG		= 64;		// Field is zerofill
  BINARY_FLAG  		= 128;
	// The following are only sent to new clients
  ENUM_FLAG    		= 256;		// field is an enum
  AUTO_INCREMENT_FLAG	= 512;		// field is a autoincrement field
  TIMESTAMP_FLAG 	= 1024;		// Field is a timestamp
  SET_FLAG    		= 2048;		// field is a set
  NUM_FLAG    		= 32768;	// Field is num (for clients)
  PART_KEY_FLAG		= 16384;	// Intern; Part of some key
  GROUP_FLAG   		= 32768;	// Intern: Group field
  UNIQUE_FLAG  		= 65536;	// Intern: Used by sql_yacc

  REFRESH_GRANT		= 1;		// Refresh grant tables
  REFRESH_LOG  		= 2;		// Start on new log file
  REFRESH_TABLES	= 4;		// close all tables
  REFRESH_HOSTS		= 8;		// Flush host cache
  REFRESH_STATUS	= 16;		// Flush status variables
  REFRESH_THREADS	= 32;		// Flush status variables
  REFRESH_SLAVE         = 64;     	// Reset master info and restart slave thread
  REFRESH_MASTER        = 128;     	// Remove all bin logs in the index and truncate the index

	// The following can't be set with mysql_refresh()
  REFRESH_READ_LOCK	= 16384;	// Lock tables for read
  REFRESH_FAST	       	= 32768;	// Intern flag

  CLIENT_LONG_PASSWORD	= 1;		// new more secure passwords
  CLIENT_FOUND_ROWS	= 2;		// Found instead of affected rows
  CLIENT_LONG_FLAG	= 4;		// Get all column flags
  CLIENT_CONNECT_WITH_DB= 8;		// One can specify db on connect
  CLIENT_NO_SCHEMA	= 16;		// Don't allow database.table.column
  CLIENT_COMPRESS	= 32;		// Can use compression protocol
  CLIENT_ODBC		= 64;		// Odbc client
  CLIENT_LOCAL_FILES	= 128;		// Can use LOAD DATA LOCAL
  CLIENT_IGNORE_SPACE	= 256;		// Ignore spaces before '('
  CLIENT_CHANGE_USER	= 512;		// Support the mysql_change_user()
  CLIENT_INTERACTIVE	= 1024;		// This is an interactive client
  CLIENT_SSL            = 2048; 	// Switch to SSL after handshake
  CLIENT_IGNORE_SIGPIPE = 4096; 	// IGNORE sigpipes
  CLIENT_TRANSACTIONS	= 8192;		// Client knows about transactions

  SERVER_STATUS_IN_TRANS  = 1;		// Transaction has started
  SERVER_STATUS_AUTOCOMMIT= 2;		// Server in auto_commit mode

  MYSQL_ERRMSG_SIZE	= 200;
  NET_READ_TIMEOUT	= 30;		// Timeout on read
  NET_WRITE_TIMEOUT	= 60;		// Timeout on write
  NET_WAIT_TIMEOUT	= 8*60*60;	// Wait for new query

type
  TNET = packed record
    vio:           Pointer;
    fd:            TMySocket;
    fcntl:         Integer;		// For Perl DBI/dbd 
    buff:          PChar;
    buff_end:      PChar;
    write_pos:     PChar;
    read_pos:      PChar;
    last_error:    array[0..MYSQL_ERRMSG_SIZE] of Char;
    last_errno:    Integer;
    max_packet:    Integer;
    timeout:       Integer;
    pkt_nr:        Integer;
    error:         Char;
    return_errno:  TMyBool;
    compress:      TMyBool;
    no_send_ok:    TMyBool;	// needed if we are doing several queries in one command ( as in LOAD TABLE ... FROM MASTER ),
   				// and do not want to confuse the client with OK at the wrong time
    remain_in_buf: LongInt;
    length:        LongInt;
    buf_length:    LongInt;
    where_b:       LongInt;
    return_status: PInteger;
    reading_or_writing: Char;
    save_char:     Char;
  end;

const
	// Enum Field Types
  FIELD_TYPE_DECIMAL   = 0;
  FIELD_TYPE_TINY      = 1;
  FIELD_TYPE_SHORT     = 2;
  FIELD_TYPE_LONG      = 3;
  FIELD_TYPE_FLOAT     = 4;
  FIELD_TYPE_DOUBLE    = 5;
  FIELD_TYPE_NULL      = 6;
  FIELD_TYPE_TIMESTAMP = 7;
  FIELD_TYPE_LONGLONG  = 8;
  FIELD_TYPE_INT24     = 9;
  FIELD_TYPE_DATE      = 10;
  FIELD_TYPE_TIME      = 11;
  FIELD_TYPE_DATETIME  = 12;
  FIELD_TYPE_YEAR      = 13;
  FIELD_TYPE_NEWDATE   = 14;
  FIELD_TYPE_ENUM      = 247;
  FIELD_TYPE_SET       = 248;
  FIELD_TYPE_TINY_BLOB = 249;
  FIELD_TYPE_MEDIUM_BLOB = 250;
  FIELD_TYPE_LONG_BLOB = 251;
  FIELD_TYPE_BLOB      = 252;
  FIELD_TYPE_VAR_STRING = 253;
  FIELD_TYPE_STRING    = 254;

	// For Compatibility
  FIELD_TYPE_CHAR      = FIELD_TYPE_TINY;
  FIELD_TYPE_INTERVAL  = FIELD_TYPE_ENUM;


{*******************************************************************************
*	mysql.h - defines for the libmysql library                             *
*******************************************************************************}

type
  PUSED_MEM	= ^TUSED_MEM;
  TUSED_MEM	= packed record		// struct for once_alloc
    next:       PUSED_MEM;              // Next block in use
    left:       Integer;                // memory left in block
    size:       Integer;                // size of block
  end;

  POnMyErrorProc = ^TOnMyErrorProc;
  TOnMyErrorProc = procedure;

  TMEM_ROOT 	= packed record
    free:          PUSED_MEM;
    used:          PUSED_MEM;
    pre_alloc:     PUSED_MEM;
    min_malloc:    Integer;
    block_size:    Integer;
    error_handler: POnMyErrorProc;
  end;
  PMEM_ROOT 	= ^TMEM_ROOT;

(*
// extern unsigned int mysql_port;
// extern char *mysql_unix_port;

 #define IS_PRI_KEY(n)	((n) & PRI_KEY_FLAG)
 #define IS_NOT_NULL(n)	((n) & NOT_NULL_FLAG)
 #define IS_BLOB(n)	((n) & BLOB_FLAG)
 #define IS_NUM(t)	((t) <= FIELD_TYPE_INT24 || (t) == FIELD_TYPE_YEAR)
 #define IS_NUM_FIELD(f)	 ((f)->flags & NUM_FLAG)
 #define INTERNAL_NUM_FIELD(f) (((f)->type <= FIELD_TYPE_INT24 && ((f)->type != FIELD_TYPE_TIMESTAMP || (f)->length == 14 || (f)->length == 8)) || (f)->type == FIELD_TYPE_YEAR)
*)

  TMYSQL_FIELD = record
    name:       PChar;		// Name of column
    table:      PChar;          // Table of column if column was a field
    def:        PChar;          // Default value (set by mysql_list_fields)
    ftype:      Byte;           // Type of field. Se mysql_com.h for types
    length:     Integer;        // Width of column
    max_length: Integer;        // Max width of selected set
    flags:      Integer;        // Div flags
    decimals:   Integer;        // Number of decimals in field
  end;
  PMYSQL_FIELD = ^TMYSQL_FIELD;
  TMYSQL_FIELD_OFFSET = Cardinal;	// offset to current field

  TMYSQL_ROW = array[00..$FF] of PChar;	// return data as array of strings
  PMYSQL_ROW = ^TMYSQL_ROW;

{$IFNDEF SD_VCL4}
  TMyInt64 = LongInt;
{$ELSE}
  TMyInt64 = Int64;
{$ENDIF}


    
(*
 #if defined(NO_CLIENT_LONG_LONG)
 typedef unsigned long my_ulonglong;
 #elif defined (__WIN__)
 typedef unsigned __int64 my_ulonglong;
 #else
 typedef unsigned long long my_ulonglong;
 #endif
 #define MYSQL_COUNT_ERROR (~(my_ulonglong) 0)
*)

  PMYSQL_ROWS = ^TMYSQL_ROWS;
  TMYSQL_ROWS = record
    next:       PMYSQL_ROWS;		// list of rows
    data:       TMYSQL_ROW;
  end;

  TMYSQL_ROW_OFFSET = PMYSQL_ROWS;	// offset to current row

  TMYSQL_DATA = record
    Rows:       TMyInt64;
    Fields:     Cardinal;
    Data:       PMYSQL_ROWS;
    Alloc:      TMEM_ROOT;
  end;
  PMYSQL_DATA = ^TMYSQL_DATA;

  TMYSQL_OPTIONS = record
    connect_timeout,
    client_flag:     Integer;
    compress,
    named_pipe:      TMyBool;
    port:            Integer;
    host,
    init_command,
    user,
    password,
    unix_socket,
    db:              PChar;
    my_cnf_file,
    my_cnf_group:    PChar;
    charset_dir,
    charset_name:    PChar;
    use_ssl:         TMyBool;		// if to use SSL or not
    ssl_key,                            // PEM key file
    ssl_cert,                           // PEM cert file
    ssl_ca,                             // PEM CA file
    ssl_capath:      PChar;             // PEM directory of CA-s?
  end;
  PMYSQL_OPTIONS = ^TMYSQL_OPTIONS;

  TMySqlOption = (
    MYSQL_OPT_CONNECT_TIMEOUT,
    MYSQL_OPT_COMPRESS,
    MYSQL_OPT_NAMED_PIPE,
    MYSQL_INIT_COMMAND,
    MYSQL_READ_DEFAULT_FILE,
    MYSQL_READ_DEFAULT_GROUP,
    MYSQL_SET_CHARSET_DIR,
    MYSQL_SET_CHARSET_NAME
  );

  TMySqlStatus = (
    MYSQL_STATUS_READY,
    MYSQL_STATUS_GET_RESULT,
    MYSQL_STATUS_USE_RESULT
  );

  TMYSQL = record
    net:            	TNET;		// Communication parameters
    connector_fd:    	TGPtr;          // ConnectorFd for SSL
    host,
    user,
    passwd,
    unix_socket,
    server_version,
    host_info,
    info,
    db:             	PChar;
    port,
    client_flag,
    server_capabilities:Integer;
    protocol_version: 	Integer;
    field_count:     	Integer;
    server_status:   	Integer;
    thread_id:       	LongInt;	// Id for connection in server
    affected_rows,
    insert_id,                          // id if insert on table with NEXTNR
    extra_info:      	TMyInt64;       // Used by mysqlshow
    packet_length:   	LongInt;
    status:          	TMySqlStatus;
    fields:          	PMYSQL_FIELD;
    field_alloc:     	TMEM_ROOT;
    free_me,				// If free in mysql_close
    reconnect: 		TMyBool;	// set to 1 if automatic reconnect
    options:         	TMYSQL_OPTIONS;
    scramble_buff:   	array[0..8] of Char;
    charset:         	PCHARSET_INFO;
    server_language: 	Integer;
  end;
  PMYSQL = ^TMYSQL;

  TMYSQL_RES = packed record
    row_count:       TMyInt64;
    field_count,
    current_field:   Integer;
    fields:          PMYSQL_FIELD;
    data:            PMYSQL_DATA;
    data_cursor:     PMYSQL_ROWS;
    field_alloc:     TMEM_ROOT;
    row:             TMYSQL_ROW;	// If unbuffered read
    current_row:     TMYSQL_ROW;        // buffer to current row
    lengths:         PLongInt;          // column lengths of current row
    handle:          PMYSQL;            // for unbuffered reads
    eof:             TMyBool;           // Used my mysql_fetch_row 
  end;
  PMYSQL_RES = ^TMYSQL_RES;

{
 Functions to get information from the MYSQL and MYSQL_RES structures
 Should definitely be used if one uses shared libraries
}

var
  mysql_num_rows:
  	function(res: PMYSQL_RES): TMyInt64; stdcall;
  mysql_num_fields:
  	function(res: PMYSQL_RES): Integer; stdcall;
  mysql_eof:
  	function(res: PMYSQL_RES): TMyBool; stdcall;
  mysql_fetch_field_direct:
  	function(res: PMYSQL_RES; fieldnr: Integer): PMYSQL_FIELD; stdcall;
  mysql_fetch_fields:
  	function(res: PMYSQL_RES): PMYSQL_FIELD; stdcall;
  mysql_row_tell:
  	function(res: PMYSQL_RES): PMYSQL_ROWS; stdcall;
  mysql_field_tell:
  	function(res: PMYSQL_RES): Integer; stdcall;
  mysql_field_count:
  	function(mysql: PMYSQL): Integer; stdcall;
  mysql_affected_rows:
  	function(mysql: PMYSQL): TMyInt64; stdcall;
  mysql_insert_id:
  	function(mysql: PMYSQL): TMyInt64; stdcall;
  mysql_errno:
  	function(mysql: PMYSQL): Integer; stdcall;
  mysql_error:
  	function(mysql: PMYSQL): PChar; stdcall;
  mysql_info:
  	function(mysql: PMYSQL): PChar; stdcall;
  mysql_thread_id:
  	function(mysql: PMYSQL): LongInt; stdcall;
  mysql_character_set_name:
  	function(mysql: PMYSQL): PChar; stdcall;
  mysql_init:
  	function(mysql: PMYSQL): PMYSQL; stdcall;

  mysql_ssl_set:
  	function(mysql: PMYSQL;
        	 const key, cert, ca, capath: PChar): Integer; stdcall;
  mysql_ssl_cipher:
  	function(mysql: PMYSQL): PChar; stdcall;
  mysql_ssl_clear:
  	function(mysql: PMYSQL): Integer; stdcall;

  mysql_connect:
  	function(mysql: PMYSQL; const host, user, passwd: PChar): PMYSQL; stdcall;
  mysql_change_user:
  	function(mysql: PMYSQL; const user, passwd, db: PChar): TMyBool; stdcall;

  mysql_real_connect:
  	function(mysql: PMYSQL; const host, user, passwd, db: PChar;
		port: Integer; const unix_socket: PChar;
		clientflag: Integer): PMYSQL; stdcall;
  mysql_close:
  	procedure(sock: PMYSQL); stdcall;
  mysql_select_db:
  	function(mysql: PMYSQL; const db: PChar): Integer; stdcall;
  mysql_query:
  	function(mysql: PMYSQL; const q: PChar): Integer; stdcall;
  mysql_send_query:
  	function(mysql: PMYSQL; const q: PChar; length: Integer): Integer; stdcall;
  mysql_read_query_result:
  	function(mysql: PMYSQL): Integer; stdcall;
  mysql_real_query:
  	function(mysql: PMYSQL; const q: PChar; length: Integer): Integer; stdcall;
  mysql_create_db:
  	function(mysql: PMYSQL; const DB: PChar): Integer; stdcall;
  mysql_drop_db:
  	function(mysql: PMYSQL; const DB: PChar): Integer; stdcall;
  mysql_shutdown:
  	function(mysql: PMYSQL): Integer; stdcall;
  mysql_dump_debug_info:
  	function(mysql: PMYSQL): Integer; stdcall;
  mysql_refresh:
  	function(mysql: PMYSQL; refresh_options: Integer): Integer; stdcall;
  mysql_kill:
  	function(mysql: PMYSQL; pid: Integer): Integer; stdcall;
  mysql_ping:
  	function(mysql: PMYSQL): Integer; stdcall;
  mysql_stat:
  	function(mysql: PMYSQL): PChar; stdcall;
  mysql_get_server_info:
  	function(mysql: PMYSQL): PChar; stdcall;
  mysql_get_client_info:
  	function: PChar; stdcall;
  mysql_get_host_info:
  	function(mysql: PMYSQL): PChar; stdcall;
  mysql_get_proto_info:
  	function(mysql: PMYSQL): Integer; stdcall;
  mysql_list_dbs:
  	function(mysql: PMYSQL; const wild: PChar): PMYSQL_RES; stdcall;
  mysql_list_tables:
  	function(mysql: PMYSQL; const wild: PChar): PMYSQL_RES; stdcall;
  mysql_list_fields:
  	function(mysql: PMYSQL; const table, wild: PChar): PMYSQL_RES; stdcall;
  mysql_list_processes:
  	function(mysql: PMYSQL): PMYSQL_RES; stdcall;
  mysql_store_result:
  	function(mysql: PMYSQL): PMYSQL_RES; stdcall;
  mysql_use_result:
  	function(mysql: PMYSQL): PMYSQL_RES; stdcall;
  mysql_options:
  	function(mysql: PMYSQL; option: TMySqlOption; const arg: PChar): Integer; stdcall;
  mysql_free_result:
  	procedure(res: PMYSQL_RES); stdcall;
  mysql_data_seek:
  	procedure(res: PMYSQL_RES; offset: TMyInt64); stdcall;
  mysql_row_seek:
  	function(res: PMYSQL_RES; Row: TMYSQL_ROW_OFFSET): TMYSQL_ROW_OFFSET; stdcall;
  mysql_field_seek:
  	function(res: PMYSQL_RES; offset: TMYSQL_FIELD_OFFSET): TMYSQL_FIELD_OFFSET; stdcall;
  mysql_fetch_row:
  	function(res: PMYSQL_RES): PMYSQL_ROW; stdcall;
  mysql_fetch_lengths:
  	function(res: PMYSQL_RES): PLongInt; stdcall;
  mysql_fetch_field:
  	function(res: PMYSQL_RES): PMYSQL_FIELD; stdcall;
  mysql_escape_string:
  	function(szTo: PChar; const szFrom: PChar; from_length: LongInt): LongInt; stdcall;
  mysql_real_escape_string:
  	function(mysql: PMYSQL; szTo: PChar; const szFrom: PChar;
		 length: LongInt): LongInt; stdcall;
  mysql_debug:
  	procedure(const debug: PChar); stdcall;

{
  mysql_odbc_escape_string:
  	function(MYSQL *mysql, char *to, unsigned long to_length,
		 const char *from, unsigned long from_length,
		 void *param,
                 char *(*extend_buffer) (void *, char *to, unsigned long *length)
                 ): PChar; stdcall;
}

  myodbc_remove_escape:
  	procedure(mysql: PMYSQL; name: PChar); stdcall;
  mysql_thread_safe:
  	function: Integer; stdcall;

type
  ESDMySQLError = class(ESDEngineError);

{ TIMyDatabase }
  TMySrvInfo	= record
    MySQLPtr:	PMYSQL;			// pointer to TMYSQL structure for communication with the MySQL
  end;
  PMySrvInfo	= ^TMySrvInfo;

  TIMyDatabase = class(TISrvDatabase)
  private
    FHandle: PSDHandleRec;

    procedure Check(mysql: PMYSQL);
    procedure AllocHandle;
    procedure FreeHandle;
    function GetMySQLPtr: PMYSQL;
    procedure GetStmtResult(const Stmt: string; List: TStrings);
  protected
    function GetClientVersion: LongInt; override;
    function GetServerVersion: LongInt; override;
    function GetVersionString: string; override;
    procedure Commit; override;
    function FieldDataType(ExtDataType: Integer): TFieldType; override;
    function GetHandle: PSDHandleRec; override;
    procedure GetStoredProcNames(List: TStrings); override;
    procedure GetTableFieldNames(const TableName: string; List: TStrings); override;
    procedure GetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings); override;
    procedure Logon(const sRemoteDatabase, sUserName, sPassword: string); override;
    procedure Logoff(Force: Boolean); override;
    function NativeDataSize(FieldType: TFieldType): Word; override;
    function RequiredCnvtFieldType(FieldType: TFieldType): Boolean; override;
    procedure Rollback; override;
    procedure SetDefaultParams; override;
    procedure SetHandle(AHandle: PSDHandleRec); override;
    procedure SetTransIsolation(Value: TSDTransIsolation); override;
    procedure StartTransaction; override;
    function SPDescriptionsAvailable: Boolean; override;
  public
    constructor Create(ADatabase: TSDDatabase); override;
    destructor Destroy; override;
    function CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet; override;
    property MySQLPtr: PMYSQL read GetMySQLPtr;
  end;

{ TIMyDataSet }
  TIMyDataSet = class(TISrvDataSet)
  private
    FHandle: PSDCursor;
    FRow: PMYSQL_ROW;		// current row
    FFieldLens: PLongInt;	// field lengths of the current row

    FStmt,			// w/o bind data
    FBindStmt: string;		// with bind parameter data, it's used to check whether the statement was executed
    FRowsAffected: TMyInt64;	// number of rows affected by the last query

    function GetExecuted: Boolean;
    function GetSrvDatabase: TIMyDatabase;
    function CnvtDateTime2SqlString(Value: TDateTime): string;
    function CnvtFloat2SqlString(Value: Double): string;
    function CnvtVarBytes2EscapeString(Value: string): string;
    procedure InternalQBindParams;
    procedure InternalQExecute;
  protected
    procedure Check(Status: TSDEResult);

    procedure CloseResultSet; override;
    function CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean; override;
    procedure Disconnect(Force: Boolean); override;
    procedure Execute; override;
    function FetchNextRow: Boolean; override;
    procedure GetFieldDescs(Descs: TSDFieldDescList); override;
    function GetHandle: PSDCursor; override;
    procedure SetSelectBuffer; override;
    function ResultSetExists: Boolean; override;
    function ReadBlob(FieldNo: Integer; var BlobData: string): Longint; override;
	// Query methods
    procedure QBindParams; override;
    function QGetRowsAffected: Integer; override;
    procedure QPrepareSQL(Value: PChar); override;
    procedure QExecute; override;
	// StoredProc methods
    procedure SpBindParams; override;
    procedure SpCreateParamDesc; override;
    procedure SpPrepareProc; override;
    procedure SpExecute; override;
    procedure SpExecProc; override;
    procedure SpGetResults; override;
    function SpNextResultSet: Boolean; override;

    property Executed: Boolean read GetExecuted;
    property SrvDatabase: TIMyDatabase read GetSrvDatabase;
  public
    constructor Create(ADataSet: TSDDataSet); override;
    destructor Destroy; override;
  end;

const
  DefSqlApiDLL	= 'libmysql.dll';

{*******************************************************************************
		Load/Unload Sql-library
*******************************************************************************}
procedure LoadSqlLib;
procedure FreeSqlLib;

implementation

resourcestring
  SErrLibLoading 	= 'Error loading library ''%s''';
  SErrLibUnloading	= 'Error unloading library ''%s''';
  SErrFuncNotFound	= 'Function ''%s'' not found in MySQL library';

var
  hSqlLibModule: THandle;
  SqlLibRefCount: Integer;
  SqlLibLock: TCriticalSection;  

(*******************************************************************************
			Load/Unload Sql-library
********************************************************************************)
procedure SetProcAddresses;
begin
  @mysql_num_rows		:= GetProcAddress(hSqlLibModule, 'mysql_num_rows');  		ASSERT( @mysql_num_rows		<>nil, Format(SErrFuncNotFound, ['mysql_num_rows']) );
  @mysql_num_fields		:= GetProcAddress(hSqlLibModule, 'mysql_num_fields');  		ASSERT( @mysql_num_fields	<>nil, Format(SErrFuncNotFound, ['mysql_num_fields']) );
  @mysql_eof			:= GetProcAddress(hSqlLibModule, 'mysql_eof');  		ASSERT( @mysql_eof	    	<>nil, Format(SErrFuncNotFound, ['mysql_eof']) );
  @mysql_fetch_field_direct	:= GetProcAddress(hSqlLibModule, 'mysql_fetch_field_direct');  	ASSERT( @mysql_fetch_field_direct<>nil, Format(SErrFuncNotFound,['mysql_fetch_field_direct']) );
  @mysql_fetch_fields		:= GetProcAddress(hSqlLibModule, 'mysql_fetch_fields');  	ASSERT( @mysql_fetch_fields   	<>nil, Format(SErrFuncNotFound, ['mysql_fetch_fields']) );
  @mysql_row_tell		:= GetProcAddress(hSqlLibModule, 'mysql_row_tell');  		ASSERT( @mysql_row_tell	       	<>nil, Format(SErrFuncNotFound, ['mysql_row_tell']) );
  @mysql_field_tell		:= GetProcAddress(hSqlLibModule, 'mysql_field_tell');  		ASSERT( @mysql_field_tell	<>nil, Format(SErrFuncNotFound, ['mysql_field_tell']) );
  @mysql_field_count		:= GetProcAddress(hSqlLibModule, 'mysql_field_count');  	ASSERT( @mysql_field_count     	<>nil, Format(SErrFuncNotFound, ['mysql_field_count']) );
  @mysql_affected_rows		:= GetProcAddress(hSqlLibModule, 'mysql_affected_rows');  	ASSERT( @mysql_affected_rows   	<>nil, Format(SErrFuncNotFound, ['mysql_affected_rows']) );
  @mysql_insert_id		:= GetProcAddress(hSqlLibModule, 'mysql_insert_id');  		ASSERT( @mysql_insert_id       	<>nil, Format(SErrFuncNotFound, ['mysql_insert_id']) );
  @mysql_errno			:= GetProcAddress(hSqlLibModule, 'mysql_errno');  		ASSERT( @mysql_errno	       	<>nil, Format(SErrFuncNotFound, ['mysql_errno']) );
  @mysql_error			:= GetProcAddress(hSqlLibModule, 'mysql_error');  		ASSERT( @mysql_error	       	<>nil, Format(SErrFuncNotFound, ['mysql_error']) );
  @mysql_info			:= GetProcAddress(hSqlLibModule, 'mysql_info');  		ASSERT( @mysql_info	       	<>nil, Format(SErrFuncNotFound, ['mysql_info']) );
  @mysql_thread_id		:= GetProcAddress(hSqlLibModule, 'mysql_thread_id');  		ASSERT( @mysql_thread_id       	<>nil, Format(SErrFuncNotFound, ['mysql_thread_id']) );
  @mysql_character_set_name	:= GetProcAddress(hSqlLibModule, 'mysql_character_set_name');
  @mysql_init			:= GetProcAddress(hSqlLibModule, 'mysql_init');  		ASSERT( @mysql_init	      	<>nil, Format(SErrFuncNotFound, ['mysql_init']) );
  @mysql_ssl_set		:= GetProcAddress(hSqlLibModule, 'mysql_ssl_set');
  @mysql_ssl_cipher		:= GetProcAddress(hSqlLibModule, 'mysql_ssl_cipher');
  @mysql_ssl_clear		:= GetProcAddress(hSqlLibModule, 'mysql_ssl_clear');
  @mysql_connect		:= GetProcAddress(hSqlLibModule, 'mysql_connect');  		ASSERT( @mysql_connect	       	<>nil, Format(SErrFuncNotFound, ['mysql_connect']) );
  @mysql_change_user		:= GetProcAddress(hSqlLibModule, 'mysql_change_user');
  @mysql_real_connect		:= GetProcAddress(hSqlLibModule, 'mysql_real_connect');  	ASSERT( @mysql_real_connect	<>nil, Format(SErrFuncNotFound, ['mysql_real_connect']) );
  @mysql_close			:= GetProcAddress(hSqlLibModule, 'mysql_close');  		ASSERT( @mysql_close	       	<>nil, Format(SErrFuncNotFound, ['mysql_close']) );
  @mysql_select_db		:= GetProcAddress(hSqlLibModule, 'mysql_select_db');  		ASSERT( @mysql_select_db	<>nil, Format(SErrFuncNotFound, ['mysql_select_db']) );
  @mysql_query			:= GetProcAddress(hSqlLibModule, 'mysql_query');  		ASSERT( @mysql_query		<>nil, Format(SErrFuncNotFound, ['mysql_query']) );
  @mysql_send_query		:= GetProcAddress(hSqlLibModule, 'mysql_send_query');
  @mysql_read_query_result	:= GetProcAddress(hSqlLibModule, 'mysql_read_query_result');
  @mysql_real_query		:= GetProcAddress(hSqlLibModule, 'mysql_real_query');  		ASSERT( @mysql_real_query	<>nil, Format(SErrFuncNotFound, ['mysql_real_query']) );
  @mysql_create_db		:= GetProcAddress(hSqlLibModule, 'mysql_create_db');  		ASSERT( @mysql_create_db	<>nil, Format(SErrFuncNotFound, ['mysql_create_db']) );
  @mysql_drop_db		:= GetProcAddress(hSqlLibModule, 'mysql_drop_db');  		ASSERT( @mysql_drop_db	       	<>nil, Format(SErrFuncNotFound, ['mysql_drop_db']) );
  @mysql_shutdown		:= GetProcAddress(hSqlLibModule, 'mysql_shutdown');  		ASSERT( @mysql_shutdown	       	<>nil, Format(SErrFuncNotFound, ['mysql_shutdown']) );
  @mysql_dump_debug_info	:= GetProcAddress(hSqlLibModule, 'mysql_dump_debug_info');  	ASSERT( @mysql_dump_debug_info  <>nil, Format(SErrFuncNotFound, ['mysql_dump_debug_info']) );
  @mysql_refresh		:= GetProcAddress(hSqlLibModule, 'mysql_refresh');  		ASSERT( @mysql_refresh	       	<>nil, Format(SErrFuncNotFound, ['mysql_refresh']) );
  @mysql_kill			:= GetProcAddress(hSqlLibModule, 'mysql_kill');  		ASSERT( @mysql_kill	       	<>nil, Format(SErrFuncNotFound, ['mysql_kill']) );
  @mysql_ping			:= GetProcAddress(hSqlLibModule, 'mysql_ping');  		ASSERT( @mysql_ping	       	<>nil, Format(SErrFuncNotFound, ['mysql_ping']) );
  @mysql_stat			:= GetProcAddress(hSqlLibModule, 'mysql_stat');  		ASSERT( @mysql_stat	       	<>nil, Format(SErrFuncNotFound, ['mysql_stat']) );
  @mysql_get_server_info	:= GetProcAddress(hSqlLibModule, 'mysql_get_server_info');  	ASSERT( @mysql_get_server_info 	<>nil, Format(SErrFuncNotFound, ['mysql_get_server_info']) );
  @mysql_get_client_info	:= GetProcAddress(hSqlLibModule, 'mysql_get_client_info');  	ASSERT( @mysql_get_client_info 	<>nil, Format(SErrFuncNotFound, ['mysql_get_client_info']) );
  @mysql_get_host_info		:= GetProcAddress(hSqlLibModule, 'mysql_get_host_info');  	ASSERT( @mysql_get_host_info   	<>nil, Format(SErrFuncNotFound, ['mysql_get_host_info']) );
  @mysql_get_proto_info		:= GetProcAddress(hSqlLibModule, 'mysql_get_proto_info');  	ASSERT( @mysql_get_proto_info  	<>nil, Format(SErrFuncNotFound, ['mysql_get_proto_info']) );
  @mysql_list_dbs		:= GetProcAddress(hSqlLibModule, 'mysql_list_dbs');  		ASSERT( @mysql_list_dbs	       	<>nil, Format(SErrFuncNotFound, ['mysql_list_dbs']) );
  @mysql_list_tables		:= GetProcAddress(hSqlLibModule, 'mysql_list_tables');  	ASSERT( @mysql_list_tables     	<>nil, Format(SErrFuncNotFound, ['mysql_list_tables']) );
  @mysql_list_fields		:= GetProcAddress(hSqlLibModule, 'mysql_list_fields');  	ASSERT( @mysql_list_fields     	<>nil, Format(SErrFuncNotFound, ['mysql_list_fields']) );
  @mysql_list_processes		:= GetProcAddress(hSqlLibModule, 'mysql_list_processes');  	ASSERT( @mysql_list_processes  	<>nil, Format(SErrFuncNotFound, ['mysql_list_processes']) );
  @mysql_store_result		:= GetProcAddress(hSqlLibModule, 'mysql_store_result');  	ASSERT( @mysql_store_result    	<>nil, Format(SErrFuncNotFound, ['mysql_store_result']) );
  @mysql_use_result		:= GetProcAddress(hSqlLibModule, 'mysql_use_result');  		ASSERT( @mysql_use_result      	<>nil, Format(SErrFuncNotFound, ['mysql_use_result']) );
  @mysql_options		:= GetProcAddress(hSqlLibModule, 'mysql_options');  		ASSERT( @mysql_options	       	<>nil, Format(SErrFuncNotFound, ['mysql_options']) );
  @mysql_free_result		:= GetProcAddress(hSqlLibModule, 'mysql_free_result');  	ASSERT( @mysql_free_result	<>nil, Format(SErrFuncNotFound, ['mysql_free_result']) );
  @mysql_data_seek		:= GetProcAddress(hSqlLibModule, 'mysql_data_seek');  		ASSERT( @mysql_data_seek	<>nil, Format(SErrFuncNotFound, ['mysql_data_seek']) );
  @mysql_row_seek		:= GetProcAddress(hSqlLibModule, 'mysql_row_seek');  		ASSERT( @mysql_row_seek	       	<>nil, Format(SErrFuncNotFound, ['mysql_row_seek']) );
  @mysql_field_seek		:= GetProcAddress(hSqlLibModule, 'mysql_field_seek');  		ASSERT( @mysql_field_seek      	<>nil, Format(SErrFuncNotFound, ['mysql_field_seek']) );
  @mysql_fetch_row		:= GetProcAddress(hSqlLibModule, 'mysql_fetch_row');  		ASSERT( @mysql_fetch_row       	<>nil, Format(SErrFuncNotFound, ['mysql_fetch_row']) );
  @mysql_fetch_lengths		:= GetProcAddress(hSqlLibModule, 'mysql_fetch_lengths');  	ASSERT( @mysql_fetch_lengths   	<>nil, Format(SErrFuncNotFound, ['mysql_fetch_lengths']) );
  @mysql_fetch_field		:= GetProcAddress(hSqlLibModule, 'mysql_fetch_field');  	ASSERT( @mysql_fetch_field     	<>nil, Format(SErrFuncNotFound, ['mysql_fetch_field']) );
  @mysql_escape_string		:= GetProcAddress(hSqlLibModule, 'mysql_escape_string');  	ASSERT( @mysql_escape_string   	<>nil, Format(SErrFuncNotFound, ['mysql_escape_string']) );
  @mysql_real_escape_string    	:= GetProcAddress(hSqlLibModule, 'mysql_real_escape_string');
  @mysql_debug			:= GetProcAddress(hSqlLibModule, 'mysql_debug');  		ASSERT( @mysql_debug	       	<>nil, Format(SErrFuncNotFound, ['mysql_debug']) );
  @myodbc_remove_escape		:= GetProcAddress(hSqlLibModule, 'myodbc_remove_escape');  	ASSERT( @myodbc_remove_escape  	<>nil, Format(SErrFuncNotFound, ['myodbc_remove_escape']) );
  @mysql_thread_safe		:= GetProcAddress(hSqlLibModule, 'mysql_thread_safe');  	ASSERT( @mysql_thread_safe     	<>nil, Format(SErrFuncNotFound, ['mysql_thread_safe']) );
end;

procedure ResetProcAddresses;
begin
  @mysql_num_rows		:= nil;
  @mysql_num_fields		:= nil;
  @mysql_eof			:= nil;
  @mysql_fetch_field_direct	:= nil;
  @mysql_fetch_fields		:= nil;
  @mysql_row_tell		:= nil;
  @mysql_field_tell		:= nil;
  @mysql_field_count		:= nil;
  @mysql_affected_rows		:= nil;
  @mysql_insert_id		:= nil;
  @mysql_errno			:= nil;
  @mysql_error			:= nil;
  @mysql_info			:= nil;
  @mysql_thread_id		:= nil;
  @mysql_character_set_name	:= nil;
  @mysql_init			:= nil;
  @mysql_ssl_set		:= nil;
  @mysql_ssl_cipher		:= nil;
  @mysql_ssl_clear		:= nil;
  @mysql_connect		:= nil;
  @mysql_change_user		:= nil;
  @mysql_real_connect		:= nil;
  @mysql_close			:= nil;
  @mysql_select_db		:= nil;
  @mysql_query			:= nil;
  @mysql_send_query		:= nil;
  @mysql_read_query_result	:= nil;
  @mysql_real_query		:= nil;
  @mysql_create_db		:= nil;
  @mysql_drop_db		:= nil;
  @mysql_shutdown		:= nil;
  @mysql_dump_debug_info	:= nil;
  @mysql_refresh		:= nil;
  @mysql_kill			:= nil;
  @mysql_ping			:= nil;
  @mysql_stat			:= nil;
  @mysql_get_server_info	:= nil;
  @mysql_get_client_info	:= nil;
  @mysql_get_host_info		:= nil;
  @mysql_get_proto_info		:= nil;
  @mysql_list_dbs		:= nil;
  @mysql_list_tables		:= nil;
  @mysql_list_fields		:= nil;
  @mysql_list_processes		:= nil;
  @mysql_store_result		:= nil;
  @mysql_use_result		:= nil;
  @mysql_options		:= nil;
  @mysql_free_result		:= nil;
  @mysql_data_seek		:= nil;
  @mysql_row_seek		:= nil;
  @mysql_field_seek		:= nil;
  @mysql_fetch_row		:= nil;
  @mysql_fetch_lengths		:= nil;
  @mysql_fetch_field		:= nil;
  @mysql_escape_string		:= nil;
  @mysql_real_escape_string    	:= nil;
  @mysql_debug			:= nil;
  @myodbc_remove_escape		:= nil;
  @mysql_thread_safe		:= nil;
end;

procedure LoadSqlLib;
begin
  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 0) then begin
      hSqlLibModule := LoadLibrary( PChar( SrvApiDLLs[stMySQL] ) );
      if (hSqlLibModule = 0) then
        raise Exception.CreateFmt(SErrLibLoading, [ SrvApiDLLs[stMySQL] ]);
      Inc(SqlLibRefCount);
      SetProcAddresses;
    end else
      Inc(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;

procedure FreeSqlLib;
begin
  if SqlLibRefCount = 0 then
    Exit;

  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 1) then begin
      if FreeLibrary(hSqlLibModule) then
        hSqlLibModule := 0
      else
        raise Exception.CreateFmt(SErrLibUnloading, [ SrvApiDLLs[stMySQL] ]);
      Dec(SqlLibRefCount);
      ResetProcAddresses;
    end else
      Dec(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;


{ TIMyDatabase }
constructor TIMyDatabase.Create(ADatabase: TSDDatabase);
begin
  inherited Create(ADatabase);
end;

destructor TIMyDatabase.Destroy;
begin
  inherited;
end;

function TIMyDatabase.CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet;
begin
  Result := TIMyDataSet.Create( ADataSet );
end;

procedure TIMyDatabase.Check(mysql: PMYSQL);
var
  E: ESDMySQLError;
  szErrMsg: PChar;
  ErrNo: Integer;
begin
  ResetIdleTimeOut;
  
  if mysql <> nil then
    Exit;
  ResetBusyState;

  ErrNo := mysql_errno( MySQLPtr );
  szErrMsg := mysql_error( MySQLPtr );

  E := ESDMySQLError.Create(ErrNo, ErrNo, szErrMsg, 0);
  raise E;
end;

function TIMyDatabase.GetClientVersion: LongInt;
var
  szVer: PChar;
begin
  szVer := mysql_get_client_info;
  Result := VersionStringToDWORD(szVer);
end;

function TIMyDatabase.GetServerVersion: LongInt;
var
  szVer: PChar;
begin
  szVer := mysql_get_server_info( MySQLPtr );
  Result := VersionStringToDWORD(szVer);
end;

function TIMyDatabase.GetVersionString: string;
begin
  Result := mysql_get_server_info( MySQLPtr ) + '/' + mysql_get_host_info( MySQLPtr );
end;

function TIMyDatabase.GetHandle: PSDHandleRec;
begin
  Result := FHandle;
end;

procedure TIMyDatabase.SetHandle(AHandle: PSDHandleRec);
begin
  LoadSqlLib;

  AllocHandle;

  PMySrvInfo(FHandle^.SrvInfo)^.MySQLPtr :=
  	PMySrvInfo(AHandle^.SrvInfo)^.MySQLPtr;
end;

function TIMyDatabase.GetMySQLPtr: PMYSQL;
begin
  ASSERT( Assigned(FHandle) and Assigned(PMySrvInfo(FHandle^.SrvInfo)),
  	'TIMyDatabase.GetMySQLPtr' );

  Result := PMySrvInfo(FHandle^.SrvInfo)^.MySQLPtr;
end;

procedure TIMyDatabase.AllocHandle;
var
  s: PMySrvInfo;
begin
  ASSERT( not Assigned(FHandle), 'TIMyDatabase.AllocHandle' );

  New(FHandle);
  FillChar( FHandle^, SizeOf(FHandle^), $0 );
  FHandle^.SrvType := Ord( Database.ServerType );

  New(s);
  FillChar( s^, SizeOf(s^), $0 );
  FHandle^.SrvInfo := s;
end;

procedure TIMyDatabase.FreeHandle;
begin
  if Assigned(FHandle) then begin
    if Assigned(FHandle^.SrvInfo) then begin
      Dispose( PMySrvInfo(FHandle^.SrvInfo) );
      PMySrvInfo(FHandle^.SrvInfo) := nil;
    end;
    Dispose( FHandle );
    FHandle := nil;
  end;
end;

procedure TIMyDatabase.Logon(const sRemoteDatabase, sUserName, sPassword: string);
var
  sSrvName, sDbName: string;
  IntValue: Integer;
begin
  try
    LoadSqlLib;

    AllocHandle;

    sSrvName := ExtractServerName(sRemoteDatabase);
    sDbName := ExtractDatabaseName(sRemoteDatabase);

	// gets the custom server port
    IntValue := StrToIntDef( Database.Params.Values[szSERVERPORT], 0 );

    PMySrvInfo(FHandle^.SrvInfo)^.MySQLPtr := mysql_init( nil );
    mysql_options( MySQLPtr, MYSQL_OPT_COMPRESS, nil);
    if Assigned( @mysql_real_connect ) then
      Check( mysql_real_connect( MySQLPtr, PChar(sSrvName), PChar(sUserName),
    		PChar(sPassword), PChar(sDbName), IntValue, nil,
                CLIENT_CONNECT_WITH_DB or CLIENT_FOUND_ROWS ) )
    else begin
      Check( mysql_connect( MySQLPtr, PChar(sSrvName), PChar(sUserName), PChar(sPassword) ) );
      if sDbName <> '' then
        if mysql_query( MySQLPtr, PChar('use ' + sDbName) ) <> 0 then
          Check( MySQLPtr );
    end;

  except
    Logoff(False);

    raise;
  end;
end;

procedure TIMyDatabase.Logoff(Force: Boolean);
begin
  if Assigned(FHandle) and Assigned(FHandle^.SrvInfo) and
     Assigned( PMySrvInfo(FHandle^.SrvInfo)^.MySQLPtr )
  then begin
    if Database.InTransaction then
      mysql_query( MySQLPtr, 'ROLLBACK' );
    mysql_close( PMySrvInfo(FHandle^.SrvInfo)^.MySQLPtr );
    PMySrvInfo(FHandle^.SrvInfo)^.MySQLPtr := nil;
  end;

  FreeHandle;
  FreeSqlLib;
end;

procedure TIMyDatabase.SetDefaultParams;
var
  sValue: string;
begin
  if not AutoCommitDef then begin
    sValue := 'SET AUTOCOMMIT = ';
    if AutoCommit
    then sValue := sValue + '1'
    else sValue := sValue + '0';

    if mysql_query( MySQLPtr, PChar(sValue) ) <> 0 then
      Check( nil );
  end
end;

procedure TIMyDatabase.StartTransaction;
begin
  if mysql_query( MySQLPtr, 'BEGIN' ) <> 0 then
    Check( nil );
end;

procedure TIMyDatabase.Commit;
begin
  if mysql_query( MySQLPtr, 'COMMIT' ) <> 0 then
    Check( nil );
end;

procedure TIMyDatabase.Rollback;
begin
  if mysql_query( MySQLPtr, 'ROLLBACK' ) <> 0 then
    Check( nil );
end;

function TIMyDatabase.FieldDataType(ExtDataType: Integer): TFieldType;
begin
  case ExtDataType of
    FIELD_TYPE_LONGLONG:
{$IFDEF SD_VCL4}
      Result := ftLargeInt;
{$ELSE}
      Result := ftInteger;
{$ENDIF}
    FIELD_TYPE_TINY, FIELD_TYPE_YEAR, FIELD_TYPE_LONG, FIELD_TYPE_SHORT,
    FIELD_TYPE_INT24:
      Result := ftInteger;
    FIELD_TYPE_DECIMAL, FIELD_TYPE_FLOAT, FIELD_TYPE_DOUBLE:
      Result := ftFloat;
    FIELD_TYPE_DATE, FIELD_TYPE_NEWDATE:
      Result := ftDate;
    FIELD_TYPE_TIME:
      Result := ftTime;
    FIELD_TYPE_DATETIME:
      Result := ftDateTime;
    FIELD_TYPE_TINY_BLOB, FIELD_TYPE_MEDIUM_BLOB, FIELD_TYPE_LONG_BLOB,
    FIELD_TYPE_BLOB:
      Result := ftBlob;
    else
      Result := ftString;
  end;
end;

function TIMyDatabase.NativeDataSize(FieldType: TFieldType): Word;
begin
  Result := 0;
end;

function TIMyDatabase.RequiredCnvtFieldType(FieldType: TFieldType): Boolean;
begin
  Result := FieldType in
  	[ftInteger, {$IFDEF SD_VCL4}ftLargeInt,{$ENDIF} ftSmallInt,
         ftFloat,
         ftDate, ftTime, ftDateTime];
end;

procedure TIMyDatabase.GetStmtResult(const Stmt: string; List: TStrings);
var
  res: PMYSQL_RES;
  Row: PMYSQL_ROW;
  FieldLens: PLongInt;
  s: string;
begin
  if mysql_real_query( MySQLPtr, PChar(Stmt), Length(Stmt) ) <> 0 then
    Check( MySQLPtr );
  res := mysql_store_result( MySQLPtr );

  try
    if Assigned(res) and (mysql_num_fields( res ) > 0) then begin
      Row := mysql_fetch_row( res );
      while Assigned(Row) do begin
        FieldLens := mysql_fetch_lengths( res );
        if FieldLens^ > 0 then begin
          SetLength(s, FieldLens^);
          System.Move( Row[0]^, PChar(s)^, FieldLens^ );
          List.Add(s);
        end;
        Row := mysql_fetch_row( res );
      end;
    end;
  finally
    mysql_free_result( res );
  end;
end;

procedure TIMyDatabase.GetStoredProcNames(List: TStrings);
begin
end;

procedure TIMyDatabase.GetTableFieldNames(const TableName: string; List: TStrings);
begin
  GetStmtResult( Format('SHOW COLUMNS FROM %s', [TableName]), List);
end;

procedure TIMyDatabase.GetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings);
begin
  GetStmtResult( Format('SHOW TABLES LIKE ''%s''', [Pattern]), List);
end;

procedure TIMyDatabase.SetTransIsolation(Value: TSDTransIsolation);
begin
  DatabaseError(SNoCapability);
end;

function TIMyDatabase.SPDescriptionsAvailable: Boolean;
begin
  Result := False;
end;

{ TIMyDataSet }
constructor TIMyDataSet.Create(ADataSet: TSDDataSet);
begin
  inherited Create(ADataSet);

  FStmt		:= '';
  FBindStmt	:= '';
  FRowsAffected := -1;

  FHandle 	:= nil;
  FRow		:= nil;
  FFieldLens	:= nil;
end;

destructor TIMyDataSet.Destroy;
begin
  Disconnect(False);

  inherited Destroy;
end;

function TIMyDataSet.GetExecuted: Boolean;
begin
  Result := Assigned( FHandle );
end;

function TIMyDataSet.GetHandle: PSDCursor;
begin
  Result := FHandle;
end;

function TIMyDataSet.GetSrvDatabase: TIMyDatabase;
begin
  Result := (inherited SrvDatabase) as TIMyDatabase;
end;

procedure TIMyDataSet.Check(Status: TSDEResult);
begin
	// if success
  if Status = 0 then
    SrvDatabase.Check( SrvDatabase.MySQLPtr )
  else
    SrvDatabase.Check( nil )
end;

procedure TIMyDataSet.Disconnect(Force: Boolean);
begin
  // nothing
end;

procedure TIMyDataSet.Execute;
begin
  if DataSet is TSDStoredProc
  then SpExecute
  else QExecute;
end;

function TIMyDataSet.ResultSetExists: Boolean;
begin
  Result := True;
end;

procedure TIMyDataSet.SetSelectBuffer;
begin
  // nothing
end;

procedure TIMyDataSet.QBindParams;
begin
  // nothing
end;

procedure TIMyDataSet.QExecute;
begin
  if not Executed then
    InternalQExecute;
end;

procedure TIMyDataSet.QPrepareSQL(Value: PChar);
begin
  FStmt		:= Value;
  FBindStmt	:= '';
end;

procedure TIMyDataSet.GetFieldDescs(Descs: TSDFieldDescList);
var
  pFieldDesc: PSDFieldDesc;
  ft: TFieldType;
  Col, NumCols: Integer;
  FldPtr: PMYSQL_FIELD;
begin
  if not Executed then
    InternalQExecute;
	// the statement can't return a result set
  if not Assigned(FHandle) then
    Exit;
  NumCols := mysql_num_fields( FHandle );
  for Col:=0 to NumCols-1 do begin
    FldPtr := mysql_fetch_field_direct( FHandle, Col );

    ft := SrvDatabase.FieldDataType(FldPtr^.ftype);
    if ft = ftUnknown then
      DatabaseErrorFmt( SBadFieldType, [StrPas(FldPtr^.name)] );
	// if the current blob field is not binary      
    if (ft = ftBlob) and ((FldPtr^.flags and BINARY_FLAG) = 0) then
      ft := ftMemo; 

    New( pFieldDesc );
    with pFieldDesc^ do begin
      FieldName	:= StrPas(FldPtr^.name);
      FieldNo	:= Col+1;
      DataType	:= ft;
      Size	:= FldPtr^.length;
      Precision	:= FldPtr^.decimals;
  	// null values are not permitted for the column (Required = True)
      Required	:= (FldPtr^.flags and NOT_NULL_FLAG) <> 0;
    end;

    Descs.Add( pFieldDesc );
  end;
end;

function TIMyDataSet.CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean;
var
  OutData: Pointer;
  FieldLenPtr: PLongInt;
  FldSize: Integer;
  sValue: string;
  dtValue: TDateTime;
begin
  FieldLenPtr := Pointer( LongInt(FFieldLens) + (AField.FieldNo-1)*SizeOf(FieldLenPtr^) );
  if FieldLenPtr^ > 0 then
    PFieldInfo(OutBuf)^.FetchStatus := indVALUE
  else
    PFieldInfo(OutBuf)^.FetchStatus := indNULL;

  if PFieldInfo(OutBuf)^.FetchStatus <> indNULL then begin
    OutData	:= Pointer( Integer(OutBuf) + SizeOf(TFieldInfo) );
    if SrvDatabase.RequiredCnvtFieldType(AField.DataType) then begin
      sValue := FRow[AField.FieldNo-1];
      case AField.DataType of
{$IFDEF SD_VCL4}
      ftLargeInt:
         begin
           Int64(OutData^) := StrToInt64Def( sValue, 0 );
           PFieldInfo(OutBuf)^.DataSize := SizeOf(Int64);
         end;
{$ENDIF}
      ftInteger, ftSmallInt:
        begin
          Integer(OutData^) := StrToIntDef( sValue, 0 );
          PFieldInfo(OutBuf)^.DataSize := SizeOf(Integer);
        end;
      ftFloat:
        begin
          Double(OutData^) := SqlStrToFloatDef(sValue, 0);
          PFieldInfo(OutBuf)^.DataSize := SizeOf(Double);
        end;
      ftTime:
        begin
          dtValue := SqlTimeToDateTime(sValue);
          TDateTimeRec(OutData^).Time := DateTimeToTimeStamp( dtValue ).Time;
          PFieldInfo(OutBuf)^.DataSize := SizeOf(TDateTimeRec);
        end;
      ftDate, ftDateTime:
        begin
          dtValue := SqlDateToDateTime(sValue);
          if AField.DataType in [ftDate] then
            TDateTimeRec(OutData^).Date := DateTimeToTimeStamp(dtValue).Date
          else
            TDateTimeRec(OutData^).DateTime := TimeStampToMSecs( DateTimeToTimeStamp(dtValue) );
          PFieldInfo(OutBuf)^.DataSize := SizeOf(TDateTimeRec);
        end;
      end;	// end of case
    end else begin
      if IsBlobType(AField.DataType) then begin
        Result := False;
        Exit;
      end;
      if AField.DataType = ftString then begin
      	// sometimes MySQL can return more data than it was described for this field
        if FieldLenPtr^ > AField.Size then
          FldSize := AField.Size
        else
          FldSize := FieldLenPtr^;
        Move( FRow[AField.FieldNo-1]^, OutData^, FldSize );
        PChar(OutData)[FldSize] := #0;
      end else
        Move( FRow[AField.FieldNo-1]^, OutData^, FieldLenPtr^ );
      PFieldInfo(OutBuf)^.DataSize := FieldLenPtr^;
    end;
  end;
  Result := True;
end;

function TIMyDataSet.ReadBlob(FieldNo: Integer; var BlobData: string): Longint;
var
  FieldLenPtr: PLongInt;
  BlobSize: LongInt;
  DataPtr: PChar;
begin
  DataPtr := nil;
  FieldLenPtr := Pointer( LongInt(FFieldLens) + (FieldNo-1)*SizeOf(FieldLenPtr^) );

  BlobSize := FieldLenPtr^;
  if BlobSize > 0 then
    DataPtr := FRow[FieldNo-1];
  if (BlobSize > 0) and Assigned(DataPtr) then begin
    SetLength(BlobData, BlobSize);
    System.Move(DataPtr^, PChar(BlobData)^, BlobSize);
  end;

  Result := BlobSize;
end;

function TIMyDataSet.FetchNextRow: Boolean;
begin
  Result := False;
	// the statement can't return a result set
  if not Assigned(FHandle) then
    Exit;
    
  FRow := mysql_fetch_row( FHandle );

  SrvDatabase.ResetIdleTimeOut;
  
  Result := Assigned( FRow );
  if Result then
    FFieldLens := mysql_fetch_lengths( FHandle );
  if Result and (BlobFieldCount > 0) then
    FetchBlobFields;
end;

{ Convert TDateTime to string for SQL statement }
function TIMyDataSet.CnvtDateTime2SqlString(Value: TDateTime): string;
  { Format date in ISO format 'yyyy-mm-dd' }
  function FormatSqlDate(Value: TDateTime): string;
  var
    Year, Month, Day: Word;
  begin
    DecodeDate(Value, Year, Month, Day);
    Result := Format('%.4d-%.2d-%.2d', [Year, Month, Day]);
  end;
  { Format time in ISO format 'hh:nn:ss' }
  function FormatSqlTime(Value: TDateTime): string;
  var
    Hour, Min, Sec, MSec: Word;
  begin
    DecodeTime(Value, Hour, Min, Sec, MSec);
    Result := Format('%.2d:%.2d:%.2d', [Hour, Min, Sec]);
  end;

begin
  if Trunc(Value) <> 0 then
    Result := FormatSqlDate(Value)
  else
    Result := '1899-12-30';

  if Frac(Value) <> 0 then begin
    if Result <> '' then
      Result := Result + ' ';
    Result := Result + FormatSqlTime(Value);
  end;
  Result := '''' + Result + '''';
end;

{ Convert float value to string with '.' delimiter }
function TIMyDataSet.CnvtFloat2SqlString(Value: Double): string;
var
  i: Integer;
begin
  Result := FloatToStr(Value);
  if DecimalSeparator <> '.' then begin
    i := AnsiPos(DecimalSeparator,Result);
    if i <> 0 then Result[i] := '.';
  end;
end;

function TIMyDataSet.CnvtVarBytes2EscapeString(Value: string): string;
var
  MaxSize: Integer;
begin
  MaxSize := 2*Length(Value) + 1;
  SetLength(Result, MaxSize);

  if Assigned(@mysql_real_escape_string) then
    MaxSize := mysql_real_escape_string(SrvDatabase.MySQLPtr, PChar(Result), PChar(Value), Length(Value))
  else
    MaxSize := mysql_escape_string(PChar(Result), PChar(Value), Length(Value));
  SetLength(Result, MaxSize);	// set length without terminating null
  Result := '''' + Result + '''';
end;

procedure TIMyDataSet.InternalQBindParams;
const
  ParamPrefix	= ':';
  SqlNullValue	= 'NULL';
  QuoteChar	= '"';	// for surroundings of the parameter's name, which can include, for example, spaces
var
  i: Integer;
  sFullParamName, sValue: string;
begin
  FBindStmt := FStmt;

  with Query do
    for i:=Query.ParamCount-1 downto 0 do begin
      	// set parameter value
      if Params[i].IsNull then begin
        sValue := SqlNullValue
      end else begin
        case Params[i].DataType of
{$IFDEF SD_VCL4}
          ftLargeInt,
{$ENDIF}
          ftInteger,
          ftSmallint,
          ftWord:	sValue := Params[i].AsString;
          ftBoolean:
            if Params[i].AsBoolean
            then sValue := '1' else sValue := '0';
          ftBytes,
          ftVarBytes,
          ftBlob:
            sValue := CnvtVarBytes2EscapeString(Params[i].Value);
          ftDate,
          ftTime,
          ftDateTime:	sValue := CnvtDateTime2SqlString(Params[i].AsDateTime);
          ftCurrency,
          ftFloat:	sValue := CnvtFloat2SqlString(Params[i].AsFloat);
        else
          sValue := CnvtVarBytes2EscapeString(Params[i].AsString);
        end;
      end;
	// change a parameter's name on a value of the parameter
      sFullParamName := ParamPrefix + Params[i].Name;
      if not ReplaceString( False, sFullParamName, sValue, FBindStmt ) then begin
      	// if parameter's name is enclosed in double quotation marks
        sFullParamName := Format( '%s%s%s%s', [ParamPrefix, QuoteChar, Params[i].Name, QuoteChar] );
        ReplaceString( False, sFullParamName, sValue, FBindStmt );
      end;
    end;
end;

procedure TIMyDataSet.InternalQExecute;
begin
  FRowsAffected := -1;
	// set parameter's values
  InternalQBindParams;

  Check( mysql_real_query( SrvDatabase.MySQLPtr, PChar(FBindStmt), Length(FBindStmt) ) );
  FHandle := mysql_store_result( SrvDatabase.MySQLPtr );
  if FHandle = nil then
    FRowsAffected := mysql_affected_rows( SrvDatabase.MySQLPtr );
end;

procedure TIMyDataSet.CloseResultSet;
begin
  if Assigned( FHandle ) then
    mysql_free_result(FHandle);
  FBindStmt := '';
  FHandle := nil;
end;

function TIMyDataSet.QGetRowsAffected: Integer;
begin
  Result := FRowsAffected;
end;

procedure TIMyDataSet.SpBindParams;
begin
  DatabaseError(SNoCapability);
end;

procedure TIMyDataSet.SpCreateParamDesc;
begin
  DatabaseError(SNoCapability);
end;

procedure TIMyDataSet.SpPrepareProc;
begin
  DatabaseError(SNoCapability);
end;

procedure TIMyDataSet.SpExecute;
begin
  DatabaseError(SNoCapability);
end;

procedure TIMyDataSet.SpExecProc;
begin
  DatabaseError(SNoCapability);
end;

procedure TIMyDataSet.SpGetResults;
begin
  DatabaseError(SNoCapability);
end;

function TIMyDataSet.SpNextResultSet: Boolean;
begin
  Result := False;
end;


initialization
  hSqlLibModule := 0;
  SqlLibRefCount:= 0;
  SqlLibLock 	:= TCriticalSection.Create;  
finalization
  while SqlLibRefCount > 0 do
    FreeSqlLib;
  SqlLibLock.Free;
end.
