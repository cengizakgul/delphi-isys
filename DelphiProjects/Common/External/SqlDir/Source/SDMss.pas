
{**********************************************************}
{							   }
{       Delphi SQLDirect Component Library		   }
{	MS SQLServer API (DB-LIB, 03-04-96) Interface Unit }
{                                                          }
{       Copyright (c) 1997,2002 by Yuri Sheino		   }
{                                                          }
{**********************************************************}

unit SDMss;

interface

{$I SqlDir.inc}

uses
  Windows, SysUtils, Classes, Forms, Db, SyncObjs,
{$IFDEF SD_VCL6}
  Variants,
{$ENDIF}
  SDEngine, SDConsts;

{*****************************************************************************
*                                                                            *
*     SQLFRONT.H - DB-Library header file for the Microsoft SQL Server.      *
*                                                                            *
* All constant and macro definitions for DB-Library applications programming *
* are contained in this file.  This file must be included before SQLDB.H and *
* one of the following #defines must be made, depending on the operating     *
* system: DBMSDOS, DBMSWIN or DBNTWIN32.                                     *
*                                                                            *
*****************************************************************************}

// call convention
// #define SQLAPI			cdecl

{*****************************************************************************
			Datatype definitions
*****************************************************************************}

type
  void 		= Pointer;

  INT		= Integer;
  LONG		= LongInt;
  USHORT	= Word;
  BOOL		= INT;
  LPINT		= ^INT;
  LPBYTE	= ^Byte;
  LPVOID	= ^void;

type

// DBPROCESS, LOGINREC and DBCURSOR
  PDBPROCESS	= ^Pointer;	 // pointer to the dbprocess structure
  PLOGINREC	= ^Pointer;      // pointer to the login record
  PDBCURSOR	= ^Pointer;      // pointer to the cursor record
  PDBHANDLE	= ^Pointer;      // pointer to the generic handle


{#define PTR *
typedef int (SQLAPI *SQLFARPROC)();
#else
typedef long (far pascal *LGFARPROC)();  // Windows loadable driver fp
#endif}



{*****************************************************************************
		DB-Library datatype definitions
*****************************************************************************}
const
  DBMAXCHAR	= 256; // Max length of DBVARBINARY and DBVARCHAR, etc.

type
  RETCODE	= INT;
  STATUS	= INT;

// DB-Library datatypes
  DBCHAR	= CHAR;
  DBBINARY	= UCHAR;
  DBTINYINT	= BYTE;
  DBSMALLINT	= SHORT;
  DBUSMALLINT	= USHORT;
  DBINT 	= LONG;
  DBFLT8	= Double;
  DBBIT 	= BYTE;
  DBBOOL	= BYTE;
  DBFLT4	= Single;
  DBMONEY4	= LONG;

  DBREAL	= DBFLT4;
  DBUBOOL	= UINT;

  DBDATETIME4	= record	// 4-byte smalldatetime
    numdays: USHORT;        	// No of days since Jan-1-1900
    nummins: USHORT;        	// No. of minutes since midnight
  end;
  DBDATETIM4	= DBDATETIME4;

  DBVARYCHAR	= record
    len: DBSMALLINT;
    str: array [0..DBMAXCHAR-1] of DBCHAR;
  end;

  DBVARYBIN	= record
    len: DBSMALLINT;
    arr: array[0..DBMAXCHAR-1] of BYTE;
  end;

  DBMONEY	= record
    mnyhigh: DBINT;
    mnylow:  ULONG;
  end;

  DBDATETIME	= record	// 8-byte datetime
    dtdays: DBINT;		// Days since Jan 1, 1900
    dttime: ULONG;		// 300ths of a second since midnight
  end;

// DBDATEREC structure used by dbdatecrack
  DBDATEREC	= record
    year: 	INT;    // 1753 - 9999
    quarter: 	INT;    // 1 - 4
    month: 	INT;    // 1 - 12
    dayofyear: 	INT;    // 1 - 366
    day: 	INT;    // 1 - 31
    week: 	INT;    // 1 - 54 (for leap years)
    weekday: 	INT;    // 1 - 7  (Mon - Sun)
    hour: 	INT;    // 0 - 23
    minute: 	INT;    // 0 - 59
    second:	INT;    // 0 - 59
    millisecond:INT;  	// 0 - 999
  end;

const
  MAXNUMERICLEN		= 16;
  MAXNUMERICDIG 	= 38;

  DEFAULTPRECISION	= 18;
  DEFAULTSCALE    	=  0;

type
  DBNUMERIC     = record
    precision: 	BYTE;
    scale: 	BYTE;
    sign: 	BYTE; // 1 = Positive, 0 = Negative
    val: 	array[0..MAXNUMERICLEN-1] of BYTE;
  end;

  DBDECIMAL	= DBNUMERIC;


// Pack the following structures on a word boundary
{#ifdef __BORLANDC__
#pragma option -a+
#else
	#ifndef DBLIB_SKIP_PRAGMA_PACK   // Define this if your compiler does not support #pragma pack()
	#pragma pack(2)
	#endif
#endif}

const
  MAXCOLNAMELEN 	= 30;
  MAXTABLENAME 		= 30;

type
	// SizeOf(TDBCOL) = 122, alignment on word boundary
  LPDBCOL		= ^TDBCOL;
  TDBCOL		= packed record
    SizeOfStruct:	DBINT;
    Name: 	array[0..MAXCOLNAMELEN] of CHAR;
    ActualName: array[0..MAXCOLNAMELEN] of CHAR;
    TableName: 	array[0..MAXTABLENAME] of CHAR;
    dummy1:		Byte;
    ColType:		SHORT;
    UserType: 		DBINT;
    MaxLength: 		DBINT;
    Precision: 		BYTE;
    Scale: 		BYTE;
    VarLength: 		BOOL;   // TRUE, FALSE
    Null: 		BYTE;   // TRUE, FALSE or DBUNKNOWN
    CaseSensitive: 	BYTE; 	// TRUE, FALSE or DBUNKNOWN
    Updatable:		BYTE;   // TRUE, FALSE or DBUNKNOWN
    dummy2:		Byte;
    Identity:		BOOL;   // TRUE, FALSE
  end;

const
  MAXSERVERNAME		= 30;
  MAXNETLIBNAME		= 255;
  MAXNETLIBCONNSTR	= 255;

type
  TDBPROCINFO    = record
    SizeOfStruct:	DBINT;
    ServerType:		BYTE;
    ServerMajor: 	USHORT;
    ServerMinor: 	USHORT;
    ServerRevision: 	USHORT;
    ServerName:		array[0..MAXSERVERNAME] of CHAR;
    NetLibName: 	array[0..MAXNETLIBNAME] of CHAR;
    NetLibConnStr: 	array[0..MAXNETLIBCONNSTR] of CHAR;
  end;
  LPDBPROCINFO	= ^TDBPROCINFO;

  TDBCURSORINFO	= record
    SizeOfStruct: 	DBINT;	// Use sizeof(DBCURSORINFO)
    TotCols: 		ULONG;  // Total Columns in cursor
    TotRows: 		ULONG;  // Total Rows in cursor
    CurRow: 		ULONG;  // Current actual row in server
    TotRowsFetched: 	ULONG; 	// Total rows actually fetched
    CurType:		ULONG;  // See CU_...
    Status: 		ULONG;  // See CU_...
  end;
  LPDBCURSORINFO	= ^TDBCURSORINFO;

const
  INVALID_UROWNUM	= ULONG(-1);

{*****************************************************************************
			Pointer Datatypes
*****************************************************************************}
type
  LPCINT 	= LPINT;
  LPCBYTE	= LPBYTE;
  LPUSHORT	= ^USHORT;
  LPCUSHORT	= LPUSHORT;
  LPDBINT	= ^DBINT;
  LPCDBINT 	= LPDBINT;
  LPDBBINARY	= ^DBBINARY;
  LPCDBBINARY	= LPDBBINARY;
  LPDBDATEREC	= ^DBDATEREC;
  LPCDBDATEREC	= LPDBDATEREC;
  LPDBDATETIME	= ^DBDATETIME;
  LPCDBDATETIME	= LPDBDATETIME;


{*****************************************************************************
			General #defines
*****************************************************************************}

const
  TIMEOUT_IGNORE	= ULONG(-1);
  TIMEOUT_INFINITE	= ULONG(0);
  TIMEOUT_MAXIMUM	= ULONG(1200);	// 20 minutes maximum timeout value

// Used for ServerType in dbgetprocinfo
  SERVTYPE_UNKNOWN	= 0;
  SERVTYPE_MICROSOFT	= 1;

// Used by dbcolinfo
//enum CI_TYPES { CI_REGULAR=1, CI_ALTERNATE=2, CI_CURSOR=3 };
  CI_REGULAR    = 1;
  CI_ALTERNATE  = 2;
  CI_CURSOR     = 3;

const
// Bulk Copy Definitions (bcp)
  DB_IN		= 1;         // Transfer from client to server
  DB_OUT	= 2;         // Transfer from server to client

  BCPMAXERRS	= 1;	// bcp_control parameter
  BCPFIRST	= 2;    // bcp_control parameter
  BCPLAST     	= 3;    // bcp_control parameter
  BCPBATCH    	= 4;    // bcp_control parameter
  BCPKEEPNULLS	= 5;    // bcp_control parameter
  BCPABORT    	= 6;    // bcp_control parameter

// redefine predefined constants
  DB_TRUE 	= 1;
  DB_FALSE 	= 0;

  TINYBIND		= 1;
  SMALLBIND		= 2;
  INTBIND  		= 3;
  CHARBIND 		= 4;
  BINARYBIND		= 5;
  BITBIND   		= 6;
  DATETIMEBIND		= 7;
  MONEYBIND   		= 8;
  FLT8BIND    		= 9;
  STRINGBIND  		= 10;
  NTBSTRINGBIND		= 11;
  VARYCHARBIND		= 12;
  VARYBINBIND 		= 13;
  FLT4BIND    		= 14;
  SMALLMONEYBIND 	= 15;
  SMALLDATETIBIND	= 16;
  DECIMALBIND 		= 17;
  NUMERICBIND 		= 18;
  SRCDECIMALBIND	= 19;
  SRCNUMERICBIND	= 20;
  MAXBIND       	= SRCNUMERICBIND;

  DBSAVE	= 1;
  DBNOSAVE      = 0;

  DBNOERR	= -1;
  DBFINDONE	= $04;  // Definately done
  DBMORE	= $10;  // Maybe more commands waiting
  DBMORE_ROWS	= $20;  // This command returned rows

  MAXNAME	= 31;


  DBTXTSLEN	= 8;     // Timestamp length

  DBTXPLEN	= 16;    // Text pointer length

// Error code returns
  INT_EXIT	= 0;
  INT_CONTINUE	= 1;
  INT_CANCEL	= 2;


// dboptions
  DBBUFFER      = 0;
  DBOFFSET      = 1;
  DBROWCOUNT    = 2;
  DBSTAT        = 3;
  DBTEXTLIMIT   = 4;
  DBTEXTSIZE    = 5;
  DBARITHABORT  = 6;
  DBARITHIGNORE = 7;
  DBNOAUTOFREE  = 8;
  DBNOCOUNT     = 9;
  DBNOEXEC      = 10;
  DBPARSEONLY   = 11;
  DBSHOWPLAN    = 12;
  DBSTORPROCID	= 13;

  DBANSItoOEM	= 14;
  DBOEMtoANSI	= 15;


  DBCLIENTCURSORS	= 16;
  DBSETTIME_OPT		= 17;
  DBQUOTEDIDENT 	= 18;


// Data Type Tokens
  SQLVOID       = $1f;
  SQLTEXT       = $23;
  SQLVARBINARY  = $25;
  SQLINTN       = $26;
  SQLVARCHAR    = $27;
  SQLBINARY     = $2d;
  SQLIMAGE      = $22;
  SQLCHAR       = $2f;
  SQLINT1       = $30;
  SQLBIT        = $32;
  SQLINT2       = $34;
  SQLINT4       = $38;
  SQLMONEY      = $3c;
  SQLDATETIME   = $3d;
  SQLFLT8       = $3e;
  SQLFLTN       = $6d;
  SQLMONEYN     = $6e;
  SQLDATETIMN   = $6f;
  SQLFLT4       = $3b;
  SQLMONEY4     = $7a;
  SQLDATETIM4   = $3a;
  SQLDECIMAL    = $6a;
  SQLNUMERIC    = $6c;

// Data stream tokens
  SQLCOLFMT     = $a1;
  OLD_SQLCOLFMT = $2a;
  SQLPROCID     = $7c;
  SQLCOLNAME    = $a0;
  SQLTABNAME    = $a4;
  SQLCOLINFO    = $a5;
  SQLALTNAME    = $a7;
  SQLALTFMT     = $a8;
  SQLERROR      = $aa;
  SQLINFO       = $ab;
  SQLRETURNVALUE= $ac;
  SQLRETURNSTATUS= $79;
  SQLRETURN     = $db;
  SQLCONTROL    = $ae;
  SQLALTCONTROL = $af;
  SQLROW        = $d1;
  SQLALTROW     = $d3;
  SQLDONE       = $fd;
  SQLDONEPROC   = $fe;
  SQLDONEINPROC = $ff;
  SQLOFFSET     = $78;
  SQLORDER      = $a9;
  SQLLOGINACK   = $ad; // NOTICE: change to real value

// Ag op tokens
  SQLAOPCNT	= $4b;
  SQLAOPSUM	= $4d;
  SQLAOPAVG	= $4f;
  SQLAOPMIN	= $51;
  SQLAOPMAX	= $52;
  SQLAOPANY	= $53;
  SQLAOPNOOP	= $56;

// Error numbers (dberrs) DB-Library error codes
  SQLEMEM	= 10000;
  SQLENULL	= 10001;
  SQLENLOG	= 10002;
  SQLEPWD 	= 10003;
  SQLECONN	= 10004;
  SQLEDDNE	= 10005;
  SQLENULLO	= 10006;
  SQLESMSG 	= 10007;
  SQLEBTOK 	= 10008;
  SQLENSPE 	= 10009;
  SQLEREAD 	= 10010;
  SQLECNOR 	= 10011;
  SQLETSIT 	= 10012;
  SQLEPARM 	= 10013;
  SQLEAUTN	= 10014;
  SQLECOFL	= 10015;
  SQLERDCN	= 10016;
  SQLEICN 	= 10017;
  SQLECLOS	= 10018;
  SQLENTXT	= 10019;
  SQLEDNTI	= 10020;
  SQLETMTD	= 10021;
  SQLEASEC	= 10022;
  SQLENTLL	= 10023;
  SQLETIME	= 10024;
  SQLEWRIT	= 10025;
  SQLEMODE	= 10026;
  SQLEOOB 	= 10027;
  SQLEITIM	= 10028;
  SQLEDBPS	= 10029;
  SQLEIOPT	= 10030;
  SQLEASNL	= 10031;
  SQLEASUL	= 10032;
  SQLENPRM	= 10033;
  SQLEDBOP	= 10034;
  SQLENSIP	= 10035;
  SQLECNULL	= 10036;
  SQLESEOF	= 10037;
  SQLERPND	= 10038;
  SQLECSYN	= 10039;
  SQLENONET	= 10040;
  SQLEBTYP	= 10041;
  SQLEABNC	= 10042;
  SQLEABMT	= 10043;
  SQLEABNP	= 10044;
  SQLEBNCR	= 10045;
  SQLEAAMT	= 10046;
  SQLENXID	= 10047;
  SQLEIFNB	= 10048;
  SQLEKBCO	= 10049;
  SQLEBBCI	= 10050;
  SQLEKBCI	= 10051;
  SQLEBCWE	= 10052;
  SQLEBCNN	= 10053;
  SQLEBCOR	= 10054;
  SQLEBCPI	= 10055;
  SQLEBCPN	= 10056;
  SQLEBCPB	= 10057;
  SQLEVDPT	= 10058;
  SQLEBIVI	= 10059;
  SQLEBCBC	= 10060;
  SQLEBCFO	= 10061;
  SQLEBCVH	= 10062;
  SQLEBCUO	= 10063;
  SQLEBUOE	= 10064;
  SQLEBWEF	= 10065;
  SQLEBTMT	= 10066;
  SQLEBEOF	= 10067;
  SQLEBCSI	= 10068;
  SQLEPNUL	= 10069;
  SQLEBSKERR	= 10070;
  SQLEBDIO	= 10071;
  SQLEBCNT	= 10072;
  SQLEMDBP	= 10073;
  SQLINIT	= 10074;
  SQLCRSINV	= 10075;
  SQLCRSCMD	= 10076;
  SQLCRSNOIND	= 10077;
  SQLCRSDIS	= 10078;
  SQLCRSAGR	= 10079;
  SQLCRSORD	= 10080;
  SQLCRSMEM	= 10081;
  SQLCRSBSKEY	= 10082;
  SQLCRSNORES	= 10083;
  SQLCRSVIEW	= 10084;
  SQLCRSBUFR	= 10085;
  SQLCRSFROWN	= 10086;
  SQLCRSBROL 	= 10087;
  SQLCRSFRAND	= 10088;
  SQLCRSFLAST	= 10089;
  SQLCRSRO   	= 10090;
  SQLCRSTAB  	= 10091;
  SQLCRSUPDTAB	= 10092;
  SQLCRSUPDNB	= 10093;
  SQLCRSVIIND	= 10094;
  SQLCRSNOUPD	= 10095;
  SQLCRSOS2	= 10096;
  SQLEBCSA 	= 10097;
  SQLEBCRO	= 10098;
  SQLEBCNE	= 10099;
  SQLEBCSK	= 10100;
  SQLEUVBF	= 10101;
  SQLEBIHC	= 10102;
  SQLEBWFF	= 10103;
  SQLNUMVAL	= 10104;
  SQLEOLDVR	= 10105;
  SQLEBCPS	= 10106;
  SQLEDTC 	= 10107;
  SQLENOTIMPL	= 10108;
  SQLENONFLOAT	= 10109;
  SQLECONNFB	= 10110;


// The severity levels are defined here
  EXINFO	= 1;	// Informational, non-error
  EXUSER	= 2;  	// User error
  EXNONFATAL	= 3;  	// Non-fatal error
  EXCONVERSION	= 4;  	// Error in DB-LIBRARY data conversion
  EXSERVER   	= 5;  	// The Server has returned an error flag
  EXTIME     	= 6;  	// We have exceeded our timeout period while
                     	// waiting for a response from the Server - the
                     	// DBPROCESS is still alive
  EXPROGRAM	= 7;  	// Coding error in user program
  EXRESOURCE	= 8;  	// Running out of resources - the DBPROCESS may be dead
  EXCOMM 	= 9;  	// Failure in communication with Server - the DBPROCESS is dead
  EXFATAL	= 10;	// Fatal error - the DBPROCESS is dead
  EXCONSISTENCY	= 11;	// Internal software error  - notify MS Technical Supprt

// Offset identifiers
  OFF_SELECT    = $16d;
  OFF_FROM      = $14f;
  OFF_ORDER     = $165;
  OFF_COMPUTE   = $139;
  OFF_TABLE     = $173;
  OFF_PROCEDURE = $16a;
  OFF_STATEMENT = $1cb;
  OFF_PARAM     = $1c4;
  OFF_EXEC      = $12c;

// Print lengths for certain fixed length data types
  PRINT4      = 11;
  PRINT2      = 6;
  PRINT1      = 3;
  PRFLT8      = 20;
  PRMONEY     = 26;
  PRBIT       = 3;
  PRDATETIME  = 27;
  PRDECIMAL   = (MAXNUMERICDIG + 2);
  PRNUMERIC   = (MAXNUMERICDIG + 2);

  SUCCEED 	= 1;
  FAIL    	= 0;
  SUCCEED_ABORT = 2;

  DBUNKNOWN 	= 2;

  MORE_ROWS    	= -1;
  NO_MORE_ROWS 	= -2;
  REG_ROW      	= MORE_ROWS;
  BUF_FULL     	= -3;

// Status code for dbresults(). Possible return values are SUCCEED, FAIL, and NO_MORE_RESULTS.
  NO_MORE_RESULTS	= 2;
  NO_MORE_RPC_RESULTS	= 3;

// Macros for dbsetlname()
  DBSETHOST	= 1;
  DBSETUSER	= 2;
  DBSETPWD 	= 3;
  DBSETAPP 	= 4;
  DBSETID  	= 5;
  DBSETLANG	= 6;
  DBSETSECURE	= 7;
  DBVER42 	= 8;
  DBVER60 	= 9;
  DBSETLOGINTIME_OPT= 10;
  DBSETFALLBACK = 12;

// Standard exit and error values
  STDEXIT 	= 0;
  ERREXIT 	= -1;

// dbrpcinit flags
  DBRPCRECOMPILE= $0001;
  DBRPCRESET    = $0004;
  DBRPCCURSOR   = $0008;

// dbrpcparam flags
  DBRPCRETURN   = $1;
  DBRPCDEFAULT  = $2;


{ 		Cursor related constants 		}

// Following flags are used in the concuropt parameter in the dbcursoropen function
  CUR_READONLY 	= 1;	// Read only cursor, no data modifications
  CUR_LOCKCC   	= 2; 	// Intent to update, all fetched data locked when
                 	// dbcursorfetch is called inside a transaction block
  CUR_OPTCC    	= 3;	// Optimistic concurrency control, data modifications
                 	// succeed only if the row hasn't been updated since
                 	// the last fetch.
  CUR_OPTCCVAL 	= 4; 	// Optimistic concurrency control based on selected column values

// Following flags are used in the scrollopt parameter in dbcursoropen
  CUR_FORWARD 	= 0;    // Forward only scrolling
  CUR_KEYSET  	= -1;   // Keyset driven scrolling
  CUR_DYNAMIC 	= 1;    // Fully dynamic
  CUR_INSENSITIVE= -2;	// Server-side cursors only

// Following flags define the fetchtype in the dbcursorfetch function
  FETCH_FIRST	= 1;	// Fetch first n rows
  FETCH_NEXT 	= 2;	// Fetch next n rows
  FETCH_PREV 	= 3;	// Fetch previous n rows
  FETCH_RANDOM	= 4;	// Fetch n rows beginning with given row #
  FETCH_RELATIVE= 5;	// Fetch relative to previous fetch row #
  FETCH_LAST 	= 6;	// Fetch the last n rows

// Following flags define the per row status as filled by dbcursorfetch and/or dbcursorfetchex
  FTC_EMPTY		= $00;  // No row available
  FTC_SUCCEED		= $01;  // Fetch succeeded, (failed if not set)
  FTC_MISSING		= $02;  // The row is missing
  FTC_ENDOFKEYSET 	= $04;  // End of the keyset reached
  FTC_ENDOFRESULTS	= $08;	// End of results set reached

// Following flags define the operator types for the dbcursor function
  CRS_UPDATE 	= 1;  	// Update operation
  CRS_DELETE 	= 2;  	// Delete operation
  CRS_INSERT 	= 3;  	// Insert operation
  CRS_REFRESH 	= 4;  	// Refetch given row
  CRS_LOCKCC 	= 5;	// Lock given row

// Following value can be passed to the dbcursorbind function for NOBIND type
  NOBIND 	= -2;	// Return length and pointer to data

// Following are values used by DBCURSORINFO's Type parameter
  CU_CLIENT     = $00000001;
  CU_SERVER     = $00000002;
  CU_KEYSET     = $00000004;
  CU_MIXED      = $00000008;
  CU_DYNAMIC    = $00000010;
  CU_FORWARD    = $00000020;
  CU_INSENSITIVE= $00000040;
  CU_READONLY   = $00000080;
  CU_LOCKCC     = $00000100;
  CU_OPTCC      = $00000200;
  CU_OPTCCVAL   = $00000400;

// Following are values used by DBCURSORINFO's Status parameter
  CU_FILLING    = $00000001;
  CU_FILLED     = $00000002;


// Following are values used by dbupdatetext's type parameter
  UT_TEXTPTR    = $0001;
  UT_TEXT       = $0002;
  UT_MORETEXT   = $0004;
  UT_DELETEONLY = $0008;
  UT_LOG        = $0010;


// The following values are passed to dbserverenum for searching criteria.
  NET_SEARCH 	= $0001;
  LOC_SEARCH 	= $0002;

// These constants are the possible return values from dbserverenum.
  ENUM_SUCCESS		= $0000;
  MORE_DATA   		= $0001;
  NET_NOT_AVAIL		= $0002;
  OUT_OF_MEMORY		= $0004;
  NOT_SUPPORTED		= $0008;
  ENUM_INVALID_PARAM	= $0010;


// Netlib Error problem codes.  ConnectionError() should return one of
// these as the dblib-mapped problem code, so the corresponding string
// is sent to the dblib app's error handler as dberrstr.  Return NE_E_NOMAP
// for a generic DB-Library error string (as in prior versions of dblib).

  NE_E_NOMAP      	= 0;   	// No string; uses dblib default.
  NE_E_NOMEMORY   	= 1;   	// Insufficient memory.
  NE_E_NOACCESS   	= 2;   	// Access denied.
  NE_E_CONNBUSY   	= 3;   	// Connection is busy.
  NE_E_CONNBROKEN 	= 4;   	// Connection broken.
  NE_E_TOOMANYCONN	= 5;   	// Connection limit exceeded.
  NE_E_SERVERNOTFOUND	= 6;   	// Specified SQL server not found.
  NE_E_NETNOTSTARTED 	= 7;   	// The network has not been started.
  NE_E_NORESOURCE	= 8;   	// Insufficient network resources.
  NE_E_NETBUSY    	= 9;   	// Network is busy.
  NE_E_NONETACCESS	= 10;  	// Network access denied.
  NE_E_GENERAL    	= 11;  	// General network error.  Check your documentation.
  NE_E_CONNMODE   	= 12;  	// Incorrect connection mode.
  NE_E_NAMENOTFOUND	= 13;  	// Name not found in directory service.
  NE_E_INVALIDCONN 	= 14;  	// Invalid connection.
  NE_E_NETDATAERR  	= 15;  	// Error reading or writing network data.
  NE_E_TOOMANYFILES	= 16;  	// Too many open file handles.
  NE_E_CANTCONNECT 	= 17;	// SQL Server does not exist or access denied.

  NE_MAX_NETERROR 	= 17;


{*****************************************************************************
*                                                                            *
*       SQLDB.H - DB-Library header file for the Microsoft SQL Server.       *
*                                                                            *
*****************************************************************************}


{// Macros for setting the PLOGINREC
#define DBSETLHOST(a,b)    dbsetlname   ((a), (b), DBSETHOST)
#define DBSETLUSER(a,b)    dbsetlname   ((a), (b), DBSETUSER)
#define DBSETLPWD(a,b)     dbsetlname   ((a), (b), DBSETPWD)
#define DBSETLAPP(a,b)     dbsetlname   ((a), (b), DBSETAPP)
#define BCP_SETL(a,b)      bcp_setl     ((a), (b))
#define DBSETLNATLANG(a,b) dbsetlname   ((a), (b), DBSETLANG)
#define DBSETLPACKET(a,b)  dbsetlpacket ((a), (b))
#define DBSETLSECURE(a)    dbsetlname   ((a), 0,   DBSETSECURE)
#define DBSETLVERSION(a,b) dbsetlname   ((a), 0,  (b))
#define DBSETLTIME(a,b)		dbsetlname    ((a), (LPCSTR)(ULONG)(b), DBSETLOGINTIME)
#define DBSETLFALLBACK(a,b) dbsetlname   ((a), (b),   DBSETFALLBACK)
}

{
/*****************************************************************************
* Windows 3.x and Non-Windows 3.x differences.                               *
*****************************************************************************/

#define dbwinexit()

#define DBLOCKLIB()
#define DBUNLOCKLIB()

typedef INT (SQLAPI *DBERRHANDLE_PROC)(PDBPROCESS, INT, INT, INT, LPCSTR, LPCSTR);
typedef INT (SQLAPI *DBMSGHANDLE_PROC)(PDBPROCESS, DBINT, INT, INT, LPCSTR, LPCSTR, LPCSTR, DBUSMALLINT);

extern DBERRHANDLE_PROC SQLAPI dberrhandle(DBERRHANDLE_PROC);
extern DBMSGHANDLE_PROC SQLAPI dbmsghandle(DBMSGHANDLE_PROC);

extern DBERRHANDLE_PROC SQLAPI dbprocerrhandle(PDBHANDLE, DBERRHANDLE_PROC);
extern DBMSGHANDLE_PROC SQLAPI dbprocmsghandle(PDBHANDLE, DBMSGHANDLE_PROC);
}
type
  TDBERRHANDLE_PROC =   function (dbproc: PDBPROCESS; severity, dberr, oserr: INT; dberrstr, oserrstr: PChar): INT; cdecl;
  TDBMSGHANDLE_PROC =   function (dbproc: PDBPROCESS; msgno: DBINT; msgstate, severity: INT;
                                 msgtext, srvname, procname: PChar; line: DBUSMALLINT): INT; cdecl;

{*****************************************************************************
* Function Prototypes                                                        *
*****************************************************************************}

{// Functions macros
#define DBCMDROW(a)      dbcmdrow(a)
#define DBCOUNT(a)       dbcount (a)
#define DBCURCMD(a)      dbcurcmd(a)
#define DBCURROW(a)      dbcurrow(a)
#define DBDEAD(a)        dbdead(a)
#define DBFIRSTROW(a)    dbfirstrow(a)
#define DBGETTIME()      dbgettime()
#define DBISAVAIL(a)     dbisavail(a)
#define DBLASTROW(a)     dblastrow(a)
#define DBMORECMDS(a)    dbmorecmds(a)
#define DBNUMORDERS(a)   dbnumorders(a)
#define dbrbuf(a)        ((DBINT)dbdataready(a))
#define DBRBUF(a)        ((DBINT)dbdataready(a))
#define DBROWS(a)        dbrows (a)
#define DBROWTYPE(a)     dbrowtype (a)
}

var
// Two-phase commit functions
  abort_xact:	function(connect: PDBPROCESS; CommId: DBINT): RETCODE; cdecl;
  build_xact_string: procedure (xact_name, service_name: LPCSTR; CommId: DBINT; sResult: LPSTR); cdecl;
  close_commit: procedure (connect: PDBPROCESS); cdecl;
  commit_xact:	function (connect: PDBPROCESS; commId: DBINT): RETCODE; cdecl;
  open_commit:	function (login: PLOGINREC; servername: LPCSTR): PDBPROCESS; cdecl;
  remove_xact:	function (connect: PDBPROCESS; commId: DBINT; n: INT): RETCODE; cdecl;
  scan_xact:	function (connect: PDBPROCESS; commId: DBINT): RETCODE; cdecl;
  start_xact:	function (connect: PDBPROCESS; application_name, xact_name: LPCSTR; site_count: INT): DBINT; cdecl;
  stat_xact:	function (connect: PDBPROCESS; commId: DBINT): INT; cdecl;

// BCP functions
  bcp_batch:	function (dbproc: PDBPROCESS): DBINT; cdecl;
  bcp_bind:	function (dbproc: PDBPROCESS; varaddr: LPCBYTE;
  			prefixlen: INT; varlen: DBINT; terminator: LPCBYTE;
        		termlen, datatype, table_column: INT): RETCODE; cdecl;
  bcp_colfmt:	function (dbproc: PDBPROCESS; file_column: INT; file_type: BYTE;
  			file_prefixlen: INT; file_collen: DBINT; file_term: LPCBYTE;
        		file_termlen, table_column: INT): RETCODE; cdecl;
  bcp_collen:	function (dbproc: PDBPROCESS; varlen: DBINT; table_column: INT): RETCODE; cdecl;
  bcp_colptr:	function (dbproc: PDBPROCESS; colptr: LPCBYTE; table_column: INT): RETCODE; cdecl;
  bcp_columns:	function (dbproc: PDBPROCESS; file_colcount: INT): RETCODE; cdecl;
  bcp_control:	function (dbproc: PDBPROCESS; field: INT; value: DBINT): RETCODE; cdecl;
  bcp_done:	function (dbproc: PDBPROCESS): DBINT; cdecl;
  bcp_exec:	function (dbproc: PDBPROCESS; rows_copied: LPDBINT): RETCODE; cdecl;
  bcp_init:	function (dbproc: PDBPROCESS; tblname, hfile, errfile: LPCSTR; direction: INT): RETCODE; cdecl;
  bcp_moretext:	function (dbproc: PDBPROCESS; size: DBINT; text: LPCBYTE): RETCODE; cdecl;
  bcp_readfmt:	function (dbproc: PDBPROCESS; filename: LPCSTR): RETCODE; cdecl;
  bcp_sendrow:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  bcp_setl:	function (loginrec: PLOGINREC; enable: BOOL): RETCODE; cdecl;
  bcp_writefmt:	function (dbproc: PDBPROCESS; filename: LPCSTR): RETCODE; cdecl;

// Standard DB-Library functions
  dbadata:	function (dbproc: PDBPROCESS; computeId, column: INT): LPCBYTE; cdecl;
  dbadlen:	function (dbproc: PDBPROCESS; computeId, column: INT): DBINT; cdecl;
  dbaltbind:	function (dbproc: PDBPROCESS; computeId, column, vartype: INT;
  			varlen: DBINT; varaddr: LPCBYTE): RETCODE; cdecl;
  dbaltcolid:	function (dbproc: PDBPROCESS; computeId, column: INT): INT; cdecl;
  dbaltlen:	function (dbproc: PDBPROCESS; computeId, column: INT): DBINT; cdecl;
  dbaltop:	function (dbproc: PDBPROCESS; computeId, column: INT): INT; cdecl;
  dbalttype:	function (dbproc: PDBPROCESS; computeId, column: INT): INT; cdecl;
  dbaltutype:	function (dbproc: PDBPROCESS; computeId, column: INT): DBINT; cdecl;
  dbanullbind:	function (dbproc: PDBPROCESS; computeId, column: INT; indicator: LPCDBINT): RETCODE; cdecl;
  dbbind:	function (dbproc: PDBPROCESS; column, vartype: INT; varlen: DBINT; varaddr: LPBYTE): RETCODE; cdecl;
  dbbylist:	function (dbproc: PDBPROCESS; computeId: INT; size: LPINT): LPCBYTE; cdecl;
  dbcancel:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  dbcanquery:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  dbchange:	function (dbproc: PDBPROCESS): LPCSTR; cdecl;
  dbclose:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  dbclrbuf: 	procedure (dbproc: PDBPROCESS; n: DBINT); cdecl;
  dbclropt:	function (dbproc: PDBPROCESS; option: INT; param: LPCSTR): RETCODE; cdecl;
  dbcmd:	function (dbproc: PDBPROCESS; cmdstring: LPCSTR): RETCODE; cdecl;
  dbcmdrow:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  dbcolbrowse:	function (dbproc: PDBPROCESS; colnum: INT): BOOL; cdecl;
  dbcolinfo:	function (dbHandle: PDBHANDLE; colinfo, column, computeId: INT; lpDbCol: LPDBCOL): RETCODE; cdecl;
  dbcollen:	function (dbproc: PDBPROCESS; column: INT): DBINT; cdecl;
  dbcolname:	function (dbproc: PDBPROCESS; column: INT): LPCSTR; cdecl;
  dbcolsource:	function (dbproc: PDBPROCESS; colnum: INT): LPCSTR; cdecl;
  dbcoltype:	function (dbproc: PDBPROCESS; column: INT): INT; cdecl;
  dbcolutype:	function (dbproc: PDBPROCESS; column: INT): DBINT; cdecl;
  dbconvert:	function (dbproc: PDBPROCESS; srctype: INT; src: LPCBYTE; srclen: DBINT;
  			desttype: INT; dest: LPBYTE; destlen: DBINT): INT; cdecl;
  dbcount:	function (dbproc: PDBPROCESS): DBINT; cdecl;
  dbcurcmd:	function (dbproc: PDBPROCESS): INT; cdecl;
  dbcurrow:	function (dbproc: PDBPROCESS): DBINT; cdecl;
  dbcursor:	function (hc: PDBCURSOR; optype, row: INT; table, values: LPCSTR): RETCODE; cdecl;
  dbcursorbind:	function (hc: PDBCURSOR; col, vartype: INT; varlen: DBINT; poutlen: LPDBINT; pvaraddr: LPBYTE): RETCODE; cdecl;
  dbcursorclose:function (handle: PDBHANDLE): RETCODE; cdecl;
  dbcursorcolinfo:function (hcursor: PDBCURSOR; column: INT; colname: LPSTR;
  			coltype: LPINT; collen: LPDBINT; usertype: LPINT): RETCODE; cdecl;
  dbcursorfetch:function (hc: PDBCURSOR; fetchtype, rownum: INT): RETCODE; cdecl;
  dbcursorfetchex:function (hc: PDBCURSOR; fetchtype: INT; rownum, nfetchrows, reserved: DBINT): RETCODE; cdecl;
  dbcursorinfo:	function (hcursor: PDBCURSOR; ncols: LPINT; nrows: LPDBINT): RETCODE; cdecl;
  dbcursorinfoex:function (hc: PDBCURSOR; pdbcursorinfo: LPDBCURSORINFO): RETCODE; cdecl;
  dbcursoropen:	function (dbproc: PDBPROCESS; stmt: LPCSTR; scrollopt, concuropt: INT;
  			nrows: UINT; pstatus: LPDBINT): PDBCURSOR; cdecl;
  dbdata:	function (dbproc: PDBPROCESS; column: INT): LPCBYTE; cdecl;
  dbdataready:	function (dbproc: PDBPROCESS): BOOL; cdecl;
  dbdatecrack:	function (dbproc: PDBPROCESS; dateinfo: LPDBDATEREC; datetime: LPCDBDATETIME): RETCODE; cdecl;
  dbdatlen:	function (dbproc: PDBPROCESS; column: INT): DBINT; cdecl;
  dbdead:	function (dbproc: PDBPROCESS): BOOL; cdecl;
  dbexit:	procedure ; cdecl;
  dbenlisttrans:function(dbproc: PDBPROCESS; pTransaction: LPVOID): RETCODE; cdecl;
  dbenlistxatrans:function(dbproc: PDBPROCESS; enlisttran: BOOL): RETCODE; cdecl;
//  dbfcmd:	function (PDBPROCESS, LPCSTR, ...): RETCODE; cdecl;
  dbfirstrow:	function (dbproc: PDBPROCESS): DBINT; cdecl;
  dbfreebuf:	procedure (dbproc: PDBPROCESS); cdecl;
  dbfreelogin:	procedure (login: PLOGINREC); cdecl;
  dbfreequal:	procedure (ptr: LPCSTR); cdecl;
  dbgetchar:	function (dbproc: PDBPROCESS; n: INT): LPSTR; cdecl;
  dbgetmaxprocs:function : SHORT; cdecl;
  dbgetoff:	function (dbproc: PDBPROCESS; offtype: DBUSMALLINT; startfrom: INT): INT; cdecl;
  dbgetpacket:	function (dbproc: PDBPROCESS): UINT; cdecl;
  dbgetrow:	function (dbproc: PDBPROCESS; row: DBINT): STATUS; cdecl;
  dbgettime:	function : INT; cdecl;
  dbgetuserdata:function (dbproc: PDBPROCESS): LPVOID; cdecl;
  dbhasretstat:	function (dbproc: PDBPROCESS): DBBOOL; cdecl;
  dbinit:	function : LPCSTR; cdecl;
  dbisavail:	function (dbproc: PDBPROCESS): BOOL; cdecl;
  dbiscount:	function (dbproc: PDBPROCESS): BOOL; cdecl;
  dbisopt:	function (dbproc: PDBPROCESS; option: INT; param: LPCSTR): BOOL; cdecl;
  dblastrow:	function (dbproc: PDBPROCESS): DBINT; cdecl;
  dblogin:	function : PLOGINREC; cdecl;
  dbmorecmds:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  dbmoretext:	function (dbproc: PDBPROCESS; size: DBINT; text: LPCBYTE): RETCODE; cdecl;
  dbname:	function (dbproc: PDBPROCESS): LPCSTR; cdecl;
  dbnextrow:	function (dbproc: PDBPROCESS): STATUS; cdecl;
  dbnullbind:	function (dbproc: PDBPROCESS; column: INT; indicator: LPCDBINT): RETCODE; cdecl;
  dbnumalts:	function (dbproc: PDBPROCESS; computeId: INT): INT; cdecl;
  dbnumcols:	function (dbproc: PDBPROCESS): INT; cdecl;
  dbnumcompute:	function (dbproc: PDBPROCESS): INT; cdecl;
  dbnumorders:	function (dbproc: PDBPROCESS): INT; cdecl;
  dbnumrets:	function (dbproc: PDBPROCESS): INT; cdecl;
  dbopen:	function (login: PLOGINREC; servername: LPCSTR): PDBPROCESS; cdecl;
  dbordercol:	function (dbproc: PDBPROCESS; order: INT): INT; cdecl;
  dbprocinfo:	function (dbproc: PDBPROCESS; pdbprocinfo: LPDBPROCINFO): RETCODE; cdecl;
  dbprhead:	procedure (dbproc: PDBPROCESS); cdecl;
  dbprrow:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  dbprtype:	function (token: INT): LPCSTR; cdecl;
  dbqual:	function (dbproc: PDBPROCESS; tabnum: INT; tabname: LPCSTR): LPCSTR; cdecl;
//  dbreadpage:	function (dbproc: PDBPROCESS; LPCSTR, DBINT, LPBYTE): DBINT; cdecl;
  dbreadtext:	function (dbproc: PDBPROCESS; buf: LPVOID; bufsize: DBINT): DBINT; cdecl;
  dbresults:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  dbretdata:	function (dbproc: PDBPROCESS; retnum: INT): LPCBYTE; cdecl;
  dbretlen:	function (dbproc: PDBPROCESS; retnum: INT): DBINT; cdecl;
  dbretname:	function (dbproc: PDBPROCESS; retnum: INT): LPCSTR; cdecl;
  dbretstatus:	function (dbproc: PDBPROCESS): DBINT; cdecl;
  dbrettype:	function (dbproc: PDBPROCESS; retnum: INT): INT; cdecl;
  dbrows:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  dbrowtype:	function (dbproc: PDBPROCESS): STATUS; cdecl;
  dbrpcinit:	function (dbproc: PDBPROCESS; rpcname: LPCSTR; options: DBSMALLINT): RETCODE; cdecl;
  dbrpcparam:	function (dbproc: PDBPROCESS; paramname: LPCSTR; status: BYTE;
  			datatype: INT; maxlen, datalen: DBINT; value: LPCBYTE): RETCODE; cdecl;
  dbrpcsend:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  dbrpcexec:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
//  dbrpwclr:	procedure (PLOGINREC); cdecl;
//  dbrpwset:	function (PLOGINREC, LPCSTR, LPCSTR, INT): RETCODE; cdecl;
  dbserverenum:	function (searchmode: USHORT; servnamebuf: LPSTR; sizeservnamebuf: USHORT; numentries: LPUSHORT): INT; cdecl;
  dbsetavail:	procedure (dbproc: PDBPROCESS); cdecl;
  dbsetmaxprocs:function (maxprocs: SHORT): RETCODE; cdecl;
  dbsetlname:	function (login: PLOGINREC; value: LPCSTR; param: INT): RETCODE; cdecl;
  dbsetlogintime:function (seconds: INT): RETCODE; cdecl;
  dbsetlpacket:	function (login: PLOGINREC; packet_size: USHORT): RETCODE; cdecl;
  dbsetnull:	function (dbproc: PDBPROCESS; bindtype, bindlen: INT; bindval: LPCBYTE): RETCODE; cdecl;
  dbsetopt:	function (dbproc: PDBPROCESS; option: INT; param: LPCSTR): RETCODE; cdecl;
  dbsettime:	function (seconds: INT): RETCODE; cdecl;
  dbsetuserdata:procedure (dbproc: PDBPROCESS; ptr: LPVOID); cdecl;
  dbsqlexec:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  dbsqlok:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  dbsqlsend:	function (dbproc: PDBPROCESS): RETCODE; cdecl;
  dbstrcpy:	function (dbproc: PDBPROCESS; start, numbytes: INT; dest: LPSTR): RETCODE; cdecl;
  dbstrlen:	function (dbproc: PDBPROCESS): INT; cdecl;
  dbtabbrowse:	function (dbproc: PDBPROCESS; tabnum: INT): BOOL; cdecl;
  dbtabcount:	function (dbproc: PDBPROCESS): INT; cdecl;
  dbtabname:	function (dbproc: PDBPROCESS; tabnum: INT): LPCSTR; cdecl;
  dbtabsource:	function (dbproc: PDBPROCESS; colnum: INT; tabnum: LPINT): LPCSTR; cdecl;
  dbtsnewlen:	function (dbproc: PDBPROCESS): INT; cdecl;
  dbtsnewval:	function (dbproc: PDBPROCESS): LPCDBBINARY; cdecl;
  dbtsput:	function (dbproc: PDBPROCESS; newts: LPCDBBINARY; newtslen, tabnum: INT; tabname: LPCSTR): RETCODE; cdecl;
  dbtxptr:	function (dbproc: PDBPROCESS; column: INT): LPCDBBINARY; cdecl;
  dbtxtimestamp:function (dbproc: PDBPROCESS; column: INT): LPCDBBINARY; cdecl;
  dbtxtsnewval:	function (dbproc: PDBPROCESS): LPCDBBINARY; cdecl;
  dbtxtsput:	function (dbproc: PDBPROCESS; newtxts: LPCDBBINARY; colnum: INT): RETCODE; cdecl;
  dbuse:	function (dbproc: PDBPROCESS; dbname: LPCSTR): RETCODE; cdecl;
  dbvarylen:	function (dbproc: PDBPROCESS; column: INT): BOOL; cdecl;
  dbwillconvert:function (srctype, desttype: INT): BOOL; cdecl;
//  dbwritepage:	function (dbproc: PDBPROCESS; LPCSTR, DBINT, DBINT, LPBYTE): RETCODE; cdecl;
  dbwritetext:	function (dbproc: PDBPROCESS; objname: LPCSTR;
  			textptr: LPCDBBINARY; textptrlen: DBTINYINT; timestamp: LPCDBBINARY;
                        log: BOOL; size: DBINT; text: LPCBYTE): RETCODE; cdecl;
  dbupdatetext:	function (dbproc: PDBPROCESS; dest_object: LPCSTR; dest_textptr: LPCDBBINARY;
  			dest_timestamp: LPCDBBINARY; update_type: INT; insert_offset, delete_length: DBINT;
  			src_object: LPCSTR; src_size: DBINT; src_text: LPCDBBINARY): RETCODE; cdecl;

  dberrhandle:  function (handler: TDBERRHANDLE_PROC): TDBERRHANDLE_PROC; cdecl;
  dbmsghandle:  function (handler: TDBMSGHANDLE_PROC): TDBMSGHANDLE_PROC; cdecl;
  dbprocerrhandle:function (dbproc: PDBPROCESS; handler: TDBERRHANDLE_PROC): TDBERRHANDLE_PROC; cdecl;
  dbprocmsghandle:function (dbproc: PDBPROCESS; handler: TDBMSGHANDLE_PROC): TDBMSGHANDLE_PROC; cdecl;

type
  ESDMssError = class(ESDEngineError)
  private
    FHProcess: PDBPROCESS;		// FDbProcess - pointer to DBPROCESS structure for communication with the SQLServer
    FSeverity: INT;
  public
    constructor Create(DbProc: PDBPROCESS; AErrorCode, ANativeError: TSDEResult;
      ASeverity: INT; const Msg: string; AErrorPos: LongInt);
    property HProcess: PDBPROCESS read FHProcess;
    property Severity: INT read FSeverity;
  end;

{ TIMssDatabase }
  TMssSrvInfo	= record
    DBProcPtr:		PDBPROCESS;	// pointer to DBPROCESS structure for communication with the SQLServer
    LoginRecPtr:	PLOGINREC;
    szServerName: 	PChar;
  end;
  PMssSrvInfo	= ^TMssSrvInfo;

  TIMssDatabase = class(TISrvDatabase)
  private
    FHandle: PSDHandleRec;

    FIsSingleConn: Boolean;		// Whether or not a database FHandle is used for opened TSDQuery
    FCurrDataSet: TSDDataSet;		// a dataset, which uses a database handle currently (when FIsSingleConn is True)
    FServerVersion: LongInt;		// HiWord = ServerMajor, LoWord = ServerMinor

    procedure Check;
    procedure AllocHandle;
    procedure FreeHandle;
    procedure HandleSetDefParams(AHandle: PDBPROCESS);
    procedure HandleSetTransIsolation(AHandle: PDBPROCESS; Value: TSDTransIsolation);
    procedure HandleReset(AHandle: PDBPROCESS);
    function GetDBProcPtr: PDBPROCESS;
    function GetLoginRecPtr: PLOGINREC;
    function GetServerName: string;
    procedure GetStmtResult(const Stmt: string; List: TStrings);
  protected
    function GetClientVersion: LongInt; override;
    function GetServerVersion: LongInt; override;
    function GetVersionString: string; override;

    procedure Commit; override;
    function FieldDataType(ExtDataType: Integer): TFieldType; override;
    function GetHandle: PSDHandleRec; override;
    procedure GetStoredProcNames(List: TStrings); override;
    procedure GetTableFieldNames(const TableName: string; List: TStrings); override;
    procedure GetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings); override;
    procedure Logon(const sRemoteDatabase, sUserName, sPassword: string); override;
    procedure Logoff(Force: Boolean); override;
    function NativeDataSize(FieldType: TFieldType): Word; override;
    function NativeDataType(FieldType: TFieldType): Integer; override;
    function RequiredCnvtFieldType(FieldType: TFieldType): Boolean; override;
    procedure Rollback; override;
    procedure SetDefaultParams; override;
    procedure SetHandle(AHandle: PSDHandleRec); override;
    procedure SetTransIsolation(Value: TSDTransIsolation); override;
    procedure StartTransaction; override;
    function SPDescriptionsAvailable: Boolean; override;
    property IsSingleConn: Boolean read FIsSingleConn;
  public
    constructor Create(ADatabase: TSDDatabase); override;
    destructor Destroy; override;
    function CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet; override;
    procedure ReleaseDBHandle(ADataSet: TSDDataSet; IsFetchAll: Boolean);
    property CurrDataSet: TSDDataSet read FCurrDataSet;
    property DBProcPtr: PDBPROCESS read GetDBProcPtr;
    property LoginRecPtr: PLOGINREC read GetLoginRecPtr;
    property ServerName: string read GetServerName;
  end;

{ TIMssDataSet }
  TIMssDataSet = class(TISrvDataSet)
  private
    FStmt,			// w/o bind data
    FBindStmt: string;		// with bind parameter data, it's used to check whether the statement was executed
    FRowsAffected: DBINT;	// number of rows affected by the last Transact-SQL statement (DML)
    FHandle: PDBPROCESS;
    FIsSingleConn: Boolean;	// True, if it's required to use a database handle always
    FConnected: Boolean;	// True, when Connect method was called
    FNextResults: Boolean;	// if one of multiple result sets processing
    FEndResults: Boolean;	// if all results have been processed

    procedure Check;
    procedure Connect;
    function CanReturnRows: Boolean;
    function CnvtDateTimeToSQLVarChar(Value: TDateTime): string;
    function CnvtDateTimeToSQLDateTime(Value: TDateTime): DBDATETIME;
    function CnvtDBDateTime2DateTimeRec(ADataType: TFieldType; Buffer: PChar; BufSize: Integer): TDateTimeRec;
    function CnvtFloatToSQLVarChar(Value: Double): string;
    function CreateDBProcess: PDBPROCESS;
    procedure DbExecHandle(AHandle: PDBPROCESS);
    procedure HandleCancel(AHandle: PDBPROCESS);
    procedure HandleCurReset(AHandle: PDBPROCESS);
    procedure HandleReset(AHandle: PDBPROCESS);
    procedure HandleSpInit(AHandle: PDBPROCESS; ARpcName: PChar);
    procedure HandleSpExec(AHandle: PDBPROCESS);
    function HandleSpResults(AHandle: PDBPROCESS): Boolean;
    function GetExecuted: Boolean;
    function GetSrvDatabase: TIMssDatabase;
    procedure InternalExecute;
    procedure InternalQBindParams;
    procedure InternalQExecute;
    procedure InternalSpBindParams;
    procedure InternalSpExecute;
    procedure InternalSpGetParams;
    procedure QGetFieldDescs(Descs: TSDFieldDescList);
    procedure SpGetFieldDescs(Descs: TSDFieldDescList);
    procedure FetchDataSize;
  protected
    procedure AcquireDBHandle;
    procedure ReleaseDBHandle;

    function BindBufferSize: Integer; override;
    procedure CloseResultSet; override;
    function CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean; override;
    procedure Disconnect(Force: Boolean); override;
    procedure Execute; override;
    function FetchNextRow: Boolean; override;
    procedure GetFieldDescs(Descs: TSDFieldDescList); override;
    function GetHandle: PSDCursor; override;
    procedure SetSelectBuffer; override;
    function ResultSetExists: Boolean; override;
    function ReadBlob(FieldNo: Integer; var BlobData: string): Longint; override;
	// Query methods
    procedure QBindParams; override;
    function QGetRowsAffected: Integer; override;
    procedure QPrepareSQL(Value: PChar); override;
    procedure QExecute; override;
	// StoredProc methods
    procedure SpBindParams; override;
    procedure SpCreateParamDesc; override;
    procedure SpPrepareProc; override;
    procedure SpExecute; override;
    procedure SpExecProc; override;
    procedure SpGetResults; override;
    function SpNextResultSet: Boolean; override;

    property SrvDatabase: TIMssDatabase read GetSrvDatabase;
  public
    constructor Create(ADataSet: TSDDataSet); override;
    destructor Destroy; override;
    procedure AllocBindBuffer; override;
    property Executed: Boolean read GetExecuted;
  end;

const
  DefSqlApiDLL	= 'NTWDBLIB.DLL';

{*******************************************************************************
		Load/Unload Sql-library
*******************************************************************************}
procedure LoadSqlLib;
procedure FreeSqlLib;


implementation

uses
  TypInfo;

const
  APP_CONNECT_MAX	= 100;	// max connections permits

  IsolLevelCmd: array[TSDTransIsolation] of string =
  	('SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED',
         'SET TRANSACTION ISOLATION LEVEL READ COMMITTED',
         'SET TRANSACTION ISOLATION LEVEL REPEATABLE READ'
        );

  OPT_DBTEXTLIMIT	= '2147483647';
  OPT_DBTEXTSIZE	= OPT_DBTEXTLIMIT;

  CRLF = #$0D#$0A;

  QuoteChar	= '"';	// for surroundings of the parameter's name, which can include, for example, spaces


const
  { Converting from TFieldType � SQL Data Type(SQLServer) - used in stored procedure bind }
  SrvNativeDataTypes: array[TFieldType] of Word = ( 0,	// ftUnknown
	// ftString, ftSmallint, ftInteger, ftWord, ftBoolean
	SQLCHAR, 	SQLINT2, SQLINT4, SQLINT2, SQLINT2,
	// ftFloat, ftCurrency, ftBCD, ftDate, ftTime
        SQLFLT8,	SQLFLT8,0, SQLDATETIME, SQLDATETIME,
        // ftDateTime, ftBytes, ftVarBytes, ftAutoInc, ftBlob
        SQLDATETIME, 	SQLBINARY, SQLBINARY,	0, 	SQLIMAGE,
        // ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle
        SQLTEXT,	0,	0,	0,	0,
        // ftTypedBinary, ftCursor
        0,	0
{$IFDEF SD_VCL4},
	// ftFixedChar, ftWideString, ftLargeint,
        0,	0,	0,
        // ftADT, ftArray, ftReference, ftDataSet
        0,	0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL5},
        // ftOraBlob, ftOraClob, ftVariant,
        0,	0,	0,
        // ftInterface, ftIDispatch, ftGuid
        0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL6},
        // ftTimeStamp, ftFMTBcd
        0,      0
{$ENDIF}
        );

type
  TMssErrorList = class(TList)
  private
    function GetError(Index: Integer): ESDMssError;
  public
    destructor Destroy; override;
    property Errors[Index: Integer]: ESDMssError read GetError;
  end;

{ TMssErrorList }
destructor TMssErrorList.Destroy;
var
  i: Integer;
begin
  for i:=0 to Count-1 do
    ESDMssError( Items[i] ).Destroy;

  inherited;
end;

function TMssErrorList.GetError(Index: Integer): ESDMssError;
begin
  Result := ESDMssError( Items[Index] );
end;

resourcestring
  SErrLibLoading 	= 'Error loading library ''%s''';
  SErrLibUnloading	= 'Error unloading library ''%s''';
  SErrLibInit		= 'Error initialization of DB-library';
  SErrFuncNotFound	= 'Function ''%s'' not found in MS SQLServer DB-library';
  SErrDBProcIsNull	= 'Unable to allocate DBPROCESS connection structure';

var
  DbErrorList: TMssErrorList;
  hSqlLibModule: THandle;
  SqlLibRefCount: Integer;
  SqlLibLock: TCriticalSection;
  dwLoadedDBLIB: LongInt;

(*******************************************************************************
			Load/Unload Sql-library
********************************************************************************)
procedure SetProcAddresses;
begin
  @abort_xact           := GetProcAddress(hSqlLibModule, 'abort_xact');  	ASSERT( @abort_xact       <>nil, Format(SErrFuncNotFound, ['abort_xact       ']) );
  @build_xact_string    := GetProcAddress(hSqlLibModule, 'build_xact_string');  ASSERT( @build_xact_string<>nil, Format(SErrFuncNotFound, ['build_xact_string']) );
  @close_commit         := GetProcAddress(hSqlLibModule, 'close_commit');  	ASSERT( @close_commit     <>nil, Format(SErrFuncNotFound, ['close_commit     ']) );
  @commit_xact          := GetProcAddress(hSqlLibModule, 'commit_xact');  	ASSERT( @commit_xact      <>nil, Format(SErrFuncNotFound, ['commit_xact      ']) );
  @open_commit          := GetProcAddress(hSqlLibModule, 'open_commit');  	ASSERT( @open_commit      <>nil, Format(SErrFuncNotFound, ['open_commit      ']) );
  @remove_xact          := GetProcAddress(hSqlLibModule, 'remove_xact');  	ASSERT( @remove_xact      <>nil, Format(SErrFuncNotFound, ['remove_xact      ']) );
//  @scan_xact            := GetProcAddress(hSqlLibModule, 'scan_xact');  	ASSERT( @scan_xact        <>nil, Format(SErrFuncNotFound, ['scan_xact        ']) );
  @start_xact           := GetProcAddress(hSqlLibModule, 'start_xact');  	ASSERT( @start_xact       <>nil, Format(SErrFuncNotFound, ['start_xact       ']) );
  @stat_xact            := GetProcAddress(hSqlLibModule, 'stat_xact');  	ASSERT( @stat_xact        <>nil, Format(SErrFuncNotFound, ['stat_xact        ']) );
  @bcp_batch            := GetProcAddress(hSqlLibModule, 'bcp_batch');  	ASSERT( @bcp_batch        <>nil, Format(SErrFuncNotFound, ['bcp_batch        ']) );
  @bcp_bind             := GetProcAddress(hSqlLibModule, 'bcp_bind');  		ASSERT( @bcp_bind         <>nil, Format(SErrFuncNotFound, ['bcp_bind         ']) );
  @bcp_colfmt           := GetProcAddress(hSqlLibModule, 'bcp_colfmt');  	ASSERT( @bcp_colfmt       <>nil, Format(SErrFuncNotFound, ['bcp_colfmt       ']) );
  @bcp_collen           := GetProcAddress(hSqlLibModule, 'bcp_collen');  	ASSERT( @bcp_collen       <>nil, Format(SErrFuncNotFound, ['bcp_collen       ']) );
  @bcp_colptr           := GetProcAddress(hSqlLibModule, 'bcp_colptr');  	ASSERT( @bcp_colptr       <>nil, Format(SErrFuncNotFound, ['bcp_colptr       ']) );
  @bcp_columns          := GetProcAddress(hSqlLibModule, 'bcp_columns');  	ASSERT( @bcp_columns      <>nil, Format(SErrFuncNotFound, ['bcp_columns      ']) );
  @bcp_control          := GetProcAddress(hSqlLibModule, 'bcp_control');  	ASSERT( @bcp_control      <>nil, Format(SErrFuncNotFound, ['bcp_control      ']) );
  @bcp_done             := GetProcAddress(hSqlLibModule, 'bcp_done');  		ASSERT( @bcp_done         <>nil, Format(SErrFuncNotFound, ['bcp_done         ']) );
  @bcp_exec             := GetProcAddress(hSqlLibModule, 'bcp_exec');  		ASSERT( @bcp_exec         <>nil, Format(SErrFuncNotFound, ['bcp_exec         ']) );
  @bcp_init             := GetProcAddress(hSqlLibModule, 'bcp_init');  		ASSERT( @bcp_init         <>nil, Format(SErrFuncNotFound, ['bcp_init         ']) );
  @bcp_moretext         := GetProcAddress(hSqlLibModule, 'bcp_moretext');  	ASSERT( @bcp_moretext     <>nil, Format(SErrFuncNotFound, ['bcp_moretext     ']) );
  @bcp_readfmt          := GetProcAddress(hSqlLibModule, 'bcp_readfmt');  	ASSERT( @bcp_readfmt      <>nil, Format(SErrFuncNotFound, ['bcp_readfmt      ']) );
  @bcp_sendrow          := GetProcAddress(hSqlLibModule, 'bcp_sendrow');  	ASSERT( @bcp_sendrow      <>nil, Format(SErrFuncNotFound, ['bcp_sendrow      ']) );
  @bcp_setl             := GetProcAddress(hSqlLibModule, 'bcp_setl');  		ASSERT( @bcp_setl         <>nil, Format(SErrFuncNotFound, ['bcp_setl         ']) );
  @bcp_writefmt         := GetProcAddress(hSqlLibModule, 'bcp_writefmt');  	ASSERT( @bcp_writefmt     <>nil, Format(SErrFuncNotFound, ['bcp_writefmt     ']) );
  @dbadata              := GetProcAddress(hSqlLibModule, 'dbadata');  		ASSERT( @dbadata          <>nil, Format(SErrFuncNotFound, ['dbadata          ']) );
  @dbadlen              := GetProcAddress(hSqlLibModule, 'dbadlen');  		ASSERT( @dbadlen          <>nil, Format(SErrFuncNotFound, ['dbadlen          ']) );
  @dbaltbind            := GetProcAddress(hSqlLibModule, 'dbaltbind');  	ASSERT( @dbaltbind        <>nil, Format(SErrFuncNotFound, ['dbaltbind        ']) );
  @dbaltcolid           := GetProcAddress(hSqlLibModule, 'dbaltcolid');  	ASSERT( @dbaltcolid       <>nil, Format(SErrFuncNotFound, ['dbaltcolid       ']) );
  @dbaltlen             := GetProcAddress(hSqlLibModule, 'dbaltlen');  		ASSERT( @dbaltlen         <>nil, Format(SErrFuncNotFound, ['dbaltlen         ']) );
  @dbaltop              := GetProcAddress(hSqlLibModule, 'dbaltop');  		ASSERT( @dbaltop          <>nil, Format(SErrFuncNotFound, ['dbaltop          ']) );
  @dbalttype            := GetProcAddress(hSqlLibModule, 'dbalttype');  	ASSERT( @dbalttype        <>nil, Format(SErrFuncNotFound, ['dbalttype        ']) );
  @dbaltutype           := GetProcAddress(hSqlLibModule, 'dbaltutype');  	ASSERT( @dbaltutype       <>nil, Format(SErrFuncNotFound, ['dbaltutype       ']) );
  @dbanullbind          := GetProcAddress(hSqlLibModule, 'dbanullbind');  	ASSERT( @dbanullbind      <>nil, Format(SErrFuncNotFound, ['dbanullbind      ']) );
  @dbbind               := GetProcAddress(hSqlLibModule, 'dbbind');  		ASSERT( @dbbind           <>nil, Format(SErrFuncNotFound, ['dbbind           ']) );
  @dbbylist             := GetProcAddress(hSqlLibModule, 'dbbylist');  		ASSERT( @dbbylist         <>nil, Format(SErrFuncNotFound, ['dbbylist         ']) );
  @dbcancel             := GetProcAddress(hSqlLibModule, 'dbcancel');  		ASSERT( @dbcancel         <>nil, Format(SErrFuncNotFound, ['dbcancel         ']) );
  @dbcanquery           := GetProcAddress(hSqlLibModule, 'dbcanquery');  	ASSERT( @dbcanquery       <>nil, Format(SErrFuncNotFound, ['dbcanquery       ']) );
  @dbchange             := GetProcAddress(hSqlLibModule, 'dbchange');  		ASSERT( @dbchange         <>nil, Format(SErrFuncNotFound, ['dbchange         ']) );
  @dbclose              := GetProcAddress(hSqlLibModule, 'dbclose');  		ASSERT( @dbclose          <>nil, Format(SErrFuncNotFound, ['dbclose          ']) );
  @dbclrbuf             := GetProcAddress(hSqlLibModule, 'dbclrbuf');  		ASSERT( @dbclrbuf         <>nil, Format(SErrFuncNotFound, ['dbclrbuf         ']) );
  @dbclropt             := GetProcAddress(hSqlLibModule, 'dbclropt');  		ASSERT( @dbclropt         <>nil, Format(SErrFuncNotFound, ['dbclropt         ']) );
  @dbcmd                := GetProcAddress(hSqlLibModule, 'dbcmd');  		ASSERT( @dbcmd            <>nil, Format(SErrFuncNotFound, ['dbcmd            ']) );
  @dbcmdrow             := GetProcAddress(hSqlLibModule, 'dbcmdrow');  		ASSERT( @dbcmdrow         <>nil, Format(SErrFuncNotFound, ['dbcmdrow         ']) );
  @dbcolbrowse          := GetProcAddress(hSqlLibModule, 'dbcolbrowse');  	ASSERT( @dbcolbrowse      <>nil, Format(SErrFuncNotFound, ['dbcolbrowse      ']) );
  @dbcolinfo            := GetProcAddress(hSqlLibModule, 'dbcolinfo');  	ASSERT( @dbcolinfo        <>nil, Format(SErrFuncNotFound, ['dbcolinfo        ']) );
  @dbcollen             := GetProcAddress(hSqlLibModule, 'dbcollen');  		ASSERT( @dbcollen         <>nil, Format(SErrFuncNotFound, ['dbcollen         ']) );
  @dbcolname            := GetProcAddress(hSqlLibModule, 'dbcolname');  	ASSERT( @dbcolname        <>nil, Format(SErrFuncNotFound, ['dbcolname        ']) );
  @dbcolsource          := GetProcAddress(hSqlLibModule, 'dbcolsource');  	ASSERT( @dbcolsource      <>nil, Format(SErrFuncNotFound, ['dbcolsource      ']) );
  @dbcoltype            := GetProcAddress(hSqlLibModule, 'dbcoltype');  	ASSERT( @dbcoltype        <>nil, Format(SErrFuncNotFound, ['dbcoltype        ']) );
  @dbcolutype           := GetProcAddress(hSqlLibModule, 'dbcolutype');  	ASSERT( @dbcolutype       <>nil, Format(SErrFuncNotFound, ['dbcolutype       ']) );
  @dbconvert            := GetProcAddress(hSqlLibModule, 'dbconvert');  	ASSERT( @dbconvert        <>nil, Format(SErrFuncNotFound, ['dbconvert        ']) );
  @dbcount              := GetProcAddress(hSqlLibModule, 'dbcount');  		ASSERT( @dbcount          <>nil, Format(SErrFuncNotFound, ['dbcount          ']) );
  @dbcurcmd             := GetProcAddress(hSqlLibModule, 'dbcurcmd');  		ASSERT( @dbcurcmd         <>nil, Format(SErrFuncNotFound, ['dbcurcmd         ']) );
  @dbcurrow             := GetProcAddress(hSqlLibModule, 'dbcurrow');  		ASSERT( @dbcurrow         <>nil, Format(SErrFuncNotFound, ['dbcurrow         ']) );
  @dbcursor             := GetProcAddress(hSqlLibModule, 'dbcursor');  		ASSERT( @dbcursor         <>nil, Format(SErrFuncNotFound, ['dbcursor         ']) );
  @dbcursorbind         := GetProcAddress(hSqlLibModule, 'dbcursorbind');  	ASSERT( @dbcursorbind     <>nil, Format(SErrFuncNotFound, ['dbcursorbind     ']) );
  @dbcursorclose        := GetProcAddress(hSqlLibModule, 'dbcursorclose');  	ASSERT( @dbcursorclose    <>nil, Format(SErrFuncNotFound, ['dbcursorclose    ']) );
  @dbcursorcolinfo      := GetProcAddress(hSqlLibModule, 'dbcursorcolinfo');  	ASSERT( @dbcursorcolinfo  <>nil, Format(SErrFuncNotFound, ['dbcursorcolinfo  ']) );
  @dbcursorfetch        := GetProcAddress(hSqlLibModule, 'dbcursorfetch');  	ASSERT( @dbcursorfetch    <>nil, Format(SErrFuncNotFound, ['dbcursorfetch    ']) );
  @dbcursorfetchex      := GetProcAddress(hSqlLibModule, 'dbcursorfetchex');  	ASSERT( @dbcursorfetchex  <>nil, Format(SErrFuncNotFound, ['dbcursorfetchex  ']) );
  @dbcursorinfo         := GetProcAddress(hSqlLibModule, 'dbcursorinfo');  	ASSERT( @dbcursorinfo     <>nil, Format(SErrFuncNotFound, ['dbcursorinfo     ']) );
  @dbcursorinfoex       := GetProcAddress(hSqlLibModule, 'dbcursorinfoex');  	ASSERT( @dbcursorinfoex   <>nil, Format(SErrFuncNotFound, ['dbcursorinfoex   ']) );
  @dbcursoropen         := GetProcAddress(hSqlLibModule, 'dbcursoropen');  	ASSERT( @dbcursoropen     <>nil, Format(SErrFuncNotFound, ['dbcursoropen     ']) );
  @dbdata               := GetProcAddress(hSqlLibModule, 'dbdata');  		ASSERT( @dbdata           <>nil, Format(SErrFuncNotFound, ['dbdata           ']) );
  @dbdataready          := GetProcAddress(hSqlLibModule, 'dbdataready');  	ASSERT( @dbdataready      <>nil, Format(SErrFuncNotFound, ['dbdataready      ']) );
  @dbdatecrack          := GetProcAddress(hSqlLibModule, 'dbdatecrack');  	ASSERT( @dbdatecrack      <>nil, Format(SErrFuncNotFound, ['dbdatecrack      ']) );
  @dbdatlen             := GetProcAddress(hSqlLibModule, 'dbdatlen');  		ASSERT( @dbdatlen         <>nil, Format(SErrFuncNotFound, ['dbdatlen         ']) );
  @dbdead               := GetProcAddress(hSqlLibModule, 'dbdead');  		ASSERT( @dbdead           <>nil, Format(SErrFuncNotFound, ['dbdead           ']) );
  @dbexit               := GetProcAddress(hSqlLibModule, 'dbexit');  		ASSERT( @dbexit           <>nil, Format(SErrFuncNotFound, ['dbexit           ']) );
  @dbenlisttrans        := GetProcAddress(hSqlLibModule, 'dbenlisttrans');  	ASSERT( @dbenlisttrans    <>nil, Format(SErrFuncNotFound, ['dbenlisttrans    ']) );
  @dbenlistxatrans      := GetProcAddress(hSqlLibModule, 'dbenlistxatrans');  	ASSERT( @dbenlistxatrans  <>nil, Format(SErrFuncNotFound, ['dbenlistxatrans  ']) );
  @dbfirstrow           := GetProcAddress(hSqlLibModule, 'dbfirstrow');  	ASSERT( @dbfirstrow       <>nil, Format(SErrFuncNotFound, ['dbfirstrow       ']) );
  @dbfreebuf            := GetProcAddress(hSqlLibModule, 'dbfreebuf');  	ASSERT( @dbfreebuf        <>nil, Format(SErrFuncNotFound, ['dbfreebuf        ']) );
  @dbfreelogin          := GetProcAddress(hSqlLibModule, 'dbfreelogin');  	ASSERT( @dbfreelogin      <>nil, Format(SErrFuncNotFound, ['dbfreelogin      ']) );
  @dbfreequal           := GetProcAddress(hSqlLibModule, 'dbfreequal');  	ASSERT( @dbfreequal       <>nil, Format(SErrFuncNotFound, ['dbfreequal       ']) );
  @dbgetchar            := GetProcAddress(hSqlLibModule, 'dbgetchar');  	ASSERT( @dbgetchar        <>nil, Format(SErrFuncNotFound, ['dbgetchar        ']) );
  @dbgetmaxprocs        := GetProcAddress(hSqlLibModule, 'dbgetmaxprocs');  	ASSERT( @dbgetmaxprocs    <>nil, Format(SErrFuncNotFound, ['dbgetmaxprocs    ']) );
  @dbgetoff             := GetProcAddress(hSqlLibModule, 'dbgetoff');  		ASSERT( @dbgetoff         <>nil, Format(SErrFuncNotFound, ['dbgetoff         ']) );
  @dbgetpacket          := GetProcAddress(hSqlLibModule, 'dbgetpacket');  	ASSERT( @dbgetpacket      <>nil, Format(SErrFuncNotFound, ['dbgetpacket      ']) );
  @dbgetrow             := GetProcAddress(hSqlLibModule, 'dbgetrow');  		ASSERT( @dbgetrow         <>nil, Format(SErrFuncNotFound, ['dbgetrow         ']) );
  @dbgettime            := GetProcAddress(hSqlLibModule, 'dbgettime');  	ASSERT( @dbgettime        <>nil, Format(SErrFuncNotFound, ['dbgettime        ']) );
  @dbgetuserdata        := GetProcAddress(hSqlLibModule, 'dbgetuserdata');  	ASSERT( @dbgetuserdata    <>nil, Format(SErrFuncNotFound, ['dbgetuserdata    ']) );
  @dbhasretstat         := GetProcAddress(hSqlLibModule, 'dbhasretstat');  	ASSERT( @dbhasretstat     <>nil, Format(SErrFuncNotFound, ['dbhasretstat     ']) );
  @dbinit               := GetProcAddress(hSqlLibModule, 'dbinit');  		ASSERT( @dbinit           <>nil, Format(SErrFuncNotFound, ['dbinit           ']) );
  @dbisavail            := GetProcAddress(hSqlLibModule, 'dbisavail');  	ASSERT( @dbisavail        <>nil, Format(SErrFuncNotFound, ['dbisavail        ']) );
  @dbiscount            := GetProcAddress(hSqlLibModule, 'dbiscount');  	ASSERT( @dbiscount        <>nil, Format(SErrFuncNotFound, ['dbiscount        ']) );
  @dbisopt              := GetProcAddress(hSqlLibModule, 'dbisopt');  		ASSERT( @dbisopt          <>nil, Format(SErrFuncNotFound, ['dbisopt          ']) );
  @dblastrow            := GetProcAddress(hSqlLibModule, 'dblastrow');  	ASSERT( @dblastrow        <>nil, Format(SErrFuncNotFound, ['dblastrow        ']) );
  @dblogin              := GetProcAddress(hSqlLibModule, 'dblogin');  		ASSERT( @dblogin          <>nil, Format(SErrFuncNotFound, ['dblogin          ']) );
  @dbmorecmds           := GetProcAddress(hSqlLibModule, 'dbmorecmds');  	ASSERT( @dbmorecmds       <>nil, Format(SErrFuncNotFound, ['dbmorecmds       ']) );
  @dbmoretext           := GetProcAddress(hSqlLibModule, 'dbmoretext');  	ASSERT( @dbmoretext       <>nil, Format(SErrFuncNotFound, ['dbmoretext       ']) );
  @dbname               := GetProcAddress(hSqlLibModule, 'dbname');  		ASSERT( @dbname           <>nil, Format(SErrFuncNotFound, ['dbname           ']) );
  @dbnextrow            := GetProcAddress(hSqlLibModule, 'dbnextrow');  	ASSERT( @dbnextrow        <>nil, Format(SErrFuncNotFound, ['dbnextrow        ']) );
  @dbnullbind           := GetProcAddress(hSqlLibModule, 'dbnullbind');  	ASSERT( @dbnullbind       <>nil, Format(SErrFuncNotFound, ['dbnullbind       ']) );
  @dbnumalts            := GetProcAddress(hSqlLibModule, 'dbnumalts');  	ASSERT( @dbnumalts        <>nil, Format(SErrFuncNotFound, ['dbnumalts        ']) );
  @dbnumcols            := GetProcAddress(hSqlLibModule, 'dbnumcols');  	ASSERT( @dbnumcols        <>nil, Format(SErrFuncNotFound, ['dbnumcols        ']) );
  @dbnumcompute         := GetProcAddress(hSqlLibModule, 'dbnumcompute');  	ASSERT( @dbnumcompute     <>nil, Format(SErrFuncNotFound, ['dbnumcompute     ']) );
  @dbnumorders          := GetProcAddress(hSqlLibModule, 'dbnumorders');  	ASSERT( @dbnumorders      <>nil, Format(SErrFuncNotFound, ['dbnumorders      ']) );
  @dbnumrets            := GetProcAddress(hSqlLibModule, 'dbnumrets');  	ASSERT( @dbnumrets        <>nil, Format(SErrFuncNotFound, ['dbnumrets        ']) );
  @dbopen               := GetProcAddress(hSqlLibModule, 'dbopen');  		ASSERT( @dbopen           <>nil, Format(SErrFuncNotFound, ['dbopen           ']) );
  @dbordercol           := GetProcAddress(hSqlLibModule, 'dbordercol');  	ASSERT( @dbordercol       <>nil, Format(SErrFuncNotFound, ['dbordercol       ']) );
  @dbprocinfo           := GetProcAddress(hSqlLibModule, 'dbprocinfo');  	ASSERT( @dbprocinfo       <>nil, Format(SErrFuncNotFound, ['dbprocinfo       ']) );
  @dbprhead             := GetProcAddress(hSqlLibModule, 'dbprhead');  		ASSERT( @dbprhead         <>nil, Format(SErrFuncNotFound, ['dbprhead         ']) );
  @dbprrow              := GetProcAddress(hSqlLibModule, 'dbprrow');  		ASSERT( @dbprrow          <>nil, Format(SErrFuncNotFound, ['dbprrow          ']) );
  @dbprtype             := GetProcAddress(hSqlLibModule, 'dbprtype');  		ASSERT( @dbprtype         <>nil, Format(SErrFuncNotFound, ['dbprtype         ']) );
  @dbqual               := GetProcAddress(hSqlLibModule, 'dbqual');  		ASSERT( @dbqual           <>nil, Format(SErrFuncNotFound, ['dbqual           ']) );
  @dbreadtext           := GetProcAddress(hSqlLibModule, 'dbreadtext');  	ASSERT( @dbreadtext       <>nil, Format(SErrFuncNotFound, ['dbreadtext       ']) );
  @dbresults            := GetProcAddress(hSqlLibModule, 'dbresults');  	ASSERT( @dbresults        <>nil, Format(SErrFuncNotFound, ['dbresults        ']) );
  @dbretdata            := GetProcAddress(hSqlLibModule, 'dbretdata');  	ASSERT( @dbretdata        <>nil, Format(SErrFuncNotFound, ['dbretdata        ']) );
  @dbretlen             := GetProcAddress(hSqlLibModule, 'dbretlen');  		ASSERT( @dbretlen         <>nil, Format(SErrFuncNotFound, ['dbretlen         ']) );
  @dbretname            := GetProcAddress(hSqlLibModule, 'dbretname');  	ASSERT( @dbretname        <>nil, Format(SErrFuncNotFound, ['dbretname        ']) );
  @dbretstatus          := GetProcAddress(hSqlLibModule, 'dbretstatus');  	ASSERT( @dbretstatus      <>nil, Format(SErrFuncNotFound, ['dbretstatus      ']) );
  @dbrettype            := GetProcAddress(hSqlLibModule, 'dbrettype');  	ASSERT( @dbrettype        <>nil, Format(SErrFuncNotFound, ['dbrettype        ']) );
  @dbrows               := GetProcAddress(hSqlLibModule, 'dbrows');  		ASSERT( @dbrows           <>nil, Format(SErrFuncNotFound, ['dbrows           ']) );
  @dbrowtype            := GetProcAddress(hSqlLibModule, 'dbrowtype');  	ASSERT( @dbrowtype        <>nil, Format(SErrFuncNotFound, ['dbrowtype        ']) );
  @dbrpcinit            := GetProcAddress(hSqlLibModule, 'dbrpcinit');  	ASSERT( @dbrpcinit        <>nil, Format(SErrFuncNotFound, ['dbrpcinit        ']) );
  @dbrpcparam           := GetProcAddress(hSqlLibModule, 'dbrpcparam');  	ASSERT( @dbrpcparam       <>nil, Format(SErrFuncNotFound, ['dbrpcparam       ']) );
  @dbrpcsend            := GetProcAddress(hSqlLibModule, 'dbrpcsend');  	ASSERT( @dbrpcsend        <>nil, Format(SErrFuncNotFound, ['dbrpcsend        ']) );
  @dbrpcexec            := GetProcAddress(hSqlLibModule, 'dbrpcexec');  	ASSERT( @dbrpcexec        <>nil, Format(SErrFuncNotFound, ['dbrpcexec        ']) );
  @dbserverenum         := GetProcAddress(hSqlLibModule, 'dbserverenum');  	ASSERT( @dbserverenum     <>nil, Format(SErrFuncNotFound, ['dbserverenum     ']) );
  @dbsetavail           := GetProcAddress(hSqlLibModule, 'dbsetavail');  	ASSERT( @dbsetavail       <>nil, Format(SErrFuncNotFound, ['dbsetavail       ']) );
  @dbsetmaxprocs        := GetProcAddress(hSqlLibModule, 'dbsetmaxprocs');  	ASSERT( @dbsetmaxprocs    <>nil, Format(SErrFuncNotFound, ['dbsetmaxprocs    ']) );
  @dbsetlname           := GetProcAddress(hSqlLibModule, 'dbsetlname');  	ASSERT( @dbsetlname       <>nil, Format(SErrFuncNotFound, ['dbsetlname       ']) );
  @dbsetlogintime       := GetProcAddress(hSqlLibModule, 'dbsetlogintime');  	ASSERT( @dbsetlogintime   <>nil, Format(SErrFuncNotFound, ['dbsetlogintime   ']) );
  @dbsetlpacket         := GetProcAddress(hSqlLibModule, 'dbsetlpacket');  	ASSERT( @dbsetlpacket     <>nil, Format(SErrFuncNotFound, ['dbsetlpacket     ']) );
  @dbsetnull            := GetProcAddress(hSqlLibModule, 'dbsetnull');  	ASSERT( @dbsetnull        <>nil, Format(SErrFuncNotFound, ['dbsetnull        ']) );
  @dbsetopt             := GetProcAddress(hSqlLibModule, 'dbsetopt');  		ASSERT( @dbsetopt         <>nil, Format(SErrFuncNotFound, ['dbsetopt         ']) );
  @dbsettime            := GetProcAddress(hSqlLibModule, 'dbsettime');  	ASSERT( @dbsettime        <>nil, Format(SErrFuncNotFound, ['dbsettime        ']) );
  @dbsetuserdata        := GetProcAddress(hSqlLibModule, 'dbsetuserdata');  	ASSERT( @dbsetuserdata    <>nil, Format(SErrFuncNotFound, ['dbsetuserdata    ']) );
  @dbsqlexec            := GetProcAddress(hSqlLibModule, 'dbsqlexec');  	ASSERT( @dbsqlexec        <>nil, Format(SErrFuncNotFound, ['dbsqlexec        ']) );
  @dbsqlok              := GetProcAddress(hSqlLibModule, 'dbsqlok');  		ASSERT( @dbsqlok          <>nil, Format(SErrFuncNotFound, ['dbsqlok          ']) );
  @dbsqlsend            := GetProcAddress(hSqlLibModule, 'dbsqlsend');  	ASSERT( @dbsqlsend        <>nil, Format(SErrFuncNotFound, ['dbsqlsend        ']) );
  @dbstrcpy             := GetProcAddress(hSqlLibModule, 'dbstrcpy');  		ASSERT( @dbstrcpy         <>nil, Format(SErrFuncNotFound, ['dbstrcpy         ']) );
  @dbstrlen             := GetProcAddress(hSqlLibModule, 'dbstrlen');  		ASSERT( @dbstrlen         <>nil, Format(SErrFuncNotFound, ['dbstrlen         ']) );
  @dbtabbrowse          := GetProcAddress(hSqlLibModule, 'dbtabbrowse');  	ASSERT( @dbtabbrowse      <>nil, Format(SErrFuncNotFound, ['dbtabbrowse      ']) );
  @dbtabcount           := GetProcAddress(hSqlLibModule, 'dbtabcount');  	ASSERT( @dbtabcount       <>nil, Format(SErrFuncNotFound, ['dbtabcount       ']) );
  @dbtabname            := GetProcAddress(hSqlLibModule, 'dbtabname');  	ASSERT( @dbtabname        <>nil, Format(SErrFuncNotFound, ['dbtabname        ']) );
  @dbtabsource          := GetProcAddress(hSqlLibModule, 'dbtabsource');  	ASSERT( @dbtabsource      <>nil, Format(SErrFuncNotFound, ['dbtabsource      ']) );
  @dbtsnewlen           := GetProcAddress(hSqlLibModule, 'dbtsnewlen');  	ASSERT( @dbtsnewlen       <>nil, Format(SErrFuncNotFound, ['dbtsnewlen       ']) );
  @dbtsnewval           := GetProcAddress(hSqlLibModule, 'dbtsnewval');  	ASSERT( @dbtsnewval       <>nil, Format(SErrFuncNotFound, ['dbtsnewval       ']) );
  @dbtsput              := GetProcAddress(hSqlLibModule, 'dbtsput');  		ASSERT( @dbtsput          <>nil, Format(SErrFuncNotFound, ['dbtsput          ']) );
  @dbtxptr              := GetProcAddress(hSqlLibModule, 'dbtxptr');  		ASSERT( @dbtxptr          <>nil, Format(SErrFuncNotFound, ['dbtxptr          ']) );
  @dbtxtimestamp        := GetProcAddress(hSqlLibModule, 'dbtxtimestamp');  	ASSERT( @dbtxtimestamp    <>nil, Format(SErrFuncNotFound, ['dbtxtimestamp    ']) );
  @dbtxtsnewval         := GetProcAddress(hSqlLibModule, 'dbtxtsnewval');  	ASSERT( @dbtxtsnewval     <>nil, Format(SErrFuncNotFound, ['dbtxtsnewval     ']) );
  @dbtxtsput            := GetProcAddress(hSqlLibModule, 'dbtxtsput');  	ASSERT( @dbtxtsput        <>nil, Format(SErrFuncNotFound, ['dbtxtsput        ']) );
  @dbuse                := GetProcAddress(hSqlLibModule, 'dbuse');  		ASSERT( @dbuse            <>nil, Format(SErrFuncNotFound, ['dbuse            ']) );
  @dbvarylen            := GetProcAddress(hSqlLibModule, 'dbvarylen');  	ASSERT( @dbvarylen        <>nil, Format(SErrFuncNotFound, ['dbvarylen        ']) );
  @dbwillconvert        := GetProcAddress(hSqlLibModule, 'dbwillconvert');  	ASSERT( @dbwillconvert    <>nil, Format(SErrFuncNotFound, ['dbwillconvert    ']) );
  @dbwritetext          := GetProcAddress(hSqlLibModule, 'dbwritetext');  	ASSERT( @dbwritetext      <>nil, Format(SErrFuncNotFound, ['dbwritetext      ']) );
  @dbupdatetext         := GetProcAddress(hSqlLibModule, 'dbupdatetext');  	ASSERT( @dbupdatetext     <>nil, Format(SErrFuncNotFound, ['dbupdatetext     ']) );

  @dberrhandle         := GetProcAddress(hSqlLibModule, 'dberrhandle');  	ASSERT( @dberrhandle     <>nil, Format(SErrFuncNotFound, ['dberrhandle     ']) );
  @dbmsghandle         := GetProcAddress(hSqlLibModule, 'dbmsghandle');  	ASSERT( @dbmsghandle     <>nil, Format(SErrFuncNotFound, ['dbmsghandle     ']) );
  @dbprocerrhandle     := GetProcAddress(hSqlLibModule, 'dbprocerrhandle');  	ASSERT( @dbprocerrhandle <>nil, Format(SErrFuncNotFound, ['dbprocerrhandle     ']) );
  @dbprocmsghandle     := GetProcAddress(hSqlLibModule, 'dbprocmsghandle');  	ASSERT( @dbprocmsghandle <>nil, Format(SErrFuncNotFound, ['dbprocmsghandle     ']) );
end;

procedure ResetProcAddresses;
begin
  @abort_xact           := nil;
  @build_xact_string    := nil;
  @close_commit         := nil;
  @commit_xact          := nil;
  @open_commit          := nil;
  @remove_xact          := nil;
  @scan_xact            := nil;
  @start_xact           := nil;
  @stat_xact            := nil;
  @bcp_batch            := nil;
  @bcp_bind             := nil;
  @bcp_colfmt           := nil;
  @bcp_collen           := nil;
  @bcp_colptr           := nil;
  @bcp_columns          := nil;
  @bcp_control          := nil;
  @bcp_done             := nil;
  @bcp_exec             := nil;
  @bcp_init             := nil;
  @bcp_moretext         := nil;
  @bcp_readfmt          := nil;
  @bcp_sendrow          := nil;
  @bcp_setl             := nil;
  @bcp_writefmt         := nil;
  @dbadata              := nil;
  @dbadlen              := nil;
  @dbaltbind            := nil;
  @dbaltcolid           := nil;
  @dbaltlen             := nil;
  @dbaltop              := nil;
  @dbalttype            := nil;
  @dbaltutype           := nil;
  @dbanullbind          := nil;
  @dbbind               := nil;
  @dbbylist             := nil;
  @dbcancel             := nil;
  @dbcanquery           := nil;
  @dbchange             := nil;
  @dbclose              := nil;
  @dbclrbuf             := nil;
  @dbclropt             := nil;
  @dbcmd                := nil;
  @dbcmdrow             := nil;
  @dbcolbrowse          := nil;
  @dbcolinfo            := nil;
  @dbcollen             := nil;
  @dbcolname            := nil;
  @dbcolsource          := nil;
  @dbcoltype            := nil;
  @dbcolutype           := nil;
  @dbconvert            := nil;
  @dbcount              := nil;
  @dbcurcmd             := nil;
  @dbcurrow             := nil;
  @dbcursor             := nil;
  @dbcursorbind         := nil;
  @dbcursorclose        := nil;
  @dbcursorcolinfo      := nil;
  @dbcursorfetch        := nil;
  @dbcursorfetchex      := nil;
  @dbcursorinfo         := nil;
  @dbcursorinfoex       := nil;
  @dbcursoropen         := nil;
  @dbdata               := nil;
  @dbdataready          := nil;
  @dbdatecrack          := nil;
  @dbdatlen             := nil;
  @dbdead               := nil;
  @dbexit               := nil;
  @dbenlisttrans        := nil;
  @dbenlistxatrans      := nil;
  @dbfirstrow           := nil;
  @dbfreebuf            := nil;
  @dbfreelogin          := nil;
  @dbfreequal           := nil;
  @dbgetchar            := nil;
  @dbgetmaxprocs        := nil;
  @dbgetoff             := nil;
  @dbgetpacket          := nil;
  @dbgetrow             := nil;
  @dbgettime            := nil;
  @dbgetuserdata        := nil;
  @dbhasretstat         := nil;
  @dbinit               := nil;
  @dbisavail            := nil;
  @dbiscount            := nil;
  @dbisopt              := nil;
  @dblastrow            := nil;
  @dblogin              := nil;
  @dbmorecmds           := nil;
  @dbmoretext           := nil;
  @dbname               := nil;
  @dbnextrow            := nil;
  @dbnullbind           := nil;
  @dbnumalts            := nil;
  @dbnumcols            := nil;
  @dbnumcompute         := nil;
  @dbnumorders          := nil;
  @dbnumrets            := nil;
  @dbopen               := nil;
  @dbordercol           := nil;
  @dbprocinfo           := nil;
  @dbprhead             := nil;
  @dbprrow              := nil;
  @dbprtype             := nil;
  @dbqual               := nil;
  @dbreadtext           := nil;
  @dbresults            := nil;
  @dbretdata            := nil;
  @dbretlen             := nil;
  @dbretname            := nil;
  @dbretstatus          := nil;
  @dbrettype            := nil;
  @dbrows               := nil;
  @dbrowtype            := nil;
  @dbrpcinit            := nil;
  @dbrpcparam           := nil;
  @dbrpcsend            := nil;
  @dbrpcexec            := nil;
  @dbserverenum         := nil;
  @dbsetavail           := nil;
  @dbsetmaxprocs        := nil;
  @dbsetlname           := nil;
  @dbsetlogintime       := nil;
  @dbsetlpacket         := nil;
  @dbsetnull            := nil;
  @dbsetopt             := nil;
  @dbsettime            := nil;
  @dbsetuserdata        := nil;
  @dbsqlexec            := nil;
  @dbsqlok              := nil;
  @dbsqlsend            := nil;
  @dbstrcpy             := nil;
  @dbstrlen             := nil;
  @dbtabbrowse          := nil;
  @dbtabcount           := nil;
  @dbtabname            := nil;
  @dbtabsource          := nil;
  @dbtsnewlen           := nil;
  @dbtsnewval           := nil;
  @dbtsput              := nil;
  @dbtxptr              := nil;
  @dbtxtimestamp        := nil;
  @dbtxtsnewval         := nil;
  @dbtxtsput            := nil;
  @dbuse                := nil;
  @dbvarylen            := nil;
  @dbwillconvert        := nil;
  @dbwritetext          := nil;
  @dbupdatetext         := nil;

  @dberrhandle          := nil;
  @dbmsghandle          := nil;
  @dbprocerrhandle      := nil;
  @dbprocmsghandle      := nil;
end;

{ ESDMssError }
constructor ESDMssError.Create(DbProc: PDBPROCESS; AErrorCode, ANativeError: TSDEResult;
  ASeverity: INT; const Msg: string; AErrorPos: LongInt);
begin
  inherited Create(AErrorCode, ANativeError, Msg, AErrorPos);

  FHProcess	:= DbProc;
  FSeverity	:= ASeverity;
end;


procedure SaveError(dbproc: PDBPROCESS; errno, severity: INT; errstr: string; line: DBUSMALLINT);
var
  E: ESDMssError;
  ErrPos: LongInt;
begin
  if DbErrorList.Count = 0 then begin
    ErrPos := -1;
    if line > 0 then
      ErrPos := line;
    E := ESDMssError.Create(dbproc, errno, errno, severity, errstr, ErrPos);
    DbErrorList.Add( E );
  end;
end;

procedure ClearErrors;
var
  i: Integer;
begin
  for i:=DbErrorList.Count-1 downto 0 do begin
    DbErrorList.Errors[i].Free;
    DbErrorList.Delete(i);
  end;
end;

function ErrHandler(dbproc: PDBPROCESS; severity, dberr, oserr: INT; dberrstr, oserrstr: PChar): INT; cdecl;
var
  sStr: string;
begin
  sStr := Format('DB-Library error %d: %s'#$0A#$0D, [dberr, dberrstr]);

  if (severity = EXCOMM) and ((oserr <> DBNOERR) or (oserrstr <> nil)) then
    sStr := sStr + Format('Net-Lib error %d:  %s'#$0A#$0D, [oserr, StrPas(oserrstr)]);

  if oserr <> DBNOERR then
    sStr := sStr + Format('Operating-system error: %s'#$0A#$0D, [StrPas(oserrstr)]);

  Result := INT_CANCEL;
    // If the query returns nothing (10086(SQLCRSFROWN) - row fetched outside valid range)
  if dberr = SQLCRSFROWN then
    Exit;
  if severity > EXINFO then
    SaveError(dbproc, dberr, severity, sStr, 0);
end;

function MsgHandler(dbproc: PDBPROCESS; msgno: DBINT; msgstate, severity: INT;
                           msgtext, srvname, procname: PChar; line: DBUSMALLINT): INT; cdecl;
var
  sStr: string;
begin
  Result := 0;
  sStr := Format('SQL Server message %d:', [msgno]);
  if msgtext <> '' then
    sStr := sStr + Format(' %s', [msgtext]);
  if line <> 0 then
    sStr := sStr + Format('(line %d)', [line]);

    	// invalid RowNum(0): end of result set
  if msgno = 16902 then
    Exit;
  if severity > EXINFO then
    SaveError(dbproc, msgno, severity, sStr, line);
end;

procedure LoadSqlLib;
var
  sVer: PChar;
begin
  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 0) then begin
      hSqlLibModule := LoadLibrary( PChar( SrvApiDLLs[stSQLServer] ) );
      if (hSqlLibModule = 0) then
        raise Exception.CreateFmt(SErrLibLoading, [ SrvApiDLLs[stSQLServer] ]);
      Inc(SqlLibRefCount);
      SetProcAddresses;

      dberrhandle(ErrHandler);
      dbmsghandle(MsgHandler);

    	// if 1-st loading DB-library
      if SqlLibRefCount = 1 then begin
        sVer := dbinit;
        if sVer = nil then
          raise Exception.Create(SErrLibInit);
          // for example: sVer ~= 'DB-Library version 6.50.252'
        dwLoadedDBLIB := VersionStringToDWORD( StrPas(sVer) );
      end;
    end else
      Inc(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;

procedure FreeSqlLib;
begin
  if SqlLibRefCount = 0 then
    Exit;
    
  SqlLibLock.Acquire;
  try
    if (SqlLibRefCount = 1) then begin
      if not IsLibrary then
        dbexit;	// closes and frees  all DBPROCESS structure of the current application

      if FreeLibrary(hSqlLibModule) then
        hSqlLibModule := 0
      else
        raise Exception.CreateFmt(SErrLibUnloading, [ SrvApiDLLs[stSQLServer] ]);
      Dec(SqlLibRefCount);
      ResetProcAddresses;
      dwLoadedDBLIB := 0;
    end else
      Dec(SqlLibRefCount);
  finally
    SqlLibLock.Release;
  end;
end;


{ TIMssDatabase }
constructor TIMssDatabase.Create(ADatabase: TSDDatabase);
begin
  inherited Create(ADatabase);

  FHandle	:= nil;
  FCurrDataSet	:= nil;
	// by default IsSingleConn = False
  FIsSingleConn := AnsiUpperCase( Trim( ADatabase.Params.Values[szSINGLECONN] ) ) = STrueString;
end;

destructor TIMssDatabase.Destroy;
begin
  FreeHandle;

  if AcquiredHandle then
    FreeSqlLib;
  
  inherited Destroy;
end;

function TIMssDatabase.CreateSrvDataSet(ADataSet: TSDDataSet): TISrvDataSet;
begin
  Result := TIMssDataSet.Create( ADataSet );
end;

procedure TIMssDatabase.Check;
var
  i: Integer;
  E: ESDMssError;
begin
  ResetIdleTimeOut;
	// raise last error message
  if DbErrorList.Count > 0 then begin
    i := DbErrorList.Count-1;
    E := DbErrorList.Errors[i];
    DbErrorList.Delete(i);
    raise E;
  end;
end;

{ Acquire DB handle for the specified ADataset }
procedure TIMssDatabase.ReleaseDBHandle(ADataSet: TSDDataSet; IsFetchAll: Boolean);
var
  ds: TSDDataSet;
begin
  if Assigned(FCurrDataSet) and (ADataSet <> FCurrDataSet) then begin
    ds := FCurrDataSet;
    FCurrDataSet := nil;
    try
      if IsFetchAll then begin
        if ds is TSDStoredProc then
          TSDStoredProc(ds).GetResults
        else if ds is TSDQuery then
          TSDQuery(ds).FetchAll
        else
          raise Exception.Create( Format(SFatalError, ['TIMssDatabase.ReleaseDBHandle']) );
      end;
    except
      FCurrDataSet := ds;
      raise;    
    end;
  end;
  FCurrDataSet := ADataSet;
end;

function TIMssDatabase.SPDescriptionsAvailable: Boolean;
begin
  Result := True;
end;

procedure TIMssDatabase.StartTransaction;
begin
  ReleaseDBHandle(nil, True);		// release an acquired handle
  HandleReset( DBProcPtr );

  dbcmd( DBProcPtr, 'BEGIN TRAN' );
  if dbsqlexec( DBProcPtr ) <> SUCCEED then
    Check;
  if dbresults( DBProcPtr ) <> SUCCEED then
    Check;
end;

procedure TIMssDatabase.Commit;
var
  rcd: RETCODE;
begin
  ReleaseDBHandle(nil, True);		// release an acquired handle
  HandleReset( DBProcPtr );

  dbcmd( DBProcPtr, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRAN END' );
  if dbsqlexec( DBProcPtr ) <> SUCCEED then
    Check;
  repeat
    rcd := dbresults( DBProcPtr );
    if rcd = FAIL then
      Check;
  until rcd = NO_MORE_RESULTS;
end;

procedure TIMssDatabase.Rollback;
var
  rcd: RETCODE;
begin
  ASSERT( Database <> nil );

  ReleaseDBHandle(nil, True);		// release an acquired handle
  HandleReset( DBProcPtr );

  dbcmd( DBProcPtr, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRAN END' );
  if dbsqlexec( DBProcPtr ) <> SUCCEED then
    Check;
  repeat
    rcd := dbresults( DBProcPtr );
    if rcd = FAIL then
      Check;
  until rcd = NO_MORE_RESULTS;
end;

function TIMssDatabase.FieldDataType(ExtDataType: Integer): TFieldType;
begin
  case ExtDataType of
	// Data Type Tokens
    SQLIMAGE:	//$22
      Result := ftBlob;
    SQLTEXT:	//$23
      Result := ftMemo;
    SQLVARBINARY://$25
      Result := ftVarBytes;
    SQLBINARY:	//$2d
      Result := ftBytes;
    SQLVARCHAR,	//$27
    SQLCHAR:	//$2f
      Result := ftString;
    SQLBIT:	//$32
      Result := ftBoolean;
    SQLINT1,	//$30
    SQLINT2:	//$34
      Result := ftSmallInt;
    SQLINT4,	//$38
    SQLINTN:	//$26
      Result := ftInteger;
    SQLDATETIM4,//$3a
    SQLDATETIME,//$3d
    SQLDATETIMN://$6f
      Result := ftDateTime;

    SQLFLT4,	//$3b
    SQLFLT8,	//$3e
    SQLFLTN:	//$6d
      Result := ftFloat;
    SQLDECIMAL,	//$6a
    SQLNUMERIC:	//$6c
      Result := ftFloat;
    SQLMONEY4,	//$7a
    SQLMONEY,	//$3c
    SQLMONEYN:	//$6e
      Result := ftCurrency;
  else
    Result := ftUnknown;
  end;
end;

function TIMssDatabase.GetHandle: PSDHandleRec;
begin
  Result := FHandle;
end;

function TIMssDatabase.GetDBProcPtr: PDBPROCESS;
begin
  ASSERT( Assigned(FHandle) and Assigned(PMssSrvInfo(FHandle^.SrvInfo)),
  	'TIMssDatabase.GetDBProcPtr' );

  Result := PMssSrvInfo(FHandle^.SrvInfo)^.DBProcPtr;
end;

function TIMssDatabase.GetLoginRecPtr: PLOGINREC;
begin
  ASSERT( Assigned(FHandle) and Assigned(PMssSrvInfo(FHandle^.SrvInfo)),
  	'TIMssDatabase.GetLoginRecPtr' );

  Result := PMssSrvInfo(FHandle^.SrvInfo)^.LoginRecPtr;
end;

function TIMssDatabase.GetServerName: string;
begin
  if Assigned(FHandle) then
    if Assigned(FHandle^.SrvInfo) then
      Result := PMssSrvInfo(FHandle^.SrvInfo)^.szServerName;
end;

procedure TIMssDatabase.SetHandle(AHandle: PSDHandleRec);
begin
  LoadSqlLib;

  AllocHandle;

  PMssSrvInfo(FHandle^.SrvInfo)^.DBProcPtr	:=
  	PMssSrvInfo(PSDHandleRec(AHandle)^.SrvInfo)^.DBProcPtr;
  PMssSrvInfo(FHandle^.SrvInfo)^.LoginRecPtr	:=
  	PMssSrvInfo(PSDHandleRec(AHandle)^.SrvInfo)^.LoginRecPtr;
  PMssSrvInfo(FHandle^.SrvInfo)^.szServerName	:=
  	StrNew( PMssSrvInfo(PSDHandleRec(AHandle)^.SrvInfo)^.szServerName );
end;

procedure TIMssDatabase.AllocHandle;
var
  s: PMssSrvInfo;
begin
  ASSERT( not Assigned(FHandle), 'TIMssDatabase.AllocHandle' );

  New(FHandle);
  FillChar( FHandle^, SizeOf(FHandle^), $0 );
  FHandle^.SrvType := Ord( Database.ServerType );

  New(s);
  FillChar( s^, SizeOf(s^), $0 );
  FHandle^.SrvInfo := s;
end;

procedure TIMssDatabase.FreeHandle;
begin
  if Assigned(FHandle) then begin
    if Assigned(FHandle^.SrvInfo) then begin
      StrDispose( PMssSrvInfo(FHandle^.SrvInfo)^.szServerName );
      Dispose( PMssSrvInfo(FHandle^.SrvInfo) );
      PMssSrvInfo(FHandle^.SrvInfo) := nil;
    end;
    Dispose( FHandle );
    FHandle := nil;
  end;
end;

procedure TIMssDatabase.GetStmtResult(const Stmt: string; List: TStrings);
var
  rcd: RETCODE;
  DataLen: DBINT;
  sObjName: string;
begin
  ReleaseDBHandle(nil, True);	// release an acquired handle
  HandleReset( DBProcPtr );

  dbcmd( DBProcPtr, PChar(Stmt) );
  if dbsqlexec( DBProcPtr ) <> SUCCEED then Check;
  if dbresults( DBProcPtr ) <> SUCCEED then Check;

  rcd := dbnextrow( DBProcPtr );
	//  rcd = NO_MORE_ROWS or FAIL when EOF
  while rcd = REG_ROW do begin
    DataLen := dbdatlen( DBProcPtr, 1 );
    if DataLen > 0 then begin
      SetLength(sObjName, DataLen);
      StrLCopy( PChar(sObjName), PChar( dbdata( DBProcPtr, 1 ) ), DataLen );
    end;

    if sObjName <> '' then
      List.Add( sObjName );
    sObjName	:= '';

    rcd := dbnextrow( DBProcPtr );
  end;
  HandleReset( DBProcPtr );
end;

function TIMssDatabase.GetClientVersion: LongInt;
begin
  Result := dwLoadedDBLIB;
end;

function TIMssDatabase.GetServerVersion: LongInt;
var
  procinfo: TDBPROCINFO;
begin
  procinfo.SizeOfStruct := SizeOf(procinfo);
  if dbprocinfo( DBProcPtr, @procinfo) <> SUCCEED then
    Check;

  Result := MakeLong( procinfo.ServerMinor, procinfo.ServerMajor );
end;

function TIMssDatabase.GetVersionString: string;
var
  List: TStringList;
begin
  List := TStringList.Create;
  try
    GetStmtResult( 'select @@version', List );
    if List.Count > 0 then
      Result := List.Strings[0];
  finally
    List.Free;
  end;
end;

procedure TIMssDatabase.GetStoredProcNames(List: TStrings);
  procedure IGetStoredProcName( AList: TStrings );
  var
    rcd: RETCODE;
    DataLen: DBINT;
    sOwner, sProcName: string;
  begin
    HandleReset( DBProcPtr );
    dbcmd( DBProcPtr, PChar('sp_stored_procedures') );
    if dbsqlexec( DBProcPtr ) <> SUCCEED then Check;
    if dbresults( DBProcPtr ) <> SUCCEED then Check;

    rcd := dbnextrow( DBProcPtr );
	//  rcd = NO_MORE_ROWS or FAIL when EOF
    while rcd = REG_ROW do begin
      sOwner 	:= '';
      sProcName	:= '';
    	// column name: PROCEDURE_OWNER
      DataLen := dbdatlen( DBProcPtr, 2 );
      if DataLen > 0 then begin
        SetLength(sOwner, DataLen);
        StrLCopy( PChar(sOwner), PChar( dbdata( DBProcPtr, 2 ) ), DataLen );
      end;
    	// column name: PROCEDURE_NAME
      DataLen := dbdatlen( DBProcPtr, 3 );
      if DataLen > 0 then begin
        SetLength(sProcName, DataLen);
        StrLCopy( PChar(sProcName), PChar( dbdata( DBProcPtr, 3 ) ), DataLen );
      end;

      if sProcName <> '' then
        AList.Add( Format('%s.%s', [sOwner, sProcName]) );

      rcd := dbnextrow( DBProcPtr );
    end;
    HandleReset( DBProcPtr );
  end;

var
  sStmt: string;
begin
  sStmt := 'select distinct su.name + ''.'' + so.name + '';'' + CONVERT(varchar, sp.number) ' +
  	   'from sysobjects so, sysusers su, sysprocedures sp ' +
	   'where so.type in (''P'', ''X'') and so.uid = su.uid and so.id = sp.id order by 1';

  List.BeginUpdate;
  try
    List.Clear;
    if Database.ServerMajor >= 7 then begin
      ReleaseDBHandle(nil, True);		// release an acquired handle
      IGetStoredProcName( List )
    end else
      GetStmtResult( sStmt, List );
  finally
    List.EndUpdate;
  end;
end;

procedure TIMssDatabase.GetTableNames(Pattern: string; SystemTables: Boolean; List: TStrings);
var
  sStmt, sTableTypes, sTblCondition: string;
begin
  if Trim(Pattern) <> '' then
    sTblCondition := ' and so.name like ''' + Pattern + '%''';
  if SystemTables then sTableTypes := '''S'', ';
  sTableTypes := Format( '(%s''U'', ''V'')', [sTableTypes] );
  sStmt := 'select su.name + ''.'' + so.name from sysobjects so, sysusers su '+
 	   'where so.uid = su.uid ' + sTblCondition +
  	   ' and so.type in ' + sTableTypes + ' order by 1';

  List.BeginUpdate;
  try
    List.Clear;

    GetStmtResult( sStmt, List );
  finally
    List.EndUpdate;
  end;
end;

procedure TIMssDatabase.GetTableFieldNames(const TableName: string; List: TStrings);
var
  sStmt: string;
begin
  sStmt := Format( 'select name from syscolumns where id = OBJECT_ID(''%s'')',
  			[TableName] );

  List.BeginUpdate;
  try
    List.Clear;

    GetStmtResult( sStmt, List );
  finally
    List.EndUpdate;
  end;
end;

procedure TIMssDatabase.HandleReset(AHandle: PDBPROCESS);
var
  rcd: RETCODE;
begin
  dbfreebuf( AHandle );

  repeat
    rcd := dbresults( AHandle );
    if rcd = FAIL then
      Check;
  until (rcd = NO_MORE_RESULTS) or (rcd = FAIL);

  if (rcd <> NO_MORE_RESULTS) and (rcd <> SUCCEED) then
    Check;
end;

procedure TIMssDatabase.Logon(const sRemoteDatabase, sUserName, sPassword: string);
var
  dbproc: PDBPROCESS;
  pLogin: PLOGINREC;
  sAppName, sHostName, sSrvName, sDbName: string;
  IntValue: Integer;
begin
  try
    LoadSqlLib;

    AllocHandle;
    pLogin := dblogin;

    sAppName := Database.Params.Values[szAPPNAME];
    sHostName := Database.Params.Values[szHOSTNAME];

    if Length(Trim( sAppName )) = 0 then
      sAppName := GetAppName;
    if Length(Trim( sHostName )) = 0 then
      sHostName := GetHostName;
	// Sets the maximum number of simultaneously open DBPROCESS structures, which has to be more then current value
    IntValue := StrToIntDef( Database.Params.Values[szMAXCURSORS], 0 );
    if IntValue > dbgetmaxprocs then
      dbsetmaxprocs( IntValue );
	// set login timeout before connect
    if Trim( Database.Params.Values[szLOGINTIMEOUT] ) <> '' then begin
      IntValue := StrToIntDef( Trim( Database.Params.Values[szLOGINTIMEOUT] ), 0 );
      dbsetlogintime(IntValue);
    end;

    dbsetlname(pLogin, PChar(sUserName), DBSETUSER);
    dbsetlname(pLogin, PChar(sPassword), DBSETPWD);
    dbsetlname(pLogin, PChar(sAppName), DBSETAPP);
    dbsetlname(pLogin, PChar(sHostName), DBSETHOST);
    dbsetlname(pLogin, nil, DBVER60);
    	// it's required to use NT Athentication for MSSQL client, which is started via DCOM
    if Trim(sUserName) = '' then
      dbsetlname((pLogin), nil, DBSETSECURE);

    sSrvName := ExtractServerName(sRemoteDatabase);
    dbproc := dbopen( pLogin, PChar( sSrvName ) );
    if dbproc = nil then begin
      Check;
      // Sometimes, Check function does not always raise an exception, because DBErrorList is empty (causing a null process exception in the following functions)
      DatabaseError(SErrDBProcIsNull);
    end;

    PMssSrvInfo(FHandle^.SrvInfo)^.LoginRecPtr	:= pLogin;
    PMssSrvInfo(FHandle^.SrvInfo)^.DBProcPtr	:= dbproc;

	// enable or disable the use of quoted identifiers, which can be applied to a database name too
    if dbisopt( dbproc, DBQUOTEDIDENT, nil ) = DB_TRUE then
      dbclropt( dbproc, DBQUOTEDIDENT, nil );
    if AnsiUpperCase( Trim( Database.Params.Values[szQUOTEDIDENT] ) ) <> SFalseString then begin
      if (dbsetopt( dbproc, DBQUOTEDIDENT, nil ) <> SUCCEED) or
         (dbsqlexec( dbproc ) <> SUCCEED)
      then
        Check;
    end;
    HandleReset( dbproc );
    
	// dbuse fails with a quoted database name
    sDbName := ExtractDatabaseName(sRemoteDatabase);
    if Length(sDbName) > 0 then
      if (dbcmd(dbproc, PChar('use ' + sDbName)) <> SUCCEED) or
         (dbsqlexec( dbproc ) <> SUCCEED)
      then
        Check;
    HandleReset( dbproc );

    if Assigned(pLogin) and Assigned(dbproc) then begin
      PMssSrvInfo(FHandle^.SrvInfo)^.LoginRecPtr:= pLogin;
      PMssSrvInfo(FHandle^.SrvInfo)^.DBProcPtr	:= dbproc;
      PMssSrvInfo(FHandle^.SrvInfo)^.szServerName := StrNew( PChar(sSrvName) );
    end;
    FServerVersion := GetServerVersion;

  except
    Logoff(False);

    raise;
  end;
end;

procedure TIMssDatabase.Logoff(Force: Boolean);
begin
  if Assigned(FHandle) and Assigned(DBProcPtr) then
    if (dbclose( DBProcPtr ) <> SUCCEED) and Force then
      Check;
  FreeHandle;

  FreeSqlLib;
end;

function TIMssDatabase.NativeDataSize(FieldType: TFieldType): Word;
const
  { Converting from TFieldType � Program Data Type(MSSQL) }
  MSSQLSrvDataSizeMap: array[TFieldType] of Word = ( 0,	// ftUnknown
	// ftString, ftSmallint, ftInteger, ftWord, ftBoolean
	0,	2, 	4, 	2, 	2,
	// ftFloat, ftCurrency, ftBCD, ftDate, ftTime
        SizeOf(Double), SizeOf(Double),	0, SizeOf(DBDATETIME), SizeOf(DBDATETIME),
        // ftDateTime, ftBytes, ftVarBytes, ftAutoInc, ftBlob
        SizeOf(DBDATETIME), 	0, 	0, 	0, 	0,
        // ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle
        0,	0,	0,	0,	0,
        // ftTypedBinary, ftCursor
        0,	0
{$IFDEF SD_VCL4},
	// ftFixedChar, ftWideString, ftLargeint,
        0,	0,	0,
        // ftADT, ftArray, ftReference, ftDataSet
        0,	0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL5},
        // ftOraBlob, ftOraClob, ftVariant,
        0,	0,	0,
        // ftInterface, ftIDispatch, ftGuid
        0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL6},
        // ftTimeStamp, ftFMTBcd
        0,      0
{$ENDIF}
        );
begin
  Result := MSSQLSrvDataSizeMap[FieldType];
end;

function TIMssDatabase.NativeDataType(FieldType: TFieldType): Integer;
const
  { Converting from TFieldType � bind Data Type(SQLServer) }
  MSSQLSrvDataTypeMap: array[TFieldType] of Integer = ( 0,	// ftUnknown
	// ftString, ftSmallint, ftInteger, ftWord, ftBoolean
	NTBSTRINGBIND, SMALLBIND, INTBIND, INTBIND, SMALLBIND,
	// ftFloat, ftCurrency, ftBCD, ftDate, ftTime
        FLT8BIND, FLT8BIND, 	0, DATETIMEBIND, DATETIMEBIND,
        // ftDateTime, ftBytes, ftVarBytes, ftAutoInc, ftBlob
        DATETIMEBIND, BINARYBIND, BINARYBIND, 0, VARYBINBIND,
        // ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle
        VARYBINBIND,	0,	0,	0,	0,
        // ftTypedBinary, ftCursor
        0,	0
{$IFDEF SD_VCL4},
	// ftFixedChar, ftWideString, ftLargeint,
        0,	0,	0,
        // ftADT, ftArray, ftReference, ftDataSet
        0,	0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL5},
        // ftOraBlob, ftOraClob, ftVariant,
        0,	0,	0,
        // ftInterface, ftIDispatch, ftGuid
        0,	0,	0
{$ENDIF}
{$IFDEF SD_VCL6},
        // ftTimeStamp, ftFMTBcd
        0,      0
{$ENDIF}        
        );
begin
  Result := MSSQLSrvDataTypeMap[FieldType];
end;

function TIMssDatabase.RequiredCnvtFieldType(FieldType: TFieldType): Boolean;
begin
  Result :=
  	FieldType in [ftBoolean,
	         ftDate, ftTime, ftDateTime,
        	 ftCurrency, ftFloat];
end;

procedure TIMssDatabase.HandleSetDefParams(AHandle: PDBPROCESS);
var
  sValue: string;
begin
	// Blob limit for DB-Library
  if dbsetopt( AHandle, DBTEXTLIMIT, OPT_DBTEXTLIMIT ) <> SUCCEED then
    Check;
  	// Blob limit for SQLServer
  if dbsetopt( AHandle, DBTEXTSIZE, OPT_DBTEXTSIZE ) <> SUCCEED then
    Check;

	// set command timeout value, if it's set
  sValue := Trim( Database.Params.Values[szCMDTIMEOUT] );
  if sValue <> '' then begin
    if dbsettime( StrToIntDef( sValue, 0 ) ) <> SUCCEED then
      Check;
  end;
	// enable or disable the use of quoted identifiers
  if AnsiUpperCase( Trim( Database.Params.Values[szQUOTEDIDENT] ) ) <> SFalseString then begin
    if dbsetopt( AHandle, DBQUOTEDIDENT, nil ) <> SUCCEED then
      Check;
  end else begin
    if dbclropt( AHandle, DBQUOTEDIDENT, nil) <> SUCCEED then
      Check;
  end;

  if dbsqlexec( AHandle ) = SUCCEED then
    HandleSetTransIsolation( AHandle, Database.TransIsolation )
  else
    Check;
end;

procedure TIMssDatabase.SetDefaultParams;
begin
  HandleSetDefParams( DBProcPtr );
end;

procedure TIMssDatabase.HandleSetTransIsolation(AHandle: PDBPROCESS; Value: TSDTransIsolation);
begin
  ReleaseDBHandle(nil, True);		// release an acquired handle
  HandleReset( AHandle );

  dbcmd( AHandle, PChar(IsolLevelCmd[Value]) );
  if dbsqlexec( AHandle ) <> SUCCEED then
    Check;
  if dbresults( AHandle ) <> SUCCEED then
    Check;
end;

procedure TIMssDatabase.SetTransIsolation(Value: TSDTransIsolation);
begin
  HandleSetTransIsolation( DBProcPtr, Value );
end;

{ TIMssDataSet }
constructor TIMssDataSet.Create(ADataSet: TSDDataSet);
begin
  inherited Create(ADataSet);

  FStmt		:= '';
  FBindStmt	:= '';
  FRowsAffected := -1;
  FConnected	:= False;
  FNextResults	:= False;

  FHandle := nil;

  FIsSingleConn := SrvDatabase.IsSingleConn;
end;

destructor TIMssDataSet.Destroy;
begin
  if FHandle <> nil then
    Disconnect(False);

  inherited Destroy;
end;

procedure TIMssDataSet.Check;
begin
  SrvDatabase.Check;
end;

procedure TIMssDataSet.CloseResultSet;
begin
  if FNextResults then
    Exit;
  FBindStmt := '';
	// to avoid dropping of the current active result set, when other query is closing
  if FIsSingleConn and
     Assigned(SrvDatabase.CurrDataSet) and
     (SrvDatabase.CurrDataSet <> DataSet)
  then
    Exit;

	// drop procedure's result sets
  if (DataSet is TSDStoredProc) and (not FEndResults) then begin
    repeat
      HandleCurReset( Handle );
    until not HandleSpResults( Handle );
  end;

	// for example, in case of empty statement(raise an exception), Handle is not created at all
  if Assigned(Handle) then begin
    if DataSet is TSDQuery then
      HandleCancel( Handle );
    HandleReset( Handle );
  end;

	// last condition is required for possibility to add persistent fields at design-time
  if (DataSet is TSDStoredProc) and not(csDesigning in DataSet.ComponentState) then
    DataSet.FieldDefs.Clear;

	// it's need in case of close a result set without full fetch
  ReleaseDBHandle;
end;

procedure TIMssDataSet.Connect;

  function IsQuery(const AStmt: string): Boolean;
  var
    SelPos, i: Integer;
  begin
    Result := (dsfOpened in DSFlags) or (dsfStoredProc in DSFlags);
    if Result then
      Exit;
    if dsfExecSQL in DSFlags then
      Exit;

    SelPos := AnsiTextPos('select', AStmt);
    if SelPos = 0 then
      Exit;
    Result := SelPos = 1;
    if Result then
      Exit;
    for i:=1 to SelPos-1 do
      if not (AStmt[i] in [#$0A, #$0D, #$20]) then
        Exit;

    Result := True;
  end;

begin
  // if it is not need to use a single process or statement(FStmt) can return rows,
  //else it's using Process of SrvDatabase (without connecting private process)
  if not FConnected and not FIsSingleConn and IsQuery(FStmt) then
    FHandle := CreateDBProcess;

  FConnected	:= True;
  FEndResults	:= False;
end;

procedure TIMssDataSet.Disconnect(Force: Boolean);
begin
  FBindStmt	:= '';
  FStmt		:= '';
  FConnected	:= False;

  ReleaseDBHandle;

  if FHandle = nil then Exit;

  if (dbclose( FHandle ) <> SUCCEED) and Force then
    Check;

  FHandle := nil;
end;

procedure TIMssDataSet.Execute;
begin
  if DataSet is TSDStoredProc
  then SpExecute
  else QExecute;
end;

function TIMssDataSet.FetchNextRow: Boolean;
var
  rcd: RETCODE;
begin
  rcd := FAIL;
  try
    rcd := dbnextrow( Handle );
    SrvDatabase.ResetIdleTimeOut;
  finally
	//  rcd = NO_MORE_ROWS or FAIL when EOF
    Result := not ((rcd = NO_MORE_ROWS) or (rcd = FAIL));
  end;

  if Result then begin
    FetchDataSize;
    if BlobFieldCount > 0 then
      FetchBlobFields;
  end else
    ReleaseDBHandle;  
end;

function TIMssDataSet.CnvtDBField2Field(AField: TField; InBuf, OutBuf: Pointer): Boolean;
var
  InData, OutData: Pointer;
  dtDateTime: TDateTimeRec;
begin
  if PFieldInfo(InBuf)^.DataSize > 0 then
    PFieldInfo(OutBuf)^.FetchStatus := indVALUE
  else
    PFieldInfo(OutBuf)^.FetchStatus := indNULL;

  if PFieldInfo(OutBuf)^.FetchStatus <> indNULL then begin
    InData	:= Pointer( Integer(InBuf) + SizeOf(TFieldInfo) );
    OutData	:= Pointer( Integer(OutBuf) + SizeOf(TFieldInfo) );
    	// DateTime fields
    if SrvDatabase.RequiredCnvtFieldType(AField.DataType) then begin

      if AField is TDateTimeField then begin
        dtDateTime := CnvtDBDateTime2DateTimeRec(AField.DataType, InData, PFieldInfo(InBuf)^.DataSize);
        TDateTimeRec(OutData^) := dtDateTime;
        PFieldInfo(OutBuf)^.DataSize := SizeOf(TDateTimeRec);
      end else if AField.DataType = ftBoolean then begin

    	// for ftBoolean (bit datatype) allocates 2 byte(as for ftSmallint), but returns only 1 byte of data
        if SrvDatabase.NativeDataSize(AField.DataType) <> PFieldInfo(InBuf)^.DataSize then
          PFieldInfo(InBuf)^.DataSize := SrvDatabase.NativeDataSize(AField.DataType);
        Move( InData^, OutData^, PFieldInfo(InBuf)^.DataSize );
        PFieldInfo(OutBuf)^.DataSize := PFieldInfo(InBuf)^.DataSize;
      end else begin

      	// for DECIMAL and NUMERIC datatypes (returns as double(8 bytes),
        //but PFieldInfo(InBuf)^.DataSize = 19, it's wrong)
        // for SMALLMONEY returns DataSize = 4 (it's need set to 8 byte)
        if (AField is TFloatField) and
           (SrvDatabase.NativeDataSize(AField.DataType) <> PFieldInfo(InBuf)^.DataSize)
        then
          PFieldInfo(InBuf)^.DataSize := SrvDatabase.NativeDataSize(AField.DataType);
        Move( InData^, OutData^, PFieldInfo(InBuf)^.DataSize );
        PFieldInfo(OutBuf)^.DataSize := PFieldInfo(InBuf)^.DataSize;
      end;

    end else begin
    	// for ftSmallint (tinyint datatype) allocates 2 byte(as for ftSmallint), but returns only 1 byte of data
      if AField.DataType = ftSmallint then
        PFieldInfo(InBuf)^.DataSize := SrvDatabase.NativeDataSize(AField.DataType);

      if AField.DataType = ftString
      then Move( InData^, OutData^, PFieldInfo(InBuf)^.DataSize + 1 )
      else Move( InData^, OutData^, PFieldInfo(InBuf)^.DataSize );
      PFieldInfo(OutBuf)^.DataSize := PFieldInfo(InBuf)^.DataSize;
    end;
  end;
  Result := True;
end;

function TIMssDataSet.CnvtDBDateTime2DateTimeRec(ADataType: TFieldType; Buffer: PChar; BufSize: Integer): TDateTimeRec;
var
  dateinfo: DBDATEREC;
  dtDate, dtTime: TDateTime;
begin
  if dbdatecrack( FHandle, @dateinfo, LPDBDATETIME(Buffer) ) <> SUCCEED then
    Check;

  dtDate := EncodeDate(dateinfo.Year, dateinfo.Month, dateinfo.Day);
  dtTime := EncodeTime(dateinfo.hour, dateinfo.minute, dateinfo.second, 0);

  case (ADataType) of
    ftTime:
      Result.Time := DateTimeToTimeStamp(dtTime).Time + dateinfo.millisecond;
    ftDate:
      Result.Date := DateTimeToTimeStamp(dtDate).Date;
    ftDateTime:
      Result.DateTime := TimeStampToMSecs( DateTimeToTimeStamp(dtDate + dtTime) ) + dateinfo.millisecond;
  else
    Result.DateTime := 0.0;
  end;
end;

function TIMssDataSet.ResultSetExists: Boolean;
begin
  Result := True;
end;

procedure TIMssDataSet.GetFieldDescs(Descs: TSDFieldDescList);
begin
  if DataSet is TSDStoredProc then
    SpGetFieldDescs( Descs )
  else
    QGetFieldDescs( Descs );
end;

function TIMssDataSet.GetExecuted: Boolean;
begin
  Result := Length(FBindStmt) > 0;
end;

function TIMssDataSet.GetHandle: PSDCursor;
begin
  if DataSet is TSDStoredProc then begin
    Result := SrvDatabase.DBProcPtr;
    Exit;
  end;
  if FConnected then begin
  	// if FHandle = nil then the statement can't return rows
    if FIsSingleConn or (FHandle = nil)
    then Result := SrvDatabase.DBProcPtr
    else Result := FHandle;
  end else
    Result := nil;
end;

function TIMssDataSet.GetSrvDatabase: TIMssDatabase;
begin
  Result := (inherited SrvDatabase) as TIMssDatabase;
end;

{ Marks a database handle as used by the current dataset }
procedure TIMssDataSet.AcquireDBHandle;
begin
  if FIsSingleConn then
    SrvDatabase.ReleaseDBHandle(DataSet, True);
end;

{ Releases a database handle, which was used by the current dataset }
procedure TIMssDataSet.ReleaseDBHandle;
begin
  if SrvDatabase.CurrDataSet = DataSet then
    SrvDatabase.ReleaseDBHandle(nil, False);
end;

{ Setting returned data size after field fetch }
procedure TIMssDataSet.FetchDataSize;
var
  i: Integer;
  InfoPtr: PChar;
begin
  for i:=0 to DataSet.FieldCount-1 do begin
    with DataSet.Fields[i] do begin
    	// skips calculated and lookup fields
      if not(FieldNo > 0) then Continue;
	// get PFieldInfo pointer
      InfoPtr := PChar( Integer(FSelectBuffer) + FieldBufOffs[FieldNo-1] );
	// set a data length for returned columns (excepting text/image columns)
      if not IsBlobType(DataType) then
        PFieldInfo( InfoPtr )^.DataSize := dbdatlen( Handle, FieldNo );
    end;
  end;
end;

{ finds a command which can return rows and omit other (for example: DECLARE, SET commands) }
function TIMssDataSet.CanReturnRows: Boolean;
var
  rcd: RETCODE;
begin
  Result := False;
  if (DataSet is TSDQuery) and ((dsfOpened in DSFlags) or (dsfFieldList in DSFlags)) then begin
    while dbcmdrow( Handle ) <> SUCCEED do begin
      rcd := dbresults( Handle );
      if rcd = FAIL then
        Check
      else if rcd <> SUCCEED then
        Exit;
    end;
    Result := True;
  end;
end;

{ Binds result column from select list }
procedure TIMssDataSet.SetSelectBuffer;
var
  i, nOffset: Integer;
  BindDataSize: Word;
  BindDataType: Word;
  BindDataBuffer: PChar;
  TmpField: TField;
begin
  if not Executed then
    InternalExecute;

  CanReturnRows;
  nOffset := 0;		// pointer to the TFieldInfo

  for i:=0 to DataSet.FieldDefs.Count-1 do begin
    TmpField := FieldByNumber(DataSet.FieldDefs[i].FieldNo);
    if Assigned(TmpField) then with TmpField do begin
    	// skips calculated fields
      if Calculated then Continue;

      BindDataType := SrvDatabase.NativeDataType(DataType);
      if BindDataType = 0 then
        DatabaseErrorFmt( SUnknownFieldType, [FieldName] );

      BindDataSize := NativeFieldDataSize(TmpField);
      BindDataBuffer := PChar( Integer(FSelectBuffer) + nOffset + SizeOf(TFieldInfo) );

      if not IsBlobType(DataType) then
        if dbbind( Handle, FieldNo, BindDataType, BindDataSize, LPBYTE(BindDataBuffer) ) = FAIL then
          Check;

      Inc(nOffset, SizeOf(TFieldInfo) + BindDataSize);
    end;
  end;
end;

function TIMssDataSet.ReadBlob(FieldNo: Integer; var BlobData: string): Longint;
var
  DataPtr: LPCBYTE;
  BlobSize: DBINT;
begin
  DataPtr := nil;

  BlobSize := dbdatlen( Handle, FieldNo);
  if BlobSize > 0 then
    DataPtr := dbdata( Handle, FieldNo );	// dbdata can return up 2GB
  if (BlobSize > 0) and Assigned(DataPtr) then begin
    SetLength(BlobData, BlobSize);
    System.Move(DataPtr^, PChar(BlobData)^, BlobSize);
  end;
  
  Result := BlobSize;
end;

	// Query methods
procedure TIMssDataSet.QBindParams;
begin
end;

function TIMssDataSet.QGetRowsAffected: Integer;
begin
  Result := FRowsAffected;
end;

// byte array -> '0x.....'
// to '19980131 14:28:53:880' = 'yyyymmdd hh:mi:ss:nnn'
function TIMssDataSet.CnvtDateTimeToSQLVarChar(Value: TDateTime): string;
const
  sDateFormat = 'yyyymmdd';
  sTimeFormat = 'hh":"nn":"ss';
var
  Hour, Min, Sec, MSec: Word;
begin
  Result := FormatDateTime(sDateFormat, Value);
  DecodeTime(Value, Hour, Min, Sec, MSec);
  if (Hour > 0) or (Min > 0) or (Sec > 0) then
    Result := Result + ' ' + FormatDateTime(sTimeFormat, Value);
  if MSec > 0 then
    Result := Format('%s:%.3d', [Result, MSec]);
  Result := Format('''%s''', [Result]);
end;

function TIMssDataSet.CnvtDateTimeToSQLDateTime(Value: TDateTime): DBDATETIME;
var
  tstamp: TTimeStamp;
begin
	// TDateTime(0,....) = 30.12.1899
  tstamp := DateTimeToTimeStamp(Value);
	// (Days since 12/30/1899) - 2 = Days since Jan 1, 1900
  Result.dtdays := Trunc(Value) - 2;
      	// (Number of milliseconds(100ths of second(1s=1000)) since midnight) *3/10 = 300ths of a second since midnight (1s=300)
  Result.dttime := (tstamp.Time * 3)div 10;
end;

function TIMssDataSet.CnvtFloatToSQLVarChar(Value: Double): string;
var
  Len: Integer;
  szStr: PChar;
begin
  Result := '';
  szStr := StrAlloc(DBMAXCHAR);
  try
    Len := dbconvert( SrvDatabase.DBProcPtr, SQLFLT8, LPBYTE(@Value), SizeOf(Value),
  			SQLCHAR, LPBYTE(szStr), -1 );
    if Len > 0 then
      Result := szStr;
  finally
    StrDispose(szStr);
  end;
end;

procedure TIMssDataSet.InternalQBindParams;
const
  ParamPrefix	= ':';
  SqlNullValue	= 'NULL';
var
  i: Integer;
  sFullParamName, sValue: string;
begin
  FBindStmt := FStmt;

  with Query do
    for i:=Query.ParamCount-1 downto 0 do begin
      	// set parameter value
      if Params[i].IsNull then begin
        sValue := SqlNullValue
      end else begin
        case Params[i].DataType of
{$IFDEF SD_VCL4}
          ftLargeInt,
{$ENDIF}
          ftInteger,
          ftSmallint,
          ftWord:	sValue := Params[i].AsString;
          ftBoolean:
            if Params[i].AsBoolean
            then sValue := '1' else sValue := '0';
          ftBytes,
          ftVarBytes,
          ftBlob:
            sValue := CnvtVarBytesToHexString(Params[i].Value);
          ftDate,
          ftTime,
          ftDateTime:	sValue := CnvtDateTimeToSQLVarChar(Params[i].AsDateTime);
          ftCurrency,
          ftFloat:	sValue := CnvtFloatToSQLVarChar(Params[i].AsFloat);
        else
          sValue := Params[i].AsString;
          	// exclude zero-terminator from string (and string length), what happens after richedit stores data
          while (Length(sValue) > 0) and (sValue[Length(sValue)] = #0) do
            SetLength(sValue, Length(sValue)-1);

          sValue := Format('''%s''', [RepeatChar('''', sValue)]);
        end;	// end of case
      end;	// end of for
	// change a parameter's name on a value of the parameter
      sFullParamName := ParamPrefix + Params[i].Name;
      if not ReplaceString( False, sFullParamName, sValue, FBindStmt ) then begin
      	// if parameter's name is enclosed in double quotation marks
        sFullParamName := Format( '%s%s%s%s', [ParamPrefix, QuoteChar, Params[i].Name, QuoteChar] );
        ReplaceString( False, sFullParamName, sValue, FBindStmt );
      end;
    end;
end;

procedure TIMssDataSet.InternalQExecute;
begin
  FRowsAffected := -1;

  if not FIsSingleConn and (Handle = nil) then
    Connect;

  InternalQBindParams;

  HandleReset(Handle);

  dbcmd( Handle, PChar(FBindStmt) );

  DbExecHandle( Handle );

  if dsfExecSQL in DSFlags then
    FRowsAffected := dbcount( Handle );
end;

function TIMssDataSet.CreateDBProcess: PDBPROCESS;
var
  szDBName: PChar;
begin
  Result := dbopen( SrvDatabase.LoginRecPtr, PChar( SrvDatabase.ServerName ) );
  if Result = nil then begin
    Check;
      // Sometimes, Check function does not always raise an exception, because DBErrorList is empty (causing a null process exception in the following functions)
    DatabaseError(SErrDBProcIsNull);
  end;

	// dbname function returns the current database name for database component,
      // which could be changed after login using USE statement
  szDBName := SDMss.dbname( SrvDatabase.DBProcPtr );
  if Length(szDBName) > 0 then
    if dbuse( Result, szDBName ) = FAIL then
      Check;

  SrvDatabase.HandleSetDefParams( Result );
end;

procedure TIMssDataSet.QGetFieldDescs(Descs: TSDFieldDescList);
var
  pFieldDesc: PSDFieldDesc;
  ft: TFieldType;
  Col, NumCols: INT;
  dbcol: TDBCOL;
begin
  if not Executed then
    InternalExecute;
	// find a command which can return rows and omit other (for example: DECLARE, SET commands)
  CanReturnRows;

  NumCols := dbnumcols( Handle );

  for Col:=1 to NumCols do begin
    FillChar(dbcol, SizeOf(dbcol), 0);
    dbcol.SizeOfStruct := SizeOf(dbcol);
    if dbcolinfo( PDBHANDLE(Handle), CI_REGULAR, Col, 0, @dbcol ) = FAIL then
      Check;

    ft := SrvDatabase.FieldDataType(dbcol.ColType);
    if ft = ftUnknown then
      DatabaseErrorFmt( SBadFieldType, [StrPas(dbcol.Name)] );

    New( pFieldDesc );
    with pFieldDesc^ do begin
      FieldName	:= StrPas(dbcol.Name);
      FieldNo	:= Col;
      DataType	:= ft;
      Size	:= dbcol.MaxLength;
      Precision	:= dbcol.Precision;
  	// if dbcol.Null <> 1 then null values are not permitted for the column (Required = True)
      Required	:= not(dbcol.Null = 1);
    end;
    dbcol.MaxLength := 0;
    dbcol.MaxLength := dbcollen( PDBPROCESS(Handle), 1 );
    
    Descs.Add( pFieldDesc );
  end;
end;

procedure TIMssDataSet.QPrepareSQL(Value: PChar);
begin
  FStmt		:= Value;
  FBindStmt	:= '';

  Connect;
end;

procedure TIMssDataSet.InternalExecute;
begin
  AcquireDBHandle;

  if DataSet is TSDStoredProc then
    InternalSpExecute
  else
    InternalQExecute;
end;

procedure TIMssDataSet.QExecute;
begin
	// to process a prepared statement(w/o result set: UPDATE, INSERT..) repeatedly
  if not Executed or (dsfExecSQL in DSFlags) then
    InternalExecute;
end;

{ BindBuffer is use really only for modify result set with Blob fields }
function TIMssDataSet.BindBufferSize: Integer;
var
  i, NumBytes, DataLen: Integer;
begin
  NumBytes := 0;
  if DataSet is TSDQuery then begin
    for i := 0 to Query.Params.Count - 1 do
      with Query.Params[I] do begin
        if DataType = ftUnknown then
          DatabaseErrorFmt(SNoParameterValue, [Name]);
        if ParamType = ptUnknown then
          DatabaseErrorFmt(SNoParameterType, [Name]);
        if SrvDatabase.RequiredCnvtFieldType(DataType) then begin
          case DataType of
            ftCurrency, ftFloat:
              DataLen := Length( CnvtFloatToSQLVarChar(AsFloat) );
            ftBoolean:
              DataLen := QParamBindSize(Query.Params[I]);
          else
            DataLen := SizeOf( DBDATETIME );
          end;
  		// for zero-terminator
          Inc(DataLen);
        end else
          DataLen := QParamBindSize(Query.Params[I]);
        Inc(NumBytes, SizeOf(TFieldInfo) + DataLen);
      end;
  end else if DataSet is TSDStoredProc then begin
    for i := 0 to StoredProc.Params.Count - 1 do
      with StoredProc.Params[I] do begin
        if DataType = ftUnknown then
          DatabaseErrorFmt(SNoParameterValue, [Name]);
        if ParamType = ptUnknown then
          DatabaseErrorFmt(SNoParameterType, [Name]);
        if SrvDatabase.RequiredCnvtFieldType(DataType) then begin
          if IsDateTimeType( DataType ) then
            DataLen := SizeOf( DBDATETIME )
          else
            DataLen := SpParamBindSize(StoredProc.Params[I])
        end else
          DataLen := SpParamBindSize(StoredProc.Params[I]);

        Inc(NumBytes, SizeOf(TFieldInfo) + DataLen);
      end;
  end else
    raise Exception.Create( Format(SFatalError, ['TIMssDataSet.BindBufferSize']) );
  Result := NumBytes;
end;

procedure TIMssDataSet.DbExecHandle(AHandle: PDBPROCESS);
begin
  if dbsqlexec( AHandle ) <> SUCCEED then
    Check;
  if dbresults( AHandle ) <> SUCCEED then
    Check;
end;

procedure TIMssDataSet.HandleSpInit(AHandle: PDBPROCESS; ARpcName: PChar);
begin
  if dbrpcinit( AHandle, ARpcName, 0 ) <> SUCCEED then
    Check;
end;

procedure TIMssDataSet.HandleSpExec(AHandle: PDBPROCESS);
begin
  if dbrpcexec( AHandle ) <> SUCCEED then
    Check;
end;

// returns True, if it's need to process regular row result (CS_ROW_RESULT)
function TIMssDataSet.HandleSpResults(AHandle: PDBPROCESS): Boolean;
var
  rcd: RETCODE;
begin
  Result := False;
  repeat
    rcd := dbresults( AHandle );

    case rcd of
      FAIL:
        begin
          Check;
          Break;	// if fails without exception
        end;
      SUCCEED:
        begin
          Result := True;
          Break;
        end;
      NO_MORE_RESULTS: InternalSpGetParams;
    end;
  until rcd = NO_MORE_RESULTS;

  FEndResults := (rcd = NO_MORE_RESULTS) or (rcd = FAIL);
end;

{ Cancels the current command batch }
procedure TIMssDataSet.HandleCancel(AHandle: PDBPROCESS);
begin
  if dbcancel( AHandle ) = FAIL then
    Check;
end;

{ Cancels the most recently executed query in the command batch }
procedure TIMssDataSet.HandleCurReset(AHandle: PDBPROCESS);
begin
  if dbcanquery( AHandle ) = FAIL then
    Check;
end;

procedure TIMssDataSet.HandleReset(AHandle: PDBPROCESS);
begin
  SrvDatabase.HandleReset( AHandle );
end;


	// Stored procedure methods
procedure TIMssDataSet.SpCreateParamDesc;
var
  sSpName, sInfoStmt, sName: string;
  DataTyp, DataLen, ColType: Integer;
  rcd: RETCODE;
  ft: TFieldType;
  pt: TSDHelperParamType;
begin
  SpAddParam( SResultName, ftInteger, ptResult );

  sSpName := ExtractStoredProcName( StoredProc.StoredProcName );
  if SrvDatabase.Database.ServerMajor >= 7 then
  	// this statement is not tested with MSSQL6
    sInfoStmt := 'select sc.name, type, sc.length, CONVERT(smallint, ((sc.status/64)&1)) ' +
  	       'from syscolumns sc ' +
	Format('where sc.id = OBJECT_ID(''%s'') and sc.number = 1', [sSpName])
  else	// for MSSQL7 st.usertype is equal 0 for nchar, ntext datatypes
    sInfoStmt := 'select sc.name, st.type, sc.length, CONVERT(smallint, ((sc.status/64)&1)) ' +
  	       'from syscolumns sc, systypes st ' +
	Format('where sc.usertype = st.usertype and sc.id = OBJECT_ID(''%s'') and sc.number = 1', [sSpName]);

  HandleReset( SrvDatabase.DBProcPtr );
  dbcmd( SrvDatabase.DBProcPtr, PChar(sInfoStmt) );
  DbExecHandle( SrvDatabase.DBProcPtr );
  if dbbind( SrvDatabase.DBProcPtr, 2,
  	SrvDatabase.NativeDataType(ftInteger),
        SrvDatabase.NativeDataSize(ftInteger), LPBYTE(@DataTyp) ) = FAIL
  then
    Check;
  if dbbind( SrvDatabase.DBProcPtr, 3,
  	SrvDatabase.NativeDataType(ftInteger),
        SrvDatabase.NativeDataSize(ftInteger), LPBYTE(@DataLen) ) = FAIL
  then
    Check;
  if dbbind( SrvDatabase.DBProcPtr, 4,
  	SrvDatabase.NativeDataType(ftInteger),
        SrvDatabase.NativeDataSize(ftInteger), LPBYTE(@ColType) ) = FAIL
  then
    Check;

  rcd := dbnextrow( SrvDatabase.DBProcPtr );
	//  rcd = NO_MORE_ROWS or FAIL when EOF
  while rcd = REG_ROW do begin
    DataLen := dbdatlen( SrvDatabase.DBProcPtr, 1 );
    if DataLen > 0 then begin
      SetLength(sName, DataLen);
      StrLCopy( PChar(sName), PChar( dbdata( SrvDatabase.DBProcPtr, 1 ) ), DataLen );
    end;

    ft := SrvDatabase.FieldDataType(DataTyp);
    if ColType <> 0
    then pt := ptInputOutput
    else pt := ptInput;

    SpAddParam( sName, ft, pt );

    rcd := dbnextrow( SrvDatabase.DBProcPtr );
  end;
end;

procedure TIMssDataSet.SpBindParams;
begin
end;

procedure TIMssDataSet.SpGetFieldDescs(Descs: TSDFieldDescList);
var
  pFieldDesc: PSDFieldDesc;
  ft: TFieldType;
  Col, NumCols: INT;
  dbcol: TDBCOL;
begin
  if not Executed then
    InternalExecute;

  NumCols := dbnumcols( Handle );

  for Col:=1 to NumCols do begin
    FillChar(dbcol, SizeOf(dbcol), 0);
    dbcol.SizeOfStruct := SizeOf(dbcol);
    if dbcolinfo( PDBHANDLE(Handle), CI_REGULAR, Col, 0, @dbcol ) = FAIL then
      Check;

    ft := SrvDatabase.FieldDataType(dbcol.ColType);
    if ft = ftUnknown then
      DatabaseErrorFmt( SBadFieldType, [StrPas(dbcol.Name)] );

    New( pFieldDesc );
    with pFieldDesc^ do begin
      FieldName	:= StrPas(dbcol.Name);
      FieldNo	:= Col;
      DataType	:= ft;
      Size	:= dbcol.MaxLength;
      Precision	:= dbcol.Precision;
  	// if dbcol.Null <> 1 then null values are not permitted for the column (Required = True)
      Required	:= not(dbcol.Null = 1);
    end;
    Descs.Add( pFieldDesc );
  end;
end;

procedure TIMssDataSet.SpPrepareProc;
begin
  if StoredProc.Params.Count = 0 then
    SpCreateParamDesc;
end;

procedure TIMssDataSet.AllocBindBuffer;
begin
  if DataSet is TSDStoredProc then begin
    if FBindBuffer = nil then
      inherited;
  end else
    inherited;
end;

procedure TIMssDataSet.InternalSpBindParams;
var
  DataPtr: Pointer;
  i, DataLen, nOffset, MaxLen: Integer;
  sParamName: string;
  dt: TFieldType;
  status: Byte;
  rcd: RETCODE;
  dtval: DBDATETIME;
begin
  if FBindBuffer = nil then
    AllocBindBuffer;

  nOffset := 0;

  with StoredProc do begin
    for i:=0 to ParamCount-1 do begin
      DataPtr := Pointer( Integer(FBindBuffer) + nOffset );
      if Params[i].IsNull
      then DataLen := 0
      else DataLen := SpParamBindSize( Params[i] );
      if Params[i].ParamType <> ptResult then begin
        if Params[i].ParamType in [ptInputOutput, ptOutput]
        then status := DBRPCRETURN
        else status := 0;
        dt := Params[i].DataType;
        case dt of
          ftString:
            if not Params[i].IsNull then begin
          	// DataLen is always equal 255(with '\0'), but string length may be less or more then 255
              if DataLen > (Length(Params[i].AsString)+1)
              then StrMove(DataPtr, PChar(Params[i].AsString), Length(Params[i].AsString)+1)
              else StrMove(DataPtr, PChar(Params[i].AsString), DataLen);
              PChar(DataPtr)[DataLen] := #$0;
            end;
          ftInteger:
            if DataLen > 0 then DWORD(DataPtr^) := Params[i].AsInteger;
          ftSmallInt:
            if DataLen > 0 then WORD(DataPtr^) := Params[i].AsSmallInt;
          ftBoolean:
            if DataLen > 0 then
              if Params[i].AsBoolean
              then WORD(DataPtr^) := 1
              else WORD(DataPtr^) := 0;
          ftBytes,
          ftVarBytes:
            if DataLen > 0 then begin
              FillChar( DataPtr^, DataLen, 0 );
              Params[i].GetData( DataPtr );
            end;
          ftDate,
          ftTime,
          ftDateTime:
            if DataLen > 0 then begin
              DataLen := SizeOf(DBDATETIME);
              dtval := CnvtDateTimeToSQLDateTime( Params[i].AsDateTime );
              StrMove( DataPtr, @dtval, DataLen );
            end;
          ftCurrency,
          ftFloat:
            if DataLen > 0 then Double(DataPtr^) := Params[i].AsFloat;
          else
            if not IsSupportedBlobTypes(dt) then
              raise EDatabaseError.CreateFmt(SNoParameterDataType, [Params[i].Name]);
        end;
        sParamName := Params[i].Name;
        MaxLen := -1;			// for fixed length parameters
        if not(Params[i].ParamType in [ptInputOutput, ptOutput]) and
           Params[i].IsNull and (dt in [ftString, ftBytes, ftVarBytes])
        then MaxLen := 0;

        if IsBlobType(dt) then begin
          if TVarData( Params[i].Value ).VType <> varString then
            VarAsType( Params[i].Value, varString );
		// if parameter is NULL
          if Length( VarToStr(Params[i].Value) ) = 0 then MaxLen := 0;

          rcd := dbrpcparam( Handle, PChar(sParamName), 0, SrvNativeDataTypes[dt],
          		MaxLen, Length(VarToStr(Params[i].Value)),
                          LPBYTE(PChar(TVarData(Params[i].Value).VString)) );
        end else begin
          if DataLen = 0 then
            rcd := dbrpcparam( Handle, PChar(sParamName), status, SrvNativeDataTypes[dt],
          		MaxLen, DataLen, LPBYTE(DataPtr) )
          else if dt in [ftString] then
            rcd := dbrpcparam( Handle, PChar(sParamName), status, SrvNativeDataTypes[dt],
          		MaxLen, StrLen(DataPtr), LPBYTE(DataPtr) )
          else if dt in [ftBytes, ftVarBytes] then
            rcd := dbrpcparam( Handle, PChar(sParamName), status, SrvNativeDataTypes[dt],
        		MaxLen, DataLen, LPBYTE(DataPtr) )
          else
            rcd := dbrpcparam( Handle, PChar(sParamName), status, SrvNativeDataTypes[dt],
          		MaxLen, -1, LPBYTE(DataPtr) );
        end;
        if rcd <> SUCCEED then
          Check;
      end;
      Inc( nOffset, DataLen );
    end;
  end;
end;

procedure TIMssDataSet.InternalSpExecute;
var
  ErrCount: Integer;
begin
  FBindStmt := ExtractStoredProcName( StoredProc.StoredProcName );

  HandleSpInit( Handle, PChar(FBindStmt) );

  InternalSpBindParams;

  HandleSpExec( Handle );

  FEndResults	:= False;

  ErrCount := DbErrorList.Count;

  HandleSpResults( Handle );
	// show an exception, which is raised by RAISEERROR statement
  if ErrCount < DbErrorList.Count then
    Check;
end;

procedure TIMssDataSet.InternalSpGetParams;

  function CnvtBytesToStr( Buffer: Pointer; Size: Integer ): string;
  begin
    SetLength(Result, Size);
    Move(Buffer^, PChar(Result)^, Size);
  end;

var
  i: Integer;
  RetNum, DataLen: INT;
  DataPtr: Pointer;
  szRetName: PChar;
  dtDateTime: TDateTimeRec;
begin
  with StoredProc do begin
    RetNum := 1;	// the current index of the returned parameter

    for i:=0 to ParamCount-1 do begin
      if Params[i].ParamType = ptResult then begin	// i must be equal 0
       	// gets return status number of the stored procedure
        if dbhasretstat( Handle ) = DB_TRUE then
          Params[i].AsInteger := dbretstatus( Handle );
      end else if Params[i].ParamType in [ptInputOutput, ptOutput] then begin
        szRetName	:= dbretname( Handle, RetNum );
        ASSERT( Params[i].Name = szRetName );
        DataPtr		:= dbretdata( Handle, RetNum );
        if DataPtr <> nil
        then DataLen	:= dbretlen( Handle, RetNum )
        else DataLen 	:= 0;

        if DataLen > 0 then begin	// not null value
		// for datetime and float fields
          if SrvDatabase.RequiredCnvtFieldType(Params[i].DataType) then begin
        	// DateTime fields
            if IsDateTimeType( Params[i].DataType ) then begin
              dtDateTime := CnvtDBDateTime2DateTimeRec( Params[i].DataType, DataPtr, DataLen );
              Params[i].SetData( @dtDateTime );
            end else if Params[i].DataType = ftBoolean then
              Params[i].AsBoolean := WORD(DataPtr^) <> 0
            else begin
            	// Float types
              Params[i].SetData( DataPtr );
            end
          end else begin
               	// for string datatype set
              if IsRequiredSizeTypes(Params[i].DataType) then
                PChar(DataPtr)[DataLen] := #$00;

              if Params[i].DataType in [ftBytes, ftVarBytes] then begin
                Params[i].Value := CnvtBytesToStr( PChar(DataPtr), DataLen )
              end else
                Params[i].SetData( DataPtr );
          end;
        end;

        Inc(RetNum);
      end;
    end;	{ end of for ... }
  end;		{ end of with ... }
end;

procedure TIMssDataSet.SpExecute;
begin
  if (not Executed) or (dsfExecProc in DSFlags) then
    InternalSpExecute;
end;

procedure TIMssDataSet.SpExecProc;
begin
  SpExecute;

  repeat
    HandleCurReset( Handle );
  until not HandleSpResults( Handle );
end;

procedure TIMssDataSet.SpGetResults;
begin
  if FEndResults then
    Exit;
	// fetch all rows for the first result set
  DataSet.FetchAll;
	// reset(without fetching) the rest result sets
  repeat
    HandleCurReset( Handle )
  until not HandleSpResults( Handle );

  ReleaseDBHandle;
end;

function TIMssDataSet.SpNextResultSet: Boolean;
var
  bSaveFlag: Boolean;
begin
  Result := False;
  if FEndResults then
    Exit;

  HandleCurReset( Handle );

  if HandleSpResults( Handle ) then begin
	// set a flag of processing the next result set
    FNextResults := True;
    bSaveFlag := not (dsfStoredProc in DSFlags);
    try
	// to preserve connection when dataset will be closed
      if bSaveFlag then
        SetDSFlag(dsfStoredProc, True);

      DataSet.Close;
      DataSet.FieldDefs.Clear;
    	// open the next result set
      DataSet.Open;

      Result := True;
    finally
      FNextResults := False;
	// restore a flag state
      if bSaveFlag then
        SetDSFlag(dsfStoredProc, False);
    end;
  end;
end;

initialization
  hSqlLibModule := 0;
  SqlLibRefCount:= 0;
  SqlLibLock 	:= TCriticalSection.Create;

  DbErrorList	:= TMssErrorList.Create;
finalization
  // must be: DbErrorList.Count = 0, else some errors isn't processed
  DbErrorList.Free;

  while SqlLibRefCount > 0 do
    FreeSqlLib;
  SqlLibLock.Free;
end.
