//Author: Poul Bak
//Copyright � 1999 - 2004 : Bak-O-Soft (Poul Bak). All rights reserved.
//http://bak-o-soft.dk/
//Mailto:info@bak-o-soft.dk

unit PBShellFolders;

{$INCLUDE PBDefines.inc}

interface

uses
	Windows, SysUtils, ActiveX, ShlObj
	{$IFDEF COMPILER_MAX_5} ,FileCtrl {$ENDIF};

type
	TShellFolderPath = type string;

	EShellFolderException = Exception;

	TFolderAttributes = (atBrowsable, atCanCopy, atCanDelete, atCanLink, atCanMoniker, atCanMove, atCanRename,
		atCompressed, atDropTarget, atEncrypted, atFileSysAncestor, atFileSystem, atFolder, atGhosted,
		atHasPropSheet, atHasSubFolder, atHidden, atHasStorage, atIsSlow, atLink, atNewContent, atNonEnumerated,
		atReadOnly, atRemovable, atShare, atStorage, atStorageAncestor, atStream);
	TFolderAttributeSet = set of TFolderAttributes;

const
	CSIDL_TEMP = -1;
	CSIDL_MYDOCUMENTS = $0C;
	CSIDL_FLAG_PER_USER_INIT = $0800;
	CSIDL_FLAG_NO_ALIAS = $1000;
	CSIDL_FLAG_DONT_VERIFY = $4000;
	CSIDL_FLAG_CREATE = $8000;
	CSIDL_FLAG_MASK = $FF00;
	MAX_SHELLFOLDERNAMES = $3D;
	ShellFolderNames : array[CSIDL_TEMP..MAX_SHELLFOLDERNAMES] of TShellFolderPath =
		('%CSIDL_TEMP%', '%CSIDL_DESKTOP%',	'%CSIDL_INTERNET%', '%CSIDL_PROGRAMS%',
		'%CSIDL_CONTROLS%', '%CSIDL_PRINTERS%',	'%CSIDL_PERSONAL%',
		'%CSIDL_FAVORITES%', '%CSIDL_STARTUP%', '%CSIDL_RECENT%',	'%CSIDL_SENDTO%',
		'%CSIDL_BITBUCKET%', '%CSIDL_STARTMENU%',	'%CSIDL_MYDOCUMENTS%',
		'%CSIDL_MYMUSIC%', '%CSIDL_MYVIDEO%', '%%',	'%CSIDL_DESKTOPDIRECTORY%',
		'%CSIDL_DRIVES%', '%CSIDL_NETWORK%', '%CSIDL_NETHOOD%', '%CSIDL_FONTS%',
		'%CSIDL_TEMPLATES%', '%CSIDL_COMMON_STARTMENU%', '%CSIDL_COMMON_PROGRAMS%',
		'%CSIDL_COMMON_STARTUP%',	'%CSIDL_COMMON_DESKTOPDIRECTORY%', '%CSIDL_APPDATA%',
		'%CSIDL_PRINTHOOD%', '%CSIDL_LOCAL_APPDATA%',	'%CSIDL_ALTSTARTUP%',
		'%CSIDL_COMMON_ALTSTARTUP%', '%CSIDL_COMMON_FAVORITES%',
		'%CSIDL_INTERNET_CACHE%', '%CSIDL_COOKIES%', '%CSIDL_HISTORY%',
		'%CSIDL_COMMON_APPDATA%',	'%CSIDL_WINDOWS%', '%CSIDL_SYSTEM%',
		'%CSIDL_PROGRAM_FILES%',	'%CSIDL_MYPICTURES%', '%CSIDL_PROFILE%',
		'%CSIDL_SYSTEMX86%',	'%CSIDL_PROGRAM_FILESX86%',
		'%CSIDL_PROGRAM_FILES_COMMON%',	'%CSIDL_PROGRAM_FILES_COMMONX86%',
		'%CSIDL_COMMON_TEMPLATES%', '%CSIDL_COMMON_DOCUMENTS%',
		'%CSIDL_COMMON_ADMINTOOLS%', '%CSIDL_ADMINTOOLS%', '%CSIDL_CONNECTIONS%',
		'%%', '%%', '%%', '%CSIDL_COMMON_MUSIC%', '%CSIDL_COMMON_PICTURES%',
		'%CSIDL_COMMON_VIDEO%', '%CSIDL_RESOURCES%', '%CSIDL_RESOURCES_LOCALIZED%',
		'%CSIDL_COMMON_OEM_LINKS%', '%CSIDL_CDBURN_AREA%', '%%',
		'%CSIDL_COMPUTERSNEARME%');
	SFGAO_CANMONIKER = $400000;
	SFGAO_ENCRYPTED = $2000;
	SFGAO_HASSTORAGE = $400000;
	SFGAO_ISSLOW = $4000;
	SFGAO_STORAGE = $8;
	SFGAO_STORAGEANCESTOR = $800000;
	SFGAO_STREAM = $400000;
	FolderAttrArray : array[TFolderAttributes] of Cardinal = (SFGAO_BROWSABLE, SFGAO_CANCOPY, SFGAO_CANDELETE,
		SFGAO_CANLINK, SFGAO_CANMONIKER, SFGAO_CANMOVE, SFGAO_CANRENAME, SFGAO_COMPRESSED, SFGAO_DROPTARGET,
		SFGAO_ENCRYPTED, SFGAO_FILESYSANCESTOR, SFGAO_FILESYSTEM, SFGAO_FOLDER, SFGAO_GHOSTED, SFGAO_HASPROPSHEET,
		SFGAO_HASSUBFOLDER, SFGAO_HIDDEN, SFGAO_HASSTORAGE, SFGAO_ISSLOW, SFGAO_LINK, SFGAO_NEWCONTENT,
		SFGAO_NONENUMERATED, SFGAO_READONLY, SFGAO_REMOVABLE, SFGAO_SHARE, SFGAO_STORAGE, SFGAO_STORAGEANCESTOR,
		SFGAO_STREAM);

function BindToParent(PIDL : PItemIDList; var RelPIDL : PItemIDList): Pointer;
function BuildShellFolderPath(const Path : string) : TShellFolderPath; overload;
function BuildShellFolderPath(const PIDL : PItemIDList) : TShellFolderPath; overload;
function DisplayPath(const Path: string) : string;
function ExpandShellFolderPath(const ShellFolderPath : TShellFolderPath;
	const ForceCreate : Boolean; const ReturnFullPath : Boolean = True;
	const ReturnPrettyCase : Boolean = True) : string;
function ExpandShellFolderPIDL(const ShellFolderPath : TShellFolderPath;
	const ForceCreate : Boolean;
	const PatchMyDocuments : Boolean = False) : PItemIDList;
function FullPath(const Path: string) : string;
function GetFolderAttributes(const Attr : Cardinal) : TFolderAttributeSet;
function GetRelDisplay(RelShellFolder : IShellFolder; RelPIDL : PItemIDList; ForParsing : Boolean) : string;
procedure GetPathDisplayAndAttr(PIDL : PItemIDList; var Path, Display : string;
	var Attr : TFolderAttributeSet; const Pretty : Boolean = True);
function GetShellFolderPath(const CSIDL : integer;
	const ForceCreate : Boolean) : TShellFolderPath;
function GetShortcutTarget(var PIDL : PItemIDList) : Boolean;
function PrettyCase(const Path: string;
	const SpaceSeparates : Boolean = True): string;
function SHGetIDListFromPath(const Path : string) : PItemIDList;
procedure FreeItemIDList(PIDL : PItemIDList);


implementation

type
	TSHGetFolderPathA = function(HwndOwner : HWND; nFolder : LongInt;
		hToken : THandle;	dwFlags : DWord; PPath : PChar) : HRESULT; stdcall;
	TSHGetFolderPathW = function(HwndOwner : HWND; nFolder : LongInt;
		hToken : THandle;	dwFlags : DWord; PPath : PWideChar) : HRESULT; stdcall;
	TSHGetFolderLocation = function(HwndOwner : HWND; nFolder : LongInt;
		hToken : THandle; dwReserved : Cardinal;
		var PIDL : PItemIDList) : HRESULT; stdcall;

var
	SHFolderHandle, Shell32Handle : THandle;
	SHGetFolderPathA : TSHGetFolderPathA;
	SHGetFolderPathW : TSHGetFolderPathW;
	SHGetFolderLocation : TSHGetFolderLocation;

function BindToParent(PIDL : PItemIDList; var RelPIDL : PItemIDList): Pointer;
type
	TSHBindToParent = function(const PIDL : PItemIDList; const Riid : TGUID;
		out ppvOut; out RelPIDL : PItemIDList) : HResult; stdcall;
var
	Old_cb : Word;
	APIDL: PItemIDList;
	Desktop: IShellFolder;
	LibHandle : THandle;
	SHBindToParent : TSHBindToParent;
begin
	Result := nil;
	APIDL := PIDL;
	RelPIDL := PIDL;
	if Assigned(PIDL) then
	begin
		if (Win32MajorVersion >= 5) then
		begin
			LibHandle := GetModuleHandle('Shell32.dll');
			if LibHandle <> 0 then
			begin
				@SHBindToParent := GetProcAddress(LibHandle, 'SHBindToParent');
				if @SHBindToParent <> nil
					then SHBindToParent(PIDL, IID_IShellFolder, Result, RelPIDL);
				if Result = nil then SHGetDesktopFolder(IShellFolder(Result));
			end;
		end;
		if (Result = nil) or (RelPIDL = nil) then
		begin
			while (APIDL.mkid.cb <> 0) do
			begin
				RelPIDL := APIDL;
				Inc(PChar(APIDL), Integer(APIDL.mkid.cb));
			end;
			Old_cb := RelPIDL.mkid.cb;
			RelPIDL.mkid.cb := 0;
			try
				SHGetDesktopFolder(Desktop);
				Desktop.BindToObject(PIDL, nil, IID_IShellFolder, Pointer(Result));
				if Result = nil then SHGetDesktopFolder(IShellFolder(Result));
			finally
				RelPIDL.mkid.cb := Old_cb;
			end;
		end;
	end;
end;

function BuildShellFolderPath(const Path : string) : TShellFolderPath;
var
	CSIDL, Len, ResultLen : integer;
	TempShellFolder : TShellFolderPath;
begin
	Result := Path;
	ResultLen := 0;
	for CSIDL := CSIDL_TEMP to MAX_SHELLFOLDERNAMES do
	begin
		TempShellFolder := GetShellFolderPath(CSIDL, False);
		Len := Length(TempShellFolder);
		if (Uppercase(TempShellFolder) = Uppercase(Copy(Path, 1, Len)))
			and (Len > ResultLen) then
		begin
			Result := ShellFolderNames[CSIDL] + Copy(Path, Len + 1, MAX_PATH);
			ResultLen := Len;
		end;
	end;
end;

function BuildShellFolderPath(const PIDL : PItemIDList) : TShellFolderPath;
var
	TempPath : array[0..MAX_PATH] of Char;
begin
	SHGetPathFromIDList(PIDL, TempPath);
	Result := BuildShellFolderPath(TempPath);
end;

function DisplayPath(const Path: string) : string;
begin
	if Path = '' then Result := ''
	else if Length(ExtractFileDrive(Path)) = Length(Path)
		then Result := Path + '\'
	else if (Length(ExtractFileDrive(Path)) = Length(Path) - 1)
		and (Path[Length(Path)] = '\')
		then Result := Path
	else if Path[Length(Path)] = '\'
		then Result := Copy(Path, 1, Length(Path) - 1)
	else Result := Path;
end;

function GetCSIDL(const ShellFolderName : TShellFolderPath) : integer;
var
	t : integer;
begin
	for t := CSIDL_TEMP to MAX_SHELLFOLDERNAMES do
	begin
		if UpperCase(ShellFolderName) = UpperCase(ShellFolderNames[t]) then
		begin
			Result := t;
			Exit;
		end;
	end;
	Raise EShellFolderException.Create(ShellFolderName
				+ ' is not a valid shellfoldername !');
end;

function ExpandShellFolderPath(const ShellFolderPath : TShellFolderPath;
	const ForceCreate : Boolean; const ReturnFullPath : Boolean = True;
	const ReturnPrettyCase : Boolean = True) : string;
var
	Drive, EndPath : string;
	ShellName : TShellFolderPath;
	Endpos, CSIDL : integer;
begin
	Drive := ExtractFileDrive(ShellFolderPath);
	if (Drive <> '') and DirectoryExists(Drive + '\') then
	begin
		if ForceCreate and (not FileExists(ShellFolderPath))
			then ForceDirectories(ShellFolderPath);
		Result := ShellFolderPath;
	end
	else if (ShellFolderPath = '') or (ShellFolderPath[1] <> '%')
		then Result := ShellFolderPath
	else
	begin
		EndPos := Pos('%', Copy(ShellFolderPath, 2, Length(ShellFolderPath) - 1));
		if (EndPos = 0) then Result := ShellFolderPath
		else
		begin
			ShellName := Copy(ShellFolderPath, 1, EndPos + 1);
			CSIDL := GetCSIDL(ShellName);
			Result := GetShellFolderPath(CSIDL, ForceCreate);
			EndPath := Copy(ShellFolderPath, Length(ShellName) + 1, MAX_Path);
			if Copy(EndPath, 1, 1) = '\' then Delete(EndPath, 1, 1);
			Result := DisplayPath(FullPath(Result) + EndPath);
			if ForceCreate and (Result <> '') and (not FileExists(Result))
				then ForceDirectories(Result);
		end;
	end;
	if ReturnFullPath then Result := FullPath(Result);
	if ReturnPrettyCase then Result := PrettyCase(Result);
end;

function GetMyDocuments(const PatchMyDocuments : Boolean) : PItemIDList;
const
	GUIDPersonal = '::{450d8fba-ad25-11d0-98a8-0800361b1103}';
var
	DeskTop : IShellFolder;
	EnumIDList : IEnumIDList;
	Dummy : Cardinal;
	Success : Boolean;
	HResult : integer;
	PersonalPath : TShellFolderPath;
	TempPath: array[0..MAX_PATH] of Char;
begin
// This is a patch for Windows versions earlier than XP!
// The virtual folder CSIDL_MYDOCUMENTS is not defined in Shell32.dll
// but it still works in 'NewDialogStyle' or if it's visible on the desktop.
// Otherwise get the PIDL to CSIDL_PERSONAL (so it's safe to use).
	Result := nil;
	Success := False;
	PersonalPath := GetShellFolderPath(CSIDL_PERSONAL, True);
	SHGetDesktopFolder(DeskTop);
	if DeskTop.EnumObjects(0, SHCONTF_FOLDERS or SHCONTF_NONFOLDERS
		or SHCONTF_INCLUDEHIDDEN, EnumIDList) = NO_ERROR then
	begin
		EnumIDList.Reset;
		Success := False;
		repeat
			HResult := EnumIDList.Next(1, Result, Dummy);
			if HResult = NO_ERROR then
			begin
				if SHGetPathFromIDList(Result, TempPath)
					and (TempPath = PersonalPath) then Success := True
				else CoTaskMemFree(Result);
			end;
		until Success or (HResult <> NO_ERROR);
	end;
	if Success then Exit
	else if PatchMyDocuments
		then Result := SHGetIDListFromPath(GetShellFolderPath(CSIDL_PERSONAL, True))
	else Result := SHGetIDListFromPath(GUIDPersonal);
end;

function ExpandShellFolderPIDL(const ShellFolderPath : TShellFolderPath;
	const ForceCreate : Boolean;
	const PatchMyDocuments : Boolean = False) : PItemIDList;
var
	CSIDL : integer;
begin
	Result := nil;
	if ShellFolderPath = '' then Exit
	else if (ShellFolderPath[1] = '%')
		and (ShellFolderPath[Length(ShellFolderPath)] = '%') then
	begin
		CSIDL := GetCSIDL(ShellFolderPath);
		if (@SHGetFolderLocation <> nil) and (SHGetFolderLocation(0,
			CSIDL, 0, 0, Result) = NO_ERROR) then Exit
		else if SHGetSpecialFolderLocation(0, CSIDL, Result) = NO_ERROR
			then Exit;
		if (Result = nil) and (CSIDL = CSIDL_MYDOCUMENTS)
			then Result := GetMyDocuments(PatchMyDocuments);
	end;
	if(Result = nil)
		then Result := SHGetIDListFromPath(ExpandShellFolderPath(ShellFolderPath,
		ForceCreate));
end;

function FullPath(const Path: string) : string;
begin
	if Path = '' then Result := ''
	else if Path[Length(Path)] = '\' then Result := Path
	else Result := Path + '\';
end;

function GetFolderAttributes(const Attr : Cardinal) : TFolderAttributeSet;
var
	t : TFolderAttributes;
begin
	Result := [];
	for t := Low(TFolderAttributes) to High(TFolderAttributes) do
	begin
		if (FolderAttrArray[t] and Attr) = FolderAttrArray[t] then Include(Result, t);
	end;
end;

function GetRelDisplay(RelShellFolder : IShellFolder; RelPIDL : PItemIDList; ForParsing : Boolean) : string;
const
	Flags : array[Boolean] of Cardinal = (SHGDN_NORMAL, SHGDN_FORPARSING);
var
	lpDisplay : TStrRet;
	TempPath: array[0..MAX_PATH] of Char;
begin
	ZeroMemory(@lpDisplay, SizeOf(lpDisplay));
	lpDisplay.uType := STRRET_CSTR;
	RelShellFolder.GetDisplayNameOf(RelPIDL, Flags[ForParsing] or SHGDN_INCLUDE_NONFILESYS, lpDisplay);
	case lpDisplay.uType of
		STRRET_CSTR : Result := StrPas(lpDisplay.cStr);
		STRRET_WSTR :
		begin
			ZeroMemory(@TempPath, SizeOf(TempPath));
			if WideCharToMultiByte(CP_ACP, 0, lpDisplay.pOleStr, -1, TempPath,
				MAX_PATH, nil, nil) <> 0 then Result := StrPas(TempPath);
			CoTaskMemFree(lpDisplay.pOleStr);
		end;
		STRRET_OFFSET :
		begin
			SetString(Result, PChar(RelPIDL) + lpDisplay.uOffset,
				RelPIDL.mkid.cb - lpDisplay.uOffset);
		end;
	end;
end;

procedure GetPathDisplayAndAttr(PIDL : PItemIDList;
	var Path, Display : string; var Attr : TFolderAttributeSet; const Pretty : Boolean = True);
var
	TempPath: array[0..MAX_PATH] of Char;
	RelPIDL: PItemIDList;
	RelShellFolder : IShellFolder;
	TempAttr : Cardinal;
	TempDisplay : string;
begin
	TempPath := #0;
	SHGetPathFromIDList(PIDL, TempPath);
	Path := StrPas(TempPath);
	Display := Path;
	RelShellFolder := IShellFolder(BindToParent(PIDL, RelPIDL));
	if (RelShellFolder = nil) or (RelPIDL = nil) then Exit;
	Display := GetRelDisplay(RelShellFolder, RelPIDL, False);
	if Path = '' then
	begin
		TempDisplay := GetRelDisplay(RelShellFolder, RelPIDL, True);
		if (Copy(TempDisplay, 1, 2) = '\\') then Path := TempDisplay
		else if Pos('://', TempDisplay) <> 0 then Display := TempDisplay;
	end;
	TempAttr := $FFFFFFFF and not SFGAO_VALIDATE;
	RelShellFolder.GetAttributesOf(1, RelPIDL, TempAttr);
	Attr :=  GetFolderAttributes(TempAttr);
	if Pretty then
	begin
		Path := PrettyCase(Path);
		Display := PrettyCase(Display);
	end;
end;

function GetShellFolderPath(const CSIDL : integer;
	const ForceCreate : Boolean) : TShellFolderPath;
var
	TempPath : array[0..MAX_PATH] of Char;
	WidePath : array[0..MAX_PATH] of WideChar;
	Flag : integer;
begin
	Result := '';
	ZeroMemory(@TempPath, SizeOf(TempPath));
	ZeroMemory(@WidePath, SizeOf(WidePath));
	if (CSIDL < CSIDL_TEMP) or (CSIDL > MAX_SHELLFOLDERNAMES)
		then Raise EShellFolderException.Create(IntToStr(CSIDL)
				+ ' is not a valid shellfolder identifier !');
	if CSIDL = CSIDL_TEMP then
	begin
		GetTempPath(MAX_PATH, TempPath);
		Result := DisplayPath(TempPath);
	end
	else
	begin
		if ForceCreate then Flag := CSIDL or CSIDL_FLAG_CREATE
		else Flag := CSIDL;
		if @SHGetFolderPathA <> nil then
		begin
			SHGetFolderPathA(0, Flag, 0, 0, TempPath);
			Result := TempPath;
		end
		else if @SHGetFolderPathW <> nil then
		begin
			SHGetFolderPathW(0, Flag, 0, 0, WidePath);
			if WideCharToMultiByte(CP_ACP, 0, WidePath, -1, TempPath,
					MAX_PATH, nil, nil) <> 0 then Result := StrPas(TempPath);
		end;
		if (Result = '') and (CSIDL = CSIDL_MYDOCUMENTS)
			then Result := GetShellFolderPath(CSIDL_PERSONAL, ForceCreate);
	end;
end;

function GetShortcutTarget(var PIDL : PItemIDList) : Boolean;
var
	TempPath: array[0..MAX_PATH] of Char;
	TempWidePath : array[0..MAX_PATH] of WideChar;
	ShellLink : IShellLink;
	PersistFile : IPersistFile;
	RelPIDL: PItemIDList;
	RelShellFolder : IShellFolder;
	Attr : Cardinal;
begin
	Result := False;
	if PIDL = nil then Exit;
	TempPath := #0;
	SHGetPathFromIDList(PIDL, TempPath);
	if (TempPath <> '') and (CoCreateInstance(CLSID_ShellLink, nil,
		CLSCTX_INPROC_SERVER, IID_IShellLinkA, ShellLink) = S_OK) then
	begin
		if ShellLink.QueryInterface(IPersistFile, PersistFile) = S_OK then
		begin
			MultiByteToWideChar(CP_ACP, 0, TempPath, -1, TempWidePath, MAX_PATH);
			if (PersistFile.Load(TempWidePath, STGM_READ) = S_OK)
				and (ShellLink.GetIDList(PIDL) = S_OK) and (PIDL <> nil) then
			begin
				RelShellFolder := IShellFolder(BindToParent(PIDL, RelPIDL));
				if (RelShellFolder <> nil) and (RelPIDL <> nil) then
				begin
					Attr := High(Cardinal);
					RelShellFolder.GetAttributesOf(1, RelPIDL, Attr);
					if ((Attr and SFGAO_FOLDER) <> 0) then Result := True
					else CoTaskMemFree(PIDL);
				end;
			end;
		end;
	end;
end;

function PrettyCase(const Path : string; const SpaceSeparates : Boolean = True): string;
var
	S1, S2, Sep : string;
	Flag : boolean;
	t : integer;
begin
	Sep := '.,;-+*/?=()&!\<>:_"{|}[]%';
	if SpaceSeparates then Sep := ' ' + Sep;
	if Pos(':/', Path) > 0 then Result := Path
	else
	begin
		Flag := True;
		Result := '';
		for t := 1 to Length(Path) do
		begin
			S1 := Path[t];
			if Flag then Result := Result + AnsiUpperCase(S1)
			else Result := Result + AnsiLowerCase(S1);
			if Pos(S1, Sep) <> 0 then Flag := True
			else Flag := False;
		end;
		S2 := ExtractFileExt(Result);
		if S2 <> '' then Result := ChangeFileExt(Result, AnsiLowerCase(S2));
	end;
end;

function SHGetIDListFromPath(const Path : string) : PItemIDList;
var
	ShellFolder : IShellFolder;
	WidePath : WideString;
	chEaten, dwAttributes : Cardinal;
begin
	Result := nil;
	if SHGetDesktopFolder(ShellFolder) = NO_ERROR then
	begin
		WidePath := Path;
		ShellFolder.ParseDisplayName(0, nil, PWideChar(WidePath),
			chEaten, Result, dwAttributes);
	end;
end;

procedure FreeItemIDList(PIDL : PItemIDList);
begin
	if PIDL <> nil then CoTaskMemFree(PIDL);
end;

//  ---------------  initialization  ----------------------------
initialization
	CoInitialize(nil);
	SHFolderHandle := LoadLibrary('SHFolder.dll');
	if SHFolderHandle <> 0 then
	begin
		@SHGetFolderPathA := GetProcAddress(SHFolderHandle,	'SHGetFolderPathA');
		@SHGetFolderPathW := GetProcAddress(SHFolderHandle,	'SHGetFolderPathW');
	end;
	Shell32Handle := GetModuleHandle('Shell32.dll');
	if Shell32Handle <> 0 then
	begin
		@SHGetFolderLocation := GetProcAddress(Shell32Handle, 'SHGetFolderLocation');
	end;

//  ----------------  finalization  -----------------------------
finalization
	if SHFolderHandle <> 0 then FreeLibrary(SHFolderHandle);
	SHFolderHandle := 0;
	Shell32Handle := 0;
	CoUnInitialize;

end.
