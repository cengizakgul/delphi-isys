//Author: Poul Bak
//Copyright � 1999 - 2007 : Bak-O-Soft (Poul Bak). All rights reserved.
//http://bak-o-soft.dk/
//Mailto:info@bak-o-soft.dk

{$INCLUDE PBDefines.inc}

unit PBFolderDialog;

interface

uses
	Windows, SysUtils, Messages, Classes, Forms, Dialogs, ActiveX, ShlObj,
	FileCtrl, Controls, Graphics, PBShellFolders;

const
	IID_IFolderFilterSite : TGUID = (D1: $C0A651F5; D2: $B48B; D3: $11D2; D4: ($B5, $ED, $00, $60, $97, $C6, $86, $F6));
	IID_IFolderFilter : TGUID = (D1: $9CC22886; D2: $DC8E; D3: $11D2; D4: ($B1, $D0, $00, $C0, $4F, $8E, $EB, $3E));
	SID_IFolderFilterSite = '{C0A651F5-B48B-11d2-B5ED-006097C686F6}';
	SID_IFolderFilter = '{9CC22886-DC8E-11d2-B1D0-00C04F8EEB3E}';

type
	IFolderFilterSite = interface(IUnknown)
		[SID_IFolderFilterSite]
		function SetFilter(PUnk : IUnknown): HResult; stdcall;
	end;

	IFolderFilter = interface(IUnknown)
		[SID_IFolderFilter]
		function ShouldShow(ShellFolder : IShellFolder; ParentPIDL, ItemPIDL: PItemIDList): HResult; stdcall;
		function GetEnumFlags(ShellFolder : IShellFolder; PIDL: PItemIDList; const ParentWindow : HWND;
			var EnumFlags : DWORD): HResult; stdcall;
	end;

	PItemIDList = ShlObj.PItemIDList;
	IShellFolder = ShlObj.IShellFolder;

	TBrowseFor = (bfAll, bfOnlyAncestors, bfOnlyComputers, bfOnlyDomains,
		bfOnlyFileSystem, bfOnlyPrinters);

	TBrowseInfoFlags = (bfShowPath, bfEditBox, bfIncludefiles, bfIncludeURLs,
		bfNewDialogStyle, bfUsageHint, bfHideNewFolderButton, bfDontBrowseShortcuts,
		bfHideMappedDrives, bfValidate);
	TBrowseInfoFlagSet = set of TBrowseInfoFlags;

	TExtraFlags = (efAutoExpandTree, efUsePrettyCase);
	TExtraFlagSet = set of TExtraFlags;

	TFolderAttributes = PBShellFolders.TFolderAttributes;
	TFolderAttributeSet = PBShellFolders.TFolderAttributeSet;

const
	DefaultBrowseFlags = [bfShowPath, bfNewDialogStyle];
	DefaultExtraFlags = [efAutoExpandTree, efUsePrettyCase];
	atBrowseable = PBShellFolders.atBrowsable;
	atCanCopy = PBShellFolders.atCanCopy;
	atCanDelete = PBShellFolders.atCanDelete;
	atCanLink = PBShellFolders.atCanLink;
	atCanMoniker = PBShellFolders.atCanMoniker;
	atCanMove = PBShellFolders.atCanMove;
	atCanRename = PBShellFolders.atCanRename;
	atCompressed = PBShellFolders.atCompressed;
	atDropTarget = PBShellFolders.atDropTarget;
	atEncrypted = PBShellFolders.atEncrypted;
	atFileSysAncestor = PBShellFolders.atFileSysAncestor;
	atFileSystem = PBShellFolders.atFileSystem;
	atFolder = PBShellFolders.atFolder;
	atGhosted = PBShellFolders.atGhosted;
	atHasPropSheet = PBShellFolders.atHasPropSheet;
	atHasSubFolder = PBShellFolders.atHasSubFolder;
	atHidden = PBShellFolders.atHidden;
	atIsSlow = PBShellFolders.atIsSlow;
	atLink = PBShellFolders.atLink;
	atNewContent = PBShellFolders.atNewContent;
	atNonEnumerated = PBShellFolders.atNonEnumerated;
	atReadOnly = PBShellFolders.atReadOnly;
	atRemovable = PBShellFolders.atRemovable;
	atShare = PBShellFolders.atShare;
	atStorage = PBShellFolders.atStorage;
	atStorageAncestor = PBShellFolders.atStorageAncestor;
	atStream = PBShellFolders.atStream;

type
	TPBFolderDialog = class;

	TBrowserInitializedEvent=procedure(Sender: TPBFolderDialog; DialogHandle: HWND) of object;
	TSelectionChangedEvent=procedure(Sender: TPBFolderDialog; DialogHandle: HWND;
		const ItemIDList: PItemIDList; const Folder: String; const Attr : TFolderAttributeSet) of object;
	TValidateFailedEvent=procedure(Sender: TPBFolderDialog;
		DialogHandle: HWND; var CloseDialog : Boolean) of object;
	TXPIncludeItemEvent=procedure(Sender: TPBFolderDialog; DialogHandle: HWND; const ShellFolder : IShellFolder;
		const ParentPIDL, ItemPIDL: PItemIDList; const Folder : String; const Attr : TFolderAttributeSet;
		var IncludeItem : Boolean) of object;

	TPBFolderDialog = class(TComponent, IFolderFilter)
	private
		FDialogHandle, FNewFolderHandle, FParentHandle : HWnd;
		FPathHandle, FListhandle, FSelecteditem : HWnd;
		FSelectedFolder, FRestartFolder, FDisplayName : string;
		FFolder, FRootFolder : TShellFolderPath;
		FImageIndex : Integer;
		FFolderAttr : TFolderAttributeSet;
		FBrowseFor : TBrowseFor;
		FExtraFlags : TExtraFlagSet;
		FFlags : TBrowseInfoFlagSet;
		FInitialized : Boolean;
		POldWndProc, PNewWndProc : Pointer;
		FNewFolderCaption, FVersion, FLocale, FPrimaryLocale, FLabelCaption : string;
		FRestart, FValidPath, FNewStyle : Boolean;
		FNewFolderCaptions, FLabelCaptions, FTitles : TStringList;
		PStartItemIDList : PItemIDList;
		FOnInitialized : TBrowserInitializedEvent;
		FOnSelectionChanged : TSelectionChangedEvent;
		FOnValidateFailed : TValidateFailedEvent;
		FOnXPIncludeItem : TXPIncludeItemEvent;
		function GetEnumFlags(ShellFolder : IShellFolder; PIDL: PItemIDList; const ParentWindow : HWND;
			var EnumFlags : DWORD): HResult; stdcall;
		function GetListHandle(HWindow : HWnd) : HWnd;
		function GetSystemLang : string;
		function HookMain(var Msg : TMessage): Boolean;
		function LocaleText(List : TStringList) : string;
		function ShouldShow(ShellFolder : IShellFolder; ParentPIDL, ItemPIDL: PItemIDList): HResult; stdcall;
		function StoreNewFolderCaptions : Boolean;
		function StoreCurrentFolderCaptions : Boolean;
		procedure AddNewFolderButton;
		procedure BrowseCallBackProc(var Msg : TMessage);
		procedure CenterWindow(HWindow: HWND);
		procedure Dummy(Value: String);
		procedure LabelCaptionsChange (Sender: TObject);
		procedure NewFolderCaptionsChange (Sender: TObject);
		procedure SetNewFolderCaption(Value : String);
		procedure SetNewFolderCaptions(Value : TStringList);
		procedure SetSelectedFolder(Value : String);
		procedure SetLabelCaptions(Value : TStringList);
		procedure SetTitles(Value : TStringList);
		procedure ShowLabelPath;
		procedure WndProc(var Msg : TMessage);
	public
		constructor Create(AOwner : TComponent); override;
		destructor Destroy; override;
		function Execute : Boolean; overload;
		function Execute(var ItemIDList : PItemIDList;
			const FreeStartPIDL : Boolean = True) : Boolean; overload;
		procedure EnableOK(const Hwindow : HWND; const Value : Boolean);
		procedure Loaded; override;
		procedure SetExpanded(const HWindow : HWnd; const ItemIdList : PItemIDList;
			const Expand : Boolean);
		procedure SetSelectionPIDL(const Hwindow : HWND; const ItemIDList : PItemIDList);
		procedure SetSelectionPath(const Hwindow : HWND; const Path : String);
		property DialogHandle : HWnd read FDialogHandle;
		property DisplayName : String read FDisplayName;
		property FolderAttributes : TFolderAttributeSet read FFolderAttr;
		property ImageIndex : Integer read FImageIndex;
		property ParentHandle : HWnd read FParentHandle write FParentHandle;
		property SelectedFolder : String read FSelectedFolder write SetSelectedFolder;
	published
		property BrowseFor : TBrowseFor read FBrowseFor
			write FBrowseFor default bfOnlyFileSystem;
		property ExtraFlags : TExtraFlagSet read FExtraFlags
			write FExtraFlags default DefaultExtraFlags;
		property Folder : TShellFolderPath read FFolder write FFolder;
		property Flags : TBrowseInfoFlagSet read FFlags
			write FFlags default DefaultBrowseFlags;
		property RootFolder : TShellFolderPath read FRootFolder write FRootFolder;
		property OnInitialized : TBrowserInitializedEvent read FOnInitialized
			write FOnInitialized;
		property OnSelectionChanged : TSelectionChangedEvent read FOnSelectionChanged
			write FOnSelectionChanged;
		property OnValidateFailed : TValidateFailedEvent read FOnValidateFailed
			write FOnValidateFailed;
		property OnXPIncludeItem : TXPIncludeItemEvent read FOnXPIncludeItem write FOnXPIncludeItem;
		property LabelCaptions : TStringList read FLabelCaptions
			write SetLabelCaptions stored StoreCurrentFolderCaptions;
		property NewFolderCaptions : TStringList read FNewFolderCaptions
			write SetNewFolderCaptions stored StoreNewFolderCaptions;
		property Titles : TStringList read FTitles write SetTitles;
		property Version : string read FVersion write Dummy stored False;
	end;
	
procedure FreeItemIDList(PIDL : PItemIDList);

implementation

const
	ComponentVersion = '10.00.00.00';
	MinButtonWidth = 75;
	_BUTTON_ID = 255;
	BFFM_VALIDATEFAILED = $03;
	BFFM_IUNKNOWN = $05;
	BFFM_SETEXPANDED = (WM_USER + 106);
	PB_SHOWLABEL = WM_USER + 5633;
	BIF_EDITBOX = $10;
	BIF_VALIDATE = $20;
	BIF_NEWDIALOGSTYLE = $40;
	BIF_BROWSEINCLUDEURLS = $80;
	BIF_UAHINT = $100;
	BIF_NONEWFOLDERBUTTON = $200;
	BIF_NOTRANSLATETARGETS = $400;
	BIF_SHAREABLE = $8000;
	MS_FLAGS_ARRAY : array[TBrowseFor] of Cardinal = (0, BIF_RETURNFSANCESTORS,
		BIF_BROWSEFORCOMPUTER, BIF_DONTGOBELOWDOMAIN, BIF_RETURNONLYFSDIRS,
		BIF_BROWSEFORPRINTER);
	BROWSE_FLAG_ARRAY : array[TBrowseInfoFlags] of Cardinal = (BIF_STATUSTEXT,
		BIF_EDITBOX, BIF_BROWSEINCLUDEFILES, BIF_BROWSEINCLUDEURLS,
		BIF_NEWDIALOGSTYLE, BIF_UAHINT, BIF_NONEWFOLDERBUTTON,
		BIF_NOTRANSLATETARGETS, BIF_SHAREABLE, BIF_VALIDATE);
	NEW_FOLDER_CAPTIONS =	'"Default=New folder","0006=Ny mappe",'
		+ '"0007=Neuer Ordner","0009=New folder","000C=Nouveau dossier",'
		+ '"0010=Nuova Cartella","0013=Nieuwe map","0015=Nowy folder",'
		+ '"0016=Nova Pasta","0406=Ny mappe","0407=Neuer Ordner","0409=New folder",'
		+ '"040C=Nouveau dossier","0410=Nuova Cartella","0413=Nieuwe map",'
		+ '"0415=Nowy folder","0416=Nova Pasta","0807=Neuer Ordner",'
		+ '"0809=New folder","080C=Nouveau dossier","0810=Nuova Cartella",'
		+ '"0C07=Neuer Ordner","0C09=New folder","0C0C=Nouveau dossier",'
		+ '"1007=Neuer Ordner","1009=New folder","100C=Nouveau dossier",'
		+ '"1407=Neuer Ordner","1409=New folder","140C=Nouveau dossier",'
		+ '"1809=New folder","180C=Nouveau dossier","1C09=New folder",'
		+ '"2009=New folder","2809=New folder","2C09=New folder"';
	CURRENT_FOLDER_CAPTIONS = '"Default=Current folder:","0006=Valgt mappe:",'
		+ '"0007=Ausgew�hlter Ordner:","0009=Current folder:","000C=Dossier courant :",'
		+ '"0010=Cartella selezionata:","0013=Huidige map:","0015=Bie��cy folder",'
		+ '"0016=Pasta Atual:","0406=Valgt mappe:","0407=Ausgew�hlter Ordner:",'
		+ '"0409=Current folder:","040C=Dossier courant :","0410=Cartella selezionata:",'
		+ '"0413=Huidige map:","0415=Bie��cy folder","0416=Pasta Atual:",'
		+ '"0807=Ausgew�hlter Ordner:","0809=Current folder:","080C=Dossier courant :",'
		+ '"0810=Cartella selezionata:","0C07=Ausgew�hlter Ordner:",'
		+ '"0C09=Current folder:","0C0C=Dossier courant :","1007=Ausgew�hlter Ordner:",'
		+ '"1009=Current folder:","100C=Dossier courant :","1407=Ausgew�hlter Ordner:",'
		+ '"1409=Current folder:","140C=Dossier courant :","1809=Current folder:",'
		+ '"180C=Dossier courant :","1C09=Current folder:","2009=Current folder:",'
		+ '"2809=Current folder:","2C09=Current folder:"';

var
	FWindowRect : TRect;

procedure FreeItemIDList(PIDL : PItemIDList);
begin
	if PIDL <> nil then CoTaskMemFree(PIDL);
end;

// -----------------------  TPBFolderDialog  ---------------------
constructor TPBFolderDialog.Create(AOwner: TComponent);
begin
	inherited Create(AOwner);
	CoInitialize(nil);
	FParentHandle := 0;
	FRootFolder := '';
	FBrowseFor := bfOnlyFileSystem;
	FExtraFlags := DefaultExtraFlags;
	FFlags := DefaultBrowseFlags;
	FFolder := '';
	FSelectedFolder := '';
	FRestartFolder := '';
	FDisplayName := '';
	PStartItemIDList := nil;
	FInitialized := False;
	FValidPath := True;
	FVersion := ComponentVersion;
	FLocale := GetSystemLang;
	FPrimaryLocale := IntToHex((StrToInt('$' + FLocale) and $3FF), 4);
	FNewFolderCaptions := TStringList.Create;
	FNewFolderCaptions.OnChange := NewFolderCaptionsChange;
	FNewFolderCaptions.CommaText := NEW_FOLDER_CAPTIONS;
	FLabelCaptions := TStringList.Create;
	FLabelCaptions.OnChange := LabelCaptionsChange;
	FLabelCaptions.CommaText := CURRENT_FOLDER_CAPTIONS;
	FTitles := TStringList.Create;
end;

procedure TPBFolderDialog.Loaded;
begin
	SetNewFolderCaption(LocaleText(FNewFolderCaptions));
	FLabelCaption := LocaleText(FLabelCaptions);
end;

destructor TPBFolderDialog.Destroy;
begin
	FTitles.Free;
	FNewFolderCaptions.Free;
	FLabelCaptions.Free;
	CoUnInitialize;
	inherited Destroy;
end;

function TPBFolderDialog.Execute: Boolean;
var
	ItemIDList : PItemIDList;
begin
	FNewStyle := (Win32MajorVersion >= 5) and (bfNewDialogStyle in FFLags);
	ItemIDList := ExpandShellFolderPIDL(FFolder, True, not FNewStyle);
	Result := Execute(ItemIDList, False);
	CoTaskMemFree(ItemIDList);
end;

function TPBFolderDialog.Execute(var ItemIDList : PItemIDList;
	const FreeStartPIDL : Boolean = True) : Boolean;
var
	BrowseInfo : TBrowseInfo;
	BrowseInfoFlags : TBrowseInfoFlags;
	OldItemIDList : PItemIDList;
begin
	Result := False;
	FNewStyle := (Win32MajorVersion >= 5) and (bfNewDialogStyle in FFLags);
	if ItemIDList = nil then ItemIDList := ExpandShellFolderPIDL(FFolder, True, not FNewStyle);
	try
		ZeroMemory(@BrowseInfo, SizeOf(TBrowseInfo));
		if IsWindow(FParentHandle) then BrowseInfo.hwndOwner:=FParentHandle
		else if (Owner is TWinControl)
			then BrowseInfo.hwndOwner:=TWinControl(Owner).Handle
		else if Assigned(Application.MainForm)
			then BrowseInfo.hwndOwner:=Application.MainForm.Handle
		else BrowseInfo.hwndOwner := Application.Handle;
		BrowseInfo.pidlRoot := ExpandShellFolderPIDL(FRootFolder, True, not FNewStyle);
		BrowseInfo.pszDisplayName := StrAlloc(MAX_PATH + 1);
		ZeroMemory(BrowseInfo.pszDisplayName, MAX_PATH + 1);
		BrowseInfo.lpszTitle := PChar(FLabelCaption);
		BrowseInfo.ulFlags := MS_FLAGS_ARRAY[FBrowseFor];
		for BrowseInfoFlags := Low(TBrowseInfoFlags) to High(TBrowseInfoFlags)
			do if BrowseInfoFlags in FFlags then BrowseInfo.ulFlags:=BrowseInfo.ulFlags
			or BROWSE_FLAG_ARRAY[BrowseInfoFlags];
		BrowseInfo.lpfn := MakeObjectInstance(BrowseCallBackProc);
		BrowseInfo.iImage := 0;
		FSelectedFolder := FFolder;
		FRestartFolder := '';
		PStartItemIDList := ItemIDList;
		OldItemIDList := ItemIDList;
		repeat
			FInitialized := False;
			FNewFolderHandle := 0;
			FListHandle := 0;
			FSelectedItem := 0;
			FPathHandle := 0;
			FDisplayName := '';
			FRestart := False;
			{SHBrowseForFolder; return is nil if user cancels}
			ItemIDList := SHBrowseForFolder(BrowseInfo);
			FreeObjectInstance(PNewWndProc);
			Application.UnHookMainWindow(HookMain);
		until not FRestart;
		if ItemIDList <> nil then
		begin
			Result := True;
			GetPathDisplayAndAttr(ItemIDList, String(FFolder), FDisplayName, FFolderAttr,
				efUsePrettyCase in FExtraFlags);
			FImageIndex := BrowseInfo.iImage;
			if FreeStartPIDL and (OldItemIDList <> nil)
				then CoTaskMemFree(OldItemIDList);
		end;
	finally
		if Assigned(BrowseInfo.lpfn) then FreeObjectInstance(@BrowseInfo.lpfn);
		if BrowseInfo.pidlRoot <> nil then CoTaskMemFree(BrowseInfo.pidlRoot);
		StrDispose(BrowseInfo.pszDisplayName);
	end;
end;

function TPBFolderDialog.GetEnumFlags(ShellFolder : IShellFolder;
  PIDL: PItemIDList; const ParentWindow : HWND;
	var EnumFlags : DWORD): HResult;
begin
	EnumFlags := EnumFlags or $100;
	Result := S_OK;
end;

function TPBFolderDialog.GetListHandle(HWindow : HWnd) : HWnd;
begin
	if FNewStyle then
	begin
		Result := FindWindowEx(HWindow, 0,
			'SHBrowseForFolder ShellNameSpace Control', nil);
		Result := FindWindowEx(Result, 0, 'SysTreeView32', nil);
	end
	else Result := FindWindowEx(HWindow, 0, 'SysTreeView32', nil);
end;

function TPBFolderDialog.GetSystemLang : string;
var
	Size, LangID, Dummy : DWord;
	PBuffer : PChar;
	PLang : Pointer;
begin
	Size := GetFileVersionInfoSize('Shell32.dll', Dummy);
	LangID := GetSystemDefaultLangID;
	if Size <> 0 then
	begin
		PBuffer := StrAlloc(Size);
		try
			if GetFileVersionInfo('Shell32.dll', 0, Size, PBuffer) then
			begin
				if VerQueryValue(PBuffer, '\VarFileInfo\Translation', PLang, Dummy)
					then LangID := LoWord(Longint(PLang^));
			end;
		finally
			StrDispose(PBuffer);
		end;
	end;
	Result := IntToHex(LangID, 4);
end;

function TPBFolderDialog.HookMain(var Msg : TMessage): Boolean;
begin
	with Msg do
	begin
		if Msg = WM_SIZE then
		begin
			if WParam = SIZE_MINIMIZED then SendMessage(FDialogHandle,
				WM_SHOWWINDOW, Integer(False), SW_PARENTCLOSING)
			else if WParam = SIZE_RESTORED then SendMessage(FDialogHandle,
				WM_SHOWWINDOW, Integer(True), SW_PARENTOPENING);
		end;
	end;
	Result := False;
end;

function TPBFolderDialog.LocaleText(List : TStringList) : string;
begin
	if List.Count = 0 then Result := ''
	else
	begin
		if List.IndexOfName(FLocale) <> -1 then Result := List.Values[FLocale]
		else if List.IndexOfName(FPrimaryLocale) <> -1
			then Result := List.Values[FPrimaryLocale]
		else if List.IndexOfName('Default') <> -1 then Result := List.Values['Default']
		else Result := List.Values[List.Names[0]];
	end;
end;

function TPBFolderDialog.ShouldShow(ShellFolder : IShellFolder; ParentPIDL, ItemPIDL : PItemIDList): HResult;
var
	Path : string;
	TempAttr : Cardinal;
	Attr : TFolderAttributeSet;
	Include : Boolean;
begin
	Result := S_OK;
	if Assigned(FOnXPIncludeItem) then
	begin
		Include := True;
		Path := GetRelDisplay(ShellFolder, ItemPIDL, True);
		TempAttr := $FFFFFFFF and not SFGAO_VALIDATE;
		ShellFolder.GetAttributesOf(1, ItemPIDL, TempAttr);
		Attr :=  GetFolderAttributes(TempAttr);
		FOnXPIncludeItem(Self, FDialogHandle, ShellFolder, ParentPIDL, ItemPIDL, Path, Attr, Include);
		if not Include then Result := S_FALSE;
	end;
end;

function TPBFolderDialog.StoreCurrentFolderCaptions : Boolean;
begin
	Result := (FLabelCaptions.CommaText <> CURRENT_FOLDER_CAPTIONS);
end;

function TPBFolderDialog.StoreNewFolderCaptions : Boolean;
begin
	Result := (FNewFolderCaptions.CommaText <> NEW_FOLDER_CAPTIONS);
end;

procedure TPBFolderDialog.AddNewFolderButton;
var
	OkHandle : HWND;
	DC : HDC;
	ButtonFont : HFONT;
	ControlCreateStyles, TextWidth, OkTop : Integer;
	Rect0, Rect1 : TRect;
	TopLeft : TPoint;
begin
	OkHandle := FindWindowEx(FDialogHandle, 0, 'BUTTON', nil);
	GetWindowRect(OkHandle, Rect0);
	ScreenToClient(FDialogHandle,Rect0.TopLeft);
	OkTop := Rect0.Top;
	ControlCreateStyles := WS_CHILD or WS_CLIPSIBLINGS or WS_VISIBLE or WS_TABSTOP
		or BS_PUSHBUTTON;
	case Screen.PixelsPerInch of
		72, 96: FNewFolderHandle:=CreateWindow('Button', PChar(FNewFolderCaption),
			ControlCreateStyles, 12, OkTop, MinButtonWidth, 23,
			FDialogHandle, _BUTTON_ID, HInstance, nil);
		120: FNewFolderHandle:=CreateWindow('Button', PChar(FNewFolderCaption),
			ControlCreateStyles, 15, OkTop, MinButtonWidth * 5 div 4, 28,
			FDialogHandle, _BUTTON_ID, HInstance, nil);
		144: FNewFolderHandle:=CreateWindow('Button', PChar(FNewFolderCaption),
			ControlCreateStyles, 17, OkTop, MinButtonWidth * 3 div 2, 35,
			FDialogHandle, _BUTTON_ID, HInstance, nil);
		192: FNewFolderHandle:=CreateWindow('Button', PChar(FNewFolderCaption),
			ControlCreateStyles, 20, OkTop, MinButtonWidth * 192 div 96,
			41, FDialogHandle, _BUTTON_ID, HInstance, nil);
		else FNewFolderHandle:=CreateWindow('Button', PChar(FNewFolderCaption),
			ControlCreateStyles, 12 * Screen.PixelsPerInch div 96, OkTop,
			MinButtonWidth * Screen.PixelsPerInch div 96, 23 * Screen.PixelsPerInch div 96,
			FDialogHandle, _BUTTON_ID, HInstance, nil);
	end;
	ButtonFont := HFONT(SendMessage(OkHandle, WM_GETFONT, 0, 0));
	PostMessage(FNewFolderHandle, WM_SETFONT, Integer(ButtonFont),
		MAKELPARAM(1,0));
	DC := GetDC(FNewFolderHandle);
	GetClientRect(FNewFolderHandle, Rect1);
	if DC <> 0 then
	begin
		DrawText(DC, PChar(FNewFolderCaption), -1, Rect1, DT_CALCRECT	or DT_CENTER
			or DT_NOCLIP or DT_SINGLELINE);
		ReleaseDC(FNewFolderHandle, DC);
	end;
	TextWidth := Rect1.Right - Rect1.Left;
	GetClientRect(FNewFolderHandle, Rect1);
	Rect1.Right := Rect1.Left + TextWidth + 12;
	if (Rect1.Right < MinButtonWidth) and (Screen.PixelsPerInch = 72)
		then Rect1.Right := MinButtonWidth
	else if Rect1.Right < MinButtonWidth * Screen.PixelsPerInch div 96
		then Rect1.Right := MinButtonWidth * Screen.PixelsPerInch div 96;
	AdjustWindowRect(Rect1, ControlCreateStyles, False);
	GetWindowRect(FNewFolderHandle, Rect0);
	TopLeft := Rect0.TopLeft;
	ScreenToClient(FDialogHandle, TopLeft);
	MoveWindow(FNewFolderHandle, TopLeft.x, TopLeft.y,
		Rect1.Right - Rect1.Left, Rect1.Bottom - Rect1.Top, True);
	EnableWindow(FNewFolderHandle, True);
end;

procedure TPBFolderDialog.BrowseCallBackProc(var Msg : TMessage);
var
	Title : string;
	Attr : TFolderAttributeSet;
	pWnd : ^HWND;
	FCloseDialog : Boolean;
	TempIDList : PItemIDList;
	State : SmallInt;
	FolderFilterSite : IFolderFilterSite;
begin
	with Msg do
	begin
		Result := 0;
		pWnd := Pointer(LongInt(@Msg) + 24);
		FDialogHandle := pWnd^;
		if FListHandle = 0 then FListHandle := GetListHandle(FDialogHandle);
		begin
		end;
		case Msg of
			BFFM_INITIALIZED:
			begin
				Application.HookMainWindow(HookMain);
				if (bfShowPath in FFlags) then ShowLabelPath;
				CenterWindow(FDialogHandle);
				Title := LocaleText(FTitles);
				if Title <> ''
					then SendMessage(FDialogHandle, WM_SETTEXT, 0, Integer(PChar(Title)));
				POldWndProc := Pointer(GetWindowLong(FDialogHandle, GWL_WNDPROC));
				PNewWndProc := MakeObjectInstance(WndProc);
				if Assigned(PNewWndProc) then SetWindowLong(FDialogHandle, GWL_WNDPROC,
					Integer(PNewWndProc));
				FInitialized := True;
				if (PStartItemIDList <> nil) then
				begin
					SetSelectionPIDL(FDialogHandle, PStartItemIDList);
					PStartItemIDList := nil;
				end
				else if FRestartFolder <> '' then
				begin
					SetSelectionPath(FDialogHandle, FRestartFolder);
					FRestartFolder := '';
				end;
				SendMessage(FListHandle, $1114{TVM_ENSUREVISIBLE}, 0,
					Integer(FSelectedItem));
				SendMessage(FListHandle, $1102{TVM_EXPAND}, $0002{TVE_EXPAND},
					Integer(FSelectedItem));
				if Assigned(OnInitialized)
					then OnInitialized(Self, FDialogHandle);
			end;
			BFFM_IUNKNOWN:
			begin
				if (Win32MajorVersion < 6) and (WParam <> 0) and (IUnknown(WParam).QueryInterface(IID_IFolderFilterSite,
					FolderFilterSite) = 0) then
				begin
					FolderFilterSite.SetFilter(IFolderFilter(Self));
					FolderFilterSite._Release;
				end;
			end;
			BFFM_SELCHANGED:
			begin
				TempIDList := PItemIDList(WParam);
				if TempIDList = nil then Exit;
				if (not (bfDontBrowseShortcuts in FFlags))
					and GetShortcutTarget(TempIDList) then
				begin
					SetSelectionPIDL(FDialogHandle, TempIDList);
					CoTaskMemFree(TempIDList);
					Exit;
				end
				else TempIDList := PItemIDList(WParam);
				GetPathDisplayAndAttr(TempIDList, FSelectedFolder, FDisplayName, Attr, efUsePrettyCase in FExtraFlags);
				FSelectedItem := SendMessage(FListHandle,	$110A{TVM_GETNEXTITEM},
					$0009{TVGN_CARET}, 0);
				if Boolean(GetSystemMetrics(SM_SWAPBUTTON))
					then State := GetAsyncKeyState(VK_RBUTTON)
				else State := GetAsyncKeyState(VK_LBUTTON);
				if (efAutoExpandTree in FExtraFlags) and (State > 0)
					then SendMessage(FListHandle, $1102{TVM_EXPAND}, $0002{TVE_EXPAND},
					Integer(FSelectedItem));
				if FSelectedFolder <> '' then FDisplayName := FSelectedFolder;
				if (bfShowPath in FFlags) and FInitialized then ShowLabelPath;
				FValidPath := (FSelectedFolder <> '');
				if (FBrowseFor = bfOnlyFileSystem) then EnableOK(FDialogHandle, FValidPath)
				else EnableOK(FDialogHandle, True);
				if (FBrowseFor = bfOnlyAncestors) and not (atFileSysAncestor in Attr)
					then EnableOK(FDialogHandle, False);
				if IsWindow(FNewFolderHandle)
					then EnableWindow(FNewFolderHandle, FValidPath);
				if Assigned(OnSelectionChanged)
					then OnSelectionChanged(Self, FDialogHandle,
					PItemIDList(WParam), SelectedFolder, Attr);
			end;
			BFFM_VALIDATEFAILED :
			begin
				Result := 1;
				FCloseDialog := False;
				if Assigned(FOnValidateFailed) then
				begin
					FOnValidateFailed(Self, FDialogHandle, FCloseDialog);
					if FCloseDialog then Result := 0;
				end
				else MessageBeep(0);
			end;
		end;
	end;
end;

procedure TPBFolderDialog.CenterWindow(HWindow: HWND);
var
	Rect0: TRect;
begin
	GetWindowRect(HWindow, Rect0);
	if (FWindowRect.Right = 0) and (FWindowRect.Bottom = 0)
		then SetWindowPos(HWindow, 0,
		(Screen.Width -Rect0.Right + Rect0.Left) div 2,
		(Screen.Height - Rect0.Bottom + Rect0.Top) div 2,
		0, 0, SWP_NOACTIVATE or SWP_NOSIZE or SWP_NOZORDER)
	else SetWindowPos(HWindow, 0,
		FWindowRect.Left, FWindowRect.Top,
		0, 0, SWP_NOACTIVATE or SWP_NOSIZE or SWP_NOZORDER);
end;

procedure TPBFolderDialog.EnableOK(const Hwindow : HWND; const Value: Boolean);
begin
	SendMessage(Hwindow, BFFM_ENABLEOK, 0, Ord(Value));
end;

procedure TPBFolderDialog.LabelCaptionsChange (Sender: TObject);
begin
	FLabelCaption := LocaleText(FLabelCaptions);
end;

procedure TPBFolderDialog.NewFolderCaptionsChange (Sender: TObject);
begin
	SetNewFolderCaption(LocaleText(FNewFolderCaptions));
end;

procedure TPBFolderDialog.SetExpanded(const HWindow : HWnd;
	const ItemIdList : PItemIDList; const Expand : Boolean);
begin
	SendMessage(HWindow, BFFM_SETEXPANDED, Integer(ItemIDList), Integer(Expand));
end;

procedure TPBFolderDialog.SetNewFolderCaption(Value: String);
begin
	FNewFolderCaption:=Value;
	if (IsWindow(FNewFolderHandle))
		then SetWindowText(FNewFolderHandle, PChar(Value));
end;

procedure TPBFolderDialog.SetLabelCaptions(Value : TStringList);
begin
	if FLabelCaptions.Text <> Value.Text then
	begin
		FLabelCaptions.Assign(Value);
	end;
end;

procedure TPBFolderDialog.SetNewFolderCaptions(Value : TStringList);
begin
	if FNewFolderCaptions.Text <> Value.Text then
	begin
		FNewFolderCaptions.Assign(Value);
	end;
end;

procedure TPBFolderDialog.SetSelectedFolder(Value: String);
begin
	SetSelectionPath(DialogHandle, Value);
end;

procedure TPBFolderDialog.SetSelectionPath(const Hwindow : HWND; const Path: String);
begin
	SendMessage(Hwindow, BFFM_SETSELECTION, Ord(TRUE), Integer(PChar(Path)));
end;

procedure TPBFolderDialog.SetSelectionPIDL(const Hwindow: HWND; const ItemIDList: PItemIDList);
begin
	SendMessage(Hwindow, BFFM_SETSELECTION, Ord(FALSE), Integer(ItemIDList));
end;

procedure TPBFolderDialog.SetTitles(Value : TStringList);
begin
	if FTitles.Text <> Value.Text then FTitles.Assign(Value);
end;

procedure TPBFolderDialog.ShowLabelPath;
var
	Style : integer;
	Rect0 : TRect;
	LabelTopLeft, ListTopLeft : TPoint;
	LabelHandle, NextHandle : HWnd;
	Text : string;
	Canvas : TCanvas;
begin
	LabelHandle := FindWindowEx(FDialogHandle, 0, 'STATIC', PChar(FLabelCaption));
	GetWindowRect(LabelHandle, Rect0);
	LabelTopLeft := Rect0.TopLeft;
	ScreenToClient(FDialogHandle, LabelTopLeft);
	if FNewStyle then NextHandle := FindWindowEx(FDialogHandle, 0,
		'SHBrowseForFolder ShellNameSpace Control', nil)
	else if bfEditBox in FFlags then NextHandle := FindWindowEx(FDialogHandle, 0,
		'EDIT', nil)
	else NextHandle := FindWindowEx(FDialogHandle, 0, 'SysTreeView32', nil);
	GetWindowRect(NextHandle, Rect0);
	ListTopLeft := Rect0.TopLeft;
	ScreenToClient(FDialogHandle, ListTopLeft);
	if FPathHandle = 0 then
	begin
		FPathHandle := FindWindowEx(FDialogHandle, 0, 'STATIC', '');
		if FPathHandle = 0 then Exit;
		Style := GetWindowLong(FPathHandle, GWL_STYLE) or SS_LEFTNOWORDWRAP;
		SetWindowLong(FPathHandle, GWL_STYLE, Style);
		EnableWindow(FPathHandle, True);
		ShowWindow(FPathHandle, SW_SHOW);
	end;
	MoveWindow(FPathHandle, ListTopLeft.X,
		(LabelTopLeft.Y + ListTopLeft.Y) div 2, Rect0.Right - Rect0.Left + 8,
		ListTopLeft.Y - (LabelTopLeft.Y + ListTopLeft.Y) div 2, True);
	GetClientRect(FPathHandle, Rect0);
	Canvas := TCanvas.Create;
	try
		Canvas.Handle := GetDC(FPathHandle);
		if Canvas.Handle <> 0 then
		begin
			Text := MinimizeName(FDisplayName, Canvas, Rect0.Right - 8);
			ReleaseDC(FPathHandle, Canvas.Handle);
			Canvas.Handle := 0;
			SendMessage(FPathHandle, WM_SETTEXT, 0, Integer(PChar(Text)));
		end;
	finally
		Canvas.Free;
	end;
end;

procedure TPBFolderDialog.WndProc(var Msg : TMessage);
var
	NewFolder : String;
	ControlID : integer;
begin
	with Msg do
	begin
		if (not FNewStyle) and (Msg = WM_COMMAND) and (WParamLo = _BUTTON_ID) then
		begin
			NewFolder := InputBox(FNewFolderCaption, '', '');
			if NewFolder <> '' then
			begin
				FRestart := True;
				if (NewFolder[1] <> '\')
					and (FSelectedFolder[Length(FSelectedFolder)] <> '\')
					then NewFolder := '\' + NewFolder;
				FRestartFolder := FSelectedFolder + NewFolder;
				try
					ForceDirectories(FRestartFolder);
					if not DirectoryExists(FRestartFolder) then Raise Exception.Create('');
				except
					FInitialized := False;
					FRestartFolder := '';
					ShowMessage(SysErrorMessage(82));
				end;
				PostMessage(FDialogHandle, WM_KEYDOWN, VK_ESCAPE, 0);
				PostMessage(FDialogHandle, WM_KEYUP, VK_ESCAPE, 0);
			end;
		end
		else if (Msg = WM_SHOWWINDOW) and (BOOL(WParam) = True) and (LParam = 0) then
		begin
			CenterWindow(FDialogHandle);
			if FNewStyle and (FNewFolderHandle = 0) then
			begin
				FNewFolderHandle := GetDlgItem(FDialogHandle, 14150);
				ControlID := GetDlgCtrlID(FNewFolderHandle);
				while (FNewFolderHandle = 0) and (ControlID < 2) do
				begin
					FNewFolderHandle := FindWindowEx(FDialogHandle, FNewFolderHandle,
						'BUTTON', nil);
					ControlID := GetDlgCtrlID(FNewFolderHandle);
				end;
				if (bfHideNewFolderButton in FFlags) and (Win32MajorVersion = 5)
					and (FNewFolderHandle <> 0) and IsWindow(FNewFolderHandle) then
				begin
					EnableWindow(FNewFolderHandle, False);
					ShowWindow(FNewFolderHandle, SW_HIDE);
				end;
			end
			else if (not (bfHideNewFolderButton in FFlags))
				and (FNewFolderHandle = 0) then
			begin
				AddNewFolderButton;
				if IsWindow(FNewFolderHandle) then
				begin
					EnableWindow(FNewFolderHandle, FValidPath);
					ShowWindow(FNewFolderHandle, SW_SHOW);
				end;
			end;
		end
		else if (((Msg = WM_SIZE) and (WParam = SIZE_RESTORED))
			or (Msg = WM_MOVE)) and IsWindowVisible(FDialogHandle)
			and (bfShowPath in FFlags) then PostMessage(FDialogHandle, PB_SHOWLABEL, 0, 0)
		else if (Msg = PB_SHOWLABEL) then
		begin
			GetWindowRect(FDialogHandle, FWindowRect);
			ShowLabelPath;
		end
		else if (Msg = WM_DESTROY) and not FNewStyle and (FNewFolderHandle <> 0)
			and IsWindow(FNewFolderHandle) then DestroyWindow(FNewFolderHandle);
		Result := CallWindowProc(POldWndProc, FDialogHandle, Msg, WParam, LParam);
	end;
end;

procedure TPBFolderDialog.Dummy(Value: String); begin {Read only !} end;

end.
