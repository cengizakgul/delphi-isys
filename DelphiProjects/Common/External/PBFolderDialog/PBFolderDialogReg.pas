unit PBFolderDialogReg;

{$INCLUDE PBDefines.inc}

interface

uses
	SysUtils, Classes,
{$IFDEF COMPILER_MAX_5} DsgnIntf;
{$ELSE}	DesignIntf, DesignEditors, VCLEditors;
{$ENDIF}

{$R *.dcr}

procedure Register;

implementation

uses PBFolderDialog, PBShellFolders;

type
// ---------------  PropertyEditor TFolderListProperty  ---------
	TShellFolderPathProperty = class(TStringProperty)
	protected
		FValueList : TStringList;
	public
		destructor Destroy; override;
		procedure Initialize; override;
		function GetAttributes : TPropertyAttributes; override;
		procedure GetValues(Proc: TGetStrProc); override;
	end;

destructor TShellFolderPathProperty.Destroy;
begin
	FValueList.Free;
	FValueList := nil;
	inherited Destroy;
end;

function TShellFolderPathProperty.GetAttributes : TPropertyAttributes;
begin
	Result := [paValueList, paSortList, paRevertable];
end;

procedure TShellFolderPathProperty.Initialize;
var
	t : integer;
begin
	inherited Initialize;
	FValueList := TStringList.Create;
	for t := 0 to MAX_SHELLFOLDERNAMES
		do if ShellFolderNames[t] <> '%%'
		then FValuelist.Add(ShellFolderNames[t]);
end;

procedure TShellFolderPathProperty.GetValues(Proc: TGetStrProc);
var
	t : integer;
begin
	t := 0;
	while t < FValueList.Count do
	begin
		Proc(FValueList[t]);
		Inc(t);
	end;
end;

//  ---------------------  Register  ----------------------------
procedure Register;
begin
	RegisterComponents('PB', [TPBFolderDialog]);
	RegisterPropertyEditor(TypeInfo(TShellFolderPath), TPBFolderDialog, '',
		TShellFolderPathProperty);
end;

end.
