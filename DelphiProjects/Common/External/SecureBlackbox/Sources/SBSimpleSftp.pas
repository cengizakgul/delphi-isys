(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSimpleSftp;

interface

uses
  SBUtils,

  {$ifndef CLX_USED}
  Windows,
  Winsock,
  Messages,
  {$else}
  libc,
  {$endif}
  Classes,
  SysUtils,
  SBSftp,
  SBSftpCommon,
  SBSocket
  ,
  SBSSHCommon,
  SBSSHClient,
  SBSSHConstants,
  SBSSHKeyStorage
  {$ifdef SECURE_BLACKBOX_DEBUG},
  SBDumper
  {$endif}
  ;

type

    ESFTPTimeout =  class(ESecureBlackboxError);

    TElSimpleSFTPClient = class(TSBControlBase)
    private
      FCurOp       : TElSftpOperation;
      FOpParams    : TObject;
      FOpStrParams : string;
      FOpened      : boolean;
      FNewLineConvention: String;
      FErrorCode  : integer;
      FSFTPClient : TElSFTPClient;
      FClient     : TElSSHClient;
      FTunnelList : TElSSHTunnelList;
      FTunnel     : TElSubsystemSSHTunnel;
      FSFTPExt    : TSBSftpExtendedAttributes;
      FDataBuf    : Pointer;
      FDataSize   : integer;
      FMessageLoop: TSBSftpMessageLoopEvent;
      FSocket     : TElSocket;
      FUseInternalSocket  : boolean;
      FDoUseInternalSocket: boolean;
      FReadSomething: boolean;
      FTransferBlockSize: Integer;
      FAutoAdjustTransferBlock : boolean;
      FDownloadBlockSize: Integer;
      FUploadBlockSize: Integer;
      FLastOpTick : integer;
      FOnProgress : TSBProgressEvent;
      FPipelineLength: Integer;

      // Socks support
      FSocketTimeout: Integer;
      FSocksAuthentication: TElSocksAuthentication;
      FSocksPassword: string;
      FSocksPort: Integer;
      FSocksResolveAddress: boolean;
      FSocksServer: string;
      FSocksUserCode: string;
      FSocksVersion: TElSocksVersion;
      FUseSocks: boolean;
      //  Web tunneling support
      FUseWebTunneling: boolean;
      FWebTunnelAddress: string;
      FWebTunnelAuthentication: TElWebTunnelAuthentication;
      FWebTunnelPassword: string;
      FWebTunnelPort: Integer;
      FWebTunnelUserId: string;
    procedure SetDownloadBlockSize(const Value: Integer);
    procedure SetUploadBlockSize(const Value: Integer);
    protected
      FOnAuthenticationFailed: TSSHAuthenticationFailedEvent;
      FOnAuthenticationKeyboard: TSSHAuthenticationKeyboardEvent;
      FOnAuthenticationSuccess: TNotifyEvent;
      FOnBanner: TSSHBannerEvent;
      FOnCloseConnection: TSSHCloseConnectionEvent;
      FOnError: TSSHErrorEvent;
      FOnKeyValidate: TSSHKeyValidateEvent;
      FOnReceive: TSSHReceiveEvent;
      FOnSend: TSSHSendEvent;

      function IntMessageLoop: boolean;
      procedure DataAvailable;
      procedure DoAuthenticationFailed(Sender : TObject; AuthenticationType :
          integer);
      procedure DoAuthenticationKeyboard(Sender : TObject; Prompts : TStringList;
          Echo : TBits; Responses : TStringList);
      procedure DoAuthenticationSuccess(Sender : TObject);
      procedure DoCloseConnection(Sender : TObject);
      //procedure DoConnClosed(Sender : TObject; CloseType : TSSHCloseType);

      procedure DoBanner(Sender: TObject; const Text : string; const Language : string);
      procedure DoClose(Force: boolean);

      procedure DoError(Sender : TObject; ErrorCode : integer);
      procedure DoKeyValidate(Sender : TObject; ServerKey : TElSSHKey; var Validate : boolean);

      procedure DoReceive(Sender : TObject; Buffer : pointer; MaxSize : longint; out
        Written : longint);
      procedure DoSend(Sender : TObject; Buffer : pointer; Size : longint);

      function GetActive: boolean;
      function GetAddress: string;
      function GetAuthenticationTypes: integer;
      function GetClientHostname: string;
      function GetClientUsername: string;
      function GetCompressionAlgorithmCS: TSSHCompressionAlgorithm;
      function GetCompressionAlgorithms(Index : TSSHCompressionAlgorithm): boolean;
      function GetCompressionAlgorithmSC: TSSHCompressionAlgorithm;
      function GetCompressionLevel: integer;
      function GetEncryptionAlgorithmCS: TSSHEncryptionAlgorithm;
      function GetEncryptionAlgorithms(Index : TSSHEncryptionAlgorithm): boolean;
      function GetEncryptionAlgorithmSC: TSSHEncryptionAlgorithm;
      function GetForceCompression: boolean;
      function GetKexAlgorithm: TSSHKexAlgorithm;
      function GetKexAlgorithms(Index : TSSHKexAlgorithm): boolean;
      function GetKeyStorage: TElSSHCustomKeyStorage;
      function GetLastSyncComment: string;
      function GetLastSyncError: Integer;
      function GetMacAlgorithmCS: TSSHMacAlgorithm;
      function GetMACAlgorithms(Index : TSSHMacAlgorithm): boolean;
      function GetMacAlgorithmSC: TSSHMacAlgorithm;
      function GetPassword: string;
      function GetPort: Integer;
      function GetPublicKeyAlgorithm: TSSHPublicKeyAlgorithm;
      function DoMessageLoop: boolean;
      function GetPublicKeyAlgorithms(Index : TSSHPublicKeyAlgorithm): boolean;
      function GetServerCloseReason: string;
      function GetServerSoftwareName: string;
      function GetSftpBufferSize: Integer;
      function GetSoftwareName: string;
      function GetUsername: string;
      function GetVersion: TSBSftpVersion;
      function GetVersions: TSBSftpVersions;
      function GetUseUTF8 : boolean;
      function GetExtendedProperties : TElSFTPExtendedProperties;
      procedure HandleAbsolutePath(Sender : TObject; const Path : string);
      procedure HandleAttributes(Sender: TObject; Attributes: TElSftpFileAttributes);
      procedure HandleSFTPError(Sender: TObject; ErrorCode: integer; const Comment:
          string);
      {$ifndef BUILDER_USED}
      procedure HandleDirectoryListing(Sender: TObject; Listing: array of TElSftpFileInfo);
      {$else}
      procedure HandleDirectoryListing(Sender: TObject; Listing: TList);
      {$endif}
      procedure HandleSftpData(Sender : TObject; Buffer : pointer; Size : integer);
      procedure HandleSftpOpen(Sender : TObject);
      procedure HandleSftpClose(Sender : TObject);
      function HandleSftpMessageLoop : boolean;
      procedure SetAddress(const Value: string);
      procedure SetAuthenticationTypes(Value : integer);
      procedure SetClientHostname(Value : string);
      procedure SetClientUsername(Value : string);
      procedure SetCompressionAlgorithms(Index : TSSHCompressionAlgorithm;
        Value : boolean);
      procedure SetCompressionLevel(Value : integer);
      procedure SetEncryptionAlgorithms(Index : TSSHEncryptionAlgorithm;
        Value : boolean);
      procedure SetForceCompression(Value : boolean);
      procedure SetKexAlgorithms(Index : TSSHKexAlgorithm;
        Value : boolean);
      procedure SetKeyStorage(Value : TElSSHCustomKeyStorage);
      procedure SetMACAlgorithms(Index : TSSHMacAlgorithm;
        Value : boolean);
      procedure SetPassword(Value : string);
      procedure SetPort(Value: Integer);
      procedure SetPublicKeyAlgorithms(Index : TSSHPublicKeyAlgorithm;
        Value : boolean);
      procedure SetSftpBufferSize(Value: Integer);
      procedure SetSocketTimeout(Value: Integer);
      procedure SetSoftwareName(Value : string);
      procedure SetUseInternalSocket(Value: boolean);
      procedure SetUsername(Value : string);
      procedure SetUseSocks(const Value: boolean);
      procedure SetUseWebTunneling(const Value: boolean);
      procedure SetVersions(Value: TSBSftpVersions);
      procedure SetUseUTF8(Value : boolean);
      procedure SetPipelineLength(const Value: Integer);



      procedure DoProgress(Total, Current : Int64; var Cancel : boolean);

      function GetLocalAddress: string;
      function GetLocalPort: Integer;
      procedure SetLocalAddress(const Value: string);
      procedure SetLocalPort(const Value: Integer);
      function GetAuthOrder() : TSBSSHAuthOrder;
      procedure SetAuthOrder(Value : TSBSSHAuthOrder);
    public
      constructor Create(AOwner : TSBComponentBase); override;
      destructor Destroy; override;

      {$ifdef BUILDER_USED}
      function SftpCreateFile(const Filename: string): TSBSftpFileHandle;
      function SftpOpenFile(const Filename: string; Modes: TSBSftpFileOpenModes; Attributes:
        TElSftpFileAttributes): TSBSftpFileHandle; overload;
      function SftpOpenFile(const Filename: string; Modes: TSBSftpFileOpenModes;
        Access : TSBSftpFileOpenAccess; Attributes: TElSftpFileAttributes): TSBSftpFileHandle; overload;
      procedure SftpRemoveDirectory(const Path: string);
      procedure SftpRemoveFile(const Filename: string);
      {$endif}

      procedure Block(const Handle: TSBSftpFileHandle; Offset, Length : Int64;
        LockMask : TSBSftpFileOpenAccess);
      procedure Unblock(const Handle: TSBSftpFileHandle; Offset, Length : Int64);
      procedure Close(Silent : boolean {$ifdef HAS_DEF_PARAMS}= false{$endif});
      procedure CloseHandle(const Handle: TSBSftpFileHandle);
      function CreateFile(const Filename: string): TSBSftpFileHandle;
      procedure CreateSymLink(const LinkPath: string; const TargetPath: string);
      procedure CreateHardLink(const LinkPath: string; const TargetPath: string);
      procedure DownloadFile(RemoteFileName, LocalFileName : string);
      procedure UploadFile(LocalFileName, RemoteFileName : string);
      procedure MakeDirectory(const Path: string; Attributes: TElSftpFileAttributes);
      procedure Open;
      function OpenDirectory(const Path: string): TSBSFTPFileHandle;
      function OpenFile(const Filename: string; Modes: TSBSftpFileOpenModes;
        Attributes: TElSftpFileAttributes): TSBSftpFileHandle; overload;
      function OpenFile(const Filename: string; Modes: TSBSftpFileOpenModes;
        Access : TSBSftpFileOpenAccess; Attributes : TElSftpFileAttributes) : TSBSftpFileHandle; overload;
      procedure ReadDirectory(const Handle: TSBSftpFileHandle; Listing: TList);
      procedure ReadSymLink(const Path: string; Info : TElSftpFileInfo);
      function Read(const Handle: TSBSftpFileHandle; Offset: Int64; Buffer : Pointer; Length: integer): integer;
      procedure RemoveDirectory(const Path: string);
      procedure RemoveFile(const Filename: string);
      procedure RenameFile(const OldPath, NewPath: string);
      function RequestAbsolutePath(const Path: string) : string;
      function RequestAbsolutePathEx(const Path: string;
        Control : TSBSftpRealpathControl; ComposePath : TStringList) : string;
      procedure RequestAttributes(const Path: string; FollowSymLinks: boolean;
          Attributes : TElSftpFileAttributes); overload;
      procedure RequestAttributes(const Handle: TSBSftpFileHandle; Attributes :
          TElSftpFileAttributes); overload;
      procedure SetAttributes(const Path: string; Attributes: TElSftpFileAttributes);
      procedure SetAttributesByHandle(const Handle: TSBSftpFileHandle; Attributes: TElSftpFileAttributes);
      procedure Write(const Handle: TSBSftpFileHandle; Offset: Int64; Buffer: Pointer; Size : integer);
      procedure TextSeek(const Handle: TSBSftpFileHandle; LineNumber: Int64);

      property Active: boolean read GetActive;
      property CompressionAlgorithmClientToServer: TSSHCompressionAlgorithm
        read GetCompressionAlgorithmCS;
      property CompressionAlgorithms[Index : TSSHCompressionAlgorithm]: boolean
        read GetCompressionAlgorithms write SetCompressionAlgorithms;
      property CompressionAlgorithmServerToClient: TSSHCompressionAlgorithm
        read GetCompressionAlgorithmSC;
      property EncryptionAlgorithmClientToServer: TSSHEncryptionAlgorithm
        read GetEncryptionAlgorithmCS;
      property EncryptionAlgorithms[Index : TSSHEncryptionAlgorithm]: boolean
        read GetEncryptionAlgorithms write SetEncryptionAlgorithms;
      property EncryptionAlgorithmServerToClient: TSSHEncryptionAlgorithm
        read GetEncryptionAlgorithmSC;
      property KexAlgorithm: TSSHKexAlgorithm
        read GetKexAlgorithm;
      property KexAlgorithms[Index : TSSHKexAlgorithm]: boolean
        read GetKexAlgorithms write SetKexAlgorithms;
    {end modifications}
      property LastSyncComment: string read GetLastSyncComment;
      property LastSyncError: Integer read GetLastSyncError;
      property MacAlgorithmClientToServer: TSSHMacAlgorithm
        read GetMacAlgorithmCS;
      property MacAlgorithms[Index : TSSHMacAlgorithm]: boolean
        read GetMACAlgorithms write SetMACAlgorithms;
      property MacAlgorithmServerToClient: TSSHMacAlgorithm
        read GetMacAlgorithmSC;
    {end modification}
    {JPM Modification for ASCII newline convention in SFTP4 - used for ASCII transfers}
      property NewLineConvention: String read FNewLineConvention write
          FNewLineConvention;
      property PublicKeyAlgorithm: TSSHPublicKeyAlgorithm
        read GetPublicKeyAlgorithm;
      property PublicKeyAlgorithms[Index : TSSHPublicKeyAlgorithm]: boolean
        read GetPublicKeyAlgorithms write SetPublicKeyAlgorithms;
      property ServerCloseReason: string read GetServerCloseReason;
      property ServerSoftwareName: string read GetServerSoftwareName;
      property SFTPExt: TSBSftpExtendedAttributes read FSFTPExt write FSFTPExt;
    {JPM MOdifications for SFTP Extensions}
      property Version: TSBSftpVersion read GetVersion;

      property ExtendedProperties : TElSftpExtendedProperties read GetExtendedProperties;

    published
      property SSHAuthOrder : TSBSSHAuthOrder read GetAuthOrder write SetAuthOrder;
      property Address: string read GetAddress write SetAddress;
      property AuthenticationTypes: integer read GetAuthenticationTypes write
          SetAuthenticationTypes default SSH_AUTH_TYPE_PASSWORD;
      property ClientHostname: string read GetClientHostname write SetClientHostname;
      property ClientUsername: string read GetClientUsername write SetClientUsername;
      property CompressionLevel: integer read GetCompressionLevel write
          SetCompressionLevel default 9;
      property ForceCompression: boolean read GetForceCompression write
          SetForceCompression;
      property KeyStorage: TElSSHCustomKeyStorage
        read GetKeyStorage write SetKeyStorage;
      property MessageLoop: TSBSftpMessageLoopEvent read FMessageLoop write FMessageLoop;

      property OnAuthenticationFailed: TSSHAuthenticationFailedEvent
        read FOnAuthenticationFailed write FOnAuthenticationFailed;
      property OnAuthenticationKeyboard: TSSHAuthenticationKeyboardEvent
        read FOnAuthenticationKeyboard write FOnAuthenticationKeyboard;
      property OnAuthenticationSuccess: TNotifyEvent
        read FOnAuthenticationSuccess write FOnAuthenticationSuccess;
      property OnBanner: TSSHBannerEvent
        read FOnBanner write FOnBanner;
      property OnCloseConnection: TSSHCloseConnectionEvent
        read FOnCloseConnection write FOnCloseConnection;
      property OnError: TSSHErrorEvent
        read FOnError write FOnError;
      property OnKeyValidate: TSSHKeyValidateEvent
        read FOnKeyValidate write FOnKeyValidate;
      property OnReceive: TSSHReceiveEvent
        read FOnReceive write FOnReceive;
      property OnSend: TSSHSendEvent
        read FOnSend write FOnSend;

      property OnProgress : TSBProgressEvent read FOnProgress write FOnProgress;
        
      property Password: string read GetPassword write SetPassword;
      property Port: Integer read GetPort write SetPort default 22;
      property SftpBufferSize: Integer read GetSftpBufferSize write SetSftpBufferSize;
      property SocketTimeout: Integer read FSocketTimeout write SetSocketTimeout
          default 0;
      property SocksAuthentication: TElSocksAuthentication read FSocksAuthentication
          write FSocksAuthentication;
      property SocksPassword: string read FSocksPassword write FSocksPassword;
      property SocksPort: Integer read FSocksPort write FSocksPort default 1080;
      property SocksResolveAddress: boolean read FSocksResolveAddress write
          FSocksResolveAddress default false;
      property SocksServer: string read FSocksServer write FSocksServer;
      property SocksUserCode: string read FSocksUserCode write FSocksUserCode;
      property SocksVersion: TElSocksVersion read FSocksVersion write FSocksVersion
          default elSocks5;
      property SoftwareName: string read GetSoftwareName write SetSoftwareName;
      property TransferBlockSize: Integer read FTransferBlockSize write
          FTransferBlockSize stored false default 65535;
      property AutoAdjustTransferBlock : boolean read FAutoAdjustTransferBlock
          write FAutoAdjustTransferBlock default true;
      property UseInternalSocket: boolean read FUseInternalSocket write
          SetUseInternalSocket default true;
      property Username: string read GetUsername write SetUsername;
      property UseSocks: boolean read FUseSocks write SetUseSocks default false;
      property UseUTF8 : boolean read GetUseUTF8 write SetUseUTF8;
    //  Web tunneling support
      property UseWebTunneling: boolean read FUseWebTunneling write
          SetUseWebTunneling default false;
      property Versions: TSBSftpVersions read GetVersions write SetVersions
        default [sbSFTP3];
      property WebTunnelAddress: string read FWebTunnelAddress write
          FWebTunnelAddress;
      property WebTunnelAuthentication: TElWebTunnelAuthentication read
          FWebTunnelAuthentication write FWebTunnelAuthentication 
          default wtaNoAuthentication;
      property LocalAddress: string read GetLocalAddress write SetLocalAddress;
      property LocalPort: Integer read GetLocalPort write SetLocalPort;
      property PipelineLength: Integer read FPipelineLength write SetPipelineLength default 1;
    property DownloadBlockSize: Integer read FDownloadBlockSize write
        SetDownloadBlockSize;
    property UploadBlockSize: Integer read FUploadBlockSize write
        SetUploadBlockSize;
      property WebTunnelPassword: string read FWebTunnelPassword write
          FWebTunnelPassword;
      property WebTunnelPort: Integer read FWebTunnelPort write FWebTunnelPort;
      property WebTunnelUserId: string read FWebTunnelUserId write FWebTunnelUserId;
    end;

    EElSimpleSFTPClientException =  class(ESecureBlackboxError);

procedure Register;

implementation

resourcestring
  SConnAlreadyEstablished = 'SFTP Connection already established';
  SConnNotPresent = 'SFTP Connection not present';
  SConnClosedByUser = 'SFTP Connection closed by user';
  SConnError = 'Connection failed';
  SSftpConnFailed = 'Failed to establish SFTP connection';
  SConnLost  = 'Connection lost';
  SConnTimeout = 'Connection timed out';

procedure Register;
begin
  RegisterComponents('SFTPBlackbox', [TElSimpleSftpClient]);
end;

constructor TElSimpleSFTPClient.Create(AOwner : TSBComponentBase);
begin
  inherited Create(AOwner);

  FClient := TElSSHClient.Create(nil);
  FClient.Versions := [sbSSH2];
  FClient.OnError := DoError;
  FClient.OnAuthenticationFailed := DoAuthenticationFailed;
  FClient.OnAuthenticationSuccess := DoAuthenticationSuccess;
  FClient.OnAuthenticationKeyboard := DoAuthenticationKeyboard;
  FClient.OnCloseConnection := DoCloseConnection;
  FClient.OnSend := DoSend;
  FClient.OnReceive := DoReceive;
  FClient.OnKeyValidate := DoKeyValidate;
  FClient.OnBanner := DoBanner;

  FTunnelList := TElSSHTunnelList.Create(nil);
  FClient.TunnelList := FTunnelList;

  FTunnel := TElSubsystemSSHTunnel.Create(nil);
  FTunnel.TunnelList := FTunnelList;

  FSFTPClient := TElSFTPClient.Create(nil);
  FSFTPClient.SynchronousMode := true;
  FSFTPClient.Tunnel := FTunnel;
  FSFTPClient.OnDirectoryListing := HandleDirectoryListing;
  FSFTPClient.OnOpenConnection := HandleSftpOpen;
  FSFTPClient.OnCloseConnection := HandleSftpClose;
  FSFTPClient.OnAbsolutePath := HandleAbsolutePath;
  FSFTPClient.OnError := HandleSftpError;
  FSFTPClient.OnFileAttributes := HandleAttributes;
  FSFTPClient.OnData := HandleSftpData;

  FSFTPClient.MessageLoop := HandleSftpMessageLoop;
  FMessageLoop := nil;


  FTransferBlockSize := 65535;
  FPipelineLength := 1;
  FAutoAdjustTransferBlock := true;

  FSocket := TElSocket.Create;
  FSocket.Port := 22;
  FSocket.Address := '';

  FUseInternalSocket := true;
  FUseSocks := False;
  FSocksPort := 1080;
  FSocksUserCode := '';
  FSocksPassword := '';
  FSocksVersion := elSocks5;
  FWebTunnelPort := 3128;

  AuthenticationTypes := SSH_AUTH_TYPE_PASSWORD;
end;


destructor TElSimpleSFTPClient.Destroy;
begin
  FreeAndNil(FClient);
  FreeAndNil(FTunnelList);
  FreeAndNil(FTunnel);
  FreeAndNil(FSFTPClient);
  inherited;
  FreeAndNil(FSocket);
end;


procedure TElSimpleSFTPClient.Close(Silent : boolean {$ifdef HAS_DEF_PARAMS}= false{$endif});
begin
  try
    if FClient.Active then
      FClient.Close(Silent);
  except
  end;
  if UseInternalSocket and (FSocket.State >= issConnected) then
    FSocket.Close(Silent);
end;

procedure TElSimpleSFTPClient.CloseHandle(const Handle: TSBSftpFileHandle);
begin
  FLastOpTick := GetTickCount();
  FSFTPClient.CloseHandleSync(Handle);
end;

function TElSimpleSFTPClient.CreateFile(const Filename: string):
    TSBSftpFileHandle;
begin
  FLastOpTick := GetTickCount();
  result := FSFTPClient.CreateFileSync(FileName);
end;

procedure TElSimpleSFTPClient.CreateSymLink(const LinkPath: string; const
    TargetPath: string);
begin
  FLastOpTick := GetTickCount();
  FSFTPClient.CreateSymLinkSync(LinkPath, TargetPath);
end;

procedure TElSimpleSFTPClient.CreateHardLink(const LinkPath: string; const TargetPath: string);
begin
  FLastOpTick := GetTickCount();
  FSFTPClient.CreateHardLinkSync(LinkPath, TargetPath);
end;

procedure TElSimpleSFTPClient.DataAvailable;
var Time : integer;
begin
    if (not FDoUseInternalSocket) then
      FClient.DataAvailable
    else
    begin
      if SocketTimeout = 0 then
        Time := -1
      else
        Time := 200;
      if FSocket.CanReceive(Time) then
        FClient.DataAvailable;
    end;
end;

procedure TElSimpleSFTPClient.DoAuthenticationFailed(Sender : TObject;
    AuthenticationType : integer);
begin
  if assigned(FOnAuthenticationFailed) then
    FOnAuthenticationFailed(Self, AuthenticationType);
end;

procedure TElSimpleSFTPClient.DoAuthenticationKeyboard(Sender : TObject; Prompts
    : TStringList; Echo : TBits; Responses : TStringList);
begin
  if Assigned(FOnAuthenticationKeyboard) then
    FOnAuthenticationKeyboard(Self, Prompts, Echo, Responses);
end;


procedure TElSimpleSFTPClient.DoBanner(Sender: TObject; const Text : string; const Language : string);
begin
  if assigned(FOnBanner) then
    FOnBanner(Self, Text, Language);
end;

procedure TElSimpleSFTPClient.DoAuthenticationSuccess(Sender : TObject);
begin
  if Assigned(FOnAuthenticationSuccess) then
    FOnAuthenticationSuccess(Self);
end;

procedure TElSimpleSFTPClient.DoClose(Force: boolean);
begin
  FClient.Close(Force);
  if FDoUseInternalSocket then
  begin
    try
      if FSocket.State in [issConnected, issConnecting] then
        FSocket.Close(true);
    except
    end;
  end;
end;

procedure TElSimpleSFTPClient.DoCloseConnection(Sender : TObject);
begin
  if FDoUseInternalSocket then
    try
      FSocket.Close(false);
    except
    end;
  if assigned(FOnCloseConnection) then
    FOnCloseConnection(Self);
end;

procedure TElSimpleSFTPClient.DoError(Sender : TObject; ErrorCode : integer);
begin
  FErrorCode := ErrorCode;
  if FDoUseInternalSocket then
    try
      FSocket.Close(true);
    except
    end;
  if assigned(FOnError) then
    FOnError(Self, ErrorCode);
end;

procedure TElSimpleSFTPClient.DoKeyValidate(Sender : TObject; ServerKey :
  TElSSHKey; var Validate : boolean);
begin
  if assigned(FOnKeyValidate) then
    FOnKeyValidate(Self, ServerKey, Validate)
  else
    Validate := true;
end;

function TElSimpleSFTPClient.DoMessageLoop: boolean;
begin
  Result := IntMessageLoop;
  if Result and FDoUseInternalSocket and (SocketTimeout > 0) then
  begin
    if TickDiff(FLastOpTick, GetTickCount) > SocketTimeout then
      raise EElSocketError.Create(SConnTimeout);
  end;
  if Result and Assigned(FMessageLoop) then
    result := FMessageLoop();
end;

procedure TElSimpleSFTPClient.DoReceive(Sender : TObject; Buffer : pointer;
    MaxSize : longint; out Written : longint);
//var Time : integer;
begin
  //assert(MaxSize > 0);
  if FDoUseInternalSocket then
  begin
    FLastOpTick := GetTickCount();

(*    if SocketTimeout = 0 then
        Time := -1
      else
        Time := 200;

    if not FSocket.CanReceive(Time) then
    begin
      if TickDiff(FLastOpTick, GetTickCount) > SocketTimeout then
        raise EElSocketError.Create(SConnTimeout);

      Written := 0;
      exit;
    end;
    *)
    if (FSocket.Receive(Buffer, MaxSize, Written) <> 0) or (Written = 0) then
      raise EElSimpleSFTPClientException.Create(SConnLost);
    FLastOpTick := GetTickCount();
    //except
    //  Written := 0;
    //end;
  end
  else
  if assigned(FOnReceive) then
    FOnReceive(Self, Buffer, MaxSize, Written)
  else
    Written := 0;
end;

procedure TElSimpleSFTPClient.DoSend(Sender : TObject; Buffer : pointer; Size :
    longint);
var ToSend, Sent : integer;
    ErrorCode : integer;
//    Time : integer;
begin
  if FDoUseInternalSocket then
  begin
    Sent := 0;
    ToSend := Size;

    FLastOpTick := GetTickCount();
    while ToSend > 0 do
    begin
      try

//        if SocketTimeout = 0 then
//          Time := -1
//        else
//          Time := 200;

        if true then

        //if FSocket.CanSend(Time) then
        begin
          ErrorCode := FSocket.Send(Pointer(Integer(Buffer) + (Size - ToSend)), ToSend, Sent);
          if (ErrorCode <> 0)
  {$IFNDEF LINUX}
          and (ErrorCode <> WSAEWOULDBLOCK) and (ErrorCode <> WSAEISCONN) and (ErrorCode <> WSAEINPROGRESS)
  {$ELSE}
          and (ErrorCode <> EWOULDBLOCK)
  {$ENDIF}
          then
            raise EElSimpleSFTPClientException.Create(SConnLost);
          Dec(ToSend, Sent);
          FLastOpTick := GetTickCount();
        end
        else
        begin
          if TickDiff(FLastOpTick, GetTickCount) > SocketTimeout then
            raise EElSocketError.Create(SConnTimeout);
        end;
      except
        on E : EElSocketError do
        begin
          DoClose(true);
          raise EElSimpleSFTPClientException.Create(SConnLost);
        end;
      end;
    end;
  end
  else
  if Assigned(FOnSend) then
    FOnSend(Self, Buffer, Size);
end;

procedure TElSimpleSFTPClient.DownloadFile(RemoteFileName, LocalFileName : string);
var Stream : TStream;
    Handle : TSBSftpFileHandle;
    Buffer : ByteArray;
    Recv   : integer;
    Total  : integer;
    BlockSize : integer;
    Cancel : boolean;
begin
  if FAutoAdjustTransferBlock or (DownloadBlockSize = 0) then
    BlockSize := SFTP_OPTIMAL_DOWNLOAD_BLOCK_SIZE
  else
    BlockSize := DownloadBlockSize;

  BlockSize := BlockSize * PipelineLength;

  FErrorCode := 0;
  Stream := TFileStream.Create(LocalFileName, Classes.fmCreate or fmShareDenyWrite);
  try
    Handle := OpenFile(RemoteFileName, [SBSftpCommon.fmRead], TElSftpFileAttributes(nil));
    try
      if (Handle <> '') then
      begin
        SetLength(Buffer, BlockSize);
        Total := 0;
        while true do
        begin
          Recv := Self.Read(Handle, Total, @Buffer[0], BlockSize);
          if Recv > 0 then
          begin
            inc(Total, Recv);
            Stream.WriteBuffer(Buffer[0], Recv);
          end;
          if (Recv = 0) or (FErrorCode = SSH_ERROR_EOF) then
            break;
          Cancel := false;
          DoProgress(-1, Total, Cancel);
          if Cancel then
            raise EOperationCancelledError.Create(SOperationCancelled);
        end;
      end;
    finally
      if Active and ((not UseInternalSocket) or (UseInternalSocket and (FSocket.State = issConnected))) then
        CloseHandle(Handle);
    end;
  finally
    Stream.Free;
  end;
end;

procedure TElSimpleSFTPClient.UploadFile(LocalFileName, RemoteFileName :
    string);
var Stream : TStream;
    Handle : TSBSftpFileHandle;
    Buffer : ByteArray;
    Recv   : integer;
    Total  : integer;
    BlockSize : integer;
    Cancel : boolean;
begin
  if FAutoAdjustTransferBlock or (UploadBlockSize = 0) then
    BlockSize := SFTP_OPTIMAL_UPLOAD_BLOCK_SIZE
  else
    BlockSize := UploadBlockSize;

  BlockSize := BlockSize * PipelineLength;

  Stream := TFileStream.Create(LocalFileName, fmOpenRead or fmShareDenyWrite);
  try
    Handle := OpenFile(RemoteFileName, [SBSftpCommon.fmCreate, SBSftpCommon.fmWrite], TElSftpFileAttributes(nil));
    try
      if (Handle <> '') then
      begin
        SetLength(Buffer, BlockSize);
        Total := 0;
        while true do
        begin
          Recv := Stream.Read(Buffer[0], BlockSize);
          if Recv > 0 then
          begin
            Self.Write(Handle, Total, @Buffer[0], Recv);
            inc(Total, Recv);
          end;
          if (Recv < BlockSize) then
            break;
          Cancel := false;
          DoProgress(Stream.Size, Total, Cancel);
          if Cancel then
            raise EOperationCancelledError.Create(SOperationCancelled); 
        end;
      end;
    finally
      if Active and ((not UseInternalSocket) or (UseInternalSocket and (FSocket.State = issConnected))) then
        CloseHandle(Handle);
    end;
  finally
    Stream.Free;
  end;
end;

function TElSimpleSFTPClient.GetActive: boolean;
begin
  Result := FSFTPClient.Active;
end;

function TElSimpleSFTPClient.GetAddress: string;
begin
  Result := FSocket.Address;
end;

function TElSimpleSFTPClient.GetAuthenticationTypes: integer;
begin
  Result := FClient.AuthenticationTypes;
end;

function TElSimpleSFTPClient.GetClientHostname: string;
begin
  Result := FClient.ClientHostName;
end;

function TElSimpleSFTPClient.GetClientUsername: string;
begin
  Result := FClient.ClientUserName;
end;

function TElSimpleSFTPClient.GetCompressionAlgorithmCS:
  TSSHCompressionAlgorithm;
begin
  Result := FClient.CompressionAlgorithmClientToServer;
end;

function TElSimpleSFTPClient.GetCompressionAlgorithms(Index :
  TSSHCompressionAlgorithm): boolean;
begin
  Result := FClient.CompressionAlgorithms[Index];
end;

function TElSimpleSFTPClient.GetCompressionAlgorithmSC:
  TSSHCompressionAlgorithm;
begin
  Result := FClient.CompressionAlgorithmServerToClient;
end;

function TElSimpleSFTPClient.GetCompressionLevel: integer;
begin
  Result := FClient.CompressionLevel;
end;

function TElSimpleSFTPClient.GetEncryptionAlgorithmCS:
  TSSHEncryptionAlgorithm;
begin
  Result := FClient.EncryptionAlgorithmClientToServer;
end;

function TElSimpleSFTPClient.GetEncryptionAlgorithms(Index :
  TSSHEncryptionAlgorithm): boolean;
begin
  Result := FClient.EncryptionAlgorithms[Index];
end;

function TElSimpleSFTPClient.GetEncryptionAlgorithmSC: TSSHEncryptionAlgorithm;
begin
  Result := FClient.EncryptionAlgorithmServerToClient;
end;

function TElSimpleSFTPClient.GetForceCompression: boolean;
begin
  Result := FClient.ForceCompression;
end;

function TElSimpleSFTPClient.GetKexAlgorithm: TSSHKexAlgorithm;
begin
  Result := FClient.KexAlgorithm;
end;

function TElSimpleSFTPClient.GetKexAlgorithms(Index : TSSHKexAlgorithm):
    boolean;
begin
  Result := FClient.KexAlgorithms[Index];
end;

function TElSimpleSFTPClient.GetKeyStorage: TElSSHCustomKeyStorage;
begin
  Result := FClient.KeyStorage;
end;

function TElSimpleSFTPClient.GetLastSyncComment: string;
begin
  Result := FSFTPClient.LastSyncComment;
end;

function TElSimpleSFTPClient.GetLastSyncError: Integer;
begin
  Result := FSFTPClient.LastSyncError;
end;

function TElSimpleSFTPClient.GetMacAlgorithmCS: TSSHMacAlgorithm;
begin
  Result := FClient.MacAlgorithmClientToServer;
end;

function TElSimpleSFTPClient.GetMACAlgorithms(Index : TSSHMacAlgorithm):
    boolean;
begin
  Result := FClient.MACAlgorithms[Index];
end;

function TElSimpleSFTPClient.GetMacAlgorithmSC: TSSHMacAlgorithm;
begin
  Result := FClient.MacAlgorithmServerToClient;
end;

function TElSimpleSFTPClient.GetPassword: string;
begin
  Result := FClient.Password;
end;

function TElSimpleSFTPClient.GetPort: Integer;
begin
  Result := FSocket.Port;
end;

function TElSimpleSFTPClient.GetPublicKeyAlgorithm: TSSHPublicKeyAlgorithm;
begin
  Result := FClient.PublicKeyAlgorithm;
end;

function TElSimpleSFTPClient.GetPublicKeyAlgorithms(Index :
  TSSHPublicKeyAlgorithm): boolean;
begin
  Result := FClient.PublicKeyAlgorithms[Index];
end;

function TElSimpleSFTPClient.GetServerCloseReason: string;
begin
  Result := FClient.ServerCloseReason;
end;

function TElSimpleSFTPClient.GetServerSoftwareName: string;
begin
  Result := FClient.ServerSoftwareName;
end;

function TElSimpleSFTPClient.GetSftpBufferSize: Integer;
begin
  Result := FSFTPClient.SftpBufferSize;
end;

function TElSimpleSFTPClient.GetSoftwareName: string;
begin
  Result := FClient.SoftwareName;
end;

function TElSimpleSFTPClient.GetUsername: string;
begin
  Result := FClient.Username;
end;

function TElSimpleSFTPClient.GetVersion: TSBSftpVersion;
begin
  Result := FSFTPClient.Version;
end;

function TElSimpleSFTPClient.GetVersions: TSBSftpVersions;
begin
  Result := FSFTPClient.Versions;
end;

function TElSimpleSFTPClient.GetUseUTF8 : boolean;
begin
  Result := FSFTPClient.UseUTF8;
end;

function TElSimpleSFTPClient.GetExtendedProperties : TElSFTPExtendedProperties;
begin
  Result := FSFTPClient.ExtendedProperties;
end;

procedure TElSimpleSFTPClient.HandleAbsolutePath(Sender : TObject; const Path :
    string);
begin
  FOpStrParams := Path;
end;

procedure TElSimpleSFTPClient.HandleAttributes(Sender: TObject; Attributes:
    TElSftpFileAttributes);
begin
  if FCurOp = soRequestAttributes then
    Attributes.CopyTo(TElSftpFileAttributes(FOpParams));
end;

procedure TElSimpleSFTPClient.HandleSFTPError(Sender: TObject; ErrorCode:
    integer; const Comment: string);
begin
  Self.FErrorCode := ErrorCode;  
end;

{$ifndef BUILDER_USED}
procedure TElSimpleSFTPClient.HandleDirectoryListing(Sender: TObject; Listing: array of TElSftpFileInfo);
{$else}
procedure TElSimpleSFTPClient.HandleDirectoryListing(Sender: TObject; Listing: TList);
{$endif}
var i : integer;
    Info : TElSftpFileInfo;
begin
  if FCurOp = soReadSymLink then
    TElSftpFileInfo(Listing[0]).CopyTo(TElSftpFileInfo(FOpParams))
  else
  if FCurOp = soReadDirectory then
  begin
    {$ifndef BUILDER_USED}
    for i := 0 to Length(Listing) - 1 do
    {$else}
    for i := 0 to Listing.Count - 1 do
    {$endif}
    begin
      Info := TElSftpFileInfo.Create;
      TElSftpFileInfo(Listing[i]).CopyTo(Info);
      TList(FOpParams).Add(Info);
    end;
  end;
end;

procedure TElSimpleSFTPClient.HandleSftpData(Sender : TObject; Buffer : pointer; Size : integer);
begin
  if FCurOp = soRead then
  begin
    if Buffer <> FDataBuf then
    begin
      Move(Buffer^, FDataBuf^, Size);
    end;
    FReadSomething := true;
    FDataSize := Size;
  end;
end;

procedure TElSimpleSFTPClient.HandleSftpOpen(Sender : TObject);
begin
  FOpened := true;
end;

procedure TElSimpleSFTPClient.HandleSftpClose(Sender : TObject);
begin
  FOpened := false;
end;

procedure TElSimpleSFTPClient.Block(const Handle: TSBSftpFileHandle; Offset, Length : Int64;
  LockMask : TSBSftpFileOpenAccess);
begin
  FLastOpTick := GetTickCount();
  FSFTPClient.BlockSync(Handle, Offset, Length, LockMask);
end;

procedure TElSimpleSFTPClient.Unblock(const Handle: TSBSftpFileHandle; Offset, Length : Int64);
begin
  FLastOpTick := GetTickCount();
  FSFTPClient.UnblockSync(Handle, Offset, Length);
end;

procedure TElSimpleSFTPClient.MakeDirectory(const Path: string; Attributes:
    TElSftpFileAttributes);
begin
  FLastOpTick := GetTickCount();
  FSFTPClient.MakeDirectorySync(Path, Attributes);
end;

procedure TElSimpleSFTPClient.Open;
begin
  if Active then
    raise EElSimpleSFTPClientException.Create(SConnAlreadyEstablished);

  FDoUseInternalSocket := FUseInternalSocket and (FSocket.Address <> '');

  //System.Console.WriteLine('Started at ' + Int32(System.Environment.TickCount).ToString());
  if FDoUseInternalSocket then
  begin
    try
      FSocket.Init(istStream);
      FSocket.UseSocks := UseSocks;
      FSocket.SocksAuthentication := SocksAuthentication;
      FSocket.SocksPassword := SocksPassword;
      FSocket.SocksUserCode := SocksUserCode;
      FSocket.SocksPort := SocksPort;
      FSocket.SocksResolveAddress := SocksResolveAddress;
      FSocket.SocksServer := SocksServer;
      FSocket.SocksPort := SocksPort;
      FSocket.SocksVersion := SocksVersion;
      FSocket.UseWebTunneling := UseWebTunneling;
      FSocket.WebTunnelAddress := WebTunnelAddress;
      FSocket.WebTunnelPort := WebTunnelPort;
      FSocket.WebTunnelAuthentication := WebTunnelAuthentication;
      FSocket.WebTunnelUserId := WebTunnelUserId;
      FSocket.WebTunnelPassword := WebTunnelPassword;

      if FSocket.Connect(Self.SocketTimeout) <> 0 then
        raise EElSimpleSFTPClientException.Create(SConnError);

      FLastOpTick := GetTickCount();
    except
      on E : EElSocketError do
      begin
        raise EElSimpleSFTPClientException.Create(SConnError);
      end;
    end;
  end;
  FErrorCode := 0;
  //System.Console.WriteLine('Socket connected at ' + Int32(System.Environment.TickCount).ToString());
  FClient.Open;
  while (not FClient.Active) and (FErrorCode = 0) do
  begin
    if not DoMessageLoop then
    {
      DataAvailable
    else
    }
      raise EElSimpleSFTPClientException.Create(SConnClosedByUser);
  end;
  //System.Console.WriteLine('SSH connected at ' + Int32(System.Environment.TickCount).ToString());
  if FClient.Active then
  begin
    while (not FOpened) and (FErrorCode = 0) do
    begin
      if DoMessageLoop then
      begin
        if FOpened or (FErrorCode <> 0) then break;
        //  DataAvailable;
      end
      else
        raise EElSimpleSFTPClientException.Create(SSftpConnFailed);
    end;
    //System.Console.WriteLine('SFTP opened at ' + Int32(System.Environment.TickCount).ToString());
    if FOpened then
      FSFTPClient.Open;

    if not FSFTPClient.Active then
      raise EElSimpleSFTPClientException.Create(SSftpConnFailed);
  end;
  //System.Console.WriteLine('SFTP connection complete at ' + Int32(System.Environment.TickCount).ToString());

  if not FClient.Active then
    raise EElSimpleSFTPClientException.Create(SConnError);
end;

function TElSimpleSFTPClient.OpenDirectory(const Path: string):
    TSBSFTPFileHandle;
begin
  FLastOpTick := GetTickCount();
  result := FSFTPClient.OpenDirectorySync(Path);
end;

function TElSimpleSFTPClient.OpenFile(const Filename: string; Modes:
    TSBSftpFileOpenModes; Attributes: TElSftpFileAttributes): TSBSFTPFileHandle;
begin
  FLastOpTick := GetTickCount();
  result := FSFTPClient.OpenFileSync(FileName, Modes, Attributes);
end;

function TElSimpleSFTPClient.OpenFile(const Filename: string; Modes: TSBSftpFileOpenModes;
  Access : TSBSftpFileOpenAccess; Attributes : TElSftpFileAttributes) : TSBSftpFileHandle;
begin
  FLastOpTick := GetTickCount();
  result := FSFTPClient.OpenFileSync(FileName, Modes, Access, Attributes);
end;

procedure TElSimpleSFTPClient.ReadDirectory(const Handle: TSBSftpFileHandle; Listing: TList);
begin
  FCurOp := soReadDirectory;
  FOpParams := Listing;
  FLastOpTick := GetTickCount();
  while FSFTPClient.ReadDirectorySync(Handle) do ;
end;

procedure TElSimpleSFTPClient.ReadSymLink(const Path: string; Info : TElSftpFileInfo);
begin
  FCurOp := soReadSymLink;
  FOpParams := Info;
  FLastOpTick := GetTickCount();
  FSFTPClient.ReadSymLinkSync(Path);
end;

function TElSimpleSFTPClient.Read(const Handle: TSBSftpFileHandle; Offset:
    Int64; Buffer : Pointer; Length: integer): integer;
begin
  FCurOp := soRead;
  FDataBuf := Buffer;
  FDataSize := Length;
  FReadSomething := false;
  FLastOpTick := GetTickCount();
  FSFTPClient.ReadSync(Handle, Offset,  FDataBuf, Length);
  if FReadSomething then
    Result := FDataSize
  else
    Result := 0;
end;


procedure TElSimpleSFTPClient.RemoveDirectory(const Path: string);
begin
  FLastOpTick := GetTickCount();
  FSFTPClient.RemoveDirectorySync(Path);
end;

procedure TElSimpleSFTPClient.RemoveFile(const Filename: string);
begin
  FLastOpTick := GetTickCount();
  FSFTPClient.RemoveFileSync(FileName);
end;

procedure TElSimpleSFTPClient.RenameFile(const OldPath, NewPath: string);
begin
  FLastOpTick := GetTickCount();
  FSFTPClient.RenameFileSync(OldPath, NewPath);
end;

function TElSimpleSFTPClient.RequestAbsolutePath(const Path: string) : string;
begin
  FLastOpTick := GetTickCount();
  FCurOp := soRequestAbsolutePath;
  FSFTPClient.RequestAbsolutePathSync(Path);
  Result := FOpStrParams;
end;

function TElSimpleSFTPClient.RequestAbsolutePathEx(const Path: string;
  Control : TSBSftpRealpathControl; ComposePath : TStringList) : string;
begin
  FLastOpTick := GetTickCount();
  FCurOp := soRequestAbsolutePath;
  FSFTPClient.RequestAbsolutePathExSync(Path, Control, ComposePath);
  Result := FOpStrParams;
end;

procedure TElSimpleSFTPClient.RequestAttributes(const Path: string;
    FollowSymLinks: boolean; Attributes : TElSftpFileAttributes);
begin
  FLastOpTick := GetTickCount();
  FCurOp := soRequestAttributes;
  FOpParams := Attributes;
  FSFTPClient.RequestAttributesSync(Path, FollowSymLinks);
end;

procedure TElSimpleSFTPClient.RequestAttributes(const Handle: TSBSftpFileHandle;
    Attributes : TElSftpFileAttributes);
begin
  FLastOpTick := GetTickCount();
  FCurOp := soRequestAttributes;
  FOpParams := Attributes;
  FSFTPClient.RequestAttributesSync(Handle);
end;

procedure TElSimpleSFTPClient.SetAttributes(const Path: string; Attributes:
    TElSftpFileAttributes);
begin
  FLastOpTick := GetTickCount();
  FSFTPClient.SetAttributesSync(Path, Attributes);
end;

procedure TElSimpleSFTPClient.SetAttributesByHandle(const Handle: TSBSftpFileHandle;
  Attributes: TElSftpFileAttributes);
begin
  FLastOpTick := GetTickCount();
  FSFTPClient.SetAttributesByHandleSync(Handle, Attributes);
end;

procedure TElSimpleSFTPClient.TextSeek(const Handle: TSBSftpFileHandle;
  LineNumber: Int64);
begin
  FLastOpTick := GetTickCount();
  FSFTPClient.TextSeekSync(Handle, LineNumber);
end;

procedure TElSimpleSFTPClient.SetAuthenticationTypes(Value : integer);
begin
  FClient.AuthenticationTypes := Value;
end;

procedure TElSimpleSFTPClient.SetClientHostname(Value : string);
begin
  FClient.ClientHostname := Value;
end;

procedure TElSimpleSFTPClient.SetClientUsername(Value : string);
begin
  FClient.ClientUsername := Value;
end;

procedure TElSimpleSFTPClient.SetCompressionAlgorithms(Index :
  TSSHCompressionAlgorithm;
  Value : boolean);
begin
  FClient.CompressionAlgorithms[Index] := Value;
end;

procedure TElSimpleSFTPClient.SetCompressionLevel(Value : integer);
begin
  FClient.CompressionLevel := Value;
end;

procedure TElSimpleSFTPClient.SetEncryptionAlgorithms(Index :
  TSSHEncryptionAlgorithm;
  Value : boolean);
begin
  FClient.EncryptionAlgorithms[Index] := Value;
end;

procedure TElSimpleSFTPClient.SetForceCompression(Value : boolean);
begin
  FClient.ForceCompression := Value;
end;

procedure TElSimpleSFTPClient.SetKexAlgorithms(Index : TSSHKexAlgorithm;
  Value : boolean);
begin
  FClient.KexAlgorithms[Index] := Value;
end;

procedure TElSimpleSFTPClient.SetKeyStorage(Value : TElSSHCustomKeyStorage);
begin
  FClient.KeyStorage := Value;
end;

procedure TElSimpleSFTPClient.SetMACAlgorithms(Index : TSSHMacAlgorithm;
  Value : boolean);
begin
  FClient.MACAlgorithms[Index] := Value;
end;

procedure TElSimpleSFTPClient.SetPassword(Value : string);
begin
  FClient.Password := Value;
end;

procedure TElSimpleSFTPClient.SetPublicKeyAlgorithms(Index :
  TSSHPublicKeyAlgorithm;
  Value : boolean);
begin
  FClient.PublicKeyAlgorithms[Index] := Value;
end;

procedure TElSimpleSFTPClient.SetSoftwareName(Value : string);
begin
  FClient.SoftwareName := Value;
end;

procedure TElSimpleSFTPClient.SetUsername(Value : string);
begin
  FClient.Username := Value;
end;

procedure TElSimpleSFTPClient.SetVersions(Value: TSBSftpVersions);
begin
  FSFTPClient.Versions := Value;
end;

procedure TElSimpleSFTPClient.Write(const Handle: TSBSftpFileHandle; Offset:
    Int64; Buffer: Pointer; Size : integer);
var
  ChunkSize : integer;
  BlockSize : integer;
begin
  if FAutoAdjustTransferBlock or (UploadBlockSize = 0) then
    BlockSize := SFTP_OPTIMAL_UPLOAD_BLOCK_SIZE
  else
    BlockSize := UploadBlockSize;

  BlockSize := BlockSize * PipelineLength;

  while Size > 0 do
  begin
    ChunkSize := Min(Size, BlockSize);
    FLastOpTick := GetTickCount();
    FSFTPClient.WriteSync(Handle, Offset, Buffer, ChunkSize);
    Inc(Offset, ChunkSize);
    Dec(Size, ChunkSize);
    Buffer := @PByteArray(Buffer)[ChunkSize];
  end;
end;


{$IFNDEF CLX_USED}
function TElSimpleSftpClient.IntMessageLoop: boolean;
var
  Msg: TMsg;
begin
  if not FDoUseInternalSocket then
  begin
    result := true;
    DataAvailable;
    while PeekMessage(Msg, 0, 0, 0, PM_REMOVE) do
    begin
      if (Msg.message = WM_QUIT) then
      begin
        result := false;
        break;
      end
      else
      begin
        TranslateMessage(Msg);
        DispatchMessage(Msg);
      end;
    end;
  end
  else
  begin
    DataAvailable;
    result := true;
  end;
end;

{$ELSE}
function TElSimpleSftpClient.IntMessageLoop: boolean;
begin
  DataAvailable;
  result := true;
end;
{$ENDIF}

function TElSimpleSftpClient.HandleSftpMessageLoop : boolean;
begin
  //DataAvailable;
  Result := DoMessageLoop();
end;

procedure TElSimpleSFTPClient.SetAddress(const Value: string);
begin
  if FSocket.Address <> Value then
  begin
    if (not Active) then
      FSocket.Address := Value
    else
      raise EElSimpleSFTPClientException.Create(SConnAlreadyEstablished);
  end;
end;


procedure TElSimpleSFTPClient.SetPort(Value: Integer);
begin
  if FSocket.Port <> Value then
  begin
    if (not Active) then
      FSocket.Port := Value
    else
      raise EElSimpleSFTPClientException.Create(SConnAlreadyEstablished);
  end;
end;

procedure TElSimpleSFTPClient.SetSftpBufferSize(Value: Integer);
begin
  if (FSFTPClient.SftpBufferSize <> Value) and (not Active) and (Value > 0) then
  begin
    FSFTPClient.SftpBufferSize := Value;
  end;
end;

procedure TElSimpleSFTPClient.SetSocketTimeout(Value: Integer);
begin
  if (FSocketTimeout <> Value) and (FSocketTimeout >= 0) then
  begin
    FSocketTimeout := Value;
  end;
end;

procedure TElSimpleSFTPClient.SetUseInternalSocket(Value: boolean);
begin
  if FUseInternalSocket <> Value then
  begin
    FUseInternalSocket := Value;
  end;
end;

procedure TElSimpleSFTPClient.SetUseSocks(const Value: boolean);
begin
  FUseSocks := Value;
  if Value then
    UseWebTunneling := false;
end;

procedure TElSimpleSFTPClient.SetUseWebTunneling(const Value: boolean);
begin
  FUseWebTunneling := Value;
  if Value then
    UseSocks := false;
end;

procedure TElSimpleSFTPClient.SetUseUTF8(Value : boolean);
begin
  FSFTPClient.UseUTF8 := Value;
end;

procedure TElSimpleSFTPClient.DoProgress(Total, Current : Int64; var Cancel : boolean);
begin
  if assigned(FOnProgress) then
    FOnProgress(Self, Total, Current, Cancel);
end;

function TElSimpleSFTPClient.GetLocalAddress: string;
begin
  Result := FSocket.ListenAddress;
end;

function TElSimpleSFTPClient.GetLocalPort: Integer;
begin
  Result := FSocket.ListenPort;
end;

procedure TElSimpleSFTPClient.SetLocalAddress(const Value: string);
begin
  FSocket.ListenAddress := Value;
end;

procedure TElSimpleSFTPClient.SetLocalPort(const Value: Integer);
begin
  FSocket.ListenPort := Value;
end;

function TElSimpleSFTPClient.GetAuthOrder() : TSBSSHAuthOrder;
begin
  result := FClient.SSHAuthOrder;
end;

procedure TElSimpleSFTPClient.SetAuthOrder(Value : TSBSSHAuthOrder);
begin
  FClient.SSHAuthOrder := Value;
end;


procedure TElSimpleSFTPClient.SetPipelineLength(const Value: Integer);
begin
  if (FPipelineLength <> Value) and (Value > 0) then
  begin
    FPipelineLength := Value;
  end;
end;

procedure TElSimpleSFTPClient.SetUploadBlockSize(const Value: Integer);
begin
  FUploadBlockSize := Value;
  FSftpClient.UploadBlockSize := Value;
end;

procedure TElSimpleSFTPClient.SetDownloadBlockSize(const Value: Integer);
begin
  FDownloadBlockSize := Value;
  FSftpClient.DownloadBlockSize := Value;
end;

{$ifdef BUILDER_USED}
function TElSimpleSFTPClient.SftpCreateFile(const Filename: string): TSBSftpFileHandle;
begin
  result := CreateFile(Filename);
end;

function TElSimpleSFTPClient.SftpOpenFile(const Filename: string; Modes: TSBSftpFileOpenModes; Attributes:
  TElSftpFileAttributes): TSBSftpFileHandle;
begin
  result := OpenFile(Filename, Modes, Attributes);
end;

function TElSimpleSFTPClient.SftpOpenFile(const Filename: string; Modes: TSBSftpFileOpenModes;
  Access : TSBSftpFileOpenAccess; Attributes: TElSftpFileAttributes): TSBSftpFileHandle;
begin
  result := OpenFile(Filename, Modes, Access, Attributes);
end;

procedure TElSimpleSFTPClient.SftpRemoveDirectory(const Path: string);
begin
  RemoveDirectory(Path);
end;

procedure TElSimpleSFTPClient.SftpRemoveFile(const Filename: string);
begin
  RemoveFile(Filename);
end;
{$endif}

end.

