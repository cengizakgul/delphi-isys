(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSftpCommon;

interface

uses
  SBConstants,
  SBSSHConstants,
  //SBSSHClient,
  SBSSHCommon,
  SBUtils,
  Classes,
  SysUtils
  {$ifndef CLX_USED},
  Windows
,
  Messages
  {$else}
  , libc
  {$endif}
  ;


const

  SSH_ERROR_BASE = Integer(0);
  SSH_ERROR_WRONG_MODE = SSH_ERROR_BASE + Integer(-1);
  SSH_ERROR_OK = Integer(0);
  SSH_ERROR_EOF = SSH_ERROR_BASE + Integer(1);
  SSH_ERROR_NO_SUCH_FILE = SSH_ERROR_BASE + Integer(2);
  SSH_ERROR_PERMISSION_DENIED = SSH_ERROR_BASE + Integer(3);
  SSH_ERROR_FAILURE = SSH_ERROR_BASE + Integer(4);
  SSH_ERROR_BAD_MESSAGE = SSH_ERROR_BASE + Integer(5);
  SSH_ERROR_NO_CONNECTION = SSH_ERROR_BASE + Integer(6);
  SSH_ERROR_CONNECTION_LOST = SSH_ERROR_BASE + Integer(7);
  SSH_ERROR_OP_UNSUPPORTED = SSH_ERROR_BASE + Integer(8);
  SSH_ERROR_INVALID_HANDLE = SSH_ERROR_BASE + Integer(9);
  SSH_ERROR_NO_SUCH_PATH = SSH_ERROR_BASE + Integer(10);
  SSH_ERROR_FILE_ALREADY_EXISTS = SSH_ERROR_BASE + Integer(11);
  SSH_ERROR_WRITE_PROTECT = SSH_ERROR_BASE + Integer(12);
  SSH_ERROR_NO_MEDIA = SSH_ERROR_BASE + Integer(13);
  SSH_ERROR_NO_SPACE_ON_FILESYSTEM = SSH_ERROR_BASE + Integer(14);
  SSH_ERROR_QUOTA_EXCEEDED = SSH_ERROR_BASE + Integer(15);
  SSH_ERROR_UNKNOWN_PRINCIPAL = SSH_ERROR_BASE + Integer(16);
  SSH_ERROR_LOCK_CONFLICT = SSH_ERROR_BASE + Integer(17);
  SSH_ERROR_DIR_NOT_EMPTY = SSH_ERROR_BASE + Integer(18);
  SSH_ERROR_NOT_A_DIRECTORY = SSH_ERROR_BASE + Integer(19);
  SSH_ERROR_INVALID_FILENAME = SSH_ERROR_BASE + Integer(20);
  SSH_ERROR_LINK_LOOP = SSH_ERROR_BASE + Integer(21);
  SSH_ERROR_CANNOT_DELETE = SSH_ERROR_BASE + Integer(22);
  SSH_ERROR_INVALID_PARAMETER = SSH_ERROR_BASE + Integer(23);
  SSH_ERROR_FILE_IS_A_DIRECTORY = SSH_ERROR_BASE + Integer(24);
  SSH_ERROR_BYTE_RANGE_LOCK_CONFLICT = SSH_ERROR_BASE + Integer(25);
  SSH_ERROR_BYTE_RANGE_LOCK_REFUSED = SSH_ERROR_BASE + Integer(26);
  SSH_ERROR_DELETE_PENDING = SSH_ERROR_BASE + Integer(27);
  SSH_ERROR_FILE_CORRUPT = SSH_ERROR_BASE + Integer(28);
  SSH_ERROR_OWNER_INVALID = SSH_ERROR_BASE + Integer(29);
  SSH_ERROR_GROUP_INVALID = SSH_ERROR_BASE + Integer(30);

  SSH_ERROR_UNSUPPORTED_VERSION = SSH_ERROR_BASE + Integer(100);
  SSH_ERROR_INVALID_PACKET = SSH_ERROR_BASE + Integer(101);
  SSH_ERROR_TUNNEL_ERROR = SSH_ERROR_BASE + Integer(102);
  SSH_ERROR_CONNECTION_CLOSED = SSH_ERROR_BASE + Integer(103);


type
  TSBSftpFileOpenMode = (fmRead, fmWrite, fmAppend, fmCreate, fmTruncate,
    fmExcl, fmText, fmOpenOrCreate, fmAppendAtomic, fmNoFollow, fmDeleteOnClose);
  TSBSftpFileOpenModes = set of TSBSftpFileOpenMode;
  TSBSftpFileOpenAccessItem = (faReadLock, faWriteLock, faDeleteLock, faBlockAdvisory);
  TSBSftpFileOpenAccess = set of TSBSftpFileOpenAccessItem;

  TSBSftpVersion = (sbSFTP0, sbSFTP1, sbSFTP2, sbSFTP3, sbSFTP4, sbSFTP5, sbSFTP6 {!!});
  TSBSftpVersions = set of TSBSftpVersion;

  TSBSftpAttribute = (saSize, saOwner, saGroup, saUID, saGID, saPermissions, saATime,
    saMTime, saCTime, saSubSeconds, saAttribBits, saACL, saExtended, saCATime,
    saASize, saTextHint, saMimeType, saLinkCount, saUName);

  TSBSftpAttributes = set of TSBSftpAttribute;
  TSBSftpRenameFlag = (rfOverwrite, rfAtomic, rfNative);
  TSBSftpRenameFlags = set of TSBSftpRenameFlag;

  TSBSftpTextHint =  (thKnownText, thGuessedText, thKnownBinary, thGuessedBinary);
  TSBSftpRealpathControl =  (rcNoCheck, rcStatIf, rcStatAlways);

  TSBSftpFileHandle =  BufferType;

  {.$ifdef BUILDER_USED}
  {.$HPPEMIT '#undef CreateFile'}
  {.$endif}

  EElSFTPError = class(Exception)
  private
    FErrorCode : integer;
  public
    constructor CreateCode(ErrorCode : integer; Comment : string);

    property ErrorCode : integer read FErrorCode;
  end;

  TSBSftpExtendedAttribute = class
  private
    FExtType: string;
    FExtData: string;
  public
    function LoadFromBuffer : boolean; virtual; //load values from ExtData & ExtType
    procedure SaveToBuffer; virtual; //save values to ExtData & ExtType
    function Write : string;

    property ExtType : string
      read FExtType write FExtType;
    property ExtData : string
      read FExtData write FExtData;
  end;

  TElSftpExtendedReply = class
  private
    FReplyData: BufferType;
  public
    function LoadFromBuffer : boolean; virtual; //load data from ReplyData
    procedure SaveToBuffer; virtual; //save values to ReplyData
    function Write : BufferType;

    property ReplyData : BufferType read FReplyData write FReplyData;
  end;

  TElSftpNewlineExtension =  class(TSBSftpExtendedAttribute)
  private
    FNewline : string;
  public
    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property Newline : string read FNewline write FNewline;
  end;

  TElSftpVersionsExtension =  class(TSBSftpExtendedAttribute)
  private
    FVersions : TSBSftpVersions;
    FVersionsStr : string;
  public
    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property Versions : TSBSftpVersions read FVersions write FVersions;
    property VersionsStr : string read FVersionsStr;
  end;

  TElSftpFilenameTranslationExtension =  class(TSBSftpExtendedAttribute)
  private
    FDoTranslate : boolean;
  public
    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property DoTranslate : boolean read FDoTranslate write FDoTranslate;
  end;

  TElSftpFilenameCharsetExtension =  class(TSBSftpExtendedAttribute)
  private
    FCharset : string;
  public
    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property Charset : string read FCharset write FCharset;
  end;

  TElSftpVersionSelectExtension =  class(TSBSftpExtendedAttribute)
  private
    FVersion : TSBSftpVersion;
    FVersionStr : string;

    function GetVersionInt : integer;
    procedure SetVersionInt(Value : integer);
  public
    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property Version : TSBSftpVersion read FVersion write FVersion;
    property VersionInt : integer read GetVersionInt write SetVersionInt;
    property VersionStr : string read FVersionStr write FVersionStr;
  end;

  TElSftpSupportedExtension = class(TSBSftpExtendedAttribute)
  private
    FSupportedAttributes : TSBSftpAttributes;
    FSupportedAttribBits : cardinal;
    FSupportedAccessModes : TSBSftpFileOpenAccess;
    FSupportedOpenModes : TSBSftpFileOpenModes;
    FSupportedAccessMask : cardinal;
    FMaxReadSize : cardinal;
    FSupportedAttrExtensions : TStringList;
    FSupportedExtensions : TStringList;

    FSupportedOpenBlockVector : word;
    FSupportedBlockVector : word;

    FVersion : integer;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Clear;
    procedure FillDefault;
    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;


    function IsSupportedOpenBlockMode(Mode : TSBSftpFileOpenAccess) : boolean;
    procedure SetSupportedOpenBlockMode(Mode : TSBSftpFileOpenAccess; Supported : boolean);
    function IsSupportedBlockMode(Mode : TSBSftpFileOpenAccess) : boolean;
    procedure SetSupportedBlockMode(Mode : TSBSftpFileOpenAccess; Supported : boolean);
    function IsSupportedExtension(ExtName : string) : boolean;
    procedure SetSupportedExtension(ExtName : string; Support : boolean);
    function IsSupportedAttrExtension(ExtName : string) : boolean;
    procedure SetSupportedAttrExtension(ExtName : string; Support : boolean);

    property SupportedAttributes : TSBSftpAttributes read FSupportedAttributes
      write FSupportedAttributes;
    property SupportedAttribBits : cardinal read FSupportedAttribBits
      write FSupportedAttribBits;
    property SupportedOpenModes : TSBSftpFileOpenModes read FSupportedOpenModes
      write FSupportedOpenModes;
    property SupportedAccessModes : TSBSftpFileOpenAccess read FSupportedAccessModes
      write FSupportedAccessModes;
    property SupportedAccessMask : cardinal read FSupportedAccessMask
      write FSupportedAccessMask;
    property MaxReadSize : cardinal read FMaxReadSize write FMaxReadSize;
    property SupportedAttrExtensions : TStringList read FSupportedAttrExtensions;
    property SupportedExtensions : TStringList read FSupportedExtensions;

    property Version : integer read FVersion write FVersion;
  end;

  TElSftpVendorIDExtension = class(TSBSftpExtendedAttribute)
  private
    FVendorName : string;
    FProductName : string;
    FProductVersion : string;
    FBuildNumber : Int64;
  public
    constructor Create; virtual;

    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property VendorName : string read FVendorName write FVendorName;
    property ProductName : string read FProductName write FProductName;
    property ProductVersion : string read FProductVersion write FProductVersion;
    property BuildNumber : Int64 read FBuildNumber write FBuildNumber;
  end;

  TElSftpCheckFileExtension = class(TSBSftpExtendedAttribute)
  private
    FName : string;
    FHandle : BufferType;
    FHashAlgorithms : string;
    FStartOffset : Int64;
    FLength : Int64;
    FBlockSize : Cardinal;
  public
    constructor Create; virtual;

    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property Name : string read FName write FName;
    property Handle : BufferType read FHandle write FHandle;
    property HashAlgorithms : string read FHashAlgorithms write FHashAlgorithms;
    property StartOffset : Int64 read FStartOffset write FStartOffset;
    property DataLength : Int64 read FLength write FLength;
    property BlockSize : Cardinal read FBlockSize write FBlockSize;
  end;

  TElSftpSpaceAvailableExtension = class(TSBSftpExtendedAttribute)
  private
    FPath : string;
  public
    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property Path : string read FPath write FPath;
  end;

  TElSftpHomeDirectoryExtension = class(TSBSftpExtendedAttribute)
  private
    FUsername : string;
  public
    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property Username : string read FUsername write FUsername;
  end;

  TElSftpCopyFileExtension = class(TSBSftpExtendedAttribute)
  private
    FSource : string;
    FDestination : string;
    FOverwrite : boolean;
  public
    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property Source : string read FSource write FSource;
    property Destination : string read FDestination write FDestination;
    property Overwrite : boolean read FOverwrite write FOverwrite;
  end;

  TElSftpCopyDataExtension = class(TSBSftpExtendedAttribute)
  private
    FReadHandle : TSBSFTPFileHandle;
    FReadOffset : Int64;
    FWriteHandle : TSBSFTPFileHandle;
    FWriteOffset : Int64;
    FDataLength : Int64;
  public
    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property ReadHandle : TSBSFTPFileHandle read FReadHandle write FReadHandle;
    property ReadOffset : Int64 read FReadOffset write FReadOffset;
    property WriteHandle : TSBSFTPFileHandle read FWriteHandle write FWriteHandle;
    property WriteOffset : Int64 read FWriteOffset write FWriteOffset;
    property DataLength : Int64 read FDataLength write FDataLength;
  end;

  TElSftpCheckFileReply = class(TElSftpExtendedReply)
  private
    FHashes : TStringList;
    FHashAlgorithm : integer;
    function GetHashSize : integer;
  public
    constructor Create;
    destructor Destroy; override;
    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property Hashes : TStringList read FHashes;
    property HashAlgorithm : integer read FHashAlgorithm write FHashAlgorithm;
    property HashSize : integer read GetHashSize;
  end;

  TElSftpSpaceAvailableReply = class(TElSftpExtendedReply)
  private
    FBytesOnDevice : Int64;
    FUnusedBytesOnDevice : Int64;
    FBytesAvailableToUser : Int64;
    FUnusedBytesAvailableToUser : Int64;
    FBytesPerAllocationUnit : Cardinal;
  public
    function LoadFromBuffer : boolean; override;
    procedure SaveToBuffer; override;

    property BytesOnDevice : Int64 read FBytesOnDevice write FBytesOnDevice;
    property UnusedBytesOnDevice : Int64 read FUnusedBytesOnDevice
      write FUnusedBytesOnDevice;
    property BytesAvailableToUser : Int64 read FBytesAvailableToUser
      write FBytesAvailableToUser;
    property UnusedBytesAvailableToUser : Int64 read FUnusedBytesAvailableToUser
      write FUnusedBytesAvailableToUser;
    property BytesPerAllocationUnit : Cardinal read FBytesPerAllocationUnit
      write FBytesPerAllocationUnit;
  end;

  TSBSftpExtendedAttributes =  array of TSBSftpExtendedAttribute;

  TSBSftpACE = class
  private
    FACEType : integer;
    FACEFlags : integer;
    FACEMask : integer;
    FWho : String;
  public
    property ACEType: integer read FACEType write FACEType;
    property ACEFlags : integer read FACEFlags write FACEFlags;
    property ACEMask : integer read FACEMask write FACEMask;
    property Who : string read FWho write FWho;
  end;

  TSBSftpACEs =  array of TSBSftpACE;

  TSBSftpFileType = 
   (ftFile, ftDirectory, ftSymblink, ftSpecial, ftUnknown,
    ftSocket, ftCharDevice, ftBlockDevice, ftFIFO);

  TElSftpExtendedProperties = class
  private
    FFilenameCharset : string;
    FDesiredAccess : cardinal;
    FAutoAdjustDesiredAccess : boolean;
    FRenameFlags : TSBSftpRenameFlags;
    FRequestAttributes : TSBSftpAttributes;
    FAutoAdjustRequestAttributes : boolean;

    FSupportedAvailable : boolean;
    FSupportedExtension : TElSFTPSupportedExtension;
    FNewlineAvailable : boolean;
    FNewlineExtension : TElSFTPNewlineExtension;
    FVersionsAvailable : boolean;
    FVersionsExtension : TElSFTPVersionsExtension;
    FVendorIDAvailable : boolean;
    FVendorIDExtension : TElSFTPVendorIDExtension;

    function GetRenameFlagsCardinal : cardinal;
    function GetRequestAttributesCardinal : cardinal;
  public
    constructor Create;
    destructor Destroy; override;

    function RequestAttributesByVersion(Version : integer) : cardinal;

    property DesiredAccess : cardinal read FDesiredAccess write FDesiredAccess;
    property AutoAdjustDesiredAccess : boolean read FAutoAdjustDesiredAccess
      write FAutoAdjustDesiredAccess;

    property RenameFlags : TSBSftpRenameFlags read FRenameFlags write FRenameFlags;
    property RenameFlagsCardinal : cardinal read GetRenameFlagsCardinal;
    property RequestAttributes : TSBSftpAttributes read FRequestAttributes
      write FRequestAttributes;
    property RequestAttributesCardinal : cardinal read GetRequestAttributesCardinal;
    property AutoAdjustRequestAttributes : boolean read FAutoAdjustRequestAttributes
      write FAutoAdjustRequestAttributes;

    property SupportedAvailable : boolean read FSupportedAvailable
      write FSupportedAvailable;
    property SupportedExtension : TElSFTPSupportedExtension read FSupportedExtension
      write FSupportedExtension;

    property NewlineAvailable : boolean read FNewlineAvailable
      write FNewlineAvailable;
    property NewlineExtension : TElSFTPNewlineExtension read FNewlineExtension
      write FNewlineExtension;

    property VersionsAvailable : boolean read FVersionsAvailable
      write FVersionsAvailable;
    property VersionsExtension : TElSFTPVersionsExtension read FVersionsExtension
      write FVersionsExtension;

    property VendorIDAvailable : boolean read FVendorIDAvailable
      write FVendorIDAvailable;
    property VendorIDExtension : TElSFTPVendorIDExtension read FVendorIDExtension
      write FVendorIDExtension;

    property FilenameCharset : string read FFilenameCharset write FFilenameCharset;  
  end;


  TElSftpFileAttributes = class
  private
    FSize: Int64;
    FAllocationSize : Int64;
    FOwner : string;
    FGroup : string;
    FUID: Integer;
    FGID: Integer;
    FATime: Int64;
    FMTime: Int64;
    FCTime: Int64;
    FCATime: Int64;
    FIncludedAttributes: TSBSftpAttributes;
    FExtendedAttributes: TList;
    FACEs : TList;
    FAttribBits: Cardinal;
    FAttribBitsValid: Cardinal;
    FATimeMS: Cardinal;
    FCTimeMS: Cardinal;
    FCATimeMS: Cardinal;
    FFileType: Byte;
    FMTimeMS: Cardinal;
    FPermissions: cardinal;
    FTextHint : TSBSftpTextHint;
    FMimeType : string;
    FLinkCount : cardinal;
    FUntranslatedName : string;

    function GetATime: TDateTime;
    function GetMTime: TDateTime;
    function GetCTime: TDateTime;
    function GetCATime: TDateTime;
    function GetATimeCardinal : cardinal;
    function GetMTimeCardinal : cardinal;
    function GetCTimeCardinal : cardinal;
    function GetCATimeCardinal : cardinal;
    function GetFileType : TSBSftpFileType;
    function GetUserRead: boolean;
    function GetUserWrite: boolean;
    function GetUserExecute: boolean;
    function GetGroupRead: boolean;
    function GetGroupWrite: boolean;
    function GetGroupExecute: boolean;
    function GetOtherRead: boolean;
    function GetOtherWrite: boolean;
    function GetOtherExecute: boolean;

    function GetUIDBit: boolean;
    function GetGIDBit: boolean;
    function GetStickyBit : boolean;
    function GetACE(Index: integer) : TSBSftpACE;
    function GetACECount : integer;
    function GetExtendedAttribute(Index: integer): TSBSftpExtendedAttribute;
    function GetExtendedAttributeCount: integer;

    function GetDirectory: boolean;
    procedure SetATime(Value: TDateTime);
    procedure SetMTime(Value: TDateTime);
    procedure SetCTime(Value: TDateTime);
    procedure SetCATime(Value: TDateTime);
    procedure SetATimeCardinal(Value: cardinal);
    procedure SetMTimeCardinal(Value: cardinal);
    procedure SetCTimeCardinal(Value: cardinal);
    procedure SetCATimeCardinal(Value: cardinal);
    procedure SetFileType(Value : TSBSftpFileType);

    procedure SetUserRead(Value: boolean);
    procedure SetUserWrite(Value: boolean);
    procedure SetUserExecute(Value: boolean);
    procedure SetGroupRead(Value: boolean);
    procedure SetGroupWrite(Value: boolean);
    procedure SetGroupExecute(Value: boolean);
    procedure SetOtherRead(Value: boolean);
    procedure SetOtherWrite(Value: boolean);
    procedure SetOtherExecute(Value: boolean);
    {JPM Modifications}
    procedure SetUIDBit(Value: boolean);
    procedure SetGIDBit(Value: boolean);
    procedure SetStickyBit(Value: boolean);
    {end}
    procedure SetDirectory(Value: boolean);
  public
    constructor Create;
    destructor Destroy; override;

    procedure CopyTo(dst: TElSftpFileAttributes);

    function SaveToBuffer(Ver : integer) : BufferType;
    function LoadFromBuffer(Buffer : pointer; Size : integer; Ver : integer) : integer;
    
    function AddACE : integer;
    procedure RemoveACE(Index: integer);
    procedure ClearACEs;
    function AddExtendedAttribute: integer;
    procedure RemoveExtendedAttribute(Index: integer);
    procedure ClearExtendedAttributes;

    property Size: Int64 read FSize write FSize;
    property AllocationSize : Int64 read FAllocationSize write FAllocationSize;
    property UID: Integer read FUID write FUID;
    property GID: Integer read FGID write FGID;
    {JPM Modifications}
    property Owner : string read FOwner write FOwner;
    property Group : string read FGroup write FGroup;
    property CTime: TDateTime read GetCTime write SetCTime;
    property CATime : TDateTime read GetCATime write SetCATime;

    property FileType : TSBSftpFileType read GetFileType write SetFileType;
    {end}
    property ATime: TDateTime read GetATime write SetATime;
    property MTime: TDateTime read GetMTime write SetMTime;

    property ExtendedAttributeCount: Integer read GetExtendedAttributeCount;
    property ExtendedAttributes[Index: integer]: TSBSftpExtendedAttribute
      read GetExtendedAttribute;

    property IncludedAttributes: TSBSftpAttributes read FIncludedAttributes
      write FIncludedAttributes;
    property UserRead: boolean read GetUserRead write SetUserRead;
    property UserWrite: boolean read GetUserWrite write SetUserWrite;
    property UserExecute: boolean read GetUserExecute write SetUserExecute;
    property GroupRead: boolean read GetGroupRead write SetGroupRead;
    property GroupWrite: boolean read GetGroupWrite write SetGroupWrite;
    property GroupExecute: boolean read GetGroupExecute write SetGroupExecute;
    property OtherRead: boolean read GetOtherRead write SetOtherRead;
    property OtherWrite: boolean read GetOtherWrite write SetOtherWrite;
    property OtherExecute: boolean read GetOtherExecute write SetOtherExecute;
    property UIDBit : boolean read GetUIDBit write SetUIDBit;
    property GIDBit : boolean read GetGIDBit write SetGIDBit;
    property StickyBit : boolean read GetStickyBit write SetStickyBit;
    property Directory: boolean read GetDirectory write SetDirectory;

    property ACECount: Integer read GetACECount;
    property ACEs[Index: integer] : TSBSftpACE read GetACE;

    property ATimeInt64: Int64 read FATime write FATime;
    property MTimeInt64: Int64 read FMTime write FMTime;
    property CTimeInt64: Int64 read FCTime write FCTime;
    property CATimeInt64: Int64 read FCATime write FCATime;

    property ATimeCardinal: cardinal read GetATimeCardinal write SetATimeCardinal;
    property MTimeCardinal: cardinal read GetMTimeCardinal write SetMTimeCardinal;
    property CTimeCardinal: cardinal read GetCTimeCardinal write SetCTimeCardinal;
    property CATimeCardinal: cardinal read GetCATimeCardinal write SetCATimeCardinal;

    property ATimeMS: Cardinal read FATimeMS write FATimeMS;
    property CTimeMS: Cardinal read FCTimeMS write FCTimeMS;
    property CATimeMS: Cardinal read FCATimeMS write FCATimeMS;
    property MTimeMS: Cardinal read FMTimeMS write FMTimeMS;

    property AttribBits: Cardinal read FAttribBits write FAttribBits;
    property AttribBitsValid : Cardinal read FAttribBitsValid write FAttribBitsValid;
    property FileTypeByte: Byte read FFileType write FFileType;
    property Permissions: cardinal read FPermissions write FPermissions;
    property TextHint : TSBSftpTextHint read FTextHint write FTextHint;
    property MimeType : string read FMimeType write FMimeType;
    property LinkCount : Cardinal read FLinkCount write FLinkCount;
    property UntranslatedName : string read FUntranslatedName write FUntranslatedName;
  end;

  TElSftpFileInfo = class
  private
    FName: string;
    FLongName: string;
    FAttributes: TElSftpFileAttributes;
  public
    constructor Create;
    destructor Destroy; override;

    procedure CopyTo(FileInfo: TElSftpFileInfo);
    property Name: string read FName write FName;
    property LongName: string read FLongName write FLongName;
    property Attributes: TElSftpFileAttributes read FAttributes;
  end;

  TSBSftpFileOpenEvent = procedure(Sender: TObject; Handle: TSBSftpFileHandle) of
    object;
  TSBSftpErrorEvent = procedure(Sender: TObject; ErrorCode: integer; const Comment: string) of object;
  TSBSftpSuccessEvent = procedure(Sender: TObject; const Comment: string) of object;
  {$ifndef BUILDER_USED}
  TSBSftpDirectoryListingEvent = procedure(Sender: TObject; Listing: array of TElSftpFileInfo) of object;
  {$else}
  TSBSftpDirectoryListingEvent = procedure(Sender: TObject; Listing: TList) of object;
  {$endif}
  TSBSftpFileAttributesEvent = procedure(Sender: TObject; Attributes: TElSftpFileAttributes) of object;
  TSBSftpDataEvent = procedure(Sender: TObject; Buffer: pointer; Size: integer) of object;
  TSBSftpAbsolutePathEvent = procedure(Sender: TObject; const Path: string) of object;

  TSBSftpVersionSelectEvent =  procedure(
    Sender : TObject;
    Versions : TSBSftpVersions; var Version : TSBSftpVersion) of object;
  TSBSftpAvailableSpaceEvent =  procedure(
    Sender : TObject;
    AvailSpace : TElSFTPSpaceAvailableReply) of object;

  TSBSftpFileHashEvent =  procedure(
    Sender : TObject;
    Reply : TElSftpCheckFileReply; var Cont : boolean) of object;

  TSBSftpNameEvent =  procedure(
    Sender : TObject;
    FileInfo : TElSftpFileInfo) of object;

  TSBSftpMessageLoopEvent =  function : boolean of object;
    
{  TElSFTPServerOpenDirectoryEvent = procedure(Sender : TObject;
    const Path : string; var Handle : TSBSftpFileHandle;
    var ErrorCode : integer; var Comment : string) of object;}

  TElSFTPServerBlockEvent =  procedure(
    Sender : TObject;
    Data : pointer;
    Offset, Length : Int64; LockMask : TSBSftpFileOpenAccess;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerUnblockEvent =  procedure(
    Sender : TObject;
    Data : pointer;
    Offset, Length : Int64; var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerCreateDirectoryEvent =  procedure(
    Sender : TObject;
    const Path : string; Attributes : TElSftpFileAttributes;
    var ErrorCode : integer; var Comment : string) of object;

{  TElSFTPServerCreateFileEvent = procedure(Sender : TObject;
    const Path : string; var Handle : TSBSftpFileHandle;
    var ErrorCode : integer; var Comment : string) of object;}

  TElSFTPServerCreateSymLinkEvent =  procedure(
    Sender : TObject;
    const LinkPath : string; const TargetPath : string;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerCreateHardLinkEvent =  procedure(
    Sender : TObject;
    const NewLinkPath : string; const ExistingPath : string;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerOpenFileEvent =  procedure(Sender : TObject; const Path : string;
    Modes: TSBSftpFileOpenModes; Access : TSBSftpFileOpenAccess;
    DesiredAccess : Cardinal;
    Attributes : TElSftpFileAttributes;
    var Data : pointer;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerRemoveEvent =  procedure(Sender : TObject;
    const Path : string; var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerRenameFileEvent =  procedure(Sender : TObject;
    const OldPath, NewPath : string; Flags : TSBSftpRenameFlags; var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerReadSymLinkEvent =  procedure(Sender : TObject;
    const Path : string; Info : TElSftpFileInfo;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerSetAttributesEvent =  procedure(Sender : TObject;
    const Path : string; Attributes : TElSftpFileAttributes;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerSetAttributes2Event =  procedure(Sender : TObject;
    Data : pointer;
    Attributes : TElSftpFileAttributes;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerRequestAttributesEvent =  procedure(Sender : TObject;
    const Path : string; FollowSymLinks : boolean; Attributes : TElSftpFileAttributes;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerRequestAbsolutePathEvent =  procedure(Sender : TObject;
    const Path : string; var AbsolutePath : string;
    Control : TSBSftpRealpathControl; ComposePath : TStringList;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerRequestAttributes2Event =  procedure(Sender : TObject;
    Data : pointer;
    Attributes : TElSftpFileAttributes;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerFindFirstEvent =  procedure(Sender : TObject;
    const Path : string; var Data :
    pointer;
    Info : TElSftpFileInfo; var ErrorCode : integer;
    var Comment : string) of object;

  TElSFTPServerFindNextEvent =  procedure(Sender : TObject;
    Data : pointer;
    Info : TElSftpFileInfo; var ErrorCode : integer;
    var Comment : string) of object;

  TElSFTPServerFindCloseEvent =  procedure(Sender : TObject;
    SearchRec : pointer;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerReadEvent = procedure(Sender : TObject;
    Data : pointer; Offset : Int64; Buffer : Pointer; Count : integer;
    var Read : integer; var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerWriteEvent = procedure(Sender : TObject;
    Data : pointer; Offset : Int64; Buffer : Pointer; Count : integer;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerTextSeekEvent =  procedure(Sender : TObject;
    Data : pointer;
    LineNumber : Int64; var ErrorCode : integer;
    var Comment : string) of object;

  TElSFTPServerCloseHandleEvent =  procedure(Sender : TObject;
    Data : pointer;
    var ErrorCode : integer; var Comment : string) of object;

  TElSFTPServerSendEvent = procedure(Sender : TObject;
    Buffer: pointer; Size: integer) of object;

  TElSFTPServerReceiveEvent = procedure(Sender : TObject;
    Buffer: pointer; MaxSize: integer; var Written: integer) of object;

  TElSFTPServerExtendedRequestEvent = procedure(Sender : TObject;
    const Request: string; Buffer : pointer; Size : integer;
    Response : TStream; var ErrorCode : integer; var Comment : string) of object;
  TElSftpServerVersionChangeEvent =  procedure(Sender :
    TObject; Version : TSBSftpVersion) of object;
  TElSFTPServerSendVendorIDEvent =  procedure(Sender :
    TObject;
    VendorID : TElSftpVendorIDExtension; out Send : boolean) of object;

  TElSFTPServerRequestFileHashEvent =  procedure(Sender :
    TObject;
    Extension : TElSftpCheckFileExtension; Reply : TElSftpCheckFileReply;
    var ErrCode : integer; var ErrComment : string) of object;

  TElSFTPServerRequestFileHashByHandleEvent =  procedure(
    Sender : TObject;
    Data : pointer;
    Extension : TElSftpCheckFileExtension; Reply : TElSftpCheckFileReply;
    var ErrCode : integer; var ErrComment : string) of object;

  TElSFTPServerRequestAvailableSpaceEvent =  procedure(
    Sender : TObject;
    const Path : string; Reply : TElSftpSpaceAvailableReply;
    var ErrCode : integer; var ErrComment : string) of object;

  TElSFTPServerRequestHomeDirectoryEvent =  procedure(
    Sender : TObject;
    const Username : string; var HomeDir : string;
    var ErrCode : integer; var ErrComment : string) of object;

  TElSFTPServerCopyRemoteFileEvent =  procedure(
    Sender : TObject;
    const Source, Destination : string; Overwrite : boolean;
    var ErrCode : integer; var ErrComment : string) of object;

  TElSFTPServerCopyRemoteDataEvent =  procedure(
    Sender : TObject;
    ReadFromData : pointer; ReadFromOffset : Int64;
    WriteToData : pointer; WriteToOffset : Int64;
    DataLength : Int64; var ErrCode : integer; var ErrComment : string) of object;

  TElSFTPServerReturnPathEvent =  procedure(
    Sender : TObject;
    var Path : string; var ErrCode : integer; var ErrComment : string) of object;

  TElSftpOperation = 
   (soNone, soCreateFile, soCreateSymLink, soMakeDirectory,
    soOpenDirectory, soOpenFile, soRead, soReadDirectory, soReadSymLink,
    soRemoveDirectory, soRemoveFile, soRenameFile, soRequestAbsolutePath,
    soRequestAttributes, soSetAttributes, soWrite, soCloseHandle, soVersion,
    soExtension, soCreateHardLink, soBlock, soUnblock);

  TSBSftpRequestInfo = record
    Id: cardinal;
    Request: cardinal;
    ExtType: string;
  end;
  PSBSftpRequestInfo = ^TSBSftpRequestInfo;

  TSBSftpDataRequestInfo = record
    Id,
    DataID : cardinal;
    Offset,
    Size        : Int64;
  end;
  PSBSftpDataRequestInfo = ^TSBSftpDataRequestInfo;

{JPM MOdications for ACE}
const
  ACE4_ACCESS_ALLOWED_ACE_TYPE = $00000000;
  ACE4_ACCESS_DENIED_ACE_TYPE  = $00000001;
  ACE4_SYSTEM_AUDIT_ACE_TYPE   = $00000002;
  ACE4_SYSTEM_ALARM_ACE_TYPE   = $00000003;

  ACE4_FILE_INHERIT_ACE        = $00000001;
  ACE4_DIRECTORY_INHERIT_ACE   = $00000002;
  ACE4_NO_PROPAGATE_INHERIT_ACE = $00000004;
  ACE4_INHERIT_ONLY_ACE         =  $00000008;
  ACE4_SUCCESSFUL_ACCESS_ACE_FLAG = $00000010;
  ACE4_FAILED_ACCESS_ACE_FLAG    = $00000020;
  ACE4_IDENTIFIER_GROUP          = $00000040;

  { From RFC 3010 }

  ACE4_READ_DATA         = $00000001; //Permission to read the data of the file
  ACE4_LIST_DIRECTORY    = $00000001; //Permission to list the contents of a directory
  ACE4_WRITE_DATA        = $00000002; //Permission to modify the file�s data
  ACE4_ADD_FILE          = $00000002; //Permission to add a new file to a directory
  ACE4_APPEND_DATA       = $00000004; //Permission to append data to a file
  ACE4_ADD_SUBDIRECTORY  = $00000004; //Permission to create a subdirectory to a directory
  ACE4_READ_NAMED_ATTRS  = $00000008; //Permission to read the named attributes of a file
  ACE4_WRITE_NAMED_ATTRS = $00000010; //Permission to write the named attributes of a file
  ACE4_EXECUTE           = $00000020; //Permission to execute a file
  ACE4_DELETE_CHILD      = $00000040; //Permission to delete a file or directory within a directory
  ACE4_READ_ATTRIBUTES   = $00000080; //The ability to read basic attributes (non-acls) of a file
  ACE4_WRITE_ATTRIBUTES  = $00000100; //Permission to change basic attributes (non-acls) of a file
  ACE4_DELETE            = $00010000; //Permission to Delete the file
  ACE4_READ_ACL          = $00020000; //Permission to Read the ACL
  ACE4_WRITE_ACL         = $00040000; //Permission to Write the ACL
  ACE4_WRITE_OWNER       = $00080000; //Permission to change the owner
  ACE4_SYNCHRONIZE       = $00100000; //Permission to access file locally at the server with synchronous reads and writes

  {SFTP5 attributes bit flags}
  SSH_FILEXFER_ATTR_FLAGS_READONLY        = $00000001;
  SSH_FILEXFER_ATTR_FLAGS_SYSTEM          = $00000002;
  SSH_FILEXFER_ATTR_FLAGS_HIDDEN          = $00000004;
  SSH_FILEXFER_ATTR_FLAGS_CASE_INSENSITIVE= $00000008;
  SSH_FILEXFER_ATTR_FLAGS_ARCHIVE         = $00000010;
  SSH_FILEXFER_ATTR_FLAGS_ENCRYPTED       = $00000020;
  SSH_FILEXFER_ATTR_FLAGS_COMPRESSED      = $00000040;
  SSH_FILEXFER_ATTR_FLAGS_SPARSE          = $00000080;
  SSH_FILEXFER_ATTR_FLAGS_APPEND_ONLY     = $00000100;
  SSH_FILEXFER_ATTR_FLAGS_IMMUTABLE       = $00000200;
  SSH_FILEXFER_ATTR_FLAGS_SYNC            = $00000400;
  SSH_FILEXFER_ATTR_FLAGS_TRANSLATION_ERR = $00000800;//!! SFTP6 draft-06

//POSIX BItmask
//From: http://www.watersprings.org/pub/id/draft-ietf-secsh-filexfer-05.txt
  {$externalsym S_IRUSR}
  S_IRUSR  = $100; //256
  {$externalsym S_IWUSR}
  S_IWUSR  = $080; //128
  {$externalsym S_IXUSR}
  S_IXUSR  = $040; //64
  {$externalsym S_IRGRP}
  S_IRGRP  = $020; //32
  {$externalsym S_IWGRP}
  S_IWGRP  = $010; //16
  {$externalsym S_IXGRP}
  S_IXGRP  = $008; //8
  {$externalsym S_IROTH}
  S_IROTH  = $004; //4
  {$externalsym S_IWOTH}
  S_IWOTH  = $002; //2
  {$externalsym S_IXOTH}
  S_IXOTH  = $001; //1
  {$externalsym S_ISUID}
  S_ISUID  = $800; //2,048
  {$externalsym S_ISGID}
  S_ISGID  = $400; //1,024
  {$externalsym S_ISVTX}
  S_ISVTX  = $200; //  512

  BUFFER_SIZE = 131072;//65536; //16384;
  DATA_BUFFER_SIZE = BUFFER_SIZE - 1024;

  { packet type constants }
  SSH_FXP_INIT = 1;
  SSH_FXP_VERSION = 2;
  SSH_FXP_OPEN = 3;
  SSH_FXP_CLOSE = 4;
  SSH_FXP_READ = 5;
  SSH_FXP_WRITE = 6;
  SSH_FXP_LSTAT = 7;
  SSH_FXP_FSTAT = 8;
  SSH_FXP_SETSTAT = 9;
  SSH_FXP_FSETSTAT = 10;
  SSH_FXP_OPENDIR = 11;
  SSH_FXP_READDIR = 12;
  SSH_FXP_REMOVE = 13;
  SSH_FXP_MKDIR = 14;
  SSH_FXP_RMDIR = 15;
  SSH_FXP_REALPATH = 16;
  SSH_FXP_STAT = 17;
  SSH_FXP_RENAME = 18;
  SSH_FXP_READLINK = 19; //SFTP3
  SSH_FXP_SYMLINK = 20; //SFTP3 - till SFTP6 draft-07
  SSH_FXP_LINK = 21; //!!SFTP6 draft-08
  SSH_FXP_BLOCK = 22; //!!SFTP6 draft-08
  SSH_FXP_UNBLOCK = 23; //!!SFTP6 draft-08

  SSH_FXP_STATUS = 101;
  SSH_FXP_HANDLE = 102;
  SSH_FXP_DATA = 103;
  SSH_FXP_NAME = 104;
  SSH_FXP_ATTRS = 105;

  SSH_FXP_EXTENDED = 200;
  SSH_FXP_EXTENDED_REPLY = 201;

  { file attributes constants }
  SSH_FILEXFER_ATTR_SIZE             = $00000001;
  SSH_FILEXFER_ATTR_UIDGID           = $00000002;
  SSH_FILEXFER_ATTR_PERMISSIONS      = $00000004; //SFTP3 draft-03
  SSH_FILEXFER_ATTR_ACCESSTIME       = $00000008;
  SSH_FILEXFER_ATTR_ACMODTIME        = $00000008; //till SFTP4 draft-03
  SSH_FILEXFER_ATTR_CREATETIME       = $00000010;
  SSH_FILEXFER_ATTR_MODIFYTIME       = $00000020;
  SSH_FILEXFER_ATTR_ACL              = $00000040;
  SSH_FILEXFER_ATTR_OWNERGROUP       = $00000080;
  SSH_FILEXFER_ATTR_SUBSECOND_TIMES  = $00000100;
  SSH_FILEXFER_ATTR_BITS             = $00000200; //SFTP5
  SSH_FILEXFER_ATTR_ALLOCATION_SIZE  = $00000400; //SFTP6 draft-06 by Romio Pedchenko 2005/02/21
  SSH_FILEXFER_ATTR_TEXT_HINT        = $00000800; //SFTP6 draft-06
  SSH_FILEXFER_ATTR_MIME_TYPE        = $00001000; //SFTP6 draft-06
  SSH_FILEXFER_ATTR_LINK_COUNT       = $00002000; //SFTP6 draft-06
  SSH_FILEXFER_ATTR_UNTRANSLATED_NAME= $00004000; //SFTP6 draft-06
  SSH_FILEXFER_ATTR_CTIME            = $00008000; //!!SFTP6 draft-08
  SSH_FILEXFER_ATTR_EXTENDED         = $80000000;

  { supported attributes by-vesion }
  SSH_FILEXFER_ATTR_V3 =
    SSH_FILEXFER_ATTR_SIZE or
    SSH_FILEXFER_ATTR_UIDGID or
    SSH_FILEXFER_ATTR_PERMISSIONS or
    SSH_FILEXFER_ATTR_ACMODTIME or
    SSH_FILEXFER_ATTR_EXTENDED;

  SSH_FILEXFER_ATTR_V4 =
    SSH_FILEXFER_ATTR_SIZE or
    SSH_FILEXFER_ATTR_PERMISSIONS or
    SSH_FILEXFER_ATTR_ACCESSTIME or
    SSH_FILEXFER_ATTR_CREATETIME or
    SSH_FILEXFER_ATTR_MODIFYTIME or
    SSH_FILEXFER_ATTR_ACL or
    SSH_FILEXFER_ATTR_OWNERGROUP or
    SSH_FILEXFER_ATTR_SUBSECOND_TIMES or
    SSH_FILEXFER_ATTR_EXTENDED;

  SSH_FILEXFER_ATTR_V5 = SSH_FILEXFER_ATTR_V4 or
    SSH_FILEXFER_ATTR_BITS;

  SSH_FILEXFER_ATTR_V6 = SSH_FILEXFER_ATTR_V5 or
    SSH_FILEXFER_ATTR_ALLOCATION_SIZE or
    SSH_FILEXFER_ATTR_TEXT_HINT or
    SSH_FILEXFER_ATTR_MIME_TYPE or
    SSH_FILEXFER_ATTR_LINK_COUNT or
    SSH_FILEXFER_ATTR_UNTRANSLATED_NAME or
    SSH_FILEXFER_ATTR_CTIME;

  { file type constants }
  //SFTP4
  SSH_FILEXFER_TYPE_ERRONEOUS        = 0; // should not happen, but will in case of invalid client
  SSH_FILEXFER_TYPE_REGULAR          = 1;
  SSH_FILEXFER_TYPE_DIRECTORY        = 2;
  SSH_FILEXFER_TYPE_SYMLINK          = 3;
  SSH_FILEXFER_TYPE_SPECIAL          = 4;
  SSH_FILEXFER_TYPE_UNKNOWN          = 5;
  SSH_FILEXFER_TYPE_SOCKET           = 6;
  SSH_FILEXFER_TYPE_CHAR_DEVICE      = 7;
  SSH_FILEXFER_TYPE_BLOCK_DEVICE     = 8;
  SSH_FILEXFER_TYPE_FIFO             = 9;

  { file open modes }
  { older constants, till SFTP5 }
  SSH_FXF_READ   = $00000001;
  SSH_FXF_WRITE  = $00000002;
  SSH_FXF_APPEND = $00000004;
  SSH_FXF_CREAT  = $00000008;
  SSH_FXF_TRUNC  = $00000010;
  SSH_FXF_EXCL   = $00000020;
  SSH_FXF_TEXT   = $00000040;
  { SFTP5 }
  SSH_FXF_ACCESS_DISPOSITION         = $00000007;
      SSH_FXF_CREATE_NEW             = $00000000;
      SSH_FXF_CREATE_TRUNCATE        = $00000001;
      SSH_FXF_OPEN_EXISTING          = $00000002;
      SSH_FXF_OPEN_OR_CREATE         = $00000003;
      SSH_FXF_TRUNCATE_EXISTING      = $00000004;
  SSH_FXF_ACCESS_APPEND_DATA         = $00000008;
  SSH_FXF_ACCESS_APPEND_DATA_ATOMIC  = $00000010;
  SSH_FXF_ACCESS_TEXT_MODE           = $00000020;

  SSH_FXF_ACCESS_READ_LOCK           = $00000040; //renamed in draft-08
  SSH_FXF_ACCESS_WRITE_LOCK          = $00000080;
  SSH_FXF_ACCESS_DELETE_LOCK         = $00000100;

  SSH_FXF_ACCESS_BLOCK_READ          = $00000040; //since draft-08
  SSH_FXF_ACCESS_BLOCK_WRITE         = $00000080;
  SSH_FXF_ACCESS_BLOCK_DELETE        = $00000100;

  SSH_FXF_ACCESS_BLOCK_ADVISORY      = $00000200;
  SSH_FXF_ACCESS_NOFOLLOW            = $00000400;
  SSH_FXF_ACCESS_DELETE_ON_CLOSE     = $00000800;

  { text hint attribute values }
  SSH_FILEXFER_ATTR_KNOWN_TEXT       = $00;
  SSH_FILEXFER_ATTR_GUESSED_TEXT     = $01;
  SSH_FILEXFER_ATTR_KNOWN_BINARY     = $02;
  SSH_FILEXFER_ATTR_GUESSED_BINARY   = $03;

  { file rename flags }
  SSH_FXF_RENAME_OVERWRITE           = $01;
  SSH_FXF_RENAME_ATOMIC              = $02;
  SSH_FXF_RENAME_NATIVE              = $04;

  { realpath control field values }
  SSH_FXP_REALPATH_NO_CHECK          = $01;
  SSH_FXP_REALPATH_STAT_IF           = $02;
  SSH_FXP_REALPATH_STAT_ALWAYS       = $03;

  { Error codes }
  SSH_FX_OK                = 0;
  SSH_FX_EOF               = 1;
  SSH_FX_NO_SUCH_FILE      = 2;
  SSH_FX_PERMISSION_DENIED = 3;
  SSH_FX_FAILURE           = 4;
  SSH_FX_BAD_MESSAGE       = 5;
  SSH_FX_NO_CONNECTION     = 6;
  SSH_FX_CONNECTION_LOST   = 7;
  SSH_FX_OP_UNSUPPORTED    = 8;
  { SFTP4 }
  SSH_FX_INVALID_HANDLE     = 9;
  SSH_FX_NO_SUCH_PATH       = 10;
  SSH_FX_FILE_ALREADY_EXISTS= 11;
  SSH_FX_WRITE_PROTECT      = 12;
  SSH_FX_NO_MEDIA           = 13; // SFTP4 draft-04
  { SFTP5 }
  SSH_FX_NO_SPACE_ON_FILESYSTEM = 14;
  SSH_FX_QUOTA_EXCEEDED         = 15;
  SSH_FX_UNKNOWN_PRINCIPLE      = 16;
  SSH_FX_LOCK_CONFlICT          = 17;
  SSH_FX_DIR_NOT_EMPTY          = 18; // SFTP5 draft-06, by Romio Pedchenko 2005/02/20
  SSH_FX_NOT_A_DIRECTORY        = 19;
  SSH_FX_INVALID_FILENAME       = 20;
  SSH_FX_LINK_LOOP              = 21;
  { !!SFTP6 }
  SSH_FX_CANNOT_DELETE           = 22;
  SSH_FX_INVALID_PARAMETER       = 23;
  SSH_FX_FILE_IS_A_DIRECTORY     = 24;
  SSH_FX_BYTE_RANGE_LOCK_CONFLICT= 25;
  SSH_FX_BYTE_RANGE_LOCK_REFUSED = 26;
  SSH_FX_DELETE_PENDING          = 27;
  SSH_FX_FILE_CORRUPT            = 28;
  SSH_FX_OWNER_INVALID           = 29; // SFTP6 draft-10
  SSH_FX_GROUP_INVALID           = 30;

{JPM MOdifications - value for sending in attribute requests for version 4}
  SSH_ATTR_REQUEST4 = 
    SSH_FILEXFER_ATTR_SIZE or
    SSH_FILEXFER_ATTR_PERMISSIONS or
    SSH_FILEXFER_ATTR_ACCESSTIME or
    SSH_FILEXFER_ATTR_CREATETIME or
    SSH_FILEXFER_ATTR_MODIFYTIME or
    SSH_FILEXFER_ATTR_ACL or
    SSH_FILEXFER_ATTR_OWNERGROUP or
    SSH_FILEXFER_ATTR_SUBSECOND_TIMES;

  SSH_ATTR_REQUEST5 =
    SSH_ATTR_REQUEST4 or
    SSH_FILEXFER_ATTR_BITS;

  SSH_ATTR_REQUEST6 =
    SSH_ATTR_REQUEST5 or
    SSH_FILEXFER_ATTR_CTIME;

{ SSH extension names }
  SSH_EXT_NEWLINE = 'newline';
  SSH_EXT_NEWLINE_VANDYKE = 'newline@vandyke.com';
  SSH_EXT_SUPPORTED = 'supported';
  SSH_EXT_SUPPORTED2 = 'supported2';
  SSH_EXT_VERSIONS = 'versions';
  SSH_EXT_VERSION_SELECT = 'version-select';
  SSH_EXT_FILENAME_CHARSET = 'filename-charset';
  SSH_EXT_FILENAME_TRANSLATION_CONTROL = 'filename-translation-control';
  SSH_EXT_TEXTSEEK = 'text-seek';
  SSH_EXT_VENDOR_ID = 'vendor-id';
  SSH_EXT_CHECK_FILE_HANDLE = 'check-file-handle';
  SSH_EXT_CHECK_FILE_NAME = 'check-file-name';
  SSH_EXT_SPACE_AVAILABLE = 'space-available';
  SSH_EXT_HOME_DIRECTORY = 'home-directory';
  SSH_EXT_COPY_FILE = 'copy-file';
  SSH_EXT_COPY_DATA = 'copy-data';
  SSH_EXT_GET_TEMP_FOLDER = 'get-temp-folder';
  SSH_EXT_MAKE_TEMP_FOLDER = 'make-temp-folder';

{ Auxiliary extension-specific constants }
  SSH_CHECKFILE_HASH_ALGORITHMS = 'md5,sha1,sha224,sha256,sha384,sha512,crc32';
  SSH_SBB_VENDOR_NAME = 'EldoS';
  SSH_SBB_PRODUCT_NAME = 'SFTPBlackbox';
  SSH_SBB_PRODUCT_VERSION = '4.4'; //!!
  SSH_SBB_PRODUCT_BUILD = $0004000400000000; //!!

{ Auxiliary routines }
function WriteACL(Attrs : TElSftpFileAttributes) : String;
procedure ReadACL(Attrs : TElSftpFileAttributes; const AACL : String);
procedure WriteDefaultAttributes(var Attributes: TElSftpFileAttributes); 
function RealPathControlToByte(Value : TSBSFTPRealpathControl) : byte; 
function ByteToRealPathControl(Value : byte) : TSBSFTPRealpathControl; 
function FileOpenAccessToCardinal(Value : TSBSftpFileOpenAccess) : cardinal; 
function CardinalToFileOpenAccess(Value : Cardinal) : TSBSftpFileOpenAccess; 
function CardinalToFileOpenModes(Value : Cardinal) : TSBSftpFileOpenModes; 
function FileOpenModesToCardinal(Value : TSBSftpFileOpenModes) : Cardinal; 
function RenameFlagsToCardinal(Value : TSBSftpRenameFlags) : Cardinal; 
function CardinalToRenameFlags(Value : Cardinal) : TSBSftpRenameFlags; 

function CardinalToAttributes(Value : Cardinal) : TSBSftpAttributes; 
function AttributesToCardinal(Value : TSBSftpAttributes) : Cardinal; 


implementation

uses
  SBSSHUtils;

resourcestring
  SErrorUnsupportedVersion = 'Unsupported Protocol Version';
  SErrorInvalidPacket = 'Invalid Packet';
  SErrorTunnelError = 'Tunnel Error';
  SErrorConnectionClosed = 'Connection closed by lower level protocol';
  SNotInSyncMode = 'SFTP Client is not in synchronous mode';
  SNotConnected = 'SFTP component not connected';

function EncodeSFTPTime(Seconds : Int64; NSeconds : cardinal) : TDateTime;
const
  UnixDateDelta = 25569;
  SecsPerDay = 86400.0;
begin
  NSeconds := NSeconds mod 1000000000;
  Result := (Seconds/SecsPerDay + UnixDateDelta) + NSeconds/(1000000000.0 * SecsPerDay);
end;

procedure DecodeSFTPTime(Value : TDateTime;
  var Seconds : Int64; var NSeconds : cardinal);
var
  Tmp : Integer;
const
  UnixDateDelta = 25569;
  SecsPerDay = 86400;
begin
  Seconds := Round((Value - UnixDateDelta) * SecsPerDay);
  Tmp := Round(Value * 1000000000.0 * SecsPerDay) mod 1000000000;
  if Tmp < 0 then Tmp := Tmp + 1000000000;
  NSeconds := Tmp;
end;

function ByteToTextHint(Value : Byte) : TSBSftpTextHint;
begin
  case Value of
    SSH_FILEXFER_ATTR_KNOWN_TEXT     : Result := thKnownText;
    SSH_FILEXFER_ATTR_GUESSED_TEXT   : Result := thGuessedText;
    SSH_FILEXFER_ATTR_KNOWN_BINARY   : Result := thKnownBinary;
    SSH_FILEXFER_ATTR_GUESSED_BINARY : Result := thGuessedBinary;
  else
    Result := thGuessedBinary;
  end;  
end;

function TextHintToByte(Value : TSBSftpTextHint) : byte;
begin
  case Value of
    thKnownText     : Result := SSH_FILEXFER_ATTR_KNOWN_TEXT;
    thGuessedText   : Result := SSH_FILEXFER_ATTR_GUESSED_TEXT;
    thKnownBinary   : Result := SSH_FILEXFER_ATTR_KNOWN_BINARY;
    thGuessedBinary : Result := SSH_FILEXFER_ATTR_GUESSED_BINARY;
  else
    Result := SSH_FILEXFER_ATTR_GUESSED_BINARY;
  end;  
end;

////////////////////////////////////////////////////////////////////////////////
// Public routines

constructor EElSFTPError.CreateCode(ErrorCode : integer; Comment : string);
begin
  inherited Create(Comment);
  FErrorCode := ErrorCode;
end;


////////////////////////////////////////////////////////////////////////////////
// TElSftpFileAttributes

constructor TElSftpFileAttributes.Create;
begin
  inherited;
  FIncludedAttributes := [];
  FACEs := TList.Create;
  FExtendedAttributes := TList.Create;
  FAttribBitsValid := 0;
end;

destructor TElSftpFileAttributes.Destroy;
begin
  ClearACEs;
  FACEs.Free;
  ClearExtendedAttributes;
  FExtendedAttributes.Free;
  inherited;
end;

function TElSftpFileAttributes.GetATimeCardinal : cardinal;
begin
  Result := Cardinal(FATime);
end;

function TElSftpFileAttributes.GetMTimeCardinal : cardinal;
begin
  Result := Cardinal(FMTime);
end;

function TElSftpFileAttributes.GetCTimeCardinal : cardinal;
begin
  Result := Cardinal(FCTime);
end;

function TElSftpFileAttributes.GetCATimeCardinal : cardinal;
begin
  Result := Cardinal(FCATime);
end;

procedure TElSftpFileAttributes.SetATimeCardinal(Value : cardinal);
begin
  FATime := Value;
end;

procedure TElSftpFileAttributes.SetMTimeCardinal(Value : cardinal);
begin
  FMTime := Value;
end;

procedure TElSftpFileAttributes.SetCTimeCardinal(Value : cardinal);
begin
  FCTime := Value;
end;

procedure TElSftpFileAttributes.SetCATimeCardinal(Value : cardinal);
begin
  FCATime := Value;
end;

function TElSftpFileAttributes.GetATime: TDateTime;
begin
  Result := EncodeSFTPTime(FATime, FATimeMS);
end;

procedure TElSftpFileAttributes.CopyTo(dst: TElSftpFileAttributes);
var
  i, ind : integer;
begin
  if not assigned(dst) then
    exit;

  dst.FFileType := FFileType;
  dst.FSize := FSize;
  dst.FAllocationSize := FAllocationSize;  
  dst.FUID := FUID;
  dst.FGID := FGID;
  dst.FPermissions := FPermissions;
  dst.FIncludedAttributes := FIncludedAttributes;
  dst.FOwner := FOwner;
  dst.FGroup := FGroup;

  dst.FATime := FATime;
  dst.FMTime := FMTime;
  dst.FCTime := FCTime;
  dst.FCATime := FCATime;
  dst.FATimeMS := FATimeMS;
  dst.FMTimeMS := FMTimeMS;
  dst.FCTimeMS := FCTimeMS;
  dst.FCATimeMS := FCATimeMS;
  dst.FAttribBits := FAttribBits;
  dst.FAttribBitsValid := FAttribBitsValid;
  dst.FTextHint := FTextHint;
  dst.FMimeType := FMimeType;
  dst.FLinkCount := FLinkCount;
  dst.FUntranslatedName := FUntranslatedName;

  dst.ClearExtendedAttributes;

  dst.ClearACEs;
  for i := 0 to ACECount - 1 do
  begin
    ind := dst.AddACE;
    dst.ACEs[ind].FACEType := ACEs[i].FACEType;
    dst.ACEs[ind].FACEFlags := ACEs[i].FACEFlags;
    dst.ACEs[ind].FACEMask := ACEs[i].FACEMask;
    dst.ACEs[ind].FWho := ACEs[i].FWho;
  end;
  for i := 0 to ExtendedAttributeCount - 1 do
  begin
    ind := dst.AddExtendedAttribute;
    dst.ExtendedAttributes[ind].FExtType := ExtendedAttributes[i].FExtType;
    dst.ExtendedAttributes[ind].FExtData := ExtendedAttributes[i].FExtData;
  end;
end;

function TElSFTPFileAttributes.SaveToBuffer(Ver : integer) : BufferType;
var
  Flags: cardinal;
  LACL : String;
  I : integer;
begin
  if Ver > 3 then
  begin
    { version 4-6 attributes format }
    Flags := 0;
    Result := '';

    Result := Result + Char(Self.FileTypeByte);
    if saSize in Self.IncludedAttributes then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_SIZE;
      Result := Result + WriteUINT64(Self.Size);
    end;

    if (Ver > 5) and (saASize in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_ALLOCATION_SIZE;
      Result := Result + WriteUINT64(Self.AllocationSize);
    end;

    if (saGroup in Self.IncludedAttributes) or (saOwner in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_OWNERGROUP;
      Result := Result + WriteString( Self.Owner ) + WriteString( Self.Group );
    end;

    if saPermissions in Self.IncludedAttributes then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_PERMISSIONS;
      //strip off 16384 which was used for a dir for backwards compatibility
      //That is not done for SFTP4
      Result := Result + WriteUINT32(Self.Permissions and (not 16384));
    end;
    if (saSubSeconds in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_SUBSECOND_TIMES;
    end;
    if (saATime in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_ACCESSTIME;
      Result := Result + WriteUINT64(Self.ATimeCardinal);
      if saSubSeconds in Self.IncludedAttributes then
      begin
        Result := Result + WriteUINT32(Self.ATimeMS );
      end;
    end;
    if (saCTime in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_CREATETIME;
      Result := Result + WriteUINT64(Self.CTimeCardinal);
      if saSubSeconds in Self.IncludedAttributes then
      begin
        Result := Result + WriteUINT32(Self.CTimeMS );
      end;
    end;
    if (saMTime in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_MODIFYTIME;
      Result := Result + WriteUINT64(Self.MTimeCardinal);
      if saSubSeconds in Self.IncludedAttributes then
      begin
        Result := Result + WriteUINT32(Self.MTimeMS );
      end;
    end;

    if (Ver > 5) and (saCATime in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_MODIFYTIME;
      Result := Result + WriteUINT64(Self.CATimeCardinal);
      if saSubSeconds in Self.IncludedAttributes then
      begin
        Result := Result + WriteUINT32(Self.CATimeMS );
      end;
    end;

    if (saACL in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_ACL;
      LACL := WriteACL(Self);
      Result := Result + WriteString(LACL);
    end;

    if Ver > 4 then
    begin
      if (saAttribBits in Self.IncludedAttributes) then
      begin
        Flags := Flags or SSH_FILEXFER_ATTR_BITS;
        Result := Result + WriteUINT32(Self.AttribBits);

        if Ver > 5 then
          Result := (Result) + WriteUINT32(Self.AttribBitsValid);
      end;
    end;

    if (Ver > 5) and (saTextHint in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_TEXT_HINT;

      Result := Result + Char(TextHintToByte(Self.TextHint));
    end;

    if (Ver > 5) and (saMimeType in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_MIME_TYPE;
      Result := Result + WriteString(Self.MimeType);
    end;

    if (Ver > 5) and (saLinkCount in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_LINK_COUNT;
      Result := Result + WriteUINT32(Self.LinkCount);
    end;

    if (Ver > 5) and (saUName in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_UNTRANSLATED_NAME;
      Result := Result + WriteString(Self.UntranslatedName);
    end;

    if (saExtended in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_EXTENDED;
      Result := Result + WriteUINT32(Self.ExtendedAttributeCount);
      for I := 0 to Self.ExtendedAttributeCount - 1 do
        Result := Result + WriteString(Self.ExtendedAttributes[I].ExtType) +
          WriteString(Self.ExtendedAttributes[I].ExtData);
    end;

    Result := WriteUINT32(Flags) + Result;
  end
  else
  begin
    { Version 3 attributes format }

    Flags := 0;
    Result := '';
    if saSize in Self.IncludedAttributes then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_SIZE;
      Result := WriteUINT64(Self.Size);
    end;
    if (saUID in Self.IncludedAttributes) or (saGID in
      Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_UIDGID;
      Result := Result + WriteUINT32(Self.UID) +
        WriteUINT32(Self.GID);
    end;
    if saPermissions in Self.IncludedAttributes then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_PERMISSIONS;
      Result := Result + WriteUINT32(Self.Permissions);
    end;
    if (saATime in Self.IncludedAttributes) or (saMTime in
      Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_ACMODTIME;
      Result := Result + WriteUINT32(Self.ATimeCardinal) +
        WriteUINT32(Self.MTimeCardinal);
    end;
    if (saExtended in Self.IncludedAttributes) then
    begin
      Flags := Flags or SSH_FILEXFER_ATTR_EXTENDED;
      Result := Result + WriteUINT32(Self.ExtendedAttributeCount);
      for I := 0 to Self.ExtendedAttributeCount - 1 do
        Result := Result + WriteString(Self.ExtendedAttributes[I].ExtType) +
          WriteString(Self.ExtendedAttributes[I].ExtData);
    end;
    Result := WriteUINT32(Flags) + Result;
  end;
end;

function TElSFTPFileAttributes.LoadFromBuffer(Buffer : pointer; Size : integer; Ver : integer) : integer;
var
  Flags: cardinal;
  CurrP: ^byte;
  LACL : String;
  OrigSize: integer;
  I, Index : integer;
  ExCount : integer;
begin
  if Ver > 3 then
  begin
    { version 4 and newer attributes format }
    CurrP := Buffer;
    OrigSize := Size;
    try
      Flags := ReadLength(CurrP, Size);
      Inc(CurrP, 4);
      Dec(Size, 4);
      ClearExtendedAttributes;
      ClearACEs;
      IncludedAttributes := [];
      if (Flags and SSH_FILEXFER_ATTR_SIZE) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saSize];
      if (Flags and SSH_FILEXFER_ATTR_OWNERGROUP) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saOwner,saGroup];
      if (Flags and SSH_FILEXFER_ATTR_PERMISSIONS) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saPermissions];
      if (Flags and SSH_FILEXFER_ATTR_SUBSECOND_TIMES)>0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saSubSeconds];
      if (Flags and SSH_FILEXFER_ATTR_ACCESSTIME) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saATime];
      if (Flags and SSH_FILEXFER_ATTR_CREATETIME) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saCTime];
      if (Flags and SSH_FILEXFER_ATTR_MODIFYTIME) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saMTime];
      if (Flags and SSH_FILEXFER_ATTR_ACL) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saACL];

      if (Flags and SSH_FILEXFER_ATTR_BITS) > 0 then
      begin
        if Ver < 5 then
          raise EElSFTPError.Create(SErrorInvalidPacket);
        Self.IncludedAttributes := Self.IncludedAttributes + [saAttribBits];
      end;

      if (Flags and SSH_FILEXFER_ATTR_CTIME) > 0 then
      begin
        if Ver < 6 then
          raise EElSFTPError.Create(SErrorInvalidPacket);
        Self.IncludedAttributes := Self.IncludedAttributes + [saCATime];
      end;

      if (Flags and SSH_FILEXFER_ATTR_TEXT_HINT) > 0 then
      begin
        if Ver < 6 then
          raise EElSFTPError.Create(SErrorInvalidPacket);
        Self.IncludedAttributes := Self.IncludedAttributes + [saTextHint];
      end;

      if (Flags and SSH_FILEXFER_ATTR_MIME_TYPE) > 0 then
      begin
        if Ver < 6 then
          raise EElSFTPError.Create(SErrorInvalidPacket);
        Self.IncludedAttributes := Self.IncludedAttributes + [saMimeType];
      end;

      if (Flags and SSH_FILEXFER_ATTR_LINK_COUNT) > 0 then
      begin
        if Ver < 6 then
          raise EElSFTPError.Create(SErrorInvalidPacket);
        Self.IncludedAttributes := Self.IncludedAttributes + [saLinkCount];
      end;

      if (Flags and SSH_FILEXFER_ATTR_UNTRANSLATED_NAME) > 0 then
      begin
        if Ver < 6 then
          raise EElSFTPError.Create(SErrorInvalidPacket);
        Self.IncludedAttributes := Self.IncludedAttributes + [saUName];
      end;

      if (Flags and SSH_FILEXFER_ATTR_EXTENDED) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saExtended];

      { byte type }
      Self.FileTypeByte := PByteArray(CurrP)[0];
      Inc(CurrP, 1);
      Dec(Size, 1);

      { uint64 size if flag SSH_FILEXFER_ATTR_SIZE }
      if (saSize in Self.IncludedAttributes) then
      begin
        Self.Size := ReadUINT64(CurrP, Size);
        Inc(CurrP, 8);
        Dec(Size, 8);
      end;
      
      { string owner if flag SSH_FILEXFER_ATTR_OWNERGROUP }
      if (saOwner in Self.IncludedAttributes) then
      begin
        Self.Owner := ReadString(CurrP, Size);
        Inc(CurrP, 4 + Length(Self.Owner));
        Dec(Size, 4 + Length(Self.Owner));
      end;

      { string group if flag SSH_FILEXFER_ATTR_OWNERGROUP }
      if (saGroup in Self.IncludedAttributes) then
      begin
        Self.Group := ReadString(CurrP, Size);
        Inc(CurrP, 4 + Length(Self.Group));
        Dec(Size, 4 + Length(Self.Group));
      end;

      { uint32 permissions if flag SSH_FILEXFER_ATTR_PERMISSIONS }
      if (saPermissions in Self.IncludedAttributes) then
      begin
        Self.Permissions := ReadLength(CurrP, Size);
        Inc(CurrP, 4);
        Dec(Size, 4);
      end;

      { int64 atime if flag ACCESSTIME }
      if (saATime in Self.IncludedAttributes) then
      begin
        Self.ATimeInt64 := ReadUINT64(CurrP, Size);
        Inc(CurrP, 8);
        Dec(Size, 8);
        { uint32 atime_nseconds if flag SUBSECOND_TIMES }
        if (saSubSeconds in Self.IncludedAttributes) then
        begin
          Self.ATimeMS := ReadLength(CurrP, Size);
          Inc(CurrP, 4);
          Dec(Size, 4);
        end
        else
          Self.ATimeMS := 0;
      end;

      { int64 createtime if flag SSH_FILEXFER_ATTR_CREATETIME }
      if (saCTime in Self.IncludedAttributes) then
      begin
        Self.CTimeInt64 := ReadUINT64(CurrP, Size);
        Inc(CurrP, 8);
        Dec(Size, 8);
        { uint32 createtime_nseconds if flag SUBSECOND_TIMES }
        if (saSubSeconds in Self.IncludedAttributes) then
        begin
          Self.CTimeMS := ReadLength(CurrP, Size);
          Inc(CurrP, 4);
          Dec(Size, 4);
        end
        else
          Self.CTimeMS := 0;
      end;
      { int64   mtime if flag MODIFYTIME }
      if (saMTime in Self.IncludedAttributes) then
      begin
        Self.MTimeInt64 := ReadUINT64(CurrP, Size);
        Inc(CurrP, 8);
        Dec(Size, 8);
        { uint32 atime_nseconds if flag SUBSECOND_TIMES }
        if (saSubSeconds in Self.IncludedAttributes) then
        begin
          Self.MTimeMS := ReadLength(CurrP, Size);
          Inc(CurrP, 4);
          Dec(Size, 4);
        end
        else
          Self.MTimeMS := 0;
      end;

      { int64 ctime if flag CTIME }
      if (saCATime in Self.IncludedAttributes) then
      begin
        Self.CATimeInt64 := ReadUINT64(CurrP, Size);
        Inc(CurrP, 8);
        Dec(Size, 8);
        { uint32 ctime_nseconds if flag SUBSECOND_TIMES }
        if (saSubSeconds in Self.IncludedAttributes) then
        begin
          Self.CATimeMS := ReadLength(CurrP, Size);
          Inc(CurrP, 4);
          Dec(Size, 4);
        end
        else
          Self.CATimeMS := 0;
      end;

      { string acl if flag ACL}
      if (saACL in Self.IncludedAttributes) then
      begin
        LACL := ReadString(CurrP, Size);
        Inc(CurrP, Length(LACL)+4);
        Dec(Size, Length(LACL)+4);
        ReadACL(Self, LACL);
      end;

      { uint32 attrib-bits if flag BITS }
      { uint32 attrib-bits-valid if flag BITS }
      if (saAttribBits in Self.IncludedAttributes) then
      begin
        Self.AttribBits := ReadLength(CurrP,Size);
        Inc(CurrP, 4);
        Dec(Size, 4);

        if Ver > 5 then
        begin
          Self.AttribBitsValid := ReadLength(CurrP,Size);
          Inc(CurrP, 4);
          Dec(Size, 4);
        end;  
      end;

      { byte text-hint if flag TEXT_HINT }
      if (saTextHint in Self.IncludedAttributes) then
      begin
        Self.TextHint := ByteToTextHint(PByte(CurrP)^);
        Inc(CurrP, 1);
        Dec(Size, 1);
      end;

      { string mime-type if flag MIME_TYPE }
      if (saMimeType in Self.IncludedAttributes) then
      begin
        Self.MimeType := ReadString(CurrP, Size);
        I := 4 + ReadLength(CurrP, Size);
        Inc(CurrP, I);
        Dec(Size, I);
      end;

      { uint32 link-count if flag LINK_COUNT }
      if (saLinkCount in Self.IncludedAttributes) then
      begin
        Self.LinkCount := ReadLength(CurrP, Size);
        Inc(CurrP, 4);
        Dec(Size, 4);
      end;
      
      { string untranslated-name if flag UNTRANSLATED_NAME }
      if (saUName in Self.IncludedAttributes) then
      begin
        Self.UntranslatedName := ReadString(CurrP, Size);
        I := 4 + ReadLength(CurrP, Size);
        Inc(CurrP, I);
        Dec(Size, I);
      end;

      { extended attributes }
      if (saExtended in Self.IncludedAttributes) then
      begin
        ExCount := ReadLength(CurrP, Size);
        Inc(CurrP, 4);
        Dec(Size, 4);
        for I := 0 to ExCount - 1 do
        begin
          Index := Self.AddExtendedAttribute;
          Self.ExtendedAttributes[Index].ExtType := ReadString(CurrP, Size);
          Inc(CurrP, 4 + Length(Self.ExtendedAttributes[Index].ExtType));
          Dec(Size, 4 + Length(Self.ExtendedAttributes[Index].ExtType));
          Self.ExtendedAttributes[Index].ExtData := ReadString(CurrP, Size);
          Inc(CurrP, 4 + Length(Self.ExtendedAttributes[Index].ExtData));
          Dec(Size, 4 + Length(Self.ExtendedAttributes[Index].ExtData));
        end;
      end;
    except
      raise;
    end;

    Result := OrigSize - Size;
  end
  else
  begin
    { Parsing attributes of older version, than 4 }
    CurrP := Buffer;
    OrigSize := Size;
    try
      Flags := ReadLength(CurrP, Size);
      Dec(Size, 4);
      Inc(CurrP, 4);
      Self.IncludedAttributes := [];
      Self.ClearExtendedAttributes;
      if (Flags and SSH_FILEXFER_ATTR_SIZE) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saSize];
      if (Flags and SSH_FILEXFER_ATTR_UIDGID) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saUID, saGID];
      if (Flags and SSH_FILEXFER_ATTR_PERMISSIONS) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saPermissions];
      if (Flags and SSH_FILEXFER_ATTR_ACMODTIME) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saATime, saMTime];
      if (Flags and SSH_FILEXFER_ATTR_EXTENDED) > 0 then
        Self.IncludedAttributes := Self.IncludedAttributes + [saExtended];
      if (saSize in Self.IncludedAttributes) then
      begin
        Self.Size := ReadUINT64(CurrP, Size);
        Dec(Size, 8);
        Inc(CurrP, 8);
      end;
      if (saUID in Self.IncludedAttributes) then
      begin
        Self.UID := ReadLength(CurrP, Size);
        Dec(Size, 4);
        Inc(CurrP, 4);
      end;
      if (saGID in Self.IncludedAttributes) then
      begin
        Self.GID := ReadLength(CurrP, Size);
        Dec(Size, 4);
        Inc(CurrP, 4);
      end;
      if (saPermissions in Self.IncludedAttributes) then
      begin
        Self.Permissions := ReadLength(CurrP, Size);
        Dec(Size, 4);
        Inc(CurrP, 4);
      end;
      if (saATime in Self.IncludedAttributes) then
      begin
        Self.ATimeMS := 0;
        Self.ATimeCardinal := ReadLength(CurrP, Size);
        Dec(Size, 4);
        Inc(CurrP, 4);
      end;
      if (saMTime in Self.IncludedAttributes) then
      begin
        Self.MTimeMS := 0;
        Self.MTimeCardinal := ReadLength(CurrP, Size);
        Dec(Size, 4);
        Inc(CurrP, 4);
      end;
      if (saExtended in Self.IncludedAttributes) then
      begin
        ExCount := ReadLength(CurrP, Size);
        Dec(Size, 4);
        Inc(CurrP, 4);
        
        for I := 0 to ExCount - 1 do
        begin
          Index := Self.AddExtendedAttribute;
          Self.ExtendedAttributes[Index].ExtType := ReadString(CurrP, Size);
          Dec(Size, 4 + Length(Self.ExtendedAttributes[Index].ExtType));
          Inc(CurrP, 4 + Length(Self.ExtendedAttributes[Index].ExtType));
          Self.ExtendedAttributes[Index].ExtData := ReadString(CurrP, Size);
          Dec(Size, 4 + Length(Self.ExtendedAttributes[Index].ExtData));
          Inc(CurrP, 4 + Length(Self.ExtendedAttributes[Index].ExtData));
        end;
      end;
    except
      raise;
    end;
    Result := OrigSize - Size;
  end;
end;

function TElSftpFileAttributes.GetMTime: TDateTime;
begin
  Result := EncodeSFTPTime(FMTime, FMTimeMS);
end;

function TElSftpFileAttributes.GetCTime: TDateTime;
begin
  Result := EncodeSFTPTime(FCTime, FCTimeMS);
end;

function TElSftpFileAttributes.GetCATime: TDateTime;
begin
  Result := EncodeSFTPTime(FCATime, FCATimeMS);
end;

function TElSftpFileAttributes.GetUserRead: boolean;
begin
  Result := FPermissions and 256 > 0;
end;

function TElSftpFileAttributes.GetUserWrite: boolean;
begin
  Result := FPermissions and 128 > 0;
end;

function TElSftpFileAttributes.GetUserExecute: boolean;
begin
  Result := FPermissions and 64 > 0;
end;

function TElSftpFileAttributes.GetGroupRead: boolean;
begin
  Result := FPermissions and 32 > 0;
end;

function TElSftpFileAttributes.GetGroupWrite: boolean;
begin
  Result := FPermissions and 16 > 0;
end;

function TElSftpFileAttributes.GetGroupExecute: boolean;
begin
  Result := FPermissions and 8 > 0;
end;

function TElSftpFileAttributes.GetOtherRead: boolean;
begin
  Result := FPermissions and 4 > 0;
end;

function TElSftpFileAttributes.GetOtherWrite: boolean;
begin
  Result := FPermissions and 2 > 0;
end;

function TElSftpFileAttributes.GetOtherExecute: boolean;
begin
  Result := FPermissions and 1 > 0;
end;

{JPM Modifications}
function TElSftpFileAttributes.GetUIDBit: boolean;
begin
  Result := FPermissions and S_ISUID > 0;
end;

function TElSftpFileAttributes.GetGIDBit: boolean;
begin
  Result := FPermissions and S_ISGID > 0;
end;

function TElSftpFileAttributes.GetStickyBit : boolean;
begin
  Result := FPermissions and S_ISVTX > 0;
end;
{end}

function TElSftpFileAttributes.GetDirectory: boolean;
begin
  { JPM Modification for File Type determination on some systems.
  With SFTP3, the file type is determined by using the permission
  bits. The file type is determined in a POSIX manner.
  The value is from libc.pas and are standard POSIX }
  Result := (FPermissions and $F000) = $4000;
  if Result=False then
  begin
    Result := (FFileType = SSH_FILEXFER_TYPE_DIRECTORY);
  end;
end;

function TElSftpFileAttributes.GetFileType : TSBSftpFileType;
begin
  { JPM Modification for File Type determination on some systems.
    The values are from libc.pas and are standard POSIX }
  case FFileType of
    0 :
    begin
      case (FPermissions and $F000) of
        $8000 : Result := ftFile; { Regular file.  }
        $4000 : Result := ftDirectory;   { Directory.  }
        $A000 : Result := ftSymblink;  { Symbolic link.  }
        $C000 : Result := ftSocket; { Socket.  }
        $2000 : Result := ftCharDevice; { Character device.  }
        $6000 : Result := ftBlockDevice;  { Block device.  }
        $1000 : Result := ftFIFO;  { FIFO.  }
      else
        Result := ftUnknown;
      end;
    end;
    SSH_FILEXFER_TYPE_REGULAR : Result := ftFile;
    SSH_FILEXFER_TYPE_DIRECTORY : Result := ftDirectory;
    SSH_FILEXFER_TYPE_SYMLINK : Result := ftSymblink;
    SSH_FILEXFER_TYPE_SPECIAL : Result := ftSpecial;
    SSH_FILEXFER_TYPE_UNKNOWN : Result := ftUnknown;
    SSH_FILEXFER_TYPE_SOCKET    : Result := ftSocket;
    SSH_FILEXFER_TYPE_CHAR_DEVICE : Result := ftCharDevice;
    SSH_FILEXFER_TYPE_BLOCK_DEVICE : Result := ftBlockDevice;
    SSH_FILEXFER_TYPE_FIFO      : Result :=  ftFIFO;
  else
    Result := ftUnknown;
  end;
end;

procedure TElSftpFileAttributes.SetATime(Value: TDateTime);
begin
  DecodeSFTPTime(Value, FATime, FATimeMS);
end;

procedure TElSftpFileAttributes.SetMTime(Value: TDateTime);
begin
  DecodeSFTPTime(Value, FMTime, FMTimeMS);
end;

procedure TElSftpFileAttributes.SetCTime(Value: TDateTime);
begin
  DecodeSFTPTime(Value, FCTime, FCTimeMS);
end;

procedure TElSftpFileAttributes.SetCATime(Value: TDateTime);
begin
  DecodeSFTPTime(Value, FCATime, FCATimeMS);
end;

procedure TElSftpFileAttributes.SetUserRead(Value: boolean);
begin
  if Value then
    FPermissions := FPermissions or 256
  else
    FPermissions := FPermissions and not (256);
end;

procedure TElSftpFileAttributes.SetUserWrite(Value: boolean);
begin
  if Value then
    FPermissions := FPermissions or 128
  else
    FPermissions := FPermissions and not (128);
end;

procedure TElSftpFileAttributes.SetUserExecute(Value: boolean);
begin
  if Value then
    FPermissions := FPermissions or 64
  else
    FPermissions := FPermissions and not (64);
end;

procedure TElSftpFileAttributes.SetGroupRead(Value: boolean);
begin
  if Value then
    FPermissions := FPermissions or 32
  else
    FPermissions := FPermissions and not (32);
end;

procedure TElSftpFileAttributes.SetGroupWrite(Value: boolean);
begin
  if Value then
    FPermissions := FPermissions or 16
  else
    FPermissions := FPermissions and not (16);
end;

procedure TElSftpFileAttributes.SetGroupExecute(Value: boolean);
begin
  if Value then
    FPermissions := FPermissions or 8
  else
    FPermissions := FPermissions and not (8);
end;

procedure TElSftpFileAttributes.SetOtherRead(Value: boolean);
begin
  if Value then
    FPermissions := FPermissions or 4
  else
    FPermissions := FPermissions and not (4);
end;

procedure TElSftpFileAttributes.SetOtherWrite(Value: boolean);
begin
  if Value then
    FPermissions := FPermissions or 2
  else
    FPermissions := FPermissions and not (2);
end;

procedure TElSftpFileAttributes.SetOtherExecute(Value: boolean);
begin
  if Value then
    FPermissions := FPermissions or 1
  else
    FPermissions := FPermissions and not (1);
end;

{JPM Modifications}
procedure TElSftpFileAttributes.SetUIDBit(Value: boolean);
begin
  FPermissions := FPermissions or S_ISUID;
end;

procedure TElSftpFileAttributes.SetGIDBit(Value: boolean);
begin
  FPermissions := FPermissions or S_ISGID;
end;

procedure TElSftpFileAttributes.SetStickyBit(Value: boolean);
begin
  FPermissions := FPermissions or S_ISVTX;
end;
{end}

procedure TElSftpFileAttributes.SetDirectory(Value: boolean);
begin
  if Value then
  begin
    FFileType := SSH_FILEXFER_TYPE_DIRECTORY;
    FPermissions := FPermissions or 16384 and not 32768;
  end
  else
  begin
    FFileType := SSH_FILEXFER_TYPE_REGULAR;
    FPermissions := FPermissions or 32768 and not 16384;
  end;
end;

procedure TElSftpFileAttributes.SetFileType(Value : TSBSftpFileType);
begin
  case Value of
    ftFile : FFileType := SSH_FILEXFER_TYPE_REGULAR;
    ftDirectory : FFileType := SSH_FILEXFER_TYPE_DIRECTORY;
    ftSymblink : FFileType := SSH_FILEXFER_TYPE_SYMLINK;
    ftSpecial : FFileType := SSH_FILEXFER_TYPE_SPECIAL;
    ftUnknown : FFileType :=  SSH_FILEXFER_TYPE_UNKNOWN;
    ftSocket : FFileType :=  SSH_FILEXFER_TYPE_SOCKET;
    ftCharDevice : FFileType := SSH_FILEXFER_TYPE_CHAR_DEVICE;
    ftBlockDevice : FFileType := SSH_FILEXFER_TYPE_BLOCK_DEVICE;
    ftFIFO : FFileType := SSH_FILEXFER_TYPE_FIFO;
  else
    FFileType := SSH_FILEXFER_TYPE_UNKNOWN;
  end;
end;

function TElSftpFileAttributes.GetACE(Index: integer) : TSBSftpACE;
begin
  if (Index >= 0) and (Index < FACEs.Count) then
    Result := FACEs.Items[Index]
  else
    Result := nil;
end;

function TElSftpFileAttributes.GetACECount : integer;
begin
  Result := FACEs.Count;
end;

function TElSftpFileAttributes.AddACE : integer;
begin
  Result := FACEs.Add(TSBSftpACE.Create);
end;

procedure TElSftpFileAttributes.RemoveACE(Index: integer);
begin
  if (Index >= 0) and (Index < FACEs.Count) then
  begin
    TSBSftpACE(FACEs.Items[Index]).Free;
    FACEs.Delete(Index);
  end;
end;

procedure TElSftpFileAttributes.ClearACEs;
var
  i : integer;
begin
  for i := 0 to FACEs.Count - 1 do
    TSBSftpACE(FACEs.Items[i]).Free;
    
  FACEs.Clear;
end;                        

function TElSftpFileAttributes.AddExtendedAttribute: integer;
begin
  Result := FExtendedAttributes.Add(TSBSftpExtendedAttribute.Create);
end;

procedure TElSftpFileAttributes.RemoveExtendedAttribute(Index: integer);
begin
  if (Index >= 0) and (Index < FExtendedAttributes.Count) then
  begin
    TSBSftpExtendedAttribute(FExtendedAttributes.Items[Index]).Free;
    FExtendedAttributes.Delete(Index);
  end;
end;

procedure TElSftpFileAttributes.ClearExtendedAttributes;
var
  i : integer;
begin
  for i := 0 to FExtendedAttributes.Count - 1 do
    TSBSftpExtendedAttribute(FExtendedAttributes.Items[i]).Free;
    
  FExtendedAttributes.Clear;
end;

function TElSftpFileAttributes.GetExtendedAttribute(Index: integer): TSBSftpExtendedAttribute;
begin
  if (Index >= 0) and (Index < FExtendedAttributes.Count) then
    Result := FExtendedAttributes.Items[Index]
  else
    Result := nil;
end;

function TElSftpFileAttributes.GetExtendedAttributeCount: integer;
begin
  Result := FExtendedAttributes.Count;
end;

////////////////////////////////////////////////////////////////////////////////
// TElSftpFileInfo

constructor TElSftpFileInfo.Create;
begin
  inherited;
  FName := '';
  FLongName := '';
  FAttributes := TElSftpFileAttributes.Create;
end;

destructor TElSftpFileInfo.Destroy;
begin
  FAttributes.Free;
  inherited;
end;

procedure TElSftpFileInfo.CopyTo(FileInfo: TElSftpFileInfo);
begin
  FileInfo.FName := FName;
  FileInfo.FLongName := FLongName;
  FAttributes.CopyTo(FileInfo.Attributes);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPExtendedProperties

constructor TElSFTPExtendedProperties.Create;
begin
  inherited;

  FDesiredAccess := 0;
  FAutoAdjustDesiredAccess := true;
  FRequestAttributes := [];
  FRenameFlags := [];
  FAutoAdjustRequestAttributes := true;
  FSupportedAvailable := false;
  FSupportedExtension := nil;
  FNewlineAvailable := false;
  FNewlineExtension := nil;
  FVersionsAvailable := false;
  FVersionsExtension := nil;
  FFilenameCharset := '';
  FVendorIDAvailable := false;
  FVendorIDExtension := nil;
end;

function TElSFTPExtendedProperties.GetRenameFlagsCardinal : cardinal;
begin
  Result := RenameFlagsToCardinal(FRenameFlags);
end;

function TElSFTPExtendedProperties.GetRequestAttributesCardinal : cardinal;
begin
  Result := 0;

  if saSize in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_SIZE;
  if saOwner in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_OWNERGROUP;
  if saGroup in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_OWNERGROUP;
  if saUID in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_UIDGID;
  if saGID in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_UIDGID;
  if saPermissions in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_PERMISSIONS;
  if saATime in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_ACCESSTIME;
  if saMTime in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_MODIFYTIME;
  if saCTime in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_CREATETIME;
  if saCATime in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_CTIME;
  if saSubSeconds in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_SUBSECOND_TIMES;
  if saAttribBits in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_BITS;
  if saACL in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_ACL;
  if saExtended in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_EXTENDED;
  if saASize in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_ALLOCATION_SIZE;
  if saTextHint in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_TEXT_HINT;
  if saMimeType in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_MIME_TYPE;
  if saLinkCount in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_LINK_COUNT;
  if saUName in FRequestAttributes then
    Result := Result or SSH_FILEXFER_ATTR_UNTRANSLATED_NAME;
end;

function TElSFTPExtendedProperties.RequestAttributesByVersion(Version : integer) : cardinal;
begin
  Result := 0;

  if Version < 4 then Exit;

  if FAutoAdjustRequestAttributes then
  begin
    if Version = 4 then
      Result := SSH_ATTR_REQUEST4
    else if Version = 5 then
      Result:= SSH_ATTR_REQUEST5
    else
      Result:= SSH_ATTR_REQUEST6;
  end
  else
  begin
    Result := RequestAttributesCardinal;

    if Version = 4 then
      Result := Result and SSH_FILEXFER_ATTR_V4
    else if Version = 5 then
      Result := Result and SSH_FILEXFER_ATTR_V5
    else if Version = 6 then
      Result := Result and SSH_FILEXFER_ATTR_V6;
  end;
end;

destructor TElSFTPExtendedProperties.Destroy;
begin
  (*

  These objects are disposed of when they are in the global list of extensions

  if FVersionsAvailable then
    FreeAndNil(FVersionsExtension);
  if FSupportedAvailable then
    FreeAndNil(FSupportedExtension);
  if FNewlineAvailable then
    FreeAndNil(FNewlineExtension);
  if FVendorIDAvailable then
    FreeAndNil(FVendorIDExtension);
  *)
  inherited;
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPExtendedAttribute
function TSBSFTPExtendedAttribute.LoadFromBuffer : boolean;
begin
  Result := false;
end;

procedure TSBSFTPExtendedAttribute.SaveToBuffer;
begin
  SetLength(FExtType, 0);
  SetLength(FExtData, 0);
end;

function TSBSFTPExtendedAttribute.Write : string;
begin
  try
    SaveToBuffer;
    Result := WriteString(FExtType) + WriteString(FExtData);
  except
    SetLength(Result, 0);
  end;
end;


////////////////////////////////////////////////////////////////////////////////
//  TElSFTPExtendedReply

function TElSFTPExtendedReply.LoadFromBuffer : boolean;
begin
  Result := false;
end;

procedure TElSFTPExtendedReply.SaveToBuffer;
begin
  SetLength(FReplyData, 0);
end;

function TElSFTPExtendedReply.Write : BufferType;
begin
  try
    SaveToBuffer;
    Result := FReplyData;
  except
    SetLength(Result, 0);
  end;  
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPNewlineExtension
function TElSFTPNewlineExtension.LoadFromBuffer : boolean;
begin
  if FExtType = SSH_EXT_NEWLINE then
  begin
    FNewline := (FExtData);
    Result := true;
  end
  else
  if FExtType = SSH_EXT_NEWLINE_VANDYKE then
  begin
    if (Length(FExtData) > 0) and (FExtData[1] = #0) then
      FNewline := ReadString(@FExtData[1], Length(FExtData))
    else
      FNewline := FExtData;
    Result := true;
  end
  else
  begin
    FNewline := '';
    Result := false;
  end;  
end;

procedure TElSFTPNewlineExtension.SaveToBuffer;
begin
  FExtType := (SSH_EXT_NEWLINE);
  FExtData := (FNewline);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPVersionsExtension

function TElSFTPVersionsExtension.LoadFromBuffer : boolean;
var
  Index, ChPos : integer;
  Ver : string;
begin
  Result := false;

  if FExtType = SSH_EXT_VERSIONS then
  begin
    FVersionsStr := ReadString(@FExtData[1], Length(FExtData));
    FVersions := [];

    Index := 1;
    ChPos := 1;

    while Index <= Length(FVersionsStr) do
    begin
      while (ChPos <= Length(FVersionsStr)) and 
            (FVersionsStr[ChPos ] <> ',') do
        Inc(ChPos);
      Ver := Copy(FVersionsStr, Index, ChPos - Index);
      Index := ChPos + 1;
      Inc(ChPos);

      if Ver = '1' then
        FVersions := FVersions + [sbSFTP1]
      else if Ver = '2' then
        FVersions := FVersions + [sbSFTP2]
      else if Ver = '3' then
        FVersions := FVersions + [sbSFTP3]
      else if Ver = '4' then
        FVersions := FVersions + [sbSFTP4]
      else if Ver = '5' then
        FVersions := FVersions + [sbSFTP5]
      else if Ver = '6' then
        FVersions := FVersions + [sbSFTP6];
    end;

    Result := true;
  end;
end;

procedure TElSFTPVersionsExtension.SaveToBuffer;
begin
  FExtType := (SSH_EXT_VERSIONS);
  FVersionsStr := '';

  if sbSFTP1 in FVersions then
    FVersionsStr := FVersionsStr + '1,';
  if sbSFTP2 in FVersions then
    FVersionsStr := FVersionsStr + '2,';
  if sbSFTP3 in FVersions then
    FVersionsStr := FVersionsStr + '3,';
  if sbSFTP4 in FVersions then
    FVersionsStr := FVersionsStr + '4,';
  if sbSFTP5 in FVersions then
    FVersionsStr := FVersionsStr + '5,';
  if sbSFTP6 in FVersions then
    FVersionsStr := FVersionsStr + '6,';

  if Length(FVersionsStr) > 0 then FVersionsStr := Copy(FVersionsStr, 1, Length(FVersionsStr) - 1);
  FExtData := WriteString(FVersionsStr);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPVersionSelectExtension

function TElSFTPVersionSelectExtension.LoadFromBuffer : boolean;
begin
  Result := false;
  if FExtType = SSH_EXT_VERSION_SELECT then
  begin
    FVersionStr := ReadString(@FExtData[1], Length(FExtData));

    if FVersionStr = '1' then FVersion := sbSFTP1
    else if FVersionStr = '2' then FVersion := sbSFTP2
    else if FVersionStr = '3' then FVersion := sbSFTP3
    else if FVersionStr = '4' then FVersion := sbSFTP4
    else if FVersionStr = '5' then FVersion := sbSFTP5
    else if FVersionStr = '6' then FVersion := sbSFTP6
    else
      Exit;

    Result := true;
  end;
end;

procedure TElSFTPVersionSelectExtension.SaveToBuffer;
begin
  FExtType := (SSH_EXT_VERSION_SELECT);

  if FVersion = sbSFTP1 then FVersionStr := '1'
  else if FVersion = sbSFTP2 then FVersionStr := '2'
  else if FVersion = sbSFTP3 then FVersionStr := '3'
  else if FVersion = sbSFTP4 then FVersionStr := '4'
  else if FVersion = sbSFTP5 then FVersionStr := '5'
  else if FVersion = sbSFTP6 then FVersionStr := '6';

  FExtData := WriteString(FVersionStr);
end;

function TElSFTPVersionSelectExtension.GetVersionInt : integer;
begin
  Result := 0;

  if FVersion = sbSFTP1 then Result := 1
  else if FVersion = sbSFTP2 then Result := 2
  else if FVersion = sbSFTP3 then Result := 3
  else if FVersion = sbSFTP4 then Result := 4
  else if FVersion = sbSFTP5 then Result := 5
  else if FVersion = sbSFTP6 then Result := 6;
end;

procedure TElSFTPVersionSelectExtension.SetVersionInt(Value : integer);
begin
  if Value = 0 then FVersion := sbSFTP0
  else if Value = 1 then FVersion := sbSFTP1
  else if Value = 2 then FVersion := sbSFTP2
  else if Value = 3 then FVersion := sbSFTP3
  else if Value = 4 then FVersion := sbSFTP4
  else if Value = 5 then FVersion := sbSFTP5
  else if Value = 6 then FVersion := sbSFTP6;        
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPFilenameCharsetExtension

function TElSFTPFilenameCharsetExtension.LoadFromBuffer : boolean;
begin
  Result := false;

  if FExtType = SSH_EXT_FILENAME_CHARSET then
  begin
    FCharset := ReadString(@FExtData[1], Length(FExtData));
    Result := true;
  end;
end;

procedure TElSFTPFilenameCharsetExtension.SaveToBuffer;
begin
  FExtType := (SSH_EXT_FILENAME_CHARSET);
  FExtData := WriteString(FCharset);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPFilenameTranslationExtension

function TElSFTPFilenameTranslationExtension.LoadFromBuffer : boolean;
begin
  Result := false;

  if (FExtType = SSH_EXT_FILENAME_TRANSLATION_CONTROL) and 
     (Length(FExtData) > 0) then
  begin
    FDoTranslate := ReadBoolean(@FExtData[1], Length(FExtData));
    Result := true;
  end;
end;

procedure TElSFTPFilenameTranslationExtension.SaveToBuffer;
begin
  FExtType := (SSH_EXT_FILENAME_TRANSLATION_CONTROL);
  FExtData := WriteBoolean(FDoTranslate);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPSupportedExtension

constructor TElSFTPSupportedExtension.Create;
begin
  inherited;

  FSupportedAttrExtensions := TStringList.Create;
  FSupportedExtensions := TStringList.Create;
  Clear;
end;

procedure TElSFTPSupportedExtension.FillDefault;
begin
  Clear;

  FSupportedAttributes := [saSize, saOwner, saGroup, saUID, saGID, saPermissions, saATime,
    saMTime, saCTime, saSubSeconds, saAttribBits, saACL, saExtended, saCATime,
    saASize, saTextHint, saMimeType, saLinkCount, saUName];
  FSupportedAccessModes := [faReadLock, faWriteLock, faDeleteLock, faBlockAdvisory];
  FSupportedOpenModes := [fmRead, fmWrite, fmAppend, fmCreate, fmTruncate,
    fmExcl, fmText, fmOpenOrCreate, fmAppendAtomic, fmNoFollow, fmDeleteOnClose];

  FSupportedExtensions.Add(SSH_EXT_NEWLINE);
  FSupportedExtensions.Add(SSH_EXT_NEWLINE_VANDYKE);
  FSupportedExtensions.Add(SSH_EXT_VERSION_SELECT);
  FSupportedExtensions.Add(SSH_EXT_FILENAME_TRANSLATION_CONTROL);
  FSupportedExtensions.Add(SSH_EXT_TEXTSEEK);
  FSupportedExtensions.Add(SSH_EXT_VENDOR_ID);
  FSupportedExtensions.Add(SSH_EXT_CHECK_FILE_HANDLE);
  FSupportedExtensions.Add(SSH_EXT_CHECK_FILE_NAME);
  FSupportedExtensions.Add(SSH_EXT_SPACE_AVAILABLE);
  FSupportedExtensions.Add(SSH_EXT_HOME_DIRECTORY);
  FSupportedExtensions.Add(SSH_EXT_COPY_FILE);
  FSupportedExtensions.Add(SSH_EXT_COPY_DATA);
  FSupportedExtensions.Add(SSH_EXT_GET_TEMP_FOLDER);
  FSupportedExtensions.Add(SSH_EXT_MAKE_TEMP_FOLDER);

  FSupportedAttribBits := $FFF;
  FSupportedAccessMask := $1F01FF;
  FSupportedOpenBlockVector := $FFFF;
  FSupportedBlockVector := $FFFF;
end;

procedure TElSFTPSupportedExtension.Clear;
begin
  FVersion := 0;
  FSupportedAttribBits := 0;
  FSupportedAccessMask := 0;
  FMaxReadSize := 0;
  FSupportedBlockVector := 0;
  FSupportedOpenBlockVector := 0;
  FSupportedAccessModes := [];
  FSupportedAttributes := [];
  FSupportedOpenModes := [];
  FSupportedAttrExtensions.Clear;
  FSupportedExtensions.Clear;
end;

destructor TElSFTPSupportedExtension.Destroy;
begin
  FreeAndNil(FSupportedAttrExtensions);
  FreeAndNil(FSupportedExtensions);

  inherited;
end;


function TElSFTPSupportedExtension.LoadFromBuffer : boolean;
var
  Val, Item, AttrCount : cardinal;
  Index, Size : integer;
begin
  Result := false;
  Clear;

  Index := 0;
  Size := Length(FExtData);

  if FExtType = SSH_EXT_SUPPORTED then
  begin
    { SFTP5 extension style }
    if Size < 24 then Exit;
    
    try
      Val := ReadLength(@FExtData[Index + 1], Size);
      FSupportedAttributes := CardinalToAttributes(Val);
      Inc(Index, 4); Dec(Size, 4);
      FSupportedAttribBits := ReadLength(@FExtData[Index + 1], Size);
      Inc(Index, 4); Dec(Size, 4);
      Val := ReadLength(@FExtData[Index + 1], Size);
      FSupportedOpenModes := CardinalToFileOpenModes(Val);
      FSupportedAccessModes := CardinalToFileOpenAccess(Val);
      Inc(Index, 4); Dec(Size, 4);
      FSupportedAccessMask := ReadLength(@FExtData[Index + 1], Size);
      Inc(Index, 4); Dec(Size, 4);
      FMaxReadSize := ReadLength(@FExtData[Index + 1], Size);
      Inc(Index, 4); Dec(Size, 4);

      while Size > 0 do
      begin
        FSupportedExtensions.Add(ReadString(@FExtData[Index + 1], Size));
        Val := ReadLength(@FExtData[Index + 1], Size);
        Inc(Index, Val + 4); Dec(Size, Val + 4);
      end;

      FVersion := 1;
      Result := true;
    except
      Clear;
    end;
  end
  else
  if FExtType = SSH_EXT_SUPPORTED2 then
  begin
    { SFTP6 extension }
    if Size < 32 then Exit;
    try
      Val := ReadLength(@FExtData[Index + 1], Size);
      FSupportedAttributes := CardinalToAttributes(Val);
      Inc(Index, 4); Dec(Size, 4);
      FSupportedAttribBits := ReadLength(@FExtData[Index + 1], Size);
      Inc(Index, 4); Dec(Size, 4);
      Val := ReadLength(@FExtData[Index + 1], Size);
      FSupportedOpenModes := CardinalToFileOpenModes(Val);
      FSupportedAccessModes := CardinalToFileOpenAccess(Val);
      Inc(Index, 4); Dec(Size, 4);
      FSupportedAccessMask := ReadLength(@FExtData[Index + 1], Size);
      Inc(Index, 4); Dec(Size, 4);
      FMaxReadSize := ReadLength(@FExtData[Index + 1], Size);
      Inc(Index, 4); Dec(Size, 4);
      FSupportedOpenBlockVector := ReadUINT16(@FExtData[Index + 1], Size);
      Inc(Index, 2); Dec(Size, 2);
      FSupportedBlockVector := ReadUINT16(@FExtData[Index + 1], Size);
      Inc(Index, 2); Dec(Size, 2);

      AttrCount := ReadLength(@FExtData[Index + 1], Size);
      Inc(Index, 4); Dec(Size, 4);

      for Item := 1 to AttrCount do
      begin
        FSupportedAttrExtensions.Add(ReadString(@FExtData[Index + 1], Size));
        Val := ReadLength(@FExtData[Index + 1], Size);
        Inc(Index, Val + 4); Dec(Size, Val + 4);
      end;

      AttrCount := ReadLength(@FExtData[Index + 1], Size);
      Inc(Index, 4); Dec(Size, 4);

      for Item := 1 to AttrCount do
      begin
        FSupportedExtensions.Add(ReadString(@FExtData[Index + 1], Size));
        Val := ReadLength(@FExtData[Index + 1], Size);
        Inc(Index, Val + 4); Dec(Size, Val + 4);
      end;

      FVersion := 2;
      Result := true;
    except
      Clear;
    end;
  end;
end;

procedure TElSFTPSupportedExtension.SaveToBuffer;
var
  Index : integer;
  Flags : cardinal;
begin
  if FVersion = 1 then
  begin
    Flags := FileOpenModesToCardinal(FSupportedOpenModes) or
      FileOpenAccessToCardinal(FSupportedAccessModes);
    FExtType := (SSH_EXT_SUPPORTED);


    FExtData := WriteUINT32(AttributesToCardinal(FSupportedAttributes)) +
      WriteUINT32(FSupportedAttribBits) + WriteUINT32(Flags) +
      WriteUINT32(FSupportedAccessMask) + WriteUINT32(FMaxReadSize);

    for Index := 0 to FSupportedExtensions.Count - 1 do
      FExtData := FExtData + WriteString(FSupportedExtensions[Index]);
  end
  else
  if FVersion = 2 then
  begin
    Flags := FileOpenModesToCardinal(FSupportedOpenModes) or
      FileOpenAccessToCardinal(FSupportedAccessModes);
    FExtType := (SSH_EXT_SUPPORTED2);


    FExtData := WriteUINT32(AttributesToCardinal(FSupportedAttributes)) +
      WriteUINT32(FSupportedAttribBits) + WriteUINT32(Flags) +
      WriteUINT32(FSupportedAccessMask) + WriteUINT32(FMaxReadSize) +
      WriteUINT16(FSupportedOpenBlockVector) + WriteUINT16(FSupportedBlockVector) +
      WriteUINT32(FSupportedAttrExtensions.Count);

    for Index := 0 to FSupportedAttrExtensions.Count - 1 do
      FExtData := FExtData + WriteString(FSupportedAttrExtensions[Index]);

    FExtData := FExtData + WriteUINT32(FSupportedExtensions.Count);

    for Index := 0 to FSupportedExtensions.Count - 1 do
      FExtData := FExtData + WriteString(FSupportedExtensions[Index]);
  end
  else
  begin
    SetLength(FExtType, 0);
    SetLength(FExtData, 0);
  end;
end;

function TElSFTPSupportedExtension.IsSupportedOpenBlockMode(Mode : TSBSftpFileOpenAccess) : boolean;
var
  Index : word;
begin
  Index := 0;
  if faReadLock in Mode then
    Index := Index or 1;
  if faWriteLock in Mode then
    Index := Index or 2;
  if faDeleteLock in Mode then
    Index := Index or 4;
  if faBlockAdvisory in Mode then
    Index := Index or 8;

  if (FSupportedOpenBlockVector and (1 shl Index)) <> 0 then
    Result := true
  else
    Result := false;
end;

procedure TElSFTPSupportedExtension.SetSupportedOpenBlockMode(Mode : TSBSftpFileOpenAccess; Supported : boolean);
var
  Index : word;
begin
  Index := 0;
  if faReadLock in Mode then
    Index := Index or 1;
  if faWriteLock in Mode then
    Index := Index or 2;
  if faDeleteLock in Mode then
    Index := Index or 4;
  if faBlockAdvisory in Mode then
    Index := Index or 8;

  if Supported then
    FSupportedOpenBlockVector := FSupportedOpenBlockVector or (1 shl Index)
  else
    FSupportedOpenBlockVector := FSupportedOpenBlockVector and not (1 shl Index);
end;

function TElSFTPSupportedExtension.IsSupportedBlockMode(Mode : TSBSftpFileOpenAccess) : boolean;
var
  Index : word;
begin
  Index := 0;
  if faReadLock in Mode then
    Index := Index or 1;
  if faWriteLock in Mode then
    Index := Index or 2;
  if faDeleteLock in Mode then
    Index := Index or 4;
  if faBlockAdvisory in Mode then
    Index := Index or 8;

  if (FSupportedBlockVector and (1 shl Index)) <> 0 then
    Result := true
  else
    Result := false;
end;

procedure TElSFTPSupportedExtension.SetSupportedBlockMode(Mode : TSBSftpFileOpenAccess; Supported : boolean);
var
  Index : word;
begin
  Index := 0;
  if faReadLock in Mode then
    Index := Index or 1;
  if faWriteLock in Mode then
    Index := Index or 2;
  if faDeleteLock in Mode then
    Index := Index or 4;
  if faBlockAdvisory in Mode then
    Index := Index or 8;

  if Supported then
    FSupportedBlockVector := FSupportedBlockVector or (1 shl Index)
  else
    FSupportedBlockVector := FSupportedBlockVector and not (1 shl Index);
end;

function TElSFTPSupportedExtension.IsSupportedExtension(ExtName : string) : boolean;
begin
  Result := (FSupportedExtensions.IndexOf(ExtName) <> -1);
end;

procedure TElSFTPSupportedExtension.SetSupportedExtension(ExtName : string;
  Support : boolean);
var
  Index : integer;
begin
  Index := FSupportedExtensions.IndexOf(ExtName);
  if (not Support) and (Index > -1) then
    FSupportedExtensions.Delete(Index);
  if Support and (Index = -1) then
    FSupportedExtensions.Add(ExtName);
end;

function TElSFTPSupportedExtension.IsSupportedAttrExtension(ExtName : string) : boolean;
begin
  Result := (FSupportedAttrExtensions.IndexOf(ExtName) <> -1);
end;

procedure TElSFTPSupportedExtension.SetSupportedAttrExtension(ExtName : string;
  Support : boolean);
var
  Index : integer;
begin
  Index := FSupportedAttrExtensions.IndexOf(ExtName);
  if (not Support) and (Index > -1) then
    FSupportedAttrExtensions.Delete(Index);
  if Support and (Index = -1) then
    FSupportedAttrExtensions.Add(ExtName);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPVendorIDExtension

constructor TElSFTPVendorIDExtension.Create;
begin
  inherited;
  
  FVendorName := SSH_SBB_VENDOR_NAME;
  FProductName := SSH_SBB_PRODUCT_NAME;
  FProductVersion := SSH_SBB_PRODUCT_VERSION;
  FBuildNumber := SSH_SBB_PRODUCT_BUILD;
  SaveToBuffer;
end;

function TElSFTPVendorIDExtension.LoadFromBuffer : boolean;
var
  Index, Len, Size : integer;
begin
  Result := false;
  if not (FExtType = SSH_EXT_VENDOR_ID) then
    Exit;

  Index := 0;
  Size := Length(FExtData);
  if Size < 20 then Exit;

  try
    FVendorName := ReadString(@FExtData[Index + 1], Size);
    Len := ReadLength(@FExtData[Index + 1], Size) + 4;
    Dec(Size, Len);
    Inc(Index, Len);
    FProductName := ReadString(@FExtData[Index + 1], Size);
    Len := ReadLength(@FExtData[Index + 1], Size) + 4;
    Dec(Size, Len);
    Inc(Index, Len);
    FProductVersion := ReadString(@FExtData[Index + 1], Size);
    Len := ReadLength(@FExtData[Index + 1], Size) + 4;
    Dec(Size, Len);
    Inc(Index, Len);
    FBuildNumber := ReadUINT64(@FExtData[Index + 1], Size);
    Result := true;
  except
    ;
  end;
end;

procedure TElSFTPVendorIDExtension.SaveToBuffer;
begin
  FExtType := (SSH_EXT_VENDOR_ID);

  FExtData := WriteString(FVendorName) + WriteString(FProductName) +
    WriteString(FProductVersion) + WriteUINT64(FBuildNumber);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPCheckFileExtension

constructor TElSFTPCheckFileExtension.Create;
begin
  inherited;
  
  FHashAlgorithms := SSH_CHECKFILE_HASH_ALGORITHMS;
end;

function TElSFTPCheckFileExtension.LoadFromBuffer : boolean;
var
  IsHandle : boolean;
  Index, Size, Len : integer;
begin
  Result := false;
  if (FExtType = SSH_EXT_CHECK_FILE_HANDLE) then
    IsHandle := true
  else
  if (FExtType = SSH_EXT_CHECK_FILE_NAME) then
    IsHandle := false
  else
    Exit;

  Index := 0;

  Size := Length(FExtData);

  if Size < 28 then Exit;

  try
    if IsHandle then
    begin
      FHandle := ReadString(@FExtData[Index + 1], Size);
      FName := '';
    end
    else
    begin
      FName := ReadString(@FExtData[Index + 1], Size);
      FHandle := EmptyBuffer;
    end;

    Len := ReadLength(@FExtData[Index + 1], Size) + 4;
    Inc(Index, Len); Dec(Size, Len);
    FHashAlgorithms := ReadString(@FExtData[Index + 1], Size);
    Len := ReadLength(@FExtData[Index + 1], Size) + 4;
    Inc(Index, Len); Dec(Size, Len);

    FStartOffset := ReadUINT64(@FExtData[Index + 1], Size);
    Inc(Index, 8); Dec(Size, 8);
    FLength := ReadUINT64(@FExtData[Index + 1], Size);
    Inc(Index, 8); Dec(Size, 8);
    FBlockSize := ReadLength(@FExtData[Index + 1], Size);

    Result := true;
  except
    ;
  end;
end;

procedure TElSFTPCheckFileExtension.SaveToBuffer;
begin
  if Length(FHandle) > 0 then
  begin
    FExtType := (SSH_EXT_CHECK_FILE_HANDLE);
    FExtData := WriteString(FHandle);
  end
  else
  begin
    FExtType := (SSH_EXT_CHECK_FILE_NAME);
    FExtData := WriteString((FName));
  end;


  FExtData := FExtData + WriteString(FHashAlgorithms) + WriteUINT64(FStartOffset) +
    WriteUINT64(FLength) + WriteUINT32(FBlockSize);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPSpaceAvailableExtension

function TElSFTPSpaceAvailableExtension.LoadFromBuffer : boolean;
var
  Index, Size : integer;
begin
  Result := false;
  if not (FExtType = SSH_EXT_SPACE_AVAILABLE) then
    Exit;

  Index := 0;
  Size := Length(FExtData);

  try
    FPath := ReadString(@FExtData[Index + 1], Size);
    Result := true;
  except
    ;
  end;
end;

procedure TElSFTPSpaceAvailableExtension.SaveToBuffer;
begin
  FExtType := (SSH_EXT_SPACE_AVAILABLE);
  FExtData := WriteString((FPath));
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPHomeDirectoryExtension

function TElSFTPHomeDirectoryExtension.LoadFromBuffer : boolean;
begin
  Result := false;
  if not (FExtType = SSH_EXT_HOME_DIRECTORY) then
    Exit;
  try
    FUsername := ReadString(@FExtData[1], Length(FExtData));
    Result := true;
  except
    ;
  end;
end;

procedure TElSFTPHomeDirectoryExtension.SaveToBuffer;
begin
  FExtType := (SSH_EXT_HOME_DIRECTORY);
  FExtData := WriteString((FUsername));
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPCopyFileExtension

function TElSFTPCopyFileExtension.LoadFromBuffer : boolean;
var
  Index, Len, Size : integer;
begin
  Result := false;
  if not (FExtType = SSH_EXT_COPY_FILE) then
    Exit;

  Index := 0;
  Size := Length(FExtData);

  try
    FSource := ReadString(@FExtData[Index + 1], Size);
    Len := ReadLength(@FExtData[Index + 1], Size) + 4;
    Inc(Index, Len); Dec(Size, Len);
    FDestination := ReadString(@FExtData[Index + 1], Size);
    Len := ReadLength(@FExtData[Index + 1], Size) + 4;
    Inc(Index, Len); Dec(Size, Len);
    FOverwrite := ReadBoolean(@FExtData[Index + 1], Size);

    Result := true;
  except
    ;
  end;
end;

procedure TElSFTPCopyFileExtension.SaveToBuffer;
begin
  FExtType := (SSH_EXT_COPY_FILE);

  FExtData := WriteString(FSource) + WriteString(FDestination) +
    WriteBoolean(FOverwrite);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPCopyDataExtension

function TElSFTPCopyDataExtension.LoadFromBuffer : boolean;
var
  Index, Len, Size : integer;
begin
  Result := false;
  if not (FExtType = SSH_EXT_COPY_DATA) then
    Exit;

  Index := 0;
  Size := Length(FExtData);

  try
    FReadHandle := ReadString(@FExtData[Index + 1], Size);
    Len := ReadLength(@FExtData[Index + 1], Size) + 4;
    Inc(Index, Len); Dec(Size, Len);

    FReadOffset := ReadUINT64(@FExtData[Index + 1], Size);
    Inc(Index, 8); Dec(Size, 8);

    FWriteHandle := ReadString(@FExtData[Index + 1], Size);

    Len := ReadLength(@FExtData[Index + 1], Size) + 4;
    Inc(Index, Len); Dec(Size, Len);

    FWriteOffset := ReadUINT64(@FExtData[Index + 1], Size);
    Inc(Index, 8); Dec(Size, 8);

    FDataLength := ReadUINT64(@FExtData[Index + 1], Size);

    Result := true;
  except
    ;
  end;
end;

procedure TElSFTPCopyDataExtension.SaveToBuffer;
begin
  FExtType := (SSH_EXT_COPY_DATA);

  FExtData := WriteString(FReadHandle) + WriteUINT64(FReadOffset) +
    WriteString(FWriteHandle) + WriteUINT64(FWriteOffset) +
    WriteUINT64(FDataLength);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPCheckFileReply

constructor TElSFTPCheckFileReply.Create;
begin
  inherited;

  FHashes := TStringList.Create;
end;

destructor TElSFTPCheckFileReply.Destroy;
begin
  FreeAndNil(FHashes);
  inherited;
end;

function TElSFTPCheckFileReply.GetHashSize : integer;
begin
  Result := GetDigestSizeBits(FHashAlgorithm);
  if Result <> -1 then Result := Result div 8;
end;

function TElSFTPCheckFileReply.LoadFromBuffer : boolean;
var
  HashAlg : string;
  Index, Size, HSize : cardinal;
begin
  Result := false;
  Size := Length(FReplyData);
  Index := 0;
  if Length(FReplyData) < 4 then Exit;

  try
    HashAlg := ReadString(@FReplyData[1], Size);
    Index := Index + ReadLength(@FReplyData[1], Size) + 4;

    if HashAlg = 'md5' then
      FHashAlgorithm := SB_ALGORITHM_DGST_MD5
    else if HashAlg = 'sha1' then
      FHashAlgorithm := SB_ALGORITHM_DGST_SHA1
    else if HashAlg = 'sha224' then
      FHashAlgorithm := SB_ALGORITHM_DGST_SHA224
    else if HashAlg = 'sha256' then
      FHashAlgorithm := SB_ALGORITHM_DGST_SHA256
    else if HashAlg = 'sha384' then
      FHashAlgorithm := SB_ALGORITHM_DGST_SHA384
    else if HashAlg = 'sha512' then
      FHashAlgorithm := SB_ALGORITHM_DGST_SHA512
    else if HashAlg = 'crc32' then
      FHashAlgorithm := SB_ALGORITHM_DGST_CRC32
    else
      Exit;

    HSize := HashSize;

    while Index + Cardinal(HashSize) <= Size do
    begin
      Hashes.Add(Copy(FReplyData, Index + 1, HSize));
      Index := Index + HSize;
    end;

    Result := true;
  except
    ;
  end;
end;

procedure TElSFTPCheckFileReply.SaveToBuffer;
var
  HashAlg : string;
  Size, Index, HSize : integer;
begin
  SetLength(FReplyData, 0);

  if FHashAlgorithm = SB_ALGORITHM_DGST_MD5 then HashAlg := 'md5'
  else if FHashAlgorithm = SB_ALGORITHM_DGST_SHA1 then HashAlg := 'sha1'
  else if FHashAlgorithm = SB_ALGORITHM_DGST_SHA224 then HashAlg := 'sha224'
  else if FHashAlgorithm = SB_ALGORITHM_DGST_SHA256 then HashAlg := 'sha256'
  else if FHashAlgorithm = SB_ALGORITHM_DGST_SHA384 then HashAlg := 'sha384'
  else if FHashAlgorithm = SB_ALGORITHM_DGST_SHA512 then HashAlg := 'sha512'
  else if FHashAlgorithm = SB_ALGORITHM_DGST_CRC32 then HashAlg := 'crc32'
  else
    Exit;

  FReplyData := WriteString((HashAlg));
  Size := Length(FReplyData);
  HSize := HashSize;
  SetLength(FReplyData, Size + FHashes.Count * HSize);
  for Index := 0 to Hashes.Count - 1 do
    Move(Hashes[Index][1], FReplyData[Size + Index * HSize + 1], HSize);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSFTPSpaceAvailableReply

function TElSFTPSpaceAvailableReply.LoadFromBuffer : boolean;
begin
  Result := false;
  if Length(FReplyData) < 36 then Exit;

  try
    FBytesOnDevice := ReadUINT64(@FReplyData[1], 8);
    FUnusedBytesOnDevice := ReadUINT64(@FReplyData[9], 8);
    FBytesAvailableToUser := ReadUINT64(@FReplyData[17], 8);
    FUnusedBytesAvailableToUser := ReadUINT64(@FReplyData[25], 8);
    FBytesPerAllocationUnit := ReadLength(@FReplyData[33], 4);
    Result := true;
  except
    ;
  end;
end;

procedure TElSFTPSpaceAvailableReply.SaveToBuffer;
begin
  FReplyData := WriteUINT64(FBytesOnDevice) + WriteUINT64(FUnusedBytesOnDevice) +
    WriteUINT64(FBytesAvailableToUser) + WriteUINT64(FUnusedBytesAvailableToUser) +
    WriteUINT32(FBytesPerAllocationUnit);
end;

////////////////////////////////////////////////////////////////////////////////
// Auxiliary routines

{JPM MOdifications}
function WriteACL(Attrs : TElSftpFileAttributes) : String;
var
  i : Integer;
begin
{format:

       uint32   ace-count

       repeated ace-count time:
       uint32   ace-type
       uint32   ace-flag
       uint32   ace-mask
       string   who [UTF-8]
}
  Result := '';
  for i := 0 to Attrs.ACECount - 1 do
  begin
    Result := Result + WriteUInt32(Attrs.ACEs[i].ACEType);
    Result := Result + WriteUInt32(Attrs.ACEs[i].ACEFlags );
    Result := Result + WriteUInt32(Attrs.ACEs[i].ACEMask);
    Result := Result + WriteString(Attrs.ACEs[i].Who);
  end;
  Result := WriteUINT32(Attrs.ACECount) + Result;
end;

procedure ReadACL(Attrs : TElSftpFileAttributes; const AACL : String);
var
  CurrP: ^byte;
  VACECount, Size : Integer;
  i, ind : Integer;

{format:

       uint32   ace-count

       repeated ace-count time:
       uint32   ace-type
       uint32   ace-flag
       uint32   ace-mask
       string   who [UTF-8]
}
begin
  CurrP := @AACL[1];
  Size := Length(AACL);
  VACECount := ReadLength(CurrP, Size);
//  SetLength(Result, VACECount);
  Inc(CurrP, 4);
  Dec(Size, 4);
  for i := 0 to VACECount - 1 do
  begin
    ind := Attrs.AddACE;
    Attrs.ACEs[ind].ACEType := ReadLength(CurrP, Size);
    Inc(CurrP, 4);
    Dec(Size, 4);
    Attrs.ACEs[ind].ACEFlags := ReadLength(CurrP, Size);
    Inc(CurrP, 4);
    Dec(Size, 4);
    Attrs.ACEs[ind].ACEMask := ReadLength(CurrP, Size);
    Inc(CurrP, 4);
    Dec(Size, 4);
    Attrs.ACEs[ind].Who := ReadString(CurrP, Size);
    Inc(CurrP, 4 + Length(Attrs.ACEs[ind].Who));
    Dec(Size, 4 + Length(Attrs.ACEs[ind].Who));
  end;
end;
{end}

procedure WriteDefaultAttributes(var Attributes:
  TElSftpFileAttributes);
begin
  Attributes := TElSftpFileAttributes.Create;
  Attributes.FileType := ftFile;
  Attributes.IncludedAttributes := [];
end;

function RealPathControlToByte(Value : TSBSFTPRealpathControl) : byte;
begin
  case Value of
    rcStatIf     : Result := SSH_FXP_REALPATH_STAT_IF;
    rcStatAlways : Result := SSH_FXP_REALPATH_STAT_ALWAYS;
  else
    Result := SSH_FXP_REALPATH_NO_CHECK;
  end;
end;

function ByteToRealPathControl(Value : byte) : TSBSFTPRealpathControl;
begin
  case Value of
    SSH_FXP_REALPATH_STAT_IF     : Result := rcStatIf;
    SSH_FXP_REALPATH_STAT_ALWAYS : Result := rcStatAlways;
  else
    Result := rcNoCheck;
  end;
end;

function FileOpenAccessToCardinal(Value : TSBSftpFileOpenAccess) : cardinal;
begin
  Result := 0;
  if faReadLock in Value then
    Result := Result or SSH_FXF_ACCESS_BLOCK_READ;
  if faWriteLock in Value then
    Result := Result or SSH_FXF_ACCESS_BLOCK_WRITE;
  if faDeleteLock in Value then
    Result := Result or SSH_FXF_ACCESS_BLOCK_DELETE;
  if faBlockAdvisory in Value then
    Result := Result or SSH_FXF_ACCESS_BLOCK_ADVISORY;
end;

function CardinalToFileOpenAccess(Value : Cardinal) : TSBSftpFileOpenAccess;
begin
  Result := [];
  if (Value and SSH_FXF_ACCESS_BLOCK_READ) <> 0 then
    Result := Result  + [faReadLock];
  if (Value and SSH_FXF_ACCESS_BLOCK_WRITE) <> 0 then
    Result := Result  + [faWriteLock];
  if (Value and SSH_FXF_ACCESS_BLOCK_DELETE) <> 0 then
    Result := Result  + [faDeleteLock];
  if (Value and SSH_FXF_ACCESS_BLOCK_ADVISORY) <> 0 then
    Result := Result  + [faBlockAdvisory];
end;

function CardinalToFileOpenModes(Value : Cardinal) : TSBSftpFileOpenModes;
begin
  Result := [];

  {(fmRead, fmWrite, fmAppend, fmCreate, fmTruncate,
    fmExcl, fmText, fmOpenOrCreate, fmAppendAtomic, fmNoFollow, fmDeleteOnClose)}

  if (Value and SSH_FXF_ACCESS_DISPOSITION) = SSH_FXF_CREATE_NEW then
    Result := Result  + [fmCreate];

  if (Value and SSH_FXF_ACCESS_DISPOSITION) = SSH_FXF_CREATE_TRUNCATE then
    Result := Result  + [fmCreate, fmTruncate];

  if (Value and SSH_FXF_ACCESS_DISPOSITION) = SSH_FXF_OPEN_OR_CREATE then
    Result := Result  + [fmOpenOrCreate];

  if (Value and SSH_FXF_ACCESS_DISPOSITION) = SSH_FXF_TRUNCATE_EXISTING then
    Result := Result  + [fmTruncate];

  if (Value and SSH_FXF_ACCESS_APPEND_DATA) <> 0 then
    Result := Result  + [fmAppend];

  if (Value and SSH_FXF_ACCESS_APPEND_DATA_ATOMIC) <> 0 then
    Result := Result  + [fmAppendAtomic];

  if (Value and SSH_FXF_ACCESS_TEXT_MODE) <> 0 then
    Result := Result  + [fmText];

  if (Value and SSH_FXF_ACCESS_NOFOLLOW) <> 0 then
    Result := Result  + [fmNoFollow];

  if (Value and SSH_FXF_ACCESS_DELETE_ON_CLOSE) <> 0 then
    Result := Result  + [fmDeleteOnClose];
end;

function FileOpenModesToCardinal(Value : TSBSftpFileOpenModes) : Cardinal;
begin
  if fmOpenOrCreate in Value then
  begin
    if fmTruncate in Value then
      Result := SSH_FXF_CREATE_TRUNCATE
    else
      Result := SSH_FXF_OPEN_OR_CREATE;
  end
  else
  if fmCreate in Value then
  begin
    if fmTruncate in Value then
      Result := SSH_FXF_CREATE_TRUNCATE
    else
      Result := SSH_FXF_CREATE_NEW;
  end
  else
  begin
    if fmTruncate in Value then
      Result := SSH_FXF_TRUNCATE_EXISTING
    else
      Result := SSH_FXF_OPEN_EXISTING;
  end;

  if fmAppend in Value then
    Result := Result or SSH_FXF_ACCESS_APPEND_DATA;
  if fmAppendAtomic in Value then
    Result := Result or SSH_FXF_ACCESS_APPEND_DATA_ATOMIC;
  if fmNoFollow in Value then
    Result := Result or SSH_FXF_ACCESS_NOFOLLOW;
  if fmDeleteOnClose in Value then
    Result := Result or SSH_FXF_ACCESS_DELETE_ON_CLOSE;
  if fmText in Value then
    Result := Result or SSH_FXF_ACCESS_TEXT_MODE;
end;

function CardinalToAttributes(Value : Cardinal) : TSBSftpAttributes;
begin
  Result := [];

  if (Value and SSH_FILEXFER_ATTR_SIZE) <> 0 then
    Result := Result +[saSize];

  if (Value and SSH_FILEXFER_ATTR_UIDGID) <> 0 then
    Result := Result +[saUID, saGID];

  if (Value and SSH_FILEXFER_ATTR_PERMISSIONS) <> 0 then
    Result := Result +[saPermissions];

  if (Value and SSH_FILEXFER_ATTR_ACCESSTIME) <> 0 then
    Result := Result +[saATime];

  if (Value and SSH_FILEXFER_ATTR_CREATETIME) <> 0 then
    Result := Result +[saCTime];

  if (Value and SSH_FILEXFER_ATTR_MODIFYTIME) <> 0 then
    Result := Result +[saMTime];

  if (Value and SSH_FILEXFER_ATTR_ACL) <> 0 then
    Result := Result +[saACL];

  if (Value and SSH_FILEXFER_ATTR_OWNERGROUP) <> 0 then
    Result := Result +[saOwner, saGroup];

  if (Value and SSH_FILEXFER_ATTR_SUBSECOND_TIMES) <> 0 then
    Result := Result +[saSubSeconds];

  if (Value and SSH_FILEXFER_ATTR_BITS) <> 0 then
    Result := Result +[saAttribBits];

  if (Value and SSH_FILEXFER_ATTR_ALLOCATION_SIZE) <> 0 then
    Result := Result +[saASize];

  if (Value and SSH_FILEXFER_ATTR_TEXT_HINT) <> 0 then
    Result := Result +[saTextHint];

  if (Value and SSH_FILEXFER_ATTR_MIME_TYPE) <> 0 then
    Result := Result +[saMimeType];

  if (Value and SSH_FILEXFER_ATTR_LINK_COUNT) <> 0 then
    Result := Result +[saLinkCount];

  if (Value and SSH_FILEXFER_ATTR_UNTRANSLATED_NAME) <> 0 then
    Result := Result +[saUName];

  if (Value and SSH_FILEXFER_ATTR_CTIME) <> 0 then
    Result := Result +[saCATime];

  if (Value and SSH_FILEXFER_ATTR_EXTENDED) <> 0 then
    Result := Result +[saExtended];
end;

function AttributesToCardinal(Value : TSBSftpAttributes) : Cardinal;
begin
  Result := 0;

  if saSize in Value then
    Result := Result or SSH_FILEXFER_ATTR_SIZE;

  if (saUID in Value) or (saGID in Value) then
    Result := Result or SSH_FILEXFER_ATTR_UIDGID;

  if saPermissions in Value then
    Result := Result or SSH_FILEXFER_ATTR_PERMISSIONS;

  if saATime in Value then
    Result := Result or SSH_FILEXFER_ATTR_ACCESSTIME;

  if saCTime in Value then
    Result := Result or SSH_FILEXFER_ATTR_CREATETIME;

  if saMTime in Value then
    Result := Result or SSH_FILEXFER_ATTR_MODIFYTIME;

  if saACL in Value then
    Result := Result or SSH_FILEXFER_ATTR_ACL;

  if (saOwner in Value) or (saGroup in Value) then
    Result := Result or SSH_FILEXFER_ATTR_OWNERGROUP;

  if saSubSeconds in Value then
    Result := Result or SSH_FILEXFER_ATTR_SUBSECOND_TIMES;

  if saAttribBits in Value then
    Result := Result or SSH_FILEXFER_ATTR_BITS;

  if saASize in Value then
    Result := Result or SSH_FILEXFER_ATTR_ALLOCATION_SIZE;

  if saTextHint in Value then
    Result := Result or SSH_FILEXFER_ATTR_TEXT_HINT;

  if saMimeType in Value then
    Result := Result or SSH_FILEXFER_ATTR_MIME_TYPE;

  if saLinkCount in Value then
    Result := Result or SSH_FILEXFER_ATTR_LINK_COUNT;

  if saUName in Value then
    Result := Result or SSH_FILEXFER_ATTR_UNTRANSLATED_NAME;

  if saCATime in Value then
    Result := Result or SSH_FILEXFER_ATTR_CTIME;

  if saExtended in Value then
    Result := Result or SSH_FILEXFER_ATTR_EXTENDED;
end;

function RenameFlagsToCardinal(Value : TSBSftpRenameFlags) : Cardinal;
begin
  Result := 0;

  if rfOverwrite in Value then
    Result := Result or SSH_FXF_RENAME_OVERWRITE;
  if rfAtomic in Value then
    Result := Result or SSH_FXF_RENAME_ATOMIC;
  if rfNative in Value then
    Result := Result or SSH_FXF_RENAME_NATIVE;
end;

function CardinalToRenameFlags(Value : Cardinal) : TSBSftpRenameFlags;
begin
  Result := [];
  
  if (Value and SSH_FXF_RENAME_OVERWRITE) <> 0 then
    Result := Result  + [rfOverwrite];
  if (Value and SSH_FXF_RENAME_ATOMIC) <> 0 then
    Result := Result  + [rfAtomic];
  if (Value and SSH_FXF_RENAME_NATIVE) <> 0 then
    Result := Result  + [rfNative]
end;



end.
