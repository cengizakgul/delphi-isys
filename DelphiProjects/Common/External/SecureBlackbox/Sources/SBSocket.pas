(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSocket;

interface

uses
  SBUtils,
  SysUtils,
  Classes,
{$IFNDEF LINUX}
  Windows,
  Winsock;
{$ELSE}
  Libc;
{$ENDIF}




{$ifdef BUILDER_USED}

{$HPPEMIT '#ifdef SD_RECEIVE'}
{$HPPEMIT '#undef SD_RECEIVE'}
{$HPPEMIT '#endif'}

{$HPPEMIT '#ifdef SD_SEND'}
{$HPPEMIT '#undef SD_SEND'}
{$HPPEMIT '#endif'}

{$HPPEMIT '#ifdef SD_BOTH'}
{$HPPEMIT '#undef SD_BOTH'}
{$HPPEMIT '#endif'}

{$HPPEMIT '#define SD_RECEIVE 0'}
{$HPPEMIT '#define SD_SEND 1'}
{$HPPEMIT '#define SD_BOTH 2'}

{$endif}

const
  {$ifdef BUILDER_USED}
  {$externalsym SD_RECEIVE}
  {$endif}
  SD_RECEIVE = 00;
  {$ifdef BUILDER_USED}
  {$externalsym SD_SEND}
  {$endif}
  SD_SEND = 01;
  {$ifdef BUILDER_USED}
  {$externalsym SD_BOTH}
  {$endif}
  SD_BOTH = 02;

{$IFDEF LINUX}
const
  FIONBIO = 5421;
{$ENDIF}

type
  TElSocketState = (issNotASocket, issInitializing, issInitialized, issBound,
    issConnected, issListening, issConnecting);
  TElSocketType = (istStream, istDatagram);
  TElSocksVersion = (elSocks4, elSocks5);
  TElSocksAuthentication = (saNoAuthentication, saUsercode);
  TElWebTunnelAuthentication = (wtaNoAuthentication, wtaRequiresAuthentication);
  EElSocketError = class(Exception);

  TElSocket = class;

  TElSocketSecondaryEvent =  procedure(Sender : TObject;
    Socket : TElSocket; State : integer; var AbortConnect : boolean) of object;

  TElSocket = class
  protected
    FSocket: TSocket;
    FState: TElSocketState;
    FRemoteAddress: string;
    FRemotePort: integer;
    FBoundAddress: string;
    FBoundPort: integer;
    FLocalAddress: string;
    FLocalPort: integer;
    FBuffer: ByteArray;
    FBufStart: integer;
    FBufLen: integer;
    CloseRequest: Boolean;
    FSocksAuthentication: TElSocksAuthentication;
    FSocksPassword: string;
    FSocksPort: Integer;
    FSocksResolveAddress: Boolean;
    FSocksServer: string;
    FSocksUserCode: string;
    FSocksVersion: TElSocksVersion;
    FUseSocks: Boolean;
    //  Web tunneling support
    FUseWebTunneling: boolean;
    FWebTunnelAddress: string;
    FWebTunnelAuthentication: TElWebTunnelAuthentication;
    FWebTunnelPassword: string;
    FWebTunnelPort: Integer;
    FWebTunnelUserId: string;
    procedure FinishAsyncConnect;
    function HTTPConnect(Timeout: integer): Integer;
    procedure ReturnData(Data: Pointer; DataLen: Integer);
    procedure SetAddress(const Value: string);
    procedure SetPort(Value: Integer);
    function SocksConnect(Timeout: integer): Integer;
    procedure SetUseSocks(const Value: Boolean);
    procedure SetUseWebTunneling(const Value: boolean);
    function SocksSendReceive(Timeout: Cardinal; sendBuf: pointer; sendBufSize:
      integer; var wasSent: integer; readBuf: pointer; readBufSize: integer; var
      wasRead: integer): boolean;
    function StartAsyncConnect: Integer;

  public
    constructor Create;
    destructor Destroy; override;

    procedure Close(Forced: boolean);
    function AsyncConnect(Timeout: integer): Integer;
    class procedure FinalizeWinSock;
    function Init(SktType: TElSocketType): Integer;
    class procedure InitializeWinSock;
    function Receive(Data: pointer; DataLen: integer; var Received: integer):
    Integer;
    function Send(Data: pointer; DataLen: integer; var Sent: integer): Integer;
    function CanReceive(WaitTime: integer = 0): Boolean;
    function CanSend(WaitTime: integer = 0): Boolean;
    function Connect(Timeout: integer): Integer;
    function Bind: Integer;
    function Listen: Integer;
    function Accept(Timeout: integer): Integer; overload;
    procedure Accept(Timeout: integer; var Socket : TElSocket); overload;
    function AsyncConnectEx(Timeout: integer; SecondarySocket : TElSocket; SecSend,
      SecRecv : boolean; SecEvent : TElSocketSecondaryEvent): Integer;
    //class procedure SelectSockets(var ReadSockets, WriteSockets : array of TElSocket; Timeout : integer);

    property State: TElSocketState read FState;
  published
    property Address: string read FRemoteAddress write SetAddress;
    property Port: Integer read FRemotePort write SetPort;
    property ListenPort: Integer read FLocalPort write FLocalPort;
    property ListenAddress: string read FLocalAddress write FLocalAddress;
    property BoundPort: Integer read FBoundPort;
    property BoundAddress: string read FBoundAddress;
    property SocksAuthentication: TElSocksAuthentication read FSocksAuthentication
    write FSocksAuthentication;
    property SocksPassword: string read FSocksPassword write FSocksPassword;
    property SocksPort: Integer read FSocksPort write FSocksPort default 1080;
    property SocksResolveAddress: Boolean read FSocksResolveAddress write
    FSocksResolveAddress default false;
    property SocksServer: string read FSocksServer write FSocksServer;
    property SocksUserCode: string read FSocksUserCode write FSocksUserCode;
    property SocksVersion: TElSocksVersion read FSocksVersion write FSocksVersion
 default elSocks5;
    property UseSocks: Boolean read FUseSocks write SetUseSocks default false;
    //  Web tunneling support
    property UseWebTunneling: boolean read FUseWebTunneling write
    SetUseWebTunneling default false;
    property WebTunnelAddress: string read FWebTunnelAddress write
    FWebTunnelAddress;
    property WebTunnelAuthentication: TElWebTunnelAuthentication read
    FWebTunnelAuthentication write FWebTunnelAuthentication
 default wtaNoAuthentication;
    property WebTunnelPassword: string read FWebTunnelPassword write
    FWebTunnelPassword;
    property WebTunnelPort: Integer read FWebTunnelPort write FWebTunnelPort;
    property WebTunnelUserId: string read FWebTunnelUserId write FWebTunnelUserId;
  end;


implementation

const
  DEF_BUFFER_SIZE = 16384;//16384;//65535;//16384;

resourcestring
  sWinsockInitFailed = 'Winsock initialization failed';
  sWrongSocketState = 'Wrong socket state';
  sNotASocket = 'Not a socket';
  sInvalidAddress = 'Invalid address';
  sAcceptFailed = 'Accept failed';

{$IFNDEF LINUX}
const
  SocketTypes: array[TElSocketType] of integer = (SOCK_STREAM, SOCK_DGRAM);
{$ELSE}
const
  SocketTypes: array[TElSocketType] of __socket_type = (SOCK_STREAM,
    SOCK_DGRAM);
{$ENDIF}


{$IFNDEF LINUX}
var
  WinsockInitialized: boolean;
{$ENDIF}

function LastNetError: Integer;
begin
{$IFDEF LINUX}
  Result := errno;
{$ELSE}
  Result := WSAGetLastError();
{$ENDIF}
end;


{
********************************** TElSocket ***********************************
}

constructor TElSocket.Create;
begin
  inherited;
  FSocket := INVALID_SOCKET;
  FState := issNotASocket;
  SetLength(FBuffer, DEF_BUFFER_SIZE);
  FSocksVersion := elSocks5;
end;


destructor TElSocket.Destroy;
begin
  Close(false);
  inherited Destroy;
end;


procedure TElSocket.Close(Forced: boolean);
begin
  if FSocket <> INVALID_SOCKET then
  begin
    if not Forced then
      shutdown(FSocket, SD_BOTH);
{$IFNDEF LINUX}
    closesocket(FSocket);
{$ELSE}
    Libc.__close(FSocket);
{$ENDIF LINUX}
    FSocket := INVALID_SOCKET;
    FState := issNotASocket;
  end
  else
  if FState = issInitializing then
    CloseRequest := true;
end;

function TElSocket.CanReceive(WaitTime: integer = 0): Boolean;
var
  FDSet: TFDSet;
  TimeVal: TTimeVal;
  PTV: PTimeVal;
begin
  if FBufLen > FBufStart then
  begin
    result := true;
    exit;
  end;
  if State <> issConnected then
    raise EElSocketError.Create(sWrongSocketState);
  PTV := @TimeVal;
  if WaitTime = -1 then
    PTV := nil
  else
    if WaitTime = 0 then
  begin
    TimeVal.tv_sec := 0;
    TimeVal.tv_usec := 0;
  end
  else
  begin
    TimeVal.tv_sec := WaitTime div 1000;
    TimeVal.tv_usec := (WaitTime mod 1000) * 1000;
  end;
  FD_ZERO(FDSet);
  FD_SET(FSocket, FDSet);
//  result := select(FSocket + 1, @FDSet, nil, nil, PTV) <> 0;
  result := select(FSocket + 1, @FDSet, nil, nil, PTV) > 0;
end;

function TElSocket.AsyncConnect(Timeout: integer): Integer;
var
  FAddr: TSockAddrIn;
  {$ifndef LINUX}
  FAddrLen: integer;
  {$else}
  FAddrLen: cardinal;
  {$endif}
  addr: LongWord;
  HostEnt: PHostEnt;
  nbs: integer;
  FDSendSet: TFDSet;
  TV: TTimeVal;
  select_res: Integer;
  ErrorCode: integer;
begin
  if FLocalAddress <> '' then
    result := Bind()
  else
    result := 0;
  if result <> 0 then exit;

  if (FSocket = INVALID_SOCKET) and
  (Init(istStream) <> 0) then
    raise EElSocketError.Create(sNotASocket);
  if (State <> issInitialized) and (State <> issBound) then
    raise EElSocketError.Create(sWrongSocketState);
  if Timeout <> 0 then
  begin
    nbs := 1;
{$IFNDEF LINUX}
    ioctlsocket(FSocket, FIONBIO, u_long(nbs));
{$ELSE}
{$ifndef FPC}
    ioctl(FSocket, FIONBIO, u_long(nbs));
{$else}
    ioctl(FSocket, FIONBIO, [1]);
{$endif}
{$ENDIF}
  end;
  addr := inet_addr(PChar(FRemoteAddress));
  if addr = LongWord(INADDR_NONE) then
  begin
    HostEnt := gethostbyname(PChar(FRemoteAddress));
    if HostEnt <> nil then
      Move(HostEnt.h_addr^[0], addr, sizeof(addr));
  end;
  if addr = LongWord(INADDR_NONE) then
    raise EElSocketError.Create(sInvalidAddress);
  FillChar(Faddr, sizeof(FAddr), 0);
  FAddr.sin_family := AF_INET;
  FAddr.sin_addr.S_addr := addr;
  FAddr.sin_port := htons(FRemotePort);
{$IFNDEF LINUX}
  result := winsock.connect(FSocket, FAddr, sizeof(FAddr));
{$ELSE}
  result := Libc.connect(FSocket, FAddr, sizeof(FAddr));
{$ENDIF}
  if Result <> 0 then
  begin
    ErrorCode := LastNetError();
    if Timeout > 0 then
    begin
{$IFNDEF LINUX}
      if (ErrorCode <> WSAEWOULDBLOCK) and (ErrorCode <> WSAEISCONN) and (ErrorCode <> WSAEINPROGRESS) then
{$ELSE}
      if ErrorCode <> EWOULDBLOCK then
{$ENDIF}
      begin
        FState := issInitialized;
        Result := ErrorCode;
      end
{$IFNDEF LINUX}
      else
        if ErrorCode = WSAEISCONN then
        FState := issConnected
      else
        FState := issConnecting;
{$ENDIF}
    end;
  end
  else
    FState := issConnected;
  if FState = issConnecting then
  begin
    FD_ZERO(FDSendSet);
    FD_SET(FSocket, FDSendSet);
    TV.tv_sec := 0;
    TV.tv_usec := Timeout * 1000;
    select_res := select(FSocket + 1, nil, @FDSendSet, nil, @TV);
    if select_res = 1 then
    begin
      FState := issConnected;
      result := 0;
    end
    else
      if select_res = 0 then
    begin
      FState := issInitialized;
{$IFNDEF LINUX}
      result := WSAETIMEDOUT;
{$ELSE}
      result := ETIMEDOUT;
{$ENDIF}
    end
    else
    begin
      FState := issInitialized;
      result := LastNetError;
    end;
  end;

  if FState = issConnected then
  begin
    FAddrLen := sizeof(FAddr);
{$IFNDEF LINUX}
    result := winsock.getsockname(FSocket, FAddr, FAddrLen);
{$ELSE}
    result := Libc.getsockname(FSocket, FAddr, FAddrLen);
{$ENDIF}
//    if result = 0 then
// when getsockname returns an error then FBound* receive just empty values
    FBoundAddress := inet_ntoa(FAddr.sin_addr);
    FBoundPort := ntohs(FAddr.sin_port);
  end;
  if result = 0 then
    FState := issConnected;
end;

function TElSocket.CanSend(WaitTime: integer = 0): Boolean;
var
  FDSet: TFDSet;
  TimeVal: TTimeVal;
  PTV: PTimeVal;
begin
  if FBufLen > FBufStart then
  begin
    result := true;
    exit;
  end;
  if State <> issConnected then
    raise EElSocketError.Create(sWrongSocketState);
  PTV := @TimeVal;
  if WaitTime = -1 then
    PTV := nil
  else
  if WaitTime = 0 then
  begin
    TimeVal.tv_sec := 0;
    TimeVal.tv_usec := 0;
  end
  else
  begin
    TimeVal.tv_sec := WaitTime div 1000;
    TimeVal.tv_usec := (WaitTime mod 1000) * 1000;
  end;
  FD_ZERO(FDSet);
  FD_SET(FSocket, FDSet);
  result := select(FSocket + 1, nil, @FDSet, nil, PTV) <> 0;
end;

function TElSocket.Connect(Timeout: integer): Integer;
begin
  if (FUseSocks) then
    Result := SocksConnect(Timeout)
  else
    if (FUseWebTunneling) then
    Result := HTTPConnect(Timeout)
  else
    Result := AsyncConnect(Timeout);
end;

function TElSocket.Bind: Integer;
var
  FAddr: TSockAddrIn;
  {$ifndef LINUX}
  FAddrLen: integer;
  {$else}
  FAddrLen: cardinal;
  {$endif}
  addr: LongWord;
  HostEnt: PHostEnt;
begin
  if (FSocket = INVALID_SOCKET) and
  (Init(istStream) <> 0) then
    raise EElSocketError.Create(sNotASocket);
  if State <> issInitialized then
    raise EElSocketError.Create(sWrongSocketState);
  FillChar(FAddr, sizeof(FAddr), 0);
  FAddr.sin_family := AF_INET;
  if Length(FLocalAddress) = 0 then
    FAddr.sin_addr.S_addr := LongWord(INADDR_ANY)
  else
  begin
    addr := inet_addr(PChar(FLocalAddress));

    if addr = LongWord(INADDR_NONE) then
    begin
      HostEnt := gethostbyname(PChar(FLocalAddress));
      if HostEnt <> nil then
        Move(HostEnt.h_addr^[0], addr, sizeof(addr));
    end;
    if addr = LongWord(INADDR_NONE) then
      raise EElSocketError.Create(sInvalidAddress);

    FAddr.sin_addr.S_addr := addr;
  end;
  FAddr.sin_port := htons(FLocalPort);
{$IFNDEF LINUX}
  result := winsock.bind(FSocket, FAddr, sizeof(FAddr));
{$ELSE}
  result := Libc.bind(FSocket, FAddr, sizeof(FAddr));
{$ENDIF}
  if result <> 0 then
    result := LastNetError()
  else
  begin
    FAddrLen := sizeof(FAddr);
{$IFNDEF LINUX}
    result := winsock.getsockname(FSocket, FAddr, FAddrLen);
{$ELSE}
    result := Libc.getsockname(FSocket, FAddr, FAddrLen);
{$ENDIF}
    if result <> 0 then
      result := LastNetError()
    else
    begin
      FBoundAddress := inet_ntoa(FAddr.sin_addr);
      FBoundPort := ntohs(FAddr.sin_port);
      result := 0;
    end;
  end;
  if result = 0 then
    FState := issBound;
end;

function TElSocket.Listen: Integer;
begin
  if (FSocket = INVALID_SOCKET) and
  (Init(istStream) <> 0) then
    raise EElSocketError.Create(sNotASocket);
  if State <> issBound then
    raise EElSocketError.Create(sWrongSocketState);
{$IFNDEF LINUX}
  result := winsock.listen(FSocket, 1);
{$ELSE}
  result := Libc.listen(FSocket, 1);
{$ENDIF}
  if result <> 0 then
    result := LastNetError()
  else
    FState := issListening;
end;

function TElSocket.Accept(Timeout: integer): Integer;
var
  FAddr: TSockAddrIn;
  {$ifndef LINUX}
  FAddrLen: integer;
  {$else}
  FAddrLen: cardinal;
  {$endif}
  nbs: integer;
  FDRecvSet: TFDSet;
  TV: TTimeVal;
  select_res: Integer;
begin
  result := -1;
  if (FSocket = INVALID_SOCKET) and
  (Init(istStream) <> 0) then
    raise EElSocketError.Create(sNotASocket);
  if State <> issListening then
    raise EElSocketError.Create(sWrongSocketState);
  if FState = issListening then
  begin
    if Timeout <> 0 then
    begin
      nbs := 1;
{$IFNDEF LINUX}
      ioctlsocket(FSocket, FIONBIO, u_long(nbs));
{$ELSE}
{$ifndef FPC}
    ioctl(FSocket, FIONBIO, u_long(nbs));
{$else}
    ioctl(FSocket, FIONBIO, [1]);
{$endif}
{$ENDIF}
    end;
    FD_ZERO(FDRecvSet);
    FD_SET(FSocket, FDRecvSet);
    TV.tv_sec := 0;
    TV.tv_usec := Timeout * 1000;
    select_res := select(FSocket + 1, @FDRecvSet, nil, nil, @TV);
    if select_res = 1 then
    begin
      FillChar(FAddr, sizeof(FAddr), 0);
      FAddr.sin_family := AF_INET;
      FAddrLen := sizeof(FAddr);
{$IFNDEF LINUX}
      result := winsock.accept(FSocket, @FAddr, @FAddrLen);
{$ELSE}
      result := Libc.accept(FSocket, @FAddr, @FAddrLen);
{$ENDIF}
      if result < 0 then
      begin
        FState := issBound;
        result := LastNetError();
      end
      else
      begin
        FRemoteAddress := inet_ntoa(FAddr.sin_addr);
        FRemotePort := ntohs(FAddr.sin_port);
{$IFNDEF LINUX}
        winsock.closesocket(FSocket);
{$ELSE}
        Libc.__close(FSocket);
{$ENDIF}
        FSocket := result;
        FState := issConnected;

        // get bound address
        FAddrLen := sizeof(FAddr);
{$IFNDEF LINUX}
        result := winsock.getsockname(FSocket, FAddr, FAddrLen);
{$ELSE}
        result := Libc.getsockname(FSocket, FAddr, FAddrLen);
{$ENDIF}
        if result <> 0 then
          result := LastNetError()
        else
        begin
          FBoundAddress := inet_ntoa(FAddr.sin_addr);
          FBoundPort := ntohs(FAddr.sin_port);
          result := 0;
        end;
      end;
    end
    else
      if select_res = 0 then
    begin
      FState := issBound;
{$IFNDEF LINUX}
      result := WSAETIMEDOUT;
{$ELSE}
      result := ETIMEDOUT;
{$ENDIF}
    end
    else
    begin
      FState := issBound;
      result := LastNetError();
    end;
  end;
  if result = 0 then
    FState := issConnected;
end;

procedure TElSocket.Accept(Timeout: integer; var Socket : TElSocket);
var
  FAddr: TSockAddrIn;
  {$ifndef LINUX}
  FAddrLen: integer;
  {$else}
  FAddrLen: cardinal;
  {$endif}
  nbs: integer;
  FDRecvSet: TFDSet;
  TV: TTimeVal;
  select_res: Integer;
  result : integer;
begin
  Socket := nil;
  if (FSocket = INVALID_SOCKET) and
  (Init(istStream) <> 0) then
    raise EElSocketError.Create(sNotASocket);
  if State <> issListening then
    raise EElSocketError.Create(sWrongSocketState);
  if FState = issListening then
  begin
    if Timeout <> 0 then
    begin
      nbs := 1;
{$IFNDEF LINUX}
      ioctlsocket(FSocket, FIONBIO, u_long(nbs));
{$ELSE}
      {$ifndef FPC}
      ioctl(FSocket, FIONBIO, u_long(nbs));
      {$else}
      ioctl(FSocket, FIONBIO, [1]);
      {$endif}
{$ENDIF}
    end;
    if Timeout > 0 then
    begin
      FD_ZERO(FDRecvSet);
      FD_SET(FSocket, FDRecvSet);
      TV.tv_sec := 0;
      TV.tv_usec := Timeout * 1000;
      select_res := select(FSocket + 1, @FDRecvSet, nil, nil, @TV);
    end
    else
      select_res := 1;
    if select_res = 1 then
    begin
      FillChar(FAddr, sizeof(FAddr), 0);
      FAddr.sin_family := AF_INET;
      FAddrLen := sizeof(FAddr);
{$IFNDEF LINUX}
      result := winsock.accept(FSocket, @FAddr, @FAddrLen);
{$ELSE}
      result := Libc.accept(FSocket, @FAddr, @FAddrLen);
{$ENDIF}
      if result > 0 then
      begin
        Socket := TElSocket.Create;
        Socket.FState := issConnected;
        Socket.FSocket := result;
        Socket.FRemoteAddress := inet_ntoa(FAddr.sin_addr);
        Socket.FRemotePort := ntohs(FAddr.sin_port);

        // get bound address
        FAddrLen := sizeof(FAddr);
{$IFNDEF LINUX}
        result := winsock.getsockname(Socket.FSocket, FAddr, FAddrLen);
{$ELSE}
        result := Libc.getsockname(Socket.FSocket, FAddr, FAddrLen);
{$ENDIF}
        if result = 0 then
        begin
          Socket.FBoundAddress := inet_ntoa(FAddr.sin_addr);
          Socket.FBoundPort := ntohs(FAddr.sin_port);
        end;

      end;
    end
    else
    if select_res = 0 then
    begin
      ; // doing nothing
    end
    else
      raise EElSocketError.Create(sAcceptFailed);
  end;
end;


function TElSocket.StartAsyncConnect: Integer;
var
  FAddr: TSockAddrIn;
  addr: LongWord;
  HostEnt: PHostEnt;
  nbs: integer;
  //FDSendSet: TFDSet;
  //TV: TTimeVal;
  //select_res: Integer;
  ErrorCode: integer;

begin
  //Result := 0;

  if (FSocket = INVALID_SOCKET) and
  (Init(istStream) <> 0) then
    raise EElSocketError.Create(sNotASocket);
  if State <> issInitialized then
    raise EElSocketError.Create(sWrongSocketState);

  nbs := 1;
  {$IFNDEF LINUX}
  ioctlsocket(FSocket, FIONBIO, u_long(nbs));
  {$ELSE}
{$ifndef FPC}
  ioctl(FSocket, FIONBIO, u_long(nbs));
{$else}
  ioctl(FSocket, FIONBIO, [1]);
{$endif}
  {$ENDIF LINUX}

  addr := inet_addr(PChar(FRemoteAddress));
  if addr = LongWord(INADDR_NONE) then
  begin
    HostEnt := gethostbyname(PChar(FRemoteAddress));
    if HostEnt <> nil then
      Move(HostEnt.h_addr^[0], addr, sizeof(addr));
  end;
  if addr = LongWord(INADDR_NONE) then
    raise EElSocketError.Create(sInvalidAddress);
  FillChar(Faddr, sizeof(FAddr), 0);
  FAddr.sin_family := AF_INET;
  FAddr.sin_addr.S_addr := addr;
  FAddr.sin_port := htons(FRemotePort);
  {$IFNDEF LINUX}
  result := winsock.connect(FSocket, FAddr, sizeof(FAddr));
  {$ELSE}
  result := Libc.connect(FSocket, FAddr, sizeof(FAddr));
  {$ENDIF LINUX}
  if Result <> 0 then
  begin
    ErrorCode := LastNetError;
{$IFNDEF LINUX}
    if (ErrorCode <> WSAEWOULDBLOCK) and (ErrorCode <> WSAEISCONN) and (ErrorCode <> WSAEINPROGRESS) then
{$ELSE}
    if ErrorCode <> EWOULDBLOCK then
{$ENDIF LINUX}
    begin
      FState := issInitialized;
      Result := ErrorCode;
    end
{$IFNDEF LINUX}
    else
    if ErrorCode = WSAEISCONN then
      FState := issConnected
    else
      FState := issConnecting;
{$ENDIF LINUX}
  end
  else
    FState := issConnected;

  if FState = issConnected then
    FinishAsyncConnect()
  else
  if FState <> issConnecting then
    Result := LastNetError;
end;

procedure TElSocket.FinishAsyncConnect;
var
  FAddr: TSockAddrIn;
  {$ifndef LINUX}
  FAddrLen: integer;
  {$else}
  FAddrLen: cardinal;
  {$endif}
//  addr: LongWord;
begin
  FState := issConnected;
  FAddrLen := sizeof(FAddr);
{$IFNDEF LINUX}
  winsock.getsockname(FSocket, FAddr, FAddrLen);
{$ELSE}
  Libc.getsockname(FSocket, FAddr, FAddrLen);
{$ENDIF}
// if result = 0 then
// when getsockname returns an error then FBound* receive just empty values
  FBoundAddress := inet_ntoa(FAddr.sin_addr);
  FBoundPort := ntohs(FAddr.sin_port);
end;

function TElSocket.AsyncConnectEx(Timeout: integer; SecondarySocket : TElSocket;
    SecSend, SecRecv : boolean; SecEvent : TElSocketSecondaryEvent): Integer;
var
  Cancel : boolean;
  Dir    : integer;

  TV: TTimeVal;
  HighSocketHandle: Integer;
  select_res: Integer;
  FDErrSet,
  FDRecvSet,
  FDSendSet: TFDSet;
  Elapsed  : integer;
  StartTime: TDateTime;


begin
  if FLocalAddress <> '' then
    result := Bind()
  else
    result := 0;
  if result <> 0 then exit;

  StartAsyncConnect();


  Elapsed := 0;

  while (State = issConnecting) and
    ((Elapsed < Timeout) or (Timeout = 0)) do
  begin
    if (SecondarySocket <> nil) and (SecondarySocket.FSocket > FSocket) then
      HighSocketHandle := SecondarySocket.FSocket
    else
      HighSocketHandle := FSocket;

    FD_ZERO(FDSendSet);
    FD_SET(FSocket, FDSendSet);
    if SecSend then
      FD_SET(SecondarySocket.FSocket, FDSendSet);

    FD_ZERO(FDRecvSet);
    if SecRecv then
      FD_SET(SecondarySocket.FSocket, FDRecvSet);

    FD_ZERO(FDErrSet);
    FD_SET(FSocket, FDErrSet);
    FD_SET(SecondarySocket.FSocket, FDErrSet);

    TV.tv_sec := 0;
    TV.tv_usec := (Timeout - Elapsed) * 1000;


    StartTime := Now;

    select_res := select(HighSocketHandle + 1, @FDRecvSet, @FDSendSet, @FDErrSet, @TV);

    if select_res > 0 then
    begin
      Cancel := false;

      if FD_ISSET(FSocket, FDErrSet) then
      begin
        Close(true);
        result := LastNetError;
        exit;
      end;
      if (FD_ISSET(SecondarySocket.FSocket, FDRecvSet) or FD_ISSET(SecondarySocket.FSocket, FDRecvSet)) then
      begin
        Dir := 0;
        if FD_ISSET(SecondarySocket.FSocket, FDRecvSet) then
          Dir := SD_RECEIVE;
        if FD_ISSET(SecondarySocket.FSocket, FDSendSet) then
        begin
          if Dir = SD_RECEIVE then
            Dir := SD_BOTH
          else
            Dir := SD_SEND;
        end;
        Cancel := false;
        if Assigned(SecEvent) then
          SecEvent(Self, SecondarySocket, Dir, Cancel);
      end;

      if FD_ISSET(FSocket, FDSendSet) then
      begin
        FinishAsyncConnect();
        break;
      end;

      if Cancel then
      begin
        Close(true);
        result := -1;
        exit;
      end;
    end
    else
    if select_res = SOCKET_ERROR then
    begin
      FState := issInitialized;
      result := LastNetError;
      exit;
    end;

    Elapsed := Elapsed + Trunc((Now() - StartTime) * 86400 * 1000 + 0.5);
  end;

  if State <> issConnected then
  begin
    FState := issInitialized;
{$IFNDEF LINUX}
    result := WSAETIMEDOUT;
{$ELSE}
    result := ETIMEDOUT;
{$ENDIF}
  end
  else
    result := 0;
end;

class procedure TElSocket.FinalizeWinSock;
begin
{$IFNDEF LINUX}
  if WinsockInitialized then
    WSACleanup();
{$ENDIF}
end;

function TElSocket.HTTPConnect(Timeout: integer): Integer;
var
  len: integer;
  buf: BufferType;
  rAddr: string;
  rPort: word;
  BasicCredentials: string;
  BasicCredentialsEnc: BufferType;
  HttpRequest: BufferType;
  HTTPResponse: BufferType;
  psize, rcvcnt, cnt: integer;
  httpMajor, httpMinor, httpCode: integer;
begin

  if FLocalAddress <> '' then
    result := Bind()
  else
    result := 0;
  if result <> 0 then exit;

  len := 1024;
  result := 0;
  rAddr := FRemoteAddress;
  rPort := FRemotePort;
  FRemoteAddress := (FWebTunnelAddress);
  FRemotePort := (FWebTunnelPort);
  AsyncConnect(Timeout);
  SetLength(buf, len);
  try
    if (FState <> issConnected) then
      result := -1
    else
    begin
      if ((wtaRequiresAuthentication = FWebTunnelAuthentication) and
        (FWebTunnelUserId <> '') and (FWebTunnelPassword <> '')) then
      begin
        BasicCredentials := Format('%s:%s', [FWebTunnelUserId, FWebTunnelPassword]);
        len := 0;
        SBUtils.Base64Encode(PChar(BasicCredentials), length(BasicCredentials), nil, len, false);
        SetLength(BasicCredentialsEnc, len);
        if len > 0 then
        begin
          SBUtils.Base64Encode(PChar(BasicCredentials), length(BasicCredentials), @BasicCredentialsEnc[BufferTypeStartOffset], len, false);
          SetLength(BasicCredentialsEnc, len);
        end;
        HttpRequest :=
          Format('CONNECT %s:%d HTTP/1.1'#13#10'User-agent: SecureBlackbox'#13#10'Proxy-Authorization: Basic %s'#13#10#13#10,
          [rAddr, rPort, BasicCredentialsEnc]);
      end
      else
      begin
        HttpRequest := Format('CONNECT %s:%d HTTP/1.1'#13#10'User-agent: SecureBlackbox'#13#10#13#10, [rAddr, rPort]);
      end;
      psize := length(HttpRequest);
      Move(HttpRequest[1], buf[1], psize);
      cnt := 0;
      rcvcnt := 0;
      if (not SocksSendReceive(Timeout, @buf[BufferTypeStartOffset], psize, cnt, @buf[BufferTypeStartOffset], 1024, rcvcnt)) then
        result := -1;
      if cnt < 12 then
        result := -1;
      if result = -1 then
        exit;
      httpMajor := StrToIntDef(Copy(buf, 6, 1), 0);
      httpMinor := StrToIntDef(Copy(buf, 8, 1), 0);
      httpCode := StrToIntDef(Copy(buf, 10, 3), 0);
      if (1 = httpMajor) and ((0 = httpMinor) or (1 = httpMinor)) and (httpCode = 200) then
        result := 0
      else
        result := -1;
      if result = 0 then
      begin
        SetLength(HTTPResponse, rcvcnt);
        Move(buf[1], HTTPResponse[1], rcvcnt);
        cnt := Pos(#13#10#13#10, HTTPResponse);
        if (cnt > 0) and (cnt < rcvcnt - 4) then
        begin
          ReturnData(@Buf[cnt + 4], rcvcnt - (cnt + 4));
        end;
      end;
      result := 0;
    end;
  finally
    if (FState = issConnected) and (result <> 0) then
      Close(true);
  end;
end;

function TElSocket.Init(SktType: TElSocketType): Integer;
begin
  if (FState <> issNotASocket) and (FState <> issInitialized) then
    raise EElSocketError.Create(sWrongSocketState);
  if FState = issNotASocket then
  begin
    FState := issInitializing;
  {$IFNDEF LINUX}
    FSocket := winsock.socket(PF_INET, SocketTypes[SktType], IPPROTO_IP);
  {$ELSE}
    FSocket := Libc.socket(PF_INET, SocketTypes[SktType], IPPROTO_IP);
  {$ENDIF}
    if FSocket = INVALID_SOCKET then
  {$IFNDEF LINUX}
      result := WSAGetLastError
  {$ELSE}
      result := errno
  {$ENDIF}
    else
      result := 0;
  end
  else
    result := 0;

  if CloseRequest then
  begin
    CloseRequest := false;
    FState := issNotASocket;
    if result = 0 then
{$IFNDEF LINUX}
      closesocket(FSocket);
{$ELSE}
      Libc.__close(FSocket);
{$ENDIF}
    FSocket := INVALID_SOCKET;
    exit;
  end;
  if result = 0 then
    FState := issInitialized;
end;

class procedure TElSocket.InitializeWinSock;
{$IFNDEF LINUX}
var
  lpData: WSADATA;
{$ENDIF}
begin
{$IFNDEF LINUX}
  if WSAStartup(MakeWord(1, 1), lpData) <> 0 then
    raise EElSocketError.Create(sWinsockInitFailed);
  WinsockInitialized := true;
{$ENDIF}
end;

function TElSocket.Receive(Data: pointer; DataLen: integer; var Received: integer): Integer;
begin
  result := SOCKET_ERROR;
  if State <> issConnected then
    raise EElSocketError.Create(sWrongSocketState);
  if FBufLen > FBufStart then
  begin
    result := 0;
    if (FBufLen - FBufStart) > DataLen then
      Received := DataLen
    else
      Received := FBufLen - FBufStart;
    Move(FBuffer[FBufStart], Data^, Received);
    if Received < (FBufLen - FBufStart) then
    begin
      Inc(FBufStart, Received);
    end
    else
    begin
      FBufStart := 0;
      FBufLen := 0;
    end;
  end
  else
  begin
    try
      if DataLen > Length(FBuffer) then
        result := recv(FSocket, PChar(Data)^, DataLen, 0)
      else
      begin
        result := recv(FSocket, FBuffer[0], Length(FBuffer), 0);
        if Result <> SOCKET_ERROR then
        begin
          FBufLen := result;
          if FBufLen > 0 then
            Receive(Data, DataLen, Received)
          else
            Received := 0;
          result := 0;
          exit;
        end;
      end;
    except
    end;
    if result = SOCKET_ERROR then
    begin
      Received := 0;
      Result := LastNetError;
    end
    else
    begin
      Received := result;
      result := 0;
    end;
  end;
end;

procedure TElSocket.ReturnData(Data: Pointer; DataLen: Integer);
begin
  SetLength(FBuffer, DataLen + (FBufLen - FBufStart));
  Move(FBuffer[FBufStart], FBuffer[DataLen], FBufLen - FBufStart);
  Move(Data^, FBuffer[0], DataLen);
  FBufStart := 0;
  FBufLen := Length(FBuffer);
end;

(*
class procedure TElSocket.SelectSockets(var ReadSockets, WriteSockets : array of TElSocket; Timeout : integer);
var
  i : integer;
{$IFNDEF DELPHI_NET}
  TV: TTimeVal;
  HighSocketHandle: Integer;
{$ELSE}
  //RemoteIP: IPAddress;
  //ErrorCode: integer;
{$ENDIF}
  select_res: Integer;
  FDErrSet,
  FDRecvSet,
  FDSendSet: {$ifndef DELPHI_NET}TFDSet{$else}ArrayList{$endif};
  Elapsed  : integer;
  StartTime: TDateTime;

  {$ifdef DELPHI_NET}
  function FD_ISSET(ASocket : Socket; ASet : ArrayList) : Boolean;
  begin
    result := ASet.Contains(ASocket);
  end;

  procedure FD_SET(ASocket : Socket; ASet : ArrayList);
  begin
    ASet.Add(ASocket);
  end;
  {$endif}

begin
  {$ifndef DELPHI_NET}
  HighSocketHandle := -1;

  FD_ZERO(FDErrSet);
  FD_ZERO(FDRecvSet);
  FD_ZERO(FDSendSet);

  {$else}

  FDRecvSet := ArrayList.Create;
  FDSendSet := ArrayList.Create;
  FDErrSet := ArrayList.Create;
  {$endif}

  for i := 0 to Length(ReadSockets) do
  begin
    FD_SET(ReadSockets[i].FSocket, FDRecvSet);
    FD_SET(ReadSockets[i].FSocket, FDErrSet);

    if HighSocketHandle < ReadSockets[i].FSocket then
      HighSocketHandle := ReadSockets[i].FSocket;
  end;

  for i := 0 to Length(WriteSockets) do
  begin
    FD_SET(WriteSockets[i].FSocket, FDSendSet);
    FD_SET(WriteSockets[i].FSocket, FDErrSet);

    if HighSocketHandle < WriteSockets[i].FSocket then
      HighSocketHandle := WriteSockets[i].FSocket;
  end;

  TV.tv_sec := 0;
  TV.tv_usec := Timeout * 1000;

  {$ifndef DELPHI_NET}
  select_res := select(HighSocketHandle + 1, @FDRecvSet, @FDSendSet, @FDErrSet, @TV);
  {$else}
  select_res := -1;
  try
    System.Net.Sockets.Socket.Select(FDRecvSet, FDSendSet, FDErrSet, Timeout - Elapsed);
    select_res := FDRecvSet.Count + FDSendSet.Count + FDErrSet.Count;
  except
    on E : SocketException do
    begin
      LastNetError := E.ErrorCode;
      select_res := SOCKET_ERROR;
    end;
    on E : Exception do
      select_res := SOCKET_ERROR;
  end;
  {$endif}

  if select_res > 0 then
  begin
    
  end;

end;

*)

function TElSocket.Send(Data: pointer; DataLen: integer; var Sent: integer):
Integer;
begin
  if State <> issConnected then
    raise EElSocketError.Create(sWrongSocketState);
{$IFNDEF LINUX}
  result := Winsock.Send(FSocket, PChar(Data)^, DataLen, 0);
{$ELSE}
  result := Libc.Send(FSocket, PChar(Data)^, DataLen, 0);
{$ENDIF LINUX}
  if result = SOCKET_ERROR then
  begin
    Sent := 0;
    Result := LastNetError;
  end
  else
  begin
    Sent := result;
    result := 0;
  end;
end;

procedure TElSocket.SetAddress(const Value: string);
begin
  if not (FState in [issNotASocket, issInitialized]) then
    raise EElSocketError.Create(sWrongSocketState);
  FRemoteAddress := Value;
end;

procedure TElSocket.SetPort(Value: Integer);
begin
  if not (FState in [issNotASocket, issInitialized]) then
    raise EElSocketError.Create(sWrongSocketState);
  FRemotePort := Value;
end;

procedure TElSocket.SetUseSocks(const Value: Boolean);
begin
  FUseSocks := Value;
  if Value then
    UseWebTunneling := false;
end;

procedure TElSocket.SetUseWebTunneling(const Value: boolean);
begin
  FUseWebTunneling := Value;
  if Value then
    UseSocks := false;
end;

function TElSocket.SocksConnect(Timeout: integer): Integer;
var
  rAddr: BufferType;
  rPrt: Word;
  buf: BufferType;
  rIP: in_addr;
  HostEnt: PHostent;
  cnt, len, psize, SelectRes: integer;
  realDataLen: integer;
(*
const
  AuthList: array [0..3] of byte = (5, // Socks version
                                    2, // NMETHODS
                                    0, // NO AUTHENTICATION REQUIRED
                                    2);// USERNAME/PASSWORD
*)
begin
  if FLocalAddress <> '' then
    result := Bind()
  else
    result := 0;
  if result <> 0 then exit;

  Result := -1;
  rAddr := FRemoteAddress;
  rPrt := FRemotePort;
  FRemoteAddress := FSocksServer;
  FRemotePort := FSocksPort;
  AsyncConnect(Timeout);
  if State = issConnected then
  begin
    len := 1024;
    SetLength(buf, len);
    try
      if FSocksVersion = elSocks5 then
      begin
        psize := 4;
        case FSocksAuthentication of
          saUsercode:
            begin
              PCardinal(@buf[BufferTypeStartOffset])^ := $02000205;
            end;
          saNoAuthentication:
            begin
              PCardinal(@buf[BufferTypeStartOffset])^ := $00000105;
              psize := 3;
            end;
        end;
        if not SocksSendReceive(Timeout, @buf[BufferTypeStartOffset], psize, cnt, @buf[BufferTypeStartOffset], 1024, cnt) then
          exit;
        if (Byte(buf[BufferTypeStartOffset]) = 05) and (Byte(buf[BufferTypeStartOffset + 1]) = $FF) then
          exit;
        if (Byte(buf[BufferTypeStartOffset]) = 05) and (Byte(buf[BufferTypeStartOffset + 1]) = 02) then
        begin
          cnt := Length(FSocksUserCode);
          buf[BufferTypeStartOffset] := BufferElementType(1);
          buf[BufferTypeStartOffset + 1] := BufferElementType(cnt);
          if cnt > 0 then
            Move(FSocksUserCode[1], buf[BufferTypeStartOffset + 2], cnt);
          psize := Length(FSocksPassword);
          buf[BufferTypeStartOffset + cnt + 2] := BufferElementType(psize);
          if psize > 0 then
            Move(FSocksPassword[1], buf[BufferTypeStartOffset + cnt + 3], psize);
          psize := psize + cnt + 3;
          if not SocksSendReceive(Timeout, @buf[BufferTypeStartOffset], psize, cnt, @buf[BufferTypeStartOffset], 1024, cnt) then
            exit;
          if buf[BufferTypeStartOffset + 1] <> BufferElementType(0) then
            exit;
        end;
        psize := 8;
        rIP.S_addr := inet_addr(PChar(rAddr));
        if rIP.S_addr = u_long(INADDR_NONE) then
        begin
          if FSocksResolveAddress then
          begin
            psize := length(rAddr);
            Move(rAddr[BufferTypeStartOffset], buf[BufferTypeStartOffset + 5], psize);
            buf[BufferTypeStartOffset + 4] := BufferElementType(psize);
            buf[BufferTypeStartOffset + 2] := BufferElementType(0);
            buf[BufferTypeStartOffset + 3] := BufferElementType(3);
            psize := 5 + psize;
          end
          else
          begin
            HostEnt := gethostbyname(PChar(rAddr));
            if HostEnt <> nil then
              Move(HostEnt.h_addr^[0], buf[BufferTypeStartOffset + 4], sizeof(rIP.S_addr))
            else
              exit;
            buf[BufferTypeStartOffset + 2] := BufferElementType(0);
            buf[BufferTypeStartOffset + 3] := BufferElementType(1);
            // 01 - IP V4 address, 04 - IP V6 address
          end;
        end
        else
        begin
          Move(rIP.S_addr, buf[BufferTypeStartOffset + 4], sizeof(rIP.S_addr));
          buf[BufferTypeStartOffset + 2] := BufferElementType(0);
          buf[BufferTypeStartOffset + 3] := BufferElementType(1);
            // 01 - IP V4 address, 04 - IP V6 address
        end;
        buf[BufferTypeStartOffset + 0] := BufferElementType(5);
        buf[BufferTypeStartOffset + 1] := BufferElementType(1);
        //PWord(@buf[BufferTypeStartOffset + psize])^ := htons(rPrt);
        buf[BufferTypeStartOffset + psize] := BufferElementType((rPrt shr 8) and $FF);
        buf[BufferTypeStartOffset + psize + 1] := BufferElementType(rPrt and $FF);
        psize := psize + 2;
        if not SocksSendReceive(Timeout, @buf[BufferTypeStartOffset], psize, cnt, @buf[BufferTypeStartOffset], 1024, cnt) then
          exit;
        SelectRes := 0;
        Result := Byte(buf[BufferTypeStartOffset + 1]);
        if SelectRes <> 0 then
        begin
          result := SelectRes;
          exit;
        end;
        if Result = -1 then
          exit;
        case Byte(buf[BufferTypeStartOffset + 3]) of
          1: realDataLen := 4 + 4 + 2;
          3: realDataLen := 4 + (1 + Byte(buf[BufferTypeStartOffset + 4])) + 2;
          4: realDataLen := 4 + 16 + 2;
        else
          realDataLen := -1;
        end;
        if (realDataLen = -1) then
          exit;
        if (realDataLen < cnt) then
          returnData(@buf[realDataLen + 1], cnt - realDataLen);
      end
      else
      begin
        buf[BufferTypeStartOffset] := BufferElementType(04);
        buf[BufferTypeStartOffset + 1] := BufferElementType(01);
        buf[BufferTypeStartOffset + 2] := BufferElementType((rPrt shr 8) and $FF);
        buf[BufferTypeStartOffset + 3 + 1] := BufferElementType(rPrt and $FF);
        cnt := Length(FSocksUserCode);
        if cnt > 0 then
          Move(FSocksUserCode[BufferTypeStartOffset], buf[BufferTypeStartOffset + 8], cnt);
        psize := cnt + 8 + 1;
        buf[BufferTypeStartOffset + psize - 1] := BufferElementType(0);
        rIP.S_addr := inet_addr(PChar(rAddr));
        if rIP.S_addr = u_long(INADDR_NONE) then
        begin
          if FSocksResolveAddress then
          begin
            cnt := length(rAddr);
            PCardinal(@buf[BufferTypeStartOffset + 4])^ := $FF000000;
            Move(rAddr[BufferTypeStartOffset], PByte(@buf[BufferTypeStartOffset + psize])^, cnt);
            psize := psize + cnt + 1;
            buf[BufferTypeStartOffset + psize - 1] := BufferElementType(0);
          end
          else
          begin
            HostEnt := gethostbyname(PChar(rAddr));
            if HostEnt <> nil then
              Move(HostEnt.h_addr^[0], PByte(@buf[BufferTypeStartOffset + 4])^,
                sizeof(rIP.S_addr))
            else
              exit;
          end;
        end
        else
          Move(rIP.S_addr, buf[BufferTypeStartOffset + 4], sizeof(rIP.S_addr));
        if not SocksSendReceive(Timeout, @buf[BufferTypeStartOffset], psize, cnt, @buf[BufferTypeStartOffset], 1024, cnt) then
          exit;
        if buf[BufferTypeStartOffset + 1] = BufferElementType(90) then
          Result := 0 // request granted
        else
          exit;
        if result = 0 then
        begin
          if cnt > 8 then
          begin
            returnData(@buf[BufferTypeStartOffset + 8], cnt - 8);
          end;
        end;
      end;
    finally
      if (Result <> 0) and (State = issConnected) then
        Close(false);
    end;
  end;
end;

function TElSocket.SocksSendReceive(Timeout: Cardinal; sendBuf: pointer;
  sendBufSize: integer; var wasSent: integer; readBuf: pointer; readBufSize:
  integer; var wasRead: integer): boolean;
var
  FDSendSet, FDRecvSet: TFDSet;
  PTV: PTimeVal;
  TimeVal: TTimeVal;
  TimeoutV, SelectRes: Integer;
  Elapsed,
    StartTime: Cardinal;
begin
  StartTime := GetTickCount();
  while (true) do
  begin
    Elapsed := TickDiff(StartTime, GetTickCount);
    if (Timeout > 0) and
      ((Elapsed) >= Timeout) then
    begin
      result := false;
      exit;
    end;
    FD_ZERO(FDRecvSet);
    FD_ZERO(FDSendSet);
    if Timeout > 0 then
      TimeoutV := Timeout - Elapsed
    else
      TimeoutV := 0;
    if TimeoutV > 0 then
    begin
      TimeVal.tv_sec := TimeoutV div 1000;
      TimeVal.tv_usec := (TimeoutV mod 1000) * 1000;
      PTV := @TimeVal;
    end
    else
      PTV := nil;
    FD_ZERO(FDSendSet);
    FD_SET(FSocket, FDSendSet);
    SelectRes := select(FSocket + 1, nil, @FDSendSet, nil, PTV);
    if (0 <> SelectRes) and (SOCKET_ERROR <> SelectRes) then
    begin
      if FD_ISSET(FSocket, FDSendSet) then
      begin
        if SOCKET_ERROR = Send(sendBuf, sendBufSize, wasSent) then
        begin
          result := false;
          exit;
        end
        else
          break;
      end;
    end
    else
      if SelectRes = SOCKET_ERROR then
    begin
      result := false;
      exit;
    end;
  end;
  while (true) do
  begin
    Elapsed := GetTickCount - StartTime;
    if (Timeout > 0) and
      ((Elapsed) >= Timeout) then
    begin
      result := false;
      exit;
    end;
    FD_ZERO(FDRecvSet);
    if Timeout > 0 then
      TimeoutV := Timeout - Elapsed
    else
      TimeoutV := 0;
    if TimeoutV > 0 then
    begin
      TimeVal.tv_sec := TimeoutV div 1000;
      TimeVal.tv_usec := (TimeoutV mod 1000) * 1000;
      PTV := @TimeVal;
    end
    else
      PTV := nil;
    FD_SET(FSocket, FDRecvSet);
    SelectRes := select(FSocket + 1, @FDRecvSet, nil, nil, PTV);

    if (0 <> SelectRes) and (SOCKET_ERROR <> SelectRes) then
    begin
      if FD_ISSET(FSocket, FDRecvSet) then
      begin
        if SOCKET_ERROR = Receive(readBuf, readBufSize, wasRead) then
        begin
          result := false;
          exit
        end
        else
        begin
          Result := true;
          exit;
        end;
      end;
    end
    else
    if SelectRes = SOCKET_ERROR then
    begin
      result := false;
      exit;
    end;
  end;
end;

initialization
{$IFNDEF LINUX}
  WinsockInitialized := false;
{$ENDIF}
  TElSocket.InitializeWinsock;
finalization
  TElSocket.FinalizeWinsock;

end.


