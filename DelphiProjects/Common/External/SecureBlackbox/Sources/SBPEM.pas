
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBPEM;

interface

uses
  SysUtils,
  Classes,
  {$ifndef CLX_USED}
  Windows,
  {$else}
  Libc,
  {$endif}
  SBUtils;


const
  PEM_DECODE_RESULT_OK                  = Integer(0);
  PEM_DECODE_RESULT_INVALID_FORMAT      = Integer($1D01);
  PEM_DECODE_RESULT_INVALID_PASSPHRASE  = Integer($1D02);
  PEM_DECODE_RESULT_NOT_ENOUGH_SPACE    = Integer($1D03);
  PEM_DECODE_RESULT_UNKNOWN_CIPHER      = Integer($1D04);

type

  TElPEMProcessor = class(TSBComponentBase)
  protected
    FHeader : string;
    FPassphrase: string;

  public
    function PEMEncode(const InBuffer : ByteArray; var OutBuffer : ByteArray; Encrypt : boolean) : boolean; overload;
    function PEMDecode(const InBuffer : ByteArray; var OutBuffer : ByteArray) : integer;  overload;

    function PEMEncode(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer; Encrypt : boolean) : boolean; overload;
    function PEMDecode(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer) : integer; overload;
  published
    property Header : string read FHeader write FHeader;
    property Passphrase: string read FPassphrase write FPassphrase;
  end;

function Encode(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
  var OutSize : integer; const Header : string; Encrypt : boolean;
  PassPhrase : string) : boolean;
function Decode(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
  PassPhrase : string; var OutSize : integer; var Header : string) : integer;

type 
  EPEMError =  class(ESecureBlackboxError);
  
procedure RaisePEMError(ErrorCode : integer); 

implementation

uses
  {$ifndef SBB_NO_DES}
  SBDES,
  SB3DES,
  {$endif}
  SBConstants,
  SBMath,
  SBMD
  ;

resourcestring

  sInvalidPEMFormat     = 'Invalid file format (possibly not a PEM?)';
  sIncorrectPassphrase  = 'Incorrect password';
  sNotEnoughBufferSpace = 'Not enough buffer space';
  sUnknownCipher        = 'Unsupported data encryption method';

procedure RaisePEMError(ErrorCode : integer);
begin
  case ErrorCode of
    PEM_DECODE_RESULT_INVALID_FORMAT     : raise EPEMError.Create(sInvalidPEMFormat, ErrorCode{$ifndef HAS_DEF_PARAMS}{$ifndef FPC}, 0{$endif}{$endif});
    PEM_DECODE_RESULT_INVALID_PASSPHRASE : raise EPEMError.Create(sIncorrectPassphrase, ErrorCode{$ifndef HAS_DEF_PARAMS}{$ifndef FPC}, 0{$endif}{$endif});
    PEM_DECODE_RESULT_NOT_ENOUGH_SPACE   : raise EPEMError.Create(sNotEnoughBufferSpace, ErrorCode{$ifndef HAS_DEF_PARAMS}{$ifndef FPC}, 0{$endif}{$endif});
    PEM_DECODE_RESULT_UNKNOWN_CIPHER     : raise EPEMError.Create(sUnknownCipher, ErrorCode{$ifndef HAS_DEF_PARAMS}{$ifndef FPC}, 0{$endif}{$endif});
    else
      exit;
  end;
end;

{$ifndef SBB_NO_DES}
procedure ConvertPassPhraseToKey(const PassPhrase : string; var Key : TDESKey;
  var IV : TDESBuffer);
var
  MD1, MD2, MD3 : TMessageDigest128;
begin
  MD1 := HashMD5(PassPhrase);
  MD2 := HashMD5(PassPhrase + base64Pad + PassPhrase);
  MD3.A := MD1.A xor MD2.A;
  MD3.B := MD1.B xor MD2.B;
  MD3.C := MD1.C xor MD2.C;
  MD3.D := MD1.D xor MD2.D;
  Move(PByteArray(@MD3)[0], Key[0], 8);
  Move(PByteArray(@MD3)[8], IV[0], 8);
end;
{$endif}

function BytesToKey(const Passphrase : BufferType; const Salt : BufferType; Needed: integer) : BufferType;
var
  Context : TMD5Context;
  AM : integer;
  MDS, I, KeyCount : integer;
  M128 : TMessageDigest128;
  KeyPtr : ^byte;
begin
  AM := 0;
  MDS := 0;
  SetLength(Result, Needed);
  KeyPtr := @Result[1];
  KeyCount := Needed;
  while true do
  begin
    SBMD.InitializeMD5(Context);
    Inc(AM);
    if (AM <> 0) then
      SBMD.HashMD5(Context, @M128, MDS);
    SBMD.HashMD5(Context, @Passphrase[1], Length(Passphrase));
    if Length(Salt) <> 0 then
      SBMD.HashMD5(Context, @Salt[1], Length(Salt));
    M128 := SBMD.FinalizeMD5(Context);
    I := 0;
    MDS := 16;
    while true do
    begin
      if KeyCount = 0 then
        Break;
      if I = MDS then
        Break;
      KeyPtr^ := PByteArray(@M128)[I];
      Inc(KeyPtr);
      Dec(KeyCount);
      Inc(I);
    end;
    if KeyCount = 0 then
      Break;
  end;
end;

function Encode(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
  var OutSize : integer; const Header : string; Encrypt : boolean;
  PassPhrase : string) : boolean;
var
  InBuf, Buf : ByteArray;
  Sz, I, EstSize : integer;
  OutSz : cardinal;
  PEMBeginLine, PEMEndLine, PEMHeaders : AnsiString;
  {$ifndef SBB_NO_DES}
  DESKey : T3DESKey;
  DESIV : T3DESBuffer;
  Ctx : T3DESContext;
  {$endif}
  Key, Salt : BufferType;
  //RandCtx : TRC4RandomContext;
begin
  Result := true;
  {$ifndef SBB_NO_DES}
  if Encrypt then
  begin
    SetLength(Salt, 8);
    //LRC4Init(RandCtx);
    for I := 1 to 8 do
      Salt[I] := Chr(SBRndGenerate(256){LRC4RandomByte(RandCtx)});
    Key := BytesToKey(Passphrase, Salt, 24);
    Move(Key[1], DESKey[0], 24);
    Move(Salt[1], DESIV[0], 8);
    PEMHeaders := 'Proc-Type: 4,ENCRYPTED'#$0A;
    PEMHeaders := PEMHeaders + 'DEK-Info: DES-EDE3-CBC,';
    for I := 0 to 7 do
      PEMHeaders := PEMHeaders + IntToHex(DESIV[I], 2);
    PEMHeaders := PEMHeaders + #$0A#$0A;
    SetLength(InBuf, (InSize div 8 + 1) * 8);
    Move(InBuffer^, InBuf[0], InSize);
    FillChar(InBuf[InSize], Length(InBuf) - InSize, Chr(Length(InBuf) - InSize));
    SB3DES.InitializeEncryptionCBC(Ctx, DESKey, DESIV);
    OutSz := Length(InBuf);
    SB3DES.EncryptCBC(Ctx, @InBuf[0], Length(InBuf), @InBuf[0], OutSz);
    SetLength(Buf, Length(InBuf) * 2);
    Sz := Length(InBuf) * 2;
    if not Base64Encode(@InBuf[0], Length(InBuf), @Buf[0], Sz) then
    begin
      Result := false;
      Exit;
    end;
  end
  else
  {$endif}
  begin
    SetLength(Buf, InSize * 2);
    Sz := InSize * 2;
    if not Base64Encode(InBuffer, InSize, @Buf[0], Sz) then
    begin
      Result := false;
      Exit;
    end;
    PEMHeaders := '';
  end;
  
  PEMBeginLine := BeginLine + Header + Line + #$0A + PEMHeaders;
  PEMEndLine := EndLine + Header + Line + #$0A;

  { Checking whether we have enough space in buffer }
  EstSize := Length(PEMBeginLine) + Length(PEMEndLine) + Sz;
  if (OutSize < EstSize)  then
  begin
    OutSize := EstSize;
    Result := false;
    Exit;
  end;
  Move(PEMBeginLine[1], PByteArray(OutBuffer)[0], Length(PEMBeginLine));
  SetLength(Buf, Sz);
  Move(Buf[0], PByteArray(OutBuffer)[Length(PEMBeginLine)], Length(Buf));
  Move(PEMEndLine[1], PByteArray(OutBuffer)[Length(PEMBeginLine) + Length(Buf)],
    Length(PEMEndLine));
  OutSize := EstSize; //Length(PEMBeginLine) + Length(Buf) + Length(PEMEndLine);
  SetLength(Buf, 0);
end;

function HexStringToBuffer(const HexStr : string) : BufferType;
  function HexBytesToSym(B1, B2 : char) : byte;
  var
    P1, P2 : byte;
  begin
    if (B1 >= 'A') and (B1 <= 'F') then
      P1 := Ord(B1) - Ord('A') + 10
    else if (B1 >= '0') and (B1 <= '9') then
      P1 := Ord(B1) - Ord('0')
    else
      P1 := 0;
    if (B2 >= 'A') and (B2 <= 'F') then
      P2 := Ord(B2) - Ord('A') + 10
    else if (B2 >= '0') and (B2 <= '9') then
      P2 := Ord(B2) - Ord('0')
    else
      P2 := 0;
    Result := (P1 shl 4) or P2;
  end;
var
  I : integer;
begin
  if Length(HexStr) mod 2 <> 0 then
    SetLength(Result, 0)
  else
  begin
    I := 1;
    SetLength(Result, Length(HexStr) div 2);
    while I <= Length(Result) do
    begin
      Result[I] := Chr(HexBytesToSym(HexStr[I * 2 - 1], HexStr[I * 2]));
      Inc(I);
    end;
  end;
end;

function Decode(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
  PassPhrase : string; var OutSize : integer; var Header : string) : integer;
var
  S1, S2 : BufferType;
  S, Headers, Tmp : String;
  I, HeaderEnd, Res, Cipher, I1, I2, I3 : integer;
  {$ifndef SBB_NO_DES}
  DESIV : TDESBuffer;
  DESEDE3IV : T3DESBuffer;
  DESKey : TDESKey;
  DESEDE3Key : T3DESKey;
  Ctx : TDESContext;
  CtxEDE : T3DESContext;
  {$endif}
  Sz : cardinal;
  EolLen : integer;
begin
  Result := PEM_DECODE_RESULT_OK;
  SetLength(S, InSize);
  Move(InBuffer^, S[1], InSize);
  if Pos(BeginLine, S) <= 0 then
  begin
    Result := PEM_DECODE_RESULT_INVALID_FORMAT;
    Exit;
  end;

  I1 := Pos(#13#13#10, S);
  I2 := Pos(#13#10, S);
  I3 := Pos(#10, S);
  if I1 = 0 then I1 := $7FFFFFFF;
  if I2 = 0 then I2 := $7FFFFFFF;
  if I3 = 0 then I3 := $7FFFFFFF;
  I := Min(Min(I1, I2), I3);
  if I = I1 then
    EolLen := 3
  else if I = I2 then
    EolLen := 2
  else
    EolLen := 1;

  if I = $7FFFFFFF then
  begin
    Result := PEM_DECODE_RESULT_INVALID_FORMAT;
    OutSize := 0;
    Exit;
  end;

  Header := Copy(S, Length(BeginLine) + 1, I - Length(Line) - Length(BeginLine) - 1);
  S := Copy(S, I + EolLen, Length(S));

  if Pos('Proc-Type', S) = 1 then
  begin

    HeaderEnd := Pos(#$0D#$0D#$0A#$0D#$0D#$0A, S);
    if HeaderEnd = 0 then
    begin
      HeaderEnd := Pos(#$0A#$0A, S);
      if HeaderEnd = 0 then
        HeaderEnd := Pos(#$0D#$0A#$0D#$0A, S);
    end;

    Headers := Copy(S, 1, HeaderEnd - 1);
    
    while Pos('DEK-Info:', Headers) > 1 do
    begin
      I := Pos(#$0A, Headers);
      Headers := Copy(Headers, I + 1, Length(Headers));
    end;

    if Pos('DEK-Info:', Headers) = 1 then
    begin
      I := Pos(#$0A, Headers);

      if I > 0 then
        Headers := Copy(Headers, 1, I - 1);
      I := Pos(' ', Headers);
      Headers := Copy(Headers, I + 1, Length(Headers));
      I := Pos(',', Headers);
      Tmp := Copy(Headers, 1, I - 1);

      {$ifndef SBB_NO_DES}
      if CompareStr('DES-CBC', Tmp) = integer(0) then
        Cipher := SB_ALGORITHM_CNT_DES
      else
      if CompareStr('DES-EDE3-CBC', Tmp) = integer(0) then
        Cipher := SB_ALGORITHM_CNT_3DES
      else
      {$endif}
      begin
        Result := PEM_DECODE_RESULT_UNKNOWN_CIPHER;
        Exit;
      end;
      
      Tmp := Copy(Headers, I + 1, Length(Headers));
      S1 := HexStringToBuffer(Tmp);

      {$ifndef SBB_NO_DES}
      if Cipher = SB_ALGORITHM_CNT_DES then
      begin
        S2 := BytesToKey(Passphrase, S1, 8);
        Move(S2[1], DESKey[0], 8);
        Move(S1[1], DESIV[0], 8);
      end
      else
      if Cipher = SB_ALGORITHM_CNT_3DES then
      begin
        S2 := BytesToKey(Passphrase, S1, 24);
        Move(S2[1], DESEDE3Key[0], 24);
        Move(S1[1], DESEDE3IV[0], 8);
      end;
      {$endif}
      Headers := '';
      {$ifndef SBB_NO_DES}
      for I := 0 to 7 do
        if Cipher = SB_ALGORITHM_CNT_DES then
          Headers := Headers + IntToHex(DESIV[I], 2)
        else
        if Cipher = SB_ALGORITHM_CNT_3DES then
          Headers := Headers + IntToHex(DESEDE3IV[I], 2);
      {$endif}
      if CompareStr(Headers, Tmp) <> integer(0) then
      begin
        Result := PEM_DECODE_RESULT_INVALID_PASSPHRASE;
        Exit;
      end;
      I := Pos(EndLine, S);
      Res := Base64Decode(@S[HeaderEnd + 2], I - HeaderEnd - 2, OutBuffer, OutSize);

      if Res = 0 then
      begin
        {$ifndef SBB_NO_DES}
        if Cipher = SB_ALGORITHM_CNT_DES then
        begin
          SBDES.InitializeDecryptionCBC(Ctx, DESKey, DESIV);
          Sz := OutSize;
          SBDES.DecryptCBC(Ctx, OutBuffer, OutSize, OutBuffer, Sz);
        end
        else
        if Cipher = SB_ALGORITHM_CNT_3DES then
        begin
          SB3DES.InitializeDecryptionCBC(CtxEDE, DESEDE3Key, DESEDE3IV);
          Sz := OutSize;
          SB3DES.DecryptCBC(CtxEDE, OutBuffer, OutSize, OutBuffer, Sz);
        end;
        {$endif}
        if PByteArray(OutBuffer)[Sz - 1] > 8 then
        begin
          Result := PEM_DECODE_RESULT_INVALID_PASSPHRASE;
          Exit;
        end;
        OutSize := Sz - PByteArray(OutBuffer)[Sz - 1];
        if OutSize < 0 then
        begin
          Result := PEM_DECODE_RESULT_INVALID_PASSPHRASE;
          Exit;
        end;
        Result := PEM_DECODE_RESULT_OK;
      end
      else
      if Res = BASE64_DECODE_NOT_ENOUGH_SPACE then
      begin
        Result := PEM_DECODE_RESULT_NOT_ENOUGH_SPACE;
        Exit;
      end
      else
      if Res = BASE64_DECODE_INVALID_CHARACTER then
      begin
        Result := PEM_DECODE_RESULT_INVALID_FORMAT;
        Exit;
      end;
    end
    else
    begin
      Result := PEM_DECODE_RESULT_INVALID_FORMAT;
      Exit;
    end;
  end
  else
  begin
    I := Pos(EndLine, S);
    Result := Base64Decode(@S[1], I - 1, OutBuffer, OutSize);

    case Result of
      BASE64_DECODE_OK:
        Result := PEM_DECODE_RESULT_OK;
      BASE64_DECODE_NOT_ENOUGH_SPACE:
        Result := PEM_DECODE_RESULT_NOT_ENOUGH_SPACE
    else
      Result := PEM_DECODE_RESULT_INVALID_FORMAT;
    end;
  end;
end;

function TElPEMProcessor.PEMEncode(const InBuffer : ByteArray; var OutBuffer : ByteArray; Encrypt : boolean) : boolean;
var OutSize : integer;
begin
  OutSize := 0;
  Encode(InBuffer, Length(InBuffer), nil, OutSize, FHeader, Encrypt, FPassphrase);
  SetLength(OutBuffer, OutSize);
  result := Encode(InBuffer, Length(InBuffer), OutBuffer, OutSize, FHeader, Encrypt, FPassphrase);
  if result then
  begin
    if (Length(OutBuffer) <> OutSize) then
     SetLength(OutBuffer, OutSize);
  end
  else
    SetLength(OutBuffer, 0);
end;

function TElPEMProcessor.PEMEncode(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
  var OutSize : integer; Encrypt : boolean) : boolean;
begin
  result := Encode(InBuffer, InSize, OutBuffer, OutSize, FHeader, Encrypt, FPassphrase);
end;

function TElPEMProcessor.PEMDecode(const InBuffer : ByteArray; var OutBuffer : ByteArray) : integer;
var OutSize : integer;
begin
  OutSize := 0;
  result := Decode(InBuffer, Length(InBuffer),  nil, FPassphrase, OutSize, FHeader);
  if result = PEM_DECODE_RESULT_NOT_ENOUGH_SPACE then
  begin
    SetLength(OutBuffer, OutSize);
    result := Decode(InBuffer, Length(InBuffer),  OutBuffer, FPassphrase, OutSize, FHeader);
    if result = 0 then
    begin
      if (Length(OutBuffer) <> OutSize) then
       SetLength(OutBuffer, OutSize);
    end
    else
      SetLength(OutBuffer, 0);
  end
  else
    SetLength(OutBuffer, 0);
end;

function TElPEMProcessor.PEMDecode(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
  var OutSize : integer) : integer;
begin
  result := Decode(InBuffer, InSize, OutBuffer, FPassphrase, OutSize, FHeader);
end;


end.
