(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBBzlib;
//-------------------------------------------------------------*/
//--- Library top-level functions.                          ---*/
//---                                               bzlib.c ---*/
//-------------------------------------------------------------*/

interface

uses
  SysUtils,
  //Math,
  SBUtils,
  SBBzip2Types,
  SBBzip2BaseTypes,
  SBBzip2Utils;

type

  bzfile = class(TObject)
  public
      handle: TOpenedFile;
      buf: TSBByteArray;
      bufN: Int32;
      writing: Boolean;
      strm: bz_stream;
      lastErr: Int32;
      initialisedOk: Boolean;
   end;                 

procedure AssertH(Condition:Boolean; Err_Code:integer);
procedure AssertD(Condition:Boolean; Err_Text:string);
function BZ2_indexIntoF ( indx:Int32; var cftab:array of Int32):Int32;
procedure BZ2_bzWriteClose64
                  ( var bzerror:Int32;
                    b:BZFILE;
                    abandon:boolean;
                    var nbytes_in_lo32,
                    nbytes_in_hi32,
                    nbytes_out_lo32,
                    nbytes_out_hi32:UInt32 );
function BZ2_bzWriteOpen
                    ( var bzerror:Int32;
                      f:TOpenedFile;
                      blockSize100k,
                      verbosity,
                      workFactor:integer):BZFILE;
function BZ2_bzwrite(b:BZFILE;buf:TSBByteArray; len:integer ):integer;
procedure BZ2_bzWrite2
             ( var bzerror:Int32;
               b:BZFILE;
               buf:TSBByteArray;
               len:integer );
procedure BZ2_bzReadClose( var bzerror:Int32; b:BZFILE);
function BZ2_bzReadOpen
                   ( var bzerror:Int32;
                     f:TOpenedFile;
                     verbosity:Int32;
                     small:boolean;
                     var unused:array  of Byte;
                     nUnused:integer ):BZFILE;
function BZ2_bzCompressInit
                    ( strm:bz_stream;
                     blockSize100k,
                     verbosity,
                     workFactor:integer ):integer;
function BZ2_bzCompress( strm:bz_stream; action:integer ):integer;
function BZ2_bzCompressEnd( strm:bz_stream ):integer;
function BZ2_bzDecompressInit
                     ( strm:bz_stream;
                       verbosity:integer;
                       small:boolean ):integer;
function BZ2_bzDecompress ( strm:bz_stream):Integer;
function BZ2_bzDecompressEnd( strm:bz_stream):integer;
function BZ2_bzread(b:BZFILE; buf:TSBByteArray; len:integer ):integer;
function BZ2_bzRead2
           ( var bzerror:Int32;
             b:BZFILE;
             buf:TSBByteArray;
             len:integer ):integer;
procedure BZ2_bzReadGetUnused
                     ( var bzerror:Int32;
                       b:BZFILE;
                       var unused:array of Byte;
                       var nUnused:Int32);

function BZ2_bzBuffToBuffCompress
                         ( dest:TSBByteArray;
                           var destLen:UInt32;
                           source:TSBByteArray;
                           sourceLen:UInt32;
                           blockSize100k,
                           verbosity,
                           workFactor:integer):integer;
function BZ2_bzBuffToBuffDecompress
                           ( dest:TSBByteArray;
                             var destLen:UInt32;
                             source:TSBByteArray;
                             sourceLen:UInt32;
                             small:boolean;
                             verbosity:Int32):integer;


function f_myfeof ( F:TOpenedFile):boolean;
function f_fileread(F:TOpenedFile;var Buf:TBytes;Count:integer):integer;
function f_filewrite(F:TOpenedFile;Buf:TBytes;Count:integer):integer;

implementation

uses
  SBBzip2Common;

procedure AssertH(Condition:Boolean; Err_Code:integer);
begin
  If not Condition then
    Halt(Err_Code);
end;

procedure AssertD(Condition:Boolean; Err_Text:string);
begin
  If not Condition then
  begin
    Writeln(ErrOutput,Err_Text);
    Halt(1);
  end;
end;


procedure BZ2_bz__AssertH__fail (errcode: integer);
begin
   Writeln(ErrOutput,Format(
      CRLF+CRLF+'bzip2/libbzip2: internal error number %d.'
(*
+CRLF+
      'This is a bug in bzip2/libbzip2, %s.'+CRLF+
      'Please report it to me at: jseward@bzip.org.  If this happened'+CRLF+
      'when you were using some program which uses libbzip2 as a'+CRLF+
      'component, you should also report this bug to the author(s)'+CRLF+
      'of that program.  Please make an effort to report this bug;'+CRLF+
      'timely and accurate bug reports eventually lead to higher'+CRLF+
      'quality software.  Thanks.  Julian Seward, 15 February 2005.'+CRLF+CRLF
*),
      [errcode,BZ_VERSION])
   );

   if (errcode = 1007) then
    Writeln(ErrOutput,
      CRLF+'*** A special note about internal error number 1007 ***'+CRLF+
      CRLF+
      'Experience suggests that a common cause of i.e. 1007'+CRLF+
      'is unreliable memory or other hardware.'+CRLF+

(*
'  The 1007 assertion'+CRLF+
      'just happens to cross-check the results of huge numbers of'+CRLF+
      'memory reads/writes, and so acts (unintendedly) as a stress'+CRLF+
      'test of your memory system.'+CRLF+
      CRLF+
      'I suggest the following: try compressing the file again,'+CRLF+
      'possibly monitoring progress in detail with the -vv flag.'+CRLF+
      CRLF+
      '* If the error cannot be reproduced, and/or happens at different'+CRLF+
      '  points in compression, you may have a flaky memory system.'+CRLF+
      '  Try a memory-test program.  I have used Memtest86'+CRLF+
      '  (www.memtest86.com).  At the time of writing it is free (GPLd).'+CRLF+
      '  Memtest86 tests memory much more thorougly than your BIOSs'+CRLF+
      '  power-on test, and may find failures that the BIOS doesn''t.'+CRLF+
      CRLF+
      '* If the error can be repeatably reproduced, this is a bug in'+CRLF+
      '  bzip2, and I would very much like to hear about it.  Please'+CRLF+
      '  let me know, and, ideally, save a copy of the file causing the'+CRLF+
      '  problem -- without which I will be unable to investigate it.'+CRLF+
*)
      CRLF);
   halt(3);
end;

function bz_config_ok:Boolean;
begin
   Result:=not ((sizeof(Int32) <> 4) or (sizeof(UInt32) <> 4)  or
       (sizeof(Int16) <> 2) or (sizeof(UInt16) <> 2)  or
       (sizeof(Byte) <> 1));
end;



procedure BZ_UPDATE_CRC(var crcVar:UInt32; cha:Byte);
begin
   crcVar := (crcVar shl 8) xor
            BZ2_crc32Table[(crcVar shr 24) xor
                           cha];
end;



procedure prepare_new_block ( s:EState );
var
   i:Int32;
begin
   s.nblock := 0;
   s.numZ := 0;
   s.state_out_pos := 0;
   //BZ_INITIALISE_CRC
   s.blockCRC :=$ffffffff;
   for i := 0 to 255 do
      s.inUse[i] := False;
   inc(s.blockNo);
end;


procedure init_RL ( s:EState );
begin
   s.state_in_ch  := 256;
   s.state_in_len := 0;
end;


function isempty_RL ( s:EState):boolean;
begin
   Result:=not ((s.state_in_ch < 256) and (s.state_in_len > 0))
end;


function BZ2_bzCompressInit
                    ( strm:bz_stream;
                     blockSize100k,
                     verbosity,
                     workFactor:integer ):integer;
var
   n :Int32;
   s:EState;
begin

   if (not bz_config_ok()) then
   begin
      Result:=BZ_CONFIG_ERROR;
      exit;
   end;

   if (strm = nil) or
       (blockSize100k < 1) or (blockSize100k > 9) or
       (workFactor < 0) or (workFactor > 250) then
   begin
     Result:=BZ_PARAM_ERROR;
     exit;
   end;

   if (workFactor = 0) then workFactor := 30;

//   s := BZALLOC( sizeof(EState) );
   S:=EState.Create;     
   if (s = nil) then
   begin
     Result:=BZ_MEM_ERROR;
     exit;
   end;
   s.strm := strm;

   s.arr1 := nil;
   s.arr2 := nil;
   s.ftab := nil;
   s.zbits := nil;

   n       := 100000 * blockSize100k;
   s.arr2 := TSBUInt32Array.Create(n+BZ_N_OVERSHOOT);
   s.arr1 := TSBUInt32Array.Create(n);
   s.ftab := TSBUInt32Array.Create(65537);
   s.zbits:= TSBByteonUInt32Array.Create(s.arr2,'s.arr2');
   if (s.arr1 = nil) or (s.arr2 = nil) or (s.ftab = nil) or (s.zbits= nil)then
   begin
      if (s.arr1 <> nil) then s.arr1.Free;//Destroy;
      if (s.arr2 <> nil) then s.arr2.Free;//Destroy;
      if (s.ftab <> nil) then s.ftab.Free;//Destroy;
      if (s.zbits <> nil) then s.zbits.Free;//Destroy;
      if (s       <> nil) then s.Free;
      Result:=BZ_MEM_ERROR;
      exit;
   end;
   for n := 0 to BZ_N_GROUPS do
   begin
        s.len[n] := nil;
        s.code[n] := nil;
        s.rfreq[n] := nil;
   end;

   for n := 0 to BZ_N_GROUPS do
   begin
        s.len[n] := TSBByteArray.Create(BZ_MAX_ALPHA_SIZE);
        s.code[n] := TSBInt32Array.Create(BZ_MAX_ALPHA_SIZE);
        s.rfreq[n] := TSBInt32Array.Create(BZ_MAX_ALPHA_SIZE);
        if (s.len[n] = nil) or (s.code[n]=nil) or (s.rfreq[n]=nil) then
        begin
                if (s.arr1 <> nil) then s.arr1.Free;//Destroy;
                if (s.arr2 <> nil) then s.arr2.Free;//Destroy;
                if (s.ftab <> nil) then s.ftab.Free;//Destroy;
                if (s       <> nil) then s.Free;
                Result:=BZ_MEM_ERROR;
                exit;
        end;
   end;



   s.blockNo           := 0;
   s.state             := BZ_S_INPUT;
   s.mode              := BZ_M_RUNNING;
   s.combinedCRC       := 0;
   s.blockSize100k     := blockSize100k;
   s.nblockMAX         := 100000 * blockSize100k - 19;
   s.verbosity         := verbosity;
   s.workFactor        := workFactor;

   s.block             := TSBByteonUInt32Array.Create(s.arr2,'s.arr2');
   s.mtfv              := TSBUInt16onUInt32Array.Create(s.arr1,'s.arr1');
   s.ptr               := TSBUInt32ArrayPointer.Create(s.arr1,'s.arr1');

   strm.state_e          := s;
   strm.total_in_lo32  := 0;
   strm.total_in_hi32  := 0;
   strm.total_out_lo32 := 0;
   strm.total_out_hi32 := 0;
   init_RL ( s );
   prepare_new_block ( s );
   Result:=BZ_OK;
end;


procedure add_pair_to_block ( s:EState );
var
   i:Int32;
   ch:Byte;
begin
   ch := s.state_in_ch;
   for i := 0 to s.state_in_len-1 do
      BZ_UPDATE_CRC( s.blockCRC, ch );
   s.inUse[s.state_in_ch] := True;
   case s.state_in_len of
      1:begin
          s.block.SetData(ch,s.nblock);
          inc(s.nblock);
        end;
      2:begin
          s.block.SetData(ch,s.nblock);
          inc(s.nblock);
          s.block.SetData(ch,s.nblock);
          inc(s.nblock);
        end;
      3:begin
          s.block.SetData(ch,s.nblock);
          inc(s.nblock);
          s.block.SetData(ch,s.nblock);
          inc(s.nblock);
          s.block.SetData(ch,s.nblock);
          inc(s.nblock);
        end;
      else
      begin
          s.inUse[s.state_in_len-4] := True;
          s.block.SetData(ch,s.nblock);
          inc(s.nblock);
          s.block.SetData(ch,s.nblock);
          inc(s.nblock);
          s.block.SetData(ch,s.nblock);
          inc(s.nblock);
          s.block.SetData(ch,s.nblock);
          inc(s.nblock);
          s.block.SetData(s.state_in_len-4,s.nblock);
          inc(s.nblock);
      end;
   end;
end;


procedure flush_RL ( s:EState );
begin
   if (s.state_in_ch < 256) then add_pair_to_block ( s );
   init_RL ( s );
end;


procedure ADD_CHAR_TO_BLOCK(zs:EState;zchh0:Byte);
var
   zchh:Byte;
   ch:Byte;
begin
   zchh := zchh0;
   if (zchh <> zs.state_in_ch) and
       (zs.state_in_len = 1) then
   begin
      ch := zs.state_in_ch;
      BZ_UPDATE_CRC( zs.blockCRC, ch );
      zs.inUse[zs.state_in_ch] := True;
      zs.block.SetData(ch,zs.nblock);
      inc(zs.nblock);
      zs.state_in_ch := zchh;
   end
   else
   if (zchh <> zs.state_in_ch) or
      (zs.state_in_len = 255) then
   begin
      if (zs.state_in_ch < 256) then
         add_pair_to_block ( zs );
      zs.state_in_ch := zchh;
      zs.state_in_len := 1;
   end
   else
      inc(zs.state_in_len);
end;



function copy_input_until_stop ( s:EState ):boolean;
var
   progress_in:boolean;
begin
   progress_in:=False;
   if (s.mode = BZ_M_RUNNING) then
      while (True) do
      begin
         if (s.nblock >= s.nblockMAX) then
                break;
         if (s.strm.avail_in = 0) then break;
         progress_in := True;
         ADD_CHAR_TO_BLOCK ( s, s.strm.next_in.GetAtPtr);
         s.strm.next_in.IncOffset(1);
         dec(s.strm.avail_in);
         inc(s.strm.total_in_lo32);
         if (s.strm.total_in_lo32 = 0) then inc(s.strm.total_in_hi32);
      end
   else
      while (True) do
      begin
         if (s.nblock >= s.nblockMAX) then
                break;
         if (s.strm.avail_in = 0) then break;
         if (s.avail_in_expect = 0) then break;
         progress_in := True;
         ADD_CHAR_TO_BLOCK ( s, s.strm.next_in.GetAtPtr);
         s.strm.next_in.IncOffset(1);
         dec(s.strm.avail_in);
         inc(s.strm.total_in_lo32);
         if (s.strm.total_in_lo32 = 0) then inc(s.strm.total_in_hi32);
         dec(s.avail_in_expect);
      end;
   Result:=progress_in;
end;


function copy_output_until_stop ( s:EState ):boolean;
var
   progress_out:boolean;
begin
   progress_out := False;
   while (True) do
   begin

      if (s.strm.avail_out = 0) then  break;

      if (s.state_out_pos >= s.numZ) then break;

      progress_out := True;
//      *(s.strm.next_out) = s.zbits[s.state_out_pos];
      s.strm.next_out.SetAtPtr(s.zbits.GetData(s.state_out_pos));
      inc(s.state_out_pos);
      dec(s.strm.avail_out);
      s.strm.next_out.IncOffset(1);
      inc(s.strm.total_out_lo32);
      if (s.strm.total_out_lo32 = 0) then inc(s.strm.total_out_hi32);
   end;

   Result:=progress_out;
end;


function handle_compress ( strm:bz_stream):boolean;
var
   progress_in,progress_out:boolean;
   s:EState;
begin
   progress_in  := False;
   progress_out := False;
   s := strm.state_e;

   while (True) do
   begin

      if (s.state = BZ_S_OUTPUT) then
      begin
         progress_out := copy_output_until_stop ( s ) or progress_out ;
         if (s.state_out_pos < s.numZ) then break;
         if (s.mode = BZ_M_FINISHING) and
             (s.avail_in_expect = 0) and
             isempty_RL(s) then break;
         prepare_new_block ( s );
         s.state := BZ_S_INPUT;
         if (s.mode = BZ_M_FLUSHING) and
             (s.avail_in_expect = 0) and
             isempty_RL(s) then break;
      end;

      if (s.state = BZ_S_INPUT) then
      begin
         progress_in := copy_input_until_stop ( s ) or progress_in;
         if (s.mode <> BZ_M_RUNNING) and (s.avail_in_expect = 0) then
         begin
            flush_RL ( s );
            BZ2_compressBlock ( s, s.mode = BZ_M_FINISHING );
            s.state := BZ_S_OUTPUT;
         end
         else
         if (s.nblock >= s.nblockMAX) then
         begin
            BZ2_compressBlock ( s, False );
            s.state := BZ_S_OUTPUT;
         end
         else
         if (s.strm.avail_in = 0) then
            break;
      end;
   end;

   Result:=progress_in or progress_out;
end;


function BZ2_bzCompress( strm:bz_stream; action:integer ):integer;
var
   progress:boolean;
   s:EState;
begin
   if (strm = nil) then
   begin
      Result:=BZ_PARAM_ERROR;
      exit;
   end;
   s := strm.state_e;
   if (s = nil) then
   begin
      Result:=BZ_PARAM_ERROR;
      exit;
   end;
   if (s.strm <> strm) then
   begin
      Result:=BZ_PARAM_ERROR;
      exit;
   end;

   While (True) do
   begin
       case (s.mode) of
          BZ_M_IDLE:
             begin
                Result:=BZ_SEQUENCE_ERROR;
                exit;
             end;
          BZ_M_RUNNING:
             if (action = BZ_RUN) then
             begin
                progress := handle_compress ( strm );
                Result:=IfThenInt(progress,BZ_RUN_OK,BZ_PARAM_ERROR);
                exit;
             end
             else
             if (action = BZ_FLUSH) then
             begin
                s.avail_in_expect := strm.avail_in;
                s.mode := BZ_M_FLUSHING;
                continue;
             end
             else
             if (action = BZ_FINISH) then
             begin
                s.avail_in_expect := strm.avail_in;
                s.mode := BZ_M_FINISHING;
                continue;
             end
             else
             begin
                Result:=BZ_PARAM_ERROR;
                exit;
             end;

          BZ_M_FLUSHING:
             begin
                 if (action <> BZ_FLUSH) then
                 begin
                    Result:=BZ_SEQUENCE_ERROR;
                    exit;
                 end;
                 if (integer(s.avail_in_expect) <> s.strm.avail_in) then
                 begin
                    Result:=BZ_SEQUENCE_ERROR;
                    exit;
                 end;
                 {progress := }handle_compress ( strm );
                 if (s.avail_in_expect > 0) or (not isempty_RL(s)) or
                     (s.state_out_pos < s.numZ) then
                 begin
                    Result:=BZ_FLUSH_OK;
                    exit;
                 end;
                 s.mode := BZ_M_RUNNING;
                 Result:=BZ_RUN_OK;
                 exit;
             end;

          BZ_M_FINISHING:
            begin
               if (action <> BZ_FINISH) then
               begin
                   Result:=BZ_SEQUENCE_ERROR;
                   exit;
               end;
               if (integer(s.avail_in_expect) <> s.strm.avail_in) then
               begin
                   Result:=BZ_SEQUENCE_ERROR;
                   exit;
               end;
               progress := handle_compress ( strm );
               if (not progress) then
               begin
                   Result:=BZ_SEQUENCE_ERROR;
                   exit;
               end;
               if (s.avail_in_expect > 0) or (not isempty_RL(s)) or
                 (s.state_out_pos < s.numZ) then
               begin
                   Result:=BZ_FINISH_OK;
                   exit;
               end;
               s.mode := BZ_M_IDLE;
               Result:=BZ_STREAM_END;
               exit;
            end;
       end;
       break;
   end;
   Result:=BZ_OK;
end;


function BZ2_bzCompressEnd( strm:bz_stream ):integer;
var
   s:EState;
   n:Int32;
begin
   if (strm = nil) then
   begin
    Result:=BZ_PARAM_ERROR;
    exit;
   end;
   s := strm.state_e;
   if (s = nil) then
   begin
    Result:=BZ_PARAM_ERROR;
    exit;
   end;
   if (s.strm <> strm) then
   begin
    Result:=BZ_PARAM_ERROR;
    exit;
   end;

   if (s.arr1 <> nil) then s.arr1.Free;//Destroy;
   if (s.arr2 <> nil) then s.arr2.Free;//Destroy;
   if (s.ftab <> nil) then s.ftab.Free;//Destroy;
   if (s.zbits <> nil) then s.zbits.Free;
   for n := 0 to BZ_N_GROUPS do
   begin
        if s.len[n]<>nil then s.len[n].Free;//Destroy;
        if s.code[n]<>nil then s.code[n].Free;//Destroy;
        if s.rfreq[n]<>nil then s.rfreq[n].Free;//Destroy;
   end;

   if s.block <> nil then s.block.Free;
   if s.mtfv <> nil then s.mtfv.Free;
   if s.ptr <> nil then s.ptr.Free;

   strm.state_e.Free;

   strm.state_e := nil;

   Result:=BZ_OK;
end;


//---------------------------------------------------*/
//--- Decompression stuff                         ---*/
//---------------------------------------------------*/

function BZ2_bzDecompressInit
                     ( strm:bz_stream;
                       verbosity:integer;
                       small:boolean ):integer;
var
   s : DState;
   n : Int32;
begin
   if (not bz_config_ok()) then
   begin
    Result:=BZ_CONFIG_ERROR;
    exit;
   end;

   if (strm = nil) then
   begin
    Result:=BZ_PARAM_ERROR;
    exit;
   end;
   if (verbosity < 0) or (verbosity > 4) then
   begin
    Result:=BZ_PARAM_ERROR;
    exit;
   end;


   s:=DState.Create;
   if (s = nil) then
   begin
    Result:=BZ_MEM_ERROR;
    exit;
   end;

   s.strm                  := strm;
   strm.state_d              := s;
   s.state                 := BZ_X_MAGIC_1;
   s.bsLive                := 0;
   s.bsBuff                := 0;
   s.calculatedCombinedCRC := 0;
   strm.total_in_lo32      := 0;
   strm.total_in_hi32      := 0;
   strm.total_out_lo32     := 0;
   strm.total_out_hi32     := 0;
   s.smallDecompress       := small;
   s.ll4                   := nil;
   s.ll16                  := nil;
   s.tt                    := nil;
   s.currBlockNo           := 0;
   s.verbosity             := verbosity;
   for n := 0 to BZ_N_GROUPS do
   begin
        s.len[n] := nil;
        s.limit[n] := nil;
        s.base[n] := nil;
        s.perm[n] := nil;
   end;

   for n := 0 to BZ_N_GROUPS do
   begin
      s.len[n] := TSBByteArray.Create(BZ_MAX_ALPHA_SIZE);
      s.limit[n] := TSBInt32Array.Create(BZ_MAX_ALPHA_SIZE);
      s.base[n] := TSBInt32Array.Create(BZ_MAX_ALPHA_SIZE);
      s.perm[n] := TSBInt32Array.Create(BZ_MAX_ALPHA_SIZE);
      if (s.len[n] = nil) or (s.limit[n]=nil) or (s.base[n]=nil) or (s.perm[n]=nil) then
      begin
              if s.len[n]<>nil then s.len[n].Free;//Destroy;
              if s.limit[n]<>nil then s.limit[n].Free;//Destroy;
              if s.base[n]<>nil then s.base[n].Free;//Destroy;
              if s.perm[n]<>nil then s.perm[n].Free;//Destroy;
              s.Free;
              Result:=BZ_MEM_ERROR;
              exit;
      end;
   end;


   Result:=BZ_OK;
end;


//---------------------------------------------------*/
// Return  True iff data corruption is discovered.
//   Returns False if there is no problem.
//
function unRLE_obuf_to_output_FAST ( s:DState):boolean;
var
   k1:Byte;
c_calculatedBlockCRC:UInt32;
c_state_out_ch:Byte;
c_state_out_len,c_nblock_used,c_k0:Int32;
      c_tt:TSBUInt32ArrayPointer;
      c_tPos:UINt32;
      cs_next_out:TSBByteArrayPointer;
      cs_avail_out:UInt32;
      avail_out_INIT:UInt32;
      s_save_nblockPP:Int32;
      total_out_lo32_old:UInt32;
      ExitFlag:Boolean;
// label s_state_out_len_eq_one;

procedure BZ_GET_FAST(var ccc:Int32); overload;
begin
    s.tPos := s.tt.GetData(s.tPos);
    ccc := s.tPos and  $ff;
    s.tPos := s.tPos shr 8;
end;

procedure BZ_GET_FAST2(var ccc:Byte); overload;
begin
    s.tPos := s.tt.GetData(s.tPos);
    ccc := s.tPos and  $ff;
    s.tPos := s.tPos shr 8;
end;

procedure BZ_GET_FAST_C(var ccc:Int32); overload;
begin
    c_tPos := c_tt.GetData(c_tPos);
    ccc := c_tPos and  $ff;
    c_tPos := c_tPos shr 8;
end;

procedure BZ_GET_FAST_C2(var ccc:Byte); overload;
begin
    c_tPos := c_tt.GetData(c_tPos);
    ccc := c_tPos and  $ff;
    c_tPos := c_tPos shr 8;
end;



procedure BZ_RAND_UPD_MASK;
begin
    if (s.rNToGo = 0) then
    begin
       s.rNToGo := BZ2_rNums[s.rTPos];
       inc(s.rTPos);
       if (s.rTPos = 512) then s.rTPos := 0;
    end;
    dec(s.rNToGo);
end;

var
  bBreak: Boolean;
begin
   if (s.blockRandomised) then
   begin

      while (True) do
      begin
         // try to finish existing run */
         while (True) do
         begin
            if (s.strm.avail_out = 0) then
            begin
              Result:=False;
              exit;
            end;
            if (s.state_out_len = 0) then break;
            s.strm.next_out.SetAtPtr(s.state_out_ch);
            BZ_UPDATE_CRC ( s.calculatedBlockCRC, s.state_out_ch );
            dec(s.state_out_len);
            s.strm.next_out.IncOffset(1);
            dec(s.strm.avail_out);
            inc(s.strm.total_out_lo32);
            if (s.strm.total_out_lo32 = 0) then  inc(s.strm.total_out_hi32);
         end;

         // can a new run be started? */
         if (s.nblock_used = s.save_nblock+1) then
         begin
           Result:=False;
           exit;
         end;

         // Only caused by corrupt data stream? */
         if (s.nblock_used > s.save_nblock+1) then
            begin
              Result:=True;
              exit;
            end;


         s.state_out_len := 1;
         s.state_out_ch := s.k0;
         BZ_GET_FAST2(k1);

         BZ_RAND_UPD_MASK;

         k1 := k1 xor IfThenInt(s.rNToGo = 1, integer(1), 0); //BZ_RAND_MASK;
         inc(s.nblock_used);
         if (s.nblock_used = (s.save_nblock+1)) then continue;
         if (k1 <> s.k0) then
         begin
          s.k0 := k1;
          continue;
         end;

         s.state_out_len := 2;
         BZ_GET_FAST2(k1); BZ_RAND_UPD_MASK;
         k1 := k1 xor IfThenInt(s.rNToGo = 1, integer(1), 0); //BZ_RAND_MASK;
         inc(s.nblock_used);
         if (s.nblock_used = (s.save_nblock+1)) then continue;
         if (k1 <> s.k0) then
         begin
          s.k0 := k1;
          continue;
         end;

         s.state_out_len := 3;
         BZ_GET_FAST2(k1); BZ_RAND_UPD_MASK;
         k1 := k1 xor IfThenInt(s.rNToGo = 1, integer(1), 0); //BZ_RAND_MASK;
         inc(s.nblock_used);
         if (s.nblock_used = (s.save_nblock+1)) then continue;
         if (k1 <> s.k0) then
         begin
          s.k0 := k1;
          continue;
         end;

         BZ_GET_FAST2(k1); BZ_RAND_UPD_MASK;
         k1 := k1 xor IfThenInt(s.rNToGo = 1, integer(1), 0); //BZ_RAND_MASK;
         inc(s.nblock_used);
         s.state_out_len := k1 + 4;
         BZ_GET_FAST(s.k0); BZ_RAND_UPD_MASK;
         s.k0 := s.k0 xor IfThenInt(s.rNToGo = 1, integer(1), 0); //BZ_RAND_MASK;
         inc(s.nblock_used);
      end;

   end
   else
   begin

      c_calculatedBlockCRC := s.calculatedBlockCRC;
      c_state_out_ch       := s.state_out_ch;
      c_state_out_len      := s.state_out_len;
      c_nblock_used        := s.nblock_used;
      c_k0                 := s.k0;
      c_tt                 := TSBUInt32ArrayPointer.Create(s.tt,'s.tt');
      c_tPos               := s.tPos;
      cs_next_out          := TSBByteArrayPointer.Create(s.strm.next_out.Base_Array,'s.strm.next_out.Base_Array');
      cs_next_out.SetOffset(s.strm.next_out.Offset);
      cs_avail_out         := s.strm.avail_out;
      avail_out_INIT := cs_avail_out;
      s_save_nblockPP := s.save_nblock+1;

      ExitFlag:=true;
      while (ExitFlag) do
      begin

         // try to finish existing run */
         if (c_state_out_len > 0) then
         begin
            while (True) do
            begin
               if (cs_avail_out = 0) then
               begin
                  ExitFlag:=false;
                  break;
               end;
               if (c_state_out_len = 1) then break;
               cs_next_out.SetAtPtr(c_state_out_ch);
               BZ_UPDATE_CRC ( c_calculatedBlockCRC, c_state_out_ch );
               dec(c_state_out_len);
               cs_next_out.IncOffset(1);
               dec(cs_avail_out);
            end;
            If not ExitFlag then
              break;

               if (cs_avail_out = 0) then
               begin
                  c_state_out_len := 1;
                  break;
               end;

               cs_next_out.SetAtPtr(c_state_out_ch);
               BZ_UPDATE_CRC ( c_calculatedBlockCRC, c_state_out_ch );
               cs_next_out.IncOffset(1);
               dec(cs_avail_out);
         end;
         // Only caused by corrupt data stream? */
         if (c_nblock_used > s_save_nblockPP) then
         begin
            Result:=true;
            exit;
         end;

         // can a new run be started? */
         if (c_nblock_used = s_save_nblockPP) then
         begin
            c_state_out_len := 0;
            break;
         end;
         c_state_out_ch := c_k0;
         BZ_GET_FAST_C2(k1); inc(c_nblock_used);

         // s_state_out_len_eq_one
         bBreak := False;
         while (k1 <> c_k0) or
               (c_nblock_used = s_save_nblockPP) do
         begin
           c_k0 := k1;
           if (cs_avail_out = 0) then
           begin
             c_state_out_len := 1;
             bBreak := True;
             break;
           end;

           cs_next_out.SetAtPtr(c_state_out_ch);
           BZ_UPDATE_CRC ( c_calculatedBlockCRC, c_state_out_ch );
           cs_next_out.IncOffset(1);
           dec(cs_avail_out);

           // Only caused by corrupt data stream? */
           if (c_nblock_used > s_save_nblockPP) then
           begin
              Result:=true;
              exit;
           end;

           // can a new run be started? */
           if (c_nblock_used = s_save_nblockPP) then
           begin
             c_state_out_len := 0;
             bBreak := True;
             break;
           end;
           c_state_out_ch := c_k0;
           BZ_GET_FAST_C2(k1); inc(c_nblock_used);
         end;
         if bBreak then
           Break;

         c_state_out_len := 2;
         BZ_GET_FAST_C2(k1); inc(c_nblock_used);
         if (c_nblock_used = s_save_nblockPP) then continue;
         if (k1 <> c_k0) then
         begin
            c_k0 := k1;
            continue;
         end;

         c_state_out_len := 3;
         BZ_GET_FAST_C2(k1); inc(c_nblock_used);
         if (c_nblock_used = s_save_nblockPP) then
            continue;
         if (k1 <> c_k0) then
         begin
            c_k0 := k1;
            continue;
         end;
   
         BZ_GET_FAST_C2(k1); inc(c_nblock_used);
         c_state_out_len := k1 + 4;
         BZ_GET_FAST_C(c_k0); inc(c_nblock_used);
      end;

      total_out_lo32_old := s.strm.total_out_lo32;
      s.strm.total_out_lo32 :=s.strm.total_out_lo32+ integer(avail_out_INIT - cs_avail_out);
      if (s.strm.total_out_lo32 < integer(total_out_lo32_old)) then
         inc(s.strm.total_out_hi32);

      // save */
      s.calculatedBlockCRC := c_calculatedBlockCRC;
      s.state_out_ch       := c_state_out_ch;
      s.state_out_len      := c_state_out_len;
      s.nblock_used        := c_nblock_used;
      s.k0                 := c_k0;
//      s.tt                 := c_tt;
      s.tPos               := c_tPos;
//      s.strm.next_out     := cs_next_out;
      s.strm.next_out.SetOffset(cs_next_out.Offset);
      s.strm.avail_out    := cs_avail_out;
      // end save */

      if c_tt <> nil then
        FreeAndNil(c_tt);
      if cs_next_out <> nil then
        FreeAndNil(cs_next_out);
   end;
   Result:=False;
end;


function BZ2_indexIntoF ( indx:Int32; var cftab:array of Int32 ):Int32;
var
   nb, na, mid:Int32;
begin
   nb := 0;
   na := 256;
   repeat
      mid := (nb + na) shr 1;
      if (indx >= cftab[mid]) then
        nb := mid
      else
        na := mid;
   until (na - nb) = 1;
   Result:=nb;
end;


//---------------------------------------------------*/
// Return  True iff data corruption is discovered.
//   Returns False if there is no problem.
//
function unRLE_obuf_to_output_SMALL ( s:DState ):boolean;
var
   k1:Byte;

procedure BZ_GET_SMALL(var cccc:Int32); overload;
begin
      cccc := BZ2_indexIntoF ( s.tPos, s.cftab );
//      s.tPos := GET_LL(s.tPos);
      s.tPos := ((UInt32(s.ll16.GetData(s.tPos))) or ((((UInt32(s.ll4.GetData((s.tPos) shr 1))) shr (((s.tPos) shl 2) and $4)) and $F) shl 16))
end;

procedure BZ_GET_SMALL2(var cccc:Byte); overload;
begin
      cccc := BZ2_indexIntoF ( s.tPos, s.cftab );
//      s.tPos := GET_LL(s.tPos);
      s.tPos := ((UInt32(s.ll16.GetData(s.tPos))) or ((((UInt32(s.ll4.GetData((s.tPos) shr 1))) shr (((s.tPos) shl 2) and $4)) and $F) shl 16))
end;

procedure BZ_RAND_UPD_MASK;
begin
    if (s.rNToGo = 0) then
    begin
       s.rNToGo := BZ2_rNums[s.rTPos];
       inc(s.rTPos);
       if (s.rTPos = 512) then s.rTPos := 0;
    end;
    dec(s.rNToGo);
end;


begin
   if (s.blockRandomised) then
   begin

      while (True) do
      begin
         // try to finish existing run */
         while (True) do
         begin
            if (s.strm.avail_out = 0) then
            begin
              Result:=False;
              exit;
            end;
            if (s.state_out_len = 0) then break;
            s.strm.next_out.SetAtPtr(s.state_out_ch);
            BZ_UPDATE_CRC ( s.calculatedBlockCRC, s.state_out_ch );
            dec(s.state_out_len);
            s.strm.next_out.IncOffset(1);
            dec(s.strm.avail_out);
            inc(s.strm.total_out_lo32);
            if (s.strm.total_out_lo32 = 0) then inc(s.strm.total_out_hi32);
         end;

         // can a new run be started? */
         if (s.nblock_used = s.save_nblock+1) then
            begin
              Result:=False;
              exit;
            end;

         // Only caused by corrupt data stream? */
         if (s.nblock_used > s.save_nblock+1) then
            begin
              Result:=True;
              exit;
            end;


         s.state_out_ch := s.k0;
         s.state_out_len := 1;
         BZ_GET_SMALL2(k1); BZ_RAND_UPD_MASK;
         k1 := k1 xor IfThenInt(s.rNToGo = 1, integer(1), 0); inc(s.nblock_used);
         if (s.nblock_used = s.save_nblock+1) then continue;
         if (k1 <> s.k0) then
         begin
            s.k0 := k1;
            continue;
         end;

         s.state_out_len := 2;
         BZ_GET_SMALL2(k1); BZ_RAND_UPD_MASK;
         k1 := k1 xor IfThenInt(s.rNToGo = 1, integer(1), 0); inc(s.nblock_used);
         if (s.nblock_used = s.save_nblock+1) then continue;
         if (k1 <> s.k0) then
         begin
            s.k0 := k1;
            continue;
         end;

         s.state_out_len := 3;
         BZ_GET_SMALL2(k1); BZ_RAND_UPD_MASK;
         k1 := k1 xor IfThenInt(s.rNToGo = 1, integer(1), 0); inc(s.nblock_used);
         if (s.nblock_used = s.save_nblock+1) then continue;
         if (k1 <> s.k0) then
         begin
            s.k0 := k1;
            continue;
         end;

         BZ_GET_SMALL2(k1); BZ_RAND_UPD_MASK;
         k1 := k1 xor  IfThenInt(s.rNToGo = 1, integer(1), 0); inc(s.nblock_used);
         s.state_out_len := k1 + 4;
         BZ_GET_SMALL(s.k0); BZ_RAND_UPD_MASK;
         s.k0 := s.k0 xor  IfThenInt(s.rNToGo = 1, integer(1), 0); inc(s.nblock_used);
      end
   end
   else
   begin
      while (True) do
      begin
         // try to finish existing run */
         while (True) do
         begin
            if (s.strm.avail_out = 0) then
            begin
              Result:=False;
              exit;
            end;

            if (s.state_out_len = 0) then  break;
            s.strm.next_out.SetAtPtr(s.state_out_ch);
            BZ_UPDATE_CRC ( s.calculatedBlockCRC, s.state_out_ch );
            dec(s.state_out_len);
            s.strm.next_out.IncOffset(1);
            dec(s.strm.avail_out);
            inc(s.strm.total_out_lo32);
            if (s.strm.total_out_lo32 = 0) then inc(s.strm.total_out_hi32);
         end;

         // can a new run be started? */
         if (s.nblock_used = s.save_nblock+1) then
            begin
              Result:=False;
              exit;
            end;

         // Only caused by corrupt data stream? */
         if (s.nblock_used > s.save_nblock+1) then
            begin
              Result:=True;
              exit;
            end;


         s.state_out_ch := s.k0;
         s.state_out_len := 1;
         BZ_GET_SMALL2(k1); inc(s.nblock_used);
         if (s.nblock_used = s.save_nblock+1) then
          continue;
         if (k1 <> s.k0) then
         begin
            s.k0 := k1;
            continue;
         end;

         s.state_out_len := 2;
         BZ_GET_SMALL2(k1); inc(s.nblock_used);
         if (s.nblock_used = s.save_nblock+1) then
          continue;
         if (k1 <> s.k0) then
         begin
            s.k0 := k1;
            continue;
         end;

         s.state_out_len := 3;
         BZ_GET_SMALL2(k1); inc(s.nblock_used);
         if (s.nblock_used = s.save_nblock+1) then
          continue;
         if (k1 <> s.k0) then
         begin
            s.k0 := k1;
            continue;
         end;

         BZ_GET_SMALL2(k1); inc(s.nblock_used);
         s.state_out_len := k1 + 4;
         BZ_GET_SMALL(s.k0); inc(s.nblock_used);
      end;

   end;
end;


//---------------------------------------------------*/
function BZ2_bzDecompress ( strm:bz_stream):Integer;
var
   corrupt:boolean;
   s:DState;
   r:Int32;
begin
   if (strm = nil) then
   begin
      Result:=BZ_PARAM_ERROR;
      exit;
   end;
   s := strm.state_d;
   if (s = nil) then
   begin
      Result:=BZ_PARAM_ERROR;
      exit;
   end;

   if (s.strm <> strm) then
   begin
      Result:=BZ_PARAM_ERROR;
      exit;
   end;

   while (True) do
   begin
      if (s.state = BZ_X_IDLE) then
      begin
            Result:=BZ_SEQUENCE_ERROR;
            exit;
      end;
      if (s.state = BZ_X_OUTPUT) then
      begin
         if (s.smallDecompress) then
            corrupt := unRLE_obuf_to_output_SMALL ( s )
         else
            corrupt := unRLE_obuf_to_output_FAST  ( s );
         if (corrupt) then
         begin
            Result:=BZ_DATA_ERROR;
            exit;
         end;
         if (s.nblock_used = s.save_nblock+1) and (s.state_out_len = 0) then
         begin
            // BZ_FINALISE_CRC
            s.calculatedBlockCRC:= not s.calculatedBlockCRC;
            if (s.verbosity >= 3) then
               Write (ErrOutput, Format(' {0x%08x, 0x%08x}', [s.storedBlockCRC,
                          s.calculatedBlockCRC]) );
            if (s.verbosity >= 2) then Write (ErrOutput, ']' );
            if (s.calculatedBlockCRC <> s.storedBlockCRC) then
            begin
               Result:=BZ_DATA_ERROR;
               exit;
            end;
            s.calculatedCombinedCRC
               := (s.calculatedCombinedCRC shl 1) or
                    (s.calculatedCombinedCRC shr 31);
            s.calculatedCombinedCRC := s.calculatedCombinedCRC xor s.calculatedBlockCRC;
            s.state := BZ_X_BLKHDR_1;
         end
         else
         begin
            Result:=BZ_OK;
            exit;
         end;
      end;
      if (s.state >= BZ_X_MAGIC_1) then
      begin
         r := BZ2_decompress ( s );
         if (r = BZ_STREAM_END) then
         begin
            if (s.verbosity >= 3) then
               Write (ErrOutput, Format(CRLF+'    combined CRCs: stored = 0x%08x, computed = 0x%08x'+CRLF,
                          [s.storedCombinedCRC, s.calculatedCombinedCRC]) );
            if (s.calculatedCombinedCRC <> s.storedCombinedCRC) then
            begin
               Result:=BZ_DATA_ERROR;
               exit;
            end;
           Result:=r;
           exit;
         end;
         if (s.state <> BZ_X_OUTPUT) then
         begin
           Result:=r;
           exit;
         end;
      end;
   end;

   AssertH ( false, 6001 );

   Result:=0;
end;


function BZ2_bzDecompressEnd( strm:bz_stream):integer;
var
   s:DState;
   i:integer;
begin
   if (strm = nil) then
   begin
      Result:=BZ_PARAM_ERROR;
      exit;
   end;
   s := strm.state_d;
   if (s = nil) then
   begin
      Result:=BZ_PARAM_ERROR;
      exit;
   end;

   if (s.strm <> strm) then
   begin
      Result:=BZ_PARAM_ERROR;
      exit;
   end;

   if (s.tt   <> nil) then s.tt.Free;//Destroy;
   if (s.ll16 <> nil) then s.ll16.Free;//Destroy;
   if (s.ll4  <> nil) then s.ll4.Free;//Destroy;
   for i := 1 to BZ_N_GROUPS do
   begin
    if s.len[i-1]<>nil then s.len[i-1].Free;//Destroy;
    if s.base[i-1]<>nil then s.base[i-1].Free;//Destroy;
    if s.limit[i-1]<>nil then s.limit[i-1].Free;//Destroy;
    if s.perm[i-1]<>nil then s.perm[i-1].Free;//Destroy;
   end;

   strm.state_d.Free;
   strm.state_d := nil;

   Result:=BZ_OK;
end;


procedure BZ_SETERR(var bzerror:Int32; bzf:bzFile;eee:Int32);
begin
   bzerror := eee;
   if (bzf <> nil) then bzf.lastErr := eee;
end;


function f_fileread(F:TOpenedFile;var Buf:TBytes;Count:integer):integer;
{$IFNDEF CLR}
var PBuf: PByte;
{$ENDIF}
begin
        {$IFNDEF CLR}
        PBuf:=@(Buf[0]);
        Result:= FileRead(F,PBuf^,Count);
        {$ELSE}
        //Result:= FileRead(F,Buf,Count);
        Result := F.Read(Buf, 0, Count);
        {$ENDIF}
end;

function f_filewrite(F:TOpenedFile;Buf:TBytes;Count:integer):integer;
{$IFNDEF CLR}
var PBuf:PByte;
{$ENDIF}
begin
        {$IFNDEF CLR}
        PBuf:=@(Buf[0]);
        Result:= FileWrite(F,PBuf^,Count);
        {$ELSE}
        //Result:= FileWrite(F,Buf,Count);
        F.Write(Buf, 0, Count);
        Result := Count;
        {$ENDIF}
end;


function f_myfeof ( F:TOpenedFile):boolean;
var
  c:TBytes;
  FPos:Int32;
  n:integer;
begin
   SetLength(c,1);
   FPos:=FileSeek(F,0,1);
   n:=f_FileRead(f,c,1 );
   if n<=0 then
      Result:=True
   else
   begin
      FileSeek(F,FPos,0);
      Result:=False;
   end;
end;





function BZ2_bzWriteOpen
                    ( var bzerror:Int32;
                      f:TOpenedFile;
                      blockSize100k,
                      verbosity,
                      workFactor:integer):BZFILE;
var
   ret:Int32;
   bzf:bzFile;
begin
   bzf:=nil;
   BZ_SETERR(bzerror,bzf,BZ_OK);

   if (f = NOFILE) or
       (blockSize100k < 1) or (blockSize100k > 9) or
       (workFactor < 0) or (workFactor > 250) or
       (verbosity < 0) or (verbosity > 4) then
   begin
      BZ_SETERR(bzerror,bzf,BZ_PARAM_ERROR);
      Result:=nil;
      exit;
   end;

   if (IoResult<>0) then
   begin
      BZ_SETERR(bzerror,bzf,BZ_IO_ERROR);
      Result:=nil;
      exit;
   end;

   bzf:=bzfile.Create;
   if (bzf = nil) then
   begin
      BZ_SETERR(bzerror,bzf,BZ_MEM_ERROR);
      Result:=nil;
      exit;
   end;

   BZ_SETERR(bzerror,bzf,BZ_OK);
   bzf.initialisedOk := False;
   bzf.bufN          := 0;
   bzf.handle        := f;
   bzf.writing       := True;
   bzf.strm := bz_stream.Create;
//   bzf.strm.opaque   := nil;

   if (workFactor = 0) then workFactor := 30;
   ret := BZ2_bzCompressInit ( bzf.strm, blockSize100k,
                              verbosity, workFactor );
   if (ret <> BZ_OK) then
   begin
      BZ_SETERR(bzerror,bzf,ret);
      FreeAndNil(bzf);
      Result:=nil;
      exit;
   end;

   bzf.strm.avail_in := 0;
   bzf.strm.next_in:=nil;
   bzf.strm.next_out:=nil;
   bzf.buf := TSBByteArray.Create(BZ_MAX_UNUSED+1);
   bzf.initialisedOk := True;
   Result:=bzf;
end;




procedure BZ2_bzWrite2
             ( var bzerror:Int32;
               b:BZFILE;
               buf:TSBByteArray;
               len:integer );
var
   n, n2, ret:Int32;
   bzf:bzFile;
   b_b:TBytes;
begin
   bzf := b;

   BZ_SETERR(bzerror,bzf,BZ_OK);
   if (bzf = nil) or (buf = nil) or (len < 0) then
   begin
      BZ_SETERR(bzerror,bzf,BZ_PARAM_ERROR);
      exit;
   end;
   if (not (bzf.writing)) then
   begin
      BZ_SETERR(bzerror,bzf,BZ_SEQUENCE_ERROR);
      exit;
   end;

   if (IoResult<>0) then
   begin
      BZ_SETERR(bzerror,bzf,BZ_IO_ERROR);
      exit;
   end;

   if (len = 0) then
   begin
      BZ_SETERR(bzerror,bzf,BZ_OK);
      exit;
   end;

   bzf.strm.avail_in := len;
   if bzf.strm.next_in<>nil then
        bzf.strm.next_in.Free;//Destroy;
   if bzf.strm.next_out<>nil then
        bzf.strm.next_out.Free;//Destroy;
   bzf.strm.next_in  := TSBByteArrayPointer.Create(buf,'buf');
   bzf.strm.next_out := TSBByteArrayPointer.Create(bzf.buf,'bzf.buf');

   while (True) do
   begin
      bzf.strm.avail_out := BZ_MAX_UNUSED;
      bzf.strm.next_out.Offset:=0;
      bzf.strm.next_out.Base_Array := bzf.buf;
      ret := BZ2_bzCompress ( bzf.strm, BZ_RUN );
      if (ret <> BZ_RUN_OK) then
      begin
         BZ_SETERR(bzerror,bzf,ret);
         exit;
      end;

      if (bzf.strm.avail_out < BZ_MAX_UNUSED) then
      begin
         SetLength(b_b,BZ_MAX_UNUSED - bzf.strm.avail_out);
         for n := 1 to BZ_MAX_UNUSED - bzf.strm.avail_out do
             b_b[n-1] := bzf.buf.GetData(n-1);
         n2:=f_filewrite(bzf.handle,b_b,BZ_MAX_UNUSED - bzf.strm.avail_out);
         if (IOResult<>0) or ((BZ_MAX_UNUSED - bzf.strm.avail_out) <> n2)  then
         begin
            BZ_SETERR(bzerror,bzf, BZ_IO_ERROR);
            exit;
         end;
      end;

      if (bzf.strm.avail_in = 0) then
      begin
         BZ_SETERR(bzerror,bzf, BZ_OK);
         exit;
      end;
   end;
end;



procedure BZ2_bzWriteClose64
                  ( var bzerror:Int32;
                    b:BZFILE;
                    abandon:boolean;
                    var nbytes_in_lo32,
                    nbytes_in_hi32,
                    nbytes_out_lo32,
                    nbytes_out_hi32:UInt32 );
var
   n, n2, ret:Int32;
   bzf:bzFile;
   b_b:TBytes;
begin
   bzf := b;

   if (bzf = nil) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_OK);
      exit;
   end;
   if (not (bzf.writing)) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_SEQUENCE_ERROR);
      exit;
   end;

   if (IOResult<>0) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_IO_ERROR);
      exit;
   end;

    nbytes_in_lo32 := 0;
    nbytes_in_hi32 := 0;
    nbytes_out_lo32 := 0;
    nbytes_out_hi32 := 0;

   if bzf.strm.next_out=nil then
        bzf.strm.next_out:=TSBByteArrayPointer.Create(bzf.buf,'bzf.buf')
   else
   begin
        bzf.strm.next_out.Base_Array:=bzf.buf;
        bzf.strm.next_out.Name:='bzf.buf';
        bzf.strm.next_out.Offset := 0;
   end;
   if (not abandon) and (bzf.lastErr = BZ_OK) then
   begin
      while (True) do
      begin
         bzf.strm.avail_out := BZ_MAX_UNUSED;
         bzf.strm.next_out.Offset :=0;
         ret := BZ2_bzCompress ( bzf.strm, BZ_FINISH );
         if (ret <> BZ_FINISH_OK) and (ret <> BZ_STREAM_END) then
         begin
            BZ_SETERR(bzerror,bzf,ret);
            exit;
         end;

         if (bzf.strm.avail_out < BZ_MAX_UNUSED) then
         begin
            SetLength(b_b,BZ_MAX_UNUSED - bzf.strm.avail_out);
            for n := 1 to BZ_MAX_UNUSED - bzf.strm.avail_out do
                b_b[n-1] := bzf.buf.GetData(n-1);
            n2:=f_filewrite(bzf.handle,b_b,BZ_MAX_UNUSED - bzf.strm.avail_out);
            if (IOResult<>0) or ((BZ_MAX_UNUSED - bzf.strm.avail_out) <> n2)  then
            begin
               BZ_SETERR(bzerror,bzf, BZ_IO_ERROR);
               exit;
            end;
         end;

         if (ret = BZ_STREAM_END) then break;
      end;
   end;

      nbytes_in_lo32 := bzf.strm.total_in_lo32;
      nbytes_in_hi32 := bzf.strm.total_in_hi32;
      nbytes_out_lo32 := bzf.strm.total_out_lo32;
      nbytes_out_hi32 := bzf.strm.total_out_hi32;

   BZ_SETERR(bzerror,bzf, BZ_OK);
   BZ2_bzCompressEnd ( bzf.strm );
   FreeAndNil(bzf);
end;



procedure BZ2_bzWriteClose
                  ( var bzerror:Int32;
                    b:BZFILE;
                    abandon:boolean;
                    var nbytes_in,
                    nbytes_out:UInt32 );
var tmp1,tmp2:UInt32;
begin
   tmp1:=0;
   tmp2:=0;
   BZ2_bzWriteClose64 ( bzerror, b, abandon,
                        nbytes_in, tmp1, nbytes_out, tmp2 );
end;



function BZ2_bzReadOpen
                   ( var bzerror:Int32;
                     f:TOpenedFile;
                     verbosity:Int32;
                     small:boolean;
                     var unused:array  of Byte;
                     nUnused:integer ):BZFILE;
var
   bzf:bzFile;
   ret:integer;
   pos_unused:integer;
begin

   bzf := nil;
   pos_unused:=0;
   BZ_SETERR(bzerror,bzf, BZ_OK);

   if (f = NOFILE) or
       ((verbosity < 0) or (verbosity > 4)) or
       ((Length(unused) = 0) and (nUnused <> 0)) or
       ((Length(unused) <> 0) and ((nUnused < 0) or (nUnused > BZ_MAX_UNUSED))) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_PARAM_ERROR);
      Result:=nil;
      exit;
   end;

   if (IOResult<>0) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_IO_ERROR);
      Result:=nil;
      exit;
   end;

   bzf:=bzfile.Create;
   if (bzf = nil) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_MEM_ERROR);
      Result:=nil;
      exit;
   end;


   BZ_SETERR(bzerror,bzf, BZ_OK);

   bzf.initialisedOk := False;
   bzf.handle        := f;
   bzf.bufN          := 0;
   bzf.writing       := False;
//   bzf.strm.opaque   := nil;
   bzf.buf           := TSBByteArray.Create(BZ_MAX_UNUSED+1);

   while (nUnused > 0) do
   begin
      bzf.buf.SetData(unused[pos_unused],bzf.bufN); inc(bzf.bufN);
      inc(pos_unused);
      dec(nUnused);
   end;

   bzf.strm := bz_stream.Create;
   ret := BZ2_bzDecompressInit ( bzf.strm, verbosity, small );
   if (ret <> BZ_OK) then
   begin
      BZ_SETERR(bzerror,bzf, ret);
      FreeAndNil(bzf);
      Result:=nil;
      exit;
   end;

   bzf.strm.avail_in := bzf.bufN;
   bzf.strm.next_in  := TSBByteArrayPointer.Create(bzf.buf,'bzf.buf');
   bzf.strm.next_out:=nil;

   bzf.initialisedOk := True;
   Result:=bzf;
end;



procedure BZ2_bzReadClose( var bzerror:Int32; b:BZFILE);
var
   bzf:bzFile;
begin
   if b=nil then
        exit;

   bzf := b;
   BZ_SETERR(bzerror,bzf, BZ_OK);
   if (bzf = nil) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_OK);
      exit;
   end;

   if (bzf.writing) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_SEQUENCE_ERROR);
      exit;
   end;

   if (bzf.initialisedOk) then
      BZ2_bzDecompressEnd ( bzf.strm);
   FreeAndNil(bzf);
end;

function BZ2_bzRead2
           ( var bzerror:Int32;
             b:BZFILE;
             buf:TSBByteArray;
             len:integer ):integer;
var
   n, ret:Int32;
   i:integer;
   bzf:bzFile;
   bbuf:TBytes;
 begin
   bzf := b;
   BZ_SETERR(bzerror,bzf, BZ_OK);

   if (bzf = nil) or (buf = nil) or (len < 0) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_PARAM_ERROR);
      Result:=0;
      exit;
   end;

   if (bzf.writing) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_SEQUENCE_ERROR);
      Result:=0;
      exit;
   end;

   if (len = 0) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_OK);
      Result:=0;
      exit;
   end;


   bzf.strm.avail_out := len;
   bzf.strm.next_out := TSBByteArrayPointer.Create(buf,'buf');


   while (True) do
   begin
      if (IOResult<>0) then
      begin
         BZ_SETERR(bzerror,bzf, BZ_IO_ERROR);
         Result:=0;
         exit;
      end;

      if (bzf.strm.avail_in = 0) and (not f_myfeof(bzf.handle)) then
      begin
         SetLength(bbuf,BZ_MAX_UNUSED);
         n:=f_fileread(bzf.handle,bbuf, BZ_MAX_UNUSED);
         for i := 1 to n do
                bzf.buf.SetData(bbuf[i-1],i-1);

         if (IOResult<>0) then
         begin
            BZ_SETERR(bzerror,bzf, BZ_IO_ERROR);
            Result:=0;
            exit;
         end;
         bzf.bufN := n;
         bzf.strm.avail_in := bzf.bufN;
         if bzf.strm.next_in<>nil then
                bzf.strm.next_in.Free;//Destroy;
         bzf.strm.next_in := TSBByteArrayPointer.Create(bzf.buf,'bzf.buf');
      end;

      ret := BZ2_bzDecompress ( bzf.strm );

      if (ret <> BZ_OK) and (ret <> BZ_STREAM_END) then
      begin
         BZ_SETERR(bzerror,bzf, ret);
         Result:=0;
         exit;
      end;

      if (ret = BZ_OK) and f_myfeof(bzf.handle) and
          (bzf.strm.avail_in = 0) and (bzf.strm.avail_out > 0) then
      begin
         BZ_SETERR(bzerror,bzf, BZ_UNEXPECTED_EOF);
         Result:=0;
         exit;
      end;

      if (ret = BZ_STREAM_END) then
      begin
         BZ_SETERR(bzerror,bzf, BZ_STREAM_END);
         Result:=len - bzf.strm.avail_out;
         exit;
      end;
      if (bzf.strm.avail_out = 0) then
      begin
         BZ_SETERR(bzerror,bzf, BZ_OK);
         Result:=len;
         exit;
      end;

   end;
   Result:=0;
end;


procedure BZ2_bzReadGetUnused
                     ( var bzerror:Int32;
                       b:BZFILE;
                       var unused:array of Byte;
                       var nUnused:Int32);
var
   bzf:bzFile;
begin
   bzf := b;
   if (bzf = nil) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_PARAM_ERROR);
      exit;
   end;
   if (bzf.lastErr <> BZ_STREAM_END) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_SEQUENCE_ERROR);
      exit;
   end;
   if (Length(unused) = 0) or (nUnused = -1) then
   begin
      BZ_SETERR(bzerror,bzf, BZ_PARAM_ERROR);
      exit;
   end;

   BZ_SETERR(bzerror,bzf, BZ_OK);
   nUnused := bzf.strm.avail_in;
   unused[0] := bzf.strm.next_in.GetAtPtr;
end;

//---------------------------------------------------*/
//--- Misc convenience stuff                      ---*/
//---------------------------------------------------*/

function BZ2_bzBuffToBuffCompress
                         ( dest:TSBByteArray;
                           var destLen:UInt32;
                           source:TSBByteArray;
                           sourceLen:UInt32;
                           blockSize100k,
                           verbosity,
                           workFactor:integer):integer;
var
   strm:bz_stream;
   ret:integer;
begin
   if (dest = nil) or
       (source = nil) or
       (blockSize100k < 1) or (blockSize100k > 9) or
       (verbosity < 0) or (verbosity > 4) or
       (workFactor < 0) or (workFactor > 250) then
   begin
      Result:=BZ_PARAM_ERROR;
      exit;
   end;

   if (workFactor = 0) then workFactor := 30;
   strm := bz_stream.Create;
   ret := BZ2_bzCompressInit ( strm, blockSize100k,
                              verbosity, workFactor );
   if (ret <> BZ_OK) then
   begin
      Result:=ret;
      exit;
   end;

   strm.next_in:=TSBByteArrayPointer.Create(source,'source');
   strm.next_out:=TSBByteArrayPointer.Create(dest,'dest');
   strm.avail_in := sourceLen;
   strm.avail_out := destLen;

   ret := BZ2_bzCompress ( strm, BZ_FINISH );
   if (ret = BZ_FINISH_OK) then
   begin
     BZ2_bzCompressEnd ( strm );
     Result:=BZ_OUTBUFF_FULL;
     exit;
   end;

   if (ret <> BZ_STREAM_END) then
   begin
     BZ2_bzCompressEnd ( strm );
     Result:=ret;
     exit;
   end;

   destLen := destLen - cardinal(strm.avail_out);
   BZ2_bzCompressEnd ( strm );
   Result:=BZ_OK;
end;



function BZ2_bzBuffToBuffDecompress
                           ( dest:TSBByteArray;
                             var destLen:UInt32;
                             source:TSBByteArray;
                             sourceLen:UInt32;
                             small:boolean;
                             verbosity:Int32):integer;
var
   strm:bz_stream;
   ret:integer;
begin
   if (dest = nil) or
       (source = nil) or
       (verbosity < 0) or (verbosity > 4) then
   begin
      Result:=BZ_PARAM_ERROR;
      exit;
   end;

   strm := bz_stream.Create;
   ret := BZ2_bzDecompressInit ( strm, verbosity, small );
   if (ret <> BZ_OK) then
   begin
    Result:=ret;
    exit;
   end;

   strm.next_in:=TSBByteArrayPointer.Create(source,'source');
   strm.next_out:=TSBByteArrayPointer.Create(dest,'dest');
   strm.avail_in := sourceLen;
   strm.avail_out := destLen;

   ret := BZ2_bzDecompress ( strm );
   if (ret = BZ_OK) then
   begin
       BZ2_bzDecompressEnd ( strm );
       if (strm.avail_out > 0) then
          Result:=BZ_UNEXPECTED_EOF
       else
          Result:=BZ_OUTBUFF_FULL;
       exit;
   end;

   if (ret <> BZ_STREAM_END) then
   begin
     BZ2_bzDecompressEnd ( strm );
     Result:=ret;
     exit;
   end;

   destLen := destLen - cardinal(strm.avail_out);
   BZ2_bzDecompressEnd ( strm );
   Result:=BZ_OK;

end;


function bzopen_or_bzdopen
               ( path:string;
                 fd:TOpenedFile;
                 mode:string;
                 open_mode:integer):BZFILE;
var
   bzerr:integer;
   unused: array [0..BZ_MAX_UNUSED] of Byte;
   blockSize100k,writing:integer;
   fp:TOpenedFile;
   i:integer;
   bzfp:BZFILE;
   verbosity,   workFactor,   nUnused:integer;
   smallMode:boolean;
begin
   blockSize100k := 9;
   writing       := 0;
   //fp           := NOFILE;
   //bzfp         := nil;
   verbosity     := 0;
   workFactor    := 30;
   smallMode     := false;
   nUnused       := 0;


   if (mode = '') then
   begin
    Result:=nil;
    exit;
   end;
   i:=1;
   while (i<=Length(Mode)) do
   begin
      case (mode[i]) of
      'r':writing := 0;
      'w':writing := 1;
      's':smallMode := true;
      else
         if (mode[i] >='0') and (mode[i] <='9') then
            blockSize100k := Ord(mode[i])-BZ_HDR_0;
      end;
      inc(i);
   end;

      if (path='')  then
        If writing=1 then
                {$IFNDEF CLR}
                fp := TTextRec(Output).Handle
                {$ELSE}
                //fp := FileOpen('con',fmOpenWrite)
                fp := FileStream.Create('con', FileMode.Open, FileAccess.Write)
                {$ENDIF}
        else
                {$IFNDEF CLR}
                fp := TTextRec(Input).Handle
                {$ELSE}
                //fp := FileOpen('con',fmOpenWrite)
                fp := FileStream.Create('con', FileMode.Open, FileAccess.Write)
                {$ENDIF}

      else
        If writing=1 then
                fp := FileOpen(path,fmOpenWrite)
        else
                fp := FileOpen(path,fmOpenRead);
   if (fp = NOFILE) then
   begin
      Result:=nil;
      exit;
   end;

   if (writing=1) then
   begin
      if (blockSize100k < 1) then blockSize100k := 1;
      if (blockSize100k > 9) then blockSize100k := 9;
      bzfp := BZ2_bzWriteOpen(bzerr,fp,blockSize100k,
                             verbosity,workFactor);
   end
   else
      bzfp := BZ2_bzReadOpen(bzerr,fp,verbosity,smallMode,
                            unused,nUnused);
   if (bzfp = nil) then
   begin
      FileClose(fp);
      Result:=nil;
      exit;
   end;
   Result:=bzfp;
end;


function BZ2_bzopen
               ( path,
                 mode:string ):BZFILE;
begin
   Result:=bzopen_or_bzdopen(path,NOFILE,mode,0);
end;



function BZ2_bzdopen
               ( fd:TOpenedFile;
                 mode:string ):BZFILE;
begin
   Result:=bzopen_or_bzdopen('',fd,mode,1);
end;


function BZ2_bzread(b:BZFILE; buf:TSBByteArray; len:integer ):integer;
var
   bzerr, nread:integer;
begin
   if b.lastErr = BZ_STREAM_END then
   begin
      Result:=0;
      exit;
   end;
   nread := BZ2_bzRead2(bzerr,b,buf,len);
   if (bzerr = BZ_OK) or (bzerr = BZ_STREAM_END) then
      Result:=nread
   else
      Result:=-1;
end;



function BZ2_bzwrite(b:BZFILE;buf:TSBByteArray; len:integer ):integer;
var
   bzerr:integer;
begin
   BZ2_bzWrite2(bzerr,b,buf,len);
   if(bzerr = BZ_OK)then
      result:=len
   else
      Result:=-1;
end;                                     

function BZ2_bzflush(b:BZFILE):integer;
begin
   // do nothing now... */
   Result:=0;
end;



procedure BZ2_bzclose(b:BZFILE);
var
   bzerr,tmp:integer;
   fp :TOpenedFile;
   tmp1,tmp2:UInt32;
begin
   if (b=nil) then
      exit;

   tmp1:=0;
   tmp2:=0;
   fp := b.handle;
   if b.writing then
   begin
      BZ2_bzWriteClose(bzerr,b,false,tmp1,tmp1);
      tmp:=-1;
      if(bzerr <> BZ_OK) then
         BZ2_bzWriteClose(tmp,b,true,tmp1,tmp2);
   end
   else
      BZ2_bzReadClose(bzerr,b);
   FileClose(fp);
end;

const bzerrorstrings:array [0..15] of string =
(
       'OK'
      ,'SEQUENCE_ERROR'
      ,'PARAM_ERROR'
      ,'MEM_ERROR'
      ,'DATA_ERROR'
      ,'DATA_ERROR_MAGIC'
      ,'IO_ERROR'
      ,'UNEXPECTED_EOF'
      ,'OUTBUFF_FULL'
      ,'CONFIG_ERROR'
      ,'???'
      ,'???'
      ,'???'
      ,'???'
      ,'???'
      ,'???'
);


function BZ2_bzerror (b:BZFILE; var errnum:Int32):string;
var
  err:integer;
begin
   err := b.lastErr;

   if(err>0)  then err := 0;
   errnum := err;
   Result:= bzerrorstrings[err - 1];
end;
end.
