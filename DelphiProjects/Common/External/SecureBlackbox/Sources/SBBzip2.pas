(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBBzip2;

interface

uses
  SBBzlib,
  SBBzip2Types,
  SBBzip2Utils,
  SysUtils,
  SBUtils;

type

  TBzip2Context = record
    strm : bz_stream;
  end;

  EElBzip2Error =  class(Exception{ESecureBlackboxError});

  TSBBzip2OutputFunc = function(Buffer: pointer; Size: integer; Param: pointer): boolean of object;

procedure InitializeCompression(var Context : TBzip2Context; CompressionLevel : integer); 
procedure InitializeDecompression(var Context : TBzip2Context); 
procedure Compress(var Context : TBzip2Context; InBuffer : pointer; InSize : cardinal;
  OutputFunc : TSBBzip2OutputFunc; Param : pointer);
procedure Decompress(var Context : TBzip2Context; InBuffer : pointer; InSize : cardinal;
  OutputFunc : TSBBzip2OutputFunc; Param : pointer);
procedure FinalizeCompression(var Context : TBzip2Context; OutputFunc: TSBBzip2OutputFunc;
  Param : pointer); 
procedure FinalizeDecompression(var Context : TBzip2Context; OutputFunc: TSBBzip2OutputFunc;
  Param : pointer); 

implementation

resourcestring
  SBzip2InitializationFailed = 'Bzip2 initialization failed';
  SCompressionFailed = 'Compression failed, error %d';
  SDecompressionFailed = 'Decompression failed, error %d';

procedure InitializeCompression(var Context : TBzip2Context; CompressionLevel : integer);
var
  R : integer;
begin
  //if not Assigned(Context.strm) then
  Context.strm := bz_stream.Create;
  R := BZ2_bzCompressInit(Context.strm, CompressionLevel, 0, 30);
  if R <> 0 then
    raise EElBzip2Error.Create(SBzip2InitializationFailed);
end;

procedure InitializeDecompression(var Context : TBzip2Context);
begin
  //if not Assigned(Context.strm) then
  Context.strm := bz_stream.Create;
  BZ2_bzDecompressInit(Context.strm, 0, false);
end;

procedure Compress(var Context : TBzip2Context; InBuffer : pointer; InSize : cardinal;
  OutputFunc : TSBBzip2OutputFunc; Param : pointer);
var
  InBuf, OutBuf : TSBByteArray;
  InBufPtr, OutBufPtr : TSBByteArrayPointer;
  OutBuffer : ByteArray;
  OutBufSize : integer;
  Sz : integer;
  I : integer;
begin
  InBuf := TSBByteArray.Create(InSize);
  OutBufSize := InSize;
  OutBuf := TSBByteArray.Create(OutBufSize);
  InBufPtr := TSBByteArrayPointer.Create(InBuf, 'source');
  OutBufPtr := TSBByteArrayPointer.Create(OutBuf, 'dest');
  try
    // copying input data to a temporary buffer
    for I := 0 to InSize - 1 do
      InBuf.SetData(PByteArray(InBuffer)[I], I);

    // setting up bzip2 stream properties
    Context.strm.next_in := InBufPtr;
    Context.strm.avail_in := InSize;

    // compressing input data
    repeat
      Context.strm.next_out := OutBufPtr;
      Context.strm.next_out.Offset := 0;
      Context.strm.avail_out := OutBufSize;

      Sz := BZ2_bzCompress(Context.strm, BZ_RUN);
      if Sz = BZ_RUN_OK then
      begin
        if OutBufSize - Context.strm.avail_out > 0 then
        begin
          SetLength(OutBuffer, OutBufSize - Context.strm.avail_out);
          for I := 0 to OutBufSize - Context.strm.avail_out - 1 do
            OutBuffer[I] := OutBuf.GetData(I);
          if not OutputFunc(@OutBuffer[0], OutBufSize - Context.strm.avail_out, Param) then
            Break;
        end;
      end
      else
        raise Exception.CreateFmt(SCompressionFailed, [Sz]);
    until (Context.strm.avail_out <> 0);
  finally
    FreeAndNil(InBufPtr);
    FreeAndNil(OutBufPtr);
    FreeAndNil(InBuf);
    FreeAndNil(OutBuf);
  end;
end;

procedure FinalizeCompression(var Context : TBzip2Context; OutputFunc: TSBBzip2OutputFunc;
  Param : pointer);
var
  OutBuf : TSBByteArray;
  OutBufPtr : TSBByteArrayPointer;
  Sz : integer;
  I : integer;
  OutBuffer : ByteArray;
const
  OUTBUF_SIZE = 65536;
begin
  OutBuf := TSBByteArray.Create(OUTBUF_SIZE);
  OutBufPtr := TSBByteArrayPointer.Create(OutBuf, 'dest');
  try
    Context.strm.next_in := nil;
    Context.strm.avail_in := 0;
    repeat
      Context.strm.next_out := OutBufPtr;
      Context.strm.next_out.Offset := 0;
      Context.strm.avail_out := OUTBUF_SIZE;
      Sz := BZ2_bzCompress(Context.strm, BZ_FINISH);
      if (Sz = BZ_STREAM_END) or (Sz = BZ_FINISH_OK) then
      begin
        if OUTBUF_SIZE - Context.strm.avail_out > 0 then
        begin
          SetLength(OutBuffer, OUTBUF_SIZE - Context.strm.avail_out);
          for I := 0 to OUTBUF_SIZE - Context.strm.avail_out - 1 do
            OutBuffer[I] := OutBuf.GetData(I);
          if not OutputFunc(@OutBuffer[0], OUTBUF_SIZE - Context.strm.avail_out, Param) then
            Break;
        end;
      end
      else
        raise Exception.CreateFmt(SCompressionFailed, [Sz]);
    until (Context.strm.avail_out <> 0);
    BZ2_bzCompressEnd(Context.strm);
    FreeAndNil(Context.strm);
  finally
    FreeAndNil(OutBufPtr);
    FreeAndNil(OutBuf);
  end;
end;

procedure Decompress(var Context : TBzip2Context; InBuffer : pointer; InSize : cardinal;
  OutputFunc : TSBBzip2OutputFunc; Param : pointer);
var
  InBuf, OutBuf : TSBByteArray;
  InBufPtr, OutBufPtr : TSBByteArrayPointer;
  OutBufSize : integer;
  Sz : integer;
  I : integer;
  OutBuffer : ByteArray;
begin
  InBuf := TSBByteArray.Create(InSize);
  OutBufSize := InSize;
  OutBuf := TSBByteArray.Create(OutBufSize);
  InBufPtr := TSBByteArrayPointer.Create(InBuf, 'source');
  OutBufPtr := TSBByteArrayPointer.Create(OutBuf, 'dest');
  try
    // copying input data to a temporary buffer
    for I := 0 to InSize - 1 do
      InBuf.SetData(PByteArray(InBuffer)[I], I);

    // setting up bzip2 stream properties
    Context.strm.next_in := InBufPtr;
    Context.strm.avail_in := InSize;

    // decompressing input data
    repeat
      Context.strm.next_out := OutBufPtr;
      Context.strm.next_out.Offset := 0;
      Context.strm.avail_out := OutBufSize;
      Sz := BZ2_bzDecompress(Context.strm);
      if (Sz = BZ_RUN_OK) or (Sz = BZ_STREAM_END) or (Sz = BZ_OK) then
      begin
        if OutBufSize - Context.strm.avail_out > 0 then
        begin
          SetLength(OutBuffer, OutBufSize - Context.strm.avail_out);
          for I := 0 to OutBufSize - Context.strm.avail_out - 1 do
            OutBuffer[I] := OutBuf.GetData(I);
          OutputFunc(@OutBuffer[0], OutBufSize - Context.strm.avail_out, Param);
        end;
      end
      else
        raise Exception.CreateFmt(SDecompressionFailed, [Sz]);
      if Sz = BZ_STREAM_END then
        Break;
    until Context.strm.avail_out <> 0;
  finally
    FreeAndNil(InBufPtr);
    FreeAndNil(OutBufPtr);
    FreeAndNil(InBuf);
    FreeAndNil(OutBuf);
  end;
end;

procedure FinalizeDecompression(var Context : TBzip2Context; OutputFunc: TSBBzip2OutputFunc;
  Param : pointer);
begin
  BZ2_bzDecompressEnd(Context.strm);
  FreeAndNil(Context.strm);
end;

end.
