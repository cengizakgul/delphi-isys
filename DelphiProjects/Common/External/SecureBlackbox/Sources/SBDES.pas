
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBDES;

interface

uses
  Classes,
  SysUtils,
  SBUtils;


const TDESBufferSize = 8;
      TDESKeySize = 8;
      TDESExpandedKeySize = 16 * 48;
      T3DESBufferSize = 8;
      T3DESKeySize = 24;
      T3DESExpandedKeySize = 16 * 48 * 3;

type
  TDESBuffer =  array[0..7] of byte;
  TDESKey =  array[0..7] of byte;
  TDESExpandedKey = array[0..15, 0..47] of byte;
  TDESExpandedKeyEx = array[0..191] of longword;
  
  PDESBuffer = ^TDESBuffer;
  PDESKey = ^TDESKey;
  PDESExpandedKey = ^TDESExpandedKey;

  T3DESBuffer =  array[0..7] of byte;
  T3DESKey =  array[0..23] of byte;
  T3DESExpandedKey = array[0..2] of TDESExpandedKey;
    //array [0..47, 0..47] of byte;
  P3DESBuffer = ^T3DESBuffer;
  P3DESKey = ^T3DESKey;
  P3DESExpandedKey = ^T3DESExpandedKey;

  TDESContext = packed record
    ExpandedKey: TDESExpandedKey;
    ExpandedKeyEx : TDESExpandedKeyEx;
    Vector: TDESBuffer;
    Tail: TDESBuffer;
    TailLen: cardinal;
  end;

  T3DESContext = packed record
    ExpandedKey: T3DESExpandedKey;
    Vector: T3DESBuffer;
    Tail: T3DESBuffer;
    TailLen: cardinal;
  end;

// Special procedures for DES and 3DES
procedure IPERM(var left, right: Cardinal); 
procedure FPERM(var left, right: Cardinal); 
procedure RawProcessBlock(var l_, r_: Cardinal; ExpandedKey: TDESExpandedKey);
procedure GetBlockBigEndian(block: PLongWordArray; var a, b: Cardinal);
procedure PutBlockBigEndian(block: PLongWordArray; a, b: Cardinal);

// Key expansion routine
procedure ExpandKeyForEncryption(const Key: TDESKey;
  out ExpandedKey: TDESExpandedKey); 
procedure ExpandKeyForDecryption(const Key: TDESKey;
  out ExpandedKey: TDESExpandedKey); 

// Block processing routines
procedure Encrypt(const InBuf: TDESBuffer; const ExpandedKey: TDESExpandedKey;
  out OutBuf: TDESBuffer); procedure Decrypt(const InBuf: TDESBuffer; const ExpandedKey: TDESExpandedKey;
  out OutBuf: TDESBuffer); 

// Memory processing routines

// ECB Mode
function EncryptECB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TDESExpandedKey): boolean; overload;
function DecryptECB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TDESExpandedKey): boolean; overload;

// CBC Mode
function EncryptCBC(InBuffer: pointer; InSize: cardinal; OutBuffer: pointer;
  var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer): boolean; overload;
function DecryptCBC(InBuffer: pointer; InSize: cardinal; OutBuffer: pointer;
  var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer): boolean; overload;

// CFB Mode
function EncryptCFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer): boolean; overload;
function DecryptCFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer): boolean; overload;
// CFB Mode w/ Step
function EncryptCFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer; const Step: byte): boolean; overload;
function DecryptCFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer; const Step: byte): boolean; overload;
// OFB Mode
function EncryptOFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer): boolean; overload;
function DecryptOFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer): boolean; overload;
// OFB Mode w/ Step
function EncryptOFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer; const Step: byte): boolean; overload;
function DecryptOFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer; const Step: byte): boolean; overload;

// Chunks processing routines
// ECB Mode
procedure InitializeEncryptionECB(var Context: TDESContext; Key: TDESKey);
procedure InitializeDecryptionECB(var Context: TDESContext; Key: TDESKey);
function EncryptECB(var Context: TDESContext; Chunk: pointer; ChunkSize:
  cardinal;
  OutBuf: pointer; var OutSize: cardinal): boolean; overload;

function DecryptECB(var Context: TDESContext; Chunk: pointer; ChunkSize:
  cardinal;
  OutBuf: pointer; var OutSize: cardinal): boolean; overload;

function FinalizeEncryptionECB(var Context: TDESContext;
  OutBuf: pointer; var OutSize: cardinal): boolean;
function FinalizeDecryptionECB(var Context: TDESContext;
  OutBuf: pointer; var OutSize: cardinal): boolean;

// CBC Mode
procedure InitializeEncryptionCBC(var Context: TDESContext; const Key: TDESKey;
  const InitVector: TDESBuffer);
procedure InitializeDecryptionCBC(var Context: TDESContext; Key: TDESKey;
  InitVector: TDESBuffer);

function EncryptCBC(var Context: TDESContext; Chunk: pointer; ChunkSize:
  cardinal;
  OutBuf: pointer; var OutSize: cardinal): boolean; overload;

function DecryptCBC(var Context: TDESContext; Chunk: pointer; ChunkSize:
  cardinal;
  OutBuf: pointer; var OutSize: cardinal): boolean; overload;

function FinalizeEncryptionCBC(var Context: TDESContext; OutBuf: pointer;
  var OutSize: cardinal): boolean;


// Stream processing routines
// ECB Mode
procedure EncryptStreamECB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; Dest: TStream);
procedure DecryptStreamECB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; Dest: TStream);
// CBC Mode
procedure EncryptStreamCBC(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream);
procedure DecryptStreamCBC(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream);
// CFB Mode
procedure EncryptStreamCFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream); overload;
procedure DecryptStreamCFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream); overload;
// CFB Mode w/ Step
procedure EncryptStreamCFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream; const Step: byte); overload;
procedure DecryptStreamCFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream; const Step: byte); overload;
// OFB Mode
procedure EncryptStreamOFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream); overload;
procedure DecryptStreamOFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream); overload;
// OFB Mode w/ Step
procedure EncryptStreamOFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream; const Step: byte); overload;
procedure DecryptStreamOFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream; const Step: byte); overload;


implementation


const
  // permuted choice table (key) ported from C++
  PC1: array[0..55] of byte =  (
    57, 49, 41, 33, 25, 17, 9,
    1, 58, 50, 42, 34, 26, 18,
    10, 2, 59, 51, 43, 35, 27,
    19, 11, 3, 60, 52, 44, 36,

    63, 55, 47, 39, 31, 23, 15,
    7, 62, 54, 46, 38, 30, 22,
    14, 6, 61, 53, 45, 37, 29,
    21, 13, 5, 28, 20, 12, 4
     );

  // permuted choice key (table)
  PC2: array[0..47] of byte =  (
    14, 17, 11, 24, 1, 5,
    3, 28, 15, 6, 21, 10,
    23, 19, 12, 4, 26, 8,
    16, 7, 27, 20, 13, 2,
    41, 52, 31, 37, 47, 55,
    30, 40, 51, 45, 33, 48,
    44, 49, 39, 56, 34, 53,
    46, 42, 50, 36, 29, 32 );

  // bit 0 is left-most in byte
  bytebit: array[0..7] of cardinal =  (
    {-$0100,} $0080, $040, $020, $010, $08, $04, $02, $01 );

  // number left rotations of pc1 */
  totrot: array[0..15] of byte =  (
    1, 2, 4, 6, 8, 10, 12, 14, 15, 17, 19, 21, 23, 25, 27, 28 );

  {$ifndef NET_CF_1_0}
  SpBox: array[0..7, 0..63] of Cardinal =  (
  {$else}
  SpBox: array of array of Cardinal = [
  {$endif}
     (
    $01010400, $00000000, $00010000, $01010404, $01010004, $00010404, $00000004,
      $00010000,
    $00000400, $01010400, $01010404, $00000400, $01000404, $01010004, $01000000,
      $00000004,
    $00000404, $01000400, $01000400, $00010400, $00010400, $01010000, $01010000,
      $01000404,
    $00010004, $01000004, $01000004, $00010004, $00000000, $00000404, $00010404,
      $01000000,
    $00010000, $01010404, $00000004, $01010000, $01010400, $01000000, $01000000,
      $00000400,
    $01010004, $00010000, $00010400, $01000004, $00000400, $00000004, $01000404,
      $00010404,
    $01010404, $00010004, $01010000, $01000404, $01000004, $00000404, $00010404,
      $01010400,
    $00000404, $01000400, $01000400, $00000000, $00010004, $00010400, $00000000,
      $01010004 ),
     (
    $80108020, $80008000, $00008000, $00108020, $00100000, $00000020, $80100020,
      $80008020,
    $80000020, $80108020, $80108000, $80000000, $80008000, $00100000, $00000020,
      $80100020,
    $00108000, $00100020, $80008020, $00000000, $80000000, $00008000, $00108020,
      $80100000,
    $00100020, $80000020, $00000000, $00108000, $00008020, $80108000, $80100000,
      $00008020,
    $00000000, $00108020, $80100020, $00100000, $80008020, $80100000, $80108000,
      $00008000,
    $80100000, $80008000, $00000020, $80108020, $00108020, $00000020, $00008000,
      $80000000,
    $00008020, $80108000, $00100000, $80000020, $00100020, $80008020, $80000020,
      $00100020,
    $00108000, $00000000, $80008000, $00008020, $80000000, $80100020, $80108020,
      $00108000 ),
     (
    $00000208, $08020200, $00000000, $08020008, $08000200, $00000000, $00020208,
      $08000200,
    $00020008, $08000008, $08000008, $00020000, $08020208, $00020008, $08020000,
      $00000208,
    $08000000, $00000008, $08020200, $00000200, $00020200, $08020000, $08020008,
      $00020208,
    $08000208, $00020200, $00020000, $08000208, $00000008, $08020208, $00000200,
      $08000000,
    $08020200, $08000000, $00020008, $00000208, $00020000, $08020200, $08000200,
      $00000000,
    $00000200, $00020008, $08020208, $08000200, $08000008, $00000200, $00000000,
      $08020008,
    $08000208, $00020000, $08000000, $08020208, $00000008, $00020208, $00020200,
      $08000008,
    $08020000, $08000208, $00000208, $08020000, $00020208, $00000008, $08020008,
      $00020200 ),
     (
    $00802001, $00002081, $00002081, $00000080, $00802080, $00800081, $00800001,
      $00002001,
    $00000000, $00802000, $00802000, $00802081, $00000081, $00000000, $00800080,
      $00800001,
    $00000001, $00002000, $00800000, $00802001, $00000080, $00800000, $00002001,
      $00002080,
    $00800081, $00000001, $00002080, $00800080, $00002000, $00802080, $00802081,
      $00000081,
    $00800080, $00800001, $00802000, $00802081, $00000081, $00000000, $00000000,
      $00802000,
    $00002080, $00800080, $00800081, $00000001, $00802001, $00002081, $00002081,
      $00000080,
    $00802081, $00000081, $00000001, $00002000, $00800001, $00002001, $00802080,
      $00800081,
    $00002001, $00002080, $00800000, $00802001, $00000080, $00800000, $00002000,
      $00802080 ),
     (
    $00000100, $02080100, $02080000, $42000100, $00080000, $00000100, $40000000,
      $02080000,
    $40080100, $00080000, $02000100, $40080100, $42000100, $42080000, $00080100,
      $40000000,
    $02000000, $40080000, $40080000, $00000000, $40000100, $42080100, $42080100,
      $02000100,
    $42080000, $40000100, $00000000, $42000000, $02080100, $02000000, $42000000,
      $00080100,
    $00080000, $42000100, $00000100, $02000000, $40000000, $02080000, $42000100,
      $40080100,
    $02000100, $40000000, $42080000, $02080100, $40080100, $00000100, $02000000,
      $42080000,
    $42080100, $00080100, $42000000, $42080100, $02080000, $00000000, $40080000,
      $42000000,
    $00080100, $02000100, $40000100, $00080000, $00000000, $40080000, $02080100,
      $40000100 ),
     (
    $20000010, $20400000, $00004000, $20404010, $20400000, $00000010, $20404010,
      $00400000,
    $20004000, $00404010, $00400000, $20000010, $00400010, $20004000, $20000000,
      $00004010,
    $00000000, $00400010, $20004010, $00004000, $00404000, $20004010, $00000010,
      $20400010,
    $20400010, $00000000, $00404010, $20404000, $00004010, $00404000, $20404000,
      $20000000,
    $20004000, $00000010, $20400010, $00404000, $20404010, $00400000, $00004010,
      $20000010,
    $00400000, $20004000, $20000000, $00004010, $20000010, $20404010, $00404000,
      $20400000,
    $00404010, $20404000, $00000000, $20400010, $00000010, $00004000, $20400000,
      $00404010,
    $00004000, $00400010, $20004010, $00000000, $20404000, $20000000, $00400010,
      $20004010 ),
     (
    $00200000, $04200002, $04000802, $00000000, $00000800, $04000802, $00200802,
      $04200800,
    $04200802, $00200000, $00000000, $04000002, $00000002, $04000000, $04200002,
      $00000802,
    $04000800, $00200802, $00200002, $04000800, $04000002, $04200000, $04200800,
      $00200002,
    $04200000, $00000800, $00000802, $04200802, $00200800, $00000002, $04000000,
      $00200800,
    $04000000, $00200800, $00200000, $04000802, $04000802, $04200002, $04200002,
      $00000002,
    $00200002, $04000000, $04000800, $00200000, $04200800, $00000802, $00200802,
      $04200800,
    $00000802, $04000002, $04200802, $04200000, $00200800, $00000000, $00000002,
      $04200802,
    $00000000, $00200802, $04200000, $00000800, $04000002, $04000800, $00000800,
      $00200002 ),
     (
    $10001040, $00001000, $00040000, $10041040, $10000000, $10001040, $00000040,
      $10000000,
    $00040040, $10040000, $10041040, $00041000, $10041000, $00041040, $00001000,
      $00000040,
    $10040000, $10000040, $10001000, $00001040, $00041000, $00040040, $10040040,
      $10041000,
    $00001040, $00000000, $00000000, $10040040, $10000040, $10001000, $00041040,
      $00040000,
    $00041040, $00040000, $10041000, $00001000, $00000040, $10040040, $00001000,
      $00041040,
    $10001000, $00000040, $10000040, $10040000, $10040040, $10000000, $00040000,
      $10001040,
    $00000000, $10041040, $00040040, $10000040, $10040000, $10001000, $10001040,
      $00000000,
    $10041040, $00041000, $00041000, $00001040, $00001040, $00040040, $10000000,
      $10041000 )
     );
    

// Key expansion routine

procedure ExpandKeyForEncryption(const Key: TDESKey;
  out ExpandedKey: TDESExpandedKey);
var
  K: array[0..31] of cardinal;
  Buffer: array[0..56 + 56 + 8 - 1] of byte;
  pc1m: PByteArray;
  pcr: PByteArray;
  ks: PByteArray;
  i, j, l, m: Cardinal;
  A, B: Cardinal;
begin

  FillChar(Buffer[0], SizeOf(Buffer), 0);
  FillChar(K[0], SizeOf(K), 0);

  pc1m := @buffer; // place to modify pc1 into
  pcr := Pointer(PtrUInt(pc1m) + 56); // place to rotate pc1 into
  ks := Pointer(PtrUInt(pcr) + 56);

  for j := 0 to 55 do // convert pc1 to bits of key
  begin
    l := pc1[j] - 1; // integer bit location
    m := l and 07; // find bit
  // pc1m[j] := (key [l shr 3] and bytebit[m]) ? 1 : 0;
    if (key[l shr 3] and bytebit[m]) > 0 then
      pc1m[j] := 1
    else
      pc1m[j] := 0;
  end;

  for i := 0 to 15 do // key chunk for each iteration
  begin
    FillChar(ks^, 8, 0); // Clear key schedule
    for j := 0 to 55 do // rotate pc1 the right amount
    begin
   //pcr[j] := pc1m[(l=j+totrot[i])<(j<28? 28 : 56) ? l: l-28];
      if j < 28 then
        A := 28
      else
        A := 56;
      l := j + totrot[i];
      if l < A then
        B := l
      else
        B := l - 28;
      pcr[j] := pc1m[B];
    end;
    for j := 0 to 47 do
    begin
      if (pcr[pc2[j] - 1]) > 0 then
      begin
        l := j mod 6;
        ks[j div 6] := (ks[j div 6]) or (bytebit[l] shr 2);
      end;
    end;
    k[2 * i] := (cardinal(ks[0]) shl 24)
      or (cardinal(ks[2]) shl 16)
      or (cardinal(ks[4]) shl 8)
      or (cardinal(ks[6]));
    k[2 * i + 1] := (cardinal(ks[1]) shl 24)
      or (cardinal(ks[3]) shl 16)
      or (cardinal(ks[5]) shl 8)
      or (cardinal(ks[7]));
  end;

  Move(K[0], ExpandedKey[0], SizeOf(K));
end;

procedure ExpandKeyForDecryption(const Key: TDESKey;
  out ExpandedKey: TDESExpandedKey);
var
  K: array[0..31] of cardinal;
  Buffer: array[0..56 + 56 + 8 - 1] of byte;
  pc1m: PByteArray;
  pcr: PByteArray;
  ks: PByteArray;
  i, j, l, m: Cardinal;
  A, B: Cardinal;
  swap: Cardinal;
begin

  FillChar(Buffer[0], SizeOf(Buffer), 0);
  FillChar(K[0], SizeOf(K), 0);

  pc1m := @buffer; // place to modify pc1 into
  pcr := Pointer(PtrUInt(pc1m) + 56); // place to rotate pc1 into
  ks := Pointer(PtrUInt(pcr) + 56);

  for j := 0 to 55 do // convert pc1 to bits of key
  begin
    l := pc1[j] - 1; // integer bit location
    m := l and 07; // find bit
  // pc1m[j] := (key [l shr 3] and bytebit[m]) ? 1 : 0;
    if (key[l shr 3] and bytebit[m]) > 0 then
      pc1m[j] := 1
    else
      pc1m[j] := 0;
  end;

  for i := 0 to 15 do // key chunk for each iteration
  begin
    fillChar(ks^, 8, 0); // Clear key schedule
    for j := 0 to 55 do // rotate pc1 the right amount
    begin
   //pcr[j] := pc1m[(l=j+totrot[i])<(j<28? 28 : 56) ? l: l-28];
      if j < 28 then
        A := 28
      else
        A := 56;
      l := j + totrot[i];
      if l < A then
        B := l
      else
        B := l - 28;
      pcr[j] := pc1m[B];
    end;
    for j := 0 to 47 do
    begin
      if (pcr[pc2[j] - 1]) > 0 then
      begin
        l := j mod 6;
        ks[j div 6] := (ks[j div 6]) or (bytebit[l] shr 2);
      end;
    end;
    k[2 * i] := (cardinal(ks[0]) shl 24)
      or (cardinal(ks[2]) shl 16)
      or (cardinal(ks[4]) shl 8)
      or (cardinal(ks[6]));
    k[2 * i + 1] := (cardinal(ks[1]) shl 24)
      or (cardinal(ks[3]) shl 16)
      or (cardinal(ks[5]) shl 8)
      or (cardinal(ks[7]));
  end;

  for j := 0 to 7 do
  begin
    i := j * 2;
    swap := k[i];
    k[i] := k[32 - 2 - i];
    k[32 - 2 - i] := swap;

    swap := k[i + 1];
    k[i + 1] := k[32 - 1 - i];
    k[32 - 1 - i] := swap;
  end;

  Move(K[0], ExpandedKey[0], SizeOf(K));
end;

// procedures for DES and 3DES

function rotlFixed(x: Cardinal; y: word): Cardinal;
begin
  result := (x shl y) or (x shr (sizeof(Cardinal) * 8 - y));
end;

function rotrFixed(x: Cardinal; y: word): Cardinal;
begin
  result := (x shr y) or (x shl (sizeof(Cardinal) * 8 - y));
end;

function byteReverse(value: Cardinal): Cardinal;
begin
 // 5 instructions with rotate instruction, 9 without
  result := (rotrFixed(value, 8) and $FF00FF00) or (rotlFixed(value, 8) and
    $00FF00FF);
end;

procedure GetBlockBigEndian(block: PLongWordArray; var a, b: Cardinal);
begin
  a := byteReverse(block[0]);
  b := byteReverse(block[1]);
end;

// Put 2 words back into user's buffer in BIG-endian order

procedure PutBlockBigEndian(block: PLongWordArray; a, b: Cardinal);
begin
  block[0] := byteReverse(a);
  block[1] := byteReverse(b);
end;

procedure IPERM(var left, right: Cardinal);
var
  work: Cardinal;
begin
  right := rotlFixed(right, 4);
  work := (left xor right) and $F0F0F0F0;
  left := left xor work;
  right := rotrFixed(right xor work, 20);
  work := (left xor right) and $FFFF0000;
  left := left xor work;
  right := rotrFixed(right xor work, 18);
  work := (left xor right) and $33333333;
  left := left xor work;
  right := rotrFixed(right xor work, 6);
  work := (left xor right) and $00FF00FF;
  left := left xor work;
  right := rotlFixed(right xor work, 9);
  work := (left xor right) and $AAAAAAAA;
  left := rotlFixed(left xor work, 1);
  right := right xor work;
end;

procedure FPERM(var left, right: Cardinal);
var
  work: Cardinal;
begin
  right := rotrFixed(right, 1);
  work := (left xor right) and $AAAAAAAA;
  right := right xor work;
  left := rotrFixed(left xor work, 9);
  work := (left xor right) and $00FF00FF;
  right := right xor work;
  left := rotlFixed(left xor work, 6);
  work := (left xor right) and $33333333;
  right := right xor work;
  left := rotlFixed(left xor work, 18);
  work := (left xor right) and $FFFF0000;
  right := right xor work;
  left := rotlFixed(left xor work, 20);
  work := (left xor right) and $F0F0F0F0;
  right := right xor work;
  left := rotrFixed(left xor work, 4);
end;


procedure RawProcessBlock(var l_, r_: Cardinal; ExpandedKey: TDESExpandedKey);
var
  l, r: Cardinal;
  kptr: PLongWordArray;
  i: integer;
  work: Cardinal;
begin
  l := l_;
  r := r_;
  kptr := @ExpandedKey[0][0];

  for i := 0 to 7 do
  begin
    work := rotrFixed(r, Word(4)) xor kptr[4 * i + 0];
    l := l xor Spbox[6][(work) and $3F]
      xor Spbox[4][(work shr 8) and $3F]
      xor Spbox[2][(work shr 16) and $3F]
      xor Spbox[0][(work shr 24) and $3F];
    work := r xor kptr[4 * i + 1];
    l := l xor Spbox[7][(work) and $3F]
      xor Spbox[5][(work shr 8) and $3F]
      xor Spbox[3][(work shr 16) and $3F]
      xor Spbox[1][(work shr 24) and $3F];

    work := rotrFixed(l, 4) xor kptr[4 * i + 2];
    r := r xor Spbox[6][(work) and $3F]
      xor Spbox[4][(work shr 8) and $3F]
      xor Spbox[2][(work shr 16) and $3F]
      xor Spbox[0][(work shr 24) and $3F];
    work := l xor kptr[4 * i + 3];
    r := r xor Spbox[7][(work) and $3F]
      xor Spbox[5][(work shr 8) and $3F]
      xor Spbox[3][(work shr 16) and $3F]
      xor Spbox[1][(work shr 24) and $3F];
  end;

  l_ := l;
  r_ := r;
end;

procedure Encrypt(const InBuf: TDESBuffer;
  const ExpandedKey: TDESExpandedKey; out OutBuf: TDESBuffer);
var
  inBlock, outBlock: PByteArray;
  kptr: PLongWordArray;
  l, r: Cardinal;
  i: Word;
  work: Cardinal;
begin


  inBlock := @InBuf[0];
  outBlock := @OutBuf[0];
  kptr := @ExpandedKey[0][0];

  GetBlockBigEndian(@inBlock[0], l, r);


  IPERM(l, r);

  for i := 0 to 7 do
  begin
    work := rotrFixed(r, Word(4)) xor kptr[4 * i + 0];
    l := l xor Spbox[6][(work) and $3F]
      xor Spbox[4][(work shr 8) and $3F]
      xor Spbox[2][(work shr 16) and $3F]
      xor Spbox[0][(work shr 24) and $3F];
    work := r xor kptr[4 * i + 1];
    l := l xor Spbox[7][(work) and $3F]
      xor Spbox[5][(work shr 8) and $3F]
      xor Spbox[3][(work shr 16) and $3F]
      xor Spbox[1][(work shr 24) and $3F];

    work := rotrFixed(l, 4) xor kptr[4 * i + 2];
    r := r xor Spbox[6][(work) and $3F]
      xor Spbox[4][(work shr 8) and $3F]
      xor Spbox[2][(work shr 16) and $3F]
      xor Spbox[0][(work shr 24) and $3F];
    work := l xor kptr[4 * i + 3];
    r := r xor Spbox[7][(work) and $3F]
      xor Spbox[5][(work shr 8) and $3F]
      xor Spbox[3][(work shr 16) and $3F]
      xor Spbox[1][(work shr 24) and $3F];
  end;

  FPERM(l, r);
  PutBlockBigEndian(@outBlock[0], r, l);
end;


{procedure ExpandKey(const Key: TDESKey; out ExpandedKey: TDESExpandedKey);
var
  C, D: array [0..27] of byte;
  I, J: integer;
  T: word;
begin
  FillChar(C, SizeOf(C), 0);
  FillChar(D, SizeOf(D), 0);
  // permuted choise 1
  for I := 0 to 27 do
  begin
    if (Key[(PC1[I]) shr 3] and (128 shr ((PC1[I]) and $07))) > 0 then
      C[I] := 1;
    if (Key[(PC1[I + 28]) shr 3] and (128 shr ((PC1[I + 28]) and $07))) > 0 then
      D[I] := 1;
  end;
  // producing subkeys
  for I := 0 to 15 do
  begin
    // shifting C
    T := 0;
    Move(C[0], T, Shifts[I]);
    Move(C[Shifts[I]], C[0], 28 - Shifts[I]);
    Move(T, C[28 - Shifts[I]], Shifts[I]);
    // shifting D
    T := 0;
    Move(D[0], T, Shifts[I]);
    Move(D[Shifts[I]], D[0], 28 - Shifts[I]);
    Move(T, D[28 - Shifts[I]], Shifts[I]);
    // permuted choise 2
    for J := 0 to 47 do
      if PC2[J] <= 27 then
        ExpandedKey[I, J] := C[PC2[J]]
      else
        ExpandedKey[I, J] := D[PC2[J] - 28];
  end;
end;    }

// Block processing routines

{procedure Encrypt(const InBuf: TDESBuffer;
  const ExpandedKey: TDESExpandedKey; out OutBuf: TDESBuffer);
var
  L, R, T2, T3: array [0..31] of byte;
  I, J: integer;
  B: byte;
  T: array [0..47] of byte;
  Row, Col: integer;

  V: int64;
begin
  // dividing into bits and performing initial permutation
  Move(InBuf[0], V, SizeOf(V));
//  V:=$8000000000000000 shr 6;
  for I := 0 to 63 do
  begin
    //B := (InBuf[(IP[I]) shr 3] and (128 shr ((IP[I]) and $07)));
    B:=(V shr (63-IP[I])) and 1;
    if B > 0 then B := 1 else B := 0;
    if I <= 31 then
      R[I] := B
    else
      L[I - 32] := B;
  end;

  for I := 0 to 15 do
  begin
    // expanding and XORing
    for J := 0 to 47 do
      T[J] := R[E[J]] xor ExpandedKey[I, J];
    // selecting
    // step 1
    Row := (T[0] shl 1) or T[5];
    Col := (T[1] shl 3) or (T[2] shl 2) or (T[3] shl 1) or T[4];
    T2[0] := (S1[Row, Col] and $08) shr 3;
    T2[1] := (S1[Row, Col] and $04) shr 2;
    T2[2] := (S1[Row, Col] and $02) shr 1;
    T2[3] := S1[Row, Col] and $01;
    // step 2
    Row := (T[6] shl 1) or T[11];
    Col := (T[7] shl 3) or (T[8] shl 2) or (T[9] shl 1) or T[10];
    T2[4] := (S2[Row, Col] and $08) shr 3;
    T2[5] := (S2[Row, Col] and $04) shr 2;
    T2[6] := (S2[Row, Col] and $02) shr 1;
    T2[7] := S2[Row, Col] and $01;
    // step 3
    Row := (T[12] shl 1) or T[17];
    Col := (T[13] shl 3) or (T[14] shl 2) or (T[15] shl 1) or T[16];
    T2[8] := (S3[Row, Col] and $08) shr 3;
    T2[9] := (S3[Row, Col] and $04) shr 2;
    T2[10] := (S3[Row, Col] and $02) shr 1;
    T2[11] := S3[Row, Col] and $01;
    // step 4
    Row := (T[18] shl 1) or T[23];
    Col := (T[19] shl 3) or (T[20] shl 2) or (T[21] shl 1) or T[22];
    T2[12] := (S4[Row, Col] and $08) shr 3;
    T2[13] := (S4[Row, Col] and $04) shr 2;
    T2[14] := (S4[Row, Col] and $02) shr 1;
    T2[15] := S4[Row, Col] and $01;
    // step 5
    Row := (T[24] shl 1) or T[29];
    Col := (T[25] shl 3) or (T[26] shl 2) or (T[27] shl 1) or T[28];
    T2[16] := (S5[Row, Col] and $08) shr 3;
    T2[17] := (S5[Row, Col] and $04) shr 2;
    T2[18] := (S5[Row, Col] and $02) shr 1;
    T2[19] := S5[Row, Col] and $01;
    // step 6
    Row := (T[30] shl 1) or T[35];
    Col := (T[31] shl 3) or (T[32] shl 2) or (T[33] shl 1) or T[34];
    T2[20] := (S6[Row, Col] and $08) shr 3;
    T2[21] := (S6[Row, Col] and $04) shr 2;
    T2[22] := (S6[Row, Col] and $02) shr 1;
    T2[23] := S6[Row, Col] and $01;
    // step 7
    Row := (T[36] shl 1) or T[41];
    Col := (T[37] shl 3) or (T[38] shl 2) or (T[39] shl 1) or T[40];
    T2[24] := (S7[Row, Col] and $08) shr 3;
    T2[25] := (S7[Row, Col] and $04) shr 2;
    T2[26] := (S7[Row, Col] and $02) shr 1;
    T2[27] := S7[Row, Col] and $01;
    // step 8
    Row := (T[42] shl 1) or T[47];
    Col := (T[43] shl 3) or (T[44] shl 2) or (T[45] shl 1) or T[46];
    T2[28] := (S8[Row, Col] and $08) shr 3;
    T2[29] := (S8[Row, Col] and $04) shr 2;
    T2[30] := (S8[Row, Col] and $02) shr 1;
    T2[31] := S8[Row, Col] and $01;
    // permutation
    for J := 0 to 31 do
      T3[J] := T2[P[J]] xor L[J];
    // exchanging
    Move(R, L, SizeOf(R));
    Move(T3, R, SizeOf(T3));
  end;
  // finalization
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  for I := 0 to 63 do
  begin
    B := IPR[I];
    if B <= 31 then
      B := R[B]
    else
      B := L[B - 32];
    if B > 0 then
      OutBuf[I shr 3] := OutBuf[I shr 3] or (128 shr (I and $07));
  end;
end;    }

{procedure Decrypt(const InBuf: TDESBuffer;
  const ExpandedKey: TDESExpandedKey; out OutBuf: TDESBuffer);
var
  L, R, T2, T3: array [0..31] of byte;
  I, J: integer;
  B: byte;
  T: array [0..47] of byte;
  Row, Col: integer;
begin
  // dividing into bits and performing initial permutation
  for I := 0 to 63 do
  begin
    B := (InBuf[IP[I] shr 3] and (128 shr (IP[I] and $07)));
    if B > 0 then
      B := 1;
    if I <= 31 then
      L[I] := B
    else
      R[I - 32] := B;
  end;
  for I := 0 to 15 do
  begin
    // expanding and XORing
    for J := 0 to 47 do
      T[J] := R[E[J]] xor ExpandedKey[15 - I, J];
    // selecting
    // step 1
    Row := (T[0] shl 1) or T[5];
    Col := (T[1] shl 3) or (T[2] shl 2) or (T[3] shl 1) or T[4];
    T2[0] := (S1[Row, Col] and $08) shr 3;
    T2[1] := (S1[Row, Col] and $04) shr 2;
    T2[2] := (S1[Row, Col] and $02) shr 1;
    T2[3] := S1[Row, Col] and $01;
    // step 2
    Row := (T[6] shl 1) or T[11];
    Col := (T[7] shl 3) or (T[8] shl 2) or (T[9] shl 1) or T[10];
    T2[4] := (S2[Row, Col] and $08) shr 3;
    T2[5] := (S2[Row, Col] and $04) shr 2;
    T2[6] := (S2[Row, Col] and $02) shr 1;
    T2[7] := S2[Row, Col] and $01;
    // step 3
    Row := (T[12] shl 1) or T[17];
    Col := (T[13] shl 3) or (T[14] shl 2) or (T[15] shl 1) or T[16];
    T2[8] := (S3[Row, Col] and $08) shr 3;
    T2[9] := (S3[Row, Col] and $04) shr 2;
    T2[10] := (S3[Row, Col] and $02) shr 1;
    T2[11] := S3[Row, Col] and $01;
    // step 4
    Row := (T[18] shl 1) or T[23];
    Col := (T[19] shl 3) or (T[20] shl 2) or (T[21] shl 1) or T[22];
    T2[12] := (S4[Row, Col] and $08) shr 3;
    T2[13] := (S4[Row, Col] and $04) shr 2;
    T2[14] := (S4[Row, Col] and $02) shr 1;
    T2[15] := S4[Row, Col] and $01;
    // step 5
    Row := (T[24] shl 1) or T[29];
    Col := (T[25] shl 3) or (T[26] shl 2) or (T[27] shl 1) or T[28];
    T2[16] := (S5[Row, Col] and $08) shr 3;
    T2[17] := (S5[Row, Col] and $04) shr 2;
    T2[18] := (S5[Row, Col] and $02) shr 1;
    T2[19] := S5[Row, Col] and $01;
    // step 6
    Row := (T[30] shl 1) or T[35];
    Col := (T[31] shl 3) or (T[32] shl 2) or (T[33] shl 1) or T[34];
    T2[20] := (S6[Row, Col] and $08) shr 3;
    T2[21] := (S6[Row, Col] and $04) shr 2;
    T2[22] := (S6[Row, Col] and $02) shr 1;
    T2[23] := S6[Row, Col] and $01;
    // step 7
    Row := (T[36] shl 1) or T[41];
    Col := (T[37] shl 3) or (T[38] shl 2) or (T[39] shl 1) or T[40];
    T2[24] := (S7[Row, Col] and $08) shr 3;
    T2[25] := (S7[Row, Col] and $04) shr 2;
    T2[26] := (S7[Row, Col] and $02) shr 1;
    T2[27] := S7[Row, Col] and $01;
    // step 8
    Row := (T[42] shl 1) or T[47];
    Col := (T[43] shl 3) or (T[44] shl 2) or (T[45] shl 1) or T[46];
    T2[28] := (S8[Row, Col] and $08) shr 3;
    T2[29] := (S8[Row, Col] and $04) shr 2;
    T2[30] := (S8[Row, Col] and $02) shr 1;
    T2[31] := S8[Row, Col] and $01;
    // permutation
    for J := 0 to 31 do
      T3[J] := T2[P[J]] xor L[J];
    // exchanging
    Move(R, L, SizeOf(R));
    Move(T3, R, SizeOf(T3));
  end;
  // finalization
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  for I := 0 to 63 do
  begin
    B := IPR[I];
    if B <= 31 then
      B := R[B]
    else
      B := L[B - 32];
    if B > 0 then
      OutBuf[I shr 3] := OutBuf[I shr 3] or (128 shr (I and $07));
  end;
end;      }

procedure Decrypt(const InBuf: TDESBuffer;
  const ExpandedKey: TDESExpandedKey; out OutBuf: TDESBuffer);
begin
  Encrypt(InBuf, ExpandedKey, OutBuf);
end;


function EncryptECB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TDESExpandedKey): boolean;
var
  TempIn, TempOut: PDESBuffer;
  Temp: TDESBuffer;
  S: cardinal;
begin
  S := (InSize div SizeOf(TDESBuffer)) * SizeOf(TDESBuffer);
  if (InSize mod SizeOf(TDESBuffer)) <> 0 then
    S := S + SizeOf(TDESBuffer);
  if OutSize < S then
  begin
    OutSize := S;
    Result := False;
    exit;
  end;
  Result := True;
  OutSize := 0;
  while InSize >= SizeOf(TDESBuffer) do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(TempIn^, ExpandedKey, TempOut^);
    Dec(InSize, SizeOf(TDESBuffer));
    Inc(OutSize, SizeOf(TDESBuffer));
  end;
  if InSize > 0 then
  begin
    Move(Pointer(PtrUInt(InBuffer) + OutSize)^, Temp, InSize);
    FillChar(Temp[InSize], SizeOf(Temp) - InSize, 0);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Temp, ExpandedKey, TempOut^);
    Inc(OutSize, SizeOf(TDESBuffer));
  end;
end;

function DecryptECB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TDESExpandedKey): boolean;
var
  TempIn, TempOut: PDESBuffer;
begin
  if (InSize mod SizeOf(TDESBuffer)) > 0 then
    raise EEncryptionError.Create(SInvalidInputSize);
  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := False;
    exit;
  end;
  OutSize := 0;
  while InSize >= SizeOf(TDESBuffer) do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Decrypt(TempIn^, ExpandedKey, TempOut^);
    Dec(InSize, SizeOf(TDESBuffer));
    Inc(OutSize, SizeOf(TDESBuffer));
  end;
  Result := True;
end;

// CBC Mode

function EncryptCBC(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer): boolean;
var
  TempIn, TempOut: PDESBuffer;
  Vector, Temp: TDESBuffer;
  S: cardinal;
begin
  S := (InSize div SizeOf(TDESBuffer)) * SizeOf(TDESBuffer);
  if (InSize mod SizeOf(TDESBuffer)) <> 0 then
    S := S + SizeOf(TDESBuffer);
  if OutSize < S then
  begin
    OutSize := S;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector := InitVector;
  while InSize >= SizeOf(TDESBuffer) do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    PLongWord(@Temp[0])^ := PLongWord(@TempIn^[0])^ xor PLongWord(@Vector[0])^;
    PLongWord(@Temp[4])^ := PLongWord(@TempIn^[4])^ xor PLongWord(@Vector[4])^;
    Encrypt(Temp, ExpandedKey, TempOut^);
    Vector := TempOut^;
    Dec(InSize, SizeOf(TDESBuffer));
    Inc(OutSize, SizeOf(TDESBuffer));
  end;
  if InSize > 0 then
  begin
    Move(Pointer(PtrUInt(InBuffer) + OutSize)^, Temp, InSize);
    FillChar(Temp[InSize], SizeOf(Temp) - InSize, 0);
    PLongWord(@Temp[0])^ := PLongWord(@Temp[0])^ xor PLongWord(@Vector[0])^;
    PLongWord(@Temp[4])^ := PLongWord(@Temp[4])^ xor PLongWord(@Vector[4])^;
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Temp, ExpandedKey, TempOut^);
    Inc(OutSize, SizeOf(TDESBuffer));
  end;
  Result := True;
end;

function DecryptCBC(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer): boolean;
var
  TempIn, TempOut: PDESBuffer;
  Vector1, Vector2: TDESBuffer;
begin
  if (InSize mod SizeOf(TDESBuffer)) > 0 then
    raise EEncryptionError.Create(SInvalidInputSize);
  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector1 := InitVector;
  while InSize >= SizeOf(TDESBuffer) do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Vector2 := TempIn^;
    Decrypt(TempIn^, ExpandedKey, TempOut^);
    PLongWord(@TempOut^[0])^ := PLongWord(@TempOut^[0])^ xor
      PLongWord(@Vector1[0])^;
    PLongWord(@TempOut^[4])^ := PLongWord(@TempOut^[4])^ xor
      PLongWord(@Vector1[4])^;
    Vector1 := Vector2;
    Dec(InSize, SizeOf(TDESBuffer));
    Inc(OutSize, SizeOf(TDESBuffer));
  end;
  Result := True;
end;

// CFB Mode

function EncryptCFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer): boolean;
var
  TempIn, TempOut: PByte;
  Vector, Temp: TDESBuffer;
begin
  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector := InitVector;
  while InSize > 0 do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Vector, ExpandedKey, Temp);
    TempOut^ := Temp[0] xor TempIn^;
    PInt64(@Vector)^ := PInt64(@Vector)^ shl 8;
    Vector[7] := TempOut^;
    Dec(InSize);
    Inc(OutSize);
  end;
  Result := True;
end;

function DecryptCFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer): boolean;
var
  TempIn, TempOut: PByte;
  Vector, Temp: TDESBuffer;
begin
  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector := InitVector;
  while InSize >= 1 do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Vector, ExpandedKey, Temp);
    TempOut^ := Temp[0] xor TempIn^;
    PInt64(@Vector)^ := PInt64(@Vector)^ shl 8;
    Vector[7] := TempIn^;
    Dec(InSize);
    Inc(OutSize);
  end;
  Result := True;
end;

// CFB Mode w/ Step

function EncryptCFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer; const Step: byte): boolean;
var
  TempIn, TempOut: PDESBuffer;
  Vector, Temp, Temp1: TDESBuffer;
  S: cardinal;
  I: byte;
begin
  S := (InSize div Step) * Step;
  if (InSize mod Step) <> 0 then
    S := S + Step;
  if OutSize < S then
  begin
    OutSize := S;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector := InitVector;
  while InSize >= Step do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Vector, ExpandedKey, Temp);
    I := 0;
    repeat
      TempOut^[I] := Temp[I] xor TempIn^[I];
      Inc(I);
    until I >= Step;
    I := 0;
    repeat
      Vector[I] := Vector[I + Step];
      Inc(I);
    until I > 7 - Step;
    repeat
      Vector[I] := TempOut^[I - 8 + Step];
      Inc(I);
    until I >= 8;
    Dec(InSize, Step);
    Inc(OutSize, Step);
  end;
  if InSize > 0 then
  begin
    Move(Pointer(PtrUInt(InBuffer) + OutSize)^, Temp1, InSize);
    FillChar(Temp1[InSize], Step - InSize, 0);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Vector, ExpandedKey, Temp);
    I := 0;
    repeat
      TempOut^[I] := Temp[I] xor Temp1[I];
      Inc(I);
    until I >= Step;
    Inc(OutSize, Step);
  end;
  Result := True;
end;

function DecryptCFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer; const Step: byte): boolean;
var
  TempIn, TempOut: PDESBuffer;
  Vector, Temp: TDESBuffer;
  I: byte;
begin
  if (InSize mod Step) > 0 then
    raise EEncryptionError.Create(SInvalidInputSize);
  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector := InitVector;
  while InSize >= Step do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Vector, ExpandedKey, Temp);
    I := 0;
    repeat
      TempOut^[I] := Temp[I] xor TempIn^[I];
      Inc(I);
    until I >= Step;
    I := 0;
    repeat
      Vector[I] := Vector[I + Step];
      Inc(I);
    until I > 7 - Step;
    repeat
      Vector[I] := TempIn^[I - 8 + Step];
      Inc(I);
    until I >= 8;
    Dec(InSize, Step);
    Inc(OutSize, Step);
  end;
  Result := True;
end;

// OFB Mode

function EncryptOFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer): boolean;
var
  TempIn, TempOut: PByte;
  Vector, Temp: TDESBuffer;
begin
  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector := InitVector;
  while InSize >= 1 do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Vector, ExpandedKey, Temp);
    PInt64(@Vector)^ := PInt64(@Vector)^ shl 8;
    Vector[7] := Temp[0];
    TempOut^ := Temp[0] xor TempIn^;
    Dec(InSize);
    Inc(OutSize);
  end;
  Result := True;
end;

function DecryptOFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer): boolean;
var
  TempIn, TempOut: PByte;
  Vector, Temp: TDESBuffer;
begin
  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector := InitVector;
  while InSize >= 1 do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Vector, ExpandedKey, Temp);
    PInt64(@Vector)^ := PInt64(@Vector)^ shl 8;
    Vector[7] := Temp[0];
    TempOut^ := Temp[0] xor TempIn^;
    Dec(InSize);
    Inc(OutSize);
  end;
  Result := True;
end;

// OFB Mode w/ Step

function EncryptOFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TDESExpandedKey;
  const InitVector: TDESBuffer; const Step: byte): boolean;
var
  TempIn, TempOut: PDESBuffer;
  Vector, Temp, Temp1: TDESBuffer;
  S: cardinal;
  I: byte;
begin
  S := (InSize div Step) * Step;
  if (InSize mod Step) <> 0 then
    S := S + Step;
  if OutSize < S then
  begin
    OutSize := S;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector := InitVector;
  while InSize >= Step do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Vector, ExpandedKey, Temp);
    I := 0;
    repeat
      Vector[I] := Vector[I + Step];
      Inc(I);
    until I > 7 - Step;
    repeat
      Vector[I] := Temp[I - 8 + Step];
      Inc(I);
    until I >= 8;
    I := 0;
    repeat
      TempOut^[I] := Temp[I] xor TempIn^[I];
      Inc(I);
    until I >= Step;
    Dec(InSize, Step);
    Inc(OutSize, Step);
  end;
  if InSize > 0 then
  begin
    Move(Pointer(PtrUInt(InBuffer) + OutSize)^, Temp1, InSize);
    FillChar(Temp1[InSize], Step - InSize, 0);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Vector, ExpandedKey, Temp);
    I := 0;
    repeat
      TempOut^[I] := Temp[I] xor Temp1[I];
      Inc(I);
    until I >= Step;
    Inc(OutSize, Step);
  end;
  Result := True;
end;

function DecryptOFB(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  const Step: byte): boolean;
var
  TempIn, TempOut: PDESBuffer;
  Vector, Temp: TDESBuffer;
  I: byte;
begin
  if (InSize mod Step) > 0 then
    raise EEncryptionError.Create(SInvalidInputSize);
  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector := InitVector;
  while InSize >= Step do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Vector, ExpandedKey, Temp);
    I := 0;
    repeat
      Vector[I] := Vector[I + Step];
      Inc(I);
    until I > 7 - Step;
    repeat
      Vector[I] := Temp[I - 8 + Step];
      Inc(I);
    until I >= 8;
    I := 0;
    repeat
      TempOut^[I] := Temp[I] xor TempIn^[I];
      Inc(I);
    until I >= Step;
    Dec(InSize, Step);
    Inc(OutSize, Step);
  end;
  Result := True;
end;

// Chunks processing routines
// ECB Mode

procedure InitializeEncryptionECB(var Context: TDESContext; Key: TDESKey);
begin
  ExpandKeyForEncryption(Key, Context.ExpandedKey);
  Context.TailLen := 0;
end;

procedure InitializeDecryptionECB(var Context: TDESContext; Key: TDESKey);
begin
  ExpandKeyForDecryption(Key, Context.ExpandedKey);
  Context.TailLen := 0;
end;

function EncryptECB(var Context: TDESContext; Chunk: pointer; ChunkSize:
  cardinal;
  OutBuf: pointer; var OutSize: cardinal): boolean;
var
  Left, Needed: cardinal;
begin
  if ChunkSize = 0 then
  begin
    OutSize := 0;
    Result := True;
    exit;
  end;
  Needed := ((Context.TailLen + ChunkSize) div SizeOf(TDESBuffer)) *
    SizeOf(TDESBuffer);
  if Needed > OutSize then
  begin
    OutSize := Needed;
    Result := False;
    exit;
  end;
  if Context.TailLen > 0 then
  begin
    Left := SizeOf(TDESBuffer) - Context.TailLen;
    if Left > ChunkSize then
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], ChunkSize);
      Inc(Context.TailLen, ChunkSize);
      ChunkSize := 0;
    end
    else
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], Left);
      Dec(ChunkSize, Left);
      Encrypt(Context.Tail, Context.ExpandedKey, PDESBuffer(OutBuf)^);
      Inc(PtrUInt(Chunk), Left);
      Inc(PtrUInt(OutBuf), SizeOf(TDESBuffer));
      Context.TailLen := 0;
    end;
  end;
  while ChunkSize >= SizeOf(TDESBuffer) do
  begin
    Encrypt(PDESBuffer(Chunk)^, Context.ExpandedKey, PDESBuffer(OutBuf)^);
    Inc(PtrUInt(Chunk), SizeOf(TDESBuffer));
    Inc(PtrUInt(OutBuf), SizeOf(TDESBuffer));
    Dec(ChunkSize, SizeOf(TDESBuffer));
  end;
  if ChunkSize > 0 then
  begin
    Move(Chunk^, Context.Tail[0], ChunkSize);
    Context.TailLen := ChunkSize;
  end;
  OutSize := Needed;
  Result := True;
end;

function DecryptECB(var Context: TDESContext; Chunk: pointer; ChunkSize:
  cardinal;
  OutBuf: pointer; var OutSize: cardinal): boolean;
var
  Left, Needed: cardinal;
begin
  if ChunkSize = 0 then
  begin
    OutSize := 0;
    Result := True;
    exit;
  end;
  Needed := ((Context.TailLen + ChunkSize) div SizeOf(TDESBuffer)) *
    SizeOf(TDESBuffer);
  if Needed > OutSize then
  begin
    OutSize := Needed;
    Result := False;
    exit;
  end;
  if Context.TailLen > 0 then
  begin
    Left := SizeOf(TDESBuffer) - Context.TailLen;
    if Left > ChunkSize then
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], ChunkSize);
      Inc(Context.TailLen, ChunkSize);
      ChunkSize := 0;
    end
    else
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], Left);
      Dec(ChunkSize, Left);
      Decrypt(Context.Tail, Context.ExpandedKey, PDESBuffer(OutBuf)^);
      Inc(PtrUInt(Chunk), Left);
      Inc(PtrUInt(OutBuf), SizeOf(TDESBuffer));
      Context.TailLen := 0;
    end;
  end;
  while ChunkSize >= SizeOf(TDESBuffer) do
  begin
    Decrypt(PDESBuffer(Chunk)^, Context.ExpandedKey, PDESBuffer(OutBuf)^);
    Inc(PtrUInt(Chunk), SizeOf(TDESBuffer));
    Inc(PtrUInt(OutBuf), SizeOf(TDESBuffer));
    Dec(ChunkSize, SizeOf(TDESBuffer));
  end;
  if ChunkSize > 0 then
  begin
    Move(Chunk^, Context.Tail[0], ChunkSize);
    Context.TailLen := ChunkSize;
  end;
  OutSize := Needed;
  Result := True;
end;

function FinalizeEncryptionECB(var Context: TDESContext; OutBuf: pointer;
  var OutSize: cardinal): boolean;
begin
  if Context.TailLen > 0 then
  begin
    if OutSize < SizeOf(TDESBuffer) then
    begin
      OutSize := SizeOf(TDESBuffer);
      Result := False;
    end
    else
    begin
      FillChar(Context.Tail[Context.TailLen], SizeOf(TDESBuffer) -
        Context.TailLen, 0);
      Encrypt(Context.Tail, Context.ExpandedKey, PDESBuffer(OutBuf)^);
      OutSize := SizeOf(TDESBuffer);
      Result := True;
    end;
  end
  else
  begin
    OutSize := 0;
    Result := True;
  end;
end;

function FinalizeDecryptionECB(var Context: TDESContext; OutBuf: pointer;
  var OutSize: cardinal): boolean;
begin
  if Context.TailLen > 0 then
  begin
    if Context.TailLen < SizeOf(TDESBuffer) then
    begin
      OutSize := 0;
      Result := False;
    end
    else
      if OutSize < SizeOf(TDESBuffer) then
      begin
        OutSize := SizeOf(TDESBuffer);
        Result := False;
      end
      else
      begin
        Decrypt(Context.Tail, Context.ExpandedKey, PDESBuffer(OutBuf)^);
        OutSize := SizeOf(TDESBuffer);
        Result := True;
      end;
  end
  else
  begin
    OutSize := 0;
    Result := True;
  end;
end;

// ************** CBC Mode

procedure InitializeEncryptionCBC(var Context: TDESContext; const Key: TDESKey;
  const InitVector: TDESBuffer);
begin

  ExpandKeyForEncryption(Key, Context.ExpandedKey);
  Context.Vector := InitVector;
  Context.TailLen := 0;
end;

procedure InitializeDecryptionCBC(var Context: TDESContext; Key: TDESKey;
  InitVector: TDESBuffer);
begin

  ExpandKeyForDecryption(Key, Context.ExpandedKey);
  Context.Vector := InitVector;
  Context.TailLen := 0;
end;


function EncryptCBC(var Context: TDESContext; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean;
var
  Left, Needed: cardinal;
  Temp: TDESBuffer;
begin
  if ChunkSize = 0 then
  begin
    OutSize := 0;
    Result := True;
    exit;
  end;
  Needed := ((Context.TailLen + ChunkSize) div SizeOf(TDESBuffer)) *
    SizeOf(TDESBuffer);
  if Needed > OutSize then
  begin
    OutSize := Needed;
    Result := False;
    exit;
  end;
  if Context.TailLen > 0 then
  begin
    Left := SizeOf(TDESBuffer) - Context.TailLen;
    if Left > ChunkSize then
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], ChunkSize);
      Inc(Context.TailLen, ChunkSize);
      ChunkSize := 0;
    end
    else
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], Left);
      Dec(ChunkSize, Left);
      PLongWord(@Temp[0])^ := PLongWord(@Context.Tail[0])^ xor
        PLongWord(@Context.Vector[0])^;
      PLongWord(@Temp[4])^ := PLongWord(@Context.Tail[4])^ xor
        PLongWord(@Context.Vector[4])^;
      Encrypt(Temp, Context.ExpandedKey, PDESBuffer(OutBuf)^);
      Context.Vector := PDESBuffer(OutBuf)^;
      Inc(PtrUInt(OutBuf), SizeOf(TDESBuffer));
      Inc(PtrUInt(Chunk), Left);
      Context.TailLen := 0;
    end;
  end;
  while ChunkSize >= SizeOf(TDESBuffer) do
  begin
    PLongWord(@Temp[0])^ := PLongWord(Chunk)^ xor
      PLongWord(@Context.Vector[0])^;
    Inc(PtrUInt(Chunk), 4);
    PLongWord(@Temp[4])^ := PLongWord(Chunk)^ xor
      PLongWord(@Context.Vector[4])^;
    Inc(PtrUInt(Chunk), 4);
    Encrypt(Temp, Context.ExpandedKey, PDESBuffer(OutBuf)^);
    Context.Vector := PDESBuffer(OutBuf)^;
    Inc(PtrUInt(OutBuf), SizeOf(TDESBuffer));
    Dec(ChunkSize, SizeOf(TDESBuffer));
  end;
  if ChunkSize > 0 then
  begin
    Move(Chunk^, Context.Tail[0], ChunkSize);
    Context.TailLen := ChunkSize;
  end;
  OutSize := Needed;
  Result := True;
end;


function DecryptCBC(var Context: TDESContext; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean;
var
  Left, Needed: cardinal;
  NextVector: TDESBuffer;
begin
  if ChunkSize = 0 then
  begin
    OutSize := 0;
    Result := True;
    exit;
  end;
  Needed := ((Context.TailLen + ChunkSize) div SizeOf(TDESBuffer)) *
    SizeOf(TDESBuffer);
  if Needed > OutSize then
  begin
    OutSize := Needed;
    Result := False;
    exit;
  end;
  if Context.TailLen > 0 then
  begin
    Left := SizeOf(TDESBuffer) - Context.TailLen;
    if Left > ChunkSize then
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], ChunkSize);
      Inc(Context.TailLen, ChunkSize);
      ChunkSize := 0;
    end
    else
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], Left);
      Inc(PtrUInt(Chunk), Left);
      Dec(ChunkSize, Left);
      NextVector := Context.Tail;
      Decrypt(Context.Tail, Context.ExpandedKey, PDESBuffer(OutBuf)^);
      PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
        PLongWord(@Context.Vector[0])^;
      Inc(PtrUInt(OutBuf), 4);
      PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
        PLongWord(@Context.Vector[4])^;
      Inc(PtrUInt(OutBuf), 4);
      Context.Vector := NextVector;
      Context.TailLen := 0;
    end;
  end;
  while ChunkSize >= SizeOf(TDESBuffer) do
  begin
    NextVector := PDESBuffer(Chunk)^;
    Decrypt(PDESBuffer(Chunk)^, Context.ExpandedKey, PDESBuffer(OutBuf)^);
    PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor PLongWord(@Context.Vector[0])^;
    Inc(PtrUInt(OutBuf), 4);
    PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor PLongWord(@Context.Vector[4])^;
    Inc(PtrUInt(OutBuf), 4);
    Context.Vector := NextVector;
    Inc(PtrUInt(Chunk), SizeOf(TDESBuffer));
    Dec(ChunkSize, SizeOf(TDESBuffer));
  end;
  if ChunkSize > 0 then
  begin
    Move(Chunk^, Context.Tail[0], ChunkSize);
    Context.TailLen := ChunkSize;
  end;
  OutSize := Needed;
  Result := True;
end;


function FinalizeEncryptionCBC(var Context: TDESContext; OutBuf: pointer;
  var OutSize: cardinal): boolean;
begin
  if Context.TailLen > 0 then
  begin
    if OutSize < SizeOf(TDESBuffer) then
    begin
      OutSize := SizeOf(TDESBuffer);
      Result := False;
    end
    else
    begin
      FillChar(Context.Tail[Context.TailLen], SizeOf(TDESBuffer) -
        Context.TailLen, 0);
      PLongWord(@Context.Tail[0])^ := PLongWord(@Context.Tail[0])^ xor
        PLongWord(@Context.Vector[0])^;
      PLongWord(@Context.Tail[4])^ := PLongWord(@Context.Tail[4])^ xor
        PLongWord(@Context.Vector[4])^;
      Encrypt(Context.Tail, Context.ExpandedKey, PDESBuffer(OutBuf)^);
      OutSize := SizeOf(TDESBuffer);
      Result := True;
    end;
  end
  else
  begin
    OutSize := 0;
    Result := True;
  end;
end;

function FinalizeDecryptionCBC(var Context: TDESContext; OutBuf: pointer;
  var OutSize: cardinal): boolean;
begin
  if Context.TailLen > 0 then
  begin
    if Context.TailLen < SizeOf(TDESBuffer) then
    begin
      OutSize := 0;
      Result := False;
    end
    else
      if OutSize < SizeOf(TDESBuffer) then
      begin
        OutSize := SizeOf(TDESBuffer);
        Result := False;
      end
      else
      begin
        Decrypt(Context.Tail, Context.ExpandedKey, PDESBuffer(OutBuf)^);
        PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
          PLongWord(@Context.Vector[0])^;
        Inc(PtrUInt(OutBuf), 4);
        PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
          PLongWord(@Context.Vector[4])^;
        OutSize := SizeOf(TDESBuffer);
        Result := True;
      end;
  end
  else
  begin
    OutSize := 0;
    Result := True;
  end;
end;


// Stream processing routines

// ECB Mode
procedure EncryptStreamECB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; Dest: TStream);
var
  TempIn, TempOut: TDESBuffer;
begin
  if Count = 0 then
  begin
    Source.Position := 0;
    Count := Source.Size;
  end
  else
    Count := Min(Count, Cardinal(Source.Size - Source.Position));
  if Count = 0 then
    exit;
  while Count >= SizeOf(TDESBuffer) do
  begin
    Source.ReadBuffer(TempIn, SizeOf(TempIn));
    Encrypt(TempIn, ExpandedKey, TempOut);
    Dest.WriteBuffer(TempOut, SizeOf(TempOut));
    Dec(Count, SizeOf(TDESBuffer));
  end;
  if Count > 0 then
  begin
    Source.ReadBuffer(TempIn, Count);
    FillChar(TempIn[Count], SizeOf(TempIn) - Count, 0);
    Encrypt(TempIn, ExpandedKey, TempOut);
    Dest.WriteBuffer(TempOut, SizeOf(TempOut));
  end;
end;

procedure DecryptStreamECB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; Dest: TStream);
var
  TempIn, TempOut: TDESBuffer;
begin
  if Count = 0 then
  begin
    Source.Position := 0;
    Count := Source.Size;
  end
  else
    Count := Min(Count, Cardinal(Source.Size - Source.Position));
  if Count = 0 then
    exit;
  if (Count mod SizeOf(TDESBuffer)) > 0 then
    raise EEncryptionError.Create(SInvalidInputSize);
  while Count >= SizeOf(TDESBuffer) do
  begin
    Source.ReadBuffer(TempIn, SizeOf(TempIn));
    Decrypt(TempIn, ExpandedKey, TempOut);
    Dest.WriteBuffer(TempOut, SizeOf(TempOut));
    Dec(Count, SizeOf(TDESBuffer));
  end;
end;

// CBC Mode

procedure EncryptStreamCBC(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream);
var
  TempIn, TempOut, Vector: TDESBuffer;
begin
  if Count = 0 then
  begin
    Source.Position := 0;
    Count := Source.Size;
  end
  else
    Count := Min(Count, Cardinal(Source.Size - Source.Position));
  if Count = 0 then
    exit;
  Vector := InitVector;
  while Count >= SizeOf(TDESBuffer) do
  begin
    Source.ReadBuffer(TempIn, SizeOf(TempIn));
    PLongWord(@TempIn[0])^ := PLongWord(@TempIn[0])^ xor PLongWord(@Vector[0])^;
    PLongWord(@TempIn[4])^ := PLongWord(@TempIn[4])^ xor PLongWord(@Vector[4])^;
    Encrypt(TempIn, ExpandedKey, TempOut);
    Dest.WriteBuffer(TempOut, SizeOf(TempOut));
    Vector := TempOut;
    Dec(Count, SizeOf(TDESBuffer));
  end;
  if Count > 0 then
  begin
    Source.ReadBuffer(TempIn, Count);
    FillChar(TempIn[Count], SizeOf(TempIn) - Count, 0);
    PLongWord(@TempIn[0])^ := PLongWord(@TempIn[0])^ xor PLongWord(@Vector[0])^;
    PLongWord(@TempIn[4])^ := PLongWord(@TempIn[4])^ xor PLongWord(@Vector[4])^;
    Encrypt(TempIn, ExpandedKey, TempOut);
    Dest.WriteBuffer(TempOut, SizeOf(TempOut));
  end;
end;

procedure DecryptStreamCBC(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream);
var
  TempIn, TempOut: TDESBuffer;
  Vector1, Vector2: TDESBuffer;
begin
  if Count = 0 then
  begin
    Source.Position := 0;
    Count := Source.Size;
  end
  else
    Count := Min(Count, Cardinal(Source.Size - Source.Position));
  if Count = 0 then
    exit;
  if (Count mod SizeOf(TDESBuffer)) > 0 then
    raise EEncryptionError.Create(SInvalidInputSize);
  Vector1 := InitVector;
  while Count >= SizeOf(TDESBuffer) do
  begin
    Source.ReadBuffer(TempIn, SizeOf(TempIn));
    Vector2 := TempIn;
    Decrypt(TempIn, ExpandedKey, TempOut);
    PLongWord(@TempOut[0])^ := PLongWord(@TempOut[0])^ xor
      PLongWord(@Vector1[0])^;
    PLongWord(@TempOut[4])^ := PLongWord(@TempOut[4])^ xor
      PLongWord(@Vector1[4])^;
    Dest.WriteBuffer(TempOut, SizeOf(TempOut));
    Vector1 := Vector2;
    Dec(Count, SizeOf(TDESBuffer));
  end;
end;

// CFB Mode

procedure EncryptStreamCFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream);
var
  TempIn, TempOut, Vector: TDESBuffer;
begin
  if Count = 0 then
  begin
    Source.Position := 0;
    Count := Source.Size;
  end
  else
    Count := Min(Count, Cardinal(Source.Size - Source.Position));
  if Count = 0 then
    exit;
  Vector := InitVector;
  while Count >= 1 do
  begin
    Source.ReadBuffer(TempIn, 1);
    Encrypt(Vector, ExpandedKey, TempOut);
    TempOut[0] := TempOut[0] xor TempIn[0];
    Dest.WriteBuffer(TempOut, 1);
    PInt64(@Vector[0])^ := PInt64(@Vector[0])^ shl 8;
    Vector[7] := TempOut[0];
    Dec(Count);
  end;
end;

procedure DecryptStreamCFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream);
var
  TempIn, TempOut: TDESBuffer;
  Vector: TDESBuffer;
begin
  if Count = 0 then
  begin
    Source.Position := 0;
    Count := Source.Size;
  end
  else
    Count := Min(Count, Cardinal(Source.Size - Source.Position));
  if Count = 0 then
    exit;
  Vector := InitVector;
  while Count >= 1 do
  begin
    Source.ReadBuffer(TempIn, 1);
    Encrypt(Vector, ExpandedKey, TempOut);
    TempOut[0] := TempOut[0] xor TempIn[0];
    Dest.WriteBuffer(TempOut, 1);
    PInt64(@Vector[0])^ := PInt64(@Vector[0])^ shl 8;
    Vector[7] := TempIn[0];
    Dec(Count);
  end;
end;

// CFB Mode w/ Step

procedure EncryptStreamCFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream; const Step: byte);
var
  TempIn, TempOut, Vector: TDESBuffer;
  I: byte;
begin
  if Count = 0 then
  begin
    Source.Position := 0;
    Count := Source.Size;
  end
  else
    Count := Min(Count, Cardinal(Source.Size - Source.Position));
  if Count = 0 then
    exit;
  Vector := InitVector;
  while Count >= Step do
  begin
    Source.ReadBuffer(TempIn, Step);
    Encrypt(Vector, ExpandedKey, TempOut);
    I := 0;
    repeat
      TempOut[I] := TempOut[I] xor TempIn[I];
      Inc(I);
    until I >= Step;
    Dest.WriteBuffer(TempOut, Step);
    I := 0;
    repeat
      Vector[I] := Vector[I + Step];
      Inc(I);
    until I > 7 - Step;
    repeat
      Vector[I] := TempIn[I - 8 + Step];
      Inc(I);
    until I >= 8;
    Dec(Count, Step);
  end;
  if Count > 0 then
  begin
    Source.ReadBuffer(TempIn, Count);
    Encrypt(Vector, ExpandedKey, TempOut);
    FillChar(TempIn[Count], Step - Count, 0);
    I := 0;
    repeat
      TempOut[I] := TempOut[I] xor TempIn[I];
      Inc(I);
    until I >= Step;
    Dest.WriteBuffer(TempOut, Step);
  end;
end;

procedure DecryptStreamCFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream; const Step: byte);
var
  TempIn, TempOut: TDESBuffer;
  Vector: TDESBuffer;
  I: Byte;
begin
  if Count = 0 then
  begin
    Source.Position := 0;
    Count := Source.Size;
  end
  else
    Count := Min(Count, Cardinal(Source.Size - Source.Position));
  if Count = 0 then
    exit;
  if (Count mod Step) > 0 then
    raise EEncryptionError.Create(SInvalidInputSize);
  Vector := InitVector;
  while Count >= Step do
  begin
    Source.ReadBuffer(TempIn, Step);
    Encrypt(Vector, ExpandedKey, TempOut);
    I := 0;
    repeat
      TempOut[I] := TempOut[I] xor TempIn[I];
      Inc(I);
    until I >= Step;
    Dest.WriteBuffer(TempOut, Step);
    I := 0;
    repeat
      Vector[I] := Vector[I + Step];
      Inc(I);
    until I > 7 - Step;
    repeat
      Vector[I] := TempIn[I - 8 + Step];
      Inc(I);
    until I >= 8;
    Dec(Count, Step);
  end;
end;

// OFB Mode

procedure EncryptStreamOFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream);
var
  TempIn, TempOut, Vector: TDESBuffer;
begin
  if Count = 0 then
  begin
    Source.Position := 0;
    Count := Source.Size;
  end
  else
    Count := Min(Count, Cardinal(Source.Size - Source.Position));
  if Count = 0 then
    exit;
  Vector := InitVector;
  while Count >= 1 do
  begin
    Source.ReadBuffer(TempIn, 1);
    Encrypt(Vector, ExpandedKey, TempOut);
    PInt64(@Vector[0])^ := PInt64(@Vector[0])^ shl 8;
    Vector[7] := TempOut[0];
    TempOut[0] := TempOut[0] xor TempIn[0];
    Dest.WriteBuffer(TempOut, 1);
    Dec(Count);
  end;
end;

procedure DecryptStreamOFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream);
var
  TempIn, TempOut, Vector: TDESBuffer;
begin
  if Count = 0 then
  begin
    Source.Position := 0;
    Count := Source.Size;
  end
  else
    Count := Min(Count, Cardinal(Source.Size - Source.Position));
  if Count = 0 then
    exit;
  Vector := InitVector;
  while Count >= 1 do
  begin
    Source.ReadBuffer(TempIn, 1);
    Encrypt(Vector, ExpandedKey, TempOut);
    PInt64(@Vector[0])^ := PInt64(@Vector[0])^ shl 8;
    Vector[7] := TempOut[0];
    TempOut[0] := TempOut[0] xor TempIn[0];
    Dest.WriteBuffer(TempOut, 1);
    Dec(Count);
  end;
end;

// OFB Mode w/ Step

procedure EncryptStreamOFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream; const Step: byte);
var
  TempIn, TempOut, Vector: TDESBuffer;
  I: byte;
begin
  if Count = 0 then
  begin
    Source.Position := 0;
    Count := Source.Size;
  end
  else
    Count := Min(Count, Cardinal(Source.Size - Source.Position));
  if Count = 0 then
    exit;
  Vector := InitVector;
  while Count >= Step do
  begin
    Source.ReadBuffer(TempIn, Step);
    Encrypt(Vector, ExpandedKey, TempOut);
    I := 0;
    repeat
      Vector[I] := Vector[I + Step];
      Inc(I);
    until I > 7 - Step;
    repeat
      Vector[I] := TempOut[I - 8 + Step];
      Inc(I);
    until I >= 8;
    I := 0;
    repeat
      TempOut[I] := TempOut[I] xor TempIn[I];
      Inc(I);
    until I >= Step;
    Dest.WriteBuffer(TempOut, Step);
    Dec(Count, Step);
  end;
  if Count > 0 then
  begin
    Source.ReadBuffer(TempIn, Count);
    Encrypt(Vector, ExpandedKey, TempOut);
    FillChar(TempIn[Count], Step - Count, 0);
    I := 0;
    repeat
      TempOut[I] := TempOut[I] xor TempIn[I];
      Inc(I);
    until I >= Step;
    Dest.WriteBuffer(TempOut, Step);
  end;
end;

procedure DecryptStreamOFB(Source: TStream; Count: cardinal;
  const ExpandedKey: TDESExpandedKey; const InitVector: TDESBuffer;
  Dest: TStream; const Step: byte);
var
  TempIn, TempOut, Vector: TDESBuffer;
  I: byte;
begin
  if Count = 0 then
  begin
    Source.Position := 0;
    Count := Source.Size;
  end
  else
    Count := Min(Count, Cardinal(Source.Size - Source.Position));
  if Count = 0 then
    exit;
  if (Count mod Step) > 0 then
    raise EEncryptionError.Create(SInvalidInputSize);
  Vector := InitVector;
  while Count >= Step do
  begin
    Source.ReadBuffer(TempIn, Step);
    Encrypt(Vector, ExpandedKey, TempOut);
    I := 0;
    repeat
      Vector[I] := Vector[I + Step];
      Inc(I);
    until I > 7 - Step;
    repeat
      Vector[I] := TempOut[I - 8 + Step];
      Inc(I);
    until I >= 8;
    I := 0;
    repeat
      TempOut[I] := TempOut[I] xor TempIn[I];
      Inc(I);
    until I >= Step;
    Dest.WriteBuffer(TempOut, Step);
    Dec(Count, Step);
  end;
end;




end.
