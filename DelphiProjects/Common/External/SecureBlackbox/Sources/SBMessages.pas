(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$i SecBbox.inc}

unit SBMessages;

interface

uses
    Classes,
    SysUtils,
    SBRDN,
    SBX509,
    SBUtils,
    SBCustomCertStorage,
    SBPKCS7,
    SBPKCS7Utils,
    {$ifndef B_6}
//    SBTSPCommon,
    SBTSPClient,
    {$endif}
    SBHashFunction,
    SBRandom
;


const
  // error codes
  SB_MESSAGE_ERROR_NO_ENCRYPTED_DATA            = Integer($2001);
  SB_MESSAGE_ERROR_NO_CERTIFICATE               = Integer($2002);
  SB_MESSAGE_ERROR_KEY_DECRYPTION_FAILED        = Integer($2003);
  SB_MESSAGE_ERROR_BUFFER_TOO_SMALL             = Integer($2004);
  SB_MESSAGE_ERROR_CONTENT_DECRYPTION_FAILED    = Integer($2005);
  SB_MESSAGE_ERROR_INVALID_FORMAT               = Integer($2006);
  SB_MESSAGE_ERROR_NO_RECIPIENTS                = Integer($2007);
  SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM        = Integer($2008);
  SB_MESSAGE_ERROR_ENCRYPTION_FAILED            = Integer($2009);
  SB_MESSAGE_ERROR_INVALID_KEY_LENGTH           = Integer($200A);
  SB_MESSAGE_ERROR_NO_SIGNED_DATA               = Integer($200B);
  SB_MESSAGE_ERROR_INVALID_SIGNATURE            = Integer($200C);
  SB_MESSAGE_ERROR_INVALID_DIGEST               = Integer($200D);
  SB_MESSAGE_ERROR_SIGNING_FAILED               = Integer($200E);
  SB_MESSAGE_ERROR_INTERNAL_ERROR               = Integer($200F);
  SB_MESSAGE_ERROR_INVALID_MAC                  = Integer($2010);
  SB_MESSAGE_ERROR_UNSUPPORTED_SIGNATURE_TYPE   = Integer($2011);
  SB_MESSAGE_ERROR_INVALID_COUNTERSIGNATURE     = Integer($2012);
  SB_MESSAGE_ERROR_DIGEST_NOT_FOUND             = Integer($2013);
  SB_MESSAGE_ERROR_UNSUPPORTED_DIGEST_ALGORITHM = Integer($2014);


type
  {
    The following class contains shared functionality for other
    ElMessage* classes. It should not be instantiated.
  }
  TElMessageProcessor = class(TSBControlBase)
  protected
    FUseOAEP : boolean;

    function EncryptRSA(const Key : BufferType; Certificate : TElX509Certificate;
      var EncryptedKey : BufferType) : boolean; overload;
    function EncryptRSAOAEP(const Key : BufferType; Certificate : TElX509Certificate;
      var EncryptedKey : BufferType) : boolean; overload;
    function EncryptRSA(Certificate : TElX509Certificate; Digest : pointer;
      DigestSize : integer; const OID : BufferType; var EncryptedDigest : BufferType): boolean; overload;
    function DecryptRSA(Certificate : TElX509Certificate; Recipient : TElPKCS7Recipient;
      var Key : BufferType) : boolean; overload;
    function DecryptRSAOAEP(Certificate : TElX509Certificate; Recipient : TElPKCS7Recipient;
      var Key : BufferType) : boolean; overload;
    function DecryptRSAForSigner(Certificate : TElX509Certificate; Signer : TElPKCS7Signer;
      var Digest : BufferType) : boolean; overload;
    function VerifyDSA(Certificate : TElX509Certificate; Signer : TElPKCS7Signer;
      Digest : pointer; Size: integer) : boolean;
    function VerifyRSAPSS(Certificate : TElX509Certificate; Signer : TElPKCS7Signer;
      Digest : pointer; Size: integer; HashAlgorithm : integer; SaltSize : integer) : boolean;
    function EncryptKey(const Key : BufferType;
      Certificate : TElX509Certificate; var EncryptedKey : BufferType) : boolean;
    function DecryptKey(Certificate : TElX509Certificate; Recipient : TElPKCS7Recipient;
      var Key : BufferType): boolean;
    function FillRecipient(Recipient : TElPKCS7Recipient; Certificate :
      TElX509Certificate; const Key : BufferType) : boolean;
    function CalculateMAC(Buffer: pointer; Size: integer; const Key : BufferType;
      var Mac : BufferType; MacAlg: integer) : boolean;
    function CalculateDigest(Buffer: pointer; Size: integer; Alg: integer): BufferType;
  end;

  {
    The following class is used to encrypt data and save it in PKCS7 format.
    CertStorage should contain recipients' certificates (without private keys)
  }
  TElMessageEncryptor = class(TElMessageProcessor)
  private
    FCertStorage : TElCustomCertStorage;
    FAlgorithm : integer;
    FBitsInKey : integer;
    FRandom : TElRandom;
    FUseUndefSize : boolean;
  protected
    procedure SetCertStorage(Value : TElCustomCertStorage);
    procedure Notification(AComponent : TComponent; AOperation : TOperation); override;
    procedure GenerateContentKey(KeyBuffer : pointer; KeySize : integer;
      IVBuffer : pointer; IVSize : integer);
    function EncryptContent(InBuffer : pointer; InSize : integer; OutBuffer :
      pointer; var OutSize : integer; const Key : BufferType; const IV : BufferType) : boolean;
    function FillRC2Params(KeyLen : integer; const IV : BufferType) : BufferType;

    {$ifndef BUILDER_USED}
    function ChooseEncryptionAlgorithm(Algs : array of TSBArrayOfPairs; var Bits :
      integer) : integer;
    {$else}
    function ChooseEncryptionAlgorithm(Algs : pointer; Count: integer;
      var Bits : integer) : integer;
    {$endif}



  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
    function Encrypt(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer) : integer; overload;
    function Encrypt(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer; Key : pointer; KeySize : integer) : integer; overload;
  published
    property CertStorage : TElCustomCertStorage read FCertStorage
      write SetCertStorage;
    property Algorithm : integer read FAlgorithm write FAlgorithm;
    property BitsInKey : integer read FBitsInKey write FBitsInKey;
    property UseUndefSize: Boolean read FUseUndefSize write FUseUndefSize default true;
    property UseOAEP : boolean read FUseOAEP write FUseOAEP;
  end;

  {
    The following class is used to decrypt data stored in PKCS7 format.
    CertStorage should contain a list of certificates with corresponding
    private keys.
  }
  TElMessageDecryptor = class(TElMessageProcessor)
  private
    FCertStorage : TElMemoryCertStorage;
    FAlgorithm : integer;
    FBitsInKey : integer;
    FUsedCertificate : integer;
    FCertIDs : TList;
  protected
    {$ifdef SB_HAS_WINCRYPT}
    {$ifndef NET_CF}
    function DecryptWin32(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer; Cert : TElX509Certificate) : integer;
    {$endif}
    {$endif}

    function DecryptContent(Content : TElPKCS7EncryptedContent; const Key : BufferType;
      OutBuffer : pointer; var OutSize : integer) : boolean;
    function GetRC2KeyLengthByIdentifier(const Id : BufferType) : cardinal;
    procedure SetCertStorage(Value : TElMemoryCertStorage);
    procedure Notification(AComponent : TComponent; AOperation : TOperation); override;
    function GetCertIDs(Index : integer) : TElPKCS7Issuer;
    procedure ClearCertIDs;
    function GetCertIDCount : integer;
    function GetUsedCertificate : integer;

  
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
    function Decrypt(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer) : integer; overload;
    function Decrypt(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer; Key : pointer; KeySize : integer) : integer; overload;
    class function IsConventionallyEncrypted(Buffer: pointer; Size: integer): boolean;

    property Algorithm : integer read FAlgorithm;
    property BitsInKey : integer read FBitsInKey;
    property CertIDs[Index : integer] : TElPKCS7Issuer read GetCertIDs;
    property CertIDCount : integer read GetCertIDCount;
    property UsedCertificate : integer read GetUsedCertificate;
    property UseOAEP : boolean read FUseOAEP;
  published
    property CertStorage : TElMemoryCertStorage read FCertStorage write
      SetCertStorage;
  end;

  {
    The following class is used to verify digital signatures.
    Signature should be stored in PKCS7 format.
    CertStorage should contain a list of trusted certificates.
    Use "Certificates" property to access certificates which are
    included to message. This property is updated after each call to
    "Verify".
  }
  TSBMessageSignatureType = 
    (mstPublicKey, mstMAC);

  TSBVerificationOption = (voUseEmbeddedCerts, voUseLocalCerts, voVerifyMessageDigests,
    voVerifyTimestamps);
  TSBVerificationOptions = set of TSBVerificationOption;

  TElMessageVerifier = class(TElMessageProcessor)
  private
    FUsePSS : boolean;
    FCertStorage : TElCustomCertStorage;
    FCertificates : TElMemoryCertStorage;
    FAttributes : TElPKCS7Attributes;
    FAlgorithm : integer;
    FMacAlgorithm : integer;
    FVerifyCountersignatures : boolean;
    FInputIsDigest : boolean;
    FCertIDs : TList;
    FCSCertIDs : TList;
    FTimestamps : TList;
    FSignatureType : TSBMessageSignatureType;
    FVerificationOptions : TSBVerificationOptions;
  protected
    procedure SetCertStorage(Value : TElCustomCertStorage);

    procedure Notification(AComponent : TComponent; AOperation : TOperation); override;

    function VerifySingle(Signer : TElPKCS7Signer; Digest : pointer;
      DigestSize : integer; Countersign : boolean = false) : integer;
    function VerifyMessageDigests(Msg : TElPKCS7Message; Buffer: pointer;
      Size: integer) : integer;
    {$ifndef B_6}
    function VerifyTimestamps(Signer: TElPKCS7Signer): integer;
    {$endif}
    function GetCertIDs(Index : integer) : TElPKCS7Issuer;
    function GetCertIDCount : integer;
    function GetCountersignatureCertIDs(Index : integer) : TElPKCS7Issuer;
    function GetCountersignatureCertIDCount : integer;
    procedure ClearCertIDs;
    function ExtractMACKey(AuthData : TElPKCS7AuthenticatedData;
      var Key : BufferType): integer;
    {$ifndef B_6}
    function GetTimestamp(Index: integer): TElClientTSPInfo;
    function GetTimestampCount: integer;
    procedure ClearTimestamps;
    {$endif}
  
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
    function Verify(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer) : integer; virtual;
    function VerifyDetached(Buffer : pointer; Size : integer; Signature : pointer;
      SignatureSize : integer) : integer; virtual;
    class function IsSignatureDetached(Signature : pointer; Size : integer) : boolean;
    property Certificates : TElMemoryCertStorage read FCertificates;
    property Attributes : TElPKCS7Attributes read FAttributes;
    property HashAlgorithm : integer read FAlgorithm;
    property MacAlgorithm : integer read FMacAlgorithm;
    property CertIDs[Index : integer] : TElPKCS7Issuer read GetCertIDs;
    property CertIDCount : integer read GetCertIDCount;
    property CountersignatureCertIDs[Index : integer] : TElPKCS7Issuer
      read GetCountersignatureCertIDs;
    property CountersignatureCertIDCount : integer read GetCountersignatureCertIDCount;
    property SignatureType : TSBMessageSignatureType read FSignatureType;
    property UsePSS : boolean read FUsePSS;
    property InputIsDigest : boolean read FInputIsDigest write FInputIsDigest default false;
    {$ifndef B_6}
    property Timestamps[Index: integer] : TElClientTSPInfo read GetTimestamp;
    property TimestampCount : integer read GetTimestampCount;
    {$endif}
  published
    property CertStorage : TElCustomCertStorage read FCertStorage
      write SetCertStorage;
    property VerifyCountersignatures : boolean read FVerifyCountersignatures
      write FVerifyCountersignatures;
    property VerificationOptions : TSBVerificationOptions read FVerificationOptions
      write FVerificationOptions default [voUseEmbeddedCerts,
      voUseLocalCerts, voVerifyMessageDigests];
  end;

  TSBSigningOption = (soInsertMessageDigests);
  TSBSigningOptions = set of TSBSigningOption;

  {
    The following class is used to sign messages.
    CertStorage should contain at least one certificate with private key
    (and any amount of certificates without private keys).
    Message will be signed with every certificate that has a corresponding
    private key.
    Other certificates may be used to specify the chain from trusted root
    certificate.
  }
  TElMessageSigner = class(TElMessageProcessor)
  private
    FUsePSS : boolean;
    FCertStorage : TElMemoryCertStorage;
    FRecipientCerts : TElCustomCertStorage;
    FAAttributes : TElPKCS7Attributes;
    FUAttributes : TElPKCS7Attributes;
    FAlgorithm : integer;
    FMacAlgorithm : integer;
    FIncludeCertificates : boolean;
    FSignatureType : TSBMessageSignatureType;
    FContentType : BufferType;
    FUseUndefSize : boolean;
    FSigningOptions : TSBSigningOptions;
    {$ifndef B_6}
    FTSPClient : TElCustomTSPClient;
    {$endif}
  protected
    procedure SetCertStorage(Value : TElMemoryCertStorage);
    procedure SetRecipientCerts(Value : TElCustomCertStorage);
    {$ifndef B_6}
    procedure SetTSPClient(Value : TElCustomTSPClient);
    {$endif}
    procedure Notification(AComponent : TComponent; AOperation : TOperation); override;
    function FillSigner(Signer : TElPKCS7Signer; Certificate : TElX509Certificate;
      const DigestAlgorithm : BufferType; Hash : pointer; HashSize : integer): boolean;
    function SignDSA(Certificate : TElX509Certificate; Digest : pointer;
      DigestSize: integer; var Signature : BufferType) : boolean;
    function SignRSAPSS(Certificate : TElX509Certificate;
      Digest : pointer; DigestSize: integer; var Signature : BufferType) : boolean;
    function CalculateEstimatedSize(InputSize : integer; Detached: boolean) : integer;
    {$ifndef B_6}
    function TimestampMessage(Msg : TElPKCS7Message) : integer;
    {$endif}

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
    function Sign(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer; Detached : boolean = false) : integer; virtual;
    function Countersign(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer) : integer; virtual;
    property AuthenticatedAttributes : TElPKCS7Attributes read FAAttributes;
    property UnauthenticatedAttributes : TElPKCS7Attributes read FUAttributes;
    property HashAlgorithm : integer read FAlgorithm write FAlgorithm;
    property MacAlgorithm : integer read FMacAlgorithm write FMacAlgorithm;
    property ContentType : BufferType read FContentType write FContentType;
  published
    property SignatureType : TSBMessageSignatureType read FSignatureType
      write FSignatureType default mstPublicKey;
    property CertStorage : TElMemoryCertStorage read FCertStorage
      write SetCertStorage;
    property IncludeCertificates: boolean read FIncludeCertificates
      write FIncludeCertificates default true;
    property RecipientCerts : TElCustomCertStorage read FRecipientCerts
      write SetRecipientCerts;
    property UseUndefSize: Boolean read FUseUndefSize write FUseUndefSize default true;
    property UsePSS : Boolean read FUsePSS write FUsePSS;
    property SigningOptions : TSBSigningOptions read FSigningOptions
      write FSigningOptions default [soInsertMessageDigests];
    {$ifndef B_6}
    property TSPClient : TElCustomTSPClient read FTSPClient write SetTSPClient;
    {$endif}
  end;

function FormatAttributeValue(TagID : integer; Value : BufferType) : BufferType; 
function UnformatAttributeValue(Value : BufferType; out TagID : integer) : BufferType; 

function CompareRDN(Name1, Name2 : TElRelativeDistinguishedName) : boolean; 

procedure Register;

implementation

uses
  SBRSA, SBDSA, SBConstants, SBRC4, 
  {$ifndef SBB_NO_DES}
  SBDES, SB3DES,
  {$endif}

  SBASN1Tree, SBRC2, SBAES,
  {$ifndef SBB_NO_PKCS11}
  SBPKCS11CertStorage,
  {$endif}
  {$ifndef CLX_USED}Windows, {$endif} 
  {$ifdef SB_HAS_WINCRYPT}SBWinCrypt,{$endif}
  SBSHA, SBSHA2, SBMD, SBHMAC, SBPKICommon;

const
  RC2KeyLength2Identifiers : array[0..255] of byte = (
    $bd, $56, $ea, $f2, $a2, $f1, $ac, $2a, $b0, $93, $d1, $9c, $1b, $33, $fd, $d0,
    $30, $04, $b6, $dc, $7d, $df, $32, $4b, $f7, $cb, $45, $9b, $31, $bb, $21, $5a,
    $41, $9f, $e1, $d9, $4a, $4d, $9e, $da, $a0, $68, $2c, $c3, $27, $5f, $80, $36,
    $3e, $ee, $fb, $95, $1a, $fe, $ce, $a8, $34, $a9, $13, $f0, $a6, $3f, $d8, $0c,
    $78, $24, $af, $23, $52, $c1, $67, $17, $f5, $66, $90, $e7, $e8, $07, $b8, $60,
    $48, $e6, $1e, $53, $f3, $92, $a4, $72, $8c, $08, $15, $6e, $86, $00, $84, $fa,
    $f4, $7f, $8a, $42, $19, $f6, $db, $cd, $14, $8d, $50, $12, $ba, $3c, $06, $4e,
    $ec, $b3, $35, $11, $a1, $88, $8e, $2b, $94, $99, $b7, $71, $74, $d3, $e4, $bf,
    $3a, $de, $96, $0e, $bc, $0a, $ed, $77, $fc, $37, $6b, $03, $79, $89, $62, $c6,
    $d7, $c0, $d2, $7c, $6a, $8b, $22, $a3, $5b, $05, $5d, $02, $75, $d5, $61, $e3,
    $18, $8f, $55, $51, $ad, $1f, $0b, $5e, $85, $e5, $c2, $57, $63, $ca, $3d, $6c,
    $b4, $c5, $cc, $70, $b2, $91, $59, $0d, $47, $20, $c8, $4f, $58, $e0, $01, $e2,
    $16, $38, $c4, $6f, $3b, $0f, $65, $46, $be, $7e, $2d, $7b, $82, $f9, $40, $b5,
    $1d, $73, $f8, $eb, $26, $c7, $87, $97, $25, $54, $b1, $28, $aa, $98, $9d, $a5,
    $64, $6d, $7a, $d4, $10, $81, $44, $ef, $49, $d6, $ae, $2e, $dd, $76, $5c, $2f,
    $a7, $1c, $c9, $09, $69, $9a, $83, $cf, $29, $39, $b9, $e9, $4c, $ff, $43, $ab
  );
  
  RC2Identifiers2KeyLength : array[0..255] of byte = (
    $5D, $BE, $9B, $8B, $11, $99, $6E, $4D, $59, $F3, $85, $A6, $3F, $B7, $83, $C5,
    $E4, $73, $6B, $3A, $68, $5A, $C0, $47, $A0, $64, $34, $0C, $F1, $D0, $52, $A5,
    $B9, $1E, $96, $43, $41, $D8, $D4, $2C, $DB, $F8, $07, $77, $2A, $CA, $EB, $EF,
    $10, $1C, $16, $0D, $38, $72, $2F, $89, $C1, $F9, $80, $C4, $6D, $AE, $30, $3D,
    $CE, $20, $63, $FE, $E6, $1A, $C7, $B8, $50, $E8, $24, $17, $FC, $25, $6F, $BB,
    $6A, $A3, $44, $53, $D9, $A2, $01, $AB, $BC, $B6, $1F, $98, $EE, $9A, $A7, $2D,
    $4F, $9E, $8E, $AC, $E0, $C6, $49, $46, $29, $F4, $94, $8A, $AF, $E1, $5B, $C3,
    $B3, $7B, $57, $D1, $7C, $9C, $ED, $87, $40, $8C, $E2, $CB, $93, $14, $C9, $61,
    $2E, $E5, $CC, $F6, $5E, $A8, $5C, $D6, $75, $8D, $62, $95, $58, $69, $76, $A1,
    $4A, $B5, $55, $09, $78, $33, $82, $D7, $DD, $79, $F5, $1B, $0B, $DE, $26, $21,
    $28, $74, $04, $97, $56, $DF, $3C, $F0, $37, $39, $DC, $FF, $06, $A4, $EA, $42,
    $08, $DA, $B4, $71, $B0, $CF, $12, $7A, $4E, $FA, $6C, $1D, $84, $00, $C8, $7F,
    $91, $45, $AA, $2B, $C2, $B1, $8F, $D5, $BA, $F2, $AD, $19, $B2, $67, $36, $F7,
    $0F, $0A, $92, $7D, $E3, $9D, $E9, $90, $3E, $23, $27, $66, $13, $EC, $81, $15,
    $BD, $22, $BF, $9F, $7E, $A9, $51, $4B, $4C, $FB, $02, $D3, $70, $86, $31, $E7,
    $3B, $05, $03, $54, $60, $48, $65, $18, $D2, $CD, $5F, $32, $88, $0E, $35, $FD
  );

procedure Register;
begin
  RegisterComponents('PKIBlackbox', [TElMessageEncryptor,
    TElMessageDecryptor, TElMessageSigner, TElMessageVerifier]);
end;

////////////////////////////////////////////////////////////////////////////////
// TElMessageProcessor class

function TElMessageProcessor.EncryptRSA(const Key : BufferType;
  Certificate : TElX509Certificate; var EncryptedKey : BufferType) : boolean;
var
  RSAM, RSAE : ByteArray;
  RSAMSize, RSAESize, OutSz, Indent : integer;
begin
  Result := false;
  RSAMSize := 0;
  RSAESize := 0;
  Certificate.GetRSAParams(nil, RSAMSize, nil, RSAESize);
  SetLength(RSAM, RSAMSize);
  SetLength(RSAE, RSAESize);
  if not Certificate.GetRSAParams(@RSAM[0], RSAMSize, @RSAE[0], RSAESize) then
    Exit;
  Indent := 0;
  while RSAM[Indent] = 0 do
    Inc(Indent);
  SetLength(EncryptedKey, RSAMSize - Indent);
  OutSz := RSAMSize - Indent;
  if not SBRSA.Encrypt(@Key[1], Length(Key), @RSAM[Indent], RSAMSize - Indent,
    @RSAE[0], RSAESize, @EncryptedKey[1], OutSz) then
    Exit;
  SetLength(EncryptedKey, OutSz);
  Result := true;
end;

function TElMessageProcessor.EncryptRSAOAEP(const Key : BufferType;
  Certificate : TElX509Certificate; var EncryptedKey : BufferType) : boolean;
var
  RSAM, RSAE : ByteArray;
  RSAMSize, RSAESize, OutSz, Indent : integer;
  StrLabel : pointer;
begin
  Result := false;
  RSAMSize := 0;
  RSAESize := 0;
  Certificate.GetRSAParams(nil, RSAMSize, nil, RSAESize);
  SetLength(RSAM, RSAMSize);
  SetLength(RSAE, RSAESize);
  if not Certificate.GetRSAParams(@RSAM[0], RSAMSize, @RSAE[0], RSAESize) then
    Exit;
  Indent := 0;
  while RSAM[Indent] = 0 do
    Inc(Indent);
  SetLength(EncryptedKey, RSAMSize - Indent);
  OutSz := RSAMSize - Indent;
  if Length(Certificate.OAEPParams.StrLabel) > 0 then
    StrLabel := @Certificate.OAEPParams.StrLabel[1]
  else
    StrLabel := nil;

  if not SBRSA.EncryptOAEP(@Key[1], Length(Key), @RSAM[Indent], RSAMSize - Indent,
    @RSAE[0], RSAESize, StrLabel, Length(Certificate.OAEPParams.StrLabel),
    Certificate.OAEPParams.HashAlgorithm, @EncryptedKey[1], OutSz) then
    Exit;
  SetLength(EncryptedKey, OutSz);
  Result := true;
end;

function TElMessageProcessor.EncryptKey(const Key : BufferType;
  Certificate : TElX509Certificate; var EncryptedKey : BufferType) : boolean;
begin
  if (Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) or
    ((Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) and FUseOAEP) then
    Result := EncryptRSAOAEP(Key, Certificate, EncryptedKey)
  else
  if Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    Result := EncryptRSA(Key, Certificate, EncryptedKey)
  else
    Result := false;
end;

function TElMessageProcessor.FillRecipient(Recipient : TElPKCS7Recipient; Certificate :
  TElX509Certificate; const Key : BufferType) : boolean;
var
  I : integer;
  EK : BufferType;
begin
  Result := false;
  Recipient.Version := 0;
  Recipient.Issuer.SerialNumber := CloneBuffer(Certificate.SerialNumber);
  Recipient.Issuer.Issuer.Count := Certificate.IssuerRDN.Count;
  for I := 0 to Certificate.IssuerRDN.Count - 1 do
  begin
    Recipient.Issuer.Issuer.OIDs[I] := CloneBuffer(Certificate.IssuerRDN.OIDs[I]);
    Recipient.Issuer.Issuer.Values[I] := CloneBuffer(Certificate.IssuerRDN.Values[I]);
    Recipient.Issuer.Issuer.Tags[I] := Certificate.IssuerRDN.Tags[I];
  end;

  if (Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) or
    ((Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) and FUseOAEP) then
  begin
    Recipient.KeyEncryptionAlgorithm := GetOIDByPKAlgorithm(SB_CERT_ALGORITHM_ID_RSAOAEP);
    Recipient.KeyEncryptionAlgorithmParams := WriteOAEPParams(Certificate.OAEPParams.HashAlgorithm,
      Certificate.OAEPParams.MGFHashAlgorithm, Certificate.OAEPParams.StrLabel);
    if not EncryptKey(Key, Certificate, EK) then
      Exit;
    Recipient.EncryptedKey := EK;
  end
  else
  if Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
  begin
    Recipient.KeyEncryptionAlgorithm := GetOIDByPKAlgorithm(SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION);
    Recipient.KeyEncryptionAlgorithmParams := EmptyBuffer;
    if not EncryptKey(Key, Certificate, EK) then
      Exit;
    Recipient.EncryptedKey := EK;
  end;
  Result := true;
end;

function TElMessageProcessor.DecryptKey(Certificate : TElX509Certificate;
  Recipient : TElPKCS7Recipient; var Key : BufferType) : boolean;
var
  Alg : integer;
  {$ifndef SBB_NO_PKCS11}
  Size: integer;
  Ctx : TElPKCS11StgCtx;
  {$endif}
begin
  {$ifndef SBB_NO_PKCS11}
  if (Certificate.BelongsTo = BT_PKCS11) then
  begin
    Ctx := GetContextByCertificate(Certificate);
    if Ctx = nil then
    begin
      Result := false;
      Exit;
    end;

    try
      Size := 0;
      Ctx.Decrypt(@Recipient.EncryptedKey[1], Length(Recipient.EncryptedKey),
        nil, Size);
      SetLength(Key, Size);
      if Ctx.Decrypt(@Recipient.EncryptedKey[1], Length(Recipient.EncryptedKey),
        @Key[1], Size) then
      begin
        Result := true;
        SetLength(Key, Size);
      end
      else
        Result := false;
    except
      Result := false;
    end;
  end
  else
  {$endif}
  begin
    Alg := GetPKAlgorithmByOID(Recipient.KeyEncryptionAlgorithm);
    if Alg = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
      Result := DecryptRSA(Certificate, Recipient, Key)
    else
    if Alg = SB_CERT_ALGORITHM_ID_RSAOAEP then
      Result := DecryptRSAOAEP(Certificate, Recipient, Key)
    else
      Result := false;
  end;
end;

function TElMessageProcessor.DecryptRSA(Certificate : TElX509Certificate;
  Recipient : TElPKCS7Recipient; var Key : BufferType) : boolean;
var
  RSAM, RSAE : ByteArray;
  RSAMSize, RSAESize, KeySize : integer;
  Sz : integer;
begin
  Result := false;
  if Certificate.PublicKeyAlgorithm <> SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    Exit;
  RSAMSize := 0;
  RSAESize := 0;
  SetLength(RSAM, RSAMSize);
  SetLength(RSAE, RSAESize);
  Certificate.GetRSAParams(nil, RSAMSize, nil, RSAESize);
  SetLength(RSAM, RSAMSize);
  SetLength(RSAE, RSAESize);
  Certificate.GetRSAParams(@RSAM[0], RSAMSize, @RSAE[0], RSAESize);
  Sz := 0;
  SetLength(RSAE, Sz);
  Certificate.SaveKeyValueToBuffer(nil, Sz);
  SetLength(RSAE, Sz);
  Certificate.SaveKeyValueToBuffer(@RSAE[0], Sz);
  KeySize := RSAMSize;
  SetLength(Key, KeySize);

  if not SBRSA.Decrypt(@Recipient.EncryptedKey[1], Length(Recipient.EncryptedKey),
    @RSAM[0], RSAMSize, @RSAE[0], Sz, @Key[1], KeySize) then
    Exit;
  SetLength(Key, KeySize);
  Result := true;
end;

function TElMessageProcessor.DecryptRSAOAEP(Certificate : TElX509Certificate;
  Recipient : TElPKCS7Recipient; var Key : BufferType) : boolean;
var
  RSAM, RSAE : ByteArray;
  RSAMSize, RSAESize, KeySize : integer;
  Sz : integer;
  StrLabel : pointer;
begin
  Result := false;
  if (Certificate.PublicKeyAlgorithm <> SB_CERT_ALGORITHM_ID_RSAOAEP) and
    (Certificate.PublicKeyAlgorithm <> SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) then
    Exit;
  RSAMSize := 0;
  RSAESize := 0;
  SetLength(RSAM, RSAMSize);
  SetLength(RSAE, RSAESize);
  Certificate.GetRSAParams(nil, RSAMSize, nil, RSAESize);
  SetLength(RSAM, RSAMSize);
  SetLength(RSAE, RSAESize);
  Certificate.GetRSAParams(@RSAM[0], RSAMSize, @RSAE[0], RSAESize);
  Sz := 0;
  SetLength(RSAE, Sz);
  Certificate.SaveKeyValueToBuffer(nil, Sz);
  SetLength(RSAE, Sz);
  Certificate.SaveKeyValueToBuffer(@RSAE[0], Sz);
  KeySize := RSAMSize;
  SetLength(Key, KeySize);

  if Length(Certificate.OAEPParams.StrLabel) = 0 then
    StrLabel := nil
  else
    StrLabel := @Certificate.OAEPParams.StrLabel[1];

  if not SBRSA.DecryptOAEP(@Recipient.EncryptedKey[1], Length(Recipient.EncryptedKey),
    @RSAM[0], RSAMSize, @RSAE[0], Sz, StrLabel, Length(Certificate.OAEPParams.StrLabel),
    Certificate.OAEPParams.HashAlgorithm, @Key[1], KeySize) then
    Exit;
  SetLength(Key, KeySize);
  Result := true;
end;

function TElMessageProcessor.EncryptRSA(Certificate : TElX509Certificate; Digest :
  pointer; DigestSize : integer; const OID : BufferType; var EncryptedDigest : BufferType): boolean;
var
  CTag, CTagSeq : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  TmpS : BufferType;
  RSAE, RSAM, RSAX : ByteArray;
  ESize, MSize, XSize : integer;
  Sz : integer;
  {$ifndef SBB_NO_PKCS11}
  Ctx : TElPKCS11StgCtx;
  {$endif}
  RealModulus : ^byte;
begin
  Result := false;
  CTag := TElASN1ConstrainedTag.Create;
  CTag.TagId := SB_ASN1_SEQUENCE;
  CTagSeq := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
  CTagSeq.TagId := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(CTagSeq.GetField(CTagSeq.AddField(false)));
  STag.TagId := SB_ASN1_OBJECT;
  STag.Content := OID;
  STag := TElASN1SimpleTag(CTagSeq.GetField(CTagSeq.AddField(false)));
  STag.TagId := SB_ASN1_NULL;
  STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;
  SetLength(TmpS, DigestSize);
  Move(Digest^, TmpS[1], DigestSize);
  STag.Content := TmpS;
  XSize := 0;
  CTag.SaveToBuffer(nil, XSize);
  SetLength(RSAX, XSize);
  CTag.SaveToBuffer(@RSAX[0], XSize);
  CTag.Free;
  {$ifndef SBB_NO_PKCS11}
  if Certificate.BelongsTo = BT_PKCS11 then
  begin
    Sz := 0;
    Result := false;
    Ctx := GetContextByCertificate(Certificate);
    if Ctx = nil then
      Exit;
    try
      Ctx.Sign(@RSAX[0], XSize, nil, Sz);
      SetLength(EncryptedDigest, Sz);
      if Ctx.Sign(@RSAX[0], XSize, @EncryptedDigest[1], Sz) then
      begin
        SetLength(EncryptedDigest, Sz);
        Result := true;
      end;
    except
      Exit;
    end;
  end
  else
  {$endif}
  begin
    MSize := 0;
    ESize := 0;
    Certificate.GetRSAParams(nil, MSize, nil, ESize);
    SetLength(RSAM, MSize);
    SetLength(RSAE, ESize);
    Certificate.GetRSAParams(@RSAM[0], MSize, @RSAE[0], ESize);
    RealModulus := @RSAM[0];
    while RealModulus^ = 0 do
    begin
      Inc(RealModulus);
      Dec(MSize);
    end;
    Sz := 0;
    Certificate.SaveKeyValueToBuffer(nil, Sz);
    SetLength(RSAE, Sz);
    Certificate.SaveKeyValueToBuffer(@RSAE[0], Sz);
    SetLength(EncryptedDigest, MSize);
    ESize := MSize;
    if not SBRSA.Sign(@RSAX[0], XSize, RealModulus, MSize, @RSAE[0], Sz, @EncryptedDigest[1],
      ESize) then
      Exit;
    SetLength(EncryptedDigest, ESize);
    Result := true;
  end;
end;

function TElMessageProcessor.DecryptRSAForSigner(Certificate : TElX509Certificate; Signer :
  TElPKCS7Signer; var Digest : BufferType) : boolean;
var
  RSAE, RSAM : ByteArray;
  ESize, MSize, Sz : integer;
  Buf : ByteArray;
  CTag, CNewTag : TElASN1ConstrainedTag;
begin
  Result := false;
  ESize := 0;
  MSize := 0;
  Certificate.GetRSAParams(nil, MSize, nil, ESize);
  SetLength(RSAE, ESize);
  SetLength(RSAM, MSize);
  if not Certificate.GetRSAParams(@RSAM[0], MSize, @RSAE[0], ESize) then
    Exit;
  Sz := MSize;
  SetLength(Buf, Sz);
  if not SBRSA.Decrypt(@Signer.EncryptedDigest[1], Length(Signer.EncryptedDigest),
    @RSAM[0], MSize, @RSAE[0], ESize, @Buf[0], Sz) then
    Exit;
  CTag := TElASN1ConstrainedTag.Create;
  CTag.LoadFromBuffer(@Buf[0], Sz);
  if (CTag.Count <= 0) or (not CTag.GetField(0).IsConstrained) then
  begin
    CTag.Free;
    Exit;
  end;
  CNewTag := TElASN1ConstrainedTag(CTag.GetField(0));
  if (CNewTag.TagId <> SB_ASN1_SEQUENCE) or (CNewTag.Count <> 2) then
  begin
    CTag.Free;
    Exit;
  end;
  if (CNewTag.GetField(1).IsConstrained) or (CNewTag.GetField(1).TagId <> SB_ASN1_OCTETSTRING) then
  begin
    CTag.Free;
    Exit;
  end;
  Digest := TElASN1SimpleTag(CNewTag.GetField(1)).Content;
  CTag.Free;
  Result := true;
end;

function TElMessageProcessor.VerifyRSAPSS(Certificate : TElX509Certificate; Signer : TElPKCS7Signer;
  Digest : pointer; Size: integer; HashAlgorithm : integer; SaltSize : integer) : boolean;
var
  N, E : ByteArray;
  NSize, ESize : integer;
begin
  Result := false;
  NSize := 0;
  ESize := 0;
  Certificate.GetRSAParams(nil, NSize, nil, ESize);
  SetLength(N, NSize);
  SetLength(E, ESize);

  if not Certificate.GetRSAParams(@N[0], NSize, @E[0], ESize) then
    Exit;

  Result := SBRSA.VerifyPSS(Digest, Size, HashAlgorithm, SaltSize, N, NSize, E, ESize,
    @Signer.EncryptedDigest[1], Length(Signer.EncryptedDigest));
end;


function TElMessageProcessor.VerifyDSA(Certificate : TElX509Certificate; Signer :
  TElPKCS7Signer; Digest : pointer; Size : integer) : boolean;
var
  P, Q, G, Y, R, S : ByteArray;
  PSize, QSize, GSize, YSize, RSize, SSize : integer;
begin
  Result := false;
  PSize := 0;
  Certificate.GetDSSParams(nil, PSize, nil, QSize, nil, GSize, nil, YSize);
  SetLength(P, PSize);
  SetLength(Q, QSize);
  SetLength(G, GSize);
  SetLength(Y, YSize);
  Certificate.GetDSSParams(@P[0], PSize, @Q[0], QSize, @G[0], GSize, @Y[0], YSize);
  RSize := 21;
  SSize := 21;
  SetLength(R, RSize);
  SetLength(S, SSize);
  if not SBDSA.DecodeSignature(@Signer.EncryptedDigest[1], Length(Signer.EncryptedDigest),
    @R[0], RSize, @S[0], SSize)
  then
    Exit;
  Result := SBDSA.ValidateSignature(Digest, Size, @P[0], PSize, @Q[0], QSize, @G[0], GSize,
    @Y[0], YSize, @R[0], RSize, @S[0], SSize);
end;

function TElMessageProcessor.CalculateMAC(Buffer: pointer; Size: integer;
  const Key : BufferType; var Mac : BufferType; MacAlg : integer) : boolean;
var
  M160 : TMessageDigest160;
begin
  if MacAlg = SB_ALGORITHM_MAC_HMACSHA1 then
  begin
    M160 := SBHMAC.HashMACSHA1(Buffer, Size, Key);
    SetLength(Mac, 20);
    Move(M160, Mac[1], Length(Mac));
    Result := true;                    
  end
  else
    Result := false;
end;

function TElMessageProcessor.CalculateDigest(Buffer: pointer; Size: integer;
  Alg : integer): BufferType;
var
  HashFunction : TElHashFunction;
begin
  if not TElHashFunction.IsAlgorithmSupported(Alg) then
    SetLength(Result, 0)
  else
  begin
    HashFunction := TElHashFunction.Create(Alg);
    try
      HashFunction.Update(Buffer, Size);
      Result := HashFunction.Finish;
    finally
      FreeAndNil(HashFunction);
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TElMessageDecryptor class

type
  TElPKCS7FakedIssuer = class(TElPKCS7Issuer)
  public
    constructor Create;
    destructor Destroy; override;
  end;
  TElFakedX509Certificate = class(TElX509Certificate);

constructor TElPKCS7FakedIssuer.Create;
begin
  inherited;
end;

destructor TElPKCS7FakedIssuer.Destroy;
begin
  inherited;
end;

constructor TElMessageDecryptor.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  FAlgorithm := SB_CERT_ALGORITHM_UNKNOWN;
  FBitsInKey := 0;
  FCertIDs := TList.Create;
  FUseOAEP := false;
end;


destructor TElMessageDecryptor.Destroy;
begin
  ClearCertIDs;
  FreeAndNil(FCertIDs);
  inherited;
end;


function TElMessageDecryptor.Decrypt(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer) : integer;
var
  FMessage : TElPKCS7Message;
  I, J : integer;
  Found : boolean;
  Certificate : TElX509Certificate;
  FakedCert : TElFakedX509Certificate;
  Recipient : TElPKCS7Recipient;
  Key : BufferType;
  Sz : word;
  CertIndex : integer;
  Issuer : TElPKCS7Issuer;
begin
  CheckLicenseKey();




  FMessage := TElPKCS7Message.Create;
  FUsedCertificate := -1;
  CertIndex := -1;
  Certificate := nil;
  Recipient := nil;
  Result := FMessage.LoadFromBuffer(InBuffer, InSize);
  if Result <> 0 then
  begin
    FreeAndNil(FMessage);
    Result := SB_MESSAGE_ERROR_INVALID_FORMAT;
    Exit;
  end;
  if FMessage.ContentType <> ctEnvelopedData then
  begin
    Result := SB_MESSAGE_ERROR_NO_ENCRYPTED_DATA;
    FreeAndNil(FMessage);
    Exit;
  end;

  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
    FreeAndNil(FMessage);
    Exit;
  end;
  
  ClearCertIDs;
  for I := 0 to FMessage.EnvelopedData.RecipientCount - 1 do
  begin
    Issuer := TElPKCS7FakedIssuer.Create;
    Issuer.SerialNumber := FMessage.EnvelopedData.Recipients[I].Issuer.SerialNumber;
    Issuer.Issuer.Count := FMessage.EnvelopedData.Recipients[I].Issuer.Issuer.Count;
    for J := 0 to FMessage.EnvelopedData.Recipients[I].Issuer.Issuer.Count - 1 do
    begin
      Issuer.Issuer.OIDs[J] := FMessage.EnvelopedData.Recipients[I].Issuer.Issuer.OIDs[J];
      Issuer.Issuer.Values[J] := FMessage.EnvelopedData.Recipients[I].Issuer.Issuer.Values[J];
      Issuer.Issuer.Tags[J] := FMessage.EnvelopedData.Recipients[I].Issuer.Issuer.Tags[J];
    end;
    FCertIDs.Add(Issuer);
  end;
  if (not Assigned(FCertStorage)) then
  begin
    Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
    FreeAndNil(FMessage);
    Exit;
  end;
  
  Found := false;
  for I := 0 to FMessage.EnvelopedData.RecipientCount - 1 do
  begin
    for J := 0 to FCertStorage.Count - 1 do
    begin
      if (CompareRDN(FCertStorage.Certificates[J].IssuerRDN,
        FMessage.EnvelopedData.Recipients[I].Issuer.Issuer)) and
        (CompareStr(FCertStorage.Certificates[J].SerialNumber,
        FMessage.EnvelopedData.Recipients[I].Issuer.SerialNumber) = 0) then
      begin
        Certificate := FCertStorage.Certificates[J];
        Recipient := FMessage.EnvelopedData.Recipients[I];
        CertIndex := J;
        Found := true;
        Break;
      end;
    end;
    if Found and Certificate.PrivateKeyExists then
      Break;
  end;
  if (not Found) or (not Certificate.PrivateKeyExists) then
  begin
    Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
    FreeAndNil(FMessage);
    Exit;
  end;
  
  Sz := 0;
  Certificate.SaveKeyToBuffer(nil, Sz);
  if Sz = 0 then
  begin
    FakedCert := TElFakedX509Certificate.Create(nil);
    Certificate.Clone(FakedCert, true);
    {$ifndef SBB_NO_PKCS11}
    if not (FakedCert.BelongsTo = BT_PKCS11) then
    {$endif}
    begin
      {$ifdef SB_HAS_WINCRYPT}
      {$ifndef NET_CF}
      if FakedCert.BelongsTo = BT_WINDOWS then
        Result := DecryptWin32(InBuffer, InSize, OutBuffer, OutSize, FakedCert)
      else
      {$endif}
      {$endif}
        Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
      FUsedCertificate := CertIndex;

      FreeAndNil(FMessage);
      FreeAndNil(FakedCert);
      Exit;
    end;
  end;
  if (OutSize < Length(FMessage.EnvelopedData.EncryptedContent.EncryptedContent)) then
  begin
    OutSize := Length(FMessage.EnvelopedData.EncryptedContent.EncryptedContent);
    Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
    FreeAndNil(FMessage);
    Exit;
  end;
  if not DecryptKey(Certificate, Recipient, Key) then
  begin
    Result := SB_MESSAGE_ERROR_KEY_DECRYPTION_FAILED;
    FreeAndNil(FMessage);
    Exit;
  end;
  if not DecryptContent(FMessage.EnvelopedData.EncryptedContent, Key, OutBuffer,
    OutSize) then
  begin
    Result := SB_MESSAGE_ERROR_CONTENT_DECRYPTION_FAILED;
    FreeAndNil(FMessage);
    Exit;
  end;
  
  FreeAndNil(FMessage);
  FUsedCertificate := CertIndex;
  Result := 0;
end;

function TElMessageDecryptor.Decrypt(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer; Key : pointer;
  KeySize : integer) : integer;
var
  FMessage : TElPKCS7Message;
  StrKey : string;
begin
  CheckLicenseKey();




  FMessage := TElPKCS7Message.Create;
  FUsedCertificate := -1;
  Result := FMessage.LoadFromBuffer(InBuffer, InSize);
  if Result <> 0 then
  begin
    FreeAndNil(FMessage);
    Result := SB_MESSAGE_ERROR_INVALID_FORMAT;
    Exit;
  end;
  
  if FMessage.ContentType <> ctEnvelopedData then
  begin
    Result := SB_MESSAGE_ERROR_NO_ENCRYPTED_DATA;
    FreeAndNil(FMessage);
    Exit;
  end;
  
  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
    FreeAndNil(FMessage);
    Exit;
  end;
  
  ClearCertIDs;
  if FMessage.EncryptedData.Version > 2 then
  begin
    Result := SB_MESSAGE_ERROR_INVALID_FORMAT;
    FreeAndNil(FMessage);
    Exit; 
  end;
  
  if FMessage.EncryptedData.EncryptedContent.ContentType <> SB_OID_PKCS7_DATA then
  begin
    Result := SB_MESSAGE_ERROR_NO_ENCRYPTED_DATA;
    FreeAndNil(FMessage);
    Exit;
  end;
  SetLength(StrKey, KeySize);
  Move(Key^, StrKey[1], Length(StrKey));
  if DecryptContent(FMessage.EncryptedData.EncryptedContent, StrKey, OutBuffer,
    OutSize) then
    Result := 0
  else
    Result := SB_MESSAGE_ERROR_CONTENT_DECRYPTION_FAILED;
end;

function TElMessageDecryptor.DecryptContent(Content : TElPKCS7EncryptedContent;
  const Key : string; OutBuffer : pointer; var OutSize : integer) : boolean;
var
  Alg : integer;
  RC4Ctx : TRC4Context;
  {$ifndef SBB_NO_DES}
  TripleDESCtx : T3DESContext;
  TripleDESKey : T3DESKey;
  TripleDESIV : T3DESBuffer;
  DESCtx : TDESContext;
  DESKey : TDESKey;
  DESIV : TDESBuffer;
  {$endif}
  RC2Ctx : TRC2Context;
  RC2Key : TRC2Key;
  RC2IV : TRC2Buffer;
  AES128Ctx : TAESContext128;
  AES192Ctx : TAESContext192;
  AES256Ctx : TAESContext256;
  AESKey128 : TAESKey128;
  AESKey192 : TAESKey192;
  AESKey256 : TAESKey256;
  AESIV : TAESBuffer;
  Tmp : cardinal;
  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
begin
  Alg := GetAlgorithmByOID(Content.ContentEncryptionAlgorithm);
  FAlgorithm := Alg;
  FBitsInKey := Length(Key) * 8;
  Result := false;
  if Alg = SB_ALGORITHM_CNT_RC4 then
  begin
    SBRC4.Initialize(RC4Ctx, TRC4Key(BytesOfString(Key)));
    SBRC4.Decrypt(RC4Ctx, @Content.EncryptedContent[1], OutBuffer,
      Length(Content.EncryptedContent));
    OutSize := Length(Content.EncryptedContent);
    Result := true;
  end
  else
  {$ifndef SBB_NO_DES}
  if Alg = SB_ALGORITHM_CNT_3DES then
  begin
    if (Length(Key) <> 24) or (Length(Content.ContentEncryptionAlgorithmParams) < 8) then
      Exit;
    Move(Content.ContentEncryptionAlgorithmParams[Length(Content.ContentEncryptionAlgorithmParams) - 7],
      TripleDESIV[0], 8);
    Move(Key[1], TripleDESKey[0], 24);
    SB3DES.InitializeDecryptionCBC(TripleDESCtx, TripleDESKey, TripleDESIV);
    Tmp := OutSize;
    SB3DES.DecryptCBC(TripleDESCtx, @Content.EncryptedContent[1],
      Length(Content.EncryptedContent), OutBuffer, Tmp);
    OutSize := Tmp - PByteArray(OutBuffer)[Tmp - 1];
    Result := (OutSize >= 0) and (PByteArray(OutBuffer)[Tmp - 1] <= 8);
  end
  else
  if Alg = SB_ALGORITHM_CNT_DES then
  begin
    if (Length(Key) <> 8) or (Length(Content.ContentEncryptionAlgorithmParams) < 8) then
      Exit;
    Move(Content.ContentEncryptionAlgorithmParams[Length(Content.ContentEncryptionAlgorithmParams) - 7],
      DESIV[0], 8);
    Move(Key[1], DESKey[0], 8);
    SBDES.InitializeDecryptionCBC(DESCtx, DESKey, DESIV);
    Tmp := OutSize;
    SBDES.DecryptCBC(DESCtx, @Content.EncryptedContent[1],
      Length(Content.EncryptedContent), OutBuffer, Tmp);
    OutSize := Tmp - PByteArray(OutBuffer)[Tmp - 1];
    Result := (OutSize >= 0) and (PByteArray(OutBuffer)[Tmp - 1] <= 8);
  end
  else
  {$endif}
  if Alg = SB_ALGORITHM_CNT_AES128 then
  begin
    if (Length(Key) <> 16) or (Length(Content.ContentEncryptionAlgorithmParams) < 16) then
      Exit;
    Move(Content.ContentEncryptionAlgorithmParams[Length(Content.ContentEncryptionAlgorithmParams) - 15],
      AESIV[0], 16);
    Move(Key[1], AESKey128[0], 16);
    SBAES.InitializeDecryptionCBC128(AES128Ctx, AESKey128, AESIV);
    Tmp := OutSize;
    SBAES.DecryptCBC128(AES128Ctx, @Content.EncryptedContent[1],
      Length(Content.EncryptedContent), OutBuffer, Tmp);
    OutSize := Tmp - PByteArray(OutBuffer)[Tmp - 1];
    Result := (OutSize >= 0) and (PByteArray(OutBuffer)[Tmp - 1] <= 16);
  end
  else
  if Alg = SB_ALGORITHM_CNT_AES192 then
  begin
    if (Length(Key) <> 24) or (Length(Content.ContentEncryptionAlgorithmParams) < 16) then
      Exit;
    Move(Content.ContentEncryptionAlgorithmParams[Length(Content.ContentEncryptionAlgorithmParams) - 15],
      AESIV[0], 16);
    Move(Key[1], AESKey192[0], 24);
    SBAES.InitializeDecryptionCBC192(AES192Ctx, AESKey192, AESIV);
    Tmp := OutSize;
    SBAES.DecryptCBC192(AES192Ctx, @Content.EncryptedContent[1],
      Length(Content.EncryptedContent), OutBuffer, Tmp);
    OutSize := Tmp - PByteArray(OutBuffer)[Tmp - 1];
    Result := (OutSize >= 0) and (PByteArray(OutBuffer)[Tmp - 1] <= 16);
  end
  else
  if Alg = SB_ALGORITHM_CNT_AES256 then
  begin
    if (Length(Key) <> 32) or (Length(Content.ContentEncryptionAlgorithmParams) < 16) then
      Exit;
    Move(Content.ContentEncryptionAlgorithmParams[Length(Content.ContentEncryptionAlgorithmParams) - 15],
      AESIV[0], 16);
    Move(Key[1], AESKey256[0], 32);
    SBAES.InitializeDecryptionCBC256(AES256Ctx, AESKey256, AESIV);
    Tmp := OutSize;
    SBAES.DecryptCBC256(AES256Ctx, @Content.EncryptedContent[1],
      Length(Content.EncryptedContent), OutBuffer, Tmp);
    OutSize := Tmp - PByteArray(OutBuffer)[Tmp - 1];
    Result := (OutSize >= 0) and (PByteArray(OutBuffer)[Tmp - 1] <= 16);
  end
  else
  if Alg = SB_ALGORITHM_CNT_RC2 then
  begin
    Tag := TElASN1ConstrainedTag.Create;
    if not Tag.LoadFromBuffer(@Content.ContentEncryptionAlgorithmParams[1],
      Length(Content.ContentEncryptionAlgorithmParams)) then
      Exit;
    if Tag.Count < 1 then
      Exit;
    if Tag.GetField(0).IsConstrained then
    begin
      if (Tag.GetField(0).TagId <> SB_ASN1_SEQUENCE) or (TElASN1ConstrainedTag(Tag.GetField(0)).Count <> 2) then
        Exit;
      if (TElASN1ConstrainedTag(Tag.GetField(0)).GetField(0).IsConstrained <> false) or
        (TElASN1ConstrainedTag(Tag.GetField(0)).GetField(1).IsConstrained <> false) then
        Exit;
      STag := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(0));
      if (STag.TagId <> SB_ASN1_INTEGER) then
        Exit;
      Tmp := GetRC2KeyLengthByIdentifier(STag.Content) shr 3;
      STag := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(1));
      if (STag.TagId <> SB_ASN1_OCTETSTRING) then
        Exit;
      if Length(STag.Content) <> 8 then
        Exit;
      if cardinal(Length(Key)) <> Tmp then
        Exit;
      SetLength(RC2Key, Tmp);
      Move(Key[1], RC2Key[0], Tmp);
      Move(STag.Content[1], RC2IV[0], 8);
    end
    else
    begin
      STag := TElASN1SimpleTag(Tag.GetField(0));
      if STag.TagId <> SB_ASN1_OCTETSTRING then
        Exit;
      if Length(STag.Content) <> 8 then
        Exit;
      if Length(Key) <> 4 then
        Exit;
      SetLength(RC2Key, 4);
      Move(Key[1], RC2Key[0], 4);
      Move(STag.Content[1], RC2IV[0], 8);
    end;
    SBRC2.InitializeDecryptionCBC(RC2Ctx, RC2Key, RC2IV);
    Tmp := OutSize;
    SBRC2.DecryptCBC(RC2Ctx, @Content.EncryptedContent[1],
      Length(Content.EncryptedContent), OutBuffer, Tmp);
    OutSize := Tmp - PByteArray(OutBuffer)[Tmp - 1];
    Result := (OutSize >= 0) and (PByteArray(OutBuffer)[Tmp - 1] <= 8);
  end;
end;

function TElMessageDecryptor.GetRC2KeyLengthByIdentifier(const Id : BufferType) : cardinal;
begin
  if Length(Id) > 1 then
    Result := RC2Identifiers2KeyLength[PByte(@Id[Length(Id)])^]
  else
  if Length(Id) = 1 then
    Result := RC2Identifiers2KeyLength[PByte(@Id[1])^]
  else
    Result := RC2Identifiers2KeyLength[0];
end;

procedure TElMessageDecryptor.SetCertStorage(Value : TElMemoryCertStorage);
begin
  FCertStorage := Value;
  if FCertStorage <> nil then
    FCertStorage.FreeNotification(Self);
end;

procedure TElMessageDecryptor.Notification(AComponent : TComponent; AOperation :
  TOperation);
begin
  inherited;
  if (AComponent = FCertStorage) and (AOperation = opRemove) then
    CertStorage := nil;
end;

function TElMessageDecryptor.GetCertIDs(Index : integer) : TElPKCS7Issuer;
begin
  if (Index >= 0) and (Index < FCertIDs.Count) then
    Result := TElPKCS7Issuer(FCertIDs[Index])
  else
    Result := nil;
end;

procedure TElMessageDecryptor.ClearCertIDs;
var
  I : integer;
begin
  for I := 0 to FCertIDs.Count - 1 do
    TElPKCS7Issuer(FCertIDs.Items[I]).Free;
  FCertIDs.Clear;
end;

function TElMessageDecryptor.GetCertIDCount : integer;
begin
  Result := FCertIDs.Count;
end;

{$ifdef SB_HAS_WINCRYPT}
{$ifndef NET_CF}
function TElMessageDecryptor.DecryptWin32(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer; Cert : TElX509Certificate) : integer;
begin
  Result := TElFakedX509Certificate(Cert).DecryptWin32(InBuffer, InSize, OutBuffer,
    OutSize);
end;
{$endif}
{$endif}

function TElMessageDecryptor.GetUsedCertificate : integer;
begin
  Result := FUsedCertificate;
end;


class function TElMessageDecryptor.IsConventionallyEncrypted(Buffer: pointer;
  Size: integer): boolean;
var
  FMessage : TElPKCS7Message;
  R : integer;
begin
  CheckLicenseKey();
  FMessage := TElPKCS7Message.Create;
  R := FMessage.LoadFromBuffer(Buffer, Size);
  if R = 0 then
    Result := FMessage.ContentType = ctEncryptedData
  else
    Result := false;
    
  FreeAndNil(FMessage);
end;

////////////////////////////////////////////////////////////////////////////////
// TElMessageEncryptor

constructor TElMessageEncryptor.Create(AOwner : TComponent);
var
  S : BufferType;
{$ifndef CLX_USED}
  C : cardinal;
{$else}
  C,
{$endif}
  D : double;
begin
  inherited Create (AOwner);
  FRandom := TElRandom.Create;
  SetLength(S, 12);
{$ifdef CLX_USED}
  C := Now;
  D := Now;
{$else}
  C := GetTickCount;
  D := Now;
{$endif}
  Move(PByteArray(@C)[0], S[1], 4);
  Move(PByteArray(@D)[0], S[5], 8);
  FRandom.Randomize(S);
  Algorithm := SB_ALGORITHM_CNT_3DES; // in order to preserve compatibility with Win32
  fUseUndefSize := true;
  FUseOAEP := false;
end;


destructor TElMessageEncryptor.Destroy;
begin
  FRandom.Free;
  inherited;
end;


procedure TElMessageEncryptor.SetCertStorage(Value : TElCustomCertStorage);
begin
  FCertStorage := Value;
  if FCertStorage <> nil then
    FCertStorage.FreeNotification(Self);
end;

procedure TElMessageEncryptor.Notification(AComponent : TComponent; AOperation :
  TOperation);
begin
  inherited;
  if (AComponent = FCertStorage) and (AOperation = opRemove) then
    CertStorage := nil;
end;

function TElMessageEncryptor.Encrypt(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer) : integer;
var
  FMessage : TElPKCS7Message;
  EnvData : TElPKCS7EnvelopedData;
  Recipient : TElPKCS7Recipient;
  I, J, Index, Sz, TotalSize : integer;
  Key, IV, Cnt : BufferType;
  SzW : word;
  FakedCert : TElFakedX509Certificate;
  SupportedAlgs : array of TSBArrayOfPairs;
  B : boolean;
  Alg, Bits : integer;
begin
  CheckLicenseKey();




  if (not Assigned(FCertStorage)) or (FCertStorage.Count <= 0) then
  begin
    Result := SB_MESSAGE_ERROR_NO_RECIPIENTS;
    Exit;
  end;
  { Counting estimated size of outgoing message at the same time looking for win32 certs }
  TotalSize := 0;
  SetLength(SupportedAlgs, 0);
  for I := 0 to FCertStorage.Count - 1 do
  begin
    Sz := 0;
    Index := 0;
    FCertStorage.Certificates[I].GetRSAParams(nil, Sz, nil, Index);
    Inc(TotalSize, Sz);
    for J := 0 to FCertStorage.Certificates[I].IssuerRDN.Count - 1 do
      TotalSize := TotalSize + Length(FCertStorage.Certificates[I].IssuerRDN.OIDs[J]) +
        Length(FCertStorage.Certificates[I].IssuerRDN.Values[J]) +
        Length(FCertStorage.Certificates[I].SerialNumber);
    Inc(TotalSize, FCertStorage.Certificates[I].IssuerRDN.Count * 20);
    if FCertStorage.Certificates[I].PrivateKeyExists then
    begin
      SzW := 0;
      FCertStorage.Certificates[I].SaveKeyToBuffer(nil, SzW);
      if SzW = 0 then
      begin
        FakedCert := TElFakedX509Certificate.Create(nil);
        FCertStorage.Certificates[I].Clone(FakedCert{$ifndef HAS_DEF_PARAMS}, true{$endif});
        {$ifndef CLX_USED}
        if FakedCert.BelongsTo = BT_WINDOWS then
        begin
          // obtaining the list of supported algorithms
          SetLength(SupportedAlgs, Length(SupportedAlgs) + 1);
          B := FakedCert.GetSupportedAlgorithmsFromWin32(SupportedAlgs[Length(SupportedAlgs) - 1]);
          if (not B) or (Length(SupportedAlgs[Length(SupportedAlgs) - 1]) = 0) then
            SetLength(SupportedAlgs, Length(SupportedAlgs) - 1);
        end;
        {$endif}
        FreeAndNil(FakedCert);
      end;
    end;
  end;
  if Length(SupportedAlgs) > 0 then
  begin
    {$ifndef BUILDER_USED}
    Alg := ChooseEncryptionAlgorithm(SupportedAlgs, Bits);
    {$else}
    Alg := ChooseEncryptionAlgorithm(@SupportedAlgs[0], Length(SupportedAlgs), Bits);
    {$endif}
    FBitsInKey := Bits;
    FAlgorithm := Alg;
  end;

  Inc(TotalSize, InSize);
  Inc(TotalSize, 512);
  if (TotalSize > OutSize) then
  begin
    OutSize := TotalSize;
    Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
    Exit;
  end;
  FMessage := TElPKCS7Message.Create;
  FMessage.UseUndefSize := FUseUndefSize;
  FMessage.ContentType := ctEnvelopedData;
  EnvData := FMessage.EnvelopedData;
  EnvData.Version := 0;
  case FAlgorithm of
    SB_ALGORITHM_CNT_3DES:
    begin
      SetLength(Key, 24);
      SetLength(IV, 8);
    end;
    SB_ALGORITHM_CNT_DES:
    begin
      SetLength(Key, 8);
      SetLength(IV, 8);
    end;
    SB_ALGORITHM_CNT_RC2:
    begin
      if FBitsInKey > 255 then
      begin
        Result := SB_MESSAGE_ERROR_INVALID_KEY_LENGTH;
        FreeAndNil(FMessage);
        Exit;
      end;
      
      if FBitsInKey mod 8 <> 0 then
        FBitsInKey := FBitsInKey div 8 * 8 + 1;
      SetLength(Key, FBitsInKey shr 3);
      SetLength(IV, 8);
    end;
    SB_ALGORITHM_CNT_AES128 :
    begin
      SetLength(Key, 16);
      SetLength(IV, 16);
    end;
    SB_ALGORITHM_CNT_AES192 :
    begin
      SetLength(Key, 24);
      SetLength(IV, 16);
    end;
    SB_ALGORITHM_CNT_AES256:
    begin
      SetLength(Key, 32);
      SetLength(IV, 16);
    end;
    SB_ALGORITHM_CNT_RC4:
    begin
      if FBitsInKey > 255 then
      begin
        Result := SB_MESSAGE_ERROR_INVALID_KEY_LENGTH;
        FreeAndNil(FMessage);
        Exit;
      end;
      
      if FBitsInKey mod 8 <> 0 then
        FBitsInKey := FBitsInKey div 8 * 8 + 1;
      SetLength(Key, FBitsInKey shr 3);
      SetLength(IV, 0);
    end;
    else
    begin
      Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
      FreeAndNil(FMessage);
      Exit;
    end;
  end;

  GenerateContentKey(@Key[1], Length(Key), @IV[1], Length(IV));
  for I := 0 to FCertStorage.Count - 1 do
  begin
    Index := EnvData.AddRecipient;
    Recipient := EnvData.Recipients[Index];
    if not FillRecipient(Recipient, FCertStorage.Certificates[I], Key) then
      EnvData.RemoveRecipient(Index);
  end;
  if EnvData.RecipientCount = 0 then
  begin
    FreeAndNil(FMessage);
    Result := SB_MESSAGE_ERROR_NO_RECIPIENTS;
    Exit;
  end;
  
  EnvData.ContentEncryptionAlgorithm := FAlgorithm;
  EnvData.EncryptedContent.ContentType := SB_OID_PKCS7_DATA;
  EnvData.EncryptedContent.ContentEncryptionAlgorithm := GetOIDByAlgorithm(FAlgorithm);
  if FAlgorithm = SB_ALGORITHM_CNT_RC2 then
    EnvData.EncryptedContent.ContentEncryptionAlgorithmParams := FillRC2Params(Length(Key), IV)
  else
  if FAlgorithm = SB_ALGORITHM_CNT_RC4 then
    EnvData.EncryptedContent.ContentEncryptionAlgorithmParams :=
      TBufferTypeConst(#5#0)
  else
    EnvData.EncryptedContent.ContentEncryptionAlgorithmParams :=
      TBufferTypeConst(#4 + Chr(Length(IV)) + IV);
  Sz := InSize + 2 * Length(IV);
  SetLength(Cnt, Sz);
  if not EncryptContent(InBuffer, InSize, @Cnt[1], Sz, Key, IV) then
  begin
    FreeAndNil(FMessage);
    Result := SB_MESSAGE_ERROR_ENCRYPTION_FAILED;
    Exit;
  end;
  
  SetLength(Cnt, Sz);
  EnvData.EncryptedContent.EncryptedContent := Cnt;
  Result := 0;
  if not FMessage.SaveToBuffer(OutBuffer, OutSize) then
    Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
    
  FreeAndNil(FMessage);
end;

function TElMessageEncryptor.Encrypt(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer; Key : pointer; KeySize : integer) : integer;
var
  FMessage : TElPKCS7Message;
  EncData : TElPKCS7EncryptedData;
  Sz, TotalSize : integer;
  KeyStr, IV, Cnt : BufferType;
begin
  CheckLicenseKey();





  { Counting estimated size of outgoing message at the same time looking for win32 certs }
  TotalSize := 0;
  Inc(TotalSize, InSize);
  Inc(TotalSize, 400);
  if (TotalSize > OutSize) then
  begin
    OutSize := TotalSize;
    Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
    Exit;
  end;
  FMessage := TElPKCS7Message.Create;
  FMessage.UseUndefSize := FUseUndefSize;
  FMessage.ContentType := ctEncryptedData;
  EncData := FMessage.EncryptedData;
  EncData.Version := 0;
  case FAlgorithm of
    SB_ALGORITHM_CNT_3DES:
    begin
      SetLength(KeyStr, 24);
      SetLength(IV, 8);
    end;
    SB_ALGORITHM_CNT_DES:
    begin
      SetLength(KeyStr, 8);
      SetLength(IV, 8);
    end;
    SB_ALGORITHM_CNT_RC2:
    begin
      if FBitsInKey > 255 then
      begin
        Result := SB_MESSAGE_ERROR_INVALID_KEY_LENGTH;
        FreeAndNil(FMessage);
        Exit;
      end;
      
      if FBitsInKey mod 8 <> 0 then
        FBitsInKey := FBitsInKey div 8 * 8 + 1;
      SetLength(KeyStr, FBitsInKey shr 3);
      SetLength(IV, 8);
    end;
    SB_ALGORITHM_CNT_AES128 :
    begin
      SetLength(KeyStr, 16);
      SetLength(IV, 16);
    end;
    SB_ALGORITHM_CNT_AES192 :
    begin
      SetLength(KeyStr, 24);
      SetLength(IV, 16);
    end;
    SB_ALGORITHM_CNT_AES256:
    begin
      SetLength(KeyStr, 32);
      SetLength(IV, 16);
    end;
    SB_ALGORITHM_CNT_RC4:
    begin
      if FBitsInKey > 255 then
      begin
        Result := SB_MESSAGE_ERROR_INVALID_KEY_LENGTH;
        FreeAndNil(FMessage);
        Exit;
      end;
      
      if FBitsInKey mod 8 <> 0 then
        FBitsInKey := FBitsInKey div 8 * 8 + 1;
      SetLength(KeyStr, FBitsInKey shr 3);
      SetLength(IV, 0);
    end;
    else
    begin
      Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
      FreeAndNil(FMessage);
      Exit;
    end;
  end;
  
  if KeySize <> Length(KeyStr) then
  begin
    Result := SB_MESSAGE_ERROR_INVALID_KEY_LENGTH;
    FreeAndNil(FMessage);
    Exit;
  end;

  GenerateContentKey(@KeyStr[1], Length(KeyStr), @IV[1], Length(IV));
  Move(Key^, KeyStr[1], Length(KeyStr));
  EncData.EncryptedContent.ContentType := SB_OID_PKCS7_DATA;
  EncData.EncryptedContent.ContentEncryptionAlgorithm := GetOIDByAlgorithm(FAlgorithm);
  if FAlgorithm = SB_ALGORITHM_CNT_RC2 then
    EncData.EncryptedContent.ContentEncryptionAlgorithmParams := FillRC2Params(Length(KeyStr), IV)
  else
  if FAlgorithm = SB_ALGORITHM_CNT_RC4 then
    EncData.EncryptedContent.ContentEncryptionAlgorithmParams :=
      TBufferTypeConst(#5#0)
  else
    EncData.EncryptedContent.ContentEncryptionAlgorithmParams :=
      TBufferTypeConst(#4 + Chr(Length(IV)) + IV);
  Sz := InSize + 2 * Length(IV);
  SetLength(Cnt, Sz);
  if not EncryptContent(InBuffer, InSize, @Cnt[1], Sz, KeyStr, IV) then
  begin
    FreeAndNil(FMessage);
    Result := SB_MESSAGE_ERROR_ENCRYPTION_FAILED;
    Exit;
  end;
  
  SetLength(Cnt, Sz);
  EncData.EncryptedContent.EncryptedContent := Cnt;
  Result := 0;
  if not FMessage.SaveToBuffer(OutBuffer, OutSize) then
    Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
   
  FreeAndNil(FMessage);
end;

procedure TElMessageEncryptor.GenerateContentKey(KeyBuffer : pointer; KeySize : integer;
  IVBuffer : pointer; IVSize : integer);
begin
  FRandom.Generate(KeyBuffer, KeySize);
  FRandom.Generate(IVBuffer, IVSize);
end;

function TElMessageEncryptor.EncryptContent(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer; const Key : string; const IV : string) : boolean;
var
  {$ifndef SBB_NO_DES}
  TripleDESCtx : T3DESContext;
  DESCtx : TDESContext;
  {$endif}
  RC2Ctx : TRC2Context;
  RC4Ctx : TRC4Context;
  AES128Ctx : TAESContext128;
  AES192Ctx : TAESContext192;
  AES256Ctx : TAESContext256;
  Sz : cardinal;
  Padding : ByteArray;
  LenBlocks : integer;
  LI     : Cardinal;
begin
  Result := false;
  case FAlgorithm of
    {$ifndef SBB_NO_DES}
    SB_ALGORITHM_CNT_3DES :
    begin
      if (Length(Key) <> 24) or (Length(IV) <> 8) then
        Exit;
      SB3DES.InitializeEncryptionCBC(TripleDESCtx, P3DESKey(@Key[1])^, P3DESBuffer(@IV[1])^);
      LenBlocks := InSize and (not 7); // InSize div 8 * 8
      Sz := LenBlocks;
      SB3DES.EncryptCBC(TripleDESCtx, InBuffer, LenBlocks, OutBuffer, Sz);
      SetLength(Padding, 8);
      Move(PByteArray(InBuffer)[LenBlocks], Padding[0], InSize - LenBlocks);
      FillChar(Padding[InSize - LenBlocks], 8 - (InSize - LenBlocks), 8 - (InSize - LenBlocks));

      Sz := 8;
      SB3DES.EncryptCBC(TripleDESCtx, @Padding[0], 8, @PByteArray(OutBuffer)[LenBlocks], Sz);
      OutSize := LenBlocks + 8;
    end;
    SB_ALGORITHM_CNT_DES :
    begin
      if (Length(Key) <> 8) or (Length(IV) <> 8) then
        Exit;
      SBDES.InitializeEncryptionCBC(DESCtx, PDESKey(@Key[1])^, PDESBuffer(@IV[1])^);
      LenBlocks := InSize and (not 7); // div 8 * 8
      Sz := LenBlocks;
      SBDES.EncryptCBC(DESCtx, InBuffer, LenBlocks, OutBuffer, Sz);
      SetLength(Padding, 8);
      FillChar(Padding[InSize - LenBlocks], 8 - (InSize - LenBlocks), 8 - (InSize - LenBlocks));
      Move(PByteArray(InBuffer)[LenBlocks], Padding[0], InSize - LenBlocks);
      Sz := 8;
      SBDES.EncryptCBC(DESCtx, @Padding[0], 8, @PByteArray(OutBuffer)[LenBlocks], Sz);
      OutSize := LenBlocks + 8;
    end;
    {$endif}
    SB_ALGORITHM_CNT_RC2 :
    begin
      if (Length(IV) <> 8) then
        Exit;
      SBRC2.InitializeEncryptionCBC(RC2Ctx, TRC2Key(BytesOfString(Key)), PRC2Buffer(@IV[1])^);
      LenBlocks := InSize and (not 7);
      Sz := LenBlocks;
      SBRC2.EncryptCBC(RC2Ctx, InBuffer, LenBlocks, OutBuffer, Sz);
      SetLength(Padding, 8);
      Move(PByteArray(InBuffer)[LenBlocks], Padding[0], InSize - LenBlocks);
      FillChar(Padding[InSize - LenBlocks], 8 - (InSize - LenBlocks), 8 - (InSize - LenBlocks));
      Sz := 8;
      SBRC2.EncryptCBC(RC2Ctx, @Padding[0], 8, @PByteArray(OutBuffer)[LenBlocks], Sz);
      OutSize := LenBlocks + 8;
    end;
    SB_ALGORITHM_CNT_RC4 :
    begin
      SBRC4.Initialize(RC4Ctx, TRC4Key(BytesOfString(Key)));
      LI := InSize;
      SBRC4.Encrypt(RC4Ctx, InBuffer, OutBuffer, LI);
      OutSize := LI;
    end;
    SB_ALGORITHM_CNT_AES128 :
    begin
      SBAES.InitializeEncryptionCBC128(AES128Ctx, PAESKey128(@Key[1])^, PAESBuffer(@IV[1])^);
      LenBlocks := InSize and (not 15); // shr 4 shl 4
      Sz := LenBlocks;
      SBAES.EncryptCBC128(AES128Ctx, InBuffer, LenBlocks, OutBuffer, Sz);
      SetLength(Padding, 16);
      Move(PByteArray(InBuffer)[LenBlocks], Padding[0], InSize - LenBlocks);
      FillChar(Padding[InSize - LenBlocks], 16 - (InSize - LenBlocks), 16 - (InSize - LenBlocks));
      Sz := 16;
      SBAES.EncryptCBC128(AES128Ctx, @Padding[0], 16, @PByteArray(OutBuffer)[LenBlocks], Sz);
      OutSize := LenBlocks + 16;
    end;
    SB_ALGORITHM_CNT_AES192 :
    begin
      SBAES.InitializeEncryptionCBC192(AES192Ctx, PAESKey192(@Key[1])^, PAESBuffer(@IV[1])^);
      LenBlocks := InSize and (not 15);
      Sz := LenBlocks;
      SBAES.EncryptCBC192(AES192Ctx, InBuffer, LenBlocks, OutBuffer, Sz);
      SetLength(Padding, 16);
      Move(PByteArray(InBuffer)[LenBlocks], Padding[0], InSize - LenBlocks);
      FillChar(Padding[InSize - LenBlocks], 16 - (InSize - LenBlocks), 16 - (InSize - LenBlocks));
      Sz := 16;

      SBAES.EncryptCBC192(AES192Ctx, @Padding[0], 16, @PByteArray(OutBuffer)[LenBlocks], Sz);
      OutSize := LenBlocks + 16;
    end;
    SB_ALGORITHM_CNT_AES256 :
    begin
      SBAES.InitializeEncryptionCBC256(AES256Ctx, PAESKey256(@Key[1])^, PAESBuffer(@IV[1])^);
      LenBlocks := InSize div 16 * 16;
      Sz := LenBlocks;

      SBAES.EncryptCBC256(AES256Ctx, InBuffer, LenBlocks, OutBuffer, Sz);
      SetLength(Padding, 16);
      Move(PByteArray(InBuffer)[LenBlocks], Padding[0], InSize - LenBlocks);
      FillChar(Padding[InSize - LenBlocks], 16 - (InSize - LenBlocks), 16 - (InSize - LenBlocks));
      Sz := 16;
      SBAES.EncryptCBC256(AES256Ctx, @Padding[0], 16, @PByteArray(OutBuffer)[LenBlocks], Sz);
      OutSize := LenBlocks + 16;
    end;
  end;
  Result := true;
end;

function TElMessageEncryptor.FillRC2Params(KeyLen : integer; const IV : BufferType) : BufferType;
begin
  if KeyLen = 4 then
    Result := TBufferTypeConst(#4#8 + IV)
  else
  begin
    Result := TBufferTypeConst(#2#2#0 + Chr(RC2KeyLength2Identifiers[KeyLen shl 3]));
    Result := TBufferTypeConst(Result + #4#8 + IV);
    Result := TBufferTypeConst(#$30 + Chr(Length(Result)) + Result);
  end;
end;


{$ifndef BUILDER_USED}
function TElMessageEncryptor.ChooseEncryptionAlgorithm(Algs : array of TSBArrayOfPairs;
  var Bits : integer) : integer;
{$else}
function TElMessageEncryptor.ChooseEncryptionAlgorithm(Algs : pointer; Count : integer;
  var Bits : integer) : integer;
{$endif}
var
  I, J : integer;
  BDES, B3DES, BRC2, BRC4, LDES, L3DES, LRC2, LRC4 : boolean;
  {$ifdef BUILDER_USED}
  CurrAlg : PSBArrayOfPairs;
  {$endif}
begin
  BDES := true;
  B3DES := true;
  BRC2 := true;
  BRC4 := true;
  Result := SB_ALGORITHM_CNT_3DES;
  Bits := 40;
  {$ifdef BUILDER_USED}
  CurrAlg := Algs;
  {$endif}
  for J := 0 to {$ifndef BUILDER_USED}Length(Algs){$else}Count{$endif} - 1 do
  begin
    LDES := false;
    L3DES := false;
    LRC2 := false;
    LRC4 := false;
    {$ifndef BUILDER_USED}
    for I := 0 to Length(Algs[J]) - 1 do
    {$else}
    for I := 0 to Length(CurrAlg^) - 1 do
    {$endif}
    begin
      {$ifndef BUILDER_USED}
      if ((Algs[J])[I].A = LongWord(CALG_DES)) then
        LDES := true
      else if ((Algs[J])[I].A = LongWord(CALG_3DES)) then
        L3DES := true
      else if ((Algs[J])[I].A = LongWord(CALG_RC2)) then
        LRC2 := true
      else if ((Algs[J])[I].A = LongWord(CALG_RC4)) then
        LRC4 := true;
      {$else}
      if (CurrAlg^[I].A = LongWord(CALG_DES)) then
        LDES := true
      else if (CurrAlg^[I].A = LongWord(CALG_3DES)) then
        L3DES := true
      else if (CurrAlg^[I].A = LongWord(CALG_RC2)) then
        LRC2 := true
      else if (CurrAlg^[I].A = LongWord(CALG_RC4)) then
        LRC4 := true;
      {$endif}
    end;
    BDES := BDES and LDES;
    B3DES := B3DES and L3DES;
    BRC2 := BRC2 and LRC2;
    BRC4 := BRC4 and LRC4;
    {$ifdef BUILDER_USED}
    Inc(CurrAlg);
    {$endif}
  end;
  if B3DES then
    Result := SB_ALGORITHM_CNT_3DES
  else if BRC2 then
    Result := SB_ALGORITHM_CNT_RC2
  else if BRC4 then
    Result := SB_ALGORITHM_CNT_RC4
  else if BDES then
    Result := SB_ALGORITHM_CNT_DES;
end;

////////////////////////////////////////////////////////////////////////////////
// TElMessageVerifier

constructor TElMessageVerifier.Create(AOwner : TComponent);
begin
  inherited Create (AOwner);
  FCertificates := TElMemoryCertStorage.Create(nil);
  FAttributes := TElPKCS7Attributes.Create;
  FCertIDs := TList.Create;
  FCSCertIDs := TList.Create;
  FSignatureType := mstPublicKey;
  FUsePSS := false;
  FVerifyCountersignatures := true;
  FInputIsDigest := false;
  FVerificationOptions := [voUseEmbeddedCerts, voUseLocalCerts, voVerifyMessageDigests,
    voVerifyTimestamps];
  FTimestamps := TList.Create;
end;


destructor TElMessageVerifier.Destroy;
begin
  FreeAndNil(FCertificates);
  FreeAndNil(FAttributes);
  ClearCertIDs;
  FreeAndNil(FCertIDs);
  FreeAndNil(FCSCertIDs);
  {$ifndef B_6}
  ClearTimestamps;
  FreeAndNil(FTimestamps);
  {$endif}
  inherited;
end;


procedure TElMessageVerifier.SetCertStorage(Value : TElCustomCertStorage);
begin
  FCertStorage := Value;
  if FCertStorage <> nil then
    FCertStorage.FreeNotification(Self);
end;

procedure TElMessageVerifier.Notification(AComponent : TComponent;
  AOperation : TOperation);
begin
  inherited;
  if (AComponent = FCertStorage) and (AOperation = opRemove) then
    CertStorage := nil;
end;

function TElMessageVerifier.Verify(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer) : integer;
var
  FMessage : TElPKCS7Message;
  Tmp : BufferType;
  I, J : integer;
  Issuer : TElPKCS7Issuer;
  MacKey : BufferType;
  Mac : BufferType;
  Digest : BufferType;
begin
  result := -1;
  CheckLicenseKey();




  ClearCertIDs;
  FAlgorithm := SB_ALGORITHM_UNKNOWN;
  FMacAlgorithm := SB_ALGORITHM_UNKNOWN;
  FAttributes.Count := 0;
  while FCertificates.Count > 0 do
    FCertificates.Remove(0);
  {$ifndef B_6}
  ClearTimestamps;
  {$endif}
  FMessage := TElPKCS7Message.Create;
  I := FMessage.LoadFromBuffer(InBuffer, InSize);
  if I <> 0 then
  begin
    Result := SB_MESSAGE_ERROR_INVALID_FORMAT;
    FreeAndNil(FMessage);
    Exit;
  end;
  if FMessage.ContentType = ctSignedData then
  begin
    FSignatureType := mstPublicKey;
    for I := 0 to FMessage.SignedData.SignerCount - 1 do
    begin
      Issuer := TElPKCS7FakedIssuer.Create;
      Issuer.SerialNumber := FMessage.SignedData.Signers[I].Issuer.SerialNumber;
      Issuer.Issuer.Count := FMessage.SignedData.Signers[I].Issuer.Issuer.Count;
      for J := 0 to Issuer.Issuer.Count - 1 do
      begin
        Issuer.Issuer.OIDs[J] := FMessage.SignedData.Signers[I].Issuer.Issuer.OIDs[J];
        Issuer.Issuer.Values[J] := FMessage.SignedData.Signers[I].Issuer.Issuer.Values[J];
        Issuer.Issuer.Tags[J] := FMessage.SignedData.Signers[I].Issuer.Issuer.Tags[J];
      end;
      FCertIDs.Add(Issuer);
    end;
    if Length(FMessage.SignedData.Content) > OutSize then
    begin
      OutSize := Length(FMessage.SignedData.Content);
      FreeAndNil(FMessage);
      Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
      Exit;
    end
    else
    begin
      OutSize := Length(FMessage.SignedData.Content);
      Tmp := FMessage.SignedData.Content;
      Move(Tmp[1], OutBuffer^, OutSize);
    end;
    if FMessage.SignedData.SignerCount = 0 then
    begin
      Result := SB_MESSAGE_ERROR_INVALID_FORMAT;
      FreeAndNil(FMessage);
      Exit;
    end;
    for I := 0 to FMessage.SignedData.Certificates.Count - 1 do
      FCertificates.Add(FMessage.SignedData.Certificates.Certificates[I]{$ifndef HAS_DEF_PARAMS}, true{$endif});
    for I := 0 to FMessage.SignedData.SignerCount - 1 do
    begin
      FMessage.SignedData.Signers[I].AuthenticatedAttributes.Copy(FAttributes);
      FMessage.SignedData.Signers[I].UnauthenticatedAttributes.Copy(FAttributes);
      Result := VerifySingle(FMessage.SignedData.Signers[I],
        @FMessage.SignedData.Content[1], Length(FMessage.SignedData.Content));
      if Result <> 0 then
      begin
        FreeAndNil(FMessage);
        Exit;
      end;
    end;
    if OutSize > 0 then
      Result := VerifyMessageDigests(FMessage, OutBuffer, OutSize);
  end
  else
  if FMessage.ContentType = ctAuthenticatedData then
  begin
    FSignatureType := mstMAC;
    if (FCertStorage = nil) or (FCertStorage.Count = 0) then
    begin
      FreeAndNil(FMessage);
      Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
      Exit;
    end;
    for I := 0 to FMessage.AuthenticatedData.RecipientCount - 1 do
    begin
      Issuer := TElPKCS7FakedIssuer.Create;
      Issuer.SerialNumber := FMessage.AuthenticatedData.Recipients[I].Issuer.SerialNumber;
      Issuer.Issuer.Count := FMessage.AuthenticatedData.Recipients[I].Issuer.Issuer.Count;
      for J := 0 to Issuer.Issuer.Count - 1 do
      begin
        Issuer.Issuer.OIDs[J] := FMessage.AuthenticatedData.Recipients[I].Issuer.Issuer.OIDs[J];
        Issuer.Issuer.Values[J] := FMessage.AuthenticatedData.Recipients[I].Issuer.Issuer.Values[J];
        Issuer.Issuer.Tags[J] := FMessage.AuthenticatedData.Recipients[I].Issuer.Issuer.Tags[J];
      end;
      FCertIDs.Add(Issuer);
    end;
    if Length(FMessage.AuthenticatedData.Content) > OutSize then
    begin
      OutSize := Length(FMessage.AuthenticatedData.Content);
      FreeAndNil(FMessage);
      Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
      Exit;
    end
    else
    begin
      OutSize := Length(FMessage.AuthenticatedData.Content);
      Tmp := FMessage.AuthenticatedData.Content;
      Move(Tmp[1], OutBuffer^, OutSize);
    end;
    if FMessage.AuthenticatedData.RecipientCount = 0 then
    begin
      Result := SB_MESSAGE_ERROR_INVALID_FORMAT;
      FreeAndNil(FMessage);
      Exit;
    end;

    Result := ExtractMacKey(FMessage.AuthenticatedData, MacKey);
    if Result <> 0 then
    begin
      FreeAndNil(FMessage);
      Exit;
    end;

    FMessage.AuthenticatedData.AuthenticatedAttributes.Copy(FAttributes);
    FMessage.AuthenticatedData.UnauthenticatedAttributes.Copy(FAttributes);

    FMacAlgorithm := GetAlgorithmByOID(FMessage.AuthenticatedData.MacAlgorithm);

    if (FMessage.AuthenticatedData.AuthenticatedAttributes.Count > 0) then
    begin
      // calculating MAC over authenticated data
      FMessage.AuthenticatedData.RecalculateAuthenticatedAttributes;
      Tmp := FMessage.AuthenticatedData.AuthenticatedAttributesPlain;
      if not CalculateMAC(@Tmp[1], Length(Tmp), MacKey, Mac, FMacAlgorithm) then
      begin
        Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
        FreeAndNil(FMessage);
        Exit;
      end;
      if not CompareContent(Mac, FMessage.AuthenticatedData.Mac) then
        Result := SB_MESSAGE_ERROR_INVALID_MAC
      else
      begin
        // and hash over content data
        FAlgorithm := GetHashAlgorithmByOID(FMessage.AuthenticatedData.DigestAlgorithm);
        Digest := CalculateDigest(OutBuffer, OutSize, FAlgorithm);
        if Length(Digest) > 0 then
        begin
          // searching for 'message-digest' authenticated attribute
          Result := SB_MESSAGE_ERROR_INVALID_DIGEST;
          for I := 0 to FMessage.AuthenticatedData.AuthenticatedAttributes.Count - 1 do
          begin
            if CompareContent(FMessage.AuthenticatedData.AuthenticatedAttributes.Attributes[I],
              SB_OID_MESSAGE_DIGEST) then
            begin
              if (FMessage.AuthenticatedData.AuthenticatedAttributes.Values[I].Count > 0) then
              begin
                Tmp := UnformatAttributeValue(FMessage.AuthenticatedData.AuthenticatedAttributes.Values[I][0], J);
                if CompareContent(Tmp, Digest) then
                  Result := 0
                else
                  Result := SB_MESSAGE_ERROR_INVALID_DIGEST;
              end;
              Break;
            end;
          end
        end
        else
          Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
      end;
    end
    else
    begin
      // calculating MAC over content data
      if not CalculateMAC(OutBuffer, OutSize, MacKey, Mac, FMacAlgorithm) then
      begin
        Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
        FreeAndNil(FMessage);
        Exit;
      end;
      if CompareContent(Mac, FMessage.AuthenticatedData.Mac) then
        Result := 0
      else
        Result := SB_MESSAGE_ERROR_INVALID_MAC;
    end;
  end
  else
    Result := SB_MESSAGE_ERROR_NO_SIGNED_DATA;

  FreeAndNil(FMessage);
end;

function TElMessageVerifier.VerifyDetached(Buffer : pointer; Size : integer;
  Signature : pointer; SignatureSize : integer) : integer;
var
  FMessage : TElPKCS7Message;
  I, J : integer;
  Issuer : TElPKCS7Issuer;
  MacKey : BufferType;
  Mac : BufferType;
  Digest : BufferType;
  Tmp : BufferType;
begin
  CheckLicenseKey();



  
  ClearCertIDs;
  FAlgorithm := SB_ALGORITHM_UNKNOWN;
  FMacAlgorithm := SB_ALGORITHM_UNKNOWN;
  FAttributes.Count := 0;
  while FCertificates.Count > 0 do
    FCertificates.Remove(0);
  {$ifndef B_6}
  ClearTimestamps;
  {$endif}
  FMessage := TElPKCS7Message.Create;
  I := FMessage.LoadFromBuffer(Signature, SignatureSize);
  if I <> 0 then
  begin
    Result := SB_MESSAGE_ERROR_INVALID_FORMAT;
    FMessage.Free;
    Exit;
  end;
  if FMessage.ContentType = ctSignedData then
  begin
    FSignatureType := mstPublicKey;
    for I := 0 to FMessage.SignedData.SignerCount - 1 do
    begin
      Issuer := TElPKCS7FakedIssuer.Create;
      Issuer.SerialNumber := FMessage.SignedData.Signers[I].Issuer.SerialNumber;
      Issuer.Issuer.Count := FMessage.SignedData.Signers[I].Issuer.Issuer.Count;
      for J := 0 to Issuer.Issuer.Count - 1 do
      begin
        Issuer.Issuer.OIDs[J] := FMessage.SignedData.Signers[I].Issuer.Issuer.OIDs[J];
        Issuer.Issuer.Values[J] := FMessage.SignedData.Signers[I].Issuer.Issuer.Values[J];
        Issuer.Issuer.Tags[J] := FMessage.SignedData.Signers[I].Issuer.Issuer.Tags[J];
      end;
      FCertIDs.Add(Issuer);
    end;
    if FMessage.SignedData.SignerCount = 0 then
    begin
      Result := SB_MESSAGE_ERROR_INVALID_FORMAT;
      FMessage.Free;
      Exit;
    end;
    for I := 0 to FMessage.SignedData.Certificates.Count - 1 do
      FCertificates.Add(FMessage.SignedData.Certificates.Certificates[I]{$ifndef HAS_DEF_PARAMS}, true{$endif});
    for I := 0 to FMessage.SignedData.SignerCount - 1 do
    begin
      FMessage.SignedData.Signers[I].AuthenticatedAttributes.Copy(FAttributes);
      FMessage.SignedData.Signers[I].UnauthenticatedAttributes.Copy(FAttributes);
      Result := VerifySingle(FMessage.SignedData.Signers[I], Buffer, Size {$ifndef HAS_DEF_PARAMS}, False{$endif});
      if Result <> 0 then
      begin
        FMessage.Free;
        Exit;
      end;
    end;
    Result := VerifyMessageDigests(FMessage, Buffer, Size);
  end
  else if FMessage.ContentType = ctAuthenticatedData then
  begin
    FSignatureType := mstMAC;
    if (FCertStorage = nil) or (FCertStorage.Count = 0) then
    begin
      FMessage.Free;
      Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
      Exit;
    end;
    for I := 0 to FMessage.AuthenticatedData.RecipientCount - 1 do
    begin
      Issuer := TElPKCS7FakedIssuer.Create;
      Issuer.SerialNumber := FMessage.AuthenticatedData.Recipients[I].Issuer.SerialNumber;
      Issuer.Issuer.Count := FMessage.AuthenticatedData.Recipients[I].Issuer.Issuer.Count;
      for J := 0 to Issuer.Issuer.Count - 1 do
      begin
        Issuer.Issuer.OIDs[J] := FMessage.AuthenticatedData.Recipients[I].Issuer.Issuer.OIDs[J];
        Issuer.Issuer.Values[J] := FMessage.AuthenticatedData.Recipients[I].Issuer.Issuer.Values[J];
        Issuer.Issuer.Tags[J] := FMessage.AuthenticatedData.Recipients[I].Issuer.Issuer.Tags[J];
      end;
      FCertIDs.Add(Issuer);
    end;
    if FMessage.AuthenticatedData.RecipientCount = 0 then
    begin
      Result := SB_MESSAGE_ERROR_INVALID_FORMAT;
      FMessage.Free;
      Exit;
    end;

    Result := ExtractMacKey(FMessage.AuthenticatedData, MacKey);
    if Result <> 0 then
    begin
      FMessage.Free;
      Exit;
    end;

    FMessage.AuthenticatedData.AuthenticatedAttributes.Copy(FAttributes);
    FMessage.AuthenticatedData.UnauthenticatedAttributes.Copy(FAttributes);

    FMacAlgorithm := GetAlgorithmByOID(FMessage.AuthenticatedData.MacAlgorithm);

    if (FMessage.AuthenticatedData.AuthenticatedAttributes.Count > 0) then
    begin
      // calculating MAC over authenticated data
      FMessage.AuthenticatedData.RecalculateAuthenticatedAttributes;
      Tmp := FMessage.AuthenticatedData.AuthenticatedAttributesPlain;
      if not CalculateMAC(@Tmp[1], Length(Tmp), MacKey, Mac, FMacAlgorithm) then
      begin
        Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
        FMessage.Free;
        Exit;
      end;
      
      if not CompareContent(Mac, FMessage.AuthenticatedData.Mac) then
        Result := SB_MESSAGE_ERROR_INVALID_MAC
      else
      begin
        // and hash over content data
        FAlgorithm := GetHashAlgorithmByOID(FMessage.AuthenticatedData.DigestAlgorithm);
        Digest := CalculateDigest(Buffer, Size, FAlgorithm);
        if Length(Digest) > 0 then
        begin
          // searching for 'message-digest' authenticated attribute
          Result := SB_MESSAGE_ERROR_INVALID_DIGEST;
          for I := 0 to FMessage.AuthenticatedData.AuthenticatedAttributes.Count - 1 do
          begin
            if CompareContent(FMessage.AuthenticatedData.AuthenticatedAttributes.Attributes[I],
              SB_OID_MESSAGE_DIGEST) then
            begin
              if (FMessage.AuthenticatedData.AuthenticatedAttributes.Values[I].Count > 0) then
              begin
                Tmp := UnformatAttributeValue(FMessage.AuthenticatedData.AuthenticatedAttributes.Values[I][0], J);
                if CompareContent(Tmp, Digest) then
                  Result := 0;
              end;
              Break;
            end;
          end
        end
        else
          Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
      end;
    end
    else
    begin
      // calculating MAC over content data
      if not CalculateMAC(Buffer, Size, MacKey, Mac, FMacAlgorithm) then
      begin
        Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
        FMessage.Free;
        Exit;
      end;
      
      if CompareContent(Mac, FMessage.AuthenticatedData.Mac) then
        Result := 0
      else
        Result := SB_MESSAGE_ERROR_INVALID_MAC;
    end;
  end
  else
    Result := SB_MESSAGE_ERROR_NO_SIGNED_DATA;

  FMessage.Free;
end;

function TElMessageVerifier.VerifySingle(Signer : TElPKCS7Signer; Digest : pointer;
  DigestSize : integer; Countersign : boolean = false) : integer;
var
  EncAlg, DigestAlg : integer;
  Cert : TElX509Certificate;
  I, J, K : integer;
  Found : boolean;

  IncomingDigest : BufferType;
  ActualDigest : BufferType;

  M160 : TMessageDigest160;
  PData : pointer;
  DataSize : integer;
  SaltSize, MGF, MGFHash, Trailer : integer;
  Countersignature : BufferType;
  Info : TElPKCS7Signer;
  Tag : TElASN1ConstrainedTag;
  Dgst : BufferType;
  TagID : integer;
  SignerInfos : TList;
  Issuer : TElPKCS7FakedIssuer;
begin
  { Obtaining signer's certificate }
  Found := false;
  Cert := nil;
  if voUseEmbeddedCerts in FVerificationOptions then
  begin
    for I := 0 to FCertificates.Count - 1 do
    begin
      if (CompareRDN(FCertificates.Certificates[I].IssuerRDN, Signer.Issuer.Issuer))
        and (CompareStr(FCertificates.Certificates[I].SerialNumber,
        Signer.Issuer.SerialNumber) = 0) then
      begin
        Cert := FCertificates.Certificates[I];
        Found := true;
        Break;
      end;
    end;
  end;
  if not Found then
  begin
    if voUseLocalCerts in FVerificationOptions then
    begin
      if not Assigned(FCertStorage) then
      begin
        Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
        Exit;
      end;
      for I := 0 to FCertStorage.Count - 1 do
      begin
        if (CompareRDN(FCertStorage.Certificates[I].IssuerRDN, Signer.Issuer.Issuer))
          and (CompareStr(FCertStorage.Certificates[I].SerialNumber,
          Signer.Issuer.SerialNumber) = 0) then
        begin
          Cert := FCertStorage.Certificates[I];
          Found := true;
          Break;
        end;
      end;
    end;
    if not Found then
    begin
      Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
      Exit;
    end;
  end;
  { Decrypting EncryptedDigest }
  EncAlg := GetSigAlgorithmByOID(Signer.DigestEncryptionAlgorithm);
  FAlgorithm := EncAlg;
  if (EncAlg in [SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION, SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION, SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION, SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION]) and (Cert.PublicKeyAlgorithm =
    SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) then
  begin
    if not DecryptRSAForSigner(Cert, Signer, IncomingDigest) then
    begin
      Result := SB_MESSAGE_ERROR_INVALID_SIGNATURE;
      Exit;
    end;
  end
  else if (EncAlg = SB_CERT_ALGORITHM_ID_RSAPSS) and ((Cert.PublicKeyAlgorithm =
    SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or (Cert.PublicKeyAlgorithm =
    SB_CERT_ALGORITHM_ID_RSAPSS)) then
  begin
  end
  else if (EncAlg = SB_CERT_ALGORITHM_ID_DSA) and (Cert.PublicKeyAlgorithm =
    SB_CERT_ALGORITHM_ID_DSA) then
  begin
  end
  else
  begin
    Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
    Exit;
  end;
  DigestAlg := GetHashAlgorithmByOID(Signer.DigestAlgorithm);
  if Signer.AuthenticatedAttributes.Count > 0 then
  begin
    PData := @Signer.AuthenticatedAttributesPlain[1];
    DataSize := Length(Signer.AuthenticatedAttributesPlain);
  end
  else
  begin
    PData := Digest;
    DataSize := DigestSize;
  end;

  FAlgorithm := DigestAlg;
  if EncAlg = SB_CERT_ALGORITHM_ID_DSA then
  begin
    M160 := HashSHA1(PData, DataSize);
    if VerifyDSA(Cert, Signer, @M160, 20) then
      Result := 0
    else
      Result := SB_MESSAGE_ERROR_INVALID_SIGNATURE;
  end
  else if EncAlg in [SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION, SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION, SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION, SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION] then
  begin
    if (not FInputIsDigest) or (Signer.AuthenticatedAttributes.Count > 0) then
      ActualDigest := CalculateDigest(PData, DataSize, DigestAlg)
    else
    begin
      SetLength(ActualDigest, DataSize);
      Move(PData^, ActualDigest[1], Length(ActualDigest));
    end;
    if Length(ActualDigest) = 0 then
    begin
      FAlgorithm := SB_ALGORITHM_UNKNOWN;
      Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
      Exit;
    end;
    if (CompareContent(ActualDigest, IncomingDigest)) then
      Result := 0
    else
      Result := SB_MESSAGE_ERROR_INVALID_DIGEST;
  end
  else if EncAlg = SB_CERT_ALGORITHM_ID_RSAPSS then
  begin
    FUsePSS := true;
    if not ReadPSSParams(@Signer.DigestEncryptionAlgorithmParams[1],
      Length(Signer.DigestEncryptionAlgorithmParams), FAlgorithm, SaltSize, MGF,
      MGFHash, Trailer) then
    begin
      FAlgorithm := SB_ALGORITHM_UNKNOWN;
      Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
      Exit;
    end;         
    ActualDigest := CalculateDigest(PData, DataSize, DigestAlg);
    if Length(ActualDigest) = 0 then
    begin
      FAlgorithm := SB_ALGORITHM_UNKNOWN;
      Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
      Exit;
    end;

    if VerifyRSAPSS(Cert, Signer, @ActualDigest[1], Length(ActualDigest),
      FAlgorithm, SaltSize)
    then
      Result := 0
    else
      Result := SB_MESSAGE_ERROR_INVALID_DIGEST
  end
  else
    Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;

  // verifying countersignatures, if any
  if not FVerifyCountersignatures then
    Exit;
  if Result = 0 then
  begin
    // extracting signerinfos
    SignerInfos := TList.Create;
    try
      for I := 0 to Signer.UnauthenticatedAttributes.Count - 1 do
      begin
        if CompareContent(Signer.UnauthenticatedAttributes.Attributes[I],
          SB_OID_COUNTER_SIGNATURE) then
        begin
          for J := 0 to Signer.UnauthenticatedAttributes.Values[I].Count - 1 do
          begin
            Countersignature := Signer.UnauthenticatedAttributes.Values[I][J];
            Tag := TElASN1ConstrainedTag.Create;
            try
              if not Tag.LoadFromBuffer(@Countersignature[1], Length(Countersignature)) then
              begin
                Result := SB_MESSAGE_ERROR_INVALID_COUNTERSIGNATURE;
                Break;
              end;
              if (Tag.Count <> 1) or (not Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
              begin
                Result := SB_MESSAGE_ERROR_INVALID_COUNTERSIGNATURE;
                Break;
              end;
              Info := TElPKCS7Signer.Create;
              if ProcessSignerInfo(Tag.GetField(0), Info) <> 0 then
              begin
                Result := SB_MESSAGE_ERROR_INVALID_COUNTERSIGNATURE;
                FreeAndNil(Info);
                Exit;
              end;
            finally
              FreeAndNil(Tag);
            end;
            SignerInfos.Add(Info);
            Issuer := TElPKCS7FakedIssuer.Create;
            Issuer.SerialNumber := Info.Issuer.SerialNumber;
            Issuer.Issuer.Count := Info.Issuer.Issuer.Count;
            for K := 0 to Issuer.Issuer.Count - 1 do
            begin
              Issuer.Issuer.OIDs[J] := Info.Issuer.Issuer.OIDs[J];
              Issuer.Issuer.Values[J] := Info.Issuer.Issuer.Values[J];
              Issuer.Issuer.Tags[J] := Info.Issuer.Issuer.Tags[J];
            end;
            FCSCertIDs.Add(Issuer);
          end;
        end;
      end;

      // verifying signerinfos
      for I := 0 to SignerInfos.Count - 1 do
      begin
        Info := TElPKCS7Signer(SignerInfos[I]);
        Dgst := Signer.EncryptedDigest;
        Result := VerifySingle(Info, @Dgst[1], Length(Dgst), true);
        if Result <> 0 then
          Exit;
        // verifying message-digest
        if Info.AuthenticatedAttributes.Count > 0 then
        begin
          Result := SB_MESSAGE_ERROR_INVALID_DIGEST;
          for K := 0 to Info.AuthenticatedAttributes.Count - 1 do
          begin
            if CompareContent(Info.AuthenticatedAttributes.Attributes[K],
              SB_OID_MESSAGE_DIGEST) then
            begin
              if Info.AuthenticatedAttributes.Values[K].Count > 0 then
                IncomingDigest := UnformatAttributeValue(Info.AuthenticatedAttributes.Values[K][0],
                  TagID)
              else
                IncomingDigest := EmptyBuffer;
              DigestAlg := GetHashAlgorithmByOID(Info.DigestAlgorithm);
              Dgst := CalculateDigest(@Dgst[1], Length(Dgst), DigestAlg);
              if CompareContent(IncomingDigest, Dgst) then
                Result := 0;
              Break;
            end;
          end;
        end;
        if Result <> 0 then
          Exit;
      end;

    finally
      for I := 0 to SignerInfos.Count - 1 do
        TElPKCS7Signer(SignerInfos[I]).Free;
      SignerInfos.Free;
    end;
  end;

  {$ifndef B_6}
  if (Result = 0) and (voVerifyTimestamps in VerificationOptions) then
    Result := VerifyTimestamps(Signer);
  {$endif}
end;

class function TElMessageVerifier.IsSignatureDetached(Signature : pointer; Size :
  integer) : boolean;
var
  Msg : TElPKCS7Message;
  I : integer;
begin
  CheckLicenseKey();
  Result := false;
  Msg := TElPKCS7Message.Create;
  I := Msg.LoadFromBuffer(Signature, Size);
  if I = 0 then
  begin
    if Msg.ContentType = ctSignedData then
      Result := Length(Msg.SignedData.Content) = 0
    else if Msg.ContentType = ctAuthenticatedData then
      Result := Length(Msg.AuthenticatedData.Content) = 0;
  end;
  
  Msg.Free;
end;

function TElMessageVerifier.ExtractMACKey(AuthData : TElPKCS7AuthenticatedData;
  var Key: BufferType) : integer;
var
  I, J : integer;
  Certificate : TElX509Certificate;
  Recipient : TElPKCS7Recipient;
  Found : boolean;
  Sz : word;
begin
  Found := false;
  Certificate := nil;
  Recipient := nil;
  for I := 0 to AuthData.RecipientCount - 1 do
  begin
    for J := 0 to FCertStorage.Count - 1 do
    begin
      if (CompareRDN(FCertStorage.Certificates[J].IssuerRDN,
        AuthData.Recipients[I].Issuer.Issuer)) and
        (CompareStr(FCertStorage.Certificates[J].SerialNumber,
        AuthData.Recipients[I].Issuer.SerialNumber) = 0) then
      begin
        Certificate := FCertStorage.Certificates[J];
        Recipient := AuthData.Recipients[I];
        Found := true;
        Break;
      end;
    end;
    if Found and Certificate.PrivateKeyExists then
      Break;
  end;
  if (not Found) or (not Certificate.PrivateKeyExists) then
  begin
    Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
    Exit;
  end;
  Sz := 0;
  Certificate.SaveKeyToBuffer(nil, Sz);
  if Sz = 0 then
  begin
    Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
    Exit;
  end;
  if not DecryptKey(Certificate, Recipient, Key) then
  begin
    Result := SB_MESSAGE_ERROR_KEY_DECRYPTION_FAILED;
    Exit;
  end;
  Result := 0;
end;


function TElMessageVerifier.GetCertIDs(Index : integer) : TElPKCS7Issuer;
begin
  if (Index >= 0) and (Index < FCertIDs.Count) then
    Result := TElPKCS7Issuer(FCertIDs[Index])
  else
    Result := nil;
end;

function TElMessageVerifier.GetCertIDCount : integer;
begin
  Result := FCertIDs.Count;
end;

function TElMessageVerifier.GetCounterSignatureCertIDs(Index : integer) : TElPKCS7Issuer;
begin
  if (Index >= 0) and (Index < FCSCertIDs.Count) then
    Result := TElPKCS7Issuer(FCSCertIDs[Index])
  else
    Result := nil;
end;

function TElMessageVerifier.GetCounterSignatureCertIDCount : integer;
begin
  Result := FCSCertIDs.Count;
end;

procedure TElMessageVerifier.ClearCertIDs;
var
  I : integer;
begin
  for I := 0 to FCertIDs.Count - 1 do
    TElPKCS7Issuer(FCertIDs.Items[I]).Free;
  for I := 0 to FCSCertIDs.Count - 1 do
    TElPKCS7Issuer(FCSCertIDs.Items[I]).Free;
  FCertIDs.Clear;
  FCSCertIDs.Clear;
end;

function TElMessageVerifier.VerifyMessageDigests(Msg : TElPKCS7Message;
  Buffer: pointer; Size: integer) : integer;
var
  Func : TElHashFunction;
  HashResult, Digest : BufferType;
  I, K  : integer;
  DigestFound : boolean;
begin
  Result := 0;
  if not (voVerifyMessageDigests in FVerificationOptions) then
    Exit;
  for K := 0 to Msg.SignedData.SignerCount - 1 do
  begin
    if Msg.SignedData.Signers[K].AuthenticatedAttributes.Count = 0 then
      Continue;
    DigestFound := false;
    try
      if FInputIsDigest then
      begin
        SetLength(HashResult, Size);
        Move(Buffer^, HashResult[1], Length(HashResult));
      end
      else
      begin
        Func := TElHashFunction.Create(HashAlgorithm);
        try
          Func.Update(Buffer, Size);
          HashResult := Func.Finish;
        finally
          FreeAndNil(Func);
        end;
      end;
      for I := 0 to Msg.SignedData.Signers[K].AuthenticatedAttributes.Count - 1 do
      begin
        if CompareContent(Msg.SignedData.Signers[K].AuthenticatedAttributes.Attributes[I], SB_OID_MESSAGE_DIGEST) then
        begin
          DigestFound := true;
          Digest := Msg.SignedData.Signers[K].AuthenticatedAttributes.Values[I].Strings[0];
          if (Length(Digest) = Length(HashResult) + 2) and
            (Digest[1] = #$04) and (Digest[2] = Chr(Length(HashResult))) and
            CompareMem(@Digest[3], @HashResult[1], Length(HashResult))
          then
            Result := 0
          else
          begin
            Result := SB_MESSAGE_ERROR_INVALID_DIGEST;
            Exit;
          end;
        end;
      end;
      if not DigestFound then
      begin
        Result := SB_MESSAGE_ERROR_DIGEST_NOT_FOUND;
        Exit;
      end;
    except
      Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
    end;
  end;
end;

{$ifndef B_6}
function TElMessageVerifier.VerifyTimestamps(Signer: TElPKCS7Signer): integer;
var
  I : integer;
  Val : BufferType;
  Info : TElClientTSPInfo;
  R : integer;
begin
  Result := 0;
  for I := 0 to Signer.UnauthenticatedAttributes.Count - 1 do
  begin
    if CompareContent(Signer.UnauthenticatedAttributes.Attributes[I], SB_OID_TIMESTAMP_TOKEN) then
    begin
      if Signer.UnauthenticatedAttributes.Values[I].Count > 0 then
      begin
        Val := Signer.UnauthenticatedAttributes.Values[I][0];
        Info := TElClientTSPInfo.Create;
        R := Info.ParseCMS(BytesOfString(Val));
        if (Result = 0) and (R <> 0) then
          Result := R;
        FTimestamps.Add(Info);
      end;
    end;
  end;
end;

function TElMessageVerifier.GetTimestamp(Index: integer): TElClientTSPInfo;
begin
  Result := TElClientTSPInfo(FTimestamps[Index]);
end;

function TElMessageVerifier.GetTimestampCount: integer;
begin
  Result := FTimestamps.Count;
end;

procedure TElMessageVerifier.ClearTimestamps;
var
  I : integer;
begin
  for I := 0 to FTimestamps.Count - 1 do
    TElClientTSPInfo(FTimestamps[I]).Free;
  FTimestamps.Clear;
end;
{$endif}

////////////////////////////////////////////////////////////////////////////////
// TElMessageSigner

constructor TElMessageSigner.Create(AOwner : TComponent);
begin
  inherited Create (AOwner);
  FAAttributes := TElPKCS7Attributes.Create;
  FUAttributes := TElPKCS7Attributes.Create;
  FIncludeCertificates := true;
  FAlgorithm := SB_ALGORITHM_DGST_SHA1;
  FMacAlgorithm := SB_ALGORITHM_MAC_HMACSHA1;
  fUseUndefSize := true;
  FSignatureType := mstPublicKey;
  FUsePSS := false;
  FSigningOptions := [soInsertMessageDigests];
  FContentType := SB_OID_PKCS7_DATA;
end;


destructor TElMessageSigner.Destroy;
begin
  FreeAndNil(FAAttributes);
  FreeAndNil(FUAttributes);
  inherited;
end;


procedure TElMessageSigner.SetCertStorage(Value : TElMemoryCertStorage);
begin
  FCertStorage := Value;
  if FCertStorage <> nil then
    FCertStorage.FreeNotification(Self);
end;

procedure TElMessageSigner.SetRecipientCerts(Value : TElCustomCertStorage);
begin
  FRecipientCerts := Value;
  if FRecipientCerts <> nil then
    FRecipientCerts.FreeNotification(Self);
end;

{$ifndef B_6}
procedure TElMessageSigner.SetTSPClient(Value : TElCustomTSPClient);
begin
  FTSPClient := Value;
  if FTSPClient <> nil then
    FTSPClient.FreeNotification(Self);
end;
{$endif}

procedure TElMessageSigner.Notification(AComponent : TComponent; AOperation :
  TOperation);
begin
  inherited;
  if (AComponent = FCertStorage) and (AOperation = opRemove) then
    CertStorage := nil
  else if (AComponent = FRecipientCerts) and (AOperation = opRemove) then
    RecipientCerts := nil
  {$ifndef B_6}
  else
  if (AComponent = FTSPClient) and (AOperation = opRemove) then
    TSPClient := nil
  {$endif}
  ;
end;

function TElMessageSigner.Sign(InBuffer : pointer; InSize : integer; OutBuffer :
  pointer; var OutSize : integer; Detached : boolean = false) : integer;
var
  FMessage : TElPKCS7Message;
  SgnData : TElPKCS7SignedData;
  AuthData : TElPKCS7AuthenticatedData;
  I : integer;
  TmpS, DAlg : BufferType;
  Sz : word;

  MinKeyLen : integer;
  SzInt, OldOutSize : integer;
  FakedCert : TElFakedX509Certificate;
  {$ifdef SB_HAS_WINCRYPT}
  Str : TElCustomCertStorage;
  {$endif}
  Err : boolean;
  DigestAttrFound, ContentAttrFound : boolean;
  Hash : BufferType;
  Key  : BufferType;
  Index : integer;
  PrivateKeyFound: boolean;
  TSResult : integer;
begin
  CheckLicenseKey();




  if FSignatureType = mstPublicKey then
  begin
    if (not Assigned(FCertStorage)) or (FCertStorage.Count = 0) then
    begin
      Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
      Exit;
    end;
    PrivateKeyFound := false;
    for I := 0 to FCertStorage.Count - 1 do
    begin
      if not FCertStorage.Certificates[I].PrivateKeyExists then
        Continue;
      Sz := 0;
      FCertStorage.Certificates[I].SaveKeyToBuffer(nil, Sz);
      if Sz = 0 then
      begin
        FakedCert := TElFakedX509Certificate.Create(nil);
        FCertStorage.Certificates[I].Clone(FakedCert{$ifndef HAS_DEF_PARAMS}, true{$endif});
        {$ifdef SB_HAS_WINCRYPT}
        {$ifndef NET_CF}
        if FakedCert.BelongsTo = BT_WINDOWS then
        begin
          OldOutSize := OutSize;
          if FIncludeCertificates then
            Str := FCertStorage
          else
            Str := nil;
          Err := FakedCert.SignWin32(InBuffer, InSize, OutBuffer, OutSize, FAAttributes,
            FUAttributes, Detached, Str);
          if not Err then
          begin
            if OldOutSize < OutSize then
              Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL
            else
              Result := SB_MESSAGE_ERROR_SIGNING_FAILED;
          end
          else
            Result := 0;
          {$ifndef B_6}
          if FTSPClient <> nil then
          begin
            if Result = 0 then
            begin
              FMessage := TElPKCS7Message.Create;
              try
                if FMessage.LoadFromBuffer(OutBuffer, OutSize) = 0 then
                begin
                  TimestampMessage(FMessage);
                  OutSize := OldOutSize;
                  if not FMessage.SaveToBuffer(OutBuffer, OutSize) then
                    Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
                end;
              finally
                FreeAndNil(FMessage);
              end;
            end
            else
              Inc(OutSize, 4096);
          end;
          {$endif}
          FakedCert.Free;
          Exit;
        end
        {$ifndef SBB_NO_PKCS11}
        else 
        if FakedCert.BelongsTo = BT_PKCS11 then
          PrivateKeyFound := true
        {$endif}
        ;
        {$endif}
        {$endif}

        FakedCert.Free;
      end
      else
        PrivateKeyFound := true;
    end;
    if not PrivateKeyFound then
    begin
      Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
      Exit;
    end;
  end
  else if SignatureType = mstMAC then
  begin
    if (not Assigned(FRecipientCerts)) or (FRecipientCerts.Count = 0) then
    begin
      Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
      Exit;
    end;
  end;

  SzInt := CalculateEstimatedSize(InSize, Detached) + 128;
  if OutSize < SzInt then
  begin
    OutSize := SzInt;
    Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
    Exit;
  end;
  SetLength(TmpS, InSize);
  Move(InBuffer^, TmpS[1], InSize);

  FMessage := TElPKCS7Message.Create;
  FMessage.UseUndefSize := FUseUndefSize;
  if FSignatureType = mstPublicKey then
  begin
    FMessage.ContentType := ctSignedData;
    SgnData := FMessage.SignedData;
    SgnData.Version := 1;
    if FIncludeCertificates then
      FCertStorage.ExportTo(SgnData.Certificates);
    SgnData.ContentType := FContentType;//SB_OID_PKCS7_DATA;
    if not Detached then
      SgnData.Content := TmpS
    else
      SgnData.Content := EmptyBuffer;

    if FAAttributes.Count > 0 then
    begin
      // calculating digest over content data if authenticated attributes are present
      Hash := CalculateDigest(@TmpS[1], Length(TmpS), FAlgorithm);
      // adding the 'message digest' attribute to the set
      // of authenticated attributes
      if soInsertMessageDigests in FSigningOptions then
      begin
        DigestAttrFound := false;
        for I := 0 to FAAttributes.Count - 1 do
        begin
          if (not DigestAttrFound) and (CompareContent(FAAttributes.Attributes[I],
            SB_OID_MESSAGE_DIGEST)) then
          begin
            FAAttributes.Values[I].Clear;
            FAAttributes.Values[I].Add(FormatAttributeValue(SB_ASN1_OCTETSTRING, Hash));
            DigestAttrFound := true;
            Break;
          end;
        end;
        if not DigestAttrFound then
        begin
          I := FAAttributes.Count;
          FAAttributes.Count := I + 1;
          FAAttributes.Attributes[I] := SB_OID_MESSAGE_DIGEST;
          FAAttributes.Values[I].Add(FormatAttributeValue(SB_ASN1_OCTETSTRING, Hash));
        end;
      end;

      I := 0;
      FAAttributes.SaveToBuffer(nil, I);
      SetLength(TmpS, I);
      if not FAAttributes.SaveToBuffer(@TmpS[1], I) then
      begin
        Result := SB_MESSAGE_ERROR_INTERNAL_ERROR;
        FMessage.Free;
        Exit;
      end;
    end;
    Hash := CalculateDigest(@TmpS[1], Length(TmpS), FAlgorithm);
    if Length(Hash) = 0 then
    begin
      Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
      FMessage.Free;
      Exit;
    end;

    DAlg := GetOIDByHashAlgorithm(FAlgorithm);
    for I := 0 to FCertStorage.Count - 1 do
    begin
      if not FCertStorage.Certificates[I].PrivateKeyExists then
        Continue;
      Sz := 0;
      FCertStorage.Certificates[I].SaveKeyToBuffer(nil, Sz);
      if (Sz > 0) or (FCertStorage.Certificates[I].BelongsTo = BT_PKCS11) then
      begin
        if not FillSigner(SgnData.Signers[SgnData.AddSigner], FCertStorage.Certificates[I],
          DAlg, @Hash[1], Length(Hash)) then
        begin
          Result := SB_MESSAGE_ERROR_SIGNING_FAILED;
          FMessage.Free;
          Exit;
        end;
      end
      else
      begin
        // possibly, we have a win32 certificate
        FakedCert := TElFakedX509Certificate.Create(nil);
        FCertStorage.Certificates[I].Clone(FakedCert{$ifndef HAS_DEF_PARAMS}, true{$endif});
        {$ifndef CLX_USED}
        if FakedCert.BelongsTo = BT_WINDOWS then
        begin
          OldOutSize := OutSize;
          Err := FakedCert.SignWin32(InBuffer, InSize, OutBuffer, OutSize, FAAttributes,
            FUAttributes, Detached);
          if not Err then
          begin
            if OldOutSize < OutSize then
              Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL
            else
              Result := SB_MESSAGE_ERROR_SIGNING_FAILED;
          end
          else
            Result := 0;
            
          FakedCert.Free;
          FMessage.Free;
          Exit;
        end;
        {$endif}
      end;
    end;
  end
  else if FSignatureType = mstMAC then
  begin
    FMessage.ContentType := ctAuthenticatedData;
    AuthData := FMessage.AuthenticatedData;

    // setting up AuthenticationData basic properties
    AuthData.Version := 0;
    AuthData.ContentType := SB_OID_PKCS7_DATA;
    if not Detached then
      AuthData.Content := TmpS
    else
      AuthData.Content := EmptyBuffer;

    // generating secret MAC key
    MinKeyLen := 64;
    for I := 0 to FRecipientCerts.Count - 1 do
    begin
      if FRecipientCerts.Certificates[I].CanEncrypt then
        MinKeyLen := Min(MinKeyLen, FRecipientCerts.Certificates[I].GetPublicKeySize shr 3 - 16);
    end;

    SetLength(Key, MinKeyLen);
    SBRndGenerate(@Key[1], Length(Key));

    // encrypting key for each recipient
    Err := true;
    if Assigned(FRecipientCerts) then
    begin
      for I := 0 to FRecipientCerts.Count - 1 do
      begin
        if FRecipientCerts.Certificates[I].CanEncrypt then
        begin
          Index := AuthData.AddRecipient;
          if FillRecipient(AuthData.Recipients[Index], FRecipientCerts.Certificates[I],
            Key) then
            Err := false
          else
            AuthData.RemoveRecipient(Index);
        end;
      end;
    end;
    if Err then
    begin
      Result := SB_MESSAGE_ERROR_NO_RECIPIENTS;
      FMessage.Free;
      Exit;
    end;

    if FAAttributes.Count > 0 then
    begin
      // calculating digest over content data and MAC over attributes
      Hash := CalculateDigest(@TmpS[1], Length(TmpS), FAlgorithm);
      // adding the 'message digest' and 'content-type' attributes to the set
      // of authenticated attributes
      DigestAttrFound := false;
      ContentAttrFound := false;
      for I := 0 to FAAttributes.Count - 1 do
      begin
        if (not DigestAttrFound) and (CompareContent(FAAttributes.Attributes[I],
          SB_OID_MESSAGE_DIGEST) and
          (soInsertMessageDigests in FSigningOptions)) then
        begin
          FAAttributes.Values[I].Clear;
          FAAttributes.Values[I].Add(FormatAttributeValue(SB_ASN1_OCTETSTRING, Hash));
          DigestAttrFound := true;
        end;
        if (CompareContent(FAAttributes.Attributes[I],
          SB_OID_CONTENT_TYPE)) then
          ContentAttrFound := true;
        if DigestAttrFound and ContentAttrFound then
          Break;
      end;
      if (not DigestAttrFound) and
      (soInsertMessageDigests in FSigningOptions) then
      begin
        I := FAAttributes.Count;
        FAAttributes.Count := I + 1;
        FAAttributes.Attributes[I] := SB_OID_MESSAGE_DIGEST;
        FAAttributes.Values[I].Add(FormatAttributeValue(SB_ASN1_OCTETSTRING, Hash));
      end;
      if not ContentAttrFound then
      begin
        I := FAAttributes.Count;
        FAAttributes.Count := I + 1;
        FAAttributes.Attributes[I] := SB_OID_CONTENT_TYPE;
        FAAttributes.Values[I].Add(FormatAttributeValue(SB_ASN1_OBJECT, SB_OID_PKCS7_DATA));
      end;
      // passing authenticated attributes to MAC function input
      FAAttributes.Copy(AuthData.AuthenticatedAttributes);
      FUAttributes.Copy(AuthData.UnauthenticatedAttributes);
      AuthData.RecalculateAuthenticatedAttributes;
      TmpS := AuthData.AuthenticatedAttributesPlain;
      if not CalculateMAC(@TmpS[1], Length(TmpS), Key, Hash, FMacAlgorithm) then
      begin
        Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
        FMessage.Free;
        Exit;
      end;
      AuthData.MacAlgorithm := GetOIDByAlgorithm(FMacAlgorithm);
      AuthData.DigestAlgorithm := GetOIDByHashAlgorithm(FAlgorithm);
      AuthData.Mac := Hash;
    end
    else
    begin
      // calculating MAC over content data
      if not CalculateMAC(@TmpS[1], Length(TmpS), Key, Hash, FMacAlgorithm) then
      begin
        Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
        FMessage.Free;
        Exit;
      end;
      AuthData.MacAlgorithm := GetOIDByAlgorithm(FMacAlgorithm);
      AuthData.DigestAlgorithm := '';
      AuthData.Mac := Hash;
    end;
  end;
  {$ifndef B_6}
  if FTSPClient <> nil then
    TSResult := TimestampMessage(FMessage)
  else
  {$endif}
    TSResult := 0;

  if TSResult = 0 then
  begin
    if not FMessage.SaveToBuffer(OutBuffer, OutSize) then
      Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL
    else
      Result := 0;
  end
  else
    Result := TSResult;

  FMessage.Free;
end;

function TElMessageSigner.FillSigner(Signer : TElPKCS7Signer; Certificate :
  TElX509Certificate; const DigestAlgorithm : BufferType; Hash : pointer; HashSize :
  integer): boolean;
var
  I : integer;
  EncryptedDigest : BufferType;
begin
  Result := false;
  Signer.Version := 1;
  Signer.Issuer.SerialNumber := CloneBuffer(Certificate.SerialNumber);

  Signer.Issuer.Issuer.Count := Certificate.IssuerRDN.Count;
  for I := 0 to Certificate.IssuerRDN.Count - 1 do
  begin
    Signer.Issuer.Issuer.Values[I] := CloneBuffer(Certificate.IssuerRDN.Values[I]);
    Signer.Issuer.Issuer.OIDs[I] := CloneBuffer(Certificate.IssuerRDN.OIDs[I]);
    Signer.Issuer.Issuer.Tags[I] := Certificate.IssuerRDN.Tags[I];
  end;
  Signer.DigestAlgorithm := CloneBuffer(DigestAlgorithm);
  Signer.DigestAlgorithmParams := EmptyBuffer;
  if (Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) and (not UsePSS) then
  begin
    Signer.DigestEncryptionAlgorithm := SB_OID_RSAENCRYPTION;
    Signer.DigestEncryptionAlgorithmParams := EmptyBuffer;
    if not EncryptRSA(Certificate, Hash, HashSize, DigestAlgorithm, EncryptedDigest) then
      Exit;
    Signer.EncryptedDigest := EncryptedDigest;
    FAAttributes.Copy(Signer.AuthenticatedAttributes);
    FUAttributes.Copy(Signer.UnauthenticatedAttributes);
  end
  else if (Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) or
    ((Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) and UsePss) then
  begin
    Signer.DigestEncryptionAlgorithm := SB_OID_RSAPSS;
    Signer.DigestEncryptionAlgorithmParams :=
      WritePSSParams(FAlgorithm, Certificate.PSSParams.SaltSize,
        Certificate.PSSParams.MGF, Certificate.PSSParams.TrailerField);
    if not SignRSAPSS(Certificate, Hash, HashSize, 
      EncryptedDigest)
    then
      Exit;
    Signer.EncryptedDigest := EncryptedDigest;
    FAAttributes.Copy(Signer.AuthenticatedAttributes);
    FUAttributes.Copy(Signer.UnauthenticatedAttributes);
  end
  else if Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    Signer.DigestEncryptionAlgorithm := SB_CERT_OID_DSA;
    Signer.DigestEncryptionAlgorithmParams := EmptyBuffer;
    if not SignDSA(Certificate, Hash, HashSize, EncryptedDigest) then
      Exit;
    Signer.EncryptedDigest := EncryptedDigest;
    FAAttributes.Copy(Signer.AuthenticatedAttributes);
    FUAttributes.Copy(Signer.UnauthenticatedAttributes);
  end
  else
    Exit;

  Result := true;
end;

function TElMessageSigner.Countersign(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer): integer;
var
  Msg : TElPKCS7Message;
  SgnData : TElPKCS7SignedData;
  I, J, K : integer;
  Countersignature : TElPKCS7Signer;
  Hash, HashSource : BufferType;
  DAlg : BufferType;
  Index : integer;
  Buf : BufferType;
  BufSize : integer;
  Tag : TElASN1ConstrainedTag;
begin
  // Only public-key countersigning is allowed
  if FSignatureType <> mstPublicKey then
  begin
    Result := SB_MESSAGE_ERROR_UNSUPPORTED_SIGNATURE_TYPE;
    Exit;
  end;
  // checking that CertStorage is assigned
  if (not Assigned(FCertStorage)) or (FCertStorage.Count = 0) then
  begin
    Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
    Exit;
  end;
  // processing input message
  try
    Msg := TElPKCS7Message.Create;
    try
      Result := Msg.LoadFromBuffer(InBuffer, InSize);
      if Result <> 0 then
        Exit;
      if Msg.ContentType <> ctSignedData then
      begin
        Result := SB_MESSAGE_ERROR_NO_SIGNED_DATA;
        Exit;
      end;
      SgnData := Msg.SignedData;

      if IncludeCertificates then
      begin
        for I := 0 to FCertStorage.Count - 1 do
          SgnData.Certificates.Add(FCertStorage.Certificates[I], false);
      end;

      // preparing attributes according to the specification (RFC3852)
      // 1. The signedAttributes field MUST NOT contain a content-type
      //   attribute; there is no content type for countersignatures.
      I := 0;
      while I < FAAttributes.Count do
      begin
        if CompareContent(FAAttributes.Attributes[I], SB_OID_CONTENT_TYPE) then
          FAAttributes.Remove(I)
        else
          Inc(I);
      end;

      // countersigning with each certificate that has a corresponding private key
      for I := 0 to FCertStorage.Count - 1 do
      begin
        if FCertStorage.Certificates[I].PrivateKeyExists and
          FCertStorage.Certificates[I].PrivateKeyExtractable then
        begin
          // signing all existing signatures
          for J := 0 to SgnData.SignerCount - 1 do
          begin
            // calculating hash over existing digest
            HashSource := SgnData.Signers[J].EncryptedDigest;
            Hash := CalculateDigest(@HashSource[1], Length(HashSource), FAlgorithm);
            if Length(Hash) = 0 then
            begin
              Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
              Exit;
            end;
            DAlg := GetOIDByHashAlgorithm(FAlgorithm);

            // 2. The signedAttributes field MUST contain a message-digest
            //   attribute if it contains any other attributes.
            if FAAttributes.Count > 0 then
            begin
              Index := -1;
              for K := 0 to FAAttributes.Count - 1 do
              begin
                if CompareContent(FAAttributes.Attributes[K], SB_OID_MESSAGE_DIGEST) then
                begin
                  Index := K;
                  Break;
                end;
              end;
              if Index = -1 then
              begin
                Index := FAAttributes.Count;
                FAAttributes.Count := FAAttributes.Count + 1;
                FAAttributes.Attributes[Index] := SB_OID_MESSAGE_DIGEST;
              end;
              FAAttributes.Values[Index].Clear();
              FAAttributes.Values[Index].Add(FormatAttributeValue(SB_ASN1_OCTETSTRING,
                Hash));
              // calculating hash over authenticated attributes
              BufSize := 0;
              FAAttributes.SaveToBuffer(nil,
                BufSize);
              SetLength(Buf, BufSize);
              FAAttributes.SaveToBuffer(@Buf[1], BufSize);
              Hash := CalculateDigest(@Buf[1], BufSize, FAlgorithm);
            end;

            // creating countersignature field
            Countersignature := TElPKCS7Signer.Create;
            try
              if not FillSigner(Countersignature, FCertStorage.Certificates[I], DAlg,
                @Hash[1], Length(Hash)) then
              begin
                Result := SB_MESSAGE_ERROR_SIGNING_FAILED;
                Exit;
              end;

              // adding the corresponding attribute to the signer's
              // unsigned attributes list
              Tag := TElASN1ConstrainedTag.Create;
              try
                SaveSignerInfo(Tag, Countersignature);
                BufSize := 0;
                Tag.SaveToBuffer(nil, BufSize);
                SetLength(Buf, BufSize);
                Tag.SaveToBuffer(@Buf[1], BufSize);
                SetLength(Buf, BufSize);
              finally
                FreeAndNil(Tag);
              end;
              Index := SgnData.Signers[J].UnauthenticatedAttributes.Count;
              SgnData.Signers[J].UnauthenticatedAttributes.Count := Index + 1;
              SgnData.Signers[J].UnauthenticatedAttributes.Attributes[Index] :=
                SB_OID_COUNTER_SIGNATURE;
              SgnData.Signers[J].UnauthenticatedAttributes.Values[Index].Add(Buf);
            finally
              FreeAndNil(CounterSignature);
            end;                           
          end;
        end;
      end;

      // saving the message
      if Msg.SaveToBuffer(OutBuffer, OutSize) then
        Result := 0
      else
        Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
      
    finally
      FreeAndNil(Msg);
    end;
  except
    Result := SB_MESSAGE_ERROR_SIGNING_FAILED;
  end;
end;                                                  

function TElMessageSigner.SignRSAPSS(Certificate : TElX509Certificate;
  Digest : pointer; DigestSize: integer; var Signature : BufferType) : boolean;
var
  KeyB : ByteArray;
  KeySize : word;
  SigSize : integer;
begin
  Result := false;
  KeySize := 0;
  Certificate.SaveKeyToBuffer(nil, KeySize);

  SetLength(KeyB, KeySize);
  if not Certificate.SaveKeyToBuffer(@KeyB[0], KeySize) then
    Exit;

  SBRSA.SignPSS(Digest, DigestSize, FAlgorithm,
    Certificate.PSSParams.SaltSize, @KeyB[0], KeySize, nil, SigSize);

  SetLength(Signature, SigSize);

  if SBRSA.SignPSS(Digest, DigestSize, FAlgorithm,
    Certificate.PSSParams.SaltSize, @KeyB[0], KeySize, @Signature[1], SigSize)
  then
    Result := true;
end;


function TElMessageSigner.SignDSA(Certificate : TElX509Certificate; Digest : pointer;
  DigestSize: integer; var Signature : BufferType) : boolean;
var
  P, Q, G, Y, X, R, S : ByteArray;
  PSize, QSize, GSize, YSize, XSize, RSize, SSize, BlobSize : integer;
  {$ifndef SBB_NO_PKCS11}
  Ctx : TElPKCS11StgCtx;
  {$endif}
begin
  {$ifndef SBB_NO_PKCS11}
  if Certificate.BelongsTo = BT_PKCS11 then
  begin
    BlobSize := 0;
    Result := false;
    Ctx := GetContextByCertificate(Certificate);
    if Ctx = nil then
      Exit;
    try
      Ctx.Sign(Digest, DigestSize, nil, BlobSize);
      SetLength(Signature, BlobSize);
      if Ctx.Sign(Digest, DigestSize, @Signature[1], BlobSize) then
      begin
        SetLength(Signature, BlobSize);
        Result := true;
      end;
    except
      Result := false;
    end;
  end
  else
  {$endif}
  begin
    Result := false;
    PSize := 0;
    Certificate.GetDSSParams(nil, PSize, nil, QSize, nil, GSize, nil, YSize);
    SetLength(P, PSize);
    SetLength(Q, QSize);
    SetLength(G, GSize);
    SetLength(Y, YSize);
    Certificate.GetDSSParams(@P[0], PSize, @Q[0], QSize, @G[0], GSize, @Y[0], YSize);
    RSize := 21;
    SSize := 21;
    SetLength(R, RSize);
    SetLength(S, SSize);
    XSize := 0;
    Certificate.SaveKeyValueToBuffer(nil, XSize);
    SetLength(X, XSize);
    Certificate.SaveKeyValueToBuffer(@X[0], XSize);
    if not SBDSA.Sign(Digest, DigestSize, @P[0], PSize, @Q[0], QSize, @G[0], GSize,
      @X[0], XSize, @R[0], RSize, @S[0], SSize) then
      Exit;
    BlobSize := 0;
    SBDSA.EncodeSignature(@R[0], RSize, @S[0], SSize, nil, BlobSize);
    SetLength(Signature, BlobSize);
    if SBDSA.EncodeSignature(@R[0], RSize, @S[0], SSize, @Signature[1], BlobSize) then
    begin
      SetLength(Signature, BlobSize);
      Result := true;
    end
    else
      Result := false;
  end;
end;

function TElMessageSigner.CalculateEstimatedSize(InputSize : integer; Detached : boolean) : integer;
var
  DumbMessage : TElPKCS7Message;
  Cnt : BufferType;
  I, J, K : integer;
  SzA, SzB : integer;
  Sz : word;
begin
  Result := 0;
  if (not Assigned(FCertStorage)) and 
     (SignatureType = mstPublicKey) then
    Exit;
  if (not Assigned(FRecipientCerts)) and 
     (SignatureType = mstMAC) then
    Exit;
    
  DumbMessage := TElPKCS7Message.Create;
  if SignatureType = mstPublicKey then
  begin
    DumbMessage.ContentType := ctSignedData;
    FCertStorage.ExportTo(DumbMessage.SignedData.Certificates);
    if not Detached then
      SetLength(Cnt, InputSize)
    else
      SetLength(Cnt, 0);
    DumbMessage.SignedData.Content := CloneBuffer(Cnt);
    DumbMessage.SignedData.ContentType := FContentType;//SB_OID_PKCS7_DATA;
    for I := 0 to FCertStorage.Count - 1 do
    begin
      Sz := 0;
      FCertStorage.Certificates[I].SaveKeyToBuffer(nil, Sz);
      if (Sz > 0) or (FCertStorage.Certificates[I].PrivateKeyExists and
        (FCertStorage.Certificates[I].BelongsTo = BT_PKCS11)) then
      begin
        K := DumbMessage.SignedData.AddSigner;
        DumbMessage.SignedData.Signers[K].Issuer.SerialNumber :=
          FCertStorage.Certificates[I].SerialNumber;
        DumbMessage.SignedData.Signers[K].Issuer.Issuer.Count :=
          FCertStorage.Certificates[I].IssuerRDN.Count;
        for J := 0 to FCertStorage.Certificates[I].IssuerRDN.Count - 1 do
        begin
          DumbMessage.SignedData.Signers[K].Issuer.Issuer.Values[J] :=
            FCertStorage.Certificates[I].IssuerRDN.Values[J];
          DumbMessage.SignedData.Signers[K].Issuer.Issuer.OIDs[J] :=
            FCertStorage.Certificates[I].IssuerRDN.OIDs[J];
        end;
        DumbMessage.SignedData.Signers[K].DigestAlgorithm := TBufferTypeConst(#0#0#0#0#0#0#0#0#0#0#0#0);
        DumbMessage.SignedData.Signers[K].DigestAlgorithmParams := EmptyBuffer;
        if (FCertStorage.Certificates[I].PublicKeyAlgorithm =
          SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) and (not UsePSS) then
        begin
          DumbMessage.SignedData.Signers[K].DigestEncryptionAlgorithm := SB_OID_RSAENCRYPTION;
          DumbMessage.SignedData.Signers[K].DigestEncryptionAlgorithmParams := EmptyBuffer;
          SzA := 0;
          SzB := 0;
          FCertStorage.Certificates[I].GetRSAParams(nil, SzA, nil, SzB);
          SetLength(Cnt, SzA);
          DumbMessage.SignedData.Signers[K].EncryptedDigest := CloneBuffer(Cnt);
          FAAttributes.Copy(DumbMessage.SignedData.Signers[K].AuthenticatedAttributes);
          FUAttributes.Copy(DumbMessage.SignedData.Signers[K].UnauthenticatedAttributes);
        end
        else if ((FCertStorage.Certificates[I].PublicKeyAlgorithm =
          SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) and UsePSS) or
          (FCertStorage.Certificates[I].PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) then
        begin
          DumbMessage.SignedData.Signers[K].DigestEncryptionAlgorithm := SB_OID_RSAPSS;
          DumbMessage.SignedData.Signers[K].DigestEncryptionAlgorithmParams :=
            WritePSSParams(SB_ALGORITHM_DGST_SHA1, 20{$ifndef HAS_DEF_PARAMS}, SB_CERT_MGF1, 1{$endif});
          SzA := 0;
          SzB := 0;
          FCertStorage.Certificates[I].GetRSAParams(nil, SzA, nil, SzB);
          SetLength(Cnt, SzA);
          DumbMessage.SignedData.Signers[K].EncryptedDigest := CloneBuffer(Cnt);
          FAAttributes.Copy(DumbMessage.SignedData.Signers[K].AuthenticatedAttributes);
          FUAttributes.Copy(DumbMessage.SignedData.Signers[K].UnauthenticatedAttributes);
        end
      end;
    end;
  end
  else if SignatureType = mstMAC then
  begin
    DumbMessage.ContentType := ctAuthenticatedData;
    FRecipientCerts.ExportTo(DumbMessage.AuthenticatedData.OriginatorCerts);
    if not Detached then
      SetLength(Cnt, InputSize)
    else
      SetLength(Cnt, 0);
    DumbMessage.AuthenticatedData.Content := CloneBuffer(Cnt);
    DumbMessage.AuthenticatedData.ContentType := SB_OID_PKCS7_DATA;
    for I := 0 to FRecipientCerts.Count - 1 do
    begin
      K := DumbMessage.AuthenticatedData.AddRecipient;
      DumbMessage.AuthenticatedData.Recipients[K].Issuer.SerialNumber :=
        FRecipientCerts.Certificates[I].SerialNumber;
      DumbMessage.AuthenticatedData.Recipients[K].Issuer.Issuer.Count :=
        FRecipientCerts.Certificates[I].IssuerRDN.Count;
      for J := 0 to FRecipientCerts.Certificates[I].IssuerRDN.Count - 1 do
      begin
        DumbMessage.AuthenticatedData.Recipients[K].Issuer.Issuer.Values[J] :=
          FRecipientCerts.Certificates[I].IssuerRDN.Values[J];
        DumbMessage.AuthenticatedData.Recipients[K].Issuer.Issuer.OIDs[J] :=
          FRecipientCerts.Certificates[I].IssuerRDN.OIDs[J];
      end;
      DumbMessage.AuthenticatedData.Recipients[K].KeyEncryptionAlgorithm := TBufferTypeConst(#0#0#0#0#0#0#0#0#0#0#0#0);
      DumbMessage.AuthenticatedData.Recipients[K].KeyEncryptionAlgorithmParams := EmptyBuffer;
      SzA := FRecipientCerts.Certificates[I].GetPublicKeySize shr 3 + 4;
      SetLength(Cnt, SzA);
      DumbMessage.AuthenticatedData.Recipients[K].EncryptedKey := CloneBuffer(Cnt);
    end;
    DumbMessage.AuthenticatedData.MacAlgorithm := TBufferTypeConst(#0#0#0#0#0#0#0#0#0#0#0#0);
    DumbMessage.AuthenticatedData.DigestAlgorithm := TBufferTypeConst(#0#0#0#0#0#0#0#0#0#0#0#0);
    SetLength(Cnt, 64);
    DumbMessage.AuthenticatedData.Mac := CloneBuffer(Cnt);
    FAAttributes.Copy(DumbMessage.AuthenticatedData.AuthenticatedAttributes);
    FUAttributes.Copy(DumbMessage.AuthenticatedData.UnauthenticatedAttributes);
  end;
  SzA := 0;
  DumbMessage.SaveToBuffer(nil, SzA);
  {$ifndef B_6}
  if (FTSPClient <> nil) and (DumbMessage.ContentType = ctSignedData) then
    Inc(SzA, 3072 * DumbMessage.SignedData.SignerCount);
  {$endif}
  DumbMessage.Free;
  Result := SzA;
end;

{$ifndef B_6}
function TElMessageSigner.TimestampMessage(Msg : TElPKCS7Message) : integer;
var
  OldCount : integer;
  I : integer;
  Func : TElHashFunction;
  Buf : BufferType;
  Hash : ByteArray;
  ServerResult : TSBPKIStatus;
  FailureInfo : integer;
  ReplyCMS : ByteArray;
begin
  Result := 0;
  if (FTSPClient = nil) or (Msg.ContentType <> ctSignedData) then
    Exit;
  for I := 0 to Msg.SignedData.SignerCount - 1 do
  begin
    SetLength(Hash, 0);
    try
      Func := TElHashFunction.Create(FTSPClient.HashAlgorithm);
      try
        Buf := Msg.SignedData.Signers[I].EncryptedDigest;
        Func.Update(@Buf[1], Length(Buf));
        Hash := BytesOfString(Func.Finish); 
      finally
        FreeAndNil(Func);
      end;
    except
      on E : EElHashFunctionUnsupportedError do
      begin
        Result := SB_MESSAGE_ERROR_UNSUPPORTED_DIGEST_ALGORITHM;
        Exit;
      end;
    end;
    Result := FTSPClient.Timestamp(Hash, ServerResult, FailureInfo, ReplyCMS);
    if Result <> 0 then
      Exit;
    if Length(ReplyCMS) > 0 then
    begin
      // adding a timestamp to the unauthenticated attributes sequence
      OldCount := FUAttributes.Count;
      Msg.SignedData.Signers[I].UnauthenticatedAttributes.Count := OldCount + 1;
      Msg.SignedData.Signers[I].UnauthenticatedAttributes.Attributes[OldCount] := SB_OID_TIMESTAMP_TOKEN;
      Msg.SignedData.Signers[I].UnauthenticatedAttributes.Values[OldCount].Add(StringOfBytes(ReplyCMS));
    end;
  end;
end;
{$endif}


////////////////////////////////////////////////////////////////////////////////
// Auxiliary routines

function CompareRDN(Name1, Name2 : TElRelativeDistinguishedName) : boolean;
var
  I, J : integer;
  OID, Value : BufferType;
  Found : boolean;
  TagID : byte;
begin
  Result := false;
  if Name1.Count <> Name2.Count then
    Exit;
  for I := 0 to Name1.Count - 1 do
  begin
    OID := Name1.OIDs[I];
    Value := Name1.Values[I];
    TagID := Name1.Tags[I];
    Found := false;
    for J := 0 to Name2.Count - 1 do
    begin
      if (CompareStr(Name2.OIDs[J], OID) = 0) and (CompareStr(Name2.Values[J], Value) = 0) and
        (Name2.Tags[J] = TagID) then
      begin
        Found := true;
        Break;
      end;
    end;
    if not Found then
      Exit;
  end;
  Result := true;
end;

function FormatAttributeValue(TagID : integer; Value : BufferType) : BufferType;
var
  CTag : TElASN1SimpleTag;
  Sz : integer;
begin
  CTag := TElASN1SimpleTag.Create;
  CTag.TagID := TagID;
  CTag.Content := Value;
  Sz := 0;
  CTag.SaveToBuffer(nil, Sz);
  SetLength(Result, Sz);
  CTag.SaveToBuffer(@Result[1], Sz);
  SetLength(Result, Sz);
  CTag.Free;
end;

function UnformatAttributeValue(Value : BufferType; out TagID : integer) : BufferType;
var
  Tag : TElASN1ConstrainedTag;
  Sz : integer;
begin
  Tag := TElASN1ConstrainedTag.Create;
  if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
  begin
    if Tag.Count > 0 then
    begin
      Sz := 0;
      Tag.GetField(0).WriteHeader := false;
      Tag.GetField(0).SaveToBuffer(nil, Sz);
      SetLength(Result, Sz);
      Tag.GetField(0).SaveToBuffer(@Result[1], Sz);
      SetLength(Result, Sz);
      TagID := Tag.GetField(0).TagID;
    end
    else
    begin
      Result := Value;
      TagID := 0;
    end;
  end
  else
  begin
    Result := Value;
    TagID := 0;
  end;
  
  Tag.Free;
end;

end.
