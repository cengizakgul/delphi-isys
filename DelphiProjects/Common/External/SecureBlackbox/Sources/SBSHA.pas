(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSHA;

interface

uses
  Classes, SysUtils,
  SBUtils;

type

  TSHA1Context = packed  record
    Size: int64;//cardinal;
    Buffer: array [0..63] of byte;
    BufSize: cardinal;
    A, B, C, D, E: longword;
  end;

procedure InitializeSHA1(var Context: TSHA1Context); 

function HashSHA1(const S: string): TMessageDigest160; overload;
procedure HashSHA1(var Context: TSHA1Context; Chunk: pointer; Size: cardinal); overload;
function HashSHA1(Buffer: pointer; Size: cardinal): TMessageDigest160; overload;
procedure InternalSHA1(Chunk: PLongWordArray; var A, B, C, D, E: longword);

function FinalizeSHA1(var Context: TSHA1Context): TMessageDigest160; 


implementation

{$ifdef ActiveX_registered}
uses
  SBClientBase;
{$endif}


procedure InternalSHA1(Chunk: PLongWordArray; var A, B, C, D, E: longword);
var
  SA, SB, SC, SD, SE: longword;
  T: longword;
  I: integer;
begin
  SA := A; SB := B; SC := C; SD := D; SE := E;

  for I := 0 to 79 do
  begin
    T := ((A shl 5) or (A shr 27)) + E + Chunk[I];
    case I of
      0..19: Inc(T, ((B and C) or (not B and D)) + $5A827999);
      20..39: Inc(T, (B xor C xor D) + $6ED9EBA1);
      40..59: Inc(T, ((B and C) or (B and D) or (C and D)) + $8F1BBCDC);
      60..79: Inc(T, (B xor C xor D) + $CA62C1D6);
    end;
    E := D;
    D := C;
    C := (B shl 30) or (B shr 2);
    B := A;
    A := T;
  end;

  Inc(A, SA); Inc(B, SB); Inc(C, SC); Inc(D, SD); Inc(E, SE);
end;

function HashSHA1(const S: string): TMessageDigest160;
begin
  Result := HashSHA1(@S[1], Length(S));
end;

function HashSHA1(Buffer: pointer; Size: cardinal): TMessageDigest160; overload;
var
  Addon: array[0..127] of byte;
  Chunk: array[0..79] of longword;
  T, I, J, ToAdd: cardinal;
  Temp: longword;
  Count: int64;
  A, B, C, D, E: longword;
  SrcP : PByteArray;
begin
  FillChar(Addon, SizeOf(Addon), 0);
  // initializing
  A := {$ifdef ActiveX_registered}Constant1{$else}$67452301{$endif};
  B := $EFCDAB89;
  C := $98BADCFE;
  D := $10325476;
  E := {$ifdef ActiveX_registered}Constant2{$else}$C3D2E1F0{$endif};
  Count := Size * 8;
  // processing
  T := Size mod 64;
  if 56 - integer(T) <= 0 then
    ToAdd := 120 - T
  else
    ToAdd := 56 - T;
  Addon[T] := $80;
  Move(Pointer(PtrUInt(Buffer) + Size - T)^, Addon[0], T);

  Temp := Count shr 32;
  SrcP := @Temp;
  for j := 0 to 3 do
    Addon[ToAdd + T + j] := SrcP[3 - j];

  Temp := LongWord(Count);
  for j := 0 to 3 do
    Addon[ToAdd + T + 4 + j] := SrcP[3 - j];

  I := 0;
  repeat
    // transforming
    if I + 64 <= Size then
      Move(Pointer(PtrUInt(Buffer) + I)^, Chunk[0], 64)
    else
      if I <= Size then
        Move(Addon[0], Chunk[0], 64)
      else
        Move(Addon[64], Chunk[0], 64);

    // changing byte-order
    SrcP := @Temp;

    for J := 0 to 15 do
    begin
      Temp := Chunk[J];
      Chunk[J] := LongWord(SrcP[0] shl 24) or LongWord(SrcP[1] shl 16) or
                  LongWord(SrcP[2] shl 8) or LongWord(SrcP[3]);
    end;
    for J := 16 to 79 do
    begin
      Temp := Chunk[J - 3] xor Chunk[J - 8] xor Chunk[J - 14] xor Chunk[J - 16];
      Chunk[J] := (Temp shl 1) or (Temp shr 31);
    end;
    InternalSHA1(@Chunk, A, B, C, D, E);
    Inc(I, 64);
  until I = Size + ToAdd + 8;

  // finalizing
  SrcP := @A;
  Result.A := LongWord(SrcP[0] shl 24) or LongWord(SrcP[1] shl 16) or
              LongWord(SrcP[2] shl 8) or LongWord(SrcP[3]);
  SrcP := @B;
  Result.B := LongWord(SrcP[0] shl 24) or LongWord(SrcP[1] shl 16) or
              LongWord(SrcP[2] shl 8) or LongWord(SrcP[3]);
  SrcP := @C;
  Result.C := LongWord(SrcP[0] shl 24) or LongWord(SrcP[1] shl 16) or
              LongWord(SrcP[2] shl 8) or LongWord(SrcP[3]);
  SrcP := @D;
  Result.D := LongWord(SrcP[0] shl 24) or LongWord(SrcP[1] shl 16) or
              LongWord(SrcP[2] shl 8) or LongWord(SrcP[3]);
  SrcP := @E;
  Result.E := LongWord(SrcP[0] shl 24) or LongWord(SrcP[1] shl 16) or
              LongWord(SrcP[2] shl 8) or LongWord(SrcP[3]);
end;


procedure BlockSHA1(Buf: Pointer; var Context: TSHA1Context);
var
  SrcP: PByteArray;
  J: byte;
  Temp: LongWord;
  Chunk: array[0..79] of longword;
begin
  Move(Buf^, Chunk[0], 64);
  SrcP := @Temp;
  for J := 0 to 15 do
  begin
    Temp := Chunk[J];
    Chunk[J] := LongWord(SrcP[0] shl 24) or LongWord(SrcP[1] shl 16) or
                LongWord(SrcP[2] shl 8) or LongWord(SrcP[3]);
  end;
  for J := 16 to 79 do
  begin
    Temp := Chunk[J - 3] xor Chunk[J - 8] xor Chunk[J - 14] xor Chunk[J - 16];
    Chunk[J] := (Temp shl 1) or (Temp shr 31);
  end;
  InternalSHA1(@Chunk, Context.A, Context.B, Context.C, Context.D, Context.E);
end;

procedure InitializeSHA1(var Context: TSHA1Context);
begin
  Context.Size := 0;
  FillChar(Context.Buffer, SizeOf(Context.Buffer), 0);
  Context.BufSize := 0;
  Context.A := {$ifdef ActiveX_registered}Constant1{$else}$67452301{$endif};
  Context.B := $EFCDAB89;
  Context.C := $98BADCFE;
  Context.D := $10325476;
  Context.E := {$ifdef ActiveX_registered}Constant2{$else}$C3D2E1F0{$endif};
end;

procedure HashSHA1(var Context: TSHA1Context; Chunk: pointer; Size: cardinal); overload;
var
  Left, I: cardinal;
begin
  if Size = 0 then
    exit;

  Inc(Context.Size, Size);
  if Context.BufSize > 0 then
  begin
    Left := 64 - Context.BufSize;
    if Left > Size then
    begin
      Move(Chunk^, Context.Buffer[Context.BufSize], Size);
      Inc(Context.BufSize, Size);
      exit;
    end
    else
    begin
      Move(Chunk^, Context.Buffer[Context.BufSize], Left);
      Inc(PtrUInt(Chunk), Left);
      Dec(Size, Left);
      BlockSHA1(Pointer(@Context.Buffer), Context);
      Context.BufSize := 0;
    end;
  end;
  I := 0;
  while Size >= 64 do
  begin
    BlockSHA1(Pointer(PtrUInt(Chunk) + I), Context);
    Inc(I, 64);
    Dec(Size, 64);
  end;
  if Size > 0 then
  begin
    Move(Pointer(PtrUInt(Chunk) + I)^, Context.Buffer[0], Size);
    Context.BufSize := Size;
  end;
end;


function FinalizeSHA1(var Context: TSHA1Context): TMessageDigest160;
var
  j: byte;
  SrcP: PByteArray;
  Tail: array[0..127] of byte;
  ToAdd, ToDo: cardinal;
  Count: int64;
  Temp: LongWord;
begin
  FillChar(Tail[0], SizeOf(Tail), 0);
  Count := Context.Size * 8;

  if 56 - Integer(Context.BufSize) <= 0 then
    ToAdd := 120 - Context.BufSize
  else
    ToAdd := 56 - Context.BufSize;

  if Context.BufSize > 0 then
  begin
    Move(Context.Buffer[0], Tail[0], Context.BufSize);
  end;
  Temp := Count shr 32;
  SrcP := @Temp;
  for j := 0 to 3 do
    Tail[ToAdd + Context.BufSize + j] := SrcP[3 - j];

  Temp := LongWord(Count);
  for j := 0 to 3 do
    Tail[ToAdd + Context.BufSize + 4 + j] := SrcP[3 - j];

  Tail[Context.BufSize] := $80;

  ToDo := Context.BufSize + ToAdd + 8;

  BlockSHA1(@Tail[0], Context);
  if ToDo > 64 then
    BlockSHA1(@Tail[64], Context);

  // finalizing
  SrcP := @Context.A;
  Result.A := LongWord(SrcP[0] shl 24) or LongWord(SrcP[1] shl 16) or
              LongWord(SrcP[2] shl 8) or LongWord(SrcP[3]);
  SrcP := @Context.B;
  Result.B := LongWord(SrcP[0] shl 24) or LongWord(SrcP[1] shl 16) or
              LongWord(SrcP[2] shl 8) or LongWord(SrcP[3]);
  SrcP := @Context.C;
  Result.C := LongWord(SrcP[0] shl 24) or LongWord(SrcP[1] shl 16) or
              LongWord(SrcP[2] shl 8) or LongWord(SrcP[3]);
  SrcP := @Context.D;
  Result.D := LongWord(SrcP[0] shl 24) or LongWord(SrcP[1] shl 16) or
              LongWord(SrcP[2] shl 8) or LongWord(SrcP[3]);
  SrcP := @Context.E;
  Result.E := LongWord(SrcP[0] shl 24) or LongWord(SrcP[1] shl 16) or
              LongWord(SrcP[2] shl 8) or LongWord(SrcP[3]);
end;

end.
