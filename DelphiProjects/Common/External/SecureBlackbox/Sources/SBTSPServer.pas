(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBTSPServer;

interface

uses

  Classes,
  SysUtils,
  SBUtils,
  SBASN1,
  SBASN1Tree,
//  SBRDN,
//  SBX509,
  SBX509Ext,
  SBPKCS7,
  SBMessages,
//  SBSHA,
  SBPKICommon,
  SBTSPCommon,
  SBConstants,
  SBCustomCertStorage;

type

  TElServerTSPInfo = class(TElTSPInfo)
  protected
    FHashAlgorithm: integer;
  published
    property Nonce: BufferType read FNonce; 
    property HashAlgorithm: integer read FHashAlgorithm;
    property Time: TDateTime read FTime write FTime; 
    property AccuracySec: integer read FAccuracySec write FAccuracySec; 
    property AccuracyMilli: integer read FAccuracyMilli write FAccuracyMilli; 
    property AccuracyMicro: integer read FAccuracyMicro write FAccuracyMicro; 
    property TSAName: TElGeneralName read FTSAName; 
  end;

  TElCustomTSPServer = class(TElTSPClass)
  private
  protected
    FSigner: TElMessageSigner;
    FCertReq: boolean;
    FMessageImprint: BufferType;
    FReqPolicy,
    FDefaultPolicy: BufferType;
    FIncludeNonce: boolean;
    FPKIFailureInfo: integer;
    FTSPInfo: TElServerTSPInfo;
    FCertificates: TElMemoryCertStorage;
    function CreateTSTInfo: BufferType;
    function ProcessRequest(const Request: ByteArray): integer;
    function CreateReply(ServerResult: TSBPKIStatus; FailureInfo: integer; var Reply: ByteArray): boolean;
    procedure SetCertificates(Value: TElMemoryCertStorage);
    procedure Notification(AComponent : TComponent; AOperation : TOperation); override;
  public
    constructor Create(Owner: TSBComponentBase); override;


    destructor Destroy; override;

    property TSPInfo: TElServerTSPInfo read FTSPInfo;
    property DefaultPolicy : BufferType read FDefaultPolicy write FDefaultPolicy;
    
  published
    // Certificates contains certificates to sign the data.
    // Note, that the value for TElMessageSigner.IncludeCertificates is taken from
    // TSP request, namely it's certReq field.
    property Certificates: TElMemoryCertStorage read FCertificates write
      SetCertificates;
  end;

  TElFileTSPServer = class(TElCustomTSPServer)
  public
    function SaveReplyToStream(ServerResult: TSBPKIStatus;
      FailureInfo: integer; Stream: TStream): boolean;
    function LoadRequestFromStream(Stream: TStream): Integer;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('PKIBlackbox', [TElFileTSPServer]);
end;

constructor TElCustomTSPServer.Create(Owner: TSBComponentBase);
begin
  inherited Create(Owner);
  FTSPInfo := TElServerTSPInfo.Create;
  FSigner := TElMessageSigner.Create(nil);
  FDefaultPolicy := TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$30#$03#$08);
end;


destructor TElCustomTSPServer.Destroy;
begin
  FreeAndNil(FTSPInfo);
  FreeAndNil(FSigner);
  inherited;
end;


procedure TElCustomTSPServer.Notification(AComponent : TComponent; AOperation : TOperation);
begin
  inherited;
  if (AComponent = FCertificates) and (AOperation = opRemove) then
    FCertificates := nil;
end;

function TElCustomTSPServer.ProcessRequest(const Request: ByteArray): integer;
var
  ASN,
  Sequence,
  Field: TElASN1ConstrainedTag;
  Param: TElASN1SimpleTag;
  sz,
  fld: integer;

begin
  result := -1;
  FCertReq := false;
  FIncludeNonce := false;
  try
    ASN := TElASN1ConstrainedTag.Create;
    try
      try
        if not ASN.LoadFromBuffer(@Request[0], Length(Request)) then
          exit;
      except
        exit;
      end;
      if ASN.Count = 0 then
        exit;
      // TimeStampReq ::= SEQUENCE  {
      Field := TElASN1ConstrainedTag(ASN.GetField(0));
      if (Field = nil) or not Field.IsConstrained then
        exit;
      if Field.Count < 2 then
        exit;
      //   version                      INTEGER  { v1(1) },
      Param := TElASN1SimpleTag(Field.GetField(0));
      if (Param = nil) or Param.IsConstrained or (Param.TagId <> SB_ASN1_INTEGER) then
        exit;
      if ASN1ReadInteger(Param) <> 1 {default v1(1)} then
        exit;
      //   messageImprint               MessageImprint,
      Sequence := TElASN1ConstrainedTag(Field.GetField(1));
      if (Sequence = nil) or not Sequence.IsConstrained then
        exit;
      sz := 0;
      Sequence.SaveToBuffer(nil, sz);
      SetLength(FMessageImprint, sz);
      Sequence.SaveToBuffer(@FMessageImprint[1], sz);
      //     hashAlgorithm                AlgorithmIdentifier,
      Sequence := TElASN1ConstrainedTag(Sequence.GetField(0));
      if (Sequence = nil) or not Sequence.IsConstrained then
        exit;
      Param := TElASN1SimpleTag(Sequence.GetField(0));
      if (Param = nil) or Param.IsConstrained or (Param.TagId <> SB_ASN1_OBJECT) then
        exit;

      FTSPInfo.FHashAlgorithm := GetHashAlgorithmByOID(Param.Content);
      if FTSPInfo.FHashAlgorithm = SB_CERT_ALGORITHM_UNKNOWN then
        exit;

      // -- optionals
      fld := 2;
      //   reqPolicy             TSAPolicyId              OPTIONAL,
      //     TSAPolicyId ::= OBJECT IDENTIFIER
      SetLength(FReqPolicy, 0);
      Param := TElASN1SimpleTag(Field.GetField(fld));
      if (Param <> nil) and not Param.IsConstrained and (Param.TagId = SB_ASN1_OBJECT) then
      begin
        sz := 0;
        FReqPolicy := WritePrimitive(SB_ASN1_OBJECT, Param.Content);
        inc(fld);
        Param := TElASN1SimpleTag(Field.GetField(fld));
      end;

      //   nonce                 INTEGER                  OPTIONAL,
      if (Param <> nil) and not Param.IsConstrained and (Param.TagId = SB_ASN1_INTEGER) then
      begin
        TElServerTSPInfo(FTSPInfo).FNonce := Param.Content;
        FIncludeNonce := true;
        inc(fld);
        Param := TElASN1SimpleTag(Field.GetField(fld));
      end;

      //   certReq               BOOLEAN                  DEFAULT FALSE,
      FCertReq := false;
      if (Param <> nil) and not Param.IsConstrained and (Param.TagId = SB_ASN1_BOOLEAN) then
        FCertReq := ASN1ReadInteger(Param) > 0;

      // }

    finally
      ASN.Free;
    end;

  except
    exit;
  end;
  result := 0;
end;

function TElCustomTSPServer.CreateTSTInfo: BufferType;
var
  s, sn: BufferType;
  sz: integer;
  l: TStringList;
  Tag: TElASN1SimpleTag;
begin
  if FTSPInfo.AccuracySet then
  begin
    //  Accuracy ::= SEQUENCE {
    l := TStringList.Create;
    //     seconds        INTEGER              OPTIONAL,
    l.Add(WriteInteger(TElServerTSPInfo(FTSPInfo).FAccuracySec));
    //     millis     [0] INTEGER  (1..999)    OPTIONAL,
    l.Add(WriteInteger(TElServerTSPInfo(FTSPInfo).FAccuracyMilli, $80));
    //l.Add(WritePrimitive($A0, WriteInteger(TElServerTSPInfo(FTSPInfo).FAccuracyMilli)));
    //     micros     [1] INTEGER  (1..999)    OPTIONAL
    l.Add(WriteInteger(TElServerTSPInfo(FTSPInfo).FAccuracyMicro, $81));
    //l.Add(WritePrimitive($A1, WriteInteger(TElServerTSPInfo(FTSPInfo).FAccuracyMicro)));
    //  }
    s := WriteSequence(l);
    l.Free;
  end;

  //  TSTInfo ::= SEQUENCE  {
  l := TStringList.Create;
  //     version                      INTEGER  { v1(1) },
  l.Add(WriteInteger(1));
  //     policy                       TSAPolicyId,
  if Length(FReqPolicy) > 0 then
    l.Add(FReqPolicy)
  else
    l.Add(WritePrimitive(SB_ASN1_OBJECT, FDefaultPolicy));
  //     messageImprint               MessageImprint,
  l.Add(FMessageImprint);
  //     serialNumber                 INTEGER,

  SetLength(sn, Length(TElServerTSPInfo(FTSPInfo).FSerialNumber));
  Move(TElServerTSPInfo(FTSPInfo).FSerialNumber[0], sn[1], Length(sn));
  l.Add(WriteInteger(sn));
  //     genTime                      GeneralizedTime,
  l.Add(WriteGeneralizedTime(TElServerTSPInfo(FTSPInfo).Time));

  //     accuracy                     Accuracy                 OPTIONAL,
  if FTSPInfo.AccuracySet then
    l.Add(s);

  //     nonce                        INTEGER                  OPTIONAL,
  if FIncludeNonce then
    l.Add(WritePrimitive($02, TElServerTSPInfo(FTSPInfo).FNonce));
  //     tsa                          [0] GeneralName          OPTIONAL,
  if Assigned(TElServerTSPInfo(FTSPInfo).TSAName) and FTSPInfo.TSANameSet then
  begin
    Tag := TElASN1SimpleTag.Create;
    TElServerTSPInfo(FTSPInfo).TSAName.SaveToTag(Tag);
    sz := 0;
    Tag.SaveToBuffer(nil, sz);
    SetLength(s, sz);
    Tag.SaveToBuffer(@s[1], sz);
    s := WritePrimitive($a0, s);
    l.Add(s);
  end;
  //  }
  result := WriteSequence(l);
  l.Free;
end;

function TElCustomTSPServer.CreateReply(ServerResult: TSBPKIStatus; FailureInfo: integer; var Reply: ByteArray): boolean;
var
  token, s: BufferType;
  l: TStringList;
  sz: integer;
  SigTime : BufferType;
begin
  result := false;
  //  id_ct_TSTInfo = #$2b#$06#$2a#$86#$48#$86#$f7#$0d#$01#$09#$10#$01#$04;
  //   TimeStampToken ::= ContentInfo
  //     -- contentType is id-signedData ([CMS])
  //     -- content is SignedData ([CMS])


  FSigner.CertStorage := Certificates;
  FSigner.IncludeCertificates := FCertReq;
  FSigner.HashAlgorithm := TElServerTSPInfo(FTSPInfo).FHashAlgorithm;
  FSigner.UseUndefSize := false;
  FSigner.ContentType := SB_OID_TSTINFO;
  FSigner.AuthenticatedAttributes.Count := 2;
  with FSigner.AuthenticatedAttributes do
  begin
    Attributes[0] := TBufferTypeConst(SB_OID_CONTENT_TYPE){$ifdef DELPHI)_NET}.Data{$endif};
    Values[0].Add(Chr(SB_ASN1_OBJECT) +
      Chr(Length(SB_OID_TSTINFO)) + SB_OID_TSTINFO);
    Attributes[1] := SB_OID_SIGNING_TIME;
    if (FTSPInfo.Time >=
      EncodeDate(2050, 1, 1))
      or
      (FTSPInfo.Time <
      EncodeDate(1950, 1, 1))
    then
    begin
      SigTime := DateTimeToGeneralizedTime(FTSPInfo.Time);
      SigTime := FormatAttributeValue(SB_ASN1_GENERALIZEDTIME, SigTime);
    end
    else
    begin
      SigTime := DateTimeToUTCTime(FTSPInfo.Time);
      SigTime := FormatAttributeValue(SB_ASN1_UTCTIME, SigTime);
    end;
    Values[1].Add(SigTime);
  end;

  s :=  CreateTSTInfo();
  SetLength(token, 0);
  sz := 0;
  if FSigner.Sign(@s[1], Length(s), nil, sz)
  = SB_MESSAGE_ERROR_BUFFER_TOO_SMALL then
  begin
    SetLength(token, sz);
    if FSigner.Sign(@s[1], Length(s), @token[1], sz)
    <> 0 then
    begin
      SetLength(Reply, 0);
      exit;
    end
    else
      SetLength(token, sz);
  end
  else
  begin
    SetLength(Reply, 0);
    exit;
  end;

  //   PKIStatusInfo ::= SEQUENCE {

  l := TStringList.Create;

  //      status        PKIStatus,

  l.Add(WriteInteger(integer(ServerResult)));

  //      failInfo      PKIFailureInfo  OPTIONAL


  SetLength(s, sizeof(FailureInfo));

  Move(FailureInfo, s[1], sizeof(FailureInfo));

  l.Add(WriteBitString(s)); 


  s := WriteSequence(l);
  l.Free;

  //   }

  //   TimeStampResp ::= SEQUENCE  {

  l := TStringList.Create;

  //      status                  PKIStatusInfo,

  l.Add(s);

  //      timeStampToken          TimeStampToken     OPTIONAL

  l.Add(token);

  s := WriteSequence(l);
  l.Free;
  //   }

  Reply := BytesOfString(s);

  result := true;

end;

procedure TElCustomTSPServer.SetCertificates(Value: TElMemoryCertStorage);
begin
  if (FCertificates <> Value) then
  begin
    FCertificates := Value;
    if FCertificates <> nil then
      FCertificates.FreeNotification(Self);
  end;
end;

function TElFileTSPServer.LoadRequestFromStream(Stream: TStream): Integer;
var
  Request: ByteArray;
begin
  SetLength(Request, Stream.Size - Stream.Position);
  Stream.Read(Request[0], Length(Request));
  result := ProcessRequest(Request);
end;

function TElFileTSPServer.SaveReplyToStream(ServerResult: TSBPKIStatus;
  FailureInfo: integer; Stream: TStream): boolean;
var
  Reply: ByteArray;
begin
  result := CreateReply(ServerResult, FailureInfo, Reply);
  if result then
    Stream.Write(Reply[0], Length(Reply));
end;

end.
