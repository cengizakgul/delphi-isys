(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBStreamCipher;

interface

uses
  SBConstants, SBUtils,
  SysUtils,
  
  {$ifndef SBB_NO_DES}
  SBDES,
  SB3DES,
  {$endif}
  
  SBRC2;

type
  TSBStreamCipherMode = 
    (scmECB, scmCBC);
    
  TSBStreamCipherDirection = 
    (scdEncrypt, scdDecrypt);
    
  TElStreamCipher = class
  private
    FIndex: integer;
    FMode : TSBStreamCipherMode;
    FAlgorithm: integer;
    FDirection : TSBStreamCipherDirection;
    FNotProcessed : ByteArray;
    FProcessed : ByteArray;
    FBytesWritten : integer;
    FFinished : boolean;
    FCheckPadding : boolean;
    {$ifndef SBB_NO_DES}
    FCtxDES : TDESContext;
    FCtx3DES : T3DESContext;
    {$endif}
    FCtxRC2 : TRC2Context;
    function FindIndexByOID(const OID : BufferType): integer;
    procedure InitializeCipher(const Key: BufferType; const IV : BufferType);
    procedure FinalizeCipher;
    function GetBytesAvailable: integer;
    procedure Process(InBuffer: pointer; InSize: integer);
  public
    constructor Create(Alg: integer; const Key : BufferType;
      const IV : BufferType; Direction : TSBStreamCipherDirection;
      Mode : TSBStreamCipherMode = scmCBC); overload;
    constructor Create(const OID : BufferType; const Key : BufferType;
      const IV : BufferType; Direction : TSBStreamCipherDirection); overload;
    destructor Destroy; override;
    
    procedure Write(Buffer: pointer; Size: integer);
    function Read(Buffer: pointer; Size: integer): integer;
    procedure Finish;
    class function IsAlgorithmSupported(Alg : integer; Mode: TSBStreamCipherMode) : boolean; overload;
    class function IsAlgorithmSupported(const OID : BufferType) : boolean; overload;
    class function GetAlgorithmParameters(const OID : BufferType;
      const Params: BufferType; var KeySize : integer; var IV : BufferType) : boolean;
    class function SaveAlgorithmParameters(const OID : BufferType;
      KeySize: integer; const IV : BufferType; var Params: BufferType): boolean;
    class function GetAlgorithmByOID(const OID : BufferType): integer;
    property BytesWritten : integer read FBytesWritten;
    property BytesAvailable : integer read GetBytesAvailable;
    property CheckPadding : boolean read FCheckPadding write FCheckPadding default true;
  end;

  EElStreamCipherError =  class(Exception);
  EElStreamCipherUnsupportedError =  class(EElStreamCipherError);
  EElStreamCipherInvalidParameterError =  class(EElStreamCipherError);
  EElStreamCipherInternalError =  class(EElStreamCipherError);
  EElStreamCipherInvalidPaddingError =  class(EElStreamCipherError);

implementation

uses
  SBASN1Tree;

resourcestring
  SUnsupportedAlgorithm = 'Unsupported algorithm: %s';
  SUnsupportedAlgorithmOrMode = 'Unsupported algorithm or mode';
  SInvalidKeyLength = 'Invalid key length';
  SInvalidIVLength = 'Invalid IV length';
  SInvalidBlockSize = 'Invalid block size';
  SFinalBlockTooSmall = 'Final block too small';
  SInvalidPadding = 'Invalid padding';

const
  SB_STREAM_CIPHER_COUNT = 3;

  SB_STREAM_CIPHER_OIDs : array {$ifndef NET_CF_1_0}[0..SB_STREAM_CIPHER_COUNT - 1]{$endif} of TBufferTypeConst = 
   (
    TBufferTypeConst(#$2b#$0e#$03#$02#$07),                       // des-cbc
    TBufferTypeConst(#$2A#$86#$48#$86#$F7#$0D#$03#$07),           // des-ede3-cbc
    TBufferTypeConst(#$2A#$86#$48#$86#$F7#$0D#$03#$02)            // rc2-cbc
   );

  SB_STREAM_CIPHER_ALGORITHMS : array[0..SB_STREAM_CIPHER_COUNT - 1] of integer = 
   (
    SB_ALGORITHM_CNT_DES,
    SB_ALGORITHM_CNT_3DES,
    SB_ALGORITHM_CNT_RC2
   );

  SB_STREAM_CIPHER_MODES : array[0..SB_STREAM_CIPHER_COUNT - 1] of TSBStreamCipherMode = 
   (
    scmCBC,
    scmCBC,
    scmCBC
   );

  SB_STREAM_CIPHER_BLOCK_SIZES : array[0..SB_STREAM_CIPHER_COUNT - 1] of integer = 
   (
    8,
    8,
    8
   );

  SB_STREAM_CIPHER_KEY_SIZES : array[0..SB_STREAM_CIPHER_COUNT - 1] of integer = 
   (
    8,
    24,
    -1
   );

constructor TElStreamCipher.Create(Alg: integer; const Key : BufferType;
  const IV : BufferType; Direction: TSBStreamCipherDirection;
  Mode : TSBStreamCipherMode = scmCBC);
var
  I : integer;
begin
  inherited Create;
  FCheckPadding := true;
  FIndex := -1;
  for I := 0 to SB_STREAM_CIPHER_COUNT - 1 do
    if (SB_STREAM_CIPHER_ALGORITHMS[I] = Alg) and (SB_STREAM_CIPHER_MODES[I] = Mode) then
    begin
      FIndex := I;
      Break;
    end;
  if FIndex = -1 then
    raise EElStreamCipherUnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(Alg)]);
  FMode := SB_STREAM_CIPHER_MODES[FIndex];
  FAlgorithm := SB_STREAM_CIPHER_ALGORITHMS[FIndex];
  FDirection := Direction;
  InitializeCipher(Key, IV);
end;

constructor TElStreamCipher.Create(const OID : BufferType; const Key : BufferType;
  const IV : BufferType; Direction : TSBStreamCipherDirection);
begin
  inherited Create;
  FCheckPadding := true;
  FIndex := FindIndexByOID(OID);
  if FIndex = -1 then
    raise EElStreamCipherUnsupportedError.CreateFmt(SUnsupportedAlgorithm, [OIDToStr(OID)]);
  FMode := SB_STREAM_CIPHER_MODES[FIndex];
  FAlgorithm := SB_STREAM_CIPHER_ALGORITHMS[FIndex];
  FDirection := Direction;
  InitializeCipher(Key, IV);
end;

destructor TElStreamCipher.Destroy;
begin
  inherited;
end;

procedure TElStreamCipher.InitializeCipher(const Key: BufferType; const IV : BufferType);
  procedure RaiseUnsupportedException;
  begin
    raise EElStreamCipherUnsupportedError.Create(SUnsupportedAlgorithmOrMode);
  end;

  procedure RaiseInvalidParameterException(const Msg : string);
  begin
    raise EElStreamCipherInvalidParameterError.Create(Msg);
  end;
  
var
  {$ifndef SBB_NO_DES}
  DESKey : TDESKey;
  DESIV : TDESBuffer;
  DESEDEKey : T3DESKey;
  DESEDEIV : T3DESBuffer;
  {$endif}
  RC2Key : TRC2Key;
  RC2IV : TRC2Buffer;
begin
  SetLength(FNotProcessed, 0);
  SetLength(FProcessed, 0);
  FBytesWritten := 0;
  FFinished := false;
  case SB_STREAM_CIPHER_ALGORITHMS[FIndex] of
    {$ifndef SBB_NO_DES}
    SB_ALGORITHM_CNT_DES:
    begin
      if Length(Key) <> 8 then
        RaiseInvalidParameterException(SInvalidKeyLength);
      Move(Key[1], DESKey[0], SizeOf(DESKey));
      if (FMode = scmECB) then
      begin
        if FDirection = scdEncrypt then
          SBDES.InitializeEncryptionECB(FCtxDES, DESKey)
        else
          SBDES.InitializeDecryptionECB(FCtxDES, DESKey);
      end
      else if (FMode = scmCBC) then
      begin
        if Length(IV) <> 8 then
          RaiseInvalidParameterException(SInvalidIVLength);
        Move(IV[1], DESIV[0], SizeOf(DESIV));
        if FDirection = scdEncrypt then
          SBDES.InitializeEncryptionCBC(FCtxDES, DESKey, DESIV)
        else
          SBDES.InitializeDecryptionCBC(FCtxDES, DESKey, DESIV);
      end
    end;
    SB_ALGORITHM_CNT_3DES:
    begin
      if Length(Key) <> 24 then
        RaiseInvalidParameterException(SInvalidKeyLength);
      Move(Key[1], DESEDEKey[0], SizeOf(DESEDEKey));
      if (FMode = scmECB) then
      begin
        RaiseUnsupportedException;
      end
      else if (FMode = scmCBC) then
      begin
        if Length(IV) <> 8 then
          RaiseInvalidParameterException(SInvalidIVLength);
        Move(IV[1], DESEDEIV[0], SizeOf(DESEDEIV));
        if FDirection = scdEncrypt then
          SB3DES.InitializeEncryptionCBC(FCtx3DES, DESEDEKey, DESEDEIV)
        else
          SB3DES.InitializeDecryptionCBC(FCtx3DES, DESEDEKey, DESEDEIV);
      end
    end;
    {$endif}
    SB_ALGORITHM_CNT_RC2:
    begin
      if (Length(Key) < 1) or (Length(Key) > 128) then
        RaiseInvalidParameterException(SInvalidKeyLength);
      SetLength(RC2Key, Length(Key));
      Move(Key[1], RC2Key[0], Length(RC2Key));
      if (FMode = scmECB) then
      begin
        RaiseUnsupportedException;
      end
      else if (FMode = scmCBC) then
      begin
        if Length(IV) <> 8 then
          RaiseInvalidParameterException(SInvalidIVLength);
        Move(IV[1], RC2IV[0], SizeOf(RC2IV));
        if FDirection = scdEncrypt then
          SBRC2.InitializeEncryptionCBC(FCtxRC2, RC2Key, RC2IV)
        else
          SBRC2.InitializeDecryptionCBC(FCtxRC2, RC2Key, RC2IV);
      end
    end;
    else
      RaiseUnsupportedException;
  end;
end;

procedure TElStreamCipher.FinalizeCipher;
var
  I, K : integer;
  PadLen : integer;
begin
  if FDirection = scdDecrypt then
  begin
    if Length(FProcessed) < SB_STREAM_CIPHER_BLOCK_SIZES[FIndex] then
      raise EElStreamCipherInternalError.Create(SFinalBlockTooSmall);
    if FCheckPadding then
    begin
      // removing padding
      // expected padding: RFC1423 (0x01, 0x02 0x02, 0x03 0x03 0x03 and so on).
      I := Length(FProcessed) - 1;
      PadLen := FProcessed[I];
      if (PadLen < 1) or (PadLen > 8) then
        raise EElStreamCipherInvalidPaddingError.Create(SInvalidPadding);
      K := PadLen;
      while (K > 0) do
      begin
        If FProcessed[I] <> PadLen then
          raise EElStreamCipherInvalidPaddingError.Create(SInvalidPadding);
        Dec(K);
      end;
      SetLength(FProcessed, Length(FProcessed) - PadLen);
    end;
  end
  else
  begin
    PadLen := 8 - Length(FNotProcessed);
    SetLength(FNotProcessed, 8);
    for I := 0 to PadLen - 1 do
      FNotProcessed[Length(FNotProcessed) - 1 - I] := PadLen;
    Process(@FNotProcessed[0], 8);
    SetLength(FNotProcessed, 0);
  end;
  FFinished := true;
end;

function TElStreamCipher.FindIndexByOID(const OID : BufferType): integer;
var
  I : integer;
begin
  Result := -1;
  for I := 0 to SB_STREAM_CIPHER_COUNT - 1 do
  begin
    if CompareStr(SB_STREAM_CIPHER_OIDS[I], OID) = 0 then
    begin
      Result := I;
      Break;
    end;
  end;
end;

procedure TElStreamCipher.Process(InBuffer: pointer; InSize: integer);
var
  OldSize, BlockSize : integer;
  OutSize : cardinal;
begin
  BlockSize := SB_STREAM_CIPHER_BLOCK_SIZES[FIndex];
  if InSize mod BlockSize <> 0 then
    raise EElStreamCipherInternalError.Create(SInvalidBlockSize);

  OutSize := InSize;
  OldSize := Length(FProcessed);
  SetLength(FProcessed, OldSize + InSize);
  case SB_STREAM_CIPHER_ALGORITHMS[FIndex] of
    {$ifndef SBB_NO_DES}
    SB_ALGORITHM_CNT_DES:
    begin
      if FMode = scmECB then
      begin
        if FDirection = scdEncrypt then
          SBDES.EncryptECB(FCtxDES, InBuffer, InSize, @FProcessed[OldSize], OutSize)
        else
          SBDES.DecryptECB(FCtxDES, InBuffer, InSize, @FProcessed[OldSize], OutSize)
      end
      else
      if FMode = scmCBC then
      begin
        if FDirection = scdEncrypt then
          SBDES.EncryptCBC(FCtxDES, InBuffer, InSize, @FProcessed[OldSize], OutSize)
        else
          SBDES.DecryptCBC(FCtxDES, InBuffer, InSize, @FProcessed[OldSize], OutSize)
      end;
    end;
    SB_ALGORITHM_CNT_3DES:
    begin
      if FMode = scmECB then
      begin
        raise EElStreamCipherUnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(SB_ALGORITHM_CNT_3DES)]);
      end
      else if FMode = scmCBC then
      begin
        if FDirection = scdEncrypt then
          SB3DES.EncryptCBC(FCtx3DES, InBuffer, InSize, @FProcessed[OldSize], OutSize)
        else
          SB3DES.DecryptCBC(FCtx3DES, InBuffer, InSize, @FProcessed[OldSize], OutSize)
      end;
    end;
    {$endif}
    SB_ALGORITHM_CNT_RC2:
    begin
      if FMode = scmECB then
      begin
        raise EElStreamCipherUnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(SB_ALGORITHM_CNT_RC2)]);
      end
      else if FMode = scmCBC then
      begin
        if FDirection = scdEncrypt then
          SBRC2.EncryptCBC(FCtxRC2, InBuffer, InSize, @FProcessed[OldSize], OutSize)
        else
          SBRC2.DecryptCBC(FCtxRC2, InBuffer, InSize, @FProcessed[OldSize], OutSize)
      end;
    end;
  else
    raise EElStreamCipherUnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(SB_STREAM_CIPHER_ALGORITHMS[FIndex])]);
  end;
end;

procedure TElStreamCipher.Write(Buffer: pointer; Size: integer);
  procedure RaiseUnsupportedException;
  begin
    raise EElStreamCipherUnsupportedError.Create(SUnsupportedAlgorithmOrMode);
  end;
var
  Ptr : ^byte;
  BlockSize : integer;
  OldSize : integer;
  ChunkSize : integer;
begin
  Inc(FBytesWritten, Size);
  BlockSize := SB_STREAM_CIPHER_BLOCK_SIZES[FIndex];
  Ptr := Buffer;
  if Length(FNotProcessed) > 0 then
  begin
    // appending data to spool if it is not empty
    OldSize := Length(FNotProcessed);
    ChunkSize := Min(BlockSize - OldSize, Size);
    SetLength(FNotProcessed, OldSize + ChunkSize);
    Move(Ptr^, FNotProcessed[OldSize], ChunkSize);
    Inc(Ptr, ChunkSize);
    Dec(Size, ChunkSize);
    if Length(FNotProcessed) = BlockSize then
    begin
      // feeding the spool to processing routine
      Process(@FNotProcessed[0], BlockSize);
      SetLength(FNotProcessed, 0);
    end;
  end;
  // processing integer number of input blocks 
  while (Size >= BlockSize) do
  begin
    Process(Ptr, BlockSize);
    Inc(Ptr, BlockSize);
    Dec(Size, BlockSize);
  end;
  // adding extra data to spool
  if Size > 0 then
  begin
    SetLength(FNotProcessed, Size);
    Move(Ptr^, FNotProcessed[0], Size);
  end;
end;

function TElStreamCipher.Read(Buffer: pointer; Size: integer): integer;
begin
  if (FDirection = scdEncrypt) or 
     (FFinished) then
    Result := Min(Size, Length(FProcessed))
  else
    Result := Max(0, Min(Size, Length(FProcessed) - SB_STREAM_CIPHER_BLOCK_SIZES[FIndex]));

  Move(FProcessed[0], Buffer^, Result);
  Move(FProcessed[Result], FProcessed[0], Length(FProcessed) - Result);
  SetLength(FProcessed, Length(FProcessed) - Result);
end;

procedure TElStreamCipher.Finish;
begin
  FinalizeCipher;
end;

function TElStreamCipher.GetBytesAvailable: integer;
begin
  if (FDirection = scdEncrypt) or 
     (FFinished) then
    Result := Length(FProcessed)
  else
    Result := Max(0, Length(FProcessed) - SB_STREAM_CIPHER_BLOCK_SIZES[FIndex]);
end;

class function TElStreamCipher.IsAlgorithmSupported(Alg : integer; Mode : TSBStreamCipherMode) : boolean;
var
  I : integer;
begin
  Result := false;
  for I := 0 to SB_STREAM_CIPHER_COUNT - 1 do
  begin
    if (SB_STREAM_CIPHER_ALGORITHMS[I] = Alg) and (SB_STREAM_CIPHER_MODES[I] = Mode) then
    begin
      Result := true;
      Break;
    end;
  end;
end;

class function TElStreamCipher.IsAlgorithmSupported(const OID : BufferType) : boolean;
var
  I : integer;
begin
  Result := false;
  for I := 0 to SB_STREAM_CIPHER_COUNT - 1 do
  begin
    if CompareStr(SB_STREAM_CIPHER_OIDS[I], OID) = 0 then
    begin
      Result := true;
      Break;
    end;
  end;
end;

class function TElStreamCipher.GetAlgorithmParameters(const OID : BufferType;
  const Params: BufferType; var KeySize : integer; var IV : BufferType) : boolean;
var
  I : integer;
  Index : integer;
  Tag : TElASN1ConstrainedTag;
begin
  Index := -1;
  Result := false;
  for I := 0 to SB_STREAM_CIPHER_COUNT - 1 do
  begin
    if CompareStr(SB_STREAM_CIPHER_OIDS[I], OID) = 0 then
    begin
      Index := I;
      Break;
    end;
  end;
  if Index < 0 then
    Exit;
  KeySize := SB_STREAM_CIPHER_KEY_SIZES[Index];
  if SB_STREAM_CIPHER_ALGORITHMS[Index] = SB_ALGORITHM_CNT_RC2 then
  begin
    if not SBRC2.ParseASN1Params(Params, IV, KeySize) then
      Exit;
    KeySize := KeySize shr 3;
  end
  else
  begin
    Tag := TElASN1ConstrainedTag.Create;
    try
      if not Tag.LoadFromBuffer(@Params[1], Length(Params)) then
        Exit;
      if (Tag.Count <> 1) or (not Tag.GetField(0).CheckType(SB_ASN1_OCTETSTRING, false)) then
        Exit;
      IV := TElASN1SimpleTag(Tag.GetField(0)).Content;
    finally
      FreeAndNil(Tag);
    end;
  end;
  Result := true;
end;

class function TElStreamCipher.SaveAlgorithmParameters(const OID : BufferType;
  KeySize: integer; const IV : BufferType; var Params: BufferType): boolean;
var
  Index : integer;
  I : integer;
begin
  Result := false;
  Index := -1;
  for I := 0 to SB_STREAM_CIPHER_COUNT - 1 do
  begin
    if CompareStr(SB_STREAM_CIPHER_OIDS[I], OID) = 0 then
    begin
      Index := I;
      Break;
    end;
  end;
  if Index < 0 then
    Exit;
  
  if SB_STREAM_CIPHER_ALGORITHMS[Index] = SB_ALGORITHM_CNT_RC2 then
    SBRC2.WriteASN1Params(IV, KeySize * 8, Params)
  else
    Params := #4 + Chr(Length(IV)) + IV;
  Result := true;
end;

class function TElStreamCipher.GetAlgorithmByOID(const OID : BufferType): integer;
var
  I : integer;
begin
  Result := SB_ALGORITHM_UNKNOWN;
  for I := 0 to SB_STREAM_CIPHER_COUNT - 1 do
    if CompareStr(OID, SB_STREAM_CIPHER_OIDS[I]) = 0 then
    begin
      Result := SB_STREAM_CIPHER_ALGORITHMS[I];
      Break;
    end;
end;


end.
