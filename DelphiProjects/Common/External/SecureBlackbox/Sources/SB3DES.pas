(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SB3DES;

interface

uses
  SysUtils,
  SBDES,
  SBUtils;

type

  T3DESBuffer =  array[0..7] of Byte;
  T3DESKey =  array[0..23] of Byte;

  {$ifndef TriDES_KEY_AS_REC}
  T3DESExpandedKey = array[0..2] of TDESExpandedKey;
  {$else}
  T3DESExpandedKey = packed record
    Key1: TDESExpandedKey;
    Key2: TDESExpandedKey;
    Key3: TDESExpandedKey;        
  end;
  {$endif}

  T3DESExpandedKeyEx = array[0..2] of TDESExpandedKeyEx;

  P3DESBuffer = ^T3DESBuffer;
  P3DESKey = ^T3DESKey;
  P3DESExpandedKey = ^T3DESExpandedKey;
  
  T3DESContext = packed record
    ExpandedKey: T3DESExpandedKey;
    ExpandedKeyEx: T3DESExpandedKeyEx;
    Vector: T3DESBuffer;
    Tail: T3DESBuffer;
    TailLen: Cardinal;
    BytesLeft : integer;
  end;

// Key expansion routines
procedure ExpandKeyForEncryption(const Key: T3DESKey;
  var ExpandedKey: T3DESExpandedKey); overload;procedure ExpandKeyForEncryption(const Key1, Key2, Key3: TDESKey;
  var ExpandedKey: T3DESExpandedKey); overload;
procedure ExpandKeyForDecryption(const Key: T3DESKey;
  var ExpandedKey: T3DESExpandedKey); overload;procedure ExpandKeyForDecryption(const Key1, Key2, Key3: TDESKey;
  var ExpandedKey: T3DESExpandedKey); overload;
// Block processing routines
procedure Encrypt(const InBuf: T3DESBuffer;
  const ExpandedKey: T3DESExpandedKey; var OutBuf: T3DESBuffer);
procedure Decrypt(const InBuf: T3DESBuffer;
  const ExpandedKey: T3DESExpandedKey; var OutBuf: T3DESBuffer);


// Memory processing routines
// CBC Mode
function EncryptCBC(InBuffer: Pointer; InSize: Cardinal;
  OutBuffer: Pointer; var OutSize: Cardinal;
  const ExpandedKey: T3DESExpandedKey;
  const InitVector: T3DESBuffer): Boolean; overload;
function DecryptCBC(InBuffer: Pointer; InSize: Cardinal;
  OutBuffer: Pointer; var OutSize: Cardinal;
  const ExpandedKey: T3DESExpandedKey;
  const InitVector: T3DESBuffer): Boolean; overload;

// Chunks processing routines
// CBC Mode
procedure InitializeEncryptionCBC(var Context: T3DESContext; 
  const Key: T3DESKey; const InitVector: T3DESBuffer);
procedure InitializeDecryptionCBC(var Context: T3DESContext; Key: T3DESKey; 
  InitVector: T3DESBuffer);
function EncryptCBC(var Context: T3DESContext; Chunk: Pointer; ChunkSize:
  Cardinal; OutBuf: Pointer; var OutSize: Cardinal): Boolean; overload;

function DecryptCBC(var Context: T3DESContext; Chunk: Pointer; ChunkSize:
  Cardinal; OutBuf: Pointer; var OutSize: Cardinal): Boolean; overload;

function FinalizeEncryptionCBC(var Context: T3DESContext; OutBuf: Pointer;
  var OutSize: Cardinal): Boolean;
function FinalizeDecryptionCBC(var Context: T3DESContext; OutBuf: Pointer;
  var OutSize: Cardinal): Boolean;

{ CFB 8-bit mode routines }
function InitializeEncryptionCFB8(var Context: T3DESContext; 
  const Key: T3DESKey; const InitVector: T3DESBuffer) : boolean;
function InitializeDecryptionCFB8(var Context: T3DESContext; const Key: T3DESKey;
  const InitVector: T3DESBuffer) : boolean;
function EncryptCFB8(var Context: T3DESContext; Chunk: Pointer; ChunkSize:
  Cardinal; OutBuf: Pointer; var OutSize: Cardinal): Boolean; overload;
function DecryptCFB8(var Context: T3DESContext; Chunk: Pointer; ChunkSize:
  Cardinal; OutBuf: Pointer; var OutSize: Cardinal): Boolean; overload;
function FinalizeEncryptionCFB8(var Context: T3DESContext; OutBuf: Pointer;
  var OutSize: Cardinal): Boolean;
function FinalizeDecryptionCFB8(var Context: T3DESContext; OutBuf: Pointer;
  var OutSize: Cardinal): Boolean;

implementation



// Key expansion routines

procedure ExpandKeyForEncryption(const Key: T3DESKey; 
  var ExpandedKey: T3DESExpandedKey);
begin
  ExpandKeyForEncryption(PDESKey(@Key[0])^, PDESKey(@Key[8])^,
    PDESKey(@Key[16])^, ExpandedKey);
end;

procedure ExpandKeyForDecryption(const Key: T3DESKey; 
  var ExpandedKey: T3DESExpandedKey);
begin
  ExpandKeyForDecryption(PDESKey(@Key[0])^, PDESKey(@Key[8])^,
    PDESKey(@Key[16])^, ExpandedKey);
end;

procedure ExpandKeyForEncryption(const Key1, Key2, Key3: TDESKey;
  var ExpandedKey: T3DESExpandedKey);
begin
  
  FillChar(ExpandedKey[0, 0], SizeOf(T3DESExpandedKey), 0);
  SBDES.ExpandKeyForEncryption(Key1, ExpandedKey{$ifndef TriDES_KEY_AS_REC}[0]{$else}.Key1{$endif});
  SBDES.ExpandKeyForDecryption(Key2, ExpandedKey{$ifndef TriDES_KEY_AS_REC}[1]{$else}.Key2{$endif});
  SBDES.ExpandKeyForEncryption(Key3, ExpandedKey{$ifndef TriDES_KEY_AS_REC}[2]{$else}.Key3{$endif});
end;

procedure ExpandKeyForDecryption(const Key1, Key2, Key3: TDESKey;
  var ExpandedKey: T3DESExpandedKey);
begin
  
  FillChar(ExpandedKey[0, 0], SizeOf(T3DESExpandedKey), 0);
  SBDES.ExpandKeyForDecryption(Key1, ExpandedKey{$ifndef TriDES_KEY_AS_REC}[0]{$else}.Key1{$endif});
  SBDES.ExpandKeyForEncryption(Key2, ExpandedKey{$ifndef TriDES_KEY_AS_REC}[1]{$else}.Key2{$endif});
  SBDES.ExpandKeyForDecryption(Key3, ExpandedKey{$ifndef TriDES_KEY_AS_REC}[2]{$else}.Key3{$endif});
end;

// Block processing routines
procedure Encrypt(const InBuf: T3DESBuffer; const ExpandedKey: T3DESExpandedKey;
  var OutBuf: T3DESBuffer);
var
  L, R: Cardinal;
  InBlock, OutBlock: PByteArray;
begin
  InBlock := @InBuf[0];
  OutBlock := @OutBuf[0];
  GetBlockBigEndian(@InBlock[0], L, R);
  IPerm(L, R);
  RawProcessBlock(L, R, ExpandedKey[0]);
  RawProcessBlock(R, L, ExpandedKey[1]);
  RawProcessBlock(L, R, ExpandedKey[2]);
  FPerm(L, R);
  PutBlockBigEndian(@OutBlock[0], R, L);
end;

procedure Decrypt(const InBuf: T3DESBuffer; const ExpandedKey: T3DESExpandedKey;
  var OutBuf: T3DESBuffer);
var
  L, R: Cardinal;
  InBlock, OutBlock: PByteArray;
begin
  InBlock := @InBuf[0];
  OutBlock := @OutBuf[0];
  GetBlockBigEndian(@InBlock[0], L, R);
  IPerm(L, R);
  RawProcessBlock(L, R, ExpandedKey[2]);
  RawProcessBlock(R, L, ExpandedKey[1]);
  RawProcessBlock(L, R, ExpandedKey[0]);
  FPerm(L, R);
  PutBlockBigEndian(@OutBlock[0], R, L);
end;

// Memory processing routines


// CBC Mode
 function EncryptCBC(InBuffer: Pointer; InSize: Cardinal;
  OutBuffer: Pointer; var OutSize: Cardinal;
  const ExpandedKey: T3DESExpandedKey; const InitVector: T3DESBuffer): Boolean;
var
  TempIn, TempOut: P3DESBuffer;
  Vector, Temp: T3DESBuffer;
  S: Cardinal;
begin
  S := (InSize div SizeOf(T3DESBuffer)) * SizeOf(T3DESBuffer);
  if (InSize mod SizeOf(T3DESBuffer)) <> 0 then
    S := S + SizeOf(T3DESBuffer);
  if OutSize < S then
  begin
    OutSize := S;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector := InitVector;
  while InSize >= SizeOf(T3DESBuffer) do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    PLongWord(@Temp[0])^ := PLongWord(@TempIn^[0])^ xor PLongWord(@Vector[0])^;
    PLongWord(@Temp[4])^ := PLongWord(@TempIn^[4])^ xor PLongWord(@Vector[4])^;
    Encrypt(Temp, ExpandedKey, TempOut^);
    Vector := TempOut^;
    Dec(InSize, SizeOf(T3DESBuffer));
    Inc(OutSize, SizeOf(T3DESBuffer));
  end;
  if InSize > 0 then
  begin
    Move(Pointer(PtrUInt(InBuffer) + OutSize)^, Temp, InSize);
    FillChar(Temp[InSize], SizeOf(Temp) - InSize, 0);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    PLongWord(@Temp[0])^ := PLongWord(@Temp[0])^ xor PLongWord(@Vector[0])^;
    PLongWord(@Temp[4])^ := PLongWord(@Temp[4])^ xor PLongWord(@Vector[4])^;
    Encrypt(Temp, ExpandedKey, TempOut^);
    Inc(OutSize, SizeOf(T3DESBuffer));
  end;
  Result := True;
end;

function DecryptCBC(InBuffer: Pointer; InSize: Cardinal;
  OutBuffer: Pointer; var OutSize: Cardinal;
  const ExpandedKey: T3DESExpandedKey; const InitVector: T3DESBuffer): Boolean;
var
  TempIn, TempOut: P3DESBuffer;
  Vector1, Vector2: T3DESBuffer;
begin
  if (InSize mod SizeOf(T3DESBuffer)) > 0 then
    raise EEncryptionError.Create(SInvalidInputSize);
  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := False;
    Exit;
  end;
  OutSize := 0;
  Vector1 := InitVector;
  while InSize >= SizeOf(T3DESBuffer) do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Vector2 := TempIn^;
    Decrypt(TempIn^, ExpandedKey, TempOut^);
    PLongWord(@TempOut^[0])^ := PLongWord(@TempOut^[0])^ xor
      PLongWord(@Vector1[0])^;
    PLongWord(@TempOut^[4])^ := PLongWord(@TempOut^[4])^ xor
      PLongWord(@Vector1[4])^;
    Vector1 := Vector2;
    Dec(InSize, SizeOf(T3DESBuffer));
    Inc(OutSize, SizeOf(T3DESBuffer));
  end;
  Result := True;
end;

// Chunks processing routines

// CBC Mode

procedure InitializeEncryptionCBC(var Context: T3DESContext; 
  const Key: T3DESKey; const InitVector: T3DESBuffer);
begin

  ExpandKeyForEncryption(Key, Context.ExpandedKey);
  Context.Vector := InitVector;
  Context.TailLen := 0;
end;

procedure InitializeDecryptionCBC(var Context: T3DESContext; Key: T3DESKey;
  InitVector: T3DESBuffer);
begin

  ExpandKeyForDecryption(Key, Context.ExpandedKey);
  Context.Vector := InitVector;
  Context.TailLen := 0;
end;

function EncryptCBC(var Context: T3DESContext; Chunk: Pointer; ChunkSize:
  Cardinal; OutBuf: Pointer; var OutSize: Cardinal): Boolean;
var
  Left, Needed: Cardinal;
  Temp: T3DESBuffer;
begin
  if ChunkSize = 0 then
  begin
    OutSize := 0;
    Result := True;
    Exit;
  end;
  Needed := ((Context.TailLen + ChunkSize) div SizeOf(T3DESBuffer)) *
    SizeOf(T3DESBuffer);
  if Needed > OutSize then
  begin
    OutSize := Needed;
    Result := False;
    Exit;
  end;
  if Context.TailLen > 0 then
  begin
    Left := SizeOf(T3DESBuffer) - Context.TailLen;
    if Left > ChunkSize then
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], ChunkSize);
      Inc(Context.TailLen, ChunkSize);
      ChunkSize := 0;
    end
    else
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], Left);
      Dec(ChunkSize, Left);
      PLongWord(@Temp[0])^ := PLongWord(@Context.Tail[0])^ xor
        PLongWord(@Context.Vector[0])^;
      PLongWord(@Temp[4])^ := PLongWord(@Context.Tail[4])^ xor
        PLongWord(@Context.Vector[4])^;
      Encrypt(Temp, Context.ExpandedKey, P3DESBuffer(OutBuf)^);
      Context.Vector := P3DESBuffer(OutBuf)^;
      Inc(PtrUInt(OutBuf), SizeOf(T3DESBuffer));
      Inc(PtrUInt(Chunk), Left);
      Context.TailLen := 0;
    end;
  end;
  while ChunkSize >= SizeOf(T3DESBuffer) do
  begin
    PLongWord(@Temp[0])^ := PLongWord(Chunk)^ xor
      PLongWord(@Context.Vector[0])^;
    Inc(PtrUInt(Chunk), 4);
    PLongWord(@Temp[4])^ := PLongWord(Chunk)^ xor
      PLongWord(@Context.Vector[4])^;
    Inc(PtrUInt(Chunk), 4);
    Encrypt(Temp, Context.ExpandedKey, P3DESBuffer(OutBuf)^);
    Context.Vector := P3DESBuffer(OutBuf)^;
    Inc(PtrUInt(OutBuf), SizeOf(T3DESBuffer));
    Dec(ChunkSize, SizeOf(T3DESBuffer));
  end;
  if ChunkSize > 0 then
  begin
    Move(Chunk^, Context.Tail[0], ChunkSize);
    Context.TailLen := ChunkSize;
  end;
  OutSize := Needed;
  Result := True;
end;


function DecryptCBC(var Context: T3DESContext; Chunk: Pointer; ChunkSize:
  Cardinal; OutBuf: Pointer; var OutSize: Cardinal): Boolean;
var
  Left, Needed: Cardinal;
  NextVector: T3DESBuffer;
begin
  if ChunkSize = 0 then
  begin
    OutSize := 0;
    Result := True;
    Exit;
  end;
  Needed := ((Context.TailLen + ChunkSize) div SizeOf(T3DESBuffer)) *
    SizeOf(T3DESBuffer);
  if Needed > OutSize then
  begin
    OutSize := Needed;
    Result := False;
    Exit;
  end;
  if Context.TailLen > 0 then
  begin
    Left := SizeOf(T3DESBuffer) - Context.TailLen;
    if Left > ChunkSize then
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], ChunkSize);
      Inc(Context.TailLen, ChunkSize);
      ChunkSize := 0;
    end
    else
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], Left);
      Inc(PtrUInt(Chunk), Left);
      Dec(ChunkSize, Left);
      NextVector := Context.Tail;
      Decrypt(Context.Tail, Context.ExpandedKey, P3DESBuffer(OutBuf)^);
      PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
        PLongWord(@Context.Vector[0])^;
      Inc(PtrUInt(OutBuf), 4);
      PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
        PLongWord(@Context.Vector[4])^;
      Inc(PtrUInt(OutBuf), 4);
      Context.Vector := NextVector;
      Context.TailLen := 0;
    end;
  end;
  while ChunkSize >= SizeOf(T3DESBuffer) do
  begin
    NextVector := P3DESBuffer(Chunk)^;
    Decrypt(P3DESBuffer(Chunk)^, Context.ExpandedKey, P3DESBuffer(OutBuf)^);
    PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor PLongWord(@Context.Vector[0])^;
    Inc(PtrUInt(OutBuf), 4);
    PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor PLongWord(@Context.Vector[4])^;
    Inc(PtrUInt(OutBuf), 4);
    Context.Vector := NextVector;
    Inc(PtrUInt(Chunk), SizeOf(T3DESBuffer));
    Dec(ChunkSize, SizeOf(T3DESBuffer));
  end;
  if ChunkSize > 0 then
  begin
    Move(Chunk^, Context.Tail[0], ChunkSize);
    Context.TailLen := ChunkSize;
  end;
  OutSize := Needed;
  Result := True;
end;

function FinalizeEncryptionCBC(var Context: T3DESContext; OutBuf: Pointer;
  var OutSize: Cardinal): Boolean;
begin
  if Context.TailLen > 0 then
  begin
    if OutSize < SizeOf(T3DESBuffer) then
    begin
      OutSize := SizeOf(T3DESBuffer);
      Result := False;
    end
    else
    begin
      FillChar(Context.Tail[Context.TailLen], SizeOf(T3DESBuffer) -
        Context.TailLen, 0);
      PLongWord(@Context.Tail[0])^ := PLongWord(@Context.Tail[0])^ xor
        PLongWord(@Context.Vector[0])^;
      PLongWord(@Context.Tail[4])^ := PLongWord(@Context.Tail[4])^ xor
        PLongWord(@Context.Vector[4])^;
      Encrypt(Context.Tail, Context.ExpandedKey, P3DESBuffer(OutBuf)^);
      OutSize := SizeOf(T3DESBuffer);
      Result := True;
    end;
  end
  else
  begin
    OutSize := 0;
    Result := True;
  end;
end;

function FinalizeDecryptionCBC(var Context: T3DESContext; OutBuf: Pointer;
  var OutSize: Cardinal): Boolean;
begin
  if Context.TailLen > 0 then
  begin
    if Context.TailLen < SizeOf(T3DESBuffer) then
    begin
      OutSize := 0;
      Result := False;
    end
    else
      if OutSize < SizeOf(T3DESBuffer) then
      begin
        OutSize := SizeOf(T3DESBuffer);
        Result := False;
      end
      else
      begin
        Decrypt(Context.Tail, Context.ExpandedKey, P3DESBuffer(OutBuf)^);
        PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
          PLongWord(@Context.Vector[0])^;
        Inc(PtrUInt(OutBuf), 4);
        PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
          PLongWord(@Context.Vector[4])^;
        OutSize := SizeOf(T3DESBuffer);
        Result := True;
      end;
  end
  else
  begin
    OutSize := 0;
    Result := True;
  end;
end;


function InitializeEncryptionCFB8(var Context: T3DESContext; 
  const Key: T3DESKey; const InitVector: T3DESBuffer) : boolean;
begin
  InitializeEncryptionCBC(Context,Key,InitVector);
  Encrypt(InitVector, Context.ExpandedKey, Context.Vector);
  Context.BytesLeft := 8;
  Result := true;
end;

function InitializeDecryptionCFB8(var Context: T3DESContext; const Key: T3DESKey;
  const InitVector: T3DESBuffer) : boolean;
begin
  InitializeEncryptionCBC(Context,Key,InitVector);
  Encrypt(InitVector, Context.ExpandedKey, Context.Vector);
  Context.BytesLeft := 8;
  Result := true;
end;

function EncryptCFB8(var Context: T3DESContext; Chunk: Pointer; ChunkSize:
  Cardinal; OutBuf: Pointer; var OutSize: Cardinal): Boolean; overload;
var
  I : cardinal;
  J : integer;
  P1, P2 : pointer;
  Temp : T3DESBuffer;
begin
  if (OutSize < ChunkSize) then
  begin
    Result := false;
    OutSize := 0;
    Exit;
  end;
  Result := true;
  I := 0;
  while I < ChunkSize do
  begin
    P1 := pointer(PtrUInt(Chunk) + I);
    P2 := pointer(PtrUInt(OutBuf) + I);
    PByte(P2)^ := PByte(P1)^ xor PByte(@Context.Vector)^;
    for J:=0 to 6 do Context.Vector[J]:=Context.Vector[J+1];
    Context.Vector[7]:=PByte(P2)^;
    Dec(Context.BytesLeft);
    if Context.BytesLeft = 0 then
    begin
      Encrypt(Context.Vector, Context.ExpandedKey, Temp);
      Context.Vector := Temp;
      Context.BytesLeft := 8;
    end;
    Inc(I);
  end;
  OutSize := ChunkSize;
end;

function DecryptCFB8(var Context: T3DESContext; Chunk: Pointer; ChunkSize:
  Cardinal; OutBuf: Pointer; var OutSize: Cardinal): Boolean; overload;
var
  I : cardinal;
  J : integer;
  P1, P2 : pointer;
  Temp : T3DESBuffer;
begin
  if (OutSize < ChunkSize) then
  begin
    Result := false;
    OutSize := 0;
    Exit;
  end;
  Result := true;
  I := 0;
  while I < ChunkSize do
  begin
    P1 := pointer(PtrUInt(Chunk) + I);
    P2 := pointer(PtrUInt(OutBuf) + I);
    PByte(P2)^ := PByte(P1)^ xor PByte(@Context.Vector)^;
    for J:=0 to 6 do Context.Vector[J]:=Context.Vector[J+1];
    Context.Vector[7]:=PByte(P1)^;
    Dec(Context.BytesLeft);
    if Context.BytesLeft = 0 then
    begin
      Encrypt(Context.Vector, Context.ExpandedKey, Temp);
      Context.Vector := Temp;
      Context.BytesLeft := 8;
    end;
    Inc(I);
  end;
  OutSize := ChunkSize;
end;

function FinalizeEncryptionCFB8(var Context: T3DESContext; OutBuf: Pointer;
  var OutSize: Cardinal): Boolean;
begin
  Result := true;
end;

function FinalizeDecryptionCFB8(var Context: T3DESContext; OutBuf: Pointer;
  var OutSize: Cardinal): Boolean;
begin
  Result := true;
end;


end.
