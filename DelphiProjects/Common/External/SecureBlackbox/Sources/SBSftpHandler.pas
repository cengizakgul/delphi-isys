(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSftpHandler;

interface

uses
  SBUtils,
  SBSSHCommon,
  SBSSHHandlers,
  SBSftpCommon,
  SBSftpServer;

type
  TElSFTPSSHSubsystemHandler = class(TElCustomSSHSubsystemHandler)
  private
    FNewLineConvention: string;
    FRemotelyClosed: Boolean;
    FVersion: TSBSftpVersion;
    FVersions: TSBSftpVersions;
    procedure SftpServerReceive(Sender: TObject; Buffer: pointer; MaxSize:
      integer; var Written: integer);
    procedure SftpServerSend(Sender: TObject; Buffer: pointer; Size: integer);
  protected
    FServer: TElSFTPServer;
    FServerOwned: boolean;
    procedure Execute; override;
    procedure Initialize; override;
    procedure NFinalize; override;
    procedure SetServer(Value: TElSFTPServer);
  public
    {class }function HandlerType: TSBSSHSubsystemHandlerType; override;

    constructor Create(Connection: TElSSHTunnelConnection); overload; override;
    constructor CreateDelayed(Connection: TElSSHTunnelConnection; Server: TElSftpServer); reintroduce; overload;
    
    property Version: TSBSftpVersion read FVersion;
    property Versions: TSBSftpVersions read FVersions write FVersions 
      default  [sbSFTP3, sbSFTP4] ;
    property NewLineConvention: string read FNewLineConvention write FNewLineConvention;
  published
    property Server: TElSFTPServer read FServer write SetServer;
  end;

implementation

{$ifndef LINUX}
uses
  Windows;
{$endif}

resourcestring

  sServerNotAssigned = 'Server was not assigned';
  sConnectionClosed = 'Connection closed';

constructor TElSFTPSSHSubsystemHandler.Create(Connection: TElSSHTunnelConnection);
begin
  inherited Create(Connection);
  FVersions := [sbSFTP3, sbSFTP4];
end;

constructor TElSFTPSSHSubsystemHandler.CreateDelayed(Connection:
  TElSSHTunnelConnection; Server: TElSftpServer);
begin
  FServer := Server;
  inherited CreateDelayed(Connection);
  FVersions := [sbSFTP3, sbSFTP4];
end;

procedure TElSFTPSSHSubsystemHandler.Initialize;
begin
  FRemotelyClosed := false;

  // ^ Romio 2005/02/14
  if FServer = nil then
    SetServer(TElSFTPServer.Create(nil));
  // v Romio 2005/02/14

  if FServer <> nil then
  begin
    FServer.OnReceive := Self.SftpServerReceive;
    FServer.OnSend := Self.SftpServerSend;
    FServer.Open;
  end
  else
  begin
    raise EElSFTPError.Create(sServerNotAssigned);
  end;
end;

procedure TElSFTPSSHSubsystemHandler.NFinalize;
begin
  if FServer <> nil then
    FServer.Close;
end;

procedure TElSFTPSSHSubsystemHandler.Execute;
begin
  while (not Terminated) and Connected do
  begin
    WaitForInputData(100);
    if Terminated or (not Connected) then
      break;
    while ReceiveLength > 0 do
      FServer.DataAvailable;
  end;
end;

{class }function TElSFTPSSHSubsystemHandler.HandlerType:
  TSBSSHSubsystemHandlerType;
begin
  Result := shtSynchronous;
end;

procedure TElSFTPSSHSubsystemHandler.SetServer(Value: TElSFTPServer);
begin
  if FServer <> Value then
  begin
    FServer := Value;
  end;
end;

procedure TElSFTPSSHSubsystemHandler.SftpServerReceive(Sender: TObject;
  Buffer: pointer; MaxSize: integer; var Written: integer);
begin
  Written := ReceiveData(Buffer, MaxSize);
end;

procedure TElSFTPSSHSubsystemHandler.SftpServerSend(Sender: TObject; Buffer:
  pointer; Size: integer);
begin
  SendData(Buffer, Size);
end;

end.

