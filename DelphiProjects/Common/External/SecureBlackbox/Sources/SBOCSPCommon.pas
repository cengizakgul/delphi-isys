(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBOCSPCommon;

interface

uses
  SysUtils,
  SBUtils,
  SBX509Ext,
  SBASN1,
  SBASN1Tree;



const

  ERROR_FACILITY_OCSP = $13000;
  ERROR_OCSP_PROTOCOL_ERROR_FLAG = $00800;

  SB_OCSP_ERROR_NO_CERTIFICATES = ERROR_FACILITY_OCSP + ERROR_OCSP_PROTOCOL_ERROR_FLAG + 1;
  SB_OCSP_ERROR_WRONG_DATA = ERROR_FACILITY_OCSP + ERROR_OCSP_PROTOCOL_ERROR_FLAG + 2;
  SB_OCSP_ERROR_NO_EVENT_HANDLER = ERROR_FACILITY_OCSP + ERROR_OCSP_PROTOCOL_ERROR_FLAG + 3;

type
  EOCSPParseError =  class(Exception);

  TElOCSPServerError = (oseSuccessful, oseMalformedRequest, oseInternalError,
  oseTryLater, oseUnused1, oseSigRequired, oseUnauthorized);

  TElOCSPCertificateStatus = (csGood, csRevoked, csUnknown);

  TElResponderIDType = (ritName, ritKeyHash);  


  TElOCSPReplyArray =  array of TElOCSPCertificateStatus;

  TElCertificateOCSPCheckEvent =  procedure(
    Sender : TObject;
    CertID : integer;
    var CertStatus : TElOCSPCertificateStatus;
    var Reason : TSBCRLReasonFlag;
    var RevocationTime, ThisUpdate, NextUpdate : TDateTime) of object;

implementation

end.
