
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSSHUtils;

interface



function WriteString(S : string) : string;
function WriteBoolean(B : boolean) : string;
function WriteUINT32(N : cardinal) : string;
function WriteUINT16(N : word) : string;
function WriteBitCount(P : pointer; Size : longint) : string;
function WriteSSH2MPInt(P : pointer; Size : longint) : string;
function WriteUINT64(N : Int64) : string;
function ReadSSH2MPInt(P : pointer; Size : longint; var HeaderSz : integer) : string;
function ReadBoolean(P : pointer; Size : longint) : boolean;
function ReadString(Buffer : pointer; Size : longint) : string;
function ReadSSH1MPInt(Buffer : pointer; Size : longint) : string;
function ReadLength(Buffer : pointer; Size : longint) : cardinal;
function ReadUINT16(Buffer : pointer; Size : longint) : word;
function ReadUINT64(Buffer : pointer; Size : longint) : Int64;

implementation

uses
  SysUtils,
  SBSSHCommon,
  SBSSHConstants;


resourcestring
  SInvalidSize          = 'Invalid size';

const
  MAX_STRING_SIZE       = 32768;

function WriteUINT32(N : cardinal) : string;
begin
  Result := Chr(N shr 24) + Chr((N shr 16) and $FF) + Chr((N shr 8) and $FF) +
    Chr(N and $FF);
end;

function WriteUINT16(N : word) : string;
begin
  Result := Chr(N shr 8) + Chr(N and $FF);
end;

function WriteString(S : string) : string;
var
  Len : cardinal;
begin
  Len := Length(S);
  Result := Chr((Len shr 24)) + Chr((Len shr 16) and $FF) + Chr((Len shr 8) and $FF) +
    Chr(Len and $FF) + S;
end;

function WriteBoolean(B : boolean) : string;
begin
  if B then
    Result := #1
  else
    Result := #0;
end;

function WriteSSH2MPInt(P : pointer; Size : longint) : string;
var
  OldLen : integer;
  OffP : ^byte;
begin
  OffP := P;
  while OffP^ = 0 do
  begin
    Inc(OffP);
    Dec(Size);
  end;
  if (PByteArray(OffP)[0] shr 7) = 1 then
    Result := WriteUINT32(Size + 1) + #$0
  else
    Result := WriteUINT32(Size);
  OldLen := Length(Result);
  SetLength(Result, OldLen + Size);
  Move(OffP^, Result[OldLen + 1], Size);
end;

function WriteBitCount(P : pointer; Size : longint) : string;
var
  I : integer;
  Bits : cardinal;
  B : byte;
begin
  I := 0;
  while (PByteArray(P)[I] = 0) and (I < Size) do
    Inc(I);
  if I < Size then
  begin
    B := PByteArray(P)[I];
    Bits := 0;
    while B <> 0 do
    begin
      B := B shr 1;
      Inc(Bits);
    end;
    Bits := Bits + cardinal((Size - I - 1) * 8);
  end
  else
    Bits := 0;
  Result := Chr(Bits shr 8) + Chr(Bits and $FF);
end;

function WriteUINT64(N : Int64) : string;
begin
  Result := Chr(N shr 56) + Chr((N shr 48) and $FF) + Chr((N shr 40) and $FF) +
    Chr((N shr 32) and $FF) + Chr((N shr 24) and $FF) + Chr((N shr 16) and $FF) +
    Chr((N shr 8) and $FF) + Chr(N and $FF);
end;

function ReadSSH1MPInt(Buffer : pointer; Size : longint) : string;
var
  BitCount : word;
begin
  BitCount := (PByteArray(Buffer)[0] shl 8) or PByteArray(Buffer)[1];
  SetLength(Result, (BitCount + 7) div 8);
  if Size - 2 < Length(Result) then
    raise EElSSHException.Create(SInvalidSize);
  Move(PByteArray(Buffer)[2], Result[1], Length(Result));
end;

function ReadLength(Buffer : pointer; Size : longint) : cardinal;
begin
  if Size < 4 then
    raise EElSSHException.Create(SInvalidSize);
  Result := (PByteArray(Buffer)[0] shl 24) or (PByteArray(Buffer)[1] shl 16) or
    (PByteArray(Buffer)[2] shl 8) or PByteArray(Buffer)[3];
end;

function ReadUINT16(Buffer : pointer; Size : longint) : word;
begin
  if Size <2 then
    raise EElSSHException.Create(SInvalidSize);
  Result := (PByteArray(Buffer)[0] shl 8) or (PByteArray(Buffer)[1]);
end;

function ReadSSH2MPInt(P : pointer; Size : longint; var HeaderSz :
  integer) : string;
var
  LIndex: Integer;
begin
  LIndex := 0;

  if Size < LIndex + 5 then
    raise EElSSHException.Create(SInvalidSize);

  SetLength(Result,(PByteArray(P)[LIndex] shl 24) or (PByteArray(P)[LIndex + 1] shl 16) or
    (PByteArray(P)[LIndex + 2] shl 8) or PByteArray(P)[LIndex + 3]);
  if Length(Result) > MAX_STRING_SIZE then
    raise EElSSHException.Create(SInvalidSize);
  if Length(Result) <> 0 then
  begin
    if PByteArray(P)[LIndex + 4] = 0 then
    begin
      SetLength(Result, Length(Result) - 1);
      Move(PByteArray(P)[LIndex + 5], Result[1], Length(Result));
      HeaderSz := 5;
    end
    else
    begin
      Move(PByteArray(P)[LIndex + 4], Result[1], Length(Result));
      HeaderSz := 4;
    end;
  end;
end;

function ReadBoolean(P : pointer; Size : longint) : boolean;
begin
  if (Size > 0) and (PByteArray(P)[0] <> 0) then
    Result := true
  else
    Result := false;
end;



function ReadString(Buffer : pointer; Size : longint) : string;
var
  Sz : integer;
begin
  if Size < 4 then
    raise EElSSHException.Create(SInvalidSize);
  Sz := (PByteArray(Buffer)[0] shl 24) or (PByteArray(Buffer)[1] shl 16) or
    (PByteArray(Buffer)[2] shl 8) or PByteArray(Buffer)[3];
  if (Size - 4 >= Sz) and (Sz <= MAX_STRING_SIZE) then
  begin
    SetLength(Result, Sz);
    Move(PByteArray(Buffer)[4], Result[1], Sz);
  end
  else
    raise EElSSHException.Create(SInvalidSize);
end;

function ReadUINT64(Buffer : pointer; Size : longint) : Int64;
var
  LStart: Integer;
begin
  LStart := 0;
  if Size < LStart + 8 then
    raise EElSSHException.Create(SInvalidSize)
  else
  begin
    Result := 
      (Int64(PByteArray(Buffer)[LStart]) shl 56) or
      (Int64(PByteArray(Buffer)[LStart + 1]) shl 48) or
      (Int64(PByteArray(Buffer)[LStart + 2]) shl 40) or
      (Int64(PByteArray(Buffer)[LStart + 3]) shl 32) or
      (Int64(PByteArray(Buffer)[LStart + 4]) shl 24) or
      (Int64(PByteArray(Buffer)[LStart + 5]) shl 16) or
      (Int64(PByteArray(Buffer)[LStart + 6]) shl 8) or
      (PByteArray(Buffer)[LStart + 7]);
  end;
end;


end.
