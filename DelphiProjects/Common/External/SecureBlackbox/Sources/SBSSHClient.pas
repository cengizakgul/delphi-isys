(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)
unit SBSSHClient;

{$I SecBbox.inc}

interface

uses
  SBMath,
  SBSymmetricCrypto,
  SBSHA,
  SBUtils,
  SBMD,
  SBBlowfish,
  {$ifndef SBB_NO_DES}
  SBDES,
  SB3DES,
  SBSSH3DES,
  {$endif}
  SBSSHTerm,
  {$ifndef SBB_NO_RC4}
  SBRC4,
  {$endif}
  {$ifndef SBB_NO_IDEA}
  SBIDEA,
  {$endif}
  SBSharedResource, SBSSHConstants, SBSSHCommon,
  {$ifndef DONT_USE_ZLIB}SBZlib, {$endif}
  SBSSHKeyStorage, SBHMAC, SBAES, SBSSHUtils, SBSerpent, SBCAST128,
  {$ifdef SECURE_BLACKBOX_DEBUG}
  SBDumper,
  {$endif}
  {$ifndef CLX_USED}Windows,{$endif}
  SysUtils,
  Classes,
  SBTwofish,
  SBSSHMath
;

type
  TElSSHClient = class;

  TSSHClientState = 
    (csBefore, csIdentificationLineReceived, csIdentificationLineSent);

  TElSSHClientTunnelList = class(TElSSHTunnelList);

  { This class handles single tunnel connection }
  TElSSHClientTunnelConnection = class(TElSSHTunnelConnection)
  protected
    procedure DoClose(CloseType : TSSHCloseType); override;
    procedure DoWindowChanged(Cols, Rows, Width, Height: integer); override;

    function GetCloseType: TSSHCloseType; override;
    function GetCloseWhenDataIsSent: Boolean; override;
    function GetInternalState: Integer; override;
    function GetInternalType: Integer; override;
    function GetLocalChannel: Cardinal; override;
    function GetLocalChannelClosed: Boolean; override;
    function GetLocalWindowSize: Integer; override;
    function GetParent: TElSSHClass; override;
    function GetRemoteChannel: Cardinal; override;
    function GetRemoteChannelClosed: Boolean; override;
    function GetWindowBuffer: ByteArray; override;
    function GetWindowSize: Integer; override;
    procedure SetCloseType(const Value: TSSHCloseType); override;
    procedure SetCloseWhenDataIsSent(Value: Boolean); override;
    procedure SetExitMessage(const Value: string); override;
    procedure SetExitSignal(const Value: string); override;
    procedure SetExitStatus(const Value: Integer); override;
    procedure SetInternalState(Value: Integer); override;
    procedure SetInternalType(Value: Integer); override;
    procedure SetLocalChannel(const Value: Cardinal); override;
    procedure SetLocalChannelClosed(const Value: Boolean); override;
    procedure SetLocalWindowSize(const Value: Integer); override;
    procedure SetParent(const Value: TElSSHClass); override;
    procedure SetRemoteChannel(const Value: Cardinal); override;
    procedure SetRemoteChannelClosed(const Value: Boolean); override;
    procedure SetWindowBuffer(const Value: ByteArray); override;
    procedure SetWindowBufferLength(const NewLength: Integer); override;
    procedure SetWindowSize(const Value: Integer); override;

    property Parent: TElSSHClass read GetParent write SetParent;

    property LocalChannel: Cardinal read GetLocalChannel write SetLocalChannel;
    property RemoteChannel: Cardinal read GetRemoteChannel write SetRemoteChannel;
    property LocalChannelClosed: boolean read GetLocalChannelClosed write SetLocalChannelClosed;
    property RemoteChannelClosed: boolean read GetRemoteChannelClosed write SetRemoteChannelClosed;
    property InternalType: Integer read GetInternalType write SetInternalType;
    property InternalState: Integer read GetInternalState write SetInternalState;

    property ExitStatus: Integer write SetExitStatus; // to read value use ExitStatus property
    property ExitSignal: string write SetExitSignal;
    property ExitMessage: string write SetExitMessage;

    property CloseType: TSSHCloseType read GetCloseType write SetCloseType;
    property WindowSize: Integer read GetWindowSize write SetWindowSize;
    property WindowBuffer: ByteArray read GetWindowBuffer write SetWindowBuffer;
    property LocalWindowSize: Integer read GetLocalWindowSize write SetLocalWindowSize;

    property CloseWhenDataIsSent: Boolean read GetCloseWhenDataIsSent write SetCloseWhenDataIsSent;

    procedure DoData(Buffer : pointer; Size : longint); override;
    procedure DoExtendedData(Buffer : pointer; Size : longint); override;
  public
    procedure SendData(Buffer : pointer; Size : longint); override;
    procedure SendExtendedData(Buffer : pointer; Size : longint); override;
    function CanSend : boolean; override;
  end;

  TElSSHClient = class(TElSSHClass)
  private
  protected
    FServerCloseReason : string;
    FServerSoftwareName : string;

    FClientState : TSSHClientState;
    FAuthOrder : TSBSSHAuthOrder;

    FSSH1ServerRSAParams : TRSAParams;
    FSSH1HostRSAParams : TRSAParams;
    FSSH1State : TSSH1State;
    FSSH1Params : TSSH1Params;
    { cipher contexts }
    {$ifndef SBB_NO_DES}
    FSSH1DesInputCtx : TDESContext;
    FSSH1DesOutputCtx : TDESContext;
    FSSH13DesInputCtx : TSSH3DESContext;
    FSSH13DesOutputCtx : TSSH3DESContext;
    {$endif}
    FSSH1BlowfishInputCtx : TSBBlowfishContext;
    FSSH1BlowfishOutputCtx : TSBBlowfishContext;
    {$ifndef SBB_NO_IDEA}
    FSSH1IdeaInputCtx : TIDEAContext;
    FSSH1IdeaOutputCtx : TIDEAContext;
    {$endif}
    {$ifndef SBB_NO_RC4}
    FSSH1RC4InputCtx : TRC4Context;
    FSSH1RC4OutputCtx : TRC4Context;
    {$endif}
    FSSH1ClientLastMessage : integer;
    FSSH1ServerLastMessage : integer;
    {$ifndef DONT_USE_ZLIB}
    FSSH1CompressionCtx : TZlibContext;
    FSSH1DecompressionCtx : TZlibContext;
    {$endif}
    FSSH1ShellTunnelIndex : integer;
    FSSH1SessionType : integer;
    FKexHash : ByteArray;
    FKexKey : BufferType;
    FDecompressionBuffer : ByteArray;

    FWrkArdFSecure1 : boolean;
    FClosing : boolean;
    FErrorString : string;


    procedure AnalyseBuffer;
    function GetThreadSafe : boolean;
    procedure SetThreadSafe(Value: boolean);
    function DecomprFunc(Buffer: pointer; Size: integer; Param: pointer): boolean;

  protected
    function GetSharedResource: TElSharedResource;

    procedure SendIdentificationString;
    procedure SendTunnelData(RemoteChannel : cardinal; Buffer : pointer; Size : longint); override;
    procedure SendTunnelSignal(RemoteChannel : cardinal; Signal : string); override;
    procedure SendTerminalResize(RemoteChannel: cardinal; Cols, Rows, Width,
      Height: integer); override;
    procedure CloseTunnel(Tunnel : TElSSHTunnelConnection; FlushCachedData: boolean {$ifdef HAS_DEF_PARAMS}= false{$endif}); override;
    procedure ConnectTunnel(Tunnel : TElCustomSSHTunnel; 
        Data : pointer {$ifdef HAS_DEF_PARAMS}= nil{$endif}); override;
    { SSH handshakes }
    procedure ParseServerIdentificationString(Buffer : pointer; Size : longint);
    { SSHv1 }
    procedure SSH1ParseOnTransportLayer(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerPublicKey(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerSuccess;
    procedure SSH1ParseServerFailure;
    procedure SSH1ParseServerCommandExitStatus(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerStdout(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerStderr(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerDisconnect(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerAuthRSAChallenge(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerPortOpen(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerChannelData(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerChannelOpenConfirmation(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerChannelOpenFailure(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerChannelClose(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerChannelCloseConfirmation(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerAgentOpen(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerX11Open(Buffer : pointer; Size : longint);
    procedure SSH1ParseServerDebug(Buffer : pointer; Size : longint);
    procedure SSH1SendOnTransportLayer(Size : longint);
    procedure SSH1SendSessionKey;
    procedure SSH1SendUser;
    procedure SSH1SendAuthPassword;
    procedure SSH1SendAuthRSA(Modulus : pointer; ModulusSize : integer);
    procedure SSH1SendAuthRSAResponse(Buffer : pointer; Size : integer);
    procedure SSH1SendAuthRhosts;
    procedure SSH1SendAuthTIS;
    procedure SSH1SendAuthRhostsRSA(Modulus : pointer; ModulusSize : integer;
      Exponent : pointer; ExponentSize : integer);
    procedure SSH1SendRequestPty;
    procedure SSH1SendExecShell;
    procedure SSH1SendRequestCompression;
    procedure SSH1SendExitConfirmation;
    procedure SSH1SendExecCmd(Cmd : string {ByteArray});
    procedure SSH1SendStdinData(Buffer : pointer; Size : integer);
    procedure SSH1SendDisconnect(const ReasonLine : string);
    procedure SSH1SendPortForwardRequest(RemotePort : word; Host : string; Port : word);
    procedure SSH1SendPortOpen(LocalChannel : cardinal; Host : string;
      Port : word; OriginatorString : string);
    procedure SSH1SendEOF;
    procedure SSH1SendAgentRequestForwarding;
    procedure SSH1SendChannelOpenConfirmation(ServerChannel : cardinal;
      ClientChannel : cardinal);
    procedure SSH1SendChannelOpenFailure(ServerChannel : cardinal);
    procedure SSH1SendChannelData(ServerChannel : cardinal; Buffer : pointer;
      Size : longint);
    procedure SSH1SendChannelClose(ServerChannel : cardinal);
    procedure SSH1SendChannelCloseConfirmation(ServerChannel : cardinal);
    procedure SSH1SendTunnelData(RemoteChannel : cardinal; Buffer : pointer;
      Size : longint);
    procedure SSH1SendX11RequestForwarding;
    procedure SSH1StartClientAuthentication;
    procedure SSH1ContinueClientAuthentication(AuthMask : cardinal);
    procedure SSH1LoginCompleted;
    procedure SSH1PtyAllocated;
    procedure SSH1ShellAllocated;
    procedure SSH1CompressionAllowed(Allow : boolean);
    procedure SSH1AuthRSAAllowed(Allow : boolean);
    procedure SSH1AuthRhostsRSAAllowed(Allow : boolean);
    procedure SSH1AuthTISAllowed(Allow : boolean);
    procedure SSH1AuthRhostsAllowed(Allow : boolean);
    procedure SSH1AuthPasswordAllowed(Allow : boolean);
    procedure SSH1PortForwardAllowed(Allow : boolean);
    procedure SSH1X11ForwardAllowed(Allow : boolean);
    procedure SSH1AgentForwardAllowed(Allow : boolean);
    procedure SSH1EstablishTunneling(TunnelIndex : integer);
    procedure SSH1EstablishAuthRSA(KeyIndex : integer);
    procedure SSH1EstablishAuthRhostsRSA(KeyIndex : integer);
    procedure SSH1CloseTunnel(Tunnel : TElSSHClientTunnelConnection);
    procedure SSH1ExecuteCommand(Command : string);
    procedure SSH1EncryptWithServerRSAKey(const InBuffer; var OutBuffer;
      const InSize : integer; var OutSize : integer);
    procedure SSH1EncryptWithHostRSAKey(const InBuffer; var OutBuffer;
      const InSize : integer; var OutSize : integer);
    procedure SSH1GenerateSessionKey;
    procedure SSH1DecryptPacket(InBuffer : pointer; OutBuffer : pointer; Size : integer);
    procedure SSH1EncryptPacket(InBuffer : pointer; OutBuffer : pointer; Size : integer);
    procedure SSH1CompressPacket(InBuffer : pointer; InSize : integer;
      OutBuffer : pointer; var OutSize : integer);
    procedure SSH1DecompressPacket(InBuffer : pointer; InSize : integer;
      OutBuffer : pointer; var OutSize : integer);
    function SSH1EncodePtyModes(TerminalInfo : TElTerminalInfo) : string;
    function SSH1ChooseCipher(Ciphers : cardinal) : cardinal;
    function SSH1ChooseAuth(Ciphers : integer): Integer;
    procedure SSH1CloseChannel(Channel : cardinal; Client : boolean);
    procedure SSH1ConnectTunnel(Tunnel : TElCustomSSHTunnel; Data : pointer);
    function SSH1CreateServerClientChannel(ServerChannel : cardinal; TunnelIndex : integer;
      var Connection : TElSSHClientTunnelConnection) : cardinal;
    function SSH1GetTunnelConnectionByLocalChannel(Channel : cardinal) : TElSSHClientTunnelConnection;
    function SSH1GetTunnelConnectionByRemoteChannel(Channel : cardinal) : TElSSHClientTunnelConnection;
    { SSHv2 }
    procedure SSH2ParseTransportLayerHeader(Buffer : pointer; Size : longint);
    procedure SSH2ParseOnTransportLayer(Buffer : pointer; Size : longint);
    procedure SSH2ParseOnUserauthLayer(Buffer : pointer; Size : longint);
    procedure SSH2ParseOnConnectionLayer(Buffer : pointer; Size : longint);
    procedure SSH2ParseKexInit(Buffer : pointer; Size : longint);
    procedure SSH2ParseKexDHReply(Buffer : pointer; Size : longint);
    procedure SSH2ParseKexDHGexGroup(Buffer : pointer; Size : longint);
    procedure SSH2ParseKexDHGexReply(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerNewkeys(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerDisconnect(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerDebug(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerServiceAccept(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerUserauthSuccess(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerUserauthFailure(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerUserauthBanner(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerUserauthPKOK(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerUserauthInfoRequest(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerChannelOpenConfirmation(Buffer : pointer; Size :
      longint);
    procedure SSH2ParseServerChannelSuccess(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerChannelFailure(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerChannelData(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerChannelExtendedData(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerChannelOpen(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerChannelOpenFailure(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerChannelEOF(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerChannelClose(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerChannelRequest(Buffer : pointer; Size : longint);
    procedure SSH2ParseServerChannelWindowAdjust(Buffer : pointer; Size : longint);
    procedure SSH2SendOnTransportLayer(Size : longint);
    procedure SSH2SendDisconnect(ReasonCode : integer; Desc : string);
    procedure SSH2SendKexInit;
    procedure SSH2SendKexDHInit;
    procedure SSH2SendKexDHGexRequest;
    procedure SSH2SendKexDHGexInit;
    procedure SSH2SendNewKeys;
    procedure SSH2SendServiceRequest(ServiceName : string);
    procedure SSH2SendUserauthRequestNone;
    procedure SSH2SendUserauthRequestPassword;
    procedure SSH2SendUserauthRequestPublickey(AlgName : string; PublicKeyBlob : string);
    procedure SSH2SendUserauthRequestHostbased(AlgName : string; PublicKeyBlob : string);
    procedure SSH2SendUserauthRequestPublickeySignature;
    procedure SSH2SendUserauthRequestKeyboardInteractive;
    procedure SSH2SendUserauthInfoResponse(Response : TStringList; Count : integer);
    procedure SSH2SendChannelSuccess(Channel : cardinal);
    procedure SSH2SendChannelFailure(Channel : cardinal);
    procedure SSH2SendChannelOpenSession(Channel : cardinal);
    procedure SSH2SendChannelOpenDirectTcpIp(Channel : cardinal; Host : string;
      Port : word; OrigIP : string; OrigPort : word);
    procedure SSH2SendChannelRequestPty(Channel : cardinal);
    procedure SSH2SendChannelRequestShell(Channel : cardinal);
    procedure SSH2SendChannelRequestExec(Channel : cardinal; Command : string);
    procedure SSH2SendChannelRequestSubsystem(Channel : cardinal; SubSystem : string);
    procedure SSH2SendChannelRequestX11(Channel : cardinal; AuthProto : string;
      ScreenNumber : integer);
    procedure SSH2SendChannelRequestSignal(Channel : cardinal; Signal : string);
    procedure SSH2SendChannelRequestEnv(Channel : cardinal; VarName : string; VarValue : string);
    procedure SSH2SendChannelClose(Channel : cardinal);
    procedure SSH2SendChannelOpenConfirmation(RemoteChannel : cardinal;
      LocalChannel : cardinal; WinSize : integer; MaxPacketSize : integer);
    procedure SSH2SendChannelOpenFailure(RemoteChannel : cardinal; ReasonCode : integer;
      Comment : string; LanguageTag : string);
    procedure SSH2SendChannelEOF(RemoteChannel : cardinal);
    procedure SSH2SendChannelWindowAdjust(RemoteChannel : cardinal; WinSize : cardinal);
    procedure SSH2SendChannelRequestWindowChange(RemoteChannel : cardinal;
      Cols, Rows, Width, Height : integer);
    procedure SSH2SendGlobalRequestTcpIpForward(Addr : string; Port : word);
    procedure SSH2DestroyChannel(Conn : TElSSHClientTunnelConnection);
    procedure SSH2StartClientAuthentication;
    procedure SSH2SendTunnelData(RemoteChannel : cardinal; Buffer : pointer;
      Size : integer);
    procedure SSH2SendEnvironment(RemoteChannel : cardinal; Environment : TStringList);
    procedure SSH2EstablishAuthPublicKey(KeyIndex : integer);
    procedure SSH2EstablishAuthHostbased(KeyIndex : integer);
    procedure SSH2EstablishAuthKeyboardInteractive;
    procedure SSH2EstablishInteractiveSessions;
    procedure SSH2EstablishSingleInteractiveSession(TunnelIndex : integer);
    procedure SSH2EstablishSession(Tunnel : TElCustomSSHTunnel; 
        Data: pointer {$ifdef HAS_DEF_PARAMS}= nil{$endif});
    procedure SSH2EstablishShell(Connection : TElSSHClientTunnelConnection);
    procedure SSH2EstablishCommand(Connection : TElSSHClientTunnelConnection);
    procedure SSH2EstablishSubsystem(Connection : TElSSHClientTunnelConnection);
    procedure SSH2EstablishX11(Connection : TElSSHClientTunnelConnection);
    procedure SSH2CreateChannel(Tunnel : TElCustomSSHTunnel; var Connection :
      TElSSHClientTunnelConnection);
    procedure SSH2CloseTunnel(Tunnel : TElSSHClientTunnelConnection; FlushCachedData: boolean {$ifdef HAS_DEF_PARAMS}= false{$endif});
    procedure SSH2ConnectTunnel(Tunnel : TElCustomSSHTunnel; Data : pointer);
    procedure SSH2ChooseAlgorithms(KexLines : TStringList);
    function SSH2ProduceKeyData(K : string; H : pointer; HSize : integer;
      Salt : string; Needed : integer) : string;
    procedure SSH2GenerateKeys(K : string; H : pointer; HSize : integer);
    procedure SSH2SetEncryptionKeys(Key : string; IV : string);
    procedure SSH2SetDecryptionKeys(Key : string; IV : string);
    function SSH2ComputeMAC(P : pointer; Size : longint; IsClient : boolean) : string;
    function SSH2ValidateMAC(Chunk1 : pointer; Size1 : longint; Chunk2 : pointer;
      Size2 : longint; OrigDigest : pointer) : boolean;
    function SSH2ValidateDSS(M : string; P : string; Q : string; G : string; Y : string;
      R : string; S : string) : boolean;
    procedure SSH2EncryptPacket(InBuffer : pointer; OutBuffer : pointer; Size : integer);
    procedure SSH2DecryptPacket(InBuffer : pointer; OutBuffer : pointer; Size : integer);
    procedure SSH2CompressPacket(InBuffer : pointer; InSize : integer;
      OutBuffer : pointer; var OutSize : integer);
    procedure SSH2DecompressPacket(InBuffer : pointer; InSize : integer;
      OutBuffer : pointer; var OutSize : integer);
    function SSH2CalculateSignature(Hash : string; KeyIndex : integer) : string;
    procedure SSH2KexDHGroupGenerate;
    function SSH2GetKexByName(KexStr : string) : integer;
    function SSH2GetEncryptionAlgorithmByName(EncStr : string) : integer;
    function SSH2GetPublicKeyAlgorithmByName(PubStr : string) : integer;
    function SSH2GetMacAlgorithmByName(MacStr : string) : integer;
    function SSH2GetCompressionAlgorithmByName(CompStr : string) : integer;
    function SSH2EncodePtyModes(TerminalInfo : TElTerminalInfo) : string;
    procedure SSH2KexDHPower(A, B, C : PLInt; var D : PLInt);
    procedure GenerateDHX;
    { Data formatting routines }
    function WriteKexAlgorithms : string;
    function WriteServerHostKeyAlgorithms : string;
    function WriteEncAlgorithmsCS : string;
    function WriteEncAlgorithmsSC : string;
    function WriteMACAlgorithmsCS : string;
    function WriteMACAlgorithmsSC : string;
    function WriteCompAlgorithmsCS : string;
    function WriteCompAlgorithmsSC : string;
    function WriteLanguagesCS : string;
    function WriteLanguagesSC : string;
    procedure CloseByError(ReasonLine : string);
    procedure DoTunnelOpen(Connection : TElSSHTunnelConnection); override;
    

    procedure PerformClose(Forced: Boolean; CloseReason: string);
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    { Commands }
    procedure Open;
    procedure Close(Forced : boolean {$ifdef HAS_DEF_PARAMS}= false{$endif}); override;
    procedure DataAvailable;
    { Properties }
    property Version : TSSHVersion read FVersion;
    property ServerCloseReason : string read FServerCloseReason;
    property ServerSoftwareName : string read FServerSoftwareName;
    property ErrorString : string read FErrorString;
  published
    property SSHAuthOrder : TSBSSHAuthOrder read FAuthOrder write FAuthOrder;

    property Versions : TSSHVersions read FVersions write FVersions;
    property ClientUserName : string read FClientUserName write FClientUserName;
    property ClientHostName : string read FClientHostName write FClientHostName;
    property UserName;
    property Password;
    property TunnelList;
    property OnKeyValidate;
    property OnAuthenticationSuccess;
    property OnAuthenticationFailed;
    property OnAuthenticationKeyboard;
    property OnBanner;
    property ThreadSafe : boolean read GetThreadSafe write SetThreadSafe;
  end;

procedure Register;

implementation

uses
  SBCRC, SBX509;

type

  TElSSHClientTunnel = TElCustomSSHTunnel;

procedure Register;
begin
  RegisterComponents('SSHBlackbox', [TElSSHClient]);
end;

constructor TElSSHClient.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  FAuthenticationTypes := SSH_AUTH_TYPE_PUBLICKEY or SSH_AUTH_TYPE_KEYBOARD or SSH_AUTH_TYPE_PASSWORD;

  LCreate(FSSH1ServerRSAParams.Modulus);
  LCreate(FSSH1ServerRSAParams.PublicExponent);
  LCreate(FSSH1HostRSAParams.Modulus);
  LCreate(FSSH1HostRSAParams.PublicExponent);


  {$ifdef SECURE_BLACKBOX_DEBUG}
  FDebugLevel := 1;
  {$endif}
  FCloseIfNoActiveTunnels := false;
end;


destructor TElSSHClient.Destroy;
begin
  LDestroy(FSSH1HostRSAParams.Modulus);
  LDestroy(FSSH1HostRSAParams.PublicExponent);
  LDestroy(FSSH1ServerRSAParams.Modulus);
  LDestroy(FSSH1ServerRSAParams.PublicExponent);
  inherited;
end;


function MemPos(SubStr : pointer; SubStrSize : integer; Str : pointer; Size : integer) : integer;
var
  I : integer;
begin
  Result := -1;
  for I := 0 to Size - SubStrSize do
  begin
    if CompareMem(SubStr, @PByteArray(Str)[I], SubStrSize) then
    begin
      Result := I;
      Break;
    end;
  end;
end;

procedure TElSSHClient.Open;
begin
  CheckLicenseKey();
  FInBufferIndex := 0;
  FOutBufferIndex := 0;
  FInBufferNeeded := IDENTIFICATION_STRING_SIZE;
  FClientState := csBefore;
  SetLength(FServerSoftwareName, 0);
  FServerCloseReason := '';
  FLastError := 0;
  FSSH1State := stNotEncrypted;
  FSSH2State := stNotEncrypted;
  FOpenCommand := false;
  FOpenSubsystem := false;
  FLastChannelIndex := 0;
  ClearChannels;
  FSSH1ClientLastMessage := -1;
  FSSH1ServerLastMessage := -1;
  FSSH2ClientSequenceNumber := 0;
  FSSH2ServerSequenceNumber := 0;
  FSSH2Params.EncCSBlockSize := 0;
  FSSH2Params.EncSCBlockSize := 0;
  FSSH2AuthenticationPassed := false;
  SetLength(FInSpool, 0);




  {$ifdef SECURE_BLACKBOX_DEBUG}
  DoDebug('----------------------------Opening SSH client--------------------------------', SSH_DEBUG_LEVEL_GLOBAL_EVENTS);
  {$endif}
  FErrorString := '';
end;

procedure TElSSHClient.Close(Forced : boolean {$ifdef HAS_DEF_PARAMS}= false{$endif});
begin
  PerformClose(Forced, SDisconnectConnectionClosed);
end;

procedure TElSSHClient.CloseByError(ReasonLine : string);
begin
  PerformClose(false, ReasonLine);
end;

procedure TElSSHClient.DataAvailable;
var
  Written : longint;
begin
  GetSharedResource.WaitToWrite;
  try
    if FInBufferNeeded = 0 then exit;

    if FInBufferNeeded + FInBufferIndex > Length(FInBuffer) then
      SetLength(FInBuffer, FInBufferNeeded + FInBufferIndex);
    DoReceive(@FInBuffer[FInBufferIndex], FInBufferNeeded, Written);
    if Written > 0 then
    begin
      Inc(FInBufferIndex, Written);
      Dec(FInBufferNeeded, Written);
      {$ifdef SECURE_BLACKBOX_DEBUG}
      if (FDebugLevel = SSH_DEBUG_LEVEL_BUGSEARCH) and (FInBufferNeeded < 0) then
        DoDebug('InBufferNeeded is less than 0 (' + IntToStr(FInBufferNeeded) + ') [2]', SSH_DEBUG_LEVEL_BUGSEARCH);
      {$endif}
      if (FInBufferNeeded <= 0) or
         (FClientState = csBefore) then
        AnalyseBuffer;
    end;
  finally
    GetSharedResource.Done;
  end;
end;

function Max(A, B : integer) : integer;
begin
  if A >= B then
    Result := A
  else
    Result := B;
end;

procedure TElSSHClient.AnalyseBuffer;
var
  Index : integer;
begin
  if FClientState = csBefore then
  begin
    Index := MemPos(@CRLF[1], 2, @FInBuffer[0], FInBufferIndex);
    if Index > 0 then
    begin
      FServerNewLine := CRLF;
      if MemPos(@SSH_IDENTIFIER[1], Length(SSH_IDENTIFIER), @FInBuffer[0], FInBufferIndex) <> 0 then
      begin
        FInBufferNeeded := IDENTIFICATION_STRING_SIZE;
        FInBufferIndex := 0;
      end;
      ParseServerIdentificationString(FInBuffer, Index);
      if FLastError = 0 then
        SendIdentificationString;

      { caching data which may appear after identification line (kexinit packet) }
      Inc(Index, 2);
      if FInBufferIndex > Index then
      begin
        SetLength(FInSpool, FInBufferIndex - Index);
        Move(FInBuffer[Index], FInSpool[0], FInBufferIndex - Index);
      end;

      FInBufferIndex := 0;
      if FVersion = sbSSH2 then
        FInBufferNeeded := 8
      else
        FInBufferNeeded := 4;
      FReceiveState := rsRecordHeaderWanted;

      { Feeding cached data to ElSSHClient }
      while Length(FInSpool) > 0 do
        DataAvailable;

      Exit;
    end;
    Index := MemPos(@LF[1], 1, @FInBuffer[0], FInBufferIndex);

    if Index <= 0 then
    begin
      (*
      { Neither CRLF nor LF was found. Possibly, the remote software does not use line endings. }
      { Considering first 0x00 character (the start of SSH packet) as an end of id line. }
      { If the line does not contain 0x00 characters, considering the whole line to be the id line. }
      {$ifdef SECURE_BLACKBOX_DEBUG}
      Dumper.WriteString('No CR/LF found for identification string');
      {$endif}
      {$ifdef DELPHI_NET}
      Index := MemPos(NULLCHAR, 1, FInBuffer, FInBufferIndex);
      {$else}
      Index := MemPos(@NULLCHAR[1], 1, FInBuffer, FInBufferIndex);
      {$endif}
      if Index < 0 then
        Index := FInBufferIndex;
      *)
      Exit;
    end;

    if Index > 0 then
    begin
      FServerNewLine := LF;
      if MemPos(@SSH_IDENTIFIER[1], Length(SSH_IDENTIFIER), @FInBuffer[0], FInBufferIndex) <> 0 then
      begin
        FInBufferNeeded := IDENTIFICATION_STRING_SIZE;
        FInBufferIndex := 0;
      end;
      ParseServerIdentificationString(@FInBuffer[0], Index);
      if FLastError = 0 then
        SendIdentificationString;

      { caching kexinit data which may appear after identification line }
      Inc(Index, 1);
      if FInBufferIndex > Index then
      begin
        SetLength(FInSpool, FInBufferIndex - Index);
        Move(FInBuffer[Index], FInSpool[0], FInBufferIndex - Index);
      end;

      FInBufferIndex := 0;
      if FVersion = sbSSH2 then
        FInBufferNeeded := 8
      else
        FInBufferNeeded := 4;
      FReceiveState := rsRecordHeaderWanted;

      { Feeding ElSSHClient with cached data }
      while Length(FInSpool) > 0 do
        DataAvailable;

      Exit;
    end;
  end;
  if FReceiveState = rsRecordHeaderWanted then
  begin
    {$ifdef SECURE_BLACKBOX_DEBUG}
    Dumper.WriteString('ElSSHClient::AnalyseBuffer(rsRecordHeaderWanted)');
    {$endif}
    FReceiveState := rsRecordWanted;
    if FVersion = sbSSH2 then
    begin
      SSH2ParseTransportLayerHeader(@FInBuffer[0], FInBufferIndex);
    end
    else
    begin
      FInBufferNeeded := (FInBuffer[0] shl 24) or (FInBuffer[1] shl 16) or
        (FInBuffer[2] shl 8) or (FInBuffer[3]);
      FPaddingSize := 8 - FInBufferNeeded mod 8;
      FInBufferNeeded := FInBufferNeeded + FPaddingSize;
      if FInBufferNeeded >= IN_BUFFER_SIZE then
      begin
        DoError(ERROR_SSH_INVALID_PACKET_SIZE);
        CloseByError(SDisconnectInvalidPacketSize);
        Exit;
      end;
      FInBufferIndex := 0;
    end;
    {$ifdef SECURE_BLACKBOX_DEBUG}
    Dumper.WriteString('Done');
    {$endif}
  end
  else if FReceiveState = rsRecordWanted then
  begin
    {$ifdef SECURE_BLACKBOX_DEBUG}
    Dumper.WriteString('ElSSHClient::AnalyseBuffer(rsRecordWanted)');
    {$endif}
    if FVersion = sbSSH2 then
    begin
      if FSSH2State = stEncrypted then
      begin
        { Decrypting and checking mac }
        SSH2DecryptPacket(@FInBuffer[FInBufferOffset],
          @FInBuffer[FInBufferOffset], FInBufferIndex - FInBufferOffset -
          SSH2MacDigestSizes[FSSH2Params.MacAlgorithmSC]);
        if not SSH2ValidateMAC(@FInBufferHeader[1], Length(FInBufferHeader), @FInBuffer[0],
          FInBufferIndex - SSH2MacDigestSizes[FSSH2Params.MacAlgorithmSC],
          @FInBuffer[FInBufferIndex - 20]) then
        begin
          DoError(ERROR_SSH_INVALID_MAC);
          CloseByError(SDisconnectInvalidMAC);
          Exit;
        end;

        SSH2ParseOnTransportLayer(@FInBuffer[0], FInBufferIndex - FPaddingSize -
          SSH2MacDigestSizes[FSSH2Params.MacAlgorithmSC]);
      end
      else
        SSH2ParseOnTransportLayer(@FInBuffer[0], FInBufferIndex - FPaddingSize);
      FInBufferNeeded := Max(8, FSSH2Params.EncSCBlockSize);
    end
    else
    begin
      SSH1ParseOnTransportLayer(FInBuffer, FInBufferIndex);
      FInBufferNeeded := 4;
    end;
    FInBufferIndex := 0;
    FReceiveState := rsRecordHeaderWanted;
  end;
end;



procedure TElSSHClient.SendTunnelData(RemoteChannel : cardinal; Buffer : pointer;
  Size : longint);
begin
  if not FActive then
  begin
    DoError(ERROR_SSH_NOT_CONNECTED);
    Exit;
  end;
  {$ifdef SECURE_BLACKBOX_DEBUG}
  DoDebug('Sending ' + IntToStr(Size) + ' bytes to channel ' + IntToStr(RemoteChannel),
    SSH_DEBUG_LEVEL_ALL_EVENTS);
  {$endif}
  if FVersion = sbSSH1 then
    SSH1SendTunnelData(RemoteChannel, Buffer, Size)
  else
  if FVersion = sbSSH2 then
    SSH2SendTunnelData(RemoteChannel, Buffer, Size);
end;

procedure TElSSHClient.SendTunnelSignal(RemoteChannel : cardinal; Signal : string);
begin
  {$ifdef SECURE_BLACKBOX_DEBUG}
  DoDebug('Sending signal ' + Signal + ' to channel ' + IntToStr(RemoteChannel),
    SSH_DEBUG_LEVEL_ALL_EVENTS);
  {$endif}
  if FVersion = sbSSH2 then
    SSH2SendChannelRequestSignal(RemoteChannel, Signal);
end;

procedure TElSSHClient.CloseTunnel(Tunnel : TElSSHTunnelConnection;
  FlushCachedData: boolean {$ifdef HAS_DEF_PARAMS}= false{$endif});
begin
  if FVersion = sbSSH1 then
    SSH1CloseTunnel(TElSSHClientTunnelConnection(Tunnel))
  else
  if FVersion = sbSSH2 then
    SSH2CloseTunnel(TElSSHClientTunnelConnection(Tunnel), FlushCachedData);
end;

procedure TElSSHClient.ConnectTunnel(Tunnel : TElCustomSSHTunnel; 
    Data : pointer {$ifdef HAS_DEF_PARAMS}= nil{$endif});
begin
  if FVersion = sbSSH1 then
    SSH1ConnectTunnel(Tunnel, Data)
  else
  if FVersion = sbSSH2 then
    SSH2ConnectTunnel(Tunnel, Data);
end;

procedure TElSSHClient.DoTunnelOpen(Connection : TElSSHTunnelConnection);
begin
// commented by II Oct 18 2004
//  if FVersion = sbSSH2 then
//    SSH2SendChannelWindowAdjust(TElSSHClientTunnelConnection(Connection).FRemoteChannel, $00020000);
  inherited;
end;

////////////////////////////////////////////////////////////////////////////////
// SSH1 protocol routines

procedure TElSSHClient.SSH1ParseOnTransportLayer(Buffer : pointer; Size : longint);
var
  Checksum, ActualChecksum : cardinal;
  I : integer;
begin
  { 1. Decrypting the data }
  if FSSH1State = stEncrypted then
  begin
    SSH1DecryptPacket(Buffer, Buffer, Size);
  end;
  { 2. Validating CRC }
  Checksum := CRC32(Buffer, Size - 4);
  ActualChecksum := (PByteArray(Buffer)[Size - 4] shl 24) or
    (PByteArray(Buffer)[Size - 3] shl 16) or
    (PByteArray(Buffer)[Size - 2] shl 8) or
    (PByteArray(Buffer)[Size - 1]);
  if Checksum <> ActualChecksum then
  begin
    DoError(ERROR_SSH_INVALID_CRC);
    CloseByError(SDisconnectInvalidCRC);
  end;
  { 2.5. Decompressing the data }
  if FUseCompression then
  begin
    I := Size - FPaddingSize;
    try
      SSH1DecompressPacket(@PByteArray(Buffer)[FPaddingSize], Size - FPaddingSize - 4,
        @PByteArray(Buffer)[FPaddingSize], I);
    except
      on E : Exception do
      begin
        DoError(ERROR_SSH_COMPRESSION_ERROR);
        CloseByError(E.Message);
        Exit;
      end;
    end;
    Size := I + FPaddingSize + 4;
  end;
  { 3. Analyzing message }
  FSSH1ServerLastMessage := PByteArray(Buffer)[FPaddingSize];
  case PByteArray(Buffer)[FPaddingSize] of
    SSH1_MSG_PUBLIC_KEY : SSH1ParseServerPublicKey(@PByteArray(Buffer)[FPaddingSize],
      Size - FPaddingSize - 4);
    SSH1_MSG_SUCCESS : SSH1ParseServerSuccess;
    SSH1_MSG_FAILURE : SSH1ParseServerFailure;
    SSH1_MSG_STDOUT_DATA : SSH1ParseServerStdout(@PByteArray(Buffer)[FPaddingSize],
      Size - FPaddingSize - 4);
    SSH1_MSG_STDERR_DATA : SSH1ParseServerStderr(@PByteArray(Buffer)[FPaddingSize],
      Size - FPaddingSize - 4);
    SSH1_MSG_EXIT_STATUS: SSH1ParseServerCommandExitStatus(@PByteArray(Buffer)[FPaddingSize],
      Size - FPaddingSize - 4);
    SSH1_MSG_DISCONNECT : SSH1ParseServerDisconnect(@PByteArray(Buffer)[FPaddingSize],
      Size - FPaddingSize - 4);
    SSH1_MSG_AUTH_RSA_CHALLENGE : SSH1ParseServerAuthRSAChallenge(@PByteArray(Buffer)[FPaddingSize],
      Size - FPaddingSize - 4);
    SSH1_MSG_PORT_OPEN : SSH1ParseServerPortOpen(@PByteArray(Buffer)[FPaddingSize],
      Size - FPaddingSize - 4);
    SSH1_MSG_CHANNEL_OPEN_CONFIRMATION : SSH1ParseServerChannelOpenConfirmation(
      @PByteArray(Buffer)[FPaddingSize], Size - FPaddingSize - 4);
    SSH1_MSG_CHANNEL_OPEN_FAILURE : SSH1ParseServerChannelOpenFailure(
      @PByteArray(Buffer)[FPaddingSize], Size - FPaddingSize - 4);
    SSH1_MSG_CHANNEL_DATA : SSH1ParseServerChannelData(@PByteArray(Buffer)[FPaddingSize],
      Size - FPaddingSize - 4);
    SSH1_MSG_CHANNEL_CLOSE : SSH1ParseServerChannelClose(@PByteArray(Buffer)[FPaddingSize],
      Size - FPaddingSize - 4);
    SSH1_MSG_CHANNEL_CLOSE_CONFIRMATION : SSH1ParseServerChannelCloseConfirmation(
      @PByteArray(Buffer)[FPaddingSize], Size - FPaddingSize - 4);
    SSH1_MSG_AGENT_OPEN : SSH1ParseServerAgentOpen(@PByteArray(Buffer)[FPaddingSize],
      Size - FPaddingSize - 4);
    SSH1_MSG_X11_OPEN : SSH1ParseServerX11Open(@PByteArray(Buffer)[FPaddingSize],
      Size - FPaddingSize - 4);
    SSH1_MSG_DEBUG : SSH1ParseServerDebug(@PByteArray(Buffer)[FPaddingSize],
      Size - FPaddingSize - 4);
    SSH1_MSG_IGNORE :;
    else
    begin
      DoError(ERROR_SSH_INVALID_PACKET_TYPE);
      CloseByError(SDisconnectInvalidPacketType);
    end;
  end;
end;

procedure TElSSHClient.SSH1ParseServerPublicKey(Buffer : pointer; Size : longint);
var
  SPubExp, SMod, HPubExp, HMod, Tmp : string;
//  ServerKeyBits, HostKeyBits : cardinal;
{  ProtocolFlags, }SupportedCiphers, SupportedAuths : cardinal;
  Index : integer;
  NewSessionID : TMessageDigest128;
  Key : TElSSHKey;
  B : boolean;
begin
  if (FSSH1ClientLastMessage <> -1) or (Size < 12) then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  Move(PByteArray(Buffer)[1], FServerCookie[0], 8);
  {ServerKeyBits := }ReadLength(@PByteArray(Buffer)[9], 4);
  Index := 12;
  try
    SPubExp := ReadSSH1MPInt(@PByteArray(Buffer)[13], Size - Index);
    Index := 13 + Length(SPubExp) + 2;
    SMod := ReadSSH1MPInt(@PByteArray(Buffer)[Index], Size - Index);
    Index := Index + Length(SMod) + 2;
    {HostKeyBits := }ReadLength(@PByteArray(Buffer)[Index], Size - Index);
    Index := Index + 4;
    HPubExp := ReadSSH1MPInt(@PByteArray(Buffer)[Index], Size - Index);
    Index := Index + Length(HPubExp) + 2;
    HMod := ReadSSH1MPInt(@PByteArray(Buffer)[Index], Size - Index);
    Index := Index + Length(HMod) + 2;
    {ProtocolFlags := }ReadLength(@PByteArray(Buffer)[Index], Size - Index);
    Inc(Index, 4);
    SupportedCiphers := ReadLength(@PByteArray(Buffer)[Index], Size - Index);
    Inc(Index, 4);
    SupportedAuths := ReadLength(@PByteArray(Buffer)[Index], Size - Index);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  
  Key := TElSSHKey.Create;
  Key.Algorithm := ALGORITHM_RSA;
  Key.RSAPublicExponent := HPubExp;
  Key.RSAPublicModulus := HMod;
  B := true;
  DoKeyValidate(Key, B);
  FreeAndNil(Key);
  
  if not B then
  begin
    DoError(ERROR_SSH_HOST_KEY_NOT_VERIFIABLE);
    CloseByError(SDisconnectHostKeyNotVerifiable);
    Exit;
  end;
  NewSessionId.A := SupportedAuths + SupportedCiphers + 39;
  Tmp := HMod + SMod;
  SetLength(Tmp, Length(Tmp) + 8);

  Move(FServerCookie[0], Tmp[Length(Tmp) - 7], 8);
  NewSessionID := HashMD5(Tmp);
  Move(NewSessionID, FSSH1Params.SessionId[0], 16);
  try
    FSSH1Params.Cipher := SSH1ChooseCipher(SupportedCiphers);
  except
    DoError(ERROR_SSH_UNSUPPORTED_CIPHER);
    CloseByError(SDisconnectUnsupportedCipher);
    Exit;
  end;
  try
    FSSH1Params.Auth := SSH1ChooseAuth(SupportedAuths);
  except
    DoError(ERROR_SSH_UNSUPPORTED_AUTH_TYPE);
    CloseByError(SDisconnectUnsupportedAuthType);
    Exit;
  end;
  SSH1GenerateSessionKey;
  PointerToLInt(FSSH1ServerRSAParams.Modulus, @SMod[1], Length(SMod));
  PointerToLInt(FSSH1ServerRSAParams.PublicExponent, @SPubExp[1], Length(SPubExp));
  PointerToLInt(FSSH1HostRSAParams.Modulus, @HMod[1], Length(HMod));
  PointerToLInt(FSSH1HostRSAParams.PublicExponent, @HPubExp[1], Length(HPubExp));
  SSH1SendSessionKey;
end;

procedure TElSSHClient.SSH1ParseServerSuccess;
begin
  case FSSH1ClientLastMessage of
    SSH1_MSG_SESSION_KEY: SSH1SendUser;
    SSH1_MSG_AUTH_PASSWORD: SSH1LoginCompleted;
    SSH1_MSG_AUTH_RHOSTS : SSH1LoginCompleted;
    SSH1_MSG_AUTH_RSA_RESPONSE : SSH1LoginCompleted;
    SSH1_MSG_REQUEST_PTY : SSH1PtyAllocated;
    SSH1_MSG_EXEC_SHELL : SSH1ShellAllocated;
    SSH1_MSG_USER : SSH1LoginCompleted;
    SSH1_MSG_REQUEST_COMPRESSION : SSH1CompressionAllowed(true);
    SSH1_MSG_PORT_FORWARD_REQUEST : SSH1PortForwardAllowed(true);
    SSH1_MSG_X11_REQUEST_FORWARDING : SSH1X11ForwardAllowed(true);
    SSH1_MSG_AGENT_REQUEST_FORWARDING : SSH1AgentForwardAllowed(true);
    else
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
    end;
  end;
end;

procedure TElSSHClient.SSH1ParseServerFailure;
begin
  case FSSH1ClientLastMessage of
    SSH1_MSG_USER: SSH1StartClientAuthentication;
    SSH1_MSG_REQUEST_COMPRESSION : SSH1CompressionAllowed(false);
    SSH1_MSG_AUTH_RSA: SSH1AuthRSAAllowed(false);
    SSH1_MSG_AUTH_TIS: SSH1AuthTISAllowed(false);
    SSH1_MSG_PORT_FORWARD_REQUEST : SSH1PortForwardAllowed(false);
    SSH1_MSG_X11_REQUEST_FORWARDING : SSH1X11ForwardAllowed(false);
    SSH1_MSG_AGENT_REQUEST_FORWARDING : SSH1AgentForwardAllowed(false);
    SSH1_MSG_AUTH_RHOSTS_RSA: SSH1AuthRhostsRSAAllowed(false);
    SSH1_MSG_AUTH_PASSWORD : SSH1AuthPasswordAllowed(false);
    SSH1_MSG_AUTH_RHOSTS: SSH1AuthRhostsAllowed(false);
    SSH1_MSG_AUTH_RSA_RESPONSE:
      begin
        if FCurrentRSAAuth = SSH_AUTH_TYPE_PUBLICKEY then
          SSH1AuthRSAAllowed(false)
        else if FCurrentRSAAuth = SSH_AUTH_TYPE_HOSTBASED then
          SSH1AuthRhostsRSAAllowed(false)
        else
        begin
          DoError(ERROR_SSH_INVALID_PACKET);
          CloseByError(SDisconnectInvalidPacket);
        end;
      end;
    SSH1_MSG_REQUEST_PTY :
      begin
        SSH1EstablishTunneling(0)
      end;
    else
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
    end;
  end;
end;

procedure TElSSHClient.SSH1ParseServerCommandExitStatus(Buffer : pointer; Size : longint);
var
  ExitStatus : cardinal;
  I : integer;
begin
  if Size < 4 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  ExitStatus := (PByteArray(Buffer)[1] shl 24) or (PByteArray(Buffer)[2] shl 16) or
    (PByteArray(Buffer)[3] shl 8) or PByteArray(Buffer)[4];
  SSH1SendExitConfirmation;
  for I := 0 to FChannels.Count - 1 do
    if (TElSSHClientTunnelConnection(FChannels[I]).GetInternalType = SSH_CONN_INTERNAL_TYPE_SHELL) or
       (TElSSHClientTunnelConnection(FChannels[I]).GetInternalType = SSH_CONN_INTERNAL_TYPE_COMMAND) then
    begin
      TElSSHClientTunnelConnection(FChannels[I]).SetExitStatus(ExitStatus);
      TElSSHClientTunnelConnection(FChannels[I]).SetExitMessage('');
      TElSSHClientTunnelConnection(FChannels[I]).DoClose(ctReturn);
      // shell or command finished, raising OnClose (only SSH1)
      DoCloseConnection;
      Break;
    end
end;

procedure TElSSHClient.SSH1ParseServerStdout(Buffer : pointer; Size : longint);
var
  I : integer;
begin
  if FSSH1ShellTunnelIndex <> -1 then
  begin
    for I := 0 to FChannels.Count - 1 do
    begin
      if (TElSSHClientTunnelConnection(FChannels.Items[I]).GetInternalType =
        SSH_CONN_INTERNAL_TYPE_SHELL) or
        (TElSSHClientTunnelConnection(FChannels.Items[I]).GetInternalType =
        SSH_CONN_INTERNAL_TYPE_COMMAND)
        then
      begin
        TElSSHClientTunnelConnection(FChannels.Items[I]).DoData(@PByteArray(Buffer)[5], Size - 5);
        Break;
      end;
    end;
  end;
end;

procedure TElSSHClient.SSH1ParseServerStderr(Buffer : pointer; Size : longint);
var
  I : integer;
begin
  if FSSH1ShellTunnelIndex <> -1 then
  begin
    for I := 0 to FChannels.Count - 1 do
    begin
      if TElSSHClientTunnelConnection(FChannels.Items[I]).GetInternalType =
        SSH_CONN_INTERNAL_TYPE_SHELL then
      begin
        TElSSHClientTunnelConnection(FChannels.Items[I]).DoExtendedData(@PByteArray(Buffer)[5],
          Size - 5);
        Break;
      end;
    end;
  end;
end;

procedure TElSSHClient.SSH1ParseServerDisconnect(Buffer : pointer; Size : longint);
var
  Len : integer;
begin
  Len := (PByteArray(Buffer)[1] shl 24) or (PByteArray(Buffer)[2] shl 16) or
    (PByteArray(Buffer)[3] shl 8) or PByteArray(Buffer)[4];
  if Len > Size - 5 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  SetLength(FServerCloseReason, Len);
  Move(PByteArray(Buffer)[5], FServerCloseReason[1], Len);
  // raising OnClose for all opened channels
  for Len := 0 to FChannels.Count - 1 do
    if Assigned(FChannels.Items[Len]) then
      TElSSHClientTunnelConnection(FChannels.Items[Len]).DoClose(ctError);
  DoCloseConnection;
end;

procedure TElSSHClient.SSH1ParseServerAuthRSAChallenge(Buffer : pointer; Size : longint);
var
  Challenge : string;
  Key : TElSSHKey;
  PrivateKey, PublicModulus, EncryptedChallenge, DecryptedChallenge : PLInt;
  Sz : integer;
  Ctx : TMD5Context;
  M128 : TMessageDigest128;
begin
  try
    Challenge := ReadSSH1MPInt(@PByteArray(Buffer)[1], Size - 1);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  LCreate(PrivateKey);
  LCreate(PublicModulus);
  LCreate(EncryptedChallenge);
  LCreate(DecryptedChallenge);
  PointerToLInt(EncryptedChallenge, @Challenge[1], Length(Challenge));
  Key := FKeyStorage.Keys[FLastKeyIndex];
  if not Assigned(Key) then
  begin
    DoError(ERROR_SSH_INTERNAL_ERROR);
    CloseByError(SDisconnectInternalError);
    Exit;
  end;
  PointerToLInt(PrivateKey, @Key.RSAPrivateExponent[1], Length(Key.RSAPrivateExponent));
  PointerToLInt(PublicModulus, @Key.RSAPublicModulus[1], Length(Key.RSAPublicModulus));
  LSSHModPower(EncryptedChallenge, PrivateKey, PublicModulus, DecryptedChallenge); ///
  Sz := DecryptedChallenge^.Length * 4 + 1;
  SetLength(Challenge, Sz);
  LIntToPointer(DecryptedChallenge, @Challenge[1], Sz);
  SetLength(Challenge, Sz);
  LDestroy(PrivateKey);
  LDestroy(PublicModulus);
  LDestroy(EncryptedChallenge);
  LDestroy(DecryptedChallenge);
  if (Challenge[1] <> #0) or (Challenge[2] <> #2) then
  begin
    DoError(ERROR_SSH_INVALID_RSA_CHALLENGE);
    CloseByError(SDisconnectInvalidRSAChallenge);
    Exit;
  end;
  Challenge := Copy(Challenge, 3, Length(Challenge));
  Sz := Pos(#0, Challenge);
  Challenge := Copy(Challenge, Sz + 1, Length(Challenge));
  if Length(Challenge) <> 32 then
  begin
    DoError(ERROR_SSH_INVALID_RSA_CHALLENGE);
    CloseByError(SDisconnectInvalidRSAChallenge);
    Exit;
  end;
  InitializeMD5(Ctx);
  HashMD5(Ctx, @Challenge[1], Length(Challenge));
  HashMD5(Ctx, @FSSH1Params.SessionID[0], 16);
  M128 := FinalizeMD5(Ctx);
  SSH1SendAuthRSAResponse(@M128, 16);
end;

procedure TElSSHClient.SSH1ParseServerPortOpen(Buffer : pointer; Size : longint);
var
  ServerChannel, ClientChannel : cardinal;
  HostName, OrigString : string;
  Port : word;
  I : integer;
  Found : boolean;
  Conn : TElSSHClientTunnelConnection;
begin
  try
    ServerChannel := ReadLength(@PByteArray(Buffer)[1], Size - 1);
    HostName := ReadString(@PByteArray(Buffer)[5], Size - 5);
    Port := ReadLength(@PByteArray(Buffer)[9 + Length(HostName)], Size - 9 -
      Length(HostName));
    OrigString := ReadString(@PByteArray(Buffer)[13 + Length(HostName)], Size -
      9 - Length(HostName) - 4);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  { Creating the channel }
  Found := false;
  if Assigned(FTunnelList) then
  begin
    for I := 0 to FTunnelList.Count - 1 do
    begin
      if FTunnelList.Tunnels[I] is TElRemotePortForwardSSHTunnel then
      begin
        if (TElRemotePortForwardSSHTunnel(FTunnelList.Tunnels[I]).ToPort = Port) and
          (CompareStr(TElRemotePortForwardSSHTunnel(FTunnelList.Tunnels[I]).ToHost,
          HostName) = 0) then
        begin
          ClientChannel := SSH1CreateServerClientChannel(ServerChannel, I, Conn);
          SSH1SendChannelOpenConfirmation(ServerChannel, ClientChannel);
          TElSSHClientTunnel(FTunnelList.Tunnels[I]).DoOpen(Conn);
          Found := true;
          Break;
        end;
      end;
    end;
  end;
  if not Found then
    SSH1SendChannelOpenFailure(ServerChannel);
  {$ifdef SECURE_BLACKBOX_DEBUG}
  DoDebug('Server port opened', SSH_DEBUG_LEVEL_ALL_EVENTS);
  DoDebug('ServerChannel=' + Inttostr(ServerChannel) + ', hostname=' + HostName +
    ', port=' + Inttostr(port) + ', orig=' + OrigString, SSH_DEBUG_LEVEL_ALL_EVENTS);
  {$endif}
end;

procedure TElSSHClient.SSH1ParseServerChannelData(Buffer : pointer; Size : longint);
var
  ClientChannel : cardinal;
  Data : string;
  Conn : TElSSHClientTunnelConnection;
begin
  try
    ClientChannel := ReadLength(@PByteArray(Buffer)[1], Size - 1);
    Data := ReadString(@PByteArray(Buffer)[5], Size - 5);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  {$ifdef SECURE_BLACKBOX_DEBUG}
  DoDebug('Received data from channel ' + IntToStr(ClientChannel) + ': ' + Data,
    SSH_DEBUG_LEVEL_PACKET_DATA);
  {$endif}
  Conn := SSH1GetTunnelConnectionByLocalChannel(ClientChannel);
  if Conn <> nil then
  begin
    Conn.DoData(@Data[1], Length(Data));
  end
  else
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
  end;
end;

procedure TElSSHClient.SSH1ParseServerChannelOpenConfirmation(Buffer : pointer;
  Size : longint);
var
  ClientCh, ServerCh : cardinal;
  Conn : TElSSHClientTunnelConnection;
begin
  if Size < 9 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  ClientCh := ReadLength(@PByteArray(Buffer)[1], Size);
  ServerCh := ReadLength(@PByteArray(Buffer)[5], Size);
  Conn := SSH1GetTunnelConnectionByLocalChannel(ClientCh);
  if Conn <> nil then
  begin
    Conn.SetRemoteChannel(ServerCh);
    TElSSHClientTunnel(Conn.Tunnel).DoOpen(Conn);
  end
  else
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
  end;
end;

procedure TElSSHClient.SSH1ParseServerChannelOpenFailure(Buffer : pointer; Size : longint);
var
  Ch : cardinal;
  Conn : TElSSHClientTunnelConnection;
begin
  if Size < 5 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  Ch := ReadLength(@PByteArray(Buffer)[1], Size);
  Conn := SSH1GetTunnelConnectionByLocalChannel(Ch);
  if Conn <> nil then
  begin
    FChannels.Remove(Conn);
    TElSSHClientTunnel(Conn.Tunnel).ConnectionsList.Remove(Conn);
    TElSSHClientTunnel(Conn.Tunnel).DoError(SSH_TUNNEL_ERROR_SERVER_ERROR,
      Conn.Data);
    FreeAndNil(Conn);
  end
  else
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
  end;
end;

procedure TElSSHClient.SSH1ParseServerChannelClose(Buffer : pointer; Size : longint);
var
  Ch: cardinal;
  Conn : TElSSHClientTunnelConnection;
begin
  if Size < 5 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  Ch := ReadLength(@PByteArray(Buffer)[1], 4);
  Conn := SSH1GetTunnelConnectionByLocalChannel(Ch);
  if Conn = nil then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  SSH1SendChannelCloseConfirmation(Conn.GetRemoteChannel);
  Conn.DoClose(ctReturn);
  SSH1CloseChannel(Conn.GetRemoteChannel, false);
  if Assigned(Conn) then
  begin
    if not Conn.GetLocalChannelClosed then
      SSH1SendChannelClose(Conn.GetRemoteChannel);
  end;
end;

procedure TElSSHClient.SSH1ParseServerChannelCloseConfirmation(Buffer : pointer;
  Size : longint);
var
  Ch : cardinal;
begin
  if Size < 5 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  Ch := ReadLength(@PByteArray(Buffer)[1], 4);
  SSH1CloseChannel(Ch, true);
end;

procedure TElSSHClient.SSH1ParseServerX11Open(Buffer : pointer; Size : longint);
var
  ClientChannel, ServerChannel : cardinal;
  Orig : string;
  Found : boolean;
  I : integer;
  Conn : TElSSHClientTunnelConnection;
begin
  try
    ServerChannel := ReadLength(@PByteArray(Buffer)[1], Size - 1);
    Orig := ReadString(@PByteArray(Buffer)[5], Size - 5);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  { Creating the channel }
  Found := false;
  if Assigned(FTunnelList) then
  begin
    for I := 0 to FTunnelList.Count - 1 do
      if (FTunnelList.Tunnels[I] is TElX11ForwardSSHTunnel) then
      begin
        ClientChannel := SSH1CreateServerClientChannel(ServerChannel, I, Conn);
        SSH1SendChannelOpenConfirmation(ServerChannel, ClientChannel);
        TElSSHClientTunnel(FTunnelList.Tunnels[I]).DoOpen(Conn);
        Found := true;
        Break;
      end;
  end;
  if not Found then
    SSH1SendChannelOpenFailure(ServerChannel);
  {$ifdef SECURE_BLACKBOX_DEBUG}
  DoDebug('X11 tunnel opened', SSH_DEBUG_LEVEL_ALL_EVENTS);
  {$endif}
end;

procedure TElSSHClient.SSH1ParseServerAgentOpen(Buffer : pointer; Size : longint);
var
  ClientChannel, ServerChannel : cardinal;
  Found : boolean;
  I : integer;
  Conn : TElSSHClientTunnelConnection;
begin
  if Size < 5 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  ServerChannel := ReadLength(@PByteArray(Buffer)[1], Size);
  { Creating the channel }
  Found := false;
  if Assigned(FTunnelList) then
  begin
    for I := 0 to FTunnelList.Count - 1 do
      if (FTunnelList.Tunnels[I] is TElAuthenticationAgentSSHTunnel) then
      begin
        ClientChannel := SSH1CreateServerClientChannel(ServerChannel, I, Conn);
        SSH1SendChannelOpenConfirmation(ServerChannel, ClientChannel);
        TElSSHClientTunnel(FTunnelList.Tunnels[I]).DoOpen(Conn);
        Found := true;
        Break;
      end;
  end;
  if not Found then
    SSH1SendChannelOpenFailure(ServerChannel);
  {$ifdef SECURE_BLACKBOX_DEBUG}
  DoDebug('Authentication agent tunnel opened', SSH_DEBUG_LEVEL_ALL_EVENTS);
  {$endif}
end;

procedure TElSSHClient.SSH1ParseServerDebug(Buffer : pointer; Size : longint);
var
  DebugStr : string;
begin
  try
    DebugStr := ReadString(@PByteArray(Buffer)[1], Size - 1);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  DoDataDebug(@DebugStr, Length(DebugStr));
end;

procedure TElSSHClient.SSH1SendOnTransportLayer(Size : longint);
var
  TotalLen,
  PaddingLen : integer;
  I : integer;
  J : Cardinal;
  CheckSum : integer;
  // S : string;
begin
  FSSH1ClientLastMessage := PByteArray(FOutBuffer)[16];
  { Compressing data if needed }
  if FUseCompression then
  begin
    I := Size;
    try
      SSH1CompressPacket(@PByteArray(FOutBuffer)[16], Size,
        @PByteArray(FOutBuffer)[16], I);
    except
      on E : Exception do
      begin
        DoError(ERROR_SSH_COMPRESSION_ERROR);
        CloseByError(E.Message);
        Exit;
      end;
    end;
    Size := I;
  end;
  { Counting padding }
  TotalLen := Size + 4;
  PaddingLen := 8 - (TotalLen mod 8);
  if FSSH1State = stEncrypted then
    for I := 0 to PaddingLen - 1 do
      PByteArray(FOutBuffer)[15 - I] := SBRndGenerate(256) //LRC4RandomByte(FRC4RandomCtx)
  else
    for I := 0 to PaddingLen - 1 do
      PByteArray(FOutBuffer)[15 - I] := 0;
  { Counting CRC32 sum }
  CheckSum := CRC32(@PByteArray(FOutBuffer)[16 - PaddingLen], Size + PaddingLen);
  Move(Checksum, J, 4);
  J := ((J shr 24) and $FF) or
              ((J shr 8) and $FF00) or
              ((J shl 8) and $FF0000) or
               (J shl 24 and $FF000000);
  Move(J, PByteArray(FOutBuffer)[16 + Size], 4);

  { Encrypting the data }
  if FSSH1State = stEncrypted then
  begin
    SSH1EncryptPacket(@PByteArray(FOutBuffer)[16 - PaddingLen],
      @PByteArray(FOutBuffer)[16 - PaddingLen], TotalLen + PaddingLen);
  end;
  { Sending the data }

  Move(TotalLen, J, 4);
  J := ((J shr 24) and $FF) or
              ((J shr 8) and $FF00) or
              ((J shl 8) and $FF0000) or
               (J shl 24 and $FF000000);

  Move(J, PByteArray(FOutBuffer)[16 - PaddingLen - 4], 4);
  DoSend(@PByteArray(FOutBuffer)[16 - PaddingLen - 4], TotalLen + PaddingLen + 4);
end;

procedure TElSSHClient.SSH1SendSessionKey;
var
  P : ^byte;
  SessionKey : array[0..31] of byte;
  I : Integer;
  Buffer, NextBuffer : array of byte;
begin
  for I := 0 to 15 do
    SessionKey[I] := FSSH1Params.SessionKey[I] xor FSSH1Params.SessionID[I];
  Move(FSSH1Params.SessionKey[16], SessionKey[16], 16);

  if LGreater(FSSH1HostRSAParams.Modulus, FSSH1ServerRSAParams.Modulus) then
  begin
    I := 4096;
    SetLength(Buffer, 4096);
    SSH1EncryptWithServerRSAKey(SessionKey, Buffer[0], 32, I);
    SetLength(Buffer, I);
    I := 4096;
    SetLength(NextBuffer, 4096);
    SSH1EncryptWithHostRSAKey(Buffer[0], NextBuffer[0], Length(Buffer), I);
    SetLength(NextBuffer, I);
  end
  else
  begin
    I := 4096;
    SetLength(Buffer, 4096);
    SSH1EncryptWithHostRSAKey(SessionKey, Buffer[0], 32, I);
    SetLength(Buffer, I);
    I := 4096;
    SetLength(NextBuffer, 4096);
    SSH1EncryptWithServerRSAKey(Buffer[0], NextBuffer[0], Length(Buffer), I);
    SetLength(NextBuffer, I);
  end;

  P := @PByteArray(FOutBuffer)[16];
  PByteArray(P)[0] := SSH1_MSG_SESSION_KEY;
  PByteArray(P)[1] := FSSH1Params.Cipher;
  Move(FServerCookie[0], PByteArray(P)[2], 8);
  PByteArray(P)[10] := (Length(NextBuffer) * 8) shr 8;
  PByteArray(P)[11] := (Length(NextBuffer) * 8) and $FF;
  Move(NextBuffer[0], PByteArray(P)[12], Length(NextBuffer));
  I := $03000000;
  Move(I, PByteArray(P)[10 + Length(NextBuffer) + 2], 4);

  SSH1SendOnTransportLayer(10 + Length(NextBuffer) + 2 + 4);
  FSSH1State := stEncrypted;
end;

procedure TElSSHClient.SSH1SendUser;
var
  J : Cardinal;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_USER;

  J := Length(FUserName);
  J := ((J shr 24) and $FF) or
              ((J shr 8) and $FF00) or
              ((J shl 8) and $FF0000) or
               (J shl 24 and $FF000000);

  Move(J, PByteArray(FOutBuffer)[17], 4);
  Move(FUserName[1], PByteArray(FOutBuffer)[21], Length(FUserName));
  SSH1SendOnTransportLayer(5 + Length(FUserName));
end;

procedure TElSSHClient.SSH1SendAuthPassword;
var
  J : Cardinal;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_AUTH_PASSWORD;
  J := Length(FPassword);
  J := ((J shr 24) and $FF) or
              ((J shr 8) and $FF00) or
              ((J shl 8) and $FF0000) or
               (J shl 24 and $FF000000);

  Move(J, PByteArray(FOutBuffer)[17], 4);
  Move(FPassword[1], PByteArray(FOutBuffer)[21], Length(FPassword));
  SSH1SendOnTransportLayer(5 + Length(FPassword));
end;

procedure TElSSHClient.SSH1SendAuthRSA(Modulus : pointer; ModulusSize : integer);
var
  S1 : string;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_AUTH_RSA;
//  S1 := WriteBitCount(Modulus, ModulusSize);
  SetLength(S1, 2);
  S1[1] := Chr(ModulusSize shr 5);
  S1[2] := Chr((ModulusSize shl 3) and $ff);
  Move(S1[1], PByteArray(FOutBuffer)[17], 2);
  Move(Modulus^, PByteArray(FOutBuffer)[19], ModulusSize);
  SSH1SendOnTransportLayer(3 + ModulusSize);
end;

procedure TElSSHClient.SSH1SendAuthRSAResponse(Buffer : pointer; Size : integer);
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_AUTH_RSA_RESPONSE;
  Move(Buffer^, PByteArray(FOutBuffer)[17], 16);
  SSH1SendOnTransportLayer(17);
end;

procedure TElSSHClient.SSH1SendAuthRhosts;
var
  S : string;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_AUTH_RHOSTS;
  S := WriteUINT32(Length(FUsername)) + FUsername;
  Move(S[1], PByteArray(FOutBuffer)[17], Length(S));
  SSH1SendOnTransportLayer(1 + Length(S));
end;

procedure TElSSHClient.SSH1SendAuthTIS;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_AUTH_TIS;
  SSH1SendOnTransportLayer(1);
end;

procedure TElSSHClient.SSH1SendAuthRhostsRSA(Modulus : pointer; ModulusSize : integer;
  Exponent : pointer; ExponentSize : integer);
var
  S : string;
  Tmp : string;
  I : integer;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_AUTH_RHOSTS_RSA;
  Tmp := WriteUINT32(Length(FClientUsername)) + FClientUsername;
  S := WriteBitCount(Modulus, ModulusSize);
  I := (Ord(S[1]) shl 8) or Ord(S[2]);
  Tmp := Tmp + WriteUINT32(I) + WriteBitCount(Exponent, ExponentSize);
  Move(Tmp[1], PByteArray(FOutBuffer)[17], Length(Tmp));
  Move(Exponent^, PByteArray(FOutBuffer)[17 + Length(Tmp)], ExponentSize);
  Move(S[1], PByteArray(FOutBuffer)[17 + Length(Tmp) + ExponentSize], Length(S));
  Move(Modulus^, PByteArray(FOutBuffer)[17 + Length(Tmp) + ExponentSize + 2], ModulusSize);
  SSH1SendOnTransportLayer(1 + Length(Tmp) + ExponentSize + 2 + ModulusSize);
end;

procedure TElSSHClient.SSH1SendRequestPty;
var
  PtyName, Modes : string;
  HR, WC, WP, HP : Cardinal;
  TermInfo : TElTerminalInfo;
begin
  TermInfo := nil;
  if (FSSH1SessionType = SSH1_SESS_TYPE_SHELL) and (FSSH1ShellTunnelIndex >= 0) then
    TermInfo := TElShellSSHTunnel(FTunnelList.Tunnels[FSSH1ShellTunnelIndex]).TerminalInfo
  else if (FSSH1SessionType = SSH1_SESS_TYPE_COMMAND) and (FSSH1ShellTunnelIndex >= 0) then
    TermInfo := TElCommandSSHTunnel(FTunnelList.Tunnels[FSSH1ShellTunnelIndex]).TerminalInfo;
  if Assigned(TermInfo) then
    PtyName := TermInfo.TerminalType
  else
    PtyName := 'vt100';
  PtyName := WriteUINT32(Length(PtyName)) + PtyName;
  HR := $18000000; // WriteUINT32(24);
  WC := $50000000; // WriteUINT32(80);
  WP := 0;
  HP := 0;
  Modes := SSH1EncodePtyModes(TermInfo);
  PByteArray(FOutBuffer)[16] := SSH1_MSG_REQUEST_PTY;
  Move(PtyName[1], PByteArray(FOutBuffer)[17], Length(PtyName));
  Move(HR, PByteArray(FOutBuffer)[17 + Length(PtyName)], 4);
  Move(WC, PByteArray(FOutBuffer)[21 + Length(PtyName)], 4);
  Move(WP, PByteArray(FOutBuffer)[25 + Length(PtyName)], 4);
  Move(HP, PByteArray(FOutBuffer)[29 + Length(PtyName)], 4);
  Move(Modes[1], PByteArray(FOutBuffer)[33 + Length(PtyName)], Length(Modes));
  SSH1SendOnTransportLayer(17 + Length(PtyName) + Length(Modes));
end;

procedure TElSSHClient.SSH1SendExecShell;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_EXEC_SHELL;
  SSH1SendOnTransportLayer(1);
end;

procedure TElSSHClient.SSH1SendRequestCompression;
var
  S : string;
begin
  S := WriteUINT32(FCompressionLevel);
  PByteArray(FOutBuffer)[16] := SSH1_MSG_REQUEST_COMPRESSION;
  Move(S[1], PByteArray(FOutBuffer)[17], 4);
  SSH1SendOnTransportLayer(5);
end;

procedure TElSSHClient.SSH1SendExecCmd(Cmd : string);
var
  J : Cardinal;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_EXEC_CMD;
  J := Length(Cmd);
  J := ((J shr 24) and $FF) or
       ((J shr 8) and $FF00) or
       ((J shl 8) and $FF0000) or
       (J shl 24 and $FF000000);

  Move(J, PByteArray(FOutBuffer)[17], 4);
  Move(Cmd[1], PByteArray(FOutBuffer)[21], Length(Cmd));
  SSH1SendOnTransportLayer(5 + Length(Cmd));
end;

procedure TElSSHClient.SSH1SendExitConfirmation;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_EXIT_CONFIRMATION;
  SSH1SendOnTransportLayer(1);
end;

procedure TElSSHClient.SSH1SendStdinData(Buffer : pointer; Size : integer);
var
  J : Cardinal;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_STDIN_DATA;
  J := Size;
  J := ((J shr 24) and $FF) or
       ((J shr 8) and $FF00) or
       ((J shl 8) and $FF0000) or
       (J shl 24 and $FF000000);

  Move(J, PByteArray(FOutBuffer)[17], 4);
  Move(Buffer^, PByteArray(FOutBuffer)[21], Size);
  SSH1SendOnTransportLayer(5 + Size);
end;

procedure TElSSHClient.SSH1SendEOF;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_EOF;
  SSH1SendOnTransportLayer(1);
end;

procedure TElSSHClient.SSH1SendDisconnect(const ReasonLine : string);
var
  J : Cardinal;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_DISCONNECT;
  J := Length(ReasonLine);
  J := ((J shr 24) and $FF) or
       ((J shr 8) and $FF00) or
       ((J shl 8) and $FF0000) or
       (J shl 24 and $FF000000);
  Move(J, PByteArray(FOutBuffer)[17], 4);
  Move(ReasonLine[1], PByteArray(FOutBuffer)[21], Length(ReasonLine));
  SSH1SendOnTransportLayer(5 + Length(ReasonLine));
end;

procedure TElSSHClient.SSH1SendPortForwardRequest(RemotePort : word; Host : string;
  Port : word);
var
  S : string;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_PORT_FORWARD_REQUEST;
  S := WriteUINT32(RemotePort) + WriteString(Host) + WriteUINT32(Port);
  Move(S[1], PByteArray(FOutBuffer)[17], Length(S));
  SSH1SendOnTransportLayer(1 + Length(S));
end;

procedure TElSSHClient.SSH1SendPortOpen(LocalChannel : cardinal; Host : string;
  Port : word; OriginatorString : string);
var
  S : string;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_PORT_OPEN;
  S := WriteUINT32(LocalChannel) + WriteString(Host) +
    WriteUINT32(Port) + WriteString(OriginatorString);
  Move(S[1], PByteArray(FOutBuffer)[17], Length(S));
  SSH1SendOnTransportLayer(1 + Length(S));
end;

procedure TElSSHClient.SSH1SendAgentRequestForwarding;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_AGENT_REQUEST_FORWARDING;
  SSH1SendOnTransportLayer(1);
end;

procedure TElSSHClient.SSH1SendChannelOpenConfirmation(ServerChannel : cardinal;
  ClientChannel : cardinal);
var
  S : string;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_CHANNEL_OPEN_CONFIRMATION;
  S := WriteUINT32(ServerChannel) + WriteUINT32(ClientChannel);
  Move(S[1], PByteArray(FOutBuffer)[17], Length(S));
  SSH1SendOnTransportLayer(1 + Length(S));
end;

procedure TElSSHClient.SSH1SendChannelOpenFailure(ServerChannel : cardinal);
var
  J : Cardinal;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_CHANNEL_OPEN_FAILURE;
  J := ServerChannel;
  J := ((J shr 24) and $FF) or
       ((J shr 8) and $FF00) or
       ((J shl 8) and $FF0000) or
       (J shl 24 and $FF000000);

  Move(J, PByteArray(FOutBuffer)[17], 4);
  SSH1SendOnTransportLayer(5);
end;

procedure TElSSHClient.SSH1SendChannelData(ServerChannel : cardinal; Buffer : pointer;
  Size : longint);
var
  S : string;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_CHANNEL_DATA;
  S := WriteUINT32(ServerChannel) + WriteUINT32(Size);
  Move(S[1], PByteArray(FOutBuffer)[17], 8);
  Move(Buffer^, PByteArray(FOutBuffer)[25], Size);
  SSH1SendOnTransportLayer(9 + Size);
end;

procedure TElSSHClient.SSH1SendChannelClose(ServerChannel : cardinal);
var
  J : Cardinal;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_CHANNEL_CLOSE;

  J := ServerChannel;
  J := ((J shr 24) and $FF) or
       ((J shr 8) and $FF00) or
       ((J shl 8) and $FF0000) or
       (J shl 24 and $FF000000);

  Move(J, PByteArray(FOutBuffer)[17], 4);
  SSH1SendOnTransportLayer(5);
end;

procedure TElSSHClient.SSH1SendChannelCloseConfirmation(ServerChannel : cardinal);
var
  J : Cardinal;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_CHANNEL_CLOSE_CONFIRMATION;

  J := ServerChannel;
  J := ((J shr 24) and $FF) or
       ((J shr 8) and $FF00) or
       ((J shl 8) and $FF0000) or
       (J shl 24 and $FF000000);

  Move(J, PByteArray(FOutBuffer)[17], 4);
  SSH1SendOnTransportLayer(5);
end;

procedure TElSSHClient.SSH1SendTunnelData(RemoteChannel : cardinal; Buffer : pointer;
  Size : longint);
begin
  if RemoteChannel = $FFFFFFFF then
    SSH1SendStdinData(Buffer, Size)
  else
    SSH1SendChannelData(RemoteChannel, Buffer, Size);
end;

procedure TElSSHClient.SSH1SendX11RequestForwarding;
var
  S, Tmp : string;
begin
  PByteArray(FOutBuffer)[16] := SSH1_MSG_X11_REQUEST_FORWARDING;
  S := 'MIT-MAGIC-COOKIE-1';
  Tmp := WriteUINT32(Length(S)) + S;
  S := 'abcd';
  Tmp := Tmp + WriteUINT32(Length(S)) + S + WriteUINT32(150);
  Move(Tmp[1], PByteArray(FOutBuffer)[17], Length(Tmp));
  SSH1SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH1StartClientAuthentication;
begin
  SSH1ContinueClientAuthentication(FSSH1Params.Auth);
end;

procedure TElSSHClient.SSH1ContinueClientAuthentication(AuthMask : cardinal);
begin
  if (SSH_AUTH_TYPE_RHOSTS and AuthMask) > 0 then
  begin
    FLastAuthMask := AuthMask and not (SSH_AUTH_TYPE_RHOSTS);
    SSH1SendAuthRhosts;
  end
  else if (SSH_AUTH_TYPE_PUBLICKEY and AuthMask) > 0 then
  begin
    FCurrentRSAAuth := SSH_AUTH_TYPE_PUBLICKEY;
    FLastAuthMask := AuthMask and not (SSH_AUTH_TYPE_PUBLICKEY);
    SSH1EstablishAuthRSA(0);
  end
  else if (SSH_AUTH_TYPE_PASSWORD and AuthMask) > 0 then
  begin
    FLastAuthMask := AuthMask and not (SSH_AUTH_TYPE_PASSWORD);
    SSH1SendAuthPassword;
  end
  else if (SSH_AUTH_TYPE_HOSTBASED and AuthMask) > 0 then
  begin
    FCurrentRSAAuth := SSH_AUTH_TYPE_HOSTBASED;
    FLastAuthMask := AuthMask and not (SSH_AUTH_TYPE_HOSTBASED);
    SSH1EstablishAuthRhostsRSA(0);
  end
  else
  begin
    DoError(ERROR_SSH_AUTHENTICATION_FAILED);
    CloseByError(SDisconnectAuthenticationFailed);
  end;
//  FLastAuthMask := AuthMask;
end;

procedure TElSSHClient.SSH1LoginCompleted;
var
  I : integer;
begin
  if Assigned(FOnAuthenticationSuccess) then
    FOnAuthenticationSuccess(Self);
  FSSH1ShellTunnelIndex := -1;
  FSSH1SessionType := SSH1_SESS_TYPE_NONE;
  if Assigned(FTunnelList) then
  begin
    for I := 0 to FTunnelList.Count - 1 do
    begin
      if FTunnelList.Tunnels[I] is TElShellSSHTunnel then
      begin
        FSSH1SessionType := SSH1_SESS_TYPE_SHELL;
        FSSH1ShellTunnelIndex := I;
        Break;
      end
      else if FTunnelList.Tunnels[I] is TElCommandSSHTunnel then
      begin
        FSSH1SessionType := SSH1_SESS_TYPE_COMMAND;
        FSSH1ShellTunnelIndex := I;
        Break;
      end;
    end;
  end;
  if FRequestCompression then
    SSH1SendRequestCompression
  else
  begin
    if (FSSH1SessionType = SSH1_SESS_TYPE_COMMAND) or (FSSH1SessionType = SSH1_SESS_TYPE_SHELL) then
      SSH1SendRequestPty
    else
      DoOpenConnection;
  end
end;

procedure TElSSHClient.SSH1PtyAllocated;
begin
  SSH1EstablishTunneling(0);
end;

procedure TElSSHClient.SSH1ShellAllocated;
begin
  DoOpenConnection;
end;

procedure TElSSHClient.SSH1CompressionAllowed(Allow : boolean);
begin
  FUseCompression := Allow;
  if Allow then
  begin
    {$ifndef DONT_USE_ZLIB}
    SBZlib.InitializeCompression(FSSH1CompressionCtx, FCompressionLevel);
    SBZlib.InitializeDecompression(FSSH1DecompressionCtx);
    FCompressionAlgorithmCS := SSH_CA_ZLIB;
    FCompressionAlgorithmSC := SSH_CA_ZLIB;
    {$endif}
  end
  else
  begin
    FCompressionAlgorithmCS := SSH_CA_NONE;
    FCompressionAlgorithmSC := SSH_CA_NONE;
  end;
  SSH1SendRequestPty;
end;

procedure TElSSHClient.SSH1AuthRSAAllowed(Allow : boolean);
begin
  if not Allow then
    SSH1EstablishAuthRSA(FLastKeyIndex + 1);
end;

procedure TElSSHClient.SSH1AuthRhostsRSAAllowed(Allow : boolean);
begin
  if not Allow then
    SSH1EstablishAuthRhostsRSA(FLastKeyIndex + 1);
end;

procedure TElSSHClient.SSH1AuthRhostsAllowed(Allow : boolean);
begin
  if not Allow then
  begin
    DoAuthenticationFailed(SSH_AUTH_TYPE_RHOSTS);
    SSH1ContinueClientAuthentication(FLastAuthMask);
  end;
end;

procedure TElSSHClient.SSH1AuthPasswordAllowed(Allow : boolean);
begin
  if not Allow then
  begin
    DoAuthenticationFailed(SSH_AUTH_TYPE_PASSWORD);
    SSH1ContinueClientAuthentication(FLastAuthMask);
  end;
end;

procedure TElSSHClient.SSH1AuthTISAllowed(Allow : boolean);
begin
  if Allow then
  begin

  end
  else
  begin
    // sending other type of auth request
  end;
end;

procedure TElSSHClient.SSH1PortForwardAllowed(Allow : boolean);
begin
  if not Allow then
    TElSSHClientTunnel(FTunnelList.Tunnels[FLastTunnelIndex]).DoError(SSH_TUNNEL_ERROR_FORWARD_DISALLOWED, nil);
  SSH1EstablishTunneling(FLastTunnelIndex + 1);
end;

procedure TElSSHClient.SSH1X11ForwardAllowed(Allow : boolean);
begin
  if not Allow then
    TElSSHClientTunnel(FTunnelList.Tunnels[FLastTunnelIndex]).DoError(SSH_TUNNEL_ERROR_FORWARD_DISALLOWED, nil);
  SSH1EstablishTunneling(FLastTunnelIndex + 1);
end;

procedure TElSSHClient.SSH1AgentForwardAllowed(Allow : boolean);
begin
  if not Allow then
    TElSSHClientTunnel(FTunnelList.Tunnels[FLastTunnelIndex]).DoError(SSH_TUNNEL_ERROR_FORWARD_DISALLOWED, nil);
  SSH1EstablishTunneling(FLastTunnelIndex + 1);
end;

procedure TElSSHClient.SSH1ExecuteCommand(Command : string);
begin
  SSH1SendExecCmd(Command);
end;

procedure TElSSHClient.SSH1EstablishTunneling(TunnelIndex : integer);
var
  Tunnel : TElCustomSSHTunnel;
  Conn : TElSSHClientTunnelConnection;
begin
  FLastTunnelIndex := TunnelIndex;
  if (FTunnelList <> nil) then
  begin
    if TunnelIndex < FTunnelList.Count then
    begin
      Tunnel := FTunnelList.Tunnels[TunnelIndex];
      if Tunnel is TElRemotePortForwardSSHTunnel then
        { insert here check-up for proper port and host values }
        SSH1SendPortForwardRequest(TElRemotePortForwardSSHTunnel(Tunnel).Port,
          TElRemotePortForwardSSHTunnel(Tunnel).ToHost,
          TElRemotePortForwardSSHTunnel(Tunnel).ToPort)
      else if Tunnel is TElX11ForwardSSHTunnel then
        SSH1SendX11RequestForwarding
      else if Tunnel is TElAuthenticationAgentSSHTunnel then
        SSH1SendAgentRequestForwarding
      else
        SSH1EstablishTunneling(TunnelIndex + 1);
      Exit;
    end
  end;
  if {FRequestShell}FSSH1SessionType = SSH1_SESS_TYPE_SHELL then
  begin
    SSH1SendExecShell;
    Conn := TElSSHClientTunnelConnection.Create;
    Conn.Tunnel := FTunnelList.Tunnels[FSSH1ShellTunnelIndex];
    Conn.SetParent(Self);
    //Conn.FParentIsClient := true;
    Conn.SetLocalChannel($FFFFFFFF);
    Conn.SetRemoteChannel($FFFFFFFF);
    Conn.SetInternalType(SSH_CONN_INTERNAL_TYPE_SHELL);
    FChannels.Add(Conn);
    TElSSHClientTunnel(Conn.Tunnel).ConnectionsList.Add(Conn);
    TElSSHClientTunnel(FTunnelList.Tunnels[FSSH1ShellTunnelIndex]).DoOpen(Conn);
    DoOpenConnection;
  end
  else if FSSH1SessionType = SSH1_SESS_TYPE_COMMAND then
  begin
    SSH1SendExecCmd(TElCommandSSHTunnel(FTunnelList.Tunnels[FSSH1ShellTunnelIndex]).Command);
    Conn := TElSSHClientTunnelConnection.Create;
    Conn.Tunnel := FTunnelList.Tunnels[FSSH1ShellTunnelIndex];
    Conn.SetParent(Self);
    Conn.SetLocalChannel($FFFFFFFF);
    Conn.SetRemoteChannel($FFFFFFFF);
    Conn.SetInternalType(SSH_CONN_INTERNAL_TYPE_COMMAND);
    FChannels.Add(Conn);
    Conn.Tunnel.AddConnection(Conn);
    TElSSHClientTunnel(FTunnelList.Tunnels[FSSH1ShellTunnelIndex]).DoOpen(Conn);
    DoOpenConnection;
  end
  else
    DoOpenConnection;
end;

procedure TElSSHClient.SSH1EstablishAuthRSA(KeyIndex : integer);
var
  Key : TElSSHKey;
begin
  FLastKeyIndex := KeyIndex;
  if (FKeyStorage <> nil) and (KeyIndex < FKeyStorage.Count) then
  begin
    Key := FKeyStorage.Keys[KeyIndex];
    if not Assigned(Key) then
    begin
      SSH1EstablishAuthRSA(KeyIndex + 1);
      Exit;
    end;
    if Key.Algorithm = ALGORITHM_RSA then
      SSH1SendAuthRSA(@Key.RSAPublicModulus[1], Length(Key.RSAPublicModulus))
    else
      SSH1AuthRSAAllowed(false);
  end
  else
  begin
    DoAuthenticationFailed(SSH_AUTH_TYPE_PUBLICKEY);
    SSH1ContinueClientAuthentication(FLastAuthMask);
  end;
end;

procedure TElSSHClient.SSH1EstablishAuthRhostsRSA(KeyIndex : integer);
var
  Key : TElSSHKey;
begin
  FLastKeyIndex := KeyIndex;
  if (FKeyStorage <> nil) and (KeyIndex < FKeyStorage.Count) then
  begin
    Key := FKeyStorage.Keys[KeyIndex];
    if not Assigned(Key) then
    begin
      SSH1EstablishAuthRhostsRSA(KeyIndex + 1);
      Exit;
    end;
    if Key.Algorithm = ALGORITHM_RSA then
      SSH1SendAuthRhostsRSA(@Key.RSAPublicModulus[1], Length(Key.RSAPublicModulus),
        @Key.RSAPublicExponent[1], Length(Key.RSAPublicExponent))
    else
      SSH1AuthRhostsRSAAllowed(false);
  end
  else
  begin
    DoAuthenticationFailed(SSH_AUTH_TYPE_HOSTBASED);
    SSH1ContinueClientAuthentication(FLastAuthMask);
  end;
end;

procedure TElSSHClient.SSH1CloseTunnel(Tunnel : TElSSHClientTunnelConnection);
begin
  Tunnel.SetLocalChannelClosed(true);
  SSH1SendChannelClose(Tunnel.GetRemoteChannel);
end;

procedure TElSSHClient.SSH1EncryptWithServerRSAKey(const InBuffer; var OutBuffer;
  const InSize : integer; var OutSize : integer);
var
  I, Sz : integer;
  LongData, LongRes : PLInt;
  Buffer : array of byte;
begin
  if OutSize < FSSH1ServerRSAParams.Modulus.Length * 4 then
    OutSize := FSSH1ServerRSAParams.Modulus.Length * 4
  else
  begin
    LCreate(LongRes);
    LCreate(LongData);
    Sz := FSSH1ServerRSAParams.Modulus.Length * 4;
    SetLength(Buffer, Sz);
    Buffer[0] := 0;
    Buffer[1] := 2;
    for I := 2 to Sz - InSize - 2 do
      Buffer[I] := SBRndGenerate($FE) + 1;
    Buffer[Sz - InSize - 1] := 0;
    Move(InBuffer, Buffer[Sz - InSize], InSize);
    PointerToLInt(LongData, @Buffer[0], Length(Buffer));
    LSSHModPower(LongData, FSSH1ServerRSAParams.PublicExponent, FSSH1ServerRSAParams.Modulus,
      LongRes);///
    LIntToPointer(LongRes, @Buffer[0], OutSize);
    Move(Buffer[0], byte(OutBuffer), OutSize);
    LDestroy(LongRes);
    LDestroy(LongData);
  end;
end;

procedure TElSSHClient.SSH1EncryptWithHostRSAKey(const InBuffer; var OutBuffer;
  const InSize : integer; var OutSize : integer);
var
  I, Sz : integer;
  LongData, LongRes : PLInt;
  Buffer : array of byte;
begin
  if OutSize < FSSH1HostRSAParams.Modulus.Length * 4 then
    OutSize := FSSH1HostRSAParams.Modulus.Length * 4
  else
  begin
    LCreate(LongRes);
    LCreate(LongData);
    Sz := FSSH1HostRSAParams.Modulus.Length * 4;
    SetLength(Buffer, Sz);
    Buffer[0] := 0;
    Buffer[1] := 2;
    for I := 2 to Sz - InSize - 2 do
      Buffer[I] := SBRndGenerate($FE) + 1;
    Buffer[Sz - InSize - 1] := 0;
    Move(InBuffer, Buffer[Sz - InSize], InSize);
    PointerToLInt(LongData, @Buffer[0], Length(Buffer));
    LSSHModPower(LongData, FSSH1HostRSAParams.PublicExponent, FSSH1HostRSAParams.Modulus,
      LongRes); ///
    LIntToPointer(LongRes, @Buffer[0], OutSize);
    Move(Buffer[0], byte(OutBuffer), OutSize);
    LDestroy(LongRes);
    LDestroy(LongData);
  end;
end;

procedure TElSSHClient.SSH1GenerateSessionKey;
var
  I : integer;
  {$ifndef SBB_NO_DES}
  DesKey : TDESKey;
  DesIV : TDESBuffer;
  TripleDesIV : TSSH3DESKey;
  TripleDesKey : TSSH3DESKey;
  {$endif}
  BlowFishIV : TSBBlowfishBuffer;
  BlowFishKey : TSBBlowfishKey;
  {$ifndef SBB_NO_IDEA}
  IdeaKey : TIDEAKey;
  IdeaIV : TIDEABuffer;
  {$endif}
  {$ifndef SBB_NO_RC4}
  RC4Key : TRC4Key;
  {$endif}
begin
  case FSSH1Params.Cipher of
    {$ifndef SBB_NO_DES}
    SSH1_CIPHER_DES : FEncryptionAlgorithmCS := SSH_EA_DES;
    SSH1_CIPHER_3DES : FEncryptionAlgorithmCS := SSH_EA_3DES;
    {$endif}
    SSH1_CIPHER_BLOWFISH : FEncryptionAlgorithmCS := SSH_EA_BLOWFISH;
    {$ifndef SBB_NO_RC4}
    SSH1_CIPHER_RC4 : FEncryptionAlgorithmCS := SSH_EA_ARCFOUR;
    {$endif}
    {$ifndef SBB_NO_IDEA}
    SSH1_CIPHER_IDEA: 
      if IdeaEnabled then 
        FEncryptionAlgorithmCS := SSH_EA_IDEA;
    {$endif}
    SSH1_CIPHER_NONE: FEncryptionAlgorithmCS := SSH_EA_NONE;
  end;
  FEncryptionAlgorithmSC := FEncryptionAlgorithmCS;
  FMACAlgorithmCS := SSH_MA_NONE;
  FMACAlgorithmSC := SSH_MA_NONE;
  FPublicKeyAlgorithm := SSH_PK_RSA;
  for I := 0 to 31 do
    FSSH1Params.SessionKey[I] := SBRndGenerate(256); //LRC4RandomByte(FRC4RandomCtx);
  case FSSH1Params.Cipher of
    {$ifndef SBB_NO_DES}
    SSH1_CIPHER_DES :
    begin
      FillChar(DesIV[0], 8, 0);
      Move(FSSH1Params.SessionKey[0], DesKey[0], 8);
      SBDES.InitializeEncryptionCBC(FSSH1DesOutputCtx, DesKey, DesIV);
      SBDES.InitializeDecryptionCBC(FSSH1DesInputCtx, DesKey, DesIV);
    end;
    SSH1_CIPHER_3DES :
    begin
      FillChar(TripleDesIV[0], 24, 0);
      Move(FSSH1Params.SessionKey[0], TripleDesKey[0], 24);
      SBSSH3DES.InitializeDecryptionCBC(FSSH13DesInputCtx, TripleDesKey, TripleDesIV);
      SBSSH3DES.InitializeEncryptionCBC(FSSH13DesOutputCtx, TripleDesKey, TripleDesIV);
    end;
    {$endif}
    SSH1_CIPHER_BLOWFISH :
    begin
      FillChar(BlowfishIV[0], 8, 0);
      SetLength(BlowfishKey, 32);
      Move(FSSH1Params.SessionKey[0], BlowfishKey[0], 32);
      SBBlowFish.InitializeCBC(FSSH1BlowfishInputCtx, BlowfishKey, BlowfishIV);
      SBBlowFish.InitializeCBC(FSSH1BlowfishOutputCtx, BlowfishKey, BlowfishIV);
    end;
    {$ifndef SBB_NO_IDEA}
    SSH1_CIPHER_IDEA :
    if IdeaEnabled then  
    begin
      FillChar(IdeaIV[0], 8, 0);
      Move(FSSH1Params.SessionKey[0], IdeaKey[0], 16);
      SBIDEA.InitializeEncryptionCFB(FSSH1IdeaInputCtx, IdeaKey, IdeaIV);
      SBIDEA.InitializeDecryptionCFB(FSSH1IdeaOutputCtx, IdeaKey, IdeaIV);
    end;
    {$endif}
    {$ifndef SBB_NO_RC4}
    SSH1_CIPHER_RC4 :
    begin
      SetLength(RC4Key, 16);
      Move(FSSH1Params.SessionKey[0], RC4Key[0], 16);
      SBRC4.Initialize(FSSH1RC4InputCtx, RC4Key);
      Move(FSSH1Params.SessionKey[16], RC4Key[0], 16);
      SBRC4.Initialize(FSSH1RC4OutputCtx, RC4Key);
    end;
    {$endif}
  end;
end;

procedure TElSSHClient.SSH1DecryptPacket(InBuffer : pointer; OutBuffer : pointer;
  Size : integer);
var
  OutSize : cardinal;
begin
  OutSize := Size;
  {$ifdef CLX_USED}
  if FSSH1Params.Cipher = SSH1_CIPHER_NONE then
  begin
    Move(InBuffer^, OutBuffer^, Size);
  end;
  {$endif}
  case FSSH1Params.Cipher of
    {$ifndef SBB_NO_DES}
    SSH1_CIPHER_DES :
    begin
      SBDES.DecryptCBC(FSSH1DesInputCtx, InBuffer, Size, OutBuffer, OutSize);
    end;
    SSH1_CIPHER_3DES :
    begin
      SBSSH3DES.DecryptCBC(FSSH13DesInputCtx, InBuffer, Size, OutBuffer, OutSize);
    end;
    {$endif}
    SSH1_CIPHER_BLOWFISH:
    begin
      SBBlowFish.DecryptCBCSwap(FSSH1BlowFishInputCtx, InBuffer, Size, OutBuffer, OutSize);
    end;
    {$ifndef SBB_NO_RC4}
    SSH1_CIPHER_RC4:
    begin
      SBRC4.Decrypt(FSSH1RC4InputCtx, InBuffer, OutBuffer, Size);
    end;
    {$endif}
    {$ifndef SBB_NO_IDEA}
    SSH1_CIPHER_IDEA:
    if IdeaEnabled then 
    begin
      SBIDEA.DecryptCFB(FSSH1IdeaInputCtx, InBuffer, Size, OutBuffer, OutSize);
    end;
    {$endif}
    SSH1_CIPHER_NONE:
    begin
      {$ifndef CLX_USED}
      Move(InBuffer^, OutBuffer^, Size);
      {$endif}
    end;
  end;
end;

procedure TElSSHClient.SSH1EncryptPacket(InBuffer : pointer; OutBuffer : pointer;
  Size : integer);
var
  OutSize : cardinal;
begin
  OutSize := Size;
  {$ifdef CLX_USED}
  if FSSH1Params.Cipher = SSH1_CIPHER_NONE then
  begin
    Move(InBuffer^, OutBuffer^, Size);
  end;
  {$endif}
  case FSSH1Params.Cipher of
    {$ifndef SBB_NO_DES}
    SSH1_CIPHER_DES:
    begin
      SBDES.EncryptCBC(FSSH1DesOutputCtx, InBuffer, Size, OutBuffer, OutSize);
    end;
    SSH1_CIPHER_3DES :
    begin
      SBSSH3DES.EncryptCBC(FSSH13DesOutputCtx, InBuffer, Size, OutBuffer, OutSize);
    end;
    {$endif}
    SSH1_CIPHER_BLOWFISH :
    begin
      SBBlowFish.EncryptCBCSwap(FSSH1BlowFishOutputCtx, InBuffer, Size, OutBuffer, OutSize);
    end;
    {$ifndef SBB_NO_RC4}
    SSH1_CIPHER_RC4 :
    begin
      SBRC4.Encrypt(FSSH1RC4OutputCtx, InBuffer, OutBuffer, Size);
    end;
    {$endif}
    {$ifndef SBB_NO_IDEA}
    SSH1_CIPHER_IDEA :
    if IdeaEnabled then 
    begin
      SBIDEA.EncryptCFB(FSSH1IdeaOutputCtx, InBuffer, Size, OutBuffer, OutSize);
    end;
    {$endif}
    SSH1_CIPHER_NONE:
    begin
      {$ifndef CLX_USED}
      Move(InBuffer^, OutBuffer^, Size);
      {$endif}
    end;
  end;
end;

procedure TElSSHClient.SSH1CompressPacket(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer);
var
  Buf : array of byte;
  Sz : cardinal;
begin
  Sz := $FF + InSize;
  SetLength(Buf, Sz);
  {$ifndef DONT_USE_ZLIB}
  SBZlib.Compress(FSSH1CompressionCtx, InBuffer, InSize, @Buf[0], Sz);
  {$endif}
  Move(Buf[0], OutBuffer^, Sz);
  OutSize := Sz;
end;

procedure TElSSHClient.SSH1DecompressPacket(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer);
var
  Sz : cardinal;
  Buf : ByteArray;
begin
  Sz := $FF + InSize * 10;
  SetLength(Buf, Sz);
  {$ifndef DONT_USE_ZLIB}
  SBZlib.Decompress(FSSH1DecompressionCtx, InBuffer, InSize, @Buf[0], Sz);
  {$endif}
  Move(Buf[0], OutBuffer^, Sz);
  OutSize := Sz;
end;

function TElSSHClient.SSH1EncodePtyModes(TerminalInfo : TElTerminalInfo) : string;
var
  I : integer;
  C : byte;
begin
  Result := '';
  if TerminalInfo = nil then
  begin
    for I := Low(PtyCodesFirsts) to High(PtyCodesFirsts) do
      Result := Result + Chr(PtyCodesFirsts[I]) + Chr(PtyCodesSeconds[I]);
    Result := Result + Chr(192) + WriteUINT32(38400);
    Result := Result + Chr(193) + WriteUINT32(38400);
    Result := Result + #0;
  end
  else
  begin
    for I := OPCODE_FIRST to OPCODE_LAST do
    begin
      try
        C := TerminalInfo.OpCodes[I];
        Result := Result + Chr(I) + Chr(C);
      except
      end;
    end;
    Result := Result + Chr(192) + WriteUINT32(38400);
    Result := Result + Chr(193) + WriteUINT32(38400);
    Result := Result + #0;
  end;
end;

function TElSSHClient.SSH1ChooseCipher(Ciphers : cardinal) : cardinal;
var
  Bits : array[0..7] of boolean;
  I : integer;
begin
  for I := 0 to 7 do
    Bits[I] := ((Ciphers shr I) and 1) = 1;
  if (Bits[SSH1_CIPHER_3DES]) and (FEncryptionAlgorithms[SSH_EA_3DES]) then
    Result := SSH1_CIPHER_3DES
  else if (Bits[SSH1_CIPHER_BLOWFISH]) and (FEncryptionAlgorithms[SSH_EA_BLOWFISH]) then
    Result := SSH1_CIPHER_BLOWFISH
  else if (Bits[SSH1_CIPHER_DES]) and (FEncryptionAlgorithms[SSH_EA_DES]) then
    Result := SSH1_CIPHER_DES
  else if (Bits[SSH1_CIPHER_RC4]) and (FEncryptionAlgorithms[SSH_EA_ARCFOUR]) then
    Result := SSH1_CIPHER_RC4
  else if (Bits[SSH1_CIPHER_IDEA]) and (FEncryptionAlgorithms[SSH_EA_IDEA]) then
    Result := SSH1_CIPHER_IDEA
  else if (Bits[SSH1_CIPHER_NONE]) and (FEncryptionAlgorithms[SSH_EA_NONE]) then
    Result := SSH1_CIPHER_NONE
  else
  begin
    Result := SSH1_CIPHER_NONE;
    DoError(ERROR_SSH_UNSUPPORTED_CIPHER);
    CloseByError(SDisconnectUnsupportedCipher);
  end;
end;

function TElSSHClient.SSH1ChooseAuth(Ciphers : integer): Integer;
var
  Bits : array[0..7] of boolean;
  I : integer;
begin
  for I := 0 to 7 do
    Bits[I] := ((Ciphers shr I) and 1) = 1;
  Result := 0;
  if (Bits[SSH1_AUTH_PASSWORD]) and (FAuthenticationTypes and SSH_AUTH_TYPE_PASSWORD > 0) then
    Result := Result or SSH_AUTH_TYPE_PASSWORD;
  if (Bits[SSH1_AUTH_RSA]) and (FAuthenticationTypes and SSH_AUTH_TYPE_PUBLICKEY > 0) then
    Result := Result or SSH_AUTH_TYPE_PUBLICKEY;
  if (Bits[SSH1_AUTH_RHOSTS_RSA]) and (FAuthenticationTypes and SSH_AUTH_TYPE_HOSTBASED > 0) then
    Result := Result or SSH_AUTH_TYPE_HOSTBASED;
  if (Bits[SSH1_AUTH_RHOSTS]) and (FAuthenticationTypes and SSH_AUTH_TYPE_RHOSTS > 0) then
    Result := Result or SSH_AUTH_TYPE_RHOSTS;
  if Result = 0 then
  begin
    DoError(ERROR_SSH_UNSUPPORTED_AUTH_TYPE);
    CloseByError(SDisconnectUnsupportedAuthType);
  end
end;

procedure TElSSHClient.SSH1CloseChannel(Channel : cardinal; Client : boolean);
var
  Conn : TElSSHClientTunnelConnection;
begin
// Client is True when we close the channel and false when server does
  if Client then
    Conn := SSH1GetTunnelConnectionByLocalChannel(Channel)
  else
    Conn := SSH1GetTunnelConnectionByRemoteChannel(Channel);
  if Conn = nil then Exit;
  
  if (Client and Conn.GetRemoteChannelClosed) or
     (not Client and Conn.GetLocalChannelClosed) then
  begin
    FChannels.Remove(Conn);
    TElSSHClientTunnel(Conn.Tunnel).ConnectionsList.Remove(Conn);
    FreeAndNil(Conn);
  end
  else
  if Client and not Conn.GetRemoteChannelClosed then
  begin
    Conn.SetLocalChannelClosed(true);
  end
  else
  if not Client and not Conn.LocalChannelClosed then
  begin
    Conn.RemoteChannelClosed := true
  end;
end;

function TElSSHClient.SSH1CreateServerClientChannel(ServerChannel : cardinal;
  TunnelIndex : integer; var Connection : TElSSHClientTunnelConnection) : cardinal;
var
  LastClientCh : cardinal;
begin
  if FChannels.Count = 0 then
    LastClientCh := 0
  else
    LastClientCh := FLastChannelIndex;
  Result := LastClientCh + 1;
  FLastChannelIndex := Result;
  if Assigned(Connection) then
    Connection.Free;
    
  Connection := TElSSHClientTunnelConnection.Create;
  Connection.LocalChannel := Result;
  Connection.RemoteChannel := ServerChannel;
  Connection.Parent := Self;
  //Connection.FParentIsClient := true;
  Connection.Tunnel := FTunnelList.Tunnels[TunnelIndex];
  FChannels.Add(Connection);
  TElSSHClientTunnel(Connection.Tunnel).ConnectionsList.Add(Connection);
end;

function TElSSHClient.SSH1GetTunnelConnectionByLocalChannel(Channel : cardinal) :
  TElSSHClientTunnelConnection;
var
  I : integer;
begin
  Result := nil;
  for I := 0 to FChannels.Count - 1 do
    if TElSSHClientTunnelConnection(FChannels.Items[I]).LocalChannel = Channel then
    begin
      Result := FChannels.Items[I];
      Break;
    end;
end;

function TElSSHClient.SSH1GetTunnelConnectionByRemoteChannel(Channel : cardinal) :
  TElSSHClientTunnelConnection;
var
  I : integer;
begin
  Result := nil;
  for I := 0 to FChannels.Count - 1 do
    if TElSSHClientTunnelConnection(FChannels.Items[I]).RemoteChannel = Channel then
    begin
      Result := FChannels.Items[I];
      Break;
    end;
end;

procedure TElSSHClient.SSH1ConnectTunnel(Tunnel : TElCustomSSHTunnel;
  Data : pointer);
var
  Connection : TElSSHClientTunnelConnection;
  LastClientCh : cardinal;
begin
  if not (Tunnel is TElLocalPortForwardSSHTunnel) then
  begin
    DoTunnelError(Tunnel, SSH_TUNNEL_ERROR_UNSUPPORTED_BY_SSH_VERSION, Data);
    Exit;
  end;

  if FChannels.Count = 0 then
    LastClientCh := 0
  else
    LastClientCh := FLastChannelIndex;
  FLastChannelIndex := LastClientCh + 1;
  Connection := TElSSHClientTunnelConnection.Create;
  Connection.LocalChannel := FLastChannelIndex;
  Connection.RemoteChannel := $FFFFFFFF;
  Connection.Parent := Self;
  Connection.Data := Data;
  //Connection.FParentIsClient := true;
  Connection.Tunnel := Tunnel;
  FChannels.Add(Connection);
  TElSSHClientTunnel(Connection.Tunnel).ConnectionsList.Add(Connection);
  SSH1SendPortOpen(Connection.LocalChannel,
    TElLocalPortForwardSSHTunnel(Tunnel).ToHost,
    TElLocalPortForwardSSHTunnel(Tunnel).ToPort, '');
end;

////////////////////////////////////////////////////////////////////////////////
// SSH Identification string routines

procedure TElSSHClient.ParseServerIdentificationString(Buffer : pointer;
  Size : longint);
var
  Index : integer;
  VersionStr : string;
begin
  if Size < 8 then
  begin
    DoError(ERROR_SSH_INVALID_IDENTIFICATION_STRING);
    Exit;
  end;

  Index := MemPos(@MINUS[1], Length(MINUS), @PByteArray(Buffer)[4], Size - 4);
  SetLength(VersionStr, Index);
  Move(PByteArray(Buffer)[4], VersionStr[1], Index);
  if CompareStr(VersionStr, '1.99') = 0 then
  begin
    if sbSSH2 in FVersions then
      FVersion := sbSSH2
    else if sbSSH1 in FVersions then
      FVersion := sbSSH1
    else
    begin
      DoError(ERROR_SSH_INVALID_VERSION);
      Exit;
    end;
  end
  else if CompareStr(VersionStr, '1.5') = 0 then
  begin
    if sbSSH1 in FVersions then
      FVersion := sbSSH1
    else
    begin
      DoError(ERROR_SSH_INVALID_VERSION);
      Exit;
    end
  end
  else if CompareStr(VersionStr, '2.0') = 0 then
  begin
    if sbSSH2 in FVersions then
      FVersion := sbSSH2
    else
    begin
      DoError(ERROR_SSH_INVALID_VERSION);
      Exit;
    end;
  end
  else
  begin
    DoError(ERROR_SSH_INVALID_VERSION);
    Exit;
  end;
  SetLength(FServerSoftwareName, Size - Index - 5);
  Move(PByteArray(Buffer)[Index + 5], FServerSoftwareName[1], Size - Index - 5);
  FHandshakeParams.ServerVersionString := 'SSH-' + VersionStr + '-' + FServerSoftwareName;
  {$ifdef SECURE_BLACKBOX_DEBUG}
  DoDebug('Server Identification Line received', SSH_DEBUG_LEVEL_ALL_EVENTS);
  DoDebug('Version=' + VersionStr, SSH_DEBUG_LEVEL_ALL_EVENTS);
  DoDebug('ServerName=' + FServerSoftwareName, SSH_DEBUG_LEVEL_ALL_EVENTS);
  {$endif}
end;

procedure TElSSHClient.SendIdentificationString;
var
  IdString : string;
  Index : integer;
begin
  IdString := FSoftwareName;
  Index := Pos(' ', IdString);
  if Index > 0 then
    IdString := Copy(IdString, 1, Index - 1);
  Index := Pos('-', IdString);
  if Index > 0 then
    IdString := Copy(IdString, 1, Index - 1);
  if FVersion = sbSSH1 then
    IdString := 'SSH-1.5-' + IdString
  else if FVersion = sbSSH2 then
    IdString := 'SSH-2.0-' + IdString;
  if Length(IdString) > 253 then
    IdString := Copy(IdString, 1, 253);
  FHandshakeParams.ClientVersionString := IdString;
  IdString := IdString + FServerNewLine;
  FClientState := csIdentificationLineSent;
  DoSend(@IdString[1], Length(IdString));
end;

////////////////////////////////////////////////////////////////////////////////
// SSH2 protocol routines

procedure TElSSHClient.SSH2ParseTransportLayerHeader(Buffer : pointer; Size : longint);
begin
  { Decrypting header }
  if FSSH2State = stEncrypted then
  begin
    SSH2DecryptPacket(Buffer, Buffer, Size);
  end;
  {$ifdef SECURE_BLACKBOX_DEBUG}
  DoDebug('Decrypted length header: ' + FormatBuf(Buffer, Size), SSH_DEBUG_LEVEL_PACKET_DATA);
  {$endif}
  FInBufferNeeded := (PByteArray(Buffer)[0] shl 24) or (PByteArray(Buffer)[1] shl 16) or
    (PByteArray(Buffer)[2] shl 8) or PByteArray(Buffer)[3] - 1;
  {$ifdef SECURE_BLACKBOX_DEBUG}
  if (FDebugLevel = SSH_DEBUG_LEVEL_BUGSEARCH) and (FInBufferNeeded < 0) then
    DoDebug('InBufferNeeded is less than 0 ' + IntToStr(FInBufferNeeded) + ') [0]',
    SSH_DEBUG_LEVEL_BUGSEARCH);
  {$endif}
  FPaddingSize := PByteArray(Buffer)[4];
  SetLength(FInBufferHeader, 5);
  Move(Buffer^, FInBufferHeader[1], 5);
  Move(PByteArray(Buffer)[5], FInBuffer[0], Size - 5);
  FInBufferIndex := Size - 5;
  FInBufferNeeded := FInBufferNeeded - FInBufferIndex;
  if FSSH2State = stEncrypted then
  begin
    Inc(FInBufferNeeded, SSH2MacDigestSizes[FSSH2Params.MacAlgorithmSC]);
    FInBufferOffset := FInBufferIndex;
  end;
  if FInBufferNeeded = 0 then
  begin
    SSH2ParseOnTransportLayer(@FInBuffer[0], FInBufferIndex);
    FInBufferIndex := 0;
    FReceiveState := rsRecordHeaderWanted;
    FInBufferNeeded := Max(8, FSSH2Params.EncSCBlockSize);
  end
  else if FInBufferNeeded >= IN_BUFFER_SIZE then
  begin
    FInBufferNeeded := 0;
    DoError(ERROR_SSH_INVALID_PACKET_SIZE);
    CloseByError(SDisconnectInvalidPacketSize);
  end;
  {$ifdef SECURE_BLACKBOX_DEBUG}
  if (FDebugLevel = SSH_DEBUG_LEVEL_BUGSEARCH) and (FInBufferNeeded < 0) then
    DoDebug('InBufferNeeded is less than 0 ' + IntToStr(FInBufferNeeded) + ') [1]',
    SSH_DEBUG_LEVEL_BUGSEARCH);
  {$endif}
end;

procedure TElSSHClient.SSH2ParseOnTransportLayer(Buffer : pointer; Size : longint);
var
  I : integer;
begin
  if (FSSH2Params.CompAlgorithmSC = SSH_CA_ZLIB) and 
     (FSSH2State = stEncrypted) then
  begin
    I := IN_BUFFER_SIZE;
    try
      SSH2DecompressPacket(Buffer, Size, FComprBuffer, I);
    except
      on E : Exception do
      begin
        DoError(ERROR_SSH_COMPRESSION_ERROR);
        CloseByError(E.Message);
        Exit;
      end;
    end;
    Buffer := FComprBuffer;
    Size := I;
  end;
  FSSH2ServerLastMessage := PByteArray(Buffer)[0];
  {$ifdef SECURE_BLACKBOX_DEBUG}
  {$endif}
  if (PByteArray(Buffer)[0] >= 50) and (PByteArray(Buffer)[0] <= 79) then
    SSH2ParseOnUserauthLayer(Buffer, Size)
  else if (PByteArray(Buffer)[0] >= 80) and (PByteArray(Buffer)[0] <= 127) then
    SSH2ParseOnConnectionLayer(Buffer, Size)
  else
  begin
    case PByteArray(Buffer)[0] of
      SSH2_MSG_DISCONNECT : SSH2ParseServerDisconnect(Buffer, Size);
      SSH2_MSG_IGNORE : ;
      SSH2_MSG_DEBUG : SSH2ParseServerDebug(Buffer, Size);
      SSH2_MSG_KEXINIT : SSH2ParseKexInit(Buffer, Size);
      SSH2_MSG_KEX_DH_GEX_GROUP{ and SSH2_MSG_KEXDH_REPLY} :
        if FSSH2Params.KexAlgorithm in [SSH_KEX_DH_GROUP, SSH_KEX_DH_GROUP_14] then
          SSH2ParseKexDHReply(Buffer, Size)
        else if FSSH2Params.KexAlgorithm = SSH_KEX_DH_GROUP_EXCHANGE then
          SSH2ParseKexDHGexGroup(Buffer, Size);
      SSH2_MSG_KEX_DH_GEX_REPLY : SSH2ParseKexDHGexReply(Buffer, Size);
      SSH2_MSG_NEWKEYS : SSH2ParseServerNewkeys(Buffer, Size);
      SSH2_MSG_SERVICE_ACCEPT : SSH2ParseServerServiceAccept(Buffer, Size);
      SSH2_MSG_UNIMPLEMENTED : ;
    else
      DoError(ERROR_SSH_INVALID_MESSAGE_CODE);
      CloseByError(SDisconnectInvalidPacket);
    end;
  end;
  Inc(FSSH2ServerSequenceNumber);
end;

procedure TElSSHClient.SSH2ParseOnUserauthLayer(Buffer : pointer; Size : longint);
begin
  if FSSH2AuthenticationPassed then
    Exit; { Ignoring if authentication is completed }
  case PByteArray(Buffer)[0] of
    SSH2_MSG_USERAUTH_SUCCESS : SSH2ParseServerUserauthSuccess(Buffer, Size);
    SSH2_MSG_USERAUTH_FAILURE : SSH2ParseServerUserauthFailure(Buffer, Size);
    SSH2_MSG_USERAUTH_BANNER : SSH2ParseServerUserauthBanner(Buffer, Size);
    SSH2_MSG_USERAUTH_PK_OK :
      if FSSH2LastUserauthMethod = SSH_AUTH_TYPE_PUBLICKEY then
        SSH2ParseServerUserauthPKOK(Buffer, Size)
      else
      if FSSH2LastUserauthMethod = SSH_AUTH_TYPE_KEYBOARD then
        SSH2ParseServerUserauthInfoRequest(Buffer, Size);
  end;
end;

procedure TElSSHClient.SSH2ParseOnConnectionLayer(Buffer : pointer; Size : longint);
begin
  case PByteArray(Buffer)[0] of
    SSH2_MSG_CHANNEL_OPEN_CONFIRMATION : SSH2ParseServerChannelOpenConfirmation(
      Buffer, Size);
    SSH2_MSG_CHANNEL_SUCCESS : SSH2ParseServerChannelSuccess(Buffer, Size);
    SSH2_MSG_CHANNEL_FAILURE : SSH2ParseServerChannelFailure(Buffer, Size);
    SSH2_MSG_CHANNEL_DATA : SSH2ParseServerChannelData(Buffer, Size);
    SSH2_MSG_CHANNEL_EXTENDED_DATA : SSH2ParseServerChannelExtendedData(Buffer, Size);
    SSH2_MSG_CHANNEL_OPEN : SSH2ParseServerChannelOpen(Buffer, Size);
    SSH2_MSG_CHANNEL_OPEN_FAILURE : SSH2ParseServerChannelOpenFailure(Buffer, Size);
    SSH2_MSG_CHANNEL_EOF : SSH2ParseServerChannelEOF(Buffer, Size);
    SSH2_MSG_CHANNEL_CLOSE : SSH2ParseServerChannelClose(Buffer, Size);
    SSH2_MSG_CHANNEL_REQUEST : SSH2ParseServerChannelRequest(Buffer, Size);
    SSH2_MSG_CHANNEL_WINDOW_ADJUST: SSH2ParseServerChannelWindowAdjust(Buffer, Size);
    SSH2_MSG_REQUEST_SUCCESS : ;
    SSH2_MSG_REQUEST_FAILURE : ;
  end;
end;

procedure TElSSHClient.SSH2ParseKexInit(Buffer : pointer; Size : longint);
var
  Index, I : integer;
  KexStrings : TStringList;
begin
  if Size < 17 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  SetLength(FHandshakeParams.ServerKexInit, Size);
  Move(Buffer^, FHandshakeParams.ServerKexInit[1], Size);
  KexStrings := TStringList.Create;
  Move(PByteArray(Buffer)[1], FServerCookie[0], 16);
  Index := 17;
  try
    for I := 0 to 9 do
    begin
      KexStrings.Add(ReadString(@PByteArray(Buffer)[Index], Size - Index));
      Index := Index + Length(KexStrings.Strings[KexStrings.Count - 1]) + 4;
    end;
  except
    KexStrings.Free;
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  {$ifdef SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('Server KEXINIT received');
  Dumper.WriteString(KexStrings.Text);
  {$endif}
  SSH2ChooseAlgorithms(KexStrings);
  KexStrings.Free;
  SSH2SendKexInit;
  if FSSH2Params.KexAlgorithm in [SSH_KEX_DH_GROUP, SSH_KEX_DH_GROUP_14] then
  begin
    SSH2KexDHGroupGenerate;
    SSH2SendKexDHInit;
  end
  else if FSSH2Params.KexAlgorithm = SSH_KEX_DH_GROUP_EXCHANGE then
  begin
    SSH2SendKexDHGexRequest;
  end;
end;

procedure TElSSHClient.SSH2ParseKexDHReply(Buffer : pointer; Size : longint);
var
  F : PLInt;
  PubKeyStr, FStr, Signature, Tmp : string;
  FHostAlg, FHostE, FHostN, FSignAlg, FSignVal : string;
  FHostLE, FHostLN, FHostSign, FHostDecSign : PLInt;
  DecSign, Arr : array of byte;
  Sz : integer;
  SHA1Ctx : TSHA1Context;
  Hash, SignHash : TMessageDigest160;
  Key : TElSSHKey;
  B : boolean;
  Alg, I : integer;
  M, P, Q, G, Y, R, S : string;
  Cert : TElX509Certificate;
  CertBody : string;
  NSize, ESize, PSize, QSize, GSize, YSize: integer;
  SigOffset : integer;
begin
  try
    PubKeyStr := ReadString(@PByteArray(Buffer)[1], Size);
    FStr := ReadSSH2MPInt(@PByteArray(Buffer)[5 + Length(PubKeyStr)], Size, Sz);
    Signature := ReadString(@PByteArray(Buffer)[5 + Sz + Length(PubKeyStr) + Length(FStr)], Size);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  try
    FHostAlg := ReadString(@PubKeyStr[1], Length(PubKeyStr));
    if FPublicKeyAlgorithm in [SSH_PK_X509_SIGN_RSA, SSH_PK_X509_SIGN_DSS] then
      CertBody := Copy(PubKeyStr, Length(FHostAlg) + 5, Length(PubKeyStr));
  except
    if FPublicKeyAlgorithm in [SSH_PK_X509_SIGN_RSA, SSH_PK_X509_SIGN_DSS] then
    begin
      { For the case of certificate host algorithm string might be empty }
      FHostAlg := SSH2PublicStrings[FPublicKeyAlgorithm];
      CertBody := PubKeyStr;
    end
    else
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
  end;
  if (CompareStr(FHostAlg, 'ssh-rsa') = 0) and (FSSH2Params.ServerHostKeyAlgorithm = SSH_PK_RSA) then
    Alg := 1
  else if (CompareStr(FHostAlg, 'ssh-dss') = 0) and (FSSH2Params.ServerHostKeyAlgorithm = SSH_PK_DSS) then
    Alg := 2
  else if (CompareStr(FHostAlg, 'x509v3-sign-rsa') = 0) and (FSSH2Params.ServerHostKeyAlgorithm = SSH_PK_X509_SIGN_RSA) then
    Alg := 3
  else if (CompareStr(FHostAlg, 'x509v3-sign-dss') = 0) and (FSSH2Params.ServerHostKeyAlgorithm = SSH_PK_X509_SIGN_DSS) then
    Alg := 4
  else
  begin
    // unsupported public key algorithm
    DoError(ERROR_SSH_KEY_EXCHANGE_FAILED);
    CloseByError(SDisconnectKeyExchangeFailed);
    Exit;
  end;
  LCreate(F);
  PointerToLInt(F, @FStr[1], Length(FStr));
  if LGreater(F, FDHParams.P) then
  begin
    DoError(ERROR_SSH_KEY_EXCHANGE_FAILED);
    CloseByError(SDisconnectKeyExchangeFailed);
    Exit;
  end;
  SSH2KexDHPower(F, FDHParams.X, FDHParams.P, FDHParams.K);
  Cert := nil;
  if Alg in [1, 3] then { Plain RSA algorithm or X.509 RSA certificate }
  begin
    if Alg = 1 then { plain rsa }
    begin
      try
        FHostE := ReadString(@PubKeyStr[5 + Length(FHostAlg)], Length(PubKeyStr));
        FHostN := ReadString(@PubKeyStr[9 + Length(FHostAlg) + Length(FHostE)], Length(PubKeyStr));
      except
        DoError(ERROR_SSH_INVALID_PACKET);
        CloseByError(SDisconnectInvalidPacket);
        Exit;
      end;
    end
    else { x509v3 rsa certificate }
    begin
      Cert := TElX509Certificate.Create(nil);
      try
        Cert.LoadFromBuffer(@CertBody[1], Length(CertBody));
        if Cert.PublicKeyAlgorithm <> SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
          raise EElSSHSilentException.Create(SCertAlgDoesNotMatchSSH);
        NSize := 0;
        Cert.GetRSAParams(nil, NSize, nil, ESize);
        SetLength(FHostN, NSize);
        SetLength(FHostE, ESize);
        Cert.GetRSAParams(@FHostN[1], NSize, @FHostE[1], ESize);
        SetLength(FHostN, NSize);
        SetLength(FHostE, ESize);
      except
        Cert.Free;
        DoError(ERROR_SSH_INVALID_PACKET);
        CloseByError(SDisconnectInvalidPacket);
        Exit;
      end;
    end;
    Key := TElSSHKey.Create;
    if Cert <> nil then
    begin
      Key.Import(Cert);
      Cert.Free;
    end;
    Key.Algorithm := ALGORITHM_RSA;
    Key.RSAPublicExponent := FHostE;
    Key.RSAPublicModulus := FHostN;
    B := true;
    DoKeyValidate(Key, B);
    Key.Free;

    if not B then
    begin
      DoError(ERROR_SSH_HOST_KEY_NOT_VERIFIABLE);
      CloseByError(SDisconnectHostKeyNotVerifiable);
      Exit;     // key unrecognized
    end;
    try
      FSignAlg := ReadString(@Signature[1], Length(Signature));
      FSignVal := ReadString(@Signature[5 + Length(FSignAlg)], Length(Signature));
    except
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    if ((FSSH2Params.ServerHostKeyAlgorithm = SSH_PK_RSA) and (CompareStr(FSignAlg, 'ssh-rsa') <> 0)) or
      ((FSSH2Params.ServerHostKeyAlgorithm = SSH_PK_X509_SIGN_RSA) and (CompareStr(FSignAlg, 'x509v3-sign-rsa') <> 0)) then
    begin
      DoError(ERROR_SSH_KEY_EXCHANGE_FAILED);
      CloseByError(SDisconnectKeyExchangeFailed);
      Exit;
    end;
    { checking singature }
    LCreate(FHostLE);
    LCreate(FHostLN);
    LCreate(FHostSign);
    LCreate(FHostDecSign);
    PointerToLInt(FHostLE, @FHostE[1], Length(FHostE));
    PointerToLInt(FHostLN, @FHostN[1], Length(FHostN));
    PointerToLInt(FHostSign, @FSignVal[1], Length(FSignVal));
    LSSHModPower(FHostSign, FHostLE, FHostLN, FHostDecSign); ///
    SetLength(DecSign, Length(Signature));
    Sz := Length(Signature);
    LIntToPointer(FHostDecSign, @DecSign[0], Sz);
    SetLength(DecSign, Sz);
    LDestroy(FHostLE);
    LDestroy(FHostLN);
    LDestroy(FHostSign);
    LDestroy(FHostDecSign);
    { checking the decrypted signature to be formatted properly }
    if (Sz < 3) or (DecSign[0] <> 0) or (DecSign[1] <> 1) then
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    I := 3;
    while (I < Length(DecSign)) and (DecSign[I] = $FF) do
      Inc(I);
    if (I >= Length(DecSign)) or (DecSign[I] <> 0) then
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    DecSign := Copy(DecSign, I + 1, Length(DecSign));
    if Length(DecSign) <> 35 then
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
  end
  else if Alg in [2, 4] then { plain dss or x509-dss }
  begin
    if Alg = 2 then { plain dss }
    begin
      try
        Tmp := ReadString(@PubKeyStr[1], Length(PubKeyStr));
        P := ReadString(@PubKeyStr[4 + Length(Tmp) + 1], Length(PubKeyStr));
        Q := ReadString(@PubKeyStr[4 + Length(Tmp) + 1 + 4 + Length(P)], Length(PubKeyStr));
        G := ReadString(@PubKeyStr[4 + Length(Tmp) + 1 + 4 + Length(P) + 4 + Length(Q)], Length(PubKeyStr));
        Y := ReadString(@PubKeyStr[4 + Length(Tmp) + 1 + 4 + Length(P) + 4 + Length(Q) + 4 + Length(G)],
          Length(PubKeyStr));
      except
        DoError(ERROR_SSH_INVALID_PACKET);
        CloseByError(SDisconnectInvalidPacket);
        Exit;
      end;
    end
    else if Alg = 4 then { x509 dss }
    begin
      Cert := TElX509Certificate.Create(nil);
      try
        Cert.LoadFromBuffer(@CertBody[1], Length(CertBody));
        if Cert.PublicKeyAlgorithm <> SB_CERT_ALGORITHM_ID_DSA then
          raise EElSSHSilentException.Create(SCertAlgDoesNotMatchSSH);
        PSize := 0;
        Cert.GetDSSParams(nil, PSize, nil, QSize, nil, GSize, nil, YSize);
        SetLength(P, PSize);
        SetLength(Q, QSize);
        SetLength(G, GSize);
        SetLength(Y, YSize);
        Cert.GetDSSParams(@P[1], PSize, @Q[1], QSize, @G[1], GSize, @Y[1], YSize);
        SetLength(P, PSize);
        SetLength(Q, QSize);
        SetLength(G, GSize);
        SetLength(Y, YSize);
      except
        Cert.Free;
        DoError(ERROR_SSH_INVALID_PACKET);
        CloseByError(SDisconnectInvalidPacket);
        Exit;
      end;
    end;

    Key := TElSSHKey.Create;
    if Cert <> nil then
    begin
      Key.Import(Cert);
      Cert.Free;
    end;
    Key.Algorithm := ALGORITHM_DSS;
    Key.DSSP := P;
    Key.DSSQ := Q;
    Key.DSSG := G;
    Key.DSSY := Y;
    B := true;
    DoKeyValidate(Key, B);
    Key.Free;

    if not B then
    begin
      DoError(ERROR_SSH_HOST_KEY_NOT_VERIFIABLE);
      CloseByError(SDisconnectHostKeyNotVerifiable);
      Exit;     // key unrecognized
    end;
  end;
  { Counting hash }
  InitializeSHA1(SHA1Ctx);
  Tmp := WriteUINT32(Length(FHandshakeParams.ClientVersionString));
  HashSHA1(SHA1Ctx, @Tmp[1], 4);
  HashSHA1(SHA1Ctx, @FHandshakeParams.ClientVersionString[1],
    Length(FHandshakeParams.ClientVersionString));
  Tmp := WriteUINT32(Length(FHandshakeParams.ServerVersionString));
  HashSHA1(SHA1Ctx, @Tmp[1], 4);
  HashSHA1(SHA1Ctx, @FHandshakeParams.ServerVersionString[1],
    Length(FHandshakeParams.ServerVersionString));
  Tmp := WriteUINT32(Length(FHandshakeParams.ClientKexInit));
  HashSHA1(SHA1Ctx, @Tmp[1], 4);
  HashSHA1(SHA1Ctx, @FHandshakeParams.ClientKexInit[1],
    Length(FHandshakeParams.ClientKexInit));
  Tmp := WriteUINT32(Length(FHandshakeParams.ServerKexInit));
  HashSHA1(SHA1Ctx, @Tmp[1], 4);
  HashSHA1(SHA1Ctx, @FHandshakeParams.ServerKexInit[1],
    Length(FHandshakeParams.ServerKexInit));
  Tmp := WriteUINT32(Length(PubKeyStr));
  HashSHA1(SHA1Ctx, @Tmp[1], 4);
  HashSHA1(SHA1Ctx, @PubKeyStr[1], Length(PubKeyStr));
  SetLength(Arr, FDHParams.E^.Length * 4);
  Sz := FDHParams.E^.Length * 4;
  LIntToPointer(FDHParams.E, @Arr[0], Sz);
  SetLength(Arr, Sz);
  Tmp := WriteSSH2MPInt(@Arr[0], Sz);
  HashSHA1(SHA1Ctx, @Tmp[1], Length(Tmp));
  SetLength(Arr, F^.Length * 4);
  Sz := F^.Length * 4;
  LIntToPointer(F, @Arr[0], Sz);
  SetLength(Arr, Sz);
  Tmp := WriteSSH2MPInt(@Arr[0], Sz);
  HashSHA1(SHA1Ctx, @Tmp[1], Length(Tmp));
  SetLength(Arr, FDHParams.K^.Length * 4);
  Sz := FDHParams.K^.Length * 4;
  LIntToPointer(FDHParams.K, @Arr[0], Sz);
  SetLength(Arr, Sz);
  Tmp := WriteSSH2MPInt(@Arr[0], Sz);
  HashSHA1(SHA1Ctx, @Tmp[1], Length(Tmp));
  Hash := FinalizeSHA1(SHA1Ctx);
  LDestroy(F);
  if Alg in [1, 3] then
  begin
    SignHash := HashSHA1(@Hash, 20);
    DecSign := Copy(DecSign, Length(DecSign) - 20, 20);
    if (Length(DecSign) <> 20) or (not CompareMem(@SignHash, @DecSign[0], 20)) then
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
  end
  else 
  if Alg in [2, 4] then
  begin
    if (Pos(#0#0#0#7'ssh-dss', Signature) = 1) and 
       (Length(Signature) = 55) then
      SigOffset := 15
    else
    if (Pos(#0#0#0#15'x509v3-sign-dss', Signature) = 1) and 
       (Length(Signature) = 63) then
      SigOffset := 23
    else
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;

    SetLength(M, 20);
    Move(PByteArray(@Hash)[0], M[1], 20);
    SetLength(R, 20);
    SetLength(S, 20);
    Move(Signature[SigOffset + 1], R[1], 20);
    Move(Signature[SigOffset + 21], S[1], 20);
    if not SSH2ValidateDSS(M, P, Q, G, Y, R, S) then
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
  end;
  if FSSH2State = stEncrypted then
  begin
    // rekeying is done, so applying new keys only after the corresponding
    // (SSH_MSG_NEWKEYS) packet is received from server
    SSH2SendNewKeys;
    SetLength(FKexHash, 20);
    Move(Hash, FKexHash[0], 20);
    FKexKey := Tmp;
  end
  else
  begin
    SetLength(FSSH2Params.SessionID, 20);
    Move(PByteArray(@Hash)[0], FSSH2Params.SessionID[0], 20);
    SSH2GenerateKeys(Tmp, @Hash, 20);
    SSH2SendNewKeys;
  end;
end;

procedure TElSSHClient.SSH2ParseKexDHGexGroup(Buffer : pointer; Size : longint);
var
  BufP, BufG : string;
begin
  try
    BufP := ReadString(@PByteArray(Buffer)[1], Size);
    BufG := ReadString(@PByteArray(Buffer)[1 + 4 + Length(BufP)], Size);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  FHandshakeParams.P := WriteSSH2MPInt(@BufP[1], Length(BufP)); //WriteUINT32(Length(BufP)) + BufP;
  FHandshakeParams.G := WriteSSH2MPInt(@BufG[1], Length(BufG));//WriteUINT32(Length(BufG)) + BufG;
  PointerToLint(FDHParams.P, @BufP[1], Length(BufP));
  PointerToLint(FDHParams.G, @BufG[1], Length(BufG));
  GenerateDHX;
  SSH2SendKexDHGexInit;
end;

procedure TElSSHClient.SSH2ParseKexDHGexReply(Buffer : pointer; Size : longint);
var
  PubHostKey, FStr, SignStr, Tmp, PrivateExp, Modulus, KStr, W : string;
  M, P, Q, G, Y, R, S : string;
  Index : integer;
  K, F : PLint;
  L1, L2, L3, L4 : PLInt;
  Hash, SignHash : TMessageDigest160;
  ShaCtx : TSHA1Context;
  Alg : integer;
  Key : TElSSHKey;
  B : boolean;
  Cert : TElX509Certificate;
  CertBody : string;
  NSize, ESize, PSize, QSize, GSize, YSize: integer;
  SigOffset : integer; 
begin
  try
    PubHostKey := ReadString(@PByteArray(Buffer)[1], Size);
    FHandshakeParams.PubHostKey := WriteUINT32(Length(PubHostKey)) + PubHostKey;
    Index := 1 + 4 + Length(PubHostKey);
    FStr := ReadString(@PByteArray(Buffer)[Index], Size - Index + 1);
    Index := Index + 4 + Length(FStr);
    SignStr := ReadString(@PByteArray(Buffer)[Index], Size - Index + 1);
    FHandshakeParams.F := WriteUINT32(Length(FStr)) + FStr;
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  // obtaining K
  LCreate(K);
  LCreate(F);
  PointerToLint(F, @FStr[1], Length(FStr));

  LSSHModPower(F, FDHParams.X, FDHParams.P, K);

  SetLength(KStr, 10000);
  Index := 10000;
  LIntToPointer(K, @KStr[1], Index);
  SetLength(KStr, Index);
  LDestroy(F);
  LDestroy(K);

  try
    Tmp := ReadString(@PubHostKey[1], Length(PubHostKey));
    if FSSH2Params.ServerHostKeyAlgorithm in [SSH_PK_X509_SIGN_RSA, SSH_PK_X509_SIGN_DSS] then
    begin
      CertBody := Copy(PubHostKey, 5 + Length(Tmp), Length(PubHostKey));
    end;
  except
    if FSSH2Params.ServerHostKeyAlgorithm in [SSH_PK_X509_SIGN_RSA, SSH_PK_X509_SIGN_DSS] then
    begin
      { certificate arrived in plain }
      Tmp := SSH2PublicStrings[FSSH2Params.ServerHostKeyAlgorithm];
      CertBody := PubHostKey;
    end
    else
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
  end;

  if (CompareStr(Tmp, 'ssh-rsa') = 0) and (FSSH2Params.ServerHostKeyAlgorithm = SSH_PK_RSA) then
    Alg := 1
  else if (CompareStr(Tmp, 'ssh-dss') = 0) and
          (FSSH2Params.ServerHostKeyAlgorithm = SSH_PK_DSS) then
    Alg := 2
  else if (CompareStr(Tmp, 'x509v3-sign-rsa') = 0) and 
          (FSSH2Params.ServerHostKeyAlgorithm = SSH_PK_X509_SIGN_RSA) then
    Alg := 3
  else if (CompareStr(Tmp, 'x509v3-sign-dss') = 0) and 
          (FSSH2Params.ServerHostKeyAlgorithm = SSH_PK_X509_SIGN_DSS) then
    Alg := 4
  else
  begin
    DoError(ERROR_SSH_KEY_EXCHANGE_FAILED);
    CloseByError(SDisconnectKeyExchangeFailed);
    Exit;
  end;
  Cert := nil;
  if Alg in [1, 3] then { plain RSA key or X.509 with RSA key }
  begin
    if Alg = 1 then { plain RSA }
    begin
      try
        Tmp := ReadString(@SignStr[1], Length(SignStr));
        SignStr := Copy(SignStr, 4 + Length(Tmp) + 1, Length(SignStr));
        SignStr := ReadString(@SignStr[1], Length(SignStr));
        PubHostKey := Copy(PubHostKey, 4 + Length(Tmp) + 1, Length(PubHostKey));
        PrivateExp := ReadString(@PubHostKey[1], Length(PubHostKey));
        PubHostKey := Copy(PubHostKey, 4 + Length(PrivateExp) + 1, Length(PubHostKey));
        Modulus := ReadString(@PubHostKey[1], Length(PubHostKey));
      except
        DoError(ERROR_SSH_INVALID_PACKET);
        CloseByError(SDisconnectInvalidPacket);
        Exit;
      end;
    end
    else { X.509 RSA certificate }
    begin
      // draft-ietf-secsh-transport.txt declares the following format for signatures:
      // -----
      // Signatures are encoded as follows:
      //   string    signature format identifier (as specified by the
      //     public key / cert format)
      //   byte[n]   signature blob in format specific encoding.
      // -----
      // However, some servers do not provide the signature format identifier
      // for RSA certificates. In order to interoperate with these servers, we
      // have to detect the signature format ourselves.

      try
        Tmp := ReadString(@SignStr[1], Length(SignStr));
        SignStr := Copy(SignStr, 4 + Length(Tmp) + 1, Length(SignStr));
        SignStr := ReadString(@SignStr[1], Length(SignStr));
      except
        ;
      end;

      // certificate data
      Cert := TElX509Certificate.Create(nil);
      try
        Cert.LoadFromBuffer(@CertBody[1], Length(CertBody));
        if Cert.PublicKeyAlgorithm <> SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
          raise EElSSHSilentException.Create(SCertAlgDoesNotMatchSSH);
        NSize := 0;
        Cert.GetRSAParams(nil, NSize, nil, ESize);
        SetLength(Modulus, NSize);
        SetLength(PrivateExp, ESize);
        Cert.GetRSAParams(@Modulus[1], NSize, @PrivateExp[1], ESize);
        SetLength(Modulus, NSize);
        SetLength(PrivateExp, ESize);
      except
        Cert.Free;
        DoError(ERROR_SSH_INVALID_PACKET);
        CloseByError(SDisconnectInvalidPacket);
        Exit;
      end;
    end;
    Key := TElSSHKey.Create;
    if Cert <> nil then
    begin
      Key.Import(Cert);
      Cert.Free;
    end;
    Key.Algorithm := ALGORITHM_RSA;
    Key.RSAPublicExponent := PrivateExp;
    Key.RSAPublicModulus := Modulus;
    B := true;
    DoKeyValidate(Key, B);
    Key.Free;

    if not B then
    begin
      DoError(ERROR_SSH_HOST_KEY_NOT_VERIFIABLE);
      CloseByError(SDisconnectHostKeyNotVerifiable);
      Exit;     // key unrecognized
    end;
    LCreate(L1);
    LCreate(L2);
    LCreate(L3);
    LCreate(L4);
    PointerToLint(L1, @Modulus[1], Length(Modulus));
    PointerToLint(L2, @PrivateExp[1], Length(PrivateExp));
    PointerToLint(L3, @SignStr[1], Length(SignStr));
    LSSHModPower(L3, L2, L1, L4); ///
    SetLength(W, 2048);
    Index := 2048;
    LintToPointer(L4, @W[1], Index);
    SetLength(W, Index);
    LDestroy(L1);
    LDestroy(L2);
    LDestroy(L3);
    LDestroy(L4);
    if (W[1] <> #0) or (W[2] <> #1) then
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    Index := 3;
    while (Index <= Length(W)) and (W[Index] = #$FF) do
      Inc(Index);
    if (Index > Length(W)) or (W[Index] <> #0) then
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    W := Copy(W, Index + 1, Length(W));
    if Length(W) <> 35 then
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    W := Copy(W, Length(W) - 19, 20);
  end
  else if Alg in [2, 4] then { plain DSS or X.509 DSS certificate }
  begin
    if Alg = 2 then { plain DSS }
    begin
      try
        P := ReadString(@PubHostKey[4 + Length(Tmp) + 1], Length(PubHostKey));
        Q := ReadString(@PubHostKey[4 + Length(Tmp) + 1 + 4 + Length(P)], Length(PubHostKey));
        G := ReadString(@PubHostKey[4 + Length(Tmp) + 1 + 4 + Length(P) + 4 + Length(Q)], Length(PubHostKey));
        Y := ReadString(@PubHostKey[4 + Length(Tmp) + 1 + 4 + Length(P) + 4 + Length(Q) + 4 + Length(G)],
          Length(PubHostKey));
      except
        DoError(ERROR_SSH_INVALID_PACKET);
        CloseByError(SDisconnectInvalidPacket);
        Exit;
      end;
    end
    else if Alg = 4 then { X.509 DSS certificate }
    begin
      Cert := TElX509Certificate.Create(nil);
      try
        Cert.LoadFromBuffer(@CertBody[1], Length(CertBody));
        if Cert.PublicKeyAlgorithm <> SB_CERT_ALGORITHM_ID_DSA then
          raise EElSSHSilentException.Create(SCertAlgDoesNotMatchSSH);
        PSize := 0;
        Cert.GetDSSParams(nil, PSize, nil, QSize, nil, GSize, nil, YSize);
        SetLength(P, PSize);
        SetLength(Q, QSize);
        SetLength(G, GSize);
        SetLength(Y, YSize);
        Cert.GetDSSParams(@P[1], PSize, @Q[1], QSize, @G[1], GSize, @Y[1], YSize);
        SetLength(P, PSize);
        SetLength(Q, QSize);
        SetLength(G, GSize);
        SetLength(Y, YSize);
      except
        Cert.Free;
        DoError(ERROR_SSH_INVALID_PACKET);
        CloseByError(SDisconnectInvalidPacket);
        Exit;
      end;
    end;
    Key := TElSSHKey.Create;
    if Cert <> nil then
    begin
      Key.Import(Cert);
      Cert.Free;
    end;
    Key.Algorithm := ALGORITHM_DSS;
    Key.DSSP := P;
    Key.DSSQ := Q;
    Key.DSSG := G;
    Key.DSSY := Y;
    B := true;
    DoKeyValidate(Key, B);
    Key.Free;

    if not B then
    begin
      DoError(ERROR_SSH_HOST_KEY_NOT_VERIFIABLE);
      CloseByError(SDisconnectHostKeyNotVerifiable);
      Exit;     // key unrecognized
    end;
  end;
  InitializeSHA1(ShaCtx);
  Tmp := WriteUINT32(Length(FHandshakeParams.ClientVersionString));
  HashSHA1(ShaCtx, @Tmp[1], 4);
  HashSHA1(ShaCtx, @FHandshakeParams.ClientVersionString[1], Length(
    FHandshakeParams.ClientVersionString));
  Tmp := WriteUINT32(Length(FHandshakeParams.ServerVersionString));
  HashSHA1(ShaCtx, @Tmp[1], 4);
  HashSHA1(ShaCtx, @FHandshakeParams.ServerVersionString[1], Length(
    FHandshakeParams.ServerVersionString));
  Tmp := WriteUINT32(Length(FHandshakeParams.ClientKexInit));
  HashSHA1(ShaCtx, @Tmp[1], 4);
  HashSHA1(ShaCtx, @FHandshakeParams.ClientKexInit[1], Length(
    FHandshakeParams.ClientKexInit));
  Tmp := WriteUINT32(Length(FHandshakeParams.ServerKexInit));
  HashSHA1(ShaCtx, @Tmp[1], 4);
  HashSHA1(ShaCtx, @FHandshakeParams.ServerKexInit[1], Length(
    FHandshakeParams.ServerKexInit));
  HashSHA1(ShaCtx, @FHandshakeParams.PubHostKey[1], Length(FHandshakeParams.PubHostKey));
  HashSHA1(ShaCtx, @FHandshakeParams.Min[1], Length(FHandshakeParams.Min));
  HashSHA1(ShaCtx, @FHandshakeParams.N[1], Length(FHandshakeParams.N));
  HashSHA1(ShaCtx, @FHandshakeParams.Max[1], Length(FHandshakeParams.Max));
  HashSHA1(ShaCtx, @FHandshakeParams.P[1], Length(FHandshakeParams.P));
  HashSHA1(ShaCtx, @FHandshakeParams.G[1], Length(FHandshakeParams.G));
  HashSHA1(ShaCtx, @FHandshakeParams.E[1], Length(FHandshakeParams.E));
  HashSHA1(ShaCtx, @FHandshakeParams.F[1], Length(FHandshakeParams.F));
  Tmp := WriteSSH2MPInt(@KStr[1], Length(KStr));
  HashSHA1(ShaCtx, @Tmp[1], Length(Tmp));
  Hash := FinalizeSHA1(ShaCtx);
  if Alg in [1, 3] then
  begin
    SignHash := HashSHA1(@Hash, 20);
    if not CompareMem(@SignHash, @W[1], 20) then
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
  end
  else if Alg in [2, 4] then
  begin
    if (Pos(#0#0#0#7'ssh-dss', SignStr) = 1) and 
       (Length(SignStr) = 55) then
      SigOffset := 15
    else
    if (Pos(#0#0#0#15'x509v3-sign-dss', SignStr) = 1) and 
            (Length(SignStr) = 63) then
      SigOffset := 23
    else
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    SetLength(M, 20);
    Move(PByteArray(@Hash)[0], M[1], 20);
    SetLength(R, 20);
    SetLength(S, 20);
    Move(SignStr[SigOffset + 1], R[1], 20);
    Move(SignStr[SigOffset + 21], S[1], 20);
    if not SSH2ValidateDSS(M, P, Q, G, Y, R, S) then
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
  end;
  SetLength(FSSH2Params.SessionID, 20);
  if FSSH2State = stEncrypted then
  begin
    // rekeying is done, so applying new keys only after the corresponding
    // (SSH_MSG_NEWKEYS) packet is received from server
    SSH2SendNewKeys;
    SetLength(FKexHash, 20);
    Move(Hash, FKexHash[0], 20);
    FKexKey := Tmp;
  end
  else
  begin
    Move(PByteArray(@Hash)[0], FSSH2Params.SessionID[0], 20);
    SSH2GenerateKeys(Tmp, @Hash, 20);
    SSH2SendNewKeys;
  end;
end;

procedure TElSSHClient.SSH2ParseServerNewkeys(Buffer : pointer; Size : longint);
begin
  if FSSH2State = stEncrypted then
  begin
    // rekeying, authentication is not needed
    SSH2GenerateKeys(FKexKey, @FKexHash[0], Length(FKexHash));
    FKexHash := EmptyArray;
    FKexKey := EmptyBuffer;
  end
  else
  begin
    FSSH2State := stEncrypted;
    SSH2SendServiceRequest('ssh-userauth');
  end;
end;

procedure TElSSHClient.SSH2ParseServerDisconnect(Buffer : pointer; Size : longint);
var
  ReasonCode : cardinal;
  LangTag, Desc : string;
  I : integer;
begin
  try
    ReasonCode := ReadLength(@PByteArray(Buffer)[1], Size);
    Desc := ReadString(@PByteArray(Buffer)[5], Size);
    LangTag := ReadString(@PByteArray(Buffer)[5 + 4 + Length(Desc)], Size);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  FServerCloseReason := (Desc);
  for I := 0 to FChannels.Count - 1 do
    TElSSHClientTunnelConnection(FChannels.Items[I]).DoClose(ctError);
  DoError(ReasonCode);
  DoCloseConnection;
end;

procedure TElSSHClient.SSH2ParseServerDebug(Buffer : pointer; Size : longint);
var
  Len : integer;
begin
  try
    Len := ReadLength(@PByteArray(Buffer)[2], Size);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  if Size < Len + 4 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  DoDataDebug(@PByteArray(Buffer)[6], Len);
end;

procedure TElSSHClient.SSH2ParseServerServiceAccept(Buffer : pointer; Size : longint);
var
  Svc : string;
begin
  try
    Svc := ReadString(@PByteArray(Buffer)[1], Size - 1);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  if (CompareStr(Svc, 'ssh-userauth') = 0) and 
     (FSSH2ClientLastMessage = SSH2_MSG_SERVICE_REQUEST) then
  begin
    SSH2StartClientAuthentication;
  end;
end;

procedure TElSSHClient.SSH2ParseServerUserauthSuccess(Buffer : pointer; Size : longint);
begin
  FSSH2AuthenticationPassed := true;
  if Assigned(FOnAuthenticationSuccess) then
    FOnAuthenticationSuccess(Self);
  DoOpenConnection;
  SSH2EstablishInteractiveSessions;
end;

procedure TElSSHClient.SSH2ParseServerUserauthFailure(Buffer : pointer; Size : longint);
var
  AuthTypes : string;
  PartialSuccess : boolean;
  Index : integer;
begin
  // sending one by one
  // disabling last unsuccessful method
  SetLength(FSSH2UserauthServerAlgs, Size);
  Move(Buffer^, FSSH2UserauthServerAlgs[1], Size);

  try
    AuthTypes := ReadString(@PByteArray(Buffer)[1], Size - 1);
    Index := 5 + Length(AuthTypes);
    PartialSuccess := ReadBoolean(@PByteArray(Buffer)[Index], Size);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;

  if (FSSH2LastUserauthMethod = SSH_AUTH_TYPE_PASSWORD) or
    (FSSH2LastUserauthMethod = SSH_AUTH_TYPE_KEYBOARD) then
  begin
    FSSH2EnabledAuthTypes := FSSH2EnabledAuthTypes and not FSSH2LastUserauthMethod;
    if not PartialSuccess then
      DoAuthenticationFailed(FSSH2LastUserauthMethod);
  end
  else if (FSSH2LastUserauthMethod = SSH_AUTH_TYPE_PUBLICKEY) and ((FKeyStorage = nil) or (FSSH2LastKeyIndex > FKeyStorage.Count)) then
    FSSH2EnabledAuthTypes := FSSH2EnabledAuthTypes and not (SSH_AUTH_TYPE_PUBLICKEY)
  else if (FSSH2LastUserauthMethod = SSH_AUTH_TYPE_HOSTBASED) and ((FKeyStorage = nil) or (FSSH2LastKeyIndex > FKeyStorage.Count)) then
    FSSH2EnabledAuthTypes := FSSH2EnabledAuthTypes and not (SSH_AUTH_TYPE_HOSTBASED);

  // trying next sequence of auths: KbdInt, Password, PublicKey, Hostbased
  if ((FSSH2EnabledAuthTypes and SSH_AUTH_TYPE_KEYBOARD) > 0) and
    ((FAuthenticationTypes and SSH_AUTH_TYPE_KEYBOARD) > 0) and
    (FAuthOrder = aoDefault) and
    (Pos('keyboard-interactive', AuthTypes) > 0) then
  begin
    SSH2EstablishAuthKeyboardInteractive;
  end
(*  else if ((FSSH2EnabledAuthTypes and SSH_AUTH_TYPE_PASSWORD) > 0) and
    ((FAuthenticationTypes and SSH_AUTH_TYPE_PASSWORD) > 0) and
    (Pos('password', AuthTypes) > 0) then
  begin
    SSH2SendUserauthRequestPassword;
  end
*)  else if ((FSSH2EnabledAuthTypes and SSH_AUTH_TYPE_PUBLICKEY) > 0) and
    ((FAuthenticationTypes and SSH_AUTH_TYPE_PUBLICKEY) > 0) and
    (Pos('publickey', AuthTypes) > 0) then
  begin
    if FSSH2LastUserauthMethod <> SSH_AUTH_TYPE_PUBLICKEY then
    begin
      FSSH2LastKeyIndex := 0;
      FWrkArdFSecure1 := false; 
    end;
    SSH2EstablishAuthPublicKey(FSSH2LastKeyIndex);
  end
  else if ((FSSH2EnabledAuthTypes and SSH_AUTH_TYPE_PASSWORD) > 0) and
    ((FAuthenticationTypes and SSH_AUTH_TYPE_PASSWORD) > 0) and
    (Pos('password', AuthTypes) > 0) then
  begin
    SSH2SendUserauthRequestPassword;
  end
  else if ((FSSH2EnabledAuthTypes and SSH_AUTH_TYPE_HOSTBASED) > 0) and
    ((FAuthenticationTypes and SSH_AUTH_TYPE_HOSTBASED) > 0) and
    (Pos('hostbased', AuthTypes) > 0) then
  begin
    if FSSH2LastUserauthMethod <> SSH_AUTH_TYPE_HOSTBASED then
      FSSH2LastKeyIndex := 0;
    SSH2EstablishAuthHostbased(FSSH2LastKeyIndex);
  end
  else
  if ((FSSH2EnabledAuthTypes and SSH_AUTH_TYPE_KEYBOARD) > 0) and
    ((FAuthenticationTypes and SSH_AUTH_TYPE_KEYBOARD) > 0) and
    (FAuthOrder = aoKbdIntLast) and 
    (Pos('keyboard-interactive', AuthTypes) > 0) then
  begin
    SSH2EstablishAuthKeyboardInteractive;
  end
  else
  begin
    DoError(ERROR_SSH_NO_MORE_AUTH_METHODS_AVAILABLE);
    CloseByError(SDisconnectNoMoreAuthMethodsAvailable);
    Exit;
  end;
end;

procedure TElSSHClient.SSH2ParseServerUserauthPKOK(Buffer : pointer; Size : longint);
begin
//  Checking server response to be equal for our FSSH2UserauthAlgName an KeyBlob
  SSH2SendUserauthRequestPublickeySignature;
end;

procedure TElSSHClient.SSH2ParseServerUserauthInfoRequest(Buffer : pointer; Size : longint);
var
  Name, Instruction, Lang, Tmp : string;
  Prompt, Response : TStringList;
  Echo : TBits;
  Num, Index, I : integer;
  B : boolean;
begin
  Echo := nil;
  Prompt := TStringList.Create;
  Response := TStringList.Create;
  try
    Echo := TBits.Create;
    Index := 1;
    try
      Name := ReadString(@PByteArray(Buffer)[Index], Size - Index);

      Inc(Index, Length(Name) + 4);
      Instruction := ReadString(@PByteArray(Buffer)[Index], Size - Index);
      Inc(Index, Length(Instruction) + 4);
      Lang := ReadString(@PByteArray(Buffer)[Index], Size - Index);
      Inc(Index, Length(Lang) + 4);
      Num := ReadLength(@PByteArray(Buffer)[Index], Size - Index);
    except
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    Inc(Index, 4);
    Echo.Size := Num;
    for I := 0 to Num - 1 do
    begin
      try
        Tmp := ReadString(@PByteArray(Buffer)[Index], Size - Index);
        Inc(Index, Length(Tmp) + 4);
        B := ReadBoolean(@PByteArray(Buffer)[Index], Size - Index);
        Inc(Index);
        Prompt.Add((Tmp));
        Echo[I] := B;
      except
        DoError(ERROR_SSH_INVALID_PACKET);
        CloseByError(SDisconnectInvalidPacket);
        Exit;
      end;
    end;
    if Assigned(FOnAuthenticationKeyboard) then
      FOnAuthenticationKeyboard(Self, Prompt, Echo, Response);
    SSH2SendUserauthInfoResponse(Response, Num);
  finally
    Prompt.Free;
    Response.Free;
    Echo.Free;
  end;
end;

procedure TElSSHClient.SSH2ParseServerUserauthBanner(Buffer : pointer; Size : longint);
var
  Msg, Lang : string;
  Index : integer;
begin
  try
    Index := 1;
    Msg := ReadString(@PByteArray(Buffer)[Index], Size - Index);
    Inc(Index, Length(Msg) + 4);
    Lang := ReadString(@PByteArray(Buffer)[Index], Size - Index);
  except
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
  end;
  if Assigned(FOnBanner) then
    FOnBanner(Self, Msg, Lang);
end;

procedure TElSSHClient.SSH2ParseServerChannelOpenConfirmation(Buffer : pointer; Size :
  longint);
var
  LocCh, RemCh, WinSize{, MaxPacketSize} : cardinal;
  Conn : TElSSHClientTunnelConnection;
begin
  try
    LocCh := ReadLength(@PByteArray(Buffer)[1], Size);
    RemCh := ReadLength(@PByteArray(Buffer)[5], Size);
    WinSize := ReadLength(@PByteArray(Buffer)[9], Size);
    {MaxPacketSize := }ReadLength(@PByteArray(Buffer)[13], Size);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  Conn := SSH1GetTunnelConnectionByLocalChannel(LocCh);
  if Conn = nil then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  Conn.RemoteChannel := RemCh;
  Conn.WindowSize := WinSize;
  if {TElSSHClientTunnel(}Conn.Tunnel{)} is TElShellSSHTunnel then
  begin
    SSH2EstablishShell(Conn);
  end
  else
  if {TElSSHClientTunnel(}Conn.Tunnel{)} is TElCommandSSHTunnel then
  begin
    SSH2EstablishCommand(Conn);
  end
  else
  if {TElSSHClientTunnel(}Conn.Tunnel{)} is TElSubsystemSSHTunnel then
  begin
    SSH2EstablishSubsystem(Conn);
  end
  else
  if {TElSSHClientTunnel(}Conn.Tunnel{)} is TElLocalPortForwardSSHTunnel then
  begin
    TElSSHClientTunnel(Conn.Tunnel).DoOpen(Conn);
  end
  else
  if {TElSSHClientTunnel(}Conn.Tunnel{)} is TElX11ForwardSSHTunnel then
  begin
    SSH2EstablishX11(Conn);
  end;
end;

procedure TElSSHClient.SSH2ParseServerChannelSuccess(Buffer : pointer; Size : longint);
var
  Ch : cardinal;
  Conn : TElSSHClientTunnelConnection;
  CmdIndex : integer;
begin
  try
    Ch := ReadLength(@PByteArray(Buffer)[1], Size);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  Conn := SSH1GetTunnelConnectionByLocalChannel(Ch);
  if Conn = nil then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  if (Conn.InternalState = SSH2_CONN_INTERNAL_STATE_PTY_REQUEST_SENT) then
  begin
    // sending environment variables
    if Conn.Tunnel is TElShellSSHTunnel then
    begin
      SSH2SendEnvironment(Conn.RemoteChannel, TElShellSSHTunnel(Conn.Tunnel).Environment);
      Conn.InternalState := SSH2_CONN_INTERNAL_STATE_SHELL_REQUEST_SENT;
      SSH2SendChannelRequestShell(Conn.RemoteChannel);
    end
    else if Conn.Tunnel is TElCommandSSHTunnel then
    begin
      SSH2SendEnvironment(Conn.RemoteChannel, TElCommandSSHTunnel(Conn.Tunnel).Environment);
      Conn.InternalState := SSH2_CONN_INTERNAL_STATE_EXEC_REQUEST_SENT;
      CmdIndex := TElCommandSSHTunnel(Conn.Tunnel).ActiveCommand;
      if CmdIndex < TElCommandSSHTunnel(Conn.Tunnel).Commands.Count then
        SSH2SendChannelRequestExec(Conn.RemoteChannel, TElCommandSSHTunnel(Conn.Tunnel).Commands[CmdIndex])
      else
        SSH2SendChannelRequestExec(Conn.RemoteChannel, '');
    end
    else if Conn.Tunnel is TElSubsystemSSHTunnel then
    begin
// II 030603
//      SSH2SendEnvironment(Conn.RemoteChannel, TElSubsystemSSHTunnel(Conn.FTunnel).FEnvironment);
      Conn.InternalState := SSH2_CONN_INTERNAL_STATE_SUBSYSTEM_REQUEST_SENT;
      SSH2SendChannelRequestSubsystem(Conn.RemoteChannel, TElSubsystemSSHTunnel(Conn.Tunnel).Subsystem);
    end
    else
    if Conn.Tunnel is TElX11ForwardSSHTunnel then
    begin
      SSH2SendEnvironment(Conn.RemoteChannel, TElX11ForwardSSHTunnel(Conn.Tunnel).Environment);
      Conn.InternalState := SSH2_CONN_INTERNAL_STATE_X_REQUEST_SENT;
      SSH2SendChannelRequestX11(Conn.RemoteChannel,
        TElX11ForwardSSHTunnel(Conn.Tunnel).AuthenticationProtocol,
        TElX11ForwardSSHTunnel(Conn.Tunnel).ScreenNumber);
    end;
    SSH2EstablishSingleInteractiveSession(FLastTunnelIndex + 1);
  end
  else if (Conn.InternalState = SSH2_CONN_INTERNAL_STATE_SHELL_REQUEST_SENT) then
  begin
    TElSSHClientTunnel(Conn.Tunnel).DoOpen(Conn);
    SSH2EstablishSingleInteractiveSession(FLastTunnelIndex + 1); // II 170204
  end
  else if (Conn.InternalState = SSH2_CONN_INTERNAL_STATE_EXEC_REQUEST_SENT) then
  begin
    TElSSHClientTunnel(Conn.Tunnel).DoOpen(Conn);
    SSH2EstablishSingleInteractiveSession(FLastTunnelIndex + 1); // II 170204
  end
  else if (Conn.InternalState = SSH2_CONN_INTERNAL_STATE_SUBSYSTEM_REQUEST_SENT) then
  begin
//    DoOpenConnection; // II 170204
    TElSSHClientTunnel(Conn.Tunnel).DoOpen(Conn);
    SSH2EstablishSingleInteractiveSession(FLastTunnelIndex + 1); // II 170204
  end;
end;

procedure TElSSHClient.SSH2ParseServerChannelFailure(Buffer : pointer; Size : longint);
var
  Ch : cardinal;
  Conn : TElSSHClientTunnelConnection;
  CmdIndex : integer;
begin
  try
    Ch := ReadLength(@PByteArray(Buffer)[1], Size);
    Conn := SSH1GetTunnelConnectionByLocalChannel(Ch);
    if Conn = nil then
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    (*
    FChannels.Remove(Conn);
    Conn.Tunnel.RemoveConnection(Conn);
    TElSSHClientTunnel(Conn.Tunnel).DoError(SSH_TUNNEL_ERROR_OPEN_FAILED, Conn.Data);
    {$ifndef CHROME}
    Conn.Free;
    {$else}
    FreeAndNil(Conn);
    {$endif}
    *)
    if (Conn.InternalState = SSH2_CONN_INTERNAL_STATE_PTY_REQUEST_SENT) then
    begin
      if Conn.Tunnel is TElShellSSHTunnel then
      begin
        Conn.InternalState := SSH2_CONN_INTERNAL_STATE_SHELL_REQUEST_SENT;
        SSH2SendChannelRequestShell(Conn.RemoteChannel);
      end
      else if Conn.Tunnel is TElCommandSSHTunnel then
      begin
        Conn.InternalState := SSH2_CONN_INTERNAL_STATE_EXEC_REQUEST_SENT;
        CmdIndex := TElCommandSSHTunnel(Conn.Tunnel).ActiveCommand;
        if CmdIndex < TElCommandSSHTunnel(Conn.Tunnel).Commands.Count then
          SSH2SendChannelRequestExec(Conn.RemoteChannel, TElCommandSSHTunnel(Conn.Tunnel).Commands[CmdIndex])
        else
          SSH2SendChannelRequestExec(Conn.RemoteChannel, '');
      end
      else if Conn.Tunnel is TElSubsystemSSHTunnel then
      begin
        Conn.InternalState := SSH2_CONN_INTERNAL_STATE_SUBSYSTEM_REQUEST_SENT;
        SSH2SendChannelRequestSubsystem(Conn.RemoteChannel, TElSubsystemSSHTunnel(Conn.Tunnel).Subsystem);
      end
      else
      if Conn.Tunnel is TElX11ForwardSSHTunnel then
      begin
        Conn.InternalState := SSH2_CONN_INTERNAL_STATE_X_REQUEST_SENT;
        SSH2SendChannelRequestX11(Conn.RemoteChannel,
          TElX11ForwardSSHTunnel(Conn.Tunnel).AuthenticationProtocol,
          TElX11ForwardSSHTunnel(Conn.Tunnel).ScreenNumber);
      end;
      SSH2EstablishSingleInteractiveSession(FLastTunnelIndex + 1);
    end;

  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
end;

procedure TElSSHClient.SSH2ParseServerChannelData(Buffer : pointer; Size : longint);
var
  Ch, Len : cardinal;
  Conn : TElSSHClientTunnelConnection;
begin
  {$ifdef SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('ElSSHClient::ParseServerChannelData');
  {$endif}
  try
    Ch := ReadLength(@PByteArray(Buffer)[1], Size);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  Conn := SSH1GetTunnelConnectionByLocalChannel(Ch);

  Len := ReadLength(@PByteArray(Buffer)[5], Size);

  Conn.LocalWindowSize := Conn.LocalWindowSize - Integer(Len);

  if (Conn.LocalWindowSize < 2048) then
  begin
    Conn.LocalWindowSize := Conn.LocalWindowSize + 65536;
    SSH2SendChannelWindowAdjust(Conn.RemoteChannel, 65536);
  end;
  
  if Conn <> nil then
    Conn.DoData(@PByteArray(Buffer)[9], Len);
end;

procedure TElSSHClient.SSH2ParseServerChannelExtendedData(Buffer : pointer; Size : longint);
var
  Ch{, DataType}, Len : cardinal;
  Conn : TElSSHClientTunnelConnection;
begin
  try
    Ch := ReadLength(@PByteArray(Buffer)[1], Size);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  Conn := SSH1GetTunnelConnectionByLocalChannel(Ch);
  try
    {DataType := }ReadLength(@PByteArray(Buffer)[5], Size);
    Len := ReadLength(@PByteArray(Buffer)[9], Size);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;

  Conn.LocalWindowSize := Conn.LocalWindowSize - Integer(Len);
  if (Conn.LocalWindowSize < 2048) then
  begin
    Conn.LocalWindowSize := Conn.LocalWindowSize + 65536;
    SSH2SendChannelWindowAdjust(Conn.RemoteChannel, 65536);
  end;

  if Conn <> nil then
    Conn.DoExtendedData(@PByteArray(Buffer)[13], Len);
end;

procedure TElSSHClient.SSH2ParseServerChannelOpen(Buffer : pointer; Size : longint);
var
  Str, Addr, OrigIP : string;
  Ch : cardinal;
  WinSize {MaxPacketSize, } : cardinal;
  Port  : integer;
  Index : integer;
  Conn : TElSSHClientTunnelConnection;
  Tunnel : TElCustomSSHTunnel;
begin
  try
    Str := ReadString(@PByteArray(Buffer)[1], Size - 1);
    Ch := ReadLength(@PByteArray(Buffer)[5 + Length(Str)], Size - Length(Str) - 5);
    Index := 9 + Length(Str);
    WinSize := ReadLength(@PByteArray(Buffer)[Index], Size - Index);
    Inc(Index, 4);
    {MaxPacketSize := }ReadLength(@PByteArray(Buffer)[Index], Size - Index);
    Inc(Index, 4);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  if CompareStr(Str, 'forwarded-tcpip') = 0 then
  begin
    try
      Addr := ReadString(@PByteArray(Buffer)[Index], Size - Index);
      Inc(Index, 4 + Length(Addr));
      Port := ReadLength(@PByteArray(Buffer)[Index], Size - Index);
      Inc(Index, 4);
      OrigIP := ReadString(@PByteArray(Buffer)[Index], Size - Index);
    except
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    Tunnel := nil;
    Conn := nil;
    if Assigned(FTunnelList) then
    begin
      for Index := 0 to FTunnelList.Count - 1 do
      begin
        if (FTunnelList.Tunnels[Index] is TElRemotePortForwardSSHTunnel) and
          (TElRemotePortForwardSSHTunnel(FTunnelList.Tunnels[Index]).Port = Port) then
        begin
          Tunnel := FTunnelList.Tunnels[Index];
          Break;
        end;
      end;
    end;
    if Assigned(Tunnel) then
    begin
      SSH2CreateChannel(Tunnel, Conn);
      Conn.RemoteChannel := Ch;
      Conn.WindowSize := WinSize;
      Conn.LocalWindowSize := 32768;
      SSH2SendChannelOpenConfirmation(Conn.RemoteChannel, 
        Conn.LocalChannel,
        32768, 32768); // 16384
      TElSSHClientTunnel(Tunnel).DoOpen(Conn);
    end
    else
    begin
      // sending error info and ChannelOpenFailure
    end;
  end
  else if CompareStr(Str, 'x11') = 0 then
  begin
    try
      OrigIP := ReadString(@PByteArray(Buffer)[Index], Size - Index);
      Inc(Index, 4 + Length(OrigIP));
      {Port := }ReadLength(@PByteArray(Buffer)[Index], Size - Index);
    except
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    Tunnel := nil;
    Conn := nil;
    if Assigned(FTunnelList) then
    begin
      for Index := 0 to FTunnelList.Count - 1 do
      begin
        if (FTunnelList.Tunnels[Index] is TElX11ForwardSSHTunnel) then
        begin
          Tunnel := FTunnelList.Tunnels[Index];
          Break;
        end;
      end;
    end;
    if Assigned(Tunnel) then
    begin
      SSH2CreateChannel(Tunnel, Conn);
      Conn.RemoteChannel := Ch;
      Conn.WindowSize := WinSize;
      Conn.LocalWindowSize := 32768;
      SSH2SendChannelOpenConfirmation(Conn.RemoteChannel,
        Conn.LocalChannel,
        32768, 32768); // 16384
      TElSSHClientTunnel(Tunnel).DoOpen(Conn);
    end
    else
    begin
      DoError(ERROR_SSH_INTERNAL_ERROR);
      CloseByError(SDisconnectInternalError);
      Exit;
    end;
  end;
end;

procedure TElSSHClient.SSH2ParseServerChannelOpenFailure(Buffer : pointer; Size : longint);
var
  LocCh, ReasonCode : cardinal;
  Info : string;
  Conn : TElSSHClientTunnelConnection;
begin
  try
    LocCh := ReadLength(@PByteArray(Buffer)[1], Size - 1);
    ReasonCode := ReadLength(@PByteArray(Buffer)[5], Size - 5);
    Info := ReadString(@PByteArray(Buffer)[9], Size - 9);
    if Length(Info) > 0 then
      DoDataDebug(@Info[1], Length(Info));
    FErrorString := (Info);
    Conn := SSH1GetTunnelConnectionByLocalChannel(LocCh);
    if Assigned(Conn) then
    begin
      FChannels.Remove(Conn);
      Conn.Tunnel.RemoveConnection(Conn);
      TElSSHClientTunnel(Conn.Tunnel).DoError(ReasonCode, Conn.Data);
      Conn.Free;
    end
    else
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
end;

procedure TElSSHClient.SSH2ParseServerChannelEOF(Buffer : pointer; Size : longint);
var
  LocCh : cardinal;
  Conn : TElSSHClientTunnelConnection;
begin
  try
    LocCh := ReadLength(@PByteArray(Buffer)[1], Size - 1);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  // closing the channel
  Conn := SSH1GetTunnelConnectionByLocalChannel(LocCh);
  if Conn = nil then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  if not Conn.LocalChannelClosed then
  begin
    SSH2SendChannelClose(Conn.RemoteChannel);
    Conn.LocalChannelClosed := true;
  end;
end;

procedure TElSSHClient.SSH2ParseServerChannelClose(Buffer : pointer; Size : longint);
var
  LocCh : cardinal;
  Conn : TElSSHClientTunnelConnection;
  CmdTunnel : TElCommandSSHTunnel;
  CmdData : pointer ;
begin
  try
    LocCh := ReadLength(@PByteArray(Buffer)[1], Size - 1);
  except
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  Conn := SSH1GetTunnelConnectionByLocalChannel(LocCh);
  if Conn = nil then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  Conn.RemoteChannelClosed := true;
  if not Conn.LocalChannelClosed then
  begin
    SSH2SendChannelClose(Conn.RemoteChannel);
    Conn.LocalChannelClosed := true;
  end;

  if Conn.Tunnel is TElCommandSSHTunnel then
  begin
    CmdTunnel := TElCommandSSHTunnel(Conn.Tunnel);
    CmdData := Conn.Data;
  end  
  else
  begin
    CmdTunnel := nil;
    CmdData := nil;//just to prevent compiler warning
  end;

  SSH2DestroyChannel(Conn);

  if Assigned(CmdTunnel) then
  begin
    //running next command in list
    if CmdTunnel.ActiveCommand < CmdTunnel.Commands.Count - 1 then
    begin
      CmdTunnel.ActiveCommand := CmdTunnel.ActiveCommand + 1;
      Conn := nil;
      SSH2CreateChannel(CmdTunnel, Conn);
      Conn.Data := CmdData;
      SSH2SendChannelOpenSession(Conn.LocalChannel);
    end;
  end;
end;

procedure TElSSHClient.SSH2ParseServerChannelRequest(Buffer : pointer; Size : longint);
var
  LocCh : cardinal;
  Succ : boolean;
  Signal : string;
  S : string;
  B : boolean;
  ExitStatus : cardinal;
  Index : integer;
  Conn : TElSSHClientTunnelConnection;
  procedure RaiseSpecException;
  begin
    raise EElSSHSilentException.Create(SInternalUsedException);
  end;
begin
  try
    LocCh := ReadLength(@PByteArray(Buffer)[1], Size - 1);
    S := ReadString(@PByteArray(Buffer)[5], Size - 5);
    B := ReadBoolean(@PByteArray(Buffer)[9 + Length(S)], Size - 9);
  except                   
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  Succ := true;
  if CompareStr(S, 'exit-status') = 0 then
  begin
    try
      if B then
        RaiseSpecException;
      ExitStatus := ReadLength(@PByteArray(Buffer)[10 + Length(S)], Size - 10);
    except
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    Conn := SSH1GetTunnelConnectionByLocalChannel(LocCh);
    if Assigned(Conn) then
    begin
      Conn.ExitStatus := ExitStatus;
      Conn.CloseType := ctReturn;
    end;
  end
  else if CompareStr(S, 'exit-signal') = 0 then
  begin
    try
      if B then
        RaiseSpecException;
      Signal := ReadString(@PByteArray(Buffer)[10 + Length(S)], Size - 10);
    except
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
    Index := 10 + Length(S) + 4 + Length(Signal);
    {B :=}ReadBoolean(@PByteArray(Buffer)[Index], Size - Index); // core dumped flag
    Inc(Index);
    try
      S := ReadString(@PByteArray(Buffer)[Index], Size - Index);
    except
      S := '';
    end;
    Conn := SSH1GetTunnelConnectionByLocalChannel(LocCh);
    if Assigned(Conn) then
    begin
      Conn.ExitStatus := 0;
      Conn.ExitSignal := (Signal);
      Conn.ExitMessage := (S);
      Conn.CloseType := ctSignal;
    end;
    // parsing more
  end
  else if CompareStr(S, 'keepalive@openssh.com') = 0 then
  begin
    // keep alive signal, just ignoring
    Succ := false; // we should send Failure message 
  end
  else
    Succ := false;
  if B then
  begin
    Conn := SSH1GetTunnelConnectionByLocalChannel(LocCh);
    if Assigned(Conn) then
    begin
      if Succ then
        SSH2SendChannelSuccess(Conn.RemoteChannel)
      else
        SSH2SendChannelFailure(Conn.RemoteChannel);
    end      
    else
    begin
      DoError(ERROR_SSH_PROTOCOL_ERROR);
      CloseByError(SDisconnectInvalidPacket);
    end;
  end;
end;

procedure TElSSHClient.SSH2ParseServerChannelWindowAdjust(Buffer : pointer; Size :
  longint);
var
  LocCh : cardinal;
  BytesToAdd : cardinal;
  Conn : TElSSHClientTunnelConnection;
begin
  try
    LocCh := ReadLength(@PByteArray(Buffer)[1], Size - 1);
    BytesToAdd := ReadLength(@PByteArray(Buffer)[5], Size - 5);
    Conn := SSH1GetTunnelConnectionByLocalChannel(LocCh);
    if Conn <> nil then
    begin
      Conn.WindowSize := Conn.WindowSize + Integer(BytesToAdd);

      Conn.SendData(nil, 0);
    end;
  except
    on E : Exception do
    begin
      DoError(ERROR_SSH_INVALID_PACKET);
      CloseByError(SDisconnectInvalidPacket);
      Exit;
    end;
  end;
end;

{ ActualData begins from FOutBuffer[5], Size = Size(ActualData) }
procedure TElSSHClient.SSH2SendOnTransportLayer(Size : longint);
var
  PaddingLen : byte;
  I : integer;
  MAC : string;
  BlSize : integer;
begin
  FSSH2ClientLastMessage := PByteArray(FOutBuffer)[5];
  {$ifdef SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('ElSSHClient::SendOnTransportLayer(packet=' + IntToStr(FSSH2ClientLastMessage) + ')');
  Dumper.WriteBinary(@PByteArray(FOutBuffer)[5], Size);
  {$endif}

  { Compressing }
  if (FSSH2Params.CompAlgorithmCS = SSH_CA_ZLIB) and 
     (FSSH2State = stEncrypted) then
  begin
    try
      SSH2CompressPacket(@PByteArray(FOutBuffer)[5], Size, @PByteArray(FOutBuffer)[5], I);
    except
      on E : Exception do
      begin
        DoError(ERROR_SSH_COMPRESSION_ERROR);
        CloseByError(E.Message);
        Exit;
      end;
    end;
    Size := I;
  end;

  BlSize := Max(8, FSSH2Params.EncCSBlockSize);
  PaddingLen := BlSize - (Size + 5) mod BlSize;
  if PaddingLen < 4 then
    Inc(PaddingLen, BlSize);
  PByteArray(FOutBuffer)[0] := (Size + PaddingLen + 1) shr 24;
  PByteArray(FOutBuffer)[1] := ((Size + PaddingLen + 1) shr 16) and $FF;
  PByteArray(FOutBuffer)[2] := ((Size + PaddingLen + 1) shr 8) and $FF;
  PByteArray(FOutBuffer)[3] := (Size + PaddingLen + 1) and $FF;
  PByteArray(FOutBuffer)[4] := PaddingLen;
  FillChar(PByteArray(FOutBuffer)[5 + Size], PaddingLen, 0);
  { Encrypting }
  if FSSH2State = stEncrypted then
  begin
    {$ifdef SECURE_BLACKBOX_DEBUG}
    DoDebug('Encrypting data: ', SSH_DEBUG_LEVEL_PACKET_DATA);
    DoDebug(FormatBuf(FOutBuffer, 5 + Size + PaddingLen), SSH_DEBUG_LEVEL_PACKET_DATA);
    {$endif}
    for I := 0 to PaddingLen - 1 do
      PByteArray(FOutBuffer)[5 + Size + i] := SBRndGenerate(255);
    MAC := SSH2ComputeMAC(FOutBuffer, 5 + Size + PaddingLen, true);
    SSH2EncryptPacket(FOutBuffer, FOutBuffer, 5 + Size + PaddingLen);
  end;
  { Sending }
  Inc(FSSH2ClientSequenceNumber);
(*
  if FSSH2State = {$ifdef CHROME}TSSH1State.{$endif}stEncrypted then
  begin
    Move(MAC[1], PByteArray(FOutBuffer)[5 + Size + PaddingLen], Length(MAC));
    DoSend(FOutBuffer, 5 + Size + PaddingLen + Length(MAC));
  end
  else
    DoSend(FOutBuffer, 5 + Size + PaddingLen);
*)
  DoSend(FOutBuffer, 5 + Size + PaddingLen);
  if FSSH2State = stEncrypted then
    DoSend(@MAC[1], Length(MAC));
end;


procedure TElSSHClient.SSH2SendDisconnect(ReasonCode : integer; Desc : string);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_DISCONNECT;
  Tmp := WriteUINT32(ReasonCode) + WriteString(Desc) + WriteString('');
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendKexInit;
var
  P : pointer;
  S : string;
  I : integer;
begin
  P := @PByteArray(FOutBuffer)[5];
  PByteArray(P)[0] := SSH2_MSG_KEXINIT;
  for I := 0 to 15 do
  begin
    FClientCookie[I] := SBRndGenerate(255);
    PByteArray(P)[I + 1] := FClientCookie[I];
  end;
  S := WriteKexAlgorithms;
  I := 17;
  Move(S[1], PByteArray(P)[I], Length(S));
  Inc(I, Length(S));
  S := WriteServerHostKeyAlgorithms;
  Move(S[1], PByteArray(P)[I], Length(S));
  Inc(I, Length(S));
  S := WriteEncAlgorithmsCS;
  Move(S[1], PByteArray(P)[I], Length(S));
  Inc(I, Length(S));
  S := WriteEncAlgorithmsSC;
  Move(S[1], PByteArray(P)[I], Length(S));
  Inc(I, Length(S));
  S := WriteMACAlgorithmsCS;
  Move(S[1], PByteArray(P)[I], Length(S));
  Inc(I, Length(S));
  S := WriteMACAlgorithmsSC;
  Move(S[1], PByteArray(P)[I], Length(S));
  Inc(I, Length(S));
  S := WriteCompAlgorithmsCS;
  Move(S[1], PByteArray(P)[I], Length(S));
  Inc(I, Length(S));
  S := WriteCompAlgorithmsSC;
  Move(S[1], PByteArray(P)[I], Length(S));
  Inc(I, Length(S));
  S := WriteLanguagesCS;
  Move(S[1], PByteArray(P)[I], Length(S));
  Inc(I, Length(S));
  S := WriteLanguagesSC;
  Move(S[1], PByteArray(P)[I], Length(S));
  Inc(I, Length(S));
  PByteArray(P)[I] := 0;
  PByteArray(P)[I + 1] := 0;
  PByteArray(P)[I + 2] := 0;
  PByteArray(P)[I + 3] := 0;
  PByteArray(P)[I + 4] := 0;
  SetLength(FHandshakeParams.ClientKexInit, I + 5);
  Move(P^, FHandshakeParams.ClientKexInit[1], I + 5);
  {$ifdef SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('Sending KEXINIT');
  Dumper.WriteString(FHandshakeParams.ClientKexInit);
  {$endif}
  SSH2SendOnTransportLayer(I + 5);
end;

procedure TElSSHClient.SSH2SendKexDHInit;
var
  P : pointer;
  S : string;
  Sz : integer;
begin
  P := @PByteArray(FOutBuffer)[5];
  PByteArray(P)[0] := SSH2_MSG_KEXDH_INIT;
  Sz := FDHParams.E^.Length * 4;
  LIntToPointer(FDHParams.E, @PByteArray(P)[5], Sz);
  S := WriteSSH2MPInt(@PByteArray(P)[5], Sz);
  Move(S[1], PByteArray(P)[1], Length(S));
  SSH2SendOnTransportLayer(Length(S) + 1);
end;

procedure TElSSHClient.SSH2SendKexDHGexRequest;
var
  P : pointer;
  S : string;
begin
  P := @PByteArray(FOutBuffer)[5];
  PByteArray(P)[0] := SSH2_MSG_KEX_DH_GEX_REQUEST;
  S := WriteUINT32(1024);
  FHandshakeParams.Min := S;
  Move(S[1], PByteArray(P)[1], Length(S));
  // S := WriteUINT32(1024);
  FHandshakeParams.N := S;
  Move(S[1], PByteArray(P)[5], Length(S));
  S := WriteUINT32(8192);
  FHandshakeParams.Max := S;
  Move(S[1], PByteArray(P)[9], Length(S));
  SSH2SendOnTransportLayer(13);
end;

procedure TElSSHClient.SSH2SendKexDHGexInit;
var
  P : pointer;
  S : string;
  Buf : array[0..10000] of byte;
  Sz : integer;
begin
  P := @PByteArray(FOutBuffer)[5];
  PByteArray(P)[0] := SSH2_MSG_KEX_DH_GEX_INIT;

  // II 100603 start
///  LMModPower(FDHParams.G, FDHParams.X, FDHParams.P, FDHParams.E);
///  SSH2KexDHPower(FDHParams.G, FDHParams.X, FDHParams.P, FDHParams.E); // was this
  LSSHModPower(FDHParams.G, FDHParams.X, FDHParams.P, FDHParams.E);
  // II 100603 end

  Sz := 10000;
  LintToPointer(FDHParams.E, @Buf[0], Sz);
  S := WriteSSH2MPInt(@Buf[0], Sz);
  Move(S[1], PByteArray(P)[1], Length(S));
  FHandshakeParams.E := S;
  SSH2SendOnTransportLayer(1 + Length(S));
end;

procedure TElSSHClient.SSH2SendNewKeys;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_NEWKEYS;
  SSH2SendOnTransportLayer(1);
end;

procedure TElSSHClient.SSH2SendServiceRequest(ServiceName : string);
var
  SN : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_SERVICE_REQUEST;
  SN := WriteUINT32(Length(ServiceName));
  Move(SN[1], PByteArray(FOutBuffer)[6], 4);
  Move(ServiceName[1], PByteArray(FOutBuffer)[10], Length(ServiceName));
  SSH2SendOnTransportLayer(5 + Length(ServiceName));
end;

procedure TElSSHClient.SSH2SendUserauthRequestNone;
var
  Tmp : string;
begin
  FSSH2LastUserauthMethod := -1;
  PByteArray(FOutBuffer)[5] := SSH2_MSG_USERAUTH_REQUEST;
  Tmp := WriteString(FUsername) + WriteString('ssh-connection') + WriteString('none');
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  // TODO: Check this out
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendUserauthRequestPassword;
var
  Tmp : string;
begin
  FSSH2LastUserauthMethod := SSH_AUTH_TYPE_PASSWORD;
  PByteArray(FOutBuffer)[5] := SSH2_MSG_USERAUTH_REQUEST;
  Tmp := WriteString(FUsername) + WriteString('ssh-connection') + WriteString('password');
  Tmp := Tmp + WriteBoolean(false) + WriteString(FPassword);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  {$ifdef SECURE_BLACKBOX_DEBUG}
  DoDebug('Sending username and password', SSH_DEBUG_LEVEL_GLOBAL_EVENTS);
  {$endif}
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendUserauthRequestPublickey(AlgName : string;
  PublicKeyBlob : string);
var
  Tmp : string;
begin
  FSSH2LastUserauthMethod := SSH_AUTH_TYPE_PUBLICKEY;
  PByteArray(FOutBuffer)[5] := SSH2_MSG_USERAUTH_REQUEST;
  FSSH2UserauthAlgName := WriteString(AlgName);
  FSSH2UserauthKeyBlob := WriteString(PublicKeyBlob);
  Tmp := WriteString(FUserName) + WriteString('ssh-connection') +
    WriteString('publickey') + WriteBoolean(false) + {WriteString(AlgName) +
    WriteString(PublicKeyBlob)} FSSH2UserauthAlgName + FSSH2UserauthKeyBlob;
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendUserauthRequestPublickeySignature;
var
  Tmp, Signature : string;
  ShaCtx : TSHA1Context;
  M160 : TMessageDigest160;
const
  ASN_SHA1_ID = #$30#$21#$30#$09#$06#$05#$2B#$0E#$03#$02#$1A#$05#$00#$04#$14;
begin
  // counting signature
  InitializeSHA1(ShaCtx);
  Tmp := WriteUINT32(Length(FSSH2Params.SessionID));
  HashSHA1(ShaCtx, @Tmp[1], 4);
  HashSHA1(ShaCtx, @FSSH2Params.SessionID[0], Length(FSSH2Params.SessionID));
  Tmp := Chr(SSH2_MSG_USERAUTH_REQUEST);
  HashSHA1(ShaCtx, @Tmp[1], 1);
  Tmp := WriteString(FUsername) + WriteString('ssh-connection') +
    WriteString('publickey') + WriteBoolean(true) + FSSH2UserauthAlgName +
    FSSH2UserauthKeyBlob;
  HashSHA1(ShaCtx, @Tmp[1], Length(Tmp));
  M160 := FinalizeSHA1(ShaCtx);
  SetLength(Tmp, 20);
  Move(PByteArray(@M160)[0], Tmp[1], 20);
  Signature := SSH2CalculateSignature(Tmp, FSSH2LastKeyIndex - 1);
  PByteArray(FOutBuffer)[5] := SSH2_MSG_USERAUTH_REQUEST;
  Tmp := WriteString(FUserName) + WriteString('ssh-connection') +
    WriteString('publickey') + WriteBoolean(true) + FSSH2UserauthAlgName +
    FSSH2UserauthKeyBlob + WriteString(Signature);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendUserauthRequestHostbased(AlgName : string; PublicKeyBlob : string);
var
  Tmp, Signature : string;
  M160 : TMessageDigest160;
  ShaCtx : TSHA1Context;
begin
  FSSH2LastUserauthMethod := SSH_AUTH_TYPE_HOSTBASED;
  PByteArray(FOutBuffer)[5] := SSH2_MSG_USERAUTH_REQUEST;
  FSSH2UserauthAlgName := WriteString(AlgName);
  FSSH2UserauthKeyBlob := WriteString(PublicKeyBlob);

  // counting hash
  InitializeSHA1(ShaCtx);
  Tmp := WriteUINT32(Length(FSSH2Params.SessionID));
  HashSHA1(ShaCtx, @Tmp[1], 4);
  HashSHA1(ShaCtx, @FSSH2Params.SessionID[0], Length(FSSH2Params.SessionID));
  Tmp := Chr(SSH2_MSG_USERAUTH_REQUEST);
  HashSHA1(ShaCtx, @Tmp[1], 1);
  Tmp := WriteString(FUsername) + WriteString('ssh-connection') +
    WriteString('hostbased') + FSSH2UserauthAlgName + FSSH2UserauthKeyBlob +
    WriteString(FClientHostName) + WriteString(FClientUsername);
  HashSHA1(ShaCtx, @Tmp[1], Length(Tmp));


  M160 := FinalizeSHA1(ShaCtx);
  SetLength(Tmp, 20);
  Move(PByteArray(@M160)[0], Tmp[1], 20);
  Signature := SSH2CalculateSignature(Tmp, FSSH2LastKeyIndex - 1);
  PByteArray(FOutBuffer)[5] := SSH2_MSG_USERAUTH_REQUEST;
  Tmp := WriteString(FUserName) + WriteString('ssh-connection') +
    WriteString('hostbased') + FSSH2UserauthAlgName + FSSH2UserauthKeyBlob +
    WriteString(FClientHostName) + WriteString(FClientUsername) +
    WriteString(Signature);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

(*{$ifdef DELPHI_NET}
procedure TElSSHClient.SSH2SendUserauthRequestHostbased(const AlgName : ByteArray; const PublicKeyBlob : ByteArray);
{$else}
procedure TElSSHClient.SSH2SendUserauthRequestHostbased(AlgName : string; PublicKeyBlob : string);
{$endif}
var
  {$ifdef DELPHI_NET}
  Tmp, Signature : ByteArray;
  {$else}
  Tmp, Signature : string;
  {$endif}
  M160 : TMessageDigest160;
  ShaCtx : TSHA1Context;
begin
  FSSH2LastUserauthMethod := SSH_AUTH_TYPE_HOSTBASED;
  PByteArray(FOutBuffer)[5] := SSH2_MSG_USERAUTH_REQUEST;
  {$ifdef DELPHI_NET}
  FSSH2UserauthAlgName := WriteString({BytesOfString(}AlgName{)});
  {$else}
  FSSH2UserauthAlgName := WriteString(AlgName);
  {$endif}
  FSSH2UserauthKeyBlob := WriteString(PublicKeyBlob);
  // counting hash
  InitializeSHA1(ShaCtx);
  Tmp := WriteUINT32(Length(FSSH2Params.SessionID));
  {$ifdef DELPHI_NET}
  HashSHA1(ShaCtx, Tmp, 4);
  HashSHA1(ShaCtx, FSSH2Params.SessionID, Length(FSSH2Params.SessionID));
  SetLength(Tmp, 1);
  Tmp[0] := SSH2_MSG_USERAUTH_REQUEST;
  HashSHA1(ShaCtx, Tmp, 1);

  SetLength(Tmp, 0);
  Tmp := TBufferTypeConst(Tmp) + WriteString(BytesOfString(FUserName));
  Tmp := TBufferTypeConst(Tmp) + WriteString(BytesOfString('ssh-connection'));
  Tmp := TBufferTypeConst(Tmp) + WriteString(BytesOfString('hostbased'));
  Tmp := TBufferTypeConst(Tmp) + {BytesOfString(}FSSH2UserauthAlgName{)};
  Tmp := TBufferTypeConst(Tmp) + {BytesOfString(}FSSH2UserauthKeyBlob{)};
  Tmp := TBufferTypeConst(Tmp) + WriteString(BytesOfString(FClientHostName));
  Tmp := TBufferTypeConst(Tmp) + WriteString(BytesOfString(FClientUsername));
  HashSHA1(ShaCtx, Tmp, Length(Tmp));
  {$else}
  HashSHA1(ShaCtx, @Tmp[1], 4);
  HashSHA1(ShaCtx, @FSSH2Params.SessionID[0], Length(FSSH2Params.SessionID));
  Tmp := Chr(SSH2_MSG_USERAUTH_REQUEST);
  HashSHA1(ShaCtx, @Tmp[1], 1);
  Tmp := WriteString(FUserName) +
    WriteString('ssh-connection') +
    WriteString('hostbased') + FSSH2UserauthAlgName + FSSH2UserauthKeyBlob +
    WriteString(FClientHostName) + WriteString(FClientUsername);
  HashSHA1(ShaCtx, @Tmp[1], Length(Tmp));
  {$endif}
  M160 := FinalizeSHA1(ShaCtx);
  SetLength(Tmp, 20);
  {$ifdef DELPHI_NET}
  Move(StructureToByteArray(M160), 0, Tmp, 0, 20);
  {$else}
  Move(PByteArray(@M160)[0], Tmp[1], 20);
  {$endif}
  Signature := SSH2CalculateSignature(Tmp, FSSH2LastKeyIndex - 1);
  {$ifdef DELPHI_NET}

  Tmp := WriteString(BytesOfString(FUserName));
  Tmp := TBufferTypeConst(Tmp) + WriteString(BytesOfString('ssh-connection'));
  Tmp := TBufferTypeConst(Tmp) + WriteString(BytesOfString('hostbased'));
  Tmp := TBufferTypeConst(Tmp) + {BytesOfString(}FSSH2UserauthAlgName{)};
  Tmp := TBufferTypeConst(Tmp) + {BytesOfString(}FSSH2UserauthKeyBlob{)};
  Tmp := TBufferTypeConst(Tmp) + WriteString(BytesOfString(FClientHostName));
  Tmp := TBufferTypeConst(Tmp) + WriteString(BytesOfString(FClientUsername));
  Tmp := TBufferTypeConst(Tmp) + WriteString(BytesOfString(Signature));

  Move(Tmp, 0, FOutBuffer, 6, Length(Tmp));
  {$else}
  Tmp := WriteString(FUserName) + WriteString('ssh-connection') +
    WriteString('hostbased') + FSSH2UserauthAlgName + FSSH2UserauthKeyBlob +
    WriteString(FClientHostName) + WriteString(FClientUsername) +
    WriteString(Signature);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  {$endif}
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;*)

procedure TElSSHClient.SSH2SendUserauthRequestKeyboardInteractive;
var
  Tmp : string;
begin
  FSSH2LastUserauthMethod := SSH_AUTH_TYPE_KEYBOARD;
  PByteArray(FOutBuffer)[5] := SSH2_MSG_USERAUTH_REQUEST;
  Tmp := WriteString(FUsername) + WriteString('ssh-connection') +
    WriteString('keyboard-interactive') + WriteString('en-US') + WriteString('');
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendUserauthInfoResponse(Response : TStringList; Count : integer);
var
  Tmp : string;
  I : integer;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_USERAUTH_INFO_RESPONSE;
  Tmp := WriteUINT32(Count);
  for I := 0 to Count - 1 do
    if I < Response.Count then
      Tmp := Tmp + WriteString(Response.Strings[I])
    else
      Tmp := Tmp + WriteString('');
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelSuccess(Channel : cardinal);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_SUCCESS;
  Tmp := WriteUINT32(Channel);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelFailure(Channel : cardinal);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_FAILURE;
  Tmp := WriteUINT32(Channel);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelOpenSession(Channel : cardinal);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_OPEN;
  Tmp := WriteString('session') + WriteUINT32(Channel) + WriteUINT32(32768) +
    WriteUINT32(16384);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelOpenDirectTcpIp(Channel : cardinal; Host : string;
  Port : word; OrigIP : string; OrigPort : word);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_OPEN;
  Tmp := WriteString('direct-tcpip') + WriteUINT32(Channel) + WriteUINT32(32768) +
    WriteUINT32(16384) + WriteString(Host) + WriteUINT32(Port) + WriteString(OrigIP) +
    WriteUINT32(OrigPort);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelRequestPty(Channel : cardinal);
var
  Tmp : string;
  TermType : string;
  TermInfo : TElTerminalInfo;
  Tunnel : TElCustomSSHTunnel;
begin
  TermInfo := nil;
  Tunnel := SSH1GetTunnelConnectionByRemoteChannel(Channel).Tunnel;
  if Tunnel is TElShellSSHTunnel then
    TermInfo := TElShellSSHTunnel(Tunnel).TerminalInfo
  else if Tunnel is TElCommandSSHTunnel then
    TermInfo := TElCommandSSHTunnel(Tunnel).TerminalInfo
  else if Tunnel is TElX11ForwardSSHTunnel then
    TermInfo := TElX11ForwardSSHTunnel(Tunnel).TerminalInfo;

  if Assigned(TermInfo) then
    TermType := TermInfo.TerminalType
  else
    TermType := 'vt100';
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_REQUEST;
  Tmp := WriteUINT32(Channel) + WriteString('pty-req') + WriteBoolean(true) +
    WriteString(TermType);
  if Assigned(TermInfo) then
    Tmp := Tmp + WriteUINT32(TermInfo.Cols) + WriteUINT32(TermInfo.Rows) +
      WriteUINT32(TermInfo.Width) + WriteUINT32(TermInfo.Height)
  else
    Tmp := Tmp + WriteUINT32(80) + WriteUINT32(24) + WriteUINT32(0) +
    WriteUINT32(0);
  Tmp := Tmp + WriteString(SSH2EncodePtyModes(TermInfo));
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelRequestShell(Channel : cardinal);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_REQUEST;
  Tmp := WriteUINT32(Channel) + WriteString('shell') + WriteBoolean(true);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelRequestExec(Channel : cardinal; Command : string);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_REQUEST;
  Tmp := WriteUINT32(Channel) + WriteString('exec') + WriteBoolean(true) +
    WriteString(Command);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelRequestSubsystem(Channel : cardinal; SubSystem : string);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_REQUEST;
  Tmp := WriteUINT32(Channel) + WriteString('subsystem') + WriteBoolean(true) +
    WriteString(SubSystem);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelRequestX11(Channel : cardinal; AuthProto : string;
  ScreenNumber : integer);
var
  Tmp : string;
  FakedCookie : string;
  I : integer;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_REQUEST;
  FakedCookie := '';
  for I := 0 to 15 do
    FakedCookie := FakedCookie + IntToHex(SBRndGenerate(256), 2);
  Tmp := WriteUINT32(Channel) + WriteString('x11-req') + WriteBoolean(true) +
    WriteBoolean(false) + WriteString(AuthProto) + WriteString(FakedCookie) +
    WriteUINT32(ScreenNumber);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelRequestSignal(Channel : cardinal; Signal : string);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_REQUEST;
  Tmp := WriteUINT32(Channel) + WriteString('signal') + WriteBoolean(false) +
    WriteString(Signal);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelRequestEnv(Channel : cardinal; VarName : string;
  VarValue : string);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_REQUEST;
  Tmp := WriteUINT32(Channel) + WriteString('env') + WriteBoolean(false) +
    WriteString(VarName) + WriteString(VarValue);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelRequestWindowChange(RemoteChannel: cardinal;
  Cols, Rows, Width, Height : integer);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_REQUEST;
  Tmp := WriteUINT32(RemoteChannel) + WriteString('window-change') +
    WriteBoolean(false) + WriteUINT32(Cols) + WriteUINT32(Rows) +
    WriteUINT32(Width) + WriteUINT32(Height);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;
                                            
procedure TElSSHClient.SSH2SendTunnelData(RemoteChannel : cardinal; Buffer : pointer;
  Size : integer);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_DATA;
  Tmp := WriteUINT32(RemoteChannel) + WriteUINT32(Size);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], 8);
  Move(Buffer^, PByteArray(FOutBuffer)[14], Size);
  SSH2SendOnTransportLayer(9 + Size);
end;

procedure TElSSHClient.SSH2SendChannelEOF(RemoteChannel : cardinal);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_EOF;
  Tmp := WriteUINT32(RemoteChannel);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], 4);
  SSH2SendOnTransportLayer(5);
end;

procedure TElSSHClient.SSH2SendChannelWindowAdjust(RemoteChannel : cardinal;
  WinSize : cardinal);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_WINDOW_ADJUST;
  Tmp := WriteUINT32(RemoteChannel) + WriteUINT32(WinSize);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], 8);
  SSH2SendOnTransportLayer(9);
end;

procedure TElSSHClient.SSH2SendChannelClose(Channel : cardinal);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_CLOSE;
  Tmp := WriteUINT32(Channel);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], 4);
  SSH2SendOnTransportLayer(5);
end;

procedure TElSSHClient.SSH2SendChannelOpenConfirmation(RemoteChannel : cardinal;
  LocalChannel : cardinal; WinSize : integer; MaxPacketSize : integer);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_CHANNEL_OPEN_CONFIRMATION;
  Tmp := WriteUINT32(RemoteChannel) + WriteUINT32(LocalChannel) +
    WriteUINT32(WinSize) + WriteUINT32(MaxPacketSize);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendChannelOpenFailure(RemoteChannel : cardinal;
  ReasonCode : integer; Comment : string; LanguageTag : string);
var
  Tmp : string;
begin
  Tmp := WriteUINT32(RemoteChannel) + WriteUINT32(ReasonCode) +
    WriteString(Comment) + WriteString(LanguageTag);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2SendGlobalRequestTcpIpForward(Addr : string; Port : word);
var
  Tmp : string;
begin
  PByteArray(FOutBuffer)[5] := SSH2_MSG_GLOBAL_REQUEST;
  Tmp := WriteString('tcpip-forward') + WriteBoolean(true) + WriteString({Addr}'0.0.0.0') +
    WriteUINT32(Port);
  Move(Tmp[1], PByteArray(FOutBuffer)[6], Length(Tmp));
  SSH2SendOnTransportLayer(1 + Length(Tmp));
end;

procedure TElSSHClient.SSH2StartClientAuthentication;
begin
  { consequently send all supported authentication requests }
  FSSH2LastKeyIndex := 0;
  FSSH2EnabledAuthTypes := $FFFFFFFF; // making all auth types enabled
  SSH2SendUserauthRequestNone;
end;

procedure TElSSHClient.SSH2EstablishAuthPublicKey(KeyIndex : integer);
var
  Key : TElSSHKey;
  AlgName, PublicKeyBlob : string;
  TmpBuf : string;
  Sz : word;
begin
  FSSH2LastUserauthMethod := SSH_AUTH_TYPE_PUBLICKEY;
  FSSH2LastKeyIndex := KeyIndex + 1;

  if (FKeyStorage <> nil) then
    if (not FWrkArdFSecure1) and (KeyIndex = FKeyStorage.Count) then
    begin
      FWrkArdFSecure1 := true;
      KeyIndex := 0;
      FSSH2LastKeyIndex := 1;
    end;

  if (FKeyStorage <> nil) and (KeyIndex < FKeyStorage.Count) then
  begin
    Key := FKeyStorage.Keys[KeyIndex];
    if not Assigned(Key) then
    begin
      DoError(ERROR_SSH_INTERNAL_ERROR);
      CloseByError(SDisconnectInternalError);
      Exit;
    end;

    // setting algorithm and blob
    if Key.KeyFormat <> kfX509 then
    begin
      if Key.Algorithm = ALGORITHM_RSA then
      begin
        AlgName := 'ssh-rsa'; // pub priv mod
        PublicKeyBlob := WriteString('ssh-rsa') +
          WriteSSH2MPInt(@Key.RSAPublicExponent[1], Length(Key.RSAPublicExponent)) +
          WriteSSH2MPInt(@Key.RSAPublicModulus[1], Length(Key.RSAPublicModulus));
      end
      else if Key.Algorithm = ALGORITHM_DSS then
      begin
        AlgName := 'ssh-dss';
        PublicKeyBlob := WriteString('ssh-dss') +
          WriteSSH2MPInt(@Key.DSSP[1], Length(Key.DSSP)) +
          WriteSSH2MPInt(@Key.DSSQ[1], Length(Key.DSSQ)) +
          WriteSSH2MPInt(@Key.DSSG[1], Length(Key.DSSG)) +
          WriteSSH2MPInt(@Key.DSSY[1], Length(Key.DSSY));
      end
      else
        SSH2EstablishAuthPublicKey(FSSH2LastKeyIndex);
    end
    else
    begin
      Sz := 0;
      Key.Certificate.SaveToBuffer(nil, Sz);
      SetLength(TmpBuf, Sz);
      Key.Certificate.SaveToBuffer(@TmpBuf[1], Sz);
      if Key.Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
      begin
        AlgName := 'x509v3-sign-rsa';
        PublicKeyBlob := WriteString('x509v3-sign-rsa') + TmpBuf;
      end
      else if Key.Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
      begin
        AlgName := 'x509v3-sign-dss';
        PublicKeyBlob := WriteString('x509v3-sign-dss') + TmpBuf;
      end
      else
        SSH2EstablishAuthPublicKey(FSSH2LastKeyIndex);
    end;
    SSH2SendUserauthRequestPublickey(AlgName, PublicKeyBlob);
  end
  else
  begin
    DoAuthenticationFailed(SSH_AUTH_TYPE_PUBLICKEY);
    SSH2ParseServerUserauthFailure(@FSSH2UserauthServerAlgs[1], Length(FSSH2UserauthServerAlgs));
  end;
end;

procedure TElSSHClient.SSH2EstablishAuthHostbased(KeyIndex : integer);
var
  Key : TElSSHKey;
  AlgName, PublicKeyBlob : string;
begin
  FSSH2LastKeyIndex := KeyIndex + 1;
  FSSH2LastUserauthMethod := SSH_AUTH_TYPE_HOSTBASED;
  if (FKeyStorage <> nil) and (KeyIndex < FKeyStorage.Count) then
  begin
    Key := FKeyStorage.Keys[KeyIndex];
    if not Assigned(Key) then
    begin
      DoError(ERROR_SSH_INTERNAL_ERROR);
      CloseByError(SDisconnectInternalError);
      Exit;
    end;
    // setting algorithm and blob
    if Key.Algorithm = ALGORITHM_RSA then
    begin
      AlgName := 'ssh-rsa'; // pub priv mod
      PublicKeyBlob := WriteString('ssh-rsa') +
        WriteSSH2MPInt(@Key.RSAPublicExponent[1], Length(Key.RSAPublicExponent)) +
        WriteSSH2MPInt(@Key.RSAPublicModulus[1], Length(Key.RSAPublicModulus));
    end
    else if Key.Algorithm = ALGORITHM_DSS then
    begin
      AlgName := 'ssh-dss';
      PublicKeyBlob := WriteString('ssh-dss') +
        WriteSSH2MPInt(@Key.DSSP[1], Length(Key.DSSP)) +
        WriteSSH2MPInt(@Key.DSSQ[1], Length(Key.DSSQ)) +
        WriteSSH2MPInt(@Key.DSSG[1], Length(Key.DSSG)) +
        WriteSSH2MPInt(@Key.DSSY[1], Length(Key.DSSY));
    end
    else
      SSH2EstablishAuthHostbased(FSSH2LastKeyIndex);
    SSH2SendUserauthRequestHostbased(AlgName, PublicKeyBlob);
  end
  else
  begin
    DoAuthenticationFailed(SSH_AUTH_TYPE_HOSTBASED);
    SSH2ParseServerUserauthFailure(@FSSH2UserauthServerAlgs[1], Length(FSSH2UserauthServerAlgs));
  end;
end;

procedure TElSSHClient.SSH2EstablishAuthKeyboardInteractive;
begin
  SSH2SendUserauthRequestKeyboardInteractive;
end;

procedure TElSSHClient.SSH2EstablishInteractiveSessions;
begin
  SSH2EstablishSingleInteractiveSession(0);
end;

procedure TElSSHClient.SSH2EstablishSingleInteractiveSession(TunnelIndex : integer);
var
  Tunnel : TElCustomSSHTunnel;
begin
  FLastTunnelIndex := TunnelIndex;
  if not Assigned(FTunnelList) then
  begin
//    DoOpenConnection; // II 170204
    Exit;
  end;
  if FTunnelList.Count <= TunnelIndex then
  begin
//    DoOpenConnection; // II 170204
    Exit;
  end;
  Tunnel := FTunnelList.Tunnels[TunnelIndex];
  if (Tunnel <> nil) and (Tunnel.AutoOpen) then
  begin
    if (Tunnel is TElShellSSHTunnel) or (Tunnel is TElCommandSSHTunnel) or
      (Tunnel is TElSubsystemSSHTunnel) or
      (Tunnel is TElX11ForwardSSHTunnel) then
      SSH2EstablishSession(Tunnel{$ifndef HAS_DEF_PARAMS}, nil{$endif})
    else if Tunnel is TElRemotePortForwardSSHTunnel then
    begin
      SSH2SendGlobalRequestTcpIpForward(TElRemotePortForwardSSHTunnel(Tunnel).ToHost,
        TElRemotePortForwardSSHTunnel(Tunnel).Port);
      SSH2EstablishSingleInteractiveSession(TunnelIndex + 1);
    end
    else if Tunnel is TElLocalPortForwardSSHTunnel then
    begin
      SSH2EstablishSingleInteractiveSession(TunnelIndex + 1);
    end
    else
      SSH2EstablishSingleInteractiveSession(TunnelIndex + 1);
  end
  else
    SSH2EstablishSingleInteractiveSession(TunnelIndex + 1);
end;

procedure TElSSHClient.SSH2SendEnvironment(RemoteChannel : cardinal; Environment : TStringList);
var
  I : integer;
begin
  for I := 0 to Environment.Count - 1 do
    SSH2SendChannelRequestEnv(RemoteChannel, Environment.Names[I], Environment.Values[Environment.Names[I]]);
end;

procedure TElSSHClient.SSH2EstablishSession(Tunnel : TElCustomSSHTunnel; 
    Data: pointer {$ifdef HAS_DEF_PARAMS}= nil{$endif});
var
  Conn : TElSSHClientTunnelConnection;
begin
  Conn := nil;
  SSH2CreateChannel(Tunnel, Conn);
  Conn.Data := Data;
  SSH2SendChannelOpenSession(Conn.LocalChannel);
end;

procedure TElSSHClient.SSH2EstablishShell(Connection : TElSSHClientTunnelConnection);
begin
  Connection.InternalState := SSH2_CONN_INTERNAL_STATE_PTY_REQUEST_SENT;
  Connection.LocalWindowSize := 32768;
  SSH2SendChannelRequestPty(Connection.RemoteChannel);
end;

procedure TElSSHClient.SSH2EstablishCommand(Connection : TElSSHClientTunnelConnection);
begin
  Connection.InternalState := SSH2_CONN_INTERNAL_STATE_PTY_REQUEST_SENT;
  Connection.LocalWindowSize := 32768;
  SSH2SendChannelRequestPty(Connection.RemoteChannel);
end;

procedure TElSSHClient.SSH2EstablishSubsystem(Connection : TElSSHClientTunnelConnection);
begin
// II 030603
//  Connection.InternalState := SSH2_CONN_INTERNAL_STATE_PTY_REQUEST_SENT;
//  SSH2SendChannelRequestPty(Connection.RemoteChannel);
   Connection.InternalState := SSH2_CONN_INTERNAL_STATE_SUBSYSTEM_REQUEST_SENT;
   Connection.LocalWindowSize := 32768;
   SSH2SendChannelRequestSubsystem(Connection.RemoteChannel,
     TElSubsystemSSHTunnel(Connection.Tunnel).Subsystem);

end;

procedure TElSSHClient.SSH2EstablishX11(Connection : TElSSHClientTunnelConnection);
begin
  Connection.InternalState := SSH2_CONN_INTERNAL_STATE_PTY_REQUEST_SENT;
  Connection.LocalWindowSize := 32768;
  SSH2SendChannelRequestPty(Connection.RemoteChannel);
end;

procedure TElSSHClient.SSH2CreateChannel(Tunnel : TElCustomSSHTunnel; var Connection :
  TElSSHClientTunnelConnection);
begin
  if Assigned(Connection) then
    Connection.Free;
    
  Connection := TElSSHClientTunnelConnection.Create;
  Connection.Tunnel := Tunnel;
  Connection.Parent := Self;
  //Connection.FParentIsClient := true;
  Connection.LocalChannelClosed := false;
  Connection.RemoteChannelClosed := false;
  Connection.LocalChannel := FLastChannelIndex;
  Connection.RemoteChannel := $FFFFFFFF;
  Connection.InternalType := SSH2_CONN_INTERNAL_TYPE_NONE;
  Connection.InternalState := SSH2_CONN_INTERNAL_STATE_BEFORE;
  FChannels.Add(Connection);
  Connection.Tunnel.AddConnection(Connection);
  Inc(FLastChannelIndex);
end;

procedure TElSSHClient.SSH2DestroyChannel(Conn : TElSSHClientTunnelConnection);
begin
  if Assigned(Conn) then
  begin
    Conn.DoClose(Conn.CloseType);
    FChannels.Remove(Conn);
    Conn.Tunnel.RemoveConnection(Conn);
    Conn.Free;
  end;
  if FCloseIfNoActiveTunnels and (FChannels.Count = 0) then
    DoCloseConnection;
end;

procedure TElSSHClient.SSH2CloseTunnel(Tunnel : TElSSHClientTunnelConnection;
  FlushCachedData: boolean {$ifdef HAS_DEF_PARAMS}= false{$endif});
begin
  if (not FlushCachedData) or (Length(Tunnel.WindowBuffer) = 0) then
  begin
    if (not Tunnel.LocalChannelClosed) and (Tunnel.RemoteChannel <> $FFFFFFFF) then
    begin
      Tunnel.LocalChannelClosed := true;
      SSH2SendChannelEOF(Tunnel.RemoteChannel);
      SSH2SendChannelClose(Tunnel.RemoteChannel);
    end;
  end
  else
  begin
    Tunnel.CloseWhenDataIsSent := true;
  end;
end;

procedure TElSSHClient.SSH2ConnectTunnel(Tunnel : TElCustomSSHTunnel;
  Data : pointer);
var
  Connection : TElSSHClientTunnelConnection;
  LastClientCh : cardinal;
begin
  if Tunnel is TElLocalPortForwardSSHTunnel then
  begin
    if FChannels.Count = 0 then
      LastClientCh := 0
    else
      LastClientCh := FLastChannelIndex;
    FLastChannelIndex := LastClientCh + 1;
    Connection := TElSSHClientTunnelConnection.Create;
    Connection.LocalChannel := FLastChannelIndex;
    Connection.RemoteChannel := $FFFFFFFF;
    Connection.Parent := Self;
    Connection.Data := Data;
    //Connection.FParentIsClient := true;
    Connection.Tunnel := Tunnel;
    FChannels.Add(Connection);
    Connection.Tunnel.AddConnection(Connection);
    SSH2SendChannelOpenDirectTcpIp(Connection.LocalChannel,
      TElLocalPortForwardSSHTunnel(Tunnel).ToHost,
      TElLocalPortForwardSSHTunnel(Tunnel).ToPort,
      '0.0.0.0', 0);
  end
  else if (Tunnel is TElCommandSSHTunnel) or (Tunnel is TElShellSSHTunnel) or
    (Tunnel is TElSubsystemSSHTunnel) or (Tunnel is TElX11ForwardSSHTunnel) then
  begin
    SSH2EstablishSession(Tunnel, Data);
  end
  else if Tunnel is TElRemotePortForwardSSHTunnel then
  begin
    SSH2SendGlobalRequestTcpIpForward(TElRemotePortForwardSSHTunnel(Tunnel).ToHost,
      TElRemotePortForwardSSHTunnel(Tunnel).Port);
  end
  else
    DoTunnelError(Tunnel, SSH_TUNNEL_ERROR_UNSUPPORTED_ACTION, Data);
end;

procedure TElSSHClient.SSH2KexDHGroupGenerate;
var
  One : PLInt;
begin
  if (FSSH2Params.KexAlgorithm = SSH_KEX_DH_GROUP) then
  begin
    LInit(FDHParams.P, 'FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E' +
      '088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F1' +
      '4374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE38' +
      '6BFB5A899FA5AE9F24117C4B1FE649286651ECE65381FFFFFFFFFFFFFFFF');
  end
  else if (FSSH2Params.KexAlgorithm = SSH_KEX_DH_GROUP_14) then
  begin
    LInit(FDHParams.P, 'FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD1' +
      '29024E088A67CC74020BBEA63B139B22514A08798E3404DD' +
      'EF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245' +
      'E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7ED' +
      'EE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3D' +
      'C2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F' +
      '83655D23DCA3AD961C62F356208552BB9ED529077096966D' +
      '670C354E4ABC9804F1746C08CA18217C32905E462E36CE3B' +
      'E39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9' +
      'DE2BCBF6955817183995497CEA956AE515D2261898FA0510' +
      '15728E5A8AACAA68FFFFFFFFFFFFFFFF');
  end;
  LInit(FDHParams.G, '02');
  LCreate(One);
  LSub(FDHParams.P, One, FDHParams.Q);
  LShr(FDHParams.Q);
  LDestroy(One);
  GenerateDHX;
  LSSHModPower(FDHParams.G, FDHParams.X, FDHParams.P, FDHParams.E);
end;


procedure TElSSHClient.SSH2ChooseAlgorithms(KexLines : TStringList);
var
  S, Lex : string;
  Index, Id : integer;
  ServerConf : set of byte;
begin
  { 1. Choosing kex algorithm }
  S := KexLines.Strings[0];
  ServerConf := [];
  while Length(S) <> 0 do
  begin
    Index := Pos(COMMA, S);
    if Index <> 0 then
    begin
      Lex := Copy(S, 1, Index - 1);
      S := Copy(S, Index + 1, Length(S));
    end
    else
    begin
      Lex := S;
      S := '';
    end;
    
    Id := SSH2GetKexByName(Lex);
    if Id <> -1 then
      ServerConf := ServerConf + [Id];
  end;
  
  Id := -1;
  for Index := SSH_KEX_FIRST to SSH_KEX_LAST do
    if FKexAlgorithms[Index] and (Index in ServerConf) then
    begin
      Id := Index;
      Break;
    end;
  if Id = -1 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  FSSH2Params.KexAlgorithm := Id;
  FKexAlgorithm := Id;
  { 2. Choosing server host key algorithm }
  S := KexLines.Strings[1];
  ServerConf := [];
  while Length(S) <> 0 do
  begin
    Index := Pos(COMMA, S);
    if Index <> 0 then
    begin
      Lex := Copy(S, 1, Index - 1);
      S := Copy(S, Index + 1, Length(S));
    end
    else
    begin
      Lex := S;
      S := '';
    end;
    
    Id := SSH2GetPublicKeyAlgorithmByName(Lex);
    if Id <> -1 then
      ServerConf := ServerConf + [Id];
  end;
  
  Id := -1;
  for Index := SSH_PK_FIRST to SSH_PK_LAST do
    if FPublicKeyAlgorithms[Index] and (Index in ServerConf) then
    begin
      Id := Index;
      Break;
    end;
  if Id = -1 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  FSSH2Params.ServerHostKeyAlgorithm := Id;
  FPublicKeyAlgorithm := Id;
  { 3. Choosing encryption algorithm Client to Server}
  S := KexLines.Strings[2];
  ServerConf := [];
  while Length(S) <> 0 do
  begin
    Index := Pos(COMMA, S);
    if Index <> 0 then
    begin
      Lex := Copy(S, 1, Index - 1);
      S := Copy(S, Index + 1, Length(S));
    end
    else
    begin
      Lex := S;
      S := '';
    end;
    
    Id := SSH2GetEncryptionAlgorithmByName(Lex);
    if Id <> -1 then
      ServerConf := ServerConf + [Id];
  end;
  
  Id := -1;
  for Index := SSH_EA_FIRST to SSH_EA_LAST do
    if FEncryptionAlgorithms[Index] and (Index in ServerConf) then
    begin
      Id := Index;
      Break;
    end;
  if Id = -1 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  FSSH2Params.EncryptionAlgorithmCS := Id;
  FEncryptionAlgorithmCS := Id;
  { 4. Choosing encryption algorithm Server to Client }
  S := KexLines.Strings[3];
  ServerConf := [];
  while Length(S) <> 0 do
  begin
    Index := Pos(COMMA, S);
    if Index <> 0 then
    begin
      Lex := Copy(S, 1, Index - 1);
      S := Copy(S, Index + 1, Length(S));
    end
    else
    begin
      Lex := S;
      S := '';
    end;
    
    Id := SSH2GetEncryptionAlgorithmByName(Lex);
    if Id <> -1 then
      ServerConf := ServerConf + [Id];
  end;
  
  Id := -1;
  for Index := SSH_EA_FIRST to SSH_EA_LAST do
    if FEncryptionAlgorithms[Index] and (Index in ServerConf) then
    begin
      Id := Index;
      Break;
    end;
  if Id = -1 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  FSSH2Params.EncryptionAlgorithmSC := Id;
  FEncryptionAlgorithmSC := Id;
  { 5. Choosing MAC algorithm Client to Server }
  S := KexLines.Strings[4];
  ServerConf := [];
  while Length(S) <> 0 do
  begin
    Index := Pos(COMMA, S);
    if Index <> 0 then
    begin
      Lex := Copy(S, 1, Index - 1);
      S := Copy(S, Index + 1, Length(S));
    end
    else
    begin
      Lex := S;
      S := '';
    end;
    
    Id := SSH2GetMACAlgorithmByName(Lex);
    if Id <> -1 then
      ServerConf := ServerConf + [Id];
  end;
  
  Id := -1;
  for Index := SSH_MA_FIRST to SSH_MA_LAST do
    if FMacAlgorithms[Index] and (Index in ServerConf) then
    begin
      Id := Index;
      Break;
    end;
  if Id = -1 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  FSSH2Params.MacAlgorithmCS := Id;
  FMacAlgorithmCS := Id;
  { 6. Choosing MAC algorithm Server to Client }
  S := KexLines.Strings[5];
  ServerConf := [];
  while Length(S) <> 0 do
  begin
    Index := Pos(COMMA, S);
    if Index <> 0 then
    begin
      Lex := Copy(S, 1, Index - 1);
      S := Copy(S, Index + 1, Length(S));
    end
    else
    begin
      Lex := S;
      S := '';
    end;
    
    Id := SSH2GetMACAlgorithmByName(Lex);
    if Id <> -1 then
      ServerConf := ServerConf + [Id];
  end;
  
  Id := -1;
  for Index := SSH_MA_FIRST to SSH_MA_LAST do
    if FMacAlgorithms[Index] and (Index in ServerConf) then
    begin
      Id := Index;
      Break;
    end;
  if Id = -1 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  FSSH2Params.MacAlgorithmSC := Id;
  FMacAlgorithmSC := Id;
  { 7. Choosing compression Client to Server }
  {$ifndef DONT_USE_ZLIB}
  S := KexLines.Strings[6];
  ServerConf := [];
  while Length(S) <> 0 do
  begin
    Index := Pos(COMMA, S);
    if Index <> 0 then
    begin
      Lex := Copy(S, 1, Index - 1);
      S := Copy(S, Index + 1, Length(S));
    end
    else
    begin
      Lex := S;
      S := '';
    end;
    Id := SSH2GetCompressionAlgorithmByName(Lex);
    if Id <> -1 then
      ServerConf := ServerConf + [Id];
  end;
  Id := -1;
  if FRequestCompression then
  begin
    for Index := SSH_CA_LAST downto SSH_CA_FIRST do
      if FCompressionAlgorithms[Index] and (Index in ServerConf) then
      begin
        Id := Index;
        Break;
      end;
  end
  else
  begin
    for Index := SSH_CA_FIRST to SSH_CA_LAST do
      if FCompressionAlgorithms[Index] and (Index in ServerConf) then
      begin
        Id := Index;
        Break;
      end;
  end;
  if Id = -1 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  FSSH2Params.CompAlgorithmCS := Id;
  FCompressionAlgorithmCS := Id;
  {$else}
  FSSH2Params.CompAlgorithmCS := SSH_CA_NONE;
  FCompressionAlgorithmCS := SSH_CA_NONE;
  {$endif}
  { 8. Choosing compression Server to Client }
  {$ifndef DONT_USE_ZLIB}
  S := KexLines.Strings[7];
  ServerConf := [];
  while Length(S) <> 0 do
  begin
    Index := Pos(COMMA, S);
    if Index <> 0 then
    begin
      Lex := Copy(S, 1, Index - 1);
      S := Copy(S, Index + 1, Length(S));
    end
    else
    begin
      Lex := S;
      S := '';
    end;
    Id := SSH2GetCompressionAlgorithmByName(Lex);
    if Id <> -1 then
      ServerConf := ServerConf + [Id];
  end;
  Id := -1;
  if FRequestCompression then
  begin
    for Index := SSH_CA_LAST downto SSH_CA_FIRST do
      if FCompressionAlgorithms[Index] and (Index in ServerConf) then
      begin
        Id := Index;
        Break;
      end;
  end
  else
  begin
    for Index := SSH_CA_FIRST to SSH_CA_LAST do
      if FCompressionAlgorithms[Index] and (Index in ServerConf) then
      begin
        Id := Index;
        Break;
      end;
  end;
  if Id = -1 then
  begin
    DoError(ERROR_SSH_INVALID_PACKET);
    CloseByError(SDisconnectInvalidPacket);
    Exit;
  end;
  FSSH2Params.CompAlgorithmSC := Id;
  FCompressionAlgorithmSC := Id;
  {$else}
  FSSH2Params.CompAlgorithmSC := SSH_CA_NONE;
  FCompressionAlgorithmSC := SSH_CA_NONE;
  {$endif}
end;

function TElSSHClient.SSH2ProduceKeyData(K : string; H : pointer; HSize : integer;
  Salt : string; Needed : integer) : string;
var
  M160 : TMessageDigest160;
  ShaCtx : TSHA1Context;
  Tmp : string;
begin
  SetLength(Result, 0);
  InitializeSHA1(ShaCtx);
  HashSHA1(ShaCtx, @K[1], Length(K));
  HashSHA1(ShaCtx, H, HSize);
  HashSHA1(ShaCtx, @Salt[1], Length(Salt));
  HashSHA1(ShaCtx, @FSSH2Params.SessionID[0], Length(FSSH2Params.SessionID));
  M160 := FinalizeSHA1(ShaCtx);
  SetLength(Result, 20);
  Move(PByteArray(@M160)[0], Result[1], 20);
  while Length(Result) < Needed do
  begin
    InitializeSHA1(ShaCtx);
    HashSHA1(ShaCtx, @K[1], Length(K));
    HashSHA1(ShaCtx, H, HSize);
    HashSHA1(ShaCtx, @Result[1], Length(Result));
    M160 := FinalizeSHA1(ShaCtx);
    SetLength(Tmp, 20);
    Move(PByteArray(@M160)[0], Tmp[1], 20);
    Result := Result + Tmp;
  end;
  Result := Copy(Result, 1, Needed);
end;

procedure TElSSHClient.SSH2GenerateKeys(K : string; H : pointer; HSize : integer);
var
  IVCS, IVSC, EKCS, EKSC, IKCS, IKSC : string;
begin
  IVCS := SSH2ProduceKeyData(K, H, HSize, 'A', SSH2CipherBlockSizes[FSSH2Params.EncryptionAlgorithmCS]);
  IVSC := SSH2ProduceKeyData(K, H, HSize, 'B', SSH2CipherBlockSizes[FSSH2Params.EncryptionAlgorithmSC]);
  EKCS := SSH2ProduceKeyData(K, H, HSize, 'C', SSH2CipherKeyLengths[FSSH2Params.EncryptionAlgorithmCS]);
  EKSC := SSH2ProduceKeyData(K, H, HSize, 'D', SSH2CipherKeyLengths[FSSH2Params.EncryptionAlgorithmSC]);
  IKCS := SSH2ProduceKeyData(K, H, HSize, 'E', SSH2MacKeySizes[FSSH2Params.MacAlgorithmCS]);
  IKSC := SSH2ProduceKeyData(K, H, HSize, 'F', SSH2MacKeySizes[FSSH2Params.MacAlgorithmSC]);
  {$ifdef SECURE_BLACKBOX_DEBUG}
  DoDebug('Keys', SSH_DEBUG_LEVEL_PACKET_DATA);
  DoDebug(FormatBuf(@IVCS[1], Length(IVCS)), SSH_DEBUG_LEVEL_PACKET_DATA);
  DoDebug(FormatBuf(@IVSC[1], Length(IVSC)), SSH_DEBUG_LEVEL_PACKET_DATA);
  DoDebug(FormatBuf(@EKCS[1], Length(EKCS)), SSH_DEBUG_LEVEL_PACKET_DATA);
  DoDebug(FormatBuf(@EKSC[1], Length(EKSC)), SSH_DEBUG_LEVEL_PACKET_DATA);
  DoDebug(FormatBuf(@IKCS[1], Length(IKCS)), SSH_DEBUG_LEVEL_PACKET_DATA);
  DoDebug(FormatBuf(@IKSC[1], Length(IKSC)), SSH_DEBUG_LEVEL_PACKET_DATA);
  {$endif}
  SetLength(FSSH2Params.MacKeyCS, Length(IKCS));
  Move(IKCS[1], FSSH2Params.MacKeyCS[1], Length(IKCS));
  SetLength(FSSH2Params.MacKeySC, Length(IKSC));
  Move(IKSC[1], FSSH2Params.MacKeySC[1], Length(IKSC));
  SSH2SetEncryptionKeys(EKCS, IVCS);
  SSH2SetDecryptionKeys(EKSC, IVSC);
  if (FSSH2Params.CompAlgorithmCS = SSH_CA_ZLIB) or
    (FSSH2Params.CompAlgorithmSC = SSH_CA_ZLIB) then
  begin
    {$ifndef DONT_USE_ZLIB}
    SBZlib.InitializeCompression(FSSH2CompressionCtx, FCompressionLevel);
    SBZlib.InitializeDecompression(FSSH2DecompressionCtx);
    {$endif}
  end;
  FSSH2Params.EncCSBlockSize := SSH2CipherBlockSizes[FSSH2Params.EncryptionAlgorithmCS];
  FSSH2Params.EncSCBlockSize := SSH2CipherBlockSizes[FSSH2Params.EncryptionAlgorithmSC];
end;

procedure TElSSHClient.SSH2SetEncryptionKeys(Key : string; IV : string);
var
  AKey, AIV : ByteArray;
begin
  FOutputKeyMaterial := TElSymmetricKeyMaterial.Create;
  SetLength(AKey, Length(Key));
  if Length(AKey) > 0 then
    Move(Key[1], AKey[0], Length(Key));
  SetLength(AIV, Length(IV));
  if Length(AIV) > 0 then
    Move(IV[1], AIV[0], Length(IV));
  FOutputKeyMaterial.Key := AKey;
  FOutputKeyMaterial.IV := AIV;

  FOutputCrypto := SSH2GetSymmetricCrypto(FSSH2Params.EncryptionAlgorithmCS);

  if Assigned(FOutputCrypto) then
  begin
    FOutputCrypto.KeyMaterial := FOutputKeyMaterial;
    FOutputCrypto.InitializeEncryption;
  end
end;

procedure TElSSHClient.SSH2SetDecryptionKeys(Key : string; IV : string);
var
  AKey, AIV : ByteArray;
begin
  FInputKeyMaterial := TElSymmetricKeyMaterial.Create;
  SetLength(AKey, Length(Key));
  if Length(AKey) > 0 then
    Move(Key[1], AKey[0], Length(Key));
  SetLength(AIV, Length(IV));
  if Length(AIV) > 0 then
    Move(IV[1], AIV[0], Length(IV));
  FInputKeyMaterial.Key := AKey;
  FInputKeyMaterial.IV := AIV;

  FInputCrypto := SSH2GetSymmetricCrypto(FSSH2Params.EncryptionAlgorithmSC);

  if Assigned(FInputCrypto) then
  begin
    FInputCrypto.KeyMaterial := FInputKeyMaterial;
    FInputCrypto.InitializeDecryption;
  end
end;

function TElSSHClient.SSH2ComputeMAC(P : pointer; Size : longint; IsClient : boolean) : string;
var
  M160 : TMessageDigest160;
  M128 : TMessageDigest128;
  ShaCtx : TMACSHA1Context;
  Md5Ctx : TMACMD5Context;
  SeqStr : Cardinal;
begin
  if IsClient then
  begin
    case FSSH2Params.MacAlgorithmCS of
      SSH_MA_HMAC_SHA1, SSH_MA_HMAC_SHA1_96:
      begin
        InitializeMACSHA1(ShaCtx, FSSH2Params.MacKeyCS);
        SeqStr := ((FSSH2ClientSequenceNumber shr 24) and $FF) or
                  ((FSSH2ClientSequenceNumber shr 8) and $FF00) or
                  ((FSSH2ClientSequenceNumber shl 8) and $FF0000) or
                   (FSSH2ClientSequenceNumber shl 24 and $FF000000);

        HashMACSHA1(ShaCtx, @SeqStr, sizeof(SeqStr));
        HashMACSHA1(ShaCtx, P, Size);
        M160 := FinalizeMACSHA1(ShaCtx);
        SetLength(Result, SSH2MacDigestSizes[FSSH2Params.MacAlgorithmCS]);
        Move(PByteArray(@M160)[0], Result[1], SSH2MacDigestSizes[FSSH2Params.MacAlgorithmCS]);
      end;
      SSH_MA_HMAC_MD5, SSH_MA_HMAC_MD5_96:
      begin
        InitializeMACMD5(Md5Ctx, FSSH2Params.MacKeyCS);
        SeqStr := ((FSSH2ClientSequenceNumber shr 24) and $FF) or
                  ((FSSH2ClientSequenceNumber shr 16) and $FF) or
                  ((FSSH2ClientSequenceNumber shr 8) and $FF) or
                   (FSSH2ClientSequenceNumber and $FF);
        HashMACMD5(Md5Ctx, @SeqStr, sizeof(SeqStr));
        HashMACMD5(Md5Ctx, P, Size);
        M128 := FinalizeMACMD5(Md5Ctx);
        SetLength(Result, SSH2MacDigestSizes[FSSH2Params.MacAlgorithmCS]);
        Move(PByteArray(@M128)[0], Result[1], SSH2MacDigestSizes[FSSH2Params.MacAlgorithmCS]);
      end;
    else
      Result := '';
    end;
  end;
end;

function TElSSHClient.SSH2ValidateMAC(Chunk1 : pointer; Size1 : longint; Chunk2 : pointer;
  Size2 : longint; OrigDigest : pointer) : boolean;
var
  M160 : TMessageDigest160;
  M128 : TMessageDigest128;
  ShaCtx : TMACSHA1Context;
  Md5Ctx : TMACMD5Context;
  SeqStr : string;
begin
  Result := false;
  case FSSH2Params.MacAlgorithmSC of
    SSH_MA_HMAC_SHA1:
    begin
      InitializeMACSHA1(ShaCtx, FSSH2Params.MacKeySC);
      SeqStr := WriteUINT32(FSSH2ServerSequenceNumber);
      HashMACSHA1(ShaCtx, @SeqStr[1], Length(SeqStr));
      HashMACSHA1(ShaCtx, Chunk1, Size1);
      HashMACSHA1(ShaCtx, Chunk2, Size2);
      M160 := FinalizeMACSHA1(ShaCtx);
      if CompareMem(@M160, OrigDigest, SSH2MacDigestSizes[FSSH2Params.MacAlgorithmCS]) then
        Result := true
      else
        Result := false;
    end;
    SSH_MA_HMAC_MD5:
    begin
      InitializeMACMD5(Md5Ctx, FSSH2Params.MacKeySC);
      SeqStr := WriteUINT32(FSSH2ServerSequenceNumber);
      HashMACMD5(Md5Ctx, @SeqStr[1], Length(SeqStr));
      HashMACMD5(Md5Ctx, Chunk1, Size1);
      HashMACMD5(Md5Ctx, Chunk2, Size2);
      M128 := FinalizeMACMD5(Md5Ctx);
      if CompareMem(@M128, OrigDigest, SSH2MacDigestSizes[FSSH2Params.MacAlgorithmCS]) then
        Result := true
      else
        Result := false;
    end;
    SSH_MA_NONE :
      Result := true;
  end;
end;

function TElSSHClient.SSH2ValidateDSS(M : string; P : string; Q : string; G : string;
  Y : string; R : string; S : string) : boolean;
var
  LP, LQ, LG, LY, LR, LS : PLInt;
  LW, LT, LU1, LU2 : PLInt;
  M160 : TMessageDigest160;
begin
  LCreate(LP);
  LCreate(LQ);
  LCreate(LG);
  LCreate(LY);
  LCreate(LR);
  LCreate(LS);
  LCreate(LW);
  LCreate(LT);
  LCreate(LU1);
  LCreate(LU2);
  PointerToLInt(LP, @P[1], Length(P));
  PointerToLInt(LQ, @Q[1], Length(Q));
  PointerToLInt(LG, @G[1], Length(G));
  PointerToLInt(LY, @Y[1], Length(Y));
  PointerToLInt(LR, @R[1], Length(R));
  PointerToLInt(LS, @S[1], Length(S));
  // validating signature
  LGCD(LS, LQ, LT, LW);
  M160 := HashSHA1(@M[1], Length(M));
  PointerToLInt(LT, @M160, 20);
  LMult(LT, LW, LU2);
  LMod(LU2, LQ, LU1);
  LMult(LR, LW, LT);
  LMod(LT, LQ, LU2);
  LSSHModPower(LG, LU1, LP, LT); ///
  LSSHModPower(LY, LU2, LP, LW); ///
  LMult(LT, LW, LU1);
  LMod(LU1, LP, LU2);
  LMod(LU2, LQ, LT);
  Result := LEqual(LT, LR);
  LDestroy(LP);
  LDestroy(LQ);
  LDestroy(LG);
  LDestroy(LY);
  LDestroy(LR);
  LDestroy(LS);
  LDestroy(LW);
  LDestroy(LT);
  LDestroy(LU1);
  LDestroy(LU2);
end;

procedure TElSSHClient.SSH2EncryptPacket(InBuffer : pointer; OutBuffer : pointer; Size : integer);
var
  Sz : integer;
  TmpBuf : ByteArray;
begin
  if FEncryptionAlgorithmCS <> SSH_EA_NONE then
  begin
    Sz := Size;
    SetLength(TmpBuf, Size);
    //this method is called with InBuffer == OutBuffer, SBSymCrypt don't like this
    FOutputCrypto.EncryptUpdate(InBuffer, Size, @TmpBuf[0], Sz);
    Move(TmpBuf[0], OutBuffer^, Sz);
  end;
end;

procedure TElSSHClient.SSH2DecryptPacket(InBuffer : pointer; OutBuffer : pointer;
  Size : integer);
var
  Sz : integer;
  TmpBuf : ByteArray;
begin
  if FEncryptionAlgorithmSC <> SSH_EA_NONE then
  begin
    Sz := Size;
    SetLength(TmpBuf, Size);
    //this method is called with InBuffer == OutBuffer, SBSymCrypt doesn't like this.
    //so we have to use the temporary buffer

    FInputCrypto.DecryptUpdate(InBuffer, Size, @TmpBuf[0], Sz);
    Move(TmpBuf[0], OutBuffer^, Sz);
  end;
end;

procedure TElSSHClient.SSH2CompressPacket(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer);
var
  Sz : cardinal;
  Buf : array of byte;
begin
  Sz := $FF + InSize;
  SetLength(Buf, Sz);
  {$ifndef DONT_USE_ZLIB}
  SBZlib.Compress(FSSH2CompressionCtx, InBuffer, InSize, @Buf[0], Sz);
  {$endif}
  Move(Buf[0], OutBuffer^, Sz);
  OutSize := Sz;
end;

function TElSSHClient.DecomprFunc(Buffer: pointer; Size: integer; Param: pointer): boolean;
var
  OldLen : integer;
begin
  OldLen := Length(FDecompressionBuffer);
  SetLength(FDecompressionBuffer, OldLen + Size);
  Move(Buffer^, FDecompressionBuffer[OldLen], Size);
  Result := true;
end;

procedure TElSSHClient.SSH2DecompressPacket(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer);
var
  Sz : cardinal;
begin
  SetLength(FDecompressionBuffer, 0);
  {$ifndef DONT_USE_ZLIB}
  SBZlib.DecompressEx(FSSH2DecompressionCtx, InBuffer, InSize, 
    DecomprFunc, nil);
  {$endif}
  Sz := Length(FDecompressionBuffer);
  if Sz > 0 then
  begin
    Move(FDecompressionBuffer[0], OutBuffer^, Sz);
  end;
  OutSize := Sz;
end;

procedure TrimValue(var Value : BufferType);
var
  Index: integer;
begin
  if Length(Value) = 0 then
    Exit;
  Index := 1;
  while (Value[Index] = #0) and (Index <= Length(Value)) do
    Inc(Index);
  if Index > 1 then
  begin
    Move(Value[Index], Value[1], Length(Value) - Index + 1);
    SetLength(Value, Length(Value) - Index + 1);
  end;
  if ((Length(Value) > 0) and (PByte(@Value[1])^ >= $80)) or
    (Length(Value) = 0) then
    Value := #0 + Value;
end;

procedure TrimValueEx(var Value : BufferType; StripLeading : boolean; ValLength : integer);
var
  Index: integer;
  trg  : integer;
  ml   : integer;
  NewLen: integer;
begin
  if Length(Value) = 0 then
    Exit;

  ml := Min(Length(Value), ValLength);

  Index := 1;
  while (Value[Index] = #0) and (Index <= ml) do
    Inc(Index);

  if (Index > 1) then
  begin
    trg := 1;
    NewLen := ml - Index + 1;
    if (((NewLen > 0) and (Ord(Value[Index]) > $80)) or
      (Length(Value) = 0)) and (not StripLeading) then
    begin
      inc(trg);
      inc(NewLen);
      if NewLen > ml - Index + trg then
        SetLength(Value, NewLen);
      Value[1] := #0;
    end
    else
    if NewLen > ml - Index + trg then
      SetLength(Value, NewLen);
    Move(Value[Index], Value[trg], ml - Index + 1);
    if NewLen <= ml - Index + trg then
      SetLength(Value, NewLen);
  end
  else
  begin
    if (((Length(Value) > 0) and (Ord(Value[Index]) > $80)) or
      (Length(Value) = 0)) and (not StripLeading) then
    begin
      NewLen := min(Length(Value), ValLength);
      SetLength(Value, ValLength + 1);
      Move(Value[1], Value[2], NewLen);
      Value[1] := #0;
    end
    else
      SetLength(Value, ValLength);
  end;
end;

function TElSSHClient.SSH2CalculateSignature(Hash : string; KeyIndex : integer) : string;
var
  Tmp : string;
  S1, S2, S3, S4 : string;
  Size1, Size2, Size3, Size4 : integer;
  L1, L2, L3, L4, L5, L6, L7, L8 : PLInt;
  Sz : integer;
  AlgId : string;
const
  ASN_SHA1_ID = #$30#$21#$30#$09#$06#$05#$2B#$0E#$03#$02#$1A#$05#$00#$04#$14;
begin
  Result := '';
  if Length(Hash) <> 20 then
    Exit;
  if (FKeyStorage = nil) or (KeyIndex >= FKeyStorage.Count) then
  begin
    DoError(ERROR_SSH_INTERNAL_ERROR);
    CloseByError(SDisconnectInternalError);
    Exit;
  end;
  if (FKeyStorage.Keys[KeyIndex].Algorithm = ALGORITHM_RSA) or
    ((FKeyStorage.Keys[KeyIndex].KeyFormat = kfX509) and
    (FKeyStorage.Keys[KeyIndex].Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION))
  then
  begin
    if FKeyStorage.Keys[KeyIndex].KeyFormat <> kfX509 then
    begin
      Sz := Length(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].RSAPublicModulus);
      if FKeyStorage.Keys[FSSH2LastKeyIndex - 1].RSAPublicModulus[1] = #0 then
        Dec(Sz);
    end
    else
      Sz := FKeyStorage.Keys[KeyIndex].Certificate.GetPublicKeySize shr 3;
    SetLength(Tmp, Sz);
    Move(Hash[1], Tmp[Length(Tmp) - 19], Length(Hash));
    Move(ASN_SHA1_ID[1], Tmp[Length(Tmp) - 19 - Length(ASN_SHA1_ID)], Length(ASN_SHA1_ID));
    Tmp[1] := #0; Tmp[2] := #1;
    FillChar(Tmp[3], Length(Tmp) - 20 - Length(ASN_SHA1_ID) - 3, $FF);
    Tmp[Length(Tmp) - 20 - Length(ASN_SHA1_ID)] := #0;
    LCreate(L1);
    LCreate(L2);
    LCreate(L3);
    LCreate(L4);
    PointerToLInt(L1, @Tmp[1], Length(Tmp));
    if FKeyStorage.Keys[KeyIndex].KeyFormat <> kfX509 then
    begin
      PointerToLInt(L2, @(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].RSAPrivateExponent[1]),
        Length(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].RSAPrivateExponent));
      PointerToLInt(L3, @(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].RSAPublicModulus[1]),
        Length(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].RSAPublicModulus));
    end
    else
    begin
      Size1 := 0;
      FKeyStorage.Keys[KeyIndex].Certificate.GetRSAParams(nil,
        Size1, nil, Size2);
      SetLength(S1, Size1);
      SetLength(S2, Size2);
      FKeyStorage.Keys[KeyIndex].Certificate.GetRSAParams(@S1[1],
        Size1, @S2[1], Size2);
      Size2 := 0;
      FKeyStorage.Keys[KeyIndex].Certificate.SaveKeyValueToBuffer(nil,
        Size2);
      SetLength(S2, Size2);
      FKeyStorage.Keys[KeyIndex].Certificate.SaveKeyValueToBuffer(@S2[1],
        Size2);
      PointerToLInt(L2, @S2[1], Length(S2));
      PointerToLInt(L3, @S1[1], Length(S1));
    end;
//    LSSHModPower(L1, L2, L3, L4); ///
    {$ifdef SECURE_BLACKBOX_DEBUG}
    Dumper.WriteString('SSH2CalculateSignature');
    Dumper.BeginSubsection;
    Dumper.WriteString('L1=' + LToStr(L1));
    Dumper.WriteString('L2=' + LToStr(L2));
    Dumper.WriteString('L3=' + LToStr(L3));
    {$endif}
    LMModPower(L1, L2, L3, L4);
    {$ifdef SECURE_BLACKBOX_DEBUG}
    Dumper.WriteString('L4=' + LToStr(L4));
    Dumper.EndSubsection;
    {$endif}
    Sz := L4^.Length * 4;
    SetLength(Result, Sz);
    LIntToPointer(L4, @Result[1], Sz);
    // SetLength(Result, Sz);
    // 'not' operator was added by II on Aug 01 2005 to make ElSSHClient
    // compatible with OpenSSH that expects signature NOT to be prepended by zero byte.
    TrimValueEx(Result, not FWrkArdFSecure1, Sz);
    
    LDestroy(L1);
    LDestroy(L2);
    LDestroy(L3);
    LDestroy(L4);
    if FKeyStorage.Keys[KeyIndex].KeyFormat = kfX509 then
      AlgId := 'x509v3-sign-rsa'
    else
      AlgId := 'ssh-rsa';
    Result := WriteString(AlgId) + WriteString(Result);
  end
  else if (FKeyStorage.Keys[FSSH2LastKeyIndex - 1].Algorithm = ALGORITHM_DSS) or
    ((FKeyStorage.Keys[KeyIndex].KeyFormat = kfX509) and
    (FKeyStorage.Keys[KeyIndex].Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA))
  then
  begin
    SetLength(Tmp, 20);
    Move(Hash[1], Tmp[1], 20);
    LCreate(L1); // L1 = Tmp, L2 = P, L3 = Q, L4 = K, L5 = K-1, L6 = G, L7 = X, L8 = Temporar
    LCreate(L2);
    LCreate(L3);
    LCreate(L4);
    LCreate(L5);
    LCreate(L6);
    LCreate(L7);
    LCreate(L8);
    PointerToLInt(L1, @Tmp[1], Length(Tmp));
    if FKeyStorage.Keys[KeyIndex].KeyFormat <> kfX509 then
    begin
      PointerToLInt(L3, @(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].DSSQ[1]),
        Length(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].DSSQ));
      //LRandom(FRC4RandomCtx, L4, 18);
      SBRndGenerateLInt(L4, 18);
      LGCD(L4, L3, L2, L5);
      PointerToLInt(L2, @(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].DSSP[1]),
        Length(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].DSSP));
      PointerToLInt(L6, @(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].DSSG[1]),
        Length(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].DSSG));
      PointerToLInt(L7, @(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].DSSX[1]),
        Length(FKeyStorage.Keys[FSSH2LastKeyIndex - 1].DSSX));
    end
    else
    begin
      Size1 := 0;
      FKeyStorage.Keys[KeyIndex].Certificate.GetDSSParams(
        nil, Size1,
        nil, Size2,
        nil, Size3,
        nil, Size4);
      SetLength(S1, Size1);
      SetLength(S2, Size2);
      SetLength(S3, Size3);
      SetLength(S4, Size4);
      FKeyStorage.Keys[KeyIndex].Certificate.GetDSSParams(
        @S1[1], Size1,
        @S2[1], Size2,
        @S3[1], Size3,
        @S4[1], Size4);
      Size4 := 0;
      FKeyStorage.Keys[KeyIndex].Certificate.SaveKeyValueToBuffer(nil,
        Size4);
      SetLength(S4, Size4);
      FKeyStorage.Keys[KeyIndex].Certificate.SaveKeyValueToBuffer(@S4[1],
        Size4);

      PointerToLInt(L3, @S2[1], Length(S2));
      //LRandom(FRC4RandomCtx, L4, 18);
      SBRndGenerateLInt(L4, 18);
      LGCD(L4, L3, L2, L5);
      PointerToLInt(L2, @S1[1], Length(S1));
      PointerToLInt(L6, @S3[1], Length(S3));
      PointerToLInt(L7, @S4[1], Length(S4));
    end;
    LSSHModPower(L6, L4, L2, L8); ///
    LMod(L8, L3, L2);                   // now L2 = R
    LMult(L7, L2, L8);
    LAdd(L8, L1, L6);                   // now L6 = SHA1(m) + XR
    LMult(L6, L5, L8);
    LMod(L8, L3, L6);                   // now L6 = S
    // Creating DSS signature blob
    SetLength(Result, 40);
    Sz := 20;

    if FKeyStorage.Keys[KeyIndex].KeyFormat = kfX509 then
      AlgId := 'x509v3-sign-dss'
    else
      AlgId := 'ssh-dss';

    LIntToPointer(L2, @Result[1], Sz);
    LIntToPointer(L6, @Result[21], Sz);
    Result := WriteString(AlgId) + WriteString(Result);
    LDestroy(L1);
    LDestroy(L2);
    LDestroy(L3);
    LDestroy(L4);
    LDestroy(L5);
    LDestroy(L6);
    LDestroy(L7);
    LDestroy(L8);
  end;
end;

procedure TElSSHClient.GenerateDHX;
//var
//  Ctx : TRC4RandomContext;
begin
//  LRC4Init(Ctx);
//  LRandom(Ctx, FDHParams.X, FDHParams.P.Length);
  SBRndGenerateLInt(FDHParams.X, FDHParams.P.Length);
end;

function TElSSHClient.SSH2GetKexByName(KexStr : string) : integer;
var
  Index : integer;
begin
  Result := -1;
  for Index := SSH_KEX_FIRST to SSH_KEX_LAST do
    if CompareStr(SSH2KexStrings[Index], KexStr) = 0 then
    begin
      Result := Index;
      Break;
    end;
end;

function TElSSHClient.SSH2GetEncryptionAlgorithmByName(EncStr : string) : integer;
var
  Index : integer;
begin
  Result := -1;
  for Index := SSH_EA_FIRST to SSH_EA_LAST do
    if CompareStr(SSH2CipherStrings[Index], EncStr) = 0 then
    begin
      Result := Index;
      Break;
    end;
end;

function TElSSHClient.SSH2GetPublicKeyAlgorithmByName(PubStr : string) : integer;
var
  Index : integer;
begin
  Result := -1;
  for Index := SSH_PK_FIRST to SSH_PK_LAST do
    if CompareStr(SSH2PublicStrings[Index], PubStr) = 0 then
    begin
      Result := Index;
      Break;
    end;
end;

function TElSSHClient.SSH2GetMacAlgorithmByName(MacStr : string) : integer;
var
  Index : integer;
begin
  Result := -1;
  for Index := SSH_MA_FIRST to SSH_MA_LAST do
    if CompareStr(SSH2MacStrings[Index], MacStr) = 0 then
    begin
      Result := Index;
      Break;
    end;
end;

function TElSSHClient.SSH2GetCompressionAlgorithmByName(CompStr : string) : integer;
var
  Index : integer;
begin
  Result := -1;
  for Index := SSH_CA_FIRST to SSH_CA_LAST do
    if CompareStr(SSH2CompStrings[Index], CompStr) = 0 then
    begin
      Result := Index;
      Break;
    end;
end;

function TElSSHClient.SSH2EncodePtyModes(TerminalInfo : TElTerminalInfo) : string;
var
  I : integer;
  C : byte;
begin
  Result := '';
  if TerminalInfo = nil then
  begin
    for I := Low(PtyCodesFirsts) to High(PtyCodesFirsts) do
      Result := Result + Chr(PtyCodesFirsts[I]) + WriteUINT32(PtyCodesSeconds[I]);
    Result := Result + Chr(128) + WriteUINT32(38400);
    Result := Result + Chr(129) + WriteUINT32(38400);
    Result := Result + #0;
  end
  else
  begin
    for I := OPCODE_FIRST to OPCODE_LAST do
    begin
      try
        C := TerminalInfo.OpCodes[I];
        Result := Result + Chr(I) + WriteUINT32(C);
      except
      end;
    end;
    Result := Result + Chr(128) + WriteUINT32(38400);
    Result := Result + Chr(129) + WriteUINT32(38400);
    Result := Result + #0;
  end;
end;

procedure TElSSHClient.SSH2KexDHPower(A, B, C : PLInt; var D : PLInt);
const
  E : cardinal = 4369;
var
  Q, P : PLInt;
begin
  LCreate(Q);
  LCreate(P);
  LMultSh(C, E, Q);
  LSSHModPower(A, B, Q, P);///
  LMultSh(P, E, D);
  LMod(D, Q, P);
  LDivSh(P, E, D, Q);
  LTrim(D);
  LDestroy(Q);
  LDestroy(P);
end;

////////////////////////////////////////////////////////////////////////////////
// SSH format typecasters

function TElSSHClient.WriteKexAlgorithms : string;
var
  I : integer;
begin
{  Result := WriteString('diffie-hellman-group-exchange-sha1,diffie-hellman-group1-sha1');
//  Result := WriteString('diffie-hellman-group1-sha1,diffie-hellman-group-exchange-sha1');
}
  Result := '';
  for I := SSH_KEX_FIRST to SSH_KEX_LAST do
    if FKexAlgorithms[I] then
      Result := Result + SSH2KexStrings[I] + ',';
  if Length(Result) <> 0 then
    SetLength(Result, Length(Result) - 1);
  Result := WriteString(Result);
end;

function TElSSHClient.WriteServerHostKeyAlgorithms : string;
var
  I : integer;
begin
//  Result := WriteString('ssh-rsa,ssh-dss');
  Result := '';
  for I := SSH_PK_FIRST to SSH_PK_LAST do
    if FPublicKeyAlgorithms[I] then
      Result := Result + SSH2PublicStrings[I] + ',';
  if Length(Result) <> 0 then
    SetLength(Result, Length(Result) - 1);
  Result := WriteString(Result);
end;

function TElSSHClient.WriteEncAlgorithmsCS : string;
var
  I : integer;
begin
//  Result := WriteString('aes128-cbc,3des-cbc,blowfish-cbc,cast128-cbc,arcfour,aes192-cbc,aes256-cbc,rijndael-cbc@lysator.liu.se');
  Result := '';
  for I := SSH_EA_FIRST to SSH_EA_LAST do
    if FEncryptionAlgorithms[I] then
      Result := Result + SSH2CipherStrings[I] + ',';
  if Length(Result) <> 0 then
    SetLength(Result, Length(Result) - 1);
  Result := WriteString(Result);
end;

function TElSSHClient.WriteEncAlgorithmsSC : string;
var
  I : integer;
begin
//  Result := WriteString('aes128-cbc,3des-cbc,blowfish-cbc,cast128-cbc,arcfour,aes192-cbc,aes256-cbc,rijndael-cbc@lysator.liu.se');
  Result := '';
  for I := SSH_EA_FIRST to SSH_EA_LAST do
    if FEncryptionAlgorithms[I] then
      Result := Result + SSH2CipherStrings[I] + ',';
  if Length(Result) <> 0 then
    SetLength(Result, Length(Result) - 1);
  Result := WriteString(Result);
end;

function TElSSHClient.WriteMACAlgorithmsCS : string;
var
  I : integer;
begin
//  Result := WriteString('hmac-md5,hmac-sha1,hmac-ripemd160,hmac-ripemd160@openssh.com,hmac-sha1-96,hmac-md5-96');
  Result := '';
  for I := SSH_MA_FIRST to SSH_MA_LAST do
    if FMacAlgorithms[I] then
      Result := Result + SSH2MacStrings[I] + ',';
  if Length(Result) <> 0 then
    SetLength(Result, Length(Result) - 1);
  Result := WriteString(Result);
end;

function TElSSHClient.WriteMACAlgorithmsSC : string;
var
  I : integer;
begin
//  Result := WriteString('hmac-md5,hmac-sha1,hmac-ripemd160,hmac-ripemd160@openssh.com,hmac-sha1-96,hmac-md5-96');
  Result := '';
  for I := SSH_MA_FIRST to SSH_MA_LAST do
    if FMacAlgorithms[I] then
      Result := Result + SSH2MacStrings[I] + ',';
  if Length(Result) <> 0 then
    SetLength(Result, Length(Result) - 1);
  Result := WriteString(Result);
end;

function TElSSHClient.WriteCompAlgorithmsCS : string;
{$ifndef DONT_USE_ZLIB}
var
  I : integer;
  {$endif}
begin
  {$ifndef DONT_USE_ZLIB}
  Result := '';
  if FRequestCompression then
  begin
    for I := SSH_CA_LAST downto SSH_CA_FIRST do
      if FCompressionAlgorithms[I] then
        Result := Result + SSH2CompStrings[I] + ',';
  end
  else
  begin
    for I := SSH_CA_FIRST to SSH_CA_LAST do
      if FCompressionAlgorithms[I] then
        Result := Result + SSH2CompStrings[I] + ',';
  end;
  if Length(Result) <> 0 then
    SetLength(Result, Length(Result) - 1);
  Result := WriteString(Result);
  {$else}
  Result := WriteString('none');
  {$endif}
end;

function TElSSHClient.WriteCompAlgorithmsSC : string;
{$ifndef DONT_USE_ZLIB}
var
  I : integer;
  {$endif}
begin
  {$ifndef DONT_USE_ZLIB}
  Result := '';
  if FRequestCompression then
  begin
    for I := SSH_CA_LAST downto SSH_CA_FIRST do
      if FCompressionAlgorithms[I] then
        Result := Result + SSH2CompStrings[I] + ',';
  end
  else
  begin
    for I := SSH_CA_FIRST to SSH_CA_LAST do
      if FCompressionAlgorithms[I] then
        Result := Result + SSH2CompStrings[I] + ',';
  end;
  if Length(Result) <> 0 then
    SetLength(Result, Length(Result) - 1);
  Result := WriteString(Result);
  {$else}
  Result := WriteString('none');
  {$endif}
end;

function TElSSHClient.WriteLanguagesCS : string;
begin
  Result := WriteString('');
end;

function TElSSHClient.WriteLanguagesSC : string;
begin
  Result := WriteString('');
end;

{$ifdef SECURE_BLACKBOX_DEBUG}
function FormatBuf(P : pointer; Size : longint) : string;
var
  I : integer;
begin
  Result := '';
  for I := 0 to Size - 1 do
    Result := Result + IntToHex(PByteArray(P)[I], 2) + ' ';
end;
{$endif}

procedure TElSSHClient.SendTerminalResize(RemoteChannel: cardinal; Cols, Rows,
  Width, Height: integer);
begin
  if FVersion = sbSSH2 then
    SSH2SendChannelRequestWindowChange(RemoteChannel, Cols, Rows, Width, Height);
end;

function TElSSHClient.GetThreadSafe : boolean;
begin
  Result := GetSharedResource.Enabled;
end;

procedure TElSSHClient.SetThreadSafe(Value: boolean);
begin
  GetSharedResource.Enabled := Value;
end;

function TElSSHClient.GetSharedResource: TElSharedResource;
begin
  Result := FSharedResource;
end;

procedure TElSSHClient.PerformClose(Forced: Boolean; CloseReason: string);
var
  Conn : TElSSHClientTunnelConnection;
  I : integer;
begin
  if not FClosing then
  begin
    FClosing := true;
    try
      CheckLicenseKey();
      {$ifdef SECURE_BLACKBOX_DEBUG}
      DoDebug('Closing SSH client', SSH_DEBUG_LEVEL_GLOBAL_EVENTS);
      {$endif}
      for I := 0 to FChannels.Count - 1 do
      begin
        Conn := FChannels.Items[I];
        if (FVersion = sbSSH2) or
          ((FVersion = sbSSH1) and (not (Conn.Tunnel is TElShellSSHTunnel)) and
          (not (Conn.Tunnel is TElCommandSSHTunnel))) then
        Conn.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
      end;

      if not Forced then
      begin
        if FVersion = sbSSH1 then
        begin
          SSH1SendEOF;
          SSH1SendDisconnect(CloseReason);
        end
        else
        begin
          SSH2SendDisconnect(SSH_DISCONNECT_BY_APPLICATION, CloseReason);
        end;
      end;

      // the following command calls DoCloseConnection, which frees
      // the allocated tunnel connection objects.
      inherited Close(Forced);

      FClientState := csBefore;
    finally
      FClosing := false;
    end;
  end;
end;



procedure TElSSHClientTunnelConnection.SendExtendedData(Buffer : pointer; Size : longint);
begin
  // default does nothing
end;


procedure TElSSHClientTunnelConnection.SendData(Buffer : pointer; Size : longint);
var
  Sz, OldSize: integer;
begin




  if (Self.LocalChannelClosed or 
      Self.RemoteChannelClosed) then
  begin
    // possibly, raising exception
    Exit;
  end;
  //if FParentIsClient then
  begin
    TElSSHClient(Parent).GetSharedResource.WaitToWrite;
    try
      if TElSSHClient(Parent).Version = sbSSH2 then
      begin
        // 1. Sending data left in FWindowBuffer
        if Length(WindowBuffer) > 0 then
        begin
          Sz := Min(Length(WindowBuffer), WindowSize);
          // preventing local buffer from overflow
          Sz := Min(Sz, OUT_BUFFER_SIZE - 128);
          if Sz > 0 then
          begin
            TElSSHClient(Parent).SendTunnelData(RemoteChannel, @WindowBuffer[0], Sz);
            Move(WindowBuffer[Sz], WindowBuffer[0], Length(WindowBuffer) - Sz);
            SetWindowBufferLength(Length(WindowBuffer) - Sz);
            WindowSize := WindowSize - Sz;
          end;
        end;
        // 2. Sending buffer data
        Sz := Min(Size, WindowSize);
        // preventing local buffer from overflow
        Sz := Min(Sz, OUT_BUFFER_SIZE - 128);
        if Sz > 0 then
        begin
          TElSSHClient(Parent).SendTunnelData(RemoteChannel, Buffer,  Sz);
          WindowSize := WindowSize - Sz;
        end;
        // 3. Appending trail to the FWindowBuffer
        if Size <> sz then
        begin
          OldSize := Length(WindowBuffer);
          SetWindowBufferLength(OldSize + Size - Sz);
          Move(PByteArray(Buffer)[Sz], WindowBuffer[OldSize], Size - Sz);
        end;
        // 4. Checking if close request was received
        if (CloseWhenDataIsSent) and (Length(WindowBuffer) = 0) then
        begin
          DoClose(ctReturn);
          TElSSHClient(Parent).CloseTunnel(Self, false);
        end;
      end
      else
        TElSSHClient(Parent).SendTunnelData(RemoteChannel, Buffer, Size);
    finally
      TElSSHClient(Parent).GetSharedResource.Done;
    end;
  end;
end;

function TElSSHClientTunnelConnection.CanSend : boolean;
begin
  Result := (not (Self.LocalChannelClosed or Self.RemoteChannelClosed)) and
    (Length(WindowBuffer) < WindowSize); 
end;

procedure TElSSHClientTunnelConnection.DoData(Buffer : pointer; Size : longint);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.DoExtendedData(Buffer : pointer; Size : longint);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.DoClose(CloseType : TSSHCloseType);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.DoWindowChanged(Cols, Rows, Width, Height: integer);
begin
  inherited;
end;

function TElSSHClientTunnelConnection.GetCloseType: TSSHCloseType;
begin
  Result := inherited GetCloseType;
end;

function TElSSHClientTunnelConnection.GetCloseWhenDataIsSent: Boolean;
begin
  Result := inherited GetCloseWhenDataIsSent;
end;

function TElSSHClientTunnelConnection.GetInternalState: Integer;
begin
  Result := inherited GetInternalState;
end;

function TElSSHClientTunnelConnection.GetInternalType: Integer;
begin
  Result := inherited GetInternalType;
end;

function TElSSHClientTunnelConnection.GetLocalChannel: Cardinal;
begin
  Result := inherited GetLocalChannel;
end;

function TElSSHClientTunnelConnection.GetLocalChannelClosed: Boolean;
begin
  Result := inherited GetLocalChannelClosed;
end;

function TElSSHClientTunnelConnection.GetLocalWindowSize: Integer;
begin
  Result := inherited GetLocalWindowSize;
end;

function TElSSHClientTunnelConnection.GetParent: TElSSHClass;
begin
  Result := inherited GetParent;
end;

function TElSSHClientTunnelConnection.GetRemoteChannel: Cardinal;
begin
  Result := inherited GetRemoteChannel;
end;

function TElSSHClientTunnelConnection.GetRemoteChannelClosed: Boolean;
begin
  Result := inherited GetRemoteChannelClosed;
end;

function TElSSHClientTunnelConnection.GetWindowBuffer: ByteArray;
begin
  Result := inherited GetWindowBuffer;
end;

function TElSSHClientTunnelConnection.GetWindowSize: Integer;
begin
  Result := inherited GetWindowSize;
end;

procedure TElSSHClientTunnelConnection.SetCloseType(const Value: TSSHCloseType);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetCloseWhenDataIsSent(Value: Boolean);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetExitMessage(const Value: string);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetExitSignal(const Value: string);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetExitStatus(const Value: Integer);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetInternalState(Value: Integer);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetInternalType(Value: Integer);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetLocalChannel(const Value: Cardinal);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetLocalChannelClosed(const Value:
    Boolean);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetLocalWindowSize(const Value: Integer);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetParent(const Value: TElSSHClass);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetRemoteChannel(const Value: Cardinal);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetRemoteChannelClosed(const Value:
    Boolean);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetWindowBuffer(const Value: ByteArray);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetWindowBufferLength(const NewLength:
    Integer);
begin
  inherited;
end;

procedure TElSSHClientTunnelConnection.SetWindowSize(const Value: Integer);
begin
  inherited;
end;

(*
procedure TElSSHClientTunnel.DoClose(Connection : TElSSHTunnelConnection);
begin
  inherited;
end;

procedure TElSSHClientTunnel.DoError(Error : integer; Data : {$ifndef DELPHI_NET}pointer{$else}System.Object{$endif});
begin
  inherited;
end;

procedure TElSSHClientTunnel.DoOpen(Connection : TElSSHTunnelConnection);
begin
  inherited;
end;

*)

end.
