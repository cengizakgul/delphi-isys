(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSSHForwarding;

interface

uses
  Classes,
  SysUtils,
  {$ifndef LINUX}
  Windows,
  {$endif}
  SyncObjs,
  SBSSHCommon,
  SBSSHClient,
  SBSSHKeyStorage,
  SBSharedResource,
  SBUtils,
  SBSocket,
  SBSSHConstants;

const
  ERROR_SSH_TCP_CONNECTION_FAILED               = integer($6001);
  ERROR_SSH_TCP_BIND_FAILED                     = integer($6002);

type
  TElSSHForwardedConnection = class;

  TSBSSHConnectionEvent = procedure(Sender: TObject; Conn: TElSSHForwardedConnection) of object;
  TSBSSHConnectionErrorEvent = procedure(Sender: TObject; Conn: TElSSHForwardedConnection;
    ErrorCode: integer) of object;

  TSBSSHLPFAcceptEvent = procedure(Sender: TObject; Socket : TElSocket) of object;

  TElSSHForwardingSession = class;

  // base class for forwarding components
  TElSSHCustomForwarding = class(TSBControlBase)
  private
    // Socks support
    FSocketTimeout: Integer;
    FSocksAuthentication: TElSocksAuthentication;
    FSocksPassword: string;
    FSocksPort: Integer;
    FSocksResolveAddress: Boolean;
    FSocksServer: string;
    FSocksUserCode: string;
    FSocksVersion: TElSocksVersion;
    FUseSocks: Boolean;
    //  Web tunneling support
    FUseWebTunneling: boolean;
    FWebTunnelAddress: string;
    FWebTunnelAuthentication: TElWebTunnelAuthentication;
    FWebTunnelPassword: string;
    FWebTunnelPort: Integer;
    FWebTunnelUserId: string;
    // SSH properties
    FAuthenticationTypes : integer;
    FVersions : TSSHVersions;
    FUsername : string;
    FPassword : string;
    FClientHostname : string;
    FClientUsername : string;
    FSoftwareName : string;
    FForceCompression : boolean;
    FCompressionLevel : integer;
    FAddress : string;
    FPort : integer;
    FEncryptionAlgorithms : array[SSH_EA_FIRST..SSH_EA_LAST] of boolean;
    FCompressionAlgorithms : array[SSH_CA_FIRST..SSH_CA_LAST] of boolean;
    FMacAlgorithms : array[SSH_MA_FIRST..SSH_MA_LAST] of boolean;
    FPublicKeyAlgorithms : array[SSH_PK_FIRST..SSH_PK_LAST] of boolean;
    FKexAlgorithms : array[SSH_KEX_FIRST..SSH_KEX_LAST] of boolean;
    FKeyStorage : TElSSHCustomKeyStorage;
  protected

    FActive : boolean;
    FOnCloseRaised : boolean;
    FErrorCode : integer;
    FOnAuthenticationFailed: TSSHAuthenticationFailedEvent;
    FOnAuthenticationKeyboard: TSSHAuthenticationKeyboardEvent;
    FOnAuthenticationSuccess: TNotifyEvent;
    FOnBanner: TSSHBannerEvent;
    FOnError: TSSHErrorEvent;
    FOnKeyValidate: TSSHKeyValidateEvent;
    FOnDebugData : TSSHDataEvent;
    FOnConnectionOpen : TSBSSHConnectionEvent;
    FOnConnectionClose : TSBSSHConnectionEvent;
    FOnConnectionChange : TSBSSHConnectionEvent;
    FOnConnectionError : TSBSSHConnectionErrorEvent;
    FOnConnectionsRefresh : TNotifyEvent;
    FOnOpen : TNotifyEvent;
    FOnClose : TNotifyEvent;
    FServerSoftwareName : string;
    FVersion : TSSHVersion;
    FServerCloseReason : string;
    FEncryptionAlgorithmSC : TSSHEncryptionAlgorithm;
    FEncryptionAlgorithmCS : TSSHEncryptionAlgorithm;
    FCompressionAlgorithmSC : TSSHCompressionAlgorithm;
    FCompressionAlgorithmCS : TSSHCompressionAlgorithm;
    FMacAlgorithmSC : TSSHMacAlgorithm;
    FMacAlgorithmCS : TSSHMacAlgorithm;
    FKexAlgorithm : TSSHKexAlgorithm;
    FPublicKeyAlgorithm : TSSHPublicKeyAlgorithm;
    FErrorString : string;
    FForwardedPort : integer;
    FForwardedHost : string;
    FDestHost : string;
    FDestPort : integer;
    FSession : TElSSHForwardingSession;
    FSynchronizeGUI : boolean;
    procedure DoAuthenticationFailed(Sender : TObject; AuthenticationType :
        integer);
    procedure DoAuthenticationKeyboard(Sender : TObject; Prompts : TStringList;
        Echo : TBits; Responses : TStringList);
    procedure DoAuthenticationSuccess(Sender : TObject);
    procedure DoError(Sender : TObject; ErrorCode : integer);
    procedure DoKeyValidate(Sender : TObject; ServerKey : TElSSHKey; var Validate :
      boolean);
    procedure DoBanner(Sender: TObject; const Text : string; const Language : string);
    function GetEncryptionAlgorithms(Index : TSSHEncryptionAlgorithm) : boolean;
    procedure SetEncryptionAlgorithms(Index : TSSHEncryptionAlgorithm; Value : boolean);
    function GetCompressionAlgorithms(Index : TSSHCompressionAlgorithm) : boolean;
    procedure SetCompressionAlgorithms(Index : TSSHCompressionAlgorithm; Value : boolean);
    function GetMACAlgorithms(Index : TSSHMacAlgorithm) : boolean;
    procedure SetMACAlgorithms(Index : TSSHMacAlgorithm; Value : boolean);
    function GetKexAlgorithms(Index : TSSHKexAlgorithm) : boolean;
    procedure SetKexAlgorithms(Index : TSSHKexAlgorithm; Value : boolean);
    function GetPublicKeyAlgorithms(Index : TSSHPublicKeyAlgorithm) : boolean;
    procedure SetPublicKeyAlgorithms(Index : TSSHPublicKeyAlgorithm; Value : boolean);
    function GetKeyStorage: TElSSHCustomKeyStorage;
    procedure SetKeyStorage(Value: TElSSHCustomKeyStorage);
    procedure SetSocketTimeout(Value: Integer);
    procedure SetUseSocks(const Value: Boolean);
    procedure SetUseWebTunneling(const Value: boolean);
    procedure Notification(AComponent: TComponent; AOperation : TOperation); override;
    function CreateSession : TElSSHForwardingSession; virtual;
    procedure SetupSSHProperties;
    function GetConnection(Index: integer) : TElSSHForwardedConnection;
    function GetConnectionCount : integer;
    procedure OnSessionTerminate(Sender: TObject);
    function GetActive : boolean;

  protected
    // left here for future
    property OnConnectionsRefresh : TNotifyEvent read FOnConnectionsRefresh
      write FOnConnectionsRefresh;
    property Connections[Index: integer] : TElSSHForwardedConnection read GetConnection;
    property ConnectionCount : integer read GetConnectionCount;
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    // methods
    procedure Open;
    procedure Close;
    // properties
    property Active : boolean read GetActive;
    property CompressionAlgorithms[Index : TSSHCompressionAlgorithm] : boolean
      read GetCompressionAlgorithms write SetCompressionAlgorithms;
    property CompressionAlgorithmServerToClient : TSSHCompressionAlgorithm
      read FCompressionAlgorithmSC;
    property CompressionAlgorithmClientToServer : TSSHCompressionAlgorithm
      read FCompressionAlgorithmCS;
    property EncryptionAlgorithms[Index : TSSHEncryptionAlgorithm] : boolean
      read GetEncryptionAlgorithms write SetEncryptionAlgorithms;
    property EncryptionAlgorithmServerToClient : TSSHEncryptionAlgorithm
      read FEncryptionAlgorithmSC;
    property EncryptionAlgorithmClientToServer : TSSHEncryptionAlgorithm
      read FEncryptionAlgorithmCS;
    property KexAlgorithms[Index : TSSHKexAlgorithm] : boolean
      read GetKexAlgorithms write SetKexAlgorithms;
    property KexAlgorithm : TSSHKexAlgorithm read FKexAlgorithm;
    property MacAlgorithms[Index : TSSHMacAlgorithm] : boolean
      read GetMACAlgorithms write SetMACAlgorithms;
    property MacAlgorithmServerToClient : TSSHMacAlgorithm read FMacAlgorithmSC;
    property MacAlgorithmClientToServer : TSSHMacAlgorithm read FMacAlgorithmCS;
    property PublicKeyAlgorithms[Index : TSSHPublicKeyAlgorithm] : boolean
      read GetPublicKeyAlgorithms write SetPublicKeyAlgorithms;
    property PublicKeyAlgorithm : TSSHPublicKeyAlgorithm read FPublicKeyAlgorithm;
    property ServerCloseReason : string read FServerCloseReason;
    property ServerSoftwareName : string read FServerSoftwareName;
    property Version : TSSHVersion read FVersion;
  published
    property Address: string read FAddress write FAddress;
    property AuthenticationTypes : integer read FAuthenticationTypes
      write FAuthenticationTypes default SSH_AUTH_TYPE_PASSWORD;
    property ClientHostname : string read FClientHostname
      write FClientHostname;
    property ClientUsername : string read FClientUsername
      write FClientUsername;
    property CompressionLevel : integer read FCompressionLevel
      write FCompressionLevel default 9;
    property DestHost : string read FDestHost write FDestHost;
    property DestPort : integer read FDestPort write FDestPort;
    property ForceCompression : boolean read FForceCompression
      write FForceCompression;
    property ForwardedPort : integer read FForwardedPort write FForwardedPort;
    property ForwardedHost : string read FForwardedHost write FForwardedHost;
    property KeyStorage: TElSSHCustomKeyStorage read GetKeyStorage write
      SetKeyStorage;
    property Password : string read FPassword write FPassword;
    property Port: Integer read FPort write FPort default 22;
    property SoftwareName : string read FSoftwareName write FSoftwareName;
    property SynchronizeGUI : boolean read FSynchronizeGUI write FSynchronizeGUI;
    property Username : string read FUsername write FUsername;
    property Versions : TSSHVersions read FVersions write FVersions
                   default [sbSSH1, sbSSH2];
    property SocketTimeout: Integer read FSocketTimeout write SetSocketTimeout
        default 0;
    // Socks support
    property SocksAuthentication: TElSocksAuthentication read FSocksAuthentication
        write FSocksAuthentication;
    property SocksPassword: string read FSocksPassword write FSocksPassword;
    property SocksPort: Integer read FSocksPort write FSocksPort default 1080;
    property SocksResolveAddress: Boolean read FSocksResolveAddress write
        FSocksResolveAddress default false;
    property SocksServer: string read FSocksServer write FSocksServer;
    property SocksUserCode: string read FSocksUserCode write FSocksUserCode;
    property SocksVersion: TElSocksVersion read FSocksVersion write FSocksVersion
        default elSocks5;
    property UseSocks: Boolean read FUseSocks write SetUseSocks default false;
    // Web tunneling support
    property UseWebTunneling: boolean read FUseWebTunneling write
        SetUseWebTunneling default false;
    property WebTunnelAddress: string read FWebTunnelAddress write
        FWebTunnelAddress;
    property WebTunnelAuthentication: TElWebTunnelAuthentication read
        FWebTunnelAuthentication write FWebTunnelAuthentication 
        default wtaNoAuthentication;
    property WebTunnelPassword: string read FWebTunnelPassword write
        FWebTunnelPassword;
    property WebTunnelPort: Integer read FWebTunnelPort write FWebTunnelPort;
    property WebTunnelUserId: string read FWebTunnelUserId write FWebTunnelUserId;
    // Events 
    property OnAuthenticationKeyboard: TSSHAuthenticationKeyboardEvent read
        FOnAuthenticationKeyboard write FOnAuthenticationKeyboard;
    property OnAuthenticationFailed: TSSHAuthenticationFailedEvent read
        FOnAuthenticationFailed write FOnAuthenticationFailed;
    property OnAuthenticationSuccess: TNotifyEvent read FOnAuthenticationSuccess
        write FOnAuthenticationSuccess;
    property OnBanner: TSSHBannerEvent read FOnBanner write FOnBanner;
    property OnError: TSSHErrorEvent read FOnError write FOnError;
    property OnKeyValidate: TSSHKeyValidateEvent read FOnKeyValidate write
        FOnKeyValidate;
    property OnDebugData : TSSHDataEvent read FOnDebugData write FOnDebugData;
    property OnOpen : TNotifyEvent read FOnOpen write FOnOpen;
    property OnClose : TNotifyEvent read FOnClose write FOnClose;
    property OnConnectionOpen : TSBSSHConnectionEvent read FOnConnectionOpen
      write FOnConnectionOpen;
    property OnConnectionClose : TSBSSHConnectionEvent read FOnConnectionClose
      write FOnConnectionClose;
    property OnConnectionChange : TSBSSHConnectionEvent read FOnConnectionChange
      write FOnConnectionChange;
    property OnConnectionError : TSBSSHConnectionErrorEvent read FOnConnectionError
      write FOnConnectionError;
  end;

  // local port forwarding component
  TElSSHLocalPortForwarding = class(TElSSHCustomForwarding)
  protected
    function CreateSession : TElSSHForwardingSession; override;
  public
  end;

  // remote port forwarding component
  TElSSHRemotePortForwarding = class(TElSSHCustomForwarding)
  private
    FUseProxySettingsForForwardedConnections : boolean;
  protected
    function CreateSession : TElSSHForwardingSession; override;
  public
    constructor Create(Owner: TComponent); override;
  published
    property UseProxySettingsForForwardedConnections : boolean
      read FUseProxySettingsForForwardedConnections
      write FUseProxySettingsForForwardedConnections;
  end;

  // base class for SSH session thread
  TElSSHForwardingSession = class(TThread)
  private
    FOnReceive: TSSHReceiveEvent;
    FOnSend: TSSHSendEvent;
    FLastKey : TElSSHKey;
    FLastInt : integer;
    FLastStrList1 : TStringList;
    FLastStrList2 : TStringList;
    FLastBits : TBits;
    FLastBool : boolean;
    FLastStr1 : string;
    FLastStr2 : string;
    FConnections : TList;
    FLastConn : TElSSHForwardedConnection;
  protected

    FOwner : TElSSHCustomForwarding;
    FClient : TElSSHClient;
    FSocket : TElSocket;
    FTunnelList : TElSSHTunnelList;
    FUsername : string;
    FPassword : string;
    FForwardedPort: integer;
    FDestHost : string;
    FDestPort : integer;
    FError : boolean;
    FOnConnectionOpen : TSBSSHConnectionEvent;
    FOnConnectionChange : TSBSSHConnectionEvent;
    FOnConnectionRemove : TSBSSHConnectionEvent;
    FGuiCS : TCriticalSection;
    FChannelCount : integer;
    FCS : TCriticalSection;
    FConnListCS : TCriticalSection;
    procedure DoSynchronize(Func : TThreadMethod);
    procedure OnClientKeyValidate(Sender : TObject; ServerKey : TElSSHKey; var Validate : boolean);
    procedure OnClientAuthSuccess(Sender : TObject);
    procedure OnClientAuthFailed(Sender : TObject; AuthType: integer);
    procedure OnClientSend(Sender : TObject; Buffer: pointer; Size : integer);
    procedure OnClientReceive(Sender : TObject; Buffer : pointer; MaxSize : longint;
      out Written : longint);
    procedure OnClientClose(Sender : TObject);
    procedure OnClientError(Sender : TObject; ErrorCode : integer);
    procedure OnClientAuthKeyboard(Sender : TObject; Prompts : TStringList;
        Echo : TBits; Responses : TStringList);
    procedure OnClientBanner(Sender: TObject; const Text : string; const Language : string);
    procedure OnClientDebugData(Sender : TObject; Buffer : pointer; Size : longint);
    procedure OnConnectionError(Conn: TElSSHForwardedConnection; ErrorCode: integer);
    procedure DoOpen;
    procedure DoClose;
    procedure DoKeyValidate;
    procedure DoAuthSuccess;
    procedure DoAuthFailed;
    procedure DoAuthKeyboard;
    procedure DoBanner;
    procedure DoDebugData;
    procedure DoError;
    procedure AddConnectionToList(Conn : TElSSHForwardedConnection);
    procedure RemoveConnectionFromList(Conn : TElSSHForwardedConnection);
    procedure RefreshConnection(Conn : TElSSHForwardedConnection);
    procedure RefreshConnectionList;
    procedure DoConnectionOpenSync;
    procedure DoConnectionChangeSync;
    procedure DoConnectionCloseSync;
    procedure DoConnectionsRefreshSync;
    procedure DoConnectionError;
    procedure SetupSSHClient;
  protected
    procedure ConnectViaSocket;
    procedure Execute; override;
    procedure Initialize; virtual;
    procedure Finalize; virtual;
    procedure BeforeConnecting; virtual;
    procedure SessionConnected; virtual;
    procedure SessionDisconnected; virtual;
    procedure SocketDisconnected; virtual;

  public
    constructor Create(Owner : TElSSHCustomForwarding); reintroduce; 
    destructor Destroy; override;
    property Username : string read FUsername write FUsername;
    property Password : string read FPassword write FPassword;
    property ForwardedPort: integer read FForwardedPort write FForwardedPort;
    property DestHost : string read FDestHost write FDestHost;
    property DestPort : integer read FDestPort write FDestPort;
    property OnConnectionOpen : TSBSSHConnectionEvent read FOnConnectionOpen
      write FOnConnectionOpen;
    property OnConnectionChange : TSBSSHConnectionEvent read FOnConnectionChange
      write FOnConnectionChange;
    property OnConnectionRemove : TSBSSHConnectionEvent read FOnConnectionRemove
      write FOnConnectionRemove;
    property OnReceive: TSSHReceiveEvent read FOnReceive write FOnReceive;
    property OnSend: TSSHSendEvent read FOnSend write FOnSend;
  end;

  TElSSHLPFListeningThread = class( TThread)
  private
    FSocket : TElSocket;
    FAddress : string;
    FPort : integer;
    FOnAccept : TSBSSHLPFAcceptEvent;
    FOwner : TElSSHForwardingSession;
  protected
    procedure Execute; override;
  public
    constructor Create(Owner : TElSSHForwardingSession; const Address : string; Port : integer); reintroduce; 
    destructor Destroy; override;
    property OnAccept : TSBSSHLPFAcceptEvent read FOnAccept write FOnAccept;
  end;

  TElSSHLocalPortForwardingSession = class(TElSSHForwardingSession)
  private
    FTunnel : TElLocalPortForwardSSHTunnel;
    FServerThread : TElSSHLPFListeningThread;
    procedure OnServerThreadTerminate(Sender: TObject);
    procedure OnServerThreadAccept(Sender: TObject;
      Socket : TElSocket);
    procedure OnTunnelOpen(Sender : TObject;
      TunnelConnection : TElSSHTunnelConnection);
    procedure OnTunnelClose(Sender : TObject;
      TunnelConnection : TElSSHTunnelConnection);
    procedure OnTunnelError(Sender : TObject;
      Error : integer; Data : pointer);
  protected
    procedure Initialize; override;
    procedure Finalize; override;
    procedure BeforeConnecting; override;
    procedure SessionConnected; override;
    procedure SessionDisconnected; override;
    procedure SocketDisconnected; override;
    procedure StartListeningThread;
    procedure StopListeningThread;
  public
  end;

  TElSSHRemotePortForwardingSession = class(TElSSHForwardingSession)
  private
    FTunnel : TElRemotePortForwardSSHTunnel;
    procedure OnTunnelOpen(Sender : TObject;
      TunnelConnection : TElSSHTunnelConnection);
    procedure OnTunnelClose(Sender : TObject;
      TunnelConnection : TElSSHTunnelConnection);
    procedure OnTunnelError(Sender : TObject;
      Error : integer; Data : pointer);
  protected
    procedure Initialize; override;
    procedure Finalize; override;
    procedure BeforeConnecting; override;
    procedure SessionConnected; override;
    procedure SessionDisconnected; override;
    procedure SocketDisconnected; override;
  end;

  TSBSSHForwardingSocketState = (fssEstablishing, fssActive, fssClosing, fssClosed);
  TSBSSHForwardingChannelState = (fcsEstablishing, fcsActive, fcsClosing, fcsClosed);

  TElSSHForwardedConnection = class(TThread)
  private
    FSocket : TElSocket;
    FConnection : TElSSHTunnelConnection;
    FOwner : TElSSHForwardingSession;
    FSharedResource : TElSharedResource;
    FChannelBuffer : ByteArray;
    FSocketBuffer : ByteArray;
    FReceivedFromSocket : Int64;
    FSentToSocket : Int64;
    FReceivedFromChannel : Int64;
    FSentToChannel : Int64;
    FSocketState : TSBSSHForwardingSocketState;
    FChannelState : TSBSSHForwardingChannelState;
    FData : pointer;
    FCS : TCriticalSection;
    FEstablishSocketConnection : boolean;
    FHost : string;
    FPort : integer;
    FUseOwnerProxySettings : boolean;

    procedure SetSocket(Value: TElSocket);
    procedure SetConnection(Value: TElSSHTunnelConnection);
    function ReadFromSocketBuffer(Buffer: pointer; MaxSize: integer): integer;
    function ReadFromChannelBuffer(Buffer: pointer; MaxSize: integer): integer;
    procedure WriteToSocketBuffer(Buffer: pointer; Size: integer);
    procedure WriteToChannelBuffer(Buffer: pointer; Size: integer);
    procedure OnConnectionData(Sender: TObject; Buffer: pointer; Size: integer);
    procedure ConnectViaSocket;

  protected
    procedure Execute; override;
    procedure ConnClosed;

    property EstablishSocketConnection : boolean read FEstablishSocketConnection
      write FEstablishSocketConnection;
    property Host : string read FHost write FHost;
    property Port : integer read FPort write FPort;
    property UseOwnerProxySettings : boolean read FUseOwnerProxySettings
      write FUseOwnerProxySettings;
    property Connection : TElSSHTunnelConnection read FConnection write SetConnection;
  public
    constructor Create(Owner : TElSSHForwardingSession); reintroduce; 
    destructor Destroy; override;
    procedure Close;
    property Socket : TElSocket read FSocket write SetSocket;
    property ReceivedFromSocket : Int64 read FReceivedFromSocket;
    property SentToSocket : Int64 read FSentToSocket;
    property ReceivedFromChannel : Int64 read FReceivedFromChannel;
    property SentToChannel : Int64 read FSentToChannel;
    property SocketState : TSBSSHForwardingSocketState read FSocketState;
    property ChannelState : TSBSSHForwardingChannelState read FChannelState;
    property Data : pointer read FData write FData;
  end;

  EElSSHForwardingException = class(ESecureBlackboxError);

procedure Register;

implementation

resourcestring
  SConnFailed = 'SSH connection failed due to error (%d)';
  SConnError = 'SSH connection error (%d)';
  SConnError2 = 'SSH connection error (%s)';
  SIndexOutOfBounds = 'Index out of bounds (%d)';
  SFailedToBindServerSocket = 'Failed to bind server socket';
  SAlreadyOpened = 'Component is already connected';
  SConnFailed2 = 'SSH connection failed';

procedure Register;
begin
  RegisterComponents('SSHBlackbox', [TElSSHLocalPortForwarding, TElSSHRemotePortForwarding]);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSSHCustomForwarding class

constructor TElSSHCustomForwarding.Create(AOwner : TComponent);
begin
  inherited;
  SetupSSHProperties;
  FSynchronizeGUI := true;
  FForwardedHost := '';
end;


destructor TElSSHCustomForwarding.Destroy;
begin
  if FSession <> nil then
    Close;
  inherited;
end;

procedure TElSSHCustomForwarding.Open;
begin
  if FSession <> nil then
    raise EElSSHForwardingException.Create(SAlreadyOpened);
  FSession := CreateSession;
  FSession.FreeOnTerminate := true;
  FSession.Resume;
end;

procedure TElSSHCustomForwarding.OnSessionTerminate(Sender: TObject);
begin
  FSession := nil;
end;

procedure TElSSHCustomForwarding.Close;
begin
  if FSession <> nil then
  begin
    FSession.Terminate;
    while FSession <> nil do
      Sleep(0);
  end;
end;

procedure TElSSHCustomForwarding.DoAuthenticationFailed(Sender : TObject; AuthenticationType :
  integer);
begin
  if Assigned(FOnAuthenticationFailed) then
    FOnAuthenticationFailed(Self, AuthenticationType);
end;

procedure TElSSHCustomForwarding.DoAuthenticationKeyboard(Sender : TObject; Prompts : TStringList;
    Echo : TBits; Responses : TStringList);
begin
  if Assigned(FOnAuthenticationKeyboard) then
    FOnAuthenticationKeyboard(Self, Prompts, Echo, Responses);
end;

procedure TElSSHCustomForwarding.DoAuthenticationSuccess(Sender : TObject);
begin
  if Assigned(FOnAuthenticationSuccess) then
    FOnAuthenticationSuccess(Self);
end;

procedure TElSSHCustomForwarding.DoError(Sender : TObject; ErrorCode : integer);
begin
  if Assigned(FOnError) then
    FOnError(Self, ErrorCode);
end;

procedure TElSSHCustomForwarding.DoKeyValidate(Sender : TObject; ServerKey : TElSSHKey; var Validate :
  boolean);
begin
  if Assigned(FOnKeyValidate) then
    FOnKeyValidate(Self, ServerKey, Validate);
end;

procedure TElSSHCustomForwarding.DoBanner(Sender: TObject; const Text : string; const Language : string);
begin
  if Assigned(FOnBanner) then
    FOnBanner(Self, Text, Language);
end;

function TElSSHCustomForwarding.GetEncryptionAlgorithms(Index : TSSHEncryptionAlgorithm) : boolean;
begin
  Result := FEncryptionAlgorithms[Index];
end;

procedure TElSSHCustomForwarding.SetEncryptionAlgorithms(Index : TSSHEncryptionAlgorithm; Value : boolean);
begin
  FEncryptionAlgorithms[Index] := Value;
end;

function TElSSHCustomForwarding.GetCompressionAlgorithms(Index : TSSHCompressionAlgorithm) : boolean;
begin
  Result := FCompressionAlgorithms[Index];
end;

procedure TElSSHCustomForwarding.SetCompressionAlgorithms(Index : TSSHCompressionAlgorithm; Value : boolean);
begin
  FCompressionAlgorithms[Index] := Value;
end;

function TElSSHCustomForwarding.GetMACAlgorithms(Index : TSSHMacAlgorithm) : boolean;
begin
  Result := FMacAlgorithms[Index];
end;

procedure TElSSHCustomForwarding.SetMACAlgorithms(Index : TSSHMacAlgorithm; Value : boolean);
begin
  FMacAlgorithms[Index] := Value;
end;

function TElSSHCustomForwarding.GetKexAlgorithms(Index : TSSHKexAlgorithm) : boolean;
begin
  Result := FKexAlgorithms[Index];
end;

procedure TElSSHCustomForwarding.SetKexAlgorithms(Index : TSSHKexAlgorithm; Value : boolean);
begin
  FKexAlgorithms[Index] := Value;
end;

function TElSSHCustomForwarding.GetPublicKeyAlgorithms(Index : TSSHPublicKeyAlgorithm) : boolean;
begin
  Result := FPublicKeyAlgorithms[Index];
end;

procedure TElSSHCustomForwarding.SetPublicKeyAlgorithms(Index : TSSHPublicKeyAlgorithm; Value : boolean);
begin
  FPublicKeyAlgorithms[Index] := Value;
end;

function TElSSHCustomForwarding.GetKeyStorage: TElSSHCustomKeyStorage;
begin
  Result := FKeyStorage;
end;

procedure TElSSHCustomForwarding.SetKeyStorage(Value: TElSSHCustomKeyStorage);
begin
  FKeyStorage := Value;
  if FKeyStorage <> nil then
    FKeyStorage.FreeNotification(Self);
end;

procedure TElSSHCustomForwarding.SetSocketTimeout(Value: Integer);
begin
  if (FSocketTimeout <> Value) and (FSocketTimeout >= 0) then
  begin
    FSocketTimeout := Value;
  end;
end;

procedure TElSSHCustomForwarding.SetUseSocks(const Value: Boolean);
begin
  FUseSocks := Value;
  if Value then
    UseWebTunneling := false;
end;

procedure TElSSHCustomForwarding.SetUseWebTunneling(const Value: boolean);
begin
  FUseWebTunneling := Value;
  if Value then
    UseSocks := false;
end;

procedure TElSSHCustomForwarding.Notification(AComponent: TComponent;
  AOperation : TOperation);
begin
  inherited;
  if (AComponent = FKeyStorage) and (AOperation = opRemove) then
    KeyStorage := nil;
end;

function TElSSHCustomForwarding.CreateSession : TElSSHForwardingSession;
begin
  Result := nil;
end;

procedure TElSSHCustomForwarding.SetupSSHProperties;
var
  I : integer;
begin
  for I := SSH_EA_FIRST to SSH_EA_LAST do
    EncryptionAlgorithms[I] := true;
  for I := SSH_CA_FIRST to SSH_CA_LAST do
    CompressionAlgorithms[I] := true;
  for I := SSH_MA_FIRST to SSH_MA_LAST do
    MacAlgorithms[I] := true;
  for I := SSH_PK_FIRST to SSH_PK_LAST do
    PublicKeyAlgorithms[I] := true;
  for I := SSH_KEX_FIRST to SSH_KEX_LAST do
    KexAlgorithms[I] := true;
  FAuthenticationTypes := SSH_AUTH_TYPE_PASSWORD;
  FSoftwareName := 'EldoS.SSHBlackbox.4';
  FVersions := [sbSSH2];
end;

function TElSSHCustomForwarding.GetConnection(Index: integer) : TElSSHForwardedConnection;
begin
  if Assigned(FSession) then
    Result := TElSSHForwardedConnection(FSession.FConnections[Index])
  else
    raise EElSSHForwardingException.CreateFmt(SIndexOutOfBounds, [Index]);
end;

function TElSSHCustomForwarding.GetConnectionCount : integer;
begin
  if Assigned(FSession) then
    Result := FSession.FConnections.Count
  else
    Result := 0;
end;

function TElSSHCustomForwarding.GetActive : boolean;
begin
  Result := (FSession <> nil);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSSHLocalPortForwarding class

function TElSSHLocalPortForwarding.CreateSession : TElSSHForwardingSession;
begin
  Result := TElSSHLocalPortForwardingSession.Create(Self);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSSHLPFSession class


constructor TElSSHForwardingSession.Create(Owner : TElSSHCustomForwarding);
begin
  inherited Create(true);
  FTunnelList := TElSSHTunnelList.Create(nil);
  FClient := TElSSHClient.Create(nil);
  FClient.TunnelList := FTunnelList;
  
  FClient.OnKeyValidate := OnClientKeyValidate;
  FClient.OnAuthenticationSuccess := OnClientAuthSuccess;
  FClient.OnAuthenticationKeyboard := OnClientAuthKeyboard;
  FClient.OnAuthenticationFailed := OnClientAuthFailed;
  FClient.OnBanner := OnClientBanner;
  FClient.OnSend := OnClientSend;
  FClient.OnReceive := OnClientReceive;
  FClient.OnCloseConnection := OnClientClose;
  FClient.OnError := OnClientError;
  FClient.OnDebugData := OnClientDebugData;
  
  FClient.CloseIfNoActiveTunnels := false;
  FClient.ThreadSafe := false;
  FSocket := TElSocket.Create();
  FGuiCS := TCriticalSection.Create;
  FCS := TCriticalSection.Create;
  FConnListCS := TCriticalSection.Create;
  FOwner := Owner;
  FConnections := TList.Create;
  SetupSSHClient;
  Initialize;
end;



destructor TElSSHForwardingSession.Destroy;
begin
  while FConnections.Count > 0 do
    Sleep(0);
  Finalize;
  FreeAndNil(FConnections);
  FreeAndNil(FTunnelList);
  FreeAndNil(FClient);
  FreeAndNil(FSocket);
  FreeAndNil(FConnListCS);
  FreeAndNil(FGuiCS);
  FreeAndNil(FCS);
  inherited;
end;

procedure TElSSHForwardingSession.SetupSSHClient;
var
  I : integer;
begin
  for I := SSH_EA_FIRST to SSH_EA_LAST do
    FClient.EncryptionAlgorithms[I] := FOwner.EncryptionAlgorithms[I];
  for I := SSH_CA_FIRST to SSH_CA_LAST do
    FClient.CompressionAlgorithms[I] := FOwner.CompressionAlgorithms[I];
  for I := SSH_PK_FIRST to SSH_PK_LAST do
    FClient.PublicKeyAlgorithms[I] := FOwner.PublicKeyAlgorithms[I];
  for I := SSH_KEX_FIRST to SSH_KEX_LAST do
    FClient.KexAlgorithms[I] := FOwner.KexAlgorithms[I];
  for I := SSH_MA_FIRST to SSH_MA_LAST do
    FClient.MacAlgorithms[I] := FOwner.MacAlgorithms[I];
  FClient.KeyStorage := FOwner.KeyStorage;
  FClient.AuthenticationTypes := FOwner.AuthenticationTypes;
  FClient.Versions := FOwner.Versions;
  FClient.ClientUserName := FOwner.ClientUsername;
  FClient.ClientHostName := FOwner.ClientHostname;
  FClient.UserName := FOwner.Username;
  FClient.Password := FOwner.Password;
  FClient.SoftwareName := FOwner.SoftwareName;
  FClient.ForceCompression := FOwner.ForceCompression;
  FClient.CompressionLevel := FOwner.CompressionLevel;
  FClient.CloseIfNoActiveTunnels := false;
end;

procedure TElSSHForwardingSession.ConnectViaSocket;
var
  err : integer;
begin
  FSocket.Address := FOwner.Address;
  FSocket.Port := FOwner.Port;
  FSocket.Init(istStream);

  FSocket.UseSocks := FOwner.UseSocks;
  FSocket.SocksAuthentication := FOwner.SocksAuthentication;
  FSocket.SocksPassword := FOwner.SocksPassword;
  FSocket.SocksUserCode := FOwner.SocksUserCode;
  FSocket.SocksPort := FOwner.SocksPort;
  FSocket.SocksResolveAddress := FOwner.SocksResolveAddress;
  FSocket.SocksServer := FOwner.SocksServer;
  FSocket.SocksPort := FOwner.SocksPort;
  FSocket.SocksVersion := FOwner.SocksVersion;
  FSocket.UseWebTunneling := FOwner.UseWebTunneling;
  FSocket.WebTunnelAddress := FOwner.WebTunnelAddress;
  FSocket.WebTunnelPort := FOwner.WebTunnelPort;
  FSocket.WebTunnelAuthentication := FOwner.WebTunnelAuthentication;
  FSocket.WebTunnelUserId := FOwner.WebTunnelUserId;
  FSocket.WebTunnelPassword := FOwner.WebTunnelPassword;

  err := FSocket.Connect(FOwner.SocketTimeout);
  if err <> 0 then
    raise EElSSHForwardingException.Create(Format(SConnError, [err]));
end;

procedure TElSSHForwardingSession.Initialize;
begin
  ;
end;

procedure TElSSHForwardingSession.Finalize;
begin
  ;
end;

procedure TElSSHForwardingSession.Execute;
begin
  try
    try
      BeforeConnecting;
      FError := false;
      FChannelCount := 0;
      // establishing TCP session
      try
        ConnectViaSocket;
      except
        OnClientError(Self, ERROR_SSH_TCP_CONNECTION_FAILED);
        raise;
      end;
      // establishing SSH session
      FClient.Open;
      while (not FClient.Active) and (not FError) and (not Terminated) do
        FClient.DataAvailable;
      if (FError) or (not FClient.Active) then
        raise EElSSHForwardingException.Create(SConnFailed2);
      try
        SessionConnected;
        if Assigned(FOwner.FOnOpen) then
        begin
          FGuiCS.Acquire;
          try
            DoSynchronize(DoOpen);
          finally
            FGuiCS.Release;
          end;
        end;
        try
          // running client loop
          while (FClient.Active) and (FSocket.State = issConnected) and (not FError) and (not Terminated) do
          begin
            if FSocket.CanReceive(500) then
            begin
              FCS.Acquire;
              try
                FClient.DataAvailable;
              finally
                FCS.Release;
              end;
            end
            else
              Sleep(0);
          end;
        finally
          SessionDisconnected;
        end;
      finally
        // session closed, cleanup
        if FClient.Active then
          FClient.Close(true);
        if FSocket.State = issConnected then
          FSocket.Close(true);
        SocketDisconnected;
      end;
    finally
      if Assigned(FOwner) then
        FOwner.OnSessionTerminate(Self);
      if Assigned(FOwner.FOnClose) then
      begin
        FGuiCS.Acquire;
        try
          DoSynchronize(DoClose);
        finally
          FGuiCS.Release;
        end;
      end;
    end;
  except
    ;
  end;
end;

procedure TElSSHForwardingSession.BeforeConnecting;
begin
  ;
end;

procedure TElSSHForwardingSession.SessionConnected;
begin
  ;
end;

procedure TElSSHForwardingSession.SessionDisconnected;
begin
  ;
end;

procedure TElSSHForwardingSession.SocketDisconnected;
begin
  ;
end;

procedure TElSSHForwardingSession.OnClientKeyValidate(Sender : TObject; ServerKey : TElSSHKey;
  var Validate : boolean);
begin
  if not Assigned(FOwner.FOnKeyValidate) then
    Exit;
  FGuiCS.Acquire;
  try
    FLastKey := ServerKey;
    FLastBool := Validate;
    DoSynchronize(DoKeyValidate);
    Validate := FLastBool;
  finally
    FGuiCS.Release;
  end;
end;

procedure TElSSHForwardingSession.OnClientAuthSuccess(Sender : TObject);
begin
  if not Assigned(FOwner.FOnAuthenticationSuccess) then
    Exit;
  FGuiCS.Acquire;
  try
    DoSynchronize(DoAuthSuccess);
  finally
    FGuiCS.Release;
  end;
end;

procedure TElSSHForwardingSession.OnClientAuthFailed(Sender : TObject; AuthType: integer);
begin
  if not Assigned(FOwner.FOnAuthenticationFailed) then
    Exit;
  FGuiCS.Acquire;
  try
    FLastInt := AuthType;
    DoSynchronize(DoAuthFailed);
  finally
    FGuiCS.Release;
  end;
end;

procedure TElSSHForwardingSession.OnClientSend(Sender : TObject; Buffer: pointer; Size : integer);
var
  Sent : integer;
  Ptr : ^byte;
begin
  try
    if FSocket.State = issConnected then
    begin
      Ptr := Buffer;
      while Size > 0 do
      begin
        if FSocket.CanSend(200) then
        begin
          FSocket.Send(Ptr, Size, Sent);
          if Sent = 0 then
          begin
            FError := true;
            Break;
          end;
          Inc(Ptr, Sent);
          Dec(Size, Sent);
        end
        else
          Sleep(0);
      end;
    end;
  except
    FError := true;
  end;
end;

procedure TElSSHForwardingSession.OnClientReceive(Sender : TObject; Buffer : pointer; MaxSize : longint;
  out Written : longint);
var
  Time : integer;
begin
  try
    if FOwner.SocketTimeout = 0 then
      Time := -1
    else
      Time := 100;
    if FSocket.CanReceive(Time) then
    begin
      FSocket.Receive(Buffer, MaxSize, Written);
      if Written = 0 then
        FError := true;
    end
    else
      Written := 0;
  except
    FError := true;
  end;
end;

procedure TElSSHForwardingSession.OnClientClose(Sender : TObject);
begin
  FError := true;
end;

procedure TElSSHForwardingSession.OnClientError(Sender : TObject; ErrorCode : integer);
begin
  if not Assigned(FOwner.FOnError) then
    Exit;
  FGuiCS.Acquire;
  try
    FLastInt := ErrorCode;
    DoSynchronize(DoError);
  finally
    FGuiCS.Release;
  end;
  FError := true;
end;

procedure TElSSHForwardingSession.OnClientAuthKeyboard(Sender : TObject; Prompts : TStringList;
  Echo : TBits; Responses : TStringList);
begin
  if not Assigned(FOwner.FOnAuthenticationKeyboard) then
    Exit;
  FGuiCS.Acquire;
  try
    FLastStrList1 := Prompts;
    FLastStrList2 := Responses;
    FLastBits := Echo;
    DoSynchronize(DoAuthKeyboard);
  finally
    FGuiCS.Release;
  end;
end;

procedure TElSSHForwardingSession.OnClientBanner(Sender: TObject; const Text : string; const Language : string);
begin
  if not Assigned(FOwner.FOnBanner) then
    Exit;
  FGuiCS.Acquire;
  try
    FLastStr1 := Text;
    FLastStr2 := Language;
    DoSynchronize(DoBanner);
  finally
    FGuiCS.Release;
  end;
end;

procedure TElSSHForwardingSession.OnClientDebugData(Sender : TObject; Buffer : pointer; Size : longint);
begin
  if not Assigned(FOwner.FOnDebugData) then
    Exit;
  FGuiCS.Acquire;
  try
    SetLength(FLastStr1, Size);
    Move(Buffer^, FLastStr1[1], Length(FLastStr1));
    DoSynchronize(DoDebugData);
  finally
    FGuiCS.Release;
  end;
end;

procedure TElSSHForwardingSession.DoOpen;
begin
  FOwner.FOnOpen(FOwner);
end;

procedure TElSSHForwardingSession.DoClose;
begin
  FOwner.FOnClose(FOwner);
end;

procedure TElSSHForwardingSession.DoKeyValidate;
begin
  FOwner.FOnKeyValidate(FOwner, FLastKey, FLastBool);
end;

procedure TElSSHForwardingSession.DoAuthSuccess;
begin
  FOwner.FOnAuthenticationSuccess(FOwner);
end;

procedure TElSSHForwardingSession.DoAuthFailed;
begin
  FOwner.FOnAuthenticationFailed(FOwner, FLastInt);
end;

procedure TElSSHForwardingSession.DoAuthKeyboard;
begin
  FOwner.FOnAuthenticationKeyboard(FOwner, FLastStrList1, FLastBits, FLastStrList2);
end;

procedure TElSSHForwardingSession.DoBanner;
begin
  FOwner.FOnBanner(FOwner, FLastStr1, FLastStr2);
end;

procedure TElSSHForwardingSession.DoError;
begin
  FOwner.FOnError(FOwner, FLastInt);
end;

procedure TElSSHForwardingSession.DoDebugData;
begin
  FOwner.FOnDebugData(FOwner, @FLastStr1[1], Length(FLastStr1));
end;

procedure TElSSHForwardingSession.DoConnectionOpenSync;
begin
  FOwner.FOnConnectionOpen(FOwner, FLastConn);
end;

procedure TElSSHForwardingSession.DoConnectionChangeSync;
begin
  FOwner.FOnConnectionChange(FOwner, FLastConn);
end;

procedure TElSSHForwardingSession.DoConnectionCloseSync;
begin
  FOwner.FOnConnectionClose(FOwner, FLastConn);
end;

procedure TElSSHForwardingSession.DoConnectionsRefreshSync;
begin
  FOwner.FOnConnectionsRefresh(FOwner);
end;

procedure TElSSHForwardingSession.DoConnectionError;
begin
  FOwner.FOnConnectionError(Self, FLastConn, FLastInt);
end;

procedure TElSSHForwardingSession.AddConnectionToList(Conn : TElSSHForwardedConnection);
begin
  FConnListCS.Acquire;
  try
    FConnections.Add(Conn);
    if Assigned(FOwner.FOnConnectionOpen) then
    begin
      FGuiCS.Acquire;
      try
        FLastConn := Conn;
        DoSynchronize( DoConnectionOpenSync);
      finally
        FGuiCS.Release;
      end;
    end;
  finally
    FConnListCS.Release;
  end;
end;

procedure TElSSHForwardingSession.RemoveConnectionFromList(Conn : TElSSHForwardedConnection);
begin
  FConnListCS.Acquire;
  try
    if Assigned(FOwner.FOnConnectionClose) then
    begin
      FGuiCS.Acquire;
      try
        FLastConn := Conn;
        DoSynchronize( DoConnectionCloseSync);
      finally
        FGuiCS.Release;
      end;
    end;
    FConnections.Remove(Conn);
  finally
    FConnListCS.Release;
  end;
end;

procedure TElSSHForwardingSession.RefreshConnection(Conn : TElSSHForwardedConnection);
begin
  if Assigned(FOwner.FOnConnectionChange) then
  begin
    FGuiCS.Acquire;
    try
      FLastConn := Conn;
      DoSynchronize( DoConnectionChangeSync);
    finally
      FGuiCS.Release;
    end;
  end;
end;

procedure TElSSHForwardingSession.OnConnectionError(Conn: TElSSHForwardedConnection;
  ErrorCode: integer);
begin
  if Assigned(FOwner.OnConnectionError) then
  begin
    FGuiCS.Acquire;
    try
      FLastConn := Conn;
      FLastInt := ErrorCode;
      DoSynchronize( DoConnectionError);
    finally
      FGuiCS.Release;
    end;
  end;
end;

procedure TElSSHForwardingSession.RefreshConnectionList;
begin
  // left for future
  {if Assigned(FOwner.FOnConnectionsRefresh) then
  begin
    FConnListCS.Acquire;
    try
      Synchronize(DoConnectionsRefreshSync);
    finally
      FConnListCS.Release;
    end;
  end;}
end;

procedure TElSSHForwardingSession.DoSynchronize(Func : TThreadMethod);
begin
  if FOwner.FSynchronizeGUI then
    Synchronize(Func)
  else
    Func();
end;

////////////////////////////////////////////////////////////////////////////////
// TElSSHLocalPortForwardingSession class

procedure TElSSHLocalPortForwardingSession.Initialize;
begin
  FTunnel := TElLocalPortForwardSSHTunnel.Create(nil);
  FServerThread := nil;
end;

procedure TElSSHLocalPortForwardingSession.Finalize;
begin
  FreeAndNil(FTunnel);
  if FServerThread <> nil then
    StopListeningThread;
end;

procedure TElSSHLocalPortForwardingSession.StartListeningThread;
begin
  if FServerThread <> nil then
    StopListeningThread;
  FServerThread := TElSSHLPFListeningThread.Create(Self, FOwner.ForwardedHost, FOwner.ForwardedPort);
  FServerThread.OnAccept := OnServerThreadAccept;
  FServerThread.FreeOnTerminate := true;
  FServerThread.Resume;
end;

procedure TElSSHLocalPortForwardingSession.StopListeningThread;
begin
  if FServerThread <> nil then
  begin
    FServerThread.Terminate;
    SBUtils.WaitFor(FServerThread.Handle);
  end;
end;

procedure TElSSHLocalPortForwardingSession.OnServerThreadTerminate(Sender: TObject);
begin
  FServerThread := nil;
  FError := true;
end;

procedure TElSSHLocalPortForwardingSession.OnServerThreadAccept(Sender: TObject;
  Socket : TElSocket);
var
  Conn : TElSSHForwardedConnection;
begin
  Conn := TElSSHForwardedConnection.Create(Self);
  Conn.FreeOnTerminate := true;
  AddConnectionToList(Conn);
  Conn.Socket := Socket;
  Conn.Resume;
  FCS.Acquire;
  try
    FTunnel.Open(Conn);
  finally
    FCS.Release;
  end;
end;

procedure TElSSHLocalPortForwardingSession.OnTunnelOpen(Sender : TObject;
  TunnelConnection : TElSSHTunnelConnection);
begin
  TElSSHForwardedConnection(TunnelConnection.Data).Connection := TunnelConnection;
end;

procedure TElSSHLocalPortForwardingSession.OnTunnelClose(Sender : TObject;
  TunnelConnection : TElSSHTunnelConnection);
var
  Fwd : TElSSHForwardedConnection;
begin
  Fwd := TElSSHForwardedConnection(TunnelConnection.Data);
  if Fwd <> nil then
  begin
    TunnelConnection.Data := nil;
    TElSSHForwardedConnection(Fwd).ConnClosed();
  end;
end;

procedure TElSSHLocalPortForwardingSession.OnTunnelError(Sender : TObject;
  Error : integer; Data : pointer);
var
  Fwd : TElSSHForwardedConnection;
begin
  Fwd := TElSSHForwardedConnection(Data);
  if Fwd <> nil then
  begin
    OnConnectionError(Fwd, Error);
    TElSSHForwardedConnection(Fwd).ConnClosed();
  end;
end;

procedure TElSSHLocalPortForwardingSession.BeforeConnecting;
begin
  FTunnel.TunnelList := FTunnelList;
  FTunnel.Port := FOwner.FForwardedPort;
  FTunnel.ToHost := FOwner.FDestHost;
  FTunnel.ToPort := FOwner.FDestPort;
  FTunnel.OnOpen := OnTunnelOpen;
  FTunnel.OnClose := OnTunnelClose;
  FTunnel.OnError := OnTunnelError;
end;

procedure TElSSHLocalPortForwardingSession.SessionConnected;
begin
  StartListeningThread;
end;

procedure TElSSHLocalPortForwardingSession.SessionDisconnected;
begin
  StopListeningThread;
end;

procedure TElSSHLocalPortForwardingSession.SocketDisconnected;
begin
  ;
end;

////////////////////////////////////////////////////////////////////////////////
// TElSSHLPFListeningThread class


constructor TElSSHLPFListeningThread.Create(Owner : TElSSHForwardingSession;
  const Address : string; Port : integer);
begin
  inherited Create(true);
  FOwner := Owner;
  FAddress := Address;
  FPort := Port;
  FSocket := TElSocket.Create();
end;



destructor TElSSHLPFListeningThread.Destroy;
begin
  FreeAndNil(FSocket);
  inherited;
end;

procedure TElSSHLPFListeningThread.Execute;
var
  AccSocket : TElSocket;
begin
  try
    FSocket.ListenAddress := FAddress;
    FSocket.ListenPort := FPort;
    try
      if FSocket.Bind <> 0 then
        raise EElSSHForwardingException.Create(SFailedToBindServerSocket);
      if FSocket.Listen <> 0 then
        raise EElSSHForwardingException.Create(SFailedToBindServerSocket);
    except
      if Assigned(FOwner) then
        FOwner.OnClientError(Self, ERROR_SSH_TCP_BIND_FAILED);
      Terminate;
    end;
    try
      try
        while not Terminated do
        begin
          AccSocket := nil;
          FSocket.Accept(2000, AccSocket);
          if (AccSocket <> nil) and Assigned(FOnAccept) then
            FOnAccept(Self, AccSocket);
        end;
      finally
        FSocket.Close(true);
      end;
    except
      ;
    end;
  finally
    if Assigned(FOwner) then
      TElSSHLocalPortForwardingSession(FOwner).OnServerThreadTerminate(Self);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TElSSHRemotePortForwarding class

constructor TElSSHRemotePortForwarding.Create(Owner: TComponent);
begin
  inherited;
  FUseProxySettingsForForwardedConnections := false;
end;

function TElSSHRemotePortForwarding.CreateSession : TElSSHForwardingSession;
begin
  Result := TElSSHRemotePortForwardingSession.Create(Self);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSSHRemotePortForwardingSession class

procedure TElSSHRemotePortForwardingSession.Initialize;
begin
  FTunnel := TElRemotePortForwardSSHTunnel.Create(nil);
end;

procedure TElSSHRemotePortForwardingSession.Finalize;
begin
  FreeAndNil(FTunnel);
end;

procedure TElSSHRemotePortForwardingSession.BeforeConnecting;
begin
  FTunnel.TunnelList := FTunnelList;
  FTunnel.Port := FOwner.FForwardedPort;
  FTunnel.ToHost := FOwner.FDestHost;
  FTunnel.ToPort := FOwner.FDestPort;
  FTunnel.OnOpen := OnTunnelOpen;
  FTunnel.OnClose := OnTunnelClose;
  FTunnel.OnError := OnTunnelError;
end;

procedure TElSSHRemotePortForwardingSession.SessionConnected;
begin
  ;
end;

procedure TElSSHRemotePortForwardingSession.SessionDisconnected;
begin
  ;
end;

procedure TElSSHRemotePortForwardingSession.SocketDisconnected;
begin
  ;
end;

procedure TElSSHRemotePortForwardingSession.OnTunnelOpen(Sender : TObject; TunnelConnection : TElSSHTunnelConnection);
var
  Conn : TElSSHForwardedConnection;
begin
  Conn := TElSSHForwardedConnection.Create(Self);
  Conn.FreeOnTerminate := true;
  AddConnectionToList(Conn);
  Conn.EstablishSocketConnection := true;
  Conn.Host := FOwner.DestHost;
  Conn.Port := FOwner.DestPort;
  Conn.UseOwnerProxySettings := TElSSHRemotePortForwarding(FOwner).UseProxySettingsForForwardedConnections;
  Conn.Connection := TunnelConnection;
  TunnelConnection.Data := Conn;
  Conn.Resume;
end;

procedure TElSSHRemotePortForwardingSession.OnTunnelClose(Sender : TObject; TunnelConnection : TElSSHTunnelConnection);
var
  Fwd : TElSSHForwardedConnection;
begin
  Fwd := TElSSHForwardedConnection(TunnelConnection.Data);
  if Fwd <> nil then
  begin
    TunnelConnection.Data := nil;
    TElSSHForwardedConnection(Fwd).ConnClosed();
  end;
end;

procedure TElSSHRemotePortForwardingSession.OnTunnelError(Sender : TObject;
  Error : integer; Data : pointer);
var
  Fwd : TElSSHForwardedConnection;
begin
  Fwd := TElSSHForwardedConnection(Data);
  if Fwd <> nil then
    TElSSHForwardedConnection(Fwd).ConnClosed();
end;

////////////////////////////////////////////////////////////////////////////////
// TElSSHForwardedConnection class


constructor TElSSHForwardedConnection.Create(Owner : TElSSHForwardingSession);
begin
  inherited Create(true);
  FOwner := Owner;
  FSharedResource := TElSharedResource.Create;
  SetLength(FChannelBuffer, 0);
  SetLength(FSocketBuffer, 0);
  FReceivedFromSocket := 0;
  FReceivedFromChannel := 0;
  FSentToSocket := 0;
  FSentToChannel := 0;
  FSocketState := fssEstablishing;
  FChannelState := fcsEstablishing;
  FCS := TCriticalSection.Create;
  FEstablishSocketConnection := false;
  FHost := '';
  FPort := 0;
  FUseOwnerProxySettings := false;
end;



destructor TElSSHForwardedConnection.Destroy;
begin
  if FSocket <> nil then
    FreeAndNil(FSocket);
  FreeAndNil(FCS);
  FreeAndNil(FSharedResource);
  inherited;
end;

procedure TElSSHForwardedConnection.Close;
begin
  Terminate;
end;

procedure TElSSHForwardedConnection.Execute;
var
  Buf : ByteArray;
  Read, Sent, Left : integer;
  Ptr : ^byte;
  SendDone : boolean;
  Changed : boolean;
const
  MAX_CACHE_SIZE : integer = $20000;
begin
  if (FEstablishSocketConnection) and (Length(FHost) > 0) then
  begin
    FSocket := TElSocket.Create();
    try
      ConnectViaSocket();
    except
      FSocketState := fssClosed;
      FChannelState := fcsClosed;
    end;
  end;
  SetLength(Buf, 65536);
  // running server loop
  while (not Terminated) {and (FSocket.Connected)} and
    (not ((FSocketState = fssClosed) and (FChannelState = fcsClosed))) do
  begin
    Changed := false;

    // processing socket endpoint
    if (FSocketState = fssActive) and (FSocket.State = issConnected) then
    begin
      try
        // reading from socket connection
        if (Length(FChannelBuffer) < MAX_CACHE_SIZE) and (FSocket.CanReceive(200)) then
        begin
          FSocket.Receive(@Buf[0], Length(Buf), Read);
          if Read > 0 then
            WriteToChannelBuffer(@Buf[0], Read)
          else
          begin
            FSocketState := fssClosed;
            FSocket.Close(true);
          end;
          Inc(FReceivedFromSocket, Read);
          Changed := true;
        end;
        // writing to socket connection
        repeat
          Read := ReadFromSocketBuffer(@Buf[0], Length(Buf));
          if Read > 0 then
          begin
            Left := Read;
            Ptr := @Buf[0];
            while Left > 0 do
            begin
              if FSocket.CanSend(200) then
              begin
                FSocket.Send(Ptr, Left, Sent);
                Inc(Ptr, Sent);
                Dec(Left, Sent);
                Inc(FSentToSocket, Sent);
              end
              else
                Sleep(0);
            end;
            Changed := true;
          end;
        until Read = 0;
      except
        FSocketState := fssClosed;
        Changed := true;
      end;
    end;
    // processing channel endpoint
    if FChannelState = fcsActive then
    begin
      repeat
        Read := ReadFromChannelBuffer(@Buf[0], Length(Buf));
        if Read > 0 then
        begin
          SendDone := false;
          while (not SendDone) and (not Terminated) and (FChannelState = fcsActive) do
          begin
            FOwner.FCS.Acquire;
            try
              if FConnection.CanSend then
              begin
                FConnection.SendData(@Buf[0], Read);
                SendDone := true;
              end;
            finally
              FOwner.FCS.Release;
            end;
            if not SendDone then
              Sleep(200);
          end;
          if SendDone then
            Inc(FSentToChannel, Read);
          Changed := true;
        end;
      until Read = 0;
    end;

    // re-adjusting connection states
    if (FSocketState = fssActive) and (FChannelState in [fcsClosed, fcsClosing]) and (Length(FSocketBuffer) = 0) then
    begin
      Changed := true;
      FSocketState := fssClosing;
      FSocket.Close(true);
    end
    else
    if (FChannelState = fcsActive) and (FSocketState in [fssClosing, fssClosed]) and (Length(FChannelBuffer) = 0) then
    begin
      Changed := true;
      FChannelState := fcsClosing;
      FOwner.FCS.Acquire;
      try
        FConnection.Close(true);
      finally
        FOwner.FCS.Release;
      end;
    end
    else
    if (FSocketState = fssClosing) and (not (FSocket.State = issConnected)) then
    begin
      Changed := true;
      FSocketState := fssClosed;
    end;
    if Changed then
      FOwner.RefreshConnection(Self);
    Sleep(0);
  end;
  if Terminated then
  begin
    if FSocketState = fssActive then
    begin
      FSocketState := fssClosing;
      FSocket.Close(true);
    end;
    if FChannelState = fcsActive then
    begin
      FChannelState := fcsClosing;
      FOwner.FCS.Acquire;
      try
        FConnection.Close(false);
      finally
        FOwner.FCS.Release;
      end;
    end;
  end;
  SetLength(Buf, 0);
  FCS.Acquire;
  try
    if Assigned(FConnection) then
      FConnection.OnData := nil;
  finally
    FCS.Release;
  end;
  FOwner.RemoveConnectionFromList(Self);
end;

procedure TElSSHForwardedConnection.SetSocket(Value: TElSocket);
begin
  FCS.Acquire;
  try
    FSocket := Value;
    if FSocket.State = issConnected then
      FSocketState := fssActive
    else if FSocket.State = issConnecting then
      FSocketState := fssEstablishing
    else
      FSocketState := fssClosed;
  finally
    FCS.Release;
  end;
end;

procedure TElSSHForwardedConnection.SetConnection(Value: TElSSHTunnelConnection);
begin
  FCS.Acquire;
  try
    FConnection := Value;
    FConnection.OnData := OnConnectionData;
    FChannelState := fcsActive;
  finally
    FCS.Release;
  end;
end;

procedure TElSSHForwardedConnection.ConnClosed;
begin
  FCS.Acquire;
  try
    FChannelState := fcsClosed;
    if FConnection <> nil then
      FConnection.OnData := nil;
    FConnection := nil;
  finally
    FCS.Release;
  end;
end;

procedure TElSSHForwardedConnection.OnConnectionData(Sender: TObject;
  Buffer: pointer; Size : integer);
begin
  FCS.Acquire;
  try
    WriteToSocketBuffer(Buffer, Size);
    Inc(FReceivedFromChannel, Size);
  finally
    FCS.Release;
  end;
end;

procedure TElSSHForwardedConnection.ConnectViaSocket;
var
  err : integer;
begin
  FSocket.Address := FHost;
  FSocket.Port := FPort;
  FSocket.Init(istStream);

  FSocket.UseSocks := FOwner.FOwner.UseSocks;
  FSocket.SocksAuthentication := FOwner.FOwner.SocksAuthentication;
  FSocket.SocksPassword := FOwner.FOwner.SocksPassword;
  FSocket.SocksUserCode := FOwner.FOwner.SocksUserCode;
  FSocket.SocksPort := FOwner.FOwner.SocksPort;
  FSocket.SocksResolveAddress := FOwner.FOwner.SocksResolveAddress;
  FSocket.SocksServer := FOwner.FOwner.SocksServer;
  FSocket.SocksPort := FOwner.FOwner.SocksPort;
  FSocket.SocksVersion := FOwner.FOwner.SocksVersion;
  FSocket.UseWebTunneling := FOwner.FOwner.UseWebTunneling;
  FSocket.WebTunnelAddress := FOwner.FOwner.WebTunnelAddress;
  FSocket.WebTunnelPort := FOwner.FOwner.WebTunnelPort;
  FSocket.WebTunnelAuthentication := FOwner.FOwner.WebTunnelAuthentication;
  FSocket.WebTunnelUserId := FOwner.FOwner.WebTunnelUserId;
  FSocket.WebTunnelPassword := FOwner.FOwner.WebTunnelPassword;

  err := FSocket.Connect(FOwner.FOwner.SocketTimeout);
  if err <> 0 then
    raise EElSSHForwardingException.Create(Format(SConnError, [err]));
  FSocketState := fssActive;
end;

function TElSSHForwardedConnection.ReadFromSocketBuffer(Buffer: pointer; MaxSize: integer): integer;
begin
  FSharedResource.WaitToRead;
  try
    Result := Min(MaxSize, Length(FSocketBuffer));
    Move(FSocketBuffer[0], Buffer^, Result);
    Move(FSocketBuffer[Result], FSocketBuffer[0], Length(FSocketBuffer) - Result);
    SetLength(FSocketBuffer, Length(FSocketBuffer) - Result);
  finally
    FSharedResource.Done;
  end;
end;

function TElSSHForwardedConnection.ReadFromChannelBuffer(Buffer: pointer; MaxSize: integer): integer;
begin
  FSharedResource.WaitToRead;
  try
    Result := Min(MaxSize, Length(FChannelBuffer));
    Move(FChannelBuffer[0], Buffer^, Result);
    Move(FChannelBuffer[Result], FChannelBuffer[0], Length(FChannelBuffer) - Result);
    SetLength(FChannelBuffer, Length(FChannelBuffer) - Result);
  finally
    FSharedResource.Done;
  end;
end;

procedure TElSSHForwardedConnection.WriteToSocketBuffer(Buffer: pointer; Size: integer);
var
  OldLen : integer;
begin
  FSharedResource.WaitToWrite;
  try
    OldLen := Length(FSocketBuffer);
    SetLength(FSocketBuffer, OldLen + Size);
    Move(Buffer^, FSocketBuffer[OldLen], Size);
  finally
    FSharedResource.Done;
  end;
end;

procedure TElSSHForwardedConnection.WriteToChannelBuffer(Buffer: pointer; Size: integer);
var
  OldLen : integer;
begin
  FSharedResource.WaitToWrite;
  try
    OldLen := Length(FChannelBuffer);
    SetLength(FChannelBuffer, OldLen + Size);
    Move(Buffer^, FChannelBuffer[OldLen], Size);
  finally
    FSharedResource.Done;
  end;
end;

end.
