(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBPKCS10;

interface

uses
  SBASN1,
  SBASN1Tree,
  SysUtils,
  Classes,
  SBMath, 
  SBSHA, 
  SBMD, 
  SBUtils, 
  SBConstants, 
  SBDSA, 
  SBRSA, 
  SBRDN, 
  SBX509Ext
  ;



type

  TSBCertReqFileFormat = (crfUnknown, crfDER, crfPEM);

  TElCertificateRequest = class(TSBControlBase)
  private
    FBuf : ByteArray;
    FBufAux : ByteArray;
    FSource : ByteArray;
    FState : integer;
    FStateAux : integer;
    FName : TElRelativeDistinguishedName;
    FAttributes : TElRelativeDistinguishedName;
    FNesting : integer;
    FOldNesting : integer;
    FVersion : integer;
    FPublicKeyAlgorithm : BufferType;
    FPublicKeyAlgorithmParams : BufferType;
    FPublicKey : BufferType;
    FSignatureAlgorithm : BufferType;
    FSignatureAlgorithmParams : BufferType;
    FSignature : BufferType;
    FRSAPublicModulus : BufferType;
    FRSAPublicExponent : BufferType;
    FLastOID : BufferType;
    FDSAY : BufferType;
    FDSAP : BufferType;
    FDSAQ : BufferType;
    FDSAG : BufferType;
    FDSAR : BufferType;
    FDSAS : BufferType;
    FPrivateKey : BufferType;
    FAlgorithm : integer;
    FSignAlgorithm : integer;
    FRequestInfo : ByteArray;
    FRequestInfoSz : integer;
    FPrivateKeyBlob : ByteArray;
    FExtensions : TElCertificateExtensions;
  protected
    procedure HandleASN1Read(Sender : TObject; Buffer : pointer; var Size : longint);
    procedure HandleASN1Tag(Sender : TObject; TagType: asn1TagType; TagConstrained: boolean;
      Tag: pointer; TagSize: integer; Size: integer; Data: pointer; BitRest: integer;
      var Valid : boolean);
    procedure HandleASN1ReadAux(Sender : TObject; Buffer : pointer; var Size : longint);
    procedure HandleASN1TagAux(Sender : TObject; TagType: asn1TagType; TagConstrained: boolean;
      Tag: pointer; TagSize: integer; Size: integer; Data: pointer; BitRest: integer;
      var Valid : boolean);

    function ValidateSignatureRSA : boolean;
    function ValidateSignatureDSA : boolean;
    function ParsePublicKey : boolean;
    function ParsePublicKeyRSA : boolean;
    function ParsePublicKeyDSA : boolean;
    function ParseAlgorithmParams : boolean;
    function CalculateSignature : BufferType;
    function CompareSignature(const Received : BufferType; const Real : BufferType) : boolean;
    procedure ClearInfo;
    function WriteCertificateRequest : BufferType;
    function WriteCertificationRequestInfo : BufferType;
    function WriteSignatureAlgorithm : BufferType;
    function WriteSignature : BufferType;
    function WriteName(Name : TElRelativeDistinguishedName) : BufferType;
    function WriteSubjectPublicKeyInfo : BufferType;
    function WriteAttributes : BufferType;
    procedure GenerateRSA(Bits : integer);
    procedure GenerateDSA(Bits : integer);
    function Sign(const S : BufferType) : BufferType;
    function SignRSA(const S : BufferType) : BufferType;
    function SignDSA(const S : BufferType) : BufferType;
    procedure ProcessAttributes;
  public
    constructor Create(Owner : TSBComponentBase);  override;
    destructor Destroy; override;

    class function DetectCertReqFileFormat(FileName: string): TSBCertReqFileFormat; overload;
    class function DetectCertReqFileFormat(Stream: TStream): TSBCertReqFileFormat; overload;

    procedure LoadFromBuffer(Buffer : pointer; Size : integer);
    procedure LoadFromBufferPEM(Buffer : pointer; Size : integer);
    procedure LoadFromStream(Stream: TStream; Count : integer = 0);
    procedure LoadFromStreamPEM(Stream: TStream; Count : integer = 0);
    procedure Generate(PublicKeyAlgorithm : integer; Bits : integer;
      SignatureAlgorithm : integer);
    procedure SaveToStream(Stream: TStream);
    procedure SaveKeyToStream(Stream: TStream);
    function SaveToBuffer(Buffer : pointer; var Size : integer) : boolean;
    function SaveToBufferPEM(Buffer: pointer; var Size : integer) : boolean;
    procedure SaveToStreamPEM(Stream: TStream);
    procedure SaveKeyToStreamPEM(Stream: TStream; const Passphrase: string);
    function SaveKeyToBufferPEM(Buffer: pointer; var Size: integer;
      const Passphrase: string) : boolean;
    function SaveKeyToBufferPVK(Buffer: pointer; var Size : integer;
      const Password : string; UseStrongEncryption: boolean = true) : integer;
    function SaveKeyToStreamPVK(Stream : TStream; const Password : string;
      UseStrongEncryption: boolean = true) : integer;
    function ValidateSignature : boolean;
    function GetRSAParams(PublicModulus : pointer; var PublicModulusSize :
      integer; PublicExponent : pointer; var PublicExponentSize : integer) : boolean;
    function GetDSAParams(P : pointer; var PSize : integer; Q : pointer;
      var QSize : integer; G : pointer; var GSize : integer; Y : pointer;
      var YSize : integer) : boolean;

    function SaveKeyToBuffer(Buffer : pointer; var Size : integer) : boolean;
    function GetPrivateKey(Buffer : pointer; var Size : integer) : boolean;
    property Subject : TElRelativeDistinguishedName read FName;
    property Attributes : TElRelativeDistinguishedName read FAttributes;
    property Version : integer read FVersion write FVersion;
    property PublicKeyAlgorithm : integer read FAlgorithm;
    property SignatureAlgorithm : integer read FSignAlgorithm;
    property Extensions : TElCertificateExtensions read FExtensions;
  end;

  EElCertificateRequestException =  class(Exception);

implementation

uses
  SBX509, SBMSKeyBlob, SBRC4, SBPEM;

resourcestring

  SExceptionInvalidFormat = 'Invalid PKCS10 data';
  SExceptionInvalidSignature = 'Invalid signature';
  SExceptionInvalidAlgorithm = 'Invalid public key algorithm';
  SExceptionInvalidDSAKeyLength = 'DSA key length should be a multiple of 512';
  SExceptionInternalError = 'Internal Error';
  SExceptionInvalidPEMData = 'Invalid PEM data';

const
  STATE_STARTUP                         = 1;
  STATE_REQUEST_INFO                    = 2;
  STATE_VERSION                         = 3;
  STATE_SUBJECT_NAME                    = 4;
  STATE_SUBJECT_NAME_RDN                = 5;
  STATE_SUBJECT_NAME_RDN_SEQUENCE       = 6;
  STATE_SUBJECT_NAME_RDN_ATTR           = 7;
  STATE_SUBJECT_NAME_RDN_VALUE          = 8;
  STATE_SUBJECT_PK_INFO                 = 9;
  STATE_SUBJECT_PK_INFO_ALGORITHM       = 10;
  STATE_SUBJECT_PK_INFO_ALGORITHM_ID    = 11;
  STATE_SUBJECT_PK_INFO_ALGORITHM_PARAMS= 12;
  STATE_SUBJECT_PK_INFO_PUBLICKEY       = 13;
  STATE_ATTRIBUTES_A0                   = 14;
  STATE_ATTRIBUTES                      = 15;
  STATE_ATTRIBUTE                       = 16;
  STATE_ATTRIBUTE_VALUES                = 17;
  STATE_ATTRIBUTE_VALUE                 = 18;
  STATE_SIGNATURE_ALGORITHM             = 19;
  STATE_SIGNATURE_ALGORITHM_OID         = 20;
  STATE_SIGNATURE_ALGORITHM_PARAMS      = 21;
  STATE_SIGNATURE                       = 22;
  STATE_AUX_RSA_PUBLICKEY               = 101;
  STATE_AUX_RSA_PUBLICMODULUS           = 102;
  STATE_AUX_RSA_PUBLICEXPONENT          = 103;
  STATE_AUX_DSA_Y                       = 111;
  STATE_AUX_DSA_P                       = 112;
  STATE_AUX_DSA_Q                       = 113;
  STATE_AUX_DSA_G                       = 114;
  STATE_AUX_DSA_SIGNATURE               = 115;
  STATE_AUX_DSA_SIGNATURE_R             = 116;
  STATE_AUX_DSA_SIGNATURE_S             = 117;
  STATE_OVER                            = 255;
  STATE_ERROR                           = 254;

constructor TElCertificateRequest.Create(Owner: TSBComponentBase);
begin
  inherited Create(Owner);

  FName := TElRelativeDistinguishedName.Create;
  FAttributes := TElRelativeDistinguishedName.Create;
  FExtensions := TElCertificateExtensions.Create;
end;


destructor TElCertificateRequest.Destroy;
begin
  FreeAndNil(FExtensions);
  FName.Free;
  FAttributes.Free;
  inherited;
end;


procedure TElCertificateRequest.LoadFromBuffer(Buffer : pointer; Size : integer);
var
  FASN1Parser : TElASN1Parser;
begin
  CheckLicenseKey();
  ClearInfo;
  FASN1Parser := TElASN1Parser.Create;
  SetLength(FBuf, Size);
  Move(Buffer^, FBuf[0], Size);
  SetLength(FSource, Size);
  Move(Buffer^, FSource[0], Size);
  FASN1Parser.OnRead := HandleASN1Read;
  FASN1Parser.OnTag := HandleASN1Tag;
  FState := STATE_STARTUP;
  FNesting := 0;
  try
    FASN1Parser.Parse;
  except
    FASN1Parser.Free;
    raise EElCertificateRequestException.Create(SExceptionInvalidFormat);
    Exit;
  end;
  if FState = STATE_ERROR then
  begin
    FASN1Parser.Free;
    raise EElCertificateRequestException.Create(SExceptionInvalidFormat);
  end;
  FASN1Parser.Free;
  ProcessAttributes;
end;

procedure TElCertificateRequest.HandleASN1Read(Sender : TObject; Buffer : pointer; var Size : longint);
begin
  if Size > Length(FBuf) then
  begin
    Move(FBuf[0], Buffer^, Length(FBuf));
    Size := Length(FBuf);
    SetLength(FBuf, 0);
  end
  else
  begin
    Move(FBuf[0], Buffer^, Size);
    FBuf := Copy(FBuf, Size, Length(FBuf));
  end;
end;

procedure TElCertificateRequest.HandleASN1Tag(Sender : TObject; TagType: asn1TagType;
  TagConstrained: boolean; Tag: pointer; TagSize: integer; Size: integer; Data: pointer;
  BitRest: integer; var Valid : boolean);
var
  S : BufferType;
begin
  if not Assigned(Tag) then
  begin
    Dec(FNesting);
    Exit;
  end;
  if (TagConstrained) then
    Inc(FNesting);
  case FState of
    STATE_STARTUP :
    begin
      if (PByte(Tag)^ <> asn1Sequence) then
        FState := STATE_ERROR
      else
      begin
        FState := STATE_REQUEST_INFO;
        SetLength(FRequestInfo, Length(FBuf));
        Move(FBuf[0], FRequestInfo[0], Length(FRequestInfo));
      end;
    end;
    STATE_REQUEST_INFO :
    begin
      if (PByte(Tag)^ <> asn1Sequence) then
        FState := STATE_ERROR
      else
      begin
        FRequestInfoSz := Size;
        FState := STATE_VERSION;
      end;
    end;
    STATE_VERSION :
    begin
      if (PByte(Tag)^ <> asn1Integer) then
        FState := STATE_ERROR
      else
      begin
        FVersion := PByte(Data)^;
        FState := STATE_SUBJECT_NAME;
      end;
    end;
    STATE_SUBJECT_NAME :
    begin
      if (PByte(Tag)^ <> asn1Sequence) then
        FState := STATE_ERROR
      else
        FState := STATE_SUBJECT_NAME_RDN;
    end;
    STATE_SUBJECT_NAME_RDN :
    begin
      if (PByte(Tag)^ = asn1Set) then
        FState := STATE_SUBJECT_NAME_RDN_SEQUENCE
      else
      if (PByte(Tag)^ = asn1Sequence) then
      begin
        FState := STATE_SUBJECT_PK_INFO;
        HandleASN1Tag(Sender, TagType, TagConstrained, Tag, TagSize, Size,
          Data, BitRest, Valid);
      end
      else
        FState := STATE_ERROR
    end;
    STATE_SUBJECT_NAME_RDN_SEQUENCE :
    begin
      if (PByte(Tag)^ <> asn1Sequence) then
        FState := STATE_ERROR
      else
        FState := STATE_SUBJECT_NAME_RDN_ATTR;
    end;
    STATE_SUBJECT_NAME_RDN_ATTR :
    begin
      if (PByte(Tag)^ <> asn1Object) then
        FState := STATE_ERROR
      else
      begin
        FState := STATE_SUBJECT_NAME_RDN_VALUE;
        if (Assigned(Data)) then
        begin
          SetLength(FLastOID, Size);
          Move(Data^, FLastOID[1], Size);
        end
        else
          SetLength(FLastOID, 0);
      end;
    end;
    STATE_SUBJECT_NAME_RDN_VALUE :
    begin
      Valid := false;
      if Assigned(Data) then
      begin
        FName.Count := FName.Count + 1;
        SetLength(S, Size);
        Move(Data^, S[1], Size);
        FName.OIDs[FName.Count - 1] := CloneBuffer(FLastOID);
        FName.Values[FName.Count - 1] := S;
      end;
      FState := STATE_SUBJECT_NAME_RDN;
    end;
    STATE_SUBJECT_PK_INFO :
    begin
      if (PByte(Tag)^ <> asn1Sequence) then
        FState := STATE_ERROR
      else
        FState := STATE_SUBJECT_PK_INFO_ALGORITHM
    end;
    STATE_SUBJECT_PK_INFO_ALGORITHM :
    begin
      if (PByte(Tag)^ <> asn1Sequence) then
        FState := STATE_ERROR
      else
        FState := STATE_SUBJECT_PK_INFO_ALGORITHM_ID;
    end;
    STATE_SUBJECT_PK_INFO_ALGORITHM_ID :
    begin
      if (PByte(Tag)^ <> asn1Object) then
        FState := STATE_ERROR
      else
      begin
        if (Assigned(Data)) then
        begin
          SetLength(FPublicKeyAlgorithm, Size);
          Move(Data^, FPublicKeyAlgorithm[1], Size);
          if CompareContent(FPublicKeyAlgorithm, SB_CERT_OID_RSAENCRYPTION) then
            FAlgorithm := SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION
          else
          if CompareContent(FPublicKeyAlgorithm, SB_CERT_OID_DSA) then
            FAlgorithm := SB_CERT_ALGORITHM_ID_DSA;
        end
        else
          SetLength(FPublicKeyAlgorithm, 0);
        FState := STATE_SUBJECT_PK_INFO_ALGORITHM_PARAMS;
      end;
    end;
    STATE_SUBJECT_PK_INFO_ALGORITHM_PARAMS :
    begin
      if (PByte(Tag)^ <> asn1Null) then
      begin
        SetLength(FPublicKeyAlgorithmParams, Size);
        Move(FBuf[0], FPublicKeyAlgorithmParams[1], Size);
        if not ParseAlgorithmParams then
          FState := STATE_ERROR;
      end
      else
        SetLength(FPublicKeyAlgorithmParams, 0);

      Valid := false;
      FState := STATE_SUBJECT_PK_INFO_PUBLICKEY;
    end;
    STATE_SUBJECT_PK_INFO_PUBLICKEY :
    begin
      if (PByte(Tag)^ <> asn1BitStr) then
        FState := STATE_ERROR
      else
      begin
        FState := STATE_ATTRIBUTES_A0;
        if Assigned(Data) then
        begin
          SetLength(FPublicKey, Size);
          Move(Data^, FPublicKey[1], Size);
          if not ParsePublicKey then
            FState := STATE_ERROR;
        end;
      end;
    end;
    STATE_ATTRIBUTES_A0 :
    begin
      if (PByte(Tag)^ <> asn1A0) then
        FState := STATE_ERROR
      else
      begin
        FState := STATE_ATTRIBUTES;
        FOldNesting := FNesting + 1;
      end;
    end;
    STATE_ATTRIBUTES :
    begin
      if (PByte(Tag)^ <> asn1Sequence) then
        FState := STATE_ERROR
      else
      begin
        if (FOldNesting <> FNesting) then
        begin
          FState := STATE_SIGNATURE_ALGORITHM;
          HandleASN1Tag(Sender, TagType, TagConstrained, Tag, TagSize, Size,
            Data, BitRest, Valid);
        end
        else
          FState := STATE_ATTRIBUTE;
      end;
    end;
    STATE_ATTRIBUTE :
    begin
      if (PByte(Tag)^ <> asn1Object) then
        FState := STATE_ERROR
      else
      begin
        if Assigned(Data) then
        begin
          SetLength(FLastOID, Size);
          Move(Data^, FLastOID[1], Size);
          FState := STATE_ATTRIBUTE_VALUES;
        end
        else
          FState := STATE_ERROR;
      end;
    end;
    STATE_ATTRIBUTE_VALUES :
    begin
      if (PByte(Tag)^ = asn1Set) then
      begin
        SetLength(S, Size);
        Move(FBuf[0], S[1], Size);
        FAttributes.Count := FAttributes.Count + 1;
        FAttributes.OIDs[FAttributes.Count - 1] := FLastOID;
        FAttributes.Values[FAttributes.Count - 1] := S;
        FState := STATE_ATTRIBUTE_VALUE
      end
      else if (PByte(Tag)^ = asn1Sequence) then
      begin
        FState := STATE_SIGNATURE_ALGORITHM;
        HandleASN1Tag(Sender, TagType, TagConstrained, Tag, TagSize, Size,
          Data, BitRest, Valid);
      end
      else
        FState := STATE_ERROR;
    end;
    STATE_ATTRIBUTE_VALUE :
    begin
      if (TagConstrained) then
        Dec(FNesting);
      Valid := false;
      FState := STATE_ATTRIBUTES;
    end;
    STATE_SIGNATURE_ALGORITHM :
    begin
      if (PByte(Tag)^ <> asn1Sequence) then
        FState := STATE_ERROR
      else
        FState := STATE_SIGNATURE_ALGORITHM_OID;
    end;
    STATE_SIGNATURE_ALGORITHM_OID :
    begin
      if (PByte(Tag)^ <> asn1Object) then
        FState := STATE_ERROR
      else
      begin
        if (Assigned(Data)) then
        begin
          SetLength(FSignatureAlgorithm, Size);
          Move(Data^, FSignatureAlgorithm[1], Size);
          if CompareContent(FSignatureAlgorithm, SB_CERT_OID_MD2_RSAENCRYPTION) then
            FSignAlgorithm := SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION
          else
          if CompareContent(FSignatureAlgorithm, SB_CERT_OID_MD5_RSAENCRYPTION) then
            FSignAlgorithm := SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION
          else
          if CompareContent(FSignatureAlgorithm, SB_CERT_OID_SHA1_RSAENCRYPTION) then
            FSignAlgorithm := SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION
          else
          if CompareContent(FSignatureAlgorithm, SB_CERT_OID_SHA1_DSA) then
            FSignAlgorithm := SB_CERT_ALGORITHM_ID_DSA_SHA1;
        end;
        FState := STATE_SIGNATURE_ALGORITHM_PARAMS;
      end;
    end;
    STATE_SIGNATURE_ALGORITHM_PARAMS :
    begin
      Valid := false;
      FState := STATE_SIGNATURE;
    end;
    STATE_SIGNATURE :
    begin
      if (PByte(Tag)^ <> asn1BitStr) then
        FState := STATE_ERROR
      else
      begin
        if (Assigned(Data)) then
        begin
          SetLength(FSignature, Size);
          Move(Data^, FSignature[1], Size);
        end;
        FState := 0;
      end;
    end;
  end;
end;

procedure TElCertificateRequest.HandleASN1ReadAux(Sender : TObject; Buffer : pointer;
  var Size : longint);
begin
  if Size > Length(FBufAux) then
  begin
    Move(FBufAux[0], Buffer^, Length(FBufAux));
    Size := Length(FBufAux);
    SetLength(FBufAux, 0);
  end
  else
  begin
    Move(FBufAux[0], Buffer^, Size);
    FBufAux := Copy(FBufAux, Size, Length(FBufAux));
  end;
end;

procedure TElCertificateRequest.HandleASN1TagAux(Sender : TObject; TagType: asn1TagType;
  TagConstrained: boolean; Tag: pointer; TagSize: integer; Size: integer; Data: pointer;
  BitRest: integer; var Valid : boolean);
var
  I : integer;
begin
  if not Assigned(Tag) then
    Exit;
  case FStateAux of
    STATE_AUX_RSA_PUBLICKEY :
    begin
      if (PByte(Tag)^ <> asn1Sequence) then
        FStateAux := STATE_ERROR
      else
        FStateAux := STATE_AUX_RSA_PUBLICMODULUS;
    end;
    STATE_AUX_RSA_PUBLICMODULUS :
    begin
      if (PByte(Tag)^ <> asn1Integer) then
        FStateAux := STATE_ERROR
      else
      begin
        FStateAux := STATE_AUX_RSA_PUBLICEXPONENT;
        if Assigned(Data) then
        begin
          SetLength(FRSAPublicModulus, Size);
          for I := 1 to Size do
            FRSAPublicModulus[I] := Chr(PByteArray(Data)[Size - I]);
        end
        else
          FStateAux := STATE_ERROR;
      end;
    end;
    STATE_AUX_RSA_PUBLICEXPONENT :
    begin
      if (PByte(Tag)^ <> asn1Integer) then
        FStateAux := STATE_ERROR
      else
      begin
        FStateAux := STATE_OVER;
        if Assigned(Data) then
        begin
          SetLength(FRSAPublicExponent, Size);
          for I := 1 to Size do
            FRSAPublicExponent[I] := Chr(PByteArray(Data)[Size - I]);
        end
        else
          FStateAux := STATE_ERROR;
      end;
    end;
    STATE_AUX_DSA_Y :
    begin
      if (PByte(Tag)^ <> asn1Integer) then
        FStateAux := STATE_ERROR
      else
      begin
        if Assigned(Data) then
        begin
          SetLength(FDSAY, Size);
          for I := 1 to Size do
            FDSAY[I] := Chr(PByteArray(Data)[Size - I]);
        end
        else
          FStateAux := STATE_ERROR;
      end;
    end;
    STATE_AUX_DSA_P :
    begin
      if (PByte(Tag)^ <> asn1Integer) then
        FStateAux := STATE_ERROR
      else
      begin
        if Assigned(Data) then
        begin
          FStateAux := STATE_AUX_DSA_Q;
          SetLength(FDSAP, Size);
          for I := 1 to Size do
            FDSAP[I] := Chr(PByteArray(Data)[Size - I]);
        end
        else
          FStateAux := STATE_ERROR;
      end;
    end;
    STATE_AUX_DSA_Q :
    begin
      if (PByte(Tag)^ <> asn1Integer) then
        FStateAux := STATE_ERROR
      else
      begin
        if Assigned(Data) then
        begin
          FStateAux := STATE_AUX_DSA_G;
          SetLength(FDSAQ, Size);
          for I := 1 to Size do
            FDSAQ[I] := Chr(PByteArray(Data)[Size - I]);
        end
        else
          FStateAux := STATE_ERROR;
      end;
    end;
    STATE_AUX_DSA_G :
    begin
      if (PByte(Tag)^ <> asn1Integer) then
        FStateAux := STATE_ERROR
      else
      begin
        if Assigned(Data) then
        begin
          SetLength(FDSAG, Size);
          for I := 1 to Size do
            FDSAG[I] := Chr(PByteArray(Data)[Size - I]);
        end
        else
          FStateAux := STATE_ERROR;
      end;
    end;
    STATE_AUX_DSA_SIGNATURE :
    begin
      if (PByte(Tag)^ <> asn1Sequence) then
        FStateAux := STATE_ERROR
      else
        FStateAux := STATE_AUX_DSA_SIGNATURE_R;
    end;
    STATE_AUX_DSA_SIGNATURE_R :
    begin
      if (PByte(Tag)^ <> asn1Integer) then
        FStateAux := STATE_ERROR
      else
      begin
        if Assigned(Data) then
        begin
          SetLength(FDSAR, Size);
          for I := 1 to Size do
            FDSAR[I] := Chr(PByteArray(Data)[Size - I]);
          FStateAux := STATE_AUX_DSA_SIGNATURE_S;
        end
        else
          FStateAux := STATE_ERROR;
      end;
    end;
    STATE_AUX_DSA_SIGNATURE_S :
    begin
      if (PByte(Tag)^ <> asn1Integer) then
        FStateAux := STATE_ERROR
      else
      begin
        if Assigned(Data) then
        begin
          SetLength(FDSAS, Size);
          for I := 1 to Size do
            FDSAS[I] := Chr(PByteArray(Data)[Size - I]);
        end
        else
          FStateAux := STATE_ERROR;
      end;
    end;
  end;
end;

function TElCertificateRequest.ParsePublicKey : boolean;
begin
  if FAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    Result := ParsePublicKeyRSA
  else if FAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
    Result := ParsePublicKeyDSA
  else
    Result := false;
end;

function TElCertificateRequest.ParsePublicKeyRSA : boolean;
var
  Aux : TElASN1Parser;
begin
  Aux := TElASN1Parser.Create;
  Aux.OnRead := HandleASN1ReadAux;
  Aux.OnTag := HandleASN1TagAux;
  SetLength(FBufAux, Length(FPublicKey));
  Move(FPublicKey[1], FBufAux[0], Length(FBufAux));
  FStateAux := STATE_AUX_RSA_PUBLICKEY;
  try
    Aux.Parse;
  except
    Aux.Free;
    Result := false;
    Exit;
  end;
  
  if FStateAux = STATE_ERROR then
    Result := false
  else
    Result := true;
    
  Aux.Free;
end;

function TElCertificateRequest.ParsePublicKeyDSA : boolean;
var
  Aux : TElASN1Parser;
begin
  Aux := TElASN1Parser.Create;
  Aux.OnRead := HandleASN1ReadAux;
  Aux.OnTag := HandleASN1TagAux;
  SetLength(FBufAux, Length(FPublicKey));
  Move(FPublicKey[1], FBufAux[0], Length(FBufAux));
  FStateAux := STATE_AUX_DSA_Y;
  try
    Aux.Parse;
  except
    Aux.Free;
    Result := false;
    Exit;
  end;
  
  if FStateAux = STATE_ERROR then
    Result := false
  else
    Result := true;
    
  Aux.Free;
end;

function TElCertificateRequest.ParseAlgorithmParams : boolean;
var
  Aux : TElASN1Parser;
begin
  if FAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    Aux := TElASN1Parser.Create;
    Aux.OnRead := HandleASN1ReadAux;
    Aux.OnTag := HandleASN1TagAux;
    SetLength(FBufAux, Length(FPublicKeyAlgorithmParams));
    Move(FPublicKeyAlgorithmParams[1], FBufAux[0], Length(FBufAux));
    FStateAux := STATE_AUX_DSA_P;
    try
      Aux.Parse;
    except
      Aux.Free;
      Result := false;
      Exit;
    end;
    
    if FStateAux = STATE_ERROR then
      Result := false
    else
      Result := true;
      
    Aux.Free;
  end
  else
    Result := false;
end;

function TElCertificateRequest.ValidateSignature : boolean;
begin
  CheckLicenseKey();
  if FAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    Result := ValidateSignatureRSA
  else
  if FAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
    Result := ValidateSignatureDSA
  else
    Result := false;
end;

function TElCertificateRequest.ValidateSignatureRSA : boolean;
var
  X, E, N, Y : PLInt;
  DecryptedSignature, RealSignature : BufferType;
  Sz, I : integer;
begin
  LCreate(X);
  LCreate(E);
  LCreate(N);
  LCreate(Y);
  PointerToLInt(X, @FSignature[1], Length(FSignature));
  PointerToLInt(E, @FRSAPublicExponent[1], Length(FRSAPublicExponent));
  PointerToLInt(N, @FRSAPublicModulus[1], Length(FRSAPublicModulus));
  LMModPower(X, E, N, Y);

  Sz := Y.Length * 4;

  SetLength(DecryptedSignature, Sz);
  LIntToPointer(Y, @DecryptedSignature[1], Sz);
  LDestroy(X);
  LDestroy(E);
  LDestroy(N);
  LDestroy(Y);
  if (DecryptedSignature[1] <> #$00) or (DecryptedSignature[2] <> #$01) then
  begin
    Result := false;
    Exit;
  end;
  I := 3;
  while (I <= Length(DecryptedSignature)) and (DecryptedSignature[I] = #$FF) do
    Inc(I);
  if (I > Length(DecryptedSignature)) or (DecryptedSignature[I] <> #$0) then
  begin
    Result := false;
    Exit;
  end;
  RealSignature := CalculateSignature;
  if not CompareSignature(DecryptedSignature, RealSignature) then
    Result := false
  else
    Result := true;
end;

function TElCertificateRequest.CompareSignature(const Received : BufferType; const Real : BufferType) : boolean;
var Tmp : BufferType;
begin
  Result := false;
  if Length(Received) < Length(Real) then
    Exit;
  Tmp := Copy(Received, Length(Received) - Length(Real) + 1, Length(Real));
  if CompareStr(Tmp, Real) = 0 then
    Result := true;
end;

function TElCertificateRequest.ValidateSignatureDSA : boolean;
var
  Aux : TElASN1Parser;
  RealSignature : BufferType;
begin
  Result := False;
  Aux := TElASN1Parser.Create;
  try
    Aux.OnRead := HandleASN1ReadAux;
    Aux.OnTag := HandleASN1TagAux;
    SetLength(FBufAux, Length(FSignature));
    Move(FSignature[1], FBufAux[0], Length(FBufAux));
    FStateAux := STATE_AUX_DSA_SIGNATURE;

    try
      Aux.Parse;
    except
      Result := false;
      Exit;
    end;
  finally
    Aux.Free;
  end;

  if FStateAux = STATE_ERROR then
  begin
    Result := false;
    Exit;
  end;

  RealSignature := CalculateSignature;
  Result := SBDSA.ValidateSignature(@RealSignature[1], Length(RealSignature),
    @FDSAP[1], Length(FDSAP), @FDSAQ[1], Length(FDSAQ), @FDSAG[1],
    Length(FDSAG), @FDSAY[1], Length(FDSAY), @FDSAR[1], Length(FDSAR),
    @FDSAS[1], Length(FDSAS));
end;

function TElCertificateRequest.CalculateSignature : BufferType;
var
  Sz, I : integer;
  M160 : TMessageDigest160;
  M128 : TMessageDigest128;
  ofs  : integer;
begin
  // cutting the RequestInfo to needed size
  ofs := 1;
  if FRequestInfo[ofs] < 128 then
    Sz := FRequestInfo[ofs] + 2
  else
  if FRequestInfo[ofs] = $80 then
    Sz := FRequestInfoSz + 2
  else
  begin
    I := FRequestInfo[ofs] and not $80;
    Sz := FRequestInfoSz + 2 + I;
  end;
  SetLength(FRequestInfo, Sz);
  if (FSignAlgorithm = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION) or
    (FSignAlgorithm = SB_CERT_ALGORITHM_ID_DSA_SHA1) then
  begin
    M160 := HashSHA1(@FRequestInfo[0], Length(FRequestInfo));
    SetLength(Result, 20);
    Move(M160, Result[1], 20);
  end
  else
  if (FSignAlgorithm = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION) then
  begin
    M128 := HashMD2(@FRequestInfo[0], Length(FRequestInfo));
    SetLength(Result, 16);
    Move(M128, Result[1], 16);
  end
  else
  if (FSignAlgorithm = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION) then
  begin
    M128 := HashMD5(@FRequestInfo[0], Length(FRequestInfo));
    SetLength(Result, 16);
    Move(M128, Result[1], 16);
  end;
end;

procedure TElCertificateRequest.Generate(PublicKeyAlgorithm : integer; Bits : integer;
  SignatureAlgorithm : integer);
var
  S : BufferType;
  Tmp : BufferType;
  BlobTmp : ByteArray;
begin
  CheckLicenseKey();
  SetLength(FPrivateKeyBlob, 0);
  if (PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) and
    (SignatureAlgorithm <> SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION) and
    (SignatureAlgorithm <> SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION) and
    (SignatureAlgorithm <> SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION) then
    raise EElCertificateRequestException.Create(SExceptionInvalidAlgorithm)
  else if (PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA) and
    (SignatureAlgorithm <> SB_CERT_ALGORITHM_ID_DSA_SHA1) then
    raise EElCertificateRequestException.Create(SExceptionInvalidAlgorithm);
  // generating security parameters
  if PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    GenerateRSA(Bits)
  else if PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
    GenerateDSA(Bits)
  else
    raise EElCertificateRequestException.Create(SExceptionInvalidAlgorithm);
  FSignAlgorithm := SignatureAlgorithm;
  FAlgorithm := PublicKeyAlgorithm;
  // writing certificate request
  S := WriteCertificateRequest;
  SetLength(FSource, Length(S));
  Move(S[1], FSource[0], Length(FSource));
  Tmp := FPrivateKey;
  SetLength(BlobTmp, Length(FPrivateKeyBlob));
  Move(FPrivateKeyBlob[0], BlobTmp[0], Length(BlobTmp));
  LoadFromBuffer(@S[1], Length(S));
  SetLength(FPrivateKeyBlob, Length(BlobTmp));
  Move(BlobTmp[0], FPrivateKeyBlob[0], Length(FPrivateKeyBlob));
  FPrivateKey := Tmp;
end;

procedure TElCertificateRequest.SaveToStream(Stream: TStream);
var Buf : ByteArray;
    Sz  : integer;
begin
  Sz := 0;
  SaveToBuffer(nil, Sz);
  SetLength(Buf, Sz);
  SaveToBuffer(@Buf[0], Sz);
  Stream.WriteBuffer(Buf[0], Sz);
end;

procedure TElCertificateRequest.SaveKeyToStream(Stream: TStream);
var Buf : ByteArray;
    Sz  : integer;
begin
  Sz := 0;
  GetPrivateKey(nil, Sz);
  SetLength(Buf, Sz);
  GetPrivateKey(@Buf[0], Sz);
  Stream.WriteBuffer(Buf[0], Sz);
end;


function TElCertificateRequest.SaveToBuffer(Buffer : pointer; var Size : integer) : boolean;
begin
  CheckLicenseKey();
  if (Size < Length(FSource))  then
  begin
    Size := Length(FSource);
    Result := false;
  end
  else
  begin
    Size := Length(FSource);
    Move(FSource[0], Buffer^, Size);
    Result := true;
  end;
end;

function TElCertificateRequest.GetRSAParams(PublicModulus : pointer; var PublicModulusSize :
  integer; PublicExponent : pointer; var PublicExponentSize : integer) : boolean;
begin
  if ((PublicModulusSize < Length(FRSAPublicModulus)) or (PublicExponentSize < Length(FRSAPublicExponent))) then
    Result := false
  else
  begin
    Move(FRSAPublicModulus[1], PublicModulus^, Length(FRSAPublicModulus));
    Move(FRSAPublicExponent[1], PublicExponent^, Length(FRSAPublicExponent));
    Result := true;
  end;
  PublicModulusSize := Length(FRSAPublicModulus);
  PublicExponentSize := Length(FRSAPublicExponent);
end;

function TElCertificateRequest.GetDSAParams(P : pointer; var PSize : integer; Q : pointer;
  var QSize : integer; G : pointer; var GSize : integer; Y : pointer;
  var YSize : integer) : boolean;
begin
  if ((PSize < Length(FDSAP)) or (QSize < Length(FDSAQ)) or (GSize < Length(FDSAG)) or
    (YSize < Length(FDSAY))) then
    Result := false
  else
  begin
    Move(FDSAP[1], P^, Length(FDSAP));
    Move(FDSAQ[1], Q^, Length(FDSAQ));
    Move(FDSAG[1], G^, Length(FDSAG));
    Move(FDSAY[1], Y^, Length(FDSAY));
    Result := true;
  end;
  PSize := Length(FDSAP);
  QSize := Length(FDSAQ);
  GSize := Length(FDSAG);
  YSize := Length(FDSAY);
end;

procedure TElCertificateRequest.ClearInfo;
begin
  FName.Count := 0;
  FAttributes.Count := 0;
  FVersion := 0;
  SetLength(FPublicKeyAlgorithm, 0);
  SetLength(FPublicKeyAlgorithmParams, 0);
  SetLength(FPublicKey, 0);
  SetLength(FSignatureAlgorithm, 0);
  SetLength(FSignatureAlgorithmParams, 0);
  SetLength(FSignature, 0);
  SetLength(FRSAPublicModulus, 0);
  SetLength(FRSAPublicExponent, 0);
  SetLength(FDSAY, 0);
  SetLength(FDSAP, 0);
  SetLength(FDSAQ, 0);
  SetLength(FDSAG, 0);
  SetLength(FDSAS, 0);
  SetLength(FDSAR, 0);
  FAlgorithm := SB_CERT_ALGORITHM_UNKNOWN;
  FSignAlgorithm := SB_CERT_ALGORITHM_UNKNOWN;
  SetLength(FLastOID, 0);
  SetLength(FSource, 0);
  SetLength(FPrivateKey, 0);
  SetLength(FPrivateKeyBlob, 0);
  FExtensions.ClearExtensions;
end;

function TElCertificateRequest.WriteCertificateRequest : BufferType;
var
  Lst : TStringList;
begin
  Lst := TStringList.Create;
  Lst.Add(WriteCertificationRequestInfo);
  FSignature := Sign(Lst[0]);
  Lst.Add(WriteSignatureAlgorithm);
  Lst.Add(WriteSignature);
  Result := WriteSequence(Lst);
  Lst.Free;
end;

function TElCertificateRequest.WriteCertificationRequestInfo : BufferType;
var
  Lst : TStringList;
begin
  Lst := TStringList.Create;
  Lst.Add(WriteInteger(FVersion));
  Lst.Add(WriteName(FName));
  Lst.Add(WriteSubjectPublicKeyInfo);
  Lst.Add(WriteAttributes);
  Result := WriteSequence(Lst);
  Lst.Free;
end;

function TElCertificateRequest.WriteSignatureAlgorithm : BufferType;
var
  Lst : TStringList;
begin
  Lst := TStringList.Create;
  if FSignAlgorithm = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION then
    Lst.Add(WriteOID(SB_CERT_OID_MD2_RSAENCRYPTION))
  else if FSignAlgorithm = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION then
    Lst.Add(WriteOID(SB_CERT_OID_MD5_RSAENCRYPTION))
  else if FSignAlgorithm = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION then
    Lst.Add(WriteOID(SB_CERT_OID_SHA1_RSAENCRYPTION))
  else if FSignAlgorithm = SB_CERT_ALGORITHM_ID_DSA_SHA1 then
    Lst.Add(WriteOID(SB_CERT_OID_SHA1_DSA));
  Lst.Add(WriteNull);
  Result := WriteSequence(Lst);
  Lst.Free;
end;

function TElCertificateRequest.WriteSignature : BufferType;
begin
  Result := WriteBitString(FSignature);
end;

function TElCertificateRequest.WriteName(Name : TElRelativeDistinguishedName) : BufferType;
var
  OutFields : TStringList;
  Fields, Tmp : TStringList;
  I : integer;
begin
  OutFields := TStringList.Create;
  Tmp := TStringList.Create;
  Fields := TStringList.Create;
  Fields.Add('');
  Fields.Add('');
  Tmp.Add('');

  for I := 0 to Name.Count - 1 do
  begin
    Fields[0] := WriteOID(Name.OIDs[I]);
    if CompareContent(Name.OIDs[I], SB_CERT_OID_EMAIL) then
      Fields[1] := WriteIA5String(Name.Values[I])
    else
      Fields[1] := WritePrintableString(Name.Values[I]);
    Tmp[0] := WriteSequence(Fields);
    OutFields.Add(WriteSet(Tmp));
  end;

  Result := WriteSequence(OutFields);
  Fields.Free;
  OutFields.Free;
  Tmp.Free;
end;

function TElCertificateRequest.WriteSubjectPublicKeyInfo : BufferType;
var
  Lst, TmpLst, Params : TStringList;
begin
  Lst := TStringList.Create;
  TmpLst := TStringList.Create;
  if FAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
  begin
    TmpLst.Add(WriteOID(SB_CERT_OID_RSAENCRYPTION));
    TmpLst.Add(WriteNull);
    Lst.Add(WriteSequence(TmpLst));
    TmpLst.Clear;
    TmpLst.Add(WriteInteger(FRSAPublicModulus));
    TmpLst.Add(WriteInteger(FRSAPublicExponent));
    Lst.Add(WriteBitString(WriteSequence(TmpLst)));
  end
  else
  if FAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    TmpLst.Add(WriteOID(SB_CERT_OID_DSA));
    Params := TStringList.Create;
    Params.Add(WriteInteger(#0 + FDSAP));
    Params.Add(WriteInteger(#0 + FDSAQ));
    Params.Add(WriteInteger(#0 + FDSAG));
    TmpLst.Add(WriteSequence(Params));
    Lst.Add(WriteSequence(TmpLst));
    Params.Free;
    Lst.Add(WriteBitString(WriteInteger(#0 + FDSAY)));
  end;
  Result := WriteSequence(Lst);
  TmpLst.Free;
  Lst.Free;
end;

function TElCertificateRequest.WriteAttributes : BufferType;
var
  Lst, TmpLst, SetLst : TStringList;
  I : integer;

  procedure AddExtensionsAttribute;
  var
    Writer : TElExtensionWriter;
  begin
    Writer := TElExtensionWriter.Create(FExtensions, false);
    try
      TmpLst.Clear;
      SetLst.Clear;
      TmpLst.Add(WriteOID(SB_OID_CERT_EXTENSIONS));
      SetLst.Add(Writer.WriteExtensions);
      TmpLst.Add(WriteSet(SetLst));
      Lst.Add(WriteSequence(TmpLst));
    finally
      FreeAndNil(Writer);
    end;
  end;
  
begin
  Lst := TStringList.Create;
  TmpLst := TStringList.Create;
  SetLst := TStringList.Create;
  for I := 0 to FAttributes.Count - 1 do
  begin
    TmpLst.Clear;
    SetLst.Clear;
    TmpLst.Add(WriteOID(FAttributes.OIDs[I]));
    SetLst.Add(FAttributes.Values[I]);
    TmpLst.Add(WriteSet(SetLst));
    Lst.Add(WriteSequence(TmpLst));
  end;
  // writing extensions
  if FExtensions.Included <> [] then
  begin
    AddExtensionsAttribute;
  end;
  Result := WriteA0(Lst);
  SetLst.Free;
  TmpLst.Free;
  Lst.Free;
end;

procedure TElCertificateRequest.GenerateRSA(Bits : integer);
var
  MSize, ESize, DSize, BSize : integer;
begin
  MSize := 0; ESize := 0; DSize := 0; BSize := 0;
  SBRSA.Generate(Bits, nil, MSize, nil, ESize, nil, DSize, nil, BSize);
  SetLength(FRSAPublicModulus, MSize);
  SetLength(FRSAPublicExponent, ESize);
  SetLength(FPrivateKey, DSize);
  SetLength(FPrivateKeyBlob, BSize);
  if not SBRSA.Generate(Bits, @FRSAPublicModulus[1], MSize, @FRSAPublicExponent[1], ESize,
    @FPrivateKey[1], DSize, @FPrivateKeyBlob[0], BSize) then
    raise EElCertificateRequestException.Create(SExceptionInternalError);
  SetLength(FRSAPublicModulus, MSize);
  SetLength(FRSAPublicExponent, ESize);
  SetLength(FPrivateKey, DSize);
  SetLength(FPrivateKeyBlob, BSize);
end;

procedure TElCertificateRequest.GenerateDSA(Bits : integer);
var
  PSize, QSize, GSize, YSize, XSize, BSize : integer;
begin
  if (Bits mod 512) <> 0 then
    raise EElCertificateRequestException.Create(SExceptionInvalidDSAKeyLength);
  PSize := Bits shr 3 + 1;
  GSize := PSize;
  YSize := PSize;
  XSize := PSize;
  QSize := 21;
  SetLength(FDSAP, PSize);
  SetLength(FDSAQ, QSize);
  SetLength(FDSAG, GSize);
  SetLength(FDSAY, YSize);
  SetLength(FPrivateKey, XSize);
  BSize := PSize * 5 + 128;
  SetLength(FPrivateKeyBlob, BSize);
  if not SBDSA.Generate(Bits, @FDSAP[1], PSize, @FDSAQ[1], QSize, @FDSAG[1], GSize,
    @FDSAY[1], YSize, @FPrivateKey[1], XSize, @FPrivateKeyBlob[0], BSize) then
    raise EElCertificateRequestException.Create(SExceptionInternalError);
  SetLength(FDSAP, PSize);
  SetLength(FDSAQ, QSize);
  SetLength(FDSAG, GSize);
  SetLength(FDSAY, YSize);
  SetLength(FPrivateKey, XSize);
  SetLength(FPrivateKeyBlob, BSize);
end;


function TElCertificateRequest.SaveKeyToBuffer(Buffer : pointer; var Size : integer) : boolean;
begin
  result := GetPrivateKey(Buffer, Size);
end;


function TElCertificateRequest.GetPrivateKey(Buffer : pointer; var Size : integer) : boolean;
begin
  if (Size < Length(FPrivateKeyBlob))  then
  begin
    Size := Length(FPrivateKeyBlob);
    Result := false;
  end
  else
  begin
    Size := Length(FPrivateKeyBlob);
    Move(FPrivateKeyBlob[0], Buffer^, Size);
    Result := true;
  end;
end;

function TElCertificateRequest.Sign(const S : BufferType) : BufferType;
begin
  if FAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    Result := SignRSA(S)
  else
    Result := SignDSA(S);
end;

function TElCertificateRequest.SignRSA(const S : BufferType) : BufferType;
var
  M160 : TMessageDigest160;
  M128 : TMessageDigest128;
  Lst, TmpLst : TStringList;
  TmpS : BufferType;

  X, E, N, Y : PLInt;
  Sz : integer;
begin
  Lst := TStringList.Create;
  TmpLst := TStringList.Create;

  if FSignAlgorithm = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION then
  begin
    M160 := HashSHA1(S);
    SetLength(TmpS, 20);
    Move(M160, TmpS[1], 20);
    Lst.Add(WriteOID(SB_CERT_OID_SHA1));
  end
  else
  if FSignAlgorithm = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION then
  begin
    M128 := HashMD5(S);
    SetLength(TmpS, 16);
    Move(M128, TmpS[1], 16);
    Lst.Add(WriteOID(SB_CERT_OID_MD5));
  end
  else
  if FSignAlgorithm = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION then
  begin
    M128 := HashMD2(S);
    SetLength(TmpS, 16);
    Move(M128, TmpS[1], 16);
    Lst.Add(WriteOID(SB_CERT_OID_MD2));
  end;
  Lst.Add(WriteNull);
  TmpLst.Add(WriteSequence(Lst));
  TmpLst.Add(WriteOctetString(TmpS));
  TmpS := WriteSequence(TmpLst);
  Lst.Free;
  TmpLst.Free;
  TmpS := #$0 + TmpS;
  while Length(TmpS) < Length(FRSAPublicModulus) - 2 do
    TmpS := #$FF + TmpS;
  TmpS := #$00#$01 + TmpS;
  LCreate(X);
  LCreate(E);
  LCreate(N);
  LCreate(Y);
  PointerToLInt(X, @TmpS[1], Length(TmpS));
  PointerToLInt(E, @FPrivateKey[1], Length(FPrivateKey));
  PointerToLInt(N, @FRSAPublicModulus[1], Length(FRSAPublicModulus));
  LMModPower(X, E, N, Y);
  Sz := Y.Length * 4;
  SetLength(Result, Sz);
  LIntToPointer(Y, @Result[1], Sz);
  LDestroy(X);
  LDestroy(E);
  LDestroy(N);
  LDestroy(Y);
end;

function TElCertificateRequest.SignDSA(const S : BufferType) : BufferType;
var
  M160 : TMessageDigest160;

  SR, SS : BufferType;

  RSize, SSize : integer;
  Lst : TStringList;
begin
  M160 := HashSHA1(S);
  RSize := 20;
  SSize := 20;
  SetLength(SS, 20);
  SetLength(SR, 21);
  if not SBDSA.Sign(@M160, 20, @FDSAP[1], Length(FDSAP), @FDSAQ[1], Length(FDSAQ),
    @FDSAG[1], Length(FDSAG), @FPrivateKey[1], Length(FPrivateKey), @SR[2],
    RSize, @SS[1], SSize) then
    raise EElCertificateRequestException.Create(SExceptionInternalError);
  SR[1] := #0;
  Lst := TStringList.Create;
  Lst.Add(WriteInteger(SR));
  Lst.Add(WriteInteger(SS));
  Result := WriteSequence(Lst);
  Lst.Free;
end;

procedure TElCertificateRequest.LoadFromBufferPEM(Buffer : pointer; Size : integer);
var
  Header : string;
  OutSize : integer;
  Buf : ByteArray;
  PemResult : integer;
begin
  OutSize := 0;
  SBPEM.Decode(Buffer, Size, nil, '', OutSize, Header);
  SetLength(Buf, OutSize);
  PemResult := SBPEM.Decode(Buffer, Size, @Buf[0], '', OutSize, Header);
  if PemResult <> 0 then
    raise EElCertificateRequestException.Create(SExceptionInvalidPEMData);
  LoadFromBuffer(@Buf[0], OutSize);
end;


function TElCertificateRequest.SaveToBufferPEM(Buffer: pointer; var Size : integer) : boolean;
var
  Buf : ByteArray;
  Sz : integer;
begin
  Sz := 0;
  SaveToBuffer(nil, Sz);
  SetLength(Buf, Sz);
  SaveToBuffer(@Buf[0], Sz);
  Result := SBPEM.Encode(@Buf[0], Sz, Buffer, Size, 'NEW CERTIFICATE REQUEST', false, '');
end;


procedure TElCertificateRequest.LoadFromStream(Stream: TStream; Count : integer = 0);
var
  Buf : ByteArray;
begin
  if Count = 0 then
    Count := Stream.Size - Stream.Position;
  SetLength(Buf, Count);
  Stream.ReadBuffer(Buf[0], Length(Buf));
  LoadFromBuffer(@Buf[0], Length(Buf));
end;

procedure TElCertificateRequest.LoadFromStreamPEM(Stream: TStream; Count : integer = 0);
var
  Buf : ByteArray;
begin
  if Count = 0 then
    Count := Stream.Size - Stream.Position;
  SetLength(Buf, Count);
  Stream.ReadBuffer(Buf[0], Length(Buf));
  LoadFromBufferPEM(@Buf[0], Length(Buf));
end;

procedure TElCertificateRequest.SaveToStreamPEM(Stream: TStream);
var
  Buf : ByteArray;
  Size : integer;
begin
  Size := 0;
  SaveToBufferPEM(nil, Size);
  SetLength(Buf, Size);
  SaveToBufferPEM(@Buf[0], Size);
  Stream.WriteBuffer(Buf[0], Size);
end;

procedure TElCertificateRequest.SaveKeyToStreamPEM(Stream: TStream; const Passphrase: string);
var
  Buf : ByteArray;
  Size : integer;
begin
  Size := 0;
  SaveKeyToBufferPEM(nil, Size, Passphrase);
  SetLength(Buf, Size);
  SaveKeyToBufferPEM(@Buf[0], Size, Passphrase);
  Stream.WriteBuffer(Buf[0], Size);
end;


function TElCertificateRequest.SaveKeyToBufferPEM(Buffer: pointer; var Size:
  integer; const Passphrase: string): boolean;
var
  Buf : ByteArray;
  Sz : integer;
  Encrypt : boolean;
  Header : string;
begin
  Result := false;
  Sz := 0;
  SaveKeyToBuffer(nil, Sz);
  if Sz = 0 then
    Exit;
  SetLength(Buf, Sz);
  SaveKeyToBuffer(@Buf[0], Sz);
  SetLength(Buf, Sz);

  case FAlgorithm of
  SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION:
    Header := 'RSA PRIVATE KEY';
  SB_CERT_ALGORITHM_ID_DSA:
    Header := 'DSA PRIVATE KEY';
  else
    raise EElCertificateRequestException.Create(SExceptionInvalidAlgorithm);
  end;

  Encrypt := (Length(Passphrase) > 0);
  Result := SBPEM.Encode(@Buf[0], Sz, Buffer, Size, Header, Encrypt, Passphrase);
end;

function TElCertificateRequest.SaveKeyToStreamPVK(Stream : TStream;
  const Password : string; UseStrongEncryption: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}) : integer;
var
  Buf : ByteArray;
  Size : integer;
begin
  Size := 0;
  SaveKeyToBufferPVK(nil, Size, Password, UseStrongEncryption);
  SetLength(Buf, Size);
  Result := SaveKeyToBufferPVK(@Buf[0],
    Size, Password, UseStrongEncryption);
  if Result = 0 then
    Stream.WriteBuffer(Buf[0], Size);
end;



function TElCertificateRequest.SaveKeyToBufferPVK(Buffer: pointer; var Size : integer;
  const Password : string; UseStrongEncryption: boolean = true) : integer;
var
  Blob, MSBlob : ByteArray;
  BlobSize: integer;
  BlobType: byte;
  MSBlobSize : integer;
  Salt, Key : string;
  PVKHeader: TPVKHeader;

  function GenerateSalt : string;
  begin
    SetLength(Result, 16);
    SBRndGenerate(@Result[1], Length(Result));
  end;

  procedure EncryptBlob(const Key : string;
    Buffer: pointer; Size: integer);
  var
    Context : TRC4Context;
  begin
    SBRC4.Initialize(Context, TRC4Key(BytesOfString(Key)));
    SBRC4.Encrypt(Context, Buffer, Buffer, Size);
  end;
begin
  BlobSize := 0;
  SaveKeyToBuffer(nil, BlobSize);
  if (BlobSize = 0) then
  begin
    Result := SB_X509_ERROR_NO_PRIVATE_KEY;
    Exit;
  end;
  SetLength(Blob, BlobSize);
  if not SaveKeyToBuffer(@Blob[0], BlobSize) then
  begin
    Result := SB_X509_ERROR_NO_PRIVATE_KEY;
    Exit;
  end;
  SetLength(Blob, BlobSize);
  if PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    BlobType := SB_KEY_BLOB_RSA
  else if PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
    BlobType := SB_KEY_BLOB_DSS
  else
  begin
    Result := SB_X509_ERROR_UNSUPPORTED_ALGORITHM;
    Exit;
  end;

  MSBlobSize := 0;
  SBMSKeyBlob.WriteMSKeyBlob(@Blob[0], BlobSize, nil, MSBlobSize, BlobType);
  SetLength(MSBlob, MSBlobSize);
  if not SBMSKeyBlob.WriteMSKeyBlob(@Blob[0], BlobSize, @MSBlob[0], MSBlobSize,
    BlobType) then
  begin
    Result := SB_X509_ERROR_INVALID_PRIVATE_KEY;
    Exit;
  end;

  if MSBlobSize < 8 then
  begin
    Result := SB_X509_ERROR_INTERNAL_ERROR;
    Exit;
  end;
  
  if Length(Password) > 0 then
  begin
    if Size < MSBlobSize + 16 + SizeOf(TPVKHeader) then
    begin
      Result := SB_X509_ERROR_BUFFER_TOO_SMALL;
      Size := MSBlobSize + 16 + SizeOf(TPVKHeader);
      Exit; 
    end;

    Salt := GenerateSalt;
    Key := PVK_DeriveKey((Password), Salt, not UseStrongEncryption);
    EncryptBlob(Key, @MSBlob[8], MSBlobSize - 8);
    PVKHeader.encrypted := 1;
    PVKHeader.saltlen := 16;
  end
  else
  begin
    if Size < MSBlobSize + SizeOf(TPVKHeader) then
    begin
      Result := SB_X509_ERROR_BUFFER_TOO_SMALL;
      Size := MSBlobSize + SizeOf(TPVKHeader);
      Exit;
    end;

    PVKHeader.encrypted := 0;
    PVKHeader.saltlen := 0;
  end;

  PVKHeader.magic := $b0b5f11e;
  PVKHeader.reserved := 0;
  if BlobType = SB_KEY_BLOB_RSA then
    PVKHeader.keytype := 1
  else
    PVKHeader.keytype := 2;
  
  PVKHeader.keylen := MSBlobSize;
  Move(PVKHeader, Buffer^, SizeOf(PVKHeader));
  Move(Salt[1], PByteArray(Buffer)[SizeOf(PVKHeader)], PVKHeader.saltlen);
  Move(MSBlob[0], PByteArray(Buffer)[SizeOf(PVKHeader) + PVKHeader.saltlen],
    MSBlobSize);
  Size := SizeOf(TPVKHeader) + integer(PVKHeader.saltlen) + MSBlobSize;
  Result := 0;
end;

procedure TElCertificateRequest.ProcessAttributes;
var
  I, K : integer;
  Tag, Sequence, ExtSequence : TElASN1ConstrainedTag;
  Data : BufferType;
  Index : integer;
  OID, Value : BufferType;
  Critical : boolean;
  Reader : TElExtensionReader;
begin
  // extracting requested extensions attribute
  for I := 0 to FAttributes.Count - 1 do
  begin
    if CompareContent(FAttributes.OIDs[I], SB_OID_CERT_EXTENSIONS) then
    begin
      Tag := TElASN1ConstrainedTag.Create;
      Reader := TElExtensionReader.Create(FExtensions, false);
      try
        Data := FAttributes.Values[I];
        if Tag.LoadFromBuffer(@Data[1], Length(Data)) then
        begin
          if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
          begin
            Sequence := TElASN1ConstrainedTag(Tag.GetField(0));
            for K := 0 to Sequence.Count - 1 do
            begin
              if (Sequence.GetField(K).CheckType(SB_ASN1_SEQUENCE, true)) then
              begin
                ExtSequence := TElASN1ConstrainedTag(Sequence.GetField(K));
                Index := 0;
                if (Index < ExtSequence.Count) and (ExtSequence.GetField(Index).CheckType(SB_ASN1_OBJECT, false)) then
                begin
                  OID := TElASN1SimpleTag(ExtSequence.GetField(Index)).Content;
                  Inc(Index);
                  if (Index < ExtSequence.Count) and (ExtSequence.GetField(Index).CheckType(SB_ASN1_BOOLEAN, false)) then
                  begin
                    Critical := TElASN1SimpleTag(ExtSequence.GetField(Index)).Content = #$FF;
                    Inc(Index);
                  end
                  else
                    Critical := false;
                  if (Index < ExtSequence.Count) and (ExtSequence.GetField(Index).CheckType(SB_ASN1_OCTETSTRING, false)) then
                  begin
                    Value := TElASN1SimpleTag(ExtSequence.GetField(Index)).Content;
                    Reader.ParseExtension(OID, Critical, Value);
                  end;
                end
              end;
            end;
          end;
        end;
      finally
        FreeAndNil(Tag);
        FreeAndNil(Reader);
      end;
    end;
  end;
end;

class function TElCertificateRequest.DetectCertReqFileFormat(FileName: string): TSBCertReqFileFormat;
var
  Stream: TFileStream;
begin
  Result := crfUnknown;
  try
    Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
      result := DetectCertReqFileFormat(Stream);
    finally
      Stream.Free;
    end;
  except
  end;
end;

class function TElCertificateRequest.DetectCertReqFileFormat(Stream: TStream): TSBCertReqFileFormat;
var
  TmpStream: TMemoryStream;
  Req: TElCertificateRequest;
  SavePos: integer;
begin
  Result := crfUnknown;
  TmpStream := TMemoryStream.Create;
  try
    SavePos := Stream.Position;
    TmpStream.CopyFrom(Stream, Stream.Size - Stream.Position);
    TmpStream.Position := 0;
    Stream.Position := SavePos;
    Req := TElCertificateRequest.Create(nil);
    try

      // try DER
      try
        Req.LoadFromStream(TmpStream);
        result := crfDER;
        exit;
      except
      end;

      // try PEM
      TmpStream.Position := 0;
      try
        Req.LoadFromStreamPEM(TmpStream);
        result := crfPEM;
        exit;
      except
      end;
    finally
      FreeAndNil(Req);
    end;
  finally
    TmpStream.Free;
  end;
end;

end.
