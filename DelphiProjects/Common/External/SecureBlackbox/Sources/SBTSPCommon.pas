(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBTSPCommon;

interface

uses

  SysUtils,
  Classes,
  SBUtils,
  SBASN1,
  SBASN1Tree,
//  SBX509,
  SBX509Ext,
//  SBPKICommon,
//  SBCustomCertStorage,
  SBPKCS7;


const

  ERROR_FACILITY_TSP = $14000;

  ERROR_TSP_PROTOCOL_ERROR_FLAG = $00800;

  SB_TSP_ERROR_ABORTED  = ERROR_FACILITY_TSP + ERROR_TSP_PROTOCOL_ERROR_FLAG + 1;
  SB_TSP_ERROR_NO_REPLY = ERROR_FACILITY_TSP + ERROR_TSP_PROTOCOL_ERROR_FLAG + 2;
  SB_TSP_ERROR_NO_PARAMETERS = ERROR_FACILITY_TSP + ERROR_TSP_PROTOCOL_ERROR_FLAG + 3;
  SB_TSP_ERROR_NO_CERTIFICATES = ERROR_FACILITY_TSP + ERROR_TSP_PROTOCOL_ERROR_FLAG + 4;
  SB_TSP_ERROR_WRONG_DATA = ERROR_FACILITY_TSP + ERROR_TSP_PROTOCOL_ERROR_FLAG + 5;
  SB_TSP_ERROR_WRONG_IMPRINT = ERROR_FACILITY_TSP + ERROR_TSP_PROTOCOL_ERROR_FLAG + 6;
  SB_TSP_ERROR_WRONG_NONCE = ERROR_FACILITY_TSP + ERROR_TSP_PROTOCOL_ERROR_FLAG + 7;
  SB_TSP_ERROR_UNEXPECTED_CERTIFICATES = ERROR_FACILITY_TSP + ERROR_TSP_PROTOCOL_ERROR_FLAG + 8;

  tfiBadAlg = SmallInt(0);
  tfiBadRequest = SmallInt(2);
  tfiBadDataFormat = SmallInt(5);
  tfiTimeNotAvailable = SmallInt(14);
  tfiUnacceptedPolicy = SmallInt(15);
  tfiUnacceptedExtension = SmallInt(16);
  tfiAddInfoNotAvailable = SmallInt(17);
  tfiSystemFailure = SmallInt(25);

type

  TSBTSPFailureInfo =  SmallInt;

  TElTSPInfo = class(TPersistent)
  protected
    FNonce: BufferType;
    FSerialNumber: ByteArray;
    FTime: TDateTime;
    FAccuracySet: Boolean;
    FAccuracySec: integer;
    FAccuracyMilli: integer;
    FAccuracyMicro: integer;
    FTSAName: TElGeneralName;
    FTSANameSet: Boolean;


  public
    constructor Create; virtual;
    destructor Destroy; override;

    procedure Assign(Source : TPersistent); override;
    property SerialNumber: ByteArray read FSerialNumber write FSerialNumber;

    property Nonce: BufferType read FNonce; 
    property Time: TDateTime read FTime; 
    property AccuracySec: integer read FAccuracySec; 
    property AccuracyMilli: integer read FAccuracyMilli; 
    property AccuracyMicro: integer read FAccuracyMicro; 
    property AccuracySet: Boolean read FAccuracySet write FAccuracySet;  
    property TSAName: TElGeneralName read FTSAName; 
    property TSANameSet: Boolean read FTSANameSet write FTSANameSet;  
  end;

  TElTSPClass = class(TSBControlBase)
  public
    constructor Create(Owner: TSBComponentBase); override;

    destructor Destroy; override;
    function ValidateImprint(Algorithm: Integer; HashedData, Imprint: ByteArray):
        Boolean;
  end;

implementation

resourcestring
  SInvalidObjectType = 'Invalid object type';

constructor TElTSPClass.Create(Owner: TSBComponentBase);
begin
  inherited Create(Owner);
end;


destructor TElTSPClass.Destroy;
begin
  inherited;
end;


function TElTSPClass.ValidateImprint(Algorithm: Integer; HashedData, Imprint:
    ByteArray): Boolean;
var
  ASN, Seq, Field: TElASN1ConstrainedTag;
  Param: TElASN1SimpleTag;
  OID  : BufferType;
begin
  result := false;
  ASN := TElASN1ConstrainedTag.Create;
  try
    try
      if not ASN.LoadFromBuffer(@Imprint[0], Length(Imprint)) then
        exit;
    except
      exit;
    end;
    if ASN.Count <> 1 then
      exit;
    Field := TElASN1ConstrainedTag(ASN.GetField(0));
    if (Field = nil) or not Field.IsConstrained then
      exit;
    if Field.Count < 2 then
      exit;

    // Read AlgorithmIdentifier
    Seq := TElASN1ConstrainedTag(Field.GetField(0));
    if (Seq = nil) or (not Seq.IsConstrained) then
      exit;
    if (Seq.Count <> 1) and (Seq.Count <> 2) then
      exit;
    Param := TElASN1SimpleTag(Seq.GetField(0));
    if Param.TagId <> SB_ASN1_OBJECT then
      exit;
    OID := GetOIDByHashAlgorithm(Algorithm);
    if (Length(OID) = 0) or
    (not CompareMem(@Param.Content[1], @OID[1], Length(OID)))
     then
      exit;

    // read HashedData
    Param := TElASN1SimpleTag(Field.GetField(1));
    if Param.TagId <> SB_ASN1_OCTETSTRING then exit;
    if (Length(Param.Content) <> Length(HashedData)) or (not CompareMem(HashedData, Param.Content)) then
      exit; 
  finally
    ASN.Free;
  end;
  result := true;
end;

constructor TElTSPInfo.Create;
begin
  inherited;
  FTSAName := TElGeneralName.Create;
  SetLength(FNonce, 8);
  SBRndGenerate(@FNonce[1], Length(FNonce));
end;

destructor TElTSPInfo.Destroy;
begin
  FreeAndNil(FTSAName);
  inherited;
end;

procedure TElTSPInfo.Assign(Source : TPersistent);
begin
  inherited;
  if not (Source is TElTSPInfo) then
    raise EConvertError.Create(SInvalidObjectType);
  FNonce := TElTSPInfo(Source).FNonce;
  FSerialNumber := TElTSPInfo(Source).FSerialNumber;
  FTime := TElTSPInfo(Source).FTime; 
  FAccuracySet := TElTSPInfo(Source).FAccuracySet;
  FAccuracySec := TElTSPInfo(Source).FAccuracySec;
  FAccuracyMilli := TElTSPInfo(Source).FAccuracyMilli;
  FAccuracyMicro := TElTSPInfo(Source).FAccuracyMicro;
  FTSAName.Assign(TElTSPInfo(Source).FTSAName);
  FTSANameSet := TElTSPInfo(Source).FTSANameSet;
end;

end.



