
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSSH3DES;

interface

uses
  SBDES
  ;

type

  TSSH3DESContext =  record
    Context1 : TDESContext;
    Context2 : TDESContext;
    Context3 : TDESContext;
  end;
  
  TSSH3DESKey =  array[0..23] of Byte;

procedure InitializeEncryptionCBC(var Context : TSSH3DESContext;
  Key : TSSH3DESKey; IV : TSSH3DESKey);
procedure InitializeDecryptionCBC(var Context : TSSH3DESContext;
  Key : TSSH3DESKey; IV : TSSH3DESKey);

function EncryptCBC(var Context : TSSH3DESContext; InBuffer : pointer; InSize : cardinal;
  OutBuffer : pointer; OutSize : cardinal) : boolean;
function DecryptCBC(var Context : TSSH3DESContext; InBuffer : pointer; InSize : cardinal;
  OutBuffer : pointer; OutSize : cardinal) : boolean;

implementation


procedure InitializeEncryptionCBC(var Context : TSSH3DESContext;
  Key : TSSH3DESKey; IV : TSSH3DESKey);
var
  DesKey : TDESKey;
  DesIV : TDESBuffer;
begin
  Move(Key[0], DesKey[0], 8);
  Move(IV[0], DesIV[0], 8);
  SBDES.InitializeEncryptionCBC(Context.Context1, DesKey, DesIV);
  Move(Key[8], DesKey[0], 8);
  Move(IV[8], DesIV[0], 8);
  SBDES.InitializeDecryptionCBC(Context.Context2, DesKey, DesIV);
  Move(Key[16], DesKey[0], 8);
  Move(IV[16], DesIV[0], 8);
  SBDES.InitializeEncryptionCBC(Context.Context3, DesKey, DesIV);
end;

procedure InitializeDecryptionCBC(var Context : TSSH3DESContext;
  Key : TSSH3DESKey; IV : TSSH3DESKey);
var
  DesKey : TDESKey;
  DesIV : TDESBuffer;
begin
  Move(Key[16], DesKey[0], 8);
  Move(IV[16], DesIV[0], 8);
  SBDES.InitializeDecryptionCBC(Context.Context3, DesKey, DesIV);
  Move(Key[8], DesKey[0], 8);
  Move(IV[8], DesIV[0], 8);
  SBDES.InitializeEncryptionCBC(Context.Context2, DesKey, DesIV);
  Move(Key[0], DesKey[0], 8);
  Move(IV[0], DesIV[0], 8);
  SBDES.InitializeDecryptionCBC(Context.Context1, DesKey, DesIV);
end;

function EncryptCBC(var Context : TSSH3DESContext; InBuffer : pointer; InSize : cardinal;
  OutBuffer : pointer; OutSize : cardinal) : boolean;
begin
  Result := true;
  OutSize := InSize;
  if not SBDES.EncryptCBC(Context.Context1, InBuffer, InSize, OutBuffer, OutSize) then
  begin
    Result := false;
    Exit;
  end;
  if not SBDES.DecryptCBC(Context.Context2, OutBuffer, InSize, InBuffer, OutSize) then
  begin
    Result := false;
    Exit;
  end;
  if not SBDES.EncryptCBC(Context.Context3, InBuffer, InSize, OutBuffer, OutSize) then
  begin
    Result := false;
    Exit;
  end;
end;

function DecryptCBC(var Context : TSSH3DESContext; InBuffer : pointer; InSize : cardinal;
  OutBuffer : pointer; OutSize : cardinal) : boolean;
var
  TmpBuf : array of byte;
begin
  Result := true;
  OutSize := InSize;
  SetLength(TmpBuf, InSize);
  if not SBDES.DecryptCBC(Context.Context3, InBuffer, InSize, @TmpBuf[0], OutSize) then
  begin
    Result := false;
    Exit;
  end;
  if not SBDES.EncryptCBC(Context.Context2, @TmpBuf[0], InSize, OutBuffer, OutSize) then
  begin
    Result := false;
    Exit;
  end;
  if not SBDES.DecryptCBC(Context.Context1, OutBuffer, InSize, @TmpBuf[0], OutSize) then
  begin
    Result := false;
    Exit;
  end;
  Move(TmpBuf[0], OutBuffer^, OutSize);
end;


end.
