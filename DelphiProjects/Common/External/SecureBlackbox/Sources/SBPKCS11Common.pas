(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBPKCS11Common;

interface

uses
  SBUtils;

const

  PKCS11_FUNC_FIRST = 0;
  PKCS11_Initialize = 0;
  PKCS11_Finalize = 1;
  PKCS11_GetInfo = 2;
  PKCS11_GetFunctionList = 3;
  PKCS11_GetSlotList = 4;
  PKCS11_GetSlotInfo = 5;
  PKCS11_GetTokenInfo = 6;
  PKCS11_GetMechanismList = 7;
  PKCS11_GetMechanismInfo = 8;
  PKCS11_InitToken = 9;
  PKCS11_InitPIN = 10;
  PKCS11_SetPIN = 11;
  PKCS11_OpenSession = 12;
  PKCS11_CloseSession = 13;
  PKCS11_CloseAllSessions = 14;
  PKCS11_GetSessionInfo = 15;
  PKCS11_GetOperationState = 16;
  PKCS11_SetOperationState = 17;
  PKCS11_Login = 18;
  PKCS11_Logout = 19;
  PKCS11_CreateObject = 20;
  PKCS11_CopyObject = 21;
  PKCS11_DestroyObject = 22;
  PKCS11_GetObjectSize = 23;
  PKCS11_GetAttributeValue = 24;
  PKCS11_SetAttributeValue = 25;
  PKCS11_FindObjectsInit = 26;
  PKCS11_FindObjects = 27;
  PKCS11_FindObjectsFinal = 28;
  PKCS11_EncryptInit = 29;
  PKCS11_Encrypt = 30;
  PKCS11_EncryptUpdate = 31;
  PKCS11_EncryptFinal = 32;
  PKCS11_DecryptInit = 33;
  PKCS11_Decrypt = 34;
  PKCS11_DecryptUpdate = 35;
  PKCS11_DecryptFinal = 36;
  PKCS11_DigestInit = 37;
  PKCS11_Digest = 38;
  PKCS11_DigestUpdate = 39;
  PKCS11_DigestKey = 40;
  PKCS11_DigestFinal = 41;
  PKCS11_SignInit = 42;
  PKCS11_Sign = 43;
  PKCS11_SignUpdate = 44;
  PKCS11_SignFinal = 45;
  PKCS11_SignRecoverInit = 46;
  PKCS11_SignRecover = 47;
  PKCS11_VerifyInit = 48;
  PKCS11_Verify = 49;
  PKCS11_VerifyUpdate = 50;
  PKCS11_VerifyFinal = 51;
  PKCS11_VerifyRecoverInit = 52;
  PKCS11_VerifyRecover = 53;
  PKCS11_DigestEncryptUpdate = 54;
  PKCS11_DecryptDigestUpdate = 55;
  PKCS11_SignEncryptUpdate = 56;
  PKCS11_DecryptVerifyUpdate = 57;
  PKCS11_GenerateKey = 58;
  PKCS11_GenerateKeyPair = 59;
  PKCS11_WrapKey = 60;
  PKCS11_UnwrapKey = 61;
  PKCS11_DeriveKey = 62;
  PKCS11_SeedRandom = 63;
  PKCS11_GenerateRandom = 64;
  PKCS11_GetFunctionStatus = 65;
  PKCS11_CancelFunction = 66;
  PKCS11_WaitForSlotEvent = 67;
  PKCS11_FUNC_LAST = 67;

  PKCS11_FuncNames : array [PKCS11_FUNC_FIRST..PKCS11_FUNC_LAST] of string =
  (
   'C_Initialize', 'C_Finalize', 'C_GetInfo', 'C_GetFunctionList', 'C_GetSlotList',
   'C_GetSlotInfo', 'C_GetTokenInfo', 'C_GetMechanismList', 'C_GetMechanismInfo',
   'C_InitToken', 'C_InitPIN', 'C_SetPIN', 'C_OpenSession', 'C_CloseSession',
   'C_CloseAllSessions', 'C_GetSessionInfo', 'C_GetOperationState', 'C_SetOperationState',
   'C_Login', 'C_Logout', 'C_CreateObject', 'C_CopyObject', 'C_DestroyObject',
   'C_GetObjectSize', 'C_GetAttributeValue', 'C_SetAttributeValue', 'C_FindObjectsInit',
   'C_FindObjects', 'C_FindObjectsFinal', 'C_EncryptInit', 'C_Encrypt', 'C_EncryptUpdate',
   'C_EncryptFinal', 'C_DecryptInit', 'C_Decrypt', 'C_DecryptUpdate', 'C_DecryptFinal',
   'C_DigestInit', 'C_Digest', 'C_DigestUpdate', 'C_DigestKey', 'C_DigestFinal',
   'C_SignInit', 'C_Sign', 'C_SignUpdate', 'C_SignFinal', 'C_SignRecoverInit',
   'C_SignRecover', 'C_VerifyInit', 'C_Verify', 'C_VerifyUpdate', 'C_VerifyFinal',
   'C_VerifyRecoverInit', 'C_VerifyRecover', 'C_DigestEncryptUpdate', 'C_DecryptDigestUpdate',
   'C_SignEncryptUpdate', 'C_DecryptVerifyUpdate', 'C_GenerateKey', 'C_GenerateKeyPair',
   'C_WrapKey', 'C_UnwrapKey', 'C_DeriveKey', 'C_SeedRandom', 'C_GenerateRandom',
   'C_GetFunctionStatus', 'C_CancelFunction', 'C_WaitForSlotEvent'
  );


  CKR_OK                                = $00000000;
  CKR_CANCEL                            = $00000001;
  CKR_HOST_MEMORY                       = $00000002;
  CKR_SLOT_ID_INVALID                   = $00000003;
  CKR_GENERAL_ERROR                     = $00000005;
  CKR_FUNCTION_FAILED                   = $00000006;
  CKR_ARGUMENTS_BAD                     = $00000007;
  CKR_NO_EVENT                          = $00000008;
  CKR_NEED_TO_CREATE_THREADS            = $00000009;
  CKR_CANT_LOCK                         = $0000000A;
  CKR_ATTRIBUTE_READ_ONLY               = $00000010;
  CKR_ATTRIBUTE_SENSITIVE               = $00000011;
  CKR_ATTRIBUTE_TYPE_INVALID            = $00000012;
  CKR_ATTRIBUTE_VALUE_INVALID           = $00000013;
  CKR_DATA_INVALID                      = $00000020;
  CKR_DATA_LEN_RANGE                    = $00000021;
  CKR_DEVICE_ERROR                      = $00000030;
  CKR_DEVICE_MEMORY                     = $00000031;
  CKR_DEVICE_REMOVED                    = $00000032;
  CKR_ENCRYPTED_DATA_INVALID            = $00000040;
  CKR_ENCRYPTED_DATA_LEN_RANGE          = $00000041;
  CKR_FUNCTION_CANCELED                 = $00000050;
  CKR_FUNCTION_NOT_PARALLEL             = $00000051;
  CKR_FUNCTION_NOT_SUPPORTED            = $00000054;
  CKR_KEY_HANDLE_INVALID                = $00000060;
  CKR_KEY_SIZE_RANGE                    = $00000062;
  CKR_KEY_TYPE_INCONSISTENT             = $00000063;
  CKR_KEY_NOT_NEEDED                    = $00000064;
  CKR_KEY_CHANGED                       = $00000065;
  CKR_KEY_NEEDED                        = $00000066;
  CKR_KEY_INDIGESTIBLE                  = $00000067;
  CKR_KEY_FUNCTION_NOT_PERMITTED        = $00000068;
  CKR_KEY_NOT_WRAPPABLE                 = $00000069;
  CKR_KEY_UNEXTRACTABLE                 = $0000006A;
  CKR_MECHANISM_INVALID                 = $00000070;
  CKR_MECHANISM_PARAM_INVALID           = $00000071;
  CKR_OBJECT_HANDLE_INVALID             = $00000082;
  CKR_OPERATION_ACTIVE                  = $00000090;
  CKR_OPERATION_NOT_INITIALIZED         = $00000091;
  CKR_PIN_INCORRECT                     = $000000A0;
  CKR_PIN_INVALID                       = $000000A1;
  CKR_PIN_LEN_RANGE                     = $000000A2;
  CKR_PIN_EXPIRED                       = $000000A3;
  CKR_PIN_LOCKED                        = $000000A4;
  CKR_SESSION_CLOSED                    = $000000B0;
  CKR_SESSION_COUNT                     = $000000B1;
  CKR_SESSION_HANDLE_INVALID            = $000000B3;
  CKR_SESSION_PARALLEL_NOT_SUPPORTED    = $000000B4;
  CKR_SESSION_READ_ONLY                 = $000000B5;
  CKR_SESSION_EXISTS                    = $000000B6;
  CKR_SESSION_READ_ONLY_EXISTS          = $000000B7;
  CKR_SESSION_READ_WRITE_SO_EXISTS      = $000000B8;
  CKR_SIGNATURE_INVALID                 = $000000C0;
  CKR_SIGNATURE_LEN_RANGE               = $000000C1;
  CKR_TEMPLATE_INCOMPLETE               = $000000D0;
  CKR_TEMPLATE_INCONSISTENT             = $000000D1;
  CKR_TOKEN_NOT_PRESENT                 = $000000E0;
  CKR_TOKEN_NOT_RECOGNIZED              = $000000E1;
  CKR_TOKEN_WRITE_PROTECTED             = $000000E2;
  CKR_UNWRAPPING_KEY_HANDLE_INVALID     = $000000F0;
  CKR_UNWRAPPING_KEY_SIZE_RANGE         = $000000F1;
  CKR_UNWRAPPING_KEY_TYPE_INCONSISTENT  = $000000F2;
  CKR_USER_ALREADY_LOGGED_IN            = $00000100;
  CKR_USER_NOT_LOGGED_IN                = $00000101;
  CKR_USER_PIN_NOT_INITIALIZED          = $00000102;
  CKR_USER_TYPE_INVALID                 = $00000103;
  CKR_USER_ANOTHER_ALREADY_LOGGED_IN    = $00000104;
  CKR_USER_TOO_MANY_TYPES               = $00000105;
  CKR_WRAPPED_KEY_INVALID               = $00000110;
  CKR_WRAPPED_KEY_LEN_RANGE             = $00000112;
  CKR_WRAPPING_KEY_HANDLE_INVALID       = $00000113;
  CKR_WRAPPING_KEY_SIZE_RANGE           = $00000114;
  CKR_WRAPPING_KEY_TYPE_INCONSISTENT    = $00000115;
  CKR_RANDOM_SEED_NOT_SUPPORTED         = $00000120;
  CKR_RANDOM_NO_RNG                     = $00000121;
  CKR_BUFFER_TOO_SMALL                  = $00000150;
  CKR_SAVED_STATE_INVALID               = $00000160;
  CKR_INFORMATION_SENSITIVE             = $00000170;
  CKR_STATE_UNSAVEABLE                  = $00000180;
  CKR_CRYPTOKI_NOT_INITIALIZED          = $00000190;
  CKR_CRYPTOKI_ALREADY_INITIALIZED      = $00000191;
  CKR_MUTEX_BAD                         = $000001A0;
  CKR_MUTEX_NOT_LOCKED                  = $000001A1;

  PKCS11_VENDOR_DEFINED                 = $80000000;

  CKF_TOKEN_PRESENT                     = $00000001;
  CKF_REMOVABLE_DEVICE                  = $00000002;
  CKF_HW_SLOT                           = $00000004;

  CKF_RNG                               = $00000001;
  CKF_WRITE_PROTECTED                   = $00000002;
  CKF_LOGIN_REQUIRED                    = $00000004;
  CKF_USER_PIN_INITIALIZED              = $00000008;
  CKF_RESTORE_KEY_NOT_NEEDED            = $00000020;
  CKF_CLOCK_ON_TOKEN                    = $00000040;
  CKF_PROTECTED_AUTHENTICATION_PATH     = $00000100;
  CKF_DUAL_CRYPTO_OPERATIONS            = $00000200;
  CKF_TOKEN_INITIALIZED                 = $00000400;
  CKF_SECONDARY_AUTHENTICATION          = $00000800;
  CKF_USER_PIN_COUNT_LOW                = $00001000;
  CKF_USER_PIN_FINAL_TRY                = $00002000;

  CKF_USER_PIN_LOCKED                   = $00040000;
  CKF_USER_PIN_TO_BE_CHANGED            = $00080000;
  CKF_SO_PIN_COUNT_LOW                  = $00100000;
  CKF_SO_PIN_FINAL_TRY                  = $00200000;
  CKF_SO_PIN_LOCKED                     = $00400000;
  CKF_SO_PIN_TO_BE_CHANGED              = $00800000;


  CKM_RSA_PKCS_KEY_PAIR_GEN  = $00000000;
  CKM_RSA_PKCS  = $00000001;
  CKM_RSA_9796  = $00000002;
  CKM_RSA_X_509  = $00000003;
  CKM_MD2_RSA_PKCS  = $00000004;
  CKM_MD5_RSA_PKCS  = $00000005;
  CKM_SHA1_RSA_PKCS  = $00000006;
  CKM_RIPEMD128_RSA_PKCS  = $00000007;
  CKM_RIPEMD160_RSA_PKCS  = $00000008;
  CKM_RSA_PKCS_OAEP  = $00000009;
  CKM_RSA_X9_31_KEY_PAIR_GEN  = $0000000A;
  CKM_RSA_X9_31  = $0000000B;
  CKM_SHA1_RSA_X9_31  = $0000000C;
  CKM_RSA_PKCS_PSS  = $0000000D;
  CKM_SHA1_RSA_PKCS_PSS  = $0000000E;
  CKM_DSA_KEY_PAIR_GEN  = $00000010;
  CKM_DSA  = $00000011;
  CKM_DSA_SHA1  = $00000012;
  CKM_DH_PKCS_KEY_PAIR_GEN  = $00000020;
  CKM_DH_PKCS_DERIVE  = $00000021;
  CKM_X9_42_DH_KEY_PAIR_GEN  = $00000030;
  CKM_X9_42_DH_DERIVE  = $00000031;
  CKM_X9_42_DH_HYBRID_DERIVE  = $00000032;
  CKM_X9_42_MQV_DERIVE  = $00000033;
  CKM_RC2_KEY_GEN  = $00000100;
  CKM_RC2_ECB  = $00000101;
  CKM_RC2_CBC  = $00000102;
  CKM_RC2_MAC  = $00000103;
  CKM_RC2_MAC_GENERAL  = $00000104;
  CKM_RC2_CBC_PAD  = $00000105;
  CKM_RC4_KEY_GEN  = $00000110;
  CKM_RC4  = $00000111;
  CKM_DES_KEY_GEN  = $00000120;
  CKM_DES_ECB  = $00000121;
  CKM_DES_CBC  = $00000122;
  CKM_DES_MAC  = $00000123;
  CKM_DES_MAC_GENERAL  = $00000124;
  CKM_DES_CBC_PAD  = $00000125;
  CKM_DES2_KEY_GEN  = $00000130;
  CKM_DES3_KEY_GEN  = $00000131;
  CKM_DES3_ECB  = $00000132;
  CKM_DES3_CBC  = $00000133;
  CKM_DES3_MAC  = $00000134;
  CKM_DES3_MAC_GENERAL  = $00000135;
  CKM_DES3_CBC_PAD  = $00000136;
  CKM_CDMF_KEY_GEN  = $00000140;
  CKM_CDMF_ECB  = $00000141;
  CKM_CDMF_CBC  = $00000142;
  CKM_CDMF_MAC  = $00000143;
  CKM_CDMF_MAC_GENERAL  = $00000144;
  CKM_CDMF_CBC_PAD  = $00000145;
  CKM_MD2  = $00000200;
  CKM_MD2_HMAC  = $00000201;
  CKM_MD2_HMAC_GENERAL  = $00000202;
  CKM_MD5  = $00000210;
  CKM_MD5_HMAC  = $00000211;
  CKM_MD5_HMAC_GENERAL  = $00000212;
  CKM_SHA_1  = $00000220;
  CKM_SHA_1_HMAC  = $00000221;
  CKM_SHA_1_HMAC_GENERAL  = $00000222;
  CKM_RIPEMD128  = $00000230;
  CKM_RIPEMD128_HMAC  = $00000231;
  CKM_RIPEMD128_HMAC_GENERAL  = $00000232;
  CKM_RIPEMD160  = $00000240;
  CKM_RIPEMD160_HMAC  = $00000241;
  CKM_RIPEMD160_HMAC_GENERAL  = $00000242;
  CKM_CAST_KEY_GEN  = $00000300;
  CKM_CAST_ECB  = $00000301;
  CKM_CAST_CBC  = $00000302;
  CKM_CAST_MAC  = $00000303;
  CKM_CAST_MAC_GENERAL  = $00000304;
  CKM_CAST_CBC_PAD  = $00000305;
  CKM_CAST3_KEY_GEN  = $00000310;
  CKM_CAST3_ECB  = $00000311;
  CKM_CAST3_CBC  = $00000312;
  CKM_CAST3_MAC  = $00000313;
  CKM_CAST3_MAC_GENERAL  = $00000314;
  CKM_CAST3_CBC_PAD  = $00000315;
  CKM_CAST5_KEY_GEN  = $00000320;
  CKM_CAST128_KEY_GEN  = $00000320;
  CKM_CAST5_ECB  = $00000321;
  CKM_CAST128_ECB  = $00000321;
  CKM_CAST5_CBC  = $00000322;
  CKM_CAST128_CBC  = $00000322;
  CKM_CAST5_MAC  = $00000323;
  CKM_CAST128_MAC  = $00000323;
  CKM_CAST5_MAC_GENERAL  = $00000324;
  CKM_CAST128_MAC_GENERAL  = $00000324;
  CKM_CAST5_CBC_PAD  = $00000325;
  CKM_CAST128_CBC_PAD  = $00000325;
  CKM_RC5_KEY_GEN  = $00000330;
  CKM_RC5_ECB  = $00000331;
  CKM_RC5_CBC  = $00000332;
  CKM_RC5_MAC  = $00000333;
  CKM_RC5_MAC_GENERAL  = $00000334;
  CKM_RC5_CBC_PAD  = $00000335;
  CKM_IDEA_KEY_GEN  = $00000340;
  CKM_IDEA_ECB  = $00000341;
  CKM_IDEA_CBC  = $00000342;
  CKM_IDEA_MAC  = $00000343;
  CKM_IDEA_MAC_GENERAL  = $00000344;
  CKM_IDEA_CBC_PAD  = $00000345;
  CKM_GENERIC_SECRET_KEY_GEN  = $00000350;
  CKM_CONCATENATE_BASE_AND_KEY  = $00000360;
  CKM_CONCATENATE_BASE_AND_DATA  = $00000362;
  CKM_CONCATENATE_DATA_AND_BASE  = $00000363;
  CKM_XOR_BASE_AND_DATA  = $00000364;
  CKM_EXTRACT_KEY_FROM_KEY  = $00000365;
  CKM_SSL3_PRE_MASTER_KEY_GEN  = $00000370;
  CKM_SSL3_MASTER_KEY_DERIVE  = $00000371;
  CKM_SSL3_KEY_AND_MAC_DERIVE  = $00000372;
  CKM_SSL3_MASTER_KEY_DERIVE_DH  = $00000373;
  CKM_TLS_PRE_MASTER_KEY_GEN  = $00000374;
  CKM_TLS_MASTER_KEY_DERIVE  = $00000375;
  CKM_TLS_KEY_AND_MAC_DERIVE  = $00000376;
  CKM_TLS_MASTER_KEY_DERIVE_DH  = $00000377;
  CKM_SSL3_MD5_MAC  = $00000380;
  CKM_SSL3_SHA1_MAC  = $00000381;
  CKM_MD5_KEY_DERIVATION  = $00000390;
  CKM_MD2_KEY_DERIVATION  = $00000391;
  CKM_SHA1_KEY_DERIVATION  = $00000392;
  CKM_PBE_MD2_DES_CBC  = $000003A0;
  CKM_PBE_MD5_DES_CBC  = $000003A1;
  CKM_PBE_MD5_CAST_CBC  = $000003A2;
  CKM_PBE_MD5_CAST3_CBC  = $000003A3;
  CKM_PBE_MD5_CAST5_CBC  = $000003A4;
  CKM_PBE_MD5_CAST128_CBC  = $000003A4;
  CKM_PBE_SHA1_CAST5_CBC  = $000003A5;
  CKM_PBE_SHA1_CAST128_CBC  = $000003A5;
  CKM_PBE_SHA1_RC4_128  = $000003A6;
  CKM_PBE_SHA1_RC4_40  = $000003A7;
  CKM_PBE_SHA1_DES3_EDE_CBC  = $000003A8;
  CKM_PBE_SHA1_DES2_EDE_CBC  = $000003A9;
  CKM_PBE_SHA1_RC2_128_CBC  = $000003AA;
  CKM_PBE_SHA1_RC2_40_CBC  = $000003AB;
  CKM_PKCS5_PBKD2  = $000003B0;
  CKM_PBA_SHA1_WITH_SHA1_HMAC  = $000003C0;
  CKM_KEY_WRAP_LYNKS  = $00000400;
  CKM_KEY_WRAP_SET_OAEP  = $00000401;
  CKM_CMS_SIG = $00000500;
  CKM_SKIPJACK_KEY_GEN  = $00001000;
  CKM_SKIPJACK_ECB64  = $00001001;
  CKM_SKIPJACK_CBC64  = $00001002;
  CKM_SKIPJACK_OFB64  = $00001003;
  CKM_SKIPJACK_CFB64  = $00001004;
  CKM_SKIPJACK_CFB32  = $00001005;
  CKM_SKIPJACK_CFB16  = $00001006;
  CKM_SKIPJACK_CFB8  = $00001007;
  CKM_SKIPJACK_WRAP  = $00001008;
  CKM_SKIPJACK_PRIVATE_WRAP  = $00001009;
  CKM_SKIPJACK_RELAYX  = $0000100a;
  CKM_KEA_KEY_PAIR_GEN  = $00001010;
  CKM_KEA_KEY_DERIVE  = $00001011;
  CKM_FORTEZZA_TIMESTAMP  = $00001020;
  CKM_BATON_KEY_GEN  = $00001030;
  CKM_BATON_ECB128  = $00001031;
  CKM_BATON_ECB96  = $00001032;
  CKM_BATON_CBC128  = $00001033;
  CKM_BATON_COUNTER  = $00001034;
  CKM_BATON_SHUFFLE  = $00001035;
  CKM_BATON_WRAP  = $00001036;
  CKM_ECDSA_KEY_PAIR_GEN  = $00001040;
  CKM_EC_KEY_PAIR_GEN  = $00001040;
  CKM_ECDSA  = $00001041;
  CKM_ECDSA_SHA1  = $00001042;
  CKM_ECDH1_DERIVE  = $00001050;
  CKM_ECDH1_COFACTOR_DERIVE  = $00001051;
  CKM_ECMQV_DERIVE  = $00001052;
  CKM_JUNIPER_KEY_GEN  = $00001060;
  CKM_JUNIPER_ECB128  = $00001061;
  CKM_JUNIPER_CBC128  = $00001062;
  CKM_JUNIPER_COUNTER  = $00001063;
  CKM_JUNIPER_SHUFFLE  = $00001064;
  CKM_JUNIPER_WRAP  = $00001065;
  CKM_FASTHASH  = $00001070;
  CKM_AES_KEY_GEN  = $00001080;
  CKM_AES_ECB  = $00001081;
  CKM_AES_CBC  = $00001082;
  CKM_AES_MAC  = $00001083;
  CKM_AES_MAC_GENERAL  = $00001084;
  CKM_AES_CBC_PAD  = $00001085;
  CKM_DSA_PARAMETER_GEN  = $00002000;
  CKM_DH_PKCS_PARAMETER_GEN  = $00002001;
  CKM_X9_42_DH_PARAMETER_GEN  = $00002002;
  CKM_VENDOR_DEFINED  = $80000000;

  CK_B_FALSE          = byte(0);
  CK_B_TRUE           = byte($FF);

  CK_INVALID_HANDLE   = 0;


  CKA_CLASS             = $00000000;
  CKA_TOKEN             = $00000001;
  CKA_LABEL             = $00000003;
  CKA_VALUE             = $00000011;
  CKA_CERTIFICATE_TYPE  = $00000080;
  CKA_ISSUER            = $00000081;
  CKA_SERIAL_NUMBER     = $00000082;
  CKA_CERTIFICATE_CATEGORY = $00087;
  CKA_KEY_TYPE          = $00000100;
  CKA_SUBJECT           = $00000101;
  CKA_ID                = $00000102;
  CKA_SENSITIVE         = $00000103;
  CKA_DECRYPT           = $00000105;
  CKA_SIGN              = $00000108;
  CKA_SIGN_RECOVER      = $00000108;
  CKA_MODULUS           = $00000120;
  CKA_PUBLIC_EXPONENT   = $00000122;
  CKA_PRIVATE_EXPONENT  = $00000123;
  CKA_PRIME_1           = $00000124;
  CKA_PRIME_2           = $00000125;
  CKA_EXPONENT_1        = $00000126;
  CKA_EXPONENT_2        = $00000127;
  CKA_COEFFICIENT       = $00000128;
  CKA_PRIME             = $00000130;
  CKA_SUBPRIME          = $00000131;
  CKA_BASE              = $00000132;
  CKA_EXTRACTABLE       = $00000162;

  CKC_X_509             = $00000000;

  CKF_OS_LOCKING_OK     = $00000002;

  CKF_RW_SESSION        = 2;
  CKF_SERIAL_SESSION    = 4;

  CKK_RSA               = $00000000;
  CKK_DSA               = $00000001;
  CKK_DH                = $00000002;
  CKK_X9_42_DH          = $00000004;

  CKN_SURRENDER       = 0;

  CKO_CERTIFICATE       = $00000001;
  CKO_PUBLIC_KEY        = $00000002;
  CKO_PRIVATE_KEY       = $00000003;

  CKS_RO_PUBLIC_SESSION = 0;
  CKS_RO_USER_FUNCTIONS = 1;
  CKS_RW_PUBLIC_SESSION = 2;
  CKS_RW_USER_FUNCTIONS = 3;
  CKS_RW_SO_FUNCTIONS   = 4;

  CKU_SO                = 0;
  CKU_USER              = 1;

  NULL_PTR              = pointer(0);

type

  TPKCS11FuncArray = array[PKCS11_FUNC_FIRST..PKCS11_FUNC_LAST] of Pointer;

  CK_RV =  Cardinal;
  CK_ULONG =  Cardinal;
  CK_FLAGS =  CK_ULONG;
  CK_SLOT_ID =  CK_ULONG;
  CK_SLOT_ID_ARRAY =  array[0..0] of CK_SLOT_ID;
  CK_SESSION_HANDLE =  CK_ULONG;
  CK_VOID_PTR = pointer;
  CK_NOTIFICATION =  CK_ULONG;
  CK_STATE =  CK_ULONG;
  CK_USER_TYPE =  CK_ULONG;
  CK_ATTRIBUTE_TYPE =  CK_ULONG;
  CK_KEY_TYPE =  CK_ULONG;

  CK_UTF8CHAR_PTR = PChar;

  CK_OBJECT_CLASS =  ULONG;
  CK_CERTIFICATE_TYPE =  ULONG;
  CK_OBJECT_HANDLE =  ULONG;
  CK_OBJECT_HANDLE_ARRAY =  array[0..0] of CK_OBJECT_HANDLE;
  CK_BYTE =  byte;
  CK_CHAR =  CK_BYTE;     // an unsigned 8-bit character
  CK_UTF8CHAR =  CK_BYTE; // an 8-bit UTF-8 character
  CK_BBOOL =  CK_BYTE ;   // a BYTE-sized Boolean flag
  CK_LONG   =  Integer;   //* a signed value, the same size as a CK_ULONG */
  CK_CHAR_PTR = ^CK_CHAR;//* Pointer to a CK_CHAR */
  CK_VOID_PTR_PTR = ^CK_VOID_PTR;
  CK_VOID = pointer;
  CK_BYTE_PTR = ^CK_BYTE;
  CK_ULONG_PTR = ^CK_ULONG;

  TPKCS11Version = packed  record
    Major, Minor : byte;
  end;

  TPKCS11FunctionList = packed record
    Version : TPKCS11Version;
    C_Initialize,
    C_Finalize,
    C_GetInfo,
    C_GetFunctionList,
    C_GetSlotList,
    C_GetSlotInfo,
    C_GetTokenInfo,
    C_GetMechanismList,
    C_GetMechanismInfo,
    C_InitToken,
    C_InitPIN,
    C_SetPIN,
    C_OpenSession,
    C_CloseSession,
    C_CloseAllSessions,
    C_GetSessionInfo,
    C_GetOperationState,
    C_SetOperationState,
    C_Login,
    C_Logout,
    C_CreateObject,
    C_CopyObject,
    C_DestroyObject,
    C_GetObjectSize,
    C_GetAttributeValue,
    C_SetAttributeValue,
    C_FindObjectsInit,
    C_FindObjects,
    C_FindObjectsFinal,
    C_EncryptInit,
    C_Encrypt,
    C_EncryptUpdate,
    C_EncryptFinal,
    C_DecryptInit,
    C_Decrypt,
    C_DecryptUpdate,
    C_DecryptFinal,
    C_DigestInit,
    C_Digest,
    C_DigestUpdate,
    C_DigestKey,
    C_DigestFinal,
    C_SignInit,
    C_Sign,
    C_SignUpdate,
    C_SignFinal,
    C_SignRecoverInit,
    C_SignRecover,
    C_VerifyInit,
    C_Verify,
    C_VerifyUpdate,
    C_VerifyFinal,
    C_VerifyRecoverInit,
    C_VerifyRecover,
    C_DigestEncryptUpdate,
    C_DecryptDigestUpdate,
    C_SignEncryptUpdate,
    C_DecryptVerifyUpdate,
    C_GenerateKey,
    C_GenerateKeyPair,
    C_WrapKey,
    C_UnwrapKey,
    C_DeriveKey,
    C_SeedRandom,
    C_GenerateRandom,
    C_GetFunctionStatus,
    C_CancelFunction,
    C_WaitForSlotEvent
    : pointer;
  end;
  PPKCS11FunctionList = ^TPKCS11FunctionList;
  CK_FUNCTION_LIST = TPKCS11FunctionList;
  CK_FUNCTION_LIST_PTR = ^CK_FUNCTION_LIST;
  CK_FUNCTION_LIST_PTR_PTR = ^CK_FUNCTION_LIST_PTR;

  TPKCS11InitializeArgs = packed  record
    CreateMutex  : pointer;
    DestroyMutex : pointer;
    LockMutex    : pointer;
    UnlockMutex  : pointer;
    flags        : CK_FLAGS;
    pReserved    : CK_VOID_PTR;
  end;

  PPKCS11InitializeArgs = ^TPKCS11InitializeArgs;

  CK_C_INITIALIZE_ARGS = TPKCS11InitializeArgs;

  CK_C_INITIALIZE_ARGS_PTR = ^CK_C_INITIALIZE_ARGS;


  TPKCS11Info = packed  record
    cryptokiVersion    : TPKCS11Version;
    manufacturerID     : array[0..31] of char;
    flags              : DWORD;
    libraryDescription : array[0..31] of char;
    libraryVersion     : TPKCS11Version;
  end;

  PPKCS11Info = ^TPKCS11Info;

  CK_INFO =  TPKCS11Info;

  CK_INFO_PTR = ^CK_INFO;

  TPKCS11SlotInfo = packed  record
    slotDescription : array[0..63] of char;
    manufacturerID  : array[0..31] of char;
    flags           : DWORD;
    hardwareVersion : TPKCS11Version;
    firmwareVersion : TPKCS11Version;
  end;

  PPKCS11SlotInfo = ^TPKCS11SlotInfo;

  CK_SLOT_INFO =  TPKCS11SlotInfo;

  CK_SLOT_INFO_PTR = ^CK_SLOT_INFO;

  TPKCS11TokenInfo = packed  record
    _label         : array[0..31] of char;
    manufacturerID : array[0..31] of char;
    model          : array[0..15] of char;
    serialNumber   : array[0..15] of char;
    flags          : DWORD;

    ulMaxSessionCount : DWORD;
    ulSessionCount : DWORD;
    ulMaxRwSessionCount : DWORD;
    ulRwSessionCount : DWORD;
    ulMaxPinLen : DWORD;
    ulMinPinLen : DWORD;
    ulTotalPublicMemory : DWORD;
    ulFreePublicMemory : DWORD;
    ulTotalPrivateMemory : DWORD;
    ulFreePrivateMemory : DWORD;

    hardwareVersion : TPKCS11Version;
    firmwareVersion : TPKCS11Version;
    utcTime         : array[0..15] of char;
  end;

  PPKCS11TokenInfo = ^TPKCS11TokenInfo;

  CK_TOKEN_INFO =  TPKCS11TokenInfo;

  CK_TOKEN_INFO_PTR = ^CK_TOKEN_INFO;

  TPKCS11SessionInfo = packed  record
    slotID : CK_SLOT_ID;
    state  : CK_STATE;
    flags  : CK_FLAGS;
    ulDeviceError : CK_ULONG;
  end;

  PPKCS11SessionInfo = ^TPKCS11SessionInfo;

  CK_SESSION_INFO =  TPKCS11SessionInfo;

  CK_SESSION_INFO_PTR = ^CK_SESSION_INFO;

  TPKCS11Attribute = packed  record
    _type      : CK_ATTRIBUTE_TYPE;
    pValue     : CK_VOID_PTR;
    ulValueLen : CK_ULONG;
  end;

  PPKCS11Attribute = ^TPKCS11Attribute;

  CK_ATTRIBUTE =  TPKCS11Attribute;

  CK_ATTRIBUTE_PTR = ^CK_ATTRIBUTE;

  CK_ATTRIBUTE_ARRAY =  array[0..65536] of CK_ATTRIBUTE;

  CK_MECHANISM_TYPE =  CK_ULONG;

  CK_MECHANISM = packed  record
    mechanism  : CK_MECHANISM_TYPE;
    pParameter : CK_VOID_PTR;
    ulParameterLen: CK_ULONG;
  end;

  CK_MECHANISM_PTR = ^CK_MECHANISM;

  TPKCS11MechanismInfo = packed  record
     ulMinKeySize : CK_ULONG;
     ulMaxKeySize : CK_ULONG;
     flags : CK_FLAGS;
  end;

  PPKCS11MechanismInfo = ^TPKCS11MechanismInfo;
  CK_MECHANISM_INFO = ^TPKCS11MechanismInfo;
  CK_MECHANISM_INFO_PTR = ^CK_MECHANISM_INFO;

  TPKCS11GetFunctionListFunc = function(var FunctionList : PPKCS11FunctionList) : CK_RV; cdecl;
  TPKCS11NotifyCallbackFunc = function (hSession : CK_SESSION_HANDLE; event : CK_NOTIFICATION; pApplication : CK_VOID_PTR) : CK_RV; cdecl;
  CK_NOTIFY = ^TPKCS11NotifyCallbackFunc;
  TPKCS11InitializeFunc = function(pArgs : PPKCS11InitializeArgs) : CK_RV; cdecl;
  TPKCS11FinalizeFunc = function(pReserved : pointer) : CK_RV; cdecl;
  TPKCS11GetInfoFunc = function(var pInfo : TPKCS11Info) : CK_RV; cdecl;
  TPKCS11GetSlotListFunc = function(tokenPresent : CK_BBOOL; var pSlotList : CK_SLOT_ID; var pulCount : CK_ULONG) : CK_RV; cdecl;
  TPKCS11GetSlotInfoFunc = function(slotID : CK_SLOT_ID; var pInfo : TPKCS11SlotInfo) : CK_RV; cdecl;
  TPKCS11GetTokenInfoFunc = function(slotID : CK_SLOT_ID; var pInfo : TPKCS11TokenInfo) : CK_RV; cdecl;
  TPKCS11OpenSessionFunc  = function(slotID : CK_SLOT_ID; flags : CK_FLAGS; CallbackData : CK_VOID_PTR; NotifyProc : TPKCS11NotifyCallbackFunc; var SessionHandle : CK_SESSION_HANDLE) : CK_RV; cdecl;
  TPKCS11GetSessionInfoFunc = function(hSession : CK_SESSION_HANDLE; var pInfo : TPKCS11SessionInfo) : CK_RV; cdecl;
  TPKCS11CloseSessionFunc  = function(hSession : CK_SESSION_HANDLE) : CK_RV; cdecl;
  TPKCS11LoginFunc = function(hSession : CK_SESSION_HANDLE; userType : CK_USER_TYPE; pPin : CK_UTF8CHAR_PTR; ulPinLen : CK_ULONG)  : CK_RV; cdecl;
  TPKCS11LogoutFunc = function(hSession : CK_SESSION_HANDLE) : CK_RV; cdecl;
  TPKCS11FindObjectsInitFunc  = function(hSession : CK_SESSION_HANDLE; {var pAttributes : CK_ATTRIBUTE} pAttributes : CK_ATTRIBUTE_PTR; ulCount : CK_ULONG) : CK_RV; cdecl;
  TPKCS11FindObjectsFunc  = function(hSession : CK_SESSION_HANDLE; var phObject : CK_OBJECT_HANDLE; ulMaxObjectCount : CK_ULONG; var pulObjectCount : CK_ULONG) : CK_RV; cdecl;
  TPKCS11FindObjectsFinalFunc = function(hSession : CK_SESSION_HANDLE) : CK_RV; cdecl;
  TPKCS11GetAttributeValueFunc = function(hSession : CK_SESSION_HANDLE; hObject : CK_OBJECT_HANDLE; pTemplate : pointer; ulCount : CK_ULONG) : CK_RV; cdecl;
  TPKCS11DestroyObjectFunc = function(hSession : CK_SESSION_HANDLE; hObject : CK_OBJECT_HANDLE) : CK_RV; cdecl;
  TPKCS11CreateObjectFunc  = function(hSession : CK_SESSION_HANDLE; pTemplate : CK_ATTRIBUTE_PTR; ulCount : CK_ULONG; var hObject : CK_OBJECT_HANDLE) : CK_RV; cdecl;

  TPKCS11SignInitFunc = function(hSession : CK_SESSION_HANDLE; pMechanism: CK_MECHANISM_PTR;
    hKey: CK_OBJECT_HANDLE): CK_RV; cdecl;
  TPKCS11SignFunc = function(hSession: CK_SESSION_HANDLE; pData: CK_BYTE_PTR;
    ulDataLen: CK_ULONG; pSignature: CK_BYTE_PTR; pulSignatureLen: CK_ULONG_PTR): CK_RV; cdecl;
  TPKCS11DecryptInitFunc = function(hSession: CK_SESSION_HANDLE; pMechanism: CK_MECHANISM_PTR;
    hKey: CK_OBJECT_HANDLE): CK_RV; cdecl;
  TPKCS11DecryptFunc = function(hSession: CK_SESSION_HANDLE; pEncryptedData:
    CK_BYTE_PTR; ulEncryptedDataLen: CK_ULONG; pData: CK_BYTE_PTR; pulDataLen:
    CK_ULONG_PTR): CK_RV; cdecl;
  ///////////////////////////////////////
  TPKCS11WaitForSlotEventFunc = function (flags : CK_FLAGS; var pSlot : CK_SLOT_ID; pReserved : pointer) : CK_RV; cdecl;
  TPKCS11GetMechanismListFunc = function (slotID : CK_SLOT_ID; var pMechanismList : CK_MECHANISM_TYPE;var pulCount : CK_ULONG) : CK_RV; cdecl;
  TPKCS11GetMechanismInfoFunc = function (slotID : CK_SLOT_ID; mechanism_type : CK_MECHANISM_TYPE; var pInfo : CK_MECHANISM_INFO) : CK_RV; cdecl;
  TPKCS11InitTokenFunc = function (slotID : CK_SLOT_ID; pPin : CK_UTF8CHAR_PTR; ulPinLen : CK_ULONG; pLabel : CK_UTF8CHAR_PTR) : CK_RV; cdecl;
  TPKCS11InitPINFunc = function (hSession : CK_SESSION_HANDLE; pPin : CK_UTF8CHAR_PTR; ulPinLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11SetPINFunc = function (hSession : CK_SESSION_HANDLE; pOldPin : CK_UTF8CHAR_PTR; ulOldLen : CK_ULONG; pNewPin : CK_UTF8CHAR_PTR; ulNewLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11CloseAllSessionsFunc = function (slotID : CK_SLOT_ID) : CK_RV; cdecl;
  TPKCS11GetOperationStateFunc = function (hSession : CK_SESSION_HANDLE; var pOperationState : CK_BYTE; var pulOperationStateLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11SetOperationStateFunc = function (hSession : CK_SESSION_HANDLE; pOperationState : CK_BYTE_PTR; ulOperationStateLen : CK_ULONG;
                             hEncryptionKey : CK_OBJECT_HANDLE; hAuthenticationKey : CK_OBJECT_HANDLE) : CK_RV; cdecl;
  TPKCS11CopyObjectFunc = function (hSession : CK_SESSION_HANDLE; hObject : CK_OBJECT_HANDLE; pTemplate : CK_ATTRIBUTE_PTR; ulCount : CK_ULONG; var phNewObject : CK_OBJECT_HANDLE) : CK_RV; cdecl;
  TPKCS11GetObjectSizeFunc = function (hSession : CK_SESSION_HANDLE; hObject : CK_OBJECT_HANDLE; var pulSize : CK_ULONG) : CK_RV; cdecl;
  TPKCS11SetAttributeValueFunc = function (hSession : CK_SESSION_HANDLE; hObject : CK_OBJECT_HANDLE; pTemplate : CK_ATTRIBUTE_PTR; ulCount : CK_ULONG) : CK_RV; cdecl;
  TPKCS11EncryptInitFunc = function (hSession : CK_SESSION_HANDLE; pMechanism : CK_MECHANISM_PTR; hKey: CK_OBJECT_HANDLE) : CK_RV; cdecl;
  TPKCS11EncryptFunc = function (hSession : CK_SESSION_HANDLE; pData : CK_BYTE_PTR; ulDataLen : CK_ULONG;
                   var pEncryptedData : CK_BYTE; var pulEncryptedDataLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11EncryptUpdateFunc = function (hSession : CK_SESSION_HANDLE; pPart : CK_BYTE_PTR; ulPartLen : CK_ULONG;
                   var pEncryptedPart : CK_BYTE; var pulEncryptedPartLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11EncryptFinalFunc = function (hSession : CK_SESSION_HANDLE; var pLastEncryptedPart : CK_BYTE; var pulLastEncryptedPartLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11DecryptUpdateFunc = function (hSession : CK_SESSION_HANDLE; pEncryptedPart : CK_BYTE_PTR; ulEncryptedPartLen : CK_ULONG;
                         var pPart : CK_BYTE; var pulPartLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11DecryptFinalFunc = function (hSession : CK_SESSION_HANDLE; var pLastPart : CK_BYTE; var pulLastPartLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11DigestInitFunc = function (hSession : CK_SESSION_HANDLE; pMechanism : CK_MECHANISM_PTR) : CK_RV; cdecl;
  TPKCS11DigestFunc = function (hSession : CK_SESSION_HANDLE; pData : CK_BYTE_PTR; ulDataLen : CK_ULONG;
                  var pDigest : CK_BYTE; var pulDigestLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11DigestUpdateFunc = function (hSession : CK_SESSION_HANDLE; pPart : CK_BYTE_PTR; ulPartLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11DigestKeyFunc = function (hSession : CK_SESSION_HANDLE; hKey : CK_OBJECT_HANDLE) : CK_RV; cdecl;
  TPKCS11DigestFinalFunc = function (hSession : CK_SESSION_HANDLE; var pDigest : CK_BYTE; var pulDigestLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11SignUpdateFunc = function (hSession : CK_SESSION_HANDLE; pPart : CK_BYTE_PTR; ulPartLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11SignFinalFunc = function (hSession : CK_SESSION_HANDLE; var pSignature : CK_BYTE; var pulSignatureLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11SignRecoverInitFunc = function (hSession : CK_SESSION_HANDLE; pMechanism : CK_MECHANISM_PTR; hKey : CK_OBJECT_HANDLE) : CK_RV; cdecl;
  TPKCS11SignRecoverFunc = function (hSession : CK_SESSION_HANDLE; pData : CK_BYTE_PTR; ulDataLen : CK_ULONG;
                       var pSignature : CK_BYTE; var pulSignatureLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11VerifyInitFunc = function (hSession : CK_SESSION_HANDLE; pMechanism : CK_MECHANISM_PTR; hKey : CK_OBJECT_HANDLE) : CK_RV; cdecl;
  TPKCS11VerifyFunc = function (hSession : CK_SESSION_HANDLE; pData : CK_BYTE_PTR; ulDataLen : CK_ULONG;
                  pSignature : CK_BYTE_PTR; ulSignatureLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11VerifyUpdateFunc = function (hSession : CK_SESSION_HANDLE; pPart : CK_BYTE_PTR; ulPartLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11VerifyFinalFunc = function (hSession : CK_SESSION_HANDLE; pSignature : CK_BYTE_PTR; ulSignatureLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11VerifyRecoverInitFunc = function (hSession : CK_SESSION_HANDLE; pMechanism : CK_MECHANISM_PTR; hKey : CK_OBJECT_HANDLE) : CK_RV; cdecl;
  TPKCS11VerifyRecoverFunc = function (hSession : CK_SESSION_HANDLE; pSignature : CK_BYTE_PTR; ulSignatureLen : CK_ULONG;
                         var pData : CK_BYTE; var pulDataLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11DigestEncryptUpdateFunc = function (hSession : CK_SESSION_HANDLE; pPart : CK_BYTE_PTR; ulPartLen : CK_ULONG;
                               var pEncryptedPart : CK_BYTE; var pulEncryptedPartLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11DecryptDigestUpdateFunc = function (hSession : CK_SESSION_HANDLE; pEncryptedPart : CK_BYTE_PTR; ulEncryptedPartLen : CK_ULONG;
                               var pPart : CK_BYTE; var pulPartLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11SignEncryptUpdateFunc = function (hSession : CK_SESSION_HANDLE; pPart : CK_BYTE_PTR; ulPartLen : CK_ULONG;
                             var pEncryptedPart : CK_BYTE; var pulEncryptedPartLen : CK_ULONG)  : CK_RV; cdecl;
  TPKCS11DecryptVerifyUpdateFunc = function (hSession : CK_SESSION_HANDLE; pEncryptedPart : CK_BYTE_PTR; ulEncryptedPartLen : CK_ULONG;
                               var pPart : CK_BYTE; var pulPartLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11GenerateKeyFunc = function (hSession : CK_SESSION_HANDLE; pMechanism : CK_MECHANISM_PTR; pTemplate : CK_ATTRIBUTE_PTR;
                       ulCount : CK_ULONG; var phKey : CK_OBJECT_HANDLE) : CK_RV; cdecl;
  TPKCS11GenerateKeyPairFunc = function (hSession : CK_SESSION_HANDLE; pMechanism : CK_MECHANISM_PTR;
                           pPublicKeyTemplate : CK_ATTRIBUTE_PTR; ulPublicKeyAttributeCount : CK_ULONG;
                           pPrivateKeyTemplate : CK_ATTRIBUTE_PTR; ulPrivateKeyAttributeCount : CK_ULONG;
                           var phPublicKey : CK_OBJECT_HANDLE; var phPrivateKey : CK_OBJECT_HANDLE
                           ) : CK_RV; cdecl;
  TPKCS11WrapKeyFunc = function (hSession : CK_SESSION_HANDLE; pMechanism : CK_MECHANISM_PTR; hWrappingKey : CK_OBJECT_HANDLE;
                   hKey : CK_OBJECT_HANDLE; var pWrappedKey : CK_BYTE; var pulWrappedKeyLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11UnwrapKeyFunc = function (hSession : CK_SESSION_HANDLE; pMechanism : CK_MECHANISM_PTR; hUnwrappingKey : CK_OBJECT_HANDLE;
                     pWrappedKey : CK_BYTE_PTR; ulWrappedKeyLen : CK_ULONG; pTemplate : CK_ATTRIBUTE_PTR;
                     ulAttributeCount : CK_ULONG; var phKey : CK_OBJECT_HANDLE) : CK_RV; cdecl;
  TPKCS11DeriveKeyFunc = function (hSession : CK_SESSION_HANDLE; pMechanism : CK_MECHANISM_PTR; hBaseKey : CK_OBJECT_HANDLE;
                     pTemplate : CK_ATTRIBUTE_PTR; ulAttributeCount : CK_ULONG; var phKey : CK_OBJECT_HANDLE) : CK_RV; cdecl;
  TPKCS11SeedRandomFunc = function (hSession : CK_SESSION_HANDLE; pSeed : CK_BYTE_PTR; ulSeedLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11GenerateRandomFunc = function (hSession : CK_SESSION_HANDLE; var pRandomData : CK_BYTE; ulRandomLen : CK_ULONG) : CK_RV; cdecl;
  TPKCS11GetfunctionStatusFunc = function (hSession : CK_SESSION_HANDLE) : CK_RV; cdecl;
  TPKCS11CancelfunctionFunc = function (hSession : CK_SESSION_HANDLE) : CK_RV; cdecl;

implementation

end.

