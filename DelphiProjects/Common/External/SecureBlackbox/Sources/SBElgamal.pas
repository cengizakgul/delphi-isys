(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBElgamal;

interface

uses
  SBMath, SBSSHMath;


function Generate(Bits : integer; P : PLInt; G : PLInt; X: PLInt; Y : PLInt) : boolean; overload;
function Generate(Bits : integer; P : PLInt; G : PLInt; X: PLInt; Y : PLInt;
  ProgressFunc : TSBMathProgressFunc; Data : pointer) : boolean; overload;
function Encrypt(Src : PLInt; P : PLInt; G : PLInt; Y : PLInt; A : PLInt; B : PLInt) : boolean; 

function Decrypt(P : PLInt; G : PLInt; X : PLInt; A : PLInt; B : PLInt; Dest : PLInt) : boolean; 

function Sign(Src : PLint; P : PLInt; G : PLInt; X : PLInt; A : PLint; B : PLInt) : boolean; 

function Verify(Src : PLInt; P : PLInt; G : PLInt; Y : PLInt; A : PLInt; B : PLInt) : boolean; 

implementation

function Generate(Bits : integer; P : PLInt; G : PLInt; X: PLInt; Y : PLInt) : boolean;
var
  Ctx : TRC4RandomContext;
begin
  LGenPrime(P, Bits shr 5, false);
  LRC4Init(Ctx);
  G.Digits[1] := 2;
  G.Length := 1;
  LRandom(Ctx, X, Bits shr 3);
  while LGreater(X, P) do
    LShr(X);
  X.Digits[1] := X.Digits[1] or 1;
  LMModPower(G, X, P, Y);
  Result := true;
end;

function Generate(Bits : integer; P : PLInt; G : PLInt; X: PLInt; 
  Y : PLInt; 
  ProgressFunc : TSBMathProgressFunc; 
  Data : pointer) : boolean;
var
  Ctx : TRC4RandomContext;
begin
  try
    LGenPrime(P, Bits shr 5, false, ProgressFunc, Data, true);
    LRC4Init(Ctx);
    G.Digits[1] := 2;
    G.Length := 1;
    LRandom(Ctx, X, Bits shr 3);
    while LGreater(X, P) do
      LShr(X);
    X.Digits[1] := X.Digits[1] or 1;
    LMModPower(G, X, P, Y, ProgressFunc, Data, true);
    Result := true;
  except
    Result := false;
  end;
end;

function Encrypt(Src : PLInt; P : PLInt; G : PLInt; Y : PLInt; A : PLInt; B : PLInt) : boolean;
var
  K, T1, T2, T3, One : PLInt;
  F : boolean;
begin
  LCreate(K);
  LCreate(T1);
  LCreate(T2);
  LCreate(T3);
  LCreate(One);
  repeat
    LGenerate(K, P.Length);
    while LGreater(K, P) do
      LShr(K);
    K.Digits[1] := K.Digits[1] or 1;
    LSub(P, One, T2);
    LGCD(K, T2, T1, T3);
    F := (T1.Length = 1) and (T1.Digits[1] = 1);
  until F;
  //LMModPower(G, K, P, A);
  //LMModPower(Y, K, P, T1);
  LSSHModPower(G, K, P, A);
  LSSHModPower(Y, K, P, T1);
  LMult(T1, Src, T2);
  LModEx(T2, P, B);
  LDestroy(K);
  LDestroy(T1);
  LDestroy(T2);
  LDestroy(T3);
  LDestroy(One);
  Result := true;
end;

function Decrypt(P : PLInt; G : PLInt; X : PLInt; A : PLInt; B : PLInt; Dest : PLInt) : boolean;
var
  T1, T2 : PLInt;
begin
  LCreate(T1);
  LCreate(T2);
  LGCD(A, P, T1, T2);
  //LMModPower(T2, X, P, T1);
  LSSHModPower(T2, X, P, T1);
  LMult(B, T1, T2);
  LModEx(T2, P, Dest);
  LDestroy(T1);
  LDestroy(T2);
  Result := true;
end;

function Sign(Src : PLint; P : PLInt; G : PLInt; X : PLInt; A : PLint; B : PLInt) : boolean;
var
  K, KInv, T1, T2, T3, One : PLInt;
  F : boolean;
begin
  LCreate(K);
  LCreate(KInv);
  LCreate(T1);
  LCreate(T2);
  LCreate(T3);
  LCreate(One);
  LSub(P, One, T2);
  repeat
    LGenerate(K, P.Length);
    while LGreater(K, P) do
      LShr(K);
    K.Digits[1] := K.Digits[1] or 1;
    LGCD(K, T2, T1, KInv);
    F := (T1.Length = 1) and (T1.Digits[1] = 1);
  until F;
  //LMModPower(G, K, P, A);
  LSSHModPower(G, K, P, A);
  LMult(A, X, T1);
  LModEx(T1, T2, T3);
  if not LGreater(Src, T3) then
    LAdd(Src, T2, T1)
  else
    LCopy(T1, Src);
  LSub(T1, T3, B);
  LMult(B, KInv, T1);
  LModEx(T1, T2, B);
  LDestroy(K);
  LDestroy(KInv);
  LDestroy(T1);
  LDestroy(T2);
  LDestroy(T3);
  LDestroy(One);
  Result := true;
end;

function Verify(Src : PLInt; P : PLInt; G : PLInt; Y : PLInt; A : PLInt; B : PLInt) : boolean;
var
  T1, T2, T3 : PLInt;
begin
  LCreate(T1);
  LCreate(T2);
  LCreate(T3);
  //LMModPower(Y, A, P, T1);
  //LMModPower(A, B, P, T2);
  LSSHModPower(Y, A, P, T1);
  LSSHModPower(A, B, P, T2);
  LMult(T1, T2, T3);
  LModEx(T3, P, T1);
  //LMModPower(G, Src, P, T2);
  LSSHModPower(G, Src, P, T2);
  Result := LEqual(T2, T1);
  LDestroy(T1);
  LDestroy(T2);
  LDestroy(T3);
end;

end.
