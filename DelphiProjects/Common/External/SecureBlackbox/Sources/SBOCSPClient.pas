(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBOCSPClient;

interface

uses
  Classes,
  SysUtils,
 
  SBUtils,
  SBASN1,
  SBASN1Tree,
  SBX509,
  SBX509Ext,
  SBSHA,
  SBOCSPCommon,
  SBCustomCertStorage;

type
  TElOCSPClient = class(TSBControlBase)
  protected
    FCertStorage: TElCustomCertStorage;
    FReplyProducedAt: TDateTime;

    FThisUpdate : array of TDateTime;
    FNextUpdate : array of TDateTime;
    FRevocationTime : array of TDateTime;
    FRevocationReason : array of TSBCRLReasonFlag;
    FCertStatus : array of TElOCSPCertificateStatus;
//    FRequestorName : string;
    FNonce: BufferType;

    FReplyBuf: ByteArray;
    FReplyBuf2: ByteArray;
    FNesting: integer;
    FParseState: integer;
    FParseState2: integer;
    FParseCert: integer;
    FRespStatus: TElOCSPServerError;
    FASN1Parser : TElASN1Parser;
  protected
    procedure HandleASN1Read(Sender: TObject; Buffer: pointer; var Size: longint);
    procedure HandleASN1Tag(Sender: TObject; TagType: asn1TagType;
      TagConstrained: boolean; Tag: pointer; TagSize: integer; Size: integer; Data: pointer;
      BitRest: integer; var Valid: boolean);
    procedure HandleASN1Tag2(Sender: TObject; TagType: asn1TagType;
      TagConstrained: boolean; Tag: pointer; TagSize: integer; Size: integer; Data: pointer;
      BitRest: integer; var Valid: boolean);

    function GetCertStatus(Index: Integer): TElOCSPCertificateStatus;
    function GetThisUpdate(Index: Integer): TDateTime;
    function GetNextUpdate(Index: Integer): TDateTime;

    function GetRevocationTime(Index: Integer): TDateTime;
    function GetRevocationReason(Index: Integer): TSBCRLReasonFlag;

    procedure SetCertStorage(Value : TElCustomCertStorage);

    procedure Notification(AComponent : TComponent; AOperation : TOperation); override;

  
  public
    constructor Create(Owner: TSBComponentBase);  override;
    destructor Destroy; override;

    function CreateRequest(var Request : ByteArray) : integer;
    function ProcessReply(const Reply : ByteArray; var ServerResult : TElOCSPServerError) : integer;

    property ReplyProducedAt: TDateTime read FReplyProducedAt;

    property CertStatus[Index: Integer]: TElOCSPCertificateStatus read GetCertStatus;
    property RevocationTime[Index: Integer]:TDateTime read GetRevocationTime;
    property RevocationReason[Index: Integer]: TSBCRLReasonFlag read GetRevocationReason;

    property ThisUpdate[Index: Integer]: TDateTime read GetThisUpdate;
    property NextUpdate[Index: Integer]: TDateTime read GetNextUpdate;

  published
    property CertStorage: TElCustomCertStorage read FCertStorage write SetCertStorage;
//    property RequestorName: string read FRequestorName write FRequestorName;
    property Nonce: BufferType read FNonce write FNonce;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('PKIBlackbox', [TElOCSPClient]);
end;

constructor TElOCSPClient.Create(Owner: TSBComponentBase);
begin
  inherited Create(Owner);
end;


destructor TElOCSPClient.Destroy;
begin
  inherited;
end;

(*function WriteRequestorName(Name: string): BufferType;
var
  {$ifndef DELPHI_NET}
  b: string;
  l: TStringList;
  {$else}
  b: ByteArray;
  l: array of ByteArray;
  {$endif}
begin
  {$ifndef DELPHI_NET}
  b := #4; // ??? directory name = 4
  l := TStringList.Create;
  l.Add(WriteOID(b));
  l.Add(WritePrintableString(Name));
  b := WriteSequence(l);
  l.Clear;
  l.Add(b);
  b := WriteSet(l);
  l.Clear;
  l.Add(b);
  b := WriteSequence(l);
  result := WriteExplicit(b);
  {$else}
  SetLength(b, 1);
  b[0] := 4; // ??? directory name = 4
  SetLength(l, 2);
  l[0] := WriteOID(b);
  l[1] := WritePrintableString(Name);
  b := WriteSequence(l);
  l[0] := b;
  b := WriteSet(l);
  l[0] := b;
  b := WriteSequence(l);
  result := WriteExplicit(b);
  {$endif}
end;*)

function SHA1Digest(d: BufferType): BufferType;
var
  dg: TMessageDigest160;
//  ctx: TSHA1Context;
begin
//  InitializeSHA1(ctx);
  dg := HashSHA1(d);
  result := DigestToBinary(dg);
//  FinalizeSHA1(ctx);
end;

function issuerNameHash(cert: TElX509Certificate): BufferType;
var
  iname: BufferType;
begin
  iname := SHA1Digest(cert.WriteSubject{Issuer?});
  result := WriteOctetString(iname);
end;

function issuerKeyHash(cert: TElX509Certificate): BufferType;
var
  i: integer;
  ikey: BufferType;
begin
  i := cert.GetPublicKeySize;
  SetLength(ikey, i);
  cert.GetPublicKeyBlob(@ikey[1], i);
  result := WriteOctetString(SHA1Digest(ikey));
end;

function WriteExtension(id: BufferType; critical: boolean; value: BufferType): BufferType;
var
  l: TStringList;
begin
  l := TStringList.Create;
  l.Add(WriteOID(id));
  l.Add(WriteBoolean(critical));
  l.Add(WriteOctetString(value));
  result := WriteSequence(l);
  l.Free;
end;

function WriteNonce(value: BufferType): BufferType;
var
  s: BufferType;
  l: TStringList;
begin
  s := TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$30#$01#$02);
  l := TStringList.Create;
  l.Add(WriteOID(s));
  l.Add(WriteOctetString(value));
  s := WriteSequence(l);
  l.Clear;
  l.Add(s);
  result := WriteSequence(l);
  l.Free;
end;

function WriteHashAlgorithm: BufferType;
var
  id: BufferType;
  l: TStringList;
begin
  id := TBufferTypeConst(#$2A#$86#$48#$86#$F7#$0D#$01#$01#$05);
  l := TStringList.Create;
  l.Add(WriteOID(id));
  l.Add(WriteNULL);
  result := WriteSequence(l);
  l.Free;
end;

function WriteRequestList(cstor: TElCustomCertStorage): BufferType;
var
  requestList: TStringList;
  request: TStringList;
  i: integer;
  b: BufferType;
begin
  requestList := TStringList.Create;
  request := TStringList.Create;
  for i := 1 to cstor.Count do
  begin
    request.Clear;
    request.Add(WriteHashAlgorithm);
    request.Add(issuerNameHash(cstor.Certificates[i - 1]));
    request.Add(issuerKeyHash(cstor.Certificates[i - 1]));
    if Length(cstor.Certificates[i - 1].SerialNumber) > 0 then
      request.Add(cstor.Certificates[i - 1].WriteSerialNumber);
    b := WriteSequence(request);
    request.Clear;
    request.Add(b);
    requestList.Add(WriteSequence(request));
  end;
  result := WriteSequence(requestList);
  requestList.Free;
  request.Free;
end;

function TElOCSPClient.CreateRequest(var Request : ByteArray): integer;
var
  l: TStringList;
  b: BufferType;
begin
  if (CertStorage = nil) or (CertStorage.Count = 0) then
  begin
    SetLength(Request, 0);
    result := SB_OCSP_ERROR_NO_CERTIFICATES
  end
  else
  begin
    l := TStringList.Create;
    //  tbsRequest
    //    version
    //l.Add(WriteExplicit(WriteInteger(0)));
    //    requestorName
    //l.Add(WriteRequestorName(FRequestorName));
    l.Add(WriteRequestList(CertStorage));
    if Length(FNonce) > 0 then
      l.Add(WritePrimitive($A2, WriteNonce(FNonce)));
    b := WriteSequence(l);
    l.Clear;
    l.Add(b);
    Request := BytesOfString(WriteSequence(l));
    l.Free;
    result := 0;
  end;
end;

procedure TElOCSPClient.HandleASN1Read(Sender : TObject; Buffer : pointer; var Size : longint);
begin
  if Size > Length(FReplyBuf) then
  begin
    Move(FReplyBuf[0], Buffer^, Length(FReplyBuf));
    Size := Length(FReplyBuf);
    SetLength(FReplyBuf, 0);
  end
  else
  begin
    Move(FReplyBuf[0], Buffer^, Size);
    FReplyBuf := {$ifndef FPC}Copy{$else}SBCopy{$endif}(FReplyBuf, Size, Length(FReplyBuf));
  end;
end;

procedure TElOCSPClient.HandleASN1Tag(Sender : TObject; TagType: asn1TagType;
  TagConstrained: boolean; Tag: pointer; TagSize: integer; Size: integer; Data: pointer;
  BitRest: integer; var Valid : boolean);
var
  s: BufferType;
begin
  if not Assigned(Tag) then
  begin
    Dec(FNesting);
    Exit;
  end;
  case FParseState of
    0:
      if PByte(Tag)^ = asn1Sequence then
        inc(FParseState)
      else
        FParseState := -1;
    1:
      if (PByte(Tag)^ = asn1Enumerated) and (Size = 1) then
      begin
        FRespStatus := TElOCSPServerError(PByte(Data)^);
        inc(FParseState);
      end
      else
        FParseState := -1;
    2:
      if PByte(Tag)^ = 0 then
        inc(FParseState)
      else
        FParseState := -1;
    3:
      if PByte(Tag)^ = asn1Sequence then
        inc(FParseState)
      else
        FParseState := -1;
    4:
      if PByte(Tag)^ = asn1Object then
      begin
        if Size = 9 then
        begin
          SetLength(s, Size);
          Move(Data^, s[1], Size);
          if s <> #$2B#$06#$01#$05#$05#$07#$30#$01#$01 then
            FParseState := -1
          else
            inc(FParseState);
        end
        else
          FParseState := -1;
      end
      else
        FParseState := -1;
    5:
      if PByte(Tag)^ = asn1OctetStr then
      begin
        SetLength(FReplyBuf2, Size);
        Move(Data^, FReplyBuf2[0], Size);
        inc(FParseState);
      end
      else
        FParseState := -1;
    6: ;
    -1:
      raise EOCSPParseError.Create('server response parsing failure');
  end;
  if FParseState = -1 then
    FRespStatus := oseMalformedRequest;
  if (TagConstrained) then
    Inc(FNesting);
end;

(*
function GeneralizedTimeToDateTime(T: BufferType): {$ifndef DELPHI_NET}TDateTime{$else}DateTime{$endif};
var
  Year, Day, Month, Hour, Min, Sec: word;
begin
  try
    {$ifndef DELPHI_NET}
    Year := StrToInt(copy(T, 1, 4));
    Month := StrToInt(copy(T, 5, 2));
    Day := StrToInt(copy(T, 7, 2));
    Hour := StrToInt(copy(T, 9, 2));
    Min := StrToInt(copy(T, 11, 2));
    Sec := StrToInt(copy(T, 13, 2));
    {$else}

    Year := StrToInt(copy(T, 1, 4));
    Month := StrToInt(copy(T, 5, 2));
    Day := StrToInt(copy(T, 7, 2));
    Hour := StrToInt(copy(T, 9, 2));
    Min := StrToInt(copy(T, 11, 2));
    Sec := StrToInt(copy(T, 13, 2));
    {$endif}
  {$ifndef DELPHI_NET}
    if T[15] <> 'Z' then
      raise EConvertError.Create('');
    Result := EncodeDate(Year, Month, Day) + EncodeTime(Hour, Min, Sec, 0);
  except
    Result := 0;
  {$else}
    if T[15] <> byte('Z') then
      raise EConvertError.Create('');
    Result := Borland.Delphi.System.TDateTime.EncodeDateTime(Year, Month, Day, Hour, Min, Sec, 0);
  except
    Result := DateTime.Create(0);
  {$endif}
  end;
end;
*)

procedure TElOCSPClient.HandleASN1Tag2(Sender : TObject; TagType: asn1TagType;
  TagConstrained: boolean; Tag: pointer; TagSize: integer; Size: integer; Data: pointer;
  BitRest: integer; var Valid : boolean);
var
  s: BufferType;
begin
  if not Assigned(Tag) then
  begin
    if (FParseState = 5) and (FNesting = 3) then
      inc(FParseState);
    Dec(FNesting);
    Exit;
  end;
  while true do
  begin
(*
  write('{s:', FParseState, '/s2:', FParseState2, '/n:', FNesting, '/t:', integer(TagType), '}');
  writeln('.', StringOfChar('-', FNesting * 2),
    {$ifdef DELPHI_NET}
    Tag[0]
    {$else}
    PByte(Tag)^
    {$endif}
    , '-> ', Size, ' [', FNesting, ']');
*)
    case FParseState of
      0: // BasicOCSPResponse
        if PByte(Tag)^ = asn1Sequence then
          inc(FParseState)
        else
          FParseState := -1;
      1: // ResponseData
        if PByte(Tag)^ = asn1Sequence then
          inc(FParseState)
        else
          FParseState := -1;
      2:
        if (PByte(Tag)^ = asn1GeneralizedTime)
          and (FNesting <= 2) then
          inc(FParseState);
      3: // responses
        if PByte(Tag)^ = asn1Sequence then
        begin
          inc(FParseState);
          FParseCert := 0;
        end
        else
          FParseState := -1;
      4: // SingleResponse
        if (PByte(Tag)^ = asn1Sequence) and
          (FNesting = 3) then
        begin
          inc(FParseState);
          FParseState2 := 0;
          FThisUpdate[FParseCert] := 0;
          FNextUpdate[FParseCert] := 0;
          FRevocationTime[FParseCert] := 0;
          FRevocationReason[FParseCert] := TSBCRLReasonFlag(0);
          FCertStatus[FParseCert] := TElOCSPCertificateStatus(0);
        end
        else
          FParseState := -1;
      5:
        begin
          if FNesting < 4 then
          begin
            inc(FParseCert);
            FParseState := 4;
          end
          else
          case FParseState2 of
            0: // certID
              inc(FParseState2);
            1: // certID
              if FNesting < 5 then
              begin
                inc(FParseState2);
                continue;
              end;
            2: // certStatus
              if (TagType <> asn1tSpecific) or
                not (PByte(Tag)^ in [0, 1, 2]) then
                FParseState := -1
              else
              begin
                FCertStatus[FParseCert] :=
                  TElOCSPCertificateStatus(PByte(Tag)^);
                if integer(FCertStatus[FParseCert]) = 1 then
                  inc(FParseState2)
                else
                  inc(FParseState2, 3);
              end;
            3: // revoked 0
              if PByte(Tag)^ = asn1Sequence then
                inc(FParseState2)
              else
              begin
                inc(FParseState2, 2);
                continue;
              end;
            4: // revoked 1
              if FNesting < 5 then
              begin
                inc(FParseState2, 2);
                continue;
              end
              else
              if PByte(Tag)^ = asn1GeneralizedTime then
              begin
                SetLength(s, Size);
                Move(Data^, s[1], Size);
                FRevocationTime[FParseCert] := GeneralizedTimeToDateTime((s));
              end
              else
              if (TagType <> asn1tSpecific) and
                (PByte(Tag)^ = 0) then
              begin
                SetLength(s, Size);
                Move(Data^, s[1], Size);
                FRevocationTime[FParseCert] := GeneralizedTimeToDateTime((s));
              end
              else
              if PByte(Tag)^ = asn1Enumerated then
              begin
                if Size = 1 then
                  FRevocationReason[FParseCert] := 
                    TSBCRLReasonFlag(PByte(Data)^)
                else
                  FParseState := -1;
              end
              else
              begin
                inc(FParseState2, 2);
                continue;
              end;
            5: // thisUpdate
              if PByte(Tag)^ = asn1GeneralizedTime then
              begin
                SetLength(s, Size);
                Move(Data^, s[1], Size);
                FThisUpdate[FParseCert] := GeneralizedTimeToDateTime((s));
                inc(FParseState2);
              end;
            6: // is nextUpdate?
              if (TagType <> asn1tSpecific) and
                (PByte(Tag)^ = 0) then
                inc(FParseState2)
              else
                inc(FParseState2, 2);
            7: // nextUpdate
              if PByte(Tag)^ = asn1GeneralizedTime then
              begin
                SetLength(s, Size);
                Move(Data^, s[1], Size);
                FNextUpdate[FParseCert] := GeneralizedTimeToDateTime((s));
                inc(FParseState2);
              end;
            8:;
          end;
        end;
      6: 
        if (PByte(Tag)^ = 1) and
          (TagType = asn1tSpecific) then
        begin
          inc(FParseState);
          FParseState2 := 0;
        end
        else
        begin
          inc(FParseState, 2);
          continue;
        end;
      7: // extensions
        while true do
        begin
          case FParseState2 of
            0: // extensions
              if PByte(Tag)^ = asn1Sequence then
                inc(FParseState2)
              else
                FParseState := -1;
            1: // extension
              if PByte(Tag)^ = asn1Sequence then
                inc(FParseState2)
              else
                FParseState := -1;
            2: 
              if PByte(Tag)^ = asn1Object then
              begin
                SetLength(s, Size);
                Move(Data^, s[1], Size);
                if s = #$2B#$06#$01#$05#$05#$07#$30#$01#$02 then
                  inc(FParseState2)
                else
                  inc(FParseState2, 2);
              end
              else
                FParseState := -1;
            3:
              if PByte(Tag)^ = asn1OctetStr then
              begin
                SetLength(s, Size);
                Move(Data^, s[1], Size);
//                writeln(length(fnonce), ' - ', Length(s));
                if FNonce <> s then
                begin
                  FRespStatus := oseMalformedRequest;
                  FParseState := -1;
                end;
                FParseState2 := 4;
              end
              else
                FParseState := -1;
            4:
              if FNesting < 4 then
                inc(FParseState)
              else
              if FNesting < 5 then
              begin
                FParseState2 := 1;
                continue;
              end;
          end;
          break;
        end;
      8: ;
      -1:
        raise EOCSPParseError.Create('server response parsing failure');
    end;
    break;
  end;
  if FParseState = -1 then
    FRespStatus := oseMalformedRequest;
  if (TagConstrained) then
    Inc(FNesting);
end;

function TElOCSPClient.ProcessReply(const Reply : ByteArray; var ServerResult : TElOCSPServerError): integer;
var
  Count : integer;
begin
  result := 0;

  if (Length(Reply) = 0) then
  begin
    result := SB_OCSP_ERROR_WRONG_DATA;
  end;

  if (CertStorage = nil) or (CertStorage.Count = 0) then
  begin
    result := SB_OCSP_ERROR_NO_CERTIFICATES;
    Count := 0;
  end
  else
  begin
    Count := CertStorage.Count;
  end;

  SetLength(FThisUpdate, Count);
  SetLength(FNextUpdate, Count);
  SetLength(FRevocationTime, Count);
  SetLength(FRevocationReason, Count);
  SetLength(FCertStatus, Count);

  FReplyBuf := Reply;
  FASN1Parser := TElASN1Parser.Create;
  FASN1Parser.OnRead := HandleASN1Read;
  FASN1Parser.OnTag := HandleASN1Tag;
  FNesting := 0;
  FParseState := 0;
  FRespStatus := oseInternalError;
  try
    FASN1Parser.Parse;
  except
  end;
  FNesting := 0;
  FParseState := 0;
  FReplyBuf := FReplyBuf2;
  FASN1Parser.OnTag := HandleASN1Tag2;
  try
    FASN1Parser.Parse;
  except
  end;

  FreeAndNil(FASN1Parser);
  ServerResult := FRespStatus;
end;

procedure TElOCSPClient.SetCertStorage(Value : TElCustomCertStorage);
begin
  FCertStorage := Value;
  if FCertStorage <> nil then
    FCertStorage.FreeNotification(Self);
end;

function TElOCSPClient.GetCertStatus(Index: Integer): TElOCSPCertificateStatus;
begin
  if (index < 0) or (index > Length(FCertStatus)) then
    result := csUnknown
  else
    result := FCertStatus[index];
end;

function TElOCSPClient.GetThisUpdate(Index: Integer): TDateTime;
begin
  if (index < 0) or (index > Length(FThisUpdate)) then
    result := 0
  else
    result := FThisUpdate[index];
end;

function TElOCSPClient.GetNextUpdate(Index: Integer): TDateTime;
begin
  if (index < 0) or (index > Length(FNextUpdate)) then
    result := 0
  else
    result := FNextUpdate[index];
end;

procedure TElOCSPClient.Notification(AComponent : TComponent; AOperation :
  TOperation);
begin
  inherited;
  if (AComponent = FCertStorage) and (AOperation = opRemove) then
    CertStorage := nil;
end;

function TElOCSPClient.GetRevocationTime(Index: Integer): TDateTime;
begin
  if (index < 0) or (index > Length(FRevocationTime)) then
    result := 0
  else
    result := FRevocationTime[index];
end;

function TElOCSPClient.GetRevocationReason(Index: Integer): TSBCRLReasonFlag;
begin
  if GetCertStatus(Index) = csRevoked then
    result := FRevocationReason[index]
  else
    result := rfUnspecified;
end;

end.

