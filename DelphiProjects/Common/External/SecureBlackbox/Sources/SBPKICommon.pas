(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBPKICommon;

interface



type

  TSBPKIStatus = (psGranted, psGrantedWithMods, psRejection, psWaiting,
    psRevocationWarning, psRevocationNotification, psKeyUpdateWarning);

  TSBPKIFailureInfo = (pfiBadAlg, pfiBadMessageCheck, pfiBadRequest,
    pfiBadTime, pfiBadCertId, pfiBadDataFormat, pfiWrongAuthority,
    pfiIncorrectData, pfiMissingTimeStamp, pfiBadPOP);

implementation

end.
