(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBWinCertStorage;

interface

uses
  SBCustomCertStorage,
  Classes,
  SysUtils,
  SBX509,
  SBWinCrypt,
  SBUtils,
  SBSharedResource,
  SBMSKeyBlob
  ;

{$WARNINGS OFF}
type
  TSBStorageType =  
    (stSystem, stRegistry, stLDAP);
  
  TSBStorageAccessType =  
    (atCurrentService, atCurrentUser, atCurrentUserGroupPolicy,
     atLocalMachine, atLocalMachineEnterprise, atLocalMachineGroupPolicy,
     atServices, atUsers);
    
  TSBStorageProviderType =  
    (ptDefault, ptBaseDSSDH, ptBaseDSS, ptBase, ptRSASchannel,
     ptRSASignature, ptEnhancedDSSDH, ptEnhancedRSAAES, ptEnhanced, ptBaseSmartCard,
     ptStrong);

  TElWinCertStorage = class(TElCustomCertStorage)
  private
    FStores : TStringList;
    FPhysicalStores : TStringList;
    FStorageType : TSBStorageType;
    FAccessType : TSBStorageAccessType;
    FProvider : TSBStorageProviderType;
    FList : TList;
    FCtxList : TList;
    FStoreIndexes : TList;
    FSystemStoresCtx : array of pointer;
    procedure SetPhysicalStores(const Value: TStringList);
    procedure SetStores(const Value: TStringList);
    procedure SetStorageType(Value: TSBStorageType);
    procedure SetAccessType(Value: TSBStorageAccessType);
    class function SetupAccessRights(Access : TSBStorageAccessType) : cardinal;
    class function GetProviderString(Prov : TSBStorageProviderType) : string;
    class function GetProviderType(Prov : TSBStorageProviderType; Alg : integer) : DWORD;
  protected
    function GetCount : integer; override;
    function GetCertificates(Index : integer) : TElX509Certificate; override;
    procedure Open;
    function OpenRegistryStore(const Name : string; UserRights : cardinal) : HCERTSTORE;
    function OpenLDAPStore(const Name : string; UserRights: cardinal) : HCERTSTORE;
    procedure ClearInfo;
    procedure HandleStoresChange(Sender : TObject);
    function LoadPrivateKey(Cert : TElX509Certificate; Key : HCRYPTKEY) : boolean;
    procedure SetPrivateKeyForCertificate(Context : PCCERT_CONTEXT; Cert : TElX509Certificate;
      Exportable : boolean = true; Protected : boolean = true);

    
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    
    class procedure GetAvailableStores(Stores: TStrings;
      AccessType: TSBStorageAccessType = atCurrentUser);
      
    class procedure GetAvailablePhysicalStores(SystemStore: string;
      Stores: TStrings; 
      AccessType: TSBStorageAccessType = atCurrentUser);

    {$ifndef NET_CF}
    class function GetStoreFriendlyName(StoreName : string) : string;
    {$endif}

    procedure Add(Certificate: TElX509Certificate; CopyPrivateKey: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}); overload; override;
    procedure Add(Certificate: TElX509Certificate; StoreName: string;
        CopyPrivateKey: boolean {$ifdef HAS_DEF_PARAMS}= false{$endif};
        Exportable: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}; 
        Protected: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}); reintroduce; overload;
    procedure Remove(Index: integer); override;
    procedure Refresh;
    procedure CreateStore(const StoreName: string);
    procedure DeleteStore(const StoreName: string);
    property Count: integer read GetCount; 
    property Certificates[Index: integer]: TElX509Certificate read GetCertificates; 
  published
    property SystemStores : TStringList read FStores write SetStores;
    property PhysicalStores : TStringList read FPhysicalStores write SetPhysicalStores;
    property StorageType : TSBStorageType read FStorageType write SetStorageType default stSystem;
    property AccessType : TSBStorageAccessType read FAccessType write SetAccessType default atCurrentUser;
    property Provider : TSBStorageProviderType read FProvider write FProvider default ptDefault;
  end;
{$WARNINGS ON}

procedure Register;

implementation

uses Windows;

resourcestring

  SCertificateAlreadyExists = 'Certificate already exists';
  SFailedToGetPrivateKey    = 'Failed to retrieve certificate private key';
  SWin32Error               = 'Win32 error';
  SInvalidPrivateKey        = 'Invalid private key';
  SFailedToAddCertificate   = 'Failed to add certificate to Windows storage due to error ';
  SIndexOutOfBounds         = 'Index out of bounds';
  SUnableToDeleteCertificate= 'Unable to delete certificate';

procedure Register;
begin
  RegisterComponents('PKIBlackbox', [TElWinCertStorage]);
end;

constructor TElWinCertStorage.Create(Owner : TComponent);
begin
  inherited;
  FStores := TStringList.Create;
  FPhysicalStores := TStringList.Create;
  FStores.OnChange := HandleStoresChange;
  FPhysicalStores.OnChange := HandleStoresChange;
  FList := TList.Create;
  FCtxList := TList.Create;
  FStoreIndexes := TList.Create;
  FStorageType := stSystem;
  FAccessType := atCurrentUser;
  FProvider := ptDefault;
end;


destructor TElWinCertStorage.Destroy;
begin
  begin
    ClearInfo;
    FCtxList.Free;
    FStores.Free;
    FPhysicalStores.Free;
    FList.Free;
    FStoreIndexes.Free;
    inherited;
  end
end;


procedure TElWinCertStorage.ClearInfo;
var
  I : integer;
begin
  FSharedResource.WaitToWrite;
  for I := 0 to FList.Count - 1 do
  begin
    if Assigned(FList[I]) then
      TElX509Certificate(FList[I]).Free;
    if Assigned(FCtxList[I]) then
      CertFreeCertificateContext(FCtxList[I]);
  end;
  for I := 0 to Length(FSystemStoresCtx) - 1 do
    CertCloseStore(FSystemStoresCtx[I], 0);
  SetLength(FSystemStoresCtx, 0);
  FList.Clear;
  FCtxList.Clear;
  FStoreIndexes.Clear;
  FRebuildChains := true;
  FSharedResource.Done;
end;

procedure TElWinCertStorage.HandleStoresChange(Sender : TObject);
begin
  if not (csDesigning in ComponentState) then
    Open;
end;

procedure TElWinCertStorage.Add(Certificate: TElX509Certificate; StoreName: string; 
    CopyPrivateKey: boolean {$ifdef HAS_DEF_PARAMS}= false{$endif};
    Exportable: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}; 
    Protected: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif});
var
  hSystemStore : pointer;
  Cert : PCCERT_CONTEXT;
  ErrCode : Cardinal;
  Size : word;
  Rights: cardinal;
  WideStr : PWideChar;
  Len : integer;
begin
  CheckLicenseKey();
  FRebuildChains := true;
  Rights := SetupAccessRights(FAccessType);
  Len := Length(StoreName) shl 1 + 4;
  GetMem(WideStr, Len);
  try
    StringToWideChar(StoreName, WideStr, Len);
    hSystemStore := CertOpenStore(PChar(CERT_STORE_PROV_SYSTEM), X509_ASN_ENCODING or
      PKCS_7_ASN_ENCODING, 0, Rights, WideStr);
  finally
    FreeMem(WideStr);
  end;

  if hSystemStore = nil then
    hSystemStore := CertOpenSystemStore(0, PChar(StoreName));

  if hSystemStore <> nil then
  begin
///    FSharedResource.WaitToWrite;
    if not CertAddEncodedCertificateToStore(hSystemStore, X509_ASN_ENCODING,
      PByte(Certificate.CertificateBinary), Certificate.CertificateSize,
      CERT_STORE_ADD_NEW, Cert) then
    begin
      ErrCode := GetLastError;
      CertCloseStore(hSystemStore, 0);
      if ErrCode = CRYPT_E_EXISTS then
        raise EDuplicateCertError.Create(SCertificateAlreadyExists, integer(CRYPT_E_EXISTS){$ifndef FPC}, 0{$endif})
      else
        raise ECertStorageError.Create(SFailedToAddCertificate, ErrCode{$ifndef FPC}, 0{$endif});
    end
    else
    try
      if CopyPrivateKey then
      begin
        if Certificate.PrivateKeyExists then
        begin
          Size := 0;
          Certificate.SaveKeyToBuffer(nil, Size);
          if Size = 0 then
            raise ECertStorageError.Create(SFailedToGetPrivateKey);
          SetPrivateKeyForCertificate(Cert, Certificate, Exportable, Protected);
        end;
      end;
      Certificate.StorageName := StoreName;
    finally
      CertFreeCertificateContext(Cert);
      CertCloseStore(hSystemStore, 0);
    end;
  end;
  HandleStoresChange(FStores);
end;

procedure TElWinCertStorage.Add(Certificate: TElX509Certificate;
    CopyPrivateKey: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif});
begin
  if FStores.Count = 0 then
    Add(Certificate, 'ROOT', CopyPrivateKey{$ifndef HAS_DEF_PARAMS}, true, true{$endif})
  else
    Add(Certificate, FStores[0], CopyPrivateKey{$ifndef HAS_DEF_PARAMS}, true, true{$endif});
end;

function TElWinCertStorage.GetCount: integer;
begin
  Result := FCtxList.Count;
end;

function TElWinCertStorage.GetCertificates(Index: integer): TElX509Certificate;
var
  pDesiredCert : PCCERT_CONTEXT;
  Buffer : pointer;
  ProvInfo : PCRYPT_KEY_PROV_INFO;
  Prov : HCRYPTPROV;
  ProvName, ContName : PChar;
  Key : HCRYPTKEY;
  LenProvName, LenContName : integer;
  Sz : DWORD;
  ErrCode : boolean;
  I, J, K, Ind : integer;
begin
  CheckLicenseKey();
  FSharedResource.WaitToRead;
  if Index >= FList.Count then
  begin
    Result := nil;
    FSharedResource.Done;
    Exit;
  end;
  if (Index < FList.Count) then
  begin
    Result := TElX509Certificate(FList[Index]);
    if Result <> nil then
    begin
      FSharedResource.Done;
      Exit;
    end;
  end;
  Result := TElX509Certificate.Create(nil);
  Result.CertStorage := Self;
  Ind := integer(FStoreIndexes[Index]);
  I := 0;
  Result.StorageName := '';
  if (FPhysicalStores.Count = 0) and (Ind < FStores.Count) and (Ind >= 0) then
    Result.StorageName := FStores.Strings[Ind]
  else
  begin
    for J := 0 to FStores.Count - 1 do
    begin
      for K := 0 to FPhysicalStores.Count - 1 do
      begin
        if I = Ind then
        begin
          Result.StorageName := FStores.Strings[J] + '\' +
            FPhysicalStores.Strings[K];
          Break;
        end;
        Inc(I);
      end;
      if I = Ind then
        Break;
    end;
  end;
  pDesiredCert := FCtxList[Index];
  Result.LoadFromBuffer(pDesiredCert^.pbCertEncoded, pDesiredCert.cbCertEncoded);
  FList[Index] := Result;
  Result.BelongsTo := BT_WINDOWS;
  { Reading the private key existence }
  Sz := 0;
  CertGetCertificateContextProperty(pDesiredCert, CERT_KEY_PROV_INFO_PROP_ID, nil, @Sz);
  GetMem(Buffer, Sz);
  try
    if CertGetCertificateContextProperty(pDesiredCert, CERT_KEY_PROV_INFO_PROP_ID, Buffer, @Sz) then
    begin
      ProvInfo := PCRYPT_KEY_PROV_INFO(Buffer);
      LenProvName := WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszProvName, -1, nil, 0, nil, nil);
      LenContName := WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszContainerName, -1, nil, 0, nil, nil);
      GetMem(ProvName, LenProvName);
      GetMem(ContName, LenContName);
      try
        WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszProvName, -1, ProvName, LenProvName, nil, nil);
        WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszContainerName, -1, ContName, LenContName, nil, nil);
        if CryptAcquireContext(@Prov, ContName, ProvName, ProvInfo.dwProvType, ProvInfo.dwFlags) then
        begin
          ErrCode := CryptGetUserKey(Prov, AT_SIGNATURE, @Key) or CryptGetUserKey(Prov, AT_KEYEXCHANGE, @Key);
          CryptReleaseContext(Prov, 0);
          Result.PrivateKeyExists := ErrCode;
        end;
      finally
        FreeMem(ProvName);
        FreeMem(ContName);
      end;
    end;
  finally
    FreeMem(Buffer);
  end;
  FSharedResource.Done;
end;

procedure TElWinCertStorage.Remove(Index : integer);
var
  pDesiredCert : PCCERT_CONTEXT;
  pCertCopy : PCCERT_CONTEXT;
begin
  CheckLicenseKey();
  if (Index >= FList.Count) or (Index < 0) then
    raise ECertStorageError.Create(SIndexOutOfBounds);

  pDesiredCert := FCtxList[Index];
  pCertCopy := CertDuplicateCertificateContext(pDesiredCert);
  if not (CertDeleteCertificateFromStore(pCertCopy) ) then
    raise ECertStorageError.Create(SUnableToDeleteCertificate)
  else
  begin
    CertFreeCertificateContext(FCtxList[Index]);
    FCtxList.Delete(Index);
    if Assigned(FList[Index]) then
      TElX509Certificate(FList[Index]).Free;
    FList.Delete(Index);
    FStoreIndexes.Delete(Index);
  end;
end;

procedure TElWinCertStorage.Open;
var
  hSystemStore : pointer;
  pDesiredCert : PCCERT_CONTEXT;
  pContextCopy : PCCERT_CONTEXT;
  StrName : PWideChar;
  Len : integer;
  I, J : integer;
  Lst : TStringList;
  Rights : cardinal;
begin
  CheckLicenseKey();
  ClearInfo;
  Rights := SetupAccessRights(FAccessType);
  FSharedResource.WaitToWrite;
  pDesiredCert := nil;
  if (FPhysicalStores.Count = 0) or
     (FStorageType in [stRegistry, stLDAP]) then
  begin
    SetLength(FSystemStoresCtx, FStores.Count);
    for I := 0 to FStores.Count - 1 do
    begin
      if FStorageType = stSystem then
      begin
//        hSystemStore := CertOpenSystemStore(0, PChar(FStores[i]));

        Len := Length(FStores.Strings[I]) * 2 + 4;
        GetMem(StrName, Len);
        try
          try
            StringToWideChar(FStores.Strings[I], StrName, Len);
            hSystemStore := CertOpenStore(PChar(CERT_STORE_PROV_SYSTEM), X509_ASN_ENCODING,
              0, {CERT_SYSTEM_STORE_CURRENT_USER}Rights, StrName);
          finally
            FreeMem(StrName);
          end;
        except
          hSystemStore := nil;
        end;
      end
      else if FStorageType = stRegistry then
      begin
        hSystemStore := OpenRegistryStore(FStores[I], Rights);
      end
      else if FStorageType = stLDAP then
      begin
        hSystemStore := OpenLDAPStore(FStores[I], Rights);
      end
      else
        hSystemStore := nil;

      FSystemStoresCtx[I] := hSystemStore;
      if hSystemStore <> nil then
      begin
        pDesiredCert := CertEnumCertificatesInStore(hSystemStore, nil);
        while pDesiredCert <> nil do
        begin
          pContextCopy := CertDuplicateCertificateContext(pDesiredCert);
          FCtxList.Add(pContextCopy);
          FStoreIndexes.Add(pointer(I));
          pDesiredCert := CertEnumCertificatesInStore(hSystemStore, pDesiredCert);
        end;
      end;
    end;
  end
  else
  begin
    SetLength(FSystemStoresCtx, FStores.Count * FPhysicalStores.Count);
    Lst := TStringList.Create;
    for I := 0 to FStores.Count - 1 do
      for J := 0 to FPhysicalStores.Count - 1 do
        Lst.Add(FStores.Strings[I] + '\' + FPhysicalStores.Strings[J]);
    for I := 0 to Lst.Count - 1 do
    begin
      Len := Length(Lst.Strings[I]) * 2 + 4;
      GetMem(StrName, Len);
      try
        StringToWideChar(Lst.Strings[I], StrName, Len);
        hSystemStore := CertOpenStore(PChar(CERT_STORE_PROV_PHYSICAL), X509_ASN_ENCODING,
          0, {CERT_SYSTEM_STORE_CURRENT_USER}Rights, StrName);
        FSystemStoresCtx[I] := hSystemStore;
        if Assigned(hSystemStore) then
        begin
          pDesiredCert := CertEnumCertificatesInStore(hSystemStore, nil);
          while pDesiredCert <> nil do
          begin
            pContextCopy := CertDuplicateCertificateContext(pDesiredCert);
            FCtxList.Add(pContextCopy);
            FStoreIndexes.Add(pointer(I));
            pDesiredCert := CertEnumCertificatesInStore(hSystemStore, pDesiredCert);
          end;
        end;
      finally
        FreeMem(StrName);
      end;
    end;
    Lst.Free;
  end;
  if Assigned(pDesiredCert) then
    CertFreeCertificateContext(pDesiredCert);
  FList.Count := FCtxList.Count;
  for I := 0 to FList.Count - 1 do
    FList[I] := nil;
  FRebuildChains := true;
  FSharedResource.Done;
end;


function CBF(pvSystemStore: Pointer; dwFlags: DWORD; pStoreInfo:
  PCERT_SYSTEM_STORE_INFO; pvReserved: Pointer; pvArg: Pointer): BOOL; stdcall;
begin
  TStringList(pvArg).Add(WideCharToString(pvSystemStore));
  Result := true;
end;

class procedure TElWinCertStorage.GetAvailableStores(Stores: TStrings;
  AccessType: TSBStorageAccessType = atCurrentUser);  
var
  ModuleHandle: HMODULE;
  P: pointer;
  Flag: boolean;
  Rights : cardinal;
begin
  Stores.BeginUpdate;
  try
    Stores.Clear;
    Rights := SetupAccessRights(AccessType);
    Flag := true;
    ModuleHandle := GetModuleHandle(PChar('crypt32.dll'));
    if ModuleHandle = 0 then
    begin
      ModuleHandle := LoadLibrary(PChar('crypt32.dll'));
      if ModuleHandle = 0 then
      begin
        Flag := false;
      end
      else
      begin
        P := GetProcAddress(ModuleHandle, PChar('CertEnumSystemStore'));
        if not Assigned(P) then
          Flag := false;
      end;
    end;
    if (not Flag) or (not CertEnumSystemStore(Rights{CERT_SYSTEM_STORE_CURRENT_USER}, nil, Stores, CBF)) then
    begin
      Stores.Add('ROOT');
      Stores.Add('CA');
      Stores.Add('MY');
      Stores.Add('SPC');
    end;
  finally
    Stores.EndUpdate;
  end;
end;

function CBFPhysical(pvSystemStore: pointer; dwFlags : DWORD; pwszStoreName : PChar;
  pStoreInfo : PCERT_SYSTEM_STORE_INFO; pvReserver : pointer; pvArg : pointer) : BOOL; stdcall;
begin
  TStringList(pvArg).Add(PWideChar(pwszStoreName));
  Result := true;
end;

class procedure TElWinCertStorage.GetAvailablePhysicalStores(SystemStore : string;
  Stores : TStrings; AccessType : TSBStorageAccessType = atCurrentUser);
var
  S : PWideChar;
  Sz : integer;
  Rights : cardinal;
begin
  Stores.BeginUpdate;
  try
    Stores.Clear;
    Rights := SetupAccessRights(AccessType);
    Sz := Length(SystemStore) * 2 + 4;
    GetMem(S, Sz);
    StringToWideChar(SystemStore, S, Sz);
    CertEnumPhysicalStore(S, Rights{CERT_SYSTEM_STORE_CURRENT_USER},
      Stores, CBFPhysical);
    FreeMem(S);
  finally
    Stores.EndUpdate;
  end;
end;

function TElWinCertStorage.LoadPrivateKey(Cert : TElX509Certificate; Key : HCRYPTKEY) : boolean;
var
  KeyLen : DWORD;
  KeyBuf, EncKeyBuf : PBYTE;
  LenEnc : integer;
  BT, ErrCode : integer;
begin
  Result := true;
  if Cert = nil then
  begin
    result := false;
    exit;
  end;
  KeyLen := 0;
  CryptExportKey(Key, 0, PRIVATEKEYBLOB, 0, nil, @KeyLen);
  GetMem(KeyBuf, KeyLen);
  try
    if CryptExportKey(Key, 0, PRIVATEKEYBLOB, 0, KeyBuf, @KeyLen) then
    begin
      LenEnc := 0;
      ParseMSKeyBlob(KeyBuf, KeyLen, nil, LenEnc, BT);
      GetMem(EncKeyBuf, LenEnc);
      ErrCode := ParseMSKeyBlob(KeyBuf, KeyLen, EncKeyBuf, LenEnc, BT);
      if ErrCode = 0 then
        Cert.LoadKeyFromBuffer(EncKeyBuf, LenEnc)
      else
        Result := false;
      FreeMem(EncKeyBuf);
    end
    else
      Result := false;
  finally
    FreeMem(KeyBuf);
  end;
end;

{$ifndef NET_CF}
class function TElWinCertStorage.GetStoreFriendlyName(StoreName : string) : string;
var
  Buffer, Dest : PWideChar;
  Size : integer;
begin
  Size := Length(StoreName) shl 1 + 2;
  GetMem(Buffer, Size);
  StringToWideChar(StoreName, Buffer, Size);
  Dest := CryptFindLocalizedName(Buffer);
  if Assigned(Dest) then
    Result := WideCharToString(Dest)
  else
    Result := '';
end;
{$endif}

procedure TElWinCertStorage.SetPhysicalStores(const Value: TStringList);
begin
  FPhysicalStores.Assign(Value);
end;

procedure TElWinCertStorage.SetStores(const Value: TStringList);
begin
  FStores.Assign(Value);
end;

procedure TElWinCertStorage.SetPrivateKeyForCertificate(Context : PCCERT_CONTEXT;
  Cert : TElX509Certificate; Exportable : boolean = true; Protected : boolean = true);
var
  Prov: HCRYPTPROV;
  Key: HCRYPTKEY;
  g : GUID;
  I : integer;
  ProvInfo: CRYPT_KEY_PROV_INFO;
  Size, BlobType : integer;
  Err : longword;
  GuidStr : string;
  Sz : word;

  Buf, BlobBuf : ByteArray;
  ProvType : DWORD;
  Flags : integer;

  Tmp : DWORD;
  ProvStr : string;
  ProvPtr : PChar;
  FlagModifier : DWORD;
begin
  Sz := 0;
  SetLength(Buf, Sz);
  Cert.SaveKeyToBuffer(Buf, Sz);
  if Sz = 0 then
    raise ECertStorageError.Create(SFailedToGetPrivateKey);
  SetLength(Buf, Sz);
  Cert.SaveKeyToBuffer(@Buf[0], Sz);
  SetLength(Buf, Sz);
  Prov := 0;
  CoCreateGuid(@g);
  GuidStr := '{' + IntToHex(g.Data1, 8) + '-' + IntToHex(g.Data2, 4) + '-' +
    IntToHex(g.Data3, 4) + '-' + IntToHex(g.Data4[0], 2) + IntToHex(g.Data4[1], 2) + '-';
  for I := 2 to 7 do
    GuidStr := GuidStr + IntToHex(g.Data4[I], 2);
  GuidStr := GuidStr + '}';
  
  if Cert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
  begin
    //ProvType := PROV_RSA;
    BlobType := SB_KEY_BLOB_RSA;
  end
  else if Cert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    //ProvType := PROV_DSS;
    BlobType := SB_KEY_BLOB_DSS;
  end
  else
    raise ECertStorageError.Create(SFailedToGetPrivateKey);

  ProvStr := GetProviderString(FProvider);
  ProvType := GetProviderType(FProvider, Cert.PublicKeyAlgorithm);
  if Length(ProvStr) > 0 then
  begin
    ProvStr := ProvStr + #0;
    ProvPtr := @ProvStr[1];
  end
  else
    ProvPtr := nil;

  if AccessType in [atLocalMachine, 
                    atLocalMachineEnterprise, 
                    atLocalMachineGroupPolicy,
                    atServices, 
                    atUsers] then
    FlagModifier := CRYPT_MACHINE_KEYSET
  else
    FlagModifier := 0;

  if not CryptAcquireContext(@Prov, PChar(GuidStr), ProvPtr, ProvType, CRYPT_NEWKEYSET or FlagModifier) then
  begin
    err := GetLastError;
    raise ECertStorageError.Create(SWin32Error + ' ' + IntToHex(err, 8));
  end;
  Size := 0;
  SBMSKeyBlob.WriteMSKeyBlob(@Buf[0], Length(Buf), nil, Size, BlobType);
  SetLength(BlobBuf, Size);
  if not SBMSKeyBlob.WriteMSKeyBlob(@Buf[0], Length(Buf), @BlobBuf[0], Size, BlobType) then
    raise ECertStorageError.Create(SInvalidPrivateKey);
  Flags := 0;
  if Exportable then
    Flags := Flags or CRYPT_EXPORTABLE;
  if Protected then
    Flags := Flags or CRYPT_USER_PROTECTED;

  SetLength(BlobBuf, Size);

  if not CryptImportKey(Prov, @BlobBuf[0], Length(BlobBuf), 0,
    Flags, @Key) then
  begin
    err := GetLastError;
    CryptReleaseContext(Prov, 0);
    raise ECertStorageError.Create(SWin32Error + ' ' + IntToHex(err, 8));
  end;
  FillChar(ProvInfo, SizeOf(ProvInfo), 0);
  I := Length(GuidStr) * 2 + 4;
  GetMem(ProvInfo.pwszContainerName, I);
  FillChar(ProvInfo.pwszContainerName^, I, 0);
  MultiByteToWideChar(CP_ACP, 0, PChar(GuidStr), Length(GuidStr), ProvInfo.pwszContainerName,
    I);
  Tmp := 0;
  CryptGetProvParam(Prov, PP_NAME, nil, @Tmp, 0);
  SetLength(GuidStr, Tmp);
  CryptGetProvParam(Prov, PP_NAME, @GuidStr[1], @Tmp, 0);
  SetLength(GuidStr, Tmp);
  I := Length(GuidStr) * 2 + 4;
  GetMem(ProvInfo.pwszProvName, I);
  FillChar(ProvInfo.pwszProvName^, I, 0);
  MultiByteToWideChar(CP_ACP, 0, PChar(GuidStr), Length(GuidStr), ProvInfo.pwszProvName,
    I);
  ProvInfo.dwProvType := ProvType;
  ProvInfo.dwFlags := FlagModifier;
  ProvInfo.cProvParam := 0;
  ProvInfo.rgProvParam := nil;
  ProvInfo.dwKeySpec := AT_KEYEXCHANGE;
  if not CertSetCertificateContextProperty(Context, CERT_KEY_PROV_INFO_PROP_ID, 0, @ProvInfo) then
  begin
    err := GetLastError;
    FreeMem(ProvInfo.pwszProvName);
    FreeMem(ProvInfo.pwszContainerName);
    CryptDestroyKey(Key);
    CryptReleaseContext(Prov, 0);
    raise ECertStorageError.Create(SWin32Error + ' ' + IntToHex(err, 8));
  end;
  FreeMem(ProvInfo.pwszProvName);
  FreeMem(ProvInfo.pwszContainerName);
  CryptDestroyKey(Key);
  CryptReleaseContext(Prov, 0);
end;

procedure TElWinCertStorage.SetStorageType(Value: TSBStorageType);
begin
  if FStorageType <> Value then
  begin
    FStorageType := Value;
    HandleStoresChange(nil);
  end;
end;

procedure TElWinCertStorage.SetAccessType(Value: TSBStorageAccessType);
begin
  if FAccessType <> Value then
  begin
    FAccessType := Value;
    HandleStoresChange(nil);
  end;
end;

function TElWinCertStorage.OpenRegistryStore(const Name : string; UserRights: cardinal) : HCERTSTORE;
var
  SrcKey : HKEY;
  I, Index : integer;
  RestOfKey: string;
const
  RegKeyCount = 5;
  RegStrings : array[0..4] of string = (
    'HKEY_CLASSES_ROOT', 'HKEY_CURRENT_CONFIG',
    'HKEY_CURRENT_USER', 'HKEY_LOCAL_MACHINE', 'HKEY_USERS'
    );

{$ifdef FPC}
var
{$endif}
  RegKeys : array[0..4] of HKEY
          {$ifndef FPC}
          = (
              HKEY_CLASSES_ROOT, HKEY_CURRENT_CONFIG, HKEY_CURRENT_USER,
              HKEY_LOCAL_MACHINE, HKEY_USERS
            )
          {$endif}
    ;
begin
  {$ifdef FPC}
  RegKeys[0] := HKEY_CLASSES_ROOT;
  RegKeys[1] := HKEY_CURRENT_CONFIG;
  RegKeys[2] := HKEY_CURRENT_USER;
  RegKeys[3] := HKEY_LOCAL_MACHINE;
  RegKeys[4] := HKEY_USERS;
  {$endif}
  Index := -1;
  Result := nil;
  for I := 0 to RegKeyCount - 1 do    
    if Pos(RegStrings[I], Name) > 0 then
    begin
      Index := I;
      RestOfKey := Copy(Name, 2 + Length(RegStrings[I]), Length(Name));
      Break;
    end;

  if Index = -1 then
    Exit;
    
  if RegOpenKeyEx(RegKeys[Index], PChar(RestOfKey), 0, KEY_READ, SrcKey) <> ERROR_SUCCESS then
    Exit;
  try
    Result := CertOpenStore(PChar(CERT_STORE_PROV_REG), X509_ASN_ENCODING or PKCS_7_ASN_ENCODING,
      0, UserRights or CERT_STORE_OPEN_EXISTING_FLAG, pointer(SrcKey));
  except
    Result := nil;
  end;
  RegCloseKey(SrcKey);
end;

function TElWinCertStorage.OpenLDAPStore(const Name : string; UserRights : cardinal) : HCERTSTORE;
var
  WideStr : pointer;
  Len : integer;
begin
  Len := Length(Name) shl 1 + 4;
  GetMem(WideStr, Len);
  try
    StringToWideChar(Name, WideStr, Len);
    Result := CertOpenStore(PChar(CERT_STORE_PROV_LDAP), X509_ASN_ENCODING or PKCS_7_ASN_ENCODING,
      0, UserRights or CERT_STORE_OPEN_EXISTING_FLAG, WideStr);
  finally
    FreeMem(WideStr);
  end;
end;

class function TElWinCertStorage.SetupAccessRights(Access : TSBStorageAccessType) : cardinal;
begin
  case Access of
    atCurrentService : Result := CERT_SYSTEM_STORE_CURRENT_SERVICE;
    atCurrentUser : Result := CERT_SYSTEM_STORE_CURRENT_USER;
    atCurrentUserGroupPolicy : Result := CERT_SYSTEM_STORE_CURRENT_USER_GROUP_POLICY;
    atLocalMachine : Result := CERT_SYSTEM_STORE_LOCAL_MACHINE;
    atLocalMachineEnterprise : Result := CERT_SYSTEM_STORE_LOCAL_MACHINE_ENTERPRISE;
    atLocalMachineGroupPolicy : Result := CERT_SYSTEM_STORE_LOCAL_MACHINE_GROUP_POLICY;
    atServices : Result := CERT_SYSTEM_STORE_SERVICES;
    atUsers : Result := CERT_SYSTEM_STORE_USERS;
  else
    Result := 0;
  end;
end;

class function TElWinCertStorage.GetProviderString(Prov : TSBStorageProviderType) : string;
begin
  case Prov of
    ptDefault : Result := '';
    ptBaseDSSDH : Result := MS_DEF_DSS_DH_PROV;
    ptBaseDSS : Result := MS_DEF_DSS_PROV;
    ptBase : Result := MS_DEF_PROV;
    ptRSASchannel : Result := MS_DEF_RSA_SCHANNEL_PROV;
    ptRSASignature : Result := MS_DEF_RSA_SIG_PROV;
    ptEnhancedDSSDH : Result := MS_ENH_DSS_DH_PROV;
    ptEnhancedRSAAES : Result := MS_ENH_RSA_AES_PROV;
    ptEnhanced : Result := MS_ENHANCED_PROV;
    ptBaseSmartCard : Result := MS_SCARD_PROV;
    ptStrong : Result := MS_STRONG_PROV;
  else
    Result := '';
  end;
end;

class function TElWinCertStorage.GetProviderType(Prov : TSBStorageProviderType;
  Alg : integer) : DWORD;
begin
  case Prov of
    ptBaseDSSDH :
      Result := PROV_DSS_DH;
    ptBaseDSS :
      Result := PROV_DSS;
    ptRSASchannel :
      Result := PROV_RSA_SCHANNEL;
    ptRSASignature :
      Result := PROV_RSA_SIG;
    ptEnhancedDSSDH :
      Result := PROV_DSS_DH;
    ptEnhancedRSAAES :
      Result := PROV_RSA_AES;
    else
    begin
      if Alg = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
        Result := PROV_RSA
      else if Alg = SB_CERT_ALGORITHM_ID_DSA then
        Result := PROV_DSS
      else
        Result := PROV_DSS_DH;
    end;
  end;
end;

procedure TElWinCertStorage.Refresh;
begin
  if not (csDesigning in ComponentState) then
    Open;
end;

procedure TElWinCertStorage.CreateStore(const StoreName: string);
var
  hSystemStore : pointer;
  Rights: cardinal;
  WideStr : PWideChar;
  Len : integer;
  Err : integer;
begin
  Rights := SetupAccessRights(FAccessType);
  Len := Length(StoreName) shl 1 + 4;
  GetMem(WideStr, Len);
  try
    StringToWideChar(StoreName, WideStr, Len);
    hSystemStore := CertOpenStore(PChar(CERT_STORE_PROV_SYSTEM), X509_ASN_ENCODING or
      PKCS_7_ASN_ENCODING, 0, Rights or CERT_STORE_CREATE_NEW_FLAG, WideStr);
  finally
    FreeMem(WideStr);
  end;

  if hSystemStore <> nil then
    CertCloseStore(hSystemStore, 0)
  else
  begin
    Err := GetLastError();
      raise ECertStorageError.Create(SWin32Error + ' ' + IntToHex(err, 8));
  end;
end;

procedure TElWinCertStorage.DeleteStore(const StoreName: string);
var
  Rights: cardinal;
  WideStr : PWideChar;
  Len : integer;
  Err : integer;
begin
  Rights := SetupAccessRights(FAccessType);
  Len := Length(StoreName) shl 1 + 4;
  GetMem(WideStr, Len);
  try
    StringToWideChar(StoreName, WideStr, Len);
    CertOpenStore(PChar(CERT_STORE_PROV_SYSTEM), X509_ASN_ENCODING or
      PKCS_7_ASN_ENCODING, 0, Rights or CERT_STORE_DELETE_FLAG, WideStr);
  finally
    FreeMem(WideStr);
  end;

  Err := GetLastError();
  if Err <> 0 then
    raise ECertStorageError.Create(SWin32Error + ' ' + IntToHex(err, 8));
end;

end.
