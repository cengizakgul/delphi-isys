
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSimpleSSH;

interface

uses
  SBUtils,
  {$ifndef CLX_USED}
  Windows,
  Messages,
  {$endif}
  Classes,
  SysUtils,
  {$ifdef SECURE_BLACKBOX_DEBUG}
  SBDumper,
  {$endif}
  SBSSHCommon,
  SBSSHTerm,
  SBSocket,
  SBSSHClient,
  SBSSHConstants,
  SBSSHKeyStorage;

type

  TElSimpleSSHClient = class(TSBControlBase)
  private
      // Socks support
    FSocketTimeout: Integer;
    FSocksAuthentication: TElSocksAuthentication;
    FSocksPassword: string;
    FSocksPort: Integer;
    FSocksResolveAddress: Boolean;
    FSocksServer: string;
    FSocksUserCode: string;
    FSocksVersion: TElSocksVersion;
    FUseSocks: Boolean;
      //  Web tunneling support
    FUseWebTunneling: boolean;
    FWebTunnelAddress: string;
    FWebTunnelAuthentication: TElWebTunnelAuthentication;
    FWebTunnelPassword: string;
    FWebTunnelPort: Integer;
    FWebTunnelUserId: string;
  protected
    FClient     : TElSSHClient;
    FTunnelList : TElSSHTunnelList;
    FTunnel     : TElCustomSSHTunnel;
    FConnection : TElSSHTunnelConnection;
    FOnCloseRaised : boolean;
    FErrorCode : integer;
    FTerminalInfo: TElTerminalInfo;
    FEnvironment: TStringList;
    FDataBuf   : ByteArray;
    FDataLen   : integer;
    FExtDataBuf: ByteArray;
    FExtDataLen: integer;
    FDataReceived : boolean;

    FCommands : TStringList;

    FSocket     : TElSocket;
    FUseInternalSocket  : Boolean;
    FDoUseInternalSocket: boolean;

    FOnCloseConnection: TSSHCloseConnectionEvent;
    FOnAuthenticationFailed: TSSHAuthenticationFailedEvent;
    FOnAuthenticationKeyboard: TSSHAuthenticationKeyboardEvent;
    FOnAuthenticationSuccess: TNotifyEvent;
    FOnBanner: TSSHBannerEvent;
    FOnError: TSSHErrorEvent;
    FOnKeyValidate: TSSHKeyValidateEvent;
    FOnReceive: TSSHReceiveEvent;
    FOnSend: TSSHSendEvent;
    FMessageLoop: TSSHMessageLoopEvent;
    FLastOpTick : integer;

    function IntMessageLoop(NoPeek : boolean): boolean;
    procedure DataAvailable;
    procedure DoAuthenticationFailed(Sender : TObject; AuthenticationType :
        integer);
    procedure DoAuthenticationKeyboard(Sender : TObject; Prompts : TStringList;
        Echo : TBits; Responses : TStringList);
    procedure DoAuthenticationSuccess(Sender : TObject);
    procedure DoCloseConnection(Sender : TObject);
    procedure DoConnClosed(Sender : TObject; CloseType : TSSHCloseType);
    procedure DoError(Sender : TObject; ErrorCode : integer);
    procedure DoKeyValidate(Sender : TObject; ServerKey : TElSSHKey; var Validate :
      boolean);
    procedure DoBanner(Sender: TObject; const Text : string; const Language : string);
    procedure DoClose(Force: boolean);
    procedure DoReceive(Sender : TObject; Buffer : pointer; MaxSize : longint; out
      Written : longint);
    procedure DoSend(Sender : TObject; Buffer : pointer; Size : longint);
    procedure OnTunnelOpen(Sender : TObject; Conn : TElSSHTunnelConnection);
    procedure OnTunnelClose(Sender : TObject; Conn : TElSSHTunnelConnection);
    procedure OnTunnelData(Sender : TObject; Buffer : pointer; Size : integer);
    procedure OnTunnelExtData(Sender : TObject; Buffer : pointer; Size : integer);
    function GetVersions : TSSHVersions;
    procedure SetVersions(Value : TSSHVersions);
    function GetPassword : string;
    procedure SetPassword(Value : string);
    function GetUsername : string;
    procedure SetUsername(Value : string);
    function GetClientHostname : string;
    procedure SetClientHostname(Value : string);
    function GetClientUsername : string;
    procedure SetClientUsername(Value : string);
    function GetSoftwareName : string;
    procedure SetSoftwareName(Value : string);
    function GetForceCompression : boolean;
    procedure SetForceCompression(Value : boolean);
    function GetCompressionLevel : integer;
    procedure SetCompressionLevel(Value : integer);
    function GetAuthenticationTypes : integer;
    procedure SetAuthenticationTypes(Value : integer);
    function GetActive : boolean;
    function GetAddress: string;
    function GetEncryptionAlgorithms(Index : TSSHEncryptionAlgorithm) : boolean;
    procedure SetEncryptionAlgorithms(Index : TSSHEncryptionAlgorithm; Value : boolean);
    function GetCompressionAlgorithms(Index : TSSHCompressionAlgorithm) : boolean;
    procedure SetCompressionAlgorithms(Index : TSSHCompressionAlgorithm; Value : boolean);
    function GetMACAlgorithms(Index : TSSHMacAlgorithm) : boolean;
    procedure SetMACAlgorithms(Index : TSSHMacAlgorithm; Value : boolean);
    function GetKexAlgorithms(Index : TSSHKexAlgorithm) : boolean;
    procedure SetKexAlgorithms(Index : TSSHKexAlgorithm; Value : boolean);
    function GetPublicKeyAlgorithms(Index : TSSHPublicKeyAlgorithm) : boolean;
    procedure SetPublicKeyAlgorithms(Index : TSSHPublicKeyAlgorithm; Value : boolean);
    function GetServerSoftwareName : string;
    function GetVersion : TSSHVersion;
    function GetServerCloseReason : string;
    function GetEncryptionAlgorithmSC : TSSHEncryptionAlgorithm;
    function GetEncryptionAlgorithmCS : TSSHEncryptionAlgorithm;
    function GetCompressionAlgorithmSC : TSSHCompressionAlgorithm;
    function GetCompressionAlgorithmCS : TSSHCompressionAlgorithm;
    function GetMacAlgorithmSC : TSSHMacAlgorithm;
    function GetMacAlgorithmCS : TSSHMacAlgorithm;
    function GetKexAlgorithm : TSSHKexAlgorithm;
    function GetPublicKeyAlgorithm : TSSHPublicKeyAlgorithm;

    function GetCommand : string;
    procedure SetCommand(const Value: string);

    function GetKeyStorage: TElSSHCustomKeyStorage;
    function GetPort: Integer;
    procedure SetKeyStorage(Value: TElSSHCustomKeyStorage);
    procedure SetTerminalInfo(Value: TElTerminalInfo);
    function DoMessageLoop(NoPeek : boolean): boolean;
    procedure SetAddress(const Value: string);
    procedure SetPort(Value: Integer);
    procedure SetUseInternalSocket(Value: Boolean);
    procedure ReleaseTunnel;
    procedure SetSocketTimeout(Value: Integer);
    procedure SetUseSocks(const Value: Boolean);
    procedure SetUseWebTunneling(const Value: boolean);
    function GetErrorString: string;

  protected
    function GetLocalAddress: string;
    function GetLocalPort: Integer;
    procedure SetLocalAddress(const Value: string);
    procedure SetLocalPort(const Value: Integer);
    function GetAuthOrder() : TSBSSHAuthOrder;
    procedure SetAuthOrder(Value : TSBSSHAuthOrder);

  public
    constructor Create(AOwner : TSBComponentBase); override;
    destructor Destroy; override;
    
    procedure Close;
    procedure Open;


    procedure SendData(Buffer : pointer; Size : integer);
    procedure ReceiveData(Buffer : pointer; var Size : integer; StdErrBuffer : pointer; var StdErrSize : integer);
    procedure SendText(const S : string);
    procedure ReceiveText(var Text : string; var StdErrText : string);
    function CanReceive(Timeout : integer): Boolean;

    property Active : boolean read GetActive;
    property EncryptionAlgorithms[Index : TSSHEncryptionAlgorithm] : boolean
      read GetEncryptionAlgorithms write SetEncryptionAlgorithms;
    property CompressionAlgorithms[Index : TSSHCompressionAlgorithm] : boolean
      read GetCompressionAlgorithms write SetCompressionAlgorithms;
    property MacAlgorithms[Index : TSSHMacAlgorithm] : boolean
      read GetMACAlgorithms write SetMACAlgorithms;
    property KexAlgorithms[Index : TSSHKexAlgorithm] : boolean
      read GetKexAlgorithms write SetKexAlgorithms;
    property PublicKeyAlgorithms[Index : TSSHPublicKeyAlgorithm] : boolean
      read GetPublicKeyAlgorithms write SetPublicKeyAlgorithms;
    property ServerSoftwareName : string read GetServerSoftwareName;
    property Version : TSSHVersion read GetVersion;
    property ServerCloseReason : string read GetServerCloseReason;
    property EncryptionAlgorithmServerToClient : TSSHEncryptionAlgorithm
      read GetEncryptionAlgorithmSC;
    property EncryptionAlgorithmClientToServer : TSSHEncryptionAlgorithm
      read GetEncryptionAlgorithmCS;
    property CompressionAlgorithmServerToClient : TSSHCompressionAlgorithm
      read GetCompressionAlgorithmSC;
    property CompressionAlgorithmClientToServer : TSSHCompressionAlgorithm
      read GetCompressionAlgorithmCS;
    property MacAlgorithmServerToClient : TSSHMacAlgorithm read GetMacAlgorithmSC;
    property MacAlgorithmClientToServer : TSSHMacAlgorithm read GetMacAlgorithmCS;
    property KexAlgorithm : TSSHKexAlgorithm read GetKexAlgorithm;
    property PublicKeyAlgorithm : TSSHPublicKeyAlgorithm read GetPublicKeyAlgorithm;
    property KeyStorage: TElSSHCustomKeyStorage read GetKeyStorage write
        SetKeyStorage;
    property TerminalInfo: TElTerminalInfo read FTerminalInfo write SetTerminalInfo;
  published
    property Command : string read GetCommand write SetCommand;
    property Commands : TStringList read FCommands;

    property AuthenticationTypes : integer read GetAuthenticationTypes
      write SetAuthenticationTypes default SSH_AUTH_TYPE_PASSWORD;
    property ClientHostname : string read GetClientHostname
      write SetClientHostname;
    property ClientUsername : string read GetClientUsername
      write SetClientUsername;
    property CompressionLevel : integer read GetCompressionLevel
      write SetCompressionLevel default 9;
    property ForceCompression : boolean read GetForceCompression
      write SetForceCompression;
    property Password : string read GetPassword write SetPassword;
    property SoftwareName : string read GetSoftwareName write SetSoftwareName;
    property Username : string read GetUsername write SetUsername;
    property Versions : TSSHVersions read GetVersions write SetVersions
                   default [sbSSH1, sbSSH2];
    property Address: string read GetAddress write SetAddress;
    property Environment: TStringList read FEnvironment;
    
    property OnAuthenticationKeyboard: TSSHAuthenticationKeyboardEvent read
        FOnAuthenticationKeyboard write FOnAuthenticationKeyboard;
    property OnAuthenticationFailed: TSSHAuthenticationFailedEvent read
        FOnAuthenticationFailed write FOnAuthenticationFailed;
    property OnAuthenticationSuccess: TNotifyEvent read FOnAuthenticationSuccess
        write FOnAuthenticationSuccess;
    property OnBanner: TSSHBannerEvent read FOnBanner write FOnBanner;
    property OnCloseConnection: TSSHCloseConnectionEvent read FOnCloseConnection
        write FOnCloseConnection;
    property OnError: TSSHErrorEvent read FOnError write FOnError;
    property OnKeyValidate: TSSHKeyValidateEvent read FOnKeyValidate write
        FOnKeyValidate;
    property OnReceive: TSSHReceiveEvent read FOnReceive write FOnReceive;
    property OnSend: TSSHSendEvent read FOnSend write FOnSend;
    
    property MessageLoop: TSSHMessageLoopEvent read FMessageLoop write FMessageLoop;
    property Port: Integer read GetPort write SetPort default 22;
    property SocketTimeout: Integer read FSocketTimeout write SetSocketTimeout
        default 0;
    property SocksAuthentication: TElSocksAuthentication read FSocksAuthentication
        write FSocksAuthentication;
    property SocksPassword: string read FSocksPassword write FSocksPassword;
    property SocksPort: Integer read FSocksPort write FSocksPort default 1080;
    property SocksResolveAddress: Boolean read FSocksResolveAddress write
        FSocksResolveAddress default false;
    property SocksServer: string read FSocksServer write FSocksServer;
    property SocksUserCode: string read FSocksUserCode write FSocksUserCode;
    property SocksVersion: TElSocksVersion read FSocksVersion write FSocksVersion
        default elSocks5;
    property UseInternalSocket: Boolean read FUseInternalSocket write
        SetUseInternalSocket default true;
    property UseSocks: Boolean read FUseSocks write SetUseSocks default false;
    //  Web tunneling support
    property UseWebTunneling: boolean read FUseWebTunneling write
        SetUseWebTunneling default false;
    property WebTunnelAddress: string read FWebTunnelAddress write
        FWebTunnelAddress;
    property WebTunnelAuthentication: TElWebTunnelAuthentication read
        FWebTunnelAuthentication write FWebTunnelAuthentication
        default wtaNoAuthentication;
    property WebTunnelPassword: string read FWebTunnelPassword write
        FWebTunnelPassword;
    property WebTunnelPort: Integer read FWebTunnelPort write FWebTunnelPort;
    property WebTunnelUserId: string read FWebTunnelUserId write FWebTunnelUserId;
    property ErrorString : string read GetErrorString;

    property LocalAddress: string read GetLocalAddress write SetLocalAddress;
    property LocalPort: Integer read GetLocalPort write SetLocalPort;
  property SSHAuthOrder: TSBSSHAuthOrder read GetAuthOrder write SetAuthOrder;
  end;
  
  EElSimpleSSHClientException =  class(ESecureBlackboxError);

procedure Register;

implementation

resourcestring

  SConnAlreadyEstablished = 'SSH connection already established';
  SConnNotPresent = 'SSH connection not present';
  SConnLost = 'Connection lost';
  SConnFailed = 'SSH connection failed due to error (%d)';
  SConnError = 'SSH connection error (%d)';
  SConnError2 = 'SSH connection error (%s)';
  SNotConnected = 'SSH component not connected';

procedure Register;
begin
  RegisterComponents('SSHBlackbox', [TElSimpleSSHClient]);
end;

constructor TElSimpleSSHClient.Create(AOwner : TSBComponentBase);
begin
  inherited Create(AOwner);
  FClient := TElSSHClient.Create(nil);
  FTunnelList := TElSSHTunnelList.Create(nil);
  FClient.TunnelList := FTunnelList;
  FClient.OnError := DoError;
  FClient.OnAuthenticationFailed := DoAuthenticationFailed;
  FClient.OnAuthenticationSuccess := DoAuthenticationSuccess;
  FClient.OnAuthenticationKeyboard := DoAuthenticationKeyboard;
  FClient.OnCloseConnection := DoCloseConnection;
  FClient.OnSend := DoSend;
  FClient.OnReceive := DoReceive;
  FClient.OnKeyValidate := DoKeyValidate;
  FClient.OnBanner := DoBanner;

  FEnvironment := TStringList.Create;

  FUseInternalSocket := true;
  
  FSocket := TElSocket.Create;
  FSocket.Port := 22;
  FSocket.Address := '';

  FCommands := TStringList.Create;
end;


destructor TElSimpleSSHClient.Destroy;
begin
  FreeAndNil(FEnvironment);
  FreeAndNil(FClient);
  FreeAndNil(FTunnelList);
  FreeAndNil(FTunnel);
  FreeAndNil(FCommands);
  inherited;
  FreeAndNil(FSocket);
end;



function TElSimpleSSHClient.CanReceive(Timeout : integer): Boolean;
begin
  if (not FDoUseInternalSocket) then
  begin
    Result := (FDataLen > 0) or (FExtDataLen > 0);
  end
  else
  begin
    Result := (FDataLen > 0) or (FExtDataLen > 0);
    if (not Result) {and (Timeout >= 0) }then
    begin
      if FSocket.CanReceive(Timeout) then
        FClient.DataAvailable;
      Result := (FDataLen > 0) or (FExtDataLen > 0);
    end;
  end;
end;

procedure TElSimpleSSHClient.Close;
begin
  if Assigned(FConnection) then
    FConnection.Close(false);

  FClient.Close(false);
  ReleaseTunnel;
  FDataLen := 0;
  SetLength(FDataBuf, 0);
  FExtDataLen := 0;
  SetLength(FExtDataBuf, 0);
end;

procedure TElSimpleSSHClient.DataAvailable;
var Time : integer;
begin
  if (not FDoUseInternalSocket) then
    FClient.DataAvailable
  else
  begin
    if SocketTimeout = 0 then
      Time := -1
    else
      Time := 200;
    if FSocket.CanReceive(Time) then
      FClient.DataAvailable;
  end;
end;

procedure TElSimpleSSHClient.DoAuthenticationFailed(Sender : TObject;
    AuthenticationType : integer);
begin
  if assigned(FOnAuthenticationFailed) then
    FOnAuthenticationFailed(Self, AuthenticationType);
end;

procedure TElSimpleSSHClient.DoAuthenticationKeyboard(Sender : TObject; Prompts
    : TStringList; Echo : TBits; Responses : TStringList);
begin
  if Assigned(FOnAuthenticationKeyboard) then
    FOnAuthenticationKeyboard(Self, Prompts, Echo, Responses);
end;

procedure TElSimpleSSHClient.DoAuthenticationSuccess(Sender : TObject);
begin
  if Assigned(FOnAuthenticationSuccess) then
    FOnAuthenticationSuccess(Self);
end;

procedure TElSimpleSSHClient.DoCloseConnection(Sender : TObject);
begin
  if FDoUseInternalSocket then
    try
      FSocket.Close(false);
    except
    end;
  if assigned(FOnCloseConnection) then
    FOnCloseConnection(Self);
end;

procedure TElSimpleSSHClient.Open;
var
  Index : integer;
  err : integer;
begin
  if FClient.Active then
    raise EElSimpleSSHClientException.Create(SConnAlreadyEstablished);

  {$ifdef SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('ElSimpleSSHClient.Open called. Establishing new connection');
  Dumper.BeginSubsection;
  {$endif}

  FErrorCode := 0;
  FConnection := nil;
  FOnCloseRaised := false;

  ReleaseTunnel;

  FClient.CloseIfNoActiveTunnels := true;

  if Commands.Count = 0 then
  begin
    {$ifdef SECURE_BLACKBOX_DEBUG}
    Dumper.WriteString('Mode: shell');
    {$endif}
    FTunnel := TElShellSSHTunnel.Create(nil);
    TElShellSSHTunnel(FTunnel).Environment.Assign(FEnvironment);
    TElShellSSHTunnel(FTunnel).TerminalInfo := Self.FTerminalInfo;
  end
  else
  begin
    {$ifdef SECURE_BLACKBOX_DEBUG}
    Dumper.WriteString('Mode: command (' + Commands[0] + ')');
    {$endif}
    FTunnel := TElCommandSSHTunnel.Create(nil);

    for Index := 0 to Commands.Count - 1 do
      TElCommandSSHTunnel(FTunnel).Commands.Add(Commands[Index]);
      
    TElCommandSSHTunnel(FTunnel).Environment.Assign(FEnvironment);
    TElCommandSSHTunnel(FTunnel).TerminalInfo := Self.FTerminalInfo;
  end;

  FTunnel.TunnelList := FTunnelList;
  FTunnel.OnOpen := OnTunnelOpen;
  FTunnel.OnClose := OnTunnelClose;

  FDoUseInternalSocket := FUseInternalSocket and (FSocket.Address <> '');

  if FDoUseInternalSocket then
  begin
    try
      FSocket.Init(istStream);

      FSocket.UseSocks := UseSocks;
      FSocket.SocksAuthentication := SocksAuthentication;
      FSocket.SocksPassword := SocksPassword;
      FSocket.SocksUserCode := SocksUserCode;
      FSocket.SocksPort := SocksPort;
      FSocket.SocksResolveAddress := SocksResolveAddress;
      FSocket.SocksServer := SocksServer;
      FSocket.SocksPort := SocksPort;
      FSocket.SocksVersion := SocksVersion;
      FSocket.UseWebTunneling := UseWebTunneling;
      FSocket.WebTunnelAddress := WebTunnelAddress;
      FSocket.WebTunnelPort := WebTunnelPort;
      FSocket.WebTunnelAuthentication := WebTunnelAuthentication;
      FSocket.WebTunnelUserId := WebTunnelUserId;
      FSocket.WebTunnelPassword := WebTunnelPassword;

      err := FSocket.Connect(SocketTimeout);
      if err <> 0 then
        raise EElSimpleSSHClientException.Create(Format(SConnError, [err]));

      FLastOpTick := GetTickCount();
    except
      on E : EElSocketError do
      begin
        raise EElSimpleSSHClientException.Create(Format(SConnError2, [E.Message]));
      end;
    end;
  end;

  FClient.Open;
  { Opening client }
  while (not FClient.Active) and (FErrorCode = 0) do
  begin
    if not DoMessageLoop(false) then
    begin
      FErrorCode := ERROR_SSH_CONNECTION_CANCELLED_BY_USER;
      DoError(Self, FErrorCode);
    end;
  end;

  {$ifdef SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('SSH connection established. Waiting for shell connection to be opened.');
  {$endif}

  { Waiting shell tunnel to be opened }
  while (FConnection = nil) and (FErrorCode = 0) do
  begin
    if not DoMessageLoop(false) then
    begin
      FErrorCode := ERROR_SSH_CONNECTION_CANCELLED_BY_USER;
      DoError(Self, FErrorCode);
    end;
  end;

  {$ifdef SECURE_BLACKBOX_DEBUG}
  if FErrorCode = 0 then
    Dumper.WriteString('Shell connection opened.')
  else
    Dumper.WriteString('Shell connection is not opened');
  Dumper.EndSubsection;
  {$endif}

  if FErrorCode <> 0 then
    raise EElSimpleSSHClientException.CreateFmt(SConnFailed, [FErrorCode]);
end;

procedure TElSimpleSSHClient.SendData(Buffer : pointer; Size : integer);
begin
  if (not FClient.Active) then
    raise EElSimpleSSHClientException.Create(SNotConnected);

  FLastOpTick := GetTickCount();

  if Assigned(FConnection) then
    FConnection.SendData(Buffer, Size);
end;

procedure TElSimpleSSHClient.SendText(const S : string);
begin
  SendData(@S[1], Length(S));
end;

procedure TElSimpleSSHClient.ReceiveText(var Text : string; var StdErrText : string);
var
  Recvd, RecvdErr: integer;
  Buf, BufErr: string;
const
  CHUNK_SIZE = 16384;
begin
  Text := '';
  StdErrText := '';
  repeat
    if FClient.Active then
    begin
      Recvd := CHUNK_SIZE;
      RecvdErr := CHUNK_SIZE;
      SetLength(Buf, Recvd);
      SetLength(BufErr, RecvdErr);
      ReceiveData(@Buf[1], Recvd, @BufErr[1], RecvdErr);
      SetLength(Buf, Recvd);
      SetLength(BufErr, RecvdErr);
      Text := Text + Buf;
      StdErrText := StdErrText + BufErr;
    end;
  until ((Recvd = 0) and (RecvdErr = 0)) or (not FClient.Active);
end;

procedure TElSimpleSSHClient.OnTunnelOpen(Sender : TObject; Conn :
    TElSSHTunnelConnection);
begin
  //MessageBox(0, 'Connection opened', PChar('Debug'), 0); 
  FConnection := TElSSHClientTunnelConnection(Conn);
  FConnection.OnData := OnTunnelData;
  FConnection.OnExtendedData := OnTunnelExtData;
  FConnection.OnClose := DoConnClosed;
end;

procedure TElSimpleSSHClient.OnTunnelClose(Sender : TObject; Conn :
    TElSSHTunnelConnection);
begin
  FConnection := nil;
end;

procedure TElSimpleSSHClient.OnTunnelData(Sender : TObject; Buffer : pointer;
  Size : integer);
begin
  {$ifdef SECURE_BLACKBOX_DEBUG}
  {$endif}
  if Length(FDataBuf) < FDataLen + Size then
    SetLength(FDataBuf, FDataLen + Size);
  Move(Buffer^, FDataBuf[FDataLen], Size);
  Inc(FDataLen, Size);
end;

procedure TElSimpleSSHClient.OnTunnelExtData(Sender : TObject; Buffer : pointer;
  Size : integer);
begin
  {$ifdef SECURE_BLACKBOX_DEBUG}
  {$endif}
  if Length(FExtDataBuf) < FExtDataLen + Size then
    SetLength(FExtDataBuf, FExtDataLen + Size);
  Move(Buffer^, FExtDataBuf[FExtDataLen], Size);
  Inc(FExtDataLen, Size);
end;

procedure TElSimpleSSHClient.DoConnClosed(Sender : TObject; CloseType : TSSHCloseType);
begin

end;

function TElSimpleSSHClient.GetCommand : string;
begin
  if FCommands.Count > 0 then
    Result := FCommands[0]
  else
    Result := '';
end;

procedure TElSimpleSSHClient.SetCommand(const Value: string);
begin
  if (Length(Value) > 0) then
  begin
    if FCommands.Count > 0 then
      FCommands[0] := Value
    else
      FCommands.Add(Value);
  end
  else
    FCommands.Clear;
end;

function TElSimpleSSHClient.GetVersions : TSSHVersions;
begin
  // bug in Chrome .241
  Result := FClient.Versions;
end;

procedure TElSimpleSSHClient.SetVersions(Value : TSSHVersions);
begin
  FClient.Versions := Value;
end;

function TElSimpleSSHClient.GetPassword : string;
begin
  Result := FClient.Password;
end;

procedure TElSimpleSSHClient.SetPassword(Value : string);
begin
  FClient.Password := Value;
end;

function TElSimpleSSHClient.GetUsername : string;
begin
  Result := FClient.Username;
end;

procedure TElSimpleSSHClient.SetUsername(Value : string);
begin
  FClient.Username := Value;
end;

function TElSimpleSSHClient.GetClientHostname : string;
begin
  Result := FClient.ClientHostName;
end;

procedure TElSimpleSSHClient.SetClientHostname(Value : string);
begin
  FClient.ClientHostname := Value;
end;

function TElSimpleSSHClient.GetClientUsername : string;
begin
  Result := FClient.ClientUserName;
end;

procedure TElSimpleSSHClient.SetClientUsername(Value : string);
begin
  FClient.ClientUsername := Value;
end;

function TElSimpleSSHClient.GetSoftwareName : string;
begin
  Result := FClient.SoftwareName;
end;

procedure TElSimpleSSHClient.SetSoftwareName(Value : string);
begin
  FClient.SoftwareName := Value;
end;

function TElSimpleSSHClient.GetForceCompression : boolean;
begin
  Result := FClient.ForceCompression;
end;

procedure TElSimpleSSHClient.SetForceCompression(Value : boolean);
begin
  FClient.ForceCompression := Value;
end;

function TElSimpleSSHClient.GetCompressionLevel : integer;
begin
  Result := FClient.CompressionLevel;
end;

procedure TElSimpleSSHClient.SetCompressionLevel(Value : integer);
begin
  FClient.CompressionLevel := Value;
end;

function TElSimpleSSHClient.GetAuthenticationTypes : integer;
begin
  Result := FClient.AuthenticationTypes;
end;

procedure TElSimpleSSHClient.SetAuthenticationTypes(Value : integer);
begin
  FClient.AuthenticationTypes := Value;
end;

function TElSimpleSSHClient.GetActive : boolean;
begin
  Result := FClient.Active and (FConnection <> nil);
end;

function TElSimpleSSHClient.GetEncryptionAlgorithms(Index :
  TSSHEncryptionAlgorithm) : boolean;
begin
  Result := FClient.EncryptionAlgorithms[Index];
end;

procedure TElSimpleSSHClient.SetEncryptionAlgorithms(Index :
  TSSHEncryptionAlgorithm; Value : boolean);
begin
  FClient.EncryptionAlgorithms[Index] := Value;
end;

function TElSimpleSSHClient.GetCompressionAlgorithms(Index :
  TSSHCompressionAlgorithm) : boolean;
begin
  Result := FClient.CompressionAlgorithms[Index];
end;

procedure TElSimpleSSHClient.SetCompressionAlgorithms(Index :
  TSSHCompressionAlgorithm; Value : boolean);
begin
  FClient.CompressionAlgorithms[Index] := Value;
end;

function TElSimpleSSHClient.GetMACAlgorithms(Index : TSSHMacAlgorithm) : boolean;
begin
  Result := FClient.MACAlgorithms[Index];
end;

procedure TElSimpleSSHClient.SetMACAlgorithms(Index : TSSHMacAlgorithm; Value :
  boolean);
begin
  FClient.MACAlgorithms[Index] := Value;
end;

function TElSimpleSSHClient.GetKexAlgorithms(Index : TSSHKexAlgorithm) : boolean;
begin
  Result := FClient.KexAlgorithms[Index];
end;

procedure TElSimpleSSHClient.SetKexAlgorithms(Index : TSSHKexAlgorithm; Value :
  boolean);
begin
  FClient.KexAlgorithms[Index] := Value;
end;

function TElSimpleSSHClient.GetPublicKeyAlgorithms(Index :
  TSSHPublicKeyAlgorithm) : boolean;
begin
  Result := FClient.PublicKeyAlgorithms[Index];
end;

procedure TElSimpleSSHClient.SetPublicKeyAlgorithms(Index :
  TSSHPublicKeyAlgorithm; Value : boolean);
begin
  FClient.PublicKeyAlgorithms[Index] := Value;
end;

function TElSimpleSSHClient.GetServerSoftwareName : string;
begin
  Result := FClient.ServerSoftwareName;
end;

function TElSimpleSSHClient.GetVersion : TSSHVersion;
begin
  Result := FClient.Version;
end;

function TElSimpleSSHClient.GetServerCloseReason : string;
begin
  Result := FClient.ServerCloseReason;
end;

function TElSimpleSSHClient.GetEncryptionAlgorithmSC : TSSHEncryptionAlgorithm;
begin
  Result := FClient.EncryptionAlgorithmServerToClient;
end;

function TElSimpleSSHClient.GetEncryptionAlgorithmCS : TSSHEncryptionAlgorithm;
begin
  Result := FClient.EncryptionAlgorithmClientToServer;
end;

function TElSimpleSSHClient.GetCompressionAlgorithmSC : TSSHCompressionAlgorithm;
begin
  Result := FClient.CompressionAlgorithmServerToClient;
end;

function TElSimpleSSHClient.GetCompressionAlgorithmCS : TSSHCompressionAlgorithm;
begin
  Result := FClient.CompressionAlgorithmClientToServer;
end;

function TElSimpleSSHClient.GetMacAlgorithmSC : TSSHMacAlgorithm;
begin
  Result := FClient.MacAlgorithmServerToClient;
end;

function TElSimpleSSHClient.GetMacAlgorithmCS : TSSHMacAlgorithm;
begin
  Result := FClient.MacAlgorithmClientToServer;
end;

function TElSimpleSSHClient.GetKexAlgorithm : TSSHKexAlgorithm;
begin
  Result := FClient.KexAlgorithm;
end;

function TElSimpleSSHClient.GetPublicKeyAlgorithm : TSSHPublicKeyAlgorithm;
begin
  Result := FClient.PublicKeyAlgorithm;
end;

(*
{$ifdef DELPHI_NET}
function TElSimpleSSHClient.GetCommand: string;
begin
  Result := StringOfBytes(FCommand);
end;

procedure TElSimpleSSHClient.SetCommand(const AValue: string);
begin
  FCommand := BytesOfString(AValue);
end;
{$endif}
*)
procedure TElSimpleSSHClient.DoError(Sender : TObject; ErrorCode : integer);
begin
  FErrorCode := ErrorCode;

  if FDoUseInternalSocket then
    try
      FSocket.Close(true);
    except
    end;
  
  if assigned(FOnError) then
    FOnError(Self, ErrorCode);
end;

procedure TElSimpleSSHClient.DoBanner(Sender: TObject; const Text : string; const Language : string);
begin
  if assigned(FOnBanner) then
    FOnBanner(Self, Text, Language);
end;

procedure TElSimpleSSHClient.DoClose(Force: boolean);
begin
  FClient.Close(Force);
  if FDoUseInternalSocket then
  begin
    try
      if FSocket.State in [issConnected, issConnecting] then
        FSocket.Close(true);
    except
    end;
  end;
end;

procedure TElSimpleSSHClient.DoKeyValidate(Sender : TObject; ServerKey :
    TElSSHKey; var Validate : boolean);
begin
  if assigned(FOnKeyValidate) then
    FOnKeyValidate(Self, ServerKey, Validate)
  else
    Validate := true;
end;

procedure TElSimpleSSHClient.DoReceive(Sender : TObject; Buffer : pointer;
    MaxSize : longint; out Written : longint);
var Time : integer;
begin
  if FDoUseInternalSocket then
  begin
    try
      if (SocketTimeout > 0) then
        Time := SocketTimeout
      else
        Time := 0;
      if not FSocket.CanReceive(Time) then
      begin
        Written := 0;
      end
      else
      if (FSocket.Receive(Buffer, MaxSize, Written) <> 0) or (Written = 0) then
        raise EElSimpleSSHClientException.Create(SConnLost);
    except
      FErrorCode := ERROR_SSH_CONNECTION_LOST;
      Written := 0;
      DoClose(true);
      raise EElSimpleSSHClientException.Create(SConnLost);
    end;
  end
  else
  if assigned(FOnReceive) then
    FOnReceive(Self, Buffer, MaxSize, Written)
  else
    Written := 0;

  if Written = 0 then
    FDataReceived := false;
end;

procedure TElSimpleSSHClient.DoSend(Sender : TObject; Buffer : pointer; Size :
    longint);
var ToSend, Sent : integer;
begin
  if FDoUseInternalSocket then
  begin
    Sent := 0;
    ToSend := Size;

    while ToSend > 0 do
    begin
      try
        if SocketTimeout > 0 then
        begin
          if not FSocket.CanSend(SocketTimeout) then
            raise EElSocketError.Create('');
        end;
        
        if FSocket.Send(Pointer(Integer(Buffer) + (Size - ToSend)), ToSend, Sent) <> 0 then
          raise EElSimpleSSHClientException.Create(SConnLost);
        Dec(ToSend, Sent);
      except
        on E : EElSocketError do
        begin
          DoClose(true);
          raise EElSimpleSSHClientException.Create(SConnLost);
        end;
      end;
    end;
  end
  else
  if Assigned(FOnSend) then
    FOnSend(Self, Buffer, Size);
end;

function TElSimpleSSHClient.GetAddress: string;
begin
  Result := FSocket.Address;
end;

function TElSimpleSSHClient.GetKeyStorage: TElSSHCustomKeyStorage;
begin
  Result := FClient.KeyStorage;
end;

function TElSimpleSSHClient.GetPort: Integer;
begin
  Result := FSocket.Port;
end;

procedure TElSimpleSSHClient.ReceiveData(Buffer : pointer; var Size : integer;
  StdErrBuffer : pointer; var StdErrSize : integer);
var ToWrite : integer;
begin
(*
  if (not FClient.Active) and (FDataLen = 0) and (FExtDataLen = 0) then
    raise EElSimpleSSHClientException.Create(SConnNotPresent);
*)

  {$ifdef SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('Calling ElSimpleSSHClient.ReceiveData');
  Dumper.BeginSubsection();
  {$endif}

  FLastOpTick := GetTickCount();

  if (FConnection <> nil) then
  begin
    FDataReceived := true;

    while ((Size > FDataLen) or (StdErrSize > FExtDataLen)) and FDataReceived and FClient.Active do
    begin
      {$ifdef SECURE_BLACKBOX_DEBUG}
      Dumper.WriteString('Size is greater than FDataLen. Calling DoMessageLoop.');
      {$endif}
      if DoMessageLoop(true) then
      try
        FClient.DataAvailable;
      except
        on E : EElSimpleSSHClientException do
        begin
          if (FDataLen > 0) or (FExtDataLen > 0) then
            break
          else
            raise;
        end;
      end
      else
        raise EElSimpleSSHClientException.CreateFmt(SConnError, [ERROR_SSH_CONNECTION_CANCELLED_BY_USER]); 
    end;
  end;

  // read stdout
  if FDataLen > 0 then
  begin
    ToWrite := min(Size, FDataLen);
    if (ToWrite > 0) and (Buffer <> nil) then
    begin
      Move(FDataBuf[0], Buffer^, ToWrite);
      if Size < FDataLen then
      begin
        Move(FDataBuf[Size], FDataBuf[0], FDataLen - Size);
        Dec(FDataLen, Size);
      end
      else
      begin
        Size := FDataLen;
        FDataLen := 0;
      end;

      if FDataLen < Length(FDataBuf) shr 1 then
        SetLength(FDataBuf, FDataLen);
    end
    else
      Size := 0;
  end
  else
    Size := 0;

  // read stderr
  if FExtDataLen > 0 then
  begin
    ToWrite := min(StdErrSize, FExtDataLen);
    if (ToWrite > 0) and (StdErrBuffer <> nil) then
    begin
      Move(FExtDataBuf[0], StdErrBuffer^, ToWrite);
      if StdErrSize < FExtDataLen then
      begin
        Move(FExtDataBuf[StdErrSize], FExtDataBuf[0], FExtDataLen - StdErrSize);
        Dec(FExtDataLen, StdErrSize);
      end
      else
      begin
        StdErrSize := FExtDataLen;
        FExtDataLen := 0;
      end;

      if FExtDataLen < Length(FExtDataBuf) shr 1 then
        SetLength(FExtDataBuf, FExtDataLen);
    end
    else
      StdErrSize := 0;
  end
  else
    StdErrSize := 0;

  (*
  end
  else
  begin
    FClient.DataAvailable;
    Size := 0;
    StdErrSize := 0;
  end;
*)

  {$ifdef SECURE_BLACKBOX_DEBUG}
  Dumper.EndSubsection;
  {$endif}
end;

procedure TElSimpleSSHClient.SetKeyStorage(Value: TElSSHCustomKeyStorage);
begin
  FClient.KeyStorage := Value;
end;

procedure TElSimpleSSHClient.SetTerminalInfo(Value: TElTerminalInfo);
begin
  FTerminalInfo := Value;
  if FTunnel <> nil then
  begin
    if FTunnel is TElShellSSHTunnel then
      TElShellSSHTunnel(FTunnel).TerminalInfo := Value
    else
      TElCommandSSHTunnel(FTunnel).TerminalInfo := Value;
  end;
end;

{$IFNDEF CLX_USED}
function TElSimpleSSHClient.IntMessageLoop(NoPeek : boolean): boolean;
var
  Msg: TMsg;
begin
  if not FDoUseInternalSocket then
  begin
    result := true;
    if not NoPeek then
      DataAvailable;

    if PeekMessage(Msg, 0, 0, 0, PM_REMOVE) then
    begin
      if (Msg.message = WM_QUIT) then
        result := false
      else
      begin
        TranslateMessage(Msg);
        DispatchMessage(Msg);
      end;
    end;
  end
  else
  begin
    if not NoPeek then
      DataAvailable;
    result := true;
  end;
end;

{$ELSE}
function TElSimpleSSHClient.IntMessageLoop(NoPeek : boolean): boolean;
begin
  if not NoPeek then
    DataAvailable;
  result := true;
end;
{$ENDIF}

function TElSimpleSSHClient.DoMessageLoop(NoPeek : boolean): boolean;
begin
  {$ifdef SECURE_BLACKBOX_DEBUG}
  if NoPeek then
    Dumper.WriteString('DoMessageLoop: NoPeek = FALSE')
  else
    Dumper.WriteString('DoMessageLoop: NoPeek = TRUE');
  {$endif}
  Result := IntMessageLoop(NoPeek);
  {$ifdef SECURE_BLACKBOX_DEBUG}
  if Result then
    Dumper.WriteString('IntMessageLoop returned true')
  else
    Dumper.WriteString('IntMessageLoop returned false');
  {$endif}
  if Result and FDoUseInternalSocket and (SocketTimeout > 0) then
  begin
    if TickDiff(FLastOpTick, GetTickCount) > SocketTimeout then
      raise EElSocketError.Create('Connection timeout');
  end;
  if Result and Assigned(FMessageLoop) then
    result := FMessageLoop();
end;




procedure TElSimpleSSHClient.SetPort(Value: Integer);
begin
  if FSocket.Port <> Value then
  begin
    if (not Active) then
      FSocket.Port := Value
    else
      raise EElSimpleSSHClientException.Create(SConnAlreadyEstablished);
  end;
end;

procedure TElSimpleSSHClient.SetUseInternalSocket(Value: Boolean);
begin
  if FUseInternalSocket <> Value then
  begin
    FUseInternalSocket := Value;
  end;
end;

procedure TElSimpleSSHClient.SetAddress(const Value: string);
begin
  if FSocket.Address <> Value then
  begin
    if (not Active) then
      FSocket.Address := Value
    else
      raise EElSimpleSSHClientException.Create(SConnAlreadyEstablished);
  end;
end;

procedure TElSimpleSSHClient.ReleaseTunnel;
begin
  if Assigned(FTunnel) then
  begin
    FTunnel.TunnelList := nil;
    FreeAndNil(FTunnel);
  end;
end;

procedure TElSimpleSSHClient.SetSocketTimeout(Value: Integer);
begin
  if (FSocketTimeout <> Value) and (FSocketTimeout >= 0) then
  begin
    FSocketTimeout := Value;
  end;
end;

procedure TElSimpleSSHClient.SetUseSocks(const Value: Boolean);
begin
  FUseSocks := Value;
  if Value then
    UseWebTunneling := false;
end;

procedure TElSimpleSSHClient.SetUseWebTunneling(const Value: boolean);
begin
  FUseWebTunneling := Value;
  if Value then
    UseSocks := false;
end;

function TElSimpleSSHClient.GetErrorString: string;
begin
  Result := FClient.ErrorString;
end;

function TElSimpleSSHClient.GetLocalAddress: string;
begin
  Result := FSocket.ListenAddress;
end;

function TElSimpleSSHClient.GetLocalPort: Integer;
begin
  Result := FSocket.ListenPort;
end;

procedure TElSimpleSSHClient.SetLocalAddress(const Value: string);
begin
  FSocket.ListenAddress := Value;
end;

procedure TElSimpleSSHClient.SetLocalPort(const Value: Integer);
begin
  FSocket.ListenPort := Value;
end;

function TElSimpleSSHClient.GetAuthOrder() : TSBSSHAuthOrder;
begin
  result := FClient.SSHAuthOrder;
end;

procedure TElSimpleSSHClient.SetAuthOrder(Value : TSBSSHAuthOrder);
begin
  FClient.SSHAuthOrder := Value;
end;

end.
