(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBBzip2Types;

interface

uses
  SBBzip2BaseTypes,
  SBBzip2Utils;


const BZ_VERSION = '1.0.3, 15-Feb-2005';

const CRLF = #13+#10;
const TAB = #9;
//*-- Header bytes. --*/

const BZ_HDR_B = $42;                         // 'B'
const BZ_HDR_Z = $5a;                         // 'Z'
const BZ_HDR_h = $68;                         // 'h'
const BZ_HDR_0 = $30;                         // '0'

//-- Constants for the back end. --*/

const BZ_MAX_ALPHA_SIZE= 258;
const BZ_MAX_CODE_LEN= 23;

const BZ_RUNA =0;
const BZ_RUNB =1;

const BZ_N_GROUPS= 6;
const BZ_G_SIZE  = 50;
const BZ_N_ITERS = 4;

const BZ_MAX_SELECTORS = (2 + (900000 div BZ_G_SIZE));

const BZ_LESSER_ICOST = 0;
const BZ_GREATER_ICOST  = 15;




const BZ_RUN               =0;
const BZ_FLUSH             =1;
const BZ_FINISH            =2;

const BZ_OK                =0;
const BZ_RUN_OK            =1;
const BZ_FLUSH_OK          =2;
const BZ_FINISH_OK         =3;
const BZ_STREAM_END        =4;
const BZ_SEQUENCE_ERROR    =(-1);
const BZ_PARAM_ERROR       =(-2);
const BZ_MEM_ERROR         =(-3);
const BZ_DATA_ERROR        =(-4);
const BZ_DATA_ERROR_MAGIC  =(-5);
const BZ_IO_ERROR          =(-6);
const BZ_UNEXPECTED_EOF    =(-7);
const BZ_OUTBUFF_FULL      =(-8);
const BZ_CONFIG_ERROR      =(-9);
const BZ_NULL              =(-999);

const BZ_M_IDLE      =1;
const BZ_M_RUNNING   =2;
const BZ_M_FLUSHING  =3;
const BZ_M_FINISHING =4;

const BZ_S_OUTPUT    =1;
const BZ_S_INPUT     =2;

const BZ_N_RADIX =2;
const BZ_N_QSORT =12;
const BZ_N_SHELL =18;
const BZ_N_OVERSHOOT =(BZ_N_RADIX + BZ_N_QSORT + BZ_N_SHELL + 2);

//-- states for decompression. --*/

const BZ_X_IDLE       = 1;
const BZ_X_OUTPUT     = 2;
const BZ_X_MAGIC_1    = 10;
const BZ_X_MAGIC_2    = 11;
const BZ_X_MAGIC_3    = 12;
const BZ_X_MAGIC_4    = 13;
const BZ_X_BLKHDR_1   = 14;
const BZ_X_BLKHDR_2   = 15;
const BZ_X_BLKHDR_3   = 16;
const BZ_X_BLKHDR_4   = 17;
const BZ_X_BLKHDR_5   = 18;
const BZ_X_BLKHDR_6   = 19;
const BZ_X_BCRC_1     = 20;
const BZ_X_BCRC_2     = 21;
const BZ_X_BCRC_3     = 22;
const BZ_X_BCRC_4     = 23;
const BZ_X_RANDBIT    = 24;
const BZ_X_ORIGPTR_1  = 25;
const BZ_X_ORIGPTR_2  = 26;
const BZ_X_ORIGPTR_3  = 27;
const BZ_X_MAPPING_1  = 28;
const BZ_X_MAPPING_2  = 29;
const BZ_X_SELECTOR_1 = 30;
const BZ_X_SELECTOR_2 = 31;
const BZ_X_SELECTOR_3 = 32;
const BZ_X_CODING_1   = 33;
const BZ_X_CODING_2   = 34;
const BZ_X_CODING_3   = 35;
const BZ_X_MTF_1      = 36;
const BZ_X_MTF_2      = 37;
const BZ_X_MTF_3      = 38;
const BZ_X_MTF_4      = 39;
const BZ_X_MTF_5      = 40;
const BZ_X_MTF_6      = 41;
const BZ_X_ENDHDR_2   = 42;
const BZ_X_ENDHDR_3   = 43;
const BZ_X_ENDHDR_4   = 44;
const BZ_X_ENDHDR_5   = 45;
const BZ_X_ENDHDR_6   = 46;
const BZ_X_CCRC_1     = 47;
const BZ_X_CCRC_2     = 48;
const BZ_X_CCRC_3     = 49;
const BZ_X_CCRC_4     = 50;



//-- Constants for the fast MTF decoder. --*/

const MTFA_SIZE= 4096;
const MTFL_SIZE=16;



//-- Structure holding all the decompression-side stuff. --*/

const BZ_MAX_UNUSED=5000;

{$IFNDEF CLR}
type TOpenedFile = integer;
type TBytes = array of byte;
const NOFILE = -1;
{$ELSE}
type TOpenedFile =  System.IO.FileStream;
const NOFILE = nil;
{$ENDIF}

type
  EState = class;
  DState = class;
  bz_stream = class(TObject)
  public
      next_in:TSBByteArrayPointer;
      avail_in:Longint;
      total_in_lo32:Longint;
      total_in_hi32:Longint;

      next_out:TSBByteArrayPointer;
      avail_out:Longint;
      total_out_lo32:Longint;
      total_out_hi32:Longint;

      state_e:EState;
      state_d:DState;

//      opaque:Pointer;
   end;






 EState = class(TObject)
 public

      // pointer back to the struct bz_stream */
      strm:bz_stream;

      // mode this stream is in, and whether inputting */
      // or outputting data */
      mode:Int32;
      state:Int32;

      // remembers avail_in when flush/finish requested */
      avail_in_expect:UInt32;

      // for doing the block sorting */
      arr1,arr2,ftab: TSBUInt32Array;
      origPtr:Int32;

      // aliases for arr1 and arr2 */
      ptr:TSBUInt32ArrayPointer;
      block:TSBByteonUInt32Array;
      mtfv:TSBUInt16onUInt32Array;
      zbits:TSBByteonUInt32Array;

      // for deciding when to use the fallback sorting algorithm */
      workFactor:Int32;

      // run-length-encoding of the input */
      state_in_ch:UInt32;
      state_in_len:Int32;
      rNToGo:Int32;
      rTPos:Int32;

      // input and output limits and current posns */
      nblock:Int32;
      nblockMAX:Int32;
      numZ:Int32;
      state_out_pos:Int32;

      // map of bytes used in block */
      nInUse:Int32;
      inUse:array [0..256] of Boolean;
      unseqToSeq:array [0..256] of Byte;

      // the buffer for bit stream creation */
      bsBuff:UInt32;
      bsLive:Int32;

      // block and combined CRCs */
      blockCRC:UInt32;
      combinedCRC:UInt32;

      // misc administratium */
      verbosity:Int32;
      blockNo:Int32;
      blockSize100k:Int32;

      // stuff for coding the MTF values */
      nMTF:Int32;
      mtfFreq:array [0..BZ_MAX_ALPHA_SIZE] of Int32;
      selector:array[0..BZ_MAX_SELECTORS] of Byte;
      selectorMtf:array [0..BZ_MAX_SELECTORS] of Byte;

      len:array [0..BZ_N_GROUPS] of TSBByteArray;
      code:array [0..BZ_N_GROUPS] of TSBInt32Array;
      rfreq:array [0..BZ_N_GROUPS] of TSBInt32Array;
      // second dimension: only 3 needed; 4 makes index calculations faster */
      len_pack:array [0..BZ_MAX_ALPHA_SIZE,0..4] of UInt32;

   end;





//-- Structure holding all the decompression-side stuff. --*/

 DState = class(TObject)
 public

      // pointer back to the struct bz_stream */
      strm:bz_stream;

      // state indicator for this stream */
      state:Int32;

      // for doing the final run-length decoding */
      state_out_ch:Byte;
      state_out_len:Int32;
      blockRandomised:Boolean;
      rNToGo:Int32;
      rTPos:Int32;

      // the buffer for bit stream reading */
      bsBuff:UInt32;
      bsLive:Int32;

      // misc administratium */
      blockSize100k:UInt32;
      smallDecompress:Boolean;
      currBlockNo:Int32;
      verbosity:Int32;

      // for undoing the Burrows-Wheeler transform */
      origPtr:Int32;
      tPos:UInt32;
      k0:Int32;
      unzftab:array [0..256] of Int32;
      nblock_used:Int32;
      cftab: array [0..257] of Int32;
      cftabCopy:array [0..257] of Int32;

      // for undoing the Burrows-Wheeler transform (FAST) */
      tt:TSBUint32Array;

      // for undoing the Burrows-Wheeler transform (SMALL) */
      ll16:TSBUInt16Array;
      ll4:TSBByteArray;

      // stored and calculated CRCs */
      storedBlockCRC:UInt32;
      storedCombinedCRC:UInt32;
      calculatedBlockCRC:UInt32;
      calculatedCombinedCRC:Uint32;

      // map of bytes used in block */
      nInUse:Int32;
      inUse: array [0..256] of Boolean;
      inUse16: array [0..16] of Boolean;
      seqToUnseq:array [0..256] of Byte;

      // for decoding the MTF values */
      mtfa:array [0..MTFA_SIZE] of Byte;
      mtfbase:array [0..(256 div MTFL_SIZE)] of Int32;
      selector:array [0..BZ_MAX_SELECTORS] of Byte;
      selectorMtf:array [0..BZ_MAX_SELECTORS] of Byte;
      len:array [0..BZ_N_GROUPS] of TSBByteArray;

      limit: array [0..BZ_N_GROUPS] of TSBInt32Array;
      base: array [0..BZ_N_GROUPS] of TSBInt32Array;
      perm: array [0..BZ_N_GROUPS] of TSBInt32Array;
      minLens: array [0..BZ_N_GROUPS] of Int32;

      // save area for scalars in the main decompress code */
      save_i,
      save_j,
      save_t,
      save_alphaSize,
      save_nGroups,
      save_nSelectors,
      save_EOB,
      save_groupNo,
      save_groupPos,
      save_nextSym,
      save_nblockMAX,
      save_nblock,
      save_es,
      save_N,
      save_curr,
      save_zt,
      save_zn,
      save_zvec,
      save_zj,
      save_gSel,
      save_gMinlen:Int32;

      save_gLimit,
      save_gBase,
      save_gPerm:TSBInt32Array;

    end;

implementation


end.
