(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBConstants;

interface

uses SBUtils;

  
const
  SB_SUITE_FIRST = SmallInt(0);
  // generic SSL/TLS ciphersuites
  SB_SUITE_NULL_NULL_NULL = SmallInt(0);
  SB_SUITE_RSA_NULL_MD5 = SmallInt(1);
  SB_SUITE_RSA_NULL_SHA = SmallInt(2);
  SB_SUITE_RSA_RC4_MD5 = SmallInt(3);
  SB_SUITE_RSA_RC4_SHA = SmallInt(4);
  SB_SUITE_RSA_RC2_MD5 = SmallInt(5);
  SB_SUITE_RSA_IDEA_MD5 = SmallInt(6);
  SB_SUITE_RSA_IDEA_SHA = SmallInt(7);
  SB_SUITE_RSA_DES_MD5 = SmallInt(8);
  SB_SUITE_RSA_DES_SHA = SmallInt(9);
  SB_SUITE_RSA_3DES_MD5 = SmallInt(10);
  SB_SUITE_RSA_3DES_SHA = SmallInt(11);
  SB_SUITE_RSA_AES128_SHA = SmallInt(12);
  SB_SUITE_RSA_AES256_SHA = SmallInt(13);
  SB_SUITE_DH_DSS_DES_SHA = SmallInt(14);
  SB_SUITE_DH_DSS_3DES_SHA = SmallInt(15);
  SB_SUITE_DH_DSS_AES128_SHA = SmallInt(16);
  SB_SUITE_DH_DSS_AES256_SHA = SmallInt(17);
  SB_SUITE_DH_RSA_DES_SHA = SmallInt(18);
  SB_SUITE_DH_RSA_3DES_SHA = SmallInt(19);
  SB_SUITE_DH_RSA_AES128_SHA = SmallInt(20);
  SB_SUITE_DH_RSA_AES256_SHA = SmallInt(21);
  SB_SUITE_DHE_DSS_DES_SHA = SmallInt(22);
  SB_SUITE_DHE_DSS_3DES_SHA = SmallInt(23);
  SB_SUITE_DHE_DSS_AES128_SHA = SmallInt(24);
  SB_SUITE_DHE_DSS_AES256_SHA = SmallInt(25);
  SB_SUITE_DHE_RSA_DES_SHA = SmallInt(26);
  SB_SUITE_DHE_RSA_3DES_SHA = SmallInt(27);
  SB_SUITE_DHE_RSA_AES128_SHA = SmallInt(28);
  SB_SUITE_DHE_RSA_AES256_SHA = SmallInt(29);
  SB_SUITE_DH_ANON_RC4_MD5 = SmallInt(30);
  SB_SUITE_DH_ANON_DES_SHA = SmallInt(31);
  SB_SUITE_DH_ANON_3DES_SHA = SmallInt(32);
  SB_SUITE_DH_ANON_AES128_SHA = SmallInt(33);
  SB_SUITE_DH_ANON_AES256_SHA = SmallInt(34);
  SB_SUITE_RSA_RC2_MD5_EXPORT = SmallInt(35);
  SB_SUITE_RSA_RC4_MD5_EXPORT = SmallInt(36);
  SB_SUITE_RSA_DES_SHA_EXPORT = SmallInt(37);
  SB_SUITE_DH_DSS_DES_SHA_EXPORT = SmallInt(38);
  SB_SUITE_DH_RSA_DES_SHA_EXPORT = SmallInt(39);
  SB_SUITE_DHE_DSS_DES_SHA_EXPORT = SmallInt(40);
  SB_SUITE_DHE_RSA_DES_SHA_EXPORT = SmallInt(41);
  SB_SUITE_DH_ANON_RC4_MD5_EXPORT = SmallInt(42);
  SB_SUITE_DH_ANON_DES_SHA_EXPORT = SmallInt(43);
  // camellia ciphersuites
  SB_SUITE_RSA_CAMELLIA128_SHA = SmallInt(44);
  SB_SUITE_DH_DSS_CAMELLIA128_SHA = SmallInt(45);
  SB_SUITE_DH_RSA_CAMELLIA128_SHA = SmallInt(46);
  SB_SUITE_DHE_DSS_CAMELLIA128_SHA = SmallInt(47);
  SB_SUITE_DHE_RSA_CAMELLIA128_SHA = SmallInt(48);
  SB_SUITE_DH_ANON_CAMELLIA128_SHA = SmallInt(49);
  SB_SUITE_RSA_CAMELLIA256_SHA = SmallInt(50);
  SB_SUITE_DH_DSS_CAMELLIA256_SHA = SmallInt(51);
  SB_SUITE_DH_RSA_CAMELLIA256_SHA = SmallInt(52);
  SB_SUITE_DHE_DSS_CAMELLIA256_SHA = SmallInt(53);
  SB_SUITE_DHE_RSA_CAMELLIA256_SHA = SmallInt(54);
  SB_SUITE_DH_ANON_CAMELLIA256_SHA = SmallInt(55);
  // psk ciphersuites (rfc4279)
  SB_SUITE_PSK_RC4_SHA = SmallInt(56);
  SB_SUITE_PSK_3DES_SHA = SmallInt(57);
  SB_SUITE_PSK_AES128_SHA = SmallInt(58);
  SB_SUITE_PSK_AES256_SHA = SmallInt(59);
  SB_SUITE_DHE_PSK_RC4_SHA = SmallInt(60);
  SB_SUITE_DHE_PSK_3DES_SHA = SmallInt(61);
  SB_SUITE_DHE_PSK_AES128_SHA = SmallInt(62);
  SB_SUITE_DHE_PSK_AES256_SHA = SmallInt(63);
  SB_SUITE_RSA_PSK_RC4_SHA = SmallInt(64);
  SB_SUITE_RSA_PSK_3DES_SHA = SmallInt(65);
  SB_SUITE_RSA_PSK_AES128_SHA = SmallInt(66);
  SB_SUITE_RSA_PSK_AES256_SHA = SmallInt(67);  
  // trailer
  SB_SUITE_LAST = SmallInt(67);


type
  TSBCipherSuite = SB_SUITE_FIRST..SB_SUITE_LAST;
  TSBVersion = (sbSSL2, sbSSL3, sbTLS1, sbTLS11);
  TSBVersions = set of TSBVersion;
  TSSLVersion =  record
    Major: byte;
    Minor: byte;
  end;

  { Algorithms and OIDs }

const

  //PKCS#1
  // Encryption Algorithm OIDs
  SB_OID_RC4                :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$03#$04; 
  SB_OID_RSAENCRYPTION	    :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$01; 
  SB_OID_RSAPSS             :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$0A; 
  SB_OID_RSAOAEP            :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$07; 
  SB_OID_DSA                :TBufferTypeConst = #$2A#$86#$48#$CE#$38#$04#$01; 
  SB_OID_DSA_SHA1           :TBufferTypeConst = #$2A#$86#$48#$CE#$38#$04#$03; 
  SB_OID_DSA_ALT            :TBufferTypeConst = #$2B#$0E#$03#$02#$0C; 
  SB_OID_DSA_SHA1_ALT       :TBufferTypeConst = #$2B#$0E#$03#$02#$0D; 
  SB_OID_DH                 :TBufferTypeConst = #$2A#$86#$48#$CE#$3E#$02#$01; 
  SB_OID_DES_EDE3_CBC	      :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$03#$07; 
  SB_OID_PKCS7_DATA	        :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$07#$01; 
  SB_OID_RC2_CBC	          :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$03#$02; 
  SB_OID_DES_CBC	          :TBufferTypeConst = #$2b#$0e#$03#$02#$07; 
  SB_OID_SHA1_RSAENCRYPTION :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$05; 
  SB_OID_SHA1_RSAENCRYPTION2 :TBufferTypeConst = #$2B#$0E#$03#$02#$1D; 
  SB_OID_SHA224_RSAENCRYPTION :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$0E; 
  SB_OID_SHA256_RSAENCRYPTION :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$0B; 
  SB_OID_SHA384_RSAENCRYPTION :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$0C; 
  SB_OID_SHA512_RSAENCRYPTION :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$0D; 
  SB_OID_RSASIGNATURE_RIPEMD160: TBufferTypeConst = #$2B#$24#$03#$03#$01#$02; 
  SB_OID_TSTINFO	        :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$09#$10#$01#$04; 

  SB_OID_AES128_CBC         :TBufferTypeConst = #$60#$86#$48#$01#$65#$03#$04#$01#$02; 
  SB_OID_AES192_CBC         :TBufferTypeConst = #$60#$86#$48#$01#$65#$03#$04#$01#$16; 
  SB_OID_AES256_CBC         :TBufferTypeConst = #$60#$86#$48#$01#$65#$03#$04#$01#$2A; 
  //mask generation function
  SB_OID_MGF1               :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$08; 
  //label source function for RSA-OAEP
  SB_OID_OAEP_SRC_SPECIFIED :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$09; 
  //PKCS#5 password-based encryption
  SB_OID_PBE_MD2_DES        :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$05#$01; 
  SB_OID_PBE_MD2_RC2        :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$05#$04; 
  SB_OID_PBE_MD5_DES        :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$05#$03; 
  SB_OID_PBE_MD5_RC2        :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$05#$03; 
  SB_OID_PBE_SHA1_DES       :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$05#$0A; 
  SB_OID_PBE_SHA1_RC2       :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$05#$0B; 
  SB_OID_PBES2              :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$05#$0D; 
  SB_OID_PBKDF2             :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$05#$0C; 
  SB_OID_PBMAC1             :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$05#$0E; 

  //PKCS#12
  SB_OID_PBE_SHA1_RC4_128   :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$0c#$01#$01; 
  SB_OID_PBE_SHA1_RC4_40    :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$0c#$01#$02; 
  SB_OID_PBE_SHA1_3DES      :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$0c#$01#$03; 
  SB_OID_PBE_SHA1_RC2_128   :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$0c#$01#$05; 
  SB_OID_PBE_SHA1_RC2_40    :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$0c#$01#$06; 
  SB_OID_MD2_RSAENCRYPTION  :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$02; 
  SB_OID_MD5_RSAENCRYPTION  :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$04; 
  SB_OID_SHA1_RSA           :TBufferTypeConst = #$2B#$0E#$03#$02#$1D; 
  SB_OID_SHA1_DSA           :TBufferTypeConst = #$2A#$86#$48#$CE#$38#$04#$03; 

  // Digest Algorithm OIDs
  SB_OID_MD2                :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$02#$02; 
  SB_OID_MD4                :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$02#$04; 
  SB_OID_MD5                :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$02#$05; 
  SB_OID_SHA1		    :TBufferTypeConst = #$2b#$0E#$03#$02#$1A; 
  SB_OID_SHA224		    :TBufferTypeConst = #$60#$86#$48#$01#$65#$03#$04#$02#$04; 
  SB_OID_SHA256		    :TBufferTypeConst = #$60#$86#$48#$01#$65#$03#$04#$02#$01; 
  SB_OID_SHA384		    :TBufferTypeConst = #$60#$86#$48#$01#$65#$03#$04#$02#$02; 
  SB_OID_SHA512		    :TBufferTypeConst = #$60#$86#$48#$01#$65#$03#$04#$02#$03; 
  SB_OID_RIPEMD160          :TBufferTypeConst = #$2B#$24#$03#$02#$01; 
  // MAC Algorithm OIDs
  SB_OID_HMACSHA1           :TBufferTypeConst = #$2B#$06#$01#$05#$05#$08#$01#$02; 
  SB_OID_RSA_HMACSHA1       :TBufferTypeConst = #$2a#$86#$48#$86#$F7#$0D#$02#$07; 
  // Attribute OIDs
  SB_OID_CONTENT_TYPE	    :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$09#$03; 
  SB_OID_MESSAGE_DIGEST	    :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$09#$04; 
  SB_OID_SIGNING_TIME	    :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$09#$05; 
  SB_OID_COUNTER_SIGNATURE  :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$09#$06; 
  SB_OID_SMIME_CAPABILITIES :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$09#$0F; 
  SB_OID_TIMESTAMP_TOKEN    :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$09#$10#$02#$0E; 
  // Authenticode OIDs
  SB_OID_SPC_INDIRECT_DATA  :TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$04; 
  SB_OID_SPC_SP_AGENCY_INFO :TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$0A; 
  SB_OID_SPC_STATEMENT_TYPE_OBJID :TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$0A; 
  SB_OID_SPC_STATEMENT_TYPE :TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$0B; 
  SB_OID_SPC_SP_OPUS_INFO   :TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$0C; 
  SB_OID_SPC_PE_IMAGE_DATA  :TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$0F; 
  SB_OID_SPC_MINIMAL_CRITERIA:TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$1A; 
  SB_OID_SPC_FINANCIAL_CRITERIA:TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$1B; 
  SB_OID_SPC_LINK           :TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$1C; 
  SB_OID_SPC_HASH_INFO      :TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$1D; 
  SB_OID_SPC_SIPINFO        :TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$1E; 
  SB_OID_SPC_CERT_EXTENSIONS:TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$0E; 
  SB_OID_SPC_RAW_FILE_DATA  :TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$12; 
  SB_OID_SPC_STRUCTURED_STORAGE_DATA:TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$13; 
  SB_OID_SPC_JAVA_CLASS_DATA:TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$14; 
  SB_OID_SPC_INDIVIDUAL_SP_KEY_PURPOSE:TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$15; 
  SB_OID_SPC_COMMERCIAL_SP_KEY_PURPOSE:TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$16; 
  SB_OID_SPC_CAB_DATA       :TBufferTypeConst = #$2B#$06#$01#$04#$01#$82#$37#$02#$01#$19; 
  // certificate extension OIDs
  SB_OID_QT_CPS             : TBufferTypeConst = #$2B#$06#$01#$05#$05#$07#$02#$01; 
  SB_OID_QT_UNOTICE         : TBufferTypeConst = #$2B#$06#$01#$05#$05#$07#$02#$02; 
  SB_OID_SERVER_AUTH        : TBufferTypeConst = #$2B#$06#$01#$05#$05#$07#$03#$01; 
  SB_OID_CLIENT_AUTH        : TBufferTypeConst = #$2B#$06#$01#$05#$05#$07#$03#$02; 
  SB_OID_CODE_SIGNING       : TBufferTypeConst = #$2B#$06#$01#$05#$05#$07#$03#$03; 
  SB_OID_EMAIL_PROT         : TBufferTypeConst = #$2B#$06#$01#$05#$05#$07#$03#$04; 
  SB_OID_TIME_STAMPING      : TBufferTypeConst = #$2B#$06#$01#$05#$05#$07#$03#$08; 

  SB_OID_UNSTRUCTURED_NAME  : TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$09#$02; 

  SB_OID_CERT_EXTENSIONS    : TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$09#$0e; 


const

  SB_ALGORITHM_CNT_RC4                  = SmallInt($7001);
  SB_ALGORITHM_CNT_DES                  = SmallInt($7002);
  SB_ALGORITHM_CNT_3DES                 = SmallInt($7003);
  SB_ALGORITHM_CNT_RC2                  = SmallInt($7004);
  SB_ALGORITHM_CNT_AES128               = SmallInt($7005);
  SB_ALGORITHM_CNT_AES192               = SmallInt($7006);
  SB_ALGORITHM_CNT_AES256               = SmallInt($7007);
  SB_ALGORITHM_CNT_IDENTITY             = SmallInt($700A);
  SB_ALGORITHM_CNT_BLOWFISH             = SmallInt($7010);
  SB_ALGORITHM_CNT_TWOFISH              = SmallInt($7011);
  SB_ALGORITHM_CNT_CAMELLIA             = SmallInt($7012);
  SB_ALGORITHM_CNT_CAST128              = SmallInt($7013);
  SB_ALGORITHM_CNT_IDEA                 = SmallInt($7014);
  SB_ALGORITHM_CNT_SERPENT              = SmallInt($7015);
  SB_ALGORITHM_DGST_SHA1                = SmallInt($7101);
  SB_ALGORITHM_DGST_MD5                 = SmallInt($7102);
  SB_ALGORITHM_DGST_MD2                 = SmallInt($7103);
  SB_ALGORITHM_DGST_SHA256              = SmallInt($7104);
  SB_ALGORITHM_DGST_SHA384              = SmallInt($7105);
  SB_ALGORITHM_DGST_SHA512              = SmallInt($7106);
  SB_ALGORITHM_DGST_SHA224              = SmallInt($7107);
  SB_ALGORITHM_DGST_MD4                 = SmallInt($7108);
  SB_ALGORITHM_DGST_RIPEMD160           = SmallInt($7109);
  SB_ALGORITHM_DGST_CRC32               = SmallInt($710A); // RFC 1510,ISO 3309
  { PKCS#12 PBE algorithms }
  SB_ALGORITHM_PBE_SHA1_RC4_128         = SmallInt($7201);
  SB_ALGORITHM_PBE_SHA1_RC4_40          = SmallInt($7202);
  SB_ALGORITHM_PBE_SHA1_3DES            = SmallInt($7203);
  SB_ALGORITHM_PBE_SHA1_RC2_128         = SmallInt($7204);
  SB_ALGORITHM_PBE_SHA1_RC2_40          = SmallInt($7205);
  { PKCS#5 PBES1 algorithms }
  SB_ALGORITHM_P5_PBE_MD2_DES           = SmallInt($7221);
  SB_ALGORITHM_P5_PBE_MD2_RC2           = SmallInt($7222);
  SB_ALGORITHM_P5_PBE_MD5_DES           = SmallInt($7223);
  SB_ALGORITHM_P5_PBE_MD5_RC2           = SmallInt($7224);
  SB_ALGORITHM_P5_PBE_SHA1_DES          = SmallInt($7225);
  SB_ALGORITHM_P5_PBE_SHA1_RC2          = SmallInt($7226);
  { PKCS#5 auxiliary algorithms }
  SB_ALGORITHM_P5_PBES1                 = SmallInt($7241);
  SB_ALGORITHM_P5_PBES2                 = SmallInt($7242);
  SB_ALGORITHM_P5_PBKDF1                = SmallInt($7251);
  SB_ALGORITHM_P5_PBKDF2                = SmallInt($7252);
  SB_ALGORITHM_P5_PBMAC1                = SmallInt($7261);

  SB_ALGORITHM_MAC_HMACSHA1             = SmallInt($7301);

  SB_ALGORITHM_UNKNOWN                  = SmallInt($7FFF);

  {$EXTERNALSYM ALG_CLASS_DATA_ENCRYPT}
  ALG_CLASS_DATA_ENCRYPT                = SmallInt((3 shl 13));
  {$EXTERNALSYM ALG_TYPE_BLOCK}
  ALG_TYPE_BLOCK                        = SmallInt((3 shl 9));
  {$EXTERNALSYM ALG_TYPE_STREAM}
  ALG_TYPE_STREAM                       = SmallInt((4 shl 9));
  {$EXTERNALSYM ALG_SID_DES}
  ALG_SID_DES                           = SmallInt(1);
  {$EXTERNALSYM ALG_SID_3DES}
  ALG_SID_3DES                          = SmallInt(3);
  {$EXTERNALSYM ALG_SID_RC2}
  ALG_SID_RC2                           = SmallInt(2);
  {$EXTERNALSYM ALG_SID_RC4}
  ALG_SID_RC4                           = SmallInt(1);

  {$EXTERNALSYM CALG_DES}
  CALG_DES                              = SmallInt(ALG_CLASS_DATA_ENCRYPT or ALG_TYPE_BLOCK or ALG_SID_DES);
  {$EXTERNALSYM CALG_3DES}
  CALG_3DES                             = SmallInt(ALG_CLASS_DATA_ENCRYPT or ALG_TYPE_BLOCK or ALG_SID_3DES);
  {$EXTERNALSYM CALG_RC2}
  CALG_RC2                              = SmallInt(ALG_CLASS_DATA_ENCRYPT or ALG_TYPE_BLOCK or ALG_SID_RC2);
  {$EXTERNALSYM CALG_RC4}
  CALG_RC4                              = SmallInt(ALG_CLASS_DATA_ENCRYPT or ALG_TYPE_STREAM or ALG_SID_RC4);

implementation

end.
