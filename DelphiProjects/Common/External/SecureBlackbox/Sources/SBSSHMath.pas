(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSSHMath;

interface

uses
  SBMath
  ;


procedure LSSHModPower(X, E, M, Y : PLInt); overload;procedure LSSHModPower(X, E, M, Y : PLInt; ProgressFunc : TSBMathProgressFunc;
  Data : pointer; RaiseExceptionOnCancel : boolean); overload;
implementation

resourcestring
  SMathOperationCanceled = 'Mathematical operation canceled';

type
  TLongWordArray = array[0..16384] of LongWord;
  TSBModExpContext = record
    bn : array[0..15] of PLInt;
  end;
  PSBModExpContext = ^TSBModExpContext;

  TSBModExpContextMont = record
    Ri : integer;
    RR : PLInt;
    N : PLInt;
    Ni : PLInt;
    N0 : cardinal;
  end;
  PSBModExpContextMont = ^TSBModExpContextMont;
  PLongWordArray = ^TLongWordArray;
  PLongWord = ^longword;

procedure LCreateExp(MontCtx : PSBModExpContextMont);
begin
  MontCtx.Ri := 0;
  LCreate(MontCtx.Rr);
  LCreate(MontCtx.N);
  MontCtx.Ni := nil;
end;

procedure LDestroyExp(MontCtx : PSBModExpContextMont);
begin
  LDestroy(MontCtx.Rr);
  LDestroy(MontCtx.N);
end;

procedure LInitializeExp(MontCtx : PSBModExpContextMont; M : PLInt; Ctx : PSBModExpContext);
var
  R, Ri : PLInt;
  TMod : PLInt;
  Buf : array[0..1] of cardinal;
begin
  Ri := nil;
  R := MontCtx.RR;
  LCopy(MontCtx.N, M);
  LCreate(Ri);
  LCreate(TMod); // now TMod contains 1
  MontCtx.Ri := ((LBitCount(M) + (32 - 1)) shr 5) shl 5;
  LShlNum(TMod, R, 32);
  Buf[0] := M.Digits[1];
  Buf[1] := 0;
  TMod.Digits[1] := Buf[0];
  TMod.Digits[2] := Buf[1];
  TMod.Length := 1;
  LGCD(R, TMod, Ctx.bn[0], Ri);
  if LNull(Ri) then
  begin
    Exit;
  end;
  LShiftLeft(Ri, 1);
  Ctx.Bn[0].Digits[1] := 1;
  Ctx.Bn[0].Length := 1;
  LSub(Ri, Ctx.Bn[0], Ctx.Bn[1]);
  LCopy(Ri, Ctx.Bn[1]);
  LDiv(Ri, TMod, Ctx.Bn[0], Ctx.Bn[1]);
  Ctx.Bn[1].Digits[1] := 1;
  Ctx.Bn[1].Length := 1;
  LAdd(Ctx.Bn[0], Ctx.Bn[1], Ri);
  MontCtx.n0 := Ri.Digits[1];
  LDestroy(Ri);
  Ctx.Bn[0].Digits[1] := 1;
  Ctx.Bn[0].Length := 1;
  LShlNum(Ctx.Bn[0], MontCtx.RR, MontCtx.Ri shl 1);
  LMod(MontCtx.RR, MontCtx.N, MontCtx.RR);
  LDestroy(TMod);
  LDestroy(Ri);
end;

procedure LMulAdd(R : plongword; A : plongword; W : plongword; C : plongword);
var
  T : TSBInt64;
begin
  T := TSBInt64(W^) * TSBInt64(A^) + TSBInt64(R^) + TSBInt64(C^);
  R^ := longword(T and $FFFFFFFF);
  C^ := longword((T shr 32) and $FFFFFFFF);
end;

function LMulAddWords(rp, ap : PLongWord; Num : integer; W : longword) : longword;
var
  C1 : longword;
begin
  C1 := 0;
  while true do
  begin
    LMulAdd(@PLongWordArray(rp)[0], @PLongWordArray(ap)[0], @w, @C1);
    Dec(Num);
    if (Num = 0) then
      Break;
    LMulAdd(@PLongWordArray(rp)[1], @PLongWordArray(ap)[1], @w, @C1);
    Dec(Num);
    if (Num = 0) then
      Break;
    LMulAdd(@PLongWordArray(rp)[2], @PLongWordArray(ap)[2], @w, @C1);
    Dec(Num);
    if (Num = 0) then
      Break;
    LMulAdd(@PLongWordArray(rp)[3], @PLongWordArray(ap)[3], @w, @C1);
    Dec(Num);
    if (Num = 0) then
      Break;
    Inc(Ap, 4);
    Inc(Rp, 4);
  end;
  Result := C1;
end;

function LFromMont(A : PLint; Res : PLInt; Mont : PSBModExpContextMont;
  Ctx : PSBModExpContext) : boolean;
var
  N, R : PLInt;
  Ap, Np, Rp : PLongWord;
  N0, V : LongWord;
  Al, Nl, Max, I, X, Ri : integer;
begin
  R := Ctx.Bn[0];
  LCopy(R, A);
  N := Mont.N;
  Al := Mont.Ri shr 5;
  Ri := Al;
  Nl := N.Length;
  Max := (Nl + Al + 1);
  np := @N.Digits[1];
  rp := @R.Digits[1];
  for I := R.Length + 1 to Max do
    R.Digits[I] := 0;
  R.Length := Max;
  N0 := Mont.N0;
  for I := 0 to Nl - 1 do
  begin
    V := LMulAddWords(Rp, Np, Nl, (PLongWordArray(Rp)[0] * n0) and $FFFFFFFF);
    Inc(PlongwordArray(Rp)[nl], V);
    if (PlongwordArray(Rp)[nl] and $FFFFFFFF) < V then
    begin
      X := nl + 1;
      Inc(PLongwordArray(rp)[x]);
      while (PLongwordArray(rp)[x] and $FFFFFFFF) = 0 do
        Inc(X);
    end;
    Inc(Rp);
  end;
  while (r.digits[r.length] = 0) do
    dec(r.length);
  ap := @r.digits[1];
  rp := @Res.digits[1];
  X := ri;
  al := r.length - x;
  for I := 0 to al - 1 do
    PlongwordArray(rp)[i] := PlongwordArray(ap)[I + x];
  Res.length := al;
  if LGreater(Res, Mont.N) then
  begin
    LSub(Res, Mont.N, Ctx.BN[0]);
    LCopy(Res, Ctx.BN[0]);
  end;
  Result := true;
end;

function LMModMult(A, B : PLInt; Mont : PSBModExpContextMont; Ctx : PSBModExpContext;
  Res : PLInt) : boolean;
var
  Tmp : PLInt;
begin
  Tmp := Ctx.Bn[0];
  if LEqual(A, B) then
    LMult(A, A, Tmp)
  else
    LMult(A, B, Tmp);
  LFromMont(Tmp, Res, Mont, Ctx);
  Result := true;
end;

function LMModPowerEx(A, P, M : PLInt; R : PLInt; Ctx : PSBModExpContext;
  ProgressFunc : TSBMathProgressFunc; Data : pointer; RaiseExceptionOnCancel : boolean) : boolean;
var
  I , J, Bits, WStart, WEnd, Window, WValue : integer;
  Start : integer;
  D, ADup : PLInt;
  Val : array[0..15] of PLInt;
  montCtx : TSBModExpContextMont;
begin
  for I := 0 to 15 do
    val[I] := nil;
  if (M.Digits[1] and 1) <> 1 then
  begin
    Result := false;
    Exit;
  end;
  D := Ctx.bn[15];
  bits := LBitCount(P);
  if (bits = 0) then
  begin
    R.Digits[1] := 1;
    R.length := 1;
    Result := true;
    Exit;
  end;
  Result := false;
  LCreateExp(@montCtx);
  LInitializeExp(@montCtx, M, Ctx);
  LCreate(val[0]);
  if LGreater(a, m) then
  begin
    LMod(a, m, val[0]);
    ADup := val[0];
  end
  else
    ADup := a;
  LMModMult(ADup, MontCtx.RR, @MontCtx, Ctx, Val[0]);
  LMModMult(val[0], val[0], @MontCtx, Ctx, D);
  if MathOperationCanceled(ProgressFunc, Data) then
    Exit;
  if Bits <= 20 then
    Window := 1
  else if Bits > 250 then
    Window := 5
  else if Bits >= 120 then
    Window := 4
  else
    Window := 3;
  J := 1 shl (Window - 1);
  for I := 1 to J - 1 do
  begin
    LCreate(Val[I]);
    LMModMult(val[I - 1], D, @MontCtx, Ctx, val[i]);
    if MathOperationCanceled(ProgressFunc, Data) then
      Exit;
  end;
  Start := 1;
  WStart := Bits - 1;
  Ctx.bn[9].Digits[1] := 1;
  Ctx.bn[9].Length := 1;
  LMModMult(Ctx.Bn[9], MontCtx.RR, @MontCtx, Ctx, R);
  while true do
  begin
    if MathOperationCanceled(ProgressFunc, Data) then
      Exit;
    if not LBitSet(P, wstart) then
    begin
      if Start = 0 then
      begin
        LMModMult(R, R, @MontCtx, Ctx, Ctx.bn[6]);
        LCopy(R, Ctx.bn[6]);
      end;
      if WStart = 0 then
        Break;
      Dec(Wstart);
      Continue;
    end;
    WValue := 1;
    WEnd := 0;
    for I := 1 to Window - 1 do
    begin
      if Wstart - I < 0 then
        Break;
      if LBitSet(P, WStart - I) then
      begin
        WValue := WValue shl (I - Wend);
        WValue := WValue or 1;
        WEnd := I;
      end;
    end;
    J := Wend + 1;
    if Start = 0 then
      for I := 0 to J - 1 do
      begin
        if MathOperationCanceled(ProgressFunc, Data) then
          Exit;
        LMModMult(R, R, @MontCtx, Ctx, Ctx.BN[7]);
        LCopy(R, Ctx.BN[7]);
      end;
    LMModMult(R, Val[WValue shr 1], @MontCtx, Ctx, Ctx.BN[7]);
    LCopy(R, Ctx.BN[7]);
    Dec(WStart, Wend + 1);
    Start := 0;
    if WStart < 0 then
      Break;
  end;
  LFromMont(R, Ctx.BN[8], @MontCtx, Ctx);
  LCopy(R, Ctx.BN[8]);
  LTrim(R);
  LDestroyExp(@MontCtx);
  for I := 0 to 15 do
    if Assigned(Val[I]) then
      LDestroy(Val[I]);
  Result := true;
end;

procedure LSSHModPower(X, E, M, Y : PLInt);
var
  Ctx : TSBModExpContext;
  I : integer;
begin
  for I := 0 to 15 do
    LCreate(Ctx.BN[I]);
  LMModPowerEx(X, E, M, Y, @Ctx, nil, nil, false);
  for I := 0 to 15 do
    LDestroy(Ctx.BN[I]);
end;

procedure LSSHModPower(X, E, M, Y : PLInt; ProgressFunc : TSBMathProgressFunc;
  Data : pointer; RaiseExceptionOnCancel : boolean);
var
  Ctx : TSBModExpContext;
  I : integer;
begin
  for I := 0 to 15 do
    LCreate(Ctx.BN[I]);
  try
    try
      LMModPowerEx(X, E, M, Y, @Ctx, ProgressFunc, Data, true);
    except
      if RaiseExceptionOnCancel then
        raise EElMathException.Create(SMathOperationCanceled);
    end;
  finally
    for I := 0 to 15 do
      LDestroy(Ctx.BN[I]);
  end;
end;

end.