
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBIDEA;

interface

uses
  SBUtils;

{$IFNDEF SBB_NO_IDEA}

  
const
  TIDEABufferSize = 8;
  TIDEAExpandedKeySize = 52 * 2;

type
  TIDEAKey =  array[0..15] of byte;
  PIDEAKey = ^TIDEAKey;

  TIDEABuffer =  array[0..7] of byte;
  PIDEABuffer = ^TIDEABuffer;

  TIDEAExpandedKey = array[0..51] of word;
  PIDEAExpandedKey = ^TIDEAExpandedKey;

  TIDEAContext = packed  record
    ExpandedKey: TIDEAExpandedKey;
    Vector: TIDEABuffer;
    Tail: TIDEABuffer;
    TailLen: cardinal;
    BytesLeft : integer;
  end;

// Key expansion routines
procedure ExpandKeyForEncryption(const Key: TIDEAKey; 
  out ExpandedKey: TIDEAExpandedKey); 
procedure ExpandKeyForDecryption(const CipherKey: TIDEAExpandedKey;
  out DecipherKey: TIDEAExpandedKey); 

// Block processing routines
procedure Encrypt(const Input: TIDEABuffer; 
  const Key: TIDEAExpandedKey; out Output: TIDEABuffer); 
procedure Decrypt(const Input: TIDEABuffer; 
  const Key: TIDEAExpandedKey; out Output: TIDEABuffer); 


// Memory processing routines
// CBC Mode
function EncryptCBC(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TIDEAExpandedKey; const InitVector: TIDEABuffer): boolean; overload; 
function DecryptCBC(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TIDEAExpandedKey; const InitVector: TIDEABuffer): boolean; overload; 

// Chunks processing routines
// CBC Mode
procedure InitializeEncryptionCBC(var Context: TIDEAContext; const Key: TIDEAKey; 
  const InitVector: TIDEABuffer); 
procedure InitializeDecryptionCBC(var Context: TIDEAContext; const Key: TIDEAKey; 
  const InitVector: TIDEABuffer); 

function EncryptCBC(var Context: TIDEAContext; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean; overload;

function DecryptCBC(var Context: TIDEAContext; Chunk: pointer; ChunkSize:
  cardinal;
  OutBuf: pointer; var OutSize: cardinal): boolean; overload;
function FinalizeEncryptionCBC(var Context: TIDEAContext; OutBuf: pointer;
  var OutSize: cardinal): boolean;
function FinalizeDecryptionCBC(var Context: TIDEAContext; OutBuf: pointer;
  var OutSize: cardinal): boolean;

// CFB Mode
procedure InitializeEncryptionCFB(var Context: TIDEAContext;
  const Key: TIDEAKey; const InitVector: TIDEABuffer);
procedure InitializeDecryptionCFB(var Context: TIDEAContext;
  const Key: TIDEAKey; const InitVector: TIDEABuffer);
function EncryptCFB(var Context: TIDEAContext; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean; overload;
function DecryptCFB(var Context: TIDEAContext; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean; overload;

{ CFB 8-bit mode routines }
procedure InitializeEncryptionCFB8(var Context: TIDEAContext; const Key:
  TIDEAKey; const InitVector: TIDEABuffer);
procedure InitializeDecryptionCFB8(var Context: TIDEAContext; const Key: TIDEAKey;
  const InitVector: TIDEABuffer);
function EncryptCFB8(var Context: TIDEAContext; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean; overload;
function DecryptCFB8(var Context: TIDEAContext; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean; overload;
function FinalizeEncryptionCFB8(var Context: TIDEAContext): boolean;
function FinalizeDecryptionCFB8(var Context: TIDEAContext): boolean;

{$ENDIF SBB_NO_IDEA}

var
  IdeaEnabled : boolean = true;

implementation

{$IFNDEF SBB_NO_IDEA}

uses
  SysUtils;


// Internal functions

function Multiply(X, Y: word): word;  register;
var
  A, B: word;
  P: longword;
begin
  P := X * Y;
  if P = 0 then
    Result := $10001 - X - Y
  else
  begin
    A := LongRec(P).Hi;
    B := LongRec(P).Lo;
    A := B - A;
    if B < A then
      Inc(A, $10001);
    Result := A;
  end;
end;

function Invert(X: word): word;
var
  A, B, C, D: word;
begin
  if X <= 1 then
    Result := X
  else
  begin
    A := 1;
    B := $10001 div X;
    C := $10001 mod X;
    while C <> 1 do
    begin
      D := X div C;
      X := X mod C;
      Inc(A, B * D);
      if X = 1 then
      begin
        Result := A;
        exit;
      end;
      D := C div X;
      C := C mod X;
      Inc(B, A * D);
    end;
    Result := 1 - B;
  end;
end;

// Key expansion routines

procedure ExpandKeyForEncryption(const Key: TIDEAKey;
  out ExpandedKey: TIDEAExpandedKey);
var
  I: integer;
begin

  for I := 0 to 7 do
    ExpandedKey[I] := (Key[I * 2] shl 8) or Key[I * 2 + 1];
  for I := 0 to 39 do
    ExpandedKey[I + 8] := (ExpandedKey[I and not 7 + (I + 1) and 7] shl 9) or
    (ExpandedKey[I and not 7 + (I + 2) and 7] shr 7);
  for I := 41 to 44 do
    ExpandedKey[I + 7] := (ExpandedKey[I] shl 9) or (ExpandedKey[I + 1] shr 7);
end;

procedure ExpandKeyForDecryption(const CipherKey: TIDEAExpandedKey; 
    out DecipherKey: TIDEAExpandedKey);
var
  A, B, C: Word;
  I, CI, DI: integer;
begin

  A := Invert(CipherKey[0]);
  B := 65536 - CipherKey[1];
  C := 65536 - CipherKey[2];
  DecipherKey[51] := Invert(CipherKey[3]);
  DecipherKey[50] := C;
  DecipherKey[49] := B;
  DecipherKey[48] := A;
  CI := 4;
  DI := 48;
  for I := 0 to 7 do
  begin
    Dec(DI, 6);
    A := CipherKey[CI];
    DecipherKey[DI + 5] := CipherKey[CI + 1];
    DecipherKey[DI + 4] := A;
    A := Invert(CipherKey[CI + 2]);
    B := 65536 - CipherKey[CI + 3];
    C := 65536 - CipherKey[CI + 4];
    DecipherKey[DI + 3] := Invert(CipherKey[CI + 5]);
    DecipherKey[DI + 2] := B;
    DecipherKey[DI + 1] := C;
    DecipherKey[DI] := A;
    Inc(CI, 6);
  end;
  A := DecipherKey[DI + 2];
  DecipherKey[DI + 2] := DecipherKey[DI + 1];
  DecipherKey[DI + 1] := A;
end;

// Block processing routines

procedure Encrypt(const Input: TIDEABuffer; const Key: TIDEAExpandedKey;
    out Output: TIDEABuffer);
var
  I: integer;
  A, B, C, D, X, Y: word;
  PKey: PWordArray;
  P: PWord;
begin
  P := @Input[0]; A := (P^ shr 8) or (P^ shl 8);
  P := @Input[2]; B := (P^ shr 8) or (P^ shl 8);
  P := @Input[4]; C := (P^ shr 8) or (P^ shl 8);
  P := @Input[6]; D := (P^ shr 8) or (P^ shl 8);
  PKey := @Key;
  for I := 0 to 7 do
  begin
    A := Multiply(A, PKey[0]);
    Inc(B, PKey[1]);
    Inc(C, PKey[2]);
    D := Multiply(D, PKey[3]);
    Y := C;
    C := Multiply(C xor A, PKey[4]);
    X := B;
    B := Multiply(B xor D + C, PKey[5]);
    C := C + B;
    A := A xor B;
    D := D xor C;
    B := B xor Y;
    C := C xor X;
    Inc(PWord(PKey), 6);
  end;
  A := Multiply(A, PKey[0]);
  Inc(C, PKey[1]);
  Inc(B, PKey[2]);
  D := Multiply(D, PKey[3]);
  PWord(@Output[0])^ := (A shr 8) or (A shl 8);
  PWord(@Output[2])^ := (C shr 8) or (C shl 8);
  PWord(@Output[4])^ := (B shr 8) or (B shl 8);
  PWord(@Output[6])^ := (D shr 8) or (D shl 8);

end;

procedure Decrypt(const Input: TIDEABuffer; const Key: TIDEAExpandedKey;
    out Output: TIDEABuffer);
begin
  Encrypt(Input, Key, Output);
end;

// Memory processing routines

// CBC Mode

function EncryptCBC(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TIDEAExpandedKey; const InitVector: TIDEABuffer): boolean;
var
  Vector, Temp: TIDEABuffer;
  S: cardinal;
begin
  S := (InSize div SizeOf(TIDEABuffer)) * SizeOf(TIDEABuffer);
  if (InSize mod SizeOf(TIDEABuffer)) <> 0 then
    S := S + SizeOf(TIDEABuffer);
  if OutSize < S then
  begin
    OutSize := S;
    Result := False;
    exit;
  end;
  Result := True;
  OutSize := 0;
  Vector := InitVector;
  while InSize >= SizeOf(TIDEABuffer) do
  begin
    PLongWord(@Temp[0])^ := PLongWord(InBuffer)^ xor PLongWord(@Vector[0])^;
    Inc(PtrUInt(InBuffer), 4);
    PLongWord(@Temp[4])^ := PLongWord(InBuffer)^ xor PLongWord(@Vector[4])^;
    Inc(PtrUInt(InBuffer), 4);
    Encrypt(Temp, ExpandedKey, PIDEABuffer(OutBuffer)^);
    Vector := PIDEABuffer(OutBuffer)^;
    Dec(InSize, SizeOf(TIDEABuffer));
    Inc(PtrUInt(OutBuffer), SizeOf(TIDEABuffer));
    Inc(OutSize, SizeOf(TIDEABuffer));
  end;
  if InSize > 0 then
  begin
    Move(InBuffer^, Temp, InSize);
    FillChar(Temp[InSize], SizeOf(Temp) - InSize, 0);
    PLongWord(@Temp[0])^ := PLongWord(@Temp[0])^ xor PLongWord(@Vector[0])^;
    PLongWord(@Temp[4])^ := PLongWord(@Temp[4])^ xor PLongWord(@Vector[4])^;
    Encrypt(Temp, ExpandedKey, PIDEABuffer(OutBuffer)^);
    Inc(OutSize, SizeOf(TIDEABuffer));
  end;
end;

function DecryptCBC(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TIDEAExpandedKey; const InitVector: TIDEABuffer): boolean;
var
  TempIn, TempOut: PIDEABuffer;
  Vector1, Vector2: TIDEABuffer;
  P: PLongWord;
begin
  if (InSize mod SizeOf(TIDEABuffer)) > 0 then
    raise EEncryptionError.Create(SInvalidInputSize);
  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := False;
    exit;
  end;
  Result := True;
  OutSize := 0;
  Vector1 := InitVector;
  while InSize >= SizeOf(TIDEABuffer) do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Vector2 := TempIn^;
    Decrypt(TempIn^, ExpandedKey, TempOut^);
    P := @TempOut^[0]; P^ := P^ xor PLongWord(@Vector1[0])^;
    P := @TempOut^[4]; P^ := P^ xor PLongWord(@Vector1[4])^;
    Vector1 := Vector2;
    Dec(InSize, SizeOf(TIDEABuffer));
    Inc(OutSize, SizeOf(TIDEABuffer));
  end;
end;

// Chunks processing routines

// CBC Mode

procedure InitializeEncryptionCBC(var Context: TIDEAContext;
  const Key: TIDEAKey; 
  const InitVector: TIDEABuffer);
begin

  ExpandKeyForEncryption(Key, Context.ExpandedKey);
  Context.Vector := InitVector;
  Context.TailLen := 0;
end;

procedure InitializeDecryptionCBC(var Context: TIDEAContext;
  const Key: TIDEAKey; 
  const InitVector: TIDEABuffer);
var
  ExKey: TIDEAExpandedKey;
begin

  ExpandKeyForEncryption(Key, ExKey);
  ExpandKeyForDecryption(ExKey, Context.ExpandedKey);
  Context.Vector := InitVector;
  Context.TailLen := 0;
end;



function EncryptCBC(var Context: TIDEAContext; Chunk: pointer; ChunkSize:
  cardinal;
  OutBuf: pointer; var OutSize: cardinal): boolean;
var
  Left, Needed: cardinal;
  Temp: TIDEABuffer;
begin
  if ChunkSize = 0 then
  begin
    OutSize := 0;
    Result := True;
    exit;
  end;
  Needed := ((Context.TailLen + ChunkSize) div SizeOf(TIDEABuffer)) *
    SizeOf(TIDEABuffer);
  if Needed > OutSize then
  begin
    OutSize := Needed;
    Result := False;
    exit;
  end;
  if Context.TailLen > 0 then
  begin
    Left := SizeOf(TIDEABuffer) - Context.TailLen;
    if Left > ChunkSize then
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], ChunkSize);
      Inc(Context.TailLen, ChunkSize);
      ChunkSize := 0;
    end
    else
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], Left);
      Dec(ChunkSize, Left);
      PLongWord(@Temp[0])^ := PLongWord(@Context.Tail[0])^ xor
        PLongWord(@Context.Vector[0])^;
      PLongWord(@Temp[4])^ := PLongWord(@Context.Tail[4])^ xor
        PLongWord(@Context.Vector[4])^;
      Encrypt(Temp, Context.ExpandedKey, PIDEABuffer(OutBuf)^);
      Context.Vector := PIDEABuffer(OutBuf)^;
      Inc(PtrUInt(OutBuf), SizeOf(TIDEABuffer));
      Inc(PtrUInt(Chunk), Left);
      Context.TailLen := 0;
    end;
  end;
  while ChunkSize >= SizeOf(TIDEABuffer) do
  begin
    PLongWord(@Temp[0])^ := PLongWord(Chunk)^ xor
      PLongWord(@Context.Vector[0])^;
    Inc(PtrUInt(Chunk), 4);
    PLongWord(@Temp[4])^ := PLongWord(Chunk)^ xor
      PLongWord(@Context.Vector[4])^;
    Inc(PtrUInt(Chunk), 4);
    Encrypt(Temp, Context.ExpandedKey, PIDEABuffer(OutBuf)^);
    Context.Vector := PIDEABuffer(OutBuf)^;
    Inc(PtrUInt(OutBuf), SizeOf(TIDEABuffer));
    Dec(ChunkSize, SizeOf(TIDEABuffer));
  end;
  if ChunkSize > 0 then
  begin
    Move(Chunk^, Context.Tail[0], ChunkSize);
    Context.TailLen := ChunkSize;
  end;
  OutSize := Needed;
  Result := True;
end;

function DecryptCBC(var Context: TIDEAContext; Chunk: pointer; ChunkSize:
  cardinal;
  OutBuf: pointer; var OutSize: cardinal): boolean;
var
  Left, Needed: cardinal;
  NextVector: TIDEABuffer;
begin
  if ChunkSize = 0 then
  begin
    OutSize := 0;
    Result := True;
    exit;
  end;
  Needed := ((Context.TailLen + ChunkSize) div SizeOf(TIDEABuffer)) *
    SizeOf(TIDEABuffer);
  if Needed > OutSize then
  begin
    OutSize := Needed;
    Result := False;
    exit;
  end;
  if Context.TailLen > 0 then
  begin
    Left := SizeOf(TIDEABuffer) - Context.TailLen;
    if Left > ChunkSize then
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], ChunkSize);
      Inc(Context.TailLen, ChunkSize);
      ChunkSize := 0;
    end
    else
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], Left);
      Inc(PtrUInt(Chunk), Left);
      Dec(ChunkSize, Left);
      NextVector := Context.Tail;
      Decrypt(Context.Tail, Context.ExpandedKey, PIDEABuffer(OutBuf)^);
      PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
        PLongWord(@Context.Vector[0])^;
      Inc(PtrUInt(OutBuf), 4);
      PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
        PLongWord(@Context.Vector[4])^;
      Inc(PtrUInt(OutBuf), 4);
      Context.Vector := NextVector;
      Context.TailLen := 0;
    end;
  end;
  while ChunkSize >= SizeOf(TIDEABuffer) do
  begin
    NextVector := PIDEABuffer(Chunk)^;
    Decrypt(PIDEABuffer(Chunk)^, Context.ExpandedKey, PIDEABuffer(OutBuf)^);
    PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor PLongWord(@Context.Vector[0])^;
    Inc(PtrUInt(OutBuf), 4);
    PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor PLongWord(@Context.Vector[4])^;
    Inc(PtrUInt(OutBuf), 4);
    Context.Vector := NextVector;
    Inc(PtrUInt(Chunk), SizeOf(TIDEABuffer));
    Dec(ChunkSize, SizeOf(TIDEABuffer));
  end;
  if ChunkSize > 0 then
  begin
    Move(Chunk^, Context.Tail[0], ChunkSize);
    Context.TailLen := ChunkSize;
  end;
  OutSize := Needed;
  Result := True;
end;

function FinalizeEncryptionCBC(var Context: TIDEAContext; OutBuf: pointer;
  var OutSize: cardinal): boolean;
begin
  if Context.TailLen > 0 then
  begin
    if OutSize < SizeOf(TIDEABuffer) then
    begin
      OutSize := SizeOf(TIDEABuffer);
      Result := False;
    end
    else
    begin
      FillChar(Context.Tail[Context.TailLen], SizeOf(TIDEABuffer) -
        Context.TailLen, 0);
      PLongWord(@Context.Tail[0])^ := PLongWord(@Context.Tail[0])^ xor
        PLongWord(@Context.Vector[0])^;
      PLongWord(@Context.Tail[4])^ := PLongWord(@Context.Tail[4])^ xor
        PLongWord(@Context.Vector[4])^;
      Encrypt(Context.Tail, Context.ExpandedKey, PIDEABuffer(OutBuf)^);
      OutSize := SizeOf(TIDEABuffer);
      Result := True;
    end;
  end
  else
  begin
    OutSize := 0;
    Result := True;
  end;
end;

function FinalizeDecryptionCBC(var Context: TIDEAContext; OutBuf: pointer;
  var OutSize: cardinal): boolean;
begin
  if Context.TailLen > 0 then
  begin
    if Context.TailLen < SizeOf(TIDEABuffer) then
    begin
      OutSize := 0;
      Result := False;
    end
    else
      if OutSize < SizeOf(TIDEABuffer) then
      begin
        OutSize := SizeOf(TIDEABuffer);
        Result := False;
      end
      else
      begin
        Decrypt(Context.Tail, Context.ExpandedKey, PIDEABuffer(OutBuf)^);
        PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
          PLongWord(@Context.Vector[0])^;
        Inc(PtrUInt(OutBuf), 4);
        PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
          PLongWord(@Context.Vector[4])^;
        OutSize := SizeOf(TIDEABuffer);
        Result := True;
      end;
  end
  else
  begin
    OutSize := 0;
    Result := True;
  end;
end;

// CFB64 mode

procedure InitializeEncryptionCFB(var Context: TIDEAContext;
  const Key: TIDEAKey; 
  const InitVector: TIDEABuffer);
begin
  InitializeEncryptionCBC(Context, Key, InitVector);
  Encrypt(InitVector, Context.ExpandedKey, Context.Vector);
end;

procedure InitializeDecryptionCFB(var Context: TIDEAContext;
  const Key: TIDEAKey;
  const InitVector: TIDEABuffer);
begin
  InitializeEncryptionCBC(Context, Key, InitVector);
  Encrypt(InitVector, Context.ExpandedKey, Context.Vector);
end;

function EncryptCFB(var Context: TIDEAContext; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean; overload;
var
  I : cardinal;
  P1, P2 : pointer;
begin
  if (ChunkSize mod 8 <> 0) or (OutSize < ChunkSize) then
  begin
    Result := false;
    OutSize := 0;
    Exit;
  end;
  Result := true;
  I := 0;
  while I < ChunkSize do
  begin
    P1 := pointer(PtrUInt(Chunk) + I);
    P2 := pointer(PtrUInt(OutBuf) + I);
    PInt64(P2)^ := PInt64(P1)^ xor PInt64(@Context.Vector)^;
    PInt64(@Context.Vector)^ := PInt64(P2)^;
    Encrypt(Context.Vector, Context.ExpandedKey, Context.Vector);
    Inc(I, 8);
  end;
  OutSize := ChunkSize;
end;

function DecryptCFB(var Context: TIDEAContext; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean; overload;
var
  I : cardinal;
  P1, P2 : pointer;
  Temp : TIDEABuffer;
begin
  if (ChunkSize mod 8 <> 0) or (OutSize < ChunkSize) then
  begin
    Result := false;
    OutSize := 0;
    Exit;
  end;
  I := 0;
  Result := true;
  while I < ChunkSize do
  begin
    P1 := pointer(PtrUInt(Chunk) + I);
    P2 := pointer(PtrUInt(OutBuf) + I);
    PInt64(P2)^ := PInt64(P1)^ xor PInt64(@Context.Vector)^;
    PInt64(@Context.Vector)^ := PInt64(P1)^;
    Encrypt(Context.Vector, Context.ExpandedKey, Temp);
    PInt64(@Context.Vector)^ := PInt64(@Temp)^;
    Inc(I, 8);
  end;
  OutSize := ChunkSize;
end;


procedure InitializeEncryptionCFB8(var Context: TIDEAContext;
  const Key: TIDEAKey;
  const InitVector: TIDEABuffer);
begin
  InitializeEncryptionCFB(Context,Key,InitVector);
  Context.BytesLeft := 8;
end;

procedure InitializeDecryptionCFB8(var Context: TIDEAContext;
  const Key: TIDEAKey;
  const InitVector: TIDEABuffer);
begin
  InitializeDecryptionCFB(Context,Key,InitVector);
  Context.BytesLeft := 8;
end;

{ CFB 8-bit mode }

function EncryptCFB8(var Context: TIDEAContext; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean; overload;
var
  I : cardinal;
  J : integer;
  P1, P2 : pointer;
  Temp : TIDEABuffer;
begin
  if (OutSize < ChunkSize) then
  begin
    Result := false;
    OutSize := 0;
    Exit;
  end;
  Result := true;
  I := 0;
  while I < ChunkSize do
  begin
    P1 := pointer(PtrUInt(Chunk) + I);
    P2 := pointer(PtrUInt(OutBuf) + I);
    PByte(P2)^ := PByte(P1)^ xor PByte(@Context.Vector)^;
    for J:=0 to 6 do
      Context.Vector[J]:=Context.Vector[J+1];
    Context.Vector[7]:=PByte(P2)^;
    Dec(Context.BytesLeft);
    if Context.BytesLeft = 0 then
    begin
      Encrypt(Context.Vector, Context.ExpandedKey, Temp);
      Context.Vector := Temp;
      Context.BytesLeft := 8;
    end;
    Inc(I);
  end;
  OutSize := ChunkSize;
end;

function DecryptCFB8(var Context: TIDEAContext; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean; overload;
var
  I : cardinal;
  J : integer;
  P1, P2 : pointer;
  Temp : TIDEABuffer;
begin
  if (OutSize < ChunkSize) then
  begin
    Result := false;
    OutSize := 0;
    Exit;
  end;
  Result := true;
  I := 0;
  while I < ChunkSize do
  begin
    P1 := pointer(PtrUInt(Chunk) + I);
    P2 := pointer(PtrUInt(OutBuf) + I);
    PByte(P2)^ := PByte(P1)^ xor PByte(@Context.Vector)^;

    for J:=0 to 6 do
      Context.Vector[J]:=Context.Vector[J+1];

    Context.Vector[7]:=PByte(P1)^;

    Dec(Context.BytesLeft);
    if Context.BytesLeft = 0 then
    begin
      Encrypt(Context.Vector, Context.ExpandedKey, Temp);
      Context.Vector := Temp;
      Context.BytesLeft := 8;
    end;  
    Inc(I);
  end;
  OutSize := ChunkSize;
end;

function FinalizeEncryptionCFB8(var Context: TIDEAContext): boolean;
begin
  Result:=true;
end;

function FinalizeDecryptionCFB8(var Context: TIDEAContext): boolean;
begin
  Result:=true;
end;


{$ENDIF SBB_NO_IDEA}

end.
