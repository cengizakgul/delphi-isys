(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBX509;

(*

Change history (not complete)

24 June 2005

  Added PKCS8 support

21 June 2005

  Fixed DetectKeyFileFormat() method, which didn't work correctly

*)

interface

uses
{$IFNDEF CLX_USED}
  Windows,
  Activex,
  {$ifdef SB_HAS_WINCRYPT}
  SBWinCrypt,
  {$endif}
{$ELSE}
  //Libc,
{$ENDIF}
  Classes,
  SysUtils,
  SBUtils,
  SBMD,
  SBSHA,
  SBSHA2,
  SBRIPEMD,
  SBMath,
  SBRDN,
  SBASN1,
  SBPKCS10,
  SBPEM,
  SBDSA,
  SBRSA,
  SBHashFunction,
  SBPKIAsync,
  SBX509Ext,
{$IFDEF SECURE_BLACKBOX_DEBUG}
  SBDumper,
{$ENDIF}
  SBASN1Tree
;



const
  SB_X509_ERROR_INVALID_PVK_FILE = Integer($5001);
  SB_X509_ERROR_INVALID_PASSWORD = Integer($5002);
  SB_X509_ERROR_NO_PRIVATE_KEY = Integer($5003);
  SB_X509_ERROR_UNSUPPORTED_ALGORITHM = Integer($5004);
  SB_X509_ERROR_INVALID_PRIVATE_KEY = Integer($5005);
  SB_X509_ERROR_INTERNAL_ERROR = Integer($5006);
  SB_X509_ERROR_BUFFER_TOO_SMALL = Integer($5007);

  BT_WINDOWS = 1;
  BT_PKCS11 = 2;
  BT_WAB = 4;
  BT_OUTLOOK = 8;
  BT_FILE = 16;

type

  TSBCertFileFormat = (cfUnknown, cfDER, cfPEM, cfPFX, {cfMSBLOB, } cfSPC);
  TSBX509KeyFileFormat = (kffUnknown, kffDER, kffPEM, kffPFX, kffPVK, kffNET);

  // New implementation
  TElEncryptionParameters = class
  protected
    FAlgorithm: integer;
    procedure LoadPublicKey(Tag: TElASN1ConstrainedTag); virtual; abstract;
  public
    procedure Assign(Source : TElEncryptionParameters); virtual;
    property Algorithm: integer read FAlgorithm;
  end;

  TElRSAEncryptionParameters =  class(TElEncryptionParameters)
  private
    FPublicExponent: ByteArray;
    FPrivateExponent: ByteArray;
    FPublicModulus: ByteArray;
  protected
    procedure LoadPublicKey(Tag: TElASN1ConstrainedTag); override;
  public
    procedure Assign(Source : TElEncryptionParameters); override;
    property PublicExponent: ByteArray read FPublicExponent;
    property PrivateExponent: ByteArray read FPrivateExponent;
    property PublicModulus: ByteArray read FPublicModulus;
  end;

  TElRSAPSSSignParameters =  class(TElRSAEncryptionParameters)
  private
    FSaltSize : integer;
    FHashAlgorithm : integer;
    FTrailerField : integer;
    FMGF : integer;
    FMGFHashAlgorithm : integer;
  public
    constructor Create;
    procedure Assign(Source : TElEncryptionParameters); override;
    property HashAlgorithm : integer read FHashAlgorithm write FHashAlgorithm;
    property SaltSize : integer read FSaltSize write FSaltSize;
    property TrailerField : integer read FTrailerField write FTrailerField;
    property MGF : integer read FMGF write FMGF;
    property MGFHashAlgorithm : integer read FMGFHashAlgorithm write FMGFHashAlgorithm;
  end;

  TElRSAOAEPEncryptionParameters =  class(TElRSAEncryptionParameters)
  private
    FHashAlgorithm : integer;
    FStrLabel : string;
    FMGFHashAlgorithm : integer;
    FMGF : integer;
  public
    constructor Create;
    procedure Assign(Source : TElEncryptionParameters); override;
    property HashAlgorithm : integer read FHashAlgorithm write FHashAlgorithm;
    property MGF : integer read FMGF write FMGF;
    property MGFHashAlgorithm : integer read FMGFHashAlgorithm write FMGFHashAlgorithm;
    property StrLabel : string read FStrLabel write FStrLabel;
  end;

  TElDSAEncryptionParameters =  class(TElEncryptionParameters)
  private
    FP: ByteArray;
    FQ: ByteArray;
    FG: ByteArray;
    FY: ByteArray;
    FX: ByteArray;
  protected
    procedure LoadPublicKey(Tag: TElASN1ConstrainedTag); override;
  public
    procedure Assign(Source : TElEncryptionParameters); override;
    property P: ByteArray read FP write FP;
    property Q: ByteArray read FQ write FQ;
    property G: ByteArray read FG write FG;
    property Y: ByteArray read FY write FY;
    property X: ByteArray read FX write FX;
  end;

  TElDHEncryptionParameters =  class(TElEncryptionParameters)
  private
    FP: ByteArray;
    FQ: ByteArray;
    FG: ByteArray;
    FY: ByteArray;
    FX: ByteArray;
  protected
    procedure LoadPublicKey(Tag: TElASN1ConstrainedTag); override;
  public
    procedure Assign(Source : TElEncryptionParameters); override;  
    property P: ByteArray read FP write FP;
    property Q: ByteArray read FQ write FQ;
    property G: ByteArray read FG write FG;
    property Y: ByteArray read FY write FY;
    property X: ByteArray read FX write FX;
  end;

  TValidity = record
    NotBefore: TDateTime;
    NotAfter: TDateTime;
  end;

  TName =  record
    Country: string;
    StateOrProvince: string;
    Locality: string;
    Organization: string;
    OrganizationUnit: string;
    CommonName: string;
    EMailAddress: string;
  end;

  TLongRSAParams = record
    PrivateExponent: PLInt;
    PublicKey: PLInt;
    Modulus: PLInt;
  end;
  PLongRSAParams = ^TLongRSAParams;

  TLongDHParams = record
    P: PLInt;
    G: PLInt;
    X: PLInt;
    Ys: PLInt;
  end;
  PLongDHParams = ^TLongDHParams;
  TLongDSAParams =  record
    P: PLInt;
    Q: PLInt;
    G: PLInt;
    X: PLInt;
    Y: PLInt;
  end;
  PLongDSAParams = ^TLongDSAParams;

  TElSubjectPublicKeyInfo =  class
  private
    FEncryptionParameters: TElEncryptionParameters;
    FPublicKeyAlgorithm: integer;
    FRawData : ByteArray;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Clear;
    //procedure Load(Tag: TElASN1ConstrainedTag);
    property PublicKeyAlgorithm: integer read FPublicKeyAlgorithm;
    property EncryptionParameters: TElEncryptionParameters read FEncryptionParameters;
    property RawData : ByteArray read FRawData;
  end;

  TElTBSCertificate =  class
  private
    FVersion: Byte; // v1(0), v2(1), v3(2)
    FSerialNumber: BufferType;
    FSignature: TElEncryptionParameters;
    Validity: TValidity;
    FIssuer: TStringList;
    FSubject: TStringList;
    FSubjectPublicKeyInfo: TElSubjectPublicKeyInfo;
    FIssuerUniqueID: BufferType; //If present, version shall be v2 or v3
    FSubjectUniqueID: BufferType; //If present, version shall be v2 or v3
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
  end;

  TElBaseCertStorage =  class(TSBControlBase);

  TElX509Certificate = class(TSBControlBase)
  private
  protected
    Count: Word; // Pointer to current byte while parsing proccessed
    FtbsCertificate: TElTBSCertificate;
    FSignatureAlgorithm: TElEncryptionParameters;
    FPSSParams : TElRSAPSSSignParameters;
    FOAEPParams : TElRSAOAEPEncryptionParameters;
    FParent : TElX509Certificate; //used only in generation

    FSignatureValue: BufferType;
    FIssuerName: TName;
    FSubjectName: TName;

    FErrorCode: Byte;
    FPData: PByteArray; // Pointer to internal Certificate binary data

    FAllSize: integer;
    FCertificateSize: integer; // length of Certificate block
    FCertificateOffset: integer; // tbsCertificate data offset
    FSignatureAlgorithmInt: integer;

    FLongRSAEncryptParams: TLongRSAParams;
    FLongDHEncryptParams: TLongDHParams;
    FLongRSACAEncryptParams: TLongRSAParams;
    FLongDSAEncryptParams: TLongDSAParams;
    FLongDSACAEncryptParams: TLongDSAParams;
    FGeneratingFromRequest: boolean;

    FGenEncryptAlgorithm: integer;
    FGenSignatureAlgorithm: integer;

    FNewSubject: TName;
    FNewIssuer: TName;

    FCAAvailable: boolean;
    FCACertType: integer;
    FCACertTypeSign: integer;
    FCAKeyIdentifier: BufferType;
    FOurKeyIdentifier: BufferType;
    FGenerate: boolean;
    FPrivateKey: ByteArray;

    FLastOID: BufferType;
    FCertificateExtensions: TElCertificateExtensions;
    FLastExtensionOID: BufferType;
    FIssuerRDN: TElRelativeDistinguishedName;
    FSubjectRDN: TElRelativeDistinguishedName;
    FPrivateKeyBlob: ByteArray;
    FPublicKeyBlob: ByteArray;
    FPreserveKeyMaterial: boolean;
    FStrictMode: boolean;
    FUseUTF8 : boolean;
    FGenerationToken : TElPublicKeyComputationToken;
    function GetCertificateSelfSigned: boolean;
    procedure ReadCertificate;
    procedure ReadCertificateFromASN;

    procedure AddFieldByOID(var Name: TName; const OID: BufferType; Tag : byte;
      const Content: BufferType);

    function GetValidFrom: TDateTime;
    function GetValidTo: TDateTime;
    procedure SetValidFrom(const Value: TDateTime);
    procedure SetValidTo(const Value: TDateTime);
    function GetPublicKeyAlgorithm: integer;
  protected
    FCertStorage: TElBaseCertStorage;
    FBelongsTo: integer;
    FStorageName: string;
    FPrivateKeyExists: boolean;
{$IFDEF SB_HAS_WINCRYPT}
    function LoadPrivateKeyFromWin32: boolean;
    function GetSupportedAlgorithmsFromWin32(var Pairs: TSBArrayOfPairs): boolean;
    function GetContextByHash(var CertStore: HCERTSTORE): PCCERT_CONTEXT;
    function LoadPrivateKeyFromContext(Context: PCCERT_CONTEXT): boolean;
    function LoadPrivateKeyFromKey(Key: HCRYPTKEY): boolean;
    function SignWin32(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize: integer; AuthAttrs: TObject; UnauthAttrs: TObject;
      Detached: boolean = false; Storage: TObject = nil; IncludeCertificates: boolean = true): boolean;
    function DecryptWin32(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize: integer): integer;

{$ENDIF}
    //procedure ParseExtension(const OID: BufferType; Critical: boolean; const Content: BufferType);

    function GenerateRSA(Words: integer; Params: PLongRSAParams): boolean;
    function GenerateDSA(Bits: integer; Params: PLongDSAParams): boolean;
    function GenerateDH(Bits: integer; Params: PLongDHParams): boolean;

    function WriteTBSCertificate: BufferType;
    function WriteSignatureAlgorithm: BufferType;
    function WriteSignatureValue(const TBSCertificate: BufferType): BufferType;

    function WriteVersion: BufferType;
    function WriteSignature: BufferType;
    function WriteValidity: BufferType;
    function WriteSubjectPublicKeyInfo: BufferType;
    function WriteAlgorithmIdentifier(const Algorithm: BufferType; Parameters: TStringList): BufferType;
    function WriteAttributeTypeAndValue(const AttributeType: BufferType; const AttributeValue: BufferType): BufferType;
    function WriteSubjectPublicKey: BufferType;
    function WriteDHParams: BufferType;
    function WriteRSAPublicKey: BufferType;
    function WriteDSAParams: BufferType;
    function WriteDSAPublicKey: BufferType;
    function WriteORAddress(const Value: BufferType): BufferType;
    function WriteBuiltInStandardAttributes(const Value: string): BufferType;
    function WritePolicyInformation(P: TElSinglePolicyInformation): BufferType;
    function WriteDistributionPoint(P: TElDistributionPoint): BufferType;
    function WriteName(Name: TElRelativeDistinguishedName): BufferType;
    function InternalSaveToBuffer(P: Pointer; var Size: Word): boolean;
    procedure ClearData;
    procedure AssignTo(Dest: TPersistent); override;
    procedure RaiseInvalidCertificateException;
    function ParseDSSSignature(P: pointer; Size: integer; var R: string;
      var S: string): boolean;
    function SignSSLHashWin32(Hash: pointer; HashSize: integer; Buffer: pointer;
      var Size: integer): boolean;
    {$ifndef SBB_NO_PKCS11}
    function SignSSLHashPKCS11(Hash: pointer; HashSize: integer; Buffer: pointer;
      var Size: integer): boolean;
    {$endif}
    procedure SetupOriginalKeyMaterial;
    function GetCanEncrypt: boolean;
    function GetCanSign: boolean;
    function GetVersion: byte;
    function GetSerialNumber: BufferType;
    procedure SetSerialNumber(const Value: BufferType);
    function GetIssuer: TStringList;
    function GetSubject: TStringList;
    function GetIssuerUniqueID: BufferType;
    function GetSubjectUniqueID: BufferType;
    function GetPrivateKeyExtractable : boolean;

    
  public
    constructor Create(Owner: TSBComponentBase);  override;
    destructor Destroy; override;

    class function DetectCertFileFormat(FileName: string): TSBCertFileFormat; overload;    class function DetectCertFileFormat(Stream: TStream): TSBCertFileFormat; overload;    class function DetectKeyFileFormat(FileName: string; const Password: string): TSBX509KeyFileFormat; overload;    class function DetectKeyFileFormat(Stream: TStream; const Password: string): TSBX509KeyFileFormat; overload;

    procedure Clone(Dest: TElX509Certificate; CopyPrivateKey: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif});

    function SignSSLHash(Hash: pointer; HashSize: integer; Buffer: pointer;
      var Size: integer): boolean;
    function SignRawHash(HashAlg : integer; Hash: pointer; HashSize: integer; Buffer: pointer;
      var Size: integer): boolean;
    function SignRawHashWin32(HashAlg : integer; Hash: pointer; HashSize: integer; Buffer: pointer;
      var Size: integer): boolean;
    {$ifndef SBB_NO_PKCS11}
    function SignRawHashPKCS11(HashAlg : integer; Hash: pointer; HashSize: integer; Buffer: pointer;
      var Size: integer): boolean;
    {$endif}

    procedure LoadFromBuffer(Buffer: Pointer; Size: Word);
    function LoadFromBufferPEM(Buffer: pointer; Size: Word; const PassPhrase: string): integer;
    procedure LoadKeyFromBuffer(Buffer: Pointer; Size: Word);
    function LoadKeyFromBufferPEM(Buffer: pointer; Size: Word; const PassPhrase: string): integer;
    function LoadFromBufferPFX(Buffer: pointer; Size: integer; const Password: string): integer;
    function LoadFromBufferSPC(Buffer: pointer; Size: integer): integer;
    function LoadKeyFromBufferMS(Buffer: pointer; Size: integer): integer;
    {$ifndef B_6}
    function LoadKeyFromBufferNET(Buffer: pointer; Size: integer; const Password: string): integer;
    {$endif}
    function LoadKeyFromBufferPVK(Buffer: pointer; Size: integer; const Password: string): integer;

    procedure LoadFromStream(Stream: TStream; Count: integer = 0);{$IFNDEF BUILDER_USED}{$IFNDEF CLX_USED} overload;{$ENDIF}{$ENDIF}
{$IFNDEF BUILDER_USED}
{$IFNDEF CLX_USED}
{$ifndef FPC}
    procedure LoadFromStream(Stream: IStream; Count: integer = 0); overload;
{$endif}
{$ENDIF CLX_USED}
{$ENDIF BUILDER_USED}
    procedure LoadKeyFromStream(Stream: TStream; Count: integer = 0);
    function LoadKeyFromStreamPEM(Stream: TStream; const PassPhrase: string;
      Count: integer = 0): integer;
    function LoadFromStreamPEM(Stream: TStream; const PassPhrase: string; Count: integer = 0): integer;
    function LoadFromStreamPFX(Stream: TStream; const Password: string; Count: integer = 0): integer;
    function LoadFromStreamSPC(Stream: TStream; Count: integer = 0): integer;
    function LoadKeyFromStreamMS(Stream: TStream; Count: integer = 0): integer;
    function LoadKeyFromStreamPVK(Stream: TStream; const Password: string; Count: integer = 0): integer;
    {$ifndef B_6}
    function LoadKeyFromStreamNET(Stream: TStream; const Password: string; Count: integer = 0): integer;
    {$endif}

    procedure Generate(Algorithm: integer; DWordsInEncryptKey: integer); overload;
    procedure CAGenerate(Algorithm: integer; DWordsInEncryptKey: integer);
    procedure Generate(Request: TElCertificateRequest; Certificate:
      TElX509Certificate); overload;
    procedure Generate(Parent: TElX509Certificate; Algorithm: integer;
      DWordsInEncryptKey: integer); overload;
    // asynchronous generation functions
    procedure BeginGenerate(Algorithm: integer; DWordsInEncryptKey: integer); overload;
    procedure BeginGenerate(Parent: TElX509Certificate; Algorithm: integer;
      DWordsInEncryptKey: integer); overload;
    procedure EndGenerate();
    procedure CancelGeneration();
    function AsyncOperationFinished : boolean;
    function SaveToBuffer(Buffer: Pointer; var Size: Word): boolean;
    function SaveKeyToBuffer(Buffer: Pointer; var Size: Word): boolean;
    function SaveToBufferPEM(Buffer: Pointer; var Size: Word; const PassPhrase: string): boolean;
    function SaveKeyToBufferPEM(Buffer: Pointer; var Size: Word; const PassPhrase: string): boolean;
    function SaveToBufferPFX(Buffer: pointer; var Size: integer; const Password: string;
      KeyEncryptionAlgorithm: integer; CertEncryptionAlgorithm: integer): integer;
    //function SaveToBufferMS(Buffer: pointer; var Size : integer) : integer;
    function SaveToBufferSPC(Buffer: pointer; var Size: integer): integer;
    function SaveKeyToBufferMS(Buffer: pointer; var Size: integer): integer;
    function SaveKeyToBufferNET(Buffer: pointer; var Size: integer): integer;
    function SaveKeyToBufferPVK(Buffer: pointer; var Size: integer;
      const Password: string; UseStrongEncryption: boolean = true): integer;
    procedure SaveToStream(Stream: TStream);
    procedure SaveKeyToStream(Stream: TStream);
    procedure SaveToStreamPEM(Stream: TStream; const PassPhrase: string);
    procedure SaveKeyToStreamPEM(Stream: TStream; const PassPhrase: string);
    function SaveToStreamPFX(Stream: TStream; const Password: string;
      KeyEncryptionAlgorithm: integer; CertEncryptionAlgorithm: integer): integer;
    function SaveToStreamSPC(Stream: TStream): integer;
    function SaveKeyValueToBuffer(Buffer: pointer; var Size: integer): boolean;
    //function SaveToStreamMS(Stream: TStream): integer;
    function SaveKeyToStreamMS(Stream: TStream): integer;
    function SaveKeyToStreamNET(Stream: TStream; const Password: string): integer;
    function SaveKeyToStreamPVK(Stream: TStream; const Password: string;
      UseStrongEncryption: boolean = true): integer;

    procedure SetIssuer(Issuer: TName);
    procedure SetSubject(Subject: TName);

    procedure SetCACertificate(Buffer: Pointer; Size: longint);
    procedure SetCAPrivateKey(Buffer: Pointer; Size: longint);
    function Validate: boolean;

    function ValidateRSA(PRSAModulus: pointer; RSAModulusSize: integer;
      PRSAPublicKey: pointer; RSAPublicKeySize: integer): boolean; overload;
    function ValidateRSAPSS(PRSAModulus: pointer; RSAModulusSize:
      integer; PRSAPublicKey: pointer; RSAPublicKeySize: integer;
      HashAlg, SaltSize : integer): boolean;
    function ValidateDSS(PDSSP: pointer; DSSPSize: integer;
      PDSSQ: pointer; DSSQSize: integer; PDSSG: pointer; DSSGSize: integer;
      PDSSY: pointer; DSSYSize: integer): boolean; overload;

    function ValidateWithCA(CACertificate: TElX509Certificate): boolean;

    function GetRSAParams(RSAModulus: pointer; var RSAModulusSize: integer;
      RSAPublicKey: pointer; var RSAPublicKeySize: integer): boolean;

    function GetDSSSignatureParams(DSSS: pointer; var DSSSSize: integer;
      DSSR: pointer; var DSSRSize: integer): boolean;

    function GetDSSParams(DSSP: pointer; var DSSPSize: integer; DSSQ: pointer;
      var DSSQSize: integer; DSSG: pointer; var DSSGSize: integer;
      DSSY: pointer; var DSSYSize: integer): boolean;

    function GetDHParams(DHP: pointer; var DHPSize: integer; DHG: pointer;
      var DHGSize: integer; DHY: pointer; var DHYSize: integer): boolean;

    function GetPublicKeyBlob(Buffer: pointer; var Size: integer): boolean;
    { Signing routines }
    function Sign(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize: integer; Detached: boolean = false;
      IncludeCertificates: boolean = true): boolean;
    function Verify(InBuffer: pointer; InSize: integer; Signature: pointer;
      SignatureSize: integer): boolean;
    function GetHashMD5: TMessageDigest128;
    function GetHashSHA1: TMessageDigest160;
    function GetKeyHashSHA1: TMessageDigest160;
    function GetPublicKeySize: integer;

    function WriteExtensionSubjectKeyIdentifier: BufferType;
    function WriteIssuer: BufferType;
    function WriteSerialNumber: BufferType;
    function WriteSubject: BufferType;

    function IsKeyValid: boolean;

    property CertificateBinary: PByteArray read FPData;
    property CertificateSize: integer read FAllSize;
    property SignatureAlgorithm: integer read FSignatureAlgorithmInt;
    property Signature: BufferType read FSignatureValue;
    property Version: byte read GetVersion;
    property SerialNumber: BufferType read GetSerialNumber
      write SetSerialNumber;
    property PSSParams : TElRSAPSSSignParameters read FPSSParams;
    property OAEPParams : TElRSAOAEPEncryptionParameters read FOAEPParams;

    property ValidFrom: TDateTime read GetValidFrom write
    SetValidFrom;
    property ValidTo: TDateTime read GetValidTo write
    SetValidTo;

    property BelongsTo: integer read FBelongsTo write FBelongsTo;
    property IssuerUniqueID: BufferType read GetIssuerUniqueID;
    property SubjectUniqueID: BufferType read GetSubjectUniqueID;
    property PublicKeyAlgorithm: integer read GetPublicKeyAlgorithm;
    property PrivateKeyExists: boolean read FPrivateKeyExists write FPrivateKeyExists;
    property PrivateKeyExtractable: boolean read GetPrivateKeyExtractable;
    property CAAvailable: boolean read FCAAvailable write FCAAvailable;
    property SelfSigned: boolean read GetCertificateSelfSigned;
    property IssuerName: TName read FIssuerName;
    property SubjectName: TName read FSubjectName;
    property IssuerRDN: TElRelativeDistinguishedName read FIssuerRDN;
    property SubjectRDN: TElRelativeDistinguishedName read FSubjectRDN;
    property Extensions: TElCertificateExtensions read FCertificateExtensions;
    property CertStorage: TElBaseCertStorage read FCertStorage write FCertStorage;
    property StorageName: string read FStorageName write FStorageName;
    property PreserveKeyMaterial: boolean read FPreserveKeyMaterial
    write FPreserveKeyMaterial default false;
    property CanEncrypt: boolean read GetCanEncrypt;
    property CanSign: boolean read GetCanSign;
    property StrictMode: boolean read FStrictMode write FStrictMode default false;
    property UseUTF8 : boolean read FUseUTF8 write FUseUTF8 default false;
  end;

  TElX509CertificateClass = class of TElX509Certificate;

type

  EX509Error =  class(ESecureBlackboxError);

type
  TPVKHeader =  packed record
    magic: DWORD;
    reserved: DWORD;
    keytype: DWORD;
    encrypted: DWORD;
    saltlen: DWORD;
    keylen: DWORD;
  end;

function PVK_DeriveKey(const Password: string; const Salt: string; AWeakMethod: boolean): string;

procedure RaiseX509Error(ErrorCode: integer); 

(*
function DetectCertFileFormat(FileName: string): TSBCertFileFormat; {$ifndef CHROME}overload;{$endif}
function DetectCertFileFormat(Stream: {$IFNDEF DELPHI_NET}TStream{$ELSE}System.IO.Stream{$ENDIF}): TSBCertFileFormat; {$ifndef CHROME}overload;{$endif}
function DetectKeyFileFormat(FileName: string; const Password: string): TSBX509KeyFileFormat; {$ifndef CHROME}overload;{$endif}
function DetectKeyFileFormat(Stream: {$IFNDEF DELPHI_NET}TStream{$ELSE}System.IO.Stream{$ENDIF}; const Password: string): TSBX509KeyFileFormat; {$ifndef CHROME}overload;{$endif}
*)

procedure Register;

implementation

uses
  SBPKCS12,
  SBMessages,
  SBCustomCertStorage,
  SBPKCS7,
  SBPKCS7Utils,
  SBMSKeyBlob,
{$ifndef B_6}
  SBPKCS8,
{$endif}
  SBConstants
  {$ifndef SBB_NO_RC4}
  ,
  SBRC4
  {$endif}
{$IFDEF SB_HAS_WINCRYPT}
  ,
  SBWinCertStorage
{$ENDIF}
{$ifndef SBB_NO_PKCS11}
  ,
  SBPKCS11Common,
  SBPKCS11CertStorage
{$ENDIF}
  ;

resourcestring

  sInvalidPVKFormat = 'Invalid file format (possibly not a PVK?)';
  sIncorrectPassphrase = 'Incorrect password';
  sNotEnoughBufferSpace = 'Not enough buffer space';
  SInvalidtbsCert = 'Invalid certificate data';
  SInvalidSignAlgorithmSize = 'Invalid signature algorithm size';
  SPrivateKeyNotFound = 'Private key not found';
  SInvalidPointer = 'Invalid pointer';
  SInvalidRequestSignature = 'Invalid request signature';
  SUnknownAlgorithm = 'Unknown algorithm';
  SInternalError = 'Internal Error. Please contact EldoS support for details.';
  SNoCertificateFound = 'No certificate found';
  SInvalidCertificate = 'No X.509 certificate data found';
  SInvalidPrivateKey = 'No private key found';
  SKeyLengthTooSmall = 'Key length is too small';
  SInvalidAlgorithmIdentifier = 'Invalid algorithm identifier';
  SObjectTooLong = 'Object Identifier is too long';
  SCertAlgorithmMismatch = 'Certificate algorithm mismatch';
  SNonCriticalExtensionMarkedAsCritical = 'Non-critical extension is marked as critical';
  SInvalidPublicKeyAlgorithm = 'Invalid public key algorithm';
  SCertIsNotBeingGenerated = 'Certificate is not being generated (use BeginGenerate() method)';

procedure RaiseX509Error(ErrorCode: integer);
begin
  case ErrorCode of
    SB_X509_ERROR_INVALID_PVK_FILE: raise EX509Error.Create(sInvalidPVKFormat, ErrorCode{$ifndef HAS_DEF_PARAMS}{$ifndef FPC}, 0{$endif}{$endif});
    SB_X509_ERROR_INVALID_PASSWORD: raise EX509Error.Create(sIncorrectPassphrase, ErrorCode{$ifndef HAS_DEF_PARAMS}{$ifndef FPC}, 0{$endif}{$endif});
    SB_X509_ERROR_NO_PRIVATE_KEY: raise EX509Error.Create(SPrivateKeyNotFound, ErrorCode{$ifndef HAS_DEF_PARAMS}{$ifndef FPC}, 0{$endif}{$endif});
    SB_X509_ERROR_UNSUPPORTED_ALGORITHM: raise EX509Error.Create(SUnknownAlgorithm, ErrorCode{$ifndef HAS_DEF_PARAMS}{$ifndef FPC}, 0{$endif}{$endif});
    SB_X509_ERROR_INVALID_PRIVATE_KEY: raise EX509Error.Create(SInvalidPrivateKey, ErrorCode{$ifndef HAS_DEF_PARAMS}{$ifndef FPC}, 0{$endif}{$endif});
    SB_X509_ERROR_INTERNAL_ERROR: raise EX509Error.Create(sInternalError, ErrorCode{$ifndef HAS_DEF_PARAMS}{$ifndef FPC}, 0{$endif}{$endif});
    SB_X509_ERROR_BUFFER_TOO_SMALL: raise EX509Error.Create(sNotEnoughBufferSpace, ErrorCode{$ifndef HAS_DEF_PARAMS}{$ifndef FPC}, 0{$endif}{$endif});
  else
    exit;
  end;
end;

type
  THackElCertificateExtensions = class(TElCertificateExtensions);


procedure Register;
begin
  RegisterComponents('PKIBlackbox', [TElX509Certificate]);
end;

{$IFDEF SB_HAS_WINCRYPT}

function SetupCryptAttribute(const AttrOID: string; AttrValues: TStringList;
  CryptAttribute: PCRYPT_ATTRIBUTE): boolean;
var
  OIDStr: BufferType;
  I: integer;
  Ptr: PCRYPT_ATTR_BLOB;
  S: PChar;
begin
  if not Assigned(CryptAttribute) then
  begin
    Result := false;
    Exit;
  end;
  OIDStr := OIDToStr(AttrOID);
  GetMem(S, Length(OIDStr) + 1);
  Move(OIDStr[1], S^, Length(OIDStr));
  PByteArray(S)[Length(OIDStr)] := 0;
  CryptAttribute.pszObjId := S;
  CryptAttribute.cValue := AttrValues.Count;
  GetMem(CryptAttribute.rgValue, SizeOf(CRYPT_ATTR_BLOB) * AttrValues.Count);
  Ptr := CryptAttribute.rgValue;
  for I := 0 to AttrValues.Count - 1 do
  begin
    Ptr^.cbData := Length(AttrValues.Strings[I]);
    GetMem(Ptr^.pbData, Ptr^.cbData);
    Move((AttrValues.Strings[I])[1], Ptr^.pbData^, Ptr^.cbData);
    Inc(Ptr);
  end;
  Result := true;
end;



procedure ReleaseCryptAttribute(CryptAttribute: PCRYPT_ATTRIBUTE);
var
  I: integer;
  Ptr: PCRYPT_ATTR_BLOB;
begin
  if not Assigned(CryptAttribute) then
    Exit;
  FreeMem(CryptAttribute.pszObjId);
  Ptr := CryptAttribute.rgValue;
  for I := 0 to CryptAttribute.cValue - 1 do
  begin
    FreeMem(Ptr.pbData);
    Inc(Ptr);
  end;
  FreeMem(CryptAttribute.rgValue);
end;

{$ENDIF}


function PVK_DeriveKey(const Password: string; const Salt: string; AWeakMethod: boolean): string;
var
  M160: TMessageDigest160;
begin
  M160 := HashSHA1(TBufferTypeConst(Salt) + Password);
  SetLength(Result, 16);
  if AWeakMethod then
  begin
    Move(M160, Result[1], 5);
    FillChar(Result[6], 11, 0);
  end
  else
    Move(M160, Result[1], Length(Result));
end;

{$ifndef SBB_NO_RC4}
function PVK_CheckKey(const Key: string; InBuffer: pointer; OutBuffer: pointer;
  var Context: TRC4Context): boolean;
const
  Magic = $32415352;
begin
  SBRC4.Initialize(Context, TRC4Key(BytesOfString(Key)));
  SBRC4.Decrypt(Context, InBuffer, OutBuffer, 4);
  Result := (Magic = PLongword(OutBuffer)^);
end;
{$endif}

constructor TElX509Certificate.Create(Owner: TSBComponentBase);
begin
  inherited Create(Owner);
  FTbsCertificate := TElTBSCertificate.Create;
  // load constants (types of extensions)
  GetMem(FPData, 32768);
  SetLength(FPrivateKey, 0);
  FPSSParams := TElRSAPSSSignParameters.Create;
  FOAEPParams := TElRSAOAEPEncryptionParameters.Create;

  FCertificateExtensions := TElCertificateExtensions.Create;
  FIssuerRDN := TElRelativeDistinguishedName.Create;
  FSubjectRDN := TElRelativeDistinguishedName.Create;
  FGeneratingFromRequest := false;
  SetLength(FPrivateKeyBlob, 0);
  FBelongsTo := 0;
  FPrivateKeyExists := false;
  LCreate(FLongRSAEncryptParams.PrivateExponent);
  LCreate(FLongRSAEncryptParams.PublicKey);
  LCreate(FLongRSAEncryptParams.Modulus);
  LCreate(FLongDHEncryptParams.P);
  LCreate(FLongDHEncryptParams.G);
  LCreate(FLongDHEncryptParams.X);
  LCreate(FLongDHEncryptParams.Ys);
  LCreate(FLongRSACAEncryptParams.PrivateExponent);
  LCreate(FLongRSACAEncryptParams.PublicKey);
  LCreate(FLongRSACAEncryptParams.Modulus);
  LCreate(FLongDSAEncryptParams.P);
  LCreate(FLongDSAEncryptParams.Q);
  LCreate(FLongDSAEncryptParams.G);
  LCreate(FLongDSAEncryptParams.X);
  LCreate(FLongDSAEncryptParams.Y);
  LCreate(FLongDSACAEncryptParams.P);
  LCreate(FLongDSACAEncryptParams.Q);
  LCreate(FLongDSACAEncryptParams.G);
  LCreate(FLongDSACAEncryptParams.X);
  LCreate(FLongDSACAEncryptParams.Y);
  FPreserveKeyMaterial := false;
  FStrictMode := false;
  FSignatureAlgorithm := nil;
  FParent := nil;
  FUseUTF8 := false;
  FGenerationToken := nil;
end;


destructor TElX509Certificate.Destroy;
begin
  FreeAndNil(FTbsCertificate);
  FreeMem(FPData);
  FCertificateExtensions.Free;
  FIssuerRDN.Free;
  FSubjectRDN.Free;

  if Assigned(FSignatureAlgorithm) then
    FreeAndNil(FSignatureAlgorithm);

  FPSSParams.Free;
  FOAEPParams.Free;

  LDestroy(FLongRSAEncryptParams.PrivateExponent);
  LDestroy(FLongRSAEncryptParams.PublicKey);
  LDestroy(FLongRSAEncryptParams.Modulus);
  LDestroy(FLongDHEncryptParams.P);
  LDestroy(FLongDHEncryptParams.G);
  LDestroy(FLongDHEncryptParams.X);
  LDestroy(FLongDHEncryptParams.Ys);
  LDestroy(FLongRSACAEncryptParams.PrivateExponent);
  LDestroy(FLongRSACAEncryptParams.PublicKey);
  LDestroy(FLongRSACAEncryptParams.Modulus);
  LDestroy(FLongDSAEncryptParams.P);
  LDestroy(FLongDSAEncryptParams.Q);
  LDestroy(FLongDSAEncryptParams.G);
  LDestroy(FLongDSAEncryptParams.X);
  LDestroy(FLongDSAEncryptParams.Y);
  LDestroy(FLongDSACAEncryptParams.P);
  LDestroy(FLongDSACAEncryptParams.Q);
  LDestroy(FLongDSACAEncryptParams.G);
  LDestroy(FLongDSACAEncryptParams.X);
  LDestroy(FLongDSACAEncryptParams.Y);
  if Assigned(FGenerationToken) then
    FreeAndNil(FGenerationToken);
  inherited;
end;


procedure TElX509Certificate.Clone(Dest: TElX509Certificate;
    CopyPrivateKey: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif});
var
  Buf: ByteArray;
  BufSize: word;
  NewBuf: PByteArray;
begin
  if Assigned(Dest) then
  begin
    GetMem(NewBuf, FAllSize);
    try
      Move(FPData^, NewBuf^, FAllSize);
      Dest.LoadFromBuffer(NewBuf, FAllSize);
    finally
      FreeMem(NewBuf);
    end;

    Dest.StorageName := StorageName;

    if CopyPrivateKey then
    begin
      BufSize := 0;
      if (BelongsTo <> BT_WINDOWS) and (BelongsTo <> BT_PKCS11) and (FPrivateKeyExists) then
      begin
        SaveKeyToBuffer(nil, BufSize);
        SetLength(Buf, BufSize);
        SaveKeyToBuffer(@Buf[0], BufSize);
        Dest.LoadKeyFromBuffer(@Buf[0], BufSize);
      end;
      Dest.BelongsTo := BelongsTo;
      Dest.FPrivateKeyExists := FPrivateKeyExists;
    end
    else
    begin
      Dest.FPrivateKeyExists := false;
      Dest.BelongsTo := 0;
    end;
  end;
end;

procedure TElX509Certificate.ReadCertificate;
begin
  CheckLicenseKey();
  FCertificateOffset := 0;
  FCertificateSize := 0;
  BelongsTo := 0;
  FPrivateKeyExists := false;
  ReadCertificateFromASN;
end;

procedure TElX509Certificate.ReadCertificateFromASN;

  procedure ReadRDNSequence(Seq: TElASN1ConstrainedTag; Name: TElRelativeDistinguishedName);
  var
    I, J: integer;
    TagSet, TagSeq: TElASN1ConstrainedTag;
    ObjID, Val: BufferType;
    Size: integer;
  begin
    Name.Clear;
    for I := 0 to Seq.Count - 1 do
    begin
      if not Seq.GetField(I).CheckType(SB_ASN1_SET, true) then
        raise EElCertificateException.Create(SInvalidtbsCert);
      TagSet := TElASN1ConstrainedTag(Seq.GetField(I));
      for J := 0 to TagSet.Count - 1 do
      begin
        if not TagSet.GetField(J).CheckType(SB_ASN1_SEQUENCE, true) then
          raise EElCertificateException.Create(SInvalidtbsCert);
        TagSeq := TElASN1ConstrainedTag(TagSet.GetField(J));
        if (TagSeq.Count <> 2) or (not TagSeq.GetField(0).CheckType(SB_ASN1_OBJECT, false)) then
          raise EElCertificateException.Create(SInvalidtbsCert);
        ObjID := TElASN1SimpleTag(TagSeq.GetField(0)).Content;
        if not TagSeq.GetField(1).IsConstrained then
        begin
          Name.Add(ObjID, TElASN1SimpleTag(TagSeq.GetField(1)).Content,
            TElASN1SimpleTag(TagSeq.GetField(1)).TagId);
        end
        else
        begin
          Size := 0;
          TagSeq.GetField(1).SaveToBuffer(nil, Size);
          SetLength(Val, Size);
          TagSeq.GetField(1).SaveToBuffer(@Val[1], Size);
          SetLength(Val, Size);
          Name.Add(ObjID, Val, 0);
        end;
      end;
    end;
  end;

  function ReadTime(Tag: TElASN1CustomTag): TDateTime;
  begin
    if Tag.CheckType(SB_ASN1_UTCTIME, false) then
      Result := UTCTimeToDateTime((TElASN1SimpleTag(Tag).Content))
    else
      if Tag.CheckType(SB_ASN1_GENERALIZEDTIME, false) then
      Result := GeneralizedTimeToDateTime((TElASN1SimpleTag(Tag).Content))
    else
      raise EElCertificateException.Create(SInvalidTbsCert);
  end;

  procedure ReadAlgorithmIdentifier(Tag: TElASN1ConstrainedTag; var EncParams:
    TElEncryptionParameters);
  var
    OID: BufferType;
    Alg, Size: integer;
    Buf : ByteArray;
    HAlg, SaltSize, MGF, MGFHAlg, TrField : integer;
    LblStr : string;
  begin
    EncParams := nil;
    if (Tag.Count < 1) or (not Tag.GetField(0).CheckType(SB_ASN1_OBJECT, false)) then
      raise EElCertificateException.Create(SInvalidAlgorithmIdentifier);
    // reading algorithm identifier
    OID := TElASN1SimpleTag(Tag.GetField(0)).Content;
    // reading algorithm parameters
    Alg := GetPKAlgorithmByOID(OID);
    if Alg = SB_ALGORITHM_UNKNOWN then
      Alg := GetSigAlgorithmByOID(OID);

    case Alg of
      SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION,
      SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION,
      SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION,
      SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION,
      SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION,
      SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION,
      SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION,
      SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION,
      SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160:   
        begin
          // expecting NULL as parameters
          if (Tag.Count < 2) or (not Tag.GetField(1).CheckType(SB_ASN1_NULL, false)) then
          begin
            if Tag.Count <> 1 then
              raise EElCertificateException.Create(SInvalidAlgorithmIdentifier);
          end;
          EncParams := TElRSAEncryptionParameters.Create;
        end;
      SB_CERT_ALGORITHM_ID_RSAPSS:
        begin
          Size := 0;

          if Tag.Count > 2 then
            raise EElCertificateException.Create(SInvalidAlgorithmIdentifier);

          EncParams := TElRSAPSSSignParameters.Create;
                      
          if Tag.Count = 2 then
          begin
            Tag.GetField(1).SaveToBuffer(nil, Size);
            SetLength(Buf, Size);
            Tag.GetField(1).SaveToBuffer(@Buf[0], Size);

            if not ReadPSSParams(@Buf[0],
              Size, HAlg, SaltSize, MGF, MGFHAlg, TrField) then
            begin
              EncParams.Free;
              raise EElCertificateException.Create(SInvalidAlgorithmIdentifier);
            end;
          end
          else
          begin
            SaltSize := 20;
            HAlg := SB_ALGORITHM_DGST_SHA1;
            TrField := 1;
            MGF := SB_CERT_MGF1;
            MGFHAlg := SB_ALGORITHM_DGST_SHA1;
          end;  

          TElRSAPSSSignParameters(EncParams).FSaltSize := SaltSize;
          TElRSAPSSSignParameters(EncParams).FHashAlgorithm := HAlg;
          TElRSAPSSSignParameters(EncParams).FTrailerField := TrField;
          TElRSAPSSSignParameters(EncParams).FMGF := MGF;
          TElRSAPSSSignParameters(EncParams).FMGFHashAlgorithm := MGFHAlg;
        end;
      SB_CERT_ALGORITHM_ID_RSAOAEP:
        begin
          Size := 0;

          if Tag.Count = 2 then
          begin
            Tag.GetField(1).SaveToBuffer(nil, Size);
            SetLength(Buf, Size);
            Tag.GetField(1).SaveToBuffer(@Buf[0], Size);

            EncParams := TElRSAOAEPEncryptionParameters.Create;
            if not ReadOAEPParams(@Buf[0],
              Size, HAlg, MGFHAlg, LblStr) then
            begin
              EncParams.Free;
              raise EElCertificateException.Create(SInvalidAlgorithmIdentifier);
            end;
          end
          else
          begin
            HAlg := SB_ALGORITHM_DGST_SHA1;
            LblStr := '';
            MGFHAlg := SB_ALGORITHM_DGST_SHA1;
          end;  

          TElRSAOAEPEncryptionParameters(EncParams).FHashAlgorithm := HAlg;
          TElRSAOAEPEncryptionParameters(EncParams).FStrLabel := LblStr;
          TElRSAOAEPEncryptionParameters(EncParams).FMGFHashAlgorithm := MGFHAlg;
          TElRSAOAEPEncryptionParameters(EncParams).FMGF := SB_CERT_MGF1;
        end;
      SB_CERT_ALGORITHM_ID_DSA:
        begin
          if (Tag.Count = 2) then
          begin
          // expecting the following parameters structure:
          // Dss-Parms  ::=  SEQUENCE  (
          //    p             INTEGER,
          //    q             INTEGER,
          //    g             INTEGER  )
            if (not Tag.GetField(1).CheckType(SB_ASN1_SEQUENCE, true)) or
              (TElASN1ConstrainedTag(Tag.GetField(1)).Count <> 3) or
              (not TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0).CheckType(SB_ASN1_INTEGER, false)) or
              (not TElASN1ConstrainedTag(Tag.GetField(1)).GetField(1).CheckType(SB_ASN1_INTEGER, false)) or
              (not TElASN1ConstrainedTag(Tag.GetField(1)).GetField(2).CheckType(SB_ASN1_INTEGER, false)) then
              raise EElCertificateException.Create(SInvalidAlgorithmIdentifier);
            EncParams := TElDSAEncryptionParameters.Create;
            TElDSAEncryptionParameters(EncParams).P := BytesOfString(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0)).Content);
            TElDSAEncryptionParameters(EncParams).Q := BytesOfString(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(1)).Content);
            TElDSAEncryptionParameters(EncParams).G := BytesOfString(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(2)).Content);
          end
          else
            if (Tag.Count = 1) then
          begin
          // TODO:
          // using DSS params of certificate issuer
          end
          else
            raise EElCertificateException.Create(SInvalidAlgorithmIdentifier);
        end;
      SB_CERT_ALGORITHM_ID_DSA_SHA1:
        begin
        // parameters are absent
          if (Tag.Count <> 1) then
          begin
            if not ((Tag.Count = 2) and (Tag.GetField(0).CheckType(SB_ASN1_OBJECT, false)) and
              (Tag.GetField(1).CheckType(SB_ASN1_NULL, false))) then
              raise EElCertificateException.Create(SInvalidAlgorithmIdentifier)
            else
              EncParams := TElDSAEncryptionParameters.Create; // just a stub
          end;
        end;
      SB_CERT_ALGORITHM_DH_PUBLIC:
        begin
        // DomainParameters ::= SEQUENCE {
        //      p       INTEGER, -- odd prime, p=jq +1
        //      g       INTEGER, -- generator, g
        //      q       INTEGER, -- factor of p-1
        //      j       INTEGER OPTIONAL, -- subgroup factor
        //      validationParms  ValidationParms OPTIONAL }
          if (Tag.Count <> 2) or (not Tag.GetField(1).CheckType(SB_ASN1_SEQUENCE, true)) or
            (TElASN1ConstrainedTag(Tag.GetField(1)).Count < 3) or
            (not TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0).CheckType(SB_ASN1_INTEGER, false)) or
            (not TElASN1ConstrainedTag(Tag.GetField(1)).GetField(1).CheckType(SB_ASN1_INTEGER, false)) or
            (not TElASN1ConstrainedTag(Tag.GetField(1)).GetField(2).CheckType(SB_ASN1_INTEGER, false)) then
            raise EElCertificateException.Create(SInvalidAlgorithmIdentifier);
          EncParams := TElDHEncryptionParameters.Create;
          TElDHEncryptionParameters(EncParams).P := BytesOfString(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0)).Content);
          TElDHEncryptionParameters(EncParams).G := BytesOfString(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(1)).Content);
          TElDHEncryptionParameters(EncParams).Q := BytesOfString(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(2)).Content);
        end;
    end;
    if EncParams <> nil then
      EncParams.FAlgorithm := Alg
    else
    begin
      // II 20060516
      //raise EElCertificateException.Create(SUnknownAlgorithm + ' (' + OIDToStr(OID) + ')');
    end;
  end;

  procedure ReadSPKI(Tag: TElASN1ConstrainedTag);
  var
    PublicKey: BufferType;
    PKTag: TElASN1ConstrainedTag;
  begin
    if (Tag.Count = 2) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) and
      (Tag.GetField(1).CheckType(SB_ASN1_BITSTRING, false)) then
    begin
      try
        ReadAlgorithmIdentifier(TElASN1ConstrainedTag(Tag.GetField(0)),
          FtbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters);
        // II 20060516
        //if FtbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters = nil then
        //  raise EElCertificateException.Create(SUnknownAlgorithm);
        if FTbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters is TElRSAPSSSignParameters then
        begin
          FTbsCertificate.FSubjectPublicKeyInfo.FPublicKeyAlgorithm := SB_CERT_ALGORITHM_ID_RSAPSS;
          FPSSParams.Assign(FTbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters);
        end
        else
        if FTbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters is TElRSAOAEPEncryptionParameters then
        begin
          FTbsCertificate.FSubjectPublicKeyInfo.FPublicKeyAlgorithm := SB_CERT_ALGORITHM_ID_RSAOAEP;
          FOAEPParams.Assign(FTbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters);
        end
        else
        if FtbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters is TElRSAEncryptionParameters then
          FTbsCertificate.FSubjectPublicKeyInfo.FPublicKeyAlgorithm := SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION
        else
          if FtbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters is TElDSAEncryptionParameters then
          FTbsCertificate.FSubjectPublicKeyInfo.FPublicKeyAlgorithm := SB_CERT_ALGORITHM_ID_DSA
        else
          if FtbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters is TElDHEncryptionParameters then
          FTbsCertificate.FSubjectPublicKeyInfo.FPublicKeyAlgorithm := SB_CERT_ALGORITHM_DH_PUBLIC
        else
          FTbsCertificate.FSubjectPublicKeyInfo.FPublicKeyAlgorithm := SB_CERT_ALGORITHM_UNKNOWN;

        // subject public key
        PublicKey := TElASN1SimpleTag(Tag.GetField(1)).Content;
        if (Length(PublicKey) < 1) or (PublicKey[1] <> #0) then
          raise EElCertificateException.Create(SInvalidtbsCert);
        FTbsCertificate.FSubjectPublicKeyInfo.FRawData := BytesOfString(PublicKey);
        PublicKey := Copy(PublicKey, 2, Length(PublicKey) - 1);
        PKTag := TElASN1ConstrainedTag.Create;
        try
          if not PKTag.LoadFromBuffer(@PublicKey[1], Length(PublicKey)) then
            raise EElCertificateException.Create(SInvalidtbsCert);

          // reading public key
          SetLength(FPublicKeyBlob, Length(PublicKey));
          Move(PublicKey[1], FPublicKeyBlob[0], Length(FPublicKeyBlob));

          if Assigned(FtbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters) then
            FtbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters.LoadPublicKey(PKTag);
        finally
          FreeAndNil(PKTag);
        end;
      except
        raise EElCertificateException.Create(SInvalidTbsCert);
      end;
    end
    else
      raise EElCertificateException.Create(SInvalidTbsCert);
  end;

  procedure ReadTBSCertificate(Tag: TElASN1ConstrainedTag);
  var
    CurrTagIndex, CurrExtTagIndex: integer;
    OID, Cnt: BufferType;
    Critical: boolean;
    I: integer;
    ExtTag: TElASN1ConstrainedTag;
    Reader : TElExtensionReader;
  begin
    FTbsCertificate.Clear;
    CurrTagIndex := 0;
    FCertificateOffset := Tag.TagOffset;
    FCertificateSize := Tag.TagSize;

    { Version }
    if (Tag.GetField(CurrTagIndex).CheckType(SB_ASN1_A0, true)) and
      (TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)).Count = 1) and
      (TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)).GetField(0).CheckType(SB_ASN1_INTEGER, false)) then
    begin
      Cnt := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)).GetField(0)).Content;
      if Length(Cnt) > 0 then
        FTbsCertificate.FVersion := Byte(Cnt[BufferTypeStartOffset])
      else
        FTbsCertificate.FVersion := 0;
      Inc(CurrTagIndex);
    end
    else
      FTbsCertificate.FVersion := 0;

    { Serial number }
    if (CurrTagIndex >= Tag.Count) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    if (not Tag.GetField(CurrTagIndex).CheckType(SB_ASN1_INTEGER, false)) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    Cnt := TElASN1SimpleTag(Tag.GetField(CurrTagIndex)).Content;
    FTbsCertificate.FSerialNumber := Cnt;
    while (FTBSCertificate.FSerialNumber[1] = #0) and
      (Length(FTBSCertificate.FSerialNumber) > 1) do
      FTBSCertificate.FSerialNumber := Copy(FTBSCertificate.FSerialNumber, 2,
        Length(FTBSCertificate.FSerialNumber));
    if (Length(FTBSCertificate.FSerialNumber) >= 1) and
      (Ord(FTBSCertificate.FSerialNumber[1]) >= $80) then
      FTBSCertificate.FSerialNumber := #0 + FTBSCertificate.FSerialNumber;
    Inc(CurrTagIndex);

    { Signature }
    if (CurrTagIndex >= Tag.Count) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    if not Tag.GetField(CurrTagIndex).CheckType(SB_ASN1_SEQUENCE, true) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    ReadAlgorithmIdentifier(TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)),
      FTbsCertificate.FSignature);
    Inc(CurrTagIndex);

    { Issuer }
    if (CurrTagIndex >= Tag.Count) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    if (not Tag.GetField(CurrTagIndex).CheckType(SB_ASN1_SEQUENCE, true)) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    ReadRDNSequence(TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)),
      FIssuerRDN);
    for I := 0 to FIssuerRDN.Count - 1 do
      AddFieldByOID(FIssuerName, FIssuerRDN.OIDs[I], FIssuerRDN.Tags[I], FIssuerRDN.Values[I]);
    Inc(CurrTagIndex);
    
    { Validity }
    if (CurrTagIndex >= Tag.Count) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    if (not Tag.GetField(CurrTagIndex).CheckType(SB_ASN1_SEQUENCE, true)) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    if (TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)).Count <> 2) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    FtbsCertificate.Validity.NotBefore := ReadTime(TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)).GetField(0));
    FtbsCertificate.Validity.NotAfter := ReadTime(TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)).GetField(1));
    Inc(CurrTagIndex);

    { Subject }
    if (CurrTagIndex >= Tag.Count) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    if (not Tag.GetField(CurrTagIndex).CheckType(SB_ASN1_SEQUENCE, true)) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    ReadRDNSequence(TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)),
      FSubjectRDN);
    for I := 0 to FSubjectRDN.Count - 1 do
      AddFieldByOID(FSubjectName, FSubjectRDN.OIDs[I], FSubjectRDN.Tags[I], FSubjectRDN.Values[I]);
    Inc(CurrTagIndex);

    { SPKI }
    if (CurrTagIndex >= Tag.Count) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    if not Tag.GetField(CurrTagIndex).CheckType(SB_ASN1_SEQUENCE, true) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    ReadSPKI(TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)));
    Inc(CurrTagIndex);

    if FTbsCertificate.FVersion < 1 then
    begin
      if CurrTagIndex < Tag.Count then
        raise EElCertificateException.Create(SInvalidtbsCert);
    end;

    { Issuer Unique ID }
    if (CurrTagIndex >= Tag.Count) then
      Exit;
    if Tag.GetField(CurrTagIndex).CheckType($81, false) then
    begin
      FTBSCertificate.FIssuerUniqueID := TElASN1SimpleTag(Tag.GetField(CurrTagIndex)).Content;
      if (Length(FTBSCertificate.FIssuerUniqueID) = 0) or
      (FTBSCertificate.FIssuerUniqueID[1] <> #0) then
        raise EElCertificateException.Create(SInvalidtbsCert);
      FTBSCertificate.FIssuerUniqueID := Copy(FTBSCertificate.FIssuerUniqueID, 2,
        Length(FTBSCertificate.FIssuerUniqueID) - 1);
      Inc(CurrTagIndex);
    end;

    { Subject Unique ID }
    if (CurrTagIndex >= Tag.Count) then
      Exit;
    if Tag.GetField(CurrTagIndex).CheckType($82, false) then
    begin
      FTBSCertificate.FSubjectUniqueID := TElASN1SimpleTag(Tag.GetField(CurrTagIndex)).Content;
      if (Length(FTBSCertificate.FSubjectUniqueID) = 0) or
      (FTBSCertificate.FSubjectUniqueID[1] <> #0) then
        raise EElCertificateException.Create(SInvalidtbsCert);
      FTBSCertificate.FSubjectUniqueID := Copy(FTBSCertificate.FSubjectUniqueID, 2,
        Length(FTBSCertificate.FSubjectUniqueID) - 1);
      Inc(CurrTagIndex);
    end;

    { Extensions }
    if FTbsCertificate.FVersion < 2 then
    begin
      if CurrTagIndex < Tag.Count then
        raise EElCertificateException.Create(SInvalidtbsCert);
    end;
    if (CurrTagIndex >= Tag.Count) then
      Exit;
    if Tag.GetField(CurrTagIndex).CheckType(SB_ASN1_A3, true) then
    begin
      if (TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)).Count = 1) and
        (TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)).GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        Reader := TElExtensionReader.Create(FCertificateExtensions, FStrictMode);
        try
          for I := 0 to TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)).GetField(0)).Count - 1 do
          begin
            if not TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)).GetField(0)).GetField(I).CheckType(SB_ASN1_SEQUENCE, true) then
              raise EElCertificateException.Create(SInvalidtbsCert);
            CurrExtTagIndex := 0;
            ExtTag := TElASN1ConstrainedTag(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag.GetField(CurrTagIndex)).GetField(0)).GetField(I));
            if (CurrExtTagIndex < ExtTag.Count) and (ExtTag.GetField(CurrExtTagIndex).CheckType(SB_ASN1_OBJECT, false)) then
              OID := TElASN1SimpleTag(ExtTag.GetField(CurrExtTagIndex)).Content
            else
              raise EElCertificateException.Create(SInvalidtbsCert);
            Inc(CurrExtTagIndex);
            if (CurrExtTagIndex < ExtTag.Count) and (ExtTag.GetField(CurrExtTagIndex).CheckType(SB_ASN1_BOOLEAN, false)) then
            begin
              Critical := TElASN1SimpleTag(ExtTag.GetField(CurrExtTagIndex)).Content = #$FF;
              Inc(CurrExtTagIndex);
            end
            else
              Critical := false;

            if (CurrExtTagIndex < ExtTag.Count) and (ExtTag.GetField(CurrExtTagIndex).CheckType(SB_ASN1_OCTETSTRING, false)) then
              Cnt := TElASN1SimpleTag(ExtTag.GetField(CurrExtTagIndex)).Content
            else
              raise EElCertificateException.Create(SInvalidtbsCert);
            Reader.ParseExtension(OID, Critical, Cnt);
          end;
        finally
          FreeAndNil(Reader);
        end;
      end
      else
        raise EElCertificateException.Create(SInvalidtbsCert);
    end
    else
      raise EElCertificateException.Create(SInvalidtbsCert);
    Inc(CurrTagIndex);
    if CurrTagIndex < Tag.Count then
      raise EElCertificateException.Create(SInvalidtbsCert);
  end;

  procedure ReadSignatureAlgorithmIdentifier(Tag: TElASN1ConstrainedTag);
  begin
    if Assigned(FSignatureAlgorithm) then
      FreeAndNil(FSignatureAlgorithm);
    ReadAlgorithmIdentifier(Tag, FSignatureAlgorithm);
    if Assigned(FSignatureAlgorithm) then
      FSignatureAlgorithmInt := FSignatureAlgorithm.Algorithm
    else
      FSignatureAlgorithmInt := SB_CERT_ALGORITHM_UNKNOWN;
  end;

  (*
  function GetRDNProperty(const OID : BufferType; RDN : TElRelativeDistinguishedName;
    List: {$ifndef DELPHI_NET}TStringList{$else}ArrayList{$endif}): string;
  begin
    List.Clear;
    RDN.GetValuesByOID(OID, List);
    if List.Count > 0 then
      Result := {$ifdef DELPHI_NET}StringOfBytes{$endif}(BufferType(List[0]))
    else
      SetLength(Result, 0);
  end;

  procedure ExtractName(var Name: TName; RDN : TElRelativeDistinguishedName);
  var
    List : {$ifndef DELPHI_NET}TStringList{$else}ArrayList{$endif};
  begin
    List := {$ifndef DELPHI_NET}TStringList.Create{$else}ArrayList.Create{$endif};
    try
      Name.Country := GetRDNProperty(SB_CERT_OID_COUNTRY{$ifdef CHROME}.Data{$endif}, RDN, List);
      Name.StateOrProvince := GetRDNProperty(SB_CERT_OID_STATE_OR_PROVINCE{$ifdef CHROME}.Data{$endif}, RDN, List);
      Name.Locality := GetRDNProperty(SB_CERT_OID_LOCALITY{$ifdef CHROME}.Data{$endif}, RDN, List);
      Name.Organization := GetRDNProperty(SB_CERT_OID_ORGANIZATION{$ifdef CHROME}.Data{$endif}, RDN, List);
      Name.OrganizationUnit := GetRDNProperty(SB_CERT_OID_ORGANIZATION_UNIT{$ifdef CHROME}.Data{$endif}, RDN, List);
      Name.CommonName := GetRDNProperty(SB_CERT_OID_COMMON_NAME{$ifdef CHROME}.Data{$endif}, RDN, List);
      Name.EMailAddress := GetRDNProperty(SB_CERT_OID_EMAIL{$ifdef CHROME}.Data{$endif}, RDN, List);
    finally
      FreeAndNil(List);
    end;
  end;

  procedure SetupAuxiliaryProperties;
  begin
    ExtractName(FIssuerName, FIssuerRDN);
    ExtractName(FSubjectName, FSubjectRDN);
  end;
  *)
  
var
  MainTag: TElASN1ConstrainedTag;
  Buf: BufferType;
begin
  MainTag := TElASN1ConstrainedTag.Create;
  try
    if not MainTag.LoadFromBuffer(FPData, FAllSize) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    if (MainTag.Count <> 1) or (not MainTag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    if (TElASN1ConstrainedTag(MainTag.GetField(0)).Count <> 3) or
      (not TElASN1ConstrainedTag(MainTag.GetField(0)).GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) or
      (not TElASN1ConstrainedTag(MainTag.GetField(0)).GetField(1).CheckType(SB_ASN1_SEQUENCE, true)) or
      (not TElASN1ConstrainedTag(MainTag.GetField(0)).GetField(2).CheckType(SB_ASN1_BITSTRING, false)) then
      raise EElCertificateException.Create(SInvalidtbsCert);
    // TBSCertificate
    ReadTBSCertificate(TElASN1ConstrainedTag(TElASN1ConstrainedTag(MainTag.GetField(0)).GetField(0)));
    // SignatureAlgorithm
    ReadSignatureAlgorithmIdentifier(TElASN1ConstrainedTag(TElASN1ConstrainedTag(MainTag.GetField(0)).GetField(1)));
    // Signature
    //Buf := TElASN1SimpleTag(TElASN1ConstrainedTag(TElASN1ConstrainedTag(MainTag.GetField(0)).GetField(2))).Content;
    Buf := TElASN1SimpleTag(TElASN1ConstrainedTag(MainTag.GetField(0)).GetField(2)).Content;
    if Length(Buf) > 0 then
    begin
      FSignatureValue := Copy(Buf, 2, Length(Buf));
    end;
    //SetupAuxiliaryProperties;
  finally
    FreeAndNil(MainTag);
  end;
end;

function TElX509Certificate.GetRSAParams(RSAModulus: pointer; var RSAModulusSize: integer;
  RSAPublicKey: pointer; var RSAPublicKeySize: integer): boolean;
var
  RSAParams: TElRSAEncryptionParameters;
begin
  if not (FTBSCertificate.FSubjectPublicKeyInfo.EncryptionParameters is TElRSAEncryptionParameters) then
    raise EElCertificateException.Create(SCertAlgorithmMismatch);

  RSAParams := TElRSAEncryptionParameters(FTBSCertificate.FSubjectPublicKeyInfo.EncryptionParameters);

  if (Word(RSAModulusSize) < Length(RSAParams.FPublicModulus)) or
    (RSAPublicKeySize < Length(RSAParams.FPublicExponent)) then
  begin
    RSAModulusSize := Length(RSAParams.FPublicModulus);
    RSAPublicKeySize := Length(RSAParams.FPublicExponent);
    Result := false;
  end
  else
  begin
    Move(RSAParams.FPublicModulus[0], RSAModulus^, Length(RSAParams.FPublicModulus));
    Move(RSAParams.FPublicExponent[0], RSAPublicKey^, Length(RSAParams.FPublicExponent));
    RSAModulusSize := Length(RSAParams.FPublicModulus);
    RSAPublicKeySize := Length(RSAParams.FPublicExponent);
    Result := true;
  end;
end;


function TElX509Certificate.GetDSSSignatureParams(DSSS: pointer; var DSSSSize:
  integer; DSSR: pointer; var DSSRSize: integer): boolean;
var
  R, S: string;
begin
  if not ParseDSSSignature(@FSignatureValue[1], Length(FSignatureValue), R, S) then
    Result := false
  else
  begin
    if (DSSSSize < Length(S)) or (DSSRSize < Length(R)) then
    begin
      DSSSSize := Length(S);
      DSSRSize := Length(R);
      Result := false;
    end
    else
    begin
      Move(R[1], DSSR^, Length(R));
      Move(S[1], DSSS^, Length(S));
      DSSSSize := Length(S);
      DSSRSize := Length(R);
      Result := true;
    end;
  end;
end;


function TElX509Certificate.GetDSSParams(DSSP: pointer; var DSSPSize: integer;
  DSSQ: pointer;
  var DSSQSize: integer; DSSG: pointer; var DSSGSize: integer; DSSY: pointer;
  var DSSYSize: integer): boolean;
var
  DSAParams: TElDSAEncryptionParameters;
begin
  if not (FTBSCertificate.FSubjectPublicKeyInfo.EncryptionParameters is TElDSAEncryptionParameters) then
    raise EElCertificateException.Create(SCertAlgorithmMismatch);

  DSAParams := TElDSAEncryptionParameters(FTBSCertificate.FSubjectPublicKeyInfo.EncryptionParameters);

  if (DSSPSize < Length(DSAParams.p)) or (DSSQSize < Length(DSAParams.q)) or
    (DSSYSize < Length(DSAParams.y)) or (DSSGSize < Length(DSAParams.g)) then
  begin
    DSSPSize := Length(DSAParams.p);
    DSSQSize := Length(DSAParams.q);
    DSSGSize := Length(DSAParams.g);
    DSSYSize := Length(DSAParams.y);
    Result := false;
  end
  else
  begin
    Move(DSAParams.p[0], DSSP^, Length(DSAParams.p));
    Move(DSAParams.q[0], DSSQ^, Length(DSAParams.q));
    Move(DSAParams.g[0], DSSG^, Length(DSAParams.g));
    Move(DSAParams.y[0], DSSY^, Length(DSAParams.y));
    DSSPSize := Length(DSAParams.p);
    DSSQSize := Length(DSAParams.q);
    DSSGSize := Length(DSAParams.g);
    DSSYSize := Length(DSAParams.y);
    Result := true;
  end;
end;


function TElX509Certificate.GetDHParams(DHP: pointer; var DHPSize: integer; DHG:
  pointer; var DHGSize: integer; DHY: pointer; var DHYSize: integer): boolean;
var
  DHParams: TElDHEncryptionParameters;
begin
  if not (FTBSCertificate.FSubjectPublicKeyInfo.EncryptionParameters is TElDHEncryptionParameters) then
    raise EElCertificateException.Create(SCertAlgorithmMismatch);

  DHParams := TElDHEncryptionParameters(FTBSCertificate.FSubjectPublicKeyInfo.EncryptionParameters);

  if (DHPSize < Length(DHParams.p)) or
    (DHGSize < Length(DHParams.g)) or
    (DHYSize < Length(DHParams.y)) then
  begin
    DHPSize := Length(DHParams.p);
    DHGSize := Length(DHParams.g);
    DHYSize := Length(DHParams.y);
    Result := false;
  end
  else
  begin
    Move(DHParams.P[0], DHP^, Length(DHParams.P));
    Move(DHParams.G[0], DHG^, Length(DHParams.G));
    Move(DHParams.Y[0], DHY^, Length(DHParams.Y));
    DHPSize := Length(DHParams.P);
    DHGSize := Length(DHParams.G);
    DHYSize := Length(DHParams.Y);
    Result := true;
  end;
end;



procedure TElX509Certificate.LoadFromBuffer(Buffer: Pointer; Size: Word);
begin
  
  ClearData;
  FCertificateExtensions.ClearExtensions;
  SetLength(FPrivateKeyBlob, 0);
  if Size > 32768 then
    Exit;
  Move(Buffer^, FPData^, Size);
  FAllSize := Size;
  FErrorCode := $FF;
  Count := 0;
  ReadCertificate;
  FErrorCode := 0;
  SetLength(FPrivateKey, 0);
end;

procedure TElX509Certificate.LoadFromStream(Stream: TStream; Count: integer = 0);
var
  Buffer: ByteArray;
begin
  if Count = 0 then
  begin
    Count := Stream.Size - Stream.Position;
  end
  else
    Count := Min(Integer(Stream.Size - Stream.Position), Count);
  SetLength(Buffer, Count);
  Stream.ReadBuffer(Buffer[0], Length(Buffer));
  LoadFromBuffer(@Buffer[0], Length(Buffer));
end;

function TElX509Certificate.LoadFromStreamSPC(Stream: TStream; Count: integer = 0): integer;
var
  Buffer: ByteArray;
begin
  if Count = 0 then
  begin
    Count := Stream.Size - Stream.Position;
  end
  else
    Count := Min(Integer(Stream.Size - Stream.Position), Count);
  SetLength(Buffer, Count);
  Stream.ReadBuffer(Buffer[0], Length(Buffer));
  result := LoadFromBufferSPC(@Buffer[0], Length(Buffer));
end;

{$IFNDEF BUILDER_USED}
{$IFNDEF CLX_USED}
{$ifndef FPC}
procedure TElX509Certificate.LoadFromStream(Stream: IStream; Count: integer =
  0);
var
  Buffer: array of Byte;
  STG: tagSTATSTG;
  tmp: longint;
begin
  Stream.Stat(STG, 0);
  if Count = 0 then
  begin
    Count := STG.cbSize;
  end
  else
    Count := Min(Integer(STG.cbSize), Count);
  SetLength(Buffer, Count);
  Stream.Read(@Buffer[0], Length(Buffer), @tmp);
  LoadFromBuffer(@Buffer[0], tmp);
end;
{$endif}
{$ENDIF}
{$ENDIF}

function TElX509Certificate.GetCertificateSelfSigned: boolean;
begin
  Result := CompareRDN(FIssuerRDN, FSubjectRDN);
  if Result then
  begin
    if (ceSubjectKeyIdentifier in FCertificateExtensions.Included) and
      (ceAuthorityKeyIdentifier in FCertificateExtensions.Included) then
    begin
      Result := CompareStr(FCertificateExtensions.SubjectKeyIdentifier.KeyIdentifier,
        FCertificateExtensions.AuthorityKeyIdentifier.KeyIdentifier) = 0;
    end;
  end;
end;

function TElX509Certificate.ValidateRSAPSS(PRSAModulus: pointer; RSAModulusSize:
  integer; PRSAPublicKey: pointer; RSAPublicKeySize: integer;
  HashAlg, SaltSize : integer): boolean;
var
  HashFunction : TElHashFunction;
  Hash : BufferType;
begin
  Result := false;

  if not TElHashFunction.IsAlgorithmSupported(HashAlg) then Exit;
  
  HashFunction := TElHashFunction.Create(HashAlg);

  HashFunction.Update(@FPData[FCertificateOffset], FCertificateSize);

  Hash := HashFunction.Finish;
  HashFunction.Free;

  if Length(Hash) = 0 then Exit;

  if (Length(Signature) = 0) or (RSAModulusSize = 0) or (RSAPublicKeySize = 0) then
    Exit;

  Result := SBRSA.VerifyPSS(@Hash[1], Length(Hash), HashAlg, SaltSize,
    PRSAModulus, RSAModulusSize, PRSAPublicKey, RSAPublicKeySize, @Signature[1], Length(Signature));
end;


function TElX509Certificate.ValidateRSA(PRSAModulus: pointer; RSAModulusSize:
  integer; PRSAPublicKey: pointer; RSAPublicKeySize: integer): boolean;
var
  DgInfo, CTag, AlgId : TElASN1ConstrainedTag;
  HashAlg : integer;
  Hash, EncHash : ByteArray;
  ResHash, TmpBuf : BufferType;
  LS, LPK, LM, LRes: PLInt;
  I: integer;
  HashFunction : TElHashFunction;
begin
  Result := false;
  DgInfo := nil;

  if (FSignatureAlgorithm.Algorithm = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION) then
    HashAlg := SB_ALGORITHM_DGST_MD5
  else if (FSignatureAlgorithm.Algorithm = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION) then
    HashAlg := SB_ALGORITHM_DGST_MD2
  else if (FSignatureAlgorithm.Algorithm = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION) then
    HashAlg := SB_ALGORITHM_DGST_SHA1
  else if (FSignatureAlgorithm.Algorithm = SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION) then
    HashAlg := SB_ALGORITHM_DGST_SHA224
  else if (FSignatureAlgorithm.Algorithm = SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION) then
    HashAlg := SB_ALGORITHM_DGST_SHA256
  else if (FSignatureAlgorithm.Algorithm = SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION) then
    HashAlg := SB_ALGORITHM_DGST_SHA384
  else if (FSignatureAlgorithm.Algorithm = SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION) then
    HashAlg := SB_ALGORITHM_DGST_SHA512
  else if (FSignatureAlgorithm.Algorithm = SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160) then
    HashAlg := SB_ALGORITHM_DGST_RIPEMD160
  else
  begin
    Result := false;
    exit;
  end;

  if not TElHashFunction.IsAlgorithmSupported(HashAlg) then
  begin
    Result := false;
    Exit;
  end;

  HashFunction := TElHashFunction.Create(HashAlg);

  HashFunction.Update(@FPData[FCertificateOffset], FCertificateSize);

  TmpBuf := HashFunction.Finish;
  HashFunction.Free;

  if Length(TmpBuf) = 0 then
  begin
    Result := false;
    Exit;
  end;

  SetLength(Hash, Length(TmpBuf));
  Move(TmpBuf[1], Hash[0], Length(TmpBuf));

  if (Length(Signature) = 0) or (RSAModulusSize = 0) or (RSAPublicKeySize = 0) then
  begin
    Result := false;
    Exit;
  end;

  { RSA1.5 signature validation }

  LCreate(LS);
  LCreate(LM);
  LCreate(LPK);
  LCreate(LRes);
  PointerToLint(LS, @Signature[1], Length(Signature));
  PointerToLint(LM, PRSAModulus, RSAModulusSize);
  PointerToLint(LPK, PRSAPublicKey, RSAPublicKeySize);
  try
    if (LS.Length = 0) or (LM.Length = 0) or (LPK.Length = 0) then
    begin
      Result := false;
      exit;
    end;
    LMModPower(LS, LPK, LM, LRes);
    I := LRes.Length * 4;
    SetLength(EncHash, LRes.Length * 4);

    LIntToPointer(LRes, @EncHash[0], I);

    if (Length(EncHash) < 12) or (EncHash[0] <> 0) or (EncHash[1] <> 1) then Exit;

    I := 2;
    while (I < Length(EncHash)) and (EncHash[I] = $ff) do Inc(I);
    if (I >= Length(EncHash)) or (EncHash[I] <> $00) then Exit;

    DgInfo := TElASN1ConstrainedTag.Create;
    if not DgInfo.LoadFromBuffer(@EncHash[I + 1], Length(EncHash) - I - 1) then
      Exit;
    if (DgInfo.Count <> 1) or
      (not DgInfo.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then Exit;

    CTag := TElASN1ConstrainedTag(DgInfo.GetField(0));

    if not ((CTag.Count = 2) and (CTag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)))
      and (CTag.GetField(1).CheckType(SB_ASN1_OCTETSTRING, false))
    then
      Exit;

    AlgId := TElASN1ConstrainedTag(CTag.GetField(0));

    if AlgId.Count <> 2 then Exit;
    if not (AlgId.GetField(0).CheckType(SB_ASN1_OBJECT, false) and
      AlgId.GetField(1).CheckType(SB_ASN1_NULL, false))
    then
      Exit;

    I := GetHashAlgorithmByOID(TElASN1SimpleTag(AlgId.GetField(0)).Content);
    if I <> HashAlg then Exit;

    ResHash := TElASN1SimpleTag(CTag.GetField(1)).Content;

    if Length(ResHash) <> Length(Hash) then Exit;

    if not CompareMem(@ResHash[1], @Hash[0], Length(ResHash)) then
      Result := false
    else
      Result := true;
  finally
    LDestroy(LS);
    LDestroy(LM);
    LDestroy(LPK);
    LDestroy(LRes);
    if Assigned(DgInfo) then DgInfo.Free;
  end;
end;


function TElX509Certificate.ValidateDSS(PDSSP: pointer; DSSPSize: integer;
  PDSSQ: pointer; DSSQSize: integer; PDSSG: pointer; DSSGSize: integer;
  PDSSY: pointer; DSSYSize: integer): boolean;
var
  M160: TMessageDigest160;
  LS, LR, LP, LQ, LG, LW, LT1, LT2, LU1, LU2, LY: PLInt;
  SigTag: TElASN1ConstrainedTag;
  TagS, TagR: TElASN1SimpleTag;
begin
  Result := false;
  if (DSSPSize = 0) or (DSSQSize = 0) or (DSSGSize = 0) or (DSSYSize = 0) then
    Exit;

  SigTag := TElASN1ConstrainedTag.Create;
  if not SigTag.LoadFromBuffer(@Signature[1], Length(Signature)) then
  begin
    SigTag.Free;
    Exit;
  end;
  if (SigTag.Count <> 1) or (not SigTag.GetField(0).IsConstrained) or
    (SigTag.GetField(0).TagID <> SB_ASN1_SEQUENCE) or
    (TElASN1ConstrainedTag(SigTag.GetField(0)).Count <> 2) or
    (TElASN1ConstrainedTag(SigTag.GetField(0)).GetField(0).TagID <> SB_ASN1_INTEGER) or
    (TElASN1ConstrainedTag(SigTag.GetField(0)).GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(SigTag.GetField(0)).GetField(1).TagID <> SB_ASN1_INTEGER) or
    (TElASN1ConstrainedTag(SigTag.GetField(0)).GetField(1).IsConstrained) then
  begin
    SigTag.Free;
    Exit;
  end;

  LCreate(LR);
  LCreate(LS);
  LCreate(LP);
  LCreate(LQ);
  LCreate(LG);
  LCreate(LW);
  LCreate(LT1);
  LCreate(LT2);
  LCreate(LU1);
  LCreate(LU2);
  LCreate(LY);

  PointerToLInt(LP, PDSSP, DSSPSize);
  PointerToLInt(LQ, PDSSQ, DSSQSize);
  PointerToLInt(LG, PDSSG, DSSGSize);
  PointerToLInt(LY, PDSSY, DSSYSize);

  TagR := TElASN1SimpleTag((TElASN1ConstrainedTag(SigTag.GetField(0)).GetField(0)));
  TagS := TElASN1SimpleTag((TElASN1ConstrainedTag(SigTag.GetField(0)).GetField(1)));

  PointerToLint(LR, @TagR.Content[1], Length(TagR.Content));
  PointerToLint(LS, @TagS.Content[1], Length(TagS.Content));
  SigTag.Free;
  LGCD(LS, LQ, LT1, LW);
  M160 := HashSHA1(@PByteArray(FPData)[FCertificateOffset], FCertificateSize);
  PointerToLint(LT2, @M160, 20);
  LMult(LT2, LW, LT1);
  LMod(LT1, LQ, LU1);
  LMult(LR, LW, LT1);
  LMod(LT1, LQ, LU2);
  LMModPower(LG, LU1, LP, LT1);
  LMModPower(LY, LU2, LP, LT2);
  LMult(LT1, LT2, LU1);
  LMod(LU1, LP, LU2);
  LMod(LU2, LQ, LT1);
  Result := LEqual(LT1, LR);
  LDestroy(LR);
  LDestroy(LS);
  LDestroy(LP);
  LDestroy(LQ);
  LDestroy(LG);
  LDestroy(LW);
  LDestroy(LT1);
  LDestroy(LT2);
  LDestroy(LU1);
  LDestroy(LU2);
  LDestroy(LY);
end;

function TElX509Certificate.Validate: boolean;
var
  RSAParams: TElRSAEncryptionParameters;
  RSAPSSParams : TElRSAPSSSignParameters;
  DSSParams: TElDSAEncryptionParameters;
  SaltSize, HashAlg : integer;
begin
  CheckLicenseKey();
  if not SelfSigned then
  begin
    Result := false;
    exit;
  end;

  if ((FTbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) and
    (FTbsCertificate.FSignature.Algorithm = SB_CERT_ALGORITHM_ID_RSAPSS)) or
    ((FTbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) and
    (FTbsCertificate.FSignature.Algorithm = SB_CERT_ALGORITHM_ID_RSAPSS)) then
  begin
    { RSA-PSS scheme }
    SaltSize := TElRSAPSSSignParameters(FTbsCertificate.FSignature).SaltSize;
    HashAlg := TElRSAPSSSignParameters(FTbsCertificate.FSignature).HashAlgorithm;
    
    RSAPSSParams := TElRSAPSSSignParameters(FTbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters);
    Result := ValidateRSAPSS(@RSAPSSParams.PublicModulus[0], Length(RSAPSSParams.PublicModulus),
      @RSAPSSParams.PublicExponent[0], Length(RSAPSSParams.PublicExponent),
      HashAlg, SaltSize);
  end
  else if (Assigned(FTbsCertificate.FSignature)) and (FTbsCertificate.FSignature.Algorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) then
  begin
    { RSA-OAEP - not for signing }
    Result := false;
    Exit;
  end
  else
  if FTbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
  begin
    RSAParams := TElRSAEncryptionParameters(FTbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters);
    Result :=
    ValidateRSA(@RSAParams.PublicModulus[0], Length(RSAParams.PublicModulus),
      @RSAParams.PublicExponent[0], Length(RSAParams.PublicExponent));
  end
  else
    if FTbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    DSSParams := TElDSAEncryptionParameters(FTbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters);

    Result := ValidateDSS(
      @DSSParams.P[0], Length(DSSParams.P),
      @DSSParams.Q[0], Length(DSSParams.Q),
      @DSSParams.G[0], Length(DSSParams.G),
      @DSSParams.Y[0], Length(DSSParams.Y));
  end
  else
    Result := false;
end;

function TElX509Certificate.ValidateWithCA(CACertificate: TElX509Certificate): boolean;
var
  PRSAM, PRSAK, PDSSP, PDSSQ, PDSSG, PDSSY: ^Byte;
  MSize, KSize, PSize, QSize, GSize, YSize: integer;
  PSSHashAlg, PSSSaltSize : integer;
begin
  Result := false;
  if (FSignatureAlgorithmInt = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION) or
    (FSignatureAlgorithmInt = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION) or
    (FSignatureAlgorithmInt = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION) or
    (FSignatureAlgorithmInt = SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION) or
    (FSignatureAlgorithmInt = SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION) or
    (FSignatureAlgorithmInt = SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION) or
    (FSignatureAlgorithmInt = SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION) or
    (FSignatureAlgorithmInt = SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160) 
  then
  begin
    if CACertificate.FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm <>
      SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    begin
      Result := false;
      exit;
    end;

    MSize := 0;
    KSize := 0;
    PRSAM := nil;
    PRSAK := nil;
    CACertificate.GetRSAParams(PRSAM, MSize, PRSAK, KSize);

    GetMem(PRSAM, MSize);
    GetMem(PRSAK, KSize);
    try
      CACertificate.GetRSAParams(PRSAM, MSize, PRSAK, KSize);
      Result := ValidateRSA(PRSAM, MSize, PRSAK, KSize);
    finally
      FreeMem(PRSAM);
      FreeMem(PRSAK);
    end;
  end
  else if FSignatureAlgorithmInt = SB_CERT_ALGORITHM_ID_RSAPSS then
  begin
    if (CACertificate.FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm <>
      SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) and
      (CACertificate.FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm <>
      SB_CERT_ALGORITHM_ID_RSAPSS) then
    begin
      Result := false;
      Exit;
    end;

    if FtbsCertificate.FSignature is TElRSAPSSSignParameters then
    begin
      PSSHashAlg := TElRSAPSSSignParameters(FtbsCertificate.FSignature).HashAlgorithm;
      PSSSaltSize := TElRSAPSSSignParameters(FtbsCertificate.FSignature).SaltSize;
    end
    else
    if FtbsCertificate.FSignature is TElRSAEncryptionParameters then
    begin
      PSSHashAlg := SB_ALGORITHM_DGST_SHA1;
      PSSSaltSize := 20;
    end
    else
    begin
      Result := false;
      Exit;
    end;

    MSize := 0;
    KSize := 0;
    PRSAM := nil;
    PRSAK := nil;
    CACertificate.GetRSAParams(PRSAM, MSize, PRSAK, KSize);

    GetMem(PRSAM, MSize);
    GetMem(PRSAK, KSize);
    try
      CACertificate.GetRSAParams(PRSAM, MSize, PRSAK, KSize);
      Result := ValidateRSAPSS(PRSAM, MSize,PRSAK
        , KSize, PSSHashAlg, PSSSaltSize);
    finally
      FreeMem(PRSAM);
      FreeMem(PRSAK);
    end;
  end
  else if FSignatureAlgorithmInt = SB_CERT_ALGORITHM_ID_DSA_SHA1 then
  begin
    if (CACertificate.FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm <>
      SB_CERT_ALGORITHM_ID_DSA) and
      (CACertificate.FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm <>
      SB_CERT_ALGORITHM_ID_DSA_SHA1) then
    begin
      Result := false;
      exit;
    end;
    PSize := 0;
    QSize := 0;
    GSize := 0;
    YSize := 0;
    PDSSP := nil;
    PDSSQ := nil;
    PDSSG := nil;
    PDSSY := nil;
    CACertificate.GetDSSParams(PDSSP, PSize, PDSSQ, QSize, PDSSG, GSize, PDSSY,
      YSize);
    GetMem(PDSSP, PSize);
    GetMem(PDSSQ, QSize);
    GetMem(PDSSG, GSize);
    GetMem(PDSSY, YSize);
    try
      CACertificate.GetDSSParams(PDSSP, PSize, PDSSQ, QSize, PDSSG, GSize, PDSSY,
        YSize);
      Result := ValidateDSS(PDSSP, PSize, 
        PDSSQ, QSize, 
        PDSSG, GSize, 
        PDSSY, YSize);
    finally
      FreeMem(PDSSP);
      FreeMem(PDSSQ);
      FreeMem(PDSSG);
      FreeMem(PDSSY);
    end;
  end;
end;


procedure TElX509Certificate.SetIssuer(Issuer: TName);
begin
  FNewIssuer := Issuer;
end;

procedure TElX509Certificate.SetSubject(Subject: TName);
begin
  FNewSubject := Subject;
end;


procedure TElX509Certificate.SetCACertificate(Buffer: Pointer; Size: longint);
var
  X509: TElX509Certificate;
  P1, P2, P3, P4: ^Byte;
  Size1, Size2, Size3, Size4: integer;
begin
  X509 := TElX509Certificate.Create(nil);
  X509.LoadFromBuffer(Buffer, Size);
  P1 := nil;
  P2 := nil;
  P3 := nil;
  P4 := nil;

  if (X509.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or
    (X509.PublicKeyAlgorithm = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION) or
    (X509.PublicKeyAlgorithm = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION) or
    (X509.PublicKeyAlgorithm = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION) or
    (X509.PublicKeyAlgorithm = SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION) or
    (X509.PublicKeyAlgorithm = SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION) or
    (X509.PublicKeyAlgorithm = SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION) or
    (X509.PublicKeyAlgorithm = SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION) or
    (X509.PublicKeyAlgorithm = SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160) or
    (X509.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) then
  begin
    Size1 := 0;
    Size2 := 0;
    X509.GetRSAParams(P1, Size1, P2, Size2);

    GetMem(P1, Size1);
    GetMem(P2, Size2);
    try
      X509.GetRSAParams(P1, Size1, P2, Size2);
      if FLongRSACAEncryptParams.PublicKey = nil then
        LCreate(FLongRSACAEncryptParams.PublicKey);
      if FLongRSACAEncryptParams.Modulus = nil then
        LCreate(FLongRSACAEncryptParams.Modulus);
      PointerToLInt(FLongRSACAEncryptParams.Modulus, P1, Size1);
      PointerToLInt(FLongRSACAEncryptParams.PublicKey, P2, Size2);
    finally
      FreeMem(P1);
      FreeMem(P2);
    end;
  end
  else
    if (X509.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA) or
    (X509.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA_SHA1) then
  begin
    Size1 := 0;
    Size2 := 0;
    Size3 := 0;
    Size4 := 0;
    X509.GetDSSParams(P1, Size1, P2, Size2, P3, Size3, P4, Size4);
    GetMem(P1, Size1);
    GetMem(P2, Size2);
    GetMem(P3, Size3);
    GetMem(P4, Size4);
    try
      X509.GetDSSParams(P1, Size1, P2, Size2, P3, Size3, P4, Size4);
      if FLongDSACAEncryptParams.P = nil then
        LCreate(FLongDSACAEncryptParams.P);
      if FLongDSACAEncryptParams.Q = nil then
        LCreate(FLongDSACAEncryptParams.Q);
      if FLongDSACAEncryptParams.G = nil then
        LCreate(FLongDSACAEncryptParams.G);
      if FLongDSACAEncryptParams.Y = nil then
        LCreate(FLongDSACAEncryptParams.Y);
      PointerToLInt(FLongDSACAEncryptParams.P, P1, Size1);
      PointerToLInt(FLongDSACAEncryptParams.Q, P2, Size2);
      PointerToLInt(FLongDSACAEncryptParams.G, P3, Size3);
      PointerToLInt(FLongDSACAEncryptParams.Y, P4, Size4);
    finally
      FreeMem(P1);
      FreeMem(P2);
      FreeMem(P3);
      FreeMem(P4);
    end;
  end;
  FNewIssuer.Country := X509.SubjectName.Country; //X509.Subject[0]; // issuer
  FNewIssuer.StateOrProvince := X509.SubjectName.StateOrProvince; //X509.Subject[1];
  FNewIssuer.Locality := X509.SubjectName.Locality; //X509.Subject[2];
  FNewIssuer.Organization := X509.SubjectName.Organization; //X509.Subject[3];
  FNewIssuer.OrganizationUnit := X509.SubjectName.OrganizationUnit; //X509.Subject[4];
  FNewIssuer.CommonName := X509.SubjectName.CommonName; //X509.Subject[5];
  FNewIssuer.EMailAddress := X509.SubjectName.EMailAddress;
  FIssuerRDN.Assign(X509.SubjectRDN);
//  FCACertType := X509.SignatureAlgorithm;
  FCACertType := X509.PublicKeyAlgorithm; // II 05/05/03
  FCACertTypeSign := X509.SignatureAlgorithm;
  if FCACertType = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
  begin
    if FCACertTypeSign = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION then
      FCACertType := SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION
    else
      if FCACertTypeSign = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION then
      FCACertType := SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION
    else
      if FCACertTypeSign = SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION then
      FCACertType := SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION
    else
      if FCACertTypeSign = SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION then
      FCACertType := SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION
    else
      if FCACertTypeSign = SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION then
      FCACertType := SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION
    else
      if FCACertTypeSign = SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION then
      FCACertType := SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION
    else
      FCACertType := SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION;
  end
  else
    if FCACertType = SB_CERT_ALGORITHM_ID_DSA then
    FCACertType := SB_CERT_ALGORITHM_ID_DSA_SHA1;

  // loading authority key identifier
  if ceSubjectKeyIdentifier in X509.Extensions.Included then
    FCAKeyIdentifier := X509.Extensions.SubjectKeyIdentifier.KeyIdentifier
  else
    FCAKeyIdentifier := EmptyBuffer;

  FreeAndNil(X509);
end;

procedure TElX509Certificate.SetCAPrivateKey(Buffer: Pointer; Size: longint);
var
  MSize, ESize, DSize, YSize, XSize: integer;
  M, E, D, Y, X: ByteArray;
begin
  { Checking whether the key is in PKCS format }
  if (FCACertType {Sign} = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION) or
    (FCACertType {Sign} = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION) or
    (FCACertType {Sign} = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION) or
    (FCACertType {Sign} = SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION) or
    (FCACertType {Sign} = SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION) or
    (FCACertType {Sign} = SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION) or
    (FCACertType {Sign} = SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION) or
    (FCACertType {Sign} = SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160) or
    (FCACertType {Sign} = SB_CERT_ALGORITHM_ID_RSAPSS) then
  begin
    if FLongRSACAEncryptParams.PrivateExponent = nil then
      LCreate(FLongRSACAEncryptParams.PrivateExponent);
    MSize := 0;
    ESize := 0;
    DSize := 0;
    SBRSA.DecodePrivateKey(Buffer, Size, nil, MSize, nil, ESize, nil, DSize);
    if (MSize = 0) and (ESize = 0) and (DSize = 0) then
      PointerToLInt(FLongRSACAEncryptParams.PrivateExponent, Buffer, Size)
    else
    begin
      SetLength(M, MSize);
      SetLength(E, ESize);
      SetLength(D, DSize);
      if not SBRSA.DecodePrivateKey(Buffer, Size, @M[0], MSize, @E[0], ESize, @D[0],
        DSize) then
        raise EElCertificateException.Create(SInternalError);
      PointerToLInt(FLongRSACAEncryptParams.PrivateExponent, @D[0], DSize);
    end;
  end
  else
    if FCACertType {Sign} = SB_CERT_ALGORITHM_ID_DSA_SHA1 then
  begin
    if FLongDSACAEncryptParams.X = nil then
      LCreate(FLongDSACAEncryptParams.X);
    MSize := 0;
    ESize := 0;
    DSize := 0;
    YSize := 0;
    XSize := 0;
    SBDSA.DecodePrivateKey(Buffer, Size, nil, MSize, nil, ESize, nil, DSize, nil, YSize,
      nil, XSize);
    if (MSize = 0) and (ESize = 0) and (DSize = 0) and (YSize = 0) and (XSize = 0) then
      PointerToLInt(FLongDSACAEncryptParams.X, Buffer, Size)
    else
    begin
      SetLength(M, MSize);
      SetLength(E, ESize);
      SetLength(D, DSize);
      SetLength(Y, YSize);
      SetLength(X, XSize);
      if not SBDSA.DecodePrivateKey(Buffer, Size, @M[0], MSize, @E[0], ESize, @D[0],
        DSize, @Y[0], YSize, @X[0], XSize) then
        raise EElCertificateException.Create(SInternalError);
      PointerToLInt(FLongDSACAEncryptParams.X, @X[0], XSize);
    end;
  end;
end;

function TElX509Certificate.GenerateRSA(Words: integer; Params: PLongRSAParams): boolean;
var
  MSize, ESize, DSize, BSize: integer;
  M, E, D: ByteArray;
begin
  MSize := 0;
  ESize := 0;
  DSize := 0;
  BSize := 0;
  SBRSA.Generate(Words shl 5, nil, MSize, nil, ESize, nil, DSize, nil, BSize);
  SetLength(M, MSize);
  SetLength(E, ESize);
  SetLength(D, DSize);
  SetLength(FPrivateKeyBlob, BSize);
  if not SBRSA.Generate(Words shl 5, @M[0], MSize, @E[0], ESize, @D[0], DSize,
    @FPrivateKeyBlob[0], BSize) then
    raise EElCertificateException.Create(SInternalError);
  SetLength(FPrivateKeyBlob, BSize);
  PointerToLInt(Params.Modulus, @M[0], MSize);
  PointerToLInt(Params.PublicKey, @E[0], ESize);
  PointerToLInt(Params.PrivateExponent, @D[0], DSize);
  Result := true;
end;

function TElX509Certificate.GenerateDSA(Bits: integer; Params: PLongDSAParams): boolean;
var
  PSize, QSize, GSize, YSize, XSize, BSize: integer;
  P, Q, G, Y, X: ByteArray;
begin
  PSize := 0;
  QSize := 0;
  GSize := 0;
  YSize := 0;
  XSize := 0;
  BSize := 0;
  SBDSA.Generate(Bits, nil, PSize, nil, QSize, nil, GSize, nil, YSize,
    nil, XSize, nil, BSize);
  SetLength(P, PSize);
  SetLength(Q, QSize);
  SetLength(G, GSize);
  SetLength(X, XSize);
  SetLength(Y, YSize);
  SetLength(FPrivateKeyBlob, BSize);
  if not SBDSA.Generate(Bits, @P[0], PSize, @Q[0], QSize, @G[0], GSize,
    @Y[0], YSize, @X[0], XSize, @FPrivateKeyBlob[0], BSize) then
    raise EElCertificateException.Create(SInternalError);
  SetLength(FPrivateKeyBlob, BSize);
  PointerToLInt(Params.Q, @Q[0], QSize);
  PointerToLInt(Params.P, @P[0], PSize);
  PointerToLInt(Params.G, @G[0], GSize);
  PointerToLInt(Params.X, @X[0], XSize);
  PointerToLInt(Params.Y, @Y[0], YSize);
  Result := true;
end;

function TElX509Certificate.GenerateDH(Bits: integer; Params: PLongDHParams): boolean;
//var
//  Ctx: TRC4RandomContext;
begin
  LGenPrime(Params.P, Bits shr 5{$ifndef HAS_DEF_PARAMS}, false{$endif});
  LInit(Params.G, '2');
  //LRC4Init(Ctx);
  //LRandom(Ctx, Params.X, Bits shr 3 - 1);
  SBRndGenerateLInt(Params.X, Bits shr 3 - 1);
//  Result.X^.Digits[1] := Result.X^.Digits[1] and $FFFFFF00;
  Params.X.Digits[16] := Params.X.Digits[16] and $00FFFFFF;
  LMModPower(Params.G, Params.X, Params.P, Params.Ys);
  Result := true;
end;

procedure TElX509Certificate.SetupOriginalKeyMaterial;
var
  K, N, E, P, Q, G, Y: ByteArray;
  NSize, ESize, PSize, QSize, GSize, YSize: integer;
begin
  NSize := 0;
  SaveKeyValueToBuffer(nil, NSize);
  SetLength(K, NSize);
  SaveKeyValueToBuffer(@K[0], NSize);
  SetLength(K, NSize);
  case PublicKeyAlgorithm of
    SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_ID_RSAPSS,
    SB_CERT_ALGORITHM_ID_RSAOAEP:
      begin
        NSize := 0;
        GetRSAParams(nil, NSize, nil, ESize);
        SetLength(N, NSize);
        SetLength(E, ESize);
        GetRSAParams(@N[0], NSize, @E[0], ESize);
        PointerToLInt(FLongRSAEncryptParams.PublicKey, @E[0], ESize);
        PointerToLInt(FLongRSAEncryptParams.Modulus, @N[0], NSize);
        if Length(K) > 0 then
          PointerToLInt(FLongRSAEncryptParams.PrivateExponent, @K[0], Length(K));
      end;
    SB_CERT_ALGORITHM_ID_DSA:
      begin
        PSize := 0;
        GetDSSParams(nil, PSize, nil, QSize, nil, GSize, nil, YSize);
        SetLength(P, PSize);
        SetLength(Q, QSize);
        SetLength(G, GSize);
        SetLength(Y, YSize);
        GetDSSParams(@P[0], PSize, @Q[0], QSize, @G[0], GSize, @Y[0], YSize);
        PointerToLInt(FLongDSAEncryptParams.P, @P[0], PSize);
        PointerToLInt(FLongDSAEncryptParams.Q, @Q[0], QSize);
        PointerToLInt(FLongDSAEncryptParams.G, @G[0], GSize);
        PointerToLInt(FLongDSAEncryptParams.Y, @Y[0], YSize);
        if Length(K) > 0 then
          PointerToLInt(FLongDSAEncryptParams.X, @K[0], Length(K));
      end;
    SB_CERT_ALGORITHM_DH_PUBLIC:
      begin
        PSize := 0;
        GetDHParams(nil, PSize, nil, GSize, nil, YSize);
        SetLength(P, PSize);
        SetLength(G, GSize);
        SetLength(Y, YSize);
        GetDHParams(@P[0], PSize, @G[0], GSize, @Y[0], YSize);
        PointerToLInt(FLongDHEncryptParams.P, @P[0], PSize);
        PointerToLInt(FLongDHEncryptParams.G, @G[0], GSize);
        PointerToLInt(FLongDHEncryptParams.Ys, @Y[0], YSize);
        if Length(K) > 0 then
          PointerToLInt(FLongDHEncryptParams.X, @K[0], Length(K));
      end;
  end;
end;

procedure TElX509Certificate.Generate(Algorithm: integer; DWordsInEncryptKey:
  integer);
var
  Sz: integer;
  CertSz, KeySz: word;
  Buf,
    KeyBuf: array[0..16384] of byte;
  PubKeyBuf: ByteArray;
  M160: TMessageDigest160;
begin
  CheckLicenseKey();
  FGenerate := true;
  if not FPreserveKeyMaterial then
  begin
    SetLength(FPrivateKeyBlob, 0);
    FGenSignatureAlgorithm := Algorithm;
    if (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160) then
      FGenEncryptAlgorithm := SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION
    else
      if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_ID_DSA_SHA1 then
      FGenEncryptAlgorithm := SB_CERT_ALGORITHM_ID_DSA
    else
      FGenEncryptAlgorithm := Algorithm;

    if not FGeneratingFromRequest then
    begin
      if (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or
        (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) or
        (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) then
        GenerateRSA(DWordsInEncryptKey, @FLongRSAEncryptParams)
      else
        if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
        GenerateDSA(DWordsInEncryptKey * 32, @FLongDSAEncryptParams)
      else
        if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_DH_PUBLIC then
      begin
        GenerateDH(DWordsInEncryptKey shl 5, @FLongDHEncryptParams);
        FGenSignatureAlgorithm := SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION;
      end;
    end
    else
    begin
      // nothing
    end;
  end
  else
  begin
    if not FGeneratingFromRequest then
    begin
      if (not FCAAvailable) and (not PrivateKeyExists) then
        RaiseX509Error(SB_X509_ERROR_NO_PRIVATE_KEY);

      FGenEncryptAlgorithm := PublicKeyAlgorithm;
      FGenSignatureAlgorithm := SignatureAlgorithm;
      SetupOriginalKeyMaterial;
    end;
  end;

  if FCAAvailable then
    FGenSignatureAlgorithm := FCACertType;

// writing key to FPrivateKey and computing key identifiers

  if (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or
    (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) or
    (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) then
  begin
    if not FGeneratingFromRequest then
    begin
      Sz := FLongRSAEncryptParams.PrivateExponent.Length * 4;
      SetLength(FPrivateKey, Sz);
      LIntToPointer(FLongRSAEncryptParams.PrivateExponent, @FPrivateKey[0], Sz);
    end
    else
      SetLength(FPrivateKey, 0);
    // computing key identifier
    Sz := (FLongRSAEncryptParams.PublicKey.Length + FLongRSAEncryptParams.Modulus.Length) shl 2;
    SetLength(PubKeyBuf, Sz);
    LIntToPointer(FLongRSAEncryptParams.PublicKey, @PubKeyBuf[0], Sz);
    Sz := Length(PubKeyBuf) - Sz;
    LIntToPointer(FLongRSAEncryptParams.Modulus, @PubKeyBuf[Length(PubKeyBuf) - Sz], Sz);
    M160 := HashSHA1(@PubKeyBuf[0], Length(PubKeyBuf));
    SetLength(FOurKeyIdentifier, 20);
    Move(M160, FOurKeyIdentifier[1], 20);
  end
  else
    if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_DH_PUBLIC then
  begin
    Sz := FLongDHEncryptParams.X.Length * 4;
    SetLength(FPrivateKey, Sz);
    LIntToPointer(FLongDHEncryptParams.X, @FPrivateKey[0], Sz);
    // computing key identifier
    Sz := (FLongDHEncryptParams.P.Length + FLongDHEncryptParams.Ys.Length) shl 2;
    SetLength(PubKeyBuf, Sz);
    LIntToPointer(FLongDHEncryptParams.P, @PubKeyBuf[0], Sz);
    Sz := Length(PubKeyBuf) - Sz;
    LIntToPointer(FLongDHEncryptParams.Ys, @PubKeyBuf[Length(PubKeyBuf) - Sz], Sz);
    M160 := HashSHA1(@PubKeyBuf[0], Length(PubKeyBuf));
    SetLength(FOurKeyIdentifier, 20);
    Move(M160, FOurKeyIdentifier[1], 20);
  end
  else
    if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    if not FGeneratingFromRequest then
    begin
      Sz := FLongDSAEncryptParams.X.Length * 4;
      SetLength(FPrivateKey, Sz);
      LIntToPointer(FLongDSAEncryptParams.X, @FPrivateKey[0], Sz);
    end
    else
      SetLength(FPrivateKey, 0);
    // computing key identifier
    Sz := (FLongDSAEncryptParams.P.Length + FLongDSAEncryptParams.Y.Length) shl 2;
    SetLength(PubKeyBuf, Sz);
    LIntToPointer(FLongDSAEncryptParams.P, @PubKeyBuf[0], Sz);
    Sz := Length(PubKeyBuf) - Sz;
    LIntToPointer(FLongDSAEncryptParams.Y, @PubKeyBuf[Length(PubKeyBuf) - Sz], Sz);
    M160 := HashSHA1(@PubKeyBuf[0], Length(PubKeyBuf));
    SetLength(FOurKeyIdentifier, 20);
    Move(M160, FOurKeyIdentifier[1], 20);
  end;

  if (ceSubjectKeyIdentifier in FCertificateExtensions.Included) and
  (FCertificateExtensions.SubjectKeyIdentifier.KeyIdentifier = '') then
    FCertificateExtensions.SubjectKeyIdentifier.KeyIdentifier := FOurKeyIdentifier;
  if (ceAuthorityKeyIdentifier in FCertificateExtensions.Included) and
  (FCertificateExtensions.AuthorityKeyIdentifier.KeyIdentifier = '') then
  begin
    if FCAAvailable then
      FCertificateExtensions.AuthorityKeyIdentifier.KeyIdentifier := FCAKeyIdentifier
    else
      FCertificateExtensions.AuthorityKeyIdentifier.KeyIdentifier := FOurKeyIdentifier;
  end;

// end writing
// now writing binary certificate code

  CertSz := 16384;
  KeySz := 16384;
  InternalSaveToBuffer(@Buf[0], CertSz);
  SaveKeyToBuffer(@KeyBuf[0], KeySz);


  LoadFromBuffer(@Buf[0], CertSz);
  LoadKeyFromBuffer(@KeyBuf[0], KeySz);
  FGenerate := false;
end;

procedure TElX509Certificate.Generate(Request: TElCertificateRequest;
  Certificate: TElX509Certificate);
var
  I: integer;
  BufA, BufB, BufC, BufD: ByteArray;
  SzA, SzB, SzC, SzD: integer;
begin
  CheckLicenseKey();
  if not PrivateKeyExists then
  //if Length(FPrivateKey) = 0 then
    raise EElCertificateException.Create(SPrivateKeyNotFound);
  if not Assigned(Certificate) then
    raise EElCertificateException.Create(SInvalidPointer);
  if not Request.ValidateSignature then
    raise EElCertificateException.Create(SInvalidRequestSignature);
  Certificate.SetCACertificate(FPData, FAllSize);
(*
  {$ifndef DELPHI_NET}
  Certificate.SetCAPrivateKey(@FPrivateKey[0], Length(FPrivateKey));
  {$else}
  Certificate.SetCAPrivateKey(FPrivateKey);
  {$endif}
*)
  Certificate.SetCAPrivateKey(@FPrivateKeyBlob[0], Length(FPrivateKeyBlob));
  Certificate.CAAvailable := true;
  Certificate.SubjectRDN.Count := Request.Subject.Count;
  for I := 0 to Request.Subject.Count - 1 do
  begin
    Certificate.SubjectRDN.OIDs[I] := CloneBuffer(Request.Subject.OIDs[I]);
    Certificate.SubjectRDN.Values[I] := CloneBuffer(Request.Subject.Values[I]);
  end;
  if (Request.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or
    (Request.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) or
    (Request.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) then
  begin
    LCreate(Certificate.FLongRSAEncryptParams.PublicKey);
    LCreate(Certificate.FLongRSAEncryptParams.Modulus);
    LCreate(Certificate.FLongRSAEncryptParams.PrivateExponent);
    SzA := 0;
    SzB := 0;
    Request.GetRSAParams(nil, SzA, nil, SzB);
    SetLength(BufA, SzA);
    SetLength(BufB, SzB);

    Request.GetRSAParams(@BufA[0], SzA, @BufB[0], SzB);
    PointerToLInt(Certificate.FLongRSAEncryptParams.Modulus, @BufA[0], SzA);
    PointerToLInt(Certificate.FLongRSAEncryptParams.PublicKey, @BufB[0], SzB);
    SzA := 0;
    Request.GetPrivateKey(nil, SzA);
    if SzA > 0 then
    begin
      SetLength(BufA, SzA);
      Request.GetPrivateKey(@BufA[0], SzA);
      PointerToLInt(Certificate.FLongRSAEncryptParams.PrivateExponent, @BufA[0], SzA);
    end;
  end
  else
    if Request.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    LCreate(Certificate.FLongDSAEncryptParams.P);
    LCreate(Certificate.FLongDSAEncryptParams.Q);
    LCreate(Certificate.FLongDSAEncryptParams.G);
    LCreate(Certificate.FLongDSAEncryptParams.Y);
    SzA := 0;
    SzB := 0;
    SzC := 0;
    SzD := 0;
    Request.GetDSAParams(nil, SzA, nil, SzB, nil, SzC, nil, SzD);
    SetLength(BufA, SzA);
    SetLength(BufB, SzB);
    SetLength(BufC, SzC);
    SetLength(BufD, SzD);
    Request.GetDSAParams(@BufA[0], SzA, @BufB[0], SzB, @BufC[0], SzC, @BufD[0], SzD);
    PointerToLInt(Certificate.FLongDSAEncryptParams.P, @BufA[0], SzA);
    PointerToLInt(Certificate.FLongDSAEncryptParams.Q, @BufB[0], SzB);
    PointerToLInt(Certificate.FLongDSAEncryptParams.G, @BufC[0], SzC);
    PointerToLInt(Certificate.FLongDSAEncryptParams.Y, @BufD[0], SzD);
  end
  else
    raise EElCertificateException.Create(SUnknownAlgorithm);
  Certificate.FGeneratingFromRequest := true;
  Certificate.Generate(Request.PublicKeyAlgorithm, 16);
end;

procedure TElX509Certificate.Generate(Parent: TElX509Certificate; Algorithm: integer;
  DWordsInEncryptKey: integer);
var
  PrivateKeyAvailable: boolean;
  Sz: word;
  PrivKey: ByteArray;
begin
  CheckLicenseKey();
  PrivateKeyAvailable := true;
  if Parent.PrivateKeyExists then
  begin
    Sz := 0;
    Parent.SaveKeyToBuffer(nil, Sz);
    SetLength(PrivKey, Sz);
    Parent.SaveKeyToBuffer(@PrivKey[0], Sz);
    SetLength(PrivKey, Sz);
    if Sz = 0 then
      PrivateKeyAvailable := false
  end
  else
    PrivateKeyAvailable := false;
  if not PrivateKeyAvailable then
    RaiseX509Error(SB_X509_ERROR_NO_PRIVATE_KEY);

  SetCACertificate(Parent.CertificateBinary, Parent.CertificateSize);
  SetCAPrivateKey(@PrivKey[0], Sz);
  CAAvailable := true;
  if Algorithm in [SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160] then
    Algorithm := SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION
  else
    if Algorithm = SB_CERT_ALGORITHM_ID_DSA_SHA1 then
    Algorithm := SB_CERT_ALGORITHM_ID_DSA;

  FParent := Parent;
  Generate(Algorithm, DWordsInEncryptKey);
  FParent := nil;
end;

procedure TElX509Certificate.BeginGenerate(Algorithm: integer; DWordsInEncryptKey: integer); 
begin
  SetLength(FPrivateKeyBlob, 0);                               
  FGenSignatureAlgorithm := Algorithm;
  if (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION) or
    (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION) or
    (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION) or
    (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION) or
    (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION) or
    (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION) or
    (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION) or
    (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160) then
    FGenEncryptAlgorithm := SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION
  else if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_ID_DSA_SHA1 then
    FGenEncryptAlgorithm := SB_CERT_ALGORITHM_ID_DSA
  else
    FGenEncryptAlgorithm := Algorithm;

  if (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or
    (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) or
    (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) then
    FGenerationToken := GetGlobalAsyncCalculator.BeginRSAGeneration(DWordsInEncryptKey * 32)
  else if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
    FGenerationToken := GetGlobalAsyncCalculator.BeginDSAGeneration(DWordsInEncryptKey * 32)
  else
    // DH is not supported by this method, the overloaded method should be used instead
    raise EX509Error.Create(SInvalidPublicKeyAlgorithm);
end;

procedure TElX509Certificate.BeginGenerate(Parent: TElX509Certificate; Algorithm: integer;
  DWordsInEncryptKey: integer); 
var
  PrivateKeyAvailable: boolean;
  Sz: word;
  PrivKey: ByteArray;
begin
  CheckLicenseKey();
  PrivateKeyAvailable := true;
  if Parent.PrivateKeyExists then
  begin
    Sz := 0;
    Parent.SaveKeyToBuffer(nil, Sz);
    SetLength(PrivKey, Sz);
    Parent.SaveKeyToBuffer(@PrivKey[0], Sz);
    SetLength(PrivKey, Sz);
    if Sz = 0 then
      PrivateKeyAvailable := false
  end
  else
    PrivateKeyAvailable := false;
  if not PrivateKeyAvailable then
    RaiseX509Error(SB_X509_ERROR_NO_PRIVATE_KEY);

  SetCACertificate(Parent.CertificateBinary, Parent.CertificateSize);
  SetCAPrivateKey(@PrivKey[0], Sz);
  CAAvailable := true;
  if Algorithm in [SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160] then
    Algorithm := SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION
  else
    if Algorithm = SB_CERT_ALGORITHM_ID_DSA_SHA1 then
    Algorithm := SB_CERT_ALGORITHM_ID_DSA;

  FParent := Parent;
  BeginGenerate(Algorithm, DWordsInEncryptKey);
end;

procedure TElX509Certificate.EndGenerate();
  procedure EndGenerateRSA;
  var
    BlobSize : integer;
    MBuf, EBuf, DBuf : ByteArray;
    MSize, ESize, DSize : integer;
    EncParams : TElRSAEncryptionParameters;
  begin
    BlobSize := 0;
    GetGlobalAsyncCalculator.EndRSAGeneration(FGenerationToken, nil, BlobSize);
    SetLength(FPrivateKeyBlob, BlobSize);
    if not GetGlobalAsyncCalculator.EndRSAGeneration(FGenerationToken, @FPrivateKeyBlob[0], BlobSize) then
      raise EElCertificateException.Create(SInternalError);
    MSize := 0;
    SBRSA.DecodePrivateKey(@FPrivateKeyBlob[0], BlobSize, nil, MSize, nil,
      ESize, nil, DSize);
    SetLength(MBuf, MSize);
    SetLength(EBuf, ESize);
    SetLength(DBuf, DSize);
    SBRSA.DecodePrivateKey(@FPrivateKeyBlob[0], BlobSize, @MBuf[0], MSize, @EBuf[0],
      ESize, @DBuf[0], DSize);
    SetLength(MBuf, MSize);
    SetLength(EBuf, ESize);
    SetLength(DBuf, DSize);
    PointerToLInt(FLongRSAEncryptParams.PrivateExponent, @DBuf[0], DSize);
    PointerToLInt(FLongRSAEncryptParams.PublicKey, @EBuf[0], ESize);
    PointerToLInt(FLongRSAEncryptParams.Modulus, @MBuf[0], MSize);
    EncParams := TElRSAEncryptionParameters.Create;
    EncParams.FAlgorithm := FGenEncryptAlgorithm;
    EncParams.FPublicExponent := EBuf;
    EncParams.FPrivateExponent := DBuf;
    EncParams.FPublicModulus := MBuf;
    FtbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters := EncParams;
    FtbsCertificate.FSubjectPublicKeyInfo.FPublicKeyAlgorithm := FGenEncryptAlgorithm;
    FSignatureAlgorithmInt := FGenSignatureAlgorithm;
  end;

  procedure EndGenerateDSA;
  var
    BlobSize : integer;
    EncParams : TElDSAEncryptionParameters;
    PBuf, QBuf, GBuf, YBuf, XBuf : ByteArray;
    PSize, QSize, GSize, YSize, XSize : integer;
  begin
    BlobSize := 0;
    GetGlobalAsyncCalculator.EndDSAGeneration(FGenerationToken, nil, BlobSize);
    SetLength(FPrivateKeyBlob, BlobSize);
    if not GetGlobalAsyncCalculator.EndDSAGeneration(FGenerationToken, @FPrivateKeyBlob[0], BlobSize) then
      raise EElCertificateException.Create(SInternalError);
    GetGlobalAsyncCalculator.EndDSAGeneration(FGenerationToken, FLongDSAEncryptParams.P,
      FLongDSAEncryptParams.Q, FLongDSAEncryptParams.G, FLongDSAEncryptParams.X,
      FLongDSAEncryptParams.Y);
    PSize := 0;
    SBDSA.DecodePrivateKey(@FPrivateKeyBlob[0], BlobSize, nil, PSize, nil, QSize,
      nil, GSize, nil, YSize, nil, XSize);
    SetLength(PBuf, PSize);
    SetLength(QBuf, QSize);
    SetLength(GBuf, GSize);
    SetLength(YBuf, YSize);
    SetLength(XBuf, XSize);
    SBDSA.DecodePrivateKey(@FPrivateKeyBlob[0], BlobSize, @PBuf[0], PSize,
      @QBuf[0], QSize, @GBuf[0], GSize, @YBuf[0], YSize, @XBuf[0], XSize);
    SetLength(PBuf, PSize);
    SetLength(QBuf, QSize);
    SetLength(GBuf, GSize);
    SetLength(YBuf, YSize);
    SetLength(XBuf, XSize);
    EncParams := TElDSAEncryptionParameters.Create;
    EncParams.FAlgorithm := FGenEncryptAlgorithm;
    EncParams.FP := PBuf;
    EncParams.FQ := QBuf;
    EncParams.FG := GBuf;
    EncParams.FY := YBuf;
    EncParams.FX := XBuf;
    FtbsCertificate.FSubjectPublicKeyInfo.FEncryptionParameters := EncParams;
    FtbsCertificate.FSubjectPublicKeyInfo.FPublicKeyAlgorithm := FGenEncryptAlgorithm;
    FSignatureAlgorithmInt := FGenSignatureAlgorithm;
  end;
begin
  if not Assigned(FGenerationToken) then
    raise EX509Error.Create(SCertIsNotBeingGenerated);
  try
    if (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or
      (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) or
      (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) then
      EndGenerateRSA()
    else if (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_DSA) then
      EndGenerateDSA()
    else
      raise EX509Error.Create(SInvalidPublicKeyAlgorithm);
  finally
    if Assigned(FGenerationToken) then
      FreeAndNil(FGenerationToken);
  end;
  FPrivateKeyExists := true;
  FPreserveKeyMaterial := true;
  try
    Generate(-1, 0); // specifying fake values since PreserveKeyMaterial is on
  finally
    FPreserveKeyMaterial := false;
  end;
  FParent := nil;
end;

procedure TElX509Certificate.CancelGeneration();
begin
  if Assigned(FGenerationToken) then
  begin
    try
      FGenerationToken.Cancel;
    finally
      FreeAndNil(FGenerationToken);
    end;
  end
  else
    raise EX509Error.Create(SCertIsNotBeingGenerated);
end;

function TElX509Certificate.AsyncOperationFinished : boolean;
begin
  Result := Assigned(FGenerationToken) and (FGenerationToken.Finished);
end;

function TElX509Certificate.InternalSaveToBuffer(P: Pointer; var Size: Word): boolean;
var
  Fields: TStringList;
  Cert: string;
begin
  Fields := TStringList.Create;
  Result := false;
  if FGenerate = false then
  begin
    if Size < FAllSize then
    begin
      Size := FAllSize;
      Result := false;
    end
    else
    begin
      Size := FAllSize;
      Move(FPData^, P^, FAllSize);
    end;
  end
  else
  begin
    Fields.Add(WriteTBSCertificate);
    Fields.Add(WriteSignatureAlgorithm);
    Fields.Add(WriteSignatureValue(Fields[0]));
    Cert := WriteSequence(Fields);
    if Length(Cert) > Size then
    begin
      Result := false;
      Size := Length(Cert);
    end
    else
    begin
      Move(Cert[1], PByteArray(P)[0], Length(Cert));
      Size := Length(Cert);
      Result := true;
    end;
  end;
  Fields.Free;
end;


function TElX509Certificate.SaveToBuffer(Buffer: Pointer; var Size: Word): boolean;
begin
  Result := true;
  if Size < FAllSize then
  begin
    Size := FAllSize;
    Result := false;
  end
  else
  begin
    Size := FAllSize;
    Move(FPData^, Buffer^, FAllSize);
  end;
end;

procedure TElX509Certificate.SaveToStream(Stream: TStream);
var
  Fields: TStringList;
  Cert: string;
begin
  Fields := TStringList.Create;

  if FGenerate = false then
  begin
    Stream.WriteBuffer(FPData^, FAllSize);
  end
  else
  begin
    Fields.Add(WriteTBSCertificate);
    Fields.Add(WriteSignatureAlgorithm);
    Fields.Add(WriteSignatureValue(Fields.Strings[0]));
    Cert := WriteSequence(Fields);
    Stream.WriteBuffer(Cert[1], Length(Cert));
  end;
  Fields.Free;
end;


function TElX509Certificate.SaveKeyToBuffer(Buffer: Pointer; var Size: Word): boolean;
var
  ErrCode: boolean;
begin
  if Length(FPrivateKeyBlob) > 0 then
  begin
    if (Size < Length(FPrivateKeyBlob)) then
    begin
      Size := Length(FPrivateKeyBlob);
      Result := false;
    end
    else
    begin
      Size := Length(FPrivateKeyBlob);
      Move(FPrivateKeyBlob[0], Buffer^, Size);
      Result := true;
    end;
  end
  else
  begin
    if BelongsTo = BT_WINDOWS then
    begin
{$IFDEF SB_HAS_WINCRYPT}
      ErrCode := LoadPrivateKeyFromWin32;
{$ELSE}
      ErrCode := false;
{$ENDIF}
      if (ErrCode) and (Length(FPrivateKeyBlob) > 0) then
        Result := SaveKeyToBuffer(Buffer, Size)
      else
      begin
        Result := false;
        Size := 0;
      end;
    end
    else
    if BelongsTo = BT_PKCS11 then
    begin
      result := false;
      Size := 0;
    end
    else
    begin
      if Size < Length(FPrivateKey) then
      begin
        Size := Length(FPrivateKey);
        Result := false;
      end
      else
      begin
        Size := Length(FPrivateKey);
        Move(FPrivateKey[0], Buffer^, Size);
        Result := true;
      end;
    end;
  end;
end;

procedure TElX509Certificate.SaveKeyToStream(Stream: TStream);
var
  Buf: ByteArray;
  Size: word;
begin
  Size := 0;
  SetLength(Buf, Size);
  SaveKeyToBuffer(nil, Size);
  SetLength(Buf, Size);
  SaveKeyToBuffer(@Buf[0], Size);
  Stream.WriteBuffer(Buf[0], Size);
end;

function TElX509Certificate.WriteTBSCertificate: BufferType;
var
  Fields: TStringList;
  Writer : TElExtensionWriter;
begin
  Fields := TStringList.Create;
  Fields.Add(WriteVersion);
  Fields.Add(WriteSerialNumber);
  Fields.Add(WriteSignature);
  Fields.Add(WriteIssuer);
  Fields.Add(WriteValidity);
  Fields.Add(WriteSubject);
  Fields.Add(WriteSubjectPublicKeyInfo);
  if (FCertificateExtensions.Included <> []) or (FCertificateExtensions.OtherCount > 0) then
  begin
    Writer := TElExtensionWriter.Create(FCertificateExtensions{$ifndef HAS_DEF_PARAMS}, true{$endif});
    try
      Fields.Add(Writer.WriteExtensions);
    finally
      FreeAndNil(Writer);
    end;
  end;
  Result := WriteSequence(Fields);
  Fields.Free;
end;

function TElX509Certificate.WriteSignatureAlgorithm: BufferType;
var
  Params: TStringList;
begin
  Params := TStringList.Create;

  if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION then
    Result := WriteAlgorithmIdentifier(SB_CERT_OID_MD5_RSAENCRYPTION, Params)
  else
    if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION then
    Result := WriteAlgorithmIdentifier(SB_CERT_OID_MD2_RSAENCRYPTION, Params)
  else
    if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION then
    Result := WriteAlgorithmIdentifier(SB_CERT_OID_SHA1_RSAENCRYPTION, Params)
  else
    if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION then
    Result := WriteAlgorithmIdentifier(SB_CERT_OID_SHA224_RSAENCRYPTION, Params)
  else
    if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION then
    Result := WriteAlgorithmIdentifier(SB_CERT_OID_SHA256_RSAENCRYPTION, Params)
  else
    if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION then
    Result := WriteAlgorithmIdentifier(SB_CERT_OID_SHA384_RSAENCRYPTION, Params)
  else
    if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION then
    Result := WriteAlgorithmIdentifier(SB_CERT_OID_SHA512_RSAENCRYPTION, Params)
  else
    if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_ID_DSA_SHA1 then
    Result := WriteAlgorithmIdentifier(SB_CERT_OID_DSA_SHA1, Params)
  else
    if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160 then
    Result := WriteAlgorithmIdentifier(SB_OID_RSASIGNATURE_RIPEMD160, Params)
  else
    if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS then
    begin
      if FCAAvailable then
      begin
        if Assigned(FParent) then
          Params.Add(WritePSSParams(FParent.PSSParams.HashAlgorithm,
            FParent.PSSParams.SaltSize, FParent.PSSParams.MGF, FParent.PSSParams.TrailerField))
        else
          Params.Add(WritePSSParams{$ifndef HAS_DEF_PARAMS}(SB_ALGORITHM_DGST_SHA1, 20, SB_CERT_MGF1, 1){$endif});
      end
      else
        Params.Add(WritePSSParams(FPSSParams.HashAlgorithm,
          FPSSParams.SaltSize, FPSSParams.MGF, FPSSParams.TrailerField));

      Result := WriteAlgorithmIdentifier(SB_CERT_OID_RSAPSS, Params)
    end;

  Params.Free;
end;

const
  ASN_MD5: array[0..17] of byte =
   (
    $30, $20, $30, $0C, $06, $08, $2A, $86,
    $48, $86, $F7, $0D, $02, $05, $05, $00, $04, $10
   );
  ASN_MD2: array[0..17] of byte =
   (
    $30, $20, $30, $0C, $06, $08, $2A, $86,
    $48, $86, $F7, $0D, $02, $02, $05, $00, $04, $10
   );
  ASN_SHA1: array[0..14] of byte =
   (
    $30, $21, $30, $09, $06, $05, $2B, $0E,
    $03, $02, $1A, $05, $00, $04, $14
   );
  ASN_SHA224: array[0..18] of byte =
   (
    $30, $2D, $30, $0D, $06, $09, $60, $86, $48, $01, $65, $03, $04, $02, $04,
    $05, $00, $04, $1C
    );
  ASN_SHA256: array[0..18] of byte =
   (
    $30, $31, $30, $0D, $06, $09, $60, $86, $48, $01, $65, $03, $04, $02, $01,
    $05, $00, $04, $20
    );
  ASN_SHA384: array[0..18] of byte =
   (
    $30, $41, $30, $0D, $06, $09, $60, $86, $48, $01, $65, $03, $04, $02, $02,
    $05, $00, $04, $30
    );
  ASN_SHA512: array[0..18] of byte =
   (
    $30, $51, $30, $0D, $06, $09, $60, $86, $48, $01, $65, $03, $04, $02, $03,
    $05, $00, $04, $40
    );
  ASN_RMD160: array[0..14] of byte =
    (
    $30, $21, $30, $09, $06, $05, $2B, $24, $03, $02, $01, $05, $00, $04, $14
    );

function TElX509Certificate.WriteSignatureValue(const TBSCertificate: BufferType): BufferType;
var
  M128: TMessageDigest128;
  M160: TMessageDigest160;
  M224: TMessageDigest224;
  M256: TMessageDigest256;
  M384: TMessageDigest384;
  M512: TMessageDigest512;
  HashArr: ByteArray;
  I: integer;
  Size: integer;
  S: BufferType;
  Sign, EncSign, K, R, DS, Tmp: PLInt;
  HashSize: integer;
  RArr, SArr: BufferType;

  Sz: integer;
  //RandCtx: TRC4RandomContext;
  RSize: integer;
  SSize: integer;
  Seq: TStringList;
  HashFunc : TElHashFunction;
  HashAlg, SaltSize : integer;
  HashRes : BufferType;
  N, E, D : ByteArray;

begin
  Result := EmptyBuffer;
  if FCAAvailable then
  begin
    if (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION) or
      (FGenSignatureAlgorithm = SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160)
    then
    begin // temporary
      Sz := FLongRSACAEncryptParams.Modulus.Length * 4;

      { Sz should be not smaller, than HashSize + 11 length, which differs }

      if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION then
      begin
        M128 := HashMD5(TBSCertificate);
        HashSize := 16 + Length(ASN_MD5);

        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz); // encryptParams
        Move(M128, HashArr[Length(HashArr) - 16], 16);
        Move(ASN_MD5[0], HashArr[Length(HashArr) - 16 - Length(ASN_MD5)], Length(ASN_MD5));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION then
      begin
        M128 := HashMD2(TBSCertificate);
        HashSize := 16 + Length(ASN_MD2);

        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz);
        Move(M128, HashArr[Length(HashArr) - 16], 16);
        Move(ASN_MD2[0], HashArr[Length(HashArr) - 16 - Length(ASN_MD2)], Length(ASN_MD2));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION then
      begin
        M160 := HashSHA1(TBSCertificate);
        HashSize := 20 + Length(ASN_SHA1);

        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz);
        Move(M160, HashArr[Length(HashArr) - 20], 20);
        Move(ASN_SHA1[0], HashArr[Length(HashArr) - 20 - Length(ASN_SHA1)], Length(ASN_SHA1));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION then
      begin
        M224 := HashSHA224(TBSCertificate);
        HashSize := 28 + Length(ASN_SHA224);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz);
        Move(M224, HashArr[Length(HashArr) - 28], 28);
        Move(ASN_SHA224[0], HashArr[Length(HashArr) - 28 - Length(ASN_SHA224)], Length(ASN_SHA224));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION then
      begin
        M256 := HashSHA256(TBSCertificate);
        HashSize := 32 + Length(ASN_SHA256);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz);
        Move(M256, HashArr[Length(HashArr) - 32], 32);
        Move(ASN_SHA256[0], HashArr[Length(HashArr) - 32 - Length(ASN_SHA256)], Length(ASN_SHA256));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION then
      begin
        M384 := HashSHA384(TBSCertificate);
        HashSize := 48 + Length(ASN_SHA384);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz);
        Move(M384, HashArr[Length(HashArr) - 48], 48);
        Move(ASN_SHA384[0], HashArr[Length(HashArr) - 48 - Length(ASN_SHA384)], Length(ASN_SHA384));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION then
      begin
        M512 := HashSHA512(TBSCertificate);
        HashSize := 64 + Length(ASN_SHA512);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz);
        Move(M512, HashArr[Length(HashArr) - 64], 64);
        Move(ASN_SHA512[0], HashArr[Length(HashArr) - 64 - Length(ASN_SHA512)], Length(ASN_SHA512));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160 then
      begin
        M160 := HashRMD160(TBSCertificate);
        HashSize := 20 + Length(ASN_RMD160);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);
        SetLength(HashArr, Sz);
        Move(M160, HashArr[Length(HashArr) - 20], 20);
        Move(ASN_RMD160[0], HashArr[Length(HashArr) - 20 - Length(ASN_RMD160)], Length(ASN_RMD160));
      end
      else
      begin
        SetLength(HashArr, 0);
        Result := WriteNULL;
        exit;
      end;

      for I := 2 to Length(HashArr) - HashSize - 2 do
        HashArr[I] := $FF;

      HashArr[0] := 0;
      HashArr[1] := 1;
      HashArr[Length(HashArr) - HashSize - 1] := 0;
      LCreate(Sign);
      LCreate(EncSign);
      PointerToLInt(Sign, @HashArr[0], Length(HashArr));
      LMModPower(Sign, FLongRSACAEncryptParams.PrivateExponent,
        FLongRSACAEncryptParams.Modulus, EncSign);
      LIntToPointer(EncSign, @HashArr[0], Size);
      SetLength(S, Length(HashArr));
      Move(HashArr[0], S[1], Length(HashArr));
      Result := WriteBitString(S);
      LDestroy(Sign);
      LDestroy(EncSign);
    end
    else
      if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS then
    begin
      if Assigned(FParent) then
      begin
        HashAlg := FParent.PSSParams.HashAlgorithm;
        SaltSize := FParent.PSSParams.SaltSize;
      end
      else
      begin
        HashAlg := SB_ALGORITHM_DGST_SHA1;
        SaltSize := 20;
      end;

      if not TElHashFunction.IsAlgorithmSupported(HashAlg) then
      begin
        Result := WriteNULL;
        Exit;
      end;

      HashFunc := TElHashFunction.Create(HashAlg);
      HashFunc.Update(@TBSCertificate[1], Length(TBSCertificate));
      HashRes := HashFunc.Finish;
      HashFunc.Free;

      SetLength(N, FLongRSACAEncryptParams.Modulus.Length * 4);
      LIntToPointer(FLongRSACAEncryptParams.Modulus, @N[0], I);
      SetLength(E, FLongRSACAEncryptParams.PublicKey.Length * 4);
      LIntToPointer(FLongRSACAEncryptParams.PublicKey, @E[0], I);
      SetLength(D, FLongRSACAEncryptParams.PrivateExponent.Length * 4);
      LIntToPointer(FLongRSACAEncryptParams.PrivateExponent, @D[0], I);

      I := 0;
      SBRSA.SignPSS(@HashRes[1], Length(HashRes), HashAlg, SaltSize,
        @N[0], Length(N), @E[0], Length(E), @D[0], Length(D),  nil, I);

      SetLength(Result, I);

      if not SBRSA.SignPSS(@HashRes[1], Length(HashRes), HashAlg, SaltSize,
        @N[0], Length(N), @E[0], Length(E), @D[0], Length(D), @Result[1], I)
      then
      begin
        Result := WriteNULL;
        Exit;
      end
      else
        SetLength(Result, I);
        
      Result := WriteBitString(Result);  
    end
    else
      if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP then
    begin
      Result := WriteNULL;
      Exit;
    end
    else
      if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_ID_DSA_SHA1 then
    begin
      M160 := HashSHA1(TBSCertificate);
      LCreate(K);
      LCreate(R);
      LCreate(DS);
      LCreate(Tmp);
      LCreate(Sign);
      LCreate(EncSign);
      //LRC4Init(RandCtx);
      //LRandom(RandCtx, K, 18);
      SBRndGenerateLInt(K, 18);
      LMModPower(FLongDSACAEncryptParams.G, K, FLongDSACAEncryptParams.P, Tmp);
      LMod(Tmp, FLongDSACAEncryptParams.Q, R);
      LMult(FLongDSACAEncryptParams.X, R, Tmp);
      PointerToLInt(Sign, @M160, 20);
      LAdd(Tmp, Sign, EncSign);
      LGCD(K, FLongDSACAEncryptParams.Q, Tmp, Sign);
      LMult(Sign, EncSign, Tmp);
      LMod(Tmp, FLongDSACAEncryptParams.Q, DS);
      RSize := 20;
      SSize := 20;
      SetLength(SArr, 20);
      SetLength(RArr, 21);
      FillChar(RArr[1], 21, 0);
      FillChar(SArr[1], 20, 0);
      RArr[1] := #0;
      LIntToPointer(R, @RArr[2], RSize);
      LIntToPointer(DS, @SArr[1], SSize);
      LDestroy(EncSign);
      LDestroy(Sign);
      LDestroy(Tmp);
      LDestroy(K);
      LDestroy(R);
      LDestroy(DS);
      Seq := TStringList.Create;
      Seq.Add(WriteInteger(RArr));
      Seq.Add(WriteInteger(SArr));
      Result := WriteBitString(WriteSequence(Seq));
      Seq.Free;
    end;
  end
  else
  begin
    if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    begin // temporary
      Sz := FLongRSAEncryptParams.Modulus.Length * 4;

      if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION then
      begin
        M128 := HashMD5(TBSCertificate);
        HashSize := 16 + Length(ASN_MD5);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz); // encryptParams
        Move(M128, HashArr[Length(HashArr) - 16], 16);
        Move(ASN_MD5[0], HashArr[Length(HashArr) - 16 - Length(ASN_MD5)], Length(ASN_MD5));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION then
      begin
        M128 := HashMD2(TBSCertificate);
        HashSize := 16 + Length(ASN_MD2);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz);
        Move(M128, HashArr[Length(HashArr) - 16], 16);
        Move(ASN_MD2[0], HashArr[Length(HashArr) - 16 - Length(ASN_MD2)], Length(ASN_MD2));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION then
      begin
        M160 := HashSHA1(TBSCertificate);
        HashSize := 20 + Length(ASN_SHA1);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz);
        Move(M160, HashArr[Length(HashArr) - 20], 20);
        Move(ASN_SHA1[0], HashArr[Length(HashArr) - 20 - Length(ASN_SHA1)], Length(ASN_SHA1));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION then
      begin
        M224 := HashSHA224(TBSCertificate);
        HashSize := 28 + Length(ASN_SHA224);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz);
        Move(M224, HashArr[Length(HashArr) - 28], 28);
        Move(ASN_SHA224[0], HashArr[Length(HashArr) - 28 - Length(ASN_SHA224)], Length(ASN_SHA224));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION then
      begin
        M256 := HashSHA256(TBSCertificate);
        HashSize := 32 + Length(ASN_SHA256);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz);
        Move(M256, HashArr[Length(HashArr) - 32], 32);
        Move(ASN_SHA256[0], HashArr[Length(HashArr) - 32 - Length(ASN_SHA256)], Length(ASN_SHA256));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION then
      begin
        M384 := HashSHA384(TBSCertificate);
        HashSize := 48 + Length(ASN_SHA384);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz);
        Move(M384, HashArr[Length(HashArr) - 48], 48);
        Move(ASN_SHA384[0], HashArr[Length(HashArr) - 48 - Length(ASN_SHA384)], Length(ASN_SHA384));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION then
      begin
        M512 := HashSHA512(TBSCertificate);
        HashSize := 64 + Length(ASN_SHA512);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);

        SetLength(HashArr, Sz);
        Move(M512, HashArr[Length(HashArr) - 64], 64);
        Move(ASN_SHA512[0], HashArr[Length(HashArr) - 64 - Length(ASN_SHA512)], Length(ASN_SHA512));
      end
      else
        if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160 then
      begin
        M160 := HashRMD160(TBSCertificate);
        HashSize := 20 + Length(ASN_RMD160);
        if Sz < HashSize + 11 then
          raise EElCertificateException.Create(SKeyLengthTooSmall);
        SetLength(HashArr, Sz);
        Move(M160, HashArr[Length(HashArr) - 20], 20);
        Move(ASN_RMD160[0], HashArr[Length(HashArr) - 20 - Length(ASN_RMD160)], Length(ASN_RMD160));
      end
      else
      begin
        SetLength(HashArr, 0);
        Result := WriteNULL;
        exit;
      end;
      for I := 2 to Length(HashArr) - HashSize - 2 do
        HashArr[I] := $FF;
      HashArr[0] := 0;
      HashArr[1] := 1;
      HashArr[Length(HashArr) - HashSize - 1] := 0;
      LCreate(Sign);
      LCreate(EncSign);
      PointerToLInt(Sign, @HashArr[0], Length(HashArr));
      LMModPower(Sign, FLongRSAEncryptParams.PrivateExponent,
        FLongRSAEncryptParams.Modulus, EncSign);

      LIntToPointer(EncSign, @HashArr[0], Size);
      SetLength(S, Length(HashArr));
      Move(HashArr[0], S[1], Length(HashArr));
      Result := WriteBitString(S);
      LDestroy(Sign);
      LDestroy(EncSign);
    end
    else
    if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS then
    begin
      HashAlg := FPSSParams.HashAlgorithm;
      SaltSize := FPSSParams.SaltSize;

      if not TElHashFunction.IsAlgorithmSupported(HashAlg) then
      begin
        Result := WriteNULL;
        Exit;
      end;

      HashFunc := TElHashFunction.Create(HashAlg);
      HashFunc.Update(@TBSCertificate[1], Length(TBSCertificate));
      HashRes := HashFunc.Finish;
      HashFunc.Free;

      SetLength(N, FLongRSAEncryptParams.Modulus.Length * 4);
      LIntToPointer(FLongRSAEncryptParams.Modulus, @N[0], I);
      SetLength(E, FLongRSAEncryptParams.PublicKey.Length * 4);
      LIntToPointer(FLongRSAEncryptParams.PublicKey, @E[0], I);
      SetLength(D, FLongRSAEncryptParams.PrivateExponent.Length * 4);
      LIntToPointer(FLongRSAEncryptParams.PrivateExponent, @D[0], I);

      I := 0;
      SBRSA.SignPSS(@HashRes[1], Length(HashRes), HashAlg, SaltSize,
        @N[0], Length(N), @E[0], Length(E), @D[0], Length(D),  nil, I);
      SetLength(Result, I);

      if not SBRSA.SignPSS(@HashRes[1], Length(HashRes), HashAlg, SaltSize,
        @N[0], Length(N), @E[0], Length(E), @D[0], Length(D), @Result[1], I)
      then
      begin
        Result := WriteNULL;
        Exit;
      end
      else
        SetLength(Result, I);

      Result := WriteBitString(Result);  
    end
    else
      if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
    begin
      M160 := HashSHA1(TBSCertificate);
      LCreate(K);
      LCreate(R);
      LCreate(DS);
      LCreate(Tmp);
      LCreate(Sign);
      LCreate(EncSign);
      //LRC4Init(RandCtx);
      //LRandom(RandCtx, K, 18);
      SBRndGenerateLInt(K, 18);
      LMModPower(FLongDSAEncryptParams.G, K, FLongDSAEncryptParams.P, Tmp);
      LMod(Tmp, FLongDSAEncryptParams.Q, R);
      LMult(FLongDSAEncryptParams.X, R, Tmp);
      PointerToLInt(Sign, @M160, 20);
      LAdd(Tmp, Sign, EncSign);
      LGCD(K, FLongDSAEncryptParams.Q, Tmp, Sign);
      LMult(Sign, EncSign, Tmp);
      LMod(Tmp, FLongDSAEncryptParams.Q, DS);
      RSize := 20;
      SSize := 20;
      SetLength(RArr, 21);
      SetLength(SArr, 20);
      FillChar(RArr[1], 21, 0);
      FillChar(SArr[1], 20, 0);
      RArr[1] := #0;
      LIntToPointer(R, @RArr[2], RSize);
      LIntToPointer(DS, @SArr[1], SSize);

      LDestroy(EncSign);
      LDestroy(Sign);
      LDestroy(Tmp);
      LDestroy(K);
      LDestroy(R);
      LDestroy(DS);

      Seq := TStringList.Create;
      Seq.Add(WriteInteger(RArr));
      Seq.Add(WriteInteger(SArr));
      Result := WriteBitString(WriteSequence(Seq));
      Seq.Free;
    end;
  end;
end;

function TElX509Certificate.WriteVersion: BufferType;
begin
  Result := WriteExplicit(WriteInteger(2));
end;

function TElX509Certificate.WriteSerialNumber: BufferType;
begin
  if Length(FtbsCertificate.FSerialNumber) > 0 then
    Result := WriteInteger(FtbsCertificate.FSerialNumber)
  else
    Result := WriteInteger(0);
end;

function TElX509Certificate.WriteSignature: BufferType;
var
  A: TStringList;
  Algorithm: BufferType;
begin
  A := TStringList.Create;
  Algorithm := GetOIDBySigAlgorithm(FGenSignatureAlgorithm);

  if FGenSignatureAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS then
  begin
    if FCAAvailable then
    begin
      if Assigned(FParent) then
        A.Add(WritePSSParams(FParent.PSSParams.HashAlgorithm, FParent.PSSParams.SaltSize,
          FParent.PSSParams.MGF, FParent.PSSParams.TrailerField))
      else
        A.Add(WritePSSParams{$ifndef HAS_DEF_PARAMS}(SB_ALGORITHM_DGST_SHA1, 20, SB_CERT_MGF1, 1){$endif});  
    end
    else
      A.Add(WritePSSParams(FPSSParams.HashAlgorithm, FPSSParams.SaltSize,
        FPSSParams.MGF, FPSSParams.TrailerField));
  end;

  Result := WriteAlgorithmIdentifier(Algorithm, A);
  
  A.Free;
end;

function TElX509Certificate.WriteIssuer: BufferType;
var
  Fields: TStringList;
  OutFields: TStringList;
  Tmp: TStringList;
  I: integer;
begin
  Fields := TStringList.Create;
  OutFields := TStringList.Create;
  Tmp := TStringList.Create;

  Fields.Add('');
  Fields.Add('');
  Tmp.Add('');
  if FIssuerRDN.Count = 0 then
  begin
    if FNewIssuer.Country <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(#$55#$04#$06));
      if UseUTF8 then
        Fields[1] := WriteUTF8String(FNewIssuer.Country)
      else
        Fields[1] := WritePrintableString(FNewIssuer.Country);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if FNewIssuer.StateOrProvince <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(#$55#$04#$08));
      if UseUTF8 then
        Fields[1] := WriteUTF8String(FNewIssuer.StateOrProvince)
      else
        Fields[1] := WritePrintableString(FNewIssuer.StateOrProvince);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if FNewIssuer.Locality <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(#$55#$04#$07));
      if UseUTF8 then
        Fields[1] := WriteUTF8String(FNewIssuer.Locality)
      else
        Fields[1] := WritePrintableString(FNewIssuer.Locality);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if FNewIssuer.Organization <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(#$55#$04#$0A));
      if UseUTF8 then
        Fields[1] := WriteUTF8String(FNewIssuer.Organization)
      else
        Fields[1] := WritePrintableString(FNewIssuer.Organization);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if FNewIssuer.OrganizationUnit <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(#$55#$04#$0B));
      if UseUTF8 then
        Fields[1] := WriteUTF8String(FNewIssuer.OrganizationUnit)
      else
        Fields[1] := WritePrintableString(FNewIssuer.OrganizationUnit);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if FNewIssuer.CommonName <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(#$55#$04#$03));
      if UseUTF8 then
        Fields[1] := WriteUTF8String(FNewIssuer.CommonName)
      else
        Fields[1] := WritePrintableString(FNewIssuer.CommonName);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if FNewIssuer.EMailAddress <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(SB_CERT_OID_EMAIL));
      Fields[1] := WriteIA5String(FNewIssuer.EMailAddress);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if OutFields.Count = 0 then
    begin
      Fields.Free;
      OutFields.Free;
      Tmp.Free;
      raise EElCertificateException.Create('Issuer fields are empty');
    end;
  end
  else
  begin
    for I := 0 to FIssuerRDN.Count - 1 do
    begin
      Fields[0] := WriteOID(FIssuerRDN.OIDs[I]);
      if FIssuerRDN.Tags[I] = 0 then
      begin
        if CompareContent(SB_CERT_OID_EMAIL, FIssuerRDN.OIDs[I]) then
          Fields[1] := WriteIA5String(FIssuerRDN.Values[I])
        else if UseUTF8 then
          Fields[1] := WriteUTF8String(FIssuerRDN.Values[I])
        else
          Fields[1] := WritePrintableString(FIssuerRDN.Values[I]);
      end
      else
        Fields[1] := WritePrimitive(FIssuerRDN.Tags[I], FIssuerRDN.Values[I]);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
  end;
  Result := WriteSequence(OutFields);
  Fields.Free;
  OutFields.Free;
  Tmp.Free;
end;

function TElX509Certificate.WriteSubject: BufferType;
var
  Fields: TStringList;
  OutFields: TStringList;
  Tmp: TStringList;
  I: integer;
begin
  Fields := TStringList.Create;
  OutFields := TStringList.Create;
  Tmp := TStringList.Create;

  Fields.Add('');
  Fields.Add('');
  Tmp.Add('');
  if FSubjectRDN.Count = 0 then
  begin
    if FNewSubject.Country <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(#$55#$04#$06));
      if UseUTF8 then
        Fields[1] := WriteUTF8String(FNewSubject.Country)
      else
        Fields[1] := WritePrintableString(FNewSubject.Country);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if FNewSubject.StateOrProvince <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(#$55#$04#$08));
      if UseUTF8 then
        Fields[1] := WriteUTF8String(FNewSubject.StateOrProvince)
      else
        Fields[1] := WritePrintableString(FNewSubject.StateOrProvince);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if FNewSubject.Locality <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(#$55#$04#$07));
      if UseUTF8 then
        Fields[1] := WriteUTF8String(FNewSubject.Locality)
      else
        Fields[1] := WritePrintableString(FNewSubject.Locality);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if FNewSubject.Organization <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(#$55#$04#$0A));
      if UseUTF8 then
        Fields[1] := WriteUTF8String(FNewSubject.Organization)
      else
        Fields[1] := WritePrintableString(FNewSubject.Organization);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if FNewSubject.OrganizationUnit <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(#$55#$04#$0B));
      if UseUTF8 then
        Fields[1] := WriteUTF8String(FNewSubject.OrganizationUnit)
      else
        Fields[1] := WritePrintableString(FNewSubject.OrganizationUnit);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if FNewSubject.CommonName <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(#$55#$04#$03));
      if UseUTF8 then
        Fields[1] := WriteUTF8String(FNewSubject.CommonName)
      else
        Fields[1] := WritePrintableString(FNewSubject.CommonName);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if FNewSubject.EMailAddress <> '' then
    begin
      Fields[0] := WriteOID(TBufferTypeConst(SB_CERT_OID_EMAIL));
      Fields[1] := WriteIA5String(FNewSubject.EMailAddress);
      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
    if OutFields.Count = 0 then
    begin
      Fields.Free;
      OutFields.Free;
      Tmp.Free;
      raise EElCertificateException.Create('Subject fields are empty');
    end;
  end
  else
  begin
    for I := 0 to FSubjectRDN.Count - 1 do
    begin
      Fields[0] := WriteOID(FSubjectRDN.OIDs[I]);
      if FSubjectRDN.Tags[I] = 0 then
      begin
        if CompareContent(FSubjectRDN.OIDs[I], SB_CERT_OID_EMAIL) then
          Fields[1] := WriteIA5String(FSubjectRDN.Values[I])
        else if UseUTF8 then
          Fields[1] := WriteUTF8String(FSubjectRDN.Values[I])
        else
          Fields[1] := WritePrintableString(FSubjectRDN.Values[I]);
      end
      else
        Fields[1] := WritePrimitive(FSubjectRDN.Tags[I], FSubjectRDN.Values[I]);

      Tmp[0] := WriteSequence(Fields);
      OutFields.Add(WriteSet(Tmp));
    end;
  end;
  Result := WriteSequence(OutFields);
  Fields.Free;
  OutFields.Free;
  Tmp.Free;
end;


function TElX509Certificate.WriteValidity: BufferType;
var
  Fields: TStringList;
  Year, Month, Day: Word;
begin
  Fields := TStringList.Create;
  DecodeDate(FTbsCertificate.Validity.NotBefore, Year, Month, Day);
  if Year < 2050 then
    Fields.Add(WriteUTCTime(DateTimeToUTCTime(FTbsCertificate.Validity.NotBefore)))
  else
    Fields.Add(WriteGeneralizedTime(FTbsCertificate.Validity.NotBefore));

  DecodeDate(FTbsCertificate.Validity.NotAfter, Year, Month, Day);
  if Year < 2050 then
    Fields.Add(WriteUTCTime(DateTimeToUTCTime(FTbsCertificate.Validity.NotAfter)))
  else
    Fields.Add(WriteGeneralizedTime(FTbsCertificate.Validity.NotAfter));

  Result := WriteSequence(Fields);
  Fields.Free;
end;

function TElX509Certificate.WriteSubjectPublicKeyInfo: BufferType;
var
  Params: TStringList;
  Fields: TStringList;
  Algorithm: BufferType;
begin
  Params := TStringList.Create;
  Fields := TStringList.Create;
  if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    Algorithm := SB_OID_RSAENCRYPTION
  else
    if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_DH_PUBLIC then
  begin
    Algorithm := SB_OID_DH;
    Params.Add(WriteDHParams);
  end
  else
    if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    Algorithm := SB_OID_DSA;
    Params.Add(WriteDSAParams);
  end
  else
   if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS then
  begin
    Algorithm := SB_OID_RSAPSS;
    Params.Add(WritePSSParams(FPSSParams.HashAlgorithm,
      FPSSParams.SaltSize, FPSSParams.MGF, FPSSParams.TrailerField));
  end
  else
   if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP then
  begin
    Algorithm := SB_OID_RSAOAEP;
    Params.Add(WriteOAEPParams(
      FOAEPParams.HashAlgorithm, FOAEPParams.MGFHashAlgorithm, FOAEPParams.StrLabel));
  end;
  Fields.Add(WriteAlgorithmIdentifier(Algorithm, Params));
  Fields.Add(WriteSubjectPublicKey);
  Result := WriteSequence(Fields);
  Params.Free;
  Fields.Free;
end;

function TElX509Certificate.WriteAlgorithmIdentifier(const Algorithm: BufferType;
  Parameters: TStringList): BufferType;
var
  OutFields: TStringList;
begin
  OutFields := TStringList.Create;
  OutFields.Add('');
  if Parameters.Count = 0 then
    OutFields.Add(WriteNULL)
  else
    OutFields.Add(Parameters[0]);

  OutFields.Strings[0] :=
    TBufferTypeConst(#$06 + WriteSize(Length(Algorithm)) + Algorithm);

  Result := WriteSequence(OutFields);
  OutFields.Free;
end;

function TElX509Certificate.WriteAttributeTypeAndValue(const AttributeType: BufferType; const AttributeValue: BufferType): BufferType;
var
  Fields: TStringList;
begin
  Fields := TStringList.Create;
  Fields.Add(AttributeType);
  Fields.Add(AttributeValue);
  Result := WriteSequence(Fields);
  Fields.Free;
end;

function TElX509Certificate.WriteSubjectPublicKey: BufferType;
var
  P1: ^Byte;
  P1Size: integer;
  S1: BufferType;
  Params: TStringList;
begin
  Params := TStringList.Create;
  if (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or
    (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) or
    (FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) then
  begin
    Result := WriteRSAPublicKey;
  end
  else
    if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_DH_PUBLIC then
  begin
    P1Size := FLongDHEncryptParams.Ys.Length shl 2;
    GetMem(P1, P1Size);
    try
      LIntToPointer(FLongDHEncryptParams.Ys, P1, P1Size);
      SetLength(S1, P1Size);
      Move(P1^, S1[1], P1Size);
      Params.Add(WriteInteger(S1));
      //Result := WriteSequence(Params);
      //Result := WriteBITString(Result);
      Result := WriteBITString(Params[0]);
    finally
      FreeMem(P1);
    end;
  end
  else
    if FGenEncryptAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    Result := WriteDSAPublicKey;
  end;
  Params.Free;
end;


function TElX509Certificate.WriteORAddress(const Value: BufferType): BufferType;
var
  Lst: TStringList;
begin
  Lst := TStringList.Create;
  Lst.Add(WriteBuiltInStandardAttributes(Value));
  Result := WriteSequence(Lst);
  Lst.Free;
end;

function TElX509Certificate.WritePolicyInformation(P: TElSinglePolicyInformation): BufferType;
var
  Lst, TmpLst, InfoLst, UserNoticeLst, NoticeRefLst, NoticeNumLst: TStringList;
  I: integer;
begin
  Lst := TStringList.Create;
  TmpLst := TStringList.Create;
  InfoLst := TStringList.Create;
  Lst.Add(WriteOID(P.PolicyIdentifier));
  // adding policy qualifiers
  {1}
  if P.CPSURI <> '' then
  begin
    InfoLst.Add(WriteOID(TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$02#$01)));
    InfoLst.Add(WriteStringPrimitive($16, P.CPSURI));
  end;
  TmpLst.Add(WriteSequence(InfoLst));
  {2}
  InfoLst.Clear;
  { checkup usernotice to be turned on }
  // if UserNotice is On then
  UserNoticeLst := TStringList.Create;
  NoticeRefLst := TStringList.Create;
  NoticeNumLst := TStringList.Create;
  { Info-ID }
  InfoLst.Add(WriteOID(TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$02#$02)));
    { Organization }
  NoticeRefLst.Add(WriteStringPrimitive($16, P.UserNotice.Organization));
  for I := 0 to P.UserNotice.NoticeNumbersCount - 1 do
    NoticeNumLst.Add(WriteInteger(P.UserNotice.NoticeNumbers[I]));
    { Notice numbers }
  NoticeRefLst.Add(WriteSequence(NoticeNumLst));
  { Notice Ref }
  UserNoticeLst.Add(WriteSequence(NoticeRefLst));
  { Explicit Text }
  UserNoticeLst.Add(WriteVisibleString(P.UserNotice.ExplicitText));
  { Info-Qualifier }
  InfoLst.Add(WriteSequence(UserNoticeLst));
  FreeAndNil(UserNoticeLst);
  FreeAndNil(NoticeRefLst);
  FreeAndNil(NoticeNumLst);
  TmpLst.Add(WriteSequence(InfoLst));

  Lst.Add(WriteSequence(TmpLst));
  Result := WriteSequence(Lst);
  
  Lst.Free;
  TmpLst.Free;
  InfoLst.Free;
end;

function TElX509Certificate.WriteBuiltInStandardAttributes(const Value: string): BufferType;
var
  Lst: TStringList;
  Tmp: BufferType;
begin
  Lst := TStringList.Create;
  Tmp := WritePrintableString(Value);
  Lst.Add(WritePrimitive($61, Tmp));
  Result := WriteSequence(Lst);
  Lst.Free;
end;


function TElX509Certificate.WriteDistributionPoint(P: TElDistributionPoint): BufferType;
var
  Lst: TStringList;
  Tmp: BufferType;
  B1: byte;
  Tag: TElASN1ConstrainedTag;
  Size: integer;
begin
  Lst := TStringList.Create;

  if P.Name.Count > 0 then
  begin
    Tag := TElASN1ConstrainedTag.Create;
    try
      P.Name.SaveToTag(Tag);
      Size := 0;
      Tag.SaveToBuffer(nil, Size);
      SetLength(Tmp, Size);
      Tag.SaveToBuffer(@Tmp[1], Size);
      SetLength(Tmp, Size);
      Tmp[1] := #$A0;
    finally
      FreeAndNil(Tag);
    end;
    Lst.Add(WritePrimitive($A0, Tmp));
  end;

  B1 := 0;
  if rfAACompromise in P.ReasonFlags then
    B1 := B1 or $400;
  if rfPrivilegeWithdrawn in P.ReasonFlags then
    B1 := B1 or $200;
  if rfRemoveFromCRL in P.ReasonFlags then
    B1 := B1 or $100;
  if rfObsolete1 in P.ReasonFlags then
    B1 := B1 or $80;
  if rfUnspecified in P.ReasonFlags then
    B1 := B1 or $1;
  if rfKeyCompromise in P.ReasonFlags then
    B1 := B1 or $40;
  if rfCACompromise in P.ReasonFlags then
    B1 := B1 or $20;
  if rfAffiliationChanged in P.ReasonFlags then
    B1 := B1 or $10;
  if rfSuperseded in P.ReasonFlags then
    B1 := B1 or $08;
  if rfCessationOfOperation in P.ReasonFlags then
    B1 := B1 or $04;
  if rfCertificateHold in P.ReasonFlags then
    B1 := B1 or $02;
  Tmp := WriteBitString(Chr(B1));
  //Lst.Add(WritePrimitive($A1, Tmp));
  Tmp[1] := Chr($81);
  Lst.Add(Tmp); //!!

  if (P.CRLIssuer.Count > 0) then
  begin
    //Tmp := WriteGeneralNamesSeq(P.CRLIssuer);
    Tag := TElASN1ConstrainedTag.Create;
    try
      P.CRLIssuer.SaveToTag(Tag);
      Size := 0;
      Tag.SaveToBuffer(nil, Size);
      SetLength(Tmp, Size);
      Tag.SaveToBuffer(@Tmp[1], Size);
      SetLength(Tmp, Size);
      Tmp[1] := #$A2;
    finally
      FreeAndNil(Tag);
    end;
    Lst.Add(Tmp);
  end;

  Result := WriteSequence(Lst);
  Lst.Free;
end;

function TElX509Certificate.WriteDHParams: BufferType;
var
  Params: TStringList;
  PDHP, PDHG: ^Byte;
  S1, S2: BufferType;
  PSize, GSize: integer;
begin
  Params := TStringList.Create;
  PSize := FLongDHEncryptParams.P.Length shl 2;
  GSize := FLongDHEncryptParams.G.Length shl 2;
  PSize := PSize;
  GSize := GSize;
  GetMem(PDHP, PSize);
  GetMem(PDHG, GSize);
  try
    LIntToPointer(FLongDHEncryptParams.P, PDHP, PSize);
    LIntToPointer(FLongDHEncryptParams.G, PDHG, GSize);
    SetLength(S1, PSize);
    SetLength(S2, GSize);
    Move(PByteArray(PDHP)[0], S1[1], PSize);
    Move(PByteArray(PDHG)[0], S2[1], GSize);
    Params.Add(WriteInteger(S1));
    Params.Add(WriteInteger(S2));
    Params.Add(WriteInteger(Chr(2)));
    Result := WriteSequence(Params);
  finally
    FreeMem(PDHP);
    FreeMem(PDHG);
    Params.Free;
  end;
end;

function TElX509Certificate.WriteRSAPublicKey: BufferType;
var
  Params: TStringList;
  P1, P2: ^Byte;
  S1, S2: BufferType;
  P1Size, P2Size: integer;
begin
  Params := TStringList.Create;
  P1Size := FLongRSAEncryptParams.Modulus.Length shl 2;
  P2Size := FLongRSAEncryptParams.PublicKey.Length shl 2;
  GetMem(P1, P1Size);
  GetMem(P2, P2Size);
  try
    LIntToPointer(FLongRSAEncryptParams.Modulus, P1, P1Size); // encrypt
    LIntToPointer(FLongRSAEncryptParams.PublicKey, P2, P2Size);
    SetLength(S1, P1Size);
    SetLength(S2, P2Size);
    Move(P1^, S1[1], P1Size);
    Move(P2^, S2[1], P2Size);
    Params.Add(WriteInteger(S1));
    Params.Add(WriteInteger(S2));
    Result := WriteSequence(Params);
    Result := WriteBITString(Result);
  finally
    FreeMem(P1);
    FreeMem(P2);
    Params.Free;
  end;
end;

function TElX509Certificate.WriteDSAParams: BufferType;
var
  PDSAP, PDSAG, PDSAQ: ^Byte;
  Params: TStringList;
  S1, S2, S3: string;
  PSize, GSize, QSize: integer;
begin
  PSize := FLongDSAEncryptParams.P.Length shl 2;
  GSize := FLongDSAEncryptParams.G.Length shl 2;
  QSize := FLongDSAEncryptParams.Q.Length shl 2;
  Params := TStringList.Create;
  GetMem(PDSAP, PSize);
  GetMem(PDSAG, GSize);
  GetMem(PDSAQ, QSize);
  try
    LIntToPointer(FLongDSAEncryptParams.P, PDSAP, PSize);
    LIntToPointer(FLongDSAEncryptParams.G, PDSAG, GSize);
    LIntToPointer(FLongDSAEncryptParams.Q, PDSAQ, QSize);
    SetLength(S1, PSize);
    SetLength(S2, GSize);
    SetLength(S3, QSize);
    Move(PByteArray(PDSAP)[0], S1[1], PSize);
    Move(PByteArray(PDSAG)[0], S2[1], GSize);
    Move(PByteArray(PDSAQ)[0], S3[1], QSize);
    Params.Add(WriteInteger(S1));
    Params.Add(WriteInteger(S3));
    Params.Add(WriteInteger(S2));
    Result := WriteSequence(Params);
  finally
    FreeMem(PDSAP);
    FreeMem(PDSAG);
    FreeMem(PDSAQ);
    Params.Free;
  end;
end;

function TElX509Certificate.WriteDSAPublicKey: BufferType;
var
  P: ^Byte;
  S: string;
  PSize: integer;
begin
  PSize := FLongDSAEncryptParams.Y.Length shl 2;
  GetMem(P, PSize);
  try
    LIntToPointer(FLongDSAEncryptParams.Y, P, PSize);
    SetLength(S, PSize);
    Move(P^, S[1], PSize);
    Result := WriteInteger(S);
    Result := WriteBITString(Result);
  finally
    FreeMem(P);
  end;
end;

function TElX509Certificate.WriteName(Name: TElRelativeDistinguishedName): BufferType;
var
  OutFields: TStringList;
  Fields, Tmp: TStringList;
  I: integer;
begin
  OutFields := TStringList.Create;
  Tmp := TStringList.Create;
  Fields := TStringList.Create;
  Fields.Add('');
  Fields.Add('');
  Tmp.Add('');

  for I := 0 to Name.Count - 1 do
  begin
    Fields[0] := WriteOID(Name.OIDs[I]);
    Fields[1] := WritePrintableString(Name.Values[I]);
    Tmp[0] := WriteSequence(Fields);
    OutFields.Add(WriteSet(Tmp));
  end;

  Result := WriteSequence(OutFields);
  Fields.Free;
  OutFields.Free;
  Tmp.Free;
end;

procedure TElX509Certificate.CAGenerate(Algorithm: integer; DWordsInEncryptKey
  : integer);
var
  OldVal: boolean;
begin
  OldVal := FCAAvailable;
  FCAAvailable := false;
  Generate(Algorithm, DWordsInEncryptKey);
  FCAAvailable := OldVal;
end;

function Compare(OID1, OID2: TOIDEx): boolean;
begin
  if (OID1[0] = OID2[0]) and (OID1[1] = OID2[1]) and (OID1[2] = OID2[2]) then
    Result := true
  else
    Result := false;
end;



procedure TElX509Certificate.LoadKeyFromBuffer(Buffer: Pointer; Size: Word);
var
  M, E, G, Y: ByteArray;
  MSize, ESize, DSize, YSize, XSize: integer;
begin
  if Size = 0 then
  begin
    FPrivateKeyExists := false;
    SetLength(FPrivateKey, 0);
    SetLength(FPrivateKeyBlob, 0);
    exit;
  end;

  { Trying to load key in PKCS format. If failed, loading it as a simple byte string. }
  MSize := 0;
  ESize := 0;
  DSize := 0;
  { Checking whether we got RSA key }
  SBRSA.DecodePrivateKey(Buffer, Size, nil, MSize, nil, ESize, nil, DSize);
  if (MSize = 0) and (ESize = 0) and (DSize = 0) then
  begin
    { Maybe, we got DSA one }
    YSize := 0;
    XSize := 0;
    SBDSA.DecodePrivateKey(Buffer, Size, nil, MSize, nil, ESize, nil, DSize, nil, YSize,
      nil, XSize);
    if (MSize = 0) and (ESize = 0) and (DSize = 0) and (YSize = 0) and (XSize = 0) then
    begin
      { Neither RSA nor DSA, saving in plain }
      if PublicKeyAlgorithm = SB_CERT_ALGORITHM_DH_PUBLIC then
      begin
        SetLength(FPrivateKey, Size);
        Move(Buffer^, FPrivateKey[0], Size);
        SetLength(FPrivateKeyBlob, Size);
        Move(Buffer^, FPrivateKeyBlob[0], Size);
        FPrivateKeyExists := true;
      end;
    end
    else
    begin
      { Really DSA key }
      SetLength(M, MSize);
      SetLength(E, ESize);
      SetLength(G, DSize);
      SetLength(Y, YSize);
      SetLength(FPrivateKey, XSize);
      if not SBDSA.DecodePrivateKey(Buffer, Size, @M[0], MSize, @E[0], ESize,
        @G[0], DSize, @Y[0], YSize, @FPrivateKey[0], XSize) then
      begin
        SetLength(FPrivateKey, 0);
        SetLength(FPrivateKeyBlob, 0);
      end
      else
      begin
        SetLength(FPrivateKeyBlob, Size);
        Move(Buffer^, FPrivateKeyBlob[0], Size);
      end;
      FPrivateKeyExists := true;
    end;
  end
  else
  begin
    SetLength(M, MSize);
    SetLength(E, ESize);
    SetLength(FPrivateKey, DSize);
    if not SBRSA.DecodePrivateKey(Buffer, Size, @M[0], MSize, @E[0], ESize,
      @FPrivateKey[0], DSize) then
    begin
      SetLength(FPrivateKey, 0);
      SetLength(FPrivateKeyBlob, 0);
    end
    else
    begin
      SetLength(FPrivateKeyBlob, Size);
      Move(Buffer^, FPrivateKeyBlob[0], Size);
    end;
    FPrivateKeyExists := true;
  end;
end;

procedure TElX509Certificate.LoadKeyFromStream(Stream: TStream; Count: integer = 0);
var
  P: ^Byte;
begin
  if Count = 0 then
  begin
    Count := Stream.Size - Stream.Position;
  end
  else
    Count := Min(Count, integer(Stream.Size - Stream.Position));
  GetMem(P, Count);
  try
    Stream.ReadBuffer(P^, Count);
    LoadKeyFromBuffer(P, Count);
  finally
    FreeMem(P);
  end;
end;

procedure TElX509Certificate.AddFieldByOID(var Name: TName; const OID: BufferType;
  Tag : byte; const Content: BufferType);
var
  RealContent : string;
  WS : WideString;
  Len : integer;
begin
  RealContent := CloneBuffer(Content);
  if Tag = SB_ASN1_BMPSTRING then
  begin
    // decoding Unicode string
    Len := Length(RealContent);
    SwapBigEndianWords(@RealContent[1], Len);
    SetLength(WS, Len div 2);
    Move(PWideChar(@RealContent[1])^, WS[1], Len);
    ConvertUTF16ToUTF8(WS, RealContent, strictConversion, true);
  end
  else
  begin
  end;

  if CompareContent(OID, TBufferTypeConst(#$55#$04#$03)) then
    Name.CommonName := RealContent
  else if CompareContent(OID, TBufferTypeConst(#$55#$04#$06)) then
    Name.Country := RealContent
  else if CompareContent(OID, TBufferTypeConst(#$55#$04#$07)) then
    Name.Locality := RealContent
  else if CompareContent(OID, TBufferTypeConst(#$55#$04#$08)) then
    Name.StateOrProvince := RealContent
  else if CompareContent(OID, TBufferTypeConst(#$55#$04#$0A)) then
    Name.Organization := RealContent
  else if CompareContent(OID, TBufferTypeConst(#$55#$04#$0B)) then
    Name.OrganizationUnit := RealContent
  else if CompareContent(OID, SB_CERT_OID_EMAIL) then
    Name.EMailAddress := RealContent;
end;



function TElX509Certificate.LoadFromBufferPEM(Buffer: pointer; Size: Word; const PassPhrase: string): integer;
var
  S, PEMEntity, Hd: string;
  IndStart, IndEnd: integer;
  Buf: ByteArray;
  Sz, Err: integer;
begin
  SetLength(S, Size);
  Move(Buffer^, S[1], Size);
  IndStart := Pos(PEM_CERTIFICATE_BEGIN_LINE, S);
  IndEnd := Pos(PEM_CERTIFICATE_END_LINE, S);
  if (IndStart <= 0) or (IndEnd <= 0) then
  begin
    Result := PEM_DECODE_RESULT_INVALID_FORMAT;
    Exit;
  end;
  PEMEntity := Copy(S, IndStart, IndEnd - IndStart + 1 + Length(PEM_CERTIFICATE_END_LINE));
  SetLength(Buf, Length(PEMEntity));
  Sz := Length(PEMEntity);
  Err := SBPEM.Decode(@PEMEntity[1], Length(PEMEntity), @Buf[0], PassPhrase, Sz, Hd);
  Result := Err;
  if Err = PEM_DECODE_RESULT_OK then
  begin
    LoadFromBuffer(@Buf[0], Sz);
  end
  else
    Exit;
    
  IndStart := Pos(PEM_RSA_PRIVATE_KEY_BEGIN_LINE, S);
  IndEnd := Pos(PEM_RSA_PRIVATE_KEY_END_LINE, S);
  if (IndStart > 0) and (IndEnd > 0) then
  begin
    PEMEntity := Copy(S, IndStart, IndEnd + Length(PEM_RSA_PRIVATE_KEY_END_LINE));
    LoadKeyFromBufferPEM(@PEMEntity[1], Length(PEMEntity), PassPhrase);
    Exit;
  end;

  IndStart := Pos(PEM_DSA_PRIVATE_KEY_BEGIN_LINE, S);
  IndEnd := Pos(PEM_DSA_PRIVATE_KEY_END_LINE, S);
  if (IndStart > 0) and (IndEnd > 0) then
  begin
    PEMEntity := Copy(S, IndStart, IndEnd + Length(PEM_DSA_PRIVATE_KEY_END_LINE));
    LoadKeyFromBufferPEM(@PEMEntity[1], Length(PEMEntity), PassPhrase);
    Exit;
  end;

  IndStart := Pos(PEM_DH_PRIVATE_KEY_BEGIN_LINE, S);
  IndEnd := Pos(PEM_DH_PRIVATE_KEY_END_LINE, S);
  if (IndStart > 0) and (IndEnd > 0) then
  begin
    PEMEntity := Copy(S, IndStart, IndEnd + Length(PEM_DH_PRIVATE_KEY_END_LINE));
    LoadKeyFromBufferPEM(@PEMEntity[1], Length(PEMEntity), PassPhrase);
    Exit;
  end;
end;

function TElX509Certificate.LoadFromStreamPEM(Stream: TStream; const PassPhrase: string;
  Count: integer = 0): integer;
var
  Buf: ByteArray;
begin
  if Count = 0 then
    Count := Stream.Size - Stream.Position
  else
    Count := Min(Count, integer(Stream.Size - Stream.Position));
  SetLength(Buf, Count);
  Stream.Read(Buf[0], Count);
  Result := LoadFromBufferPEM(@Buf[0], Length(Buf), PassPhrase);
end;



function TElX509Certificate.LoadKeyFromBufferPEM(Buffer: pointer; Size: Word;
  const PassPhrase: string): integer;
var
  Buf: ByteArray;
  Sz, Err: integer;
  Header: string;
begin
  if Size = 0 then
  begin
    result := PEM_DECODE_RESULT_NOT_ENOUGH_SPACE;
    FPrivateKeyExists := false;
    SetLength(FPrivateKey, 0);
    SetLength(FPrivateKeyBlob, 0);
    exit;
  end;

  Sz := Size;
  SetLength(Buf, Size);
  Err := SBPEM.Decode(Buffer, Size, @Buf[0], PassPhrase, Sz, Header);
  Result := Err;
  if Err = PEM_DECODE_RESULT_OK then
  begin
    LoadKeyFromBuffer(@Buf[0], Sz);
  end
  else
  begin
    FPrivateKeyExists := false;
    SetLength(FPrivateKey, 0);
    SetLength(FPrivateKeyBlob, 0);
  end;
end;

function TElX509Certificate.LoadKeyFromStreamPEM(Stream: TStream; const PassPhrase: string;
  Count: integer = 0): integer;
var
  Buf: array of byte;
begin
  if Count = 0 then
    Count := Stream.Size - Stream.Position
  else
    Count := Min(Count, integer(Stream.Size - Stream.Position));
  SetLength(Buf, Count);
  Stream.Read(Buf[0], Count);
  Result := LoadKeyFromBufferPEM(@Buf[0], Length(Buf), PassPhrase);
end;



function TElX509Certificate.SaveToBufferPEM(Buffer: Pointer; var Size: Word; const PassPhrase: string): boolean;
var
  Buf: ByteArray;
  Sz: word;

  Enc: boolean;
  OutSz: integer;
begin
  SetLength(Buf, FAllSize);
  Sz := FAllSize;
  SaveToBuffer(@Buf[0], Sz);
  Enc := PassPhrase <> '';
  OutSz := Size;
  Result := SBPEM.Encode(@Buf[0], Sz, Buffer, OutSz, 'CERTIFICATE', Enc, PassPhrase);
  Size := OutSz;
end;


procedure TElX509Certificate.SaveToStreamPEM(Stream: TStream; const PassPhrase: string);
var
  Buf: ByteArray;
  Sz: word;
begin
  SetLength(Buf, FAllSize * 4);
  Sz := Length(Buf);
  SaveToBufferPEM(@Buf[0], Sz, PassPhrase);
  Stream.Write(Buf[0], Sz);
end;



function TElX509Certificate.SaveKeyToBufferPEM(Buffer: Pointer; var Size: Word; const PassPhrase: string): boolean;
var
  Buf: ByteArray;
  Sz: word;
  Enc: boolean;
  OutSz: integer;
  Name: string;
begin
  Sz := 0;
  SaveKeyToBuffer(nil, Sz);
  SetLength(Buf, Sz);
  SaveKeyToBuffer(@Buf[0], Sz);
  if Sz = 0 then
  begin
    Size := 0;
    Result := false;
    Exit;
  end;
  Enc := PassPhrase <> '';
  OutSz := Size;
  if (FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or
    (FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) or
    (FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) then
    Name := 'RSA PRIVATE KEY'
  else
    if FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
    Name := 'DSA PRIVATE KEY'
  else
    if FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm = SB_CERT_ALGORITHM_DH_PUBLIC then
    Name := 'DH PRIVATE KEY'
  else
    Name := 'PRIVATE KEY';
  Result := SBPEM.Encode(@Buf[0], Sz, Buffer, OutSz, Name, Enc, PassPhrase);
  Size := OutSz;
end;


procedure TElX509Certificate.SaveKeyToStreamPEM(Stream: TStream; const PassPhrase: string);
var
  Buf: ByteArray;
  Sz: word;

begin
  Sz := 0;
  SaveKeyToBufferPEM(nil, Sz, PassPhrase);
  SetLength(Buf, Sz);
//  Sz := Length(FPrivateKey) * 4 + 48;
  SaveKeyToBufferPEM(@Buf[0], Sz, PassPhrase);
  Stream.Write(Buf[0], Sz);
end;

procedure TElX509Certificate.ClearData;
begin
  FIssuerName.Country := '';
  FIssuerName.StateOrProvince := '';
  FIssuerName.Locality := '';
  FIssuerName.Organization := '';
  FIssuerName.OrganizationUnit := '';
  FIssuerName.CommonName := '';
  FIssuerName.EMailAddress := '';
  FSubjectName.Country := '';
  FSubjectName.StateOrProvince := '';
  FSubjectName.Locality := '';
  FSubjectName.Organization := '';
  FSubjectName.OrganizationUnit := '';
  FSubjectName.CommonName := '';
  FSubjectName.EMailAddress := '';
  FAllSize := 0;
  SetLength(FPrivateKey, 0);
  SetLength(FLastOID, 0);
  SetLength(FLastExtensionOID, 0);
  FIssuerRDN.Count := 0;
  FSubjectRDN.Count := 0;
  FGeneratingFromRequest := false;
end;

function TElX509Certificate.Sign(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
  var OutSize: integer; Detached: boolean = false; IncludeCertificates: boolean = true): boolean;
var
  Signer: TElMessageSigner;
  CertStorage: TElMemoryCertStorage;
  Buf: ByteArray;
  Sz: word;
begin
  if (BelongsTo = BT_WINDOWS) and (FPrivateKeyExists) then
  begin
    Sz := 0;
    SaveKeyToBuffer(nil, Sz);
    if Sz > 0 then
    begin
      SetLength(Buf, Sz);
      SaveKeyToBuffer(@Buf[0], Sz);
    end
    else
    begin
{$IFDEF SB_HAS_WINCRYPT}
      Result := SignWin32(InBuffer, InSize, OutBuffer, OutSize, nil, nil, Detached, nil, IncludeCertificates);
{$ELSE}
      Result := false;
{$ENDIF}
      Exit;
    end;
  end;

  if (Length(FPrivateKey) = 0) and (Length(FPrivateKeyBlob) = 0) and
    not ((FPrivateKeyExists) and (BelongsTo = BT_PKCS11)) then
  begin
    Result := false;
    Exit;
  end;
  
  CertStorage := TElMemoryCertStorage.Create(nil);
  Signer := TElMessageSigner.Create(nil);
  try
    CertStorage.Add(Self{$ifndef HAS_DEF_PARAMS}, true{$endif});
    Signer.CertStorage := CertStorage;
    Signer.HashAlgorithm := SB_ALGORITHM_DGST_SHA1;
    Signer.IncludeCertificates := IncludeCertificates;
    Result := Signer.Sign(InBuffer, InSize, OutBuffer, OutSize, Detached) = 0;
  finally
    FreeAndNil(Signer);
    FreeAndNil(CertStorage);
  end;
end;



function TElX509Certificate.Verify(InBuffer: pointer; InSize: integer; Signature: pointer;
  SignatureSize: integer): boolean;
var
  Verifier: TElMessageVerifier;
  CertStorage: TElMemoryCertStorage;
  Buf: ByteArray;
  I: integer;
  Detached: boolean;
begin
  CertStorage := TElMemoryCertStorage.Create(nil);
  Verifier := TElMessageVerifier.Create(nil);
  try
    Verifier.CertStorage := CertStorage;
    CertStorage.Add(Self{$ifndef HAS_DEF_PARAMS}, true{$endif});
    I := 0;
    Detached := TElMessageVerifier.IsSignatureDetached(Signature, SignatureSize);
    if Detached then
      Result := Verifier.VerifyDetached(InBuffer, InSize, Signature, SignatureSize) = 0
    else
    begin
      Verifier.Verify(Signature, SignatureSize, nil, I);
      SetLength(Buf, I);
      Result := Verifier.Verify(Signature, SignatureSize, @Buf[0], I) = 0;
      Result := Result and (InSize = I) and (CompareMem(InBuffer, @Buf[0], I));
    end;
  finally
    FreeAndNil(CertStorage);
    FreeAndNil(Verifier);
  end;
end;




function TElX509Certificate.LoadFromBufferPFX(Buffer: pointer; Size: integer;
  const Password: string): integer;
var
  Msg: TElPKCS12Message;
  Buf: ByteArray;
  Sz: word;
  Cert: TElX509Certificate;
begin
  Msg := TElPKCS12Message.Create;
  Msg.Password := Password;
  try
    Result := Msg.LoadFromBuffer(Buffer, Size);
    if (Result = 0) then
    begin
      if Msg.Certificates.Count > 0 then
      begin
        Cert := Msg.Certificates.Certificates[0];
        LoadFromBuffer(Cert.CertificateBinary, Cert.CertificateSize);
        Sz := 0;
        Cert.SaveKeyToBuffer(nil, Sz);
        if Sz > 0 then
        begin
          SetLength(Buf, Sz);
          Cert.SaveKeyToBuffer(@Buf[0], Sz);
          LoadKeyFromBuffer(@Buf[0], Sz);
        end;
      end
      else
        raise EElCertificateException.Create(SNoCertificateFound);
    end;
  finally
    FreeAndNil(Msg);
  end;
end;

function TElX509Certificate.LoadKeyFromStreamPVK(Stream: TStream; const Password: string; Count: integer = 0): integer;
var
  Buf: ByteArray;
begin
  if Count = 0 then
    Count := Stream.Size - Stream.Position
  else
    Count := Min(Count, integer(Stream.Size - Stream.Position));
  SetLength(Buf, Count);
  Stream.Read(Buf[0], Count);
  Result := LoadKeyFromBufferPVK(@Buf[0], Length(Buf), Password);
end;

function TElX509Certificate.LoadFromStreamPFX(Stream: TStream; const Password:
  string; Count: integer = 0): integer;
var
  Buf: ByteArray;
begin
  if Count = 0 then
    Count := Stream.Size - Stream.Position
  else
    Count := Min(Count, integer(Stream.Size - Stream.Position));
  SetLength(Buf, Count);
  Stream.Read(Buf[0], Count);
  Result := LoadFromBufferPFX(@Buf[0], Length(Buf), Password);
end;



function TElX509Certificate.SaveToBufferPFX(Buffer: pointer; var Size: integer;
  const Password: string; KeyEncryptionAlgorithm: integer; CertEncryptionAlgorithm:
  integer): integer;
var
  Msg: TElPKCS12Message;
begin
  Msg := TElPKCS12Message.Create;
  Msg.Iterations := 2048;
  Msg.KeyEncryptionAlgorithm := KeyEncryptionAlgorithm;
  Msg.CertEncryptionAlgorithm := CertEncryptionAlgorithm;
  Msg.Password := Password;
  try
    Msg.Certificates.Add(Self{$ifndef HAS_DEF_PARAMS}, true{$endif});
    Result := Msg.SaveToBuffer(Buffer, Size);
  finally
    FreeAndNil(Msg);
  end;
end;


function TElX509Certificate.SaveToStreamPFX(Stream: TStream; const Password: string;
  KeyEncryptionAlgorithm: integer; CertEncryptionAlgorithm: integer): integer;
var
  Buf: ByteArray;
  Sz: integer;
begin
  Sz := 0;
  SaveToBufferPFX(nil, Sz, Password, KeyEncryptionAlgorithm, CertEncryptionAlgorithm);
  SetLength(Buf, Sz);
  Result := SaveToBufferPFX(@Buf[0], Sz, Password, KeyEncryptionAlgorithm,
    CertEncryptionAlgorithm);
  if Result = 0 then
    Stream.Write(Buf[0], Sz);
end;


function TElX509Certificate.SaveKeyValueToBuffer(Buffer: pointer; var Size:
  integer): boolean;
var
  ASize, BSize, CSize, DSize, ESize: integer;
  A, B, C, D, E: ByteArray;
  W: word;
begin
  if Length(FPrivateKeyBlob) > 0 then
  begin
    if (FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm =
      SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or
      (FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm =
      SB_CERT_ALGORITHM_ID_RSAPSS) or
      (FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm =
      SB_CERT_ALGORITHM_ID_RSAOAEP) then
    begin
      ASize := 0;
      SBRSA.DecodePrivateKey(@FPrivateKeyBlob[0], Length(FPrivateKeyBlob), nil,
        ASize, nil, BSize, nil, CSize);
      SetLength(A, ASize);
      SetLength(B, BSize);
      SetLength(C, CSize);
      if SBRSA.DecodePrivateKey(@FPrivateKeyBlob[0], Length(FPrivateKeyBlob),
        @A[0], ASize, @B[0], BSize, @C[0], CSize) then
      begin
        if Size < CSize then
        begin
          Size := CSize;
          Result := false;
        end
        else
        begin
          Size := CSize;
          Move(C[0], Buffer^, Size);
          Result := true;
        end;
      end
      else
      begin
        if Size < Length(FPrivateKeyBlob) then
        begin
          Size := Length(FPrivateKeyBlob);
          Result := false;
        end
        else
        begin
          Size := Length(FPrivateKeyBlob);
          Move(FPrivateKeyBlob[0], Buffer^, Size);
          Result := true;
        end;
      end;
    end
    else
      if FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm =
      SB_CERT_ALGORITHM_ID_DSA then
    begin
      ASize := 0;
      SBDSA.DecodePrivateKey(@FPrivateKeyBlob[0], Length(FPrivateKeyBlob), nil,
        ASize, nil, BSize, nil, CSize, nil, DSize, nil, ESize);
      SetLength(A, ASize);
      SetLength(B, BSize);
      SetLength(C, CSize);
      SetLength(D, DSize);
      SetLength(E, ESize);
      if SBDSA.DecodePrivateKey(@FPrivateKeyBlob[0], Length(FPrivateKeyBlob),
        @A[0], ASize, @B[0], BSize, @C[0], CSize, @D[0], DSize, @E[0], ESize) then
      begin
        if Size < ESize then
        begin
          Size := ESize;
          Result := false;
        end
        else
        begin
          Size := ESize;
          Move(E[0], Buffer^, Size);
          Result := true;
        end;
      end
      else
      begin
        if Size < Length(FPrivateKeyBlob) then
        begin
          Size := Length(FPrivateKeyBlob);
          Result := false;
        end
        else
        begin
          Size := Length(FPrivateKeyBlob);
          Move(FPrivateKeyBlob[0], Buffer^, Size);
          Result := true;
        end;
      end;
    end
    else
    begin
      W := 0;
      SaveKeyToBuffer(nil, W);
      if Size < W then
      begin
        Result := false;
        Size := W;
      end
      else
      begin
        SaveKeyToBuffer(Buffer, W);
        Result := true;
        Size := W;
      end;
    end;
  end
  else
  begin
    W := 0;
    SaveKeyToBuffer(nil, W);
    if Size < W then
    begin
      Result := false;
      Size := W;
    end
    else
    begin
      SaveKeyToBuffer(Buffer, W);
      Result := true;
      Size := W;
    end;
  end;
end;

function TElX509Certificate.GetHashMD5: TMessageDigest128;
begin
  Result := HashMD5(FPData, FAllSize);
end;

function TElX509Certificate.GetHashSHA1: TMessageDigest160;
begin
  Result := HashSHA1(FPData, FAllSize);
end;

function TElX509Certificate.GetValidFrom: TDateTime;
var
  Validity: TValidity;
begin
  Validity := FtbsCertificate.Validity;
  result := Validity.NotBefore;
end;

function TElX509Certificate.GetValidTo: TDateTime;
var
  Validity: TValidity;
begin
  Validity := FtbsCertificate.Validity;
  result := Validity.NotAfter;
end;

procedure TElX509Certificate.SetValidFrom(const Value: TDateTime);
var
  Validity: TValidity;
begin
  Validity := FtbsCertificate.Validity;
  Validity.NotBefore := Value;
  FtbsCertificate.Validity := Validity;
end;

procedure TElX509Certificate.SetValidTo(const Value: TDateTime);
var
  Validity: TValidity;
begin
  Validity := FtbsCertificate.Validity;
  Validity.NotAfter := Value;
  FtbsCertificate.Validity := Validity;
end;

function TElX509Certificate.GetPublicKeyAlgorithm: integer;
begin
  result := FtbsCertificate.FSubjectPublicKeyInfo.PublicKeyAlgorithm;
end;

function TElX509Certificate.GetPublicKeySize: integer;
var
  Sz1, Sz2, Sz3, Sz4, I: integer;
  Buf1, Buf2, Buf3, Buf4: ByteArray;
begin
  Sz1 := 0;
  Sz2 := 0;
  Sz3 := 0;
  Sz4 := 0;
  case PublicKeyAlgorithm of
    SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_ID_RSAPSS,
    SB_CERT_ALGORITHM_ID_RSAOAEP:
      begin
        GetRSAParams(nil, Sz1, nil, Sz2);
        SetLength(Buf1, Sz1);
        SetLength(Buf2, Sz2);
        GetRSAParams(@Buf1[0], Sz1, @Buf2[0], Sz2);
      end;
    SB_CERT_ALGORITHM_ID_DSA:
      begin
        GetDSSParams(nil, Sz1, nil, Sz2, nil, Sz3, nil, Sz4);
        SetLength(Buf1, Sz1);
        SetLength(Buf2, Sz2);
        SetLength(Buf3, Sz3);
        SetLength(Buf4, Sz4);
        GetDSSParams(@Buf1[0], Sz1, @Buf2[0], Sz2, @Buf3[0], Sz3, @Buf4[0], Sz4);
      end;
    SB_CERT_ALGORITHM_DH_PUBLIC:
      begin
        GetDHParams(nil, Sz1, nil, Sz2, nil, Sz3);
        SetLength(Buf1, Sz1);
        SetLength(Buf2, Sz2);
        SetLength(Buf3, Sz3);
        GetDHParams(@Buf1[0], Sz1, @Buf2[0], Sz2, @Buf3[0], Sz3);
      end;
  else
    begin
      Result := 0;
      Exit;
    end;
  end;
  I := 0;
  while (I < Length(Buf1)) and (Buf1[I] = 0) do
    Inc(I);
  Result := (Length(Buf1) - I) shl 3;
end;


function TElX509Certificate.GetPublicKeyBlob(Buffer: pointer; var Size: integer): boolean;
begin
  if Length(FPublicKeyBlob) > Size then
    Result := false
  else
  begin
    Move(FPublicKeyBlob[0], Buffer^, Length(FPublicKeyBlob));
    Result := true;
  end;
  Size := Length(FPublicKeyBlob);
end;


{$IFDEF SB_HAS_WINCRYPT}

function OpenSystemStoreByName(const Name: string; Access: TSBStorageAccessType): HCERTSTORE;
var
  Flag: cardinal;
  WideStr: PWideChar;
  Len: integer;
begin
  case Access of
    atCurrentService: Flag := CERT_SYSTEM_STORE_CURRENT_SERVICE;
    atCurrentUser: Flag := CERT_SYSTEM_STORE_CURRENT_USER;
    atCurrentUserGroupPolicy: Flag := CERT_SYSTEM_STORE_CURRENT_USER_GROUP_POLICY;
    atLocalMachine: Flag := CERT_SYSTEM_STORE_LOCAL_MACHINE;
    atLocalMachineEnterprise: Flag := CERT_SYSTEM_STORE_LOCAL_MACHINE_ENTERPRISE;
    atLocalMachineGroupPolicy: Flag := CERT_SYSTEM_STORE_LOCAL_MACHINE_GROUP_POLICY;
    atServices: Flag := CERT_SYSTEM_STORE_SERVICES;
    atUsers: Flag := CERT_SYSTEM_STORE_USERS;
  else
    Flag := 0;
  end;

  Len := Length(Name) shl 1 + 4;
  GetMem(WideStr, Len);
  try
    StringToWideChar(Name, WideStr, Len);
    Result := CertOpenStore(PChar(CERT_STORE_PROV_SYSTEM), X509_ASN_ENCODING or
      PKCS_7_ASN_ENCODING, 0, Flag, WideStr);
  finally
    FreeMem(WideStr);
  end;

  if Result = nil then
    Result := CertOpenSystemStore(0, PChar(Name));

end;

function TElX509Certificate.LoadPrivateKeyFromWin32: boolean;
var
  SysStores: TStringList;
  I: integer;
  CertStore: HCERTSTORE;
  Blob: CRYPT_HASH_BLOB;
  hCert: PCCERT_CONTEXT;
  M160: TMessageDigest160;
  AT: TSBStorageAccessType;
begin
{$IFDEF SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('Entering: LoadPrivateKeyFromWin32');
  Dumper.BeginSubsection;
{$ENDIF}
  SysStores := TStringList.Create;
  Result := false;

  for AT := Low(TSBStorageAccessType) to High(TSBStorageAccessType) do
  begin
    SysStores.Clear;
    TElWinCertStorage.GetAvailableStores(SysStores, AT);
    for I := 0 to SysStores.Count - 1 do
    begin
{$IFDEF SECURE_BLACKBOX_DEBUG}
      Dumper.WriteString('Searching: ' + SysStores[I]);
{$ENDIF}

      CertStore := OpenSystemStoreByName(SysStores[I], AT); //CertOpenSystemStore(0, PChar(SysStores.Strings[I]));
      if Assigned(CertStore) then
      begin
{$IFDEF SECURE_BLACKBOX_DEBUG}
        Dumper.WriteString('Store opened OK');
{$ENDIF}
        M160 := GetHashSHA1;
        Blob.cbData := 20;
        GetMem(Blob.pbData, 20);
        Move(M160, Blob.pbData^, 20);
        hCert := CertFindCertificateInStore(CertStore, X509_ASN_ENCODING or PKCS_7_ASN_ENCODING, 0, CERT_FIND_HASH, @Blob, nil);
        FreeMem(Blob.pbData);
        if Assigned(hCert) then
        begin
{$IFDEF SECURE_BLACKBOX_DEBUG}
          Dumper.WriteString('Certificate found, loading its private key');
{$ENDIF}
          Result := LoadPrivateKeyFromContext(hCert);
          CertFreeCertificateContext(hCert);
        end;
        CertCloseStore(CertStore, 0);
        if Result then
          Break;
      end;
    end;
    if Result then
      Break;
  end;
  FreeAndNil(SysStores);
{$IFDEF SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('Exiting: LoadPrivateKeyFromWin32');
  Dumper.EndSubsection;
{$ENDIF}
end;


function TElX509Certificate.LoadPrivateKeyFromContext(Context: PCCERT_CONTEXT): boolean;
var
  Sz: DWORD;
  Buffer: pointer;
  ProvInfo: PCRYPT_KEY_PROV_INFO;
  Prov: HCRYPTPROV;
  ProvName, ContName: PChar;
  LenProvName, LenContName: integer;
  Key: HCRYPTKEY;
begin
  Sz := 0;
  Result := false;
{$IFDEF SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('Entering: LoadPrivateKeyFromContext');
  Dumper.BeginSubsection;
{$ENDIF}

  CertGetCertificateContextProperty(Context, CERT_KEY_PROV_INFO_PROP_ID, nil, @Sz);
  GetMem(Buffer, Sz);
  if CertGetCertificateContextProperty(Context, CERT_KEY_PROV_INFO_PROP_ID, Buffer, @Sz) then
  begin
{$IFDEF SECURE_BLACKBOX_DEBUG}
    Dumper.WriteString('CertGetCertificateContextProperty succeeded');
{$ENDIF}
    ProvInfo := PCRYPT_KEY_PROV_INFO(Buffer);
    LenProvName := WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszProvName, -1, nil, 0, nil, nil);
    LenContName := WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszContainerName, -1, nil, 0, nil, nil);
    GetMem(ProvName, LenProvName);
    GetMem(ContName, LenContName);
    WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszProvName, -1, ProvName, LenProvName,
      nil, nil);
    WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszContainerName, -1, ContName, LenContName,
      nil, nil);
{$IFDEF SECURE_BLACKBOX_DEBUG}
    Dumper.WriteString('Trying to acquire a context');
{$ENDIF}
    if CryptAcquireContext(@Prov, ContName, ProvName, ProvInfo.dwProvType, ProvInfo.dwFlags {or CRYPT_SILENT}) then
    begin
{$IFDEF SECURE_BLACKBOX_DEBUG}
      Dumper.WriteString('Context acquired');
{$ENDIF}
      if CryptGetUserKey(Prov, AT_SIGNATURE, @Key)  then
      begin
{$IFDEF SECURE_BLACKBOX_DEBUG}
        Dumper.WriteString('CryptGetUserKey [AT_SIGNATURE] succeeded');
{$ENDIF}
        try
          Result := LoadPrivateKeyFromKey(Key);
        finally
          CryptDestroyKey(Key);
        end;
      end
      else
        if CryptGetUserKey(Prov, AT_KEYEXCHANGE, @Key)  then
      begin
{$IFDEF SECURE_BLACKBOX_DEBUG}
        Dumper.WriteString('CryptGetUserKey [AT_KEYEXCHANGE] succeeded');
{$ENDIF}
        try
          Result := LoadPrivateKeyFromKey(Key);
        finally
          CryptDestroyKey(Key);
        end;
      end;
      CryptReleaseContext(Prov, 0);
    end;
    FreeMem(ProvName);
    FreeMem(ContName);
  end;
  FreeMem(Buffer);
{$IFDEF SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('Exiting: LoadPrivateKeyFromContext');
  Dumper.EndSubsection;
{$ENDIF}
end;


function TElX509Certificate.LoadPrivateKeyFromKey(Key: HCRYPTKEY): boolean;
var
  KeyLen: DWORD;
  KeyBuf, EncKeyBuf: Windows.PBYTE;
  LenEnc: integer;
  BT, ErrCode: integer;
begin
{$IFDEF SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('Entering: LoadPrivateKeyFromKey');
  Dumper.BeginSubsection;
{$ENDIF}
  Result := true;
  KeyLen := 0;
  SetLastError(0);
  if not CryptExportKey(Key, 0, PRIVATEKEYBLOB, 0, nil, @KeyLen)  then
  begin
    //ErrCode := GetLastError;
{$IFDEF SECURE_BLACKBOX_DEBUG}
    Dumper.WriteString('Failed to export a key, exiting');
    Dumper.EndSubsection;
{$ENDIF}
    result := false;
    exit;
  end;
  GetMem(KeyBuf, KeyLen);
  try
    if CryptExportKey(Key, 0, PRIVATEKEYBLOB, 0, KeyBuf, @KeyLen)  then
    begin
{$IFDEF SECURE_BLACKBOX_DEBUG}
      Dumper.WriteString('CryptExportKey succeeded');
{$ENDIF}
      LenEnc := 0;
      ParseMSKeyBlob(KeyBuf, KeyLen, nil, LenEnc, BT);
      GetMem(EncKeyBuf, LenEnc);
      try
        ErrCode := ParseMSKeyBlob(KeyBuf, KeyLen, EncKeyBuf, LenEnc, BT);
{$IFDEF SECURE_BLACKBOX_DEBUG}
        Dumper.WriteString('ParseMSKeyBlob finished with code ' + IntToStr(ErrCode));
{$ENDIF}
        if ErrCode = 0 then
          LoadKeyFromBuffer(EncKeyBuf, LenEnc)
        else
          Result := false;
      finally
        FreeMem(EncKeyBuf);
      end;
    end
    else
    begin
      //ErrCode := GetLastError;
      Result := false;
    end;
  finally
    FreeMem(KeyBuf);
  end;
{$IFDEF SECURE_BLACKBOX_DEBUG}
  Dumper.WriteString('Exiting: LoadPrivateKeyFromKey');
  Dumper.EndSubsection;
{$ENDIF}
end;


function TElX509Certificate.GetContextByHash(var CertStore: HCERTSTORE): PCCERT_CONTEXT;
var
  SysStores: TStringList;
  I: integer;
  M160: TMessageDigest160;
  Blob: CRYPT_HASH_BLOB;
  hCert: PCCERT_CONTEXT;
begin
  SysStores := TStringList.Create;
  try
    TElWinCertStorage.GetAvailableStores(SysStores);
    Result := nil;
    for I := 0 to SysStores.Count - 1 do
    begin
      CertStore := CertOpenSystemStore(0, PChar(SysStores.Strings[I]));
      if Assigned(CertStore) then
      begin
        M160 := GetHashSHA1;
        GetMem(Blob.pbData, 20);
        Move(M160, Blob.pbData^, 20);
        Blob.cbData := 20;
        hCert := CertFindCertificateInStore(CertStore, X509_ASN_ENCODING or PKCS_7_ASN_ENCODING, 0, CERT_FIND_HASH, @Blob, nil);
        FreeMem(Blob.pbData);
        Result := hCert;
        if Assigned(Result) then
          Break;
        CertCloseStore(CertStore, 0);
        CertStore := nil;
      end;
    end;
  finally
    FreeAndNil(SysStores);
  end;
end;

{$ifndef NET_CF}


function TElX509Certificate.SignWin32(InBuffer: pointer; InSize: integer; OutBuffer:
  pointer; var OutSize: integer; AuthAttrs: TObject; UnauthAttrs: TObject;
  Detached: boolean = false; Storage: TObject = nil; IncludeCertificates: boolean = true): boolean;
var
  CertStore: HCERTSTORE;
  CertContext: PCCERT_CONTEXT;
  MsgCertContext: PCCERT_CONTEXT;
  MsgCerts: array of PCCERT_CONTEXT;
  Para: CRYPT_SIGN_MESSAGE_PARA;
  MessageArr: array[0..0] of PBYTE;
  AuthAttrArr: array of _CRYPT_ATTRIBUTE;
  UnauthAttrArr: array of _CRYPT_ATTRIBUTE;

  Sz: DWORD;
  Attrs: TElPKCS7Attributes;
  I: integer;
  MessageSizeArr: array[0..0] of DWORD;
  ErrCode: boolean;
  Str: TElCustomCertStorage;
begin
  Result := false;
  if Assigned(Storage) then
    Str := TElCustomCertStorage(Storage)
  else
    Str := nil;
  CertContext := GetContextByHash(CertStore);
  if (CertContext = nil) then
    Exit;
  Para.cbSize := SizeOf(CRYPT_SIGN_MESSAGE_PARA);
  Para.dwMsgEncodingType := X509_ASN_ENCODING or PKCS_7_ASN_ENCODING;
  Para.pSigningCert := CertContext; //insert here certificate context
  Para.HashAlgorithm.pszObjId := szOID_RSA_MD5;
  Para.HashAlgorithm.Parameters.cbData := 0;
  Para.pvHashAuxInfo := nil;

  if Str = nil then
  begin
    if IncludeCertificates then
    begin
      Para.cMsgCert := 1; // handle this like certificate chain
      Para.rgpMsgCert := @CertContext;
    end
    else
    begin
      Para.cMsgCert := 0;
      Para.rgpMsgCert := nil;
    end;
  end
  else
  begin
    Para.cMsgCert := 0;
    SetLength(MsgCerts, 0);
    for I := 0 to Str.Count - 1 do
    begin
      MsgCertContext := CertCreateCertificateContext(X509_ASN_ENCODING or PKCS_7_ASN_ENCODING,
        Windows.PByte(Str.Certificates[I].CertificateBinary),
        Str.Certificates[I].CertificateSize);
      if Assigned(MsgCertContext) then
      begin
        Inc(Para.cMsgCert);
        SetLength(MsgCerts, Para.cMsgCert);
        MsgCerts[Para.cMsgCert - 1] := MsgCertContext;
      end;
    end;
    Para.rgpMsgCert := @MsgCerts[0];
  end;

  Para.cMsgCrl := 0;
  Para.rgpMsgCrl := nil;

  //AuthAttrs := nil;
  //UnauthAttrs:=nil;

  if (not Assigned(AuthAttrs)) or (not (AuthAttrs is TElPKCS7Attributes)) then
  begin
    Para.cAuthAttr := 0;
    Para.rgAuthAttr := nil;
  end
  else
  begin
    Attrs := TElPKCS7Attributes(AuthAttrs);
    SetLength(AuthAttrArr, Attrs.Count);

    Para.cAuthAttr := Attrs.Count;
    for I := 0 to Attrs.Count - 1 do
      SetupCryptAttribute(Attrs.Attributes[I], Attrs.Values[I], @AuthAttrArr[I]);
    Para.rgAuthAttr := @AuthAttrArr[0];
  end;
  if (not Assigned(UnauthAttrs)) or (not (UnauthAttrs is TElPKCS7Attributes)) then
  begin
    Para.cUnauthAttr := 0;
    Para.rgUnauthAttr := nil;
  end
  else
  begin
    Attrs := TElPKCS7Attributes(UnauthAttrs);
    SetLength(UnauthAttrArr, Attrs.Count);
    
    Para.cUnauthAttr := Attrs.Count;
    for I := 0 to Attrs.Count - 1 do
      SetupCryptAttribute(Attrs.Attributes[I], Attrs.Values[I], @UnauthAttrArr[I]);
    Para.rgUnauthAttr := @UnauthAttrArr[0];
  end;
  Para.dwFlags := 0; //CRYPT_MESSAGE_SILENT_KEYSET_FLAG;
  Para.dwInnerContentType := 0;
  Para.HashEncryptionAlgorithm.pszObjId := nil;
  Para.HashEncryptionAlgorithm.Parameters.cbData := 0;
  Para.HashEncryptionAlgorithm.Parameters.pbData := nil;
  Para.pvHashEncryptionAuxInfo := nil;
  MessageArr[0] := InBuffer;
  MessageSizeArr[0] := InSize;

  Sz := OutSize;
  ErrCode := CryptSignMessage(@Para, Detached, 1, @MessageArr[0], @MessageSizeArr[0], OutBuffer, @Sz);
    if not ErrCode then
    begin
      if cardinal(OutSize) < Sz then
      begin
        OutSize := Sz + 32; //sometimes there take place strange error in CryptSignMessage
      end
      else
        OutSize := -1;
      Result := false;
    end
    else
    begin
      if cardinal(OutSize) < Sz then
        Result := false
      else
        Result := true;
      OutSize := Sz;
    end;

  for I := 0 to Length(AuthAttrArr) - 1 do
    ReleaseCryptAttribute(@AuthAttrArr[I]);
  for I := 0 to Length(UnauthAttrArr) - 1 do
    ReleaseCryptAttribute(@UnauthAttrArr[I]);

  for I := 0 to Length(MsgCerts) - 1 do
    CertFreeCertificateContext(MsgCerts[I]);

  CertFreeCertificateContext(CertContext);
  CertCloseStore(CertStore, 0);
end;


function TElX509Certificate.DecryptWin32(InBuffer: pointer; InSize: integer;
  OutBuffer: pointer; var OutSize: integer): integer;
var
  Para: CRYPT_DECRYPT_MESSAGE_PARA;
  CertStore: HCERTSTORE;
  CertContext: PCCERT_CONTEXT;
  Sz: DWORD;
  ErrCode: boolean;
  Err: cardinal;
begin
  Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
  CertContext := GetContextByHash(CertStore);
  if (CertContext = nil) then
    Exit;
  Sz := 0;
  FillChar(Para, SizeOf(Para), 0);
  Para.cbSize := SizeOf(CRYPT_DECRYPT_MESSAGE_PARA);
  Para.dwMsgAndCertEncodingType := X509_ASN_ENCODING or PKCS_7_ASN_ENCODING;
  Para.cCertStore := 1;
  Para.rghCertStore := @CertStore;
  ErrCode := CryptDecryptMessage(@Para, InBuffer, InSize, nil, @Sz, nil);
    if not ErrCode then
    begin
      Err := GetLastError;
      case Cardinal(Err) of
        CRYPT_E_UNEXPECTED_MSG_TYPE: Result := SB_MESSAGE_ERROR_NO_ENCRYPTED_DATA;
        _NTE_BAD_ALGID: Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
        CRYPT_E_NO_DECRYPT_CERT: Result := SB_MESSAGE_ERROR_NO_CERTIFICATE;
      else
        Result := SB_MESSAGE_ERROR_INVALID_FORMAT;
      end;
      CertFreeCertificateContext(CertContext);
      CertCloseStore(CertStore, 0);
      Exit;
    end;
    if integer(Sz) > OutSize then
    begin
      OutSize := Sz;
      Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
    end
    else
    begin
      Sz := OutSize;
      ErrCode := CryptDecryptMessage(@Para, InBuffer, InSize, OutBuffer, @Sz, nil);
      if ErrCode then
      begin
        OutSize := Sz;
        Result := 0;
      end
      else
      begin
        Err := GetLastError;
        if Err = CRYPT_E_UNKNOWN_ALGO then
          Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM
        else
          if Err = CRYPT_E_NO_DECRYPT_CERT then
          Result := SB_MESSAGE_ERROR_NO_CERTIFICATE
        else
          Result := SB_MESSAGE_ERROR_INTERNAL_ERROR
      end;
    end;
  CertFreeCertificateContext(CertContext);
  CertCloseStore(CertStore, 0);
end;
{$endif}

function TElX509Certificate.GetSupportedAlgorithmsFromWin32(var Pairs: TSBArrayOfPairs): boolean;
var
  Sz: Cardinal;
  Buffer: pointer;
  ProvInfo: PCRYPT_KEY_PROV_INFO;
  Prov: HCRYPTPROV;
  ProvName, ContName: PChar;
  LenProvName, LenContName: integer;
  Context: PCCERT_CONTEXT;
  CertStore: HCERTSTORE;
  P: pointer;
  Len: DWORD;
begin
  Result := false;
  SetLength(Pairs, 0);
  Context := GetContextByHash(CertStore);
  if not Assigned(Context) then
    Exit;
  Sz := 0;
  CertGetCertificateContextProperty(Context, CERT_KEY_PROV_INFO_PROP_ID, nil, @Sz);
  GetMem(Buffer, Sz);
  if CertGetCertificateContextProperty(Context, CERT_KEY_PROV_INFO_PROP_ID, Buffer, @Sz)  then
  begin
    ProvInfo := PCRYPT_KEY_PROV_INFO(Buffer);
    LenProvName := WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszProvName, -1, nil, 0, nil, nil);
    LenContName := WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszContainerName, -1, nil, 0, nil, nil);
    GetMem(ProvName, LenProvName);
    GetMem(ContName, LenContName);
    WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszProvName, -1, ProvName, LenProvName, nil, nil);
    WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszContainerName, -1, ContName, LenContName, nil, nil);
    if CryptAcquireContext(@Prov, ContName, ProvName, ProvInfo.dwProvType, ProvInfo.dwFlags {or CRYPT_SILENT}) then
    begin
      Len := 0;
      Result := true;

      if (CryptGetProvParam(Prov, PP_ENUMALGS, nil, @Len, CRYPT_FIRST))  then
      begin
        GetMem(P, Len);
        CryptGetProvParam(Prov, PP_ENUMALGS, P, @Len, CRYPT_FIRST);
        SetLength(Pairs, Length(Pairs) + 1);
        Pairs[Length(Pairs) - 1].A := PPROV_ENUMALGS(P).aiAlgId;
        Pairs[Length(Pairs) - 1].B := PPROV_ENUMALGS(P).dwBitLen;
        FreeMem(P);
      end;
      while (CryptGetProvParam(Prov, PP_ENUMALGS, nil, @Len, 0)) do
      begin
        GetMem(P, Len);
        CryptGetProvParam(Prov, PP_ENUMALGS, P, @Len, 0);
        SetLength(Pairs, Length(Pairs) + 1);
        Pairs[Length(Pairs) - 1].A := PPROV_ENUMALGS(P).aiAlgId;
        Pairs[Length(Pairs) - 1].B := PPROV_ENUMALGS(P).dwBitLen;
        FreeMem(P);
      end;
      CryptReleaseContext(Prov, 0);
    end;
    FreeMem(ProvName);
    FreeMem(ContName);
  end;
  FreeMem(Buffer);
  CertFreeCertificateContext(Context);
  CertCloseStore(CertStore, 0);
end;

{$ENDIF CLX_USED}

function TElX509Certificate.SignSSLHash(Hash: pointer; HashSize: integer;
  Buffer: pointer; var Size: integer): boolean;
begin
  if BelongsTo = BT_WINDOWS then
    Result := SignSSLHashWin32(Hash, HashSize, Buffer, Size)
{$ifndef SBB_NO_PKCS11}
  else
  if BelongsTo = BT_PKCS11 then
    Result := SignSSLHashPKCS11(Hash, HashSize, Buffer, Size)
{$ENDIF}
  else
    Result := false;
end;

function TElX509Certificate.SignRawHash(HashAlg : integer; Hash: pointer; HashSize: integer; Buffer: pointer;
  var Size: integer): boolean;
begin
  if BelongsTo = BT_WINDOWS then
    Result := SignRawHashWin32(HashAlg, Hash, HashSize, Buffer, Size)
  else 
  {$ifndef SBB_NO_PKCS11}
  if BelongsTo = BT_PKCS11 then
    Result := SignRawHashPKCS11(HashAlg, Hash, HashSize, Buffer, Size)
  else
  {$endif}
    Result := false;
end;

function TElX509Certificate.SignRawHashWin32(HashAlg : integer; Hash: pointer; HashSize: integer; Buffer: pointer;
  var Size: integer): boolean;
var
  Sz: DWORD;
  Win32AlgID : cardinal;
  I : integer;
  B : byte;
{$IFDEF SB_HAS_WINCRYPT}
  hHash: HCRYPTHASH;
  CertStore: HCERTSTORE;
  CertContext: PCCERT_CONTEXT;
  Buf: pointer;
  ProvInfo: PCRYPT_KEY_PROV_INFO;
  LenProvName, LenContName: integer;
  ProvName, ContName: PChar;
  Prov: HCRYPTPROV;
{$ENDIF}
begin
  Result := false;
{$IFDEF SB_HAS_WINCRYPT}
  CertContext := GetContextByHash(CertStore);
  if not Assigned(CertContext) then
    Exit;
  case HashAlg of
    SB_ALGORITHM_DGST_SHA1 : Win32AlgID := CALG_SHA1;
    SB_ALGORITHM_DGST_MD2 : Win32AlgID := CALG_MD2;
    SB_ALGORITHM_DGST_MD5 : Win32AlgID := CALG_MD5;
  else
    Exit;
  end;
    
  CertGetCertificateContextProperty(CertContext, CERT_KEY_PROV_INFO_PROP_ID, nil, @Sz);
  GetMem(Buf, Sz);
  if CertGetCertificateContextProperty(CertContext, CERT_KEY_PROV_INFO_PROP_ID, Buf, @Sz)  then
  begin
    ProvInfo := PCRYPT_KEY_PROV_INFO(Buf);
    LenProvName := WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszProvName, -1, nil, 0, nil, nil);
    LenContName := WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszContainerName, -1, nil, 0, nil, nil);
    GetMem(ProvName, LenProvName);
    GetMem(ContName, LenContName);
    WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszProvName, -1, ProvName, LenProvName, nil, nil);
    WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszContainerName, -1, ContName, LenContName, nil, nil);
    if CryptAcquireContext(@Prov, ContName, ProvName, ProvInfo.dwProvType, ProvInfo.dwFlags {or
      CRYPT_SILENT}) then
    begin
      if CryptCreateHash(Prov, Win32AlgID, 0, 0, @hHash)  then
      begin
        if CryptSetHashParam(hHash, HP_HASHVAL, Hash, 0) then
        begin
          ;
          Sz := 0;
          CryptSignHash(hHash, AT_KEYEXCHANGE, nil, 0, nil, @Sz);
          if integer(Sz) > Size then
            Size := Sz
          else
          begin
            Sz := Size;
            Result := CryptSignHash(hHash, ProvInfo.dwKeySpec {AT_KEYEXCHANGE}, nil, 0, Buffer, @Sz);
            Size := Sz;
          end;
        end;
      end;
      CryptDestroyHash(hHash);
    end;
    FreeMem(ProvName);
    FreeMem(ContName);
    CryptReleaseContext(Prov, 0);
  end;
  FreeMem(Buf);
  CertFreeCertificateContext(CertContext);
  CertCloseStore(CertStore, 0);
{$ENDIF}
  if Result and (Size > 0) then
  begin
    for I := 0 to (Size div 2) - 1 do
    begin
      B := PByteArray(Buffer)[I];
      PByteArray(Buffer)[I] := PByteArray(Buffer)[Size - I - 1];
      PByteArray(Buffer)[Size - I - 1] := B;
    end;
  end;
end;

{$ifndef SBB_NO_PKCS11}
function TElX509Certificate.SignRawHashPKCS11(HashAlg : integer; Hash: pointer;
  HashSize: integer; Buffer: pointer; var Size: integer): boolean;
var
  Ctx : TElPKCS11StgCtx;
  Sz : integer;
  FmtHash : ByteArray;
const
  PKCS1Prefix_SHA1: array [0..14] of byte =
    (
    $30, $21, $30, $09, $06, $05,
    $2b, $0E, $03, $02, $1A, $05, $00, $04, $14
    );
  PKCS1Prefix_SHA1_Length = 15;
begin
  Result := false;
  if HashAlg <> SB_ALGORITHM_DGST_SHA1 then
    Exit;
  Sz := 0;
  Ctx := GetContextByCertificate(Self);
  if Ctx = nil then
    Exit;
  SetLength(FmtHash, HashSize + PKCS1Prefix_SHA1_Length);
  Move(PKCS1Prefix_SHA1[0], FmtHash[0], PKCS1Prefix_SHA1_Length);
  Move(Hash^, FmtHash[PKCS1Prefix_SHA1_Length], HashSize);
  try
    Ctx.Sign(FmtHash, HashSize + PKCS1Prefix_SHA1_Length, nil, Sz);
    if Sz > Size then
    begin
      Size := Sz;
      Result := false;
      Exit;
    end;
    if Ctx.Sign(FmtHash, HashSize + PKCS1Prefix_SHA1_Length, Buffer, Sz) then
    begin
      Size := Sz;
      Result := true;
    end
    else
      Result := false;
  except
    ;
  end;
end;

function TElX509Certificate.SignSSLHashPKCS11(Hash: pointer; HashSize: integer;
  Buffer: pointer; var Size: integer): boolean;
var
  Ctx: TElPKCS11StgCtx;
  I: integer;
  B: byte;
begin
  Ctx := GetContextByCertificate(Self);
  if Ctx <> nil then
  begin
    Result := Ctx.Sign(Hash, HashSize, Buffer, Size);
    if Result then
      for I := 0 to (Size shr 1) - 1 do
      begin
        B := PByteArray(Buffer)[I];
        PByteArray(Buffer)[I] := PByteArray(Buffer)[Size - I - 1];
        PByteArray(Buffer)[Size - I - 1] := B;
      end;
  end
  else
    Result := false;
end;
{$endif}

function TElX509Certificate.SignSSLHashWin32(Hash: pointer; HashSize: integer;
  Buffer: pointer; var Size: integer): boolean;
var
  Sz: DWORD;
{$IFDEF SB_HAS_WINCRYPT}
  hHash: HCRYPTHASH;
  CertStore: HCERTSTORE;
  CertContext: PCCERT_CONTEXT;
  Buf: pointer;
  ProvInfo: PCRYPT_KEY_PROV_INFO;
  LenProvName, LenContName: integer;
  ProvName, ContName: PChar;
  Prov: HCRYPTPROV;
{$ENDIF}
begin
  Result := false;
{$IFDEF SB_HAS_WINCRYPT}
  CertContext := GetContextByHash(CertStore);
  if not Assigned(CertContext) then
    Exit;
    
  CertGetCertificateContextProperty(CertContext, CERT_KEY_PROV_INFO_PROP_ID, nil, @Sz);
  GetMem(Buf, Sz);
  if CertGetCertificateContextProperty(CertContext, CERT_KEY_PROV_INFO_PROP_ID, Buf, @Sz)  then
  begin
    ProvInfo := PCRYPT_KEY_PROV_INFO(Buf);
    LenProvName := WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszProvName, -1, nil, 0, nil, nil);
    LenContName := WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszContainerName, -1, nil, 0, nil, nil);
    GetMem(ProvName, LenProvName);
    GetMem(ContName, LenContName);
    WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszProvName, -1, ProvName, LenProvName, nil, nil);
    WideCharToMultiByte(CP_ACP, 0, ProvInfo.pwszContainerName, -1, ContName, LenContName, nil, nil);
    if CryptAcquireContext(@Prov, ContName, ProvName, ProvInfo.dwProvType, ProvInfo.dwFlags {or
      CRYPT_SILENT}) then
    begin
      if CryptCreateHash(Prov, CALG_SSL3_SHAMD5, 0, 0, @hHash)  then
      begin
        if CryptSetHashParam(hHash, HP_HASHVAL, Hash, 0) then
        begin
          ;
          Sz := 0;
          CryptSignHash(hHash, AT_KEYEXCHANGE, nil, 0, nil, @Sz);
          if integer(Sz) > Size then
            Size := Sz
          else
          begin
            Sz := Size;
            Result := CryptSignHash(hHash, ProvInfo.dwKeySpec {AT_KEYEXCHANGE}, nil, 0, Buffer, @Sz);
            Size := Sz;
          end;
        end;
      end;
      CryptDestroyHash(hHash);
    end;
    FreeMem(ProvName);
    FreeMem(ContName);
    CryptReleaseContext(Prov, 0);
  end;
  FreeMem(Buf);
  CertFreeCertificateContext(CertContext);
  CertCloseStore(CertStore, 0);
{$ENDIF}
end;


procedure TElX509Certificate.AssignTo(Dest: TPersistent);
begin
  if Dest is TElX509Certificate then
    Clone(TElX509Certificate(Dest){$ifndef HAS_DEF_PARAMS}, true{$endif})
  else
    inherited;
end;

procedure TElX509Certificate.RaiseInvalidCertificateException;
begin
  raise EElCertificateException.Create(SInvalidCertificate);
end;

class function TElX509Certificate.DetectKeyFileFormat(Stream: TStream; const Password: string): TSBX509KeyFileFormat;
var
  TmpStream: TMemoryStream;
  Cert: TElX509Certificate;
  Loaded: boolean;
  err: integer;
  SavePos: integer;
begin
  Result := kffUnknown;
  TmpStream := TMemoryStream.Create;
  try
    SavePos := Stream.Position;
    TmpStream.CopyFrom(Stream, Stream.Size - Stream.Position);
    TmpStream.Position := 0;
    Stream.Position := SavePos;
    Cert := TElX509Certificate.Create(nil);
    try

      Loaded := false;

      try

        // try DER
        try
          Cert.LoadKeyFromStream(TmpStream {$ifndef HAS_DEF_PARAMS}, 0{$endif});
          if Cert.PrivateKeyExists then
          begin
            result := kffDER;
            exit;
          end;
        except
        end;

        // try PEM
        if not Loaded then
        try
          TmpStream.Position := 0;
          err := Cert.LoadKeyFromStreamPEM(TmpStream, Password {$ifndef HAS_DEF_PARAMS}, 0{$endif});
          if (err <> 0) and (err <> PEM_DECODE_RESULT_INVALID_PASSPHRASE) then
            RaisePEMError(err);
          if (err = PEM_DECODE_RESULT_INVALID_PASSPHRASE) or Cert.PrivateKeyExists then
          begin
            result := kffPEM;
            exit;
          end;
        except
        end;

        // try PVK
        if not Loaded then
        try
          TmpStream.Position := 0;
          err := Cert.LoadKeyFromStreamPVK(TmpStream, Password {$ifndef HAS_DEF_PARAMS}, 0{$endif});
          if (err <> 0) and (err <> SB_X509_ERROR_INVALID_PASSWORD) then
            RaiseX509Error(err);
          if Cert.PrivateKeyExists then
          begin
            result := kffPVK;
            exit;
          end;
        except
        end;

        // try PFX
        if not Loaded then
        try
          TmpStream.Position := 0;
          err := Cert.LoadFromStreamPFX(TmpStream, Password {$ifndef HAS_DEF_PARAMS}, 0{$endif});
          if (err <> 0) and (err <> SB_PKCS12_ERROR_INVALID_PASSWORD) then
            RaisePKCS12Error(err);
          if (err = SB_PKCS12_ERROR_INVALID_PASSWORD) or Cert.PrivateKeyExists then
          begin
            result := kffPFX;
            exit;
          end;
        except
        end;

        {$ifndef B_6}
        // try PKCS8
        if not Loaded then
        try
          TmpStream.Position := 0;
          err := Cert.LoadKeyFromStreamNET(TmpStream, Password {$ifndef HAS_DEF_PARAMS}, 0{$endif});
          if (err <> 0) and (err <> SB_PKCS8_ERROR_INVALID_PASSWORD) then
            RaisePKCS8Error(err);
          if (err = SB_PKCS8_ERROR_INVALID_PASSWORD) or Cert.PrivateKeyExists then
          begin
            result := kffNET;
            exit;
          end;
        except
        end;
        {$endif}
      except
      end;
    finally
      FreeAndNil(Cert);
    end;
  finally
    TmpStream.Free;
  end;
end;

class function TElX509Certificate.DetectKeyFileFormat(FileName: string; const Password: string): TSBX509KeyFileFormat;
var
  Stream: TFileStream;
begin
  Result := kffUnknown;

  try
    Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
      result := DetectKeyFileFormat(Stream, Password);
    finally
      Stream.Free;
    end;
  except
  end;
end;

class function TElX509Certificate.DetectCertFileFormat(Stream: TStream):
TSBCertFileFormat;
var
  TmpStream: TMemoryStream;
  Cert: TElX509Certificate;
  Loaded: boolean;
  err: integer;
  SavePos: integer;
begin
  Result := cfUnknown;
  TmpStream := TMemoryStream.Create;
  try
    SavePos := Stream.Position;
    TmpStream.CopyFrom(Stream, Stream.Size - Stream.Position);
    TmpStream.Position := 0;
    Stream.Position := SavePos;
    Cert := TElX509Certificate.Create(nil);
    try

      Loaded := false;

      try
        // try DER
        try
          Cert.LoadFromStream(TmpStream {$ifndef HAS_DEF_PARAMS}, 0{$endif});
          if Cert.CertificateSize > 0 then
          begin
            result := cfDER;
            exit;
          end;
        except
        end;

        (*
        // try MS BLOB
        try
          Cert.LoadFromStreamMS(Stream {$ifndef HAS_DEF_PARAMS}, 0{$endif});
          if Cert.CertificateSize > 0 then
          begin
            result := cfMSBLOB;
            exit;
          end;
        except
        end;
        *)

        // try PEM
        if not Loaded then
        try
          TmpStream.Position := 0;
          err := Cert.LoadFromStreamPEM(TmpStream, '' {$ifndef HAS_DEF_PARAMS}, 0{$endif});
          if (err <> 0) and (err <> PEM_DECODE_RESULT_INVALID_PASSPHRASE) then
            RaisePEMError(err);
          if (err = PEM_DECODE_RESULT_INVALID_PASSPHRASE) or (Cert.CertificateSize > 0) then
          begin
            result := cfPEM;
            exit;
          end;
        except
        end;

        // try PFX
        if not Loaded then
        try
          TmpStream.Position := 0;
          err := Cert.LoadFromStreamPFX(TmpStream, '' {$ifndef HAS_DEF_PARAMS}, 0{$endif});
          if (err <> 0) and (err <> SB_PKCS12_ERROR_INVALID_PASSWORD) then
            RaisePKCS12Error(err);
          if (err = SB_PKCS12_ERROR_INVALID_PASSWORD) or (Cert.CertificateSize > 0) then
          begin
            result := cfPFX;
            exit;
          end;
        except
        end;

        // try SPC
        if not Loaded then
        try
          TmpStream.Position := 0;
          err := Cert.LoadFromStreamSPC(TmpStream {$ifndef HAS_DEF_PARAMS}, 0{$endif});
          if (err <> 0) then
            RaisePKCS7Error(err);
          if (Cert.CertificateSize > 0) then
          begin
            result := cfSPC;
            exit;
          end;
        except
        end;

      except
      end;
    finally
      FreeAndNil(Cert);
    end;
  finally
    TmpStream.Free;
  end;
end;

class function TElX509Certificate.DetectCertFileFormat(FileName: string): TSBCertFileFormat;
var
  Stream: TFileStream;
begin
  Result := cfUnknown;
  try
    Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
      result := DetectCertFileFormat(Stream);
    finally
      Stream.Free;
    end;
  except
  end;
end;



function TElX509Certificate.LoadFromBufferSPC(Buffer: pointer; Size: integer): integer;
var
  Msg: TElPKCS7Message;
begin
  Msg := TElPKCS7Message.Create;
  try
    result := Msg.LoadFromBuffer(Buffer, Size);
    if (result = 0) and (Msg.SignedData.Certificates <> nil) and (Msg.SignedData.Certificates.Count = 1) then
    begin
      Msg.SignedData.Certificates.Certificates[0].AssignTo(Self);
    end;
  finally
    FreeAndNil(Msg);
  end;
end;



function TElX509Certificate.LoadKeyFromBufferMS(Buffer: pointer; Size: integer): integer;
var
  OutSize, BlobType: integer;
  Buf: ByteArray;
begin
  OutSize := 0;
  ParseMSKeyBlob(Buffer, Size, nil, OutSize, BlobType);
  SetLength(Buf, OutSize);
  Result := ParseMSKeyBlob(Buffer, Size, @Buf[0], OutSize, BlobType);
  SetLength(Buf, OutSize);
  if Result = 0 then
    LoadKeyFromBuffer(@Buf[0], OutSize);
end;

function TElX509Certificate.LoadKeyFromStreamMS(Stream: TStream; Count: integer = 0): integer;
var
  Buf: ByteArray;
begin
  if Count = 0 then
    Count := Stream.Size;
  SetLength(Buf, Count);
  Stream.Read(Buf[0], Count);
  Result := LoadKeyFromBufferMS(@Buf[0], Count);
end;



function TElX509Certificate.SaveKeyToBufferMS(Buffer: pointer; var Size: integer): integer;
var
  Buf: ByteArray;
  Sz: word;
  B: boolean;
  OldSize: integer;
begin
  Sz := 0;
  SaveKeyToBuffer(nil, Sz);
  SetLength(Buf, Sz);
  if (not SaveKeyToBuffer(@Buf[0], Sz)) or (Sz = 0) then
    Result := SB_MSKEYBLOB_ERROR_NO_PRIVATE_KEY
  else
  begin
    OldSize := Size;
    SetLength(Buf, Sz);
    if PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
      B := WriteMSKeyBlob(@Buf[0], Sz, Buffer, Size, SB_KEY_BLOB_RSA)
    else
      if PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
      B := WriteMSKeyBlob(@Buf[0], Sz, Buffer, Size, SB_KEY_BLOB_DSS)
    else
    begin
      Result := SB_MSKEYBLOB_ERROR_UNSUPPORTED_ALGORITHM;
      Exit;
    end;
    if B then
      Result := 0
    else
    begin
      if OldSize < Size then
        Result := SB_MSKEYBLOB_ERROR_BUFFER_TOO_SMALL
      else
        Result := SB_MSKEYBLOB_ERROR_INVALID_FORMAT;
    end;
  end;
end;


function TElX509Certificate.SaveKeyToStreamMS(Stream: TStream): integer;
var
  Buf: ByteArray;
  Size: integer;
begin
  Size := 0;
  SaveKeyToBufferMS(nil, Size);
  SetLength(Buf, Size);
  Result := SaveKeyToBufferMS(@Buf[0], Size);
  if Result = 0 then
    Stream.Write(Buf[0], Size);
end;

function TElX509Certificate.SaveKeyToStreamPVK(Stream: TStream;
  const Password: string; UseStrongEncryption: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}): integer;
var
  Buf: ByteArray;
  Size: integer;
begin
  Size := 0;
  SaveKeyToBufferPVK(nil, Size, Password, UseStrongEncryption);
  SetLength(Buf, Size);
  Result := SaveKeyToBufferPVK(@Buf[0],
    Size, Password, UseStrongEncryption);
  if Result = 0 then
    Stream.WriteBuffer(Buf[0], Size);
end;


function TElX509Certificate.SaveKeyToBufferPVK(Buffer: pointer; var Size: integer;
  const Password: string; UseStrongEncryption: boolean = true): integer;
var
  Blob, MSBlob: ByteArray;
  BlobSize: word;
  BlobType: byte;
  MSBlobSize: integer;
  Salt, Key: string;
  PVKHeader: TPVKHeader;

  function GenerateSalt: string;
  begin
    SetLength(Result, 16);
    SBRndGenerate(@Result[1], Length(Result));
  end;

  {$IFNDEF SBB_NO_RC4}
  procedure EncryptBlob(const Key: string;
  Buffer: pointer; Size: integer);
  var
    Context: TRC4Context;
  begin
    SBRC4.Initialize(Context, TRC4Key(BytesOfString(Key)));
    SBRC4.Encrypt(Context, Buffer, Buffer, Size);
  end;
  {$endif}

begin
  BlobSize := 0;
  SaveKeyToBuffer(nil, BlobSize);
  if (BlobSize = 0) or (not FPrivateKeyExists) then
  begin
    Result := SB_X509_ERROR_NO_PRIVATE_KEY;
    Exit;
  end;
  SetLength(Blob, BlobSize);
  if not SaveKeyToBuffer(@Blob[0], BlobSize) then
  begin
    Result := SB_X509_ERROR_NO_PRIVATE_KEY;
    Exit;
  end;
  SetLength(Blob, BlobSize);
  if PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    BlobType := SB_KEY_BLOB_RSA
  else
    if PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
    BlobType := SB_KEY_BLOB_DSS
  else
  begin
    Result := SB_X509_ERROR_UNSUPPORTED_ALGORITHM;
    Exit;
  end;

  MSBlobSize := 0;
  SBMSKeyBlob.WriteMSKeyBlob(@Blob[0], BlobSize, nil, MSBlobSize, BlobType);
  SetLength(MSBlob, MSBlobSize);
  if not SBMSKeyBlob.WriteMSKeyBlob(@Blob[0], BlobSize, @MSBlob[0], MSBlobSize,
    BlobType) then
  begin
    Result := SB_X509_ERROR_INVALID_PRIVATE_KEY;
    Exit;
  end;

  if MSBlobSize < 8 then
  begin
    Result := SB_X509_ERROR_INTERNAL_ERROR;
    Exit;
  end;

  {$IFNDEF SBB_NO_RC4}
  if Length(Password) > 0 then
  begin
    if Size < MSBlobSize + 16 + SizeOf(TPVKHeader) then
    begin
      Result := SB_X509_ERROR_BUFFER_TOO_SMALL;
      Size := MSBlobSize + 16 + SizeOf(TPVKHeader);
      Exit;
    end;

    Salt := GenerateSalt;
    Key := PVK_DeriveKey((Password), Salt, not UseStrongEncryption);
    EncryptBlob(Key, @MSBlob[8], MSBlobSize - 8);
    PVKHeader.encrypted := 1;
    PVKHeader.saltlen := 16;
  end
  else
  {$endif}
  begin
    if Size < MSBlobSize + SizeOf(TPVKHeader) then
    begin
      Result := SB_X509_ERROR_BUFFER_TOO_SMALL;
      Size := MSBlobSize + SizeOf(TPVKHeader);
      Exit;
    end;

    PVKHeader.encrypted := 0;
    PVKHeader.saltlen := 0;
  end;

  PVKHeader.magic := $B0B5F11E;
  PVKHeader.reserved := 0;
  if BlobType = SB_KEY_BLOB_RSA then
    PVKHeader.keytype := 1
  else
    PVKHeader.keytype := 2;

  PVKHeader.keylen := MSBlobSize;
  Move(PVKHeader, Buffer^, SizeOf(PVKHeader));
  Move(Salt[1], PByteArray(Buffer)[SizeOf(PVKHeader)], PVKHeader.saltlen);
  Move(MSBlob[0], PByteArray(Buffer)[SizeOf(PVKHeader) + PVKHeader.saltlen],
    MSBlobSize);
  Size := SizeOf(TPVKHeader) + integer(PVKHeader.saltlen) + MSBlobSize;
  Result := 0;
end;


function TElX509Certificate.ParseDSSSignature(P: pointer; Size: integer;
  var R: string; var S: string): boolean;
var
  Tag, ContentTag: TElASN1ConstrainedTag;
begin
  Tag := TElASN1ConstrainedTag.Create;
  Result := Tag.LoadFromBuffer(P, Size);
  if Result then
  begin
    if (Tag.Count <> 1) or (not Tag.GetField(0).IsConstrained) or
      (Tag.GetField(0).TagID <> SB_ASN1_SEQUENCE) then
      Result := false
    else
    begin
      ContentTag := TElASN1ConstrainedTag(Tag.GetField(0));
      if (ContentTag.Count <> 2) or (ContentTag.GetField(0).IsConstrained) or
        (ContentTag.GetField(1).IsConstrained) or (ContentTag.GetField(0).TagID <>
        SB_ASN1_INTEGER) or (ContentTag.GetField(1).TagID <> SB_ASN1_INTEGER) then
        Result := false
      else
      begin
        SetLength(R, Length(TElASN1SimpleTag(ContentTag.GetField(0)).Content));
        SetLength(S, Length(TElASN1SimpleTag(ContentTag.GetField(1)).Content));
        Move(TElASN1SimpleTag(ContentTag.GetField(0)).Content[1], R[1],
          Length(TElASN1SimpleTag(ContentTag.GetField(0)).Content));
        Move(TElASN1SimpleTag(ContentTag.GetField(1)).Content[1], S[1],
          Length(TElASN1SimpleTag(ContentTag.GetField(1)).Content));
        result := true;
      end;
    end;
  end;
  
  Tag.Free;
end;


// PVK format as described at http://www.drh-consultancy.demon.co.uk/pvk.html
function TElX509Certificate.LoadKeyFromBufferPVK(Buffer: pointer; Size: integer;
  const Password: string): integer;
var
  PVKHeader: TPVKHeader;
  Salt: string;
  Key: string;
  {$IFNDEF SBB_NO_RC4}
  Ctx: TRC4Context;
  {$endif}
  Ptr: ^byte;
  Blob, DecryptedBlob: ByteArray;
  BT: integer;
  PBSize: integer;
begin
  result := SB_X509_ERROR_INVALID_PVK_FILE;
  if Size < SizeOf(TPVKHeader) then
    Exit;
  Move(Buffer^, PVKHeader, sizeof(PVKHeader));
  if (PVKHeader.magic <> $B0B5F11E) or
    (PVKHeader.reserved <> 0) or
    (not (PVKHeader.keytype in [1, 2])) or
    (not (PVKHeader.encrypted in [0, 1])) or
    (not (PVKHeader.saltlen in [0, $10])) then
    Exit;
  if Size <> integer(SizeOf(TPVKHeader) + PVKHeader.saltlen + PVKHeader.keylen) then
    Exit;
  SetLength(Blob, PVKHeader.keylen);
  if PVKHeader.keylen < 8 then
    Exit;

  Ptr := @PByteArray(Buffer)[SizeOf(PVKHeader) + PVKHeader.saltlen];
  Dec(Size, SizeOf(TPVKHeader) + PVKHeader.saltlen);
  Move(Ptr^, Blob[0], 8);
  Inc(Ptr, 8);
  Dec(Size, 8);

  {$IFNDEF SBB_NO_RC4}
  if (PVKHeader.encrypted = 1) and (PVKHeader.saltlen = $10) then
  begin
    SetLength(Salt, 16);
    Move(PByteArray(Buffer)[SizeOf(PVKHeader)], Salt[1], Length(Salt));
    if Size < 4 then
      Exit;
    Key := PVK_DeriveKey((Password), Salt, false);
    if not PVK_CheckKey(Key, Ptr, @Blob[8], Ctx) then
    begin
      Key := PVK_DeriveKey((Password), Salt, true);
      if not PVK_CheckKey(Key, Ptr, @Blob[8], Ctx) then
      begin
        Result := SB_X509_ERROR_INVALID_PASSWORD;
        Exit;
      end;
    end;
    Inc(Ptr, 4);
    Dec(Size, 4);
    SBRC4.Decrypt(Ctx, Ptr, @Blob[12], Size);
  end
  else
  {$endif}
    Move(Ptr^, Blob[8], Length(Blob) - 8);
  PBSize := 0;
  SBMSKeyBlob.ParseMSKeyBlob(@Blob[0], Length(Blob), nil, PBSize, BT);
  SetLength(DecryptedBlob, PBSize);
  Result := SBMSKeyBlob.ParseMSKeyBlob(@Blob[0], Length(Blob), @DecryptedBlob[0],
    PBSize, BT);
  if Result <> 0 then
    Exit;
  LoadKeyFromBuffer(@DecryptedBlob[0], PBSize);
  if Length(FPrivateKeyBlob) = 0 then
    Result := SB_X509_ERROR_INVALID_PVK_FILE;
end;



{$ifndef B_6}
function TElX509Certificate.LoadKeyFromBufferNET(Buffer: pointer; Size: integer;
  const Password: string): integer;
{$endif}
{$ifndef B_6}
var
  Tag, ContentTag: TElASN1ConstrainedTag;
  Key : TElPKCS8PrivateKey;
  Buf : BufferType;
  Sz: integer;
const
  PRIVATE_KEY_ID = 'private-key';
begin
  Tag := TElASN1ConstrainedTag.Create;
  if not Tag.LoadFromBuffer(Buffer, Size) then
  begin
    Result := SB_PKCS8_ERROR_INVALID_ASN_DATA;
    Tag.Free;
    Exit;
  end;
  if (Tag.Count <> 1) or (Tag.GetField(0).TagID <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS8_ERROR_INVALID_FORMAT;
    Tag.Free;
    Exit;
  end;
  ContentTag := TElASN1ConstrainedTag(Tag.GetField(0));
  if (ContentTag.Count <> 2) or (ContentTag.GetField(0).IsConstrained) or
    (ContentTag.GetField(0).TagID <> SB_ASN1_OCTETSTRING) or
    (not ContentTag.GetField(1).IsConstrained) or
    (ContentTag.GetField(1).TagID <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS8_ERROR_INVALID_FORMAT;
    Tag.Free;
    Exit;
  end;
  if CompareStr(TElASN1SimpleTag(ContentTag.GetField(0)).Content, PRIVATE_KEY_ID) <> 0 then
  begin
    Result := SB_PKCS8_ERROR_INVALID_FORMAT;
    Tag.Free;
    Exit;
  end;
  Sz := 0;
  ContentTag.GetField(1).SaveToBuffer(nil, Sz);
  SetLength(Buf, Sz);
  ContentTag.GetField(1).SaveToBuffer(@Buf[1], Sz);
  Key := TElPKCS8PrivateKey.Create;
  try
    Result := Key.LoadFromBuffer(@Buf[1], Sz, Password);
    if Result <> 0 then
      Exit;
    LoadKeyFromBuffer(@Key.KeyMaterial[1], Length(Key.KeyMaterial));
  finally
    FreeAndNil(Key);
  end;
  Result := 0;
end;
{$endif}

{$ifndef B_6}
function TElX509Certificate.LoadKeyFromStreamNET(Stream: TStream; const Password: string; Count: integer = 0): integer;
{$endif}
{$ifndef B_6}
var
  Buf: ByteArray;
begin
  if Count = 0 then
    Count := Stream.Size;
  SetLength(Buf, Count);
  Stream.Read(Buf[0], Count);
  Result := LoadKeyFromBufferNET(@Buf[0], Count, Password);
end;
{$endif}



function TElX509Certificate.SaveKeyToBufferNET(Buffer: pointer; var Size:
  integer): integer;
begin
  result := -1;
end;

function TElX509Certificate.SaveKeyToStreamNET(Stream: TStream; const Password:
  string): integer;
begin
  result := -1;
end;



function TElX509Certificate.SaveToBufferSPC(Buffer: pointer; var Size: integer): integer;
var
  Msg: TElPKCS7Message;
begin
  Msg := TElPKCS7Message.Create;
  try
    Msg.ContentType := ctSignedData;
    Msg.SignedData.Version := 1;
    Msg.SignedData.Certificates.Add(Self{$ifndef HAS_DEF_PARAMS}, true{$endif});
    if Msg.SaveToBuffer(Buffer, Size) then
      Result := 0
    else
      Result := SB_X509_ERROR_BUFFER_TOO_SMALL;
  finally
    FreeAndNil(Msg);
  end;
end;


function TElX509Certificate.SaveToStreamSPC(Stream: TStream): integer;
var
  Buf: ByteArray;
  Size: integer;
begin
  Size := 0;
  SaveToBufferSPC(nil, Size);
  SetLength(Buf, Size);
  Result := SaveToBufferSPC(@Buf[0], Size);
  if Result = 0 then
    Stream.WriteBuffer(Buf[0], Size);
end;

function TElX509Certificate.IsKeyValid: boolean;
var
  Size: word;
  Buf, M, E, D, PM, PE, Sig: ByteArray;
  MSize, ESize, DSize, PMSize, PESize, SigSize: integer;
  Hash: array[0..19] of byte;
begin
  Result := false;
  if not PrivateKeyExists then
    Exit;

  Size := 0;
  SaveKeyToBuffer(nil, Size);
  SetLength(Buf, Size);
  if not SaveKeyToBuffer(@Buf[0], Size) then
    Exit;

  if (PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or
    (PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) or
    (PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) then
  begin
    // checking correspondence between public and private key
    MSize := 0;
    SBRSA.DecodePrivateKey(@Buf[0], Size, nil, MSize, nil, ESize, nil, DSize);
    SetLength(M, MSize);
    SetLength(E, ESize);
    SetLength(D, DSize);
    SBRSA.DecodePrivateKey(@Buf[0], Size, @M[0], MSize, @E[0], ESize, @D[0], DSize);
    PMSize := 0;
    GetRSAParams(nil, PMSize, nil, PESize);
    SetLength(PM, PMSize);
    SetLength(PE, PESize);
    GetRSAParams(@PM[0], PMSize, @PE[0], PESize);
    if (PMSize <> MSize) or (PESize <> ESize) or
    (not CompareMem(@M[0], @PM[0], MSize)) or
      (not Comparemem(@E[0], @PE[0], ESize))
 then
      Exit;
    Result := SBRSA.IsValidKey(@Buf[0], Size);
  end
  else
    if PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    SBRndGenerate(@Hash[0], Length(Hash));
    SigSize := 0;
    Sign(@Hash[0], Length(Hash), nil, SigSize, true, false);
    SetLength(Sig, SigSize);
    if Sign(@Hash[0], Length(Hash), @Sig[0], SigSize, true, false) then
      Result := Verify(@Hash[0], Length(Hash), @Sig[0], SigSize);
  end
  else
    if PublicKeyAlgorithm = SB_CERT_ALGORITHM_DH_PUBLIC then
    Result := true;
end;

function TElX509Certificate.GetCanEncrypt: boolean;
begin
  Result := (PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or (PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP);
end;

function TElX509Certificate.GetCanSign: boolean;
begin
  Result := (PublicKeyAlgorithm in [SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_ID_DSA, SB_CERT_ALGORITHM_ID_RSAPSS]);
end;

function TElX509Certificate.GetVersion: byte;
begin
  Result := FTbsCertificate.FVersion;
end;

function TElX509Certificate.GetSerialNumber: BufferType;
begin
  Result := FTbsCertificate.FSerialNumber;
end;

procedure TElX509Certificate.SetSerialNumber(const Value: BufferType);
begin
  FTbsCertificate.FSerialNumber := Value;
end;

function TElX509Certificate.GetIssuer: TStringList;
begin
  Result := FTbsCertificate.FIssuer;
end;

function TElX509Certificate.GetSubject: TStringList;
begin
  Result := FTbsCertificate.FSubject;
end;

function TElX509Certificate.GetIssuerUniqueID: BufferType;
begin
  Result := FTbsCertificate.FIssuerUniqueID;
end;

function TElX509Certificate.GetSubjectUniqueID: BufferType;
begin
  Result := FTbsCertificate.FSubjectUniqueID;
end;

function TElX509Certificate.GetPrivateKeyExtractable : boolean;
var
  Sz : word;
begin
  Sz := 0;
  SaveKeyToBuffer(nil, Sz);
  Result := Sz > 0;
end;

function TElX509Certificate.WriteExtensionSubjectKeyIdentifier: BufferType;
var
  Writer : TElExtensionWriter;
begin
  Writer := TElExtensionWriter.Create(FCertificateExtensions{$ifndef HAS_DEF_PARAMS}, true{$endif});
  try
    Result := Writer.WriteExtensionSubjectKeyIdentifier;
  finally
    FreeAndNil(Writer);
  end;
end;

function TElX509Certificate.GetKeyHashSHA1: TMessageDigest160;
var
  MSize, ESize : integer;
  MBuf, EBuf : ByteArray;
  Index : integer;
begin
  if PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
  begin
    MSize := 0;
    GetRSAParams(nil, MSize, nil, ESize);
    SetLength(MBuf, MSize);
    SetLength(EBuf, ESize);
    GetRSAParams(@MBuf[0], MSize, @EBuf[0], ESize);
    Index := 0;
    while (Index < MSize) and (MBuf[Index] = 0) do
      Inc(Index);
    if Index < MSize then
      Result := HashSHA1(@MBuf[Index], MSize - Index)
    else
      Result := HashSHA1(EmptyBuffer);
  end
  else
  begin
    Result := HashSHA1(@FTbsCertificate.FSubjectPublicKeyInfo.FRawData[0],
      Length(FTbsCertificate.FSubjectPublicKeyInfo.FRawData));
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TElEncryptionParameters class

procedure TElEncryptionParameters.Assign(Source : TElEncryptionParameters);
begin
  FAlgorithm := Source.FAlgorithm;
end;

////////////////////////////////////////////////////////////////////////////////
// TElRSAEncryptionParameters class

procedure TElRSAEncryptionParameters.Assign(Source : TElEncryptionParameters);
begin
  inherited;

  if Source is TElRSAEncryptionParameters then
  begin
    SetLength(FPublicExponent, Length(TElRSAEncryptionParameters(Source).FPublicExponent));
    if Length(FPublicExponent) > 0 then
      Move(TElRSAEncryptionParameters(Source).FPublicExponent[0], FPublicExponent[0], Length(FPublicExponent));
    SetLength(FPublicModulus, Length(TElRSAEncryptionParameters(Source).FPublicModulus));
    if Length(FPublicModulus) > 0 then
      Move(TElRSAEncryptionParameters(Source).FPublicModulus[0], FPublicModulus[0], Length(FPublicModulus));
    SetLength(FPrivateExponent, Length(TElRSAEncryptionParameters(Source).FPrivateExponent));
    if Length(FPrivateExponent) > 0 then
      Move(TElRSAEncryptionParameters(Source).FPrivateExponent[0], FPrivateExponent[0], Length(FPrivateExponent));
  end;
end;

procedure TElRSAEncryptionParameters.LoadPublicKey(Tag: TElASN1ConstrainedTag);
var
  SeqTag: TElASN1ConstrainedTag;
begin
  if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
  begin
    SeqTag := TElASN1ConstrainedTag(Tag.GetField(0));
    if (SeqTag.Count = 2) and (SeqTag.GetField(0).CheckType(SB_ASN1_INTEGER, false)) and
      (SeqTag.GetField(1).CheckType(SB_ASN1_INTEGER, false)) then
    begin
      FPublicModulus := BytesOfString(TElASN1SimpleTag(SeqTag.GetField(0)).Content);
      FPublicExponent := BytesOfString(TElASN1SimpleTag(SeqTag.GetField(1)).Content);
    end
    else
      raise EElCertificateException.Create(SInvalidtbsCert);
  end
  else
    raise EElCertificateException.Create(SInvalidtbsCert);
end;

////////////////////////////////////////////////////////////////////////////////
// TElRSAPSSSignParameters class

constructor TElRSAPSSSignParameters.Create;
begin
  inherited;

  FSaltSize := 20;
  FHashAlgorithm := SB_ALGORITHM_DGST_SHA1;
  FTrailerField := 1;
  FMGF := SB_CERT_MGF1;
  FMGFHashAlgorithm := SB_ALGORITHM_DGST_SHA1;
end;

procedure TElRSAPSSSignParameters.Assign(Source : TElEncryptionParameters);
begin
  inherited;

  if Source is TElRSAPSSSignParameters then
  begin
    FSaltSize := TElRSAPSSSignParameters(Source).FSaltSize;
    FHashAlgorithm := TElRSAPSSSignParameters(Source).FHashAlgorithm;
    FTrailerField := TElRSAPSSSignParameters(Source).FTrailerField;
    FMGF := TElRSAPSSSignParameters(Source).FMGF;
    FMGFHashAlgorithm := TElRSAPSSSignParameters(Source).FMGFHashAlgorithm;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TElRSAOAEPEncryptionParameters class

constructor TElRSAOAEPEncryptionParameters.Create;
begin
  inherited;

  FHashAlgorithm := SB_ALGORITHM_DGST_SHA1;
  FStrLabel := '';
  FMGFHashAlgorithm := SB_ALGORITHM_DGST_SHA1;
  FMGF := SB_CERT_MGF1;
end;

procedure TElRSAOAEPEncryptionParameters.Assign(Source : TElEncryptionParameters);
begin
  inherited;

  if Source is TElRSAOAEPEncryptionParameters then
  begin
    FHashAlgorithm := TElRSAOAEPEncryptionParameters(Source).FHashAlgorithm;
    FStrLabel := TElRSAOAEPEncryptionParameters(Source).FStrLabel;
    FMGFHashAlgorithm := TElRSAOAEPEncryptionParameters(Source).FMGFHashAlgorithm;
    FMGF := TElRSAOAEPEncryptionParameters(Source).FMGF;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TElDSAEncryptionParameters class

procedure TElDSAEncryptionParameters.Assign(Source : TElEncryptionParameters);
begin
  inherited;

  if Source is TElDSAEncryptionParameters then
  begin
    SetLength(FP, Length(TElDSAEncryptionParameters(Source).FP));
    if Length(FP) > 0 then
      Move(TElDSAEncryptionParameters(Source).FP[0], FP[0], Length(FP));
    SetLength(FQ, Length(TElDSAEncryptionParameters(Source).FQ));
    if Length(FQ) > 0 then
      Move(TElDSAEncryptionParameters(Source).FQ[0], FQ[0], Length(FQ));
    SetLength(FG, Length(TElDSAEncryptionParameters(Source).FG));
    if Length(FG) > 0 then
      Move(TElDSAEncryptionParameters(Source).FG[0], FG[0], Length(FG));
    SetLength(FY, Length(TElDSAEncryptionParameters(Source).FY));
    if Length(FY) > 0 then
      Move(TElDSAEncryptionParameters(Source).FY[0], FY[0], Length(FY));
    SetLength(FX, Length(TElDSAEncryptionParameters(Source).FX));
    if Length(FX) > 0 then
      Move(TElDSAEncryptionParameters(Source).FX[0], FX[0], Length(FX));
  end;
end;

procedure TElDSAEncryptionParameters.LoadPublicKey(Tag: TElASN1ConstrainedTag);
begin
  if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_INTEGER, false)) then
  begin
    FY := BytesOfString(TElASN1SimpleTag(Tag.GetField(0)).Content);
  end
  else
    raise EElCertificateException.Create(SInvalidtbsCert);
end;

////////////////////////////////////////////////////////////////////////////////
// TElRSAEncryptionParameters class

procedure TElDHEncryptionParameters.Assign(Source : TElEncryptionParameters);
begin
  inherited;

  if Source is TElDHEncryptionParameters then
  begin
    SetLength(FP, Length(TElDHEncryptionParameters(Source).FP));
    if Length(FP) > 0 then
      Move(TElDHEncryptionParameters(Source).FP[0], FP[0], Length(FP));
    SetLength(FQ, Length(TElDHEncryptionParameters(Source).FQ));
    if Length(FQ) > 0 then
      Move(TElDHEncryptionParameters(Source).FQ[0], FQ[0], Length(FQ));
    SetLength(FG, Length(TElDHEncryptionParameters(Source).FG));
    if Length(FG) > 0 then
      Move(TElDHEncryptionParameters(Source).FG[0], FG[0], Length(FG));
    SetLength(FY, Length(TElDHEncryptionParameters(Source).FY));
    if Length(FY) > 0 then
      Move(TElDHEncryptionParameters(Source).FY[0], FY[0], Length(FY));
    SetLength(FX, Length(TElDHEncryptionParameters(Source).FX));
    if Length(FX) > 0 then
      Move(TElDHEncryptionParameters(Source).FX[0], FX[0], Length(FX));
  end;
end;

procedure TElDHEncryptionParameters.LoadPublicKey(Tag: TElASN1ConstrainedTag);
begin
  if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_INTEGER, false)) then
  begin
    FY := BytesOfString(TElASN1SimpleTag(Tag.GetField(0)).Content);
  end
  else
    raise EElCertificateException.Create(SInvalidtbsCert);
end;

////////////////////////////////////////////////////////////////////////////////
// TElSubjectPublicKeyInfo class

constructor TElSubjectPublicKeyInfo.Create;
begin
  inherited;
  FEncryptionParameters := nil;
end;

destructor TElSubjectPublicKeyInfo.Destroy;
begin
  FreeAndNil(FEncryptionParameters);
  inherited;
end;

procedure TElSubjectPublicKeyInfo.Clear;
begin
  if Assigned(FEncryptionParameters) then
    FreeAndNil(FEncryptionParameters);
end;

////////////////////////////////////////////////////////////////////////////////
// TElTBSCertificate class

constructor TElTBSCertificate.Create;
begin
  inherited;
  FIssuer := TStringList.Create;
  FSubject := TStringList.Create;
  FSubjectPublicKeyInfo := TElSubjectPublicKeyInfo.Create;
  FSignature := nil;
end;

destructor TElTBSCertificate.Destroy;
begin
  FreeAndNil(FIssuer);
  FreeAndNil(FSubject);
  FreeAndNil(FSubjectPublicKeyInfo);
  if Assigned(FSignature) then
    FreeAndNil(FSignature);
  inherited;
end;

procedure TElTBSCertificate.Clear;
begin
  FIssuer.Clear;
  FSubject.Clear;
  FVersion := 0;
  SetLength(FSerialNumber, 0);
  if Assigned(FSignature) then
    FreeAndNil(FSignature);
  FSubjectPublicKeyInfo.Clear;
  SetLength(FIssuerUniqueID, 0);
  SetLength(FSubjectUniqueID, 0);
end;

(*
function DetectCertFileFormat(FileName: string): TSBCertFileFormat; {$ifndef CHROME}overload;{$endif}
begin
  result := TElX509Certificate.DetectCertFileFormat(FileName);
end;

function DetectCertFileFormat(Stream: {$IFNDEF DELPHI_NET}TStream{$ELSE}System.IO.Stream{$ENDIF}): TSBCertFileFormat; {$ifndef CHROME}overload;{$endif}
begin
  result := TElX509Certificate.DetectCertFileFormat(nil, Stream);
end;

function DetectKeyFileFormat(FileName: string; const Password: string): TSBX509KeyFileFormat; {$ifndef CHROME}overload;{$endif}
begin
  result := TElX509Certificate.DetectKeyFileFormat(nil, FileName);
end;

function DetectKeyFileFormat(Stream: {$IFNDEF DELPHI_NET}TStream{$ELSE}System.IO.Stream{$ENDIF}; const Password: string): TSBX509KeyFileFormat; {$ifndef CHROME}overload;{$endif}
begin
  result := TElX509Certificate.DetectKeyFileFormat(nil, Stream);
end;
*)
end.


