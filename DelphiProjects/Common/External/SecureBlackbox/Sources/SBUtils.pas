(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}



unit SBUtils;

interface

uses
  SysUtils,
  Classes,
  {$ifndef LINUX}
  Windows,
  {$endif}



    {$ifdef KYLIX_USED}
    {$endif}

    {$ifndef KYLIX_USED}

      SyncObjs,
    {$else}
      SyncObjs,
      Libc,
    {$endif}
  {$ifndef CUSTOM_ZLIB}
  SBMath
  {$endif}
  ;


const
  BufferSize = 1023;

  BufferTypeStartOffset = 1;
  BufferTypeStartInvOffset = 0;
  AnsiStrOffset = 1;


  MaxArrSize = $7FFFFFFF;

type
  TSBControlBase = TComponent;
  TSBComponentBase = TComponent;


  TElStringList = class(TStringList);


  ByteArray = array of byte;

  WordArray =  array of word;
  CharArray =  array of char;


  BufferType = string;
  BufferElementType = char;


  TByteArray =  array[0..MaxArrSize shr 1] of byte;
  // PByteArray = ^TByteArray;

  TWordArray =  array[0..MaxArrSize shr 2] of word;
  // PWordArray = ^TWordArray;

  TLongWordArray =  array[0..MaxArrSize shr 3] of longword;
  PLongWordArray = ^TLongWordArray;

  TInt64Array =  array[0..MaxArrSize shr 4] of int64;
  PInt64Array = ^TInt64Array;

  {$EXTERNALSYM WCHAR}
  WCHAR =  WideChar;

  {$EXTERNALSYM DWORD}
  DWORD =  LongWord;

  {$EXTERNALSYM BOOL}
  BOOL = LongBool;

  {$EXTERNALSYM UCHAR}
  UCHAR =  Byte;

  {$EXTERNALSYM SHORT}
  SHORT =  Smallint;


  {$EXTERNALSYM UINT}
  UINT =  Cardinal;

  {$EXTERNALSYM ULONG}
  ULONG =  LongWord;

  {$EXTERNALSYM LCID}
  LCID =  DWORD;

  {$EXTERNALSYM LANGID}
  LANGID =  Word;

  THandle = cardinal;

  PByte = ^Byte;
  PWord = ^Word;
  PInt  = ^Integer;
  PLongWord = ^LongWord;
  PLongint = ^Longint;
  PInt64 = ^Int64;
  PCardinal = ^cardinal;

  // we define PtrInt and PtrUInt for easier porting to 64-bit platforms, starting from FreePascal
  {$ifdef SB_CPU32}
  {$ifndef FPC}
  PtrInt  =  LongInt;
  PtrUInt =  LongWord;
  {$endif}
  TPtrHandle =  Cardinal;
  {$endif}

  {$ifdef SB_CPU64}
  {$ifndef FPC}
  PtrInt  =  Int64;
  PtrUInt =  Int64;
  {$endif}
  TPtrHandle =  Int64;
  {$endif}

  

  TSBProgressEvent =  procedure(
    Sender : TObject;
    Total, Current : Int64; var Cancel : boolean) of object;

  TElMessageLoopEvent =  function: boolean of object;

  EEncryptionError  =  class(Exception);

  ESecureBlackboxError = class (Exception)
  private
    FCode: integer;
  public
    constructor Create(Message: string); overload;
    constructor Create(Message: string; Code: integer {$ifndef FPC}; Fake: integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}{$endif}); overload;
    property ErrorCode: integer read FCode;
  end;

  ECertificateError =  class(ESecureBlackboxError);
  ECertStorageError =  class(ESecureBlackboxError);
  EOIDError =  class(ESecureBlackboxError);
  EDuplicateCertError =  class(ECertStorageError);
  ECertNotFoundError =  class(ECertStorageError);

  EOperationCancelledError =  class(ESecureBlackboxError);


  TMessageDigest128 = packed  record
    A, B, C, D: longword;
  end;

  TMessageDigest160 = packed  record
    A, B, C, D, E: longword;
  end;

  TMessageDigest224 = packed  record
    A1, B1, C1, D1: longword;
    A2, B2, C2: longword;
  end;


  TMessageDigest256 = packed  record
    A1, B1, C1, D1: longword;
    A2, B2, C2, D2: longword;
  end;

  TMessageDigest320 = packed  record
    A1, B1, C1, D1, E1: longword;
    A2, B2, C2, D2, E2: longword;
  end;

  TMessageDigest384 = packed  record
    A, B, C, D, E, F: int64;
  end;

  TMessageDigest512 = packed  record
    A1, B1, C1, D1: int64;
    A2, B2, C2, D2: int64;
  end;

  TSBLongwordPair =  record
    A, B : longword;
  end;

  TSBArrayOfPairs =  array of TSBLongwordPair;
  PSBArrayOfPairs = ^TSBArrayOfPairs;

// Converting functions
function DigestToStr(const Digest: TMessageDigest128;
  LowerCase: boolean = true): string; overload;
function DigestToStr(const Digest: TMessageDigest160;
  LowerCase: boolean = true): string; overload;
function DigestToStr(const Digest: TMessageDigest224;
  LowerCase: boolean = true): string; overload;
function DigestToStr(const Digest: TMessageDigest256;
  LowerCase: boolean = true): string; overload;
function DigestToStr(const Digest: TMessageDigest320;
  LowerCase: boolean = true): string; overload;
function DigestToStr(const Digest: TMessageDigest384;
  LowerCase: boolean = true): string; overload;
function DigestToStr(const Digest: TMessageDigest512;
  LowerCase: boolean = true): string; overload;
function StrToDigest(const DigestStr : string;
  var Digest : TMessageDigest128) : boolean; overload;
function StrToDigest(const DigestStr : string;
  var Digest : TMessageDigest160) : boolean; overload;

function DigestToBinary(const Digest : TMessageDigest128) : string; overload;
function DigestToBinary(const Digest : TMessageDigest160) : string; overload;

{$ifndef CUSTOM_ZLIB}
procedure PointerToLInt(var B: PLint; P: Pointer; Size: LongInt);
procedure LIntToPointer(B: PLInt; P: Pointer; var Size: LongInt);


{$endif}

function BeautifyBinaryString(const Str : string; Separator : Char): string; 

function SwapInt32(value : integer) : integer; 
function SwapUInt32(value : LongWord) : LongWord; function SwapInt64(value : Int64) : Int64; function SwapSomeInt(Value : integer) : BufferType; 
function RotateInteger(const Value: BufferType) : BufferType; 


procedure SwapBigEndianWords(P: Pointer; Size: LongInt);
// Mathematical routines
function Min(const A, B: integer): integer; overload;
function Min(const A, B: cardinal): cardinal; overload;function Min(const A, B: Int64): Int64; overload;   // in D6, D7, 8 "TStream.Size" declared as "Int64".

function Max(const A, B: integer): integer; overload;
function Max(const A, B: cardinal): cardinal; overload;function Max(const A, B: Int64): Int64; overload;  // in D6, D7, 8 "TStream.Size" declared as "Int64".


function Base64Decode(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
  var OutSize : integer) : integer;
function Base64Encode(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
  var OutSize : integer; WrapLines : boolean = true) : boolean;


function CompareMem(const Mem1: Pointer; const Mem2: Pointer; Size : integer): Boolean;  overload;
function CompareMem(const Mem1: ByteArray; const Mem2: AnsiString): Boolean;  overload;
function CompareMem(const Mem1: ByteArray; const Mem2: ByteArray) : Boolean; overload;function CompareMem(const Mem1: ByteArray; Offset1 : integer; const Mem2: ByteArray; Offset2 : integer) : Boolean; overload;function CompareMem(const Mem1: ByteArray; Offset1 : integer; const Mem2: ByteArray; Offset2 : integer; Size : integer) : Boolean; overload;
function CompareMD128(const M1, M2 : TMessageDigest128) : boolean; 
function CompareMD160(const M1, M2 : TMessageDigest160) : boolean; 

procedure FreeAndNil(var Obj); 

{$ifndef CUSTOM_ZLIB}
function GetPBEAlgorithmByOID(const OID: BufferType): Integer; 
function GetOIDByPBEAlgorithm(Algorithm: Integer): BufferType; 
function GetPKAlgorithmByOID(const OID: BufferType): Integer; 
function GetOIDByPKAlgorithm(Algorithm : Integer): BufferType; 
function GetSigAlgorithmByOID(const OID: BufferType): integer; 
function GetOIDBySigAlgorithm(Algorithm: integer): BufferType; 
function GetHashAlgorithmByOID(const OID : BufferType) : integer; 
function GetOIDByHashAlgorithm(Algorithm : integer) : BufferType; 
function GetAlgorithmByOID(const OID : BufferType) : integer; 
function GetOIDByAlgorithm(Algorithm : integer) : BufferType; 

function GetDigestSizeBits(Algorithm : integer) : integer; 
{$endif}

function CompareAnsiStr(const Content, OID : AnsiString) : boolean; 


function BinaryToString(Buffer : Pointer; BufSize : integer): string;

function StringToBinary(const S: string; Buffer : pointer; var Size: integer) : boolean;

function CompareGUID(const Guid1, Guid2 : TGUID) : boolean;

function StringOfBytes(const Src : ByteArray) : string; overload;
function StringOfBytes(Src: ByteArray; ALow: Integer; ALen: integer): string; overload;
function BytesOfString(const Str : string) : ByteArray;

function WideStrToUTF8(const AStr: WideString) : ByteArray; overload;
function WideStrToUTF8(const ASrc; Size: integer) : ByteArray; overload;
function UTF8ToWideStr(const Buf: ByteArray): WideString; overload;
function UTF8ToWideStr(const Buf; Size: Integer): WideString; overload;


function EmptyBuffer: BufferType; 
function EmptyArray: ByteArray;


// not used
function SBConcatBuffers(const Buf1, Buf2 : BufferType): BufferType; overload;function SBConcatBuffers(const Buf1, Buf2 : ByteArray): ByteArray; overload;


function CloneBuffer(const Arr : string) : string;

function UTCTimeToDate(const UTCTime: string): TDateTime; 
function UTCTimeToTime(const UTCTime: string): TDateTime; 
function UTCTimeToDateTime(const UTCTime: string): TDateTime; 
function GeneralizedTimeToDate(const GenTime: string): TDateTime; 
function GeneralizedTimeToTime(const GenTime: string): TDateTime; 
function GeneralizedTimeToDateTime(const GenTime: string): TDateTime; 
function DateTimeToUTCTime(const ADateTime : TDateTime) : string; 
function DateTimeToGeneralizedTime(const ADateTime : TDateTime) : string; 

{$ifndef HAS_DEF_PARAMS}
function SBPos(const SubP : ByteArray; const P : ByteArray) : integer; overload;function SBPos(SubP : string; const P : ByteArray) : integer; overload;{$endif}
function SBPos(const SubP : ByteArray; const P : ByteArray; StartPos : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}) : integer; overload;function SBPos(const SubP : string; const P : ByteArray; StartPos : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}) : integer; overload;

function SBCopy(const str: ByteArray; Offset, Size : integer): ByteArray; overload;
function OIDToStr(const OID: BufferType): String; 
function StrToOID(const Str : string) : BufferType; 

function CopyStream(SrcStream, DestStream: TStream; Offset : Int64; Count : Int64;
  PreservePosition : boolean = true): Int64;


{$ifndef VCL50}
function SameText(const S1, S2: string): Boolean;
{$endif}



function TickDiff(First, Second : integer) : integer; 

function SBTrim(const S : ByteArray) : ByteArray; 

function SBUppercase(const S : ByteArray) : ByteArray;  

function GenerateGUID: string; 

{$ifdef LINUX}
function GetTickCount: integer;
{$endif}

function WaitFor(Handle : THandle): LongWord;

{$ifdef NET_CF}
const
  COREDLL = 'coredll.dll';
  OLE32   = 'ole32.dll';

function StringToCoTaskMemUni(const s: string): IntPtr; 
function PtrToStringUni(Ptr: IntPtr): string; 

function UnsafeAddrOfPinnedArrayElement(ArrPin: GCHandle): IntPtr; 
{$endif}

{$ifdef NET_CF_1_0}
function LocalAlloc(Flags: DWORD; Size: Integer): IntPtr; 
procedure LocalFree(Ptr: IntPtr); 

function AllocCoTaskMem(Size: Integer): IntPtr; 
procedure FreeCoTaskMem(Ptr: IntPtr); 

function DateTimeToOADate(const DateTime: System.DateTime): Double; 
function DateTimeFromOADate(const d: Double): System.DateTime; 
{$endif}

type

{$IFNDEF VCL50}
  TListNotification = (lnAdded, lnExtracted, lnDeleted);
{$ENDIF}

{$WARNINGS OFF}

  TSBObjectList = class(TList)
  private
    FOwnsObjects: Boolean;
  protected
    procedure Notify(Ptr: Pointer; Action: TListNotification);
//{$ifdef FPC}override;{$endif}
{$IFDEF VCL50} override;
{$ELSE} {$ifndef FPC}virtual;{$endif}
{$ENDIF}
    function GetItem(Index: Integer): TObject;
    procedure SetItem(Index: Integer; AObject: TObject);
  public
    constructor Create; overload;
    constructor Create(AOwnsObjects: Boolean); overload;
    function Add(AObject: TObject): Integer;
    function Remove(AObject: TObject): Integer;
    function IndexOf(AObject: TObject): Integer;
    procedure Insert(Index: Integer; AObject: TObject);
    function FindInstanceOf(AClass: TClass ; AExact: Boolean {$ifdef HAS_DEF_PARAMS}= true{$endif};
      AStartAt: Integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}): Integer;
    function Extract(Item: TObject): TObject;
    property OwnsObjects: Boolean read FOwnsObjects write FOwnsObjects;
    property Items[Index: Integer]: TObject read GetItem write SetItem; default;
  end;


{$WARNINGS ON}

type
  UTF32 =  Longword;
  UTF16 =  Word;
  UTF8  =  Byte;

  pUTF32 = ^UTF32;
  pUTF16 = ^UTF16;
  pUTF8  = ^UTF8;

  ConversionResult =  (
    conversionOK,       { conversion successful }
    sourceExhausted,    { partial character in source, but hit end }
    targetExhausted,    { insuff. room in target for conversion }
    sourceIllegal       { source sequence is illegal/malformed }
  );


const
  UTF8BOM: string = char($ef)+char($bb)+char($bf);

const
  LowerAlphabet = '0123456789abcdef';
  UpperAlphabet = '0123456789ABCDEF';


const
  UNI_REPLACEMENT_CHAR: UTF32 = $0000FFFD;
  UNI_MAX_BMP: UTF32 = $0000FFFF;
  UNI_MAX_UTF16: UTF32 = $0010FFFF;
  UNI_MAX_UTF32: UTF32 = $7FFFFFFF;
  halfShift: integer = 10;      { used for shifting by 10 bits }
  halfBase: UTF32 = $0010000;
  halfMask: UTF32 = $3FF;
  UNI_SUR_HIGH_START: UTF32 = $0D800;
  UNI_SUR_HIGH_END: UTF32 = $0DBFF;
  UNI_SUR_LOW_START: UTF32 = $0DC00;
  UNI_SUR_LOW_END: UTF32 = $0DFFF;

{
  Index into the table below with the first byte of a UTF-8 sequence to
  get the number of trailing bytes that are supposed to follow it.
}

  trailingBytesForUTF8: array [0..255] of byte =
    (
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
        2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, 3,3,3,3,3,3,3,3,4,4,4,4,5,5,5,5
    );


{ Constants }

type

  TOIDEx =  array[0..2] of Byte;
  TAlOID =  array[0..127] of Byte;

  TExtOID =  record
    OID: TOIDEx;
    Name: string;
  end;

const
  //error code
  SizeError = $1111;

  // tags (IDs) for data objects

  SIZE128 = $81 - 1;
  // data blocks IDs (for Synchronization)
  xBLOCKID_0 = $A0;
  xBLOCKID_1 = $A1;
  xBLOCKID_2 = $A2;
  xBLOCKID_3 = $A3;
  xBLOCK_SIZE_128 = $81;

  xMARKEREND = $00;
  xBOOL = $01;

  // data type - integer
  xINTEGER = $02;
  xINTEGER_SIZE_128 = $81;

  // data type - string of bit
  xBITSTRING = $03;
  xBITSTRING_SIZE_128 = $81;

  xOCTETSTRING = $04;
  xANYDATA = $05;
  xNULL = $05;

  xOID = $06;
  xUTF8STRING = $0C;
  xPRINTABLESTRING = $13;
  xUNKNOWNSTRING1 = $14;
  xIASTRING = $16;
  xUTCTime = $17;

  // data type - record
  xSEQUENCE = $30;
  xSEQUENCE_SIZE_UNDEFINED = $80;
  xSEQUENCE_SIZE_128 = $81;

  xSET = $31;

  // Extension OID
  xauthorityKeyIdentifier: TOIDEx =
  ($55, $1D, $23);
  xsubjectKeyIdentifier: TOIDEx =
  ($55, $1D, $0E);
  xkeyUsage: TOIDEx =
  ($55, $1D, $0F);
  xextendedKeyUsage: TOIDEx =
  ($55, $1D, $25);
  xprivateKeyUsagePeriod: TOIDEx =
  ($55, $1D, $10);
  xcertificatePolicies: TOIDEx =
  ($55, $1D, $20);
  xpolicyMappings: TOIDEx =
  ($55, $1D, $21);
  xsubjectAltName: TOIDEx =
  ($55, $1D, $11);
  xissuerAltName: TOIDEx =
  ($55, $1D, $12);
  xbasicConstraints: TOIDEx =
  ($55, $1D, $13);
  xnameConstraints: TOIDEx =
  ($55, $1D, $1E);
  xpolicyConstraints: TOIDEx =
  ($55, $1D, $24);
  xcRLDistributionPoints: TOIDEx =
  ($55, $1D, $1F);
  xsubjectDirectoryAttributes: TOIDEx =
  ($55, $1D, $09);
  xauthorityInfoAccess: TOIDEx =
  ($55, $1D, $01);

const
  SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION     = Integer($0000);
  SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION    = Integer($0001);
  SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION    = Integer($0002);
  SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION   = Integer($0003);
  SB_CERT_ALGORITHM_ID_DSA                = Integer($0004);
  SB_CERT_ALGORITHM_ID_DSA_SHA1           = Integer($0005);
  SB_CERT_ALGORITHM_DH_PUBLIC             = Integer($0006);
  SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION = Integer($0007);
  SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION = Integer($0008);
  SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION = Integer($0009);
  SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION = Integer($000A);
  SB_CERT_ALGORITHM_ID_RSAPSS             = Integer($000B);
  SB_CERT_ALGORITHM_ID_RSAOAEP            = Integer($000C);
  SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160= Integer($000D);

  SB_CERT_MGF1                            = Integer($0201);
  SB_CERT_MGF1_SHA1                       = Integer($0202);

  SB_CERT_ALGORITHM_UNKNOWN               = Integer($0000FFFF);

type
  TBufferTypeConst = AnsiString; // == BufferType

// not usage
//function OIDToContent(const OID: string) : BufferType;
function CompareContent(const Content, OID: BufferType): Boolean; overload;
const

  SB_CERT_OID_COMMON_NAME          :TBufferTypeConst = #$55#$04#$03; 
  SB_CERT_OID_SURNAME              :TBufferTypeConst = #$55#$04#$04; 
  SB_CERT_OID_COUNTRY              :TBufferTypeConst = #$55#$04#$06; 
  SB_CERT_OID_LOCALITY             :TBufferTypeConst = #$55#$04#$07; 
  SB_CERT_OID_STATE_OR_PROVINCE    :TBufferTypeConst = #$55#$04#$08; 
  SB_CERT_OID_ORGANIZATION         :TBufferTypeConst = #$55#$04#$0A; 
  SB_CERT_OID_ORGANIZATION_UNIT    :TBufferTypeConst = #$55#$04#$0B; 
  SB_CERT_OID_TITLE                :TBufferTypeConst = #$55#$04#$0C; 
  SB_CERT_OID_NAME                 :TBufferTypeConst = #$55#$04#$29; 
  SB_CERT_OID_GIVEN_NAME           :TBufferTypeConst = #$55#$04#$2A; 
  SB_CERT_OID_INITIALS             :TBufferTypeConst = #$55#$04#$2B; 
  SB_CERT_OID_GENERATION_QUALIFIER :TBufferTypeConst = #$55#$04#$2C; 
  SB_CERT_OID_DN_QUALIFIER         :TBufferTypeConst = #$55#$04#$2E; 
  SB_CERT_OID_EMAIL                :TBufferTypeConst =#$2a#$86#$48#$86#$f7#$0d#$01#$09#$01; 

  SB_CERT_OID_STREET_ADDRESS       :TBufferTypeConst =#$55#$04#$09; 
  SB_CERT_OID_POSTAL_ADDRESS       :TBufferTypeConst =#$55#$04#$10; 
  SB_CERT_OID_POSTAL_CODE          :TBufferTypeConst =#$55#$04#$11; 
  SB_CERT_OID_POST_OFFICE_BOX      :TBufferTypeConst =#$55#$04#$12; 
  SB_CERT_OID_PHYSICAL_DELIVERY_OFFICE_NAME :TBufferTypeConst = #$55#$04#$13; 
  SB_CERT_OID_TELEPHONE_NUMBER     :TBufferTypeConst = #$55#$04#$14; 
  SB_CERT_OID_TELEX_NUMBER         :TBufferTypeConst = #$55#$04#$15; 
  SB_CERT_OID_TELEX_TERMINAL_ID    :TBufferTypeConst = #$55#$04#$16; 
  SB_CERT_OID_FACIMILE_PHONE_NUMBER:TBufferTypeConst = #$55#$04#$17; 

  SB_CERT_OID_X12_ADDRESS          :TBufferTypeConst = #$55#$04#$18; 
  SB_CERT_OID_INTERNATIONAL_ISDN_NUMBER :TBufferTypeConst = #$55#$04#$19; 
  SB_CERT_OID_REGISTERED_ADDRESS   :TBufferTypeConst = #$55#$04#$1A; 
  SB_CERT_OID_DESTINATION_INDICATOR:TBufferTypeConst = #$55#$04#$1B; 
  SB_CERT_OID_PREFERRED_DELIVERY_METHOD :TBufferTypeConst = #$55#$04#$1C; 
  SB_CERT_OID_PRESENTATION_ADDRESS :TBufferTypeConst = #$55#$04#$1D; 
  SB_CERT_OID_SUPPORTED_APPLICATION_CONTEXT :TBufferTypeConst = #$55#$04#$1E; 
  SB_CERT_OID_MEMBER            :TBufferTypeConst = #$55#$04#$1F; 
  SB_CERT_OID_OWNER             :TBufferTypeConst = #$55#$04#$20; 
  SB_CERT_OID_ROLE_OCCUPENT        :TBufferTypeConst = #$55#$04#$21; 
  SB_CERT_OID_SEE_ALSO             :TBufferTypeConst = #$55#$04#$22; 
  SB_CERT_OID_USER_PASSWORD        :TBufferTypeConst = #$55#$04#$23; 
  SB_CERT_OID_USER_CERTIFICATE     :TBufferTypeConst = #$55#$04#$24; 
  SB_CERT_OID_CA_CERTIFICATE       :TBufferTypeConst = #$55#$04#$25; 
  SB_CERT_OID_AUTHORITY_REVOCATION_LIST :TBufferTypeConst = #$55#$04#$26; 
  SB_CERT_OID_CERTIFICATE_REVOCATION_LIST:TBufferTypeConst = #$55#$04#$27; 
  SB_CERT_OID_CERTIFICATE_PAIR     :TBufferTypeConst = #$55#$04#$28; 
  SB_CERT_OID_UNIQUE_IDENTIFIER    :TBufferTypeConst = #$55#$04#$2D; 
  SB_CERT_OID_ENHANCED_SEARCH_GUIDE:TBufferTypeConst = #$55#$04#$2F; 

  SB_CERT_OID_OBJECT_CLASS         :TBufferTypeConst = #$55#$04#$00; 
  SB_CERT_OID_ALIASED_ENTRY_NAME   :TBufferTypeConst = #$55#$04#$01; 
  SB_CERT_OID_KNOWLEDGE_INFORMATION:TBufferTypeConst = #$55#$04#$02; 
  SB_CERT_OID_SERIAL_NUMBER        :TBufferTypeConst = #$55#$04#$05; 
  SB_CERT_OID_DESCRIPTION          :TBufferTypeConst = #$55#$04#$0D; 
  SB_CERT_OID_SEARCH_GUIDE         :TBufferTypeConst = #$55#$04#$0E; 
  SB_CERT_OID_BUSINESS_CATEGORY    :TBufferTypeConst = #$55#$04#$0F; 
  SB_CERT_OID_PROTOCOL_INFORMATION :TBufferTypeConst = #$55#$04#$30; 
  SB_CERT_OID_DISTINGUISHED_NAME   :TBufferTypeConst = #$55#$04#$31; 
  SB_CERT_OID_UNIQUE_MEMBER        :TBufferTypeConst = #$55#$04#$32; 
  SB_CERT_OID_HOUSE_IDENTIFIER     :TBufferTypeConst = #$55#$04#$33; 
  SB_CERT_OID_SUPPORTED_ALGORITHMS :TBufferTypeConst = #$55#$04#$34; 
  SB_CERT_OID_DELTA_REVOCATION_LIST:TBufferTypeConst = #$55#$04#$35; 
  SB_CERT_OID_ATTRIBUTE_CERTIFICATE:TBufferTypeConst = #$55#$04#$3A; 
  SB_CERT_OID_PSEUDONYM            :TBufferTypeConst = #$55#$04#$41; 

  SB_CERT_OID_PERMANENT_IDENTIFIER :TBufferTypeConst = #$2b#$06#$01#$05#$05#$07#$00#$12#$08#$03; 

  SB_CERT_OID_CA_ISSUER            :TBufferTypeConst = #$2b#$06#$01#$05#$05#$07#$30#$02; 

  SB_CERT_OID_RSAENCRYPTION        :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$01; 
  SB_CERT_OID_RSAOAEP              :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$07; 
  SB_CERT_OID_RSAPSS               :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$0A; 
  SB_CERT_OID_DSA                  :TBufferTypeConst = #$2A#$86#$48#$CE#$38#$04#$01; 
  SB_CERT_OID_DH                   :TBufferTypeConst = #$2A#$86#$48#$CE#$3E#$02#$01; 
  SB_CERT_OID_DSA_SHA1             :TBufferTypeConst = #$2A#$86#$48#$CE#$38#$04#$03; 
  SB_CERT_OID_MD2_RSAENCRYPTION    :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$02; 
  SB_CERT_OID_MD5_RSAENCRYPTION    :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$04; 
  SB_CERT_OID_SHA1_RSAENCRYPTION   :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$05; 
  SB_CERT_OID_SHA224_RSAENCRYPTION :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$0E; 
  SB_CERT_OID_SHA256_RSAENCRYPTION :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$0B; 
  SB_CERT_OID_SHA384_RSAENCRYPTION :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$0C; 
  SB_CERT_OID_SHA512_RSAENCRYPTION :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$0D; 

  SB_CERT_OID_SHA1_RSA             :TBufferTypeConst = #$2B#$0E#$03#$02#$1D; 
  SB_CERT_OID_SHA1_DSA             :TBufferTypeConst = #$2A#$86#$48#$CE#$38#$04#$03; 
  SB_CERT_OID_SHA1                 :TBufferTypeConst = #$2B#$0E#$03#$02#$1A; 
  SB_CERT_OID_MD2                  :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$02#$02; 
  SB_CERT_OID_MD5                  :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$02#$05; 

const

{
  Magic values subtracted from a buffer value during UTF8 conversion.
  This table contains as many values as there might be trailing bytes
  in a UTF-8 sequence.
}
  offsetsFromUTF8: array [0..5] of UTF32 =
	(
	$00000000, $00003080, $000E2080,
	$03C82080, $0FA082080, $82082080
	);

  firstByteMark: array [0..6] of UTF8 =
	(
	$00, $00, $0c0, $0e0, $0f0, $0f8, $0fc
	);

type

  ConversionFlags = 
    (strictConversion, lenientConversion ); { strictConversion = 0 }

const
  EncArr : array[0..63] of char
   = (
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
    'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1',
    '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
  );

  base64PadByte = Byte($3D);


  SpaceByteArray : AnsiString = ' ';
  ComaByteArray : AnsiString = ',';
  LFByteArray : AnsiString = #$0A;
  LFLFByteArray : AnsiString = #$0A#$0A;
  CRLFByteArray : AnsiString = #$0D#$0A;
  CRLFCRLFByteArray : AnsiString = #$0D#$0A#$0D#$0A;
  CRCRLFByteArray : AnsiString = #$0D#$0D#$0A;
  CRCRLFCRCRLFByteArray : AnsiString = #$0D#$0D#$0A#$0D#$0D#$0A;
  base64Pad : char = '=';
  Line : AnsiString = '-----';
  BeginLine : AnsiString = '-----BEGIN ';
  EndLine : AnsiString = #$0A'-----END ';
  CRLFStr = #13#10;
  LFStr = #10;
  CRLFCRLFZero = #13#10#13#10#0;

  BASE64_DECODE_OK                      = 0;
  BASE64_DECODE_INVALID_CHARACTER       = 1;
  BASE64_DECODE_NOT_ENOUGH_SPACE        = 3;

function ConvertUTF16toUTF8(const source: WideString;
                        var target: AnsiString;
                        flags: ConversionFlags;
                        BOM: boolean): ConversionResult;

function isLegalUTF8(source: string; sourcelen: cardinal): boolean;

function ConvertUTF8toUTF16(const source: string;
                             var target: WideString;
                             flags: ConversionFlags;
                             BOM: boolean): ConversionResult;

function ConvertFromUTF8String(const Source : string; CheckBOM : boolean = true) : WideString;
function ConvertToUTF8String(const Source : WideString) : string;

resourcestring

  SInvalidInputSize = 'Invalid input block size';
  SInvalidKeySize = 'Invalid key size [%d bits]';
  SInvalidOID = 'Invalid object identifier';

  SOperationCancelled = 'Synchronous operation has been cancelled';

  SLicenseKeyNotSet = 'SecureBlackbox license key is not set. Please pass your (or evaluation) license key to SBUtils.SetLicenseKey function in initialization section of your application.';
  SOldLicenseKey = 'Provided license key is valid for old version of SecureBlackbox and not the current one. Please upgrade your license.';
  SUnknownLicenseKey = 'Provided license key is valid for version of SecureBlackbox, other than current one. Please check the version of SecureBlackbox and your license.';
  SLicenseKeyExpired = 'SecureBlackbox license key has expired. Please use evaluation license key to continue evaluation.';


procedure SetLicenseKey(const Key : string);
procedure CheckLicenseKey();


{$ifndef CUSTOM_ZLIB}
{ Random wrappers }
procedure SBRndInit; 
procedure SBRndCreate; 
procedure SBRndDestroy; 
procedure SBRndSeed(const Salt : string {$ifdef HAS_DEF_PARAMS}= ''{$endif}); overload;procedure SBRndSeed(Buffer: pointer; Size: integer); overload;
procedure SBRndSeedTime; 
procedure SBRndGenerate(Buffer: pointer; Size: integer); overload;
function SBRndGenerate(UpperBound: cardinal {$ifdef HAS_DEF_PARAMS}= 0{$endif}): cardinal; overload;procedure SBRndGenerateLInt(A : PLInt; Bytes : integer); 
{$endif}


implementation

{$ifndef CUSTOM_ZLIB}
uses
  SBConstants,
  SBRandom,
  SBMD,
  SBSHA
  ;
  {$endif}

  {$ifndef CUSTOM_ZLIB}
var
  SBRndCreated : boolean = false;
  FGlobalRandom : TElRandom = nil;
  {$ifndef NET_CF}
  FRndCriticalSection : TCriticalSection;
  {$else}
  // use Monitor.Enter/Exit(FGlobalRandom)
  {$endif}
  {$endif}


constructor ESecureBlackboxError.Create(Message: string);
begin
  inherited Create(Message);
  FCode := 0;
end;

constructor ESecureBlackboxError.Create(Message: string; Code: integer{$ifndef FPC}; Fake: integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}{$endif});
begin
  inherited Create(Message + ' (error code is ' + IntToStr(Code) + ')');

  FCode := Code;
end;

function TickDiff(First, Second : integer) : integer;
begin
  if Second > First then
    result := Second - First
  else
  if Second < First then
    result := Second + ($7FFFFFFF - First) + 1
  else
    result := 0;
end;

{$IFDEF LINUX}
function GetTickCount: integer;
var
  f: text;
  up: double;
begin
  assignfile(f, '/proc/uptime');
  reset(f);
  read(f, up);
  closefile(f);
  result := trunc(up * 1000);
end;
{$ENDIF}


function StringOfBytes(const Src : ByteArray) : string;
begin
  SetLength(Result, Length(Src));
  Move(Src[0], Result[1], Length(Result));
end;

function StringOfBytes(Src: ByteArray; ALow: Integer; ALen: integer): string;
var ToCopy : integer;
begin
  ToCopy := Min(ALen, Length(Src) - ALow);
  SetLength(Result, ToCopy);
  Move(Src[ALow], Result[1], ToCopy);
end;

function BytesOfString(const Str : string) : ByteArray;
begin
  SetLength(Result, Length(Str));
  Move(Str[1], Result[0], Length(Result));
end;



function CloneBuffer(const Arr : string) : string;
begin
  result := Arr;
end;

function SwapInt32(value : integer) : integer;
begin
  result := (((value) shl 24) or (((value) shl 8) and $00ff0000) or (((value) shr 8) and $0000ff00) or ((value) shr 24));
end;

function SwapUInt32(value : LongWord) : LongWord;
begin
  result := (((value) shl 24) or (((value) shl 8) and $00ff0000) or (((value) shr 8) and $0000ff00) or ((value) shr 24));
end;


function SwapInt64(value : Int64) : Int64;
begin
  result := Int64(SwapUInt32(value shr 32)) or (Int64(SwapUInt32(value and $ffffffff)) shl 32);
end;


function SwapSomeInt(Value : integer) : BufferType;
const ofs = 1;
begin
  if Value > $FFFFFF then
  begin
    SetLength(Result, 4);
    Result[ofs + 1] := BufferElementType((Value shr 24) and $FF);
    Result[ofs + 1] := BufferElementType((Value shr 16) and $FF);
    Result[ofs + 2] := BufferElementType((Value shr 8) and $FF);
    Result[ofs + 3] := BufferElementType(Value and $FF);
  end
  else
  if Value > $FFFF then
  begin
    SetLength(Result, 3);
    Result[ofs] := BufferElementType((Value shr 16) and $FF);
    Result[ofs + 1] := BufferElementType((Value shr 8) and $FF);
    Result[ofs + 2] := BufferElementType(Value and $FF);
  end
  else
  if Value > $FF then
  begin
    SetLength(Result, 2);
    Result[ofs] := BufferElementType((Value shr 8) and $FF);
    Result[ofs + 1] := BufferElementType(Value and $FF);
  end
  else
  begin
    SetLength(Result, 1);
    Result[ofs] := BufferElementType(Value and $FF);
  end;
end;

function RotateInteger(const Value: BufferType) : BufferType;
var
  I, Len : integer;
begin
  Len := Length(Value);
  SetLength(Result, Len);
  for I := 1 to Len do
    Result[I] := Value[Len - I + 1];
end;

function Base64CharIndex(C : char) : byte;
begin
  case C of
    'A'..'Z' :
      Result := Ord(C) - Ord('A');
    'a'..'z' :
      Result := Ord(C) - Ord('a') + 26;
    '0'..'9' :
      Result := Ord(C) - Ord('0') + 52;
    '+' :
      Result := 62;
    '/' :
      Result := 63;
  else
    Result := $FF;
  end;
end;

function Base64Encode(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
  var OutSize : integer; WrapLines : boolean = true) : boolean;
var
  I, J, CurrentOut, EstSize : integer;
  CurrentBit, TotalBits : cardinal;
  Index, B64Index : byte;
begin
  EstSize := InSize * 4 div 3 + 4;
  if WrapLines then
    Inc(EstSize, EstSize shr 5);
  if OutSize < EstSize then
  begin
    OutSize := EstSize;
    Result := false;
    Exit;
  end;
  CurrentBit := 0;
  TotalBits := InSize shl 3;
  CurrentOut := 0;
  Result := true;
  B64Index := 0;
  while CurrentBit < TotalBits do
  begin
    I := CurrentBit shr 3;
    J := CurrentBit mod 8;
    if I >= InSize then
      Break;
    Index := ((PByteArray(InBuffer)[I] shr (7 - J)) and 1) shl 5;
    I := (CurrentBit + 1) shr 3;
    J := (CurrentBit + 1) mod 8;
    if I < InSize then
      Index := Index or ((PByteArray(InBuffer)[I] shr (7 - J)) and 1) shl 4;
    I := (CurrentBit + 2) shr 3;
    J := (CurrentBit + 2) mod 8;
    if I < InSize then
      Index := Index or ((PByteArray(InBuffer)[I] shr (7 - J)) and 1) shl 3;
    I := (CurrentBit + 3) shr 3;
    J := (CurrentBit + 3) mod 8;
    if I < InSize then
      Index := Index or ((PByteArray(InBuffer)[I] shr (7 - J)) and 1) shl 2;
    I := (CurrentBit + 4) shr 3;
    J := (CurrentBit + 4) mod 8;
    if I < InSize then
      Index := Index or ((PByteArray(InBuffer)[I] shr (7 - J)) and 1) shl 1;
    I := (CurrentBit + 5) shr 3;
    J := (CurrentBit + 5) mod 8;
    if I < InSize then
      Index := Index or ((PByteArray(InBuffer)[I] shr (7 - J)) and 1);
    PByteArray(OutBuffer)[CurrentOut] := Byte(EncArr[Index]);
    Inc(CurrentBit, 6);
    Inc(CurrentOut);
    Inc(B64Index);
    if WrapLines then
      if B64Index = 64 then
      begin
        PByteArray(OutBuffer)[CurrentOut] := $0A;
        B64Index := 0;
        Inc(CurrentOut);
      end;
  end;
  while B64Index mod 4 <> 0 do
  begin
    PByteArray(OutBuffer)[CurrentOut] := Ord(base64Pad);
    Inc(CurrentOut);
    Inc(B64Index);
  end;
  OutSize := CurrentOut;
end;

function Base64Decode(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
  var OutSize : integer) : integer;
var
  CurrentInByte : cardinal;
  CurrentOutBit : cardinal;
  I, J : integer;
  Index : byte;
  extraSyms : integer;
begin
  extraSyms := 0;
  for i := 0 to InSize - 1 do
  begin
    if (PByteArray(InBuffer)[i] = 13) or (PByteArray(InBuffer)[i] = 10) then
      inc(extraSyms);
  end;

  Result := BASE64_DECODE_OK;
  if OutSize < (InSize - extraSyms) * 3 div 4 then
  begin
    Result := BASE64_DECODE_NOT_ENOUGH_SPACE;
    OutSize := (InSize - extraSyms) * 3 div 4 + 1;
    Exit;
  end;
  FillChar(OutBuffer^, OutSize {(InSize - extraSyms) * 3 div 4}, 0);
  CurrentOutBit := 0;
  CurrentInByte := 0;
  while integer(CurrentInByte) < InSize do
  begin
    while (PByteArray(InBuffer)[CurrentInByte] = 13) or (PByteArray(InBuffer)[CurrentInByte] = 10) do
      Inc(CurrentInByte);
    if PByteArray(InBuffer)[CurrentInByte] = Ord(base64Pad) then
    begin
      Inc(CurrentInByte);
      while ((PByteArray(InBuffer)[CurrentInByte] = 13) or (PByteArray(InBuffer)[CurrentInByte] = 10)) and (integer(CurrentInByte) < InSize) do
        Inc(CurrentInByte);
      Continue;
    end;
    Index := Base64CharIndex(Chr(PByteArray(InBuffer)[CurrentInByte]));
    if Index = $FF then
    begin
      Result := BASE64_DECODE_INVALID_CHARACTER;
      Exit;
    end;
    I := CurrentOutBit shr 3; J := CurrentOutBit mod 8;
    PByteArray(OutBuffer)[I] := PByteArray(OutBuffer)[I] or ((Index shr 5) and 1) shl (7 - J);
    I := (CurrentOutBit + 1) shr 3; J := (CurrentOutBit + 1) mod 8;
    PByteArray(OutBuffer)[I] := PByteArray(OutBuffer)[I] or ((Index shr 4) and 1) shl (7 - J);
    I := (CurrentOutBit + 2) shr 3; J := (CurrentOutBit + 2) mod 8;
    PByteArray(OutBuffer)[I] := PByteArray(OutBuffer)[I] or ((Index shr 3) and 1) shl (7 - J);
    I := (CurrentOutBit + 3) shr 3; J := (CurrentOutBit + 3) mod 8;
    PByteArray(OutBuffer)[I] := PByteArray(OutBuffer)[I] or ((Index shr 2) and 1) shl (7 - J);
    I := (CurrentOutBit + 4) shr 3; J := (CurrentOutBit + 4) mod 8;
    PByteArray(OutBuffer)[I] := PByteArray(OutBuffer)[I] or ((Index shr 1) and 1) shl (7 - J);
    I := (CurrentOutBit + 5) shr 3; J := (CurrentOutBit + 5) mod 8;
    PByteArray(OutBuffer)[I] := PByteArray(OutBuffer)[I] or ((Index shr 0) and 1) shl (7 - J);
    Inc(CurrentInByte);
    Inc(CurrentOutBit, 6);
    while (PByteArray(InBuffer)[CurrentInByte] = 13) or (PByteArray(InBuffer)[CurrentInByte] = 10) do
      Inc(CurrentInByte);
  end;
  OutSize := CurrentOutBit shr 3;
end;



function StrToDigest(const DigestStr : string;
  var Digest : TMessageDigest128) : boolean; overload;
var i, j : integer;
    Value: string;
begin
  if Length(DigestStr) <> 32 then
    result := false
  else
  begin
    for i := 0 to 3 do
    begin
      Value := Copy(DigestStr, i * 8 + 1, 8);
      try
        j := SwapInt32(StrToInt('$' + Value));
        case i of
          0: Digest.A := j;
          1: Digest.B := j;
          2: Digest.C := j;
          3: Digest.D := j;
        end;
      except
        result := false;
        exit;
      end;
    end;
    result := true;
  end;
end;

function StrToDigest(const DigestStr : string;
  var Digest : TMessageDigest160) : boolean; overload;
var i, j : integer;
    Value: string;
begin
  if Length(DigestStr) <> 40 then
    result := false
  else
  begin
    for i := 0 to 4 do
    begin
      Value := Copy(DigestStr, i * 8 + 1, 8);
      try
        j := SwapInt32(StrToInt('$' + Value));
        case i of
          0: Digest.A := j;
          1: Digest.B := j;
          2: Digest.C := j;
          3: Digest.D := j;
          4: Digest.E := j;
        end;
      except
        result := false;
        exit;
      end;
    end;
    result := true;
  end;
end;

function DigestToStr(const Digest: TMessageDigest128;
  LowerCase: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}): string;
var
  Buffer: array[0..15] of byte absolute Digest;
  Alphabet: PChar;
  I: integer;
begin
  if LowerCase then
    Alphabet := LowerAlphabet
  else
    Alphabet := UpperAlphabet;
  SetLength(Result, 32);
  for I := 0 to 15 do
  begin
    Result[I * 2 + 1] := (Alphabet + (Buffer[I] shr 4))^;
    Result[I * 2 + 2] := (Alphabet + (Buffer[I] and $0F))^;
  end;
end;

function DigestToStr(const Digest: TMessageDigest160;
  LowerCase: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}): string;
var
  Buffer: array[0..19] of byte absolute Digest;
  Alphabet: PChar;
  I: integer;
begin
  if LowerCase then
    Alphabet := LowerAlphabet
  else
    Alphabet := UpperAlphabet;
  SetLength(Result, 40);
  for I := 0 to 19 do
  begin
    Result[I * 2 + 1] := (Alphabet + (Buffer[I] shr 4))^;
    Result[I * 2 + 2] := (Alphabet + (Buffer[I] and $0F))^;
  end;
end;

function DigestToStr(const Digest: TMessageDigest224;
  LowerCase: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}): string;
var
  Buffer: array[0..27] of byte absolute Digest;
  Alphabet: PChar;
  I: integer;
begin
  if LowerCase then
    Alphabet := LowerAlphabet
  else
    Alphabet := UpperAlphabet;
  SetLength(Result, 56);
  for I := 0 to 27 do
  begin
    Result[I * 2 + 1] := (Alphabet + (Buffer[I] shr 4))^;
    Result[I * 2 + 2] := (Alphabet + (Buffer[I] and $0F))^;
  end;
end;

function DigestToStr(const Digest: TMessageDigest256;
  LowerCase: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}): string;
var
  Buffer: array[0..31] of byte absolute Digest;
  Alphabet: PChar;
  I: integer;
begin
  if LowerCase then
    Alphabet := LowerAlphabet
  else
    Alphabet := UpperAlphabet;
  SetLength(Result, 64);
  for I := 0 to 31 do
  begin
    Result[I * 2 + 1] := (Alphabet + (Buffer[I] shr 4))^;
    Result[I * 2 + 2] := (Alphabet + (Buffer[I] and $0F))^;
  end;
end;

function DigestToStr(const Digest: TMessageDigest320;
  LowerCase: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}): string;
var
  Buffer: array[0..39] of byte absolute Digest;
  Alphabet: PChar;
  I: integer;
begin
  if LowerCase then
    Alphabet := LowerAlphabet
  else
    Alphabet := UpperAlphabet;
  SetLength(Result, 80);
  for I := 0 to 39 do
  begin
    Result[I * 2 + 1] := (Alphabet + (Buffer[I] shr 4))^;
    Result[I * 2 + 2] := (Alphabet + (Buffer[I] and $0F))^;
  end;
end;

function DigestToStr(const Digest: TMessageDigest384;
  LowerCase: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}): string;
var
  Buffer: array[0..47] of byte absolute Digest;
  Alphabet: PChar;
  I: integer;
begin
  if LowerCase then
    Alphabet := LowerAlphabet
  else
    Alphabet := UpperAlphabet;
  SetLength(Result, 96);
  for I := 0 to 47 do
  begin
    Result[I * 2 + 1] := (Alphabet + (Buffer[I] shr 4))^;
    Result[I * 2 + 2] := (Alphabet + (Buffer[I] and $0F))^;
  end;
end;

function DigestToStr(const Digest: TMessageDigest512;
  LowerCase: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}): string;
var
  Buffer: array[0..63] of byte absolute Digest;
  Alphabet: PChar;
  I: integer;
begin
  if LowerCase then
    Alphabet := LowerAlphabet
  else
    Alphabet := UpperAlphabet;
  SetLength(Result, 128);
  for I := 0 to 63 do
  begin
    Result[I * 2 + 1] := (Alphabet + (Buffer[I] shr 4))^;
    Result[I * 2 + 2] := (Alphabet + (Buffer[I] and $0F))^;
  end;
end;

function BeautifyBinaryString(const Str : string; Separator : Char): string;
var
  i : integer;
  pl: integer;
begin
  if Length(Str) mod 2 = 0 then
  begin
    pl := Length(Str) div 2;
    SetLength(Result, Length(Str) * 3 shr 1 - 1);
    for i := 0 to pl - 1 do
    begin
      Result[i * 3 + 1] := Str[i * 2 + AnsiStrOffset];
      Result[i * 3 + 2] := Str[i * 2 + AnsiStrOffset + 1];
      if i < pl - 1 then
        Result[i * 3 + 3] := Separator;
    end;

  end
  //else
  //  result := Str;
end;

function Min(const A, B: integer): integer; overload;
begin
  if A < B then
    Result := A
  else
    Result := B;
end;

function Min(const A, B: cardinal): cardinal; overload;
begin
  if A < B then
    Result := A
  else
    Result := B;
end;

function Min(const A, B: Int64): Int64; overload;
begin
  if A < B then
    Result := A
  else
    Result := B;
end;

function Max(const A, B: integer): integer; overload;
begin
  if A > B then
    Result := A
  else
    Result := B;
end;

function Max(const A, B: cardinal): cardinal; overload;
begin
  if A > B then
    Result := A
  else
    Result := B;
end;

function Max(const A, B: Int64): Int64; overload;
begin
  if A > B then
    Result := A
  else
    Result := B;
end;



procedure PointerToLInt(var B : PLInt; P : pointer; Size : integer);
var
  I : integer;
  j : LongInt;
begin
  B.Length := Size shr 2;
  for I := 1 to B.Length do
  begin
    J := PInt(@(PByteArray(P)[Size - 4]))^;
    asm
      push eax
      mov eax, j
      bswap eax
      mov J, eax
      pop eax
    end;
    B.Digits[I] := J;
    (* DO NOT REMOVE!!!
    B.Digits[I] := (PByteArray(P)[Size - 4] shl 24) or (PByteArray(P)[Size - 3] shl 16) or
      (PByteArray(P)[Size - 2] shl 8) or PByteArray(P)[Size - 1];
    *)
    Dec(Size, 4);
  end;
  if Size > 0 then
  begin
    Inc(B.Length);
    I := 0;
    B.Digits[B.Length] := 0;
    while Size > 0 do
    begin
      B.Digits[B.Length] := B.Digits[B.Length] or (PByteArray(P)[Size - 1] shl I);
      Dec(Size);
      Inc(I, 8);
    end;
  end;
  with B^ do
    while (Digits[Length] = 0) and (Length > 1) do
      Dec(Length);
end;

{$ifndef CUSTOM_ZLIB}


procedure LIntToPointer(B: PLInt; P: Pointer; var Size: LongInt);
var
  I, J: Integer;
begin
  J := 0;
  for I := B.Length downto 1 do
  begin
    PByteArray(P)[J] := (B.digits[I] shr 24) and $FF;
    PByteArray(P)[J + 1] := (B.digits[I] shr 16) and $FF;
    PByteArray(P)[J + 2] := (B.digits[I] shr 8) and $FF;
    PByteArray(P)[J + 3] := B.digits[I] and $FF;
    J := J + 4;
  end;
  Size := J;
end;


function GetPBEAlgorithmByOID(const OID: BufferType): Integer;
begin
  if CompareContent(OID, SB_OID_PBE_SHA1_3DES) then
    Result := SB_ALGORITHM_PBE_SHA1_3DES
  else
  if CompareContent(OID, SB_OID_PBE_SHA1_RC4_128) then
    Result := SB_ALGORITHM_PBE_SHA1_RC4_128
  else if CompareContent(OID, SB_OID_PBE_SHA1_RC4_40) then
    Result := SB_ALGORITHM_PBE_SHA1_RC4_40
  else if CompareContent(OID, SB_OID_PBE_SHA1_RC2_128) then
    Result := SB_ALGORITHM_PBE_SHA1_RC2_128
  else if CompareContent(OID, SB_OID_PBE_SHA1_RC2_40) then
    Result := SB_ALGORITHM_PBE_SHA1_RC2_40
  { PKCS#5 PBES1 }
  else if CompareContent(OID, SB_OID_PBE_MD2_DES) then
    Result := SB_ALGORITHM_P5_PBE_MD2_DES
  else if CompareContent(OID, SB_OID_PBE_MD2_RC2) then
    Result := SB_ALGORITHM_P5_PBE_MD2_RC2
  else if CompareContent(OID, SB_OID_PBE_MD5_DES) then
    Result := SB_ALGORITHM_P5_PBE_MD5_DES
  else if CompareContent(OID, SB_OID_PBE_MD5_RC2) then
    Result := SB_ALGORITHM_P5_PBE_MD5_RC2
  else if CompareContent(OID, SB_OID_PBE_SHA1_DES) then
    Result := SB_ALGORITHM_P5_PBE_SHA1_DES
  else if CompareContent(OID, SB_OID_PBE_SHA1_RC2) then
    Result := SB_ALGORITHM_P5_PBE_SHA1_RC2
  { PKCS#5 PBES2 }
  else if CompareContent(OID, SB_OID_PBES2) then
    Result := SB_ALGORITHM_P5_PBES2
  { PKCS#5 : key derivation function PBKDF2 }
  else if CompareContent(OID, SB_OID_PBKDF2) then
    Result := SB_ALGORITHM_P5_PBKDF2
  else
    Result := SB_ALGORITHM_UNKNOWN;
end;

function GetOIDByPBEAlgorithm(Algorithm: Integer): BufferType;
begin
  case Algorithm of
    SB_ALGORITHM_PBE_SHA1_3DES    : Result := SB_OID_PBE_SHA1_3DES;
    SB_ALGORITHM_PBE_SHA1_RC4_128 : Result := SB_OID_PBE_SHA1_RC4_128;
    SB_ALGORITHM_PBE_SHA1_RC4_40  : Result := SB_OID_PBE_SHA1_RC4_40;
    SB_ALGORITHM_PBE_SHA1_RC2_128 : Result := SB_OID_PBE_SHA1_RC2_128;
    SB_ALGORITHM_PBE_SHA1_RC2_40  : Result := SB_OID_PBE_SHA1_RC2_40;
    SB_ALGORITHM_P5_PBE_MD2_DES      : Result := SB_OID_PBE_MD2_DES;
    SB_ALGORITHM_P5_PBE_MD2_RC2      : Result := SB_OID_PBE_MD2_RC2;
    SB_ALGORITHM_P5_PBE_MD5_DES      : Result := SB_OID_PBE_MD5_DES;
    SB_ALGORITHM_P5_PBE_MD5_RC2      : Result := SB_OID_PBE_MD5_RC2;
    SB_ALGORITHM_P5_PBE_SHA1_DES     : Result := SB_OID_PBE_SHA1_DES;
    SB_ALGORITHM_P5_PBE_SHA1_RC2     : Result := SB_OID_PBE_SHA1_RC2;
    SB_ALGORITHM_P5_PBES2            : Result := SB_OID_PBES2;
    SB_ALGORITHM_P5_PBKDF2           : Result := SB_OID_PBKDF2;
  else
    Result := EmptyBuffer;
  end;
end;

function GetPKAlgorithmByOID(const OID: BufferType): Integer;
begin
  // compatible with X.509 unit constants
  if CompareContent(OID, SB_OID_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION {0}
  else if (CompareContent(OID, SB_OID_DSA)) or
    (CompareContent(OID, SB_OID_DSA_ALT)) then
    Result := SB_CERT_ALGORITHM_ID_DSA {4}
  else if CompareContent(OID, SB_OID_DH) then
    Result := SB_CERT_ALGORITHM_DH_PUBLIC {6}
  else if CompareContent(OID, SB_OID_RSAPSS) then
    Result := SB_CERT_ALGORITHM_ID_RSAPSS
  else if CompareContent(OID, SB_OID_RSAOAEP) then
    Result := SB_CERT_ALGORITHM_ID_RSAOAEP
  else
    Result := SB_ALGORITHM_UNKNOWN;
end;

function GetOIDByPKAlgorithm(Algorithm : Integer): BufferType;
begin
  case Algorithm of
    SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION :
      Result := SB_OID_RSAENCRYPTION;
    SB_CERT_ALGORITHM_ID_DSA :
      Result := SB_OID_DSA;
    SB_CERT_ALGORITHM_DH_PUBLIC :
      Result := SB_OID_DH;
    SB_CERT_ALGORITHM_ID_RSAPSS :
      Result := SB_OID_RSAPSS;
    SB_CERT_ALGORITHM_ID_RSAOAEP :
      Result := SB_OID_RSAOAEP;
  else
    Result := EmptyBuffer;
  end;
end;

function GetSigAlgorithmByOID(const OID: BufferType): integer;
begin
  if CompareContent(OID, SB_OID_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION
  else if CompareContent(OID, SB_OID_MD2_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION
  else if CompareContent(OID, SB_OID_MD5_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION
  else if CompareContent(OID, SB_OID_SHA1_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION
  else if CompareContent(OID, SB_OID_SHA1_RSAENCRYPTION2) then
    Result := SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION
  else if CompareContent(OID, SB_OID_RSAPSS) then
    Result := SB_CERT_ALGORITHM_ID_RSAPSS
  else if CompareContent(OID, SB_OID_SHA1_DSA) or
    (CompareContent(OID, SB_OID_DSA_SHA1_ALT)) then
    Result := SB_CERT_ALGORITHM_ID_DSA_SHA1
  else if CompareContent(OID, SB_CERT_OID_SHA224_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION
  else if CompareContent(OID, SB_CERT_OID_SHA256_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION
  else if CompareContent(OID, SB_CERT_OID_SHA384_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION
  else if CompareContent(OID, SB_CERT_OID_SHA512_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION
  else if CompareContent(OID, SB_OID_RSASIGNATURE_RIPEMD160) then
    Result := SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160
  else
    Result := SB_ALGORITHM_UNKNOWN;
end;

function GetOIDBySigAlgorithm(Algorithm: integer): BufferType;
begin
  case Algorithm of
    SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION  : Result := SB_CERT_OID_MD2_RSAENCRYPTION;
    SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION  : Result := SB_CERT_OID_MD5_RSAENCRYPTION;
    SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION : Result := SB_CERT_OID_SHA1_RSAENCRYPTION;
    SB_CERT_ALGORITHM_ID_RSAPSS : Result := SB_CERT_OID_RSAPSS;
    SB_CERT_ALGORITHM_ID_RSAOAEP : Result := SB_CERT_OID_RSAOAEP;
    SB_CERT_ALGORITHM_ID_DSA_SHA1 : Result := SB_CERT_OID_SHA1_DSA;
    SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION : Result := SB_CERT_OID_SHA224_RSAENCRYPTION;
    SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION : Result := SB_CERT_OID_SHA256_RSAENCRYPTION;
    SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION : Result := SB_CERT_OID_SHA384_RSAENCRYPTION;
    SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION : Result := SB_CERT_OID_SHA512_RSAENCRYPTION;
    SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160: Result := SB_OID_RSASIGNATURE_RIPEMD160;
  else
    Result := EmptyBuffer;
  end;
end;

function GetAlgorithmByOID(const OID : BufferType) : integer;
begin
  if CompareContent(OID, SB_CERT_OID_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION
  else if CompareContent(OID, SB_CERT_OID_MD5_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION
  else if CompareContent(OID, SB_CERT_OID_SHA1_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION
  else if CompareContent(OID, SB_OID_SHA1_RSAENCRYPTION2) then
    Result := SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION
  else if CompareContent(OID, SB_CERT_OID_SHA224_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION
  else if CompareContent(OID, SB_CERT_OID_SHA256_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION
  else if CompareContent(OID, SB_CERT_OID_SHA384_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION
  else if CompareContent(OID, SB_CERT_OID_SHA512_RSAENCRYPTION) then
    Result := SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION
  else if CompareContent(OID, SB_OID_RSASIGNATURE_RIPEMD160) then
    Result := SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160
  else if CompareContent(OID, SB_CERT_OID_DSA) then
    Result := SB_CERT_ALGORITHM_ID_DSA
  else if CompareContent(OID, SB_CERT_OID_DSA_SHA1) then
    Result := SB_CERT_ALGORITHM_ID_DSA_SHA1
  else if CompareContent(OID, SB_CERT_OID_RSAPSS) then
    Result := SB_CERT_ALGORITHM_ID_RSAPSS
  else if CompareContent(OID, SB_CERT_OID_DH) then
    Result := SB_CERT_ALGORITHM_DH_PUBLIC
  else if CompareContent(OID, SB_OID_RC4) then
    Result := SB_ALGORITHM_CNT_RC4
  else if CompareContent(OID, SB_OID_DES_EDE3_CBC) then
    Result := SB_ALGORITHM_CNT_3DES
  else if CompareContent(OID, SB_OID_RC2_CBC) then
    Result := SB_ALGORITHM_CNT_RC2
  else if CompareContent(OID, SB_OID_DES_CBC) then
    Result := SB_ALGORITHM_CNT_DES
  else if CompareContent(OID, SB_OID_AES128_CBC) then
    Result := SB_ALGORITHM_CNT_AES128
  else if CompareContent(OID, SB_OID_AES192_CBC) then
    Result := SB_ALGORITHM_CNT_AES192
  else if CompareContent(OID, SB_OID_AES256_CBC) then
    Result := SB_ALGORITHM_CNT_AES256
  else if CompareContent(OID, SB_OID_SHA1) then
    Result := SB_ALGORITHM_DGST_SHA1
  else if CompareContent(OID, SB_OID_MD2) then
    Result := SB_ALGORITHM_DGST_MD2
  else if CompareContent(OID, SB_OID_MD4) then
    Result := SB_ALGORITHM_DGST_MD4
  else if CompareContent(OID, SB_OID_MD5) then
    Result := SB_ALGORITHM_DGST_MD5
  else if CompareContent(OID, SB_OID_SHA224) then
    Result := SB_ALGORITHM_DGST_SHA224
  else if CompareContent(OID, SB_OID_SHA256) then
    Result := SB_ALGORITHM_DGST_SHA256
  else if CompareContent(OID, SB_OID_SHA384) then
    Result := SB_ALGORITHM_DGST_SHA384
  else if CompareContent(OID, SB_OID_SHA512) then
    Result := SB_ALGORITHM_DGST_SHA512
  else if CompareContent(OID, SB_OID_RIPEMD160) then
    Result := SB_ALGORITHM_DGST_RIPEMD160
  else if CompareContent(OID, SB_OID_HMACSHA1) then
    Result := SB_ALGORITHM_MAC_HMACSHA1
  else
    Result := SB_CERT_ALGORITHM_UNKNOWN;
end;

function GetOIDByAlgorithm(Algorithm : integer) : BufferType;
begin
  case Algorithm of
    SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION :
      Result := SB_OID_RSAENCRYPTION;
    SB_CERT_ALGORITHM_RSASIGNATURE_RIPEMD160 :
      Result := SB_OID_RSASIGNATURE_RIPEMD160;
    SB_CERT_ALGORITHM_ID_DSA :
      Result := SB_OID_DSA;
    SB_CERT_ALGORITHM_DH_PUBLIC :
      Result := SB_OID_DH;
    SB_ALGORITHM_CNT_RC4 :
      Result := SB_OID_RC4;
    SB_ALGORITHM_CNT_RC2 :
      Result := SB_OID_RC2_CBC;
    SB_ALGORITHM_CNT_3DES :
      Result := SB_OID_DES_EDE3_CBC;
    SB_ALGORITHM_CNT_DES :
      Result := SB_OID_DES_CBC;
    SB_ALGORITHM_CNT_AES128 :
      Result := SB_OID_AES128_CBC;
    SB_ALGORITHM_CNT_AES192 :
      Result := SB_OID_AES192_CBC;
    SB_ALGORITHM_CNT_AES256 :
       Result := SB_OID_AES256_CBC;
    SB_ALGORITHM_DGST_SHA1 :
      Result := SB_OID_SHA1;
    SB_ALGORITHM_DGST_MD2 :
      Result := SB_OID_MD2;
    SB_ALGORITHM_DGST_MD4 :
      Result := SB_OID_MD4;
    SB_ALGORITHM_DGST_MD5 :
      Result := SB_OID_MD5;
    SB_ALGORITHM_DGST_SHA224 :
      Result := SB_OID_SHA224;
    SB_ALGORITHM_DGST_SHA256 :
      Result := SB_OID_SHA256;
    SB_ALGORITHM_DGST_SHA384 :
      Result := SB_OID_SHA384;
    SB_ALGORITHM_DGST_SHA512 :
      Result := SB_OID_SHA512;
    SB_ALGORITHM_DGST_RIPEMD160 :
      Result := SB_OID_RIPEMD160;
    SB_ALGORITHM_MAC_HMACSHA1 :
      Result := SB_OID_HMACSHA1;
  else
    Result := EmptyBuffer;
  end;
end;

function GetHashAlgorithmByOID(const OID : BufferType) : integer;
begin
  if CompareContent(OID, SB_OID_SHA1) then
    Result := SB_ALGORITHM_DGST_SHA1
  else if CompareContent(OID, SB_OID_MD2) then
    Result := SB_ALGORITHM_DGST_MD2
  else if CompareContent(OID, SB_OID_MD4) then
    Result := SB_ALGORITHM_DGST_MD4
  else if CompareContent(OID, SB_OID_MD5) then
    Result := SB_ALGORITHM_DGST_MD5
  else if CompareContent(OID, SB_OID_SHA224) then
    Result := SB_ALGORITHM_DGST_SHA224
  else if CompareContent(OID, SB_OID_SHA256) then
    Result := SB_ALGORITHM_DGST_SHA256
  else if CompareContent(OID, SB_OID_SHA384) then
    Result := SB_ALGORITHM_DGST_SHA384
  else if CompareContent(OID, SB_OID_SHA512) then
    Result := SB_ALGORITHM_DGST_SHA512
  else if CompareContent(OID, SB_OID_RIPEMD160) then
    Result := SB_ALGORITHM_DGST_RIPEMD160
  else
    Result := SB_CERT_ALGORITHM_UNKNOWN;
end;

function GetOIDByHashAlgorithm(Algorithm : integer) : BufferType;
begin
  case Algorithm of
    SB_ALGORITHM_DGST_SHA1 :
      Result := SB_OID_SHA1;
    SB_ALGORITHM_DGST_MD2 :
      Result := SB_OID_MD2;
    SB_ALGORITHM_DGST_MD4 :
      Result := SB_OID_MD4;
    SB_ALGORITHM_DGST_MD5 :
      Result := SB_OID_MD5;
    SB_ALGORITHM_DGST_SHA224 :
      Result := SB_OID_SHA224;
    SB_ALGORITHM_DGST_SHA256 :
      Result := SB_OID_SHA256;
    SB_ALGORITHM_DGST_SHA384 :
      Result := SB_OID_SHA384;
    SB_ALGORITHM_DGST_SHA512 :
      Result := SB_OID_SHA512;
    SB_ALGORITHM_DGST_RIPEMD160 :
      Result := SB_OID_RIPEMD160;
  else
    Result := EmptyBuffer;
  end;
end;
{$endif}


function CompareMem(const Mem1: ByteArray; const Mem2: ByteArray): Boolean;  overload;
var
  I: Integer;
begin
  Result := False;
  if Length(Mem1) <> Length(Mem2) then
    exit;
  for I := 0 to Length(Mem1) - 1 do
  begin
    if Mem1[I] <> Mem2[I] then
      Exit;
  end;
  Result := true;
end;

function CompareMem(const Mem1: ByteArray; const Mem2: AnsiString): Boolean; overload;
var
  I: Integer;
begin
  Result := False;
  if Length(Mem1) <> Length(Mem2) then
    exit;
  for I := 0 to Length(Mem1) - 1 do
  begin
    if Mem1[I] <> Ord(Mem2[I + 1]) then
      Exit;
  end;
  Result := true;
end;

function CompareMem(const Mem1: Pointer; const Mem2: Pointer; Size : integer): Boolean;
begin
  result := SysUtils.CompareMem(Mem1, Mem2, Size);
end;


function CompareMem(const Mem1: ByteArray; Offset1 : integer; const Mem2: ByteArray; Offset2 : integer) : Boolean; overload;
var
  I: Integer;
begin
  Result := False;
  if Length(Mem1) - Offset1 <> Length(Mem2) - Offset2 then
    exit;
  for I := Offset1 to Length(Mem1) - 1 do
  begin
    if Mem1[I] <> Mem2[I - Offset1 + Offset2] then
      Exit;
  end;
  Result := true;
end;

function CompareMem(const Mem1: ByteArray; Offset1 : integer; const Mem2: ByteArray; Offset2 : integer; Size : integer) : Boolean; overload;
var
  I: Integer;
begin
Result := False;
  if Length(Mem1) - Offset1 < Size then
    exit;

  if Length(Mem2) - Offset2 < Size then
    exit;

  for I := Offset1 to Offset1 + Size - 1 do
  begin
    if Mem1[I] <> Mem2[I - Offset1 + Offset2] then
      Exit;
  end;
  Result := true;
end;


function BinaryToString(Buffer : Pointer; BufSize : integer): string;
var i : integer;
  Alphabet: PChar;
  Buf : PByteArray;
begin
  result := '';
  Buf := PByteArray(Buffer);

  SetLength(result, BufSize * 2);

  Alphabet := UpperAlphabet;
  for i := 0 to BufSize - 1 do
  begin
    Result[I * 2 + 1] := (Alphabet + (Buf^[I] shr 4))^;
    Result[I * 2 + 2] := (Alphabet + (Buf^[I] and $0F))^;
  end;

end;

function StringToBinary(const S: string; Buffer : pointer; var Size: integer) : boolean;

  function ConvertChar(Ch: byte) : byte;
  begin
    if (Ch >= $30) and (Ch <= $39) then
      Result := Ch - $30
    else if (Ch >= $41) and (Ch <= $46) then
      Result := Ch - 55
    else if (Ch >= $61) and (Ch <= $66) then
      Result := Ch - 87
    else
      raise EConvertError.Create('String contains non-hexadecimal characters');
  end;

var
  InPtr, OutPtr : ^byte;
  BytesLeft: integer;
begin
  Result := false;
  if Length(S) and 1 = 1 then
    Exit;

  if Size < Length(S) shr 1 then
  begin
    Size := Length(S) shr 1;
    Exit;
  end;

  InPtr := @S[1];
  OutPtr := Buffer;
  BytesLeft := Length(S);
  while BytesLeft > 0 do
  begin
    OutPtr^ := (ConvertChar(InPtr^) shl 4) or ConvertChar(PByteArray(InPtr)[1]);
    Inc(InPtr, 2);
    Inc(OutPtr);
    Dec(BytesLeft, 2);
  end;
  Size := Length(S) shr 1;

  Result := True;
end;

procedure SwapBigEndianWords(P: Pointer; Size: LongInt);
var p1 : PByteArray;
    i : integer;
    b : byte;
begin
  p1 := PByteArray(p);
  for i := 0 to (size shr 1) - 1 do
  begin
    b := p1[i * 2];
    p1[i * 2] := p1[i * 2 + 1];
    p1[i * 2 + 1] := b;
  end;
end;


function DigestToBinary(const Digest : TMessageDigest128) : string;
begin
  SetLength(Result, 16);
  Move(Digest, Result[1], 16);
end;

function DigestToBinary(const Digest : TMessageDigest160) : string;
begin
  SetLength(Result, 20);
  Move(Digest, Result[1], 20);
end;

function ConvertUTF16toUTF8(const source: WideString;
                        var target: AnsiString;
                        flags: ConversionFlags;
                        BOM: boolean): ConversionResult;
const
  byteMask: UTF32 = $0BF;
  byteMark: UTF32 = $80;
var
  ch, ch2: UTF32;
  bytesToWrite: word;
  i, sourcelen: integer;
  ts: string;
begin
  Result := conversionOK;
  target := '';
  i := 1;
  sourcelen := length(source) + i;
  while i < sourcelen do
  begin
    // bytesToWrite := 0;
    ch := UTF32(source[i]);
    inc(i);
    { If we have a surrogate pair, convert to UTF32 first }
    if ((ch >= UNI_SUR_HIGH_START) and (ch <= UNI_SUR_HIGH_END) and (i < sourcelen)) then
    begin
      ch2 := UTF32(source[i]);
      if ((ch2 >= UNI_SUR_LOW_START) and (ch2 <= UNI_SUR_LOW_END)) then
      begin
        ch := ((ch - UNI_SUR_HIGH_START) shl halfShift) + (ch2 - UNI_SUR_LOW_START) + halfBase;
        inc(i);
      end
      else
        if (flags = strictConversion) then  { it's an unpaired high surrogate }
        begin
          Result := sourceIllegal;
          break;
        end;
    end
    else
      if ((flags = strictConversion) and ((ch >= UNI_SUR_LOW_START) and (ch <= UNI_SUR_LOW_END))) then
      begin
        Result := sourceIllegal;
        break;
      end;
    { Figure out how many bytes the Result will require }
    if ch < UTF32($80) then
      bytesToWrite := 1
    else
    if ch < UTF32($800) then
      bytesToWrite := 2
    else
    if ch < UTF32($10000) then
      bytesToWrite := 3
    else
    if ch < UTF32($200000) then
      bytesToWrite := 4
    else
    begin
      bytesToWrite := 2;
      ch := UNI_REPLACEMENT_CHAR;
    end;

    { note: everything falls through. }
    ts := '';
    if bytesToWrite = 4 then
    begin
      ts := char((ch or byteMark) and byteMask);
      ch := ch shr 6
    end;
    if bytesToWrite >= 3 then
    begin
      ts := char((ch or byteMark) and byteMask) + ts;
      ch := ch shr 6
    end;
    if bytesToWrite >= 2 then
    begin
      ts := char((ch or byteMark) and byteMask) + ts;
      ch := ch shr 6
    end;
    if bytesToWrite >= 1 then
    begin
      ts := char(ch or firstByteMark[bytesToWrite]) + ts;
    end;
    target := target + ts;
  end;
  if BOM and (Pos(UTF8BOM, target)<>1) then
    target := UTF8BOM + target;
end;

function ConvertUTF8toUTF16(const source: string;
                             var target: WideString;
                             flags: ConversionFlags;
                             BOM: boolean): ConversionResult;
var
  ch: UTF32;
  extraBytesToRead: word;
  i, sourcelen: integer;
begin
  Result := conversionOK;
  i := 1;
  sourcelen := length(source) + i;
  target := '';

  while i < sourcelen do
  begin
    ch := 0;
    extraBytesToRead := trailingBytesForUTF8[UTF8(source[i])];
    if (i + extraBytesToRead) >= sourcelen then
    begin
      Result := sourceExhausted;
      break;
    end;
    { Do this check whether lenient or strict }
    if (not isLegalUTF8(copy(source,i,extraBytesToRead+1), extraBytesToRead+1)) then
    begin
      Result := sourceIllegal;
      break;
    end;
    { The cases all fall through.}
    if extraBytesToRead = 3 then
    begin
      ch := ch + UTF32(source[i]);
      inc(i);
      ch := ch shl 6;
    end;
    if (extraBytesToRead >= 2) and (extraBytesToRead < 4) then
    begin
      ch := ch + UTF32(source[i]);
      inc(i);
      ch := ch shl 6;
    end;
    if (extraBytesToRead >= 1) and (extraBytesToRead < 4) then
    begin
      ch := ch + UTF32(source[i]);
      inc(i);
      ch := ch shl 6;
    end;
    if (* (extraBytesToRead >= 0) and *) (extraBytesToRead < 4) then
    begin
      ch := ch + UTF32(source[i]);
      inc(i);
    end;
    ch := ch - offsetsFromUTF8[extraBytesToRead];
    if (ch <= UNI_MAX_BMP) then         { Target is a character <= 0xFFFF }
    begin
      if ((flags = strictConversion) and ((ch >= UNI_SUR_HIGH_START) and (ch <= UNI_SUR_LOW_END))) then
      begin
        Result := sourceIllegal;
        break;
      end else
        target := target + widechar(ch);  { normal case }
    end
    else
      if (ch > UNI_MAX_UTF16) then
      begin
        if (flags = strictConversion) then
        begin
          Result := sourceIllegal;
          i := i - extraBytesToRead;
        end else
          target := target + widechar(UNI_REPLACEMENT_CHAR);
      end
      else
      { target is a character in range 0xFFFF - 0x10FFFF. }
      begin
        ch := ch - halfBase;
        target := target + widechar((ch shr halfShift) + UNI_SUR_HIGH_START) +
                           widechar((ch and halfMask) + UNI_SUR_LOW_START);
      end;
  end;
  if BOM and (ord(target[1])<>$FEFF) then target := widechar($FEFF) + target;
end;

function isLegalUTF8(source: string; sourcelen: cardinal): boolean;
var
  length: cardinal;
begin
  length := trailingBytesForUTF8[byte(source[1])]+1;
  if length > sourcelen then
    Result := false
  else
  if length = sourcelen then
    Result := true
  else
    Result := isLegalUTF8(source, length);
end;

function ConvertToUTF8String(const Source : WideString) : string;
begin
  {$ifdef FPC}
  Result := '';
  {$endif}
  if ConvertUTF16ToUTF8(Source, Result, strictConversion, false) = sourceIllegal then
    result := source;
end;

function ConvertFromUTF8String(const Source : string; CheckBOM : boolean = true) : WideString;
begin
  if (not CheckBOM) or (copy (Source, 1, 3) = UTF8BOM) then
  begin
    result := '';
    if ConvertUTF8ToUTF16(Source, Result,  strictConversion, false) = sourceIllegal then
      result := Source
    else
    begin
      if (Length(Result) > 1) and (Result[1] = WideChar($FEFF)) then
      begin
        Delete(Result, 1, 1);
      end;
    end;
  end
  else
  begin
    result := Source;
  end;
end;


function SBTrim(const S : ByteArray) : ByteArray; 
var fp, lp : integer;
begin
  fp := 0;
  lp := Length(S) - 1;
  while fp <= lp do
  begin
    if ((S[fp] = 9) or (S[fp] = 32) or (S[fp] = 13)or (S[fp] = 10)) then
      inc (fp)
    else
      break;
  end;

  while lp >= fp do
  begin
    if ((S[lp] = 9) or (S[lp] = 32) or (S[lp] = 13)or (S[lp] = 10)) then
      dec(lp)
    else
      break;
  end;
  result := SBCopy(S, fp, lp - fp + 1);
end;

function SBUppercase(const S : ByteArray) : ByteArray;
var i : integer;
begin
  SetLength(Result, Length(S));
  for i := 0 to Length(S) -1 do
    result[i] := Ord(Upcase(Char(S[i])));
end;

function GetByteArrayFromDWord(Value: cardinal) : ByteArray;
begin
  SetLength(Result, 4);
  Result[0] := Value shr 24;
  Result[1] := (Value shr 16) and $ff;
  Result[2] := (Value shr 8) and $ff;
  Result[3] := Value and $ff;
end;

function UTCTimeToDate(const UTCTime: string): TDateTime;
var
  Y, M, D: Word;
  Str: string;
begin
  if Length(UTCTime) < 6 then
  begin
    result := 0;
    exit;
  end;
  // year
  Str := Copy(UTCTime, 1, 2);
  Y := Word(StrToIntDef(Str, 0));
  if (Y >= 50) then
    Y := (1900 + Y)
  else
    Y := (2000 + Y);
  // month
  Str := Copy(UTCTime, 3, 2);
  M := Word(StrToIntDef(Str, 1));
  // day
  Str := Copy(UTCTime, 5, 2);
  D := Word(StrToIntDef(Str, 1));
  Result := EncodeDate(Y, M, D);
end;

function UTCTimeToTime(const UTCTime: string): TDateTime;
var
  H, M, S: Word;
  Str: string;
begin
  // hour
  Str := Copy(UTCTime, 7, 2);
  H := Word(StrToIntDef(Str, 0));
  // minute
  Str := Copy(UTCTime, 9, 2);
  M := Word(StrToIntDef(Str, 0));
  // second
  Str := Copy(UTCTime, 11, 2);
  S := Word(StrToIntDef(Str, 0));
  Result := EncodeTime(H, M, S, 0);
end;

function UTCTimeToDateTime(const UTCTime: string): TDateTime;
begin
  Result := UTCTimeToDate(UTCTime) + UTCTimeToTime(UTCTime);
end;

function GeneralizedTimeToDate(const GenTime: string): TDateTime;
var
  Y, M, D: Word;
  Str: string;
begin
  // year
  // hour
  Str := Copy(GenTime, 1, 4);
  Y := Word(StrToIntDef(Str, 0));
  // month
  //SetLength(Str, 2);
  Str := Copy(GenTime, 5, 2);
  M := Word(StrToIntDef(Str, 1));
  // day
  Str := Copy(GenTime, 7, 2);
  D := Word(StrToIntDef(Str, 1));
  Result := EncodeDate(Y, M, D);
end;

function GeneralizedTimeToTime(const GenTime: string): TDateTime;
var
  H, M, S: Word;
  Str: string;
begin
  // hour
  Str := Copy(GenTime, 9, 2);
  H := Word(StrToIntDef(Str, 0));
  // minute
  Str := Copy(GenTime, 11, 2);
  M := Word(StrToIntDef(Str, 0));
  // second
  Str := Copy(GenTime, 13, 2);
  S := Word(StrToIntDef(Str, 0));
  Result := EncodeTime(H, M, S, 0);
end;

function GeneralizedTimeToDateTime(const GenTime: string): TDateTime;
begin
  Result := GeneralizedTimeToDate(GenTime) + GeneralizedTimeToTime(GenTime);
end;

function DateTimeToUTCTime(const ADateTime : TDateTime) : string;
var
  T1, T2, T3, T4: WORD;
  S1, S2, S3: string;
begin
  {$ifdef FPC}
  T1 := 0;
  T2 := 0;
  T3 := 0;
  {$endif}
  DecodeDate(ADateTime, T1, T2, T3);
  T1 := T1 mod 100;
  T2 := T2 mod 100;
  T3 := T3 mod 100;

  S1 := IntToStr(T1);
  S2 := IntToStr(T2);
  S3 := IntToStr(T3);
  if Length(S1) = 1 then S1 := '0' + S1;
  if Length(S2) = 1 then S2 := '0' + S2;
  if Length(S3) = 1 then S3 := '0' + S3;

  Result := S1 + S2 + S3;

  {$ifdef FPC}
  T4 := 0;
  {$endif}
  DecodeTime(ADateTime, T1, T2, T3, T4);
  S1 := IntToStr(T1);
  S2 := IntToStr(T2);
  S3 := IntToStr(T3);
  if Length(S1) = 1 then S1 := '0' + S1;
  if Length(S2) = 1 then S2 := '0' + S2;
  if Length(S3) = 1 then S3 := '0' + S3;
  Result := Result + S1 + S2 + S3 + 'Z';
end;

function DateTimeToGeneralizedTime(const ADateTime : TDateTime) : string;
var
  T1, T2, T3, T4: WORD;
  S1, S2, S3: string;
begin
  DecodeDate(ADateTime, T1, T2, T3);
  T2 := T2 mod 100;
  T3 := T3 mod 100;
  S1 := IntToStr(T1);
  S2 := IntToStr(T2);
  S3 := IntToStr(T3);
  while Length(S1) < 4 do S1 := '0' + S1;
  if Length(S2) = 1 then S2 := '0' + S2;
  if Length(S3) = 1 then S3 := '0' + S3;
  Result := S1 + S2 + S3;

  DecodeTime(ADateTime, T1, T2, T3, T4);
  S1 := IntToStr(T1);
  S2 := IntToStr(T2);
  S3 := IntToStr(T3);
  if Length(S1) = 1 then S1 := '0' + S1;
  if Length(S2) = 1 then S2 := '0' + S2;
  if Length(S3) = 1 then S3 := '0' + S3;
  Result := Result + S1 + S2 + S3 + 'Z';
end;

function CompareMD128(const M1, M2 : TMessageDigest128) : boolean;
begin
  result := (M1.A = M2.A) and
            (M1.B = M2.B) and
            (M1.C = M2.C) and
            (M1.D = M2.D);
end;


function CompareMD160(const M1, M2 : TMessageDigest160) : boolean;
begin
  result := (M1.A = M2.A) and
            (M1.B = M2.B) and
            (M1.C = M2.C) and
            (M1.D = M2.D) and
            (M1.E = M2.E);
end;

(*
function OIDToContent(const OID: string) : BufferType;
begin
  Result := TBufferTypeConst(OID);
end;
//*)

function CompareAnsiStr(const Content, OID : AnsiString) : boolean;
begin
  Result := CompareStr(Content, OID) = 0;
end;

function CompareContent(const Content, OID: BufferType): Boolean;  overload;
begin
  Result := CompareStr(Content, OID) = 0;
end;


function EmptyBuffer: BufferType;
begin
  Result := '';
end;


function EmptyArray: ByteArray;
begin
  SetLength(Result, 0);
end;

function SBConcatBuffers(const Buf1, Buf2 : ByteArray): ByteArray;
begin
  SetLength(Result, Length(Buf1) + Length(Buf2));
  Move(Buf1[0], Result[0], Length(Buf1));
  Move(Buf2[0], Result[Length(Buf1)], Length(Buf2));
end;

function SBConcatBuffers(const Buf1, Buf2 : BufferType): BufferType;
begin
  Result:= Buf1 + Buf2;
end;



procedure FreeAndNil(var Obj);
var
  O: TObject;
begin
  o := TObject(Obj);
  TObject(Obj) := nil;
  O.Free;
end;


function SBCopy(const str: ByteArray; Offset, Size : integer): ByteArray;
begin
  if (Offset < 0) or (Size <= 0) then
  begin
    result := EmptyArray;
    exit;
  end;
  if Offset + size > Length(str) then
  begin
    Size := Length(str) - offset;

    if Size <= 0 then
    begin
      result := EmptyArray;
      exit;
    end;
  end;
  SetLength(result, Size);
  Move(str[Offset], result[0], Size);
end;

function OIDToStr(const OID: BufferType): String;
var
  Index, Start : integer;
  ID, A, B : cardinal;
  I : integer;
begin
  // reading the first subgroup
  Index := 1;
  while (Index <= Length(OID)) and
      ((PByte(@OID[Index])^ and $80) = $80)
  do
    Inc(Index);
  if Index > Length(OID) then
    Index := Length(OID);
  ID := 0;
  for I := 0 to Index - 1 do
    ID := ID or ((Ord(OID[Index - I]) and $7f) shl (7 * I));
  if ID < 40 then
    A := 0
  else if (ID >= 40) and (ID < 80) then
    A := 1
  else
    A := 2;
  B := ID - (A * 40);

//  {$ifdef DELPHI_NET}
//  Result := System.Convert.ToString(A) + '.' + System.Convert.ToString(B);
//  {$else}
  Result := IntToStr(A) + '.' + IntToStr(B);
//  {$endif}
  // reading the following subgroups
  Inc(Index);
  if Index > Length(OID) then
    Result := Result + '.' + '0'
  else
  begin
    Start := Index;
    while Index <= Length(OID) do
    begin
      // reading the subgroup
      if ((Ord(OID[Index]) and $80) <> $80) or (Index = Length(OID)) then
      begin
        ID := 0;
        for I := 0 to Index - Start do
          ID := ID or ((Ord(OID[Index - I]) and $7f) shl (7 * I));
        Result := Result + '.' + IntToStr(ID);
        Start := Index + 1;
      end;
      Inc(Index);
    end;
  end;
end;

(*
function StrToInt(const Str : string) : integer;
{$ifndef CHROME}
var
  Err: integer;
begin
  Val(Str, Result, Err);
  if Err <> 0 then
    raise EConvertError.Create('Invalid integer value: ' + Str);
{$else}
begin
  result := Int32.Parse(Str);
{$endif}
end;
*)

function StrToOID(const Str : string) : BufferType;
  function ReadNextArc(var S : string) : string;
  var
    Index : integer;
  begin
    Index := Pos('.', S);
    if (Index > 0) then
    begin
      Result := Copy(S, 1, Index - 1);
      S := Copy(S, Index + 1, Length(S));
    end
    else
    begin
      Result := S;
      SetLength(S, 0);
    end;
  end;
var
  FirstArc, SecondArc, S : string;
  N1, N2 : integer;
  B : byte;
  Right : boolean;
begin
  S := Str;
  FirstArc := ReadNextArc(S);
  SecondArc := ReadNextArc(S);
  try
    N1 := StrToInt(FirstArc);
    N2 := StrToInt(SecondArc);
  except
    raise EOIDError.Create(SInvalidOID);
  end;
  if (N1 < 0) or (N1 > 2) or (N2 < 0) or (N2 > 39) then
    raise EOIDError.Create(SInvalidOID);
  Result := Chr(N1 * 40 + N2);
  while Length(S) > 0 do
  begin
    FirstArc := ReadNextArc(S);
    try
      N1 := StrToInt(FirstArc);
    except
      raise EOIDError.Create(SInvalidOID);
    end;
    Right := true;
    if N1 = 0 then
    begin
      SecondArc := #0;
    end
    else
    begin
      while N1 > 0 do
      begin
        B := N1 and $7F;
        if Right then
        begin
          SecondArc := Chr(B);
          Right := false;
        end
        else
         SecondArc := Chr(B or $80) + SecondArc;
         N1 := N1 shr 7;
      end;
    end;
    Result := TBufferTypeConst(Result) + SecondArc;
  end;
end;

{$ifndef FPC}
{$ifndef LINUX}
{$define HAS_WCTOMB}
{$endif}
{$endif}

{$ifdef HAS_WCTOMB}

function UTF8ToWideStr(const Buf: ByteArray): WideString;
begin
  if Length(Buf) > 0 then
    Result := UTF8ToWideStr(Buf[0], Length(Buf))
  else
    Result := '';
end;

function WideStrToUTF8(const AStr: WideString) : ByteArray;
begin
  Result := WideStrToUTF8(AStr[1], Length(AStr));
end;

function WideStrToUTF8(const ASrc; Size: integer) : ByteArray;
var
  Sz: integer;
begin
  Sz := WideCharToMultiByte(CP_UTF8, 0, @ASrc, Size, nil, 0, nil, nil);
  SetLength(Result, Sz);
  Sz := WideCharToMultiByte(CP_UTF8, 0, @ASrc, Size, @Result[0], Length(Result),
    nil, nil);
  SetLength(Result, Sz);
end;

function UTF8ToWideStr(const Buf; Size: Integer): WideString;
var
  Sz: integer;
begin
  if Size <= 0 then
  begin
    Result := '';
    Exit;
  end;

  SetLength(Result, Size);
  Sz := MultiByteToWideChar(CP_UTF8, 0, @Buf, Size, @Result[1], Size);
  SetLength(Result, Sz);
end;
{$else}

function UTF8ToWideStr(const Buf: ByteArray): WideString;
begin
  if Length(Buf) > 0 then
    Result := UTF8ToWideStr(Buf[0], Length(Buf))
  else
    Result := '';
end;

function UTF8ToWideStr(const Buf; Size: Integer): WideString;
var
  Sz: integer;
  TS: string;
begin
  if Size <= 0 then
  begin
    Result := '';
    Exit;
  end;

  SetLength(TS, Size);
  Move(Buf, TS[1], Size);

  ConvertUTF8toUTF16(TS, Result, lenientConversion, false);
end;

function WideStrToUTF8(const AStr: WideString) : ByteArray;
var S : String;
begin
  SetLength(Result, 0);
  s := ConvertToUTF8String(AStr);
  if (Length(S) > 0) then
    Move(S[1], Result[0], Length(S));
end;

function WideStrToUTF8(const ASrc; Size: integer) : ByteArray;
var S : String;
    WS: WideString;
begin
  SetLength(Result, 0);
  SetLength(WS, Size shr 1);
  Move(ASrc, WS[1], Size);
  S := ConvertToUTF8String(WS);
  if (Length(S) > 0) then
    Move(S[1], Result[0], Length(S));
end;
{$endif}

function CompareGUID(const Guid1, Guid2 : TGUID) : boolean;
begin
  Result := SysUtils.CompareMem(@Guid1, @Guid2, SizeOf(Guid1));
end;

function GenerateGUID: string;
var
  Buf : array[0..15] of byte;
  DW : cardinal;
  W1, W2, W3 : word;
  B1, B2, B3, B4, B5, B6 : byte;
begin
  SBRndGenerate(@Buf[0], 16);
  DW := (Buf[0] shl 24) or (Buf[1] shl 16) or (Buf[2] shl 8) or Buf[3];
  W1 := (Buf[4] shl 8) or Buf[5];
  W2 := (Buf[6] shl 8) or Buf[7];
  W3 := (Buf[8] shl 8) or Buf[9];
  B1 := Buf[10];
  B2 := Buf[11];
  B3 := Buf[12];
  B4 := Buf[13];
  B5 := Buf[14];
  B6 := Buf[15];
  Result := '{' + IntToHex(DW, 8) + '-' + IntToHex(W1, 4) + '-' + IntToHex(W2, 4) + '-' +
    IntToHex(W3, 4) + '-' + IntToHex(B1, 2) + IntToHex(B2, 2) + IntToHex(B3, 2) +
    IntToHex(B4, 2) + IntToHex(B5, 2) + IntToHex(B6, 2) + '}';
end;


function CopyStream(SrcStream, DestStream: TStream; Offset : Int64; Count : Int64;
  PreservePosition : boolean = true): Int64;
const
  BUFFER_SIZE : integer = 8192;
var
  OldPos : Int64;
  Buf : ByteArray;
  ToRead, ToWrite : Int64;
begin
  Result := 0;
  OldPos := SrcStream.Position;
  try
    SetLength(Buf, BUFFER_SIZE);
    SrcStream.Position := Offset;
    while (Count > 0) and (SrcStream.Position < SrcStream.Size) do
    begin
      ToRead := Min(Count, BUFFER_SIZE);
      ToWrite := SrcStream.Read(Buf[0], ToRead);
      DestStream.Write(Buf[0], ToWrite);
      Dec(Count, ToWrite);
      Inc(Result, ToWrite);
    end;
  finally
    if PreservePosition then
      SrcStream.Position := OldPos;
  end;
end;


{$ifndef VCL50}
function SameText(const S1, S2: string): Boolean;
begin
  result := (Length(S1) = Length(S2)) and (lowercase(S1) = lowercase(S2));
end;
{$endif}



{$ifndef CUSTOM_ZLIB}
function GetDigestSizeBits(Algorithm : integer) : integer;
begin
  case Algorithm of
    SB_ALGORITHM_DGST_SHA1   : Result := 160;
    SB_ALGORITHM_DGST_MD5    : Result := 128;
    SB_ALGORITHM_DGST_MD2    : Result := 128;
    SB_ALGORITHM_DGST_SHA224 : Result := 224;
    SB_ALGORITHM_DGST_SHA256 : Result := 256;
    SB_ALGORITHM_DGST_SHA384 : Result := 384;
    SB_ALGORITHM_DGST_SHA512 : Result := 512;
    SB_ALGORITHM_DGST_RIPEMD160 : Result := 160;
    SB_ALGORITHM_DGST_CRC32 : Result := 32;
  else
    Result := -1;
  end;
end;
{$endif}

{$ifndef HAS_DEF_PARAMS}
function SBPos(const SubP : ByteArray; const P : ByteArray) : integer;
begin
  Result := SBPos(SubP, P, 0);
end;

function SBPos(const SubP : string; const P : ByteArray) : integer;
begin
  Result := SBPos(SubP, P, 0);
end;
{$endif}

function SBPos(const SubP : ByteArray; const P : ByteArray; StartPos : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}) : integer;
var
  I, J : integer;
  Flag : boolean;
begin
  Result := -1;
  for I := StartPos to Length(P) - Length(SubP) do
  begin
    Flag := true;
    for J := 0 to Length(SubP) - 1 do
    begin
      if P[I + J] <> SubP[J] then
      begin
        Flag := false;
        break;
      end;
    end;
    if Flag then
    begin
      Result := I;
      Exit;
    end;
  end;
end;

function SBPos(const SubP : string; const P : ByteArray; StartPos : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}) : integer;
var
  I, J : integer;
  Flag : boolean;
begin
  Result := -1;
  for I := StartPos to Length(P) - Length(SubP) do
  begin
    Flag := true;
    for J := AnsiStrOffset to Length(SubP) - 1 + AnsiStrOffset do
    begin
      if P[I + J - AnsiStrOffset] <> Ord(SubP[J]) then
      begin
        Flag := false;
        break;
      end;
    end;
    if Flag then
    begin
      Result := I;
      Exit;
    end;
  end;
end;

// ---------------------------- TSBObjectList ----------------------------------
constructor TSBObjectList.Create;
begin
  inherited Create;
  FOwnsObjects := true;
end;

constructor TSBObjectList.Create(AOwnsObjects: Boolean);
begin
  inherited Create;
  FOwnsObjects := AOwnsObjects;
end;

function TSBObjectList.Add(AObject: TObject): Integer;
begin
  Result := inherited Add(AObject);
end;

function TSBObjectList.FindInstanceOf(AClass: TClass ; AExact: Boolean;
  AStartAt: Integer): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := AStartAt to Count - 1 do
    if (AExact and
      (Self[I].ClassType = AClass)) or
      (not AExact and
      Self[I].InheritsFrom(AClass)) then
    begin
      Result := I;
      break;
    end;
end;

function TSBObjectList.GetItem(Index: Integer): TObject;
begin
  Result := inherited Items[Index];
end;

function TSBObjectList.IndexOf(AObject: TObject): Integer;
begin
  Result := inherited IndexOf(AObject);
end;

procedure TSBObjectList.Insert(Index: Integer; AObject: TObject);
begin
  inherited Insert(Index, AObject);
end;


procedure TSBObjectList.Notify(Ptr: Pointer; Action: TListNotification);
{.$else}
//procedure TSBObjectList.Notify(Ptr: TObject; Action: TListNotification);
begin
  if OwnsObjects then
    if Action = lnDeleted then
      TObject(Ptr).Free;
{$IFDEF VCL50}
  inherited Notify(Ptr, Action);
{$ENDIF}
end;

function TSBObjectList.Remove(AObject: TObject): Integer;
begin
  Result := inherited Remove(AObject);
end;

procedure TSBObjectList.SetItem(Index: Integer; AObject: TObject);
begin
  inherited Items[Index] := AObject;
end;

function TSBObjectList.Extract(Item: TObject): TObject;
var
  I: Integer;
begin
  Result := nil;
  I := IndexOf(Item);
  if I >= 0 then
  begin
    Result := Item;
    List^[I] := nil;
    Delete(I);
    Notify(Result, lnExtracted);
  end;
end;



{$ifdef NET_CF_1_0}
procedure TSBControlBase.Dispose;
begin
  Dispose(true);
end;

[DllImport(COREDLL, CharSet = CharSet.Auto,
  SetLastError = true,
  EntryPoint = 'LocalAlloc')]
function LocalAlloc(Flags: DWORD; Size: Integer): IntPtr; external;
[DllImport(COREDLL, CharSet = CharSet.Auto,
  SetLastError = true,
  EntryPoint = 'LocalFree')]
procedure LocalFree(Ptr: IntPtr); external;

[DllImport(OLE32)]
function CoTaskMemAlloc(cb: Integer): IntPtr; external;
[DllImport(OLE32)]
procedure CoTaskMemFree(Ptr: IntPtr); external;

function AllocCoTaskMem(Size: Integer): IntPtr;
begin
  Result := CoTaskMemAlloc(Size);
  if Result = IntPtr.Zero then
    raise OutOfMemoryException.Create();
end;

procedure FreeCoTaskMem(Ptr: IntPtr);
begin
  CoTaskMemFree(Ptr);
end;

function DateTimeToOADate(const DateTime: System.DateTime): Double;
var
  n, n2: Int64;
begin
  n := DateTime.Ticks;
  if n = 0 then
  begin
    Result := 0;
    Exit;
  end;

  if n < $c92a69c000 then
    n := n + $085103c0cb83c000;

  if n < $6efdddaec64000 then
    raise OverflowException.Create;

  n := (n - $85103c0cb83c000) div $2710;
  if n < 0 then
  begin
    n2 := n mod $5265c00;
    if n2 <> 0 then
     n := n - (($5265c00 + n2) * 2);
  end;

  Result := Double(n)/86400000;
end;

function DateTimeFromOADate(const d: Double): System.DateTime;
var
  n: Int64;
begin
  if (d >= 2958466) or (d <= -657435) then
    raise ArgumentException.Create;

  if d >= 0 then
    n := Int64(d * 86400000 + 0.5)
  else
    n := Int64(d * 86400000 - 0.5);

  if n < 0 then
    n := n - (n mod $5265c00) * 2;

  n := n + $3680b5e1fc00;
  if (n < 0) or (n >= $11efae44cb400) then
    raise ArgumentException.Create;

  Result := System.DateTime.Create(n * $2710);
end;

const
 RegValueKind_DWORD = 4;

var
  HKEY_CURRENT_USER: IntPtr = new IntPtr(-2147483647);
  HKEY_LOCAL_MACHINE: IntPtr = new IntPtr(-2147483646);

[DllImport(COREDLL, CharSet = CharSet.Unicode, EntryPoint = 'RegCreateKeyEx')]
function RegCreateKeyEx(hKey: IntPtr; lpSubKey: string; Reserved: Integer; lpClass: string; dwOptions: Integer; samDesigner: Integer; lpSecurityAttributes: IntPtr; out hkResult: IntPtr; out lpdwDisposition: Integer): Integer; external;
[DllImport(COREDLL, CharSet = CharSet.Unicode, EntryPoint = 'RegOpenKeyEx')]
function RegOpenKeyEx(hKey: IntPtr; lpSubKey: string; ulOptions: Integer; samDesired: Integer; out hkResult: IntPtr): Integer; external;
[DllImport(COREDLL, CharSet = CharSet.Unicode, EntryPoint = 'RegQueryValueEx')]
function RegQueryValueEx(hKey: IntPtr; lpValueName: string; lpReserved: IntPtr; var lpType: Integer; var lpData: DWORD; var lpcbData: Integer): Integer; external;
[DllImport(COREDLL, CharSet = CharSet.Unicode, EntryPoint = 'RegSetValueEx')]
function RegSetValueEx(hKey: IntPtr; lpValueName: string; Reserved: Integer; dwType: Integer; var lpData: DWORD; cbData: Integer): Integer; external;
[DllImport(COREDLL, CharSet = CharSet.Unicode, EntryPoint = 'RegCloseKey')]
function RegCloseKey(hKey: IntPtr): Integer; external;

function RegCreateSubKey(hKey: IntPtr; SubKey: string): IntPtr;
var
  i, t: Integer;
begin
  if SubKey.Chars[Length(SubKey) - 1] = '\' then
    SubKey := SubKey.Substring(0, Length(SubKey) - 1);

  i := RegCreateKeyEx(hKey, SubKey, 0, nil, 0, $2001f, nil, Result, t);
  if i <> 0 then
    Result := IntPtr.Zero;
end;

function RegOpenSubKey(hKey: IntPtr; SubKey: string; Writable: Boolean): IntPtr;
var
  AccessRights, i: Integer;
begin
  if Writable then
    AccessRights := $2001f
  else
    AccessRights := $20019;

  if SubKey.Chars[Length(SubKey) - 1] = '\' then
    SubKey := SubKey.Substring(0, Length(SubKey) - 1);

  i := RegOpenKeyEx(hKey, SubKey, 0, AccessRights, Result);
  if i <> 0 then
    Result := IntPtr.Zero;
end;

procedure RegSetValue(hKey: IntPtr; const ValueName: string; var Value: DWORD);
begin
  RegSetValueEx(hKey, ValueName, 0, RegValueKind_DWORD, Value, 4);
end;

function RegGetValue(hKey: IntPtr; const ValueName: string): System.Object;
var
  VK, Len: Integer;
  Value: DWORD;
begin
  Len := 4;
  RegQueryValueEx(hKey, ValueName, nil, VK, Value, Len);
  if VK <> RegValueKind_DWORD then
    Result := System.Object(nil)
  else
    Result := Value;
end;
{$endif}

{$ifdef NET_CF}
[DllImport(COREDLL, CharSet = CharSet.Unicode, EntryPoint = 'memmove')]
procedure MemMove(pdst: IntPtr; psrc: string; sizetcb: IntPtr); external;
[DllImport(COREDLL, CharSet = CharSet.Unicode, EntryPoint = 'memmove')]
procedure MemMove(pdst: StringBuilder; psrc: IntPtr; sizetcb: IntPtr); external;

[DllImport(COREDLL, CharSet = CharSet.Unicode, EntryPoint = 'strlen')]
function StrLen(Ptr: IntPtr): Integer; external;

function StringToCoTaskMemUni(const s: string): IntPtr;
var
  i: Integer;
begin
  if Length(s) = 0 then
  begin
    Result := IntPtr.Zero;
    Exit;
  end;

  i := (Length(s) + 1) * SizeOf(Char);
  Result := {$ifndef NET_CF_1_0}Marshal.{$endif}AllocCoTaskMem(i);
  MemMove(Result, s, new IntPtr(i));
end;

function PtrToStringUni(Ptr: IntPtr): string;
var
  i: Integer;
  sb: StringBuilder;
begin
  if Ptr = IntPtr.Zero then
  begin
    Result := '';
    Exit;
  end;

  i := StrLen(Ptr);
  sb := StringBuilder.Create(i);
  MemMove(sb, Ptr, new IntPtr(SizeOf(Char) * (1 + i)));
  Result := sb.ToString;
end;

function UnsafeAddrOfPinnedArrayElement(ArrPin: GCHandle): IntPtr;
begin
  {$ifndef NET_CF_1_0}
  Result := ArrPin.AddrOfPinnedObject;
  {$else}
  // bug in NET CF 1.0
  // It returns a pointer to the internal array structure (consists of 32 bit
  // array size followed by array data), not to the array itself.
  if IntPtr.Size = 4 then
    Result := IntPtr(ArrPin.AddrOfPinnedObject.ToInt32 + 4)
  else
    Result := IntPtr(ArrPin.AddrOfPinnedObject.ToInt64 + 4);
  {$endif}
end;
{$endif}

procedure SetLicenseKey(const Key : string);
begin

end;

procedure CheckLicenseKey;
begin
  ;
end;








function WaitFor(Handle: THandle): LongWord;
var
  Msg: TMsg;
  H: THandle;
begin
  H := Handle;
  if GetCurrentThreadID = MainThreadID then
    while MsgWaitForMultipleObjects(1, H, False, INFINITE,
      QS_SENDMESSAGE) = WAIT_OBJECT_0 + 1 do PeekMessage(Msg, 0, 0, 0, PM_NOREMOVE)
  else WaitForSingleObject(H, INFINITE);
  GetExitCodeThread(H, Result);
end;

{$ifndef CUSTOM_ZLIB}
procedure SBRndInit;
begin
  SBRndCreate;
  SBRndSeed('initialization');
  SBRndSeedTime;
end;

procedure SBRndCreate;
begin
  if FGlobalRandom <> nil then
    FreeAndNil(FGlobalRandom);
  FGlobalRandom := TElRandom.Create;
  //InitializeCriticalSection(FRndCriticalSection);
  FRndCriticalSection := TCriticalSection.Create;
  SBRndCreated := true;
end;

procedure SBRndDestroy;
begin
    FreeAndNil(FRndCriticalSection);
    FreeAndNil(FGlobalRandom);
end;

procedure SBRndSeed(const Salt : string {$ifdef HAS_DEF_PARAMS}= ''{$endif});
var
  A : cardinal;
  B : array[0..1] of byte;
  D : double;
  I, J : integer;
  M : TMessageDigest160;
  Ctx : TSHA1Context;
begin
  FRndCriticalSection.Enter;
  FillChar(M, 20, 63);
  for I := 0 to 15 do
  begin
    A := GetTickCount;
    D := Now;
    B[0] := (A shr 24) xor (A and $ff);
    B[1] := ((A shr 16) and $ff) xor ((A shr 8) and $ff);
    InitializeSHA1(Ctx);
    HashSHA1(Ctx, @M, 20);
    for J := 0 to 1023 do
    begin
      HashSHA1(Ctx, @A, 4);
      HashSHA1(Ctx, @D, 8);
      HashSHA1(Ctx, @B[0], 2);
      HashSHA1(Ctx, @Salt[1], Length(Salt));
    end;
    M := FinalizeSHA1(Ctx);
    FGlobalRandom.Seed(@M, 20);
  end;
  FRndCriticalSection.Leave;
end;

procedure SBRndSeed(Buffer: pointer; Size: integer);
begin
  FRndCriticalSection.Enter;
  FGlobalRandom.Seed(Buffer, Size);
  FRndCriticalSection.Leave;
end;

procedure SBRndSeedTime;
var
  C : cardinal;
  D : double;
  A : array[0..47] of byte;
begin
  C := GetTickCount;
  D := Now;
  Move(C, A[0], 4);
  Move(D, A[4], 8);
  Move(C, A[12], 4);
  Move(D, A[16], 8);
  Move(C, A[24], 4);
  Move(D, A[28], 8);
  Move(C, A[36], 4);
  Move(D, A[40], 8);
  SBRndSeed(@A[0], 48);
end;

procedure SBRndGenerate(Buffer: pointer; Size: integer);
begin
  FRndCriticalSection.Enter;
  FGlobalRandom.Generate(Buffer, Size);
  FRndCriticalSection.Leave;
end;


function SBRndGenerate(UpperBound: cardinal{$ifdef HAS_DEF_PARAMS}= 0{$endif}): cardinal;
begin
  SBRndGenerate(@Result, 4);
  if UpperBound <> 0 then
    Result := Result mod UpperBound;
end;

procedure SBRndGenerateLInt(A : PLInt; Bytes : integer);
var
  I : integer;
  Tm: TSBInt64;
begin
  A.Length := Bytes div 4 + 1;
  for I := 1 to A.Length - 1 do
  begin
    A.Digits[I] := 0;
    Tm := SBRndGenerate(256);
    A.Digits[I] := A.Digits[I] or Tm;
    Tm := SBRndGenerate(256);
    A.Digits[I] := A.Digits[I] or (Tm shl 8);
    Tm := SBRndGenerate(256);
    A.Digits[I] := A.Digits[I] or (Tm shl 16);
    Tm := SBRndGenerate(256);
    A.Digits[I] := A.Digits[I] or (Tm shl 24);
  end;
  A.Digits[A.Length] := 0;
  for I := 1 to (Bytes mod 4) do
  begin
    A.Digits[A.Length] := A.Digits[A.Length] or (TSBInt64(SBRndGenerate(256))
      shl ((I - 1) * 8));
  end;
  if (Bytes mod 4) = 0 then A.Length := A.Length - 1;
end;
{$endif}

initialization


  SBRndInit;

finalization
  SBRndDestroy;


end.


