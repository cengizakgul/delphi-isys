(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBPKCS7;

interface

uses
  SBASN1Tree,
  SBUtils,
  SBRDN,
  SBPKCS7Utils,
  Classes,
  SBCustomCertStorage;


const
  SB_PKCS7_ERROR_INVALID_ASN_DATA                          = Integer($1E01);
  SB_PKCS7_ERROR_NO_DATA                                   = Integer($1E02);
  SB_PKCS7_ERROR_INVALID_CONTENT_INFO                      = Integer($1E03);
  SB_PKCS7_ERROR_UNKNOWN_DATA_TYPE                         = Integer($1E04);
  SB_PKCS7_ERROR_INVALID_DATA                              = Integer($1E05);
  SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA                    = Integer($1E06);
  SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA_VERSION            = Integer($1E07);
  SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA_CONTENT            = Integer($1E08);
  SB_PKCS7_ERROR_INVALID_RECIPIENT_INFOS                   = Integer($1E09);
  SB_PKCS7_ERROR_INVALID_RECIPIENT_INFO                    = Integer($1E0A);
  SB_PKCS7_ERROR_INVALID_RECIPIENT_INFO_VERSION            = Integer($1E0B);
  SB_PKCS7_ERROR_INVALID_RECIPIENT_INFO_KEY                = Integer($1E0C);
  SB_PKCS7_ERROR_INVALID_ISSUER                            = Integer($1E0D);
  SB_PKCS7_ERROR_INVALID_ALGORITHM                         = Integer($1E0E);
  SB_PKCS7_ERROR_INVALID_SIGNED_DATA                       = Integer($1E0F);
  SB_PKCS7_ERROR_INVALID_SIGNED_DATA_VERSION               = Integer($1E10);
  SB_PKCS7_ERROR_INVALID_SIGNER_INFOS                      = Integer($1E11);
  SB_PKCS7_ERROR_INVALID_SIGNER_INFO_VERSION               = Integer($1E12);
  SB_PKCS7_ERROR_INVALID_SIGNER_INFO                       = Integer($1E13);
  SB_PKCS7_ERROR_INTERNAL_ERROR                            = Integer($1E14);
  SB_PKCS7_ERROR_INVALID_ATTRIBUTES                        = Integer($1E15);
  SB_PKCS7_ERROR_INVALID_DIGESTED_DATA                     = Integer($1E16);
  SB_PKCS7_ERROR_INVALID_DIGESTED_DATA_VERSION             = Integer($1E17);
  SB_PKCS7_ERROR_INVALID_ENCRYPTED_DATA                    = Integer($1E18);
  SB_PKCS7_ERROR_INVALID_ENCRYPTED_DATA_VERSION            = Integer($1E19);
  SB_PKCS7_ERROR_INVALID_SIGNED_AND_ENVELOPED_DATA         = Integer($1E1A);
  SB_PKCS7_ERROR_INVALID_SIGNED_AND_ENVELOPED_DATA_VERSION = Integer($1E1B);
  SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA                = Integer($1E1C);
  SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA_VERSION        = Integer($1E1D);

type
  TSBPKCS7ContentType =  
   (ctData, ctSignedData, ctEnvelopedData, ctSignedAndEnvelopedData,
    ctDigestedData, ctEncryptedData, ctAuthenticatedData);

  {$hints off}

  TElPKCS7Issuer = class
  private
    FIssuer : TElRelativeDistinguishedName;
    FSerialNumber : BufferType;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TElPKCS7Issuer);
    property Issuer : TElRelativeDistinguishedName read FIssuer;
    property SerialNumber : BufferType read FSerialNumber write FSerialNumber;
  end;

  TElPKCS7Recipient = class
  private
    FVersion : integer;
    FIssuer : TElPKCS7Issuer;
    FKeyEncryptionAlgorithm : BufferType;
    FKeyEncryptionAlgorithmParams : BufferType;
    FEncryptedKey : BufferType;
  public
    constructor Create;
    destructor Destroy; override;
    property Version : integer read FVersion write FVersion;
    property Issuer : TElPKCS7Issuer read FIssuer;
    property KeyEncryptionAlgorithm : BufferType read FKeyEncryptionAlgorithm
      write FKeyEncryptionAlgorithm;
    property KeyEncryptionAlgorithmParams : BufferType read
      FKeyEncryptionAlgorithmParams write FKeyEncryptionAlgorithmParams;
    property EncryptedKey : BufferType read FEncryptedKey write FEncryptedKey;
  end;

  TElPKCS7EncryptedContent = class
  private
    FContentType : BufferType;
    FContentEncryptionAlgorithm : BufferType;
    FContentEncryptionAlgorithmParams : BufferType;
    FEncryptedContent : BufferType;
  public
    constructor Create;
    destructor Destroy; override;
    property ContentType : BufferType read FContentType write FContentType;
    property ContentEncryptionAlgorithm : BufferType read FContentEncryptionAlgorithm
      write FContentEncryptionAlgorithm;
    property ContentEncryptionAlgorithmParams : BufferType read
      FContentEncryptionAlgorithmParams write FContentEncryptionAlgorithmParams;
    property EncryptedContent : BufferType read FEncryptedContent write FEncryptedContent;
  end;

  TElPKCS7EnvelopedData = class
  private
    FVersion : integer;
    FRecipientList : TList;
    FEncryptedContent : TElPKCS7EncryptedContent;
    FContentEncryptionAlgorithm : integer;
    function GetRecipient(Index : integer) : TElPKCS7Recipient;
    function GetRecipientCount : integer;
    procedure Clear;
  public
    constructor Create;
    destructor Destroy; override;
    function AddRecipient : integer;
    function RemoveRecipient(Index : integer) : boolean;
    property Version : integer read FVersion write FVersion;
    property Recipients[Index : integer] : TElPKCS7Recipient read GetRecipient;
    property RecipientCount : integer read GetRecipientCount;
    property EncryptedContent : TElPKCS7EncryptedContent read FEncryptedContent
      write FEncryptedContent;
    property ContentEncryptionAlgorithm : integer read FContentEncryptionAlgorithm
      write FContentEncryptionAlgorithm;
  end;

  TElPKCS7Signer = class
  private
    FVersion : integer;
    FIssuer : TElPKCS7Issuer;
    FDigestAlgorithm : BufferType;
    FDigestAlgorithmParams : BufferType;
    FAuthenticatedAttributes : TElPKCS7Attributes;
    FUnauthenticatedAttributes : TElPKCS7Attributes;
    FDigestEncryptionAlgorithm : BufferType;
    FDigestEncryptionAlgorithmParams : BufferType;
    FEncryptedDigest : BufferType;
    FAuthenticatedAttributesPlain : BufferType;
  protected
    function GetAuthenticatedAttributesPlain : BufferType;
  public
    constructor Create;
    destructor Destroy; override;
    procedure RecalculateAuthenticatedAttributes;
    procedure Assign(Source : TElPKCS7Signer);
    property Version : integer read FVersion write FVersion;
    property Issuer : TElPKCS7Issuer read FIssuer;
    property DigestAlgorithm : BufferType read FDigestAlgorithm write FDigestAlgorithm;
    property DigestAlgorithmParams : BufferType read FDigestAlgorithmParams
      write FDigestAlgorithmParams;
    property DigestEncryptionAlgorithm : BufferType read FDigestEncryptionAlgorithm
      write FDigestEncryptionAlgorithm;
    property DigestEncryptionAlgorithmParams : BufferType read
      FDigestEncryptionAlgorithmParams write FDigestEncryptionAlgorithmParams;
    property AuthenticatedAttributes : TElPKCS7Attributes
      read FAuthenticatedAttributes;
    property UnauthenticatedAttributes : TElPKCS7Attributes
      read FUnauthenticatedAttributes;
    property EncryptedDigest : BufferType read FEncryptedDigest write FEncryptedDigest;
    property AuthenticatedAttributesPlain : BufferType read GetAuthenticatedAttributesPlain;
  end;

  TElPKCS7SignedData = class
  private
    FVersion : integer;
    FSignerList : TList;
    FCertStorage : TElMemoryCertStorage;

    FContent : BufferType;
    FContentType : BufferType;

    function GetSigner(Index : integer) : TElPKCS7Signer;
    function GetSignerCount : integer;
  public
    constructor Create;
    destructor Destroy; override;

    function AddSigner : integer;
    function RemoveSigner(Index : integer) : boolean;
    property Version : integer read FVersion write FVersion;
    property Signers[Index : integer] : TElPKCS7Signer read GetSigner;
    property SignerCount : integer read GetSignerCount;
    property Certificates : TElMemoryCertStorage read FCertStorage;

    property Content : BufferType read FContent write FContent;
    property ContentType : BufferType read FContentType write FContentType;
  end;

  TElPKCS7DigestedData = class
  private
    FVersion : integer;

    FDigestAlgorithm : BufferType;
    FDigestAlgorithmParams : BufferType;
    FContent : BufferType;
    FDigest : BufferType;
  public
    property DigestAlgorithm : BufferType read FDigestAlgorithm write FDigestAlgorithm;
    property DigestAlgorithmParams : BufferType read FDigestAlgorithmParams
      write FDigestAlgorithmParams;
    property Content : BufferType read FContent write FContent;
    property Digest : BufferType read FDigest write FDigest;

    property Version : integer read FVersion write FVersion;
  end;

  TElPKCS7EncryptedData = class
  private
    FVersion : integer;
    FEncryptedContent : TElPKCS7EncryptedContent;
  public
    constructor Create;
    destructor Destroy; override;
    property Version : integer read FVersion write FVersion;
    property EncryptedContent : TElPKCS7EncryptedContent read FEncryptedContent;
  end;

  TElPKCS7SignedAndEnvelopedData = class
  private
    FVersion : integer;
    FRecipientList : TList;
    FSignerList : TList;
    FEncryptedContent : TElPKCS7EncryptedContent;
    FCertStorage : TElMemoryCertStorage;
  public
    constructor Create;
    destructor Destroy; override;

    function GetRecipient(Index : integer) : TElPKCS7Recipient;
    function GetRecipientCount : integer;
    function GetSigner(Index : integer) : TElPKCS7Signer;
    function GetSignerCount : integer;
    function AddRecipient : integer;
    function AddSigner : integer;
    function RemoveRecipient(Index : integer) : boolean;
    function RemoveSigner(Index : integer) : boolean;
    property Version : integer read FVersion write FVersion;
    property Recipients[Index : integer] : TElPKCS7Recipient read GetRecipient;
    property RecipientCount : integer read GetRecipientCount;
    property EncryptedContent : TElPKCS7EncryptedContent read FEncryptedContent;
    property Certificates : TElMemoryCertStorage read FCertStorage;
    property Signers[Index : integer] : TElPKCS7Signer read GetSigner;
    property SignerCount : integer read GetSignerCount;
  end;

  TElPKCS7AuthenticatedData = class
  private
    FVersion : integer;
    FOriginatorCerts : TElCustomCertStorage;
    FMacAlgorithm : BufferType;
    FMacAlgorithmParams : BufferType;
    FDigestAlgorithm : BufferType;
    FDigestAlgorithmParams : BufferType;
    FContentType : BufferType;
    FContent : BufferType;
    FAuthenticatedAttributes : TElPKCS7Attributes;
    FUnauthenticatedAttributes : TElPKCS7Attributes;
    FMac : BufferType;
    FRecipientList : TList;
    FAuthenticatedAttributesPlain : BufferType;
    function GetRecipient(Index: integer) : TElPKCS7Recipient;
    function GetRecipientCount: integer;
    function GetAuthenticatedAttributesPlain: BufferType;
  public
    constructor Create;
    destructor Destroy; override;

    function AddRecipient : integer;
    function RemoveRecipient(Index : integer) : boolean;
    procedure RecalculateAuthenticatedAttributes;
    property Version: integer read FVersion write FVersion;
    property OriginatorCerts : TElCustomCertStorage read FOriginatorCerts;
    property Recipients[Index: integer] : TElPKCS7Recipient read GetRecipient;
    property RecipientCount: integer read GetRecipientCount;
    property MacAlgorithm: BufferType read FMacAlgorithm write FMacAlgorithm;
    property DigestAlgorithm: BufferType read FDigestAlgorithm write FDigestAlgorithm;
    property ContentType : BufferType read FContentType write FContentType;
    property Content : BufferType read FContent write FContent;
    property AuthenticatedAttributes : TElPKCS7Attributes read FAuthenticatedAttributes;
    property UnauthenticatedAttributes : TElPKCS7Attributes read FUnauthenticatedAttributes;
    property Mac : BufferType read FMac write FMac;
    property AuthenticatedAttributesPlain: BufferType read GetAuthenticatedAttributesPlain;
  end;

  TElPKCS7Message = class
  private
    FMessage : TElASN1ConstrainedTag;
    FContentType : TSBPKCS7ContentType;
    FData : BufferType;
    FUseImplicitContent: Boolean;
    FUseUndefSize: Boolean;
    FEnvelopedData : TElPKCS7EnvelopedData;
    FSignedData : TElPKCS7SignedData;
    FDigestedData : TElPKCS7DigestedData;
    FEncryptedData : TElPKCS7EncryptedData;
    FSignedAndEnvelopedData : TElPKCS7SignedAndEnvelopedData;
    FAuthenticatedData : TElPKCS7AuthenticatedData;
  protected
    { Common PKCS7 routines }
    function ProcessMessage : integer;
    function ProcessData(Tag : TElASN1CustomTag) : integer; virtual;
    function ProcessSignedData(Tag : TElASN1CustomTag) : integer; virtual;
    function ProcessEnvelopedData(Tag : TElASN1CustomTag) : integer; virtual;
    function ProcessSignedEnvelopedData(Tag : TElASN1CustomTag) : integer; virtual;
    function ProcessDigestData(Tag : TElASN1CustomTag) : integer; virtual;
    function ProcessEncryptedData(Tag : TElASN1CustomTag) : integer; virtual;
    function ProcessAuthenticatedData(Tag : TElASN1CustomTag) : integer; virtual;
    { Auxiliary PKCS7 routines }
    function ProcessRecipientInfos(Tag : TElASN1CustomTag; RecipientList : TList) : integer;
    function ProcessRecipientInfo(Tag : TElASN1CustomTag; Recipient :
      TElPKCS7Recipient) : integer; virtual;
    function ProcessEncryptedContentInfo(Tag : TElASN1ConstrainedTag;
      EncryptedContent : TElPKCS7EncryptedContent) : integer;
    procedure ProcessCertificates(Tag : TElASN1ConstrainedTag; Storage :
      TElCustomCertStorage);
    function ProcessSignerInfos(Tag : TElASN1CustomTag; SignerList : TList) : integer;
    procedure Clear;
    procedure SaveEnvelopedData(Tag : TElASN1ConstrainedTag);
    procedure SaveRecipientInfos(Tag : TElASN1ConstrainedTag; RecipientList : TList);
    procedure SaveRecipientInfo(Tag : TElASN1ConstrainedTag; Recipient :
      TElPKCS7Recipient);
    procedure SaveEncryptedContentInfo(Tag : TElASN1ConstrainedTag; EncryptedContent :
      TElPKCS7EncryptedContent);
    procedure SaveSignedData(Tag : TElASN1ConstrainedTag);
    procedure SaveCertificates(Storage : TElCustomCertStorage; Tag : TElASN1ConstrainedTag);
    procedure SaveSignerInfos(Tag : TElASN1ConstrainedTag; SignerList : TList);
    procedure SaveDigestedData(Tag : TElASN1ConstrainedTag);
    procedure SaveEncryptedData(Tag : TElASN1ConstrainedTag);
    procedure SaveSignedAndEnvelopedData(Tag : TElASN1ConstrainedTag);
    procedure SaveAuthenticatedData(Tag : TElASN1ConstrainedTag);
  public
    constructor Create;
    destructor Destroy; override;

    function LoadFromBuffer(Buffer : pointer; Size : integer) : integer;
    function SaveToBuffer(Buffer : pointer; var Size : integer) : boolean;
    property ContentType : TSBPKCS7ContentType read FContentType write FContentType;
    property Data : BufferType read FData write FData;

    property EnvelopedData : TElPKCS7EnvelopedData read FEnvelopedData;
    property SignedData : TElPKCS7SignedData read FSignedData;
    property DigestedData : TElPKCS7DigestedData read FDigestedData;
    property EncryptedData : TElPKCS7EncryptedData read FEncryptedData;
    property SignedAndEnvelopedData : TElPKCS7SignedAndEnvelopedData
      read FSignedAndEnvelopedData;
    property AuthenticatedData : TElPKCS7AuthenticatedData read FAuthenticatedData;
    property UseImplicitContent: Boolean read FUseImplicitContent write FUseImplicitContent;
    property UseUndefSize: Boolean read FUseUndefSize write FUseUndefSize;
  end;

  {$hints on}


function ProcessAlgorithmIdentifier(Tag : TElASN1CustomTag; var Algorithm :
  string; var Params : string; ImplicitTagging : boolean = false) : integer;
function ProcessContentInfo(Tag : TElASN1ConstrainedTag; Buffer : pointer;
  var Size : integer; var ContentType : string) : boolean;
procedure SaveAlgorithmIdentifier(Tag : TElASN1ConstrainedTag; const Algorithm :
  string; const Params : string; ImplicitTag : byte = 0);
procedure SaveSignerInfo(Tag : TElASN1ConstrainedTag; Signer : TElPKCS7Signer);
procedure SaveIssuerAndSerialNumber(Tag : TElASN1ConstrainedTag; Issuer : TElPKCS7Issuer);
function ProcessSignerInfo(Tag : TElASN1CustomTag; SignerInfo : TElPKCS7Signer) : integer;
function ProcessIssuerAndSerialNumber(Tag : TElASN1CustomTag; Issuer : TElPKCS7Issuer): integer;
function ProcessAttributes(Tag : TElASN1CustomTag; Attributes : TElPKCS7Attributes) : integer;

type
  EPKCS7Error =  class(ESecureBlackboxError);

procedure RaisePKCS7Error(ErrorCode : integer); 

implementation

uses
  SysUtils, SBX509;

const

  SB_OID_PKCS7_DATA	                     :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$07#$01;
  SB_OID_PKCS7_SIGNED_DATA		     :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$07#$02;
  SB_OID_PKCS7_ENVELOPED_DATA		     :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$07#$03;
  SB_OID_PKCS7_SIGNED_AND_ENVELOPED_DATA     :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$07#$04;
  SB_OID_PKCS7_DIGESTED_DATA		     :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$07#$05;
  SB_OID_PKCS7_ENCRYPTED_DATA		     :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$07#$06;
  SB_OID_PKCS7_AUTHENTICATED_DATA            :TBufferTypeConst =  #$2A#$86#$48#$86#$F7#$0D#$01#$09#$10#$01#$02;

resourcestring

  sPKCS7Error = 'PKCS#7 error';

procedure RaisePKCS7Error(ErrorCode : integer);
begin
  if ErrorCode <> 0 then
    raise EPKCS7Error.Create(sPKCS7Error + '#' + IntToStr(ErrorCode));
end;

////////////////////////////////////////////////////////////////////////////////
// TElPKCS7Message

constructor TElPKCS7Message.Create;
begin
  inherited;
  FMessage := TElASN1ConstrainedTag.Create;
  FEnvelopedData := TElPKCS7EnvelopedData.Create;
  FSignedData := TElPKCS7SignedData.Create;
  FDigestedData := TElPKCS7DigestedData.Create;
  FEncryptedData := TElPKCS7EncryptedData.Create;
  FSignedAndEnvelopedData := TElPKCS7SignedAndEnvelopedData.Create;
  FAuthenticatedData := TElPKCS7AuthenticatedData.Create;
  FUseImplicitContent := False;
  FUseUndefSize := True;
end;

destructor TElPKCS7Message.Destroy;
begin
  FMessage.Free;
  FEnvelopedData.Free;
  FSignedData.Free;
  FDigestedData.Free;
  FEncryptedData.Free;
  FSignedAndEnvelopedData.Free;
  FAuthenticatedData.Free;
  inherited;
end;

function TElPKCS7Message.LoadFromBuffer(Buffer : pointer; Size : integer) : integer;
begin
  CheckLicenseKey();
  FMessage.Clear;
  Clear;
  if FMessage.LoadFromBufferSingle(Buffer, Size) = -1 then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ASN_DATA;
    FMessage.Clear;
    Exit;
  end;
  Result := ProcessMessage;
end;

function TElPKCS7Message.SaveToBuffer(Buffer : pointer; var Size : integer) : boolean;
var
  STag : TElASN1SimpleTag;
  CTag : TElASN1ConstrainedTag;
begin
  FMessage.Clear;
  FMessage.TagId := SB_ASN1_SEQUENCE;
  FMessage.UndefSize := FUseUndefSize;
  { Writing ContentType }
  STag := TElASN1SimpleTag(FMessage.GetField(FMessage.AddField(false)));
  STag.TagId := SB_ASN1_OBJECT;
  CTag := TElASN1ConstrainedTag(FMessage.GetField(FMessage.AddField(true)));
  CTag.TagId := SB_ASN1_A0;
  CTag.UndefSize := FUseUndefSize;
  if FContentType = ctEnvelopedData then
  begin
    STag.Content := SB_OID_PKCS7_ENVELOPED_DATA;
    SaveEnvelopedData(CTag);
  end
  else
  if FContentType = ctSignedData then
  begin
    STag.Content := SB_OID_PKCS7_SIGNED_DATA;
    SaveSignedData(CTag);
  end
  else if FContentType = ctDigestedData then
  begin
    STag.Content := SB_OID_PKCS7_DIGESTED_DATA;
    SaveDigestedData(CTag);
  end
  else if FContentType = ctEncryptedData then
  begin
    STag.Content := SB_OID_PKCS7_ENCRYPTED_DATA;
    SaveEncryptedData(CTag);
  end
  else if FContentType = ctData then
  begin
    STag.Content := SB_OID_PKCS7_DATA;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagId := SB_ASN1_OCTETSTRING;
    STag.Content := CloneBuffer(FData);
  end
  else if FContentType = ctSignedAndEnvelopedData then
  begin
    STag.Content := SB_OID_PKCS7_SIGNED_AND_ENVELOPED_DATA;
    SaveSignedAndEnvelopedData(CTag);
  end
  else if FContentType = ctAuthenticatedData then
  begin
    STag.Content := SB_OID_PKCS7_AUTHENTICATED_DATA;
    SaveAuthenticatedData(CTag);
  end;
  Result := FMessage.SaveToBuffer(Buffer, Size);
end;

function TElPKCS7Message.ProcessMessage : integer;
var
  Tag, ObjTag, InnerTag : TElASN1CustomTag;
begin
  if FMessage.Count < 1 then
  begin
    Result := SB_PKCS7_ERROR_NO_DATA;
    Exit;
  end;
  Tag := FMessage.GetField(0);
  if not Tag.IsConstrained then
  begin
    Result := SB_PKCS7_ERROR_INVALID_CONTENT_INFO;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).Count < 1 then
  begin
    Result := SB_PKCS7_ERROR_INVALID_CONTENT_INFO;
    Exit;
  end;
  ObjTag := TElASN1ConstrainedTag(Tag).GetField(0);
  if (ObjTag.IsConstrained) or (ObjTag.TagId <> SB_ASN1_OBJECT) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_CONTENT_INFO;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).Count >= 2 then
  begin
    InnerTag := TElASN1ConstrainedTag(Tag).GetField(1);
    if not InnerTag.IsConstrained then
    begin
      Result := SB_PKCS7_ERROR_INVALID_CONTENT_INFO;
      Exit;
    end;
    if TElASN1ConstrainedTag(InnerTag).Count < 1 then
    begin
      Result := SB_PKCS7_ERROR_INVALID_CONTENT_INFO;
      Exit;
    end;
    InnerTag := TElASN1ConstrainedTag(InnerTag).GetField(0);
  end
  else
    InnerTag := nil;
  if CompareContent(TElASN1SimpleTag(ObjTag).Content, SB_OID_PKCS7_DATA) then
  begin
    FContentType := ctData;
    Result := ProcessData(InnerTag);
  end
  else
  if CompareContent(TElASN1SimpleTag(ObjTag).Content, SB_OID_PKCS7_SIGNED_DATA) then
  begin
    FContentType := ctSignedData;
    Result := ProcessSignedData(InnerTag);
  end
  else
  if CompareContent(TElASN1SimpleTag(ObjTag).Content, SB_OID_PKCS7_ENVELOPED_DATA) then
  begin
    FContentType := ctEnvelopedData;
    Result := ProcessEnvelopedData(InnerTag);
  end
  else
  if CompareContent(TElASN1SimpleTag(ObjTag).Content, SB_OID_PKCS7_SIGNED_AND_ENVELOPED_DATA) then
  begin
    FContentType := ctSignedAndEnvelopedData;
    Result := ProcessSignedEnvelopedData(InnerTag);
  end
  else
  if CompareContent(TElASN1SimpleTag(ObjTag).Content, SB_OID_PKCS7_DIGESTED_DATA) then
  begin
    FContentType := ctDigestedData;
    Result := ProcessDigestData(InnerTag);
  end
  else
  if CompareContent(TElASN1SimpleTag(ObjTag).Content, SB_OID_PKCS7_ENCRYPTED_DATA) then
  begin
    FContentType := ctEncryptedData;
    Result := ProcessEncryptedData(InnerTag);
  end
  else
  if CompareContent(TElASN1SimpleTag(ObjTag).Content, SB_OID_PKCS7_AUTHENTICATED_DATA) then
  begin
    FContentType := ctAuthenticatedData;
    Result := ProcessAuthenticatedData(InnerTag);
  end
  else
    Result := SB_PKCS7_ERROR_UNKNOWN_DATA_TYPE;
end;

function TElPKCS7Message.ProcessData(Tag : TElASN1CustomTag) : integer;
var
  MsgSize : integer;
begin
  if not Assigned(Tag) then
  begin
    SetLEngth(FData, 0);
    Result := 0;
    Exit;
  end;
  if (Tag.TagNum <> SB_ASN1_OCTETSTRING) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_DATA;
    Exit;
  end;
  if Tag.IsConstrained then
  begin
    MsgSize := 0;
    TElASN1ConstrainedTag(Tag).SaveContentToBuffer(nil, MsgSize);
    SetLength(FData, MsgSize);
    TElASN1ConstrainedTag(Tag).SaveContentToBuffer(@FData[1], MsgSize);
    SetLength(FData, MsgSize);
  end
  else
    FData := TElASN1SimpleTag(Tag).Content;
  Result := 0;
end;

function TElPKCS7Message.ProcessSignedData(Tag : TElASN1CustomTag) : integer;
var
  STag : TElASN1SimpleTag;
  Sz : integer;
  TmpS  : BufferType;
begin
  if (not Tag.IsConstrained) or (Tag.TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_DATA;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).Count < 4 then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_DATA;
    Exit;
  end;
  if (TElASN1ConstrainedTag(Tag).GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(0).TagId <> SB_ASN1_INTEGER) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_DATA;
    Exit;
  end;
  STag := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(0));

  if (Length(STag.Content) < 1) or (Ord(STag.Content[Length(STag.Content)]) < 1) or (Ord(STag.Content[Length(STag.Content)]) > 3) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_DATA_VERSION;
    Exit;
  end;

  FSignedData.FVersion :=
    Ord(STag.Content[Length(STag.Content)])
    ;
    
  { Checking digestAlgorithms }
  if (not TElASN1ConstrainedTag(Tag).GetField(1).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(1).TagId <> SB_ASN1_SET) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_DATA;
    Exit;
  end;
  { Reading ContentInfo }
  if (not TElASN1ConstrainedTag(Tag).GetField(2).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(2).TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_DATA;
    Exit;
  end;
  Sz := 0;

  ProcessContentInfo(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(2)), nil, Sz, TmpS);
  SetLength(SignedData.FContent, Sz);
  if not ProcessContentInfo(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(2)),
    @(SignedData.FContent[1]), Sz, TmpS) then
  begin
    SetLength(SignedData.FContent, 0);
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_DATA;
    Exit;
  end;
  SignedData.ContentType := TmpS;
  { Processing certificates }
  if (TElASN1ConstrainedTag(Tag).GetField(3).IsConstrained) and
    (TElASN1ConstrainedTag(Tag).GetField(3).TagId = SB_ASN1_A0) and
    (TElASN1ConstrainedTag(Tag).Count > 4) then
  begin
    ProcessCertificates(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(3)),
      FSignedData.FCertStorage);
    if (TElASN1ConstrainedTag(Tag).GetField(4).IsConstrained) and
      (TElASN1ConstrainedTag(Tag).GetField(4).TagId = SB_ASN1_A1) and
      (TElASN1ConstrainedTag(Tag).Count > 5) then
    begin
      if (TElASN1ConstrainedTag(Tag).GetField(5).IsConstrained) and
        (TElASN1ConstrainedTag(Tag).GetField(5).TagId = SB_ASN1_SET) then
      begin
        Result := ProcessSignerInfos(TElASN1ConstrainedTag(Tag).GetField(5),
          SignedData.FSignerList);
      end
      else
      begin
        Result := SB_PKCS7_ERROR_INVALID_SIGNED_DATA;
        Exit;
      end;
    end
    else if (TElASN1ConstrainedTag(Tag).GetField(4).IsConstrained) and
      (TElASN1ConstrainedTag(Tag).GetField(4).TagId = SB_ASN1_SET) then
    begin
      Result := ProcessSignerInfos(TElASN1ConstrainedTag(Tag).GetField(4),
        SignedData.FSignerList);
    end
    else
    begin
      Result := SB_PKCS7_ERROR_INVALID_SIGNED_DATA;
      Exit;
    end;
  end
  else if (TElASN1ConstrainedTag(Tag).GetField(3).IsConstrained) and
    (TElASN1ConstrainedTag(Tag).GetField(3).TagId = SB_ASN1_A1) and
    (TElASN1ConstrainedTag(Tag).Count > 4) then
  begin
    if (TElASN1ConstrainedTag(Tag).GetField(4).IsConstrained) and
      (TElASN1ConstrainedTag(Tag).GetField(4).TagId = SB_ASN1_SET) then
    begin
      Result := ProcessSignerInfos(TElASN1ConstrainedTag(Tag).GetField(4),
        SignedData.FSignerList);
    end
    else
    begin
      Result := SB_PKCS7_ERROR_INVALID_SIGNED_DATA;
      Exit;
    end;
  end
  else if (TElASN1ConstrainedTag(Tag).GetField(3).IsConstrained) and
    (TElASN1ConstrainedTag(Tag).GetField(3).TagId = SB_ASN1_SET) then
  begin
    Result := ProcessSignerInfos(TElASN1ConstrainedTag(Tag).GetField(3),
      SignedData.FSignerList);
  end
  else
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_DATA;
    Exit;
  end;
end;

function TElPKCS7Message.ProcessEnvelopedData(Tag : TElASN1CustomTag) : integer;
var
  STag : TElASN1SimpleTag;
begin
  if (not Tag.IsConstrained) or (Tag.TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).Count <> 3 then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA;
    Exit;
  end;
  { Reading version (should be 0 for this version of standard }
  if (TElASN1ConstrainedTag(Tag).GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(0).TagId <> SB_ASN1_INTEGER) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA;
    Exit;
  end;
  STag := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(0));
  if (STag.Content[Length(STag.Content)] <> #0) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA_VERSION;
    Exit;
  end;
  { Reading RecipientInfos }
  Result := ProcessRecipientInfos(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(1)),
    FEnvelopedData.FRecipientList);
  if Result <> 0 then
    Exit;
  { Reading EncryptedContentInfo }
  if (not TElASN1ConstrainedTag(Tag).GetField(2).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(2).TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA;
    Exit;
  end;
  Result := ProcessEncryptedContentInfo(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(2)),
    FEnvelopedData.EncryptedContent);
end;

function TElPKCS7Message.ProcessSignedEnvelopedData(Tag : TElASN1CustomTag) : integer;
begin
  if (not Tag.IsConstrained) or (Tag.TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_AND_ENVELOPED_DATA;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).Count < 5 then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_AND_ENVELOPED_DATA;
    Exit;
  end;
  if (TElASN1ConstrainedTag(Tag).GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(0).TagId <> SB_ASN1_INTEGER) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_AND_ENVELOPED_DATA;
    Exit;
  end;
  if not CompareContent(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(0)).Content, TBufferTypeConst(#1)) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_AND_ENVELOPED_DATA_VERSION;
    Exit;
  end;
  if (not TElASN1ConstrainedTag(Tag).GetField(1).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(1).TagId <> SB_ASN1_SET) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_AND_ENVELOPED_DATA;
    Exit;
  end;
  Result := ProcessRecipientInfos(TElASN1ConstrainedTag(Tag).GetField(1),
    FSignedAndEnvelopedData.FRecipientList);
  if Result <> 0 then
    Exit;
  if (not TElASN1ConstrainedTag(Tag).GetField(3).IsConstrained) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_AND_ENVELOPED_DATA;
    Exit;
  end;
  Result := ProcessEncryptedContentInfo(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(3)),
    FSignedAndEnvelopedData.FEncryptedContent);
  if Result <> 0 then
    Exit;
  if not TElASN1ConstrainedTag(Tag).GetField(4).IsConstrained then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_AND_ENVELOPED_DATA;
    Exit;
  end;
  if (TElASN1ConstrainedTag(Tag).GetField(4).TagId = SB_ASN1_A0) and
    (TElASN1ConstrainedTag(Tag).Count > 5) then
  begin
    ProcessCertificates(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(4)),
      FSignedAndEnvelopedData.FCertStorage);
    if (TElASN1ConstrainedTag(Tag).GetField(5).TagId = SB_ASN1_A1)
      and (TElASN1ConstrainedTag(Tag).Count > 6) then
    begin
      Result := ProcessSignerInfos(TElASN1ConstrainedTag(Tag).GetField(6),
        FSignedAndEnvelopedData.FSignerList);
    end
    else if TElASN1ConstrainedTag(Tag).GetField(5).TagId = SB_ASN1_SET then
    begin
      Result := ProcessSignerInfos(TElASN1ConstrainedTag(Tag).GetField(5),
        FSignedAndEnvelopedData.FSignerList);
    end
    else
      Result := SB_PKCS7_ERROR_INVALID_SIGNED_AND_ENVELOPED_DATA;
  end
  else if (TElASN1ConstrainedTag(Tag).GetField(4).TagId = SB_ASN1_A1) and
    (TElASN1ConstrainedTag(Tag).Count > 5) then
  begin
    if (not TElASN1ConstrainedTag(Tag).GetField(5).IsConstrained) or
      (TElASN1ConstrainedTag(Tag).GetField(5).TagId <> SB_ASN1_SET) then
    begin
      Result := SB_PKCS7_ERROR_INVALID_SIGNED_AND_ENVELOPED_DATA;
      Exit;
    end;
    Result := ProcessSignerInfos(TElASN1ConstrainedTag(Tag).GetField(5),
      FSignedAndEnvelopedData.FSignerList);
  end
  else if TElASN1ConstrainedTag(Tag).GetField(4).TagId = SB_ASN1_SET then
  begin
    Result := ProcessSignerInfos(TElASN1ConstrainedTag(Tag).GetField(4),
      FSignedAndEnvelopedData.FSignerList);
  end
  else
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNED_AND_ENVELOPED_DATA;
    Exit;
  end;
end;

function TElPKCS7Message.ProcessDigestData(Tag : TElASN1CustomTag) : integer;
var
  CT : string;
  Cont : string;
  Sz : integer;
begin
  if not (Tag.IsConstrained) or (Tag.TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_DIGESTED_DATA;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).Count <> 4 then
  begin
    Result := SB_PKCS7_ERROR_INVALID_DIGESTED_DATA;
    Exit;
  end;
  if (TElASN1ConstrainedTag(Tag).GetField(0).TagId <> SB_ASN1_INTEGER) or
    (TElASN1ConstrainedTag(Tag).GetField(0).IsConstrained) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_DIGESTED_DATA;
    Exit;
  end;
  if not CompareContent(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(0)).Content, TBufferTypeConst(#0)) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_DIGESTED_DATA_VERSION;
    Exit;
  end;
  ProcessAlgorithmIdentifier(TElASN1ConstrainedTag(Tag).GetField(1),
    FDigestedData.FDigestAlgorithm, FDigestedData.FDigestAlgorithmParams {$ifndef HAS_DEF_PARAMS}, False{$endif});
  if (not TElASN1ConstrainedTag(Tag).GetField(2).IsConstrained) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_DIGESTED_DATA;
    Exit;
  end;
  Sz := 0;
  ProcessContentInfo(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(2)),
    nil, Sz, CT);
  SetLength(Cont, Sz);
  ProcessContentInfo(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(2)),
    @Cont[1], Sz, CT);
  if (TElASN1Constrainedtag(Tag).GetField(3).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(3).TagId <> SB_ASN1_OCTETSTRING) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_DIGESTED_DATA;
    Exit;
  end;
  Result := 0;
end;

function TElPKCS7Message.ProcessEncryptedData(Tag : TElASN1CustomTag) : integer;
begin
  if not (Tag.IsConstrained) or (Tag.TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENCRYPTED_DATA;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).Count <> 2 then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENCRYPTED_DATA;
    Exit;
  end;
  if (TElASN1ConstrainedTag(Tag).GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(0).TagId <> SB_ASN1_INTEGER) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENCRYPTED_DATA;
    Exit;
  end;
  if not CompareContent(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(0)).Content, TBufferTypeConst(#0)) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENCRYPTED_DATA_VERSION;
    Exit;
  end;
  if (not TElASN1ConstrainedTag(Tag).GetField(1).IsConstrained) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENCRYPTED_DATA;
    Exit;
  end;
  Result := ProcessEncryptedContentInfo(
    TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(1)),
    FEncryptedData.FEncryptedContent);
end;

function TElPKCS7Message.ProcessAuthenticatedData(Tag : TElASN1CustomTag) : integer;
var
  CurrParamIndex : integer;
  Size : integer;
begin
  if not (Tag.IsConstrained) or (Tag.TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENCRYPTED_DATA;
    Exit;
  end;
  CurrParamIndex := 0;

  // reading version
  if (CurrParamIndex >= TElASN1ConstrainedTag(Tag).Count) or
    (TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex).TagID <> SB_ASN1_INTEGER) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA;
    Exit;
  end;
  if not CompareContent(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex)).Content,
    TBufferTypeConst(#0)) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA_VERSION;
    Exit;
  end;
  Inc(CurrParamIndex);

  // reading OriginatorInfo
  if (CurrParamIndex >= TElASN1ConstrainedTag(Tag).Count) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex).TagId = SB_ASN1_A0 then
  begin
    // OriginatorInfo is not supported at the moment. Skipping.
    Inc(CurrParamIndex);
  end;

  // reading RecipientInfos
  if CurrParamIndex >= TElASN1ConstrainedTag(Tag).Count then
  begin
    Result := SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA;
    Exit;
  end;
  Result := ProcessRecipientInfos(TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex),
    FAuthenticatedData.FRecipientList);
  if Result <> 0 then
    Exit;
  Inc(CurrParamIndex);

  // reading MacAlgorithm
  if CurrParamIndex >= TElASN1ConstrainedTag(Tag).Count then
  begin
    Result := SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA;
    Exit;
  end;
  Result := ProcessAlgorithmIdentifier(TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex),
    FAuthenticatedData.FMacAlgorithm, FAuthenticatedData.FMacAlgorithmParams {$ifndef HAS_DEF_PARAMS}, False{$endif});
  if Result <> 0 then
    Exit;
  Inc(CurrParamIndex);

  // reading DigestAlgorithm 
  if CurrParamIndex >= TElASN1ConstrainedTag(Tag).Count then
  begin
    Result := SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex).TagId = SB_ASN1_A1 then
  begin
    Result := ProcessAlgorithmIdentifier(TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex),
      FAuthenticatedData.FDigestAlgorithm, FAuthenticatedData.FDigestAlgorithmParams,
      true);
    if Result <> 0 then
      Exit;
    Inc(CurrParamIndex);
  end;

  // reading EncapsulatedContentInfo
  if (CurrParamIndex >= TElASN1ConstrainedTag(Tag).Count) or
    (not TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex).IsConstrained) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA;
    Exit;
  end;
  Size := 0;
  ProcessContentInfo(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex)),
    nil, Size, FAuthenticatedData.FContentType);
  SetLength(FAuthenticatedData.FContent, Size);
  ProcessContentInfo(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex)),
    @FAuthenticatedData.FContent[1],
    Size, FAuthenticatedData.FContentType);
  SetLength(FAuthenticatedData.FContent, Size);
  Inc(CurrParamIndex);

  // reading AuthenticatedAttributes
  if (CurrParamIndex >= TElASN1ConstrainedTag(Tag).Count) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA;
    Exit;
  end;
  if (TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex).TagID = SB_ASN1_A2)
    and (TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex).IsConstrained) then
  begin
    Result := ProcessAttributes(TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex),
      FAuthenticatedData.FAuthenticatedAttributes);
    if Result <> 0 then
      Exit;
    Inc(CurrParamIndex);
  end;

  // reading Mac
  if (CurrParamIndex >= TElASN1ConstrainedTag(Tag).Count) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA;
    Exit;
  end;
  if (TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex).TagID <> SB_ASN1_OCTETSTRING) or
    (TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex).IsConstrained) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA;
    Exit;
  end;
  FAuthenticatedData.FMac := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex)).Content;
  Inc(CurrParamIndex);

  // reading UnauthenticatedAttributes
  if CurrParamIndex < TElASN1ConstrainedTag(Tag).Count then
  begin
    if (not TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex).IsConstrained) or
      (TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex).TagId <> SB_ASN1_A3) then
    begin
      Result := SB_PKCS7_ERROR_INVALID_AUTHENTICATED_DATA;
      Exit;
    end;
    Result := ProcessAttributes(TElASN1ConstrainedTag(Tag).GetField(CurrParamIndex),
      FAuthenticatedData.FUnauthenticatedAttributes);
  end;
end;

procedure TElPKCS7Message.Clear;
begin
  SetLength(FData, 0);
end;

function TElPKCS7Message.ProcessRecipientInfos(Tag : TElASN1CustomTag; RecipientList:
  TList) : integer;
var
  I : integer;
  Recipient : TElPKCS7Recipient;
begin
  if (not Tag.IsConstrained) or (Tag.TagId <> SB_ASN1_SET) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_RECIPIENT_INFOS;
    Exit;
  end;
  for I := 0 to TElASN1ConstrainedTag(Tag).Count - 1 do
  begin
    Recipient := TElPKCS7Recipient.Create;
    RecipientList.Add(Recipient);
    Result := ProcessRecipientInfo(TElASN1ConstrainedTag(Tag).GetField(I),
      Recipient);
    if Result <> 0 then
      Exit;
  end;
  Result := 0;
end;

function TElPKCS7Message.ProcessRecipientInfo(Tag : TElASN1CustomTag; Recipient :
  TElPKCS7Recipient) : integer;
var
  STag : TElASN1SimpleTag;
begin
  if (not Tag.IsConstrained) or (Tag.TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_RECIPIENT_INFO;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).Count < 4 then
  begin
    Result := SB_PKCS7_ERROR_INVALID_RECIPIENT_INFO;
    Exit;
  end;
  { Reading version }
  if (TElASN1ConstrainedTag(Tag).GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(0).TagId <> SB_ASN1_INTEGER) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_RECIPIENT_INFO;
    Exit;
  end;
  STag := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(0));
  if (STag.Content[Length(STag.Content)] <> #0) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_RECIPIENT_INFO_VERSION;
    Exit;
  end;
  { Reading IssuerAndSerialNumber }
  Result := ProcessIssuerAndSerialNumber(TElASN1ConstrainedTag(Tag).GetField(1),
    Recipient.Issuer);
  if Result <> 0 then
    Exit;
  { Reading KeyEncryptionAlgorithm }
  Result := ProcessAlgorithmIdentifier(TElASN1ConstrainedTag(Tag).GetField(2),
    Recipient.FKeyEncryptionAlgorithm, Recipient.FKeyEncryptionAlgorithmParams {$ifndef HAS_DEF_PARAMS}, False{$endif});
  if Result <> 0 then
    Exit;
  { Reading EncryptedKey }
  if (TElASN1ConstrainedTag(Tag).GetField(3).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(3).TagId <> SB_ASN1_OCTETSTRING) then
  begin
    Result:= SB_PKCS7_ERROR_INVALID_RECIPIENT_INFO_KEY;
    Exit;
  end;
  Recipient.FEncryptedKey := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(3)).Content;
  Result := 0;
end;

function ProcessIssuerAndSerialNumber(Tag : TElASN1CustomTag;
  Issuer : TElPKCS7Issuer): integer;
var
  T, InnerT, SeqT : TElASN1CustomTag;
  I, Sz : integer;
  TagID : byte;
  Value : string;
begin
  if (not Tag.IsConstrained) or (Tag.TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ISSUER;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).Count < 2 then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ISSUER;
    Exit;
  end;
  { Reading Issuer RDN }
  T := TElASN1ConstrainedTag(Tag).GetField(0);
  if (not T.IsConstrained) or (T.TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ISSUER;
    Exit;
  end;
  for I := 0 to TElASN1ConstrainedTag(T).Count - 1 do
  begin
    InnerT := TElASN1ConstrainedTag(T).GetField(I);
    if (not InnerT.IsConstrained) or (InnerT.TagId <> SB_ASN1_SET) then
    begin
      Result := SB_PKCS7_ERROR_INVALID_ISSUER;
      Exit;
    end;
    if TElASN1ConstrainedTag(InnerT).Count < 1 then
    begin
      Result := SB_PKCS7_ERROR_INVALID_ISSUER;
      Exit;
    end;
    SeqT := TElASN1ConstrainedTag(InnerT).GetField(0);
    if (not SeqT.IsConstrained) or (SeqT.TagId <> SB_ASN1_SEQUENCE) then
    begin
      Result := SB_PKCS7_ERROR_INVALID_ISSUER;
      Exit;
    end;
    if TElASN1ConstrainedTag(SeqT).Count <> 2 then
    begin
      Result := SB_PKCS7_ERROR_INVALID_ISSUER;
      Exit;
    end;
    if (TElASN1ConstrainedTag(SeqT).GetField(0).IsConstrained) or
      (TElASN1ConstrainedTag(SeqT).GetField(0).TagId <> SB_ASN1_OBJECT) then
    begin
      Result := SB_PKCS7_ERROR_INVALID_ISSUER;
      Exit;
    end;
    if TElASN1ConstrainedTag(SeqT).GetField(1).IsConstrained then
    begin
      Sz := 0;
      SetLength(Value, Sz);
      TElASN1ConstrainedTag(TElASN1ConstrainedTag(SeqT).GetField(1)).SaveToBuffer(@Value[1], Sz);
      SetLength(Value, Sz);
      TElASN1ConstrainedTag(TElASN1ConstrainedTag(SeqT).GetField(1)).SaveToBuffer(@Value[1], Sz);
      TagID := SB_ASN1_OCTETSTRING;
    end
    else
    begin
      Value := TElASN1SimpleTag(TElASN1ConstrainedTag(SeqT).GetField(1)).Content;
      TagID := TElASN1ConstrainedTag(SeqT).GetField(1).TagID;
    end;
    Issuer.Issuer.Count := Issuer.Issuer.Count + 1;
    Issuer.Issuer.OIDs[Issuer.Issuer.Count - 1] := TElASN1SimpleTag(TElASN1ConstrainedTag(SeqT).GetField(0)).Content;
    Issuer.Issuer.Values[Issuer.Issuer.Count - 1] := Value;
    Issuer.Issuer.Tags[Issuer.Issuer.Count - 1] := TagID;
  end;
  { Reading SerialNumber }
  T := TElASN1ConstrainedTag(Tag).GetField(1);
  if (T.IsConstrained) or (T.TagId <> SB_ASN1_INTEGER) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ISSUER;
    Exit;
  end;
  Issuer.SerialNumber := TElASN1SimpleTag(T).Content;
  Result := 0;
end;

function TElPKCS7Message.ProcessEncryptedContentInfo(Tag : TElASN1ConstrainedTag;
  EncryptedContent : TElPKCS7EncryptedContent) : integer;
var
  InnerTag : TElASN1CustomTag;
  I : integer;
begin
  if (not Tag.IsConstrained) or (Tag.TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA_CONTENT;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).Count < 3 then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA_CONTENT;
    Exit;
  end;
  { Reading ContentType }
  if (TElASN1ConstrainedTag(Tag).GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(0).TagId <> SB_ASN1_OBJECT) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA_CONTENT;
    Exit;
  end;
  EncryptedContent.FContentType := TElASN1SimpleTag(
    TElASN1ConstrainedTag(Tag).GetField(0)).Content;
  { Reading ContentEncryptionAlgorithm }
  Result := ProcessAlgorithmIdentifier(TElASN1ConstrainedTag(Tag).GetField(1),
    EncryptedContent.FContentEncryptionAlgorithm,
    EncryptedContent.FContentEncryptionAlgorithmParams {$ifndef HAS_DEF_PARAMS}, False{$endif});
  if Result <> 0 then
    Exit;
  { Reading EncryptedContent }
  InnerTag := TElASN1ConstrainedTag(Tag).GetField(2);
  if (not ((InnerTag.IsConstrained) and (InnerTag.TagId = SB_ASN1_A0))) and
    (not ((not InnerTag.IsConstrained) and (InnerTag.TagId = $80))) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA_CONTENT;
    Exit;
  end;
  if InnerTag.IsConstrained then
  begin
    for I := 0 to TElASN1ConstrainedTag(InnerTag).Count - 1 do
    begin
      if (TElASN1ConstrainedTag(InnerTag).GetField(I).IsConstrained) or
        (TElASN1ConstrainedTag(InnerTag).GetField(I).TagId <> SB_ASN1_OCTETSTRING) then
      begin
        Result := SB_PKCS7_ERROR_INVALID_ENVELOPED_DATA_CONTENT;
        Exit;
      end;
      EncryptedContent.FEncryptedContent := TBufferTypeConst(EncryptedContent.FEncryptedContent) +
        TBufferTypeConst(TElASN1SimpleTag(TElASN1ConstrainedTag(InnerTag).GetField(I)).Content);
    end;
  end
  else
  begin
    EncryptedContent.FEncryptedContent := CloneBuffer(TElASN1SimpleTag(InnerTag).Content);
  end;
  Result := 0;
end;

function TElPKCS7Message.ProcessSignerInfos(Tag : TElASN1CustomTag; SignerList :
  TList) : integer;
var
  I : integer;
  SignerInfo : TElPKCS7Signer;
begin
  if (not Tag.IsConstrained) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNER_INFOS;
    Exit;
  end;
  if not Assigned(SignerList) then
  begin
    Result := SB_PKCS7_ERROR_INTERNAL_ERROR;
    Exit;
  end;
  Result := 0;
  for I := 0 to TElASN1ConstrainedTag(Tag).Count - 1 do
  begin
    SignerInfo := TElPKCS7Signer.Create;
    SignerList.Add(SignerInfo);
    Result := ProcessSignerInfo(TElASN1ConstrainedTag(Tag).GetField(I),
      SignerInfo);
    if Result <> 0 then
      Break;
  end;
end;

function ProcessSignerInfo(Tag : TElASN1CustomTag; SignerInfo :
  TElPKCS7Signer) : integer;
var
  CurrentTagIndex, Sz : integer;
begin
  SignerInfo.FAuthenticatedAttributes.Count := 0;
  SignerInfo.FUnauthenticatedAttributes.Count := 0;
  if (not Tag.IsConstrained) or (Tag.TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNER_INFO;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag).Count < 5 then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNER_INFO;
    Exit;
  end;
  if (TElASN1ConstrainedTag(Tag).GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(0).TagId <> SB_ASN1_INTEGER) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNER_INFO;
    Exit;
  end;
  if not CompareContent(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(0)).Content,
    TBufferTypeConst(#1)) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNER_INFO_VERSION;
    Exit;
  end;
  SignerInfo.FVersion := 1;
  Result := ProcessIssuerAndSerialNumber(TElASN1ConstrainedTag(Tag).GetField(1),
    SignerInfo.FIssuer);
  if Result <> 0 then
    Exit;
  { Processing DigestAlgorithmIdentifier }
  Result := ProcessAlgorithmIdentifier(TElASN1ConstrainedTag(Tag).GetField(2),
    SignerInfo.FDigestAlgorithm, SignerInfo.FDigestAlgorithmParams {$ifndef HAS_DEF_PARAMS}, False{$endif});
  if Result <> 0 then
    Exit;
  CurrentTagIndex := 3;
  { Checking authenticated attributes }
  if (TElASN1ConstrainedTag(Tag).GetField(3).IsConstrained) and
    (TElASN1ConstrainedTag(Tag).GetField(3).TagId = SB_ASN1_A0) then
  begin
    Result := ProcessAttributes(TElASN1ConstrainedTag(Tag).GetField(3),
      SignerInfo.FAuthenticatedAttributes);
    if Result <> 0 then
      Exit;
    Sz := 0;

    TElASN1ConstrainedTag(Tag).GetField(3).SaveToBuffer(nil, Sz);
    SetLength(SignerInfo.FAuthenticatedAttributesPlain, Sz);
    TElASN1ConstrainedTag(Tag).GetField(3).SaveToBuffer(
      @SignerInfo.FAuthenticatedAttributesPlain[1], Sz);
    SignerInfo.FAuthenticatedAttributesPlain[1] := Chr(SB_ASN1_SET);
    CurrentTagIndex := 4;
  end;
  { Processing DigestEncryptionAlgorithmIdentifier }
  Result := ProcessAlgorithmIdentifier(TElASN1ConstrainedTag(Tag).GetField(
    CurrentTagIndex), SignerInfo.FDigestEncryptionAlgorithm,
    SignerInfo.FDigestEncryptionAlgorithmParams {$ifndef HAS_DEF_PARAMS}, False{$endif});
  if Result <> 0 then
    Exit;
  Inc(CurrentTagIndex);
  if CurrentTagIndex >= TElASN1ConstrainedTag(Tag).Count then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNER_INFO;
    Exit;
  end;
  { Processing EncryptedDigest }
  if (TElASN1ConstrainedTag(Tag).GetField(CurrentTagIndex).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(CurrentTagIndex).TagId <> SB_ASN1_OCTETSTRING) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_SIGNER_INFO;
    Exit;
  end;
  SignerInfo.FEncryptedDigest := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(CurrentTagIndex)).Content;
  Inc(CurrentTagIndex);
  if CurrentTagIndex >= TElASN1ConstrainedTag(Tag).Count then
  begin
    Result := 0;
    Exit;
  end;
  { Processing unauthenticated attributes }
  if (TElASN1ConstrainedTag(Tag).GetField(CurrentTagIndex).IsConstrained) and
    (TElASN1ConstrainedTag(Tag).GetField(CurrentTagIndex).TagId = SB_ASN1_A1) then
  begin
    Result := ProcessAttributes(TElASN1ConstrainedTag(Tag).GetField(CurrentTagIndex),
      SignerInfo.FUnauthenticatedAttributes);
    if Result <> 0 then
      Exit;
  end;
  Result := 0;
end;

procedure TElPKCS7Message.ProcessCertificates(Tag : TElASN1ConstrainedTag; Storage :
  TElCustomCertStorage);
var
  I, Size : integer;
  CertBuf : array of byte;
  Cert : TElX509Certificate;
begin
  while Storage.Count > 0 do
    Storage.Remove(0);
  Cert := TElX509Certificate.Create(nil);
  for I := 0 to Tag.Count - 1 do
  begin
    Size := 0;
    Tag.GetField(I).SaveToBuffer(nil, Size);
    SetLength(CertBuf, Size);
    Tag.GetField(I).SaveToBuffer(@CertBuf[0], Size);
    Cert.LoadFromBuffer(@CertBuf[0], Word(Size));
    Storage.Add(Cert{$ifndef HAS_DEF_PARAMS}, true{$endif});
  end;

  FreeAndNil(Cert);
end;

function ProcessAttributes(Tag : TElASN1CustomTag; Attributes :
  TElPKCS7Attributes) : integer;
var
  I, J, Sz : integer;
  Buf : string;
  CTag : TElASN1ConstrainedTag;
begin
  Attributes.Count := 0;
  if not Tag.IsConstrained then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ATTRIBUTES;
    Exit;
  end;
  for I := 0 to TElASN1ConstrainedTag(Tag).Count - 1 do
  begin
    if (not TElASN1ConstrainedTag(Tag).GetField(I).IsConstrained) or
      (TElASN1ConstrainedTag(Tag).GetField(I).TagId <> SB_ASN1_SEQUENCE) then
    begin
      Result := SB_PKCS7_ERROR_INVALID_ATTRIBUTES;
      Exit;
    end;
    CTag := TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(I));
    if (CTag.GetField(0).IsConstrained) or (CTag.GetField(0).TagId <>
      SB_ASN1_OBJECT) then
    begin
      Result := SB_PKCS7_ERROR_INVALID_ATTRIBUTES;
      Exit;
    end;
    if (not CTag.GetField(1).IsConstrained) or (CTag.GetField(1).TagId <>
      SB_ASN1_SET) then
    begin
      Result := SB_PKCS7_ERROR_INVALID_ATTRIBUTES;
      Exit;
    end;
    Attributes.Count := I + 1;
    Attributes.Attributes[I] := CloneBuffer(TElASN1SimpleTag(CTag.GetField(0)).Content);
    CTag := TElASN1ConstrainedTag(CTag.GetField(1));
    for J := 0 to CTag.Count - 1 do
    begin
      Sz := 0;
      CTag.GetField(J).SaveToBuffer(nil, Sz);
      SetLength(Buf, Sz);
      CTag.GetField(J).SaveToBuffer(@Buf[1], Sz);
      Attributes.Values[I].Add(Buf);
    end;
  end;
  Result := 0;
end;

procedure TElPKCS7Message.SaveEnvelopedData(Tag : TElASN1ConstrainedTag);
var
  STag : TElASN1SimpleTag;
  CTag : TElASN1ConstrainedTag;
begin
  { Adding EnvelopedData sequence }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTag.TagId := SB_ASN1_SEQUENCE;
  CTag.UndefSize := FUseUndefSize;
  { Writing version }
  STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := #0;
  { Writing RecipientInfos }
  SaveRecipientInfos(TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true))),
    FEnvelopedData.FRecipientList);
  { Writing EncryptedContentInfo }
  SaveEncryptedContentInfo(TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true))),
    FEnvelopedData.FEncryptedContent);
end;

procedure TElPKCS7Message.SaveRecipientInfos(Tag : TElASN1ConstrainedTag;
  RecipientList : TList);
var
  I : integer;
  CTag : TElASN1ConstrainedTag;
begin
  Tag.TagId := SB_ASN1_SET;
  for I := 0 to RecipientList.Count - 1 do
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    SaveRecipientInfo(CTag, TElPKCS7Recipient(RecipientList[I]));
  end;
end;

procedure TElPKCS7Message.SaveRecipientInfo(Tag : TElASN1ConstrainedTag; Recipient :
  TElPKCS7Recipient);
var
  CTag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
begin
  Tag.TagId := SB_ASN1_SEQUENCE;
  { writing version }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := #0;
  { writing IssuerAndSerialNumber }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveIssuerAndSerialNumber(CTag, Recipient.FIssuer);
  { writing KeyEncryptionAlgorithm }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveAlgorithmIdentifier(CTag, Recipient.FKeyEncryptionAlgorithm,
    Recipient.FKeyEncryptionAlgorithmParams {$ifndef HAS_DEF_PARAMS}, 0{$endif});
  { writing EncryptedKey }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;
  STag.Content := CloneBuffer(Recipient.FEncryptedKey);
end;

procedure SaveIssuerAndSerialNumber(Tag : TElASN1ConstrainedTag;
  Issuer : TElPKCS7Issuer);
var
  CTag, CInnerTag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  I : integer;
begin
  Tag.TagId := SB_ASN1_SEQUENCE;
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTag.TagId := SB_ASN1_SEQUENCE;
  for I := 0 to Issuer.Issuer.Count - 1 do
  begin
    CInnerTag := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
    CInnerTag.TagId := SB_ASN1_SET;
    CInnerTag := TElASN1ConstrainedTag(CInnerTag.GetField(CInnerTag.AddField(true)));
    CInnerTag.TagId := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(CInnerTag.GetField(CInnerTag.AddField(false)));
    STag.TagId := SB_ASN1_OBJECT;
    STag.Content := CloneBuffer(Issuer.Issuer.OIDs[I]);
    STag := TElASN1SimpleTag(CInnerTag.GetField(CInnerTag.AddField(false)));
    STag.TagId := Issuer.Issuer.Tags[I];//SB_ASN1_PRINTABLESTRING;
    STag.Content := CloneBuffer(Issuer.Issuer.Values[I]);
  end;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := CloneBuffer(Issuer.SerialNumber);
end;

procedure TElPKCS7Message.SaveEncryptedContentInfo(Tag : TElASN1ConstrainedTag;
  EncryptedContent : TElPKCS7EncryptedContent);
var
  CTag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
begin
  Tag.TagId := SB_ASN1_SEQUENCE;
  Tag.UndefSize := FUseUndefSize;
  { Writing ContentType }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_OBJECT;
  STag.Content := CloneBuffer(EncryptedContent.FContentType);
  { Writing contentEncryptionAlgorithm }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTag.TagId := SB_ASN1_SEQUENCE;
  SaveAlgorithmIdentifier(CTag, EncryptedContent.FContentEncryptionAlgorithm,
    EncryptedContent.FContentEncryptionAlgorithmParams {$ifndef HAS_DEF_PARAMS}, 0{$endif});
  { Writing EncryptedContent }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTag.TagId := SB_ASN1_A0;
  CTag.UndefSize := FUseUndefSize;
  STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;
  STag.Content := CloneBuffer(EncryptedContent.FEncryptedContent);
end;

procedure TElPKCS7Message.SaveSignedData(Tag : TElASN1ConstrainedTag);
var
  STag : TElASN1SimpleTag;
  CTag, CTagSeq : TElASN1ConstrainedTag;
  Lst : TStringList;
  I : integer;
begin
  Tag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  Tag.TagId := SB_ASN1_SEQUENCE;
  Tag.UndefSize := FUseUndefSize;
  { Writing version }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := Char(FSignedData.Version);
  { Writing digestAlgorithms }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTag.TagId := SB_ASN1_SET;

  Lst := TStringList.Create;
  for I := 0 to FSignedData.SignerCount - 1 do
  begin
    if Lst.IndexOf(FSignedData.Signers[I].FDigestAlgorithm) = -1 then
      Lst.Add(FSignedData.Signers[I].FDigestAlgorithm);
  end;
  for I := 0 to Lst.Count - 1 do
  begin
    CTagSeq := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
    SaveAlgorithmIdentifier(CTagSeq, Lst[I], '');
  end;
  Lst.Free;
  { Writing contentInfo }
  CTagSeq := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTagSeq.TagId := SB_ASN1_SEQUENCE;

  STag := TElASN1SimpleTag(CTagSeq.GetField(CTagSeq.AddField(false)));
  STag.TagId := SB_ASN1_OBJECT;
  if Length(FSignedData.FContentType) > 0  then
    STag.Content := CloneBuffer(FSignedData.FContentType)
  else
    STag.Content := SB_OID_PKCS7_DATA;
  if Length(FSignedData.FContent) > 0 then
  begin
    if FUseImplicitContent then
    begin
      STag := TElASN1SimpleTag(CTagSeq.GetField(CTagSeq.AddField(False)));
      STag.TagID := SB_ASN1_A0;
      STag.Content := CloneBuffer(FSignedData.FContent);
    end
    else
    begin
      CTag := TElASN1ConstrainedTag(CTagSeq.GetField(CTagSeq.AddField(true)));
      CTag.TagId := SB_ASN1_A0;

      STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
      STag.TagId := SB_ASN1_OCTETSTRING;
      STag.Content := CloneBuffer(FSignedData.Content);
    end;
  end
  else
  begin
    CTagSeq.UndefSize := FUseUndefSize;
  end;
  { Writing certificates }
  if FSignedData.FCertStorage.Count > 0 then
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    SaveCertificates(FSignedData.FCertStorage, CTag);
    CTag.TagId := SB_ASN1_A0;
  end;
  { Writing signerInfos }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveSignerInfos(CTag, FSignedData.FSignerList);
end;

procedure TElPKCS7Message.SaveCertificates(Storage : TElCustomCertStorage; Tag :
  TElASN1ConstrainedTag);
var
  I : integer;
  STag : TElASN1SimpleTag;
  TmpS : string;
begin
  for I := 0 to Storage.Count - 1 do
  begin
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.WriteHeader := false;
    SetLength(TmpS, Storage.Certificates[I].CertificateSize);
    Move(Storage.Certificates[I].CertificateBinary^, TmpS[1], Length(TmpS));
    STag.Content := TmpS;
  end;
end;

procedure TElPKCS7Message.SaveSignerInfos(Tag : TElASN1ConstrainedTag; SignerList :
  TList);
var
  I : integer;
begin
  Tag.TagId := SB_ASN1_SET;
  for I := 0 to SignerList.Count - 1 do
  begin
    SaveSignerInfo(TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true))),
      TElPKCS7Signer(SignerList[I]));
  end;
end;

procedure SaveSignerInfo(Tag : TElASN1ConstrainedTag; Signer :
  TElPKCS7Signer);
var
  STag : TElASN1SimpleTag;
  CTag : TElASN1ConstrainedTag;
begin
  Tag.TagId := SB_ASN1_SEQUENCE;
  { Writing version }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := Chr(Signer.FVersion);
  { Writing IssuerAndSerialNumber }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveIssuerAndSerialNumber(CTag, Signer.FIssuer);
  { Writing digestAlgorithm }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveAlgorithmIdentifier(CTag, Signer.FDigestAlgorithm, '');
  { Writing authenticatedAttributes }
  if Signer.FAuthenticatedAttributes.Count > 0 then
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    SaveAttributes(CTag, Signer.FAuthenticatedAttributes);
    CTag.TagId := SB_ASN1_A0;
  end;
  { Writing digestEncryptionAlgorithm }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveAlgorithmIdentifier(CTag, Signer.FDigestEncryptionAlgorithm,
    Signer.FDigestEncryptionAlgorithmParams {$ifndef HAS_DEF_PARAMS}, 0{$endif});

  { Writing encryptedDigest }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;
  STag.Content := CloneBuffer(Signer.FEncryptedDigest);
  { Writing unauthenticatedAttributes }
  if Signer.FUnauthenticatedAttributes.Count > 0 then
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    SaveAttributes(CTag, Signer.FUnauthenticatedAttributes);
    CTag.TagId := SB_ASN1_A1;
  end;
end;

procedure TElPKCS7Message.SaveDigestedData(Tag : TElASN1ConstrainedTag);
var
  STag : TElASN1SimpleTag;
  CTag : TElASN1ConstrainedTag;
begin
  Tag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  Tag.TagId := SB_ASN1_SEQUENCE;
  Tag.UndefSize := FUseUndefSize;
  { Writing version }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := Chr(FDigestedData.FVersion);
  { Writing DigestAlgorithm }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveAlgorithmIdentifier(CTag, FDigestedData.FDigestAlgorithm,
    FDigestedData.FDigestAlgorithmParams {$ifndef HAS_DEF_PARAMS}, 0{$endif});
  { Writing ContentInfo }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTag.TagId := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
  STag.TagId := SB_ASN1_OBJECT;
  STag.Content := SB_OID_PKCS7_DATA;
  CTag := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
  CTag.TagId := SB_ASN1_A0;
  STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;
  STag.Content := CloneBuffer(FDigestedData.FContent);
  { Writing digest }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;
  STag.Content := CloneBuffer(FDigestedData.FDigest);
end;

procedure TElPKCS7Message.SaveEncryptedData(Tag : TElASN1ConstrainedTag);
var
  STag : TElASN1SimpleTag;
begin
  Tag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  Tag.TagId := SB_ASN1_SEQUENCE;
  Tag.UndefSize := FUseUndefSize;
  { Writing version }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := Chr(FEncryptedData.FVersion);
  { Writing encryptedContentInfo }
  SaveEncryptedContentInfo(TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true))),
    FEncryptedData.FEncryptedContent);
end;

procedure TElPKCS7Message.SaveSignedAndEnvelopedData(Tag : TElASN1ConstrainedTag);
var
  STag : TElASN1SimpleTag;
  CTag, CTagSeq : TElASN1ConstrainedTag;
  Lst : TStringList;
  I : integer;
begin
  Tag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  Tag.TagId := SB_ASN1_SEQUENCE;
  Tag.UndefSize := FUseUndefSize;
  { Writing version }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;

  STag.Content := Chr(FSignedAndEnvelopedData.FVersion);

  { Writing recipientInfos }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveRecipientInfos(CTag, FSignedAndEnvelopedData.FRecipientList);
  { Writing digestAlgorithms }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTag.TagId := SB_ASN1_SET;

  Lst := TStringList.Create;
  for I := 0 to FSignedAndEnvelopedData.SignerCount - 1 do
  begin
    if Lst.IndexOf(FSignedAndEnvelopedData.Signers[I].FDigestAlgorithm) = -1 then
      Lst.Add(FSignedAndEnvelopedData.Signers[I].FDigestAlgorithm);
  end;
  for I := 0 to Lst.Count - 1 do
  begin
    CTagSeq := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
    SaveAlgorithmIdentifier(CTagSeq, Lst[I], '');
  end;
  Lst.Free;

  { Writing encryptedContentInfo }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveEncryptedContentInfo(CTag, FSignedAndEnvelopedData.FEncryptedContent);
  { Writing certificates }
  if FSignedAndEnvelopedData.FCertStorage.Count > 0 then
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    CTag.TagId := SB_ASN1_A0;
    SaveCertificates(FSignedAndEnvelopedData.FCertStorage, CTag);
  end;
  { Writing signerInfos }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveSignerInfos(CTag, FSignedAndEnvelopedData.FSignerList);
end;

procedure TElPKCS7Message.SaveAuthenticatedData(Tag : TElASN1ConstrainedTag);
var
  STag : TElASN1SimpleTag;
  CTag : TElASN1ConstrainedTag;
begin
  Tag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  Tag.TagId := SB_ASN1_SEQUENCE;
  Tag.UndefSize := FUseUndefSize;
  { Writing version }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := Chr(FSignedAndEnvelopedData.FVersion);

  { OriginatorInfo is not supported at the moment. }

  { Writing recipientInfos }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveRecipientInfos(CTag, FAuthenticatedData.FRecipientList);

  { Writing macAlgorithm }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveAlgorithmIdentifier(CTag, FAuthenticatedData.FMacAlgorithm, '' {$ifndef HAS_DEF_PARAMS}, 0{$endif});

  { Writing digestAlgorithm }
  if FAuthenticatedData.FAuthenticatedAttributes.Count > 0 then
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    SaveAlgorithmIdentifier(CTag, FAuthenticatedData.FDigestAlgorithm, '',
      SB_ASN1_A1);
  end;

  { Writing encapsulatedContentInfo }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTag.TagID := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
  STag.TagId := SB_ASN1_OBJECT;
  STag.Content := FAuthenticatedData.FContentType;
  CTag := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
  CTag.TagID := SB_ASN1_A0;
  STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;
  STag.Content := FAuthenticatedData.FContent;

  { Writing authenticated attributes }
  if FAuthenticatedData.FAuthenticatedAttributes.Count > 0 then
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    SaveAttributes(CTag, FAuthenticatedData.FAuthenticatedAttributes);
    CTag.TagId := SB_ASN1_A2;
  end;

  { Writing message authentication code }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagID := SB_ASN1_OCTETSTRING;
  STag.Content := FAuthenticatedData.FMac;

  { Writing unauthenticated attributes }
  if (FAuthenticatedData.FUnauthenticatedAttributes.Count > 0) then
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    SaveAttributes(CTag, FAuthenticatedData.FUnauthenticatedAttributes);
    CTag.TagId := SB_ASN1_A3;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TElPKCS7Issuer

constructor TElPKCS7Issuer.Create;
begin
  inherited;
  FIssuer := TElRelativeDistinguishedName.Create;
end;

destructor TElPKCS7Issuer.Destroy;
begin
  FIssuer.Free;
  inherited;
end;

procedure TElPKCS7Issuer.Assign(Source: TElPKCS7Issuer);
begin
  FSerialNumber := Source.FSerialNumber;
  FIssuer.Assign(TElRelativeDistinguishedName(Source.FIssuer));
end;

////////////////////////////////////////////////////////////////////////////////
// TElPKCS7Recipient

constructor TElPKCS7Recipient.Create;
begin
  inherited;
  FIssuer := TElPKCS7Issuer.Create;
end;

destructor TElPKCS7Recipient.Destroy;
begin
  FIssuer.Free;
  inherited;
end;

////////////////////////////////////////////////////////////////////////////////
// TElPKCS7EnvelopedData

constructor TElPKCS7EnvelopedData.Create;
begin
  inherited;
  FEncryptedContent := TElPKCS7EncryptedContent.Create;
  FRecipientList := TList.Create;
end;

destructor TElPKCS7EnvelopedData.Destroy;
begin
  Clear;
  FreeAndNil(FRecipientList);
  FreeAndNil(FEncryptedContent);
  inherited;
end;

function TElPKCS7EnvelopedData.GetRecipient(Index : integer) : TElPKCS7Recipient;
begin
  if Index < FRecipientList.Count then
    Result := TElPKCS7Recipient(FRecipientList[Index])
  else
    Result := nil;
end;

function TElPKCS7EnvelopedData.GetRecipientCount : integer;
begin
  Result := FRecipientList.Count;
end;

procedure TElPKCS7EnvelopedData.Clear;
var
  Rec : TElPKCS7Recipient;
begin
  while FRecipientList.Count > 0 do
  begin
    Rec := TElPKCS7Recipient(FRecipientList[0]);
    FRecipientList.Delete(0);
    Rec.Free;
  end;
end;

function TElPKCS7EnvelopedData.AddRecipient : integer;
begin
  Result := FRecipientList.Add(TElPKCS7Recipient.Create);
end;

function TElPKCS7EnvelopedData.RemoveRecipient(Index : integer) : boolean;
var
  Rec : TElPKCS7Recipient;
begin
  if Index < FRecipientList.Count then
  begin
    Rec := TElPKCS7Recipient(FRecipientList[Index]);
    FRecipientList.Delete(Index);
    Rec.Free;
    Result := true;
  end
  else
    Result := false;
end;

////////////////////////////////////////////////////////////////////////////////
// TElPKCS7SignedData

constructor TElPKCS7SignedData.Create;
begin
  inherited;
  FCertStorage := TElMemoryCertStorage.Create(nil);
  FSignerList := TList.Create;
end;

destructor TElPKCS7SignedData.Destroy;
begin
  FreeAndNil(FCertStorage);

  while FSignerList.Count > 0 do
  begin
    TList(FSignerList.Items[0]).Free;
    FSignerList.Delete(0);
  end;
  FreeAndNil(FSignerList);
  inherited;
end;

function TElPKCS7SignedData.GetSigner(Index : integer) : TElPKCS7Signer;
begin
  Result := TElPKCS7Signer(FSignerList[Index]);
end;

function TElPKCS7SignedData.GetSignerCount : integer;
begin
  Result := FSignerList.Count;
end;

function TElPKCS7SignedData.AddSigner : integer;
begin
  Result := FSignerList.Add(TElPKCS7Signer.Create);
end;

function TElPKCS7SignedData.RemoveSigner(Index : integer) : boolean;
begin
  if Index < FSignerList.Count then
  begin
    TElPKCS7Signer(FSignerList[Index]).Free;
    FSignerList.Delete(Index);
    Result := true;
  end
  else
    Result := false;
end;

////////////////////////////////////////////////////////////////////////////////
// TElPKCS7Signer

constructor TElPKCS7Signer.Create;
begin
  inherited;
  FAuthenticatedAttributes := TElPKCS7Attributes.Create;
  FUnauthenticatedAttributes := TElPKCS7Attributes.Create;
  FIssuer := TElPKCS7Issuer.Create;
end;

destructor TElPKCS7Signer.Destroy;
begin
  FreeAndNil(FAuthenticatedAttributes);
  FreeAndNil(FUnauthenticatedAttributes);
  FreeAndNil(FIssuer);
  inherited;
end;

function TElPKCS7Signer.GetAuthenticatedAttributesPlain : string;
var
  Sz : integer;
  Tag : TElASN1ConstrainedTag;
begin
  if Length(FAuthenticatedAttributesPlain) > 0 then
    Result := FAuthenticatedAttributesPlain
  else
  begin
    Tag := TElASN1ConstrainedTag.Create;
    SaveAttributes(Tag, FAuthenticatedAttributes);

    Sz := 0;

    SetLength(FAuthenticatedAttributesPlain, Sz);
    Tag.SaveToBuffer(nil, Sz);

    SetLength(FAuthenticatedAttributesPlain, Sz);
    Tag.SaveToBuffer(@FAuthenticatedAttributesPlain[1], Sz);
    Tag.Free;

    Result := FAuthenticatedAttributesPlain;
  end;
end;

procedure TElPKCS7Signer.RecalculateAuthenticatedAttributes;
begin
  SetLength(FAuthenticatedAttributesPlain, 0);
  GetAuthenticatedAttributesPlain;
end;

procedure TElPKCS7Signer.Assign(Source : TElPKCS7Signer);
begin
  FVersion := Source.FVersion;
  FIssuer.Assign(Source.FIssuer);
  FDigestAlgorithm := Source.FDigestAlgorithm;
  FDigestAlgorithmParams := Source.FDigestAlgorithmParams;
  Source.FAuthenticatedAttributes.Copy(FAuthenticatedAttributes);
  Source.FUnauthenticatedAttributes.Copy(FUnauthenticatedAttributes);
  FDigestEncryptionAlgorithm := Source.FDigestEncryptionAlgorithm;
  FDigestEncryptionAlgorithmParams := Source.FDigestEncryptionAlgorithmParams;
  FEncryptedDigest := Source.FEncryptedDigest;
  FAuthenticatedAttributesPlain := Source.FAuthenticatedAttributesPlain;
end;

////////////////////////////////////////////////////////////////////////////////
// TElPKCS7EncryptedContent

constructor TElPKCS7EncryptedContent.Create;
begin
  inherited;
end;

destructor TElPKCS7EncryptedContent.Destroy;
begin
  inherited;
end;

////////////////////////////////////////////////////////////////////////////////
// TElPKCS7SignedAndEnvelopedData

constructor TElPKCS7SignedAndEnvelopedData.Create;
begin
  inherited;
  FEncryptedContent := TElPKCS7EncryptedContent.Create;
  FCertStorage := TElMemoryCertStorage.Create(nil);
  FRecipientList := TList.Create;
  FSignerList := TList.Create;
end;

destructor TElPKCS7SignedAndEnvelopedData.Destroy;
begin
  FreeAndNil(FEncryptedContent);
  while FRecipientList.Count > 0 do
  begin
    TElPKCS7Recipient(FRecipientList.Items[0]).Free;
    FRecipientList.Delete(0);
  end;
  while FSignerList.Count > 0 do
  begin
    TElPKCS7Signer(FSignerList.Items[0]).Free;
    FSignerList.Delete(0);
  end;
  FreeAndNil(FCertStorage);
  FreeAndNil(FRecipientList);
  FreeAndNil(FSignerList);
  inherited;
end;

function TElPKCS7SignedAndEnvelopedData.GetRecipient(Index : integer) :
  TElPKCS7Recipient;
begin
  if Index < FRecipientList.Count then
    Result := TElPKCS7Recipient(FRecipientList[Index])
  else
    Result := nil;
end;

function TElPKCS7SignedAndEnvelopedData.GetRecipientCount : integer;
begin
  Result := FRecipientList.Count;
end;

function TElPKCS7SignedAndEnvelopedData.GetSigner(Index : integer) : TElPKCS7Signer;
begin
  if Index < FSignerList.Count then
    Result := TElPKCS7Signer(FSignerList[Index])
  else
    Result := nil;
end;

function TElPKCS7SignedAndEnvelopedData.GetSignerCount : integer;
begin
  Result := FSignerList.Count;
end;

function TElPKCS7SignedAndEnvelopedData.AddRecipient : integer;
begin
  Result := FRecipientList.Add(TElPKCS7Recipient.Create);
end;

function TElPKCS7SignedAndEnvelopedData.AddSigner : integer;
begin
  Result := FSignerList.Add(TElPKCS7Signer.Create);
end;

function TElPKCS7SignedAndEnvelopedData.RemoveRecipient(Index : integer) : boolean;
begin
  if Index < FRecipientList.Count then
  begin
    TElPKCS7Recipient(FRecipientList.Items[Index]).Free;
    FRecipientList.Delete(Index);
    Result := true;
  end
  else
    Result := false;
end;

function TElPKCS7SignedAndEnvelopedData.RemoveSigner(Index : integer) : boolean;
begin
  if Index < FSignerList.Count then
  begin
    TElPKCS7Signer(FSignerList.Items[Index]).Free;
    FSignerList.Delete(Index);
    Result := true;
  end
  else
    Result := false;
end;

////////////////////////////////////////////////////////////////////////////////
// TElPKCS7EncryptedData

constructor TElPKCS7EncryptedData.Create;
begin
  inherited;
  FEncryptedContent := TElPKCS7EncryptedContent.Create;
end;

destructor TElPKCS7EncryptedData.Destroy;
begin
  FEncryptedContent.Free;
  inherited;
end;

////////////////////////////////////////////////////////////////////////////////
// TElPKCS7AuthenticatedData

constructor TElPKCS7AuthenticatedData.Create;
begin
  inherited;
  FRecipientList := TList.Create;
  FAuthenticatedAttributes := TElPKCS7Attributes.Create;
  FUnauthenticatedAttributes := TElPKCS7Attributes.Create;
  FOriginatorCerts := TElMemoryCertStorage.Create(nil);
  FVersion := 0;
end;

destructor TElPKCS7AuthenticatedData.Destroy;
begin
  while RecipientCount > 0 do
    RemoveRecipient(0);
  FreeAndNil(FRecipientList);
  FreeAndNil(FAuthenticatedAttributes);
  FreeAndNil(FUnauthenticatedAttributes);
  FreeAndNil(FOriginatorCerts);
  inherited;
end;

function TElPKCS7AuthenticatedData.GetRecipient(Index: integer) : TElPKCS7Recipient;
begin
  if (Index >= 0) and (Index < FRecipientList.Count) then
    Result := TElPKCS7Recipient(FRecipientList[Index])
  else
    Result := nil;
end;

function TElPKCS7AuthenticatedData.GetRecipientCount: integer;
begin
  Result := FRecipientList.Count;
end;

function TElPKCS7AuthenticatedData.GetAuthenticatedAttributesPlain: BufferType;
var
  Sz : integer;
  Tag : TElASN1ConstrainedTag;
begin
  if Length(FAuthenticatedAttributesPlain) > 0 then
    Result := FAuthenticatedAttributesPlain
  else
  begin
    Tag := TElASN1ConstrainedTag.Create;
    SaveAttributes(Tag, FAuthenticatedAttributes);

    Sz := 0;

    SetLength(FAuthenticatedAttributesPlain, Sz);
    Tag.SaveToBuffer(nil, Sz);

    SetLength(FAuthenticatedAttributesPlain, Sz);
    Tag.SaveToBuffer(@FAuthenticatedAttributesPlain[1], Sz);
    Tag.Free;

    Result := FAuthenticatedAttributesPlain;
  end;
end;

procedure TElPKCS7AuthenticatedData.RecalculateAuthenticatedAttributes;
begin
  SetLength(FAuthenticatedAttributesPlain, 0);
  GetAuthenticatedAttributesPlain;
end;

function TElPKCS7AuthenticatedData.AddRecipient : integer;
begin
  Result := FRecipientList.Add(TElPKCS7Recipient.Create);
end;

function TElPKCS7AuthenticatedData.RemoveRecipient(Index : integer) : boolean;
begin
  if (Index >= 0) and (Index < FRecipientList.Count) then
  begin
    TElPKCS7Recipient(FRecipientList[Index]).Free;
    FRecipientList.Delete(Index);
    Result := true;
  end
  else
    Result := false;
end;

////////////////////////////////////////////////////////////////////////////////
// Auxiliary routines

function ProcessAlgorithmIdentifier(Tag : TElASN1CustomTag; var Algorithm :
  string; var Params : string; ImplicitTagging : boolean = false) : integer;
var
  Sz : integer;
begin
  if (not Tag.IsConstrained) or
    ((not ImplicitTagging) and (Tag.TagId <> SB_ASN1_SEQUENCE)) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ALGORITHM;
    Exit;
  end;
  if (TElASN1ConstrainedTag(Tag).Count > 2) or (TElASN1ConstrainedTag(Tag).Count < 1) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ALGORITHM;
    Exit;
  end;
  if (TElASN1ConstrainedTag(Tag).GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(Tag).GetField(0).TagId <> SB_ASN1_OBJECT) then
  begin
    Result := SB_PKCS7_ERROR_INVALID_ALGORITHM;
    Exit;
  end;
  Algorithm := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(0)).Content;
  if TElASN1ConstrainedTag(Tag).Count = 2 then
  begin
    if TElASN1ConstrainedTag(Tag).GetField(1).IsConstrained then
    begin
      Sz := 0;
      SetLength(Params, Sz);
      TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(1)).SaveToBuffer(nil, Sz);
      SetLength(Params, Sz);
      TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(1)).SaveToBuffer(
        @Params[1], Sz);
    end
    else
    begin
      Sz := 0;
      SetLength(Params, Sz);
      TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(1)).SaveToBuffer(nil, Sz);
      SetLength(Params, Sz);
      TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(1)).SaveToBuffer(@Params[1], Sz);
    end;
  end
  else
    Params := '';
  Result := 0;
end;

function ProcessContentInfo(Tag : TElASN1ConstrainedTag; Buffer :
  pointer; var Size : integer; var ContentType : string) : boolean;
begin
  Result := false;
  if Tag.TagId <> SB_ASN1_SEQUENCE then
    Exit;
  if (Tag.Count < 1) or (Tag.Count > 2) then
    Exit;
  if (Tag.GetField(0).IsConstrained) or (Tag.GetField(0).TagId <> SB_ASN1_OBJECT) then
    Exit;
  ContentType := TElASN1SimpleTag(Tag.GetField(0)).Content;
  if Tag.Count = 2 then
  begin
    if (not Tag.GetField(1).IsConstrained) or (Tag.GetField(1).TagId <> SB_ASN1_A0) then
      Exit;
    if (TElASN1ConstrainedTag(Tag.GetField(1)).Count < 1) then
      Exit;
    if (not TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0).IsConstrained) then
    begin
      if Size < Length(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0)).Content) then
      begin
        Size := Length(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0)).Content);
        Result := false;
      end
      else
      begin
        Size := Length(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0)).Content);
        Move(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0)).Content[1],
          Buffer^, Size);
        Result := true;
      end;
    end
    else
    begin
      if TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0).TagNum = SB_ASN1_OCTETSTRING then
        Result := TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0)).SaveContentToBuffer(Buffer, Size) 
      else
        Result := TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0)).SaveToBuffer(Buffer, Size);
    end;
  end
  else
  begin
    Size := 0;
    Result := true;
  end;
end;

procedure SaveAlgorithmIdentifier(Tag : TElASN1ConstrainedTag; const Algorithm :
  string; const Params : string; ImplicitTag: byte = 0);
var
  STag : TElASN1SimpleTag;
begin
  if ImplicitTag = 0 then
    Tag.TagId := SB_ASN1_SEQUENCE
  else
    Tag.TagId := ImplicitTag;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_OBJECT;
  STag.Content := CloneBuffer(Algorithm);
  if CompareContent(Algorithm, SB_CERT_OID_DSA) then
    Exit;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));

  STag.Content := CloneBuffer(Params);
  if Params = '' then
    STag.TagId := SB_ASN1_NULL
  else
    STag.WriteHeader := false;
end;

end.
