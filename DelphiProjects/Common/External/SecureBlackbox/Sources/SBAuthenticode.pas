(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$i SecBbox.inc}

(*

Change history (not complete)

02 March 2005

Fixed ElAuthenticodeVerifier.RemoveAuthenticode() method

*)

unit SBAuthenticode;

interface

uses
  SysUtils,
  Classes,
  {$ifndef CLX_USED}
  Windows,
  {$else}
  Libc,
  {$endif}
  SBMD,
  SBSHA,
  SBPKCS7,
  SBPKCS7Utils,
  SBUtils,
  SBCustomCertStorage,
  SBX509;

const
  SB_AUTHENTICODE_ERROR_BASE                  = $2400;
  SB_AUTHENTICODE_ERROR_SUCCESS               = 0;
  SB_AUTHENTICODE_ERROR_UNKNOWN               = SB_AUTHENTICODE_ERROR_BASE + 1;
  SB_AUTHENTICODE_ERROR_CANNOT_OPEN_FILE      = SB_AUTHENTICODE_ERROR_BASE + 2;
  SB_AUTHENTICODE_ERROR_FILE_TOO_SMALL        = SB_AUTHENTICODE_ERROR_BASE + 3;
  SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE      = SB_AUTHENTICODE_ERROR_BASE + 4;
  SB_AUTHENTICODE_ERROR_CANNOT_WRITE_FILE     = SB_AUTHENTICODE_ERROR_BASE + 5;
  SB_AUTHENTICODE_ERROR_INVALID_EXECUTABLE    = SB_AUTHENTICODE_ERROR_BASE + 6;
  SB_AUTHENTICODE_ERROR_INVALID_PE_FORMAT     = SB_AUTHENTICODE_ERROR_BASE + 7;
  SB_AUTHENTICODE_ERROR_FILE_NOT_SIGNED       = SB_AUTHENTICODE_ERROR_BASE + 8;
  SB_AUTHENTICODE_ERROR_SIGNATURE_CORRUPTED   = SB_AUTHENTICODE_ERROR_BASE + 9;
  SB_AUTHENTICODE_ERROR_INVALID_ALGORITHM     = SB_AUTHENTICODE_ERROR_BASE + 10;
  SB_AUTHENTICODE_ERROR_INVALID_DIGEST_SIZE   = SB_AUTHENTICODE_ERROR_BASE + 11;
  SB_AUTHENTICODE_ERROR_INVALID_INDEX         = SB_AUTHENTICODE_ERROR_BASE + 12;
  SB_AUTHENTICODE_ERROR_NO_SIGNER_CERTIFICATE = SB_AUTHENTICODE_ERROR_BASE + 13;
  SB_AUTHENTICODE_ERROR_INVALID_SIGNATURE     = SB_AUTHENTICODE_ERROR_BASE + 14;
  SB_AUTHENTICODE_ERROR_INVALID_CERTIFICATE   = SB_AUTHENTICODE_ERROR_BASE + 15;
  SB_AUTHENTICODE_ERROR_FILE_ALREADY_SIGNED   = SB_AUTHENTICODE_ERROR_BASE + 16;
  SB_AUTHENTICODE_ERROR_INVALID_ARGUMENT      = SB_AUTHENTICODE_ERROR_BASE + 17;
  SB_AUTHENTICODE_ERROR_INVALID_DATA          = SB_AUTHENTICODE_ERROR_BASE + 18;
  SB_AUTHENTICODE_ERROR_NO_SIGNATURES         = SB_AUTHENTICODE_ERROR_BASE + 19;
  SB_AUTHENTICODE_ERROR_NO_COUNTERSIGNATURES  = SB_AUTHENTICODE_ERROR_BASE + 20;
  SB_AUTHENTICODE_ERROR_FILE_NOT_OPEN         = SB_AUTHENTICODE_ERROR_BASE + 21;
  SB_AUTHENTICODE_ERROR_SIGNATURE_NOT_LAST    = SB_AUTHENTICODE_ERROR_BASE + 22;
  SB_AUTHENTICODE_ERROR_INVALID_AUTHENTICODE  = SB_AUTHENTICODE_ERROR_BASE + 23;

type
  TChecksumContext = record
    Current: LongWord;
    Buffer: Word;
    BufferInUse: Boolean;
  end;
  PChecksumContext = ^TChecksumContext;

  TSBAuthenticodeDigestAlgorithm = (acMD5, acSHA1);
  TSBAuthenticodeDigestEncryptionAlgorithm = (acRSA, acDSA);

  TElAuthenticodeSigner = class(TComponent)
  private
    FAuthenticode: string;
    FAuthenticodeMD5: string;
    FAuthenticodeSHA1: string;
    FCertificates: TElMemoryCertStorage;
    FChecksum: TChecksumContext;
    FDigestMD5: TMessageDigest128;
    FDigestSHA1: TMessageDigest160;
    FHeaderOffset: LongWord;
    FPadding: string;
    FSignatures: TSBObjectList;
    FStream: TFileStream;
  protected
    function EncryptRSA(const Buffer: string; Certificate: TElX509Certificate): string;
    function FormatSignatureInfo(Description: WideString; URL: string): string;
    function SignDSA(const Buffer: string; Certificate: TElX509Certificate): string;
    function SignFile: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function AddAuthenticode(DigestAlgorithm: TSBAuthenticodeDigestAlgorithm): Boolean;
    function AddCertificate(Certificate: TElX509Certificate): Boolean;
    function AddSignature(Description: WideString; URL: string;
      Algorithm: TSBAuthenticodeDigestAlgorithm;
      Signer: TElX509Certificate; AddSigner: Boolean; AddTimestamp : boolean = false): Integer; overload;
    procedure Clear;
    function CloseFile(Apply: Boolean): Integer;
    function OpenFile(const FileName: string): Integer;
  end;

  TElAuthenticodeVerifier = class(TComponent)
  private
    FActualChecksum: LongWord;
    FActualDigest: string;
    FBaseChecksum: TChecksumContext;
    FCertStorage: TElCustomCertStorage;
    FChecksum: LongWord;
    FDigest: string;
    FDigestAlgorithm: TSBAuthenticodeDigestAlgorithm;
    FHeaderOffset: LongWord;
    FMessage: TElPKCS7Message;
    FSignatureOffset: LongWord;
    FSignatureSize: LongWord;
    //FSignatureMarker: LongWord;
    FStream: TFileStream;
    function GetCertificates: TElMemoryCertStorage;
    function GetSignature(Index: Integer): TElPKCS7Signer;
    function GetSignatureCount: Integer;
    procedure SetCertStorage(Value: TElCustomCertStorage);
  protected
    function DecryptRSA(const Buffer: string; Certificate: TElX509Certificate): string;
    function LoadAuthenticodeInfo(): Integer;
    procedure Notification(AComponent: TComponent; AOperation: TOperation); override;
    function ParseSignature(Buffer: Pointer; BufferSize: Cardinal): Integer;
    function ReadPEFile(): Integer;
    function VerifyDSA(const Algorithm: string; const Buffer: string;
      const Original: string; Certificate: TElX509Certificate): Boolean;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure CloseFile;
    function GetSignatureInfo(Index: Integer; out Description: WideString;
      out URL: string; out TimeStamp: TDateTime): Boolean;
    function OpenFile(const FileName: string): Integer;
    function RemoveAuthenticode: Integer;
    function VerifySignature(Index: Integer): Integer;
    property ActualChecksum: LongWord read FActualChecksum;
    property ActualDigest: string read FActualDigest;
    property Certificates: TElMemoryCertStorage read GetCertificates;
    property Checksum: LongWord read FChecksum;
    property Digest: string read FDigest;
    property DigestAlgorithm: TSBAuthenticodeDigestAlgorithm read FDigestAlgorithm;
    property SignatureCount: Integer read GetSignatureCount;
    property Signatures[Index: Integer]: TElPKCS7Signer read GetSignature;
  published
    property CertStorage: TElCustomCertStorage read FCertStorage write SetCertStorage;
  end;

procedure InitializeChecksum(var Context: TChecksumContext);
procedure CalcChecksum(var Context: TChecksumContext; Chunk: Pointer; Size: Cardinal);
function FinalizeChecksum(var Context: TChecksumContext; DataSize: Cardinal): Cardinal;

procedure Register;

implementation

uses
  Math, SBASN1Tree, SBConstants, SBMessages, SBRSA, SBDSA, SBRDN;

const
  SB_AUTHENTICODE_PE_IMAGE_DATA_STAMP: string =
    #$00#$3C#$00#$3C#$00#$3C#$00#$4F#$00#$62#$00#$73#$00#$6F +
    #$00#$6C#$00#$65#$00#$74#$00#$65#$00#$3E#$00#$3E#$00#$3E;

procedure Register;
begin
  RegisterComponents('PKIBlackbox', [TElAuthenticodeVerifier, TElAuthenticodeSigner]);
end;

{procedure SaveBufferToFile(const FileName: string; Buffer: Pointer; Size: Cardinal);
var
  Stream: TFileStream;
begin
  Stream := TFileStream.Create(FileName, fmCreate);
  try
    Stream.WriteBuffer(Buffer^, Size);
  finally
    Stream.Free;
  end;
end;}

type
  TElAuthenticodeSignature = class
  private
    FAttributes: TElPKCS7Attributes;
    FDigestAlgorithm: string;
    FEncryptedDigest: string;
    FEncryptionAlgorithm: string;
    FIssuer: TElRelativeDistinguishedName;
    FSerialNumber: string;
  public
    constructor Create;
    destructor Destroy; override;
    property Attributes: TElPKCS7Attributes read FAttributes;
    property Issuer: TElRelativeDistinguishedName read FIssuer;
    property DigestAlgorithm: string read FDigestAlgorithm write FDigestAlgorithm;
    property EncryptedDigest: string read FEncryptedDigest write FEncryptedDigest;
    property EncryptionAlgorithm: string read FEncryptionAlgorithm write FEncryptionAlgorithm;
    property SerialNumber: string read FSerialNumber write FSerialNumber;
  end;

  TElCertificateHack = class(TElX509Certificate);

procedure InitializeChecksum(var Context: TChecksumContext);
begin
  Context.Current := 0;
  Context.Buffer := 0;
  Context.BufferInUse := False;
end;

procedure CalcChecksum(var Context: TChecksumContext; Chunk: Pointer; Size: Cardinal);
var
  P: PWord;
begin
  if (Chunk = nil) or (Size = 0) then
    Exit;
  if Context.BufferInUse then
  begin
    WordRec(Context.Buffer).Lo := PByte(Chunk)^;
    Inc(Context.Current, Context.Buffer);
    if LongRec(Context.Current).Hi > 0 then
      Context.Current := LongRec(Context.Current).Lo + LongRec(Context.Current).Hi;
    Context.Buffer := 0;
    Context.BufferInUse := False;
    P := PWord(PtrUInt(Chunk) + 1);
    Dec(Size);
  end
  else
    P := Chunk;
  while Size > 1 do
  begin
    Inc(Context.Current, P^);
    if LongRec(Context.Current).Hi > 0 then
      Context.Current := LongRec(Context.Current).Lo + LongRec(Context.Current).Hi;
    Inc(PtrUInt(P), 2);
    Dec(Size, 2);
  end;
  if Size > 0 then
  begin
    WordRec(Context.Buffer).Hi := PByte(P)^;
    Context.BufferInUse := True;
  end;
end;

function FinalizeChecksum(var Context: TChecksumContext; DataSize: Cardinal): Cardinal;
begin
  if Context.BufferInUse then
  begin
    Inc(Context.Current, Context.Buffer);
    if LongRec(Context.Current).Hi > 0 then
      Context.Current := LongRec(Context.Current).Lo + LongRec(Context.Current).Hi;
  end;
  Inc(Context.Current, DataSize);
  Result := Context.Current;
end;

{ TElAuthenticodeVerifier }

procedure TElAuthenticodeVerifier.CloseFile;
begin
  FActualChecksum := 0;
  FActualDigest := '';
  FChecksum := 0;
  FDigest := '';
  FDigestAlgorithm := acMD5;
  FHeaderOffset := 0;
  FSignatureOffset := 0;
  FSignatureSize := 0;
  FreeAndNil(FMessage);
  FillChar(FBaseChecksum, SizeOf(FBaseChecksum), 0);
  FreeAndNil(FStream);
end;

constructor TElAuthenticodeVerifier.Create(AOwner: TComponent);
begin
  inherited;
  FActualChecksum := 0;
  FActualDigest := '';
  FChecksum := 0;
  FDigest := '';
  FDigestAlgorithm := acMD5;
  FHeaderOffset := 0;
  FMessage := nil;
  FSignatureOffset := 0;
  FSignatureSize := 0;
  FStream := nil;
  FillChar(FBaseChecksum, SizeOf(FBaseChecksum), 0);
end;

function TElAuthenticodeVerifier.DecryptRSA(const Buffer: string;
  Certificate: TElX509Certificate): string;
var
  M, E: Pointer;
  MSize, ESize, Size: Integer;
  S: string;
  Tag: TElASN1ConstrainedTag;
  Field: TElASN1CustomTag;
begin
  Result := '';
  MSize := 0;
  ESize := 0;
  Certificate.GetRSAParams(nil, MSize, nil, ESize);
  if (MSize = 0) or (ESize = 0) then
    Exit;
  GetMem(M, MSize);
  GetMem(E, ESize);
  try
    if not Certificate.GetRSAParams(M, MSize, E, ESize) then
      Exit;
    Size := MSize;
    SetLength(S, Size);
    if not SBRSA.Decrypt(@Buffer[1], Length(Buffer), M, MSize, E, ESize, @S[1], Size) then
      Exit;
    SetLength(S, Size);
  finally
    FreeMem(M);
    FreeMem(E);
  end;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if not Tag.LoadFromBuffer(@S[1], Size) or (Tag.Count = 0) then
      Exit;
    Field := Tag.GetField(0);
    if (Field = nil) or not Field.IsConstrained or (Field.TagID <> SB_ASN1_SEQUENCE) then
      Exit;
    if TElASN1ConstrainedTag(Field).Count <> 2 then
      Exit;
    Field := TElASN1ConstrainedTag(Field).GetField(1);
    if Field = nil then
      Exit;
    if Field.IsConstrained or (Field.TagID <> SB_ASN1_OCTETSTRING) then
      Exit;
    Result := TElASN1SimpleTag(Field).Content;
  finally
    Tag.Free;
  end;
end;

destructor TElAuthenticodeVerifier.Destroy;
begin
  CloseFile;
  inherited;
end;

function TElAuthenticodeVerifier.GetCertificates: TElMemoryCertStorage;
begin
  Result := nil;
  if FMessage <> nil then
    Result := FMessage.SignedData.Certificates;
end;

function TElAuthenticodeVerifier.GetSignature(Index: Integer): TElPKCS7Signer;
begin
  Result := nil;
  if FMessage <> nil then
    if FMessage.ContentType = ctSignedData then
      Result := FMessage.SignedData.Signers[Index];
end;

function TElAuthenticodeVerifier.GetSignatureCount: Integer;
begin
  Result := 0;
  if FMessage <> nil then
     Result := FMessage.SignedData.SignerCount;
end;

function TElAuthenticodeVerifier.GetSignatureInfo(Index: Integer;
  out Description: WideString; out URL: string; out TimeStamp : TDateTime): Boolean;
var
  I, Info: Integer;
  Signer: TElPKCS7Signer;
  S: string;
  Tag: TElASN1ConstrainedTag;
  Field, Param: TElASN1CustomTag;
begin
  Result := False;
  Description := '';
  URL := '';
  if FMessage = nil then
    Exit;
  if (Index < 0) or (Index >= FMessage.SignedData.SignerCount) then
    Exit;
  Signer := FMessage.SignedData.Signers[Index];
  if Signer = nil then
    Exit;
  if Signer.AuthenticatedAttributes.Count = 0 then
    Exit;

  TimeStamp := 0;
  for I := 0 to Signer.AuthenticatedAttributes.Count - 1 do
    if Signer.AuthenticatedAttributes.Attributes[I] = SB_OID_SIGNING_TIME then
    begin
      try
        TimeStamp := UTCTimeToDateTime(Copy(Signer.AuthenticatedAttributes.Values[I].Strings[0], 3,
          Length(Signer.AuthenticatedAttributes.Values[I].Strings[0])));
      except
        TimeStamp := 0;
      end;
      Break;
    end;

  Info := -1;
  for I := 0 to Signer.AuthenticatedAttributes.Count - 1 do
    if Signer.AuthenticatedAttributes.Attributes[I] = SB_OID_SPC_SP_OPUS_INFO then
    begin
      Info := I;
      Break;
    end;
  Result := True;
  if Info < 0 then
    Exit;
  if Signer.AuthenticatedAttributes.Values[Info] = nil then
    Exit;
  if Signer.AuthenticatedAttributes.Values[Info].Count = 0 then
    Exit;
  S := Signer.AuthenticatedAttributes.Values[Info].Strings[0];
  if S = '' then
    Exit;
  Result := False;
  try
    Tag := TElASN1ConstrainedTag.Create;
    try
      if Tag.LoadFromBuffer(@S[1], Length(S)) then
      begin
        S := '';
        if Tag.Count = 0 then
          Exit;
        Field := Tag.GetField(0);
        if not Field.IsConstrained or (Field.TagID <> SB_ASN1_SEQUENCE) then
          Exit;
        // read description
        Param := TElASN1ConstrainedTag(Field).GetField(0);
        if (Param = nil) or (Param.TagID <> SB_ASN1_A0) or not Param.IsConstrained then
          Exit;
        Param := TElASN1ConstrainedTag(Param).GetField(0);
        if not (Param is TElASN1SimpleTag) then
          Exit;
        S := TElASN1SimpleTag(Param).Content;
        SwapBigEndianWords(@S[1], Length(S));
        SetLength(Description, Length(S) shr 1);
        Move(S[1], Description[1], Length(S));
        // read URL info
        Param := TElASN1ConstrainedTag(Field).GetField(1);
        if (Param = nil) or (Param.TagID <> SB_ASN1_A1) then
          Exit;
        Param := TElASN1ConstrainedTag(Param).GetField(0);
        if not (Param is TElASN1SimpleTag) then
          Exit;
        URL := TElASN1SimpleTag(Param).Content;
        Result := True;
      end;
    finally
      Tag.Free;
    end;
  except
    // ignore all errors; just return False
  end;
end;

function TElAuthenticodeVerifier.LoadAuthenticodeInfo: Integer;
var
  ASN, Sequence: TElASN1ConstrainedTag;
  Field, Param: TElASN1CustomTag;
begin
  Result := SB_AUTHENTICODE_ERROR_SIGNATURE_CORRUPTED;
  if FMessage.SignedData.Content = '' then
    Exit
  else
  begin
    try
      ASN := TElASN1ConstrainedTag.Create;
      try
        try
          if not ASN.LoadFromBuffer(@FMessage.SignedData.Content[1], Length(FMessage.SignedData.Content)) then
            Exit;
        except
          Exit;
        end;
        if ASN.Count = 0 then
          Exit;
        Field := ASN.GetField(0);
        if (Field = nil) or not Field.IsConstrained then
          Exit;
        if TElASN1ConstrainedTag(Field).Count < 2 then
          Exit;
        // check PE Image Data sequence
        Param := TElASN1ConstrainedTag(Field).GetField(0);
        if (Param = nil) or not Param.IsConstrained then
          Exit;
        Sequence := TElASN1ConstrainedTag(Param);
        if Sequence.Count < 2 then
          Exit;
        Param := Sequence.GetField(0);
        if (Param = nil) or (Param.TagID <> SB_ASN1_OBJECT) then
          Exit;
        if TElASN1SimpleTag(Param).Content <> SB_OID_SPC_PE_IMAGE_DATA then
          Exit;
        Param := Sequence.GetField(1);
        if (Param = nil) or not Param.IsConstrained or
          (TElASN1ConstrainedTag(Param).Count < 2) then
          Exit;
        Param := TElASN1ConstrainedTag(Param).GetField(1);
        if (Param = nil) or not Param.IsConstrained or
          (Param.TagID <> SB_ASN1_A0) then
          Exit;
        Param := TElASN1ConstrainedTag(Param).GetField(0);
        if (Param = nil) or not Param.IsConstrained or
          (Param.TagID <> SB_ASN1_A2) then
          Exit;
        Param := TElASN1ConstrainedTag(Param).GetField(0);
        if (Param = nil) or (Param.TagID <> SB_ASN1_A0_PRIMITIVE) then
          Exit;
        if TElASN1SimpleTag(Param).Content <> SB_AUTHENTICODE_PE_IMAGE_DATA_STAMP then
          Exit;
        // check Digest sequence
        Param := TElASN1ConstrainedTag(Field).GetField(1);
        if (Param = nil) or not Param.IsConstrained then
          Exit;
        Sequence := TElASN1ConstrainedTag(Param);
        if Sequence.Count < 2 then
          Exit;
        Param := Sequence.GetField(0);
        if (Param = nil) or not Param.IsConstrained then
          Exit;
        if TElASN1ConstrainedTag(Param).Count = 0 then
          Exit;
        Param := TElASN1ConstrainedTag(Param).GetField(0);
        if (Param = nil) or (Param.TagID <> SB_ASN1_OBJECT) then
          Exit;
        if TElASN1SimpleTag(Param).Content = SB_OID_MD5 then
          FDigestAlgorithm := acMD5
        else
        if TElASN1SimpleTag(Param).Content = SB_OID_SHA1 then
          FDigestAlgorithm := acSHA1
        else
        begin
          Result := SB_AUTHENTICODE_ERROR_INVALID_ALGORITHM;
          Exit;
        end;
        Param := Sequence.GetField(1);
        if (Param = nil) or (Param.TagID <> SB_ASN1_OCTETSTRING) then
          Exit;
        case FDigestAlgorithm of
          acMD5 : if Length(TElASN1SimpleTag(Param).Content) = SizeOf(TMessageDigest128) then
                    FDigest := TElASN1SimpleTag(Param).Content
                  else
                  begin
                    Result := SB_AUTHENTICODE_ERROR_INVALID_DIGEST_SIZE;
                    Exit;
                  end;
          acSHA1: if Length(TElASN1SimpleTag(Param).Content) = SizeOf(TMessageDigest160) then
                    FDigest := TElASN1SimpleTag(Param).Content
                  else
                  begin
                    Result := SB_AUTHENTICODE_ERROR_INVALID_DIGEST_SIZE;
                    Exit;
                  end;
        end;
        Result := SB_AUTHENTICODE_ERROR_SUCCESS;
      finally
        ASN.Free;
      end;
    except
      Result := SB_AUTHENTICODE_ERROR_UNKNOWN;
    end;
  end;
end;

procedure TElAuthenticodeVerifier.Notification(AComponent: TComponent;
  AOperation: TOperation);
begin
  inherited;
  if (AComponent = FCertStorage) and (AOperation = opRemove) then
    SetCertStorage(nil);
end;

function TElAuthenticodeVerifier.OpenFile(const FileName: string): Integer;
begin
  CheckLicenseKey();
  try
    FStream := TFileStream.Create(FileName, fmOpenReadWrite or fmShareDenyWrite);
  except
    Result := SB_AUTHENTICODE_ERROR_CANNOT_OPEN_FILE;
    Exit;
  end;
  try
    Result := ReadPEFile();
  except
    Result := SB_AUTHENTICODE_ERROR_UNKNOWN;
  end;
end;

function TElAuthenticodeVerifier.ParseSignature(Buffer: Pointer; BufferSize: Cardinal): Integer;
begin
  Result := SB_AUTHENTICODE_ERROR_SIGNATURE_CORRUPTED;
  if BufferSize < 8 then
    Exit;
  try
    // load PKCS7 message and parse it
    FMessage := TElPKCS7Message.Create;
    try
      // check for size of the signature
      if PCardinal(Buffer)^ <> BufferSize then
        Exit;
      Inc(PtrUInt(Buffer), 4);
      Dec(BufferSize, 4);
      // check for 'magic' number
      if PCardinal(Buffer)^ <> $00020200 then
        Exit;
      Inc(PtrUInt(Buffer), 4);
      Dec(BufferSize, 4);
      if BufferSize = 0 then
        Exit;
      //SaveBufferToFile('D:\test.bin', Buffer, BufferSize);
      if FMessage.LoadFromBuffer(Buffer, BufferSize) <> 0 then
        Exit;
      // examine loaded PKCS7 message
      if FMessage.ContentType <> ctSignedData then
        Exit;
      if FMessage.SignedData.ContentType <> SB_OID_SPC_INDIRECT_DATA then
        Exit;
      // parse authenticode data
      Result := LoadAuthenticodeInfo();
      if Result <> SB_AUTHENTICODE_ERROR_SUCCESS then
        Exit;
    finally
      if Result <> SB_AUTHENTICODE_ERROR_SUCCESS then
        FreeAndNil(FMessage);
    end;
  except
    Result := SB_AUTHENTICODE_ERROR_UNKNOWN;
  end;
end;

function TElAuthenticodeVerifier.ReadPEFile(): Integer;
var
  MZSign: Word;
  Checksum: TChecksumContext;
  MD5: TMD5Context;
  SHA1: TSHA1Context;
  HeaderOffset, PESign, SignatureOffset, SignatureSize: LongWord;
  Signature: Pointer;
  Total, Read: Cardinal;
  DigestMD5: TMessageDigest128;
  DigestSHA1: TMessageDigest160;

  function SeekStream(Position: Cardinal; SkipDigests: Boolean = False): Boolean;
  var
    Left, Read: Cardinal;
    Buffer: array [0..4095] of Byte;
  begin
    Result := False;
    Left := Position - cardinal(FStream.Position);
    try
      while Left > 0 do
      begin
        Read := FStream.Read(Buffer, Min(Left, SizeOf(Buffer)));
        if Read = 0 then
          Exit;
        CalcChecksum(Checksum, @Buffer[0], Read);
        if not SkipDigests then
        begin
          HashMD5(MD5, @Buffer[0], Read);
          HashSHA1(SHA1, @Buffer[0], Read);
        end;
        Dec(Left, Read);
      end;
    except
      Exit;
    end;
    Result := True;
  end;

begin
  InitializeChecksum(Checksum);
  InitializeMD5(MD5);
  InitializeSHA1(SHA1);
  // the file must be at least 64 bytes in size
  if FStream.Size < 64 then
  begin
    Result := SB_AUTHENTICODE_ERROR_FILE_TOO_SMALL;
    Exit;
  end;
  // check for DOS EXE signature 'MZ'
  if FStream.Read(MZSign, 2) < 2 then
  begin
    Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
    Exit;
  end;
  if MZSign <> $5A4D then
  begin
    Result := SB_AUTHENTICODE_ERROR_INVALID_EXECUTABLE;
    Exit;
  end;
  // add 'MZ' to checksum and hashes
  CalcChecksum(Checksum, @MZSign, 2);
  HashMD5(MD5, @MZSign, 2);
  HashSHA1(SHA1, @MZSign, 2);
  // get new header offset
  if not SeekStream($0000003C) then
  begin
    Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
    Exit;
  end;
  if FStream.Read(HeaderOffset, 4) < 4 then
  begin
    Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
    Exit;
  end;
  if HeaderOffset + $A0 > Cardinal(FStream.Size) then
  begin
    Result := SB_AUTHENTICODE_ERROR_INVALID_PE_FORMAT;
    Exit;
  end;
  FHeaderOffset := HeaderOffset;
  // add found header offset to checksum and hashes
  CalcChecksum(Checksum, @HeaderOffset, 4);
  HashMD5(MD5, @HeaderOffset, 4);
  HashSHA1(SHA1, @HeaderOffset, 4);
  // check for PE signature
  if not SeekStream(HeaderOffset) then
  begin
    Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
    Exit;
  end;
  if FStream.Read(PESign, 4) < 4 then
  begin
    Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
    Exit;
  end;
  if PESign <> $00004550 then
  begin
    Result := SB_AUTHENTICODE_ERROR_INVALID_PE_FORMAT;
    Exit;
  end;
  // add PE signature to checksum and hashes
  CalcChecksum(Checksum, @PESign, 4);
  HashMD5(MD5, @PESign, 4);
  HashSHA1(SHA1, @PESign, 4);
  // get current checksum
  if not SeekStream(HeaderOffset + $58) then
  begin
    Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
    Exit;
  end;
  if FStream.Read(FChecksum, 4) < 4 then
  begin
    Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
    Exit;
  end;
  // get signature section parameters
  if not SeekStream(HeaderOffset + $98) then
  begin
    Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
    Exit;
  end;
  //FSignatureMarker := HeaderOffset + $98;
  if FStream.Read(SignatureOffset, 4) < 4 then
  begin
    Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
    Exit;
  end; 
  if FStream.Read(SignatureSize, 4) < 4 then
  begin
    Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
    Exit;
  end;
  // check signature section integrity
  if SignatureOffset = 0 then
    SignatureSize := 0
  else
  if (SignatureSize = 0) or (SignatureOffset + SignatureSize > Cardinal(FStream.Size)) then
  begin
    Result := SB_AUTHENTICODE_ERROR_SIGNATURE_CORRUPTED;
    Exit;
  end;
  FSignatureOffset := SignatureOffset;
  FSignatureSize := SignatureSize;
  
  // check signature section
  if SignatureOffset > 0 then
  begin
    // read data from signature section
    if not SeekStream(SignatureOffset) then
    begin
      Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
      Exit;
    end;
    GetMem(Signature, SignatureSize);
    try
      Total := 0;
      repeat
        Read := FStream.Read(Pointer(PtrUInt(Signature) + Total)^, SignatureSize - Total);
        if Read = 0 then
        begin
          Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
          Exit;
        end;
        Inc(Total, Read);
      until Total = SignatureSize;
      CalcChecksum(Checksum, @SignatureOffset, 4);
      CalcChecksum(Checksum, @SignatureSize, 4);
      CalcChecksum(Checksum, Signature, Total);
      if not SeekStream(FStream.Size, True) then
      begin
        Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
        Exit;
      end;
      Result := ParseSignature(Signature, SignatureSize);
      if Result = SB_AUTHENTICODE_ERROR_SUCCESS then
      begin
        FActualChecksum := FinalizeChecksum(Checksum, FStream.Size);
        case FDigestAlgorithm of
          acMD5 : begin
                    DigestMD5 := FinalizeMD5(MD5);
                    SetLength(FActualDigest, SizeOf(DigestMD5));
                    Move(DigestMD5, FActualDigest[1], SizeOf(DigestMD5));
                  end;
          acSHA1: begin
                    DigestSHA1 := FinalizeSHA1(SHA1);
                    SetLength(FActualDigest, SizeOf(DigestSHA1));
                    Move(DigestSHA1, FActualDigest[1], SizeOf(DigestSHA1));
                  end;
        end;
      end;
    finally
      FreeMem(Signature);
    end;
  end
  else
    Result := SB_AUTHENTICODE_ERROR_FILE_NOT_SIGNED;
end;

function TElAuthenticodeVerifier.RemoveAuthenticode: Integer;
var
  Checksum: LongWord;
begin
  // check the current state and ability to remove authenticode signature
  Result := SB_AUTHENTICODE_ERROR_FILE_NOT_OPEN;
  if FStream = nil then
    Exit;
  Result := SB_AUTHENTICODE_ERROR_FILE_NOT_SIGNED;
  if FMessage = nil then
    Exit;
  // check if authenticode is placed last in the file
  Result := SB_AUTHENTICODE_ERROR_SIGNATURE_NOT_LAST;
  if (FSignatureOffset + FSignatureSize) < cardinal(FStream.Size) then
    Exit;
  // truncate the file before signature section
  Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
  if Cardinal(FStream.Seek(FSignatureOffset, soFromBeginning)) <> FSignatureOffset then
    Exit;
  Result := SB_AUTHENTICODE_ERROR_CANNOT_WRITE_FILE;
  if not SetEndOfFile(FStream.Handle) then
    Exit;
  // prepare new checksum
  FSignatureOffset := 0;
  FSignatureSize := 0;
  CalcChecksum(FBaseChecksum, @FSignatureOffset, SizeOf(FSignatureOffset));
  CalcChecksum(FBaseChecksum, @FSignatureSize, SizeOf(FSignatureSize));
  Checksum := FinalizeChecksum(FBaseChecksum, FStream.Size);
  // write the new signature parameters to the file
  Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
  if Cardinal(FStream.Seek(FHeaderOffset + $98, soFromBeginning)) <> (FHeaderOffset + $98) then
    Exit;
  Result := SB_AUTHENTICODE_ERROR_CANNOT_WRITE_FILE;
  if FStream.Write(FSignatureOffset, 4) < 4 then
    Exit;
  if FStream.Write(FSignatureSize, 4) < 4 then
    Exit;
  // write the new checksum to the file
  Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
  if Cardinal(FStream.Seek(FHeaderOffset + $58, soFromBeginning)) <> (FHeaderOffset + $58) then
    Exit;
  if FStream.Write(Checksum, 4) < 4 then
    Exit;
  CloseFile();
  Result := SB_AUTHENTICODE_ERROR_SUCCESS;
end;

procedure TElAuthenticodeVerifier.SetCertStorage(Value: TElCustomCertStorage);
begin
  if FCertStorage <> Value then
  begin
    {$ifdef VCL_6_USED}
    if FCertStorage <> nil then
      FCertStorage.RemoveFreeNotification(Self);
    {$endif}
    FCertStorage := Value;
    if FCertStorage <> nil then
      FCertStorage.FreeNotification(Self);
  end;
end;

function TElAuthenticodeVerifier.VerifyDSA(const Algorithm: string; const Buffer: string;
  const Original: string; Certificate: TElX509Certificate): Boolean;
var
  Envelope: string;
  R, S, P, Q, G, Y: Pointer;
  RSize, SSize, PSize, QSize, GSize, YSize, Size: Integer;
  Tag, Field: TElASN1ConstrainedTag;
  Param: TElASN1SimpleTag;
begin
  Result := False;
  // envelope original digest to verify the given one
  Tag := TElASN1ConstrainedTag.Create;
  try
    Tag.TagID := SB_ASN1_SEQUENCE;
    if Tag.AddField(True) <> 0 then
      Exit;
    Field := TElASN1ConstrainedTag(Tag.GetField(0));
    Field.TagID := SB_ASN1_SEQUENCE;
    if Field.AddField(False) <> 0 then
      Exit;
    Param := TElASN1SimpleTag(Field.GetField(0));
    Param.TagID := SB_ASN1_OBJECT;
    Param.Content := Algorithm;
    if Field.AddField(False) <> 1 then
      Exit;
    Param := TElASN1SimpleTag(Field.GetField(1));
    Param.TagID := SB_ASN1_NULL;
    Param.Content := '';
    if Tag.AddField(False) <> 1 then
      Exit;
    Param := TElASN1SimpleTag(Tag.GetField(1));
    Param.TagID := SB_ASN1_OCTETSTRING;
    Param.Content := Original;
    Size := 0;
    Tag.SaveToBuffer(nil, Size);
    if Size = 0 then
      Exit;
    SetLength(Envelope, Size);
    if not Tag.SaveToBuffer(@Envelope[1], Size) then
      Exit;
    SetLength(Envelope, Size);
  finally
    Tag.Free;
  end;
  // verify the given digest
  RSize := 21;
  SSize := 21;
  GetMem(R, RSize);
  GetMem(S, SSize);
  try
    if not SBDSA.DecodeSignature(@Buffer[1], Length(Buffer), R, RSize, S, SSize) then
      Exit;
      
    PSize := 0;
    QSize := 0;
    GSize := 0;
    YSize := 0;
    Certificate.GetDSSParams(nil, PSize, nil, QSize, nil, GSize, nil, YSize);
    if (PSize = 0) or (QSize = 0) or (GSize = 0) or (YSize = 0) then
      Exit;
    GetMem(P, PSize);
    GetMem(Q, QSize);
    GetMem(G, GSize);
    GetMem(Y, YSize);
    try
      if not Certificate.GetDSSParams(P, PSize, Q, QSize, G, GSize, Y, YSize) then
        Exit;

      if SBDSA.ValidateSignature(@Envelope[1], Length(Envelope), P, PSize, Q, QSize, G, GSize, Y, YSize, R, RSize, S, SSize) then
        Result := True;
    finally
      FreeMem(P);
      FreeMem(Q);
      FreeMem(G);
      FreeMem(Y);
    end;
  finally
    FreeMem(R);
    FreeMem(S);
  end;
end;

function TElAuthenticodeVerifier.VerifySignature(Index: Integer): Integer;
var
  Signer: TElPKCS7Signer;
  Certificate: TElX509Certificate;
  Lookup: TElCertificateLookup;
  I, TagID: Integer;
  IncludedDigest, OriginalDigest, DecryptedDigest: string;
  MD5: TMessageDigest128;
  SHA1: TMessageDigest160;
  Data: string;
begin
  try
    // check for signature index validity
    Result := SB_AUTHENTICODE_ERROR_INVALID_INDEX;
    if (Index < 0) or (Index >= FMessage.SignedData.SignerCount) then
      Exit;
    Signer := FMessage.SignedData.Signers[Index];
    if Signer = nil then
      Exit;
    // attempt to find singer's certificate
    Result := SB_AUTHENTICODE_ERROR_NO_SIGNER_CERTIFICATE;
    Certificate := nil;
    Lookup := TElCertificateLookup.Create(nil);
    try
      Lookup.Criteria := [lcIssuer];
      Lookup.Options := [loExactMatch, loMatchAll];
      Lookup.IssuerRDN.Assign(Signer.Issuer.Issuer);
      I := FMessage.SignedData.Certificates.FindFirst(Lookup);
      while I >= 0 do
      begin
        if FMessage.SignedData.Certificates.Certificates[I].SerialNumber = Signer.Issuer.SerialNumber then
        begin
          Certificate := FMessage.SignedData.Certificates.Certificates[I];
          Break;
        end;
        I := FMessage.SignedData.Certificates.FindNext(Lookup);
      end;
    finally
      Lookup.Free;
    end;
    if (Certificate = nil) and (FCertStorage <> nil) then
    begin
      Lookup := TElCertificateLookup.Create(nil);
      try
        Lookup.Criteria := [lcIssuer];
        Lookup.Options := [loExactMatch, loMatchAll];
        Lookup.IssuerRDN.Assign(Signer.Issuer.Issuer);
        I := FCertStorage.FindFirst(Lookup);
        while I >= 0 do
        begin
          if FCertStorage.Certificates[I].SerialNumber = Signer.Issuer.SerialNumber then
          begin
            Certificate := FCertStorage.Certificates[I];
            Break;
          end;
          I := FCertStorage.FindNext(Lookup);
        end;
      finally
        Lookup.Free;
      end;
    end;
    if Certificate = nil then
      Exit;
    // calculate digest for authenticated attributes
    if Signer.DigestAlgorithm = SB_OID_MD5 then
    begin
      MD5 := HashMD5(Signer.AuthenticatedAttributesPlain);
      SetLength(OriginalDigest, SizeOf(MD5));
      Move(MD5, OriginalDigest[1], SizeOf(MD5));
    end
    else
    if Signer.DigestAlgorithm = SB_OID_SHA1 then
    begin
      SHA1 := HashSHA1(Signer.AuthenticatedAttributesPlain);
      SetLength(OriginalDigest, SizeOf(SHA1));
      Move(SHA1, OriginalDigest[1], SizeOf(SHA1));
    end
    else
    begin
      Result := SB_AUTHENTICODE_ERROR_INVALID_ALGORITHM;
      Exit;
    end;
    // check for authenticated attributes digest validity
    Result := SB_AUTHENTICODE_ERROR_INVALID_SIGNATURE;
    if (Signer.DigestEncryptionAlgorithm = SB_OID_RSAENCRYPTION) and
      (Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) then
    begin
      DecryptedDigest := DecryptRSA(Signer.EncryptedDigest, Certificate);
      if DecryptedDigest = '' then
        Exit;
      // compare the decrypted digest with calculated one
      if DecryptedDigest <> OriginalDigest then
        Exit;
    end
    else
    if (Signer.DigestEncryptionAlgorithm = SB_OID_DSA) and
      (Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA) then
    begin
      if not VerifyDSA(Signer.DigestAlgorithm, Signer.EncryptedDigest, OriginalDigest, Certificate) then
        Exit;
    end
    else
    begin
      Result := SB_AUTHENTICODE_ERROR_INVALID_ALGORITHM;
      Exit;
    end;
    // attempt to find included digest for authenticode data
    Result := SB_AUTHENTICODE_ERROR_SIGNATURE_CORRUPTED;
    IncludedDigest := '';
    if Signer.AuthenticatedAttributes.Count > 0 then
    begin
      for I := 0 to Signer.AuthenticatedAttributes.Count - 1 do
        if (Signer.AuthenticatedAttributes.Attributes[I] = SB_OID_MESSAGE_DIGEST) and
          (Signer.AuthenticatedAttributes.Values[I].Count > 0) then
        begin
          IncludedDigest := UnformatAttributeValue(Signer.AuthenticatedAttributes.Values[I].Strings[0], TagID);
          Break;
        end;
    end;
    if IncludedDigest = '' then
      Exit;
    // calculate digest for authenticode data
    Result := SB_AUTHENTICODE_ERROR_SIGNATURE_CORRUPTED;
    if Length(FMessage.SignedData.Content) < 2 then
      Exit;
    if Ord(FMessage.SignedData.Content[1]) <> $30 then
      Exit;
    case Ord(FMessage.SignedData.Content[2]) of
      $81: Data := Copy(FMessage.SignedData.Content, 4, MaxInt);
      $82: Data := Copy(FMessage.SignedData.Content, 5, MaxInt);
      $83: Data := Copy(FMessage.SignedData.Content, 6, MaxInt);
    else
      if Ord(FMessage.SignedData.Content[2]) < $81 then
        Data := Copy(FMessage.SignedData.Content, 3, MaxInt)
      else
        Exit;
    end;
    if Signer.DigestAlgorithm = SB_OID_MD5 then
    begin
      MD5 := HashMD5(Data);
      SetLength(OriginalDigest, SizeOf(MD5));
      Move(MD5, OriginalDigest[1], SizeOf(MD5));
    end
    else
    if Signer.DigestAlgorithm = SB_OID_SHA1 then
    begin
      SHA1 := HashSHA1(Data);
      SetLength(OriginalDigest, SizeOf(SHA1));
      Move(SHA1, OriginalDigest[1], SizeOf(SHA1));
    end
    else
    begin
      Result := SB_AUTHENTICODE_ERROR_INVALID_ALGORITHM;
      Exit;
    end;
    // check for authenticode data digest validity
    if OriginalDigest <> IncludedDigest then
    begin
      Result := SB_AUTHENTICODE_ERROR_INVALID_SIGNATURE;
      Exit;
    end;
    if (FActualDigest <> FDigest) then
      Result := SB_AUTHENTICODE_ERROR_INVALID_AUTHENTICODE
    else
      Result := SB_AUTHENTICODE_ERROR_SUCCESS;
  except
    Result := SB_AUTHENTICODE_ERROR_UNKNOWN;
  end;
end;

{ TElAuthenticodeSignature }

constructor TElAuthenticodeSignature.Create;
begin
  inherited;
  FAttributes := TElPKCS7Attributes.Create;
  FIssuer := TElRelativeDistinguishedName.Create;
end;

destructor TElAuthenticodeSignature.Destroy;
begin
  FreeAndNil(FIssuer);
  FreeAndNil(FAttributes);
  inherited;
end;

{ TElAuthenticodeSigner }

function TElAuthenticodeSigner.AddAuthenticode(
  DigestAlgorithm: TSBAuthenticodeDigestAlgorithm): Boolean;
var
  Digest: string;
  Tag, Field: TElASN1ConstrainedTag;
  Param: TElASN1CustomTag;
  Value: TElASN1SimpleTag;
  Size: Integer;
  MD5: TMessageDigest128;
  SHA1: TMessageDigest160;
begin
  Result := False;
  if FAuthenticode <> '' then
    Exit;
  try
    case DigestAlgorithm of
      acMD5 : begin
                SetLength(Digest, SizeOf(FDigestMD5));
                Move(FDigestMD5, Digest[1], SizeOf(FDigestMD5));
              end;
      acSHA1: begin
                SetLength(Digest, SizeOf(FDigestSHA1));
                Move(FDigestSHA1, Digest[1], SizeOf(FDigestSHA1));
              end;
    end;
    Tag := TElASN1ConstrainedTag.Create;
    try
      // compose ASN.1 Tree for authenticode data
      Tag.TagID := SB_ASN1_SEQUENCE;
      // add PE Image Data
      if Tag.AddField(True) <> 0 then
        Exit;
      Field := TElASN1ConstrainedTag(Tag.GetField(0));
      Field.TagID := SB_ASN1_SEQUENCE;
      if Field.AddField(False) <> 0 then
        Exit;
      Value := TElASN1SimpleTag(Field.GetField(0));
      Value.TagID := SB_ASN1_OBJECT;
      Value.Content := SB_OID_SPC_PE_IMAGE_DATA;
      if Field.AddField(True) <> 1 then
        Exit;
      Param := Field.GetField(1);
      Param.TagID := SB_ASN1_SEQUENCE;
      if TElASN1ConstrainedTag(Param).AddField(False) <> 0 then
        Exit;
      Value := TElASN1SimpleTag(TElASN1ConstrainedTag(Param).GetField(0));
      Value.TagID := SB_ASN1_BITSTRING;
      Value.Content := #0;
      if TElASN1ConstrainedTag(Param).AddField(True) <> 1 then
        Exit;
      Param := TElASN1ConstrainedTag(Param).GetField(1);
      Param.TagID := SB_ASN1_A0;
      if TElASN1ConstrainedTag(Param).AddField(True) <> 0 then
        Exit;
      Param := TElASN1ConstrainedTag(Param).GetField(0);
      Param.TagID := SB_ASN1_A2;
      if TElASN1ConstrainedTag(Param).AddField(False) <> 0 then
        Exit;
      Param := TElASN1ConstrainedTag(Param).GetField(0);
      Param.TagID := SB_ASN1_A0_PRIMITIVE;
      TElASN1SimpleTag(Param).Content := SB_AUTHENTICODE_PE_IMAGE_DATA_STAMP;
      // add Digest
      if Tag.AddField(True) <> 1 then
        Exit;
      Field := TElASN1ConstrainedTag(Tag.GetField(1));
      Field.TagID := SB_ASN1_SEQUENCE;
      if Field.AddField(True) <> 0 then
        Exit;
      Param := Field.GetField(0);
      Param.TagID := SB_ASN1_SEQUENCE;
      if TElASN1ConstrainedTag(Param).AddField(False) <> 0 then
        Exit;
      Value := TElASN1SimpleTag(TElASN1ConstrainedTag(Param).GetField(0));
      Value.TagID := SB_ASN1_OBJECT;
      if DigestAlgorithm = acMD5 then
        Value.Content := SB_OID_MD5
      else
        Value.Content := SB_OID_SHA1;
      if TElASN1ConstrainedTag(Param).AddField(False) <> 1 then
        Exit;
      Value := TElASN1SimpleTag(TElASN1ConstrainedTag(Param).GetField(1));
      Value.TagID := SB_ASN1_NULL;
      Value.Content := '';
      if Field.AddField(False) <> 1 then
        Exit;
      Value := TElASN1SimpleTag(TElASN1ConstrainedTag(Field).GetField(1));
      Value.TagID := SB_ASN1_OCTETSTRING;
      Value.Content := Digest;
      // save authenticode data in BER format
      Size := 0;
      Tag.SaveToBuffer(nil, Size);
      if Size = 0 then
        Exit;
      SetLength(FAuthenticode, Size);
      if not Tag.SaveToBuffer(@FAuthenticode[1], Size) then
      begin
        FAuthenticode := '';
        Exit;
      end;
    finally
      Tag.Free;
    end;
    // prepare authenticode data digests
    if Ord(FAuthenticode[1]) <> $30 then
    begin
      SetLength(FAuthenticode, 0);
      Exit;
    end;
    case Ord(FAuthenticode[2]) of
      $81: Digest := Copy(FAuthenticode, 4, MaxInt);
      $82: Digest := Copy(FAuthenticode, 5, MaxInt);
      $83: Digest := Copy(FAuthenticode, 6, MaxInt);
    else
      if Ord(FAuthenticode[2]) < $81 then
        Digest := Copy(FAuthenticode, 3, MaxInt)
      else
      begin
        SetLength(FAuthenticode, 0);
        Exit;
      end;
    end;
    MD5 := HashMD5(Digest);
    SetLength(FAuthenticodeMD5, SizeOf(MD5));
    Move(MD5, FAuthenticodeMD5[1], SizeOf(MD5));
    SHA1 := HashSHA1(Digest);
    SetLength(FAuthenticodeSHA1, SizeOf(SHA1));
    Move(SHA1, FAuthenticodeSHA1[1], SizeOf(SHA1));

    Result := True;
  except
    FAuthenticode := '';
    // ignore any errors; just return False
  end;
end;

function TElAuthenticodeSigner.AddCertificate(Certificate: TElX509Certificate): Boolean;
var
  Digest: TMessageDigest160;
begin
  Result := False;
  Digest := Certificate.GetHashSHA1();
  if FCertificates.FindByHash(Digest) < 0 then
  begin
    FCertificates.Add(Certificate, False);
    Result := True;
  end;
end;

function TElAuthenticodeSigner.AddSignature(Description: WideString;
  URL: string; Algorithm: TSBAuthenticodeDigestAlgorithm;
  Signer: TElX509Certificate; AddSigner: Boolean; AddTimestamp: boolean = false): Integer;
var
  Signature: TElAuthenticodeSignature;
  S: string;
  Size: Integer;
  MD5: TMessageDigest128;
  SHA1: TMessageDigest160;
  Tag, Field: TElASN1ConstrainedTag;
  Param: TElASN1SimpleTag;
  St : TDateTime;
begin
  Result := SB_AUTHENTICODE_ERROR_UNKNOWN;
  // check arguments
  if FAuthenticode = '' then
  begin
    Result := SB_AUTHENTICODE_ERROR_INVALID_DATA;
    Exit;
  end;
  if Signer = nil then
  begin
    Result := SB_AUTHENTICODE_ERROR_INVALID_ARGUMENT;
    Exit;
  end;
  if not Signer.PrivateKeyExists then
  begin
    Result := SB_AUTHENTICODE_ERROR_INVALID_CERTIFICATE;
    Exit;
  end;
  // create signature object
  Signature := TElAuthenticodeSignature.Create;
  try
    // set signature issuer
    Signature.SerialNumber := Signer.SerialNumber;
    Signature.Issuer.Assign(Signer.IssuerRDN);
    // set authenticated attributes
    Signature.Attributes.Count := 3;
    Signature.Attributes.Attributes[0] := SB_OID_CONTENT_TYPE;
    Signature.Attributes.Values[0].Add(FormatAttributeValue(SB_ASN1_OBJECT, SB_OID_SPC_INDIRECT_DATA));
    Signature.Attributes.Attributes[1] := SB_OID_MESSAGE_DIGEST;
    if Algorithm = acMD5 then
    begin
      Signature.DigestAlgorithm := SB_OID_MD5;
      Signature.Attributes.Values[1].Add(FormatAttributeValue(SB_ASN1_OCTETSTRING, FAuthenticodeMD5));
    end
    else
    if Algorithm = acSHA1 then
    begin
      Signature.DigestAlgorithm := SB_OID_SHA1;
      Signature.Attributes.Values[1].Add(FormatAttributeValue(SB_ASN1_OCTETSTRING, FAuthenticodeSHA1));
    end;
    Signature.Attributes.Attributes[2] := SB_OID_SPC_SP_OPUS_INFO;
    Signature.Attributes.Values[2].Add(FormatSignatureInfo(Description, URL));

    if AddTimestamp then
    begin
      Signature.Attributes.Count := 4;
      Signature.Attributes.Attributes[3] := SB_OID_SIGNING_TIME;
      St := Now;
      Signature.Attributes.Values[3].Add(FormatAttributeValue(SB_ASN1_UTCTIME,
        DateTimeToUTCTime(St)));
    end;

    // prepare digest of the authenticated attributes
    Size := 0;
    Signature.Attributes.SaveToBuffer(nil, Size);
    if Size = 0 then
      Exit;
    SetLength(S, Size);
    if not Signature.Attributes.SaveToBuffer(@S[1], Size) then
      Exit;
    SetLength(S, Size);
    if Algorithm = acMD5 then
    begin
      MD5 := HashMD5(S);
      SetLength(S, SizeOf(MD5));
      Move(MD5, S[1], SizeOf(MD5));
    end
    else
    if Algorithm = acSHA1 then
    begin
      SHA1 := HashSHA1(S);
      SetLength(S, SizeOf(SHA1));
      Move(SHA1, S[1], SizeOf(SHA1));
    end;
    // envelope the prepared digest with ASN.1
    Tag := TElASN1ConstrainedTag.Create;
    try
      Tag.TagID := SB_ASN1_SEQUENCE;
      if Tag.AddField(True) <> 0 then
        Exit;
      Field := TElASN1ConstrainedTag(Tag.GetField(0));
      Field.TagID := SB_ASN1_SEQUENCE;
      if Field.AddField(False) <> 0 then
        Exit;
      Param := TElASN1SimpleTag(Field.GetField(0));
      Param.TagID := SB_ASN1_OBJECT;
      Param.Content := Signature.DigestAlgorithm;
      if Field.AddField(False) <> 1 then
        Exit;
      Param := TElASN1SimpleTag(Field.GetField(1));
      Param.TagID := SB_ASN1_NULL;
      Param.Content := '';
      if Tag.AddField(False) <> 1 then
        Exit;
      Param := TElASN1SimpleTag(Tag.GetField(1));
      Param.TagID := SB_ASN1_OCTETSTRING;
      Param.Content := S;
      Size := 0;
      Tag.SaveToBuffer(nil, Size);
      if Size = 0 then
        Exit;
      SetLength(S, Size);
      if not Tag.SaveToBuffer(@S[1], Size) then
        Exit;
      SetLength(S, Size);
    finally
      Tag.Free;
    end;
    if S = '' then
      Exit;
    // encrypt enveloped authenticated attributes using the given certificate
    if Signer.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    begin
      Signature.EncryptionAlgorithm := SB_OID_RSAENCRYPTION;
      Signature.EncryptedDigest := EncryptRSA(S, Signer);
      if Signature.EncryptedDigest = '' then
        Exit;
    end
    else
    if Signer.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
    begin
      Signature.EncryptionAlgorithm := SB_OID_DSA;
      Signature.EncryptedDigest := SignDSA(S, Signer);
      if Signature.EncryptedDigest = '' then
        Exit;
    end
    else
    begin
      Result := SB_AUTHENTICODE_ERROR_INVALID_ALGORITHM;
      Exit;
    end;
    // add created signature object to the internal list
    FSignatures.Add(Signature);
    // add the given certificate to the internal storage
    if AddSigner then
      AddCertificate(Signer);
    Result := SB_AUTHENTICODE_ERROR_SUCCESS;
  finally
    if Result <> SB_AUTHENTICODE_ERROR_SUCCESS then
    begin
      FSignatures.Remove(Signature);
      Signature.Free;
    end;
  end;
end;

procedure TElAuthenticodeSigner.Clear;
begin
  FCertificates.Clear;
  FSignatures.Clear;
  FreeAndNil(FStream);
  FillChar(FChecksum, SizeOf(FChecksum), 0);
  FillChar(FDigestMD5, SizeOf(FDigestMD5), 0);
  FillChar(FDigestSHA1, SizeOf(FDigestSHA1), 0);
  FAuthenticode := '';
  FAuthenticodeMD5 := '';
  FAuthenticodeSHA1 := '';
  FHeaderOffset := 0;
  FPadding := '';
end;

function TElAuthenticodeSigner.CloseFile(Apply: Boolean): Integer;
begin
  if Apply then
  begin
    Result := SignFile();
    if Result <> SB_AUTHENTICODE_ERROR_SUCCESS then
      Exit;
  end;
  Clear;
  Result := SB_AUTHENTICODE_ERROR_SUCCESS;
end;

constructor TElAuthenticodeSigner.Create(AOwner: TComponent);
begin
  inherited;
  FCertificates := TElMemoryCertStorage.Create(nil);
  FillChar(FChecksum, SizeOf(FChecksum), 0);
  FillChar(FDigestMD5, SizeOf(FDigestMD5), 0);
  FillChar(FDigestSHA1, SizeOf(FDigestSHA1), 0);
  FAuthenticode := '';
  FAuthenticodeMD5 := '';
  FAuthenticodeSHA1 := '';
  FHeaderOffset := 0;
  FPadding := '';
  FSignatures := TSBObjectList.Create;
  FStream := nil;
end;

destructor TElAuthenticodeSigner.Destroy;
begin
  CloseFile(False);
  FreeAndNil(FSignatures);
  FreeAndNil(FCertificates);
  inherited;
end;

function TElAuthenticodeSigner.EncryptRSA(const Buffer: string;
  Certificate: TElX509Certificate): string;
var
  Size: Integer;
  M, E: Pointer;
  RM: PByte;
  MSize, ESize: Integer;
begin
  Result := '';
  MSize := 0;
  ESize := 0;
  Certificate.GetRSAParams(nil, MSize, nil, ESize);
  if (MSize = 0) or (ESize = 0) then
    Exit;
  GetMem(M, MSize);
  try
    GetMem(E, ESize);
    try
      if not Certificate.GetRSAParams(M, MSize, E, ESize) then
        Exit;
      RM := M;
      while (MSize > 0) and (RM^ = 0) do
      begin
        Inc(RM);
        Dec(MSize);
      end;
      if MSize = 0 then
        Exit;
    finally
      FreeMem(E);
    end;
    ESize := 0;
    Certificate.SaveKeyValueToBuffer(nil, ESize);
    if ESize = 0 then
      Exit;
    GetMem(E, ESize);
    try
      if not Certificate.SaveKeyValueToBuffer(E, ESize) then
        Exit;
      Size := MSize;
      SetLength(Result, Size);
      if not SBRSA.Sign(@Buffer[1], Length(Buffer), RM, MSize, E, ESize, @Result[1], Size) then
      begin
        SetLength(Result, 0);
        Exit;
      end;
      SetLength(Result, Size);
    finally
      FreeMem(E);
    end;
  finally
    FreeMem(M);
  end;
end;

function TElAuthenticodeSigner.FormatSignatureInfo(Description: WideString;
  URL: string): string;
var
  Tag, Field: TElASN1ConstrainedTag;
  Param: TElASN1SimpleTag;
  S: string;
  Size: Integer;
begin
  Result := '';
  // compose Signature Info data
  Tag := TElASN1ConstrainedTag.Create;
  try
    Tag.TagID := SB_ASN1_SEQUENCE;
    if Tag.AddField(True) <> 0 then
      Exit;
    // add Description
    Field := TElASN1ConstrainedTag(Tag.GetField(0));
    Field.TagID := SB_ASN1_A0;
    if Field.AddField(False) <> 0 then
      Exit;
    Param := TElASN1SimpleTag(Field.GetField(0));
    Param.TagID := SB_ASN1_A0_PRIMITIVE;
    S := '';
    Size := Length(Description) * 2;
    if Size > 0 then
    begin
      SetLength(S, Size);
      Move(Description[1], S[1], Size);
      SwapBigEndianWords(@S[1], Size);
    end;
    Param.Content := S;
    // add URL
    if Tag.AddField(True) <> 1 then
      Exit;
    Field := TElASN1ConstrainedTag(Tag.GetField(1));
    Field.TagID := SB_ASN1_A1;
    if Field.AddField(False) <> 0 then
      Exit;
    Param := TElASN1SimpleTag(Field.GetField(0));
    Param.TagID := SB_ASN1_A0_PRIMITIVE;
    Param.Content := URL;
    // save Signature Info data as string
    Size := 0;
    Tag.SaveToBuffer(nil, Size);
    if Size = 0 then
      Exit;
    SetLength(Result, Size);
    Tag.SaveToBuffer(@Result[1], Size);
    SetLength(Result, Size);
  finally
    Tag.Free;
  end;
end;

function TElAuthenticodeSigner.OpenFile(const FileName: string): Integer;
var
  MD5: TMD5Context;
  SHA1: TSHA1Context;
  MZSign: Word;
  PESign, Checksum: LongWord;
  SignatureOffset, SignatureSize: LongWord;

  function SeekStream(Position: Cardinal; SkipDigests: Boolean = False): Boolean;
  var
    Left, Read: Cardinal;
    Buffer: array [0..4095] of Byte;
  begin
    Result := False;
    Left := Position - cardinal(FStream.Position);
    try
      while Left > 0 do
      begin
        Read := FStream.Read(Buffer, Min(Left, SizeOf(Buffer)));
        if Read = 0 then
          Exit;
        CalcChecksum(FChecksum, @Buffer[0], Read);
        if not SkipDigests then
        begin
          HashMD5(MD5, @Buffer[0], Read);
          HashSHA1(SHA1, @Buffer[0], Read);
        end;
        Dec(Left, Read);
      end;
    except
      Exit;
    end;
    Result := True;
  end;

begin
  CheckLicenseKey();
  if FStream <> nil then
    CloseFile(False);
  try
    FStream := TFileStream.Create(FileName, fmOpenReadWrite or fmShareDenyWrite);
  except
    Result := SB_AUTHENTICODE_ERROR_CANNOT_OPEN_FILE;
    Exit;
  end;
  try
    Result := SB_AUTHENTICODE_ERROR_UNKNOWN;
    try
      InitializeChecksum(FChecksum);
      InitializeMD5(MD5);
      InitializeSHA1(SHA1);

      // the file must be at least 64 bytes in size
      if FStream.Size < 64 then
      begin
        Result := SB_AUTHENTICODE_ERROR_FILE_TOO_SMALL;
        Exit;
      end;
      // check for DOS EXE signature 'MZ'
      if FStream.Read(MZSign, 2) < 2 then
      begin
        Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
        Exit;
      end;
      if MZSign <> $5A4D then
      begin
        Result := SB_AUTHENTICODE_ERROR_INVALID_EXECUTABLE;
        Exit;
      end;
      // add 'MZ' to checksum and hashes
      CalcChecksum(FChecksum, @MZSign, 2);
      HashMD5(MD5, @MZSign, 2);
      HashSHA1(SHA1, @MZSign, 2);
      // get new header offset
      if not SeekStream($0000003C) then
      begin
        Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
        Exit;
      end;
      if FStream.Read(FHeaderOffset, 4) < 4 then
      begin
        Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
        Exit;
      end;
      if FHeaderOffset + $A0 > cardinal(FStream.Size) then
      begin
        Result := SB_AUTHENTICODE_ERROR_INVALID_PE_FORMAT;
        Exit;
      end;
      // add found header offset to checksum and hashes
      CalcChecksum(FChecksum, @FHeaderOffset, 4);
      HashMD5(MD5, @FHeaderOffset, 4);
      HashSHA1(SHA1, @FHeaderOffset, 4);
      // check for PE signature
      if not SeekStream(FHeaderOffset) then
      begin
        Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
        Exit;
      end;
      if FStream.Read(PESign, 4) < 4 then
      begin
        Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
        Exit;
      end;
      if PESign <> $00004550 then
      begin
        Result := SB_AUTHENTICODE_ERROR_INVALID_PE_FORMAT;
        Exit;
      end;
      // add PE signature to checksum and hashes
      CalcChecksum(FChecksum, @PESign, 4);
      HashMD5(MD5, @PESign, 4);
      HashSHA1(SHA1, @PESign, 4);
      // skip checksum
      if not SeekStream(FHeaderOffset + $58) then
      begin
        Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
        Exit;
      end;
      if FStream.Read(Checksum, 4) < 4 then
      begin
        Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
        Exit;
      end;
      // get signature section parameters
      if not SeekStream(FHeaderOffset + $98) then
      begin
        Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
        Exit;
      end;
      if FStream.Read(SignatureOffset, 4) < 4 then
      begin
        Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
        Exit;
      end;
      if FStream.Read(SignatureSize, 4) < 4 then
      begin
        Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
        Exit;
      end;
      // check signature section integrity
      if SignatureOffset = 0 then
        SignatureSize := 0
      else
      if (SignatureSize = 0) or (SignatureOffset + SignatureSize > cardinal(FStream.Size)) then
      begin
        Result := SB_AUTHENTICODE_ERROR_SIGNATURE_CORRUPTED;
        Exit;
      end;
      // check signature section
      if SignatureOffset > 0 then
      begin
        Result := SB_AUTHENTICODE_ERROR_FILE_ALREADY_SIGNED;
        Exit;
      end;
      // calc base checksum and digests for the file
      if not SeekStream(FStream.Size) then
      begin
        Result := SB_AUTHENTICODE_ERROR_CANNOT_READ_FILE;
        Exit;
      end;
      // prepare padding buffer if necessary
      if (FStream.Size and $F) > 0 then
      begin
        SetLength(FPadding, 16 - (FStream.Size and $F));
        FillChar(FPadding[1], Length(FPadding), 0);
        CalcChecksum(FChecksum, @FPadding[1], Length(FPadding));
        HashMD5(MD5, @FPadding[1], Length(FPadding));
        HashSHA1(SHA1, @FPadding[1], Length(FPadding));
      end;
      // complete digests for the file
      FDigestMD5 := FinalizeMD5(MD5);
      FDigestSHA1 := FinalizeSHA1(SHA1);
      
      Result := SB_AUTHENTICODE_ERROR_SUCCESS;
    finally
      if Result <> SB_AUTHENTICODE_ERROR_SUCCESS then
        CloseFile(False);
    end;
  except
    Result := SB_AUTHENTICODE_ERROR_UNKNOWN;
  end;
end;

function TElAuthenticodeSigner.SignDSA(const Buffer: string;
  Certificate: TElX509Certificate): string;
var
  P, Q, G, Y, X, R, S, Blob: Pointer;
  PSize, QSize, GSize, YSize, XSize, RSize, SSize, BlobSize: Integer;
begin
  Result := '';
  PSize := 0;
  QSize := 0;
  GSize := 0;
  YSize := 0;
  Certificate.GetDSSParams(nil, PSize, nil, QSize, nil, GSize, nil, YSize);
  if (PSize = 0) or (QSize = 0) or (GSize = 0) or (YSize = 0) then
    Exit;
  GetMem(P, PSize);
  GetMem(Q, QSize);
  GetMem(G, GSize);
  GetMem(Y, YSize);
  try
    if not Certificate.GetDSSParams(P, PSize, Q, QSize, G, GSize, Y, YSize) then
      Exit;
    XSize := 0;
    Certificate.SaveKeyValueToBuffer(nil, XSize);
    if XSize = 0 then
      Exit;
    GetMem(X, XSize);
    try
      if not Certificate.SaveKeyValueToBuffer(X, XSize) then
        Exit;
      RSize := 21;
      SSize := 21;
      GetMem(R, RSize);
      GetMem(S, SSize);
      try
        if not SBDSA.Sign(@Buffer[1], Length(Buffer), P, PSize, Q, QSize, G, GSize, X, XSize,
          R, RSize, S, SSize) then
          Exit;
        BlobSize := 0;
        SBDSA.EncodeSignature(R, RSize, S, SSize, nil, BlobSize);
        if BlobSize = 0 then
          Exit;
        GetMem(Blob, BlobSize);
        try
          if not SBDSA.EncodeSignature(R, RSize, S, SSize, Blob, BlobSize) then
            Exit;
          SetLength(Result, BlobSize);
          Move(Blob^, Result[1], BlobSize);
        finally
          FreeMem(Blob);
        end;
      finally
        FreeMem(S);
        FreeMem(R);
      end;
    finally
      FreeMem(X);
    end;
  finally
    FreeMem(Y);
    FreeMem(G);
    FreeMem(Q);
    FreeMem(P);
  end;
end;

function TElAuthenticodeSigner.SignFile: Integer;
var
  PKCS: TElPKCS7Message;
  I, Index: Integer;
  Signature: TElAuthenticodeSignature;
  Signer: TElPKCS7Signer;
  S: string;
  SignatureOffset, SignatureSize, Dummy: LongWord;
begin
  // check environment
  if Length(FAuthenticode) < 2 then
  begin
    Result := SB_AUTHENTICODE_ERROR_INVALID_DATA;
    Exit;
  end;
  if FSignatures.Count = 0 then
  begin
    Result := SB_AUTHENTICODE_ERROR_NO_SIGNATURES;
    Exit;
  end;
  // compose PKCS 7 message
  PKCS := TElPKCS7Message.Create;
  try
    // prepare signed data structure
    PKCS.UseImplicitContent := True;
    PKCS.UseUndefSize := False;
    PKCS.ContentType := ctSignedData;
    PKCS.SignedData.Version := 1;
    PKCS.SignedData.ContentType := SB_OID_SPC_INDIRECT_DATA;
    PKCS.SignedData.Content := FAuthenticode;
    // add certificates
    FCertificates.ExportTo(PKCS.SignedData.Certificates);
    // add signatures
    for I := 0 to FSignatures.Count - 1 do
    begin
      // prepare signature
      Signature := TElAuthenticodeSignature(FSignatures[I]);
      Index := PKCS.SignedData.AddSigner();
      Signer := PKCS.SignedData.Signers[Index];
      Signer.Version := 1;
      Signer.Issuer.Issuer.Assign(Signature.Issuer);
      Signer.Issuer.SerialNumber := Signature.SerialNumber;
      Signer.DigestAlgorithm := Signature.DigestAlgorithm;
      Signer.DigestAlgorithmParams := '';
      Signer.DigestEncryptionAlgorithm := Signature.EncryptionAlgorithm;
      Signer.DigestEncryptionAlgorithmParams := '';
      Signer.EncryptedDigest := Signature.EncryptedDigest;
      Signature.Attributes.Copy(Signer.AuthenticatedAttributes);
    end;
    // save PKCS 7 envelope
    Result := SB_AUTHENTICODE_ERROR_UNKNOWN;
    I := 0;
    PKCS.SaveToBuffer(nil, I);
    if I = 0 then
      Exit;
    SetLength(S, I);
    if not PKCS.SaveToBuffer(@S[1], I) then
      Exit;
    SetLength(S, I);
    // pad the enveloped data to 16 bytes
    I := 8 - ((Length(S) + 8) mod 8);
    if I < 8 then
      S := S + StringOfChar(#0, I);
  finally
    PKCS.Free;
  end;
  // write composed signature to the file
  FStream.Position := FStream.Size;
  if FPadding <> '' then
    FStream.WriteBuffer(FPadding[1], Length(FPadding));
  SignatureOffset := FStream.Position;
  SignatureSize := Length(S) + 8;
  FStream.WriteBuffer(SignatureSize, SizeOf(SignatureSize));
  CalcChecksum(FChecksum, @SignatureSize, SizeOf(SignatureSize));
  Dummy := $00020200;
  FStream.WriteBuffer(Dummy, SizeOf(Dummy));
  CalcChecksum(FChecksum, @Dummy, SizeOf(Dummy));
  FStream.WriteBuffer(S[1], Length(S));
  CalcChecksum(FChecksum, @S[1], Length(S));
  FStream.Position := FHeaderOffset + $98;
  FStream.WriteBuffer(SignatureOffset, SizeOf(SignatureOffset));
  CalcChecksum(FChecksum, @SignatureOffset, SizeOf(SignatureOffset));
  FStream.WriteBuffer(SignatureSize, SizeOf(SignatureSize));
  CalcChecksum(FChecksum, @SignatureSize, SizeOf(SignatureSize));
  FStream.Position := FHeaderOffset + $58;
  Dummy := FinalizeChecksum(FChecksum, FStream.Size);
  FStream.WriteBuffer(Dummy, SizeOf(Dummy));
  Result := SB_AUTHENTICODE_ERROR_SUCCESS;
end;


end.
