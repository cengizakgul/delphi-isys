(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSMIMESignatures;

interface

uses
  SBMessages,
  Classes,
  SBUtils;


const
  SB_MESSAGE_ERROR_INVALID_MESSAGE_DIGEST   = Integer($2050);
  SB_MESSAGE_WARNING_OMITTED_MESSAGE_DIGEST = Integer($2051);

type
  TElSMIMEMessageSigner = class(TElMessageSigner)
  private
    FSigningTime: TDateTime;
  public
    function Sign(InBuffer: Pointer; InSize: Integer; OutBuffer: Pointer;
      var OutSize: Integer; Detached: Boolean = False): Integer;  override;
    property SigningTime: TDateTime read FSigningTime write FSigningTime;
  end;

  TElSMIMEMessageVerifier = class(TElMessageVerifier)
  private
    FSigningTime: TDateTime;
    function ProcessTime(const Time: BufferType): TDateTime;
  public
    function Verify(InBuffer: Pointer; InSize: Integer; OutBuffer: Pointer;
      var OutSize: Integer): Integer; override;
    function VerifyDetached(Buffer: Pointer; Size: Integer; Signature: Pointer;
      SignatureSize: Integer): Integer;  override;
    property SigningTime: TDateTime read FSigningTime;
  end;

implementation

uses
  SysUtils,
  SBConstants,
  //SBSHA,
  SBMD,
  SBASN1Tree,
  SBHashFunction;

const

//OID_SHA1               :TBufferTypeConst = #$2b#$0E#$03#$02#$1A;
//OID_RSAENCRYPTION      :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$01;
//OID_PKCS7_DATA         :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$07#$01;
  OID_DES_EDE3_CBC       :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$03#$07;
  OID_RC2_CBC            :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$03#$02;
  OID_CONTENT_TYPE       :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$09#$03;
  OID_SIGNING_TIME       :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$09#$05;
  OID_MESSAGE_DIGEST     :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$09#$04;
  OID_SMIME_CAPABILITIES :TBufferTypeConst = #$2a#$86#$48#$86#$f7#$0d#$01#$09#$0f;
  OID_DES_CBC            :TBufferTypeConst = #$2b#$0e#$03#$02#$07;
//OID_SHA1_RSAENCRYPTION :TBufferTypeConst = #$2A#$86#$48#$86#$F7#$0D#$01#$01#$05;

{ TElSMIMEMessageSigner }

(*
{$IFDEF DELPHI_NET}
function TElSMIMEMessageSigner.Sign(const InBuffer: ByteArray; var OutBuffer: ByteArray;
  var OutSize: Integer; Detached: Boolean = false): Integer;
{$ELSE}
function TElSMIMEMessageSigner.Sign(InBuffer: Pointer; InSize: Integer; OutBuffer: Pointer;
  var OutSize: Integer; Detached: Boolean = false): Integer;
{$ENDIF}
//*)
function TElSMIMEMessageSigner.Sign;
var
  Signer: TElMessageSigner;
  HashRes, Digest, SigTime: BufferType;
  Tag, CTag: TElASN1ConstrainedTag;
  STag: TElASN1SimpleTag;
  Sz: Integer;
  Capabs: AnsiString;
  HFunc : TElHashFunction;
begin
  Signer := TElMessageSigner.Create(nil);
  Signer.HashAlgorithm := HashAlgorithm;
  Signer.CertStorage := CertStorage;

  HFunc := TElHashFunction.Create(HashAlgorithm);
  HFunc.Update(InBuffer, InSize);
  HashRes := HFunc.Finish;
  HFunc.Free;

  if Length(HashRes) > 0 then
  begin
    SetLength(Digest, Length(HashRes) + 2);

    Digest[1] := #$04;
    Digest[2] := Chr(Length(HashRes));

    Move(HashRes[1], Digest[3], Length(HashRes));
  end
  else
  begin
    Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
    Signer.Free;
    Exit;
  end;

  Signer.AuthenticatedAttributes.Count := 4;
  with Signer.AuthenticatedAttributes do
  begin
    Attributes[0] := TBufferTypeConst(OID_CONTENT_TYPE){$ifdef DELPHI)_NET}.Data{$endif};
    Values[0].Add(Chr(SB_ASN1_OBJECT) +
      Chr(Length(SB_OID_PKCS7_DATA)) + SB_OID_PKCS7_DATA);
    Attributes[1] := OID_SIGNING_TIME;
    if (FSigningTime >=
      EncodeDate(2050, 1, 1))
      or
      (FSigningTime <
      EncodeDate(1950, 1, 1))
    then
    begin
      SigTime := DateTimeToGeneralizedTime(FSigningTime);
      SigTime := FormatAttributeValue(SB_ASN1_GENERALIZEDTIME, SigTime);
      //SigTime := TBufferTypeConst(Chr(SB_ASN1_GENERALIZEDTIME)) + Chr(Length(SigTime)) + SigTime;
    end
    else
    begin
      SigTime := DateTimeToUTCTime(FSigningTime);
      SigTime := FormatAttributeValue(SB_ASN1_UTCTIME, SigTime);
      //SigTime := TBufferTypeConst(Chr(SB_ASN1_UTCTIME)) + Chr(Length(SigTime)) + SigTime;
    end;
    Values[1].Add(SigTime);
    Attributes[2] := OID_MESSAGE_DIGEST;
    Values[2].Add(Digest);
    Attributes[3] := OID_SMIME_CAPABILITIES;
    Tag := TElASN1ConstrainedTag.Create;
    Tag.TagId := SB_ASN1_SEQUENCE;
    { 3DES }
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    CTag.TagId := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagID := SB_ASN1_OBJECT;
    STag.Content := OID_DES_EDE3_CBC;
    { RC2128 }
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    CTag.TagId := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagID := SB_ASN1_OBJECT;
    STag.Content := OID_RC2_CBC;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagID := SB_ASN1_Integer;
    STag.Content := TBufferTypeConst(#$0#$80);
    { AES128 }
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    CTag.TagId := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagID := SB_ASN1_OBJECT;
    STag.Content := SB_OID_AES128_CBC;
    { AES192 }
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    CTag.TagId := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagID := SB_ASN1_OBJECT;
    STag.Content := SB_OID_AES192_CBC;
    { AES256 }
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    CTag.TagId := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagID := SB_ASN1_OBJECT;
    STag.Content := SB_OID_AES256_CBC;
    { RC264 }
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    CTag.TagId := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagID := SB_ASN1_OBJECT;
    STag.Content := OID_RC2_CBC;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagID := SB_ASN1_Integer;
    STag.Content := TBufferTypeConst(#$40);
    { DES }
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    CTag.TagId := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagID := SB_ASN1_OBJECT;
    STag.Content := OID_DES_CBC;
    { SHA1WithRSA }
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    CTag.TagId := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagID := SB_ASN1_OBJECT;
    STag.Content := SB_OID_SHA1_RSA;
    Sz := 0;
    Tag.SaveToBuffer(nil, Sz);
    SetLength(Capabs, Sz);
    Tag.SaveToBuffer(@Capabs[1], Sz);
    SetLength(Capabs, Sz);
    Values[3].Add(Capabs);
    Tag.Free;
  end;
  
  Result := Signer.Sign(InBuffer,InSize, OutBuffer, OutSize, Detached);
  Signer.Free;
end;

{ TElSMIMEMessageVerifier }

(*
{$IFDEF DELPHI_NET}
function TElSMIMEMessageVerifier.Verify(const InBuffer: ByteArray; var OutBuffer: ByteArray;
  var OutSize: Integer): Integer;
{$ELSE}
function TElSMIMEMessageVerifier.Verify(InBuffer: Pointer; InSize: Integer; OutBuffer: Pointer;
  var OutSize: Integer): Integer;
{$ENDIF}
//*)
function TElSMIMEMessageVerifier.Verify;
var
  Buf: ByteArray;
  Tag: TElASN1ConstrainedTag;
  I, Sz, Written: Integer;
  Ptr: PByte;
  Digest, HashRes: BufferType;
  HFunc : TElHashFunction;
begin
  Sz := 0;
  inherited Verify(InBuffer, InSize, nil, Sz);
  if Sz > OutSize then
  begin
    OutSize := Sz;
    Result := SB_MESSAGE_ERROR_BUFFER_TOO_SMALL;
    Exit;
  end;
  SetLength(Buf, Sz);
  Result := inherited Verify(InBuffer, InSize, @Buf[0], Sz);
  if Result <> 0 then
    Exit;
  Tag := TElASN1ConstrainedTag.Create;
  if Tag.LoadFromBuffer(@Buf[0], Sz) then
  begin
    if (Tag.GetField(0).IsConstrained) then
    begin
      Ptr := OutBuffer;
      Sz := OutSize;
      Written := 0;
      for I := 0 to TElASN1ConstrainedTag(Tag.GetField(0)).Count - 1 do
      begin //???: ������ ������ � ����: �� ����� �� ������ �� �������� ???
        Sz := OutSize - Written;
        TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I).WriteHeader := false;
        TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I).SaveToBuffer(Ptr, Sz);
        Inc(Written, Sz);
        Inc(Ptr, Sz);
      end;
      OutSize := Written;
    end
    else
    begin
      Move(Buf[0], OutBuffer^, Sz);
      OutSize := Sz;
    end;
  end
  else
  begin
    Move(Buf[0], OutBuffer^, Sz);
    OutSize := Sz;
  end;
  Tag.Free;

  if Attributes.Count > 0 then
  begin
    for I := 0 to Attributes.Count - 1 do
    begin
      if CompareContent(Attributes.Attributes[I], OID_MESSAGE_DIGEST) then
        Digest := Attributes.Values[I].Strings[0]
      else
      if CompareContent(Attributes.Attributes[I], OID_SIGNING_TIME) then
        if Attributes.Values[I].Count > 0 then
        begin
          FSigningTime := ProcessTime(Attributes.Values[I].Strings[0])
        end
        else
        begin
          FSigningTime := 0;
        end;
    end;
  end;

  if (Attributes.Count = 0) or (Length(Digest) = 0) then
  begin
    Result := SB_MESSAGE_WARNING_OMITTED_MESSAGE_DIGEST;
  end
  else
  begin
    HFunc := TElHashFunction.Create(HashAlgorithm);
    HFunc.Update(OutBuffer, OutSize);
    HashRes := HFunc.Finish;
    HFunc.Free;

    if Length(HashRes) > 0 then
    begin
      Result := SB_MESSAGE_ERROR_INVALID_MESSAGE_DIGEST;

      if (Length(Digest) = Length(HashRes) + 2) and
         (Digest[1] = #$04) and (Digest[2] = Chr(Length(HashRes)))
      then
      begin
        if CompareMem(@Digest[3], @HashRes[1], Length(HashRes))
        then
          Result := 0;
      end;
    end
    else
      Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
  end;
end;

(*
{$IFDEF DELPHI_NET}
function TElSMIMEMessageVerifier.VerifyDetached(const Buffer: ByteArray;
  const Signature: ByteArray): Integer;
{$ELSE}
function TElSMIMEMessageVerifier.VerifyDetached(Buffer: Pointer; Size: Integer; Signature: Pointer;
  SignatureSize: Integer): Integer;
{$ENDIF}
//*)
function TElSMIMEMessageVerifier.VerifyDetached;
var
  OutBuf: ByteArray;
  OutSize: Integer;
  I: Integer;
  Digest, HashRes: BufferType;
  HFunc : TElHashFunction;
begin
  OutSize := SignatureSize;
  SetLength(OutBuf, OutSize);
  Result := inherited Verify(Signature, SignatureSize, @OutBuf[0], OutSize);
  if Result <> 0 then
    Exit;
  for I := 0 to Attributes.Count - 1 do
  begin
    if CompareContent(Attributes.Attributes[I], OID_MESSAGE_DIGEST) then
      Digest := Attributes.Values[I].Strings[0]
    else
    if CompareContent(Attributes.Attributes[I], OID_SIGNING_TIME) then
      if Attributes.Values[I].Count > 0 then
      begin
        FSigningTime := ProcessTime(Attributes.Values[I].Strings[0])
      end
      else
      begin
        FSigningTime := 0;
      end;
  end;

  HFunc := TElHashFunction.Create(HashAlgorithm);
  HFunc.Update(Buffer, Size);
  HashRes := HFunc.Finish;
  HFunc.Free;

  if Length(HashRes) > 0 then
  begin
    Result := SB_MESSAGE_ERROR_INVALID_MESSAGE_DIGEST;

    if (Length(Digest) = Length(HashRes) + 2) and
       (Digest[1] = #$04) and (Digest[2] = Chr(Length(HashRes)))
    then
    begin
      if CompareMem(@Digest[3], @HashRes[1], Length(HashRes))
      then
        Result := 0;
    end;
  end
  else
    Result := SB_MESSAGE_ERROR_UNSUPPORTED_ALGORITHM;
end;

function TElSMIMEMessageVerifier.ProcessTime(const Time: BufferType): TDateTime;
var
  Tag: Byte;
  Content: BufferType;
begin
  if Length(Time)=0 then
  begin
    Result := 0;
    exit;
  end;
  Tag := PByte(@Time[1])^; // ->:   Tag := Byte(Time[1]); ???
  Content := Copy(Time, 3, Length(Time));
  if Tag = SB_ASN1_UTCTIME then
    Result := UTCTimeToDateTime((Content))
  else if Tag = SB_ASN1_GENERALIZEDTIME then
    Result := GeneralizedTimeToDateTime((Content))
  else
    Result := 0;
end;

end.
