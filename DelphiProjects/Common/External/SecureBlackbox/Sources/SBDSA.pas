
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

unit SBDSA;
{$I SecBbox.inc}

interface

uses
  SBMath,
  SBUtils;


function ValidateSignature(Hash : pointer; HashSize : integer; P : pointer;
  PSize : integer; Q : pointer; QSize : integer; G : pointer; GSize : integer;
  Y : pointer; YSize : integer; R : pointer; RSize : integer; S : pointer;
  SSize : integer) : boolean;

function Generate(Bits : integer; P : pointer; var PSize : integer; Q : pointer;
  var QSize : integer; G : pointer; var GSize : integer; Y : pointer;
  var YSize : integer; X : pointer; var XSize : integer) : boolean; overload; 

function Generate(Bits : integer; P : pointer; var PSize : integer; Q : pointer;
  var QSize : integer; G : pointer; var GSize : integer; Y : pointer;
  var YSize : integer; X : pointer; var XSize : integer; PrivateKeyBlob : pointer;
  var PrivateKeyBlobSize : integer; ProgressFunc : TSBMathProgressFunc = nil;
  Data : pointer = nil) : boolean; overload; 

function Sign(Hash : pointer; HashSize : integer; P : pointer; PSize : integer;
  Q : pointer; QSize : integer; G : pointer; GSize : integer; X : pointer;
  XSize : integer; R : pointer; var RSize : integer; S : pointer;
  var SSize : integer) : boolean;

function DecodePrivateKey(Buffer : pointer; Size : integer; P : pointer;
  var PSize : integer; Q : pointer; var QSize : integer; G : pointer;
  var GSize : integer; Y : pointer; var YSize : integer; X : pointer;
  var XSize : integer) : boolean;

function EncodePrivateKey(P : pointer; PSize : integer; Q : pointer; QSize :
  integer; G : pointer; GSize : integer; Y : pointer; YSize : integer;
  X : pointer; XSize : integer; OutBuffer : pointer; var OutSize : integer) : boolean;

function EncodeSignature(R : pointer; RSize : integer; S : pointer; SSize :
  integer; Blob: pointer; var BlobSize : integer) : boolean;

function DecodeSignature(Blob : pointer; Size : integer; R : pointer;
  var RSize : integer; S : pointer; var SSize : integer) : boolean;

function IsValidKey(P : pointer; PSize : integer; Q : pointer; QSize : integer;
  G : pointer; GSize : integer; Y : pointer; YSize : integer;
  X : pointer; XSize : integer; Secret : boolean;
  StrictMode : boolean = false) : boolean;


procedure GenerateDSAQ(var Q: PLInt; var Seed: ByteArray); 
procedure GenerateDSAP(var P: PLInt; Q: PLInt; var RandCtx : TRC4RandomContext; var B: PLint; Bits: integer); 

implementation

uses
  SBASN1, SBASN1Tree, SBSHA, SysUtils;

function ValidateSignature(Hash : pointer; HashSize : integer; P : pointer;
  PSize : integer; Q : pointer; QSize : integer; G : pointer; GSize : integer;
  Y : pointer; YSize : integer; R : pointer; RSize : integer; S : pointer;
  SSize : integer) : boolean;
var
  LM, LP, LQ, LG, LY, LR, LS, LTmp, LW : PLInt;
begin
  LCreate(LM);
  LCreate(LP);
  LCreate(LQ);
  LCreate(LG);
  LCreate(LY);
  LCreate(LR);
  LCreate(LS);
  LCreate(LTmp);
  LCreate(LW);
  PointerToLInt(LM, Hash, HashSize);
  PointerToLInt(LP, P, PSize);
  PointerToLInt(LQ, Q, QSize);
  PointerToLInt(LG, G, GSize);
  PointerToLInt(LY, Y, YSize);
  PointerToLInt(LR, R, RSize);
  PointerToLInt(LS, S, SSize);
  LGCD(LS, LQ, LTmp, LW);
  LMult(LM, LW, LTmp);
  LMod(LTmp, LQ, LS);   // now LS contains U1
  LMult(LR, LW, LTmp);
  LMod(LTmp, LQ, LM);   // now LM contains U2
  LMModPower(LG, LS, LP, LTmp);
  LMModPower(LY, LM, LP, LG);   // now LG contains the result
  LMult(LG, LTmp, LS);
  LMod(LS, LP, LTmp);
  LMod(LTmp, LQ, LS);
  Result := LEqual(LS, LR);
  LDestroy(LM);
  LDestroy(LP);
  LDestroy(LQ);
  LDestroy(LG);
  LDestroy(LY);
  LDestroy(LR);
  LDestroy(LS);
  LDestroy(LTmp);
  LDestroy(LW);
end;

procedure GenerateDSAQ(var Q: PLInt; var Seed: ByteArray);
var
  FSeed: array[0..24] of byte;
  U: array[0..19] of byte;
  I: Word;
  M1601, M1602: TMessageDigest160;
begin
  SBRndSeedTime;
  for I := 0 to 24 do
    FSeed[I] := SBRndGenerate(256);
  M1601 := HashSHA1(@FSeed[0], 25);
  FSeed[24] := Seed[24] + 1;
  M1602 := HashSHA1(@FSeed[0], 25);

  for I := 0 to 19 do
    U[I] := PByteArray(@M1601)[I] xor PByteArray(@M1602)[I];

  U[0] := U[0] or $80;
  U[19] := U[19] or $01;

  PointerToLInt(Q, @U[0], 20);
  Move(FSeed[0], Seed[0], 25);
end;

procedure GenerateDSAP(var P: PLInt; Q: PLInt; var RandCtx:
  TRC4RandomContext; var B: PLint; Bits: integer);
var
  Tmp, One: PLInt;
begin
  LCreate(Tmp);
  LCreate(One);
  LRandom(RandCtx, B, (Bits - 160) div 8);
  B.Digits[1] := B.Digits[1] and $FFFFFFFE;
  LMult(Q, B, Tmp);
  LAdd(Tmp, One, P);
  LDestroy(Tmp);
  LDestroy(One);
end;

function Generate(Bits : integer; P : pointer; var PSize : integer; Q : pointer;
  var QSize : integer; G : pointer; var GSize : integer; Y : pointer;
  var YSize : integer; X : pointer; var XSize : integer) : boolean;
var
  LP, LQ, LG, LX, LY, H, Tmp, Cmp: PLInt;
  Seed: ByteArray;
  RandCtx: TRC4RandomContext;
begin
  SetLength(Seed, 25);

  if (PSize < (Bits shr 3)) or (YSize < (Bits shr 3)) or (GSize < (Bits shr 3)) or
    (XSize < (Bits shr 3)) or (QSize < 20) then
  begin
    PSize := Bits shr 3;
    YSize := PSize;
    GSize := PSize;
    XSize := PSize;
    QSize := 20;
    Result := false;
    Exit;
  end;
  LCreate(LQ);
  LCreate(LP);
  LCreate(LG);
  LCreate(LX);
  LCreate(LY);
  LCreate(H);
  LCreate(Tmp);
  LCreate(Cmp);
  LShlNum(Tmp, Cmp, Bits - 1);
  repeat
    GenerateDSAQ(LQ, Seed);
  until LIsPrimeEx(LQ);
  LRC4Init(RandCtx);
  repeat
    GenerateDSAP(LP, LQ, RandCtx, Tmp, Bits);
  until (LGreater(LP, Cmp)) and (LIsPrimeEx(LP));
  LRandom(RandCtx, H, LP^.length * 4 - integer(SBRndGenerate(LP^.length * 2)) - 1);
  LMModPower(H, Tmp, LP, LG);
  LRandom(RandCtx, LX, 18);
  LMModPower(LG, LX, LP, LY);
  LIntToPointer(LP, P, PSize);
  LIntToPointer(LG, G, GSize);
  LIntToPointer(LQ, Q, QSize);
  LIntToPointer(LY, Y, YSize);
  LIntToPointer(LX, X, XSize);

  Result := true;
  LDestroy(Tmp);
  LDestroy(Cmp);
  LDestroy(H);
  LDestroy(LP);
  LDestroy(LQ);
  LDestroy(LG);
  LDestroy(LX);
  LDestroy(LY);
end;

function Generate(Bits : integer; P : pointer; var PSize : integer; Q : pointer;
  var QSize : integer; G : pointer; var GSize : integer; Y : pointer;
  var YSize : integer; X : pointer; var XSize : integer; PrivateKeyBlob : pointer;
  var PrivateKeyBlobSize : integer; ProgressFunc : TSBMathProgressFunc = nil;
  Data : pointer = nil) : boolean;
var
  LP, LQ, LG, LX, LY, H, Tmp, Cmp: PLInt;
  Seed: ByteArray;
  RandCtx: TRC4RandomContext;
  EstimatedBlobSize : integer;
  Sz : integer;
  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  TmpS : AnsiString;
begin
  SetLength(Seed, 25);

  Sz := Bits shr 3;
  EstimatedBlobSize := Sz * 4 + 21 + 64;
  if (PSize < Sz) or (YSize < Sz) or (GSize < Sz) or
    (XSize < Sz) or (QSize < 20) or (PrivateKeyBlobSize < EstimatedBlobSize) then
  begin
    PSize := Bits shr 3;
    YSize := PSize;
    GSize := PSize;
    XSize := PSize;
    QSize := 20;
    PrivateKeyBlobSize := EstimatedBlobSize;
    Result := false;
    Exit;
  end;
  Result := false;
  LCreate(LQ);
  LCreate(LP);
  LCreate(LG);
  LCreate(LX);
  LCreate(LY);
  LCreate(H);
  LCreate(Tmp);
  LCreate(Cmp);
  try
    LShlNum(Tmp, Cmp, Bits - 1);
    repeat
      GenerateDSAQ(LQ, Seed);
      if MathOperationCanceled(ProgressFunc, Data) then
        Exit;
    until LIsPrime(LQ);
    LRC4Init(RandCtx);
    repeat
      if MathOperationCanceled(ProgressFunc, Data) then
        Exit;
      GenerateDSAP(LP, LQ, RandCtx, Tmp, Bits);
    until (LGreater(LP, Cmp)) and (LIsPrime(LP));
    LRandom(RandCtx, H, LP^.length * 4 - integer(SBRndGenerate(LP^.length * 2)) - 1);
    try
      LMModPower(H, Tmp, LP, LG, ProgressFunc, Data, true);
      LRandom(RandCtx, LX, 18);
      LMModPower(LG, LX, LP, LY, ProgressFunc, Data, true);
    except
      Exit;
    end;
    LIntToPointer(LP, P, PSize);
    LIntToPointer(LG, G, GSize);
    LIntToPointer(LQ, Q, QSize);
    LIntToPointer(LY, Y, YSize);
    LIntToPointer(LX, X, XSize);
    Tag := TElASN1ConstrainedTag.Create;
    Tag.TagId := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    STag.Content := #0;

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;

    SetLength(TmpS, PSize);
    Move(P^, TmpS[1], Length(TmpS));
    if Ord(TmpS[1]) >= 128 then
      TmpS := #0 + TmpS;
    STag.Content := TmpS;

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    SetLength(TmpS, QSize);
    Move(Q^, TmpS[1], Length(TmpS));
    if Ord(TmpS[1]) >= 128 then
      TmpS := #0 + TmpS;
    STag.Content := TmpS;

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    SetLength(TmpS, GSize);
    Move(G^, TmpS[1], Length(TmpS));
    if Ord(TmpS[1]) >= 128 then
      TmpS := #0 + TmpS;
    STag.Content := TmpS;

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    SetLength(TmpS, YSize);
    Move(Y^, TmpS[1], Length(TmpS));
    if Ord(TmpS[1]) >= 128 then
      TmpS := #0 + TmpS;
    STag.Content := TmpS;

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    SetLength(TmpS, XSize);
    Move(X^, TmpS[1], Length(TmpS));
    if Ord(TmpS[1]) >= 128 then
      TmpS := #0 + TmpS;
    STag.Content := TmpS;

    Result := Tag.SaveToBuffer(PrivateKeyBlob, PrivateKeyBlobSize);
    Tag.Free;
  finally
    LDestroy(Tmp);
    LDestroy(Cmp);
    LDestroy(H);
    LDestroy(LP);
    LDestroy(LQ);
    LDestroy(LG);
    LDestroy(LX);
    LDestroy(LY);
  end;
end;

function Sign(Hash : pointer; HashSize : integer; P : pointer; PSize : integer;
  Q : pointer; QSize : integer; G : pointer; GSize : integer; X : pointer;
  XSize : integer; R : pointer; var RSize : integer; S : pointer;
  var SSize : integer) : boolean;
var
  K, LR, DS, Tmp, ASign, EncSign, LP, LQ, LG, LX : PLInt;
  RandCtx : TRC4RandomContext;
begin
  if (RSize < 20) or (SSize < 20) then
  begin
    RSize := 20;
    SSize := 20;
    Result := false;
    Exit;
  end;
  LCreate(K);
  LCreate(LR);
  LCreate(DS);
  LCreate(Tmp);
  LCreate(ASign);
  LCreate(EncSign);
  LCreate(LP);
  LCreate(LQ);
  LCreate(LG);
  LCreate(LX);

  PointerToLInt(LP, P, PSize);
  PointerToLInt(LG, G, GSize);
  PointerToLInt(LQ, Q, QSize);
  PointerToLInt(LX, X, XSize);

  LRC4Init(RandCtx);
  LRandom(RandCtx, K, 18);
  LMModPower(LG, K, LP, Tmp);
  LMod(Tmp, LQ, LR);
  LMult(LX, LR, Tmp);
  PointerToLInt(ASign, Hash, HashSize);
  LAdd(Tmp, ASign, EncSign);
  LGCD(K, LQ, Tmp, ASign);
  LMult(ASign, EncSign, Tmp);
  LMod(Tmp, LQ, DS);
  RSize := 20;
  SSize := 20;
  LIntToPointer(LR, R, RSize);
  LIntToPointer(DS, S, SSize);
  Result := true;
  LDestroy(EncSign);
  LDestroy(ASign);
  LDestroy(Tmp);
  LDestroy(K);
  LDestroy(LR);
  LDestroy(DS);
  LDestroy(LP);
  LDestroy(LQ);
  LDestroy(LG);
  LDestroy(LX);
end;

function DecodePrivateKey(Buffer : pointer; Size : integer; P : pointer;
  var PSize : integer; Q : pointer; var QSize : integer; G : pointer;
  var GSize : integer; Y : pointer; var YSize : integer; X : pointer;
  var XSize : integer) : boolean;
var
  EncodedKey : TElASN1ConstrainedTag;
  CTag : TElASN1ConstrainedTag;
  I, PInd, QInd, GInd, YInd, XInd : integer;

const
  cArrayStart = 1;
  cFakeValue  = #0;

begin
  Result := false;
  EncodedKey := TElASN1ConstrainedTag.Create;
  try
  
    if not EncodedKey.LoadFromBuffer(Buffer, Size) then
      Exit;
    if (EncodedKey.Count < 1) or (EncodedKey.GetField(0).TagId <> SB_ASN1_SEQUENCE) or
      (not EncodedKey.GetField(0).IsConstrained) then
      Exit;

    CTag := TElASN1ConstrainedTag(EncodedKey.GetField(0));
    if CTag.Count <> 6 then
      Exit;

    for I := 0 to 5 do
      if (CTag.GetField(I).TagId <> SB_ASN1_INTEGER) or
        (CTag.GetField(I).IsConstrained) then
        Exit;

    if TElASN1SimpleTag(CTag.GetField(0)).Content <> #0 then
      Exit;

    if (PSize < Length(TElASN1SimpleTag(CTag.GetField(1)).Content) - 1) or
      (QSize < Length(TElASN1SimpleTag(CTag.GetField(2)).Content) - 1) or
      (GSize < Length(TElASN1SimpleTag(CTag.GetField(3)).Content) - 1) or
      (YSize < Length(TElASN1SimpleTag(CTag.GetField(4)).Content) - 1) or
      (XSize < Length(TElASN1SimpleTag(CTag.GetField(5)).Content) - 1) then
      Result := false
    else
      Result := true;
    PSize := Length(TElASN1SimpleTag(CTag.GetField(1)).Content);
    QSize := Length(TElASN1SimpleTag(CTag.GetField(2)).Content);
    GSize := Length(TElASN1SimpleTag(CTag.GetField(3)).Content);
    YSize := Length(TElASN1SimpleTag(CTag.GetField(4)).Content);
    XSize := Length(TElASN1SimpleTag(CTag.GetField(5)).Content);
    if TElASN1SimpleTag(CTag.GetField(1)).Content[cArrayStart] = cFakeValue then
    begin
      PInd := cArrayStart + 1;
      Dec(PSize);
    end
    else
      PInd := cArrayStart;
    if TElASN1SimpleTag(CTag.GetField(2)).Content[cArrayStart] = cFakeValue then
    begin
      QInd := cArrayStart + 1;
      Dec(QSize);
    end
    else
      QInd := cArrayStart;
    if TElASN1SimpleTag(CTag.GetField(3)).Content[cArrayStart] = cFakeValue then
    begin
      GInd := cArrayStart + 1;
      Dec(GSize);
    end
    else
      GInd := cArrayStart;
    if TElASN1SimpleTag(CTag.GetField(4)).Content[cArrayStart] = cFakeValue then
    begin
      YInd := cArrayStart + 1;
      Dec(YSize);
    end
    else
      YInd := cArrayStart;
    if TElASN1SimpleTag(CTag.GetField(5)).Content[cArrayStart] = cFakeValue then
    begin
      XInd := cArrayStart + 1;
      Dec(XSize);
    end
    else
      XInd := cArrayStart;
    if Result then
    begin
      Move(TElASN1SimpleTag(CTag.GetField(1)).Content[PInd], P^, PSize);
      Move(TElASN1SimpleTag(CTag.GetField(2)).Content[QInd], Q^, QSize);
      Move(TElASN1SimpleTag(CTag.GetField(3)).Content[GInd], G^, GSize);
      Move(TElASN1SimpleTag(CTag.GetField(4)).Content[YInd], Y^, YSize);
      Move(TElASN1SimpleTag(CTag.GetField(5)).Content[XInd], X^, XSize);
    end;
  finally
    EncodedKey.Free;
  end;
end;

function EncodePrivateKey(P : pointer; PSize : integer; Q : pointer; QSize :
  integer; G : pointer; GSize : integer; Y : pointer; YSize : integer;
  X : pointer; XSize : integer; OutBuffer : pointer; var OutSize : integer) : boolean;
var
  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;

begin
  Tag := TElASN1ConstrainedTag.Create;
  Tag.TagId := SB_ASN1_SEQUENCE;
  { Version }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;

  STag.Content := #0;
  try
    { p }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, P, PSize);
    { q }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, Q, QSize);
    { g }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, G, GSize);
    { y }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, Y, YSize);
    { x }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, X, XSize);

    Result := Tag.SaveToBuffer(OutBuffer, OutSize);
  finally
    Tag.Free;
  end;
end;

function DecodeSignature(Blob : pointer; Size : integer; R : pointer;
  var RSize : integer; S : pointer; var SSize : integer) : boolean;
var
  Tag, CTag : TElASN1ConstrainedTag;
  SR, SS : BufferType;
begin
  Result := false;
  if (RSize < 21) or (SSize < 21) then
  begin
    RSize := 21;
    SSize := 21;
    Exit;
  end;
  Tag := TElASN1ConstrainedTag.Create;
  try 
    if Tag.LoadFromBuffer(Blob, Size) then
    begin
      if (not Tag.IsConstrained) or (TElASN1ConstrainedTag(Tag).Count <> 1) then
        Exit;
      
      CTag := TElASN1ConstrainedTag(Tag.GetField(0));
      if (CTag.TagID <> SB_ASN1_SEQUENCE) then
        Exit;
      
      if TElASN1ConstrainedTag(CTag).Count <> 2 then
        Exit;
      
      if (TElASN1ConstrainedTag(CTag).GetField(0).IsConstrained) or
        (TElASN1ConstrainedTag(CTag).GetField(1).IsConstrained) or
        (TElASN1ConstrainedTag(CTag).GetField(0).TagID <> SB_ASN1_INTEGER) or
        (TElASN1ConstrainedTag(CTag).GetField(1).TagID <> SB_ASN1_INTEGER) then
        Exit;
      
      SR := TElASN1SimpleTag(TElASN1ConstrainedTag(CTag).GetField(0)).Content;
      SS := TElASN1SimpleTag(TElASN1ConstrainedTag(CTag).GetField(1)).Content;
      if (Length(SR) > 21) or (Length(SS) > 21) then
        Exit;
      
      Move(SR[1], R^, Length(SR));
      RSize := Length(SR);
      Move(SS[1], S^, Length(SS));
      SSize := Length(SS);
      Result := true;
    end;
  finally
    Tag.Free;
  end;
end;

function EncodeSignature(R : pointer; RSize : integer; S : pointer; SSize :
  integer; Blob: pointer; var BlobSize : integer) : boolean;
var
  EstSize : integer;
  BufR, BufS : BufferType;
  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
begin
  EstSize := RSize + SSize + 16;
  if BlobSize < EstSize then
  begin
    BlobSize := EstSize;
    Result := false;
    Exit;
  end;
  if PByte(R)^ >= $80 then
  begin
    SetLength(BufR, RSize + 1);
    Move(R^, BufR[2], RSize);
    BufR[1] := #0;
  end
  else
  begin
    SetLength(BufR, RSize);
    Move(R^, BufR[1], RSize);
  end;
  if PByte(S)^ >= $80 then
  begin
    SetLength(BufS, SSize + 1);
    Move(S^, BufS[2], SSize);
    BufS[1] := #0;
  end
  else
  begin
    SetLength(BufS, SSize);
    Move(S^, BufS[1], SSize);
  end;
  Tag := TElASN1ConstrainedTag.Create;
  Tag.TagId := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := BufR;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := BufS;
  try
    Result := Tag.SaveToBuffer(Blob, BlobSize);
  finally
    Tag.Free;
  end;
end;


function IsValidKey(P : pointer; PSize : integer; Q : pointer; QSize : integer;
  G : pointer; GSize : integer; Y : pointer; YSize : integer; X : pointer; XSize : integer;
  Secret : boolean; StrictMode : boolean = false) : boolean;
var
  LP, LQ, LG, LY, LX, LTmp1 : PLInt;
  BCount : cardinal;
begin
  Result := false;
  LCreate(LP);
  LCreate(LQ);
  LCreate(LG);
  LCreate(LY);
  LCreate(LX);
  LCreate(LTmp1);

  try
    PointerToLInt(LP, P, PSize);
    PointerToLInt(LQ, Q, QSize);

    { checking that P and Q are primes }

    if StrictMode then
      if (not LIsPrime(LP)) or (not LIsPrime(LQ)) then
        Exit;

    if LBitCount(LQ) <> 160 then
      Exit;

    BCount := LBitCount(LP);
    if (BCount < 512) or ((BCount and 63) <> 0) then
      Exit;

    { checking that Q is a divisor of P-1 }
    LMod(LP, LQ, LTmp1);

    if not ((LTmp1.Length = 1) and (LTmp1.Digits[1] = 1)) then
      Exit;

    PointerToLInt(LG, G, GSize);

    { G must be smaller than P}
    if not LGreater(LP, LG) then
      Exit;

    { checking that G has order Q in Fp }
    if StrictMode then
    begin
      LModPower(LG, LQ, LP, LTmp1);
      if (LTmp1.Length > 1) or (LTmp1.Digits[1] <> 1) then
        Exit;
    end;    

    PointerToLInt(LY, Y, YSize);
    { Y must be smaller than P }
    if not LGreater(LP, LY) then
      Exit;

    { for secret-key only check }  
    if Secret then
    begin
      PointerToLInt(LX, X, XSize);
      { X must be smaller than Q}
      if not LGreater(LQ, LX) then
        Exit;
      { checking that y = g^x (mod p) }

      if StrictMode then
      begin
        LModPower(LG, LX, LP, LTmp1);

        if not LEqual(LTmp1, LY) then
          Exit;
      end;    
    end;

    Result := true;
  finally
    LDestroy(LP);
    LDestroy(LQ);
    LDestroy(LG);
    LDestroy(LY);
    LDestroy(LX);
    LDestroy(LTmp1);
  end;
end;

end.
