
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSharedResource;

interface

uses 
  SBUtils,
{$ifndef CLX_USED}
  Windows;
{$else}
  SysUtils, Libc;
{$endif}


type
  TElSharedResource = class
  private
    {$ifndef CLX_USED}
    FActiveThreadID: Cardinal;
    FActive : integer;
    FWaitingReaders : integer;
    FWaitingWriters : integer;
    FSemReaders : THandle;
    FSemWriters : THandle;
    {$ifndef FPC}
    FCS : _RTL_CRITICAL_SECTION;
    {$else}
    FCS : TRTLCriticalSection;
    {$endif}
    FWriteDepth : integer;
    {$else}
    FAction : integer;
    FSynchronizer : TMultiReadExclusiveWriteSynchronizer;
    {$endif}
    FEnabled : boolean;
  public
    constructor Create;
    destructor Destroy; override;
    procedure WaitToRead;
    procedure WaitToWrite;
    procedure Done;
    property Enabled : boolean read FEnabled write FEnabled default true;
  end;

implementation

{$ifndef CLX_USED}
constructor TElSharedResource.Create;
begin
  FWaitingReaders := 0;
  FWaitingWriters := 0;
  FSemReaders := CreateSemaphore(nil, 0, MAXINT, nil);
  FSemWriters := CreateSemaphore(nil, 0, MAXINT, nil);
  FActiveThreadID := 0;
  FWriteDepth := 0;
  InitializeCriticalSection(FCS);
  FEnabled := true;
end;

destructor TElSharedResource.Destroy;
begin
  FWaitingReaders := 0;
  FWaitingWriters := 0;
  FActive := 0;
  DeleteCriticalSection(FCS);
  CloseHandle(FSemReaders);
  CloseHandle(FSemWriters);
  inherited;
end;

procedure TElSharedResource.WaitToRead;
var
  ResourceWritePending : boolean;
  DoWait : boolean;
begin
  if not Enabled then Exit;
  EnterCriticalSection(FCS);
  ResourceWritePending := (FWaitingWriters <> 0) or (FActive < 0);
  if ResourceWritePending and (FActiveThreadID <> 0) and (FActiveThreadID <> GetCurrentThreadID) then
  begin
    DoWait := true;
    Inc(FWaitingReaders);
  end
  else
  begin
    DoWait := false;
    FActiveThreadID := GetCurrentThreadID;
    if FActive = -1 then // our thread already writes to the resource
      Inc(FWriteDepth) // increasing usage depth
    else
      Inc(FActive);
  end;
  LeaveCriticalSection(FCS);
  if DoWait then
    WaitForSingleObject(FSemReaders, INFINITE);
end;

procedure TElSharedResource.WaitToWrite;
var
  ResourceOwned : boolean;
  DoWait : boolean;
begin
  if not Enabled then Exit;
  EnterCriticalSection(FCS);
  ResourceOwned := FActive <> 0;
  if ResourceOwned {and (FActiveThreadID <> 0) and (FActiveThreadID <> GetCurrentThreadID)} then
  begin
    if FActiveThreadID = GetCurrentThreadID then
    begin
      Inc(FWriteDepth);
      DoWait := false;
    end
    else
    begin
      DoWait := true;
      Inc(FWaitingWriters);
    end;
  end
  else
  begin
    DoWait := false;
    FActiveThreadID := GetCurrentThreadID;
    //Dec(FActive)
    FActive := -1;
    FWriteDepth := 1;
  end;
  LeaveCriticalSection(FCS);
  if DoWait then
  begin
    WaitForSingleObject(FSemWriters, cardinal(INFINITE));
    FActiveThreadID := GetCurrentThreadID;
  end;
end;

procedure TElSharedResource.Done;
var
  Sem : THandle;
  Count : longint;
begin
  if not Enabled then Exit;
  EnterCriticalSection(FCS);
  if FActive > 0 then
    Dec(FActive)
  else
  begin
    Dec(FWriteDepth);
    if (FWriteDepth = 0) then
      Inc(FActive);
  end;
  Sem := 0;
  Count := 1;
  if FActive = 0 then
  begin
    FActiveThreadID := 0;
    if FWaitingWriters > 0 then
    begin
      FActive := -1;
      FWriteDepth := 1;
      Dec(FWaitingWriters);
      Sem := FSemWriters;
    end
    else if FWaitingReaders > 0 then
    begin
      FActive := FWaitingReaders;
      FWaitingReaders := 0;
      Sem := FSemReaders;
      Count := FActive;
    end;
  end;
  LeaveCriticalSection(FCS);
  if Sem <> 0 then
    ReleaseSemaphore(Sem, Count, nil);
end;

{$else}

constructor TElSharedResource.Create;
begin
  inherited;
  FSynchronizer := TMultiReadExclusiveWriteSynchronizer.Create;
  FEnabled := true;
end;

destructor TElSharedResource.Destroy;
begin
  FSynchronizer.Free;
  inherited;
end;

procedure TElSharedResource.WaitToRead;
begin
  if not Enabled then Exit;
  FSynchronizer.BeginRead;
  FAction := 1;
end;

procedure TElSharedResource.WaitToWrite;
begin
  if not Enabled then Exit;
  FSynchronizer.BeginWrite;
  FAction := 2;
end;

procedure TElSharedResource.Done;
begin
  if not Enabled then Exit;
  if FAction = 1 then
    FSynchronizer.EndRead
  else
  if FAction = 2 then
    FSynchronizer.EndWrite;
end;

{$endif}

end.
