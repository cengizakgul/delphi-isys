
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBRC2;

interface

uses
  SBUtils;

type

  TRC2Key =  array of byte;
  TRC2ExpandedKey = array[0..127] of byte;
  TRC2Buffer =  array[0..7] of byte;
  PRC2Buffer = ^TRC2Buffer;

  TRC2Context = packed  record
    ExpandedKey: TRC2ExpandedKey;
    Vector: TRC2Buffer;
    Tail: TRC2Buffer;
    TailLen: cardinal;
  end;

const 
  TRC2BufferSize = 8;
  TRC2ExpandedKeySize = 64 * 2;

// Key expansion routine
procedure ExpandKey(const Key: TRC2Key; out ExpandedKey: TRC2ExpandedKey); 

// Block processing routines
procedure Encrypt(const InBuf: TRC2Buffer; const ExpandedKey: TRC2ExpandedKey;
  out OutBuf: TRC2Buffer); 
procedure Decrypt(const InBuf: TRC2Buffer; const ExpandedKey: TRC2ExpandedKey;
  out OutBuf: TRC2Buffer); 


// Memory processing routines

function EncryptCBC(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TRC2ExpandedKey;
  const InitVector: TRC2Buffer): boolean; overload;
function DecryptCBC(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal; const ExpandedKey: TRC2ExpandedKey;
  const InitVector: TRC2Buffer): boolean; overload;

// Chunks processing routines

procedure InitializeEncryptionCBC(var Context: TRC2Context; const Key: TRC2Key;
  const InitVector: TRC2Buffer); 
procedure InitializeDecryptionCBC(var Context: TRC2Context; Key: TRC2Key;
  InitVector: TRC2Buffer); 

function EncryptCBC(var Context: TRC2Context; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean; overload;
function DecryptCBC(var Context: TRC2Context; Chunk: pointer; ChunkSize:
  cardinal;
  OutBuf: pointer; var OutSize: cardinal): boolean; overload;

function FinalizeEncryptionCBC(var Context: TRC2Context; OutBuf: pointer;
  var OutSize: cardinal): boolean;

function ParseASN1Params(const Params : BufferType; var IV : BufferType;
  var KeyBits : integer): boolean; 
procedure WriteASN1Params(const IV : BufferType; KeyBits : integer;
  var Params : BufferType); 

implementation

uses
  SBASN1Tree;

const
  RC2Box: array[0..255] of byte = (
    217, 120, 249, 196, 25, 221, 181, 237, 40, 233, 253, 121, 74, 160, 216, 157,
    198, 126, 55, 131, 43, 118, 83, 142, 98, 76, 100, 136, 68, 139, 251, 162,
    23, 154, 89, 245, 135, 179, 79, 19, 97, 69, 109, 141, 9, 129, 125, 50,
    189, 143, 64, 235, 134, 183, 123, 11, 240, 149, 33, 34, 92, 107, 78, 130,
    84, 214, 101, 147, 206, 96, 178, 28, 115, 86, 192, 20, 167, 140, 241, 220,
    18, 117, 202, 31, 59, 190, 228, 209, 66, 61, 212, 48, 163, 60, 182, 38,
    111, 191, 14, 218, 70, 105, 7, 87, 39, 242, 29, 155, 188, 148, 67, 3,
    248, 17, 199, 246, 144, 239, 62, 231, 6, 195, 213, 47, 200, 102, 30, 215,
    8, 232, 234, 222, 128, 82, 238, 247, 132, 170, 114, 172, 53, 77, 106, 42,
    150, 26, 210, 113, 90, 21, 73, 116, 75, 159, 208, 94, 4, 24, 164, 236,
    194, 224, 65, 110, 15, 81, 203, 204, 36, 145, 175, 80, 161, 244, 112, 57,
    153, 124, 58, 133, 35, 184, 180, 122, 252, 2, 54, 91, 37, 85, 151, 49,
    45, 93, 250, 152, 227, 138, 146, 174, 5, 223, 41, 16, 103, 108, 186, 201,
    211, 0, 230, 207, 225, 158, 168, 44, 99, 22, 1, 63, 88, 226, 137, 169,
    13, 56, 52, 27, 171, 51, 255, 176, 187, 72, 12, 95, 185, 177, 205, 46,
    197, 243, 219, 71, 229, 165, 156, 119, 10, 166, 32, 104, 254, 127, 193,
      173);


// Key expansion routine

procedure ExpandKey(const Key: TRC2Key; out ExpandedKey: TRC2ExpandedKey);
var
  I, KeySize: integer;
  EfLen: cardinal;
  T8: integer;
  TM: byte;
  K: array[0..63] of word;
begin
  KeySize := Length(Key);
  EfLen := KeySize * 8;
  FillChar(ExpandedKey[0], SizeOf(ExpandedKey), 0);

  if (KeySize <= 0) or (KeySize > 128) then
    raise EEncryptionError.CreateFmt(SInvalidKeySize, [KeySize * 8]);
  //BUG - Move(Key[0], ExpandedKey, KeySize); ?
  for i := 0 to KeySize - 1 do
    ExpandedKey[i] := Key[i];

  for I := KeySize to 127 do
    ExpandedKey[I] := RC2Box[(ExpandedKey[I - KeySize] + ExpandedKey[I - 1]) and
      $FF];

  // Ported from C
  T8 := (EfLen + 7) div 8;
  TM := 255 shr ((8 - (EfLen mod 8)) mod 8);
  ExpandedKey[128 - T8] := RC2Box[ExpandedKey[128 - T8] and TM];

  for i := 127 - T8 downto 0 do
    ExpandedKey[i] := RC2Box[ExpandedKey[i + 1] xor ExpandedKey[i + T8]];

  for i := 0 to 63 do
    K[i] := ExpandedKey[2 * i] + word(ExpandedKey[2 * i + 1] shl 8);

  Move(K[0], ExpandedKey[0], SizeOf(K));
//  ExpandedKey[0] := RC2Box[ExpandedKey[0]];
end;


// Block processing routines

procedure Encrypt(const InBuf: TRC2Buffer; const ExpandedKey: TRC2ExpandedKey;
  out OutBuf: TRC2Buffer);
var
  T, TK: array[0..3] of word;
  TWK: array[0..63] of word;
  I: integer;
begin
  PInt64(@T)^ := PInt64(@InBuf[0])^;
  Move(ExpandedKey, TWK, SizeOf(TWK));
  for I := 0 to 15 do
  begin
    PInt64(@TK)^ := PInt64(@ExpandedKey[I * 8])^;
    Inc(T[0], (T[1] and not T[3]) + (T[2] and T[3]) + TK[0]);
    T[0] := (T[0] shl 1) or (T[0] shr 15);
    Inc(T[1], (T[2] and not T[0]) + (T[3] and T[0]) + TK[1]);
    T[1] := (T[1] shl 2) or (T[1] shr 14);
    Inc(T[2], (T[3] and not T[1]) + (T[0] and T[1]) + TK[2]);
    T[2] := (T[2] shl 3) or (T[2] shr 13);
    Inc(T[3], (T[0] and not T[2]) + (T[1] and T[2]) + TK[3]);
    T[3] := (T[3] shl 5) or (T[3] shr 11);
    if (I = 4) or (I = 10) then
    begin
      Inc(T[0], TWK[T[3] and 63]);
      Inc(T[1], TWK[T[0] and 63]);
      Inc(T[2], TWK[T[1] and 63]);
      Inc(T[3], TWK[T[2] and 63]);
    end;
  end;
  PInt64(@OutBuf[0])^ := PInt64(@T)^;
end;

procedure Decrypt(const InBuf: TRC2Buffer; const ExpandedKey: TRC2ExpandedKey; out OutBuf: TRC2Buffer);
var
  T, TK: array[0..3] of word;
  TWK: array[0..63] of word;
  I: integer;
begin
  PInt64(@T)^ := PInt64(@InBuf[0])^;
  Move(ExpandedKey, TWK, SizeOf(TWK));
  for I := 15 downto 0 do
  begin
    PInt64(@TK)^ := PInt64(@ExpandedKey[I * 8])^;
    T[3] := (T[3] shr 5) or (T[3] shl 11);
    Dec(T[3], (T[0] and not T[2]) + (T[1] and T[2]) + TK[3]);
    T[2] := (T[2] shr 3) or (T[2] shl 13);
    Dec(T[2], (T[3] and not T[1]) + (T[0] and T[1]) + TK[2]);
    T[1] := (T[1] shr 2) or (T[1] shl 14);
    Dec(T[1], (T[2] and not T[0]) + (T[3] and T[0]) + TK[1]);
    T[0] := (T[0] shr 1) or (T[0] shl 15);
    Dec(T[0], (T[1] and not T[3]) + (T[2] and T[3]) + TK[0]);
    if (I = 5) or (I = 11) then
    begin
      Dec(T[3], TWK[T[2] and $3F]);
      Dec(T[2], TWK[T[1] and $3F]);
      Dec(T[1], TWK[T[0] and $3F]);
      Dec(T[0], TWK[T[3] and $3F]);
    end;
  end;
  PInt64(@OutBuf[0])^ := PInt64(@T)^;
end;


// Memory processing routines

procedure InitializeEncryptionCBC(var Context: TRC2Context; const Key: TRC2Key;
  const InitVector: TRC2Buffer);
begin

  ExpandKey(Key, Context.ExpandedKey);
  Context.Vector := InitVector;
  Context.TailLen := 0;
end;

procedure InitializeDecryptionCBC(var Context: TRC2Context; Key: TRC2Key;
  InitVector: TRC2Buffer);
begin

  ExpandKey(Key, Context.ExpandedKey);
  Context.Vector := InitVector;
  Context.TailLen := 0;
end;


function EncryptCBC(var Context: TRC2Context; Chunk: pointer; ChunkSize:
  cardinal;
  OutBuf: pointer; var OutSize: cardinal): boolean;
var
  Left, Needed: cardinal;
  Temp: TRC2Buffer;
begin
  if ChunkSize = 0 then
  begin
    OutSize := 0;
    Result := True;
    exit;
  end;
  Needed := ((Context.TailLen + ChunkSize) div SizeOf(TRC2Buffer)) *
    SizeOf(TRC2Buffer);
  if Needed > OutSize then
  begin
    OutSize := Needed;
    Result := False;
    exit;
  end;
  if Context.TailLen > 0 then
  begin
    Left := SizeOf(TRC2Buffer) - Context.TailLen;
    if Left > ChunkSize then
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], ChunkSize);
      Inc(Context.TailLen, ChunkSize);
      ChunkSize := 0;
    end
    else
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], Left);
      Dec(ChunkSize, Left);
      PLongWord(@Temp[0])^ := PLongWord(@Context.Tail[0])^ xor
        PLongWord(@Context.Vector[0])^;
      PLongWord(@Temp[4])^ := PLongWord(@Context.Tail[4])^ xor
        PLongWord(@Context.Vector[4])^;
      Encrypt(Temp, Context.ExpandedKey, PRC2Buffer(OutBuf)^);
      Context.Vector := PRC2Buffer(OutBuf)^;
      Inc(PtrUInt(OutBuf), SizeOf(TRC2Buffer));
      Inc(PtrUInt(Chunk), Left);
      Context.TailLen := 0;
    end;
  end;
  while ChunkSize >= SizeOf(TRC2Buffer) do
  begin
    PLongWord(@Temp[0])^ := PLongWord(Chunk)^ xor
      PLongWord(@Context.Vector[0])^;
    Inc(PtrUInt(Chunk), 4);
    PLongWord(@Temp[4])^ := PLongWord(Chunk)^ xor
      PLongWord(@Context.Vector[4])^;
    Inc(PtrUInt(Chunk), 4);
    Encrypt(Temp, Context.ExpandedKey, PRC2Buffer(OutBuf)^);
    Context.Vector := PRC2Buffer(OutBuf)^;
    Inc(PtrUInt(OutBuf), SizeOf(TRC2Buffer));
    Dec(ChunkSize, SizeOf(TRC2Buffer));
  end;
  if ChunkSize > 0 then
  begin
    Move(Chunk^, Context.Tail[0], ChunkSize);
    Context.TailLen := ChunkSize;
  end;
  OutSize := Needed;
  Result := True;
end;

function DecryptCBC(var Context: TRC2Context; Chunk: pointer; ChunkSize:
  cardinal; OutBuf: pointer; var OutSize: cardinal): boolean;
var
  Left, Needed: cardinal;
  NextVector: TRC2Buffer;
begin
  if ChunkSize = 0 then
  begin
    OutSize := 0;
    Result := True;
    exit;
  end;
  Needed := ((Context.TailLen + ChunkSize) div SizeOf(TRC2Buffer)) *
    SizeOf(TRC2Buffer);
  if Needed > OutSize then
  begin
    OutSize := Needed;
    Result := False;
    exit;
  end;
  if Context.TailLen > 0 then
  begin
    Left := SizeOf(TRC2Buffer) - Context.TailLen;
    if Left > ChunkSize then
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], ChunkSize);
      Inc(Context.TailLen, ChunkSize);
      ChunkSize := 0;
    end
    else
    begin
      Move(Chunk^, Context.Tail[Context.TailLen], Left);
      Inc(PtrUInt(Chunk), Left);
      Dec(ChunkSize, Left);
      NextVector := Context.Tail;
      Decrypt(Context.Tail, Context.ExpandedKey, PRC2Buffer(OutBuf)^);
      PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
        PLongWord(@Context.Vector[0])^;
      Inc(PtrUInt(OutBuf), 4);
      PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor
        PLongWord(@Context.Vector[4])^;
      Inc(PtrUInt(OutBuf), 4);
      Context.Vector := NextVector;
      Context.TailLen := 0;
    end;
  end;
  while ChunkSize >= SizeOf(TRC2Buffer) do
  begin
    NextVector := PRC2Buffer(Chunk)^;
    Decrypt(PRC2Buffer(Chunk)^, Context.ExpandedKey, PRC2Buffer(OutBuf)^);
    PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor PLongWord(@Context.Vector[0])^;
    Inc(PtrUInt(OutBuf), 4);
    PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor PLongWord(@Context.Vector[4])^;
    Inc(PtrUInt(OutBuf), 4);
    Context.Vector := NextVector;
    Inc(PtrUInt(Chunk), SizeOf(TRC2Buffer));
    Dec(ChunkSize, SizeOf(TRC2Buffer));
  end;
  if ChunkSize > 0 then
  begin
    Move(Chunk^, Context.Tail[0], ChunkSize);
    Context.TailLen := ChunkSize;
  end;
  OutSize := Needed;
  Result := True;
end;

function FinalizeEncryptionCBC(var Context: TRC2Context; OutBuf: pointer;
  var OutSize: cardinal): boolean;
begin
  if Context.TailLen > 0 then
  begin
    if OutSize < SizeOf(TRC2Buffer) then
    begin
      OutSize := SizeOf(TRC2Buffer);
      Result := False;
    end
    else
    begin
      FillChar(Context.Tail[Context.TailLen], SizeOf(TRC2Buffer) -
        Context.TailLen, 0);
      PLongWord(@Context.Tail[0])^ := PLongWord(@Context.Tail[0])^ xor
        PLongWord(@Context.Vector[0])^;
      PLongWord(@Context.Tail[4])^ := PLongWord(@Context.Tail[4])^ xor
        PLongWord(@Context.Vector[4])^;
      Encrypt(Context.Tail, Context.ExpandedKey, PRC2Buffer(OutBuf)^);
      OutSize := SizeOf(TRC2Buffer);
      Result := True;
    end;
  end
  else
  begin
    OutSize := 0;
    Result := True;
  end;
end;

function FinalizeDecryptionCBC(var Context: TRC2Context; OutBuf: pointer;
  var OutSize: cardinal): boolean;
begin
  if Context.TailLen > 0 then
  begin
    if Context.TailLen < SizeOf(TRC2Buffer) then
    begin
      OutSize := 0;
      Result := False;
    end
    else
      if OutSize < SizeOf(TRC2Buffer) then
      begin
        OutSize := SizeOf(TRC2Buffer);
        Result := False;
      end
      else
      begin
        Decrypt(Context.Tail, Context.ExpandedKey, PRC2Buffer(OutBuf)^);
        PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor PLongWord(@Context.Vector[0])^;
        Inc(PtrUInt(OutBuf), 4);
        PLongWord(OutBuf)^ := PLongWord(OutBuf)^ xor PLongWord(@Context.Vector[4])^;
        OutSize := SizeOf(TRC2Buffer);
        Result := True;
      end;
  end
  else
  begin
    OutSize := 0;
    Result := True;
  end;
end;

function EncryptCBC(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TRC2ExpandedKey; const InitVector: TRC2Buffer): boolean;
var
  TempIn, TempOut: PRC2Buffer;
  Vector, Temp: TRC2Buffer;
  S: cardinal;
begin
  S := (InSize div SizeOf(TRC2Buffer)) * SizeOf(TRC2Buffer);
  if (InSize mod SizeOf(TRC2Buffer)) <> 0 then
    S := S + SizeOf(TRC2Buffer);
  if OutSize < S then
  begin
    OutSize := S;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector := InitVector;
  while InSize >= SizeOf(TRC2Buffer) do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    PLongWord(@TempIn^[0])^ := PLongWord(@TempIn^[0])^ xor
      PLongWord(@Vector[0])^;
    PLongWord(@TempIn^[4])^ := PLongWord(@TempIn^[4])^ xor
      PLongWord(@Vector[4])^;
    Encrypt(TempIn^, ExpandedKey, TempOut^);
    Vector := TempOut^;
    Dec(InSize, SizeOf(TRC2Buffer));
    Inc(OutSize, SizeOf(TRC2Buffer));
  end;
  if InSize > 0 then
  begin
    Move(Pointer(PtrUInt(InBuffer) + OutSize)^, Temp, InSize);
    FillChar(Temp[InSize], SizeOf(Temp) - InSize, 0);
    PLongWord(@Temp[0])^ := PLongWord(@Temp[0])^ xor PLongWord(@Vector[0])^;
    PLongWord(@Temp[4])^ := PLongWord(@Temp[4])^ xor PLongWord(@Vector[4])^;
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Encrypt(Temp, ExpandedKey, TempOut^);
    Inc(OutSize, SizeOf(TRC2Buffer));
  end;
  Result := True;
end;

function DecryptCBC(InBuffer: pointer; InSize: cardinal;
  OutBuffer: pointer; var OutSize: cardinal;
  const ExpandedKey: TRC2ExpandedKey; const InitVector: TRC2Buffer): boolean;
var
  TempIn, TempOut: PRC2Buffer;
  Vector1, Vector2: TRC2Buffer;
begin
  if (InSize mod SizeOf(TRC2Buffer)) > 0 then
    raise EEncryptionError.Create(SInvalidInputSize);
  if OutSize < InSize then
  begin
    OutSize := InSize;
    Result := False;
    exit;
  end;
  OutSize := 0;
  Vector1 := InitVector;
  while InSize > 0 do
  begin
    TempIn := Pointer(PtrUInt(InBuffer) + OutSize);
    TempOut := Pointer(PtrUInt(OutBuffer) + OutSize);
    Vector2 := TempIn^;
    Decrypt(TempIn^, ExpandedKey, TempOut^);
    PLongWord(@TempOut^[0])^ := PLongWord(@TempOut^[0])^ xor
      PLongWord(@Vector1[0])^;
    PLongWord(@TempOut^[4])^ := PLongWord(@TempOut^[4])^ xor
      PLongWord(@Vector1[4])^;
    Vector1 := Vector2;
    Dec(InSize, SizeOf(TRC2Buffer));
    Inc(OutSize, SizeOf(TRC2Buffer));
  end;
  Result := True;
end;

function ParseASN1Params(const Params : BufferType; var IV : BufferType;
  var KeyBits : integer): boolean;
var
  Tag, SeqTag : TElASN1ConstrainedTag;
  Index : integer;
  Ver : integer;
begin
  Result := false;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if not Tag.LoadFromBuffer(@Params[1], Length(Params)) then
      Exit;
    if (Tag.Count <> 1) or (not Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      Exit;
    SeqTag := TElASN1ConstrainedTag(Tag.GetField(0));
    Index := 0;
    if SeqTag.Count < 1 then
      Exit;
    if SeqTag.GetField(0).CheckType(SB_ASN1_INTEGER, false) then
    begin
      Ver := ASN1ReadInteger(TElASN1SimpleTag(SeqTag.GetField(0)));
      if Ver = 160 then
        KeyBits := 40
      else if Ver = 120 then
        KeyBits := 64
      else if Ver = 58 then
        KeyBits := 128
      else if Ver >= 256 then
        KeyBits := Ver
      else
        Exit;
      Inc(Index);
    end
    else
      KeyBits := 32;
    if Index >= SeqTag.Count then
      Exit;
    if SeqTag.GetField(Index).CheckType(SB_ASN1_OCTETSTRING, false) then
      IV := TElASN1SimpleTag(SeqTag.GetField(Index)).Content
    else
      Exit;    
    Result := true;
  finally
    FreeAndNil(Tag);
  end;
end;

procedure WriteASN1Params(const IV : BufferType; KeyBits : integer;
  var Params : BufferType);
var
  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  Ver : integer;
  Size : integer;
begin
  Tag := TElASN1ConstrainedTag.Create;
  try
    Tag.TagID := SB_ASN1_SEQUENCE;
    if KeyBits <> 32 then
    begin
      if KeyBits = 40 then
        Ver := 160
      else if KeyBits = 64 then
        Ver := 120
      else if KeyBits = 128 then
        Ver := 58
      else
        Ver := KeyBits;
      STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
      STag.TagID := SB_ASN1_INTEGER;
      ASN1WriteInteger(STag, Ver);
    end;
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagID := SB_ASN1_OCTETSTRING;
    STag.Content := IV;
    Size := 0;
    Tag.SaveToBuffer(nil, Size);
    SetLength(Params, Size);
    Tag.SaveToBuffer(@Params[1], Size);
    SetLength(Params, Size);
  finally
    FreeAndNil(Tag);
  end;
end;


end.
