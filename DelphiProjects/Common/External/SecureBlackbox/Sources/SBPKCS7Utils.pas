(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBPKCS7Utils;

interface

uses
  Classes,
  SBUtils,
  SBASN1Tree;

type

  TElPKCS7Attributes = class
  private
    FAttributes : TStringList;
    FValues : TList;
  protected 
    function GetAttribute(Index : integer) : BufferType;
    procedure SetAttribute(Index : integer; const Value : BufferType);
    function GetValues(Index : integer) : TStringList;
    function GetCount : integer;
    procedure SetCount(Value : integer);
  public
    constructor Create;
    destructor Destroy; override;
    function Remove(Index : integer) : boolean;
    procedure Copy(Dest : TElPKCS7Attributes);
    function SaveToBuffer(Buffer : pointer; var Size : integer) : boolean;
    property Attributes[Index : integer] : BufferType read GetAttribute write
      SetAttribute;
    property Values[Index : integer] : TStringList read GetValues;
    property Count : integer read GetCount write SetCount;
  end;

procedure SaveAttributes(Tag : TElASN1ConstrainedTag; Attributes : TElPKCS7Attributes;
  TagID : byte = SB_ASN1_SET); 

implementation

////////////////////////////////////////////////////////////////////////////////
// TElPKCS7Attributes

constructor TElPKCS7Attributes.Create;
begin
  inherited;
  FAttributes := TStringList.Create;
  FValues := TList.Create;
end;

destructor TElPKCS7Attributes.Destroy;
var
  Lst : TStringList;
begin
  FAttributes.Free;
  while FValues.Count > 0 do
  begin
    Lst := TStringList(FValues.Items[0]);
    FValues.Delete(0);
    Lst.Free;
  end;
  FreeAndNil(FValues);
  inherited;
end;

function TElPKCS7Attributes.GetAttribute(Index : integer) : BufferType;
begin
  if Index < FAttributes.Count then
    Result := FAttributes[Index]
  else
    Result := '';
end;

procedure TElPKCS7Attributes.SetAttribute(Index : integer; const Value : BufferType);
begin
  if Index < FAttributes.Count then
    FAttributes[Index] := Value;
end;

function TElPKCS7Attributes.GetValues(Index : integer) : TStringList;
begin
  if Index < FValues.Count then
    Result := TStringList(FValues[Index])
  else
    Result := nil;
end;

function TElPKCS7Attributes.GetCount : integer;
begin
  Result := FAttributes.Count;
end;

procedure TElPKCS7Attributes.SetCount(Value : integer);
var
  Lst : TStringList;
begin
  if Value < FAttributes.Count then
  begin
    while FAttributes.Count > Value do
      FAttributes.Delete(FAttributes.Count - 1);
    while FValues.Count > Value do
    begin
      Lst := TStringList(FValues[FValues.Count - 1]);
      FValues.Delete(FValues.Count - 1);
      Lst.Free;
    end;
  end
  else
  if Value > FAttributes.Count then
  begin
    while FAttributes.Count < Value do
      FAttributes.Add('');
    while FValues.Count < Value do
      FValues.Add(TStringList.Create);
  end;
end;

function TElPKCS7Attributes.Remove(Index : integer) : boolean;
var
  Lst : TStringList;
begin
  if Index < FAttributes.Count then
  begin
    FAttributes.Delete(Index);
    Lst := TStringList(FValues.Items[Index]);
    FValues.Delete(Index);
    Lst.Free;
    Result := true;
  end
  else
    Result := false;
end;

procedure TElPKCS7Attributes.Copy(Dest : TElPKCS7Attributes);
var
  I, J, OldCount : integer;
begin
  if not Assigned(Dest) then
    Exit;
  OldCount := Dest.Count;
  Dest.Count := OldCount + FAttributes.Count;
  for I := 0 to FAttributes.Count - 1 do
  begin
    Dest.FAttributes[OldCount + I] := FAttributes[I];
    for J := 0 to Values[I].Count - 1 do
      Dest.Values[OldCount + I].Add(Values[I].Strings[J]);
  end;
end;

function TElPKCS7Attributes.SaveToBuffer(Buffer : pointer; var Size : integer) :
  boolean;
var
  CTag : TElASN1ConstrainedTag;
begin
  CTag := TElASN1ConstrainedTag.Create;
  try
    SaveAttributes(CTag, Self);
    Result := CTag.SaveToBuffer(Buffer, Size);
  finally
    FreeAndNil(CTag);
  end;
end;

procedure SaveAttributes(Tag : TElASN1ConstrainedTag; Attributes :
  TElPKCS7Attributes; TagID : byte = SB_ASN1_SET);
var
  CTag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  I, J : integer;
begin
  Tag.TagId := TagID;
  for I := 0 to Attributes.Count - 1 do
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    CTag.TagId := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagId := SB_ASN1_OBJECT;
    STag.Content := CloneBuffer(Attributes.FAttributes.Strings[I]);
    CTag := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
    CTag.TagId := SB_ASN1_SET;
    for J := 0 to Attributes.Values[I].Count - 1 do
    begin
      STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
      STag.WriteHeader := false;
      STag.Content := CloneBuffer(Attributes.Values[I].Strings[J]);
    end;
  end;
end;

end.
