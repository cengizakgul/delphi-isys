(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBBzip2Common;

interface

uses
  SysUtils,
  SBUtils,
  SBBzlib,
  SBBzip2BaseTypes,
  SBBzip2Types,
  SBBzip2,
  SBBzip2Utils;


procedure BZ2_compressBlock ( s:EState; is_last_block:Boolean ); 
function BZ2_decompress ( s:DState ):Int32; 

implementation

////////////////////////////////////////////////////////////////////////////////
// Blocksort

//---------------------------------------------*/
//--- Fallback O(N log(N)^2) sorting        ---*/
//--- algorithm, for repetitive blocks      ---*/
//---------------------------------------------*/

procedure fallbackSimpleSort ( fmap:TSBUInt32Array;
                          eclass:TSBUInt32Array;
                          lo:Int32;
                          hi:Int32 );
var
   i, j, tmp:Int32;
   ec_tmp:UInt32;
begin
   if (lo = hi) then exit;

   if (hi - lo > 3) then
      for i := hi-4 downto lo do
      begin
         tmp := fmap.GetData(i);
         ec_tmp := eclass.GetData(tmp);
         j := i+4;
         while (j <= hi) and (ec_tmp > eclass.GetData(fmap.GetData(j))) do
         begin
            fmap.SetData(fmap.GetData(j),j-4);
            j:= j+ 4;
         end;
         fmap.SetData(tmp,j-4);
      end;

   for i := hi-1 downto lo do
   begin
      tmp := fmap.GetData(i);
      ec_tmp := eclass.GetData(tmp);
      j := i+1;
      while (j <= hi) and (ec_tmp > eclass.GetData(fmap.GetData(j))) do
      begin
        fmap.SetData(fmap.GetData(j),j-1);
        inc(j);
      end;
      fmap.SetData(tmp,j-1);
   end;
end;



procedure fswap(var zz1, zz2:Int32); overload;
var zztmp:Int32;
begin
  zztmp := zz1;
  zz1 := zz2;
  zz2 := zztmp;
end;

procedure fswap(var zz1, zz2:UInt32); overload;
var zztmp:UInt32;
begin
  zztmp := zz1;
  zz1 := zz2;
  zz2 := zztmp;
end;

procedure fvswap(zzp1, zzp2, zzn:Int32;fmap:TSBUInt32Array);
var yyp1,yyp2,yyn:Int32;
        tmp1,tmp2:Int32;
begin
   yyp1 := (zzp1);
   yyp2 := (zzp2);
   yyn  := (zzn);
   while (yyn > 0) do
   begin
      tmp1:=fmap.GetData(yyp1);
      tmp2:=fmap.GetData(yyp2);
      fswap(tmp1,tmp2);
      fmap.SetData(tmp1,yyp1);
      fmap.SetData(tmp2,yyp2);
      inc(yyp1); inc(yyp2); dec(yyn);
   end;
end;


function fmin(a,b:Int32):Int32;
begin
  //Result := {$ifndef DELPHI_NET}IfThen{$else}IfThenInt{$endif}((a) < (b),a,b);
  if a < b then 
    result := a
  else 
    result := b;
end;

procedure fpush(lz,hz:Int32; var stackLo,stackHi:array of Int32; var sp:Int32);
begin
  stackLo[sp] := lz;
  stackHi[sp] := hz;
  inc(sp);
end;

procedure fpop(var lz,hz:Int32; var stackLo,stackHi:array of Int32; var sp:Int32);
begin
  dec(sp);
  lz := stackLo[sp];
  hz := stackHi[sp];
end;

const  FALLBACK_QSORT_SMALL_THRESH = 10;
const  FALLBACK_QSORT_STACK_SIZE =  100;


procedure fallbackQSort3 ( fmap:TSBUInt32Array;
                      eclass:TSBUInt32Array;
                      loSt:Int32;
                      hiSt:Int32);
var
   unLo, unHi, ltLo, gtHi, n, m:Int32;
   sp, lo, hi:Int32;
   med, r, r3:UInt32;
   stackLo:array [0..FALLBACK_QSORT_STACK_SIZE] of Int32;
   stackHi:array [0..FALLBACK_QSORT_STACK_SIZE] of Int32;
   tmp1,tmp2:Int32;
begin
   r := 0;

   sp := 0;
   fpush ( loSt, hiSt,stackLo,stackHi,sp );

   while (sp > 0) do
   begin

      AssertH ( sp < FALLBACK_QSORT_STACK_SIZE, 1004 );

      fpop ( lo, hi,stackLo,stackHi,sp );
      if (hi - lo) < FALLBACK_QSORT_SMALL_THRESH then
      begin
         fallbackSimpleSort ( fmap, eclass, lo, hi );
         continue;
      end;

      r := ((r * 7621) + 1) mod 32768;
      r3 := r mod 3;
      if (r3 = 0) then
        med := eclass.GetData(fmap.GetData(lo))
      else
      if (r3 = 1) then
        med := eclass.GetData(fmap.GetData((lo+hi) shr 1))
      else
        med := eclass.GetData(fmap.GetData(hi));

      unLo := lo;
      ltLo := lo;
      unHi := hi;
      gtHi := hi;

      while (True) do
      begin
         while (True) do
         begin
            if (unLo > unHi) then break;
            n:=eclass.GetData(fmap.GetData(unLo));
            n:=n-integer(med);

            if (n = 0) then
            begin
               tmp1:=fmap.GetData(unLo);
               tmp2:=fmap.GetData(ltLo);
               fswap(tmp1,tmp2);
               fmap.SetData(tmp1,unLo);
               fmap.SetData(tmp2,ltLo);
               inc(ltLo);
               inc(unLo);
               continue;
            end;
            if (n > 0) then break;
            inc(unLo);
         end;
         while (True) do
         begin
            if (unLo > unHi) then break;
            n := eclass.GetData(fmap.GetData(unHi));
            n := n - integer(med);
            if (n = 0) then
            begin
               tmp1:=fmap.GetData(unHi);
               tmp2:=fmap.GetData(gtHi);
               fswap(tmp1,tmp2);
               fmap.SetData(tmp1,unHi);
               fmap.SetData(tmp2,gtHi);
               dec(gtHi);
               dec(unHi);
               continue;
            end;;
            if (n < 0) then break;
            dec(unHi);
         end;
         if (unLo > unHi) then break;

         tmp1:=fmap.GetData(unLo);
         tmp2:=fmap.GetData(unHi);
         fswap(tmp1,tmp2);
         fmap.SetData(tmp1,unLo);
         fmap.SetData(tmp2,unHi);
         inc(unLo);
         dec(unHi);
      end;

      AssertD ( unHi = unLo-1, 'fallbackQSort3(2)' );

      if (gtHi < ltLo) then continue;

      n := fmin(ltLo-lo, unLo-ltLo);
      fvswap(lo, unLo-n, n,fmap);
      m := fmin(hi-gtHi, gtHi-unHi);
      fvswap(unLo, hi-m+1, m,fmap);

      n := lo + unLo - ltLo - 1;
      m := hi - (gtHi - unHi) + 1;

      if (n - lo) > (hi - m) then
      begin
         fpush ( lo, n,stackLo,stackHi,sp  );
         fpush ( m, hi,stackLo,stackHi,sp  );
      end
      else
      begin
         fpush ( m, hi ,stackLo,stackHi,sp );
         fpush ( lo, n ,stackLo,stackHi,sp );
      end;
   end;
end;



procedure fallbackSort ( fmap:TSBUInt32Array;
                    eclass:TSBUInt32Array;
                    bhtab:TSBUInt32Array;
                    nblock:Int32;
                    verb:Int32 );
var
   ftab:array [0..256] of Int32;
   ftabCopy:array [0..255] of Int32;
   H, i, j, k, l, r, cc, cc1:Int32;
   nNotDone:Int32;
   nBhtab:Int32;
   eclass8:TSBByteonUInt32Array;

procedure SET_BH(zz:Int32);
begin
    bhtab.SetData(bhtab.GetData(zz shr 5) or (1 shl ((zz) and 31)),zz shr 5);
end;

procedure CLEAR_BH(zz:Int32);
begin
  bhtab.SetData(bhtab.GetData((zz) shr 5) and (not (1 shl ((zz) and 31))),(zz) shr 5);
end;

function ISSET_BH(zz:Int32):boolean;
begin
  Result:=(bhtab.GetData(zz shr 5) and (1 shl ((zz) and 31))) <>0;
end;

function WORD_BH(zz:Int32):UInt32;
begin
  Result:=bhtab.GetData((zz) shr 5);
end;

function UNALIGNED_BH(zz:Int32):Int32;
begin
  Result:=((zz) and $01f);
end;

begin
   eclass8 := TSBByteonUInt32Array.Create(eclass,'eclass');

   if (verb >= 4) then
      Write (ErrOutput, '        bucket sorting ...' );
   for i := 0 to 256 do
      ftab[i] := 0;
   for i := 0 to nblock-1 do
      inc(ftab[eclass8.GetData(i)]);
   for i := 0 to 255 do
      ftabCopy[i] := ftab[i];
   for i := 1 to 256 do
      ftab[i] := ftab[i] + ftab[i-1];

   for i := 0 to nblock-1 do
   begin
      j := eclass8.GetData(i);
      k := ftab[j] - 1;
      ftab[j] := k;
      fmap.SetData(i,k);
   end;

   nBhtab := 2 + (nblock div 32);
   for i := 0 to nBhtab-1 do
      bhtab.SetData(0,i);
   for i := 0 to 255 do
      SET_BH(ftab[i]);

   for i := 0 to 31 do
   begin
      SET_BH(nblock + 2*i);
      CLEAR_BH(nblock + 2*i + 1);
   end;

   H := 1;
   while (true) do
   begin

      if (verb >= 4) then
         Write (ErrOutput, Format('        depth %6d has ', [H]) );

      j := 0;
      for i := 0 to nblock-1 do
      begin
         if ISSET_BH(i) then j := i;
         k := Int32(fmap.GetData(i)) - H;
         if (k < 0) then
            k := k + nblock;
         eclass.SetData(j,k);
      end;

      nNotDone := 0;
      r := -1;
      while (True) do
      begin
         k := r + 1;
         while ISSET_BH(k) and (UNALIGNED_BH(k)>0) do
            inc(k);
         if ISSET_BH(k) then
         begin
            while (WORD_BH(k) = $ffffffff) do
                k :=  k + 32;
            while ISSET_BH(k) do
              inc(k);
         end;
         l := k - 1;
         if (l >= nblock) then break;
         while (not ISSET_BH(k)) and (UNALIGNED_BH(k)>0) do
            inc(k);
         if not ISSET_BH(k) then
         begin
            while (WORD_BH(k) = $00000000) do
              k := k + 32;
            while not ISSET_BH(k) do
              inc(k);
         end;
         r := k - 1;
         if (r >= nblock) then break;

         if (r > l) then
         begin
            nNotDone := nNotDone +  (r - l + 1);
            fallbackQSort3(fmap, eclass, l, r );

            cc := -1;
            for i := l to r do
            begin
               cc1 := eclass.GetData(fmap.GetData(i));
               if (cc <> cc1) then
               begin
                 SET_BH(i);
                 cc := cc1;
               end;
            end;
         end;
      end;

      if (verb >= 4) then
         Write ( ErrOutput,Format('%6d unresolved strings', [nNotDone]) );

      H := H * 2;
      if (H > nblock) or (nNotDone = 0) then
          break;
   end;

   if (verb >= 4) then
      Write (ErrOutput, '        reconstructing block ...' );
   j := 0;
   for i := 0 to nblock -1  do
   begin
      while (ftabCopy[j] = 0) do
        inc(j);
      dec(ftabCopy[j]);
      eclass8.SetData(j,fmap.GetData(i));
   end;
   FreeAndNil(eclass8);
   AssertH ( j < 256, 1005 );
end;

function mainGtU ( i1:UInt32;
               i2:UInt32;
               block:TSBByteonUInt32Array;
               quadrant:TSBUInt16onUInt32Array;
               nblock:UInt32;
               var budget:Int32):boolean;
var
   k:Int32;
   c1, c2:Byte;
   s1, s2:UINt16;

begin
   AssertD ( i1 <> i2, 'mainGtU' );

   c1 := block.GetData(i1);
   c2 := block.GetData(i2);
   if (c1 <> c2) then
   begin
    Result:=(c1 > c2);
    exit;
   end;
   inc(i1); inc(i2);

   c1 := block.GetData(i1); c2 := block.GetData(i2);
   if (c1 <> c2) then
   begin
    Result:=(c1 > c2);
    exit ;
   end;
   inc(i1); inc(i2);
   c1 := block.GetData(i1);
   c2 := block.GetData(i2);
   if (c1 <> c2) then
   begin
    Result:=(c1 > c2);
    exit;
   end;
   inc(i1); inc(i2);
   c1 := block.GetData(i1);
   c2 := block.GetData(i2);
   if (c1 <> c2) then
   begin
    Result:=(c1 > c2);
    exit;
   end;
   inc(i1); inc(i2);
   c1 := block.GetData(i1);
   c2 := block.GetData(i2);
   if (c1 <> c2) then
   begin
    Result:=(c1 > c2);
    exit;
   end;
   inc(i1); inc(i2);
   c1 := block.GetData(i1);
   c2 := block.GetData(i2);
   if (c1 <> c2) then
   begin
    Result:=(c1 > c2);
    exit;
   end;
   inc(i1); inc(i2);
   c1 := block.GetData(i1);
   c2 := block.GetData(i2);
   if (c1 <> c2) then
   begin
    Result:=(c1 > c2);
    exit;
   end;
   inc(i1); inc(i2);
   c1 := block.GetData(i1);
   c2 := block.GetData(i2);
   if (c1 <> c2) then
   begin
    Result:=(c1 > c2);
    exit;
   end;
   inc(i1); inc(i2);
   c1 := block.GetData(i1);
   c2 := block.GetData(i2);
   if (c1 <> c2) then
   begin
    Result:=(c1 > c2);
    exit;
   end;
   inc(i1); inc(i2);
   c1 := block.GetData(i1);
   c2 := block.GetData(i2);
   if (c1 <> c2) then
   begin
    Result:=(c1 > c2);
    exit;
   end;
   inc(i1); inc(i2);
   c1 := block.GetData(i1);
   c2 := block.GetData(i2);
   if (c1 <> c2) then
   begin
    Result:=(c1 > c2);
    exit;
   end;
   inc(i1); inc(i2);
   c1 := block.GetData(i1);
   c2 := block.GetData(i2);
   if (c1 <> c2) then
   begin
    Result:=(c1 > c2);
    exit;
   end;
   inc(i1); inc(i2);

   k := nblock + 8;

   repeat

      c1 := block.GetData(i1);
      c2 := block.GetData(i2);

      if (c1 <> c2) then
      begin
       Result:=(c1 > c2);
       exit;
      end;
      s1 := quadrant.GetData(i1);
      s2 := quadrant.GetData(i2);
      if (s1 <> s2) then
      begin
       Result:=(s1 > s2);
       exit;
      end;
      inc(i1); inc(i2);

      c1 := block.GetData(i1);
      c2 := block.GetData(i2);

      if (c1 <> c2) then
      begin
       Result:=(c1 > c2);
       exit;
      end;
      s1 := quadrant.GetData(i1);
      s2 := quadrant.GetData(i2);

      if (s1 <> s2) then
      begin
       Result:=(s1 > s2);
       exit;
      end;
      inc(i1); inc(i2);

      c1 := block.GetData(i1);
      c2 := block.GetData(i2);

      if (c1 <> c2) then
      begin
       Result:=(c1 > c2);
       exit;
      end;
      s1 := quadrant.GetData(i1);
      s2 := quadrant.GetData(i2);

      if (s1 <> s2) then
      begin
       Result:=(s1 > s2);
       exit;
      end;
      inc(i1); inc(i2);

      c1 := block.GetData(i1);
      c2 := block.GetData(i2);

      if (c1 <> c2) then
      begin
       Result:=(c1 > c2);
       exit;
      end;
      s1 := quadrant.GetData(i1);
      s2 := quadrant.GetData(i2);

      if (s1 <> s2) then
      begin
       Result:=(s1 > s2);
       exit;
      end;
      inc(i1); inc(i2);

      c1 := block.GetData(i1);
      c2 := block.GetData(i2);

      if (c1 <> c2) then
      begin
       Result:=(c1 > c2);
       exit;
      end;
      s1 := quadrant.GetData(i1);
      s2 := quadrant.GetData(i2);

      if (s1 <> s2) then
      begin
       Result:=(s1 > s2);
       exit;
      end;
      inc(i1); inc(i2);

      c1 := block.GetData(i1);
      c2 := block.GetData(i2);

      if (c1 <> c2) then
      begin
       Result:=(c1 > c2);
       exit;
      end;
      s1 := quadrant.GetData(i1);
      s2 := quadrant.GetData(i2);

      if (s1 <> s2) then
      begin
       Result:=(s1 > s2);
       exit;
      end;
      inc(i1); inc(i2);

      c1 := block.GetData(i1);
      c2 := block.GetData(i2);

      if (c1 <> c2) then
      begin
       Result:=(c1 > c2);
       exit;
      end;
      s1 := quadrant.GetData(i1);
      s2 := quadrant.GetData(i2);

      if (s1 <> s2) then
      begin
       Result:=(s1 > s2);
       exit;
      end;
      inc(i1); inc(i2);

      c1 := block.GetData(i1);
      c2 := block.GetData(i2);

      if (c1 <> c2) then
      begin
       Result:=(c1 > c2);
       exit;
      end;
      s1 := quadrant.GetData(i1);
      s2 := quadrant.GetData(i2);

      if (s1 <> s2) then
      begin
       Result:=(s1 > s2);
       exit;
      end;
      inc(i1); inc(i2);

      if (i1 >= nblock) then i1 := i1 - nblock;
      if (i2 >= nblock) then i2 := i2 - nblock;

      k := k - 8;
      dec(budget);
   until k<0;
   Result:=False;
end;


const 
  incs: array [0..13] of Int32 =
  (
    1, 4, 13, 40, 121, 364, 1093, 3280,
    9841, 29524, 88573, 265720,
    797161, 2391484
  );

procedure mainSimpleSort ( ptr:TSBUInt32ArrayPointer;
                      block:TSBByteonUInt32Array;
                      quadrant:TSBUInt16onUInt32Array;
                      nblock,
                      lo,
                      hi,
                      d:Int32;
                      var budget:Int32 );
var
   i, j, h, bigN, hp:Int32;
   v:UInt32;
begin
   bigN := hi - lo + 1;
   if (bigN < 2) then exit;

   hp := 0;
   while (incs[hp] < bigN) do
      inc(hp);
   dec(hp);

   while hp >= 0 do
   begin
      h := incs[hp];

      i := lo + h;
      while (True) do
      begin

         if (i > hi) then break;
         v := ptr.GetData(i);
         j := i;
         while ( mainGtU (
                    ptr.GetData(j-h)+UInt32(d), v+UInt32(d), block, quadrant, nblock, budget
                 ) ) do
         begin
            ptr.SetData(ptr.GetData(j-h),j);
            j := j - h;
            if (j <= (lo + h - 1)) then break;
         end;
         ptr.SetData(v,j);
         inc(i);


         if (i > hi) then break;
         v := ptr.GetData(i);
         j := i;
         while ( mainGtU (
                    ptr.GetData(j-h)+UInt32(d), v+UInt32(d), block, quadrant, nblock, budget
                 ) ) do
         begin
            ptr.SetData(ptr.GetData(j-h),j);
            j := j - h;
            if (j <= (lo + h - 1)) then break;
         end;
         ptr.SetData(v,j);
         inc(i);


         if (i > hi) then break;
         v := ptr.GetData(i);
         j := i;
         while ( mainGtU (
                    ptr.GetData(j-h)+UInt32(d), v+UInt32(d), block, quadrant, nblock, budget
                 ) ) do
         begin
            ptr.SetData(ptr.GetData(j-h),j);
            j := j - h;
            if (j <= (lo + h - 1)) then break;
         end;
         ptr.SetData(v,j);
         inc(i);

         if (budget < 0) then exit;
      end;
      dec(hp);
   end;
end;



const MAIN_QSORT_SMALL_THRESH= 20;
const MAIN_QSORT_DEPTH_THRESH=(BZ_N_RADIX + BZ_N_QSORT);
const MAIN_QSORT_STACK_SIZE= 100;

{$WARNINGS OFF}
{$HINTS OFF}
procedure mainQSort3 ( ptr:TSBUInt32ArrayPointer;
                  block:TSBByteonUInt32Array;
                  quadrant:TSBUInt16onUInt32Array;
                  nblock,
                  loSt,
                  hiSt,
                  dSt:Int32;
                  var budget:Int32 );
var
   unLo, unHi, ltLo, gtHi, m, med,
   sp, lo_, hi_, d:Int32;
   n:Int32;

   stackLo:array [0..MAIN_QSORT_STACK_SIZE] of Int32;
   stackHi:array [0..MAIN_QSORT_STACK_SIZE] of Int32;
   stackD :array [0..MAIN_QSORT_STACK_SIZE] of Int32;

   nextLo:array [0..3] of Int32;
   nextHi:array [0..3] of Int32;
   nextD :array [0..3] of Int32;

   btmp1,btmp2,btmp3:byte;
   tmp1,tmp2,tmp3:Int32;


procedure mswap(var zz1, zz2:Int32);
var zztmp:Int32;
begin
  zztmp := zz1;
  zz1 := zz2;
  zz2 := zztmp;
end;

procedure mvswap(zzp1, zzp2, zzn:Int32);
var yyp1, yyp2, yyn:Int32;
        tmp1,tmp2:Int32;
begin
   yyp1 := (zzp1);
   yyp2 := (zzp2);
   yyn  := (zzn);
   while (yyn > 0) do
   begin
      tmp1:=ptr.GetData(yyp1);
      tmp2:=ptr.GetData(yyp2);
      mswap(tmp1, tmp2);
      ptr.SetData(tmp1,yyp1);
      ptr.SetData(tmp2,yyp2);
      inc(yyp1); inc(yyp2); dec(yyn);
   end;
end;

function mmed3 (a, b, c:Byte ):Byte;
var
   t:Byte;
begin
   if (a > b) then
   begin
    t := a;
    a := b;
    b := t;
   end;;
   if (b > c) then
   begin
      b := c;
      if (a > b) then b := a;
   end;
   Result:=b;
end;

function mmin(a,b:Int32):Int32;
begin
  //Result := {$ifndef DELPHI_NET}IfThen{$else}IfThenInt{$endif}((a) < (b),a,b);
  if a < b then 
    result := a 
  else 
    result := b;
end;

procedure mpush(lz,hz,dz:UInt32);
begin
  stackLo[sp] := lz;
  stackHi[sp] := hz;
  stackD[sp]:=dz;
  inc(sp);
end;

procedure mpop(var lz,hz,dz:Int32);
begin
  dec(sp);
  lz := stackLo[sp];
  hz := stackHi[sp];
  dz := stackD[sp];
end;


function mnextsize(az:UInt32):Int32;
begin
  Result:= (nextHi[az]-nextLo[az]);
end;

procedure mnextswap(az,bz:UInt32);
var tz:Int32;
begin

     tz := nextLo[az]; nextLo[az] := nextLo[bz]; nextLo[bz] := tz;
     tz := nextHi[az]; nextHi[az] := nextHi[bz]; nextHi[bz] := tz;
     tz := nextD [az]; nextD [az] := nextD [bz]; nextD [bz] := tz;
end;

begin
   sp := 0;
   mpush ( loSt, hiSt, dSt );

   while (sp > 0) do
   begin

      AssertH ( sp < MAIN_QSORT_STACK_SIZE, 1001 );

      mpop ( lo_, hi_, d );
      if ((hi_ - lo_) < MAIN_QSORT_SMALL_THRESH) or
          (d > MAIN_QSORT_DEPTH_THRESH) then
      begin
         mainSimpleSort ( ptr, block, quadrant, nblock, lo_, hi_, d, budget );
         if (budget < 0) then exit;
         continue;
      end;

      btmp1:=block.GetData(ptr.GetData(lo_)+d);
      btmp2:=block.GetData(ptr.GetData(hi_)+d);
      btmp3:=block.GetData(ptr.GetData((lo_+hi_) shr 1)+d);
      med :=mmed3 ( btmp1,btmp2,btmp3);
      block.SetData(btmp1,ptr.GetData(lo_)+d);
      block.SetData(btmp2,ptr.GetData(hi_)+d);
      block.SetData(btmp3,ptr.GetData((lo_+hi_) shr 1)+d);

      unLo := lo_;
      ltLo := lo_;
      unHi := hi_;
      gtHi := hi_;

      while (True) do
      begin
         while (True) do
         begin
            if (unLo > unHi) then break;
            n := (block.GetData(ptr.GetData(unLo)+d));
            n := n - med;
            if (n = 0) then
            begin
               tmp1:=ptr.GetData(unLo);
               tmp2:=ptr.GetData(ltLo);
               mswap(tmp1,tmp2);
               ptr.SetData(tmp1,unLo);
               ptr.SetData(tmp2,ltLo);
               inc(ltLo); inc(unLo);
               continue;
            end;;
            if (n >  0) then break;
            inc(unLo);
         end;
         while (True) do
         begin
            if (unLo > unHi) then break;
            n := (block.GetData(ptr.GetData(unHi)+d));
            n := n - med;
            if (n = 0) then
            begin
               tmp1:=ptr.GetData(unHi);
               tmp2:=ptr.GetData(gtHi);
               mswap(tmp1,tmp2);
               ptr.SetData(tmp1,unHi);
               ptr.SetData(tmp2,gtHi);

               dec(gtHi); dec(unHi);
               continue;
            end;
            if (n <  0) then break;
            dec(unHi);
         end;
         if (unLo > unHi) then break;
         tmp1:=ptr.GetData(unLo);
         tmp2:=ptr.GetData(unHi);
         mswap(tmp1,tmp2);
         ptr.SetData(tmp1,unLo);
         ptr.SetData(tmp2,unHi);

         inc(unLo); dec(unHi);
      end;

      AssertD ( unHi = unLo-1, 'mainQSort3(2)' );

      if (gtHi < ltLo) then
      begin
         mpush(lo_, hi_, d+1 );
         continue;
      end;

      n := mmin(ltLo - lo_, unLo - ltLo);
      mvswap(lo_, UInt32(unLo-n), n);
      m := mmin(hi_-gtHi, gtHi-unHi); mvswap(unLo, hi_-m+1, m);

      n := lo_ + unLo - ltLo - 1;
      m := hi_ - (gtHi - unHi) + 1;

      nextLo[0] := lo_;  nextHi[0] := n;   nextD[0] := d;
      nextLo[1] := m;   nextHi[1] := hi_;  nextD[1] := d;
      nextLo[2] := n+1; nextHi[2] := m-1; nextD[2] := d+1;

      if (mnextsize(0) < mnextsize(1)) then mnextswap(0,1);
      if (mnextsize(1) < mnextsize(2)) then mnextswap(1,2);
      if (mnextsize(0) < mnextsize(1)) then mnextswap(0,1);

      AssertD (mnextsize(0) >= mnextsize(1), 'mainQSort3(8)' );
      AssertD (mnextsize(1) >= mnextsize(2), 'mainQSort3(9)' );

      mpush (nextLo[0], nextHi[0], nextD[0]);
      mpush (nextLo[1], nextHi[1], nextD[1]);
      mpush (nextLo[2], nextHi[2], nextD[2]);
   end;
end;
{$WARNINGS ON}
{$HINTS ON}

const SETMASK= (1 shl 21);
const CLEARMASK = (not (SETMASK));

procedure mainSort ( ptr:TSBUInt32ArrayPointer;
                block:TSBByteonUInt32Array;
                quadrant:TSBUInt16onUInt32Array;
                ftab:TSBUInt32Array;
                nblock,
                verb:Int32;
                var budget:Int32 );
var
   i, j, k, ss, sb:Int32;
   runningOrder:array [0..255] of Int32;
   bigDone:array [0..255] of Boolean;
   copyStart:array [0..255] of Int32;
   copyEnd:array [0..255] of Int32;
   c1:Byte;
   numQSorted:Int32;
   s:UInt16;
   vv, h:Int32;
   bbStart,bbSize,shifts:Int32;
   a2update:Int32;
   qVal:UInt16;
   lo_, hi_:Int32;

function BIGFREQ(b:Int32):Int32;
begin
  Result:= (ftab.GetData(((b)+1) shl 8) - ftab.GetData((b) shl 8));
end;

begin
   if (verb >= 4) then Write (ErrOutput, '        main sort initialise ...' );

   for i := 65536 downto 0 do
      ftab.SetData(0,i);

   j := block.GetData(0) shl 8;
   i := nblock-1;
   while (i>=3) do
   begin
      quadrant.SetData(0,i);
      j := (j shr 8) or ( UInt16(block.GetData(i)) shl 8);
      ftab.SetData(ftab.GetData(j)+1,j);
      quadrant.SetData(0,i-1);
      j := (j shr 8) or ( UInt16(block.GetData(i-1)) shl 8);
      ftab.SetData(ftab.GetData(j)+1,j);
      quadrant.SetData(0,i-2);
      j := (j shr 8) or ( UInt16(block.GetData(i-2)) shl 8);
      ftab.SetData(ftab.GetData(j)+1,j);
      quadrant.SetData(0,i-3);
      j := (j shr 8) or ( UInt16(block.GetData(i-3)) shl 8);
      ftab.SetData(ftab.GetData(j)+1,j);
      i:=i-4;
   end;
   while (i >=0) do
   begin
      quadrant.SetData(0,i);
      j := (j shr 8) or ( UInt16(block.GetData(i)) shl 8);
      ftab.SetData(ftab.GetData(j)+1,j);
      dec(i)
   end;

   for i := 0 to BZ_N_OVERSHOOT-1 do
   begin
      block.SetData(block.GetData(i),nblock+i);
      quadrant.SetData(0,nblock+i);
   end;

   if (verb >= 4) then Write (ErrOutput, '        bucket sorting ...' );

   for i := 1 to  65536 do
      ftab.SetData(ftab.GetData(i) + ftab.GetData(i-1),i);

   s := block.GetData(0) shl 8;
   i := nblock-1;
   while (i>=3) do
   begin
      s := (s shr 8) or (block.GetData(i) shl 8);
      j := ftab.GetData(s) -1;
      ftab.SetData(j,s);
      ptr.SetData(i,j);
      s := (s shr 8) or (block.GetData(i-1) shl 8);
      j := ftab.GetData(s) -1;
      ftab.SetData(j,s);
      ptr.SetData(i-1,j);
      s := (s shr 8) or (block.GetData(i-2) shl 8);
      j := ftab.GetData(s) -1;
      ftab.SetData(j,s);
      ptr.SetData(i-2,j);
      s := (s shr 8) or (block.GetData(i-3) shl 8);
      j := ftab.GetData(s) -1;
      ftab.SetData(j,s);
      ptr.SetData(i-3,j);
      i:=i-4;
   end;
   while (i>=0) do
   begin
      s := (s shr 8) or (block.GetData(i) shl 8);
      j := ftab.GetData(s) -1;
      ftab.SetData(j,s);
      ptr.SetData(i,j);
      dec(i);
   end;

   for i := 0 to 255 do
   begin
      bigDone     [i] := False;
      runningOrder[i] := i;
   end;

      h := 1;
      repeat
          h := 3 * h + 1;
      until h > 256;
      repeat
         h := h div 3;
         for i := h to 255 do
         begin
            vv := runningOrder[i];
            j := i;
            while ( BIGFREQ(runningOrder[j-h]) > BIGFREQ(vv) ) do
            begin
               runningOrder[j] := runningOrder[j-h];
               j := j - h;
               if (j <= (h - 1)) then
                  break;
            end;
            runningOrder[j] := vv;
         end;
      until (h = 1);


   numQSorted := 0;

   for i := 0 to 255 do
   begin
      ss := runningOrder[i];

      for j := 0 to 255 do
      begin
         if (j <> ss) then
         begin
            sb := (ss shl 8) + j;
            if ( (ftab.GetData(sb) and  SETMASK)=0 ) then
            begin
               lo_ := ftab.GetData(sb);
               lo_ := lo_   and  CLEARMASK;
               hi_ := ftab.GetData(sb+1);
               hi_ := (hi_ and  CLEARMASK) - 1;
               if (hi_ > lo_) then
               begin
                  if (verb >= 4) then
                     Write (ErrOutput,Format('        qsort [0x%x, 0x%x]   '+
                                'done %d   this %d'+CRLF,
                                [ss, j, numQSorted, hi_ - lo_ + 1]) );
                  mainQSort3 (
                     ptr, block, quadrant, nblock,
                     lo_, hi_, BZ_N_RADIX, budget
                  );
                  numQSorted :=numQSorted+ (hi_ - lo_ + 1);
                  if (budget < 0) then
                      exit;
               end;
            end;
            ftab.SetData(ftab.GetData(sb) or  SETMASK,sb);
         end;
      end;

      AssertH ( not bigDone[ss], 1006 );

         for j := 0 to 255 do
         begin
            copyStart[j] :=  ftab.GetData((j shl 8) + ss);
            copyStart[j] :=  copyStart[j] and CLEARMASK;
            copyEnd  [j] :=  ftab.GetData((j shl 8) + ss + 1);
            copyEnd  [j] :=  (copyEnd  [j] and CLEARMASK) - 1;
         end;
         j := ftab.GetData(ss shl 8) and CLEARMASK;
         while j< copyStart[ss] do
         begin
            k := ptr.GetData(j);
            k:=k-1;
            if (k < 0) then  k := k + nblock;
            c1 := block.GetData(k);
            if (not bigDone[c1]) then
            begin
               ptr.SetData( k,copyStart[c1]);
               inc(copyStart[c1]);
            end;
            inc(j);
         end;
         j := (ftab.GetData((ss+1) shl 8) and CLEARMASK) - 1;
         while j> copyEnd[ss] do
         begin
            k := ptr.GetData(j);
            k:=k-1; if (k < 0) then k := k + nblock;
            c1 := block.GetData(k);
            if (not bigDone[c1]) then
            begin
               ptr.SetData(k,copyEnd[c1]);
               dec(copyEnd[c1]);
            end;
            dec(j);
         end;

      AssertH ( (copyStart[ss]-1 = copyEnd[ss])
                or
                ((copyStart[ss] = 0) and (copyEnd[ss] = nblock-1)),
                1007 );

      for j := 0 to 255 do
          ftab.SetData(ftab.GetData((j shl 8) + ss) or SETMASK,(j shl 8) + ss);


      bigDone[ss] := True;

      if (i < 255) then
      begin
         bbStart  := ftab.GetData(ss shl 8) and CLEARMASK;
         bbSize   := Int32(ftab.GetData((ss+1) shl 8) and CLEARMASK) - bbStart;
         shifts   := 0;

         while ((bbSize shr shifts) > 65534)  do
            inc(shifts);

         for j := bbSize-1 downto 0 do
         begin
            a2update     := ptr.GetData(bbStart + j);
            qVal        := (j shr shifts);
            quadrant.SetData(qVal,a2update);
            if (a2update < BZ_N_OVERSHOOT) then
               quadrant.SetData(qVal,a2update + nblock);
         end;
         AssertH ( ((bbSize-1) shr shifts) <= 65535, 1002 );
      end;
   end;
   if (verb >= 4) then
      Write (ErrOutput, Format('        %d pointers, %d sorted, %d scanned',
                 [nblock, numQSorted, nblock - numQSorted]) );
end;

procedure BZ2_blockSort ( s:EState );
var
   ptr:TSBUInt32ArrayPointer;
   block:TSBByteonUInt32Array;
   ftab:TSBUInt32Array;
   nblock,
   verb,
   wfact:Int32;
   quadrant:TSBUInt16onUInt32Array;
   budget,
   budgetInit,
   i:Int32;

begin
   ptr    := s.ptr;
   block  := s.block;
   ftab   := s.ftab;
   nblock := s.nblock;
   verb   := s.verbosity;
   wfact  := s.workFactor;
   quadrant := TSBUInt16onUInt32Array.Create(block.Base_Array,'block.Base_Array');

   if (nblock < 10000) then
      fallbackSort ( s.arr1, s.arr2, ftab, nblock, verb )
   else
   begin
      i := nblock+BZ_N_OVERSHOOT;
      if Odd(i) then
        inc(i);

      quadrant.Offset:=i;

      if (wfact < 1  ) then
        wfact := 1;
      if (wfact > 100) then
        wfact := 100;
      budgetInit := nblock * ((wfact-1) div 3);
      budget := budgetInit;


      mainSort ( ptr, block, quadrant, ftab, nblock, verb, budget );
      if (verb >= 3) then
         Write (ErrOutput, Format('      %d work, %d block, ratio %5.2f',
                    [budgetInit - budget,
                    nblock,
                    (budgetInit - budget) /
                    (IfThenInt(nblock=0, 1, nblock))]) );
      if (budget < 0) then
      begin
         if (verb >= 2) then
            Write (ErrOutput, '    too repetitive; using fallback'+
                       ' sorting algorithm' );
         fallbackSort ( s.arr1, s.arr2, ftab, nblock, verb );
      end;
   end;

   s.origPtr := -1;
   for i := 0 to s.nblock-1 do
      if (ptr.GetData(i) = 0) then
      begin
         s.origPtr := i;
         break;
      end;

   FreeAndNil(quadrant);
   AssertH( s.origPtr <> -1, 1003 );
end;

////////////////////////////////////////////////////////////////////////////////
// Huffman core

procedure BZ2_hbMakeCodeLengths (len: TSBByteArray;
          freq:TSBInt32Array;
          alphaSize:Int32;
          maxLen:Int32);

var    nNodes, nHeap, n1, n2, i, j, k:Int32;
       tooLong:Boolean;
   heap: array[0.. BZ_MAX_ALPHA_SIZE + 2 ] of Int32;
   weight: array[0.. BZ_MAX_ALPHA_SIZE * 2 ] of Int32;
   parent: array[0.. BZ_MAX_ALPHA_SIZE * 2 ] of Int32;


        function WEIGHTOF(zz0:UInt32):UInt32;
        begin
          Result:=((zz0) and $ffffff00);
        end;


        function DEPTHOF(zz1:UInt32):UInt32;
        begin
          Result:=((zz1) and $000000ff);
        end;

        function MYMAX(zz2,zz3:UInt32):UInt32;
        begin
          Result:=IfThenLongword(zz2 > zz3, zz2, zz3);
        end;

        function ADDWEIGHTS(zw1,zw2:UInt32):UInt32;
        begin
          Result:=(WEIGHTOF(zw1)+WEIGHTOF(zw2)) or
               (1 + MYMAX(DEPTHOF(zw1),DEPTHOF(zw2)));
        end;

        procedure UPHEAP(z:Int32);
        var zz, tmp:Int32;
        begin

           zz := z;
           tmp := heap[zz];
           while (weight[tmp] < weight[heap[zz shr 1]]) do
           begin
              heap[zz] := heap[zz shr 1];
              zz := zz shr 1;
           end;
           heap[zz] := tmp;
        end;

        procedure DOWNHEAP(z:Int32);
        var zz, yy, tmp:Int32;
           While_Flag:Boolean;
        begin
           zz := z;
           tmp := heap[zz];
           While_Flag:=True;
           while (While_Flag and ((zz shl 1) <= nHeap)) do
           begin
              yy := zz shl 1;
              if (yy < nHeap) and (weight[heap[yy+1]] < weight[heap[yy]]) then
                 inc(yy);
              if (weight[tmp] < weight[heap[yy]]) then
                While_Flag := False
              else
              begin
                heap[zz] := heap[yy];
                zz := yy;
              end;
           end;
           heap[zz] := tmp;
        end;


begin
   {
      Nodes and heap entries run from 1.  Entry 0
      for both the heap and nodes is a sentinel.
   }


   for i := 0 to alphaSize - 1 do
      weight[i+1] := IfThenInt(freq.GetData(i) = 0, 1, freq.GetData(i)) shl 8;

   while True do
   begin

      nNodes := alphaSize;
      nHeap := 0;

      heap[0] := 0;
      weight[0] := 0;
      parent[0] := -2;

      for i := 1 to alphaSize do
      begin
         parent[i] := -1;
         inc(nHeap);
         heap[nHeap] := i;
         UPHEAP(nHeap);
      end;

      AssertH( nHeap < (BZ_MAX_ALPHA_SIZE+2), 2001 );

      while (nHeap > 1) do
      begin
         n1 := heap[1]; heap[1] := heap[nHeap]; dec(nHeap); DOWNHEAP(1);
         n2 := heap[1]; heap[1] := heap[nHeap]; dec(nHeap); DOWNHEAP(1);
         inc(nNodes);
         parent[n1] := nNodes;
         parent[n2] := nNodes;
         weight[nNodes] := ADDWEIGHTS(weight[n1], weight[n2]);
         parent[nNodes] := -1;
         inc(nHeap);
         heap[nHeap] := nNodes;
         UPHEAP(nHeap);
      end;

      AssertH( nNodes < (BZ_MAX_ALPHA_SIZE * 2), 2002 );

      tooLong := False;
      for i := 1 to alphaSize do
      begin
         j := 0;
         k := i;
         while (parent[k] >= 0) do
         begin
            k := parent[k];
            inc(j);
         end;
         len.SetData(j,i-1);
         if (j > maxLen) then
            tooLong := True;
      end;

      if (not tooLong) then break;
      { 17 Oct 04: keep-going condition for the following loop used
         to be 'i < alphaSize', which missed the last element,
         theoretically leading to the possibility of the compressor
         looping.  However, this count-scaling step is only needed if
         one of the generated Huffman code words is longer than
         maxLen, which up to and including version 1.0.2 was 20 bits,
         which is extremely unlikely.  In version 1.0.3 maxLen was
         changed to 17 bits, which has minimal effect on compression
         ratio, but does mean this scaling step is used from time to
         time, enough to verify that it works.

         This means that bzip2-1.0.3 and later will only produce
         Huffman codes with a maximum length of 17 bits.  However, in
         order to preserve backwards compatibility with bitstreams
         produced by versions pre-1.0.3, the decompressor must still
         handle lengths of up to 20. }

      for i := 1 to alphaSize do
      begin
         j := weight[i] shr 8;
         j := 1 + (j div 2);
         weight[i] := j shl 8;
      end;
   end;
end;


procedure BZ2_hbAssignCodes ( code:TSBInt32Array;
                         length:TSBByteArray;
                         minLen,maxLen:Int32;
                         alphaSize:Int32 );
Var n, vec, i:Int32;
begin
   vec := 0;
   for n := minLen to maxLen do
   begin
      for i := 0 to alphaSize-1 do
         if (length.GetData(i) = n) then
         begin
            code.SetData(vec,i);
            inc(vec);
         end;
      vec := vec shl 1;
   end;
end;


procedure BZ2_hbCreateDecodeTables ( limit, base, perm :TSBInt32Array;
                                length: TSBByteArray;
                                minLen,maxLen: Int32;
                                alphaSize: Int32);

Var pp, i, j, vec:Int32;
begin

   pp := 0;
   for i := minLen to maxLen do
      for j := 0 to alphaSize-1 do
         if (length.GetData(j) = i) then
         begin
            perm.SetData(j,pp);
            inc(pp);
         end;

   for i := 0 to BZ_MAX_CODE_LEN-1 do
      base.SetData(0,i);
   for i := 0 to alphaSize-1 do
   begin
      j:=base.GetData(length.GetData(i)+1);
      base.SetData(j+1,length.GetData(i)+1);
   end;

   for i := 1 to BZ_MAX_CODE_LEN do
      base.SetData(base.GetData(i) + base.GetData(i-1),i);

   for i := 0 to BZ_MAX_CODE_LEN -1 do
      limit.SetData(0,i);

   vec := 0;

   for i := minLen to maxLen do
   begin
      vec :=  vec + (base.GetData(i+1) - base.GetData(i));
      limit.SetData(vec-1,i);
      vec := vec shl 1;
   end;
   for i := minLen + 1 to maxLen do
      base.SetData(((limit.GetData(i-1) + 1) shl 1) - base.GetData(i),i);
end;

////////////////////////////////////////////////////////////////////////////////
// Compression machinery

procedure BZ2_bsInitWrite ( s:EState);
begin
   s.bsLive := 0;
   s.bsBuff := 0;
end;           

procedure bsFinishWrite ( s:EState);
begin
   while (s.bsLive > 0) do
   begin
      s.zbits.SetData(s.bsBuff shr 24,s.numZ);
      inc(s.numZ);
      s.bsBuff :=s.bsBuff shl 8;
      s.bsLive := s.bsLive - 8;
   end;
end;


procedure bsW ( s:EState; n:Int32; v:UInt32);
begin
   while (s.bsLive >= 8) do
   begin
      s.zbits.SetData(s.bsBuff shr 24,s.numZ);
      inc(s.numZ);
      s.bsBuff :=s.bsBuff shl 8;
      s.bsLive := s.bsLive - 8;
   end;
   s.bsBuff := s.bsBuff or  (v shl (32 - s.bsLive - n));
   s.bsLive := s.bsLive + n;
end;



procedure bsPutUInt32 ( s:EState; u:UInt32);
begin
   bsW ( s, 8, (u shr 24) and $ff);
   bsW ( s, 8, (u shr 16) and $ff);
   bsW ( s, 8, (u shr  8) and $ff);
   bsW ( s, 8,  u        and $ff);
end;



procedure bsPutUChar ( s:EState; c:BYte);
begin
   bsW( s, 8, UInt32(c) );
end;


procedure makeMaps_e ( s:EState );
var i:Int32;
begin
   s.nInUse := 0;
   for i := 0 to 255 do
      if (s.inUse[i]) then
      begin
         s.unseqToSeq[i] := s.nInUse;
         inc(s.nInUse);
      end;
end;


procedure generateMTFValues ( s:EState );
var
   yy:TSBByteArray;
   i, j:Int32;
   zPend:Int32;
   wr:Int32;
   EOB:Int32;

   {
      After sorting (eg, here),
         s->arr1 [ 0 .. s->nblock-1 ] holds sorted order,
         and
         ((UChar*)s->arr2) [ 0 .. s->nblock-1 ]
         holds the original block data.

      The first thing to do is generate the MTF values,
      and put them in
         ((UInt16*)s->arr1) [ 0 .. s->nblock-1 ].
      Because there are strictly fewer or equal MTF values
      than block values, ptr values in this area are overwritten
      with MTF values only when they are no longer needed.

      The final compressed bitstream is generated into the
      area starting at
         (UChar*) (&((UChar*)s->arr2)[s->nblock])

      These storage aliases are set up in bzCompressInit(),
      except for the last one, which is arranged in
      compressBlock().
   }
   ptr   :TSBUInt32ArrayPointer;
   block :TSBByteonUInt32Array;
   mtfv  :TSBUInt16onUInt32Array;
   ll_i: Byte;
   
   rtmp:Byte;
   ryy_j:TSBByteArrayPointer;
   rll_i:Byte;
   rtmp2:Byte;
begin
   ptr   := s.ptr;
   block  := s.block;
   mtfv  := s.mtfv;

   yy := TSBByteArray.Create(257);
   ryy_j:=TSBByteArrayPointer.Create(yy,'yy');
   makeMaps_e ( s );
   EOB := s.nInUse+1;

   for i := 0 to EOB do
      s.mtfFreq[i] := 0;

   wr := 0;
   zPend := 0;
   for i := 0 to s.nInUse-1 do
      yy.SetData(i,i);

   for i := 0 to s.nblock-1 do
   begin
      AssertD ( wr <= i, 'generateMTFValues(1)');
      j := ptr.GetData(i);
      j:=j-1;
      if (j < 0) then
        j:= j + s.nblock;
      ll_i := s.unseqToSeq[block.GetData(j)];
      AssertD ( Integer(ll_i) < s.nInUse, 'generateMTFValues(2a)' );

      if (yy.GetData(0) = ll_i) then
         inc(zPend)
      else
      begin
         if (zPend > 0) then
         begin
            dec(zPend);
            while (True) do
            begin
               if Odd(zPend) then
               begin
                  mtfv.SetData(BZ_RUNB,wr);
                  inc(wr);
                  inc(s.mtfFreq[BZ_RUNB]);
               end
               else
               begin
                  mtfv.SetData(BZ_RUNA,wr);
                  inc(wr);
                  inc(s.mtfFreq[BZ_RUNA]);
               end;
               if (zPend < 2) then
                  break;
               zPend := (zPend - 2) div 2;
            end;
            zPend := 0;
         end;
         rtmp  := yy.GetData(1);
         yy.SetData(yy.GetData(0),1);
         ryy_j.Offset:=1;
         rll_i := ll_i;
         while ( rll_i <> rtmp ) do
         begin
            ryy_j.IncOffset(1);
            rtmp2  := rtmp;
            rtmp   := ryy_j.GetAtPtr;
            ryy_j.SetAtPtr(rtmp2);
         end;
         yy.SetData(rtmp,0);
         j := ryy_j.Offset;
         mtfv.SetData(j+1,wr);
         inc(wr);
         inc(s.mtfFreq[j+1]);
      end;
   end;

   if (zPend > 0) then
   begin
      dec(zPend);
      while (True) do
      begin
         if Odd(zPend) then
         begin
            mtfv.SetData(BZ_RUNB,wr);
            inc(wr);
            inc(s.mtfFreq[BZ_RUNB]);
         end
         else
         begin
            mtfv.SetData(BZ_RUNA,wr);
            inc(wr);
            inc(s.mtfFreq[BZ_RUNA]);
         end;
         if (zPend < 2) then
            break;
         zPend := (zPend - 2) div 2;
      end;
      //zPend := 0;
   end;

   mtfv.SetData(EOB,wr);
   inc(wr);
   inc(s.mtfFreq[EOB]);

   s.nMTF := wr;

   FreeAndNil(yy);
   FreeAndNil(ryy_j);
end;

procedure sendMTFValues ( s:EState );
var
   v, t, i, j, gs, ge, totc, bt, bc, iter:Int32;
   nSelectors, alphaSize, minLen, maxLen, selCtr:Int32;
   nGroups, nBytes:Int32;

   {
   UChar  len [BZ_N_GROUPS][BZ_MAX_ALPHA_SIZE];
   is a global since the decoder also needs it.

   Int32  code[BZ_N_GROUPS][BZ_MAX_ALPHA_SIZE];
   Int32  rfreq[BZ_N_GROUPS][BZ_MAX_ALPHA_SIZE];
   are also globals only used in this proc.
   Made global to keep stack frame size small.
   -}


   cost:array [0..BZ_N_GROUPS] of UInt16;
   fave:array [0..BZ_N_GROUPS] of Int32;
   mtfv:TSBUInt16onUInt32Array;
   nPart, remF, tFreq, aFreq:Int32;

   cost01, cost23, cost45:UInt32;
   icv:UInt16;
   pos: array [0..BZ_N_GROUPS] of Byte;
   ll_i, tmp2, tmp:Byte;
   inUse16:array [0..16] of Boolean;

    mtfv_i:UInt16;
    s_len_sel_selCtr:TSBByteArray;
    s_code_sel_selCtr:TSBInt32Array;

    curr:Int32;

procedure BZ_ITER(nn:Integer);
begin
   icv := mtfv.GetData(gs+nn);
   cost01 := cost01 + s.len_pack[icv,0];
   cost23 := cost23 + s.len_pack[icv,1];
   cost45 := cost45 + s.len_pack[icv,2];
end;

procedure BZ_ITUR(nn:integer);
begin
  s.rfreq[bt].SetData(s.rfreq[bt].GetData(mtfv.GetData(gs+nn))+1,mtfv.GetData(gs+nn));
end;

procedure BZ_ITAH(nn:integer);
begin
   mtfv_i := mtfv.GetData(gs+nn);
   bsW ( s,
         s_len_sel_selCtr.GetData(mtfv_i),
         s_code_sel_selCtr.GetData(mtfv_i) );
end;

begin
   mtfv :=s.mtfv;

   if (s.verbosity >= 3) then
      Write(ErrOutput, Format('      %d in block, %d after MTF & 1-2 coding, '+
                '%d+2 syms in use'+CRLF,
                [s.nblock, s.nMTF, s.nInUse]) );

   alphaSize := s.nInUse+2;
   for t := 0 to BZ_N_GROUPS - 1 do
      for v := 0 to alphaSize - 1 do
         s.len[t].SetData(BZ_GREATER_ICOST,v);

   AssertH ( s.nMTF > 0, 3001 );
   if (s.nMTF < 200) then
      nGroups := 2
   else
   if (s.nMTF < 600) then
      nGroups := 3
   else
   if (s.nMTF < 1200) then
      nGroups := 4
   else
   if (s.nMTF < 2400) then
      nGroups := 5
   else
      nGroups := 6;

    nPart := nGroups;
    remF  := s.nMTF;
    gs := 0;
    while (nPart > 0) do
    begin
       tFreq := remF div nPart;
       ge := gs-1;
       aFreq := 0;
       while (aFreq < tFreq) and (ge < (alphaSize-1)) do
       begin
          inc(ge);
          aFreq := aFreq + s.mtfFreq[ge];
       end;

       if (ge > gs) and
           (nPart <> nGroups) and (nPart <> 1)
           and (((nGroups- nPart) mod 2) = 1) then
       begin
          aFreq := aFreq- s.mtfFreq[ge];
          dec(ge);
       end;

       if (s.verbosity >= 3) then
          Write(ErrOutput, Format('      initial group %d, [%d .. %d], '+
                    'has %d syms (%4.1f%%)'+CRLF,
                    [nPart, gs, ge, aFreq,
                    (100.0 * aFreq) / s.nMTF]));

       for v := 0 to alphaSize-1 do
          if (v >= gs) and (v <= ge) then
             s.len[nPart-1].SetData(BZ_LESSER_ICOST, v)
          else
             s.len[nPart-1].SetData(BZ_GREATER_ICOST,v);

       dec(nPart);
       gs := ge+1;
       remF := remF-aFreq;
    end;


   for iter := 0 to BZ_N_ITERS-1 do
   begin
      for t := 0 to nGroups-1 do
        fave[t] := 0;

      for t := 0 to nGroups-1 do
         for v := 0 to alphaSize-1 do
            s.rfreq[t].SetData(0,v);

      if (nGroups = 6) then
         for v := 0 to alphaSize-1 do
         begin
            s.len_pack[v,0] := (s.len[1].GetData(v) shl 16) or (s.len[0].GetData(v));
            s.len_pack[v,1] := (s.len[3].GetData(v) shl 16) or (s.len[2].GetData(v));
            s.len_pack[v,2] := (s.len[5].GetData(v) shl 16) or (s.len[4].GetData(v));
         end;

      nSelectors := 0;
      totc := 0;
      gs := 0;
      while (True) do
      begin
         if (gs >= s.nMTF) then break;
         ge := gs + BZ_G_SIZE - 1;
         if (ge >= s.nMTF) then
            ge := s.nMTF-1;

         for t := 0 to nGroups-1 do
            cost[t] := 0;

         if (nGroups = 6) and (50 = (ge-gs+1)) then
         begin
            cost01 := 0;
            cost23 := 0;
            cost45 := 0;

            for t := 0 to 49 do
                BZ_ITER(t);

            cost[0] := cost01 and $ffff; cost[1] := cost01 shr 16;
            cost[2] := cost23 and $ffff; cost[3] := cost23 shr 16;
            cost[4] := cost45 and $ffff; cost[5] := cost45 shr 16;
         end
         else
         begin
            for i := gs to ge do
            begin
               icv := mtfv.GetData(i);
               for t := 0 to nGroups-1 do
                  cost[t] := cost[t] + s.len[t].GetData(icv);
            end;
         end;

         bc := 999999999;
         bt := -1;
         for t := 0 to nGroups-1 do
            if (Integer(cost[t]) < bc) then
            begin
              bc := cost[t];
              bt := t;
            end;
         totc := totc + bc;
         inc(fave[bt]);
         s.selector[nSelectors] := bt;
         inc(nSelectors);

         if (nGroups = 6) and (50 = (ge-gs+1)) then
            for t := 0 to 49 do
                    BZ_ITUR(t)
         else
            for i := gs to ge do
               s.rfreq[bt].SetData(s.rfreq[bt].GetData(mtfv.GetData(i))+1,mtfv.GetData(i));
         gs := ge+1;
      end;
      if (s.verbosity >= 3) then
      begin
         Write (ErrOutput,Format('      pass %d: size is %d, grp uses are ',
                   [iter+1, totc div 8]) );
         for t := 0 to nGroups-1 do
            Write(ErrOutput,Format('%d ', [fave[t]]) );
         Write (ErrOutput,CRLF);
      end;

      for t := 0 to nGroups-1 do
         BZ2_hbMakeCodeLengths( s.len[t], s.rfreq[t],
                                 alphaSize, 17 {20} );
   end;


   AssertH( nGroups < 8, 3002 );
   AssertH( (nSelectors < 32768) and
            (nSelectors <= (2 + (900000 / BZ_G_SIZE))),
            3003 );



    for i := 0 to nGroups-1 do
      pos[i] := i;
    for i := 0 to nSelectors-1 do
    begin
       ll_i := s.selector[i];
       j := 0;
       tmp := pos[j];
       while ( ll_i <> tmp ) do
       begin
          inc(j);
          tmp2 := tmp;
          tmp := pos[j];
          pos[j] := tmp2;
       end;
       pos[0] := tmp;
       s.selectorMtf[i] := j;
    end;


   for t := 0 to nGroups-1 do
   begin
      minLen := 32;
      maxLen := 0;
      for i := 0 to alphaSize-1 do
      begin
         if (Integer(s.len[t].GetData(i)) > maxLen) then
            maxLen := s.len[t].GetData(i);
         if (Integer(s.len[t].GetData(i)) < minLen) then
            minLen := s.len[t].GetData(i);
      end;
      AssertH ( not (maxLen > 17 {20} ), 3004 );
      AssertH ( not (minLen < 1),  3005 );
      BZ2_hbAssignCodes ( s.code[t], s.len[t],
                          minLen, maxLen, alphaSize );
   end;


      for i := 0 to 15 do
      begin
          inUse16[i] := False;
          for j := 0 to 15 do
             if (s.inUse[i * 16 + j]) then
                inUse16[i] := True;
      end;

      nBytes := s.numZ;
      for i := 0 to 15 do
         if (inUse16[i]) then
            bsW(s,1,1)
         else
            bsW(s,1,0);

      for i := 0 to 15 do
         if (inUse16[i]) then
            for j := 0 to 15 do
               if (s.inUse[i * 16 + j]) then
                  bsW(s,1,1)
               else
                  bsW(s,1,0);


      if (s.verbosity >= 3) then
         Write(ErrOutput,Format('      bytes: mapping %d, ', [s.numZ-nBytes]) );

   nBytes := s.numZ;
   bsW ( s, 3, nGroups );
   bsW ( s, 15, nSelectors );
   for i := 0 to nSelectors-1 do
   begin
      for j := 0 to s.selectorMtf[i]-1 do
        bsW(s,1,1);
      bsW(s,1,0);
   end;
   if (s.verbosity >= 3) then
      Write(ErrOutput,Format( 'selectors %d, ', [s.numZ-nBytes] ));

   nBytes := s.numZ;

   for t := 0 to nGroups-1 do
   begin
      curr := s.len[t].GetData(0);
      bsW ( s, 5, curr );
      for i := 0 to alphaSize-1 do
      begin
         while (curr < Integer(s.len[t].GetData(i))) do
         begin
          bsW(s,2,2);
          inc(curr);
         end;
         while (curr > Integer(s.len[t].GetData(i))) do
         begin
          bsW(s,2,3);
          dec(curr);
         end;
         bsW ( s, 1, 0 );
      end;
   end;

   if (s.verbosity >= 3) then
      Write (ErrOutput, Format('code lengths %d, ', [s.numZ-nBytes]) );

   nBytes := s.numZ;
   selCtr := 0;
   gs := 0;
   while (True) do
   begin
      if (gs >= s.nMTF) then
        break;
      ge := gs + BZ_G_SIZE - 1;
      if (ge >= s.nMTF) then
        ge := s.nMTF-1;
      AssertH ( Integer(s.selector[selCtr]) < nGroups, 3006 );

      if (nGroups = 6) and (50 = ge-gs+1) then
      begin
            s_len_sel_selCtr := s.len[s.selector[selCtr]];
            s_code_sel_selCtr := s.code[s.selector[selCtr]];

            for t:=0 to 49 do
                BZ_ITAH(t);

      end
      else
         for i := gs to ge do
            bsW ( s,
                  s.len  [s.selector[selCtr]].GetData(mtfv.GetData(i)),
                  s.code [s.selector[selCtr]].GetData(mtfv.GetData(i)));

      gs := ge+1;
      inc(selCtr);
   end;
   AssertH( selCtr = nSelectors, 3007 );

   if (s.verbosity >= 3) then
      Write(ErrOutput, Format('codes %d'+CRLF, [s.numZ-nBytes]) );
end;


procedure BZ2_compressBlock ( s:EState; is_last_block:Boolean );
begin
   if (s.nblock > 0) then
   begin
      // BZ_FINALISE_CRC
      s.blockCRC := not s.blockCRC;
      s.combinedCRC := (s.combinedCRC shl 1) or (s.combinedCRC shr 31);
      s.combinedCRC := s.combinedCRC xor s.blockCRC;
      if (s.blockNo > 1) then
          s.numZ := 0;

      if (s.verbosity >= 2) then
         Write(ErrOutput, Format('    block %d: crc = 0x%08x, '+
                   'combined CRC = 0x%08x, size = %d'+CRLF,
                   [s.blockNo, s.blockCRC, s.combinedCRC, s.nblock]) );

      BZ2_blockSort ( s );
   end;

{   s.zbits := PBYte(LongWord(s.arr2)+s.nblock*SizeOf(s.arr2^)); }
   if s.zbits<>nil then
        s.zbits.Free;

   s.zbits:=TSBByteonUInt32Array.Create(s.arr2,'s.arr2');
   s.zbits.Offset:=s.nblock;

   if (s.blockNo = 1) then
   begin
      BZ2_bsInitWrite ( s );
      bsPutUChar ( s, BZ_HDR_B );
      bsPutUChar ( s, BZ_HDR_Z );
      bsPutUChar ( s, BZ_HDR_h );
      bsPutUChar ( s, Byte(BZ_HDR_0 + s.blockSize100k));
   end;

   if (s.nblock > 0) then
   begin

      bsPutUChar ( s, $31 ); bsPutUChar ( s, $41 );
      bsPutUChar ( s, $59 ); bsPutUChar ( s, $26 );
      bsPutUChar ( s, $53 ); bsPutUChar ( s, $59 );

      bsPutUInt32 ( s, s.blockCRC );

      bsW(s,1,0);

      bsW ( s, 24, s.origPtr );
      generateMTFValues ( s );
      sendMTFValues ( s );
   end;

   if (is_last_block) then
   begin
      bsPutUChar ( s, $17 ); bsPutUChar ( s, $72 );
      bsPutUChar ( s, $45 ); bsPutUChar ( s, $38 );
      bsPutUChar ( s, $50 ); bsPutUChar ( s, $90 );
      bsPutUInt32 ( s, s.combinedCRC );
      if (s.verbosity >= 2) then
         Write(ErrOutput, Format('    final combined CRC = 0x%08x'+CRLF, [s.combinedCRC]) );
      bsFinishWrite ( s );
   end;
end;

////////////////////////////////////////////////////////////////////////////////
// Decompression machinery

procedure BZ_FINALISE_CRC(var crcVar:MaybeUInt64);
begin
   crcVar := -crcVar;
end;

procedure BZ_UPDATE_CRC(var crcVar:MaybeUInt64; cha:Byte);
begin
   crcVar := (crcVar shl 8) xor
            BZ2_crc32Table[(crcVar shr 24) xor
                           cha];

end;

procedure makeMaps_d ( s:DState );
var
   i:Int32;
begin
   s.nInUse := 0;
   for i := 0 to 255 do
      if (s.inUse[i]) then
      begin
         s.seqToUnseq[s.nInUse] := i;
         inc(s.nInUse);
      end;
end;

{$WARNINGS OFF}
{$HINTS OFF}
function BZ2_decompress ( s:DState ):Int32;
var
   uc:Byte;
   retVal:Int32;
   minLen, maxLen:Int32;
   strm :bz_stream;


   i,
   j,
   t,
   alphaSize,
   nGroups,
   nSelectors,
   EOB,
   groupNo,
   groupPos,
   nextSym,
   nblockMAX,
   nblock,
     es,
     N,
     curr,
     zt,
     zn,
     zvec,
     zj,
     gSel,
     gMinlen:Int32;
   gBase,gLimit,
   gPerm:TSBInt32Array;

   pos:array [0..BZ_N_GROUPS] of Byte;
   tmp, v:Int32;
   ii, jj, kk, lno, off:Int32;
   nn:INt32;
   z,pp:Int32;
   vv,tmpv:UInt32;
   Res:integer;

{
label save_state_and_return, endhdr_2;
label label_BZ_X_MAGIC_1, label_BZ_X_MAGIC_2, label_BZ_X_MAGIC_3, label_BZ_X_MAGIC_4,
label_BZ_X_BLKHDR_1, label_BZ_X_BLKHDR_2, label_BZ_X_BLKHDR_3, label_BZ_X_BLKHDR_4,
label_BZ_X_BLKHDR_5, label_BZ_X_BLKHDR_6, label_BZ_X_BCRC_1, label_BZ_X_BCRC_2,
label_BZ_X_BCRC_3, label_BZ_X_BCRC_4, label_BZ_X_RANDBIT, label_BZ_X_ORIGPTR_1,
label_BZ_X_ORIGPTR_2, label_BZ_X_ORIGPTR_3, label_BZ_X_MAPPING_1, label_BZ_X_MAPPING_2,
label_BZ_X_SELECTOR_1, label_BZ_X_SELECTOR_2, label_BZ_X_SELECTOR_3, label_BZ_X_CODING_1,
label_BZ_X_CODING_2, label_BZ_X_CODING_3, label_BZ_X_MTF_1, label_BZ_X_MTF_2,
label_BZ_X_MTF_3, label_BZ_X_MTF_4, label_BZ_X_MTF_5, label_BZ_X_MTF_6,
label_BZ_X_ENDHDR_2, label_BZ_X_ENDHDR_3, label_BZ_X_ENDHDR_4, label_BZ_X_ENDHDR_5,
label_BZ_X_ENDHDR_6, label_BZ_X_CCRC_1, label_BZ_X_CCRC_2, label_BZ_X_CCRC_3,
label_BZ_X_CCRC_4;
}


function GET_BITS(lll:Int32;var vvv:UInt32;nnn:Byte):Integer; overload;
var Res:integer;
begin
   Res:=BZ_NULL;
   s.state := lll;
   while True do
   begin
      if (s.bsLive >= Integer(nnn)) then
      begin
         vvv := (s.bsBuff shr
             (s.bsLive-nnn)) and ((1 shl nnn)-1);
         s.bsLive := s.bsLive - nnn;
	 break;
      end;
      if (s.strm.avail_in = 0) then
      begin
          Res := BZ_OK;
          break;
      end
      else
      begin
          s.bsBuff := (s.bsBuff shl 8) or
                  s.strm.next_in.GetAtPtr;
          s.bsLive := s.bsLive +8;
          s.strm.next_in.IncOffset(1);
          s.strm.avail_in:=s.strm.avail_in-1;
          s.strm.total_in_lo32:=s.strm.total_in_lo32+1;
          if (s.strm.total_in_lo32 = 0) then
             s.strm.total_in_hi32:=s.strm.total_in_hi32+1;
      end;
   end;
   Result:=Res;
end;

function GET_BITS2(lll:Int32;var vvv:Int32;nnn:Byte):Integer; overload;
var Res:integer;
begin
   Res:=BZ_NULL;
   s.state := lll;
   while True do
   begin
      if (s.bsLive >= Integer(nnn)) then
      begin
         vvv := (s.bsBuff shr
             (s.bsLive-nnn)) and ((1 shl nnn)-1);
         s.bsLive := s.bsLive - nnn;
	 break;
      end;
      if (s.strm.avail_in = 0) then
      begin
          Res := BZ_OK;
          break;
      end
      else
      begin
          s.bsBuff := (s.bsBuff shl 8) or
                  s.strm.next_in.GetAtPtr;
          s.bsLive := s.bsLive +8;
          s.strm.next_in.IncOffset(1);
          s.strm.avail_in:=s.strm.avail_in-1;
          s.strm.total_in_lo32:=s.strm.total_in_lo32+1;
          if (s.strm.total_in_lo32 = 0) then
             s.strm.total_in_hi32:=s.strm.total_in_hi32+1;
      end;
   end;
   Result:=Res;
end;

function GET_BITS3(lll:Int32;var vvv:Byte;nnn:Byte):Integer; overload;
var Res:integer;
begin
   Res:=BZ_NULL;
   s.state := lll;
   while True do
   begin
      if (s.bsLive >= Integer(nnn)) then
      begin
         vvv := (s.bsBuff shr
             (s.bsLive-nnn)) and ((1 shl nnn)-1);
         s.bsLive := s.bsLive - nnn;
	 break;
      end;
      if (s.strm.avail_in = 0) then
      begin
          Res := BZ_OK;
          break;
      end
      else
      begin
          s.bsBuff := (s.bsBuff shl 8) or
                  s.strm.next_in.GetAtPtr;
          s.bsLive := s.bsLive +8;
          s.strm.next_in.IncOffset(1);
          s.strm.avail_in:=s.strm.avail_in-1;
          s.strm.total_in_lo32:=s.strm.total_in_lo32+1;
          if (s.strm.total_in_lo32 = 0) then
             s.strm.total_in_hi32:=s.strm.total_in_hi32+1;
      end;
   end;
   Result:=Res;
end;

function GET_BITS4(lll:Int32;var vvv:Boolean;nnn:Byte):Integer; overload;
var Res:integer;
begin
   Res:=BZ_NULL;
   s.state := lll;
   while True do
   begin
      if (s.bsLive >= Integer(nnn)) then
      begin
         vvv := ((s.bsBuff shr
             (s.bsLive-nnn)) and ((1 shl nnn)-1))<>0;
         s.bsLive := s.bsLive - nnn;
	 break;
      end;
      if (s.strm.avail_in = 0) then
      begin
          Res := BZ_OK;
          break;
      end
      else
      begin
          s.bsBuff := (s.bsBuff shl 8) or
                  s.strm.next_in.GetAtPtr;
          s.bsLive := s.bsLive +8;
          s.strm.next_in.IncOffset(1);
          s.strm.avail_in:=s.strm.avail_in-1;
          s.strm.total_in_lo32:=s.strm.total_in_lo32+1;
          if (s.strm.total_in_lo32 = 0) then
             s.strm.total_in_hi32:=s.strm.total_in_hi32+1;
      end;
   end;
   Result:=Res;
end;



procedure SET_LL4(i,n:Int32);
begin
   if (i and 1) = 0 then
        s.ll4.SetData((s.ll4.GetData(i shr 1) and $f0) or n,i shr 1)
   else
        s.ll4.SetData((s.ll4.GetData(i shr 1) and $0f) or ((n) shl 4),i shr 1);
end;

function GET_LL4(i:Int32):UInt32;
begin
        Result:=((UInt32(s.ll4.GetData((i) shr 1))) shr (((i) shl 2) and $4)) and $F;
end;

procedure SET_LL(i,n:UInt32);
begin
   s.ll16.SetData(UInt16(n and $0000ffff),i);
   SET_LL4(i, n shr 16);
end;

function GET_LL(i:Int32):UInt32;
begin
        Result:=UInt32(s.ll16.GetData(i)) or (GET_LL4(i) shl 16);
end;

procedure BZ_GET_SMALL(var cccc:Int32);
begin
      cccc := BZ2_indexIntoF ( s.tPos, s.cftab);
      s.tPos := GET_LL(s.tPos);
end;

procedure BZ_GET_FAST(var ccc:Int32); overload;
begin
    s.tPos := s.tt.GetData(s.tPos);
    ccc := s.tPos and  $ff;
    s.tPos := s.tPos shr 8;
end;

procedure BZ_GET_FAST2(var ccc:Byte); overload;
begin
    s.tPos := s.tt.GetData(s.tPos);
    ccc := s.tPos and  $ff;
    s.tPos := s.tPos shr 8;
end;


procedure SaveState;
begin
   s.save_i           := i;
   s.save_j           := j;
   s.save_t           := t;
   s.save_alphaSize   := alphaSize;
   s.save_nGroups     := nGroups;
   s.save_nSelectors  := nSelectors;
   s.save_EOB         := EOB;
   s.save_groupNo     := groupNo;
   s.save_groupPos    := groupPos;
   s.save_nextSym     := nextSym;
   s.save_nblockMAX   := nblockMAX;
   s.save_nblock      := nblock;
   s.save_es          := es;
   s.save_N           := N;
   s.save_curr        := curr;
   s.save_zt          := zt;
   s.save_zn          := zn;
   s.save_zvec        := zvec;
   s.save_zj          := zj;
   s.save_gSel        := gSel;
   s.save_gMinlen     := gMinlen;
   s.save_gLimit      := gLimit;
   s.save_gBase       := gBase;
   s.save_gPerm       := gPerm;
end;

var
  bOK: Boolean;
begin
   strm := s.strm;

   if (s.state = BZ_X_MAGIC_1) then
   begin
      s.save_i           := 0;
      s.save_j           := 0;
      s.save_t           := 0;
      s.save_alphaSize   := 0;
      s.save_nGroups     := 0;
      s.save_nSelectors  := 0;
      s.save_EOB         := 0;
      s.save_groupNo     := 0;
      s.save_groupPos    := 0;
      s.save_nextSym     := 0;
      s.save_nblockMAX   := 0;
      s.save_nblock      := 0;
      s.save_es          := 0;
      s.save_N           := 0;
      s.save_curr        := 0;
      s.save_zt          := 0;
      s.save_zn          := 0;
      s.save_zvec        := 0;
      s.save_zj          := 0;
      s.save_gSel        := 0;
      s.save_gMinlen     := 0;
      s.save_gLimit      := nil;
      s.save_gBase       := nil;
      s.save_gPerm       := nil;
   end;


   i           := s.save_i;
   j           := s.save_j;
   t           := s.save_t;
   alphaSize   := s.save_alphaSize;
   nGroups     := s.save_nGroups;
   nSelectors  := s.save_nSelectors;
   EOB         := s.save_EOB;
   groupNo     := s.save_groupNo;
   groupPos    := s.save_groupPos;
   nextSym     := s.save_nextSym;
   nblockMAX   := s.save_nblockMAX;
   nblock      := s.save_nblock;
   es          := s.save_es;
   N           := s.save_N;
   curr        := s.save_curr;
   zt          := s.save_zt;
   zn          := s.save_zn;
   zvec        := s.save_zvec;
   zj          := s.save_zj;
   gSel        := s.save_gSel;
   gMinlen     := s.save_gMinlen;
   gLimit      := s.save_gLimit;
   gBase       := s.save_gBase;
   gPerm       := s.save_gPerm;

   Result := BZ_OK;

(*
   case s.state of
     BZ_X_MAGIC_1: goto label_BZ_X_MAGIC_1;
   BZ_X_MAGIC_2: goto label_BZ_X_MAGIC_2;
   BZ_X_MAGIC_3: goto label_BZ_X_MAGIC_3;
   BZ_X_MAGIC_4: goto label_BZ_X_MAGIC_4;
   BZ_X_BLKHDR_1: goto label_BZ_X_BLKHDR_1;
   BZ_X_BLKHDR_2: goto label_BZ_X_BLKHDR_2;
   BZ_X_BLKHDR_3: goto label_BZ_X_BLKHDR_3;
   BZ_X_BLKHDR_4: goto label_BZ_X_BLKHDR_4;
   BZ_X_BLKHDR_5: goto label_BZ_X_BLKHDR_5;
   BZ_X_BLKHDR_6: goto label_BZ_X_BLKHDR_6;
   BZ_X_BCRC_1: goto label_BZ_X_BCRC_1;
   BZ_X_BCRC_2: goto label_BZ_X_BCRC_2;
   BZ_X_BCRC_3: goto label_BZ_X_BCRC_3;
   BZ_X_BCRC_4: goto label_BZ_X_BCRC_4;
   BZ_X_RANDBIT: goto label_BZ_X_RANDBIT;
   BZ_X_ORIGPTR_1: goto label_BZ_X_ORIGPTR_1;
   BZ_X_ORIGPTR_2: goto label_BZ_X_ORIGPTR_2;
   BZ_X_ORIGPTR_3: goto label_BZ_X_ORIGPTR_3;
   BZ_X_MAPPING_1: goto label_BZ_X_MAPPING_1;
   BZ_X_MAPPING_2: goto label_BZ_X_MAPPING_2;
   BZ_X_SELECTOR_1: goto label_BZ_X_SELECTOR_1;
   BZ_X_SELECTOR_2: goto label_BZ_X_SELECTOR_2;
   BZ_X_SELECTOR_3: goto label_BZ_X_SELECTOR_3;
   BZ_X_CODING_1: goto label_BZ_X_CODING_1;
   BZ_X_CODING_2: goto label_BZ_X_CODING_2;
   BZ_X_CODING_3: goto label_BZ_X_CODING_3;
   BZ_X_MTF_1: goto label_BZ_X_MTF_1;
   BZ_X_MTF_2: goto label_BZ_X_MTF_2;
   BZ_X_MTF_3: goto label_BZ_X_MTF_3;
   BZ_X_MTF_4: goto label_BZ_X_MTF_4;
   BZ_X_MTF_5: goto label_BZ_X_MTF_5;
   BZ_X_MTF_6: goto label_BZ_X_MTF_6;
   BZ_X_ENDHDR_2: goto label_BZ_X_ENDHDR_2;
   BZ_X_ENDHDR_3: goto label_BZ_X_ENDHDR_3;
   BZ_X_ENDHDR_4: goto label_BZ_X_ENDHDR_4;
   BZ_X_ENDHDR_5: goto label_BZ_X_ENDHDR_5;
   BZ_X_ENDHDR_6: goto label_BZ_X_ENDHDR_6;
   BZ_X_CCRC_1: goto label_BZ_X_CCRC_1;
   BZ_X_CCRC_2: goto label_BZ_X_CCRC_2;
   BZ_X_CCRC_3: goto label_BZ_X_CCRC_3;
   BZ_X_CCRC_4: goto label_BZ_X_CCRC_4;
   else
        AssertH ( False, 4001 );
   end;
*)

   if s.state = BZ_X_MAGIC_1 then
   begin
     Res := GET_BITS3(BZ_X_MAGIC_1, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> BZ_HDR_B) then
     begin
       Result := BZ_DATA_ERROR_MAGIC;
       SaveState;
       Exit;
     end;

     s.state := BZ_X_MAGIC_2;
   end;

   if s.state = BZ_X_MAGIC_2 then
   begin
     Res := GET_BITS3(BZ_X_MAGIC_2, uc, 8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> BZ_HDR_Z) then
     begin
       Result := BZ_DATA_ERROR_MAGIC;
       SaveState;
       Exit;
     end;

     s.state := BZ_X_MAGIC_3;
   end;

   if s.state = BZ_X_MAGIC_3 then
   begin
     Res := GET_BITS3(BZ_X_MAGIC_3, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> BZ_HDR_h) then
     begin
       Result := BZ_DATA_ERROR_MAGIC;
       SaveState;
       Exit;
     end;

     s.state := BZ_X_MAGIC_4;
   end;

   if s.state = BZ_X_MAGIC_4 then
   begin
     Res := GET_BITS(BZ_X_MAGIC_4, s.blockSize100k,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (s.blockSize100k < (BZ_HDR_0 + 1)) or
        (s.blockSize100k > (BZ_HDR_0 + 9)) then
     begin
       Result := BZ_DATA_ERROR_MAGIC;
       SaveState;
       Exit;
     end;
     s.blockSize100k := s.blockSize100k - BZ_HDR_0;

     if (s.smallDecompress) then
     begin
       s.ll16 := TSBUInt16Array.Create( s.blockSize100k * 100000);
       s.ll4  := TSBByteArray.Create(((1 + s.blockSize100k * 100000) shr 1));
       if (s.ll16 = nil) or (s.ll4 = nil) then
       begin
         Result := BZ_MEM_ERROR;
         SaveState;
         Exit;
       end;
     end
     else
     begin
       s.tt  := TSBUint32Array.Create( s.blockSize100k * 100000);
       if (s.tt = nil) then
       begin
         Result := BZ_MEM_ERROR;
         SaveState;
         Exit;
       end;
     end;

     s.state := BZ_X_BLKHDR_1;
   end;

   if s.state = BZ_X_BLKHDR_1 then
   begin
     Res := GET_BITS3(BZ_X_BLKHDR_1, uc,8);
     if Res <> BZ_NULL then
     begin
       Result:=Res;
       SaveState;
       Exit;
     end;

     if (uc = $17) then
       s.state := BZ_X_ENDHDR_2
     else
     if (uc <> $31) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end
     else
       s.state := BZ_X_BLKHDR_2;
   end;

   if s.state = BZ_X_BLKHDR_2 then
   begin
     Res := GET_BITS3(BZ_X_BLKHDR_2, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> $41) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     s.state := BZ_X_BLKHDR_3;
   end;

   if s.state = BZ_X_BLKHDR_3 then
   begin
     Res := GET_BITS3(BZ_X_BLKHDR_3, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> $59) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     s.state := BZ_X_BLKHDR_4;
   end;

   if s.state = BZ_X_BLKHDR_4 then
   begin
     Res := GET_BITS3(BZ_X_BLKHDR_4, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> $26) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     s.state := BZ_X_BLKHDR_5;
   end;

   if s.state = BZ_X_BLKHDR_5 then
   begin
     Res := GET_BITS3(BZ_X_BLKHDR_5, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> $53) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     s.state := BZ_X_BLKHDR_6;
   end;

   if s.state = BZ_X_BLKHDR_6 then
   begin
     Res := GET_BITS3(BZ_X_BLKHDR_6, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> $59) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     inc(s.currBlockNo);
     if (s.verbosity >= 2) then
        Write(ErrOutput, CRLF+Format('    [%d: huff+mtf ', [s.currBlockNo]) );

      s.storedBlockCRC := 0;

      s.state := BZ_X_BCRC_1;
   end;

   if s.state = BZ_X_BCRC_1 then
   begin
     Res := GET_BITS3(BZ_X_BCRC_1, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;

     s.storedBlockCRC := (s.storedBlockCRC shl 8) or UInt32(uc);

     s.state := BZ_X_BCRC_2;
   end;

   if s.state = BZ_X_BCRC_2 then
   begin
     Res := GET_BITS3(BZ_X_BCRC_2, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;

     s.storedBlockCRC := (s.storedBlockCRC shl 8) or UInt32(uc);
     s.state := BZ_X_BCRC_3;
   end;

   if s.state = BZ_X_BCRC_3 then
   begin
     Res := GET_BITS3(BZ_X_BCRC_3, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;

     s.storedBlockCRC := (s.storedBlockCRC shl 8) or UInt32(uc);
     s.state := BZ_X_BCRC_4;
   end;

   if s.state = BZ_X_BCRC_4 then
   begin
     Res := GET_BITS3(BZ_X_BCRC_4, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;

     s.storedBlockCRC := (s.storedBlockCRC shl 8) or UInt32(uc);
     s.state := BZ_X_RANDBIT;
   end;

   if s.state = BZ_X_RANDBIT then
   begin
     Res := GET_BITS4(BZ_X_RANDBIT, s.blockRandomised,1);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;

     s.origPtr := 0;
     s.state := BZ_X_ORIGPTR_1;
   end;

   if s.state = BZ_X_ORIGPTR_1 then
   begin
     Res := GET_BITS3(BZ_X_ORIGPTR_1, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;

     s.origPtr := (s.origPtr shl 8) or Int32(uc);
     s.state := BZ_X_ORIGPTR_2;
   end;

   if s.state = BZ_X_ORIGPTR_2 then
   begin
     Res := GET_BITS3(BZ_X_ORIGPTR_2, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;

     s.origPtr := (s.origPtr shl 8) or Int32(uc);
     s.state := BZ_X_ORIGPTR_3;
   end;

   if s.state = BZ_X_ORIGPTR_3 then
   begin
     Res := GET_BITS3(BZ_X_ORIGPTR_3, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     s.origPtr := (s.origPtr shl 8) or Int32(uc);

     if (s.origPtr < 0) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;
     if (s.origPtr > integer(10 + 100000*s.blockSize100k)) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     i := 0;
     s.state := BZ_X_MAPPING_1;
   end;

   //--- Receive the mapping table ---*/
   if s.state = BZ_X_MAPPING_1 then
   begin
     while i < 16 do
     begin
       Res := GET_BITS3(BZ_X_MAPPING_1, uc,1);
       if Res <> BZ_NULL then
       begin
         Result := Res;
         SaveState;
         Exit;
       end;
       if (uc = 1) then
         s.inUse16[i] := True
       else
         s.inUse16[i] := False;

       inc(i);
     end;

     for i := 0 to 255 do s.inUse[i] := False;

     i := 0;
     j := 0;
     s.state := BZ_X_MAPPING_2;
   end;

   if s.state = BZ_X_MAPPING_2 then
   begin
     while i < 16 do
     begin
       if (s.inUse16[i]) then
       begin
         while (j < 16) do
         begin
           Res := GET_BITS3(BZ_X_MAPPING_2, uc,1);
           if Res <> BZ_NULL then
           begin
             Result := Res;
             SaveState;
             Exit;
           end;

           if (uc = 1) then s.inUse[i * 16 + j] := True;

           inc(j);
         end;

         j := 0;
       end;

       inc(i);
     end;

     makeMaps_d ( s );
     if (s.nInUse = 0) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     alphaSize := s.nInUse + 2;
     s.state := BZ_X_SELECTOR_1;
   end;

   //--- Now the selectors ---*/
   if s.state = BZ_X_SELECTOR_1 then
   begin
     Res := GET_BITS2(BZ_X_SELECTOR_1, nGroups,3);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (nGroups < 2) or (nGroups > 6) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     s.state := BZ_X_SELECTOR_2;
   end;

   if s.state = BZ_X_SELECTOR_2 then
   begin
     Res := GET_BITS2(BZ_X_SELECTOR_2, nSelectors,15);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (nSelectors < 1) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     i := 0;
     j := 0;
     s.state := BZ_X_SELECTOR_3;
   end;

   if s.state = BZ_X_SELECTOR_3 then
   begin
     while i < nSelectors do
     begin
       while (True) do
       begin
         Res := GET_BITS3(BZ_X_SELECTOR_3, uc,1);
         if Res <> BZ_NULL then
         begin
           Result := Res;
           SaveState;
           Exit;
         end;
         if (uc = 0) then break;
         inc(j);
         if (j >= nGroups) then
         begin
           Result := BZ_DATA_ERROR;
           SaveState;
           Exit;
         end;
       end;

       s.selectorMtf[i] := j;
       inc(i);
       j := 0;
     end;

     //--- Undo the MTF values for the selectors. ---*/
     for v := 0 to nGroups-1 do pos[v] := v;

     for i := 0 to nSelectors-1 do
     begin
       v := s.selectorMtf[i];
       tmp := pos[v];
        while (v > 0) do
        begin
          pos[v] := pos[v-1];
          dec(v);
        end;
        pos[0] := tmp;
        s.selector[i] := tmp;
     end;

     t := 0;
     s.state := BZ_X_CODING_1;
   end;

   //--- Now the coding tables ---*/
   if (s.state = BZ_X_CODING_1) or
      (s.state = BZ_X_CODING_2) or
      (s.state = BZ_X_CODING_3) then
   begin
     bOK := False;
     if s.state = BZ_X_CODING_1 then
       bOK := True;

     while t < nGroups do
     begin
       if bOK then
       begin
         Res := GET_BITS2(BZ_X_CODING_1, curr,5);
         if Res <> BZ_NULL then
         begin
           Result := Res;
           SaveState;
           Exit;
         end;

         i := 0;
       end;

       while (i < alphaSize) do
       begin
         while (True) do
         begin
           if bOK then
           begin
             if (curr < 1) or (curr > 20) then
             begin
               Result := BZ_DATA_ERROR;
               SaveState;
               Exit;
             end;

             s.state := BZ_X_CODING_2;
           end;

           if s.state = BZ_X_CODING_2 then
           begin
             Res := GET_BITS3(BZ_X_CODING_2, uc,1);
             if Res <> BZ_NULL then
             begin
               Result := Res;
               SaveState;
               Exit;
             end;
             if (uc = 0) then break;

             s.state := BZ_X_CODING_3;
           end;

           Res := GET_BITS3(BZ_X_CODING_3, uc,1);
           if Res <> BZ_NULL then
           begin
             Result := Res;
             SaveState;
             Exit;
           end;
           if (uc = 0) then
             inc(curr)
           else
             dec(curr);

           bOK := True;
         end;

         s.len[t].SetData(curr,i);
         inc(i);
       end;

       inc(t);
       bOK := True;
     end;

     //--- Create the Huffman decoding tables ---*/
      for t := 0 to nGroups-1 do
      begin
         minLen := 32;
         maxLen := 0;
         for i := 0 to alphaSize-1 do
         begin
            if (Integer(s.len[t].GetData(i)) > maxLen) then maxLen := s.len[t].GetData(i);
            if (Integer(s.len[t].GetData(i)) < minLen) then minLen := s.len[t].GetData(i);
         end;
         BZ2_hbCreateDecodeTables (
            s.limit[t],
            s.base[t],
            s.perm[t],
            s.len[t],
            minLen, maxLen, alphaSize
         );
         s.minLens[t] := minLen;
      end;

      //--- Now the MTF values ---*/

      EOB      := s.nInUse+1;
      nblockMAX := 100000 * s.blockSize100k;
      groupNo  := -1;
      groupPos := 0;

      for i := 0 to 255 do s.unzftab[i] := 0;

      //-- MTF init --*/
       kk := MTFA_SIZE-1;
       for ii := (256 div MTFL_SIZE) - 1 downto 0 do
       begin
          for jj := MTFL_SIZE-1 downto 0 do
          begin
             s.mtfa[kk] := Byte(ii * MTFL_SIZE + jj);
             dec(kk);
          end;
          s.mtfbase[ii] := kk + 1;
       end;
      //-- end MTF init --*/

      nblock := 0;
      if (groupPos = 0) then
      begin
        inc(groupNo);
        if (groupNo >= nSelectors) then
        begin
          Result := BZ_DATA_ERROR;
          SaveState;
          Exit;
        end;
        groupPos := BZ_G_SIZE;
        gSel := s.selector[groupNo];
        gMinlen := s.minLens[gSel];
        gLimit := s.limit[gSel];
        gPerm := s.perm[gSel];
        gBase := s.base[gSel];
      end;

      dec(groupPos);
      zn := gMinlen;

     s.state := BZ_X_MTF_1;
   end;

   if (s.state = BZ_X_MTF_1) or
      (s.state = BZ_X_MTF_2) or
      (s.state = BZ_X_MTF_3) or
      (s.state = BZ_X_MTF_4) or
      (s.state = BZ_X_MTF_5) or
      (s.state = BZ_X_MTF_6) then
   begin
     bOK := False;
     if s.state = BZ_X_MTF_1 then
     begin
       Res := GET_BITS2(BZ_X_MTF_1, zvec,zn);
       if Res <> BZ_NULL then
       begin
         Result := Res;
         SaveState;
         Exit;
       end;

       bOK := True;
     end;

     if (s.state = BZ_X_MTF_1) or
        (s.state = BZ_X_MTF_2) then
     begin
       while (true) do
       begin
         if bOK then
         begin
           if (zn > 20 { the longest code }) then
           begin
             Result := BZ_DATA_ERROR;
             SaveState;
             Exit;
           end;
           if zvec <= gLimit.GetData(zn) then break;
           inc(zn);
         end;

         s.state := BZ_X_MTF_2;
         Res := GET_BITS2(BZ_X_MTF_2, zj,1);
         if Res <> BZ_NULL then
         begin
           Result := Res;
           SaveState;
           Exit;
         end;
         zvec := (zvec shl 1) or zj;
         bOK := True;
       end;

       if (zvec - gBase.GetData(zn) < 0) or
          (zvec - gBase.GetData(zn) >= BZ_MAX_ALPHA_SIZE) then
       begin
         Result:=BZ_DATA_ERROR;
         SaveState;
         Exit;
       end;

       nextSym := gPerm.GetData(zvec - gBase.GetData(zn));
     end;

     while (True) do
     begin
       if bOK and (nextSym = EOB) then break;

       if (bOK and ((nextSym = BZ_RUNA) or (nextSym = BZ_RUNB))) or
          (not bOK and ((s.state = BZ_X_MTF_3) or (s.state = BZ_X_MTF_4))) then
       begin
         if bOK then
         begin
           es := -1;
           N := 1;
         end;

         repeat
           if bOK then
           begin
             if (nextSym = BZ_RUNA) then es := es + (0+1) * N
             else
             if (nextSym = BZ_RUNB) then es := es + (1+1) * N;
             N := N * 2;
             if (groupPos = 0) then
             begin
               inc(groupNo);
               if (groupNo >= nSelectors) then
               begin
                 Result:=BZ_DATA_ERROR;
                 SaveState;
                 Exit;
               end;
               groupPos := BZ_G_SIZE;
               gSel := s.selector[groupNo];
               gMinlen := s.minLens[gSel];
               gLimit := s.limit[gSel];
               gPerm := s.perm[gSel];
               gBase := s.base[gSel];
             end;
             dec(groupPos);
             zn := gMinlen;
             s.state := BZ_X_MTF_3;
           end;

           if s.state = BZ_X_MTF_3 then
           begin
             Res := GET_BITS2(BZ_X_MTF_3, zvec,zn);
             if Res <> BZ_NULL then
             begin
               Result := Res;
               SaveState;
               Exit;
             end;

             bOK := True;
           end;

           while (True) do
           begin
             if bOK then
             begin
               if (zn > 20 { the longest code }) then
               begin
                 Result:=BZ_DATA_ERROR;
                 SaveState;
                 Exit;
               end;
               if (zvec <= gLimit.GetData(zn)) then break;
               inc(zn);
               s.state := BZ_X_MTF_4;
             end;

             Res := GET_BITS2(BZ_X_MTF_4, zj,1);
             if Res <> BZ_NULL then
             begin
               Result := Res;
               SaveState;
               Exit;
             end;
             zvec := (zvec shl 1) or zj;
             bOK := True; 
           end;
           if (zvec - gBase.GetData(zn) < 0) or
              (zvec - gBase.GetData(zn) >= BZ_MAX_ALPHA_SIZE) then
           begin
             Result:=BZ_DATA_ERROR;
             SaveState;
             Exit;
           end;
           nextSym := gPerm.GetData(zvec - gBase.GetData(zn));
         until (nextSym <> BZ_RUNA) and (nextSym <> BZ_RUNB);

         inc(es);
         uc := s.seqToUnseq[ s.mtfa[s.mtfbase[0]] ];
         s.unzftab[uc] := s.unzftab[uc] +es;

         if (s.smallDecompress) then
           while (es > 0) do
           begin
             if (nblock >= nblockMAX) then
             begin
               Result:=BZ_DATA_ERROR;
               SaveState;
               Exit;
             end;

             s.ll16.SetData(UInt16(uc),nblock);
             inc(nblock);
             dec(es);
           end
           else
             while (es > 0) do
             begin
               if (nblock >= nblockMAX) then
               begin
                 Result:=BZ_DATA_ERROR;
                 SaveState;
                 Exit;
               end;

               s.tt.SetData(UInt32(uc),nblock);
               inc(nblock);
               dec(es);
             end;

         continue;
       end
       else
       begin
         if bOK then
         begin
           if (nblock >= nblockMAX) then
           begin
             Result:=BZ_DATA_ERROR;
             SaveState;
             Exit;
           end;

            //-- uc = MTF ( nextSym-1 ) --*/
             nn := UInt32(nextSym - 1);

             if (nn < MTFL_SIZE)  then
             begin
                pp := s.mtfbase[0];
                uc := s.mtfa[pp+nn];
                while (nn > 3) do
                begin
                   z := pp+nn;
                   s.mtfa[(z)  ] := s.mtfa[(z)-1];
                   s.mtfa[(z)-1] := s.mtfa[(z)-2];
                   s.mtfa[(z)-2] := s.mtfa[(z)-3];
                   s.mtfa[(z)-3] := s.mtfa[(z)-4];
                   nn := nn -  4;
                end;
                while (nn > 0) do
                begin
                   s.mtfa[(pp+nn)] := s.mtfa[(pp+nn)-1];
                   dec(nn);
                end;
                s.mtfa[pp] := uc;
             end
             else
             begin
                // general case */
                lno := nn div MTFL_SIZE;
                off := nn mod MTFL_SIZE;
                pp := s.mtfbase[lno] + off;
                uc := s.mtfa[pp];
                while (pp > s.mtfbase[lno]) do
                begin
                   s.mtfa[pp] := s.mtfa[pp-1];
                   dec(pp);
                end;
                inc(s.mtfbase[lno]);
                while (lno > 0) do
                begin
                   dec(s.mtfbase[lno]);
                   s.mtfa[s.mtfbase[lno]] := s.mtfa[s.mtfbase[lno-1] + MTFL_SIZE - 1];
                   dec(lno);
                end;
                dec(s.mtfbase[0]);
                s.mtfa[s.mtfbase[0]] := uc;
                if (s.mtfbase[0] = 0) then
                begin
                   kk := MTFA_SIZE-1;
                   for ii := (256 div MTFL_SIZE)-1 downto 0 do
                   begin
                      for jj := MTFL_SIZE-1 downto 0 do
                      begin
                         s.mtfa[kk] := s.mtfa[s.mtfbase[ii] + jj];
                         dec(kk);
                      end;
                      s.mtfbase[ii] := kk + 1;
                   end;
                end;
             end;
            //-- end uc = MTF ( nextSym-1 ) --*/

            inc(s.unzftab[s.seqToUnseq[uc]]);
            if (s.smallDecompress) then
               s.ll16.SetData(UInt16(s.seqToUnseq[uc]),nblock)
            else
               s.tt.SetData(UInt32(s.seqToUnseq[uc]),nblock);
            inc(nblock);

           if (groupPos = 0) then
           begin
              inc(groupNo);
              if (groupNo >= nSelectors) then
              begin
                Result:=BZ_DATA_ERROR;
                SaveState;
                Exit;
              end;
              groupPos := BZ_G_SIZE;
              gSel := s.selector[groupNo];
              gMinlen := s.minLens[gSel];
              gLimit := s.limit[gSel];
              gPerm := s.perm[gSel];
              gBase := s.base[gSel];
           end;
           dec(groupPos);
           zn := gMinlen;

           s.state := BZ_X_MTF_5;
         end;

         if s.state = BZ_X_MTF_5 then
         begin
           Res:=GET_BITS2(BZ_X_MTF_5, zvec,zn);
           if Res<>BZ_NULL then
           begin
             Result:=Res;
             SaveState;
             Exit;
           end;

           bOK := True;
         end;

         while (true) do
         begin
           if bOK then
           begin
              if (zn > 20 { the longest code }) then
              begin
                Result:=BZ_DATA_ERROR;
                SaveState;
                Exit;
              end;
              if (zvec <= gLimit.GetData(zn)) then break;
              inc(zn);
              s.state := BZ_X_MTF_6;
           end;

           bOK := True;
           Res:=GET_BITS2(BZ_X_MTF_6, zj,1);
           if Res<>BZ_NULL then
           begin
             Result:=Res;
             SaveState;
             Exit;
           end;
           zvec := (zvec shl 1) or zj;
         end;

           if (zvec - gBase.GetData(zn) < 0) or
               (zvec - gBase.GetData(zn) >= BZ_MAX_ALPHA_SIZE) then
           begin
              Result:=BZ_DATA_ERROR;
              SaveState;
              Exit;
           end;
           nextSym := gPerm.GetData(zvec - gBase.GetData(zn));
           continue;
         end;
      end;

      { Now we know what nblock is, we can do a better sanity
         check on s.origPtr.
      }
      if (s.origPtr < 0) or (s.origPtr >= nblock) then
      begin
        Result:=BZ_DATA_ERROR;
        SaveState;
        Exit;
      end;

      //-- Set up cftab to facilitate generation of T^(-1)

      s.cftab[0] := 0;
      for i := 1 to 256 do s.cftab[i] := s.unzftab[i-1];
      for i := 1 to 256 do s.cftab[i] := s.cftab[i] + s.cftab[i-1];
      for i := 0 to 256 do
      begin
         if (s.cftab[i] < 0) or (s.cftab[i] > nblock) then
            // s.cftab[i] can legitimately be == nblock
         begin
           Result:=BZ_DATA_ERROR;
           SaveState;
           Exit;
         end;
      end;

      s.state_out_len := 0;
      s.state_out_ch  := 0;
      s.calculatedBlockCRC :=$ffffffff;
      s.state := BZ_X_OUTPUT;
      if (s.verbosity >= 2) then Write (ErrOutput, 'rt+rld' );

      if (s.smallDecompress) then
      begin

         //-- Make a copy of cftab, used in generation of T
         for i := 0 to  256 do s.cftabCopy[i] := s.cftab[i];

         //-- compute the T vector
         for i := 0 to nblock - 1 do
         begin
            uc := Byte(s.ll16.GetData(i));
            SET_LL(i, s.cftabCopy[uc]);
            inc(s.cftabCopy[uc]);
         end;

         //-- Compute T^(-1) by pointer reversal on T --*/
         i := s.origPtr;
         j := GET_LL(i);
         repeat
            tmp := GET_LL(j);
            SET_LL(j, i);
            i := j;
            j := tmp;
         until i = s.origPtr;

         s.tPos := s.origPtr;
         s.nblock_used := 0;
         if (s.blockRandomised) then
         begin
//            BZ_RAND_INIT_MASK;
            s.rNToGo := 0;
            s.rTPos  := 0;

            BZ_GET_SMALL(s.k0);
            inc(s.nblock_used);
//            BZ_RAND_UPD_MASK;
            if (s.rNToGo = 0) then
            begin
               s.rNToGo := BZ2_rNums[s.rTPos];
               inc(s.rTPos);
               if (s.rTPos = 512) then s.rTPos := 0;
            end;
            dec(s.rNToGo);

            s.k0 := s.k0 xor IfThenInt((s.rNToGo = 1), integer(1), 0);
         end
         else
         begin
            BZ_GET_SMALL(s.k0);
            inc(s.nblock_used);
         end;

      end
      else
      begin
         //-- compute the T^(-1) vector --*/
         for i := 0 to nblock - 1 do
         begin
            uc := BYte(s.tt.GetData(i) and  $ff);
            s.tt.SetData(s.tt.GetData(s.cftab[uc]) or cardinal(i shl 8), s.cftab[uc]);
            inc(s.cftab[uc]);
         end;

         s.tPos := s.tt.GetData(s.origPtr) shr 8;
         s.nblock_used := 0;
         if (s.blockRandomised) then
         begin
//            BZ_RAND_INIT_MASK;
            s.rNToGo := 0;
            s.rTPos  := 0;

            BZ_GET_FAST(s.k0);

            inc(s.nblock_used);
//            BZ_RAND_UPD_MASK;
            if (s.rNToGo = 0) then
            begin
               s.rNToGo := BZ2_rNums[s.rTPos];
               inc(s.rTPos);
               if (s.rTPos = 512) then s.rTPos := 0;
            end;
            dec(s.rNToGo);

            s.k0 := s.k0 xor IfThenInt((s.rNToGo = 1), integer(1), 0);
         end
         else
         begin
            BZ_GET_FAST(s.k0);

            inc(s.nblock_used);
         end;

      end;

     Result:=BZ_OK;
     SaveState;
     Exit;
   end;

   if s.state = BZ_X_ENDHDR_2 then
   begin
     Res := GET_BITS3(BZ_X_ENDHDR_2, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> $72) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     s.state := BZ_X_ENDHDR_3;
   end;

   if s.state = BZ_X_ENDHDR_3 then
   begin
     Res := GET_BITS3(BZ_X_ENDHDR_3, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> $45) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     s.state := BZ_X_ENDHDR_4;
   end;

   if s.state = BZ_X_ENDHDR_4 then
   begin
     Res := GET_BITS3(BZ_X_ENDHDR_4, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> $38) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     s.state := BZ_X_ENDHDR_5;
   end;

   if s.state = BZ_X_ENDHDR_5 then
   begin
     Res := GET_BITS3(BZ_X_ENDHDR_5, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> $50) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     s.state := BZ_X_ENDHDR_6;
   end;

   if s.state = BZ_X_ENDHDR_6 then
   begin
     Res := GET_BITS3(BZ_X_ENDHDR_6, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     if (uc <> $90) then
     begin
       Result := BZ_DATA_ERROR;
       SaveState;
       Exit;
     end;

     s.storedCombinedCRC := 0;
     s.state := BZ_X_CCRC_1;
   end;

   if s.state = BZ_X_CCRC_1 then
   begin
     Res := GET_BITS3(BZ_X_CCRC_1, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;

     s.storedCombinedCRC := (s.storedCombinedCRC shl 8) or UInt32(uc);
     s.state := BZ_X_CCRC_2;
   end;

   if s.state = BZ_X_CCRC_2 then
   begin
     Res := GET_BITS3(BZ_X_CCRC_2, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;

     s.storedCombinedCRC := (s.storedCombinedCRC shl 8) or UInt32(uc);
     s.state := BZ_X_CCRC_3;
   end;

   if s.state = BZ_X_CCRC_3 then
   begin
     Res := GET_BITS3(BZ_X_CCRC_3, uc,8);
     if Res<>BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;

     s.storedCombinedCRC := (s.storedCombinedCRC shl 8) or UInt32(uc);
     s.state := BZ_X_CCRC_4;
   end;

   if s.state = BZ_X_CCRC_4 then
   begin
     Res := GET_BITS3(BZ_X_CCRC_4, uc,8);
     if Res <> BZ_NULL then
     begin
       Result := Res;
       SaveState;
       Exit;
     end;
     
     s.storedCombinedCRC := (s.storedCombinedCRC shl 8) or UInt32(uc);
   end
   else
     AssertH ( False, 4001 );

   s.state := BZ_X_IDLE;

   Result := BZ_STREAM_END;
   SaveState;
end;
{$WARNINGS ON}
{$HINTS ON}

end.
