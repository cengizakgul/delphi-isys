(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$i SecBbox.inc}

unit SBCustomCrypto;

interface

uses
  Classes,
  SBUtils;

type
  TElKeyMaterial = class
  protected
    function GetValid : boolean; virtual;
  public
    procedure Generate(Bits : integer); virtual;
    procedure Save(Stream : TStream); virtual;
    procedure Load(Stream : TStream;
      Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}); virtual;
    property Valid : boolean read GetValid;
  end;

  TElCustomCrypto = class
  end;

implementation

procedure TElKeyMaterial.Generate(Bits : integer);
begin
  ;
end;

procedure TElKeyMaterial.Save(Stream : TStream);
begin
  ;
end;

procedure TElKeyMaterial.Load(Stream : TStream;
  Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif});
begin
  ;
end;

function TElKeyMaterial.GetValid : boolean;
begin
  Result := false;
end;

end.
