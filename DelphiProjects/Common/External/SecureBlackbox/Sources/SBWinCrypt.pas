(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

(*$HPPEMIT '#include <wincrypt.h>'*)

unit SBWinCrypt;

interface

uses
  Windows,
  SBConstants;


const
  {$ifndef NET_CF}
  CRYPT32     = 'crypt32.dll';
  OLE32       = 'ole32.dll';
  ADVAPI32    = 'advapi32.dll';
  KERNEL32    = 'kernel32.dll';
  {$else}
  CRYPT32     = 'crypt32.dll';
  OLE32       = 'ole32.dll';
  COREDLL     = 'coredll.dll';
  {$endif}
  
type

  {$ifdef VCL60}
  {$EXTERNALSYM HCRYPTOIDFUNCADDR}
  {$endif}
  HCRYPTOIDFUNCADDR = pointer;

  {$ifdef VCL60}
  {$EXTERNALSYM HCERTSTOREPROV}
  {$endif}
  HCERTSTOREPROV = pointer;

  PPBYTE = ^PBYTE;
  PCRYPTOAPI_BLOB = ^CRYPTOAPI_BLOB;

  CRYPTOAPI_BLOB = record
    cbData : DWORD;
    pbData : PBYTE;
  end;
  CRYPT_INTEGER_BLOB            =  CRYPTOAPI_BLOB;
  PCRYPT_INTEGER_BLOB           = ^CRYPT_INTEGER_BLOB;
  CRYPT_UINT_BLOB               =  CRYPTOAPI_BLOB;
  PCRYPT_UINT_BLOB              = ^CRYPT_UINT_BLOB;
  CRYPT_OBJID_BLOB              =  CRYPTOAPI_BLOB;
  PCRYPT_OBJID_BLOB             = ^CRYPT_OBJID_BLOB;
  CERT_NAME_BLOB                =  CRYPTOAPI_BLOB;
  PCERT_NAME_BLOB               = ^CERT_NAME_BLOB;
  CERT_RDN_VALUE_BLOB           =  CRYPTOAPI_BLOB;
  PCERT_RDN_VALUE_BLOB          = ^CERT_RDN_VALUE_BLOB;
  CERT_BLOB                     =  CRYPTOAPI_BLOB;
  PCERT_BLOB                    = ^CERT_BLOB;
  CRL_BLOB                      =  CRYPTOAPI_BLOB;
  PCRL_BLOB                     = ^CRL_BLOB;
  DATA_BLOB                     =  CRYPTOAPI_BLOB;
  PDATA_BLOB                    = ^DATA_BLOB;
  CRYPT_DATA_BLOB               =  CRYPTOAPI_BLOB;
  PCRYPT_DATA_BLOB              = ^CRYPT_DATA_BLOB;
  CRYPT_HASH_BLOB               =  CRYPTOAPI_BLOB;
  PCRYPT_HASH_BLOB              = ^CRYPT_HASH_BLOB;
  CRYPT_DIGEST_BLOB             =  CRYPTOAPI_BLOB;
  PCRYPT_DIGEST_BLOB            = ^CRYPT_DIGEST_BLOB;
  CRYPT_DER_BLOB                =  CRYPTOAPI_BLOB;
  PCRYPT_DER_BLOB               = ^CRYPT_DER_BLOB;
  CRYPT_ATTR_BLOB               =  CRYPTOAPI_BLOB;
  PCRYPT_ATTR_BLOB              = ^CRYPT_ATTR_BLOB;
  PCRYPT_BIT_BLOB = ^CRYPT_BIT_BLOB;
  CRYPT_BIT_BLOB =  record
    cbData      :DWORD;
    pbData      :PBYTE;
    cUnusedBits :DWORD;
  end;

  PCRYPT_ALGORITHM_IDENTIFIER = ^CRYPT_ALGORITHM_IDENTIFIER;
  CRYPT_ALGORITHM_IDENTIFIER =  record
    pszObjId   :LPSTR;
    Parameters :CRYPT_OBJID_BLOB;
  end;

  CERT_PUBLIC_KEY_INFO =  record
    Algorithm :CRYPT_ALGORITHM_IDENTIFIER;
    PublicKey :CRYPT_BIT_BLOB;
  end;
  PCERT_PUBLIC_KEY_INFO = ^CERT_PUBLIC_KEY_INFO;
  
  CERT_EXTENSION =  record
    pszObjId : LPSTR;
    fCritical :BOOL;
    Value :CRYPT_OBJID_BLOB;
  end;
  PCERT_EXTENSION = ^CERT_EXTENSION;


  PCERT_INFO = ^CERT_INFO;
  CERT_INFO =  record
    dwVersion              :DWORD;
    SerialNumber           :CRYPT_INTEGER_BLOB;
    SignatureAlgorithm     :CRYPT_ALGORITHM_IDENTIFIER;
    Issuer                 :CERT_NAME_BLOB;
    NotBefore              :TFILETIME;
    NotAfter               :TFILETIME;
    Subject                :CERT_NAME_BLOB;
    SubjectPublicKeyInfo   :CERT_PUBLIC_KEY_INFO;
    IssuerUniqueId         :CRYPT_BIT_BLOB;
    SubjectUniqueId        :CRYPT_BIT_BLOB;
    cExtension             :DWORD;
    rgExtension            :PCERT_EXTENSION;
  end;
  
  {$externalsym PVOID}
  PVOID = Pointer;

  {$externalsym LONG}
  LONG  = DWORD;

  LPAWSTR = PAnsiChar;
  HCERTSTORE = PVOID;
  PHCERTSTORE = ^HCERTSTORE;
  PCERT_CONTEXT = ^CERT_CONTEXT;
  
  CERT_CONTEXT = record
    dwCertEncodingType :DWORD;
    pbCertEncoded :PBYTE;
    cbCertEncoded :DWORD;
    pCertInfo :PCERT_INFO;
    hCertStore :HCERTSTORE;
  end;
  PCCERT_CONTEXT = ^CERT_CONTEXT;
  PPCCERT_CONTEXT = ^PCCERT_CONTEXT;

  {$ifdef BUILDER_USED}
  {$EXTERNALSYM HCRYPTPROV}
  {$endif}
  HCRYPTPROV  =  ULONG;
  {$ifdef BUILDER_USED}
  {$EXTERNALSYM PHCRYPTPROV}
  {$endif}
  PHCRYPTPROV = ^HCRYPTPROV;
  {$ifdef BUILDER_USED}
  {$EXTERNALSYM HCRYPTKEY}
  {$endif}
  HCRYPTKEY   =  ULONG;
  {$ifdef BUILDER_USED}
  {$EXTERNALSYM PHCRYPTKEY}
  {$endif}
  PHCRYPTKEY  = ^HCRYPTKEY;
  {$ifdef BUILDER_USED}
  {$EXTERNALSYM HCRYPTHASH}
  {$endif}
  HCRYPTHASH  =  ULONG;
  {$ifdef BUILDER_USED}
  {$EXTERNALSYM PHCRYPTHASH}
  {$endif}
  PHCRYPTHASH = ^HCRYPTHASH;

  {$ifdef BUILDER_USED}
  {$HPPEMIT 'typedef unsigned long *PHCRYPTPROV;'}
  {$HPPEMIT 'typedef unsigned long *PHCRYPTKEY;'}
  {$HPPEMIT 'typedef unsigned long *PHCRYPTHASH;'}
  {$endif}

  {$ifdef VCL60}
  {$EXTERNALSYM _CERT_SYSTEM_STORE_INFO}
  {$endif}
  _CERT_SYSTEM_STORE_INFO =  record
    cbSize: DWORD;
  end;

  {$ifdef VCL60}
  {$EXTERNALSYM CERT_SYSTEM_STORE_INFO}
  {$endif}
  CERT_SYSTEM_STORE_INFO =  _CERT_SYSTEM_STORE_INFO;

  {$ifdef VCL60}
  {$EXTERNALSYM PCERT_SYSTEM_STORE_INFO}
  {$endif}
  PCERT_SYSTEM_STORE_INFO = ^CERT_SYSTEM_STORE_INFO;
  TCertSystemStoreInfo =  CERT_SYSTEM_STORE_INFO;

  PCertSystemStoreInfo = PCERT_SYSTEM_STORE_INFO;

  {$ifdef VCL60}
  {$EXTERNALSYM _CRYPT_KEY_PROV_INFO}
  {$endif}
  _CRYPT_KEY_PROV_INFO = record
    pwszContainerName : PWideChar;
    pwszProvName : PWideChar;
    dwProvType : DWORD;
    dwFlags : DWORD;
    cProvParam : DWORD;
    rgProvParam : pointer;
    dwKeySpec : DWORD;
  end;
  {$ifdef VCL60}
  {$EXTERNALSYM CRYPT_KEY_PROV_INFO}
  {$endif}
  CRYPT_KEY_PROV_INFO =  _CRYPT_KEY_PROV_INFO;
  {$ifdef VCL60}
  {$EXTERNALSYM PCRYPT_KEY_PROV_INFO}
  {$endif}
  PCRYPT_KEY_PROV_INFO = ^CRYPT_KEY_PROV_INFO;

  {$ifdef VCL60}
  {$EXTERNALSYM _CRYPT_ATTRIBUTE}
  {$endif}
  _CRYPT_ATTRIBUTE = record
    pszObjId : PChar;
    cValue : DWORD;
    rgValue : PCRYPT_ATTR_BLOB;
  end;
  CRYPT_ATTRIBUTE =  _CRYPT_ATTRIBUTE;

  {$ifdef VCL60}
  {$EXTERNALSYM PCRYPT_ATTRIBUTE}
  {$endif}
  PCRYPT_ATTRIBUTE = ^_CRYPT_ATTRIBUTE;

  {$ifdef VCL60}
  {$EXTERNALSYM _CRYPT_SIGN_MESSAGE_PARA}
  {$endif}
  _CRYPT_SIGN_MESSAGE_PARA = record
     cbSize : DWORD;
     dwMsgEncodingType : DWORD;
     pSigningCert : PCCERT_CONTEXT;
     HashAlgorithm : CRYPT_ALGORITHM_IDENTIFIER;
     pvHashAuxInfo : pointer;
     cMsgCert : DWORD;
     rgpMsgCert : PPCCERT_CONTEXT;
     cMsgCrl : DWORD;
     rgpMsgCrl : PPCCERT_CONTEXT;
     cAuthAttr : DWORD;
     rgAuthAttr : PCRYPT_ATTRIBUTE;
     cUnauthAttr : DWORD;
     rgUnauthAttr : PCRYPT_ATTRIBUTE;
     dwFlags : DWORD;
     dwInnerContentType : DWORD;
     HashEncryptionAlgorithm : CRYPT_ALGORITHM_IDENTIFIER ;
     pvHashEncryptionAuxInfo : pointer;
  end;
  {$ifdef VCL60}
  {$EXTERNALSYM CRYPT_SIGN_MESSAGE_PARA}
  {$endif}
  CRYPT_SIGN_MESSAGE_PARA =  _CRYPT_SIGN_MESSAGE_PARA;

  {$ifdef VCL60}
  {$EXTERNALSYM PCRYPT_SIGN_MESSAGE_PARA}
  {$endif}
  PCRYPT_SIGN_MESSAGE_PARA = ^_CRYPT_SIGN_MESSAGE_PARA;

  {$ifdef VCL60}
  {$EXTERNALSYM _CRYPT_DECRYPT_MESSAGE_PARA}
  {$endif}
  _CRYPT_DECRYPT_MESSAGE_PARA = record
    cbSize : DWORD;
    dwMsgAndCertEncodingType : DWORD;
    cCertStore : DWORD;
    rghCertStore : PHCERTSTORE;
  end;
  {$ifdef VCL60}
  {$EXTERNALSYM CRYPT_DECRYPT_MESSAGE_PARA}
  {$endif}
  CRYPT_DECRYPT_MESSAGE_PARA =  _CRYPT_DECRYPT_MESSAGE_PARA;
  {$ifdef VCL60}
  {$EXTERNALSYM PCRYPT_DECRYPT_MESSAGE_PARA}
  {$endif}
  PCRYPT_DECRYPT_MESSAGE_PARA = ^_CRYPT_DECRYPT_MESSAGE_PARA;

  {$ifdef VCL60}
  {$EXTERNALSYM _ALG_ID}
  {$endif}
  _ALG_ID =  longword;
  {$ifdef VCL60}
  {$EXTERNALSYM ALG_ID}
  {$endif}
  ALG_ID =  _ALG_ID;

  {$ifdef VCL60}
  {$EXTERNALSYM _PROV_ENUMALGS}
  {$endif}
  _PROV_ENUMALGS = record
    aiAlgId : ALG_ID;
    dwBitLen : DWORD;
    dwNameLen : DWORD;
    szName : array[0..19] of char;
  end;
  {$ifdef VCL60}
  {$EXTERNALSYM PROV_ENUMALGS}
  {$endif}
  PROV_ENUMALGS =  _PROV_ENUMALGS;
  {$ifdef VCL60}
  {$EXTERNALSYM PROV_ENUMALGS}
  {$endif}
  PPROV_ENUMALGS = ^_PROV_ENUMALGS;


  _CRYPT_OID_FUNC_ENTRY = record
    pszOID : PChar;
    pvFuncAddr : Pointer;
  end;
  {$ifdef VCL60}
  {$EXTERNALSYM CRYPT_OID_FUNC_ENTRY}
  {$endif}
  CRYPT_OID_FUNC_ENTRY =  _CRYPT_OID_FUNC_ENTRY;
  {$ifdef VCL60}
  {$EXTERNALSYM CRYPT_OID_FUNC_ENTRY}
  {$endif}
  PCRYPT_OID_FUNC_ENTRY = ^_CRYPT_OID_FUNC_ENTRY;

  _CERT_STORE_PROV_INFO = record
    cbSize : DWORD;
    cStoreProvFunc : DWORD;
    rgpvStoreProvFunc : pointer;
    hStoreProv : HCERTSTOREPROV;
    dwStoreProvFlags : DWORD;
    hStoreProvFuncAddr2 : HCRYPTOIDFUNCADDR;
  end;
  {$ifdef VCL60}
  {$EXTERNALSYM CERT_STORE_PROV_INFO}
  {$endif}
  CERT_STORE_PROV_INFO =  _CERT_STORE_PROV_INFO;
  {$ifdef VCL60}
  {$EXTERNALSYM PCERT_STORE_PROV_INFO}
  {$endif}
  PCERT_STORE_PROV_INFO = ^_CERT_STORE_PROV_INFO;

  {$ifdef VCL60}
  {$EXTERNALSYM PFN_CERT_ENUM_SYSTEM_STORE}
  {$endif}
  PFN_CERT_ENUM_SYSTEM_STORE = function (pvSystemStore: Pointer;
    dwFlags: DWORD; pStoreInfo: PCERT_SYSTEM_STORE_INFO; pvReserved: Pointer;
    pvArg: Pointer): BOOL; stdcall;
  PfnCertEnumSystemStore = PFN_CERT_ENUM_SYSTEM_STORE;

  TCertEnumSystemStore = function(dwFlags: DWORD; pvSystemStoreLocationPara: Pointer;
    pvArg: Pointer; pfnEnum: PFN_CERT_ENUM_SYSTEM_STORE): BOOL; stdcall;

  {$ifdef VCL60}
  {$EXTERNALSYM PFN_CERT_ENUM_PHYSICAL_STORE}
  {$endif}
  PFN_CERT_ENUM_PHYSICAL_STORE = function (pvSystemStore: pointer;
    dwFlags : DWORD; pwszStoreName : PChar; pStoreInfo : PCERT_SYSTEM_STORE_INFO;
    pvReserver : pointer; pvArg : pointer) : BOOL; stdcall;

  TCertEnumPhysicalStore = function(pvSystemStore : pointer; dwFlags: DWORD;
    pvArg: Pointer; pfnEnum: PFN_CERT_ENUM_PHYSICAL_STORE): BOOL; stdcall;

  TCryptSignMessage = function(pSignPara : PCRYPT_SIGN_MESSAGE_PARA; fDetachedSignature : BOOL;
    cToBeSigned : DWORD; const rgpbToBeSigned : PPBYTE; rgcbToBeSigned : PDWORD;
    pbSignedBlob : PBYTE; pcbSignedBlob : PDWORD) : BOOL; stdcall;

  TCryptFindLocalizedName = function(const pwszCryptName : PWideChar) : PWideChar; stdcall;

  //-----------------------------------------------------------------------------
    // Type support for a pointer to an array of pointer (type **name)
    PLPSTR          = Pointer; // type for a pointer to Array of pointer a type
    PPCERT_INFO     = Pointer; // type for a pointer to Array of pointer a type
    PPVOID          = Pointer; // type for a pointer to Array of pointer a type
    PPCCTL_CONTEXT  = Pointer; // type for a pointer to Array of pointer a type
    PPCCRL_CONTEXT  = Pointer; // type for a pointer to Array of pointer a type
  //-----------------------------------------------------------------------------
  HCRYPTMSG = Pointer;

  PCTL_USAGE =^CTL_USAGE;
  CTL_USAGE =  record
    cUsageIdentifier :DWORD;
    rgpszUsageIdentifier :PLPSTR;      // array of pszObjId
  end;

  CTL_ENTRY =  record
    SubjectIdentifier :CRYPT_DATA_BLOB;    // For example, its hash
    cAttribute        :DWORD;
    rgAttribute       :PCRYPT_ATTRIBUTE;   // OPTIONAL
  end;
  PCTL_ENTRY = ^CTL_ENTRY;


  CRL_ENTRY =  record
    SerialNumber :CRYPT_INTEGER_BLOB;
    RevocationDate :TFILETIME;
    cExtension :DWORD;
    rgExtension :PCERT_EXTENSION;
  end;
  PCRL_ENTRY = ^CRL_ENTRY;

  CTL_INFO =  record
    dwVersion           :DWORD;
    SubjectUsage        :CTL_USAGE;
    ListIdentifier      :CRYPT_DATA_BLOB;     // OPTIONAL
    SequenceNumber      :CRYPT_INTEGER_BLOB;  // OPTIONAL
    ThisUpdate          :TFILETIME;
    NextUpdate          :TFILETIME;           // OPTIONAL
    SubjectAlgorithm    :CRYPT_ALGORITHM_IDENTIFIER;
    cCTLEntry           :DWORD;
    rgCTLEntry          :PCTL_ENTRY;          // OPTIONAL
    cExtension          :DWORD;
    rgExtension         :PCERT_EXTENSION;     // OPTIONAL
  end;
  PCTL_INFO = ^CTL_INFO;

  CRL_INFO =  record
    dwVersion           :DWORD;
    SignatureAlgorithm  :CRYPT_ALGORITHM_IDENTIFIER;
    Issuer              :CERT_NAME_BLOB;
    ThisUpdate          :TFILETIME;
    NextUpdate          :TFILETIME;
    cCRLEntry           :DWORD;
    rgCRLEntry          :PCRL_ENTRY;
    cExtension          :DWORD;
    rgExtension         :PCERT_EXTENSION;
  end;
  PCRL_INFO = ^CRL_INFO;

  CRL_CONTEXT =  record
    dwCertEncodingType :DWORD;
    pbCrlEncoded :PBYTE;
    cbCrlEncoded :DWORD;
    pCrlInfo     :PCRL_INFO;
    hCertStore   :HCERTSTORE;
  end;
  PCRL_CONTEXT = ^CRL_CONTEXT;
  PCCRL_CONTEXT = ^CRL_CONTEXT;

  CTL_CONTEXT =  record
    dwMsgAndCertEncodingType :DWORD;
    pbCtlEncoded :PBYTE;
    cbCtlEncoded :DWORD;
    pCtlInfo     :PCTL_INFO;
    hCertStore   :HCERTSTORE;
    hCryptMsg    :HCRYPTMSG;
    pbCtlContent :PBYTE;
    cbCtlContent :DWORD;
  end;
  PCTL_CONTEXT = ^CTL_CONTEXT;
  PCCTL_CONTEXT = ^CTL_CONTEXT;

  CERT_STORE_PROV_FIND_INFO =  record
    cbSize : DWORD ;
    dwMsgAndCertEncodingType : DWORD;
    dwFindFlags : DWORD;
    dwFindType : DWORD;
    pvFindPara : Pointer;
  end;

  PCERT_STORE_PROV_FIND_INFO = ^CERT_STORE_PROV_FIND_INFO;
  CCERT_STORE_PROV_FIND_INFO =  CERT_STORE_PROV_FIND_INFO;
  PCCERT_STORE_PROV_FIND_INFO = ^CERT_STORE_PROV_FIND_INFO;

  {$ifdef B_5_UP}
  {$EXTERNALSYM _GUID}
  {$endif}
  _GUID =  record
    Data1 : DWORD;
    Data2 : WORD;
    Data3 : WORD;
    Data4 : array [0..7] of BYTE;
  end;

  {$ifdef B_5_UP}
  {$EXTERNALSYM GUID}
  {$endif}
  GUID =  _GUID;
  PGUID = ^_GUID;
  // IntPtr = ^integer;


const
  {$externalsym  CERT_STORE_ADD_NEW}
  CERT_STORE_ADD_NEW = 1;
  {$externalsym  CERT_STORE_ADD_REPLACE_EXISTING}
  CERT_STORE_ADD_REPLACE_EXISTING = 3;
  {$externalsym  CRYPT_ASN_ENCODING}
  CRYPT_ASN_ENCODING  = $00000001;
  {$externalsym  CRYPT_NDR_ENCODING}
  CRYPT_NDR_ENCODING = $00000002;

  {$externalsym CRYPT_E_UNKNOWN_ALGO}
  CRYPT_E_UNKNOWN_ALGO = $80091002;

  {$externalsym CRYPT_E_EXISTS}
  CRYPT_E_EXISTS = $80092005;

  {$externalsym  X509_ASN_ENCODING}
  X509_ASN_ENCODING = $00000001;

  {$externalsym  X509_NDR_ENCODING}
  X509_NDR_ENCODING = $00000002;

  {$externalsym  PKCS_7_ASN_ENCODING}
  PKCS_7_ASN_ENCODING = $00010000;

  {$externalsym  PKCS_7_NDR_ENCODING}
  PKCS_7_NDR_ENCODING = $00020000;

  {$externalsym  CERT_INFO_VERSION_FLAG}
  CERT_INFO_VERSION_FLAG                 = 1;

  {$externalsym  CERT_INFO_SERIAL_NUMBER_FLAG}
  CERT_INFO_SERIAL_NUMBER_FLAG           = 2;

  {$externalsym  CERT_INFO_SIGNATURE_ALGORITHM_FLAG}
  CERT_INFO_SIGNATURE_ALGORITHM_FLAG     = 3;

  {$externalsym  CERT_INFO_ISSUER_FLAG}
  CERT_INFO_ISSUER_FLAG                  = 4;

  {$externalsym  CERT_INFO_NOT_BEFORE_FLAG}
  CERT_INFO_NOT_BEFORE_FLAG              = 5;

  {$externalsym  CERT_INFO_NOT_AFTER_FLAG}
  CERT_INFO_NOT_AFTER_FLAG               = 6;

  {$externalsym  CERT_INFO_SUBJECT_FLAG}
  CERT_INFO_SUBJECT_FLAG                 = 7;

  {$externalsym  CERT_INFO_SUBJECT_PUBLIC_KEY_INFO_FLAG}
  CERT_INFO_SUBJECT_PUBLIC_KEY_INFO_FLAG = 8;

  {$externalsym  CERT_INFO_ISSUER_UNIQUE_ID_FLAG}
  CERT_INFO_ISSUER_UNIQUE_ID_FLAG        = 9;

  {$externalsym  CERT_INFO_SUBJECT_UNIQUE_ID_FLAG}
  CERT_INFO_SUBJECT_UNIQUE_ID_FLAG       = 10;

  {$externalsym  CERT_INFO_EXTENSION_FLAG}
  CERT_INFO_EXTENSION_FLAG               = 11;

  {$externalsym  CERT_COMPARE_SHIFT}
  CERT_COMPARE_SHIFT = 16;

  {$externalsym  CERT_COMPARE_ANY}
  CERT_COMPARE_ANY = 0;

  {$externalsym  CERT_COMPARE_SHA1_HASH}
  CERT_COMPARE_SHA1_HASH = 1;

  {$externalsym  CERT_COMPARE_NAME}
  CERT_COMPARE_NAME = 2;

  {$externalsym  CERT_COMPARE_ATTR}
  CERT_COMPARE_ATTR = 3;

  {$externalsym  CERT_COMPARE_MD5_HASH}
  CERT_COMPARE_MD5_HASH  = 4;

  {$externalsym  CERT_COMPARE_PROPERTY}
  CERT_COMPARE_PROPERTY = 5;

  {$externalsym CERT_COMPARE_PUBLIC_KEY}
  CERT_COMPARE_PUBLIC_KEY = 6;

  {$externalsym CERT_COMPARE_HASH}
  CERT_COMPARE_HASH = CERT_COMPARE_SHA1_HASH;

  {$externalsym CERT_COMPARE_NAME_STR_A}
  CERT_COMPARE_NAME_STR_A = 7;

  {$externalsym CERT_COMPARE_NAME_STR_W}
  CERT_COMPARE_NAME_STR_W = 8;

  {$externalsym CERT_COMPARE_KEY_SPEC}
  CERT_COMPARE_KEY_SPEC = 9;

  {$externalsym CERT_COMPARE_ENHKEY_USAGE}
  CERT_COMPARE_ENHKEY_USAGE = 10;

  {$externalsym CERT_COMPARE_CTL_USAGE}
  CERT_COMPARE_CTL_USAGE = CERT_COMPARE_ENHKEY_USAGE;

  {$externalsym CERT_FIND_ANY}
  CERT_FIND_ANY = (CERT_COMPARE_ANY shl CERT_COMPARE_SHIFT);

  {$externalsym CERT_FIND_SHA1_HASH}
  CERT_FIND_SHA1_HASH = (CERT_COMPARE_SHA1_HASH shl CERT_COMPARE_SHIFT);

  {$externalsym CERT_FIND_MD5_HASH}
  CERT_FIND_MD5_HASH = (CERT_COMPARE_MD5_HASH shl CERT_COMPARE_SHIFT);

  {$externalsym CERT_FIND_HASH}
  CERT_FIND_HASH = CERT_FIND_SHA1_HASH;

  {$externalsym CERT_FIND_PROPERTY}
  CERT_FIND_PROPERTY = (CERT_COMPARE_PROPERTY shl CERT_COMPARE_SHIFT);

  {$externalsym CERT_FIND_PUBLIC_KEY}
  CERT_FIND_PUBLIC_KEY = (CERT_COMPARE_PUBLIC_KEY shl CERT_COMPARE_SHIFT);

  {$externalsym CERT_FIND_SUBJECT_NAME}
  CERT_FIND_SUBJECT_NAME = (CERT_COMPARE_NAME shl CERT_COMPARE_SHIFT or CERT_INFO_SUBJECT_FLAG);

  {$externalsym CERT_FIND_SUBJECT_ATTR}
  CERT_FIND_SUBJECT_ATTR = (CERT_COMPARE_ATTR shl CERT_COMPARE_SHIFT or  CERT_INFO_SUBJECT_FLAG);

  {$externalsym CERT_FIND_ISSUER_NAME}
  CERT_FIND_ISSUER_NAME = (CERT_COMPARE_NAME shl CERT_COMPARE_SHIFT or  CERT_INFO_ISSUER_FLAG);

  {$externalsym CERT_FIND_ISSUER_ATTR}
  CERT_FIND_ISSUER_ATTR = (CERT_COMPARE_ATTR shl CERT_COMPARE_SHIFT or   CERT_INFO_ISSUER_FLAG);

  {$externalsym CERT_FIND_SUBJECT_STR_A}
  CERT_FIND_SUBJECT_STR_A =  (CERT_COMPARE_NAME_STR_A shl CERT_COMPARE_SHIFT or   CERT_INFO_SUBJECT_FLAG);

  {$externalsym CERT_FIND_SUBJECT_STR_W}
  CERT_FIND_SUBJECT_STR_W =  (CERT_COMPARE_NAME_STR_W shl CERT_COMPARE_SHIFT or   CERT_INFO_SUBJECT_FLAG);

  {$externalsym CERT_FIND_SUBJECT_STR}
  CERT_FIND_SUBJECT_STR = CERT_FIND_SUBJECT_STR_W;

  {$externalsym CERT_FIND_ISSUER_STR_A}
  CERT_FIND_ISSUER_STR_A = (CERT_COMPARE_NAME_STR_A shl CERT_COMPARE_SHIFT or  CERT_INFO_ISSUER_FLAG);

  {$externalsym CERT_FIND_ISSUER_STR_W}
  CERT_FIND_ISSUER_STR_W =  (CERT_COMPARE_NAME_STR_W shl CERT_COMPARE_SHIFT or  CERT_INFO_ISSUER_FLAG);

  {$externalsym CERT_FIND_ISSUER_STR}
  CERT_FIND_ISSUER_STR = CERT_FIND_ISSUER_STR_W;

  {$externalsym CERT_FIND_KEY_SPEC}
  CERT_FIND_KEY_SPEC = (CERT_COMPARE_KEY_SPEC shl CERT_COMPARE_SHIFT);

  {$externalsym CERT_FIND_ENHKEY_USAGE}
  CERT_FIND_ENHKEY_USAGE = (CERT_COMPARE_ENHKEY_USAGE shl CERT_COMPARE_SHIFT);

  {$externalsym CERT_FIND_CTL_USAGE}
  CERT_FIND_CTL_USAGE = CERT_FIND_ENHKEY_USAGE;

  {$externalsym CERT_SYSTEM_STORE_LOCATION_MASK}
  CERT_SYSTEM_STORE_LOCATION_MASK  = $00FF0000;
  {$EXTERNALSYM CERT_SYSTEM_STORE_LOCATION_MASK}
  CERT_SYSTEM_STORE_LOCATION_SHIFT = 16;
  {$EXTERNALSYM CERT_SYSTEM_STORE_LOCATION_SHIFT}
  CERT_SYSTEM_STORE_CURRENT_USER_ID  = 1;
  {$EXTERNALSYM CERT_SYSTEM_STORE_CURRENT_USER_ID}
  CERT_SYSTEM_STORE_LOCAL_MACHINE_ID = 2;
  {$EXTERNALSYM CERT_SYSTEM_STORE_LOCAL_MACHINE_ID}
  CERT_SYSTEM_STORE_CURRENT_SERVICE_ID = 4;
  {$EXTERNALSYM CERT_SYSTEM_STORE_CURRENT_SERVICE_ID}
  CERT_SYSTEM_STORE_SERVICES_ID        = 5;
  {$EXTERNALSYM CERT_SYSTEM_STORE_SERVICES_ID}
  CERT_SYSTEM_STORE_USERS_ID = 6;
  {$EXTERNALSYM CERT_SYSTEM_STORE_USERS_ID}
  CERT_SYSTEM_STORE_CURRENT_USER_GROUP_POLICY_ID = 7;
  {$EXTERNALSYM CERT_SYSTEM_STORE_CURRENT_USER_GROUP_POLICY_ID}
  CERT_SYSTEM_STORE_LOCAL_MACHINE_GROUP_POLICY_ID = 8;
  {$EXTERNALSYM CERT_SYSTEM_STORE_LOCAL_MACHINE_GROUP_POLICY_ID}
  CERT_SYSTEM_STORE_LOCAL_MACHINE_ENTERPRISE_ID = 9;
  {$EXTERNALSYM CERT_SYSTEM_STORE_LOCAL_MACHINE_ENTERPRISE_ID}
  CERT_SYSTEM_STORE_CURRENT_USER    = (CERT_SYSTEM_STORE_CURRENT_USER_ID shl CERT_SYSTEM_STORE_LOCATION_SHIFT);
  {$EXTERNALSYM CERT_SYSTEM_STORE_CURRENT_USER}
  CERT_SYSTEM_STORE_LOCAL_MACHINE   = (CERT_SYSTEM_STORE_LOCAL_MACHINE_ID shl CERT_SYSTEM_STORE_LOCATION_SHIFT);
  {$EXTERNALSYM CERT_SYSTEM_STORE_LOCAL_MACHINE}
  CERT_SYSTEM_STORE_CURRENT_SERVICE = (CERT_SYSTEM_STORE_CURRENT_SERVICE_ID shl CERT_SYSTEM_STORE_LOCATION_SHIFT);
  {$EXTERNALSYM CERT_SYSTEM_STORE_CURRENT_SERVICE}
  CERT_SYSTEM_STORE_SERVICES        = (CERT_SYSTEM_STORE_SERVICES_ID shl CERT_SYSTEM_STORE_LOCATION_SHIFT);
  {$EXTERNALSYM CERT_SYSTEM_STORE_SERVICES}
  CERT_SYSTEM_STORE_USERS           = (CERT_SYSTEM_STORE_USERS_ID shl CERT_SYSTEM_STORE_LOCATION_SHIFT);
  {$EXTERNALSYM CERT_SYSTEM_STORE_USERS}
  CERT_SYSTEM_STORE_CURRENT_USER_GROUP_POLICY   = (CERT_SYSTEM_STORE_CURRENT_USER_GROUP_POLICY_ID shl CERT_SYSTEM_STORE_LOCATION_SHIFT);
  {$EXTERNALSYM CERT_SYSTEM_STORE_CURRENT_USER_GROUP_POLICY}
  CERT_SYSTEM_STORE_LOCAL_MACHINE_ENTERPRISE = (CERT_SYSTEM_STORE_LOCAL_MACHINE_ENTERPRISE_ID shl CERT_SYSTEM_STORE_LOCATION_SHIFT);
  {$EXTERNALSYM CERT_SYSTEM_STORE_LOCAL_MACHINE_ENTERPRISE}
  CERT_SYSTEM_STORE_LOCAL_MACHINE_GROUP_POLICY = (CERT_SYSTEM_STORE_LOCAL_MACHINE_GROUP_POLICY_ID shl CERT_SYSTEM_STORE_LOCATION_SHIFT);
  {$EXTERNALSYM CERT_SYSTEM_STORE_LOCAL_MACHINE_GROUP_POLICY}
  CERT_STORE_OPEN_EXISTING_FLAG     = $00004000;
  {$EXTERNALSYM CERT_STORE_OPEN_EXISTING_FLAG}
  CERT_STORE_CREATE_NEW_FLAG        = $00002000;
  {$EXTERNALSYM CERT_STORE_CREATE_NEW_FLAG}
  CERT_STORE_DELETE_FLAG            = $00000010;
  {$EXTERNALSYM CERT_STORE_DELETE_FLAG}


  CRYPT_OID_OPEN_STORE_PROV_FUNC        = 'CertDllOpenStoreProv';

  {$EXTERNALSYM CERT_STORE_PROV_PHYSICAL}
  CERT_STORE_PROV_PHYSICAL              = 14;
  {$EXTERNALSYM CERT_KEY_PROV_INFO_PROP_ID}
  CERT_KEY_PROV_INFO_PROP_ID            = 2;
  {$EXTERNALSYM CERT_KEY_SPEC_PROP_ID}
  CERT_KEY_SPEC_PROP_ID                 = 6;

  {$EXTERNALSYM AT_KEYEXCHANGE}
  AT_KEYEXCHANGE                        = 1;
  {$EXTERNALSYM AT_SIGNATURE}
  AT_SIGNATURE                          = 2;
  {$EXTERNALSYM PRIVATEKEYBLOB}
  PRIVATEKEYBLOB                        = 7;
  {$EXTERNALSYM CRYPT_SILENT}
  CRYPT_SILENT                          = $40;
  {$EXTERNALSYM CRYPT_MESSAGE_SILENT_KEYSET_FLAG}
  CRYPT_MESSAGE_SILENT_KEYSET_FLAG      = $40;
  szOID_RSA_MD5                         = '1.2.840.113549.2.5';
  {$EXTERNALSYM ALG_CLASS_HASH}
  ALG_CLASS_HASH                        = 4 shl 13;
  {$EXTERNALSYM ALG_TYPE_ANY}
  ALG_TYPE_ANY                          = 0;
  {$EXTERNALSYM ALG_SID_SSL3SHAMD5}
  ALG_SID_SSL3SHAMD5                    = 8;
  {$EXTERNALSYM ALG_SID_SHA1}
  ALG_SID_SHA1                          = 4;
  {$EXTERNALSYM ALG_SID_MD2}
  ALG_SID_MD2                           = 1;
  {$EXTERNALSYM ALG_SID_MD5}
  ALG_SID_MD5                           = 3;
  {$EXTERNALSYM CALG_SSL3_SHAMD5}
  CALG_SSL3_SHAMD5                      = ALG_CLASS_HASH or ALG_TYPE_ANY or ALG_SID_SSL3SHAMD5;
  {$EXTERNALSYM CALG_SHA1}
  CALG_SHA1                             = ALG_CLASS_HASH or ALG_TYPE_ANY or ALG_SID_SHA1;
  {$EXTERNALSYM CALG_MD2}
  CALG_MD2                              = ALG_CLASS_HASH or ALG_TYPE_ANY or ALG_SID_MD2;
  {$EXTERNALSYM CALG_MD5}
  CALG_MD5                              = ALG_CLASS_HASH or ALG_TYPE_ANY or ALG_SID_MD5;


  {$EXTERNALSYM HP_HASHVAL}
  HP_HASHVAL                            = $0002;

  {$EXTERNALSYM CRYPT_E_UNEXPECTED_MSG_TYPE}
  CRYPT_E_UNEXPECTED_MSG_TYPE           = $8009200A;
  _NTE_BAD_ALGID                        = $80090008;
  {$EXTERNALSYM CRYPT_E_NO_DECRYPT_CERT}
  CRYPT_E_NO_DECRYPT_CERT               = $8009200C;

  {$EXTERNALSYM PP_ENUMALGS}
  PP_ENUMALGS                           = 1;
  {$EXTERNALSYM CRYPT_FIRST}
  CRYPT_FIRST                           = 1;
  {$EXTERNALSYM CRYPT_NEXT}
  CRYPT_NEXT                            = 2;

  {$EXTERNALSYM CRYPT_EXPORTABLE}
  CRYPT_EXPORTABLE                      = 1;
  {$EXTERNALSYM CRYPT_USER_PROTECTED}
  CRYPT_USER_PROTECTED                  = 2;

  {.$EXTERNALSYM PROV_RSA}
  PROV_RSA                              = 1;
  {$EXTERNALSYM PROV_DSS}
  PROV_DSS                              = 3;
  {$EXTERNALSYM PROV_SSL}
  PROV_SSL                              = 6;
  {$EXTERNALSYM PROV_RSA_SCHANNEL}
  PROV_RSA_SCHANNEL                     = 12;
  {$EXTERNALSYM PROV_RSA_SIG}
  PROV_RSA_SIG                          = 2;
  {$EXTERNALSYM PROV_DSS_DH}
  PROV_DSS_DH                           = 13;
  {$EXTERNALSYM PROV_DH_SCHANNEL}
  PROV_DH_SCHANNEL                      = 18;
  {$EXTERNALSYM PROV_RSA_AES}
  PROV_RSA_AES                          = 24;

  {$EXTERNALSYM CRYPT_NEWKEYSET}
  CRYPT_NEWKEYSET                       = 8;
  {$EXTERNALSYM CRYPT_MACHINE_KEYSET}
  CRYPT_MACHINE_KEYSET                  = 32;

  {$EXTERNALSYM CERT_KEY_PROV_HANDLE_PROP_ID}
  CERT_KEY_PROV_HANDLE_PROP_ID          = 1;

  {$EXTERNALSYM CERT_STORE_PROV_MSG}
  CERT_STORE_PROV_MSG                   = #1;
  {$EXTERNALSYM CERT_STORE_PROV_MEMORY}
  CERT_STORE_PROV_MEMORY                = #2;
  {$EXTERNALSYM CERT_STORE_PROV_FILE}
  CERT_STORE_PROV_FILE                  = #3;
  {$EXTERNALSYM CERT_STORE_PROV_REG}
  CERT_STORE_PROV_REG                   = #4;
  {$EXTERNALSYM CERT_STORE_PROV_PKCS7}
  CERT_STORE_PROV_PKCS7                 = #5;
  {$EXTERNALSYM CERT_STORE_PROV_SERIALIZED}
  CERT_STORE_PROV_SERIALIZED            = #6;
  {$EXTERNALSYM CERT_STORE_PROV_FILENAME_A}
  CERT_STORE_PROV_FILENAME_A            = #7;
  {$EXTERNALSYM CERT_STORE_PROV_FILENAME_W}
  CERT_STORE_PROV_FILENAME_W            = #8;
  {$EXTERNALSYM CERT_STORE_PROV_FILENAME}
  CERT_STORE_PROV_FILENAME              = CERT_STORE_PROV_FILENAME_W;
  {$EXTERNALSYM CERT_STORE_PROV_SYSTEM_A}
  CERT_STORE_PROV_SYSTEM_A              = #9;
  {$EXTERNALSYM CERT_STORE_PROV_SYSTEM_W}
  CERT_STORE_PROV_SYSTEM_W              = #10;
  {$EXTERNALSYM CERT_STORE_PROV_SYSTEM}
  CERT_STORE_PROV_SYSTEM                = CERT_STORE_PROV_SYSTEM_W;
  {$EXTERNALSYM CERT_STORE_PROV_LDAP_W}
  CERT_STORE_PROV_LDAP_W                = #16;
  {$EXTERNALSYM CERT_STORE_PROV_LDAP}
  CERT_STORE_PROV_LDAP                  = CERT_STORE_PROV_LDAP_W;
  {$EXTERNALSYM CRYPT_MACHINE_DEFAULT}
  CRYPT_MACHINE_DEFAULT                 = $00000001;
  {$EXTERNALSYM CRYPT_USER_DEFAULT}
  CRYPT_USER_DEFAULT                    = $00000002;

  {$EXTERNALSYM PP_NAME}
  PP_NAME                               = 4;


  MS_DEF_PROV                             = 'Microsoft Base Cryptographic Provider v1.0';
  MS_ENHANCED_PROV                        = 'Microsoft Enhanced Cryptographic Provider v1.0';
  MS_ENH_DSS_DH_PROV                      = 'Microsoft Enhanced DSS and Diffie-Hellman Cryptographic Provider';
  MS_DEF_RSA_SIG_PROV                     = 'Microsoft RSA Signature Cryptographic Provider';
  MS_DEF_RSA_SCHANNEL_PROV                = 'Microsoft RSA SChannel Cryptographic Provider';
  MS_ENHANCED_RSA_SCHANNEL_PROV           = 'Microsoft Enhanced RSA SChannel Cryptographic Provider';
  MS_DEF_DSS_PROV                         = 'Microsoft Base DSS Cryptographic Provider';
  MS_DEF_DSS_DH_PROV                      = 'Microsoft Base DSS and Diffie-Hellman Cryptographic Provider';
  MS_ENH_RSA_AES_PROV                     = 'Microsoft Enhanced RSA and AES Cryptographic Provider';
  MS_SCARD_PROV                           = 'Microsoft Base Smart Card Crypto Provider';
  MS_STRONG_PROV                          = 'Microsoft Strong Cryptographic Provider';

function CertOpenSystemStore(hProv :HCRYPTPROV;
                             szSubsystemProtocol :LPAWSTR):HCERTSTORE ; stdcall;

function CertFindCertificateInStore(hCertStore :HCERTSTORE;
                                    dwCertEncodingType :DWORD;
                                    dwFindFlags :DWORD;
                                    dwFindType :DWORD;
                              const pvFindPara :PVOID;
                                    pPrevCertContext :PCCERT_CONTEXT
                                    ):PCCERT_CONTEXT ; stdcall;

function CertCloseStore(hCertStore :HCERTSTORE; dwFlags :DWORD):BOOL ; stdcall;

function CertEnumSystemStore(dwFlags: DWORD; pvSystemStoreLocationPara: Pointer;
  pvArg: Pointer; pfnEnum: PFN_CERT_ENUM_SYSTEM_STORE): BOOL; stdcall;

function CertAddEncodedCertificateToStore(hCertStore :HCERTSTORE;
                                          dwCertEncodingType :DWORD;
                                    const pbCertEncoded :PBYTE;
                                          cbCertEncoded :DWORD;
                                          dwAddDisposition :DWORD;
                                      var ppCertContext :PCCERT_CONTEXT):BOOL ; stdcall;

function CertFreeCertificateContext(pCertContext :PCCERT_CONTEXT):BOOL ; stdcall;

function CertDeleteCertificateFromStore(pCertContext :PCCERT_CONTEXT):BOOL ; stdcall;

function CertEnumCertificatesInStore(hCertStore : HCERTSTORE; pPrevCertContext :
  PCCERT_CONTEXT) : PCCERT_CONTEXT; stdcall;

function CertDuplicateCertificateContext(pCertContext : PCCERT_CONTEXT) : PCCERT_CONTEXT; stdcall;

function CertEnumPhysicalStore(pvSystemStore : pointer; dwFlags : DWORD; pvArg :
  pointer; pfnEnum : PFN_CERT_ENUM_PHYSICAL_STORE) : BOOL; stdcall;

function CertOpenStore(lpszStoreProvider : PChar; dwMsgAndCertEncodingType : DWORD;
  hCryptProv : HCRYPTPROV; dwFlags : DWORD; const pvPara : pointer) : HCERTSTORE; stdcall;

function CertGetCertificateContextProperty(pCertContext : PCCERT_CONTEXT; dwPropId : DWORD;
  pvData : pointer; pcbData : PDWORD) : BOOL; stdcall;

function CertSetCertificateContextProperty(pCertContext : PCCERT_CONTEXT;
  dwPropId : DWORD; dwFlags : DWORD; pvData  : Pointer) : BOOL; stdcall;

function CryptAcquireContext(hProv : PHCRYPTPROV; pszContainer : PChar;
  pszProvider : PChar; dwProvType : DWORD; dwFlags : DWORD) : BOOL; stdcall;

function CryptGetUserKey(hProv : HCRYPTPROV; dwKeySpec : DWORD; phUserKey : PHCRYPTKEY) : BOOL; stdcall;

function CryptDestroyKey(hKey : HCRYPTKEY) : BOOL; stdcall;

function CryptReleaseContext(hProv : HCRYPTPROV; dwFlags : DWORD) : BOOL; stdcall;

function CryptExportKey(hKey : HCRYPTKEY; hExpKey : HCRYPTKEY; dwBlobType : DWORD;
  dwFlags : DWORD; pbData : PBYTE; pdwDataLen : PDWORD) : BOOL; stdcall;

function CryptImportKey(hProv : HCRYPTPROV; pbData : PBYTE; dwDataLen : DWORD;
  hPubKey : HCRYPTKEY; dwFlags : DWORD; phKey : PHCRYPTKEY) : BOOL; stdcall;

{$ifndef NET_CF}
function CryptSignMessage(pSignPara : PCRYPT_SIGN_MESSAGE_PARA; fDetachedSignature : BOOL;
  cToBeSigned : DWORD; const rgpbToBeSigned : PPBYTE; rgcbToBeSigned : PDWORD;
  pbSignedBlob : PBYTE; pcbSignedBlob : PDWORD) : BOOL; stdcall;
{$endif}

function CryptCreateHash(hProv : HCRYPTPROV; AlgId : ALG_ID; hKey : HCRYPTKEY;
  dwFlags : DWORD; phHash : PHCRYPTHASH) : BOOL; stdcall;

function CryptSetHashParam(hHash : HCRYPTHASH; dwParam : DWORD; pbData : PBYTE;
  dwFlags : DWORD) : BOOL; stdcall;

function CryptSignHash(hHash : HCRYPTHASH; dwKeySpec : DWORD; sDescription : PChar;
  dwFlags : DWORD; pbSignature : PBYTE; pdwSigLen : PDWORD) : BOOL; stdcall;

function CryptDestroyHash(hHash : HCRYPTHASH) : BOOL; stdcall;

{$ifndef NET_CF}
function CryptFindLocalizedName(const pwszCryptName : PWideChar) : PWideChar; stdcall;

function CryptDecryptMessage(pDecryptPara : PCRYPT_DECRYPT_MESSAGE_PARA;
  pbEncryptedBlob : PBYTE; cbEncryptedBlob : DWORD; pbDecrypted : PBYTE;
  pcbDecrypted : PDWORD; ppXchgCert : PPCCERT_CONTEXT) : BOOL; stdcall;
{$endif}

function CertCreateCertificateContext(dwCertEncodingType : DWORD;
  const pbCertEncoded : PBYTE; cbCertEncoded : DWORD) : PCCERT_CONTEXT; stdcall;

function CryptGetProvParam(hProv : HCRYPTPROV; dwParam : DWORD; pbData : PBYTE;
  pwdDataLen : PDWORD; dwFlags : DWORD) : BOOL; stdcall;

{$ifndef NET_CF}
function CryptRegisterOIDFunction(dwEncodingType : DWORD; pszFuncName : PChar;
  pszOID : PChar; pwszDll : PWideChar; pszOverrideFuncName : PChar) : BOOL; stdcall;

function CryptUnregisterOIDFunction(dwEncodingType : DWORD; pszFuncName : PChar;
  pszOID : PChar) : BOOL; stdcall;
{$endif}

function CryptInstallOIDFunctionAddress(hModule : HModule; dwEncodingType : DWORD;
  pszFuncName : PChar; cFuncEntry : DWORD; rgFuncEntry : PCRYPT_OID_FUNC_ENTRY;
  dwFlags : DWORD) : BOOL; stdcall;

function CoCreateGuid(guid : PGUID) : HRESULT; stdcall;


procedure GetProcedureAddress(var P: Pointer; const ModuleName, ProcName: string);

implementation

function CertOpenSystemStore; external CRYPT32 name 'CertOpenSystemStoreA';
function CertFindCertificateInStore; external CRYPT32 name 'CertFindCertificateInStore';
function CertCloseStore; external CRYPT32 name 'CertCloseStore';
function CertAddEncodedCertificateToStore; external CRYPT32 name 'CertAddEncodedCertificateToStore';
function CertFreeCertificateContext; external CRYPT32 name 'CertFreeCertificateContext';
function CertDeleteCertificateFromStore; external CRYPT32 name 'CertDeleteCertificateFromStore';
function CertEnumCertificatesInStore; external CRYPT32 name 'CertEnumCertificatesInStore';
function CertDuplicateCertificateContext; external CRYPT32 name 'CertDuplicateCertificateContext';
function CertOpenStore; external CRYPT32 name 'CertOpenStore';
function CertGetCertificateContextProperty; external CRYPT32 name 'CertGetCertificateContextProperty';
function CertSetCertificateContextProperty; external CRYPT32 name 'CertSetCertificateContextProperty';
function CryptDecryptMessage; external CRYPT32 name 'CryptDecryptMessage';
function CertCreateCertificateContext; external CRYPT32 name 'CertCreateCertificateContext';

function CryptAcquireContext; external ADVAPI32 name 'CryptAcquireContextA';
function CryptGetUserKey; external ADVAPI32 name 'CryptGetUserKey';
function CryptDestroyKey; external ADVAPI32 name 'CryptDestroyKey';
function CryptReleaseContext; external ADVAPI32 name 'CryptReleaseContext';
function CryptExportKey; external ADVAPI32 name 'CryptExportKey';
function CryptImportKey; external ADVAPI32 name 'CryptImportKey';
function CryptCreateHash; external ADVAPI32 name 'CryptCreateHash';
function CryptSetHashParam; external ADVAPI32 name 'CryptSetHashParam';
function CryptSignHash; external ADVAPI32 name 'CryptSignHashA';
function CryptDestroyHash; external ADVAPI32 name 'CryptDestroyHash';
function CryptGetProvParam; external ADVAPI32 name 'CryptGetProvParam';
function CryptRegisterOIDFunction; external CRYPT32 name 'CryptRegisterOIDFunction';
function CryptUnregisterOIDFunction; external CRYPT32 name 'CryptUnregisterOIDFunction';
function CryptInstallOIDFunctionAddress; external CRYPT32 name 'CryptInstallOIDFunctionAddress';
function CoCreateGuid; external OLE32 name 'CoCreateGuid';

var
  _CertEnumSystemStore: Pointer;
  _CertEnumPhysicalStore: Pointer;
  _CryptSignMessage: Pointer;
  _CryptFindLocalizedName : pointer;

function CryptSignMessage(pSignPara : PCRYPT_SIGN_MESSAGE_PARA; fDetachedSignature : BOOL;
  cToBeSigned : DWORD; const rgpbToBeSigned : PPBYTE; rgcbToBeSigned : PDWORD;
  pbSignedBlob : PBYTE; pcbSignedBlob : PDWORD) : BOOL; stdcall;
begin
  GetProcedureAddress(_CryptSignMessage, crypt32, 'CryptSignMessage');
  if _CryptSignMessage = nil then
    Result := false
  else
    Result := TCryptSignMessage(_CryptSignMessage)(pSignPara, fDetachedSignature,
      cToBeSigned, rgpbToBeSigned, rgcbToBeSigned, pbSignedBlob, pcbSignedBlob);
end;

function CertEnumSystemStore(dwFlags: DWORD; pvSystemStoreLocationPara: Pointer;
  pvArg: Pointer; pfnEnum: PFN_CERT_ENUM_SYSTEM_STORE): BOOL; stdcall;
begin
  GetProcedureAddress(_CertEnumSystemStore, crypt32, 'CertEnumSystemStore');
  if _CertEnumSystemStore = nil then
    Result := false
  else
    Result := TCertEnumSystemStore(_CertEnumSystemStore)(dwFlags, pvSystemStoreLocationPara, pvArg, pfnEnum);
end;

function CertEnumPhysicalStore(pvSystemStore : pointer; dwFlags : DWORD; pvArg :
  pointer; pfnEnum : PFN_CERT_ENUM_PHYSICAL_STORE): BOOL; stdcall;
begin
  GetProcedureAddress(_CertEnumPhysicalStore, crypt32, 'CertEnumPhysicalStore');
  if _CertEnumPhysicalStore = nil then
    Result := false
  else
    Result := TCertEnumPhysicalStore(_CertEnumPhysicalStore)(pvSystemStore,
      dwFlags, pvArg, pfnEnum);
end;


procedure GetProcedureAddress(var P: Pointer; const ModuleName, ProcName: string);
var ModuleHandle : HMODULE;
begin
  if not Assigned(P) then
  begin
    ModuleHandle := GetModuleHandle(PChar(ModuleName));
    if ModuleHandle = 0 then
    begin
      ModuleHandle := LoadLibrary(PChar(ModuleName));
      if ModuleHandle = 0 then Exit;
    end;
    P := GetProcAddress(ModuleHandle, PChar(ProcName));
    if not Assigned(P) then Exit;
  end;
end;

function CryptFindLocalizedName(const pwszCryptName : PWideChar) : PWideChar; stdcall;
const
  Ptr : Pointer = nil;
begin
  GetProcedureAddress(_CryptFindLocalizedName, crypt32, 'CryptFindLocalizedName');
  if _CryptFindLocalizedName = nil then
    Result := nil
  else
    Result := TCryptFindLocalizedName(_CryptFindLocalizedName)(pwszCryptName);
end;

{$ifdef NET_CF_1_0}
[DllImport(OLE32, CharSet = CharSet.Unicode, EntryPoint = 'CoCreateGuid')]
function CoCreateGuid(out GUID: System.GUID): Integer; external;
{$endif}


end.

