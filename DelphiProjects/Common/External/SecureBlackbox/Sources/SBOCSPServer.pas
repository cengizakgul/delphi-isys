(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBOCSPServer;

interface

uses
  Classes,
  SysUtils,
  SBUtils,
  SBASN1,
  SBASN1Tree,
  SBRDN,
  SBSHA,
//  SBX509,
  SBX509Ext,
  SBOCSPCommon,
  SBCustomCertStorage;

type

  TElOCSPServer = class(TSBControlBase)
  private
  protected
    FOnCertificateCheck : TElCertificateOCSPCheckEvent;
    FResponderName : TElRelativeDistinguishedName;
    FResponderIdType : TElResponderIDType;
    FResponderKeyHash : ByteArray; 
    
    FCertID: integer;
    FCertIDlast: integer;
    FNonce: BufferType;
    FRequestBuf: ByteArray;
    FCertIDdata: ByteArray;
    FCertReplies: TStringList;
//    FReplyBuf2: ByteArray;
    FNesting: integer;
    FParseState: integer;
    FParseState2: integer;
//    FParseCert: integer;
//    FRespStatus: TElOCSPServerError;
    FASN1Parser : TElASN1Parser;

  protected
    procedure AddToReply(
      CertID : integer;
      var CertStatus : TElOCSPCertificateStatus;
      var Reason : TSBCRLReasonFlag;
      var RevocationTime, ThisUpdate, NextUpdate : TDateTime);

    procedure HandleASN1Read(Sender: TObject; Buffer: pointer; var Size: longint);
    procedure HandleASN1Tag(Sender: TObject; TagType: asn1TagType;
      TagConstrained: boolean; Tag: pointer; TagSize: integer; Size: integer; Data: pointer;
      BitRest: integer; var Valid: boolean);

  public
    constructor Create(Owner: TSBComponentBase);  override;
    destructor Destroy; override;
    
    function ProcessRequest(const Request : ByteArray; var Reply : ByteArray) : integer;

    property ResponderName : TElRelativeDistinguishedName read FResponderName;
    property ResponderKeyHash : ByteArray read FResponderKeyHash;
    property ResponderIdType : TElResponderIDType read FResponderIdType write FResponderIdType;

  published
  
    property OnCertificateCheck : TElCertificateOCSPCheckEvent read FOnCertificateCheck write FOnCertificateCheck;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('PKIBlackbox', [TElOCSPServer]);
end;

constructor TElOCSPServer.Create(Owner: TSBComponentBase);
begin
  inherited Create(Owner);
  FResponderName := TElRelativeDistinguishedName.Create;
end;


destructor TElOCSPServer.Destroy;
begin
  FreeAndNil(FResponderName);
  inherited;
end;


procedure TElOCSPServer.AddToReply(
  CertID : integer;
  var CertStatus : TElOCSPCertificateStatus;
  var Reason : TSBCRLReasonFlag;
  var RevocationTime, ThisUpdate, NextUpdate : TDateTime);
var
  j: TStringList;

  function RevokedInfo: BufferType;
  var
    r: BufferType;
    i: TStringList;
  begin
    i := TStringList.Create;
    i.Add(WriteGeneralizedTime(RevocationTime));
    if Reason > rfUnspecified then
    begin
      SetLength(r, 3);
      r[1] := char(asn1Enumerated);
      r[2] := #1;
      r[3] := char(Reason);
      i.Add(WritePrimitive($A0, r));
    end;
    result := WriteSequence(i);
    i.Free;
  end;

var
  r, n: BufferType;
begin
  j := TStringList.Create;
  r := WritePrimitive($30, StringOfBytes(FCertIDdata));
  j.Add(r);
  SetLength(n, 0);
  case CertStatus of
    csGood:
      j.Add(WritePrimitive($80, n));
    csRevoked:
      j.Add(WritePrimitive($81, RevokedInfo));
    else
      j.Add(WritePrimitive($82, n));
  end;
  j.Add(WriteGeneralizedTime(ThisUpdate));
  if NextUpdate <> 0 then
    j.Add(WriteExplicit(WriteGeneralizedTime(NextUpdate)));
  FCertReplies.Add(WriteSequence(j));
  j.Free;
end;

procedure TElOCSPServer.HandleASN1Read(Sender : TObject; Buffer : pointer; var Size : longint);
begin
  if Size > Length(FRequestBuf) then
  begin
    Move(FRequestBuf[0], Buffer^, Length(FRequestBuf));
    Size := Length(FRequestBuf);
    SetLength(FRequestBuf, 0);
  end
  else
  begin
    Move(FRequestBuf[0], Buffer^, Size);
    FRequestBuf := {$ifndef FPC}Copy{$else}SBCopy{$endif}(FRequestBuf, Size, Length(FRequestBuf));
  end;
end;

procedure TElOCSPServer.HandleASN1Tag(Sender : TObject; TagType: asn1TagType;
  TagConstrained: boolean; Tag: pointer; TagSize: integer; Size: integer; Data: pointer;
  BitRest: integer; var Valid : boolean);
var
  s: BufferType;
  CertStatus : TElOCSPCertificateStatus;
  Reason : TSBCRLReasonFlag;
  RevocationTime, ThisUpdate, NextUpdate : TDateTime;
begin
  if not Assigned(Tag) then
  begin
    Dec(FNesting);
    Exit;
  end;
  while true do
  begin
(*  write('{s:', FParseState, '/s2:', FParseState2, '/n:', FNesting, '/t:', integer(TagType), '}');
  writeln('.', StringOfChar('-', FNesting * 2),
    {$ifdef DELPHI_NET}
    Tag[0]
    {$else}
    PByte(Tag)^
    {$endif}
    , '-> ', Size, ' [', FNesting, ']');*)
    case FParseState of
      0: // OCSPRequest
        if PByte(Tag)^ = asn1Sequence then
          inc(FParseState)
        else
          FParseState := -1;
      1: // OCSPRequest content
        if (FNesting > 1) then
        else
        if (PByte(Tag)^ = asn1Sequence) then
          inc(FParseState)
        else
        if (TagType <> asn1tSpecific) or
          not (PByte(Tag)^ = 0) then
          FParseState := -1
        else
        begin
          dec(FParseState);
          continue;
        end;
      2: // TBSRequest
        if (FNesting > 2) then
        else
        if (FNesting < 2) then
        begin
          dec(FParseState);
          continue;
        end
        else
        if (PByte(Tag)^ = asn1Sequence) then
          inc(FParseState) // requestList
        else
        if (TagType = asn1tSpecific) then
        begin
          case PByte(Tag)^ of
            2:
              inc(FParseState, 5); // extensions - nonce
            else
            begin
              dec(FParseState);
              continue;
            end;
          end;
        end
        else
          FParseState := -1;
      3: // Request
        if (FNesting > 4) then
        else
        if (FNesting < 3) then
        begin
          dec(FParseState);
          continue;
        end
        else
        if (PByte(Tag)^ = asn1Sequence) then
          inc(FParseState)
        else
        if (TagType <> asn1tSpecific) or
          not (PByte(Tag)^ = 0) then
          FParseState := -1
        else
        begin
          dec(FParseState);
          continue;
        end;
      4: // CertID
        if PByte(Tag)^ = asn1Sequence then
        begin
          FCertIDdata := copy(FRequestBuf, 0, Length(FRequestBuf));
          inc(FCertID);
          inc(FParseState);
        end
        else
          FParseState := -1;
      5: // CertID content
        if (FNesting < 5) then
        begin
          inc(FParseState);
          continue;
        end
        else
          FCertIDlast := Length(FRequestBuf);
      6: // Cert check
        begin
          SetLength(FCertIDdata, Length(FCertIDdata) - FCertIDlast);
          FOnCertificateCheck(self, FCertID, CertStatus, Reason, RevocationTime, ThisUpdate, NextUpdate);
          AddToReply(FCertID, CertStatus, Reason, RevocationTime, ThisUpdate, NextUpdate);
          dec(FParseState, 3);
          continue;
        end;
      7: // extensions
        while true do
        begin
          case FParseState2 of
            0: // extensions
              if PByte(Tag)^ = asn1Sequence then
                inc(FParseState2)
              else
                FParseState := -1;
            1: // extension
              if PByte(Tag)^ = asn1Sequence then
                inc(FParseState2)
              else
                FParseState := -1;
            2: 
              if PByte(Tag)^ = asn1Object then
              begin
                SetLength(s, Size);
                Move(Data^, s[1], Size);
                if s = #$2B#$06#$01#$05#$05#$07#$30#$01#$02 then
                  inc(FParseState2)
                else
                  inc(FParseState2, 2);
              end
              else
                FParseState := -1;
            3:
              if PByte(Tag)^ = asn1OctetStr then
              begin
                SetLength(FNonce, Size);
                Move(Data^, FNonce[1], Size);
                FParseState2 := 4;
              end
              else
                FParseState := -1;
            4:
              if FNesting < 4 then
                inc(FParseState)
              else
              if FNesting < 5 then
              begin
                FParseState2 := 1;
                continue;
              end;
          end;
          break;
        end;
    end;
    break;
  end;
  if FParseState = -1 then
    raise EOCSPParseError.Create('client request parsing failure');
  if (TagConstrained) then
    Inc(FNesting);
end;

function WriteNonce(value: BufferType): BufferType;
var
  s: BufferType;
  l: TStringList;
begin
  s := TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$30#$01#$02);
  l := TStringList.Create;
  l.Add(WriteOID(s));
  l.Add(WriteOctetString(value));
  s := WriteSequence(l);
  l.Clear;
  l.Add(s);
  result := WriteSequence(l);
  l.Free;
end;

function WriteName(n: TElRelativeDistinguishedName): BufferType;
var
  t: TElASN1ConstrainedTag;
  s: integer;
begin
  t := TElASN1ConstrainedTag.Create;
  n.SaveToTag(t);
  s := 0;
  t.SaveToBuffer(nil, s);
  SetLength(result, s);
  t.SaveToBuffer(@result[1], s);
  SetLength(result, s);
end;

function WriteResponse(rs: integer; b: BufferType): ByteArray;
var
  i: TStringList;
  r, z: BufferType;
begin
  i := TStringList.Create;
  SetLength(r, 3);
  r[1] := char(asn1Enumerated);
  r[2] := #1;
  r[3] := char(rs);
  if Length(b) > 0 then
  begin
    i.Add(WriteOID(TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$30#$01#$01)));
    i.Add(WriteOctetString(b));
    z := WriteExplicit(WriteSequence(i));
    i.Clear;
  end;
  i.Add(r);
  if Length(b) > 0 then
    i.Add(z);
  result := BytesOfString(WriteSequence(i));
  i.Free;
end;

function SHA1Digest(d: BufferType): BufferType;
var
  dg: TMessageDigest160;
//  ctx: TSHA1Context;
begin
//  InitializeSHA1(ctx);
  dg := HashSHA1(d);
  result := DigestToBinary(dg);
//  FinalizeSHA1(ctx);
end;

function WriteHashAlgorithm: BufferType;
var
  id: BufferType;
  l: TStringList;
begin
  id := TBufferTypeConst(#$2A#$86#$48#$86#$F7#$0D#$01#$01#$05);
  l := TStringList.Create;
  l.Add(WriteOID(id));
  l.Add(WriteNULL);
  result := WriteSequence(l);
  l.Free;
end;

function TElOCSPServer.ProcessRequest(const Request: ByteArray; var Reply : ByteArray): integer;
var
  r: BufferType;
  i: TStringList;
begin
  if not Assigned(FOnCertificateCheck) then
    result := SB_OCSP_ERROR_NO_EVENT_HANDLER
  else
  if Length(Request) = 0 then
    result := SB_OCSP_ERROR_WRONG_DATA
  else
  begin
    FRequestBuf := Request;
    FASN1Parser := TElASN1Parser.Create;
    FASN1Parser.OnRead := HandleASN1Read;
    FASN1Parser.OnTag := HandleASN1Tag;
    FNesting := 0;
    FParseState := 0;
    FParseState2 := 0;
    FCertID := 0;
    FCertReplies := TStringList.Create;
    try
      FASN1Parser.Parse;
      //writeln(Length(FNonce));
      i := TStringList.Create;
      if ResponderIdType = ritName then
        i.Add(WritePrimitive($A1, WriteName(FResponderName)))
      else
        i.Add(WritePrimitive($A2, WriteOctetString(StringOfBytes(FResponderKeyHash))));
      i.Add(WriteGeneralizedTime(Now));
      i.Add(WriteSequence(FCertReplies));
      if Length(FNonce) > 0 then
        i.Add(WritePrimitive($A1, WriteNonce(FNonce)));
      r := WriteSequence(i);
      i.Clear;
      i.Add(r);
      i.Add(WriteHashAlgorithm);
      i.Add(SHA1Digest(r));
      r := WriteSequence(i);
      i.Free;
      Reply := WriteResponse(integer(oseSuccessful), r);
      result := 0;
    except
      SetLength(r, 0);
      Reply := WriteResponse(integer(oseMalformedRequest), r);
      result := -1;
    end;
    FCertReplies.Free;
  end;
end;

end.
 
