(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSSHTerm;

interface

uses
  SBUtils,
  Classes,
  SysUtils
;


const
  OPCODE_FIRST                  = SmallInt(1);
  OPCODE_VINTR                  = SmallInt(1);
  OPCODE_VQUIT                  = SmallInt(2);
  OPCODE_VERASE                 = SmallInt(3);
  OPCODE_VKILL                  = SmallInt(4);
  OPCODE_VEOF                   = SmallInt(5);
  OPCODE_VEOL                   = SmallInt(6);
  OPCODE_VEOL2                  = SmallInt(7);
  OPCODE_VSTART                 = SmallInt(8);
  OPCODE_VSTOP                  = SmallInt(9);
  OPCODE_VSUSP                  = SmallInt(10);
  OPCODE_VDSUSP                 = SmallInt(11);
  OPCODE_VREPRINT               = SmallInt(12);
  OPCODE_VWERASE                = SmallInt(13);
  OPCODE_VLNEXT                 = SmallInt(14);
  OPCODE_VFLUSH                 = SmallInt(15);
  OPCODE_VSWTCH                 = SmallInt(16);
  OPCODE_VSTATUS                = SmallInt(17);
  OPCODE_VDISCARD               = SmallInt(18);
  OPCODE_IGNPAR                 = SmallInt(30);
  OPCODE_PARMRK                 = SmallInt(31);
  OPCODE_INPCK                  = SmallInt(32);
  OPCODE_ISTRIP                 = SmallInt(33);
  OPCODE_INLCR                  = SmallInt(34);
  OPCODE_IGNCR                  = SmallInt(35);
  OPCODE_CRNL                   = SmallInt(36);
  OPCODE_IUCLC                  = SmallInt(37);
  OPCODE_IXON                   = SmallInt(38);
  OPCODE_IXANY                  = SmallInt(39);
  OPCODE_IXOFF                  = SmallInt(40);
  OPCODE_IMAXBEL                = SmallInt(41);
  OPCODE_ISIG                   = SmallInt(50);
  OPCODE_ICANON                 = SmallInt(51);
  OPCODE_XCASE                  = SmallInt(52);
  OPCODE_ECHO                   = SmallInt(53);
  OPCODE_ECHOE                  = SmallInt(54);
  OPCODE_ECHOK                  = SmallInt(55);
  OPCODE_ECHONL                 = SmallInt(56);
  OPCODE_NOFLSH                 = SmallInt(57);
  OPCODE_TOSTOP                 = SmallInt(58);
  OPCODE_IEXTEN                 = SmallInt(59);
  OPCODE_ECHOCTL                = SmallInt(60);
  OPCODE_ECHOKE                 = SmallInt(61);
  OPCODE_PENDIN                 = SmallInt(62);
  OPCODE_OPOST                  = SmallInt(70);
  OPCODE_OLCUC                  = SmallInt(71);
  OPCODE_ONLCR                  = SmallInt(72);
  OPCODE_OCRNL                  = SmallInt(73);
  OPCODE_ONOCR                  = SmallInt(74);
  OPCODE_ONLRET                 = SmallInt(75);
  OPCODE_CS7                    = SmallInt(90);
  OPCODE_CS8                    = SmallInt(91);
  OPCODE_PARENB                 = SmallInt(92);
  OPCODE_PARODD                 = SmallInt(93);
  OPCODE_LAST                   = SmallInt(93);

  OPCODE_NOT_SPECIFIED          = SmallInt(-32768);

type
  TTerminalOpCode = OPCODE_FIRST..OPCODE_LAST;
  TSBTerminalChangeNotificationEvent =  TNotifyEvent; //{$ifndef DELPHI_NET}TNotifyEvent{$else}procedure(Sender: System.Object) of object{$endif};

type
  TElTerminalInfo = class(TSBControlBase)
  private
    FCols : integer;
    FWidth : integer;
    FRows : integer;
    FHeight : integer;
    FOpcodes : array[OPCODE_FIRST..OPCODE_LAST] of SmallInt;
    FParent : TObject;
    FParentIsClient : boolean;
    FUpdating: boolean;
    
    FChangeNotifications : TList;
    
    FTerminalType : string;
  protected
  
    function GetOpcodes(Index : TTerminalOpCode): SmallInt;
    procedure SetOpcodes(Index : TTerminalOpCode; Value: SmallInt);
    procedure SetCols(Cols : integer);
    procedure SetWidth(Width : integer);
    procedure SetRows(Rows : integer);
    procedure SetHeight(Height : integer);
    procedure NotifyChange;
    procedure ClearNotifications;
    procedure RaiseInvalidOpcodeException;
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
    procedure SetDefaultOpcodes;
    property Opcodes[Index : TTerminalOpCode]: SmallInt read GetOpcodes write
        SetOpcodes;
    procedure AddChangeNotification(const Value : TSBTerminalChangeNotificationEvent);
    procedure RemoveChangeNotification(Value : TSBTerminalChangeNotificationEvent);
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure ClearOpcodes;
    
  published
  
    property Cols : integer read FCols write SetCols default 80;
    property Width : integer read FWidth write SetWidth default 0;
    property Rows : integer read FRows write SetRows default 24;
    property Height : integer read FHeight write SetHeight default 0;
    property TerminalType : string read FTerminalType write FTerminalType;
  end;
  
  EElTerminalException =  class(Exception);

const
  LEGAL_OPCODES = [
    OPCODE_VINTR,
    OPCODE_VQUIT,
    OPCODE_VERASE,
    OPCODE_VKILL,
    OPCODE_VEOF,
    OPCODE_VEOL,
    OPCODE_VEOL2,
    OPCODE_VSTART,
    OPCODE_VSTOP,
    OPCODE_VSUSP,
    OPCODE_VDSUSP,
    OPCODE_VREPRINT,
    OPCODE_VWERASE,
    OPCODE_VLNEXT,
    OPCODE_VFLUSH,
    OPCODE_VSWTCH,
    OPCODE_VSTATUS,
    OPCODE_VDISCARD,
    OPCODE_IGNPAR,
    OPCODE_PARMRK,
    OPCODE_INPCK,
    OPCODE_ISTRIP,
    OPCODE_INLCR,
    OPCODE_IGNCR,
    OPCODE_CRNL,
    OPCODE_IUCLC,
    OPCODE_IXON,
    OPCODE_IXANY,
    OPCODE_IXOFF,
    OPCODE_IMAXBEL,
    OPCODE_ISIG,
    OPCODE_ICANON,
    OPCODE_XCASE,
    OPCODE_ECHO,
    OPCODE_ECHOE,
    OPCODE_ECHOK,
    OPCODE_ECHONL,
    OPCODE_NOFLSH,
    OPCODE_TOSTOP,
    OPCODE_IEXTEN,
    OPCODE_ECHOCTL,
    OPCODE_ECHOKE,
    OPCODE_PENDIN,
    OPCODE_OPOST,
    OPCODE_OLCUC,
    OPCODE_ONLCR,
    OPCODE_OCRNL,
    OPCODE_ONOCR,
    OPCODE_ONLRET,
    OPCODE_CS7,
    OPCODE_CS8,
    OPCODE_PARENB,
    OPCODE_PARODD
  ];

procedure Register;

implementation

resourcestring
  SInvalidOpcode = 'Invalid OPCODE index';

procedure Register;
begin
  RegisterComponents('SSHBlackbox', [TElTerminalInfo]);
end;

type
  TSBChangeNotification = record
    E : TSBTerminalChangeNotificationEvent;
  end;
  PSBChangeNotification = ^TSBChangeNotification;

constructor TElTerminalInfo.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  FCols := 80;
  FRows := 24;
  FWidth := 0;
  FHeight := 0;
  SetDefaultOpcodes;
  FParent := nil;
  FParentIsClient := false;
  FTerminalType := 'vt100';

  FChangeNotifications := TList.Create;
end;


destructor TElTerminalInfo.Destroy;
begin
  ClearNotifications;
  FChangeNotifications.Free;
  inherited;
end;

procedure TElTerminalInfo.RaiseInvalidOpcodeException;
begin
  (**************************************************)
  (*                                                *)
  (*            Got an exception here?              *)
  (*                 PLEASE READ!                   *)
  (*                                                *)
  (*  This exception is absolutely normal and will  *)
  (*  only happen from within the IDE, not while    *)
  (*  your program is running as an EXE.            *)
  (*                                                *)
  (*  If you do not want to see this, add this      *)
  (*  exception (EElTerminalException) to the IDE   *)
  (*  options as exceptions not to break on.        *)
  (*                                                *)
  (*  Now just hit F9 and this exception will be    *)
  (*  caught and handled by SecureBlackbox.         *)
  (*                                                *)
  (**************************************************)
  raise EElTerminalException.Create(SInvalidOpcode);
end;

function TElTerminalInfo.GetOpcodes(Index : TTerminalOpCode): SmallInt;
begin
  if not (Index in LEGAL_OPCODES) then
    RaiseInvalidOpcodeException;
  Result := FOpcodes[Index];
end;

procedure TElTerminalInfo.SetOpcodes(Index : TTerminalOpCode; Value: SmallInt);
begin
  if not (Index in LEGAL_OPCODES) then
    RaiseInvalidOpcodeException;
  FOpcodes[Index] := Value;
end;

procedure TElTerminalInfo.SetCols(Cols : integer);
begin
  FCols := Cols;
  if not FUpdating then
    NotifyChange;
end;

procedure TElTerminalInfo.SetWidth(Width : integer);
begin
  FWidth := Width;
  if not FUpdating then
    NotifyChange;
end;

procedure TElTerminalInfo.SetRows(Rows : integer);
begin
  FRows := Rows;
  if not FUpdating then
    NotifyChange;
end;

procedure TElTerminalInfo.SetHeight(Height : integer);
begin
  FHeight := Height;
  if not FUpdating then
    NotifyChange;
end;

procedure TElTerminalInfo.SetDefaultOpcodes;
begin
  FOpCodes[OPCODE_VINTR] := 3;
  FOpCodes[OPCODE_VQUIT] := 28;
  FOpCodes[OPCODE_VERASE] := 8;
  FOpCodes[OPCODE_VKILL] := 21;
  FOpCodes[OPCODE_VEOF] := 4;
  FOpCodes[OPCODE_VEOL] := 0;
  FOpCodes[OPCODE_VEOL2] := 0;
  FOpCodes[OPCODE_VSTART] := 17;
  FOpCodes[OPCODE_VSTOP] := 19;
  FOpCodes[OPCODE_VSUSP] := 26;
  FOpCodes[OPCODE_VDSUSP] := OPCODE_NOT_SPECIFIED;
  FOpCodes[OPCODE_VREPRINT] := 18;
  FOpCodes[OPCODE_VWERASE] := 23;
  FOpCodes[OPCODE_VLNEXT] := 22;
  FOpCodes[OPCODE_VFLUSH] := OPCODE_NOT_SPECIFIED;
  FOpCodes[OPCODE_VSWTCH] := OPCODE_NOT_SPECIFIED;
  FOpCodes[OPCODE_VSTATUS] := OPCODE_NOT_SPECIFIED;
  FOpCodes[OPCODE_VDISCARD] := 15;
  FOpCodes[OPCODE_IGNPAR] := 0;
  FOpCodes[OPCODE_PARMRK] := 0;
  FOpCodes[OPCODE_INPCK] := 0;
  FOpCodes[OPCODE_ISTRIP] := 0;
  FOpCodes[OPCODE_INLCR] := 0;
  FOpCodes[OPCODE_IGNCR] := 0;
  FOpCodes[OPCODE_CRNL] := 1;
  FOpCodes[OPCODE_IUCLC] := 0;
  FOpCodes[OPCODE_IXON] := 1;
  FOpCodes[OPCODE_IXANY] := 0;
  FOpCodes[OPCODE_IXOFF] := 0;
  FOpCodes[OPCODE_IMAXBEL] := 0;
  FOpCodes[OPCODE_ISIG] := 1;
  FOpCodes[OPCODE_ICANON] := 1;
  FOpCodes[OPCODE_XCASE] := OPCODE_NOT_SPECIFIED;
  FOpCodes[OPCODE_ECHO] := 1;
  FOpCodes[OPCODE_ECHOE] := 0;
  FOpCodes[OPCODE_ECHOK] := 0;
  FOpCodes[OPCODE_ECHONL] := 0;
  FOpCodes[OPCODE_NOFLSH] := 0;
  FOpCodes[OPCODE_TOSTOP] := 0;
  FOpCodes[OPCODE_IEXTEN] := 1;
  FOpCodes[OPCODE_ECHOCTL] := 0;
  FOpCodes[OPCODE_ECHOKE] := 0;
  FOpCodes[OPCODE_PENDIN] := OPCODE_NOT_SPECIFIED;
  FOpCodes[OPCODE_OPOST] := 1;
  FOpCodes[OPCODE_OLCUC] := 0;
  FOpCodes[OPCODE_ONLCR] := 1;
  FOpCodes[OPCODE_OCRNL] := 0;
  FOpCodes[OPCODE_ONOCR] := 0;
  FOpCodes[OPCODE_ONLRET] := 0;
  FOpCodes[OPCODE_CS7] := 1;
  FOpCodes[OPCODE_CS8] := 1;
  FOpCodes[OPCODE_PARENB] := 0;
  FOpCodes[OPCODE_PARODD] := 0;
end;

procedure TElTerminalInfo.AddChangeNotification(const Value : TSBTerminalChangeNotificationEvent);
var
  P : PSBChangeNotification;
begin
  if Assigned(Value) then
  begin
    New(P);
    P.E := Value;
    FChangeNotifications.Add(P);
  end;
end;

procedure TElTerminalInfo.RemoveChangeNotification(Value : TSBTerminalChangeNotificationEvent);
var
  I : integer;
  
  function CompareMethods(M1, M2 : TMethod) : boolean;
  begin
    Result := (M1.Code = M2.Code) and (M1.Data = M2.Data);
  end;
begin
  if Assigned(Value) then
  begin
    for I := 0 to FChangeNotifications.Count - 1 do
    begin
      if CompareMethods(TMethod(PSBChangeNotification(FChangeNotifications[I]).E),
        TMethod(Value)) then
      begin
        Dispose(PSBChangeNotification(FChangeNotifications.Items[I]));
        FChangeNotifications.Delete(I);
        Break;
      end;
    end;
  end;
end;

procedure TElTerminalInfo.ClearNotifications;
var
  I : integer;
begin
  for I := 0 to FChangeNotifications.Count - 1 do
    Dispose(PSBChangeNotification(FChangeNotifications.Items[I]));
  FChangeNotifications.Clear;
end;

procedure TElTerminalInfo.NotifyChange;
var
  I : integer;
  P : PSBChangeNotification;
begin
  for I := 0 to FChangeNotifications.Count - 1 do
  begin
    P := PSBChangeNotification(FChangeNotifications[I]);
    P.E(Self);
  end;
end;

procedure TElTerminalInfo.BeginUpdate;
begin
  FUpdating := true;
end;

procedure TElTerminalInfo.EndUpdate;
begin
  FUpdating := false;
  NotifyChange;
end;

procedure TElTerminalInfo.ClearOpcodes;
var
  I : integer;
begin
  for I := OPCODE_FIRST to OPCODE_LAST do
    if I in LEGAL_OPCODES then
      Opcodes[I] := 0;
end;


end.
