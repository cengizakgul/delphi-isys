(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

unit SBMSKeyBlob;

{$I SecBbox.inc}

interface

uses SBUtils;


const
  SB_MSKEYBLOB_ERROR_UNSUPPORTED_BLOB_TYPE      = $2101;
  SB_MSKEYBLOB_ERROR_INVALID_FORMAT             = $2102;
  SB_MSKEYBLOB_ERROR_UNSUPPORTED_VERSION        = $2103;
  SB_MSKEYBLOB_ERROR_BUFFER_TOO_SMALL           = $2104;
  SB_MSKEYBLOB_ERROR_NO_PRIVATE_KEY             = $2105;
  SB_MSKEYBLOB_ERROR_UNSUPPORTED_ALGORITHM      = $2106;

  SB_KEY_BLOB_RSA                               = 1;
  SB_KEY_BLOB_DSS                               = 2;

function ParseMSKeyBlob(Buffer : pointer; Size : integer; OutBuffer : pointer;
  var OutSize : integer; out BlobType : integer) : integer;

function WriteMSKeyBlob(Buffer : pointer; Size : integer; OutBuffer : pointer;
  var OutSize : integer; BlobType : byte) : boolean;

implementation

uses
  SBRSA,
  SBDSA,
  SBASN1Tree,
  SysUtils,
  SBMath;

type
  PByte = ^byte;
  PWord = ^word;
  PLongword = ^cardinal;

const
  MS_SIMPLEKEYBLOB         = 1;
  MS_PUBLICKEYBLOB         = 6;
  MS_PRIVATEKEYBLOB        = 7;
  MS_PLAINTEXTKEYBLOB      = 8;
  MS_VERSION               = 2;

function ParsePublicKeyStruc(Buffer : PByte; out BlobType : integer;
  out KeyAlg : cardinal) : integer;
begin
  if not (Buffer^ in [MS_SIMPLEKEYBLOB, MS_PUBLICKEYBLOB, MS_PRIVATEKEYBLOB,
    MS_PLAINTEXTKEYBLOB]) then
  begin
    Result := SB_MSKEYBLOB_ERROR_UNSUPPORTED_BLOB_TYPE;
    Exit;
  end;
  BlobType := Buffer^;
  Inc(Buffer);
  if (Buffer^ <> MS_VERSION) then
  begin
    Result := SB_MSKEYBLOB_ERROR_UNSUPPORTED_VERSION;
    Exit;
  end;
  Inc(Buffer);
  if PWord(Buffer)^ <> 0 then
  begin
    Result := SB_MSKEYBLOB_ERROR_INVALID_FORMAT;
    Exit;
  end;
  Inc(Buffer, 2);
  KeyAlg := PLongword(Buffer)^;
  Result := 0;
end;

function ParseRSAPUBKEY(Buffer : PByte; BlobType : integer; out BitLen : cardinal;
  out PubExp : cardinal) : integer;
var
  Magic : cardinal;
begin
  Result := 0;
  Magic := PLongword(Buffer)^;
  if not (((Magic = $31415352) and (BlobType = MS_PUBLICKEYBLOB)) or
    ((Magic = $32415352) and (BlobType = MS_PRIVATEKEYBLOB))) then
  begin
    Result := SB_MSKEYBLOB_ERROR_INVALID_FORMAT;
    Exit;
  end;
  Inc(Buffer, 4);
  BitLen := PLongword(Buffer)^;
  Inc(Buffer, 4);
  PubExp := PLongword(Buffer)^;
end;

function ParseDSSPUBKEY(Buffer : PByte; out BitLen : cardinal) : integer;
var
  Magic : cardinal;
begin
  Result := 0;
  Magic := PLongWord(Buffer)^;
  if (Magic <> $31535344) and (Magic <> $32535344) then
  begin
    Result := SB_MSKEYBLOB_ERROR_INVALID_FORMAT;
    Exit;
  end;
  Inc(Buffer, 4);
  BitLen := PLongWord(Buffer)^;
end;

function EncodeRSAPublicKey(Modulus : pointer; ModulusSize : integer; Exponent :
  pointer; ExponentSize : integer; OutBuffer : pointer; var OutSize : integer) :
  boolean;
var
  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  S : string;
begin
  Tag := TElASN1ConstrainedTag.Create;
  Tag.TagId := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  SetLength(S, ModulusSize);
  try
    Move(Modulus^, S[1], Length(S));
    if PByte(Modulus)^ >= $80 then
      S := #0 + S;
    STag.Content := S;
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    SetLength(S, ExponentSize);
    Move(Exponent^, S[1], Length(S));
    if PByte(Exponent)^ >= $80 then
      S := #0 + S;
    STag.Content := S;
    Result := Tag.SaveToBuffer(OutBuffer, OutSize);
  finally
    Tag.Free;
  end;
end;

function DSS2PrivateKey(P : pointer; BytesInKey : integer; OutBuffer : pointer;
  var OutSize : integer) : integer;
var
  DP, DQ, DG, DX, DY : array of byte;
  I : integer;
  Ptr : ^byte;
  LA, LB, LC, LD : PLInt;
begin
  Ptr := P;
  SetLength(DP, BytesInKey);
  I := BytesInKey - 1;
  while I >= 0 do
  begin
    DP[I] := Ptr^;
    Inc(Ptr);
    Dec(I);
  end;
  SetLength(DQ, 20);
  I := 19;
  while I >= 0 do
  begin
    DQ[I] := Ptr^;
    Inc(Ptr);
    Dec(I);
  end;
  SetLength(DG, BytesInKey);
  I := BytesInKey - 1;
  while I >= 0 do
  begin
    DG[I] := Ptr^;
    Inc(Ptr);
    Dec(I);
  end;
  SetLength(DX, 20);
  I := 19;
  while I >= 0 do
  begin
    DX[I] := Ptr^;
    Inc(Ptr);
    Dec(I);
  end;
  { Checking whether we have enough space in output buffer}
  SetLength(DY, Length(DG) + 4);
  if not SBDSA.EncodePrivateKey(@DP[0], Length(DP), @DQ[0], Length(DQ), @DG[0],
    Length(DG), @DY[0], Length(DY), @DX[0], Length(DX), OutBuffer, OutSize) then
  begin
    Result := SB_MSKEYBLOB_ERROR_BUFFER_TOO_SMALL;
    Exit;
  end;
  { calculating Y }
  LCreate(LA);
  LCreate(LB);
  LCreate(LC);
  LCreate(LD);
  PointerToLInt(LA, @DX[0], Length(DX));
  PointerToLInt(LB, @DG[0], Length(DG));
  PointerToLInt(LC, @DP[0], Length(DP));
  LMModPower(LB, LA, LC, LD);
  SetLength(DY, LD^.Length shl 2);
  I := Length(DY);
  LIntToPointer(LD, @DY[0], I);
  LDestroy(LA);
  LDestroy(LB);
  LDestroy(LC);
  LDestroy(LD);
  if SBDSA.EncodePrivateKey(@DP[0], Length(DP), @DQ[0], Length(DQ), @DG[0],
    Length(DG), @DY[0], Length(DY), @DX[0], Length(DX), OutBuffer, OutSize) then
    Result := 0
  else
    Result := SB_MSKEYBLOB_ERROR_BUFFER_TOO_SMALL;
end;

function ParseMSKeyBlob(Buffer : pointer; Size : integer; OutBuffer : pointer;
  var OutSize : integer; out BlobType : integer) : integer;
var
  P : PByte;
  KeyAlg, BitLen, PubExp : cardinal;
  PubExpBuf : array[0..3] of byte;
  APubMod, APrivExp, APr1, APr2, AExp1, AExp2, ACoef : array of byte;
  I : integer;
const
  ALG_ID_RSA = $00A400;
  ALG_ID_RSA_SIG = $002400;
  ALG_ID_DSA = $002200;
begin
  P := Buffer;
  if Size < 8 then
  begin
    Result := SB_MSKEYBLOB_ERROR_INVALID_FORMAT;
    Exit;
  end;
  Result := ParsePublicKeyStruc(P, BlobType, KeyAlg);
  if Result <> 0 then
    Exit;
  Dec(Size, 8);
  Inc(P, 8);
  case BlobType of
    MS_PUBLICKEYBLOB, MS_PRIVATEKEYBLOB :
    begin
      if Size < 12 then
      begin
        Result := SB_MSKEYBLOB_ERROR_INVALID_FORMAT;
        Exit;
      end;
      if (KeyAlg = ALG_ID_RSA) or (KeyAlg = ALG_ID_RSA_SIG) then
      begin
        Result := ParseRSAPUBKEY(P, BlobType, BitLen, PubExp);
        if Result <> 0 then
          Exit;
        PubExpBuf[0] := PubExp shr 24;
        PubExpBuf[1] := (PubExp shr 16) and $FF;
        PubExpBuf[2] := (PubExp shr 8) and $FF;
        PubExpBuf[3] := PubExp and $FF;
        Dec(Size, 12);
        Inc(P, 12);
        if Size < integer(BitLen div 8) then
        begin
          Result := SB_MSKEYBLOB_ERROR_INVALID_FORMAT;
          Exit;
        end;
        if BlobType = MS_PUBLICKEYBLOB then
        begin
          SetLength(APubMod, BitLen shr 3);
          for I := 0 to BitLen shr 3 - 1 do
            APubMod[I] := PByteArray(P)[integer(BitLen shr 3) - I - 1];
          if EncodeRSAPublicKey(@APubMod[0], BitLen shr 3, @PubExpBuf[0], 4, OutBuffer, OutSize) then
            Result := 0
          else
            Result := SB_MSKEYBLOB_ERROR_BUFFER_TOO_SMALL;
        end
        else
        begin
          if Size < integer(BitLen shr 1 + BitLen shr 4) then
          begin
            Result := SB_MSKEYBLOB_ERROR_INVALID_FORMAT;
            Exit;
          end;
          SetLength(APubMod, BitLen shr 3);
          SetLength(APrivExp, BitLen shr 3);
          SetLength(APr1, BitLen shr 4);
          SetLength(APr2, BitLen shr 4);
          SetLength(AExp1, BitLen shr 4);
          SetLength(AExp2, BitLen shr 4);
          SetLength(ACoef, BitLen shr 4);
          for I := 0 to BitLen shr 3 - 1 do
          begin
            APubMod[I] := PByteArray(P)[integer(BitLen shr 3) - I - 1];
            APrivExp[I] := PByteArray(P)[integer((BitLen shr 3) shl 1 + 5 * (BitLen shr 4)) - I - 1];
          end;
          for I := 0 to BitLen shr 4 - 1 do
          begin
            APr1[I] := PByteArray(P)[integer(BitLen shr 3 + BitLen shr 4) - I - 1];
            APr2[I] := PByteArray(P)[integer(BitLen shr 3 + (BitLen shr 4) shl 1) - I - 1];
            AExp1[I] := PByteArray(P)[integer(BitLen shr 3 + 3 * BitLen shr 4) - I - 1];
            AExp2[I] := PByteArray(P)[integer(BitLen shr 3 + (BitLen shr 4) shl 2) - I - 1];
            ACoef[I] := PByteArray(P)[integer(BitLen shr 3 + 5 * BitLen shr 4) - I - 1];
          end;
          if SBRSA.EncodePrivateKey(@APubMod[0], BitLen shr 3,
            @PubExpBuf[0], 4, @APrivExp[0], BitLen shr 3, @APr1[0], BitLen shr 4,
            @APr2[0], BitLen shr 4, @AExp1[0], BitLen shr 4, @AExp2[0], BitLen shr 4,
            @ACoef[0], BitLen shr 4, OutBuffer, OutSize) then
            Result := 0
          else
            Result := SB_MSKEYBLOB_ERROR_BUFFER_TOO_SMALL;
        end;
      end
      else if KeyAlg = ALG_ID_DSA then
      begin
        ParseDSSPUBKEY(P, BitLen);
        Dec(Size, 8);
        Inc(P, 8);          // P[Size], Q[20], G[Size], X[20], [misc]
        I := BitLen shr 3;
        if Size < 2 * I + 40 then
        begin
          Result := SB_MSKEYBLOB_ERROR_INVALID_FORMAT;
          Exit;
        end;
        Result := DSS2PrivateKey(P, I, OutBuffer, OutSize);
      end;
    end;
    else
    begin
      if OutSize < Size then
        Result  := SB_MSKEYBLOB_ERROR_BUFFER_TOO_SMALL
      else
      begin
        Move(Buffer^, OutBuffer^, Size);
        Result := 0;
      end;
      OutSize := Size;
    end;
  end;
  if Result <> 0 then
    Exit;
end;

function WriteDSSPrivateKey(Buffer: pointer; Size: integer; Dest : pointer) : integer;
var
  Ptr: ^byte;
  SizeP, SizeQ, SizeG, SizeY, SizeX : integer;
  BufP, BufQ, BufG, BufY, BufX : array of byte;
  I, Bits : integer;
begin
  Result := -1;
  SizeP := 0; SizeQ := 0; SizeG := 0; SizeX := 0; SizeY := 0;
  SBDSA.DecodePrivateKey(Buffer, Size, nil, SizeP, nil, SizeQ, nil, SizeG,
    nil, SizeY, nil, SizeX);
  SetLength(BufP, SizeP);
  SetLength(BufQ, SizeQ);
  SetLength(BufG, SizeG);
  SetLength(BufY, SizeY);
  SetLength(BufX, SizeX);
  if not SBDSA.DecodePrivateKey(Buffer, Size, @BufP[0], SizeP, @BufQ[0], SizeQ, @BufG[0],
    SizeG, @BufY[0], SizeY, @BufX[0], SizeX) then
    Exit;
  SetLength(BufP, SizeP);
  SetLength(BufQ, SizeQ);
  SetLength(BufG, SizeG);
  SetLength(BufX, SizeX);
  while (BufP[0] = 0) do
    BufP := Copy(BufP, 1, Length(BufP));
  while (BufQ[0] = 0) and (Length(BufQ) > 20) do
    BufQ := Copy(BufQ, 1, Length(BufQ));
  while (BufG[0] = 0) and (Length(BufG) > Length(BufP)) do
    BufG := Copy(BufG, 1, Length(BufG));
  while (BufX[0] = 0) and (Length(BufX) > 20) do
    BufX := Copy(BufX, 1, Length(BufX));
  SizeP := Length(BufP);
  SizeQ := Length(BufQ);
  SizeG := Length(BufG);
  SizeX := Length(BufX);
  if (SizeG > SizeP) or (SizeQ > 20) or (SizeX > SizeQ) then
    Exit;
  Bits := SizeP shl 3;
  Ptr := Dest;
  PByteArray(Ptr)[0] := $44;
  PByteArray(Ptr)[1] := $53;
  PByteArray(Ptr)[2] := $53;
  PByteArray(Ptr)[3] := $32;
  PByteArray(Ptr)[4] := Bits and $ff;
  PByteArray(Ptr)[5] := (Bits shr 8) and $ff;
  PByteArray(Ptr)[6] := (Bits shr 16) and $ff;
  PByteArray(Ptr)[7] := (Bits shr 24) and $ff;
  Inc(Ptr, 8);
  // writing P
  for I := 0 to SizeP - 1 do
    PByteArray(Ptr)[I] := BufP[SizeP - I - 1];
  Inc(Ptr, SizeP);
  // writing Q
  for I := 0 to SizeQ - 1 do
    PByteArray(Ptr)[I] := BufQ[SizeQ - I - 1];
  Inc(Ptr, SizeQ);
  for I := 0 to 19 - SizeQ do
    PByteArray(Ptr)[I] := 0;
  Inc(Ptr, 20 - SizeQ);
  // writing G
  for I := 0 to SizeG - 1 do
    PByteArray(Ptr)[I] := BufG[SizeG - I - 1];
  Inc(Ptr, SizeG);
  for I := 0 to SizeP - SizeG - 1 do
    PByteArray(Ptr)[I] := 0;
  Inc(Ptr, SizeP - SizeG);
  // writing X
  for I := 0 to SizeX - 1 do
    PByteArray(Ptr)[I] := BufX[SizeX - I - 1];
  Inc(Ptr, SizeX);
  for I := 0 to 19 - SizeX do
    PByteArray(Ptr)[I] := 0;
  Inc(Ptr, 20 - SizeX);
  // writing seed
  PLongWord(Ptr)^ := $FFFFFFFF;
  Inc(Ptr, 4);
  for I := 0 to 19 do
    PByteArray(Ptr)[I] := SBRndGenerate(256);//Random(256);
  Inc(Ptr, 20);
  Result := PtrInt(Ptr) - PtrInt(Dest);
end;

function WriteRSAPrivateKey(Buffer: pointer; Size: integer; Dest : pointer) : integer;
var
  Ptr: ^byte;
  Tag : TElASN1ConstrainedTag;
  I, Bits : integer;
  TagS : TElASN1SimpleTag;
  S, T : string;
begin
  Result := -1;
  Tag := TElASN1ConstrainedTag.Create;
  if not Tag.LoadFromBuffer(Buffer, Size) then
  begin
    Tag.Free;
    Exit;
  end;
  if (Tag.Count <> 1) or (not Tag.GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(Tag.GetField(0)).Count < 9) then
  begin
    Tag.Free;
    Exit;
  end;
  for I := 0 to 8 do
    if TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I).IsConstrained then
    begin
      Tag.Free;
      Exit;
    end;
  TagS := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(1));
  S := TagS.Content;
  I := 1;
  while (I <= Length(S)) and (S[I] = #0) do
    Inc(I);
  S := Copy(S, I, Length(S));
  Bits := Length(S) shl 3;
  Ptr := Dest;
  PByteArray(Ptr)[0] := $52;
  PByteArray(Ptr)[1] := $53;
  PByteArray(Ptr)[2] := $41;
  PByteArray(Ptr)[3] := $32;
  PByteArray(Ptr)[4] := Bits and $ff;
  PByteArray(Ptr)[5] := (Bits shr 8) and $ff;
  PByteArray(Ptr)[6] := (Bits shr 16) and $ff;
  PByteArray(Ptr)[7] := Bits shr 24;
  TagS := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(2));
  T := TagS.Content;
  if Length(T) > 4 then
  begin
    Tag.Free;
    Exit;
  end;
  while Length(T) < 4 do
    T := #0 + T;
  PByteArray(Ptr)[8] := PByteArray(@T[1])[3];
  PByteArray(Ptr)[9] := PByteArray(@T[1])[2];
  PByteArray(Ptr)[10] := PByteArray(@T[1])[1];
  PByteArray(Ptr)[11] := PByteArray(@T[1])[0];
  Inc(Ptr, 12);
  // writing public modulus
  for I := 0 to Length(S) - 1 do
    PByteArray(Ptr)[I] := Ord(S[Length(S) - I]);
  Inc(Ptr, Length(S));
  // prime1
  TagS := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(4));
  T := TagS.Content;
  I := 1;
  while T[I] = #0 do
    Inc(I);
  T := Copy(T, I, Length(T));
  while Length(T) < Bits shr 4 do
    T := #0 + T;
  for I := 0 to Length(T) - 1 do
    PByteArray(Ptr)[I] := Ord(T[Length(T) - I]);
  Inc(Ptr, Length(T));
  // prime2
  TagS := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(5));
  T := TagS.Content;
  I := 1;
  while T[I] = #0 do
    Inc(I);
  T := Copy(T, I, Length(T));
  while Length(T) < Bits shr 4 do
    T := #0 + T;
  for I := 0 to Length(T) - 1 do
    PByteArray(Ptr)[I] := Ord(T[Length(T) - I]);
  Inc(Ptr, Length(T));
  // exponent1
  TagS := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(6));
  T := TagS.Content;
  I := 1;
  while T[I] = #0 do
    Inc(I);
  T := Copy(T, I, Length(T));
  while Length(T) < Bits shr 4 do
    T := #0 + T;
  for I := 0 to Length(T) - 1 do
    PByteArray(Ptr)[I] := Ord(T[Length(T) - I]);
  Inc(Ptr, Length(T));
  // exponent2
  TagS := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(7));
  T := TagS.Content;
  I := 1;
  while T[I] = #0 do
    Inc(I);
  T := Copy(T, I, Length(T));
  while Length(T) < Bits shr 4 do
    T := #0 + T;
  for I := 0 to Length(T) - 1 do
    PByteArray(Ptr)[I] := Ord(T[Length(T) - I]);
  Inc(Ptr, Length(T));
  // coeff
  TagS := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(8));
  T := TagS.Content;
  I := 1;
  while T[I] = #0 do
    Inc(I);
  T := Copy(T, I, Length(T));
  while Length(T) < Bits shr 4 do
    T := #0 + T;
  for I := 0 to Length(T) - 1 do
    PByteArray(Ptr)[I] := Ord(T[Length(T) - I]);
  Inc(Ptr, Length(T));
  // priv exp
  TagS := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(3));
  T := TagS.Content;
  I := 1;
  while T[I] = #0 do
    Inc(I);
  T := Copy(T, I, Length(T));
  while Length(T) < Bits shr 3 do
    T := #0 + T;
  for I := 0 to Length(T) - 1 do
    PByteArray(Ptr)[I] := Ord(T[Length(T) - I]);
  Inc(Ptr, Length(T));
  Tag.Free;
  Result := PtrInt(Ptr) - PtrInt(Dest);
end;

function EstimateBlobSize(Buffer: pointer; Size : integer; BlobType : byte) : integer;
var
  Tag : TElASN1ConstrainedTag;
  Sz : integer;
begin
  if BlobType = SB_KEY_BLOB_RSA then
  begin
    Tag := TElASN1ConstrainedTag.Create;
    if not Tag.LoadFromBuffer(Buffer, Size) then
    begin
      Tag.Free;
      raise Exception.Create('');
    end;
    if (Tag.Count <> 1) or (not Tag.GetField(0).IsConstrained) then
    begin
      Tag.Free;
      raise Exception.Create('');
    end;
    if TElASN1ConstrainedTag(Tag.GetField(0)).Count <> 9 then
    begin
      Tag.Free;
      raise Exception.Create('');
    end;
    Sz := Length(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(1)).Content);
    Result := Sz * 5 + 16;
  end
  else if BlobType = SB_KEY_BLOB_DSS then
  begin
    Tag := TElASN1ConstrainedTag.Create;
    if not Tag.LoadFromBuffer(Buffer, Size) then
    begin
      Tag.Free;
      raise Exception.Create('');
    end;
    if (Tag.Count <> 1) or (not Tag.GetField(0).IsConstrained) then
    begin
      Tag.Free;
      raise Exception.Create('');
    end;
    if TElASN1ConstrainedTag(Tag.GetField(0)).Count <> 6 then
    begin
      Tag.Free;
      raise Exception.Create('');
    end;
    Sz := Length(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(1)).Content);
    Result := Sz * 2 + 96;
  end
  else
    Result := 0;
end;

function WriteMSKeyBlob(Buffer : pointer; Size : integer; OutBuffer : pointer;
  var OutSize : integer; BlobType : byte) : boolean;
var
  Ptr : ^byte;
  Sz, EstSize : integer;
  alg : cardinal;
const
  ALG_ID_RSA = $00A40000;
  ALG_ID_DSA = $00220000;
begin
  // estimating size
  try
    EstSize := EstimateBlobSize(Buffer, Size, BlobType);
  except
    OutSize := 0;
    Result := false;
    Exit;
  end;
  if EstSize > OutSize then
  begin
    OutSize := EstSize;
    Result := false;
    Exit;
  end;
  Ptr := OutBuffer;
  PByteArray(Ptr)[0] := 7;
  PByteArray(Ptr)[1] := 2;
  PByteArray(Ptr)[2] := 0;
  PByteArray(Ptr)[3] := 0;
  // following 4 bytes is alg-id (rsa/dss)
  if BlobType = SB_KEY_BLOB_DSS then
    alg := ALG_ID_DSA
  else if BlobType = SB_KEY_BLOB_RSA then
    alg := ALG_ID_RSA
  else
    alg := 0;
  PByteArray(Ptr)[4] := alg shr 24;
  PByteArray(Ptr)[5] := (alg shr 16) and $ff;
  PByteArray(Ptr)[6] := (alg shr 8) and $ff;
  PByteArray(Ptr)[7] := alg and $ff;
  Inc(Ptr, 8);
  if alg = ALG_ID_DSA then
    Sz := WriteDSSPrivateKey(Buffer, Size, Ptr)
  else if alg = ALG_ID_RSA then
    Sz := WriteRSAPrivateKey(Buffer, Size, Ptr)
  else
    Sz := -1;
  if Sz <> -1 then
  begin
    OutSize := 8 + Sz;
    Result := true
  end
  else
    Result := false;
end;

// ------------------------- HERE GO .NET FUNCTIONS ----------------------------


end.
