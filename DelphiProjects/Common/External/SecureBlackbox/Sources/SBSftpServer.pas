(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSftpServer;

interface

uses
  SysUtils,
  Classes,
  SBUtils,
  SBSSHUtils,
  SBSSHHandlers,
  SBSftpCommon,
  SBConstants;

type
  TSBSftpHandle = class
  protected
    FAllocated : boolean;
    FName: string;
    FInfo: TElSftpFileInfo;
    FInfoUsed: boolean;
    FEof: boolean;
    FData : pointer;
    FDirectory : boolean;
  public
    constructor Create;
    destructor Destroy; override;

    property Allocated : boolean read FAllocated write FAllocated;
    property Name : string read FName write FName;
    property Info : TElSftpFileInfo read FInfo write FInfo;
    property InfoUsed : boolean read FInfoUsed write FInfoUsed;
    property Eof : boolean read FEof write FEof;
    property Data : pointer read FData write FData;
    property Directory : boolean read FDirectory write FDirectory;
  end;

  TElSFTPServer = class(TSBControlBase)
  private
    FOnError: TSBSftpErrorEvent;
  protected
    FInBuffer: ByteArray;
    FOutBuffer: ByteArray;

    FInBufferIndex : integer;
    FActive: Boolean;
    FVersionSelect : boolean;
    FFTranslationControl : boolean;
    FSupported : TElSFTPSupportedExtension; 
    FNewLineConvention: string;
    FFilenameCharset: string;
    FNegotiatedCharset: string;
    FOnClose: TNotifyEvent;
    FVersion: TSBSftpVersion;
    FVersions: TSBSftpVersions;
    FClientVendorID : TElSFTPVendorIDExtension;
    FOnBlock: TElSFTPServerBlockEvent;
    FOnUnblock: TElSFTPServerUnblockEvent;
    FOnCloseHandle: TElSFTPServerCloseHandleEvent;
    FOnCreateDirectory: TElSFTPServerCreateDirectoryEvent;
    FOnCreateSymLink: TElSFTPServerCreateSymLinkEvent;
    FOnCreateHardLink: TElSFTPServerCreateHardLinkEvent;
    FOnFindClose: TElSFTPServerFindCloseEvent;
    FOnFindFirst: TElSFTPServerFindFirstEvent;
    FOnFindNext: TElSFTPServerFindNextEvent;
    FOnOpen: TNotifyEvent;
    FOnOpenFile: TElSFTPServerOpenFileEvent;
    FOnReadFile: TElSFTPServerReadEvent;
    FOnReadSymLink: TElSFTPServerReadSymLinkEvent;
    FOnReceive: TElSFTPServerReceiveEvent;
    FOnRemove: TElSFTPServerRemoveEvent;
    FOnRenameFile: TElSFTPServerRenameFileEvent;
    FOnRequestAbsolutePath: TElSFTPServerRequestAbsolutePathEvent;
    FOnRequestAttributes: TElSFTPServerRequestAttributesEvent;
    FOnRequestAttributes2: TElSFTPServerRequestAttributes2Event;
    FOnSend: TElSFTPServerSendEvent;
    FOnSetAttributes: TElSFTPServerSetAttributesEvent;
    FOnSetAttributes2 : TElSFTPServerSetAttributes2Event;
    FOnTextSeek: TElSFTPServerTextSeekEvent;
    FOnVersionChange: TElSFTPServerVersionChangeEvent;
    FOnWriteFile: TElSFTPServerWriteEvent;
    FOnExtendedRequest : TElSFTPServerExtendedRequestEvent;
    FOnSendVendorID : TElSFTPServerSendVendorIDEvent;
    FOnRequestFileHash : TElSFTPServerRequestFileHashEvent;
    FOnRequestFileHashByHandle : TElSFTPServerRequestFileHashByHandleEvent;
    FOnRequestAvailableSpace : TElSFTPServerRequestAvailableSpaceEvent;
    FOnRequestHomeDirectory : TElSFTPServerRequestHomeDirectoryEvent;
    FOnCopyRemoteFile : TElSFTPServerCopyRemoteFileEvent;
    FOnCopyRemoteData : TElSFTPServerCopyRemoteDataEvent;
    FOnMakeTempFolder : TElSFTPServerReturnPathEvent;
    FOnRequestTempFolder : TElSFTPServerReturnPathEvent;
    // -------------------------------------------------------------------------------
    FHandles: TList;
    FFileInfos: TList;
    FClientVersion: integer;
    FServerVersion: integer;
    FLangTag: WideString;
    FErrorCode: integer;
    FComment: string;
    // -------------------------------------------------------------------------------
    procedure DoClose; virtual;
    procedure DoCloseHandle(Data: pointer;
      var ErrorCode: integer; var Comment: string); virtual;

    procedure DoBlock(Data : pointer;
      Offset, Length : Int64; LockMask : TSBSftpFileOpenAccess;
      var ErrorCode : integer; var Comment : string); virtual;
    procedure DoUnblock(Data : pointer;
      Offset, Length : Int64; var ErrorCode : integer; var Comment : string);
    procedure DoCreateDirectory(const Path: string; Attributes:
      TElSftpFileAttributes; var ErrorCode: integer; var Comment: string); virtual;
    procedure DoCreateHardLink(const NewLinkPath, ExistingPath: string; var ErrorCode:
      integer; var Comment: string); virtual;
    procedure DoCreateSymLink(const LinkPath, TargetPath: string; var ErrorCode:
      integer; var Comment: string); virtual;
    procedure DoFindClose(Data: pointer; var ErrorCode: integer;
      var Comment: string); virtual;
    procedure DoFindFirst(const Path: string; var Data: pointer;
      Info: TElSftpFileInfo; var ErrorCode: integer; var Comment: string); virtual;
    procedure DoFindNext(Data: pointer; Info: TElSftpFileInfo;
      var ErrorCode: integer; var Comment: string); virtual;
    procedure DoOpen; virtual;
    procedure DoOpenFile(const Path: string; Modes: TSBSftpFileOpenModes; Access:
      TSBSftpFileOpenAccess; DesiredAccess : cardinal;
      Attributes: TElSftpFileAttributes; var Data:
      pointer; var ErrorCode: integer; var Comment: string); virtual;
    procedure DoReadFile(Data: pointer; Offset: Int64; Buffer: pointer;
      Count: integer; var Read, ErrorCode: integer; var Comment: string); virtual;
    procedure DoReadSymLink(const Path: string; Info: TElSftpFileInfo; var
      ErrorCode: integer; var Comment: string); virtual;
    procedure DoReceive(Buffer: pointer; MaxSize: integer;
      var Written: integer); virtual;
    procedure DoRemove(const Path: string; var ErrorCode: integer;
      var Comment: string); virtual;
    procedure DoRenameFile(const OldPath, NewPath: string; Flags : TSBSftpRenameFlags;
      var ErrorCode: integer; var Comment: string); virtual;
    procedure DoRequestAbsolutePath(const Path: string; var AbsolutePath: string;
      Control : TSBSftpRealpathControl; ComposePath : TStringList;
      var ErrorCode: integer; var Comment: string); virtual;
    procedure DoRequestAttributes(const Path: string; FollowSymLinks: boolean;
      Attributes: TElSftpFileAttributes; var ErrorCode: integer;
      var Comment: string); virtual;
    procedure DoRequestAttributes2(Data: pointer;
      Attributes: TElSftpFileAttributes; var ErrorCode: integer;
      var Comment: string); virtual;
    procedure DoSend(Buffer: pointer; Size: integer); virtual;
    procedure DoSetAttributes(const Path: string; Attributes: TElSftpFileAttributes;
      var ErrorCode: integer; var Comment: string); virtual;
    procedure DoSetAttributes2(Data : pointer;
      Attributes : TElSftpFileAttributes;
      var ErrorCode: integer; var Comment: string); virtual;
    procedure DoTextSeek(Data : pointer;
      LineNumber: Int64; var ErrorCode: integer;
      var Comment: string); virtual;
    procedure DoVersionChange(Version : TSBSFTPVersion);
    procedure DoWriteFile(Data: pointer; Offset: Int64; Buffer: pointer;
      Count: integer; var ErrorCode: integer; var Comment: string); virtual;
    procedure DoExtendedRequest(const Request: string; Buffer : pointer;
      Size : integer; Response : TStream; var ErrorCode : integer;
      var Comment : string);
    // -------------------------------------------------------------------------------
    property LangTag: WideString read FLangTag write FLangTag;

    procedure AllocateHandles(Count : integer);
    function GetNextFreeHandle(): integer;
    function HandleToString(Handle : integer): BufferType;
    function StringToHandle(Str : BufferType) : integer;
    function IsValidHandle(Handle : integer) : boolean;
    function GetHandle(Index: integer): TSBSftpHandle;
    property Handles[Index: integer] : TSBSftpHandle read GetHandle;
    procedure ClearFileInfo(Info: TElSftpFileInfo);
    procedure ClearHandleInfo(Handle : TSBSftpHandle);

    function FakeAttrs: BufferType; virtual;
    function MakeAttrs(f: TElSftpFileInfo): BufferType; virtual;
    procedure LoadAttrs(Buffer: pointer; Size: integer; Attrs: TElSftpFileAttributes); virtual;
    function GetDataPacket(Buffer: pointer; var Size: integer): boolean; virtual;

    procedure DispatchDataPacket(Size: integer); virtual;
    procedure SendReply(Size: integer); virtual;
    procedure DoError(ErrorCode: integer; const Comment: string); virtual;

    procedure sftpParseFxpInit(Size: integer); virtual;
    procedure sftpParseFxpRealPath(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpStat(id: cardinal; Size: integer; followsym: boolean); virtual;
    procedure sftpParseFxpFStat(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpOpenDir(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpReadDir(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpMkDir(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpRmDir(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpClose(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpOpen(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpRead(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpWrite(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpRename(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpRemove(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpSetStat(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpFSetStat(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpReadLink(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpSymLink(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpLink(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpBlock(id: cardinal; Size: integer); virtual;
    procedure sftpParseFxpUnblock(id: cardinal; Size: integer); virtual;    
    procedure sftpParseFxpExtended(id: cardinal; Size: integer); virtual;
    procedure sftpSendFxpVersion; virtual;
    procedure sftpSendFxpName0(id: cardinal; const name: string); virtual;
    procedure sftpSendFxpName(id: cardinal; name: TElSftpFileInfo); virtual;
    procedure sftpSendFxpNames(id: cardinal;
      names: TList; Count : integer); virtual;
    procedure sftpSendFxpAttrs(id: cardinal; inf: TElSftpFileInfo); virtual;
    procedure sftpSendFxpStatus(id, code: cardinal; const msg, lng: string); virtual;
    procedure sftpSendFxpHandle(id: cardinal; const handle: BufferType); virtual;
    procedure sftpSendFxpExtendedReply(id: cardinal; Buffer: pointer;
      Size: integer); virtual;
    { handling of known extension }
    procedure sftpParseFxpTextSeek(id: cardinal; Size: integer); virtual;
    procedure sftpParseVersionSelect(id : cardinal; const ExtName,
      ExtData : BufferType; var Cont : boolean); virtual;
    procedure sftpParseFilenameTranslationControl(id : cardinal; const ExtName,
      ExtData : BufferType; var Cont : boolean); virtual;
    procedure sftpParseVendorID(id : cardinal; const ExtName,
      ExtData : BufferType; var Cont : boolean); virtual;
    procedure sftpParseCheckFile(id : cardinal; const ExtName,
      ExtData : BufferType; var Cont : boolean); virtual;
    procedure sftpParseSpaceAvailable(id : cardinal; const ExtName,
      ExtData : BufferType; var Cont : boolean); virtual;
    procedure sftpParseHomeDirectory(id : cardinal; const ExtName,
      ExtData : BufferType; var Cont : boolean); virtual;
    procedure sftpParseCopyRemoteFile(id : cardinal; const ExtName,
      ExtData : BufferType; var Cont : boolean); virtual;
    procedure sftpParseCopyRemoteData(id : cardinal; const ExtName,
      ExtData : BufferType; var Cont : boolean); virtual;

    // -------------------------------------------------------------------------------
    
    
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    
    procedure Close;
    procedure DataAvailable;
    procedure Open;
    property Active: Boolean read FActive;
    property ServerVersion: integer read FServerVersion write FServerVersion;
    property NewLineConvention: String read FNewLineConvention write FNewLineConvention;
    property NegotiatedCharset : string read FNegotiatedCharset;
    property Version: TSBSftpVersion read FVersion;
  published
    property Supported : TElSFTPSupportedExtension read FSupported;
    property ClientVendorID : TElSftpVendorIDExtension read FClientVendorID;
    property FilenameCharset : string read FFilenameCharset write FFilenameCharset;
    property Versions : TSBSftpVersions read FVersions write FVersions
      default [sbSFTP0, sbSFTP1, sbSFTP2, sbSFTP3, sbSFTP4, sbSFTP5, sbSFTP6];

    property OnBlock : TElSFTPServerBlockEvent read FOnBlock write FOnBlock;
    property OnUnblock : TElSFTPServerUnblockEvent read FOnUnblock write FOnUnblock;
    property OnClose: TNotifyEvent read FOnClose write FOnClose;
    property OnCloseHandle: TElSFTPServerCloseHandleEvent read FOnCloseHandle write
        FOnCloseHandle;
    property OnCreateDirectory: TElSFTPServerCreateDirectoryEvent read
        FOnCreateDirectory write FOnCreateDirectory;
    property OnCreateSymLink: TElSFTPServerCreateSymLinkEvent read FOnCreateSymLink
        write FOnCreateSymLink;
    property OnCreateHardLink: TElSFTPServerCreateHardLinkEvent read FOnCreateHardLink
        write FOnCreateHardLink;
    property OnError: TSBSftpErrorEvent read FOnError write FOnError;
    property OnFindClose: TElSFTPServerFindCloseEvent read FOnFindClose write
        FOnFindClose;
    property OnFindFirst: TElSFTPServerFindFirstEvent read FOnFindFirst write
        FOnFindFirst;
    property OnFindNext: TElSFTPServerFindNextEvent read FOnFindNext write
        FOnFindNext;
    property OnOpen: TNotifyEvent read FOnOpen write FOnOpen;
    property OnOpenFile: TElSFTPServerOpenFileEvent read FOnOpenFile write
        FOnOpenFile;
    property OnReadFile: TElSFTPServerReadEvent read FOnReadFile write FOnReadFile;
    property OnReadSymLink: TElSFTPServerReadSymLinkEvent read FOnReadSymLink write
        FOnReadSymLink;
    property OnReceive: TElSFTPServerReceiveEvent read FOnReceive write FOnReceive;
    property OnRemove: TElSFTPServerRemoveEvent read FOnRemove write FOnRemove;
    property OnRenameFile: TElSFTPServerRenameFileEvent read FOnRenameFile write
        FOnRenameFile;
    property OnRequestAbsolutePath: TElSFTPServerRequestAbsolutePathEvent read
        FOnRequestAbsolutePath write FOnRequestAbsolutePath;
    property OnRequestAttributes: TElSFTPServerRequestAttributesEvent read
        FOnRequestAttributes write FOnRequestAttributes;
    property OnRequestAttributes2: TElSFTPServerRequestAttributes2Event read
        FOnRequestAttributes2 write FOnRequestAttributes2;
    property OnSend: TElSFTPServerSendEvent read FOnSend write FOnSend;
    property OnSetAttributes: TElSFTPServerSetAttributesEvent read FOnSetAttributes
        write FOnSetAttributes;
    property OnSetAttributes2: TElSFTPServerSetAttributes2Event
      read FOnSetAttributes2 write FOnSetAttributes2;
    property OnTextSeek: TElSFTPServerTextSeekEvent read FOnTextSeek write
        FOnTextSeek;
    property OnWriteFile: TElSFTPServerWriteEvent read FOnWriteFile write
        FOnWriteFile;
    property OnExtendedRequest : TElSFTPServerExtendedRequestEvent
      read FOnExtendedRequest write FOnExtendedRequest;
    property OnVersionChange : TElSFTPServerVersionChangeEvent
      read FOnVersionChange write FOnVersionChange;
    property OnSendVendorID : TElSFTPServerSendVendorIDEvent
      read FOnSendVendorID write FOnSendVendorID;
    property OnRequestFileHash : TElSFTPServerRequestFileHashEvent read
      FOnRequestFileHash write FOnRequestFileHash;
    property OnRequestFileHashByHandle : TElSFTPServerRequestFileHashByHandleEvent read
      FOnRequestFileHashByHandle write FOnRequestFileHashByHandle;
    property OnRequestAvailableSpace : TElSFTPServerRequestAvailableSpaceEvent read
      FOnRequestAvailableSpace write FOnRequestAvailableSpace;
    property OnRequestHomeDirectory : TElSFTPServerRequestHomeDirectoryEvent read
      FOnRequestHomeDirectory write FOnRequestHomeDirectory;
    property OnCopyRemoteFile : TElSFTPServerCopyRemoteFileEvent read
      FOnCopyRemoteFile write FOnCopyRemoteFile;
    property OnCopyRemoteData : TElSFTPServerCopyRemoteDataEvent read
      FOnCopyRemoteData write FOnCopyRemoteData;
    property OnMakeTempFolder : TElSFTPServerReturnPathEvent read
      FOnMakeTempFolder write FOnMakeTempFolder;
    property OnRequestTempFolder : TElSFTPServerReturnPathEvent read
      FOnRequestTempFolder write FOnRequestTempFolder;  
  end;

procedure Register;

implementation

// -------------------------------------------------------------------------------

const
  SFTP_MIN_VERSION = 2;
  SFTP_MAX_VERSION = 6;
  BUFFER_SIZE = 132072; //36000;
  DEFAULT_HANDLE_COUNT = 16;
  MAX_HANDLE_COUNT = 128;
  DEFAULT_FILELIST_COUNT = 64;

resourcestring
  SDisconnectInvalidPacketSize          = 'Invalid packet size';
  SNoMoreHandlesAvailable               = 'No more file handles available';
  SInvalidHandle                        = 'Invalid handle';
  SInvalidPacket                        = 'Invalid packet';
  SUnsupportedPacketType                = 'Unsupported packet type';
  SNoFiles                              = 'No files';
  SSuccess                              = 'Success';
  SUnsupportedExtension                 = 'Unsupported extension';
  SUnsupportedValue                     = 'Unsupported value';
  SInternalError                        = 'Internal error';
  SUnknownFileType                      = 'Unknown file type: [%d]';
  SUnsupportedVersion                   = 'Unsupported version';

function EncodeFileType(FT : TSBSftpFileType) : byte;
begin
  case FT of
    ftFile: Result := SSH_FILEXFER_TYPE_REGULAR; 
    ftDirectory: Result := SSH_FILEXFER_TYPE_DIRECTORY;
    ftSymblink: Result := SSH_FILEXFER_TYPE_SYMLINK;
    ftSpecial: Result := SSH_FILEXFER_TYPE_SPECIAL;
    ftUnknown: Result := SSH_FILEXFER_TYPE_UNKNOWN;
    ftSocket: Result := SSH_FILEXFER_TYPE_SOCKET;
    ftCharDevice: Result := SSH_FILEXFER_TYPE_CHAR_DEVICE;
    ftBlockDevice: Result := SSH_FILEXFER_TYPE_BLOCK_DEVICE;
    ftFIFO: Result := SSH_FILEXFER_TYPE_FIFO;
  else
    raise EElSFTPError.Create(SInternalError);
  end;
end;

function DecodeFileType(Value : byte) : TSBSftpFileType;
begin
  case Value of
    SSH_FILEXFER_TYPE_ERRONEOUS, // this should not happen, but there are some mad clients, like TElSFTPClient ;))))
    SSH_FILEXFER_TYPE_REGULAR : Result := ftFile;
    SSH_FILEXFER_TYPE_DIRECTORY : Result := ftDirectory;
    SSH_FILEXFER_TYPE_SYMLINK : Result := ftSymblink;
    SSH_FILEXFER_TYPE_SPECIAL : Result := ftSpecial;
    SSH_FILEXFER_TYPE_UNKNOWN : Result := ftUnknown;
    SSH_FILEXFER_TYPE_SOCKET : Result := ftSocket;
    SSH_FILEXFER_TYPE_CHAR_DEVICE : Result := ftCharDevice;
    SSH_FILEXFER_TYPE_BLOCK_DEVICE : Result := ftBlockDevice;
    SSH_FILEXFER_TYPE_FIFO : Result := ftFIFO;
  else
    raise EElSFTPError.Create(Format(SUnknownFileType, [Value]));
  end;
end;

procedure Register;
begin
  RegisterComponents('SFTPBlackbox', [TElSFTPServer]);
end;

constructor TElSFTPServer.Create(AOwner : TComponent);
var
  I : integer;
begin
  inherited Create(AOwner);
  FHandles := TList.Create;
  SetLength(FInBuffer, BUFFER_SIZE);
  SetLength(FOutBuffer, BUFFER_SIZE);
  FNewLineConvention := #13#10;
  FFilenameCharset := '';
  FNegotiatedCharset := '';
  AllocateHandles(DEFAULT_HANDLE_COUNT);
  FFileInfos := TList.Create;
  for I := 0 to DEFAULT_FILELIST_COUNT - 1 do
    FFileInfos.Add(TElSftpFileInfo.Create);
  FVersions := [sbSFTP0, sbSFTP1, sbSFTP2, sbSFTP3, sbSFTP4, sbSFTP5, sbSFTP6];
  FInBufferIndex := 0;
  FLangTag := '';
  FVersionSelect := false;
  FSupported := TElSftpSupportedExtension.Create;
  FSupported.FillDefault;
  FClientVendorID := nil;
end;


destructor TElSFTPServer.Destroy;
var
  I  : integer;
  FI : TElSftpFileInfo;
begin
  AllocateHandles(0);
  FreeAndNil(FHandles);
  for I := 0 to FFileInfos.Count - 1 do
  begin
    FI := TElSftpFileInfo(FFileInfos[I]);
    FreeAndNil(FI);
  end;

  FreeAndNil(FFileInfos);
  FreeAndNil(FSupported);
  if Assigned(FClientVendorID) then
    FreeAndNil(FClientVendorID);
  
  inherited;
end;

procedure TElSFTPServer.Open;
begin
  FActive := true;
end;

procedure TElSFTPServer.Close;
begin
  DoClose;
  SetLength(FInBuffer, 0);
  SetLength(FOutBuffer, 0);
  FActive := false;
end;

procedure TElSFTPServer.DataAvailable;
//var
//  Size: integer;
var
  Needed : integer;
  PktSize : integer;
  Written : integer;
begin
//  Size := BUFFER_SIZE;
//  if GetDataPacket(FInBuffer, Size) then
//    DispatchDataPacket(Size);
  if FInBufferIndex < 4 then
    Needed := 4 - FInBufferIndex
  else
  begin
    PktSize := (FInBuffer[0] shl 24) or
      (FInBuffer[1] shl 16) or
      (FInBuffer[2] shl 8) or
      (FInBuffer[3]);
    Needed := PktSize - FInBufferIndex + 4; 
  end;
  if Length(FInBuffer) < FInBufferIndex + Needed then
    SetLength(FInBuffer, FInBufferIndex + Needed);
  DoReceive(@FInBuffer[FInBufferIndex], Needed, Written);
  Inc(FInBufferIndex, Written);
  if Written >= Needed then
  begin
    // processing packets
    while true do
    begin
      if FInBufferIndex < 4 then
        Break;
      PktSize := (FInBuffer[0] shl 24) or
        (FInBuffer[1] shl 16) or
        (FInBuffer[2] shl 8) or
        (FInBuffer[3]);

      if FInBufferIndex >= PktSize + 4 then
      begin
        Move(FInBuffer[4], FInBuffer[0], FInBufferIndex - 4);
        Dec(FInBufferIndex, 4);
        DispatchDataPacket(PktSize);
        Move(FInBuffer[PktSize], FInBuffer[0], FInBufferIndex - PktSize);
        Dec(FInBufferIndex, PktSize);
      end
      else
        Break;
    end;
  end;
end;

procedure TElSFTPServer.DispatchDataPacket(Size: integer);
var
  id: cardinal;
begin
  id := 0;
  try
    if FInBuffer[0] = SSH_FXP_INIT then
      sftpParseFxpInit(Size - 1)
    else
    begin
      id := ReadLength(@FInBuffer[1], Size - 1);
      dec(Size, 5);
      case FInBuffer[0] of
        SSH_FXP_MKDIR: sftpParseFxpMkDir(id, Size);
        SSH_FXP_RMDIR: sftpParseFxpRmDir(id, Size);
        SSH_FXP_REALPATH: sftpParseFxpRealPath(id, Size);
        SSH_FXP_OPENDIR: sftpParseFxpOpenDir(id, Size);
        SSH_FXP_READDIR: sftpParseFxpReadDir(id, Size);
        SSH_FXP_CLOSE: sftpParseFxpClose(id, Size);
        SSH_FXP_STAT: sftpParseFxpStat(id, Size, true);
        SSH_FXP_LSTAT: sftpParseFxpStat(id, Size, false);
        SSH_FXP_FSTAT: sftpParseFxpFStat(id, Size);
        SSH_FXP_SETSTAT: sftpParseFxpSetStat(id, Size);
        SSH_FXP_FSETSTAT: sftpParseFxpFSetStat(id, Size);
        SSH_FXP_RENAME: sftpParseFxpRename(id, Size);
        SSH_FXP_REMOVE: sftpParseFxpRemove(id, Size);
        SSH_FXP_OPEN: sftpParseFxpOpen(id, Size);
        SSH_FXP_READ: sftpParseFxpRead(id, Size);
        SSH_FXP_WRITE: sftpParseFxpWrite(id, Size);
        SSH_FXP_READLINK: sftpParseFxpReadLink(id, Size);
        SSH_FXP_SYMLINK: sftpParseFxpSymLink(id, Size);
        SSH_FXP_LINK: sftpParseFxpLink(id, Size);
        SSH_FXP_BLOCK: sftpParseFxpBlock(id, Size);
        SSH_FXP_UNBLOCK: sftpParseFxpUnblock(id, Size);
        SSH_FXP_EXTENDED: sftpParseFxpExtended(id, Size);
      else
        sftpSendFxpStatus(id, SSH_FX_OP_UNSUPPORTED, SUnsupportedPacketType, '');
      end;
    end;
  except
    sftpSendFxpStatus(id, SSH_FX_BAD_MESSAGE, SInvalidPacket, '');
  end;
end;

procedure TElSFTPServer.AllocateHandles(Count : integer);
var
  I : integer;
  SH : TSBSftpHandle;
begin
  while FHandles.Count < Count do
    FHandles.Add(TSBSftpHandle.Create);
  if FHandles.Count > Count then
  begin
    for I := Count to FHandles.Count - 1 do
    begin
      SH := TSBSftpHandle(FHandles[I]);
      FreeAndNil(SH);
    end;
    FHandles.Count := Count;
  end;
end;

function TElSFTPServer.GetNextFreeHandle(): integer;
var
  I : integer;
  Index : integer;
begin
  Index := -1;
  for I := 0 to FHandles.Count - 1 do
  begin
    if not TSBSftpHandle(FHandles[I]).Allocated then
    begin
      Index := I;
      Break;
    end;
  end;
  if Index = -1 then
  begin
    if FHandles.Count < MAX_HANDLE_COUNT then
    begin
      Index := FHandles.Count;
      AllocateHandles(Min(FHandles.Count + 16, MAX_HANDLE_COUNT));
    end;
  end;
  Result := Index;
end;

function TElSFTPServer.HandleToString(Handle : integer) : BufferType;
begin
  SetLength(Result, 4);

  PByte(@Result[1])^ := Handle shr 24;
  PByte(@Result[2])^ := (Handle shr 16) and $ff;
  PByte(@Result[3])^ := (Handle shr 8) and $ff;
  PByte(@Result[4])^ := Handle and $ff;
end;

function TElSFTPServer.StringToHandle(Str : BufferType) : integer;
begin
  if Length(Str) = 4 then
    Result := (PByte(@Str[1])^ shl 24) or (PByte(@Str[2])^ shl 16) or
      (PByte(@Str[3])^ shl 8) or PByte(@Str[4])^
  else
    Result := -1;
end;

function TElSFTPServer.IsValidHandle(Handle : integer) : boolean;
begin
  Result := (Handle >= 0) and (Handle < FHandles.Count) and
    (TSBSftpHandle(FHandles[Handle]).FAllocated);
end;

function TElSFTPServer.GetHandle(Index: integer): TSBSftpHandle;
begin
  if (Index < 0) or (Index >= FHandles.Count) then
    Result := nil
  else
    Result := TSBSftpHandle(FHandles[Index]);
end;

procedure TElSFTPServer.ClearFileInfo(Info: TElSftpFileInfo);
begin
  Info.Name := '';
  Info.LongName := '';

  Info.Attributes.IncludedAttributes := [];
  Info.Attributes.FileType := ftUnknown;
end;

procedure TElSFTPServer.ClearHandleInfo(Handle : TSBSftpHandle);
begin
  Handle.FAllocated := false;
  Handle.FName := '';
  ClearFileInfo(Handle.FInfo);
  Handle.FInfoUsed := false;
  Handle.FEof := false;
  Handle.FData := nil;
  Handle.FDirectory := false;
end;

procedure TElSFTPServer.SendReply(Size: integer);
var
  Tmp : BufferType;
begin
  Tmp := WriteUINT32(Size);
  Move(Tmp[1], FOutBuffer[0], Length(Tmp));
  DoSend(@FOutBuffer[0], Size + Length(Tmp));
end;

procedure TElSFTPServer.sftpSendFxpStatus(id, code: cardinal; const msg, lng: string);
var
  Tmp, hs : string;
  ToSend: integer;
begin
  hs := lng;
  if hs = '' then
    hs := FLangTag;
  Tmp := char(SSH_FXP_STATUS) + WriteUINT32(id) + WriteUINT32(code) + WriteString(msg) + WriteString(hs);
// <error-specific data> begins from v.5
  ToSend := Min(BUFFER_SIZE - 4, Length(Tmp));
  Move(Tmp[1], FOutBuffer[4], ToSend);
  SendReply(ToSend);
end;

procedure TElSFTPServer.sftpSendFxpHandle(id: cardinal; const handle: BufferType);
var
  Tmp : BufferType;
begin
  Tmp := char(SSH_FXP_HANDLE) + WriteUINT32(id) + WriteString(handle);
  Move(Tmp[1], FOutBuffer[4], Length(Tmp));
  SendReply(Length(Tmp));
end;

procedure TElSFTPServer.sftpSendFxpAttrs(id: cardinal; inf: TElSftpFileInfo);
var
  Tmp : BufferType;                   
  ToSend : integer;
begin
  Tmp := char(SSH_FXP_ATTRS) + WriteUINT32(id) + MakeAttrs(inf);
  ToSend := Min(Length(Tmp), BUFFER_SIZE - 4);
  Move(Tmp[1], FOutBuffer[4], ToSend);
  SendReply(ToSend);
end;

procedure TElSFTPServer.LoadAttrs(Buffer: pointer; Size: integer;
  Attrs: TElSftpFileAttributes);
begin
  Attrs.LoadFromBuffer(Buffer, Size, FServerVersion);
end;

procedure TElSFTPServer.sftpParseFxpOpen(id: cardinal; Size: integer);
var
  p: string;
  i, fl: integer;
  Access, Flags : cardinal;
  a: TElSftpFileAttributes;
  oa: TSBSftpFileOpenAccess;
  om: TSBSftpFileOpenModes;
  UserData : pointer;
  HandleIndex : integer;
  Handle : TSBSftpHandle;
begin
  p := ReadString(@FInBuffer[5], Size);
  i := Length(p) + 4;
  fl := ReadLength(@FInBuffer[5 + i], Size - i);
  inc(i, 4);
  if FServerVersion < 5 then
  begin
    Access := 0;

    oa := [];
    om := [];

    if (fl and SSH_FXF_TEXT) <> 0 then
      include(om, fmText);

    if (fl and SSH_FXF_APPEND) <> 0 then
      include(om, fmAppend);

    if (fl and SSH_FXF_CREAT) <> 0 then
      include(om, fmCreate);

    if (fl and SSH_FXF_TRUNC) <> 0 then
      include(om, fmTruncate);

    if (fl and SSH_FXF_EXCL) <> 0 then
      include(om, fmExcl);
    if (fl and SSH_FXF_READ) <> 0 then
      include(om, fmRead);
    if (fl and SSH_FXF_WRITE) <> 0 then
       include(om, fmWrite);
  end
  else
  begin
    Access := fl;

    Flags := ReadLength(@FInBuffer[5 + i], Size - i);
    Inc(i, 4);

    om := [];
    oa := [];

    case Flags and SSH_FXF_ACCESS_DISPOSITION of
      SSH_FXF_CREATE_NEW :
        om := om + [fmCreate];
      SSH_FXF_CREATE_TRUNCATE :
        om := om + [fmCreate, fmTruncate];
      SSH_FXF_OPEN_EXISTING : ; { no mapping, distinguished as absence of fmCreate and fmOpenOrCreate}
      SSH_FXF_OPEN_OR_CREATE :
        om := om + [fmOpenOrCreate];
      SSH_FXF_TRUNCATE_EXISTING :
        om := om + [fmTruncate];
    else
      begin
        sftpSendFxpStatus(id, SSH_ERROR_OP_UNSUPPORTED, SUnsupportedValue, '');
        Exit;
      end;
    end;

    if (Access and ACE4_READ_DATA) <> 0 then
      om := om + [fmRead];

    if (Access and ACE4_WRITE_DATA) <> 0 then
      om := om + [fmWrite];

    if (Access and ACE4_APPEND_DATA) <> 0 then
      om := om + [fmAppend];

    if (Flags and SSH_FXF_ACCESS_APPEND_DATA) <> 0 then
      om := om + [fmAppend];

    if (Flags and SSH_FXF_ACCESS_APPEND_DATA_ATOMIC) <> 0 then
      om := om + [fmAppendAtomic];

    if (Flags and SSH_FXF_ACCESS_TEXT_MODE) <> 0 then
      om := om + [fmText];

    if (Flags and SSH_FXF_ACCESS_NOFOLLOW) <> 0 then
      om := om + [fmNoFollow];

    if (Flags and SSH_FXF_ACCESS_DELETE_ON_CLOSE) <> 0 then
      om := om + [fmDeleteOnClose];
      

    if (Flags and SSH_FXF_ACCESS_BLOCK_READ) <> 0 then
      oa := oa + [faReadLock];

    if (Flags and SSH_FXF_ACCESS_BLOCK_WRITE) <> 0 then
      oa := oa + [faWriteLock];

    if (Flags and SSH_FXF_ACCESS_BLOCK_DELETE) <> 0 then
      oa := oa + [faDeleteLock];

    if (Flags and SSH_FXF_ACCESS_BLOCK_ADVISORY) <> 0 then
      oa := oa + [faBlockAdvisory];
  end;
  a := TElSftpFileAttributes.Create;
  try
    LoadAttrs(@FInBuffer[5 + i], Size - i, a);
    UserData := nil;
    DoOpenFile(p, om, oa, Access, a, UserData, FErrorCode, FComment);
  finally
    FreeAndNil(a);
  end;
  if FErrorCode = 0 then
  begin
    HandleIndex := GetNextFreeHandle();
    if HandleIndex <> -1 then
    begin
      Handle := Handles[HandleIndex];
      ClearHandleInfo(Handle);
      Handle.Allocated := true;
      Handle.Name := p;
      Handle.Data := UserData;
      sftpSendFxpHandle(id, HandleToString(HandleIndex));
    end
    else
      sftpSendFxpStatus(id, SSH_ERROR_FAILURE, SNoMoreHandlesAvailable, '');
  end
  else
    sftpSendFxpStatus(id, FErrorCode, FComment, '');
end;

procedure TElSFTPServer.sftpParseFxpRead(id: cardinal; Size: integer);
var
  p, Tmp: BufferType;
  i, ofs, len, l: integer;
  HandleIndex : integer;
begin
  p := ReadString(@FInBuffer[5], Size);
  i := Length(p) + 4;
  ofs := ReadUINT64(@FInBuffer[5 + i], Size - i);
  len := ReadLength(@FInBuffer[5 + i + 8], Size - i - 8);
  if len > 32768 then
    len := 32768;
  HandleIndex := StringToHandle(p);
  if IsValidHandle(HandleIndex) then
  begin
    Tmp := char(SSH_FXP_DATA) + WriteUINT32(id) + WriteUINT32(len);
    DoReadFile(Handles[HandleIndex].Data, ofs, @FOutBuffer[Length(Tmp) + 4],
      len, l, FErrorCode, FComment);
    if FErrorCode = 0 then
    begin
      Tmp := char(SSH_FXP_DATA) + WriteUINT32(id) + WriteUINT32(l);
      Move(Tmp[1], FOutBuffer[4], Length(Tmp));

      SendReply(Length(Tmp) + l);
    end
    else
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
  end
  else
    sftpSendFxpStatus(id, SSH_ERROR_INVALID_HANDLE, SInvalidHandle, '');
end;

procedure TElSFTPServer.sftpParseFxpWrite(id: cardinal; Size: integer);
var
  p: BufferType;
  i, ofs, len: integer;
  HandleIndex : integer;
begin
  p := ReadString(@FInBuffer[5], Size);
  i := Length(p) + 4;
  ofs := ReadUINT64(@FInBuffer[5 + i], Size - i);
  len := ReadLength(@FInBuffer[5 + i + 8], Size - i - 8);

  HandleIndex := StringToHandle(p);
  if IsValidHandle(HandleIndex) then
  begin
    inc(i, 12);
    DoWriteFile(Handles[HandleIndex].Data, ofs, @FInBuffer[5 + i],
      len, FErrorCode, FComment);
    if FErrorCode = 0 then
      sftpSendFxpStatus(id, SSH_FX_OK, '', '')
    else
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
  end
  else
    sftpSendFxpStatus(id, SSH_ERROR_INVALID_HANDLE, SInvalidHandle, '');
end;

procedure TElSFTPServer.sftpParseFxpSetStat(id: cardinal; Size: integer);
var
  p: BufferType;
  a: TElSftpFileAttributes;
  i: integer;
begin
  p := ReadString(@FInBuffer[5], Size);
  i := Length(p) + 4;
  a := TElSftpFileAttributes.Create;
  try
    LoadAttrs(@FInBuffer[5 + i], Size - i, a);
    DoSetAttributes((p), a, FErrorCode, FComment);
  finally
    FreeAndNil(a);
  end;
  if FErrorCode = 0 then
    sftpSendFxpStatus(id, SSH_FX_OK, '', '')
  else
    sftpSendFxpStatus(id, FErrorCode, FComment, '');
end;

procedure TElSFTPServer.sftpParseFxpFSetStat(id: cardinal; Size: integer);
var
  p: BufferType;
  a: TElSftpFileAttributes;
  i: integer;
  HandleIndex : integer;
begin
  p := ReadString(@FInBuffer[5], Size);
  i := Length(p) + 4;
  a := TElSftpFileAttributes.Create;
  try
    LoadAttrs(@FInBuffer[5 + i], Size - i, a);
    HandleIndex := StringToHandle(p);
    if IsValidHandle(HandleIndex) then
    begin
      DoSetAttributes2(Handles[HandleIndex].Data, a, FErrorCode, FComment);
      if FErrorCode = 0 then
        sftpSendFxpStatus(id, SSH_FX_OK, '', '')
      else
        sftpSendFxpStatus(id, FErrorCode, FComment, '');
    end
    else
      sftpSendFxpStatus(id, SSH_ERROR_INVALID_HANDLE, SInvalidHandle, '');
  finally
    FreeAndNil(a);
  end;
end;

procedure TElSFTPServer.sftpParseFxpRename(id: cardinal; Size: integer);
var
  p, p2: BufferType;
  i: integer;
  Flags : cardinal;
begin
  p := ReadString(@FInBuffer[5], Size);
  i := Length(p) + 4;
  p2 := ReadString(@FInBuffer[5 + i], Size - i);

  if FServerVersion > 5 then
  begin
    i := i + Length(p2) + 4;

    Flags := ReadLength(@FInBuffer[5 + i], Size - i);
  end
  else
    Flags := 0;

  DoRenameFile((p), (p2),
    CardinalToRenameFlags(Flags), FErrorCode, FComment);
  if FErrorCode = 0 then
    sftpSendFxpStatus(id, SSH_FX_OK, '', '')
  else
    sftpSendFxpStatus(id, FErrorCode, FComment, '');
end;

procedure TElSFTPServer.sftpParseFxpRemove(id: cardinal; Size: integer);
var
  p: BufferType;
begin
  p := ReadString(@FInBuffer[5], Size);
  DoRemove((p), FErrorCode, FComment);
  if FErrorCode = 0 then
    sftpSendFxpStatus(id, SSH_FX_OK, '', '')
  else
    sftpSendFxpStatus(id, FErrorCode, FComment, '');
end;

procedure TElSFTPServer.sftpParseFxpMkDir(id: cardinal; Size: integer);
var
  p: BufferType;
begin
  p := ReadString(@FInBuffer[5], Size);
  DoCreateDirectory((p), nil, FErrorCode, FComment);
  if FErrorCode = 0 then
    sftpSendFxpStatus(id, SSH_FX_OK, '', '')
  else
    sftpSendFxpStatus(id, FErrorCode, FComment, '');
end;

procedure TElSFTPServer.sftpParseFxpRmDir(id: cardinal; Size: integer);
var
  p: BufferType;
begin
  p := ReadString(@FInBuffer[5], Size);

  DoRemove((p), FErrorCode, FComment);
  if FErrorCode = 0 then
    sftpSendFxpStatus(id, SSH_FX_OK, '', '')
  else
    sftpSendFxpStatus(id, FErrorCode, FComment, '');
end;

procedure TElSFTPServer.sftpParseFxpOpenDir(id: cardinal; Size: integer);
var
  p: BufferType;
  Handle : TSBSftpHandle;
  HandleIndex : integer;
  Data : pointer;
begin
  p := ReadString(@FInBuffer[5], Size);
  HandleIndex := GetNextFreeHandle();
  if HandleIndex <> -1 then
  begin
    Handle := Handles[HandleIndex];
    ClearHandleInfo(Handle);
    Data := nil;
    DoFindFirst((p), Data, Handle.FInfo, FErrorCode, FComment);
    if FErrorCode = SSH_FX_EOF then
    begin
      Handle.Eof := true;
      Handle.InfoUsed := false;
      FErrorCode := 0;
      FComment := '';
    end;
    if FErrorCode = 0 then
    begin
      Handle.InfoUsed := true;
      Handle.Allocated := true;
      Handle.Data := Data;
      Handle.Directory := true;
      sftpSendFxpHandle(id, HandleToString(HandleIndex));
    end
    else
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
  end
  else
    sftpSendFxpStatus(id, SSH_ERROR_FAILURE, SNoMoreHandlesAvailable, '');
end;

procedure TElSFTPServer.sftpParseFxpReadLink(id: cardinal; Size: integer);
var
  p: BufferType;
  info: TElSftpFileInfo;
begin
  p := ReadString(@FInBuffer[5], Size);
  info := TElSftpFileInfo.Create;
  try
    DoReadSymLink((p), info, FErrorCode, FComment);
    if FErrorCode = 0 then
      sftpSendFxpName(id, info)
    else
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
  finally
    FreeAndNil(info);
  end;
end;

procedure TElSFTPServer.sftpParseFxpSymLink(id: cardinal; Size: integer);
var
  p, p2: BufferType;
  i: integer;
begin
  p := ReadString(@FInBuffer[5], Size);

  i := Length(p) + 4;
  p2 := ReadString(@FInBuffer[5 + i], Size - i);
  
  DoCreateSymLink((p), (p2), FErrorCode, FComment);
  if FErrorCode = 0 then
    sftpSendFxpStatus(id, SSH_FX_OK, SSuccess, '')
  else
    sftpSendFxpStatus(id, FErrorCode, FComment, '');
end;

procedure TElSFTPServer.sftpParseFxpLink(id: cardinal; Size: integer);
var
  NewPath, ExPath: BufferType;
  SymLink : boolean;
  i: integer;
begin
  NewPath := ReadString(@FInBuffer[5], Size);

  i := Length(NewPath) + 4;
  ExPath := ReadString(@FInBuffer[5 + i], Size - i);
  i := i + Length(ExPath) + 4;

  SymLink := ReadBoolean(@FInBuffer[5 + i], Size - i);

  if SymLink then
    DoCreateSymLink((NewPath), (ExPath), FErrorCode, FComment)
  else
    DoCreateHardLink((NewPath), (ExPath), FErrorCode, FComment);

  if FErrorCode = 0 then
    sftpSendFxpStatus(id, SSH_FX_OK, SSuccess, '')
  else
    sftpSendFxpStatus(id, FErrorCode, FComment, '');
end;

procedure TElSFTPServer.sftpParseFxpBlock(id: cardinal; Size: integer);
var
  p: BufferType;
  Handle: TSBSftpHandle;
  HandleIndex, Index : integer;
  Offset, Len : Int64;
  LockMask : cardinal;
begin
  p := ReadString(@FInBuffer[5], Size);
  Index := Length(P) + 4;

  Offset := ReadUINT64(@FInBuffer[Index + 5], Size - Index);
  Len := ReadUINT64(@FInBuffer[Index + 13], Size - Index - 8);
  LockMask := ReadLength(@FInBuffer[Index + 21], Size - Index - 16);

  HandleIndex := StringToHandle(p);
  if IsValidHandle(HandleIndex) then
  begin
    Handle := Handles[HandleIndex];

    DoBlock(Handle.Data, Offset, Len, CardinalToFileOpenAccess(LockMask), FErrorCode, FComment);
    if FErrorCode = 0 then
      sftpSendFxpStatus(id, SSH_FX_OK, '', '')
    else
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
  end
  else
    sftpSendFxpStatus(id, SSH_ERROR_INVALID_HANDLE, FComment, '');
end;

procedure TElSFTPServer.sftpParseFxpUnblock(id: cardinal; Size: integer);
var
  p: BufferType;
  Handle: TSBSftpHandle;
  HandleIndex, Index : integer;
  Offset, Len : Int64;
begin
  p := ReadString(@FInBuffer[5], Size);
  Index := Length(P) + 4;

  Offset := ReadUINT64(@FInBuffer[Index + 5], Size - Index);
  Len := ReadUINT64(@FInBuffer[Index + 13], Size - Index - 8);

  HandleIndex := StringToHandle(p);
  if IsValidHandle(HandleIndex) then
  begin
    Handle := Handles[HandleIndex];

    DoUnblock(Handle.Data, Offset, Len, FErrorCode, FComment);
    if FErrorCode = 0 then
      sftpSendFxpStatus(id, SSH_FX_OK, '', '')
    else
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
  end
  else
    sftpSendFxpStatus(id, SSH_ERROR_INVALID_HANDLE, FComment, '');
end;

procedure TElSFTPServer.sftpParseFxpStat(id: cardinal; Size: integer; followsym: boolean);
var
  p: BufferType;
  inf: TElSftpFileInfo;
begin
  p := ReadString(@FInBuffer[5], Size);

  inf := TElSftpFileInfo.Create;
  try
    DoRequestAttributes((p), followsym, inf.Attributes, FErrorCode, FComment);
    if FErrorCode = 0 then
      sftpSendFxpAttrs(id, inf)
    else
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
  finally
    FreeAndNil(inf);
  end;
end;

procedure TElSFTPServer.sftpParseFxpFStat(id: cardinal; Size: integer);
var
  p: BufferType;
  h: TSBSftpHandle;
  HandleIndex : integer;
begin
  p := ReadString(@FInBuffer[5], Size);

  HandleIndex := StringToHandle(p);
  if IsValidHandle(HandleIndex) then
  begin
    ClearFileInfo(TElSftpFileInfo(FFileInfos[0]));
    h := Handles[HandleIndex];
    DoRequestAttributes2(h.Data, TElSftpFileInfo(FFileInfos[0]).Attributes, FErrorCode, FComment);
    if FErrorCode = 0 then
      sftpSendFxpAttrs(id, TElSftpFileInfo(FFileInfos[0]))
    else
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
  end
  else
    sftpSendFxpStatus(id, SSH_ERROR_INVALID_HANDLE, FComment, '');
end;

procedure TElSFTPServer.sftpParseFxpReadDir(id: cardinal; Size: integer);
var
  p: BufferType;
  h: TSBSftpHandle;
  HandleIndex : integer;
  I : integer;
  EstLen : integer;
begin
  p := ReadString(@FInBuffer[5], Size);

  HandleIndex := StringToHandle(p);
  if IsValidHandle(HandleIndex) then
  begin
    h := Handles[HandleIndex];
    // requesting part of directory
    if not h.Eof then
    begin
      if h.InfoUsed then
      begin
        h.Info.CopyTo(TElSftpFileInfo(FFileInfos[0]));
        I := 1;
        h.InfoUsed := false;
      end
      else
        I := 0;
      EstLen := 0;
      while I < FFileInfos.Count do
      begin
        DoFindNext(h.Data, TElSftpFileInfo(FFileInfos[I]), FErrorCode, FComment);
        if FErrorCode <> 0 then
        begin
          h.Eof := true;
          Break;
        end
        else
        begin
          // increasing estimated length
          Inc(EstLen, Length(MakeAttrs(TElSftpFileInfo(FFileInfos[I]))));
          Inc(EstLen, Length(TElSftpFileInfo(FFileInfos[I]).Name));
          Inc(EstLen, Length(TElSftpFileInfo(FFileInfos[I]).LongName));
          Inc(EstLen, 64);
          Inc(I);
        end;
        if EstLen > BUFFER_SIZE - 8192 then
          Break;
      end;
      if I > 0 then
        sftpSendFxpNames(id, FFileInfos, I)
      else
        sftpSendFxpStatus(id, FErrorCode, FComment, '');
    end
    else
      sftpSendFxpStatus(id, SSH_FX_EOF, SNoFiles, '');
  end
  else
    sftpSendFxpStatus(id, SSH_ERROR_INVALID_HANDLE, SInvalidHandle, '');
end;

procedure TElSFTPServer.sftpParseFxpClose(id: cardinal; Size: integer);
var
  p: BufferType;
  h: TSBSftpHandle;
  HandleIndex : integer;
begin
  p := ReadString(@FInBuffer[5], Size);

  HandleIndex := StringToHandle(p);
  if IsValidHandle(HandleIndex) then
  begin
    h := Handles[HandleIndex];
    if not h.Directory then
      DoCloseHandle(h.Data, FErrorCode, FComment)
    else
      DoFindClose(h.Data, FErrorCode, FComment);
    if FErrorCode = 0 then
      sftpSendFxpStatus(id, SSH_FX_OK, '', '')
    else
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
    h.Allocated := false;
  end
  else
    sftpSendFxpStatus(id, SSH_ERROR_INVALID_HANDLE, SInvalidHandle, '');
end;

procedure TElSFTPServer.sftpParseFxpRealPath(id: cardinal; Size: integer);
var
  name: BufferType;
  CPath : string;
  us: widestring;
  name2 : string;
  Index, Len : integer;
  Control : Byte;
  ComposePath : TStringList;
begin
  name := ReadString(@FInBuffer[5], Size);

  if FServerVersion < 3 then
    us := (name)
  else
    us := ConvertFromUTF8String(name, false);

  Index := ReadLength(@PByteArray(FInBuffer)[5], Size) + 9;
  Dec(Size, Index - 5);

  if FServerVersion > 5 then
  begin
    Control := SSH_FXP_REALPATH_NO_CHECK;
    ComposePath := TStringList.Create;

    if Size > 0 then
    begin
      Control := FInBuffer[Index];
      Inc(Index);
      Dec(Size);

      while Size > 0 do
      begin
        CPath := ReadString(@FInBuffer[Index], Size);
        Len := ReadLength(@FInBuffer[Index], Size);

        ComposePath.Add(CPath);
        Inc(Index, Len + 4);
        Dec(Size, Len + 4);
      end;
    end;
  end
  else
  begin
    Control := 0;
    ComposePath := nil;
  end;

  try
    DoRequestAbsolutePath(us, name2, ByteToRealpathControl(Control), ComposePath, FErrorCode, FComment);
    if FErrorCode <> 0 then
      sftpSendFxpStatus(id, FErrorCode, FComment, '')
    else
      sftpSendFxpName0(id, name2);
  finally
    if Assigned(ComposePath) then
      FreeAndNil(ComposePath);
  end;
end;

{ Parsing of known extension }

procedure TElSFTPServer.sftpParseVersionSelect(id : cardinal;
  const ExtName, ExtData : BufferType; var Cont : boolean);
var
  VerSelectExt : TElSftpVersionSelectExtension;
begin
  Cont := false;
  if FVersionSelect then
  begin
    VerSelectExt := TElSftpVersionSelectExtension.Create;
    VerSelectExt.ExtType := ExtName;
    VerSelectExt.ExtData := ExtData;
    
    try
      if not VerSelectExt.LoadFromBuffer then
      begin
        FErrorCode := SSH_ERROR_INVALID_PACKET;
        FComment := SInvalidPacket;
        sftpSendFxpStatus(id, FErrorCode, FComment, '');
        Close;
        Exit;
      end;

      if VerSelectExt.Version in FVersions then
      begin
        FVersion := VerSelectExt.Version;
        FServerVersion := VerSelectExt.VersionInt;
        sftpSendFxpStatus(id, SSH_FX_OK, '', '');
        DoVersionChange(FVersion);
      end
      else
      begin
        FErrorCode := SSH_ERROR_UNSUPPORTED_VERSION;
        FComment := SUnsupportedVersion;
        sftpSendFxpStatus(id, FErrorCode, FComment, '');
        Close;
      end;
    finally
      VerSelectExt.Free;
    end;
  end
  else
  begin
    FErrorCode := SSH_ERROR_INVALID_PACKET;
    FComment := SInvalidPacket;
    sftpSendFxpStatus(id, FErrorCode, FComment, '');
    Close;
  end;
end;

procedure TElSFTPServer.sftpParseFilenameTranslationControl(id : cardinal;
  const ExtName, ExtData : BufferType; var Cont : boolean);
var
  FNameTranslationExt : TElSftpFilenameTranslationExtension;
begin
  Cont := false;
  if not FFTranslationControl then
  begin
    FErrorCode := SSH_ERROR_OP_UNSUPPORTED;
    FComment := SUnsupportedExtension;
    sftpSendFxpStatus(id, FErrorCode, FComment, '');
    Exit;
  end;  

  FNameTranslationExt := TElSftpFilenameTranslationExtension.Create;
  FNameTranslationExt.ExtType := ExtName;
  FNameTranslationExt.ExtData := ExtData;

  try
    if not FNameTranslationExt.LoadFromBuffer then
    begin
      FErrorCode := SSH_ERROR_INVALID_PACKET;
      FComment := SInvalidPacket;
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
      Close;
      Exit;
    end
    else
      sftpSendFxpStatus(id, SSH_FX_OK, '', '');

    if not FNameTranslationExt.DoTranslate then
      FNegotiatedCharset := FFilenameCharset
    else
      FNegotiatedCharset := 'utf8';
  finally
    FreeAndNil(FNameTranslationExt);
  end;
end;

procedure TElSFTPServer.sftpParseVendorID(id : cardinal; const ExtName,
  ExtData : BufferType; var Cont : boolean);
var
  VendorIDExt : TElSftpVendorIDExtension;
begin
  Cont := false;
  VendorIDExt := TElSftpVendorIDExtension.Create;
  VendorIDExt.ExtType := ExtName;
  VendorIDExt.ExtData := ExtData;

  try
    if not VendorIDExt.LoadFromBuffer then
    begin
      FErrorCode := SSH_ERROR_INVALID_PACKET;
      FComment := SInvalidPacket;
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
      FreeAndNil(VendorIDExt);
    end
    else
    begin
      FClientVendorID := VendorIDExt;
      sftpSendFxpStatus(id, SSH_FX_OK, '', '');
    end;
  except
    FreeAndNil(VendorIDExt);
  end;
end;

procedure TElSFTPServer.sftpParseCheckFile(id : cardinal; const ExtName,
  ExtData : BufferType; var Cont : boolean);
var
  CheckFileExt : TElSftpCheckFileExtension;
  CheckFileReply : TElSftpCheckFileReply;
  Handle : TSBSFTPHandle;
begin
  Cont := false;
  CheckFileExt := TElSftpCheckFileExtension.Create;
  CheckFileExt.ExtType := ExtName;
  CheckFileExt.ExtData := ExtData;

  CheckFileReply := TElSftpCheckFileReply.Create;
  Cont := false;

  try
    if not CheckFileExt.LoadFromBuffer then
    begin
      FErrorCode := SSH_ERROR_INVALID_PACKET;
      FComment := SInvalidPacket;
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
      Close;
      Exit;
    end
    else
    begin
      if (Length(CheckFileExt.Name) > 0) and Assigned(FOnRequestFileHash) then
        FOnRequestFileHash(Self, CheckFileExt, CheckFileReply, FErrorCode, FComment)
      else
      if (Length(CheckFileExt.Handle) > 0) and Assigned(FOnRequestFileHashByHandle) then
      begin
        if not IsValidHandle(StringToHandle(CheckFileExt.Handle)) then
        begin
          FErrorCode := SSH_ERROR_INVALID_HANDLE;
          FComment := SInvalidHandle;
        end
        else
        begin
          Handle := Handles[StringToHandle(CheckFileExt.Handle)];
          if not Assigned(Handle) then
          begin
            FErrorCode := SSH_ERROR_INVALID_HANDLE;
            FComment := SInvalidHandle;
          end
          else if Handle.Directory then
          begin
            FErrorCode := SSH_ERROR_FILE_IS_A_DIRECTORY;
            FComment := SInvalidHandle;
          end
          else
            FOnRequestFileHashByHandle(Self, Handle.Data, CheckFileExt,
              CheckFileReply, FErrorCode, FComment);
        end;
      end
      else
      begin
        Cont := true;
        Exit;
      end;

      if FErrorCode = SSH_ERROR_OK then
      begin
        CheckFileReply.SaveToBuffer;

        if Length(CheckFileReply.ReplyData) = 0 then
        begin
          FErrorCode := SSH_ERROR_FAILURE;
          FComment := SUnsupportedValue;
          sftpSendFxpStatus(id, FErrorCode, FComment, '');
          Exit;
        end;

        sftpSendFxpExtendedReply(id, @CheckFileReply.ReplyData[1],
          Length(CheckFileReply.ReplyData));
      end
      else
      begin
        sftpSendFxpStatus(id, FErrorCode, FComment, '');
        Exit;
      end;
    end;
  finally
    FreeAndNil(CheckFileExt);
    FreeAndNil(CheckFileReply);
  end;
end;

procedure TElSFTPServer.sftpParseSpaceAvailable(id : cardinal; const
  ExtName, ExtData : BufferType; var Cont : boolean);
var
  SpaceAvailReply : TElSftpSpaceAvailableReply;
  Path : string;
begin
  Cont := false;

  try
    Path := ReadString(@ExtData[1], Length(ExtData));
  except
    Path := '';
  end;

  SpaceAvailReply := TElSftpSpaceAvailableReply.Create;

  try
    if Length(Path) > 0 then
    begin
      if not Assigned(FOnRequestAvailableSpace) then
      begin
        Cont := true;
        Exit;
      end;

      FOnRequestAvailableSpace(Self, Path, SpaceAvailReply, FErrorCode, FComment);
    end
    else
    begin
      FErrorCode := SSH_ERROR_INVALID_PACKET;
      FComment := SInvalidPacket;
    end;

    if FErrorCode = SSH_ERROR_OK then
    begin
      SpaceAvailReply.SaveToBuffer;
      sftpSendFxpExtendedReply(id, @SpaceAvailReply.ReplyData[1],
        Length(SpaceAvailReply.ReplyData));
    end
    else
    begin
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
      Exit;
    end;
  finally
    FreeAndNil(SpaceAvailReply);
  end;
end;

procedure TElSFTPServer.sftpParseHomeDirectory(id : cardinal; const
  ExtName, ExtData : BufferType; var Cont : boolean);
var
  Username, Path : string;
begin
  Cont := false;

  try
    Username := ReadString(@ExtData[1], Length(ExtData));
  except
    Username := '';
  end;

  if Length(Username) > 0 then
  begin
    if not Assigned(FOnRequestHomeDirectory) then
    begin
      Cont := true;
      Exit;
    end;

    FOnRequestHomeDirectory(Self, Username, Path, FErrorCode, FComment);
  end
  else
  begin
    FErrorCode := SSH_ERROR_INVALID_PACKET;
    FComment := SInvalidPacket;
  end;

  if FErrorCode = SSH_ERROR_OK then
    sftpSendFxpName0(id, Path)
  else
    sftpSendFxpStatus(id, FErrorCode, FComment, '');
end;

procedure TElSFTPServer.sftpParseCopyRemoteFile(id : cardinal; const
  ExtName, ExtData : BufferType; var Cont : boolean);
var
  CopyFileExt : TElSftpCopyFileExtension;
begin
  Cont := false;
  CopyFileExt := TElSftpCopyFileExtension.Create;
  CopyFileExt.ExtType := ExtName;
  CopyFileExt.ExtData := ExtData;

  try
    if not CopyFileExt.LoadFromBuffer then
    begin
      FErrorCode := SSH_ERROR_INVALID_PACKET;
      FComment := SInvalidPacket;
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
      Exit;
    end
    else
    if Assigned(FOnCopyRemoteFile) then
    begin
      FOnCopyRemoteFile(Self, CopyFileExt.Source, CopyFileExt.Destination,
        CopyFileExt.Overwrite, FErrorCode, FComment);

      if FErrorCode = SSH_ERROR_OK then
        sftpSendFxpStatus(id, SSH_FX_OK, '', '')
      else
        sftpSendFxpStatus(id, FErrorCode, FComment, '');
    end
    else
      Cont := true;
  finally
    FreeAndNil(CopyFileExt);
  end;
end;

procedure TElSFTPServer.sftpParseCopyRemoteData(id : cardinal; const
  ExtName, ExtData : BufferType; var Cont : boolean);
var
  CopyDataExt : TElSftpCopyDataExtension;
  SrcHndl, DstHndl : integer;
  Src, Dst : TSBSftpHandle;
begin
  Cont := false;
  CopyDataExt := TElSftpCopyDataExtension.Create;
  CopyDataExt.ExtType := ExtName;
  CopyDataExt.ExtData := ExtData;

  try
    if not CopyDataExt.LoadFromBuffer then
    begin
      FErrorCode := SSH_ERROR_INVALID_PACKET;
      FComment := SInvalidPacket;
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
      Exit;
    end
    else
    if Assigned(FOnCopyRemoteData) then
    begin
      SrcHndl := StringToHandle(CopyDataExt.ReadHandle);
      DstHndl := StringToHandle(CopyDataExt.WriteHandle);

      Src := Handles[SrcHndl];
      Dst := Handles[DstHndl];

      if (SrcHndl = -1) or (DstHndl = -1) or (not Assigned(Src))
        or (not Assigned(Dst)) then
      begin
        FErrorCode := SSH_ERROR_INVALID_HANDLE;
        FComment := SInvalidHandle;
        sftpSendFxpStatus(id, FErrorCode, FComment, '');
        Exit;
      end;

      FOnCopyRemoteData(Self, Src.Data, CopyDataExt.ReadOffset, Dst.Data,
        CopyDataExt.WriteOffset, CopyDataExt.DataLength, FErrorCode, FComment);

      if FErrorCode = SSH_ERROR_OK then
        sftpSendFxpStatus(id, SSH_FX_OK, '', '')
      else
        sftpSendFxpStatus(id, FErrorCode, FComment, '');
    end
    else
      Cont := true;
  finally
    FreeAndNil(CopyDataExt);
  end;
end;

procedure TElSFTPServer.sftpParseFxpExtended(id: cardinal; Size: integer);
var
  ReqName, ReqData : BufferType;
  Str : TMemoryStream;
  Index, ReqPos : integer;
  Buf : ByteArray;
  Cont : boolean;
  Path : string;
begin
  ReqName := ReadString(@FInBuffer[5], Size);
  ReqPos := 9 + ReadLength(@FInBuffer[5], Size);
  SetLength(ReqData, Size - ReqPos + 5);
  Move(FInBuffer[ReqPos], ReqData[1], Size - ReqPos + 5);

  // parsing known extended types
  if CompareStr(ReqName, SSH_EXT_TEXTSEEK) = 0 then
  begin
    sftpParseFxpTextSeek(id, Size);
    Exit;
  end;

  if ReqName = SSH_EXT_VERSION_SELECT then
  begin
    Cont := false;
    sftpParseVersionSelect(id, ReqName, ReqData, Cont);
    if not Cont then Exit;
  end;

  if ReqName = SSH_EXT_FILENAME_TRANSLATION_CONTROL then
  begin
    Cont := false;
    sftpParseFilenameTranslationControl(id, ReqName, ReqData, Cont);
    if not Cont then Exit;
  end;

  if ReqName = SSH_EXT_VENDOR_ID then
  begin
    Cont := false;
    sftpParseVendorID(id, ReqName, ReqData, Cont);
    if not Cont then Exit;
  end;

  if (ReqName = SSH_EXT_CHECK_FILE_NAME) or
    (ReqName = SSH_EXT_CHECK_FILE_HANDLE) then
  begin
    Cont := false;
    sftpParseCheckFile(id, ReqName, ReqData, Cont);
    if not Cont then Exit;
  end;

  if (ReqName = SSH_EXT_SPACE_AVAILABLE) then
  begin
    Cont := false;
    sftpParseSpaceAvailable(id, ReqName, ReqData, Cont);
    if not Cont then Exit;
  end;

  if (ReqName = SSH_EXT_COPY_FILE) then
  begin
    Cont := false;
    sftpParseCopyRemoteFile(id, ReqName, ReqData, Cont);
    if not Cont then Exit;
  end;

  if (ReqName = SSH_EXT_COPY_DATA) then
  begin
    Cont := false;
    sftpParseCopyRemoteData(id, ReqName, ReqData, Cont);
    if not Cont then Exit;
  end;

  if (ReqName = SSH_EXT_HOME_DIRECTORY) then
  begin
    Cont := false;
    sftpParseHomeDirectory(id, ReqName, ReqData, Cont);
    if not Cont then Exit;
  end;

  if (ReqName = SSH_EXT_GET_TEMP_FOLDER) then
  begin
    if Assigned(FOnRequestTempFolder) then
    begin
      FOnRequestTempFolder(Self, Path, FErrorCode, FComment);
      if FErrorCode = SSH_ERROR_OK then
        sftpSendFxpName0(id, Path)
      else
        sftpSendFxpStatus(id, FErrorCode, FComment, '');
      Exit;  
    end;
  end;

  if (ReqName = SSH_EXT_MAKE_TEMP_FOLDER) then
  begin
    if Assigned(FOnMakeTempFolder) then
    begin
      FOnRequestTempFolder(Self, Path, FErrorCode, FComment);
      if FErrorCode = SSH_ERROR_OK then
        sftpSendFxpName0(id, Path)
      else
        sftpSendFxpStatus(id, FErrorCode, FComment, '');
       Exit; 
    end;
  end;

  // parsing other (unknown) extended types
  Index := 9 + Length(ReqName);
  Dec(Size, Length(ReqName) + 4);
  Str := TMemoryStream.Create;
  try
    FErrorCode := SSH_ERROR_OP_UNSUPPORTED;
    FComment := SUnsupportedExtension;
    DoExtendedRequest(ReqName, @FInBuffer[Index], Size, Str,
      FErrorCode, FComment);
    if FErrorCode <> 0 then
      sftpSendFxpStatus(id, FErrorCode, FComment, '')
    else
    begin
      SetLength(Buf, Str.Size);
      Str.Position := 0;
      Str.ReadBuffer(Buf[0], Length(Buf));
      sftpSendFxpExtendedReply(id, @Buf[0], Length(Buf))
    end;
  finally
    FreeAndNil(Str);
  end;
end;

function TElSFTPServer.FakeAttrs: BufferType;
begin
  result := WriteUINT32(0);
  if FServerVersion > 3 then
    result := result + char(0);
end;

function TElSFTPServer.MakeAttrs(f: TElSftpFileInfo): BufferType;
begin
  Result := f.Attributes.SaveToBuffer(FServerVersion);
end;

procedure TElSFTPServer.sftpSendFxpName0(id: cardinal; const name: string);
var
  Tmp : BufferType;
  ToSend : integer;
begin
  Tmp := char(SSH_FXP_NAME) + WriteUINT32(id) + WriteUINT32(1) + WriteString(name);
  if FServerVersion < 4 then
    Tmp := Tmp + WriteString(name);
  Tmp := Tmp + FakeAttrs;
  ToSend := Min(Length(Tmp), BUFFER_SIZE - 4);
  Move(Tmp[1], FOutBuffer[4], ToSend);
  SendReply(ToSend);
end;

procedure TElSFTPServer.sftpSendFxpName(id: cardinal; name: TElSftpFileInfo);
var
  Tmp : BufferType;
  ToSend : integer;
begin
  Tmp := char(SSH_FXP_NAME) + WriteUINT32(id) + WriteUINT32(1) + WriteString(name.Name);
  if FServerVersion < 4 then
    Tmp := Tmp + WriteString(name.LongName);
  Tmp := Tmp + MakeAttrs(name);
  ToSend := Min(Length(Tmp), BUFFER_SIZE - 4);
  Move(Tmp[1], FOutBuffer[4], ToSend);
  SendReply(ToSend);
end;

procedure TElSFTPServer.sftpSendFxpNames(id: cardinal; names: TList; Count : integer);
var
  Tmp : BufferType;
  i: integer;
  info: TElSftpFileInfo;
  ToSend : integer;
begin
  Tmp := char(SSH_FXP_NAME) + WriteUINT32(id) + WriteUINT32(Count);
  for i := 0 to Count - 1 do
  begin
    info := TElSftpFileInfo(names[i]);
    Tmp := Tmp + WriteString(info.Name);
    if FServerVersion < 4 then
      Tmp := Tmp + WriteString(info.LongName);
    Tmp := Tmp + MakeAttrs(info);
  end;
  Assert(Length(Tmp) <= BUFFER_SIZE - 4);
  ToSend := Min(Length(Tmp), BUFFER_SIZE - 4);
  Move(Tmp[1], FOutBuffer[4], ToSend);
  SendReply(ToSend);
end;

procedure TElSFTPServer.sftpSendFxpExtendedReply(id: cardinal; Buffer: pointer;
  Size: integer);
var
  Tmp : BufferType;
  Len : integer;
  ToSend : integer;
begin
  Tmp := char(SSH_FXP_EXTENDED_REPLY) + WriteUINT32(id);
  Len := Length(Tmp);
  Move(Tmp[1], FOutBuffer[4], Len);
  ToSend := Min(Size, BUFFER_SIZE - 4 - Len);
  Move(Buffer^, FOutBuffer[4 + Len], ToSend);
  SendReply(Len + ToSend);
end;

procedure TElSFTPServer.sftpParseFxpInit(Size: integer);
begin
  if Size <> 4 then
    DoError(SSH_ERROR_INVALID_PACKET, SDisconnectInvalidPacketSize);
  FClientVersion := ReadLength(@FInBuffer[1], Size);
  sftpSendFxpVersion;
end;

procedure TElSFTPServer.sftpParseFxpTextSeek(id: cardinal; Size: integer);
var
  ReqName : BufferType;
  Index : integer;
  H : BufferType;
  HandleIndex : integer;
  LineNum : Int64;
  Handle : TSBSftpHandle;
begin
  ReqName := ReadString(@FInBuffer[5], Size);

  if CompareStr(ReqName, SSH_EXT_TEXTSEEK) <> 0 then
    Exit;

  Index := 9 + Length(ReqName);

  // handle
  Dec(Size, Length(ReqName) + 4);
  H := ReadString(@FInBuffer[Index], Size);
  Dec(Size, Length(H) + 4);
  Inc(Index, Length(H) + 4);

  // line number
  LineNum := ReadUINT64(@FInBuffer[Index], Size);

  HandleIndex := StringToHandle(H);
  if IsValidHandle(HandleIndex) then
  begin
    Handle := Handles[HandleIndex];
    if not Handle.Directory then
      DoTextSeek(Handle.FData, LineNum, FErrorCode, FComment)
    else
    begin
      FErrorCode := SSH_ERROR_PERMISSION_DENIED;
      FComment := 'Cannot seek directory';
    end;
    if FErrorCode = 0 then
      sftpSendFxpStatus(id, SSH_FX_OK, '', '')
    else
      sftpSendFxpStatus(id, FErrorCode, FComment, '');
  end
  else
    sftpSendFxpStatus(id, SSH_ERROR_INVALID_HANDLE, SInvalidHandle, '');
end;

function min(a, b: integer): integer; begin if a < b then result := a else result := b; end;
function max(a, b: integer): integer; begin if a > b then result := a else result := b; end;

procedure TElSFTPServer.sftpSendFxpVersion;
var
  Tmp : BufferType;
  I : integer;
  Flag : boolean;
  NewlineExt : TElSFTPNewlineExtension;
  VersionsExt : TElSFTPVersionsExtension;
  FCharsetExt : TElSftpFilenameCharsetExtension;
  VendorIDExt : TElSFTPVendorIDExtension;
const
  VersionIndeces : array[0..6] of TSBSftpVersion =
    (
    sbSFTP0, sbSFTP1, sbSFTP2,
    sbSFTP3, sbSFTP4, sbSFTP5,
    sbSFTP6
    );
begin
  I := FClientVersion;
  while (I >= 0) and
    (not (VersionIndeces[I] in FVersions))
    do
    Dec(I);
  if I < 0 then
  begin
    DoError(SSH_ERROR_UNSUPPORTED_VERSION, SUnsupportedVersion);
    DoClose();
    Exit;
  end;

  FServerVersion := I;
  //FServerVersion := max(SFTP_MIN_VERSION, min(SFTP_MAX_VERSION, FClientVersion));
  FVersion := VersionIndeces[FServerVersion];
  Tmp := char(SSH_FXP_VERSION) + WriteUINT32(FServerVersion);

  if FServerVersion >= 3 then
  begin
    NewlineExt := TElSftpNewlineExtension.Create;
    NewlineExt.Newline := FNewLineConvention;
    Tmp := (Tmp) + NewlineExt.Write;
    NewlineExt.Free;
  end;

  if FServerVersion >= 3 then
  begin
    if FServerVersion < 5 then
      FSupported.Version := 1
    else
      FSupported.Version := 2;

    Tmp := (Tmp) + FSupported.Write;
  end;

  FVersionSelect := false;
  if FServerVersion >= 3 then
  begin
    VersionsExt := TElSftpVersionsExtension.Create;
    VersionsExt.Versions := FVersions;
    Tmp := (Tmp) + VersionsExt.Write;
    VersionsExt.Free;
    FVersionSelect := true;
  end;

  FFTranslationControl := false;
  if FServerVersion < 3 then FNegotiatedCharset := ''
  else FNegotiatedCharset := 'utf8';

  if (FServerVersion >= 3) and (FFilenameCharset <> '') and (FFilenameCharset <> 'utf8') then
  begin
    FCharsetExt := TElSftpFilenameCharsetExtension.Create;
    FCharsetExt.Charset := FFilenameCharset;
    Tmp := (Tmp) + FCharsetExt.Write;
    FCharsetExt.Free;
    FFTranslationControl := true;
  end;

  if (FServerVersion >= 3) then
  begin
    VendorIDExt := TElSftpVendorIDExtension.Create;

    if Assigned(FOnSendVendorID) then
      FOnSendVendorID(Self, VendorIDExt, Flag)
    else
      Flag := true;

    if Flag then
      Tmp := (Tmp) + VendorIDExt.Write;
    VendorIDExt.Free;
  end;

  Move(Tmp[1], FOutBuffer[4], Length(Tmp));

  SendReply(Length(Tmp));
  DoOpen;
end;

function TElSFTPServer.GetDataPacket(Buffer: pointer; var Size: integer): boolean;
var
  pktsize: integer;
  written: integer;
begin
  result := true;
  DoReceive(@pktsize, sizeof(pktsize), written);
  if written <> SizeOf(pktsize) then
  begin
    DoError(SSH_ERROR_INVALID_PACKET, SDisconnectInvalidPacketSize);
    result := false;
    Exit;
  end;
  pktsize := (PByteArray(@pktsize)[0] shl 24) or (PByteArray(@pktsize)[1] shl 16) or
    (PByteArray(@pktsize)[2] shl 8) or PByteArray(@pktsize)[3];
  if (pktsize < 1) or (pktsize > BUFFER_SIZE) then
  begin
    DoError(SSH_ERROR_INVALID_PACKET, SDisconnectInvalidPacketSize);
    result := false;
    Exit;
  end;
  DoReceive(@FInBuffer[0], pktsize, written);
  if written <> pktsize then
  begin
    DoError(SSH_ERROR_INVALID_PACKET, SDisconnectInvalidPacketSize);
    result := false;
    Exit;
  end;
  Size := pktsize;
end;

procedure TElSFTPServer.DoError(ErrorCode: integer; const Comment: string);
begin
  if Assigned(FOnError) then
    FOnError(Self, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoClose;
begin
  if Assigned(FOnClose) then
    FOnClose(Self);
end;

// -------------------------------------------------------------------------------

procedure TElSFTPServer.DoBlock(
  Data : pointer;
  Offset, Length : Int64; LockMask : TSBSftpFileOpenAccess;
  var ErrorCode : integer; var Comment : string);
begin
  if Assigned(FOnBlock) then
    FOnBlock(Self, Data, Offset, Length, LockMask, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoUnblock(
  Data : pointer;
  Offset, Length : Int64; var ErrorCode : integer; var Comment : string);
begin
  if Assigned(FOnUnblock) then
    FOnUnblock(Self, Data, Offset, Length, ErrorCode, Comment);
end;  


procedure TElSFTPServer.DoCloseHandle(Data: pointer;
  var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnCloseHandle) then
    FOnCloseHandle(Self, Data, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoCreateDirectory(const Path: string; Attributes:
  TElSftpFileAttributes; var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnCreateDirectory) then
    FOnCreateDirectory(Self, Path, Attributes, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoCreateSymLink(const LinkPath, TargetPath:
  string; var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnCreateSymLink) then
    FOnCreateSymLink(Self, LinkPath, TargetPath, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoCreateHardLink(const NewLinkPath, ExistingPath: string;
  var ErrorCode: integer; var Comment: string);
begin
  if  Assigned(FOnCreateHardLink) then
    FOnCreateHardLink(Self, NewLinkPath, ExistingPath, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoFindClose(Data: pointer;
  var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnFindClose) then
    FOnFindClose(Self, Data, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoFindFirst(const Path: string;
  var Data: pointer;
  Info: TElSftpFileInfo; var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnFindFirst) then
    FOnFindFirst(Self, Path, Data, Info, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoFindNext(Data: pointer;
  Info: TElSftpFileInfo; var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnFindNext) then
    FOnFindNext(Self, Data, Info, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoOpen;
begin
  if Assigned(FOnOpen) then
    FOnOpen(Self);
end;

procedure TElSFTPServer.DoOpenFile(const Path: string; Modes:
  TSBSftpFileOpenModes; Access: TSBSftpFileOpenAccess;
  DesiredAccess : cardinal; Attributes: TElSftpFileAttributes;
  var Data: pointer;
  var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnOpenFile) then
    FOnOpenFile(Self, Path, Modes, Access, DesiredAccess, Attributes, Data, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoReadFile(Data : pointer; Offset: Int64; Buffer: Pointer;
  Count: integer; var Read, ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnReadFile) then
    FOnReadFile(Self, Data, Offset, Buffer, 
      Count, Read, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoReadSymLink(const Path: string; Info:
    TElSftpFileInfo; var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnReadSymLink) then
    FOnReadSymLink(Self, Path, Info, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoReceive(Buffer: pointer; MaxSize: integer; var
  Written: integer);
begin
  if Assigned(FOnReceive) then
    FOnReceive(Self, Buffer, MaxSize, Written);
end;

procedure TElSFTPServer.DoRemove(const Path: string; var ErrorCode:
  integer; var Comment: string);
begin
  if Assigned(FOnRemove) then
    FOnRemove(Self, Path, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoRenameFile(const OldPath, NewPath: string;
  Flags : TSBSftpRenameFlags; var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnRenameFile) then
    FOnRenameFile(Self, OldPath, NewPath, Flags, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoRequestAbsolutePath(const Path: string; var
  AbsolutePath: string; Control : TSBSftpRealpathControl;
  ComposePath : TStringList; var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnRequestAbsolutePath) then
    FOnRequestAbsolutePath(Self, Path, AbsolutePath, Control,
      ComposePath, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoRequestAttributes(const Path: string;
  FollowSymLinks: boolean; Attributes: TElSftpFileAttributes; var ErrorCode:
  integer; var Comment: string);
begin
  if Assigned(FOnRequestAttributes) then
    FOnRequestAttributes(Self, Path, FollowSymLinks, Attributes, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoRequestAttributes2(Data: pointer;
  Attributes: TElSftpFileAttributes; var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnRequestAttributes2) then
    FOnRequestAttributes2(Self, Data, Attributes, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoSend(Buffer: pointer; Size: integer);
begin
  if Assigned(FOnSend) then
    FOnSend(Self, Buffer, Size);
end;

procedure TElSFTPServer.DoSetAttributes(const Path: string; Attributes:
  TElSftpFileAttributes; var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnSetAttributes) then
    FOnSetAttributes(Self, Path, Attributes, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoSetAttributes2(Data : pointer;
  Attributes : TElSftpFileAttributes; var ErrorCode: integer; var Comment: string); 
begin
  if Assigned(FOnSetAttributes2) then
    FOnSetAttributes2(Self, Data, Attributes, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoTextSeek(Data : pointer;
  LineNumber: Int64; var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnTextSeek) then
    FOnTextSeek(Self, Data, LineNumber, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoWriteFile(Data : pointer; Offset: Int64; Buffer: Pointer;
  Count: integer; var ErrorCode: integer; var Comment: string);
begin
  if Assigned(FOnWriteFile) then
    FOnWriteFile(Self, Data, Offset, Buffer, 
      Count, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoExtendedRequest(const Request: string; Buffer : pointer;
  Size : integer; Response : TStream; var ErrorCode : integer;
  var Comment : string);
begin
  if Assigned(FOnExtendedRequest) then
    FOnExtendedRequest(Self, Request, Buffer, Size, Response, ErrorCode, Comment);
end;

procedure TElSFTPServer.DoVersionChange(Version : TSBSFTPVersion);
begin
  if Assigned(FOnVersionChange) then
    FOnVersionChange(Self, Version);
end;

////////////////////////////////////////////////////////////////////////////////
// TSBSftpHandle class

constructor TSBSftpHandle.Create;
begin
  inherited;
  FAllocated := false;
  FInfoUsed := false;
  FInfo := TElSftpFileInfo.Create;
  FEof := false;
  FDirectory := false;
end;

destructor TSBSftpHandle.Destroy;
begin
  FreeAndNil(FInfo);
  inherited;
end;

end.
