
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

unit SBASN1Tree;

{$I SecBbox.inc}

interface

uses
  Classes,
  SBUtils,
  SBASN1;


const
  SB_ASN1_BOOLEAN               = 1;
  SB_ASN1_INTEGER               = 2;
  SB_ASN1_BITSTRING             = 3;
  SB_ASN1_OCTETSTRING           = 4;
  SB_ASN1_NULL                  = 5;
  SB_ASN1_OBJECT                = 6;
  SB_ASN1_REAL                  = 9;
  SB_ASN1_ENUMERATED            = 10;
  SB_ASN1_UTF8STRING            = 12;
  SB_ASN1_NUMERICSTR            = 18;
  SB_ASN1_PRINTABLESTRING       = 19;
  SB_ASN1_T61STRING             = 20;
  SB_ASN1_TELETEXSTRING         = 20;
  SB_ASN1_VIDEOTEXSTRING        = 21;
  SB_ASN1_IA5STRING             = 22;
  SB_ASN1_UTCTIME               = 23;
  SB_ASN1_GENERALIZEDTIME       = 24;
  SB_ASN1_GRAPHICSTRING         = 25;
  SB_ASN1_VISIBLESTRING         = 26;
  SB_ASN1_GENERALSTRING         = 27;
  SB_ASN1_UNIVERSALSTRING       = 28;
  SB_ASN1_BMPSTRING             = 30;
  SB_ASN1_SEQUENCE              = $30;
  SB_ASN1_SET                   = $31;
  SB_ASN1_A0_PRIMITIVE          = $80;
  SB_ASN1_A0                    = $A0;
  SB_ASN1_A1_PRIMITIVE          = $81;
  SB_ASN1_A1                    = $A1;
  SB_ASN1_A2_PRIMITIVE          = $82;
  SB_ASN1_A2                    = $A2;
  SB_ASN1_A3_PRIMITIVE          = $83;
  SB_ASN1_A3                    = $A3;
  SB_ASN1_A4_PRIMITIVE          = $84;
  SB_ASN1_A4                    = $A4;
  SB_ASN1_A5_PRIMITIVE          = $85;
  SB_ASN1_A5                    = $A5;
  SB_ASN1_A6_PRIMITIVE          = $86;
  SB_ASN1_A6                    = $A6;
  SB_ASN1_A7_PRIMITIVE          = $87;
  SB_ASN1_A7                    = $A7;
  SB_ASN1_A8_PRIMITIVE          = $88;
  SB_ASN1_A8                    = $A8;

type
  TElASN1CustomTag = class
  protected
    FTagId : byte;
    FWriteHeader : boolean;
    
    function GetConstrained : boolean; virtual;
    function GetTagNum : byte;

  protected
    Parent : TElASN1CustomTag;
    FUndefSize : boolean;
    FTagOffset : integer;
    FTagSize : integer;
    FTagHeaderSize : integer;
  public
    constructor Create;
    destructor Destroy; override;
    
    function LoadFromBuffer(Buffer : pointer; Size : integer) : boolean; virtual;
    function SaveToBuffer(Buffer : pointer; var Size : integer) : boolean; virtual;
    function CheckType(TagId: byte; Constrained: boolean) : boolean;

//    function LoadFromStream(Stream: TStream) : boolean; virtual; abstract;
//    function SaveToStream(Stream: TStream): boolean; virtual; abstract;
    property TagId : byte read FTagId write FTagId;
    property UndefSize : boolean read FUndefSize write FUndefSize;
    property WriteHeader : boolean read FWriteHeader write FWriteHeader;
    property IsConstrained : boolean read GetConstrained;
    property TagNum : byte read GetTagNum;
    property TagOffset : integer read FTagOffset;
    property TagSize : integer read FTagSize;
  end;

{$hints off}
  TElASN1SimpleTag = class(TElASN1CustomTag)
  protected
  
    function SaveToBufferUndefSize(Buffer : pointer; var Size : integer) : boolean;
    function GetConstrained : boolean; override;

    procedure SetContent(Value : BufferType);
    
  protected
    FContent : BufferType;

  public
    constructor Create;
    destructor Destroy; override;
    
    function LoadFromBuffer(Buffer : pointer; Size : integer) : boolean; override;
    function SaveToBuffer(Buffer : pointer; var Size : integer) : boolean; override;

    property Content : BufferType read FContent write SetContent;

    //property TagId;
    //property UndefSize;
    //property WriteHeader;
  end;

  TElASN1ConstrainedTag = class(TElASN1CustomTag)
  protected
    FList : TList;
    FStack : TList;

    FBuffer: ^byte;
    FBufferSize : integer;

    FCurrBufferIndex : integer;
    FSingleLoad : boolean;
    FDataProcessed : boolean;
    FSizeLeft : integer;
    FLastHeaderLen : integer;
    FLastUndefLen : boolean;
    procedure ClearList;
    procedure HandleASN1Read(Sender : TObject; Buffer : pointer; var Size : longint);
    procedure HandleASN1Tag(Sender : TObject; TagType: asn1TagType; TagConstrained: boolean;
      Tag: pointer; TagSize: integer; Size: integer; Data: pointer; BitRest: integer;
      var Valid : boolean);
    procedure HandleASN1TagHeader(Sender: TObject; TagID : byte; TagLen : integer;
      HeaderLen : integer; UndefLen : boolean);
    function GetCount : integer;

    function SaveToBufferUndefSize(Buffer : pointer; var Size : integer) : boolean;
    function GetConstrained : boolean; override;
  public
    constructor Create;
    destructor Destroy; override;
    
    function LoadFromBuffer(Buffer : pointer; Size : integer) : boolean; override;
    function LoadFromBufferSingle(Buffer: pointer; Size: integer) : integer; 
    function SaveToBuffer(Buffer : pointer; var Size : integer) : boolean; override;
    function SaveContentToBuffer(Buffer : pointer; var Size : integer) : boolean;
    function AddField(Constrained : boolean) : integer;
    function RemoveField(Index : integer) : boolean;
    function GetField(Index : integer) : TElASN1CustomTag;
    procedure Clear;
    //property TagId;
    //property UndefSize;
    //property WriteHeader;
    property Count : integer read GetCount;
  end;
{$hints on}

procedure asymWriteInteger(Tag : TElASN1SimpleTag; Buffer : pointer; Size : integer);

function ASN1ReadInteger(Tag: TElASN1SimpleTag): integer; 
procedure ASN1WriteInteger(Tag: TElASN1SimpleTag; Value: integer); 

implementation

uses
  SysUtils;

////////////////////////////////////////////////////////////////////////////////
// TElASN1CustomTag

constructor TElASN1CustomTag.Create;
begin
  inherited;
  FUndefSize := false;
  FWriteHeader := true;
  Parent := nil;
end;

destructor TElASN1CustomTag.Destroy;
begin
  inherited;
end;

function TElASN1CustomTag.LoadFromBuffer(Buffer : pointer; Size : integer) : boolean;
begin
  Result := false;
end;


function TElASN1CustomTag.SaveToBuffer(Buffer : pointer; var Size : integer) :
  boolean;
begin
  Result := false;
end;

function TElASN1CustomTag.GetConstrained : boolean;
begin
  Result := false;
end;

function TElASN1CustomTag.GetTagNum : byte;
begin
  Result := FTagID and $1F;
end;

function TElASN1CustomTag.CheckType(TagId: byte; Constrained: boolean) : boolean;
begin
  Result := (FTagID = TagID) and (Constrained = IsConstrained);
end;

////////////////////////////////////////////////////////////////////////////////
// TElASN1SimpleTag

constructor TElASN1SimpleTag.Create;
begin
  inherited;
end;

destructor TElASN1SimpleTag.Destroy;
begin
  inherited;
end;

procedure TElASN1SimpleTag.SetContent(Value : BufferType);
begin
  FContent := CloneBuffer(Value);
end;

function TElASN1SimpleTag.LoadFromBuffer(Buffer : pointer; Size : integer) : boolean;
begin
  Result := false;
end;

function TElASN1SimpleTag.SaveToBuffer(Buffer : pointer; var Size : integer) :
  boolean;
var
  HLen, Len, I : integer;
begin
  if (not FWriteHeader) then
  begin
    if (Size < Length(FContent)) then
    begin
      Size := Length(FContent);
      Result := false;
      Exit;
    end
    else
    begin
      Move(Content[1], Buffer^, Length(Content));
      Size := Length(Content);
      Result := true;
      Exit;
    end;
  end
  else
  if FUndefSize then
  begin
    Result := SaveToBufferUndefSize(Buffer, Size);
    Exit;
  end;
  Len := Length(FContent);
  if (Len <= 127) then
    HLen := 1
  else
  begin
    HLen := 1;
    while(Len > 0) do
    begin
      Len := Len shr 8;
      Inc(HLen);
    end;
  end;
  if (1 + HLen + Length(Content) > Size) then
  begin
    Size := 1 + HLen + Length(Content);
    Result := false;
    Exit;
  end
  else
  begin
    PByte(Buffer)^ := FTagId;
    if (HLen = 1) then
      PByteArray(Buffer)[1] := Length(Content)
    else
    begin
      PByteArray(Buffer)[1] := $80 + HLen - 1;
      for I := 2 to HLen do
        PByteArray(Buffer)[I] := Length(Content) shr ((HLen - I) * 8) and $FF
    end;
    Move(Content[1], PByteArray(Buffer)[HLen + 1], Length(Content));
    Size := Length(Content) + HLen + 1;
    Result := true;
  end;
end;

function TElASN1SimpleTag.SaveToBufferUndefSize(Buffer : pointer; var Size : integer) : boolean;
begin
  if (4 + Length(FContent) > Size) then
  begin
    Size := 4 + Length(FContent);
    Result := false;
  end
  else
  begin

    PByteArray(Buffer)[0] := FTagId;
    PByteArray(Buffer)[1] := $80;
    Move(FContent[1], PByteArray(Buffer)[2], Length(FContent));
    PByteArray(Buffer)[2 + Length(FContent)] := 0;
    PByteArray(Buffer)[3 + Length(FContent)] := 0;
    Result := true;
    Size := Length(FContent) + 4;
  end;
end;

function TElASN1SimpleTag.GetConstrained : boolean;
begin
  Result := false;
end;

////////////////////////////////////////////////////////////////////////////////
// TElASN1ConstrainedTag

constructor TElASN1ConstrainedTag.Create;
begin
  inherited;
  FList := TList.Create;
  FStack := TList.Create;
end;

destructor TElASN1ConstrainedTag.Destroy;
begin
  ClearList;
  FList.Free;
  FStack.Free;
  inherited;
end;

procedure TElASN1ConstrainedTag.ClearList;
var
  P : TElASN1CustomTag;
begin
  while FList.Count > 0 do
  begin
    P := TElASN1CustomTag(FList[0]);
    FList.Delete(0);
    P.Free;
  end;
end;

function TElASN1ConstrainedTag.LoadFromBuffer(Buffer : pointer; Size : integer) : boolean;
var
  ASN1Parser : TElASN1Parser;
begin
  FSingleLoad := false;
  FDataProcessed := false;
  ASN1Parser := TElASN1Parser.Create;

  ASN1Parser.OnRead := HandleASN1Read;
  ASN1Parser.OnTag := HandleASN1Tag;
  ASN1Parser.OnTagHeader := HandleASN1TagHeader;
  
  ASN1Parser.RaiseOnEOC := true;
  GetMem(FBuffer, Size);
  Move(Buffer^, FBuffer^, Size);
  FBufferSize := Size;
  FCurrBufferIndex := 0;
  Result := true;
  try
    ASN1Parser.Parse;
  except
    on E : EXception do
      Result := false;
  end;
  FreeMem(FBuffer);
  ASN1Parser.Free;
end;

function TElASN1ConstrainedTag.LoadFromBufferSingle(Buffer: pointer; Size: integer) : integer;
var
  ASN1Parser : TElASN1Parser;
begin
  FSingleLoad := true;
  FDataProcessed := false;
  ASN1Parser := TElASN1Parser.Create;
  ASN1Parser.OnRead := HandleASN1Read;
  ASN1Parser.OnTag := HandleASN1Tag;
  ASN1Parser.OnTagHeader := HandleASN1TagHeader;

  ASN1Parser.RaiseOnEOC := true;
  GetMem(FBuffer, Size);
  Move(Buffer^, FBuffer^, Size);
  FBufferSize := Size;
  FCurrBufferIndex := 0;
  try
    ASN1Parser.Parse;
    Result := FCurrBufferIndex;
  except
    Result := -1;
  end;
  FreeMem(FBuffer);
  ASN1Parser.Free;
end;

function TElASN1ConstrainedTag.SaveToBuffer(Buffer : pointer; var Size : integer) :
  boolean;
var
  I, Len, HLen, Tmp : integer;
begin
  Tmp := 0;
  if (not FWriteHeader) then
  begin
    Len := 0;
    for I := 0 to FList.Count - 1 do
    begin
      Tmp := 0;
      TElASN1CustomTag(FList[I]).SaveToBuffer(nil, Tmp);
      Len := Len + Tmp;
    end;
    if Size < Len then
    begin
      Size := Len;
      Result := false;
      Exit;
    end
    else
    begin
      Tmp := 0;
      for I := 0 to FList.Count - 1 do
      begin
        Len := Size;
        TElASN1CustomTag(FList[I]).SaveToBuffer(@PByteArray(Buffer)[Tmp], Len);
        Tmp := Tmp + Len;
      end;
      Size := tmp;
      Result := true;
      Exit;
    end;
  end;
  if (FUndefSize) then
  begin
    Result := SaveToBufferUndefSize(Buffer, Size);
    Exit;
  end;
  Len := 0;
  for I := 0 to FList.Count - 1 do
  begin
    Tmp := 0;
    TElASN1CustomTag(FList[I]).SaveToBuffer(nil, Tmp);
    Len := Len + Tmp;
  end;
  if (Len <= 127) then
    HLen := 1
  else
  begin
    Tmp := Len;
    HLen := 1;
    while(Tmp > 0) do
    begin
      Tmp := Tmp shr 8;
      Inc(HLen);
    end;
  end;
  if(Len + HLen + 1 > Size) then
  begin
    Size := Len + HLen + 1;
    Result := false;
  end
  else
  begin
    if (Len <= 127) then
      PByteArray(Buffer)[1] := Len
    else
    begin
      PByteArray(Buffer)[1] := $80 + HLen - 1;
      for I := 2 to HLen do
        PByteArray(Buffer)[I] := (Len shr ((HLen - I) * 8)) and $FF;
    end;
    PByteArray(Buffer)[0] := FTagId;

    Tmp := HLen + 1;
    for i := 0 to FList.Count - 1 do
    begin
      Len := Size;
      TElASN1CustomTag(FList[I]).SaveToBuffer(@PByteArray(Buffer)[Tmp], Len);

      Tmp := Tmp + Len;
    end;
    Size := Tmp;
    Result := true;
  end;
end;

function TElASN1ConstrainedTag.SaveToBufferUndefSize(Buffer : pointer; var Size :
  integer) : boolean;
var
  I, Len, Tmp : integer;
begin
  Len := 0;
  for I := 0 to FList.Count - 1 do
  begin
    Tmp := 0;
    TElASN1CustomTag(FList[I]).SaveToBuffer(nil, Tmp);
    Len := Len + Tmp;
  end;
  if (Len + 4 > Size) then
  begin
    Size := Len + 4;
    Result := false;
  end
  else
  begin
    PByteArray(Buffer)[1] := $80;
    PByteArray(Buffer)[0] := FTagId;
    Tmp := 2;
    for i := 0 to FList.Count - 1 do
    begin
      Len := Size;
      TElASN1CustomTag(FList[i]).SaveToBuffer(@PByteArray(Buffer)[Tmp], Len);
      Tmp := Tmp + Len;
    end;
    PByteArray(Buffer)[Tmp] := 0;
    PByteArray(Buffer)[Tmp + 1] := 0;
    Size := Tmp + 2;
    Result := true;
  end;
end;

function TElASN1ConstrainedTag.AddField(Constrained : boolean) : integer;
var
  Tag : TElASN1CustomTag;
begin
  if Constrained then
    Tag := TElASN1ConstrainedTag.Create
  else
    Tag := TElASN1SimpleTag.Create;
  Result := FList.Add(Tag);
end;

function TElASN1ConstrainedTag.RemoveField(Index : integer) : boolean;
var
  Tag : TElASN1CustomTag;
begin
  if (Index < FList.Count) and (Index >= 0) then
  begin
    Tag := TElASN1CustomTag(FList[Index]);
    FList.Delete(Index);
    Tag.Free;
    Result := true;
  end
  else
    Result := false;
end;

function TElASN1ConstrainedTag.GetField(Index : integer) : TElASN1CustomTag;
begin
  if (Index < FList.Count) and (Index >= 0) then
    Result := TElASN1CustomTag(FList[Index])
  else
    Result := nil;
end;

function TElASN1ConstrainedTag.GetCount : integer;
begin
  Result := FList.Count;
end;

procedure TElASN1ConstrainedTag.HandleASN1Read(Sender : TObject; Buffer : pointer;
  var Size : longint);
begin
  if (FSingleLoad) and (FDataProcessed) and (FSizeLeft <= 0) then
  begin
    Size := 0;
    Exit;
  end;
  if Size < FBufferSize - FCurrBufferIndex then
  begin
    Move(pointer(PtrUInt(FBuffer) + cardinal(FCurrBufferIndex))^, Buffer^, Size);
    Inc(FCurrBufferIndex, Size);
  end
  else
  begin
    Size := FBufferSize - FCurrBufferIndex;
    Move(pointer(PtrUInt(FBuffer) + cardinal(FCurrBufferIndex))^, Buffer^, Size);
    Inc(FCurrBufferIndex, Size);
  end;

  if (FSingleLoad) and (FDataProcessed) and (FSizeLeft > 0) then
    Dec(FSizeLeft, Size);

end;

procedure TElASN1ConstrainedTag.HandleASN1Tag(Sender : TObject; TagType: asn1TagType;
  TagConstrained: boolean; Tag: pointer; TagSize: integer; Size: integer; Data: pointer;
  BitRest: integer; var Valid : boolean);
var
  NewTag : TElASN1CustomTag;
  ParentTag : TElASN1ConstrainedTag;
  I : integer;
begin
  if TagType = asn1tEOC then
  begin
    if FStack.Count > 0 then
    begin
      FStack.Delete(FStack.Count - 1);
      if (FStack.Count = 0) and (FSingleLoad) then
      begin
        FDataProcessed := true;
        FSizeLeft := 0;
      end;
    end;
    Exit;
  end;
  if FStack.Count > 0 then
    ParentTag := TElASN1ConstrainedTag(FStack[FStack.Count - 1])
  else
    ParentTag := Self;
  if TagConstrained then
  begin
    NewTag := ParentTag.GetField(ParentTag.AddField(true));

    NewTag.TagId := ((integer(TagType) - integer(asn1tUniversal)) shl 6) or (integer(TagConstrained) shl 5) or PByte(Tag)^;
    NewTag.Parent := ParentTag;
    NewTag.FTagOffset := FCurrBufferIndex - FLastHeaderLen;
    NewTag.FTagSize := Size + FLastHeaderLen;
    NewTag.FTagHeaderSize := FLastHeaderLen;
    if Assigned(ParentTag) then
      Inc(NewTag.FTagOffset, ParentTag.FTagOffset + ParentTag.FTagHeaderSize);
    if (Size = 0) and (PByteArray(FBuffer)[FCurrBufferIndex - 1] = $80) then
    begin
      NewTag.FUndefSize := true;
      FStack.Add(NewTag);
    end
    else
    begin
      NewTag.LoadFromBuffer(pointer(PtrUInt(FBuffer) + cardinal(FCurrBufferIndex)), Size);
      if (FSingleLoad) and (FStack.Count = 0) then
      begin
        FDataProcessed := true;
        FSizeLeft := Size;
      end;

    end;
    Valid := false;
  end
  else
  begin
    NewTag := ParentTag.GetField(ParentTag.AddField(false));
    NewTag.Parent := ParentTag;
    NewTag.FTagOffset := FCurrBufferIndex - FLastHeaderLen - Size;
    NewTag.FTagSize := Size + FLastHeaderLen;

    if PByte(Tag)^ = asn1BitStr then
    begin
      Dec(NewTag.FTagOffset);
      Inc(NewTag.FTagSize);
    end;

    if Assigned(ParentTag) then
      Inc(NewTag.FTagOffset, ParentTag.FTagOffset + ParentTag.FTagHeaderSize);
    NewTag.TagId := ((integer(TagType) - integer(asn1tUniversal)) shl 6) or (integer(TagConstrained) shl 5) or PByte(Tag)^;
    with TElASN1SimpleTag(NewTag) do
      SetLength(FContent, Size);
    if NewTag.TagId <> SB_ASN1_INTEGER then
      Move(Data^, TElASN1SimpleTag(NewTag).FContent[1], Size)
    else
    begin
      for I := 1 to Size do
        TElASN1SimpleTag(NewTag).FContent[I] := Chr(PByteArray(Data)[Size - I]);
    end;
    if PByte(Tag)^ = asn1BitStr then
      TElASN1SimpleTag(NewTag).FContent := #0 + TElASN1SimpleTag(NewTag).FContent;
  end;
end;

procedure TElASN1ConstrainedTag.HandleASN1TagHeader(Sender: TObject; TagID : byte;
  TagLen : integer; HeaderLen : integer; UndefLen : boolean);
begin
  FLastHeaderLen := HeaderLen;
  FLastUndefLen := UndefLen;
end;

procedure TElASN1ConstrainedTag.Clear;
begin
  ClearList;
  FStack.Clear;
end;

function TElASN1ConstrainedTag.GetConstrained : boolean;
begin
  Result := true;
end;

function TElASN1ConstrainedTag.SaveContentToBuffer(Buffer : pointer;
  var Size : integer) : boolean;
var
  I, Needed : integer;
  Ptr : ^byte;
  ChunkSize : integer;
begin
  { 1. Estimating needed size }
  Needed := 0;
  Result := false;
  for I := 0 to Count - 1 do
    if GetField(I).IsConstrained then
      Exit
    else
      Inc(Needed, Length(TElASN1SimpleTag(GetField(I)).Content));
  { 2. Saving data }
  if Needed <= Size then
  begin
    Ptr := Buffer;
    for I := 0 to Count - 1 do
    begin
      ChunkSize := Length(TElASN1SimpleTag(GetField(I)).Content);
      Move(TElASN1SimpleTag(GetField(I)).Content[1], Ptr^, ChunkSize);
      Inc(Ptr, ChunkSize);
    end;
    Result := true;
  end;
  Size := Needed;
end; 

procedure asymWriteInteger(Tag : TElASN1SimpleTag; Buffer : pointer; Size : integer);
var
  S : AnsiString;
begin
  SetLength(S, Size);
  Move(Buffer^, S[1], Length(S));
  if PByte(Buffer)^ >= $80 then
    S := #0 + S;
  Tag.TagId := SB_ASN1_INTEGER;
  Tag.Content := S;
end;

function ASN1ReadInteger(Tag: TElASN1SimpleTag): integer;
var
  Cnt : BufferType;
  I, K : integer;
begin
  Cnt := Tag.Content;
  I := Length(Cnt);
  K := 0;
  Result := 0;
  while (I > 0) and (K < 5) do
  begin
    Result := Result or (PByte(@Cnt[I])^ shl (K * 8));
    Dec(I);
    Inc(K);
  end;
end;

procedure ASN1WriteInteger(Tag: TElASN1SimpleTag; Value: integer);
var
  Val : BufferType;
begin
  Tag.TagID := SB_ASN1_INTEGER;
  SetLength(Val, 4);
  PByte(@Val[1])^ := Value shr 24;
  PByte(@Val[2])^ := (Value shr 16) and $ff;
  PByte(@Val[3])^ := (Value shr 8) and $ff;
  PByte(@Val[4])^ := Value and $ff;
  while (Length(Val) > 0) and (Val[1] = #0) do
    Val := Copy(Val, 2, Length(Val) - 1);
  if Length(Val) = 0 then
    Val := #0
  else if PByte(@Val[1])^ >= $80 then
    Val := #0 + Val;
  Tag.Content := Val;
end;


end.
