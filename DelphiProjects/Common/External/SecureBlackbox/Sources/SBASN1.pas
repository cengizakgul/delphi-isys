
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

{$j+}
unit SBASN1;

interface

uses
  SysUtils,
  Classes,
  SBUtils
  ;


type
  PByte = ^byte;

const
  asn1Boolean         =  1;
  asn1Integer         =  2;
  asn1BitStr          =  3;
  asn1OctetStr        =  4;
  asn1NULL            =  5;
  asn1Object          =  6;
  asn1Real            =  9;
  asn1Enumerated      = 10;
  asn1UTF8String      = 12;

  asn1Sequence        = 16;
  asn1Set             = 17;
  asn1NumericStr      = 18;
  asn1PrintableStr    = 19;
  asn1T61String       = 20;
  asn1TeletexStr      = asn1T61String;
  asn1IA5String       = 22;
  asn1UTCTime         = 23;
  asn1GeneralizedTime = 24;
  asn1VisibleStr      = 26;
  asn1GeneralStr      = 27;

  asn1A0                = 0;
  asn1A1                = 1;
  asn1A2                = 2;
  asn1A3                = 3;
  asn1A4                = 4;
  asn1A5                = 5;
  asn1A6                = 6;
  asn1A7                = 7;
  asn1A8                = 8;


type
  EASN1Error =  class(Exception);
  EASN1ReadError =  class(Exception);

  asn1TagType = (asn1tUniversal, asn1tApplication, asn1tSpecific, asn1tPrivate, asn1tEOC);
  
// + stream read function type, when decoder needs to read stream, it calls this function
// Stream - some pointer (or casted to pointer type) data than identifies real data stream
// Data   - pointer to buffer to store data read from stream to
// Size   - size of data wanted
  asn1tReadFunc         = function (Stream: pointer; Data: pointer; Size: integer): integer; stdcall;

// + stream write function type, when encoder needs to write stream, it calls this function
// Stream - some pointer (or casted to pointer type) data than identifies real data stream
// Data   - pointer to buffer to load data written from stream from
// Size   - size of data writing
  asn1tWriteFunc        = procedure (Stream: pointer; Data: pointer; Size: integer); stdcall;

// + callback function type of tag processing function when incoming stream being parsed
// + it shall be called for each data field of incoming stream
// Stream         - some data to give callback function the possibility to differ data treating
//                  depending on stream when incoming stream being parsed
// TagType        - type of read tag
// TagConstrained - if incoming field is constrained i.e. incapsulates some other fields
//                  if it's yes callback won't get any real data
// Tag            - buffer containing tag value
// SizeTag        - size of tag
// Size           - size of incoming data
// Data           - buffer containing data value
// BitRest        - number of significant bits in last data byte (from left to right)
//                  meaningful only for BIT STRING fields, if 0, whole last byte is significant
  asn1tCallBackFunc     = function (Stream: pointer; TagType: asn1TagType; TagConstrained: boolean;
                        Tag: pointer; TagSize: integer; Size: integer; Data: pointer;
                        BitRest: integer): boolean of object; stdcall;

var
// if it needs to revert bytes in tag itself when writing to stream
  asn1RevertTagBytes: boolean = true;
// if  it needs to revert bytes in integers when reading from stream
  asn1RevertReadInts: boolean = true;
// actual stream read function
///  asn1ReadFunc:       asn1tReadFunc;
// actual stream write function
  asn1WriteFunc:      asn1tWriteFunc;

// --- general functions
//  -  generic function to add tag to stream (see later)
//  -  all parameters have same meaning as in callback function type definition
  function asn1AddTag(Stream: pointer; TagType: asn1TagType; TagConstrained: boolean; Tag: pointer;
    TagSize: integer; Size: integer; Data: pointer = nil; Revert: boolean = false;
    BitRest: integer = 0): boolean; overload;
//  -  incoming stream parsing function
  procedure asn1ParseStream(Stream: pointer; CallBack: asn1tCallBackFunc);

// --- helper for use w/ simple (1 byte long) tags
  function asn1AddTag(Stream: pointer; TagType: asn1TagType; TagConstrained: boolean; Tag: byte;
    Size: integer; Data: pointer = nil; Revert: boolean = false; BitRest: integer = 0): boolean;
    overload;

// --- helpers to use w/ most used data types
//  -  when writing to stream an integer value standard claims to use minimum of bytes need to
//  -  represent particular value so storing of integer means special treating
  function asn1AddInt(Stream: pointer; Value: integer; Revert: boolean = true): boolean;

  function asn1AddBool(Stream: pointer; Value: boolean): boolean;

  function asn1AddStr(Stream: pointer; Value: AnsiString): boolean;

  function asn1AddBuf(Stream: pointer; Value: pointer; Size: integer): boolean;

  function asn1AddSeq(Stream: pointer): boolean;

  function asn1AddSet(Stream: pointer): boolean;


// --- helpers to write constrained fields
//  -  !!! there no builtin constrains nest checking
//  -  !!! use AddConstrained/CloseConstrained calls carefully
  function asn1AddConstrained(Stream: pointer; TagType: asn1TagType; Tag: pointer; TagSize: integer;
    Size: integer = 0): boolean; overload;
  function asn1AddConstrained(Stream: pointer; TagType: asn1TagType; Tag: byte;
    Size: integer = 0): boolean; overload;
//  - !!! use CloseConstrained ONLY if you previously used AddConstrained w/ Size == 0
//  - !!! (i.e. implicit size encoding)
//  -  if you use asn1AddConstrained function w/ Size == 0, it means that implicit length encoding
//  -  is used
  procedure asn1CloseConstrained(Stream: pointer);

// --- you can specify type equivalency (like Type1 ::= [TagType1 Tag1] IMPLICIT Type2)
  procedure asn1AddTypeEqu(TagType1: asn1TagType; Tag1: pointer; TagSize1: integer;
    TagType2: asn1TagType; Tag2: pointer; TagSize2: integer); overload;
  // -- defaults: TagType2 = asn1tUniversal, TagSize2 = 1
  procedure asn1AddTypeEqu(TagType1: asn1TagType; Tag1: pointer; TagSize1: integer;
    Tag2: byte); overload;

type
  TSBASN1ReadEvent = procedure(Sender : TObject; Buffer : pointer; var Size : longint) of object;
  TSBASN1TagEvent = procedure(Sender : TObject; TagType: asn1TagType; TagConstrained: boolean;
    Tag: pointer; TagSize: integer; Size: integer; Data: pointer; BitRest: integer;
    var Valid : boolean) of object;
  TSBASN1TagHeaderEvent = procedure(Sender: TObject; TagID : byte; TagLen : integer;
    HeaderLen : integer; UndefLen : boolean) of object;
  TSBASN1SkipEvent = procedure(Sender: TObject; var Count : integer) of object;

  TElASN1Parser = class(TObject)
  private
    FOnRead : TSBASN1ReadEvent;
    FOnTag : TSBASN1TagEvent;
    FOnTagHeader : TSBASN1TagHeaderEvent;
    FOnSkip : TSBASN1SkipEvent;
    FReadSize : integer;
    FRaiseOnEOC : boolean;
    FMaxSimpleTagLength : integer;
  protected
    procedure asn1Read(Data: pointer; Size: integer);
    procedure ReadRevertBytes(Data: pointer; Size: integer);
    procedure ReadRepackedBits(Tag: pointer; var TagSize: integer;
      MaxTagSize: integer; Revert: boolean = true);
    procedure DoRead(Buffer : pointer; var Size : longint);
    procedure DoTag(TagType: asn1TagType; TagConstrained: boolean; Tag: pointer;
      TagSize: integer; Size: integer; Data: pointer; BitRest: integer; var Valid : boolean);
    procedure asn1Skip(var Count: integer);
    function DecodeField(InvokeCallBack: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}): integer; overload;

  public
    constructor Create;
    destructor Destroy; override;
    procedure Parse;
    property RaiseOnEOC : boolean read FRaiseOnEOC write FRaiseOnEOC default false;
    property MaxSimpleTagLength : integer read FMaxSimpleTagLength write FMaxSimpleTagLength;
    property OnRead : TSBASN1ReadEvent read FOnRead write FOnRead;
    property OnTag : TSBASN1TagEvent read FOnTag write FOnTag;
    property OnTagHeader : TSBASN1TagHeaderEvent read FOnTagHeader write FOnTagHeader;
    property OnSkip : TSBASN1SkipEvent read FOnSkip write FOnSkip;
  end;

function WriteSequence(const Strings: TStringList): BufferType; overload;
function WriteSet(const Strings: TStringList) :  BufferType; 
function WriteA0(const Strings : TStringList):  BufferType; 
function WriteExplicit(const Data: BufferType):  BufferType; 
function WriteInteger(const Data: BufferType; TagID : byte = $02):  BufferType;overload;function WriteInteger(Number : integer; TagID : byte = $02) :  BufferType;overload;function WriteOID(const Data: BufferType):  BufferType; 
function WritePrintableString(const Data: string): BufferType; overload;function WriteUTF8String(const Data: string): BufferType; overload;function WriteIA5String(const Data: string) : BufferType; overload;function WriteUTCTime(const Data: string) : BufferType; 
function WriteGeneralizedTime(T : TDateTime) : BufferType; 
function WriteSize(Size: LongWord):  BufferType; 
function WriteBitString(const Data: BufferType):  BufferType; 
function WriteOctetString(const Data : string) : BufferType; overload;function WriteVisibleString(const Data : string) : BufferType; 
function WriteBoolean(Data : boolean) :  BufferType; 
function WriteNULL: BufferType; 

function WritePrimitive(Tag: Byte; const Data: BufferType):  BufferType; 
function WriteStringPrimitive(Tag: Byte; const Data: string) : BufferType; 

implementation

// --- TYPE LIST ---

type
  TTypeEqu = class
  public
    TagType1: asn1TagType;
    TagType2: asn1TagType;
    Tag1: pointer; // this type is
    Tag2: pointer; // equivalent to this
    TagSize1: integer;
    TagSize2: integer;
    constructor Create(aTagType1: asn1TagType; aTag1: pointer; aTagSize1: integer;
      aTagType2: asn1TagType; aTag2: pointer; aTagSize2: integer);
    destructor Destroy; override;
  end;

constructor TTypeEqu.Create(aTagType1: asn1TagType; aTag1: pointer; aTagSize1: integer;
  aTagType2: asn1TagType; aTag2: pointer; aTagSize2: integer);
begin
  TagType1 := aTagType1;
  TagType2 := aTagType2;
  TagSize1 := aTagSize1;
  TagSize2 := aTagSize2;
  GetMem(Tag1, TagSize1);
  Move(aTag1^, Tag1^, TagSize1);
  GetMem(Tag2, TagSize2);
  Move(aTag2^, Tag2^, TagSize2);
end;

destructor TTypeEqu.Destroy;
begin
  if Assigned(Tag1) then
    FreeMem(Tag1);
  if Assigned(Tag2) then
    FreeMem(Tag2);
  inherited;
end;

var
  EquList : TList;


constructor TElASN1Parser.Create;
begin
  FReadSize := 0;
  FRaiseOnEOC := false;
  FMaxSimpleTagLength := 0;
end;

destructor TElASN1Parser.Destroy;
begin

end;

procedure TElASN1Parser.Parse;
begin
  while DecodeField(true) >= 0 do;
end;

procedure TElASN1Parser.DoRead(Buffer : pointer; var Size : longint);
begin
  if Assigned(FOnRead) then
    FOnRead(Self, Buffer, Size);
end;

procedure TElASN1Parser.DoTag(TagType: asn1TagType; TagConstrained: boolean; Tag: pointer;
  TagSize: integer; Size: integer; Data: pointer; BitRest: integer; var Valid : boolean);
begin
  if Assigned(FOnTag) then
    FOnTag(Self, TagType, TagConstrained, Tag, TagSize, Size, Data,
      BitRest, Valid);
end;

procedure asn1AddTypeEqu(TagType1: asn1TagType; Tag1: pointer; TagSize1: integer;
  TagType2: asn1TagType; Tag2: pointer; TagSize2: integer); 
var
  Equ: TTypeEqu;
begin
  Equ := TTypeEqu.Create(TagType1, Tag1, TagSize1, TagType2, Tag2, TagSize2);
  EquList.Add(Equ);
end;

procedure asn1AddTypeEqu(TagType1: asn1TagType; Tag1: pointer; TagSize1: integer;
  Tag2: byte); 
begin
  asn1AddTypeEqu(TagType1, Tag1, TagSize1, asn1tUniversal, @Tag2, sizeof(Tag2));
end;

function CompareTypes(TagType1: asn1TagType; Tag1: pointer; TagSize1: integer;
  TagType2: asn1TagType; Tag2: pointer; TagSize2: integer): boolean;
var
  t1, t2: PByte;
  i: integer;
begin
  result := false;
  if (TagType1 <> TagType2) or (TagSize1 <> TagSize2) then
    exit;
  t1 := PByte(Tag1);
  t2 := PByte(Tag2);
  for i := 1 to TagSize1 do
  begin
    if t1^ <> t2^ then
      exit;
    inc(t1);
    inc(t2);
  end;
  result := true;
end;

function IsUniType(TagType: asn1TagType; Tag: pointer; TagSize: integer; Uni: byte): boolean;
var
  i: integer;
  e: TTypeEqu;
  tType: asn1TagType;
  t: pointer;
  tSize: integer;
begin
  i := 1;
  tType := TagType;
  t := Tag;
  tSize := TagSize;


  while i <= EquList.Count do
  begin
    e := TTypeEqu(EquList[i - 1]);
    if CompareTypes(tType, t, tSize, e.TagType1, e.Tag1, e.TagSize1) then
    begin
      tType := e.TagType2;
      t := e.Tag2;
      tSize := e.TagSize2;
      i := 0;
    end;
    inc(i);
  end;
  result := (tType = asn1tUniversal) and (tSize = 1) and (PByte(t)^ = Uni);
end;

// --- ENCODER ---

//type
//  PByte = ^byte;

function noOfBits(Bits: Integer): Integer;
begin
  result := 0;
  while Bits > 0 do
  begin
    Bits := Bits shr 1;
    inc(result);
  end;
end;

procedure WriteRevertBytes(Stream: pointer; Data: pointer; Size: integer);
var
  pData: PByte;
begin
  pData := PByte(PtrInt(Data) + Size - 1);
  while Size > 0 do
  begin
    asn1WriteFunc(Stream, pData, 1);
    dec(pData);
    dec(Size);
  end;
end;

procedure WriteRepackedBits(Stream: pointer; Data: pointer; Size: integer; Revert: boolean = false;
  PackedBytes: boolean = false);
var
  lData, lBits, lBytes, lRest, lFirst: integer;
  pData: PByte;
begin
  if Revert then
    pData := PByte(PtrInt(Data) + Size - 1)
  else
    pData := PByte(Data);
  if PackedBytes then
    while (Size > 1) and (pData^ = 0) do
    begin
      dec(Size);
      if Revert then
        dec(pData)
      else
        inc(pData);
    end;
  lRest := noOfBits(pData^);
  lBits := lRest + (Size - 1) * 8;
  lBytes := (lBits + 6) div 7;
  if lRest = 0 then
    inc(lRest, 7);
  lData := $80;
  lFirst := lBits mod 7;
  if lFirst > 0 then
  begin
    if lFirst > lRest then
    begin
      lData := lData or ((pData^ and not ($ff shl lRest)) shl (lFirst - lRest));
      if Revert then
        dec(pData)
      else
        inc(pData);
      lRest := 8 - lFirst + lRest;
      lData := lData or (pData^ shr lRest);
    end
    else
    begin
      lData := lData or ((pData^ shr (lRest - lFirst)) and not ($ff shl lFirst));
      lRest := lRest - lFirst;
    end;

    asn1WriteFunc(Stream, @lData, 1);
    dec(lBytes);
  end;
  if lRest = 0 then
    inc(lRest, 7);
  while lBytes > 0 do
  begin
    if lBytes > 1 then
      lData := $80
    else
      lData := 0;
    if lRest <= 7 then
    begin
      lData := lData or ((pData^ and not ($ff shl lRest)) shl (7 - lRest));
      if Revert then
        dec(pData)
      else
        inc(pData);
      lData := lData or (pData^ shr (lRest + 1));
      lRest := lRest + 1;
    end
    else
    begin
      lData := lData or ((pData^ and $fe) shr 1);
      lRest := lRest - 7;
    end;
    asn1WriteFunc(Stream, @lData, 1);
    dec(lBytes);
  end;
end;

function asn1AddTag(Stream: pointer; TagType: asn1TagType; TagConstrained: boolean; Tag: pointer;
  TagSize: integer; Size: integer; Data: pointer = nil; Revert: boolean = false;
  BitRest: integer = 0): boolean;
var
  lTag, lLen, Prev: Byte;
  pData: PByte;
  pSize: integer;
begin
  result := false;
  if TagSize < 1 then
    exit;
  lTag := ((integer(TagType) - integer(asn1tUniversal)) shl 6) or (integer(TagConstrained) shl 5);

// --- tag encoding
  // -- checking if real tag is less than 30 - maximum that may be encoded in one byte
  if (TagSize = 1) and (PByte(Tag)^ < 30) then
  begin
    lTag := lTag or PByte(Tag)^;
    asn1WriteFunc(Stream, @lTag, 1);
  end
  else
  begin
    // -- no, must build multibyte tag
    if PByte(Tag)^ = 0 then exit;
    lTag := lTag or $1f;
    asn1WriteFunc(Stream, @lTag, 1);
    WriteRepackedBits(Stream, Tag, TagSize, asn1RevertTagBytes, true);
  end;

// --- data/size correction
  pSize := Size;
  if (Size > 0) and (not TagConstrained) then
  begin
    if Revert then
      pData := PByte(PtrInt(Data) + Size - 1)
    else
      pData := PByte(Data);
    if IsUniType(TagType, Tag, TagSize, asn1BitStr) then
      inc(pSize);
    if IsUniType(TagType, Tag, TagSize, asn1Integer) then
    begin
      while pSize > 1 do
      begin
        if Revert then
          Prev := PByte(PtrInt(pData) - 1)^
        else
          Prev := PByte(PtrInt(pData) + 1)^;
        if ((pData^ = 0) and ((Prev and $80) = 0)) or
          ((pData^ = $ff) and ((Prev and $80) = $80)) then
        begin
          dec(pSize);
          if Revert then
            dec(pData)
          else
            inc(pData);
        end
        else
          break;
      end;
    end;
{    case lTag and $df of
      asn1BitStr:
        inc(pSize);
      asn1Integer:
        while pSize > 1 do
        begin
          if Revert then
            Prev := PByte(PtrInt(pData) - 1)^
          else
            Prev := PByte(PtrInt(pData) + 1)^;
          if ((pData^ = 0) and ((Prev and $80) = 0)) or
            ((pData^ = $ff) and ((Prev and $80) = $80)) then
          begin
            dec(pSize);
            if Revert then
              dec(pData)
            else
              inc(pData);
          end
          else
            break;
        end;
    end;}
  end;

// --- length encoding
  if pSize <= 0 then
  begin
    if TagConstrained then
      lLen := $80
    else
      lLen := 0;
    asn1WriteFunc(Stream, @lLen, 1);
  end
  else
  if pSize <= $7f then
  begin
    lLen := pSize;
    asn1WriteFunc(Stream, @lLen, 1);
  end
  else
  begin
    lLen := $80 + (noOfBits(pSize) + 7) div 8;
    // -- can't use reserved value lLen == $ff
    if lLen = $ff then
      raise EASN1Error.Create('Data size too large');
    asn1WriteFunc(Stream, @lLen, 1);
    WriteRevertBytes(Stream, @pSize, lLen and $7f);
  end;

// --- data storing
  if (Size > 0) and (not TagConstrained) then
  begin
    if IsUniType(TagType, Tag, TagSize, asn1BitStr) then
    begin
      asn1WriteFunc(Stream, @BitRest, 1);
      pSize := Size;
    end;
{    case lTag and $df of
      asn1BitStr:
        begin
          asn1WriteFunc(Stream, @BitRest, 1);
          pSize := Size;
        end;
    end;}
    if Revert then
      WriteRevertBytes(Stream, Data, pSize)
    else
      asn1WriteFunc(Stream, Data, pSize {$ifndef HAS_DEF_PARAMS}, 0{$endif});
  end;

  result := true;
end;

function asn1AddTag(Stream: pointer; TagType: asn1TagType; TagConstrained: boolean; Tag: byte;
  Size: integer; Data: pointer = nil; Revert: boolean = false; BitRest: integer = 0): boolean;
begin
  result := asn1AddTag(Stream, TagType, TagConstrained, @Tag, sizeof(Tag), Size, Data, Revert, BitRest);
end;

function asn1AddBool(Stream: pointer; Value: boolean): boolean;
begin
  result := asn1AddTag(Stream, asn1tUniversal, false, asn1Boolean, sizeof(Value), @Value);
end;

function asn1AddInt(Stream: pointer; Value: integer; Revert: boolean = true): boolean;
begin
  result := asn1AddTag(Stream, asn1tUniversal, false, asn1Integer, sizeof(Value), @Value, Revert);
end;

function asn1AddStr(Stream: pointer; Value: AnsiString): boolean;
begin
  result := asn1AddTag(Stream, asn1tUniversal, false, asn1VisibleStr, Length(Value), @Value[1]);
end;

function asn1AddBuf(Stream: pointer; Value: pointer; Size: integer): boolean;
begin
  result := asn1AddTag(Stream, asn1tUniversal, false, asn1OctetStr, Size, Value, false, 0);
end;

// ---

function asn1AddConstrained(Stream: pointer; TagType: asn1TagType; Tag: pointer; TagSize: integer; Size: integer): boolean;
begin
  result := asn1AddTag(Stream, TagType, true, Tag, TagSize, Size);
end;

function asn1AddConstrained(Stream: pointer; TagType: asn1TagType; Tag: byte; Size: integer): boolean;
begin
  result := asn1AddConstrained(Stream, TagType, @Tag, sizeof(Tag), Size);
end;

procedure asn1CloseConstrained(Stream: pointer);
const
  FinalSignature: word = 0;
begin
  asn1WriteFunc(Stream, @FinalSignature, sizeof(FinalSignature));
end;

function asn1AddSeq(Stream: pointer): boolean;
begin
  result := asn1AddConstrained(Stream, asn1tUniversal, asn1Sequence {$ifndef HAS_DEF_PARAMS}, 0{$endif});
end;

function asn1AddSet(Stream: pointer): boolean;
begin
  result := asn1AddConstrained(Stream, asn1tUniversal, asn1Set {$ifndef HAS_DEF_PARAMS}, 0{$endif});
end;

// --- DECODER ---

type
  EASN1StreamEnd = class(Exception);
  EASN1Exit = class(Exception);

const
  asn1MaxTagSize = 100;

procedure TElASN1Parser.asn1Read(Data: pointer; Size: integer);
var
  r: integer;
begin
  r := Size;
  DoRead(Data, r);
//  r := asn1ReadFunc(Data, Size);
  if r <> Size then
    raise EASN1ReadError.Create(
      'Invalid ASN1 sequence');
  inc(FReadSize, r);
end;

procedure TElASN1Parser.asn1Skip(var Count: integer);
var
  R : integer;
begin
  R := Count;
  if Assigned(FOnSkip) then
    FOnSkip(Self, Count);
  if R <> Count then
    raise EASN1ReadError.Create(
      'Invalid ASN1 sequence');
  inc(FReadSize, R);
end;

procedure TElASN1Parser.ReadRevertBytes(Data: pointer; Size: integer);
var
  pData: PByte;
begin
  pData := PByte(PtrInt(Data) + Size - 1);
  while Size > 0 do
  begin
    asn1Read(pData, 1);
    dec(Size);
    dec(pData);
  end;
end;

procedure TElASN1Parser.ReadRepackedBits(Tag: pointer; var TagSize: integer;
  MaxTagSize: integer; Revert: boolean = true);
var
  lTag: PByte;
  lfTag: PByte;
  pTag: PByte;
  zTag: PByte;
  BitsLeft: byte;
  b: byte;
  x: integer;
  InTags: integer;
begin
  zTag := nil;
  inTags := 1;
  GetMem(lTag, MaxTagSize);
  lfTag := lTag;
  BitsLeft := 8;
  repeat
    asn1Read(@b, sizeof(b));
    if BitsLeft < 7 then
    begin
      lfTag^ := lfTag^ or ((b and $7f) shr (7 - BitsLeft));
      inc(InTags);
      inc(lfTag);
      BitsLeft := BitsLeft + 1;
    end
    else
      BitsLeft := BitsLeft - 7;
    lfTag^ := ((b and $7f) shl BitsLeft);
  until (b and $80) = 0;
  if not Revert then
  begin
    GetMem(pTag, MaxTagSize);
    zTag := pTag;
  end
  else
    pTag := PByte(Tag);
  TagSize := 1;
  for x := 1 to InTags do
  begin
    if TagSize > MaxTagSize then
      raise EASN1Error.Create(
        'Tag size too large');
    pTag^ := lfTag^ shr BitsLeft;
    if x < InTags then
    begin
      dec(lfTag);
      pTag^ := pTag^ or (lfTag^ shl (8 - BitsLeft));
    end;
    inc(TagSize);
    inc(pTag);
  end;
  while pTag^ = 0 do
  begin
    dec(pTag);
    dec(TagSize);
  end;
  if not Revert then
    Move(zTag^, Tag^, TagSize);
  FreeMem(lTag, MaxTagSize);
end;

function TElASN1Parser.DecodeField(InvokeCallBack: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}): integer;

// -1 - end of stream
// 0 - end of constrained
// more - field size
var
  llTag: byte;
  llLen: byte;
  lRevert: boolean;
  lSubSize: integer;
  lSubResult: integer;

  lTagBufLarge : array[0..asn1MaxTagSize - 1] of byte;

  lTagConstrained: boolean;
  lTagType: asn1TagType;
  lTagBufSmall : Byte;
  lTag: pointer;
  lTagSize: integer;
  lSize: integer;
  lData: pointer;
  lBitRest: byte;
  lInvokeCallBack: boolean;
  Len : integer;
  lTagID : byte;
  lUndefSize: boolean;
  lHeaderSize: integer;
//  boc : boolean;
begin
  result := -1;
  //boc := false;

// --- start field initialization
  lRevert := false;
  lInvokeCallBack := InvokeCallBack;
///  lTagConstrained := false;
///  lTagType := asn1tUniversal;
///  lTag := nil;
  lTagSize := 0;
{$hints off}
  lSize := 0;
{$hints on}
  lData := nil;
  lBitRest := 0;

// --- stream read start
  FReadSize := 1;

// -- decoding tag
  Len := SizeOf(llTag);
  DoRead(@llTag, Len);
  if Len <> sizeof(llTag) then
// -- probably we've reached end of the stream
    exit;
  if llTag = 0 then
  begin
    asn1Read(@llLen, sizeof(llLen));
    if llLen <> 0 then
      raise EASN1Error.Create('Invalid ASN1 Sequence');
//    if lInvokeCallBack then
//      lInvokeCallBack := CallBack(Stream, asn1TagType(0), false, nil, 0, 0, nil, 0);

    // II 220703 OnTagEnd
    if FRaiseOnEOC then
      DoTag(asn1tEOC, false, @llTag, 1, 0, nil, 0, lInvokeCallBack);

    result := 2; // 0  !!
    exit;
  end;
  lTagID := llTag;
  lTagConstrained := (llTag and $20) <> 0;
  lTagType := asn1TagType((llTag shr 6) + integer(asn1tUniversal));
  llTag := llTag and $1f;
  lHeaderSize := 1;
  if llTag < $1f then
  begin
//    GetMem(lTag, 1);
    lTag := @lTagBufSmall;
    lTagSize := 1;
    PByte(lTag)^ := llTag;
  end
  else
  begin
//    GetMem(lTag, asn1MaxTagSize);
    lTag := @lTagBufLarge;
    ReadRepackedBits(lTag, lTagSize, asn1MaxTagSize, asn1RevertTagBytes);
  end;

// -- decoding size
  lUndefSize := false;
  asn1Read(@llLen, sizeof(llLen));
  if llLen > $80 then
  begin
    llLen := llLen and $7f;      
    if llLen > sizeof(lSize) then
      raise EASN1Error.Create('Block size too large');
    ReadRevertBytes(@lSize, llLen);
    Inc(lHeaderSize, 1 + llLen);
  end
  else
  if llLen = $80 then
  begin
    if not lTagConstrained then
      raise EASN1Error.Create('Invalid ASN1 sequence');
    // II 220703
//    boc := true;
    lSize := 0;
    lUndefSize := true;
    Inc(lHeaderSize);
  end
  else
  begin
    lSize := llLen;
    Inc(lHeaderSize);
  end;
  if IsUniType(lTagType, lTag, lTagSize, asn1BitStr) then
    if not lTagConstrained then
    begin
      dec(lSize);
      asn1Read(@lBitRest, sizeof(lBitRest));
    end;
{  if lTagSize = 1 then
    case PByte(lTag)^ of
      asn1BitStr:
      if not lTagConstrained then
      begin
        dec(lSize);
        asn1Read(Stream, @lBitRest, sizeof(lBitRest));
      end;
    end;}

  if Assigned(FOnTagHeader) then
    FOnTagHeader(Self, lTagID, lSize, lHeaderSize, lUndefSize);

// -- reading data itself
  if IsUniType(lTagType, lTag, lTagSize, asn1Integer) then
    if asn1RevertReadInts then
      lRevert := true;
{  if lTagSize = 1 then
    case PByte(lTag)^ of
      asn1Integer:
        lRevert := true;
    end;}
  if (lSize > 0) and (not lTagConstrained) then
  begin
    // skipping non-integer tags larger than MaxSimpleTagLength
    if (FMaxSimpleTagLength = 0) or (lSize <= FMaxSimpleTagLength) or
      (PByte(lTag)^ = asn1Integer) then
    begin
      GetMem(lData, lSize);
      if lRevert then
        ReadRevertBytes(lData, lSize)
      else
        asn1Read(lData, lSize);
    end
    else
    begin
      lData := nil;
      asn1Skip(lSize);
    end;
  end;

  if lInvokeCallBack then
  begin
//    lInvokeCallBack := CallBack(Stream, lTagType, lTagConstrained, lTag, lTagSize, lSize, lData, lBitRest);
    DoTag(lTagType, lTagConstrained, lTag, lTagSize, lSize, lData, lBitRest, lInvokeCallBack);
  end;

// -- fetching subfields
  result := FReadSize;

  lSubSize := 0;
  lSubResult := -1;
  if lTagConstrained and (lSize > 0) then // ltagConstrained
  begin
    repeat
      if lSize > 0 then
        if lSubSize = lSize then
          break
        else
          if lSubSize > lSize then
            raise EASN1Error.Create('Invalid stream data');
      lSubResult := DecodeField(lInvokeCallBack);
      if (lSubResult < 0) and (lSubSize <> lSize) then
        raise EASN1Error.Create('Unexpected stream end');
      if lSubResult > 0 then
        inc(lSubSize, lSubResult);
    until (lSubResult = 0) and (lSize <= 0);
    if lInvokeCallBack {and (lSize > 0)} then
      DoTag(asn1TagType(0), false, nil, 0, 0, nil, 0, lInvokeCallBack);
  end;
  result := result + lSubSize;
  if lData <> nil then
    FreeMem(lData);
end;

procedure asn1ParseStream(Stream: pointer; CallBack: asn1tCallBackFunc);
begin
//  while DecodeField(Stream, CallBack) >= 0 do;
end;

procedure FinalizeAll;
var
  i: integer;
begin
  for i := 1 to EquList.Count do
    TTypeEqu(EquList[i - 1]).Free;
  EquList.Free;
end;

// ASN1 write routines

function WritePrimitiveSeq(Tag : Byte; Strings : TStringList): BufferType;
var
  I: integer;
  TotalSize: Word;
begin
  Result  := Char(Tag);
  TotalSize := 0;
  for I := 0 to Strings.Count - 1 do
    TotalSize := TotalSize + Length(Strings.Strings[I]);
  Result := Result + WriteSize(TotalSize);
  for I := 0 to Strings.Count - 1 do
    Result  := Result + Strings.Strings[I];
end;

function WriteA0(const Strings : TStringList): BufferType;
begin
  result := WritePrimitiveSeq($A0, Strings);
end;

function WriteSequence(const Strings: TStringList): BufferType;
begin
  result := WritePrimitiveSeq($30, Strings);
end;


function WriteSet(const Strings: TStringList ):  BufferType;
begin
  result := WritePrimitiveSeq($31, Strings);
end;

function WriteStringPrimitive(Tag: Byte; const Data: string) : BufferType;
begin
  result := WritePrimitive(Tag, Data);
end;

function WritePrimitive(Tag: Byte; const Data: BufferType):  BufferType;
begin
  Result := Char(Tag) + WriteSize(Length(Data)) + Data;
end;

function WriteExplicit(const Data: BufferType) : BufferType;
begin
  result := WritePrimitive($A0, Data);
end;

function WriteInteger(const Data: BufferType; TagID : byte = $02): BufferType;
var
  Tmp : BufferType;
  Index : integer;
const
  ZEROCHAR = #0;
  NEGFLAG = #$80;
begin
  // removing leading zeros
  Index := BufferTypeStartOffset;
  while (Index <= Length(Data) - BufferTypeStartInvOffset) and (Data[Index] = ZEROCHAR) do
    Inc(Index);
  if Index > Length(Data) - BufferTypeStartInvOffset then
  begin
    // the value to write is 0
    SetLength(Tmp, 1);
    Tmp[BufferTypeStartOffset] := ZEROCHAR;
  end
  else
  begin
    // appending leading 0 for values starting with 0x80+ values
    if Data[Index] >= NEGFLAG then
    begin
      SetLength(Tmp, Length(Data) - Index + BufferTypeStartOffset + 1);
      Tmp[BufferTypeStartOffset] := ZEROCHAR;
      Move(Data[Index], Tmp[2], Length(Data) - Index + 1);
    end
    else
    begin
      SetLength(Tmp, Length(Data) - Index + BufferTypeStartOffset);
      Move(Data[Index], Tmp[1], Length(Data) - Index + 1);
    end;
  end;
  result := WritePrimitive(TagID, Tmp);
end;

function WriteInteger(Number : integer; TagID : byte = $02) : BufferType;
var TmpBuf : AnsiString;
begin
  if Number = 0 then
    TmpBuf := WriteInteger(#0, TagID)
  else
  begin
    SetLength(TmpBuf, 0);
    while Number <> 0 do
    begin
      TmpBuf := Chr(Number and $FF) + TmpBuf;
      Number := Number shr 8;
    end;
    TmpBuf := WriteInteger(TmpBuf, TagID);
  end;
  Result := TmpBuf;
end;

function WriteOID(const Data: BufferType): BufferType;
begin
  result := WritePrimitive($06, Data);
end;

function WritePrintableString(const Data: string): BufferType;
begin
  result := WriteStringPrimitive($13, Data);
end;

function WriteUTF8String(const Data: string): BufferType;
begin
  result := WriteStringPrimitive(12, Data);
end;


function WriteIA5String(const Data: string) : BufferType;
begin
  result := WriteStringPrimitive(22, Data);
end; 


function WriteUTCTime(const Data: string): BufferType;
begin
  result := WriteStringPrimitive($17, Data);
end;

function WriteGeneralizedTime(T : TDateTime) : BufferType;
var
  Year, Day, Month, Hour, Min, Sec, MSec : word;
  YearStr, DayStr, MonStr, HourStr, MinStr, SecStr : string;

  function IntToStr2(Value : word) : String;
  begin
    Result := IntToStr((Value div 10) mod 10) + IntToStr(Value mod 10);
  end;

begin
  DecodeDate(T, Year, Month, Day);
  DecodeTime(T, Hour, Min, Sec, MSec);
  try
    YearStr := IntToStr(Year);
  except
    YearStr := '2000';
  end;
  while Length(YearStr) < 4 do
    YearStr := '0' + YearStr;
  DayStr := IntToStr2(Day);
  MonStr := IntToStr2(Month);
  HourStr := IntToStr2(Hour);
  MinStr := IntToStr2(Min);
  SecStr := IntToStr2(Sec);
  Result := YearStr + MonStr + DayStr + HourStr + MinStr + SecStr + 'Z';
  Result := #$18 + WriteSize(Length(Result)) + Result;
end;

function WriteSize(Size: LongWord): BufferType;
begin
  if Size < 128 then
    Result := Chr(Size)
  else
  if (Size >= 128) and (Size < 256) then
    Result := #$81 + Chr(Size)
  else
    Result := #$82 + Chr(Size shr 8) + Chr(Size and $FF);
end;

function WriteBitString(const Data: BufferType): BufferType;
begin
  Result := #$03 + WriteSize(Length(Data) + 1) + #$0 + Data;
end;

function WriteNULL: BufferType;
begin
  Result := #$05#$00;
end;

function WriteOctetString(const Data : string) : BufferType;
begin
  Result := #$04 + WriteSize(Length(Data)) + Data;
end;


function WriteBoolean(Data : boolean) : BufferType;
begin
  if Data then
    Result := #$01#$01#$FF
  else
    Result := #$01#$01#$00;
end;

function WriteVisibleString(const Data : string) : BufferType;
begin
  Result := #26 + WriteSize(Length(Data) + 1) + Data + ' ';
end;


initialization
  EquList := TList.Create;

finalization
  FinalizeAll;

  
end.
