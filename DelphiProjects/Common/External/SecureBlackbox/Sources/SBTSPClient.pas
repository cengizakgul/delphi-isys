(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBTSPClient;

interface

uses

  Classes,
  SysUtils,
  

  SBUtils,
  SBConstants,
  SBASN1,
  SBASN1Tree,
//  SBX509,
  SBX509Ext,
//  SBSHA,
  SBPKCS7,
  SBPKICommon,
  SBTSPCommon,
  SBCustomCertStorage;

type

  TElClientTSPInfo = class(TElTSPInfo)
  protected
    FOwner : TObject;
    FVerifier: TObject;//TElMessageVerifier;
    FMessageImprint : ByteArray;
    FResponseNonce: BufferType;
    FCMS : ByteArray;

    function GetCertificates: TElCustomCertStorage;
  public
    constructor Create; override;
    destructor Destroy; override;

    function ParseCMS(const CMSData: ByteArray): integer;

    property Nonce: BufferType read FNonce write FNonce; 
    property Certificates: TElCustomCertStorage read GetCertificates;
    property MessageImprint : ByteArray read FMessageImprint;
    property ResponseNonce: BufferType read FResponseNonce;
    property CMS : ByteArray read FCMS;
  end;

  TElCustomTSPClient = class(TElTSPClass)
  protected
    FTSPInfo: TElClientTSPInfo;
    FHashAlgorithm: Integer;
    FIncludeCertificates: Boolean;


    procedure CreateRequest(const HashedData : ByteArray; var Request: ByteArray);
    function MessageImprint(HashedData : ByteArray): BufferType;
    function ProcessReply(const Reply: ByteArray; out ServerResult: TSBPKIStatus;
      out FailureInfo: integer; out ReplyCMS : ByteArray): integer;
    function MatchTSPRequirements(const HashedData : ByteArray): Integer;

  public
    constructor Create(Owner: TSBComponentBase); override;
    destructor Destroy; override;

    function Timestamp(const HashedData: ByteArray;
      {$ifndef BUILDER_USED}out{$else}var{$endif} ServerResult: TSBPKIStatus;
      {$ifndef BUILDER_USED}out{$else}var{$endif} FailureInfo: integer;
      {$ifndef BUILDER_USED}out{$else}var{$endif} ReplyCMS : ByteArray) : Integer; virtual; abstract;
    property TSPInfo: TElClientTSPInfo read FTSPInfo;

  published
    property HashAlgorithm: Integer read FHashAlgorithm write FHashAlgorithm;
    // IncludeCertificates specifies, if the reply from the server must include
    // certificate(s), used for signing. This corresponds to TElMessageSigner.IncludeCertificates
    property IncludeCertificates: Boolean read FIncludeCertificates write
      FIncludeCertificates;
  end;

  TSBTimestampNeededEvent =  procedure(Sender : TObject;
    RequestStream, ReplyStream: TStream; var Succeeded : boolean) of object;

  TElFileTSPClient = class(TElCustomTSPClient)
  protected
    FOnTimestampNeeded : TSBTimestampNeededEvent;
  public
    function Timestamp(const HashedData: ByteArray;
      {$ifndef BUILDER_USED}out{$else}var{$endif} ServerResult: TSBPKIStatus;
      {$ifndef BUILDER_USED}out{$else}var{$endif} FailureInfo: integer;
      {$ifndef BUILDER_USED}out{$else}var{$endif} ReplyCMS : ByteArray) : Integer; override;
  published
    property OnTimestampNeeded : TSBTimestampNeededEvent read FOnTimestampNeeded write FOnTimestampNeeded;
  end;

procedure Register;

implementation

uses SBMessages;

procedure Register;
begin
  RegisterComponents('PKIBlackbox', [TElFileTSPClient]);
end;

constructor TElCustomTSPClient.Create(Owner: TSBComponentBase);
begin
  inherited Create(Owner);
  FTSPInfo := TElClientTSPInfo.Create;
  FTSPInfo.FOwner := Self;
  IncludeCertificates := true;
  FHashAlgorithm := SB_ALGORITHM_DGST_SHA1;
end;


destructor TElCustomTSPClient.Destroy;
begin
  FreeAndNil(FTSPInfo);
  inherited;
end;

//  s := TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$30#$03#$08); //id-kp-timeStamping

procedure TElCustomTSPClient.CreateRequest(const HashedData : ByteArray; var
    Request: ByteArray);
var
  s: BufferType;
  l: TStringList;
begin
  l := TStringList.Create;
      // TimeStampReq ::= SEQUENCE  {
      //   version                      INTEGER  { v1(1) },
  l.Add(WriteInteger(1));
      //   messageImprint               MessageImprint,
  l.Add(MessageImprint(HashedData));
      //   reqPolicy             TSAPolicyId              OPTIONAL,
      //      TSAPolicyId ::= OBJECT IDENTIFIER
      // not needed???
      //   nonce                 INTEGER                  OPTIONAL,
  if Length(FTSPInfo.Nonce) <> 0 then
  begin
    if Length(FTSPInfo.FNonce) > 8 then
      SetLength(FTSPInfo.FNonce, 8);
    l.Add(WritePrimitive($02, FTSPInfo.FNonce));
  end;

      //   certReq               BOOLEAN                  DEFAULT FALSE,
  l.Add(WriteBoolean(FIncludeCertificates));
      // }
  s := WriteSequence(l);
  l.Free;
  Request := BytesOfString(s);
end;

function TElCustomTSPClient.MatchTSPRequirements(const HashedData : ByteArray): Integer;
begin
  Result := 0;
  if CompareStr(FTSPInfo.Nonce, FTSPInfo.ResponseNonce) <> 0 then
    result := SB_TSP_ERROR_WRONG_NONCE
  else
  if not ValidateImprint(FHashAlgorithm, HashedData, FTSPInfo.FMessageImprint) then
    result := SB_TSP_ERROR_WRONG_IMPRINT
  else
  if (not IncludeCertificates) and (TSPInfo.Certificates.Count > 0) then
    result := SB_TSP_ERROR_UNEXPECTED_CERTIFICATES;
end;

function TElCustomTSPClient.MessageImprint(HashedData : ByteArray): BufferType;
var
  l: TStringList;
begin
  l := TStringList.Create;
  l.Add(WriteOID(GetOIDByHashAlgorithm(FHashAlgorithm)));
  result := WriteSequence(l);
  l.Free;
  l := TStringList.Create;
  l.Add(result);
  l.Add(WriteOctetString(StringOfBytes(HashedData)));
  result := WriteSequence(l);
  l.Free;
end;

//  id_ct_TSTInfo = #$2b#$06#$2a#$86#$48#$86#$f7#$0d#$01#$09#$10#$01#$04;

function TElCustomTSPClient.ProcessReply(const Reply: ByteArray; out ServerResult: TSBPKIStatus; out FailureInfo: integer; out ReplyCMS : ByteArray): integer;
var
  ASN, Sequence, Field: TElASN1ConstrainedTag;
  Param: TElASN1SimpleTag;
  sz: integer;
begin
  // This method doesn't parse CMS. It just parses the reply and puts CMS to
  // Reply CMS. Parsing is done separately by ParseCMS
  Result := SB_TSP_ERROR_WRONG_DATA;
  ServerResult := psGranted;
  FailureInfo := 0;

  try
    ASN := TElASN1ConstrainedTag.Create;
    try
      try
        if not ASN.LoadFromBuffer(@Reply[0], Length(reply)) then
          exit;
      except
        exit;
      end;
      if ASN.Count = 0 then
        exit;
      //   TimeStampResp ::= SEQUENCE  {
      //      status                  PKIStatusInfo,
      //      timeStampToken          TimeStampToken     OPTIONAL  }
      Field := TElASN1ConstrainedTag(ASN.GetField(0));
      if (Field = nil) or not Field.IsConstrained then
        exit;
      if Field.Count < 1 then
        exit;
      //   PKIStatusInfo ::= SEQUENCE {
      Sequence := TElASN1ConstrainedTag(Field.GetField(0));
      if (Sequence = nil) or not Sequence.IsConstrained then
        exit;
      //      status        PKIStatus,
      Param := TElASN1SimpleTag(Sequence.GetField(0));
      if (Param = nil) or Param.IsConstrained or (Param.TagId <> SB_ASN1_INTEGER) then
        exit;
      ServerResult := TSBPKIStatus(ASN1ReadInteger(Param));
      Param := TElASN1SimpleTag(Sequence.GetField(1));
      //      statusString  PKIFreeText     OPTIONAL,
      if (Param <> nil) and Param.IsConstrained then
        Param := TElASN1SimpleTag(Sequence.GetField(2));
      //      failInfo      PKIFailureInfo  OPTIONAL
      if (Param <> nil) and not Param.IsConstrained and (Param.TagId = SB_ASN1_BITSTRING) then
        FailureInfo := ASN1ReadInteger(Param);
      //   }
      //   TimeStampToken ::= ContentInfo
      Sequence := TElASN1ConstrainedTag(Field.GetField(1));
      if (Sequence <> nil) and Sequence.IsConstrained then
      begin
        sz := 0;
        Sequence.SaveToBuffer(nil, sz);
        SetLength(ReplyCMS, sz);
        Sequence.SaveToBuffer(@ReplyCMS[0], sz);
        Result := 0;
        exit;
      end;
    finally
      ASN.Free;
    end;
  except
    ServerResult := psRejection;
    FailureInfo := tfiSystemFailure;
  end;
  Result := 0;
end;

function TElFileTSPClient.Timestamp(const HashedData: ByteArray;
  {$ifndef BUILDER_USED}out{$else}var{$endif} ServerResult: TSBPKIStatus;
  {$ifndef BUILDER_USED}out{$else}var{$endif} FailureInfo: integer;
  {$ifndef BUILDER_USED}out{$else}var{$endif} ReplyCMS : ByteArray) : Integer;
var RequestStream,
    ReplyStream : TMemoryStream;
    Request : ByteArray;
    Success : boolean;
begin
  if not Assigned(FOnTimestampNeeded) then
  begin
    result := SB_TSP_ERROR_NO_PARAMETERS;
    exit;
  end;
  if FHashAlgorithm = 0 then
  begin
    result := SB_TSP_ERROR_NO_PARAMETERS;
    exit;
  end;

  RequestStream := TMemoryStream.Create;
  ReplyStream := TMemoryStream.Create;
  try
    CreateRequest(HashedData, Request);

    RequestStream.Write(Request[0], Length(Request));
    RequestStream.Position := 0;
    
    Success := true;
    FOnTimestampNeeded(Self, RequestStream, ReplyStream, Success);
    if not Success then
    begin
      result := SB_TSP_ERROR_NO_REPLY;
      exit;
    end;
    SetLength(Request, ReplyStream.Size);
    if Length(Request) = 0 then
    begin
      result := SB_TSP_ERROR_WRONG_DATA;
      exit;
    end;

    ReplyStream.Position := 0;

    ReplyStream.Read(Request[0], Length(Request));
    result := ProcessReply(Request, ServerResult, FailureInfo, ReplyCMS);

    if Result = 0 then
      result := FTSPInfo.ParseCMS(ReplyCMS);

    if Result = 0 then
      result := MatchTSPRequirements(HashedData);
  finally
    FreeAndNil(RequestStream);
    FreeAndNil(ReplyStream);
  end;
end;

constructor TElClientTSPInfo.Create;
begin
  inherited;
  FVerifier := TElMessageVerifier.Create(nil);
end;

destructor TElClientTSPInfo.Destroy;
begin
  FreeAndNil(FVerifier);
  inherited;
end;

function TElClientTSPInfo.GetCertificates: TElCustomCertStorage;
begin
  Result := TElMessageVerifier(FVerifier).Certificates;
end;

function TElClientTSPInfo.ParseCMS(const CMSData: ByteArray): integer;
var
  buf: BufferType;
  sz, fld, fld2: integer;
  ASN, Sequence, Field: TElASN1ConstrainedTag;
  Param: TElASN1SimpleTag;
  CustomParam : TElASN1CustomTag;
begin
(*
  this method does the following:
  1) Parse CMS Data using Verifier.Verify and check the result. Return this result, if it is not successful.
  2) Extract TSP-specific data and put it into the corresponding properties
*)

  CustomParam := nil;
  FCMS := (CMSData);

  sz := 0;
  Setlength(buf, 0);
  Result :=
  TElMessageVerifier(FVerifier).Verify(@CMSData[0], Length(CMSData), nil, sz);

  if Result = SB_MESSAGE_ERROR_BUFFER_TOO_SMALL then
  begin
    SetLength(buf, sz);
    Result :=
    TElMessageVerifier(FVerifier).Verify(@CMSData[0], Length(CMSData), @buf[1], sz);
    if (Result <> 0) and not ((not TElCustomTSPClient(FOwner).IncludeCertificates) and (Result = SB_MESSAGE_ERROR_NO_CERTIFICATE)) then
      exit;

    SetLength(buf, sz);
  end
  else
    exit;

  Result := -1;

  try
    ASN := TElASN1ConstrainedTag.Create;
    try
      try
        if not ASN.LoadFromBuffer(@buf[1], Length(buf)) then
          exit;
      except
        exit;
      end;

      if ASN.Count = 0 then
        exit;

      // TSTInfo ::= SEQUENCE  {
      Field := TElASN1ConstrainedTag(ASN.GetField(0));
      if (Field = nil) or not Field.IsConstrained then
        exit;
      if Field.Count < 2 then
        exit;
      //   version                      INTEGER  { v1(1) },
      Param := TElASN1SimpleTag(Field.GetField(0));
      if (Param = nil) or Param.IsConstrained or (Param.TagId <> SB_ASN1_INTEGER) then
        exit;
      if ASN1ReadInteger(Param) <> 1 {default v1(1)} then
        exit;
      //   policy                       TSAPolicyId,
      Param := TElASN1SimpleTag(Field.GetField(1));
      if (Param = nil) or Param.IsConstrained or (Param.TagId <> SB_ASN1_OBJECT) then
        exit;

      //   messageImprint               MessageImprint,
      Sequence := TElASN1ConstrainedTag(Field.GetField(2));
      if (Sequence = nil) or not Sequence.IsConstrained then
        exit;
      sz := 0;
      SetLength(FMessageImprint, sz);
      Sequence.SaveToBuffer(@FMessageImprint[0], sz);
      SetLength(FMessageImprint, sz);
      Sequence.SaveToBuffer(@FMessageImprint[0], sz);

      //   serialNumber                 INTEGER,
      Param := TElASN1SimpleTag(Field.GetField(3));
      if (Param = nil) or Param.IsConstrained or (Param.TagId <> SB_ASN1_INTEGER) then
        exit;
      FSerialNumber := BytesOfString(Param.Content);
      //   genTime                      GeneralizedTime,
      Param := TElASN1SimpleTag(Field.GetField(4));
      if (Param = nil) or Param.IsConstrained or (Param.TagId <> SB_ASN1_GENERALIZEDTIME) then
        exit;

      FTime := GeneralizedTimeToDateTime((Param.Content));

      fld := 5;
      Param := TElASN1SimpleTag(Field.GetField(fld));
      Sequence := TElASN1ConstrainedTag(Field.GetField(fld));

      FResponseNonce := EmptyBuffer;

      FAccuracySet := false;
      FAccuracySec := 0;
      FAccuracyMilli := 0;
      FAccuracyMicro := 0;
      //   accuracy                     Accuracy                 OPTIONAL,
      if (Sequence <> nil) and Sequence.IsConstrained then
      begin
        FAccuracySet := true;
        //   Accuracy ::= SEQUENCE {
        fld2 := 0;
        Param := TElASN1SimpleTag(Sequence.GetField(fld2));
//        Seq := TElASN1ConstrainedTag(Sequence.GetField(fld2));
        //     seconds        INTEGER              OPTIONAL,
        if (Param <> nil) and not Param.IsConstrained and (Param.TagId = SB_ASN1_INTEGER) then
        begin
          FAccuracySec := ASN1ReadInteger(Param);
          inc(fld2);
          //Seq := TElASN1ConstrainedTag(Sequence.GetField(fld2));
          CustomParam := Sequence.GetField(fld2);
        end;
        //     millis     [0] INTEGER  (1..999)    OPTIONAL,
        if (CustomParam <> nil) and (CustomParam.CheckType($80, false)) {Seq.IsConstrained and (Seq.TagId = SB_ASN1_A0)} then
        begin
          Param := TElASN1SimpleTag(CustomParam{Seq.GetField(0)});
          FAccuracyMilli := ASN1ReadInteger(Param);
          inc(fld2);
          //Seq := TElASN1ConstrainedTag(Sequence.GetField(fld2));
          CustomParam := Sequence.GetField(fld2);
        end;
        //     micros     [1] INTEGER  (1..999)    OPTIONAL  }
        if (CustomParam <> nil) and (CustomParam.CheckType($81, false)) {Seq.IsConstrained and (Seq.TagId = SB_ASN1_A1)} then
        begin
          Param := TElASN1SimpleTag(CustomParam{Seq.GetField(0)});
          FAccuracyMicro := ASN1ReadInteger(Param);
        end;
        inc(fld);
        Param := TElASN1SimpleTag(Field.GetField(fld));
        Sequence := TElASN1ConstrainedTag(Field.GetField(fld));
      end;
      //   ordering                     BOOLEAN             DEFAULT FALSE,
      if (Param <> nil) and not Param.IsConstrained and (Param.TagId = SB_ASN1_BOOLEAN) then
      begin
        inc(fld);
        Param := TElASN1SimpleTag(Field.GetField(fld));
        Sequence := TElASN1ConstrainedTag(Field.GetField(fld));
      end;
      //   nonce                        INTEGER                  OPTIONAL,
      if (Param <> nil) and not Param.IsConstrained and (Param.TagId = SB_ASN1_INTEGER) then
      begin
        FResponseNonce := Param.Content;
        inc(fld);
        Param := TElASN1SimpleTag(Field.GetField(fld));
        Sequence := TElASN1ConstrainedTag(Field.GetField(fld));
      end;
      //   tsa                          [0] GeneralName          OPTIONAL,
      if (Param <> nil) and Param.CheckType(SB_ASN1_A0, true) and (Sequence.Count > 0) then
      begin
        if Assigned(FTSAName) then
          FreeAndNil(FTSAName);
        FTSAName := TElGeneralName.Create;
        FTSAName.LoadFromTag(Sequence.GetField(0));
      end;

      // reading extensions if present
      if (fld < Field.Count) and (Field.GetField(fld).CheckType(SB_ASN1_A1, true)) then
      begin
        // reading extensions
      end;

    finally
      ASN.Free;
    end;
  except
    exit;
  end;
  result := 0;
end;

end.
