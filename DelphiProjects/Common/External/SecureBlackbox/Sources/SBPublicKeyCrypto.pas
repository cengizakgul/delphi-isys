(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$i SecBbox.inc}

unit SBPublicKeyCrypto;

interface

uses
  Classes,
  SysUtils,
  SBUtils,
  SBConstants,
  SBRSA,
  SBDSA,
  SBPEM,
  SBHashFunction,
  SBASN1Tree,
  SBCustomCrypto,
  SBX509;

type
  // responsible for public (and private) key material storing
  TElPublicKeyMaterial = class(TElKeyMaterial)
  protected
    FPublicKey : boolean;
    FSecretKey : boolean;
    function GetValid : boolean; override;
    function IsPEM(Buffer: pointer;
       Size: integer): boolean;
  public
    procedure Generate(Bits : integer); override;
    procedure LoadPublic(Buffer: pointer; Size: integer); overload; virtual;
    procedure LoadSecret(Buffer: pointer; Size: integer); overload; virtual;
    procedure SavePublic(Buffer: pointer; var Size: integer); overload; virtual;
    procedure SaveSecret(Buffer: pointer; var Size: integer); overload; virtual;
    procedure LoadPublic(Stream : TStream;
      Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}); overload;
    procedure LoadSecret(Stream : TStream;
      Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}); overload;
    procedure SavePublic(Stream : TStream); overload; virtual;
    procedure SaveSecret(Stream : TStream); overload; virtual;
    procedure Save(Stream : TStream); override;
    procedure Load(Stream : TStream;
      Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}); override;
    property PublicKey : boolean read FPublicKey;
    property SecretKey : boolean read FSecretKey;
  end;

  TSBPublicKeyOperation = 
   (pkoEncrypt, pkoDecrypt, pkoSign, pkoSignDetached,
    pkoVerify, pkoVerifyDetached);
    
  TSBPublicKeyVerificationResult = 
   (pkvrSuccess, pkvrInvalidSignature, pkvrKeyNotFound,
    pkvrFailure);

  // base class for other public key encryption classes. Do not instantiate.
  TElPublicKeyCrypto = class(TElCustomCrypto)
  private
    FKeyMaterial : TElPublicKeyMaterial;
    FOutput : ByteArray;
    FOutputStream : TStream;
    FOutputIsStream : boolean;
  protected
    function GetSupportsEncryption: boolean; virtual;
    function GetSupportsSigning: boolean; virtual;
    procedure SetKeyMaterial(Material : TElPublicKeyMaterial); virtual;
    procedure SignInit(Detached: boolean); virtual;
    procedure SignUpdate(Buffer: pointer;
       Size: integer); virtual;
    procedure SignFinal; virtual;
    procedure EncryptInit; virtual;
    procedure EncryptUpdate(Buffer: pointer;
       Size: integer); virtual;
    procedure EncryptFinal; virtual;
    procedure DecryptInit; virtual;
    procedure DecryptUpdate(Buffer: pointer;
       Size: integer); virtual;
    procedure DecryptFinal; virtual;
    procedure VerifyInit(Detached: boolean; Signature: pointer;
       SigSize: integer); virtual;
    procedure VerifyUpdate(Buffer: pointer; Size: integer); virtual;
    function VerifyFinal : TSBPublicKeyVerificationResult; virtual;
    function EstimateOutputSize(InBuffer: pointer;
       InSize: integer;
      Operation : TSBPublicKeyOperation): integer; virtual;
    procedure WriteToOutput(Buffer: pointer;
       Size: integer); virtual;
    procedure Reset;
    class function IsAlgorithmSupported(Alg: integer): boolean; overload; virtual;
    class function IsAlgorithmSupported(const OID: BufferType): boolean; overload; virtual;
    class function GetName() : string; virtual;
    class function GetDescription() : string; virtual;
  public
    constructor Create(const OID : BufferType); overload; virtual;
    constructor Create(Alg : integer); overload; virtual;
    constructor Create; overload; virtual;

    procedure Encrypt(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize: integer); overload;
    procedure Decrypt(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize: integer); overload;
    procedure Sign(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize: integer); overload;
    procedure SignDetached(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize: integer); overload;
    // TODO: consider possible verification results
    function Verify(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize: integer): TSBPublicKeyVerificationResult; overload;
    function VerifyDetached(InBuffer: pointer; InSize: integer; SigBuffer: pointer;
      SigSize: integer): TSBPublicKeyVerificationResult; overload;
    procedure Encrypt(InStream, OutStream : TStream;
      Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}); overload;
    procedure Decrypt(InStream, OutStream : TStream;
      Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}); overload;
    procedure Sign(InStream, OutStream : TStream;
      Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}); overload;
    procedure SignDetached(InStream, OutStream : TStream;
      Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}); overload;
    function Verify(InStream, OutStream : TStream;
      Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}): TSBPublicKeyVerificationResult; overload;
    function VerifyDetached(InStream, SigStream : TStream;
      InCount : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif};
      SigCount : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}): TSBPublicKeyVerificationResult; overload;
      
    property KeyMaterial : TElPublicKeyMaterial read FKeyMaterial write SetKeyMaterial;
    property SupportsEncryption : boolean read GetSupportsEncryption;
    property SupportsSigning : boolean read GetSupportsSigning;
  end;

  TElPublicKeyCryptoClass =  class of TElPublicKeyCrypto;

  // responsible for storing RSA key material
  TSBRSAKeyFormat = 
    (rsaPKCS1, rsaOAEP, rsaPSS, rsaX509);
  
  TElRSAKeyMaterial = class(TElPublicKeyMaterial)
  private
    FKeyBlob : ByteArray;
    FPublicKeyBlob : ByteArray;
    FM : ByteArray;
    FE : ByteArray;
    FD : ByteArray;
    FKeyFormat : TSBRSAKeyFormat;
    FPassphrase : string;
    FPEMEncode : boolean;
    FCert : TElX509Certificate;
    FStrLabel : string;
    FSaltSize : integer;
    FHashAlgorithm : integer;
    procedure Reset;
    procedure RecalculatePublicKeyBlob;
    procedure TrimParams;
    procedure ExtractKeyMaterialFromX509;
  protected
    function GetValid : boolean; override;
  public
    constructor Create;
    destructor Destroy; override;
    
    procedure Generate(Bits : integer); override;
    procedure LoadPublic(Buffer: pointer; Size: integer); override;
    procedure LoadSecret(Buffer: pointer; Size: integer); override;
    procedure SavePublic(Buffer: pointer; var Size: integer); override;
    procedure SaveSecret(Buffer: pointer; var Size: integer); override;
    procedure Import(Cert: TElX509Certificate);
    property KeyFormat : TSBRSAKeyFormat read FKeyFormat;
    property Passphrase : string read FPassphrase write FPassphrase;
    property PEMEncode : boolean read FPEMEncode write FPEMEncode;
    property StrLabel : string read FStrLabel write FStrLabel; //for RSA-OAEP
    property SaltSize : integer read FSaltSize write FSaltSize; //for RSA-PSS
    property HashAlgorithm : integer read FHashAlgorithm write FHashAlgorithm; //for RSA-PSS/OAEP
  end;

  // responsible for RSA encryption and signing
  TSBRSAPublicKeyCryptoType = 
    (rsapktPKCS1, rsapktOAEP, rsapktPSS);
    
  TSBRSAPublicKeyCryptoEncoding =  (rsapkeBinary, rsapkeBase64);
  
  TElRSAPublicKeyCrypto = class(TElPublicKeyCrypto)
  private
    FOID : BufferType;
    FSupportsEncryption : boolean;
    FSupportsSigning : boolean;
    FInputIsHash : boolean;
    FCryptoType : TSBRSAPublicKeyCryptoType;
    FInputEncoding : TSBRSAPublicKeyCryptoEncoding;
    FOutputEncoding : TSBRSAPublicKeyCryptoEncoding;
    FUseAlgorithmPrefix : boolean;
    FSpool : ByteArray;
    FHashFunction : TElHashFunction;
    FSignature : ByteArray;
    function GetUsedHashFunction: integer;
    function GetUsedHashFunctionOID: BufferType;
    function AddAlgorithmPrefix(const Hash: BufferType): BufferType;
    function RemoveAlgorithmPrefix(const Value: BufferType; var HashAlg : BufferType;
      var HashPar : BufferType): BufferType;
  protected
    function GetSupportsEncryption: boolean; override;
    function GetSupportsSigning: boolean; override;
    procedure SetKeyMaterial(Material : TElPublicKeyMaterial); override;
    procedure SetCryptoType(Value : TSBRSAPublicKeyCryptoType);
    procedure SignInit(Detached: boolean); override;
    procedure SignUpdate(Buffer: pointer;
       Size: integer); override;
    procedure SignFinal; override;
    procedure EncryptInit; override;
    procedure EncryptUpdate(Buffer: pointer;
       Size: integer); override;
    procedure EncryptFinal; override;
    procedure DecryptInit; override;
    procedure DecryptUpdate(Buffer: pointer;
       Size: integer); override;
    procedure DecryptFinal; override;
    procedure VerifyInit(Detached: boolean; Signature: pointer;
       SigSize: integer); override;
    procedure VerifyUpdate(Buffer: pointer; Size: integer); override;
    function VerifyFinal : TSBPublicKeyVerificationResult; override;
    function EstimateOutputSize(InBuffer: pointer;
       InSize: integer;
      Operation : TSBPublicKeyOperation): integer; override;
    class function IsAlgorithmSupported(Alg: integer): boolean; overload; override;
    class function IsAlgorithmSupported(const OID: BufferType): boolean; overload; override;
    class function GetName() : string; override;
    class function GetDescription() : string; override;
    procedure WriteToOutput(Buffer: pointer;
       Size: integer); override;
    procedure Reset;
  public
    constructor Create(const OID : BufferType); overload; override;
    constructor Create(Alg : integer); overload; override;
    constructor Create; overload; override;
    destructor Destroy; override;
    
    property InputIsHash : boolean read FInputIsHash write FInputIsHash;
    property CryptoType : TSBRSAPublicKeyCryptoType read FCryptoType
      write SetCryptoType;
    property UseAlgorithmPrefix : boolean read FUseAlgorithmPrefix write FUseAlgorithmPrefix;
    property InputEncoding : TSBRSAPublicKeyCryptoEncoding read FInputEncoding write FInputEncoding;
    property OutputEncoding : TSBRSAPublicKeyCryptoEncoding read FOutputEncoding write FOutputEncoding;
  end;

  // responsible for storing DSA key material
  TSBDSAKeyFormat =  (dsaFIPS, dsaX509);
  
  TElDSAKeyMaterial = class(TElPublicKeyMaterial)
  private
    FKeyBlob : ByteArray;
    FP : ByteArray;
    FQ : ByteArray;
    FG : ByteArray;
    FY : ByteArray;
    FX : ByteArray;

    FStrictKeyValidation : boolean;
    FKeyFormat : TSBDSAKeyFormat;
    FPassphrase : string;
    FPEMEncode : boolean;
    FCert : TElX509Certificate;
    procedure Reset;
    procedure TrimParams;
    procedure ExtractKeyMaterialFromX509;
  protected
    function GetValid : boolean; override;
  public
    constructor Create;
    destructor Destroy; override;
    
    procedure Generate(Bits : integer); override;
    procedure LoadSecret(Buffer: pointer; Size: integer); override;
    procedure SaveSecret(Buffer: pointer; var Size: integer); override;
    procedure ImportPublicKey(P : pointer; PSize : integer; Q : pointer;
      QSize : integer; G : pointer; GSize : integer; Y : pointer; YSize : integer);
    procedure ExportPublicKey(P : pointer; var PSize : integer;
      Q : pointer; var QSize : integer; G : pointer; var GSize : integer;
      Y : pointer; var YSize : integer);
    procedure Import(Cert: TElX509Certificate);
    property KeyFormat : TSBDSAKeyFormat read FKeyFormat;
    property Passphrase : string read FPassphrase write FPassphrase;
    property PEMEncode : boolean read FPEMEncode write FPEMEncode;
    property StrictKeyValidation : boolean read FStrictKeyValidation
      write FStrictKeyValidation;
  end;


  TSBDSAPublicKeyCryptoEncoding =  (dsapkeBinary, dsapkeBase64);
  
  TElDSAPublicKeyCrypto = class(TElPublicKeyCrypto)
  private
    FOID : BufferType;
    FInputIsHash : boolean;
    FInputEncoding : TSBDSAPublicKeyCryptoEncoding;
    FOutputEncoding : TSBDSAPublicKeyCryptoEncoding;
    FSpool : ByteArray;
    FHashFunction : TElHashFunction;
    FSignature : ByteArray;
    function GetUsedHashFunction: integer;
  protected
    function GetSupportsEncryption: boolean; override;
    function GetSupportsSigning: boolean; override;
    procedure SetKeyMaterial(Material : TElPublicKeyMaterial); override;
    procedure SignInit(Detached: boolean); override;
    procedure SignUpdate(Buffer: pointer;
       Size: integer); override;
    procedure SignFinal; override;
    procedure VerifyInit(Detached: boolean; Signature: pointer;
       SigSize: integer); override;
    procedure VerifyUpdate(Buffer: pointer;
       Size: integer); override;
    function VerifyFinal : TSBPublicKeyVerificationResult; override;
    function EstimateOutputSize(InBuffer: pointer;
       InSize: integer;
      Operation : TSBPublicKeyOperation): integer; override;
    class function IsAlgorithmSupported(Alg: integer): boolean; overload; override;
    class function IsAlgorithmSupported(const OID: BufferType): boolean; overload; override;
    class function GetName() : string; override;
    class function GetDescription() : string; override;
    procedure WriteToOutput(Buffer: pointer;
       Size: integer); override;
    procedure Reset;
  public
    constructor Create(const OID : BufferType); overload; override;
    constructor Create(Alg : integer); overload; override;
    constructor Create; overload; override;
    destructor Destroy; override;
    
    property InputIsHash : boolean read FInputIsHash write FInputIsHash;
    property InputEncoding : TSBDSAPublicKeyCryptoEncoding read FInputEncoding write FInputEncoding;
    property OutputEncoding : TSBDSAPublicKeyCryptoEncoding read FOutputEncoding write FOutputEncoding;
  end;

  // responsible for creation of ElPublicKeyEncryption classes
  TElPublicKeyCryptoFactory = class
  private
    FRegisteredClasses: TList;
    procedure RegisterDefaultClasses;
    function GetRegisteredClass(Index: integer) : TElPublicKeyCryptoClass;
    function GetRegisteredClassCount: integer;
  public
    constructor Create;
    destructor Destroy; override;
    
    procedure RegisterClass(Cls : TElPublicKeyCryptoClass);
    function CreateInstance(const OID : BufferType): TElPublicKeyCrypto; overload;
    function CreateInstance(Alg : integer): TElPublicKeyCrypto; overload;
    function IsAlgorithmSupported(const OID : BufferType): boolean; overload;
    function IsAlgorithmSupported(Alg : integer): boolean; overload;
    property RegisteredClasses[Index: integer] : TElPublicKeyCryptoClass
      read GetRegisteredClass;
    property RegisteredClassCount : integer read GetRegisteredClassCount;
  end;

  EElPublicKeyCryptoError =  class(ESecureBlackboxError);

implementation

resourcestring
  SUnsupportedOperation = 'Unsupported operation';
  SBufferTooSmall = 'Output buffer is too small';
  SInternalError = 'Internal error';
  SUnsupportedAlgorithm = 'Unsupported algorithm: %s';
  SUnsupportedAlgorithmInt = 'Unsupported algorithm: %d';
  SNotASigningAlgorithm = 'Algorithm does not support signing';
  SNotAnEncryptionAlgorithm = 'Algorithm does not support encryption';
  SInvalidPEM = 'Invalid PEM data';
  SPEMWriteError = 'PEM write error';
  SInvalidKeyMaterialType = 'Invalid key material type';
  SInvalidPublicKey = 'Invalid public key';
  SInvalidSecretKey = 'Invalid secret key';
  SInvalidPassphrase = 'Invalid passphrase';
  SInputTooLong = 'Input is too long';
  SPublicKeyNotFound = 'Public key not found';
  SSecretKeyNotFound = 'Secret key not found';
  SBadKeyMaterial = 'Bad key material';
  SEncryptionFailed = 'Encryption failed';
  SDecryptionFailed = 'Decryption failed';
  SSigningFailed = 'Signing failed';
  SUnsupportedEncryptionType = 'Unsupported encryption type';
  SBadSignatureFormatting = 'Bad signature formatting';
  SOnlyDetachedSigningSupported = 'Only detached signatures are supported';
  SNotImplemented = 'Not implemented';
  SUnsupportedCertType = 'Unsupported certificate type';
  SInvalidSignatureEncoding = 'Invalid signature encoding';
  SInvalidEncoding = 'Invalid encoding';
  SInvalidInput = 'Invalid input';


// auxiliary routines
  
function TrimParam(const Par : ByteArray) : ByteArray;
var
  Index : integer;
begin
  Index := 0;
  while (Index < Length(Par)) and (Par[Index] = 0) do
    Inc(Index);
  Result := Copy(Par, Index, Length(Par) - Index);
  if Length(Result) = 0 then
  begin
    SetLength(Result, 1);
    Result[0] := 0;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TElPublicKeyMaterial class

function TElPublicKeyMaterial.IsPEM(Buffer: pointer;
   Size: integer): boolean;
const
  PEM_HEADER : string = '-----BEGIN';
var
  Ptr: ^byte;
begin
  Ptr := Buffer;
  while (Size > 0) and (Ptr^ in [9, 10, 13, 32]) do
  begin
    Inc(Ptr);
    Dec(Size);
  end;
  Result := (Size > Length(PEM_HEADER)) and (CompareMem(Buffer, @PEM_HEADER[1],
    Length(PEM_HEADER)));
end;


procedure TElPublicKeyMaterial.Generate(Bits : integer);
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyMaterial.LoadPublic(Buffer: pointer; Size: integer);
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyMaterial.LoadSecret(Buffer: pointer; Size: integer);
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyMaterial.SavePublic(Buffer: pointer; var Size: integer);
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyMaterial.SaveSecret(Buffer: pointer; var Size: integer);
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyMaterial.LoadPublic(Stream : TStream;
  Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif});
var
  Buf : array of byte;
begin
  if Count = 0 then
    Count := Stream.Size - Stream.Position;
  SetLength(Buf, Count);
  Count := Stream.Read(Buf[0], Count);
  LoadPublic(@Buf[0], Count);
end;

procedure TElPublicKeyMaterial.LoadSecret(Stream : TStream;
  Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif});
var
  Buf : array of byte;
begin
  if Count = 0 then
    Count := Stream.Size - Stream.Position;
  SetLength(Buf, Count);
  Count := Stream.Read(Buf[0], Count);
  LoadSecret(@Buf[0], Count);
end;

procedure TElPublicKeyMaterial.SavePublic(Stream : TStream);
var
  Buf : ByteArray;
  Size : integer;
begin
  Size := 0;
  SavePublic(nil, Size);
  SetLength(Buf, Size);
  SavePublic(@Buf[0], Size);
  Stream.Write(Buf[0], Size);
end;

procedure TElPublicKeyMaterial.SaveSecret(Stream : TStream);
var
  Buf : ByteArray;
  Size : integer;
begin
  Size := 0;
  SaveSecret(nil, Size);
  SetLength(Buf, Size);
  SaveSecret(@Buf[0], Size);
  Stream.Write(Buf[0], Size);
end;

procedure TElPublicKeyMaterial.Save(Stream : TStream);
begin
  if SecretKey then
    SaveSecret(Stream)
  else
    SavePublic(Stream);
end;

procedure TElPublicKeyMaterial.Load(Stream : TStream;
  Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif});
var
  OldPos : integer;
  B : boolean;
begin
  OldPos := Stream.Position;
  // trying to load secret key
  B := true;
  try
    LoadSecret(Stream, Count);
  except
    B := false;
  end;
  // trying to load public key
  if not B then
  begin
    Stream.Position := OldPos;
    LoadPublic(Stream, Count);
  end;
end;

function TElPublicKeyMaterial.GetValid : boolean;
begin
  Result := false;
end;

////////////////////////////////////////////////////////////////////////////////
// TElPublicKeyCrypto class

constructor TElPublicKeyCrypto.Create(const OID : BufferType);
begin
  Create;
end;

constructor TElPublicKeyCrypto.Create(Alg : integer);
begin
  Create;
end;

constructor TElPublicKeyCrypto.Create;
begin
  inherited Create;
end;

function TElPublicKeyCrypto.GetSupportsEncryption: boolean;
begin
  {$ifdef FPC}
  result := false;
  {$endif}
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

function TElPublicKeyCrypto.GetSupportsSigning: boolean;
begin
  {$ifdef FPC}
  result := false;
  {$endif}
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

class function TElPublicKeyCrypto.IsAlgorithmSupported(Alg: integer): boolean;
begin
  Result := false;
end;

class function TElPublicKeyCrypto.IsAlgorithmSupported(const OID: BufferType): boolean;
begin
  Result := false;
end;

procedure TElPublicKeyCrypto.SignInit(Detached: boolean);
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyCrypto.SignUpdate(Buffer: pointer;
  Size: integer);
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyCrypto.SignFinal;
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyCrypto.EncryptInit;
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyCrypto.EncryptUpdate(Buffer: pointer;
  Size: integer);
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyCrypto.EncryptFinal;
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyCrypto.DecryptInit;
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyCrypto.DecryptUpdate(Buffer: pointer;
  Size: integer);
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyCrypto.DecryptFinal;
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyCrypto.VerifyInit(Detached: boolean; Signature: pointer;
   SigSize: integer);
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyCrypto.VerifyUpdate(Buffer: pointer; Size: integer);
begin
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

function TElPublicKeyCrypto.VerifyFinal: TSBPublicKeyVerificationResult;
begin
  {$ifdef FPC}
  result := pkvrFailure;
  {$endif}
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

function TElPublicKeyCrypto.EstimateOutputSize(InBuffer: pointer;
   InSize: integer;
  Operation : TSBPublicKeyOperation): integer;
begin
  {$ifdef FPC}
  result := 0;
  {$endif}
  raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
end;

procedure TElPublicKeyCrypto.WriteToOutput(Buffer: pointer;
   Size: integer);
var
  OldLen : integer;
begin
  if FOutputIsStream then
    FOutputStream.Write(Buffer^, Size)
  else
  begin
    OldLen := Length(FOutput);
    SetLength(FOutput, OldLen + Size);
    Move(Buffer^, FOutput[OldLen], Size);
  end;
end;

procedure TElPublicKeyCrypto.Reset;
begin
  SetLength(FOutput, 0);
  FOutputStream := nil;
  FOutputIsStream := false;
end;

procedure TElPublicKeyCrypto.Encrypt(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
  var OutSize: integer);
var
  Needed : integer;
begin  
  if not SupportsEncryption then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if (not Assigned(FKeyMaterial)) or (not FKeyMaterial.PublicKey) then
    raise EElPublicKeyCryptoError.Create(SPublicKeyNotFound);
  Reset;
  Needed := EstimateOutputSize(InBuffer, InSize,
    pkoEncrypt);
  if (Needed > OutSize) then
  begin
    if OutSize = 0 then
    begin
      OutSize := Needed;
      Exit;
    end
    else
      raise EElPublicKeyCryptoError.Create(SBufferTooSmall);
  end;
  EncryptInit();
  try
    EncryptUpdate(InBuffer, InSize);
  finally
    EncryptFinal();
  end;
  if Length(FOutput) > OutSize then
    raise EElPublicKeyCryptoError.Create(SInternalError);
  OutSize := Length(FOutput);
  Move(FOutput[0], OutBuffer^, OutSize);
end;

procedure TElPublicKeyCrypto.Decrypt(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
  var OutSize: integer);
var
  Needed : integer;
begin
  if not SupportsEncryption then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if (not Assigned(FKeyMaterial)) or (not FKeyMaterial.SecretKey) then
    raise EElPublicKeyCryptoError.Create(SSecretKeyNotFound);
  Reset;
  Needed := EstimateOutputSize(InBuffer, InSize,
    pkoDecrypt);
  if (Needed > OutSize) then
  begin
    if OutSize = 0 then
    begin
      OutSize := Needed;
      Exit;
    end
    else
      raise EElPublicKeyCryptoError.Create(SBufferTooSmall);
  end;
  DecryptInit();
  try
    DecryptUpdate(InBuffer, InSize);
  finally
    DecryptFinal();
  end;
  if Length(FOutput) > OutSize then
    raise EElPublicKeyCryptoError.Create(SInternalError);
  OutSize := Length(FOutput);
  Move(FOutput[0], OutBuffer^, OutSize);
end;

procedure TElPublicKeyCrypto.Sign(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
  var OutSize: integer);
var
  Needed : integer;
begin
  if not SupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if (not Assigned(FKeyMaterial)) or (not FKeyMaterial.SecretKey) then
    raise EElPublicKeyCryptoError.Create(SSecretKeyNotFound);
  Reset;
  Needed := EstimateOutputSize(InBuffer, InSize,
    pkoSign);
  if (Needed > OutSize) then
  begin
    if OutSize = 0 then
    begin
      OutSize := Needed;
      Exit;
    end
    else
      raise EElPublicKeyCryptoError.Create(SBufferTooSmall);
  end;
  SignInit(false);
  try
    SignUpdate(InBuffer, InSize);
  finally
    SignFinal();
  end;
  if Length(FOutput) > OutSize then
    raise EElPublicKeyCryptoError.Create(SInternalError);
  OutSize := Length(FOutput);
  Move(FOutput[0], OutBuffer^, OutSize);
end;

procedure TElPublicKeyCrypto.SignDetached(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
  var OutSize: integer);
var
  Needed : integer;
begin
  if not SupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if (not Assigned(FKeyMaterial)) or (not FKeyMaterial.SecretKey) then
    raise EElPublicKeyCryptoError.Create(SSecretKeyNotFound);
  Reset;
  Needed := EstimateOutputSize(InBuffer, InSize,
    pkoSignDetached);
  if (Needed > OutSize) then
  begin
    if OutSize = 0 then
    begin
      OutSize := Needed;
      Exit;
    end
    else
      raise EElPublicKeyCryptoError.Create(SBufferTooSmall);
  end;
  SignInit(true);
  try
    SignUpdate(InBuffer, InSize);
  finally
    SignFinal();
  end;
  if Length(FOutput) > OutSize then
    raise EElPublicKeyCryptoError.Create(SInternalError);
  OutSize := Length(FOutput);
  Move(FOutput[0], OutBuffer^, OutSize);
end;

function TElPublicKeyCrypto.Verify(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
  var OutSize: integer): TSBPublicKeyVerificationResult;
var
  Needed : integer;
begin
  if not SupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if (not Assigned(FKeyMaterial)) or (not FKeyMaterial.PublicKey) then
    raise EElPublicKeyCryptoError.Create(SPublicKeyNotFound);
  Reset;
  Needed := EstimateOutputSize(InBuffer, InSize, 
    pkoVerify);
  if (Needed > OutSize) then
  begin
    if OutSize = 0 then
    begin
      OutSize := Needed;
      Result := pkvrFailure;
      Exit;
    end
    else
      raise EElPublicKeyCryptoError.Create(SBufferTooSmall);
  end;
  VerifyInit(false, nil, 0);
  try
    VerifyUpdate(InBuffer, InSize);
  finally
    Result := VerifyFinal();
  end;
  if Length(FOutput) > OutSize then
    raise EElPublicKeyCryptoError.Create(SInternalError);
  OutSize := Length(FOutput);
  Move(FOutput[0], OutBuffer^, OutSize);
end;

function TElPublicKeyCrypto.VerifyDetached(InBuffer: pointer; InSize: integer;
  SigBuffer: pointer; SigSize: integer): TSBPublicKeyVerificationResult;
begin
  if not SupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if (not Assigned(FKeyMaterial)) or (not FKeyMaterial.PublicKey) then
    raise EElPublicKeyCryptoError.Create(SPublicKeyNotFound);
  VerifyInit(true, SigBuffer, SigSize);
  try
    VerifyUpdate(InBuffer, InSize);
  finally
    Result := VerifyFinal();
  end;
end;

procedure TElPublicKeyCrypto.Encrypt(InStream, OutStream : TStream;
  Count: integer {$ifdef HAS_DEF_PARAMS}= 0{$endif});
var
  Read : integer;
  Buf : array[0..4095] of byte;
begin
  if not SupportsEncryption then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if (not Assigned(FKeyMaterial)) or (not FKeyMaterial.PublicKey) then
    raise EElPublicKeyCryptoError.Create(SPublicKeyNotFound);
  Reset;
  if Count = 0 then
    Count := InStream.Size - InStream.Position;
  EstimateOutputSize(nil, Count, pkoEncrypt);
  FOutputStream := OutStream;
  FOutputIsStream := true;
  EncryptInit();
  try
    while Count > 0 do
    begin
      Read := InStream.Read(Buf[0], Min(Length(Buf), Count));
      EncryptUpdate(@Buf[0], Read);
      Dec(Count, Read);
    end;
  finally
    EncryptFinal();
  end;
end;

procedure TElPublicKeyCrypto.Decrypt(InStream, OutStream : TStream;
  Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif});
var
  Read : integer;
  Buf : array[0..4095] of byte;
begin
  if not SupportsEncryption then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if (FKeyMaterial = nil) or (not FKeyMaterial.SecretKey) then
    raise EElPublicKeyCryptoError.Create(SSecretKeyNotFound);
  Reset;
  if Count = 0 then
    Count := InStream.Size - InStream.Position;
  EstimateOutputSize(nil, Count, pkoDecrypt);
  FOutputStream := OutStream;
  FOutputIsStream := true;
  DecryptInit();
  try
    while Count > 0 do
    begin
      Read := InStream.Read(Buf[0], Min(Length(Buf), Count));
      DecryptUpdate(@Buf[0], Read);
      Dec(Count, Read);
    end;
  finally
    DecryptFinal();
  end;
end;

procedure TElPublicKeyCrypto.Sign(InStream, OutStream : TStream;
  Count: integer {$ifdef HAS_DEF_PARAMS}= 0{$endif});
var
  Read: integer;
  Buf: array[0..4095] of byte;
begin
  if not SupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if (FKeyMaterial = nil) or (not FKeyMaterial.SecretKey) then
    raise EElPublicKeyCryptoError.Create(SSecretKeyNotFound);
  Reset;
  if Count = 0 then
    Count := InStream.Size - InStream.Position;
  EstimateOutputSize(nil, Count, pkoSign);
  FOutputStream := OutStream;
  FOutputIsStream := true;
  SignInit(false);
  try
    while Count > 0 do
    begin
      Read := InStream.Read(Buf[0], Min(Length(Buf), Count));
      SignUpdate(@Buf[0], Read);
      Dec(Count, Read);
    end;
  finally
    SignFinal();
  end;
end;

procedure TElPublicKeyCrypto.SignDetached(InStream, OutStream : TStream;
  Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif});
var
  Read : integer;
  Buf : array[0..4095] of byte;
begin
  if not SupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if (FKeyMaterial = nil) or (not FKeyMaterial.SecretKey) then
    raise EElPublicKeyCryptoError.Create(SSecretKeyNotFound);
  Reset;
  if Count = 0 then
    Count := InStream.Size - InStream.Position;
  EstimateOutputSize(nil, Count, pkoSignDetached);
  FOutputStream := OutStream;
  FOutputIsStream := true;
  SignInit(true);
  try
    while Count > 0 do
    begin
      Read := InStream.Read(Buf[0], Min(Length(Buf), Count));
      SignUpdate(@Buf[0], Read);
      Dec(Count, Read);
    end;
  finally
    SignFinal();
  end;
end;

function TElPublicKeyCrypto.Verify(InStream, OutStream : TStream;
  Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}): TSBPublicKeyVerificationResult;
var
  Read : integer;
  Buf : array[0..4095] of byte;
begin
  if not SupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if (FKeyMaterial = nil) or (not FKeyMaterial.PublicKey) then
    raise EElPublicKeyCryptoError.Create(SPublicKeyNotFound);
  Reset;
  if Count = 0 then
    Count := InStream.Size - InStream.Position;
  EstimateOutputSize(nil, Count, pkoVerify);
  FOutputStream := OutStream;
  FOutputIsStream := true;
  VerifyInit(false, nil, 0);
  try
    while Count > 0 do
    begin
      Read := InStream.Read(Buf[0], Min(Length(Buf), Count));
      VerifyUpdate(@Buf[0], Read);
      Dec(Count, Read);
    end;
  finally
    Result := VerifyFinal();
  end;
end;

function TElPublicKeyCrypto.VerifyDetached(InStream, SigStream : TStream;
  InCount : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}; SigCount: integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}): TSBPublicKeyVerificationResult;
var
  Read : integer;
  Buf : array[0..4095] of byte;
  SigBuf : array of byte;
begin
  if not SupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if (FKeyMaterial = nil) or (not FKeyMaterial.PublicKey) then
    raise EElPublicKeyCryptoError.Create(SPublicKeyNotFound);
  Reset;
  if InCount = 0 then
    InCount := InStream.Size - InStream.Position;
  if SigCount = 0 then
    SigCount := SigStream.Size - SigStream.Position;
  SetLength(SigBuf, SigCount);
  SigStream.Read(SigBuf[0], SigCount);
  VerifyInit(true, @SigBuf[0], SigCount);
  try
    while InCount > 0 do
    begin
      Read := InStream.Read(Buf[0], Min(Length(Buf), InCount));
      VerifyUpdate(@Buf[0], Read);
      Dec(InCount, Read);
    end;
  finally
    Result := VerifyFinal();
  end;
end;

class function TElPublicKeyCrypto.GetName() : string;
begin
  Result := 'Empty';
end;

class function TElPublicKeyCrypto.GetDescription() : string;
begin
  Result := 'Base class for public key encryption. Do not instantiate.';
end;

procedure TElPublicKeyCrypto.SetKeyMaterial(Material : TElPublicKeyMaterial);
begin
  FKeyMaterial := Material;
end;

////////////////////////////////////////////////////////////////////////////////
// TElPublicKeyCryptoFactory class

constructor TElPublicKeyCryptoFactory.Create;
begin
  inherited;

  FRegisteredClasses := TList.Create;
  RegisterDefaultClasses;
end;

destructor TElPublicKeyCryptoFactory.Destroy;
begin
  FreeAndNil(FRegisteredClasses);
  inherited;
end;

procedure TElPublicKeyCryptoFactory.RegisterDefaultClasses;
begin
  RegisterClass(TElRSAPublicKeyCrypto);
  RegisterClass(TElDSAPublicKeyCrypto);
end;

procedure TElPublicKeyCryptoFactory.RegisterClass(Cls : TElPublicKeyCryptoClass);
begin
  FRegisteredClasses.Add((Cls));
end;

function TElPublicKeyCryptoFactory.CreateInstance(const OID : BufferType): TElPublicKeyCrypto;
var
  I : integer;
  Cls : TElPublicKeyCryptoClass;
begin
  Result := nil;
  for I := 0 to FRegisteredClasses.Count - 1 do
  begin
    Cls := TElPublicKeyCryptoClass(FRegisteredClasses[I]);
    if Cls.IsAlgorithmSupported(OID) then
    begin
      Result := Cls.Create;
      Break;
    end;
  end;
end;

function TElPublicKeyCryptoFactory.CreateInstance(Alg : integer): TElPublicKeyCrypto;
var
  I : integer;
  Cls : TElPublicKeyCryptoClass;
begin
  Result := nil;
  for I := 0 to FRegisteredClasses.Count - 1 do
  begin
    Cls := TElPublicKeyCryptoClass(FRegisteredClasses[I]);
    if Cls.IsAlgorithmSupported(Alg) then
    begin
      Result := Cls.Create;
      Break;
    end;
  end;
end;

function TElPublicKeyCryptoFactory.IsAlgorithmSupported(const OID : BufferType): boolean;
var
  I : integer;
begin
  Result := false;
  for I := 0 to FRegisteredClasses.Count - 1 do
  begin
    if TElPublicKeyCryptoClass(FRegisteredClasses[I]).IsAlgorithmSupported(OID) then
    begin
      Result := true;
      Break;
    end;
  end;
end;

function TElPublicKeyCryptoFactory.IsAlgorithmSupported(Alg : integer): boolean;
var
  I : integer;
begin
  Result := false;
  for I := 0 to FRegisteredClasses.Count - 1 do
  begin
    if TElPublicKeyCryptoClass(FRegisteredClasses[I]).IsAlgorithmSupported(Alg) then
    begin
      Result := true;
      Break;
    end;
  end;
end;

function TElPublicKeyCryptoFactory.GetRegisteredClass(Index: integer) : TElPublicKeyCryptoClass;
begin
  Result := TElPublicKeyCryptoClass(FRegisteredClasses[Index]);
end;

function TElPublicKeyCryptoFactory.GetRegisteredClassCount: integer;
begin
  Result := FRegisteredClasses.Count;
end;

////////////////////////////////////////////////////////////////////////////////
// TElRSAKeyMaterial

constructor TElRSAKeyMaterial.Create;
begin
  inherited;
  Reset;
  FPassphrase := '';
  FKeyFormat := rsaPKCS1;
  FCert := TElX509Certificate.Create(nil);
  FHashAlgorithm := SB_ALGORITHM_DGST_SHA1;
  FStrLabel := '';
  FSaltSize := 20;
end;

destructor TElRSAKeyMaterial.Destroy;
begin
  FreeAndNil(FCert);
  inherited;
end;

procedure TElRSAKeyMaterial.Generate(Bits : integer);
var
  MSize, ESize, DSize, BSize : integer;
begin
  Reset;
  MSize := 0;
  SBRSA.Generate(Bits, nil, MSize, nil, ESize, nil, DSize, nil, BSize);
  SetLength(FM, MSize);
  SetLength(FE, ESize);
  SetLength(FD, DSize);
  SetLength(FKeyBlob, BSize);
  if not SBRSA.Generate(Bits, @FM[0], MSize, @FE[0], ESize, @FD[0], DSize,
    @FKeyBlob[0], BSize) then
    raise EElPublicKeyCryptoError.Create(SInternalError);
  SetLength(FM, MSize);
  SetLength(FE, ESize);
  SetLength(FD, DSize);
  SetLength(FKeyBlob, BSize);
  TrimParams;
  FSecretKey := true;
  FPublicKey := true;
  RecalculatePublicKeyBlob;
  FKeyFormat := rsaPKCS1;
end;

procedure TElRSAKeyMaterial.LoadPublic(Buffer: pointer; Size: integer);
var
  PlainKey : pointer;
  PlainSize : integer;
  Header : string;
  R : integer;
  PMSize, PESize : integer;
  AlgID : BufferType;
  Decoded : ByteArray;
begin
  Reset;
  // checking if the key is PEM-enveloped
  if IsPEM(Buffer, Size) then
  begin
    PlainSize := 0;
    SBPEM.Decode(Buffer, Size, nil, FPassphrase, PlainSize, Header);
    SetLength(Decoded, PlainSize);
    R := SBPEM.Decode(Buffer, Size, @Decoded[0], FPassphrase, PlainSize, Header);
    PlainKey := @Decoded[0];
    if R <> 0 then
      raise EElPublicKeyCryptoError.Create(SInvalidPEM);
  end
  else
  begin
    PlainKey := Buffer;
    PlainSize := Size;
  end;
  // trying to load a key as plain RSA key
  PMSize := 0;
  SBRSA.DecodePublicKey(PlainKey, PlainSize, nil, PMSize, nil, PESize, AlgID);
  SetLength(FM, PMSize);
  SetLength(FE, PESize);
  if SBRSA.DecodePublicKey(PlainKey, PlainSize, @FM[0], PMSize, @FE[0], PESize, AlgID) then
  begin
    FPublicKey := true;
    SetLength(FPublicKeyBlob, PlainSize);
    Move(PlainKey^, FPublicKeyBlob[0], Length(FPublicKeyBlob));
    TrimParams;
  end
  else
    raise EElPublicKeyCryptoError.Create(SInvalidPublicKey);
end;

procedure TElRSAKeyMaterial.LoadSecret(Buffer: pointer; Size: integer);
var
  PlainKey : pointer;
  PlainSize : integer;
  Header : string;
  Decoded : ByteArray;
  R : integer;
  PMSize, PESize, PDSize : integer;
begin
  Reset;
  // checking if the key is PEM-enveloped
  if IsPEM(Buffer, Size) then
  begin
    PlainSize := 0;

    SBPEM.Decode(Buffer, Size, nil, FPassphrase, PlainSize, Header);
    SetLength(Decoded, PlainSize);
    R := SBPEM.Decode(Buffer, Size, @Decoded[0], FPassphrase, PlainSize, Header);
    if R <> 0 then
    begin
      case R of
        PEM_DECODE_RESULT_INVALID_PASSPHRASE :
          raise EElPublicKeyCryptoError.Create(SInvalidPassphrase);
        else
          raise EElPublicKeyCryptoError.Create(SInvalidPEM);
      end;
    end;
    PlainKey := @Decoded[0];
  end
  else
  begin
    PlainKey := Buffer;
    PlainSize := Size;
  end;
  // trying to load a key as plain RSA key
  PMSize := 0;
  SBRSA.DecodePrivateKey(PlainKey, PlainSize, nil, PMSize, nil, PESize, nil,
    PDSize);
  SetLength(FM, PMSize);
  SetLength(FE, PESize);
  SetLength(FD, PDSize);
  if SBRSA.DecodePrivateKey(PlainKey, PlainSize, @FM[0], PMSize, @FE[0], PESize,
    @FD[0], PDSize) then
  begin
    FPublicKey := true;
    FSecretKey := true;
    SetLength(FKeyBlob, PlainSize);
    Move(PlainKey^, FKeyBlob[0], Length(FKeyBlob));
    TrimParams;
    RecalculatePublicKeyBlob;
  end
  else
    raise EElPublicKeyCryptoError.Create(SInvalidSecretKey);
end;

procedure TElRSAKeyMaterial.SavePublic(Buffer: pointer; var Size: integer);
var
  OutSize : integer;
begin
  if PEMEncode then
  begin
    OutSize := 0;
    SBPEM.Encode(@FPublicKeyBlob[0], Length(FPublicKeyBlob), nil, OutSize,
      'PUBLIC KEY', false, '');
    if OutSize <= Size then
    begin
      if not SBPEM.Encode(@FPublicKeyBlob[0], Length(FPublicKeyBlob), Buffer, Size,
        'PUBLIC KEY', false, '') then
        raise EElPublicKeyCryptoError.Create(SPEMWriteError);
    end
    else if Size = 0 then
      Size := OutSize
    else
      raise EElPublicKeyCryptoError.Create(SBufferTooSmall);
  end
  else
  begin
    if Size = 0 then
      Size := Length(FPublicKeyBlob)
    else if Size < Length(FPublicKeyBlob) then
      raise EElPublicKeyCryptoError.Create(SBufferTooSmall)
    else
    begin
      Size := Length(FPublicKeyBlob);
      Move(FPublicKeyBlob[0], Buffer^, Size);
    end;
  end;
end;

procedure TElRSAKeyMaterial.SaveSecret(Buffer: pointer; var Size: integer);
var
  OutSize : integer;
  Encrypt : boolean;
begin
  if PEMEncode then
  begin
    OutSize := 0;
    Encrypt := Length(FPassphrase) > 0;

    SBPEM.Encode(@FKeyBlob[0], Length(FKeyBlob), nil, OutSize,
      'RSA PRIVATE KEY', Encrypt, FPassphrase);

    if OutSize <= Size then
    begin
      if not SBPEM.Encode(@FKeyBlob[0], Length(FKeyBlob), Buffer, Size,
        'RSA PRIVATE KEY', Encrypt, FPassphrase) then
        raise EElPublicKeyCryptoError.Create(SPEMWriteError);
    end
    else if Size = 0 then
      Size := OutSize
    else
      raise EElPublicKeyCryptoError.Create(SBufferTooSmall);
  end
  else
  begin
    if Size = 0 then
      Size := Length(FKeyBlob)
    else if Size < Length(FKeyBlob) then
      raise EElPublicKeyCryptoError.Create(SBufferTooSmall)
    else
    begin
      Size := Length(FKeyBlob);
      Move(FKeyBlob[0], Buffer^, Size);
    end
  end;
end;

function TElRSAKeyMaterial.GetValid : boolean;
begin
  if FSecretKey then
    Result := SBRSA.IsValidKey(@FKeyBlob[0], Length(FKeyBlob))
  else
    Result := true;
end;

procedure TElRSAKeyMaterial.Reset;
begin
  FPublicKey := false;
  FSecretKey := false;
  SetLength(FKeyBlob, 0);
  SetLength(FPublicKeyBlob, 0);
  SetLength(FM, 0);
  SetLength(FE, 0);
  SetLength(FD, 0);
  FPEMEncode := false;
  FKeyFormat := rsaPKCS1;
  FHashAlgorithm := SB_ALGORITHM_DGST_SHA1;
  FStrLabel := '';
  FSaltSize := 20;  
end;

procedure TElRSAKeyMaterial.RecalculatePublicKeyBlob;
var
  OutSize : integer;
begin
  OutSize := 0;
  SBRSA.EncodePublicKey(@FM[0], Length(FM), @FE[0], Length(FE), SB_OID_RSAENCRYPTION,
    nil, OutSize);
  SetLength(FPublicKeyBlob, OutSize);
  if not SBRSA.EncodePublicKey(@FM[0], Length(FM), @FE[0], Length(FE), SB_OID_RSAENCRYPTION,
    @FPublicKeyBlob[0], OutSize) then
    raise EElPublicKeyCryptoError.Create(SInternalError);
  SetLength(FPublicKeyBlob, OutSize);
end;

procedure TElRSAKeyMaterial.TrimParams;
begin
  FM := TrimParam(FM);
  FE := TrimParam(FE);
  if Length(FD) > 0 then
    FD := TrimParam(FD);
end;

procedure TElRSAKeyMaterial.Import(Cert: TElX509Certificate);
begin
  if (Cert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or
    (Cert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) or
    (Cert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) then
  begin
    Cert.Clone(FCert, true);
    ExtractKeyMaterialFromX509;
    FKeyFormat := rsaX509;
  end
  else
    raise EElPublicKeyCryptoError.Create(SUnsupportedCertType);
end;

procedure TElRSAKeyMaterial.ExtractKeyMaterialFromX509;
var
  MSize, ESize: integer;
  Sz : word;
  Buf : ByteArray;
begin
  Reset;
  if (FCert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION) or
    (FCert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP) or
    (FCert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS) then
  begin
    if FCert.PrivateKeyExists and FCert.PrivateKeyExtractable then
    begin
      Sz := 0;
      FCert.SaveKeyToBuffer(nil, Sz);
      SetLength(Buf, Sz);
      FCert.SaveKeyToBuffer(@Buf[0], Sz);
      LoadSecret(@Buf[0], Sz);
    end
    else
    begin
      MSize := 0;
      FCert.GetRSAParams(nil, MSize, nil, ESize);
      SetLength(FM, MSize);
      SetLength(FE, ESize);
      FCert.GetRSAParams(@FM[0], MSize, @FE[0], ESize);
      SetLength(FM, MSize);
      SetLength(FE, ESize);
      FPublicKey := true;
      RecalculatePublicKeyBlob;
    end;

    if FCert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAOAEP then
    begin
      FStrLabel := FCert.OAEPParams.StrLabel;
      FHashAlgorithm := FCert.OAEPParams.HashAlgorithm;
    end
    else if FCert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSAPSS then
    begin
      FSaltSize := FCert.PSSParams.SaltSize;
      FHashAlgorithm := FCert.PSSParams.HashAlgorithm;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TElRSAPublicKeyCrypto class

constructor TElRSAPublicKeyCrypto.Create(const OID : BufferType);
begin
  inherited Create;
  FOID := OID;
  if CompareContent(OID, SB_OID_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_SHA1_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_MD2_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_MD5_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_SHA1_RSA) or
    CompareContent(OID, SB_OID_SHA224_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_SHA256_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_SHA384_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_SHA512_RSAENCRYPTION) then
  begin
    FSupportsSigning := true;
    FSupportsEncryption := true;
    FCryptoType := rsapktPKCS1;
  end
  else
  if CompareContent(OID, SB_OID_RSAPSS) then
  begin
    FSupportsSigning := true;
    FSupportsEncryption := false;
    FCryptoType := rsapktPSS;
  end
  else
  if CompareContent(OID, SB_OID_RSAOAEP) then
  begin
    FSupportsSigning := false;
    FSupportsEncryption := true;
    FCryptoType := rsapktOAEP;
  end
  else
    raise EElPublicKeyCryptoError.CreateFmt(SUnsupportedAlgorithm, [OIDToStr(OID)]);
end;

constructor TElRSAPublicKeyCrypto.Create(Alg : integer);
begin
  inherited Create;
  FOID := EmptyBuffer;
  if Alg in [SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION] then
  begin
    FSupportsSigning := true;
    FSupportsEncryption := true;
    FOID := GetOIDByPKAlgorithm(Alg);
    if Length(FOID) = 0 then
      FOID := GetOIDBySigAlgorithm(Alg);
    FCryptoType := rsapktPKCS1;  
  end
  else if Alg = SB_CERT_ALGORITHM_ID_RSAPSS then
  begin
    FSupportsSigning := true;
    FSupportsEncryption := false;
    FOID := SB_OID_RSAPSS;
    FCryptoType := rsapktPSS;
  end
  else if Alg = SB_CERT_ALGORITHM_ID_RSAOAEP then
  begin
    FSupportsSigning := false;
    FSupportsEncryption := true;
    FOID := SB_OID_RSAOAEP;
    FCryptoType := rsapktOAEP;
  end;

  if CompareContent(FOID, EmptyBuffer) then
    raise EElPublicKeyCryptoError.CreateFmt(SUnsupportedAlgorithmInt, [Alg]);
end;


constructor TElRSAPublicKeyCrypto.Create;
begin
  Create(SB_OID_RSAENCRYPTION);
end;

procedure TElRSAPublicKeyCrypto.Reset;
begin
  FSupportsEncryption := false;
  FSupportsSigning := false;
  FInputIsHash := false;
  FUseAlgorithmPrefix := true;
  FCryptoType := rsapktPKCS1;
  FInputEncoding := rsapkeBinary;
  FOutputEncoding := rsapkeBinary;
end;

destructor TElRSAPublicKeyCrypto.Destroy;
begin
  inherited;
end;

function TElRSAPublicKeyCrypto.GetSupportsEncryption: boolean;
begin
  Result := FSupportsEncryption;
end;

function TElRSAPublicKeyCrypto.GetSupportsSigning: boolean;
begin
  Result := FSupportsSigning;
end;

procedure TElRSAPublicKeyCrypto.SetKeyMaterial(Material : TElPublicKeyMaterial);
begin
  if not (Material is TElRSAKeyMaterial) then
    raise EElPublicKeyCryptoError.Create(SBadKeyMaterial)
  else
    FKeyMaterial := Material;
end;

procedure TElRSAPublicKeyCrypto.SetCryptoType(Value : TSBRSAPublicKeyCryptoType);
begin
  FCryptoType := Value;

  FSupportsEncryption := true;
  FSupportsSigning := true;

  if Value = rsapktPSS then
    FSupportsEncryption := false
  else if Value = rsapktOAEP then
    FSupportsSigning := false;
end;


procedure TElRSAPublicKeyCrypto.SignInit(Detached: boolean);
begin
  if not FSupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if not (FKeyMaterial is TElRSAKeyMaterial) then
    raise EElPublicKeyCryptoError.Create(SBadKeyMaterial);

  SetLength(FSpool, 0);
  if not FInputIsHash then
    FHashFunction := TElHashFunction.Create(GetUsedHashFunction);
end;

procedure TElRSAPublicKeyCrypto.SignUpdate(Buffer: pointer; Size: integer);
var
  OldLen : integer;
begin
  if not FSupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);

  if FInputIsHash then
  begin
    OldLen := Length(FSpool);
    SetLength(FSpool, OldLen + Size);
    Move(Buffer^, FSpool[OldLen], Size);
  end
  else
    FHashFunction.Update(Buffer, Size);
end;

procedure TElRSAPublicKeyCrypto.SignFinal;
var
  Hash : BufferType;
  KM : TElRSAKeyMaterial;
  SigSize : integer;
  Sig : ByteArray;
begin
  if not FSupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);

  KM := TElRSAKeyMaterial(FKeyMaterial);
  if FInputIsHash then
  begin
    SetLength(Hash, Length(FSpool));
    Move(FSpool[0], Hash[1], Length(Hash));
  end
  else
  begin
    Hash := FHashFunction.Finish;
    FreeAndNil(FHashFunction);
  end;

  if FCryptoType = rsapktPSS then
  begin
    if not ((KM.KeyFormat in [rsaPKCS1, rsaPSS]) or 
            ((KM.KeyFormat = rsaX509) and
     (KM.FCert.PublicKeyAlgorithm in [SB_CERT_ALGORITHM_ID_RSAPSS, SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION])))
    then
      raise EElPublicKeyCryptoError.Create(SInvalidKeyMaterialType);

    SigSize := 0;
    SBRSA.SignPSS(@Hash[1], Length(Hash), KM.HashAlgorithm, KM.SaltSize,
      @KM.FKeyBlob[0], Length(KM.FKeyBlob), nil, SigSize);
    SetLength(Sig, SigSize);
    if not SBRSA.SignPSS(@Hash[1], Length(Hash), KM.HashAlgorithm, KM.SaltSize,
      @KM.FKeyBlob[0], Length(KM.FKeyBlob), @Sig[0], SigSize)
    then
      raise EElPublicKeyCryptoError.Create(SSigningFailed);
    WriteToOutput(@Sig[0], SigSize);
  end
  else
  begin
    if not ((KM.KeyFormat = rsaPKCS1) or 
            ((KM.KeyFormat = rsaX509) and
     (KM.FCert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION)))
    then
      raise EElPublicKeyCryptoError.Create(SInvalidKeyMaterialType);

    if FUseAlgorithmPrefix then
      Hash := AddAlgorithmPrefix(Hash);
      
    SigSize := 0;

    SBRSA.Sign(@Hash[1], Length(Hash), @KM.FKeyBlob[0], Length(KM.FKeyBlob), nil,
      SigSize);
    SetLength(Sig, SigSize);
    if not SBRSA.Sign(@Hash[1], Length(Hash), @KM.FKeyBlob[0], Length(KM.FKeyBlob),
      @Sig[0], SigSize) then
      raise EElPublicKeyCryptoError.Create(SSigningFailed);
    WriteToOutput(@Sig[0], SigSize);
  end;
end;

procedure TElRSAPublicKeyCrypto.EncryptInit;
begin
  if not (FKeyMaterial is TElRSAKeyMaterial) then
    raise EElPublicKeyCryptoError.Create(SBadKeyMaterial);
  if not FSupportsEncryption then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);

  SetLength(FSpool, 0);
end;

procedure TElRSAPublicKeyCrypto.EncryptUpdate(Buffer: pointer; Size: integer);
var
  OldLen: integer;
begin
  if not FSupportsEncryption then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);

  OldLen := Length(FSpool);
  SetLength(FSpool, OldLen + Size);
  Move(Buffer^, FSpool[OldLen], Size);
end;

procedure TElRSAPublicKeyCrypto.EncryptFinal;
var
  KM : TElRSAKeyMaterial;
  OutSize : integer;
  OutBuf : ByteArray;
  LabelPtr : pointer;
begin
  if not FSupportsEncryption then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);

  KM := TElRSAKeyMaterial(FKeyMaterial);

  if FCryptoType = rsapktOAEP then
  begin
    if not ((KM.KeyFormat in [rsaPKCS1, rsaOAEP]) or 
            ((KM.KeyFormat = rsaX509) and
     (KM.FCert.PublicKeyAlgorithm in [SB_CERT_ALGORITHM_ID_RSAOAEP, SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION])))
    then
      raise EElPublicKeyCryptoError.Create(SInvalidKeyMaterialType);

    OutSize := 0;

    if Length(KM.FStrLabel) = 0 then
      LabelPtr := nil
    else
      LabelPtr := @KM.FStrLabel[1];

    SBRSA.EncryptOAEP(@FSpool[0], Length(FSpool), @KM.FM[0], Length(KM.FM),
      @KM.FE[0], Length(KM.FE), LabelPtr, Length(KM.FStrLabel), KM.HashAlgorithm, nil, OutSize);
    SetLength(OutBuf, OutSize);
    if not SBRSA.EncryptOAEP(@FSpool[0], Length(FSpool), @KM.FM[0], Length(KM.FM),
      @KM.FE[0], Length(KM.FE), LabelPtr, Length(KM.FStrLabel), KM.HashAlgorithm, @OutBuf[0], OutSize)
    then
      raise EElPublicKeyCryptoError.Create(SEncryptionFailed);
    WriteToOutput(@OutBuf[0], OutSize);
  end
  else
  begin
    if not ((KM.KeyFormat = rsaPKCS1) or 
            ((KM.KeyFormat = rsaX509) and
     (KM.FCert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION)))
    then
      raise EElPublicKeyCryptoError.Create(SInvalidKeyMaterialType);

    OutSize := 0;
    SBRSA.Encrypt(@FSpool[0], Length(FSpool), @KM.FM[0], Length(KM.FM),
      @KM.FE[0], Length(KM.FE), nil, OutSize);
    SetLength(OutBuf, OutSize);
    if not SBRSA.Encrypt(@FSpool[0], Length(FSpool), @KM.FM[0], Length(KM.FM),
      @KM.FE[0], Length(KM.FE), @OutBuf[0], OutSize) then
      raise EElPublicKeyCryptoError.Create(SEncryptionFailed);
    WriteToOutput(@OutBuf[0], OutSize);
  end;
end;

procedure TElRSAPublicKeyCrypto.DecryptInit;
begin
  if not FSupportsEncryption then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if not (FKeyMaterial is TElRSAKeyMaterial) then
    raise EElPublicKeyCryptoError.Create(SBadKeyMaterial);
  SetLength(FSpool, 0);
end;

procedure TElRSAPublicKeyCrypto.DecryptUpdate(Buffer: pointer; Size: integer);
var
  OldLen: integer;
begin
  if not FSupportsEncryption then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);

  OldLen := Length(FSpool);
  SetLength(FSpool, OldLen + Size);
  Move(Buffer^, FSpool[OldLen], Size);
end;

procedure TElRSAPublicKeyCrypto.DecryptFinal;
var
  KM : TElRSAKeyMaterial;
  OutSize : integer;
  OutBuf : ByteArray;
  RealPtr, LabelPtr : pointer;
  RealSize : integer;
  Buf : ByteArray;
begin
  if not FSupportsEncryption then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);

  KM := TElRSAKeyMaterial(FKeyMaterial);

  if InputEncoding = rsapkeBase64 then
  begin
    RealSize := 0;
    Base64Decode(@FSpool[0], Length(FSpool), nil, RealSize);
    SetLength(Buf, RealSize);

    if Base64Decode(@FSpool[0], Length(FSpool), @Buf[0], RealSize) <> 0 then
      raise EElPublicKeyCryptoError.Create(SInvalidEncoding);
    RealPtr := @Buf[0];
  end
  else
  begin
    RealPtr := @FSpool[0];
    RealSize := Length(FSpool);
  end;

  if FCryptoType = rsapktOAEP then
  begin
    if not ((KM.KeyFormat = rsaPKCS1) or 
            ((KM.KeyFormat = rsaX509) and
     (KM.FCert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION)))
    then
      raise EElPublicKeyCryptoError.Create(SInvalidKeyMaterialType);

    if not ((KM.KeyFormat in [rsaPKCS1, rsaOAEP]) or 
            ((KM.KeyFormat = rsaX509) and
     (KM.FCert.PublicKeyAlgorithm in [SB_CERT_ALGORITHM_ID_RSAOAEP, SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION])))
    then
      raise EElPublicKeyCryptoError.Create(SInvalidKeyMaterialType);

    OutSize := 0;

    if Length(KM.FStrLabel) = 0 then
      LabelPtr := nil
    else
      LabelPtr := @KM.FStrLabel[1];

    SBRSA.DecryptOAEP(RealPtr, RealSize, @KM.FM[0], Length(KM.FM), @KM.FD[0],
      Length(KM.FD), LabelPtr, Length(KM.FStrLabel), KM.HashAlgorithm, nil, OutSize);
    SetLength(OutBuf, OutSize);
    if not SBRSA.DecryptOAEP(RealPtr, RealSize, @KM.FM[0], Length(KM.FM),
      @KM.FD[0], Length(KM.FD), LabelPtr, Length(KM.FStrLabel), KM.HashAlgorithm, @OutBuf[0], OutSize)
    then
      raise EElPublicKeyCryptoError.Create(SEncryptionFailed);
    WriteToOutput(@OutBuf[0], OutSize);
  end
  else
  begin
    if not ((KM.KeyFormat = rsaPKCS1) or 
            ((KM.KeyFormat = rsaX509) and
     (KM.FCert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION)))
    then
      raise EElPublicKeyCryptoError.Create(SInvalidKeyMaterialType);

    OutSize := 0;
    SBRSA.Decrypt(RealPtr, RealSize, @KM.FKeyBlob[0], Length(KM.FKeyBlob),
      nil, OutSize);
    SetLength(OutBuf, OutSize);
    if not SBRSA.Decrypt(RealPtr, RealSize, @KM.FKeyBlob[0], Length(KM.FKeyBlob),
      @OutBuf[0], OutSize) then
      raise EElPublicKeyCryptoError.Create(SDecryptionFailed);
    WriteToOutput(@OutBuf[0], OutSize);
  end;
end;

procedure TElRSAPublicKeyCrypto.VerifyInit(Detached: boolean; Signature: pointer;
  SigSize: integer);
var
  KM : TElRSAKeyMaterial;
  HashSize : integer;
  Hash, HashAlg, HashPar : BufferType;
  RealSigPtr : pointer;
  RealSigSize : integer;
  Buf : ByteArray;
begin
  if not (FKeyMaterial is TElRSAKeyMaterial) then
    raise EElPublicKeyCryptoError.Create(SBadKeyMaterial);
  if not FSupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);


  KM := TElRSAKeyMaterial(FKeyMaterial);
  SetLength(FSpool, 0);
  if CryptoType = rsapktPKCS1 then
  begin
    if not ((KM.KeyFormat = rsaPKCS1) or 
            ((KM.KeyFormat = rsaX509) and
     (KM.FCert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION)))
    then
      raise EElPublicKeyCryptoError.Create(SInvalidKeyMaterialType);

    if InputEncoding = rsapkeBase64 then
    begin
      RealSigSize := 0;
      Base64Decode(Signature, SigSize, nil, RealSigSize);
      SetLength(Buf, RealSigSize);

      if Base64Decode(Signature, SigSize, @Buf[0], RealSigSize) <> 0 then
        raise EElPublicKeyCryptoError.Create(SInvalidSignatureEncoding);
      RealSigPtr := @Buf[0];
    end
    else
    begin
      RealSigPtr := Signature;
      RealSigSize := SigSize;
    end;
    HashSize := 0;
    SBRSA.Decrypt(RealSigPtr, RealSigSize, @KM.FM[0], Length(KM.FM), @KM.FE[0],
      Length(KM.FE), nil, HashSize);
    SetLength(Hash, HashSize);
    if SBRSA.Decrypt(RealSigPtr, RealSigSize, @KM.FM[0], Length(KM.FM), @KM.FE[0],
      Length(KM.FE), @Hash[1], HashSize) then
    begin
      SetLength(Hash, HashSize);
      if UseAlgorithmPrefix then
        Hash := RemoveAlgorithmPrefix(Hash, HashAlg, HashPar)
      else
        HashAlg := GetUsedHashFunctionOID;
        
      SetLength(FSignature, Length(Hash));
      Move(Hash[1], FSignature[0], Length(Hash));
    end
    else
      SetLength(FSignature, 0);
  end
  else
  if CryptoType = rsapktPSS then
  begin
    if not ((KM.KeyFormat in [rsaPKCS1, rsaPSS]) or 
            ((KM.KeyFormat = rsaX509) and
     (KM.FCert.PublicKeyAlgorithm in [SB_CERT_ALGORITHM_ID_RSAPSS, SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION])))
    then
      raise EElPublicKeyCryptoError.Create(SInvalidKeyMaterialType);

    SetLength(FSignature, SigSize);
    Move(Signature^, FSignature[0], SigSize);
    HashAlg := GetUsedHashFunctionOID;  
  end
  else
    raise EElPublicKeyCryptoError.Create(SUnsupportedEncryptionType);
  if not FInputIsHash then
    FHashFunction := TElHashFunction.Create(HashAlg);
end;

procedure TElRSAPublicKeyCrypto.VerifyUpdate(Buffer: pointer; Size: integer);
var
  OldLen : integer;
begin
  if FInputIsHash then
  begin
    OldLen := Length(FSpool);
    SetLength(FSpool, OldLen + Size);
    Move(Buffer^, FSpool[OldLen], Size);
  end
  else
    FHashFunction.Update(Buffer, Size);
end;

function TElRSAPublicKeyCrypto.VerifyFinal : TSBPublicKeyVerificationResult;
var
  Hash : BufferType;
  KM : TElRSAKeyMaterial;
begin
  if not FSupportsSigning then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation);
  if not (FKeyMaterial is TElRSAKeyMaterial) then
    raise EElPublicKeyCryptoError.Create(SBadKeyMaterial);

  if not FInputIsHash then
  begin
    Hash := FHashFunction.Finish;
    FreeAndNil(FHashFunction);
  end
  else
  begin
    SetLength(Hash, Length(FSpool));
    Move(FSpool[0], Hash[1], Length(FSpool));
  end;

  KM := TElRSAKeyMaterial(FKeyMaterial);

  if CryptoType = rsapktPKCS1 then
  begin
    if not ((KM.KeyFormat = rsaPKCS1) or  
            ((KM.KeyFormat = rsaX509) and
     (KM.FCert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION)))
    then
      raise EElPublicKeyCryptoError.Create(SInvalidKeyMaterialType);

    if (Length(Hash) = Length(FSignature)) and
      (CompareMem(@Hash[1], @FSignature[0], Length(Hash)))
    then
      Result := pkvrSuccess
    else
      Result := pkvrInvalidSignature;
  end
  else
  if CryptoType = rsapktPSS then
  begin
    if not ((KM.KeyFormat in [rsaPKCS1, rsaPSS]) or 
            ((KM.KeyFormat = rsaX509) and
     (KM.FCert.PublicKeyAlgorithm in [SB_CERT_ALGORITHM_ID_RSAPSS, SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION])))
    then
      raise EElPublicKeyCryptoError.Create(SInvalidKeyMaterialType);

    if SBRSA.VerifyPSS(@Hash[1], Length(Hash), KM.HashAlgorithm, KM.SaltSize,
      @KM.FM[0], Length(KM.FM), @KM.FE[0], Length(KM.FE), @FSignature[0], Length(FSignature))
    then
      Result := pkvrSuccess
    else
      Result := pkvrInvalidSignature;
  end
  else
    raise EElPublicKeyCryptoError.Create(SUnsupportedAlgorithm);
end;

function TElRSAPublicKeyCrypto.EstimateOutputSize(InBuffer: pointer; InSize: integer;
  Operation : TSBPublicKeyOperation): integer;
begin
  if (Operation in [pkoVerify, pkoSign]) and 
     (CryptoType in [rsapktPKCS1, rsapktPSS]) then
    raise EElPublicKeyCryptoError.Create(SOnlyDetachedSigningSupported);
  if not (KeyMaterial is TElRSAKeyMaterial) then
    raise EElPublicKeyCryptoError.Create(SBadKeyMaterial); 
  if (Operation in [pkoEncrypt, pkoDecrypt]) and
    (InSize > Length(TElRSAKeyMaterial(KeyMaterial).FM)) then
    raise EElPublicKeyCryptoError.Create(SInputTooLong);
  if (Operation = pkoSign) and (InputIsHash) and
    (InSize > Length(TElRSAKeyMaterial(KeyMaterial).FM) - 11) then
    raise EElPublicKeyCryptoError.Create(SInputTooLong);
  Result := Length(TElRSAKeyMaterial(KeyMaterial).FM);
end;

class function TElRSAPublicKeyCrypto.IsAlgorithmSupported(Alg: integer): boolean;
begin
  Result := Alg in [
    SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA224_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA256_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA384_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA512_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_ID_RSAPSS,
    SB_CERT_ALGORITHM_ID_RSAOAEP
  ];
end;

class function TElRSAPublicKeyCrypto.IsAlgorithmSupported(const OID: BufferType): boolean;
begin
  Result := CompareContent(OID, SB_OID_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_MD2_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_MD5_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_SHA1_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_SHA224_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_SHA256_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_SHA384_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_SHA512_RSAENCRYPTION) or
    CompareContent(OID, SB_OID_RSAPSS) or
    CompareContent(OID, SB_OID_RSAOAEP);
end;

function TElRSAPublicKeyCrypto.GetUsedHashFunction: integer;
begin
  if CompareContent(FOID, SB_OID_RSAENCRYPTION) then
    Result := SB_ALGORITHM_DGST_SHA1
  else if CompareContent(FOID, SB_OID_MD2_RSAENCRYPTION) then
    Result := SB_ALGORITHM_DGST_MD2
  else if CompareContent(FOID, SB_OID_MD5_RSAENCRYPTION) then
    Result := SB_ALGORITHM_DGST_MD5
  else if CompareContent(FOID, SB_OID_SHA1_RSAENCRYPTION) then
    Result := SB_ALGORITHM_DGST_SHA1
  else if CompareContent(FOID, SB_OID_SHA224_RSAENCRYPTION) then
    Result := SB_ALGORITHM_DGST_SHA224
  else if CompareContent(FOID, SB_OID_SHA256_RSAENCRYPTION) then
    Result := SB_ALGORITHM_DGST_SHA256
  else if CompareContent(FOID, SB_OID_SHA384_RSAENCRYPTION) then
    Result := SB_ALGORITHM_DGST_SHA384
  else if CompareContent(FOID, SB_OID_SHA512_RSAENCRYPTION) then
    Result := SB_ALGORITHM_DGST_SHA512
  else if CompareContent(FOID, SB_OID_RSAPSS) or CompareContent(FOID, SB_OID_RSAOAEP) then
  begin
    if Assigned(FKeyMaterial) and (FKeyMaterial is TElRSAKeyMaterial)
    then
      Result := TElRSAKeyMaterial(FKeyMaterial).HashAlgorithm
    else
      Result := SB_ALGORITHM_DGST_SHA1;
  end
  else
    Result := SB_ALGORITHM_UNKNOWN;
end;

function TElRSAPublicKeyCrypto.GetUsedHashFunctionOID: BufferType;
begin
  Result := GetOIDByHashAlgorithm(GetUsedHashFunction);
end;

function TElRSAPublicKeyCrypto.AddAlgorithmPrefix(const Hash: BufferType): BufferType;
var
  HashFuncOID : BufferType;
begin
  HashFuncOID := GetUsedHashFunctionOID;
  Result := #$06 + Chr(Length(HashFuncOID)) + HashFuncOID; // HashFunction: OID
  Result := Result + #$05#$00; // HashFunction: params
  Result := #$30 + Chr(Length(Result)) + Result; // AlgorithmIdentifier SEQUENCE
  Result := Result + #$04 + Chr(Length(Hash)) + Hash; // adding hash value
  Result := #$30 + Chr(Length(Result)) + Result; // the outmost SEQUENCE
end;

function TElRSAPublicKeyCrypto.RemoveAlgorithmPrefix(const Value: BufferType;
  var HashAlg : BufferType; var HashPar : BufferType): BufferType;
var
  Tag, TagSeq : TElASN1ConstrainedTag;
  Processed : boolean;
  Size : integer;
begin
  Processed := false;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        TagSeq := TElASN1ConstrainedTag(Tag.GetField(0));
        if (TagSeq.Count = 2) and (TagSeq.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) and
          (TagSeq.GetField(1).CheckType(SB_ASN1_OCTETSTRING, false)) then
        begin
          Result := TElASN1SimpleTag(TagSeq.GetField(1)).Content;
          TagSeq := TElASN1ConstrainedTag(TagSeq.GetField(0));
          if (TagSeq.Count = 2) and (TagSeq.GetField(0).CheckType(SB_ASN1_OBJECT, false)) then
          begin
            HashAlg := TElASN1SimpleTag(TagSeq.GetField(0)).Content;
            Size := 0;
            TagSeq.GetField(1).SaveToBuffer(nil, Size);
            SetLength(HashPar, Size);
            TagSeq.GetField(1).SaveToBuffer(@HashPar[1], Size);
            SetLength(HashPar, Size);
            Processed := true;
          end;
        end;
      end;
    end;
  finally
    FreeAndNil(Tag);
  end;
  if not Processed then
    raise EElPublicKeyCryptoError.Create(SBadSignatureFormatting);
end;

class function TElRSAPublicKeyCrypto.GetName() : string;
begin
  Result := 'RSA';
end;

class function TElRSAPublicKeyCrypto.GetDescription() : string;
begin
  Result := 'Implements base RSA encrypting and signing functionality';
end;

procedure TElRSAPublicKeyCrypto.WriteToOutput(Buffer: pointer; Size: integer);
var
  OutSize : integer;
  Buf : ByteArray;
begin
  if FOutputEncoding = rsapkeBinary then
    inherited WriteToOutput(Buffer, Size)
  else if FOutputEncoding = rsapkeBase64 then
  begin
    OutSize := 0;
    Base64Encode(Buffer, Size, nil, OutSize, false);
    SetLength(Buf, OutSize);
    Base64Encode(Buffer, Size, @Buf[0], OutSize, false);
    inherited WriteToOutput(@Buf[0], OutSize);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//  TElDSAKeyMaterial class

constructor TElDSAKeyMaterial.Create;
begin
  inherited;

  FKeyFormat := dsaFIPS;
  FStrictKeyValidation := false;
  FPassphrase := '';
  FCert := TElX509Certificate.Create(nil);
end;

destructor TElDSAKeyMaterial.Destroy;
begin
  FreeAndNil(FCert);

  inherited;
end;

procedure TElDSAKeyMaterial.Reset;
begin
  FPublicKey := false;
  FSecretKey := false;
  SetLength(FKeyBlob, 0);
  SetLength(FP, 0);
  SetLength(FQ, 0);
  SetLength(FG, 0);
  SetLength(FY, 0);
  SetLength(FX, 0);
  FPEMEncode := false;
end;

procedure TElDSAKeyMaterial.TrimParams;
begin
  FP := TrimParam(FP);
  FQ := TrimParam(FQ);
  FG := TrimParam(FG);
  FY := TrimParam(FY);
  if Length(FX) > 0 then
    FX := TrimParam(FX);
end;

procedure TElDSAKeyMaterial.ExtractKeyMaterialFromX509;
var
  PSize, QSize, GSize, YSize: integer;
  Sz : word;
  Buf : ByteArray;
begin
  Reset;
  if FCert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    if FCert.PrivateKeyExists and FCert.PrivateKeyExtractable then
    begin
      Sz := 0;
      FCert.SaveKeyToBuffer(nil, Sz);
      SetLength(Buf, Sz);
      FCert.SaveKeyToBuffer(@Buf[0], Sz);
      LoadSecret(@Buf[0], Sz);
    end
    else
    begin
      PSize := 0;
      FCert.GetDSSParams(nil, PSize, nil, QSize, nil, GSize, nil, YSize);
      SetLength(FP, PSize);
      SetLength(FQ, QSize);
      SetLength(FG, GSize);
      SetLength(FY, YSize);
      FCert.GetDSSParams(@FP[0], PSize, @FQ[0], QSize, @FG[0], GSize, @FY[0], YSize);
      SetLength(FP, PSize);
      SetLength(FQ, QSize);
      SetLength(FG, GSize);
      SetLength(FY, YSize);
      FPublicKey := true;
    end;
  end;
end;

function TElDSAKeyMaterial.GetValid : boolean;
begin
  if FSecretKey then
    Result := SBDSA.IsValidKey(@FP[0], Length(FP), @FQ[0], Length(FQ), @FG[0],
    Length(FG), @FY[0], Length(FY), @FX[0], Length(FX), true, FStrictKeyValidation)
  else
    Result := SBDSA.IsValidKey(@FP[0], Length(FP), @FQ[0], Length(FQ), @FG[0],
    Length(FG), @FY[0], Length(FY), nil, 0, false, FStrictKeyValidation);
end;

procedure TElDSAKeyMaterial.Generate(Bits : integer);
var
  PSize, QSize, GSize, YSize, XSize, BlSize : integer;
begin
  PSize := 0;
  QSize := 0;
  GSize := 0;
  YSize := 0;
  XSize := 0;
  BlSize := 0;
  SBDSA.Generate(Bits, nil, PSize, nil, QSize, nil, GSize, nil, YSize, nil, XSize, nil, BlSize);

  SetLength(FP, PSize);
  SetLength(FQ, QSize);
  SetLength(FG, GSize);
  SetLength(FY, YSize);
  SetLength(FX, XSize);
  SetLength(FKeyBlob, BlSize);

  if not SBDSA.Generate(Bits, @FP[0], PSize, @FQ[0], QSize, @FG[0], GSize,
    @FY[0], YSize, @FX[0], XSize, @FKeyBlob[0], BlSize)
  then
    raise EElPublicKeyCryptoError.Create(SInternalError);

  SetLength(FP, PSize);
  SetLength(FQ, QSize);
  SetLength(FG, GSize);
  SetLength(FY, YSize);
  SetLength(FX, XSize);
  SetLength(FKeyBlob, BlSize);
  FSecretKey := true;
  FPublicKey := true;
  FKeyFormat := dsaFIPS;
end;

procedure TElDSAKeyMaterial.LoadSecret(Buffer: pointer;
   Size: integer);
var
  PlainKey : pointer;
  PlainSize : integer;
  Header : string;
  Decoded : ByteArray;
  R : integer;
  PSize, QSize, GSize, YSize, XSize : integer;
begin
  Reset;
  // checking if the key is PEM-enveloped
  if IsPEM(Buffer, Size) then
  begin
    PlainSize := 0;
    SBPEM.Decode(Buffer, Size, nil, FPassphrase, PlainSize, Header);
    SetLength(Decoded, PlainSize);
    R := SBPEM.Decode(Buffer, Size, @Decoded[0], FPassphrase, PlainSize, Header);
    if R <> 0 then
    begin
      case R of
        PEM_DECODE_RESULT_INVALID_PASSPHRASE :
          raise EElPublicKeyCryptoError.Create(SInvalidPassphrase);
        else
          raise EElPublicKeyCryptoError.Create(SInvalidPEM);
      end;
    end;
    PlainKey := @Decoded[0];
  end
  else
  begin
    PlainKey := Buffer;
    PlainSize := Size;
  end;
  // trying to load a key as plain DSA key
  PSize := 0;
  QSize := 0;
  GSize := 0;
  YSize := 0;
  XSize := 0;
  SBDSA.DecodePrivateKey(PlainKey, PlainSize, nil, PSize, nil, QSize, nil,
    GSize, nil, YSize, nil, XSize);
  SetLength(FP, PSize);
  SetLength(FQ, QSize);
  SetLength(FG, GSize);
  SetLength(FY, YSize);
  SetLength(FX, XSize);

  if SBDSA.DecodePrivateKey(PlainKey, PlainSize, @FP[0], PSize, @FQ[0], QSize,
    @FG[0], GSize, @FY[0], YSize, @FX[0], XSize) then
  begin
    FPublicKey := true;
    FSecretKey := true;
    SetLength(FKeyBlob, PlainSize);
    Move(PlainKey^, FKeyBlob[0], Length(FKeyBlob));
    TrimParams;
  end
  else
    raise EElPublicKeyCryptoError.Create(SInvalidSecretKey);
end;

procedure TElDSAKeyMaterial.SaveSecret(Buffer: pointer;
  var Size: integer);
var
  OutSize : integer;
  Encrypt : boolean;
begin
  if not FSecretKey then
    raise EElPublicKeyCryptoError.Create(SSecretKeyNotFound);

  if PEMEncode then
  begin
    OutSize := 0;
    Encrypt := Length(FPassphrase) > 0;
    SBPEM.Encode(@FKeyBlob[0], Length(FKeyBlob), nil, OutSize,
      'DSA PRIVATE KEY', Encrypt, FPassphrase);
    if OutSize <= Size then
    begin
      if not SBPEM.Encode(@FKeyBlob[0], Length(FKeyBlob), Buffer, Size,
        'DSA PRIVATE KEY', Encrypt, FPassphrase) then
        raise EElPublicKeyCryptoError.Create(SPEMWriteError);
    end
    else if Size = 0 then
      Size := OutSize
    else
      raise EElPublicKeyCryptoError.Create(SBufferTooSmall);
  end
  else
  begin
    if Size = 0 then
      Size := Length(FKeyBlob)
    else if Size < Length(FKeyBlob) then
      raise EElPublicKeyCryptoError.Create(SBufferTooSmall)
    else
    begin
      Size := Length(FKeyBlob);
      Move(FKeyBlob[0], Buffer^, Size);
    end
  end;
end;

procedure TElDSAKeyMaterial.Import(Cert: TElX509Certificate);
begin
  if Cert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    Cert.Clone(FCert, true);
    ExtractKeyMaterialFromX509;
    FKeyFormat := dsaX509;
  end
  else
    raise EElPublicKeyCryptoError.Create(SUnsupportedCertType);
end;

procedure TElDSAKeyMaterial.ImportPublicKey(P : pointer; PSize : integer; Q : pointer;
  QSize : integer; G : pointer; GSize : integer; Y : pointer; YSize : integer);
begin
  FPublicKey := true;
  FSecretKey := false;

  SetLength(FP, PSize);
  SetLength(FQ, QSize);
  SetLength(FG, GSize);
  SetLength(FY, YSize);
  SetLength(FX, 0);
  SetLength(FKeyBlob, 0);

  Move(P^, FP[0], PSize);
  Move(Q^, FQ[0], QSize);
  Move(G^, FG[0], GSize);
  Move(Y^, FY[0], YSize);

  FKeyFormat := dsaFIPS;
end;

procedure TElDSAKeyMaterial.ExportPublicKey(P : pointer; var PSize : integer;
  Q : pointer; var QSize : integer; G : pointer; var GSize : integer;
  Y : pointer; var YSize : integer);
begin
  if (PSize = 0) then
  begin
    PSize := Length(FP);
    QSize := Length(FQ);
    GSize := Length(FG);
    YSize := Length(FY);
    Exit;
  end;
   
  if (Length(FP) < PSize) or (Length(FQ) < QSize) or (Length(FG) < GSize) or
    (Length(FY) < YSize)
  then
    raise EElPublicKeyCryptoError.Create(SBufferTooSmall);

  PSize := Length(FP);
  QSize := Length(FQ);
  GSize := Length(FG);
  YSize := Length(FY);

  Move(FP[0], P^, PSize);
  Move(FQ[0], Q^, QSize);
  Move(FG[0], G^, GSize);
  Move(FY[0], Y^, YSize);
end;

////////////////////////////////////////////////////////////////////////
//  TElDSAPublicKeyCrypto class

function TElDSAPublicKeyCrypto.GetUsedHashFunction: integer;
begin
  Result := SB_ALGORITHM_DGST_SHA1;
end;

function TElDSAPublicKeyCrypto.GetSupportsEncryption: boolean;
begin
  Result := false;
end;

function TElDSAPublicKeyCrypto.GetSupportsSigning: boolean;
begin
  Result := true;
end;

procedure TElDSAPublicKeyCrypto.SetKeyMaterial(Material : TElPublicKeyMaterial);
begin
  if not (Material is TElDSAKeyMaterial) then
    raise EElPublicKeyCryptoError.Create(SBadKeyMaterial)
  else
    FKeyMaterial := Material;
end;

procedure TElDSAPublicKeyCrypto.SignInit(Detached: boolean);
begin
  if not (FKeyMaterial is TElDSAKeyMaterial) then
    raise EElPublicKeyCryptoError.Create(SBadKeyMaterial);
  SetLength(FSpool, 0);
  if not FInputIsHash then
    FHashFunction := TElHashFunction.Create(GetUsedHashFunction);
end;

procedure TElDSAPublicKeyCrypto.SignUpdate(Buffer: pointer;
   Size: integer);
var
  OldLen : integer;
begin
  if FInputIsHash then
  begin
    OldLen := Length(FSpool);
    SetLength(FSpool, OldLen + Size);
    Move(Buffer^, FSpool[OldLen], Size);
  end
  else
    FHashFunction.Update(Buffer,  Size);
end;

procedure TElDSAPublicKeyCrypto.SignFinal;
var
  Hash : BufferType;
  KM : TElDSAKeyMaterial;
  RSize, SSize, SigSize : integer;
  R, S, Sig : ByteArray;
begin
  KM := TElDSAKeyMaterial(FKeyMaterial);
  if FInputIsHash then
  begin
    SetLength(Hash, Length(FSpool));
    Move(FSpool[0], Hash[1], Length(Hash));
  end
  else
  begin
    Hash := FHashFunction.Finish;
    FreeAndNil(FHashFunction);
  end;
  
  RSize := 0;
  SSize := 0;

  SBDSA.Sign(@Hash[1], Length(Hash), @KM.FP[0], Length(KM.FP), @KM.FQ[0], Length(KM.FQ),
    @KM.FG[0], Length(KM.FG), @KM.FX[0], Length(KM.FX), nil, RSize, nil, SSize);

  SetLength(R, RSize);
  SetLength(S, SSize);

  if not SBDSA.Sign(@Hash[1], Length(Hash), @KM.FP[0], Length(KM.FP), @KM.FQ[0], Length(KM.FQ),
    @KM.FG[0], Length(KM.FG), @KM.FX[0], Length(KM.FX), @R[0], RSize, @S[0], SSize)
  then
    raise EElPublicKeyCryptoError.Create(SSigningFailed);

  SigSize := 0;
  SBDSA.EncodeSignature(@R[0], RSize, @S[0], SSize, nil, SigSize);
  SetLength(Sig, SigSize);

  if not SBDSA.EncodeSignature(@R[0], RSize, @S[0], SSize, @Sig[0], SigSize)
  then
    raise EElPublicKeyCryptoError.Create(SSigningFailed);

  WriteToOutput(@Sig[0], SigSize);
end;

procedure TElDSAPublicKeyCrypto.VerifyInit(Detached: boolean; Signature: pointer;
   SigSize: integer);
var
  RealSigPtr : pointer;
  RealSigSize : integer;
  Buf : ByteArray;
begin
  SetLength(FSpool, 0);

  if InputEncoding = dsapkeBase64 then
  begin
    RealSigSize := 0;
    Base64Decode(Signature, SigSize, nil, RealSigSize);
    SetLength(Buf, RealSigSize);
    if Base64Decode(Signature, SigSize, @Buf[0], RealSigSize) <> 0 then
      raise EElPublicKeyCryptoError.Create(SInvalidSignatureEncoding);
    RealSigPtr := @Buf[0];
  end
  else
  begin
    RealSigPtr := Signature;
    RealSigSize := SigSize;
  end;

  SetLength(FSignature, RealSigSize);
  Move(RealSigPtr^, FSignature[0], RealSigSize);

  if not FInputIsHash then
    FHashFunction := TElHashFunction.Create(GetUsedHashFunction);
end;

procedure TElDSAPublicKeyCrypto.VerifyUpdate(Buffer: pointer;
  Size: integer);
var
  OldLen : integer;
begin
  if FInputIsHash then
  begin
    OldLen := Length(FSpool);
    SetLength(FSpool, OldLen + Size);
    Move(Buffer^, FSpool[OldLen], Size);
  end
  else
    FHashFunction.Update(Buffer, Size);
end;

function TElDSAPublicKeyCrypto.VerifyFinal : TSBPublicKeyVerificationResult;
var
  R, S : ByteArray;
  RSize, SSize : integer;
  Hash : BufferType;
  KM : TElDSAKeyMaterial;
begin
  if (not (FKeyMaterial is TElDSAKeyMaterial)) or not (FKeyMaterial.PublicKey) then
    raise EElPublicKeyCryptoError.Create(SBadKeyMaterial);

  KM := TElDSAKeyMaterial(FKeyMaterial);

  if not FInputIsHash then
  begin
    Hash := FHashFunction.Finish;
    FreeAndNil(FHashFunction);
  end
  else
  begin
    SetLength(Hash, Length(FSpool));
    Move(FSpool[0], Hash[1], Length(FSpool));
    SetLength(FSpool, 0);
  end;

  if Length(FSignature) <= 0 then
  begin
    Result := pkvrInvalidSignature;
    Exit;  
  end;

  RSize := 0;
  SSize := 0;
  SBDSA.DecodeSignature(@FSignature[0], Length(FSignature), nil, RSize, nil, SSize);
  SetLength(R, RSize);
  SetLength(S, SSize);

  if not SBDSA.DecodeSignature(@FSignature[0], Length(FSignature), @R[0], RSize, @S[0], SSize)
  then
  begin
    Result := pkvrInvalidSignature;
    Exit;
  end;

  SetLength(R, RSize);
  SetLength(S, SSize);

  if SBDSA.ValidateSignature(@Hash[1], Length(Hash), @KM.FP[0], Length(KM.FP),
    @KM.FQ[0], Length(KM.FQ), @KM.FG[0], Length(KM.FG), @KM.FY[0], Length(KM.FY),
    @R[0], RSize, @S[0], SSize)
  then
    Result := pkvrSuccess
  else
    Result := pkvrFailure; 
end;

function TElDSAPublicKeyCrypto.EstimateOutputSize(InBuffer: pointer;
   InSize: integer;
  Operation : TSBPublicKeyOperation): integer;
begin
  if (Operation in [pkoEncrypt, pkoDecrypt, 
                    pkoVerify]) then
    raise EElPublicKeyCryptoError.Create(SUnsupportedOperation); 
  if not (KeyMaterial is TElDSAKeyMaterial) then
    raise EElPublicKeyCryptoError.Create(SBadKeyMaterial); 
  if (Operation = pkoSign) and 
     (InputIsHash) and (InSize <> 20) then
    raise EElPublicKeyCryptoError.Create(SInvalidInput);

  Result := 56; {Length(R) + Length(S) + 16}
end;

class function TElDSAPublicKeyCrypto.IsAlgorithmSupported(Alg: integer): boolean;
begin
  Result := Alg in [SB_CERT_ALGORITHM_ID_DSA, SB_CERT_ALGORITHM_ID_DSA_SHA1];
end;

class function TElDSAPublicKeyCrypto.IsAlgorithmSupported(const OID: BufferType): boolean;
begin
  Result := CompareContent(OID, SB_OID_DSA) or CompareContent(OID, SB_OID_DSA_SHA1);
end;

class function TElDSAPublicKeyCrypto.GetName() : string;
begin
  Result := 'DSA';
end;

class function TElDSAPublicKeyCrypto.GetDescription() : string;
begin
  Result := 'Implements DSA signing functionality';
end;

procedure TElDSAPublicKeyCrypto.WriteToOutput(Buffer: pointer;
   Size: integer);
var
  OutSize : integer;
  Buf : ByteArray;
begin
  if FOutputEncoding = dsapkeBinary then
    inherited WriteToOutput(Buffer, Size)
  else if FOutputEncoding = dsapkeBase64 then
  begin
    OutSize := 0;
    Base64Encode(Buffer, Size, nil, OutSize, false);
    SetLength(Buf, OutSize);
    Base64Encode(Buffer, Size, @Buf[0], OutSize, false);
    inherited WriteToOutput(@Buf[0], OutSize);
  end;
end;


procedure TElDSAPublicKeyCrypto.Reset;
begin
  FInputIsHash := false;
  FInputEncoding := dsapkeBinary;
  FOutputEncoding := dsapkeBinary;
end;

constructor TElDSAPublicKeyCrypto.Create(const OID : BufferType);
begin
  inherited Create;
  FOID := OID;

  if not IsAlgorithmSupported(OID) then
    raise EElPublicKeyCryptoError.CreateFmt(SUnsupportedAlgorithm, [OIDToStr(OID)]);
end;

constructor TElDSAPublicKeyCrypto.Create(Alg : integer);
begin
  inherited Create;

  FOID := EmptyBuffer;

  if IsAlgorithmSupported(Alg) then
  begin
    FOID := GetOIDByPKAlgorithm(Alg);
    if Length(FOID) = 0 then
      FOID := GetOIDBySigAlgorithm(Alg);
  end;
  if CompareContent(FOID, EmptyBuffer) then
    raise EElPublicKeyCryptoError.CreateFmt(SUnsupportedAlgorithmInt, [Alg]);
end;

constructor TElDSAPublicKeyCrypto.Create;
begin
  Create(SB_CERT_OID_DSA);
end;

destructor TElDSAPublicKeyCrypto.Destroy;
begin
  inherited;
end;

end.
