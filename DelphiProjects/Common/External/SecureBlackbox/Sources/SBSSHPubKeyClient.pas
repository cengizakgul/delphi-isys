(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSSHPubKeyClient;

interface

uses
  SBSSHClient,
  SBSSHCommon,
  SBSSHConstants,
  SBUtils,
  SBSSHKeyStorage,
  SBSSHPubKeyCommon,
  Classes,
  SysUtils
  {$ifndef CLX_USED},
  Windows,
  Messages
  {$else}
  , libc
  {$endif}
  ;

type

  TElSSHPublicKeyClient = class(TSBControlBase)
  private
    FOutBuffer: pointer;
    FInBuffer: pointer;
    FInBufferIndex: integer;
    FTunnel: TElSubsystemSSHTunnel;
    FTunnelConnection: TElSSHTunnelConnection;
    FOnOpenConnection: TNotifyEvent;
    FOnCloseConnection: TNotifyEvent;
    {$IFDEF SBB_DEBUG}
    FOnDebug: TSSHDebugEvent;
    {$ENDIF}
    FOnError: TSBSSHPublicKeyErrorEvent;
    FOnStatus: TSBSSHPublicKeyStatusEvent;
    FOnPublicKey: TSBSSHPublicKeyPublicKeyEvent;
    FOnAttribute: TSBSSHPublicKeyAttributeEvent;
    FLocalVersion : integer;
    FRemoteVersion: integer;
    FActualVersion: integer;
    FActive : boolean;
    FPubKeyBufferSize: Integer;
    FProcessingRequest : boolean;
    FSubsystem : string;
  protected
    procedure HandleTunnelOpen(Sender: TObject; TunnelConnection:
      TElSSHTunnelConnection);
    procedure HandleTunnelClose(Sender: TObject; TunnelConnection:
      TElSSHTunnelConnection);
    procedure HandleTunnelError(Sender: TObject; Error: integer; Data : pointer);
    procedure HandleTunnelConnectionData(Sender: TObject; Buffer: pointer;
      Size: longint);
    procedure HandleTunnelConnectionExtendedData(Sender: TObject; Buffer:
      pointer; Size: longint);
    procedure HandleTunnelConnectionClose(Sender: TObject; CloseType:
      TSSHCloseType);
    procedure HandleTunnelConnectionError(Sender: TObject; Error: integer);
    procedure Notification(AComponent: TComponent; AOperation: TOperation);
      override;
    procedure SetTunnel(Tunnel: TElSubsystemSSHTunnel);
    function GetTunnel: TElSubsystemSSHTunnel;
    {$IFDEF SBB_DEBUG}
    procedure Debug(Str: string);
    {$ENDIF}
    procedure DoSend(Buffer: pointer; Size: integer);
    procedure DoOpenConnection;
    procedure DoError(ErrorCode: integer; const Comment: string);
    procedure DoCloseConnection;
    { Protocol routines }
    procedure AnalyzeBuffer;
    procedure ParseOnTransportLayer(P: pointer; Size: integer);
    procedure ParseVersion(P: pointer; Size: integer);
    procedure ParseStatus(P: pointer; Size: integer);
    procedure ParsePublicKey(P: pointer; Size: integer);
    procedure ParseAttribute(P: pointer; Size: integer);
    procedure SendOnTransportLayer(Buffer: string); overload;
    procedure SendVersion;
    procedure SendStatus(Status: integer; const Descr, Tag: string);
    function GetActive : boolean;
    procedure SetSubsystem(const S : string);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Active: boolean read GetActive;
    property RemoteVersion: integer read FRemoteVersion;
    property Version : integer read FActualVersion;
    procedure Add(Key: TElSSHKey;
      Attributes : TElSSHPublicKeyAttributes {$ifdef HAS_DEF_PARAMS}= nil{$endif};
      Overwrite: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif});
    procedure Remove(Key: TElSSHKey);
    procedure List;
    procedure ListAttributes;
  published
    property Tunnel: TElSubsystemSSHTunnel read GetTunnel write SetTunnel;
    property Subsystem: string read FSubsystem write SetSubsystem;
    
    property OnOpenConnection: TNotifyEvent read FOnOpenConnection write
    FOnOpenConnection;
    property OnCloseConnection: TNotifyEvent read FOnCloseConnection write
    FOnCloseConnection;
    {$IFDEF SBB_DEBUG}
    property OnDebug: TSSHDebugEvent read FOnDebug write FOnDebug;
    {$ENDIF}
    property OnError: TSBSSHPublicKeyErrorEvent read FOnError write FOnError;
    property OnStatus: TSBSSHPublicKeyStatusEvent read FOnStatus write FOnStatus;
    property OnPublicKey: TSBSSHPublicKeyPublicKeyEvent read FOnPublicKey write FOnPublicKey;
    property OnAttribute: TSBSSHPublicKeyAttributeEvent read FOnAttribute write FOnAttribute;
  end;

procedure Register;

implementation

uses
  SBSSHUtils;

resourcestring
  SErrorUnsupportedVersion = 'Unsupported Protocol Version';
  SErrorInvalidPacket = 'Invalid Packet';
  SErrorTunnelError = 'Tunnel Error';
  SErrorConnectionClosed = 'Connection closed by lower level protocol';
  SUnknownAlgorithm = 'Unknown algorithm: %d';
  SInvalidPublicKey = 'Invalid public key';
  SComponentBusy = 'Component is busy';
  SUnsupportedByVersion = 'Unsupported by protocol version';

procedure Register;
begin
  RegisterComponents('SSHBlackbox', [TElSSHPublicKeyClient]);
end;

constructor TElSSHPublicKeyClient.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FPubKeyBufferSize := BUFFER_SIZE;
  GetMem(FOutBuffer, FPubKeyBufferSize);
  GetMem(FInBuffer, FPubKeyBufferSize);
  FRemoteVersion := -1;
  FLocalVersion := 2;
  FActive := false;
  FProcessingRequest := false;
  FSubsystem := 'publickey';
end;


destructor TElSSHPublicKeyClient.Destroy;
begin
  FreeMem(FOutBuffer);
  FreeMem(FInBuffer);
  inherited;
end;

////////////////////////////////////////////////////////////////////////////////
// Protocol flow implementation

procedure TElSSHPublicKeyClient.AnalyzeBuffer;
var
  Len: cardinal;
begin
  while FInBufferIndex >= 4 do
  begin
    Len := (PByteArray(FInBuffer)[0] shl 24) or
      (PByteArray(FInBuffer)[1] shl 16) or
      (PByteArray(FInBuffer)[2] shl 8) or
      (PByteArray(FInBuffer)[3] and $FF);
    if Len > Cardinal(FPubKeyBufferSize) then
    begin
      DoError(ERROR_SSH_INVALID_PACKET, SErrorInvalidPacket);
      DoCloseConnection;
      FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
      exit;
    end;
    if cardinal(FInBufferIndex - 4) >= Len then
    begin
      ParseOnTransportLayer(@PByteArray(FInBuffer)[4], Len);
      Move(PByteArray(FInBuffer)[Len + 4], FInBuffer^, cardinal(FInBufferIndex) -
        Len - 4);
      Dec(FInBufferIndex, Len + 4);
    end
    else
      break;
  end;
end;

procedure TElSSHPublicKeyClient.ParseOnTransportLayer(P: pointer; Size: integer);
var
  PacketType: string;
  ofs: integer;
begin
  PacketType := ReadString(@PByteArray(P)[0], Size);
  ofs := 4 + Length(PacketType);
  if PacketType = 'version' then
    ParseVersion(@PByteArray(P)[ofs], Size - ofs)
  else
  if PacketType = 'status' then
    ParseStatus(@PByteArray(P)[ofs], Size - ofs)
  else
  if PacketType = 'publickey' then
    ParsePublicKey(@PByteArray(P)[ofs], Size - ofs)
  else
  if PacketType = 'attribute' then
    ParseAttribute(@PByteArray(P)[ofs], Size - ofs)
  else
  begin
    SendStatus(SSH_PUBLICKEY_REQUEST_NOT_SUPPORTED, '', '');
    exit;
  end;
end;

procedure TElSSHPublicKeyClient.ParseVersion(P: pointer; Size: integer);
var
  Vers: integer;
begin
  FProcessingRequest := false;
  try
    Vers := ReadLength(P, Size);
  except
    SendStatus(SSH_PUBLICKEY_GENERAL_FAILURE, '', '');
    exit;
  end;
  FRemoteVersion := Vers;
  FActualVersion := Min(FLocalVersion, FRemoteVersion);
  if not (FActualVersion in [1, 2]) then
  begin
    SendStatus(SSH_PUBLICKEY_VERSION_NOT_SUPPORTED, 'Implementation supports only versions 1 and 2', '');
    DoError(ERROR_SSH_PROTOCOL_VERSION_NOT_SUPPORTED, SErrorUnsupportedVersion);
    DoCloseConnection;
    FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
  end
  else
  begin
    FActive := true;
    if Assigned(FOnOpenConnection) then
      FOnOpenConnection(Self);
  end;
end;

procedure TElSSHPublicKeyClient.ParseStatus(P: pointer; Size: integer);
var
  StatusCode: integer;
  ErrorMessage, Lang: string;
begin
  try
    FProcessingRequest := false;
    StatusCode := ReadLength(P, Size);
    ErrorMessage := ReadString(@PByteArray(P)[4], Size - 4);
    Lang := ReadString(@PByteArray(P)[8 + Length(ErrorMessage)], Size - 8 - Length(ErrorMessage));
  except
    SendStatus(SSH_PUBLICKEY_GENERAL_FAILURE, '', '');
    exit;
  end;
  if Assigned(FOnStatus) then
    FOnStatus(self, StatusCode, ErrorMessage, Lang);
end;

procedure TElSSHPublicKeyClient.ParsePublicKey(P: pointer; Size: integer);
var
  Alg, Blob, Attr, Value: string;
  Cnt, i, x: integer;
  Key: TElSSHKey;
  Comment : string;
  Attrs : TElSSHPublicKeyAttributes;
  KeyLoadResult : integer;
begin
  try
    Attrs := TElSSHPublicKeyAttributes.Create;
    Key := TElSSHKey.Create;
    Comment := '';
    try
      i := 0;
      if FActualVersion = 1 then
      begin
        Comment := ReadString(@PByteArray(P)[i], Size - i);
        inc(i, 4 + Length(Comment));
      end;
      Alg := ReadString(@PByteArray(P)[i], Size - i);
      inc(i, 4 + Length(Alg));
      Blob := ReadString(@PByteArray(P)[i], Size - i);
      inc(i, 4 + Length(Blob));
      if FActualVersion >= 2 then
      begin
        Cnt := ReadLength(@PByteArray(P)[i], Size - i);
        inc(i, 4);
        for x := 0 to Cnt - 1 do
        begin
          Attr := ReadString(@PByteArray(P)[i], Size - i);
          inc(i, 4 + Length(Attr));
          Value := ReadString(@PByteArray(P)[i], Size - i);
          Attrs.Add(Attr, Value{$ifndef HAS_DEF_PARAMS}, false{$endif});
        end;
      end;
      KeyLoadResult := Key.LoadPublicKeyFromBlob(Alg, @Blob[1], Length(Blob));
      if KeyLoadResult <> 0 then
      begin
        DoError(KeyLoadResult, SInvalidPublicKey);
        Exit;
      end;
      Key.Comment := Comment;
      if Assigned(FOnPublicKey) then
        FOnPublicKey(self, Key, Attrs);
    finally
      FreeAndNil(Attrs);
      FreeAndNil(Key);
    end;
  except
    SendStatus(SSH_PUBLICKEY_GENERAL_FAILURE, '', '');
  end;
end;

procedure TElSSHPublicKeyClient.ParseAttribute(P: pointer; Size: integer);
var
  Attr: string;
  Compul: boolean;
begin
  try
    Attr := ReadString(P, Size);
    Compul := ReadBoolean(@PByteArray(P)[4 + Length(Attr)], Size - 4 - Length(Attr));
  except
    SendStatus(SSH_PUBLICKEY_GENERAL_FAILURE, '', '');
    exit;
  end;
  if Assigned(FOnAttribute) then
    FOnAttribute(self, Attr, Compul);
end;

procedure TElSSHPublicKeyClient.SendOnTransportLayer(Buffer: string);
var
  TotalSize: integer;
begin
  TotalSize := Length(Buffer){ + 4};
  PByteArray(FOutBuffer)[0] := TotalSize shr 24;
  PByteArray(FOutBuffer)[1] := (TotalSize shr 16) and $FF;
  PByteArray(FOutBuffer)[2] := (TotalSize shr 8) and $FF;
  PByteArray(FOutBuffer)[3] := TotalSize and $FF;
  DoSend(FOutBuffer, 4);
  DoSend(@Buffer[1], Length(Buffer));
end;

procedure TElSSHPublicKeyClient.SendVersion;
begin
  FProcessingRequest := true;
  SendOnTransportLayer(WriteString('version') + WriteUINT32(2));
end;

procedure TElSSHPublicKeyClient.SendStatus(Status: integer; const Descr, Tag: string);
begin
  FProcessingRequest := true;
  SendOnTransportLayer(WriteString('status') + WriteUINT32(Status) +
    WriteString(Descr) + WriteString(Tag));
end;

procedure TElSSHPublicKeyClient.Add(Key: TElSSHKey;
  Attributes : TElSSHPublicKeyAttributes {$ifdef HAS_DEF_PARAMS}= nil{$endif};
  Overwrite: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif});
var
  attrs: string;
  i: integer;
  Alg : string;
  Blob : BufferType;
  BlobSize : integer;
  Comment : BufferType;
begin
  if FProcessingRequest then                        
    raise EElSSHPublicKeyError.Create(SComponentBusy);
  if (FActualVersion >= 2) then
  begin
    if Attributes <> nil then
    begin
      attrs := WriteBoolean(Overwrite) + WriteUINT32(Attributes.Count);
      for i := 0 to Attributes.Count - 1 do
      begin
        attrs := attrs + WriteString(Attributes.Names[i]) + WriteString(Attributes.Values[i]) +
          WriteBoolean(Attributes.Mandatory[i]);
      end;
    end
    else
      attrs := WriteBoolean(Overwrite) + WriteUINT32(0);
  end
  else
    SetLength(Attrs, 0);
  BlobSize := 0;
  Key.SavePublicKeyToBlob(Alg, nil, BlobSize);
  SetLength(Blob, BlobSize);
  I := Key.SavePublicKeyToBlob(Alg, @Blob[1], BlobSize);
  if I <> 0 then
    raise EElSSHPublicKeyError.Create(SInvalidPublicKey);
  SetLength(Blob, BlobSize);
  if FActualVersion = 1 then
    Comment := WriteString('')
  else
    Comment := '';
  FProcessingRequest := true;
  SendOnTransportLayer(WriteString('add') + Comment + WriteString(Alg) +
    WriteString(Blob) + attrs);
end;

procedure TElSSHPublicKeyClient.Remove(Key: TElSSHKey);
var
  Size : integer;
  Buf : BufferType;
  AlgName : string;
  R : integer;
begin
  if FProcessingRequest then
    raise EElSSHPublicKeyError.Create(SComponentBusy);
  FProcessingRequest := true;
  Size := 0;
  Key.SavePublicKeyToBlob(AlgName, nil, Size);
  SetLength(Buf, Size);
  R := Key.SavePublicKeyToBlob(AlgName, @Buf[1], Size);
  SetLength(Buf, Size);
  if R <> 0 then
    raise EElSSHPublicKeyError.Create(SInvalidPublicKey);
  SendOnTransportLayer(WriteString('remove') + WriteString(AlgName) + WriteString(Buf));
end;

procedure TElSSHPublicKeyClient.List;
begin
  if FProcessingRequest then                        
    raise EElSSHPublicKeyError.Create(SComponentBusy);
  FProcessingRequest := true;
  SendOnTransportLayer(WriteString('list'));
end;

procedure TElSSHPublicKeyClient.ListAttributes;
begin
  if FProcessingRequest then                        
    raise EElSSHPublicKeyError.Create(SComponentBusy);
  if FActualVersion < 2 then
    raise EElSSHPublicKeyError.Create(SUnsupportedByVersion);
  FProcessingRequest := true;
  SendOnTransportLayer(WriteString('listattributes'));
end;

////////////////////////////////////////////////////////////////////////////////
// Handlers

procedure TElSSHPublicKeyClient.HandleTunnelOpen(Sender: TObject; TunnelConnection:
  TElSSHTunnelConnection);
begin
  FTunnelConnection := TElSSHClientTunnelConnection(TunnelConnection);
{$IFDEF SBB_Debug}
  Debug('PubKey subsystem connection established');
{$ENDIF}
  FTunnelConnection.OnData := HandleTunnelConnectionData;
  FTunnelConnection.OnExtendedData := HandleTunnelConnectionExtendedData;
  FTunnelConnection.OnError := HandleTunnelConnectionError;
  FTunnelConnection.OnClose := HandleTunnelConnectionClose;
  //FActive := true;
  //if Assigned(FOnOpenConnection) then
    //FOnOpenConnection(Self);
  SendVersion;
end;

procedure TElSSHPublicKeyClient.HandleTunnelClose(Sender: TObject; TunnelConnection:
  TElSSHTunnelConnection);
begin
  DoCloseConnection;
end;

procedure TElSSHPublicKeyClient.HandleTunnelError(Sender: TObject; Error: integer;
  Data: pointer);
begin
  DoError(SSH_TUNNEL_ERROR_SERVER_ERROR, SErrorTunnelError + ' [' + IntToStr(Error) + ']');
  DoCloseConnection;
end;

procedure TElSSHPublicKeyClient.HandleTunnelConnectionData(Sender: TObject; Buffer:
  pointer; Size: longint);
begin
  // check up here for required buffer size
  Move(Buffer^, PByteArray(FInBuffer)[FInBufferIndex], Size);
  Inc(FInBufferIndex, Size);
  AnalyzeBuffer;
end;

procedure TElSSHPublicKeyClient.HandleTunnelConnectionExtendedData(Sender: TObject;
  Buffer: pointer; Size: longint);
begin
  AnalyzeBuffer;
end;

procedure TElSSHPublicKeyClient.HandleTunnelConnectionClose(Sender: TObject; CloseType:
  TSSHCloseType);
begin
  DoError(ERROR_SSH_CONNECTION_LOST, SErrorConnectionClosed);
  DoCloseConnection;
end;

procedure TElSSHPublicKeyClient.HandleTunnelConnectionError(Sender: TObject; Error:
  integer);
begin
  DoError(SSH_TUNNEL_ERROR_SERVER_ERROR, SErrorTunnelError);
  DoCloseConnection;
end;

////////////////////////////////////////////////////////////////////////////////
// Properties management

procedure TElSSHPublicKeyClient.Notification(AComponent: TComponent; AOperation:
  TOperation);
begin
  inherited;
end;

procedure TElSSHPublicKeyClient.SetTunnel(Tunnel: TElSubsystemSSHTunnel);
begin
  FTunnel := Tunnel;
  FTunnel.OnOpen := HandleTunnelOpen;
  FTunnel.OnClose := HandleTunnelClose;
  FTunnel.OnError := HandleTunnelError;
  FTunnel.Subsystem := FSubsystem;
end;

function TElSSHPublicKeyClient.GetTunnel: TElSubsystemSSHTunnel;
begin
  Result := FTunnel;
end;

////////////////////////////////////////////////////////////////////////////////
// Event pushers

{$IFDEF SBB_Debug}

procedure TElSSHPublicKeyClient.Debug(Str: string);
begin
  if Assigned(FOnDebug) then
    FOnDebug(Self, Str);
end;
{$ENDIF}

procedure TElSSHPublicKeyClient.DoSend(Buffer: pointer; Size: integer);
begin
  FTunnelConnection.SendData(Buffer, Size);
end;

procedure TElSSHPublicKeyClient.DoOpenConnection;
begin
  FActive := true;
  if Assigned(FOnOpenConnection) then
    FOnOpenConnection(Self);
end;

procedure TElSSHPublicKeyClient.DoError(ErrorCode: integer; const Comment: string);
begin
  if Assigned(FOnError) then
    FOnError(Self, ErrorCode, Comment);
end;

procedure TElSSHPublicKeyClient.DoCloseConnection;
begin
  FActive := false;
  if Assigned(FOnCloseConnection) then
    FOnCloseConnection(Self);
end;


function TElSSHPublicKeyClient.GetActive : boolean;
begin
  Result := FActive and (FTunnelConnection <> nil);
end;

procedure TElSSHPublicKeyClient.SetSubsystem(const S : string);
begin
  FSubsystem := S;
  if FTunnel <> nil then
    FTunnel.Subsystem := S;
end;

end.
