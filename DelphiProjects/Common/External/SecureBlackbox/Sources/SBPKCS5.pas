(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBPKCS5;

interface

uses
  SBUtils, SBASN1, SBASN1Tree, SBConstants,
  SysUtils, Classes,
  SBHashFunction,
  SBStreamCipher, SBHMAC;

type
  TSBPKCS5Version = 
    (sbP5v1, sbP5v2);
  
  TElPKCS5PBE = class
  private
    FAlgorithm : integer;
    FKeyLength : integer;
    FIterationCount : integer;
    FSalt : BufferType;
    FKeyDerivationFunction: integer;
    FPseudoRandomFunction: integer;
    FPseudoRandomFunctionSize: integer;
    FHashFunction: integer;
    FIndex: integer;
    FIV : BufferType;
    FEncryptionAlgorithm: BufferType;
    FEncryptionAlgorithmParams: BufferType;
    FSymmetricAlgorithm: BufferType;
    FVersion : TSBPKCS5Version;
    procedure DeriveKeyKDF1(const Password: BufferType; Size : integer; var Key: BufferType);
    procedure DeriveKeyKDF2(const Password: BufferType; Size : integer; var Key: BufferType);
    function FindAlgIndexByOID(const OID : BufferType): integer;
    procedure ProcessPBES1Params(const Params: BufferType);
    procedure ProcessPBES2Params(const Params: BufferType);
    procedure DecryptPBES1(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize : integer; const Password : string);
    procedure DecryptPBES2(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize : integer; const Password : string);
    procedure EncryptPBES1(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize : integer; const Password : string);
    procedure EncryptPBES2(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize : integer; const Password : string);
    procedure ProcessKDFParams(const OID: BufferType; const Params: BufferType);
    procedure ProcessPBKDF2Params(const Params: BufferType);
    procedure ProcessESParams(const OID: BufferType; const Params: BufferType);
    function WriteESParams : BufferType;
    function WriteES1Params : BufferType;
    function WriteES2Params : BufferType;
    function PRF(const Password: string; const Salt : BufferType): BufferType;
    function PRFHMACSHA1(const Password: string; const Salt : BufferType): BufferType;
  public
    // use this constructor to create the PBE basing on PBE algorithm identifier and params
    constructor Create(const OID: BufferType; const Params: BufferType); overload;
    // use this constructor to create the PBE basing on STREAM and HASH algorithms
    constructor Create(StreamAlg: integer; HashAlg: integer; UseNewVersion : boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}); overload;
    destructor Destroy; override;
    
    procedure Decrypt(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize : integer; const Password : string);
    procedure Encrypt(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize : integer; const Password : string);
    class function IsAlgorithmSupported(Alg: integer) : boolean; overload;
    class function IsAlgorithmSupported(const OID: BufferType): boolean; overload;
    class function GetAlgorithmByOID(const OID: BufferType): integer;
    property Algorithm : integer read FAlgorithm;
    property Version : TSBPKCS5Version read FVersion;
    property EncryptionAlgorithmOID : BufferType read FEncryptionAlgorithm;
    property EncryptionAlgorithmParams : BufferType read FEncryptionAlgorithmParams;
  end;

  EElPKCS5Error =  class(Exception);
  EElPKCS5UnsupportedError =  class(EElPKCS5Error);
  EElPKCS5InternalError =  class(EElPKCS5Error);
  EElPKCS5InvalidParameterError =  class(EElPKCS5Error);
  EElPKCS5InvalidPasswordError =  class(EElPKCS5Error);

implementation

uses
  SBPKCS7, SBRC2;

resourcestring
  SUnsupportedAlgorithm = 'Unsupported algorithm: %s';
  SInvalidParameters = 'Invalid parameters';
  SInternalError = 'Internal error';
  SInvalidPassword = 'Invalid password';
  SDigestTooShort = 'Digest too short';
  SInvalidSaltOrIterationCount = 'Invalid salt or iteration count';

const
  SB_PKCS5_SUPPORTED_COUNT = 7;
  SB_PKCS5_SUPPORTED_ALGs : array[0..SB_PKCS5_SUPPORTED_COUNT - 1] of integer = 
  (
    SB_ALGORITHM_P5_PBE_MD2_DES,
    SB_ALGORITHM_P5_PBE_MD5_DES,
    SB_ALGORITHM_P5_PBE_SHA1_DES,
    SB_ALGORITHM_P5_PBE_MD2_RC2,
    SB_ALGORITHM_P5_PBE_MD5_RC2,
    SB_ALGORITHM_P5_PBE_SHA1_RC2,
    SB_ALGORITHM_P5_PBES2
  );
  
  SB_PKCS5_SUPPORTED_OIDs : array{$ifndef NET_CF_1_0}[0..SB_PKCS5_SUPPORTED_COUNT - 1]{$endif} of TBufferTypeConst = 
  (
    TBufferTypeConst(#$2a#$86#$48#$86#$f7#$0d#$01#$05#$01),
    #$2a#$86#$48#$86#$f7#$0d#$01#$05#$03,
    #$2a#$86#$48#$86#$f7#$0d#$01#$05#$0a,
    #$2a#$86#$48#$86#$f7#$0d#$01#$05#$04,
    #$2a#$86#$48#$86#$f7#$0d#$01#$05#$06,
    #$2a#$86#$48#$86#$f7#$0d#$01#$05#$0b,
    #$2a#$86#$48#$86#$f7#$0d#$01#$05#$0D
  );

  SB_PKCS5_HASH_FUNCTIONS : array[0..SB_PKCS5_SUPPORTED_COUNT - 1] of integer = 
  (
    SB_ALGORITHM_DGST_MD2,
    SB_ALGORITHM_DGST_MD5,
    SB_ALGORITHM_DGST_SHA1,
    SB_ALGORITHM_DGST_MD2,
    SB_ALGORITHM_DGST_MD5,
    SB_ALGORITHM_DGST_SHA1,
    SB_ALGORITHM_UNKNOWN
  );

  SB_PKCS5_BLOCK_FUNCTIONS : array[0..SB_PKCS5_SUPPORTED_COUNT - 1] of integer = 
  (
    SB_ALGORITHM_CNT_DES,
    SB_ALGORITHM_CNT_DES,
    SB_ALGORITHM_CNT_DES,
    SB_ALGORITHM_CNT_RC2,
    SB_ALGORITHM_CNT_RC2,
    SB_ALGORITHM_CNT_RC2,
    SB_ALGORITHM_UNKNOWN
  );

  SB_PKCS5_PBES_FUNCTIONS : array[0..SB_PKCS5_SUPPORTED_COUNT - 1] of integer = 
  (
    SB_ALGORITHM_P5_PBES1,
    SB_ALGORITHM_P5_PBES1,
    SB_ALGORITHM_P5_PBES1,
    SB_ALGORITHM_P5_PBES1,
    SB_ALGORITHM_P5_PBES1,
    SB_ALGORITHM_P5_PBES1,
    SB_ALGORITHM_P5_PBES2
  );

constructor TElPKCS5PBE.Create(const OID: BufferType; const Params: BufferType);
begin
  inherited Create;
  FIndex := FindAlgIndexByOID(OID);
  if FIndex < 0 then
    raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [OIDToStr(OID)]);
  FEncryptionAlgorithm := OID;
  FEncryptionAlgorithmParams := Params;
  if SB_PKCS5_PBES_FUNCTIONS[FIndex] = SB_ALGORITHM_P5_PBES1 then
  begin
    ProcessPBES1Params(Params);
    FVersion := sbP5v1;
  end
  else if SB_PKCS5_PBES_FUNCTIONS[FIndex] = SB_ALGORITHM_P5_PBES2 then
  begin
    ProcessPBES2Params(Params);
    FVersion := sbP5v2;
  end
  else
    raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(SB_PKCS5_PBES_FUNCTIONS[FIndex])]);
end;

constructor TElPKCS5PBE.Create(StreamAlg: integer; HashAlg: integer; UseNewVersion : boolean {$ifdef HAS_DEF_PARAMS}= true{$endif});
var
  Tag : TElASN1ConstrainedTag;
begin
  inherited Create;
  if UseNewVersion then
  begin
    FIndex := 6;
    case StreamAlg of
      SB_ALGORITHM_CNT_DES:
      begin
        FKeyLength := 8;
        FSymmetricAlgorithm := SB_OID_DES_CBC;
      end;
      SB_ALGORITHM_CNT_3DES:
      begin
        FKeyLength := 24;
        FSymmetricAlgorithm := SB_OID_DES_EDE3_CBC;
      end;
      SB_ALGORITHM_CNT_RC2:
      begin
        FKeyLength := 16;
        FSymmetricAlgorithm := SB_OID_RC2_CBC;
      end;
    else
      raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(StreamAlg)]);
    end;
    FKeyDerivationFunction := SB_ALGORITHM_P5_PBKDF2;
    FPseudoRandomFunction := SB_ALGORITHM_MAC_HMACSHA1;
    FPseudoRandomFunctionSize := 20;
    FHashFunction := SB_ALGORITHM_DGST_SHA1;
    SetLength(FIV, 8);
    SBRndGenerate(@FIV[1], 8);
    FVersion := sbP5v2;
    Tag := TElASN1ConstrainedTag.Create;
    try
      Tag.TagID := SB_ASN1_SEQUENCE;
    finally
      FreeAndNil(Tag);
    end;
  end
  else
  begin
    case StreamAlg of
      SB_ALGORITHM_CNT_DES:
      begin
        FKeyLength := 8;
        FSymmetricAlgorithm := SB_OID_DES_CBC;
        FIndex := 2;
      end;
      SB_ALGORITHM_CNT_RC2:
      begin
        FKeyLength := 16;
        FSymmetricAlgorithm := SB_OID_RC2_CBC;
        FIndex := 5;
      end;
    else
      raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(StreamAlg)]);
    end;
    FKeyDerivationFunction := SB_ALGORITHM_P5_PBKDF1;
    FHashFunction := SB_ALGORITHM_DGST_SHA1;
    FVersion := sbP5v1;
  end;
  FEncryptionAlgorithm := SB_PKCS5_SUPPORTED_OIDs[FIndex];
  FIterationCount := 2048;
  SetLength(FSalt, 8);
  SBRndGenerate(@FSalt[1], 8);
  FEncryptionAlgorithmParams := WriteESParams;
end;

destructor TElPKCS5PBE.Destroy;
begin
  inherited;
end;

procedure TElPKCS5PBE.ProcessPBES1Params(const Params: BufferType);
var
  Tag : TElASN1ConstrainedTag;
begin
  Tag := TElASN1ConstrainedTag.Create;
  try
    if not Tag.LoadFromBuffer(@Params[1], Length(Params)) then
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
    if (Tag.Count <> 1) or (not Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) or
      (TElASN1ConstrainedTag(Tag.GetField(0)).Count <> 2) or
      (not TElASN1ConstrainedTag(Tag.GetField(0)).GetField(0).CheckType(SB_ASN1_OCTETSTRING, false)) or
      (not TElASN1ConstrainedTag(Tag.GetField(0)).GetField(1).CheckType(SB_ASN1_INTEGER, false)) then
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
    FSalt := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(0)).Content;
    FIterationCount := ASN1ReadInteger(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(1)));
    if (Length(FSalt) <> 8) or (FIterationCount <= 0) then
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
    FHashFunction := SB_PKCS5_HASH_FUNCTIONS[FIndex];
    FAlgorithm := SB_PKCS5_BLOCK_FUNCTIONS[FIndex];
  finally
    FreeAndNil(Tag);
  end;
end;

procedure TElPKCS5PBE.ProcessPBES2Params(const Params: BufferType);
var
  Tag : TElASN1ConstrainedTag;
  AlgIDTag : TElASN1ConstrainedTag;
  KDFOID, KDFParams : BufferType;
  ESOID, ESParams : BufferType;
begin
  Tag := TElASN1ConstrainedTag.Create;
  try
    if not Tag.LoadFromBuffer(@Params[1], Length(Params)) then
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
    if (Tag.Count <> 1) or (not Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) or
      (TElASN1ConstrainedTag(Tag.GetField(0)).Count <> 2) or
      (not TElASN1ConstrainedTag(Tag.GetField(0)).GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) or
      (not TElASN1ConstrainedTag(Tag.GetField(0)).GetField(1).CheckType(SB_ASN1_SEQUENCE, true)) then
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
    // keyDerivationFunc
    AlgIDTag := TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(0));
    if ProcessAlgorithmIdentifier(AlgIDTag, KDFOID, KDFParams {$ifndef HAS_DEF_PARAMS}, False{$endif}) <> 0 then
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
    // encryptionScheme
    AlgIDTag := TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(1));
    if ProcessAlgorithmIdentifier(AlgIDTag, ESOID, ESParams {$ifndef HAS_DEF_PARAMS}, False{$endif}) <> 0 then
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
    ProcessKDFParams(KDFOID, KDFParams);
    ProcessESParams(ESOID, ESParams);
  finally
    FreeAndNil(Tag);
  end;
end;

procedure TElPKCS5PBE.ProcessKDFParams(const OID: BufferType; const Params: BufferType);
begin
  if CompareStr(SB_OID_PBKDF2, OID) = 0 then
    ProcessPBKDF2Params(Params)
  else
    raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [OIDToStr(OID)]);
end;

procedure TElPKCS5PBE.ProcessPBKDF2Params(const Params: BufferType);
var
  Tag, SeqTag : TElASN1ConstrainedTag;
  Index : integer;
  PRFOID, PRFParams : BufferType;
begin
  Tag := TElASN1ConstrainedTag.Create;
  try
    if not Tag.LoadFromBuffer(@Params[1], Length(Params)) then
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
    if (Tag.Count <> 1) or (not Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
    SeqTag := TElASN1ConstrainedTag(Tag.GetField(0));
    Index := 0;
    // salt
    if (Index >= SeqTag.Count) or (not SeqTag.GetField(Index).CheckType(SB_ASN1_OCTETSTRING, false)) then
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
    FSalt := TElASN1SimpleTag(SeqTag.GetField(Index)).Content;
    Inc(Index);
    // iteration count
    if (Index >= SeqTag.Count) or (not SeqTag.GetField(Index).CheckType(SB_ASN1_INTEGER, false)) then
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
    FIterationCount := ASN1ReadInteger(TElASN1SimpleTag(SeqTag.GetField(Index)));
    Inc(Index);
    // key length (optional)
    if (Index < SeqTag.Count) and (SeqTag.GetField(Index).CheckType(SB_ASN1_INTEGER, false)) then
    begin
      FKeyLength := ASN1ReadInteger(TElASN1SimpleTag(SeqTag.GetField(Index)));
      Inc(Index);
    end;
    // PRF
    if (Index < SeqTag.Count) and (SeqTag.GetField(Index).CheckType(SB_ASN1_SEQUENCE, true)) then
    begin
      if ProcessAlgorithmIdentifier(SeqTag.GetField(Index), PRFOID, PRFParams {$ifndef HAS_DEF_PARAMS}, False{$endif}) <> 0 then
        raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
      if CompareStr(PRFOID, SB_OID_RSA_HMACSHA1) <> 0 then
        raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [OIDToStr(PRFOID)]);
      Inc(Index);
    end
    else if Index >= SeqTag.Count then
    begin
      PRFOID := SB_OID_RSA_HMACSHA1;
      PRFParams := TBufferTypeConst(#$05#$00);
    end;
    FPseudoRandomFunction := SB_ALGORITHM_MAC_HMACSHA1;
    FPseudoRandomFunctionSize := 20; // for HMAC-SHA1
    if Index < SeqTag.Count then
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);
  finally
    FreeAndNil(Tag);
  end;
  FKeyDerivationFunction := SB_ALGORITHM_P5_PBKDF2;
end;

procedure TElPKCS5PBE.ProcessESParams(const OID: BufferType; const Params: BufferType);
var
  IV : BufferType;
  KeySize : integer;
begin
  if not TElStreamCipher.IsAlgorithmSupported(OID) then
    raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [OIDToStr(OID)]);
  if not TElStreamCipher.GetAlgorithmParameters(OID, Params, KeySize, IV) then
    raise EElPKCS5UnsupportedError.Create(SInvalidParameters);
  FKeyLength := KeySize;
  FIV := IV;
  FSymmetricAlgorithm := OID;
  FAlgorithm := TElStreamCipher.GetAlgorithmByOID(OID);
end;

function TElPKCS5PBE.WriteESParams : BufferType;
begin
  if FVersion = sbP5v1 then
    Result := WriteES1Params
  else
    Result := WriteES2Params;
end;

function TElPKCS5PBE.WriteES1Params : BufferType;
var
  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  Size : integer;
begin
  Tag := TElASN1ConstrainedTag.Create;
  try
    Tag.TagID := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagID := SB_ASN1_OCTETSTRING;
    STag.Content := FSalt;
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagID := SB_ASN1_INTEGER;
    ASN1WriteInteger(STag, FIterationCount);
    Size := 0;
    Tag.SaveToBuffer(nil, Size);
    SetLength(Result, Size);
    Tag.SaveToBuffer(@Result[1], Size);
    SetLength(Result, Size);
  finally
    FreeAndNil(Tag);
  end;
end;

function TElPKCS5PBE.WriteES2Params : BufferType;
var
  Tag : TElASN1ConstrainedTag;
  CTag : TElASN1ConstrainedTag;
  ParamTag, AlgIDTag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  Params : BufferType;
  Size : integer;
  Buf : BufferType;
begin
  Tag := TElASN1ConstrainedTag.Create;
  try
    Tag.TagID := SB_ASN1_SEQUENCE;
    // keyDerivationFunc
    if FKeyDerivationFunction = SB_ALGORITHM_P5_PBKDF2 then
    begin
      CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
      ParamTag := TElASN1ConstrainedTag.Create;
      try
        ParamTag.TagID := SB_ASN1_SEQUENCE;
        STag := TElASN1SimpleTag(ParamTag.GetField(ParamTag.AddField(false)));
        STag.TagID := SB_ASN1_OCTETSTRING;
        STag.Content := FSalt;
        STag := TElASN1SimpleTag(ParamTag.GetField(ParamTag.AddField(false)));
        STag.TagID := SB_ASN1_INTEGER;
        ASN1WriteInteger(STag, FIterationCount);
        AlgIDTag := TElASN1ConstrainedTag(ParamTag.GetField(ParamTag.AddField(true)));
        SaveAlgorithmIdentifier(AlgIDTag, SB_OID_RSA_HMACSHA1, TBufferTypeConst('') {$ifndef HAS_DEF_PARAMS}, 0{$endif});
        Size := 0;
        ParamTag.SaveToBuffer(nil, Size);
        SetLength(Buf, Size);
        ParamTag.SaveToBuffer(@Buf[1], Size);
        SetLength(Buf, Size);
      finally
        FreeAndNil(ParamTag);
      end;
      SaveAlgorithmIdentifier(CTag, SB_OID_PBKDF2, Buf {$ifndef HAS_DEF_PARAMS}, 0{$endif});
    end
    else
      raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(FKeyDerivationFunction)]);
    // encryptionScheme
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    TElStreamCipher.SaveAlgorithmParameters(FSymmetricAlgorithm, FKeyLength,
      FIV, Params);
    SaveAlgorithmIdentifier(CTag, FSymmetricAlgorithm, Params {$ifndef HAS_DEF_PARAMS}, 0{$endif});
    Size := 0;
    Tag.SaveToBuffer(nil, Size);
    SetLength(Result, Size);
    Tag.SaveToBuffer(@Result[1], Size);
    SetLength(Result, Size);
  finally
    FreeAndNil(Tag);
  end;
end;

procedure TElPKCS5PBE.Decrypt(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
  var OutSize : integer; const Password : string);
begin
  CheckLicenseKey();
  if (OutSize = 0) or (OutBuffer = nil) then
    OutSize := InSize
  else
  begin
    if SB_PKCS5_PBES_FUNCTIONS[FIndex] = SB_ALGORITHM_P5_PBES1 then
      DecryptPBES1(InBuffer, InSize, OutBuffer, OutSize, Password)
    else
    if SB_PKCS5_PBES_FUNCTIONS[FIndex] = SB_ALGORITHM_P5_PBES2 then
      DecryptPBES2(InBuffer, InSize, OutBuffer, OutSize, Password)
    else
      raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(SB_PKCS5_PBES_FUNCTIONS[FIndex])]);
  end;
end;

procedure TElPKCS5PBE.Encrypt(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
  var OutSize : integer; const Password : string);
begin
  CheckLicenseKey();
  if (OutSize = 0) or (OutBuffer = nil) then
    OutSize := InSize + 32
  else
  begin
    if SB_PKCS5_PBES_FUNCTIONS[FIndex] = SB_ALGORITHM_P5_PBES1 then
      EncryptPBES1(InBuffer, InSize, OutBuffer, OutSize, Password)
    else
    if SB_PKCS5_PBES_FUNCTIONS[FIndex] = SB_ALGORITHM_P5_PBES2 then
      EncryptPBES2(InBuffer, InSize, OutBuffer, OutSize, Password)
    else
      raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(SB_PKCS5_PBES_FUNCTIONS[FIndex])]);
  end;
end;

procedure TElPKCS5PBE.DecryptPBES1(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
  var OutSize : integer; const Password : string);
var
  Key : BufferType;
  SC : TElStreamCipher;
  SCKey, SCIV : BufferType;
begin
  DeriveKeyKDF1(Password, 16, Key);
  try
    SetLength(SCKey, 8);
    SetLength(SCIV, 8);
    Move(Key[1], SCKey[1], 8);
    Move(Key[9], SCIV[1], 8);
    SC := TElStreamCipher.Create(SB_PKCS5_BLOCK_FUNCTIONS[FIndex], SCKey, SCIV, 
        scdDecrypt, scmCBC);
    try
      SC.Write(InBuffer, InSize);
      SC.Finish;
      OutSize := SC.BytesAvailable;
      OutSize := SC.Read(OutBuffer, OutSize);
    finally
      FreeAndNil(SC);
    end;
  except
    on EElStreamCipherUnsupportedError do
      raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(SB_PKCS5_BLOCK_FUNCTIONS[FIndex])]);

    on EElStreamCipherInvalidParameterError do
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);

    on EElStreamCipherInvalidPaddingError do
      raise EElPKCS5InvalidPasswordError.Create(SInvalidPassword);

    on Exception do
      raise EElPKCS5Error.Create(SInternalError);
  end;
end;

procedure TElPKCS5PBE.DecryptPBES2(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
  var OutSize : integer; const Password : string);
var
  Key : BufferType;
  SC : TElStreamCipher;
begin
  if FKeyDerivationFunction = SB_ALGORITHM_P5_PBKDF2 then
    DeriveKeyKDF2(Password, FKeyLength, Key)
  else
    raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(FKeyDerivationFunction)]);

  try
    SC := TElStreamCipher.Create(FSymmetricAlgorithm, Key, FIV,
      scdDecrypt);
    try
      SC.Write(InBuffer, InSize);
      SC.Finish;
      OutSize := SC.BytesAvailable;
      OutSize := SC.Read(OutBuffer, OutSize);
    finally
      FreeAndNil(SC);
    end;
  except
    on EElStreamCipherUnsupportedError do
      raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(SB_PKCS5_BLOCK_FUNCTIONS[FIndex])]);

    on EElStreamCipherInvalidParameterError do
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);

    on EElStreamCipherInvalidPaddingError do
      raise EElPKCS5InvalidPasswordError.Create(SInvalidPassword);

    on Exception do
      raise EElPKCS5Error.Create(SInternalError);
  end;
end;

procedure TElPKCS5PBE.EncryptPBES1(InBuffer: pointer; InSize: integer;
  OutBuffer: pointer; var OutSize : integer; const Password : string);
var
  Key : BufferType;
  SC : TElStreamCipher;
  SCKey, SCIV : BufferType;
begin
  DeriveKeyKDF1(Password, 16, Key);
  try
    SetLength(SCKey, 8);
    SetLength(SCIV, 8);
    Move(Key[1], SCKey[1], 8);
    Move(Key[9], SCIV[1], 8);
    SC := TElStreamCipher.Create(SB_PKCS5_BLOCK_FUNCTIONS[FIndex], SCKey, SCIV, 
        scdEncrypt, scmCBC);
    try
      SC.Write(InBuffer, InSize);
      SC.Finish;
      OutSize := SC.BytesAvailable;
      OutSize := SC.Read(OutBuffer, OutSize);
    finally
      FreeAndNil(SC);
    end;
  except
    on EElStreamCipherUnsupportedError do
      raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(SB_PKCS5_BLOCK_FUNCTIONS[FIndex])]);

    on EElStreamCipherInvalidParameterError do
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);

    on EElStreamCipherInvalidPaddingError do
      raise EElPKCS5InvalidPasswordError.Create(SInvalidPassword);

    on Exception do
      raise EElPKCS5Error.Create(SInternalError);
  end;
end;

procedure TElPKCS5PBE.EncryptPBES2(InBuffer: pointer; InSize: integer;
  OutBuffer: pointer; var OutSize : integer; const Password : string);
var
  Key : BufferType;
  SC : TElStreamCipher;
begin
  if FKeyDerivationFunction = SB_ALGORITHM_P5_PBKDF2 then
    DeriveKeyKDF2(Password, FKeyLength, Key)
  else
    raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(FKeyDerivationFunction)]);
  try
    SC := TElStreamCipher.Create(FSymmetricAlgorithm, Key, FIV,
      scdEncrypt);
    try
      SC.Write(InBuffer, InSize);
      SC.Finish;
      OutSize := SC.BytesAvailable;
      OutSize := SC.Read(OutBuffer, OutSize);
    finally
      FreeAndNil(SC);
    end;
  except
    on EElStreamCipherUnsupportedError do
      raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(SB_PKCS5_BLOCK_FUNCTIONS[FIndex])]);

    on EElStreamCipherInvalidParameterError do
      raise EElPKCS5InvalidParameterError.Create(SInvalidParameters);

    on EElStreamCipherInvalidPaddingError do
      raise EElPKCS5InvalidPasswordError.Create(SInvalidPassword);

    on Exception do
      raise EElPKCS5Error.Create(SInternalError);
  end;
end;

procedure TElPKCS5PBE.DeriveKeyKDF1(const Password: BufferType; Size : integer;
  var Key: BufferType);
var
  DigestSize : integer;
  HashFunc : TElHashFunction;
  Data : BufferType;
  I : integer;
begin
  try
    DigestSize := TElHashFunction.GetDigestSizeBits(FHashFunction);
  except
    on E : EElHashFunctionUnsupportedError do
      raise EElPKCS5UnsupportedError.Create(E.Message);
  end;
  if (DigestSize shr 3) < Size then
    raise EElPKCS5InternalError.Create(SDigestTooShort);
  if (Length(FSalt) <> 8) or (FIterationCount <= 0) then
    raise EElPKCS5InvalidParameterError.Create(SInvalidSaltOrIterationCount);
  try
    HashFunc := TElHashFunction.Create(FHashFunction);
  except
    on E : EElHashFunctionUnsupportedError do
      raise EElPKCS5UnsupportedError.Create(E.Message);
  end;
  try
    try
      Data := Password + FSalt;
      for I := 0 to FIterationCount - 1 do
      begin
        HashFunc.Reset;
        HashFunc.Update(@Data[1], Length(Data));
        Data := HashFunc.Finish;
      end;
    finally
      FreeAndNil(HashFunc);
    end;
  except
    raise EElPKCS5InternalError.Create(SInternalError);
  end;
  SetLength(Data, Size);
  Key := Data;
end;

procedure TElPKCS5PBE.DeriveKeyKDF2(const Password: BufferType; Size : integer;
  var Key: BufferType);

  function WriteInt(Value : integer) : BufferType;
  begin
    SetLength(Result, 4);
    PByte(@Result[1])^ := Value shr 24;
    PByte(@Result[2])^ := (Value shr 16) and $ff;
    PByte(@Result[3])^ := (Value shr 8) and $ff;
    PByte(@Result[4])^ := Value and $ff;
  end;

var
  DigestSize: integer;
  Count : integer;
  I, K, J : integer;
  U, Chunk : BufferType;
begin
  try
    DigestSize := FPseudoRandomFunctionSize;
    Count := (Size - 1) div DigestSize + 1;
  except
    on E : EElHashFunctionUnsupportedError do
      raise EElPKCS5UnsupportedError.Create(E.Message);
  end;
  SetLength(Key, 0);
  for I := 1 to Count do
  begin
    SetLength(Chunk, DigestSize);
    FillChar(Chunk[1], DigestSize, 0);
    U := FSalt + WriteInt(I);
    for K := 0 to FIterationCount - 1 do
    begin
      U := PRF((Password), U);
      for J := 1 to Length(U) do
        PByte(@Chunk[J])^ := PByte(@Chunk[J])^ xor PByte(@U[J])^;
    end;
    Key := Key + Chunk;
  end;
  SetLength(Key, Size);
end;

function TElPKCS5PBE.PRF(const Password: string; const Salt : BufferType): BufferType;
begin
  if FPseudoRandomFunction = SB_ALGORITHM_MAC_HMACSHA1 then
    Result := PRFHMACSHA1(Password, Salt)
  else
    raise EElPKCS5UnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(FPseudoRandomFunction)]);
end;

function TElPKCS5PBE.PRFHMACSHA1(const Password: string; const Salt : BufferType): BufferType;
var
  M160 : TMessageDigest160;
begin
  M160 := SBHMAC.HashMACSHA1(Salt, Password);
  SetLength(Result, 20);
  Move(M160, Result[1], 20);
end;

class function TElPKCS5PBE.IsAlgorithmSupported(Alg: integer) : boolean;
var
  I : integer;
begin
  Result := false;
  for I := 0 to SB_PKCS5_SUPPORTED_COUNT - 1 do
    if Alg = SB_PKCS5_SUPPORTED_ALGS[I] then
    begin
      Result := true;
      Break;
    end;
end;

class function TElPKCS5PBE.IsAlgorithmSupported(const OID: BufferType): boolean;
var
  I : integer;
begin
  Result := false;
  for I := 0 to SB_PKCS5_SUPPORTED_COUNT - 1 do
    if CompareStr(OID, SB_PKCS5_SUPPORTED_OIDs[I]) = 0 then
    begin
      Result := true;
      Break;
    end;
end;

class function TElPKCS5PBE.GetAlgorithmByOID(const OID: BufferType): integer;
var
  I : integer;
begin
  Result := SB_ALGORITHM_UNKNOWN;
  for I := 0 to SB_PKCS5_SUPPORTED_COUNT - 1 do
    if CompareStr(OID, SB_PKCS5_SUPPORTED_OIDs[I]) = 0 then
    begin
      Result := SB_PKCS5_SUPPORTED_ALGS[I];
      Break;
    end;
end;

function TElPKCS5PBE.FindAlgIndexByOID(const OID : BufferType): integer;
var
  I : integer;
begin
  Result := -1;
  for I := 0 to SB_PKCS5_SUPPORTED_COUNT - 1 do
    if CompareStr(SB_PKCS5_SUPPORTED_OIDs[I], OID) = 0 then
    begin
      Result := I;
      Break;
    end;
end;

end.
