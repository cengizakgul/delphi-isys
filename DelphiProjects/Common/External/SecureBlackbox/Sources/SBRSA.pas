
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

unit SBRSA;
{$I SecBbox.inc}

interface

uses SBUtils, SBMath, SBConstants;

  
function ValidateSignature(Hash : pointer; HashSize : integer; PublicModulus :
  pointer; PublicModulusSize : integer; PublicExponent : pointer;
  PublicExponentSize : integer; Signature : pointer; SignatureSize : integer) : boolean;

function Generate(Bits : integer; PublicModulus : pointer; var PublicModulusSize :
  integer; PublicExponent : pointer; var PublicExponentSize : integer;
  PrivateExponent : pointer; var PrivateExponentSize : integer) : boolean; overload; 

function Generate(Bits : integer; PublicModulus : pointer; var PublicModulusSize :
  integer; PublicExponent : pointer; var PublicExponentSize : integer;
  PrivateExponent : pointer; var PrivateExponentSize : integer;
  PrivateKeyBlob : pointer; var PrivateKeyBlobSize : integer;
  ProgressFunc : TSBMathProgressFunc = nil; Data : pointer = nil) : boolean; overload; 

function Generate(Bits : integer; PublicModulus : PLInt; PublicExponent : PLInt;
  PrivateExponent: PLInt; P : PLInt; Q : PLInt; U : PLInt) : boolean; overload; 

function Sign(Hash : pointer; HashSize : integer; PublicModulus : pointer;
  PublicModulusSize : integer; PrivateExponent : pointer; PrivateExponentSize :
  integer; Signature : pointer; var SignatureSize : integer) : boolean; overload; 

function Sign(Hash : pointer; HashSize : integer; PrivateKeyBlob: pointer;
  PrivateKeyBlobSize : integer; Signature: pointer; var SignatureSize : integer) : boolean; overload; 

function Encrypt(InBuffer : pointer; InSize : integer; PublicModulus : pointer;
  PublicModulusSize : integer; PublicExponent : pointer; PublicExponentSize :
  integer; OutBuffer : pointer; var OutSize : integer) : boolean; overload; 

function Encrypt(InBuffer : pointer; InSize : integer; OutBuffer: pointer;
  var OutSize : integer; PublicModulus : PLInt; PublicExponent : PLInt) : boolean; overload; 

function Decrypt(InBuffer : pointer; InSize : integer; PublicModulus : pointer;
  PublicModulusSize : integer; PrivateExponent : pointer; PrivateExponentSize :
  integer; OutBuffer : pointer; var OutSize : integer) : boolean; overload; 

function Decrypt(InBuffer : pointer; InSize : integer; PrivateKeyBlob: pointer;
  PrivateKeyBlobSize : integer; OutBuffer: pointer; var OutSize : integer) : boolean; overload;

function DecodePrivateKey(Buffer : pointer; Size : integer; PublicModulus :
  pointer; var PublicModulusSize : integer; PublicExponent : pointer; var
  PublicExponentSize : integer; PrivateExponent : pointer; var PrivateExponentSize :
  integer) : boolean; overload;

function DecodePrivateKey(Buffer : pointer; Size : integer; PublicModulus :
  pointer; var PublicModulusSize : integer; PublicExponent : pointer; var
  PublicExponentSize : integer; PrivateExponent : pointer; var PrivateExponentSize :
  integer; P : pointer; var PSize : integer; Q : pointer; var QSize : integer;
  E1 : pointer; var E1Size : integer; E2 : pointer; var E2Size : integer;
  U : pointer; var USize : integer) : boolean; overload;

function EncodePrivateKey(PublicModulus : pointer; PublicModulusSize : integer;
  PublicExponent : pointer; PublicExponentSize : integer; PrivateExponent :
  pointer; PrivateExponentSize : integer; Prime1 : pointer; Prime1Size : integer;
  Prime2 : pointer; Prime2Size : integer; Exponent1 : pointer; Exponent1Size :
  integer; Exponent2 : pointer; Exponent2Size : integer; Coef : pointer; CoefSize :
  integer; OutBuffer : pointer; var OutSize : integer) : boolean; overload;

function IsValidKey(Blob: pointer; BlobSize: integer) : boolean;

function EncryptOAEP(InBuffer : pointer; InSize : integer; PublicModulus : pointer;
  PublicModulusSize : integer; PublicExponent : pointer; PublicExponentSize :
  integer; Salt: pointer; SaltSize: integer; HashAlg : integer; OutBuffer : pointer;
  var OutSize : integer) : boolean;

function DecryptOAEP(InBuffer : pointer; InSize : integer; PublicModulus : pointer;
  PublicModulusSize : integer; PrivateExponent : pointer; PrivateExponentSize :
  integer; Salt: pointer; SaltSize : integer; HashAlg: integer; OutBuffer : pointer;
  var OutSize : integer) : boolean;

function DecodePublicKey(Buffer : pointer; Size : integer; PublicModulus : pointer;
  var PublicModulusSize: integer; PublicExponent : pointer; var PublicExponentSize:
  integer; var AlgID : BufferType): boolean;

function EncodePublicKey(PublicModulus : pointer; PublicModulusSize : integer;
  PublicExponent : pointer; PublicExponentSize : integer; const AlgID: BufferType;
  OutBuffer : pointer; var OutSize : integer) : boolean;


function EncodePrivateKey(PublicModulus : pointer; PublicModulusSize : integer;
  PublicExponent : pointer; PublicExponentSize : integer; PrivateExponent :
  pointer; PrivateExponentSize : integer; OutBuffer : pointer; var OutSize : integer) : boolean; overload;

{ PKCS#1 RSASSA-PSS-SIGN }
function SignPSS(HashValue : pointer; HashValueSize : integer;
  HashAlgorithm : integer; SaltSize : integer; KeyBlob : pointer;
  KeyBlobSize : integer; Signature : pointer;
  var SignatureSize : integer) : boolean; overload;
function SignPSS(HashValue : pointer; HashValueSize : integer;
  HashAlgorithm : integer; SaltSize : integer;
  PublicModulus : pointer; PublicModulusSize : integer;
  PublicExponent : pointer; PublicExponentSize : integer;  
  PrivateExponent : pointer; PrivateExponentSize : integer;
  Signature : pointer; var SignatureSize : integer) : boolean; overload;
{ PKCS#1 RSASSA-PSS-VERIFY }
function VerifyPSS(HashValue : pointer; HashValueSize : integer;
  HashAlgorithm : integer; SaltSize : integer;
  PublicModulus : pointer; PublicModulusSize : integer;
  PublicExponent : pointer; PublicExponentSize : integer; Signature : pointer;
  SignatureSize : integer) : boolean;

{ parameters of RSA-PSS AlgorithmIdentifier}
function WritePSSParams(HashAlgorithm : integer {$ifdef HAS_DEF_PARAMS}= SB_ALGORITHM_DGST_SHA1{$endif};
  SaltSize : integer {$ifdef HAS_DEF_PARAMS}= 20{$endif};
  MGFAlgorithm : integer {$ifdef HAS_DEF_PARAMS}= SB_CERT_MGF1{$endif};
  TrailerField : integer {$ifdef HAS_DEF_PARAMS}= 1{$endif}) : BufferType; 
function ReadPSSParams(InBuffer : pointer; InBufferSize : integer;
  var HashAlgorithm, SaltSize, MGF, MGFHashAlgorithm, TrailerField : integer) : boolean;
{ parameters of RSA-OAEP AlgorithmIdentifier}
function WriteOAEPParams(HashAlgorithm : integer {$ifdef HAS_DEF_PARAMS}= SB_ALGORITHM_DGST_SHA1{$endif};
  MGFHashAlgorithm : integer {$ifdef HAS_DEF_PARAMS}= SB_ALGORITHM_DGST_SHA1{$endif};
  const StrLabel : string {$ifdef HAS_DEF_PARAMS}= ''{$endif}) : BufferType; 
function ReadOAEPParams(InBuffer : pointer; InBufferSize : integer;
  var HashAlgorithm, MGFHashAlgorithm : integer; var StrLabel : string) : boolean;

implementation

uses
  SysUtils,
  SBASN1, SBASN1Tree, SBMD, SBSHA, SBSHA2, SBHashFunction;

function ValidateSignature(Hash : pointer; HashSize : integer; PublicModulus :
  pointer; PublicModulusSize : integer; PublicExponent : pointer;
  PublicExponentSize : integer; Signature : pointer; SignatureSize : integer) : boolean;
var
  X, E, M, Y : PLInt;
  TmpBuf : array of byte;
  Sz : integer;
begin
  LCreate(X);
  LCreate(E);
  LCreate(M);
  LCreate(Y);
  PointerToLInt(X, Signature, SignatureSize);
  PointerToLInt(E, PublicExponent, PublicExponentSize);
  PointerToLInt(M, PublicModulus, PublicModulusSize);
  LMModPower(X, E, M, Y);

  Sz := Y.Length * 4;
  SetLength(TmpBuf, Sz);

  LIntToPointer(Y, @TmpBuf[0], Sz);

  LDestroy(X);
  LDestroy(E);
  LDestroy(M);
  LDestroy(Y);

  SetLength(TmpBuf, Sz);
  if (Sz > PublicModulusSize) then
  begin
    // LIntToPointer returns value with length divisible by 4
    // Therefore it may contain prefix zeros if PublicModulusSize is
    // not divisible by 4
    Move(TmpBuf[Sz - PublicModulusSize], TmpBuf[0], PublicModulusSize);
    SetLength(TmpBuf, PublicModulusSize);
  end;

  Result := false;

  if ((TmpBuf[0] <> 0) or (TmpBuf[1] <> 1)) and (TmpBuf[0] <> 1) then
    Exit;

  if TmpBuf[0] = 1 then
    Sz := 1
  else
    Sz := 2;

  while (Sz < Length(TmpBuf)) and (TmpBuf[Sz] = $FF) do
    Inc(Sz);
  if (Sz >= Length(TmpBuf)) or (TmpBuf[Sz] <> 0) then
    Exit;
  Result := CompareMem(@TmpBuf[Length(TmpBuf) - HashSize], Hash, HashSize);
end;

function Generate(Bits : integer; PublicModulus : pointer; var PublicModulusSize :
  integer; PublicExponent : pointer; var PublicExponentSize : integer;
  PrivateExponent : pointer; var PrivateExponentSize : integer) : boolean;
var
  P, Q, N, Phi, PDec, QDec : PLInt;
  Sz : integer;
begin
  Sz := Bits shr 3;
  if ((PublicModulusSize < Sz) or (PrivateExponentSize < Sz) or (PublicExponentSize < 4)) then
  begin
    PublicModulusSize := Sz;
    PrivateExponentSize := Sz;
    PublicExponentSize := Sz;
    Result := false;
    Exit;
  end;
  LCreate(P);
  LCreate(Q);
  LCreate(N);
  LCreate(Phi);
  LCreate(PDec);
  LCreate(QDec);
  LGenPrime(P, Bits shr 6, true);
  LGenPrime(Q, Bits shr 6, true);
  LMult(P, Q, N);
  LSub(P, Phi, PDec);
  LSub(Q, Phi, QDec);
  LMult(PDec, QDec, Phi);
  LInit(PDec, '00010001');
  LGCD(PDec, Phi, P, QDec);
  LIntToPointer(N, PublicModulus, PublicModulusSize);
  LIntToPointer(PDec, PublicExponent, PublicExponentSize);
  LIntToPointer(QDec, PrivateExponent, PrivateExponentSize);
  LDestroy(P);
  LDestroy(Q);
  LDestroy(N);
  LDestroy(Phi);
  LDestroy(PDec);
  LDestroy(QDec);
  Result := true;
end;

function Generate(Bits : integer; PublicModulus : PLInt; PublicExponent : PLInt;
  PrivateExponent: PLInt; P : PLInt; Q : PLInt; U : PLInt) : boolean;
var
  Phi, PDec, QDec, Tmp1: PLInt;
begin
  Result := (PublicModulus <> nil) and (PublicExponent <> nil) and
    (PrivateExponent <> nil) and (P <> nil) and (Q <> nil) and (U <> nil);
  if not Result then
    Exit;
  LCreate(Phi);
  LCreate(PDec);
  LCreate(QDec);
  LCreate(Tmp1);
  try
    LGenPrime(P, Bits shr 6, true);
    LGenPrime(Q, Bits shr 6, true);
    if LGreater(P, Q) then
    begin
      LCopy(Tmp1, P);
      LCopy(P, Q);
      LCopy(Q, Tmp1);
    end;
    LMult(P, Q, PublicModulus);
    LSub(P, Phi, PDec);
    LSub(Q, Phi, QDec);
    LMult(PDec, QDec, Phi);
    PublicExponent.Digits[1] := 65537;
    PublicExponent.Length := 1;
    LGCD(PublicExponent, Phi, Tmp1, PrivateExponent);
    LGCD(P, Q, Tmp1, U);
  finally
    LDestroy(Phi);
    LDestroy(PDec);
    LDestroy(QDec);
    LDestroy(Tmp1);
  end;
  Result := true;
end;

function Generate(Bits : integer; PublicModulus : pointer; var PublicModulusSize :
  integer; PublicExponent : pointer; var PublicExponentSize : integer;
  PrivateExponent : pointer; var PrivateExponentSize : integer;
  PrivateKeyBlob : pointer; var PrivateKeyBlobSize : integer; ProgressFunc :
  TSBMathProgressFunc = nil; Data : pointer = nil) : boolean;
const
  ZeroTagContent: array [0..0] of Byte =  (0) ;
var
  EstimatedBlobSize : integer;
  P, Q, N, Phi, PDec, QDec, Tmp1, Tmp2, Tmp3 : PLInt;
  Sz : integer;

  Tmp : AnsiString;

  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
begin
  Sz := Bits shr 3;
  EstimatedBlobSize := (Sz * 4) + (Sz shr 1 + 1) + 5 + 64;
  if ((PublicModulusSize < Sz) or (PrivateExponentSize < Sz) or (PublicExponentSize < 4) or
    (PrivateKeyBlobSize < EstimatedBlobSize)) then
  begin
    PublicModulusSize := Sz;
    PrivateExponentSize := Sz;
    PublicExponentSize := Sz;
    PrivateKeyBlobSize := EstimatedBlobSize;
    Result := false;
    Exit;
  end;
  LCreate(P);
  LCreate(Q);
  LCreate(N);
  LCreate(Phi);
  LCreate(PDec);
  LCreate(QDec);
  LCreate(Tmp1);
  LCreate(Tmp2);
  LCreate(Tmp3);
  try
    try
      LGenPrime(P, Bits shr 6, true, ProgressFunc, Data, true);
      LGenPrime(Q, Bits shr 6, true, ProgressFunc, Data, true);
    except
      Result := false;
      Exit;
    end;
    LMult(P, Q, N);
    LSub(P, Phi, PDec);
    LSub(Q, Phi, QDec);
    LMult(PDec, QDec, Phi);
    LInit(Tmp3, '00010001');
    LGCD(Tmp3, Phi, Tmp1, Tmp2);
    LIntToPointer(N, PublicModulus, PublicModulusSize);
    LIntToPointer(Tmp3, PublicExponent, PublicExponentSize);
    LIntToPointer(Tmp2, PrivateExponent, PrivateExponentSize);

    Tag := TElASN1ConstrainedTag.Create;
    Tag.TagId := SB_ASN1_SEQUENCE;

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    STag.Content := #0;

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    SetLength(Tmp, PublicModulusSize);
    Move(PublicModulus^, Tmp[1], Length(Tmp));
    if Ord(Tmp[1]) >= 128 then
      Tmp := #0 + Tmp;
    STag.Content := Tmp;

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    SetLength(Tmp, PublicExponentSize);
    Move(PublicExponent^, Tmp[1], Length(Tmp));
    if Ord(Tmp[1]) >= 128 then
      Tmp := #0 + Tmp;
    STag.Content := Tmp;

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    SetLength(Tmp, PrivateExponentSize);
    Move(PrivateExponent^, Tmp[1], Length(Tmp));
    if Ord(Tmp[1]) >= 128 then
      Tmp := #0 + Tmp;
    STag.Content := Tmp;

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    Sz := P.Length * 4;

    SetLength(Tmp, Sz);
    LIntToPointer(P, @Tmp[1], Sz);
    if Ord(Tmp[1]) >= 128 then
      Tmp := #0 + Tmp;
    STag.Content := Tmp;

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;

    Sz := Q.Length * 4;
    SetLength(Tmp, Sz);
    LIntToPointer(Q, @Tmp[1], Sz);
    if Ord(Tmp[1]) >= 128 then
      Tmp := #0 + Tmp;
    STag.Content := Tmp;
    LMod(Tmp2, PDec, Tmp3);

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;

    Sz := Tmp3.Length * 4;

    SetLength(Tmp, Sz);
    LIntToPointer(Tmp3, @Tmp[1], Sz);
    if Ord(Tmp[1]) >= 128 then
      Tmp := #0 + Tmp;
    STag.Content := Tmp;
    LMod(Tmp2, QDec, Tmp3);

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    Sz := Tmp3.Length * 4;

    SetLength(Tmp, Sz);
    LIntToPointer(Tmp3, @Tmp[1], Sz);
    if Ord(Tmp[1]) >= 128 then
      Tmp := #0 + Tmp;
    STag.Content := Tmp;
    LGCD(Q, P, Tmp1, Tmp3);

    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagId := SB_ASN1_INTEGER;
    Sz := Tmp3.Length * 4;

    SetLength(Tmp, Sz);
    LIntToPointer(Tmp3, @Tmp[1], Sz);
    if Ord(Tmp[1]) >= 128 then
      Tmp := #0 + Tmp;
    STag.Content := Tmp;

    Result := Tag.SaveToBuffer(PrivateKeyBlob, PrivateKeyBlobSize);
    Tag.Free;
  finally
    LDestroy(P);
    LDestroy(Q);
    LDestroy(N);
    LDestroy(Phi);
    LDestroy(PDec);
    LDestroy(QDec);
    LDestroy(Tmp1);
    LDestroy(Tmp2);
    LDestroy(Tmp3);
  end;
end;

function Sign(Hash : pointer; HashSize : integer; PublicModulus : pointer;
  PublicModulusSize : integer; PrivateExponent : pointer; PrivateExponentSize :
  integer; Signature : pointer; var SignatureSize : integer) : boolean;
var
  TmpBuf : ByteArray;
  X, D, M, Y : PLInt;
  I : integer;
  RealModulusSize : integer;
begin

  if SignatureSize < PublicModulusSize then
  begin
    SignatureSize := PublicModulusSize;
    Result := false;
    Exit;
  end;

  I := 0;
  while (PByteArray(PublicModulus)[I] = 0) and (I < PublicModulusSize) do
    Inc(I);
  RealModulusSize := PublicModulusSize - I;

  if HashSize > RealModulusSize - 11 then
  begin
    SignatureSize := 0;
    Result := false;
    Exit;
  end;

  SetLength(TmpBuf, RealModulusSize);
  Move(Hash^, TmpBuf[RealModulusSize - HashSize], HashSize);
  TmpBuf[0] := 0;
  TmpBuf[1] := 1;
  for I := 2 to RealModulusSize - HashSize - 2 do
    TmpBuf[I] := $FF;
  TmpBuf[RealModulusSize - HashSize - 1] := 0;
  LCreate(X);
  LCreate(Y);
  LCreate(D);
  LCreate(M);
  PointerToLInt(X, @TmpBuf[0], Length(TmpBuf));
  PointerToLInt(D, PrivateExponent, PrivateExponentSize);
  PointerToLInt(M, PublicModulus, PublicModulusSize);
  LMModPower(X, D, M, Y);
  LIntToPointer(Y, Signature, SignatureSize);
  LDestroy(X);
  LDestroy(Y);
  LDestroy(D);
  LDestroy(M);
  Result := true;
end;

function Encrypt(InBuffer : pointer; InSize : integer; PublicModulus : pointer;
  PublicModulusSize : integer; PublicExponent : pointer; PublicExponentSize :
  integer; OutBuffer : pointer; var OutSize : integer) : boolean;
var
  X, E, M, Y : PLInt;
  Buf : array of byte;
  I : integer;
  RealModulusSize : integer;
begin
  RealModulusSize := PublicModulusSize;
  I := 0;
  while (RealModulusSize > 0) and
    (PByteArray(PublicModulus)[I] = 0)
  do
  begin
    Dec(RealModulusSize);
    Inc(I);
  end;

  if InSize > RealModulusSize - 11 then
  begin
    OutSize := 0;
    Result := false;
    Exit;
  end;

  if OutSize < PublicModulusSize then
  begin
    OutSize := PublicModulusSize;
    Result := false;
    Exit;
  end;
  SetLength(Buf, RealModulusSize);
  Buf[0] := 0;
  Buf[1] := 2;
  for I := 2 to Length(Buf) - InSize - 2 do
    Buf[I] := SBRndGenerate(255) + 1;
  Buf[Length(Buf) - InSize - 1] := 0;
  Move(InBuffer^, Buf[Length(Buf) - InSize], InSize);
  LCreate(X);
  LCreate(E);
  LCreate(M);
  LCreate(Y);
  PointerToLInt(X, @Buf[0], Length(Buf));
  PointerToLInt(E, PublicExponent, PublicExponentSize);
  PointerToLInt(M, PublicModulus, PublicModulusSize);
  LMModPower(X, E, M, Y);
  LIntToPointer(Y, OutBuffer, OutSize);
  LDestroy(X);
  LDestroy(E);
  LDestroy(M);
  LDestroy(Y);
  Result := true;
end;

function Encrypt(InBuffer : pointer; InSize : integer; OutBuffer: pointer;
  var OutSize : integer; PublicModulus : PLInt; PublicExponent : PLInt) : boolean;
var
  Buf : array of byte;
  I, Len : integer;
  X, R : PLInt;
begin
  Result := false;
  { Checking for enough output size }
  if OutSize < PublicModulus.Length shl 2 then
  begin
    OutSize := PublicModulus.Length shl 2;
    Exit;
  end;
  Len := (LBitCount(PublicModulus) - 1) shr 3 + 1;
  { Check whether we may encrypt that data at all }
  if InSize > Len - 11 then
    Exit;

  SetLength(Buf, Len);
  Buf[0] := 0;
  Buf[1] := 2;
  for I := 2 to Length(Buf) - InSize - 2 do
    repeat
      Buf[I] := SBRndGenerate(256);//Random(255);
    until Buf[I] <> 0;
  Buf[Length(Buf) - InSize - 1] := 0;
  Move(InBuffer^, Buf[Length(Buf) - InSize], InSize);
  LCreate(X);
  LCreate(R);
  PointerToLInt(X, @Buf[0], Length(Buf));
  LMModPower(X, PublicExponent, PublicModulus, R);
  LDestroy(X);
  if R.Length shl 2 > OutSize then
  begin
    LDestroy(R);
    Exit;
  end;
  LIntToPointer(R, OutBuffer, OutSize);
  LDestroy(R);
  Result := true;
end;

function Decrypt(InBuffer : pointer; InSize : integer; PublicModulus : pointer;
  PublicModulusSize : integer; PrivateExponent : pointer; PrivateExponentSize :
  integer; OutBuffer : pointer; var OutSize : integer) : boolean;
var
  X, D, M, Y : PLInt;
  Buf : array of byte;
  I : integer;
begin
  if OutSize < PublicModulusSize then
  begin
    OutSize := PublicModulusSize;
    Result := false;
    Exit;
  end;
  LCreate(X);
  LCreate(D);
  LCreate(M);
  LCreate(Y);
  PointerToLInt(X, InBuffer, InSize);
  PointerToLInt(D, PrivateExponent, PrivateExponentSize);
  PointerToLInt(M, PublicModulus, PublicModulusSize);
  LMModPower(X, D, M, Y);
  I := Y.Length * 4;
  SetLength(Buf, I);
  LIntToPointer(Y, @Buf[0], I);
  LDestroy(X);
  LDestroy(D);
  LDestroy(M);
  LDestroy(Y);
  SetLength(Buf, I);

  if (I > PublicModulusSize) then
  begin
    // LIntToPointer returns value with length divisible by 4
    // Therefore it may contain prefix zeros if PublicModulusSize is
    // not divisible by 4
    Move(Buf[I - PublicModulusSize], Buf[0], PublicModulusSize);
    SetLength(Buf, PublicModulusSize);
  end;

  if (Buf[0] <> 0) or ((Buf[1] <> 2) and (Buf[1] <> 1)) then
  begin
    Result := false;
    Exit;
  end;
  I := 2;
  while (I < Length(Buf)) and (Buf[I] <> 0) do
    Inc(I);
  Move(Buf[I + 1], OutBuffer^, Length(Buf) - I - 1);
  OutSize := Length(Buf) - I - 1;
  Result := true;
end;

function DecodePrivateKey(Buffer : pointer; Size : integer; PublicModulus :
  pointer; var PublicModulusSize : integer; PublicExponent : pointer; var
  PublicExponentSize : integer; PrivateExponent : pointer; var PrivateExponentSize :
  integer) : boolean;
var
  EncodedKey : TElASN1ConstrainedTag;
  CTag : TElASN1ConstrainedTag;
  I : integer;
begin
  Result := false;

  EncodedKey := TElASN1ConstrainedTag.Create;
  try
    if not EncodedKey.LoadFromBuffer(Buffer, Size) then
      Exit;
    if (EncodedKey.Count < 1) or (EncodedKey.GetField(0).TagId <> SB_ASN1_SEQUENCE) or
      (not EncodedKey.GetField(0).IsConstrained) then
      Exit;

    CTag := TElASN1ConstrainedTag(EncodedKey.GetField(0));
    if CTag.Count > 9 then
      Exit;

    for I := 0 to CTag.Count - 1 do
      if (CTag.GetField(I).TagId <> SB_ASN1_INTEGER) or
        (CTag.GetField(I).IsConstrained) then
        Exit;
     
    if TElASN1SimpleTag(CTag.GetField(0)).Content <> #0 then
      Exit;
    
    if (PublicModulusSize < Length(TElASN1SimpleTag(CTag.GetField(1)).Content)) or
      (PublicExponentSize < Length(TElASN1SimpleTag(CTag.GetField(2)).Content)) or
      (PrivateExponentSize < Length(TElASN1SimpleTag(CTag.GetField(3)).Content)) then
      Result := false
    else
      Result := true;
    PublicModulusSize := Length(TElASN1SimpleTag(CTag.GetField(1)).Content);
    PublicExponentSize := Length(TElASN1SimpleTag(CTag.GetField(2)).Content);
    PrivateExponentSize := Length(TElASN1SimpleTag(CTag.GetField(3)).Content);
    if Result then
    begin
      Move(TElASN1SimpleTag(CTag.GetField(1)).Content[1], PublicModulus^,
        PublicModulusSize);
      Move(TElASN1SimpleTag(CTag.GetField(2)).Content[1], PublicExponent^,
        PublicExponentSize);
      Move(TElASN1SimpleTag(CTag.GetField(3)).Content[1], PrivateExponent^,
        PrivateExponentSize);
    end;
  finally
    EncodedKey.Free;
  end;
end;

function DecodePrivateKey(Buffer : pointer; Size : integer; PublicModulus :
  pointer; var PublicModulusSize : integer; PublicExponent : pointer; var
  PublicExponentSize : integer; PrivateExponent : pointer; var PrivateExponentSize :
  integer; P : pointer; var PSize : integer; Q : pointer; var QSize : integer;
  E1 : pointer; var E1Size : integer; E2 : pointer; var E2Size : integer;
  U : pointer; var USize : integer) : boolean;
var
  EncodedKey : TElASN1ConstrainedTag;
  CTag : TElASN1ConstrainedTag;
  I : integer;
begin
  Result := false;

  EncodedKey := TElASN1ConstrainedTag.Create;
  try
    if not EncodedKey.LoadFromBuffer(Buffer, Size) then
      Exit;
    if (EncodedKey.Count < 1) or (EncodedKey.GetField(0).TagId <> SB_ASN1_SEQUENCE) or
      (not EncodedKey.GetField(0).IsConstrained) then
      Exit;
    
    CTag := TElASN1ConstrainedTag(EncodedKey.GetField(0));
    if CTag.Count <> 9 then
      Exit;
    
    for I := 0 to 8 do
      if (CTag.GetField(I).TagId <> SB_ASN1_INTEGER) or
        (CTag.GetField(I).IsConstrained) then
        Exit;
    if TElASN1SimpleTag(CTag.GetField(0)).Content <> #0 then
      Exit;

    if (PublicModulusSize < Length(TElASN1SimpleTag(CTag.GetField(1)).Content)) or
      (PublicExponentSize < Length(TElASN1SimpleTag(CTag.GetField(2)).Content)) or
      (PrivateExponentSize < Length(TElASN1SimpleTag(CTag.GetField(3)).Content)) or
      (PSize < Length(TElASN1SimpleTag(CTag.GetField(4)).Content)) or
      (QSize < Length(TElASN1SimpleTag(CTag.GetField(5)).Content)) or
      (E1Size < Length(TElASN1SimpleTag(CTag.GetField(6)).Content)) or
      (E2Size < Length(TElASN1SimpleTag(CTag.GetField(7)).Content)) or
      (USize < Length(TElASN1SimpleTag(CTag.GetField(8)).Content)) then
      Result := false
    else
      Result := true;
    PublicModulusSize := Length(TElASN1SimpleTag(CTag.GetField(1)).Content);
    PublicExponentSize := Length(TElASN1SimpleTag(CTag.GetField(2)).Content);
    PrivateExponentSize := Length(TElASN1SimpleTag(CTag.GetField(3)).Content);
    PSize := Length(TElASN1SimpleTag(CTag.GetField(4)).Content);
    QSize := Length(TElASN1SimpleTag(CTag.GetField(5)).Content);
    E1Size := Length(TElASN1SimpleTag(CTag.GetField(6)).Content);
    E2Size := Length(TElASN1SimpleTag(CTag.GetField(7)).Content);
    USize := Length(TElASN1SimpleTag(CTag.GetField(8)).Content);
    if Result then
    begin
      Move(TElASN1SimpleTag(CTag.GetField(1)).Content[1], PublicModulus^,
        PublicModulusSize);
      Move(TElASN1SimpleTag(CTag.GetField(2)).Content[1], PublicExponent^,
        PublicExponentSize);
      Move(TElASN1SimpleTag(CTag.GetField(3)).Content[1], PrivateExponent^,
        PrivateExponentSize);
      Move(TElASN1SimpleTag(CTag.GetField(4)).Content[1], P^, PSize);
      Move(TElASN1SimpleTag(CTag.GetField(5)).Content[1], Q^, QSize);
      Move(TElASN1SimpleTag(CTag.GetField(6)).Content[1], E1^, E1Size);
      Move(TElASN1SimpleTag(CTag.GetField(7)).Content[1], E2^, E2Size);
      Move(TElASN1SimpleTag(CTag.GetField(8)).Content[1], U^, USize);
    end;
  finally
    EncodedKey.Free;
  end;
end;

function EncodePrivateKey(PublicModulus : pointer; PublicModulusSize : integer;
  PublicExponent : pointer; PublicExponentSize : integer; PrivateExponent :
  pointer; PrivateExponentSize : integer; Prime1 : pointer; Prime1Size : integer;
  Prime2 : pointer; Prime2Size : integer; Exponent1 : pointer; Exponent1Size :
  integer; Exponent2 : pointer; Exponent2Size : integer; Coef : pointer;
  CoefSize : integer; OutBuffer : pointer; var OutSize : integer) : boolean;
var
  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
begin
  Tag := TElASN1ConstrainedTag.Create;
  Tag.TagId := SB_ASN1_SEQUENCE;
  { version }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := #0;
  try
    { modulus }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, PublicModulus, PublicModulusSize);
    { PublicExponent }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, PublicExponent, PublicExponentSize);
    { PrivateExponent }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, PrivateExponent, PrivateExponentSize);
    { Prime1 }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, Prime1, Prime1Size);
    { Prime2 }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, Prime2, Prime2Size);
    { Exponent1 }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, Exponent1, Exponent1Size);
    { Exponent2 }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, Exponent2, Exponent2Size);
    { Coef }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, Coef, CoefSize);
    Result := Tag.SaveToBuffer(OutBuffer, OutSize);
  finally
    Tag.Free;
  end;
end;

function EncodePrivateKey(PublicModulus : pointer; PublicModulusSize : integer;
  PublicExponent : pointer; PublicExponentSize : integer; PrivateExponent :
  pointer; PrivateExponentSize : integer; OutBuffer : pointer; var OutSize : integer) : boolean; overload;
var
  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
begin
  Tag := TElASN1ConstrainedTag.Create;
  Tag.TagId := SB_ASN1_SEQUENCE;
  { version }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := #0;
  try
    { modulus }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, PublicModulus, PublicModulusSize);
    { PublicExponent }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, PublicExponent, PublicExponentSize);
    { PrivateExponent }
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    asymWriteInteger(STag, PrivateExponent, PrivateExponentSize);
    Result := Tag.SaveToBuffer(OutBuffer, OutSize);
  finally
    Tag.Free;
  end;
end;

function Decrypt(InBuffer : pointer; InSize : integer; PrivateKeyBlob: pointer;
  PrivateKeyBlobSize : integer; OutBuffer: pointer; var OutSize : integer) : boolean;
var
  CTag, Tag: TElASN1ConstrainedTag;
  I : integer;
  M, D, P, Q, dP, dQ, qInv, M1, M2, Enc, Dec, Tmp : PLInt;
  Buf : array of byte;
  ModVal : BufferType;
  PublicModulusSize: integer;
begin
  Result := false;
  Tag := TElASN1ConstrainedTag.Create;
  try
  
    if not Tag.LoadFromBuffer(PrivateKeyBlob, PrivateKeyBlobSize) then
      Exit;

    if (not Tag.GetField(0).IsConstrained) or (Tag.Count <> 1) then
      Exit;

    CTag := TElASN1ConstrainedTag(Tag.GetField(0));
    if CTag.Count <> 9 then
      Exit;

    for I := 0 to 8 do
      if (CTag.GetField(I).IsConstrained) or (CTag.GetField(I).TagID <> SB_ASN1_INTEGER) then
        Exit;

    if OutSize < Length(TElASN1SimpleTag(CTag.GetField(1)).Content) then
    begin
      OutSize := Length(TElASN1SimpleTag(CTag.GetField(1)).Content);
      Exit;
    end;

    LCreate(M);
    LCreate(D);
    LCreate(P);
    LCreate(Q);
    LCreate(dP);
    LCreate(dQ);
    LCreate(qInv);
    LCreate(M1);
    LCreate(M2);
    LCreate(Enc);
    LCreate(Dec);
    LCreate(Tmp);
    PointerToLInt(M, @TElASN1SimpleTag(CTag.GetField(1)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(1)).Content));
    ModVal := TElASN1SimpleTag(CTag.GetField(1)).Content;
    PointerToLInt(D, @TElASN1SimpleTag(CTag.GetField(3)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(3)).Content));
    PointerToLInt(P, @TElASN1SimpleTag(CTag.GetField(4)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(4)).Content));
    PointerToLInt(Q, @TElASN1SimpleTag(CTag.GetField(5)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(5)).Content));
    PointerToLInt(dP, @TElASN1SimpleTag(CTag.GetField(6)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(6)).Content));
    PointerToLInt(dQ, @TElASN1SimpleTag(CTag.GetField(7)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(7)).Content));
    PointerToLInt(qInv, @TElASN1SimpleTag(CTag.GetField(8)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(8)).Content));
    PointerToLInt(Enc, InBuffer, InSize);
  finally
    Tag.Free;
  end;
  LMModPower(Enc, dP, P, M1);
  LMModPower(Enc, dQ, Q, M2);
  if LGreater(M1, M2) then
    LSub(M1, M2, Tmp)
  else
  begin
    LSub(M2, M1, Dec);
    if LGreater(P, Dec) then
      LSub(P, Dec, Tmp)
    else
    begin
      LModEx(Dec, P, Tmp);
      LSub(P, Tmp, Dec);
      LCopy(Tmp, Dec);
    end;
  end;
  LMult(Tmp, qInv, Dec);
  LModEx(Dec, P, Tmp);
  LMult(Tmp, Q, dQ);
  LAdd(M2, dQ, Dec);
  I := Dec.Length * 4;
  SetLength(Buf, I);
  LIntToPointer(Dec, Buf, I);
  LDestroy(M);
  LDestroy(D);
  LDestroy(P);
  LDestroy(Q);
  LDestroy(dP);
  LDestroy(dQ);
  LDestroy(qInv);
  LDestroy(M1);
  LDestroy(M2);
  LDestroy(Enc);
  LDestroy(Dec);
  LDestroy(Tmp);
  SetLength(Buf, I);

  PublicModulusSize := Length(ModVal);
  if (I > PublicModulusSize) then
  begin
    // LIntToPointer returns value with length divisible by 4
    // Therefore it may contain prefix zeros if PublicModulusSize is
    // not divisible by 4
    Move(Buf[I - PublicModulusSize], Buf[0], PublicModulusSize);
    SetLength(Buf, PublicModulusSize);
  end;
  
  if (Buf[0] <> 0) or ((Buf[1] <> 2) and (Buf[1] <> 1)) then
  begin
    Result := false;
    Exit;
  end;
  I := 2;
  while (I < Length(Buf)) and (Buf[I] <> 0) do
    Inc(I);
  Move(Buf[I + 1], OutBuffer^, Length(Buf) - I - 1);
  OutSize := Length(Buf) - I - 1;
  Result := true;
end;

function Sign(Hash : pointer; HashSize : integer; PrivateKeyBlob: pointer;
  PrivateKeyBlobSize : integer; Signature: pointer; var SignatureSize : integer) : boolean;
var
  CTag, Tag: TElASN1ConstrainedTag;
  I : integer;
  M, D, P, Q, dP, dQ, qInv, M1, M2, Enc, Dec, Tmp : PLInt;
  Buf: array of byte;
  Delta, ModLen : integer;
begin
  Result := false;
  Tag := TElASN1ConstrainedTag.Create;
  try

    if not Tag.LoadFromBuffer(PrivateKeyBlob, PrivateKeyBlobSize) then
      Exit;

    if (not Tag.GetField(0).IsConstrained) or (Tag.Count <> 1) then
      Exit;

    CTag := TElASN1ConstrainedTag(Tag.GetField(0));
    if CTag.Count <> 9 then
      Exit;

    for I := 0 to 8 do
      if (CTag.GetField(I).IsConstrained) or (CTag.GetField(I).TagID <> SB_ASN1_INTEGER) then
        Exit;

    if SignatureSize < Length(TElASN1SimpleTag(CTag.GetField(1)).Content) then
    begin
      SignatureSize := Length(TElASN1SimpleTag(CTag.GetField(1)).Content);
      Exit;
    end;

    if HashSize > Length(TElASN1SimpleTag(CTag.GetField(1)).Content) - 11 then
    begin
      SignatureSize := 0;
      Result := false;
      Exit;
    end;

    LCreate(M);
    LCreate(D);
    LCreate(P);
    LCreate(Q);
    LCreate(dP);
    LCreate(dQ);
    LCreate(qInv);
    LCreate(M1);
    LCreate(M2);
    LCreate(Enc);
    LCreate(Dec);
    LCreate(Tmp);
    PointerToLInt(M, @TElASN1SimpleTag(CTag.GetField(1)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(1)).Content));
    PointerToLInt(D, @TElASN1SimpleTag(CTag.GetField(3)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(3)).Content));
    PointerToLInt(P, @TElASN1SimpleTag(CTag.GetField(4)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(4)).Content));
    PointerToLInt(Q, @TElASN1SimpleTag(CTag.GetField(5)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(5)).Content));
    PointerToLInt(dP, @TElASN1SimpleTag(CTag.GetField(6)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(6)).Content));
    PointerToLInt(dQ, @TElASN1SimpleTag(CTag.GetField(7)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(7)).Content));
    PointerToLInt(qInv, @TElASN1SimpleTag(CTag.GetField(8)).Content[1],
      Length(TElASN1SimpleTag(CTag.GetField(8)).Content));
    Delta := 0;
    ModLen := Length(TElASN1SimpleTag(CTag.GetField(1)).Content);
    while (TElASN1SimpleTag(CTag.GetField(1)).Content[Delta + 1] = #0) and (Delta <= ModLen) do
      Inc(Delta);
    SetLength(Buf, Length(TElASN1SimpleTag(CTag.GetField(1)).Content) - Delta);
    Buf[0] := 0;
    Buf[1] := 1;
    for I := 2 to Length(Buf) - HashSize - 2 do
      Buf[I] := 255;
    Buf[Length(Buf) - HashSize - 1] := 0;
    Move(Hash^, Buf[Length(Buf) - HashSize], HashSize);
    PointerToLInt(Enc, @Buf[0], Length(Buf));

  finally
    Tag.Free;
  end;
  
  LMModPower(Enc, dP, P, M1);
  LMModPower(Enc, dQ, Q, M2);
  if LGreater(M1, M2) then
    LSub(M1, M2, Tmp)
  else
  begin
    LSub(M2, M1, Dec);
    if LGreater(P, Dec) then
      LSub(P, Dec, Tmp)
    else
    begin
      LModEx(Dec, P, Tmp);
      LSub(P, Tmp, Dec);
      LCopy(Tmp, Dec);
    end;
  end;
  LMult(Tmp, qInv, Dec);
  LModEx(Dec, P, Tmp);
  LMult(Tmp, Q, dQ);
  LAdd(M2, dQ, Dec);
  LIntToPointer(Dec, Signature, SignatureSize);
  LDestroy(M);
  LDestroy(D);
  LDestroy(P);
  LDestroy(Q);
  LDestroy(dP);
  LDestroy(dQ);
  LDestroy(qInv);
  LDestroy(M1);
  LDestroy(M2);
  LDestroy(Enc);
  LDestroy(Dec);
  LDestroy(Tmp);
  Result := true;
end;

function IsValidKey(Blob: pointer; BlobSize: integer) : boolean;
var
  M, E, D, P, Q, E1, E2, U : ByteArray;
  MSize, ESize, DSize, PSize, QSize, E1Size, E2Size, USize : integer;
  LP, LQ, LM, LTmp : PLInt;
begin
  Result := false;
  MSize := 0;
  DecodePrivateKey(Blob, BlobSize, nil, MSize, nil, ESize, nil, DSize,
    nil, PSize, nil, QSize, nil, E1Size, nil, E2Size, nil, USize);

  SetLength(M, MSize);
  SetLength(E, ESize);
  SetLength(D, DSize);
  SetLength(P, PSize);
  SetLength(Q, QSize);
  SetLength(E1, E1Size);
  SetLength(E2, E2Size);
  SetLength(U, USize);

  if not DecodePrivateKey(Blob, BlobSize, M, MSize, E, ESize, D, DSize,
    P, PSize, Q, QSize, E1, E1Size, E2, E2Size, U, USize) then
    Exit;

  LCreate(LP);
  LCreate(LQ);
  LCreate(LM);
  LCreate(LTmp);
  try
    PointerToLInt(LP, P, PSize);
    PointerToLInt(LQ, Q, QSize);
    // P and Q must be primes
    if (not LIsPrime(LP)) or (not (LIsPrime(LQ))) then
      Exit;

    // P * Q == M
    PointerToLInt(LM, M, MSize);
    LMult(LP, LQ, LTmp);
    if not LEqual(LTmp, LM) then
      Exit;

    // DE = 1 mod ((p-1)(q-1))
    LDec(LP);
    LDec(LQ);
    LMult(LP, LQ, LTmp);
    PointerToLInt(LP, D, DSize);
    PointerToLInt(LQ, E, ESize);
    LMult(LP, LQ, LM);
    LMod(LM, LTmp, LP);
    LDec(LP);
    Result := LNull(LP);
  finally
    LDestroy(LP);
    LDestroy(LQ);
    LDestroy(LM);
    LDestroy(LTmp);
  end;
end;

const
  EMPTY_MSG_SHA1 : array [0..19] of byte = 
   ( 
    $da, $39, $a3, $ee, $5e, $6b, $4b, $0d, $32, $55, $bf, $ef, $95, $60, $18, $90, $af, $d8, $07, $09
   );
  EMPTY_MSG_SHA256 : array [0..31] of byte  = 
   (
    $e3, $b0, $c4, $42, $98, $fc, $1c, $14, $9a, $fb, $f4, $c8, $99, $6f, $b9, $24, $27, $ae, $41, $e4, $64, $9b, $93, $4c, $a4, $95, $99, $1b, $78, $52, $b8, $55
   );
  EMPTY_MSG_SHA384 : array [0..47] of byte = 
   (
    $38, $b0, $60, $a7, $51, $ac, $96, $38, $4c, $d9, $32, $7e, $b1, $b1, $e3, $6a, $21, $fd, $b7, $11, $14, $be, $07, $43, $4c, $0c, $c7, $bf, $63, $f6, $e1, $da, $27, $4e, $de, $bf, $e7, $6f, $65, $fb, $d5, $1a, $d2, $f1, $48, $98, $b9, $5b
   ); 
  EMPTY_MSG_SHA512 : array [0..63] of byte = 
   (
    $cf, $83, $e1, $35, $7e, $ef, $b8, $bd, $f1, $54, $28, $50, $d6, $6d, $80, $07, $d6, $20, $e4, $05, $0b, $57, $15, $dc, $83, $f4, $a9, $21, $d3, $6c, $e9, $ce, $47, $d0, $d1, $3c, $5d, $85, $f2, $b0, $ff, $83, $18, $d2, $87, $7e, $ec, $2f, $63, $b9, $31, $bd, $47, $41, $7a, $81, $a5, $38, $32, $7a, $f9, $27, $da, $3e
   );

function CalculateHash(Algorithm : integer; Buffer : pointer; Size : integer) : ByteArray;
var
  M128 : TMessageDigest128;
  M160 : TMessageDigest160;
  M224 : TMessageDigest224;  
  M256 : TMessageDigest256;
  M384 : TMessageDigest384;
  M512 : TMessageDigest512;
  
  function CloneArray(const Arr : array of byte) : ByteArray;
  begin
    SetLength(Result, Length(Arr));
    Move(Arr[0], Result[0], Length(Result));
  end;
  
begin
  case Algorithm of
    SB_ALGORITHM_DGST_SHA1:
    begin
      if Size > 0 then
      begin
        M160 := HashSHA1(Buffer, Size);
        SetLength(Result, 20);
        Move(M160, Result[0], Length(Result));
      end
      else
        Result := CloneArray(EMPTY_MSG_SHA1);
    end;
    SB_ALGORITHM_DGST_MD5:
    begin
      M128 := HashMD5(Buffer, Size);
      SetLength(Result, 16);
      Move(M128, Result[0], Length(Result));
    end;
    SB_ALGORITHM_DGST_MD2:
    begin
      M128 := HashMD2(Buffer, Size);
      SetLength(Result, 16);
      Move(M128, Result[0], Length(Result));
    end;
    SB_ALGORITHM_DGST_SHA224:
    begin
      if Size > 0 then
      begin
        M224 := HashSHA224(Buffer, Size);
        SetLength(Result, 28);
        Move(M224, Result[0], Length(Result));
      end;
    end;
    SB_ALGORITHM_DGST_SHA256:
    begin
      if Size > 0 then
      begin
        M256 := HashSHA256(Buffer, Size);
        SetLength(Result, 32);
        Move(M256, Result[0], Length(Result));
      end
      else
        Result := CloneArray(EMPTY_MSG_SHA256);
    end;
    SB_ALGORITHM_DGST_SHA384:
    begin
      if Size > 0 then
      begin
        M384 := HashSHA384(Buffer, Size);
        SetLength(Result, 48);
        Move(M384, Result[0], Length(Result));
      end
      else
        Result := CloneArray(EMPTY_MSG_SHA384);
    end;
    SB_ALGORITHM_DGST_SHA512:
    begin
      if Size > 0 then
      begin
        M512 := HashSHA512(Buffer, Size);
        SetLength(Result, 64);
        Move(M512, Result[0], Length(Result));
      end
      else
        Result := CloneArray(EMPTY_MSG_SHA512);
    end;
  end;
end;

function MGF1(InBuffer: pointer; InSize : integer; Needed: integer; HashAlg : integer) : ByteArray;
var
  hLen : integer;
  I, OldLen : integer;
  Cnt : array[0..3] of byte;
  HashInput : ByteArray;
  Hash : ByteArray;
begin
  SetLength(Result, 0);
  SetLength(Hash, 0);
  hLen := GetDigestSizeBits(HashAlg);
  if (hLen = -1) or (Needed = 0) then
    Exit;

  hLen := hLen shr 3;
  SetLength(HashInput, InSize + 4);

  Move(InBuffer^, HashInput[0], InSize);
  I := 0;
  while Length(Result) < Needed do
  begin
    Cnt[0] := I shr 24;
    Cnt[1] := (I shr 16) and $ff;
    Cnt[2] := (I shr 8) and $ff;
    Cnt[3] := I and $ff;
    Move(Cnt[0], HashInput[InSize], 4);
    Hash := CalculateHash(HashAlg, @HashInput[0], InSize + 4);
    OldLen := Length(Result);
    SetLength(Result, OldLen + hLen);
    Move(Hash[0], Result[OldLen], hLen);
    Inc(I);
  end;
  SetLength(Result, Needed);
end;

function PerformExponentiation(Modulus: pointer; ModulusSize: integer;
  Exponent: pointer; ExponentSize: integer; InBuffer: pointer; InSize: integer;
  OutBuffer: pointer; var OutSize : integer): boolean;
var
  X, E, N, R : PLInt;
begin
  if OutSize < ModulusSize then
  begin
    Result := false;
    Exit;
  end;
  LCreate(X);
  LCreate(E);
  LCreate(N);
  LCreate(R);
  try
    PointerToLInt(X, InBuffer, InSize);
    PointerToLInt(E, Exponent, ExponentSize);
    PointerToLInt(N, Modulus, ModulusSize);
    LMModPower(X, E, N, R);
    LIntToPointer(R, OutBuffer, OutSize);
  finally
    LDestroy(X);
    LDestroy(E);
    LDestroy(N);
    LDestroy(R);
  end;
  Result := true;
end;

function EncryptOAEP(InBuffer : pointer; InSize : integer; PublicModulus : pointer;
  PublicModulusSize : integer; PublicExponent : pointer; PublicExponentSize :
  integer; Salt : pointer; SaltSize : integer; HashAlg : integer;
  OutBuffer : pointer; var OutSize : integer) : boolean;
var
  hLen, padLen : integer;
  Hash : ByteArray;
  PS : ByteArray;
  DataBlock : ByteArray;
  Seed : ByteArray;
  DataMask, SeedMask : ByteArray;
  I : integer;
  EncSource : ByteArray;
  Ptr : ^byte;
begin
  Result := false;
  hLen := GetDigestSizeBits(HashAlg);
  if (hLen = -1) then
    Exit;
  hLen := hLen shr 3;
  Ptr := PublicModulus;
  while (PublicModulusSize > 0) and (Ptr^ = 0) do
  begin
    Inc(Ptr);
    Dec(PublicModulusSize);
  end;
  PublicModulus := Ptr;
  if InSize > PublicModulusSize - 2 * hLen - 2 then
    Exit;
  if OutSize < PublicModulusSize then
  begin
    OutSize := PublicModulusSize;
    Exit;
  end;
  Hash := CalculateHash(HashAlg, Salt, SaltSize);
  padLen := PublicModulusSize - InSize - 2 * hLen - 2;

  SetLength(PS, padLen);
  FillChar(PS[0], padLen, 0);

  SetLength(DataBlock, hLen + padLen + InSize + 1);
  Move(Hash[0], DataBlock[0], hLen);
  Move(PS[0], DataBlock[hLen], padLen);
  DataBlock[hLen + padLen] := 1;
  Move(InBuffer^, DataBlock[hLen + padLen + 1], InSize);

  SetLength(Seed, hLen);
  SBRndGenerate(@Seed[0], hLen);
  DataMask := MGF1(@Seed[0], hLen, PublicModulusSize - hLen - 1, HashAlg);
  for I := 0 to Length(DataBlock) - 1 do
    DataBlock[I] := DataBlock[I] xor DataMask[I];
  SeedMask := MGF1(@DataBlock[0], Length(DataBlock), hLen, HashAlg);
  for I := 0 to Length(Seed) - 1 do
    Seed[I] := Seed[I] xor SeedMask[I];

  SetLength(EncSource, PublicModulusSize);
  EncSource[0] := 0;
  Move(Seed[0], EncSource[1], hLen);
  Move(DataBlock[0], EncSource[1 + hLen], Length(DataBlock));
  Result := PerformExponentiation(PublicModulus, PublicModulusSize, PublicExponent,
    PublicExponentSize, @EncSource[0], Length(EncSource), OutBuffer, OutSize);
end;

function DecryptOAEP(InBuffer : pointer; InSize : integer; PublicModulus : pointer;
  PublicModulusSize : integer; PrivateExponent : pointer; PrivateExponentSize :
  integer; Salt: pointer; SaltSize : integer; HashAlg: integer; OutBuffer : pointer;
  var OutSize : integer) : boolean;
var
  hLen : integer;
  Ptr : ^byte;
  DecBuffer : ByteArray;
  DecSize : integer;
  Hash : ByteArray;
  Seed : ByteArray;
  DataBlock : ByteArray;
  DataMask, SeedMask : ByteArray;
  I : integer;
  MsgHash : ByteArray;
  Size : integer;
begin
  Result := false;
  hLen := GetDigestSizeBits(HashAlg);
  if (hLen = -1) then
    Exit;
  hLen := hLen shr 3;
  if OutSize < PublicModulusSize - 2 * hLen - 2 then
  begin
    OutSize := PublicModulusSize - 2 * hLen - 2;
    Exit;
  end;
  Ptr := PublicModulus;
  while (PublicModulusSize > 0) and (Ptr^ = 0) do
  begin
    Inc(Ptr);
    Dec(PublicModulusSize);
  end;
  PublicModulus := Ptr;
  Ptr := InBuffer;
  while (InSize > 0) and (Ptr^ = 0) do
  begin
    Inc(Ptr);
    Dec(InSize);
  end;
  InBuffer := Ptr;
  if InSize > PublicModulusSize then
    Exit;

  if PublicModulusSize < 2 * hLen + 2 then
    Exit;

  DecSize := (((PublicModulusSize - 1) shr 2) + 1) shl 2;
  SetLength(DecBuffer, DecSize);
  PerformExponentiation(PublicModulus, PublicModulusSize, PrivateExponent,
    PrivateExponentSize, InBuffer, InSize, @DecBuffer[0], DecSize);
  Ptr := @DecBuffer[0];
  while (DecSize > 0) and (Ptr^ = 0) do
  begin
    Inc(Ptr);
    Dec(DecSize);
  end;
  if DecSize > PublicModulusSize - 1 then
    Exit;

  Hash := CalculateHash(HashAlg, Salt, SaltSize);
  SetLength(Seed, hLen);
  Move(Ptr^, Seed[0], hLen);
  Inc(Ptr, hLen);

  SetLength(DataBlock, PublicModulusSize - hLen - 1);
  Move(Ptr^, DataBlock[0], Length(DataBlock));
  SeedMask := MGF1(@DataBlock[0], Length(DataBlock), hLen, HashAlg);
  for I := 0 to hLen - 1 do
    Seed[I] := Seed[I] xor SeedMask[I];
  DataMask := MGF1(@Seed[0], hLen, Length(DataBlock), HashAlg);
  for I := 0 to Length(DataBlock) - 1 do
    DataBlock[I] := DataBlock[I] xor DataMask[I];

  SetLength(MsgHash, hLen);
  Move(DataBlock[0], MsgHash[0], hLen);
  if (not CompareMem(@MsgHash[0], @Hash[0], hLen)) then
    Exit;
  Ptr := @DataBlock[hLen];
  Size := Length(DataBlock) - hLen;
  while (Size > 0) and (Ptr^ = 0) do
  begin
    Inc(Ptr);
    Dec(Size);
  end;
  if (Size = 0) or (Ptr^ <> 1) then
    Exit;
  Inc(Ptr);
  Dec(Size);
  OutSize := Size;
  Move(Ptr^, OutBuffer^, OutSize);
  Result := true;
end;


function GetPublicKeySizeBits(PrivateKeyBlob : pointer;
  PrivateKeyBlobSize : integer) : integer; overload;
var
  N, E, D : ByteArray;
  B : PLint;
  NSize, ESize, DSize : integer;
begin
  Result := 0;
  NSize := 0;
  ESize := 0;
  DSize := 0;

  DecodePrivateKey(PrivateKeyBlob, PrivateKeyBlobSize, nil, NSize, nil,
    ESize, nil, DSize);

  SetLength(N, NSize);
  SetLength(E, ESize);
  SetLength(D, DSize);

  if not DecodePrivateKey(PrivateKeyBlob, 
    PrivateKeyBlobSize, N, NSize, E, ESize, D, DSize)
  then
    Exit;

  LCreate(B);
  PointerToLInt(B, N, NSize);
  Result := LBitCount(B);
  LDestroy(B);
end;

{ PKCS#1 v2.1 RSASP1 signing primitive }
function SignRSASP1(EMessage : pointer; EMessageSize : integer;
  KeyBlob : pointer; KeyBlobSize : integer; Signature : pointer;
  var SignatureSize : integer) : boolean;
var
  N, E, D, S : ByteArray;
  NSize, ESize, DSize, SSize, Index : integer;
  LN, LD, LM, LS : PLInt;
begin
  Result := false;

  DecodePrivateKey(KeyBlob, KeyBlobSize, nil, NSize, nil, ESize, nil, DSize);

  SetLength(N, NSize);
  SetLength(E, ESize);
  SetLength(D, DSize);

  if not DecodePrivateKey(KeyBlob, KeyBlobSize, N, NSize, E, ESize, D, DSize)
  then
    Exit;

  SSize := 0;
  while (SSize < NSize) and (N[SSize] = 0) do
    Inc(SSize);

  SSize := NSize - SSize;  

  if SignatureSize < SSize then
  begin
    SignatureSize := SSize;
    Exit;
  end;


  LCreate(LN);
  LCreate(LD);
  LCreate(LM);
  LCreate(LS);

  try
    PointerToLInt(LN, N, NSize);
    PointerToLInt(LM, EMessage, EMessageSize);

    if LGreater(LN, LM) then
    begin
      PointerToLInt(LD, D, DSize);
      LModPower(LM, LD, LN, LS);
      SetLength(S, LS.Length * 4);
      LIntToPointer(LS, @S[0], Index);
      Move(S[4*LS.Length - SSize], Signature^, SSize);
      SignatureSize := SSize;
      Result := true;
    end;
  finally
    LDestroy(LN);
    LDestroy(LD);
    LDestroy(LM);
    LDestroy(LS);
  end;
end;

{ PKCS#1 v2.1 RSAVP1 signature verification }
function VerifyRSAVP1(Signature : pointer; SignatureSize : integer;
  PublicModulus : pointer; PublicModulusSize : integer;
  PublicExponent : pointer; PublicExponentSize : integer;
  EMessage : pointer; var EMessageSize : integer) : boolean;
begin
  Result := false;

  if EMessageSize < PublicModulusSize then
  begin
    EMessageSize := PublicModulusSize;
    Exit;
  end;

  Result := PerformExponentiation(PublicModulus, PublicModulusSize,
    PublicExponent, PublicExponentSize, Signature, SignatureSize,
    EMessage, EMessageSize);
end;

{ PKCS#1 v2.1 EMSA-PSS-ENCODE }
function EncodePSS(HashValue : pointer; HashValueLen : integer;
  HashAlgorithm : integer; SaltLen : integer; mBits : integer;
  EMessage : pointer; var EMessageLen : integer) : boolean;
var
  Index, emLen, hLen : integer;
  MTemp, DB, DBMask : ByteArray;
  H : BufferType;
  HashFunction : TElHashFunction;
  Salt : ByteArray;
begin
  Result := false;

  if not TElHashFunction.IsAlgorithmSupported(HashAlgorithm) then
    Exit;

  SetLength(Salt, SaltLen);  
  if SaltLen > 0 then
    SBRndGenerate(@Salt[0], SaltLen);

  emLen := (mBits + 7) div 8;

  if emLen > EMessageLen then
  begin
    EMessageLen := emLen;
    Exit;
  end;

  if HashValueLen + SaltLen + 2 > emLen then
    Exit;

  SetLength(MTemp, 8 + SaltLen + HashValueLen);

  FillChar(MTemp[0], 8, 0);
  Move(HashValue^, MTemp[8], HashValueLen);
  if SaltLen > 0 then
    Move(Salt[0], MTemp[8 + HashValueLen], SaltLen);

  hLen := TElHashFunction.GetDigestSizeBits(HashAlgorithm) div 8;
  HashFunction := TElHashFunction.Create(HashAlgorithm);
  HashFunction.Update(@MTemp[0], Length(MTemp));
  H := HashFunction.Finish;
  HashFunction.Free;

  SetLength(DB, emLen - hLen - 1);
  SetLength(DBMask, emLen - hLen - 1);
  FillChar(DB[0], emLen - SaltLen - hLen - 2, 0);
  DB[emLen - SaltLen - hLen - 2] := $01;
  Move(Salt[0], DB[emLen - SaltLen - hLen - 1], SaltLen);
  DBMask := MGF1(@H[1], Length(H), emLen - hLen - 1, HashAlgorithm);

  for Index := 0 to emLen - hLen - 2 do
    DB[Index] := DB[Index] xor DBMask[Index];

  { setting leftmost bits to zero }
  for Index := 1 to 8 * emLen - mBits do
    DB[0] := DB[0] and (not (1 shl (8 - Index)));

  Move(DB[0], EMessage^, emLen - hLen - 1);
  Move(H[1], PByteArray(EMessage)[emLen - hLen - 1], hLen);
  PByteArray(EMessage)[emLen - 1] := $bc;

  Result := true;
end;

function SignPSS(HashValue : pointer; HashValueSize : integer;
  HashAlgorithm : integer; SaltSize : integer;
  PublicModulus : pointer; PublicModulusSize : integer;
  PublicExponent : pointer; PublicExponentSize : integer;
  PrivateExponent : pointer; PrivateExponentSize : integer;
  Signature : pointer; var SignatureSize : integer) : boolean;
var
  KeyBlob : ByteArray;
  KeyBlobSize : integer;
begin
  Result := false;
  KeyBlobSize := 0;
  EncodePrivateKey(PublicModulus, PublicModulusSize, PublicExponent, PublicExponentSize,
    PrivateExponent, PrivateExponentSize, nil, KeyBlobSize);
  if KeyBlobSize = 0 then Exit;

  SetLength(KeyBlob, KeyBlobSize);

  if not EncodePrivateKey(PublicModulus, PublicModulusSize, PublicExponent, PublicExponentSize,
    PrivateExponent, PrivateExponentSize, @KeyBlob[0], KeyBlobSize)
  then
    Exit;

  Result := SignPSS(HashValue, HashValueSize, HashAlgorithm, SaltSize,
    @KeyBlob[0], KeyBlobSize, Signature, SignatureSize);
end;

function SignPSS(HashValue : pointer; HashValueSize : integer;
  HashAlgorithm : integer; SaltSize : integer; KeyBlob : pointer;
  KeyBlobSize : integer; Signature : pointer;
  var SignatureSize : integer) : boolean;
var
  EMessage : ByteArray;
  EMessageLen : integer;
  mBits : integer;
  N, E, D : ByteArray;
  NSize, ESize, DSize, SSize : integer;
begin
  Result := false;
  NSize := 0; ESize := 0; DSize := 0;
  DecodePrivateKey(KeyBlob, KeyBlobSize, nil, NSize, nil, ESize, nil, DSize);

  SetLength(N, NSize);
  SetLength(E, ESize);
  SetLength(D, DSize);

  if not DecodePrivateKey(KeyBlob, KeyBlobSize, N, NSize, E, ESize, D, DSize)
  then
    Exit;

  SSize := 0;
  while (SSize < NSize) and (N[SSize] = 0) do
    Inc(SSize);

  SSize := NSize - SSize;

  if SignatureSize < SSize then
  begin
    SignatureSize := SSize;
    Exit;
  end;

  mBits := GetPublicKeySizeBits(KeyBlob, KeyBlobSize);

  if mBits = 0 then
  begin
    SignatureSize := 0;
    Exit;
  end;

  mBits := mBits - 1;  

  EMessageLen := (mBits + 7) div 8;

  if (EMessageLen <= 0) or (SignatureSize < EMessageLen) then
  begin
    if EMessageLen > 0 then SignatureSize := EMessageLen;
    Exit;
  end;

  SetLength(EMessage, EMessageLen);

  if not EncodePSS(HashValue, HashValueSize, HashAlgorithm, SaltSize, mBits,
    @EMessage[0], EMessageLen)
  then
    Exit;

  Result := SignRSASP1(@EMessage[0], EMessageLen, KeyBlob, KeyBlobSize,
    Signature, SignatureSize);
end;

function VerifyPSS(HashValue : pointer; HashValueSize : integer;
  HashAlgorithm : integer; SaltSize : integer;
  PublicModulus : pointer; PublicModulusSize : integer;
  PublicExponent : pointer; PublicExponentSize : integer; Signature : pointer;
  SignatureSize : integer) : boolean;
var
  EMessage, DB, DBMask, H, Salt, TmpMessage : ByteArray;
  Index, hLen, emBits, emLen : integer;
  HashFunction : TElHashFunction;
  MHash : BufferType;
  N : PLInt;
begin
  Result := false;

  if not TElHashFunction.IsAlgorithmSupported(HashAlgorithm) then
    Exit;

  hLen := TElHashFunction.GetDigestSizeBits(HashAlgorithm) div 8;

  if hLen <> HashValueSize then
    Exit;

  LCreate(N);
  PointerToLInt(N, PublicModulus, PublicModulusSize);
  emBits := LBitCount(N);
  LDestroy(N);

  if emBits = 0 then
    Exit;

  Dec(emBits);  

  emLen := PublicModulusSize;
  SetLength(EMessage, emLen);

  if not VerifyRSAVP1(Signature, SignatureSize, PublicModulus, PublicModulusSize,
    PublicExponent, PublicExponentSize, @EMessage[0], emLen) then
    Exit;

  if emLen < hLen + SaltSize + 2 then
    Exit;

  if EMessage[emLen - 1] <> $bc then
    Exit;

  SetLength(DB, emLen - hLen - 1);
  SetLength(H, hLen);

  Move(EMessage[0], DB[0], emLen - hLen - 1);
  Move(EMessage[emLen - hLen - 1], H[0], hLen);

  DBMask := MGF1(@H[0], hLen, emLen - hLen - 1, HashAlgorithm);

  for Index := 1 to (8*emLen - emBits) do
    if (DB[0] and (1 shl (8 - Index))) <> 0 then
      Exit;

  for Index := 0 to emLen - hLen - 2 do
    DB[Index] := DB[Index] xor DBMask[Index];

  for Index := 1 to 8 * emLen - emBits do
    DB[0] := DB[0] and (not (1 shl (8 - Index)));

  for Index := 0 to emLen - hLen - SaltSize - 3 do
    if DB[Index] <> 0 then Exit;

  if DB[emLen - hLen - SaltSize - 2] <> $01 then Exit;

  SetLength(Salt, SaltSize);
  SetLength(TmpMessage, 8 + hLen + SaltSize);

  Move(DB[emLen - hLen - SaltSize - 1], Salt[0], SaltSize);
  FillChar(TmpMessage[0], 8, 0);
  Move(HashValue^, TmpMessage[8], hLen);
  if SaltSize > 0 then
    Move(Salt[0], TmpMessage[8 + hLen], SaltSize);

  HashFunction := TElHashFunction.Create(HashAlgorithm);
  HashFunction.Update(@TmpMessage[0], 8 + SaltSize + hLen);
  MHash := HashFunction.Finish;
  HashFunction.Free;

  if CompareMem(@H[0], @MHash[1], hLen) then
    Result := true
end;

function WritePSSParams(HashAlgorithm : integer {$ifdef HAS_DEF_PARAMS}= SB_ALGORITHM_DGST_SHA1{$endif};
  SaltSize : integer {$ifdef HAS_DEF_PARAMS}= 20{$endif};
  MGFAlgorithm : integer {$ifdef HAS_DEF_PARAMS}= SB_CERT_MGF1{$endif};
  TrailerField : integer {$ifdef HAS_DEF_PARAMS}= 1{$endif}) : BufferType;
var
  Tag : TElASN1SimpleTag;
  cTag, aTag : TElASN1ConstrainedTag;
  Size : integer;
begin
  Result := EmptyBuffer;

  if MGFAlgorithm <> SB_CERT_MGF1 then Exit;

  cTag := TElASN1ConstrainedTag.Create;
  cTag.TagId := SB_ASN1_SEQUENCE;

  { hash algorithm }

  if HashAlgorithm <> SB_ALGORITHM_DGST_SHA1 then
  begin
    aTag := TElASN1ConstrainedTag(cTag.GetField(cTag.AddField(True)));
    aTag.TagId := SB_ASN1_A0;
    aTag := TElASN1ConstrainedTag(aTag.GetField(aTag.AddField(true)));

    aTag.TagId := SB_ASN1_SEQUENCE;

    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(false)));
    Tag.TagId := SB_ASN1_OBJECT;
    Tag.Content := GetOIDByHashAlgorithm(HashAlgorithm);
    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(false)));
    Tag.TagId := SB_ASN1_NULL;
  end;

  { MGF }

  if (HashAlgorithm <> SB_ALGORITHM_DGST_SHA1) then
  begin
    aTag := TElASN1ConstrainedTag(cTag.GetField(cTag.AddField(True)));
    aTag.TagId := SB_ASN1_A1;
    aTag := TElASN1ConstrainedTag(aTag.GetField(aTag.AddField(true)));

    aTag.TagId := SB_ASN1_SEQUENCE;
    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(False)));
    Tag.TagId := SB_ASN1_OBJECT;
    Tag.Content := SB_OID_MGF1;
    aTag := TElASN1ConstrainedTag(aTag.GetField(aTag.AddField(True)));
    aTag.TagId := SB_ASN1_SEQUENCE;
    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(false)));
    Tag.TagId := SB_ASN1_OBJECT;
    Tag.Content := GetOIDByHashAlgorithm(HashAlgorithm);
    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(false)));
    Tag.TagId := SB_ASN1_NULL;
  end;

  { SaltSize }

  aTag := TElASN1ConstrainedTag(cTag.GetField(cTag.AddField(true)));
  aTag.TagId := SB_ASN1_A2;
  Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(False)));
  Tag.TagId := SB_ASN1_INTEGER;
  ASN1WriteInteger(Tag, SaltSize);

  { TrailerField}

  if TrailerField <> 1 then
  begin
    aTag := TElASN1ConstrainedTag(cTag.GetField(cTag.AddField(true)));
    aTag.TagId := SB_ASN1_A3;
    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(False)));
    Tag.TagId := SB_ASN1_INTEGER;
    ASN1WriteInteger(Tag, TrailerField);
  end;

  Size := 0;
  cTag.SaveToBuffer(nil, Size);

  SetLength(Result, Size);
  if not cTag.SaveToBuffer(@Result[1], Size) then
    Result := EmptyBuffer;

  cTag.Free;
end;

function ReadPSSParams(InBuffer : pointer; InBufferSize : integer;
  var HashAlgorithm, SaltSize, MGF, MGFHashAlgorithm, TrailerField : integer) : boolean;
var
  CTag, STag, ATag : TElASN1ConstrainedTag;
  Index,TagNum : integer;
begin
  Result := false;
  HashAlgorithm := SB_ALGORITHM_DGST_SHA1;
  SaltSize := 20;
  MGF := SB_CERT_MGF1;
  MGFHashAlgorithm := SB_ALGORITHM_DGST_SHA1;
  TrailerField := 1;

  CTag := TElASN1ConstrainedTag.Create;
  try
    CTag.LoadFromBuffer(InBuffer, InBufferSize);
    if (CTag.Count <> 1) then Exit;

    if CTag.GetField(0).CheckType(SB_ASN1_NULL, false) then
    begin
      Result := true;
      Exit;
    end;

    if not CTag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true) then Exit;
    STag := TElASN1ConstrainedTag(CTag.GetField(0));
    if STag.Count > 3 then Exit;

    Index := 0;
    TagNum := 0;

    while (Index < 4) and (TagNum < STag.Count) do
    begin
      ATag := TElASN1ConstrainedTag(STag.GetField(TagNum));
      Inc(TagNum);

      if (not ATag.IsConstrained) or (ATag.Count <> 1) then Exit;

      if (ATag.TagId = SB_ASN1_A0) then
      begin
        if Index > 0 then Exit;
        { hash algorithm }

        if not ATag.GetField(0).IsConstrained then Exit;
        ATag := TElASN1ConstrainedTag(ATag.GetField(0));
        if (ATag.Count <> 2) or
          (not ATag.GetField(0).CheckType(SB_ASN1_OBJECT, false)) or
          (not ATag.GetField(1).CheckType(SB_ASN1_NULL, false))
        then
          Exit;

        HashAlgorithm := GetHashAlgorithmByOID(TElASN1SimpleTag(ATag.GetField(0)).Content);

        Index := 1;
      end
      else if (ATag.TagId = SB_ASN1_A1) then
      begin
        if Index > 1 then Exit;
        { MGF }
        if not ATag.GetField(0).IsConstrained then Exit;
        ATag := TElASN1ConstrainedTag(ATag.GetField(0));
        if (ATag.Count <> 2) or
          (not ATag.GetField(0).CheckType(SB_ASN1_OBJECT, false)) or
          (not ATag.GetField(1).CheckType(SB_ASN1_SEQUENCE, true))
        then
          Exit;

        if not CompareContent(TElASN1SimpleTag(ATag.GetField(0)).Content, SB_OID_MGF1)
        then
          Exit;

        ATag := TElASN1ConstrainedTag(ATag.GetField(1));

        if (ATag.Count <> 2) or
          (not ATag.GetField(0).CheckType(SB_ASN1_OBJECT, false)) or
          (not ATag.GetField(1).CheckType(SB_ASN1_NULL, false))
        then
          Exit;

        MGFHashAlgorithm := GetHashAlgorithmByOID(TElASN1SimpleTag(ATag.GetField(0)).Content);

        Index := 2;
      end
      else if ATag.TagId = SB_ASN1_A2 then
      begin
        if Index > 2 then Exit;
        { Salt size }
        if not ATag.GetField(0).CheckType(SB_ASN1_INTEGER, false) then Exit;

        SaltSize := ASN1ReadInteger(TElASN1SimpleTag(ATag.GetField(0)));

        Index := 3;
      end
      else if ATag.TagId = SB_ASN1_A3 then
      begin
        if Index > 3 then Exit;
        { trailer }
        if not ATag.GetField(0).CheckType(SB_ASN1_INTEGER, false) then Exit;

        TrailerField := ASN1ReadInteger(TElASN1SimpleTag(ATag.GetField(0)));

        if TrailerField <> 1 then Exit; //only this type currently declared.

        Index := 4;
      end
      else
        Exit;
    end;

    Result := true;
  finally
    CTag.Free;
  end;
end;


function WriteOAEPParams(HashAlgorithm, MGFHashAlgorithm : integer;
  const StrLabel : string) : BufferType;
var
  Tag : TElASN1SimpleTag;
  cTag, aTag : TElASN1ConstrainedTag;
  Size : integer;
begin
  Result := EmptyBuffer;

  cTag := TElASN1ConstrainedTag.Create;
  cTag.TagId := SB_ASN1_SEQUENCE;

  { hash algorithm }

  if HashAlgorithm <> SB_ALGORITHM_DGST_SHA1 then
  begin
    aTag := TElASN1ConstrainedTag(cTag.GetField(cTag.AddField(True)));
    aTag.TagId := SB_ASN1_A0;
    aTag := TElASN1ConstrainedTag(aTag.GetField(aTag.AddField(true)));

    aTag.TagId := SB_ASN1_SEQUENCE;

    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(false)));
    Tag.TagId := SB_ASN1_OBJECT;
    Tag.Content := GetOIDByHashAlgorithm(HashAlgorithm);
    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(false)));
    Tag.TagId := SB_ASN1_NULL;
  end;

  { MGF }

  if (HashAlgorithm <> SB_ALGORITHM_DGST_SHA1) then
  begin
    aTag := TElASN1ConstrainedTag(cTag.GetField(cTag.AddField(True)));
    aTag.TagId := SB_ASN1_A1;
    aTag := TElASN1ConstrainedTag(aTag.GetField(aTag.AddField(true)));

    aTag.TagId := SB_ASN1_SEQUENCE;
    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(False)));
    Tag.TagId := SB_ASN1_OBJECT;
    Tag.Content := SB_OID_MGF1;
    aTag := TElASN1ConstrainedTag(aTag.GetField(aTag.AddField(True)));
    aTag.TagId := SB_ASN1_SEQUENCE;
    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(false)));
    Tag.TagId := SB_ASN1_OBJECT;
    Tag.Content := GetOIDByHashAlgorithm(HashAlgorithm);
    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(false)));
    Tag.TagId := SB_ASN1_NULL;
  end;

  { label source }

  if StrLabel <> '' then
  begin
    aTag := TElASN1ConstrainedTag(cTag.GetField(cTag.AddField(True)));
    aTag.TagId := SB_ASN1_A2;
    aTag := TElASN1ConstrainedTag(aTag.GetField(aTag.AddField(true)));

    aTag.TagId := SB_ASN1_SEQUENCE;
    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(False)));
    Tag.TagId := SB_ASN1_OBJECT;
    Tag.Content := SB_OID_OAEP_SRC_SPECIFIED;
    Tag := TElASN1SimpleTag(aTag.GetField(aTag.AddField(False)));
    Tag.TagId := SB_ASN1_OCTETSTRING;
    Tag.Content := StrLabel;
  end;

  Size := 0;

  if cTag.Count = 0 then
    Result := WriteNULL
  else
  begin
    cTag.SaveToBuffer(nil, Size);

    SetLength(Result, Size);
    if not cTag.SaveToBuffer(@Result[1], Size) then
      Result := EmptyBuffer;

    cTag.Free;
  end;  
end;


function ReadOAEPParams(InBuffer : pointer; InBufferSize : integer;
  var HashAlgorithm, MGFHashAlgorithm : integer; var StrLabel : string) : boolean;
var
  ATag, CTag, STag : TElASN1ConstrainedTag;
  //Tag : TElASN1SimpleTag;
  Index, TagNum : integer;
begin
  HashAlgorithm := SB_ALGORITHM_DGST_SHA1;
  MGFHashAlgorithm := SB_ALGORITHM_DGST_SHA1;
  StrLabel := '';
  Result := false;
  CTag := TElASN1ConstrainedTag.Create;

  try
    if not CTag.LoadFromBuffer(InBuffer, InBufferSize) then
      Exit;

    if CTag.Count <> 1 then Exit;

    if CTag.GetField(0).CheckType(SB_ASN1_NULL, false) then
    begin
      Result := true;
      Exit;
    end;

    if not CTag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true) then Exit;
    STag := TElASN1ConstrainedTag(CTag.GetField(0));
    if STag.Count > 3 then Exit;

    Index := 0;
    TagNum := 0;

    while (Index < 3) and (TagNum < STag.Count) do
    begin
      ATag := TElASN1ConstrainedTag(STag.GetField(TagNum));
      Inc(TagNum);

      if (not ATag.IsConstrained) or (ATag.Count <> 1) then Exit;

      if (ATag.TagId = SB_ASN1_A0) then
      begin
        if Index > 0 then Exit;
        { hash algorithm }

        if not ATag.GetField(0).IsConstrained then Exit;
        ATag := TElASN1ConstrainedTag(ATag.GetField(0));
        if (ATag.Count <> 2) or
          (not ATag.GetField(0).CheckType(SB_ASN1_OBJECT, false)) or
          (not ATag.GetField(1).CheckType(SB_ASN1_NULL, false))
        then
          Exit;

        HashAlgorithm := GetHashAlgorithmByOID(TElASN1SimpleTag(ATag.GetField(0)).Content);

        Index := 1;
      end
      else if (ATag.TagId = SB_ASN1_A1) then
      begin
        if Index > 1 then Exit;
        { MGF }
        if not ATag.GetField(0).IsConstrained then Exit;
        ATag := TElASN1ConstrainedTag(ATag.GetField(0));
        if (ATag.Count <> 2) or
          (not ATag.GetField(0).CheckType(SB_ASN1_OBJECT, false)) or
          (not ATag.GetField(1).CheckType(SB_ASN1_SEQUENCE, true))
        then
          Exit;

        if not CompareContent(TElASN1SimpleTag(ATag.GetField(0)).Content, SB_OID_MGF1)
        then
          Exit;

        ATag := TElASN1ConstrainedTag(ATag.GetField(1));

        if (ATag.Count <> 2) or
          (not ATag.GetField(0).CheckType(SB_ASN1_OBJECT, false)) or
          (not ATag.GetField(1).CheckType(SB_ASN1_NULL, false))
        then
          Exit;

        MGFHashAlgorithm := GetHashAlgorithmByOID(TElASN1SimpleTag(ATag.GetField(0)).Content);

        Index := 2;
      end
      else if ATag.TagId = SB_ASN1_A2 then
      begin
        if Index > 2 then Exit;
        { label source algorithm }

        if not ATag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true) then Exit;
        ATag := TElASN1ConstrainedTag(ATag.GetField(0));
        if (ATag.Count <> 2) or
          (not ATag.GetField(0).CheckType(SB_ASN1_OBJECT, false))
        then
          Exit;

        if not CompareContent(TElASN1SimpleTag(ATag.GetField(0)).Content, SB_OID_OAEP_SRC_SPECIFIED)
        then
          Exit;

        if not ATag.GetField(1).CheckType(SB_ASN1_OCTETSTRING, false) then Exit;
        StrLabel := (TElASN1SimpleTag(ATag.GetField(1)).Content);

        Index := 3;
      end
      else
        Exit;
    end;

    Result := true;
  finally
    CTag.Free;
  end;
end;

function DecodePublicKey(Buffer : pointer; Size : integer; PublicModulus : pointer;
  var PublicModulusSize: integer; PublicExponent : pointer; var PublicExponentSize:
  integer; var AlgID : BufferType): boolean;
var
  CTag, Tag : TElASN1ConstrainedTag;
  InnerTag : TElASN1ConstrainedTag;
  Buf : BufferType;
  RealMSize, RealESize: integer;
begin
  Result := false;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if (Tag.LoadFromBuffer(Buffer,Size)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        CTag := TElASN1ConstrainedTag(Tag.GetField(0));
        if (CTag.Count = 2) and (CTag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) and
          (CTag.GetField(1).CheckType(SB_ASN1_BITSTRING, false)) then
        begin
          if TElASN1ConstrainedTag(CTag.GetField(0)).GetField(0).CheckType(SB_ASN1_OBJECT, false) and
            CompareContent(TElASN1SimpleTag(TElASN1ConstrainedTag(CTag.GetField(0)).GetField(0)).Content,
              SB_OID_RSAENCRYPTION) then
          begin
            AlgID := TElASN1SimpleTag(TElASN1ConstrainedTag(CTag.GetField(0)).GetField(0)).Content;
            Buf := TElASN1SimpleTag(CTag.GetField(1)).Content;
            Buf := Copy(Buf, 2, Length(Buf) - 1);
            InnerTag := TElASN1ConstrainedTag.Create;
            try
              if InnerTag.LoadFromBuffer(@Buf[1], Length(Buf)) then
              begin
                if (InnerTag.Count = 1) and (InnerTag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
                begin
                  CTag := TElASN1ConstrainedTag(InnerTag.GetField(0));
                  if (CTag.Count = 2) and (CTag.GetField(0).CheckType(SB_ASN1_INTEGER, false)) and
                    (CTag.GetField(0).CheckType(SB_ASN1_INTEGER, false)) then
                  begin
                    RealMSize := Length(TElASN1SimpleTag(CTag.GetField(0)).Content);
                    RealESize := Length(TElASN1SimpleTag(CTag.GetField(1)).Content);
                    if (PublicModulusSize < RealMSize) or (PublicExponentSize < RealESize) then
                    begin
                      PublicModulusSize := RealMSize;
                      PublicExponentSize := RealESize;
                      Exit;
                    end;
                    Buf := TElASN1SimpleTag(CTag.GetField(0)).Content;
                    PublicModulusSize := Length(Buf);
                    Move(Buf[1], PublicModulus^, PublicModulusSize);
                    Buf := TElASN1SimpleTag(CTag.GetField(1)).Content;
                    PublicExponentSize := Length(Buf);
                    Move(Buf[1], PublicExponent^, PublicExponentSize);
                    Result := true;
                  end;
                end;
              end;
            finally
              FreeAndNil(InnerTag);
            end;
          end;
        end;
      end;
    end
  finally
    FreeAndNil(Tag);
  end;
end;

function FormatIntegerValue(Buffer: pointer; Size: integer) : BufferType;
begin
  if Size = 0 then
    Result := #0
  else if PByte(Buffer)^ >= $80 then
  begin
    SetLength(Result, Size + 1);
    Move(Buffer^, Result[2], Size);
    Result[1] := #0;
  end
  else
  begin
    SetLength(Result, Size);
    Move(Buffer^, Result[1], Size);
  end;
end;

function EncodePublicKey(PublicModulus : pointer; PublicModulusSize : integer;
  PublicExponent : pointer; PublicExponentSize : integer; const AlgID : BufferType;
  OutBuffer : pointer; var OutSize : integer) : boolean;
var
  Tag : TElASN1ConstrainedTag;
  InnerTag : TElASN1ConstrainedTag;
  SimpleTag : TElASN1SimpleTag;
  Buf : BufferType;
  BufSize: integer;
begin
  Result := false;
  Tag := TElASN1ConstrainedTag.Create;
  try
    Tag.TagID := SB_ASN1_SEQUENCE;
    // forming alg-id sequence
    InnerTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    InnerTag.TagID := SB_ASN1_SEQUENCE;
    SimpleTag := TElASN1SimpleTag(InnerTag.GetField(InnerTag.AddField(false)));
    SimpleTag.TagID := SB_ASN1_OBJECT;
    SimpleTag.Content := AlgID;
    SimpleTag := TElASN1SimpleTag(InnerTag.GetField(InnerTag.AddField(false)));
    SimpleTag.TagID := SB_ASN1_NULL;
    SimpleTag.Content := EmptyBuffer;
    // forming key material sequence
    InnerTag := TElASN1ConstrainedTag.Create;
    InnerTag.TagID := SB_ASN1_SEQUENCE;
    try
      SimpleTag := TElASN1SimpleTag(InnerTag.GetField(InnerTag.AddField(false)));
      SimpleTag.TagID := SB_ASN1_INTEGER;
      SimpleTag.Content := FormatIntegerValue(PublicModulus, PublicModulusSize);
      SimpleTag := TElASN1SimpleTag(InnerTag.GetField(InnerTag.AddField(false)));
      SimpleTag.TagID := SB_ASN1_INTEGER;
      SimpleTag.Content := FormatIntegerValue(PublicExponent, PublicExponentSize);
      BufSize := 0;
      InnerTag.SaveToBuffer(nil, BufSize);
      SetLength(Buf, BufSize);
      if not InnerTag.SaveToBuffer(@Buf[1], BufSize) then
        Exit;
      SetLength(Buf, BufSize);
      Buf := #0 + Buf;
    finally
      FreeAndNil(InnerTag);
    end;
    SimpleTag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    SimpleTag.TagID := SB_ASN1_BITSTRING;
    SimpleTag.Content := Buf;
    Result := Tag.SaveToBuffer(OutBuffer, OutSize);
  finally
    FreeAndNil(Tag);
  end;
end;

end.
