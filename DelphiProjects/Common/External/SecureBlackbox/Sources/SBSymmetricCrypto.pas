(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$i SecBbox.inc}

unit SBSymmetricCrypto;

interface

uses
  SBCustomCrypto,
  Classes,
  SysUtils,
  SBUtils,
  SBAES,
  SBBlowfish,
  SBTwofish,
  {$ifndef SBB_NO_IDEA}SBIDEA,{$endif}
  SBCAST128,
  {$ifndef SBB_NO_RC2}SBRC2,{$endif}
  {$ifndef SBB_NO_RC4}SBRC4,{$endif}
  {$ifndef SBB_NO_DES}SBDES,
  SB3DES,{$endif}
  {$ifndef SBB_NO_CAMELLIA}SBCamellia,{$endif}
  SBSerpent,
  SBSHA2,
  SBConstants;

type

  TElSymmetricKeyMaterial = class(TElKeyMaterial)
  private
    FKey : ByteArray;
    FIV : ByteArray;
    FAlgorithm : integer;
    procedure SetKey(const Value : ByteArray);
    procedure SetIV(const Value : ByteArray);
  public
    constructor Create; virtual;
    procedure Generate(Bits : integer); override;
    procedure GenerateIV(Bits : integer); virtual;
    procedure Load(Buffer : pointer; var Size : integer); reintroduce; overload; virtual;
    procedure Save(Buffer : pointer; var Size : integer); reintroduce; overload; virtual;
    procedure Load(Stream : TStream; Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}); overload; override;
    procedure Save(Stream : TStream); overload; override;

    property Key : ByteArray read FKey write SetKey;
    property IV : ByteArray read FIV write SetIV;
    property Algorithm : integer read FAlgorithm write FAlgorithm;
  end;

  TSBSymmetricCryptoMode = 
    (cmDefault, cmECB, cmCBC, cmCTR, cmCFB8);

  TSBSymmetricCipherPadding =  (cpNone, cpPKCS5);

  TElSymmetricCrypto = class(TElCustomCrypto)
  private
    FKeyMaterial : TElSymmetricKeyMaterial;
    FMode : TSBSymmetricCryptoMode;
    FKeySize : integer;
    FBlockSize : integer;
    FPadding : TSBSymmetricCipherPadding;
    FVector : ByteArray;
    FTail : ByteArray;
    FPadBytes : ByteArray;    
    FOID : BufferType;
  protected
    procedure SetKeyMaterial(Material : TElSymmetricKeyMaterial); virtual;
    function AddPadding(Block : pointer; Size : integer) : ByteArray;
    function EstimatedOutputSize(InputSize : integer; Encrypt : boolean) : integer;
    procedure InternalEncrypt(Buffer, OutBuffer : pointer; Size : integer); virtual;
    procedure EncryptBlock(Buffer, OutBuffer : pointer); overload; virtual;
    procedure EncryptBlock(Buffer, OutBuffer : pointer; Size : integer); overload; virtual;
    procedure InternalDecrypt(Buffer, OutBuffer : pointer; Size : integer); virtual;
    procedure DecryptBlock(Buffer, OutBuffer: pointer); overload; virtual;
    procedure DecryptBlock(Buffer, OutBuffer: pointer; Size : integer); overload; virtual;
    class function IsAlgorithmSupported(AlgID : integer) : boolean; overload; virtual;
    class function IsAlgorithmSupported(const AlgOID : BufferType) : boolean; overload; virtual;
    class function GetIsStreamCipher : boolean; virtual;
    function GetNetIsStreamCipher : boolean; virtual;
  public
    constructor Create(AlgID : integer;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; virtual;
    constructor Create(const AlgOID : BufferType;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; virtual;
    constructor Create(Mode : TSBSymmetricCryptoMode = cmDefault); overload; virtual;

    procedure InitializeEncryption; virtual;
    procedure InitializeDecryption; virtual;
    procedure Encrypt(InBuffer: pointer; InSize : integer; OutBuffer: pointer;
      var OutSize : integer); overload;
    procedure Encrypt(InStream, OutStream: TStream); overload;
    procedure EncryptUpdate(InBuffer: pointer; InSize : integer; OutBuffer: pointer;
      var OutSize : integer);
    procedure FinalizeEncryption(OutBuffer : pointer; var OutSize : integer); virtual;
    procedure Decrypt(InBuffer: pointer; InSize : integer; OutBuffer: pointer;
      var OutSize : integer); overload;
    procedure Decrypt(InStream, OutStream: TStream); overload;
    procedure DecryptUpdate(InBuffer: pointer; InSize : integer; OutBuffer: pointer;
      var OutSize : integer); overload;
    procedure FinalizeDecryption(OutBuffer : pointer; var OutSize : integer); virtual;

    property KeyMaterial : TElSymmetricKeyMaterial read FKeyMaterial write SetKeyMaterial;
    property Mode : TSBSymmetricCryptoMode read FMode;
    property BlockSize : integer read FBlockSize;
    property KeySize : integer read FKeySize;
    property Padding : TSBSymmetricCipherPadding read FPadding write FPadding;
    property IsStreamCipher : boolean read GetNetIsStreamCipher;
  end;

  TElSymmetricCryptoClass =  class of TElSymmetricCrypto;

  TElSymmetricCryptoFactory = class
  private
    FRegisteredClasses: TList;
    procedure RegisterDefaultClasses;
    function GetRegisteredClass(Index: integer) : TElSymmetricCryptoClass;
    function GetRegisteredClassCount: integer;
  public
    constructor Create;
    destructor Destroy; override;

    procedure RegisterClass(Cls : TElSymmetricCryptoClass);
    function CreateInstance(const OID : BufferType;
      Mode : TSBSymmetricCryptoMode = cmDefault):
      TElSymmetricCrypto; overload;
    function CreateInstance(Alg : integer;
      Mode : TSBSymmetricCryptoMode = cmDefault):
      TElSymmetricCrypto; overload;
    function IsAlgorithmSupported(const OID : BufferType): boolean; overload;
    function IsAlgorithmSupported(Alg : integer): boolean; overload;
    property RegisteredClasses[Index: integer] : TElSymmetricCryptoClass
      read GetRegisteredClass;
    property RegisteredClassCount : integer read GetRegisteredClassCount;
  end;

  TElAESSymmetricCrypto = class(TElSymmetricCrypto)
  protected
    FKey128 : TAESExpandedKey128;
    FKey192 : TAESExpandedKey192;
    FKey256 : TAESExpandedKey256;

    procedure SetKeyMaterial(Material : TElSymmetricKeyMaterial); override;
    procedure EncryptBlock(Buffer, OutBuffer : pointer); override;
    procedure DecryptBlock(Buffer, OutBuffer: pointer); override;
    class function IsAlgorithmSupported(AlgID : integer) : boolean; overload; override;
    class function IsAlgorithmSupported(const AlgOID : BufferType) : boolean; overload; override;
  public
    constructor Create(AlgID : integer;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
    constructor Create(const AlgOID : BufferType;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;

    procedure InitializeEncryption; override;
    procedure InitializeDecryption; override;
  end;

  TElBlowfishSymmetricCrypto = class(TElSymmetricCrypto)
  protected
    FContext : TSBBlowfishContext;

    procedure SetKeyMaterial(Material : TElSymmetricKeyMaterial); override;
    procedure EncryptBlock(Buffer, OutBuffer : pointer); override;
    procedure DecryptBlock(Buffer, OutBuffer: pointer); override;
    class function IsAlgorithmSupported(AlgID : integer) : boolean; overload; override;
    class function IsAlgorithmSupported(const AlgOID : BufferType) : boolean; overload; override;
  public
    constructor Create(AlgID : integer;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
    constructor Create(const AlgOID : BufferType;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
            
    procedure InitializeEncryption; override;
    procedure InitializeDecryption; override;
  end;

  TElTwofishSymmetricCrypto = class(TElSymmetricCrypto)
  protected
    FKey : TTwofishExpandedKey;

    procedure SetKeyMaterial(Material : TElSymmetricKeyMaterial); override;
    procedure EncryptBlock(Buffer, OutBuffer : pointer); override;
    procedure DecryptBlock(Buffer, OutBuffer: pointer); override;
    class function IsAlgorithmSupported(AlgID : integer) : boolean; overload; override;
    class function IsAlgorithmSupported(const AlgOID : BufferType) : boolean; overload; override;
  public
    constructor Create(AlgID : integer;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
    constructor Create(const AlgOID : BufferType;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
        
    procedure InitializeEncryption; override;
    procedure InitializeDecryption; override;
  end;

  {$ifndef SBB_NO_IDEA}
  TElIDEASymmetricCrypto = class(TElSymmetricCrypto)
  protected
    FKey : TIDEAExpandedKey;
    procedure SetKeyMaterial(Material : TElSymmetricKeyMaterial); override;
    procedure EncryptBlock(Buffer, OutBuffer : pointer); override;
    procedure DecryptBlock(Buffer, OutBuffer: pointer); override;
    class function IsAlgorithmSupported(AlgID : integer) : boolean; overload; override;
    class function IsAlgorithmSupported(const AlgOID : BufferType) : boolean; overload; override;
  public
    constructor Create(AlgID : integer;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
    constructor Create(const AlgOID : BufferType;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
        
    procedure InitializeEncryption; override;
    procedure InitializeDecryption; override;
  end;
  {$endif}

  TElCAST128SymmetricCrypto = class(TElSymmetricCrypto)
  protected
    FKey : TCAST128ExpandedKey;
    procedure SetKeyMaterial(Material : TElSymmetricKeyMaterial); override;
    procedure EncryptBlock(Buffer, OutBuffer : pointer); override;
    procedure DecryptBlock(Buffer, OutBuffer: pointer); override;
    class function IsAlgorithmSupported(AlgID : integer) : boolean; overload; override;
    class function IsAlgorithmSupported(const AlgOID : BufferType) : boolean; overload; override;
  public
    constructor Create(AlgID : integer;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
    constructor Create(const AlgOID : BufferType;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
        
    procedure InitializeEncryption; override;
    procedure InitializeDecryption; override;
  end;

  {$ifndef SBB_NO_RC2}
  TElRC2SymmetricCrypto = class(TElSymmetricCrypto)
  protected
    FKey : TRC2ExpandedKey;
    procedure SetKeyMaterial(Material : TElSymmetricKeyMaterial); override;
    procedure EncryptBlock(Buffer, OutBuffer : pointer); override;
    procedure DecryptBlock(Buffer, OutBuffer: pointer); override;
    class function IsAlgorithmSupported(AlgID : integer) : boolean; overload; override;
    class function IsAlgorithmSupported(const AlgOID : BufferType) : boolean; overload; override;
  public
    constructor Create(AlgID : integer;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
    constructor Create(const AlgOID : BufferType;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
        
    procedure InitializeEncryption; override;
    procedure InitializeDecryption; override;
  end;
  {$endif}

  {$ifndef SBB_NO_RC4}
  TElRC4SymmetricCrypto = class(TElSymmetricCrypto)
  protected
    FSkipKeyStreamBytes : integer;
    FContext : TRC4Context;
    procedure SetKeyMaterial(Material : TElSymmetricKeyMaterial); override;
    procedure EncryptBlock(Buffer, OutBuffer : pointer; Size : integer); override;
    procedure DecryptBlock(Buffer, OutBuffer: pointer; Size : integer); override;
    class function GetIsStreamCipher : boolean; override;
    class function IsAlgorithmSupported(AlgID : integer) : boolean; overload; override;
    class function IsAlgorithmSupported(const AlgOID : BufferType) : boolean; overload; override;
  public
    constructor Create(AlgID : integer;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
    constructor Create(const AlgOID : BufferType;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
        
    procedure InitializeEncryption; override;
    procedure InitializeDecryption; override;

    property SkipKeystreamBytes : integer read FSkipKeystreamBytes
      write FSkipKeystreamBytes;
  end;
  {$endif}

  {$ifndef SBB_NO_DES}
  TElDESSymmetricCrypto = class(TElSymmetricCrypto)
  protected
    FKey : TDESExpandedKey;
    procedure SetKeyMaterial(Material : TElSymmetricKeyMaterial); override;
    procedure EncryptBlock(Buffer, OutBuffer : pointer); override;
    procedure DecryptBlock(Buffer, OutBuffer: pointer); override;
    class function IsAlgorithmSupported(AlgID : integer) : boolean; overload; override;
    class function IsAlgorithmSupported(const AlgOID : BufferType) : boolean; overload; override;
  public
    constructor Create(AlgID : integer;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
    constructor Create(const AlgOID : BufferType;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
        
    procedure InitializeEncryption; override;
    procedure InitializeDecryption; override;
  end;

  TEl3DESSymmetricCrypto = class(TElSymmetricCrypto)
  protected
    FKey : T3DESExpandedKey;
    procedure SetKeyMaterial(Material : TElSymmetricKeyMaterial); override;
    procedure EncryptBlock(Buffer, OutBuffer : pointer); override;
    procedure DecryptBlock(Buffer, OutBuffer: pointer); override;
    class function IsAlgorithmSupported(AlgID : integer) : boolean; overload; override;
    class function IsAlgorithmSupported(const AlgOID : BufferType) : boolean; overload; override;
  public
    constructor Create(AlgID : integer;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
    constructor Create(const AlgOID : BufferType;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
        
    procedure InitializeEncryption; override;
    procedure InitializeDecryption; override;
  end;
  {$endif}

  {$ifndef SBB_NO_CAMELLIA}
  TElCamelliaSymmetricCrypto = class(TElSymmetricCrypto)
  protected
    FKey : TSBCamelliaExpandedKey;

    procedure SetKeyMaterial(Material : TElSymmetricKeyMaterial); override;
    procedure EncryptBlock(Buffer, OutBuffer : pointer); override;
    procedure DecryptBlock(Buffer, OutBuffer: pointer); override;
    class function IsAlgorithmSupported(AlgID : integer) : boolean; overload; override;
    class function IsAlgorithmSupported(const AlgOID : BufferType) : boolean; overload; override;
  public
    constructor Create(AlgID : integer;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
    constructor Create(const AlgOID : BufferType;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
        
    procedure InitializeEncryption; override;
    procedure InitializeDecryption; override;
  end;
  {$endif}

  TElSerpentSymmetricCrypto = class(TElSymmetricCrypto)
  protected
    FKey : TSerpentExpandedKey;

    procedure SetKeyMaterial(Material : TElSymmetricKeyMaterial); override;
    procedure EncryptBlock(Buffer, OutBuffer : pointer); override;
    procedure DecryptBlock(Buffer, OutBuffer: pointer); override;
    class function IsAlgorithmSupported(AlgID : integer) : boolean; overload; override;
    class function IsAlgorithmSupported(const AlgOID : BufferType) : boolean; overload; override;
  public
    constructor Create(AlgID : integer;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;
    constructor Create(const AlgOID : BufferType;
      Mode: TSBSymmetricCryptoMode = cmDefault); overload; override;

    procedure InitializeEncryption; override;
    procedure InitializeDecryption; override;
  end;


  EElSymmetricCryptoError =  class(ESecureBlackboxError);

const
  SYMMETRIC_BLOCK_SIZE = 16384;
  SYMMETRIC_DEFAULT_MODE = cmCBC;

implementation

resourcestring
  SOutputBufferTooSmall = 'Output buffer too small';
  SInternalError = 'Internal error';
  SUnsupportedAlgorithmInt = 'Unsupported algorithm %d';
  SUnsupportedAlgorithmOID = 'Unsupported algorithm %s';
  SUnsupportedOperation = 'Unsupported operation';
  SInternalException = 'Internal exception';
  SInvalidKeyMaterial = 'Invalid key material';
  SInvalidKeyFormat = 'Invalid key format';  
  SInvalidPadding = 'Invalid padding';
  SInvalidCipherMode = 'Invalid cipher mode of operation';
  SCannotCompleteOperation = 'Cannot complete operation';

////////////////////////////////////////////////////////////////////////////////
// TElSymmetricKeyMaterial class

constructor TElSymmetricKeyMaterial.Create;
begin
  inherited;

  SetLength(FKey, 0);
  SetLength(FIV, 0);
  FAlgorithm := 0;
end;

procedure TElSymmetricKeyMaterial.SetKey(const Value : ByteArray);
begin
  SetLength(FKey, Length(Value));
  if Length(FKey) > 0 then
    Move(Value[0], FKey[0], Length(FKey));
end;

procedure TElSymmetricKeyMaterial.SetIV(const Value : ByteArray);
begin
  SetLength(FIV, Length(Value));
  if Length(FIV) > 0 then
    Move(Value[0], FIV[0], Length(FIV));
end;

procedure TElSymmetricKeyMaterial.Generate(Bits : integer);
begin
  if Bits mod 8 <> 0 then
    raise EElSymmetricCryptoError.Create(SInvalidInputSize);

  SetLength(FKey, Bits shr 3);
  SBUtils.SBRndGenerate(@FKey[0], Bits shr 3);
end;

procedure TElSymmetricKeyMaterial.GenerateIV(Bits : integer);
begin
  if Bits mod 8 <> 0 then
    raise EElSymmetricCryptoError.Create(SInvalidInputSize);

  SetLength(FIV, Bits shr 3);
  SBUtils.SBRndGenerate(@FIV[0], Bits shr 3);
end;

{ Save/Load routines }
{ format : <word algorithm><word KeySize><key><word IVsize><IV><SHA-256 hash of all previous data>
  all numbers are big-endian }

procedure TElSymmetricKeyMaterial.Load(Buffer : pointer; var Size : integer);
var
  MinSize, MaxSize, NeedIVSize : integer;
  Alg : word;
  KeySize, IVSize : integer;
  Hash, OrigHash : TMessageDigest256;
begin
  if Size < 38 then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  Alg := PByteArray(Buffer)^[0] shl 8 + PByteArray(Buffer)^[1];
  KeySize := PByteArray(Buffer)^[2] shl 8 + PByteArray(Buffer)^[3];

  if (Size < 38 + KeySize) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  MinSize := 1;
  MaxSize := 56;
  NeedIVSize := 0;

  case Alg of
    SB_ALGORITHM_CNT_RC4 :
      begin
        MinSize := 1;
        MaxSize := 32;
        NeedIVSize := 0;
      end;
    SB_ALGORITHM_CNT_DES :
      begin
        MinSize := 8;
        MaxSize := 8;
        NeedIVSize := 8;
      end;
    SB_ALGORITHM_CNT_3DES :
      begin
        MinSize := 24;
        MaxSize := 24;
        NeedIVSize := 8;
      end;
    SB_ALGORITHM_CNT_RC2 :
      begin
        MinSize := 1;
        MaxSize := 16;
        NeedIVSize := 8;
      end;
    SB_ALGORITHM_CNT_AES128 :
      begin
        MinSize := 16;
        MaxSize := 16;
        NeedIVSize := 16;        
      end;
    SB_ALGORITHM_CNT_AES192 :
      begin
        MinSize := 24;
        MaxSize := 24;
        NeedIVSize := 16;
      end;
    SB_ALGORITHM_CNT_AES256 :
      begin
        MinSize := 32;
        MaxSize := 32;
        NeedIVSize := 16;        
      end;
    SB_ALGORITHM_CNT_BLOWFISH :
      begin
        MinSize := 4;
        MaxSize := 56;
        NeedIVSize := 8;        
      end;
    SB_ALGORITHM_CNT_TWOFISH :
      begin
        MinSize := 16;
        MaxSize := 32;
        NeedIVSize := 16;
      end;
    SB_ALGORITHM_CNT_CAMELLIA :
      begin
        MinSize := 16;
        MaxSize := 32;
        NeedIVSize := 16;
      end;
    SB_ALGORITHM_CNT_CAST128 :
      begin
        MinSize := 16;
        MaxSize := 16;
        NeedIVSize := 8;
      end;
    SB_ALGORITHM_CNT_IDEA :
      begin
        MinSize := 16;
        MaxSize := 16;
        NeedIVSize := 8;
      end;
    SB_ALGORITHM_CNT_SERPENT :
      begin
        MinSize := 16;
        MaxSize := 32;
        NeedIVSize := 16;
      end;    
   end;

   if (KeySize < MinSize) or (KeySize > MaxSize) then
     raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

   IVSize := PByteArray(Buffer)^[4 + KeySize] shl 8 + PByteArray(Buffer)^[5 + KeySize];

   if ((IVSize <> NeedIVSize) and (IVSize <> 0)) or (Size < 38 + KeySize + IVSize) then
     raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

   Move(PByteArray(Buffer)^[6 + KeySize + IVSize], Hash, 32);
   OrigHash := SBSHA2.HashSHA256(Buffer, 6 + KeySize + IVSize);

   if not CompareMem(@Hash, @OrigHash, 32) then
     raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

   FAlgorithm := Alg;

   SetLength(FKey, KeySize);
   SetLength(FIV, IVSize);

   Move(PByteArray(Buffer)^[4], FKey[0], KeySize);
   Move(PByteArray(Buffer)^[6 + KeySize], FIV[0], IVSize);

   Size := KeySize + IVSize + 38;
end;

procedure TElSymmetricKeyMaterial.Save(Buffer : pointer; var Size : integer);
var
  KeySize, IVSize : integer;
  Hash : TMessageDigest256;
begin
  if Size <  38 + Length(FKey) + Length(FIV) then
  begin
    if Size = 0 then
    begin
      Size := 38 + Length(FKey) + Length(FIV);
      Exit
    end
    else
      raise EElSymmetricCryptoError.Create(SOutputBufferTooSmall);
  end
  else
    Size := 38 + Length(FKey) + Length(FIV);

  KeySize := Length(FKey);
  IVSize := Length(FIV);

  PByteArray(Buffer)^[0] := (FAlgorithm shr 8) and $ff;
  PByteArray(Buffer)^[1] := FAlgorithm and $ff;
  PByteArray(Buffer)^[2] := (KeySize shr 8) and $ff;
  PByteArray(Buffer)^[3] := KeySize and $ff;

  Move(FKey[0], PByteArray(Buffer)^[4], KeySize);
  PByteArray(Buffer)^[4 + KeySize] := (IVSize shr 8) and $ff;
  PByteArray(Buffer)^[5 + KeySize] := IVSize and $ff;
  Move(FIV[0], PByteArray(Buffer)^[6 + KeySize], IVSize);

  Hash := SBSHA2.HashSHA256(Buffer, Size - 32);
  Move(Hash, PByteArray(Buffer)^[6 + KeySize + IVSize], 32);
end;


procedure TElSymmetricKeyMaterial.Load(Stream : TStream; Count : integer {$ifdef HAS_DEF_PARAMS}= 0{$endif});
var
  Size, KeySize, IVSize : integer;
  InSize : Int64;
  Buf : ByteArray;
begin
  if Count = 0 then 
    InSize := Stream.Size - Stream.Position
  else
    InSize := Count;

  if InSize < 38 then
    raise EElSymmetricCryptoError.Create(SInvalidKeyFormat);

  SetLength(Buf, 4);
  Stream.Read(Buf[0], 4);
  KeySize := Buf[2] shl 8 + Buf[3];

  if InSize < 38 + KeySize then
    raise EElSymmetricCryptoError.Create(SInvalidKeyFormat);

  SetLength(Buf, 6 + KeySize);
  Stream.Read(Buf[4], KeySize + 2);
  IVSize := Buf[4 + KeySize] shl 8 + Buf[5 + KeySize];

  if InSize < 38 + KeySize + IVSize then
    raise EElSymmetricCryptoError.Create(SInvalidKeyFormat);

  SetLength(Buf, 38 + KeySize + IVSize);
  Stream.Read(Buf[6 + KeySize], IVSize + 32);

  Size := 38 + KeySize + IVSize;
  Load(@Buf[0], Size);
end;

procedure TElSymmetricKeyMaterial.Save(Stream : TStream);
var
  Buf : ByteArray;
  Size : integer;
begin
  Size := 0;

  Save(nil, Size);
  SetLength(Buf, Size);
  Save(@Buf[0], Size);

  Stream.Write(Buf[0], Size);
end;


////////////////////////////////////////////////////////////////////////////////
// TElSymmetricCrypto class

constructor TElSymmetricCrypto.Create(AlgID : integer;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  inherited Create; //.NET cannot compile without this line
  raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmInt, [AlgID]));
end;

constructor TElSymmetricCrypto.Create(const AlgOID : BufferType;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  inherited Create;
  raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmOID, [AlgOID]));
end;

constructor TElSymmetricCrypto.Create(Mode : TSBSymmetricCryptoMode = cmDefault);
begin
  inherited Create;

  if IsStreamCipher then
  begin
    FMode := cmDefault;
    FBlockSize := 1;
  end
  else
  begin
    if Mode = cmDefault then
      Mode := SYMMETRIC_DEFAULT_MODE;
            
    FMode := Mode;
    FBlockSize := 0;
  end;

  FKeyMaterial := nil;
  FOID := EmptyBuffer;
  FKeySize := 0;
end;

procedure TElSymmetricCrypto.SetKeyMaterial(Material : TElSymmetricKeyMaterial);
begin
  if FKeySize > 0 then
    if Length(Material.Key) <> FKeySize then
      raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);


  if not IsStreamCipher then
  begin
    if FBlockSize > 0 then
      if not ((Length(Material.IV) = FBlockSize) or (Length(Material.IV) = 0)) then
        raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);
    if (Length(Material.IV) = 0) and 
       not (FMode in [cmDefault, cmECB]) then
      raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);
  end;

  FKeyMaterial := Material;

  if (not IsStreamCipher) and (Length(Material.IV) > 0) then
    FBlockSize := Length(Material.IV);
  if Length(Material.Key) > 0 then
    FKeySize := Length(Material.Key);
    
  // put all key- and IV- checks, including inherited call, in descendant classes.
end;

class function TElSymmetricCrypto.IsAlgorithmSupported(AlgID : integer) : boolean;
begin
  Result := false;
end;

class function TElSymmetricCrypto.IsAlgorithmSupported(const AlgOID : BufferType) : boolean;
begin
  Result := false;
end;

class function TElSymmetricCrypto.GetIsStreamCipher : boolean;
begin
  Result := false;
  { override for stream ciphers }
end;

function TElSymmetricCrypto.GetNetIsStreamCipher : boolean;
begin
  Result := GetIsStreamCipher;
end;

function TElSymmetricCrypto.EstimatedOutputSize(InputSize : integer; Encrypt : boolean) : integer;
begin
  if IsStreamCipher then
  begin
    Result := InputSize;
    Exit;
  end;

  if Encrypt then
  begin
    if FPadding = cpPKCS5 then
      Result := InputSize + FBlockSize - InputSize mod FBlockSize
    else if FPadding = cpNone then
      Result := InputSize
    else
      raise EElSymmetricCryptoError.Create(SInternalException);
  end
  else
  begin
    if FPadding = cpPKCS5 then
      Result := InputSize
    else if FPadding = cpNone then
      Result := InputSize
    else
      raise EElSymmetricCryptoError.Create(SInternalException);
  end;
end;

procedure TElSymmetricCrypto.InitializeEncryption;
begin
  SetLength(FVector, Length(FKeyMaterial.FIV));
  if Length(FVector) > 0 then
    Move(FKeyMaterial.FIV[0], FVector[0], Length(FVector));
  SetLength(FTail, 0);
  { must be called as inherited in descendant classes }
end;

procedure TElSymmetricCrypto.InternalEncrypt(Buffer, OutBuffer : pointer; Size : integer);
var
  Block : ByteArray;
  Index, Count, XIndex : Integer;
  BufferPtr, OutPtr : pointer;
begin
  { is called only for Block ciphers }

  OutPtr := OutBuffer;
  BufferPtr := Buffer;

  Count := Size div FBlockSize;

  { ECB mode }
  if FMode = cmECB then
  begin
    for Index := 0 to Count - 1 do
    begin
      EncryptBlock(BufferPtr, OutPtr);
      BufferPtr := Pointer(PtrUInt(BufferPtr) + Cardinal(FBlockSize));
      OutPtr := Pointer(PtrUInt(OutPtr) + Cardinal(FBlockSize));
    end;
  end
  { CBC mode }
  else if FMode = cmCBC then
  begin
    if Length(FVector) <> FBlockSize then
      raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

    for Index := 0 to Count - 1 do
    begin
      { currently supported ciphers have only 8 and 16 bytes block size }
      if FBlockSize = 8 then
      begin
        PLongWord(@FVector[0])^ := PLongWord(@FVector[0])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        PLongWord(@FVector[4])^ := PLongWord(@FVector[4])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
      end
      else if FBlockSize = 16 then
      begin
        PLongWord(@FVector[0])^ := PLongWord(@FVector[0])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        PLongWord(@FVector[4])^ := PLongWord(@FVector[4])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        PLongWord(@FVector[8])^ := PLongWord(@FVector[8])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        PLongWord(@FVector[12])^ := PLongWord(@FVector[12])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
      end
      else
        raise EElSymmetricCryptoError.Create(SInternalException);

      EncryptBlock(@FVector[0], OutPtr);
      Move(OutPtr^, FVector[0], FBlockSize);

      OutPtr := Pointer(PtrUInt(OutPtr) + Cardinal(FBlockSize));
    end;
  end
  { CTR - mode }
  else if FMode = cmCTR then
  begin
    if Length(FVector) <> FBlockSize then
      raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

    SetLength(Block, FBlockSize);

    for Index := 0 to Count - 1 do
    begin
      EncryptBlock(@FVector[0], @Block[0]);

      if FBlockSize = 8 then
      begin
        PLongWord(OutPtr)^ := PLongWord(@Block[0])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[4])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
      end
      else if FBlockSize = 16 then
      begin
        PLongWord(OutPtr)^ := PLongWord(@Block[0])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[4])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[8])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[12])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
      end
      else
        raise EElSymmetricCryptoError.Create(SInternalException);

      { incrementing counter }
      for XIndex := FBlockSize - 1 downto 0 do
        if FVector[XIndex] = $ff then
          FVector[XIndex] := 0
        else
        begin
          Inc(FVector[XIndex]);
          Break;
        end;
    end;
  end
  { CFB 8-bit mode }
  else if FMode = cmCFB8 then
  begin
    if Length(FVector) <> FBlockSize then
      raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

    Count := Size div FBlockSize;
    SetLength(Block, FBlockSize);

    for Index := 0 to Count - 1 do
    begin
      EncryptBlock(@FVector[0], @Block[0]);
      if FBlockSize = 8 then
      begin
        PLongWord(OutPtr)^ := PLongWord(@Block[0])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[4])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
      end
      else if FBlockSize = 16 then
      begin
        PLongWord(OutPtr)^ := PLongWord(@Block[0])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[4])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[8])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[12])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
      end
      else
        raise EElSymmetricCryptoError.Create(SInternalException);

      Move(Pointer(PtrUInt(OutPtr) - Cardinal(FBlockSize))^, FVector[0], FBlockSize);
    end;
  end
  else
    raise EElSymmetricCryptoError.Create(SUnsupportedOperation);
end;

procedure TElSymmetricCrypto.EncryptUpdate(InBuffer: pointer; InSize : integer; OutBuffer: pointer;
  var OutSize : integer);
var
  EstimatedSize, OldSize, Count, Size : integer;
  InBufPtr, OutBufPtr : pointer;
begin
  if IsStreamCipher then
    EstimatedSize := InSize
  else
  begin
    Count := InSize + Length(FTail);
    EstimatedSize := Count - Count mod FBlockSize;
  end;

  if OutSize = 0 then
  begin
    OutSize := EstimatedSize;
    Exit;
  end;
  if OutSize < EstimatedSize then
    raise EElSymmetricCryptoError.Create(SOutputBufferTooSmall);

  if IsStreamCipher then
  begin
    EncryptBlock(InBuffer, OutBuffer, InSize);
    OutSize := InSize;
    Exit;
  end;

  OldSize := Length(FTail);

  InBufPtr := InBuffer;
  OutBufPtr := OutBuffer;

  if InSize + OldSize < FBlockSize then
  begin
    SetLength(FTail, OldSize + InSize);
    Move(InBuffer^, FTail[OldSize], InSize);
    Count := 0;
  end
  else
  if OldSize > 0 then
  begin
    SetLength(FTail, FBlockSize);
    Size := FBlockSize - OldSize;
    Move(InBuffer^, FTail[OldSize], Size);
    InternalEncrypt(@FTail[0], OutBuffer, FBlockSize);
    InBufPtr := Pointer(PtrUInt(InBuffer) + Cardinal(Size));
    OutBufPtr := Pointer(PtrUInt(OutBuffer) + Cardinal(FBlockSize));
    Count := (InSize - Size) div FBlockSize;
    SetLength(FTail, (InSize - Size) mod FBlockSize);
    if Length(FTail) > 0 then
      Move(Pointer(PtrUInt(InBuffer) + Cardinal(Size) + Cardinal(Count * FBlockSize))^, FTail[0], Length(FTail));
  end
  else
  begin
    Count := EstimatedSize div FBlockSize;
    SetLength(FTail, InSize mod FBlockSize);
    if Length(FTail) > 0 then
      Move(Pointer(PtrUInt(InBuffer) + Cardinal(Count * FBlockSize))^, FTail[0], Length(FTail));
  end;

  if Count > 0 then
    InternalEncrypt(InBufPtr, OutBufPtr, Count * FBlockSize);
  OutSize := EstimatedSize;
end;


procedure TElSymmetricCrypto.Encrypt(InBuffer: pointer; InSize : integer; OutBuffer: pointer;
  var OutSize : integer);
var
  EstimatedSize : integer;
  Count : Integer;
begin
  if (not IsStreamCipher) and ((InSize mod FBlockSize) <> 0) and 
     (FPadding = cpNone) and
     ((FMode = cmECB) or 
      (FMode = cmCBC))
  then
    raise EElSymmetricCryptoError.Create(SInvalidPadding);

  EstimatedSize := EstimatedOutputSize(InSize, true);

  if (OutSize = 0) then
  begin
    OutSize := EstimatedSize;
    Exit;
  end;
  if (OutSize < EstimatedSize) then
    raise EElSymmetricCryptoError.Create(SOutputBufferTooSmall);

  InitializeEncryption;
  Count := OutSize;
  EncryptUpdate(InBuffer, InSize, OutBuffer, Count);
  Dec(OutSize, Count);
  FinalizeEncryption(Pointer(PtrUInt(OutBuffer) + Cardinal(Count)), OutSize);
  Inc(OutSize, Count);
end;

procedure TElSymmetricCrypto.Encrypt(InStream, OutStream: TStream);
var
  BytesLeft, Size, OutSize : integer;
  Buffer, OutBuffer : ByteArray;
begin
  BytesLeft := InStream.Size - InStream.Position;

  if not IsStreamCipher then
  begin
    if ((BytesLeft mod FBlockSize) <> 0) and (FPadding = cpNone)
      and ((FMode = cmECB) or 
           (FMode = cmCBC))
    then
      raise EElSymmetricCryptoError.Create(SInvalidPadding);
  end;    

  SetLength(Buffer, SYMMETRIC_BLOCK_SIZE);
  SetLength(OutBuffer, SYMMETRIC_BLOCK_SIZE);

  InitializeEncryption;

  while BytesLeft > 0 do begin
    Size := InStream.Read(Buffer[0], Min(SYMMETRIC_BLOCK_SIZE, BytesLeft));
    Dec(BytesLeft, Size);
    OutSize := SYMMETRIC_BLOCK_SIZE;
    EncryptUpdate(@Buffer[0], Size, @OutBuffer[0], OutSize);
    OutStream.Write(OutBuffer[0], OutSize);
  end;

  OutSize := SYMMETRIC_BLOCK_SIZE;
  FinalizeEncryption(@OutBuffer[0], OutSize);
  if OutSize > 0 then
    OutStream.Write(OutBuffer[0], OutSize);
end;

procedure TElSymmetricCrypto.FinalizeEncryption(OutBuffer : pointer; var OutSize : integer);
var
  PadBytes : ByteArray;
begin
  if IsStreamCipher then
  begin
    OutSize := 0;
    Exit;
  end;

  { encrypting tail bytes, if are }  
  if (FPadding = cpNone) and (Length(FTail) > 0) then
  begin
    if FMode in [cmCTR,
                 cmCFB8] then
    begin
      if OutSize = 0 then
      begin
        OutSize := Length(FTail);
        Exit;
      end
      else
        if OutSize < Length(FTail)
        then
          raise EElSymmetricCryptoError.Create(SOutputBufferTooSmall);
      OutSize := Length(FTail);
      SetLength(FTail, FBlockSize);
      SetLength(PadBytes, FBlockSize);
      InternalEncrypt(@FTail[0], @PadBytes[0], FBlockSize);
      Move(PadBytes[0], OutBuffer^, OutSize);
    end
    else
      raise EElSymmetricCryptoError.Create(SInvalidPadding);
  end
  else if FPadding = cpPKCS5 then
  begin
    if Length(FTail) = 0 then
      PadBytes := AddPadding(nil, 0)
    else
      PadBytes := AddPadding(@FTail[0], Length(FTail));
    SetLength(FTail, 0);

    EncryptUpdate(@PadBytes[0], Length(PadBytes), OutBuffer, OutSize);
  end
  else if Length(FTail) = 0 then
    OutSize := 0
  else
    raise EElSymmetricCryptoError.Create(SInternalError);

  { must be called as inherited; in descendant classes }
end;

procedure TElSymmetricCrypto.InitializeDecryption;
begin
  SetLength(FVector, Length(FKeyMaterial.IV));

  if Length(FVector) > 0 then
    Move(FKeyMaterial.IV[0], FVector[0], Length(FVector));

  SetLength(FTail, 0);
  SetLength(FPadBytes, 0);
end;

procedure TElSymmetricCrypto.DecryptUpdate(InBuffer: pointer; InSize : integer;
  OutBuffer: pointer; var OutSize : integer);
var
  EstimatedSize, OldSize, Count, Size, OutBytes : integer;
  InBufPtr, OutBufPtr : pointer;
begin
  if IsStreamCipher then
    EstimatedSize := InSize
  else
  begin
    Count := InSize + Length(FTail);
    EstimatedSize := Count - Count mod FBlockSize;
  end;

  if OutSize = 0 then
  begin
    OutSize := EstimatedSize;
    Exit;
  end;
  if OutSize < EstimatedSize then
    raise EElSymmetricCryptoError.Create(SOutputBufferTooSmall);

  if IsStreamCipher then
  begin
    DecryptBlock(InBuffer, OutBuffer, InSize);
    OutSize := InSize;
    Exit;
  end;

  OldSize := Length(FTail);
  OutBytes := 0;

  InBufPtr := InBuffer;
  OutBufPtr := OutBuffer;

  if InSize + OldSize < FBlockSize then
  begin
    SetLength(FTail, OldSize + InSize);
    Move(InBuffer^, FTail[OldSize], InSize);
    Count := 0;
  end
  else
  if OldSize > 0 then
  begin
    SetLength(FTail, FBlockSize);
    Size := FBlockSize - OldSize;
    Move(InBuffer^, FTail[OldSize], Size);

    if (FPadding <> cpNone) then
    begin
      if Length(FPadBytes) > 0 then
      begin
        Move(FPadBytes[0], OutBuffer^, FBlockSize);
        Inc(OutBytes, FBlockSize);
      end
      else
        SetLength(FPadBytes, FBlockSize);

      InternalDecrypt(@FTail[0], @FPadBytes[0], FBlockSize);
    end
    else
    begin
      InternalDecrypt(@FTail[0], OutBuffer, FBlockSize);
      Inc(OutBytes, FBlockSize);
    end;

    InBufPtr := Pointer(PtrUInt(InBuffer) + Cardinal(Size));
    OutBufPtr := Pointer(PtrUInt(OutBuffer) + Cardinal(OutBytes));

    Count := (InSize - Size) div FBlockSize;
    SetLength(FTail, (InSize - Size) mod FBlockSize);
    if Length(FTail) > 0 then
      Move(Pointer(PtrUInt(InBufPtr) + Cardinal(Count * FBlockSize))^, FTail[0], Length(FTail));
  end
  else
  begin
    InBufPtr := InBuffer;
    OutBufPtr := OutBuffer;
    Count := EstimatedSize div FBlockSize;
    SetLength(FTail, InSize mod FBlockSize);
    if Length(FTail) > 0 then
      Move(Pointer(PtrUInt(InBuffer) + Cardinal(Count * FBlockSize))^, FTail[0], Length(FTail));
  end;

  if Count > 0 then
  begin
    { dealing with last block of decrypted data, which can represent padding }
    if FPadding <> cpNone then
    begin
      if Length(FPadBytes) > 0 then
      begin
        Move(FPadBytes[0], OutBufPtr^, FBlockSize);
        OutBufPtr := Pointer(PtrUInt(OutBufPtr) + Cardinal(FBlockSize));

        Inc(OutBytes, FBlockSize);
      end
      else
        SetLength(FPadBytes, FBlockSize);

      if Count > 1 then
      begin
        InternalDecrypt(InBufPtr, OutBufPtr, (Count - 1) * FBlockSize);
        InBufPtr := Pointer(PtrUInt(InBufPtr) + Cardinal((Count - 1) * FBlockSize));
        Inc(OutBytes, (Count - 1) * FBlockSize);
      end;
      InternalDecrypt(InBufPtr, @FPadBytes[0], FBlockSize);
    end
    else
    begin
      InternalDecrypt(InBufPtr, OutBufPtr, Count * FBlockSize);
      Inc(OutBytes, Count * FBlockSize);
    end;
  end;
  OutSize := OutBytes;
end;

procedure TElSymmetricCrypto.InternalDecrypt(Buffer, OutBuffer : pointer; Size : integer);
var
  Block : ByteArray;
  Index, Count, XIndex : Integer;
  BufferPtr, OutPtr : pointer;
begin
  OutPtr := OutBuffer;
  BufferPtr := Buffer;

  Count := Size div FBlockSize;

  { ECB mode }
  if FMode = cmECB then
  begin
    for Index := 0 to Count - 1 do
    begin
      DecryptBlock(BufferPtr, OutPtr);
      BufferPtr := Pointer(PtrUInt(BufferPtr) + Cardinal(FBlockSize));
      OutPtr := Pointer(PtrUInt(OutPtr) + Cardinal(FBlockSize));
    end;
  end
  { CBC mode }
  else if FMode = cmCBC then
  begin
    if Length(FVector) <> FBlockSize then
      raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

    SetLength(Block, FBlockSize);

    for Index := 0 to Count - 1 do
    begin
      { currently supported ciphers have only 8 and 16 bytes block size }
      DecryptBlock(BufferPtr, @Block[0]);

      if FBlockSize = 8 then
      begin
        PLongWord(OutPtr)^ := PLongWord(@FVector[0])^ xor PLongWord(@Block[0])^;
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@FVector[4])^ xor PLongWord(@Block[4])^;
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
      end
      else if FBlockSize = 16 then
      begin
        PLongWord(OutPtr)^ := PLongWord(@FVector[0])^ xor PLongWord(@Block[0])^;
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@FVector[4])^ xor PLongWord(@Block[4])^;
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@FVector[8])^ xor PLongWord(@Block[8])^;
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@FVector[12])^ xor PLongWord(@Block[12])^;
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
      end
      else
        raise EElSymmetricCryptoError.Create(SInternalException);

      Move(BufferPtr^, FVector[0], FBlockSize);
      BufferPtr := Pointer(PtrUInt(BufferPtr) + Cardinal(FBlockSize));
    end;
  end
  { CTR - mode }
  else if FMode = cmCTR then
  begin
    if Length(FVector) <> FBlockSize then
      raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

    SetLength(Block, FBlockSize);

    for Index := 0 to Count - 1 do
    begin
      EncryptBlock(@FVector[0], @Block[0]);

      if FBlockSize = 8 then
      begin
        PLongWord(OutPtr)^ := PLongWord(@Block[0])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[4])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
      end
      else if FBlockSize = 16 then
      begin
        PLongWord(OutPtr)^ := PLongWord(@Block[0])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[4])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[8])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[12])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
      end
      else
        raise EElSymmetricCryptoError.Create(SInternalException);

      { incrementing counter }
      for XIndex := FBlockSize - 1 downto 0 do
        if FVector[XIndex] = $ff then
          FVector[XIndex] := 0
        else
        begin
          Inc(FVector[XIndex]);
          Break;
        end;
    end;
  end
  { CFB 8-bit mode }
  else if FMode = cmCFB8 then
  begin
    if Length(FVector) <> FBlockSize then
      raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

    Count := Size div FBlockSize;
    SetLength(Block, FBlockSize);

    for Index := 0 to Count - 1 do
    begin
      EncryptBlock(@FVector[0], @Block[0]);
      if FBlockSize = 8 then
      begin
        PLongWord(OutPtr)^ := PLongWord(@Block[0])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[4])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
      end
      else if FBlockSize = 16 then
      begin
        PLongWord(OutPtr)^ := PLongWord(@Block[0])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[4])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[8])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
        PLongWord(OutPtr)^ := PLongWord(@Block[12])^ xor PLongWord(BufferPtr)^;
        BufferPtr := Pointer(PtrUInt(BufferPtr) + 4);
        OutPtr := Pointer(PtrUInt(OutPtr) + 4);
      end
      else
        raise EElSymmetricCryptoError.Create(SInternalException);

      Move(Pointer(PtrUInt(BufferPtr) - Cardinal(FBlockSize))^, FVector[0], FBlockSize);
    end;
  end
  else
    raise EElSymmetricCryptoError.Create(SUnsupportedOperation);
end;

procedure TElSymmetricCrypto.Decrypt(InBuffer: pointer; InSize : integer; OutBuffer: pointer;
  var OutSize : integer);
var
  EstimatedSize, Count : integer;
begin
  if (not IsStreamCipher) and ((InSize mod FBlockSize) <> 0)
    and ((FMode = cmECB) or 
         (FMode = cmCBC))
  then
    raise EElSymmetricCryptoError.Create(SInvalidInputSize);


  EstimatedSize := EstimatedOutputSize(InSize, false);
  if (OutSize = 0) then
  begin
    OutSize := EstimatedSize;
    Exit;
  end;
  if (OutSize < EstimatedSize) then
    raise EElSymmetricCryptoError.Create(SOutputBufferTooSmall);

  InitializeDecryption;
  Count := OutSize;
  DecryptUpdate(InBuffer, InSize, OutBuffer, Count);
  Dec(OutSize, Count);
  FinalizeDecryption(Pointer(PtrUInt(OutBuffer) + Cardinal(Count)), OutSize);
  Inc(OutSize, Count);
end;

procedure TElSymmetricCrypto.Decrypt(InStream, OutStream: TStream);
var
  BytesLeft, Size, OutSize : integer;
  Buffer, OutBuffer : ByteArray;
begin
  BytesLeft := InStream.Size - InStream.Position;

  if (not IsStreamCipher) and ((BytesLeft mod FBlockSize) <> 0) and
    ((FMode = cmECB) or 
     (FMode = cmCBC))
  then
    raise EElSymmetricCryptoError.Create(SInvalidPadding);

  SetLength(Buffer, SYMMETRIC_BLOCK_SIZE);
  SetLength(OutBuffer, SYMMETRIC_BLOCK_SIZE);

  InitializeDecryption;

  while BytesLeft > 0 do begin
    Size := InStream.Read(Buffer[0], Min(SYMMETRIC_BLOCK_SIZE, BytesLeft));
    Dec(BytesLeft, Size);
    OutSize := SYMMETRIC_BLOCK_SIZE;
    DecryptUpdate(@Buffer[0], Size, @OutBuffer[0], OutSize);
    OutStream.Write(OutBuffer[0], OutSize);
  end;

  OutSize := SYMMETRIC_BLOCK_SIZE;
  FinalizeDecryption(@OutBuffer[0], OutSize);
  if OutSize > 0 then
    OutStream.Write(OutBuffer[0], OutSize);
end;

procedure TElSymmetricCrypto.FinalizeDecryption(OutBuffer : pointer; var OutSize : integer);
var
  EstimatedSize: integer;
  Block : ByteArray;
begin
  if IsStreamCipher then begin
    OutSize := 0;
    Exit;
  end;  


  if (Length(FTail) > 0) and ((FPadding <> cpNone) or
    (FMode = cmCBC) or 
    (FMode = cmECB))
  then
    raise EElSymmetricCryptoError.Create(SInvalidPadding);

   if FPadding = cpNone then
  begin
    if Length(FTail) > 0 then
    begin
      if OutSize = 0 then
      begin
        OutSize := Length(FTail);
        Exit;
      end
      else
        if OutSize < Length(FTail) then
          raise EElSymmetricCryptoError.Create(SOutputBufferTooSmall);

      OutSize := Length(FTail);
      SetLength(FTail, FBlockSize);
      SetLength(Block, FBlockSize);
      InternalDecrypt(@FTail[0], @Block[0], FBlockSize);
      Move(Block[0], OutBuffer^, OutSize);
    end
  end
  else if Length(FPadBytes) > 0 then
  begin
    if FPadding = cpNone then
      EstimatedSize := FBlockSize
    else if FPadding = cpPKCS5 then
      EstimatedSize := FBlockSize - FPadBytes[FBlockSize - 1]
    else
      raise EElSymmetricCryptoError.Create(SInternalException);

    if (EstimatedSize < 0) or 
       (EstimatedSize > FBlockSize) then
      raise EElSymmetricCryptoError.Create(SInvalidPadding); 

    if OutSize = 0 then
    begin
      OutSize := EstimatedSize;
      Exit;
    end;
    if OutSize < EstimatedSize then
      raise EElSymmetricCryptoError.Create(SOutputBufferTooSmall);

    OutSize := EstimatedSize;
    Move(FPadBytes[0], OutBuffer^, EstimatedSize);
    SetLength(FPadBytes, 0);
  end
  else
    OutSize := 0;

  { must be called as inherited; in descendant classes }
end;

function TElSymmetricCrypto.AddPadding(Block : pointer; Size : integer) : ByteArray;
var
  Index : integer;
begin
  if Size >= FBlockSize then
    raise EElSymmetricCryptoError.Create(SInternalError);

  if FPadding = cpNone then
  begin
    SetLength(Result, Size);
    if Size > 0 then
      Move(Block^, Result[0], Size);
  end
  else if FPadding = cpPKCS5 then
  begin
    SetLength(Result, FBlockSize);
    Move(Block^, Result[0], Size);
    for Index := Size to FBlockSize - 1 do
      Result[Index] := FBlockSize - Size;
  end
  else
    raise EElSymmetricCryptoError.Create(SInternalError);
end;

procedure TElSymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer);
begin
  //must be overriden in derived classes
end;

procedure TElSymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer; Size : integer);
begin
  //for stream ciphers, must be overriden in derived classes
end;

procedure TElSymmetricCrypto.DecryptBlock(Buffer, OutBuffer : pointer);
begin
  //must be overriden in derived classes
end;

procedure TElSymmetricCrypto.DecryptBlock(Buffer, OutBuffer : pointer; Size : integer);
begin
  //for stream ciphers, must be overriden in derived classes
end;


////////////////////////////////////////////////////////////////////////////////
//  TElSymmetricCryptoFactory

constructor TElSymmetricCryptoFactory.Create;
begin
  inherited;

  FRegisteredClasses := TList.Create;
  RegisterDefaultClasses;
end;

destructor TElSymmetricCryptoFactory.Destroy;
begin
  FreeAndNil(FRegisteredClasses);
  inherited;
end;

procedure TElSymmetricCryptoFactory.RegisterDefaultClasses;
begin
  RegisterClass(TElAESSymmetricCrypto);
  RegisterClass(TElBlowfishSymmetricCrypto);
  RegisterClass(TElTwofishSymmetricCrypto);
  {$ifndef SBB_NO_IDEA}if IDEAEnabled then RegisterClass(TElIDEASymmetricCrypto);{$endif}
  RegisterClass(TElCAST128SymmetricCrypto);
  {$ifndef SBB_NO_RC2}RegisterClass(TElRC2SymmetricCrypto);{$endif}
  {$ifndef SBB_NO_RC4}RegisterClass(TElRC4SymmetricCrypto);{$endif}
  {$ifndef SBB_NO_DES}RegisterClass(TElDESSymmetricCrypto);
  RegisterClass(TEl3DESSymmetricCrypto);{$endif}
  {$ifndef SBB_NO_CAMELLIA}RegisterClass(TElCamelliaSymmetricCrypto);{$endif}
  RegisterClass(TElSerpentSymmetricCrypto);
end;

function TElSymmetricCryptoFactory.GetRegisteredClass(Index: integer) : TElSymmetricCryptoClass;
begin
  Result := TElSymmetricCryptoClass(FRegisteredClasses[Index]);
end;

function TElSymmetricCryptoFactory.GetRegisteredClassCount: integer;
begin
  Result := FRegisteredClasses.Count;
end;

procedure TElSymmetricCryptoFactory.RegisterClass(Cls : TElSymmetricCryptoClass);
begin
  FRegisteredClasses.Add((Cls));
end;

function TElSymmetricCryptoFactory.CreateInstance(const OID : BufferType;
  Mode : TSBSymmetricCryptoMode = cmDefault): TElSymmetricCrypto;
var
  I : integer;
  Cls : TElSymmetricCryptoClass;
begin
  Result := nil;
  for I := 0 to FRegisteredClasses.Count - 1 do
  begin
    Cls := TElSymmetricCryptoClass(FRegisteredClasses[I]);
    if Cls.IsAlgorithmSupported(OID) then
    begin
      Result := Cls.Create(OID, Mode);
      Break;
    end;
  end;
end;

function TElSymmetricCryptoFactory.CreateInstance(Alg : integer;
  Mode : TSBSymmetricCryptoMode = cmDefault): TElSymmetricCrypto;
var
  I : integer;
  Cls : TElSymmetricCryptoClass;
begin
  Result := nil;
  for I := 0 to FRegisteredClasses.Count - 1 do
  begin
    Cls := TElSymmetricCryptoClass(FRegisteredClasses[I]);
    if Cls.IsAlgorithmSupported(Alg) then
    begin
      Result := Cls.Create(Alg, Mode);
      Break;
    end;
  end;
end;

function TElSymmetricCryptoFactory.IsAlgorithmSupported(const OID : BufferType): boolean;
var
  I : integer;
begin
  Result := false;
  for I := 0 to FRegisteredClasses.Count - 1 do
  begin
    if TElSymmetricCryptoClass(FRegisteredClasses[I]).IsAlgorithmSupported(OID) then
    begin
      Result := true;
      Break;
    end;
  end;
end;

function TElSymmetricCryptoFactory.IsAlgorithmSupported(Alg : integer): boolean;
var
  I : integer;
begin
  Result := false;
  for I := 0 to FRegisteredClasses.Count - 1 do
  begin
    if TElSymmetricCryptoClass(FRegisteredClasses[I]).IsAlgorithmSupported(Alg) then
    begin
      Result := true;
      Break;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//  TElAESSymmetricCrypto

procedure TElAESSymmetricCrypto.SetKeyMaterial(Material : TElSymmetricKeyMaterial);
begin
  if not (Length(Material.IV) in [0, 16]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);
  if not (Length(Material.Key) in [16, 24, 32]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  inherited;
end;

procedure TElAESSymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer);
begin
  if FKeySize = 16 then
    SBAES.Encrypt128(PAESBuffer(Buffer)^, FKey128, PAESBuffer(OutBuffer)^)
  else if FKeySize = 24 then
    SBAES.Encrypt192(PAESBuffer(Buffer)^, FKey192, PAESBuffer(OutBuffer)^)
  else if FKeySize = 32 then
    SBAES.Encrypt256(PAESBuffer(Buffer)^, FKey256, PAESBuffer(OutBuffer)^);
end;

procedure TElAESSymmetricCrypto.DecryptBlock(Buffer, OutBuffer: pointer);
begin
  if FKeySize = 16 then
    SBAES.Decrypt128(PAESBuffer(Buffer)^, FKey128, PAESBuffer(OutBuffer)^)
  else if FKeySize = 24 then
    SBAES.Decrypt192(PAESBuffer(Buffer)^, FKey192, PAESBuffer(OutBuffer)^)
  else if FKeySize = 32 then
    SBAES.Decrypt256(PAESBuffer(Buffer)^, FKey256, PAESBuffer(OutBuffer)^);
end;

class function TElAESSymmetricCrypto.IsAlgorithmSupported(AlgID : integer) : boolean;
begin
  if (AlgID = SB_ALGORITHM_CNT_AES128) or
    (AlgID = SB_ALGORITHM_CNT_AES192) or
    (AlgID = SB_ALGORITHM_CNT_AES256)
  then
    Result := true
  else
    Result := false;
end;

class function TElAESSymmetricCrypto.IsAlgorithmSupported(const AlgOID : BufferType) : boolean;
begin
  if CompareContent(AlgOID, SB_OID_AES128_CBC) or
    CompareContent(AlgOID, SB_OID_AES192_CBC) or
    CompareContent(AlgOID, SB_OID_AES256_CBC)
  then
    Result := true
  else
    Result := false;
end;

constructor TElAESSymmetricCrypto.Create(AlgID : integer;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  if AlgID = SB_ALGORITHM_CNT_AES128 then
  begin
    inherited Create(Mode);
    FBlockSize := 16;
    FKeySize := 16;
  end
  else if AlgID = SB_ALGORITHM_CNT_AES192 then
  begin
    inherited Create(Mode);
    FBlockSize := 16;
    FKeySize := 24;
  end
  else if AlgID = SB_ALGORITHM_CNT_AES256 then
  begin
    inherited Create(Mode);
    FBlockSize := 16;
    FKeySize := 32;
  end
  else
  begin
    raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmInt, [AlgID]));
  end;  
end;

constructor TElAESSymmetricCrypto.Create(const AlgOID : BufferType;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  if CompareContent(AlgOID, SB_OID_AES128_CBC) then
  begin
    if not (Mode in [cmDefault, 
                     cmCBC]) then
      raise EElSymmetricCryptoError.Create(SInvalidCipherMode);
            
    inherited Create(cmCBC);
    FOID := AlgOID;
    FBlockSize := 16;
    FKeySize := 16;
  end
  else if CompareContent(AlgOID, SB_OID_AES192_CBC) then
  begin
    if not (Mode in [cmDefault, 
                     cmCBC]) then
      raise EElSymmetricCryptoError.Create(SInvalidCipherMode);
            
    inherited Create(cmCBC);
    FOID := AlgOID;
    FBlockSize := 16;
    FKeySize := 24;
  end
  else if CompareContent(AlgOID, SB_OID_AES256_CBC) then
  begin
    if not (Mode in [cmDefault, 
                     cmCBC]) then
      raise EElSymmetricCryptoError.Create(SInvalidCipherMode);
            
    inherited Create(cmCBC);
    FOID := AlgOID;
    FBlockSize := 16;
    FKeySize := 32;
  end
  else
  begin
    inherited Create;
    raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmOID, [AlgOID]));
  end;  
end;


procedure TElAESSymmetricCrypto.InitializeEncryption;
begin
  inherited InitializeEncryption;

  if FKeySize = 16 then
    SBAES.ExpandKeyForEncryption128(PAESKey128(@FKeyMaterial.Key[0])^, FKey128)
  else if FKeySize = 24 then
    SBAES.ExpandKeyForEncryption192(PAESKey192(@FKeyMaterial.Key[0])^, FKey192)
  else if FKeySize = 32 then
    SBAES.ExpandKeyForEncryption256(PAESKey256(@FKeyMaterial.Key[0])^, FKey256);
end;

procedure TElAESSymmetricCrypto.InitializeDecryption;
begin
  inherited InitializeDecryption;

  if FKeySize = 16 then
  begin
    SBAES.ExpandKeyForEncryption128(PAESKey128(@FKeyMaterial.Key[0])^, FKey128);

    if (FMode = cmCBC) or (FMode = cmECB) then
      SBAES.ExpandKeyForDecryption128(FKey128);
  end
  else if FKeySize = 24 then
  begin
    SBAES.ExpandKeyForEncryption192(PAESKey192(@FKeyMaterial.Key[0])^, FKey192);
    if (FMode = cmCBC) or (FMode = cmECB) then
      SBAES.ExpandKeyForDecryption192(FKey192);
  end
  else if FKeySize = 32 then
  begin
    SBAES.ExpandKeyForEncryption256(PAESKey256(@FKeyMaterial.Key[0])^, FKey256);
    if (FMode = cmCBC) or (FMode = cmECB) then
      SBAES.ExpandKeyForDecryption256(FKey256);
  end;
end;


////////////////////////////////////////////////////////////////////////////////
//  TElBlowfishSymmetricCrypto

procedure TElBlowfishSymmetricCrypto.SetKeyMaterial(Material : TElSymmetricKeyMaterial);
begin
  if not (Length(Material.IV) in [0, 8]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);
  if (Length(Material.Key) < 4) or (Length(Material.Key) > 56) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  inherited;
end;

procedure TElBlowfishSymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer);
var
  L, R : Cardinal;
begin
  L := (PByteArray(Buffer)[0] shl 24) or (PByteArray(Buffer)[1] shl 16) or
    (PByteArray(Buffer)[2] shl 8) or PByteArray(Buffer)[3];
  R := (PByteArray(Buffer)[4] shl 24) or (PByteArray(Buffer)[5] shl 16) or
    (PByteArray(Buffer)[6] shl 8) or PByteArray(Buffer)[7];

  SBBlowfish.EncryptBlock(FContext, L, R);

  PByteArray(OutBuffer)[0] := L shr 24;
  PByteArray(OutBuffer)[1] := (L shr 16) and $FF;
  PByteArray(OutBuffer)[2] := (L shr 8) and $FF;
  PByteArray(OutBuffer)[3] := L and $FF;
  PByteArray(OutBuffer)[4] := R shr 24;
  PByteArray(OutBuffer)[5] := (R shr 16) and $FF;
  PByteArray(OutBuffer)[6] := (R shr 8) and $FF;
  PByteArray(OutBuffer)[7] := R and $FF;
end;

procedure TElBlowfishSymmetricCrypto.DecryptBlock(Buffer, OutBuffer: pointer);
var
  L, R : Cardinal;
begin
  L := (PByteArray(Buffer)[0] shl 24) or (PByteArray(Buffer)[1] shl 16) or
    (PByteArray(Buffer)[2] shl 8) or PByteArray(Buffer)[3];
  R := (PByteArray(Buffer)[4] shl 24) or (PByteArray(Buffer)[5] shl 16) or
    (PByteArray(Buffer)[6] shl 8) or PByteArray(Buffer)[7];

  SBBlowfish.DecryptBlock(FContext, L, R);

  PByteArray(OutBuffer)[0] := L shr 24;
  PByteArray(OutBuffer)[1] := (L shr 16) and $FF;
  PByteArray(OutBuffer)[2] := (L shr 8) and $FF;
  PByteArray(OutBuffer)[3] := L and $FF;
  PByteArray(OutBuffer)[4] := R shr 24;
  PByteArray(OutBuffer)[5] := (R shr 16) and $FF;
  PByteArray(OutBuffer)[6] := (R shr 8) and $FF;
  PByteArray(OutBuffer)[7] := R and $FF;
end;

class function TElBlowfishSymmetricCrypto.IsAlgorithmSupported(AlgID : integer) : boolean;
begin
  if (AlgID = SB_ALGORITHM_CNT_BLOWFISH) then
    Result := true
  else
    Result := false;
end;

class function TElBlowfishSymmetricCrypto.IsAlgorithmSupported(const AlgOID : BufferType) : boolean;
begin
  Result := false;
end;

constructor TElBlowfishSymmetricCrypto.Create(AlgID : integer;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  if AlgID = SB_ALGORITHM_CNT_BLOWFISH then
  begin
    inherited Create(Mode);
    FBlockSize := 8;
  end
  else
  begin
    raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmInt, [AlgID]));
  end;
end;

constructor TElBlowfishSymmetricCrypto.Create(const AlgOID : BufferType;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  { no OID's for Blowfish found }
  inherited Create;
  raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmOID, [AlgOID]));
end;


procedure TElBlowfishSymmetricCrypto.InitializeEncryption;
begin
  inherited InitializeEncryption;

  SBBlowfish.Initialize(FContext, TSBBlowfishKey(FKeyMaterial.Key));
end;

procedure TElBlowfishSymmetricCrypto.InitializeDecryption;
begin
  inherited InitializeDecryption;

  SBBlowfish.Initialize(FContext, TSBBlowfishKey(FKeyMaterial.Key));
end;


////////////////////////////////////////////////////////////////////////////////
//  TElTwofishSymmetricCrypto

procedure TElTwofishSymmetricCrypto.SetKeyMaterial(Material : TElSymmetricKeyMaterial);
begin
  if not (Length(Material.IV) in [0, 16]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);
  if not (Length(Material.Key) in [16, 24, 32]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  inherited;
end;

procedure TElTwofishSymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer);
begin
  SBTwofish.EncryptBlock(FKey, Buffer, OutBuffer);
end;

procedure TElTwofishSymmetricCrypto.DecryptBlock(Buffer, OutBuffer: pointer);
begin
  SBTwofish.DecryptBlock(FKey, Buffer, OutBuffer);
end;

class function TElTwofishSymmetricCrypto.IsAlgorithmSupported(AlgID : integer) : boolean;
begin
  if (AlgID = SB_ALGORITHM_CNT_TWOFISH) then
    Result := true
  else
    Result := false;
end;

class function TElTwofishSymmetricCrypto.IsAlgorithmSupported(const AlgOID : BufferType) : boolean;
begin
  Result := false;
end;

constructor TElTwofishSymmetricCrypto.Create(AlgID : integer;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  if AlgID = SB_ALGORITHM_CNT_TWOFISH then
  begin
    inherited Create(Mode);
    FBlockSize := 16;
  end
  else
  begin
    raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmInt, [AlgID]));
  end;
end;

constructor TElTwofishSymmetricCrypto.Create(const AlgOID : BufferType;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  { no OID's for Twofish found }
  inherited Create;
  raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmOID, [AlgOID]));
end;


procedure TElTwofishSymmetricCrypto.InitializeEncryption;
begin
  inherited InitializeEncryption;

  SBTwofish.ExpandKey(FKeyMaterial.Key, FKey);
end;

procedure TElTwofishSymmetricCrypto.InitializeDecryption;
begin
  inherited InitializeDecryption;

  SBTwofish.ExpandKey(FKeyMaterial.Key, FKey);
end;

////////////////////////////////////////////////////////////////////////////////
//  TElIDEASymmetricCrypto

{$ifndef SBB_NO_IDEA}

procedure TElIDEASymmetricCrypto.SetKeyMaterial(Material : TElSymmetricKeyMaterial);
begin
  if not (Length(Material.IV) in [0, 8]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);
  if not (Length(Material.Key) = 16) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  inherited;
end;

procedure TElIDEASymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer);
begin
  SBIDEA.Encrypt(PIDEABuffer(Buffer)^, FKey, PIDEABuffer(OutBuffer)^);
end;

procedure TElIDEASymmetricCrypto.DecryptBlock(Buffer, OutBuffer: pointer);
begin
  SBIDEA.Decrypt(PIDEABuffer(Buffer)^, FKey, PIDEABuffer(OutBuffer)^);
end;

class function TElIDEASymmetricCrypto.IsAlgorithmSupported(AlgID : integer) : boolean;
begin
  if (AlgID = SB_ALGORITHM_CNT_IDEA) then
    Result := true
  else
    Result := false;
end;

class function TElIDEASymmetricCrypto.IsAlgorithmSupported(const AlgOID : BufferType) : boolean;
begin
  Result := false;
end;

constructor TElIDEASymmetricCrypto.Create(AlgID : integer;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  if AlgID = SB_ALGORITHM_CNT_IDEA then
  begin
    inherited Create(Mode);
    FBlockSize := 8;
    FKeySize := 16;
  end
  else
  begin
    raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmInt, [AlgID]));
  end;
end;

constructor TElIDEASymmetricCrypto.Create(const AlgOID : BufferType;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  { no OID's for IDEA found }
  inherited Create;
  raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmOID, [AlgOID]));
end;


procedure TElIDEASymmetricCrypto.InitializeEncryption;
begin
  inherited InitializeEncryption;

  SBIDEA.ExpandKeyForEncryption(PIDEAKey(@FKeyMaterial.Key[0])^, FKey);
end;

procedure TElIDEASymmetricCrypto.InitializeDecryption;
var
  DecKey : TIDEAExpandedKey; 
begin
  inherited InitializeDecryption;

  SBIDEA.ExpandKeyForEncryption(PIDEAKey(@FKeyMaterial.Key[0])^, FKey);

  if FMode in [cmECB,
               cmCBC] then
  begin
    SBIDEA.ExpandKeyForDecryption(FKey, DecKey);
    FKey := DecKey;
  end;  
end;

{$endif}

////////////////////////////////////////////////////////////////////////////////
//  TElCAST128SymmetricCrypto

procedure TElCAST128SymmetricCrypto.SetKeyMaterial(Material : TElSymmetricKeyMaterial);
begin
  if not (Length(Material.IV) in [0, 8]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);
  if (Length(Material.Key) <> 16)then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  inherited;
end;

procedure TElCAST128SymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer);
begin
  SBCAST128.Encrypt16(PByteArray(OutBuffer), PByteArray(Buffer), FKey);
end;

procedure TElCAST128SymmetricCrypto.DecryptBlock(Buffer, OutBuffer: pointer);
begin
  SBCAST128.Decrypt16(PByteArray(OutBuffer), PByteArray(Buffer), FKey);
end;

class function TElCAST128SymmetricCrypto.IsAlgorithmSupported(AlgID : integer) : boolean;
begin
  if (AlgID = SB_ALGORITHM_CNT_CAST128) then
    Result := true
  else
    Result := false;
end;

class function TElCAST128SymmetricCrypto.IsAlgorithmSupported(const AlgOID : BufferType) : boolean;
begin
  Result := false;
end;

constructor TElCAST128SymmetricCrypto.Create(AlgID : integer;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  if AlgID = SB_ALGORITHM_CNT_CAST128 then
  begin
    inherited Create(Mode);
    FBlockSize := 8;
    FKeySize := 16;
  end
  else
  begin
    raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmInt, [AlgID]));
  end;
end;

constructor TElCAST128SymmetricCrypto.Create(const AlgOID : BufferType;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  { no OID's for CAST128 found }
  inherited Create;
  raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmOID, [AlgOID]));
end;


procedure TElCAST128SymmetricCrypto.InitializeEncryption;
begin
  inherited InitializeEncryption;

  SBCAST128.ExpandKey(PCAST128Key(@FKeyMaterial.Key[0])^, FKey);
end;

procedure TElCAST128SymmetricCrypto.InitializeDecryption;
begin
  inherited InitializeDecryption;

  SBCAST128.ExpandKey(PCAST128Key(@FKeyMaterial.Key[0])^, FKey);
end;

////////////////////////////////////////////////////////////////////////////////
//  TElRC2SymmetricCrypto

{$ifndef SBB_NO_RC2}
procedure TElRC2SymmetricCrypto.SetKeyMaterial(Material : TElSymmetricKeyMaterial);
begin
  if not (Length(Material.IV) in [0, 8]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);
  if (Length(Material.Key) < 1) or (Length(Material.Key) > 16) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  inherited;
end;

procedure TElRC2SymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer);
begin
  SBRC2.Encrypt(TRC2Buffer(Buffer^), FKey, TRC2Buffer(OutBuffer^));
end;

procedure TElRC2SymmetricCrypto.DecryptBlock(Buffer, OutBuffer: pointer);
begin
  SBRC2.Decrypt(TRC2Buffer(Buffer^), FKey, TRC2Buffer(OutBuffer^));
end;

class function TElRC2SymmetricCrypto.IsAlgorithmSupported(AlgID : integer) : boolean;
begin
  if (AlgID = SB_ALGORITHM_CNT_RC2) then
    Result := true
  else
    Result := false;
end;

class function TElRC2SymmetricCrypto.IsAlgorithmSupported(const AlgOID : BufferType) : boolean;
begin
  Result := false;
end;

constructor TElRC2SymmetricCrypto.Create(AlgID : integer;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  if AlgID = SB_ALGORITHM_CNT_RC2 then
  begin
    inherited Create(Mode);
    FBlockSize := 8;
  end
  else
  begin
    raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmInt, [AlgID]));
  end;
end;

constructor TElRC2SymmetricCrypto.Create(const AlgOID : BufferType;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  { no OID's for RC2 found }
  inherited Create;
  raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmOID, [AlgOID]));
end;


procedure TElRC2SymmetricCrypto.InitializeEncryption;
begin
  inherited InitializeEncryption;


  SBRC2.ExpandKey(TRC2Key(FKeyMaterial.Key), FKey);
end;

procedure TElRC2SymmetricCrypto.InitializeDecryption;
begin
  inherited InitializeDecryption;


  SBRC2.ExpandKey(TRC2Key(FKeyMaterial.Key), FKey);
end;
{$endif}

////////////////////////////////////////////////////////////////////////////////
//  TElRC4SymmetricCrypto
class function TElRC4SymmetricCrypto.GetIsStreamCipher : boolean;
begin
  Result := true;
end;

{$ifndef SBB_NO_RC4}
procedure TElRC4SymmetricCrypto.SetKeyMaterial(Material : TElSymmetricKeyMaterial);
begin
  if (Length(Material.Key) < 1) or (Length(Material.Key) > 32) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  inherited;
end;

procedure TElRC4SymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer; Size : integer);
begin
  SBRC4.Encrypt(FContext, Buffer, OutBuffer, Size)
end;

procedure TElRC4SymmetricCrypto.DecryptBlock(Buffer, OutBuffer: pointer; Size : integer);
begin
  SBRC4.Decrypt(FContext, Buffer, OutBuffer, Size)
end;

class function TElRC4SymmetricCrypto.IsAlgorithmSupported(AlgID : integer) : boolean;
begin
  if (AlgID = SB_ALGORITHM_CNT_RC4) then
    Result := true
  else
    Result := false;
end;

class function TElRC4SymmetricCrypto.IsAlgorithmSupported(const AlgOID : BufferType) : boolean;
begin
  Result := false;
end;

constructor TElRC4SymmetricCrypto.Create(AlgID : integer;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  if AlgID = SB_ALGORITHM_CNT_RC4 then
  begin
    if Mode <> cmDefault then
      raise EElSymmetricCryptoError.Create(SInvalidCipherMode);

    inherited Create(Mode);
    FBlockSize := 1;
    FSkipKeyStreamBytes := 0;
  end
  else
  begin
    raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmInt, [AlgID]));
  end;
end;

constructor TElRC4SymmetricCrypto.Create(const AlgOID : BufferType;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  { no OID's for RC4 found }
  inherited Create;
  raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmOID, [AlgOID]));
end;


procedure TElRC4SymmetricCrypto.InitializeEncryption;
var
  Buf, OutBuf : ByteArray;
begin
  inherited InitializeEncryption;

  SBRC4.Initialize(FContext, TRC4Key(FKeyMaterial.Key));

  if FSkipKeyStreamBytes > 0 then
  begin
    SetLength(Buf, FSkipKeyStreamBytes);
    SetLength(OutBuf, FSkipKeyStreamBytes);
    SBRC4.Encrypt(FContext, @Buf[0], @OutBuf[0], FSkipKeyStreamBytes);
  end;
end;

procedure TElRC4SymmetricCrypto.InitializeDecryption;
var
  Buf, OutBuf : ByteArray;
begin
  inherited InitializeDecryption;

  SBRC4.Initialize(FContext, TRC4Key(FKeyMaterial.Key));

  if FSkipKeyStreamBytes > 0 then
  begin
    SetLength(Buf, FSkipKeyStreamBytes);
    SetLength(OutBuf, FSkipKeyStreamBytes);
    SBRC4.Decrypt(FContext, @Buf[0], @OutBuf[0], FSkipKeyStreamBytes);
  end;
end;
{$endif}

////////////////////////////////////////////////////////////////////////////////
//  TElDESSymmetricCrypto

{$ifndef SBB_NO_DES}
procedure TElDESSymmetricCrypto.SetKeyMaterial(Material : TElSymmetricKeyMaterial);
begin
  if not (Length(Material.IV) in [0, 8]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);
  if (Length(Material.Key) <> 8) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  inherited;
end;

procedure TElDESSymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer);
begin
  SBDES.Encrypt(TDESBuffer(Buffer^), FKey, TDESBuffer(OutBuffer^));
end;

procedure TElDESSymmetricCrypto.DecryptBlock(Buffer, OutBuffer: pointer);
begin
  SBDES.Decrypt(TDESBuffer(Buffer^), FKey, TDESBuffer(OutBuffer^));
end;

class function TElDESSymmetricCrypto.IsAlgorithmSupported(AlgID : integer) : boolean;
begin
  if (AlgID = SB_ALGORITHM_CNT_DES) then
    Result := true
  else
    Result := false;
end;

class function TElDESSymmetricCrypto.IsAlgorithmSupported(const AlgOID : BufferType) : boolean;
begin
  Result := false;
end;

constructor TElDESSymmetricCrypto.Create(AlgID : integer;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  if AlgID = SB_ALGORITHM_CNT_DES then
  begin
    inherited Create(Mode);
    FBlockSize := 8;
    FKeySize := 8;
  end
  else
  begin
    raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmInt, [AlgID]));
  end;
end;

constructor TElDESSymmetricCrypto.Create(const AlgOID : BufferType;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  { no OID's for DES found }
  inherited Create;
  raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmOID, [AlgOID]));
end;


procedure TElDESSymmetricCrypto.InitializeEncryption;
begin
  inherited InitializeEncryption;

  SBDES.ExpandKeyForEncryption(PDESKey(@FKeyMaterial.Key[0])^, FKey);
end;

procedure TElDESSymmetricCrypto.InitializeDecryption;
begin
  inherited InitializeDecryption;

  if FMode in [cmCTR,
    cmCFB8] then
    SBDES.ExpandKeyForEncryption(PDESKey(@FKeyMaterial.Key[0])^, FKey)
  else
    SBDES.ExpandKeyForDecryption(PDESKey(@FKeyMaterial.Key[0])^, FKey);
end;
{$endif}

////////////////////////////////////////////////////////////////////////////////
//  TEl3DESSymmetricCrypto

{$ifndef SBB_NO_DES}
procedure TEl3DESSymmetricCrypto.SetKeyMaterial(Material : TElSymmetricKeyMaterial);
begin
  if not (Length(Material.IV) in [0, 8]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);
  if (Length(Material.Key) <> 24) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  inherited;
end;

procedure TEl3DESSymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer);
begin
  SB3DES.Encrypt(T3DESBuffer(Buffer^), FKey, T3DESBuffer(OutBuffer^));
end;

procedure TEl3DESSymmetricCrypto.DecryptBlock(Buffer, OutBuffer: pointer);
begin
  SB3DES.Decrypt(T3DESBuffer(Buffer^), FKey, T3DESBuffer(OutBuffer^));
end;

class function TEl3DESSymmetricCrypto.IsAlgorithmSupported(AlgID : integer) : boolean;
begin
  if (AlgID = SB_ALGORITHM_CNT_3DES) then
    Result := true
  else
    Result := false;
end;

class function TEl3DESSymmetricCrypto.IsAlgorithmSupported(const AlgOID : BufferType) : boolean;
begin
  Result := false;
end;

constructor TEl3DESSymmetricCrypto.Create(AlgID : integer;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  if AlgID = SB_ALGORITHM_CNT_3DES then
  begin
    inherited Create(Mode);
    FBlockSize := 8;
    FKeySize := 24;
  end
  else
  begin
    raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmInt, [AlgID]));
  end;
end;

constructor TEl3DESSymmetricCrypto.Create(const AlgOID : BufferType;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  { no OID's for DES found }
  inherited Create;
  raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmOID, [AlgOID]));
end;


procedure TEl3DESSymmetricCrypto.InitializeEncryption;
begin
  inherited InitializeEncryption;

  SB3DES.ExpandKeyForEncryption(P3DESKey(@FKeyMaterial.Key[0])^, FKey);
end;

procedure TEl3DESSymmetricCrypto.InitializeDecryption;
begin
  inherited InitializeDecryption;


  if FMode in [cmCTR, cmCFB8] then
  begin
    SB3DES.ExpandKeyForEncryption(P3DESKey(@FKeyMaterial.Key[0])^, FKey);
  end
  else
  begin
    SB3DES.ExpandKeyForDecryption(P3DESKey(@FKeyMaterial.Key[0])^, FKey);
  end  
end;
{$endif}


////////////////////////////////////////////////////////////////////////////////
//  TElCamelliaSymmetricCrypto

{$ifndef SBB_NO_CAMELLIA}

procedure TElCamelliaSymmetricCrypto.SetKeyMaterial(Material : TElSymmetricKeyMaterial);
begin
  if not (Length(Material.IV) in [0, 16]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);
  if not (Length(Material.Key) in [16, 24, 32]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  inherited;
end;

procedure TElCamelliaSymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer);
begin
  if FKeySize = 16 then
    SBCamellia.EncryptBlock(PSBCamelliaBuffer(Buffer)^, PSBCamelliaBuffer(OutBuffer)^, FKey, false)
  else
    SBCamellia.EncryptBlock(PSBCamelliaBuffer(Buffer)^, PSBCamelliaBuffer(OutBuffer)^, FKey, true)
end;

procedure TElCamelliaSymmetricCrypto.DecryptBlock(Buffer, OutBuffer: pointer);
begin
  if FKeySize = 16 then
    SBCamellia.EncryptBlock(PSBCamelliaBuffer(Buffer)^, PSBCamelliaBuffer(OutBuffer)^, FKey, false)
  else
    SBCamellia.EncryptBlock(PSBCamelliaBuffer(Buffer)^, PSBCamelliaBuffer(OutBuffer)^, FKey, true)
end;

class function TElCamelliaSymmetricCrypto.IsAlgorithmSupported(AlgID : integer) : boolean;
begin
  if (AlgID = SB_ALGORITHM_CNT_CAMELLIA)
  then
    Result := true
  else
    Result := false;
end;

class function TElCamelliaSymmetricCrypto.IsAlgorithmSupported(const AlgOID : BufferType) : boolean;
begin
  Result := false;
end;

constructor TElCamelliaSymmetricCrypto.Create(AlgID : integer;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  if AlgID = SB_ALGORITHM_CNT_CAMELLIA then
  begin
    inherited Create(Mode);
    FBlockSize := 16;
  end
  else
  begin
    raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmInt, [AlgID]));
  end;  
end;

constructor TElCamelliaSymmetricCrypto.Create(const AlgOID : BufferType;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  inherited Create;
  raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmOID, [AlgOID]));
end;


procedure TElCamelliaSymmetricCrypto.InitializeEncryption;
begin
  inherited InitializeEncryption;

  SBCamellia.ExpandKeyForEncryption(FKeyMaterial.Key, FKey);
end;

procedure TElCamelliaSymmetricCrypto.InitializeDecryption;
begin
  inherited InitializeDecryption;

  if (FMode = cmCBC) or 
     (FMode = cmECB) then
    SBCamellia.ExpandKeyForDecryption(FKeyMaterial.Key, FKey)
  else
    SBCamellia.ExpandKeyForEncryption(FKeyMaterial.Key, FKey)
end;

{$endif}

////////////////////////////////////////////////////////////////////////////////
//  TElSerpentSymmetricCrypto

procedure TElSerpentSymmetricCrypto.SetKeyMaterial(Material : TElSymmetricKeyMaterial);
begin
  if not (Length(Material.IV) in [0, 16]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);
  if not (Length(Material.Key) in [16, 24, 32]) then
    raise EElSymmetricCryptoError.Create(SInvalidKeyMaterial);

  inherited;
end;

procedure TElSerpentSymmetricCrypto.EncryptBlock(Buffer, OutBuffer : pointer);
begin
  SBSerpent.EncryptBlock(Buffer, OutBuffer, FKey);
end;

procedure TElSerpentSymmetricCrypto.DecryptBlock(Buffer, OutBuffer: pointer);
begin
  SBSerpent.DecryptBlock(Buffer, OutBuffer, FKey);
end;

class function TElSerpentSymmetricCrypto.IsAlgorithmSupported(AlgID : integer) : boolean;
begin
  if (AlgID = SB_ALGORITHM_CNT_SERPENT)
  then
    Result := true
  else
    Result := false;
end;

class function TElSerpentSymmetricCrypto.IsAlgorithmSupported(const AlgOID : BufferType) : boolean;
begin
  Result := false;
end;

constructor TElSerpentSymmetricCrypto.Create(AlgID : integer;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  if AlgID = SB_ALGORITHM_CNT_SERPENT then
  begin
    inherited Create(Mode);
    FBlockSize := 16;
  end
  else
  begin
    raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmInt, [AlgID]));
  end;  
end;

constructor TElSerpentSymmetricCrypto.Create(const AlgOID : BufferType;
  Mode: TSBSymmetricCryptoMode = cmDefault);
begin
  inherited Create;
  raise EElSymmetricCryptoError.Create(Format(SUnsupportedAlgorithmOID, [AlgOID]));
end;


procedure TElSerpentSymmetricCrypto.InitializeEncryption;
begin
  inherited InitializeEncryption;

  SBSerpent.ExpandKey(TSerpentKey(FKeyMaterial.Key), FKey);
end;

procedure TElSerpentSymmetricCrypto.InitializeDecryption;
begin
  inherited InitializeDecryption;

  SBSerpent.ExpandKey(TSerpentKey(FKeyMaterial.Key), FKey)
end;


end.
