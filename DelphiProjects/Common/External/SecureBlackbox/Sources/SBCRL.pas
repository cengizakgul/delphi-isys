(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBCRL;

interface

uses
  Classes,
  SBASN1,
  SBASN1Tree,
  SBUtils,
  SBX509,
  SBPEM,
  SBX509Ext,
  SBRDN,
  SBConstants;


const
  SB_CRL_ERROR_INVALID_FORMAT                   = Integer($2201);
  SB_CRL_ERROR_BAD_SIGNATURE_ALGORITHM          = Integer($2202);
  SB_CRL_ERROR_INVALID_ISSUER                   = Integer($2203);
  SB_CRL_ERROR_INVALID_SIGNATURE                = Integer($2204);
  SB_CRL_ERROR_UNSUPPORTED_VERSION              = Integer($2205);
  SB_CRL_ERROR_UNSUPPORTED_ALGORITHM            = Integer($2206);
  SB_CRL_ERROR_INVALID_CERTIFICATE              = Integer($2207);
  SB_CRL_ERROR_ALREADY_EXISTS                   = Integer($2208);
  SB_CRL_ERROR_NOT_FOUND                        = Integer($2209);
  SB_CRL_ERROR_PRIVATE_KEY_NOT_FOUND            = Integer($220A);
  SB_CRL_ERROR_UNSUPPORTED_CERTIFICATE          = Integer($220B);
  SB_CRL_ERROR_INTERNAL_ERROR                   = Integer($220C);
  SB_CRL_ERROR_BUFFER_TOO_SMALL                 = Integer($220D);


type
  TElCRLExtension = TElCustomExtension;

  TElAuthorityKeyIdentifierCRLExtension = class(TElCRLExtension)
  private
    FKeyIdentifier : BufferType;
    FAuthorityCertIssuer : TElGeneralNames;
    FAuthorityCertSerial : BufferType;
    FSaveIssuer : boolean;
  protected
    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value : BufferType); override;
    procedure SetValue(const Value : BufferType); override;
    function GetValue: BufferType; override;
  public
    constructor Create;
    destructor Destroy; override;
    property KeyIdentifier : BufferType read FKeyIdentifier write FKeyIdentifier;
    property AuthorityCertIssuer : TElGeneralNames read FAuthorityCertIssuer;
    property AuthorityCertSerial : BufferType read FAuthorityCertSerial write FAuthorityCertSerial;
    property IssuerSet : boolean read FSaveIssuer write FSaveIssuer;
  end;

  TElCRLNumberCRLExtension = class(TElCRLExtension)
  private
    FNumber: Integer;
  protected
    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value : BufferType); override;
    procedure SetValue(const Value : BufferType); override;
    function GetValue: BufferType; override;
  public
    property Number: Integer read FNumber write FNumber;
  end;

  TElDeltaCRLIndicatorCRLExtension = class(TElCRLExtension)
  private
    FNumber: Integer;
  protected
    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value : BufferType); override;
    procedure SetValue(const Value : BufferType); override;
    function GetValue: BufferType; override;
  public
    property Number: Integer read FNumber write FNumber;
  end;

  TElReasonCodeCRLExtension = class(TElCRLExtension)
  private
    FReason : TSBCRLReasonFlag;
    FRemoveFromCRL : boolean;
  protected
    procedure Clear; override;
    function GetOID : BufferType; override;
    procedure SetOID(const Value : BufferType); override;
    procedure SetValue(const Value : BufferType); override;
    function GetValue : BufferType; override;
  public
    property Reason : TSBCRLReasonFlag read FReason write FReason;
    property RemoveFromCRL : boolean read FRemoveFromCRL write FRemoveFromCRL;
  end;

  TElInstructionCode =  
    (icNone, icCallIssuer, icReject);
  
  TElHoldInstructionCodeCRLExtension = class(TElCRLExtension)
  private
    FCode : TElInstructionCode;
  protected
    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value : BufferType); override;
    procedure SetValue(const Value : BufferType); override;
    function GetValue: BufferType; override;
  public
    property Code : TElInstructionCode read FCode write FCode;
  end;

  TElInvalidityDateCRLExtension = class(TElCRLExtension)
  private
    FDate : TDateTime;
  protected
    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value : BufferType); override;
    procedure SetValue(const Value : BufferType); override;
    function GetValue: BufferType; override;
  public
    property InvalidityDate : TDateTime read FDate write FDate;
  end;

  TElCertificateIssuerCRLExtension = class(TElCRLExtension)
  private
    FIssuer : TElGeneralNames;
  protected
    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value : BufferType); override;
    procedure SetValue(const Value : BufferType); override;
    function GetValue: BufferType; override;
  public
    constructor Create;
    destructor Destroy; override;
    property Issuer : TElGeneralNames read FIssuer;
  end;

  TSBCRLExtension = (crlAuthorityKeyIdentifier, crlIssuerAlternativeName,
    crlCRLNumber, crlDeltaCRLIndicator);
  TSBCRLExtensions = set of TSBCRLExtension;

  TElCRLExtensions = class
  private
    FAuthorityKeyIdentifier : TElAuthorityKeyIdentifierCRLExtension;
    FIssuerAlternativeName : TElAlternativeNameExtension;
    FCRLNumber : TElCRLNumberCRLExtension;
    FDeltaCRLIndicator : TElDeltaCRLIndicatorCRLExtension;
    FOtherExtensions : TList;
    FIncluded : TSBCRLExtensions;
    function GetOther(Index : integer) : TElCRLExtension;
    function GetOtherCount : integer;
    procedure SetOtherCount(Value : integer);
    procedure ClearList;
    function LoadFromTag(Tag : TElASN1ConstrainedTag) : integer;
    procedure SaveToTag(Tag : TElASN1ConstrainedTag);
    function AddExtension(const OID : BufferType; Critical : boolean; const Value :
        BufferType): Integer;
  public
    constructor Create;
    destructor Destroy; override;
    property AuthorityKeyIdentifier : TElAuthorityKeyIdentifierCRLExtension read
      FAuthorityKeyIdentifier;
    property IssuerAlternativeName : TElAlternativeNameExtension read
      FIssuerAlternativeName;
    property CRLNumber : TElCRLNumberCRLExtension read FCRLNumber;
    property DeltaCRLIndicator : TElDeltaCRLIndicatorCRLExtension read
      FDeltaCRLIndicator;
    property OtherExtensions[Index : integer] : TElCRLExtension read GetOther;
    property OtherCount : integer read GetOtherCount write SetOtherCount;
    property Included : TSBCRLExtensions read FIncluded write FIncluded;
  end;

  TSBCRLEntryExtension = (crlReasonCode, crlHoldInstructionCode, crlInvalidityDate,
    crlCertificateIssuer);
  TSBCRLEntryExtensions = set of TSBCRLEntryExtension;

  TElCRLEntryExtensions = class
  private
    FReasonCode : TElReasonCodeCRLExtension;
    FHoldInstructionCode : TElHoldInstructionCodeCRLExtension;
    FInvalidityDate : TElInvalidityDateCRLExtension;
    FCertificateIssuer : TElCertificateIssuerCRLExtension;
    FOtherExtensions : TList;
    FIncluded : TSBCRLEntryExtensions;
    function GetOther(Index : integer) : TElCRLExtension;
    function GetOtherCount : integer;
    procedure SetOtherCount(Value : integer);
    procedure ClearList;
    function LoadFromTag(Tag : TElASN1ConstrainedTag) : integer;
    procedure SaveToTag(Tag : TElASN1ConstrainedTag);
    function AddExtension(const OID : BufferType; Critical : boolean; const Value :
        BufferType): Integer;
  public
    constructor Create;
    destructor Destroy; override;
    property ReasonCode : TElReasonCodeCRLExtension read FReasonCode;
    property HoldInstructionCode : TElHoldInstructionCodeCRLExtension read
      FHoldInstructionCode;
    property InvalidityDate : TElInvalidityDateCRLExtension read FInvalidityDate;
    property CertificateIssuer : TElCertificateIssuerCRLExtension read
      FCertificateIssuer;
    property OtherExtensions[Index : integer] : TElCRLExtension read GetOther;
    property OtherCount : integer read GetOtherCount write SetOtherCount;
    property Included : TSBCRLEntryExtensions read FIncluded write FIncluded;
  end;

  TElRevocationItem = class
  private
    FSerialNumber : BufferType;
    FRevocationDate : TDateTime;
    FExtensions : TElCRLEntryExtensions;
  public
    constructor Create;
    destructor Destroy; override;

    property SerialNumber : BufferType read FSerialNumber write FSerialNumber;
    property RevocationDate : TDateTime read FRevocationDate write FRevocationDate;
    property Extensions : TElCRLEntryExtensions read FExtensions;
  end;

  TElCertificateRevocationList = class(TSBControlBase)
  private
    FIssuer : TElRelativeDistinguishedName;
    FSignatureAlgCertList : BufferType;
    FSignatureParamsCertList : BufferType;
    FThisUpdate : TDateTime;
    FNextUpdate : TDateTime;
    FVersion : integer;
    FItems : TList;
    FSignature : BufferType;
    FHash : BufferType;
    FSignatureAlgorithm : integer;
    FChanged : boolean;
    FExtensions : TElCRLExtensions;
    function ParseCertList(Tag : TElASN1ConstrainedTag) : integer;
    function ParseRevokedCertificates(Tag : TElASN1ConstrainedTag) : integer;
    procedure SaveCertList(Tag : TElASN1ConstrainedTag);
    procedure SaveRevokedCertificates(Tag : TElASN1ConstrainedTag);
    procedure ClearList;
    function CalculateHash(Buffer : pointer; Size : integer) : integer;
    function GetItems(Index : integer) : TElRevocationItem;
    function GetCount : integer;
  protected
    function SignCertList(Buffer : pointer; Size : integer; Certificate :
      TElX509Certificate) : string;


  public
    constructor Create(Owner : TComponent); override;
    destructor Destroy; override;
    
    function Add(Certificate : TElX509Certificate) : integer;
    function Remove(Certificate : TElX509Certificate) : boolean; overload;
    function Remove(Index : integer) : boolean; overload;
    function IsPresent(Certificate : TElX509Certificate) : boolean;
    function LoadFromBuffer(Buffer : pointer; Size : integer) : integer;
    function SaveToBuffer(Buffer : pointer; var Size : integer; Certificate :
      TElX509Certificate) : integer;
    function LoadFromBufferPEM(Buffer: pointer; Size: integer; const Passphrase:
      string = '') : integer;
    function SaveToBufferPEM(Buffer: pointer; var Size: integer; Certificate:
      TElX509Certificate; const Passphrase: string = ''): integer; overload;
    function SaveToBufferPEM(out Buffer : ByteArray; Certificate : TElX509Certificate;
      const Passphrase : string = '') : integer; overload;
    function LoadFromStream(Stream : TStream; Count : integer = 0) : integer;
    function SaveToStream(Stream : TStream; Certificate : TElX509Certificate) : integer;
    function LoadFromStreamPEM(Stream : TStream; const Passphrase: string = '';
      Count : integer = 0): integer;
    function SaveToStreamPEM(Stream : TStream; Certificate : TElX509Certificate;
      const Passphrase : string = ''): integer;
    function Validate(Certificate : TElX509Certificate) : integer;

    property Issuer : TElRelativeDistinguishedName read FIssuer;
    property ThisUpdate : TDateTime read FThisUpdate write FThisUpdate;
    property NextUpdate : TDateTime read FNextUpdate write FNextUpdate;
    property SignatureAlgorithm : integer read FSignatureAlgorithm;
    property Items[Index : integer] : TElRevocationItem read GetItems;
    property Count : integer read GetCount;
    property Extensions : TElCRLExtensions read FExtensions;
  end;

procedure Register;

implementation

uses
  SysUtils,
  SBPKCS7, SBMD, SBSHA, SBRSA, SBDSA;

const

  SB_OID_EXT_AUTHORITYKEYIDENTIFIER     :TBufferTypeConst =  #$55#$1D#$23;
  SB_OID_EXT_ISSUERALTERNATIVENAME      :TBufferTypeConst =  #$55#$1D#$12;
  SB_OID_EXT_CRLNUMBER                  :TBufferTypeConst =  #$55#$1D#$14;
  SB_OID_EXT_DELTACRLINDICATOR          :TBufferTypeConst =  #$55#$1D#$1B;
  SB_OID_EXT_ISSUINGDISTRIBUTIONPOINT   :TBufferTypeConst =  #$55#$1D#$1C;
  SB_OID_EXT_REASONCODE                 :TBufferTypeConst =  #$55#$1D#$15;
  SB_OID_EXT_HOLDINSTRUCTIONCODE        :TBufferTypeConst =  #$55#$1D#$17;
  SB_OID_EXT_INVALIDITYDATE             :TBufferTypeConst =  #$55#$1D#$18;
  SB_OID_EXT_CERTIFICATEISSUER          :TBufferTypeConst =  #$55#$1D#$1D;

const 

  YEAR2050 : TDateTime = 54789;

procedure Register;
begin
  RegisterComponents('PKIBlackbox', [TElCertificateRevocationList]);
end;
(*
function RotateInteger(Value : BufferType) : BufferType;
var i : integer;
begin
  SetLength(Result, Length(Value));
  for i := 0 to Length(Value) -1 do
  begin
    Result[Length(Value) - i - BufferTypeStartInvOffset] := Value[i + BufferTypeStartOffset];
  end;
end;
*)
////////////////////////////////////////////////////////////////////////////////
// TElCertificateRevocationList implementation

constructor TElCertificateRevocationList.Create(Owner : TComponent);
begin
  inherited Create (Owner);
  FIssuer := TElRelativeDistinguishedName.Create;
  FItems := TList.Create;
  FChanged := false;
  FExtensions := TElCRLExtensions.Create;
end;


destructor TElCertificateRevocationList.Destroy;
begin
  ClearList;
  FreeAndNil(FItems);
  FreeAndNil(FIssuer);
  FreeAndNil(FExtensions);
  inherited;
end;

function TElCertificateRevocationList.LoadFromBuffer(Buffer : pointer; Size : integer) : integer;
var
  Tag, CTag : TElASN1ConstrainedTag;
  Alg, Params : BufferType;
begin
  CheckLicenseKey();
  FVersion := 0;
  ClearList;
  Tag := TElASN1ConstrainedTag.Create;
  try
    Result := SB_CRL_ERROR_INVALID_FORMAT;
    if not Tag.LoadFromBuffer(Buffer, Size) then
    begin
      Exit;
    end;
    if Tag.Count <> 1 then
    begin
      Exit;
    end;
    CTag := TElASN1ConstrainedTag(Tag.GetField(0));
    if (not CTag.IsConstrained) or (CTag.TagId <> SB_ASN1_SEQUENCE) or
      (TElASN1ConstrainedTag(CTag).Count <> 3) then
    begin
      Exit;
    end;
    if (not CTag.GetField(0).IsConstrained) then
    begin
      Exit;
    end;
    Result := ParseCertList(TElASN1ConstrainedTag(CTag.GetField(0)));
    if Result <> 0 then
    begin
      Exit;
    end;
    Result := ProcessAlgorithmIdentifier(CTag.GetField(1), Alg, Params {$ifndef HAS_DEF_PARAMS}, False{$endif});
    if Result <> 0 then
    begin
      Exit;
    end;
    if (CompareStr(Alg, FSignatureAlgCertList) <> 0) or
      (CompareStr(Params, FSignatureParamsCertList) <> 0) then
    begin
      Result := SB_CRL_ERROR_BAD_SIGNATURE_ALGORITHM;
      Exit;
    end;
    if (CTag.GetField(2).IsConstrained) or (CTag.GetField(2).TagID <> SB_ASN1_BITSTRING) then
    begin
      Result := SB_CRL_ERROR_INVALID_FORMAT;
      Exit;
    end;
    FSignature := TElASN1SimpleTag(CTag.GetField(2)).Content;
    FChanged := false;
    Result := CalculateHash(Buffer, Size);
  finally
    Tag.Free;
  end;
end;


function TElCertificateRevocationList.SaveToBuffer(Buffer : pointer; var Size :
  integer; Certificate : TElX509Certificate) : integer;
var
  I, SigAlg : integer;
  Tag, CTag, TagCertList : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  Buf : ByteArray;
  Sz : integer;
begin
  CheckLicenseKey();
  if not Certificate.PrivateKeyExists then
  begin
    Result := SB_CRL_ERROR_PRIVATE_KEY_NOT_FOUND;
    Exit;
  end;
  if Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
    SigAlg := SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION
  else
  if Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
    SigAlg := SB_CERT_ALGORITHM_ID_DSA_SHA1
  else
  begin
    Result := SB_CRL_ERROR_UNSUPPORTED_CERTIFICATE;
    Exit;
  end;
  FSignatureAlgCertList := GetOIDBySigAlgorithm(SigAlg);
  FIssuer.Count := Certificate.SubjectRDN.Count;
  for I := 0 to Certificate.SubjectRDN.Count - 1 do
  begin
    FIssuer.OIDs[I] := CloneBuffer(Certificate.SubjectRDN.OIDs[I]);
    FIssuer.Values[I] := CloneBuffer(Certificate.SubjectRDN.Values[I]);
    FIssuer.Tags[I] := Certificate.SubjectRDN.Tags[I];
  end;
  Tag := TElASN1ConstrainedTag.Create;
  Tag.TagId := SB_ASN1_SEQUENCE;
  { Certificate List }
  TagCertList := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveCertList(TagCertList);
  { Signature Algorithm }
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  SaveAlgorithmIdentifier(CTag, FSignatureAlgCertList, EmptyBuffer {$ifndef HAS_DEF_PARAMS}, 0{$endif});
  { Signature }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_BITSTRING;
  Sz := 0;
  TagCertList.SaveToBuffer(nil, Sz);
  SetLength(Buf, Sz);
  TagCertList.SaveToBuffer(@Buf[0], Sz);
  STag.Content := TBufferTypeConst(#0) + TBufferTypeConst(
    SignCertList(@Buf[0], Sz, Certificate));
  if Length(STag.Content) = 0 then
    Result := SB_CRL_ERROR_INTERNAL_ERROR
  else
  begin
    Tag.SaveToBuffer(nil, Sz);
    if (Sz > Size)  then
    begin
      Size := Sz + 32;
      Result := SB_CRL_ERROR_BUFFER_TOO_SMALL;
    end
    else
    begin
      Tag.SaveToBuffer(Buffer, Sz);
      Size := Sz;
      Result := 0;
    end;
  end;
end;

procedure TElCertificateRevocationList.SaveCertList(Tag : TElASN1ConstrainedTag);
var
  STag : TElASN1SimpleTag;
  CTag : TElASN1ConstrainedTag;
begin
  Tag.TagID := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagID := SB_ASN1_INTEGER;
  STag.Content := #1;
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTag.TagID := SB_ASN1_SEQUENCE;
  SaveAlgorithmIdentifier(CTag, FSignatureAlgCertList, EmptyBuffer {$ifndef HAS_DEF_PARAMS}, 0{$endif});
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  FIssuer.SaveToTag(CTag);
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  if FThisUpdate < YEAR2050 then
  begin
    STag.TagID := SB_ASN1_UTCTIME;
    STag.Content := TBufferTypeConst(DateTimeToUTCTime(FThisUpdate));
  end
  else
  begin
    STag.TagID := SB_ASN1_GENERALIZEDTIME;
    STag.Content := TBufferTypeConst(DateTimeToGeneralizedTime(FThisUpdate));
  end;
  if FNextUpdate <> 0 then
  begin
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    if FNextUpdate < YEAR2050 then
    begin
      STag.TagID := SB_ASN1_UTCTIME;
      STag.Content := TBufferTypeConst(DateTimeToUTCTime(FNextUpdate));
    end
    else
    begin
      STag.TagID := SB_ASN1_GENERALIZEDTIME;
      STag.Content := TBufferTypeConst(DateTimeToGeneralizedTime(FNextUpdate));
    end;
  end;
  if FItems.Count > 0 then
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    SaveRevokedCertificates(CTag);
  end;
  if (FExtensions.Included <> []) or (FExtensions.OtherCount > 0) then
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    CTag.TagID := $A0;
    CTag := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
    FExtensions.SaveToTag(CTag);
  end;
end;

procedure TElCertificateRevocationList.SaveRevokedCertificates(Tag : TElASN1ConstrainedTag);
var
  I : integer;
  CTag, ExtTag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
begin
  Tag.TagID := SB_ASN1_SEQUENCE;
  for I := 0 to FItems.Count - 1 do
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    CTag.TagID := SB_ASN1_SEQUENCE;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    STag.TagID := SB_ASN1_INTEGER;
    STag.Content := TElRevocationItem(FItems[I]).SerialNumber;
    STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
    if TElRevocationItem(FItems[I]).FRevocationDate < YEAR2050 then
    begin
      STag.TagID := SB_ASN1_UTCTIME;
      STag.Content := TBufferTypeConst(DateTimeToUTCTime(TElRevocationItem(FItems[I]).RevocationDate));
    end
    else
    begin
      STag.TagID := SB_ASN1_GENERALIZEDTIME;
      STag.Content := TBufferTypeConst(DateTimeToGeneralizedTime(TElRevocationItem(FItems[I]).RevocationDate));
    end;
    if (TElRevocationItem(FItems[I]).Extensions.Included <> []) or
      (TElRevocationItem(FItems[I]).Extensions.OtherCount > 0) then
    begin
      ExtTag := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
      TElRevocationItem(FItems[I]).Extensions.SaveToTag(ExtTag);
    end;
  end;
end;       

procedure TElCertificateRevocationList.ClearList;
begin
  while FItems.Count > 0 do
  begin
    TElRevocationItem(FItems[FItems.Count - 1]).Free;
    FItems.Count := FItems.Count - 1;
  end;
end;

function TElCertificateRevocationList.ParseCertList(Tag : TElASN1ConstrainedTag) :
  integer;
var
  Index : integer;
  Alg, Params : BufferType;
begin
  Result := SB_CRL_ERROR_INVALID_FORMAT;
  if (Tag.TagID <> SB_ASN1_SEQUENCE) then
    Exit;
  if Tag.Count < 1 then
    Exit;
  Index := 0;
  if not Tag.GetField(Index).IsConstrained then
  begin
    if (Tag.GetField(Index).TagId <> SB_ASN1_INTEGER) or
      ((TElASN1SimpleTag(Tag.GetField(Index)).Content <> #2) and
      (TElASN1SimpleTag(Tag.GetField(Index)).Content <> #1)) then
      Exit
    else
      FVersion := 2;
    Inc(Index);
  end;
  Result := ProcessAlgorithmIdentifier(Tag.GetField(Index), Alg, Params {$ifndef HAS_DEF_PARAMS}, False{$endif});
  if Result <> 0 then
    Exit;
  Result := SB_CRL_ERROR_INVALID_FORMAT;
  FSignatureAlgCertList := Alg;
  FSignatureParamsCertList := Params;
  Inc(Index);
  if Index >= Tag.Count then
    Exit;
  if (not Tag.GetField(Index).IsConstrained) then
    Exit;
  if not FIssuer.LoadFromTag(TElASN1ConstrainedTag(Tag.GetField(Index)){$ifndef HAS_DEF_PARAMS}, false{$endif}) then
    Exit;
  Inc(Index);
  if Index >= Tag.Count then
    Exit;
  if (Tag.GetField(Index).IsConstrained) then
    Exit;
  if (Tag.GetField(Index).TagId = SB_ASN1_UTCTIME) then
    FThisUpdate := UTCTimeToDateTime((TElASN1SimpleTag(Tag.GetField(Index)).Content))
  else
  if (Tag.GetField(Index).TagId = SB_ASN1_GENERALIZEDTIME) then
    FThisUpdate := GeneralizedTimeToDateTime((TElASN1SimpleTag(Tag.GetField(Index)).Content))
  else
    Exit;
  Inc(Index);
  Result := 0;
  if Index >= Tag.Count then
  begin
    FNextUpdate := 0;
    Exit;
  end;
  if (not Tag.GetField(Index).IsConstrained) then
  begin
    if (Tag.GetField(Index).TagId = SB_ASN1_UTCTIME) then
      FNextUpdate := UTCTimeToDateTime((TElASN1SimpleTag(Tag.GetField(Index)).Content))
    else
    if (Tag.GetField(Index).TagId = SB_ASN1_GENERALIZEDTIME) then
      FNextUpdate := GeneralizedTimeToDateTime((TElASN1SimpleTag(Tag.GetField(Index)).Content))
    else
      Exit;
    Inc(Index);
  end;
  if Index >= Tag.Count then
    Exit;
  if (Tag.GetField(Index).IsConstrained) and (Tag.GetField(Index).TagID = SB_ASN1_SEQUENCE) then
  begin
    Result := ParseRevokedCertificates(TElASN1ConstrainedTag(Tag.GetField(Index)));
    Inc(Index);
  end;
  if Index >= Tag.Count then
    Exit;
  if (Tag.GetField(Index).IsConstrained) and (Tag.GetField(Index).TagID = SB_ASN1_A0) then
  begin
    if FVersion <> 2 then
    begin
      Result := SB_CRL_ERROR_UNSUPPORTED_VERSION;
      Exit;
    end;
    FExtensions.LoadFromTag(TElASN1ConstrainedTag(Tag.GetField(Index)));
  end
  else
    Exit;
  Result := 0;
end;

function TElCertificateRevocationList.ParseRevokedCertificates(Tag :
  TElASN1ConstrainedTag) : integer;
var
  I : integer;
  TagSeq : TElASN1ConstrainedTag;
  Serial : BufferType;
  RevDate : TDateTime;
  Item : TElRevocationItem;
begin
  RevDate := 0;
  Result := SB_CRL_ERROR_INVALID_FORMAT;
  if (Tag.TagID <> SB_ASN1_SEQUENCE) then
    Exit;
  for I := 0 to Tag.Count - 1 do
  begin
    if (not Tag.GetField(I).IsConstrained) or (Tag.GetField(I).TagID <>
      SB_ASN1_SEQUENCE) then
      Exit;
    TagSeq := TElASN1ConstrainedTag(Tag.GetField(I));
    if TagSeq.Count < 2 then
      Exit;
    if (TagSeq.GetField(0).IsConstrained) or (TagSeq.GetField(0).TagID <>
      SB_ASN1_INTEGER) then
      Exit;
    Serial := TElASN1SimpleTag(TagSeq.GetField(0)).Content;
    if (not TagSeq.GetField(1).IsConstrained) and (TagSeq.GetField(1).TagID = SB_ASN1_UTCTIME) then
      RevDate := UTCTimeToDateTime((TElASN1SimpleTag(TagSeq.GetField(1)).Content))
    else
    if (not TagSeq.GetField(1).IsConstrained) and (TagSeq.GetField(1).TagID = SB_ASN1_GENERALIZEDTIME) then
      RevDate := GeneralizedTimeToDateTime((TElASN1SimpleTag(TagSeq.GetField(1)).Content))
    else
    begin
      Exit;
    end;
    
    Item := TElRevocationItem.Create;
    if TagSeq.Count = 3 then
    begin
      if FVersion <> 2 then
      begin
        Result := SB_CRL_ERROR_UNSUPPORTED_VERSION;
        Item.Free;
        Exit;
      end;
      Result := Item.Extensions.LoadFromTag(TElASN1ConstrainedTag(TagSeq.GetField(2)));
    end;
    
    Item.SerialNumber := Serial;
    Item.RevocationDate := RevDate;
    FItems.Add(Item);
  end;
  
  Result := 0;
end;

function TElCertificateRevocationList.Validate(Certificate : TElX509Certificate) :
  integer;
var
  I : integer;
  Lst : TStringList;
  N1, N2, N3, N4 : ByteArray;
  S1, S2, S3, S4 : integer;
  R, S : BufferType;
  Tag : TElASN1ConstrainedTag;
begin
  CheckLicenseKey();
  if (Length(FHash) = 0) or (FSignatureAlgorithm = SB_ALGORITHM_UNKNOWN) then
  begin
    Result := SB_CRL_ERROR_UNSUPPORTED_ALGORITHM;
    Exit;
  end;
  if not Assigned(Certificate) then
  begin
    Result := SB_CRL_ERROR_INVALID_CERTIFICATE;
    Exit;
  end;
  
  Lst := TStringList.Create;
  Result := 0;
  for I := 0 to FIssuer.Count - 1 do
  begin
    Certificate.SubjectRDN.GetValuesByOID(FIssuer.OIDs[I], Lst);
    if Lst.IndexOf(FIssuer.Values[I]) < 0 then
    begin
      Result := SB_CRL_ERROR_INVALID_ISSUER;
      Break;
    end;
  end;
  Lst.Free;
  
  if Result <> 0 then
    Exit;
    
  case FSignatureAlgorithm of
    SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION:
    begin
      if Certificate.PublicKeyAlgorithm <> SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
      begin
        Result := SB_CRL_ERROR_INVALID_CERTIFICATE;
        Exit;
      end;
      S1 := 0;
      Certificate.GetRSAParams(nil, S1, nil, S2);
      SetLength(N1, S1);
      SetLength(N2, S2);
      Certificate.GetRSAParams(@N1[0], S1, @N2[0], S2);
      if N1[0] = 0 then
      begin
        N1 := Copy(N1, 1, Length(N1));
        Dec(S1);
      end;
      if SBRSA.ValidateSignature(@FHash[1], Length(FHash), @N1[0], S1, @N2[0], S2,
        @FSignature[1], Length(FSignature)) then
        Result := 0
      else
        Result := SB_CRL_ERROR_INVALID_SIGNATURE;
    end;
    SB_CERT_ALGORITHM_ID_DSA_SHA1:
    begin
      if Certificate.PublicKeyAlgorithm <> SB_CERT_ALGORITHM_ID_DSA then
      begin
        Result := SB_CRL_ERROR_INVALID_CERTIFICATE;
        Exit;
      end;
      S1 := 0;
      Certificate.GetDSSParams(nil, S1, nil, S2, nil, S3, nil, S4);
      SetLength(N1, S1);
      SetLength(N2, S2);
      SetLength(N3, S3);
      SetLength(N4, S4);
      Certificate.GetDSSParams(@N1[0], S1, @N2[0], S2, @N3[0], S3, @N4[0], S4);
      Tag := TElASN1ConstrainedTag.Create;
      if FSignature[1] = #0 then
        I := 2
      else
        I := 1;
      
      if not Tag.LoadFromBuffer(@FSignature[I], Length(FSignature) - I + 1) then
      begin
        Tag.Free;
        Result := SB_CRL_ERROR_INVALID_SIGNATURE;
        Exit;
      end;
      
      if (Tag.Count <> 1) or (not Tag.GetField(0).IsConstrained) or
        (TElASN1ConstrainedTag(Tag.GetField(0)).GetField(0).IsConstrained) or
        (TElASN1ConstrainedTag(Tag.GetField(0)).GetField(1).IsConstrained) then
      begin
        Tag.Free;
        Result := SB_CRL_ERROR_INVALID_SIGNATURE;
        Exit;
      end;
      
      R := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(0)).Content;
      S := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(1)).Content;
      Tag.Free;
      
      if SBDSA.ValidateSignature(@FHash[1], Length(FHash), @N1[0], S1, @N2[0], S2,
        @N3[0], S3, @N4[0], S4, @R[1], Length(R), @S[1], Length(S)) then
        Result := 0
      else
        Result := SB_CRL_ERROR_INVALID_SIGNATURE;
    end;
    else
      Result := SB_CRL_ERROR_UNSUPPORTED_ALGORITHM;
  end;
end;

function TElCertificateRevocationList.CalculateHash(Buffer : pointer; Size :
  integer) : integer;
var
  TBSBlockLen : integer;
  M128 : TMessageDigest128;
  M160 : TMessageDigest160;
  Offset : integer;
  sv     : integer;
  TmpBuf : Pointer;
begin
  FSignatureAlgorithm := GetSigAlgorithmByOID(FSignatureAlgCertList);
  Result := 0;
  sv := PByteArray(Buffer)[1];
  if sv <= $80 then
    Offset := 2
  else
  if sv = $81 then
    Offset := 3
  else
  if sv = $82 then
    Offset := 4
  else
  if sv = $83 then
    Offset := 5
  else
  if sv = $84 then
    Offset := 6
  else
    Offset := 2;

  Dec(Size, Offset);
  sv := PByteArray(Buffer)[Offset + 1];
  if sv < $80 then
  begin
    TBSBlockLen := PByteArray(Buffer)[offset + 1] + 2
  end
  else
  if sv = $81 then
  begin
    TBSBlockLen := PByteArray(Buffer)[offset + 2] + 3
  end
  else
  if sv = $82 then
  begin
    TBSBlockLen := (PByteArray(Buffer)[offset + 2] shl 8) or PByteArray(Buffer)[offset + 3] + 4
  end
  else
  if sv = $83 then
  begin
    TBSBlockLen := (PByteArray(Buffer)[offset + 2] shl 16) or (PByteArray(Buffer)[offset + 3] shl 8) or
      PByteArray(Buffer)[offset + 4] + 5
  end
  else
  if sv = $84 then
  begin
    TBSBlockLen := (PByteArray(Buffer)[offset + 2] shl 24) or (PByteArray(Buffer)[offset + 3] shl 16) or
      (PByteArray(Buffer)[offset + 4] shl 8) or PByteArray(Buffer)[offset + 5] + 6
  end
  else
    TBSBlockLen := 0;

  (*
  Buffer := @PByteArray(Buffer)[Offset];
  Dec(Size, Offset);
  if PByteArray(Buffer)[1] < $80 then
    TBSBlockLen := PByteArray(Buffer)[1] + 2
  else
  if PByteArray(Buffer)[1] = $81 then
    TBSBlockLen := PByteArray(Buffer)[2] + 3
  else
  if PByteArray(Buffer)[1] = $82 then
    TBSBlockLen := (PByteArray(Buffer)[2] shl 8) or PByteArray(Buffer)[3] + 4
  else
  if PByteArray(Buffer)[1] = $83 then
    TBSBlockLen := (PByteArray(Buffer)[2] shl 16) or (PByteArray(Buffer)[3] shl 8) or
      PByteArray(Buffer)[4] + 5
  else
  if PByteArray(Buffer)[1] = $84 then
    TBSBlockLen := (PByteArray(Buffer)[2] shl 24) or (PByteArray(Buffer)[3] shl 16) or
      (PByteArray(Buffer)[4] shl 8) or PByteArray(Buffer)[5] + 6
  else
    TBSBlockLen := 0;
  *)
  if TBSBlockLen > Size then
    TBSBlockLen := Size;

  TmpBuf := @PByteArray(Buffer)[Offset];
  case FSignatureAlgorithm of
    SB_CERT_ALGORITHM_MD2_RSA_ENCRYPTION:
    begin
      M128 := HashMD2(TmpBuf, TBSBlockLen);
      SetLength(FHash, 16);
      Move(M128, FHash[1], 16);
    end;
    SB_CERT_ALGORITHM_MD5_RSA_ENCRYPTION:
    begin
      M128 := HashMD5(TmpBuf, TBSBlockLen);
      SetLength(FHash, 16);
      Move(M128, FHash[1], 16);
    end;
    SB_CERT_ALGORITHM_SHA1_RSA_ENCRYPTION,
    SB_CERT_ALGORITHM_ID_DSA_SHA1:
    begin
      M160 := HashSHA1(TmpBuf, TBSBlockLen);
      SetLength(FHash, 20);
      Move(M160, FHash[1], 20);
    end;
    else
      SetLength(FHash, 0);
  end;
end;

function TElCertificateRevocationList.IsPresent(Certificate : TElX509Certificate) :
  boolean;
var
  Lst : TStringList;
  I : integer;
begin
  CheckLicenseKey();
  Result := false;
  Lst := TStringList.Create;
  for I := 0 to FIssuer.Count - 1 do
  begin
    Certificate.IssuerRDN.GetValuesByOID(FIssuer.OIDs[I], Lst);
    if Lst.IndexOf(FIssuer.Values[I]) < 0 then
    begin
      Lst.Free;
      Exit;
    end;
  end;
  Lst.Free;
  
  for I := 0 to FItems.Count - 1 do
    if CompareContent(TElRevocationItem(FItems[I]).FSerialNumber, Certificate.SerialNumber) then
    begin
      Result := true;
      Break;
    end;
end;

function TElCertificateRevocationList.Add(Certificate : TElX509Certificate) : integer;
var
  Lst : TStringList;
  I : integer;
  Item : TElRevocationItem;
begin
  CheckLicenseKey();
  Lst := TStringList.Create;
  for I := 0 to FIssuer.Count - 1 do
  begin
    Certificate.IssuerRDN.GetValuesByOID(FIssuer.OIDs[I], Lst);
    if Lst.IndexOf(FIssuer.Values[I]) < 0 then
    begin
      Result := -1;
      Lst.Free;
      Exit;
    end;
  end;
  Lst.Free;

  for I := 0 to FItems.Count - 1 do
  begin
    if CompareContent(TElRevocationItem(FItems[I]).FSerialNumber,
      Certificate.SerialNumber) then
    begin
      Result := -1;
      Exit;
    end;
  end;
  Item := TElRevocationItem.Create;
  Item.FSerialNumber := CloneBuffer(Certificate.SerialNumber);
  Item.FRevocationDate := Now;
  FChanged := true;
  Result := FItems.Add(Item);
end;

function TElCertificateRevocationList.Remove(Certificate : TElX509Certificate) : boolean;
var
  Lst : TStringList;
  I : integer;
  // Item : TElRevocationItem;
begin
  CheckLicenseKey();
  Lst := TStringList.Create;
  for I := 0 to FIssuer.Count - 1 do
  begin
    Certificate.IssuerRDN.GetValuesByOID(FIssuer.OIDs[I], Lst);
    if Lst.IndexOf(FIssuer.Values[I]) < 0 then
    if Lst.IndexOf(FIssuer.Values[I]) < 0 then
    begin
      Result := false;
      Lst.Free;
      Exit;
    end;
  end;
  Lst.Free;
  
  for I := 0 to FItems.Count - 1 do
  begin
    if CompareContent(TElRevocationItem(FItems[I]).FSerialNumber,
      Certificate.SerialNumber) then
    begin
      TElRevocationItem(FItems[I]).Free;
      FItems.Delete(I);
      Result := true;
      Exit;
    end;
  end;
  FChanged := true;
  Result := false;
end;

function TElCertificateRevocationList.Remove(Index : integer) : boolean;
begin
  if (Index >= 0) and (Index < FItems.Count) then
  begin
    TElRevocationItem(FItems[Index]).Free;
    FItems.Delete(Index);
    Result := true;
  end
  else
    Result := false;
end;

function TElCertificateRevocationList.GetItems(Index : integer) : TElRevocationItem;
begin
  Result := TElRevocationItem(FItems[Index]);
end;

function TElCertificateRevocationList.GetCount : integer;
begin
  Result := FItems.Count;
end;

function TElCertificateRevocationList.SignCertList(Buffer : pointer; Size : integer;
  Certificate : TElX509Certificate) : string;
var
  M160 : TMessageDigest160;
  BufM, BufE, BufD, BufS, BufG, BufX, BufR : ByteArray;
  SzM, SzE, SzD, SzS, SzG, SzX, SzR, Len, Index : integer;
  Tag, TagAI : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
begin
  if Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
  begin
    M160 := HashSHA1(Buffer, Size);
    SzM := 0;
    SzE := 0;
    Certificate.GetRSAParams(nil, SzM, nil, SzE);
    SetLength(BufM, SzM);
    SetLength(BufE, SzE);
    Certificate.GetRSAParams(@BufM[0], SzM, @BufE[0], SzE);
    SzD := 0;
    Certificate.SaveKeyValueToBuffer(nil, SzD);
    SetLength(BufD, SzD);
    Certificate.SaveKeyValueToBuffer(@BufD[0], SzD);
    Tag := TElASN1ConstrainedTag.Create;
    Tag.TagID := SB_ASN1_SEQUENCE;
    TagAI := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    SaveAlgorithmIdentifier(TagAI, SB_OID_SHA1_RSAENCRYPTION, EmptyBuffer {$ifndef HAS_DEF_PARAMS}, 0{$endif});
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagID := SB_ASN1_OCTETSTRING;
    SetLength(Result, 20);
    Move(M160, Result[1], 20);
    STag.Content := CloneBuffer(Result);
    SzS := 0;
    Tag.SaveToBuffer(nil, SzS);
    SetLength(BufS, SzS);
    Tag.SaveToBuffer(@BufS[0], SzS);
    Tag.Free;
    SetLength(BufS, SzS);
    SzS := SzM + 1;
    SetLength(Result, SzS);
    SBRSA.Sign(@BufS[0], Length(BufS), @BufM[0], SzM, @BufD[0], SzD, @Result[1],
      SzS);
    SetLength(Result, SzS);
  end
  else
  if Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    M160 := HashSHA1(Buffer, Size);
    SzM := 0;
    Certificate.GetDSSParams(nil, SzM, nil, SzE, nil, SzG, nil, SzD);
    SetLength(BufM, SzM);
    SetLength(BufE, SzE);
    SetLength(BufG, SzG);
    SetLength(BufD, SzD);
    Certificate.GetDSSParams(@BufM[0], SzM, @BufE[0], SzE, @BufG[0], SzG,
      @BufD[0], SzD);
    SzX := 0;
    Certificate.SaveKeyValueToBuffer(nil, SzX);
    SetLength(BufX, SzX);
    Certificate.SaveKeyValueToBuffer(@BufX[0], SzX);
    SzR := 21;
    SzS := 21;
    SetLength(BufR, SzR);
    SetLength(BufS, SzS);
    SBDSA.Sign(@M160, 20, @BufM[0], SzM, @BufE[0], SzE, @BufG[0], SzG, @BufX[0],
      SzX, @BufR[0], SzR, @BufS[0], SzS);
    Len := SzR + SzS + 6;
    if (BufR[0] >= 128) then
      Inc(Len);
    if (BufS[0] >= 128) then
      Inc(Len);
    SetLength(Result, Len);
    Result[1] := #$30;
    Result[2] := Chr(Len - 2);
    Result[3] := #$02;
    if BufR[0] >= 128 then
    begin
      Result[4] := Chr(SzR + 1);
      Result[5] := #0;
      Index := 6;
    end
    else
    begin
      Result[4] := Chr(SzR);
      Index := 5;
    end;
    Move(BufR[0], Result[Index], SzR);
    Inc(Index, SzR);
    Result[Index] := #$02;
    Inc(Index);
    if BufS[0] >= 128 then
    begin
      Result[Index] := Chr(SzS + 1);
      Result[Index + 1] := #0;
      Inc(Index, 2);
    end
    else
    begin
      Result[Index] := Chr(SzS);
      Inc(Index);
    end;
    Move(BufS[0], Result[Index], SzS);
    Inc(Index, SzS - 1);
    SetLength(Result, Index);
  end;
end;

function TElCertificateRevocationList.LoadFromStream(Stream : TStream; Count : integer = 0) : integer;
var
  Buf : ByteArray;
  Size : integer;
begin
  if Count = 0 then
    Count := Stream.Size - Stream.Position
  else
    Count := Min(Count, Stream.Size - Stream.Position);
  SetLength(Buf, Count);
  Size := Stream.Read(Buf[0], Count);
  Result := LoadFromBuffer(@Buf[0], Size);
end;

function TElCertificateRevocationList.SaveToStream(Stream : TStream; Certificate:
  TElX509Certificate) : integer;
var
  Buf : ByteArray;
  Size : integer;
begin
  Size := 0;
  result := SaveToBuffer(nil, Size, Certificate);
  if (result <> SB_CRL_ERROR_BUFFER_TOO_SMALL) then
    exit;
  SetLength(Buf, Size);
  Result := SaveToBuffer(@Buf[0], Size, Certificate);
  if Result = 0 then
    Stream.Write(Buf[0], Size);
end;


function TElCertificateRevocationList.LoadFromBufferPEM(Buffer: pointer;
  Size: integer; const Passphrase: string = '') : integer;
var
  Header : string;
  OutSize : integer;
  Buf : ByteArray;
  PemResult : integer;
begin
  OutSize := 0;
  SBPEM.Decode(Buffer, Size, nil, Passphrase, OutSize, Header);
  SetLength(Buf, OutSize);
  PemResult := SBPEM.Decode(Buffer, Size, @Buf[0], Passphrase, OutSize, Header);
  if PemResult <> 0 then
  begin
    Result := PemResult;
    Exit;
  end;
  Result := LoadFromBuffer(@Buf[0], OutSize);
end;

function TElCertificateRevocationList.SaveToBufferPEM(out Buffer : ByteArray;
  Certificate : TElX509Certificate; const Passphrase : string) : integer;
var Sz : integer;
begin
  Sz := -1;
  Result := SaveToBufferPEM(Buffer, Sz, Certificate, Passphrase);
end;

function TElCertificateRevocationList.SaveToBufferPEM(Buffer: pointer; var Size: integer;
  Certificate: TElX509Certificate; const Passphrase: string = ''): integer;
var
  Buf : ByteArray;
  Sz : integer;
begin
  Sz := 0;
  SaveToBuffer(nil, Sz, Certificate);
  SetLength(Buf, Sz);
  Result := SaveToBuffer(@Buf[0], Sz, Certificate);
  if Result <> 0 then Exit;
  if not SBPEM.Encode(@Buf[0], Sz, Buffer, Size, 'X509 CRL',
    false, Passphrase) then
    Result := SB_CRL_ERROR_INTERNAL_ERROR;
end;

function TElCertificateRevocationList.LoadFromStreamPEM(Stream : TStream;
  const Passphrase: string = ''; Count : integer = 0): integer;
var
  Buf : ByteArray;
begin
  if Count = 0 then
    Count := Stream.Size - Stream.Position;
  SetLength(Buf, Count);
  Stream.ReadBuffer(Buf[0], Length(Buf));
  Result := LoadFromBufferPEM(@Buf[0], Length(Buf));
end;

function TElCertificateRevocationList.SaveToStreamPEM(Stream : TStream;
  Certificate : TElX509Certificate; const Passphrase : string = ''): integer;
var
  Buf : ByteArray;
  Size : integer;
begin
  Size := 0;
  SaveToBufferPEM(nil, Size, Certificate,
    Passphrase);
  SetLength(Buf, Size);
  Result := SaveToBufferPEM(@Buf[0], Size,
    Certificate, Passphrase);
  if Result <> 0 then Exit;
  Stream.WriteBuffer(Buf[0], Size);
end;

////////////////////////////////////////////////////////////////////////////////
// TElRevocationItem implementation

constructor TElRevocationItem.Create;
begin
  inherited;
  FExtensions := TElCRLEntryExtensions.Create;
end;

destructor TElRevocationItem.Destroy;
begin
  FreeAndNil(FExtensions);
  inherited;
end;

////////////////////////////////////////////////////////////////////////////////
// TElCRLExtensions implementation

constructor TElCRLExtensions.Create;
begin
  inherited;
  FOtherExtensions := TList.Create;
  FIssuerAlternativeName := TElAlternativeNameExtension.Create;
  FAuthorityKeyIdentifier := TElAuthorityKeyIdentifierCRLExtension.Create;
  FCRLNumber := TElCRLNumberCRLExtension.Create;
  FDeltaCRLIndicator := TElDeltaCRLIndicatorCRLExtension.Create;
end;

destructor TElCRLExtensions.Destroy;
begin
  ClearList;
  FreeAndNil(FIssuerAlternativeName);
  FreeAndNil(FAuthorityKeyIdentifier);
  FreeAndNil(FCRLNumber);
  FreeAndNil(FDeltaCRLIndicator);
  FreeAndNil(FOtherExtensions);
  inherited;
end;

function TElCRLExtensions.LoadFromTag(Tag : TElASN1ConstrainedTag) : integer;
var
  I, Index : integer;
  CTag : TElASN1ConstrainedTag;
  ExtOID : BufferType;
  ExtCritical : boolean;
begin
  Result := SB_CRL_ERROR_INVALID_FORMAT;
  if Tag.TagId <> SB_ASN1_A0 then
    Exit;
  if Tag.Count <> 1 then
    Exit;
  Tag := TElASN1ConstrainedTag(Tag.GetField(0));
  for I := 0 to Tag.Count - 1 do
  begin
    if (not Tag.GetField(I).IsConstrained) or (Tag.GetField(I).TagId <> SB_ASN1_SEQUENCE) then
      Exit;
    CTag := TElASN1ConstrainedTag(Tag.GetField(I));
    if CTag.Count < 2 then
      Exit;
    if (CTag.GetField(0).IsConstrained) or (CTag.GetField(0).TagId <> SB_ASN1_OBJECT) then
      Exit;
    ExtOID := TElASN1SimpleTag(CTag.GetField(0)).Content;
    if CTag.Count = 3 then
    begin
      if (CTag.GetField(1).IsConstrained) or (CTag.GetField(1).TagID <> SB_ASN1_BOOLEAN) then
        Exit;
      ExtCritical := TElASN1SimpleTag(CTag.GetField(1)).Content <> #0;
      Index := 2;
    end
    else
    begin
      ExtCritical := false;
      Index := 1;
    end;
    if (CTag.GetField(Index).IsConstrained) or (CTag.GetField(Index).TagID <> SB_ASN1_OCTETSTRING) then
      Exit;
    AddExtension(ExtOID, ExtCritical, TElASN1SimpleTag(CTag.GetField(Index)).Content);
  end;
  Result := 0;
end;

procedure TElCRLExtensions.SaveToTag(Tag : TElASN1ConstrainedTag);
var
  TagSeq : TElASN1ConstrainedTag;
  I : integer;
begin
  Tag.TagID := SB_ASN1_SEQUENCE;
  if crlAuthorityKeyIdentifier in FIncluded then
  begin
    TagSeq := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    FAuthorityKeyIdentifier.SaveToTag(TagSeq);
  end;
  if crlIssuerAlternativeName in FIncluded then
  begin
    TagSeq := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    FIssuerAlternativeName.SaveToTag(TagSeq);
  end;
  if crlCRLNumber in FIncluded then
  begin
    TagSeq := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    FCRLNumber.SaveToTag(TagSeq);
  end;
  if crlDeltaCRLIndicator in FIncluded then
  begin
    TagSeq := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    FDeltaCRLIndicator.SaveToTag(TagSeq);
  end;
  for I := 0 to OtherCount - 1 do
  begin
    TagSeq := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    OtherExtensions[I].SaveToTag(TagSeq);
  end;
end;

function TElCRLExtensions.GetOther(Index : integer) : TElCRLExtension;
begin
  Result := TElCRLExtension(FOtherExtensions[Index]);
end;

function TElCRLExtensions.GetOtherCount : integer;
begin
  Result := FOtherExtensions.Count;
end;

procedure TElCRLExtensions.SetOtherCount(Value : integer);
var
  I : integer;
begin
  while FOtherExtensions.Count < Value do
    FOtherExtensions.Add(TElCRLExtension.Create);
  if FOtherExtensions.Count > Value then
  begin
    for I := Value to FOtherExtensions.Count - 1 do
      TElCustomExtension(FOtherExtensions.Items[I]).Free;
    FOtherExtensions.Count := Value;
  end;
end;

procedure TElCRLExtensions.ClearList;
var
  I : integer;
begin
  for I := 0 to FOtherExtensions.Count - 1 do
    TElCRLExtension(FOtherExtensions.Items[I]).Free;
  FOtherExtensions.Clear;
end;

function TElCRLExtensions.AddExtension(const OID : BufferType; Critical :
    boolean; const Value : BufferType): Integer;
var
  Extn : TElCRLExtension;
begin
  if CompareContent(OID, SB_OID_EXT_AUTHORITYKEYIDENTIFIER) then
  begin
    FAuthorityKeyIdentifier.Free;
    FAuthorityKeyIdentifier := TElAuthorityKeyIdentifierCRLExtension.Create;
    FIncluded := FIncluded + [crlAuthorityKeyIdentifier];
    Extn := FAuthorityKeyIdentifier;
  end
  else
  if CompareContent(OID, SB_OID_EXT_ISSUERALTERNATIVENAME) then
  begin
    FIssuerAlternativeName.Free;
    FIssuerAlternativeName := TElAlternativeNameExtension.Create;
    FIncluded := FIncluded + [crlIssuerAlternativeName];
    Extn := FIssuerAlternativeName;
  end
  else
  if CompareContent(OID, SB_OID_EXT_CRLNUMBER) then
  begin
    FCRLNumber.Free;
    FCRLNumber := TElCRLNumberCRLExtension.Create;
    FIncluded := FIncluded + [crlCRLNumber];
    Extn := FCRLNumber;
  end
  else
  if CompareContent(OID, SB_OID_EXT_DELTACRLINDICATOR) then
  begin
    FDeltaCRLIndicator.Free;
    FDeltaCRLIndicator := TElDeltaCRLIndicatorCRLExtension.Create;
    FIncluded := FIncluded + [crlDeltaCRLIndicator];
    Extn := FDeltaCRLIndicator;
  end
  else
  begin
    Extn := TElCRLExtension.Create;
    FOtherExtensions.Add(Extn);
  end;
  
  Extn.OID := CloneBuffer(OID);
  Extn.Critical := Critical;
  Extn.Value := CloneBuffer(Value);
  Result := 0;
end;

////////////////////////////////////////////////////////////////////////////////
// TElCRLEntryExtensions implementation

constructor TElCRLEntryExtensions.Create;
begin
  inherited;
  FOtherExtensions := TList.Create;
  FReasonCode := TElReasonCodeCRLExtension.Create;
  FHoldInstructionCode := TElHoldInstructionCodeCRLExtension.Create;
  FInvalidityDate := TElInvalidityDateCRLExtension.Create;
  FCertificateIssuer := TElCertificateIssuerCRLExtension.Create;
end;

destructor TElCRLEntryExtensions.Destroy;
begin
  ClearList;
  FReasonCode.Free;
  FHoldInstructionCode.Free;
  FInvalidityDate.Free;
  FCertificateIssuer.Free;
  FOtherExtensions.Free;
  inherited;
end;

function TElCRLEntryExtensions.GetOther(Index : integer) : TElCRLExtension;
begin
  Result := TElCRLExtension(FOtherExtensions[Index]);
end;

function TElCRLEntryExtensions.GetOtherCount : integer;
begin
  Result := FOtherExtensions.Count;
end;

procedure TElCRLEntryExtensions.SetOtherCount(Value : integer);
var
  I : integer;
begin
  while FOtherExtensions.Count < Value do
    FOtherExtensions.Add(TElCRLExtension.Create);
  if FOtherExtensions.Count > Value then
  begin
    for I := Value to FOtherExtensions.Count - 1 do
      TElCustomExtension(FOtherExtensions.Items[I]).Free;
    FOtherExtensions.Count := Value;
  end;
end;

procedure TElCRLEntryExtensions.ClearList;
var
  I : integer;
begin
  for I := 0 to FOtherExtensions.Count - 1 do
    TElCRLExtension(FOtherExtensions.Items[I]).Free;
  FOtherExtensions.Clear;
end;

function TElCRLEntryExtensions.LoadFromTag(Tag : TElASN1ConstrainedTag) : integer;
var
  I, Index : integer;
  CTag : TElASN1ConstrainedTag;
  ExtOID : BufferType;
  ExtCritical : boolean;
begin
  Result := SB_CRL_ERROR_INVALID_FORMAT;
  if Tag.TagId <> SB_ASN1_SEQUENCE then
    Exit;
  for I := 0 to Tag.Count - 1 do
  begin
    if (not Tag.GetField(I).IsConstrained) or (Tag.GetField(I).TagId <> SB_ASN1_SEQUENCE) then
      Exit;
    CTag := TElASN1ConstrainedTag(Tag.GetField(I));
    if CTag.Count < 2 then
      Exit;
    if (CTag.GetField(0).IsConstrained) or (CTag.GetField(0).TagId <> SB_ASN1_OBJECT) then
      Exit;
    ExtOID := TElASN1SimpleTag(CTag.GetField(0)).Content;
    if CTag.Count = 3 then
    begin
      if (CTag.GetField(1).IsConstrained) or (CTag.GetField(1).TagID <> SB_ASN1_BOOLEAN) then
        Exit;

      ExtCritical := TElASN1SimpleTag(CTag.GetField(1)).Content <> #0;
      Index := 2;
    end
    else
    begin
      ExtCritical := false;
      Index := 1;
    end;
    if (CTag.GetField(Index).IsConstrained) or (CTag.GetField(Index).TagID <> SB_ASN1_OCTETSTRING) then
      Exit;
    AddExtension(ExtOID, ExtCritical, TElASN1SimpleTag(CTag.GetField(Index)).Content);
  end;
  Result := 0;
end;

procedure TElCRLEntryExtensions.SaveToTag(Tag : TElASN1ConstrainedTag);
var
  TagSeq : TElASN1ConstrainedTag;
  I : integer;
begin
  Tag.TagID := SB_ASN1_SEQUENCE;
  if crlReasonCode in FIncluded then
  begin
    TagSeq := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    FReasonCode.SaveToTag(TagSeq);
  end;
  if crlHoldInstructionCode in FIncluded then
  begin
    TagSeq := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    FHoldInstructionCode.SaveToTag(TagSeq);
  end;
  if crlInvalidityDate in FIncluded then
  begin
    TagSeq := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    FInvalidityDate.SaveToTag(TagSeq);
  end;
  if crlCertificateIssuer in FIncluded then
  begin
    TagSeq := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    FCertificateIssuer.SaveToTag(TagSeq);
  end;
  for I := 0 to OtherCount - 1 do
  begin
    TagSeq := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    OtherExtensions[I].SaveToTag(TagSeq);
  end;
end;

function TElCRLEntryExtensions.AddExtension(const OID : BufferType; Critical :
    boolean; const Value : BufferType): Integer;
var
  Extn : TElCustomExtension;
begin
  if CompareContent(OID, SB_OID_EXT_REASONCODE) then
  begin
    FReasonCode.Free;
    FReasonCode := TElReasonCodeCRLExtension.Create;
    FIncluded := FIncluded + [crlReasonCode];
    Extn := FReasonCode;
  end
  else
  if CompareContent(OID, SB_OID_EXT_HOLDINSTRUCTIONCODE) then
  begin
    FHoldInstructionCode.Free;
    FHoldInstructionCode := TElHoldInstructionCodeCRLExtension.Create;
    FIncluded := FIncluded + [crlHoldInstructionCode];
    Extn := FHoldInstructionCode;
  end
  else
  if CompareContent(OID, SB_OID_EXT_INVALIDITYDATE) then
  begin
    FIncluded := FIncluded + [crlInvalidityDate];
    Extn := FInvalidityDate;
  end
  else
  if CompareContent(OID, SB_OID_EXT_CERTIFICATEISSUER) then
  begin
    FCertificateIssuer.Free;
    FCertificateIssuer := TElCertificateIssuerCRLExtension.Create;
    FIncluded := FIncluded + [crlCertificateIssuer];
    Extn := FCertificateIssuer;
  end
  else
  begin
    Extn := TElCRLExtension.Create;
    FOtherExtensions.Add(Extn);
  end;
  
  Extn.OID := CloneBuffer(OID);
  Extn.Critical := Critical;
  Extn.Value := CloneBuffer(Value);
  Result := 0;
end;

////////////////////////////////////////////////////////////////////////////////
// TElAuthorityKeyIdentifierCRLExtension implementation

constructor TElAuthorityKeyIdentifierCRLExtension.Create;
begin
  inherited;
  FAuthorityCertIssuer := TElGeneralNames.Create;
  IssuerSet := false;
end;

destructor TElAuthorityKeyIdentifierCRLExtension.Destroy;
begin
  FAuthorityCertIssuer.Free;
  inherited;
end;

procedure TElAuthorityKeyIdentifierCRLExtension.Clear;
begin
  SetLength(FKeyIdentifier, 0);
  SetLength(FAuthorityCertSerial, 0);
  FAuthorityCertIssuer.Free;
  FAuthorityCertIssuer := TElGeneralNames.Create;
  IssuerSet := false;
end;

function TElAuthorityKeyIdentifierCRLExtension.GetOID: BufferType;
begin
  Result := SB_OID_EXT_AUTHORITYKEYIDENTIFIER;
end;

procedure TElAuthorityKeyIdentifierCRLExtension.SetOID(const Value :
    BufferType);
begin
end;

procedure TElAuthorityKeyIdentifierCRLExtension.SetValue(const Value :
    BufferType);
var
  Tag, SeqTag : TElASN1ConstrainedTag;
  CurrTagIndex : integer;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        SeqTag := TElASN1ConstrainedTag(Tag.GetField(0));
        CurrTagIndex := 0;
        if (CurrTagIndex < SeqTag.Count) and (SeqTag.GetField(CurrTagIndex).CheckType($80, false)) then
        begin
          FKeyIdentifier := TElASN1SimpleTag(SeqTag.GetField(CurrTagIndex)).Content;
          Inc(CurrTagIndex);
        end;
        if (CurrTagIndex < SeqTag.Count) and (SeqTag.GetField(CurrTagIndex).CheckType($A1, true)) then
        begin
          //ParseGeneralNames(TElASN1ConstrainedTag(SeqTag.GetField(CurrTagIndex)),
          //  FAuthorityCertIssuer, true, true);
          FAuthorityCertIssuer.LoadFromTag(TElASN1ConstrainedTag(SeqTag.GetField(CurrTagIndex)), true);
          Inc(CurrTagIndex);
        end;
        if (CurrTagIndex < SeqTag.Count) and (SeqTag.GetField(CurrTagIndex).CheckType($82, false)) then
        begin
          FAuthorityCertSerial := RotateInteger(TElASN1SimpleTag(SeqTag.GetField(CurrTagIndex)).Content);
        end;
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

function TElAuthorityKeyIdentifierCRLExtension.GetValue: BufferType;
var
  Tag, CTag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  Size : integer;
begin
  Tag := TElASN1ConstrainedTag.Create;
  Tag.TagID := SB_ASN1_SEQUENCE;
  if Length(FKeyIdentifier) > 0 then
  begin
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagID := $80;
    STag.Content := CloneBuffer(FKeyIdentifier);
  end;
  if FSaveIssuer then
  begin
    CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    FAuthorityCertIssuer.SaveToTag(CTag); //TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true))));
    CTag.TagID := $A1;
  end;
  if Length(FAuthorityCertSerial) > 0 then
  begin
    STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
    STag.TagID := $82;
    STag.Content := CloneBuffer(FAuthorityCertSerial);
  end;
  Size := 0;
  Tag.SaveToBuffer(nil, Size);
  SetLength(Result, Size);
  Tag.SaveToBuffer(@Result[1], Size);
  SetLength(Result, Size);
end;

////////////////////////////////////////////////////////////////////////////////
// TElCRLNumberCRLExtension implementation

procedure TElCRLNumberCRLExtension.Clear;
begin
  FNumber := 0;
end;

function TElCRLNumberCRLExtension.GetOID: BufferType;
begin
  Result := SB_OID_EXT_CRLNUMBER;
end;

procedure TElCRLNumberCRLExtension.SetOID(const Value : BufferType);
begin
end;

procedure TElCRLNumberCRLExtension.SetValue(const Value : BufferType);
var
  I, K, Len : integer;
  TmpValue : BufferType;
begin
  if Byte(Value[1]) <> SB_ASN1_INTEGER then
    Exit;
  Len := Byte(Value[2]);
  TmpValue := Copy(Value, 3, Len);

  FNumber := 0;
  K := 0;

  for I := Length(TmpValue) downto 1 do
  begin
    FNumber := FNumber or (Ord(TmpValue[I]) shl K);
    Inc(K, 8);
  end;
end;

function TElCRLNumberCRLExtension.GetValue: BufferType;
begin
  Result := Chr(SB_ASN1_INTEGER) + #$04 + Chr(FNumber shr 24) +
    Chr((FNumber shr 16) and $FF) + Chr((FNumber shr 8) and $FF) +
    Chr(FNumber and $FF);
end;

////////////////////////////////////////////////////////////////////////////////
// TElDeltaCRLIndicatorCRLExtension implementation

procedure TElDeltaCRLIndicatorCRLExtension.Clear;
begin
  FNumber := 0;
end;

function TElDeltaCRLIndicatorCRLExtension.GetOID: BufferType;
begin
  Result := SB_OID_EXT_DELTACRLINDICATOR;
end;

procedure TElDeltaCRLIndicatorCRLExtension.SetOID(const Value : BufferType);
begin
end;

procedure TElDeltaCRLIndicatorCRLExtension.SetValue(const Value : BufferType);
var
  I, K, Len : integer;
  TmpValue : BufferType;
begin
  if Byte(Value[1]) <> SB_ASN1_INTEGER then
    Exit;
  Len := Byte(Value[2]);
  TmpValue := Copy(Value, 3, Len);

  FNumber := 0;
  K := 0;

  for I := Length(TmpValue) downto 1 do
  begin
    FNumber := FNumber or (Ord(TmpValue[I]) shl K);
    Inc(K, 8);
  end;
end;

function TElDeltaCRLIndicatorCRLExtension.GetValue: BufferType;
begin
  Result := Chr(SB_ASN1_INTEGER) + #$04 + Chr(FNumber shr 24) +
    Chr((FNumber shr 16) and $FF) + Chr((FNumber shr 8) and $FF) +
    Chr(FNumber and $FF);
end;


////////////////////////////////////////////////////////////////////////////////
// TElReasonCodeCRLExtension implementation

procedure TElReasonCodeCRLExtension.Clear;
begin
  FReason := rfUnspecified;
  FRemoveFromCRL := false;
end;

function TElReasonCodeCRLExtension.GetOID : BufferType;
begin
  Result := SB_OID_EXT_REASONCODE;
end;

procedure TElReasonCodeCRLExtension.SetOID(const Value : BufferType);
begin
end;

procedure TElReasonCodeCRLExtension.SetValue(const Value : BufferType);
var
  Data : byte;
const
  Reasons : array[0..10] of TSBCRLReasonFlag = 
   (
    rfUnspecified, rfKeyCompromise,
    rfCACompromise, rfAffiliationChanged, rfSuperseded, rfCessationOfOperation,
    rfCertificateHold, rfObsolete1, rfRemoveFromCRL, rfPrivilegeWithdrawn, rfAACompromise
   );
begin
  FReason := rfUnspecified;
  if (Length(Value) > 2) and
     (Byte(Value[1]) = SB_ASN1_ENUMERATED)
     then
  begin
    if Byte(Value[2]) >= 1 then
      Data := Byte(Value[3])
    else
      Data := 0;
    if (Data <= 10) then
    begin
      FReason := Reasons[Data];
      if Data = 8 then
        FRemoveFromCRL := true;
    end
    else
      FRemoveFromCRL := false;
  end;
end;

function TElReasonCodeCRLExtension.GetValue : BufferType;
var
  Sym : char;
begin
  if FRemoveFromCRL then
    Sym := #8
  else
  begin
    case FReason of
      rfKeyCompromise : Sym := #1;
      rfCACompromise : Sym := #2;
      rfAffiliationChanged : Sym := #3;
      rfSuperseded : Sym := #4;
      rfCessationOfOperation : Sym := #5;
      rfCertificateHold : Sym := #6;
    else
      Sym := #0;
    end;
  end;
  Result := Chr(SB_ASN1_ENUMERATED) + #$01 + Sym;
end;

////////////////////////////////////////////////////////////////////////////////
// TElHoldInstructionCodeCRLExtension implementation

const

  SB_OID_EXT_HIC_NONE           :TBufferTypeConst = #$2a#$86#$48#$ce#$38#$02#$1;
  SB_OID_EXT_HIC_CALLISSUER     :TBufferTypeConst = #$2a#$86#$48#$ce#$38#$02#$2;
  SB_OID_EXT_HIC_REJECT         :TBufferTypeConst = #$2a#$86#$48#$ce#$38#$02#$3;

procedure TElHoldInstructionCodeCRLExtension.Clear;
begin
  FCode := icNone;
end;

function TElHoldInstructionCodeCRLExtension.GetOID: BufferType;
begin
  Result := SB_OID_EXT_HOLDINSTRUCTIONCODE;
end;

procedure TElHoldInstructionCodeCRLExtension.SetOID(const Value : BufferType);
begin
end;

procedure TElHoldInstructionCodeCRLExtension.SetValue(const Value : BufferType);
var
  Len : integer;
  Data : BufferType;
begin

  if (Length(Value) > 3) and
     (Byte(Value[1]) = SB_ASN1_OBJECT)
  then
  begin
    Len := Byte(Value[2]);
    Data := Copy(Value, 3, Len);
    if CompareContent(Data, SB_OID_EXT_HIC_CALLISSUER) then
      FCode := icCallIssuer
    else
    if CompareContent(Data, SB_OID_EXT_HIC_REJECT) then
      FCode := icReject
    else
      FCode := icNone;
  end
  else
    FCode := icNone;
end;

function TElHoldInstructionCodeCRLExtension.GetValue: BufferType;
begin
  if FCode = icNone then
    Result := TBufferTypeConst(Chr(SB_ASN1_OBJECT) +
      Chr(Length(SB_OID_EXT_HIC_NONE)) +
      SB_OID_EXT_HIC_NONE)
  else
  if FCode = icCallIssuer then
    Result := TBufferTypeConst(Chr(SB_ASN1_OBJECT) +
      Chr(Length(SB_OID_EXT_HIC_CALLISSUER)) +
      SB_OID_EXT_HIC_CALLISSUER)
  else
    Result := TBufferTypeConst(Chr(SB_ASN1_OBJECT) +
      Chr(Length(SB_OID_EXT_HIC_REJECT)) +
      SB_OID_EXT_HIC_REJECT);
end;

////////////////////////////////////////////////////////////////////////////////
// TElInvalidityDateCRLExtension implementation

procedure TElInvalidityDateCRLExtension.Clear;
begin
  FDate := 0;
end;

function TElInvalidityDateCRLExtension.GetOID: BufferType;
begin
  Result := SB_OID_EXT_INVALIDITYDATE;
end;

procedure TElInvalidityDateCRLExtension.SetOID(const Value : BufferType);
begin
end;

procedure TElInvalidityDateCRLExtension.SetValue(const Value : BufferType);
var
  Data : BufferType;
  Len  : integer;
begin
  if (Length(Value) > 3) and
     (Byte(Value[1]) = SB_ASN1_GENERALIZEDTIME)
  then
  begin
    Len := Byte(Value[2]);
    Data := Copy(Value, 3, Len);
    FDate := GeneralizedTimeToDateTime((Data));
  end
  else
    FDate := 0;
end;

function TElInvalidityDateCRLExtension.GetValue: BufferType;
var
  Str : string;
begin
  Str := DateTimeToGeneralizedTime(FDate);
  Result := WriteStringPrimitive(SB_ASN1_GENERALIZEDTIME, Str);
end;

////////////////////////////////////////////////////////////////////////////////
// TElCertificateIssuerCRLExtension implementation

constructor TElCertificateIssuerCRLExtension.Create;
begin
  inherited;
  FIssuer := TElGeneralNames.Create;
end;

destructor TElCertificateIssuerCRLExtension.Destroy;
begin
  FIssuer.Free;
  inherited;
end;

procedure TElCertificateIssuerCRLExtension.Clear;
begin
  FIssuer.Free;
  FIssuer := TElGeneralNames.Create;
end;

function TElCertificateIssuerCRLExtension.GetOID: BufferType;
begin
  Result := SB_OID_EXT_CERTIFICATEISSUER;
end;

procedure TElCertificateIssuerCRLExtension.SetOID(const Value : BufferType);
begin
end;

procedure TElCertificateIssuerCRLExtension.SetValue(const Value : BufferType);
var
  Tag : TElASN1ConstrainedTag;
begin
  Tag := TElASN1ConstrainedTag.Create;
  if Tag.LoadFromBuffer(@Value[1], Length(Value)) and (Tag.Count >= 1) and (Tag.GetField(0).IsConstrained) then
    FIssuer.LoadFromTag(TElASN1ConstrainedTag(Tag.GetField(0)){$ifndef HAS_DEF_PARAMS}, false{$endif});
    
  Tag.Free;
end;

function TElCertificateIssuerCRLExtension.GetValue: BufferType;
var
  Tag : TElASN1ConstrainedTag;
  Size : integer;
begin
  Tag := TElASN1ConstrainedTag.Create;
  FIssuer.SaveToTag(Tag);
  Size := 0;
  Tag.SaveToBuffer(nil, Size);
  SetLength(Result, Size);
  Tag.SaveToBuffer(@Result[1], Size);
  SetLength(Result, Size);
  
  Tag.Free;
end;

end.
