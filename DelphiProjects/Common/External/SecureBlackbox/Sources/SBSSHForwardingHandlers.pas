(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSSHForwardingHandlers;

interface

uses
  SBSSHCommon, SBSSHHandlers, ScktComp;

type
  TElTCPForwardingSSHSubsystemHandler = class(TElCustomSocketForwardingSSHSubsystemHandler)
  protected
    FSocket : TCustomWinSocket;
    procedure SocketDisconnect; override;
    function SocketReadable(Timeout : integer): boolean; override;
    function SocketConnected: boolean; override;
    procedure SocketWrite(Buffer: pointer; Size: integer); override;
    function SocketRead(Buffer: pointer; Size: integer): integer; override;
  public
  end;

  TElClientTCPForwardingSSHSubsystemHandler = class(TElTCPForwardingSSHSubsystemHandler)
  private
    FClientSocket : TClientSocket;
  protected
    procedure SocketConnect; override;
    procedure Initialize; override;
    procedure NFinalize; override;
  end;

  TElServerTCPForwardingSSHSubsystemHandler = class(TElTCPForwardingSSHSubsystemHandler)
  protected
    procedure SocketConnect; override;
  public
    property Socket: TCustomWinSocket read FSocket write FSocket;
  end;

implementation

uses
  WinSock, Windows;

////////////////////////////////////////////////////////////////////////////////
// TElTCPForwardingSSHSubsystemHandler class

procedure TElTCPForwardingSSHSubsystemHandler.SocketDisconnect;
begin
  FSocket.Close;
end;

function TElTCPForwardingSSHSubsystemHandler.SocketReadable(Timeout : integer): boolean;
var
  FDSet: TFDSet;
  TimeVal: TTimeVal;
  PTV : PTimeVal;
begin
  FD_ZERO(FDSet);
  FD_SET(FSocket.SocketHandle, FDSet);
  if Timeout = -1 then
    PTV := nil
  else
  begin
    TimeVal.tv_sec := Timeout div 1000;
    TimeVal.tv_usec := (Timeout mod 1000) * 1000;
    PTV := @TimeVal;
  end;
  Result := (select(0, @FDSet, nil, nil, PTV) > 0);
end;

function TElTCPForwardingSSHSubsystemHandler.SocketConnected: boolean;
begin
  Result := FSocket.Connected;
end;

procedure TElTCPForwardingSSHSubsystemHandler.SocketWrite(Buffer: pointer; Size: integer);
var
  Ptr: ^byte;
  Sent: integer;
begin
  Ptr := Buffer;
  while (Size > 0) do
  begin
    Sent := FSocket.SendBuf(Ptr^, Size);
    Inc(Ptr, Sent);
    Dec(Size, Sent);
    Sleep(50);
  end;
end;

function TElTCPForwardingSSHSubsystemHandler.SocketRead(Buffer: pointer; Size: integer): integer;
begin
  Result := FSocket.ReceiveBuf(Buffer^, Size);
  if Result < 0 then Result := 0;
end;

////////////////////////////////////////////////////////////////////////////////
// TElClientForwardingSSHSubsystemHandler class

procedure TElClientTCPForwardingSSHSubsystemHandler.Initialize;
begin
  FClientSocket := TClientSocket.Create(nil);
  FClientSocket.ClientType := ctBlocking;
  FSocket := FClientSocket.Socket;
end;

procedure TElClientTCPForwardingSSHSubsystemHandler.NFinalize;
begin
  FClientSocket.Free;
end;

procedure TElClientTCPForwardingSSHSubsystemHandler.SocketConnect;
begin
  FClientSocket.Host := Host;
  FClientSocket.Port := Port;
  FClientSocket.Open;
end;

////////////////////////////////////////////////////////////////////////////////
// TElServerForwardingSSHSubsystemHandler class

procedure TElServerTCPForwardingSSHSubsystemHandler.SocketConnect;
begin
  { Empty, as server socket is already connected }
end;

end.
