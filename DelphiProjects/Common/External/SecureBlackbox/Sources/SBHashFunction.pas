(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBHashFunction;

interface

uses
  SBConstants,
  SBUtils,
  SysUtils,
  SBCustomCrypto,
  SBMD,
  SBSHA,
  SBSHA2,
  SBRIPEMD;

type
  TElHashFunction = class
  private
    FAlgorithm: integer;
    FCtxMD2 : TMD2Context;
    FCtxMD5 : TMD5Context;
    FCtxSHA1 : TSHA1Context;
    FCtxSHA256 : TSHA256Context;
    FCtxSHA512 : TSHA512Context;
    FCtxRMD160 : TRMD160Context;
    FDigest : BufferType;
    procedure InitializeDigest;
    procedure UpdateDigest(Buffer: pointer; Size: integer);
    procedure FinalizeDigest;
  public
    constructor Create(Algorithm: integer); overload;
    constructor Create(const OID : BufferType); overload;
    destructor Destroy; override;
    
    procedure Reset;
    procedure Update(Buffer: pointer; Size: integer);
    function Finish : BufferType;
    property Algorithm: integer read FAlgorithm;
    //property Digest : BufferType read FDigest;
    class function IsAlgorithmSupported(Algorithm: integer): boolean; overload;
    class function IsAlgorithmSupported(const OID : BufferType): boolean; overload;
    class function GetDigestSizeBits(Algorithm: integer): integer; overload;
    class function GetDigestSizeBits(const OID : BufferType): integer; overload;
  end;

  EElHashFunctionError =  class(Exception);
  EElHashFunctionUnsupportedError =  class(EElHashFunctionError);

  TElHMACKeyMaterial = class(TElKeyMaterial)
  protected
    FKey: ByteArray;
  public
    property Key: ByteArray read FKey write FKey;
  end;

implementation

resourcestring
  SUnsupportedAlgorithm = 'Unsupported algorithm: %s';

constructor TElHashFunction.Create(Algorithm: integer);
begin
  inherited Create;
  if IsAlgorithmSupported(Algorithm) then
  begin
    FAlgorithm := Algorithm;
    InitializeDigest;
  end
  else
    raise EElHashFunctionUnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(Algorithm)]);
end;

constructor TElHashFunction.Create(const OID : BufferType);
var
  Alg : integer;
begin
  Alg := GetHashAlgorithmByOID(OID);
  Create(Alg);
end;

destructor TElHashFunction.Destroy;
begin
  inherited;
end;

procedure TElHashFunction.InitializeDigest;
begin
  case FAlgorithm of
    SB_ALGORITHM_DGST_MD2 : InitializeMD2(FCtxMD2);
    SB_ALGORITHM_DGST_MD5 : InitializeMD5(FCtxMD5);
    SB_ALGORITHM_DGST_SHA1 : InitializeSHA1(FCtxSHA1);
    SB_ALGORITHM_DGST_SHA224 : InitializeSHA256(FCtxSHA256);    
    SB_ALGORITHM_DGST_SHA256 : InitializeSHA256(FCtxSHA256);
    SB_ALGORITHM_DGST_SHA512 : InitializeSHA512(FCtxSHA512);
    SB_ALGORITHM_DGST_SHA384 : InitializeSHA384(FCtxSHA512);
    SB_ALGORITHM_DGST_RIPEMD160 : InitializeRMD160(FCtxRMD160);
  else
    raise EElHashFunctionUnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(FAlgorithm)]);
  end;
end;

procedure TElHashFunction.UpdateDigest(Buffer: pointer; Size: integer);
begin
  case FAlgorithm of
    SB_ALGORITHM_DGST_MD2 :
    begin
      HashMD2(FCtxMD2, Buffer, Size);
    end;
    SB_ALGORITHM_DGST_MD5 :
    begin
      HashMD5(FCtxMD5, Buffer, Size);
    end;
    SB_ALGORITHM_DGST_SHA1 :
    begin
      HashSHA1(FCtxSHA1, Buffer, Size);
    end;
    SB_ALGORITHM_DGST_SHA224 : HashSHA224(FCtxSHA256, Buffer, Size);    
    SB_ALGORITHM_DGST_SHA256 : HashSHA256(FCtxSHA256, Buffer, Size);
    SB_ALGORITHM_DGST_SHA512 : HashSHA512(FCtxSHA512, Buffer, Size);
    SB_ALGORITHM_DGST_SHA384 : HashSHA384(FCtxSHA512, Buffer, Size);
    SB_ALGORITHM_DGST_RIPEMD160 :
    begin
      HashRMD160(FCtxRMD160, Buffer, Size);
    end;
  else
    raise EElHashFunctionUnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(FAlgorithm)]);
  end;
end;

procedure TElHashFunction.FinalizeDigest;
var
  M128 : TMessageDigest128;
  M160 : TMessageDigest160;
  M224 : TMessageDigest224;  
  M256 : TMessageDigest256;
  M384 : TMessageDigest384;
  M512 : TMessageDigest512;
begin
  case FAlgorithm of
    SB_ALGORITHM_DGST_MD2 :
    begin
      M128 := FinalizeMD2(FCtxMD2);
      SetLength(FDigest, 16);
      Move(M128, FDigest[1], 16);
    end;
    SB_ALGORITHM_DGST_MD5 :
    begin
      M128 := FinalizeMD5(FCtxMD5);
      SetLength(FDigest, 16);
      Move(M128, FDigest[1], 16);
    end;
    SB_ALGORITHM_DGST_SHA1 :
    begin
      M160 := FinalizeSHA1(FCtxSHA1);
      SetLength(FDigest, 20);
      Move(M160, FDigest[1], 20);
    end;
    SB_ALGORITHM_DGST_SHA224 :
    begin
      M224 := FinalizeSHA224(FCtxSHA256);
      SetLength(FDigest, 28);
      Move(M224, FDigest[1], 28);
    end;
    SB_ALGORITHM_DGST_SHA256 :
    begin
      M256 := FinalizeSHA256(FCtxSHA256);
      SetLength(FDigest, 32);
      Move(M256, FDigest[1], 32);
    end;
    SB_ALGORITHM_DGST_SHA512 :
    begin
      M512 := FinalizeSHA512(FCtxSHA512);
      SetLength(FDigest, 64);
      Move(M512, FDigest[1], 64);
    end;
    SB_ALGORITHM_DGST_SHA384 :
    begin
      M384 := FinalizeSHA384(FCtxSHA512);
      SetLength(FDigest, 48);
      Move(M384, FDigest[1], 48);
    end;
    SB_ALGORITHM_DGST_RIPEMD160 :
    begin
      M160 := FinalizeRMD160(FCtxRMD160);
      SetLength(FDigest, 20);
      Move(M160, FDigest[1], 20);
    end;
  else
    raise EElHashFunctionUnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(FAlgorithm)]);
  end;
end;

class function TElHashFunction.IsAlgorithmSupported(Algorithm: integer): boolean;
begin
  Result := GetDigestSizeBits(Algorithm) > 0;
end;

class function TElHashFunction.IsAlgorithmSupported(const OID : BufferType): boolean;
var
  Alg : integer;
begin
  Alg := GetHashAlgorithmByOID(OID);
  Result := IsAlgorithmSupported(Alg);
end;

class function TElHashFunction.GetDigestSizeBits(Algorithm: integer): integer;
begin
  Result := SBUtils.GetDigestSizeBits(Algorithm);
  if Result < 0 then
    raise EElHashFunctionUnsupportedError.CreateFmt(SUnsupportedAlgorithm, [IntToStr(Algorithm)]);
end;

class function TElHashFunction.GetDigestSizeBits(const OID : BufferType): integer;
var
  Alg : integer;
begin
  Alg := GetHashAlgorithmByOID(OID);
  if Alg <> SB_ALGORITHM_UNKNOWN then
    Result := GetDigestSizeBits(Alg)
  else
    raise EElHashFunctionUnsupportedError.CreateFmt(SUnsupportedAlgorithm, [OIDToStr(OID)]);
end;

procedure TElHashFunction.Reset;
begin
  InitializeDigest;
end;

procedure TElHashFunction.Update(Buffer: pointer; Size: integer);
begin
  UpdateDigest(Buffer, Size);
end;

function TElHashFunction.Finish : BufferType;
begin
  FinalizeDigest;
  Result := FDigest;
end;

end.
