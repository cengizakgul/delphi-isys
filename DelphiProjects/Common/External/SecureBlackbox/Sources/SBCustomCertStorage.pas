(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBCustomCertStorage;

interface

uses
  Classes,
  SysUtils,
  SBRDN,
  SBX509,
  SBX509Ext,
  SBCRL,
  SBSharedResource,
  SBConstants,
  SBUtils
  ;



type
  TSBCertificateValidity =  
    (cvOk { = 1}, cvSelfSigned { = 2}, cvInvalid { = 4},
     cvStorageError { = 8});
  TSBCertificateValidityReason = set of (vrBadData { = 1}, vrRevoked { = 2},
    vrNotYetValid { = 4},
    vrExpired { = 8}, vrInvalidSignature { = 16}, vrUnknownCA { = 32});

type
  TSBLookupCriterion = (lcIssuer, lcSubject, lcValidity, lcPublicKeyAlgorithm,
    lcSignatureAlgorithm, lcPublicKeySize, lcAuthorityKeyIdentifier,
    lcSubjectKeyIdentifier, lcKeyUsage, lcEmail, lcSerialNumber);
  TSBLookupCriteria = set of TSBLookupCriterion;
  TSBLookupOption = (loExactMatch, loMatchAll);
  TSBLookupOptions = set of TSBLookupOption;
  TSBDateLookupOption = (dloBefore, dloAfter, dloBetween);
  TSBDateLookupOptions = set of TSBDateLookupOption;

  TSBKeySizeLookupOption = 
    (ksloSmaller, ksloGreater, ksloBetween);
    
  TSBKeyUsageLookupOption = (kuloMatchAll);
  TSBKeyUsageLookupOptions = set of TSBKeyUsageLookupOption;
  TElCustomCertStorage =  class;

  TElCertificateLookup = class (TSBControlBase)
  private
    FCriteria: TSBLookupCriteria;
    FOptions: TSBLookupOptions;
    FIssuerRDN: TElRelativeDistinguishedName;
    FSubjectRDN: TElRelativeDistinguishedName;
    FValidFrom: TDateTime;
    FValidTo: TDateTime;
    FPublicKeyAlgorithm: integer;
    FSignatureAlgorithm: integer;
    FPublicKeySizeMin: integer;
    FPublicKeySizeMax: integer;
    FAuthorityKeyIdentifier: BufferType;
    FSubjectKeyIdentifier: BufferType;
    FKeyUsage: TSBKeyUsage;
    FEmailAddresses: TStringList;
    FSerialNumber : BufferType;
    FDateLookupOptions: TSBDateLookupOptions;
    FKeySizeLookupOption: TSBKeySizeLookupOption;
    FKeyUsageLookupOptions: TSBKeyUsageLookupOptions;
  protected
    FLastIndex: integer;
    procedure SetCriteria(Value: TSBLookupCriteria);
    function FindNext(Storage: TElCustomCertStorage): integer; virtual;


  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    
  published
    property Criteria: TSBLookupCriteria read FCriteria write SetCriteria;
    property Options: TSBLookupOptions read FOptions write FOptions;
    property IssuerRDN: TElRelativeDistinguishedName read FIssuerRDN;
    property SubjectRDN: TElRelativeDistinguishedName read FSubjectRDN;
    property ValidFrom: TDateTime read
      FValidFrom write FValidFrom;
    property ValidTo: TDateTime read
      FValidTo write FValidTo;
    property PublicKeyAlgorithm: integer read FPublicKeyAlgorithm
    write FPublicKeyAlgorithm;
    property SignatureAlgorithm: integer read FSignatureAlgorithm
    write FSignatureAlgorithm;
    property PublicKeySizeMin: integer read FPublicKeySizeMin
    write FPublicKeySizeMin;
    property PublicKeySizeMax: integer read FPublicKeySizeMax
    write FPublicKeySizeMax;
    property AuthorityKeyIdentifier: BufferType read FAuthorityKeyIdentifier
    write FAuthorityKeyIdentifier;
    property SubjectKeyIdentifier: BufferType read FSubjectKeyIdentifier
    write FSubjectKeyIdentifier;
    property KeyUsage: TSBKeyUsage read FKeyUsage write FKeyUsage;
    property EmailAddresses: TStringList read FEmailAddresses;
    property SerialNumber : BufferType read FSerialNumber write FSerialNumber;
    property DateLookupOptions: TSBDateLookupOptions read FDateLookupOptions
    write FDateLookupOptions;
    property KeySizeLookupOption: TSBKeySizeLookupOption read
      FKeySizeLookupOption
    write FKeySizeLookupOption;
    property KeyUsageLookupOptions: TSBKeyUsageLookupOptions
    read FKeyUsageLookupOptions write FKeyUsageLookupOptions;
  end;

  TSBCertStorageOption = (csoStrictChainBuilding);
  TSBCertStorageOptions = set of TSBCertStorageOption;

  TElCustomCertStorage = class(TElBaseCertStorage)
  private
    FOptions : TSBCertStorageOptions;
    {$ifndef FPC}
    procedure ReadCertificatesProp(Reader: TStream);
    procedure WriteCertificatesProp(Writer: TStream);
    function IsCertificatesPropStored: boolean;
    procedure ReadFakeCertificatesProp(Reader: TReader);
    {$endif}
  protected
    FRebuildChains: boolean;
    FChains: array of integer;
    FCRL: TElCertificateRevocationList;
    FSharedResource: TElSharedResource;
  protected
    function Equal(N1, N2: TName): boolean;
    function GetCount: integer; virtual;
    function GetCertificates(Index: integer): TElX509Certificate; virtual;
    function GetChainCount: integer;
    function GetChain(Index: integer): integer;
    procedure AssignTo(Dest: TPersistent); override;
    {$ifndef FPC}
    procedure DefineProperties(Filer: TFiler); override;
    {$endif}
    function IsIssuerCertificate(Subject, Issuer: TElX509Certificate): boolean;
    procedure BuildAllChains;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetCRL(Value: TElCertificateRevocationList);


  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;

    function Validate(Certificate: TElX509Certificate;
      var Reason: TSBCertificateValidityReason;
      ValidityMoment: TDateTime = 0):
    TSBCertificateValidity; virtual;

    procedure Add(Certificate: TElX509Certificate; CopyPrivateKey: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}); virtual;
    procedure Remove(Index: integer); virtual;
    procedure ExportTo(Storage: TElCustomCertStorage); virtual;

    function LoadFromBufferPKCS7(Buffer: pointer; Size: longint) : integer;
    function SaveToBufferPKCS7(Buffer: pointer; var Size: longint) : boolean;
    function LoadFromStreamPKCS7(Stream: TStream; Count: integer = 0): integer;
    function SaveToStreamPKCS7(Stream: TStream): boolean;

    function LoadFromBufferPFX(Buffer: pointer; Size: integer; const Password:
      string): integer;
    function SaveToBufferPFX(Buffer: pointer; var Size: integer; const Password:
      string; KeyEncryptionAlgorithm: integer; CertEncryptionAlgorithm:
        integer): integer;
    function LoadFromStreamPFX(Stream: TStream; const Password: string;
      Count: integer = 0): integer;
    function SaveToStreamPFX(Stream: TStream; const Password: string;
      KeyEncryptionAlgorithm: integer; CertEncryptionAlgorithm: integer):
        integer;
    function IndexOf(Certificate: TElX509Certificate): Integer; virtual;
    function IsPresent(Certificate: TElX509Certificate): boolean;
    procedure Clear; virtual;
    function FindByHash(Digest: TMessageDigest160): integer; overload;
    function FindByHash(Digest: TMessageDigest128): integer; overload;
    function GetIssuerCertificate(Certificate: TElX509Certificate): integer;
      virtual;
    class function ReadOnly: Boolean; virtual;
    function FindFirst(Lookup: TElCertificateLookup): integer;
    function FindNext(Lookup: TElCertificateLookup): integer;

    property Count: integer read GetCount;
    property Certificates[Index: integer]: TElX509Certificate read GetCertificates;
    property ChainCount: integer read GetChainCount;
    property Chains[Index: integer]: integer read GetChain;
  published
    property CRL: TElCertificateRevocationList read FCRL write SetCRL;
    property Options : TSBCertStorageOptions read FOptions write FOptions;
  end;

  TElMemoryCertStorage = class(TElCustomCertStorage)
  private
    FCertificateList: TSBObjectList;
  protected
    function GetCount: integer; override;
    function GetCertificates(Index: integer): TElX509Certificate; override;

  
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    
    procedure Add(X509Certificate: TElX509Certificate; CopyPrivateKey: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}); override;
    procedure Remove(Index: integer); override;
    property CertificateList: TSBObjectList read FCertificateList;
  end;

  TSBFileCertStorageAccessType =  
    (csatImmediate, csatOnDemand);

  TSBFileCertStorageSaveOption = (fcsoSaveOnDestroy, fcsoSaveOnFilenameChange,
    fcsoSaveOnChange);
  TSBFileCertStorageSaveOptions = set of TSBFileCertStorageSaveOption;

  TElFileCertStorage = class(TElCustomCertStorage)
  private
    FFileName: string;
    FCertificateList: TSBObjectList;
    FLoaded: boolean;
    FAccessType: TSBFileCertStorageAccessType;
    FSaveOptions: TSBFileCertStorageSaveOptions;
  protected
    procedure LoadFromFile;
    function GetCount: integer; override;
    function GetCertificates(Index: integer): TElX509Certificate; override;
    procedure SetFileName(const FileName: string);
    procedure SetAccessType(Value: TSBFileCertStorageAccessType);
    procedure InternalClear;
    procedure CreateEmptyStorage;


  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    
    function Validate(Certificate: TElX509Certificate; var Reason:
      TSBCertificateValidityReason; ValidityMoment: TDateTime
        = 0):
    TSBCertificateValidity; override;
    procedure Add(X509Certificate: TElX509Certificate; 
        CopyPrivateKey: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif}); override;
    procedure Remove(Index: integer); override;
    procedure Clear; override;
    procedure SaveToFile(const FileName: string);
    procedure Reload;
    procedure Save;
  published
    property FileName: string read FFileName write SetFileName;
    property AccessType: TSBFileCertStorageAccessType read FAccessType
    write SetAccessType default csatOnDemand;
    property SaveOptions: TSBFileCertStorageSaveOptions read FSaveOptions
    write FSaveOptions default [];
  end;

  EElCertStorageException =  class(Exception);

procedure Register;

implementation

{$IFNDEF CLX_USED}
uses
  Windows,
{$ELSE}
uses
  Libc,
{$ENDIF}
  SBPKCS12,
  SBSHA,
  SBMD,
  SBPKCS7,
  SBMessages;


procedure Register;
begin
  RegisterComponents('PKIBlackbox', [TElFileCertStorage,
    TElMemoryCertStorage, TElCertificateLookup]);
end;

const
  BufferSize: longint = 400000;

const
  PKCS7OID : TBufferTypeConst =  #$2A#$86#$48#$86#$F7#$0D#$01#$07#$02;
  PKCS7Data: TBufferTypeConst =  #$2A#$86#$48#$86#$F7#$0D#$01#$07#$01;

resourcestring
  SInvalidPKCS7File = 'Invalid PKCS7 file';
  SCannotAccessFile = 'Can not access file';
  SNoSignedData = 'No signed data found';
  SUnableToWritePKCS7Data = 'Unable to write PKCS7 data';
  SUnableToMountStorage = 'Unable to mount file storage';


////////////////////////////////////////////////////////////////////////////////
// TElCustomCertStorage
////////////////////////////////////////////////////////////////////////////////

constructor TElCustomCertStorage.Create(Owner:
TComponent);
begin
  inherited Create(Owner);
  FRebuildChains := true;
  FSharedResource := TElSharedResource.Create;
  FOptions := [csoStrictChainBuilding];
end;


destructor TElCustomCertStorage.Destroy;
begin
  FreeAndNil(FSharedResource);
  inherited;
end;


function TElCustomCertStorage.Equal(N1, N2: TName): boolean;
begin
  if (N1.Country = N2.Country) and (N1.StateOrProvince = N2.StateOrProvince) and
    (N1.Locality = N2.Locality) and (N1.Organization = N2.Organization) and
    (N1.OrganizationUnit = N2.OrganizationUnit) and (N1.CommonName =
    N2.CommonName) then
    Result := true
  else
    Result := false;
end;

function TElCustomCertStorage.Validate(Certificate: TElX509Certificate;
  var Reason: TSBCertificateValidityReason; ValidityMoment:
    TDateTime = 0):
TSBCertificateValidity;
var
  CACert: TElX509Certificate;
  I: longint;
  vm: double;
begin
  CheckLicenseKey();
  Result := cvOk;
  Reason :=  [vrUnknownCA];
  vm := ValidityMoment;
  if ValidityMoment = 0.0 then
    vm := now;
  {$ifndef NET_CF_1_0}
  if (Certificate.ValidFrom > vm) then
  {$else}
  if (DateTimeToOADate(Certificate.ValidFrom) > vm) then
  {$endif}
  begin
    Reason := Reason + [vrNotYetValid];
    Result := cvInvalid;
  end;
  {$ifndef NET_CF_1_0}
  if (Certificate.ValidTo < vm) then
  {$else}
  if (DateTimeToOADate(Certificate.ValidTo) < vm) then
  {$endif}
  begin
    Reason := Reason + [vrExpired];
    Result := cvInvalid;
  end;
  if Certificate.SelfSigned then
  begin
    Result := cvSelfSigned;
    Reason := Reason - [vrUnknownCA];
    if Certificate.Validate then
      Exit
    else
    begin
      Result := cvInvalid;
      Reason := Reason + [vrInvalidSignature];
    end;
  end;

  if FCRL <> nil then
  begin
    if FCRL.IsPresent(Certificate) then
    begin
      result := cvInvalid;
      Reason := Reason + [vrRevoked];
      exit;
    end;
  end;

  FSharedResource.WaitToRead;
  try
    for I := 0 to GetCount - 1 do
    begin
      CACert := TElX509Certificate(GetCertificates(I));
      if CompareRDN(Certificate.IssuerRDN, CACert.SubjectRDN) then
      begin
        if (Certificate.ValidateWithCA(CACert)) then
        begin
          Reason := Reason - [vrInvalidSignature];
          Reason := Reason - [vrUnknownCA];
          Result := cvOk;
          Exit;
        end
        else
        begin
          Reason := Reason + [vrInvalidSignature];
          Reason := Reason - [vrUnknownCA];
          Result := cvInvalid;
        end;
      end
      else
        if (Certificate.CertificateSize = CACert.CertificateSize) and
        (CompareMem(Certificate.CertificateBinary, CACert.CertificateBinary
, CACert.CertificateSize)) then
      begin
        Reason := Reason - [vrInvalidSignature];
        Reason := Reason - [vrUnknownCA];
        Result := cvOk;
        Exit;
      end;
    end;
  finally
    FSharedResource.Done;
  end;

  if vrUnknownCA in Reason then
    Result := cvInvalid;
end;

function TElCustomCertStorage.GetCount: integer;
begin
  Result := 0;
end;

function TElCustomCertStorage.GetCertificates(Index: integer):
TElX509Certificate;
begin
  Result := nil;
end;

procedure TElCustomCertStorage.Add(Certificate: TElX509Certificate;
  CopyPrivateKey: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif});
begin
  FRebuildChains := true;
end;

procedure TElCustomCertStorage.Remove(Index: integer);
begin
end;

procedure TElCustomCertStorage.ExportTo(Storage: TElCustomCertStorage);
var
  I, Cnt: integer;
begin
  CheckLicenseKey();
  Cnt := GetCount;
  for I := 0 to Cnt - 1 do
    Storage.Add(Certificates[I]{$ifndef HAS_DEF_PARAMS}, true{$endif});
end;

function TElCustomCertStorage.IndexOf(Certificate: TElX509Certificate): Integer;
begin
  CheckLicenseKey();
  for Result := 0 to Count - 1 do
  begin
    with Certificates[Result] do
    begin
      if (CertificateSize = Certificate.CertificateSize)
      and 
      (CompareMem(CertificateBinary,
        Certificate.CertificateBinary,
        CertificateSize)) then
      begin
        exit;
      end;
    end;
  end;
  Result := -1;
end;

function TElCustomCertStorage.IsPresent(Certificate: TElX509Certificate):
  boolean;
begin
  Result := IndexOf(Certificate) >= 0;
end;

function TElCustomCertStorage.LoadFromStreamPKCS7(Stream: TStream; Count: integer = 0): integer;
var
  Buf: ByteArray;
begin
  if Count = 0 then
    Count := Stream.Size - Stream.Position
  else
    Count := Min(Count, integer(Stream.Size - Stream.Position));
  SetLength(Buf, Count);
  Stream.Read(Buf[0], Count);
  Result := LoadFromBufferPKCS7(@Buf[0], Length(Buf));
end;

function TElCustomCertStorage.SaveToStreamPKCS7(Stream: TStream): boolean;
var
  Buf: ByteArray;
  Sz: integer;
begin
  Sz := 0;
  SaveToBufferPKCS7(nil, Sz);
  SetLength(Buf, Sz);
  Result := SaveToBufferPKCS7(@Buf[0], Sz);
  if Result then
    Stream.Write(Buf[0], Sz);
end;

function TElCustomCertStorage.LoadFromStreamPFX(Stream: TStream; const Password:
  string; Count: integer = 0): integer;
var
  Buf: ByteArray;
begin
  if Count = 0 then
    Count := Stream.Size - Stream.Position
  else
    Count := Min(Count, integer(Stream.Size - Stream.Position));
  SetLength(Buf, Count);
  Stream.Read(Buf[0], Count);
  Result := LoadFromBufferPFX(@Buf[0], Length(Buf), Password);
end;

function TElCustomCertStorage.SaveToStreamPFX(Stream: TStream; const Password:
  string;
  KeyEncryptionAlgorithm: integer; CertEncryptionAlgorithm: integer): integer;
var
  Buf: ByteArray;
  Sz: integer;
begin
  Sz := 0;
  SaveToBufferPFX(nil, Sz, Password, KeyEncryptionAlgorithm,
    CertEncryptionAlgorithm);
  SetLength(Buf, Sz);
  Result := SaveToBufferPFX(@Buf[0], Sz, Password, KeyEncryptionAlgorithm,
    CertEncryptionAlgorithm);
  if Result = 0 then
    Stream.Write(Buf[0], Sz);
end;


function TElCustomCertStorage.LoadFromBufferPFX(Buffer: pointer; Size:
  integer; const Password: string): integer;
var
  Msg: TElPKCS12Message;
  I: integer;
begin
  CheckLicenseKey();
  Msg := TElPKCS12Message.Create;
  Msg.Password := Password;
  Result := Msg.LoadFromBuffer(Buffer, Size);
  if Result = 0 then
  begin
    for I := 0 to Msg.Certificates.Count - 1 do
      Add(Msg.Certificates.Certificates[I]{$ifndef HAS_DEF_PARAMS}, true{$endif});
  end;
  FreeAndNil(Msg);
end;


function TElCustomCertStorage.SaveToBufferPFX(Buffer: pointer; var Size:
  integer; const Password: string; KeyEncryptionAlgorithm: integer;
  CertEncryptionAlgorithm: integer): integer;
var
  Msg: TElPKCS12Message;
  I: integer;
begin
  CheckLicenseKey();
  Msg := TElPKCS12Message.Create;
  Msg.Password := Password;
  Msg.KeyEncryptionAlgorithm := KeyEncryptionAlgorithm;
  Msg.CertEncryptionAlgorithm := CertEncryptionAlgorithm;
  Msg.Iterations := 2048;
  for I := 0 to Count - 1 do
    Msg.Certificates.Add(Certificates[I]{$ifndef HAS_DEF_PARAMS}, true{$endif});
  Result := Msg.SaveToBuffer(Buffer, Size);
  FreeAndNil(Msg);
end;


{$ifndef FPC}
procedure TElCustomCertStorage.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineBinaryProperty('BinaryCertificates', ReadCertificatesProp,
    WriteCertificatesProp, IsCertificatesPropStored);
  Filer.DefineProperty('Certificates', ReadFakeCertificatesProp,
    nil, false);
end;

procedure TElCustomCertStorage.ReadCertificatesProp(Reader: TStream);
var
  BufSize: integer;
  Buf: pointer;
begin
  Reader.Read(BufSize, sizeof(BufSize));
  if BufSize > 0 then
  begin
    GetMem(Buf, BufSize);
    Reader.Read(PChar(Buf)^, BufSize);
    LoadFromBufferPFX(Buf, BufSize, 'Certificates');
    FreeMem(Buf);
  end
  else
    Clear;
end;

procedure TElCustomCertStorage.WriteCertificatesProp(Writer: TStream);
var
  Buf: Pointer;
  BufSize: integer;
begin
  BufSize := 0;
  SaveToBufferPFX(nil, BufSize, 'Certificates', SB_ALGORITHM_PBE_SHA1_RC4_128,
    SB_ALGORITHM_PBE_SHA1_RC4_128);
  if (BufSize > 0) then
  begin
    GetMem(Buf, BufSize);
    SaveToBufferPFX(Buf, BufSize, 'Certificates', SB_ALGORITHM_PBE_SHA1_RC4_128,
      SB_ALGORITHM_PBE_SHA1_RC4_128);
    Writer.Write(BufSize, SizeOf(BufSize));
    Writer.Write(PChar(Buf)^, BufSize);
    FreeMem(Buf);
  end
  else
  begin
    Writer.Write(BufSize, SizeOf(BufSize));
  end;
end;

function TElCustomCertStorage.IsCertificatesPropStored: boolean;
begin
  result := Count > 0;
end;
{$endif}

procedure TElCustomCertStorage.Clear;
begin
  while Count > 0 do
    Remove(0);
  FRebuildChains := true;
end;

function TElCustomCertStorage.FindByHash(Digest: TMessageDigest160): integer;
var
  M160: TMessageDigest160;
  I: integer;
  Cert: TElX509Certificate;
begin
  CheckLicenseKey();
  Result := -1;
  for I := 0 to GetCount - 1 do
  begin
    Cert := GetCertificates(I);
    Assert(Assigned(Cert));
    M160 := HashSHA1(Cert.CertificateBinary, 
      Cert.CertificateSize);
    if CompareMem(@M160, @Digest, 20) then
    begin
      Result := I;
      Break;
    end;
  end;
end;

function TElCustomCertStorage.FindByHash(Digest: TMessageDigest128): integer;
var
  M128: TMessageDigest128;
  I: integer;
  Cert: TElX509Certificate;
begin
  CheckLicenseKey();
  Result := -1;
  for I := 0 to GetCount - 1 do
  begin
    Cert := GetCertificates(I);
    Assert(Assigned(Cert));
    M128 := HashMD5(Cert.CertificateBinary, 
      Cert.CertificateSize);
    if CompareMem(@M128, @Digest, 16) then
    begin
      Result := I;
      Break;
    end;
  end;
end;

function TElCustomCertStorage.IsIssuerCertificate(Subject, Issuer:
  TElX509Certificate): boolean;
var
  IssuerRDN, SubjectRDN: TElRelativeDistinguishedName;
  I: integer;
  Lst: TStringList;
begin
  Result := false;
  IssuerRDN := Subject.IssuerRDN;
  SubjectRDN := Issuer.SubjectRDN;
  if IssuerRDN.Count <> SubjectRDN.Count then
    Exit;
  Lst := TStringList.Create;
  Result := true;
  for I := 0 to IssuerRDN.Count - 1 do
  begin
    SubjectRDN.GetValuesByOID(IssuerRDN.OIDs[I], Lst);
    if Lst.IndexOf(IssuerRDN.Values[I]) = -1 then
    begin
      Result := false;
      Break;
    end;
  end;
  
  Lst.Free;
  if not Result then
    Exit;

  if not (csoStrictChainBuilding in Options) then
    Exit;

  if (ceSubjectKeyIdentifier in Issuer.Extensions.Included) then
  begin
    if (ceAuthorityKeyIdentifier in Subject.Extensions.Included) then
      Result := CompareContent(
        Issuer.Extensions.SubjectKeyIdentifier.KeyIdentifier,
        Subject.Extensions.AuthorityKeyIdentifier.KeyIdentifier
        );
  end;
end;

function TElCustomCertStorage.GetIssuerCertificate(Certificate:
  TElX509Certificate): integer;
var
  I: integer;
  Cand: TElX509Certificate;
  Dgst1, Dgst2: TMessageDigest128;
begin
  CheckLicenseKey();
  Result := -1;
  for I := 0 to GetCount - 1 do
  begin
    Cand := Certificates[I];
    if (IsIssuerCertificate(Certificate, Cand)) then
    begin
      Dgst1 := Certificate.GetHashMD5;
      Dgst2 := Cand.GetHashMD5;
      if not CompareMem(@Dgst1, @Dgst2, 16) then
      begin
        Result := I;
        Break;
      end;
    end;
  end;
end;

function TElCustomCertStorage.LoadFromBufferPKCS7(Buffer: pointer; Size: longint) : integer;
var
  Mes: TElPKCS7Message;
  I: integer;
begin
  CheckLicenseKey();

  Mes := TElPKCS7Message.Create;
  try
    result := Mes.LoadFromBuffer(Buffer, Size);
    if result <> 0 then
      exit;

    if Mes.ContentType <> ctSignedData then
      raise EElCertStorageException.Create(SNoSignedData);
      
    // not using ExportTo here as it leads to deadlock if fcsoSaveOnChange is set
    FSharedResource.WaitToWrite;
    try
      for I := 0 to Mes.SignedData.Certificates.Count - 1 do
        Add(Mes.SignedData.Certificates.Certificates[I]{$ifndef HAS_DEF_PARAMS}, true{$endif});
    finally
      FSharedResource.Done;
    end;
  finally
    FreeAndNil(Mes);
  end;
end;

function TElCustomCertStorage.SaveToBufferPKCS7(Buffer: pointer; var Size: longint) : boolean;
var
  Mes: TElPKCS7Message;
begin
  CheckLicenseKey();
  FSharedResource.WaitToRead;
  Mes := TElPKCS7Message.Create;
  Mes.ContentType := ctSignedData;
  try
    Mes.SignedData.Version := 1;
    ExportTo(Mes.SignedData.Certificates);
    Result := Mes.SaveToBuffer(Buffer, Size);
  finally
    FSharedResource.Done;
    FreeAndNil(Mes);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TElFileCertStorage
////////////////////////////////////////////////////////////////////////////////


constructor TElFileCertStorage.Create(Owner: TComponent);
begin
  inherited Create(Owner);
  FCertificateList := TSBObjectList.Create;
  FLoaded := false;
  FAccessType := csatOnDemand;
  FSaveOptions :=  [];
end;


destructor TElFileCertStorage.Destroy;
var
  C : TElX509Certificate;
begin
  if fcsoSaveOnDestroy in
    FSaveOptions then
    Save;
  FSharedResource.WaitToWrite;
  try
    while FCertificateList.Count > 0 do
    begin
      C := TElX509Certificate(FCertificateList.Extract(FCertificateList[0]));
      FreeAndNil(C);
    end;
    FreeAndNil(FCertificateList);
  finally
    FSharedResource.Done;
  end;
  inherited;
end;


procedure TElFileCertStorage.Add(X509Certificate: TElX509Certificate;
  CopyPrivateKey: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif});
var
  NewX509: TElX509Certificate;
begin
  CheckLicenseKey();
  inherited;
  NewX509 := TElX509Certificate.Create(nil);
  X509Certificate.Clone(NewX509{$ifndef HAS_DEF_PARAMS}, true{$endif});
  FSharedResource.WaitToWrite;
  try
    FCertificateList.Add(NewX509);
  finally
    FSharedResource.Done;
  end;
  if (fcsoSaveOnChange in
    FSaveOptions) then
    Save;
end;

function TElFileCertStorage.GetCount: integer;
begin
  if not FLoaded then
    LoadFromFile;
  Result := FCertificateList.Count;
end;

function TElFileCertStorage.GetCertificates(Index: integer): TElX509Certificate;
begin
  if not FLoaded then
    LoadFromFile;
  FSharedResource.WaitToRead;
  try
    if Index < FCertificateList.Count then
      Result := TElX509Certificate(FCertificateList[Index])
    else
      Result := nil;
  finally
    FSharedResource.Done;
  end;
end;

procedure TElFileCertStorage.Remove(Index: integer);
var
  Cert: TElX509Certificate;
begin
  if not FLoaded then
    LoadFromFile;
  FSharedResource.WaitToRead;
  if (Index < FCertificateList.Count) and (Index >= 0) then
  begin
    FSharedResource.Done;
    FSharedResource.WaitToWrite;
    try
      Cert := TElX509Certificate(FCertificateList[Index]);
      FCertificateList.Delete(Index);
      if not FCertificateList.OwnsObjects then
        Cert.Free;
      FRebuildChains := true;
    finally
      FSharedResource.Done;
    end;
    if fcsoSaveOnChange in
      FSaveOptions then
      Save;
  end
  else
    FSharedResource.Done;
end;

procedure TElFileCertStorage.LoadFromFile;
var
  Size: longint;
  Buffer: ^Byte;
  InFile: TStream;
begin
  CheckLicenseKey();
  if FFileName = '' then
    Exit;
  if not FLoaded then
  begin
    InFile := nil;
    Buffer := nil;
    try
      InFile := TFileStream.Create(FFileName, fmOpenRead or fmShareDenyWrite);
      Size := InFile.Size;
      GetMem(Buffer, Size);
      InFile.Read(PChar(Buffer)^, Size);
      LoadFromBufferPKCS7(Buffer, Size);
    finally
      if Assigned(InFile) then
        InFile.Free;
        
      if Assigned(Buffer) then
        FreeMem(Buffer);
    end;
  end;
  FLoaded := true;
end;

function TElFileCertStorage.Validate(Certificate: TElX509Certificate;
  var Reason: TSBCertificateValidityReason; ValidityMoment:
    TDateTime = 0):
TSBCertificateValidity;
begin
  CheckLicenseKey();
  Result := cvInvalid;
  Reason :=  [vrBadData];

  if not FLoaded then
    LoadFromFile;
  if not FLoaded then exit;
  result := inherited Validate(Certificate, Reason, ValidityMoment);
end;

procedure TElFileCertStorage.SaveToFile(const FileName: string);
var
  Sz: integer;
  Buffer: ByteArray;
  F: TStream;
begin
  if Length(Filename) = 0 then
    Exit;
  Sz := 0;
  SetLength(Buffer, Sz);
  SaveToBufferPKCS7(@Buffer[0], Sz);
  SetLength(Buffer, Sz);
  if SaveToBufferPKCS7(@Buffer[0], Sz) then
  begin
    F := nil;
    try
      SetLength(Buffer, Sz);
      F := TFileStream.Create(FileName, fmCreate);
      F.Write(Buffer[0], Length(Buffer));
    finally
      if Assigned(F) then
        F.Free;
    end;
  end;
end;

procedure TElFileCertStorage.SetFileName(const FileName: string);
begin
  if CompareStr(FileName, FFilename) <> 0 then
  begin
    if (fcsoSaveOnFilenameChange in
      FSaveOptions) and (Length(FFilename) > 0)
    and (not (csDesigning in ComponentState))
 then
      Save;
    InternalClear;
    FFileName := FileName;
    FLoaded := false;
    if not FileExists(Filename) then
      CreateEmptyStorage;
    if (FAccessType = csatImmediate)
    and (not (csDesigning in ComponentState))
 then
      Reload;
  end;
end;

procedure TElFileCertStorage.Reload;
begin
  CheckLicenseKey();
  if FLoaded then
  begin
    InternalClear;
    FLoaded := false;
  end;
  LoadFromFile;
end;

procedure TElFileCertStorage.Save;
begin
  if csDesigning in ComponentState then
    Exit;
  try
    if not FLoaded then
      LoadFromFile;
  except
    // saving if failed to load file
    FLoaded := true;
  end;
  if FLoaded then
    SaveToFile(FFilename);
end;

procedure TElFileCertStorage.SetAccessType(Value: TSBFileCertStorageAccessType);
begin
  if FAccessType <> Value then
  begin
    FAccessType := Value;
    if (Value = csatImmediate) and 
       (not FLoaded)
    and (not (csDesigning in ComponentState))
 then
      LoadFromFile;
  end;
end;

procedure TElFileCertStorage.InternalClear;
begin
  FSharedResource.WaitToWrite;
  try
    while FCertificateList.Count > 0 do
      FCertificateList.Extract(FCertificateList[0]);
  finally
    FSharedResource.Done;
  end;
end;

procedure TElFileCertStorage.Clear;
begin
  InternalClear;
  if (fcsoSaveOnChange in
    FSaveOptions) then
    Save;
end;

procedure TElFileCertStorage.CreateEmptyStorage;
begin
  InternalClear;
  try
    FLoaded := true;
    SaveToFile(FFilename);
  except
    raise EElCertStorageException.Create(SUnableToMountStorage);
  end;
end;


////////////////////////////////////////////////////////////////////////////////
// TElMemoryCertStorage
////////////////////////////////////////////////////////////////////////////////


constructor TElMemoryCertStorage.Create(Owner: TComponent);
begin
  inherited;
  FCertificateList := TSBObjectList.Create;
  FCertificateList.OwnsObjects := false;
end;


destructor TElMemoryCertStorage.Destroy;
var
  P: TElX509Certificate;
begin
  while FCertificateList.Count > 0 do
  begin
    P := TElX509Certificate(FCertificateList.Items[0]);
    FCertificateList.Extract(FCertificateList.Items[0]);
    P.Free;
  end;
  FreeAndNil(FCertificateList);
  inherited;
end;


procedure TElMemoryCertStorage.Add(X509Certificate: TElX509Certificate;
  CopyPrivateKey: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif});
var
  NewX509: TElX509Certificate;
begin
  CheckLicenseKey();
  FSharedResource.WaitToWrite;
  try
    inherited;
    NewX509 := TElX509Certificate.Create(nil);
    X509Certificate.Clone(NewX509, CopyPrivateKey);
    FCertificateList.Add(NewX509);
  finally
    FSharedResource.Done;
  end;
end;

procedure TElMemoryCertStorage.Remove(Index: integer);
var
  Cert: TElX509Certificate;
begin
  CheckLicenseKey();
  FSharedResource.WaitToWrite;
  try
    if Index < FCertificateList.Count then
    begin
      Cert := TElX509Certificate(FCertificateList.Items[Index]);
      FCertificateList.Delete(Index);
      if not FCertificateList.OwnsObjects then
        Cert.Free;
    end;
    FRebuildChains := true;
  finally
    FSharedResource.Done;
  end;
end;

function TElMemoryCertStorage.GetCertificates(Index: integer):
TElX509Certificate;
begin
  CheckLicenseKey();
  FSharedResource.WaitToRead;
  try
    if (Index < 0) or (Index >= FCertificateList.Count) then
      Result := nil
    else
      Result := TElX509Certificate(FCertificateList[Index]);
  finally
    FSharedResource.Done;
  end;
end;

function TElMemoryCertStorage.GetCount: integer;
begin
  Result := FCertificateList.Count;
end;


procedure TElCustomCertStorage.AssignTo(Dest: TPersistent);
var
  i: integer;
begin
  if Dest is TElCustomCertStorage then
    for i := 0 to Count - 1 do
    begin
      TElCustomCertStorage(Dest).Add(Certificates[i]{$ifndef HAS_DEF_PARAMS}, true{$endif});
    end
  else
    inherited;
end;

class function TElCustomCertStorage.ReadOnly: Boolean;
begin
  Result := false;
end;

procedure TElCustomCertStorage.BuildAllChains;
var
  I, Index: integer;
  IssuerFlags: array of boolean;
begin
  FSharedResource.WaitToRead;
  SetLength(IssuerFlags, Count);
  for I := 0 to Count - 1 do
    IssuerFlags[I] := false;
  for I := 0 to Count - 1 do
  begin
    Index := GetIssuerCertificate(Certificates[I]);
    if Index >= 0 then
      IssuerFlags[Index] := true;
  end;
  SetLength(FChains, 0);
  for I := 0 to Count - 1 do
    if not IssuerFlags[I] then
    begin
      Index := Length(FChains);
      SetLength(FChains, Index + 1);
      FChains[Index] := I;
    end;
  FRebuildChains := false;
  FSharedResource.Done;
end;

function TElCustomCertStorage.GetChainCount: integer;
begin
  if FRebuildChains then
    BuildAllChains;
  Result := Length(FChains);
end;

function TElCustomCertStorage.GetChain(Index: integer): integer;
begin
  if FRebuildChains then
    BuildAllChains;
  if (Index >= 0) and (Index < Length(FChains)) then
    Result := FChains[Index]
  else
    Result := -1;
end;

{$ifndef FPC}
procedure TElCustomCertStorage.ReadFakeCertificatesProp(Reader: TReader);
var
  BufSize: integer;
  Buf: pointer;
begin
  BufSize := Reader.ReadInteger;
  if BufSize > 0 then
  begin
    GetMem(Buf, BufSize);
    Reader.Read(PChar(Buf)^, BufSize);
    LoadFromBufferPFX(Buf, BufSize, 'Certificates');
    FreeMem(Buf);
  end
  else
    Clear;
end;
{$endif}

function TElCustomCertStorage.FindFirst(Lookup: TElCertificateLookup): integer;
begin
  Lookup.FLastIndex := -1;
  Result := FindNext(Lookup);
end;

function TElCustomCertStorage.FindNext(Lookup: TElCertificateLookup): integer;
begin
  Result := Lookup.FindNext(Self);
end;

procedure TElCustomCertStorage.Notification(AComponent: TComponent; Operation:
  TOperation);
begin
  inherited;
  if Operation = opRemove then
  begin
    if (AComponent = FCRL) then
      CRL := nil;
  end;
end;

procedure TElCustomCertStorage.SetCRL(Value: TElCertificateRevocationList);
begin
  if FCRL <> Value then
  begin
    {$IFDEF VCL50}
    if (FCRL <> nil) and (not (csDestroying in FCRL.ComponentState)) then
      FCRL.RemoveFreeNotification(Self);
	{$ENDIF}
    FCRL := Value;
    if FCRL <> nil then
      FCRL.FreeNotification(Self);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TElCertificateLookup class


constructor TElCertificateLookup.Create(AOwner: TComponent);
begin
  inherited;
  FLastIndex := -1;
  FIssuerRDN := TElRelativeDistinguishedName.Create;
  FSubjectRDN := TElRelativeDistinguishedName.Create;
  FEmailAddresses := TStringList.Create;
end;


destructor TElCertificateLookup.Destroy;
begin
  FIssuerRDN.Free;
  FSubjectRDN.Free;
  FEmailAddresses.Free;
  inherited;
end;


procedure TElCertificateLookup.SetCriteria(Value: TSBLookupCriteria);
begin
  FCriteria := Value;
  FLastIndex := -1;
end;

function NonstrictCompareRDN(InnerRDN, OuterRDN: TElRelativeDistinguishedName):
  boolean;
var
  I, J: integer;
  Found: boolean;
  OID, Value: BufferType;
  TagID: byte;
begin
  Result := false;
  for I := 0 to InnerRDN.Count - 1 do
  begin
    OID := InnerRDN.OIDs[I];
    Value := InnerRDN.Values[I];
    TagID := InnerRDN.Tags[I];
    Found := false;
    for J := 0 to OuterRDN.Count - 1 do
    begin
      if (CompareStr(OuterRDN.OIDs[J], OID) = 0) and
        (CompareStr(OuterRDN.Values[J], Value) = 0) and
        (OuterRDN.Tags[J] = TagID) then
      begin
        Found := true;
        Break;
      end;
    end;
    if not Found then
      Exit;
  end;
  Result := true;
end;

function CompareBufferType(const B1, B2: BufferType): boolean;
begin
  Result := CompareStr(B1, B2) = 0;
end;

function TElCertificateLookup.FindNext(Storage: TElCustomCertStorage): integer;
var
  Index: integer;
  Cert: TElX509Certificate;
  B, MatchOne: boolean;
  I, K: integer;
begin
  CheckLicenseKey();
  Index := FLastIndex + 1;
  Result := -1;
  while Index < Storage.Count do
  begin
    FLastIndex := Index;
    Cert := Storage.Certificates[Index];
    MatchOne := false;
    Inc(Index);
    // 1. Issuer
    if lcIssuer in FCriteria then
    begin
      if loExactMatch in FOptions then
        B := CompareRDN(FIssuerRDN, Cert.IssuerRDN)
      else
        B := NonstrictCompareRDN(FIssuerRDN, Cert.IssuerRDN);
      if (not B) and (loMatchAll in FOptions) then
        Continue;
      MatchOne := MatchOne or B;
    end;
    // 2. Subject
    if lcSubject in FCriteria then
    begin
      if loExactMatch in FOptions then
        B := CompareRDN(FSubjectRDN, Cert.SubjectRDN)
      else
        B := NonstrictCompareRDN(FSubjectRDN, Cert.SubjectRDN);
      if (not B) and (loMatchAll in FOptions) then
        Continue;
      MatchOne := MatchOne or B;
    end;
    // 3. ValidFrom
    if lcValidity in FCriteria then
    begin
      if FDateLookupOptions = 
        [dloBefore] then
        B := (Cert.ValidFrom <= FValidFrom) and (Cert.ValidTo <= FValidFrom)
      else
        if FDateLookupOptions =  [dloBefore,
          dloBetween] then
        B := (Cert.ValidFrom <= FValidFrom) and (Cert.ValidTo >= FValidFrom)
      else
        if FDateLookupOptions =  [dloBefore, dloBetween,
          dloAfter] then
        B := (Cert.ValidFrom <= FValidFrom) and (Cert.ValidTo >= FValidTo)
      else
        if FDateLookupOptions =  [dloBetween,
          dloAfter] then
        B := (Cert.ValidFrom >= FValidFrom) and (Cert.ValidTo >= FValidTo)
      else
        if FDateLookupOptions = 
          [dloBetween] then
        B := (Cert.ValidFrom >= FValidFrom) and (Cert.ValidTo <= FValidTo)
      else
        if FDateLookupOptions =  [dloBefore,
          dloAfter] then
        B := (Cert.ValidTo <= FValidFrom) or (Cert.ValidFrom >= FValidTo)
      else
        if FDateLookupOptions = 
          [dloAfter] then
        B := (Cert.ValidFrom >= FValidTo) and (Cert.ValidTo >= FValidTo)
      else
        B := true;
      if (not B) and (loMatchAll in FOptions) then
        Continue;
      MatchOne := MatchOne or B;
    end;
    // 5. Public Key Algorithm
    if lcPublicKeyAlgorithm in FCriteria then
    begin
      B := Cert.PublicKeyAlgorithm = FPublicKeyAlgorithm;
      if (not B) and (loMatchAll in FOptions) then
        Continue;
      MatchOne := MatchOne or B;
    end;
    // 6. Signature algorithm
    if lcSignatureAlgorithm in FCriteria then
    begin
      B := Cert.SignatureAlgorithm = FSignatureAlgorithm;
      if (not B) and (loMatchAll in FOptions) then
        Continue;
      MatchOne := MatchOne or B;
    end;
    // 7. PublicKeySize
    if lcPublicKeySize in FCriteria then
    begin
      if FKeySizeLookupOption = ksloSmaller then
        B := Cert.GetPublicKeySize <= FPublicKeySizeMin
      else
        if FKeySizeLookupOption = ksloGreater then
        B := Cert.GetPublicKeySize >= FPublicKeySizeMax
      else
        if FKeySizeLookupOption = ksloBetween then
        B := (Cert.GetPublicKeySize <= FPublicKeySizeMax) and
          (Cert.GetPublicKeySize >= FPublicKeySizeMin)
      else
        B := false;
      if (not B) and (loMatchAll in FOptions) then
        Continue;
      MatchOne := MatchOne or B;
    end;
    // 8. Auth key id
    if lcAuthorityKeyIdentifier in FCriteria then
    begin
      B := (ceAuthorityKeyIdentifier in Cert.Extensions.Included) and
      (CompareBufferType(FAuthorityKeyIdentifier,
        Cert.Extensions.AuthorityKeyIdentifier.KeyIdentifier));
      if (not B) and (loMatchAll in FOptions) then
        Continue;
      MatchOne := MatchOne or B;
    end;
    // 9. Subject key id
    if lcSubjectKeyIdentifier in FCriteria then
    begin
      B := (ceSubjectKeyIdentifier in Cert.Extensions.Included) and
      (CompareBufferType(FSubjectKeyIdentifier,
        Cert.Extensions.SubjectKeyIdentifier.KeyIdentifier));
      if (not B) and (loMatchAll in FOptions) then
        Continue;
      MatchOne := MatchOne or B;
    end;
    // 10. KeyUsage
    if lcKeyUsage in FCriteria then
    begin
      if kuloMatchAll in FKeyUsageLookupOptions then
        B := (ceKeyUsage in Cert.Extensions.Included) and
          (kuDigitalSignature in FKeyUsage) and
            (Cert.Extensions.KeyUsage.DigitalSignature) and
          (kuNonRepudiation in FKeyUsage) and
            (Cert.Extensions.KeyUsage.NonRepudiation) and
          (kuKeyEncipherment in FKeyUsage) and
            (Cert.Extensions.KeyUsage.KeyEncipherment) and
          (kuDataEncipherment in FKeyUsage) and
            (Cert.Extensions.KeyUsage.DataEncipherment) and
          (kuKeyAgreement in FKeyUsage) and
            (Cert.Extensions.KeyUsage.KeyAgreement) and
          (kuKeyCertSign in FKeyUsage) and (Cert.Extensions.KeyUsage.KeyCertSign)
            and
          (kuCRLSign in FKeyUsage) and (Cert.Extensions.KeyUsage.CRLSign) and
          (kuEncipherOnly in FKeyUsage) and
            (Cert.Extensions.KeyUsage.EncipherOnly) and
          (kuDecipherOnly in FKeyUsage) and
            (Cert.Extensions.KeyUsage.DecipherOnly)
      else
        B := (ceKeyUsage in Cert.Extensions.Included) and
          ((kuDigitalSignature in FKeyUsage) and
            (Cert.Extensions.KeyUsage.DigitalSignature)) or
          ((kuNonRepudiation in FKeyUsage) and
            (Cert.Extensions.KeyUsage.NonRepudiation)) or
          ((kuKeyEncipherment in FKeyUsage) and
            (Cert.Extensions.KeyUsage.KeyEncipherment)) or
          ((kuDataEncipherment in FKeyUsage) and
            (Cert.Extensions.KeyUsage.DataEncipherment)) or
          ((kuKeyAgreement in FKeyUsage) and
            (Cert.Extensions.KeyUsage.KeyAgreement)) or
          ((kuKeyCertSign in FKeyUsage) and
            (Cert.Extensions.KeyUsage.KeyCertSign)) or
          ((kuCRLSign in FKeyUsage) and (Cert.Extensions.KeyUsage.CRLSign)) or
          ((kuEncipherOnly in FKeyUsage) and
            (Cert.Extensions.KeyUsage.EncipherOnly)) or
          ((kuDecipherOnly in FKeyUsage) and
            (Cert.Extensions.KeyUsage.DecipherOnly));
      if (not B) and (loMatchAll in FOptions) then
        Continue;
      MatchOne := MatchOne or B;
    end;
    // 11. E-mail address
    if lcEmail in FCriteria then
    begin
      B := false;
      for I := 0 to FEmailAddresses.Count - 1 do
      begin
        if CompareText(Cert.SubjectName.EMailAddress, FEmailAddresses[I]) = 0
          then
        begin
          B := true;
          Break;
        end;
      end;
      if not B then
      begin
        if ceSubjectAlternativeName in Cert.Extensions.Included then
        begin
          for K := 0 to Cert.Extensions.SubjectAlternativeName.Content.Count - 1 do
          begin
            if Cert.Extensions.SubjectAlternativeName.Content.Names[K].RFC822Name <> '' then
            begin
              for I := 0 to FEmailAddresses.Count - 1 do
              begin
                if
                  (CompareText(Cert.Extensions.SubjectAlternativeName.Content.Names[K].RFC822Name,
                  FEmailAddresses[I]) = 0) then
                begin
                  B := true;
                  Break;
                end;
              end;
              if B then
                Break;
            end;
          end;
        end;
      end;
      if (not B) and (loMatchAll in FOptions) then
        Continue;
      MatchOne := MatchOne or B;
    end;
    // 12. Serial number
    if lcSerialNumber in FCriteria then
    begin
      B := CompareContent(Cert.SerialNumber, FSerialNumber);
      if (not B) and (loMatchAll in FOptions) then
        Continue;
      MatchOne := MatchOne or B;
    end;

    if MatchOne then
    begin
      Result := Index - 1;
      Break;
    end;
  end;
end;


end.

