(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBX509Ext;

interface

uses
  Classes,
  SysUtils,
  SBRDN,
  SBUtils,
  SBConstants,
  SBASN1,
  SBPKCS7Utils,
  SBASN1Tree;



type
  TSBCertificateExtension = (ceAuthorityKeyIdentifier, ceSubjectKeyIdentifier,
    ceKeyUsage, cePrivateKeyUsagePeriod, ceCertificatePolicies,
      cePolicyMappings,
    ceSubjectAlternativeName, ceIssuerAlternativeName, ceBasicConstraints,
    ceNameConstraints, cePolicyConstraints, ceExtendedKeyUsage,
    ceCRLDistributionPoints, ceAuthorityInformationAccess,

    ceNetscapeCertType, ceNetscapeBaseURL, ceNetscapeRevokeURL,
    ceNetscapeCARevokeURL, ceNetscapeRenewalURL, ceNetscapeCAPolicyURL,
    ceNetscapeServerName, ceNetscapeComment, ceCommonName,

    ceSubjectDirectoryAttributes);

  TSBCertificateExtensions = set of TSBCertificateExtension;

  TSBKeyUsageType = (kuDigitalSignature, kuNonRepudiation, kuKeyEncipherment,
    kuDataEncipherment, kuKeyAgreement, kuKeyCertSign, kuCRLSign,
    kuEncipherOnly, kuDecipherOnly);
  TSBKeyUsage = set of TSBKeyUsageType;

  TSBCRLReasonFlag = (rfUnspecified, rfKeyCompromise, rfCACompromise,
    rfAffiliationChanged, rfSuperseded, rfCessationOfOperation,
      rfCertificateHold, rfObsolete1, rfRemoveFromCRL, rfPrivilegeWithdrawn, rfAACompromise);
  TSBCRLReasonFlags = set of TSBCRLReasonFlag;

type

  EElCertificateException =  class(Exception);

  { X.509 Certificate Extensions classes }

  TSBGeneralName =  (gnRFC822Name, gnDNSName, gnDirectoryName, gnEdiPartyName,
    gnUniformResourceIdentifier, gnIPAddress, gnRegisteredID, gnOtherName, gnUnknown, gnPermanentIdentifier);
    
  TElEDIPartyName =  class(TPersistent)
  private
    FNameAssigner : string;
    FPartyName : string;
  public

    property NameAssigner : string read FNameAssigner write FNameAssigner;
    property PartyName : string read FPartyName write FPartyName;
  end;

  TElOtherName =  class(TPersistent)
  private
    FOID : BufferType;
    FValue : BufferType;
  public

    property OID : BufferType read FOID write FOID;
    property Value : BufferType read FValue write FValue;
  end;

  TElPermanentIdentifier = class(TPersistent)
  private
    FPermanentIdentifier: BufferType;
    FAssigner : BufferType;
  public
  
    property PermanentIdentifier : BufferType read FPermanentIdentifier write FPermanentIdentifier;
    property Assigner : BufferType read FAssigner write FAssigner;
  end;

  TElGeneralName =
     class(TPersistent)
  protected
    FRFC822Name: string;
    FDNSName: string;
    FDirectoryName: TElRelativeDistinguishedName;
    FEdiPartyName: TElEDIPartyName;
    FUniformResourceIdentifier: string;
    FIpAddress: string;
    FRegisteredID: BufferType;
    FOtherName : TElOtherName;
    FPermanentIdentifier: TElPermanentIdentifier;
    FNameType : TSBGeneralName;
    function GetIsEmpty : boolean;
    procedure TryKnownOtherNames;
    procedure SaveKnownOtherNames;
    procedure ParsePermanentIdentifier(Buffer: pointer; Size: integer);
    procedure SavePermanentIdentifier(var OID : BufferType; var Content: BufferType);
  public
    constructor Create;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent);
     override;
    procedure AssignTo(Dest: TPersistent);
     override;
    function LoadFromTag(Tag: TElASN1CustomTag): boolean;
    function SaveToTag(Tag: TElASN1SimpleTag): boolean;
    property RFC822Name: string read FRFC822Name write FRFC822Name;
    property DNSName: string read FDNSName write FDNSName;
    property DirectoryName: TElRelativeDistinguishedName read FDirectoryName;
    property EdiPartyName: TElEDIPartyName read FEdiPartyName;
    property UniformResourceIdentifier: string read FUniformResourceIdentifier write FUniformResourceIdentifier;
    property IpAddress: string read FIpAddress write FIpAddress;
    property RegisteredID: BufferType read FRegisteredID write FRegisteredID;
    property OtherName : TElOtherName read FOtherName;
    property PermanentIdentifier : TElPermanentIdentifier read FPermanentIdentifier;
    property NameType : TSBGeneralName read FNameType write FNameType;
    property IsEmpty : boolean read GetIsEmpty;
  end;

  TElGeneralNames =
     class(TPersistent)
  private
    FNames : TList;
  protected
    function GetCount : integer;
    function GetNames(Index: integer): TElGeneralName;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent);
     override;
    procedure AssignTo(Dest: TPersistent);
     override;
    function Add : integer;
    procedure Remove(Index: integer);
    procedure Clear;
    function ContainsEmailAddress(const Addr: string): boolean;
    function FindNameByType(NameType : TSBGeneralName; StartIndex: integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}) : integer;
    function LoadFromTag(Tag: TElASN1ConstrainedTag; AllowRDN : boolean {$ifdef HAS_DEF_PARAMS}= false{$endif}): boolean;
    function SaveToTag(Tag: TElASN1ConstrainedTag): boolean;
    property Names[Index: integer] : TElGeneralName read GetNames;
    property Count: integer read GetCount;
  end;

  TElCustomExtension =
     class(TPersistent)
  protected
    FCritical: boolean;
    FOID: BufferType;
    FValue: BufferType;
    procedure Clear; virtual;
    function GetOID: BufferType; virtual;
    procedure SetOID(const Value: BufferType); virtual;
    procedure SetValue(const Value: BufferType); virtual;
    function GetValue: BufferType; virtual;
    procedure RaiseInvalidExtensionError;
  public
    constructor Create;
    procedure SaveToTag(Tag: TElASN1ConstrainedTag); virtual;
    property Critical: boolean read FCritical write FCritical default false;
    property OID: BufferType read GetOID write SetOID;
    property Value: BufferType read GetValue write SetValue;
  end;

  TElAuthorityKeyIdentifierExtension =  class(TElCustomExtension)
  protected
    FKeyIdentifier: BufferType;
    FAuthorityCertIssuer: TElGeneralNames;
    FAuthorityCertSerial: BufferType;

    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
  public
    constructor Create;
    destructor Destroy; override;

    property KeyIdentifier: BufferType read FKeyIdentifier write FKeyIdentifier;
    property AuthorityCertIssuer: TElGeneralNames read FAuthorityCertIssuer;
    property AuthorityCertSerial: BufferType read FAuthorityCertSerial write
      FAuthorityCertSerial;
  end;

  TElSubjectKeyIdentifierExtension =  class(TElCustomExtension)
  protected
    FKeyIdentifier: BufferType;

    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
  public
    property KeyIdentifier: BufferType read FKeyIdentifier write FKeyIdentifier;
  end;

  TElKeyUsageExtension =  class(TElCustomExtension)
  protected
    FDigitalSignature: boolean;
    FNonRepudiation: boolean;
    FKeyEncipherment: boolean;
    FDataEncipherment: boolean;
    FKeyAgreement: boolean;
    FKeyCertSign: boolean;
    FCRLSign: boolean;
    FEncipherOnly: boolean;
    FDecipherOnly: boolean;

    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
  public
    property DigitalSignature: boolean read FDigitalSignature write
      FDigitalSignature;
    property NonRepudiation: boolean read FNonRepudiation write FNonRepudiation;
    property KeyEncipherment: boolean read FKeyEncipherment write
      FKeyEncipherment;
    property DataEncipherment: boolean read FDataEncipherment write
      FDataEncipherment;
    property KeyAgreement: boolean read FKeyAgreement write FKeyAgreement;
    property KeyCertSign: boolean read FKeyCertSign write FKeyCertSign;
    property CRLSign: boolean read FCRLSign write FCRLSign;
    property EncipherOnly: boolean read FEncipherOnly write FEncipherOnly;
    property DecipherOnly: boolean read FDecipherOnly write FDecipherOnly;
  end;

  TElPrivateKeyUsagePeriodExtension =  class(TElCustomExtension)
  protected
    FNotBefore: TDateTime;
    FNotAfter: TDateTime;

    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
  public
    property NotBefore: TDateTime read FNotBefore write FNotBefore;
    property NotAfter: TDateTime read FNotAfter write FNotAfter;
  end;

  {JPM - added NetscapeExtensions}
  TElNetscapeCertTypeFlag = (nsSSLClient, nsSSLServer, nsSMIME, nsObjectSign,
    nsSSLCA,  nsSMIMECA, nsObjectSignCA);
  TElNetscapeCertType = set of TElNetscapeCertTypeFlag;

  TElNetscapeCertTypeExtension =  class(TElCustomExtension)
  protected
    FCertType : TElNetscapeCertType;
    procedure SetValue(const Value: BufferType); override;
    procedure Clear; override;
  public
    property CertType : TElNetscapeCertType read FCertType write FCertType;
  end;

  TElNetscapeString =  class(TElCustomExtension)
  protected
    FContent : string;
    procedure SetValue(const Value: BufferType); override;
    procedure Clear; override;
  public
    property Content : string read FContent write FContent;
  end;

  TElNetscapeBaseURL =  class(TElNetscapeString);
  TElNetscapeRevokeURL =  class(TElNetscapeString);
  TElNetscapeCARevokeURL =  class(TElNetscapeString);
  TElNetscapeRenewalURL =  class(TElNetscapeString);
  TElNetscapeCAPolicy =  class(TElNetscapeString);
  TElNetscapeServerName =  class(TElNetscapeString);
  TElNetscapeComment =  class(TElNetscapeString);

  TElCommonName =  class(TElNetscapeString);  //not a netscape extension but the coding is similar to the others
  // end
  
  TElUserNotice =
     class(TPersistent)
  protected
    FOrganization: string;
    FNoticeNumbers: array of integer;
    FExplicitText: string;
    function GetNoticeNumbers(Index: integer): integer;
    procedure SetNoticeNumbers(Index: integer; Value: integer);
    function GetNoticeNumbersCount: integer;
    procedure SetNoticeNumbersCount(Value: integer);
  public
  
    property Organization: string read FOrganization write FOrganization;
    property NoticeNumbers[Index: integer]: integer read GetNoticeNumbers
    write SetNoticeNumbers;
    property NoticeNumbersCount: integer read GetNoticeNumbersCount write
    SetNoticeNumbersCount;
    property ExplicitText: string read FExplicitText write FExplicitText;
  end;

  TElSinglePolicyInformation =
     class(TPersistent)
  protected
    FPolicyIdentifier: BufferType;
    FCPSURI: string;
    FUserNotice: TElUserNotice;
  public
    constructor Create;
    destructor Destroy; override;
    property PolicyIdentifier: BufferType read FPolicyIdentifier write
      FPolicyIdentifier;
    property CPSURI: string read FCPSURI write FCPSURI;
    property UserNotice: TElUserNotice read FUserNotice;
  end;

  TElCertificatePoliciesExtension =  class(TElCustomExtension)
  protected
    FList: TList;
    function GetPolicyInformation(Index: integer): TElSinglePolicyInformation;
    function GetCount: integer;
    procedure SetCount(Value: integer);
    procedure ClearList;

    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Remove(Index: integer);
    property PolicyInformation[Index: integer]: TElSinglePolicyInformation
    read GetPolicyInformation;
    property Count: integer read GetCount write SetCount;
  end;

  TElPolicyMapping =
     class(TPersistent)
  protected
    FIssuerDomainPolicy: BufferType;
    FSubjectDomainPolicy: BufferType;
  public
  
    property IssuerDomainPolicy: BufferType read FIssuerDomainPolicy write
      FIssuerDomainPolicy;
    property SubjectDomainPolicy: BufferType read FSubjectDomainPolicy write
      FSubjectDomainPolicy;
  end;

  TElPolicyMappingsExtension =  class(TElCustomExtension)
  protected
    FList: TList;
    function GetPolicies(Index: integer): TElPolicyMapping;
    function GetCount: integer;
    procedure SetCount(Value: integer);
    procedure ClearList;

    function GetOID: BufferType; override;
    procedure Clear; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Remove(Index: integer);
    property Count: integer read GetCount write SetCount;
    property Policies[Index: integer]: TElPolicyMapping
    read GetPolicies;
  end;

  TElAlternativeNameExtension =  class(TElCustomExtension)
  protected
    FContent: TElGeneralNames;

    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
  public
    constructor Create;
    destructor Destroy; override;
    property Content: TElGeneralNames read FContent;
  end;

  TElBasicConstraintsExtension =  class(TElCustomExtension)
  protected
    FCA: boolean;
    FPathLenConstraint: integer;

    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
  public
    property CA: boolean read FCA write FCA;
    property PathLenConstraint: integer read FPathLenConstraint write
    FPathLenConstraint;
  end;

  TElNameConstraint =
     class(TPersistent)
  protected
    FBase: TElGeneralName;
    FMinimum: Integer;
    FMaximum: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    property Base: TElGeneralName read FBase;
    property Minimum: Integer read FMinimum write FMinimum;
    property Maximum: Integer read FMaximum write FMaximum;
  end;

  TElNameConstraintsExtension =  class(TElCustomExtension)
  protected
    FPermittedList: TList;
    FExcludedList: TList;
    function GetPermittedSubtrees(Index: integer): TElNameConstraint;
    function GetExcludedSubtrees(Index: integer): TElNameConstraint;
    function GetPermittedCount: integer;
    function GetExcludedCount: integer;
    procedure SetPermittedCount(Value: integer);
    procedure SetExcludedCount(Value: integer);
    procedure ClearList;

    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
  public
    constructor Create;
    destructor Destroy; override;
    procedure RemovePermitted(Index: integer);
    procedure RemoveExcluded(Index: integer);
    property PermittedSubtrees[Index: integer]: TElNameConstraint read
    GetPermittedSubtrees;
    property ExcludedSubtrees[Index: integer]: TElNameConstraint read
    GetExcludedSubtrees;
    property PermittedCount: integer read GetPermittedCount write
      SetPermittedCount;
    property ExcludedCount: integer read GetExcludedCount write
      SetExcludedCount;
  end;

  TElPolicyConstraintsExtension =  class(TElCustomExtension)
  protected
    FRequireExplicitPolicy: integer;
    FInhibitPolicyMapping: integer;

    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
  public
    property RequireExplicitPolicy: integer read FRequireExplicitPolicy
    write FRequireExplicitPolicy;
    property InhibitPolicyMapping: integer read FInhibitPolicyMapping
    write FInhibitPolicyMapping;
  end;

  TElExtendedKeyUsageExtension =  class(TElCustomExtension)
  protected
    FServerAuthentication: boolean;
    FClientAuthentication: boolean;
    FCodeSigning: boolean;
    FEmailProtection: boolean;
    FTimeStamping: boolean;
    FCustomUsages : TStringList;
    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
    function GetCustomUsage(Index: integer) : BufferType;
    procedure SetCustomUsage(Index: integer; const Value: BufferType);
    function GetCustomUsageCount: integer;
  public
    constructor Create;
    destructor Destroy; override;
    function AddCustomUsage(const UsageOID : BufferType): integer;
    procedure RemoveCustomUsage(Index: integer);
    procedure ClearCustomUsages;
    property ServerAuthentication: boolean read FServerAuthentication
    write FServerAuthentication;
    property ClientAuthentication: boolean read FClientAuthentication
    write FClientAuthentication;
    property CodeSigning: boolean read FCodeSigning write FCodeSigning;
    property EmailProtection: boolean read FEmailProtection
    write FEmailProtection;
    property TimeStamping: boolean read FTimeStamping write FTimeStamping;
    property CustomUsages[Index: integer] : BufferType read GetCustomUsage
      write SetCustomUsage;
    property CustomUsageCount: integer read GetCustomUsageCount;
  end;

  TElDistributionPoint =
     class(TPersistent)
  protected
    FName: TElGeneralNames;
    FCRLIssuer: TElGeneralNames;
    FReasonFlags: TSBCRLReasonFlags;
  public
    constructor Create;
    destructor Destroy; override;
    property Name: TElGeneralNames read FName;
    property ReasonFlags: TSBCRLReasonFlags read FReasonFlags write
      FReasonFlags;
    property CRLIssuer: TElGeneralNames read FCRLIssuer;
  end;

  TElCRLDistributionPointsExtension =  class(TElCustomExtension)
  protected
    FPoints: TList;
    function GetDistributionPoints(Index: integer): TElDistributionPoint;
    function GetCount: integer;
    procedure SetCount(Value: integer);
    procedure ClearList;

    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
    //procedure Parse(const Value: BufferType);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Remove(Index: integer);
    property DistributionPoints[Index: integer]: TElDistributionPoint read
      GetDistributionPoints;
    property Count: integer read GetCount write SetCount;
  end;

  TElAccessDescription =
     class(TPersistent)
  protected
    FAccessMethod: BufferType;
    FGeneralName: TElGeneralName;
  public
    constructor Create;
    destructor Destroy; override;
    property AccessMethod: BufferType read FAccessMethod write FAccessMethod;
    property AccessLocation: TElGeneralName read FGeneralName;
  end;

  TElAuthorityInformationAccessExtension =  class(TElCustomExtension)
  protected
    FList: TList;
    function GetAccessDescriptions(Index: integer): TElAccessDescription;
    function GetCount: integer;
    procedure SetCount(Value: integer);
    procedure ClearList;

    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Remove(Index: integer);
    property AccessDescriptions[Index: integer]: TElAccessDescription
    read GetAccessDescriptions;
    property Count: integer read GetCount write SetCount;
  end;

  TElSubjectDirectoryAttributesExtension =  class(TElCustomExtension)
  private
    FAttributes : TElPKCS7Attributes;
  protected
    procedure Clear; override;
    function GetOID: BufferType; override;
    procedure SetOID(const Value: BufferType); override;
    procedure SetValue(const Value: BufferType); override;
  public
    constructor Create;
    destructor Destroy; override;
    property Attributes : TElPKCS7Attributes read FAttributes;
  end;

  TElCertificateExtensions =  class
  protected
    FAuthorityKeyIdentifier: TElAuthorityKeyIdentifierExtension;
    FSubjectKeyIdentifier: TElSubjectKeyIdentifierExtension;
    FKeyUsage: TElKeyUsageExtension;
    FPrivateKeyUsagePeriod: TElPrivateKeyUsagePeriodExtension;
    FCertificatePolicies: TElCertificatePoliciesExtension;
    FPolicyMappings: TElPolicyMappingsExtension;
    FSubjectAlternativeName: TElAlternativeNameExtension;
    FIssuerAlternativeName: TElAlternativeNameExtension;
    FBasicConstraints: TElBasicConstraintsExtension;
    FNameConstraints: TElNameConstraintsExtension;
    FPolicyConstraints: TElPolicyConstraintsExtension;
    FExtendedKeyUsage: TElExtendedKeyUsageExtension;
    FCRLDistributionPoints: TElCRLDistributionPointsExtension;
    FAuthorityInformationAccess: TElAuthorityInformationAccessExtension;
    //JPM Additons
    FNetscapeCertType : TElNetscapeCertTypeExtension;
    FNetscapeComment : TElNetscapeComment;
    FNetscapeCAPolicy: TElNetscapeCAPolicy;
    FNetscapeCARevokeURL: TElNetscapeCARevokeURL;
    FNetscapeRevokeURL: TElNetscapeRevokeURL;
    FNetscapeServerName: TElNetscapeServerName;
    FNetscapeBaseURL: TElNetscapeBaseURL;
    FNetscapeRenewalURL: TElNetscapeRenewalURL;
    FCommonName: TElCommonName;
    //end
    FSubjectDirectoryAttributes : TElSubjectDirectoryAttributesExtension;
    FIncluded: TSBCertificateExtensions;
    FOtherList: TList;
    procedure ClearOtherList;
    function GetOtherExtensions(Index: integer): TElCustomExtension;
    function GetOtherCount: integer;
    procedure SetOtherCount(Value: integer);
  public
    constructor Create;
    destructor Destroy; override;
    function RemoveOther(Index: integer) : boolean;
    procedure ClearExtensions;
    property AuthorityKeyIdentifier: TElAuthorityKeyIdentifierExtension
    read FAuthorityKeyIdentifier;
    property SubjectKeyIdentifier: TElSubjectKeyIdentifierExtension
    read FSubjectKeyIdentifier;
    property KeyUsage: TElKeyUsageExtension
    read FKeyUsage write FKeyUsage;
    property PrivateKeyUsagePeriod: TElPrivateKeyUsagePeriodExtension
    read FPrivateKeyUsagePeriod write FPrivateKeyUsagePeriod;
    property CertificatePolicies: TElCertificatePoliciesExtension
    read FCertificatePolicies;
    property PolicyMappings: TElPolicyMappingsExtension
    read FPolicyMappings write FPolicyMappings;
    property SubjectAlternativeName: TElAlternativeNameExtension
    read FSubjectAlternativeName write FSubjectAlternativeName;
    property IssuerAlternativeName: TElAlternativeNameExtension
    read FIssuerAlternativeName write FIssuerAlternativeName;
    property BasicConstraints: TElBasicConstraintsExtension
    read FBasicConstraints write FBasicConstraints;
    property NameConstraints: TElNameConstraintsExtension
    read FNameConstraints;
    property PolicyConstraints: TElPolicyConstraintsExtension
    read FPolicyConstraints write FPolicyConstraints;
    property ExtendedKeyUsage: TElExtendedKeyUsageExtension
    read FExtendedKeyUsage;
    property CRLDistributionPoints: TElCRLDistributionPointsExtension
    read FCRLDistributionPoints;
    property AuthorityInformationAccess: TElAuthorityInformationAccessExtension
    read FAuthorityInformationAccess;
    //jpm additon
    property NetscapeCertType : TElNetscapeCertTypeExtension read FNetscapeCertType write FNetscapeCertType;
    property NetscapeComment : TElNetscapeComment read FNetscapeComment write FNetscapeComment;
    property NetscapeBaseURL : TElNetscapeBaseURL read FNetscapeBaseURL write FNetscapeBaseURL;
    property NetscapeRevokeURL : TElNetscapeRevokeURL read FNetscapeRevokeURL write FNetscapeRevokeURL;
    property NetscapeCARevokeURL : TElNetscapeCARevokeURL read FNetscapeCARevokeURL write FNetscapeCARevokeURL;
    property NetscapeRenewalURL : TElNetscapeRenewalURL read FNetscapeRenewalURL write FNetscapeRenewalURL;
    property NetscapeCAPolicy : TElNetscapeCAPolicy read FNetscapeCAPolicy write FNetscapeCAPolicy;
    property NetscapeServerName : TElNetscapeServerName read FNetscapeServerName write FNetscapeServerName;
    property CommonName : TElCommonName read FCommonName write FCommonName;
    //end
    property SubjectDirectoryAttributes : TElSubjectDirectoryAttributesExtension
    read FSubjectDirectoryAttributes;
    property OtherExtensions[Index: integer]: TElCustomExtension read
    GetOtherExtensions;
    property OtherCount: integer read GetOtherCount write SetOtherCount;
    property Included: TSBCertificateExtensions read FIncluded write FIncluded;
  end;

  TElExtensionWriter = class
  private
    FCertificateExtensions : TElCertificateExtensions;
    FUseA3Prefix : boolean;
  public
    constructor Create(Exts : TElCertificateExtensions;
      CertExtensions: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif});
    function WriteExtensions: BufferType;
    function WriteExtension(const OID: BufferType; Critical: boolean; const Value: BufferType): BufferType;
    function WriteExtensionBasicConstraints: BufferType;
    function WriteExtensionKeyUsage: BufferType;
    function WriteExtensionPrivateKeyUsagePeriod: BufferType;
    function WriteExtensionSubjectAltName: BufferType;
    function WriteExtensionIssuerAltName: BufferType;
    function WriteExtensionExtendedKeyUsage: BufferType;
    function WriteExtensionPolicyMappings: BufferType;
    function WriteExtensionNameConstraints: BufferType;
    function WriteExtensionPolicyConstraints: BufferType;
    function WriteExtensionCertificatePolicies: BufferType;
    function WriteExtensionAuthorityKeyIdentifier: BufferType;
    function WriteExtensionCRLDistributionPoints: BufferType;
    function WriteExtensionAuthorityInformationAccess: BufferType;
    function WriteExtensionNetscapeCertType: BufferType;
    function WriteExtensionNetscapeString(const AOID: BufferType; const ANetStr: BufferType): BufferType;
    function WriteExtensionSubjectKeyIdentifier: BufferType;
    function WritePolicyInformation(P: TElSinglePolicyInformation): BufferType;
    function WriteDistributionPoint(P: TElDistributionPoint): BufferType;
    function WriteExtensionSubjectDirectoryAttributes : BufferType;
    property Extensions : TElCertificateExtensions read FCertificateExtensions;
    property UseA3Prefix : boolean read FUseA3Prefix write FUseA3Prefix;
  end;

  TElExtensionReader = class
  private
    FCertificateExtensions : TElCertificateExtensions;
    FStrictMode : boolean;
  public
    constructor Create(Exts : TElCertificateExtensions; StrictMode : boolean);
    procedure ParseExtension(const OID: BufferType; Critical: boolean;
      const Content: BufferType);
    property Extensions : TElCertificateExtensions read FCertificateExtensions;
  end;

const
  PEM_CERTIFICATE_BEGIN_LINE = '-----BEGIN CERTIFICATE-----';
  PEM_CERTIFICATE_END_LINE = '-----END CERTIFICATE-----'; //#$0A;
  PEM_RSA_PRIVATE_KEY_BEGIN_LINE = '-----BEGIN RSA PRIVATE KEY-----';
  PEM_RSA_PRIVATE_KEY_END_LINE = '-----END RSA PRIVATE KEY-----'; //#$0A;
  PEM_DSA_PRIVATE_KEY_BEGIN_LINE = '-----BEGIN DSA PRIVATE KEY-----';
  PEM_DSA_PRIVATE_KEY_END_LINE = '-----END DSA PRIVATE KEY-----'; //#$0A;
  PEM_DH_PRIVATE_KEY_BEGIN_LINE = '-----BEGIN DH PRIVATE KEY-----';
  PEM_DH_PRIVATE_KEY_END_LINE = '-----END DH PRIVATE KEY-----'; //#$0A;

function OctetsToIPAddress(const Octets : BufferType) : string; 
function IPAddressToOctets(const IPAddrStr : string) : BufferType; 

implementation

resourcestring
  SInvalidCertificateExtension = 'Unrecognized critical certificate extension';
  SInvalidTypeCast = 'Invalid type cast';
  SNonCriticalExtensionMarkedAsCritical = 'Non-critical extension is marked as critical';

////////////////////////////////////////////////////////////////////////////////
// TElCertificateExtensions

constructor TElCertificateExtensions.Create;
begin
  inherited;
  FAuthorityKeyIdentifier := TElAuthorityKeyIdentifierExtension.Create;
  FSubjectKeyIdentifier := TElSubjectKeyIdentifierExtension.Create;
  FKeyUsage := TElKeyUsageExtension.Create;
  FPrivateKeyUsagePeriod := TElPrivateKeyUsagePeriodExtension.Create;
  FSubjectAlternativeName := TElAlternativeNameExtension.Create;
  FIssuerAlternativeName := TElAlternativeNameExtension.Create;
  FBasicConstraints := TElBasicConstraintsExtension.Create;
  FExtendedKeyUsage := TElExtendedKeyUsageExtension.Create;
  FPolicyMappings := TElPolicyMappingsExtension.Create;
  FNameConstraints := TElNameConstraintsExtension.Create;
  FPolicyConstraints := TElPolicyConstraintsExtension.Create;
  FCertificatePolicies := TElCertificatePoliciesExtension.Create;
  FCRLDistributionPoints := TElCRLDistributionPointsExtension.Create;
  FAuthorityInformationAccess := TElAuthorityInformationAccessExtension.Create;
  //JPM addition
  FNetscapeCertType := TElNetscapeCertTypeExtension.Create;
  FNetscapeCAPolicy:= TElNetscapeCAPolicy.Create;
  FNetscapeCARevokeURL:= TElNetscapeCARevokeURL.Create;
  FNetscapeRevokeURL:= TElNetscapeRevokeURL.Create;
  FNetscapeServerName:= TElNetscapeServerName.Create;
  FNetscapeBaseURL:= TElNetscapeBaseURL.Create;
  FNetscapeRenewalURL:= TElNetscapeRenewalURL.Create;
  FNetscapeComment := TElNetscapeComment.Create;
  FCommonName := TElCommonName.Create;
  //end
  FSubjectDirectoryAttributes := TElSubjectDirectoryAttributesExtension.Create;
  FOtherList := TList.Create;
end;

destructor TElCertificateExtensions.Destroy;
begin
  FAuthorityKeyIdentifier.Free;
  FKeyUsage.Free;
  FPrivateKeyUsagePeriod.Free;
  FSubjectAlternativeName.Free;
  FIssuerAlternativeName.Free;
  FBasicConstraints.Free;
  FExtendedKeyUsage.Free;
  FPolicyMappings.Free;
  FNameConstraints.Free;
  FPolicyConstraints.Free;
  FSubjectKeyIdentifier.Free;
  FCertificatePolicies.Free;
  FCRLDistributionPoints.Free;
  FAuthorityInformationAccess.Free;
  //JPM addition
  FNetscapeCertType.Free;
  FNetscapeCAPolicy.Free;
  FNetscapeCARevokeURL.Free;
  FNetscapeRevokeURL.Free;
  FNetscapeServerName.Free;
  FNetscapeBaseURL.Free;
  FNetscapeRenewalURL.Free;
  FNetscapeComment.Free;
  FCommonName.Free;
  //
  FSubjectDirectoryAttributes.Free;
  ClearOtherList;
  FOtherList.Free;
  inherited;
end;

procedure TElCertificateExtensions.ClearExtensions;
begin
  FAuthorityKeyIdentifier.Clear;
  FSubjectKeyIdentifier.Clear;
  FKeyUsage.Clear;
  FPrivateKeyUsagePeriod.Clear;
  FCertificatePolicies.Clear;
  FPolicyMappings.Clear;
  FSubjectAlternativeName.Clear;
  FIssuerAlternativeName.Clear;
  FBasicConstraints.Clear;
  FNameConstraints.Clear;
  FPolicyConstraints.Clear;
  FExtendedKeyUsage.Clear;
  FCRLDistributionPoints.Clear;
  FAuthorityInformationAccess.Clear;
  FNetscapeCertType.Clear;
  FNetscapeComment.Clear;
  FNetscapeCAPolicy.Clear;
  FNetscapeCARevokeURL.Clear;
  FNetscapeRevokeURL.Clear;
  FNetscapeServerName.Clear;
  FNetscapeBaseURL.Clear;
  FNetscapeRenewalURL.Clear;
  FCommonName.Clear;
  FSubjectDirectoryAttributes.Clear;
  ClearOtherList;
  FIncluded := []
end;

procedure TElCertificateExtensions.ClearOtherList;
var
  P: TElCustomExtension;
begin
  while FOtherList.Count > 0 do
  begin
    P := TElCustomExtension(FOtherList[0]);
    FOtherList.Delete(0);
    P.Free;
  end;
end;

function TElCertificateExtensions.GetOtherExtensions(Index: integer):
  TElCustomExtension;
begin
  Result := TElCustomExtension(FOtherList[Index]);
end;

function TElCertificateExtensions.GetOtherCount: integer;
begin
  Result := FOtherList.Count;
end;

procedure TElCertificateExtensions.SetOtherCount(Value: integer);
var
  P: TElCustomExtension;
begin
  if Value < FOtherList.Count then
  begin
    while FOtherList.Count > Value do
    begin
      P := TElCustomExtension(FOtherList[FOtherList.Count - 1]);
      FOtherList.Delete(FOtherList.Count - 1);
      P.Free;
    end;
  end
  else
    if Value > FOtherList.Count then
    while FOtherList.Count < Value do
      FOtherList.Add(TElCustomExtension.Create);
end;

function TElCertificateExtensions.RemoveOther(Index: integer) : boolean;
begin
  if Index < FOtherList.Count then
  begin
    TElCustomExtension(FOtherList.Items[Index]).Free;
    FOtherList.Delete(Index);
    Result := true;
  end
  else
    Result := false;
end;

constructor TElCustomExtension.Create;
begin
  inherited;
  SetLength(FOID, 0);
  SetLength(FValue, 0);
end;

procedure TElCustomExtension.Clear;
begin
  SetLength(FOID, 0);
  SetLength(FValue, 0);
end;

function TElCustomExtension.GetOID: BufferType;
begin
  Result := FOID;
end;

procedure TElCustomExtension.SetOID(const Value: BufferType);
begin
  FOID := CloneBuffer(Value);
end;

procedure TElCustomExtension.SetValue(const Value: BufferType);
begin
  Clear;
  FValue := CloneBuffer(Value);
end;

function TElCustomExtension.GetValue: BufferType;
begin
  Result := FValue;
end;

procedure TElCustomExtension.SaveToTag(Tag: TElASN1ConstrainedTag);
var
  STag: TElASN1SimpleTag;
begin
  Tag.TagID := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagID := SB_ASN1_OBJECT;
  STag.Content := OID;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagID := SB_ASN1_BOOLEAN;
  if Critical then
    STag.Content := #$FF
  else
    STag.Content := #0;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagID := SB_ASN1_OCTETSTRING;
  STag.Content := CloneBuffer(Value);
end;

procedure TElCustomExtension.RaiseInvalidExtensionError;
begin
  if Critical then
    raise EElCertificateException.Create(SInvalidCertificateExtension);;
end;

function TElPolicyMappingsExtension.GetPolicies(Index: integer):
  TElPolicyMapping;
begin
  Result := TElPolicyMapping(FList[Index]);
end;

function TElPolicyMappingsExtension.GetCount: integer;
begin
  Result := FList.Count;
end;

constructor TElPolicyMappingsExtension.Create;
begin
  inherited;
  FList := TList.Create;
end;

destructor TElPolicyMappingsExtension.Destroy;
begin
  ClearList;
  FList.Free;
  inherited;
end;

procedure TElPolicyMappingsExtension.Clear;
begin
  ClearList;
end;

function TElPolicyMappingsExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$21);
end;

procedure TElPolicyMappingsExtension.SetOID(const Value: BufferType);
begin
end;

procedure TElPolicyMappingsExtension.SetValue(const Value: BufferType);
var
  Tag, SeqTag : TElASN1ConstrainedTag;
  I : integer;
  OldCount : integer;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        for I := 0 to TElASN1ConstrainedTag(Tag.GetField(0)).Count - 1 do
        begin
          if TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I).CheckType(SB_ASN1_SEQUENCE, true) then
          begin
            SeqTag := TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I));
            if (SeqTag.Count = 2) and (SeqTag.GetField(0).CheckType(SB_ASN1_OBJECT, false)) and
              (SeqTag.GetField(1).CheckType(SB_ASN1_OBJECT, false)) then
            begin
              OldCount := Count;
              Count := OldCount + 1;
              Policies[OldCount].IssuerDomainPolicy := TElASN1SimpleTag(SeqTag.GetField(0)).Content;
              Policies[OldCount].SubjectDomainPolicy := TElASN1SimpleTag(SeqTag.GetField(1)).Content;
            end
            else
              RaiseInvalidExtensionError;
          end
          else
            RaiseInvalidExtensionError;
        end;
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

procedure TElPolicyMappingsExtension.SetCount(Value: integer);
var
  P: TElPolicyMapping;
begin
  if Value < FList.Count then
  begin
    while FList.Count > Value do
    begin
      P := TElPolicyMapping(FList[FList.Count - 1]);
      FList.Delete(FList.Count - 1);
      P.Free;
    end;
  end
  else
    while FList.Count < Value do
      FList.Add(TElPolicyMapping.Create);
end;

procedure TElPolicyMappingsExtension.Remove(Index: integer);
var
  P: TElPolicyMapping;
begin
  P := TElPolicyMapping(FList[Index]);
  if Assigned(P) then
    P.Free;
  FList.Delete(Index);
end;

procedure TElPolicyMappingsExtension.ClearList;
begin
  while FList.Count > 0 do
  begin
    TElPolicyMapping(FList.Last).Free;
    FList.Count := FList.Count - 1;
  end;
end;

constructor TElAlternativeNameExtension.Create;
begin
  inherited;
  FContent := TElGeneralNames.Create;
end;

destructor TElAlternativeNameExtension.Destroy;
begin
  FContent.Free;
  inherited;
end;

procedure TElAlternativeNameExtension.Clear;
begin
  FContent.Free;
  FContent := TElGeneralNames.Create;
end;

function TElAlternativeNameExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$11);
end;

procedure TElAlternativeNameExtension.SetOID(const Value: BufferType);
begin
end;

procedure TElAlternativeNameExtension.SetValue(const Value: BufferType);
var
  Tag : TElASN1ConstrainedTag;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
        FContent.LoadFromTag(TElASN1ConstrainedTag(Tag.GetField(0)), false)
        //ParseGeneralNames(TElASN1ConstrainedTag(Tag.GetField(0)), FContent, false)
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

constructor TElNameConstraintsExtension.Create;
begin
  inherited;
  FPermittedList := TList.Create;
  FExcludedList := TList.Create;
end;

procedure TElNameConstraintsExtension.Clear;
begin
  ClearList;
end;

function TElNameConstraintsExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$1E);
end;

procedure TElNameConstraintsExtension.SetOID(const Value: BufferType);
begin
end;

procedure TElNameConstraintsExtension.SetValue(const Value: BufferType);
  procedure ReadGeneralSubtrees(Tag: TElASN1ConstrainedTag; List : TList);
  var
    I : integer;
    SubtreeTag : TElASN1ConstrainedTag;
    CurrIndex: integer;
    Index: integer;
  begin
    for I := 0 to Tag.Count - 1 do
    begin
      if Tag.GetField(I).CheckType(SB_ASN1_SEQUENCE, true) then
      begin
        Index := List.Add(TElNameConstraint.Create);
        SubtreeTag := TElASN1ConstrainedTag(Tag.GetField(I));
        CurrIndex := 0;
        if CurrIndex >= SubtreeTag.Count then
          Continue;
        //ParseGeneralName(SubtreeTag.GetField(CurrIndex), TElNameConstraint(List[Index]).FBase);
        TElNameConstraint(List[Index]).FBase.LoadFromTag(SubtreeTag.GetField(CurrIndex));

        Inc(CurrIndex);
        if (CurrIndex < SubtreeTag.Count) and (SubtreeTag.GetField(CurrIndex).CheckType($80, false)) then
        begin
          TElNameConstraint(List[Index]).FMinimum := ASN1ReadInteger(TElASN1SimpleTag(SubtreeTag.GetField(CurrIndex)));
          Inc(CurrIndex);
        end
        else
          TElNameConstraint(List[Index]).FMinimum := 0;
        if (CurrIndex < SubtreeTag.Count) and (SubtreeTag.GetField(CurrIndex).CheckType($81, false)) then
        begin
          TElNameConstraint(List[Index]).FMaximum := ASN1ReadInteger(TElASN1SimpleTag(SubtreeTag.GetField(CurrIndex)));
        end;
      end
      else
        RaiseInvalidExtensionError;
    end;
  end;
var
  Tag, SeqTag : TElASN1ConstrainedTag;
  CurrTagIndex: integer;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        SeqTag := TElASN1ConstrainedTag(Tag.GetField(0));
        CurrTagIndex := 0;
        if (CurrTagIndex < SeqTag.Count) and (SeqTag.GetField(CurrTagIndex).CheckType(SB_ASN1_A0, true)) then
        begin
          // reading permittedSubtrees
          ReadGeneralSubtrees(TElASN1ConstrainedTag(SeqTag.GetField(CurrTagIndex)),
            FPermittedList);
          Inc(CurrTagIndex);
        end;
        if (CurrTagIndex < SeqTag.Count) and (SeqTag.GetField(CurrTagIndex).CheckType(SB_ASN1_A1, true)) then
        begin
          // reading excluded subtrees
          ReadGeneralSubtrees(TElASN1ConstrainedTag(SeqTag.GetField(CurrTagIndex)),
            FExcludedList);
        end;
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

constructor TElGeneralName.Create;
begin
  inherited;
  FDirectoryName := TElRelativeDistinguishedName.Create;
  FEdiPartyName := TElEDIPartyName.Create;
  FOtherName := TElOtherName.Create;
  FPermanentIdentifier := TElPermanentIdentifier.Create;
end;

destructor TElGeneralName.Destroy;
begin
  FreeAndNil(FDirectoryName);
  FreeAndNil(FEdiPartyName);
  FreeAndNil(FOtherName);
  FreeAndNil(FPermanentIdentifier);
  inherited;
end;

function TElGeneralName.LoadFromTag(Tag: TElASN1CustomTag): boolean;
var
  CurrIndex : integer;
  B : boolean;
  Size : integer;
  Val : BufferType;
begin
  Result := true;
  FNameType := gnUnknown;
  if Tag.CheckType(SB_ASN1_A0, true) then
  begin
    // other name
    if (TElASN1ConstrainedTag(Tag).Count = 2) and
      (TElASN1ConstrainedTag(Tag).GetField(0).CheckType(SB_ASN1_OBJECT, false)) and
      (TElASN1ConstrainedTag(Tag).GetField(1).CheckType(SB_ASN1_A0, true)) then
    begin
      FOtherName.OID := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(0)).Content;
      B := TElASN1ConstrainedTag(Tag).GetField(1).WriteHeader;
      TElASN1ConstrainedTag(Tag).GetField(1).WriteHeader := false;
      Size := 0;
      TElASN1ConstrainedTag(Tag).GetField(1).SaveToBuffer(nil, Size);
      SetLength(Val, Size);
      TElASN1ConstrainedTag(Tag).GetField(1).SaveToBuffer(@Val[1], Size);
      SetLength(Val, Size);
      TElASN1ConstrainedTag(Tag).GetField(1).WriteHeader := B;
      FOtherName.Value := Val;
      FNameType := gnOtherName;
      TryKnownOtherNames;
    end
    else
      Result := false;
  end
  else if Tag.CheckType(SB_ASN1_A1_PRIMITIVE, false) then
  begin
    // rfc822 name
    FRFC822Name := (TElASN1SimpleTag(Tag).Content);
    FNameType := gnRFC822Name;
  end
  else if Tag.CheckType(SB_ASN1_A2_PRIMITIVE, false) then
  begin
    // dns name
    FDnsName := (TElASN1SimpleTag(Tag).Content);
    FNameType := gnDnsName;
  end
  else if Tag.CheckType(SB_ASN1_A3, true) then
  begin
    // ORAddress, unsupported
  end
  else if Tag.CheckType(SB_ASN1_A4, true) then
  begin
    // directory name
    if (TElASN1ConstrainedTag(Tag).Count = 1) and (TElASN1ConstrainedTag(Tag).GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      FDirectoryName.LoadFromTag(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag).GetField(0)), true);
    FNameType := gnDirectoryName;
  end
  else if Tag.CheckType(SB_ASN1_A5, true) then
  begin
    // edi party name
    CurrIndex := 0;
    if (CurrIndex < TElASN1ConstrainedTag(Tag).Count) and (TElASN1ConstrainedTag(Tag).GetField(CurrIndex).CheckType(SB_ASN1_A0_PRIMITIVE, false)) then
    begin
      FEdiPartyName.NameAssigner := (TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(CurrIndex)).Content);
      Inc(CurrIndex);
    end
    else
      FEdiPartyName.NameAssigner := '';
    if (CurrIndex < TElASN1ConstrainedTag(Tag).Count) and (TElASN1ConstrainedTag(Tag).GetField(CurrIndex).CheckType(SB_ASN1_A1_PRIMITIVE, false)) then
      FEdiPartyName.PartyName := (TElASN1SimpleTag(TElASN1ConstrainedTag(Tag).GetField(CurrIndex)).Content);
    FNameType := gnEdiPartyName;
  end
  else if Tag.CheckType(SB_ASN1_A6_PRIMITIVE, false) then
  begin
    // uri
    FUniformResourceIdentifier := (TElASN1SimpleTag(Tag).Content);
    FNameType := gnUniformResourceIdentifier;
  end
  else if Tag.CheckType(SB_ASN1_A7_PRIMITIVE, false) then
  begin
    // ip address
    FIpAddress := OctetsToIPAddress(TElASN1SimpleTag(Tag).Content);
    FNameType := gnIPAddress;
  end
  else if Tag.CheckType(SB_ASN1_A8_PRIMITIVE, false) then
  begin
    // registered id
    FRegisteredID := TElASN1SimpleTag(Tag).Content;
    FNameType := gnRegisteredID;
  end
  else
    Result := false;
end;

function TElGeneralName.SaveToTag(Tag: TElASN1SimpleTag): boolean;
var
  STag: TElASN1SimpleTag;
  CTag: TElASN1ConstrainedTag;
  Size : integer;
  Buf : BufferType;
begin
  Result := true;
  SaveKnownOtherNames;
  if FNameType = gnRFC822Name then
  begin
    Tag.Content := (FRFC822Name);
    Tag.TagID := SB_ASN1_A1_PRIMITIVE;
  end
  else if FNameType = gnDNSName then
  begin
    Tag.Content := (FDnsName);
    Tag.TagID := SB_ASN1_A2_PRIMITIVE;
  end
  else if FNameType = gnDirectoryName then
  begin
    CTag := TElASN1ConstrainedTag.Create;
    try
      FDirectoryName.SaveToTag(CTag);
      Size := 0;
      CTag.WriteHeader := true;
      CTag.SaveToBuffer(nil, Size);
      SetLength(Buf, Size);
      CTag.SaveToBuffer(@Buf[1], Size);
      SetLength(Buf, Size);
      Tag.TagID := SB_ASN1_A4;
      Tag.Content := Buf;                
    finally
      FreeAndNil(CTag);
    end;
  end
  else if FNameType = gnEdiPartyName then
  begin
    CTag := TElASN1ConstrainedTag.Create;
    try
      if Length(FEdiPartyName.NameAssigner) > 0 then
      begin
        STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
        STag.TagID := SB_ASN1_A0_PRIMITIVE;  
        STag.Content := (FEdiPartyName.NameAssigner);
      end;
      STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
      STag.TagID := SB_ASN1_A1_PRIMITIVE;
      STag.Content := (FEdiPartyName.PartyName);
      CTag.WriteHeader := false;//true;
      Size := 0;
      CTag.SaveToBuffer(nil, Size);
      SetLength(Buf, Size);
      CTag.SaveToBuffer(@Buf[1], Size);
      SetLength(Buf, Size);
      Tag.TagID := SB_ASN1_A5;
      Tag.Content := Buf;
    finally
      FreeAndNil(CTag);
    end;
  end
  else if FNameType = gnUniformResourceIdentifier then
  begin
    Tag.Content := (FUniformResourceIdentifier);
    Tag.TagID := SB_ASN1_A6_PRIMITIVE;
  end
  else if FNameType = gnIPAddress then
  begin
    Tag.Content := IpAddressToOctets(FIpAddress);
    Tag.TagID := SB_ASN1_A7_PRIMITIVE;
  end
  else if FNameType = gnRegisteredID then
  begin
    Tag.Content := FRegisteredID;
    Tag.TagID := SB_ASN1_A8_PRIMITIVE;
  end
  else if FNameType = gnOtherName then
  begin
    CTag := TElASN1ConstrainedTag.Create;
    try
      STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
      STag.TagID := SB_ASN1_OBJECT;
      STag.Content := FOtherName.FOID;
      STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
      STag.TagID := SB_ASN1_A0;
      STag.Content := FOtherName.FValue;
      CTag.WriteHeader := false;
      Size := 0;
      CTag.SaveToBuffer(nil, Size);
      SetLength(Buf, Size);
      CTag.SaveToBuffer(@Buf[1], Size);
      SetLength(Buf, Size);
      Tag.TagID := SB_ASN1_A0;
      Tag.Content := Buf;
    finally
      FreeAndNil(CTag);
    end;
  end
  else
    Result := false;
end;

function TElGeneralName.GetIsEmpty : boolean;
begin
  Result := not ((Length(FRFC822Name) > 0) or (Length(FDNSName) > 0) or
    (FDirectoryName.Count > 0) or (Length(FEdiPartyName.FNameAssigner) > 0) or
    (Length(FEdiPartyName.FPartyName) > 0) or
    (Length(FUniformResourceIdentifier) > 0) or (Length(FIpAddress) > 0) or
    (Length(FRegisteredID) > 0));
end;

procedure TElGeneralName.Assign(Source: TPersistent);
begin
  if not (Source is TElGeneralName) then                 
    raise EElCertificateException.Create(SInvalidTypeCast);
  FRFC822Name := TElGeneralName(Source).FRFC822Name;
  FDNSName := TElGeneralName(Source).FDNSName;
  FDirectoryName.Assign(TElGeneralName(Source).FDirectoryName);
  FEdiPartyName.FNameAssigner := TElGeneralName(Source).FEdiPartyName.FNameAssigner;
  FEdiPartyName.FPartyName := TElGeneralName(Source).FEdiPartyName.FPartyName;
  FUniformResourceIdentifier := TElGeneralName(Source).FUniformResourceIdentifier;
  FIpAddress := TElGeneralName(Source).FIpAddress;
  FRegisteredID := TElGeneralName(Source).FRegisteredID;
  FOtherName.FOID := TElGeneralName(Source).FOtherName.FOID;
  FOtherName.FValue := TElGeneralName(Source).FOtherName.FValue;
  FPermanentIdentifier.FPermanentIdentifier := TElGeneralName(Source).FPermanentIdentifier.FPermanentIdentifier;
  FPermanentIdentifier.FAssigner := TElGeneralName(Source).FPermanentIdentifier.FAssigner;
  FNameType := TElGeneralName(Source).FNameType;
end;

procedure TElGeneralName.AssignTo(Dest: TPersistent);
begin
  if not (Dest is TElGeneralName) then
    raise EElCertificateException.Create(SInvalidTypeCast);
  Dest.Assign(Self);
end;

procedure TElGeneralName.TryKnownOtherNames;
begin
  if CompareContent(FOtherName.FOID, SB_CERT_OID_PERMANENT_IDENTIFIER) then
  begin
    ParsePermanentIdentifier(@FOtherName.FValue[1], Length(FOtherName.FValue));
    FNameType := gnPermanentIdentifier;
  end;
end;

procedure TElGeneralName.SaveKnownOtherNames;
var
  ONOID, ONContent: BufferType;
begin
  if NameType = gnPermanentIdentifier then
  begin
    SavePermanentIdentifier(ONOID, ONContent);
    FOtherName.OID := ONOID;
    FOtherName.Value := ONContent;
    NameType := gnOtherName;
  end;
end;

procedure TElGeneralName.ParsePermanentIdentifier(Buffer: pointer; Size: integer);
var
  Tag, Root : TElASN1ConstrainedTag;
  CurrIndex: integer;
begin
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(Buffer, Size) then
    begin
      if (Tag.Count <> 1) or (not Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
        Exit;
      Root := TElASN1ConstrainedTag(Tag.GetField(0));
      CurrIndex := 0;
      if CurrIndex >= Root.Count then
        Exit;
      if Root.GetField(CurrIndex).CheckType(SB_ASN1_UTF8STRING, false) then
      begin
        FPermanentIdentifier.FPermanentIdentifier := TElASN1SimpleTag(Root.GetField(CurrIndex)).Content;
        Inc(CurrIndex);
      end;
      if Root.GetField(CurrIndex).CheckType(SB_ASN1_OBJECT, false) then
        FPermanentIdentifier.FAssigner := TElASN1SimpleTag(Root.GetField(CurrIndex)).Content;
    end;
  finally
    FreeAndNil(Tag);
  end;
end;

procedure TElGeneralName.SavePermanentIdentifier(var OID : BufferType; var Content: BufferType);
var
  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  Size : integer;
begin
  Tag := TElASN1ConstrainedTag.Create;
  try
    Tag.TagID := SB_ASN1_SEQUENCE;
    if Length(FPermanentIdentifier.FPermanentIdentifier) > 0 then
    begin
      STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
      STag.TagID := SB_ASN1_UTF8STRING;
      STag.Content := FPermanentIdentifier.FPermanentIdentifier;
    end;
    if Length(FPermanentIdentifier.FAssigner) > 0 then
    begin
      STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
      STag.TagID := SB_ASN1_OBJECT;
      STag.Content := FPermanentIdentifier.FAssigner;
    end;
    OID := SB_CERT_OID_PERMANENT_IDENTIFIER;
    Size := 0;
    Tag.SaveToBuffer(nil, Size);
    SetLength(Content, Size);
    Tag.SaveToBuffer(@Content[1], Size);
    SetLength(Content, Size);
  finally
    FreeAndNil(Tag);
  end;
end;

destructor TElNameConstraintsExtension.Destroy;
begin
  ClearList;
  FPermittedList.Free;
  FExcludedList.Free;
  inherited;
end;

function TElNameConstraintsExtension.GetPermittedSubtrees(Index: integer):
  TElNameConstraint;
begin
  Result := TElNameConstraint(FPermittedList[Index]);
end;

function TElNameConstraintsExtension.GetExcludedSubtrees(Index: integer):
  TElNameConstraint;
begin
  Result := TElNameConstraint(FExcludedList[Index]);
end;

function TElNameConstraintsExtension.GetPermittedCount: integer;
begin
  Result := FPermittedList.Count;
end;

function TElNameConstraintsExtension.GetExcludedCount: integer;
begin
  Result := FExcludedList.Count;
end;

procedure TElNameConstraintsExtension.SetPermittedCount(Value: integer);
var
  P: TElNameConstraint;
begin
  if Value < FPermittedList.Count then
  begin
    while FPermittedList.Count > Value do
    begin
      P := TElNameConstraint(FPermittedList[FPermittedList.Count - 1]);
      FPermittedList.Delete(FPermittedList.Count - 1);
      P.Free;
    end;
  end
  else
    while FPermittedList.Count < Value do
      FPermittedList.Add(TElNameConstraint.Create);
end;

procedure TElNameConstraintsExtension.SetExcludedCount(Value: integer);
var
  P: TElNameConstraint;
begin
  if Value < FExcludedList.Count then
  begin
    while FExcludedList.Count > Value do
    begin
      P := TElNameConstraint(FExcludedList[FExcludedList.Count - 1]);
      FExcludedList.Delete(FExcludedList.Count - 1);
      P.Free;
    end;
  end
  else
    while FExcludedList.Count < Value do
      FExcludedList.Add(TElNameConstraint.Create);
end;

procedure TElNameConstraintsExtension.ClearList;
begin
  while FPermittedList.Count > 0 do
  begin
    TElNameConstraint(FPermittedList.Last).Free;
    FPermittedList.Count := FPermittedList.Count - 1;
  end;
  while FExcludedList.Count > 0 do
  begin
    TElNameConstraint(FExcludedList.Last).Free;
    FExcludedList.Count := FExcludedList.Count - 1;
  end;
end;

procedure TElNameConstraintsExtension.RemovePermitted(Index: integer);
begin
  TElNameConstraint(FPermittedList[Index]).Free;
  FPermittedList.Delete(Index);
end;

procedure TElNameConstraintsExtension.RemoveExcluded(Index: integer);
begin
  TElNameConstraint(FExcludedList[Index]).Free;
  FExcludedList.Delete(Index);
end;

function TElUserNotice.GetNoticeNumbers(Index: integer): integer;
begin
  if Index >= Length(FNoticeNumbers) then
    raise EElCertificateException.Create('List index out of bounds')
  else
    Result := FNoticeNumbers[Index];
end;

procedure TElUserNotice.SetNoticeNumbers(Index: integer; Value: integer);
begin
  if Index >= Length(FNoticeNumbers) then
    raise EElCertificateException.Create('List index out of bounds')
  else
    FNoticeNumbers[Index] := Value;
end;

function TElUserNotice.GetNoticeNumbersCount: integer;
begin
  Result := Length(FNoticeNumbers);
end;

procedure TElUserNotice.SetNoticeNumbersCount(Value: integer);
begin
  SetLength(FNoticeNumbers, Value);
end;

constructor TElSinglePolicyInformation.Create;
begin
  inherited;
  FUserNotice := TElUserNotice.Create;
end;

destructor TElSinglePolicyInformation.Destroy;
begin
  FUserNotice.Free;
  inherited;
end;

function TElCertificatePoliciesExtension.GetPolicyInformation(Index: integer):
TElSinglePolicyInformation;
begin
  Result := TElSinglePolicyInformation(FList[Index]);
end;

function TElCertificatePoliciesExtension.GetCount: integer;
begin
  Result := FList.Count;
end;

procedure TElCertificatePoliciesExtension.Clear;
begin
  ClearList;
end;

function TElCertificatePoliciesExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$20);
end;

procedure TElCertificatePoliciesExtension.SetOID(const Value: BufferType);
begin
end;

procedure TElCertificatePoliciesExtension.SetValue(const Value: BufferType);
  procedure ReadUserNotice(Tag: TElASN1ConstrainedTag; Notice: TElUserNotice);
  var
    CurrIndex: integer;
    SeqTag : TElASN1ConstrainedTag;
    I : integer;
    OldCount : integer;
  begin
    CurrIndex := 0;
    if (CurrIndex < Tag.Count) and (Tag.GetField(CurrIndex).CheckType(SB_ASN1_SEQUENCE, true)) then
    begin
      SeqTag := TElASN1ConstrainedTag(Tag.GetField(CurrIndex));
      Inc(CurrIndex);
      if (SeqTag.Count = 2) and (SeqTag.GetField(0).TagId in [SB_ASN1_VISIBLESTRING,
        SB_ASN1_UTF8STRING, SB_ASN1_IA5STRING]) and (not SeqTag.GetField(0).IsConstrained) and
        (SeqTag.GetField(1).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        Notice.FOrganization := (TElASN1SimpleTag(SeqTag.GetField(0)).Content);
        for I := 0 to TElASN1ConstrainedTag(SeqTag.GetField(1)).Count - 1 do
        begin
          if TElASN1ConstrainedTag(SeqTag.GetField(1)).GetField(I).CheckType(SB_ASN1_INTEGER, false) then
          begin
            OldCount := Length(Notice.FNoticeNumbers);
            SetLength(Notice.FNoticeNumbers, OldCount + 1);
            Notice.FNoticeNumbers[OldCount] := ASN1ReadInteger(TElASN1SimpleTag(TElASN1ConstrainedTag(SeqTag.GetField(1)).GetField(I)));
          end;
        end;
      end;
    end;
    if (CurrIndex < Tag.Count) and (Tag.GetField(CurrIndex).TagId in [SB_ASN1_VISIBLESTRING,
      SB_ASN1_UTF8STRING]) and (not Tag.GetField(CurrIndex).IsConstrained) then
      Notice.FExplicitText := (TElASN1SimpleTag(Tag.GetField(CurrIndex)).Content);
  end;
var
  Tag, SeqTag, PITag, PQTag : TElASN1ConstrainedTag;
  I, J : integer;
  CurrTagIndex : integer;
  Obj : BufferType;
  Index : integer;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        SeqTag := TElASN1ConstrainedTag(Tag.GetField(0));
        for I := 0 to SeqTag.Count - 1 do
        begin
          if SeqTag.GetField(I).CheckType(SB_ASN1_SEQUENCE, true) then
          begin
            PITag := TElASN1ConstrainedTag(SeqTag.GetField(I));
            CurrTagIndex := 0;
            if (CurrTagIndex < PITag.Count) and (PITag.GetField(CurrTagIndex).CheckType(SB_ASN1_OBJECT, false)) then
            begin
              Obj := TElASN1SimpleTag(PITag.GetField(CurrTagIndex)).Content;
              Inc(CurrTagIndex);
            end
            else
              Continue;

            Index := FList.Add(TElSinglePolicyInformation.Create);
            TElSinglePolicyInformation(FList[Index]).PolicyIdentifier := Obj;

            if (CurrTagIndex < PITag.Count) and (PITag.GetField(CurrTagIndex).CheckType(SB_ASN1_SEQUENCE, true)) then
            begin
              for J := 0 to TElASN1ConstrainedTag(PITag.GetField(CurrTagIndex)).Count - 1 do
              begin
                if TElASN1ConstrainedTag(PITag.GetField(CurrTagIndex)).GetField(J).CheckType(SB_ASN1_SEQUENCE, true) then
                begin
                  PQTag := TElASN1ConstrainedTag(TElASN1ConstrainedTag(PITag.GetField(CurrTagIndex)).GetField(J));
                  CurrTagIndex := 0;
                  if (CurrTagIndex < PQTag.Count) and (PQTag.GetField(CurrTagIndex).CheckType(SB_ASN1_OBJECT, false)) then
                  begin
                    Obj := TElASN1SimpleTag(PQTag.GetField(CurrTagIndex)).Content;
                    Inc(CurrTagIndex);
                  end
                  else
                    Continue;
                  if (CurrTagIndex < PQTag.Count) then
                  begin
                    if CompareContent(Obj, SB_OID_QT_CPS) and (PQTag.GetField(CurrTagIndex).CheckType(SB_ASN1_IA5STRING, false)) then
                      TElSinglePolicyInformation(FList[Index]).FCPSURI := (TElASN1SimpleTag(PQTag.GetField(CurrTagIndex)).Content)
                    else if CompareContent(Obj, SB_OID_QT_UNOTICE) and (PQTag.GetField(CurrTagIndex).CheckType(SB_ASN1_SEQUENCE, true)) then
                    begin
                      ReadUserNotice(TElASN1ConstrainedTag(PQTag.GetField(CurrTagIndex)),
                        TElSinglePolicyInformation(FList[Index]).FUserNotice);
                    end;
                  end;
                end;
              end;
            end;
          end
          else
            RaiseInvalidExtensionError;
        end;
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

procedure TElCertificatePoliciesExtension.SetCount(Value: integer);
begin
  if Value < FList.Count then
  begin
    while FList.Count > Value do
    begin
      TElSinglePolicyInformation(FList.Last).Free;
      FList.Count := FList.Count - 1;
    end;
  end
  else
    while FList.Count < Value do
      FList.Add(TElSinglePolicyInformation.Create);
end;

constructor TElCertificatePoliciesExtension.Create;
begin
  inherited;
  FList := TList.Create;
end;

destructor TElCertificatePoliciesExtension.Destroy;
begin
  ClearList;
  FList.Free;
  inherited;
end;

procedure TElCertificatePoliciesExtension.ClearList;
begin
  while FList.Count > 0 do
  begin
    TElSinglePolicyInformation(FList.Last).Free;
    FList.Count := FList.Count - 1;
  end;
end;

procedure TElCertificatePoliciesExtension.Remove(Index: integer);
begin
  TElSinglePolicyInformation(FList[Index]).Free;
  FList.Delete(Index);
end;

constructor TElNameConstraint.Create;
begin
  inherited;
  FBase := TElGeneralName.Create;
end;

destructor TElNameConstraint.Destroy;
begin
  FBase.Free;
  inherited;
end;

constructor TElAuthorityKeyIdentifierExtension.Create;
begin
  inherited;
  FAuthorityCertIssuer := TElGeneralNames.Create;
end;

destructor TElAuthorityKeyIdentifierExtension.Destroy;
begin
  FAuthorityCertIssuer.Free;
  inherited;
end;

procedure TElAuthorityKeyIdentifierExtension.Clear;
begin
  SetLength(ByteArray(FKeyIdentifier), 0);
  SetLength(FAuthorityCertSerial, 0);
  FAuthorityCertIssuer.Free;
  FAuthorityCertIssuer := TElGeneralNames.Create;
end;

function TElAuthorityKeyIdentifierExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$23);
end;

procedure TElAuthorityKeyIdentifierExtension.SetOID(const Value: BufferType);
begin
end;

procedure TElAuthorityKeyIdentifierExtension.SetValue(const Value: BufferType);
var
  Tag, SeqTag : TElASN1ConstrainedTag;
  CurrTagIndex : integer;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        SeqTag := TElASN1ConstrainedTag(Tag.GetField(0));
        CurrTagIndex := 0;
        if (CurrTagIndex < SeqTag.Count) and (SeqTag.GetField(CurrTagIndex).CheckType($80, false)) then
        begin
          FKeyIdentifier := TElASN1SimpleTag(SeqTag.GetField(CurrTagIndex)).Content;
          Inc(CurrTagIndex);
        end;
        if (CurrTagIndex < SeqTag.Count) and (SeqTag.GetField(CurrTagIndex).CheckType($A1, true)) then
        begin
          //ParseGeneralNames(TElASN1ConstrainedTag(SeqTag.GetField(CurrTagIndex)),
          //  FAuthorityCertIssuer, true, true);
          FAuthorityCertIssuer.LoadFromTag(TElASN1ConstrainedTag(SeqTag.GetField(CurrTagIndex)), true);
          Inc(CurrTagIndex);
        end;
        if (CurrTagIndex < SeqTag.Count) and (SeqTag.GetField(CurrTagIndex).CheckType($82, false)) then
        begin
          FAuthorityCertSerial := RotateInteger(TElASN1SimpleTag(SeqTag.GetField(CurrTagIndex)).Content);
        end;
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

constructor TElDistributionPoint.Create;
begin
  inherited;
  FName := TElGeneralNames.Create;
  FCRLIssuer := TElGeneralNames.Create;
  FReasonFlags := [rfUnspecified, rfKeyCompromise, rfCACompromise,
    rfAffiliationChanged, rfSuperseded, rfCessationOfOperation,
      rfCertificateHold, rfRemoveFromCRL, rfPrivilegeWithdrawn, rfAACompromise];
end;

destructor TElDistributionPoint.Destroy;
begin
  FName.Free;
  FCRLIssuer.Free;
  inherited;
end;

function TElCRLDistributionPointsExtension.GetDistributionPoints(Index:
  integer):
TElDistributionPoint;
begin
  Result := TElDistributionPoint(FPoints[Index]);
end;

function TElCRLDistributionPointsExtension.GetCount: integer;
begin
  Result := FPoints.Count;
end;

procedure TElCRLDistributionPointsExtension.SetCount(Value: integer);
var
  P: TElDistributionPoint;
begin
  if Value < FPoints.Count then
  begin
    while FPoints.Count > Value do
    begin
      P := TElDistributionPoint(FPoints[FPoints.Count - 1]);
      FPoints.Delete(FPoints.Count - 1);
      P.Free;
    end;
  end
  else
    while FPoints.Count < Value do
      FPoints.Add(TElDistributionPoint.Create);
end;

procedure TElCRLDistributionPointsExtension.ClearList;
begin
  while FPoints.Count > 0 do
  begin
    TElDistributionPoint(FPoints.Last).Free;
    FPoints.Count := FPoints.Count -1;
  end;
end;

procedure TElCRLDistributionPointsExtension.Remove(Index: integer);
begin
  TElDistributionPoint(FPoints[Index]).Free;
  FPoints.Delete(Index);
end;

constructor TElCRLDistributionPointsExtension.Create;
begin
  inherited;
  FPoints := TList.Create;
end;

destructor TElCRLDistributionPointsExtension.Destroy;
begin
  ClearList;
  FreeAndNil(FPoints);
  inherited;
end;

procedure TElCRLDistributionPointsExtension.Clear;
begin
  ClearList;
end;

function TElCRLDistributionPointsExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$1F);
end;

procedure TElCRLDistributionPointsExtension.SetOID(const Value: BufferType);
begin
end;

procedure TElCRLDistributionPointsExtension.SetValue(const Value: BufferType);
var
  Tag, DP : TElASN1ConstrainedTag;
  I : integer;
  CurrTagIndex : integer;
  Point : TElDistributionPoint;
  Reasons : TSBCRLReasonFlags;
  S : BufferType;
  B : byte;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        for I := 0 to TElASN1ConstrainedTag(Tag.GetField(0)).Count - 1 do
        begin
          Point := TElDistributionPoint.Create;
          if TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I).CheckType(SB_ASN1_SEQUENCE, true) then
          begin
            DP := TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I));
            CurrTagIndex := 0;
            if (CurrTagIndex < DP.Count) and (DP.GetField(CurrTagIndex).CheckType(SB_ASN1_A0, true)) then
            begin
              if (TElASN1ConstrainedTag(DP.GetField(CurrTagIndex)).Count = 1) and
                (TElASN1ConstrainedTag(DP.GetField(CurrTagIndex)).GetField(0).CheckType(SB_ASN1_A0, true)) then
              begin
                //ParseGeneralNames(TElASN1ConstrainedTag(TElASN1ConstrainedTag(DP.GetField(CurrTagIndex)).GetField(0)),
                //  Point.FName, true);
                Point.FName.LoadFromTag(TElASN1ConstrainedTag(TElASN1ConstrainedTag(DP.GetField(CurrTagIndex)).GetField(0)), false);
              end
              else if (TElASN1ConstrainedTag(DP.GetField(CurrTagIndex)).Count = 1) and
                (TElASN1ConstrainedTag(DP.GetField(CurrTagIndex)).GetField(0).CheckType(SB_ASN1_A1, true)) then
              begin
                Point.FName.Add;
                Point.FName.Names[0].FNameType := gnDirectoryName;
                Point.FName.Names[0].DirectoryName.LoadFromTag(TElASN1ConstrainedTag(TElASN1ConstrainedTag(DP.GetField(CurrTagIndex)).GetField(0)){$ifndef HAS_DEF_PARAMS}, false{$endif});
              end;
              Inc(CurrTagIndex);
            end;
            if (CurrTagIndex < DP.Count) and (DP.GetField(CurrTagIndex).CheckType($81, false)) then
            begin
              S := TElASN1SimpleTag(DP.GetField(CurrTagIndex)).Content;
              if Length(S) >= 2 then
                B := PByte(@S[2])^
              else
                B := 0;
              Reasons := [];
              if (B and $400) = $400 then
                Reasons := Reasons + [rfAACompromise];
              if (B and $200) = $200 then
                Reasons := Reasons + [rfPrivilegeWithdrawn];
              if (B and $100) = $100 then
                Reasons := Reasons + [rfRemoveFromCRL];
              if (B and $80) = $80 then
                Reasons := Reasons + [rfObsolete1];
              if (B and $40) = $40 then
                Reasons := Reasons + [rfKeyCompromise];
              if (B and $20) = $20 then
                Reasons := Reasons + [rfCACompromise];
              if (B and $10) = $10 then
                Reasons := Reasons + [rfAffiliationChanged];
              if (B and $08) = $08 then
                Reasons := Reasons + [rfSuperseded];
              if (B and $04) = $04 then
                Reasons := Reasons + [rfCessationOfOperation];
              if (B and $02) = $02 then
                Reasons := Reasons + [rfCertificateHold];
              Point.FReasonFlags := Reasons;

              Inc(CurrTagIndex);
            end;
            if (CurrTagIndex < DP.Count) and (DP.GetField(CurrTagIndex).CheckType(SB_ASN1_A2, true)) then
            begin
              //ParseGeneralNames(TElASN1ConstrainedTag(DP.GetField(CurrTagIndex)),
              //  Point.FCRLIssuer, true);
              Point.FCRLIssuer.LoadFromTag(TElASN1ConstrainedTag(DP.GetField(CurrTagIndex)){$ifndef HAS_DEF_PARAMS}, false{$endif});
            end;
            FPoints.Add(Point);
          end;
        end;
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

constructor TElAccessDescription.Create;
begin
  inherited;
  FGeneralName := TElGeneralName.Create;
end;

destructor TElAccessDescription.Destroy;
begin
  FGeneralName.Free;
  inherited;
end;

function TElAuthorityInformationAccessExtension.GetAccessDescriptions(Index:
  integer): TElAccessDescription;
begin
  Result := TElAccessDescription(FList[Index]);
end;

function TElAuthorityInformationAccessExtension.GetCount: integer;
begin
  Result := FList.Count;
end;

procedure TElAuthorityInformationAccessExtension.SetCount(Value: integer);
var
  P: TElAccessDescription;
begin
  if Value < FList.Count then
  begin
    while FList.Count > Value do
    begin
      P := TElAccessDescription(FList[FList.Count - 1]);
      FList.Delete(FList.Count - 1);
      P.Free;
    end;
  end
  else
    while FList.Count < Value do
      FList.Add(TElAccessDescription.Create);
end;

procedure TElAuthorityInformationAccessExtension.ClearList;
var
  P: TElAccessDescription;
begin
  while FList.Count > 0 do
  begin
    P := TElAccessDescription(FList[0]);
    FList.Delete(0);
    P.Free;
  end;
end;

procedure TElAuthorityInformationAccessExtension.Remove(Index: integer);
var
  P: TElAccessDescription;
begin
  P := TElAccessDescription(FList[Index]);
  FList.Delete(Index);
  P.Free;
end;

constructor TElAuthorityInformationAccessExtension.Create;
begin
  inherited;
  FList := TList.Create;
end;

destructor TElAuthorityInformationAccessExtension.Destroy;
begin
  ClearList;
  FList.Free;
  inherited;
end;

procedure TElAuthorityInformationAccessExtension.Clear;
begin
  ClearList;
end;

function TElAuthorityInformationAccessExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$01#$01);
end;

procedure TElAuthorityInformationAccessExtension.SetOID(const Value:
  BufferType);
begin
end;

procedure TElAuthorityInformationAccessExtension.SetValue(const Value:
  BufferType);
var
  Tag : TElASN1ConstrainedTag;
  I : integer;
  AD : TElAccessDescription;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        for I := 0 to TElASN1ConstrainedTag(Tag.GetField(0)).Count - 1 do
        begin
          if (TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I).CheckType(SB_ASN1_SEQUENCE, true)) then
          begin
            if (TElASN1ConstrainedTag((TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I))).Count = 2) and
              (TElASN1ConstrainedTag((TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I))).GetField(0).CheckType(SB_ASN1_OBJECT, false)) then
            begin
              AD := TElAccessDescription.Create;
              AD.FAccessMethod := TElASN1SimpleTag(TElASN1ConstrainedTag((TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I))).GetField(0)).Content;
              //ParseGeneralName(TElASN1ConstrainedTag((TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I))).GetField(1),
              //  AD.FGeneralName);
              AD.FGeneralName.LoadFromTag(TElASN1ConstrainedTag((TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I))).GetField(1));
              FList.Add(AD);
            end;
          end
          else
            RaiseInvalidExtensionError;
        end;
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

procedure TElSubjectKeyIdentifierExtension.Clear;
begin
  SetLength(FKeyIdentifier, 0);
end;

function TElSubjectKeyIdentifierExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$0E);
end;

procedure TElSubjectKeyIdentifierExtension.SetOID(const Value: BufferType);
begin
end;

procedure TElSubjectKeyIdentifierExtension.SetValue(const Value: BufferType);
var
  Tag : TElASN1ConstrainedTag;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_OCTETSTRING, false)) then
        FKeyIdentifier := TElASN1SimpleTag(Tag.GetField(0)).Content
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

procedure TElKeyUsageExtension.Clear;
begin
  FDigitalSignature := false;
  FNonRepudiation := false;
  FKeyEncipherment := false;
  FDataEncipherment := false;
  FKeyAgreement := false;
  FKeyCertSign := false;
  FCRLSign := false;
  FEncipherOnly := false;
  FDecipherOnly := false;
end;

function TElKeyUsageExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$0F);
end;

procedure TElKeyUsageExtension.SetOID(const Value: BufferType);
begin
end;

procedure TElKeyUsageExtension.SetValue(const Value: BufferType);
var
  Tag : TElASN1ConstrainedTag;
  Size : integer;
  Tmp : ByteArray;
begin
  inherited;
  SetLength(Tmp, 0);
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_BITSTRING, false)) then
      begin
        Tmp := BytesOfString(TElASN1SimpleTag(Tag.GetField(0)).Content);
        Size := Length(Tmp);
        if Size >= 2 then
        begin
          FDigitalSignature := (Tmp[1] and $80) = $80;
          FNonRepudiation := (Tmp[1] and $40) = $40;
          FKeyEncipherment := (Tmp[1] and $20) = $20;
          FDataEncipherment := (Tmp[1] and $10) = $10;
          FKeyAgreement := (Tmp[1] and $08) = $08;
          FKeyCertSign := (Tmp[1] and $04) = $04;
          FCRLSign := (Tmp[1] and $02) = $02;
          FEncipherOnly := (Tmp[1] and $01) = $01;
        end;
        if Size >= 3 then
        begin
          FDecipherOnly := (Tmp[2] and $80) = $80;
        end;
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

procedure TElPrivateKeyUsagePeriodExtension.Clear;
begin
  FNotBefore := 0;
  FNotAfter := 0;
end;

function TElPrivateKeyUsagePeriodExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$10);
end;

procedure TElPrivateKeyUsagePeriodExtension.SetOID(const Value: BufferType);
begin
end;

procedure TElPrivateKeyUsagePeriodExtension.SetValue(const Value: BufferType);
var
  Tag, SeqTag : TElASN1ConstrainedTag;
  CurrIndex : integer;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        SeqTag := TElASN1ConstrainedTag(Tag.GetField(0));
        CurrIndex := 0;
        if (CurrIndex < SeqTag.Count) and (SeqTag.GetField(CurrIndex).CheckType($80, false)) then
        begin
          FNotBefore := GeneralizedTimeToDateTime((TElASN1SimpleTag(SeqTag.GetField(CurrIndex)).Content));
          Inc(CurrIndex);
        end;
        if (CurrIndex < SeqTag.Count) and (SeqTag.GetField(CurrIndex).CheckType($81, false)) then
          FNotAfter := GeneralizedTimeToDateTime((TElASN1SimpleTag(SeqTag.GetField(CurrIndex)).Content));
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

procedure TElBasicConstraintsExtension.Clear;
begin
  FCA := false;
  FPathLenConstraint := 0;
end;

function TElBasicConstraintsExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$13);
end;

procedure TElBasicConstraintsExtension.SetOID(const Value: BufferType);
begin
end;

procedure TElBasicConstraintsExtension.SetValue(const Value: BufferType);
var
  Tag : TElASN1ConstrainedTag;
  CurrTagIndex : integer;
  Cnt : BufferType;
  I, K : integer;
begin
  inherited;
  FCA := false;
  FPathLenConstraint := 0;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        CurrTagIndex := 0;
        if (TElASN1ConstrainedTag(Tag.GetField(0)).Count > CurrTagIndex) and
          (TElASN1ConstrainedTag(Tag.GetField(0)).GetField(CurrTagIndex).CheckType(SB_ASN1_BOOLEAN, false)) then
        begin
          Cnt := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(CurrTagIndex)).Content;
          FCA := (Length(Cnt) > 0) and (Cnt[1] = #$FF);
          Inc(CurrTagIndex);
        end
        else
          FCA := false;
        if (TElASN1ConstrainedTag(Tag.GetField(0)).Count > CurrTagIndex) and
          (TElASN1ConstrainedTag(Tag.GetField(0)).GetField(CurrTagIndex).CheckType(SB_ASN1_INTEGER, false)) then
        begin
          Cnt := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(CurrTagIndex)).Content;
          FPathLenConstraint := 0;
          I := Length(Cnt);
          K := 0;
          while (I > 0) and (K < 4) do
          begin
            FPathLenConstraint := FPathLenConstraint or (PByte(@Cnt[I])^ shl (K * 8));
            Dec(I);
            Inc(K);
          end;
        end;
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

procedure TElPolicyConstraintsExtension.Clear;
begin
  FRequireExplicitPolicy := 0;
  FInhibitPolicyMapping := 0;
end;

function TElPolicyConstraintsExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$24);
end;

procedure TElPolicyConstraintsExtension.SetOID(const Value: BufferType);
begin
end;

procedure TElPolicyConstraintsExtension.SetValue(const Value: BufferType);
var
  Tag, SeqTag : TElASN1ConstrainedTag;
  CurrIndex : integer;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        SeqTag := TElASN1ConstrainedTag(Tag.GetField(0));
        CurrIndex := 0;
        if (CurrIndex < SeqTag.Count) and (SeqTag.GetField(CurrIndex).CheckType($80, false)) then
        begin
          FRequireExplicitPolicy := ASN1ReadInteger(TElASN1SimpleTag(SeqTag.GetField(CurrIndex)));
          Inc(CurrIndex);
        end;
        if (CurrIndex < SeqTag.Count) and (SeqTag.GetField(CurrIndex).CheckType($81, false)) then
          FInhibitPolicyMapping := ASN1ReadInteger(TElASN1SimpleTag(SeqTag.GetField(CurrIndex)));
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;  
end;

constructor TElExtendedKeyUsageExtension.Create;
begin
  inherited;
  FCustomUsages := TStringList.Create;
end;

destructor TElExtendedKeyUsageExtension.Destroy;
begin
  FCustomUsages.Free;
  inherited;
end;

procedure TElExtendedKeyUsageExtension.Clear;
begin
  FServerAuthentication := false;
  FClientAuthentication := false;
  FCodeSigning := false;
  FEmailProtection := false;
  FTimeStamping := false;
  ClearCustomUsages;
end;

function TElExtendedKeyUsageExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$25);
end;

procedure TElExtendedKeyUsageExtension.SetOID(const Value: BufferType);
begin
end;

procedure TElExtendedKeyUsageExtension.SetValue(const Value: BufferType);
var
  Tag : TElASN1ConstrainedTag;
  I : integer;
  OID : BufferType;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if (Tag.LoadFromBuffer(@Value[1], Length(Value))) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        for I := 0 to TElASN1ConstrainedTag(Tag.GetField(0)).Count - 1 do
        begin
          if TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I).CheckType(SB_ASN1_OBJECT, false) then
          begin
            OID := TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(0)).GetField(I)).Content;
            if CompareContent(OID, SB_OID_SERVER_AUTH) then
              FServerAuthentication := true
            else if CompareContent(OID, SB_OID_CLIENT_AUTH) then
              FClientAuthentication := true
            else if CompareContent(OID, SB_OID_CODE_SIGNING) then
              FCodeSigning := true
            else if CompareContent(OID, SB_OID_EMAIL_PROT) then
              FEmailProtection := true
            else if CompareContent(OID, SB_OID_TIME_STAMPING) then
              FTimestamping := true
            else
              FCustomUsages.Add(OID);
          end
          else
            RaiseInvalidExtensionError;
        end;
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

function TElExtendedKeyUsageExtension.AddCustomUsage(const UsageOID : BufferType): integer;
begin
  Result := FCustomUsages.Add(UsageOID);
end;

procedure TElExtendedKeyUsageExtension.RemoveCustomUsage(Index: integer);
begin
  if (Index >= 0) and (Index < FCustomUsages.Count) then
    FCustomUsages.Delete(Index)
  else
    raise EElCertificateException.Create('List index out of bounds');
end;

procedure TElExtendedKeyUsageExtension.ClearCustomUsages;
begin
  FCustomUsages.Clear;
end;

function TElExtendedKeyUsageExtension.GetCustomUsage(Index: integer) : BufferType;
begin
  if (Index >= 0) and (Index < FCustomUsages.Count) then
    Result := FCustomUsages.Strings[Index]
  else
    raise EElCertificateException.Create('List index out of bounds');
end;

procedure TElExtendedKeyUsageExtension.SetCustomUsage(Index: integer; const Value: BufferType);
begin
  if (Index >= 0) and (Index < FCustomUsages.Count) then
    FCustomUsages.Strings[Index] := Value
  else
    raise EElCertificateException.Create('List index out of bounds');
end;

function TElExtendedKeyUsageExtension.GetCustomUsageCount: integer;
begin
  Result := FCustomUsages.Count;
end;

////////////////////////////////////////////////////////////////////////////////
// TElGeneralNames class

constructor TElGeneralNames.Create;
begin
  inherited;
  FNames := TList.Create;
end;

destructor TElGeneralNames.Destroy;
begin
  Clear;
  FNames.Free;
  inherited;
end;

function TElGeneralNames.GetCount : integer;
begin
  Result := FNames.Count;
end;

function TElGeneralNames.GetNames(Index: integer): TElGeneralName;
begin
  Result := TElGeneralName(FNames[Index]);
end;

function TElGeneralNames.Add : integer;
begin
  Result := FNames.Add(TElGeneralName.Create);
end;

procedure TElGeneralNames.Remove(Index: integer);
begin
  if (Index < 0) or (Index >= FNames.Count) then
    raise EElCertificateException.Create('List index out of bounds');
  TElGeneralName(FNames[Index]).Free;
  FNames.Delete(Index);
end;

procedure TElGeneralNames.Clear;
var
  I : integer;
begin
  for I := 0 to FNames.Count - 1 do
    TElGeneralName(FNames[I]).Free;
  FNames.Clear;
end;

function TElGeneralNames.LoadFromTag(Tag: TElASN1ConstrainedTag; 
    AllowRDN: boolean {$ifdef HAS_DEF_PARAMS}= false{$endif}): boolean;
var
  I, Index : integer;
begin
  Clear;
  Result := true;
  for I := 0 to Tag.Count - 1 do
  begin
    Index := Add;
    if not Names[Index].LoadFromTag(Tag.GetField(I)) then
    begin
      if (AllowRDN) and (Tag.GetField(I).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        Names[Index].DirectoryName.LoadFromTag(TElASN1ConstrainedTag(Tag.GetField(I)), false);
        Names[Index].NameType := gnDirectoryName;
      end
      else
      begin
        Remove(Index);
        Result := false;
      end;
    end;
  end;
end;

function TElGeneralNames.SaveToTag(Tag: TElASN1ConstrainedTag): boolean;
var
  I : integer;
  STag : TElASN1SimpleTag;
  Index: integer;
begin
  for I := 0 to Count - 1 do
  begin
    Index := Tag.AddField(false);
    STag := TElASN1SimpleTag(Tag.GetField(Index));
    if not Names[I].SaveToTag(STag) then
      Tag.RemoveField(Index);
  end;
  Tag.TagId := SB_ASN1_SEQUENCE;
  Result := true;
end;

function TElGeneralNames.FindNameByType(NameType : TSBGeneralName;
  StartIndex: integer {$ifdef HAS_DEF_PARAMS}= 0{$endif}) : integer;
var
  I : integer;
begin
  I := StartIndex;
  while (I < Count) and (Names[I].NameType <> NameType) do
    Inc(I);
  if I < Count then
    Result := I
  else
    Result := -1;
end;

function TElGeneralNames.ContainsEmailAddress(const Addr: string): boolean;
var
  CurrIndex : integer;
begin
  CurrIndex := -1;
  Result := false;
  repeat
    CurrIndex := FindNameByType(gnRFC822Name, CurrIndex + 1);
    if (CurrIndex >= 0) and (CompareStr(Names[CurrIndex].RFC822Name, Addr) = 0) then
    begin
      Result := true;
      Break;
    end;
  until CurrIndex < 0;
end;

procedure TElGeneralNames.Assign(Source: TPersistent);
var
  I : integer;
begin
  if not (Source is TElGeneralNames) then
    raise EElCertificateException.Create(SInvalidTypeCast);
  Clear;
  for I := 0 to TElGeneralNames(Source).Count - 1 do
  begin
    Add;
    Names[I].Assign(TElGeneralNames(Source).Names[I]);
  end;
end;

procedure TElGeneralNames.AssignTo(Dest: TPersistent);
begin
  if not (Dest is TElGeneralNames) then
    raise EElCertificateException.Create(SInvalidTypeCast);
  Dest.Assign(Self);
end;

////////////////////////////////////////////////////////////////////////////////
// TElNetscapeCertTypeExtension class

procedure TElNetscapeCertTypeExtension.SetValue(const Value: BufferType);
var
  LVal : Byte;
  Tag : TElASN1ConstrainedTag;
const
  ASN_BIT_0 = $80;
  ASN_BIT_1 = $40;
  ASN_BIT_2 = $20;
  ASN_BIT_3 = $10;
  ASN_BIT_4 = $08;
  ASN_BIT_5 = $04;
  ASN_BIT_6 = $02;
  ASN_BIT_7 = $01;
begin
  inherited;
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_BITSTRING, false)) then
      begin
        if Length(TElASN1SimpleTag(Tag.GetField(0)).Content) > 1 then
          LVal := PByte(@TElASN1SimpleTag(Tag.GetField(0)).Content[2])^
        else
          LVal := 0;

        if LVal and ASN_BIT_0 = ASN_BIT_0 then
        begin
          CertType := CertType + [nsSSLClient];
        end;
        if LVal and ASN_BIT_1 = ASN_BIT_1 then
        begin
          CertType := CertType + [nsSSLServer];
        end;
        if LVal and ASN_BIT_2 = ASN_BIT_2 then
        begin
          CertType := CertType + [nsSMIME];
        end;
        if LVal and ASN_BIT_3 = ASN_BIT_3 then
        begin
          CertType := CertType + [nsObjectSign];
        end;
        if LVal and ASN_BIT_5 = ASN_BIT_5 then
        begin
          CertType := CertType + [nsSSLCA];
        end;
        if LVal and ASN_BIT_6 = ASN_BIT_6 then
        begin
          CertType := CertType + [nsSMIMECA];
        end;
        if LVal and ASN_BIT_7 = ASN_BIT_7 then
        begin
          CertType := CertType + [nsObjectSignCA];
        end;
      end
      else
        RaiseInvalidExtensionError;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;

end;

procedure TElNetscapeCertTypeExtension.Clear;
begin
  inherited;
  FCertType := [];
end;

////////////////////////////////////////////////////////////////////////////////
// TElNetscapeString class

procedure TElNetscapeString.SetValue(const Value: BufferType);
var
  Tag : TElASN1ConstrainedTag;
begin
  Tag := TElASN1ConstrainedTag.Create;
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (not Tag.GetField(0).IsConstrained) and
        (Tag.GetField(0).TagID in [SB_ASN1_IA5STRING, SB_ASN1_VISIBLESTRING,
          SB_ASN1_PRINTABLESTRING]) then
      begin
        FContent := (TElASN1SimpleTag(Tag.GetField(0)).Content); 
      end;
    end
    else
      RaiseInvalidExtensionError;
  finally
    FreeAndNil(Tag);
  end;
end;

procedure TElNetscapeString.Clear;
begin
  inherited;
  SetLength(FContent, 0);
end;

////////////////////////////////////////////////////////////////////////////////
// Miscellaneous routines

function OctetsToIPAddress(const Octets : BufferType) : string;
var
  I : integer;
begin
  Result := '';
  for I := 1 to Length(Octets) do
    Result := Result + IntToStr(PByte(@Octets[I])^) + '.';
  if Length(Result) > 0 then
    Result := Copy(Result, 1, Length(Result) - 1);
end;

function IPAddressToOctets(const IPAddrStr : string) : BufferType;
var
  Tmp, A : string;
  P : byte;
  I, J : integer;
begin
  Tmp := IPAddrStr;
  SetLength(Result, 4);
  I := Pos('.', Tmp);
  J := 1;
  while I > 0 do
  begin
    A := Copy(Tmp, 1, I - 1);
    Delete(Tmp, 1, I);
    P := 0;
    try
      P := StrToInt(A);
    finally
      Result[J] := Chr(P);
    end;
    I := Pos('.', Tmp);
    inc(J);
  end;

  P := 0;
  try
    P := StrToInt(Tmp);
  finally
    Result[J] := Chr(P);
  end;
  inc(j);
  while j <= 4 do
  begin
    Result[j] := #0;
    inc(j);
  end;
end;






////////////////////////////////////////////////////////////////////////////////
// TElSubjectDirectoryAttributesExtension class

constructor TElSubjectDirectoryAttributesExtension.Create;
begin
  inherited;
  FAttributes := TElPKCS7Attributes.Create;
end;

destructor TElSubjectDirectoryAttributesExtension.Destroy;
begin
  FreeAndNil(FAttributes);
  inherited;
end;

procedure TElSubjectDirectoryAttributesExtension.Clear;
begin
  inherited;
  FAttributes.Count := 0;
end;

function TElSubjectDirectoryAttributesExtension.GetOID: BufferType;
begin
  Result := TBufferTypeConst(#$55#$1D#$09);
end;

procedure TElSubjectDirectoryAttributesExtension.SetOID(const Value: BufferType);
begin
  ;
end;

procedure TElSubjectDirectoryAttributesExtension.SetValue(const Value: BufferType);
var
  Tag, CTag, AttrTag, ValTag : TElASN1ConstrainedTag;
  I, J : integer;
  Size : integer;
  Buf : BufferType;
begin
  inherited;
  FAttributes.Count := 0;
  Tag := TElASN1ConstrainedTag.Create();
  try
    if Tag.LoadFromBuffer(@Value[1], Length(Value)) then
    begin
      if (Tag.Count = 1) and (Tag.GetField(0).CheckType(SB_ASN1_SEQUENCE, true)) then
      begin
        CTag := TElASN1ConstrainedTag(Tag.GetField(0));
        for I := 0 to CTag.Count - 1 do
        begin
          if CTag.GetField(I).CheckType(SB_ASN1_SEQUENCE, true) then
          begin
            AttrTag := TElASN1ConstrainedTag(CTag.GetField(I));
            if (AttrTag.Count = 2) and (AttrTag.GetField(0).CheckType(SB_ASN1_OBJECT, false)) and
              (AttrTag.GetField(1).CheckType(SB_ASN1_SET, true)) then
            begin
              FAttributes.Count := FAttributes.Count + 1;
              FAttributes.Attributes[FAttributes.Count - 1] := TElASN1SimpleTag(AttrTag.GetField(0)).Content;
              ValTag := TElASN1ConstrainedTag(AttrTag.GetField(1));
              for J := 0 to ValTag.Count - 1 do
              begin
                Size := 0;
                ValTag.GetField(J).SaveToBuffer(nil, Size);
                SetLength(Buf, Size);
                ValTag.GetField(J).SaveToBuffer(@Buf[1], Size);
                SetLength(Buf, Size);
                FAttributes.Values[I].Add(Buf);
              end;
            end;
          end;
        end;
      end;
    end;
  finally
    FreeAndNil(Tag);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TElExtensionWriter class

constructor TElExtensionWriter.Create(Exts : TElCertificateExtensions;
  CertExtensions: boolean {$ifdef HAS_DEF_PARAMS}= true{$endif});
begin
  inherited Create;
  FCertificateExtensions := Exts;
  FUseA3Prefix := CertExtensions;
end;

function TElExtensionWriter.WriteExtensions: BufferType;
var
  Lst: TStringList;
  I: integer;
begin
  Lst := TStringList.Create;
  if ceAuthorityKeyIdentifier in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionAuthorityKeyIdentifier);
  if ceSubjectKeyIdentifier in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionSubjectKeyIdentifier);
  if ceBasicConstraints in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionBasicConstraints);
  if ceKeyUsage in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionKeyUsage);
  if cePrivateKeyUsagePeriod in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionPrivateKeyUsagePeriod);
  if ceCertificatePolicies in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionCertificatePolicies);
  if cePolicyMappings in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionPolicyMappings);
  if ceSubjectAlternativeName in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionSubjectAltName);
  if ceIssuerAlternativeName in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionIssuerAltName);
  if ceNameConstraints in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionNameConstraints);
  if cePolicyConstraints in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionPolicyConstraints);
  if ceExtendedKeyUsage in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionExtendedKeyUsage);
  if ceCRLDistributionPoints in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionCRLDistributionPoints);
  if ceAuthorityInformationAccess in FCertificateExtensions.Included then
    Lst.Add(WriteExtensionAuthorityInformationAccess);
   //JPM additions
  if ceNetscapeCertType in FCertificateExtensions.Included then
  begin
    Lst.Add(WriteExtensionNetscapeCertType);
  end;
  if ceNetscapeBaseURL in FCertificateExtensions.Included then
  begin
    Lst.Add(WriteExtensionNetscapeString(TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$02),
        (Extensions.NetscapeBaseURL.Content)));
  end;
  if ceNetscapeRevokeURL in FCertificateExtensions.Included then
  begin
    Lst.Add(WriteExtensionNetscapeString(TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$03),
        (Extensions.NetscapeRevokeURL.Content)));
  end;
  if ceNetscapeCARevokeURL in FCertificateExtensions.Included then
  begin
    Lst.Add(WriteExtensionNetscapeString(TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$04),
        (Extensions.NetscapeCARevokeURL.Content)));
  end;
  if ceNetscapeRenewalURL in FCertificateExtensions.Included then
  begin
    Lst.Add(WriteExtensionNetscapeString(TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$07),
        (Extensions.NetscapeRenewalURL.Content)));
  end;
  if ceNetscapeCAPolicyURL in FCertificateExtensions.Included then
  begin
    Lst.Add(WriteExtensionNetscapeString(TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$08),
        (Extensions.NetscapeCAPolicy.Content)));
  end;
  if ceNetscapeServerName in FCertificateExtensions.Included then
  begin
    Lst.Add(WriteExtensionNetscapeString(TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$0C),
        (Extensions.NetscapeServerName.Content)));
  end;
  if ceNetscapeComment in FCertificateExtensions.Included then
  begin
    Lst.Add(WriteExtensionNetscapeString(TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$0D),
        (Extensions.NetscapeComment.Content)));
  end;
  if ceCommonName in FCertificateExtensions.Included then
  begin
    Lst.Add(WriteExtension(TBufferTypeConst(#$55#$04#$03),
        False, WritePrintableString((Extensions.CommonName.Content))));
  end;
  //
  if ceSubjectDirectoryAttributes in FCertificateExtensions.Included then
  begin
    if (FCertificateExtensions.SubjectDirectoryAttributes.Attributes.Count > 0) then
      Lst.Add(WriteExtensionSubjectDirectoryAttributes);
  end;                                            
  
  for I := 0 to FCertificateExtensions.OtherCount - 1 do
  begin
    Lst.Add(WriteExtension(FCertificateExtensions.OtherExtensions[I].OID,
      FCertificateExtensions.OtherExtensions[I].Critical,
      FCertificateExtensions.OtherExtensions[I].Value));
  end;
  Result := WriteSequence(Lst);
  if FUseA3Prefix then
    Result := TBufferTypeConst(#$A3) + WriteSize(Length(Result)) + Result;
  
  Lst.Free;
end;

function TElExtensionWriter.WriteExtension(const OID: BufferType; Critical: boolean; const Value: BufferType): BufferType;
var
  Lst: TStringList;
begin
  Lst := TStringList.Create;
  Lst.Add(WriteOID(OID));
  Lst.Add(WriteBoolean(Critical));
  Lst.Add(WriteOctetString(Value));
  Result := WriteSequence(Lst);
  Lst.Free;
end;

function TElExtensionWriter.WriteExtensionBasicConstraints: BufferType;
var
  Lst: TStringList;
  I: integer;
begin
  Lst := TStringList.Create;
  Lst.Add(WriteBoolean(FCertificateExtensions.BasicConstraints.CA));
  if FCertificateExtensions.BasicConstraints.PathLenConstraint >= 0 then
  begin
    I := FCertificateExtensions.BasicConstraints.PathLenConstraint;
    Lst.Add(WriteInteger(SwapSomeInt(I)));
  end;
  { Must be critical in CA certificates }
  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$13), 
        FCertificateExtensions.BasicConstraints.Critical, WriteSequence(Lst));
  Lst.Free;
end;

function TElExtensionWriter.WriteExtensionKeyUsage: BufferType;
var
  B1, B2: byte;
  Str: BufferType;
begin
  B1 := 0;
  B2 := 0;
  if FCertificateExtensions.KeyUsage.DigitalSignature then
    B1 := B1 or $80;
  if FCertificateExtensions.KeyUsage.NonRepudiation then
    B1 := B1 or $40;
  if FCertificateExtensions.KeyUsage.KeyEncipherment then
    B1 := B1 or $20;
  if FCertificateExtensions.KeyUsage.DataEncipherment then
    B1 := B1 or $10;
  if FCertificateExtensions.KeyUsage.KeyAgreement then
    B1 := B1 or $08;
  if FCertificateExtensions.KeyUsage.KeyCertSign then
    B1 := B1 or $04;
  if FCertificateExtensions.KeyUsage.CRLSign then
    B1 := B1 or $02;
  if FCertificateExtensions.KeyUsage.EncipherOnly then
    B1 := B1 or $01;
  if FCertificateExtensions.KeyUsage.DecipherOnly then
    B2 := B2 or $80;
  SetLength(Str, 2);
  Str[1] := Chr(B1);
  Str[2] := Chr(B2);
  { Should be Critical }
  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$0F), 
        FCertificateExtensions.KeyUsage.Critical, WriteBitString(Str));
end;


function TElExtensionWriter.WriteExtensionPrivateKeyUsagePeriod: BufferType;
var
  Lst: TStringList;
  Tmp: string;
begin
  Lst := TStringList.Create;
  Tmp := WriteGeneralizedTime(FCertificateExtensions.PrivateKeyUsagePeriod.NotBefore);
  Tmp[1] := #$80;
  Lst.Add(Tmp);
  Tmp := WriteGeneralizedTime(FCertificateExtensions.PrivateKeyUsagePeriod.NotAfter);
  Tmp[1] := #$81;
  Lst.Add(Tmp);
  { Must be not critical }
  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$10), FCertificateExtensions.PrivateKeyUsagePeriod.Critical,
    WriteSequence(Lst));
  Lst.Free;
end;

function TElExtensionWriter.WriteExtensionSubjectAltName: BufferType;
var
  Tag: TElASN1ConstrainedTag;
  Size: integer;
  Tmp: BufferType;
begin
  Tag := TElASN1ConstrainedTag.Create;
  try
    FCertificateExtensions.SubjectAlternativeName.Content.SaveToTag(Tag);
    Size := 0;
    Tag.SaveToBuffer(nil, Size);
    SetLength(Tmp, Size);
    Tag.SaveToBuffer(@Tmp[1], Size);
    SetLength(Tmp, Size);
  finally
    FreeAndNil(Tag);
  end;

  { Critical unknown }
  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$11),
    FCertificateExtensions.SubjectAlternativeName.Critical, Tmp);
end;

function TElExtensionWriter.WriteExtensionIssuerAltName: BufferType;
var
  Tmp: BufferType;
  Size: integer;
  Tag: TElASN1ConstrainedTag;
begin
  Tag := TElASN1ConstrainedTag.Create;
  try
    FCertificateExtensions.IssuerAlternativeName.Content.SaveToTag(Tag);
    Size := 0;
    Tag.SaveToBuffer(nil, Size);
    SetLength(Tmp, Size);
    Tag.SaveToBuffer(@Tmp[1], Size);
    SetLength(Tmp, Size);
  finally
    FreeAndNil(Tag);
  end;

  { Critical unknown }
  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$12),
    FCertificateExtensions.IssuerAlternativeName.Critical,
    Tmp);
end;

function TElExtensionWriter.WriteExtensionPolicyMappings: BufferType;
var
  Lst, TmpLst: TStringList;
  I: integer;
begin
  Lst := TStringList.Create;
  TmpLst := TStringList.Create;
  TmpLst.Add('');
  TmpLst.Add('');
  for I := 0 to FCertificateExtensions.PolicyMappings.Count - 1 do
  begin
    TmpLst[0] := WriteOID(FCertificateExtensions.PolicyMappings.Policies[I].IssuerDomainPolicy);
    TmpLst[1] := WriteOID(FCertificateExtensions.PolicyMappings.Policies[I].SubjectDomainPolicy);
    Lst.Add(WriteSequence(TmpLst));
  end;
  { Must be not critical }
  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$21), 
    FCertificateExtensions.PolicyMappings.Critical, WriteSequence(Lst));
  Lst.Free;
  TmpLst.Free;
end;

function TElExtensionWriter.WriteExtensionNameConstraints: BufferType;
var
  Lst: TStringList;
  TmpLst, OutmostLst: TStringList;
  Tmp: BufferType;
  I: integer;
  Size: integer;
  P: TElGeneralName;
  Tag: TElASN1SimpleTag;
begin
  Lst := TStringList.Create;
  TmpLst := TStringList.Create;
  OutmostLst := TStringList.Create;
  TmpLst.Add('');
  TmpLst.Add('');
  TmpLst.Add('');
  for I := 0 to FCertificateExtensions.NameConstraints.PermittedCount - 1 do
  begin
    P := FCertificateExtensions.NameConstraints.PermittedSubtrees[I].Base;
    //TmpLst[0] := WriteGeneralName(P);
    Tag := TElASN1SimpleTag.Create;
    try
      P.SaveToTag(Tag);
      Size := 0;
      Tag.SaveToBuffer(nil, Size);
      SetLength(Tmp, Size);
      Tag.SaveToBuffer(@Tmp[1], Size);
      SetLength(Tmp, Size);
      TmpLst[0] := Tmp;
    finally
      Tag.Free;
    end;

    Tmp := WriteInteger(FCertificateExtensions.NameConstraints.PermittedSubtrees[I].Minimum);
    //TmpLst[1] := WritePrimitive($A0, Tmp);
    Tmp[1] := Chr($80);
    TmpLst[1] := Tmp;

    Tmp := WriteInteger(FCertificateExtensions.NameConstraints.PermittedSubtrees[I].Maximum);
    //TmpLst[2] := WritePrimitive($A1, Tmp);
    Tmp[1] := Chr($81);
    TmpLst[2] := Tmp;
    Lst.Add(WriteSequence(TmpLst));
  end;
  Tmp := WriteSequence(Lst);
  //OutmostLst.Add(WritePrimitive($A0, Tmp));
  Tmp[1] := Chr($A0);
  OutmostLst.Add(Tmp);
  Lst.Clear;
  for I := 0 to FCertificateExtensions.NameConstraints.ExcludedCount - 1 do
  begin
    P := FCertificateExtensions.NameConstraints.ExcludedSubtrees[I].Base;
    //TmpLst[0] := WriteGeneralName(P);
    Tag := TElASN1SimpleTag.Create;
    try
      P.SaveToTag(Tag);
      Size := 0;
      Tag.SaveToBuffer(nil, Size);
      SetLength(Tmp, Size);
      Tag.SaveToBuffer(@Tmp[1], Size);
      SetLength(Tmp, Size);
      TmpLst[0] := Tmp;
    finally
      Tag.Free;
    end;

    Tmp := WriteInteger(FCertificateExtensions.NameConstraints.ExcludedSubtrees[I].Minimum);
    // TmpLst[1] := WritePrimitive($A0, Tmp);
    Tmp[1] := Chr($80);
    TmpLst[1] := Tmp;

    Tmp := WriteInteger(FCertificateExtensions.NameConstraints.ExcludedSubtrees[I].Maximum);
    // TmpLst[2] := WritePrimitive($A1, Tmp);
    Tmp[1] := Chr($81);
    TmpLst[2] := Tmp;

    Lst.Add(WriteSequence(TmpLst));
  end;
  Tmp := WriteSequence(Lst);
  Tmp[1] := Chr($A1);
  OutmostLst.Add(Tmp);
  //OutmostLst.Add(WritePrimitive($A1, Tmp));

  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$1E),
    FCertificateExtensions.NameConstraints.Critical, WriteSequence(OutmostLst));
  Lst.Free;
  TmpLst.Free;
  OutmostLst.Free;
end;

function TElExtensionWriter.WriteExtensionPolicyConstraints: BufferType;
var
  Lst: TStringList;
  Tmp: BufferType;
begin
  Lst := TStringList.Create;
  Lst.Add(WriteInteger(FCertificateExtensions.PolicyConstraints.RequireExplicitPolicy));
  Lst.Add(WriteInteger(FCertificateExtensions.PolicyConstraints.InhibitPolicyMapping));

  Tmp := Lst[0];
  Tmp[1] := Chr($80);
  Lst[0] := Tmp;

  Tmp := Lst[1];
  Tmp[1] := Chr($81);
  Lst[1] := Tmp;

  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$24),
    FCertificateExtensions.PolicyConstraints.Critical, WriteSequence(Lst));
  Lst.Free;
end;

function TElExtensionWriter.WriteExtensionExtendedKeyUsage: BufferType;
var
  Lst: TStringList;
  I: integer;
begin
  Lst := TStringList.Create;
  if FCertificateExtensions.ExtendedKeyUsage.ServerAuthentication then
    Lst.Add(WriteOID(TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$03#$01)));
  if FCertificateExtensions.ExtendedKeyUsage.ClientAuthentication then
    Lst.Add(WriteOID(TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$03#$02)));
  if FCertificateExtensions.ExtendedKeyUsage.CodeSigning then
    Lst.Add(WriteOID(TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$03#$03)));
  if FCertificateExtensions.ExtendedKeyUsage.EmailProtection then
    Lst.Add(WriteOID(TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$03#$04)));
  if FCertificateExtensions.ExtendedKeyUsage.TimeStamping then
    Lst.Add(WriteOID(TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$03#$08)));
  for I := 0 to FCertificateExtensions.ExtendedKeyUsage.CustomUsageCount - 1 do
    Lst.Add(WriteOID(FCertificateExtensions.ExtendedKeyUsage.CustomUsages[I]));
  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$25),
    FCertificateExtensions.ExtendedKeyUsage.Critical, WriteSequence(Lst));
    
  Lst.Free;
end;

function TElExtensionWriter.WriteExtensionCertificatePolicies: BufferType;
var
  Lst: TStringList;
  TmpBuf: BufferType;
  I: integer;
begin
  Lst := TStringList.Create;
  for I := 0 to FCertificateExtensions.CertificatePolicies.Count - 1 do
  begin
    TmpBuf := WritePolicyInformation(TElSinglePolicyInformation(FCertificateExtensions.CertificatePolicies.PolicyInformation[I]));
    Lst.Add(TmpBuf);
  end;
  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$20),
    FCertificateExtensions.CertificatePolicies.Critical, WriteSequence(Lst));
  Lst.Free;
end;

function TElExtensionWriter.WriteExtensionAuthorityKeyIdentifier: BufferType;
var
  Lst: TStringList;
  Tmp: BufferType;
  Size: integer;
  Tag: TElASN1ConstrainedTag;
begin
  Lst := TStringList.Create;
  Tmp := WriteOctetString(FCertificateExtensions.AuthorityKeyIdentifier.KeyIdentifier);
  Tmp[1] := Chr($80);
  Lst.Add(Tmp);

  if FCertificateExtensions.AuthorityKeyIdentifier.AuthorityCertIssuer.Count > 0 then
  begin
    Tag := TElASN1ConstrainedTag.Create;
    try
      FCertificateExtensions.AuthorityKeyIdentifier.AuthorityCertIssuer.SaveToTag(Tag);
      Size := 0;
      Tag.SaveToBuffer(nil, Size);
      SetLength(Tmp, Size);
      Tag.SaveToBuffer(@Tmp[1], Size);
      SetLength(Tmp, Size);
    finally
      FreeAndNil(Tag);
    end;

    Tmp[1] := #$A1;
    Lst.Add(Tmp);
  end;

  Tmp := FCertificateExtensions.AuthorityKeyIdentifier.AuthorityCertSerial;
  if Length(Tmp) <> 0 then
    Lst.Add(WritePrimitive($82, Tmp));
  { Must Not be critical }
  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$23),
    FCertificateExtensions.AuthorityKeyIdentifier.Critical, WriteSequence(Lst));

  Lst.Free;
end;

function TElExtensionWriter.WriteExtensionSubjectKeyIdentifier: BufferType;
var
  Tmp: BufferType;
begin
  { Must not be critical }
  Tmp := WriteOctetString(FCertificateExtensions.SubjectKeyIdentifier.KeyIdentifier);
  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$0E),
    FCertificateExtensions.SubjectKeyIdentifier.Critical, Tmp);
end;

function TElExtensionWriter.WriteExtensionCRLDistributionPoints: BufferType;
var
  Lst: TStringList;
  TmpBuf: BufferType;
  I: integer;
begin
  Lst := TStringList.Create;
  for I := 0 to FCertificateExtensions.CRLDistributionPoints.Count - 1 do
  begin
    TmpBuf := WriteDistributionPoint(TElDistributionPoint(FCertificateExtensions.CRLDistributionPoints.DistributionPoints[I]));
    Lst.Add(TmpBuf);
  end;
  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$1F), 
    FCertificateExtensions.CRLDistributionPoints.Critical, WriteSequence(Lst));
  Lst.Free;
end;

function TElExtensionWriter.WriteExtensionAuthorityInformationAccess: BufferType;
var
  Lst, TmpLst: TStringList;
  I: integer;
  P: TElAccessDescription;
  Tag: TElASN1SimpleTag;
  Size: integer;
  Tmp: BufferType;
begin
  Lst := TStringList.Create;
  TmpLst := TStringList.Create;
  TmpLst.Add('');
  TmpLst.Add('');
  for I := 0 to FCertificateExtensions.AuthorityInformationAccess.Count - 1 do
  begin
    P := TElAccessDescription(FCertificateExtensions.AuthorityInformationAccess.AccessDescriptions[I]);
    TmpLst[0] := WriteOID(P.AccessMethod);

    //TmpLst[1] := WriteGeneralName(P.AccessLocation);
    Tag := TElASN1SimpleTag.Create;
    try
      P.AccessLocation.SaveToTag(Tag);
      Size := 0;
      Tag.SaveToBuffer(nil, Size);
      SetLength(Tmp, Size);
      Tag.SaveToBuffer(@Tmp[1], Size);
      SetLength(Tmp, Size);
    finally
      FreeAndNil(Tag);
    end;
    TmpLst[1] := Tmp;

    Lst.Add(WriteSequence(TmpLst));
  end;
  Result := WriteExtension(TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$01#$01),
    FCertificateExtensions.AuthorityInformationAccess.Critical,
    WriteSequence(Lst));
  TmpLst.Free;
  Lst.Free;
end;

function TElExtensionWriter.WriteExtensionNetscapeCertType: BufferType;
var
  s: BufferType;
  LVal: Byte;
const
  ASN_BIT_0 = $80;
  ASN_BIT_1 = $40;
  ASN_BIT_2 = $20;
  ASN_BIT_3 = $10;
  ASN_BIT_4 = $08;
  ASN_BIT_5 = $04;
  ASN_BIT_6 = $02;
  ASN_BIT_7 = $01;
begin
  LVal := 0;
  if nsSSLClient in Extensions.NetscapeCertType.CertType then
  begin
    LVal := LVal or ASN_BIT_0;
  end;
  if nsSSLServer in Extensions.NetscapeCertType.CertType then
  begin
    LVal := LVal or ASN_BIT_1;
  end;
  if nsSMIME in Extensions.NetscapeCertType.CertType then
  begin
    LVal := LVal or ASN_BIT_2;
  end;
  if nsObjectSign in Extensions.NetscapeCertType.CertType then
  begin
    LVal := LVal or ASN_BIT_3;
  end;
  if nsSSLCA in Extensions.NetscapeCertType.CertType then
  begin
    LVal := LVal or ASN_BIT_5;
  end;
  if nsSMIMECA in Extensions.NetscapeCertType.CertType then
  begin
    LVal := LVal or ASN_BIT_6;
  end;
  if nsObjectSignCA in Extensions.NetscapeCertType.CertType then
  begin
    LVal := LVal or ASN_BIT_7;
  end;
  SetLength(s, 1);
  s[1] := Char(LVal);
  Result := WriteExtension(TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$01), 
        False, WriteBitString(s));
end;

function TElExtensionWriter.WriteExtensionNetscapeString(const AOID: BufferType;
  const ANetStr: BufferType): BufferType;
begin
  Result := WriteExtension(AOID, False, WritePrimitive(asn1IA5String, ANetStr));
end;

function TElExtensionWriter.WritePolicyInformation(P: TElSinglePolicyInformation): BufferType;
var
  Lst, TmpLst, InfoLst, UserNoticeLst, NoticeRefLst, NoticeNumLst: TStringList;
  I: integer;
begin
  Lst := TStringList.Create;
  TmpLst := TStringList.Create;
  InfoLst := TStringList.Create;
  Lst.Add(WriteOID(P.PolicyIdentifier));
  // adding policy qualifiers
  {1}
  if P.CPSURI <> '' then
  begin
    InfoLst.Add(WriteOID(TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$02#$01)));
    InfoLst.Add(WriteStringPrimitive($16, P.CPSURI));
  end;
  TmpLst.Add(WriteSequence(InfoLst));
  {2}
  InfoLst.Clear;
  { checkup usernotice to be turned on }
  // if UserNotice is On then
  UserNoticeLst := TStringList.Create;
  NoticeRefLst := TStringList.Create;
  NoticeNumLst := TStringList.Create;
  { Info-ID }
  InfoLst.Add(WriteOID(TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$02#$02)));
    { Organization }
  NoticeRefLst.Add(WriteStringPrimitive($16, P.UserNotice.Organization));
  for I := 0 to P.UserNotice.NoticeNumbersCount - 1 do
    NoticeNumLst.Add(WriteInteger(P.UserNotice.NoticeNumbers[I]));
    { Notice numbers }
  NoticeRefLst.Add(WriteSequence(NoticeNumLst));
  { Notice Ref }
  UserNoticeLst.Add(WriteSequence(NoticeRefLst));
  { Explicit Text }
  UserNoticeLst.Add(WriteVisibleString(P.UserNotice.ExplicitText));
  { Info-Qualifier }
  InfoLst.Add(WriteSequence(UserNoticeLst));
  FreeAndNil(UserNoticeLst);
  FreeAndNil(NoticeRefLst);
  FreeAndNil(NoticeNumLst);
  TmpLst.Add(WriteSequence(InfoLst));

  Lst.Add(WriteSequence(TmpLst));
  Result := WriteSequence(Lst);
  
  Lst.Free;
  TmpLst.Free;
  InfoLst.Free;
end;

function TElExtensionWriter.WriteDistributionPoint(P: TElDistributionPoint): BufferType;
var
  Lst: TStringList;
  Tmp: BufferType;
  B1: byte;
  Tag: TElASN1ConstrainedTag;
  Size: integer;
begin
  Lst := TStringList.Create;

  if P.Name.Count > 0 then
  begin
    Tag := TElASN1ConstrainedTag.Create;
    try
      P.Name.SaveToTag(Tag);
      Size := 0;
      Tag.SaveToBuffer(nil, Size);
      SetLength(Tmp, Size);
      Tag.SaveToBuffer(@Tmp[1], Size);
      SetLength(Tmp, Size);
      Tmp[1] := #$A0;
    finally
      FreeAndNil(Tag);
    end;
    Lst.Add(WritePrimitive($A0, Tmp));
  end;

  B1 := 0;
  if rfAACompromise in P.ReasonFlags then
    B1 := B1 or $400;
  if rfPrivilegeWithdrawn in P.ReasonFlags then
    B1 := B1 or $200;
  if rfRemoveFromCRL in P.ReasonFlags then
    B1 := B1 or $100;
  if rfObsolete1 in P.ReasonFlags then
    B1 := B1 or $80;
  if rfUnspecified in P.ReasonFlags then
    B1 := B1 or $1;
  if rfKeyCompromise in P.ReasonFlags then
    B1 := B1 or $40;
  if rfCACompromise in P.ReasonFlags then
    B1 := B1 or $20;
  if rfAffiliationChanged in P.ReasonFlags then
    B1 := B1 or $10;
  if rfSuperseded in P.ReasonFlags then
    B1 := B1 or $08;
  if rfCessationOfOperation in P.ReasonFlags then
    B1 := B1 or $04;
  if rfCertificateHold in P.ReasonFlags then
    B1 := B1 or $02;
  Tmp := WriteBitString(Chr(B1));
  //Lst.Add(WritePrimitive($A1, Tmp));
  Tmp[1] := Chr($81);
  Lst.Add(Tmp); //!!

  if (P.CRLIssuer.Count > 0) then
  begin
    //Tmp := WriteGeneralNamesSeq(P.CRLIssuer);
    Tag := TElASN1ConstrainedTag.Create;
    try
      P.CRLIssuer.SaveToTag(Tag);
      Size := 0;
      Tag.SaveToBuffer(nil, Size);
      SetLength(Tmp, Size);
      Tag.SaveToBuffer(@Tmp[1], Size);
      SetLength(Tmp, Size);
      Tmp[1] := #$A2;
    finally
      FreeAndNil(Tag);
    end;
    Lst.Add(Tmp);
  end;

  Result := WriteSequence(Lst);
  Lst.Free;
end;

function TElExtensionWriter.WriteExtensionSubjectDirectoryAttributes : BufferType;
var
  Tag : TElASN1ConstrainedTag;
  Size: integer;
  Buf : BufferType;
begin
  Tag := TElASN1ConstrainedTag.Create;
  try
    SaveAttributes(Tag, Extensions.SubjectDirectoryAttributes.Attributes, SB_ASN1_SEQUENCE);
    Size := 0;
    Tag.SaveToBuffer(nil, Size);
    SetLength(Buf, Size);
    Tag.SaveToBuffer(@Buf[1], Size);
    SetLength(Buf, Size);
  finally
    FreeAndNil(Tag);
  end;
  Result := WriteExtension(TBufferTypeConst(#$55#$1D#$09), False, Buf);
end;

////////////////////////////////////////////////////////////////////////////////
// TElExtensionReader class

constructor TElExtensionReader.Create(Exts : TElCertificateExtensions;
  StrictMode : boolean);
begin
  inherited Create;
  FCertificateExtensions := Exts;
  FStrictMode := StrictMode;
end;

procedure TElExtensionReader.ParseExtension(const OID: BufferType; Critical: boolean;
  const Content: BufferType);
var
  Exten: TElCustomExtension;
begin
  if CompareContent(OID, TBufferTypeConst(#$55#$1D#$13)) or
    CompareContent(OID, TBufferTypeConst(#$55#$1D#$0A)) then
  begin
    Exten := FCertificateExtensions.BasicConstraints;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceBasicConstraints];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$23)) or
    CompareContent(OID, TBufferTypeConst(#$55#$1D#$01)) then
  begin
    Exten := FCertificateExtensions.AuthorityKeyIdentifier;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceAuthorityKeyIdentifier];
    if (FStrictMode) and Critical then
      raise EElCertificateException.Create(SNonCriticalExtensionMarkedAsCritical);
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$0E)) then
  begin
    Exten := FCertificateExtensions.SubjectKeyIdentifier;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceSubjectKeyIdentifier];
    if (FStrictMode) and Critical then
      raise EElCertificateException.Create(SNonCriticalExtensionMarkedAsCritical);
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$0F)) then
  begin
    Exten := FCertificateExtensions.KeyUsage;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceKeyUsage];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$10)) then
  begin
    Exten := FCertificateExtensions.PrivateKeyUsagePeriod;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [cePrivateKeyUsagePeriod];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$20)) then
  begin
    Exten := FCertificateExtensions.CertificatePolicies;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceCertificatePolicies];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$21)) then
  begin
    Exten := FCertificateExtensions.PolicyMappings;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [cePolicyMappings];
    if (FStrictMode) and Critical then
      raise EElCertificateException.Create(SNonCriticalExtensionMarkedAsCritical);
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$24)) then
  begin
    Exten := FCertificateExtensions.PolicyConstraints;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [cePolicyConstraints];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$25)) then
  begin
    Exten := FCertificateExtensions.ExtendedKeyUsage;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceExtendedKeyUsage];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$2B#$06#$01#$05#$05#$07#$01#$01)) then
  begin
    Exten := FCertificateExtensions.AuthorityInformationAccess;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceAuthorityInformationAccess];
    if (FStrictMode) and Critical then
      raise EElCertificateException.Create(SNonCriticalExtensionMarkedAsCritical);
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$1E)) then
  begin
    Exten := FCertificateExtensions.NameConstraints;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceNameConstraints];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$1F)) then
  begin
    Exten := FCertificateExtensions.CRLDistributionPoints;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceCRLDistributionPoints];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$12)) then
  begin
    Exten := FCertificateExtensions.IssuerAlternativeName;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceIssuerAlternativeName];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$11)) then
  begin
    Exten := FCertificateExtensions.SubjectAlternativeName;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceSubjectAlternativeName];
  end
  else
  //JPM
    if CompareContent(OID, TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$01)) then
  begin
    Exten := FCertificateExtensions.NetscapeCertType;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceNetscapeCertType];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$02)) then
  begin
    Exten := FCertificateExtensions.NetscapeBaseURL;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceNetscapeBaseURL];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$03)) then
  begin
    Exten := FCertificateExtensions.NetscapeRevokeURL;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceNetscapeRevokeURL];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$04)) then
  begin
    Exten := FCertificateExtensions.NetscapeCARevokeURL;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceNetscapeCARevokeURL];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$07)) then
  begin
    Exten := FCertificateExtensions.NetscapeRenewalURL;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceNetscapeRenewalURL];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$08)) then
  begin
    Exten := FCertificateExtensions.NetscapeCAPolicy;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceNetscapeCAPolicyURL];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$0C)) then
  begin
    Exten := FCertificateExtensions.NetscapeServerName;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceNetscapeServerName];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$60#$86#$48#$01#$86#$F8#$42#$01#$0D)) then
  begin
    Exten := FCertificateExtensions.NetscapeComment;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceNetscapeComment];
  end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$04#$03)) then
  begin
    Exten := FCertificateExtensions.CommonName;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceCommonName];
  end
  //end
  else
    if CompareContent(OID, TBufferTypeConst(#$55#$1D#$09)) then
  begin
    Exten := FCertificateExtensions.SubjectDirectoryAttributes;
    FCertificateExtensions.Included := FCertificateExtensions.Included + [ceSubjectDirectoryAttributes];
  end
  else
  begin
    Exten := TElCustomExtension.Create;
    FCertificateExtensions.FOtherList.Add(Exten);
  end;
  if Assigned(Exten) then
  begin
    Exten.Critical := Critical;
    Exten.Value := Content;
    Exten.OID := OID;
  end;
end;

end.
