(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBPKCS11CertStorage;

(*

Change history (not complete)

26 June 2005

  Fixed certain things for better support of iKey tokens. tested on iKey 1032

*)

interface

uses
  SBRSA,
  SBDSA,
  SBASN1,
  SBPKCS11Common,
  //SBPKCS11Base,
  SBCustomCertStorage,
  Classes,
  {$ifndef LINUX}
  Windows,
  {$endif}
  SysUtils,
  SyncObjs,
  SBX509,
  SBUtils,
  SBSharedResource
;

{$ifndef SBB_NO_PKCS11}



type

  TElPKCS11NotificationFlag = (nfNone, nfSurrender);

  TElPKCS11UserType = (utSecurityOfficer, utUser);

  TElPKCS11KeyType = (ktRSA, ktDSA, ktDH, ktEC);

  TElPKCS11SessionState = (stROPublicSession, stROUserFunctions,
    stRWPublicSession, stRWUserFunctions, stRWSOFunctions);


  TElPKCS11Module = class;
  TElPKCS11SessionInfo = class;
  TElPKCS11CertStorage = class;

  TElPKCS11SlotInfo = class(TObject)
  protected

    FSlotID: CK_SLOT_ID;
    FConsumers : TList;

    FModule : TElPKCS11Module;
    FManufacturerID: string;
    FSlotDescription: string;
    FHWVersion: TPKCS11Version;
    FFWVersion: TPKCS11Version;
    FTokenPresent: Boolean;
    FLoggedIn: Integer;

    FTokenLabel: string;
    FTokenManID: string;
    FTokenModel: string;
    FTokenSerial: string;
    FTokenFlags: DWORD;

    FMaxSessionCount: DWORD;
    FSessionCount: DWORD;
    FMaxRwSessionCount: DWORD;
    FRwSessionCount: DWORD;
    FMaxPinLen: DWORD;
    FMinPinLen: DWORD;
    FTotalPublicMemory: DWORD;
    FFreePublicMemory: DWORD;
    FTotalPrivateMemory: DWORD;
    FFreePrivateMemory: DWORD;

    FSessionState: TElPKCS11SessionState;
    FHandle: CK_SESSION_HANDLE;
    FReadOnly: Boolean;

    FTokenHWVersion: TPKCS11Version;
    FTokenFWVersion: TPKCS11Version;
    FUTCTime: TDateTime;

    function GetLoggedIn: Boolean;
    procedure SetSlotData(SlotID: CK_SLOT_ID; SlotInfo: TPKCS11SlotInfo);
    procedure SetTokenData(TokenInfo: TPKCS11TokenInfo);

  public
    constructor Create;
    destructor Destroy; override;

    function PinNeeded: Boolean;
    procedure Refresh;

    property LoggedIn: Boolean read GetLoggedIn;
    property ReadOnly: Boolean read FReadOnly;
    property SlotManufacturerID: string read FManufacturerID;
    property SlotDescription: string read FSlotDescription;
    property SlotHardwareVersionHi: Byte read FHWVersion.Major;
    property SlotFirmwareVersionHi: Byte read FFWVersion.Major;
    property SlotHardwareVersionLo: Byte read FHWVersion.Minor;
    property SlotFirmwareVersionLo: Byte read FFWVersion.Minor;
    property TokenPresent: Boolean read FTokenPresent;

    property TokenHardwareVersionHi: Byte read FTokenHWVersion.Major;
    property TokenFirmwareVersionHi: Byte read FTokenFWVersion.Major;
    property TokenHardwareVersionLo: Byte read FTokenHWVersion.Minor;
    property TokenFirmwareVersionLo: Byte read FTokenFWVersion.Minor;
    property TokenManufacturerID: string read FTokenManID;
    property TokenLabel: string read FTokenLabel;
    property TokenModel: string read FTokenModel;
    property TokenSerial: string read FTokenSerial;

  end;

  TElPKCS11SessionInfo = class(TObject)
  protected
    FSlot: TElPKCS11SlotInfo;
    FStorage : TElPKCS11CertStorage;
    FSessionState: TElPKCS11SessionState;
    FHandle: CK_SESSION_HANDLE;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Login(UserType: TElPKCS11UserType; PIN: string); overload;
    procedure Logout;
    property Slot: TElPKCS11SlotInfo read FSlot{ write FSlot};
  end;

  TElPKCS11StgCtx = class(TObject)
  protected
    SlotInfo: TElPKCS11SlotInfo;
    Session: TElPKCS11SessionInfo;
    Handle: CK_OBJECT_HANDLE;
    PKHandle : CK_OBJECT_HANDLE;
    KeyType : TElPKCS11KeyType;
    Cert: TElX509Certificate;
    Hash: string;

  public
    destructor Destroy; override;

    procedure SetCertStorageName;

    function Sign(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize: integer): boolean;
    function Decrypt(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
      var OutSize: integer): boolean;
  end;

  TElPKCSNotifyEvent =  procedure(Sender: TObject;
    SessionInfo: TElPKCS11SessionInfo; NotificationFlag: TElPKCS11NotificationFlag;
    var AbortOperation: boolean) of object;

  TElPKCS11Module = class(TObject)
  private
    HLib: HModule;
    {$ifndef SBPKCS11ProxyDriver}
    FuncArray: TPKCS11FuncArray;
    {$endif}
    UseCount: integer;
    Name: string;
    FInit: boolean;
    FSlotList: TList;
    FUniqueID : ByteArray;
  protected
    FProviderDescription: string;
    procedure ReleaseSlots;

  public
    {$ifdef SBPKCS11ProxyDriver}
    FuncArray: TPKCS11FuncArray;
    function Load(ModuleName: string): CK_RV;
    {$endif}

    constructor Create;
    destructor Destroy; override;

    function GetFunctionList: Integer;
    property ProviderDescription: string read FProviderDescription write
        FProviderDescription;
  end;

  TElPKCS11ModuleList = class(TObject)
  private
    FList: TList;
    FRes: TElSharedResource;
  protected
    procedure ClearList;
    function LoadModule(ModuleName: string): TElPKCS11Module;


  public
    constructor Create;
    destructor Destroy; override;

    procedure UnloadModule(aModule: TElPKCS11Module);
  end;

  TElPKCS11CertStorage = class(TElCustomCertStorage)
  private
    FUniqueID: ByteArray;
  protected
    FModule: TElPKCS11Module;

    FSessionList: TList;
    FCtxList: TList;
    FOpened: boolean;
    FSessionCount: Integer;

    FDLLName: string;
    FOnNotification: TElPKCSNotifyEvent;


    procedure ClearCerts(Session : TElPKCS11SessionInfo);
    function GetCertificates(Index: integer): TElX509Certificate; override;
    function GetCount: integer; override;
    function GetSlot(Index: Integer): TElPKCS11SlotInfo;
    function GetSlotCount: Integer;
    function HandleNotification(hSession: CK_SESSION_HANDLE; aEvent:
      CK_NOTIFICATION): CK_RV;
    procedure DoNotification(SessionInfo: TElPKCS11SessionInfo; NotificationFlag:
      TElPKCS11NotificationFlag; var AbortOperation: boolean); virtual;
    function GetObjectAttribute(hSession : CK_SESSION_HANDLE; hObject :
        CK_OBJECT_HANDLE; Attribute : CK_ATTRIBUTE_TYPE): BufferType;
    function GetSession(Index: Integer): TElPKCS11SessionInfo;
    function GetSessionCount: Integer;
    procedure SetDLLName(const Value: string);
    function UpdateCertificate(Ctx: TElPKCS11StgCtx): TElX509Certificate;

  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;

    procedure Add(X509Certificate: TElX509Certificate;
      CopyPrivateKey: boolean = true); overload; override;
    procedure Add(SessionIndex : integer; X509Certificate: TElX509Certificate;
      CopyPrivateKey: boolean = true;
      Exportable : boolean = false); reintroduce; overload;
    procedure Close;
    procedure CloseAllSessions(SlotInfo : TElPKCS11SlotInfo);
    procedure Open;
    procedure CloseSession(SessionIndex: integer);

    function GetObjectByID(hSession : CK_SESSION_HANDLE; ObjectClass :
        CK_OBJECT_CLASS; ID : BufferType): CK_OBJECT_HANDLE;
    function GetObjectBySubjectID(hSession : CK_SESSION_HANDLE; ObjectClass :
        CK_OBJECT_CLASS; Subject, ID : BufferType): CK_OBJECT_HANDLE;
    function GetObjectID(hSession : CK_SESSION_HANDLE; hObject : CK_OBJECT_HANDLE):
        BufferType;
    function OpenSession(SlotIndex: integer; ReadOnly: boolean):
        TElPKCS11SessionInfo;
    procedure RefreshSlotCertHandles(SessionIndex : integer);
    
    procedure Remove(Index: integer); override;

    property Certificates[Index: integer]: TElX509Certificate read
      GetCertificates; 
    property Count: integer read GetCount; 

    property Slot[Index: Integer]: TElPKCS11SlotInfo read GetSlot;
    property Session[Index: Integer]: TElPKCS11SessionInfo read GetSession;
    property SlotCount: Integer read GetSlotCount;
    property SessionCount: Integer read GetSessionCount;
  published
    property DLLName: string read FDLLName write SetDLLName;
    property OnNotification: TElPKCSNotifyEvent read FOnNotification write
      FOnNotification;
  end;


function GetPKCS11String(Buffer: PChar; MaxLength: integer): string;
function GetContextByCertificate(Cert: TElX509Certificate): TElPKCS11StgCtx;


procedure Register;

{$endif SBB_NO_PKCS11}

implementation

{$ifndef SBB_NO_PKCS11}

resourcestring

  SPKCS11ProviderNotOpened = 'PKCS#11 storage is not opened';
  SNoPKCS11ProviderSpecified = 'No PKCS#11 provider DLL has been specified';
  SPKCS11ProviderNoFunctions =
    'PKCS#11 provider DLL doesn''t export all required functions';
  SPKCS11ProviderNotFound = 'PKCS#11 provider DLL could not be found or loaded';
  SPKCS11ProviderError = 'PKCS#11 provider DLL function returned fatal error';
  SIndexOutOfBounds = 'Index out of bounds';
  SPKCS11Error = 'PKCS#11 error';
  SAlgorithmDoesNotSupportEncryption = 'Certificate algorithm (%d) does not support encryption';
  SAlgorithmDoesNotSupportSigning = 'Certificate algorithm (%d) does not support signing';
  SInternalError = 'Internal error';
  SNotPKCS11Certificate = 'Not a PKCS#11 certificate';

var
  ModuleList: TElPKCS11ModuleList ;

procedure Register;
begin
  RegisterComponents('PKIBlackbox', [TElPKCS11CertStorage]);
end;

function PKCS11NotifyCallback(hSession: CK_SESSION_HANDLE; aEvent: CK_NOTIFICATION;
  pApplication: CK_VOID_PTR): CK_RV; cdecl;
begin
  if pApplication <> nil then
    result := TElPKCS11CertStorage(pApplication).HandleNotification(hSession,
      aEvent)
  else
    result := CKR_OK;
end;

procedure PKCS11CheckError(ResultCode: CK_ULONG);
begin
  if ResultCode <> CKR_OK then
    raise ECertStorageError.Create(SPKCS11Error, ResultCode{$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});
end;

function GetPKCS11String(Buffer: PChar; MaxLength: integer): string;
var
  i: integer;
begin
  for i := MaxLength downto 1 do
  begin
    if (Buffer[i - 1] <> ' ') and
      (Buffer[i - 1] <> #0) then
      break;
  end;
  SetLength(Result, i);
  if i > 0 then
    StrLCopy(PChar(Result), Buffer, i);
end;

function CreateMutexCallback(var MutexPtr : CK_VOID_PTR) : CK_RV; cdecl;
begin
  MutexPtr := TCriticalSection.Create;
  result := CKR_OK;
end;

function DestroyMutexCallback(MutexPtr : CK_VOID_PTR) : CK_RV; cdecl;
begin
  TCriticalSection(MutexPtr).Free;
  result := CKR_OK;
end;

function LockMutexCallback(MutexPtr : CK_VOID_PTR) : CK_RV; cdecl;
begin
  TCriticalSection(MutexPtr).Enter;
  result := CKR_OK;
end;

function UnlockMutexCallback(MutexPtr : CK_VOID_PTR) : CK_RV; cdecl;
begin
  TCriticalSection(MutexPtr).Leave;
  result := CKR_OK;
end;

procedure TElPKCS11CertStorage.Close;
begin
  if FOpened then
  begin
    if FSessionCount > 0 then
      CloseAllSessions(nil);
    ModuleList.UnloadModule(FModule);
    FModule := nil;
    FOpened := false;
  end;
end;

constructor TElPKCS11CertStorage.Create(Owner: TComponent);
begin
  inherited;
  FSessionList := TList.Create;
  FCtxList := TList.Create;
  SetLength(FUniqueID, 16);
  SBRndGenerate(@FUniqueID[0], Length(FUniqueID));
end;


destructor TElPKCS11CertStorage.Destroy;
begin
  Close;
  FreeAndNil(FSessionList);
  FreeAndNil(FCtxList);
  inherited;
end;



procedure TElPKCS11CertStorage.Add(SessionIndex : integer;
  X509Certificate: TElX509Certificate; CopyPrivateKey: boolean = true;
  Exportable : boolean = false);
type
  TCKAttributes = array of CK_ATTRIBUTE;

var Attribs  : TCKAttributes;
    PKAttribs: TCKAttributes;
    Cert     : TElX509Certificate;
    certClass: CK_OBJECT_CLASS;
    keyClass : CK_OBJECT_CLASS;
    certType : CK_CERTIFICATE_TYPE;
    keyType  : CK_KEY_TYPE;

    bTrue   : CK_BBOOL;
    bFalse  : CK_BBOOL;
    bVal    : CK_BBOOL;
    sLabel  : string;
    sID     : BufferType;
    sSerial : BufferType;
    sSubject: BufferType;
    sIssuer : BufferType;
    sValue  : BufferType;
    Buf1,
    Buf2,
    Buf3,
    Buf4,
    Buf5,
    Buf6,
    Buf7,
    Buf8    : BufferType;

    lVL     : word;
    hKey,
    hCert   : CK_OBJECT_HANDLE;
    CTX     : TElPKCS11StgCtx;
    SessionInfo : TElPKCS11SessionInfo;
    Hash    : TMessageDigest160;
    HashArr : BufferType;
    pkBuf   : Pointer;
    pkSize  : word;
    lVL1,
    lVL2,
    lVL3,
    lVL4,
    lVL5,
    lVL6,
    lVL7,
    lVL8    : integer;


  procedure AddAttribute(var Attributes: TCKAttributes; _type : CK_ATTRIBUTE_TYPE; pValue : CK_VOID_PTR; ulValueLen : CK_ULONG);
  var
    OldLen: integer;
  begin
    OldLen := Length(Attributes);
    SetLength(Attributes, OldLen + 1);
    Attributes[OldLen]._type := _type;
    Attributes[OldLen].pValue := pValue;
    Attributes[OldLen].ulValueLen := ulValueLen;
  end;

begin
  CheckLicenseKey();
  if (not FOpened) then
    raise ECertStorageError.Create(SPKCS11ProviderNotOpened, -1{$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});
  if (SessionIndex < 0) or (SessionIndex > FSessionList.Count - 1) then
    raise ECertStorageError.Create(SIndexOutOfBounds, GetLastError() {$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});

  FSharedResource.WaitToWrite;
  try
    SessionInfo := TElPKCS11SessionInfo(FSessionList[SessionIndex]);

    bTrue  := CK_BBOOL(true);
    bFalse := CK_BBOOL(false);
    bVal := bFalse;

    Cert := TElX509Certificate.Create(nil);
    try
      X509Certificate.Clone(Cert, false);
      SetLength(Attribs, 0);

      // Setup class
      certClass := CKO_CERTIFICATE;
      AddAttribute(Attribs, CKA_CLASS, @certClass, sizeof(certClass));

      // Setup certificate
      certType := CKC_X_509;
      AddAttribute(Attribs, CKA_CERTIFICATE_TYPE, @certType, sizeof(certType));


      // Setup token
      AddAttribute(Attribs, CKA_TOKEN, @bTrue, sizeof(bTrue));

      // Setup label
      sLabel := Cert.IssuerName.CommonName + ' | ' +
                Cert.SubjectName.CommonName;
      AddAttribute(Attribs, CKA_LABEL, @sLabel[BufferTypeStartOffset], Length(sLabel));

      // Setup subject
      sSubject := Cert.WriteSubject;
      AddAttribute(Attribs, CKA_SUBJECT, @sSubject[BufferTypeStartOffset],
        Length(sSubject));

      // Setup issuer
      sIssuer := Cert.WriteIssuer;
      AddAttribute(Attribs, CKA_ISSUER, @sIssuer[BufferTypeStartOffset],
        Length(sIssuer));

      // Setup serial
      sSerial := Cert.WriteSerialNumber;
      AddAttribute(Attribs, CKA_SERIAL_NUMBER, @sSerial[BufferTypeStartOffset], Length(sSerial));

      // Setup id
      sID := Cert.WriteExtensionSubjectKeyIdentifier;
      if Length(sID) = 0 then
      begin
        Hash := Cert.GetHashSHA1;
        SetLength(HashArr, 20);
        Move(Hash, HashArr, 20);
        sID := SBUtils.BinaryToString(@HashArr, 20);
      end
      else
        sID := SBUtils.BinaryToString(@sID[BufferTypeStartOffset], Length(sID));

      AddAttribute(Attribs, CKA_ID, @sID[BufferTypeStartOffset], Length(sID));

      // Setup value
      SetLength(sValue, 0);
      lVL := 0;

      Cert.SaveToBuffer(nil, lVL);
      SetLength(sValue, lVL);

      Cert.SaveToBuffer(@sValue[BufferTypeStartOffset], lVL);
      AddAttribute(Attribs, CKA_VALUE, @sValue[BufferTypeStartOffset], lVL);

      if CopyPrivateKey and X509Certificate.PrivateKeyExists then
      begin
        SetLength(PKAttribs, 0);

        // Setup class
        keyClass := CKO_PRIVATE_KEY;
        AddAttribute(PKAttribs, CKA_CLASS, @keyClass, sizeof(keyClass));


        // Setup certificate
        AddAttribute(PKAttribs, CKA_KEY_TYPE, @keyType, sizeof(keyType));
        
        // Setup token
        AddAttribute(PKAttribs, CKA_TOKEN, @bTrue, sizeof(bTrue));

        // Setup label
        AddAttribute(PKAttribs, CKA_LABEL, @sLabel[1], Length(sLabel));

        // Setup subject
        AddAttribute(PKAttribs, CKA_SUBJECT, @sSubject[1], Length(sSubject));

        // Setup id
        AddAttribute(PKAttribs, CKA_ID, @sID[1], Length(sID));

        // Setup sensitive
        AddAttribute(PKAttribs, CKA_SENSITIVE, @bVal, sizeof(CK_BBOOL));

        // setup decrypt
        if X509Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
          AddAttribute(PKAttribs, CKA_DECRYPT, @bTrue, sizeof(CK_BBOOL));

        // setup sign
        if X509Certificate.PublicKeyAlgorithm in [SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION,
          SB_CERT_ALGORITHM_ID_DSA] then
          AddAttribute(PKAttribs, CKA_SIGN, @bTrue, sizeof(CK_BBOOL));

        pkSize := 0;
        X509Certificate.SaveKeyToBuffer(nil, pkSize);
        GetMem(pkBuf, pkSize);
        X509Certificate.SaveKeyToBuffer(pkBuf, pkSize);

        try
          case X509Certificate.PublicKeyAlgorithm of
            SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION:
              begin
                keyType := CKK_RSA;

                lVL1 := 0;
                lVL2 := 0;
                lVL3 := 0;
                lVL4 := 0;
                lVL5 := 0;
                lVL6 := 0;
                lVL7 := 0;
                lVL8 := 0;
                SBRSA.DecodePrivateKey(pkBuf, pkSize,
                  nil, lVL1,
                  nil, lVL2,
                  nil, lVL3,
                  nil, lVL4,
                  nil, lVL5,
                  nil, lVL6,
                  nil, lVL7,
                  nil, lVL8
                );

                SetLength(buf1, lVL1);
                SetLength(buf2, lVL2);
                SetLength(buf3, lVL3);
                SetLength(buf4, lVL4);
                SetLength(buf5, lVL5);
                SetLength(buf6, lVL6);
                SetLength(buf7, lVL7);
                SetLength(buf8, lVL8);
                SBRSA.DecodePrivateKey(pkBuf, pkSize,
                  @buf1[BufferTypeStartOffset], lVL1,
                  @buf2[BufferTypeStartOffset], lVL2,
                  @buf3[BufferTypeStartOffset], lVL3,
                  @buf4[BufferTypeStartOffset], lVL4,
                  @buf5[BufferTypeStartOffset], lVL5,
                  @buf6[BufferTypeStartOffset], lVL6,
                  @buf7[BufferTypeStartOffset], lVL7,
                  @buf8[BufferTypeStartOffset], lVL8
                );

                AddAttribute(PKAttribs, CKA_MODULUS, @buf1[BufferTypeStartOffset],
                  lVL1);
                AddAttribute(PKAttribs, CKA_PUBLIC_EXPONENT,
                  @buf2[BufferTypeStartOffset], lVL2);
                AddAttribute(PKAttribs, CKA_PRIVATE_EXPONENT,
                  @buf3[BufferTypeStartOffset], lVL3);
                AddAttribute(PKAttribs, CKA_PRIME_1, @buf4[BufferTypeStartOffset],
                  lVL4);
                AddAttribute(PKAttribs, CKA_PRIME_2, @buf5[BufferTypeStartOffset],
                  lVL5);
                AddAttribute(PKAttribs, CKA_EXPONENT_1, @buf6[BufferTypeStartOffset],
                  lVL6);
                AddAttribute(PKAttribs, CKA_EXPONENT_2, @buf7[BufferTypeStartOffset],
                  lVL7);
                AddAttribute(PKAttribs, CKA_COEFFICIENT, @buf8[BufferTypeStartOffset],
                  lVL8);

                PKCS11CheckError(TPKCS11CreateObjectFunc(FModule.FuncArray[PKCS11_CreateObject])(SessionInfo.FHandle, @PKAttribs[0], Length(PKAttribs), hKey));
              end;
            SB_CERT_ALGORITHM_ID_DSA:
              begin
                keyType := CKK_DSA;

                lVL1 := 0;
                lVL2 := 0;
                lVL3 := 0;
                lVL4 := 0;
                lVL5 := 0;
                SBDSA.DecodePrivateKey(pkBuf, pkSize, nil, lVL1, nil, lVL2, nil,
                  lVL3, nil, lVL4, nil, lVL5);
                SetLength(buf1, lVL1);
                SetLength(buf2, lVL2);
                SetLength(buf3, lVL3);
                SetLength(buf4, lVL4);
                if lVL5 < 20 then
                  lVL6 := 20 - lVL5
                else
                  lVL6 := 0;
                SetLength(buf5, lVL5 + lVL6);
                FillChar(buf5[1], lVL6, 0);
                SBDSA.DecodePrivateKey(pkBuf, pkSize,
                  @buf1[BufferTypeStartOffset], lVL1,
                  @buf2[BufferTypeStartOffset], lVL2,
                  @buf3[BufferTypeStartOffset], lVL3,
                  @buf4[BufferTypeStartOffset], lVL4,
                  @buf5[BufferTypeStartOffset + lVL6], lVL5);

                AddAttribute(PKAttribs, CKA_PRIME, @buf1[BufferTypeStartOffset], lVL1);
                AddAttribute(PKAttribs, CKA_SUBPRIME, @buf2[BufferTypeStartOffset], lVL2);
                AddAttribute(PKAttribs, CKA_BASE, @buf3[BufferTypeStartOffset], lVL3);
                AddAttribute(PKAttribs, CKA_VALUE, @buf5[BufferTypeStartOffset], lVL5 + lVL6);

                PKCS11CheckError(TPKCS11CreateObjectFunc(FModule.FuncArray[PKCS11_CreateObject])(SessionInfo.FHandle, @PKAttribs[0], Length(PKAttribs), hKey));
              end;
            SB_CERT_ALGORITHM_DH_PUBLIC:
              begin
                keyType := CKK_X9_42_DH;

                lVL1 := 0;
                lVL2 := 0;
                lVL3 := 0;
                X509Certificate.GetDHParams(nil, lVL1, nil, lVL2, nil, lVL3);
                SetLength(buf1, lVL1);
                SetLength(buf2, lVL2);
                SetLength(buf3, lVL3);
                X509Certificate.GetDHParams(@buf1[BufferTypeStartOffset], lVL1,
                  @buf2[BufferTypeStartOffset], lVL2,
                  @buf3[BufferTypeStartOffset], lVL3);

                AddAttribute(PKAttribs, CKA_PRIME, @buf1[BufferTypeStartOffset], lVL1);
                AddAttribute(PKAttribs, CKA_BASE, @buf2[BufferTypeStartOffset], lVL2);
                AddAttribute(PKAttribs, CKA_VALUE, pkBuf, pkSize);

                PKCS11CheckError(TPKCS11CreateObjectFunc(FModule.FuncArray[PKCS11_CreateObject])(SessionInfo.FHandle, @PKAttribs[0], Length(PKAttribs), hKey));
              end;
          end;
        finally
          FreeMem(pkBuf);
        end;
      end;

      // create an object
      PKCS11CheckError(TPKCS11CreateObjectFunc(FModule.FuncArray[PKCS11_CreateObject])( SessionInfo.FHandle, @Attribs[0], Length(Attribs), hCert));

      // create context
      Ctx := TElPKCS11StgCtx.Create;
      try
        Ctx.SlotInfo := SessionInfo.Slot;
        Ctx.Session := SessionInfo;
        Ctx.Handle := hCert;

        Ctx.Cert := TElX509Certificate.Create(nil);
        X509Certificate.Clone(Ctx.Cert, true);
        Ctx.Cert.BelongsTo := BT_PKCS11;
        Ctx.SetCertStorageName;
        FCtxList.Add(Ctx);
      except
        FreeAndNil(Ctx);
        raise;
      end;

      UpdateCertificate(Ctx);

    finally

      FreeAndNil(Cert);
    end;
  finally
    FSharedResource.Done;
  end;
end;

procedure TElPKCS11CertStorage.Add(X509Certificate: TElX509Certificate;
  CopyPrivateKey: boolean = true);
begin
  if (not FOpened) or (SessionCount = 0) then
    raise ECertStorageError.Create(SPKCS11ProviderNotOpened, -1 {$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});

  Add(0, X509Certificate, CopyPrivateKey, false);
end;

function TElPKCS11CertStorage.UpdateCertificate(Ctx: TElPKCS11StgCtx): TElX509Certificate;
var
  pBuf : BufferType;
  pSubject : BufferType;
  PKHandle : CK_OBJECT_HANDLE;
begin
  result := nil;

  FSharedResource.WaitToWrite;
  try
    if Ctx.Cert = nil then
      Ctx.Cert := TElX509Certificate.Create(nil);

    // read certificate
    pBuf := GetObjectAttribute(Ctx.Session.FHandle, Ctx.Handle, CKA_VALUE);
    if Length(pBuf) > 0 then
      Ctx.Cert.LoadFromBuffer(@pBuf[BufferTypeStartOffset], Length(pBuf));

    Ctx.Cert.BelongsTo := BT_PKCS11;
    Ctx.SetCertStorageName;

    Ctx.Cert.LoadKeyFromBuffer(nil, 0);

    // try to load private key
    pBuf := GetObjectID(Ctx.Session.FHandle, Ctx.Handle);
    pSubject := GetObjectAttribute(Ctx.Session.FHandle, Ctx.Handle, CKA_SUBJECT);
    if Length(pBuf) > 0 then
    begin
      PKHandle := GetObjectBySubjectID(Ctx.Session.FHandle, CKO_PRIVATE_KEY, pSubject, pBuf);
      if PKHandle = CK_INVALID_HANDLE then
      begin
        PKHandle := GetObjectByID(Ctx.Session.FHandle, CKO_PRIVATE_KEY, pBuf);
        //pBuf := GetObjectAttribute(Ctx.Session.FHandle, PKHandle, CKA_SUBJECT);
      end;
      if PKHandle <> CK_INVALID_HANDLE then
      begin
        Ctx.Cert.PrivateKeyExists := true;
        Ctx.PKHandle := PKHandle;
        try
          pBuf := GetObjectAttribute(Ctx.Session.FHandle, PKHandle, CKA_KEY_TYPE);
          if (Length(pBuf) = SizeOf(CK_KEY_TYPE)) then
            Ctx.KeyType := TElPKCS11KeyType(PInteger(@pBuf[BufferTypeStartOffset])^);

          pBuf := GetObjectAttribute(Ctx.Session.FHandle, PKHandle, CKA_EXTRACTABLE);
          if (Length(pBuf) = sizeof(CK_BBOOL)) and (pBuf[BufferTypeStartOffset] <> #0) then
          begin
            // We don't implement extraction of private key since no hardware tokens support it

          end;
        except
          on E : ECertStorageError do
          begin
            if (E.ErrorCode <> CKR_KEY_UNEXTRACTABLE) and (E.ErrorCode <> CKR_ATTRIBUTE_SENSITIVE) then
              raise;
          end;
        end;
      end;
    end;
    result := Ctx.Cert;
  finally
    FSharedResource.Done;
  end;
end;

procedure TElPKCS11CertStorage.ClearCerts(Session : TElPKCS11SessionInfo);
var
  i: integer;
begin
  FSharedResource.WaitToWrite;
  try
    i := 0;
    while i < FCtxList.Count do
    begin
      if TElPKCS11StgCtx(FCtxList[i]).Session = Session then
      begin
        TElPKCS11StgCtx(FCtxList[i]).Free;
        FCtxList.Delete(i);
      end
      else
        inc(i);
    end;
  finally
    FSharedResource.Done;
  end;
end;

procedure TElPKCS11CertStorage.CloseAllSessions(SlotInfo : TElPKCS11SlotInfo);
var
  i: integer;
begin
  i := 0;
  FSharedResource.WaitToWrite;
  try
    while i < FSessionList.Count do
    begin
      if (SlotInfo = nil) or (TElPKCS11SessionInfo(FSessionList[i]).Slot = SlotInfo) then
        CloseSession(i)
      else
        inc(i);
    end;
  finally
    FSharedResource.Done;
  end;
end;

procedure TElPKCS11CertStorage.CloseSession(SessionIndex: integer);
var
  SessionInfo: TElPKCS11SessionInfo;
begin
  // check state and parameters
  if (not FOpened) then
    raise ECertStorageError.Create(SPKCS11ProviderNotOpened, -1{$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});
  if (SessionIndex < 0) or (SessionIndex > FSessionList.Count - 1) then
    raise ECertStorageError.Create(SIndexOutOfBounds, GetLastError() {$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});

  FSharedResource.WaitToWrite;
  try
    SessionInfo := TElPKCS11SessionInfo(FSessionList[SessionIndex]);

    // close the session
    if (SessionInfo.FHandle <> CK_INVALID_HANDLE) then
    begin
      try
        PKCS11CheckError(TPKCS11CloseSessionFunc(FModule.FuncArray[PKCS11_CloseSession])(SessionInfo.FHandle));
      except
        ;
      end;
      dec(FSessionCount);
    end;
    SessionInfo.FHandle := CK_INVALID_HANDLE;
    // remove ourselves from slot consumers list
    SessionInfo.FSlot.FConsumers.Remove(Self);

    // cleanup the certificates and context lists
    ClearCerts(SessionInfo);
    FSessionList.Remove(SessionInfo);
  finally
    FSharedResource.Done;
  end;
  FreeAndNil(SessionInfo);
end;

procedure TElPKCS11CertStorage.DoNotification(SessionInfo: TElPKCS11SessionInfo;
  NotificationFlag: TElPKCS11NotificationFlag; var AbortOperation: boolean);
begin
  if assigned(FOnNotification) then
    FOnNotification(Self, SessionInfo, NotificationFlag, AbortOperation);
end;

function TElPKCS11CertStorage.GetCertificates(Index: integer) : TElX509Certificate;
begin
  // check state and parameters
  if (not FOpened) then
    raise ECertStorageError.Create(SPKCS11ProviderNotOpened, -1{$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});
  if (Index < 0) or (Index > FCtxList.Count - 1) then
    raise ECertStorageError.Create(SIndexOutOfBounds, GetLastError() {$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});

  result := TElPKCS11StgCtx(FCtxList[index]).Cert;
  if Result = nil then
    result := UpdateCertificate(TElPKCS11StgCtx(FCtxList[index]));
end;

function TElPKCS11CertStorage.GetCount: integer;
begin
  result := FCtxList.Count;
end;

function TElPKCS11CertStorage.GetObjectAttribute(hSession : CK_SESSION_HANDLE;
    hObject : CK_OBJECT_HANDLE; Attribute : CK_ATTRIBUTE_TYPE): BufferType;
var
  CKRes: CK_RV;
  Attribs: CK_ATTRIBUTE;
begin
  Attribs._type := Attribute;
  Attribs.ulValueLen := 0;
  Attribs.pValue := nil;
  CKRes := TPKCS11GetAttributeValueFunc(FModule.FuncArray[PKCS11_GetAttributeValue])(hSession, hObject, @Attribs, 1);
  if CKRes = CKR_OK then
  begin
    SetLength(Result, Attribs.ulValueLen);
    if Length(Result) > 0 then
    begin
      Attribs.pValue := @Result[BufferTypeStartOffset];
      PKCS11CheckError(TPKCS11GetAttributeValueFunc(FModule.FuncArray[PKCS11_GetAttributeValue])(hSession, hObject, @Attribs, 1));
    end;
  end
  else
    PKCS11CheckError(CKRes);
end;

function TElPKCS11CertStorage.GetObjectByID(hSession : CK_SESSION_HANDLE;
    ObjectClass : CK_OBJECT_CLASS; ID : BufferType): CK_OBJECT_HANDLE;
var
  CKRes: CK_RV;
  Attribs: array[0..1] of CK_ATTRIBUTE;
  ReadBuf: CK_OBJECT_HANDLE;
  Read   : CK_ULONG;
begin
  Attribs[0]._type := CKA_CLASS;
  Attribs[0].ulValueLen := sizeof(ObjectClass);
  Attribs[0].pValue := @ObjectClass;

  Attribs[1]._type := CKA_ID;
  Attribs[1].ulValueLen := Length(ID);
  Attribs[1].pValue := @ID[BufferTypeStartOffset];

  CKRes := TPKCS11FindObjectsInitFunc(FModule.FuncArray[PKCS11_FindObjectsInit])(hSession, @Attribs, 2);
  if CKRes = CKR_OK then
  begin
    CKRes := TPKCS11FindObjectsFunc(FModule.FuncArray[PKCS11_FindObjects])(hSession, ReadBuf, 1, Read);
    if (CKRes = CKR_OK) and (Read > 0) then
      result := ReadBuf
    else
      result := CK_INVALID_HANDLE;
    TPKCS11FindObjectsFinalFunc(FModule.FuncArray[PKCS11_FindObjectsFinal])(hSession);
    exit;
  end
  else
    if (CKRes = CKR_ATTRIBUTE_TYPE_INVALID) or
    (CKRes = CKR_ATTRIBUTE_VALUE_INVALID) then
  begin
  end
  else
    PKCS11CheckError(CKRes);

  Result := CK_INVALID_HANDLE;
end;

function TElPKCS11CertStorage.GetObjectBySubjectID(hSession : CK_SESSION_HANDLE;
    ObjectClass : CK_OBJECT_CLASS; Subject, ID : BufferType): CK_OBJECT_HANDLE;
var
  CKRes: CK_RV;
  Attribs: array[0..2] of CK_ATTRIBUTE;
  ReadBuf: CK_OBJECT_HANDLE;
  Read   : CK_ULONG;
begin
  Attribs[0]._type := CKA_CLASS;
  Attribs[0].ulValueLen := sizeof(ObjectClass);
  Attribs[0].pValue := @ObjectClass;

  Attribs[1]._type := CKA_ID;
  Attribs[1].ulValueLen := Length(ID);
  Attribs[1].pValue := @ID[BufferTypeStartOffset];

  Attribs[2]._type := CKA_SUBJECT;
  Attribs[2].ulValueLen := Length(Subject);
  Attribs[2].pValue := @Subject[BufferTypeStartOffset];

  CKRes := TPKCS11FindObjectsInitFunc(FModule.FuncArray[PKCS11_FindObjectsInit])(hSession, @Attribs, 3);
  if CKRes = CKR_OK then
  begin
    CKRes := TPKCS11FindObjectsFunc(FModule.FuncArray[PKCS11_FindObjects])(hSession, ReadBuf, 1, Read);
    if (CKRes = CKR_OK) and (Read > 0) then
      result := ReadBuf
    else
      result := CK_INVALID_HANDLE;
    TPKCS11FindObjectsFinalFunc(FModule.FuncArray[PKCS11_FindObjectsFinal])(hSession);
    exit;
  end
  else
    if (CKRes = CKR_ATTRIBUTE_TYPE_INVALID) or
    (CKRes = CKR_ATTRIBUTE_VALUE_INVALID) then
  begin
  end
  else
    PKCS11CheckError(CKRes);

  Result := CK_INVALID_HANDLE;
end;

function TElPKCS11CertStorage.GetObjectID(hSession : CK_SESSION_HANDLE; hObject
    : CK_OBJECT_HANDLE): BufferType;
begin
  result := GetObjectAttribute(hSession, hObject, CKA_ID);
end;

function TElPKCS11CertStorage.GetSlot(Index: Integer): TElPKCS11SlotInfo;
begin
  Result := TElPKCS11SlotInfo(FModule.FSlotList[Index]);
end;

function TElPKCS11CertStorage.GetSession(Index: Integer): TElPKCS11SessionInfo;
begin
  Result := TElPKCS11SessionInfo(FSessionList[Index]);
end;

function TElPKCS11CertStorage.GetSlotCount: Integer;
begin
  Result := FModule.FSlotList.Count;
end;

function TElPKCS11CertStorage.GetSessionCount: Integer;
begin
  Result := FSessionList.Count;
end;

function TElPKCS11CertStorage.HandleNotification(hSession: CK_SESSION_HANDLE;
  aEvent: CK_NOTIFICATION): CK_RV;
var
  AbortOperation: boolean;
  elevent: TElPKCS11NotificationFlag;
  i: integer;
  SessionInfo: TElPKCS11SessionInfo;
begin
  Result := CKR_OK;
  SessionInfo := nil;
  for i := 0 to FSessionList.Count - 1 do
  begin
    if hSession = TElPKCS11SessionInfo(FSessionList[i]).FHandle then
    begin
      SessionInfo := TElPKCS11SessionInfo(FSessionList[i]);
      break;
    end;
  end;

  if SessionInfo <> nil then
  begin
    case aEvent of
      CKN_SURRENDER: elevent := nfSurrender;
    else
      elevent := nfNone;
    end;
    AbortOperation := false;
    DoNotification(SessionInfo, elevent, AbortOperation);
    if AbortOperation then
      result := CKR_CANCEL;
  end;
end;

procedure TElPKCS11CertStorage.Open;
begin
  CheckLicenseKey();
  if FOpened then exit;
  if FDLLName = '' then
    raise ECertStorageError.Create(SNoPKCS11ProviderSpecified);

  FModule := ModuleList.LoadModule(FDLLName);
  FOpened := true;
end;

function TElPKCS11CertStorage.OpenSession(SlotIndex: integer; ReadOnly:
    boolean): TElPKCS11SessionInfo;
var
  SlotInfo: TElPKCS11SlotInfo;
  RSessionInfo: TPKCS11SessionInfo;
  FSessionInfo: TElPKCS11SessionInfo;
  Flags: CK_FLAGS;
  SessionIndex: integer;
begin
  result := nil;
  
  // check state and parameters
  if (not FOpened) then
    raise ECertStorageError.Create(SPKCS11ProviderNotOpened, -1{$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});
  if (SlotIndex < 0) or (SlotIndex > FModule.FSlotList.Count - 1) then
    raise ECertStorageError.Create(SIndexOutOfBounds, GetLastError() {$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});

  FSharedResource.WaitToWrite;

  try
    SlotInfo := TElPKCS11SlotInfo(FModule.FSlotList[SlotIndex]);

    Flags := CKF_SERIAL_SESSION;
    if not ReadOnly then
      Flags := Flags or CKF_RW_SESSION;

    FSessionInfo := TElPKCS11SessionInfo.Create;
    try
      SessionIndex := FSessionList.Add(FSessionInfo);
      FSessionInfo.FSlot := SlotInfo;
      FSessionInfo.FStorage := Self;
      SlotInfo.FReadOnly := ReadOnly;

      // open the session
      try
        PKCS11CheckError(TPKCS11OpenSessionFunc(FModule.FuncArray[PKCS11_OpenSession])(SlotInfo.FSlotID, Flags, Self, PKCS11NotifyCallback, FSessionInfo.FHandle));
      except
        on E : ECertStorageError do
        begin
          if E.ErrorCode = CKR_TOKEN_WRITE_PROTECTED then
          begin
            SlotInfo.FReadOnly := true;
          end;
          raise;
        end;
      end;

      // get session info
      PKCS11CheckError(TPKCS11GetSessionInfoFunc(FModule.FuncArray[PKCS11_GetSessionInfo])(FSessionInfo.FHandle, RSessionInfo));
      SlotInfo.FSessionState := TElPKCS11SessionState(RSessionInfo.state);
      inc(FSessionCount);
    except
      FSessionList.Remove(FSessionInfo);
      FSessionInfo.Free;
      raise;
    end;

    // add ourselves to slot consumers list
    SlotInfo.FConsumers.Add(Self);

    RefreshSlotCertHandles(SessionIndex);
    result := FSessionInfo;
  finally
    FSharedResource.Done;
  end;
end;

procedure TElPKCS11CertStorage.RefreshSlotCertHandles(SessionIndex : integer);
var
  SessionInfo: TElPKCS11SessionInfo;
  CKRes: CK_RV;
  Attribs: ^CK_ATTRIBUTE_ARRAY;
  ReadBuf: array[0..15] of CK_OBJECT_HANDLE;
  Read: CK_ULONG;
  i: integer;
  certClass: CK_OBJECT_CLASS;
  certType: CK_CERTIFICATE_TYPE;
  CTX: TElPKCS11StgCtx;
begin
  // check state and parameters
  if not FOpened then
    raise ECertStorageError.Create(SPKCS11ProviderNotOpened, -1{$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});
  if (SessionIndex < 0) or (SessionIndex > FSessionList.Count - 1) then
    raise ECertStorageError.Create(SIndexOutOfBounds, GetLastError() {$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});
  SessionInfo := TElPKCS11SessionInfo(FSessionList[SessionIndex]);

  // initiate enumeration
  certClass := CKO_CERTIFICATE;
  certType := CKC_X_509;

  GetMem(Attribs, sizeof(CK_ATTRIBUTE) * 2);
  try
    Attribs[0]._type := CKA_CLASS;
    Attribs[0].ulValueLen := sizeof(certClass);
    Attribs[0].pValue := @certClass;

    Attribs[1]._type := CKA_CERTIFICATE_TYPE;
    Attribs[1].ulValueLen := sizeof(certType);
    Attribs[1].pValue := @certType;
    CKRes := TPKCS11FindObjectsInitFunc(FModule.FuncArray[PKCS11_FindObjectsInit])(SessionInfo.FHandle, CK_ATTRIBUTE_PTR(Attribs), 2);
    if CKRes = CKR_OK then
    begin
      while true do
      begin
        CKRes :=
          TPKCS11FindObjectsFunc(FModule.FuncArray[PKCS11_FindObjects])(SessionInfo.FHandle, ReadBuf[0], 16, Read);

        if (CKRes <> CKR_OK) or (Read = 0) then
          break;
        for i := 0 to Read - 1 do
        begin
          Ctx := TElPKCS11StgCtx.Create;
          Ctx.SlotInfo := SessionInfo.Slot;
          Ctx.Session := SessionInfo;
          Ctx.Handle := ReadBuf[i];
          Ctx.Cert := nil;
          FCtxList.Add(Ctx);
        end;
        if Read < 16 then break;
      end;
      TPKCS11FindObjectsFinalFunc(FModule.FuncArray[PKCS11_FindObjectsFinal])(SessionInfo.FHandle);
    end
    else
      if (CKRes = CKR_ATTRIBUTE_TYPE_INVALID) or
      (CKRes = CKR_ATTRIBUTE_VALUE_INVALID) then
    begin
    end
    else
      PKCS11CheckError(CKRes);
  finally
    FreeMem(Attribs);
  end;
end;

procedure TElPKCS11CertStorage.Remove(Index: integer);
var Ctx : TElPKCS11StgCtx;
    KHandle : CK_OBJECT_HANDLE;
begin
  // check state and parameters
  if (not FOpened) then
    raise ECertStorageError.Create(SPKCS11ProviderNotOpened, -1{$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});
  if (Index < 0) or (Index > FCtxList.Count - 1) then
    raise ECertStorageError.Create(SIndexOutOfBounds, GetLastError() {$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});

  FSharedResource.WaitToWrite;
  try
    Ctx := TElPKCS11StgCtx(FCtxList[index]);

    // if there's a private key, delete it
    if Ctx.PKHandle <> CK_INVALID_HANDLE then
      PKCS11CheckError(TPKCS11DestroyObjectFunc(FModule.FuncArray[PKCS11_DestroyObject])(Ctx.Session.FHandle, Ctx.PKHandle));

    // if there's a separate public key, delete it
    KHandle := GetObjectByID(Ctx.Session.FHandle, CKO_PUBLIC_KEY, GetObjectID(Ctx.Session.FHandle, Ctx.Handle));
    if KHandle <> CK_INVALID_HANDLE then
      PKCS11CheckError(TPKCS11DestroyObjectFunc(FModule.FuncArray[PKCS11_DestroyObject])(Ctx.Session.FHandle, KHandle{Ctx.PKHandle}));

    // delete the certificate itself
    PKCS11CheckError(TPKCS11DestroyObjectFunc(FModule.FuncArray[PKCS11_DestroyObject])(Ctx.Session.FHandle, Ctx.Handle));
    Self.FCtxList.Remove(Ctx);
    Ctx.Free;
  finally
    FSharedResource.Done;
  end;
end;

procedure TElPKCS11CertStorage.SetDLLName(const Value: string);
begin
  if FDLLName <> Value then
  begin
    if FOpened then Close;
    FDLLName := Value;
  end;
end;

constructor TElPKCS11SlotInfo.Create;
begin
  inherited;
  FConsumers := TList.Create;
end;

destructor TElPKCS11SlotInfo.Destroy;
begin
  while FConsumers.Count > 0 do
    TElPKCS11CertStorage(FConsumers[0]).CloseAllSessions(Self);

  FreeAndNil(FConsumers);

  inherited;
end;



function TElPKCS11SlotInfo.GetLoggedIn: Boolean;
begin
  Result := FLoggedIn > 0;
end;

function TElPKCS11SlotInfo.PinNeeded: Boolean;
begin
  Result := (CKF_PROTECTED_AUTHENTICATION_PATH and FTokenFlags) = 0;
end;

procedure TElPKCS11SlotInfo.Refresh;
var
  RSlotInfo: TPKCS11SlotInfo;
  RTokenInfo: TPKCS11TokenInfo;
  CKRes: CK_RV;
begin
  // check state and parameters

  // read slot info
  PKCS11CheckError(TPKCS11GetSlotInfoFunc(FModule.FuncArray[PKCS11_GetSlotInfo])(FSlotID, RSlotInfo));
  SetSlotData(FSlotID, RSlotInfo);

  // read token info, if token is present
  CKRes :=
    TPKCS11GetTokenInfoFunc(FModule.FuncArray[PKCS11_GetTokenInfo])(FSlotID, RTokenInfo);
  if CKRes = CKR_OK then
  begin
    FTokenPresent := true;
    SetTokenData(RTokenInfo);
  end
  else
  if (CKRes = CKR_SLOT_ID_INVALID) or (CKRes = CKR_DEVICE_ERROR) or (CKRes = CKR_TOKEN_NOT_RECOGNIZED) then // alien token/reader
  begin
    FTokenPresent := false;
  end
  else
  if (CKRes = CKR_TOKEN_NOT_PRESENT) or (CKRes = CKR_DEVICE_REMOVED) then
  begin
    FTokenPresent := false;
  end
  else
    PKCS11CheckError(CKRes);
end;

procedure TElPKCS11SlotInfo.SetSlotData(SlotID: CK_SLOT_ID; SlotInfo:
  TPKCS11SlotInfo);
begin
  Self.FSlotID := SlotID;

  FManufacturerID := GetPKCS11String(SlotInfo.manufacturerID, 32);
  Self.FSlotDescription := GetPKCS11String(SlotInfo.slotDescription, 64);
  FHWVersion := SlotInfo.hardwareVersion;
  FFWVersion := SlotInfo.firmwareVersion;
end;

procedure TElPKCS11SlotInfo.SetTokenData(TokenInfo: TPKCS11TokenInfo);
begin
  FTokenManID := GetPKCS11String(TokenInfo.manufacturerID, 32);
  FTokenLabel := GetPKCS11String(TokenInfo._label, 32);
  FTokenModel := GetPKCS11String(TokenInfo.model, 16);
  FTokenSerial := GetPKCS11String(TokenInfo.serialNumber, 16);

  FTokenHWVersion := TokenInfo.hardwareVersion;
  FTokenFWVersion := TokenInfo.firmwareVersion;
  FTokenFlags := TokenInfo.flags;
end;

destructor TElPKCS11StgCtx.Destroy;
begin
  FreeAndNil(Cert);
  inherited;
end;



procedure TElPKCS11StgCtx.SetCertStorageName;
var S : string;
    sID: BufferType;
    Hash: TMessageDigest160;
    HashArr : BufferType;
begin
  if Cert <> nil then
  begin
    S := 'PKCS#11'#13#10 + SlotInfo.FTokenSerial + #13#10;
    Hash := Cert.GetHashSHA1;
    SetLength(HashArr, 20);
    Move(Hash, HashArr[1], 20);
    sID := SBUtils.BinaryToString(@HashArr[1], 20);
    S := S + sID;
    Self.Hash := sID;
    S := S + #13#10 + IntToStr(SlotInfo.FSlotID);
    S := S + #13#10 +
      BinaryToString(@SlotInfo.FModule.FUniqueID[0], Length(SlotInfo.FModule.FUniqueID));
    S := S + #13#10 +
      BinaryToString(@Session.FStorage.FUniqueID[0], Length(Session.FStorage.FUniqueID));
    S := S + #13#10;
    Cert.StorageName := S;
  end;
end;

function TElPKCS11StgCtx.Sign(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
  var OutSize: integer): boolean;
var
  Mech : CK_MECHANISM;
  RequiredSize : integer;
  Buf : array of byte;
  Len : CK_ULONG;
begin
  if Cert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
  begin
    Mech.mechanism := CKM_RSA_PKCS;
    RequiredSize := Cert.GetPublicKeySize shr 3 + 1;
  end
  else if Cert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    Mech.mechanism := CKM_DSA;
    RequiredSize := 0;
    SBDSA.EncodeSignature(nil, 20, nil, 20, nil, RequiredSize);
    // PKCS#11 DSA signature is a simple concatenation of R and S.
    // These values are transformed to represent ASN.1-formatted signature.
  end
  else
    raise ECertStorageError.CreateFmt(SAlgorithmDoesNotSupportSigning,
      [Cert.PublicKeyAlgorithm]);

  if RequiredSize > OutSize then
  begin
    OutSize := RequiredSize;
    Result := false;
    Exit;
  end;
  Mech.pParameter := NULL_PTR;
  Mech.ulParameterLen := 0;
  PKCS11CheckError(TPKCS11SignInitFunc(SlotInfo.FModule.FuncArray[PKCS11_SignInit])(Session.FHandle,
    @Mech, PKHandle));

  Len := RequiredSize;
  SetLength(Buf, Len);

  PKCS11CheckError(TPKCS11SignFunc(SlotInfo.FModule.FuncArray[PKCS11_Sign])(Session.FHandle,
    InBuffer, InSize, @Buf[0], @Len));

  if Cert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
  begin
    Move(Buf[0], OutBuffer^, Len);
    OutSize := Len;
  end
  else if Cert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    if not SBDSA.EncodeSignature(@Buf[0], 20, @Buf[20], 20, OutBuffer, OutSize) then
      raise ECertStorageError.Create(SInternalError);
  end
  else
    raise ECertStorageError.Create(SInternalError);

  Result := true;
end;

function TElPKCS11StgCtx.Decrypt(InBuffer: pointer; InSize: integer; OutBuffer: pointer;
  var OutSize: integer): boolean;
var
  Mech : CK_MECHANISM;
  Len : CK_ULONG;
  R : CK_RV;
  RequiredSize : integer;
begin
  if Cert.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
  begin
    Mech.mechanism := CKM_RSA_PKCS;
    Mech.pParameter := nil;
    Mech.ulParameterLen := 0;
    RequiredSize := InSize;
  end
  else
    raise ECertStorageError.CreateFmt(SAlgorithmDoesNotSupportEncryption,
      [Cert.PublicKeyAlgorithm]);
  if (RequiredSize > OutSize) or (OutBuffer = nil) then
  begin
    OutSize := RequiredSize;
    Result := false;
  end
  else
  begin
    PKCS11CheckError(TPKCS11DecryptInitFunc(SlotInfo.FModule.FuncArray[PKCS11_DecryptInit])(Session.FHandle,
      @Mech, PKHandle));
    Len := OutSize;
    R := TPKCS11DecryptFunc(SlotInfo.FModule.FuncArray[PKCS11_Decrypt])(Session.FHandle,
      InBuffer, InSize, OutBuffer, @Len);
    PKCS11CheckError(R);
    OutSize := Len;
    Result := true;
  end;
end;

{ TElPKCS11ModuleList }

constructor TElPKCS11ModuleList.Create;
begin
  inherited;
  FList := TList.Create;
  FRes := TElSharedResource.Create;
end;


destructor TElPKCS11ModuleList.Destroy;
begin
  ClearList;
  FreeAndNil(FList);
  FreeAndNil(FRes);
  inherited;
end;



procedure TElPKCS11ModuleList.ClearList;
begin
  //FRes.WaitToWrite;
  while FList.Count > 0 do
    UnloadModule(TElPKCS11Module(FList[0]));
  //FRes.Done;
end;

function TElPKCS11ModuleList.LoadModule(ModuleName: string): TElPKCS11Module;
var
  HLib: HMODULE;
  aModule: TElPKCS11Module;
  i, j : integer;
  Info: TPKCS11Info;
  SlotInfo: TElPKCS11SlotInfo;
  pulCount: CK_ULONG;
  pSlotList: ^CK_SLOT_ID_ARRAY;
  InitArgs : TPKCS11InitializeArgs;
begin
  ModuleName := Uppercase(ModuleName);
  FRes.WaitToRead;
  for i := 0 to FList.Count - 1 do
  begin
    if TElPKCS11Module(FList[i]).Name = ModuleName then
    begin
      result := TElPKCS11Module(FList[i]);
      FRes.Done;
      FRes.WaitToWrite;
      inc(TElPKCS11Module(FList[i]).UseCount);
      FRes.Done;
      exit;
    end;
  end;
  FRes.Done;

  // Load module
  HLib := LoadLibrary(PChar(ModuleName));
  if HLib = 0 then
    raise ECertStorageError.Create(SPKCS11ProviderNotFound, GetLastError() {$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});

  aModule := TElPKCS11Module.Create;
  try
    aModule.FInit := false;
    aModule.HLib := HLib;

    aModule.GetFunctionList;

    InitArgs.CreateMutex := @CreateMutexCallback;
    InitArgs.DestroyMutex := @DestroyMutexCallback;
    InitArgs.LockMutex := @LockMutexCallback;
    InitArgs.UnlockMutex := @UnlockMutexCallback;
    InitArgs.pReserved := nil;
    InitArgs.flags := CKF_OS_LOCKING_OK;

    j := TPKCS11InitializeFunc(aModule.FuncArray[PKCS11_Initialize])(@InitArgs) and $FFFF;
    if j = CKR_CANT_LOCK then
    begin
      InitArgs.CreateMutex := nil;
      InitArgs.DestroyMutex := nil;
      InitArgs.LockMutex := nil;
      InitArgs.UnlockMutex := nil;

      InitArgs.flags := CKF_OS_LOCKING_OK;
      j := TPKCS11InitializeFunc(aModule.FuncArray[PKCS11_Initialize])(@InitArgs) and $FFFF;
    end;

    if j = CKR_OK then
    begin
      aModule.FInit := true;
      if aModule.FuncArray[PKCS11_GetInfo] <> nil then
        j := TPKCS11GetInfoFunc(aModule.FuncArray[PKCS11_GetInfo])(Info)
      else
        j := -1;
    end;
    if j = CKR_OK then
    begin
      if Info.cryptokiVersion.Major < 2 then
        j := CKR_FUNCTION_NOT_SUPPORTED;
    end;

    if j <> CKR_OK then
    begin
      if aModule.FInit then
        if aModule.FuncArray[PKCS11_Finalize] <> nil then
          TPKCS11FinalizeFunc(aModule.FuncArray[PKCS11_Finalize])(nil);

      FreeLibrary(aModule.HLib);
      aModule.HLib := 0;
      raise ECertStorageError.Create(SPKCS11ProviderError, j{$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});
    end;

    aModule.FProviderDescription := GetPKCS11String(PChar(@Info.LibraryDescription), 32);

    pulCount := 0;
    pSlotList := nil;
    PKCS11CheckError(TPKCS11GetSlotListFunc(aModule.FuncArray[PKCS11_GetSlotList])(CK_B_FALSE, pSlotList[0], pulCount));
    if pulCount > 0 then
    begin
      GetMem(pSlotList, pulCount * sizeof(CK_SLOT_ID));
      try
        PKCS11CheckError(TPKCS11GetSlotListFunc(aModule.FuncArray[PKCS11_GetSlotList])(CK_B_FALSE, pSlotList[0], pulCount));
        for i := 0 to pulCount - 1 do
        begin
          SlotInfo := TElPKCS11SlotInfo.Create;
          aModule.FSlotList.Add(SlotInfo);
          SlotInfo.FModule := aModule;
          SlotInfo.FSlotID := pSlotList[i];
          SlotInfo.Refresh;
        end;
      finally
        FreeMem(pSlotList);
      end;
    end;

    aModule.Name := ModuleName;
    aModule.UseCount := 1;
    FRes.WaitToWrite;
    FList.Add(aModule);
    FRes.Done;
    result := aModule;
  except
    aModule.Free;
    raise;
  end;
end;

procedure TElPKCS11ModuleList.UnloadModule(aModule: TElPKCS11Module);
var
  HLib : HINST;
begin
  FRes.WaitToWrite;
  try
    Dec(aModule.UseCount);
    if aModule.UseCount = 0 then
    begin
      if aModule.FInit then
      begin
        aModule.ReleaseSlots;
        if aModule.FuncArray[PKCS11_Finalize] <> nil then
          TPKCS11FinalizeFunc(aModule.FuncArray[PKCS11_Finalize])(nil);
        aModule.FInit := false;
      end;

      HLib := aModule.HLib;
      FList.Remove(aModule);
      FreeAndNil(aModule);
      FreeLibrary(HLib);
    end;
  finally
    FRes.Done;
  end;
end;

constructor TElPKCS11SessionInfo.Create;
begin
  inherited;
end;

destructor TElPKCS11SessionInfo.Destroy;
begin
  inherited;
end;

procedure TElPKCS11SessionInfo.Login(UserType: TElPKCS11UserType; PIN: string);
var
  RSessionInfo: TPKCS11SessionInfo;
  pPin: PChar;
  pPinLength: integer;
begin
  FStorage.FSharedResource.WaitToWrite;
  try
    // since only one login is required per application, do optional login
    if FSlot.FLoggedIn = 0 then
    begin
      // setup PIN parameters
      if FSlot.PinNeeded then
      begin
        pPin := PChar(Pin);
        pPinLength := Length(Pin);
      end
      else
      begin
        pPin := nil;
        pPinLength := 0;
      end;
      // Login
      PKCS11CheckError(TPKCS11LoginFunc(FSlot.FModule.FuncArray[PKCS11_Login])(FHandle, CK_USER_TYPE(UserType), pPin, pPinLength));

      // refresh session state after successful login
      PKCS11CheckError(TPKCS11GetSessionInfoFunc(FSlot.FModule.FuncArray[PKCS11_GetSessionInfo])(FHandle, RSessionInfo));
      FSessionState := TElPKCS11SessionState(RSessionInfo.state);
    end;
    inc(FSlot.FLoggedIn);
  finally
    FStorage.FSharedResource.Done;
  end;
end;


procedure TElPKCS11SessionInfo.Logout;
begin
  // if this is the last session, then log out
  dec(FSlot.FLoggedIn);
  if FSlot.FLoggedIn = 0 then
    PKCS11CheckError(TPKCS11LogoutFunc(FSlot.FModule.FuncArray[PKCS11_Logout])(FHandle));
end;

constructor TElPKCS11Module.Create;
begin
  inherited;
  FSlotList := TList.Create;
  SetLength(FUniqueID, 16);
  SBRndGenerate(@FUniqueID[0], Length(FUniqueID));
end;

destructor TElPKCS11Module.Destroy;
begin
  ReleaseSlots;
  FreeAndNil(FSlotList);
  inherited;
end;



function TElPKCS11Module.GetFunctionList: Integer;
var GetFuncList: TPKCS11GetFunctionListFunc;
    FuncList : PPKCS11FunctionList;
    i, j : integer;
begin
  GetFuncList := TPKCS11GetFunctionListFunc(GetProcAddress(HLib, 'C_GetFunctionList'));
  if @GetFuncList <> nil then
  begin
    result := GetFuncList(FuncList);
    if result = CKR_OK then
    begin
      FuncArray[PKCS11_Initialize] := FuncList.C_Initialize;
      FuncArray[PKCS11_Finalize] := FuncList.C_Finalize;
      FuncArray[PKCS11_GetInfo] := FuncList.C_GetInfo;
      FuncArray[PKCS11_GetFunctionList] := FuncList.C_GetFunctionList;
      FuncArray[PKCS11_GetSlotList] := FuncList.C_GetSlotList;
      FuncArray[PKCS11_GetSlotInfo] := FuncList.C_GetSlotInfo;
      FuncArray[PKCS11_GetTokenInfo] := FuncList.C_GetTokenInfo;
      FuncArray[PKCS11_GetMechanismList] := FuncList.C_GetMechanismList;
      FuncArray[PKCS11_GetMechanismInfo] := FuncList.C_GetMechanismInfo;
      FuncArray[PKCS11_InitToken] := FuncList.C_InitToken;
      FuncArray[PKCS11_InitPIN] := FuncList.C_InitPIN;
      FuncArray[PKCS11_SetPIN] := FuncList.C_SetPIN;
      FuncArray[PKCS11_OpenSession] := FuncList.C_OpenSession;
      FuncArray[PKCS11_CloseSession] := FuncList.C_CloseSession;
      FuncArray[PKCS11_CloseAllSessions] := FuncList.C_CloseAllSessions;
      FuncArray[PKCS11_GetSessionInfo] := FuncList.C_GetSessionInfo;
      FuncArray[PKCS11_GetOperationState] := FuncList.C_GetOperationState;
      FuncArray[PKCS11_SetOperationState] := FuncList.C_SetOperationState;
      FuncArray[PKCS11_Login] := FuncList.C_Login;
      FuncArray[PKCS11_Logout] := FuncList.C_Logout;
      FuncArray[PKCS11_CreateObject] := FuncList.C_CreateObject;
      FuncArray[PKCS11_CopyObject] := FuncList.C_CopyObject;
      FuncArray[PKCS11_DestroyObject] := FuncList.C_DestroyObject;
      FuncArray[PKCS11_GetObjectSize] := FuncList.C_GetObjectSize;
      FuncArray[PKCS11_GetAttributeValue] := FuncList.C_GetAttributeValue;
      FuncArray[PKCS11_SetAttributeValue] := FuncList.C_SetAttributeValue;
      FuncArray[PKCS11_FindObjectsInit] := FuncList.C_FindObjectsInit;
      FuncArray[PKCS11_FindObjects] := FuncList.C_FindObjects;
      FuncArray[PKCS11_FindObjectsFinal] := FuncList.C_FindObjectsFinal;
      FuncArray[PKCS11_EncryptInit] := FuncList.C_EncryptInit;
      FuncArray[PKCS11_Encrypt] := FuncList.C_Encrypt;
      FuncArray[PKCS11_EncryptUpdate] := FuncList.C_EncryptUpdate;
      FuncArray[PKCS11_EncryptFinal] := FuncList.C_EncryptFinal;
      FuncArray[PKCS11_DecryptInit] := FuncList.C_DecryptInit;
      FuncArray[PKCS11_Decrypt] := FuncList.C_Decrypt;
      FuncArray[PKCS11_DecryptUpdate] := FuncList.C_DecryptUpdate;
      FuncArray[PKCS11_DecryptFinal] := FuncList.C_DecryptFinal;
      FuncArray[PKCS11_DigestInit] := FuncList.C_DigestInit;
      FuncArray[PKCS11_Digest] := FuncList.C_Digest;
      FuncArray[PKCS11_DigestUpdate] := FuncList.C_DigestUpdate;
      FuncArray[PKCS11_DigestKey] := FuncList.C_DigestKey;
      FuncArray[PKCS11_DigestFinal] := FuncList.C_DigestFinal;
      FuncArray[PKCS11_SignInit] := FuncList.C_SignInit;
      FuncArray[PKCS11_Sign] := FuncList.C_Sign;
      FuncArray[PKCS11_SignUpdate] := FuncList.C_SignUpdate;
      FuncArray[PKCS11_SignFinal] := FuncList.C_SignFinal;
      FuncArray[PKCS11_SignRecoverInit] := FuncList.C_SignRecoverInit;
      FuncArray[PKCS11_SignRecover] := FuncList.C_SignRecover;
      FuncArray[PKCS11_VerifyInit] := FuncList.C_VerifyInit;
      FuncArray[PKCS11_Verify] := FuncList.C_Verify;
      FuncArray[PKCS11_VerifyUpdate] := FuncList.C_VerifyUpdate;
      FuncArray[PKCS11_VerifyFinal] := FuncList.C_VerifyFinal;
      FuncArray[PKCS11_VerifyRecoverInit] := FuncList.C_VerifyRecoverInit;
      FuncArray[PKCS11_VerifyRecover] := FuncList.C_VerifyRecover;
      FuncArray[PKCS11_DigestEncryptUpdate] := FuncList.C_DigestEncryptUpdate;
      FuncArray[PKCS11_DecryptDigestUpdate] := FuncList.C_DecryptDigestUpdate;
      FuncArray[PKCS11_SignEncryptUpdate] := FuncList.C_SignEncryptUpdate;
      FuncArray[PKCS11_DecryptVerifyUpdate] := FuncList.C_DecryptVerifyUpdate;
      FuncArray[PKCS11_GenerateKey] := FuncList.C_GenerateKey;
      FuncArray[PKCS11_GenerateKeyPair] := FuncList.C_GenerateKeyPair;
      FuncArray[PKCS11_WrapKey] := FuncList.C_WrapKey;
      FuncArray[PKCS11_UnwrapKey] := FuncList.C_UnwrapKey;
      FuncArray[PKCS11_DeriveKey] := FuncList.C_DeriveKey;
      FuncArray[PKCS11_SeedRandom] := FuncList.C_SeedRandom;
      FuncArray[PKCS11_GenerateRandom] := FuncList.C_GenerateRandom;
      FuncArray[PKCS11_GetFunctionStatus] := FuncList.C_GetFunctionStatus;
      FuncArray[PKCS11_CancelFunction] := FuncList.C_CancelFunction;
      FuncArray[PKCS11_WaitForSlotEvent] := FuncList.C_WaitForSlotEvent;
    end
    else
    begin
      for i := PKCS11_FUNC_FIRST to PKCS11_FUNC_LAST do
      begin
        FuncArray[i] := GetProcAddress(HLib, PChar(PKCS11_FuncNames[i]));

        if FuncArray[i] = nil then
        begin
          for j := PKCS11_FUNC_FIRST to i do
            FuncArray[j] := nil;

          result := GetLastError();
          FreeLibrary(HLib);
          raise ECertStorageError.Create(SPKCS11ProviderNoFunctions, result{$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});
        end;
      end;
    end;
  end
  else
  begin
    FreeLibrary(HLib);
    raise ECertStorageError.Create(SPKCS11ProviderNoFunctions, 0{$ifndef FPC}{$ifndef HAS_DEF_PARAMS}, 0{$endif}{$endif});
  end;
end;

procedure TElPKCS11Module.ReleaseSlots;
begin
  while FSlotList.Count > 0 do
  begin
    TElPKCS11SlotInfo(FSlotList[0]).Free;
    FSlotList.Delete(0);
  end;
end;

{$ifdef SBPKCS11ProxyDriver}
function TElPKCS11Module.Load(ModuleName: string): CK_RV;
var
  Info: TPKCS11Info;
  InitArgs : TPKCS11InitializeArgs;
begin
  // Load module
  HLib := LoadLibrary(PChar(ModuleName));
  if HLib = 0 then
  begin
    Result := CK_RV(-2);
    Exit;
  end;

  FInit := false;

  try
    GetFunctionList;

    InitArgs.CreateMutex := @CreateMutexCallback;
    InitArgs.DestroyMutex := @DestroyMutexCallback;
    InitArgs.LockMutex := @LockMutexCallback;
    InitArgs.UnlockMutex := @UnlockMutexCallback;
    InitArgs.pReserved := nil;
    InitArgs.flags := CKF_OS_LOCKING_OK;

    Result := TPKCS11InitializeFunc(FuncArray[PKCS11_Initialize])(@InitArgs) and $FFFF;
    if Result = CKR_CANT_LOCK then
    begin
      InitArgs.CreateMutex := nil;
      InitArgs.DestroyMutex := nil;
      InitArgs.LockMutex := nil;
      InitArgs.UnlockMutex := nil;

      InitArgs.flags := CKF_OS_LOCKING_OK;
      Result := TPKCS11InitializeFunc(FuncArray[PKCS11_Initialize])(@InitArgs) and $FFFF;
    end;

    if Result = CKR_OK then
    begin
      FInit := true;
      if FuncArray[PKCS11_GetInfo] <> nil then
        Result := TPKCS11GetInfoFunc(FuncArray[PKCS11_GetInfo])(Info)
      else
        Result := CK_RV(-1);
    end;
    if Result = CKR_OK then
    begin
      if Info.cryptokiVersion.Major < 2 then
        Result := CKR_FUNCTION_NOT_SUPPORTED;
    end;

    if Result <> CKR_OK then
    begin
      if FInit then
        if FuncArray[PKCS11_Finalize] <> nil then
          TPKCS11FinalizeFunc(FuncArray[PKCS11_Finalize])(nil);

      FreeLibrary(HLib);
      HLib := 0;
    end;

    Name := ModuleName;
    UseCount := 1;
  except
    Result := CK_RV(-1);
  end;
end;
{$endif}

function GetContextByCertificate(Cert: TElX509Certificate): TElPKCS11StgCtx;
  function ExtractData(const S : string; var TokenSerial: string; var SlotID:
    CK_SLOT_ID; var Hash: string; var ModuleID : ByteArray; var ConsumerID:
    ByteArray) : boolean;
  var
    Index, Size: integer;
    Tmp : string;
  begin
    Result := false;
    { "PKCS#11" }
    Index := Pos(#13#10, S);
    if Index < 1 then
      Exit;
    Tmp := Copy(S, Index + 2, Length(S));

    { Token serial }
    Index := Pos(#13#10, Tmp);
    if Index < 1 then
      Exit;
    TokenSerial := Copy(Tmp, 1, Index - 1);
    Tmp := Copy(Tmp, Index + 2, Length(Tmp));

    { Certificate hash }
    Index := Pos(#13#10, Tmp);
    if Index < 1 then
      Exit;
    Hash := Copy(Tmp, 1, Index - 1);
    Tmp := Copy(Tmp, Index + 2, Length(Tmp));

    { Slot ID }
    Index := Pos(#13#10, Tmp);
    if Index < 1 then
      Exit;
    try
      SlotID := StrToInt(Copy(Tmp, 1, Index - 1));
    except
      Exit;
    end;
    Tmp := Copy(Tmp, Index + 2, Length(Tmp));

    { Module ID }
    Index := Pos(#13#10, Tmp);
    if Index < 1 then
      Exit;
    try
      Size := (Index - 1) div 2;
      SetLength(ModuleID, Size);
      StringToBinary(Copy(Tmp, 1, Index - 1), @ModuleID[0], Size);
      SetLength(ModuleID, Size);
    except
      Exit;
    end;
    Tmp := Copy(Tmp, Index + 2, Length(Tmp));

    { Consumer ID }
    Index := Pos(#13#10, Tmp);
    if Index < 1 then
      Exit;
    try
      Size := (Index - 1) div 2;
      SetLength(ConsumerID, Size);
      StringToBinary(Copy(Tmp, 1, Index - 1), @ConsumerID[0], Size);
      SetLength(ConsumerID, Size);
    except
      Exit;
    end;

    Result := true;
  end;
var
  Hash : string;
  SlotID : CK_SLOT_ID;
  Serial : string;
  i, index : integer;
  ModuleID : ByteArray;
  ConsumerID : ByteArray;
  aModule : TElPKCS11Module;
  Slot : TElPKCS11SlotInfo;
  Storage: TElPKCS11CertStorage;
begin
  if Cert.BelongsTo <> BT_PKCS11 then
    raise ECertStorageError.Create(SNotPKCS11Certificate);
  if not ExtractData(Cert.StorageName, Serial, SlotID, Hash, ModuleID, ConsumerID) then
    raise ECertStorageError.Create(SNotPKCS11Certificate);

  Result := nil;

  index := -1;
  for i := 0 to ModuleList.FList.Count - 1 do
  begin
    aModule := TElPKCS11Module(ModuleList.FList[i]);
    if (Length(aModule.FUniqueID) = Length(ModuleID)) and (CompareMem(@ModuleID[0],
      @aModule.FUniqueID[0], Length(ModuleID))) then
    begin
      index := i;
      Break;
    end;
  end;
  if index = -1 then
    Exit;

  aModule := TElPKCS11Module(ModuleList.FList[index]);
  index := -1;
  for i := 0 to aModule.FSlotList.Count - 1 do
  begin
    if (TElPKCS11SlotInfo(aModule.FSlotList[i]).FSlotID = SlotID) and
      (TElPKCS11SlotInfo(aModule.FSlotList[i]).FTokenSerial = Serial) then
    begin
      index := i;
      Break;
    end;
  end;
  if index = -1 then
    Exit;

  Slot := TElPKCS11SlotInfo(aModule.FSlotList[index]);
  index := -1;
  for i := 0 to Slot.FConsumers.Count - 1 do
  begin
    Storage := TElPKCS11CertStorage(Slot.FConsumers[i]);
    if (Length(Storage.FUniqueID) = Length(ConsumerID)) and (CompareMem(@ConsumerID[0],
      @Storage.FUniqueID[0], Length(ConsumerID))) then
    begin
      index := i;
      Break;
    end;
  end;
  if index = -1 then
    Exit;

  Storage := TElPKCS11CertStorage(Slot.FConsumers[index]);
  index := -1;
  for i := 0 to Storage.FCtxList.Count - 1 do
  begin
    if TElPKCS11StgCtx(Storage.FCtxList[i]).Hash = Hash then
    begin
      index := i;
      Break;
    end;
  end;
  if index = -1 then
    Exit;

  Result := TElPKCS11StgCtx(Storage.FCtxList[index]);

end;



initialization

  ModuleList := TElPKCS11ModuleList.Create;

finalization

  FreeAndNil(ModuleList);


{$endif SBB_NO_PKCS11}

end.

