
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBPKCS12;

interface

uses
  SysUtils,
  Classes,
  {$ifndef CLX_USED}
  Windows,
  {$endif}
  SBPKCS7,
  SBPKCS7Utils,
  SBASN1Tree,
  SBX509,
  SBRandom,
  SBUtils,
  SBConstants,
  SBMD,
  SBSHA,
  SBMath,
  {$ifndef SBB_NO_DES}
  SBDES,
  {$endif}
  SBHMAC,
  SBRSA,
  SBDSA,
  SBRC4,
  SBCustomCertStorage;


const
  SB_PKCS12_ERROR_INVALID_ASN_DATA                      = Integer($1F01);
  SB_PKCS12_ERROR_NO_DATA                               = Integer($1F02);
  SB_PKCS12_ERROR_INVALID_DATA                          = Integer($1F03);
  SB_PKCS12_ERROR_INVALID_VERSION                       = Integer($1F04);
  SB_PKCS12_ERROR_INVALID_CONTENT                       = Integer($1F05);
  SB_PKCS12_ERROR_INVALID_AUTHENTICATED_SAFE_DATA       = Integer($1F06);
  SB_PKCS12_ERROR_INVALID_MAC_DATA                      = Integer($1F07);
  SB_PKCS12_ERROR_INVALID_SAFE_CONTENTS                 = Integer($1F08);
  SB_PKCS12_ERROR_INVALID_SAFE_BAG                      = Integer($1F09);
  SB_PKCS12_ERROR_INVALID_SHROUDED_KEY_BAG              = Integer($1F0A);
  SB_PKCS12_ERROR_UNKNOWN_PBE_ALGORITHM                 = Integer($1F0B);
  SB_PKCS12_ERROR_INTERNAL_ERROR                        = Integer($1F0C);
  SB_PKCS12_ERROR_INVALID_PBE_ALGORITHM_PARAMS          = Integer($1F0D);
  SB_PKCS12_ERROR_INVALID_CERT_BAG                      = Integer($1F0E);
  SB_PKCS12_ERROR_UNSUPPORTED_CERTIFICATE_TYPE          = Integer($1F0F);
  SB_PKCS12_ERROR_INVALID_PRIVATE_KEY                   = Integer($1F10);
  SB_PKCS12_ERROR_INVALID_MAC                           = Integer($1F11);
  SB_PKCS12_ERROR_NO_CERTIFICATES                       = Integer($1F12);
  SB_PKCS12_ERROR_INVALID_PASSWORD                      = Integer($1F13);
  SB_PKCS12_ERROR_BUFFER_TOO_SMALL                      = Integer($1F14);

type
  TElPKCS12Message = class
  private
    FPrivateKeys : TStringList;
    FDigestAlgorithm : string;
    FDigestAlgorithmParams : string;
    FDigest : string;
    FSalt : string;
    FPassword : string;
    FIterations : integer;
    FCertificates : TElMemoryCertStorage;
    FKeyEncryptionAlgorithm : integer;
    FCertEncryptionAlgorithm : integer;
    FRandom : TElRandom;
    FLastKeyId : cardinal;
  protected
    function ProcessAuthenticatedSafe(Buffer : pointer; Size : integer) : integer;
    function ProcessMACData(Tag : TElASN1ConstrainedTag; Buffer : pointer; Size : integer) : integer;
    function ProcessSafeBags(P : pointer; Size : integer) : integer;
    function ProcessPrivateKeyInfo(Buffer : pointer; Size : integer; var Algorithm :
      string; var PrivateKey : string) : integer;
    function ProcessSafeContents(Mes : TElPKCS7Message) : integer;
    function ProcessSafeBag(Tag : TElASN1ConstrainedTag) : integer;
    function ProcessShroudedKeyBag(Tag : TElASN1ConstrainedTag) : integer;
    function ProcessCertBag(Tag : TElASN1ConstrainedTag) : integer;
    function ProcessEncryptedSafeBags(Tag : TElPKCS7Message) : integer;

    function EncryptRC2(InBuffer : pointer; InSize : integer;
      OutBuffer : pointer; var OutSize : integer; const Key : string; const IV : string) : boolean;
    function DecryptRC2(InBuffer : pointer; InSize : integer; OutBuffer :
      pointer; var OutSize : integer; const Key : string; const IV : string) : boolean;

    function Encrypt3DES(InBuffer : pointer; InSize : integer;
      OutBuffer : pointer; var OutSize : integer; const Key : string; const IV : string) : boolean;
    function Decrypt3DES(InBuffer : pointer; InSize : integer;
      OutBuffer : pointer; var OutSize : integer; const Key : string; const IV : string) : boolean;
    function EncryptRC4(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer; const Key : string) : boolean;
    function DecryptRC4(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer; const Key : string) : boolean;
    function EncryptContent(InBuffer : pointer; InSize : integer; OutBuffer : pointer;
      var OutSize : integer; Algorithm : integer; const Key : string;
      const IV : string) : boolean;

    function GetKeyAndIVLengths(AlgId : integer; var KeyLen : integer; var IVLen :
      integer) : boolean;
    function KeyCorresponds(Certificate : TElX509Certificate; KeyBuffer : pointer;
      KeySize : integer) : boolean;
    function DeriveKeyFromPassword(const Password : string; const Salt : string; Id : byte;
      HashAlgorithm : integer; Iters : integer; Size : integer) : string;

    function CalculateHashSHA1(Buffer : pointer; Size : integer; Iterations :
      integer) : TMessageDigest160;
    function CalculateHashMD5(Buffer : pointer; Size : integer; Iterations :
      integer) : TMessageDigest128;
    function SaveAuthenticatedSafe(Tag : TElASN1ConstrainedTag; MAC :
      TElASN1ConstrainedTag) : integer;

    function SaveShroudedKeyBag(KeyBuffer : pointer; KeySize : integer; OutBuffer : pointer;
      var OutSize : integer; AlgId : integer) : integer;
    function SaveCertBag(CertBuffer : pointer; CertSize : integer; OutBuffer : pointer;
      var OutSize : integer) : integer;
    function SaveMACData(Buffer : pointer; Size : integer; Tag : TElASN1ConstrainedTag) : integer;

    function ComposeDSAPrivateKey(X : pointer; XSize : integer; Certificate :
      TElX509Certificate; OutBuffer : pointer; var OutSize : integer) : boolean;
    function DecomposeDSAPrivateKey(KeyBlob : pointer; KeyBlobSize : integer;
      PrivateKey : pointer; var PrivateKeySize : integer; Params : pointer;
      var ParamsSize : integer) : boolean;
    function GetPassword : string;
    procedure SetPassword(Value : string);
  public
    constructor Create;
    destructor Destroy; override;

    function LoadFromBuffer(Buffer : pointer; Size : integer) : integer;
    function SaveToBuffer(Buffer : pointer; var Size : integer) : integer;

    property Iterations : integer read FIterations write FIterations;
    property Password : string read GetPassword write SetPassword;
    property Certificates : TElMemoryCertStorage read FCertificates;
    property KeyEncryptionAlgorithm : integer read FKeyEncryptionAlgorithm
      write FKeyEncryptionAlgorithm;
    property CertEncryptionAlgorithm : integer read FCertEncryptionAlgorithm
      write FCertEncryptionAlgorithm;
  end;

function BufToInt(Buffer : pointer; Size : integer) : integer;
function IntToBuf(Number : integer) : string;

type

  EPKCS12Error =  class(ESecureBlackboxError);

procedure RaisePKCS12Error(ErrorCode : integer); 

implementation

uses
  {$ifndef SBB_NO_DES}
  SB3DES,
  {$endif}
  SBRC2
  ;

resourcestring

  sInvalidASNData = 'Invalid ASN.1 sequence';
  sNoData = 'No data';
  sInvalidData = 'Invalid data';
  sInvalidVersion = 'Invalid version';
  sInvalidContent = 'Invalid content';
  sInvalidAuthSafeData = 'Invalid authenticated safe data';
  sInvalidMACData = 'Invalid MAC data';
  sInvalidSafeContents = 'Invalid safe contents';
  sInvalidSafeBag = 'Invalid safe bag';
  sShroudedKeyBag = 'Shrowded key bag';
  sInvalidPBEAlgorithm = 'invalid PBE algorithm';
  sInternalError = 'Internal error';
  sInvalidPBEAlgoParams = 'Invalid PBE algorithm';
  sInvalidCertBag = 'Invalid certificate bag';
  sUnsupportedCertType = 'Unsupported certificate type';
  sInvalidPrivateKey = 'Invalid private key';
  sInvalidMAC = 'Invalid MAC';
  sNoCertificates = 'No certificates found';
  sInvalidPassword = 'Invalid password';
  sBufferTooSmall = 'Buffer too small';
  sPKCS12Error = 'PKCS#12 error';

const

  SB_OID_KEY_BAG                 :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$0c#$0a#$01#$01;
  SB_OID_PKCS8_SHROUDED_KEY_BAG  :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$0c#$0a#$01#$02;
  SB_OID_CERT_BAG                :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$0c#$0a#$01#$03;
  SB_OID_CRL_BAG                 :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$0c#$0a#$01#$04;
  SB_OID_SECRET_BAG              :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$0c#$0a#$01#$05;
  SB_OID_SAFE_CONTENTS_BAG       :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$0c#$0a#$01#$06;
  SB_OID_CERT_TYPE_X509          :TBufferTypeConst =  #$2a#$86#$48#$86#$f7#$0d#$01#$09#$16#$01;

const
  SB_PKCS12_KEYDERIVATION_ID_KEY   = 1;
  SB_PKCS12_KEYDERIVATION_ID_IV    = 2;
  SB_PKCS12_KEYDERIVATION_ID_MAC   = 3;

procedure RaisePKCS12Error(ErrorCode : integer);
begin
  if ErrorCode <> 0 then
    case ErrorCode of
      SB_PKCS12_ERROR_INVALID_ASN_DATA                 : raise EPKCS12Error.Create(sInvalidASNData);
      SB_PKCS12_ERROR_NO_DATA                          : raise EPKCS12Error.Create(sNoData);
      SB_PKCS12_ERROR_INVALID_DATA                     : raise EPKCS12Error.Create(sInvalidData);
      SB_PKCS12_ERROR_INVALID_VERSION                  : raise EPKCS12Error.Create(sInvalidVersion);
      SB_PKCS12_ERROR_INVALID_CONTENT                  : raise EPKCS12Error.Create(sInvalidContent);
      SB_PKCS12_ERROR_INVALID_AUTHENTICATED_SAFE_DATA  : raise EPKCS12Error.Create(sInvalidAuthSafeData);
      SB_PKCS12_ERROR_INVALID_MAC_DATA                 : raise EPKCS12Error.Create(sInvalidMACData);
      SB_PKCS12_ERROR_INVALID_SAFE_CONTENTS            : raise EPKCS12Error.Create(sInvalidSafeContents);
      SB_PKCS12_ERROR_INVALID_SAFE_BAG                 : raise EPKCS12Error.Create(sInvalidSafeBag);
      SB_PKCS12_ERROR_INVALID_SHROUDED_KEY_BAG         : raise EPKCS12Error.Create(sShroudedKeyBag);
      SB_PKCS12_ERROR_UNKNOWN_PBE_ALGORITHM            : raise EPKCS12Error.Create(sInvalidPBEAlgorithm);
      SB_PKCS12_ERROR_INTERNAL_ERROR                   : raise EPKCS12Error.Create(sInternalError);
      SB_PKCS12_ERROR_INVALID_PBE_ALGORITHM_PARAMS     : raise EPKCS12Error.Create(sInvalidPBEAlgoParams);
      SB_PKCS12_ERROR_INVALID_CERT_BAG                 : raise EPKCS12Error.Create(sInvalidCertBag);
      SB_PKCS12_ERROR_UNSUPPORTED_CERTIFICATE_TYPE     : raise EPKCS12Error.Create(sUnsupportedCertType);
      SB_PKCS12_ERROR_INVALID_PRIVATE_KEY              : raise EPKCS12Error.Create(sInvalidPrivateKey);
      SB_PKCS12_ERROR_INVALID_MAC                      : raise EPKCS12Error.Create(sInvalidMAC);
      SB_PKCS12_ERROR_NO_CERTIFICATES                  : raise EPKCS12Error.Create(sNoCertificates);
      SB_PKCS12_ERROR_INVALID_PASSWORD                 : raise EPKCS12Error.Create(sInvalidPassword);
      SB_PKCS12_ERROR_BUFFER_TOO_SMALL                 : raise EPKCS12Error.Create(sBufferTooSmall);
      else
          raise EPKCS12Error.Create(sPKCS12Error + '#' + IntToStr(ErrorCode));
    end;
end;


function ProcessPBEAlgorithmParams(Buffer : pointer; Size : integer; var
  Salt : string; var Iterations : integer) : boolean;
var
  ParamsTag : TElASN1ConstrainedTag;
  Content : string;
begin
  Result := false;
  ParamsTag := TElASN1ConstrainedTag.Create;
  if not ParamsTag.LoadFromBuffer(Buffer, Size) then
  begin
    ParamsTag.Free;
    Exit;
  end;
  
  if ParamsTag.Count <> 1 then
  begin
    ParamsTag.Free;
    Exit;
  end;
  
  if (not ParamsTag.GetField(0).IsConstrained) or (ParamsTag.GetField(0).TagId <>
    SB_ASN1_SEQUENCE) then
  begin
    ParamsTag.Free;
    Exit;
  end;
  
  if TElASN1ConstrainedTag(ParamsTag.GetField(0)).Count <> 2 then
  begin
    ParamsTag.Free;
    Exit;
  end;
  
  if (TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(0).TagId <> SB_ASN1_OCTETSTRING) or
    (TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(1).IsConstrained) or
    (TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(1).TagId <> SB_ASN1_INTEGER) then
  begin
    ParamsTag.Free;
    Exit;
  end;
  
  Salt := TElASN1SimpleTag(TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(0)).Content;

  Content := TElASN1SimpleTag(TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(1)).Content;

  Iterations := BufToInt(@Content[1], Length(Content));
  Result := true;
  ParamsTag.Free;
end;

function SavePBEAlgorithmParams(Salt : string; Iterations : integer; Tag :
  TElASN1ConstrainedTag) : boolean;
var
  STag : TElASN1SimpleTag;
begin
  Tag.TagId := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;
  STag.Content := CloneBuffer(Salt);
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := IntToBuf(Iterations);
  Result := true;
end;

constructor TElPKCS12Message.Create;
var
  D : double;
{$ifndef CLX_USED}
  C : cardinal;
{$else}
  C : TDateTime;
{$endif}
  Buf : array[0..15] of byte;
begin
  inherited;
  FPrivateKeys := TStringList.Create;
  FCertificates := TElMemoryCertStorage.Create(nil);
  FRandom := TElRandom.Create;
{$ifdef CLX_USED}
  C := Now;
  D := Now;
{$else}
  C := GetTickCount;
  D := Now;
{$endif}
  Move(PByteArray(@C)[0], Buf[0], 4);
  Move(PByteArray(@D)[0], Buf[4], 8);
  Move(PByteArray(@C)[0], Buf[12], 4);
  FRandom.Randomize(@Buf[0], 16);
end;

destructor TElPKCS12Message.Destroy;
begin
  FPrivateKeys.Free;
  FCertificates.Free;
  FRandom.Free;
  inherited;
end;

function TElPKCS12Message.LoadFromBuffer(Buffer : pointer; Size : integer) : integer;
var
  Tag, CTag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  Sz : integer;
  Buf : array of byte;
  CT : string;
  Content : array of byte;
  I, J : integer;
  B : boolean;
begin
  CheckLicenseKey();
  while FCertificates.Count > 0 do
    FCertificates.Remove(0);
  FPrivateKeys.Clear;
  Tag := TElASN1ConstrainedTag.Create;
  if not Tag.LoadFromBuffer(Buffer, Size) then
  begin
    Tag.Free;
    Result := SB_PKCS12_ERROR_INVALID_ASN_DATA;
    Exit;
  end;
  
  if Tag.Count <> 1 then
  begin
    Tag.Free;
    Result := SB_PKCS12_ERROR_NO_DATA;
    Exit;
  end;
  
  if (not Tag.GetField(0).IsConstrained) or (Tag.GetField(0).TagId <> SB_ASN1_SEQUENCE) then
  begin
    Tag.Free;
    Result := SB_PKCS12_ERROR_INVALID_DATA;
    Exit;
  end;
  
  CTag := TElASN1ConstrainedTag(Tag.GetField(0));
  if (CTag.Count > 3) or (CTag.Count < 2) then
  begin
    Tag.Free;
    Result := SB_PKCS12_ERROR_INVALID_DATA;
    Exit;
  end;
  
  if (CTag.GetField(0).IsConstrained) or (CTag.GetField(0).TagId <> SB_ASN1_INTEGER) then
  begin
    Tag.Free;
    Result := SB_PKCS12_ERROR_INVALID_DATA;
    Exit;
  end;
  
  STag := TElASN1SimpleTag(CTag.GetField(0));
  if not CompareContent(STag.Content, TBufferTypeConst(#3)) then
  begin
    Tag.Free;
    Result := SB_PKCS12_ERROR_INVALID_VERSION;
    Exit;
  end;
  
  if (not CTag.GetField(1).IsConstrained) or (CTag.GetField(1).TagId <> SB_ASN1_SEQUENCE) then
  begin
    Tag.Free;
    Result := SB_PKCS12_ERROR_INVALID_DATA;
    Exit;
  end;
  Sz := 0;
  ProcessContentInfo(TElASN1ConstrainedTag(CTag.GetField(1)), nil, Sz, CT);
  SetLength(Content, Sz);
  if not ProcessContentInfo(TElASN1ConstrainedTag(CTag.GetField(1)), @Content[0], Sz, CT) then
  begin
    Tag.Free;
    Result := SB_PKCS12_ERROR_INVALID_CONTENT;
    Exit;
  end;
  
  if not CompareContent(CT, SB_OID_PKCS7_DATA) then
  begin
    Tag.Free;
    Result := SB_PKCS12_ERROR_INVALID_CONTENT;
    Exit;
  end;
  
  SetLength(Content, Sz);
  Result := ProcessAuthenticatedSafe(@Content[0], Sz);
  if Result <> 0 then
  begin
    Tag.Free;
    Exit;
  end;
  
  if CTag.Count = 3 then
  begin
    if (not CTag.GetField(2).IsConstrained) then
    begin
      Result := SB_PKCS12_ERROR_INVALID_DATA;
      Tag.Free;
      Exit;
    end;
    
    Result := ProcessMACData(TElASN1ConstrainedTag(CTag.GetField(2)), @Content[0], Length(Content));
    if Result <> 0 then
    begin
      Tag.Free;
      Exit;
    end;
  end;
  
  { Finding private keys corresponding to certificates }
  for I := 0 to FCertificates.Count - 1 do
  begin
    for J := 0 to FPrivateKeys.Count - 1 do
    begin
      B := KeyCorresponds(FCertificates.Certificates[I], @(FPrivateKeys.Strings[J])[1],
        Length(FPrivateKeys.Strings[J]));
      if B then
      begin
        FCertificates.Certificates[I].LoadKeyFromBuffer(@(FPrivateKeys.Strings[J])[1],
          Length(FPrivateKeys.Strings[J]));
        Break;
      end
      else
      if (not B) and
         (FCertificates.Certificates[I].PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA) then
      begin
        // forming "good" dsa private key structure and loading it
        Sz := 0;
        ComposeDSAPrivateKey(@(FPrivateKeys.Strings[J])[1],
          Length(FPrivateKeys.Strings[J]), FCertificates.Certificates[I],
          nil, Sz);
        SetLength(Buf, Sz);
        if ComposeDSAPrivateKey(@(FPrivateKeys.Strings[J])[1],
          Length(FPrivateKeys.Strings[J]), FCertificates.Certificates[I],
          @Buf[0], Sz) then
        begin
          FCertificates.Certificates[I].LoadKeyFromBuffer(@Buf[0], Word(Sz));
          Break;
        end;
      end;
    end;
  end;
  
  Result := 0;
  Tag.Free;
end;

function TElPKCS12Message.ProcessAuthenticatedSafe(Buffer : pointer; Size :
  integer) : integer;
var
  Tag, CTag : TElASN1ConstrainedTag;
  I, Sz : integer;
  Buf : array of byte;
  Msg : TElPKCS7Message;
begin
  Tag := TElASN1ConstrainedTag.Create;
  if not Tag.LoadFromBuffer(Buffer, Size) then
  begin
    Tag.Free;
    Result := SB_PKCS12_ERROR_INVALID_ASN_DATA;
    Exit;
  end;
  if Tag.Count <> 1 then
  begin
    Tag.Free;
    Result := SB_PKCS12_ERROR_INVALID_ASN_DATA;
    Exit;
  end;
  if (not Tag.GetField(0).IsConstrained) or (Tag.GetField(0).TagId <> SB_ASN1_SEQUENCE) then
  begin
    Tag.Free;
    Result := SB_PKCS12_ERROR_INVALID_AUTHENTICATED_SAFE_DATA;
    Exit;
  end;
  
  CTag := TElASN1ConstrainedTag(Tag.GetField(0));
  Result := 0;
  for I := 0 to CTag.Count - 1 do
  begin
    if (not CTag.GetField(I).IsConstrained) or (CTag.GetField(I).TagId <> SB_ASN1_SEQUENCE) then
    begin
      Tag.Free;
      Result := SB_PKCS12_ERROR_INVALID_AUTHENTICATED_SAFE_DATA;
      Exit;
    end;
    
    Sz := 0;
    CTag.GetField(I).SaveToBuffer(nil, Sz);
    SetLength(Buf, Sz);
    CTag.GetField(I).SaveToBuffer(@Buf[0], Sz);
    Msg := TElPKCS7Message.Create;
    if Msg.LoadFromBuffer(@Buf[0], Sz) <> 0 then
    begin
      FreeAndNil(Msg);
      Tag.Free;
      Result := SB_PKCS12_ERROR_INVALID_AUTHENTICATED_SAFE_DATA;
      Exit;
    end;
    
    Result := ProcessSafeContents(Msg);
    FreeAndNil(Msg);
    if Result <> 0 then
      Break;
  end;
  Tag.Free;
end;

function TElPKCS12Message.ProcessMACData(Tag : TElASN1ConstrainedTag; Buffer :
  pointer; Size : integer) : integer;
var
  CTag : TElASN1ConstrainedTag;
  MACKey : string;
  M160 : TMessageDigest160;
begin
  if (Tag.TagId <> SB_ASN1_SEQUENCE) or (Tag.Count < 2) or (Tag.Count > 3) then
  begin
    Result := SB_PKCS12_ERROR_INVALID_MAC_DATA;
    Exit;
  end;
  { Processing digestInfo }
  if (not Tag.GetField(0).IsConstrained) or (Tag.GetField(0).TagId <> SB_ASN1_SEQUENCE) then
  begin
    Result := SB_PKCS12_ERROR_INVALID_MAC_DATA;
    Exit;
  end;
  CTag := TElASN1ConstrainedTag(Tag.GetField(0));
  if CTag.Count <> 2 then
  begin
    Result := SB_PKCS12_ERROR_INVALID_MAC_DATA;
    Exit;
  end;
  if ProcessAlgorithmIdentifier(CTag.GetField(0), FDigestAlgorithm,
    FDigestAlgorithmParams {$ifndef HAS_DEF_PARAMS}, False{$endif}) <> 0 then
  begin
    Result := SB_PKCS12_ERROR_INVALID_MAC_DATA;
    Exit;
  end;
  if (CTag.GetField(1).IsConstrained) or (CTag.GetField(1).TagId <> SB_ASN1_OCTETSTRING) then
  begin
    Result := SB_PKCS12_ERROR_INVALID_MAC_DATA;
    Exit;
  end;
  FDigest := TElASN1SimpleTag(CTag.GetField(1)).Content;
  { Processing macSalt }
  if (Tag.GetField(1).IsConstrained) or (Tag.GetField(1).TagId <> SB_ASN1_OCTETSTRING) then
  begin
    Result := SB_PKCS12_ERROR_INVALID_MAC_DATA;
    Exit;
  end;
  FSalt := TElASN1SimpleTag(Tag.GetField(1)).Content;
  { Processing iterations }
  if Tag.Count = 3 then
  begin
    if (Tag.GetField(2).IsConstrained) or (Tag.GetField(2).TagId <> SB_ASN1_INTEGER) then
    begin
      Result := SB_PKCS12_ERROR_INVALID_MAC_DATA;
      Exit;
    end;
    FIterations := BufToInt(@TElASN1SimpleTag(Tag.GetField(2)).Content[1],
      Length(TElASN1SimpleTag(Tag.GetField(2)).Content));
  end
  else
    FIterations := 1;
  { Verifying MAC }
  MACKey := DeriveKeyFromPassword(FPassword, FSalt, SB_PKCS12_KEYDERIVATION_ID_MAC,
    SB_ALGORITHM_DGST_SHA1, FIterations, 20);
  M160 := HashMACSHA1(Buffer, Size, MACKey);
  if not SysUtils.CompareMem(@M160, @FDigest[1], 20) then
  begin
    Result := SB_PKCS12_ERROR_INVALID_MAC;
    Exit;
  end;
  Result := 0;
end;

function TElPKCS12Message.ProcessSafeContents(Mes : TElPKCS7Message) : integer;
begin
  if Mes.ContentType = ctData then
  begin
    Result := ProcessSafeBags(@Mes.Data[1], Length(Mes.Data));
  end
  else if Mes.ContentType = ctEncryptedData then
  begin
    Result := ProcessEncryptedSafeBags(Mes);
  end
  else
    Result := SB_PKCS12_ERROR_INVALID_SAFE_CONTENTS;
end;

function TElPKCS12Message.ProcessSafeBags(P : pointer; Size : integer) : integer;
var
  SafeContents : TElASN1ConstrainedTag;
  CTag : TElASN1ConstrainedTag;
  I : integer;
begin
  SafeContents := TElASN1ConstrainedTag.Create;
  if not SafeContents.LoadFromBuffer(P, Size) then
  begin
    SafeContents.Free;
    Result := SB_PKCS12_ERROR_INVALID_SAFE_CONTENTS;
    Exit;
  end;
  
  if (SafeContents.Count <> 1) or (not SafeContents.GetField(0).IsConstrained) or
    (SafeContents.GetField(0).TagId <> SB_ASN1_SEQUENCE) then
  begin
    SafeContents.Free;
    Result := SB_PKCS12_ERROR_INVALID_SAFE_CONTENTS;
    Exit;
  end;
  
  CTag := TElASN1ConstrainedTag(SafeContents.GetField(0));
  Result := 0;
  for I := 0 to CTag.Count - 1 do
  begin
    if (not CTag.GetField(I).IsConstrained) or (CTag.GetField(I).TagId <> SB_ASN1_SEQUENCE) then
    begin
      SafeContents.Free;
      Result := SB_PKCS12_ERROR_INVALID_SAFE_CONTENTS;
      Exit;
    end;
    
    Result := ProcessSafeBag(TElASN1ConstrainedTag(CTag.GetField(I)));
    if Result <> 0 then
      Break;
  end;
  SafeContents.Free;
end;

function TElPKCS12Message.ProcessEncryptedSafeBags(Tag : TElPKCS7Message) : integer;
var
  AlgId : integer;
  Salt, Key, IV : string;
  OutBuf : array of byte;
  Iterations, Sz : integer;
  KeyLen, IVLen : integer;

  B : boolean;
begin
  AlgId := GetPBEAlgorithmByOID(Tag.EncryptedData.EncryptedContent.ContentEncryptionAlgorithm);
  if AlgId = SB_ALGORITHM_UNKNOWN then
  begin
    Result := SB_PKCS12_ERROR_UNKNOWN_PBE_ALGORITHM;
    Exit;
  end;
  if not ProcessPBEAlgorithmParams(@Tag.EncryptedData.EncryptedContent.ContentEncryptionAlgorithmParams[1],
    Length(Tag.EncryptedData.EncryptedContent.ContentEncryptionAlgorithmParams),
    Salt, Iterations) then
  begin
    Result := SB_PKCS12_ERROR_INVALID_PBE_ALGORITHM_PARAMS;
    Exit;
  end;
  if not GetKeyAndIVLengths(AlgId, KeyLen, IVLen) then
  begin
    Result := SB_PKCS12_ERROR_INTERNAL_ERROR;
    Exit;
  end;
  Key := DeriveKeyFromPassword(FPassword, Salt, SB_PKCS12_KEYDERIVATION_ID_KEY,
    SB_ALGORITHM_DGST_SHA1, Iterations, KeyLen);
  IV := DeriveKeyFromPassword(FPassword, Salt, SB_PKCS12_KEYDERIVATION_ID_IV,
    SB_ALGORITHM_DGST_SHA1, Iterations, IVLen);
  Sz := Length(Tag.EncryptedData.EncryptedContent.EncryptedContent);
  SetLength(OutBuf, Sz);
  case AlgId of
    SB_ALGORITHM_PBE_SHA1_3DES : B := Decrypt3DES(@Tag.EncryptedData.EncryptedContent.EncryptedContent[1],
      Sz, @OutBuf[0], Sz, Key, IV);
    SB_ALGORITHM_PBE_SHA1_RC2_40,
    SB_ALGORITHM_PBE_SHA1_RC2_128 : B := DecryptRC2(@Tag.EncryptedData.EncryptedContent.EncryptedContent[1],
      Sz, @OutBuf[0], Sz, Key, IV);
    SB_ALGORITHM_PBE_SHA1_RC4_40,
    SB_ALGORITHM_PBE_SHA1_RC4_128 : B := DecryptRC4(@Tag.EncryptedData.EncryptedContent.EncryptedContent[1],
      Sz, @OutBuf[0], Sz, Key);
  else
    B := false;
  end;
  if not B then
    Result := SB_PKCS12_ERROR_INVALID_PASSWORD
  else
    Result := ProcessSafeBags(@OutBuf[0], Sz);
end;

function TElPKCS12Message.ProcessSafeBag(Tag : TElASN1ConstrainedTag) : integer;
var
  CT : string;
begin
  if Tag.TagId <> SB_ASN1_SEQUENCE then
  begin
    Result := SB_PKCS12_ERROR_INVALID_SAFE_BAG;
    Exit;
  end;
  if (Tag.Count < 2) or (Tag.Count > 3) then
  begin
    Result := SB_PKCS12_ERROR_INVALID_SAFE_BAG;
    Exit;
  end;
  if (Tag.GetField(0).IsConstrained) or (Tag.GetField(0).TagId <> SB_ASN1_OBJECT) then
  begin
    Result := SB_PKCS12_ERROR_INVALID_SAFE_BAG;
    Exit;
  end;
  if (not Tag.GetField(1).IsConstrained) or (Tag.GetField(1).TagId <> SB_ASN1_A0) then
  begin
    Result := SB_PKCS12_ERROR_INVALID_SAFE_BAG;
    Exit;
  end;
  CT := TElASN1SimpleTag(Tag.GetField(0)).Content;
  if CompareContent(CT, SB_OID_PKCS8_SHROUDED_KEY_BAG) then
  begin
    if (TElASN1ConstrainedTag(Tag.GetField(1)).Count <> 1) or
      (not TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0).IsConstrained) then
    begin
      Result := SB_PKCS12_ERROR_INVALID_SAFE_BAG;
      Exit;
    end;
    Result := ProcessShroudedKeyBag(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0)));
  end
  else
  if CompareContent(CT, SB_OID_CERT_BAG) then
  begin
    if (TElASN1ConstrainedTag(Tag.GetField(1)).Count <> 1) or
      (not TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0).IsConstrained) then
    begin
      Result := SB_PKCS12_ERROR_INVALID_SAFE_BAG;
      Exit;
    end;
    Result := ProcessCertBag(TElASN1ConstrainedTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0)));
  end
  else
    Result := SB_PKCS12_ERROR_INVALID_SAFE_BAG;
end;

function TElPKCS12Message.ProcessShroudedKeyBag(Tag : TElASN1ConstrainedTag) : integer;
var
  Alg, Key, Params, Salt, IV : string;
  ParamsTag : TElASN1ConstrainedTag;
  Sz, Iterations, AlgId : integer;
  Data : string;
  KeyLen, IVLen : integer;
  B : boolean;
begin
  Result := SB_PKCS12_ERROR_INVALID_SHROUDED_KEY_BAG;
  if (Tag.TagId <> SB_ASN1_SEQUENCE) then
    Exit;
  if Tag.Count <> 2 then
    Exit;
  if ProcessAlgorithmIdentifier(Tag.GetField(0), Alg, Params {$ifndef HAS_DEF_PARAMS}, False{$endif}) <> 0 then
    Exit;
  AlgId := GetPBEAlgorithmByOID(Alg);
  if AlgId = SB_ALGORITHM_UNKNOWN then
  begin
    Result := SB_PKCS12_ERROR_UNKNOWN_PBE_ALGORITHM;
    Exit;
  end;
  if not GetKeyAndIVLengths(AlgId, KeyLen, IVLen) then
  begin
    Result := SB_PKCS12_ERROR_INTERNAL_ERROR;
    Exit;
  end;
  if (Tag.GetField(1).IsConstrained) or (Tag.GetField(1).TagId <> SB_ASN1_OCTETSTRING) then
    Exit;
  { Processing algorithm params }
  ParamsTag := TElASN1ConstrainedTag.Create;
  if not ParamsTag.LoadFromBuffer(@Params[1], Length(Params)) then
  begin
    ParamsTag.Free;
    Exit;
  end;
  
  if ParamsTag.Count <> 1 then
  begin
    ParamsTag.Free;
    Exit;
  end;
  
  if (not ParamsTag.GetField(0).IsConstrained) or (ParamsTag.GetField(0).TagId <>
    SB_ASN1_SEQUENCE) then
  begin
    ParamsTag.Free;
    Exit;
  end;
  
  if TElASN1ConstrainedTag(ParamsTag.GetField(0)).Count <> 2 then
  begin
    ParamsTag.Free;
    Exit;
  end;
  
  if (TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(0).TagId <> SB_ASN1_OCTETSTRING) or
    (TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(1).IsConstrained) or
    (TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(1).TagId <> SB_ASN1_INTEGER) then
  begin
    ParamsTag.Free;
    Exit;
  end;
  
  Salt := TElASN1SimpleTag(TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(0)).Content;
  Iterations := BufToInt(@TElASN1SimpleTag(TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(1)).Content[1],
    Length(TElASN1SimpleTag(TElASN1ConstrainedTag(ParamsTag.GetField(0)).GetField(1)).Content));
  { Deriving keys and decrypting data }
  Key := DeriveKeyFromPassword(FPassword, Salt, SB_PKCS12_KEYDERIVATION_ID_KEY,
    SB_ALGORITHM_DGST_SHA1, Iterations, KeyLen);
  IV := DeriveKeyFromPassword(FPassword, Salt, SB_PKCS12_KEYDERIVATION_ID_IV,
    SB_ALGORITHM_DGST_SHA1, Iterations, IVLen);
  Sz := Length(TElASN1SimpleTag(Tag.GetField(1)).Content);
  SetLength(Data, Sz);
  case AlgId of
    SB_ALGORITHM_PBE_SHA1_3DES :
      B := Decrypt3DES(@TElASN1SimpleTag(Tag.GetField(1)).Content[1], Sz, @Data[1], Sz,
        Key, IV);
    SB_ALGORITHM_PBE_SHA1_RC2_40,
    SB_ALGORITHM_PBE_SHA1_RC2_128 :
      B := DecryptRC2(@TElASN1SimpleTag(Tag.GetField(1)).Content[1], Sz, @Data[1], Sz,
        Key, IV);
    SB_ALGORITHM_PBE_SHA1_RC4_40,
    SB_ALGORITHM_PBE_SHA1_RC4_128 :
      B := DecryptRC4(@TElASN1SimpleTag(Tag.GetField(1)).Content[1], Sz, @Data[1], Sz,
        Key);
    else
    begin
      Result := SB_PKCS12_ERROR_INTERNAL_ERROR;
      ParamsTag.Free;
      Exit;
    end;
  end;
  
  if not B then
    Result := SB_PKCS12_ERROR_INVALID_PASSWORD
  else
  begin
    { Now Data contains the pkcs8 privateKeyInfo structure }
    Result := ProcessPrivateKeyInfo(@Data[1], Sz, Alg, Key);
    if Result = 0 then
      FPrivateKeys.Add(Key);
  end;
  
  ParamsTag.Free;
end;

function TElPKCS12Message.ProcessCertBag(Tag : TElASN1ConstrainedTag) : integer;
var
  Cert : TElX509Certificate;
begin
  Result := SB_PKCS12_ERROR_INVALID_CERT_BAG;
  if (Tag.TagId <> SB_ASN1_SEQUENCE) or (Tag.Count <> 2) then
    Exit;
  if (Tag.GetField(0).IsConstrained) or (Tag.GetField(0).TagId <> SB_ASN1_OBJECT) then
    Exit;
  if (not Tag.GetField(1).IsConstrained) or (Tag.GetField(1).TagId <> SB_ASN1_A0) then
    Exit;
  { Processing CertType, we support only x.509 }
  if not CompareContent(TElASN1SimpleTag(Tag.GetField(0)).Content,
    SB_OID_CERT_TYPE_X509) then
  begin
    Result := SB_PKCS12_ERROR_UNSUPPORTED_CERTIFICATE_TYPE;
    Exit;
  end;
  if TElASN1ConstrainedTag(Tag.GetField(1)).Count <> 1 then
    Exit;
  if (TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0).IsConstrained) or
    (TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0).TagId <> SB_ASN1_OCTETSTRING) then
    Exit;
  Cert := TElX509Certificate.Create(nil);
  Cert.LoadFromBuffer(@TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0)).Content[1],
    Length(TElASN1SimpleTag(TElASN1ConstrainedTag(Tag.GetField(1)).GetField(0)).Content));
  FCertificates.Add(Cert{$ifndef HAS_DEF_PARAMS}, true{$endif});
  FreeAndNil(Cert);
  Result := 0;
end;

function TElPKCS12Message.ProcessPrivateKeyInfo(Buffer : pointer; Size : integer;
  var Algorithm : string; var PrivateKey : string) : integer;
var
  Tag, CTag : TElASN1ConstrainedTag;
  Params : string;
begin
  Result := SB_PKCS12_ERROR_INVALID_PRIVATE_KEY;
  Tag := TElASN1ConstrainedTag.Create;
  if not Tag.LoadFromBuffer(Buffer, Size) then
  begin
    Tag.Free;
    Exit;
  end;
  
  if Tag.Count <> 1 then
  begin
    Tag.Free;
    Exit;
  end;
  
  if (not Tag.GetField(0).IsConstrained) or (Tag.GetField(0).TagId <> SB_ASN1_SEQUENCE) then
  begin
    Tag.Free;
    Exit;
  end;
  
  CTag := TElASN1ConstrainedTag(Tag.GetField(0));
  if (CTag.Count < 3) or (CTag.Count > 4) then
  begin
    Tag.Free;
    Exit;
  end;
  
  if (CTag.GetField(0).IsConstrained) or (CTag.GetField(0).TagId <> SB_ASN1_INTEGER) then
  begin
    Tag.Free;
    Exit;
  end;
  
  if not CompareContent(TElASN1SimpleTag(CTag.GetField(0)).Content, TBufferTypeConst(#0)) then
  begin
    Tag.Free;
    Exit;
  end;
  
  Result := ProcessAlgorithmIdentifier(CTag.GetField(1), Algorithm, Params {$ifndef HAS_DEF_PARAMS}, False{$endif});
  if Result <> 0 then
  begin
    Tag.Free;
    Exit;
  end;
  
  if (CTag.GetField(2).IsConstrained) or (CTag.GetField(2).TagId <> SB_ASN1_OCTETSTRING) then
  begin
    Result := SB_PKCS12_ERROR_INVALID_PRIVATE_KEY;
    Tag.Free;
    Exit;
  end;
  
  PrivateKey := TElASN1SimpleTag(CTag.GetField(2)).Content;
  Tag.Free;
  Result := 0;
end;

function TElPKCS12Message.DeriveKeyFromPassword(const Password : string; const Salt : string;
  Id : byte; HashAlgorithm : integer; Iters : integer; Size : integer) : string;
var
  FormattedPassword, Diversifier, S, P, SI, TmpS : string;
  fpp : PChar;
  PHash : pointer;
  Tmp, Sz : integer;
  I, J, V : integer;
  U : integer;
  M128 : TMessageDigest128;
  M160 : TMessageDigest160;

  LA, LB, LC, LD, LE : PLInt;
begin
  { 1. Formatting password }
  SetLength(FormattedPassword, Length(Password) * 2 + 2);
  fpp := @FormattedPassword[1];
  for I := 1 to Length(Password) do
  begin
    fpp^ := #0;
    inc(fpp);
    fpp^ := Password[I];
	inc(fpp);
  end;
  FormattedPassword[Length(Password) * 2 + 1]:= #0;
  FormattedPassword[Length(Password) * 2 + 2] := #0;

  { 2. Constructing diversifier }
  V := 64;
  if HashAlgorithm = SB_ALGORITHM_DGST_SHA1 then
    U := 20
  else
  if (HashAlgorithm = SB_ALGORITHM_DGST_MD5) then
    U := 16
  else
  begin
    SetLength(Result, 0);
    Exit;
  end;
  SetLength(Diversifier, V);
  FillChar(Diversifier[1], Length(Diversifier), Id);
  { 3. Constructing S string }
  Tmp := Length(Salt) div V;
  if (Length(Salt) mod V) <> 0 then
    Inc(Tmp);
  Tmp := Tmp * V;

  S := '';
  while Length(S) < Tmp do
    S := S + Salt;
  SetLength(S, Tmp);

  { 4. Constructing P string }
  Tmp := Length(FormattedPassword) div V;
  if (Length(FormattedPassword) mod V) <> 0 then
    Inc(Tmp);
  Tmp := Tmp * V;

  P := '';
  while Length(P) < Tmp do
    P := P + FormattedPassword;
  SetLength(P, Tmp);
  SI := TBufferTypeConst(S) + TBufferTypeConst(P);

  { 5. Iterating }
  Tmp := Size div U;
  if Size mod U <> 0 then
    Inc(Tmp);

  SetLength(Result, 0);

  LCreate(LA);
  LCreate(LB);
  LCreate(LC);
  LCreate(LD);
  LCreate(LE);
  LShiftLeft(LE, V shr 2);
  for I := 1 to Tmp do
  begin
    TmpS := TBufferTypeConst(Diversifier) + TBufferTypeConst(SI);

    if HashAlgorithm = SB_ALGORITHM_DGST_SHA1 then
    begin
      M160 := CalculateHashSHA1(@TmpS[1], Length(TmpS), Iters);
      PHash := @M160;
    end
    else if HashAlgorithm = SB_ALGORITHM_DGST_MD5 then
    begin
      M128 := CalculateHashMD5(@TmpS[1], Length(TmpS), Iters);
      PHash := @M128;
    end
    else
      PHash := nil;

    Assert(PHash <> nil);
    SetLength(TmpS, V);
    for J := 0 to (V div U) - 1 do
      Move(PHash^, TmpS[J * U + 1], U);
    Move(PHash^, TmpS[(V div U) * U + 1], V mod U);
    for J := 0 to ((Length(S) + Length(P)) div V) - 1 do
    begin
      PointerToLInt(LA, @SI[J * V + 1], V);
      PointerToLInt(LB, @TmpS[1], Length(TmpS));
      LAdd(LA, LB, LC);
      LAdd(LC, LD, LA);
      LMod(LA, LE, LB);
      Sz := V;
      LIntToPointer(LB, @SI[J * V + 1], Sz);
    end;
    Sz := Length(Result);
    SetLength(Result, Sz + U);
    Move(PHash^, Result[Sz + 1], U);
  end;
  SetLength(Result, Size);
  LDestroy(LA);
  LDestroy(LB);
  LDestroy(LC);
  LDestroy(LD);
  LDestroy(LE);
end;

function TElPKCS12Message.CalculateHashSHA1(Buffer : pointer; Size : integer;
  Iterations : integer) : TMessageDigest160;
var
  I : integer;
begin
  Result := HashSHA1(Buffer, Size);
  for I := 0 to Iterations - 2 do
    Result := HashSHA1(@Result, 20);
end;

function TElPKCS12Message.CalculateHashMD5(Buffer : pointer; Size : integer;
  Iterations : integer) : TMessageDigest128;
var
  I : integer;
begin
  Result := HashMD5(Buffer, Size);
  for I := 0 to Iterations - 2 do
    Result := HashMD5(@Result, 16);
end;

function TElPKCS12Message.DecryptRC2(InBuffer : pointer; InSize : integer; OutBuffer :
  pointer; var OutSize : integer; const Key : string; const IV : string) : boolean;
var
  RC2Ctx : TRC2Context;
  Sz : cardinal;
begin
  SBRC2.InitializeDecryptionCBC(RC2Ctx, TRC2Key(BytesOfString(Key)), PRC2Buffer(@IV[1])^);
  Sz := OutSize;
  Result := SBRC2.DecryptCBC(RC2Ctx, InBuffer, InSize, OutBuffer, Sz);
  if PByteArray(OutBuffer)[Sz - 1] > 8 then
    Result := false
  else
    OutSize := Sz - PByteArray(OutBuffer)[Sz - 1];
end;

{$ifndef SBB_NO_DES}
function TElPKCS12Message.Decrypt3DES(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer; const Key : string; const IV : string) : boolean;
var
  Ctx : T3DESContext;
  Sz : cardinal;
begin
  SB3DES.InitializeDecryptionCBC(Ctx, P3DESKey(@Key[1])^, P3DESBuffer(@IV[1])^);
  Sz := OutSize;
  Result := SB3DES.DecryptCBC(Ctx, InBuffer, InSize, OutBuffer, Sz);
  if PByteArray(OutBuffer)[Sz - 1] > 8 then
    Result := false
  else
    OutSize := Sz - PByteArray(OutBuffer)[Sz - 1];
end;
{$endif}

function TElPKCS12Message.DecryptRC4(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer; const Key : string) : boolean;
var
  Ctx : TRC4Context;
  Sz : cardinal;
begin
  SBRC4.Initialize(Ctx, TRC4Key(BytesOfString(Key)));
  Sz := InSize;
  Result := SBRC4.Decrypt(Ctx, InBuffer, OutBuffer, Sz);
  OutSize := Sz;
end;

function TElPKCS12Message.GetKeyAndIVLengths(AlgId : integer; var KeyLen : integer;
  var IVLen : integer) : boolean;
begin
  Result := true;
  case AlgId of
    SB_ALGORITHM_PBE_SHA1_3DES :
    begin
      KeyLen := 24;
      IVLen := 8;
    end;
    SB_ALGORITHM_PBE_SHA1_RC4_128 :
    begin
      KeyLen := 16;
      IVLen := 0;
    end;
    SB_ALGORITHM_PBE_SHA1_RC4_40 :
    begin
      KeyLen := 5;
      IVLen := 0;
    end;
    SB_ALGORITHM_PBE_SHA1_RC2_128 :
    begin
      KeyLen := 16;
      IVLen := 8;
    end;
    SB_ALGORITHM_PBE_SHA1_RC2_40 :
    begin
      KeyLen := 5;
      IVLen := 8;
    end;
  else
    Result := false;
  end;
end;

function CompareLongIntMem(Buffer1, Buffer2 : array of byte) : boolean;
var Length1,
    Length2 : integer;
begin
  result := false;
  Length1 := Length(Buffer1);
  Length2 := Length(Buffer2);
  if (Length1 <> Length2) and (Length1 + 1 <> Length2) and (Length1 - 1 <> Length2) then
    exit;
  if Length1 = Length2 then
    result := SysUtils.CompareMem(@Buffer1[0], @Buffer2[0], Length1)
  else
  if Length1 + 1 = Length2 then
    result := (Buffer2[0] = 0) and SysUtils.CompareMem(@Buffer1[0], @Buffer2[1], Length1)
  else
  if Length1 - 1 = Length2 then
    result := (Buffer1[0] = 0) and SysUtils.CompareMem(@Buffer1[1], @Buffer2[0], Length2)
end;

function TElPKCS12Message.KeyCorresponds(Certificate : TElX509Certificate;
  KeyBuffer : pointer; KeySize : integer) : boolean;
var
  A, B, C, D, E, F, G, H, I : array of byte;
  ASize, BSize, CSize, DSize, ESize, FSize, GSize, HSize, ISize : integer;
begin
  Result := false;
  if Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
  begin
    ASize := 0; BSize := 0; CSize := 0;
    Certificate.GetRSAParams(nil, ASize, nil, BSize);
    SetLength(A, ASize);
    SetLength(B, BSize);
    Certificate.GetRSAParams(@A[0], ASize, @B[0], BSize);
    CSize := 0; DSize := 0; ESize := 0;
    SBRSA.DecodePrivateKey(KeyBuffer, KeySize, nil, CSize, nil, DSize, nil, ESize);
    SetLength(C, CSize);
    SetLength(D, DSize);
    SetLength(E, ESize);
    if SBRSA.DecodePrivateKey(KeyBuffer, KeySize, @C[0], CSize, @D[0], DSize,
      @E[0], ESize) then
    begin
      //if (ASize = CSize) and (CompareMem(@C[0], @A[0], ASize)) and
      //  (BSize = DSize) and (CompareMem(@B[0], @D[0], BSize)) then
      //  Result := true;
      result := CompareLongIntMem(C, A) and CompareLongIntMem(B, D);
    end;
  end
  else
  if Certificate.PublicKeyAlgorithm = SB_CERT_ALGORITHM_ID_DSA then
  begin
    ASize := 0; BSize := 0; CSize := 0; DSize := 0;
    Certificate.GetDSSParams(nil, ASize, nil, BSize, nil, CSize, nil, DSize);
    SetLength(A, ASize);
    SetLength(B, BSize);
    SetLength(C, CSize);
    SetLength(D, DSize);
    Certificate.GetDSSParams(@A[0], ASize, @B[0], BSize, @C[0], CSize, @D[0], DSize);
    ESize := 0; FSize := 0; GSize := 0; HSize := 0; ISize := 0;
    SBDSA.DecodePrivateKey(KeyBuffer, KeySize, nil, ESize, nil, FSize, nil, GSize,
      nil, HSize, nil, ISize);
    SetLength(E, ESize);
    SetLength(F, FSize);
    SetLength(G, GSize);
    SetLength(H, HSize);
    SetLength(I, ISize);
    if SBDSA.DecodePrivateKey(KeyBuffer, KeySize, @E[0], ESize, @F[0], FSize,
      @G[0], GSize, @H[0], HSize, @I[0], ISize) then
    begin
      (*
      if (ASize = ESize) and (BSize = FSize) and (CSize = GSize) and (DSize = HSize) and
        (CompareMem(@A[0], @E[0], ASize)) and (CompareMem(@B[0], @F[0], BSize)) and
        (CompareMem(@C[0], @G[0], CSize)) and (CompareMem(@D[0], @H[0], DSize)) then
        Result := true;
      *)
      result := CompareLongIntMem(A, E) and CompareLongIntMem(B, F) and
                CompareLongIntMem(C, G) and CompareLongIntMem(D, H);
    end;
  end;
end;


function TElPKCS12Message.SaveToBuffer(Buffer : pointer; var Size : integer) : integer;
var
  Msg, TagAuth, TagMac : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
begin
  FLastKeyId := $01000001;
  if FCertificates.Count = 0 then
  begin
    Result := SB_PKCS12_ERROR_NO_CERTIFICATES;
    Exit;
  end;
  Msg := TElASN1ConstrainedTag.Create;
  Msg.TagId := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(Msg.GetField(Msg.AddField(false)));
  STag.Content := #3;
  STag.TagId := SB_ASN1_INTEGER;
  TagAuth := TElASN1ConstrainedTag(Msg.GetField(Msg.AddField(true)));
  TagMac := TElASN1ConstrainedTag(Msg.GetField(Msg.AddField(true)));
  Result := SaveAuthenticatedSafe(TagAuth, TagMac);
  if Result <> 0 then
  begin
    Msg.Free;
    Exit;
  end;
  
  if Msg.SaveToBuffer(Buffer, Size) then
    Result := 0
  else
    Result := SB_PKCS12_ERROR_BUFFER_TOO_SMALL;
    
  Msg.Free;
end;

function TElPKCS12Message.SaveAuthenticatedSafe(Tag : TElASN1ConstrainedTag;
  MAC : TElASN1ConstrainedTag) : integer;
var
  Sz : word;
  STag, STagSeq : TElASN1SimpleTag;
  CTag, CTagSeq : TElASN1ConstrainedTag;
  I, OutSz : integer;
  Buf : array of byte;
  Encrypted : string;
begin
  Tag.TagId := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_OBJECT;
  STag.Content := SB_OID_PKCS7_DATA;
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTag.TagId := SB_ASN1_A0;
  STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;
  { Writing private keys and certificates }
  CTagSeq := TElASN1ConstrainedTag.Create;
  CTagSeq.TagId := SB_ASN1_SEQUENCE;
  for I := 0 to FCertificates.Count - 1 do
  begin
    Sz := 0;
    SetLength(Buf, Sz);
    FCertificates.Certificates[I].SaveKeyToBuffer(nil, Sz);
    if (Sz > 0) and (FCertificates.Certificates[I].PublicKeyAlgorithm <> SB_CERT_ALGORITHM_DH_PUBLIC) then
    begin
      SetLength(Buf, Sz);

      FCertificates.Certificates[I].SaveKeyToBuffer(@Buf[0], Sz);
      OutSz := 0;

      SetLength(Encrypted, OutSz);
      SaveShroudedKeyBag(@Buf[0], Sz, nil, OutSz,
        FCertificates.Certificates[I].PublicKeyAlgorithm);
      SetLength(Encrypted, OutSz);
      Result := SaveShroudedKeyBag(@Buf[0], Sz, @Encrypted[1], OutSz,
        FCertificates.Certificates[I].PublicKeyAlgorithm);
      if Result <> 0 then
      begin
        CTagSeq.Free;
        Exit;
      end;
      
      SetLength(Encrypted, OutSz);
      STagSeq := TElASN1SimpleTag(CTagSeq.GetField(CTagSeq.AddField(false)));
      STagSeq.WriteHeader := false;
      STagSeq.Content := CloneBuffer(Encrypted);
    end;
    OutSz := 0;
    SetLength(Encrypted, OutSz);
    SaveCertBag(FCertificates.Certificates[i].CertificateBinary,
      FCertificates.Certificates[I].CertificateSize, nil, OutSz);
    SetLength(Encrypted, OutSz);
    Result := SaveCertBag(FCertificates.Certificates[i].CertificateBinary,
      FCertificates.Certificates[I].CertificateSize, @Encrypted[1], OutSz);
    if Result <> 0 then
    begin
      CTagSeq.Free;
      Exit;
    end;
    
    SetLength(Encrypted, OutSz);
    STagSeq := TElASN1SimpleTag(CTagSeq.GetField(CTagSeq.AddField(false)));
    STagSeq.WriteHeader := false;
    STagSeq.Content := CloneBuffer(Encrypted);
    Inc(FLastKeyId);
  end;
  OutSz := 0;
  SetLength(Encrypted, OutSz);
  CTagSeq.SaveToBuffer(nil, OutSz);
  SetLength(Encrypted, OutSz);
  CTagSeq.SaveToBuffer(@Encrypted[1], OutSz);
  CTagSeq.Free;
  STag.Content := CloneBuffer(Encrypted);
  SaveMACData(@Encrypted[1], OutSz , MAC);
  Result := 0;
end;

function TElPKCS12Message.SaveShroudedKeyBag(KeyBuffer : pointer; KeySize : integer;
  OutBuffer : pointer; var OutSize : integer; AlgId : integer) : integer;
var
  Msg : TElPKCS7Message;
  Alg, Params, Salt, Key, IV, TmpS : string;
  KeyLen, IVLen, Sz, TmpSz : integer;
  MainTag, GlobalTag, CTag, Tag, CTagSeq, KeyTag, KeyTagSeq : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  Attrs : TElPKCS7Attributes;
  Estimating : boolean;
begin
  { Call this routine with OutBuffer set to nil to count estimated output length }
  Estimating := (OutBuffer = nil) ;
  Alg := GetOIDByPBEAlgorithm(FKeyEncryptionAlgorithm);
  if Length(Alg) = 0 then
  begin
    Result := SB_PKCS12_ERROR_UNKNOWN_PBE_ALGORITHM;
    Exit;
  end;
  { The main bag tag }
  MainTag := TElASN1ConstrainedTag.Create;
  MainTag.TagId := SB_ASN1_SEQUENCE;
  GlobalTag := TElASN1ConstrainedTag(MainTag.GetField(MainTag.AddField(true))); //!!!
  GlobalTag.TagId := SB_ASN1_SEQUENCE;
  { bag id }
  STag := TElASN1SimpleTag(GlobalTag.GetField(GlobalTag.AddField(false)));
  STag.TagId := SB_ASN1_OBJECT;
  STag.Content := SB_OID_PKCS8_SHROUDED_KEY_BAG;
  { bag contents }
  CTag := TElASN1ConstrainedTag(GlobalTag.GetField(GlobalTag.AddField(true)));
  CTag.TagId := SB_ASN1_A0;
  Tag := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
  Tag.TagId := SB_ASN1_SEQUENCE;
  CTagSeq := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTagSeq.TagId := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(CTagSeq.GetField(CTagSeq.AddField(false)));
  STag.TagId := SB_ASN1_OBJECT;
  STag.Content := Alg;
  CTagSeq := TElASN1ConstrainedTag(CTagSeq.GetField(CTagSeq.AddField(true)));
  Salt := FRandom.Generate(8);
  SavePBEAlgorithmParams(Salt, FIterations, CTagSeq);
  if not GetKeyAndIVLengths(FKeyEncryptionAlgorithm, KeyLen, IVLen) then
  begin
    Result := SB_PKCS12_ERROR_INTERNAL_ERROR;
    MainTag.Free;
    Exit;
  end;
  
  if not Estimating then
  begin
    Key := DeriveKeyFromPassword(FPassword, Salt, SB_PKCS12_KEYDERIVATION_ID_KEY,
      SB_ALGORITHM_DGST_SHA1, FIterations, KeyLen);
    IV := DeriveKeyFromPassword(FPassword, Salt, SB_PKCS12_KEYDERIVATION_ID_IV,
      SB_ALGORITHM_DGST_SHA1, FIterations, IVLen);
  end;
  { forming PrivateKeyInfo structure and encrypting it }
  KeyTag := TElASN1ConstrainedTag.Create;
  KeyTag.TagId := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(KeyTag.GetField(KeyTag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := #0;
  KeyTagSeq := TElASN1ConstrainedTag(KeyTag.GetField(KeyTag.AddField(true)));
  KeyTagSeq.TagId := SB_ASN1_SEQUENCE;
  Alg := GetOIDByPKAlgorithm(AlgId);
  if Length(Alg) = 0 then
  begin
    Result := SB_PKCS12_ERROR_UNSUPPORTED_CERTIFICATE_TYPE;
    Exit;
  end;
  SetLength(Params, 0);
  if AlgId = SB_CERT_ALGORITHM_ID_DSA then
  begin
    { Decomposing private key and writing params (tmpS) }
    Sz := 0; TmpSz := 0;
    SetLength(TmpS, TmpSz);
    SetLength(Params, Sz);
    DecomposeDSAPrivateKey(KeyBuffer, KeySize, nil, TmpSz, nil, Sz);
    SetLength(TmpS, TmpSz);
    SetLength(Params, Sz);
    if not DecomposeDSAPrivateKey(KeyBuffer, KeySize, @TmpS[1], TmpSz, @Params[1], Sz) then
    begin
      Result := SB_PKCS12_ERROR_INTERNAL_ERROR;
      Exit;
    end;
    SetLength(TmpS, TmpSz);
    SetLength(Params, Sz);
  end
  else if AlgId = SB_CERT_ALGORITHM_ID_RSA_ENCRYPTION then
  begin
    SetLength(TmpS, KeySize);
    if not Estimating then
      Move(KeyBuffer^, TmpS[1], KeySize);
  end
  else if AlgId = SB_CERT_ALGORITHM_DH_PUBLIC then
  begin
    Result := SB_PKCS12_ERROR_UNSUPPORTED_CERTIFICATE_TYPE;
    Exit;
  end
  else
  begin
    Result := SB_PKCS12_ERROR_UNSUPPORTED_CERTIFICATE_TYPE;
    Exit;
  end;
  SaveAlgorithmIdentifier(KeyTagSeq, Alg, Params {$ifndef HAS_DEF_PARAMS}, 0{$endif});
  STag := TElASN1SimpleTag(KeyTag.GetField(KeyTag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;
  STag.Content := CloneBuffer(TmpS);
  KeyTagSeq := TElASN1ConstrainedTag(KeyTag.GetField(KeyTag.AddField(true)));
  Attrs := TElPKCS7Attributes.Create;
  Attrs.Count := 1;
  Attrs.Attributes[0] := TBufferTypeConst(#$2a#$86#$48#$86#$f7#$0d#$01#$09#$15);

  Attrs.Values[0].Add(TBufferTypeConst(#$04#$04 + Chr((FLastKeyId shr 24) and $FF) +
    Chr((FLastKeyId shr 16) and $FF) + Chr((FLastKeyId shr 8) and $FF) +
    Chr(FLastKeyId and $FF)));

  SaveAttributes(KeyTagSeq, Attrs);
  KeyTagSeq.TagId := SB_ASN1_A0; // implicit SET
  Sz := 0;
  SetLength(TmpS, Sz);
  KeyTag.SaveToBuffer(nil, Sz);
  SetLength(TmpS, Sz);
  KeyTag.SaveToBuffer(@TmpS[1], Sz);
  KeyTag.Free;
  SetLength(Params, Sz + IVLen);
  Sz := Length(Params);
  if not Estimating then
  begin
    if not EncryptContent(@TmpS[1], Length(TmpS), @Params[1], Sz, FKeyEncryptionAlgorithm, Key, IV) then
    begin
      Result := SB_PKCS12_ERROR_INTERNAL_ERROR;
      MainTag.Free;
      Attrs.Free;
      Exit;
    end;
  end;
  SetLength(Params, Sz);
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;
  STag.Content := CloneBuffer(Params);
  { bag attributes }
  CTag := TElASN1ConstrainedTag(GlobalTag.GetField(GlobalTag.AddField(true)));
  SaveAttributes(CTag, Attrs);
  Attrs.Free;
  Sz := 0;
  SetLength(TmpS, Sz);
  MainTag.SaveToBuffer(nil, Sz);
  SetLength(TmpS, Sz);
  MainTag.SaveToBuffer(@TmpS[1], Sz);
  MainTag.Free;

  Msg := TElPKCS7Message.Create;
  Msg.ContentType := ctData;
  Msg.Data := TmpS;
  if not Msg.SaveToBuffer(OutBuffer, OutSize) then
    Result := SB_PKCS12_ERROR_INTERNAL_ERROR
  else
    Result := 0;
    
  FreeAndNil(Msg);
  if Estimating then
    Inc(OutSize, 64);
end;

function TElPKCS12Message.SaveCertBag(CertBuffer : pointer; CertSize : integer;
  OutBuffer : pointer; var OutSize : integer) : integer;
var
  Alg, Salt, TmpS, S, Cnt : string;
  Key, IV : string;
  Msg : TElPKCS7Message;
  CTag, Tag, GlobalTag, AttrTag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  KeyLen, IVLen, Sz : integer;
  Attrs : TElPKCS7Attributes;
  Estimating : boolean;
begin
  { Call this routine with OutBuffer set to nil to count estimated output length }
  Estimating := (OutBuffer = nil) ;
  Alg := GetOIDByPBEAlgorithm(FCertEncryptionAlgorithm);
  if Length(Alg) = 0 then
  begin
    Result := SB_PKCS12_ERROR_UNKNOWN_PBE_ALGORITHM;
    Exit;
  end;
  GlobalTag := TElASN1ConstrainedTag.Create;
  GlobalTag.TagId := SB_ASN1_SEQUENCE;
  CTag := TElASN1ConstrainedTag(GlobalTag.GetField(GlobalTag.AddField(true)));
  CTag.TagId := SB_ASN1_SEQUENCE;

  STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
  STag.TagId := SB_ASN1_OBJECT;
  STag.Content := SB_OID_CERT_BAG;

  Tag := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
  Tag.TagId := SB_ASN1_A0;

  AttrTag := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
  Attrs := TElPKCS7Attributes.Create;
  Attrs.Count := 1;
  Attrs.Attributes[0] := TBufferTypeConst(#$2a#$86#$48#$86#$f7#$0d#$01#$09#$15);
  Attrs.Values[0].Add(TBufferTypeConst(#$04#$04 + Chr((FLastKeyId shr 24) and $FF) +
    Chr((FLastKeyId shr 16) and $FF) + Chr((FLastKeyId shr 8) and $FF) +
    Chr(FLastKeyId and $FF)));
  SaveAttributes(AttrTag, Attrs);
  Attrs.Free;
  AttrTag.TagId := SB_ASN1_SET;
  Tag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  Tag.TagId := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_OBJECT;
  STag.Content := SB_OID_CERT_TYPE_X509;
  Tag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  Tag.TagId := SB_ASN1_A0;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;
  SetLength(TmpS, CertSize);
  if not Estimating then
    Move(CertBuffer^, TmpS[1], CertSize);
  STag.Content := CloneBuffer(TmpS);
  Sz := 0;
  SetLength(S, Sz);
  GlobalTag.SaveToBuffer(nil, Sz);
  SetLength(S, Sz);
  GlobalTag.SaveToBuffer(@S[1], Sz);

  GlobalTag.Free;
  Tag := TElASN1ConstrainedTag.Create;
  Salt := FRandom.Generate(8);
  SavePBEAlgorithmParams(Salt, FIterations, Tag);
  Sz := 0;
  SetLength(TmpS, Sz);
  Tag.SaveToBuffer(nil, Sz);
  SetLength(TmpS, Sz);
  Tag.SaveToBuffer(@TmpS[1], Sz);
  Tag.Free;
  if not GetKeyAndIVLengths(FCertEncryptionAlgorithm, KeyLen, IVLen) then
  begin
    Result := SB_PKCS12_ERROR_INTERNAL_ERROR;
    Exit;
  end;
  if not Estimating then
  begin
    Key := DeriveKeyFromPassword(FPassword, Salt, SB_PKCS12_KEYDERIVATION_ID_KEY,
      SB_ALGORITHM_DGST_SHA1, FIterations, KeyLen);
    IV := DeriveKeyFromPassword(FPassword, Salt, SB_PKCS12_KEYDERIVATION_ID_IV,
      SB_ALGORITHM_DGST_SHA1, FIterations, IVLen);
  end;
  Sz := Length(S) + IVLen;
  SetLength(Cnt, Sz);
  if not Estimating then
  begin
    if not EncryptContent(@S[1], Length(S), @Cnt[1], Sz, FCertEncryptionAlgorithm, Key, IV) then
    begin
      Result := SB_PKCS12_ERROR_INTERNAL_ERROR;
      Exit;
    end;
    SetLength(Cnt, Sz);
  end;

  Msg := TElPKCS7Message.Create;
  Msg.ContentType := ctEncryptedData;
  Msg.EncryptedData.EncryptedContent.ContentType := SB_OID_PKCS7_DATA;
  Msg.EncryptedData.EncryptedContent.ContentEncryptionAlgorithm := Alg;
  Msg.EncryptedData.EncryptedContent.ContentEncryptionAlgorithmParams := TmpS;
  Msg.EncryptedData.EncryptedContent.EncryptedContent := Cnt;
  if not Msg.SaveToBuffer(OutBuffer, OutSize) then
    Result := SB_PKCS12_ERROR_INTERNAL_ERROR
  else
    Result := 0;
    
  FreeAndNil(Msg);
  if Estimating then
    Inc(OutSize, 64);
end;

function TElPKCS12Message.SaveMACData(Buffer : pointer; Size : integer; Tag :
  TElASN1ConstrainedTag) : integer;
var
  CTag, CTagId : TElASN1ConstrainedTag;
  STag, STagParam : TElASN1SimpleTag;
  Salt, Key : string;
  M160 : TMessageDigest160;
begin
  Tag.TagId := SB_ASN1_SEQUENCE;
  CTag := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
  CTag.TagId := SB_ASN1_SEQUENCE;
  CTagId := TElASN1ConstrainedTag(CTag.GetField(CTag.AddField(true)));
  CTagId.TagId := SB_ASN1_SEQUENCE;
  SaveAlgorithmIdentifier(CTagId, SB_OID_SHA1, '');
  STag := TElASN1SimpleTag(CTag.GetField(CTag.AddField(false)));
  STag.TagId := SB_ASN1_OCTETSTRING;

  Salt := FRandom.Generate(20);
  STagParam := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STagParam.TagId := SB_ASN1_OCTETSTRING;
  STagParam.Content := CloneBuffer(Salt);
  STagParam := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STagParam.TagId := SB_ASN1_INTEGER;
  STagParam.Content := IntToBuf(FIterations);

  Key := DeriveKeyFromPassword(FPassword, Salt, SB_PKCS12_KEYDERIVATION_ID_MAC,
    SB_ALGORITHM_DGST_SHA1, FIterations, 20);
  M160 := HashMACSHA1(Buffer, Size, Key);
  Move(M160, Key[1], 20);
  STag.Content := Key;
  Result := 0;
end;

function TElPKCS12Message.EncryptContent(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer; Algorithm : integer; const Key : string;
  const IV : string) : boolean;
var
  BlockSize : integer;
begin
  BlockSize := Length(IV);
  if (BlockSize <> 0) and (OutSize < (InSize div BlockSize + 1) * BlockSize) then
  begin
    OutSize := (InSize div BlockSize + 1) * BlockSize;
    Result := false;
    Exit;
  end
  else
  if (BlockSize = 0) and (OutSize < InSize) then
  begin
    OutSize := InSize;
    Result := false;
    Exit;
  end;
  case Algorithm of
    {$ifndef SBB_NO_DES}
    SB_ALGORITHM_PBE_SHA1_3DES :
      Result := Encrypt3DES(InBuffer, InSize, OutBuffer, OutSize, Key, IV);
    {$endif}
    SB_ALGORITHM_PBE_SHA1_RC2_40,
    SB_ALGORITHM_PBE_SHA1_RC2_128 :
      Result := EncryptRC2(InBuffer, InSize, OutBuffer, OutSize, Key, IV);
    SB_ALGORITHM_PBE_SHA1_RC4_40,
    SB_ALGORITHM_PBE_SHA1_RC4_128 :
      Result := EncryptRC4(InBuffer, InSize, OutBuffer, OutSize, Key);
  else
    Result := false;
  end;
end;

function TElPKCS12Message.EncryptRC2(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer; const Key : string; const IV : string) : boolean;
var
  Ctx : TRC2Context;
  Size8 : integer;
  Sz : cardinal;
  Buf : array[0..7] of byte;
begin

  Size8 := (InSize shr 3) shl 3;
  Move(PByteArray(InBuffer)[Size8], Buf[0], InSize - Size8);
  FillChar(Buf[InSize - Size8], 8 - (InSize - Size8), 8 - (InSize - Size8));

  SBRC2.InitializeEncryptionCBC(Ctx, TRC2Key(BytesOfString(Key)), PRC2Buffer(@IV[1])^);
  Sz := Size8;
  SBRC2.EncryptCBC(Ctx, InBuffer, Size8, OutBuffer, Sz);
  Sz := 8;
  SBRC2.EncryptCBC(Ctx, @Buf[0], 8, @PByteArray(OutBuffer)[Size8], Sz);
  OutSize := Size8 + 8;
  Result := true;
end;

{$ifndef SBB_NO_DES}
function TElPKCS12Message.Encrypt3DES(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer; const Key : string; const IV : string) : boolean;
var
  Ctx : T3DESContext;
  Size8 : integer;
  Sz : cardinal;
  Buf : array[0..7] of byte;
begin
  Size8 := (InSize shr 3) shl 3;
  Move(PByteArray(InBuffer)[Size8], Buf[0], InSize - Size8);
  FillChar(Buf[InSize - Size8], 8 - (InSize - Size8), 8 - (InSize - Size8));
  SB3DES.InitializeEncryptionCBC(Ctx, P3DESKey(@Key[1])^, P3DESBuffer(@IV[1])^);
  Sz := Size8;
  SB3DES.EncryptCBC(Ctx, InBuffer, Size8, OutBuffer, Sz);
  Sz := 8;
  SB3DES.EncryptCBC(Ctx, @Buf[0], 8, @PByteArray(OutBuffer)[Size8], Sz);
  OutSize := Size8 + 8;
  Result := true;
end;
{$endif}

function TElPKCS12Message.EncryptRC4(InBuffer : pointer; InSize : integer;
  OutBuffer : pointer; var OutSize : integer; const Key : string) : boolean;
var
  Ctx : TRC4Context;
begin
  SBRC4.Initialize(Ctx, TRC4Key(BytesOfString(Key)));
  SBRC4.Encrypt(Ctx, InBuffer, OutBuffer, InSize);
  OutSize := InSize;
  Result := true;
end;

function TElPKCS12Message.ComposeDSAPrivateKey(X : pointer; XSize : integer;
  Certificate : TElX509Certificate; OutBuffer : pointer; var OutSize :
  integer) : boolean;
var
  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
  P, Q, G, Y, PK : string;
  PSize, QSize, GSize, YSize : integer;
begin
  Result := false;
  if Certificate.PublicKeyAlgorithm <> SB_CERT_ALGORITHM_ID_DSA then
    Exit;
    
  Tag := TElASN1ConstrainedTag.Create;
  Tag.LoadFromBuffer(X, XSize);
  if (Tag.Count <> 1) or (Tag.GetField(0).IsConstrained) or (Tag.GetField(0).TagId <>
    SB_ASN1_INTEGER) then
  begin
    Tag.Free;
    Exit;
  end;
  
  PK := CloneBuffer(TElASN1SimpleTag(Tag.GetField(0)).Content);
  Tag.Free;
  PSize := 0; QSize := 0; GSize := 0; YSize := 0;
  Certificate.GetDSSParams(nil, PSize, nil, QSize, nil, GSize, nil, YSize);
  SetLength(P, PSize);
  SetLength(Q, QSize);
  SetLength(G, GSize);
  SetLength(Y, YSize);
  if (PSize = 0) or (QSize = 0) or (GSize = 0) or (YSize = 0) then
    Exit;
  Certificate.GetDSSParams(@P[1], PSize, @Q[1], QSize, @G[1], GSize, @Y[1], YSize);
  Tag := TElASN1ConstrainedTag.Create;
  Tag.TagId := SB_ASN1_SEQUENCE;
  { writing version (0) }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  STag.Content := #0;
  { writing p }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  if Ord(P[1]) >= 128 then
    P := #0 + P;
  STag.Content := CloneBuffer(P);
  { writing q }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  if Ord(Q[1]) >= 128 then
    Q := #0 + Q;
  STag.Content := CloneBuffer(Q);
  { writing g }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  if Ord(G[1]) >= 128 then
    G := #0 + G;
  STag.Content := CloneBuffer(G);
  { writing y }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  if Ord(Y[1]) >= 128 then
    Y := #0 + Y;
  STag.Content := CloneBuffer(Y);
  { writing x }
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  if Ord(PK[1]) >= 128 then
    PK := #0 + PK;
  STag.Content := CloneBuffer(PK);
  Result := Tag.SaveToBuffer(OutBuffer, OutSize);
  Tag.Free;
end;

function TElPKCS12Message.DecomposeDSAPrivateKey(KeyBlob : pointer; KeyBlobSize :
  integer; PrivateKey : pointer; var PrivateKeySize : integer; Params : pointer;
  var ParamsSize : integer) : boolean;
var
  P, Q, G, Y, X : string;
  PSize, QSize, GSize, YSize, XSize : integer;
  Tag : TElASN1ConstrainedTag;
  STag : TElASN1SimpleTag;
begin
  PSize := 0; QSize := 0; GSize := 0; YSize := 0; XSize := 0;
  SBDSA.DecodePrivateKey(KeyBlob, KeyBlobSize, nil, PSize, nil, QSize, nil,
    GSize, nil, YSize, nil, XSize);
  SetLength(P, PSize);
  SetLength(Q, QSize);
  SetLength(G, GSize);
  SetLength(Y, YSize);
  SetLength(X, XSize);
  Result := SBDSA.DecodePrivateKey(KeyBlob, KeyBlobSize, @P[1], PSize, @Q[1],
    QSize, @G[1], GSize, @Y[1], YSize, @X[1], XSize);
  if not Result then
    Exit;
  { Writing params }
  Tag := TElASN1ConstrainedTag.Create;
  Tag.TagId := SB_ASN1_SEQUENCE;
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  if Ord(P[1]) >= 128 then
    P := #0 + P;
  STag.Content := CloneBuffer(P);
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  if Ord(Q[1]) >= 128 then
    Q := #0 + Q;
  STag.Content := CloneBuffer(Q);
  STag := TElASN1SimpleTag(Tag.GetField(Tag.AddField(false)));
  STag.TagId := SB_ASN1_INTEGER;
  if Ord(G[1]) >= 128 then
    G := #0 + G;
  STag.Content := CloneBuffer(G);
  { Writing private key }
  STag := TElASN1SimpleTag.Create;
  STag.TagId := SB_ASN1_INTEGER;
  if Ord(X[1]) >= 128 then
    X := #0 + X;
  STag.Content := CloneBuffer(X);
  Result := Tag.SaveToBuffer(Params, ParamsSize);
  Result := STag.SaveToBuffer(PrivateKey, PrivateKeySize) and Result;
  Tag.Free;
  STag.Free;
end;

function TElPKCS12Message.GetPassword : string;
begin
  result := (FPassword);
end;

procedure TElPKCS12Message.SetPassword(Value : string);
begin
  FPassword := (Value);
end;

function BufToInt(Buffer : pointer; Size : integer) : integer;
var
  I : integer;
  Coef : integer;
begin
  Result := 0;
  if Size > 4 then
    Exit;
  Coef := 1;
  for I := Size - 1 downto 0 do
  begin
    Result := Result + PByteArray(Buffer)[I] * Coef;
    Coef := Coef shl 8;
  end;
end;

function IntToBuf(Number: integer): string;
begin
  if Number = 0 then
    Result := #0
  else
  begin
    SetLength(Result, 0);
    while Number > 0 do
    begin
      Result := Chr(Number and $FF) + Result;
      Number := Number shr 8;
    end;
    if Ord(Result[1]) >= 128 then
      Result := #0 + Result;
  end;
end;

end.
