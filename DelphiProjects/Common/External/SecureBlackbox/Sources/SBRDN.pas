
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBRDN;

interface

uses 
     Classes,
     SysUtils,
     SBUtils,
     SBASN1Tree;

type
  TElRelativeDistinguishedName =  class(TPersistent)
  private
    FTypes : TStringList;
    FValues : TStringList;
    FTags : TList;
    function GetValues(Index : integer) : BufferType;
    procedure SetValues(Index : integer; Value : BufferType);
    function GetOIDs(Index : integer) : BufferType;
    procedure SetOIDs(Index : integer; Value : BufferType);
    function GetCount : integer;
    procedure SetCount(Value : integer);
    function GetTags(Index: integer) : byte;
    procedure SetTags(Index: integer; Value : byte);
  protected
    procedure AssignTo(Dest: TPersistent); override;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function LoadFromTag(Tag : TElASN1ConstrainedTag; IgnoreTopSequence : boolean {$ifdef HAS_DEF_PARAMS}= false{$endif}) : boolean;
    function SaveToTag(Tag : TElASN1ConstrainedTag) : boolean;
    function Add(OID : BufferType; Value : BufferType; Tag : byte {$ifdef HAS_DEF_PARAMS}= 0{$endif}): integer;
    procedure Remove(Index : integer);
    procedure GetValuesByOID(OID : BufferType; Values : TStringList);
    function IndexOf(OID : BufferType) : integer;
    procedure Clear;
    property Values[Index : integer] : BufferType read GetValues write SetValues;
    property OIDs[Index : integer] : BufferType read GetOIDs write SetOIDs;
    property Tags[Index : integer] : byte read GetTags write SetTags;
    property Count : integer read GetCount write SetCount;
  end;

function FormatRDN(RDN : TElRelativeDistinguishedName) : string;

implementation

resourcestring
  SAssignError = 'Can not assign %s to %s';
  

function TElRelativeDistinguishedName.GetValues(Index : integer) : BufferType;
begin
  Result := FValues[Index];
end;

procedure TElRelativeDistinguishedName.SetValues(Index : integer; Value : BufferType);
begin
  FValues[Index] := Value;
end;

constructor TElRelativeDistinguishedName.Create;
begin
  inherited;
  FTypes := TStringList.Create;
  FValues := TStringList.Create;
  FTags := TList.Create;
end;

destructor TElRelativeDistinguishedName.Destroy;
begin
  FTypes.Free;
  FValues.Free;
  FTags.Free;
  inherited;
end;

procedure TElRelativeDistinguishedName.Remove(Index : integer);
begin
  if (Index >= 0) and (Index < FTypes.Count) then
  begin
    FTypes.Delete(Index);
    FValues.Delete(Index);
    FTags.Delete(Index);
  end;
end;

function TElRelativeDistinguishedName.GetOIDs(Index : integer) : BufferType;
begin
  Result := FTypes[Index];
end;

procedure TElRelativeDistinguishedName.SetOIDs(Index : integer; Value : BufferType);
begin
  FTypes[Index] := Value;
end;

function TElRelativeDistinguishedName.GetCount : integer;
begin
  Result := FTypes.Count;
end;

procedure TElRelativeDistinguishedName.SetCount(Value : integer);
begin
  while FTypes.Count < Value do
  begin
    FTypes.Add('');
    FValues.Add('');
    FTags.Add(pointer(SB_ASN1_OCTETSTRING));
  end;
  while FTypes.Count > Value do
  begin
    FTypes.Delete(FTypes.Count - 1);
    FValues.Delete(FValues.Count - 1);
    FTags.Delete(FTags.Count - 1);
  end;
end;

procedure TElRelativeDistinguishedName.GetValuesByOID(OID : BufferType; Values : TStringList);
var
  I : integer;
begin
  if Values = nil then exit;
  Values.Clear;
  for I := 0 to FTypes.Count - 1 do
  begin
    if CompareStr(FTypes[I], OID) = 0 then
      Values.Add(FValues[I]);
  end;
end;

function TElRelativeDistinguishedName.LoadFromTag(Tag : TElASN1ConstrainedTag;
  IgnoreTopSequence : boolean {$ifdef HAS_DEF_PARAMS}= false{$endif}) : boolean;
var
  TagSet, TagSeq, TagDfn : TElASN1ConstrainedTag;
  I, J, Size : integer;
  OID, Value : BufferType;
  TagID : byte;
begin
  Result := false;
  if (not IgnoreTopSequence) and (Tag.TagId <> SB_ASN1_SEQUENCE) then
    Exit;
  SetCount(0);
  TagDfn := TElASN1ConstrainedTag.Create;
  try
	for I := 0 to Tag.Count - 1 do
	begin
    if (not Tag.GetField(I).IsConstrained) or (Tag.GetField(I).TagID <> SB_ASN1_SET) then
		  Exit;
	  TagSet := TElASN1ConstrainedTag(Tag.GetField(I));
		for J := 0 to TagSet.Count - 1 do
		begin
		  if (not TagSet.GetField(J).IsConstrained) or (TagSet.GetField(J).TagID <> SB_ASN1_SEQUENCE) then
		    Exit;
		
		  TagSeq := TElASN1ConstrainedTag(TagSet.GetField(J));
		  if (TagSeq.Count <> 2) then
		    Exit;
		
		  if (TagSeq.GetField(0).IsConstrained) or (TagSeq.GetField(0).TagId <> SB_ASN1_OBJECT) then
		    Exit;
		
		  OID := TElASN1SimpleTag(TagSeq.GetField(0)).Content;
		  Size := 0;
		  SetLength(Value, Size);
		  TagSeq.GetField(1).SaveToBuffer(nil, Size);
		  SetLength(Value, Size);
		  TagSeq.GetField(1).SaveToBuffer(@Value[1], Size);
		  SetLength(Value, Size);
		  TagID := TagSeq.GetField(1).TagID;
		  TagDfn.Clear;
		  if (TagDfn.LoadFromBuffer(@Value[1], Size)) and (TagDfn.Count > 0) then
		  begin
			  TagID := PByte(@Value[1])^;
			  Size := 0;
			  SetLength(Value, Size);
			  TagDfn.GetField(0).SaveToBuffer(nil, Size);
			  SetLength(Value, Size);
			  TagDfn.GetField(0).WriteHeader := false;
			  TagDfn.GetField(0).SaveToBuffer(@Value[1], Size);
			  SetLength(Value, Size);
		  end;
		  FValues.Add(Value);
		  FTypes.Add(OID);
		  FTags.Add(pointer(TagID));
		end;
	end;
  finally
    TagDfn.Free;
  end;
  Result := true;
end;

function TElRelativeDistinguishedName.SaveToTag(Tag : TElASN1ConstrainedTag): boolean;
var
  TagSet, TagSeq : TElASN1ConstrainedTag;
  TagSimp : TElASN1SimpleTag;
  I : integer;
begin
  Tag.TagId := SB_ASN1_SEQUENCE;
  for I := 0 to FTypes.Count - 1 do
  begin
    TagSet := TElASN1ConstrainedTag(Tag.GetField(Tag.AddField(true)));
    TagSet.TagId := SB_ASN1_SET;
    TagSeq := TElASN1ConstrainedTag(TagSet.GetField(TagSet.AddField(true)));
    TagSeq.TagId := SB_ASN1_SEQUENCE;
    TagSimp := TElASN1SimpleTag(TagSeq.GetField(TagSeq.AddField(false)));
    TagSimp.TagId := SB_ASN1_OBJECT;
    TagSimp.Content := FTypes[I];
    TagSimp := TElASN1SimpleTag(TagSeq.GetField(TagSeq.AddField(false)));
//    TagSimp.TagId := SB_ASN1_PRINTABLESTRING;
    TagSimp.TagID := byte(FTags[I]);
    TagSimp.Content := FValues[I];
  end;
  Result := true;
end;

function TElRelativeDistinguishedName.IndexOf(OID : BufferType) : integer;
begin
  Result := FTypes.IndexOf(OID);
end;

procedure TElRelativeDistinguishedName.Clear;
begin
  FTypes.Clear;
  FValues.Clear;
  FTags.Clear;
end;

function TElRelativeDistinguishedName.GetTags(Index: integer) : byte;
begin
  Result := byte(FTags.Items[Index]);
end;

procedure TElRelativeDistinguishedName.SetTags(Index: integer; Value : byte);
begin
  FTags.Items[Index] := pointer(Value);
end;

procedure TElRelativeDistinguishedName.Assign(Source: TPersistent);
var
  SrcName : string;
  I : integer;
begin
  if Source is TElRelativeDistinguishedName then
  begin
    FTypes.Clear;
    FValues.Clear;
    FTags.Clear;
    FTypes.Assign(TElRelativeDistinguishedName(Source).FTypes);
    FValues.Assign(TElRelativeDistinguishedName(Source).FValues);
    for I := 0 to TElRelativeDistinguishedName(Source).FTags.Count - 1 do
      FTags.Add(TElRelativeDistinguishedName(Source).FTags.Items[I]);
  end
  else
  begin
    if Assigned(Source) then
      SrcName := Source.ClassName
    else
      SrcName := 'nil';
    raise EConvertError.CreateFmt(SAssignError, [SrcName, ClassName]);
  end;
end;

procedure TElRelativeDistinguishedName.AssignTo(Dest: TPersistent);
begin
  if Dest <> nil then
    Dest.Assign(Self)
  else
    raise EConvertError.CreateFmt(SAssignError, [ClassName, 'nil']);
end;

function TElRelativeDistinguishedName.Add(OID : BufferType; Value : BufferType;
  Tag : byte {$ifdef HAS_DEF_PARAMS}= 0{$endif}): integer;
var
  OldCount: integer;
begin
  OldCount := Count;
  Count := Count + 1;
  OIDs[OldCount] := OID;
  Values[OldCount] := Value;
  if Tag <> 0 then
    Tags[OldCount] := Tag
  else
    Tags[OldCount] := SB_ASN1_OCTETSTRING;
  Result := OldCount;
end;

function FormatRDN(RDN : TElRelativeDistinguishedName) : string;
var
  J : integer;
  S : string;
begin
  Result := '';
  for J := 0 to RDN.Count - 1 do
  begin
    if CompareContent(RDN.OIDs[J], SB_CERT_OID_COMMON_NAME) then
      S := 'CN='
    else if CompareContent(RDN.OIDs[J], SB_CERT_OID_COUNTRY) then
      S := 'C='
    else if CompareContent(RDN.OIDs[J], SB_CERT_OID_ORGANIZATION) then
      S := 'O='
    else if CompareContent(RDN.OIDs[J], SB_CERT_OID_ORGANIZATION_UNIT) then
      S := 'OU='
    else if CompareContent(RDN.OIDs[J], SB_CERT_OID_EMAIL) then
      S := 'E='
    else if CompareContent(RDN.OIDs[J], SB_CERT_OID_LOCALITY) then
      S := 'L='
    else if CompareContent(RDN.OIDs[J], SB_CERT_OID_STATE_OR_PROVINCE) then
      S := 'SP='
    else
      S := OIDToStr(RDN.OIDs[J]) + '=';
    Result := Result + S + RDN.Values[J] + ' / ';
  end;
  if RDN.Count > 0 then
    Result := Copy(Result, 1, Length(Result) - 3);
end;

end.
