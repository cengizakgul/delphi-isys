
(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBZlib;

interface

{$ifndef DONT_USE_ZLIB}

uses
  SBZCompressUnit,
  SBZCommonUnit,

  SBZUncompressUnit
  ;
  
type

  TZlibContext =  record
    strm : TZStreamRec;
  end;
  
  TSBZLibOutputFunc = function(Buffer: pointer; Size: integer; Param: pointer): boolean of object;

procedure InitializeCompression(var Context : TZlibContext; CompressionLevel : integer); 
procedure InitializeDecompression(var Context : TZlibContext); 
procedure InitializeCompressionEx(var Context : TZlibContext; Level: integer {$ifdef HAS_DEF_PARAMS}= Z_BEST_COMPRESSION{$endif}
 ); 
procedure InitializeDecompressionEx(var Context : TZlibContext; UseZLib : boolean {$ifdef HAS_DEF_PARAMS}= false{$endif}); 
procedure FinalizeCompressionEx(var Context : TZlibContext; OutBuffer: pointer;
  var OutSize: cardinal);
procedure FinalizeDecompressionEx(var Context : TZlibContext); 

procedure Compress(var Context : TZlibContext; InBuffer : pointer; InSize : cardinal;
  OutBuffer : pointer; var OutSize : cardinal);
procedure Decompress(var Context : TZlibContext; InBuffer : pointer; InSize : cardinal;
  OutBuffer : pointer; var OutSize : cardinal);
procedure CompressEx(var Context : TZlibContext; InBuffer : pointer; InSize : cardinal;
  OutBuffer : pointer; var OutSize : cardinal);
procedure DecompressEx(var Context : TZlibContext; InBuffer : pointer; InSize : cardinal;
  OutputFunc : TSBZlibOutputFunc; Param : pointer);

implementation

 // we DON'T use SysUtils in Delphi.NET
uses
  SysUtils;

resourcestring

  SCompressionFailed = 'Compression failed, deflate returned '+'[%d]';
  SDecompressionFailed = 'Decompression failed, inflate returned '+'[%d]';
  SOutputBufferTooSmall = 'Output buffer too small';

procedure InitializeCompression(var Context : TZlibContext; CompressionLevel : integer);
begin
  if (CompressionLevel < 1) or (CompressionLevel > 9) then
    CompressionLevel := 6;
  deflateInit_(Context.strm, CompressionLevel, zlib_version
    , SizeOf(Context.strm));
end;

procedure InitializeCompressionEx(var Context: TZlibContext; 
    Level: integer {$ifdef HAS_DEF_PARAMS}= Z_BEST_COMPRESSION{$endif});
begin
  deflateInit2_(Context.strm, {Z_BEST_COMPRESSION}Level, Z_DEFLATED, {-MAX_WBITS}-13,
    DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY, ZLIB_VERSION
    , SizeOf(Context.strm)); 
end;

procedure InitializeDecompressionEx(var Context: TZlibContext; 
    UseZLib: boolean {$ifdef HAS_DEF_PARAMS}= false{$endif});
begin
  if UseZLib then
    inflateInit2_(Context.strm, MAX_WBITS, zlib_version
        , SizeOf(Context.strm))
  else
    inflateInit2_(Context.strm, -MAX_WBITS{-13}, zlib_version
        , SizeOf(Context.strm))
end;


procedure Compress(var Context : TZlibContext; InBuffer : pointer; InSize : cardinal;
  OutBuffer : pointer; var OutSize : cardinal);
var
  FBuffer : array[Word] of char;
  Sz : integer;
  Index : integer;
begin
  Context.strm.next_in := InBuffer;
  Context.strm.avail_in := InSize;
  Index := 0;
  repeat
    Context.strm.next_out := FBuffer;
    Context.strm.avail_out := SizeOf(FBuffer);
    Sz := Deflate(Context.strm, Z_PARTIAL_FLUSH);
    if Sz = Z_OK then
      Move(FBuffer[0], PByteArray(OutBuffer)[Index], SizeOf(FBuffer) - Context.strm.avail_out)
    else
      raise Exception.CreateFmt(SCompressionFailed, [Sz]);
    Inc(Index, SizeOf(FBuffer) - Context.strm.avail_out);
  until Context.strm.avail_out <> 0;
  OutSize := Index;
end;

procedure CompressEx(var Context : TZlibContext; InBuffer : pointer; InSize : cardinal;
  OutBuffer : pointer; var OutSize : cardinal);
var
  FBuffer : array[Word] of char;
  Sz : integer;
  Index : integer;
begin
  Context.strm.next_in := InBuffer;
  Context.strm.avail_in := InSize;
  Index := 0;
  repeat
    Context.strm.next_out := FBuffer;
    Context.strm.avail_out := SizeOf(FBuffer);
    Sz := Deflate(Context.strm, Z_PARTIAL_FLUSH{Z_FULL_FLUSH});
    if Sz = Z_OK then
      Move(FBuffer[0], PByteArray(OutBuffer)[Index], SizeOf(FBuffer) - Context.strm.avail_out)
    else
      raise Exception.CreateFmt(SCompressionFailed, [Sz]);
    Inc(Index, SizeOf(FBuffer) - Context.strm.avail_out);
  until Context.strm.avail_out <> 0;
  OutSize := Index;
end;

procedure InitializeDecompression(var Context : TZlibContext);
begin
  
  inflateInit_(Context.strm, zlib_version
    , SizeOf(Context.strm));
end;

procedure Decompress(var Context : TZlibContext; InBuffer : pointer; InSize : cardinal;
  OutBuffer : pointer; var OutSize : cardinal);
var
  FBuffer : array[Word] of char;
  Sz : integer;
  CurrIndex : integer;
begin
  Context.strm.next_in := InBuffer;
  Context.strm.avail_in := InSize;
  CurrIndex := 0;

  while true do
  begin
    if Context.strm.avail_in = 0 then
      Break;

    Context.strm.next_out := FBuffer;
    Context.strm.avail_out := SizeOf(FBuffer);

    Sz := inflate(Context.strm, Z_PARTIAL_FLUSH);
    if (Sz = Z_OK) or (Sz = Z_STREAM_END) then
    begin
      if CurrIndex + SizeOf(FBuffer) - Context.Strm.avail_out > integer(OutSize) then
        raise Exception.Create(SOutputBufferTooSmall); 

      Move(FBuffer[0], PByteArray(OutBuffer)[CurrIndex], SizeOf(FBuffer) - Context.Strm.avail_out);
      Inc(CurrIndex, SizeOf(FBuffer) - Context.Strm.avail_out);
    end
    else
      if Sz = Z_BUF_ERROR then
        abort
      else
        raise Exception.CreateFmt(SDecompressionFailed, [Sz]);

  end;
  OutSize := CurrIndex;
//  inflateEnd(Context.Strm);
end;

procedure DecompressEx(var Context : TZlibContext; InBuffer : pointer; InSize : cardinal;
  OutputFunc: TSBZlibOutputFunc; Param: pointer);
var
  FBuffer : array[Word] of char;
  Sz : integer;
begin
  Context.strm.next_in := InBuffer;
  Context.strm.avail_in := InSize;

  while true do
  begin
    if Context.strm.avail_in = 0 then
      Break;

    Context.strm.next_out := FBuffer;
    Context.strm.avail_out := SizeOf(FBuffer);


    Sz := inflate(Context.strm, Z_FULL_FLUSH);
    if (Sz = Z_OK) or (Sz = Z_STREAM_END) then
    begin
      OutputFunc(@FBuffer[0], SizeOf(FBuffer) - Context.Strm.avail_out, Param);
      if Sz = Z_STREAM_END then
        Break;
    end

    else if Sz = Z_BUF_ERROR then
      abort
    else
      raise Exception.CreateFmt(SDecompressionFailed, [Sz]);
  end;
end;

procedure FinalizeCompressionEx(var Context : TZlibContext; OutBuffer: pointer;
  var OutSize: cardinal);
var
  FBuffer : array[0..4095] of char;
  Sz : integer;
  Index : integer;
begin
  Context.strm.next_in := nil;
  Context.strm.avail_in := 0;
  Index := 0;
  repeat
    Context.strm.next_out := FBuffer;
    Context.strm.avail_out := SizeOf(FBuffer);
    Sz := Deflate(Context.strm, Z_FINISH);
    if Sz = Z_STREAM_END then
      Move(FBuffer[0], PByteArray(OutBuffer)[Index], SizeOf(FBuffer) - Context.strm.avail_out)
    else
      raise Exception.CreateFmt(SCompressionFailed, [Sz]);
    Inc(Index, SizeOf(FBuffer) - Context.strm.avail_out);
  until Context.strm.avail_out <> 0;
  OutSize := Index;
  DeflateEnd(Context.strm);
end;

procedure FinalizeDecompressionEx(var Context : TZlibContext);
begin
  InflateEnd(Context.strm);
end;

{$else}

implementation

{$endif}

end.
