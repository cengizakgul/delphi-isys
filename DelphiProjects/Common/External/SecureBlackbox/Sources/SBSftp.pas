(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSftp;

interface

uses
  SBSSHClient,
  SBSSHCommon,
  SBSSHConstants,
  SBUtils,
  SBConstants,
  Classes,
  SysUtils,
  SBSftpCommon
  {$ifndef CLX_USED},
  Windows,
  Messages
  {$else}
  , libc
  {$endif}
  ;

type

  TElSftpClient = class(TSBControlBase)
  private
    FOutBuffer: ByteArray;
    FInBuffer: ByteArray;
    FInBufferIndex: integer;
    FDataBuffer : ByteArray;
    FDataBufferPtr : pointer;
    FDataBufferPtrSize : integer;
    FDataBufferPtrFilled : integer;

    FTunnel: TElSubsystemSSHTunnel;
    FTunnelConnection: TElSSHTunnelConnection;
    FDataRequests : TList;
    FRequests: TList;
    FOnOpenConnection: TNotifyEvent;
{$IFDEF SBB_Debug}
    FOnDebug: TSSHDebugEvent;
{$ENDIF}
    FOnOpenFile: TSBSftpFileOpenEvent;
    FOnError: TSBSftpErrorEvent;
    FOnSuccess: TSBSftpSuccessEvent;
    FOnDirectoryListing: TSBSftpDirectoryListingEvent;
    FOnFileAttributes: TSBSftpFileAttributesEvent;
    FOnData: TSBSftpDataEvent;
    FOnAbsolutePath: TSBSftpAbsolutePathEvent;
    FOnCloseConnection: TNotifyEvent;
    FCurrentDataID: cardinal;
    FCurrentID: cardinal;
    FSelectedVersion: TSBSftpVersion;
    FSyncRequest: TElsftpOperation;
    FSyncRequestComplete: TElsftpOperation;
    FMessageLoop: TSBSftpMessageLoopEvent;
    FOnVersionSelect : TSBSftpVersionSelectEvent;
    FOnAvailableSpace : TSBSftpAvailableSpaceEvent;
    FOnName : TSBSftpNameEvent;
    {JPM MOdifications for SFTP Extensions and special version handling}
    FSFTPVersion : Cardinal;
    FSFTPExt : TList;
    {end modification}
    {JPM Modification for ASCII newline convention in SFTP4 - used for ASCII transfers}
    FNewLineConvention : String;
    {end modifications}
    {JPM Modifications for SFTP Extensions}
    FOnExtendedReply : TSBSftpDataEvent;
    {end}
    FOnFileHash : TSBSftpFileHashEvent;

    FVersions : TSBSftpVersions;
    FVersion : TSBSftpVersion;
    FActive : boolean;
    FSftpBufferSize: Integer;
    FUseUTF8 : boolean;
    FExtendedProperties : TElSFTPExtendedProperties;
    FUploadBlockSize: Integer;
    FDownloadBlockSize: Integer;

    function GetSFTPExtCount : integer;
    function GetSFTPExt(Index : integer) : TSBSFTPExtendedAttribute;

    procedure PushRequest(Request: PSBSftpRequestInfo);
    procedure PopRequest(Id: cardinal; out Request: PSBSftpRequestInfo);
    procedure PushDataRequest(Request: PSBSftpDataRequestInfo);
    procedure PopDataRequest(Id: cardinal; out Request: PSBSftpDataRequestInfo);
    procedure PopDataRequest2(DataId: cardinal; out Request: PSBSftpDataRequestInfo);
    function HasPendingDataRequests(DataId : cardinal) : boolean;
  protected
    FSynchronousMode: Boolean;
    FLastSyncComment: string;
    FLastSyncError: Integer;
    FLastSyncHandle : TSBSFTPFileHandle;
    FLastSyncText   : string;

    procedure HandleTunnelOpen(Sender: TObject; TunnelConnection:
      TElSSHTunnelConnection);
    procedure HandleTunnelClose(Sender: TObject; TunnelConnection:
      TElSSHTunnelConnection);
    procedure HandleTunnelError(Sender: TObject; Error: integer; Data : pointer);
    procedure HandleTunnelConnectionData(Sender: TObject; Buffer: pointer;
      Size: longint);
    procedure HandleTunnelConnectionExtendedData(Sender: TObject; Buffer:
      pointer; Size: longint);
    procedure HandleTunnelConnectionClose(Sender: TObject; CloseType:
      TSSHCloseType);
    procedure HandleTunnelConnectionError(Sender: TObject; Error: integer);
    procedure Notification(AComponent: TComponent; AOperation: TOperation);
      override;
    procedure SetTunnel(Tunnel: TElSubsystemSSHTunnel);
    function GetTunnel: TElSubsystemSSHTunnel;
{$IFDEF SBB_Debug}
    procedure Debug(Str: string);
{$ENDIF}
    procedure DoSend(Buffer: pointer; Size: integer);
    procedure DoOpenConnection;
    procedure DoOpenFile(Handle: TSBSftpFileHandle);
    procedure DoError(ErrorCode: integer; const Comment: string);
    procedure DoSuccess(const Comment: string);
    procedure DoCloseConnection;
    { Protocol routines }
    procedure AnalizeBuffer;
    procedure ParseOnTransportLayer(P: pointer; Size: integer);
    procedure ParseVersion(P: pointer; Size: integer);
    procedure ParseStatus(P: pointer; Size: integer);
    procedure ParseHandle(P: pointer; Size: integer);
    procedure ParseName(P: pointer; Size: integer);
    function ParseFileAttributes(P: pointer; Size: integer; Attributes:
      TElSftpFileAttributes): integer;
    {JPM Modifications - for extended SFTP Commands}
    function ParseExtendedReply(P: pointer; Size: integer): integer;
    {end}
    procedure ParseAttrs(P: pointer; Size: integer);
    procedure ParseData(P: pointer; Size: integer);
    procedure SendOnTransportLayer(Size: integer; PacketType: byte); overload;
    procedure SendOnTransportLayer(Size: integer; Buffer: pointer; BufferSize:
      integer; PacketType: byte); overload;
    procedure SendFxpInit;
    procedure SendOpenFile(const Filename: string; Modes: TSBSftpFileOpenModes;
      Attributes: TElSftpFileAttributes; Access : TSBSftpFileOpenAccess);
    procedure SendOpenDir(const Path: string);
    procedure SendReadDir(const Handle: TSBSftpFileHandle);
    procedure SendClose(const Handle: TSBSftpFileHandle);
    procedure SendRemoveFile(const Filename: string);
    procedure SendRenameFile(const OldPath, NewPath: string);
    procedure SendMkDir(const Path: string; Attributes: TElSftpFileAttributes);
    procedure SendRmDir(const Path: string);
    procedure SendStat(const Path: string);
    procedure SendLStat(const Path: string);
    procedure SendFStat(const Handle: TSBSftpFileHandle);
    procedure SendSetStat(const Path: string; Attributes: TElSftpFileAttributes);
    procedure SendFSetStat(const Handle: TSBSftpFileHandle; Attributes: TElSftpFileAttributes);
    procedure SendReadlink(const Path: string);
    procedure SendSymlink(const LinkPath: string; const TargetPath: string);
    procedure SendLink(const NewLinkPath: string; const ExistingPath: string;
      SymLink : boolean);
    procedure SendRealPath(const Path: string; Control : TSBSFTPRealpathControl;
      ComposePath : TStringList);
    procedure SendWrite(const Handle: TSBSftpFileHandle; Offset: Int64; Buffer:
      pointer; Size: integer; var CurrentID : Cardinal);
    {JPM Modifications - for extended SFTP Commands}
    procedure SendExtended(const Extension : string; DataBuffer: pointer; Size: integer);
    {end}
    procedure SendBlock(const Handle: TSBSftpFileHandle; Offset, Length : Int64;
      LockMask : TSBSftpFileOpenAccess);
    procedure SendUnblock(const Handle: TSBSftpFileHandle; Offset, Length : Int64);

    procedure SendRead(const Handle: TSBSftpFileHandle; Offset: Int64; Size: integer; var CurrentID : Cardinal);

    procedure SetSynchronousMode(Value: Boolean);
    function IntMessageLoop: boolean;
    procedure SetMessageLoop(Value: TSBSftpMessageLoopEvent);
    procedure OpenInternal;
    function GetActive : boolean;
    procedure SetSftpBufferSize(Value: Integer);


    procedure CompleteDownloadRequest(DataID: Cardinal);
  public
    function Read(const Handle: TSBSftpFileHandle; Offset: Int64; Buffer : Pointer; Size: integer):
        Boolean; overload;
    procedure ReadSync(const Handle: TSBSftpFileHandle; Offset: Int64; Buffer : Pointer; Size: integer);overload;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    function Open : boolean;
    procedure OpenSync;

    {$ifdef BUILDER_USED}
    function SftpCreateFile(const Filename: string): Boolean;
    function SftpOpenFile(const Filename: string; Modes: TSBSftpFileOpenModes; Attributes:
      TElSftpFileAttributes): Boolean; overload;
    function SftpOpenFile(const Filename: string; Modes: TSBSftpFileOpenModes;
      Access : TSBSftpFileOpenAccess; Attributes: TElSftpFileAttributes): Boolean; overload;
    function SftpRemoveDirectory(const Path: string): Boolean;
    function SftpRemoveFile(const Filename: string): Boolean;
    {$endif}

    function CreateFile(const Filename: string): Boolean;
    function CreateFileSync(const Filename: string): TSBSftpFileHandle;

    function OpenFile(const Filename: string; Modes: TSBSftpFileOpenModes; Attributes:
      TElSftpFileAttributes): Boolean; overload;
    function OpenFileSync(const Filename: string; Modes: TSBSftpFileOpenModes; Attributes:
      TElSftpFileAttributes): TSBSftpFileHandle; overload;

    function OpenFile(const Filename: string; Modes: TSBSftpFileOpenModes;
      Access : TSBSftpFileOpenAccess; Attributes: TElSftpFileAttributes): Boolean; overload;
    function OpenFileSync(const Filename: string; Modes: TSBSftpFileOpenModes;
      Access : TSBSftpFileOpenAccess; Attributes: TElSftpFileAttributes): TSBSftpFileHandle; overload;

    function OpenDirectory(const Path: string): Boolean;
    function OpenDirectorySync(const Path: string): TSBSFTPFileHandle;

    function ReadDirectory(const Handle: TSBSftpFileHandle): Boolean;
    function ReadDirectorySync(const Handle: TSBSftpFileHandle): Boolean;

    function RemoveFile(const Filename: string): Boolean;
    procedure RemoveFileSync(const Filename: string);

    function RenameFile(const OldPath, NewPath: string): Boolean;
    procedure RenameFileSync(const OldPath, NewPath: string);

    function MakeDirectory(const Path: string; Attributes: TElSftpFileAttributes): Boolean;
    procedure MakeDirectorySync(const Path: string; Attributes: TElSftpFileAttributes);

    function RemoveDirectory(const Path: string): Boolean;
    procedure RemoveDirectorySync(const Path: string);

    function RequestAttributes(const Path: string; FollowSymLinks: boolean): Boolean; overload;
    procedure RequestAttributesSync(const Path: string; FollowSymLinks: boolean); overload;
    function RequestAttributes(const Handle: TSBSftpFileHandle): Boolean; overload;
    procedure RequestAttributesSync(const Handle: TSBSftpFileHandle); overload;

    function SetAttributes(const Path: string; Attributes: TElSftpFileAttributes):Boolean;
    procedure SetAttributesSync(const Path: string; Attributes: TElSftpFileAttributes);
    function SetAttributesByHandle(const Handle: TSBSftpFileHandle; Attributes: TElSftpFileAttributes):Boolean;
    procedure SetAttributesByHandleSync(const Handle: TSBSftpFileHandle; Attributes: TElSftpFileAttributes);

    function ReadSymLink(const Path: string): Boolean;
    procedure ReadSymLinkSync(const Path: string);

    function CreateSymLink(const LinkPath: string; const TargetPath: string): Boolean;
    procedure CreateSymLinkSync(const LinkPath: string; const TargetPath: string);

    function CreateHardLink(const LinkPath: string; const TargetPath: string): Boolean;
    procedure CreateHardLinkSync(const LinkPath: string; const TargetPath: string);

    function RequestAbsolutePath(const Path: string): Boolean;
    function RequestAbsolutePathSync(const Path: string): string;

    function RequestAbsolutePathEx(const Path: string; Control : TSBSFTPRealpathControl;
      ComposePath : TStringList): Boolean;
    function RequestAbsolutePathExSync(const Path: string; Control : TSBSFTPRealpathControl;
      ComposePath : TStringList): string;

    function Block(const Handle: TSBSftpFileHandle; Offset, Length : Int64;
      LockMask : TSBSftpFileOpenAccess) : boolean;
    procedure BlockSync(const Handle: TSBSftpFileHandle; Offset, Length : Int64;
      LockMask : TSBSftpFileOpenAccess);

    function Unblock(const Handle: TSBSftpFileHandle; Offset, Length : Int64) : boolean;
    procedure UnblockSync(const Handle: TSBSftpFileHandle; Offset, Length : Int64);

    function CloseHandle(const Handle: TSBSftpFileHandle): Boolean;
    procedure CloseHandleSync(const Handle: TSBSftpFileHandle);

    function Read(const Handle: TSBSftpFileHandle; Offset: Int64; Size: integer):
        Boolean; overload;
    procedure ReadSync(const Handle: TSBSftpFileHandle; Offset: Int64; Size: integer); overload;

    function Write(const Handle: TSBSftpFileHandle; Offset: Int64; Buffer: pointer; Size: cardinal): Boolean;
    procedure WriteSync(const Handle: TSBSftpFileHandle; Offset: Int64; Buffer: pointer; Size: cardinal);

    {JPM Modification for SFTP Extended commands}
    function ExtensionCmd(const Extension : string; DataBuffer: pointer; Size: integer) : Boolean; overload;
    function ExtensionCmd(const Extension : string) : boolean; overload;
    {end}
    function TextSeek(const Handle : TSBSftpFileHandle; LineNumber: Int64) : Boolean;
    procedure TextSeekSync(const Handle : TSBSftpFileHandle; LineNumber: Int64);

    function SendVendorID(VendorID : TElSFTPVendorIDExtension) : boolean;
    procedure SendVendorIDSync(VendorID : TElSFTPVendorIDExtension);

    function RequestFileHash(const FileName : string; StartOffset : Int64;
      Length : Int64; BlockSize : cardinal) : boolean;
    procedure RequestFileHashSync(const FileName : string; StartOffset : Int64;
      Length : Int64; BlockSize : cardinal);

    function RequestFileHashByHandle(const Handle : BufferType; StartOffset : Int64;
      Length : Int64; BlockSize : cardinal) : boolean;
    procedure RequestFileHashByHandleSync(const Handle : BufferType; StartOffset : Int64;
      Length : Int64; BlockSize : cardinal);

    function QueryAvailableSpace(const Path : string) : boolean;
    procedure QueryAvailableSpaceSync(const Path : string);

    function QueryHomeDirectory(const Username : string) : boolean;
    procedure QueryHomeDirectorySync(const Username : string);

    function CopyRemoteFile(const Source, Destination : string; Overwrite : boolean) : boolean;
    procedure CopyRemoteFileSync(const Source, Destination : string; Overwrite : boolean);

    function CopyRemoteData(const ReadFrom : TSBSftpFileHandle;
      ReadFromOffset : Int64; const WriteTo : TSBSftpFileHandle;
      WriteToOffset, DataLength : Int64) : boolean;
    procedure CopyRemoteDataSync(const ReadFrom : TSBSftpFileHandle;
      ReadFromOffset : Int64; const WriteTo : TSBSftpFileHandle;
      WriteToOffset, DataLength : Int64);

    function RequestTempFolder : boolean;
    procedure RequestTempFolderSync;

    function MakeTempFolder : boolean;
    procedure MakeTempFolderSync;  


    property Active : boolean read GetActive;
    {JPM MOdifications for SFTP Extensions}
    property Version : TSBSftpVersion read FVersion;
    property SFTPExtCount : integer read GetSFTPExtCount;
    property SFTPExt[Index : integer] : TSBSftpExtendedAttribute read GetSFTPExt;
    {end modification}
    {JPM Modification for ASCII newline convention in SFTP4 - used for ASCII transfers}
    property NewLineConvention : String read FNewLineConvention write FNewLineConvention;
    {end modifications}
    property LastSyncComment: string read FLastSyncComment;
    property LastSyncError: Integer read FLastSyncError;

  published
    property MessageLoop: TSBSftpMessageLoopEvent read FMessageLoop write SetMessageLoop;

    property SynchronousMode: Boolean read FSynchronousMode write SetSynchronousMode;
    property Tunnel: TElSubsystemSSHTunnel read GetTunnel write SetTunnel;
    property SftpBufferSize: Integer read FSftpBufferSize write SetSftpBufferSize;
    property UseUTF8 : boolean read FUseUTF8 write FUseUTF8 default true;
    property Versions : TSBSftpVersions read FVersions write FVersions
      default [sbSFTP3];
    property ExtendedProperties : TElSftpExtendedProperties read FExtendedProperties
      write FExtendedProperties;
    property UploadBlockSize: Integer read FUploadBlockSize write FUploadBlockSize;
    property DownloadBlockSize: Integer read FDownloadBlockSize write FDownloadBlockSize;

    property OnOpenConnection: TNotifyEvent read FOnOpenConnection write
    FOnOpenConnection;
    property OnCloseConnection: TNotifyEvent read FOnCloseConnection write
    FOnCloseConnection;
{$IFDEF SBB_Debug}
    property OnDebug: TSSHDebugEvent read FOnDebug write FOnDebug;
{$ENDIF}
    property OnOpenFile: TSBSftpFileOpenEvent read FOnOpenFile write
    FOnOpenFile;
    property OnError: TSBSftpErrorEvent read FOnError write FOnError;
    property OnSuccess: TSBSftpSuccessEvent read FOnSuccess write FOnSuccess;

    property OnDirectoryListing: TSBSftpDirectoryListingEvent
      read FOnDirectoryListing write FOnDirectoryListing;

    property OnFileAttributes: TSBSftpFileAttributesEvent
    read FOnFileAttributes write FOnFileAttributes;
    property OnData: TSBSftpDataEvent read FOnData write FOnData;
    property OnAbsolutePath: TSBSftpAbsolutePathEvent read FOnAbsolutePath
      write FOnAbsolutePath;
    {JPM Modifications for SFTP Extensions}
    property OnExtendedReply : TSBSftpDataEvent read FOnExtendedReply write FOnExtendedReply;
    {end}
    property OnVersionSelect : TSBSftpVersionSelectEvent read FOnVersionSelect write FOnVersionSelect;
    property OnFileHash : TSBSftpFileHashEvent read FOnFileHash write FOnFileHash;
    property OnAvailableSpace : TSBSftpAvailableSpaceEvent read FOnAvailableSpace
      write FOnAvailableSpace;
    property OnName : TSBSftpNameEvent read FOnName write FOnName;
  end;

procedure Register;

const
  SFTP_OPTIMAL_UPLOAD_BLOCK_SIZE    = $8000 - 256;
  SFTP_OPTIMAL_DOWNLOAD_BLOCK_SIZE  = $200000 - 256;//$200000 - 256;
  SFTP_MAX_DOWNLOAD_BLOCK_SIZE      = $800000 - 256;//$200000 - 256;

implementation

uses
  SBSSHUtils;

(*type
  
  {$ifdef DELPHI_NET}
  PByteArray = ByteArray;
  TSBSftpRequestInfo = class
  public
    Id: cardinal;
    Request: cardinal;
  end;
  PSBSftpRequestInfo = TSBSftpRequestInfo;
  {$else}
  TSBSftpRequestInfo = record
    Id: cardinal;
    Request: cardinal;
  end;
  PSBSftpRequestInfo = ^TSBSftpRequestInfo;
  {$endif}*)

resourcestring
  SErrorUnsupportedVersion = 'Unsupported Protocol Version';
  SErrorInvalidPacket = 'Invalid Packet';
  SErrorTunnelError = 'Tunnel Error';
  SErrorConnectionClosed = 'Connection closed by lower level protocol';
  SErrorInvalidRequestID = 'Invalid request id';  
  SNotInSyncMode = 'SFTP Client is not in synchronous mode';
  SNotConnected = 'SFTP component not connected';
  SUnsupportedOperation = 'Unsupported operation';
  SUnsupportedExtension = 'Unsupported extension';

procedure Register;
begin
  RegisterComponents('SFTPBlackbox', [TElSftpClient]);
end;

constructor TElSFTPClient.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FSftpBufferSize := BUFFER_SIZE;
  SetLength(FOutBuffer, FSftpBufferSize);
  SetLength(FInBuffer, FSftpBufferSize);
  FRequests := TList.Create;
  FDataRequests := TList.Create;
  FCurrentID := 0;
  FMessageLoop := IntMessageLoop;
  FSFTPExt := TList.Create;

  FVersions := [sbSFTP2, sbSFTP3,sbSFTP4];

  FActive := false;
  FUseUTF8 := true;
  FExtendedProperties := TElSftpExtendedProperties.Create;
  FSelectedVersion := sbSFTP0;
end;


destructor TElSFTPClient.Destroy;
var
  Index : integer;
  P: PSBSftpRequestInfo;
  DP: PSBSftpDataRequestInfo;
begin
  while FRequests.Count > 0 do
  begin
    P := FRequests.Items[0];
    Dispose(P);
    FRequests.Delete(0);
  end;
  FRequests.Free;

  while FDataRequests.Count > 0 do
  begin
    DP := FDataRequests.Items[0];
    Dispose(DP);
    FDataRequests.Delete(0);
  end;
  FDataRequests.Free;


  for Index := 0 to FSFTPExt.Count - 1 do
     TSBSFTPExtendedAttribute(FSFTPExt[Index]).Free;
  FreeAndNil(FSFTPExt);

  FreeAndNil(FExtendedProperties);
  inherited;
end;


procedure TElSftpClient.OpenInternal;
begin
  SendFxpInit;
end;

function TElSftpClient.Open : boolean;
begin
  if SynchronousMode then
  begin
    if not Active then
    begin
      DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
      Result := false;
      Exit;
    end;
    OpenInternal;
    FSyncRequest := soVersion;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;      
{$IFNDEF LINUX}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := (FLastSyncError = SSH_ERROR_OK) or (FLastSyncError = SSH_ERROR_EOF);
  end
  else
  begin
    DoError(SSH_ERROR_WRONG_MODE, SNotInSyncMode);
    Result := false;
  end;
end;

procedure TElSftpClient.OpenSync;
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not Open then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.ReadDirectory(const Handle: TSBSftpFileHandle): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Result := true;
  SendReadDir(Handle);

  if SynchronousMode then
  begin
    FSyncRequest := soReadDirectory;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := (FLastSyncError = SSH_ERROR_OK) or (FLastSyncError = SSH_ERROR_EOF);
  end;
end;

function TElSftpClient.ReadDirectorySync(const Handle: TSBSftpFileHandle): Boolean;
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, sNotInSyncMode);

  FLastSyncError := 0;

  if ReadDirectory(Handle) then
    Result := (FLastSyncError = SSH_ERROR_OK)
  else
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.CreateFile(const Filename: string): Boolean;
var
  Modes: TSBSftpFileOpenModes;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Result := true;
  Modes := [SBSftpCommon.fmCreate, SBSftpCommon.fmWrite];
  SendOpenFile(Filename, Modes, nil, []);
  if SynchronousMode then
  begin
    FSyncRequest := soCreateFile;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

function TElSftpClient.CreateFileSync(const Filename: string): TSBSftpFileHandle;
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;
    
  if CreateFile(FileName) then
    Result := FLastSyncHandle
  else
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.OpenFile(const Filename: string; Modes: TSBSftpFileOpenModes;
  Attributes: TElSftpFileAttributes): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Result := true;
  SendOpenFile(Filename, Modes, Attributes, []);
  if SynchronousMode then
  begin
    FSyncRequest := soOpenFile;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

function TElSftpClient.OpenFileSync(const Filename: string; Modes: TSBSftpFileOpenModes;
  Attributes: TElSftpFileAttributes): TSBSFTPFileHandle;
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if OpenFile(FileName, Modes, Attributes) then
    Result := FLastSyncHandle
  else
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.OpenFile(const Filename: string; Modes: TSBSftpFileOpenModes;
  Access : TSBSftpFileOpenAccess; Attributes: TElSftpFileAttributes): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Result := true;
  SendOpenFile(Filename, Modes, Attributes, Access);
  if SynchronousMode then
  begin
    FSyncRequest := soOpenFile;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

function TElSftpClient.OpenFileSync(const Filename: string; Modes: TSBSftpFileOpenModes;
  Access : TSBSftpFileOpenAccess; Attributes: TElSftpFileAttributes): TSBSftpFileHandle;
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if OpenFile(FileName, Modes, Access, Attributes) then
    Result := FLastSyncHandle
  else
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.OpenDirectory(const Path: string): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Result := true;
  SendOpenDir(Path);
  if SynchronousMode then
  begin
    FSyncRequest := soOpenDirectory;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

function TElSftpClient.OpenDirectorySync(const Path: string): TSBSFTPFileHandle;
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if OpenDirectory(Path) then
    Result := FLastSyncHandle
  else
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.RemoveFile(const Filename: string): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Result := true;
  SendRemoveFile(Filename);
  if SynchronousMode then
  begin
    FSyncRequest := soRemoveFile;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSftpClient.RemoveFileSync(const Filename: string);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;
    
  if not RemoveFile(FileName) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.RenameFile(const OldPath, NewPath: string): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  result := true;
  SendRenameFile(OldPath, NewPath);
  if SynchronousMode then
  begin
    FSyncRequest := soRenameFile;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSftpClient.RenameFileSync(const OldPath, NewPath: string);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not RenameFile(OldPath, NewPath) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;


function TElSftpClient.MakeDirectory(const Path: string; Attributes:
  TElSftpFileAttributes): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  result := true;
  SendMkDir(Path, Attributes);
  if SynchronousMode then
  begin
    FSyncRequest := soMakeDirectory;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSftpClient.MakeDirectorySync(const Path: string; Attributes: TElSftpFileAttributes);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not MakeDirectory(Path, Attributes) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.RemoveDirectory(const Path: string): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Result := true;
  SendRmDir(Path);
  if SynchronousMode then
  begin
    FSyncRequest := soRemoveDirectory;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSftpClient.RemoveDirectorySync(const Path: string);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not RemoveDirectory(Path) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.CloseHandle(const Handle: TSBSftpFileHandle): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  result := true;
  SendClose(Handle);

  if SynchronousMode then
  begin
    FSyncRequest := soCloseHandle;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSftpClient.CloseHandleSync(const Handle: TSBSftpFileHandle);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not CloseHandle(Handle) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.RequestAttributes(const Path: string; FollowSymLinks:
  boolean): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  result := true;
  if FollowSymLinks then
    SendStat(Path)
  else
    SendLStat(Path);

  if SynchronousMode then
  begin
    FSyncRequest := soRequestAttributes;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSftpClient.RequestAttributesSync(const Path: string; FollowSymLinks: boolean);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not RequestAttributes(Path, FollowSymLinks) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.RequestAttributes(const Handle: TSBSftpFileHandle): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  result := true;
  SendFStat(Handle);
  if SynchronousMode then
  begin
    FSyncRequest := soRequestAttributes;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSftpClient.RequestAttributesSync(const Handle: TSBSftpFileHandle);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;
    
  if not RequestAttributes(Handle) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.SetAttributes(const Path: string; Attributes:
  TElSftpFileAttributes): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  result := true;
  SendSetStat(Path, Attributes);
  if SynchronousMode then
  begin
    FSyncRequest := soSetAttributes;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSftpClient.SetAttributesSync(const Path: string; Attributes: TElSftpFileAttributes);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not SetAttributes(Path, Attributes) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;


function TElSftpClient.SetAttributesByHandle(const Handle: TSBSftpFileHandle; Attributes: TElSftpFileAttributes):Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  result := true;
  SendFSetStat(Handle, Attributes);
  
  if SynchronousMode then
  begin
    FSyncRequest := soSetAttributes;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;


procedure TElSftpClient.SetAttributesByHandleSync(const Handle: TSBSftpFileHandle; Attributes: TElSftpFileAttributes);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not SetAttributesByHandle(Handle, Attributes) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.ReadSymLink(const Path: string): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  result := true;
  SendReadlink(Path);
  if SynchronousMode then
  begin
    FSyncRequest := soReadSymLink;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSftpClient.ReadSymLinkSync(const Path: string);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, sNotInSyncMode);

  FLastSyncError := 0;

  if not ReadSymLink(Path) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.CreateSymLink(const LinkPath: string; const TargetPath: string): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Result := true;

  if FSFTPVersion > 5 then
  begin
    SendLink(LinkPath, TargetPath, true);
  end
  else
  begin
    SendSymlink(LinkPath, TargetPath);
  end;
  if SynchronousMode then
  begin
    FSyncRequest := soCreateSymLink;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSftpClient.CreateSymLinkSync(const LinkPath: string; const TargetPath: string);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not CreateSymLink(LinkPath, TargetPath) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.CreateHardLink(const LinkPath: string; const TargetPath: string): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Result := true;

  if FSFTPVersion < 5 then
  begin
    Result := false;
    Exit;
  end
  else
  begin
    SendLink(LinkPath, TargetPath, false);
  end;

  if SynchronousMode then
  begin
    FSyncRequest := soCreateHardLink;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSftpClient.CreateHardLinkSync(const LinkPath: string; const TargetPath: string);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not CreateHardLink(LinkPath, TargetPath) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;


function TElSftpClient.RequestAbsolutePath(const Path: string): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Result := true;
  SendRealPath(Path, rcNoCheck, nil);
  if SynchronousMode then
  begin
    FSyncRequest := soRequestAbsolutePath;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

function TElSftpClient.RequestAbsolutePathSync(const Path: string): string;
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if RequestAbsolutePath(Path) then
    Result := FLastSyncText
  else
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.RequestAbsolutePathEx(const Path: string;
  Control : TSBSFTPRealpathControl; ComposePath : TStringList): Boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  result := true;
  SendRealPath(Path, Control, ComposePath);
  if SynchronousMode then
  begin
    FSyncRequest := soRequestAbsolutePath;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

function TElSftpClient.RequestAbsolutePathExSync(const Path: string;
  Control : TSBSFTPRealpathControl; ComposePath : TStringList): string;
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if RequestAbsolutePathEx(Path, Control, ComposePath) then
    Result := FLastSyncText
  else
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSFTPClient.Block(const Handle: TSBSftpFileHandle; Offset, Length : Int64;
  LockMask : TSBSftpFileOpenAccess) : boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Result := true;

  if FSFTPVersion < 6 then
  begin
    Result := false;
    Exit;
  end
  else
    SendBlock(Handle, Offset, Length, LockMask);

  if SynchronousMode then
  begin
    FSyncRequest := soBlock;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSFTPClient.BlockSync(const Handle: TSBSftpFileHandle; Offset, Length : Int64;
  LockMask : TSBSftpFileOpenAccess);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not Block(Handle, Offset, Length, LockMask) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSFTPClient.Unblock(const Handle: TSBSftpFileHandle; Offset, Length : Int64) : boolean;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Result := true;

  if FSFTPVersion < 6 then
  begin
    Result := false;
    Exit;
  end
  else
    SendUnblock(Handle, Offset, Length);

  if SynchronousMode then
  begin
    FSyncRequest := soUnblock;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;
end;

procedure TElSFTPClient.UnblockSync(const Handle: TSBSftpFileHandle; Offset, Length : Int64);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not Unblock(Handle, Offset, Length) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;


function TElSFTPClient.RequestFileHash(const FileName : string; StartOffset : Int64;
  Length : Int64; BlockSize : cardinal) : boolean;
var
  CheckFileExt : TElSftpCheckFileExtension;
  ExtData : BufferType;
begin
  CheckFileExt := TElSftpCheckFileExtension.Create;
  CheckFileExt.Name := FileName;
  CheckFileExt.StartOffset := StartOffset;
  CheckFileExt.DataLength := Length;
  CheckFileExt.BlockSize := BlockSize;
  CheckFileExt.SaveToBuffer;
  ExtData := CheckFileExt.ExtData;
  CheckFileExt.Free;

  Result := ExtensionCmd(SSH_EXT_CHECK_FILE_NAME, @ExtData[1], System.Length(ExtData));
end;

procedure TElSFTPClient.RequestFileHashSync(const FileName : string;
  StartOffset : Int64; Length : Int64; BlockSize : cardinal);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not RequestFileHash(FileName, StartOffset, Length, BlockSize) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSFTPClient.RequestFileHashByHandle(const Handle : BufferType;
  StartOffset : Int64; Length : Int64; BlockSize : cardinal) : boolean;
var
  CheckFileExt : TElSftpCheckFileExtension;
  ExtData : BufferType;
begin
  CheckFileExt := TElSftpCheckFileExtension.Create;
  CheckFileExt.Handle := Handle;
  CheckFileExt.StartOffset := StartOffset;
  CheckFileExt.DataLength := Length;
  CheckFileExt.BlockSize := BlockSize;
  CheckFileExt.SaveToBuffer;
  ExtData := CheckFileExt.ExtData;
  CheckFileExt.Free;
  Result := ExtensionCmd(SSH_EXT_CHECK_FILE_HANDLE, @ExtData[1], System.Length(ExtData));
end;

procedure TElSFTPClient.RequestFileHashByHandleSync(const Handle : BufferType; StartOffset : Int64;
  Length : Int64; BlockSize : cardinal);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not RequestFileHashByHandle(Handle, StartOffset, Length, BlockSize) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSFTPClient.QueryAvailableSpace(const Path : string) : boolean;
var
  SpaceAvailExt : TElSftpSpaceAvailableExtension;
  ExtData : BufferType;
begin
  SpaceAvailExt := TElSftpSpaceAvailableExtension.Create;
  SpaceAvailExt.Path := Path;
  SpaceAvailExt.SaveToBuffer;
  ExtData := SpaceAvailExt.ExtData;
  SpaceAvailExt.Free;

  Result := ExtensionCmd(SSH_EXT_SPACE_AVAILABLE, @ExtData[1], Length(ExtData));
end;

procedure TElSFTPClient.QueryAvailableSpaceSync(const Path : string);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not QueryAvailableSpace(Path) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSFTPClient.QueryHomeDirectory(const Username : string) : boolean;
var
  HomeDirExt : TElSftpHomeDirectoryExtension;
  ExtData : BufferType;
begin
  HomeDirExt := TElSftpHomeDirectoryExtension.Create;
  HomeDirExt.Username := Username;
  HomeDirExt.SaveToBuffer;
  ExtData := HomeDirExt.ExtData;
  HomeDirExt.Free;

  Result := ExtensionCmd(SSH_EXT_HOME_DIRECTORY, @ExtData[1], Length(ExtData));
end;

procedure TElSFTPClient.QueryHomeDirectorySync(const Username : string);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not QueryHomeDirectory(Username) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSFTPClient.CopyRemoteFile(const Source, Destination : string;
  Overwrite : boolean) : boolean;
var
  CopyFileExt : TElSftpCopyFileExtension;
  ExtData : BufferType;
begin
  CopyFileExt := TElSftpCopyFileExtension.Create;
  CopyFileExt.Source := Source;
  CopyFileExt.Destination := Destination;
  CopyFileExt.Overwrite := Overwrite;
  CopyFileExt.SaveToBuffer;
  ExtData := CopyFileExt.ExtData;
  CopyFileExt.Free;

  Result := ExtensionCmd(SSH_EXT_COPY_FILE, @ExtData[1], Length(ExtData));
end;

procedure TElSFTPClient.CopyRemoteFileSync(const Source, Destination : string;
  Overwrite : boolean);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not CopyRemoteFile(Source, Destination, Overwrite) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSFTPClient.CopyRemoteData(const ReadFrom : TSBSftpFileHandle;
  ReadFromOffset : Int64; const WriteTo : TSBSftpFileHandle;
  WriteToOffset, DataLength : Int64) : boolean;
var
  CopyDataExt : TElSftpCopyDataExtension;
  ExtData : BufferType;
begin
  CopyDataExt := TElSftpCopyDataExtension.Create;
  CopyDataExt.ReadHandle := ReadFrom;
  CopyDataExt.ReadOffset := ReadFromOffset;
  CopyDataExt.WriteHandle := WriteTo;
  CopyDataExt.WriteOffset := WriteToOffset;
  CopyDataExt.DataLength := DataLength;
  CopyDataExt.SaveToBuffer;
  ExtData := CopyDataExt.ExtData;
  CopyDataExt.Free;

  Result := ExtensionCmd(SSH_EXT_COPY_DATA, @ExtData[1], Length(ExtData));
end;

procedure TElSFTPClient.CopyRemoteDataSync(const ReadFrom : TSBSftpFileHandle;
  ReadFromOffset : Int64; const WriteTo : TSBSftpFileHandle;
  WriteToOffset, DataLength : Int64);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not CopyRemoteData(ReadFrom, ReadFromOffset, WriteTo, WriteToOffset, DataLength) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSFTPClient.RequestTempFolder : boolean;
begin
  Result := ExtensionCmd(SSH_EXT_GET_TEMP_FOLDER, nil, 0);
end;

procedure TElSFTPClient.RequestTempFolderSync;
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not RequestTempFolder then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSFTPClient.MakeTempFolder : boolean;
begin
  Result := ExtensionCmd(SSH_EXT_MAKE_TEMP_FOLDER, nil, 0);
end;

procedure TElSFTPClient.MakeTempFolderSync;
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not MakeTempFolder then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.Read(const Handle: TSBSftpFileHandle; Offset: Int64; Buffer : Pointer; Size:
    integer): Boolean;
var
  ToReceive,
  Received : Integer;
  CurrentID : Cardinal;
  P: PSBSftpDataRequestInfo;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  result := true;

  FDataBufferPtr := Buffer;
  FDataBufferPtrSize := Size;
  FDataBufferPtrFilled := 0;

  Received := 0;

  try
    while Received < Size do
    begin
      if DownloadBlockSize = 0 then
        ToReceive := Min(SFTP_OPTIMAL_DOWNLOAD_BLOCK_SIZE, Size - Received)
      else
        ToReceive := Min(DownloadBlockSize, Size - Received);

      SendRead(Handle, Offset + Received, ToReceive, CurrentID);

      New(P);
      P.ID := CurrentID;
      P.DataID := FCurrentDataID;
      P.Offset := Received;
      P.Size := ToReceive;
      PushDataRequest(P);
      Inc(Received, ToReceive);
    end;
  finally
    Inc(FCurrentDataID);
  end;

  if SynchronousMode then
  begin
    FSyncRequest := soRead;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := (FLastSyncError = 0) or (FLastSyncError = SSH_ERROR_EOF);
  end;
end;

function TElSftpClient.Read(const Handle: TSBSftpFileHandle; Offset: Int64; Size:
    integer): Boolean;
begin
  Result := Read(Handle, Offset, nil, Size);
end;

procedure TElSftpClient.ReadSync(const Handle: TSBSftpFileHandle; Offset: Int64;
    Buffer : Pointer; Size: integer);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not Read(Handle, Offset, Buffer, Size) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

procedure TElSftpClient.ReadSync(const Handle: TSBSftpFileHandle; Offset: Int64;
    Size: integer);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not Read(Handle, Offset, nil, Size) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.Write(const Handle: TSBSftpFileHandle; Offset: Int64; Buffer: pointer; Size: cardinal): Boolean;
var
  ToSend,
  Sent : Cardinal;
  CurrentID : Cardinal;
  P: PSBSftpDataRequestInfo;

begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;


  Sent := 0;

  try
    while Sent < Size do
    begin
      if UploadBlockSize = 0 then
        ToSend := Min(SFTP_OPTIMAL_UPLOAD_BLOCK_SIZE, Size - Sent)
      else
        ToSend := Min(UploadBlockSize, Size - Sent);

      SendWrite(Handle, Offset + Sent, Buffer, ToSend, CurrentID);

      New(P);
      P.ID := CurrentID;
      P.DataID := FCurrentDataID;
      P.Offset := Sent;
      P.Size := ToSend;
      PushDataRequest(P);
      Inc(Sent, ToSend);
    end;
  finally
    Inc(FCurrentDataID);
  end;

  result := true;

  if SynchronousMode then
  begin
    FSyncRequest := soWrite;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
      {$IFNDEF CLX_USED}
      Sleep(0);
      {$ELSE}
      Usleep(0);
      {$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := FLastSyncError = 0;
  end;

end;

procedure TElSftpClient.WriteSync(const Handle: TSBSftpFileHandle; Offset: Int64; Buffer: pointer; Size: cardinal);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not Write(Handle, Offset, Buffer, Size) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

{JPM - modifcation for extended commands}
function TElSftpClient.ExtensionCmd(const Extension : String;
  DataBuffer: pointer; Size: integer) : Boolean;
begin
  Result := false;

  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Exit;
  end;

  if FSFTPVersion < 3 then
    raise EElSFTPError.CreateCode(SSH_ERROR_OP_UNSUPPORTED, SUnsupportedOperation);

  Result := true;
  SendExtended(Extension, DataBuffer, Size);
  if SynchronousMode then
  begin
    FSyncRequest := soExtension;
    FLastSyncError := 0;
    while FSyncRequestComplete <> FSyncRequest do
    begin
      if not MessageLoop() then break;
{$IFNDEF CLX_USED}
      Sleep(0);
{$ELSE}
      Usleep(0);
{$ENDIF}
    end;
    FSyncRequest := soNone;
    FSyncRequestComplete := soNone;
    Result := (FLastSyncError = 0) or (FLastSyncError = SSH_ERROR_EOF);
  end;
end;

function TElSftpClient.ExtensionCmd(const Extension : String) : Boolean;
begin
  Result := ExtensionCmd(Extension, nil, 0);
end;
{end}

procedure TElSftpClient.TextSeekSync(const Handle : TSBSftpFileHandle; LineNumber: Int64);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not TextSeek(Handle, LineNumber) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

function TElSftpClient.TextSeek(const Handle : TSBSftpFileHandle; LineNumber: Int64) : Boolean;
var
  Msg : string;
begin
  if not Active then
  begin
    DoError(SSH_ERROR_NO_CONNECTION, SNotConnected);
    Result := false;
    Exit;
  end;

  Msg := WriteString(Handle) + WriteUINT64(LineNumber);
  result := ExtensionCmd('text-seek', @Msg[1], Length(Msg));
end;

function TElSFTPClient.SendVendorID(VendorID : TElSFTPVendorIDExtension) : boolean;
begin
  VendorID.SaveToBuffer;
  Result := ExtensionCmd(VendorID.ExtType, @VendorID.ExtData[1], Length(VendorID.ExtData));
end;

procedure TElSFTPClient.SendVendorIDSync(VendorID : TElSFTPVendorIDExtension);
begin
  if not SynchronousMode then
    raise EElSFTPError.CreateCode(SSH_ERROR_WRONG_MODE, SNotInSyncMode);

  FLastSyncError := 0;

  if not SendVendorID(VendorID) then
    raise EElSFTPError.CreateCode(FLastSyncError, FLastSyncComment);
end;

////////////////////////////////////////////////////////////////////////////////
// Protocol flow implementation

procedure TElSFTPClient.AnalizeBuffer;
var
  Len: integer;
begin
  while FInBufferIndex >= 4 do
  begin
    Len := (FInBuffer[0] shl 24) or
      (FInBuffer[1] shl 16) or
      (FInBuffer[2] shl 8) or
      (FInBuffer[3] and $FF);

//    {$ifndef CHROME}
    if Len > Length(FInBuffer) then
//    {$else}
//    if Len > FInBuffer.Length then
//    {$endif}
    begin
      if Len < SFTP_MAX_DOWNLOAD_BLOCK_SIZE then
      begin
        if Length(FInBuffer) < Len then
          SetLength(FInBuffer, Len);
      end
      else
      begin
        DoError(SSH_ERROR_INVALID_PACKET, SErrorInvalidPacket);
        DoCloseConnection;
        FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
        Exit;
      end;
    end;
    if FInBufferIndex - 4 >= Len then
    begin
      ParseOnTransportLayer(@FInBuffer[4], Len);
      Move(FInBuffer[Len + 4], FInBuffer[0], FInBufferIndex -
        Len - 4);
      Dec(FInBufferIndex, Len + 4);
    end
    else
      Break;
  end;
end;

procedure TElSftpClient.CompleteDownloadRequest(DataID: Cardinal);
var DataReqInfo : PSBSftpDataRequestInfo;
begin
  // cancel all other requests
  repeat
    PopDataRequest2(DataId, DataReqInfo);
    if DataReqInfo <> nil then
      Dispose(DataReqInfo);
  until DataReqInfo = nil;

  if FDataBufferPtr = nil then
  begin
    if Assigned(FOnData) then
      FOnData(Self, @FDataBuffer[0], Length(FDataBuffer));
    SetLength(FDataBuffer, 0);
  end
  else
  begin
    if Assigned(FOnData) then
      FOnData(Self, FDataBufferPtr, FDataBufferPtrFilled);
    FDataBufferPtrFilled := 0;
  end;
end;

procedure TElSFTPClient.ParseOnTransportLayer(P: pointer; Size: integer);
var
  PacketType: byte;
  VersionExt : TElSftpVersionSelectExtension;
begin
  PacketType := PByteArray(P)[0];
  case PacketType of
    SSH_FXP_VERSION:
      begin
        ParseVersion(@PByteArray(P)[1], Size - 1);

        if FSelectedVersion <> sbSFTP0 then
        begin
          VersionExt := TElSftpVersionSelectExtension.Create;
          try
            VersionExt.Version := FSelectedVersion;
            VersionExt.SaveToBuffer;
            if ExtensionCmd((VersionExt.ExtType), @VersionExt.ExtData[1], Length(VersionExt.ExtData)) then
            begin
              FVersion := VersionExt.Version;
              FSFTPVersion := VersionExt.VersionInt;
            end;
          finally
            VersionExt.Free;
          end;
        end;
      end;
    SSH_FXP_STATUS:
      ParseStatus(@PByteArray(P)[1], Size - 1);
    SSH_FXP_HANDLE:
      ParseHandle(@PByteArray(P)[1], Size - 1);
    SSH_FXP_DATA:
      ParseData(@PByteArray(P)[1], Size - 1);
    SSH_FXP_NAME:
      ParseName(@PByteArray(P)[1], Size - 1);
    SSH_FXP_ATTRS:
      ParseAttrs(@PByteArray(P)[1], Size - 1);
    {JPM Modifications for Extensions}
    SSH_FXP_EXTENDED_REPLY :
      ParseExtendedReply(@PByteArray(P)[1], Size - 1);
  end;
end;

procedure TElSftpClient.ParseVersion(P: pointer; Size: integer);
var
  NewlineExt : TElSFTPNewlineExtension;
  SupportedExt : TElSFTPSupportedExtension;
  VersionsExt : TElSFTPVersionsExtension;
  FCharsetExt : TElSftpFilenameCharsetExtension;
  FNTranslationExt : TElSftpFilenameTranslationExtension;
  VendorIDExt : TElSFTPVendorIDExtension;
  CurrP: ^byte;
  {JPM Modifications}
  LExtName : string;
  LExtData : string;
  {end modifications}
  { JPM Modifications for SSH }
  procedure SSHVerError;
  begin
    DoError(SSH_ERROR_UNSUPPORTED_VERSION, SErrorUnsupportedVersion);
    DoCloseConnection;
    FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
  end;
  {end}
begin
  try
    FSFTPVersion := ReadLength(P, Size);
    case FSFTPVersion of
     0 : if (sbSFTP1 in FVersions) then
         begin
           FVersion := sbSFTP2;
         end
         else
         begin
           SSHVerError;
           Exit;
         end;
     1 : if (sbSFTP1 in FVersions) then
         begin
           FVersion := sbSFTP2;
         end
         else
         begin
           SSHVerError;
           Exit;
         end;
     2 : if (sbSFTP2 in FVersions) then
         begin
           FVersion := sbSFTP2;
         end
         else
         begin
           SSHVerError;
           Exit;
         end;
     3 : if (sbSFTP3 in FVersions) then
         begin
           FVersion := sbSFTP3;
         end
         else
         begin
           SSHVerError;
           Exit;
         end;
     4 : if (sbSFTP4 in FVersions) then
         begin
           FVersion := sbSFTP4;
         end
         else
         begin
           SSHVerError;
           Exit;
         end;

     5 : if (sbSFTP5 in FVersions) then
         begin
           FVersion := sbSFTP5;
         end
         else
         begin
           SSHVerError;
           Exit;
         end;
     6 : if (sbSFTP6 in FVersions) then
         begin
           FVersion := sbSFTP6;
         end
         else
         begin
           SSHVerError;
           Exit;
         end;
    else
      DoError(SSH_ERROR_UNSUPPORTED_VERSION, SErrorUnsupportedVersion);
      DoCloseConnection;
      FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
      Exit;
    end;
    {end}
    {JPM Modifications for SFTP Extensions}
    CurrP := P;
    Inc(CurrP,4);
    Dec(Size,4);
    while Size > 4 do
    begin
      //get the extension name
      LExtName := ReadString(CurrP, Size);
      Dec(Size, 4 + Length(LExtName));
      Inc(CurrP, 4 + Length(LExtName));
      //get extension data
      LExtData := ReadString(CurrP, Size);
      Dec(Size, 4 + Length(LExtData));
      Inc(CurrP, 4 + Length(LExtData));

      if ((LExtName) = SSH_EXT_NEWLINE) or
        ((LExtName) = SSH_EXT_NEWLINE_VANDYKE) then
      begin
        NewlineExt := TElSFTPNewlineExtension.Create;
        NewlineExt.ExtType := LExtName;
        NewlineExt.ExtData := LExtData;

        try
          if NewlineExt.LoadFromBuffer then
          begin
            FExtendedProperties.NewlineExtension := NewlineExt;
            FExtendedProperties.NewlineAvailable := true;
            FNewLineConvention := NewlineExt.Newline;
            FSFTPExt.Add(NewlineExt);
          end
          else
            FreeAndNil(NewlineExt);
        except
          FreeAndNil(NewlineExt);
        end;
      end
      else
      if ((LExtName) = SSH_EXT_SUPPORTED2) or
        ((LExtName) = SSH_EXT_SUPPORTED) then
      begin
        SupportedExt := TElSFTPSupportedExtension.Create;
        SupportedExt.ExtType := LExtName;
        SupportedExt.ExtData := LExtData;

        try
          if SupportedExt.LoadFromBuffer then
          begin
            FExtendedProperties.SupportedExtension := SupportedExt;
            FExtendedProperties.SupportedAvailable := true;
            FSFTPExt.Add(SupportedExt);
          end
          else
            FreeAndNil(SupportedExt);
        except
          FreeAndNil(SupportedExt);
        end;
      end
      else
      if ((LExtName) = SSH_EXT_VERSIONS) then
      begin
        VersionsExt := TElSFTPVersionsExtension.Create;
        VersionsExt.ExtType := LExtName;
        VersionsExt.ExtData := LExtData;

        try
          if VersionsExt.LoadFromBuffer then
          begin
            FExtendedProperties.VersionsExtension := VersionsExt;
            FExtendedProperties.VersionsAvailable := true;
            FSFTPExt.Add(VersionsExt);
          end
          else
            FreeAndNil(VersionsExt);
        except
          FreeAndNil(VersionsExt);
        end;

        if (FExtendedProperties.VersionsAvailable) and Assigned(FOnVersionSelect) then
        begin
          FSelectedVersion := sbSFTP0;

          FOnVersionSelect(Self, VersionsExt.Versions, FSelectedVersion);

          if not (FSelectedVersion in FVersions) then
            FSelectedVersion := sbSFTP0;
        end;
      end
      else
      if ((LExtName) = SSH_EXT_FILENAME_CHARSET) then
      begin
        FCharsetExt := TElSftpFilenameCharsetExtension.Create;
        FCharsetExt.ExtType := LExtName;
        FCharsetExt.ExtData := LExtData;

        try
          if FCharsetExt.LoadFromBuffer then
          begin
            FExtendedProperties.FilenameCharset := FCharsetExt.Charset;

            if FSFTPVersion > 5 then
            begin
              FNTranslationExt := TElSftpFilenameTranslationExtension.Create;
              FNTranslationExt.DoTranslate := false;

              FNTranslationExt.SaveToBuffer;

              try
                ExtensionCmd((FNTranslationExt.ExtType), @FNTranslationExt.ExtData[1], Length(FNTranslationExt.ExtData));
              finally
                FNTranslationExt.Free;
              end;
            end;
          end;
          FCharsetExt.Free;

        finally
          FCharsetExt.Free;
        end;
      end
      else
      if ((LExtName) = SSH_EXT_VENDOR_ID) then
      begin
        VendorIDExt := TElSftpVendorIDExtension.Create;
        VendorIDExt.ExtType := LExtName;
        VendorIDExt.ExtData := LExtData;

        try
          if VendorIDExt.LoadFromBuffer then
          begin
            FExtendedProperties.VendorIDExtension := VendorIDExt;
            FExtendedProperties.VendorIDAvailable := true;
            FSFTPExt.Add(VendorIDExt);
          end
          else
            FreeAndNil(VendorIDExt);

        except
          FreeAndNil(VendorIDExt);
        end;
      end;

    end;
  except
    DoError(SSH_ERROR_INVALID_PACKET, SErrorInvalidPacket);
    DoCloseConnection;
    FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
    Exit;
  end;
  if (FSFTPVersion > 1) and (FSFTPVersion < 7) then
    DoOpenConnection
  else
  begin
    DoError(SSH_ERROR_UNSUPPORTED_VERSION, SErrorUnsupportedVersion);
    DoCloseConnection;
  end;
end;

procedure TElSftpClient.ParseStatus(P: pointer; Size: integer);
var
  DataId,
  Id: cardinal;
  Request : integer;
  StatusCode: cardinal;
  ErrorMessage
  //, Lang
  : string;
  Info: PSBSftpRequestInfo;

  DataReqInfo : PSBSftpDataRequestInfo;

begin
  try
    Id := ReadLength(P, Size);
    StatusCode := ReadLength(@PByteArray(P)[4], Size - 4);
    if Size > 8 then
      ErrorMessage := ReadString(@PByteArray(P)[8], Size - 8);
    //if Size > 12 + Length(ErrorMessage) then
    //Lang := ReadString(@PByteArray(P)[12 + Length(ErrorMessage)], Size - 12 -
    //  Length(ErrorMessage));
  except
    DoError(SSH_ERROR_INVALID_PACKET, SErrorInvalidPacket);
    DoCloseConnection;
    FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
    Exit;
  end;
  PopRequest(Id, Info);

  if Info = nil then
    exit;

  Request := Info.Request;

  Dispose(PSBSftpRequestInfo(Info));


  // special case for upload operation
  if Request = SSH_FXP_WRITE then
  begin
    PopDataRequest(ID, DataReqInfo);
    if DataReqInfo <> nil then
    begin
      if StatusCode <> SSH_FX_OK then
      begin
        DataId := DataReqInfo.DataID;

        Dispose(DataReqInfo);

        // cancel all other requests
        repeat
          PopDataRequest2(DataId, DataReqInfo);
          if DataReqInfo <> nil then
            Dispose(DataReqInfo);
        until DataReqInfo = nil;
      end
      else
      begin
        // block completed
        if HasPendingDataRequests(DataReqInfo.DataID) then
          exit;
      end;
    end
    else
      exit;
  end;

  // special case for download operation
  if Request = SSH_FXP_READ then
  begin
    PopDataRequest(ID, DataReqInfo);
    if DataReqInfo <> nil then
    begin
      DataId := DataReqInfo.DataID;
      Dispose(DataReqInfo);
      CompleteDownloadRequest(DataID);
    end
    else
      exit;
  end;

  if StatusCode <> SSH_FX_OK then
    DoError(SSH_ERROR_BASE + integer(StatusCode), ErrorMessage)
  else
    DoSuccess(ErrorMessage);
end;

procedure TElSftpClient.ParseHandle(P: pointer; Size: integer);
var
  Id: cardinal;
  Hndl: string;
  Info: PSBSftpRequestInfo;
begin
  try
    Id := ReadLength(P, Size);
    Hndl := ReadString(@PByteArray(P)[4], Size - 4);
  except
    DoError(SSH_ERROR_INVALID_PACKET, SErrorInvalidPacket);
    DoCloseConnection;
    FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
    Exit;
  end;
  PopRequest(Id, Info);
  Assert(Assigned(Info));
  Dispose(PSBSftpRequestInfo(Info));
  DoOpenFile(Hndl);
end;

procedure TElSftpClient.ParseName(P: pointer; Size: integer);
var
  Id: cardinal;
  Cnt: cardinal;
  CurrP: ^byte;
  I, Tmp: integer;
  Lst: array of TElSftpFileInfo;
  {$ifdef BUILDER_USED}
  TLst : TList;
  {$endif}
  Req: PSBSftpRequestInfo;
begin
  CurrP := P;
  Cnt := 0;
  try
    Id := ReadLength(P, Size);
    Cnt := ReadLength(@PByteArray(P)[4], Size - 4);
    Dec(Size, 8);
    SetLength(Lst, Cnt);
    for I := 0 to Cnt - 1 do
    begin
      Lst[I] := TElSftpFileInfo.Create;
    end;
    Inc(CurrP, 8);
    for I := 0 to Cnt - 1 do
    begin
      Lst[I].Name := ReadString(CurrP, Size);
      Dec(Size, Length(Lst[I].Name) + 4);
      Inc(CurrP, Length(Lst[I].Name) + 4);

      if FSftpVersion < 4 then
      begin
        Lst[I].LongName := ReadString(CurrP, Size);
        Dec(Size, Length(Lst[I].LongName) + 4);
        Inc(CurrP, Length(Lst[I].LongName) + 4);
      end;
      
      Tmp := ParseFileAttributes(CurrP, Size, Lst[I].Attributes);
      Dec(Size, Tmp);
      Inc(CurrP, Tmp);
    end;
  except
    DoError(SSH_ERROR_INVALID_PACKET, SErrorInvalidPacket);
    DoCloseConnection;
    FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
    for I := 0 to Cnt - 1 do
      Lst[I].Free;
    Exit;
  end;
  PopRequest(Id, Req);
  if (Req.Request = SSH_FXP_READDIR) and
    Assigned(FOnDirectoryListing) then
    begin
{$ifndef BUILDER_USED}
      FOnDirectoryListing(Self, Lst)
{$else}
      TLst := TList.Create;
      try
        for i := 0 to Length(Lst) -1 do
          TLst.Add(Lst[i]);
        FOnDirectoryListing(Self, TLst);
      finally
        FreeAndNil(TLst);
      end;
{$endif}
    end
  else
  if (Req.Request = SSH_FXP_REALPATH)
  then
  begin
    if Assigned(FOnAbsolutePath) then
      FOnAbsolutePath(Self, Lst[0].Name);
    FLastSyncText := Lst[0].Name;
  end
  else
  if (Req.Request = SSH_FXP_EXTENDED)
  then
  begin
    if (Length(Lst) > 1) and Assigned(FOnDirectoryListing) then
    begin
{$ifndef BUILDER_USED}
      FOnDirectoryListing(Self, Lst)
{$else}
      TLst := TList.Create;
      try
        for i := 0 to Length(Lst) -1 do
          TLst.Add(Lst[i]);
        FOnDirectoryListing(Self, TLst);
      finally
        FreeAndNil(TLst);
      end;
{$endif}
    end
    else
    if (Length(Lst) = 1) and Assigned(FOnName) then
      FOnName(Self, Lst[0]);
  end
  else
    if (Req.Request = SSH_FXP_READLINK) and
    Assigned(FOnDirectoryListing) then
    begin
{$ifndef BUILDER_USED}
      FOnDirectoryListing(Self, Lst)
{$else}
      TLst := TList.Create;
      try
        for i := 0 to Length(Lst) -1 do
          TLst.Add(Lst[i]);
        FOnDirectoryListing(Self, TLst);
      finally
        FreeAndNil(TLst);
      end;
{$endif}
    end;
  if Assigned(Req) then 
    Dispose(Req);
  
  for I := 0 to Cnt - 1 do
    Lst[I].Free;
  FSyncRequestComplete := FSyncRequest;
end;

procedure TElSftpClient.ParseAttrs(P: pointer; Size: integer);
var
  Id: cardinal;
  Attrs: TElSftpFileAttributes;
  Req: PSBSftpRequestInfo;
begin
  Attrs := nil;
  try
    Attrs := TElSftpFileAttributes.Create;
    Id := ReadLength(P, Size);
    ParseFileAttributes(@PByteArray(P)[4], Size - 4, Attrs);
  except
    DoError(SSH_ERROR_INVALID_PACKET, SErrorInvalidPacket);
    DoCloseConnection;
    FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
    if Attrs <> nil then
      Attrs.Free;
    Exit;
  end;
  PopRequest(Id, Req);
  Dispose(Req);
  if Assigned(FOnFileAttributes) then
    FOnFileAttributes(Self, Attrs);

  Attrs.Free;
  FSyncRequestComplete := FSyncRequest;
end;

{JPM Addition for SFTP Extension}
function TElSftpClient.ParseExtendedReply(P: pointer; Size: integer): integer;
var
  Id: cardinal;
  Req: PSBSftpRequestInfo;
  ExtType : string;
  ExtData : BufferType;
  CheckFileReply : TElSftpCheckFileReply;
  SpaceAvailReply : TElSftpSpaceAvailableReply;
  Cont : boolean;
begin
  Result := 0;
  try
    Id := ReadLength(P, Size);
  except
    DoError(SSH_ERROR_INVALID_PACKET, SErrorInvalidPacket);
    DoCloseConnection;
    FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
    Exit;
  end;
  
  PopRequest(Id, Req);
  if not Assigned(Req) then
    raise EElSFTPError.CreateCode(SSH_ERROR_BAD_MESSAGE, SErrorInvalidRequestID);

  ExtType := Req.ExtType;
  SetLength(ExtData, Size - 4);
  Move(PByteArray(P)^[4], ExtData[1], Size - 4);

  Dispose(Req);

  Cont := true;

  if (ExtType = SSH_EXT_CHECK_FILE_NAME) or (ExtType = SSH_EXT_CHECK_FILE_HANDLE) and
    Assigned(FOnFileHash) then
  begin
    CheckFileReply := TElSftpCheckFileReply.Create;
    CheckFileReply.ReplyData := ExtData;
    try
      if CheckFileReply.LoadFromBuffer then
      begin
        Cont := false;
        FOnFileHash(Self, CheckFileReply, Cont)
      end
      else
      begin
        DoError(SSH_ERROR_INVALID_PACKET, SErrorInvalidPacket);
        DoCloseConnection;
        FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
      end;
    finally
      FreeAndNil(CheckFileReply);
    end;
  end;

  if (ExtType = SSH_EXT_SPACE_AVAILABLE) and Assigned(FOnAvailableSpace) then
  begin
    SpaceAvailReply := TElSftpSpaceAvailableReply.Create;
    SpaceAvailReply.ReplyData := ExtData;
    try
      if SpaceAvailReply.LoadFromBuffer then
      begin
        Cont := false;
        FOnAvailableSpace(Self, SpaceAvailReply)
      end  
      else
      begin
        DoError(SSH_ERROR_INVALID_PACKET, SErrorInvalidPacket);
        DoCloseConnection;
        FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
      end;
    finally
      FreeAndNil(SpaceAvailReply);
    end;
  end;  

  if Cont and Assigned(FOnExtendedReply) then
    FOnExtendedReply(Self, @ExtData[1], Length(ExtData));
  FSyncRequestComplete := FSyncRequest;
end;
{end}

function TElSftpClient.ParseFileAttributes(P: pointer; Size: integer; Attributes:
  TElSftpFileAttributes): integer;
begin
  Result := Attributes.LoadFromBuffer(P, Size, FSFTPVersion);
end;


procedure TElSftpClient.ParseData(P: pointer; Size: integer);
var
  Id: cardinal;
  DataLen: cardinal;
  Req : PSBSftpRequestInfo;
  DataReq : PSBSftpDataRequestInfo;
begin
  try
    Id := ReadLength(P, Size);
    DataLen := ReadLength(@PByteArray(P)[4], Size - 4);
  except
    DoError(SSH_ERROR_INVALID_PACKET, SErrorInvalidPacket);
    DoCloseConnection;
    FTunnelConnection.Close({$ifndef HAS_DEF_PARAMS}false{$endif});
    Exit;
  end;
  PopRequest(Id, Req);
  Dispose(Req);

  PopDataRequest(ID, DataReq);
  if DataReq <> nil then
  begin
    if FDataBufferPtr <> nil then
    begin
      if FDataBufferPtrSize >= DataReq.Offset + DataLen then
      begin
        Move(PByteArray(P)[8], PByteArray(FDataBufferPtr)[DataReq.Offset], DataLen);
        inc(FDataBufferPtrFilled, DataLen);
      end
    end
    else
    begin
      if DataReq.Offset + DataLen > Length(FDataBuffer) then
        SetLength(FDataBuffer, DataReq.Offset + DataLen);
      Move(PByteArray(P)[8], FDataBuffer[DataReq.Offset], DataLen);
    end;
    if not HasPendingDataRequests(DataReq.DataID) then
    begin
      CompleteDownloadRequest(DataReq.DataID);
      FSyncRequestComplete := FSyncRequest;
    end;
    Dispose(DataReq);
  end;

end;

procedure TElSftpClient.SendOnTransportLayer(Size: integer; PacketType: byte);
var
  P: PSBSftpRequestInfo;
begin
  Inc(Size);
  PByteArray(FOutBuffer)[0] := Size shr 24;
  PByteArray(FOutBuffer)[1] := (Size shr 16) and $FF;
  PByteArray(FOutBuffer)[2] := (Size shr 8) and $FF;
  PByteArray(FOutBuffer)[3] := Size and $FF;
  PByteArray(FOutBuffer)[4] := PacketType;
  New(P);
  P^.Id := FCurrentID;
  P^.Request := PacketType;
  PushRequest(P);
  DoSend(@FOutBuffer[0], Size + 4);
  Inc(FCurrentID);
end;

procedure TElSftpClient.SendOnTransportLayer(Size: integer; Buffer: pointer; BufferSize:
  integer; PacketType: byte);
var
  P: PSBSftpRequestInfo;
  TotalSize: integer;
begin
  Inc(Size);
  TotalSize := Size + BufferSize;
  PByteArray(FOutBuffer)[0] := TotalSize shr 24;
  PByteArray(FOutBuffer)[1] := (TotalSize shr 16) and $FF;
  PByteArray(FOutBuffer)[2] := (TotalSize shr 8) and $FF;
  PByteArray(FOutBuffer)[3] := TotalSize and $FF;
  PByteArray(FOutBuffer)[4] := PacketType;
  New(P);
  P^.Id := FCurrentID;
  P^.Request := PacketType;
  PushRequest(P);
  DoSend(@FOutBuffer[0], Size + 4);
  DoSend(Buffer, BufferSize);

  Inc(FCurrentID);
end;

procedure TElSftpClient.SendFxpInit;
begin
  PByteArray(FOutBuffer)[5] := 0;
  PByteArray(FOutBuffer)[6] := 0;
  PByteArray(FOutBuffer)[7] := 0;

  if sbSFTP6 in FVersions then
    PByteArray(FOutBuffer)[8] := 6
  else if sbSFTP5 in FVersions then
    PByteArray(FOutBuffer)[8] := 5
  else if sbSFTP4 in FVersions then
    PByteArray(FOutBuffer)[8] := 4
  else
    PByteArray(FOutBuffer)[8] := 3;
  SendOnTransportLayer(4, SSH_FXP_INIT);
end;

procedure TElSftpClient.SendOpenFile(const Filename: string; Modes:
  TSBSftpFileOpenModes; Attributes: TElSftpFileAttributes; Access : TSBSftpFileOpenAccess);
var
  Msg: string;
  Md: cardinal;
  Flags: cardinal; // for SFTPv5
  DefAttr : boolean;
begin
  Flags := 0;
  Md := 0;

  if FSFTPVersion < 5 then
  begin
    if fmRead in Modes then
      Md := Md or SSH_FXF_READ;
    if fmWrite in Modes then
      Md := Md or SSH_FXF_WRITE;
    if fmAppend in Modes then
      Md := Md or SSH_FXF_APPEND;
    if fmCreate in Modes then
    begin
      Md := Md or SSH_FXF_CREAT;
      if fmTruncate in Modes then
        Md := Md or SSH_FXF_TRUNC;
      if fmExcl in Modes then
        Md := Md or SSH_FXF_EXCL;
    end;
    if (FVersion = sbSFTP4) and (fmText in Modes) then
      Md := Md or SSH_FXF_TEXT;
  end
  else
  begin
    if FExtendedProperties.AutoAdjustDesiredAccess then
    begin
      if fmRead in Modes then
        Md := Md or ACE4_READ_DATA;
      if fmWrite in Modes then
        Md := Md or ACE4_WRITE_DATA;
      if fmAppend in Modes then
        Md := Md or ACE4_APPEND_DATA;
      //Md := Md or ACE4_READ_ATTRIBUTES; !!
    end
    else
      Md := FExtendedProperties.DesiredAccess;

    if fmOpenOrCreate in Modes then
    begin
      if fmTruncate in Modes then
        Flags := SSH_FXF_CREATE_TRUNCATE
      else
        Flags := SSH_FXF_OPEN_OR_CREATE;
    end
    else
    if fmCreate in Modes then
    begin
      if fmTruncate in Modes then
        Flags := SSH_FXF_CREATE_TRUNCATE
      else
        Flags := SSH_FXF_CREATE_NEW;
    end
    else
    begin
      if fmTruncate in Modes then
        Flags := SSH_FXF_TRUNCATE_EXISTING
      else
        Flags := SSH_FXF_OPEN_EXISTING;
    end;

    if faReadLock in Access then
      Flags := Flags or SSH_FXF_ACCESS_READ_LOCK;
    if faWriteLock in Access then
      Flags := Flags or SSH_FXF_ACCESS_WRITE_LOCK;
    if faDeleteLock in Access then
      Flags := Flags or SSH_FXF_ACCESS_DELETE_LOCK;

    if fmAppend in Modes then
      Flags := Flags or SSH_FXF_ACCESS_APPEND_DATA;
    if fmAppendAtomic in Modes then
      Flags := Flags or SSH_FXF_ACCESS_APPEND_DATA_ATOMIC;

    if FSFTPVersion > 5 then
    begin
      if faBlockAdvisory in Access then
        Flags := Flags or SSH_FXF_ACCESS_BLOCK_ADVISORY;
      if fmNoFollow in Modes then
        Flags := Flags or SSH_FXF_ACCESS_NOFOLLOW;
      if fmDeleteOnClose in Modes then
        Flags := Flags or SSH_FXF_ACCESS_DELETE_ON_CLOSE;
    end;

    if fmText in Modes then
      Flags := Flags or SSH_FXF_ACCESS_TEXT_MODE;
  end;

  DefAttr := false;
  if not Assigned(Attributes) then
  begin
    DefAttr := true;
    WriteDefaultAttributes(Attributes); // WriteDefaultAttributes will allocate an object for us
  end;

  Attributes.FileType := ftFile;

  if FSFTPVersion < 5 then
    Msg := WriteUINT32(FCurrentID) + WriteString(Filename) + WriteUINT32(Md) +
      Attributes.SaveToBuffer(FSFTPVersion)
  else
    Msg := WriteUINT32(FCurrentID) + WriteString(Filename) + WriteUINT32(Md) +
      WriteUINT32(Flags) + Attributes.SaveToBuffer(FSFTPVersion);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_OPEN);
  if DefAttr then
    Attributes.Free;
end;

procedure TElSftpClient.SendOpenDir(const Path: string);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Path);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_OPENDIR);
end;

procedure TElSFTPClient.SendBlock(const Handle: TSBSftpFileHandle; Offset, Length : Int64;
  LockMask : TSBSftpFileOpenAccess);
var
  Mask : cardinal;
  Msg : BufferType;
begin
  Mask := FileOpenAccessToCardinal(LockMask);
  Msg := WriteUINT32(FCurrentID) + WriteString(Handle) + WriteUINT64(Offset) +
    WriteUINT64(Length) + WriteUINT32(Mask);
  Move(Msg[1], FOutBuffer[5], System.Length(Msg));

  SendOnTransportLayer(System.Length(Msg), SSH_FXP_BLOCK);
end;

procedure TElSFTPClient.SendUnblock(const Handle: TSBSftpFileHandle; Offset, Length : Int64);
var
  Msg : BufferType;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Handle) + WriteUINT64(Offset) +
    WriteUINT64(Length);
  Move(Msg[1], FOutBuffer[5], System.Length(Msg));
  SendOnTransportLayer(System.Length(Msg), SSH_FXP_UNBLOCK);
end;

procedure TElSftpClient.SendReadDir(const Handle: TSBSftpFileHandle);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Handle);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_READDIR);
end;

procedure TElSftpClient.SendClose(const Handle: TSBSftpFileHandle);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Handle);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_CLOSE);
end;

procedure TElSftpClient.SendRemoveFile(const Filename: string);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Filename);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_REMOVE);
end;

procedure TElSftpClient.SendRenameFile(const OldPath, NewPath: string);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(OldPath) + WriteString(NewPath) +
    WriteUINT32(FExtendedProperties.RenameFlagsCardinal);

  Move(Msg[1], FOutBuffer[5], Length(Msg));  

  SendOnTransportLayer(Length(Msg), SSH_FXP_RENAME);
end;

procedure TElSftpClient.SendMkDir(const Path: string; Attributes:
  TElSftpFileAttributes);
var
  Msg: string;
  DefAttr : boolean;
begin
  DefAttr := false;
  if not Assigned(Attributes) then
  begin
    DefAttr := true;
    WriteDefaultAttributes(Attributes);
  end;
  {JPM Modifications for SFTP 4}
  //You should set the file type attribute in SFTP 4 and 5
  //because the file type is required in all attribute listings
  //including minimal ones.
  if FSFTPVersion > 3 then
  begin
    Attributes.FileTypeByte := SSH_FILEXFER_TYPE_DIRECTORY;
  end;
  {end modifications}

  Msg := WriteUINT32(FCurrentID) + WriteString(Path) +
    Attributes.SaveToBuffer(FSFTPVersion);
  Move(Msg[1], FOutBuffer[5], Length(Msg));

  if DefAttr then
    Attributes.Free;

  SendOnTransportLayer(Length(Msg), SSH_FXP_MKDIR);
end;

procedure TElSftpClient.SendRmDir(const Path: string);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Path);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_RMDIR);
end;

procedure TElSftpClient.SendStat(const Path: string);
var
  Msg: string;
begin

  Msg := WriteUINT32(FCurrentID) + WriteString(Path);

  if FSFTPVersion > 3 then
    Msg := Msg + WriteUINT32(FExtendedProperties.RequestAttributesByVersion(FSFTPVersion));

  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_STAT);
end;

procedure TElSftpClient.SendLStat(const Path: string);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Path);

  if FSFTPVersion >3 then
    Msg := Msg + WriteUINT32(FExtendedProperties.RequestAttributesByVersion(FSFTPVersion));

  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_LSTAT);
end;

procedure TElSftpClient.SendFStat(const Handle: TSBSftpFileHandle);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Handle);
  if FSFTPVersion > 3 then
    Msg := Msg + WriteUINT32(FExtendedProperties.RequestAttributesByVersion(FSFTPVersion));
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_FSTAT);
end;

procedure TElSftpClient.SendSetStat(const Path: string; Attributes:
  TElSftpFileAttributes);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Path) +
    Attributes.SaveToBuffer(FSFTPVersion);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_SETSTAT);
end;

procedure TElSftpClient.SendFSetStat(const Handle: TSBSftpFileHandle;
  Attributes: TElSftpFileAttributes);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Handle) +
    Attributes.SaveToBuffer(FSFTPVersion);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_SETSTAT);
end;


procedure TElSftpClient.SendReadlink(const Path: string);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Path);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_READLINK);
end;

procedure TElSftpClient.SendSymlink(const LinkPath: string; const TargetPath: string);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(LinkPath) +
    WriteString(TargetPath);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_SYMLINK);
end;

procedure TElSftpClient.SendLink(const NewLinkPath: string; const ExistingPath: string;
  SymLink : boolean);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(NewLinkPath) +
    WriteString(ExistingPath) + WriteBoolean(SymLink);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  SendOnTransportLayer(Length(Msg), SSH_FXP_LINK);
end;

procedure TElSftpClient.SendRealPath(const Path: String;
  Control : TSBSFTPRealpathControl; ComposePath : TStringList);
var
  Msg: string;
  Index : integer;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Path);

  if FSFTPVersion > 5 then
  begin
    if Assigned(ComposePath) and (ComposePath.Count > 0) then
    begin
      Msg := Msg + Char(RealPathControlToByte(Control));

      for Index := 0 to ComposePath.Count - 1 do
      begin
        Msg := Msg + WriteString(ComposePath[Index]);
      end;
    end
    else
    if Control <> rcNoCheck then
      Msg := Msg + Char(RealPathControlToByte(Control));
  end;

  Move(Msg[1], FOutBuffer[5], Length(Msg));

  SendOnTransportLayer(Length(Msg), SSH_FXP_REALPATH);
end;

procedure TElSftpClient.SendWrite(const Handle: TSBSftpFileHandle; Offset: Int64;
  Buffer: pointer; Size: integer; var CurrentID : Cardinal);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Handle) + WriteUINT64(Offset) +
    WriteUINT32(Size);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  CurrentID := FCurrentID;
  SendOnTransportLayer(Length(Msg), Buffer, Size, SSH_FXP_WRITE);
end;

procedure TElSftpClient.SendRead(const Handle: TSBSftpFileHandle; Offset: Int64; Size:
  integer; var CurrentID: Cardinal);
var
  Msg: string;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Handle) + WriteUINT64(Offset) +
    WriteUINT32(Size);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  CurrentID := FCurrentID;
  SendOnTransportLayer(Length(Msg), SSH_FXP_READ);
end;

{JPM - modification for sending an extended packet}
procedure TElSftpClient.SendExtended(const Extension : string; DataBuffer: pointer;
  Size: integer);
var
  Request : PSBSftpRequestInfo;
  Msg: string;
  Count : integer;
  ReqID : cardinal;
begin
  Msg := WriteUINT32(FCurrentID) + WriteString(Extension);
  Move(Msg[1], FOutBuffer[5], Length(Msg));
  if (DataBuffer<>nil) and (Size<>0) then
  begin
    Move(DataBuffer^, FOutBuffer[5 + Length(Msg)], Size);
    Count := Length(Msg) + Size;
  end
  else
    Count := Length(Msg);

  ReqID := FCurrentID;
  SendOnTransportLayer(Count, SSH_FXP_EXTENDED);
  PopRequest(ReqID, Request);
  Request.ExtType := Extension;
  PushRequest(Request);
end;
{end}


{$IFNDEF CLX_USED}
function TElSftpClient.IntMessageLoop: boolean;
var
  Msg: TMsg;
begin
  result := FSyncRequestComplete <> FSyncRequest;
  if result = false then exit;
  //if result then
//    TElSSHClient(FTunnel.TunnelList.SSHClass).DataAvailable;

  while PeekMessage(Msg, 0, 0, 0, PM_REMOVE) do
  begin
    if (Msg.message = WM_QUIT) or (FSyncRequestComplete = FSyncRequest) then
    begin
      result := false;
      break;
    end;
    TranslateMessage(Msg);
    DispatchMessage(Msg);
  end;
end;

{$ELSE}

function TElSftpClient.IntMessageLoop: boolean;
begin
  result := FSyncRequestComplete <> FSyncRequest;
  //result := false;
end;
{$ENDIF}

////////////////////////////////////////////////////////////////////////////////
// Handlers

procedure TElSftpClient.HandleTunnelOpen(Sender: TObject; TunnelConnection:
  TElSSHTunnelConnection);
begin
  FTunnelConnection := TElSSHClientTunnelConnection(TunnelConnection);
{$IFDEF SBB_Debug}
  Debug('Sftp subsystem connection established');
{$ENDIF}
  FTunnelConnection.OnData := HandleTunnelConnectionData;
  FTunnelConnection.OnExtendedData := HandleTunnelConnectionExtendedData;
  FTunnelConnection.OnError := HandleTunnelConnectionError;
  FTunnelConnection.OnClose := HandleTunnelConnectionClose;
  if not SynchronousMode then
    OpenInternal
  else
  begin
    FActive := true;
    if Assigned(FOnOpenConnection) then
      FOnOpenConnection(Self);
  end;
end;

procedure TElSftpClient.HandleTunnelClose(Sender: TObject; TunnelConnection:
  TElSSHTunnelConnection);
begin
  DoCloseConnection;
end;

procedure TElSftpClient.HandleTunnelError(Sender: TObject; Error: integer;
  Data: pointer);
begin
  DoError(SSH_ERROR_TUNNEL_ERROR, SErrorTunnelError);
  DoCloseConnection;
end;

procedure TElSftpClient.HandleTunnelConnectionData(Sender: TObject; Buffer:
  pointer; Size: longint);
begin
  if Length(FInBuffer) - FInBufferIndex < Size then
    SetLength(FInBuffer, FInBufferIndex + Size);
  Move(Buffer^, FInBuffer[FInBufferIndex], Size);
  Inc(FInBufferIndex, Size);
  AnalizeBuffer;
end;

procedure TElSftpClient.HandleTunnelConnectionExtendedData(Sender: TObject;
  Buffer: pointer; Size: longint);
begin
  AnalizeBuffer;
end;

procedure TElSftpClient.HandleTunnelConnectionClose(Sender: TObject; CloseType:
  TSSHCloseType);
begin
  DoError(SSH_ERROR_CONNECTION_CLOSED, SErrorConnectionClosed);
  DoCloseConnection;
end;

procedure TElSftpClient.HandleTunnelConnectionError(Sender: TObject; Error:
  integer);
begin
  DoError(SSH_ERROR_TUNNEL_ERROR, SErrorTunnelError);
  DoCloseConnection;
end;

////////////////////////////////////////////////////////////////////////////////
// Properties management

procedure TElSftpClient.Notification(AComponent: TComponent; AOperation:
  TOperation);
begin
  inherited;
end;

procedure TElSftpClient.SetTunnel(Tunnel: TElSubsystemSSHTunnel);
begin
  FTunnel := Tunnel;
  FTunnel.OnOpen := HandleTunnelOpen;
  FTunnel.OnClose := HandleTunnelClose;
  FTunnel.OnError := HandleTunnelError;
  FTunnel.Subsystem := 'sftp';
end;

function TElSftpClient.GetTunnel: TElSubsystemSSHTunnel;
begin
  Result := FTunnel;
end;

function TElSFTPClient.GetSFTPExtCount : integer;
begin
  Result := FSFTPExt.Count;
end;

function TElSFTPClient.GetSFTPExt(Index : integer) : TSBSftpExtendedAttribute;
begin
  if (Index >= 0) and (Index < FSFTPExt.Count) then
    Result := TSBSftpExtendedAttribute(FSFTPExt[Index])
  else
    Result := nil;
end;

procedure TElSftpClient.SetMessageLoop(Value: TSBSftpMessageLoopEvent);
begin
  if not Assigned(Value) then
    FMessageLoop := IntMessageLoop
  else
    FMessageLoop := Value;
end;

////////////////////////////////////////////////////////////////////////////////
// Event pushers

{$IFDEF SBB_Debug}

procedure TElSftpClient.Debug(Str: string);
begin
  if Assigned(FOnDebug) then
    FOnDebug(Self, Str);
end;
{$ENDIF}

procedure TElSftpClient.DoSend(Buffer: pointer; Size: integer);
begin
  FTunnelConnection.SendData(Buffer, Size);
end;

procedure TElSftpClient.DoOpenConnection;
begin
  FActive := true;
  if Assigned(FOnOpenConnection) and not (FSynchronousMode)then
    FOnOpenConnection(Self);
  if SynchronousMode then
  begin
    Self.FLastSyncError := 0;
    Self.FLastSyncComment := '';
    Self.FSyncRequestComplete := FSyncRequest;
  end;
end;

procedure TElSftpClient.DoOpenFile(Handle: TSBSftpFileHandle);
begin
  if Assigned(FOnOpenFile) then
    FOnOpenFile(Self, Handle);
  if SynchronousMode then
  begin
    Self.FLastSyncError := 0;
    Self.FLastSyncComment := '';
    Self.FSyncRequestComplete := FSyncRequest;
    Self.FLastSyncHandle := Handle;
  end;
end;

procedure TElSftpClient.DoError(ErrorCode: integer; const Comment: string);
begin
  if Assigned(FOnError) then
    FOnError(Self, ErrorCode, Comment);
  if SynchronousMode then
  begin
    Self.FLastSyncError := ErrorCode;
    Self.FLastSyncComment := Comment;
    Self.FSyncRequestComplete := FSyncRequest;
  end;
end;

procedure TElSftpClient.DoSuccess(const Comment: string);
begin
  if Assigned(FOnSuccess) then
    FOnSuccess(Self, Comment);
  if SynchronousMode then
  begin
    Self.FLastSyncError := 0;
    Self.FLastSyncComment := Comment;
    Self.FSyncRequestComplete := FSyncRequest;
  end;
end;

procedure TElSftpClient.DoCloseConnection;
begin
  FActive := false;
  if Assigned(FOnCloseConnection) then
    FOnCloseConnection(Self);
  if SynchronousMode then
  begin
    Self.FLastSyncError := 0;
    Self.FLastSyncComment := '';
    Self.FSyncRequestComplete := FSyncRequest;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// Request stack operations

procedure TElSftpClient.PushRequest(Request: PSBSftpRequestInfo);
begin
  FRequests.Add(Request);
end;

procedure TElSftpClient.PushDataRequest(Request : PSBSftpDataRequestInfo);
begin
  FDataRequests.Add(Request);
end;

procedure TElSftpClient.PopRequest(Id: cardinal; out Request: PSBSftpRequestInfo);
var
  I: integer;
  P: PSBSftpRequestInfo;
begin
  Request := nil;
  I := 0;
  while I < FRequests.Count do
  begin
    P := PSBSftpRequestInfo(FRequests[I]);
    if P.Id = Id then
    begin
      Request := P;
      FRequests.Remove(P);
    end;
    Inc(I);
  end;
end;

procedure TElSftpClient.PopDataRequest(Id: cardinal; out Request: PSBSftpDataRequestInfo);
var
  I: integer;
  P: PSBSftpDataRequestInfo;
begin
  Request := nil;
  I := 0;
  while I < FDataRequests.Count do
  begin
    P := PSBSftpDataRequestInfo(FDataRequests[I]);
    if P.Id = Id then
    begin
      Request := P;
      FDataRequests.Remove(P);
    end;
    Inc(I);
  end;
end;

procedure TElSftpClient.PopDataRequest2(DataId: cardinal; out Request: PSBSftpDataRequestInfo);
var
  I: integer;
  P: PSBSftpDataRequestInfo;
begin
  Request := nil;
  I := 0;
  while I < FDataRequests.Count do
  begin
    P := PSBSftpDataRequestInfo(FDataRequests[I]);
    if P.DataId = DataId then
    begin
      Request := P;
      FDataRequests.Remove(P);
    end;
    Inc(I);
  end;
end;

function TElSftpClient.HasPendingDataRequests(DataId : cardinal) : boolean;
var i : integer;
begin
  result := false;
  for i := 0 to FDataRequests.Count - 1 do
  begin
    if PSBSftpDataRequestInfo(FDataRequests[I]).DataId = DataId then
    begin
      result := true;
      exit;
    end;
  end;
end;

procedure TElSftpClient.SetSynchronousMode(Value: Boolean);
begin
  FSynchronousMode := Value;
end;


function TElSftpClient.GetActive : boolean;
begin
  Result := FActive and (FTunnelConnection <> nil);
end;

procedure TElSftpClient.SetSftpBufferSize(Value: Integer);
begin
  if (FSftpBufferSize <> Value) and (not Active) and (Value > 65535) then
  begin
    FSftpBufferSize := Value;
    SetLength(FInBuffer, FSftpBufferSize);
    SetLength(FOutBuffer, FSftpBufferSize);
  end;
end;

{$ifdef BUILDER_USED}
function TElSftpClient.SftpCreateFile(const Filename: string): Boolean;
begin
  result := CreateFile(Filename);
end;

function TElSftpClient.SftpOpenFile(const Filename: string; Modes: TSBSftpFileOpenModes; Attributes:
  TElSftpFileAttributes): Boolean;
begin
  result := OpenFile(Filename, Modes, Attributes);
end;

function TElSftpClient.SftpOpenFile(const Filename: string; Modes: TSBSftpFileOpenModes;
  Access : TSBSftpFileOpenAccess; Attributes: TElSftpFileAttributes): Boolean;
begin
  result := OpenFile(Filename, Modes, Access, Attributes);
end;

function TElSftpClient.SftpRemoveDirectory(const Path: string): Boolean;
begin
  result := RemoveDirectory(Path);
end;

function TElSftpClient.SftpRemoveFile(const Filename: string): Boolean;
begin
  result := RemoveFile(Filename);
end;
{$endif}

end.



