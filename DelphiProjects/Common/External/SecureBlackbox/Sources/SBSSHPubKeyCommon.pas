(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSSHPubKeyCommon;

interface

uses
  SysUtils,
  Classes,
  SBSSHCommon,
  SBSSHConstants,
  SBSSHKeyStorage,
  SBUtils,
  SBSSHUtils;


const
  SSH_PUBLICKEY_SUCCESS = 0;
  SSH_PUBLICKEY_ACCESS_DENIED = 1;
  SSH_PUBLICKEY_STORAGE_EXCEEDED = 2;
  SSH_PUBLICKEY_VERSION_NOT_SUPPORTED = 3;
  SSH_PUBLICKEY_KEY_NOT_FOUND = 4;
  SSH_PUBLICKEY_KEY_NOT_SUPPORTED = 5;
  SSH_PUBLICKEY_KEY_ALREADY_PRESENT = 6;
  SSH_PUBLICKEY_GENERAL_FAILURE = 7;
  SSH_PUBLICKEY_REQUEST_NOT_SUPPORTED = 8;
  SSH_PUBLICKEY_ATTRIBUTE_NOT_SUPPORTED = 9;

type
  EElSSHPublicKeyError =  class(Exception);
  
  TElSSHPublicKeyAttributes = class(TObject)
  protected
    FAttrs: TCollection;
    function GetAttrName(i: integer): string;
    function GetAttrValue(i: integer): string;
    function GetAttrMandatory(i: integer): boolean;
    function GetCount: integer;
    procedure SetAttrName(i: integer; const v: string);
    procedure SetAttrValue(i: integer; const v: string);
    procedure SetAttrMandatory(i: integer; v: boolean);
  public
    constructor Create;
    destructor Destroy; override;
    
    procedure Add(const AName, AValue: string; AMandatory: boolean {$ifdef HAS_DEF_PARAMS}= false{$endif});
    procedure Remove(Index : integer);
    procedure Clear;
    property Count: integer read GetCount;
    property Names[Index: integer]: string read GetAttrName write SetAttrName;
    property Values[Index: integer]: string read GetAttrValue write SetAttrValue;
    property Mandatory[Index: integer]: boolean read GetAttrMandatory write SetAttrMandatory;
  end;

  TSBSSHPublicKeyErrorEvent =  procedure(Sender: TObject;
    ErrorCode: integer; const Comment: string) of object;
  TSBSSHPublicKeyInvalidReplyEvent =  procedure(Sender: TObject) of object;
  TSBSSHPublicKeyStatusEvent =  procedure(Sender: TObject;
    Status: integer; const Descr, Tag: string) of object;
  TSBSSHPublicKeyAttributeEvent =  procedure(Sender: TObject;
    const Attr: string; Compulsory: boolean) of object;
  TSBSSHPublicKeyPublicKeyEvent =  procedure(Sender: TObject;
    Key: TElSSHKey; Attributes : TElSSHPublicKeyAttributes) of object;

  // public key subsystem server event types
  TSBSSHPublicKeySendEvent = procedure(Sender: TObject;
    Buffer: pointer; Size: integer) of object;
  TSBSSHPublicKeyReceiveEvent = procedure(Sender: TObject;
    Buffer: pointer; MaxSize: integer; var Written: integer) of object;

  TSBSSHPublicKeyAddEvent =  procedure(Sender: TObject;
    Key: TElSSHKey; Attributes : TElSSHPublicKeyAttributes; Overwrite: boolean;
    var ErrorCode: integer; var Comment : string) of object;
  TSBSSHPublicKeyRemoveEvent =  procedure(Sender: TObject;
    Key: TElSSHKey; var ErrorCode: integer; var Comment : string) of object;
  TSBSSHPublicKeyListEvent =  procedure(Sender: TObject;
    Keys: TList;
    Attributes : TList;
    var FreeObjects : boolean;
    var ErrorCode: integer; var Comment : string) of object;
  TSBSSHPublicKeyListAttributesEvent =  procedure(Sender: TObject;
    Names : TStringList; Compulsories : TBits;
    var ErrorCode: integer; var Comment : string) of object;
  TSBSSHPublicKeyAttrubuteSupportedEvent =  procedure(Sender: TObject;
    const Attr : string; var Supported : boolean) of object;

const
  BUFFER_SIZE = 16384; //128000;

implementation

type
  TPubKeyAttr = class(TCollectionItem)
  protected
    FName: string;
    FValue: string;
    FMandatory: boolean;
  end;

constructor TElSSHPublicKeyAttributes.Create;
begin
  inherited Create;
  FAttrs := TCollection.Create(TPubKeyAttr);
end;

destructor TElSSHPublicKeyAttributes.Destroy;
begin
  FAttrs.Free;
  inherited;
end;

procedure TElSSHPublicKeyAttributes.Add(const AName, AValue: string;
  AMandatory: boolean {$ifdef HAS_DEF_PARAMS}= false{$endif});
var
  a: TPubKeyAttr;
begin
  a := TPubKeyAttr(FAttrs.Add);
  a.FName := AName;
  a.FValue := AValue;
  a.FMandatory := AMandatory;
end;

function TElSSHPublicKeyAttributes.GetCount: integer;
begin
  result := FAttrs.Count;
end;

function TElSSHPublicKeyAttributes.GetAttrName(i: integer): string;
begin
  result := TPubKeyAttr(FAttrs.Items[i]).FName;
end;

function TElSSHPublicKeyAttributes.GetAttrValue(i: integer): string;
begin
  result := TPubKeyAttr(FAttrs.Items[i]).FValue;
end;

function TElSSHPublicKeyAttributes.GetAttrMandatory(i: integer): boolean;
begin
  result := TPubKeyAttr(FAttrs.Items[i]).FMandatory;
end;

procedure TElSSHPublicKeyAttributes.SetAttrName(i: integer; const v: string);
begin
  TPubKeyAttr(FAttrs.Items[i]).FName := v;
end;

procedure TElSSHPublicKeyAttributes.SetAttrValue(i: integer; const v: string);
begin
  TPubKeyAttr(FAttrs.Items[i]).FValue := v;
end;

procedure TElSSHPublicKeyAttributes.SetAttrMandatory(i: integer; v: boolean);
begin
  TPubKeyAttr(FAttrs.Items[i]).FMandatory := v;
end;

procedure TElSSHPublicKeyAttributes.Remove(Index : integer);
begin
  {$ifndef VCL50} // will be true only for Delphi 4
  if (Index <> -1) and (FAttrs.Items[Index] <> nil) then
  TCollectionItem(FAttrs.Items[Index]).Collection := nil;
  {$else}
  FAttrs.Delete(Index);
  {$endif}
end;

procedure TElSSHPublicKeyAttributes.Clear;
begin
  FAttrs.Clear;
end;

end.

