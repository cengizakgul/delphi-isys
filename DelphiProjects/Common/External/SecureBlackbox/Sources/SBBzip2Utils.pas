(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBBzip2Utils;

interface

uses
  SBUtils,
  SysUtils,
  //Windows,
  SBBzip2BaseTypes;

type

  TSBDataArray = class(TObject)
  private
    {$IFNDEF CLR}
    Data_Array:PInt64;
    {$ELSE}
    Data_Array:System.Array;
    {$ENDIF}
    Data_Length:Int32;
  public
    constructor Create(Len:Int32);
    destructor Destroy;override;

    function GetArrayData(Index:Int32):Int64;
    procedure SetArrayData(Data:Int64;Index:Int32);
  end;

  TSBByteDataArray = class(TObject)
  private
    {$IFNDEF CLR}
    Data_Array : PByte;
    {$ELSE}
    Data_Array:System.Array;
    {$ENDIF}
    Data_Length:Int32;
  public
    constructor Create(Len: Int32);
    destructor Destroy; override;

    function GetArrayData(Index: Int32): byte;
    procedure SetArrayData(Data: byte; Index:Int32);
  end;

type
  TSBByteArray = class({T_Byte_Data_Array}TSBDataArray)
  public
    function GetData(Index:Int32):Byte;
    procedure SetData(Data:Byte;Index:Int32);
  end;

  TSBInt16Array = class(TSBDataArray)
  public
    function GetData(Index:Int32):Int16;
    procedure SetData(Data:Int16;Index:Int32);
  end;

  TSBUInt16Array = class(TSBDataArray)
  public
    function GetData(Index:Int32):UInt16;
    procedure SetData(Data:UInt16;Index:Int32);
  end;

  TSBInt32Array = class(TSBDataArray)
  public
    function GetData(Index:Int32):Int32;
    procedure SetData(Data:Int32;Index:Int32);
  end;

  TSBUInt32Array = class(TSBDataArray)
  public
    function GetData(Index:Int32):UInt32;
    procedure SetData(Data:UInt32;Index:Int32);
  end;

type
  TSBUInt16onByteArray = class(TObject)
  public
    Base_Array: TSBByteArray;
    Offset:Int64;
    Name:string;
    constructor Create(Base:TSBByteArray; ParentName:string);
    destructor Destroy;override;

    function GetData(Index:Int32):UInt16;
    procedure SetData(Data:UInt16;Index:Int32);
  end;

  TSBByteonUInt32Array = class(TObject)
  public
    Base_Array:TSBUInt32Array;
    Offset:Int64;
    Name:string;
    constructor Create(Base:TSBUInt32Array; ParentName:string);
    destructor Destroy;override;

    function GetData(Index:Int32):Byte;
    procedure SetData(Data:Byte;Index:Int32);
  end;

  TSBUInt16onUInt32Array = class(TObject)
  public
    Base_Array:TSBUInt32Array;
    Offset:Int64;
    Name:string;
    constructor Create(Base:TSBUInt32Array; ParentName:string);
    destructor Destroy;override;

    function GetData(Index:Int32):UInt16;
    procedure SetData(Data:UInt16;Index:Int32);
  end;

type
  TSBArrayPointer = class(TObject)
  public
    Base_Array :TSBDataArray;
    Offset:Int32;
    Name:string;
    constructor Create(Base:TSBDataArray; ParentName: string);
    destructor Destroy; override;

    procedure SetData(Data:Int64;Index:Int32);
    procedure IncOffset(Index:Int32);
    procedure SetOffset(Index:Int32);
    function  GetData(Index:Int32):Int64;
    function  GetAtPtr:Int64;
    procedure SetAtPtr(Data:Int64);
  end;

  TSBInt32ArrayPointer = class(TSBArrayPointer)
  public
    procedure SetData(Data:Int32;Index:Int32);
    function  GetData(Index:Int32):Int32;
    function  GetAtPtr:Int32;
    procedure SetAtPtr(Data:Int32);
  end;

  TSBUInt32ArrayPointer = class(TSBArrayPointer)
  public
    procedure SetData(Data:UInt32;Index:Int32);
    function  GetData(Index:Int32):UInt32;
    function  GetAtPtr:UInt32;
    procedure SetAtPtr(Data:UInt32);
  end;

  TSBByteArrayPointer = class(TSBArrayPointer)
  public
    procedure SetData(Data:Byte;Index:Int32);
    function  GetData(Index:Int32):Byte;
    function  GetAtPtr:Byte;
  end;


function IfThenInt(AValue: Boolean; const ATrue: Integer; const AFalse: Integer): Integer; 
function IfThenInt64(AValue: Boolean; const ATrue: Int64; const AFalse: Int64): Int64; 
function IfThenDouble(AValue: Boolean; const ATrue: Double; const AFalse: Double): Double; 
function IfThenLongword(AValue: Boolean; const ATrue: LongWord; const AFalse: LongWord): LongWord; 

implementation

////////////////////////////////////////////////////////////////////////////////
// Int64-based array

constructor TSBDataArray.Create(Len:Int32);
var i:Int32;
begin
  inherited Create;
        {$IFNDEF CLR}
        GetMem(Data_Array,Len*SizeOf(Int64));
        {$ELSE}
        Data_Array := System.Array.CreateInstance(TypeOf(Int64), Len);
        {$ENDIF}
        Data_Length:=Len;
        for i:=0   to Len-1 do
                SetArrayData(0,i);
end;

destructor TSBDataArray.Destroy;
begin
        {$IFNDEF CLR}
        FreeMem(Data_Array);
        {$ELSE}
        Data_Array.Free;
        {$ENDIF}
  inherited;
end;

function TSBDataArray.GetArrayData(Index:Int32):Int64;
var Res:Int64;
begin
        if (Index>=Data_Length) or (Index<0) then
        begin
                ERangeError.Create('Out of index!');
                Res:=0;
        end
        else
        {$IFNDEF CLR}
          Res:=PInt64(LongWord(Data_Array)+cardinal(Index)*SizeOf(Data_Array^))^;
        {$ELSE}
          Res:=Int64(Data_Array.GetValue(Index));
        {$ENDIF}
        Result:=Res;
end;

procedure TSBDataArray.SetArrayData(Data:Int64;Index:Int32);
begin
        if (Index>=Data_Length) or (Index<0) then
                ERangeError.Create('Out of index!')
        else
        {$IFNDEF CLR}
          PInt64(LongWord(Data_Array)+cardinal(Index)*SizeOf(Data_Array^))^:=Data;
        {$ELSE}
          Data_Array.SetValue(TObject(Data),Index);
        {$ENDIF}
end;

////////////////////////////////////////////////////////////////////////////////
// Byte-based array

constructor TSBByteDataArray.Create(Len:Int32);
var i:Int32;
begin
  inherited Create;
        {$IFNDEF CLR}
        GetMem(Data_Array, Len * SizeOf(byte));
        {$ELSE}
        Data_Array := System.Array.CreateInstance(TypeOf(byte), Len);
        {$ENDIF}
        Data_Length:=Len;
        for i:=0   to Len-1 do
                SetArrayData(0, i);
end;

destructor TSBByteDataArray.Destroy;
begin
        {$IFNDEF CLR}
        FreeMem(Data_Array);
        {$ELSE}
        Data_Array.Free;
        {$ENDIF}
  inherited;
end;

function TSBByteDataArray.GetArrayData(Index:Int32): byte;
var Res: byte;
begin
        if (Index>=Data_Length) or (Index<0) then
        begin
                ERangeError.Create('Out of index!');
                Res:=0;
        end
        else
        {$IFNDEF CLR}
          Res:=PByte(LongWord(Data_Array)+cardinal(Index) * SizeOf(Data_Array^))^;
        {$ELSE}
          Res:=byte(Data_Array.GetValue(Index));
        {$ENDIF}
        Result:=Res;
end;

procedure TSBByteDataArray.SetArrayData(Data: byte; Index:Int32);
begin
        if (Index>=Data_Length) or (Index<0) then
                ERangeError.Create('Out of index!')
        else
        {$IFNDEF CLR}
          PByte(LongWord(Data_Array)+cardinal(Index) * SizeOf(Data_Array^))^ := Data;
        {$ELSE}
          Data_Array.SetValue(TObject(Data), Index);
        {$ENDIF}
end;

////////////////////////////////////////////////////////////////////////////////
// Other arrays

procedure TSBByteArray.SetData(Data:Byte;Index:Int32);
begin
        SetArrayData(Data,Index);
end;
function TSBByteArray.GetData(Index:Int32):Byte;
begin
        Result := GetArrayData(Index);
end;

procedure TSBInt16Array.SetData(Data:Int16;Index:Int32);
begin
        SetArrayData(Data,Index);
end;
function TSBInt16Array.GetData(Index:Int32):Int16;
begin
        Result := GetArrayData(Index);
end;

procedure TSBUInt16Array.SetData(Data:UInt16;Index:Int32);
begin
        SetArrayData(Data,Index);
end;
function TSBUInt16Array.GetData(Index:Int32):UInt16;
begin
        Result := GetArrayData(Index);
end;

procedure TSBInt32Array.SetData(Data:Int32;Index:Int32);
begin
        SetArrayData(Data,Index);
end;
function TSBInt32Array.GetData(Index:Int32):Int32;
begin
        Result := GetArrayData(Index);
end;

procedure TSBUInt32Array.SetData(Data:UInt32;Index:Int32);
begin
        SetArrayData(Data,Index);
end;
function TSBUInt32Array.GetData(Index:Int32):UInt32;
begin
        Result := GetArrayData(Index);
end;

constructor TSBUInt16onByteArray.Create(Base:TSBByteArray; ParentName:string);
begin
        inherited Create;
        Base_Array:=Base;
        Offset:=0;
        Name:= ParentName;
end;

destructor TSBUInt16onByteArray.Destroy;
begin
  inherited;
end;

procedure TSBUInt16onByteArray.SetData(Data:UInt16;Index:Int32);
begin
        If Base_Array=nil then exit;
        Base_Array.SetData(Data mod 256,Index*2+OffSet);
        Base_Array.SetData(Data div 256,Index*2+1+OffSet);
end;

function TSBUInt16onByteArray.GetData(Index:Int32):UInt16;
begin
        {$IFNDEF CLR}
        If Base_Array=nil then Sysutils.Exception.Create('Child byte array not defined!');
        {$ELSE}
        If Base_Array=nil then System.Exception.Create('Child byte array not defined!');
        {$ENDIF}
         Result:= UInt16(Base_Array.GetData(Index*2+OffSet))+UInt16(Base_Array.GetData(Index*2+1+OffSet))*256;
end;

constructor TSBByteonUInt32Array.Create(Base: TSBUInt32Array; ParentName:string);
begin
        inherited Create;
        Base_Array:=Base;
        Offset:=0;
        Name:=ParentName;
end;

destructor TSBByteonUInt32Array.Destroy;
begin
  inherited;
end;

procedure TSBByteonUInt32Array.SetData(Data:Byte;Index:Int32);
var Item:UInt32;
begin
        If Base_Array=nil then exit;
        Item:=($ffffffff and (not (UInt32($000000ff) shl (((Index+Offset) mod 4)*8)))) and Base_Array.GetData((Index+Offset) div 4);
        Item:=Item or (UInt32(Data) shl (((Index+Offset) mod 4)*8));
        Base_Array.SetData(Item,(Index+OffSet) div 4);
end;

function TSBByteonUInt32Array.GetData(Index:Int32):Byte;
var Item:UInt32;
begin
        {$IFNDEF CLR}
        If Base_Array=nil then Sysutils.Exception.Create('Child byte array not defined!');
        {$ELSE}
        If Base_Array=nil then System.Exception.Create('Child byte array not defined!');
        {$ENDIF}

        Item:=Base_Array.GetData((Index+Offset) div 4) and (UInt32($000000ff) shl (((Index+Offset) mod 4)*8));
        Item:=Item shr (((Index+Offset) mod 4)*8);
        Result:= Byte(Item);
end;


constructor TSBUInt16onUInt32Array.Create(Base:TSBUInt32Array; ParentName:string);
begin
        inherited Create;
        Base_Array:=Base;
        Offset:=0;
        Name:=ParentName;
end;

destructor TSBUInt16onUInt32Array.Destroy;
begin
  inherited;
end;

procedure TSBUInt16onUInt32Array.SetData(Data:UInt16;Index:Int32);
var Item:UInt32;
begin
        If Base_Array=nil then exit;
        Item:=($ffffffff and (not (UInt32($0000ffff) shl ((((Index+Offset) mod 2))*16)))) and Base_Array.GetData((Index+Offset) div 2);
        Item:=Item or (UInt32(Data) shl ((((Index+Offset) mod 2))*16));
        Base_Array.SetData(Item,(Index+OffSet) div 2);
end;

function TSBUInt16onUInt32Array.GetData(Index:Int32):UInt16;
var Item:UInt32;
begin
        {$IFNDEF CLR}
        If Base_Array=nil then Sysutils.Exception.Create('Child byte array not defined!');
        {$ELSE}
        If Base_Array=nil then System.Exception.Create('Child byte array not defined!');
        {$ENDIF}

        Item:=Base_Array.GetData((Index+Offset) div 2) and (UInt32($0000ffff) shl ((((Index+Offset) mod 2))*16));
        Item:=Item shr ((((Index+Offset) mod 2))*16);
        Result:= UInt16(Item);
end;

constructor TSBArrayPointer.Create(Base: TSBDataArray; ParentName:string);
begin
  inherited Create;
        Base_Array:=Base;
        Offset:=0;
        Name:=ParentName;
end;

destructor TSBArrayPointer.Destroy;
begin
  inherited;
end;

procedure TSBArrayPointer.SetData(Data:Int64;Index:Int32);
begin
        Base_Array.SetArrayData(Data,Index+Offset);
end;

procedure TSBArrayPointer.IncOffset(Index:Int32);
begin
        Offset:=Offset+Index;
end;

procedure TSBArrayPointer.SetOffset(Index:Int32);
begin
        Offset:=Index;
end;

function TSBArrayPointer.GetData(Index:Int32):Int64;
begin
        Result:=Base_Array.GetArrayData(Index+Offset);
end;

function TSBArrayPointer.GetAtPtr:Int64;
begin
        Result:=Base_Array.GetArrayData(Offset);
end;

procedure TSBArrayPointer.SetAtPtr(Data:Int64);
begin
        Base_Array.SetArrayData(Data,Offset);
end;

procedure TSBInt32ArrayPointer.SetData(Data:Int32;Index:Int32);
begin
  inherited SetData(Data,Index);
end;

function TSBInt32ArrayPointer.GetData(Index:Int32):Int32;
begin
        Result:=Int32(inherited GetData(Index));
end;

function TSBInt32ArrayPointer.GetAtPtr:Int32;
begin
        Result:=Int32(inherited GetAtPtr);
end;

procedure TSBInt32ArrayPointer.SetAtPtr(Data:Int32);
begin
        inherited SetAtPtr(Data);
end;                   

procedure TSBUInt32ArrayPointer.SetData(Data:UInt32;Index:Int32);
begin
  inherited SetData(Data,Index);
end;

function TSBUInt32ArrayPointer.GetData(Index:Int32):UInt32;
begin
        Result:=UInt32(inherited GetData(Index));
end;

function TSBUInt32ArrayPointer.GetAtPtr:UInt32;
begin
        Result:=UInt32(inherited GetAtPtr);
end;

procedure TSBUInt32ArrayPointer.SetAtPtr(Data:UInt32);
begin
        inherited SetAtPtr(Data);
end;

procedure TSBByteArrayPointer.SetData(Data:Byte;Index:Int32);
begin
  inherited SetData(Data,Index);
end;

function TSBByteArrayPointer.GetData(Index:Int32):Byte;
begin
        Result:=Byte(inherited GetData(Index));
end;

function TSBByteArrayPointer.GetAtPtr:Byte;
begin
        Result:=Byte(inherited GetAtPtr);
end;

////////////////////////////////////////////////////////////////////////////////
// Auxiliary functions

function IfThenInt(AValue: Boolean; const ATrue: Integer; const AFalse: Integer): Integer;
begin
  if AValue then
    Result := ATrue
  else
    Result := AFalse;
end;

function IfThenInt64(AValue: Boolean; const ATrue: Int64; const AFalse: Int64): Int64;
begin
  if AValue then
    Result := ATrue
  else
    Result := AFalse;
end;

function IfThenDouble(AValue: Boolean; const ATrue: Double; const AFalse: Double): Double;
begin
  if AValue then
    Result := ATrue
  else
    Result := AFalse;
end;

function IfThenLongword(AValue: Boolean; const ATrue: LongWord; const AFalse: LongWord): LongWord; 
begin
  if AValue then
    Result := ATrue
  else
    Result := AFalse;
end;

end.
