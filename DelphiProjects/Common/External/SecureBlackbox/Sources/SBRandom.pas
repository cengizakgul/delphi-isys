(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

unit SBRandom;

{$I SecBbox.inc}

interface

uses
{$ifndef CLX_USED}
  Classes,
  Windows
{$else}
  Classes
{$endif}
  , SysUtils
  ;

type
  TElRandom = class
  private
    S : array[0..255] of byte;
    CI, CJ : integer;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Randomize(Seed : AnsiString); overload;
{$ifndef CLX_USED}
    procedure Randomize(Stream : TStream; Count : integer = 0); overload;
{$endif}
    procedure Randomize(Buffer : pointer; Count : integer); overload;
    procedure Generate(Buffer : pointer; Count : integer); overload;
    function Generate(Count : integer) : AnsiString; overload;
    procedure Seed(Buffer: pointer; Count : integer);
{$ifndef CLX_USED}
    procedure Generate(Stream : TStream; Count : integer); overload;
{$endif}
  end;

implementation

uses
  Math;

type
  PByte = ^byte;

constructor TElRandom.Create;
var
{$ifndef CLX_USED}
  D : longword;
{$else}
  D : TDateTime;
{$endif}
begin
  inherited;
{$ifndef CLX_USED}
  D := GetTickCount;
{$else}
  D := Now;
{$endif}
  Randomize(@D, SizeOf(D));
end;

destructor TElRandom.Destroy;
begin
  inherited;
end;

procedure TElRandom.Randomize(Seed : Ansistring);
var
  I: Integer;
  C: Byte;
  K: array[0..255] of Byte;
  L, J: Word;
  P: Byte;
begin
  for I := 0 to 255 do
    S[I] := Byte(I);

  L := 1;
  J := 0;
  C := 0;
  while (J < 256) do
  begin
    K[J] := (Byte(Seed[L]) shr C) and 255;
    Inc(C);
    if C > 3 then
    begin
      Inc(L);
      C := 0;
    end;

    if (L > Length(Seed)) then L := 1;
    Inc(J);
  end;
  CJ := 0;
  for I := 0 to 255 do
  begin
    CJ := (CJ + S[I] + K[I]) mod 256;
    P := S[I];
    S[I] := S[CJ];
    S[CJ] := P;
  end;
  CJ := 0;
  CI := 0;
end;

{$ifndef CLX_USED}
procedure TElRandom.Randomize(Stream : TStream; Count : integer = 0);
var
  S : AnsiString;
begin
  if Count = 0 then
  begin
    Stream.Position := 0;
    Count := Stream.Size;
  end
  else
    Count := Min(integer(Stream.Size - Stream.Position), Count);

  SetLength(S, Count);

  Stream.ReadBuffer(S[1], Length(S));
  Randomize(S);
end;
{$endif}

procedure TElRandom.Randomize(Buffer : pointer; Count : integer);
var
  St: AnsiString;
begin
  SetLength(St, Count);
  Move(Buffer^, St[1], Length(St));
  Randomize(St);
end;

procedure TElRandom.Generate(Buffer : pointer; Count : integer);
var
  P: Byte;
  T: Word;
  Pb : PByte;
begin
  Pb := Buffer;
  while Count > 0 do
  begin
    CI := (Word(CI) + 1) mod 256;
    CJ := (Word(CJ) + Word(S[CI])) mod 256;
    P := S[CI];
    S[CI] := S[CJ];
    S[CJ] := P;
    T := (Word(S[CI]) + Word(S[CJ])) mod 256;
    Pb^ := S[T];
    Inc(Pb);
    Dec(Count);
  end;
end;


function TElRandom.Generate(Count : integer) : AnsiString;
begin
  SetLength(Result, Count);
  Generate(@Result[1], Count);
end;

procedure TElRandom.Seed(Buffer: pointer; Count : integer);
var
  I: Integer;
  C: Byte;
  K: array[0..255] of Byte;
  L, J: Word;
  P: Byte;
begin
  if Count = 0 then
    Exit;
  J := 0;
  L := 0;
  C := 0;
  while (J < 256) do
  begin
    K[J] := (Byte(PByteArray(Buffer)[L]) shr C) and 255;

    Inc(C);
    if C > 3 then
    begin
      Inc(L);
      C := 0;
    end;
    if (L >= Count) then
      L := 0;
    Inc(J);
  end;
  CJ := 0;
  for I := 0 to 255 do
  begin
    CJ := (CJ + S[I] + K[I]) mod 256;
    P := S[I];
    S[I] := S[CJ];
    S[CJ] := P;
  end;
  CJ := 0;
  CI := 0;
end;

{$ifndef CLX_USED}
procedure TElRandom.Generate(Stream : TStream; Count : integer);
var
  Buf : array of byte;
begin
  SetLength(Buf, Count);
  Generate(@Buf[0], Length(Buf));
  Stream.WriteBuffer(Buf[0], Length(Buf));
end;
{$endif}

end.
