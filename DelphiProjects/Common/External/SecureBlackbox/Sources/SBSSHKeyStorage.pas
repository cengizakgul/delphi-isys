(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSSHKeyStorage;

interface

uses
  SBUtils,
  SBRandom,
  SBSharedResource,
  SBX509,
  Classes
  ;


const
  ALGORITHM_RSA                 = Integer(0);
  ALGORITHM_DSS                 = Integer(1);
  ALGORITHM_NOT_SPECIFIED       = Integer($FFFF);

  SB_ERROR_SSH_KEYS_INVALID_PUBLIC_KEY          = Integer($0D01);
  SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY         = Integer($0D02);
  SB_ERROR_SSH_KEYS_FILE_READ_ERROR             = Integer($0D03);
  SB_ERROR_SSH_KEYS_FILE_WRITE_ERROR            = Integer($0D04);
  SB_ERROR_SSH_KEYS_UNSUPPORTED_ALGORITHM       = Integer($0D05);
  SB_ERROR_SSH_KEYS_INTERNAL_ERROR              = Integer($0D06);
  SB_ERROR_SSH_KEYS_BUFFER_TOO_SMALL            = Integer($0D07);
  SB_ERROR_SSH_KEYS_NO_PRIVATE_KEY              = Integer($0D08);
  SB_ERROR_SSH_KEYS_INVALID_PASSPHRASE          = Integer($0D09);
  SB_ERROR_SSH_KEYS_UNSUPPORTED_PEM_ALGORITHM   = Integer($0D0A);

type
  { Class for key management }
  TSBSSHKeyFormat = 
    (kfOpenSSH, kfIETF, kfPuTTY, kfX509);
  { kfIETF means private key in SSH.COM format, or public in IETF }

  TElSSHKey = class
  private
    FAlgorithm : integer;
    FKeyBlob : ByteArray;
    //FKeyBlobSC : ByteArray;
    FIsPrivate : boolean;
    FComment : string;
    FSubject : string;
    FHeaders : TStringList;
    FKeyFormat : TSBSSHKeyFormat;
    FRSAPublicExponent : string;
    FRSAPrivateExponent : string;
    FRSAPublicModulus : string;
    FDSSP : string;
    FDSSQ : string;
    FDSSG : string;
    FDSSY : string;
    FDSSX : string;
    FCert : TElX509Certificate;
    FSSHKeyBlob : ByteArray;
  protected
    procedure TrimParams;
    function ParsePuttyPrivateKey(Buffer : pointer; Size : integer; const
      Passphrase : string) : integer;
    function ReadLength(Buffer : pointer; Size : integer) : integer;
    function ParsePublicKey(Buffer : pointer; Size : integer) : boolean;
    function ParsePublicKeyIETF(Buffer : pointer; Size : integer) : boolean;
    function ParseIETFEnvelopedKey(Buffer : pointer; Size : integer;
      IsPublicKey: boolean = true; const Passphrase : string = ''): boolean;
    function ParsePrivateKey(Buffer : pointer; Size : integer; const Passphrase:
      string): integer;
    function ParsePublicKeyRSA(Buffer : pointer; Size : integer) : boolean;
    function ParsePublicKeyDSS(Buffer : pointer; Size : integer) : boolean;
    function GetKeyBlob : string;
    function ParsePrivateKeySSHCOM(Buffer: pointer; Size: integer;
      const Passphrase : string) : boolean;
    function GetFingerprintMD5 : TMessageDigest128;
    function GetFingerprintSHA1 : TMessageDigest160;
    function GetBits : integer;
    procedure RegenerateSSHBlob;
  public
    constructor Create;
    destructor Destroy; override;

    function IsKeyValid : boolean;
    procedure Copy(var Dest : TElSSHKey);
    function Generate(Algorithm : integer; Bits : integer) : integer;
    function LoadPublicKey(const PublicKeyFile : string) : integer; overload;
    function LoadPublicKey(Buffer: pointer; Size : integer) : integer; overload;
    function LoadPrivateKey(const PrivateKeyFile : string; const Passphrase : string {$ifdef HAS_DEF_PARAMS}= ''{$endif}) : integer; overload;
    function LoadPrivateKey(Buffer: pointer; Size : integer; const Passphrase : string = '') : integer; overload;
    function SavePublicKey(Buffer: pointer; var Size : integer) : integer; overload;
    function SavePublicKey(const PublicKeyFile : string) : integer; overload;
    function SavePrivateKey(Buffer: pointer; var Size : integer;
      const Passphrase : string = '') : integer; overload;
    function SavePrivateKey(const PrivateKeyFile : string; const Passphrase :
      string {$ifdef HAS_DEF_PARAMS}= ''{$endif})  : integer; overload;
    procedure Import(Certificate: TElX509Certificate);
    function LoadPublicKeyFromBlob(const AlgName: string; Buffer: pointer;
      Size : integer): integer;
    function SavePublicKeyToBlob(var AlgName : string; Buffer: pointer;
      var Size : integer): integer;
    property RSAPublicExponent : string read FRSAPublicExponent
      write FRSAPublicExponent;
    property RSAPublicModulus : string read FRSAPublicModulus
      write FRSAPublicModulus;
    property RSAPrivateExponent : string read FRSAPrivateExponent
      write FRSAPrivateExponent;
    property DSSP : string read FDSSP write FDSSP;
    property DSSQ : string read FDSSQ write FDSSQ;
    property DSSG : string read FDSSG write FDSSG;
    property DSSY : string read FDSSY write FDSSY;
    property DSSX : string read FDSSX write FDSSX;
    property FingerprintMD5 : TMessageDigest128 read GetFingerprintMD5;
    property FingerprintSHA1 : TMessageDigest160 read GetFingerprintSHA1;
    property Algorithm : integer read FAlgorithm write FAlgorithm;
    property Bits : integer read GetBits;
    property Comment : string read FComment write FComment;
    property Subject : string read FSubject write FSubject;
    property KeyFormat : TSBSSHKeyFormat read FKeyFormat write FKeyFormat
      default kfOpenSSH;
    property IsPrivate : boolean read FIsPrivate;
    property Headers : TStringList read FHeaders;
    property Certificate : TElX509Certificate read FCert;
  end;

  { Class to store a number of keys }
  TElSSHCustomKeyStorage = class(TSBControlBase)
  protected
    FSharedResource: TElSharedResource;

    function GetCount : integer; virtual;
    function GetKeys(Index : integer) : TElSSHKey; virtual;

    
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
    procedure Add(Key : TElSSHKey); overload; virtual;
    procedure Remove(Index : integer); virtual;
    procedure Clear; virtual;
    property Keys[Index : integer] : TElSSHKey read GetKeys;
    
  published
  
    property Count : integer read GetCount;
  end;

  TElSSHMemoryKeyStorage = class(TElSSHCustomKeyStorage)
  private
    FList : TList;
  protected
    function GetCount : integer; override;
    function GetKeys(Index : integer) : TElSSHKey; override;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
    procedure Add(Key : TElSSHKey); override;
    procedure Remove(Index : integer); override;
    procedure Clear; override;

  end;

procedure Register;

implementation

uses

  SysUtils,
  SBMath, SBDSA, SBRSA, SBPEM, SBMD, SBSHA, SBSSHUtils, SB3DES, SBAES, SBHMAC,
  SBSSHCommon
  ;


const
  CR = $0D;
  LF = $0A;
  COLON = ':';
  BACKSLASH = '\';
  CRLF : string = #10;
  DBLQUOTE : string = #$22;

  SSHCOMKEY_ORIG_MAGIC = $ebf96f3f;
  SSHCOMKEY_RSA_ID = 'if-modn{sign{rsa-pkcs1-sha1},encrypt{rsa-pkcs1v2-oaep}}';
  SSHCOMKEY_DSS_ID = 'dl-modp{sign{dsa-nist-sha1},dh{plain}}';
  SSHCOMKEY_DESEDE_ID = '3des-cbc';
  SSHCOMKEY_NONE_ID = 'none';
  PUTTYKEY_ID_V2 = 'PuTTY-User-Key-File-2: ';
  PUTTYKEY_ALG_RSA = 'ssh-rsa';
  PUTTYKEY_ALG_DSS = 'ssh-dss';
  PUTTYKEY_FIELD_ENCRYPTION = 'Encryption: ';
  PUTTYKEY_ENCRYPTION_AES = 'aes256-cbc';
  PUTTYKEY_ENCRYPTION_NONE = 'none';
  PUTTYKEY_FIELD_COMMENT = 'Comment: ';
  PUTTYKEY_FIELD_PUBLICLINES = 'Public-Lines: ';
  PUTTYKEY_FIELD_PRIVATELINES = 'Private-Lines: ';
  PUTTYKEY_FIELD_PRIVATEMAC = 'Private-MAC: ';
  PUTTYKEY_MAC_BEGIN = 'putty-private-key-file-mac-key';
  SSHKEY_BEGIN_MARKER = '---- BEGIN SSH2 PUBLIC KEY ----';
  SSHKEY_END_MARKER = '---- END SSH2 PUBLIC KEY ----';
  SSHKEY_PRIVATE_BEGIN_MARKER = '---- BEGIN SSH2 ENCRYPTED PRIVATE KEY ----';
  SSHKEY_PRIVATE_END_MARKER = '---- END SSH2 ENCRYPTED PRIVATE KEY ----';
  SSHKEY_PREFIX_RSA = #0#0#0#7'ssh-rsa';
  SSHKEY_PREFIX_DSS = #0#0#0#7'ssh-dss';
  SSHKEY_HEADER_SUBJECT = 'Subject: ';
  SSHKEY_HEADER_COMMENT = 'Comment: ';


procedure Register;
begin
  RegisterComponents('SSHBlackbox', [TElSSHMemoryKeyStorage]);
end;

//Some service functions
function SSHCOMDeriveKey(const Passphrase : string) : string;
var
  M128 : TMessageDigest128;
begin
  SetLength(Result, 16);
  M128 := HashMD5(Passphrase);
  Move(M128, Result[1], 16);
  M128 := HashMD5(Passphrase + Result);
  SetLength(Result, 24);
  Move(M128, Result[17], 8);
end;

{ builds PKCS blob, calculating needed D mod (P - 1) and D mod (Q - 1) }
function BuildPKCSRSABlob(const PublicModulus, PublicExponent, PrivateExponent,
  P, Q, U : string) : ByteArray;
var                                                      
  LP, LQ, LD, LRes : PLInt;
  DMP1, DMQ1 : string;
  Len : integer;
begin
  LCreate(LP);
  LCreate(LQ);
  LCreate(LD);
  LCreate(LRes);

  PointerToLInt(LP, @P[1], Length(P));
  LDec(LP);
  PointerToLInt(LQ, @Q[1], Length(Q));
  LDec(LQ);
  PointerToLInt(LD, @PrivateExponent[1], Length(PrivateExponent));
  LMod(LD, LP, LRes);
  SetLength(DMP1, LRes.Length * 4);
  LIntToPointer(LRes, @DMP1[1], Len);
  LMod(LD, LQ, LRes);
  SetLength(DMQ1, LRes.Length * 4);
  LIntToPointer(LRes, @DMQ1[1], Len);

  LDestroy(LP);
  LDestroy(LQ);
  LDestroy(LD);
  LDestroy(LRes);

  Len := 0;
  SBRSA.EncodePrivateKey(@PublicModulus[1], Length(PublicModulus),
    @PublicExponent[1], Length(PublicExponent), @PrivateExponent[1],
    Length(PrivateExponent), @P[1], Length(P), @Q[1], Length(Q), @DMP1[1],
    Length(DMP1), @DMQ1[1], Length(DMQ1), @U[1], Length(U), nil, Len);
  SetLength(Result, Len);
  SBRSA.EncodePrivateKey(@PublicModulus[1], Length(PublicModulus),
    @PublicExponent[1], Length(PublicExponent), @PrivateExponent[1],
    Length(PrivateExponent), @P[1], Length(P), @Q[1], Length(Q), @DMP1[1],
    Length(DMP1), @DMQ1[1], Length(DMQ1), @U[1], Length(U), @Result[0], Len);
  SetLength(Result, Len);
end;


function BuildPKCSDSSBlob(const P, Q, G, Y, X : string) : ByteArray;
var
  Len : integer;
begin
  Len := 0;
  SBDSA.EncodePrivateKey(@P[1], Length(P), @Q[1], Length(Q), @G[1],
    Length(G), @Y[1], Length(Y), @X[1], Length(X), nil, Len);
  SetLength(Result, Len);
  SBDSA.EncodePrivateKey(@P[1], Length(P), @Q[1], Length(Q), @G[1],
    Length(G), @Y[1], Length(Y), @X[1], Length(X), @Result[0], Len);
  SetLength(Result, Len);
end;

function PuTTYDeriveKey(const Passphrase : string) : string;
var Digest : TMessageDigest160;
    Ctx : TSHA1Context;
    Str : string;
begin
  SBSHA.InitializeSHA1(Ctx);
  Str := #0#0#0#0;
  SBSHA.HashSHA1(Ctx, @Str[1], 4);
  SBSHA.HashSHA1(Ctx, @Passphrase[1], Length(Passphrase));
  Digest := SBSHA.FinalizeSHA1(Ctx);
  Result := DigestToBinary(Digest);
  SBSHA.InitializeSHA1(Ctx);
  Str := #0#0#0#1;
  SBSHA.HashSHA1(Ctx, @Str[1], 4);
  SBSHA.HashSHA1(Ctx, @Passphrase[1], Length(Passphrase));
  Digest := SBSHA.FinalizeSHA1(Ctx);
  Result := TBufferTypeConst(Result) + DigestToBinary(Digest);
  SetLength(Result, 32);
end;

{ performs PuTTY's original HMAC }
function PuTTYHMACSimple(const Passphrase : string; const Data : ByteArray) : TMessageDigest160;
var
  Ctx1, Ctx2 : TSHA1Context;
  Foo : ByteArray;
  Index, Len : integer;
  TmpRes : TMessageDigest160;
  Value : string;
  Key : ByteArray;
begin
  { calculating MAC key }
  Value := PUTTYKEY_MAC_BEGIN + Passphrase;
  TmpRes := HashSHA1(@Value[1], Length(Value));
  SetLength(Key, 20);
  Move(TmpRes, Key[0], 20);

  { preparing contexts }
  SetLength(Foo, 64);

  Len := Length(Key);
  for Index := 0 to Len - 1 do
    Foo[Index] := Key[Index] xor $36;
  FillChar(Foo[Len], 64 - Len, $36);

  InitializeSHA1(Ctx1);
  HashSHA1(Ctx1, @Foo[0], 64);

  for Index := 0 to Len - 1 do
    Foo[Index] := Key[Index] xor $5C;
  FillChar(Foo[Len], 64 - Len, $5C);

  InitializeSHA1(Ctx2);
  HashSHA1(Ctx2, @Foo[0], 64);

  FillChar(Foo[0], 64, 0);

  { calculating HMAC }
  HashSHA1(Ctx1, @Data[0], Length(Data));
  TmpRes := FinalizeSHA1(Ctx1);
  HashSHA1(Ctx2, @TmpRes, 20);
  Result := FinalizeSHA1(Ctx2); 
end;

procedure SkipEOL(var P : PByte; var Size : integer);
begin
  while (Size > 0) and ((P^ = CR) or (P^ = LF)) do
  begin
    Inc(P);
    Dec(Size);
  end;
end;

function GetLine(P : PByte; Size : integer; var ColonFound : boolean) : integer;
begin
  Result := 0;
  ColonFound := false;
  while ((Size > 0) and (P^ <> CR) and (P^ <> LF)) do
  begin
    if P^ = Byte(COLON) then
      ColonFound := true;
    Inc(P);
    Dec(Size);
    Inc(Result);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TElCustomKeyStorage class

constructor TElSSHCustomKeyStorage.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  FSharedResource := TElSharedResource.Create;
end;


destructor TElSSHCustomKeyStorage.Destroy;
begin
  FreeAndNil(FSharedResource);
  inherited;
end;


procedure TElSSHCustomKeyStorage.Add(Key : TElSSHKey);
begin
end;

procedure TElSSHCustomKeyStorage.Remove(Index : integer);
begin
end;

procedure TElSSHCustomKeyStorage.Clear;
begin
end;

function TElSSHCustomKeyStorage.GetCount : integer;
begin
  Result := 0;
end;

function TElSSHCustomKeyStorage.GetKeys(Index : integer) : TElSSHKey;
begin
  Result := nil;
end;

////////////////////////////////////////////////////////////////////////////////
// TElMemoryKeyStorage class

constructor TElSSHMemoryKeyStorage.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  FList := TList.Create;
end;


destructor TElSSHMemoryKeyStorage.Destroy;
begin
  Clear;
  FList.Free;
  inherited;
end;


procedure TElSSHMemoryKeyStorage.Add(Key : TElSSHKey);
var
  NewKey : TElSSHKey;
begin
  CheckLicenseKey();
  if not Assigned(Key) then
    Exit;
  FSharedResource.WaitToWrite;
  try
    NewKey := TElSSHKey.Create;
    Key.Copy(NewKey);
    FList.Add(NewKey);
  finally
    FSharedResource.Done;
  end;
end;

procedure TElSSHMemoryKeyStorage.Remove(Index : integer);
var
  Key : TElSSHKey;
begin
  CheckLicenseKey();
  FSharedResource.WaitToWrite;
  try
    if Index < FList.Count then
    begin
      Key := FList.Items[Index];
      FList.Delete(Index);
      if Assigned(Key) then
        Key.Free;
    end;
  finally
    FSharedResource.Done;
  end;
end;

procedure TElSSHMemoryKeyStorage.Clear;
var
  P : TElSSHKey;
begin
  FSharedResource.WaitToWrite;
  try
    while FList.Count > 0 do
    begin
      P := FList.Items[0];
      FList.Delete(0);
      if P <> nil then
        P.Free;
    end;
  finally
    FSharedResource.Done;
  end;
end;

function TElSSHMemoryKeyStorage.GetCount : integer;
begin
  FSharedResource.WaitToRead;
  try
    Result := FList.Count;
  finally
    FSharedResource.Done;
  end;
end;

function TElSSHMemoryKeyStorage.GetKeys(Index : integer) : TElSSHKey;
begin
  FSharedResource.WaitToRead;
  try
    if Index < FList.Count then
      Result := FList.Items[Index]
    else
      Result := nil;
  finally
    FSharedResource.Done;
  end;
end;



////////////////////////////////////////////////////////////////////////////////
// TElSSHKey class

constructor TElSSHKey.Create;
begin
  inherited;
  SetLength(FRSAPublicModulus, 0);
  SetLength(FRSAPublicExponent, 0);
  SetLength(FRSAPrivateExponent, 0);
  SetLength(FDSSP, 0);
  SetLength(FDSSQ, 0);
  SetLength(FDSSG, 0);
  SetLength(FDSSY, 0);
  SetLength(FDSSX, 0);
  SetLength(FSSHKeyBlob, 0);
  FAlgorithm := ALGORITHM_NOT_SPECIFIED;
  FHeaders := TStringList.Create;
  FKeyFormat := kfOpenSSH;
  FIsPrivate := false;
  FCert := nil;
end;

destructor TElSSHKey.Destroy;
begin
  if FCert <> nil then
    FreeAndNil(FCert);
  FHeaders.Free;
  inherited;
end;

procedure TElSSHKey.Copy(var Dest : TElSSHKey);
begin
  CheckLicenseKey();
  if not Assigned(Dest) then
    Dest := TElSSHKey.Create;
  Dest.FAlgorithm := FAlgorithm;
  Dest.FIsPrivate := FIsPrivate;
  if FAlgorithm = ALGORITHM_RSA then
  begin
    SetLength(Dest.FRSAPublicModulus, Length(FRSAPublicModulus));
    Move(FRSAPublicModulus[1], Dest.FRSAPublicModulus[1], Length(FRSAPublicModulus));
    SetLength(Dest.FRSAPublicExponent, Length(FRSAPublicExponent));
    Move(FRSAPublicExponent[1], Dest.FRSAPublicExponent[1], Length(FRSAPublicExponent));
    SetLength(Dest.FRSAPrivateExponent, Length(FRSAPrivateExponent));
    Move(FRSAPrivateExponent[1], Dest.FRSAPrivateExponent[1], Length(FRSAPrivateExponent));
  end
  else if FAlgorithm = ALGORITHM_DSS then
  begin
    SetLength(Dest.FDSSP, Length(FDSSP));
    Move(FDSSP[1], Dest.FDSSP[1], Length(FDSSP));
    SetLength(Dest.FDSSQ, Length(FDSSQ));
    Move(FDSSQ[1], Dest.FDSSQ[1], Length(FDSSQ));
    SetLength(Dest.FDSSG, Length(FDSSG));
    Move(FDSSG[1], Dest.FDSSG[1], Length(FDSSG));
    SetLength(Dest.FDSSY, Length(FDSSY));
    Move(FDSSY[1], Dest.FDSSY[1], Length(FDSSY));
    SetLength(Dest.FDSSX, Length(FDSSX));
    Move(FDSSX[1], Dest.FDSSX[1], Length(FDSSX));
  end;
  SetLength(Dest.FKeyBlob, Length(FKeyBlob));
  Dest.FComment := FComment;
  Dest.FSubject := FSubject;
  Dest.FHeaders.Assign(FHeaders);
  Dest.FKeyFormat := FKeyFormat;
  if Assigned(FCert) then
  begin
    if Dest.FCert = nil then
      Dest.FCert := TElX509Certificate.Create(nil);
    FCert.Clone(Dest.FCert{$ifndef HAS_DEF_PARAMS}, true{$endif});
  end;
  Move(FKeyBlob[0], Dest.FKeyBlob[0], Length(FKeyBlob));
  SetLength(Dest.FSSHKeyBlob, Length(FSSHKeyBlob));
  Move(FSSHKeyBlob[0], Dest.FSSHKeyBlob[0], Length(FSSHKeyBlob));
end;

function TElSSHKey.IsKeyValid : boolean;
begin
  if IsPrivate then
  begin
    if Algorithm = ALGORITHM_RSA then
    begin
      Result := SBRSA.IsValidKey(@FKeyBlob[0], Length(FKeyBlob))
    end
    else if Algorithm = ALGORITHM_DSS then
      Result := true
    else
      Result := false;
  end
  else
    Result := true;
end;

function TElSSHKey.LoadPublicKey(const PublicKeyFile : string) : integer;
var
  F : TFileStream;
  Buf : array of byte;
begin
  try
    F := TFileStream.Create(PublicKeyFile, fmOpenRead or fmShareDenyNone);
  except
    Result := SB_ERROR_SSH_KEYS_FILE_READ_ERROR;
    Exit;
  end;
  SetLength(Buf, F.Size);
  F.Read(Buf[0], Length(Buf));
  F.Free;
  if not ParsePublicKey(@Buf[0], Length(Buf)) then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PUBLIC_KEY;
    Exit;
  end;
  RegenerateSSHBlob;
  Result := 0;
end;

function TElSSHKey.LoadPrivateKey(const PrivateKeyFile : string; const Passphrase : string {$ifdef HAS_DEF_PARAMS}= ''{$endif})  : integer;
var
  F : TFileStream;
  Buf : array of byte;
  Err : integer;
begin
  try
    F := TFileStream.Create(PrivateKeyFile, fmOpenRead or fmShareDenyNone);
  except
    Result := SB_ERROR_SSH_KEYS_FILE_READ_ERROR;
    Exit;
  end;
  SetLength(Buf, F.Size);
  F.Read(Buf[0], Length(Buf));
  F.Free;
  Err := ParsePrivateKey(@Buf[0], Length(Buf), Passphrase);
  Result := Err;
  if Result = 0 then
    RegenerateSSHBlob;
end;

function TElSSHKey.LoadPublicKey(Buffer: pointer; Size : integer) : integer;
begin
  if not ParsePublicKey(Buffer, Size) then
    Result := SB_ERROR_SSH_KEYS_INVALID_PUBLIC_KEY
  else
  begin
    RegenerateSSHBlob;
    Result := 0;
  end;
end;

function TElSSHKey.LoadPrivateKey(Buffer: pointer; Size : integer;
  const Passphrase : string = '') : integer;
begin
  Result := ParsePrivateKey(Buffer, Size, Passphrase);
  if Result = 0 then
    RegenerateSSHBlob;
end;

function TElSSHKey.ParsePublicKey(Buffer : pointer; Size : integer) : boolean;
const
  DSS_ID = 'ssh-dss ';
  RSA_ID = 'ssh-rsa ';
  BEGIN_MARKER = '---- B';
  SPACE  = $20;
  CR = $0d;
  LF = $0a;
var
  I, J : integer;
  DecodedKey : array of byte;
  Sz : integer;
begin
  CheckLicenseKey();
  SetLength(FKeyBlob, 0);
  FIsPrivate := false;
  FAlgorithm := ALGORITHM_NOT_SPECIFIED;
  SetLength(FComment, 0);
  SetLength(FSubject, 0);
  FHeaders.Clear;
  if Assigned(FCert) then
    FreeAndNil(FCert);
  Result := false;
  if Size < Length(DSS_ID) then
    Exit;
    
  if CompareMem(Buffer, @BEGIN_MARKER[1], Length(BEGIN_MARKER)) then
  begin
    Result := ParsePublicKeyIETF(Buffer, Size);
    Exit;
  end;
  FKeyFormat := kfOpenSSH;
  if CompareMem(Buffer, @DSS_ID[1], Length(DSS_ID)) then
    FAlgorithm := ALGORITHM_DSS
  else if CompareMem(Buffer, @RSA_ID[1], Length(RSA_ID)) then
    FAlgorithm := ALGORITHM_RSA
  else
  begin
    FAlgorithm := ALGORITHM_NOT_SPECIFIED;
    Exit;
  end;
  I := 8;
  while (I < Size) and (PByteArray(Buffer)[I] <> SPACE) do
    Inc(I);
  Sz := I - 8;
  SetLength(DecodedKey, Sz);
  if Base64Decode(@PByteArray(Buffer)[8], I - 8, @DecodedKey[0], Sz) <>
    BASE64_DECODE_OK then
    Exit;
  if FAlgorithm = ALGORITHM_DSS then
    Result := ParsePublicKeyDSS(@DecodedKey[0], Sz)
  else
    Result := ParsePublicKeyRSA(@DecodedKey[0], Sz);
  if Result then
  begin
    J := I + 1;
    while (J < Size) and (PByteArray(Buffer)[J] <> CR) and (PByteArray(Buffer)[J] <> LF) do
      Inc(J);
    if (J > I) then
    begin
      SetLength(FComment, J - I - 1);
      Move(PByteArray(Buffer)[I + 1], FComment[1], J - I - 1);
    end
    else
      SetLength(FComment, 0);
  end;
end;

function TElSSHKey.ParsePublicKeyIETF(Buffer : pointer; Size : integer) : boolean;
begin
  Result := ParseIETFEnvelopedKey(Buffer, Size {$ifndef HAS_DEF_PARAMS}, True, ''{$endif});
end;

// the following routine processes 4 dashes-armoured key formats (IETF public key
// and SSH.COM private key)
function TElSSHKey.ParseIETFEnvelopedKey(Buffer : pointer; Size : integer;
  IsPublicKey : boolean = true; const Passphrase : string = '') : boolean;
const
  STATE_FIRST_HEADER_LINE = 0;
  STATE_NEXT_HEADER_LINE  = 1;
  STATE_BASE64_DATA       = 2;
  BACKSLASH = $5c;
var
  Ptr : PByte;
  Sz : integer;
  ColonFound, EndMarkerFound : boolean;
  State : integer;
  HeaderLine, Chunk : string;
  Buf : ByteArray;
  BufSize: integer;
  I : integer;
  PrivateKey : boolean;
begin
  Result := false;

  if Size < Length(SSHKEY_PRIVATE_BEGIN_MARKER) then
    Exit;

  if CompareMem(@SSHKEY_BEGIN_MARKER[1], Buffer, Length(SSHKEY_BEGIN_MARKER)) then
  begin
    Ptr := @PByteArray(Buffer)[Length(SSHKEY_BEGIN_MARKER)];
    Dec(Size, Length(SSHKEY_BEGIN_MARKER));
    PrivateKey := false;
  end
  else if CompareMem(@SSHKEY_PRIVATE_BEGIN_MARKER[1], Buffer, Length(SSHKEY_PRIVATE_BEGIN_MARKER)) then
  begin
    Ptr := @PByteArray(Buffer)[Length(SSHKEY_PRIVATE_BEGIN_MARKER)];
    Dec(Size, Length(SSHKEY_PRIVATE_BEGIN_MARKER));
    PrivateKey := true;
  end
  else
    Exit;

  State := STATE_FIRST_HEADER_LINE;
  EndMarkerFound := false;

  while Size > 0 do
  begin
    SkipEOL(Ptr, Size);
    Sz := GetLine(Ptr, Size, ColonFound);
    if ((not PrivateKey) and (Sz >= Length(SSHKEY_END_MARKER)) and (CompareMem(Ptr, @SSHKEY_END_MARKER[1], Length(SSHKEY_END_MARKER)))) or
      ((PrivateKey) and (Sz >= Length(SSHKEY_PRIVATE_END_MARKER)) and (CompareMem(Ptr, @SSHKEY_PRIVATE_END_MARKER[1], Length(SSHKEY_PRIVATE_END_MARKER)))) then
    begin
      EndMarkerFound := true;
      Break;
    end;
    case State of
      STATE_FIRST_HEADER_LINE,
      STATE_NEXT_HEADER_LINE :
      begin
        if State = STATE_FIRST_HEADER_LINE then
        begin
          SetLength(HeaderLine, 0);
          if not ColonFound then
          begin
            State := STATE_BASE64_DATA;
            SetLength(Chunk, Sz);
            Move(Ptr^, Chunk[1], Sz);
            HeaderLine := HeaderLine + Chunk;
            Inc(Ptr, Sz);
            Continue;
          end;
        end;
        if (Sz > 0) then
        begin
          if PByteArray(Ptr)[Sz - 1] = BACKSLASH then
          begin
            SetLength(Chunk, Sz - 1);
            Move(Ptr^, Chunk[1], Sz - 1);
            HeaderLine := HeaderLine + Chunk;
            State := STATE_NEXT_HEADER_LINE;
          end
          else
          begin
            SetLength(Chunk, Sz);
            Move(Ptr^, Chunk[1], Sz);
            HeaderLine := HeaderLine + Chunk;
            FHeaders.Add(HeaderLine);
            State := STATE_FIRST_HEADER_LINE;
          end;
        end;
      end;
      STATE_BASE64_DATA :
      begin
        SetLength(Chunk, Sz);
        Move(Ptr^, Chunk[1], Sz);
        HeaderLine := HeaderLine + Chunk;
      end;
    end;
    Inc(Ptr, Sz);
  end;
  if not EndMarkerFound then
    Exit;
  BufSize := Length(HeaderLine);
  SetLength(Buf, BufSize);
  if Base64Decode(@HeaderLine[1], Length(HeaderLine), @Buf[0], BufSize) <> 0 then
    Exit;
  if not PrivateKey then
  begin
    if BufSize < Length(SSHKEY_PREFIX_RSA) then
      Exit;
    if CompareMem(@Buf[0], @SSHKEY_PREFIX_RSA[1], Length(SSHKEY_PREFIX_RSA)) then
    begin
      FAlgorithm := ALGORITHM_RSA;
      Result := ParsePublicKeyRSA(@Buf[0], BufSize);
    end
    else if CompareMem(@Buf[0], @SSHKEY_PREFIX_DSS[1], Length(SSHKEY_PREFIX_DSS)) then
    begin
      FAlgorithm := ALGORITHM_DSS;
      Result := ParsePublicKeyDSS(@Buf[0], BufSize);
    end
    else
    begin
      FAlgorithm := ALGORITHM_NOT_SPECIFIED;
      Result := false;
    end;
  end
  else // it's a private key
  begin
    Result := ParsePrivateKeySSHCOM(@Buf[0],
      BufSize, Passphrase);
  end;

  if Result then
  begin
    FKeyFormat := kfIETF;
    for I := 0 to FHeaders.Count - 1 do
    begin
      if Pos(SSHKEY_HEADER_SUBJECT, FHeaders.Strings[I]) = 1 then
        FSubject := System.Copy(FHeaders.Strings[I], Length(SSHKEY_HEADER_SUBJECT) + 1,
          Length(FHeaders.Strings[I]))
      else
      if Pos(SSHKEY_HEADER_COMMENT, FHeaders.Strings[I]) = 1 then
        FComment := System.Copy(FHeaders.Strings[I], Length(SSHKEY_HEADER_COMMENT) + 1,
          Length(FHeaders.Strings[I]));
    end;
  end;
end;


function TElSSHKey.ParsePublicKeyRSA(Buffer : pointer; Size : integer) : boolean;
const
  RSA_ID = #0#0#0#7'ssh-rsa';
var
  LenE, LenM : integer;
begin
  Result := false;
  FRSAPublicModulus := '';
  FRSAPublicExponent := '';
  if not CompareMem(Buffer, @RSA_ID[1], Length(RSA_ID)) then
    Exit;
  LenE := ReadLength(@PByteArray(Buffer)[11], Size - 11);
  if (LenE < 0) or (Size - 15 < LenE) then
    Exit;
  SetLength(FRSAPublicExponent, LenE);
  Move(PByteArray(Buffer)[15], FRSAPublicExponent[1], LenE);
  LenM := ReadLength(@PByteArray(Buffer)[15 + LenE], Size - 15 - LenE);
  if (LenM < 0) or (Size - 19 - LenE < LenM) then
    Exit;
  SetLength(FRSAPublicModulus, LenM);
  Move(PByteArray(Buffer)[19 + LenE], FRSAPublicModulus[1], LenM);
  TrimParams;
  Result := true;
end;

function TElSSHKey.ParsePublicKeyDSS(Buffer : pointer; Size : integer) : boolean;
const
  DSS_ID = #0#0#0#7'ssh-dss';
var
  Len, Index : integer;
begin
  FDSSP := '';
  FDSSQ := '';
  FDSSG := '';
  FDSSY := '';
  Result := false;
  if not CompareMem(Buffer, @DSS_ID[1], Length(DSS_ID)) then
    Exit;
  Index := 11;
  Len := ReadLength(@PByteArray(Buffer)[Index], Size - Index);
  Inc(Index, 4);
  if (Len < 0) or (Size - Index < Len) then
    Exit;
  SetLength(FDSSP, Len);
  Move(PByteArray(Buffer)[Index], FDSSP[1], Len);
  Inc(Index, Len);
  Len := ReadLength(@PByteArray(Buffer)[Index], Size - Index);
  Inc(Index, 4);
  if (Len < 0) or (Size - Index < Len) then
    Exit;
  SetLength(FDSSQ, Len);
  Move(PByteArray(Buffer)[Index], FDSSQ[1], Len);
  Inc(Index, Len);
  Len := ReadLength(@PByteArray(Buffer)[Index], Size - Index);
  Inc(Index, 4);
  if (Len < 0) or (Size - Index < Len) then
    Exit;
  SetLength(FDSSG, Len);
  Move(PByteArray(Buffer)[Index], FDSSG[1], Len);
  Inc(Index, Len);
  Len := ReadLength(@PByteArray(Buffer)[Index], Size - Index);
  Inc(Index, 4);
  if (Len < 0) or (Size - Index < Len) then
    Exit;
  SetLength(FDSSY, Len);
  Move(PByteArray(Buffer)[Index], FDSSY[1], Len);
  TrimParams;
  Result := true;
end;

function TElSSHKey.ReadLength(Buffer : pointer; Size : integer) : integer;
const
  Offset = 0;
begin
  if Size < 4 then
    Result := -1
  else
    Result := (PByteArray(Buffer)[Offset] shl 24) or (PByteArray(Buffer)[Offset + 1] shl 16) or
      (PByteArray(Buffer)[Offset + 2] shl 8) or PByteArray(Buffer)[Offset + 3];
end;

function TElSSHKey.ParsePuttyPrivateKey(Buffer : pointer; Size : integer; const
  Passphrase : string) : integer;

  function ParseFieldString(Buf : pointer; Size : integer; const FieldName : string;
    var Res : string) : integer;
  var
    Pos, Len : integer;
    Colon : boolean;
  begin
    Result := 0;
    Pos := GetLine(Buf, Size, Colon);
    Len := Length(FieldName);
    if (Pos < Len) or (not CompareMem(Buf, @FieldName[1], Len)) then
      Exit;

    SetLength(Res, Pos - Len);
    Inc(PtrUInt(Buf), Len);
    Move(Buf^, Res[1], Pos - Len);
    Inc(PtrUInt(Buf), Pos - Len);

    Pos := Size - Pos;

    SkipEOL(PByte(Buf), Pos);
    Result := Size - Pos; //number of bytes, used in this pair.
  end;

  function ParseBase64Lines(Buf : pointer; Size : integer; Lines : integer; var Res : string) : integer;
  var Len, Pos, Line : integer;
      Colon : boolean;
  begin
    Result := Size;
    Res := '';

    for Line := 1 to Lines do
    begin
      Pos := GetLine(Buf, Size, Colon);
      Len := Length(Res);
      SetLength(Res, Len + Pos);
      Move(Buf^, Res[Len + 1], Pos);
      Inc(PtrUInt(Buf), Pos);
      Dec(Size, Pos);
      SkipEOL(PByte(Buf), Size);
    end;

    Result := Result - Size;    
  end;

  function  PuTTYDecryptKeyBlob(Blob : pointer; Size : integer; KeyStr : string) : boolean;
  var
    Ctx : TAESContext256;
    Key : TAESKey256;
    IV : TAESBuffer;
  begin
    Result := false;
    if Size mod 16 > 0 then
      Exit;
    Move(KeyStr[1], Key, 32);
    FillChar(IV, SizeOf(IV), 0);
    SBAES.InitializeDecryptionCBC256(Ctx, Key, IV);

    if not SBAES.DecryptCBC256(Ctx, Blob, Cardinal(Size), Blob, Cardinal(Size)) then
      Exit;
    Result := true;
  end;

  function PuTTYParsePrivateKeyBlob(Blob : pointer; Size : integer) : boolean;
  var
    //NewLen : integer;
    P, Q, U : string;
  begin
    Result := false;

    if  FAlgorithm = ALGORITHM_RSA then
    begin
      try
        { Private exponent, D }
        FRSAPrivateExponent := ReadString(Blob, Size);
        Inc(PtrUInt(Blob), Length(FRSAPrivateExponent) + 4);
        Dec(Size, Length(FRSAPrivateExponent) + 4);
        { P }
        P := ReadString(Blob, Size);
        Inc(PtrUInt(Blob), Length(P) + 4);
        Dec(Size, Length(P) + 4);
        { Q }
        Q := ReadString(Blob, Size);
        Inc(PtrUInt(Blob), Length(Q) + 4);
        Dec(Size, Length(Q) + 4);
        { U }
        U := ReadString(Blob, Size);
        Inc(PtrUInt(Blob), Length(U) + 4);

        TrimParams;
        { forming blob }
        FKeyBlob := BuildPKCSRSABlob(FRSAPublicModulus, FRSAPublicExponent,
          FRSAPrivateExponent, P, Q, U);
        if Length(FKeyBlob) > 0 then
          Result := true;
      except
        Exit;
      end;    
    end
    else if FAlgorithm = ALGORITHM_DSS then
    begin
      try
        { X }
        FDSSX := ReadString(Blob, Size);
        Inc(PtrUInt(Blob), Length(FDSSX) + 4);

        TrimParams;
        { forming blob }
        FKeyBlob := BuildPKCSDSSBlob(FDSSP, FDSSQ, FDSSG, FDSSY, FDSSX);
        if Length(FKeyBlob) > 0 then
          Result := true;
      except
        Exit;
      end;
    end;
  end;
var
  Len, PubLines, PrivLines : integer;
  Value : string;
  Encrypted, Mac : boolean;
  PublicBlob, PrivateBlob, Tmp : ByteArray;
  Digest, CalcDigest : TMessageDigest160;
begin
  { reading ID line, with key algorithm }
  Len := ParseFieldString(Buffer, Size, PUTTYKEY_ID_V2, Value);
  if Len = 0 then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;

  if (Length(Value) = Length(PUTTYKEY_ALG_RSA)) and
    CompareMem(@Value[1], @PUTTYKEY_ALG_RSA[1], Length(PUTTYKEY_ALG_RSA)) then
    FAlgorithm := ALGORITHM_RSA
  else if (Length(Value) = Length(PUTTYKEY_ALG_DSS)) and
    CompareMem(@Value[1], @PUTTYKEY_ALG_DSS[1], Length(PUTTYKEY_ALG_DSS)) then
    FAlgorithm := ALGORITHM_DSS
  else
  begin
    Result := SB_ERROR_SSH_KEYS_UNSUPPORTED_ALGORITHM;
    Exit;
  end;

  Inc(PtrUInt(Buffer), Len);
  Dec(Size, Len);

  { private blob encryption type }
  Len := ParseFieldString(Buffer, Size, PUTTYKEY_FIELD_ENCRYPTION, Value);
  if Len = 0 then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;

  if (Length(Value) = Length(PUTTYKEY_ENCRYPTION_AES)) and
    CompareMem(@Value[1], @PUTTYKEY_ENCRYPTION_AES[1],
    Length(PUTTYKEY_ENCRYPTION_AES)) then
    Encrypted := true
  else if (Length(Value) = Length(PUTTYKEY_ENCRYPTION_NONE)) and
    CompareMem(@Value[1], @PUTTYKEY_ENCRYPTION_NONE[1],
    Length(PUTTYKEY_ENCRYPTION_NONE)) then
    Encrypted := false
  else
  begin
    Result := SB_ERROR_SSH_KEYS_UNSUPPORTED_ALGORITHM;
    Exit;
  end;

  Inc(PtrUInt(Buffer), Len);
  Dec(Size, Len);

  { key comment }
  Len := ParseFieldString(Buffer, Size, PUTTYKEY_FIELD_COMMENT, Value);
  if Len = 0 then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;
  FComment := Value;

  Inc(PtrUInt(Buffer), Len);
  Dec(Size, Len);

  { number of lines in public blob }
  Len := ParseFieldString(Buffer, Size, PUTTYKEY_FIELD_PUBLICLINES, Value);
  if Len = 0 then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;

  Inc(PtrUInt(Buffer), Len);
  Dec(Size, Len);

  Val(Value, PubLines, Len);
  if Len <> 0 then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;

  { reading and parsing public lines }
  Len := ParseBase64Lines(Buffer, Size, PubLines, Value);
  if Len = 0 then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;

  Inc(PtrUInt(Buffer), Len);
  Dec(Size, Len);

  Len := 0;
  Base64Decode(@Value[1], Length(Value), nil, Len);
  SetLength(PublicBlob, Len);
  if (Base64Decode(@Value[1], Length(Value), @PublicBlob[0], Len) <> BASE64_DECODE_OK) then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;
  SetLength(PublicBlob, Len);

  if (FAlgorithm = ALGORITHM_RSA) then
  begin
    if not ParsePublicKeyRSA(@PublicBlob[0], Length(PublicBlob)) then
    begin
      Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
      Exit;
    end;
  end
  else if FAlgorithm = ALGORITHM_DSS then
  begin
    if not ParsePublicKeyDSS(@PublicBlob[0], Length(PublicBlob)) then
    begin
      Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
      Exit;
    end;
  end;

  { private blob lines }
  Len := ParseFieldString(Buffer, Size, PUTTYKEY_FIELD_PRIVATELINES, Value);
  if Len = 0 then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;

  Inc(PtrUInt(Buffer), Len);
  Dec(Size, Len);

  Val(Value, PrivLines, Len);
  if Len <> 0 then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;

  { reading, Base64-decoding and parsing private lines }
  Len := ParseBase64Lines(Buffer, Size, PrivLines, Value);

  if Len = 0 then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;

  Inc(PtrUInt(Buffer), Len);
  Dec(Size, Len);

  Len := 0;
  Base64Decode(@Value[1], Length(Value), nil, Len);
  SetLength(PrivateBlob, Len);
  if (Base64Decode(@Value[1], Length(Value), @PrivateBlob[0], Len) <> BASE64_DECODE_OK) then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;
  SetLength(PrivateBlob, Len);

  { decrypting }
  if Encrypted then
  begin
    if (Len mod 16 > 0) then
    begin
      Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
      Exit;
    end;
    if Length(Passphrase) = 0 then
    begin
      Result := SB_ERROR_SSH_KEYS_INVALID_PASSPHRASE;
      Exit;
    end;

    Value := PuTTYDeriveKey(Passphrase);
    if not PuTTYDecryptKeyBlob(@PrivateBlob[0], Len, Value) then
    begin
      Result := SB_ERROR_SSH_KEYS_INVALID_PASSPHRASE;
      Exit;
    end;
  end;

  { parsing decrypted private key blob }
  PuTTYParsePrivateKeyBlob(@PrivateBlob[0], Len);

  { reading HMAC }
  Len := ParseFieldString(Buffer, Size, PUTTYKEY_FIELD_PRIVATEMAC, Value);
  if Len = 0 then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;
  Mac := true;
  { calculating & checking HMAC }
  if not StrToDigest(Value, Digest) then
  begin
    Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
    Exit;
  end;

  if Mac then
  begin
    Value := '';
    if FAlgorithm = ALGORITHM_RSA then
      Value := WriteString(PUTTYKEY_ALG_RSA)
    else if FAlgorithm = ALGORITHM_DSS then
      Value := WriteString(PUTTYKEY_ALG_DSS);
    if Encrypted then
      Value := Value + WriteString(PUTTYKEY_ENCRYPTION_AES)
    else
      Value := Value + WriteString(PUTTYKEY_ENCRYPTION_NONE);

    Value := TBufferTypeConst(Value) + WriteString(FComment);
    Value := TBufferTypeConst(Value) + WriteUINT32(Length(PublicBlob));
    Len := Length(Value);
    SetLength(Value, Len + Length(PublicBlob));
    Move(PublicBlob[0], Value[Len + 1], Length(PublicBlob));
    Value := Value + WriteUINT32(Length(PrivateBlob));
    Len := Length(Value);
    SetLength(Value, Len + Length(PrivateBlob));
    Move(PrivateBlob[0], Value[Len + 1], Length(PrivateBlob));

    SetLength(Tmp, Length(Value));
    Move(Value[1], Tmp[0], Length(Value));

    if not Encrypted then
      CalcDigest := PuTTYHMACSimple('', Tmp)
    else
      CalcDigest := PuTTYHMACSimple(Passphrase, Tmp);

    If not CompareMD160(CalcDigest, Digest) then
    begin
      Result := SB_ERROR_SSH_KEYS_INVALID_PASSPHRASE;
      Exit;
    end;
  end;

  FKeyFormat := kfPuTTY;
  FIsPrivate := true;
  Result := 0;
end;

function TElSSHKey.ParsePrivateKey(Buffer : pointer; Size : integer; const
  Passphrase : string): integer;
var
  Buf : ByteArray;
  Header : string;
  Sz, Err : integer;
  PSize, QSize, GSize, XSize, YSize : integer;
const
  SSH_COM_MARKER: string = '---- B';
begin
  CheckLicenseKey();
  FHeaders.Clear;
  SetLength(FComment, 0);
  SetLength(FSubject, 0);
  SetLength(FKeyBlob, 0);
  FKeyFormat := kfOpenSSH;
  FDSSX := '';
  FRSAPrivateExponent := '';
  if Assigned(FCert) then
    FreeAndNil(FCert);

  if (Size >= Length(SSH_COM_MARKER)) and (CompareMem(@SSH_COM_MARKER[1], Buffer,
    Length(SSH_COM_MARKER))) then
  begin
    if ParseIETFEnvelopedKey(Buffer, Size, false, Passphrase) then
      Result := 0
    else
      Result := SB_ERROR_SSH_KEYS_INVALID_PASSPHRASE;
  end
  else
  if (Size > Length(PUTTYKEY_ID_V2)) and
    (CompareMem(@PUTTYKEY_ID_V2[1], Buffer, Length(PUTTYKEY_ID_V2))) then
  begin
    Result := ParsePuttyPrivateKey(Buffer, Size, Passphrase);
  end
  else
  begin
    Sz := Size;
    SetLength(Buf, Sz);
    Err := SBPEM.Decode(Buffer, Size, @Buf[0], Passphrase, Sz, Header);
    case Err of
      PEM_DECODE_RESULT_INVALID_FORMAT:
        Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
      PEM_DECODE_RESULT_INVALID_PASSPHRASE:
        Result := SB_ERROR_SSH_KEYS_INVALID_PASSPHRASE;
      PEM_DECODE_RESULT_NOT_ENOUGH_SPACE:
        Result := SB_ERROR_SSH_KEYS_INTERNAL_ERROR;
      PEM_DECODE_RESULT_UNKNOWN_CIPHER:
        Result := SB_ERROR_SSH_KEYS_UNSUPPORTED_PEM_ALGORITHM;
      PEM_DECODE_RESULT_OK :
        Result := 0;
    else
      Result := Err;
    end;
    if Result <> 0 then
      Exit;
    if CompareStr(Header, 'DSA PRIVATE KEY') = 0 then
    begin
      FAlgorithm := ALGORITHM_DSS;
      PSize := 0;
      QSize := 0;
      GSize := 0;
      YSize := 0;
      XSize := 0;
      SBDSA.DecodePrivateKey(@Buf[0], Sz, nil, PSize, nil, QSize, nil, GSize,
        nil, YSize, nil, XSize);
      SetLength(FDSSP, PSize);
      SetLength(FDSSQ, QSize);
      SetLength(FDSSG, GSize);
      SetLength(FDSSY, YSize);
      SetLength(FDSSX, XSize);
      if not SBDSA.DecodePrivateKey(@Buf[0], Sz, @FDSSP[1], PSize, @FDSSQ[1], QSize,
        @FDSSG[1], GSize, @FDSSY[1], YSize, @FDSSX[1], XSize) then
      begin
        Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
        Exit;
      end;
      SetLength(FKeyBlob, Sz);
      Move(Buf[0], FKeyBlob[0], Sz);
      Result := 0;
    end
    else if CompareStr(Header, 'RSA PRIVATE KEY') = 0 then
    begin
      FAlgorithm := ALGORITHM_RSA;
      XSize := 0;
      YSize := 0;
      PSize := 0;
      SBRSA.DecodePrivateKey(@Buf[0], Sz, nil, PSize, nil, YSize, nil, XSize);
      SetLength(FRSAPublicModulus, PSize);
      SetLength(FRSAPublicExponent, YSize);
      SetLength(FRSAPrivateExponent, XSize);
      if not SBRSA.DecodePrivateKey(@Buf[0], Sz, @FRSAPublicModulus[1], PSize,
        @FRSAPublicExponent[1], YSize, @FRSAPrivateExponent[1], XSize) then
      begin
        Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
        Exit;
      end;

      TrimParams;

      SetLength(FKeyBlob, Sz);
      Move(Buf[0], FKeyBlob[0], Sz);
      Result := 0;
    end
    else
      Result := SB_ERROR_SSH_KEYS_UNSUPPORTED_ALGORITHM;
  end;
  if Result = 0 then
    FIsPrivate := true;
end;

function WriteInt(const S : string) : string;
var
  Size : integer;
begin
  Size := Length(S);
  if (Length(S) > 0) and (PByte(@S[1])^ >= $80) then
  begin
    Inc(Size);
    Result := Chr(Size shr 24) + Chr((Size shr 16) and $FF) + Chr((Size shr 8) and $FF) +
      Chr(Size and $FF) + #0 + S;
  end
  else
    Result := Chr(Size shr 24) + Chr((Size shr 16) and $FF) + Chr((Size shr 8) and $FF) +
      Chr(Size and $FF) + S;
end;

function TElSSHKey.GetKeyBlob : string;
begin
  Result := '';
  if FAlgorithm = ALGORITHM_RSA then
  begin
    Result := #0#0#0#7'ssh-rsa';
    Result := Result + WriteInt(FRSAPublicExponent) + WriteInt(FRSAPublicModulus);
  end
  else if FAlgorithm = ALGORITHM_DSS then
  begin
    Result := #0#0#0#7'ssh-dss';
    Result := Result + WriteInt(FDSSP) + WriteInt(FDSSQ) + WriteInt(FDSSG) +
      WriteInt(FDSSY);
  end;
end;

function TElSSHKey.GetFingerprintMD5 : TMessageDigest128;
begin
  Result := HashMD5(GetKeyBlob);
end;

function TElSSHKey.GetFingerprintSHA1 : TMessageDigest160;
begin
  Result := HashSHA1(GetKeyBlob);
end;

function TElSSHKey.GetBits : integer;
begin
  if FAlgorithm = ALGORITHM_RSA then
    Result := Length(FRSAPublicModulus) shl 3
  else if FAlgorithm = ALGORITHM_DSS then
    Result := Length(FDSSP) shl 3
  else
    Result := 0;
end;

function TElSSHKey.Generate(Algorithm : integer; Bits : integer) : integer;
var
  ASize, BSize, CSize, DSize, ESize, FSize : integer;
  A, B, C, D, E, F : ByteArray;
begin
  CheckLicenseKey();
  SetLength(FComment, 0);
  SetLength(FSubject, 0);
  FHeaders.Clear;
  if Algorithm = ALGORITHM_RSA then
  begin
    Result := SB_ERROR_SSH_KEYS_INTERNAL_ERROR;
    ASize := 0;
    SBRSA.Generate(Bits, nil, ASize, nil, BSize, nil, CSize, nil, DSize);
    SetLength(A, ASize);
    SetLength(B, BSize);
    SetLength(C, CSize);
    SetLength(D, DSize);
    if not SBRSA.Generate(Bits, @A[0], ASize, @B[0], BSize, @C[0], CSize, @D[0], DSize) then
      Exit;
    SetLength(FKeyBlob, DSize);
    Move(D[0], FKeyBlob[0], DSize);
    SetLength(FRSAPublicExponent, BSize);
    Move(B[0], FRSAPublicExponent[1], BSize);
    SetLength(FRSAPrivateExponent, CSize);
    Move(C[0], FRSAPrivateExponent[1], CSize);
    SetLength(FRSAPublicModulus, ASize);
    Move(A[0], FRSAPublicModulus[1], ASize);
    FAlgorithm := ALGORITHM_RSA;
    Result := 0;
  end
  else if Algorithm = ALGORITHM_DSS then
  begin
    Result := SB_ERROR_SSH_KEYS_INTERNAL_ERROR;
    ASize := 0;
    SBDSA.Generate(Bits, nil, ASize, nil, BSize, nil, CSize, nil, DSize, nil,
      ESize, nil, FSize);
    SetLength(A, ASize);
    SetLength(B, BSize);
    SetLength(C, CSize);
    SetLength(D, DSize);
    SetLength(E, ESize);
    SetLength(F, FSize);
    if not SBDSA.Generate(Bits, @A[0], ASize, @B[0], BSize, @C[0], CSize, @D[0],
      DSize, @E[0], ESize, @F[0], FSize) then
      Exit;
    SetLength(FKeyBlob, FSize);
    Move(F[0], FKeyBlob[0], FSize);
    SetLength(FDSSP, ASize);
    Move(A[0], FDSSP[1], ASize);
    SetLength(FDSSQ, BSize);
    Move(B[0], FDSSQ[1], BSize);
    SetLength(FDSSG, CSize);
    Move(C[0], FDSSG[1], CSize);
    SetLength(FDSSY, DSize);
    Move(D[0], FDSSY[1], DSize);
    SetLength(FDSSX, ESize);
    Move(E[0], FDSSX[1], ESize);
    FAlgorithm := ALGORITHM_DSS;
    Result := 0;
  end
  else
    Result := SB_ERROR_SSH_KEYS_UNSUPPORTED_ALGORITHM;
  if Result = 0 then
  begin
    TrimParams;
    RegenerateSSHBlob;
    FIsPrivate := true;
  end;
end;

function TElSSHKey.SavePublicKey(Buffer: pointer; var Size : integer) : integer;
  function EstimateArmorSize : integer;
  const
    BEGIN_MARKER_LENGTH = 31;
    END_MARKER_LENGTH = 29;
  begin
    if (FKeyFormat = kfIETF) or
       (FKeyFormat = kfPuTTY) then
    begin
      Result := Length(FHeaders.Text);
      Inc(Result, Result div 72 + 1);
      Inc(Result, BEGIN_MARKER_LENGTH + END_MARKER_LENGTH);
      Inc(Result, 32);
    end
    else
      Result := 0;
  end;
  procedure SetupIETFHeaders;
  var
    I : integer;
    S : string;
    SubjectFound, CommentFound : boolean;
  begin
    SubjectFound := false;
    CommentFound := false;
    for I := 0 to FHeaders.Count - 1 do
    begin
      if Pos(SSHKEY_HEADER_SUBJECT, FHeaders.Strings[I]) = 1 then
      begin
        S := System.Copy(FHeaders.Strings[I], Length(SSHKEY_HEADER_SUBJECT) + 1,
          Length(FHeaders.Strings[I]));
        if CompareStr(S, FSubject) <> 0 then
          FHeaders.Strings[I] := SSHKEY_HEADER_SUBJECT + FSubject;
        SubjectFound := true;
      end
      else
      if Pos(SSHKEY_HEADER_COMMENT, FHeaders.Strings[I]) = 1 then
      begin
        S := System.Copy(FHeaders.Strings[I], Length(SSHKEY_HEADER_COMMENT) + 1,
          Length(FHeaders.Strings[I]));
        if CompareStr(S, FComment) <> 0 then
          FHeaders.Strings[I] := SSHKEY_HEADER_COMMENT + FComment;
        CommentFound := true;
      end
    end;
    if (not SubjectFound) and (Length(FSubject) > 0) then
      FHeaders.Add((SSHKEY_HEADER_SUBJECT) + FSubject);
    if (not CommentFound) and (Length(FComment) > 0) then
      FHeaders.Add((SSHKEY_HEADER_COMMENT) + FComment);
  end;
  
  function WriteHeader(Header : string; var Ptr : PByte; var Size : integer) : boolean;
  var
    FormattedHeader : string;
    Index : integer;
  begin
    Result := false;
    Index := Pos(COLON, Header);
    if (Index > 65) or (Length(Header) - Index > 1024) then
      Exit; // skipping this invalid header

    SetLength(FormattedHeader, 0);
    while Length(Header) > 72 do
    begin
      FormattedHeader := FormattedHeader + System.Copy(Header, 1, 71) +
        BACKSLASH + CRLF;
      Header := System.Copy(Header, 72, Length(Header));
    end;
    FormattedHeader := FormattedHeader + Header + CRLF;
    if Size < Length(FormattedHeader) then
      Result := false
    else
    begin
      Move(FormattedHeader[1], Ptr^, Length(FormattedHeader));
      Inc(Ptr, Length(FormattedHeader));
      Dec(Size, Length(FormattedHeader));
      Result := true;
    end;
  end;
  function WriteBeginMarker(var Ptr : PByte; var Size : integer) : boolean;
  begin
    if Size < Length(SSHKEY_BEGIN_MARKER) + 1 then
      Result := false
    else
    begin
      Move(SSHKEY_BEGIN_MARKER[1], Ptr^, Length(SSHKEY_BEGIN_MARKER));
      Inc(Ptr, Length(SSHKEY_BEGIN_MARKER));
      Move(CRLF[1], Ptr^, 1);
      Inc(Ptr, 1);
      Dec(Size, Length(SSHKEY_BEGIN_MARKER) + 1);
      Result := true;
    end;
  end;
  function WriteEndMarker(var Ptr : PByte; var Size : integer) : boolean;
  begin
    if Size < Length(SSHKEY_END_MARKER) + 1 then
      Result := false
    else
    begin
      Move(SSHKEY_END_MARKER[1], Ptr^, Length(SSHKEY_END_MARKER));
      Inc(Ptr, Length(SSHKEY_END_MARKER));
      Move(CRLF[1], Ptr^, 1);
      Inc(Ptr, 1);
      Dec(Size, Length(SSHKEY_END_MARKER) + 1);
      Result := true;
    end;
  end;
const
  RSA_ID = #0#0#0#7'ssh-rsa';
  DSS_ID = #0#0#0#7'ssh-dss';
  RSA_PREFIX = 'ssh-rsa ';
  DSS_PREFIX = 'ssh-dss ';
var
  EstimatedSize, BlobSize : integer;
  Buf : string;
  WrapBase64 : boolean;
  Index, I : integer;
  Ptr: PByte;
  SizeLeft : integer;
begin
  if FKeyFormat = kfOpenSSH then
    WrapBase64 := false
  else
  begin
    WrapBase64 := true;
    SetupIETFHeaders;
  end;
  if FAlgorithm = ALGORITHM_RSA then
  begin
    EstimatedSize := (21 + Length(FRSAPublicModulus) + Length(FRSAPublicExponent)) * 4 div 3 +
      10 + Length(FComment) + 32;
    for I := 0 to FHeaders.Count - 1 do
      Inc(EstimatedSize, Length(FHeaders[I]) + 2);
    Inc(EstimatedSize, 2);
    if WrapBase64 then
      Inc(EstimatedSize, EstimatedSize shr 5 + 1);
    EstimatedSize := EstimatedSize + EstimateArmorSize;
    if Size < EstimatedSize then
    begin
      Size := EstimatedSize;
      Result := SB_ERROR_SSH_KEYS_BUFFER_TOO_SMALL;
      Exit;
    end;
    Buf := RSA_ID + WriteInt(FRSAPublicExponent) + WriteInt(FRSAPublicModulus);
  end
  else if FAlgorithm = ALGORITHM_DSS then
  begin
    EstimatedSize := (31 + Length(FDSSP) + Length(FDSSQ) + Length(FDSSP) + Length(FDSSY)) * 4 div 3 +
      10 + Length(FComment) + 32;
    for I := 0 to FHeaders.Count - 1 do
      Inc(EstimatedSize, Length(FHeaders[I]) + 2);
    Inc(EstimatedSize, 2);
    if WrapBase64 then
      Inc(EstimatedSize, EstimatedSize shr 5 + 1);
    EstimatedSize := EstimatedSize + EstimateArmorSize;
    if Size < EstimatedSize then
    begin
      Size := EstimatedSize;
      Result := SB_ERROR_SSH_KEYS_BUFFER_TOO_SMALL;
      Exit;
    end;
    Buf := DSS_ID + WriteInt(FDSSP) + WriteInt(FDSSQ) + WriteInt(FDSSG) +
      WriteInt(FDSSY);
  end
  else
  begin
    Result := SB_ERROR_SSH_KEYS_UNSUPPORTED_ALGORITHM;
    Exit;
  end;

  if FKeyFormat = kfOpenSSH then
  begin
    BlobSize := Size - 9;
    Index := 8;
  end
  else
  begin
    Ptr := Buffer;
    SizeLeft := Size;
    if not WriteBeginMarker(Ptr, SizeLeft) then
    begin
      Result := SB_ERROR_SSH_KEYS_INTERNAL_ERROR;
      Exit;
    end;
    for I := 0 to FHeaders.Count - 1 do
      if not WriteHeader(FHeaders.Strings[I], Ptr, SizeLeft) then
      begin
        Result := SB_ERROR_SSH_KEYS_INTERNAL_ERROR;
        Exit;
      end;
    BlobSize := SizeLeft;
    Index := PtrUInt(Ptr) - PtrUInt(Buffer);
  end;
  if not Base64Encode(@Buf[1], Length(Buf), @PByteArray(Buffer)[Index], BlobSize, WrapBase64) then
  begin
    Size := BlobSize + 11;
    Result := SB_ERROR_SSH_KEYS_BUFFER_TOO_SMALL;
    Exit;
  end;
  if FKeyFormat = kfOpenSSH then
  begin
    if FAlgorithm = ALGORITHM_RSA then
      Move(RSA_PREFIX[1], Buffer^, Length(RSA_PREFIX))
    else
      Move(DSS_PREFIX[1], Buffer^, Length(DSS_PREFIX));
    Size := BlobSize + 9;
    if Length(FComment) > 0 then
    begin
      PByteArray(Buffer)[Size - 1] := $20;
      Move(FComment[1], PByteArray(Buffer)[Size], Length(FComment));
      Inc(Size, Length(FComment) + 1);
    end;
    PByteArray(Buffer)[Size - 1] := $0a;
  end
  else
  begin
    Ptr := @PByteArray(Buffer)[Index + BlobSize];
    Dec(SizeLeft, BlobSize);
    if SizeLeft < 1 then
    begin
      Result := SB_ERROR_SSH_KEYS_INTERNAL_ERROR;
      Exit;
    end;
    Ptr^ := PByte(@CRLF[1])^;
    Inc(Ptr);
    Dec(SizeLeft);
    if not WriteEndMarker(Ptr, SizeLeft) then
    begin
      Result := SB_ERROR_SSH_KEYS_INTERNAL_ERROR;
      Exit;
    end;
    Dec(Size, SizeLeft);
  end;
  Result := 0;
end;

function TElSSHKey.SavePrivateKey(Buffer: pointer; var Size : integer;
  const Passphrase : string = '') : integer;
  procedure EncryptBlob(const Key: string; var Blob : string);
  const
    IV : T3DESBuffer = 
     (
      0, 0, 0, 0, 0, 0, 0, 0
     );
  var
    Context : T3DESContext;
    Sz : cardinal;
  begin
    SB3DES.InitializeEncryptionCBC(Context, P3DESKey(@Key[1])^, IV);
    Sz := Length(Blob);
    SB3DES.EncryptCBC(Context, @Blob[1], Sz, @Blob[1], Sz);
  end;

  function ConvertPKCSBlobToSSHCom : string;
  var
    MSize, ESize, DSize, P1Size, P2Size, E1Size, E2Size, USize, PSize, QSize,
      GSize, XSize, YSize : integer;
    M, E, D, P, P1, P2, E1, E2, U, B, Q, G, X, Y : string;
    function WriteMPIntSSHCOM(const S : string) : string;
    var
      I : integer;
      Bits : cardinal;
      B : byte;
      P : pointer;
      Size : integer;
    begin
      P := @S[1];
      Size := Length(S);
      I := 0;
      while (PByteArray(P)[I] = 0) and (I < Size) do
        Inc(I);
      if I < Size then
      begin
        B := PByteArray(P)[I];
        Bits := 0;
        while B <> 0 do
        begin
          B := B shr 1;
          Inc(Bits);
        end;
        Bits := Bits + cardinal((Size - I - 1) * 8);
      end
      else
        Bits := 0;

      Result := Chr(Bits shr 24) + Chr((Bits shr 16) and $ff) +
        Chr((Bits shr 8) and $ff) + Chr(Bits and $ff);
      Result := Result + System.Copy(S, I + 1, Size - I);
    end;
  begin
    SetLength(Result, 0);
    if Length(FKeyBlob) > 0 then
    begin
      if FAlgorithm = ALGORITHM_RSA then
      begin
        MSize := 0; ESize := 0; DSize := 0; P1Size := 0; P2Size := 0;
        E1Size := 0; E2Size := 0; USize := 0;
        SBRSA.DecodePrivateKey(@FKeyBlob[0], Length(FKeyBlob), nil, MSize,
          nil, ESize, nil, DSize, nil, P1Size, nil, P2Size, nil, E1Size,
          nil, E2Size, nil, USize);
        SetLength(M, MSize); SetLength(E, ESize); SetLength(D, DSize);
        SetLength(P1, P1Size); SetLength(P2, P2Size); SetLength(E1, E1Size);
        SetLength(E2, E2Size); SetLength(U, USize);
        if SBRSA.DecodePrivateKey(@FKeyBlob[0], Length(FKeyBlob), @M[1], MSize,
          @E[1], ESize, @D[1], DSize, @P1[1], P1Size, @P2[1], P2Size, @E1[1], E1Size,
          @E2[1], E2Size, @U[1], USize) then
        begin
          SetLength(M, MSize); SetLength(E, ESize); SetLength(D, DSize);
          SetLength(P1, P1Size); SetLength(P2, P2Size); SetLength(U, USize);
          B := WriteMPIntSSHCOM(E) + WriteMPIntSSHCOM(D) + WriteMPIntSSHCOM(M) +
            WriteMPIntSSHCOM(U) + WriteMPIntSSHCOM(P1) + WriteMPIntSSHCOM(P2);

          SetLength(Result, Length(B));
          Move(B[1], Result[1], Length(Result));
        end;
      end
      else if FAlgorithm = ALGORITHM_DSS then
      begin
        SetLength(B, 0);

        PSize := 0; QSize := 0; GSize := 0; YSize := 0; XSize := 0;
        SBDSA.DecodePrivateKey(@FKeyBlob[0], Length(FKeyBlob), nil, PSize, nil,
          QSize, nil, GSize, nil, YSize, nil, XSize);
        SetLength(P, PSize); SetLength(Q, QSize); SetLength(G, GSize);
        SetLength(X, XSize); SetLength(Y, YSize);
        if SBDSA.DecodePrivateKey(@FKeyBlob[0], Length(FKeyBlob), @P[1], PSize,
            @Q[1], QSize, @G[1], GSize, @Y[1], YSize, @X[1], XSize) then
        begin
          SetLength(P, PSize); SetLength(Q, QSize); SetLength(G, GSize);
          SetLength(X, XSize); SetLength(Y, YSize);

          B := TBufferTypeConst(WriteMPIntSSHCOM(P)) + WriteMPIntSSHCOM(G) + WriteMPIntSSHCOM(Q)
            + WriteMPIntSSHCOM(Y) + WriteMPIntSSHCOM(X);
          SetLength(Result, Length(B));
          Move(B[1], Result[1], Length(Result));
        end;
      end;
    end;
  end;

  function ConvertPKCSBlobToPuTTY : string;
  var
    MSize, ESize, DSize, P1Size, P2Size, E1Size, E2Size, USize : integer;
    M, E, D, P1, P2, E1, E2, U, B, Tmp : string;
    LP, LQ, LU, LTmp1 : PLInt;
    Ind : integer;
  begin
    SetLength(Result, 0);
    if Length(FKeyBlob) > 0 then
    begin
      if FAlgorithm = ALGORITHM_RSA then
      begin
        MSize := 0; ESize := 0; DSize := 0; P1Size := 0; P2Size := 0;
        E1Size := 0; E2Size := 0; USize := 0;
        SBRSA.DecodePrivateKey(@FKeyBlob[0], Length(FKeyBlob), nil, MSize,
          nil, ESize, nil, DSize, nil, P1Size, nil, P2Size, nil, E1Size,
          nil, E2Size, nil, USize);
        SetLength(M, MSize); SetLength(E, ESize); SetLength(D, DSize);
        SetLength(P1, P1Size); SetLength(P2, P2Size); SetLength(E1, E1Size);
        SetLength(E2, E2Size); SetLength(U, USize);
        if SBRSA.DecodePrivateKey(@FKeyBlob[0], Length(FKeyBlob), @M[1], MSize,
          @E[1], ESize, @D[1], DSize, @P1[1], P1Size, @P2[1], P2Size, @E1[1], E1Size,
          @E2[1], E2Size, @U[1], USize) then
        begin
          SetLength(M, MSize); SetLength(E, ESize); SetLength(D, DSize);
          SetLength(P1, P1Size); SetLength(P2, P2Size); SetLength(U, USize);

          LCreate(LP); LCreate(LQ);
          PointerToLInt(LP, @P1[1], P1Size);
          PointerToLInt(LQ, @P2[1], P2Size);
          if LGreater(LQ, LP) then
          begin
            LSwap(LP, LQ);
            LCreate(LU);
            LCreate(LTmp1);
            LGCD(LQ, LP, LTmp1, LU);

            Tmp := P1;
            P1 := P2;
            P2 := Tmp;

            SetLength(Tmp, LU.Length * 4 + 1);

            Tmp[1] := #0;
            LIntToPointer(LU, @Tmp[2], USize);
            Inc(USize);
            Ind := 2;
            while (Tmp[Ind] = #0) and (Ind < USize) do Inc(Ind);

            if Tmp[Ind] >= #$80 then Dec(Ind);

            USize := USize - Ind + 1;
            SetLength(U, USize);
            Move(Tmp[Ind], U[1], USize);
            LDestroy(LTmp1);
            LDestroy(LU);
          end;

          LDestroy(LP); LDestroy(LQ);

          B := TBufferTypeConst(WriteString(D)) + WriteString(P1) + WriteString(P2) + WriteString(U);

          SetLength(Result, Length(B));
          Move(B[1], Result[1], Length(Result));
        end;
      end
      else if FAlgorithm = ALGORITHM_DSS then
      begin
        B := WriteString(FDSSX);
        SetLength(Result, Length(B));
        Move(B[1], Result[1], Length(Result));
      end;
    end;
  end;

  function  PuTTYEncryptKeyBlob(Blob : pointer; Size : integer; KeyStr : string) : boolean;
  var
    Ctx : TAESContext256;
    Key : TAESKey256;
    IV : TAESBuffer;
  begin
    Result := false;
    if Size mod 16 > 0 then
      Exit;
    Move(KeyStr[1], Key, 32);
    FillChar(IV, SizeOf(IV), 0);
    SBAES.InitializeEncryptionCBC256(Ctx, Key, IV);
    if not SBAES.EncryptCBC256(Ctx, Blob, Cardinal(Size), Blob, Cardinal(Size)) then
      Exit;
    Result := true;
  end;

  function WrapHeaderItem(ItemName, ItemValue : string) : string;
  var Len, HLen, Index : integer;
  const WRAP_LEN = 70;
  begin
    Result := '';
    Len := Length(ItemValue);
    HLen := Length(ItemName); //must be < 70, and include ' :'.
    if Len > 0 then
    begin
      if Len < WRAP_LEN - HLen then
        Result := ItemName + ItemValue
      else
      begin
        Index := 0;
        Result := ItemName + DBLQUOTE + System.Copy(ItemValue, Index, WRAP_LEN - HLen - 2) + BACKSLASH + CRLF;
        Index := WRAP_LEN - HLen - 2;
        while Len - Index > WRAP_LEN - 1 do
        begin
          Result := Result + System.Copy(ItemValue, Index, WRAP_LEN - 1) + BACKSLASH + CRLF;
          Index := Index + WRAP_LEN - 1;
        end;
        Result := Result + System.Copy(ItemValue, Index, Len - Index) + DBLQUOTE;
      end;
      Result := Result + CRLF;
    end;
  end;

var
  Key, KeyType, CipherType, RawBlob, PubBlob, PrivBlob, Blob : string;
  Index, NewLen, Len, Lines : integer;
  Header, S, Res : string;
  Value: string;
  Encrypt : boolean;
  MAC : TMessageDigest160;
  Tmp : ByteArray;
begin
  if Length(FKeyBlob) = 0 then
  begin
    Result := SB_ERROR_SSH_KEYS_NO_PRIVATE_KEY;
    Exit;
  end;

  if FKeyFormat = kfOpenSSH then
  begin
    if FAlgorithm = ALGORITHM_RSA then
      Header := 'RSA PRIVATE KEY'
    else if FAlgorithm = ALGORITHM_DSS then
      Header := 'DSA PRIVATE KEY'
    else
    begin
      Result := SB_ERROR_SSH_KEYS_UNSUPPORTED_ALGORITHM;
      Exit;
    end;
    Encrypt := Length(Passphrase) > 0;
    if SBPEM.Encode(@FKeyBlob[0], Length(FKeyBlob), Buffer, Size, Header, Encrypt,
      Passphrase) then
      Result := 0
    else
      Result := SB_ERROR_SSH_KEYS_BUFFER_TOO_SMALL;
  end
  else
  if FKeyFormat = kfPuTTY then
  begin
    Res := PUTTYKEY_ID_V2;
    if FAlgorithm = ALGORITHM_RSA then
      Res := Res + PUTTYKEY_ALG_RSA
    else if FAlgorithm = ALGORITHM_DSS then
      Res := Res + PUTTYKEY_ALG_DSS
    else
    begin
      Result := SB_ERROR_SSH_KEYS_UNSUPPORTED_ALGORITHM;
      Exit;
    end;
    Res := Res + CRLF;

    Res := Res + PUTTYKEY_FIELD_ENCRYPTION;
    if Length(Passphrase) > 0 then
      Res := Res + PUTTYKEY_ENCRYPTION_AES
    else
      Res := Res + PUTTYKEY_ENCRYPTION_NONE;
    Res := Res + CRLF + PUTTYKEY_FIELD_COMMENT + FComment + CRLF +
      PUTTYKEY_FIELD_PUBLICLINES;

    { forming binary public-key blob }  
    Blob := '';  
    if FAlgorithm = ALGORITHM_RSA then
    begin
      PubBlob := WriteString(PUTTYKEY_ALG_RSA) + WriteString(FRSAPublicExponent) +
        WriteString(FRSAPublicModulus);
    end
    else if FAlgorithm = ALGORITHM_DSS then
    begin
      PubBlob := WriteString(PUTTYKEY_ALG_DSS) + WriteString(FDSSP) +
         WriteString(FDSSQ) +  WriteString(FDSSG) +  WriteString(FDSSY);
    end;

    { writing Base64 - encoded blob to result }
    Len := 0;
    Base64Encode(@PubBlob[1], Length(PubBlob), nil, Len, false);
    SetLength(Blob, Len);
    if Base64Encode(@PubBlob[1], Length(PubBlob), @Blob[1], Len, false) then
    begin
      SetLength(Blob, Len);
      Lines := (Length(Blob) + 63) div 64; //number of lines in public blob
       Str(Lines, S); 
      Res := Res + S + CRLF;
      for Index := 1 to Lines - 1 do
        Res := Res + System.Copy(Blob, (Index - 1) * 64 + 1, 64) + CRLF;

      Res := Res + System.Copy(Blob, (Lines - 1) * 64 + 1,
        Length(Blob) - (Lines - 1) * 64) + CRLF;
    end
    else
    begin
      Result := SB_ERROR_SSH_KEYS_INTERNAL_ERROR;
      Exit;
    end;

    Res := Res + PUTTYKEY_FIELD_PRIVATELINES;

    { forming binary private-key blob }
    PrivBlob := ConvertPKCSBlobToPuTTY;
    { padding blob to blocklength of the cipher, if encrypted }
    if Length(Passphrase) > 0 then
    begin
      Len := Length(PrivBlob);
      if Len mod 16 > 0 then
      begin
        NewLen := 16 - Len mod 16;
        SetLength(PrivBlob, Len + NewLen);
        { randomizing added bytes }
        SBRndGenerate(@PrivBlob[Len + 1], NewLen);
        SBRndSeedTime;
      end;
    end;
    { calculating HMAC, before encrypting blob }
    if FAlgorithm = ALGORITHM_RSA then
      Value := WriteString(PUTTYKEY_ALG_RSA)
    else if FAlgorithm = ALGORITHM_DSS then
      Value := WriteString(PUTTYKEY_ALG_DSS);
    if Length(Passphrase) > 0  then
      Value := Value + WriteString(PUTTYKEY_ENCRYPTION_AES)
    else
      Value := Value + WriteString(PUTTYKEY_ENCRYPTION_NONE);

    Value := TBufferTypeConst(Value) + WriteString(FComment) + WriteString(PubBlob) +
      WriteString(PrivBlob);

    SetLength(Tmp, Length(Value));
    Move(Value[1], Tmp[0], Length(Value));

    MAC := PuTTYHMACSimple(Passphrase, Tmp);

    { encrypting blob if needed }
    if Length(Passphrase) > 0 then
    begin
      Key := PuTTYDeriveKey(Passphrase);
      if not PuTTYEncryptKeyBlob(@PrivBlob[1], Length(PrivBlob), Key) then
      begin
        Result := SB_ERROR_SSH_KEYS_INTERNAL_ERROR;
        Exit;
      end;
    end;

    { writing Base64 - encoded blob to result }
    Len := 0;
    Base64Encode(@PrivBlob[1], Length(PrivBlob), nil, Len, false);
    SetLength(Blob, Len);
    if Base64Encode(@PrivBlob[1], Length(PrivBlob), @Blob[1], Len, false) then
    begin
      SetLength(Blob, Len);
      Lines := (Length(Blob) + 63) div 64; //number of lines in public blob
       Str(Lines, S); 
      Res := Res + S + CRLF;
      for Index := 1 to Lines - 1 do
        Res := Res + System.Copy(Blob, (Index - 1) * 64 + 1, 64) + CRLF;
      Res := Res + System.Copy(Blob, (Lines - 1) * 64 + 1,
        Length(Blob) - (Lines - 1) * 64) + CRLF;
    end
    else
    begin
      Result := SB_ERROR_SSH_KEYS_INTERNAL_ERROR;
      Exit;
    end;

    Res := Res + PUTTYKEY_FIELD_PRIVATEMAC;

    Res := Res + DigestToStr(MAC {$ifndef HAS_DEF_PARAMS}, true{$endif}) + CRLF;

    if Size < Length(Res) then
    begin
      Size := Length(Res);
      Result := SB_ERROR_SSH_KEYS_BUFFER_TOO_SMALL;
      Exit;
    end;

    Move(Res[1], Buffer^, Length(Res));
    Size := Length(Res);
    Result := 0    
  end
  else
  if FKeyFormat = kfIETF then
  begin
    if FAlgorithm = ALGORITHM_RSA then
      KeyType := (SSHCOMKEY_RSA_ID)
    else if FAlgorithm = ALGORITHM_DSS then
      KeyType := (SSHCOMKEY_DSS_ID)
    else
    begin
      Result := SB_ERROR_SSH_KEYS_UNSUPPORTED_ALGORITHM;
      Exit;
    end;

    RawBlob := ConvertPKCSBlobToSSHCom;

    if Length(RawBlob) = 0 then
    begin
      Result := SB_ERROR_SSH_KEYS_INVALID_PRIVATE_KEY;
      Exit;
    end;

    { writing encrypted stuff length + (sometimes) initial zero }
    if FAlgorithm = ALGORITHM_DSS then
      RawBlob := TBufferTypeConst(WriteUINT32(0)) + RawBlob;
    RawBlob := WriteString(RawBlob);  

    if Length(Passphrase) > 0 then //encrypting blob
    begin
      { padding to 8-byte boundary }
      if (Length(RawBlob) mod 8) <> 0 then
      begin
        Index := Length(RawBlob);
        NewLen := 8 - Index mod 8;
        SetLength(RawBlob, Index + NewLen);
        { randomizing added bytes }
        SBRndGenerate(@RawBlob[Index + 1], NewLen);
        SBRndSeedTime;
      end;
      Key := SSHCOMDeriveKey(Passphrase);
      EncryptBlob(Key, RawBlob);
      CipherType := SSHCOMKEY_DESEDE_ID;
    end
    else
      CipherType := SSHCOMKEY_NONE_ID;

    SetLength(Blob, 8);
    { MAGIC int }
    Blob[4] := Chr(SSHCOMKEY_ORIG_MAGIC shr 24);
    Blob[3] := Chr((SSHCOMKEY_ORIG_MAGIC shr 16) and $ff);
    Blob[2] := Chr((SSHCOMKEY_ORIG_MAGIC shr 8) and $ff);
    Blob[1] := Chr(SSHCOMKEY_ORIG_MAGIC and $ff);

    Blob := TBufferTypeConst(Blob) + WriteString(KeyType) + WriteString(CipherType) +
      WriteString(RawBlob);

    Len := Length(Blob);

    { Total blob length }
    Blob[5] := Chr(Len shr 24);
    Blob[6] := Chr((Len shr 16) and $ff);
    Blob[7] := Chr((Len shr 8) and $ff);
    Blob[8] := Chr(Len and $ff);

    Res := (SSHKEY_PRIVATE_BEGIN_MARKER) + CRLF;
    Res := Res + WrapHeaderItem((SSHKEY_HEADER_SUBJECT), Subject);
    Res := Res + WrapHeaderItem((SSHKEY_HEADER_COMMENT), Comment);

    NewLen := 0; 
    Base64Encode(@Blob[1], Length(Blob), nil, NewLen);
    Index := Length(Res);
    SetLength(Res, Index + NewLen);
    Base64Encode(@Blob[1], Length(Blob), @Res[Index + 1], NewLen);
    SetLength(Res, Index + NewLen);
    Res := Res + CRLF + SSHKEY_PRIVATE_END_MARKER + CRLF;

    if Size < Length(Res) then
    begin
      Size := Length(Res);
      Result := SB_ERROR_SSH_KEYS_BUFFER_TOO_SMALL;
      Exit;
    end;

    Move(Res[1], Buffer^, Length(Res));
    Size := Length(Res);
    Result := 0
  end
  else
    Result := SB_ERROR_SSH_KEYS_INTERNAL_ERROR; 
end;

function TElSSHKey.SavePublicKey(const PublicKeyFile : string) : integer;
var
  Buf : ByteArray;
  Size : integer;
  F : TFileStream;
begin
  Size := 0;
  SavePublicKey(nil, Size);
  SetLength(Buf, Size);
  Result := SavePublicKey(@Buf[0], Size);
  F := nil;
  if Result = 0 then
  begin
    try
      try
        F := TFileStream.Create(PublicKeyFile, fmCreate);
        F.Write(Buf[0], Size);
      finally
        if F <> nil then
          F.Free;
      end;
    except
      Result := SB_ERROR_SSH_KEYS_FILE_WRITE_ERROR;
      Exit;
    end;
  end;
end;

function TElSSHKey.SavePrivateKey(const PrivateKeyFile : string; 
    const Passphrase: string {$ifdef HAS_DEF_PARAMS}= ''{$endif}) : integer;
var
  Buf : ByteArray;
  Size : integer;
  F : TFileStream;
begin
  Size := 0;
  SavePrivateKey(nil, Size, Passphrase);
  SetLength(Buf, Size);
  Result := SavePrivateKey(@Buf[0], Size, Passphrase);
  F := nil;
  if Result = 0 then
  begin
    try
      try
        F := TFileStream.Create(PrivateKeyFile, fmCreate);
        F.Write(Buf[0], Size);
      finally
        if F <> nil then
          F.Free;
      end;
    except
      Result := SB_ERROR_SSH_KEYS_FILE_WRITE_ERROR;
      Exit;
    end;
  end;
end;


function TElSSHKey.ParsePrivateKeySSHCOM(Buffer: pointer; Size: integer;
  const Passphrase : string) : boolean;
var
  Magic, BlobSize : cardinal;
  Ptr : ^byte;
  KeyType, CipherType, EncryptedBlob: string;
  Len : integer;
  Key : string;
  P, Q, U : string;

  procedure DecryptBlob(const Key: string; var Blob : string);
  const
    IV : T3DESBuffer = 
     (
      0, 0, 0, 0, 0, 0, 0, 0
     );
  var
    Context : T3DESContext;
    Sz : cardinal;
  begin
    SB3DES.InitializeDecryptionCBC(Context, P3DESKey(@Key[1])^, IV);
    Sz := Length(Blob);
    SB3DES.DecryptCBC(Context, @Blob[1], Sz, @Blob[1], Sz);
  end;

  function ReadMPIntSSHCOM(Buffer : pointer; Size : integer) : string;
  var
    Bits : integer;
  begin
    if Size < 4 then
      raise EElSSHException.Create(SInvalidSize);
    Bits := (PByteArray(Buffer)[0] shl 24) or (PByteArray(Buffer)[1] shl 16) or
      (PByteArray(Buffer)[2] shl 8) or PByteArray(Buffer)[3];
    SetLength(Result, (Bits + 7) div 8);
    if Size - 4 < Length(Result) then
      raise EElSSHException.Create(SInvalidSize);
    Move(PByteArray(Buffer)[4], Result[1], Length(Result));
  end;
begin
  Result := false;
  if Size < 8 then Exit;
  Magic := PLongword(Buffer)^;
  BlobSize := (PByteArray(Buffer)[4] shl 24) or (PByteArray(Buffer)[5] shl 16) or
    (PByteArray(Buffer)[6] shl 8) or PByteArray(Buffer)[7];
  if (Magic <> SSHCOMKEY_ORIG_MAGIC) or (integer(BlobSize) <> Size) then
    Exit;
  Ptr := @PByteArray(Buffer)[8];
  Dec(Size, 8);
  try
    { Key type }
    KeyType := ReadString(Ptr, Size);
    Len := Length(KeyType) + 4;
    Inc(Ptr, Len);
    Dec(Size, Len);
    { Cipher type }
    CipherType := ReadString(Ptr, Size);
    Len := Length(CipherType) + 4;
    Inc(Ptr, Len);
    Dec(Size, Len);
    { Encrypted blob }
    EncryptedBlob := ReadString(Ptr, Size);
  except
    Exit;
  end;
  if CompareStr(KeyType, SSHCOMKEY_RSA_ID) = 0 then
    FAlgorithm := ALGORITHM_RSA
  else if CompareStr(KeyType, SSHCOMKEY_DSS_ID) = 0 then
    FAlgorithm := ALGORITHM_DSS
  else
    Exit;

  if CompareStr(CipherType, SSHCOMKEY_DESEDE_ID) = 0 then
  begin
    if Length(EncryptedBlob) mod 8 <> 0 then
      Exit;
    Key := SSHCOMDeriveKey(Passphrase);
    DecryptBlob(Key, EncryptedBlob);
  end
  else if CompareStr(CipherType, SSHCOMKEY_NONE_ID) <> 0 then
    Exit;
  try
    EncryptedBlob := ReadString(@EncryptedBlob[1], Length(EncryptedBlob));

    if FAlgorithm = ALGORITHM_RSA then
    begin
      Ptr := @EncryptedBlob[1];
      Size := Length(EncryptedBlob);
      { E }
      FRSAPublicExponent := ReadMPIntSSHCOM(Ptr, Size);
      Len := Length(FRSAPublicExponent) + 4;
      Inc(Ptr, Len);
      Dec(Size, Len);
      { D }
      FRSAPrivateExponent := ReadMPIntSSHCOM(Ptr, Size);
      Len := Length(FRSAPrivateExponent) + 4;
      Inc(Ptr, Len);
      Dec(Size, Len);
      { M }
      FRSAPublicModulus := ReadMPIntSSHCOM(Ptr, Size);
      Len := Length(FRSAPublicModulus) + 4;
      Inc(Ptr, Len);
      Dec(Size, Len);
      { U }
      U := ReadMPIntSSHCOM(Ptr, Size);
      Len := Length(U) + 4;
      Inc(Ptr, Len);
      Dec(Size, Len);
      { P }
      P := ReadMPIntSSHCOM(Ptr, Size);
      Len := Length(P) + 4;
      Inc(Ptr, Len);
      Dec(Size, Len);
      { Q }
      Q := ReadMPIntSSHCOM(Ptr, Size);

      TrimParams;
      FKeyBlob := BuildPKCSRSABlob(FRSAPublicModulus, FRSAPublicExponent,
        FRSAPrivateExponent, P, Q, U);
    end
    else if FAlgorithm = ALGORITHM_DSS then
    begin
      Ptr := @EncryptedBlob[1];
      Size := Length(EncryptedBlob);
      Magic := ReadLength(Ptr, Size);
      if Magic <> 0 then
        Exit;
      Inc(Ptr, 4);
      Dec(Size, 4);
      { P }
      FDSSP := ReadMPIntSSHCOM(Ptr, Size);
      Len := Length(FDSSP) + 4;
      Inc(Ptr, Len);
      Dec(Size, Len);
      { G }
      FDSSG := ReadMPIntSSHCOM(Ptr, Size);
      Len := Length(FDSSG) + 4;
      Inc(Ptr, Len);
      Dec(Size, Len);
      { Q }
      FDSSQ := ReadMPIntSSHCOM(Ptr, Size);
      Len := Length(FDSSQ) + 4;
      Inc(Ptr, Len);
      Dec(Size, Len);
      { Y }
      FDSSY := ReadMPIntSSHCOM(Ptr, Size);
      Len := Length(FDSSY) + 4;
      Inc(Ptr, Len);
      Dec(Size, Len);
      { X }
      FDSSX := ReadMPIntSSHCOM(Ptr, Size);

      TrimParams;
      FKeyBlob := BuildPKCSDSSBlob(FDSSP, FDSSQ, FDSSG, FDSSY, FDSSX);
    end;
    Result := true;
  except
  end;
end;

procedure TElSSHKey.Import(Certificate: TElX509Certificate);
begin
  if not Assigned(FCert) then
    FCert := TElX509Certificate.Create(nil);
  Certificate.Clone(FCert{$ifndef HAS_DEF_PARAMS}, true{$endif});
  FKeyFormat := kfX509;
end;

function TElSSHKey.LoadPublicKeyFromBlob(const AlgName: string; Buffer: pointer;
  Size : integer): integer;
const
  SSH_RSA_ID = #0#0#0#7'ssh-rsa';
  SSH_DSS_ID = #0#0#0#7'ssh-dss';
begin
  FAlgorithm := ALGORITHM_NOT_SPECIFIED;
  if CompareStr(AlgName, 'ssh-rsa') = 0 then
    FAlgorithm := ALGORITHM_RSA
  else if CompareStr(AlgName, 'ssh-dss') = 0 then
    FAlgorithm := ALGORITHM_DSS
  else if Length(AlgName) = 0 then
  begin
    if (Size >= Length(SSH_RSA_ID)) and
      (CompareMem(Buffer, @SSH_RSA_ID[1], Length(SSH_RSA_ID))) then
      FAlgorithm := ALGORITHM_RSA
    else if (Size >= Length(SSH_DSS_ID))and
      (CompareMem(Buffer, @SSH_DSS_ID[1], Length(SSH_DSS_ID))) then
      FAlgorithm := ALGORITHM_DSS
  end;
  if FAlgorithm = ALGORITHM_NOT_SPECIFIED then
  begin
    Result := SB_ERROR_SSH_KEYS_UNSUPPORTED_ALGORITHM;
    Exit;
  end;
  Result := 0;
  if FAlgorithm = ALGORITHM_RSA then
  begin
    if not ParsePublicKeyRSA(Buffer, Size) then
      Result := SB_ERROR_SSH_KEYS_INVALID_PUBLIC_KEY;
  end
  else if FAlgorithm = ALGORITHM_DSS then
  begin
    if not ParsePublicKeyDSS(Buffer, Size) then
      Result := SB_ERROR_SSH_KEYS_INVALID_PUBLIC_KEY;
  end
  else
    Result := SB_ERROR_SSH_KEYS_INTERNAL_ERROR;
  if Result = 0 then
  begin
    SetLength(FSSHKeyBlob, Size);
    Move(Buffer^, FSSHKeyBlob[0], Size);
  end;
end;

function TElSSHKey.SavePublicKeyToBlob(var AlgName : string; Buffer: pointer;
  var Size : integer): integer;
begin
  if Length(FSSHKeyBlob) = 0 then
    RegenerateSSHBlob;
  if Size < Length(FSSHKeyBlob) then
  begin
    Result := SB_ERROR_SSH_KEYS_BUFFER_TOO_SMALL;
    Size := Length(FSSHKeyBlob);
  end
  else
  begin
    Size := Length(FSSHKeyBlob);
    Move(FSSHKeyBlob[0], Buffer^, Size);
    if Algorithm = ALGORITHM_RSA then
      AlgName := 'ssh-rsa'
    else if Algorithm = ALGORITHM_DSS then
      AlgName := 'ssh-dss'
    else
      AlgName := '';
    Result := 0;
  end;
end;

procedure TElSSHKey.TrimParams;
  procedure TrimValue(var Value : BufferType);
  var
    Index: integer;
  begin
    if Length(Value) = 0 then
      Exit;
    Index := 1;
    while (Value[Index] = #0) and (Index <= Length(Value)) do
      Inc(Index);
    if Index > 1 then
    begin
      Move(Value[Index], Value[1], Length(Value) - Index + 1);
      SetLength(Value, Length(Value) - Index + 1);
    end;
    if ((Length(Value) > 0) and (PByte(@Value[1])^ >= $80)) or
      (Length(Value) = 0) then
      Value := #0 + Value;
  end;
begin
  TrimValue(FRSAPublicExponent);
  TrimValue(FRSAPrivateExponent);
  TrimValue(FRSAPublicModulus);
  TrimValue(FDSSP);
  TrimValue(FDSSQ);
  TrimValue(FDSSG);
  TrimValue(FDSSY);
  TrimValue(FDSSX);
end;

procedure TElSSHKey.RegenerateSSHBlob;
begin
  FSSHKeyBlob := BytesOfString(GetKeyBlob);
end;

end.

