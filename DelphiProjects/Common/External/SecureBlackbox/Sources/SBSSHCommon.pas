(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

{$I SecBbox.inc}

unit SBSSHCommon;

interface

uses
  SBMath,
  SBSHA,
  SBUtils,
  SBMD,

  SBBlowfish,
  {$ifndef SBB_NO_DES}
  SBDES,
  SB3DES,
  SBSSH3DES,
  {$endif}
  SBSSHTerm,
  {$ifndef SBB_NO_RC4}
  SBRC4,
  {$endif}
  {$ifndef SBB_NO_IDEA}
  SBIDEA,
  {$endif}
  SBSharedResource,
  SBSSHConstants,
  {$ifndef DONT_USE_ZLIB}SBZlib, {$endif}
  SBSSHKeyStorage,
  SBHMAC,
  SBAES,
  SBSSHUtils,
  SBSerpent,
  SBCAST128,
  {$ifndef CLX_USED}Windows,{$endif}
  SysUtils,
  Classes,
  SBTwofish,
  {$ifdef SECURE_BLACKBOX_DEBUG}
  SBDumper,
  {$endif}
  SBSSHMath,
  SBSymmetricCrypto,
  SBConstants;

(*
{$ifdef CHROME}
{$ifdef NET_2_0}
{$ifdef MONO}
// SecureBlackbox_Chrome_MONO.snk
[assembly: InternalsVisibleToAttribute('SecureBlackbox.SSHClient, PublicKey=0024000004800000940000000602000000240000525341310004000001000100e14f67950d1e234a00470578bcf6f55db347c0c39c0a89fc8e790206ca4fe69835e5b5c7a0748abf83ad5fbdfd91eb00f322920d096d702764a434c397fe4ce312509e4a776786415a7ca1307b90905b77c3624a6b76dc0c586c00c322f5196158f0db47b173560963f32e03cc0afeb64c70fd9ed8ef9835760cb6de225846ec')]
[assembly: InternalsVisibleToAttribute('SecureBlackbox.SSHServer, PublicKey=0024000004800000940000000602000000240000525341310004000001000100e14f67950d1e234a00470578bcf6f55db347c0c39c0a89fc8e790206ca4fe69835e5b5c7a0748abf83ad5fbdfd91eb00f322920d096d702764a434c397fe4ce312509e4a776786415a7ca1307b90905b77c3624a6b76dc0c586c00c322f5196158f0db47b173560963f32e03cc0afeb64c70fd9ed8ef9835760cb6de225846ec')]
{$else}
{$ifdef NET_CF}
// SecureBlackbox_Chrome_NET_CF.snk
[assembly: InternalsVisibleToAttribute('SecureBlackbox.SSHClient, PublicKey=0024000004800000940000000602000000240000525341310004000001000100cb6b31590465793c1937bf95c1b521a26548d483358f27860839ed17c9c38e533a6a7559551c16497b6adc118f98c55acb7ef340d44aee6578a758abe3cd66cac800ec160aa8c590163083d2529f8c5187abb35b65aaac8d6d982b311ba792ca5efc6d8ccae5ebf0747ece4df0910a99d98ceae662c6ba0d69bcea3fd6d17ab2')]
[assembly: InternalsVisibleToAttribute('SecureBlackbox.SSHServer, PublicKey=0024000004800000940000000602000000240000525341310004000001000100cb6b31590465793c1937bf95c1b521a26548d483358f27860839ed17c9c38e533a6a7559551c16497b6adc118f98c55acb7ef340d44aee6578a758abe3cd66cac800ec160aa8c590163083d2529f8c5187abb35b65aaac8d6d982b311ba792ca5efc6d8ccae5ebf0747ece4df0910a99d98ceae662c6ba0d69bcea3fd6d17ab2')]
{$else}
// SecureBlackbox_Chrome_NET.snk
[assembly: InternalsVisibleToAttribute('SecureBlackbox.SSHClient, PublicKey=002400000480000094000000060200000024000052534131000400000100010055a1a3a9af1900ff52dae13739fcfaf45ddf8500305951449d9852a7763782f6c3e1d12ca790cb998e9e12af85b34b257fe55f8a6383e59874e0efac0d7af6fd310bf1838f814ce51065b219c28b292e9a5ec79d0de0d65de23a7d3d85f9cbb45b188f8314f9896b9955dbd03b44e7dd7fb2942d3ab8ab9f768a1f780d14b4d6')]
[assembly: InternalsVisibleToAttribute('SecureBlackbox.SSHServer, PublicKey=002400000480000094000000060200000024000052534131000400000100010055a1a3a9af1900ff52dae13739fcfaf45ddf8500305951449d9852a7763782f6c3e1d12ca790cb998e9e12af85b34b257fe55f8a6383e59874e0efac0d7af6fd310bf1838f814ce51065b219c28b292e9a5ec79d0de0d65de23a7d3d85f9cbb45b188f8314f9896b9955dbd03b44e7dd7fb2942d3ab8ab9f768a1f780d14b4d6')]
{$endif}
{$endif}
// SecureBlackbox.snk
//[assembly: InternalsVisibleToAttribute('SecureBlackbox.SSHClient, PublicKey=00240000048000009400000006020000002400005253413100040000010001002926991A2470652E56B923D023ECE9682B7B5DC954FE7590F791B184C39FC27CCE6AFA20C488A9D8883A2D327A6C125682CCF71269D261A65D2FB1A45A81364B55A589CDFAA6BD6D52B8575E84C54CA3459ACB92E07F022FEE748B00A065CB5C6EAC3390033AF39BEFFBFA91A5BE4733C337AA33C4476A05B10BA35706D87DAB')]
//[assembly: InternalsVisibleToAttribute('SecureBlackbox.SSHServer, PublicKey=00240000048000009400000006020000002400005253413100040000010001002926991A2470652E56B923D023ECE9682B7B5DC954FE7590F791B184C39FC27CCE6AFA20C488A9D8883A2D327A6C125682CCF71269D261A65D2FB1A45A81364B55A589CDFAA6BD6D52B8575E84C54CA3459ACB92E07F022FEE748B00A065CB5C6EAC3390033AF39BEFFBFA91A5BE4733C337AA33C4476A05B10BA35706D87DAB')]
{$endif}
{$endif}
*)



type
  TElSSHClass =  class;
  TElSSHTunnelList =  class;
  TElCustomSSHTunnel =  class;
  TElSSHTunnelConnection =  class;

  TSSHTunnelType = 
    (ttLocalPortToRemoteAddress, ttRemotePortToLocalAddress, ttX11,
     ttAuthenticationAgent, ttSubsystem, ttCommand, ttShell);

  TSSHCloseType = 
    (ctReturn, ctSignal, ctError);

  TSBSSHAuthOrder =  (aoDefault, aoKbdIntLast);

  TSSHSendEvent = procedure(Sender : TObject; Buffer : pointer; Size : longint) of object;
  
  TSSHReceiveEvent = procedure(Sender : TObject; Buffer : pointer; MaxSize : longint; {$ifndef BUILDER_USER}out{$else}var{$endif} Written : longint) of object;
  TSSHDataEvent = procedure(Sender : TObject; Buffer : pointer; Size : longint) of object;
  TSSHOpenConnectionEvent = procedure(Sender : TObject) of object;
  TSSHCloseConnectionEvent = procedure(Sender : TObject) of object;
  TSSHChannelCloseEvent = procedure(Sender : TObject; CloseType : TSSHCloseType) of object;
  TSSHErrorEvent = procedure(Sender : TObject; ErrorCode : integer) of object;


  TTunnelEvent = procedure(Sender : TObject; TunnelConnection : TElSSHTunnelConnection) of object;

  TTunnelDataEvent = procedure(Sender : TObject; TunnelConnection :
    TElSSHTunnelConnection; Buffer : pointer; Size : longint) of object;

  TTunnelErrorEvent = procedure(Sender : TObject; Error : integer;
    Data : pointer) of object;


  {$ifdef SECURE_BLACKBOX_DEBUG}
  TSSHDebugEvent = procedure(Sender : TObject; DebugStr : string) of object;
  {$endif}
  
  TSSHAuthenticationFailedEvent = procedure(Sender : TObject; AuthenticationType : integer) of object;

  TSSHAuthenticationKeyboardEvent = procedure(Sender : TObject; Prompts : TStringList; Echo : TBits; Responses : TStringList) of object;
  
  TSSHKeyValidateEvent = procedure(Sender : TObject; ServerKey : TElSSHKey; var Validate : boolean) of object;

  TSSHBannerEvent = procedure(Sender: TObject; const Text : string; const Language : string) of object;

  TSSHMessageLoopEvent =  TElMessageLoopEvent;

  TSSHWindowChangedEvent = procedure(Sender: TObject; Cols, Rows, Width, Height: integer) of object;

  EElSSHException =  class(Exception);

  TSSHReceiveState = 
    (rsRecordHeaderWanted, rsRecordWanted, rsMACWanted);

  TSSHVersion = (sbSSH1, sbSSH2);
  TSSHVersions = set of TSSHVersion;

  TSSHEncryptionAlgorithm = SSH_EA_FIRST..SSH_EA_LAST;
  TSSHCompressionAlgorithm = SSH_CA_FIRST..SSH_CA_LAST;
  TSSHMacAlgorithm = SSH_MA_FIRST..SSH_MA_LAST;
  TSSHKexAlgorithm = SSH_KEX_FIRST..SSH_KEX_LAST;
  TSSHPublicKeyAlgorithm = SSH_PK_FIRST..SSH_PK_LAST;
  
  TSSH1State = 
    (stNotEncrypted, stEncrypted);

  TDHParams =  record
    P : PLInt;
    G : PLInt;
    Q : PLInt;
    X : PLInt;
    E : PLInt;
    K : PLInt;
  end;
  
  TRSAParams =  record
    Modulus : PLInt;
    PublicExponent : PLInt;
    PrivateExponent : PLInt;
  end;
  
  THandshakeParams =  record
    ClientVersionString : string;
    ServerVersionString : string;
    ClientKexInit : string;
    ServerKexInit : string;
    PubHostKey : string;
    Min : string;
    Max : string;
    N : string;
    P : string;
    G : string;
    E : string;
    F : string;
  end;

  TSSH1Params =  record
    Cipher : integer;
    Auth : integer;
    SessionID : array[0..15] of byte;
    SessionKey : array[0..31] of byte;
  end;

  TSSH2Params =  record
    KexAlgorithm : integer;
    ServerHostKeyAlgorithm : integer;
    EncryptionAlgorithmCS : integer;
    EncryptionAlgorithmSC : integer;
    EncCSBlockSize : integer;
    EncSCBlockSize : integer;
    MacAlgorithmCS : integer;
    MacAlgorithmSC : integer;
    MacKeyCS : string;
    MacKeySC : string;
    CompAlgorithmCS : integer;
    CompAlgorithmSC : integer;
    LanguageCS : integer;
    LanguageSC : integer;
    HashAlgorithm : integer; // constants defined later
    SessionID : array of byte;
  end;

  TElSSHTunnelConnection = class(TObject)
  private
    FParent : TElSSHClass;

    FLocalChannel : cardinal;
    FRemoteChannel : cardinal;
    FLocalChannelClosed : boolean;
    FRemoteChannelClosed : boolean;
    FInternalType : integer;
    FInternalState : integer;
    FExitStatus : integer;
    FExitSignal : string;
    FExitMessage : string;
    FCloseType : TSSHCloseType;
    FWindowSize: integer;
    FWindowBuffer: ByteArray;
    FLocalWindowSize: integer;
    FData : pointer;
    FCloseWhenDataIsSent : boolean;
  protected
    function GetParent: TElSSHClass; virtual;
    procedure SetParent(const Value: TElSSHClass); virtual;

    function GetLocalChannel: Cardinal; virtual;
    procedure SetLocalChannel(const Value: Cardinal); virtual;
    function GetRemoteChannel: Cardinal; virtual;
    procedure SetRemoteChannel(const Value: Cardinal); virtual;
    function GetLocalChannelClosed: Boolean; virtual;
    procedure SetLocalChannelClosed(const Value: Boolean); virtual;
    function GetRemoteChannelClosed: Boolean; virtual;
    procedure SetRemoteChannelClosed(const Value: Boolean); virtual;
    function GetInternalType: Integer; virtual;
    procedure SetInternalType(Value: Integer); virtual;
    function GetInternalState: Integer; virtual;
    procedure SetInternalState(Value: Integer); virtual;

    procedure SetExitStatus(const Value: Integer); virtual;
    procedure SetExitSignal(const Value: string); virtual;
    procedure SetExitMessage(const Value: string); virtual;

    function GetCloseType: TSSHCloseType; virtual;
    procedure SetCloseType(const Value: TSSHCloseType); virtual;
    function GetWindowSize: Integer; virtual;
    procedure SetWindowSize(const Value: Integer); virtual;
    function GetWindowBuffer: ByteArray; virtual;
    procedure SetWindowBuffer(const Value: ByteArray); virtual;
    procedure SetWindowBufferLength(const NewLength: Integer); virtual;
    function GetLocalWindowSize: Integer; virtual;
    procedure SetLocalWindowSize(const Value: Integer); virtual;

    function GetCloseWhenDataIsSent: Boolean; virtual;
    procedure SetCloseWhenDataIsSent(Value: Boolean); virtual;

  protected
    ATunnel: TElCustomSSHTunnel;

    FOnData : TSSHDataEvent;
    FOnError : TSSHErrorEvent;
    FOnClose : TSSHChannelCloseEvent;
    FOnExtendedData : TSSHDataEvent;
    FOnWindowChanged : TSSHWindowChangedEvent;

    procedure DoData(Buffer : pointer; Size : longint); virtual;
    procedure DoExtendedData(Buffer : pointer; Size : longint); virtual;
    procedure DoClose(CloseType : TSSHCloseType); virtual;
    procedure DoWindowChanged(Cols, Rows, Width, Height: integer);virtual;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    
    procedure SendData(Buffer : pointer; Size : longint); virtual; abstract;
    procedure SendExtendedData(Buffer : pointer; Size : longint); virtual; abstract;

    procedure SendSignal(const Signal : string);
    procedure SendText(const S : string);
    procedure SendExtendedText(const S : string);
    procedure Close(FlushCachedData : boolean {$ifdef HAS_DEF_PARAMS}= false{$endif});

    property Tunnel: TElCustomSSHTunnel read ATunnel write ATunnel;

    property Data : pointer read FData write FData;
    function CanSend : boolean; virtual;

  published

    property ExitStatus : integer read FExitStatus default 0;
    property ExitSignal : string read FExitSignal;
    property ExitMessage : string read FExitMessage;

    property OnData : TSSHDataEvent read FOnData write FOnData;
    property OnExtendedData : TSSHDataEvent read FOnExtendedData write FOnExtendedData;
    property OnError : TSSHErrorEvent read FOnError write FOnError;
    property OnClose : TSSHChannelCloseEvent read FOnClose write FOnClose;
    property OnWindowChanged : TSSHWindowChangedEvent read FOnWindowChanged
      write FOnWindowChanged;
  end;

  TElCustomSSHTunnel = class(TSBControlBase)
  private
  
  protected
    function GetConnections: TList;
    procedure SetConnections(const Value: TList);

  protected
    FOnOpen : TTunnelEvent;
    FOnClose : TTunnelEvent;
    FOnError : TTunnelErrorEvent;
    FConnections: TList;
    FAutoOpen: boolean;
    
    protected
    FTunnelList : TElSSHTunnelList;

    function GetConnectionsPrp(Index : integer) : TElSSHTunnelConnection;
    function GetConnectionCount : integer;
    procedure SetTunnelList(Value : TElSSHTunnelList);
    procedure Notification(AComponent : TComponent; AOperation : TOperation); override;
    procedure InternalOpen(Data: pointer); virtual;


  public
    procedure DoOpen(Connection : TElSSHTunnelConnection); {virtual;}
    procedure DoClose(Connection : TElSSHTunnelConnection); {virtual;}
    procedure DoError(Error : integer; Data : pointer); {virtual;}

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    procedure AddConnection(Conn : TElSSHTunnelConnection);
    procedure RemoveConnection(Conn : TElSSHTunnelConnection);

    procedure Open(Data : pointer {$ifdef HAS_DEF_PARAMS}= nil{$endif});

    property Connections[Index : integer] : TElSSHTunnelConnection
      read GetConnectionsPrp;
    property ConnectionCount : integer read GetConnectionCount;
    
    property ConnectionsList: TList read FConnections;
    
  published
  
    property AutoOpen : boolean read FAutoOpen write FAutoOpen;   
    property TunnelList : TElSSHTunnelList read FTunnelList write SetTunnelList;

    property OnOpen : TTunnelEvent read FOnOpen write FOnOpen;
    property OnClose : TTunnelEvent read FOnClose write FOnClose;
    property OnError : TTunnelErrorEvent read FOnError write FOnError;
  end;

  { This class is used to maintain the list of tunnel objects }
  TElSSHTunnelList = class(TSBControlBase)
  private
  protected
    FList : TList;

    FSSHClass : TElSSHClass;

    function GetTunnels(Index : integer) : TElCustomSSHTunnel;
    function GetCount : integer;
   protected
    procedure Notification(AComponent : TComponent; AOperation : TOperation); override;
    function Add(Tunnel : TElCustomSSHTunnel) : integer;
    procedure Remove(Index : integer); overload;
    procedure Remove(Tunnel : TElCustomSSHTunnel); overload;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
    function FindTunnel(const SubSystemType, SubSystemName : string):
        TElCustomSSHTunnel;

    property SSHClass : TElSSHClass read FSSHClass;
    property Tunnels[Index : integer] : TElCustomSSHTunnel read GetTunnels;
    property Count : integer read GetCount;
  end;

  { Shell Tunneling }
  TElShellSSHTunnel = class(TElCustomSSHTunnel)
  private
    FTerminalInfo : TElTerminalInfo;
    FEnvironment : TStringList;
  protected
    function GetTerminalInfo : TElTerminalInfo;
    procedure SetTerminalInfo(Info : TElTerminalInfo);
    procedure Notification(AComponent : TComponent; AOperation : TOperation); override;
    procedure TerminalChange(Sender: TObject);

  public
    constructor Create(AOwner : TComponent); override;

    destructor Destroy; override;
  
  published
  
    property TerminalInfo : TElTerminalInfo read GetTerminalInfo write SetTerminalInfo;
    property Environment : TStringList read FEnvironment;
  end;

  { Command tunneling }
  TElCommandSSHTunnel = class(TElCustomSSHTunnel)
  private
    FCommands : TElStringList;
    FActiveCommand : integer;
    FEnvironment : TStringList;
    FTerminalInfo : TElTerminalInfo;
  protected
    procedure SetActiveCommand(const Value : integer);
    function GetCommand : string;
    procedure SetCommand(const ACommand : string);
    function GetTerminalInfo : TElTerminalInfo;
    procedure SetTerminalInfo(Info : TElTerminalInfo);
    procedure Notification(AComponent : TComponent; AOperation : TOperation); override;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

  
  published
  
    property Command : string read GetCommand write SetCommand;
    property Commands : TElStringList read FCommands;
    property ActiveCommand : integer read FActiveCommand write SetActiveCommand;
    property Environment : TStringList read FEnvironment;
    property TerminalInfo : TElTerminalInfo read GetTerminalInfo write SetTerminalInfo;
  end;

  { Subsystem tunneling (SSH2 only) }
  TElSubsystemSSHTunnel = class(TElCustomSSHTunnel)
  private
    FSubsystem : string;
    FEnvironment : TStringList;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
  
  published
  
    property Subsystem : string read FSubsystem write FSubsystem;
    property Environment : TStringList read FEnvironment;
  end;

  { Remote port forwarding tunnel class }
  TElRemotePortForwardSSHTunnel = class(TElCustomSSHTunnel)
  private
    FRemotePort : Integer;
    FToHost : string;
    FToPort: Integer;
  protected
//    procedure InternalOpen(Data: pointer); override;
  
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
    
  published
  
    property Port: Integer read FRemotePort write FRemotePort;
    property ToHost : string read FToHost write FToHost;
    property ToPort: Integer read FToPort write FToPort;
  end;

  { Local port forwarding tunnel class }
  TElLocalPortForwardSSHTunnel = class(TElCustomSSHTunnel)
  private
    FPort: Integer;
    FToHost : string;
    FToPort: Integer;
  protected

    property AutoOpen;
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
//    procedure Open(Data : {$ifndef DELPHI_NET}pointer{$else}System.Object{$endif} = nil);
    
  published
  
    property Port: Integer read FPort write FPort;
    property ToHost : string read FToHost write FToHost;
    property ToPort: Integer read FToPort write FToPort;
  end;
  
  { X11 connections forwarding }
  TElX11ForwardSSHTunnel = class(TElCustomSSHTunnel)
  private
    FAuthenticationProtocol : string;
    FScreenNumber : integer;
    FTerminalInfo : TElTerminalInfo;
    FEnvironment : TStringList;
  protected
    function GetTerminalInfo : TElTerminalInfo;
    procedure SetTerminalInfo(Info : TElTerminalInfo);
    procedure Notification(AComponent : TComponent; AOperation : TOperation); override;

  
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
  published
  
    property AuthenticationProtocol : string read FAuthenticationProtocol
      write FAuthenticationProtocol;
    property ScreenNumber : integer read FScreenNumber write FScreenNumber;
    property TerminalInfo : TElTerminalInfo read GetTerminalInfo write SetTerminalInfo;
    property Environment : TStringList read FEnvironment;
  end;

  { This class responses for the authentication agent tunneling }
  TElAuthenticationAgentSSHTunnel = class(TElCustomSSHTunnel)
  end;

{ This is the Secure Shell Client itself }
  TElSSHClass = class(TSBControlBase)
  private
  protected
    FActive : boolean;
    FOnSend : TSSHSendEvent;
    FOnReceive : TSSHReceiveEvent;
    FOnOpenConnection : TSSHOpenConnectionEvent;
    FOnCloseConnection : TSSHCloseConnectionEvent;
    FOnDataDebug : TSSHDataEvent;
    FOnError : TSSHErrorEvent;
    FOnKeyValidate : TSSHKeyValidateEvent;
    FOnAuthenticationSuccess : TNotifyEvent;
    FOnAuthenticationFailed : TSSHAuthenticationFailedEvent;
    FOnAuthenticationKeyboard : TSSHAuthenticationKeyboardEvent;
    FOnBanner : TSSHBannerEvent;
    {$ifdef SECURE_BLACKBOX_DEBUG}
    FOnDebug : TSSHDebugEvent;
    FDebugLevel: integer;
    {$endif}

    FSoftwareName : string;
    FUserName : string;
    FClientUserName : string;
    FClientHostName : string;
    FPassword : string;
    FRequestCompression : boolean;
    FUseCompression : boolean;
    FCompressionLevel : integer;
    FOpenCommand : boolean;
    FOpenSubsystem : boolean;
    FInBuffer : ByteArray;
    FOutBuffer : ^byte;
    FComprBuffer : ^byte;
    FInBufferIndex : integer;
    FInBufferNeeded : integer;
    FInBufferOffset : integer;
    FInBufferHeader : string;
    FOutBufferIndex : integer;
    FPaddingSize : integer;
    FInSpool : ByteArray;

    FReceiveState : TSSHReceiveState;
    FServerNewLine : string;
    FLastError : integer;
    FClientCookie : array[0..15] of byte;
    FServerCookie : array[0..15] of byte;
    FEncryptionAlgorithms : array[SSH_EA_FIRST..SSH_EA_LAST] of boolean;
    FCompressionAlgorithms : array[SSH_CA_FIRST..SSH_CA_LAST] of boolean;
    FMACAlgorithms : array[SSH_MA_FIRST..SSH_MA_LAST] of boolean;
    FKexAlgorithms : array[SSH_KEX_FIRST..SSH_KEX_LAST] of boolean;
    FPublicKeyAlgorithms : array[SSH_PK_FIRST..SSH_PK_LAST] of boolean;
    FDHParams : TDHParams;
    FHandshakeParams : THandshakeParams;
    FVersions : TSSHVersions;
    FVersion : TSSHVersion;
    FEncryptionAlgorithmSC : TSSHEncryptionAlgorithm;
    FEncryptionAlgorithmCS : TSSHEncryptionAlgorithm;
    FCompressionAlgorithmSC : TSSHCompressionAlgorithm;
    FCompressionAlgorithmCS : TSSHCompressionAlgorithm;
    FMacAlgorithmSC : TSSHMacAlgorithm;
    FMacAlgorithmCS : TSSHMacAlgorithm;
    FKexAlgorithm : TSSHKexAlgorithm;
    FPublicKeyAlgorithm : TSSHPublicKeyAlgorithm;

    FSSH2State : TSSH1State;
    FSSH2Params : TSSH2Params;
    FSSH2ClientLastMessage : integer;
    FSSH2ServerLastMessage : integer;
    FSSH2ClientSequenceNumber : cardinal;
    FSSH2ServerSequenceNumber : cardinal;
    FSSH2LastUserauthMethod : integer;
    (*{$ifndef SBB_NO_DES}
    FSSH23DESInputCtx : T3DESContext;
    FSSH23DESOutputCtx : T3DESContext;
    {$endif}
    FSSH2BlowfishInputCtx : TSBBlowfishContext;
    FSSH2BlowfishOutputCtx : TSBBlowfishContext;
    FSSH2AES128InputCtx : TAESContext128;
    FSSH2AES128OutputCtx : TAESContext128;
    FSSH2AES192InputCtx : TAESContext192;
    FSSH2AES192OutputCtx : TAESContext192;
    FSSH2AES256InputCtx : TAESContext256;
    FSSH2AES256OutputCtx : TAESContext256;
    {$ifndef SBB_NO_RC4}
    FSSH2ArcfourInputCtx : TRC4Context;
    FSSH2ArcfourOutputCtx : TRC4Context;
    {$endif}
    {$ifndef SBB_NO_IDEA}
    FSSH2IdeaInputCtx : TIDEAContext;
    FSSH2IdeaOutputCtx : TIDEAContext;
    {$endif}
    FSSH2SerpentInputCtx : TSerpentContext;
    FSSH2SerpentOutputCtx : TSerpentContext;
    FSSH2Cast128InputCtx : TCAST128Context;
    FSSH2Cast128OutputCtx : TCAST128Context;
    FSSH2TwofishInputCtx : TTwofishContext;
    FSSH2TwofishOutputCtx : TTwofishContext;*)
    FInputCrypto : TElSymmetricCrypto; //Input - from other side to self
    FInputKeyMaterial : TElSymmetricKeyMaterial;
    FOutputCrypto : TElSymmetricCrypto; //Output - from self to other side
    FOutputKeyMaterial : TElSymmetricKeyMaterial;
    {$ifndef DONT_USE_ZLIB}
    FSSH2CompressionCtx : TZlibContext;
    FSSH2DecompressionCtx : TZlibContext;
    {$endif}
    FSSH2AuthenticationPassed : boolean;
    FSSH2LastKeyIndex : integer;
    FSSH2UserauthAlgName : string;
    FSSH2UserauthKeyBlob : string;
    FSSH2EnabledAuthTypes : cardinal;
    FSSH2UserauthServerAlgs : string;
    FTunnelList : TElSSHTunnelList;
    FLastTunnelIndex : integer;
    FLastKeyIndex : integer;
    FChannels : TList;
    FLastChannelIndex : integer;

    //FRC4RandomCtx : TRC4RandomContext;
    FKeyStorage : TElSSHCustomKeyStorage;
    FAuthenticationTypes: Integer;
    FLastAuthMask : cardinal;
    FCurrentRSAAuth : integer;
    FCloseIfNoActiveTunnels : boolean;
    FSharedResource : TElSharedResource;



    function SSH2GetSymmetricCrypto(Algorithm : integer) : TElSymmetricCrypto;
    function GetEncryptionAlgorithms(Index : TSSHEncryptionAlgorithm) : boolean;
    function GetCompressionAlgorithms(Index : TSSHCompressionAlgorithm) : boolean;
    function GetMACAlgorithms(Index : TSSHMACAlgorithm) : boolean;
    procedure SetEncryptionAlgorithms(Index : TSSHEncryptionAlgorithm; Value : boolean);
    procedure SetCompressionAlgorithms(Index : TSSHCompressionAlgorithm; Value : boolean);
    procedure SetMACAlgorithms(Index : TSSHMACAlgorithm; Value : boolean);
    function GetKexAlgorithms(Index : TSSHKexAlgorithm) : boolean;
    procedure SetKexAlgorithms(Index : TSSHKexAlgorithm; Value : boolean);
    function GetPublicKeyAlgorithms(Index : TSSHPublicKeyAlgorithm) : boolean;
    procedure SetPublicKeyAlgorithms(Index : TSSHPublicKeyAlgorithm; Value : boolean);

    function GetTunnelList : TElSSHTunnelList;
    procedure SetTunnelList(Value : TElSSHTunnelList);

    function GetKeyStorage : TElSSHCustomKeyStorage;
    procedure SetKeyStorage(Storage : TElSSHCustomKeyStorage);

    procedure SendTerminalResize(RemoteChannel: cardinal; Cols, Rows, Width,
      Height: integer); virtual;

    procedure Notification(AComponent : TComponent; Operation : TOperation); override;
    procedure ClearChannels;
    procedure CloseTunnel(Tunnel : TElSSHTunnelConnection;
      FlushCachedData: boolean {$ifdef HAS_DEF_PARAMS}= false{$endif}); virtual; abstract;
    procedure ConnectTunnel(Tunnel : TElCustomSSHTunnel; Data : pointer {$ifdef HAS_DEF_PARAMS}= nil{$endif}); virtual; abstract;

    procedure SendTunnelData(RemoteChannel : cardinal; Buffer : pointer; Size : longint); virtual; abstract;
    procedure SendTunnelSignal(RemoteChannel : cardinal; Signal : string); virtual; abstract;

    {$ifdef SECURE_BLACKBOX_DEBUG}
    procedure DoDebug(const DebugStr : string; DebugLevel : integer {$ifdef HAS_DEF_PARAMS}= 1{$endif});
    {$endif}
    {$ifdef SECURE_BLACKBOX_DEBUG}
    function FormatBuf(P : pointer; Size : longint) : string;
    {$endif}

    { Event pushers }

    procedure DoCloseConnection; virtual;
    procedure DoOpenConnection; virtual;

    procedure DoSend(Buffer : pointer; Size : longint);
    procedure DoReceive(Buffer : pointer; MaxSize : longint; out Written : longint);
    procedure DoDataDebug(Buffer : pointer; Size : longint);
    procedure DoError(ErrorCode : integer);
    procedure DoAuthenticationFailed(AuthType : integer);
    procedure DoKeyValidate(Key : TElSSHKey; var Validate : boolean);
    procedure DoTunnelOpen(Connection : TElSSHTunnelConnection); virtual;
    procedure DoTunnelError(Tunnel: TElCustomSSHTunnel; Error: integer;
      Data: pointer); virtual;

    property UserName : string read FUserName write FUserName;
    property Password : string read FPassword write FPassword;
    property TunnelList : TElSSHTunnelList read GetTunnelList write SetTunnelList; 
    
    property OnKeyValidate : TSSHKeyValidateEvent read FOnKeyValidate
      write FOnKeyValidate;
    property OnAuthenticationSuccess : TNotifyEvent
      read FOnAuthenticationSuccess write FOnAuthenticationSuccess;
    property OnAuthenticationFailed : TSSHAuthenticationFailedEvent
      read FOnAuthenticationFailed write FOnAuthenticationFailed;
    property OnAuthenticationKeyboard : TSSHAuthenticationKeyboardEvent
      read FOnAuthenticationKeyboard write FOnAuthenticationKeyboard;
    property OnBanner : TSSHBannerEvent read FOnBanner write FOnBanner;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    
    procedure Close(Forced : boolean {$ifdef HAS_DEF_PARAMS}= false{$endif}); virtual;

    property Active : boolean read FActive;
    property EncryptionAlgorithms[Index : TSSHEncryptionAlgorithm] : boolean
      read GetEncryptionAlgorithms write SetEncryptionAlgorithms;
    property CompressionAlgorithms[Index : TSSHCompressionAlgorithm] : boolean
      read GetCompressionAlgorithms write SetCompressionAlgorithms;
    property MacAlgorithms[Index : TSSHMacAlgorithm] : boolean
      read GetMACAlgorithms write SetMACAlgorithms;
    property KexAlgorithms[Index : TSSHKexAlgorithm] : boolean
      read GetKexAlgorithms write SetKexAlgorithms;
    property PublicKeyAlgorithms[Index : TSSHPublicKeyAlgorithm] : boolean
      read GetPublicKeyAlgorithms write SetPublicKeyAlgorithms;
    { Check these properties after the connection has being established }

    property EncryptionAlgorithmServerToClient : TSSHEncryptionAlgorithm
      read FEncryptionAlgorithmSC;
    property EncryptionAlgorithmClientToServer : TSSHEncryptionAlgorithm
      read FEncryptionAlgorithmCS;
    property CompressionAlgorithmServerToClient : TSSHCompressionAlgorithm
      read FCompressionAlgorithmSC;
    property CompressionAlgorithmClientToServer : TSSHCompressionAlgorithm
      read FCompressionAlgorithmCS;
    property MacAlgorithmServerToClient : TSSHMacAlgorithm read FMacAlgorithmSC;
    property MacAlgorithmClientToServer : TSSHMacAlgorithm read FMacAlgorithmCS;
    property KexAlgorithm : TSSHKexAlgorithm read FKexAlgorithm;
    property PublicKeyAlgorithm : TSSHPublicKeyAlgorithm read FPublicKeyAlgorithm;
    
  published
  
    property SoftwareName : string read FSoftwareName write FSoftwareName;
    { Advanced properties }
    property KeyStorage : TElSSHCustomKeyStorage read GetKeyStorage
      write SetKeyStorage;
    { SSH protocol properties }
    property ForceCompression : boolean read FRequestCompression
      write FRequestCompression;
    property CompressionLevel : integer read FCompressionLevel
      write FCompressionLevel default 9;
    property AuthenticationTypes: Integer read FAuthenticationTypes write
        FAuthenticationTypes default SSH_AUTH_TYPE_PASSWORD;
    property CloseIfNoActiveTunnels : boolean read FCloseIfNoActiveTunnels
      write FCloseIfNoActiveTunnels default true;
      
    {$ifdef SECURE_BLACKBOX_DEBUG}
    property DebugLevel : integer read FDebugLevel write FDebugLevel default 1;
    {$endif}
    
    { Events }
    property OnSend : TSSHSendEvent read FOnSend write FOnSend;
    property OnReceive : TSSHReceiveEvent read FOnReceive write FOnReceive;
    property OnOpenConnection : TSSHOpenConnectionEvent
      read FOnOpenConnection write FOnOpenConnection;
    property OnCloseConnection : TSSHCloseConnectionEvent
      read FOnCloseConnection write FOnCloseConnection;
    property OnDebugData : TSSHDataEvent read FOnDataDebug write FOnDataDebug;
    property OnError : TSSHErrorEvent read FOnError write FOnError;
    {$ifdef SECURE_BLACKBOX_DEBUG}
    property OnDebug : TSSHDebugEvent read FOnDebug write FOnDebug;
    {$endif}
  end;

//{$ENDIF}



type
  EElSSHSilentException =  class(Exception);

resourcestring

  SInvalidTunnelType                    = 'Invalid tunnel type';
  SInvalidSize                          = 'Invalid size';
  SDisconnectInvalidPacketSize          = 'Invalid packet size';
  SDisconnectConnectionClosed           = 'Connection closed';
  SDisconnectInvalidPacket              = 'Invalid packet';
  SDisconnectInvalidCRC                 = 'Invalid CRC checksum';
  SDisconnectInvalidPacketType          = 'Invalid packet type';
  SDisconnectUnsupportedCipher          = 'Unsupported cipher';
  SDisconnectUnsupportedAuthType        = 'Unsupported AUTH type';
  SDisconnectInvalidRSAChallenge        = 'Invalid RSA challenge';
  SDisconnectAuthenticationFailed       = 'Authentication failed';
  SDisconnectInternalError              = 'Internal error';
  SDisconnectHostKeyNotVerifiable       = 'Host key not verifiable';
  SDisconnectInvalidMAC                 = 'Invalid MAC';
  SDisconnectKeyExchangeFailed          = 'Key exchange failed';
  SDisconnectNoMoreAuthMethodsAvailable = 'No more authentication methods available';
  SDisconnectInvalidChannelID           = 'Invalid channel ID';
  SDisconnectBadService                 = 'Bad service id';
  SInternalUsedException                = 'Internally used and handled exception. Please ignore.';
  SForbidden                            = 'Forbidden';
  SCertAlgDoesNotMatchSSH               = 'Certificate algorithm does not match negotiated one';


procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('SSHBlackbox', [TElShellSSHTunnel,
    TElCommandSSHTunnel, TElSubsystemSSHTunnel, TElX11ForwardSSHTunnel,
    TElRemotePortForwardSSHTunnel, TElLocalPortForwardSSHTunnel, TElSSHTunnelList]);
end;

////////////////////////////////////////////////////////////////////////////////
// TElCustomSSHTunnel

constructor TElCustomSSHTunnel.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  FConnections := TList.Create;
  FAutoOpen := true;
end;


destructor TElCustomSSHTunnel.Destroy;
var
  P : TElSSHTunnelConnection;
begin
  while FConnections.Count > 0 do
  begin
    P := TElSSHTunnelConnection(FConnections.Items[0]);
    FConnections.Delete(0);
    if P <> nil then
      P.Free;
  end;
  FConnections.Free;
  inherited;
end;


procedure TElCustomSSHTunnel.DoOpen(Connection : TElSSHTunnelConnection);
begin
  FTunnelList.FSSHClass.DoTunnelOpen(Connection);

  if Assigned(FOnOpen) then
    FOnOpen(Self, Connection);
end;

procedure TElCustomSSHTunnel.DoClose(Connection : TElSSHTunnelConnection);
begin
  if Assigned(FOnClose) then
    FOnClose(Self, Connection);
end;

procedure TElCustomSSHTunnel.DoError(Error : integer; Data : pointer);
begin
  if Assigned(FOnError) then
    FOnError(Self, Error, Data);
end;

procedure TElCustomSSHTunnel.AddConnection(Conn : TElSSHTunnelConnection);
begin
  FConnections.Add(Conn);
end;

procedure TElCustomSSHTunnel.RemoveConnection(Conn : TElSSHTunnelConnection);
begin
  FConnections.Remove(Conn);
end;

function TElCustomSSHTunnel.GetConnectionsPrp(Index : integer) : TElSSHTunnelConnection;
begin
  if Index >= FConnections.Count then
    Result := nil
  else
    Result := TElSSHTunnelConnection(FConnections.Items[Index]);
end;

function TElCustomSSHTunnel.GetConnectionCount : integer;
begin
  Result := FConnections.Count;
end;

procedure TElCustomSSHTunnel.SetTunnelList(Value : TElSSHTunnelList);
begin
  if Value = FTunnelList then
    Exit;
  if Assigned(Value) and Assigned(FTunnelList) then
  begin
    FTunnelList.Remove(Self);
    FTunnelList := Value;
    Value.Add(Self);
  end
  else if Assigned(Value) and not Assigned(FTunnelList) then
  begin
    FTunnelList := Value;
    Value.Add(Self);
  end
  else if (Value = nil) and Assigned(FTunnelList) then
  begin
    FTunnelList.Remove(Self);
    FTunnelList := nil;
  end;
  if FTunnelList <> nil then
    FTunnelList.FreeNotification(Self);
end;

// Virtual function allows successors to perform their own checkup before opening
procedure TElCustomSSHTunnel.InternalOpen(Data: pointer);
begin
  FTunnelList.FSSHClass.ConnectTunnel(Self, Data);
end;

procedure TElCustomSSHTunnel.Open(Data : pointer {$ifdef HAS_DEF_PARAMS}= nil{$endif});
begin
  if (FTunnelList = nil) or (FTunnelList.FSSHClass = nil) then
    DoError(SSH_TUNNEL_ERROR_NOT_BOUND_TO_SSH_CLASS, Data)
  else if (FTunnelList <> nil) and (FTunnelList.FSSHClass <> nil) and
    (FTunnelList.FSSHClass.FActive = false) then
    DoError(SSH_TUNNEL_ERROR_SSH_NOT_CONNECTED, Data)
  else
  begin
    FTunnelList.FSSHClass.FSharedResource.WaitToWrite;
    try
      InternalOpen(Data);
    finally
      FTunnelList.FSSHClass.FSharedResource.Done;
    end;
  end;
end;

procedure TElCustomSSHTunnel.Notification(AComponent : TComponent; AOperation : TOperation);
begin
  inherited;
  if (AOperation = opRemove) and (AComponent = FTunnelList) then
    SetTunnelList(nil);
end;


function TElCustomSSHTunnel.GetConnections: TList;
begin
  Result := FConnections;
end;

procedure TElCustomSSHTunnel.SetConnections(const Value: TList);
begin
  FConnections := Value;
end;

////////////////////////////////////////////////////////////////////////////////
// TElSSHTunnelList class

function TElSSHTunnelList.GetTunnels(Index : integer) : TElCustomSSHTunnel;
begin
  if Index >= FList.Count then
    Result := nil
  else
    Result := TElCustomSSHTunnel(FList.Items[Index]);
end;

function TElSSHTunnelList.GetCount : integer;
begin
  Result := FList.Count;
end;

constructor TElSSHTunnelList.Create(AOwner : TComponent);
begin
  inherited  Create(AOwner);
  FList := TList.Create;
end;


destructor TElSSHTunnelList.Destroy;
begin
  inherited;
  FList.Free;
end;


function TElSSHTunnelList.Add(Tunnel : TElCustomSSHTunnel) : integer;
begin
  if FList.IndexOf(Tunnel) >= 0 then
  begin
    Result := -1;
    Exit;
  end;
  if Tunnel <> nil then
  begin
    Tunnel.FTunnelList := Self;
    Tunnel.FreeNotification(Self);
  end;
  Result := FList.Add(Tunnel);
end;

function TElSSHTunnelList.FindTunnel(const SubSystemType, SubSystemName :
    string): TElCustomSSHTunnel;
var i : integer;
    ss,
    sn : string;
begin
  result := nil;
  SS := lowercase(SubSystemType);
  sn := lowercase(SubSystemName);

  for i := 0 to FList.Count - 1 do
  begin
    if ((SS = 'shell') and (TElCustomSSHTunnel(FList[I]) is TElShellSSHTunnel)) or
       ((SS = 'command') and (TElCustomSSHTunnel(FList[I]) is TElCommandSSHTunnel)) or
       ((SS = 'subsystem') and (TElCustomSSHTunnel(FList[I]) is TElSubsystemSSHTunnel) and (TElSubsystemSSHTunnel(FList[I]).Subsystem = sn)) then
    begin
      Result := TElCustomSSHTunnel(FList[I]);
      Break;
    end;
  end;
end;

procedure TElSSHTunnelList.Remove(Index : integer);
begin
  {$ifdef VCL50}
  if (FList.Items[Index] <> nil) and (not (csDestroying in TElCustomSSHTunnel(FList.Items[Index]).ComponentState)) then
    TElCustomSSHTunnel(FList.Items[Index]).RemoveFreeNotification(Self);
  {$endif}
  FList.Delete(Index);
end;

procedure TElSSHTunnelList.Remove(Tunnel : TElCustomSSHTunnel);
begin
  {$ifdef VCL50}
  if (Tunnel <> nil) and (not (csDestroying in Tunnel.ComponentState)) then
    Tunnel.RemoveFreeNotification(Self);
  {$endif}
  FList.Remove(Tunnel);
end;

procedure TElSSHTunnelList.Notification(AComponent : TComponent; AOperation : TOperation);
begin
  inherited;
  if AOperation = opRemove then
    Remove(TElCustomSSHTunnel(AComponent));
end;


////////////////////////////////////////////////////////////////////////////////
// TElSSHClass class

constructor TElSSHClass.Create(AOwner : TComponent);
var
  I : integer;
begin
  inherited Create(AOwner);

  SetLength(FInBuffer, IN_BUFFER_SIZE);
  
  GetMem(FOutBuffer, OUT_BUFFER_SIZE);
  GetMem(FComprBuffer, COMPR_BUFFER_SIZE);
  FInBufferIndex := 0;
  FOutBufferIndex := 0;
  FSoftwareName := 'EldoS.SSHBlackbox.4';
  LCreate(FDHParams.P);
  LCreate(FDHParams.G);
  LCreate(FDHParams.X);
  LCreate(FDHParams.E);
  LCreate(FDHParams.Q);
  LCreate(FDHParams.K);
  for I := SSH_EA_FIRST to SSH_EA_LAST do
    FEncryptionAlgorithms[I] := true;
  FEncryptionAlgorithms[SSH_EA_3DES] := false;
  for I := SSH_CA_FIRST to SSH_CA_LAST do
    FCompressionAlgorithms[I] := true;
  {$ifdef DONT_USE_ZLIB}
  FCompressionAlgorithms[SSH_CA_ZLIB] := false;
  {$endif}
  for I := SSH_MA_FIRST to SSH_MA_LAST do
    FMACAlgorithms[I] := true;
  for I := SSH_PK_FIRST to SSH_PK_LAST do
    FPublicKeyAlgorithms[I] := true;
  for I := SSH_KEX_FIRST to SSH_KEX_LAST do
    FKexAlgorithms[I] := true;
  FCompressionLevel := 6;
  FUseCompression := false;
  FTunnelList := nil;
  FVersions := [sbSSH1, sbSSH2];

  FChannels := TList.Create;
  FLastChannelIndex := 0;
  FLastTunnelIndex := -1;
  //LRC4Init(FRC4RandomCtx);
  FCurrentRSAAuth := 0;
  FActive := false;
  FAuthenticationTypes := SSH_AUTH_TYPE_PASSWORD;
  FCloseIfNoActiveTunnels := true;

  FSharedResource := TElSharedResource.Create;

  FInputCrypto := nil;
  FInputKeyMaterial := nil;
  FOutputCrypto := nil;
  FOutputKeyMaterial := nil;

end;


destructor TElSSHClass.Destroy;
begin

  LDestroy(FDHParams.P);
  LDestroy(FDHParams.G);
  LDestroy(FDHParams.X);
  LDestroy(FDHParams.E);
  LDestroy(FDHParams.Q);
  LDestroy(FDHParams.K);

  FreeMem(FOutBuffer);
  FreeMem(FComprBuffer);

  ClearChannels;
  FreeAndNil(FChannels);
  FreeAndNil(FSharedResource);

  if Assigned(FInputCrypto) then
    FreeAndNil(FInputCrypto);
  if Assigned(FInputKeyMaterial) then
    FreeAndNil(FInputKeyMaterial);
  if Assigned(FOutputCrypto) then
    FreeAndNil(FOutputCrypto);
  if Assigned(FOutputKeyMaterial) then
    FreeAndNil(FOutputKeyMaterial);

  inherited;
end;


procedure TElSSHClass.ClearChannels;
var
  Conn : TElSSHTunnelConnection;
begin
  while FChannels.Count > 0 do
  begin
    Conn := FChannels.Items[0];
    FChannels.Delete(0);
    if Assigned(Conn) then
    begin
      if Conn.Tunnel <> nil then
        Conn.Tunnel.FConnections.Remove(Conn);
        
      Conn.Free; // <<--- here was comment
    end;
  end;
end;

procedure TElSSHClass.Close(Forced : boolean {$ifdef HAS_DEF_PARAMS}= false{$endif});
begin
  DoCloseConnection;
end;
                
////////////////////////////////////////////////////////////////////////////////
// Events

procedure TElSSHClass.DoCloseConnection;
begin
  FActive := false;
  // II Mar 18 2005
  ClearChannels;
  if Assigned(FOnCloseConnection) then
    FOnCloseConnection(Self);
  SetLength(FInBuffer, 0);
end;

procedure TElSSHClass.DoOpenConnection;
begin
  FActive := true;
  if Assigned(FOnOpenConnection) then
    FOnOpenConnection(Self);
end;

procedure TElSSHClass.DoSend(Buffer : pointer; Size : longint);
{$ifdef SECURE_BLACKBOX_DEBUG}
var
  S : string;
  I : integer;
{$endif}
begin
  {$ifdef SECURE_BLACKBOX_DEBUG}
  S := '';
  for I := 0 to Size - 1 do
    S := S + IntToHex(PByteArray(Buffer)[I], 2) + ' ';
  DoDebug('Writing data to socket', SSH_DEBUG_LEVEL_PACKET_DATA);
  DoDebug(S, SSH_DEBUG_LEVEL_PACKET_DATA);
  {$endif}
  if Assigned(FOnSend) then
    FOnSend(Self, Buffer, Size);
end;

procedure TElSSHClass.DoReceive(Buffer : pointer; MaxSize : longint; out Written : longint);
{$ifdef SECURE_BLACKBOX_DEBUG}
var
  S : string;
  I : integer;
{$endif}
begin
  if Length(FInSpool) > 0 then
  begin
    Written := Min(Length(FInSpool), MaxSize);
    Move(FInSpool[0], Buffer^, Written);
    Move(FInSpool[Written], FInSpool[0], Length(FInSpool) - Written);
    SetLength(FInSpool, Length(FInSpool) - Written);
  end
  else
  if Assigned(FOnReceive) then
    FOnReceive(Self, Buffer, MaxSize, Written);
  {$ifdef SECURE_BLACKBOX_DEBUG}
  if Written <= 0 then Exit;
  S := '';
  for I := 0 to Written - 1 do
    S := S + IntToHex(PByteArray(Buffer)[I], 2) + ' ';
  DoDebug('Read from socket: ' + Inttostr(Written) + ' bytes of data', SSH_DEBUG_LEVEL_PACKET_DATA);
  DoDebug(S, SSH_DEBUG_LEVEL_PACKET_DATA);
  SetLength(S, Written);
  Move(Buffer^, S[1], Written);
  DoDebug(S, SSH_DEBUG_LEVEL_PACKET_DATA);
  {$endif}
end;

procedure TElSSHClass.DoDataDebug(Buffer : pointer; Size : longint);
begin
  if Assigned(FOnDataDebug) then
    FOnDataDebug(Self, Buffer, Size);
end;

procedure TElSSHClass.DoError(ErrorCode : integer);
begin
  FLastError := ErrorCode;
  if Assigned(FOnError) then
    FOnError(Self, ErrorCode);
end;

procedure TElSSHClass.DoAuthenticationFailed(AuthType : integer);
begin
  if Assigned(FOnAuthenticationFailed) then
    FOnAuthenticationFailed(Self, AuthType);
end;

procedure TElSSHClass.DoKeyValidate(Key : TElSSHKey; var Validate : boolean);
begin
  if Assigned(FOnKeyValidate) then
    FOnKeyValidate(Self, Key, Validate);
end;

{$ifdef SECURE_BLACKBOX_DEBUG}
procedure TElSSHClass.DoDebug(const DebugStr : string; DebugLevel: integer {$ifdef HAS_DEF_PARAMS}= 1{$endif});
begin
  if Assigned(FOnDebug) and (DebugLevel <= FDebugLevel) then
    FOnDebug(Self, 'Debug ' + IntToStr(DebugLevel) + ': ' + DebugStr);
  Dumper.WriteString(DebugStr);
end;
{$endif}

procedure TElSSHClass.DoTunnelOpen(Connection : TElSSHTunnelConnection);
begin
  // Default implementation does nothing
end;

procedure TElSSHClass.DoTunnelError(Tunnel: TElCustomSSHTunnel; Error: integer;
  Data: pointer);
begin
  Tunnel.DoError(Error, Data);
end;

function TElSSHClass.GetKeyStorage : TElSSHCustomKeyStorage;
begin
  Result := FKeyStorage;
end;

procedure TElSSHClass.SetKeyStorage(Storage : TElSSHCustomKeyStorage);
begin
  {$ifdef VCL50}
  if (FKeyStorage <> nil) and (not (csDestroying in FKeyStorage.ComponentState)) then
    FKeyStorage.RemoveFreeNotification(Self);
  {$endif}
  FKeyStorage := Storage;
  if FKeyStorage <> nil then
  begin
    FKeyStorage.FreeNotification(Self);
  end;
end;

procedure TElSSHClass.Notification(AComponent : TComponent; Operation : TOperation);
begin
  inherited;
  if (AComponent = FTunnelList) and (Operation = opRemove) then
    TunnelList := nil
  else
  if (AComponent = FKeyStorage) and (Operation = opRemove) then
    FKeyStorage := nil;
end;

////////////////////////////////////////////////////////////////////////////////
// auxiliary routines

function TElSSHClass.SSH2GetSymmetricCrypto(Algorithm : integer) : TElSymmetricCrypto;
var
  Factory : TElSymmetricCryptoFactory;
begin
  Factory := TElSymmetricCryptoFactory.Create;

  try
    case Algorithm of
      {$ifndef SBB_NO_DES}
      SSH_EA_3DES :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_3DES, cmCBC);
      {$endif}
      SSH_EA_BLOWFISH :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_BLOWFISH, cmCBC);
      SSH_EA_TWOFISH256, SSH_EA_TWOFISH192, SSH_EA_TWOFISH128:
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_TWOFISH, cmCBC);
      SSH_EA_AES256 :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_AES256, cmCBC);
      SSH_EA_AES192 :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_AES192, cmCBC);
      SSH_EA_AES128 :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_AES128, cmCBC);
      SSH_EA_SERPENT256, SSH_EA_SERPENT192, SSH_EA_SERPENT128 :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_SERPENT, cmCBC);
      {$ifndef SBB_NO_RC4}
      SSH_EA_ARCFOUR :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_RC4, cmDefault);
      {$endif}
      {$ifndef SBB_NO_IDEA}
      SSH_EA_IDEA :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_IDEA, cmCBC);
      {$endif}
      SSH_EA_CAST128 :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_CAST128, cmCBC);
      {$ifndef SBB_NO_DES}
      SSH_EA_DES :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_DES, cmCBC);
      {$endif}
      SSH_EA_AES128_CTR :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_AES128, cmCTR);
      SSH_EA_AES192_CTR :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_AES192, cmCTR);
      SSH_EA_AES256_CTR :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_AES256, cmCTR);
      {$ifndef SBB_NO_DES}
      SSH_EA_3DES_CTR :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_3DES, cmCTR);
      {$endif}
      SSH_EA_BLOWFISH_CTR :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_BLOWFISH, cmCTR);
      SSH_EA_TWOFISH128_CTR, SSH_EA_TWOFISH192_CTR, SSH_EA_TWOFISH256_CTR :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_TWOFISH, cmCTR);
      SSH_EA_SERPENT128_CTR, SSH_EA_SERPENT192_CTR, SSH_EA_SERPENT256_CTR :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_SERPENT, cmCTR);
      {$ifndef SBB_NO_IDEA}
      SSH_EA_IDEA_CTR :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_IDEA, cmCTR);
      {$endif}
      SSH_EA_CAST128_CTR :
        Result := Factory.CreateInstance(SB_ALGORITHM_CNT_CAST128, cmCTR)
      {$ifndef SBB_NO_RC4};
      SSH_EA_ARCFOUR128, SSH_EA_ARCFOUR256 :
      begin
        Result := TElRC4SymmetricCrypto.Create(SB_ALGORITHM_CNT_RC4, cmDefault);
        TElRC4SymmetricCrypto(Result).SkipKeystreamBytes := 1536;
      end
      {$endif}
      else
        Result := nil;
    end;
    
  if Assigned(Result) then
    Result.Padding := cpNone;
  finally
    Factory.Free;
  end;
end;



////////////////////////////////////////////////////////////////////////////////
// Properties' managers

function TElSSHClass.GetEncryptionAlgorithms(Index : TSSHEncryptionAlgorithm) : boolean;
begin
  Result := FEncryptionAlgorithms[Index];
end;

function TElSSHClass.GetCompressionAlgorithms(Index : TSSHCompressionAlgorithm) : boolean;
begin
  Result := FCompressionAlgorithms[Index];
end;

function TElSSHClass.GetMACAlgorithms(Index : TSSHMACAlgorithm) : boolean;
begin
  Result := FMACAlgorithms[Index];
end;

procedure TElSSHClass.SetEncryptionAlgorithms(Index : TSSHEncryptionAlgorithm; Value : boolean);
begin
  FEncryptionAlgorithms[Index] := Value;
end;

procedure TElSSHClass.SetCompressionAlgorithms(Index : TSSHCompressionAlgorithm; Value : boolean);
begin
  FCompressionAlgorithms[Index] := Value;
  {$ifdef DONT_USE_ZLIB}
  if Index = SSH_CA_ZLIB then
    FCompressionAlgorithms[Index] := false;
  {$endif}
end;

procedure TElSSHClass.SetMACAlgorithms(Index : TSSHMACAlgorithm; Value : boolean);
begin
  FMACAlgorithms[Index] := Value;
end;

function TElSSHClass.GetKexAlgorithms(Index : TSSHKexAlgorithm) : boolean;
begin
  Result := FKexAlgorithms[Index];
end;

procedure TElSSHClass.SetKexAlgorithms(Index : TSSHKexAlgorithm; Value : boolean);
begin
  FKexAlgorithms[Index] := Value;
end;

function TElSSHClass.GetPublicKeyAlgorithms(Index : TSSHPublicKeyAlgorithm) : boolean;
begin
  Result := FPublicKeyAlgorithms[Index];
end;

procedure TElSSHClass.SetPublicKeyAlgorithms(Index : TSSHPublicKeyAlgorithm; Value : boolean);
begin
  FPublicKeyAlgorithms[Index] := Value;
end;

function TElSSHClass.GetTunnelList : TElSSHTunnelList;
begin
  Result := FTunnelList;
end;

procedure TElSSHClass.SetTunnelList(Value : TElSSHTunnelList);
begin
  {$ifdef VCL50}
  if (FTunnelList <> nil) and (not (csDestroying in FTunnelList.ComponentState)) then
    FTunnelList.RemoveFreeNotification(Self);
  {$endif}
  FTunnelList := Value;
  if FTunnelList <> nil then
  begin
    FTunnelList.FSSHClass := Self;
    FTunnelList.FreeNotification(Self);
  end;
end;

procedure TElSSHClass.SendTerminalResize(RemoteChannel: cardinal;
  Cols, Rows, Width, Height: integer);
begin
  Assert(false); // rewrite for server
end;



constructor TElSSHTunnelConnection.Create;
begin
  inherited;
  FLocalChannelClosed := false;
  FRemoteChannelClosed := false;
  FCloseWhenDataIsSent := false;
  FCloseType := ctReturn;
  FParent := nil;
  FWindowSize := 0;
  FLocalWindowSize := 0;
  SetLength(FWindowBuffer, 0);
  FData := nil;
end;

destructor TElSSHTunnelConnection.Destroy;
begin
  if FParent <> nil then
    FParent.FChannels.Remove(Self);
  inherited;
end;


procedure TElSSHTunnelConnection.Close(FlushCachedData: boolean {$ifdef HAS_DEF_PARAMS}= false{$endif});
begin
  FParent.FSharedResource.WaitToWrite;
  try
    if (not FlushCachedData) or (Length(FWindowBuffer) = 0) then
      DoClose(ctReturn);
    FParent.CloseTunnel(Self, FlushCachedData);
  finally
    FParent.FSharedResource.Done;
  end;
end;

procedure TElSSHTunnelConnection.DoData(Buffer : pointer; Size : longint);
begin
  if Assigned(FOnData) then
    FOnData(Self, Buffer, Size);
end;

procedure TElSSHTunnelConnection.DoExtendedData(Buffer : pointer; Size : longint);
begin
  if Assigned(FOnExtendedData) then
    FOnExtendedData(Self, Buffer, Size);
end;

procedure TElSSHTunnelConnection.DoClose(CloseType : TSSHCloseType);
begin
  if Assigned(ATunnel) then
    ATunnel.DoClose(Self);
  if Assigned(FOnClose) then
    FOnClose(Self, CloseType);
end;

procedure TElSSHTunnelConnection.DoWindowChanged(Cols, Rows, Width, Height: integer);
begin
  if Assigned(FOnWindowChanged) then
    FOnWindowChanged(Self, Cols, Rows, Width, Height);
end;

procedure TElSSHTunnelConnection.SendSignal(const Signal : string);
begin
  FParent.FSharedResource.WaitToWrite;
  try
    FParent.SendTunnelSignal(FRemoteChannel, Signal);
  finally
    FParent.FSharedResource.Done;
  end;
end;

procedure TElSSHTunnelConnection.SendText(const S : string);
begin
  SendData(@S[1], Length(S));
end;

procedure TElSSHTunnelConnection.SendExtendedText(const S : string);
begin
  SendExtendedData(@S[1], Length(S));
end;

function TElSSHTunnelConnection.GetParent: TElSSHClass;
begin
  Result := FParent;
end;

procedure TElSSHTunnelConnection.SetParent(const Value: TElSSHClass);
begin
  FParent := Value;
end;

function TElSSHTunnelConnection.GetLocalChannel: Cardinal;
begin
  Result := FLocalChannel;
end;

procedure TElSSHTunnelConnection.SetLocalChannel(const Value: Cardinal);
begin
  FLocalChannel := Value;
end;

function TElSSHTunnelConnection.GetRemoteChannel: Cardinal;
begin
  Result := FRemoteChannel;
end;

procedure TElSSHTunnelConnection.SetRemoteChannel(const Value: Cardinal);
begin
  FRemoteChannel := Value;
end;

function TElSSHTunnelConnection.GetLocalChannelClosed: Boolean;
begin
  Result := FLocalChannelClosed;
end;

procedure TElSSHTunnelConnection.SetLocalChannelClosed(const Value: Boolean);
begin
  FLocalChannelClosed := Value;
end;

function TElSSHTunnelConnection.GetRemoteChannelClosed: Boolean;
begin
  Result := FRemoteChannelClosed;
end;

procedure TElSSHTunnelConnection.SetRemoteChannelClosed(const Value: Boolean);
begin
  FRemoteChannelClosed := Value;
end;

function TElSSHTunnelConnection.GetInternalType: Integer;
begin
  Result := FInternalType;
end;

procedure TElSSHTunnelConnection.SetInternalType(Value: Integer);
begin
  FInternalType := Value;
end;

function TElSSHTunnelConnection.GetInternalState: Integer;
begin
  Result := FInternalState;
end;

procedure TElSSHTunnelConnection.SetInternalState(Value: Integer);
begin
  FInternalState := Value;
end;
   
procedure TElSSHTunnelConnection.SetExitStatus(const Value: Integer);
begin
  FExitStatus := Value;
end;

procedure TElSSHTunnelConnection.SetExitSignal(const Value: string);
begin
  FExitSignal := Value;
end;
    
procedure TElSSHTunnelConnection.SetExitMessage(const Value: string);
begin
  FExitMessage := Value;
end;

function TElSSHTunnelConnection.GetCloseType: TSSHCloseType;
begin
  Result := FCloseType;
end;

procedure TElSSHTunnelConnection.SetCloseType(const Value: TSSHCloseType);
begin
  FCloseType := Value;
end;

function TElSSHTunnelConnection.GetWindowSize: Integer;
begin
  Result := FWindowSize;
end;

procedure TElSSHTunnelConnection.SetWindowSize(const Value: Integer);
begin
  FWindowSize := Value;
end;

function TElSSHTunnelConnection.GetWindowBuffer: ByteArray;
begin
  Result := FWindowBuffer;
end;

procedure TElSSHTunnelConnection.SetWindowBuffer(const Value: ByteArray);
begin
  FWindowBuffer := Value;
end;

procedure TElSSHTunnelConnection.SetWindowBufferLength(const NewLength: Integer);
begin
  SetLength(FWindowBuffer, NewLength);
end;

function TElSSHTunnelConnection.GetLocalWindowSize: Integer;
begin
  Result := FLocalWindowSize;
end;

procedure TElSSHTunnelConnection.SetLocalWindowSize(const Value: Integer);
begin
  FLocalWindowSize := Value;
end;

function TElSSHTunnelConnection.GetCloseWhenDataIsSent: Boolean;
begin
  Result := FCloseWhenDataIsSent;
end;

procedure TElSSHTunnelConnection.SetCloseWhenDataIsSent(Value: Boolean);
begin
  FCloseWhenDataIsSent := Value;
end;

function TElSSHTunnelConnection.CanSend : boolean;
begin
  Result := true;
end;

////////////////////////////////////////////////////////////////////////////////
// TElShellSSHTunnel

function TElShellSSHTunnel.GetTerminalInfo : TElTerminalInfo;
begin
  Result := FTerminalInfo;
end;

procedure TElShellSSHTunnel.SetTerminalInfo(Info : TElTerminalInfo);
begin
  if (FTerminalInfo <> Info) and (FTerminalInfo <> nil) then
    FTerminalInfo.RemoveChangeNotification(TerminalChange);
    
  {$ifdef VCL50}
  if (FTerminalInfo <> nil) and (not (csDestroying in FTerminalInfo.ComponentState)) then
    FTerminalInfo.RemoveFreeNotification(Self);
  {$endif}

  FTerminalInfo := Info;
  if FTerminalInfo <> nil then
  begin
    FTerminalInfo.FreeNotification(Self);
    FTerminalInfo.AddChangeNotification(TerminalChange);
  end;
end;

procedure TElShellSSHTunnel.TerminalChange(Sender: TObject);
var
  I : integer;
begin
  if (FTunnelList <> nil) and (FTunnelList.SSHClass <> nil) and (FTerminalInfo <> nil) then
  begin
    for I := 0 to ConnectionCount - 1 do
    begin
      TElSSHTunnelConnection(Connections[I]).FParent.SendTerminalResize(
        TElSSHTunnelConnection(Connections[I]).FRemoteChannel,
        FTerminalInfo.Cols, FTerminalInfo.Rows, FTerminalInfo.Width, FTerminalInfo.Height);
    end;
  end;
end;

constructor TElShellSSHTunnel.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  FTerminalInfo := nil;
  FEnvironment := TStringList.Create;
end;


destructor TElShellSSHTunnel.Destroy;
begin
  if Assigned(FTerminalInfo) then
    FTerminalInfo.RemoveChangeNotification(TerminalChange);
  FEnvironment.Free;
  inherited;
end;


procedure TElShellSSHTunnel.Notification(AComponent : TComponent; AOperation : TOperation);
begin
  inherited;
  if (AOperation = opRemove) and (AComponent = FTerminalInfo) then
    SetTerminalInfo(nil);
end;


////////////////////////////////////////////////////////////////////////////////
// TElCommandSSHTunnel

constructor TElCommandSSHTunnel.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  FCommands := TElStringList.Create;
  FEnvironment := TStringList.Create;
  FActiveCommand := 0;
end;


destructor TElCommandSSHTunnel.Destroy;
begin
  FEnvironment.Free;
  FCommands.Free;
  inherited;
end;


procedure TElCommandSSHTunnel.SetActiveCommand(const Value : integer);
begin
  if (Value >= 0) and (Value < FCommands.Count) then
    FActiveCommand := Value;
end;

function TElCommandSSHTunnel.GetCommand : string;
begin
  if FCommands.Count > 0 then
    Result := FCommands[0]
  else
    Result := '';
end;


procedure TElCommandSSHTunnel.SetCommand(const ACommand : string);
begin
  if FCommands.Count = 0 then
    FCommands.Add(ACommand)
  else
    FCommands[0] := ACommand;
end;

function TElCommandSSHTunnel.GetTerminalInfo : TElTerminalInfo;
begin
  Result := FTerminalInfo;
end;

procedure TElCommandSSHTunnel.SetTerminalInfo(Info : TElTerminalInfo);
begin
  {$ifdef VCL50}
  if (FTerminalInfo <> nil) and (not (csDestroying in FTerminalInfo.ComponentState)) then
    FTerminalInfo.RemoveFreeNotification(Self);
  {$endif}
  FTerminalInfo := Info;
  if FTerminalInfo <> nil then
    FTerminalInfo.FreeNotification(Self);
end;

procedure TElCommandSSHTunnel.Notification(AComponent : TComponent; AOperation : TOperation);
begin
  inherited;
  if (AOperation = opRemove) and (AComponent = FTerminalInfo) then
    SetTerminalInfo(nil);
end;



////////////////////////////////////////////////////////////////////////////////
// TElSubsystemSSHTunnel


constructor TElSubsystemSSHTunnel.Create(AOwner : TComponent);
begin
  inherited;
  FEnvironment := TStringList.Create;
end;


destructor TElSubsystemSSHTunnel.Destroy;
begin
  FEnvironment.Free;
  inherited;
end;



////////////////////////////////////////////////////////////////////////////////
// TElRemotePortForwardSSHTunnel

constructor TElRemotePortForwardSSHTunnel.Create(AOwner : TComponent);
begin
  inherited;
end;


destructor TElRemotePortForwardSSHTunnel.Destroy;
begin
  inherited;
end;

{procedure TElRemotePortForwardSSHTunnel.InternalOpen(Data: pointer);
begin
  DoError(SSH_TUNNEL_ERROR_UNSUPPORTED_ACTION, Data);
end;}


////////////////////////////////////////////////////////////////////////////////
// TElLocalPortForwardSSHTunnel

constructor TElLocalPortForwardSSHTunnel.Create(AOwner : TComponent);
begin
  inherited;
end;


destructor TElLocalPortForwardSSHTunnel.Destroy;
begin
  inherited;
end;


////////////////////////////////////////////////////////////////////////////////
// TElX11ForwardSSHTunnel

constructor TElX11ForwardSSHTunnel.Create(AOwner : TComponent);
begin
  inherited;
  FEnvironment := TStringList.Create;
end;


destructor TElX11ForwardSSHTunnel.Destroy;
begin
  FEnvironment.Free;
  inherited;
end;

function TElX11ForwardSSHTunnel.GetTerminalInfo : TElTerminalInfo;
begin
  Result := FTerminalInfo;
end;

procedure TElX11ForwardSSHTunnel.SetTerminalInfo(Info : TElTerminalInfo);
begin
  {$ifdef VCL50}
  if (FTerminalInfo <> nil) and (not (csDestroying in FTerminalInfo.ComponentState)) then
    FTerminalInfo.RemoveFreeNotification(Self);
  {$endif}
  FTerminalInfo := Info;
  if FTerminalInfo <> nil then
    FTerminalInfo.FreeNotification(Self);
end;

procedure TElX11ForwardSSHTunnel.Notification(AComponent : TComponent; AOperation : TOperation);
begin
  inherited;
  if (AOperation = opRemove) and (AComponent = FTerminalInfo) then
    SetTerminalInfo(nil);
end;



{$IFDEF SECURE_BLACKBOX_DEBUG}
function TElSSHClass.FormatBuf(P : pointer; Size : longint) : string;
var
  I : integer;
begin
  SetLength(Result, 0);
  for I := 0 to Size - 1 do
    Result := Result + IntToHex(PByteArray(P)[I], 2) + ':';
end;
{$ENDIF}

end.

