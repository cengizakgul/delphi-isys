(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002--2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

unit SBIdSSHClient;

// set this define is you are using Indy 10
{$define INDY100}
// set this define is you are using Indy 9
{.$define INDY90}

// IdCompilerDefines.inc is located in Indy source folder. 
//{$I IdCompilerDefines.inc}

interface

uses
 Classes, SysUtils,
 IdGlobal, IdComponent, IdException, IdIOHandler, IdTCPClient,
 SBUtils, SBSSHConstants, SBSSHCommon, SBSSHClient, SBSSHKeyStorage;

type
  TElIdSSHClient = class;

  TElIdSSHClientReadThread = class (TThread)
  protected
    FClient: TElIdSSHClient;
    FSynchronizing: Boolean;

  public
    constructor Create(AClient: TElIdSSHClient); reintroduce;
    procedure Execute; override;

    property  Client: TElIdSSHClient read FClient;
  end;

  TElIdSSHClient = class (TIdComponent)
  private
    FOnAuthenticationSuccess: TNotifyEvent;
    FOnAuthenticationFailed: TSSHAuthenticationFailedEvent;
    FOnAuthenticationKeyboard: TSSHAuthenticationKeyboardEvent;
    FOnBanner: TSSHBannerEvent;
    FOnCloseConnection: TSSHCloseConnectionEvent;
    FOnDebugData: TSSHDataEvent;
    FOnError: TSSHErrorEvent;
    FOnKeyValidate: TSSHKeyValidateEvent;
    FOnOpenConnection: TSSHOpenConnectionEvent;

    function GetCompressionAlgorithms(
      Index: TSSHCompressionAlgorithm): Boolean;
    function GetEncryptionAlgorithms(
      Index: TSSHEncryptionAlgorithm): Boolean;
    function GetKexAlgorithms(Index: TSSHKexAlgorithm): Boolean;
    function GetMACAlgorithms(Index: TSSHMacAlgorithm): Boolean;
    function GetPublicKeyAlgorithms(
      Index: TSSHPublicKeyAlgorithm): Boolean;
    procedure SetCompressionAlgorithms(Index: TSSHCompressionAlgorithm;
      const Value: Boolean);
    procedure SetEncryptionAlgorithms(Index: TSSHEncryptionAlgorithm;
      const Value: Boolean);
    procedure SetKexAlgorithms(Index: TSSHKexAlgorithm;
      const Value: Boolean);
    procedure SetMACAlgorithms(Index: TSSHMacAlgorithm;
      const Value: Boolean);
    procedure SetPublicKeyAlgorithms(Index: TSSHPublicKeyAlgorithm;
      const Value: Boolean);
    function GetActive: Boolean;
    function GetAuthenticationTypes: Integer;
    function GetClientHostname: string;
    function GetClientUsername: string;
    function GetCompressionAlgorithmClientToServer: TSSHCompressionAlgorithm;
    function GetCompressionAlgorithmServerToClient: TSSHCompressionAlgorithm;
    function GetCompressionLevel: Integer;
    function GetEncryptionAlgorithmClientToServer: TSSHEncryptionAlgorithm;
    function GetEncryptionAlgorithmServerToClient: TSSHEncryptionAlgorithm;
    function GetForceCompression: Boolean;
    function GetHost: string;
    function GetIOHandler: TIdIOHandler;
    function GetKexAlgorithm: TSSHKexAlgorithm;
    function GetKeyStorage: TElSSHCustomKeyStorage;
    function GetMacAlgorithmClientToServer: TSSHMacAlgorithm;
    function GetMacAlgorithmServerToClient: TSSHMacAlgorithm;
    function GetPassword: string;
    function GetPort: Integer;
    function GetPublicKeyAlgorithm: TSSHPublicKeyAlgorithm;
    function GetServerCloseReason: string;
    function GetServerSoftwareName: string;
    function GetSoftwareName: string;
    function GetUsername: string;
    function GetVersion: TSSHVersion;
    function GetVersions: TSSHVersions;
    procedure SetAuthenticationTypes(const Value: Integer);
    procedure SetClientHostname(const Value: string);
    procedure SetClientUsername(const Value: string);
    procedure SetCompressionLevel(const Value: Integer);
    procedure SetForceCompression(const Value: Boolean);
    procedure SetHost(const Value: string);
    procedure SetIOHandler(const Value: TIdIOHandler);
    procedure SetKeyStorage(const Value: TElSSHCustomKeyStorage);
    procedure SetPassword(const Value: string);
    procedure SetPort(const Value: Integer);
    procedure SetSoftwareName(const Value: string);
    procedure SetUsername(const Value: string);
    procedure SetVersions(const Value: TSSHVersions);
  protected
    FSSHClient: TElSSHClient;
    FTunnelList: TElSSHTunnelList;
    FTCPClient: TIdTCPClient;
    FSSHClientReadThread: TElIdSSHClientReadThread;
    FErrorOccured: Boolean;

    procedure TerminateClientReadThread;

    procedure DoAuthenticationFailed(Sender: TObject; AuthenticationType :
        Integer);
    procedure DoAuthenticationKeyboard(Sender: TObject; Prompts: TStringList;
        Echo: TBits; Responses: TStringList);
    procedure DoAuthenticationSuccess(Sender: TObject);
    procedure DoBanner(Sender: TObject; const Text: string; const Language: string);
    procedure DoCloseConnection(Sender: TObject);
    procedure DoDebugData(Sender: TObject; Buffer: Pointer; Size: LongInt);
    procedure DoError(Sender: TObject; ErrorCode: Integer);
    procedure DoKeyValidate(Sender: TObject; ServerKey: TElSSHKey;
        var Validate: Boolean);
    procedure DoOpenConnection(Sender: TObject);

    procedure DoSSHReceive(Sender: TObject; Buffer: pointer; MaxSize: longint; out
      Written: longint);
    procedure DoSSHSend(Sender: TObject; Buffer: pointer; Size: longint);
  public
{$IFDEF Indy100}
    procedure InitComponent; override;
{$ELSE}
    constructor Create(AOwner: TComponent); override;
{$ENDIF}
    destructor Destroy; override;

    procedure Connect;
    procedure Disconnect;

    property Active: Boolean read GetActive;
    property ServerSoftwareName: string read GetServerSoftwareName;
    property Version: TSSHVersion read GetVersion;
    property ServerCloseReason: string read GetServerCloseReason;

    property TunnelList: TElSSHTunnelList read FTunnelList;

    property EncryptionAlgorithmServerToClient: TSSHEncryptionAlgorithm
      read GetEncryptionAlgorithmServerToClient;
    property EncryptionAlgorithmClientToServer: TSSHEncryptionAlgorithm
      read GetEncryptionAlgorithmClientToServer;
    property CompressionAlgorithmServerToClient: TSSHCompressionAlgorithm
      read GetCompressionAlgorithmServerToClient;
    property CompressionAlgorithmClientToServer: TSSHCompressionAlgorithm
      read GetCompressionAlgorithmClientToServer;
    property MacAlgorithmServerToClient: TSSHMacAlgorithm read GetMacAlgorithmServerToClient;
    property MacAlgorithmClientToServer: TSSHMacAlgorithm read GetMacAlgorithmClientToServer;
    property KexAlgorithm: TSSHKexAlgorithm read GetKexAlgorithm;
    property PublicKeyAlgorithm: TSSHPublicKeyAlgorithm read GetPublicKeyAlgorithm;

    property EncryptionAlgorithms[Index: TSSHEncryptionAlgorithm]: Boolean
      read GetEncryptionAlgorithms write SetEncryptionAlgorithms;
    property CompressionAlgorithms[Index: TSSHCompressionAlgorithm]: Boolean
      read GetCompressionAlgorithms write SetCompressionAlgorithms;
    property MacAlgorithms[Index: TSSHMacAlgorithm]: Boolean
      read GetMACAlgorithms write SetMACAlgorithms;
    property KexAlgorithms[Index: TSSHKexAlgorithm]: Boolean
      read GetKexAlgorithms write SetKexAlgorithms;
    property PublicKeyAlgorithms[Index: TSSHPublicKeyAlgorithm]: Boolean
      read GetPublicKeyAlgorithms write SetPublicKeyAlgorithms;
  published
    property Host: string read GetHost write SetHost;
    property Port: Integer read GetPort write SetPort;
    property IOHandler: TIdIOHandler read GetIOHandler write SetIOHandler;

    property KeyStorage: TElSSHCustomKeyStorage read GetKeyStorage write
        SetKeyStorage;

    property AuthenticationTypes: Integer read GetAuthenticationTypes
      write SetAuthenticationTypes default SSH_AUTH_TYPE_PASSWORD;
    property ClientHostname: string read GetClientHostname
      write SetClientHostname;
    property ClientUsername: string read GetClientUsername
      write SetClientUsername;
    property CompressionLevel: Integer read GetCompressionLevel
      write SetCompressionLevel default 9;
    property ForceCompression: Boolean read GetForceCompression
      write SetForceCompression;
    property Password: string read GetPassword write SetPassword;
    property SoftwareName: string read GetSoftwareName write SetSoftwareName;
    property Username: string read GetUsername write SetUsername;
    property Versions: TSSHVersions read GetVersions write SetVersions
      default [sbSSH1, sbSSH2];

    property OnAuthenticationKeyboard: TSSHAuthenticationKeyboardEvent read
      FOnAuthenticationKeyboard write FOnAuthenticationKeyboard;
    property OnAuthenticationFailed: TSSHAuthenticationFailedEvent read
      FOnAuthenticationFailed write FOnAuthenticationFailed;
    property OnAuthenticationSuccess: TNotifyEvent read FOnAuthenticationSuccess
      write FOnAuthenticationSuccess;
    property OnBanner: TSSHBannerEvent read FOnBanner write FOnBanner;
    property OnCloseConnection: TSSHCloseConnectionEvent read FOnCloseConnection
      write FOnCloseConnection;
    property OnDebugData: TSSHDataEvent read FOnDebugData write FOnDebugData;
    property OnError: TSSHErrorEvent read FOnError write FOnError;
    property OnKeyValidate: TSSHKeyValidateEvent read FOnKeyValidate write
      FOnKeyValidate;
    property OnOpenConnection: TSSHOpenConnectionEvent read FOnOpenConnection
      write FOnOpenConnection;
  end;

  EElIdNotImplementedException = class(EIdException);

  EElIdSSHException = class(EIdException);

resourcestring
  sCannotConnect = 'Cannot connect';
  sNotConnected = 'Not connected';

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('SSHBlackbox', [TElIdSSHClient]);
end;

{ TElIdSSHClient }

procedure TElIdSSHClient.Connect;
begin
  if Active then
    try
      FSSHClient.Close;
      FTCPClient.Disconnect;
    except
    end;

  try
    FErrorOccured := False;
    TerminateClientReadThread;

    FTCPClient.Connect;

    if not FTCPClient.Connected then
      raise EElIdNotImplementedException.Create(sCannotConnect);

    FSSHClient.Open;

    FSSHClientReadThread := TElIdSSHClientReadThread.Create(Self);

    while (not FErrorOccured) and (not FSSHClient.Active) do
    begin
//      Application.ProcessMessages;
{$IFDEF Indy100}
      FTCPClient.IOHandler.CheckForDataOnSource;
{$ELSE}
      FTCPClient.ReadFromStack(False, 0, False);
{$ENDIF}
      FSSHClient.DataAvailable;
    end;

    if not Active then
      raise EElIdNotImplementedException.Create(sCannotConnect);
  except
    on E: Exception do
      raise EElIdSSHException.Create(sCannotConnect);
  end;
end;

{$ifdef INDY100}
procedure TElIdSSHClient.InitComponent;
{$else}
constructor TElIdSSHClient.Create(AOwner: TComponent);
{$endif}
begin
  inherited;

  FSSHClient := TElSSHClient.Create(nil);
  FTunnelList := TElSSHTunnelList.Create(nil);
  FSSHClient.TunnelList := FTunnelList;

{$IFDEF Indy100}
  FTCPClient := TIdTCPClient.Create;
{$ELSE}
  FTCPClient := TIdTCPClient.Create(nil);
{$ENDIF}

  FSSHClient.OnKeyValidate := DoKeyValidate;
  FSSHClient.OnAuthenticationSuccess := DoAuthenticationSuccess;
  FSSHClient.OnAuthenticationFailed := DoAuthenticationFailed;
  FSSHClient.OnAuthenticationKeyboard := DoAuthenticationKeyboard;
  FSSHClient.OnBanner := DoBanner;
  FSSHClient.OnSend := DoSSHSend;
  FSSHClient.OnReceive := DoSSHReceive;
  FSSHClient.OnCloseConnection := DoCloseConnection;
  FSSHClient.OnError := DoError;
  FSSHClient.OnOpenConnection := DoOpenConnection;
end;

destructor TElIdSSHClient.Destroy;
begin
  if Active then
    Disconnect;

  TerminateClientReadThread;

  FreeAndNil(FTCPClient);
  FreeAndNil(FTunnelList);
  FreeAndNil(FSSHClient);

  inherited;
end;

procedure TElIdSSHClient.Disconnect;
begin
  if Active then
  begin
    try
      FSSHClient.Close;
      FTCPClient.Disconnect;
      TerminateClientReadThread;
    except
    end;
  end
  else
    raise EElSSHException.Create(sNotConnected);
end;

procedure TElIdSSHClient.DoAuthenticationFailed(Sender: TObject;
  AuthenticationType: Integer);
begin
  if Assigned(FOnAuthenticationFailed) then
    FOnAuthenticationFailed(Self, AuthenticationType);
end;

procedure TElIdSSHClient.DoAuthenticationKeyboard(Sender: TObject;
  Prompts: TStringList; Echo: TBits; Responses: TStringList);
begin
  if Assigned(FOnAuthenticationKeyboard) then
    FOnAuthenticationKeyboard(Self, Prompts, Echo, Responses);
end;

procedure TElIdSSHClient.DoAuthenticationSuccess(Sender: TObject);
begin
  if Assigned(FOnAuthenticationSuccess) then
    FOnAuthenticationSuccess(Self);
end;

procedure TElIdSSHClient.DoBanner(Sender: TObject; const Text,
  Language: string);
begin
  if Assigned(FOnBanner) then
    FOnBanner(Self, Text, Language);
end;

procedure TElIdSSHClient.DoCloseConnection(Sender: TObject);
begin
  FTCPClient.Disconnect;
  TerminateClientReadThread;

  if Assigned(FOnCloseConnection) then
    FOnCloseConnection(Self);
end;

procedure TElIdSSHClient.DoDebugData(Sender: TObject; Buffer: Pointer;
  Size: Integer);
begin
  if Assigned(FOnDebugData) then
    FOnDebugData(Self, Buffer, Size);
end;

procedure TElIdSSHClient.DoError(Sender: TObject; ErrorCode: Integer);
begin
  FErrorOccured := True;

  if Assigned(FOnError) then
    FOnError(Self, ErrorCode);
end;

procedure TElIdSSHClient.DoKeyValidate(Sender: TObject;
  ServerKey: TElSSHKey; var Validate: Boolean);
begin
  if Assigned(FOnKeyValidate) then
    FOnKeyValidate(Self, ServerKey, Validate)
  else
    Validate := True;
end;

procedure TElIdSSHClient.DoOpenConnection(Sender: TObject);
begin
  if Assigned(FOnOpenConnection) then
    FOnOpenConnection(Self);
end;

procedure TElIdSSHClient.DoSSHReceive(Sender: TObject; Buffer: pointer;
  MaxSize: Integer; out Written: Integer);
{$IFDEF Indy100}
var
  Buf: TIdBytes;
{$ENDIF}
begin
{$IFDEF Indy100}
  Written := Min(FTCPClient.IOHandler.InputBuffer.Size, MaxSize);
{$ELSE}
  Written := Min(FTCPClient.InputBuffer.Size, MaxSize);
{$ENDIF}

  if Written > 0 then
  begin
{$IFDEF Indy100}
    Buf := nil;
    FTCPClient.IOHandler.InputBuffer.ExtractToBytes(Buf, Written);
    Written := Length(Buf);
    Move(Buf[0], Buffer^, Written);
{$ELSE}
    FTCPClient.InputBuffer.Position := 0;
    Written := FTCPClient.InputBuffer.Read(Buffer^, Written);
    FTCPClient.InputBuffer.Remove(Written);
{$ENDIF}
  end;
end;

procedure TElIdSSHClient.DoSSHSend(Sender: TObject; Buffer: Pointer;
  Size: Integer);
{$IFDEF Indy100}
var
  Buf: TIdBytes;
{$ENDIF}
begin
{$IFDEF Indy100}
  SetLength(Buf, Size);
  Move(Buffer^, Buf[0], Size);
  FTCPClient.IOHandler.Write(Buf);
{$ELSE}
  FTCPClient.IOHandler.Send(Buffer^, Size);
{$ENDIF}
end;

function TElIdSSHClient.GetActive: Boolean;
begin
  if FSSHClient <> nil then
    Result := FSSHClient.Active
  else
    Result := false;
end;

function TElIdSSHClient.GetAuthenticationTypes: Integer;
begin
  Result := FSSHClient.AuthenticationTypes;
end;

function TElIdSSHClient.GetClientHostname: string;
begin
  Result := FSSHClient.ClientHostName;
end;

function TElIdSSHClient.GetClientUsername: string;
begin
  Result := FSSHClient.ClientUserName;
end;

function TElIdSSHClient.GetCompressionAlgorithmClientToServer: TSSHCompressionAlgorithm;
begin
  Result := FSSHClient.CompressionAlgorithmClientToServer;
end;

function TElIdSSHClient.GetCompressionAlgorithms(
  Index: TSSHCompressionAlgorithm): Boolean;
begin
  Result := FSSHClient.CompressionAlgorithms[Index];
end;

function TElIdSSHClient.GetCompressionAlgorithmServerToClient: TSSHCompressionAlgorithm;
begin
  Result := FSSHClient.CompressionAlgorithmServerToClient;
end;

function TElIdSSHClient.GetCompressionLevel: Integer;
begin
  Result := FSSHClient.CompressionLevel;
end;

function TElIdSSHClient.GetEncryptionAlgorithmClientToServer: TSSHEncryptionAlgorithm;
begin
  Result := FSSHClient.EncryptionAlgorithmClientToServer;
end;

function TElIdSSHClient.GetEncryptionAlgorithms(
  Index: TSSHEncryptionAlgorithm): Boolean;
begin
  Result := FSSHClient.EncryptionAlgorithms[Index];
end;

function TElIdSSHClient.GetEncryptionAlgorithmServerToClient: TSSHEncryptionAlgorithm;
begin
  Result := FSSHClient.EncryptionAlgorithmServerToClient;
end;

function TElIdSSHClient.GetForceCompression: Boolean;
begin
  Result := FSSHClient.ForceCompression;
end;

function TElIdSSHClient.GetHost: string;
begin
  Result := FTCPClient.Host;
end;

function TElIdSSHClient.GetIOHandler: TIdIOHandler;
begin
  Result := FTCPClient.IOHandler;
end;

function TElIdSSHClient.GetKexAlgorithm: TSSHKexAlgorithm;
begin
  Result := FSSHClient.KexAlgorithm;
end;

function TElIdSSHClient.GetKexAlgorithms(Index: TSSHKexAlgorithm): Boolean;
begin
  Result := FSSHClient.KexAlgorithms[Index];
end;

function TElIdSSHClient.GetKeyStorage: TElSSHCustomKeyStorage;
begin
  Result := FSSHClient.KeyStorage;
end;

function TElIdSSHClient.GetMacAlgorithmClientToServer: TSSHMacAlgorithm;
begin
  Result := FSSHClient.MacAlgorithmClientToServer;
end;

function TElIdSSHClient.GetMACAlgorithms(Index: TSSHMacAlgorithm): Boolean;
begin
  Result := FSSHClient.MACAlgorithms[Index];
end;

function TElIdSSHClient.GetMacAlgorithmServerToClient: TSSHMacAlgorithm;
begin
  Result := FSSHClient.MacAlgorithmServerToClient;
end;

function TElIdSSHClient.GetPassword: string;
begin
  Result := FSSHClient.Password;
end;

function TElIdSSHClient.GetPort: Integer;
begin
  Result := FTCPClient.Port;
end;

function TElIdSSHClient.GetPublicKeyAlgorithm: TSSHPublicKeyAlgorithm;
begin
  Result := FSSHClient.PublicKeyAlgorithm;
end;

function TElIdSSHClient.GetPublicKeyAlgorithms(
  Index: TSSHPublicKeyAlgorithm): Boolean;
begin
  Result := FSSHClient.PublicKeyAlgorithms[Index];
end;

function TElIdSSHClient.GetServerCloseReason: string;
begin
  Result := FSSHClient.ServerCloseReason;
end;

function TElIdSSHClient.GetServerSoftwareName: string;
begin
  Result := FSSHClient.ServerSoftwareName;
end;

function TElIdSSHClient.GetSoftwareName: string;
begin
  Result := FSSHClient.SoftwareName;
end;

function TElIdSSHClient.GetUsername: string;
begin
  Result := FSSHClient.Username;
end;

function TElIdSSHClient.GetVersion: TSSHVersion;
begin
  Result := FSSHClient.Version;
end;

function TElIdSSHClient.GetVersions: TSSHVersions;
begin
  Result := FSSHClient.Versions;
end;

procedure TElIdSSHClient.SetAuthenticationTypes(const Value: Integer);
begin
  FSSHClient.AuthenticationTypes := Value;
end;

procedure TElIdSSHClient.SetClientHostname(const Value: string);
begin
  FSSHClient.ClientHostname := Value;
end;

procedure TElIdSSHClient.SetClientUsername(const Value: string);
begin
  FSSHClient.ClientUsername := Value;
end;

procedure TElIdSSHClient.SetCompressionAlgorithms(
  Index: TSSHCompressionAlgorithm; const Value: Boolean);
begin
  FSSHClient.CompressionAlgorithms[Index] := Value;
end;

procedure TElIdSSHClient.SetCompressionLevel(const Value: Integer);
begin
  FSSHClient.CompressionLevel := Value;
end;

procedure TElIdSSHClient.SetEncryptionAlgorithms(
  Index: TSSHEncryptionAlgorithm; const Value: Boolean);
begin
  FSSHClient.EncryptionAlgorithms[Index] := Value;
end;

procedure TElIdSSHClient.SetForceCompression(const Value: Boolean);
begin
  FSSHClient.ForceCompression := Value;
end;

procedure TElIdSSHClient.SetHost(const Value: string);
begin
  FTCPClient.Host := Value;
end;

procedure TElIdSSHClient.SetIOHandler(const Value: TIdIOHandler);
begin
  FTCPClient.IOHandler := Value;
end;

procedure TElIdSSHClient.SetKexAlgorithms(Index: TSSHKexAlgorithm;
  const Value: Boolean);
begin
  FSSHClient.KexAlgorithms[Index] := Value;
end;

procedure TElIdSSHClient.SetKeyStorage(
  const Value: TElSSHCustomKeyStorage);
begin
  FSSHClient.KeyStorage := Value;
end;

procedure TElIdSSHClient.SetMACAlgorithms(Index: TSSHMacAlgorithm;
  const Value: Boolean);
begin
  FSSHClient.MACAlgorithms[Index] := Value;
end;

procedure TElIdSSHClient.SetPassword(const Value: string);
begin
  FSSHClient.Password := Value;
end;

procedure TElIdSSHClient.SetPort(const Value: Integer);
begin
  FTCPClient.Port := Value;
end;

procedure TElIdSSHClient.SetPublicKeyAlgorithms(
  Index: TSSHPublicKeyAlgorithm; const Value: Boolean);
begin
  FSSHClient.PublicKeyAlgorithms[Index] := Value;
end;

procedure TElIdSSHClient.SetSoftwareName(const Value: string);
begin
  FSSHClient.SoftwareName := Value;
end;

procedure TElIdSSHClient.SetUsername(const Value: string);
begin
  FSSHClient.Username := Value;
end;

procedure TElIdSSHClient.SetVersions(const Value: TSSHVersions);
begin
  FSSHClient.Versions := Value;
end;

procedure TElIdSSHClient.TerminateClientReadThread;
begin
  if not Assigned(FSSHClientReadThread) then
    Exit;

  FSSHClientReadThread.Terminate;
  if not FSSHClientReadThread.FSynchronizing then
  begin
    FSSHClientReadThread.WaitFor;
    FreeAndNil(FSSHClientReadThread);
  end;  
end;

{ TElIdSSHClientThread }

constructor TElIdSSHClientReadThread.Create(AClient: TElIdSSHClient);
begin
  inherited Create(False);

  FClient := AClient;
  FreeOnTerminate := False;
end;

procedure TElIdSSHClientReadThread.Execute;
begin
  while not Terminated do
  begin
{$IFDEF Indy100}
    FClient.FTCPClient.IOHandler.CheckForDataOnSource;
    if FClient.FTCPClient.IOHandler.InputBuffer.Size > 0 then
{$ELSE}
    FClient.FTCPClient.ReadFromStack(False, 0, False);
    if FClient.FTCPClient.InputBuffer.Size > 0 then
{$ENDIF}
    begin
      FSynchronizing := True;
      Synchronize(FClient.FSSHClient.DataAvailable);
      FSynchronizing := False;
    end;

    if Assigned(FClient.FTCPClient) then
{$IFDEF Indy100}
      FClient.FTCPClient.IOHandler.CheckForDisconnect(False);
{$ELSE}
      FClient.FTCPClient.CheckForDisconnect(False);
{$ENDIF}

    Sleep(50);
  end;
end;

end.
