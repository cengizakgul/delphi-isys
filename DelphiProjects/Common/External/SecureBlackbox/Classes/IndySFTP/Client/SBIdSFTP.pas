(******************************************************)
(*                                                    *)
(*            EldoS SecureBlackbox Library            *)
(*                                                    *)
(*      Copyright (c) 2002-2006 EldoS Corporation     *)
(*           http://www.secureblackbox.com            *)
(*                                                    *)
(******************************************************)

// set this define is you are using Indy 10
{$define INDY100}
// set this define is you are using Indy 9
{.$define INDY90}

unit SBIdSFTP;

interface
uses
  SBUtils,
  SBSSHConstants,
  SBSSHCommon,
  SBSFTPCommon,
  SBSimpleSFTP,
  SBSSHKeyStorage,
  Classes,
  SysUtils,
  Masks,
  IdGlobal,
  IdBaseComponent,
  IdComponent,
  IdTcpClient,
  IdFTPCommon,
  IdFTPList
  {$IFDEF INDY100}
  ,IdIOHandlerStack
  {$ELSE}
  ,IdIOHandlerSocket
  {$ENDIF}
  ;

type
  TIdSFTPListItems = class;

  TIdSFTPListItem = class(TIdFTPListItem)
  private
    procedure SetPermissions(const Value: cardinal);
  protected
    FGroupName: string;
    FOwnerName: string;
    FCreationDate: TDateTime;
    FPermissions : Cardinal;
    FUnixPermissions: string;
  public
    procedure Assign(Source: TPersistent); override;
    constructor Create(Owner : TCollection); override;

    property GroupName : string read FGroupName write FGroupName;
    property OwnerName : string read FOwnerName write FOwnerName;
    property CreationDate : TDateTime read FCreationDate write FCreationDate;
    property Permissions : cardinal read FPermissions write SetPermissions;
    property UnixPermissions : string read FUnixPermissions;
  end;

  TIdSFTPListItems = class(TCollection)
  protected
    FDirectoryName: string;

    function GetItems(AIndex: Integer): TIdSFTPListItem;
    procedure SetItems(AIndex: Integer; const Value: TIdSFTPListItem);
  public
    function Add: TIdSFTPListItem;
    constructor Create; reintroduce;
    function IndexOf(AItem: TIdSFTPListItem): Integer;

    property DirectoryName: string read FDirectoryName write FDirectoryName;
    property Items[AIndex: Integer]: TIdSFTPListItem read GetItems write SetItems; default;
  end;

  TElIdSFTPClient = class(TIdComponent)
  private
    FDirectoryListing : TIdSFTPListItems;
    FListResult : TStringList;
    FTransferTimeout: Integer;
    FLastError : integer;
    FCurrentDir : string;
    FListReturned : boolean;
    FLoginError : boolean;
    FIdTCPClient : TIdTCPClient;
    FSFTPClient  : TElSimpleSFTPClient;
    FOnKeyValidate: TSSHKeyValidateEvent;
    FSFTPExt: TSBSftpExtendedAttributes;
    FTransferType : TIdFTPTransferType;

    procedure OnIncorrectPassword(Sender : TObject; AuthenticationType : integer);
    procedure SelfOnError(Sender : TObject; ErrorCode : integer);

    function GetDirectoryListing: TIdSFTPListItems;
    function GetListResult: TStrings;

    function GetHost: string;
    procedure SetHost(const Value: string);
    function GetResume: Boolean;

    //get's path to send to server, taking in account save FCurrentDir
    function RelativePathToServer(const ADirName : string) : string;

    procedure IdSocketSend(Sender : TObject; Buffer : pointer; Size : longint);
    procedure IdSocketRecv(Sender : TObject; Buffer : pointer; MaxSize : longint; out Written : longint);
    function GetActive: Boolean;
    function GetPassword: string;
    function GetUsername: string;
    procedure SetPassword(const Value: string);
    procedure SetUsername(const Value: string);
    function GetKeyStorage: TElSSHCustomKeyStorage;
    procedure SetKeyStorage(const Value: TElSSHCustomKeyStorage);
    function GetClientHostname: string;
    function GetClientUsername: string;
    function GetCompressionAlgorithmCS: TSSHCompressionAlgorithm;
    function GetCompressionAlgorithms(
      Index: TSSHCompressionAlgorithm): boolean;
    function GetCompressionAlgorithmSC: TSSHCompressionAlgorithm;
    function GetCompressionLevel: integer;
    function GetEncryptionAlgorithmCS: TSSHEncryptionAlgorithm;
    function GetEncryptionAlgorithms(
      Index: TSSHEncryptionAlgorithm): boolean;
    function GetEncryptionAlgorithmSC: TSSHEncryptionAlgorithm;
    function GetForceCompression: boolean;
    function GetKexAlgorithm: TSSHKexAlgorithm;
    function GetKexAlgorithms(Index: TSSHKexAlgorithm): boolean;
    function GetMacAlgorithmCS: TSSHMacAlgorithm;
    function GetMACAlgorithms(Index: TSSHMacAlgorithm): boolean;
    function GetMacAlgorithmSC: TSSHMacAlgorithm;
    function GetPublicKeyAlgorithm: TSSHPublicKeyAlgorithm;
    function GetPublicKeyAlgorithms(
      Index: TSSHPublicKeyAlgorithm): boolean;
    function GetServerCloseReason: string;
    function GetServerSoftwareName: string;
    function GetSoftwareName: string;
    function GetVersion: TSBSftpVersion;
    function GetVersions: TSBSftpVersions;
    procedure SetClientHostname(const Value: string);
    procedure SetClientUsername(const Value: string);
    procedure SetCompressionAlgorithms(Index: TSSHCompressionAlgorithm;
      const Value: boolean);
    procedure SetCompressionLevel(const Value: integer);
    procedure SetEncryptionAlgorithms(Index: TSSHEncryptionAlgorithm;
      const Value: boolean);
    procedure SetForceCompression(const Value: boolean);
    procedure SetKexAlgorithms(Index: TSSHKexAlgorithm;
      const Value: boolean);
    procedure SetMACAlgorithms(Index: TSSHMacAlgorithm;
      const Value: boolean);
    procedure SetPublicKeyAlgorithms(Index: TSSHPublicKeyAlgorithm;
      const Value: boolean);
    procedure SetSoftwareName(const Value: string);
    procedure SetVersions(const Value: TSBSftpVersions);
    function GetMessgaeLoop: TSBSftpMessageLoopEvent;
    procedure SetMessageLoop(const Value: TSBSftpMessageLoopEvent);
    function RunMessageLoop : boolean;
  protected
    FAutoLogin : boolean;
    function GetAddress: string;
    function GetAuthenticationTypes: integer;
    function GetPort: Integer;
    function GetNewline : string;
    procedure SetAddress(const Value: string);
    procedure SetAuthenticationTypes(Value : integer);
    procedure SetPort(Value: Integer);

    procedure DoKeyValidate(Sender: TObject; ServerKey: TElSSHKey;
        var Validate: Boolean);
  public
    // remember to set proper defines above
{$ifndef INDY100}
    constructor Create(AOwner : TSBComponentBase); override;
{$else}
    procedure InitComponent; override;
{$endif}
    property MessageLoop : TSBSftpMessageLoopEvent read GetMessgaeLoop write SetMessageLoop;
    procedure Abort; virtual;
    procedure ChangeDir(const ADirName: string);
    procedure ChangeDirUp;
    procedure Connect;
    destructor Destroy; override;
    procedure Delete(const AFilename: string);
    procedure Get(const ASourceFile: string; ADest: TStream; AResume: Boolean = false); overload;
    procedure Get(const ASourceFile, ADestFile: string; const ACanOverwrite: boolean = false; AResume: Boolean = false); overload;
    procedure List; overload;
    procedure List(const ASpecifier: string; ADetails: Boolean = True); overload;
    procedure List(ADest: TIdSFTPListItems; const ASpecifier: string = '';
      ADetails: Boolean = True); overload;
    function  FileDate(const AFileName : String; const AsGMT : Boolean = False): TDateTime;

    procedure Login;
    procedure MakeDir(const ADirName: string);
    procedure Noop;
    procedure Put(const ASource: TStream; const ADestFile: string;
     const AAppend: boolean = false); overload;
    procedure Put(const ASourceFile: string; const ADestFile: string = '';
     const AAppend: boolean = false); overload;

    procedure Quit;
    procedure RemoveDir(const ADirName: string);
    procedure Rename(const ASourceFile, ADestFile: string);
    function  RetrieveCurrentDir: string;
    function  Size(const AFileName: String): Int64;
    procedure SetModTime(const AFileName: String; const ALocalTime: TDateTime);
    procedure SetModTimeGMT(const AFileName : String; const AGMTTime: TDateTime);

    property CanResume: Boolean read GetResume;
    property DirectoryListing: TIdSFTPListItems read GetDirectoryListing;
    property ListResult: TStrings read GetListResult;

    property Active : Boolean read GetActive;
    property AutoLogin: Boolean read FAutoLogin write FAutoLogin;
    property Host : string read GetHost write SetHost;
    property TransferTimeout: Integer read FTransferTimeout write FTransferTimeout default 0;

    property CompressionAlgorithmClientToServer: TSSHCompressionAlgorithm read GetCompressionAlgorithmCS;
    property CompressionAlgorithms[Index: TSSHCompressionAlgorithm]: boolean read GetCompressionAlgorithms write SetCompressionAlgorithms;
    property CompressionAlgorithmServerToClient: TSSHCompressionAlgorithm read GetCompressionAlgorithmSC;
    property EncryptionAlgorithmClientToServer: TSSHEncryptionAlgorithm read GetEncryptionAlgorithmCS;
    property EncryptionAlgorithms[Index: TSSHEncryptionAlgorithm]: boolean read GetEncryptionAlgorithms write SetEncryptionAlgorithms;
    property EncryptionAlgorithmServerToClient: TSSHEncryptionAlgorithm read GetEncryptionAlgorithmSC;
    property KexAlgorithm: TSSHKexAlgorithm read GetKexAlgorithm;
    property KexAlgorithms[Index : TSSHKexAlgorithm]: boolean read GetKexAlgorithms write SetKexAlgorithms;

    property MacAlgorithmClientToServer: TSSHMacAlgorithm read GetMacAlgorithmCS;
    property MacAlgorithms[Index : TSSHMacAlgorithm]: boolean read GetMACAlgorithms write SetMACAlgorithms;
    property MacAlgorithmServerToClient: TSSHMacAlgorithm read GetMacAlgorithmSC;

    property PublicKeyAlgorithm: TSSHPublicKeyAlgorithm read GetPublicKeyAlgorithm;
    property PublicKeyAlgorithms[Index : TSSHPublicKeyAlgorithm]: boolean
      read GetPublicKeyAlgorithms write SetPublicKeyAlgorithms;
      
    property ServerCloseReason: string read GetServerCloseReason;
    property ServerSoftwareName: string read GetServerSoftwareName;
    property SFTPExt: TSBSftpExtendedAttributes read FSFTPExt write FSFTPExt;
    property Version: TSBSftpVersion read GetVersion;
  published
    property Username : string read GetUsername write SetUsername;
    property Password : string read GetPassword write SetPassword;
    property Port: Integer read GetPort write SetPort default 22;

    property Address: string read GetAddress write SetAddress;
    property AuthenticationTypes: integer read GetAuthenticationTypes write
        SetAuthenticationTypes default SSH_AUTH_TYPE_PASSWORD;
    property ClientHostname: string read GetClientHostname write SetClientHostname;
    property ClientUsername: string read GetClientUsername write SetClientUsername;
    property CompressionLevel: integer read GetCompressionLevel write
        SetCompressionLevel default 9;
    property ForceCompression: boolean read GetForceCompression write
        SetForceCompression;

    property SoftwareName: string read GetSoftwareName write SetSoftwareName;
    property Versions: TSBSftpVersions read GetVersions write SetVersions
        default [sbSFTP3];

    property KeyStorage: TElSSHCustomKeyStorage read GetKeyStorage write SetKeyStorage;
    property TransferType: TIdFTPTransferType read FTransferType write FTransferType default ftBinary;
    property Newline : string read GetNewline;

    property OnKeyValidate: TSSHKeyValidateEvent read FOnKeyValidate write
      FOnKeyValidate;

    property OnWork;
    property OnWorkBegin;
    property OnWorkEnd;
  end;

  EElIdSFTPException = class(EElSFTPError);
  EElIdSFTPConnectionException = class(EElIdSFTPException);
  EElIdSFTPOperationException = class(EElIdSFTPException);

procedure Register;

implementation
const
  SB_SFTP_BUFFER_SIZE = $10000;

  SB_DIR_CURRENT = '.';
  SB_DIR_UP = '..';

resourcestring
  SInvalidParameterType = 'Invalid parameter type';
  SFileIOError = 'File I/O error.';
  SFileAlreadyExists = 'File already exists.';
  SCannotCompleteOperation = 'Cannot complete operation';
  SConnectionError = 'Connection error';
  SIncorrectLogin = 'Incorrect login';
  SCannotChDir = 'Cannot change directory';
  SNotConnected = 'No connection established.';

procedure Register;
begin
  RegisterComponents('SFTPBlackbox', [TElIdSFTPClient]);
end;

{ TIdSFTPListItem }

procedure TIdSFTPListItem.Assign(Source: TPersistent);
begin
  if Source is TIdSFTPListItem then
  begin
    FGroupName := TIdSFTPListItem(Source).FGroupName;
    FOwnerName := TIdSFTPListItem(Source).FOwnerName;
    FCreationDate := TIdSFTPListItem(Source).FCreationDate;
    FPermissions := TIdSFTPListItem(Source).FPermissions;
    FUnixPermissions := TIdSFTPListItem(Source).FUnixPermissions;
  end;
  
  inherited Assign(Source);  
end;

constructor TIdSFTPListItem.Create(Owner: TCollection);
begin
  inherited;

  FOwnerName := '';
  FGroupName := '';
  FUnixPermissions := '----------';
  FPermissions := 0;
end;

procedure TIdSFTPListItem.SetPermissions(const Value: cardinal);
begin
  FPermissions := Value;

  FUnixPermissions := '';

  if ItemType = ditDirectory then
    FUnixPermissions := 'd'
  else
    FUnixPermissions := '-';

  if Value and 256 > 0 then
    FUnixPermissions := FUnixPermissions + 'r'
  else
    FUnixPermissions := FUnixPermissions + '-';
  if Value and 128 > 0 then
    FUnixPermissions := FUnixPermissions + 'w'
  else
    FUnixPermissions := FUnixPermissions + '-';
  if Value and 64 > 0 then
    FUnixPermissions := FUnixPermissions + 'x'
  else
    FUnixPermissions := FUnixPermissions + '-';
  if Value and 32 > 0 then
    FUnixPermissions := FUnixPermissions + 'r'
  else
    FUnixPermissions := FUnixPermissions + '-';
  if Value and 16 > 0 then
    FUnixPermissions := FUnixPermissions + 'w'
  else
    FUnixPermissions := FUnixPermissions + '-';
  if Value and 8 > 0 then
    FUnixPermissions := FUnixPermissions + 'x'
  else
    FUnixPermissions := FUnixPermissions + '-';
  if Value and 4 > 0 then
    FUnixPermissions := FUnixPermissions + 'r'
  else
    FUnixPermissions := FUnixPermissions + '-';
  if Value and 2 > 0 then
    FUnixPermissions := FUnixPermissions + 'w'
  else
    FUnixPermissions := FUnixPermissions + '-';
  if Value and 1 > 0 then
    FUnixPermissions := FUnixPermissions + 'x'
  else
    FUnixPermissions := FUnixPermissions + '-';
end;

{ TElIdSFTPClient }

procedure TElIdSFTPClient.Abort;
begin
  FSFTPClient.Close;
end;

procedure TElIdSFTPClient.ChangeDir(const ADirName: string);
var
  TmpDir : string;
  HDir : TSBSftpFileHandle;
begin
  TmpDir := RelativePathToServer(ADirName);

  FLastError := 0;
  try
    HDir := FSFTPClient.OpenDirectory(TmpDir);
    if FLastError = 0 then
    begin
      FSFTPClient.CloseHandle(HDir);
      FCurrentDir := FSFTPClient.RequestAbsolutePath(TmpDir);
      FListReturned := false;
      FDirectoryListing.Clear;      
    end;
  except
    on E : Exception do
      raise EElIdSFTPException.Create(SCannotChDir);
  end;
end;

procedure TElIdSFTPClient.ChangeDirUp;
begin
  ChangeDir(SB_DIR_UP);
end;

procedure TElIdSFTPClient.Connect;
begin
  FLoginError := false;

  try
    {$IFDEF Indy90}
    FIdTCPClient.Host := Address;
    FIdTCPClient.Port := Port;
    FIdTCPClient.Connect;
    {$ELSE}
    FIdTCPClient.Connect(Address, Port);
    {$ENDIF}
    FSFTPClient.Open;
  except
    on E : Exception do
      if FLoginError then
        raise EElIdSFTPConnectionException.Create(SIncorrectLogin)
      else
        raise EElIdSFTPConnectionException.Create(SConnectionError + ' (' + E.Message + ')')
  end;

  FCurrentDir := FSFTPClient.RequestAbsolutePath(SB_DIR_CURRENT);
end;

{$ifndef INDY100}
constructor TElIdSFTPClient.Create(AOwner: TSBComponentBase);
{$else}
procedure TElIdSFTPClient.InitComponent; 
{$endif}
begin
  inherited;

  FSFTPClient := TElSimpleSFTPClient.Create(nil);
  FIdTCPClient := TIdTCPClient.Create(nil);
  FDirectoryListing := TIdSFTPListItems.Create;
  FListResult := TStringList.Create;

  FSFTPClient.OnError := SelfOnError;
  FSFTPClient.OnAuthenticationFailed := OnIncorrectPassword;
  FSFTPClient.OnReceive := IdSocketRecv;
  FSFTPClient.OnSend := IdSocketSend;

  FSFTPClient.OnKeyValidate := DoKeyValidate;

  FCurrentDir := SB_DIR_CURRENT;
  FListReturned := false;
  FSFTPClient.UseInternalSocket := false;
  FSFTPClient.AuthenticationTypes := SSH_AUTH_TYPE_PASSWORD;
  FTransferType := ftBinary;
end;

procedure TElIdSFTPClient.Delete(const AFilename: string);
begin
  FSFTPClient.RemoveFile(RelativePathToServer(AFilename));
end;

destructor TElIdSFTPClient.Destroy;
begin
  FreeAndNil(FSFTPClient);
  FreeAndNil(FIdTCPClient);
  FreeAndNil(FDirectoryListing);
  FreeAndNil(FListResult);
  inherited;
end;

function TElIdSFTPClient.FileDate(const AFileName: String;
  const AsGMT: Boolean): TDateTime;
var
  Attributes : TElSftpFileAttributes;
begin
  Attributes := TElSftpFileAttributes.Create;
  try
    FSFTPClient.RequestAttributes(RelativePathToServer(AFileName), true, Attributes);
    if saMTime in Attributes.IncludedAttributes then
      Result := Attributes.MTime
    else
    if saCTime in Attributes.IncludedAttributes then
      Result := Attributes.CTime
    else
      Result := 0;
  finally
    Attributes.Free;
  end;  
end;

procedure TElIdSFTPClient.Get(const ASourceFile: string; ADest: TStream;
  AResume: Boolean);
var
  Attributes : TElSftpFileAttributes;
  FHandle : TSBSftpFileHandle;
  Buffer : ByteArray;
  ReadPos, ReadLen : Int64;
  Succ : boolean;
  Modes : TSBSftpFileOpenModes;
begin
  Attributes := TElSftpFileAttributes.Create;
  try
    // trying to request file attributes
    // by handle
    Modes := [SBSftpCommon.fmRead];
    if (FTransferType = ftASCII) and (FSFTPClient.Version in [sbSFTP4, sbSFTP5, sbSFTP6]) then
      Modes := Modes + [fmText];
    FHandle := FSFTPClient.OpenFile(RelativePathToServer(ASourceFile), Modes,
      Attributes);
    Succ := true;
    try
      FSFTPClient.RequestAttributes(FHandle, Attributes);
    except
      Succ := false;
    end;

    // by name
    if not Succ then
      try
        FSFTPClient.RequestAttributes(RelativePathToServer(ASourceFile), false,
          Attributes);
        Succ := true;
      except
        ;
      end;

    SetLength(Buffer, SB_SFTP_BUFFER_SIZE);
    ReadPos := 0;

    if Succ then
      BeginWork(wmRead, Attributes.Size)
    else
      BeginWork(wmRead, -1);

    repeat
      ReadLen := FSFTPClient.Read(FHandle, ReadPos, @Buffer[0], SB_SFTP_BUFFER_SIZE);
      ReadPos := ReadPos + ReadLen;
      ADest.WriteBuffer(Buffer[0], ReadLen);

      DoWork(wmRead, ReadLen);
      // Need to add some kind of reaction to check
      if not RunMessageLoop then break;
    until (ReadLen = 0);

    EndWork(wmRead);
  finally
    Attributes.Free;
  end;
end;

procedure TElIdSFTPClient.Get(const ASourceFile, ADestFile: string;
  const ACanOverwrite: boolean; AResume: Boolean);
var
  FStream : TFileStream;
begin
  if (not ACanOverwrite) and (FileExists(ADestFile)) then
    raise EElIdSFTPException.Create(SFileAlreadyExists);

  try
    FStream := TFileStream.Create(ADestFile, fmCreate);
  except
    raise EElIdSFTPException.Create(SFileIOError);
  end;

  try
    Get(ASourceFile, FStream, AResume);
  finally
    FStream.Free;
  end;
end;

function TElIdSFTPClient.GetAddress: string;
begin
  Result := FSFTPClient.Address;
end;

function TElIdSFTPClient.GetAuthenticationTypes: integer;
begin
  Result := FSFTPClient.AuthenticationTypes;
end;

function TElIdSFTPClient.GetListResult : TStrings;
begin
  Result := FListResult;

  if not FListReturned then
    try
      List;
    except
      Result := nil;
    end;
end;

function TElIdSFTPClient.GetDirectoryListing: TIdSFTPListItems;
begin
  Result := FDirectoryListing;

  if not FListReturned then
    try
      List;
    except
      Result := nil;
    end;
end;

function TElIdSFTPClient.GetHost: string;
begin
  Host := Address;
end;

function TElIdSFTPClient.GetNewline : string;
begin
  if FSFTPClient.ExtendedProperties.NewlineAvailable then
    Result := FSFTPClient.ExtendedProperties.NewlineExtension.Newline
  else
    Result := '';
end;

function TElIdSFTPClient.GetPort: Integer;
begin
  Result := FSFTPClient.Port;
end;

function TElIdSFTPClient.GetResume: Boolean;
begin
  Result := false;
end;

procedure TElIdSFTPClient.List;
var
  DHandle : TSBSftpFileHandle;
  DirListItem : TIdSFTPListItem;
  FileInfo : TElSftpFileInfo;
  DirList : TList;
  Index : integer;
begin
  FListReturned := false;
  DirList := TList.Create;

  try
    FLastError := 0;
    DHandle := FSFTPClient.OpenDirectory(FCurrentDir);
    FSFTPClient.ReadDirectory(DHandle, DirList);

    if (FLastError = 0) then
    begin
      FListReturned := true;
      FDirectoryListing.Clear;
      FListResult.Clear;
      FDirectoryListing.DirectoryName := FCurrentDir;

      for Index := 0 to DirList.Count - 1 do
      begin
        DirListItem := FDirectoryListing.Add;
        FileInfo := TElSftpFileInfo(DirList[Index]);

        if saSize in FileInfo.Attributes.IncludedAttributes then
        begin
          {$IFDEF Indy100}
          DirListItem.SizeAvail := true;
          {$ENDIF}
          DirListItem.Size := FileInfo.Attributes.Size;
        end
        {$IFDEF Indy100}
        else
          DirListItem.SizeAvail := false
        {$ENDIF};

        if saMTime in FileInfo.Attributes.IncludedAttributes then
        begin
          {$IFDEF Indy100}
          DirListItem.ModifiedAvail := true;
          {$ENDIF}
          DirListItem.ModifiedDate := FileInfo.Attributes.MTime;
        end
        {$IFDEF Indy100}
        else
          DirListItem.ModifiedAvail := false
        {$ENDIF};

        {$IFDEF Indy100}
        DirListItem.LocalFileName := FileInfo.Name;
        {$ENDIF}
        DirListItem.FileName := FileInfo.Name;

        case FileInfo.Attributes.FileType of
          ftFile        : DirListItem.ItemType := ditFile;
          ftDirectory   : DirListItem.ItemType := ditDirectory;
          ftSymblink    : DirListItem.ItemType := ditSymbolicLink;
          {$IFDEF Indy100}
{          ftSocket      : DirListItem.ItemType := ditSocket;
          ftCharDevice  : DirListItem.ItemType := ditCharDev;
          ftBlockDevice : DirListItem.ItemType := ditBlockDev;
          ftFIFO        : DirListItem.ItemType := ditFIFO;
}          {$ENDIF}
        else
          DirListItem.ItemType := ditFile;
        end;

        DirListItem.Data := FileInfo.LongName;
        DirListItem.GroupName := FileInfo.Attributes.Group;
        DirListItem.OwnerName := FileInfo.Attributes.Owner;
        DirListItem.Permissions := FileInfo.Attributes.Permissions;

        FListResult.Add(FileInfo.LongName);
      end;
    end;

    FSFTPClient.CloseHandle(DHandle);
  finally
    DirList.Free;
  end;
end;

procedure TElIdSFTPClient.IdSocketSend(Sender : TObject; Buffer : pointer;
  Size : longint);
{$IFDEF Indy100}
var
  Buf : TIdBytes;
{$ENDIF}
begin
  if FIdTCPClient.IOHandler.Connected then
  begin
    {$IFDEF Indy100}
    SetLength(Buf, Size);
    Move(Buffer^, Buf[0], Size);
    FIdTCPClient.IOHandler.Write(Buf);
    {$ELSE}
    FIdTCPClient.IOHandler.Send(Buffer^, Size);
    {$ENDIF}
  end
  else
    raise EElIdSFTPConnectionException.Create(SNotConnected);
end;

procedure TElIdSFTPClient.IdSocketRecv(Sender : TObject; Buffer : pointer;
  MaxSize : longint; out Written : longint);
{$IFDEF Indy100}  
var
  Buf : TIdBytes;
{$ENDIF}
begin
  if FIdTCPClient.IOHandler.Connected then
  begin
    {$IFDEF Indy100}
    if (FIdTCPClient.IOHandler.InputBuffer.Size = 0) then
    begin
      TIdIOHandlerStack(FIdTCPClient.IOHandler).CheckForDataOnSource;
      TIdIOHandlerStack(FIdTCPClient.IOHandler).CheckForDisconnect(false);
      Written := 0;
    end;
    if (FIdTCPClient.IOHandler.InputBuffer.Size > 0) then
    begin
      Written := SBUtils.Min(MaxSize, FIdTCPClient.IOHandler.InputBuffer.Size);
      SetLength(Buf, Written);
      FIdTCPClient.IOHandler.ReadBytes(Buf, 0, false);
      FIdTCPClient.IOHandler.InputBuffer.ExtractToBytes(Buf, Written, false);
      Move(Buf[0], Buffer^, Written);
    end;
    {$ELSE}
    if (FIdTCPClient.InputBuffer.Size = 0) then
    begin
      FIdTCPClient.ReadFromStack(false, 0, false);
      FIdTCPClient.CheckForDisconnect(false);
      Written := 0;
    end;
    if (FIdTCPClient.InputBuffer.Size > 0) then
    begin
      Written := SBUtils.Min(MaxSize, FIdTCPClient.InputBuffer.Size);
      FIdTCPClient.InputBuffer.Position := 0;
      Written := FIdTCPClient.InputBuffer.Read(Buffer^, Written);
      FIdTCPClient.InputBuffer.Remove(Written);
    end;
    {$ENDIF}  
  end
  else
    raise EElIdSFTPConnectionException.Create(SNotConnected);
end;

procedure TElIdSFTPClient.List(const ASpecifier: string;
  ADetails: Boolean);
var
  Index : integer;
begin
  List;

  if FListReturned then
  begin
    Index := 0;

    while (Index < FDirectoryListing.Count) do
    begin
      if (ASpecifier <> '') and (not MatchesMask(FDirectoryListing[Index].FileName, ASpecifier)) then
        FDirectoryListing.Delete(Index)
      else
        Inc(Index);
    end;  
  end;
end;

procedure TElIdSFTPClient.List(ADest: TIdSFTPListItems;
  const ASpecifier: string; ADetails: Boolean);
begin
  List(ASpecifier, ADetails);
  ADest.Assign(FDirectoryListing);
end;

procedure TElIdSFTPClient.Login;
begin
  FSFTPClient.Open;
end;

procedure TElIdSFTPClient.MakeDir(const ADirName: string);
var
  Attr : TElSftpFileAttributes;
begin
  Attr := TElSftpFileAttributes.Create;
  Attr.FileType := ftDirectory;
  try
    FSFTPClient.MakeDirectory(RelativePathToServer(ADirName), Attr);
  except
    on E : Exception do
      begin
        Attr.Free;
        raise EElIdSFTPOperationException.Create(SCannotCompleteOperation);
      end;  
  end;
end;

procedure TElIdSFTPClient.Noop;
begin
  //!! Noop. For true philosophers.
end;

procedure TElIdSFTPClient.OnIncorrectPassword(Sender : TObject; AuthenticationType : integer);
begin
  FLoginError := true;
end;

procedure TElIdSFTPClient.Put(const ASource: TStream;
  const ADestFile: string; const AAppend: boolean);
var
  FHandle : string;
  FPos : Int64;
  ReadBytes : integer;
  Buffer : ByteArray;
  Attr : TElSftpFileAttributes;
  Succ : boolean;
  Modes : TSBSftpFileOpenModes;
begin
  FLastError := 0;
  Attr := TElSftpFileAttributes.Create;
  try
    Attr.FileType := ftFile;
    if (FTransferType = ftASCII) and (FSFTPClient.Version in [sbSFTP4, sbSFTP5, sbSFTP6]) then
      Modes := [fmText]
    else
      Modes := [];
    if AAppend then
    begin
      Modes := Modes + [SBSftpCommon.fmAppend, SBSftpCommon.fmWrite, SBSftpCommon.fmCreate];
      FHandle := FSFTPClient.OpenFile(RelativePathToServer(ADestFile), Modes, Attr);
      Succ := true;
      try
        FSFTPClient.RequestAttributes(FHandle, Attr);
      except
        Succ := false;
      end;
      if not Succ then
        try
          FSFTPClient.RequestAttributes(RelativePathToServer(ADestFile), false,
            Attr);
          Succ := true;
        except
          ;
        end;
      if not Succ then
        Attr.Size := 0;
    end
    else
    begin      
      Modes := Modes + [SBSftpCommon.fmTruncate, SBSftpCommon.fmWrite, SBSftpCommon.fmCreate];
      FHandle := FSFTPClient.OpenFile(RelativePathToServer(ADestFile), Modes, Attr);
    end;
    if FLastError = 0 then
    begin
      if AAppend then
        FPos := Attr.Size
      else
        FPos := 0;

      SetLength(Buffer, SB_SFTP_BUFFER_SIZE);
      BeginWork(wmWrite, ASource.Size);

      while (ASource.Position < ASource.Size) and (FLastError = 0) do
      begin
        ReadBytes := ASource.Read(Buffer[0], SB_SFTP_BUFFER_SIZE);
        if (ReadBytes > 0) then
        begin
          FSFTPClient.Write(FHandle, FPos, @Buffer[0], ReadBytes);
          DoWork(wmWrite, ReadBytes);
          Inc(FPos, Int64(ReadBytes));
          // Need to add some kind of reaction to check
          if not RunMessageLoop then break;
        end;  
      end;

      EndWork(wmWrite);
      FSFTPClient.CloseHandle(FHandle);
    end;
  finally
    Attr.Free;
  end;
end;

procedure TElIdSFTPClient.Put(const ASourceFile, ADestFile: string;
  const AAppend: boolean);
var
  FStream : TFileStream;
begin
  try
    FStream := TFileStream.Create(ASourceFile, fmOpenRead);
  except
    raise EElIdSFTPException.Create(SFileIOError);
  end;

  try
    Put(FStream, ADestFile, AAppend);
  finally
    FStream.Free;
  end;
end;

procedure TElIdSFTPClient.Quit;
begin
  try
    FSFTPClient.Close;
  finally
    FIdTCPClient.Disconnect;
  end;  
end;

function TElIdSFTPClient.RelativePathToServer(
  const ADirName: string): string;
var
  UpPos, EnPos : integer;

  function PosEx(const SubStr, S: string; Offset: Cardinal = 1): Integer;
  var
    I,X: Integer;
    Len, LenSubStr: Integer;
  begin
    if Offset = 1 then
      Result := Pos(SubStr, S)
    else
    begin
      I := Offset;
      LenSubStr := Length(SubStr);
      Len := Length(S) - LenSubStr + 1;
      while I <= Len do
      begin
        if S[I] = SubStr[1] then
        begin
          X := 1;
          while (X < LenSubStr) and (S[I + X] = SubStr[X + 1]) do
            Inc(X);
          if (X = LenSubStr) then
          begin
            Result := I;
            exit;
          end;
        end;
        Inc(I);
      end;
      Result := 0;
    end;
  end;

begin
  if Length(ADirName) = 0 then
  begin
    Result := FCurrentDir;
    Exit;
  end;

  Result := ADirName;

  for UpPos := 1 to Length(Result) do
    if Result[UpPos] = '\' then Result[UpPos] := '/';

  if Result[1] = '/' then
    Exit;

  if (Length(FCurrentDir) > 0) and (FCurrentDir[Length(FCurrentDir)] = '/') then
    Result := FCurrentDir + Result
  else
    Result := FCurrentDir + '/' + Result;

  UpPos := 0;
  repeat
    UpPos := PosEx(SB_DIR_UP, Result, UpPos + 1);

    if UpPos > 3 then
    begin
      EnPos := UpPos + 1;

      if Copy(Result, UpPos - 3, 2) = SB_DIR_UP then Continue;

      Dec(UpPos, 2);
      while (Result[UpPos] <> '/') and (UpPos > 1) do Dec(UpPos);
      System.Delete(Result, UpPos, EnPos - UpPos + 1);
    end;
  until UpPos <= 1;

  if Result = '' then
    Result := '/';
end;

procedure TElIdSFTPClient.RemoveDir(const ADirName: string);
begin
  try
    FSFTPClient.RemoveDirectory(RelativePathToServer(ADirName));
  except
    on E : Exception do
      raise EElIdSFTPOperationException.Create(SCannotCompleteOperation);
  end;  
end;

procedure TElIdSFTPClient.Rename(const ASourceFile, ADestFile: string);
begin
  try
    FSFTPClient.RenameFile(RelativePathToServer(ASourceFile), RelativePathToServer(ADestFile));
  except
    on E : Exception do
      raise EElIdSFTPOperationException.Create(SCannotCompleteOperation);
  end;
end;

function TElIdSFTPClient.RetrieveCurrentDir: string;
begin
  Result := FCurrentDir;
end;

procedure TElIdSFTPClient.SelfOnError(Sender: TObject; ErrorCode: integer);
begin
  FLastError := ErrorCode;

  //if Assigned(FOnFtpError) then
  //  FOnFtpError(Sender, ErrorCode);
end;

procedure TElIdSFTPClient.SetAddress(const Value: string);
begin
  FSFTPClient.Address := Value
end;

procedure TElIdSFTPClient.SetAuthenticationTypes(Value : integer);
begin
  FSFTPClient.AuthenticationTypes := Value;
end;

procedure TElIdSFTPClient.SetHost(const Value: string);
begin
  FSFTPClient.Address := Value;
end;

procedure TElIdSFTPClient.SetModTime(const AFileName: String;
  const ALocalTime: TDateTime);
var
  Attributes : TElSftpFileAttributes;
begin
  Attributes := TElSftpFileAttributes.Create;

  try
    Attributes.MTime := ALocalTime;
    Attributes.IncludedAttributes := [saMTime];
    FSFTPClient.SetAttributes(RelativePathToServer(AFileName), Attributes);
  finally
    Attributes.Free;
  end;
end;

procedure TElIdSFTPClient.SetModTimeGMT(const AFileName: String;
  const AGMTTime: TDateTime);
begin
  SetModTime(AFileName, AGMTTime);
end;

procedure TElIdSFTPClient.SetPort(Value: Integer);
begin
  FSFTPClient.Port := Value
end;

function TElIdSFTPClient.Size(const AFileName: String): Int64;
var
  Attributes : TElSftpFileAttributes;
begin
  Attributes := TElSftpFileAttributes.Create;
  try
    FSFTPClient.RequestAttributes(RelativePathToServer(AFileName), true, Attributes);
    if saSize in Attributes.IncludedAttributes then
      Result := Attributes.Size
    else
      Result := -1;
  finally
    Attributes.Free;
  end;
end;

function TElIdSFTPClient.GetActive: Boolean;
begin
  Result := FSFTPClient.Active;
end;

function TElIdSFTPClient.GetPassword: string;
begin
  Result := FSFTPClient.Password;
end;

function TElIdSFTPClient.GetUsername: string;
begin
  Result := FSFTPClient.Username;
end;

procedure TElIdSFTPClient.SetPassword(const Value: string);
begin
  FSFTPClient.Password := Value;
end;

procedure TElIdSFTPClient.SetUsername(const Value: string);
begin
  FSFTPClient.Username := Value;
end;

function TElIdSFTPClient.GetKeyStorage: TElSSHCustomKeyStorage;
begin
  Result := FSFTPClient.KeyStorage;
end;

procedure TElIdSFTPClient.SetKeyStorage(
  const Value: TElSSHCustomKeyStorage);
begin
  FSFTPClient.KeyStorage := Value;
end;

procedure TElIdSFTPClient.DoKeyValidate(Sender: TObject;
  ServerKey: TElSSHKey; var Validate: Boolean);
begin
  if Assigned(FOnKeyValidate) then
    FOnKeyValidate(Self, ServerKey, Validate)
  else
    Validate := True;
end;

function TElIdSFTPClient.GetClientHostname: string;
begin
  Result := FSFTPClient.ClientHostName;
end;

function TElIdSFTPClient.GetClientUsername: string;
begin
  Result := FSFTPClient.ClientUsername;
end;

function TElIdSFTPClient.GetCompressionAlgorithmCS: TSSHCompressionAlgorithm;
begin
  Result := FSFTPClient.CompressionAlgorithmClientToServer;
end;

function TElIdSFTPClient.GetCompressionAlgorithms(
  Index: TSSHCompressionAlgorithm): boolean;
begin
  Result := FSFTPClient.CompressionAlgorithms[Index];
end;

function TElIdSFTPClient.GetCompressionAlgorithmSC: TSSHCompressionAlgorithm;
begin
  Result := FSFTPClient.CompressionAlgorithmServerToClient;
end;

function TElIdSFTPClient.GetCompressionLevel: integer;
begin
  Result := FSFTPClient.CompressionLevel;
end;

function TElIdSFTPClient.GetEncryptionAlgorithmCS: TSSHEncryptionAlgorithm;
begin
  Result := FSFTPClient.EncryptionAlgorithmClientToServer;
end;

function TElIdSFTPClient.GetEncryptionAlgorithms(
  Index: TSSHEncryptionAlgorithm): boolean;
begin
  Result := FSFTPClient.EncryptionAlgorithms[Index];
end;

function TElIdSFTPClient.GetEncryptionAlgorithmSC: TSSHEncryptionAlgorithm;
begin
  Result := FSFTPClient.EncryptionAlgorithmServerToClient;
end;

function TElIdSFTPClient.GetForceCompression: boolean;
begin
  Result := FSFTPClient.ForceCompression;
end;

function TElIdSFTPClient.GetKexAlgorithm: TSSHKexAlgorithm;
begin
  Result := FSFTPClient.KexAlgorithm;
end;

function TElIdSFTPClient.GetKexAlgorithms(
  Index: TSSHKexAlgorithm): boolean;
begin
  Result := FSFTPClient.KexAlgorithms[Index];
end;

function TElIdSFTPClient.GetMacAlgorithmCS: TSSHMacAlgorithm;
begin
  Result := FSFTPClient.MacAlgorithmClientToServer;
end;

function TElIdSFTPClient.GetMACAlgorithms(
  Index: TSSHMacAlgorithm): boolean;
begin
  Result := FSFTPClient.MACAlgorithms[Index];
end;

function TElIdSFTPClient.GetMacAlgorithmSC: TSSHMacAlgorithm;
begin
  Result := FSFTPClient.MacAlgorithmServerToClient;
end;

function TElIdSFTPClient.GetPublicKeyAlgorithm: TSSHPublicKeyAlgorithm;
begin
  Result := FSFTPClient.PublicKeyAlgorithm;
end;

function TElIdSFTPClient.GetPublicKeyAlgorithms(
  Index: TSSHPublicKeyAlgorithm): boolean;
begin
  Result := FSFTPClient.PublicKeyAlgorithms[Index];
end;

function TElIdSFTPClient.GetServerCloseReason: string;
begin
  Result := FSFTPClient.ServerCloseReason;
end;

function TElIdSFTPClient.GetServerSoftwareName: string;
begin
  Result := FSFTPClient.ServerSoftwareName;
end;

function TElIdSFTPClient.GetSoftwareName: string;
begin
  Result := FSFTPClient.SoftwareName;
end;

function TElIdSFTPClient.GetVersion: TSBSftpVersion;
begin
  Result := FSFTPClient.Version;
end;

function TElIdSFTPClient.GetVersions: TSBSftpVersions;
begin
  Result := FSFTPClient.Versions;
end;

procedure TElIdSFTPClient.SetClientHostname(const Value: string);
begin
  FSFTPClient.ClientHostname := Value;
end;

procedure TElIdSFTPClient.SetClientUsername(const Value: string);
begin
  FSFTPClient.ClientUsername := Value;
end;

procedure TElIdSFTPClient.SetCompressionAlgorithms(
  Index: TSSHCompressionAlgorithm; const Value: boolean);
begin
  FSFTPClient.CompressionAlgorithms[Index] := Value;
end;

procedure TElIdSFTPClient.SetCompressionLevel(const Value: integer);
begin
  FSFTPClient.CompressionLevel := Value;
end;

procedure TElIdSFTPClient.SetEncryptionAlgorithms(
  Index: TSSHEncryptionAlgorithm; const Value: boolean);
begin
  FSFTPClient.EncryptionAlgorithms[Index] := Value;
end;

procedure TElIdSFTPClient.SetForceCompression(const Value: boolean);
begin
  FSFTPClient.ForceCompression := Value;
end;

procedure TElIdSFTPClient.SetKexAlgorithms(Index: TSSHKexAlgorithm;
  const Value: boolean);
begin
  FSFTPClient.KexAlgorithms[Index] := Value;
end;

procedure TElIdSFTPClient.SetMACAlgorithms(Index: TSSHMacAlgorithm;
  const Value: boolean);
begin
  FSFTPClient.MACAlgorithms[Index] := Value;
end;

procedure TElIdSFTPClient.SetPublicKeyAlgorithms(
  Index: TSSHPublicKeyAlgorithm; const Value: boolean);
begin
  FSFTPClient.PublicKeyAlgorithms[Index] := Value;
end;

procedure TElIdSFTPClient.SetSoftwareName(const Value: string);
begin
  FSFTPClient.SoftwareName := Value;
end;

procedure TElIdSFTPClient.SetVersions(const Value: TSBSftpVersions);
begin
  FSFTPClient.Versions := Value;
end;

function TElIdSFTPClient.GetMessgaeLoop: TSBSftpMessageLoopEvent;
begin
  try
    Result:=FSFTPClient.MessageLoop;
  except Result:=nil; end;
end;

procedure TElIdSFTPClient.SetMessageLoop(
  const Value: TSBSftpMessageLoopEvent);
begin
  try
    if Assigned(FSFTPCLient) then
      FSFTPClient.MessageLoop:=Value;
  except end;
end;

function TElIdSFTPClient.RunMessageLoop: boolean;
begin
  try
    if Assigned(FSFTPClient.MessageLoop)
     then Result:=FSFTPClient.MessageLoop;
  except Result:=True; end;
end;

{ TIdSFTPListItems }

function TIdSFTPListItems.Add: TIdSFTPListItem;
begin
  Result := TIdSFTPListItem(inherited Add);
end;

constructor TIdSFTPListItems.Create;
begin
  inherited Create(TIdSFTPListItem);
  FDirectoryName := '';
end;

function TIdSFTPListItems.GetItems(AIndex: Integer): TIdSFTPListItem;
begin
  Result := TIdSFTPListItem(inherited Items[AIndex]);
end;

function TIdSFTPListItems.IndexOf(AItem: TIdSFTPListItem): Integer;
var
  I: integer;
begin
  Result := -1;
  for I := 0 to Count - 1 do
    if AItem = Items[I] then begin
      Result := I;
      Break;
    end;
end;

procedure TIdSFTPListItems.SetItems(AIndex: Integer;
  const Value: TIdSFTPListItem);
begin
  inherited Items[AIndex] := Value;
end;

begin
end.
