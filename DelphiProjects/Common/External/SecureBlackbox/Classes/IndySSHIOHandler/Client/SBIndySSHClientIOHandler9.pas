unit SBIndySSHClientIOHandler9;
interface
uses
  SBUtils, SBSSHConstants, SBSSHCommon, SBSSHClient, SBSSHTerm, SBSSHKeyStorage,
  IdGlobal, IdIOHandler, IdException, IdComponent, IdTCPClient, IdTCPConnection,
  Classes, SysUtils;

type
  TElClientIndySSHTransport = class(TComponent)
  protected
    FOnAuthenticationSuccess: TNotifyEvent;
    FOnAuthenticationFailed: TSSHAuthenticationFailedEvent;
    FOnAuthenticationKeyboard: TSSHAuthenticationKeyboardEvent;
    FOnBanner: TSSHBannerEvent;
    FOnCloseConnection: TSSHCloseConnectionEvent;
    FOnOpenConnection: TSSHOpenConnectionEvent;
    FOnError: TSSHErrorEvent;
    FOnKeyValidate: TSSHKeyValidateEvent;

    FSSHClient : TElSSHClient;
    FTCPClient: TIdTCPClient;
    FErrorOccured : boolean;
    FTunnelList : TElSSHTunnelList;
    FLastError : integer;
    FActive : boolean;

    function GetCloseIfNoActiveTunnels: boolean;
    function GetActive: boolean;
    function GetAuthenticationTypes: integer;
    function GetHost: string;
    function GetIOHandler: TIdIOHandler;
    function GetPort: integer;
    function GetClientHostname: string;
    function GetClientUsername: string;
    function GetCompressionAlgorithmCS: TSSHCompressionAlgorithm;
    function GetCompressionAlgorithms(
      Index: TSSHCompressionAlgorithm): boolean;
    function GetCompressionAlgorithmSC: TSSHCompressionAlgorithm;
    function GetCompressionLevel: integer;
    function GetEncryptionAlgorithmCS: TSSHEncryptionAlgorithm;
    function GetEncryptionAlgorithms(
      Index: TSSHEncryptionAlgorithm): boolean;
    function GetEncryptionAlgorithmSC: TSSHEncryptionAlgorithm;
    function GetForceCompression: boolean;
    function GetKexAlgorithm: TSSHKexAlgorithm;
    function GetKexAlgorithms(Index: TSSHKexAlgorithm): boolean;
    function GetKeyStorage: TElSSHCustomKeyStorage;
    function GetMacAlgorithmCS: TSSHMacAlgorithm;
    function GetMACAlgorithms(Index: TSSHMacAlgorithm): boolean;
    function GetMacAlgorithmSC: TSSHMacAlgorithm;
    function GetPassword: string;
    function GetPublicKeyAlgorithm: TSSHPublicKeyAlgorithm;
    function GetPublicKeyAlgorithms(
      Index: TSSHPublicKeyAlgorithm): boolean;
    function GetServerCloseReason: string;
    function GetServerSoftwareName: string;
    function GetSoftwareName: string;
    function GetUsername: string;
    function GetVersion: TSSHVersion;
    function GetVersions: TSSHVersions;
    procedure SetAuthenticationTypes(const Value: integer);
    procedure SetClientHostname(const Value: string);
    procedure SetClientUsername(const Value: string);
    procedure SetCloseIfNoActiveTunnels(const Value: boolean);    
    procedure SetCompressionAlgorithms(Index: TSSHCompressionAlgorithm;
      const Value: boolean);
    procedure SetCompressionLevel(const Value: integer);
    procedure SetEncryptionAlgorithms(Index: TSSHEncryptionAlgorithm;
      const Value: boolean);
    procedure SetForceCompression(const Value: boolean);
    procedure SetKexAlgorithms(Index: TSSHKexAlgorithm;
      const Value: boolean);
    procedure SetKeyStorage(const Value: TElSSHCustomKeyStorage);
    procedure SetMACAlgorithms(Index: TSSHMacAlgorithm;
      const Value: boolean);
    procedure SetPassword(const Value: string);
    procedure SetPublicKeyAlgorithms(Index: TSSHPublicKeyAlgorithm;
      const Value: boolean);
    procedure SetSoftwareName(const Value: string);
    procedure SetUsername(const Value: string);
    procedure SetVersions(const Value: TSSHVersions);
    procedure SetHost(const Value: string);
    procedure SetIOHandler(const Value: TIdIOHandler);
    procedure SetPort(const Value: integer);    

    procedure DoSSHAuthenticationFailed(Sender : TObject; AuthenticationType :
        integer);
    procedure DoSSHAuthenticationKeyboard(Sender : TObject; Prompts : TStringList;
        Echo : TBits; Responses : TStringList);
    procedure DoSSHAuthenticationSuccess(Sender : TObject);
    procedure DoSSHOpenConnection(Sender : TObject);
    procedure DoSSHCloseConnection(Sender : TObject);
    procedure DoSSHError(Sender : TObject; ErrorCode : integer);
    procedure DoSSHKeyValidate(Sender : TObject; ServerKey : TElSSHKey; var Validate :
      boolean);
    procedure DoSSHBanner(Sender: TObject; const Text : string; const Language : string);
    procedure DoSSHReceive(Sender : TObject; Buffer : pointer; MaxSize : longint; out
      Written : longint);
    procedure DoSSHSend(Sender : TObject; Buffer : pointer; Size : longint);
    procedure DoTCPDisconnected(Sender : TObject);
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    procedure Connect;
    procedure Disconnect;

    property Active : boolean read GetActive;
    property ServerSoftwareName : string read GetServerSoftwareName;
    property Version : TSSHVersion read GetVersion;
    property ServerCloseReason : string read GetServerCloseReason;
    property EncryptionAlgorithmServerToClient : TSSHEncryptionAlgorithm
      read GetEncryptionAlgorithmSC;
    property EncryptionAlgorithmClientToServer : TSSHEncryptionAlgorithm
      read GetEncryptionAlgorithmCS;
    property CompressionAlgorithmServerToClient : TSSHCompressionAlgorithm
      read GetCompressionAlgorithmSC;
    property CompressionAlgorithmClientToServer : TSSHCompressionAlgorithm
      read GetCompressionAlgorithmCS;
    property MacAlgorithmServerToClient : TSSHMacAlgorithm read GetMacAlgorithmSC;
    property MacAlgorithmClientToServer : TSSHMacAlgorithm read GetMacAlgorithmCS;
    property KexAlgorithm : TSSHKexAlgorithm read GetKexAlgorithm;
    property PublicKeyAlgorithm : TSSHPublicKeyAlgorithm read GetPublicKeyAlgorithm;

    property EncryptionAlgorithms[Index : TSSHEncryptionAlgorithm] : boolean
      read GetEncryptionAlgorithms write SetEncryptionAlgorithms;
    property CompressionAlgorithms[Index : TSSHCompressionAlgorithm] : boolean
      read GetCompressionAlgorithms write SetCompressionAlgorithms;
    property MacAlgorithms[Index : TSSHMacAlgorithm] : boolean
      read GetMACAlgorithms write SetMACAlgorithms;
    property KexAlgorithms[Index : TSSHKexAlgorithm] : boolean
      read GetKexAlgorithms write SetKexAlgorithms;
    property PublicKeyAlgorithms[Index : TSSHPublicKeyAlgorithm] : boolean
      read GetPublicKeyAlgorithms write SetPublicKeyAlgorithms;
  published
    property Host : string read GetHost write SetHost;
    property Port : integer read GetPort write SetPort;
    property IOHandler : TIdIOHandler read GetIOHandler write SetIOHandler;

    property KeyStorage: TElSSHCustomKeyStorage read GetKeyStorage write
        SetKeyStorage;
        
    property AuthenticationTypes : integer read GetAuthenticationTypes
      write SetAuthenticationTypes default SSH_AUTH_TYPE_PASSWORD;
    property ClientHostname : string read GetClientHostname
      write SetClientHostname;
    property ClientUsername : string read GetClientUsername
      write SetClientUsername;
    property CloseIfNoActiveTunnels : boolean read GetCloseIfNoActiveTunnels
      write SetCloseIfNoActiveTunnels;
    property CompressionLevel : integer read GetCompressionLevel
      write SetCompressionLevel default 9;
    property ForceCompression : boolean read GetForceCompression
      write SetForceCompression;
    property Password : string read GetPassword write SetPassword;
    property SoftwareName : string read GetSoftwareName write SetSoftwareName;
    property Username : string read GetUsername write SetUsername;
    property Versions : TSSHVersions read GetVersions write SetVersions;

    property OnAuthenticationKeyboard: TSSHAuthenticationKeyboardEvent read
      FOnAuthenticationKeyboard write FOnAuthenticationKeyboard;
    property OnAuthenticationFailed: TSSHAuthenticationFailedEvent read
      FOnAuthenticationFailed write FOnAuthenticationFailed;
    property OnAuthenticationSuccess: TNotifyEvent read FOnAuthenticationSuccess
      write FOnAuthenticationSuccess;
    property OnBanner: TSSHBannerEvent read FOnBanner write FOnBanner;
    property OnError: TSSHErrorEvent read FOnError write FOnError;
    property OnCloseConnection: TSSHCloseConnectionEvent read FOnCloseConnection
      write FOnCloseConnection;
    property OnKeyValidate: TSSHKeyValidateEvent read FOnKeyValidate write
      FOnKeyValidate;
    property OnOpenConnection : TSSHOpenConnectionEvent read FOnOpenConnection
      write FOnOpenConnection;
  end;

  TElClientIndySSHIOHandlerSocket = class(TIdIOHandler)
  private
    procedure SetOwnTunnel(const Value: boolean);
  protected
    FConnection : TElSSHTunnelConnection;
    FTransport: TElClientIndySSHTransport;
    FTunnel : TElCustomSSHTunnel;
    FTunnelType: TSSHTunnelType;
    FActive : boolean;
    FOwnTunnel : boolean;
    FErrorOccured : boolean;
    FOldOnOpen : TSSHOpenConnectionEvent;
    FOldOnError : TSSHErrorEvent;
    FInputBuffer : TIdManagedBuffer;
    FDestroyConnection : boolean;

    function GetAuthenticationProtocol: string;
    function GetCommand: string;
    function GetEnvironment: TStringList;
    function GetSubsystem: string;
    function GetTerminalInfo: TElTerminalInfo;
    function GetToHost: string;
    function GetToPort: integer;
    function GetScreenNumber: integer;

    procedure SetAuthenticationProtocol(const Value: string);
    procedure SetCommand(const Value: string);
    procedure SetSubsystem(const Value: string);
    procedure SetTerminalInfo(const Value: TElTerminalInfo);
    procedure SetTransport(const Value: TElClientIndySSHTransport);
    procedure SetTunnelType(const Value: TSSHTunnelType);
    procedure SetToPort(const Value: integer);
    procedure SetScreenNumber(const Value: integer);
    procedure SetToHost(const Value: string);
    procedure SetTunnel(const Value: TElCustomSSHTunnel);

    {function ReadFromSource(ARaiseExceptionIfDisconnected: Boolean;
      ATimeout: Integer; ARaiseExceptionOnTimeout: Boolean): Integer; override;}

    procedure DoOnClose(Sender : TObject; CloseType : TSSHCloseType);
    procedure DoOnData(Sender : TObject; Buffer : pointer; Size : longint);
    procedure DoOnError(Sender : TObject; ErrorCode : integer);
    procedure DoOnOpen(Sender : TObject; TunnelConnection : TElSSHTunnelConnection);
    procedure DoOnTunnelError(Sender : TObject; ErrorCode : integer; Data : pointer);
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    procedure Close; override;
    {procedure CheckForDisconnect(ARaiseExceptionIfDisconnected: boolean = true;
      AIgnoreBuffer: boolean = false); override;}
    {procedure CheckForDataOnSource(ATimeout : Integer = 0); override;}
    function Connected : boolean; override;
    procedure Open; override;
    function Readable(AMSec: Integer = IdTimeoutDefault): Boolean; override;
    function Recv(var ABuf; ALen: Integer): Integer; override;
    function Send(var ABuf; ALen: Integer): Integer; override;
    {procedure WriteDirect(ABuffer : TIdBytes); override;}
  published
    property OwnTunnel : boolean read FOwnTunnel write SetOwnTunnel;
    property Tunnel : TElCustomSSHTunnel read FTunnel write SetTunnel;
    property Transport : TElClientIndySSHTransport read FTransport write SetTransport;
    property TunnelType : TSSHTunnelType read FTunnelType write SetTunnelType;
    { terminal tunnels }
    property TerminalInfo : TElTerminalInfo read GetTerminalInfo write SetTerminalInfo;
    property Environment : TStringList read GetEnvironment;
    { command tunnel }
    property Command : string read GetCommand write SetCommand;
    { local/remote port forwarding }
    property ToHost : string read GetToHost write SetToHost;
    property ToPort : integer read GetToPort write SetToPort;
    { custom subsystem tunnel }
    property Subsystem : string read GetSubsystem write SetSubsystem;
    { X11 forwarding tunnel }
    property AuthenticationProtocol : string read GetAuthenticationProtocol
      write SetAuthenticationProtocol;
    property ScreenNumber : integer read GetScreenNumber write SetScreenNumber;
  end;

  EElIdNotImplementedException = class(EIdException);
  EElIdSSHException = class(EIdException);

  procedure Register;
implementation
uses IdIOHandlerSocket, IdSocketHandle;

resourcestring
  SNotImplemented = 'Not implemented';
  SDisconnected = 'Disconnected';
  SCannotConnect = 'Cannot connect';
  SNotConnected = 'Not connected';
  SErrorSocketRead = 'Error while reading from socket';
  SSSHException = 'SSH protocol exception';
  SSSHTransportAlreadySet = 'SSH transport already set';
  SSSHCannotOpenTunnel = 'Cannot open SSH tunnel';

procedure Register;
begin
  RegisterComponents('SSHBlackbox', [TElClientIndySSHIOHandlerSocket,
    TElClientIndySSHTransport]);
end;


{ TElClientIndySSHIOHandlerSocket }

constructor TElClientIndySSHIOHandlerSocket.Create(AOwner: TComponent);
begin
  inherited;

  FConnection := nil;
  FTunnel := nil;
  FTransport := nil;
  FActive := false;
  FOwnTunnel := true;
  FErrorOccured := false;
  FInputBuffer := TIdManagedBuffer.Create;
  FDestroyConnection := false;
end;


procedure TElClientIndySSHIOHandlerSocket.Close;
begin
  if not Assigned(FTransport) then Exit;
  if FActive and Assigned(FConnection) then
  begin
    FActive := false;
    FConnection.Close;
    if FDestroyConnection then
      FreeAndNil(FConnection)
    else
      FConnection := nil;
  end;

  inherited;
end;

function TElClientIndySSHIOHandlerSocket.Recv(var ABuf; ALen: Integer): Integer;
begin
  if Assigned(FTransport) then
  begin
    FTransport.FSSHClient.DataAvailable;
    Result := Min(ALen, FInputBuffer.Size);

    if Result > 0 then
    begin
      FInputBuffer.Position := 0;
      Result := FInputBuffer.Read(ABuf, Result);
      FInputBuffer.Remove(Result);
    end;
  end
  else
    Result := 0;
end;

function TElClientIndySSHIOHandlerSocket.Send(var ABuf; ALen: Integer): Integer;
begin
  if FActive and Assigned(FConnection) then
    FConnection.SendData(@ABuf, ALen);
    
  Result := ALen;
end;

function TElClientIndySSHIOHandlerSocket.Connected: boolean;
begin
  Result := FActive;
end;

destructor TElClientIndySSHIOHandlerSocket.Destroy;
begin
  if FActive then
    Close;

  FTransport := nil;
  FInputBuffer.Free;

  inherited;
end;

procedure TElClientIndySSHIOHandlerSocket.Open;
begin
  inherited;
  if not Assigned(FTransport) then Exit;

  if not FTransport.Active then
    FTransport.Connect;

  if (FOwnTunnel) and (Assigned(FTunnel)) and (not FActive)then
  begin
    FErrorOccured := false;

  
    if FTransport.Version = sbSSH1 then
    begin
      if (FTransport.Active) and (FTunnelType <> ttLocalPortToRemoteAddress) then
        raise EElIdSSHException.Create(SSSHCannotOpenTunnel)
      else
        FTunnel.Open(Self);
    end
    else
      FTunnel.Open(Self);

    while (not FErrorOccured) and (not FActive) and (FTransport.Active) do
      FTransport.FSSHClient.DataAvailable;

    if (FErrorOccured) or (not FTransport.Active) then
      raise EElIdSSHException.Create(SSSHCannotOpenTunnel);
  end;
end;

function TElClientIndySSHIOHandlerSocket.Readable(AMSec: Integer): Boolean;
begin
  FTransport.FSSHClient.DataAvailable;

  if Assigned(FConnection) and (FInputBuffer.Size > 0) then
    Result := FActive
  else
    Result := false;
end;

procedure TElClientIndySSHIOHandlerSocket.DoOnData(Sender: TObject;
  Buffer: pointer; Size: Integer);
begin
  FInputBuffer.Seek(0, soFromEnd);
  FInputBuffer.Write(Buffer^, Size);
end;

procedure TElClientIndySSHIOHandlerSocket.DoOnClose(Sender: TObject;
  CloseType: TSSHCloseType);
begin
  FActive := false;
end;

procedure TElClientIndySSHIOHandlerSocket.DoOnError(Sender : TObject;
  ErrorCode : integer);
begin
  FErrorOccured := true;
end;

procedure TElClientIndySSHIOHandlerSocket.DoOnTunnelError(Sender : TObject;
  ErrorCode : integer; Data : pointer);
var
  IOHandler : TElClientIndySSHIOHandlerSocket;
begin
  if Assigned(Data) and
    (TObject(Data).ClassType = TElClientIndySSHIOHandlerSocket)
  then
  begin
    { data is set to point to IOHandler, which handles this tunnel connection }
    IOHandler := TElClientIndySSHIOHandlerSocket(Data);
    IOHandler.FErrorOccured := true;
  end
  else
  begin
    { data is not set - self is needed IOHandler }
    FErrorOccured := true;
  end;
end;

procedure TElClientIndySSHIOHandlerSocket.DoOnOpen(Sender : TObject;
  TunnelConnection : TElSSHTunnelConnection);
var
  IOHandler : TElClientIndySSHIOHandlerSocket;
begin
  if Assigned(TunnelConnection.Data) and
    (TObject(TunnelConnection.Data).ClassType = TElClientIndySSHIOHandlerSocket)
  then
  begin
    { data is set to point to IOHandler, which handles this tunnel connection }  
    IOHandler := TElClientIndySSHIOHandlerSocket(TunnelConnection.Data);
    TunnelConnection.OnClose := IOHandler.DoOnClose;
    TunnelConnection.OnData := IOHandler.DoOnData;
    TunnelConnection.OnError := IOHandler.DoOnError;
    IOHandler.FConnection := TunnelConnection;
    IOHandler.FActive := true;
  end
  else
  begin
    { data is not set - self is needed IOHandler }  
    TunnelConnection.OnClose := DoOnClose;
    TunnelConnection.OnData := DoOnData;
    TunnelConnection.OnError := DoOnError;
    FConnection := TunnelConnection;
    FActive := true;  
  end;
end;

function TElClientIndySSHIOHandlerSocket.GetCommand: string;
begin
  Command := '';
  if Assigned(FTunnel) then
    if FTunnelType = ttCommand then
      Result := TElCommandSSHTunnel(FTunnel).Command;
end;

function TElClientIndySSHIOHandlerSocket.GetEnvironment: TStringList;
begin
  Result := nil;
  if Assigned(FTunnel) then
    if FTunnelType = ttCommand then
      Result := TElCommandSSHTunnel(FTunnel).Environment
    else if FTunnelType = ttShell then
      Result := TElShellSSHTunnel(FTunnel).Environment
    else if FTunnelType = ttX11 then
      Result := TElX11ForwardSSHTunnel(FTunnel).Environment;
end;

function TElClientIndySSHIOHandlerSocket.GetToHost: string;
begin
  Result := '';
  if Assigned(FTunnel) then
    if FTunnelType = ttRemotePortToLocalAddress then
      Result := TElRemotePortForwardSSHTunnel(FTunnel).ToHost
    else if FTunnelType = ttLocalPortToRemoteAddress then
      Result := TElLocalPortForwardSSHTunnel(FTunnel).ToHost
end;

function TElClientIndySSHIOHandlerSocket.GetSubsystem: string;
begin
  Result := '';
  if Assigned(FTunnel) then
    if FTunnelType = ttSubsystem then
      Result := TElSubsystemSSHTunnel(FTunnel).Subsystem;
end;

function TElClientIndySSHIOHandlerSocket.GetTerminalInfo: TElTerminalInfo;
begin
  Result := nil;
  if Assigned(FTunnel) then
    if FTunnelType = ttCommand then
      Result := TElCommandSSHTunnel(FTunnel).TerminalInfo
    else if FTunnelType = ttShell then
      Result := TElShellSSHTunnel(FTunnel).TerminalInfo
    else if FTunnelType = ttX11 then
      Result := TElX11ForwardSSHTunnel(FTunnel).TerminalInfo;
end;

function TElClientIndySSHIOHandlerSocket.GetAuthenticationProtocol: string;
begin
  Result := '';
  if Assigned(FTunnel) then
    if FTunnelType = ttX11 then
      Result := TElX11ForwardSSHTunnel(FTunnel).AuthenticationProtocol;
end;

function TElClientIndySSHIOHandlerSocket.GetScreenNumber: integer;
begin
  Result := -1;
  if Assigned(FTunnel) then
    if FTunnelType = ttX11 then
      Result := TElX11ForwardSSHTunnel(FTunnel).ScreenNumber;
end;

function TElClientIndySSHIOHandlerSocket.GetToPort: integer;
begin
  Result := 0;
  if Assigned(FTunnel) then
    if FTunnelType = ttLocalPortToRemoteAddress then
      Result := TElRemotePortForwardSSHTunnel(FTunnel).ToPort
    else if FTunnelType = ttRemotePortToLocalAddress then
      Result := TElLocalPortForwardSSHTunnel(FTunnel).ToPort;
end;

procedure TElClientIndySSHIOHandlerSocket.SetCommand(const Value: string);
begin
  if Assigned(FTunnel) then
    if FTunnelType = ttCommand then
      TElCommandSSHTunnel(FTunnel).Command := Value;
end;

procedure TElClientIndySSHIOHandlerSocket.SetSubsystem(
  const Value: string);
begin
  if Assigned(FTunnel) then
    if FTunnelType = ttSubsystem then
      TElSubsystemSSHTunnel(FTunnel).Subsystem := Value;
end;

procedure TElClientIndySSHIOHandlerSocket.SetTerminalInfo(
  const Value: TElTerminalInfo);
begin
  if Assigned(FTunnel) then
    if FTunnelType = ttCommand then
      TElCommandSSHTunnel(FTunnel).TerminalInfo := Value
    else if FTunnelType = ttShell then
      TElShellSSHTunnel(FTunnel).TerminalInfo := Value
    else if FTunnelType = ttX11 then
      TElX11ForwardSSHTunnel(FTunnel).TerminalInfo := Value;
end;

procedure TElClientIndySSHIOHandlerSocket.SetTransport(
  const Value: TElClientIndySSHTransport);
begin
  if Assigned(FTransport) then
    raise EElIdSSHException.Create(SSSHTransportAlreadySet);

  FTransport := Value;
  FConnection := nil;

  if FOwnTunnel then
  begin
    case FTunnelType of
      ttLocalPortToRemoteAddress :
        FTunnel := TElLocalPortForwardSSHTunnel.Create(nil);
      ttRemotePortToLocalAddress :
        FTunnel := TElRemotePortForwardSSHTunnel.Create(nil);
      ttX11 :
        FTunnel := TElX11ForwardSSHTunnel.Create(nil);
      ttAuthenticationAgent :
        FTunnel := TElAuthenticationAgentSSHTunnel.Create(nil);
      ttSubsystem :
        FTunnel := TElSubsystemSSHTunnel.Create(nil);
      ttCommand :
        FTunnel := TElCommandSSHTunnel.Create(nil);
      ttShell :
        FTunnel := TElShellSSHTunnel.Create(nil);
    end;

    FTunnel.AutoOpen := false;
    FTunnel.TunnelList := FTransport.FTunnelList;
    FTunnel.OnOpen := DoOnOpen;
    FTunnel.OnError := DoOnTunnelError;
  end
  else
    FTunnel := nil;
end;

procedure TElClientIndySSHIOHandlerSocket.SetTunnelType(
  const Value: TSSHTunnelType);
begin
  if not Assigned(FTunnel) then
    FTunnelType := Value;
end;

procedure TElClientIndySSHIOHandlerSocket.SetToPort(const Value: integer);
begin
  if Assigned(FTunnel) then
    if FTunnelType = ttLocalPortToRemoteAddress then
      TElRemotePortForwardSSHTunnel(FTunnel).ToPort := Value
    else if FTunnelType = ttRemotePortToLocalAddress then
      TElLocalPortForwardSSHTunnel(FTunnel).ToPort := Value;
end;

procedure TElClientIndySSHIOHandlerSocket.SetAuthenticationProtocol(
  const Value: string);
begin
  if Assigned(FTunnel) then
    if FTunnelType = ttX11 then
      TElX11ForwardSSHTunnel(FTunnel).AuthenticationProtocol := Value;
end;

procedure TElClientIndySSHIOHandlerSocket.SetScreenNumber(
  const Value: integer);
begin
  if Assigned(FTunnel) then
    if FTunnelType = ttX11 then
      TElX11ForwardSSHTunnel(FTunnel).ScreenNumber := Value;
end;

procedure TElClientIndySSHIOHandlerSocket.SetToHost(const Value: string);
begin
  if Assigned(FTunnel) then
    if FTunnelType = ttLocalPortToRemoteAddress then
      TElRemotePortForwardSSHTunnel(FTunnel).ToHost := Value
    else if FTunnelType = ttRemotePortToLocalAddress then
      TElLocalPortForwardSSHTunnel(FTunnel).ToHost := Value;
end;

procedure TElClientIndySSHIOHandlerSocket.SetTunnel(
  const Value: TElCustomSSHTunnel);
begin
  if (not FOwnTunnel) and (not Assigned(FTunnel)) and (Assigned(FTransport))then
  begin
    FTunnel := Value;
    FConnection := TElSSHClientTunnelConnection.Create;
    FConnection.Tunnel := FTunnel;
    FConnection.Data := Self;
    FConnection.OnClose := DoOnClose;
    FConnection.OnData := DoOnData;
    FConnection.OnError := DoOnError;
    FTunnel.AddConnection(FConnection);
  end;
end;

procedure TElClientIndySSHIOHandlerSocket.SetOwnTunnel(
  const Value: boolean);
begin
  if not Assigned(FTunnel) then
    FOwnTunnel := Value;
end;

{ TElClientIndySSHTransport }

constructor TElClientIndySSHTransport.Create(AOwner : TComponent);
begin
  inherited;

  FSSHClient := TElSSHClient.Create(nil);
  FTunnelList := TElSSHTunnelList.Create(nil);
  FSSHClient.TunnelList := FTunnelList;
  FTCPClient := TIdTCPClient.Create(nil);
  FTCPClient.OnDisconnected := DoTCPDisconnected;

  FErrorOccured := false;
  FOnAuthenticationSuccess := nil;
  FOnAuthenticationFailed := nil;
  FOnAuthenticationKeyboard := nil;
  FOnBanner := nil;
  FOnCloseConnection := nil;
  FOnError := nil;
  FOnKeyValidate := nil;
  FActive := false;

  FSSHClient.OnKeyValidate := DoSSHKeyValidate;
  FSSHClient.OnAuthenticationSuccess := DoSSHAuthenticationSuccess;
  FSSHClient.OnAuthenticationFailed := DoSSHAuthenticationFailed;
  FSSHClient.OnAuthenticationKeyboard := DoSSHAuthenticationKeyboard;
  FSSHClient.OnBanner := DoSSHBanner;
  FSSHClient.OnSend := DoSSHSend;
  FSSHClient.OnReceive := DoSSHReceive;
  FSSHClient.OnCloseConnection := DoSSHCloseConnection;
  FSSHClient.OnError := DoSSHError;
end;

procedure TElClientIndySSHTransport.Connect;
begin
  try
    FTCPClient.Connect;

    if not FTCPClient.Connected then
      raise EElIdNotImplementedException.Create(SCannotConnect);

    FErrorOccured := false;
    FActive := true;  
    FSSHClient.Open;

    while FActive and (not FErrorOccured) and (not FSSHClient.Active) do
    begin
      FSSHClient.DataAvailable;
    end;

    if not FSSHClient.Active then
      raise EElIdSSHException.Create(SCannotConnect);
  except
    on E : Exception do
    begin
      if FTCPClient.Connected then
      begin
        FTCPClient.InputBuffer.Clear;
        FTCPClient.Disconnect;
      end;
      FActive := false;      
      FSSHClient.Close;
      FErrorOccured := false;
      raise;
    end;  
  end;
end;

destructor TElClientIndySSHTransport.Destroy;
begin
  if FActive then
    Disconnect;

  FreeAndNil(FSSHClient);
  FreeAndNil(FTunnelList);

  inherited;
end;

procedure TElClientIndySSHTransport.Disconnect;
begin
  if FActive then
  begin
    try
      FActive := false;
      if FSSHClient.Active then
        FSSHClient.Close(not FErrorOccured);
      while FSSHClient.Active do
        FSSHClient.DataAvailable;
      FErrorOccured := false;
      FLastError := 0;  

      FTCPClient.Disconnect;
    finally
    end;
  end;
end;

procedure TElClientIndySSHTransport.DoSSHAuthenticationFailed(
  Sender: TObject; AuthenticationType: integer);
begin
  if Assigned(FOnAuthenticationFailed) then
    FOnAuthenticationFailed(Self, AuthenticationType);
end;

procedure TElClientIndySSHTransport.DoSSHAuthenticationKeyboard(
  Sender: TObject; Prompts: TStringList; Echo: TBits;
  Responses: TStringList);
begin
  if Assigned(FOnAuthenticationKeyboard) then
    FOnAuthenticationKeyboard(Self, Prompts, Echo, Responses);
end;

procedure TElClientIndySSHTransport.DoSSHAuthenticationSuccess(
  Sender: TObject);
begin
  if Assigned(FOnAuthenticationSuccess) then
    FOnAuthenticationSuccess(Self);
end;

procedure TElClientIndySSHTransport.DoSSHBanner(Sender: TObject;
  const Text, Language: string);
begin
  if Assigned(FOnBanner) then
    FOnBanner(Self, Text, Language);
end;

procedure TElClientIndySSHTransport.DoSSHOpenConnection(
  Sender: TObject);
begin
  if Assigned(FOnOpenConnection) then
    FOnOpenConnection(Self);
end;

procedure TElClientIndySSHTransport.DoSSHCloseConnection(
  Sender: TObject);
begin
  if FActive then
    Disconnect;

  if Assigned(FOnCloseConnection) then
    FOnCloseConnection(Self);
end;

procedure TElClientIndySSHTransport.DoSSHError(Sender: TObject;
  ErrorCode: integer);
begin
  FErrorOccured := true;
  FLastError := ErrorCode;

  if Assigned(FOnError) then
    FOnError(Self, ErrorCode);
end;

procedure TElClientIndySSHTransport.DoSSHKeyValidate(Sender: TObject;
  ServerKey: TElSSHKey; var Validate: boolean);
begin
  if Assigned(FOnKeyValidate) then
    FOnKeyValidate(Self, ServerKey, Validate)
  else
    Validate := true;
end;

procedure TElClientIndySSHTransport.DoSSHReceive(Sender: TObject;
  Buffer: pointer; MaxSize: Integer; out Written: Integer);
begin
  if FActive then
  try
    FTCPClient.ReadFromStack(false, 0, false);

    if FTCPClient.Connected then
    begin
      Written := Min(FTCPClient.InputBuffer.Size, MaxSize);

      if Written > 0 then
      begin
        FTCPClient.InputBuffer.Position := 0;
        FTCPClient.InputBuffer.Read(Buffer^, Written);
        FTCPClient.InputBuffer.Remove(Written);
      end;
    end
    else
      Disconnect;
  except
    Written := 0;
    { receive error - disconnecting.. }
    DoSSHError(Self, ERROR_SSH_CONNECTION_LOST);
    Disconnect;
  end;
end;

procedure TElClientIndySSHTransport.DoSSHSend(Sender: TObject;
  Buffer: pointer; Size: Integer);
begin
  if FActive then
  try
    FTCPClient.CheckForGracefulDisconnect(false);

    if FTCPClient.Connected and TIdIOHandlerSocket(FTCPClient.IOHandler).Binding.HandleAllocated then
    begin
      FTCPClient.IOHandler.Send(Buffer^, Size);
      FTCPClient.IOHandler.DoWork(wmWrite, Size);
    end
    else if FTCPClient.InputBuffer.Size = 0 then
    begin
      DoSSHError(Self, ERROR_SSH_CONNECTION_LOST);
      Disconnect;
    end;
  except
    DoSSHError(Self, ERROR_SSH_CONNECTION_LOST);
    Disconnect;
  end;
end;

function TElClientIndySSHTransport.GetActive: boolean;
begin
  Result := FActive;
end;

function TElClientIndySSHTransport.GetAuthenticationTypes: integer;
begin
  Result := FSSHClient.AuthenticationTypes;
end;

function TElClientIndySSHTransport.GetClientHostname: string;
begin
  Result := FSSHClient.ClientHostName;
end;

function TElClientIndySSHTransport.GetClientUsername: string;
begin
  Result := FSSHClient.ClientUserName;
end;

function TElClientIndySSHTransport.GetCompressionAlgorithmCS: TSSHCompressionAlgorithm;
begin
  Result := FSSHClient.CompressionAlgorithmClientToServer;
end;

function TElClientIndySSHTransport.GetCompressionAlgorithms(
  Index: TSSHCompressionAlgorithm): boolean;
begin
  Result := FSSHClient.CompressionAlgorithms[Index];
end;

function TElClientIndySSHTransport.GetCompressionAlgorithmSC: TSSHCompressionAlgorithm;
begin
  Result := FSSHClient.CompressionAlgorithmServerToClient;
end;

function TElClientIndySSHTransport.GetCompressionLevel: integer;
begin
  Result := FSSHClient.CompressionLevel;
end;

function TElClientIndySSHTransport.GetEncryptionAlgorithmCS: TSSHEncryptionAlgorithm;
begin
  Result := FSSHClient.EncryptionAlgorithmClientToServer;
end;

function TElClientIndySSHTransport.GetEncryptionAlgorithms(
  Index: TSSHEncryptionAlgorithm): boolean;
begin
  Result := FSSHClient.EncryptionAlgorithms[Index];
end;

function TElClientIndySSHTransport.GetEncryptionAlgorithmSC: TSSHEncryptionAlgorithm;
begin
  Result := FSSHClient.EncryptionAlgorithmServerToClient;
end;

function TElClientIndySSHTransport.GetForceCompression: boolean;
begin
  Result := FSSHClient.ForceCompression;
end;

function TElClientIndySSHTransport.GetKexAlgorithm: TSSHKexAlgorithm;
begin
  Result := FSSHClient.KexAlgorithm;
end;

function TElClientIndySSHTransport.GetKexAlgorithms(
  Index: TSSHKexAlgorithm): boolean;
begin
  Result := FSSHClient.KexAlgorithms[Index];
end;

function TElClientIndySSHTransport.GetKeyStorage: TElSSHCustomKeyStorage;
begin
  Result := FSSHClient.KeyStorage;
end;

function TElClientIndySSHTransport.GetMacAlgorithmCS: TSSHMacAlgorithm;
begin
  Result := FSSHClient.MacAlgorithmClientToServer;
end;

function TElClientIndySSHTransport.GetMACAlgorithms(
  Index: TSSHMacAlgorithm): boolean;
begin
  Result := FSSHClient.MACAlgorithms[Index];
end;

function TElClientIndySSHTransport.GetMacAlgorithmSC: TSSHMacAlgorithm;
begin
  Result := FSSHClient.MacAlgorithmServerToClient;
end;

function TElClientIndySSHTransport.GetPassword: string;
begin
  Result := FSSHClient.Password;
end;

function TElClientIndySSHTransport.GetPublicKeyAlgorithm: TSSHPublicKeyAlgorithm;
begin
  Result := FSSHClient.PublicKeyAlgorithm;
end;

function TElClientIndySSHTransport.GetPublicKeyAlgorithms(
  Index: TSSHPublicKeyAlgorithm): boolean;
begin
  Result := FSSHClient.PublicKeyAlgorithms[Index];
end;

function TElClientIndySSHTransport.GetServerCloseReason: string;
begin
  Result := FSSHClient.ServerCloseReason;
end;

function TElClientIndySSHTransport.GetServerSoftwareName: string;
begin
  Result := FSSHClient.ServerSoftwareName;
end;

function TElClientIndySSHTransport.GetSoftwareName: string;
begin
  Result := FSSHClient.SoftwareName;
end;

function TElClientIndySSHTransport.GetUsername: string;
begin
  Result := FSSHClient.Username;
end;

function TElClientIndySSHTransport.GetVersion: TSSHVersion;
begin
  Result := FSSHClient.Version;
end;

function TElClientIndySSHTransport.GetVersions: TSSHVersions;
begin
  Result := FSSHClient.Versions;
end;

procedure TElClientIndySSHTransport.SetAuthenticationTypes(
  const Value: integer);
begin
  FSSHClient.AuthenticationTypes := Value;
end;

procedure TElClientIndySSHTransport.SetClientHostname(
  const Value: string);
begin
  FSSHClient.ClientHostname := Value;
end;

procedure TElClientIndySSHTransport.SetClientUsername(
  const Value: string);
begin
  FSSHClient.ClientUsername := Value;
end;

procedure TElClientIndySSHTransport.SetCompressionAlgorithms(
  Index: TSSHCompressionAlgorithm; const Value: boolean);
begin
  FSSHClient.CompressionAlgorithms[Index] := Value;
end;

procedure TElClientIndySSHTransport.SetCompressionLevel(
  const Value: integer);
begin
  FSSHClient.CompressionLevel := Value;
end;

procedure TElClientIndySSHTransport.SetEncryptionAlgorithms(
  Index: TSSHEncryptionAlgorithm; const Value: boolean);
begin
  FSSHClient.EncryptionAlgorithms[Index] := Value;
end;

procedure TElClientIndySSHTransport.SetForceCompression(
  const Value: boolean);
begin
  FSSHClient.ForceCompression := Value;
end;

procedure TElClientIndySSHTransport.SetKexAlgorithms(
  Index: TSSHKexAlgorithm; const Value: boolean);
begin
  FSSHClient.KexAlgorithms[Index] := Value;
end;

procedure TElClientIndySSHTransport.SetKeyStorage(
  const Value: TElSSHCustomKeyStorage);
begin
  FSSHClient.KeyStorage := Value;
end;

procedure TElClientIndySSHTransport.SetMACAlgorithms(
  Index: TSSHMacAlgorithm; const Value: boolean);
begin
  FSSHClient.MACAlgorithms[Index] := Value;
end;

procedure TElClientIndySSHTransport.SetPassword(const Value: string);
begin
  FSSHClient.Password := Value;
end;

procedure TElClientIndySSHTransport.SetPublicKeyAlgorithms(
  Index: TSSHPublicKeyAlgorithm; const Value: boolean);
begin
  FSSHClient.PublicKeyAlgorithms[Index] := Value;
end;

procedure TElClientIndySSHTransport.SetSoftwareName(
  const Value: string);
begin
  FSSHClient.SoftwareName := Value;
end;

procedure TElClientIndySSHTransport.SetUsername(const Value: string);
begin
  FSSHClient.Username := Value;
end;

procedure TElClientIndySSHTransport.SetVersions(
  const Value: TSSHVersions);
begin
  FSSHClient.Versions := Value;
end;

function TElClientIndySSHTransport.GetHost: string;
begin
  Result := FTCPClient.Host;
end;

function TElClientIndySSHTransport.GetIOHandler: TIdIOHandler;
begin
  Result := FTCPClient.IOHandler;
end;

function TElClientIndySSHTransport.GetPort: integer;
begin
  Result := FTCPClient.Port;
end;

procedure TElClientIndySSHTransport.SetHost(const Value: string);
begin
  FTCPClient.Host := Value;
end;

procedure TElClientIndySSHTransport.SetIOHandler(
  const Value: TIdIOHandler);
begin
  FTCPClient.IOHandler := Value;
end;

procedure TElClientIndySSHTransport.SetPort(const Value: integer);
begin
  FTCPClient.Port := Value;
end;

procedure TElClientIndySSHTransport.DoTCPDisconnected(Sender: TObject);
begin
  DoSSHError(Self, ERROR_SSH_CONNECTION_LOST);
  Disconnect;
end;

function TElClientIndySSHTransport.GetCloseIfNoActiveTunnels: boolean;
begin
  Result := FSSHClient.CloseIfNoActiveTunnels;
end;

procedure TElClientIndySSHTransport.SetCloseIfNoActiveTunnels(
  const Value: boolean);
begin
  FSSHClient.CloseIfNoActiveTunnels := Value;
end;

end.
