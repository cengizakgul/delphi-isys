unit LMDResWinControl;
{$I lmdcmps.inc}

{###############################################################################

LMDResWinControl unit (RM)
--------------------------

Changes
-------
Release 8.0 (October 2006)
 - Initial Release

###############################################################################}

interface

uses
  Controls;

type
  {make some protected routines public}
  {----------------------------------------------------------------------------}
  TLMDResWinControl=class(TWinControl)
  protected
    procedure CreateHandle;override;
  public
    procedure FreeResources;
  end;

  procedure LMDFreeResources(aControl:TWinControl);

implementation
{------------------------------------------------------------------------------}
procedure LMDFreeResources(aControl:TWinControl);
begin
  if Assigned(aControl) then

    TLMDResWinControl(aControl).FreeResources;

end;

{-------------------------- Public --------------------------------------------}
procedure TLMDResWinControl.CreateHandle;
begin
  inherited CreateHandle;
end;

{------------------------------------------------------------------------------}
procedure TLMDResWinControl.FreeResources;
begin
  DestroyHandle;
end;

end.
