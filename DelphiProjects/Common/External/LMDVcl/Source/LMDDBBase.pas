unit LMDDBBase;
{$I lmdcmps.inc}

{###############################################################################

LMDDBBase unit (VO)
-------------------

Basic DB interface settings

Changes
-------
Release 8.0 (October 2006)
 - Initial Release

###############################################################################}

interface

uses

  DB;

type  

  {!IDataControl is an interface for accessing the datalink of a data-aware
   control. It replaces the CM_GETDATALINK message. }
  IDataControl = interface
    function GetDataLink: TDataLink;
  end;

implementation

end.
