unit intfLMDFaceController;
{$I lmdcmps.inc}

{###############################################################################

intfLMDFaceController unit (JH)
-------------------------------

Defines several methods which enables a control to read control settings

Changes
-------
Release 8.0 (October 2006)
 - Initial Release

###############################################################################}

interface

uses
  Windows,
  Graphics,
  Classes,
  LMDFillObject,
  LMDBevel,
  LMDBitmapEffectObject,
  LMD3DCaption,
  intfLMDBase;

type
  ILMDFaceController = interface(IInterface)
    ['{32BE634B-1A6E-4975-9975-241836293C61}']
    function fcGetFocused: Boolean; 
    function fcGetMouseAbove: Boolean;
    function fcGetTransparent : Boolean;
    function fcGetCtlXP : Boolean;
    function fcGetBorderCtl3D : Boolean;
    function fcGetEnabled     : Boolean;
    function fcGetBackColor   : TColor;

    procedure fcSetFaceController(aComponent:TComponent);
  end;

implementation

end.
