unit LMDControl;
{$I lmdcmps.inc}
{###############################################################################

LMDControl unit (RM)
--------------------
Implements base classes with generic LMD support functionality (ILMDComponent
interface)

Changes
-------
Release 8.0 (October 2006)
 - Initial Release

###############################################################################}
interface
uses
  Classes, Controls, Menus, Messages, ComCtrls, Forms, intfLMDBase, LMDClass,
  LMDTypes;

type
  { ********************* TLMDBasePopupMenu ***********************************}
  TLMDBasePopupMenu = class (TPopupMenu, ILMDComponent)
  private
    FAbout    : TLMDAboutVar;
  protected
    property About:TLMDAboutVar read FAbout write FAbout;
  public
    function getLMDPackage:TLMDPackageID;virtual;
  end;

  { ********************* TLMDBaseCustomControl *******************************}
  TLMDBaseCustomControl = class(TCustomControl, ILMDComponent)
  private
    {$IFDEF LMD_UNICODE}
    FHint: TLMDString;
    {$ENDIF}    
    FAbout: TLMDAboutVar;
    FNeedInvalidate: Boolean;
    FUpdateCount: Word;
    FOnMouseEnter,
    FOnMouseExit   : TNotifyEvent;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
  protected
    procedure GetChange(Sender: TObject); virtual;
    procedure MouseEnter; virtual;
    procedure MouseExit; virtual;
    property NeedInvalidate:Boolean read FNeedInvalidate write FNeedInvalidate default True;
    property OnMouseEnter: TNotifyEvent read FOnMouseEnter write FOnMouseEnter;
    property OnMouseExit: TNotifyEvent read FOnMouseExit write FOnMouseExit;
    {$ifdef LMD_UNICODE}
    procedure SetHint(Value: TLMDString);
    procedure CMHintShow(var Message: TCMHintShow); message CM_HINTSHOW;
    {$endif}
  public
    constructor Create (AOwner: TComponent); override;
    procedure BeginUpdate;
    function IsUpdating:Boolean;
    procedure EndUpdate(repaint: Boolean=false);virtual;
    function getLMDPackage:TLMDPackageID;virtual;
  published
    property About: TLMDAboutVar read FAbout write FAbout stored false;
    {$ifdef LMD_UNICODE}
    property Hint: TLMDString read FHint write SetHint;
    {$endif}
  end;

  { ********************* TLMDBaseTreeView ************************************}
  TLMDBaseTreeView = class(TCustomTreeView, ILMDComponent)
  private
    FAbout: TLMDAboutVar;
  public
    function getLMDPackage:TLMDPackageID; virtual;
  published
    property About: TLMDAboutVar read FAbout write FAbout stored false;
  end;

  { ********************* TLMDBaseTreeView ************************************}
  TLMDBaseListView = class(TCustomListView, ILMDComponent)
  private
    FAbout: TLMDAboutVar;
    FThemeGlobalMode: Boolean;
    procedure SetThemeGlobalMode(const Value: Boolean);
    procedure SetThemeMode(const Value: TLMDThemeMode);
  protected
    FThemeMode : TLMDThemeMode;
    //---
    // Theme-Support
    //**************
    //handle theme changes
    procedure DoThemeChanged; virtual;
    // UseThemeMode returns active theme, depending on global settings etc.
    function UseThemeMode: TLMDThemeMode;
  public
    function IsThemed: Boolean; // any other than ttmNone
    function getLMDPackage:TLMDPackageID;virtual;
  published
    property About: TLMDAboutVar read FAbout write FAbout stored false;
    property ThemeMode:TLMDThemeMode read FThemeMode write SetThemeMode default ttmPlatform;
    property ThemeGlobalMode: Boolean read FThemeGlobalMode write SetThemeGlobalMode default false;
  end;

implementation

uses
  LMDThemes, LMDUtils;

{************************ TLMDBasePopupMenu ***********************************}
{------------------------------------------------------------------------------}
function TLMDBasePopupMenu.getLMDPackage: TLMDPackageID;
begin
  result:=pi_LMD_TOOLS;
end;

{************************ TLMDBaseCustomControl *******************************}
{ ----------------------------- private -------------------------------------- }
procedure TLMDBaseCustomControl.CMMouseEnter(var Message: TMessage);
begin
  inherited;
  MouseEnter;
end;

{------------------------------------------------------------------------------}
procedure TLMDBaseCustomControl.CMMouseLeave(var Message: TMessage);
begin
  inherited;
  MouseExit;
end;

{ --------------------------- protected -------------------------------------- }
procedure TLMDBaseCustomControl.GetChange(Sender:TObject);
begin
  if FNeedInvalidate then Invalidate;
end;

{------------------------------------------------------------------------------}
procedure TLMDBaseCustomControl.MouseEnter;
begin
  if Assigned(FOnMouseEnter) then FOnMouseEnter(self);
end;

{------------------------------------------------------------------------------}
procedure TLMDBaseCustomControl.MouseExit;
begin
  if Assigned(FOnMouseExit) then FOnMouseExit(self);
end;

{------------------------------------------------------------------------------}
procedure TLMDBaseCustomControl.BeginUpdate;
begin
  Inc(FUpdateCount);
end;

{$ifdef LMD_UNICODE}
procedure TLMDBaseCustomControl.SetHint(Value: TLMDString);
begin
  FHint := Value;
  //inherited Hint := LMDConvertVCLHint(FHint);
  inherited Hint := FHint;
end;

procedure TLMDBaseCustomControl.CMHintShow(var Message:TCMHintShow);
begin
  inherited;
end;
{$endif}

{---------------------------- public ------------------------------------------}
constructor TLMDBaseCustomControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FNeedInvalidate := True;
  FUpdateCount:=0;
end;

{------------------------------------------------------------------------------}
procedure TLMDBaseCustomControl.EndUpdate(repaint: Boolean);
begin
  if FUpdateCount>0 then Dec (FUpdateCount);
  if FUpdateCount = 0 then
    if repaint then GetChange(nil);
end;

{------------------------------------------------------------------------------}
function TLMDBaseCustomControl.IsUpdating:Boolean;
begin
  result:=FUpdateCount>0;
end;

{------------------------------------------------------------------------------}
function TLMDBaseCustomControl.getLMDPackage: TLMDPackageID;
begin
  result:=pi_LMD_TOOLS;
end;

{************************ TLMDBaseTreeView ************************************}
{---------------------------- public ------------------------------------------}
function TLMDBaseTreeView.getLMDPackage: TLMDPackageID;
begin
  result:=pi_LMD_TOOLS;
end;

{************************ TLMDBaseListView ************************************}
{---------------------------- public ------------------------------------------}
procedure TLMDBaseListView.DoThemeChanged;
begin
  // Delphi 2k7 fix
  {$IFDEF LMDCOMP11}if (csDesigning in ComponentState) then exit;{$ENDIF}
  // RM - Themes
  Invalidate;
  //NotifyControls(WM_THEMECHANGED);
  NotifyControls(CM_LMDTHEMECHANGED);
end;

{------------------------------------------------------------------------------}
function TLMDBaseListView.getLMDPackage: TLMDPackageID;
begin
  result:=pi_LMD_TOOLS;
end;

function TLMDBaseListView.IsThemed: Boolean;
begin
  result := UseThemeMode <> ttmNone;
end;

{------------------------------------------------------------------------------}
procedure TLMDBaseListView.SetThemeGlobalMode(const Value: Boolean);
begin
  if FThemeGlobalMode <> Value then
  begin
    FThemeGlobalMode := Value;
    DoThemeChanged;
  end;
end;

{------------------------------------------------------------------------------}
procedure TLMDBaseListView.SetThemeMode(const Value: TLMDThemeMode);
begin
  if (FThemeMode <> Value) then
  begin
    FThemeMode := Value;
    if not FThemeGlobalMode then       // if we are in GlobalThemeMode, no update required
      DoThemeChanged;
  end;
end;

{------------------------------------------------------------------------------}
function TLMDBaseListView.UseThemeMode: TLMDThemeMode;
begin
  if FThemeGlobalMode then
    result := LMDApplication.ThemeGlobalMode
  else
    result := FThemeMode;
  result := LMDThemeServices.UseTheme(result);
end;

end.
