unit LMDCustomComponent;
{$I lmdcmps.inc}
{###############################################################################

LMDCustomComponent unit (RM)
-----------------------------
Base control for all nonvisual LMD Controls

Changes
-------
Release 8.0 (September 2006)
 - Initial Release

###############################################################################}

interface

uses
  Windows, Classes, intfLMDBase, LMDTypes;

type
  TLMDCustomComponent = class(TComponent, ILMDComponent)
  private
    FAbout    : TLMDAboutVar;
    FOnChange : TNotifyEvent;
  protected
    FUpdateCount: Word;
    procedure GetChange(Sender:TObject);dynamic;
    procedure Change; dynamic;
    property About:TLMDAboutVar read FAbout write FAbout stored false;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  public
    constructor Create (AOwner: TComponent); override;
    procedure BeginUpdate;
    function IsUpdating:Boolean;
    procedure EndUpdate(aValue:Boolean=True);
    function getLMDPackage:TLMDPackageID;virtual;
  end;

implementation

{$IFDEF LMD_DEBUGTRACE}
uses
  Dialogs;{$I C2.INC}{$ENDIF}
  {****************** Object TLMDCustomComponent ********************************}
{------------------------- Protected ------------------------------------------}
procedure TLMDCustomComponent.Change;
begin
  if IsUpdating then exit;
  if Assigned(FOnChange) then FOnChange(Self);
end;

{------------------------------------------------------------------------------}
procedure TLMDCustomComponent.GetChange(Sender:TObject);
begin
  Change;
end;

{------------------------------------------------------------------------------}
constructor TLMDCustomComponent.Create (AOwner: TComponent);
begin
  inherited Create(AOwner);
  FUpdateCount:=0;
end;

{------------------------------------------------------------------------------}
procedure TLMDCustomComponent.BeginUpdate;
begin
  Inc(FUpdateCount);
end;

{------------------------------------------------------------------------------}
function TLMDCustomComponent.IsUpdating: Boolean;
begin
  result:=FUpdateCount>0;
end;

{------------------------------------------------------------------------------}
procedure TLMDCustomComponent.EndUpdate(aValue:Boolean=True);
begin
  if FUpdateCount>0 then Dec (FUpdateCount);
  if FUpdateCount = 0 then
    if aValue then Change;
end;

{------------------------------------------------------------------------------}
function TLMDCustomComponent.getLMDPackage: TLMDPackageID;
begin
  result:=pi_LMD_TOOLS;
end;

{$IFDEF LMD_DEBUGTRACE}
initialization
  {$I C1.INC} 
{$ENDIF}
end.
