unit intfLMDBase;
{$I lmdcmps.inc}
{###############################################################################

intfLMDBase unit (RM)
---------------------
Introduces the ILMDComponent interface which should be implemented by all
LMD controls to provide designtime features (about dialog) and access to
version numbers of a particular package.

Changes
-------
Release 8.0 (October 2006)
 - Initial Release

###############################################################################}
interface
uses
  SysUtils, LMDTypes;

type
  ILMDComponent = interface(IInterface)
    ['{42DEE038-9F98-4CA3-9734-F770483E97FF}']
    function getLMDPackage:TLMDPackageID;  // specification in LMDTypes
  end;

  function LMDSupports(const Instance: TObject; const IID: TGUID; out Intf): Boolean;

implementation

// same as implementation in Delphi 6up, but required for Delphi 5 compatibility

function LMDSupports(const Instance: TObject; const IID: TGUID; out Intf): Boolean;

var
  LUnknown: IUnknown;

begin
  Result := (Instance <> nil) and
            ((Instance.GetInterface(IUnknown, LUnknown) and Supports(LUnknown, IID, Intf)) or
             Instance.GetInterface(IID, Intf));
  end;

end.
