unit LMDButton;
{$I lmdcmps.Inc}

{###############################################################################

LMDButton unit ()
-----------------

Changes
-------
Release 8.0 (October 2006)
 - Initial Release

###############################################################################}

interface

uses
  LMDCustomButton;

type
  TLMDButton=class(TLMDCustomButton)
  public
    property CtlXP;  // compatibility
  published
    property DropDownMenu;
    property DropDownIndent;
    property DropDownRight;
    property ThemeMode;  // new in 9.0 
    property ThemeGlobalMode;
    property OnDropDown;
  end;

implementation

end.
 