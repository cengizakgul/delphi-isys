unit LMDLabel;
{$I lmdcmps.inc}

{###############################################################################

LMDLabel unit ()
----------------

Changes
-------
Release 8.0 (October 2006)
 - Initial Release

###############################################################################}

interface

uses
  LMDCustomLabel;

type
  TLMDLabel=class(TLMDCustomLabel)
  published
    property About;
    property Align;
    property Alignment;
    property AutoSize;
    property Bevel;
    property Caption;
    property Color;
    property Font;
    property FontFX;
    property MultiLine;
    property Options;
    property ShowAccelChar;
    property Transparent;
    property Twinkle;
    property TwinkleVisible;
    property TwinkleInVisible;
    property TwinkleColor;
  end;

implementation

end.
 