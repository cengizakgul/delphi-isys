unit LMDCheckBox;
{$I lmdcmps.inc}

{###############################################################################

LMDCheckBox unit ()
-------------------

Changes
-------
Release 8.0 (October 2006)
 - Initial Release

###############################################################################}

interface

uses
  LMDCustomCheckBox;

type
  {------------------------- TLMDCheckBox -------------------------------------}
  TLMDCheckBox = class(TLMDCustomCheckBox)
  published
    property BackFX;
    property Checked;
    property State;
  end;

implementation

end.
 