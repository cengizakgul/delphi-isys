unit LMDZLib;
{$I lmdcmps.inc}
{###############################################################################

LMDZLib unit - ZLib interface (AH)
----------------------------------
TStream successors for compress/decompress content of streams

Changes
-------
Release 8.0 (July 2006)
 - Initial Release

###############################################################################}

interface

uses
  Classes, LMDTypes, LMDZCommonUnit, LMDZCompressUnit, LMDZUncompressUnit,
  LMDRTLConst;

type
  TLMDCustomZlibStream = class(TStream)
  private
    FStrm: TStream;
    FStrmPos: Integer;
    FOnProgress: TNotifyEvent;
    FZRec: TZStreamRec;
    FBuffer: array [Word] of AnsiChar;
    protected
    procedure Progress(Sender: TObject); dynamic;
    property OnProgress: TNotifyEvent read FOnProgress write FOnProgress;
    constructor Create(Strm: TStream);
  end;

  TLMDCompressionLevel = (clNone, clFastest, clDefault, clMax);

  TLMDCompressionStream = class(TLMDCustomZlibStream)
  private
    function GetCompressionRate: Single;
  public
    constructor Create(CompressionLevel: TLMDCompressionLevel; Dest: TStream);
    destructor Destroy; override;
    function Read(var Buffer; Count: Longint): Longint; override;
    function Write(const Buffer; Count: Longint): Longint; override;
    function Seek(Offset: Longint; Origin: Word): Longint; override;
    property CompressionRate: Single read GetCompressionRate;
    property OnProgress;
  end;

  TLMDDecompressionStream = class(TLMDCustomZlibStream)
  public
    constructor Create(Source: TStream);
    destructor Destroy; override;
    function Read(var Buffer; Count: Longint): Longint; override;
    function Write(const Buffer; Count: Longint): Longint; override;
    function Seek(Offset: Longint; Origin: Word): Longint; override;
    property OnProgress;
  end;

procedure FillZStreamRec(var AStrm: TZStreamRec);

implementation

uses
  SysUtils;

{------------------------------------------------------------------------------}
procedure FillZStreamRec(var AStrm: TZStreamRec);
begin
  FillChar(AStrm, SizeOf(AStrm), 0);
  AStrm.zalloc := zlibAllocMem;
  AStrm.zfree := zlibFreeMem;
end;

{******************* Object TLMDCustomZlibStream ******************************}
{----------------------- public -----------------------------------------------}
constructor TLMDCustomZlibStream.Create(Strm: TStream);
begin
  inherited Create;
  FStrm := Strm;
  FStrmPos := Strm.Position;
  FZRec.zalloc := zlibAllocMem;
  FZRec.zfree := zlibFreeMem;
end;

{------------------------------------------------------------------------------}
procedure TLMDCustomZlibStream.Progress(Sender: TObject);
begin
  if Assigned(FOnProgress) then FOnProgress(Sender);
end;

{******************* Object TLMDCompressionStream *****************************}
{----------------------- public -----------------------------------------------}
constructor TLMDCompressionStream.Create(CompressionLevel: TLMDCompressionLevel;
  Dest: TStream);
const
  Levels: array [TLMDCompressionLevel] of ShortInt =
    (Z_NO_COMPRESSION, Z_BEST_SPEED, Z_DEFAULT_COMPRESSION, Z_BEST_COMPRESSION);
begin
  inherited Create(Dest);
  FZRec.next_out := FBuffer;
  FZRec.avail_out := Length(FBuffer);
  CCheck(deflateInit_(FZRec, Levels[CompressionLevel], zlib_version, sizeof(FZRec)));
end;

{------------------------------------------------------------------------------}
destructor TLMDCompressionStream.Destroy;
begin
  FZRec.next_in := nil;
  FZRec.avail_in := 0;
  try
    if FStrm.Position <> FStrmPos then
      FStrm.Position := FStrmPos;
    while (CCheck(deflate(FZRec, Z_FINISH)) <> Z_STREAM_END) and (FZRec.avail_out = 0) do
      begin
        FStrm.WriteBuffer(FBuffer, Length(FBuffer));
        FZRec.next_out := FBuffer;
        FZRec.avail_out := Length(FBuffer);
      end;
    if FZRec.avail_out < Length(FBuffer) then
      FStrm.WriteBuffer(FBuffer, Length(FBuffer) - FZRec.avail_out);
  finally
    deflateEnd(FZRec);
  end;
  inherited Destroy;
end;

function TLMDCompressionStream.Read(var Buffer; Count: Longint): Longint;

begin
  raise Exception.Create(SZLibInvalidStreamOp);
end;

function TLMDCompressionStream.Write(const Buffer; Count: Longint): Longint;

begin
  FZRec.next_in := @Buffer;
  FZRec.avail_in := Count;
  if FStrm.Position <> FStrmPos then
    FStrm.Position := FStrmPos;
  while (FZRec.avail_in > 0) do
    begin
      CCheck(deflate(FZRec, 0));
      if FZRec.avail_out = 0 then
        begin
          FStrm.WriteBuffer(FBuffer, Length(FBuffer));
          FZRec.next_out := FBuffer;
          FZRec.avail_out := Length(FBuffer);
          FStrmPos := FStrm.Position;
          Progress(Self);
        end;
    end;
  Result := Count;
end;

{------------------------------------------------------------------------------}

function TLMDCompressionStream.Seek(Offset: Longint; Origin: Word): Longint;

begin
  if (Offset = 0) and (Origin = soFromCurrent) then
    Result := FZRec.total_in
  else
    raise Exception.Create(SZLibInvalidStreamOp);
end;

{------------------------------------------------------------------------------}
function TLMDCompressionStream.GetCompressionRate: Single;
begin
  if FZRec.total_in = 0 then
    Result := 0
  else
    Result := (1.0 - (FZRec.total_out / FZRec.total_in)) * 100.0;
end;

{******************* Object TLMDDecompressionStream ***************************}
{----------------------- public -----------------------------------------------}
constructor TLMDDecompressionStream.Create(Source: TStream);
begin
  inherited Create(Source);
  FZRec.next_in := FBuffer;
  FZRec.avail_in := 0;
  CCheck(inflateInit_(FZRec, zlib_version, sizeof(FZRec)));
end;

{------------------------------------------------------------------------------}
destructor TLMDDecompressionStream.Destroy;
begin
  FStrm.Seek(-FZRec.avail_in, soFromCurrent);
  inflateEnd(FZRec);
  inherited Destroy;
end;

{------------------------------------------------------------------------------}

function TLMDDecompressionStream.Read(var Buffer; Count: Longint): Longint;

begin
  FZRec.next_out := @Buffer;
  FZRec.avail_out := Count;
  if FStrm.Position <> FStrmPos then FStrm.Position := FStrmPos;
  while (FZRec.avail_out > 0) do
    begin
      if FZRec.avail_in = 0 then
        begin
          FZRec.avail_in := FStrm.Read(FBuffer, Length(FBuffer));
          if FZRec.avail_in = 0 then
            begin
              Result := Count - FZRec.avail_out;
              Exit;
            end;
          FZRec.next_in := FBuffer;
          FStrmPos := FStrm.Position;
          Progress(Self);
        end;
      CCheck(inflate(FZRec, 0));
    end;
  Result := Count;
end;

{------------------------------------------------------------------------------}

function TLMDDecompressionStream.Write(const Buffer; Count: Longint): Longint;

begin
  raise Exception.Create(SZLibInvalidStreamOp);
end;

{------------------------------------------------------------------------------}

function TLMDDecompressionStream.Seek(Offset: Longint; Origin: Word): Longint;

var
  I: Integer;
  LOffset: Integer;
  Buf: array [0..4095] of Char;
  begin
  if (Offset = 0) and (Origin = soFromBeginning) then
    begin
      CCheck(inflateReset(FZRec));
      FZRec.next_in := FBuffer;
      FZRec.avail_in := 0;
      FStrm.Position := 0;
      FStrmPos := 0;
    end
  else if ( (Offset >= 0) and (Origin = soFromCurrent)) or
          ( ((Offset - FZRec.total_out) > 0) and (Origin = soFromBeginning)) then
    begin
      LOffset := Offset;
      if Origin = soFromBeginning then
        Dec(LOffset, FZRec.total_out);
      if LOffset > 0 then
      begin
        for I := 1 to LOffset div Length(Buf) do
          ReadBuffer(Buf, Length(Buf));
        ReadBuffer(Buf, LOffset mod Length(Buf));
      end;
    end
  else
    raise Exception.Create(SZLibInvalidStreamOp);
  Result := FZRec.total_out;
end;

end.
