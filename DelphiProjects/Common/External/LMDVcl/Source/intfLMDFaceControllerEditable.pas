unit intfLMDFaceControllerEditable;
{$I lmdcmps.inc}

{###############################################################################

intfLMDFaceControllerEditable unit (JH)
---------------------------------------

Defines several methods which enables a face controller to read edit control states (modified, readonly, wrong data)

Changes
-------
Release 8.0 (October 2006)
 - Initial Release

###############################################################################}

interface

uses
  Windows,
  Graphics,
  intfLMDBase;

type
  ILMDFaceControllerEditable = interface(IInterface)
    ['{4CF92C4B-8694-477D-8E1E-2F6A2EAAE960}']
    function fcGetModified: Boolean;
    function fcGetReadOnly: Boolean;
    function fcGetInvalidData: Boolean;
  end;

implementation

end.
