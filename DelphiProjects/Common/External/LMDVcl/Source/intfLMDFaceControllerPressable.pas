unit intfLMDFaceControllerPressable;
{$I lmdcmps.inc}

{###############################################################################

intfLMDFaceControllerPressable unit (JH)
----------------------------------------

Defines several methods which enables a face controller to read a control setting (pressed) btn state down

Changes
-------
Release 8.0 (October 2006)
 - Initial Release

###############################################################################}

interface

uses
  Windows,
  Graphics,
  intfLMDBase;

type
  ILMDFaceControllerPressable = interface(IInterface)
    ['{DAF1F323-1814-4CF5-97C9-5B82B945D7E6}']
    function fcGetPressed: Boolean;
  end;

implementation

end.
