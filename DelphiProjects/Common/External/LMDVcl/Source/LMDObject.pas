unit LMDObject;
{$I lmdcmps.inc}
{###############################################################################

LMDObject unit (RM)
------------------
Base class for all LMD TPersistent objects.

ToDo
----

Changes
-------
Release 8.0 (October 2006)
 - Initial Release

###############################################################################}

interface

uses
  Classes;

type
  {----------------------TLMDObject--------------------------------------------}
  TLMDObject = class(TPersistent)
  private
    FOwner    : TPersistent;
    FOnChange : TNotifyEvent;
    FUpdate   : Byte;
  protected
    procedure GetChange(Sender: TObject); dynamic;
    procedure Change; dynamic;
    constructor Create(Owner: TPersistent=nil); virtual;
  public
    function GetSetStr : String; dynamic;
    procedure BeginUpdate;
    procedure EndUpdate(aValue:Boolean=false);
    function IsUpdating:Boolean;
    property Owner:TPersistent read FOwner;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

implementation
{$IFDEF LMD_DEBUGTRACE}
  uses windows, dialogs;
  {$I C2.INC}
{$ENDIF}
{------------------------- Protected ------------------------------------------}
procedure TLMDObject.Change;
begin
  if FUpdate=1 then exit;
  if Assigned(FOnChange) then FOnChange(Self);
end;

{------------------------------------------------------------------------------}
procedure TLMDObject.GetChange(Sender:TObject);
begin
  Change;
end;

{---------------------------- Public ------------------------------------------}
constructor TLMDObject.Create;
begin
  inherited Create;
  FUpdate := 0;
  FOwner := Owner;
end;

{------------------------------------------------------------------------------}
function TLMDObject.GetSetStr : String;
begin
  result := '';
end;

{------------------------------------------------------------------------------}
procedure TLMDObject.BeginUpdate;
begin
  FUpdate:=1;
end;

{------------------------------------------------------------------------------}
procedure TLMDObject.EndUpdate(aValue:Boolean);
begin
  FUpdate:=0;
  if aValue then Change;
end;

{------------------------------------------------------------------------------}
function TLMDObject.IsUpdating:Boolean;
begin
  result:=FUpdate<>0;
end;

{$IFDEF LMD_DEBUGTRACE}
initialization
  {$I C1.INC}
{$ENDIF}

end.
