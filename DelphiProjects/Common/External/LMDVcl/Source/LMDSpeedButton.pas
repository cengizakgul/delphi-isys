unit LMDSpeedButton;
{$I lmdcmps.Inc}
{###############################################################################

LMDSpeedButton unit (RM)
------------------------


Changes
-------
Release 8.0 (December 2006)
 - Initial Release

###############################################################################}

interface

uses
  LMDCustomSpeedButton;

type
  TLMDSpeedButton=class(TLMDCustomSpeedButton)
  public
    property Transparent;
  published
    property Align;
    property ButtonStyle;
    property AllowAllUp;
    property GroupIndex;
    property MultiLine;
    property Pressed;
    property DropDownCombo;
    property DropDownComboWidth;
    property DropDownMenu;
    property DropDownIndent;
    property DropDownRight;
  end;

implementation

end.
 