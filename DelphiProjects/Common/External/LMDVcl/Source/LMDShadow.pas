unit LMDShadow;
{$I lmdcmps.inc}

{###############################################################################

LMDShadow unit ()
-----------------

Changes
-------
Release 8.0 (October 2006)
 - Initial Release

###############################################################################}

interface

uses
  Classes, Graphics,
  LMDGraph, LMDObject;

type
  TLMDShadow = class(TLMDObject)
    private
      FColor : TColor;
      FDepth : TLMDShadowDepth;
      procedure SetColor(AValue:TColor);
      procedure SetDepth(AValue:TLMDShadowDepth);
      procedure SetStyle(AValue:TLMDShadowStyle);
    protected
      FStyle : TLMDShadowStyle;
    public
      constructor Create(Owner:TPersistent=nil);override;
      procedure Assign(Source:TPersistent);override;
      function GetSetStr : String; override;
    published
      property Color:TColor read FColor write SetColor default clBtnShadow;
      property Depth:TLMDShadowDepth read FDepth write SetDepth default 6;
      property Style:TLMDShadowStyle read FStyle write SetStyle default hssSolid;
  end;

implementation
uses
  SysUtils, TypInfo;

{------------------------- Private --------------------------------------------}
Procedure TLMDShadow.SetColor(aValue:TColor);
begin
  if aValue<>FColor then
    begin
      FColor:=aValue;
      Change;
    end;
end;

{------------------------------------------------------------------------------}
Procedure TLMDShadow.SetDepth(aValue:TLMDShadowDepth);
begin
  if aValue<>FDepth then
    begin
      FDepth:=aValue;
      Change;
    end;
end;

{------------------------------------------------------------------------------}
Procedure TLMDShadow.SetStyle(aValue:TLMDShadowStyle);
begin
  if aValue<>FStyle then
    begin
      FStyle:=aValue;
      Change;
    end;
end;

{------------------------- Public ---------------------------------------------}
constructor TLMDShadow.Create;
begin
  inherited Create;
  FColor:=clBtnShadow;
  FStyle:=hssSolid;
  FDepth:=6;
end;

{------------------------------------------------------------------------------}
procedure TLMDShadow.Assign(Source:TPersistent);
begin
  if Source=Self then exit;
  if Source is TLMDShadow then
    begin
      FStyle:=TLMDShadow(Source).Style;
      FColor:=TLMDShadow(Source).Color;
      FDepth:=TLMDShadow(Source).Depth;
      Change;
      exit;
    end;
  inherited Assign(Source);
end;

{------------------------------------------------------------------------------}
function TLMDShadow.GetSetStr : String;
begin
  result:=GetEnumName(TypeInfo(TLMDShadowStyle), Ord(FStyle));
  result:='{'+Copy(result, 3, Length(result)-2);
  if FStyle<>hssNone then
    result:=result+', '+IntToStr (FDepth);
  result:=result+'}';
end;

end.
