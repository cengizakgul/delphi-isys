unit dynapdf;
{$LONGSTRINGS ON}
// Treat enums as 32 bit integer!!!
{$Z4}
interface

{
           DELPHI INTERFACE FILE FOR DYNAPDF VERSION 3.0.39.
   Copyright (c) Jens Boschulte, DynaForms GmbH, www.dynaforms.com.
   Please report errors and other potential problems to support@dynaforms.com.

   --------------------------------- Thank you! -------------------------------------

   Creation date: March 01, 2015
}

{$IFNDEF FPC} // Lazarus (Free Pascal)? 
	{$UNDEF DELPHI2_OR_HIGHER}
	{$UNDEF DELPHI3_OR_HIGHER}
	{$UNDEF DELPHI4_OR_HIGHER}
	{$UNDEF DELPHI5_OR_HIGHER}
	{$UNDEF DELPHI6_OR_HIGHER}
	{$UNDEF DELPHI7_OR_HIGHER}
	{$UNDEF DELPHI8_OR_HIGHER}
	{$UNDEF DELPHI9_OR_HIGHER}  // 2005
	{$UNDEF DELPHI10_OR_HIGHER} // 2006
	{$UNDEF DELPHI11_OR_HIGHER} // 2007
	{$UNDEF DELPHI_UNICODE}     // WideString is default
	{$UNDEF CROSS_PLATFORM_SUPPORT} // Delphi XE 2 or higher

	{$IFNDEF VER80} // Delphi 1.0
	  {$DEFINE DELPHI2_OR_HIGHER}
	  {$IFNDEF VER90} // Delphi 2.0
		  {$IFNDEF VER93} // C++ Builder 1.0
			 {$DEFINE DELPHI3_OR_HIGHER}
			 {$IFNDEF VER100} // Delphi 3.0
				 {$IFNDEF VER110} // C++ Builder 3.0
					{$DEFINE DELPHI4_OR_HIGHER}
					{$IFNDEF VER120} // Delphi 4.0
						{$IFNDEF VER125} // C++ Builder 4.0
						  {$DEFINE DELPHI5_OR_HIGHER}
							{$IFNDEF VER130} // Delphi 5
								{$DEFINE DELPHI6_OR_HIGHER}
								{$IFNDEF VER140} // Delphi 6
									{$DEFINE DELPHI7_OR_HIGHER}
									{$IFNDEF VER150} // Delphi 7
										{$DEFINE DELPHI8_OR_HIGHER}
										{$IFNDEF VER160} // Delphi 8
											{$DEFINE DELPHI9_OR_HIGHER}
											{$IFNDEF VER170} // Delphi 2005
												{$DEFINE DELPHI10_OR_HIGHER}
												{$IFNDEF VER180} // Delphi 2006
													{$DEFINE DELPHI11_OR_HIGHER}
													{$DEFINE DELPHI_UNICODE} // This is the important point. From now WideString is the default string format.
													{$IFNDEF VER185} // Delphi 2007
														{$IFNDEF VER200} // Delphi 2009
															{$IFNDEF VER210} // Delphi 2010
																{$IFNDEF VER220} // Delphi XE
																	{$IFNDEF VER230} // Delphi XE 2
																		{$DEFINE CROSS_PLATFORM_SUPPORT}
																	{$ENDIF}
																{$ENDIF}
															{$ENDIF}
														{$ENDIF}
													{$ENDIF}
												{$ENDIF}
											{$ENDIF}
										{$ENDIF}
									{$ENDIF}
								{$ENDIF}
							{$ENDIF}
						{$ENDIF}
					{$ENDIF}
				 {$ENDIF}
			 {$ENDIF}
		  {$ENDIF}
	  {$ENDIF}
	{$ENDIF}
{$ENDIF}

// Not sure whether MSWINDOWS is defined in all Delphi versions...
{$IFDEF WIN32}
  {$IFNDEF MSWINDOWS}
     {$DEFINE MSWINDOWS}
  {$ENDIF}
{$ENDIF}

uses {$IFDEF MSWINDOWS}Windows, {$ENDIF} Classes {$IFDEF FPC}, LCLIntf, dynlibs {$ENDIF};

{ -------------------------------------- Error types, specific error values ------------------------------------- }

{$IFDEF FPC}
Type
  HDC          = type NativeUInt;
  HWND         = type NativeUInt;
  HBITMAP      = type NativeUInt;
  HENHMETAFILE = type NativeUInt;
{$ENDIF}

// For Mac-Delphi
{$IFDEF POSIX}
type
  HDC          = type NativeUINT;
  HWND         = type NativeUInt;
  HBITMAP      = type NativeUInt;
  HENHMETAFILE = type NativeUInt;
{$ENDIF}

{$IFNDEF DELPHI6_OR_HIGHER}
   type PCardinal  = ^Cardinal;
   type PPointer   = ^Pointer;
   type PPAnsiChar = ^PAnsiChar;
{$ENDIF}

// NativeInt is not defined in earlier versions
{$IFNDEF DELPHI_UNICODE}
type NativeInt = Integer;
{$ENDIF}

type INFM  = Pointer; // Pointer of a number format object
type PINFM = ^INFM;   // Array of INFM pointers
type IMSR  = Pointer; // Pointer of a Measure object
type IPTD  = Pointer; // Point data
type LGRP  = Pointer; // Pointer of a Layer Group

type TErrMode = Integer;
const emIgnoreAll    = $00000000;
const emSyntaxError  = $00000001;
const emValueError   = $00000002;
const emWarning      = $00000004;
const emFileError    = $00000008;
const emFontError    = $00000010;
const emAllErrors    = $0000FFFF;
const emNoFuncNames  = $10000000; // Do not print function names in error messages
const emUseErrLog    = $20000000; // Redirect all error messages to the error log

const E_WARNING      = $02000000;
const E_SYNTAX_ERROR = $04000000;
const E_VALUE_ERROR  = $08000000;
const E_FONT_ERROR   = $10000000;
const E_FATAL_ERROR  = $20000000;
const E_FILE_ERROR   = $40000000;

const ENEED_PWD        = -($000000B2 or E_FILE_ERROR);
const EWRONG_OPEN_PWD  = -($000000B3 or E_FILE_ERROR);
const EWRONG_OWNER_PWD = -($000000B4 or E_FILE_ERROR);
const EWRONG_PWD       = -($000000B5 or E_FILE_ERROR);

const PDF_MAX_INT      = $7FFFFFFF;

{ ------------------------------------------------- Default types ----------------------------------------------- }

const NO_COLOR: Cardinal = $FFFFFFF1; // Transparent color used by annotations and form fields

// This flag can be combined with the annotation handle in Set3DAnnotProps().
// 3D Annotations with a transparent background are supported since PDF 1.7, Extension Level 3
const TRANSP_3D_ANNOT: Cardinal = $40000000;

type T3DActivationType =
(
   at3D_AppDefault,
   at3D_PageOpen,    // The annotaiton should be activated when the page is opened.
   at3D_PageVisible, // The annotaiton should be activated when the page becomes visible.
   at3D_Explicit     // The annotation should remain inactive until explicitely activated by a script or action (default).
);

type T3DDeActivateType =
(
   dt3D_AppDefault,
   dt3D_PageClosed,    // The annotaiton should be deactivated as soon as the page is closed.
   dt3D_PageInvisible, // The annotaiton should be deactivated as soon as the page becomes invisible (default).
   dt3D_Explicit       // The annotation should remain active until explicitely deactivated by a script or action.
);

type T3DDeActInstance =
(
   di3D_AppDefault,
   di3D_UnInstantiated, // The annotation will be uninstantiated (default)
   di3D_Instantiated,   // The annotation is left instantiated
   di3D_Live            // If the 3D artwork contains an animation then it will stay live
);

type T3DInstanceType =
(
   it3D_AppDefault,
   it3D_Instantiated, // The annotation will be instantiated but animations are disabled.
   it3D_Live          // The annotation will be instantiated and animations are enabled (default).
);

type T3DLightingSheme =
(
   lsArtwork,
   lsBlue,
   lsCAD,
   lsCube,
   lsDay,
   lsHard,
   lsHeadlamp,
   lsNight,
   lsNoLights,
   lsPrimary,
   lsRed,
   lsWhite,
   lsNotSet
);

type T3DNamedAction =
(
   naDefault,
   naFirst,
   naLast,
   naNext,
   naPrevious
);

type T3DProjType =
(
   pt3DOrthographic,
   pt3DPerspective
);

type T3DRenderingMode =
(
   rmBoundingBox,
   rmHiddenWireframe,
   rmIllustration,
   rmShadedIllustration,
   rmShadedVertices,
   rmShadedWireframe,
   rmSolid,
   rmSolidOutline,
   rmSolidWireframe,
   rmTransparent,
   rmTranspBBox,
   rmTranspBBoxOutline,
   rmTranspWireframe,
   rmVertices,
   rmWireframe,
   rmNotSet
);

type T3DScaleType =
(
   st3DValue,
   st3DWidth,
   st3DHeight,
   st3DMin,
   st3DMax
);

type TAFRelationship =
(
   arAssociated,
   arData,
   arSource,
   arSupplement,
   arAlternative
);

type TAFDestObject =
(
   adAnnotation,
   adCatalog,    // The documents catalog is the root object
   adField,
   adImage,
   adPage,
   adTemplate
);

type TAnnotFlags = Cardinal;
const afNone       = $00000000;
const afInvisible  = $00000001;
const afHidden     = $00000002;
const afPrint      = $00000004;
const afNoZoom     = $00000008;
const afNoRotate   = $00000010;
const afNoView     = $00000020;
const afReadOnly   = $00000040;

{
   By default all annotations which have an appearance stream and which have the print flag set are flattened.
   All annotations are deleted when the function returns with the exception of file attachment annotations.
   If you want to flatten the view state then set the flag affUseViewState.
}
type TAnnotFlattenFlags = Cardinal;
const affNone         = $00000000; // Printable annotations independent of the type
const affUseViewState = $00000001; // If set, annotations which are visible in a viewer become flattened.
const affMarkupAnnots = $00000002; // If set, markup annotations are flattened only. Link, Sound, or FileAttach
                                   // annotations are no markup annotations. These types will be left intact.

type TAnnotIcon =
(
  aiComment,
  aiHelp,
  aiInsert,
  aiKey,
  aiNewParagraph,
  aiNote,
  aiParagraph
);

type TActionType =
(
   atGoTo,
   atGoToR,
   atHide,
   atImportData,
   atJavaScript,
   atLaunch,
   atMovie,
   atNamed,
   atRendition,    // PDF 1.5
   atReset,        // ResetForm
   atSetOCGState,  // PDF 1.5
   atSound,
   atSubmit,       // SubmitForm
   atThread,
   atTransition,
   atURI,
   atGoTo3DView,   // PDF 1.6
   atGoToE,        // PDF 1.6 Like atGoToR but refers to an embedded PDF file.
   atRichMediaExec // PDF 1.7 Extension Level 3
);

type TAnnotString =
(
   asAuthor,
   asContent,
   asName,
   asSubject
);

type TAnnotType =
(
   atCaret,
   atCircle,
   atFileLink,    // A Link annotation with an associated GoToR action (go to remote)
   atFreeText,
   atHighlight,   // Highlight annotation
   atInk,
   atLine,
   atPageLink,    // A Link annotation with an associated GoTo action
   atPolygon,
   atPolyLine,
   atPopUp,
   atSquare,
   atSquiggly,    // Highlight annotation
   atStamp,
   atStrikeOut,   // Highlight annotation
   atText,        // Also used as container to store the State Model
   atUnderline,   // Highlight annotation
   atWebLink,     // A Link annotation with an associated URI action
   atWidget,      // Form Fields are handled separately
   at3D,          // PDF 1.6
   atSoundAnnot,  // PDF 1.2
   atFileAttach,  // PDF 1.3
   atRedact,      // PDF 1.7
   atWatermark,   // PDF 1.6
   atUnknown,     // Unknown annotation type
   atMovieAnnot,  // PDF 1.2
   atPrinterMark, // PDF 1.4
   atProjection,  // PDF 1.7 Extension Level 3
   atRichMedia,   // PDF 1.7 Extension Level 3
   atScreen,      // PDF 1.5
   atTrapNet      // PDF 1.3
);

type TBaseEncoding =
(
   beWinAnsi,
   beMacRoman,
   beMacExpert,
   beStandard
);

type TBBox = record
   x1: Single;
   y1: Single;
   x2: Single;
   y2: Single;
end;

type TBlendMode =
(
   bmNotSet,
   bmNormal,
   bmColor,
   bmColorBurn,
   bmColorDodge,
   bmDarken,
   bmDifference,
   bmExclusion,
   bmHardLight,
   bmHue,
   bmLighten,
   bmLuminosity,
   bmMultiply,
   bmOverlay,
   bmSaturation,
   bmScreen,
   bmSoftLight
);

type TBorderStyle =
(
   bsSolid,
   bsBevelled,
   bsInset,
   bsUnderline,
   bsDashed,
   bsUserDefined // Internal
);

type TBtnCaptionPos =
(
   bcpCaptionOnly,  // Default
   bcpImageOnly,    // No caption; image only
   bcpCaptionBelow, // Caption below the image
   bcpCaptionAbove, // Caption above the image
   bcpCaptionRight, // Caption on the right of the image
   bcpCaptionLeft,  // Caption on the left of the image
   bcpCaptionOver   // Caption overlaid directly on the image
);

type TButtonState =
(
   bsUp,
   bsDown,
   bsRollOver
);

type TCheckBoxChar =
(
   ccCheck,
   ccCircle,
   ccCross1,
   ccCross2,
   ccCross3,
   ccCross4,
   ccDiamond,
   ccSquare,
   ccStar
);

type TCheckOptions = Cardinal;
const coDefault                  = $0000FFFF;
const coEmbedSubsets             = $00000001;
const coDeleteTransferFuncs      = $00000002;
const coDeleteMultiMediaContents = $00000004;
const coDeleteActionsAndScripts  = $00000008;
const coDeleteInvRenderingIntent = $00000010;
const coFlattenFormFields        = $00000020;
const coReplaceV4ICCProfiles     = $00000040;
const coDeleteEmbeddedFiles      = $00000080;
const coDeleteOPIComments        = $00000100;
const coDeleteSignatures         = $00000200;
const coReplCCITTFaxWithFlate    = $00010000;
const coNoFontEmbedding          = $10000000;
const coDeletePostscript         = $00000400; // Delete Postscript XObjects. Rarely used and such Postscript fragments are meaningful on a Postscript device only.
                                              // It is usually safe to delete such objects.
const coDeleteAlternateImages    = $00000800; // Alternate images are seldom used and prohibited in PDF/A.
const coReComprJPEG2000Images    = $00001000; // Recompression results in lower quality and usually in larger images. It is often better to keep such files as is.


type TCheckBoxState =
(
   cbUnknown,
   cbChecked,
   cbUnChecked
);

type TCIDMetric = record
   Width: Single;
   x:     Single;
   y:     Single;
end;

type PCIDMetric = ^TCIDMetric;

type TClippingMode =
(
  cmEvenOdd,
  cmWinding,
  cmAllEvenOdd,
  cmAllWinding
);

type TCodepage =
(
   cp1250,
   cp1251,
   cp1252,
   cp1253,
   cp1254,
   cp1255,
   cp1256,
   cp1257,
   cp1258,
   cp8859_2,
   cp8859_3,
   cp8859_4,
   cp8859_5,
   cp8859_6,
   cp8859_7,
   cp8859_8,
   cp8859_9,
   cp8859_10,
   cp8859_13,
   cp8859_14,
   cp8859_15,
   cp8859_16,
   cpSymbol,
   cp437,
   cp737,
   cp775,
   cp850,
   cp852,
   cp855,
   cp857,
   cp860,
   cp861,
   cp862,
   cp863,
   cp864,
   cp865,
   cp866,
   cp869,
   cp874,
   cpUnicode,
   cpCJK_Big5_Uni,    // Big5 plus HKSCS extension.
   cpCJK_EUC_JP_Uni,  // EUC-JP
   cpCJK_EUC_KR_Uni,  // EUC-KR
   cpCJK_EUC_TW_Uni,  // CNS-11643-1992 (Planes 1-15).
   cpCJK_GBK_Uni,     // GBK is the Microsoft code page 936 (GB2312, EUC-CN plus GBK extension).
   cpCJK_GB12345_Uni, // GB-12345-1990 (Traditional Chinese form of GB-2312).
   cpCJK_HZ_Uni,      // Mixed ASCII / GB-2312 encoding
   cpCJK_2022_CN_Uni, // ISO-2022-CN-EXT (GB-2312 plus ISO-11643 Planes 1-7).
   cpCJK_2022_JP_Uni, // ISO-2022-JP
   cpCJK_2022_KR_Uni, // ISO-2022-KR
   cpCJK_646_CN_Uni,  // ISO-646-CN (GB-1988-80)
   cpCJK_646_JP_Uni,  // ISO-646-JP (JIS_C6220-1969-RO).
   cpCJK_IR_165_Uni,  // ISO-IR-165 (extended version of GB-2312).
   cpCJK_932_Uni,     // Microsoft extended version of SHIFT_JIS.
   cpCJK_949_Uni,     // EUC-KR extended with UHC (Unified Hangul Codes).
   cpCJK_950_Uni,     // Microsoft extended version of Big5.
   cpCJK_JOHAB_Uni,   // JOHAB
   cpShiftJIS,        // ShiftJIS charset plus code page 932 ectension.
   cpBig5,            // Big5 plus HKSCS extension.
   cpGB2312,          // GB2312 charset plus GBK and cp936 extension.
   cpWansung,         // Wansung
   cpJohab,           // JOHAB
   cpMacRoman,        // Mac Roman
   cpAdobeStd,        // This is an encoding for Type1 fonts. It should normally not be used.
   cpInternal,        // Internal -> not usable
   cpGlyphIndexes,    // Can be used with TrueType and OpenType fonts only. DynaPDF creates a reverse mapping so that copy & paste will work.
   cpPDFDocEnc,       // Internal -> not usable. Used for form fonts. This is a superset of the code page 1252 and MacRoman.
   cpExtCMap,         // Internal -> not usable. This code page is set when a font was loaded with an external cmap.
   cpDingbats,        // Internal -> Special encoding for ZapfDingbats
   cpMacExpert,       // Internal -> not usable.
   cpRoman8           // This is a standard PCL 5/6 code page
);

type TColorConvFlags = Cardinal;
const ccfBW_To_Gray   = $00000000; // Default, RGB Black and White set with rg or RG inline operators are converted to gray
const ccfRGB_To_Gray  = $00000001; // If set, inline color operators rg and RG are converted to gray
const ccfToGrayAdjust = $00000002; // Converts RGB and gray inline operators to gray and allows to darken or lighten the colors

type TCompBBoxFlags = Cardinal;
const cbfNone              = $00000000;
const cbfIgnoreWhiteAreas  = $00000001; // Ignore white vector graphics or text.
{
   Please note that images must be decompressed if one of the following flags are set. Parsing gray or color images
   is in most cases not useful and you should not parse such images if it is not really required.
}
const cbfParse1BitImages   = $00000002; // Find the visible area in 1 bit images. This is the most important case
                                        // since scanned faxes are usually 1 bit images.
const cbfParseGrayImages   = $00000004; // Find the visible area in gray images.
const cbfParseColorImages  = $00000008; // Find the visible area in color images. This is usually not required
                                        // and slow downs processing a lot.
const cbfParseAllImages    = $0000000E; // Find the visible area in all images.

// The data for user defined columns is stored in collection items.
type TColColumnType =
(
   cisCreationDate, // Data comes from the embedded file
   cisDescription,  // Data comes from the embedded file
   cisFileName,     // Data comes from the embedded file
   cisModDate,      // Data comes from the embedded file
   cisSize,         // Data comes from the embedded file
   cisCustomDate,   // User defined date.
   cisCustomNumber, // User defined nummber.
   cisCustomString  // User defined string.
);

type TColorMode =
(
   cmFill,
   cmStroke,
   cmFillStroke
);

type TColView =
(
   civNotSet,
   civDetails,
   civTile,
   civHidden,
   civCustom  // PDF 1.7 Extension Level 3, the collection view is presented by a SWF file.
);

type TCompressionLevel =
(
   clNone,
   clDefault,
   clFastest,
   clMax
);

type TCompressionFilter =
(
   cfFlate,    // PDF or TIFF output
   cfJPEG,     // PDF, JPEG, or TIFF output
   cfCCITT3,   // PDF or TIFF output -> B&W CCITT Fax G3 compression -> fast but less compression ratio
   cfCCITT4,   // PDF or TIFF output -> B&W CCITT Fax G4 compression -> slower but higher compression ratio
   cfLZW,      // TIFF or GIF output -> Very fast but less compression ratios than flate
   cfLZWBW,    // TIFF
   cfFlateBW,  // TIFF, PNG, or BMP output -> Dithered black & white output. The resulting image will be
               // compressed with Flate or left uncompressed if the output image format is a bitmap. If
               // you want to use CCITT Fax 4 compression (TIFF only) set the flag icUseCCITT4 in the
               // AddImage() function call. Note that this filter is not supported for PDF creation!
   cfJP2K      // PDF or JPEG2000 output
);

type TConformanceType =
(
   ctPDFA_1b_2005,
   ctNormalize
);

type TCTM = record
   a: Double;
   b: Double;
   c: Double;
   d: Double;
   x: Double;
   y: Double;
end;

type PCTM = ^TCTM;

type TDateType =
(
   dtCreationDate,
   dtModDate
);

type TDestType =
(
   dtXY_Zoom,       // three parameters (a, b, c) -> (X, Y, Zoom)
   dtFit,           // no parameters
   dtFitH_Top,      // one parameter    (a)
   dtFitV_Left,     // one parameter    (a)
   dtFit_Rect,      // four parameters  (a, b, c, d) -> (left, bottom, right, top)
   dtFitB,          // no parameter
   dtFitBH_Top,     // one parameter    (a)
   dtFitBV_Left     // one parameter    (a)
);

type TDuplexMode =
(
   dpmNone,          // Default
   dpmSimplex,
   dpmFlipShortEdge,
   dpmFlipLongEdge
);

type TEmbFileLocation =
(
   eflChild,         // The file is an embedded file in the current document
   eflChildAnnot,    // The file is located in a file attachment annotion in the current document
   eflExternal,      // The file is an embedded file in an external document
   eflExternalAnnot, // The file is located in a file attachment annotion in an external document 
   eflParent,        // The file is located in the parent document
   eflParentAnnot    // The file is located in a file attachment annotion in the parent document
);

type TEnumFontProcFlags = Integer;
const
   efpAnsiPath    = 0; // Code page 1252 on Windows, UTF-8 otherwise
   efpUnicodePath = 1; // FilePath is in Unicode format. Make a typecast to (UI16*) in this case.
   efpEmbeddable  = 2; // The font has embedding rights.
   efpEditable    = 4; // The font has editing rights (important for form fields).

type TFltPoint = record
   x: Single;
   y: Single;
end;

type PFltPoint = ^TFltPoint;
   
type TFltRect = record
	Left,
	Bottom,
	Right,
	Top: Single;
end;

type PFltRect = ^TFltRect;

type TIntRect = record
	x1,
	y1,
	x2,
	y2: Integer;
end;

type PIntRect = ^TIntRect;

type TPDFRect = record
   Left,
   Bottom,
   Right,
   Top: Double;
end;

type TBmkStyle = Cardinal;
const
	bmsNormal = 0;
	bmsItalic = 1;
	bmsBold   = 2;

type TBookmark = record
   Color:    Cardinal;
   DestPage: Integer;
   DestPos:  TPDFRect;
   DestType: TDestType;
   Open:     LongBool;
   Parent:   Integer;
   Style:    TBmkStyle; // 1 = Italic, 2 = Bold, 3 = BoldItalic
   Title:    Pointer;
   TitleLen: Cardinal;
   Unicode:  LongBool;
end;

{
   The font search run works as follows:

      - DynaPDF tries always to find the exact weight, if it cannot be found then a font with
        the next smaller weight is selected (if available).
      - Italic styles can always be emulated but it is not possible to emulate thinner weights or
        regular styles with an italic font.
      - If the specified weight is larger as the font weight the remaining weight will be emulated
        if the difference to the requested weight is larger than 200.
        With SetFontWeight() it is possible to control whether a missing weight should be emulated.
        If FontWeight is smaller or equal to the requested font weight then emulation will be disabled.

   TFStyle is a bitmask that is defined as follows:

      - Bits 0..7   // Style bits fsItalic, fsUnderlined, fsStriked
      - Bits 8..19  // Width class -> Defined for future use.
      - Bits 20..31 // Font Weight

   - A width class can be converted to a style constant by multiplying it with 256 (width shl 8).
   - A font weight can be converted to a style constant by multiplying it with 1048576 (weight shl 20).
   - Additional attributes can be added with a binary or operator (e.g. style or fsItalic).
   - Only one width class and one font weight can be set at time.

   - WidthFromStyle() extracts the width class.
   - WeightFromStyle() extracts the font weight.

   The following functions extract the width class or font weight from a style variable:

      widthClass  = WidthFromStyle(style);
      weightClass = WeightFromStyle(style);
}

type TFStyle = Integer;
const
   fsNone           = $00000000; // Regular weight (400)
   fsItalic         = $00000001;
   fsUnderlined     = $00000004;
   fsStriked        = $00000008;
   fsVerticalMode   = $00000010; // Not considered at this time
   // Width class
   fsUltraCondensed = $00000100; // 1
   fsExtraCondensed = $00000200; // 2
   fsCondensed      = $00000300; // 3
   fsSemiCondensed  = $00000400; // 4
   fsNormal         = $00000500; // 5
   fsSemiExpanded   = $00000600; // 6
   fsExpanded       = $00000700; // 7
   fsExtraExpanded  = $00000800; // 8
   fsUltraExpanded  = $00000900; // 9
   // Weight class
   fsThin           = $06400000; // 100
   fsExtraLight     = $0c800000; // 200
   fsLight          = $12C00000; // 300
   fsRegular        = $19000000; // 400 -> Same as fsNone
   fsMedium         = $1F400000; // 500
   fsDemiBold       = $25800000; // 600
   fsBold           = $2BC00000; // 700 -> The old constant 2 is still supported to preserve backward compatibility
   fsExtraBold      = $32000000; // 800
   fsBlack          = $38400000; // 900
   fsUltraBlack     = $3E800000; // 1000

type TDecodeFilter =
(
   dfNone,
   dfASCII85Decode,  // No parameters
   dfASCIIHexDecode, // No parameters
   dfCCITTFaxDecode, // Optional Parameters
   dfDCTDecode,      // Optional Parameters
   dfFlateDecode,    // Optional Parameters
   dfJBIG2Decode,    // Optional Parameters
   dfJPXDecode,      // No parameters
   dfLZWDecode,      // Optional Parameters
   dfRunLengthDecode // No parameters
);

type TDecSeparator =
(  // per thousand separator, decimal separator
   dsCommaDot,
   dsNoneDot,
   dsDotComma,
   dsNoneComma
);

type TDocumentInfo =
(
   diAuthor,
   diCreator,
   diKeywords,
   diProducer,
   diSubject,
   diTitle,
   diCompany,
   diPDFX_Ver,     // GetInDocInfo() or GetInDocInfoEx() only -> The PDF/X version is set by SetPDFVersion()!
   diCustom,       // User defined key
   diPDFX_Conf,    // GetInDocInfo() or GetInDocInfoEx() only. The value of the GTS_PDFXConformance key.
   diCreationDate, // GetInDocInfo() or GetInDocInfoEx() or after ImnportPDFFile() was called.
   diModDate       // GetInDocInfo() or GetInDocInfoEx() only
);

type TDrawDirection =
(
   ddCounterClockwise,
   ddClockwise
);

type TDrawMode =
(
   dmNormal,
   dmStroke,
   dmFillStroke,
   dmInvisible,
   dmFillClip,
   dmStrokeClip,
   dmFillStrokeClip,
   dmClipping
);

type TExtColorSpace =
(
   esDeviceRGB,   // Device color space
   esDeviceCMYK,  // Device color space
   esDeviceGray,  // Device color space
   esCalGray,     // CIE-based color space
   esCalRGB,      // CIE-based color space
   esLab,         // CIE-based color space
   esICCBased,    // CIE-based color space -> contains an ICC profile
   esPattern,     // Special color space
   esIndexed,     // Special color space
   esSeparation,  // Special color space
   esDeviceN,     // Special color space
   esNChannel     // Special color space
);

type TFieldColor =
(
   fcBackColor,
   fcBorderColor,
   fcTextColor
);

type TAnnotColor = TFieldColor;

type TFieldFlags = Cardinal;
const ffReadOnly         = $00000001;
const ffRequired         = $00000002;
const ffNoExport         = $00000004;

const ffInvisible        = $00000008;
const ffHidden           = $00000010;
const ffPrint            = $00000020;
const ffNoZoom           = $00000040;
const ffNoRotate         = $00000080;
const ffNoView           = $00000100;

const ffMultiline        = $00001000;  // Text fields only
const ffPassword         = $00002000;  // Text fields only
const ffNoToggleToOff    = $00004000;  // Radio buttons, check boxes
const ffRadioIsUnion     = $02000000;  // PDF-1.5 radio buttons
const ffCommitOnSelCh    = $04000000;  // PDF-1.5 combo boxes, list boxes

const ffEdit             = $00040000;  // Combo boxes only
const ffSorted           = $00080000;  // Combo boxes and list boxes -> sorts the choice values in ascending order
const ffFileSelect       = $00100000;  // PDF 1.4 Text fields only
const ffMultiSelect      = $00200000;  // PDF 1.4 List boxes only
const ffDoNotSpellCheck  = $00400000;  // PDF 1.4 Text fields, combo boxes. If the field is a combo box, this flag is meaningful only if ffEdit is also set.
const ffDoNotScroll      = $00800000;  // PDF 1.4 Text fields only
const ffComb             = $01000000;  // PDF 1.5 Text fields only

type TFieldType =
(
   ftButton,
   ftCheckBox,
   ftRadioBtn,
   ftComboBox,
   ftListBox,
   ftText,
   ftSignature,
   ftGroup     // this is not a real field type, it is just an array of fields
);

type TFileAttachIcon =
(
   faiGraph,
   faiPaperClip,
   faiPushPin,
   faiTag,
   faiUserDefined
);

type TFileOP =
(
   foOpen,
   foPrint
);

type TFlushPageFlags = Cardinal;
const
   fpfDefault      = 0; // Write anything to the file that is possible
   fpfImagesOnly   = 1; // If set, only images are written to the file. The pages are still
                        // in memory and can be modified with EditPage(). Flushed images can
                        // still be referenced in other pages. The image handles remain valid.
   fpfExclLastPage = 2; // If set, the last page is not flushed

type TFontBaseType =
(
   fbtTrueType, // TrueType, TrueType Collections, or OpenType fonts with TrueType outlines
   fbtType1,    // Type1 font
   fbtOpenType, // OpenType font with Postscript outlines
   fbtStdFont,  // PDF Standard font
   fbtDisabled  // This value can be used to disable a specific font format. See SetFontSearchOrder() for further information.
);
type PFontBaseType = ^TFontBaseType;

type TFontFileSubtype =
(
	ffsType1C,        // CFF based Type1 font
	ffsCIDFontType0C, // CFF based Type1 CID font
	ffsOpenType,      // TrueType based OpenType font
   ffsOpenTypeC,     // CFF based OpenType font
   ffsCIDFontType2,  // TrueType based CID Font
   ffsReserved1,
   ffsReserved2,
   ffsReserved3,
   ffsReserved4,
	ffsNoSubtype
);

type TFontSelMode =
(
   smFamilyName,
   smPostScriptName,
   smFullName
);

type TFontType =
(
   ftMMType1,
   ftTrueType,
   ftType0,    // Check the font file type to determine the font sub type
   ftType1,
   ftType3
);

type TGStateFlags = Cardinal;
const gfCompatible         = $00000000; // Compatible graphics state to earlier DynaPDF versions -> default
const gfRestorePageCoords  = $00000001; // Restore the coordinate system with the graphics state (the value of PageCoords, see SetPageCoords())
const gfRealTopDownCoords  = $00000002; // If set, the page coordinate system is not reset to bottom-up when transforming
                                        // the coordinate system. However, real top-down coordinates require a large internal
                                        // overhead and where never fully implemented. The usage of this flag should be avoided
                                        // if possible.
const gfNativeBlackWhite   = $00000004; // Do not convert RGB black or white to DeviceGray
const gfUseImageColorSpace = $00000008; // If set, the active color space is ignored when inserting an image. The color space is taken
                                        // from the image file instead.
const gfIgnoreICCProfiles  = $00000010; // Meaningful only if the flag gfUseImageColorSpace is set. If set, an embedded profile is not used to
                                        // create an ICCBased color space for the image. The image is inserted in the corresponding device
                                        // color space instead.
const gfAnsiStringIsUTF8   = $00000020; // If set, single byte strings in Ansi functions are treated as UTF-8 encoded Unicode strings.
const gfRealPassThrough    = $00000040; // If set, JPEG images are inserted as is. JPEG images are normally rebuild, also in pass-through mode, to avoid issues
                                        // with certain malformed JPEG images which cannot be displayed in Adobes Acrobat or Reader. If you know that your JPEG
                                        // images work then set this flag to avoid unnecessary processing time.
const gfNoBitmapAlpha      = $00000080; // If set, the alpha channel in 32 bit bitmaps will be ignored. Useful for bitmaps with an invalid alpha channel.
const gfNoImageDuplCheck   = $00000100; // If set, no duplicate check for images will be performed. This can significantly improve processing speed.


type THashType =
(
   htDetached, // CloseAndSignFileExt() returns the byte ranges of the finish PDF buffer to create a detached signature
   htSHA1      // CloseAndSignFileExt() returns the SHA1 hash of the PDF file so that it can be signed
);
                                        
type THighlightMode =
(
  hmNone,
  hmInvert,
  hmOutline,
  hmPush,
  hmPushUpd  // Update appereance stream on changes
);

type TICCProfileType =
(
   ictGray,
   ictRGB,
   ictCMYK,
   ictLab
);

type TImageConversionFlags = Cardinal;
const icNone      = 0; // Default
const icUseCCITT4 = 1; // Use CCITT Fax 4 compression instead of Flate for dithered images.

{
   TIFF is the only format that supports different compression filters. The Filter parameter of the function
   AddImage() is ignored if the image format supports only one specific compression filter.
   Note that images are automatically converted to the nearest supported color space if the image format does
   not support the color space of the image.
}
type TImageFormat =
(
   ifmTIFF,     // DeviceRGB, DeviceCMYK, DeviceGray, Black & White -> CCITT Fax Group 3/4, JPEG, Flate, LZW.
   ifmJPEG,     // DeviceRGB, DeviceCMYK, DeviceGray    -> JPEG compression.
   ifmPNG,      // DeviceGray, DeviceRGB, Black & White -> Flate compression.
   ifmReserved, // Reserved for future extensions.
   ifmBMP,      // DeviceGray, DeviceRGB, Black & White -> Uncompressed.
   ifmJPC       // DeviceRGB, DeviceCMYK, DeviceGray    -> JPEG2000 compression.
);

type TImportFlags = Cardinal;
const ifImportAll        = $0FFFFFFE; // default
const ifContentOnly      = $00000000;
// If this flag is set, only interactive objects are imported if any, Otherwise only empty pages are imported.
// This flag can be used to copy an interactive form to another PDF file.
const ifNoContent        = $00000001;
// The imported page is not converted to a template if ifImportAsPage is set.
// Note that this flag can cause resource conflicts. Use this flag carefully!
const ifImportAsPage     = $80000000;
// base objects
const ifCatalogAction    = $00000002;
const ifPageActions      = $00000004;
const ifBookmarks        = $00000008;
const ifArticles         = $00000010;
const ifPageLabels       = $00000020;
const ifThumbs           = $00000040;
const ifTranspGroups     = $00000080; // This flag is not longer considered.
const ifSeparationInfo   = $00000100;
const ifBoxColorInfo     = $00000200;
const ifStructureTree    = $00000400;
const ifTransition       = $00000800;
const ifSearchIndex      = $00001000;
const ifJavaScript       = $00002000;
const ifJSActions        = $00004000;
const ifDocInfo          = $00008000;
const ifEmbeddedFiles    = $00200000; // File attachments
const ifFileCollections  = $00400000; // File collections (PDF 1.7)
// Annotations -> Only the most important annotation types can be selected directly.
// Note that all annotation types can be deleted with DeleteAnnotation.
const ifAllAnnots        = $009F0000;
   const ifFreeText      = $00010000;
   const ifTextAnnot     = $00020000;
   const ifLink          = $00040000;
   const ifStamp         = $00080000;
   const if3DAnnot       = $00100000;
   const ifOtherAnnots   = $00800000;

// Interactive Form fields are annotations too but we handle this type separately!
const ifFormFields       = $01000000;
{ -------------------- Special flags -------------------- }
const ifPrepareForPDFA   = $10000000; // Replace LZW compression with Flate, set the Interpolate key of images to false, do not import embedded files.
const ifEnumFonts        = $20000000; // Import fonts for EnumDocFonts(). The document must be deleted when this flag is set!!!
const ifAllPageObjects   = $40000000; // Import links when using ImportPageEx() within an open page. The entire document should be imported in this case.

type TImportFlags2 = Cardinal;
const if2Default         = $00000000; // Default
const if2MergeLayers     = $00000001; // If set, layers with identical name are merged. If this flag is absent DynaPDF
                                      // imports such layers separately so that each layer refers still to the pages
                                      // where it was orignally used.
const if2Normalize       = $00000002; // Replace LZW compression with Flate, apply limit checks, repair errors if possible.
const if2UseProxy        = $00000004; // Not meaningful for PDF files which are loaded from a memory buffer. If set, all streams are loaded from the file
                                      // on demand and never hold in memory. This reduces drastically the memory usage and enables the processing of arbitrary
                                      // large PDF files with minimal memory usage. The corresponding PDF file(s) must not be deleted before CloseFile() or
                                      // CloseFileEx() was called.
const if2NoMetadata      = $00000008; // Ignore metadata streams which are attached to fonts, pages, images, and so on.
const if2DuplicateCheck  = $00000010; // Perform a duplicate check on color spaces, fonts, images, patterns, and templates when merging PDF files.

type TKeyLen =
(
   kl40bit,    // RC4 Encryption -> Acrobat 3 or higher
   kl128bit,   // RC4 Encryption -> Acrobat 5 or higher
   kl128bitEx, // RC4 Encryption -> Acrobat 6 or higher
   klAES128,   // AES Encryption -> Acrobat 7 or higher
   klAES256    // AES Encryption -> Acrobat 9 or higher
);

type TLineCapStyle =
(
  csButtCap,
  csRoundCap,
  csSquareCap
);

type TLineEndStyle =
(
   leNone,
   leButt,
   leCircle,
   leClosedArrow,
   leDiamond,
   leOpenArrow,
   leRClosedArrow,
   leROpenArrow,
   leSlash,
   leSquare
);

type TLineCaptionPos =
(
   cpInline, // The caption is centered inside the line
   cpTop     // The caption is drawn on top of the line
);

type TLineAnnotParms = record
   StructSize:       Cardinal;        // Must be set to sizeof(TLineAnnotParms)
   Caption:          LongBool;        // If true, the annotation string Content is used as caption.
   CaptionOffsetX:   Single;          // Horizontal offset of the caption from its normal position
   CaptionOffsetY:   Single;          // Vertical offset of the caption from its normal position
   CaptionPos:       TLineCaptionPos; // The position where the caption should be drawn if present
   LeaderLineLen:    Single;          // Length of the leader lines (positive or negative)
   LeaderLineExtend: Single;          // Optional leader line extend beyond the leader line (must be a positive value or zero)
   LeaderLineOffset: Single;          // Amount of space between the endpoints of the annotation and the leader lines (must be a positive value or zero)
end;

type PLineAnnotParms = ^TLineAnnotParms;

type TLineJoinStyle =
(
  jsMiterJoin,
  jsRoundJoin,
  jsBevelJoin
);

type TMetadataObj =
(
   mdoCatalog,
   mdoFont,
   mdoImage,
   mdoPage,
   mdoTemplate
);

type TMeasureNumFormat =
(
   mnfDecimal,
   mnfFractional,
   mnfRound,
   mnfTruncate
);

type TMeasureLblPos =
(
   mlpSuffix,
   mlpPrefix
);

type TMetaFlags = Cardinal;
const mfDefault           = $00000000; // Default conversion
const mfDebug             = $00000001; // Write debug information into the content stream
const mfShowBounds        = $00000002; // Show the bounding boxes of text strings
const mfNoTextScaling     = $00000004; // Do not scale text
const mfClipView          = $00000008; // Draw the file into a clipping rectangle
const mfUseRclBounds      = $00000010; // Use rclBounds instead of rclFrame
const mfNoClippingRgn     = $00000040; // Disables SelectClippingRegion and IntersectClipRect
const mfNoFontEmbedding   = $00000080; // Do not embed fonts -> Fonts should be embedded!!!
const mfNoImages          = $00000100; // Ignore image records
const mfNoStdPatterns     = $00000200; // Ignore standard patterns
const mfNoBmpPatterns     = $00000400; // Ignore bitmap patterns
const mfNoText            = $00000800; // Ignore text records
const mfUseUnicode        = $00001000; // Ignore ANSI_CHARSET
const mfUseTextScaling    = $00004000; // Scale text instead of using the intercharacter spacing array
const mfNoUnicode         = $00008000; // Avoid usage of Unicode fonts -> recommended to enable PDF 1.2 compability
const mfFullScale         = $00010000; // Scale coordinates to the window size. Recommended if 32 bit coordinates are used.
const mfUseRclFrame       = $00020000; // This flag should be set if the rclFrame rectangle is properly set
const mfDefBkModeTransp   = $00040000; // Initialize the background mode to transparent (SetBkMode() overrides this state).
const mfApplyBidiAlgo     = $00080000; // Apply the bidirectional algorithm on Unicode strings
const mfGDIFontSelection  = $00100000; // Use the GDI to select fonts
const mfRclFrameEx        = $00200000; // If set, and if the rclBounds rectangle is larger than rclFrame, the function
                                       // extends the output rectangle according to rclBounds and uses the resulting
                                       // bounding box to calculate the image size (rclBounds represents the unscaled
                                       // image size). This is probably the correct way to calculate the image size.
                                       // However, to preserve backward compatibility the default calculation cannot
                                       // be changed.
const mfNoTextClipping    = $00400000; // If set, the ETO_CLIPPED flag in text records is ignored.
const mfSrcCopy_Only      = $00800000; // If set, images which use a ROP code other than SRCCOPY are ignored. This is useful when processing Excel 2007 spool files.
const mfClipRclBounds     = $01000000; // If set, the graphic is drawn into a clipping path with the size of rclBounds.
                                       // This flag is useful if the graphic contains content outside of its bounding box.
const mfDisableRasterEMF  = $02000000; // If set, EMF files which use unsupported ROP codes are not rastered.
const mfNoBBoxCheck       = $04000000; // If set, the rclBounds and rclFrame rectangles are used as is. DynaPDF uses normally
                                       // the rclBounds rectangle to calculate the picture size if the resolution of the EMF file
                                       // seems to be larger than 1800 DPI since this is mostly an indication that the rclFrame
                                       // rectangle was incorrectly calculated. If you process EMF files in such a high resolution
                                       // then this flag must be set. The flag can be set by default.
const mfIgnoreEmbFonts    = $08000000; // If set, embedded fonts in GDIComment records will be ignored. This flag must be set if the fonts
													// of an EMF spool file were pre-loaded with ConvertEMFSpool(). Spool fonts must always be loaded
													// in a pre-processing step since required fonts are not necessarily embedded in the EMF files.


// Obsolete flags -> these flags are ignored, do no longer use them!
const mfUseSpacingArray   = $00000020; // enabled by default -> can be disabled with mfUseTextScaling
const mfIntersectClipRect = $00002000; // enabled by default -> can be disabled with mfNoClippingRgn

type TLoadCMapFlags = Cardinal;
const lcmDefault   = 0; // Load the cmaps in the directory now
const lcmRecursive = 1; // Load sub directories recursively
const lcmDelayed   = 2; // Load the cmap files only when a font requires an external cmap

type TNamedAction =
(
   naFirstPage,
   naLastPage,
   naNexPage,
   naPrevPage,
   naGoBack,
   naOpenDlg,
   naPrintDlg,
   naGeneralInfo,
   naFontsInfo,
   naSaveAs,
   naSecurityInfo,
   naFitPage,
   naFullScreen,
   naDeletePages,
   naQuit,
   naUserDefined    // Non predefined action
);

type TNegativeStyle =
(
   nsMinusBlack,
   nsRed,
   nsParensBlack,
   nsParensRed
);

type TObjEvent =
( // All actions which should be applied to an event (except On Mouse Up) must be a JavaScript action!
   oeNoEvent,           // Internal use only -> DO NOT USE THIS VALUE!!!
   oeOnOpen,            // Catalog, Pages
   oeOnClose,           // Pages only
   oeOnMouseUp,         // All fields, page link annotations, bookmarks
   oeOnMouseEnter,      // Form fields only
   oeOnMouseExit,       // Form fields only
   oeOnMouseDown,       // Form fields only
   oeOnFocus,           // Form fields only
   oeOnBlur,            // Form fields only
   oeOnKeyStroke,       // Text fields only
   oeOnFormat,          // Text fields only
   oeOnCalc,            // Text fields, combo boxes, list boxes
   oeOnValidate,        // All form fields, except buttons
   oeOnPageVisible,     // PDF 1.5 -> Form fields only
   oeOnPageInVisible,   // PDF 1.5 -> Form fields only
   oeOnPageOpen,        // PDF 1.5 -> Form fields only
   oeOnPageClose,       // PDF 1.5 -> Form fields only
   oeOnBeforeClosing,   // PDF 1.4 -> Catalog only
   oeOnBeforeSaving,    // PDF 1.4 -> Catalog only
   oeOnAfterSaving,     // PDF 1.4 -> Catalog only
   oeOnBeforePrinting,  // PDF 1.4 -> Catalog only
   oeOnAfterPrinting    // PDF 1.4 -> Catalog only
);

type TObjType =
(
   otAction,
   otAnnotation,
   otBookmark,
   otCatalog,           // PDF 1.4
   otField,
   otPage,
   otPageLink
);

type TOCObject =
(
   ooAnnotation,
   ooField,
   ooImage,
   ooTemplate
);

type TOCAppEvent = Cardinal;
	const	aeExport = 1;
	const aePrint  = 2;
	const aeView   = 4;

   type TOCGIntent = Cardinal;
   const oiDesign  = 2;
   const oiView    = 4;  // Default
   const oiAll     = 8;
   const oiEmpty   = 16; // Internal
   // Special flag for GetOCG().
   const oiVisible = 32; // This flag is not considered when creating a layer. It is only used in GetOCG() to determine whether a layer is visible.

type TOCGUsageCategory = Cardinal;
   const	oucNone     = 0;
   const oucExport   = 1;
   const oucLanguage = 2;
   const oucPrint    = 4;
	const oucUser     = 8;
	const oucView     = 16;
   const oucZoom     = 32;

type TOCPageElement =
(
	peBackgroundImage, // BG
	peForegroundImage, // FG
	peHeaderFooter,    // HF
	peLogo,            // L
	peNone
);

type TOCUserType =
(
	utIndividual,
	utOrganization,
	utTitle,
	utNotSet
);

type TOCVisibility =
(
   ovAllOff,
   ovAllOn,
   ovAnyOff,
   ovAnyOn,
   ovNotSet  // Internal
);

type TOrigin =
(
  orDownLeft,
  orTopLeft
);

type TPageCoord =
(
  pcBottomUp,
  pcTopDown
);

type TPageFormat =
(
  pfDIN_A3,
  pfDIN_A4,
  pfDIN_A5,
  pfDIN_B4,
  pfDIN_B5,
  pfDIN_B6,
  pfDIN_C3,
  pfDIN_C4,
  pfDIN_C5,
  pfDIN_C6,
  pfDIN_C65,
  pfDIN_DL,
  pfDIN_E4,
  pfDIN_E5,
  pfDIN_E6,
  pfDIN_E65,
  pfDIN_M5,
  pfDIN_M65,
  pfUS_Legal,
  pfUS_Letter
);

type TPageBoundary =
(
   pbArtBox,
   pbBleedBox,
   pbCropBox,
   pbTrimBox,
   pbMediaBox
);

type TPageMode =
(
   pmUseNone,
   pmUseOutLines,
   pmUseThumbs,
   pmFullScreen,
   pmUseOC,
   pmUseAttachments
);

type TPageLabelFormat =
(
   plfDecimalArabic,    // 1,2,3,4...
   plfUppercaseRoman,   // I,II,III,IV...
   plfLowercaseRoman,   // i,ii,iii,iv...
   plfUppercaseLetters, // A,B,C,D...
   plfLowercaseLetters, // a,b,c,d...
   plfNone
);

type TPageLayout =
(
   plSinglePage,
   plOneColumn,
   plTwoColumnLeft,
   plTwoColumnRight,
   plTwoPageLeft,
   plTwoPageRight,
   plDefault
);

type TParseFlags = Cardinal;
const pfNone             = $00000000;
const pfDecomprAllImages = $00000002; // This flag causes that all image formats will be decompressed
                                      // with the exception of JBIG2 compressed images. If this flag is
                                      // not set, images which are already stored in a valid image file
                                      // format are returned as is. This is the case for Gray and RGB JPEG
                                      // images and for JPEG2000 images. If you want to extract the images
                                      // of a PDF file this flag should NOT be set!

const pfNoJPXDecode      = $00000004; // Meaningful only if the flag pfDecomprAllImages is set. If set,
                                      // JPEG2000 images are returned as is so that you can use your own
                                      // library to decompress such images since the the entire JPEG2000
                                      // codec is still marked as experimental. If we find an alternative
                                      // to the currently used Jasper library then we will replace it
                                      // immediatly with another one...

// The following flags are ignored if an image is not decompressed. Note that only one flag must be set
// at time. If no color space conversion flag is set images are returned in their native or alternate
// device color space. Note that these flags do not convert colors of vector graphics and so on.
// Use the function ConvColor() to convert colors of the graphics state into a device space.
const pfDitherImagesToBW = $00000008; // Floyd-Steinberg dithering.
const pfConvImagesToGray = $00000010;
const pfConvImagesToRGB  = $00000020;
const pfConvImagesToCMYK = $00000040;
const pfImageInfoOnly    = $00000080; // If set, images are not decompressed. This flag is useful if you want
                                      // to enumerate the images of a PDF file or if you want to determine how
                                      // many images are stored in it.
                                      // Note that images can be compressed with multiple filters. The member
                                      // Filter of the structure TPDFImage contains only the last filter with
                                      // which the image was compressed. There is no indication whether multiple
                                      // decode filters are required to decompress the image buffer. So, it
                                      // makes no sense to set this flag if you want to try to decompress the
                                      // image buffer manually with your own decode filters.

type TPathFillMode =
(
   fmFillNoClose,
   fmStrokeNoClose,
   fmFillStrokeNoClose,
   fmFill,
   fmStroke,
   fmFillStroke,
   fmFillEvOdd,
   fmFillStrokeEvOdd,
   fmFillEvOddNoClose,
   fmFillStrokeEvOddNoClose,
   fmNoFill,
   fmClose
);

type TPatternType =
(
   ptColored,
   ptUnColored,
   ptShadingPattern // Cannot be created with DynaPDF
);

{
   Notice:
   When using a bidirectional 8 bit code page the bidi algorithm is applied by default in Left to Right mode
   also if the bidi mode is set to bmNone (default). This mode produces identical results in comparison to
   applications like Edit or WordPad.

   The Right to Left mode is available in applications which use Microsoft's Uniscribe, e.g. BabelPad or MS Word.
   This mode works very well with the Reference Bidi Algorithm which is used by DynaPDF but Uniscribe's Left to Right
   mode produces different results in comparison to the Reference Bidi Algorithm. Because the bidi algorithm that is
   used in Uniscribe is not published it is practically impossible to get the same result in Left to Right mode
   without using this library.
}
type TPDFBidiMode =
(
   bmLeftToRight, // Apply the bidi algorithm on Unicode strings in Left to Right layout.
   bmRightToLeft, // Apply the bidi algorithm on Unicode strings in Right to Left layout.
   bmNone         // Default -> do not apply the bidi algorithm
);

type TPrintScaling =
(
   psAppDefault, // Default
   psNone
);

type TPDFPrintSettings = record
   DuplexMode:        TDuplexMode;
   NumCopies:         Integer;      // -1 means not set. Values larger than 5 are ignored in viewer applications.
   PickTrayByPDFSize: Integer;      // -1 means not set. 0 == false, 1 == true.
   PrintRanges:       PCardinal;    // If set, the array contains PrintRangesCount * 2 values. Each pair consists
                                    // of the first and last pages in the sub-range. The first page in the PDF file
                                    // is denoted by 0.
   PrintRangesCount: Cardinal;      // Number of ranges available in PrintRanges.
   PrintScaling:     TPrintScaling; // dpmNone means not set.
   { Reserved fields for future extensions }
   Reserved0: Cardinal;
   Reserved1: Cardinal;
   Reserved2: Cardinal;
   Reserved3: Cardinal;
   Reserved4: Cardinal;
   Reserved5: Cardinal;
   Reserved6: Cardinal;
   Reserved7: Cardinal;
   Reserved8: Cardinal;
   Reserved9: Cardinal;
end;

type TPwdType = Cardinal;
const
   ptOpen        = 0;
   ptOwner       = 1;
   ptForceRepair = 2; // Meaningful only when opening a PDF file with OpenImportFile() or OpenImportBuffer().
                      // If set, the PDF parser rebuilds the cross-reference table by scanning all the objects
                      // in the file. This can be useful if the cross-reference table contains damages while
                      // the top level objects are intact. Setting this flag makes only sence if the file
                      // was already previously opened in normal mode and if errors occured when importing
                      // pages of it.
   ptDontCopyBuf = 4; // If set, OpenImportBuffer() does not copy the PDF buffer to an internal buffer. This
                      // increases the processing speed and reduces the memory usage. The PDF buffer must not
                      // be released until CloseImportFile() or CloseFile() was called.

type TRawImageFlags = Cardinal;
const
   rifByteAligned  = $1000;
   rifRGBData      = $2000;
   rifCMYKData     = $4000;

type TRenderingIntent =
(
   riAbsoluteColorimetric,
   riPerceptual,
   riRelativeColorimetric,
   riSaturation,
   riNone
);

type TRestrictions = Integer;
const
   rsDenyNothing      = $00000000;
   rsDenyAll          = $00000F3C;
   rsPrint            = $00000004;
   rsModify           = $00000008;
   rsCopyObj          = $00000010;
   rsAddObj           = $00000020;
   // 128/256 bit encryption only -> these flags are ignored if 40 bit encryption is used
   rsFillInFormFields = $00000100;
   rsExtractObj       = $00000200;
   rsAssemble         = $00000400;
   rsPrintHighRes     = $00000800;
   rsExlMetadata      = $00001000; // PDF 1.5 Exclude metadata streams -> 128/256 bit encryption bit only.
   rsEmbFilesOnly     = $00002000; // PDF 1.6 Encrypt embedded files only -> Requires AES encryption.

type TRubberStamp =
(
   rsApproved,
   rsAsIs,
   rsConfidential,
   rsDepartmental,
   rsDraft,
   rsExperimental,
   rsExpired,
   rsFinal,
   rsForComment,
   rsForPublicRelease,
   rsNotApproved,
   rsNotForPublicRelease,
   rsSold,
   rsTopSecred,
   rsUserDefined
);

type TSoftMaskType =
(
   smtAlpha,
   smtLuminosity
);

type TTabOrder =
(
   toRow,
   toColumn,
   toStructure,
   toNone,
   toAnnots, // Annotation order -> PDF 1.7 Extension Level 3
   toFields  // Widget order (Form Fields) -> PDF 1.7 Extension Level 3
);

type TTilingType =
(
   ttConstSpacing,
   ttNoDistortion,
   ttFastConstSpacing
);

type TTextAlign = Cardinal;
const
   taLeft      = 0;
   taCenter    = 1;
   taRight     = 2;
   taJustify   = 3;
   taPlainText = $10000000; // If this flag is set alignment and command tags are interpreted as plain text.
                            // See WriteFText() in the help file for further information.

type TTextRecordA = record
   Advance: Single;    // Negative values move the cursor to right, positive to left. The value is measured in text space!
   Text:    PAnsiChar; // Raw text (not null-terminated)
   Length:  Integer;   // Raw text length in bytes
end;

type TTextRecordAPtr = ^TTextRecordA;

type TTextRecordW = record
   Advance: Single;    // Negative values move the cursor to right, positive to left. The value is measured in text space!
   Text:    PWideChar; // Already translated Unicode string (not null-terminated)
   Length:  Integer;   // Length in characters
   Width:   Single;    // String width measured in text space
end;

type TTextRecordWPtr = ^TTextRecordW;

type TPDFAnnotation = record
   AnnotType:     TAnnotType;
   Deleted:       LongBool;
   BBox:          TPDFRect;
   BorderWidth:   Double;
   BorderColor:   Cardinal;
   BorderStyle:   TBorderStyle;
   BackColor:     Cardinal;
   Handle:        Cardinal;
   AuthorA:       PAnsiChar;
   AuthorW:       PWideChar;
   ContentA:      PAnsiChar;
   ContentW:      PWideChar;
   NameA:         PAnsiChar;
   NameW:         PWideChar;
   SubjectA:      PAnsiChar;
   SubjectW:      PWideChar;
   PageNum:       Cardinal;
   HighlightMode: THighlightMode;
end;

type TPDFAnnotationEx = record
   AnnotType:        TAnnotType;
   Deleted:          LongBool;
   BBox:             TPDFRect;
   BorderWidth:      Single;
   BorderColor:      Cardinal;
   BorderStyle:      TBorderStyle;
   BackColor:        Cardinal;
   Handle:           Cardinal;
   AuthorA:          PAnsiChar;
   AuthorW:          PWideChar;
   ContentA:         PAnsiChar;
   ContentW:         PWideChar;
   NameA:            PAnsiChar;
   NameW:            PWideChar;
   SubjectA:         PAnsiChar;
   SubjectW:         PWideChar;
   PageNum:          Cardinal;
   HighlightMode:    THighlightMode;
   // Page link annotations only
   DestPage:         Integer;
   DestPos:          TPDFRect;
   DestType:         TDestType;
   DestFile:         PAnsiChar;        // File link or web link annotations
   // The Icon type depends on the annotation type. If the annotation type is atText then the Icon
   // is of type TAnnotIcon. If the annotation type is atFileAttach then it is of type
   // TFileAttachIcon. If the annotation type is atStamp then the Icon is the stamp type (TRubberStamp).
   // For any other annotation type this value is not set (-1).
   Icon:             Integer;
   StampName:        PAnsiChar;        // Set only, if Icon == rsUserDefined
   AnnotFlags:       Cardinal;         // See TAnnotFlags for available flags
   CreateDate:       PAnsiChar;        // Creation Date -> Optional
   ModDate:          PAnsiChar;        // Modification Date -> Optional
   Grouped:          LongBool;         // (Reply type) Meaningful only if Parent != -1 and Type != atPopUp. If true,
                                       // the annotation is part of an annotation group. Properties like Content, CreateDate,
                                       // ModDate, BackColor, Subject, and Open must be taken from the parent annotation.
   Open:             LongBool;         // Meaningful only for annotations which have a corresponding PopUp annotation.
   Parent:           Integer;          // Parent annotation handle of a PopUp Annotation or the parent annotation if
                                       // this annotation represents a state of a base annotation. In this case,
                                       // the annotation type is always atText and only the following members should
                                       // be considered:
                                       //    State      // The current state
                                       //    StateModel // Marked, Review, and so on
                                       //    CreateDate // Creation Date
                                       //    ModDate    // Modification Date
                                       //    Author     // The user who has set the state
                                       //    Content    // Not displayed in Adobe's Acrobat...
                                       //    Subject    // Not displayed in Adobe's Acrobat...
                                       // The PopUp annotation of a text annotation which represent an Annotation State
                                       // must be ignored.
   PopUp:            Integer;          // Handle of the corresponding PopUp annotation if any.
   State:            PAnsiChar;        // The state of the annotation.
   StateModel:       PAnsiChar;        // The state model (Marked, Review, and so on).
   EmbeddedFile:     Integer;          // FileAttach annotations only. A handle of an embedded file -> GetEmbeddedFile().
   Subtype:          PAnsiChar;        // Set only, if Type == atUnknownAnnot
   PageIndex:        Cardinal;         // The page index is used to sort form fields. See SortFieldsByIndex().
   MarkupAnnot:      LongBool;         // If true, the annotation is a markup annotation. Markup annotations can be flattened
                                       // separately, see FlattenAnnots().
   Opacity:          Single;           // Opacity = 1.0 = Opaque, Opacity < 1.0 = Transparent, Markup annotations only
   QuadPoints:       PSingle;          // Highlight, Link, and Redact annotations only
   QuadPointsCount:  Cardinal;         // Highlight, Link, and Redact annotations only

   DashPattern:      PSingle;          // Only present if BorderStyle == bsDashed
   DashPatternCount: Cardinal;         // Number of values in the array

   Intent:           PAnsiChar;        // Markup annotations only. The intent allows to distinguish between different uses of an annotation.
                                       // For example, line annotations have two intents: LineArrow and LineDimension.
   LE1:              TLineEndStyle;    // Line end style of the start point -> Line and PolyLine annotations only
   LE2:              TLineEndStyle;    // Line end style of the end point -> Line and PolyLine annotations only
   Vertices:         PSingle;          // Line, PolyLine, and Polygon annotations only
   VerticesCount:    Cardinal;         // Number of values in the array. This is the raw number of floating point values.
                                       // Since a vertice requires always two coordinate pairs, the number of vertices
                                       // or points is VerticeCount divided by 2.
   // Line annotations only. These properties should only be considered if the member Intent is set to the string LineDimension.
   Caption:          LongBool;         // If true, the annotation string Content is used as caption. The string is shown in a PopUp annotation otherwise.
   CaptionOffsetX:   Single;           // Horizontal offset of the caption from its normal position
   CaptionOffsetY:   Single;           // Vertical offset of the caption from its normal position
   CaptionPos:       TLineCaptionPos;  // The position where the caption should be drawn if present
   LeaderLineLen:    Single;           // Length of the leader lines (positive or negative)
   LeaderLineExtend: Single;           // Optional leader line extend beyond the leader line (must be a positive value or zero)
   LeaderLineOffset: Single;           // Amount of space between the endpoints of the annotation and the leader lines (must be a positive value or zero)
   
   { Reserved fields for future extensions }
   Reserved01:    Pointer;
   Reserved02:    Pointer;
   Reserved03:    Pointer;
   Reserved04:    Pointer;
   Reserved05:    Pointer;
   Reserved06:    Pointer;
   Reserved07:    Pointer;
   Reserved08:    Pointer;
end;

type TPDFBarcode = record
   StructSize:   Cardinal;  // Must be set to sizeof(TPDFBarcode) before calling CreateBarcodeField()!
   CaptionA:     PAnsiChar; // Optional, the ansi string takes precedence
   CaptionW:     PWideChar; // Optional
   ECC:          Single;    // 0..8 for PDF417, or 0..3 for QRCode
   Height:       Single;    // Height in inches
   nCodeWordCol: Single;    // Required for PDF417. The number of codewords per barcode coloumn.
   nCodeWordRow: Single;    // Required for PDF417. The number of codewords per barcode row.
   Resolution:   Cardinal;  // Required -> Should be 300
   Symbology:    PAnsiChar; // PDF417, QRCode, or DataMatrix.
   Version:      Single;    // Should be 1
   Width:        Single;    // Width in inches
   XSymHeight:   Single;    // Only needed for PDF417. The vertical distance between two barcode modules,
                            // measured in pixels. The ratio XSymHeight/XSymWidth shall be an integer
                            // value. For PDF417, the acceptable ratio range is from 1 to 4. For QRCode
                            // and DataMatrix, this ratio shall always be 1.
   XSymWidth:    Single;    // Required -> The horizontal distance, in pixels, between two barcode modules.
end;

// The tags have the same meaning as the corresponding HTML tags.
// See PDF Reference for further information.
type TPDFBaseTag =
(
   btArt,
   btArtifact,
   btAnnot,
   btBibEntry,      // BibEntry -> Bibliography entry
   btBlockQuote,
   btCaption,
   btCode,
   btDiv,
   btDocument,
   btFigure,
   btForm,
   btFormula,
   btH,
   btH1,
   btH2,
   btH3,
   btH4,
   btH5,
   btH6,
   btIndex,
   btLink,
   btList,          // L
   btListElem,      // LI
   btListText,      // LBody
   btNote,
   btP,
   btPart,
   btQuote,
   btReference,
   btSection,       // Sect
   btSpan,
   btTable,
   btTableDataCell, // TD
   btTableHeader,   // TH
   btTableRow,      // TR
   btTOC,
   btTOCEntry       // TOCI
);

type TPDFBitmap = record
   StructSize: Cardinal; // Must be set to sizeof(TPDFBitmap)
   Buffer:     PByte;    // Image buffer
   BufSize:    Cardinal; // Buffer size in bytes
   DestX:      Integer;  // Destination x-coordinate on the main image (the rendered page)
   DestY:      Integer;  // Destination y-coordinate on the main image (the rendered page)
   Height:     Cardinal; // Image height in pixels
   Stride:     Integer;  // Scanline length in bytes
   Width:      Cardinal; // Image width
end;

type TPDFChoiceValue = record
   StructSize:  Cardinal; // Must be set with sizeof(TPDFChoiceValue)
   ExpValueA:   PAnsiChar;
   ExpValueW:   PWideChar;
   ExpValueLen: Cardinal;
   ValueA:      PAnsiChar;
   ValueW:      PWideChar;
   ValueLen:    Cardinal;
   Selected:    LongBool;
end;

{
   The structure contains several duplicate fields because CMap files contain usually a DSC comment
   section which provides Postscript specific initialization code. With exception of DSCResName the
   strings in the DSC section should not differ from their CMap counterparts. The Identity mapping
   of a character collection should contain the DSC comment "%%BeginResource: CMap (Identity)".
   Otherwise the string should be set to the CMap name.
}
type TPDFCMap = record
   StructSize:     Cardinal;  // Must be set to sizeof(TPDFCMap) before calling GetCMap()!
   BaseCMap:       PAnsiChar; // If set, this base cmap is required when loading the cmap.
   CIDCount:       Cardinal;  // 0 if not set.
   CMapName:       PAnsiChar; // The CMap name.
   CMapType:       Cardinal;  // Should be 1!
   CMapVersion:    Single;    // The CMap version.
   DSCBaseCMap:    PAnsiChar; // DSC comment.
   DSCCMapVersion: Single;    // DSC comment.
   DSCResName:     PAnsiChar; // DSC comment. If the CMap uses an Identity mapping this string should be set to Identity.
   DSCTitle:       PAnsiChar; // DSC comment -> DSC CMap name + Registry + Ordering + Supplement, e.g. "GB-EUC-H Adobe GB1 0"
   FileNameA:      PAnsiChar; // The file name and CMap name should be identical!
   FileNameW:      PWideChar; // The file name and CMap name should be identical!
   FilePathA:      PAnsiChar; // The Ansi string is set if the Ansi version of SetCMapDir() was used.
   FilePathW:      PWideChar; // The Unicode string is set if the Unicode version of SetCMapDir() was used.
   Ordering:       PAnsiChar; // CIDSystemInfo -> The Character Collection, e.g. Japan1.
   Registry:       PAnsiChar; // CIDSystemInfo -> The registrant of the Character Collection is usually Adobe.
   Supplement:     Cardinal;  // CIDSystemInfo -> The Supplement number should be supported in the used PDF Version.
   WritingMode:    Cardinal;  // 0 == Horizontal, 1 == Vertical
end;

type TPDFColorSpace =
(
   csDeviceRGB,
   csDeviceCMYK,
   csDeviceGray
);

type TPDFColorSpaceObj = record
   ColorSpaceType:   TExtColorSpace;
   Alternate:        TExtColorSpace; // Alternate color space or base space of an Indexed or Pattern color space.
   IAlternate:       Pointer;        // Only set if the color space contains an alternate or base color space -> GetColorSpaceObjEx().
   Buffer:           PByte;          // Contains either an ICC profile or the color table of an Indexed color space.
   BufSize:          Cardinal;       // Buffer length in bytes.
   BlackPoint:       PSingle;        // CIE blackpoint. If set, the array contains exactly 3 values.
   WhitePoint:       PSingle;        // CIE whitepoint. If set, the array contains exactly 3 values.
   Gamma:            PSingle;        // If set, one value per component.
   Range:            PSingle;        // min/max for each component or for the .a and .b components of a Lab color space.
   Matrix:           PSingle;        // XYZ matrix. If set, the array contains exactly 9 values.
   NumInComponents:  Cardinal;       // Number of input components.
   NumOutComponents: Cardinal;       // Number of output components.
   NumColors:        Cardinal;       // HiVal + 1 as specified in the color space. Indexed color space only.
   Colorants:        Array[0..31] of PAnsiChar; // UTF-8 Colorant names (Separation, DeviceN, and NChannel only).
   ColorantsCount:   Cardinal;       // The number of colorants in the array.
   Metadata:         PByte;          // Optional XMP metadata stream -> ICCBased only.
   MetadataSize:     Cardinal;       // Metadata length in bytes.
   IFunction:        Pointer;        // Pointer to function object -> Separation, DeviceN, and NChannel only.
   IAttributes:      Pointer;        // Optional attributes of DeviceN or NChannel color spaces -> GetNChannelOptions().
   IColorSpaceObj:   Pointer;        // Pointer of the corresponding color space object
   Reserved01:       Pointer;
   Reserved02:       Pointer;
   Reserved03:       Pointer;
   Reserved04:       Pointer;
   Reserved05:       Pointer;
   Reserved06:       Pointer;
   Reserved07:       Pointer;
   Reserved08:       Pointer;
   Reserved09:       Pointer;
end;

type TDeviceNAttributes = record
   IProcessColorSpace:    Pointer;    // Pointer to process color space or NULL -> GetColorSpaceEx().
   ProcessColorants:      Array[0..7] of PAnsiChar; // UTF-8 Process colorant names
   ProcessColorantsCount: Cardinal;   // Number of process colorants in the array or zero if not set.
   Separations:           Array[0..31] of Pointer; // Optional pointers to Separation color spaces -> GetColorSpaceEx().
   SeparationsCount:      Cardinal;   // Number of color spaces in the array.
   IMixingHints:          Pointer;    // Optional mixing hints. There is no API function at this time to access mixing hints.
   Reserved01:            Pointer;
   Reserved02:            Pointer;
   Reserved03:            Pointer;
   Reserved04:            Pointer;
end;

type TPDFDateTime =
(
   dfMM_D,
   dfM_D_YY,
   dfMM_DD_YY,
   dfMM_YY,
   dfD_MMM,
   dfD_MMM_YY,
   dfDD_MMM_YY,
   dfYY_MM_DD,
   dfMMM_YY,
   dfMMMM_YY,
   dfMMM_D_YYYY,
   dfMMMM_D_YYYY,
   dfM_D_YY_H_MM_TT,
   dfM_D_YY_HH_MM,
   { time formats }
   df24HR_MM,
   df12HR_MM,
   df24HR_MM_SS,
   df12HR_MM_SS
);

type TPDFError = record
   StructSize: Cardinal;  // Must be initialized to sizeof(TPDFError)
   Msg:        PAnsiChar; // The error message
   ObjNum:     Integer;   // -1 if not available
   Offset:     Integer;   // -1 if not available
   SrcFile:    PAnsiChar; // Source file
   SrcLine:    Cardinal;  // Source line
end;


// It is not possible to set all available graphic state parameters with DynaPDF, such as black generation functions,
// halftones and so on. The structure will be extended when further objects are supported.
type TPDFExtGState = record
   AutoStrokeAdjust: Cardinal;          // PDF_MAX_INT if not set
   BlendMode:        TBlendMode;        // Default bmNotSet
   FlatnessTol:      Single;            // -1.0 if not set
   OverPrintFill:    Cardinal;          // PDF_MAX_INT if not set
   OverPrintStroke:  Cardinal;          // PDF_MAX_INT if not set
   OverPrintMode:    Cardinal;          // PDF_MAX_INT if not set
   RenderingIntent:  TRenderingIntent;  // riNone if not set
   SmoothnessTol:    Single;            // -1.0 if not set
   FillAlpha:        Single;            // -1.0 if not set
   StrokeAlpha:      Single;            // -1.0 if not set
   AlphaIsShape:     Cardinal;          // PDF_MAX_INT if not set
   TextKnockout:     Cardinal;          // PDF_MAX_INT if not set
   SoftMaskNone:     LongBool;          // Disables the active soft mask
   SoftMask:         Pointer;           // Soft mask pointer or nil. See CreateSoftMask() for further information.
   Reserved1:        Pointer;
   Reserved2:        Pointer;
   Reserved3:        Pointer;
   Reserved4:        Pointer;
   Reserved5:        Pointer;
   Reserved6:        Pointer;
   Reserved7:        Pointer;
end;

// Extended graphics state dictionary
type TPDFExtGState2 = record
   AlphaIsShape:       Integer;          // PDF_MAX_INT if not set
   AutoStrokeAdjust:   Integer;          // PDF_MAX_INT if not set
   BlackGen:           Pointer;          // Function handle or NULL -> GetFunction()
   BlackGen2:          Pointer;          // Function handle or NULL -> GetFunction()
   BlendMode:         ^TBlendMode;       // Array of blend modes or nil
   BlendModeCount:     Cardinal;         // Number of blend modes
   FillAlpha:          Single;           // -1.0 if not set
   FlatnessTol:        Single;           // -1.0 if not set
   Halftone:           Pointer;          // Halftone handle or NULL -> GetHalftoneDict()
   OverPrintFill:      Integer;          // PDF_MAX_INT if not set
   OverPrintStroke:    Integer;          // PDF_MAX_INT if not set
   OverPrintMode:      Integer;          // PDF_MAX_INT if not set
   RenderingIntent:    TRenderingIntent; // riNone if not set
   SmoothnessTol:      Single;           // -1.0 if not set
   SoftMask:           Pointer;          // Soft mask handle or NULL
   StrokeAlpha:        Single;           // -1.0 if not set
   TextKnockout:       Integer;          // PDF_MAX_INT if not set
   TransferFunc:       Pointer;          // Array of functions -> GetFunction()
   TransferFuncCount:  Cardinal;         // Number of transfer functions
   TransferFunc2:      Pointer;          // Array of functions -> GetFunction()
   TransferFunc2Count: Cardinal;         // Number of transfer functions
   UnderColorRem:      Pointer;          // Function handle or NULL -> GetFunction()
   UnderColorRem2:     Pointer;          // Function handle or NULL -> GetFunction()
   SoftMaskNone:       LongBool;         // If true, the current softmask will be disabled
   Reserved1:          Pointer;
   Reserved2:          Pointer;
   Reserved3:          Pointer;
end;

type TPDFField = record
   FieldType:    Integer;
   Deleted:      LongBool;
   BBox:         TPDFRect;
   Handle:       Integer;
   FieldName:    PAnsiChar;
   FieldNameLen: Cardinal;
   BackCS:       Cardinal;
   TextCS:       Cardinal;
   BackColor:    Cardinal;
   BorderColor:  Cardinal;
   TextColor:    Cardinal;
   Checked:      LongBool;
   Parent:       Integer;
   KidCount:     Cardinal;
   Font:         PAnsiChar;
   FontSize:     Double;
   Value:        Pointer;
   UniVal:       LongBool;
   ValLen:       Cardinal;
   ToolTip:      Pointer;
   UniToolTip:   LongBool;
   ToolTipLen:   Cardinal;
end;

const PDF_ANNOT_INDEX = $40000000; // Special flag for GetPageFieldEx() to indicate that an annotation index
                                   // was passed to the function. See GetPageFieldEx() for further information.

type TPDFFieldEx = record
   StructSize:    Cardinal;       // Must be set to sizeof(TPDFFieldEx)
   Deleted:       LongBool;       // If true, the field was marked as deleted by DeleteField()
   BBox:          TPDFRect;       // Bounding box of the field in bottom-up coordinates
   FieldType:     TFieldType;     // Field type
   GroupType:     TFieldType;     // If GroupType != FieldType the field is a terminal field of a field group
   Handle:        Cardinal;       // Field handle
   BackColor:     Cardinal;       // Background color
   BackColorSP:   TExtColorSpace; // Color space of the background color
   BorderColor:   Cardinal;       // Border color
   BorderColorSP: TExtColorSpace; // Color space of the border color
   BorderStyle:   TBorderStyle;   // Border style
   BorderWidth:   Single;         // Border width
   CharSpacing:   Single;         // Text fields only
   Checked:       LongBool;       // Check boxes only
   CheckBoxChar:  Cardinal;       // ZapfDingbats character that is used to display the on state
   DefState:      TCheckBoxState; // Check boxes only
   DefValueA:     PAnsiChar;      // Optional default value
   DefValueW:     PWideChar;      // Optional default value
   IEditFont:     Pointer;        // Pointer to default editing font
   EditFont:      PAnsiChar;      // Postscript name of the editing font
   ExpValCount:   Cardinal;       // Combo and list boxes only. The values can be accessed with GetFieldExpValueEx()
   ExpValueA:     PAnsiChar;      // Check boxes only
   ExpValueW:     PWideChar;      // Check boxes only
   FieldFlags:    TFieldFlags;    // Field flags
   IFieldFont:    Pointer;        // Pointer to the font that is used by the field
   FieldFont:     PAnsiChar;      // Postscript name of the font
   Reserved:      Integer;        // Reserved field to avoid alignment errors
   FontSize:      Double;         // Font size. 0.0 means auto font size
   FieldNameA:    PAnsiChar;      // Note that children of a field group or radio button have no name
   FieldNameW:    PWideChar;      // Note that children of a field group or radio button have no name
   HighlightMode: THighlightMode; // Highlight mode
   IsCalcField:   LongBool;       // If true, the OnCalc event of the field is connected with a JavaScript action
   MapNameA:      PAnsiChar;      // Optional unique mapping name of the field
   MapNameW:      PWideChar;      // If true, MapName is an Unicode string
   MaxLen:        Cardinal;       // Text fields only -> zero means not restricted
   Kids:          PPointer;       // Array of child fields -> GetFieldEx2()
   KidCount:      Cardinal;       // Number of fields in the array
   Parent:        Pointer;        // Pointer to parent field or NULL
   PageNum:       Integer;        // Page on which the field is used or -1
   Rotate:        Integer;        // Rotation angle in degrees
   TextAlign:     TTextAlign;     // Text fields only
   TextColor:     Cardinal;       // Text color
   TextColorSP:   TExtColorSpace; // Color space of the field's text
   TextScaling:   Single;         // Text fields only
   ToolTipA:      PAnsiChar;      // Optional tool tip
   ToolTipW:      PWideChar;      // Optional tool tip
   UniqueNameA:   PAnsiChar;      // Optional unique name (NM key)
   UniqueNameW:   PWideChar;      // Optional unique name (NM key)
   ValueA:        PAnsiChar;      // Field value
   ValueW:        PWideChar;      // Field value
   WordSpacing:   Single;         // Text fields only
   PageIndex:     Cardinal;       // Array index to change the tab order, see SortFieldsByIndex().
   IBarcode:      Pointer;        // If present, this field is a barcode field. The field type is set to ftText
                                  // since barcode fields are extended text fields. -> GetBarcodeDict().
   ISignature:    Pointer;        // Signature fields only. Present only for imported signature fields which
                                  // which have a value. That means the file was digitally signed. -> GetSigDict().
                                  // Signed signature fields are always marked as deleted!
   ModDate:       PAnsiChar;      // Last modification date (optional)

   // Push buttons only. The down and roll over states are optional. If not present, then all states use the up state.
   // The handles of the up, down, and roll over states are template handles! The templates can be opened for editing
   // with EditTemplate2() and parsed with ParseContent().
   CaptionPos:    TBtnCaptionPos; // Where to position the caption relative to its image
   DownCaptionA:  PAnsiChar;      // Caption of the down state
   DownCaptionW:  PWideChar;      // Caption of the down state
   DownImage:     Integer;        // Image of the down state
   RollCaptionA:  PAnsiChar;      // Caption of the roll over state
   RollCaptionW:  PWideChar;      // Caption of the roll over state
   RollImage:     Integer;        // Image of the roll over state
   UpCaptionA:    PAnsiChar;      // Caption of the up state
   UpCaptionW:    PWideChar;      // Caption of the up state
   UpImage:       Integer;        // Image of the up state -> if > -1, the button is an image button
   Padding:       Integer;        // Padding field to avoid alignment issues
end;


type TPDFFileSpec = record
   Buffer:      PAnsiChar; // Buffer of an embedded file
   BufSize:     Cardinal;  // Buffer size in bytes.
   Compressed:  LongBool;  // Should be false if Decompress was true in the GetEmbeddedFile() call, otherwise usually true.
                           // DynaPDF decompresses Flate encoded streams only. Other filters can occur but this is very unusual.
   ColItem:     Pointer;   // If != NULL the embedded file contains a collection item with user defined data. This entry
                           // can occur in PDF Collections only (PDF 1.7). See "PDF Collections" in the help file for further
                           // information.
   Name:        PAnsiChar; // Name of the file specification in the name tree. This value is always present.
   NameUnicode: LongBool;  // Is Name in Unicode format?
   FileName:    PAnsiChar; // File name as 7 bit ASCII string.
   IsURL:       LongBool;  // If true, FileName contains a URL.
   UF:          PAnsiChar; // PDF 1.7. Same as FileName but Unicode is allowed.
   UFUnicode:   LongBool;  // Is UF in Unicode format?
   Desc:        PAnsiChar; // Description
   DescUnicode: LongBool;  // Is Desc in Unicode format?
   FileSize:    Cardinal;  // Size of the decompressed stream or zero if not known. Note: this is either the Size key of
                           // the Params dictionary if present or the DL key in the file stream. Whether this value is
                           // correct depends on the file creator! The parameter is definitely correct if the file was
                           // decompressed.
   MIMEType:    PAnsiChar; // MIME media type name as defined in Internet RFC 2046.
   CreateDate:  PAnsiChar; // Creation date as string. See help file "The standard date format".
   ModDate:     PAnsiChar; // Modification date as string. See help file "The standard date format".
   CheckSum:    PAnsiChar; // 16 byte MD5 digest. Note that this is a binary string. It is exactly 16 bytes long if set!
end;

type TPDFFontInfo = record
   StructSize:      Cardinal;         // Must be set to sizeof(TPDFFontInfo).
   Ascent:          Single;           // Ascent (optional).
   AvgWidth:        Single;           // Average character width (optional).
   BaseEncoding:    TBaseEncoding;    // Valid only if HaveEncoding is true.
   BaseFont:        PAnsiChar;        // PostScript Name or Family Name.
   CapHeight:       Single;           // Cap height (optional).
   CharSet:         PAnsiChar;        // The charset describes which glyphs are present in the font.
   CharSetSize:     Cardinal;         // Length of the CharSet in bytes.
   CIDOrdering:     PAnsiChar;        // SystemInfo -> A string that uniquely names the character collection within the specified registry.
   CIDRegistry:     PAnsiChar;        // SystemInfo -> Issuer of the character collection.
   CIDSet:          PByte;            // CID fonts only. This is a table of bits indexed by CIDs.
   CIDSetSize:      Cardinal;         // Length of the CIDSet in bytes.
   CIDSupplement:   Cardinal;         // CIDSystemInfo -> The supllement number of the character collection.
   CIDToGIDMap:     PByte;            // Allowed for embedded TrueType based CID fonts only.
   CIDToGIDMapSize: Cardinal;         // Length of the stream in bytes.
   CMapBuf:         PByte;            // Only available if the CMap was embedded.
   CMapBufSize:     Cardinal;         // Buffer size in bytes.
   CMapName:        PAnsiChar;        // CID fonts only (this is the encoding if the CMap is not embedded).
   Descent:         Single;           // Descent (optional).
   Encoding:        PWideChar;        // Unicode mapping 0..255 -> not available for CID fonts.
   FirstChar:       Cardinal;         // First char (simple fonts only).
   Flags:           Cardinal;         // The font flags describe various characteristics of the font. See help file for further information.
   FontBBox:        TBBox;            // This is the size of the largest glyph in this font. The bounding box is important for text selection.
   FontBuffer:      PByte;            // The font buffer is present if the font was embedded or if it was loaded from a file buffer.
   FontBufSize:     Cardinal;         // Font file size in bytes.
   FontFamilyA:     PAnsiChar;        // Optional Font Family (Family Name, always available for system fonts).
   FontFamilyW:     PWideChar;        // Optional Font Family (either the Ansi or Unicode string is set, but never both).
   FontFilePathA:   PAnsiChar;        // Only available for system fonts.
   FontFilePathW:   PWideChar;        // Either the Ansi or Unicode path is set, but never both.
   FontFileType:    TFontFileSubtype; // See description in the help file for further information.
   FontName:        PAnsiChar;        // Font name (should be the same as BaseFont).
   FontStretch:     PAnsiChar;        // Optional -> UltraCondensed, ExtraCondensed, Condensed, and so on.
   FontType:        TFontType;        // If ftType0 the font is a CID font. The Encoding is not set in this case.
   FontWeight:      Single;           // Font weight (optional).
   FullNameA:       PAnsiChar;        // System fonts only.
   FullNameW:       PWideChar;        // System fonts only (either the Ansi or Unicode string is set, but never both).
   HaveEncoding:    LongBool;         // If true, BaseEncoding was set from the font's encoding.
   HorzWidths:      PSingle;          // Horizontal glyph widths -> 0..HorzWidthsCount -1.
   HorzWidthsCount: Cardinal;         // Number of horizontal widths in the array.
   Imported:        LongBool;         // If true, the font was imported from an external PDF file.
   ItalicAngle:     Single;           // Italic angle
   Lang:            PAnsiChar;        // Optional language code defined by BCP 47.
   LastChar:        Cardinal;         // Last char (simple fonts only).
   Leading:         Single;           // Recommended leading (optional).
   Length1:         Cardinal;         // Length of the clear text portion of a Type1 font.
   Length2:         Cardinal;         // Length of the encrypted portion of a Type1 font program (Type1 fonts only).
   Length3:         Cardinal;         // Length of the fixed-content portion of a Type1 font program or zero if not present.
   MaxWidth:        Single;           // Maximum glyph width (optional).
   Metadata:        PByte;            // Optional XMP stream that contains metadata about the font file.
   MetadataSize:    Cardinal;         // Buffer size in bytes.
   MisWidth:        Single;           // Missing width (default = 0.0).
   Panose:          PByte;            // CID fonts only -> Optional 12 bytes long Panose string as described in Microsoft�s TrueType 1.0 Font Files Technical Specification.
   PostScriptNameA: PAnsiChar;        // System fonts only.
   PostScriptNameW: PWideChar;        // System fonts only (either the Ansi or Unicode string is set, but never both).
   SpaceWidth:      Single;           // Space width in font units. A default value is set if the font contains no space character.
   StemH:           Single;           // The thickness, measured vertically, of the dominant horizontal stems of glyphs in the font.
   StemV:           Single;           // The thickness, measured horizontally, of the dominant vertical stems of glyphs in the font.
   ToUnicode:       PByte;            // Only available for imported fonts. This is an embedded CMap that translates PDF strings to Unicode.
   ToUnicodeSize:   Cardinal;         // Buffer size in bytes.
   VertDefPos:      TFltPoint;        // Default vertical displacement vector.
   VertWidths:      PCIDMetric;       // Vertical glyph widths -> 0..VertWidthsCount -1.
   VertWidthsCount: Cardinal;         // Number of vertical widths in the array.
   WMode:           Cardinal;         // Writing Mode -> 0 == Horizontal, 1 == Vertical.
   XHeight:         Single;           // The height of lowercase letters (like the letter x), measured from the baseline, in fonts that have Latin characters.
end;

type TPDFFontObj = record
   Ascent:        Single;    // Ascent
   BaseFont:      PAnsiChar; // PostScript Name or Family Name
   CapHeight:     Single;    // Cap height
   Descent:       Single;    // Descent
   Encoding:      PWideChar; // Unicode mapping 0..255 -> not set if a CID font is selected
   FirstChar:     Cardinal;  // First char
   Flags:         Cardinal;  // Font flags -> font descriptor
   FontFamily:    PAnsiChar; // Optional Font Family (Family Name)
   FontFamilyUni: LongBool;  // If true, FontFamily is in Unicode format -> make a type cast to PWideChar in this case
   FontName:      PAnsiChar; // Font name -> font descriptor
   FontType:      TFontType; // If ftType0 the font is a CID font. The Encoding is not set in this case.
   ItalicAngle:   Single;    // Italic angle
   LastChar:      Cardinal;  // Last char
   SpaceWidth:    Single;    // Space width in font units. A default value is set if the font contains no space character.
   Widths:        PSingle;   // Glyph widths -> 0..WidthsCount -1
   WidthsCount:   Cardinal;  // Number of widths in the array
   XHeight:       Single;    // x-height
   DefWidth:      Single;    // Default character widths -> CID fonts only
   FontFile:      PAnsiChar; // Font file buffer -> only imported fonts are returned.
   Length1:       Cardinal;  // Length of the clear text portion of the Type1 font, or the length of the entire font program if FontType != ffType1.
   Length2:       Cardinal;  // Length of the encrypted portion of the Type1 font program (Type1 fonts only).
   Length3:       Cardinal;  // Length of the fixed-content portion of the Type1 font program or zero if not present.
   FontFileType:  TFontFileSubtype; // See description in the help file for further information.
end;

type TPDFImage = record
   Buffer:           PAnsiChar;         // Each scanline is aligned to a full byte.
   BufSize:          Cardinal;          // The size of the image buffer in bytes.
   Filter:           TDecodeFilter;     // Required decode filter if the image is compressed.
                                        // Possible values are dfDCTDecode (JPEG), dfJPXDecode (JPEG2000),
                                        // and dfJBIG2Decode. Other filters are always removed by DynaPDF since
                                        // a conversion to a native file format is then always required.
   OrgFilter:        TDecodeFilter;     // The image was compressed with this filter in the PDF file. This info is
                                        // useful to determine which compression filter should be used when creating
                                        // a new image file from the image buffer.
   JBIG2Globals:     PAnsiChar;         // Optional global page 0 segment (dfJBIG2Decode filter only).
   JBIG2GlobalsSize: Cardinal;          // The size of the bit stream in bytes.
   BitsPerPixel:     Cardinal;          // Bit depth of the image buffer. Possible values are 1, 2, 4, 8, 24, 32, and 64.
   ColorSpace:       TExtColorSpace;    // The color space refers either to the image buffer or to the color table if set.
                                        // Note that 1 bit images can occur with and without a color table.
   NumComponents:    Cardinal;          // The number of components stored in the image buffer.
   MinIsWhite:       LongBool;          // If true, the colors of 1 bit images are reversed.
   IColorSpaceObj:   Pointer;           // Pointer to the original color space.
   ColorTable:       PAnsiChar;         // The color table or NULL.
   ColorCount:       Cardinal;          // The number of colors in the color table.
   Width:            Cardinal;          // Image width in pixel.
   Height:           Cardinal;          // Image height in pixel.
   ScanLineLength:   Cardinal;          // The length of a scanline in bytes.
   InlineImage:      LongBool;          // If true, the image is an inline image.
   Interpolate:      LongBool;          // If true, image interpolation should be performed.
   Transparent:      LongBool;          // The meaning is different depending on the bit depth and whether a color
                                        // table is available. If the image is a 1 bit image and if no color table is available,
                                        // black pixels must be drawn with the current fill color.
                                        // If the image contains a color table ColorMask contains the range of indexes
                                        // in the form min/max index which appears transparent. If no color table is
                                        // present ColorMask contains the transparent ranges in the form min/max for
                                        // each component.
   ColorMask:        PAnsiChar;         // The array contains ranges in the form min/max (2 values per component) for each
                                        // component before decoding.
   IMaskImage:       Pointer;           // If set, a 1 bit image is used as a transparency mask. Call GetImageObjEx() to decode the image.
   ISoftMask:        Pointer;           // If set, a grayscale image is used as alpha channel. Call GetImageObjEx() to decode the image.
   Decode:           PSingle;           // If set, samples must be decoded. The array contains 2 * NumComponents values.
                                        // The decode array is never set if the image is returned decompressed since
                                        // it is already applied during decompression.
   Intent:           TRenderingIntent;  // Default riNone.
   SMaskInData:      Cardinal;          // JPXDecode only, PDF_MAX_INT if not set. See PDF Reference for further information.
   OCG:              Pointer;           // Pointer to Optional Content Group if any.
   Metadata:         PAnsiChar;         // Optional XML Metadata stream.
   MetadataSize:     Cardinal;          // Length of Metadata in bytes.
   ObjectPtr:        Pointer;           // Internal pointer to the image class.
   ResolutionX:      Single;            // Image resolution on the x-axis.
   ResolutionY:      Single;            // Image resolution on the y-axis.
   // Reserved fields for future extensions
   Reserved1:        Cardinal;
   Reserved2:        Cardinal;
   Reserved3:        Cardinal;
   Reserved4:        Cardinal;
   Reserved5:        Cardinal;
   Reserved6:        Cardinal;
   Reserved7:        Cardinal;
   Reserved8:        Cardinal;
   Reserved9:        Cardinal;
   Reserved10:       Cardinal;
   Reserved11:       Cardinal;
   Reserved12:       Cardinal;
   Reserved13:       Cardinal;
   Reserved14:       Cardinal;
   Reserved15:       Cardinal;
   Reserved16:       Cardinal;
   Reserved17:       Cardinal;
   Reserved18:       Cardinal;
end;

type TPDFMeasure = record
   StructSize:    Cardinal;    // Must be set to sizeof(TPDFMeasure)
   IsRectilinear: LongBool; // If true, the members of the rectilinear measure dictionary are set. The geospatial members otherwise.
   // --- Rectilinear measure dictionary ---
   Angles:        PINFM;      // Number format array to measure angles -> GetNumberFormatObj()
   AnglesCount:   Cardinal;   // Number of objects in the array.
   Area:          PINFM;      // Number format array to measure areas -> GetNumberFormatObj()
   AreaCount:     Cardinal;   // Number of objects in the array.
   CXY:           Single;     // Optional, meaningful only when Y is present.
   Distance:      PINFM;      // Number format array to measure distances -> GetNumberFormatObj()
   DistanceCount: Cardinal;   // Number of objects in the array.
   OriginX:       Single;     // Origin of the measurement coordinate system.
   OriginY:       Single;     // Origin of the measurement coordinate system.
   RA:            PAnsiChar;  // A text string expressing the scale ratio of the drawing.
   RW:            PWideChar;  // A text string expressing the scale ratio of the drawing.
   Slope:         PINFM;      // Number format array for measurement of the slope of a line -> GetNumberFormatObj()
   SlopeCount:    Cardinal;   // Number of objects in the array.
   X:             PINFM;      // Number format array for measurement of change along the x-axis and, if Y is not present, along the y-axis as well.
   XCount:        Cardinal;   // Number of objects in the array.
   Y:             PINFM;      // Number format array for measurement of change along the y-axis.
   YCount:        Cardinal;   // Number of objects in the array.

   // --- Geospatial measure dictionary ---
   Bounds:        PSingle;    // Array of numbers taken pairwise to describe the bounds for which geospatial transforms are valid.
   BoundCount:    Cardinal;   // Number of values in the array. Should be a multiple of two.

   // The DCS coordinate system is optional.
   DCS_IsSet:     LongBool;   // If true, the DCS members are set.
   DCS_Projected: LongBool;   // If true, the DCS values contains a pojected coordinate system.
   DCS_EPSG:      Integer;    // Optional, either EPSG or WKT is set.
   DCS_WKT:       PAnsiChar;  // Optional ASCII string

   // The GCS coordinate system is required and should be present.
   GCS_Projected: LongBool;   // If true, the GCS values contains a pojected coordinate system.
   GCS_EPSG:      Integer;    // Optional, either EPSG or WKT is set.
   GCS_WKT:       PAnsiChar;  // Optional ASCII string

   GPTS:          PSingle;    // Required, an array of numbers that shall be taken pairwise, defining points in geographic space as degrees of latitude and longitude, respectively.
   GPTSCount:     Cardinal;   // Number of values in the array.
   LPTS:          PSingle;    // Optional, an array of numbers that shall be taken pairwise to define points in a 2D unit square.
   LPTSCount:     Cardinal;   // Number of values in the array.

   PDU1:          PAnsiChar;  // Optional preferred linear display units.
   PDU2:          PAnsiChar;  // Optional preferred area display units.
   PDU3:          PAnsiChar;  // Optional preferred angular display units.
end;

type TPDFNamedDest = record
   StructSize:  Cardinal;  // Must be initialized to sizeof(TPDFNamedDestination)
   NameA:       PAnsiChar;
   NameW:       PWideChar;
   NameLen:     Cardinal;
   DestFileA:   PAnsiChar;
   DestFileW:   PWideChar;
   DestFileLen: Cardinal;
   SDestPage:   Integer;
   DestPos:     TPDFRect;
   DestType:    TDestType;
end;

type TPDFNumberFormat = record
   StructSize: Cardinal;
   C:          Single;
   D:          Integer;
   F:          TMeasureNumFormat;
   FD:         LongBool;
   O:          TMeasureLblPos;
   PSA:        PAnsiChar;
   PSW:        PWideChar;
   RDA:        PAnsiChar;
   RDW:        PWideChar;
   RTA:        PAnsiChar;
   RTW:        PWideChar;
   SSA:        PAnsiChar;
   SSW:        PWideChar;
   UA:         PAnsiChar;
   UW:         PWideChar;
end;

type TPDFOCG = record
   StructSize:    Cardinal;          // Must be set to sizeof(TPDFOCG)
   Handle:        Cardinal;          // Handle or array index
   Intent:        TOCGIntent;        // Bitmask -> Contains also the visivility state
   NameA:         PAnsiChar;         // Layer name
   NameW:         PWideChar;         // Layer name
   HaveContUsage: LongBool;          // If true, the layer contains a Content Usage dictionary. -> GetOCGContUsage().
   // The following two members can only be set if HaveContUsage is true.
   AppEvents:     TOCAppEvent;       // Bitmask -> If non-zero, the layer is included in one or more app events which control the layer state.
   Categories:    TOCGUsageCategory; // Bitmask -> The Usage Categories which control the layer state.
end;

type TPDFOCGContUsage = record
   StructSize:     Cardinal;       // Must be set to sizeof(TPDFOCGContUsage)
   ExportState:    Cardinal;       // 0 = Off, 1 = On, PDF_MAX_INT = not set.
   InfoCreatorA:   PAnsiChar;      // CreatorInfo -> The application that created the group
   InfoCreatorW:   PWideChar;      // CreatorInfo -> The application that created the group
   InfoSubtype:    PAnsiChar;      // CreatorInfo -> A name defining the type of content, e.g. Artwork, Technical etc.
   LanguageA:      PAnsiChar;      // A language code as described at SetLanguage() in the help file.
   LanguageW:      PWideChar;      // A language code as described at SetLanguage() in the help file.
   LangPreferred:  Cardinal;       // 0 = Off, 1 = On, PDF_MAX_INT = not set. The preffered state if there is a partial but no exact match of the language identifier.
   PageElement:    TOCPageElement; // If the group contains a pagination artefact.
   PrintState:     Cardinal;       // 0 = Off, 1 = On, PDF_MAX_INT = not set.
   PrintSubtype:   PAnsiChar;      // The type of content that is controlled by the OCG, e.g. Trapping, PrintersMarks or Watermark.
   UserNamesCount: Cardinal;       // The user names (if any) can be accessed with GetOCGUsageUserName().
   UserType:       TOCUserType;    // The user for whom this optional content group is primarily intendet.
   ViewState:      Cardinal;       // 0 = Off, 1 = On, PDF_MAX_INT = not set.
   ZoomMin:        Single;         // The minimum magnification factor at which the group should be On. -1 if not set.
   ZoomMax:        Single;         // The maximum magnification factor at which the group should be On. -1 if not set.
end;

type TPDFOutputIntent = record
   StructSize:         Cardinal;
   Buffer:             PAnsiChar;
   BufSize:            Cardinal;
   InfoA:              PAnsiChar;
   InfoW:              PWideChar;
   NumComponents:      Cardinal;
   OutputConditionA:   PAnsiChar;
   OutputConditionW:   PWideChar;
   OutputConditionIDA: PAnsiChar;
   OutputConditionIDW: PWideChar;
   RegistryNameA:      PAnsiChar;
   RegistryNameW:      PWideChar;
   SubType:            PAnsiChar;
end;

type TPDFPageLabel = record
   StartRange: Cardinal;           // Number of the first page in the range. If no further label follows, the last
                                   // page in the range is pdfGetPageCount(). The first page is denoted by 1.
   Format:       TPageLabelFormat; // Number format to be used.
   FirstPageNum: Integer;          // First page number to be displayed in the page label. Subsequent pages are
                                   // numbered sequentially from this value.
   Prefix:       PAnsiChar;        // Optional prefix
   PrefixLen:    Cardinal;         // Length of the prefix in characters
   PrefixUni:    LongBool;         // If true, the prefix is a Unicode string
end;

type TPDFRawImage = record
   StructSize:       Cardinal;       // Must be set to sizeof(TPDFRawImage)
   Buffer:           Pointer;        // Image buffer
   BufSize:          Cardinal;       // Buffer size
   BitsPerComponent: Cardinal;       // Bits per component
   NumComponents:    Cardinal;       // Number of components (max 32)
   CS:               TExtColorSpace; // Image color space
   CSHandle:         Integer;        // Color space handle (non-device color spaces only)
   Stride:           Integer;        // Scanline length in bytes -> If negative, the image is defined in bottom up coordinates, top down otherwise
   HasAlpha:         LongBool;       // If true, the last component is an alpha channel
   IsBGR:            LongBool;       // esDeviceRGB only -> If true, the image components are defined in BGR format instead of RGB
   MinIsWhite:       LongBool;       // 1 bit images only -> If true, zero pixel values must be treated as white instead of black
   Width:            Cardinal;       // Width in pixels (must be greater zero)
   Height:           Cardinal;       // Height in pixels (must be greater zero)
end;

type TPDFSigDict = record
   StructSize:     Cardinal;  // Must be set to sizeof(TPDFSigDict).
   ByteRange:      PCardinal; // ByteRange -> Byte offset followed by the corresponding length.
                              // The byte ranges are required to create the digest. The values
                              // are returned as is. So, you must check whether the offsets and
                              // length values are valid. There are normally at least two ranges.
                              // Overlapping ranges are not allowed! Any error breaks processing
                              // and the signature should be considered as invalid.
   ByteRangeCount: Cardinal;  // The number of Offset / Length pairs. ByteRange contains 2 * ByteRangeCount values!
   Cert:           PAnsiChar; // X.509 Certificate when SubFilter is adbe.x509.rsa_sha1.
   CertLen:        Cardinal;  // Length in bytes
   Changes:        PCardinal; // If set, an array of three integers that specify changes to the
                              // document that have been made between the previous signature and
                              // this signature in this order: the number of pages altered, the
                              // number of fields altered, and the number of fields filled in.
   ContactInfoA:   PAnsiChar; // Optional contact info string, e.g. an email address
   ContactInfoW:   PWideChar; // Optional contact info string, e.g. an email address
   Contents:       PAnsiChar; // The signature. This is either a DER encoded PKCS#1 binary data
                              // object or a DER-encoded PKCS#7 binary data object depending on
                              // the used SubFilter.
   ContentsSize:   Cardinal;  // Length in bytes.
   Filter:         PAnsiChar; // The name of the security handler, usually Adobe.PPKLite.
   LocationA:      PAnsiChar; // Optional location of the signer
   LocationW:      PWideChar; // Optional location of the signer
   SignTime:       PAnsiChar; // Date/Time string
   NameA:          PAnsiChar; // Optional signers name
   NameW:          PWideChar; // Optional signers name
   PropAuthTime:   Cardinal;  // Optional -> The number of seconds since the signer was last authenticated.
   PropAuthType:   PAnsiChar; // Optional -> The method that shall be used to authenticate the signer.
                              // Valid values are PIN, Password, and Fingerprint.
   ReasonA:        PAnsiChar; // Optional reason
   ReasonW:        PWideChar; // Optional reason
   Revision:       Integer;   // Optional -> The version of the signature handler that was used to create
                              // the signature.
   SubFilter:      PAnsiChar; // A name that describes the encoding of the signature value. Should be
                              // adbe.x509.rsa_sha1, adbe.pkcs7.detached, or adbe.pkcs7.sha1.
   Version:        Integer;   // The version of the signature dictionary format.
end;

type TPDFSigParms = record
   StructSize:   Cardinal;  // Must be set to sizeof(TSignParms)
   PKCS7ObjLen:  Cardinal;  // The maximum length of the signed PKCS#7 object
   HashType:     THashType; // If set to htDetached, the bytes ranges of the PDF file will be returned.
   Range1:       PByte;     // Out -> Contains either the hash or the first byte range to create a detached signature
   Range1Len:    Cardinal;  // Out -> Length of the buffer
   Range2:       PByte;     // Out -> Set only if HashType == htDetached
   Range2Len:    Cardinal;  // Out -> Length of the buffer
   ContactInfoA: PAnsiChar; // Optional, e.g. an email address
   ContactInfoW: PWideChar; // Optional, e.g. an email address
   LocationA:    PAnsiChar; // Optional location of the signer
   LocationW:    PWideChar; // Optional location of the signer
   ReasonA:      PAnsiChar; // Optional reason why the file was signed
   ReasonW:      PWideChar; // Optional reason why the file was signed
   SignerA:      PAnsiChar; // Optional, the issuer of the certificate takes precedence
   SignerW:      PWideChar; // Optional, the issuer of the certificate takes precedence
   Encrypt:      LongBool;  // If true, the file will be encrypted
   // These members will be ignored if Encrypt is set to false
   OpenPwd:      PAnsiChar; // Open password
   OwnerPwd:     PAnsiChar; // Owner password to change the security settings
   KeyLen:       TKeyLen;   // Key length to be used to encrypt the file
   Restrict:     TRestrictions; // What should be restricted?
end;

type TPDFStack = record
   ctm:          TCTM;               // Pre-multiplied global transformation matrix
   tm:           TCTM;               // Pre-multiplied text transformation matrix
   x:            Double;             // Unused -> always 0.0
   y:            Double;             // Unused -> always 0.0
   FontSize:     Double;             // Font size
   CharSP:       Double;             // Character spacing
   WordSP:       Double;             // Word spacing
   HScale:       Double;             // Horizontal text scaling
   TextRise:     Double;             // Text rise -> always 0.0 because it is already included in the text transformation matrix
   Leading:      Double;             // Leading
   LineWidth:    Double;             // Line width
   DrawMode:     TDrawMode;          // Text draw mode
   FillCS:       TPDFColorSpace;     // Fill color space
   StrokeCS:     TPDFColorSpace;     // Stroke color space
   FillColor:    Cardinal;           // Fill color
   StrokeColor:  Cardinal;           // Stroke color
   BaseObject:   Pointer;            // Internal
   CIDFont:      LongBool;           // If true, ReplacePageText() can only be used to delete a string
   Text:         PAnsiChar;          // Raw text without kerning space
   TextLen:      Cardinal;           // Raw text length
   RawKern:      TTextRecordAPtr;    // Raw kerning array
   Kerning:      TTextRecordWPtr;    // Already translated Unicode kerning array
   KerningCount: Cardinal;           // Number of kerning records
   TextWidth:    Single;             // The width of the entire text record measured in text space
   IFont:        Pointer;            // Font object used to print the string -> fntGetFont() can be used to return the font properties
   Embedded:     LongBool;           // If true, the font is embedded
   SpaceWidth:   Single;             // Measured in text space
   {
      These members can be modified after the structure has been initialized with InitStack().
      If the destination color space should be DeviceCMYK initialize FillColor and StrokeColor
      with PDF_CMYK(0,0,0,255); which represents black.
   }
   ConvColors:   LongBool;           // If set to true (default), all colors are converted to the specified destination color space
   DestSpace:    TPDFColorSpace;     // Destination color space -> default == csDeviceRGB

   // This member can be used in combination with ReplacePageText() to preserve a number
   // of kerning records from deletion. All records above this value will be deleted.
   // Take a look into the file examples/util/pdf_edit_text.cpp to determine how this member
   // can be used.
   DeleteKerningAt: Cardinal;
   FontFlags:       Cardinal;    // PDF font flags
   // ------------------------------- Reserved fields -------------------------------
   Reserved1:  Integer;
   Reserved2:  Integer;
   Reserved3:  Integer;
   Reserved4:  Integer;
   Reserved5:  Integer;
   Reserved6:  Integer;
   Reserved7:  Integer;
   Reserved8:  Integer;
   Reserved9:  Integer;
   Reserved10: Integer;
   Reserved11: Integer;
   ContentPtr: Pointer;
end;

type TUnicodeRange1 = Cardinal;
   const urBasicLatin                  = 1 shl 0;  // 0000-007F
   const urLatin1Supplement            = 1 shl 1;  // 0080-00FF
   const urLatinExtendedA              = 1 shl 2;  // 0100-017F
   const urLatinExtendedB              = 1 shl 3;  // 0180-024F
   const urIPAExtensions               = 1 shl 4;  // 0250-02AF, 1D00-1D7F, 1D80-1DBF
   const urSpacingModifierLetters      = 1 shl 5;  // 02B0-02FF, A700-A71F
   const urCombiningDiacriticalMarks   = 1 shl 6;  // 0300-036F, 1DC0-1DFF
   const urGreekandCoptic              = 1 shl 7;  // 0370-03FF
   const urCoptic                      = 1 shl 8;  // 2C80-2CFF
   const urCyrillic                    = 1 shl 9;  // 0400-04FF, 0500-052F, 2DE0-2DFF, A640-A69F
   const urArmenian                    = 1 shl 10; // 0530-058F
   const urHebrew                      = 1 shl 11; // 0590-05FF
   const urVai                         = 1 shl 12; // A500-A63F
   const urArabic                      = 1 shl 13; // 0600-06FF, 0750-077F
   const urNKo                         = 1 shl 14; // 07C0-07FF
   const urDevanagari                  = 1 shl 15; // 0900-097F
   const urBengali                     = 1 shl 16; // 0980-09FF
   const urGurmukhi                    = 1 shl 17; // 0A00-0A7F
   const urGujarati                    = 1 shl 18; // 0A80-0AFF
   const urOriya 	                     = 1 shl 19; // 0B00-0B7F
   const urTamil 	                     = 1 shl 20; // 0B80-0BFF
   const urTelugu                      = 1 shl 21; // 0C00-0C7F
   const urKannada                     = 1 shl 22; // 0C80-0CFF
   const urMalayalam                   = 1 shl 23; // 0D00-0D7F
   const urThai                        = 1 shl 24; // 0E00-0E7F
   const urLao                         = 1 shl 25; // 0E80-0EFF
   const urGeorgian                    = 1 shl 26; // 10A0-10FF, 2D00-2D2F
   const urBalinese                    = 1 shl 27; // 1B00-1B7F
   const urHangulJamo                  = 1 shl 28; // 1100-11FF
   const urLatinExtendedAdditional     = 1 shl 29; // 1E00-1EFF, 2C60-2C7F, A720-A7FF
   const urGreekExtended               = 1 shl 30; // 1F00-1FFF
   const urGeneralPunctuation          = 1 shl 31; // 2000-206F, 2E00-2E7F

type TUnicodeRange2 = Cardinal;
   const urSuperscriptsAndSubscripts   = 1 shl 0;  // 2070-209F
   const urCurrencySymbols             = 1 shl 1;  // 20A0-20CF
   const urCombDiacritMarksForSymbols  = 1 shl 2;  // 20D0-20FF
   const urLetterlikeSymbols           = 1 shl 3;  // 2100-214F
   const urNumberForms                 = 1 shl 4;  // 2150-218F
   const urArrows                      = 1 shl 5;  // 2190-21FF, 27F0-27FF, 2900-297F, 2B00-2BFF
   const urMathematicalOperators       = 1 shl 6;  // 2200-22FF, 2A00-2AFF, 27C0-27EF, 2980-29FF
   const urMiscellaneousTechnical      = 1 shl 7;  // 2300-23FF
   const urControlPictures             = 1 shl 8;  // 2400-243F
   const urOpticalCharacterRecognition = 1 shl 9;  // 2440-245F
   const urEnclosedAlphanumerics       = 1 shl 10; // 2460-24FF
   const urBoxDrawing                  = 1 shl 11; // 2500-257F
   const urBlockElements               = 1 shl 12; // 2580-259F
   const urGeometricShapes             = 1 shl 13; // 25A0-25FF
   const urMiscellaneousSymbols        = 1 shl 14; // 2600-26FF
   const urDingbats                    = 1 shl 15; // 2700-27BF
   const urCJKSymbolsAndPunctuation    = 1 shl 16; // 3000-303F
   const urHiragana                    = 1 shl 17; // 3040-309F
   const urKatakana                    = 1 shl 18; // 30A0-30FF, 31F0-31FF
   const urBopomofo                    = 1 shl 19; // 3100-312F, 31A0-31BF
   const urHangulCompatibilityJamo     = 1 shl 20; // 3130-318F
   const urPhagsPa                     = 1 shl 21; // A840-A87F
   const urEnclosedCJKLettersAndMonths = 1 shl 22; // 3200-32FF
   const urCJKCompatibility            = 1 shl 23; // 3300-33FF
   const urHangulSyllables             = 1 shl 24; // AC00-D7AF
   const urNonPlane0                   = 1 shl 25; // D800-DFFF
   const urPhoenician                  = 1 shl 26; // 10900-1091F
   const urCJKUnifiedIdeographs        = 1 shl 27; // 4E00-9FFF, 2E80-2EFF, 2F00-2FDF, 2FF0-2FFF, 3400-4DBF, 20000-2A6DF, 3190-319F
   const urPrivateUseAreaPlane0        = 1 shl 28; // E000-F8FF
   const urCJKStrokes                  = 1 shl 29; // 31C0-31EF, F900-FAFF, 2F800-2FA1F
   const urAlphabeticPresentationForms = 1 shl 30; // FB00-FB4F
   const urArabicPresentationFormsA    = 1 shl 31; // FB50-FDFF

type TUnicodeRange3 = Cardinal;
   const urCombiningHalfMarks          = 1 shl 0;  // FE20-FE2F
   const urVerticalForms               = 1 shl 1;  // FE10-FE1F, FE30-FE4F
   const urSmallFormVariants           = 1 shl 2;  // FE50-FE6F
   const urArabicPresentationFormsB    = 1 shl 3;  // FE70-FEFF
   const urHalfwidthAndFullwidthForms  = 1 shl 4;  // FF00-FFEF
   const urSpecials                    = 1 shl 5;  // FFF0-FFFF
   const urTibetan                     = 1 shl 6;  // 0F00-0FFF
   const urSyriac                      = 1 shl 7;  // 0700-074F
   const urThaana                      = 1 shl 8;  // 0780-07BF
   const urSinhala                     = 1 shl 9;  // 0D80-0DFF
   const urMyanmar                     = 1 shl 10; // 1000-109F
   const urEthiopic                    = 1 shl 11; // 1200-137F, 1380-139F, 2D80-2DDF
   const urCherokee                    = 1 shl 12; // 13A0-13FF
   const urUnifiedCanadianAboriginal   = 1 shl 13; // 1400-167F
   const urOgham                       = 1 shl 14; // 1680-169F
   const urRunic                       = 1 shl 15; // 16A0-16FF
   const urKhmer                       = 1 shl 16; // 1780-17FF, 19E0-19FF
   const urMongolian                   = 1 shl 17; // 1800-18AF
   const urBraillePatterns             = 1 shl 18; // 2800-28FF
   const urYiSyllables                 = 1 shl 19; // A000-A48F, A490-A4CF
   const urTagalog                     = 1 shl 20; // 1700-171F, 1720-173F, 1740-175F, 1760-177F
   const urOldItalic                   = 1 shl 21; // 10300-1032F
   const urGothic                      = 1 shl 22; // 10330-1034F
   const urDeseret                     = 1 shl 23; // 10400-1044F
   const urMusicalSymbols              = 1 shl 24; // 1D000-1D0FF, 1D100-1D1FF, 1D200-1D24F
   const urMathematicalAlphanumeric    = 1 shl 25; // 1D400-1D7FF
   const urPrivateUsePlane15           = 1 shl 26; // FF000-FFFFD, 100000-10FFFD
   const urVariationSelectors          = 1 shl 27; // FE00-FE0F, E0100-E01EF
   const urTags                        = 1 shl 28; // E0000-E007F
   const urLimbu                       = 1 shl 29; // 1900-194F
   const urTaiLe                       = 1 shl 30; // 1950-197F
   const urNewTaiLue                   = 1 shl 31; // 1980-19DF

type TUnicodeRange4 = Cardinal;
   const urBuginese                    = 1 shl 0;  // 1A00-1A1F
   const urGlagolitic                  = 1 shl 1;  // 2C00-2C5F
   const urTifinagh                    = 1 shl 2;  // 2D30-2D7F
   const urYijingHexagramSymbols       = 1 shl 3;  // 4DC0-4DFF
   const urSylotiNagri                 = 1 shl 4;  // A800-A82F
   const urLinearBSyllabary            = 1 shl 5;  // 10000-1007F, 10080-100FF, 10100-1013F
   const urAncientGreekNumbers         = 1 shl 6;  // 10140-1018F
   const urUgaritic                    = 1 shl 7;  // 10380-1039F
   const urOldPersian                  = 1 shl 8;  // 103A0-103DF
   const urShavian                     = 1 shl 9;  // 10450-1047F
   const urOsmanya                     = 1 shl 10; // 10480-104AF
   const urCypriotSyllabary            = 1 shl 11; // 10800-1083F
   const urKharoshthi                  = 1 shl 12; // 10A00-10A5F
   const urTaiXuanJingSymbols          = 1 shl 13; // 1D300-1D35F
   const urCuneiform                   = 1 shl 14; // 12000-123FF, 12400-1247F
   const urCountingRodNumerals         = 1 shl 15; // 1D360-1D37F
   const urSundanese                   = 1 shl 16; // 1B80-1BBF
   const urLepcha                      = 1 shl 17; // 1C00-1C4F
   const urOlChiki                     = 1 shl 18; // 1C50-1C7F
   const urSaurashtra                  = 1 shl 19; // A880-A8DF
   const urKayahLi                     = 1 shl 20; // A900-A92F
   const urRejang                      = 1 shl 21; // A930-A95F
   const urCham                        = 1 shl 22; // AA00-AA5F
   const urAncientSymbols              = 1 shl 23; // 10190-101CF
   const urPhaistosDisc                = 1 shl 24; // 101D0-101FF
   const urCarian                      = 1 shl 25; // 102A0-102DF, 10280-1029F, 10920-1093F
   const urDominoTiles                 = 1 shl 26; // 1F030-1F09F, 1F000-1F02F

type TPDFSysFont = record
   StructSize:      Cardinal;           // Must be set to sizeof(TPDFSysFont)
   BaseType:        TFontBaseType;      // Font type
   CIDOrdering:     PAnsiChar;          // OpenType CID fonts only
   CIDRegistry:     PAnsiChar;          // OpenType CID fonts only
   CIDSupplement:   Cardinal;           // OpenType CID fonts only
   DataOffset:      Cardinal;           // Data offset
   FamilyName:      PWideChar;          // Family name
   FilePathA:       PAnsiChar;          // Font file path
   FilePathW:       PWideChar;          // Font file path
   FileSize:        Cardinal;           // File size in bytes
   Flags:           TEnumFontProcFlags; // Bitmask
   FullName:        PWideChar;          // Full name
   Length1:         Cardinal;           // Length of the clear text portion of a Type1 font
   Length2:         Cardinal;           // Length of the eexec encrypted binary portion of a Type1 font
   PostScriptNameA: PAnsiChar;          // Postscript mame
   PostScriptNameW: PWideChar;          // Postscript mame
   Index:           Integer;            // Zero based font index if the font is stored in a TrueType collection
   IsFixedPitch:    LongBool;           // If true, the font is a fixed pitch font. A proprtional font otherwise.
   Style:           TFStyle;            // Font style
   UnicodeRange1:   TUnicodeRange1;     // Bitmask
   UnicodeRange2:   TUnicodeRange2;     // Bitmask
   UnicodeRange3:   TUnicodeRange3;     // Bitmask
   UnicodeRange4:   TUnicodeRange4;     // Bitmask
end;

type TPDFVersion =
(
	pvPDF_1_0,
	pvPDF_1_1,
	pvPDF_1_2,
	pvPDF_1_3,
	pvPDF_1_4,
	pvPDF_1_5,
	pvPDF_1_6,
	pvPDF_1_7,
	pvPDF_2_0,     // PDF 2.0
	pvReserved,    // Reserved for future use
	pvPDFX1a_2001, // PDF/X-1a:2001
	pvPDFX1a_2003, // PDF/X-1a:2002
	pvPDFX3_2002,  // PDF/X-3:2002
	pvPDFX3_2003,  // PDF/X-3:2003
	pvPDFA_2005,   // PDF/A-1b 2005
	pvPDFX_4,      // PDF/X-4
   pvPDFA_1a,     // PDF/A 1a 2005
   pvPDFA_2a,     // PDF/A 2a
   pvPDFA_2b,     // PDF/A 2b
   pvPDFA_2u,     // PDF/A 2u
   pvPDFA_3a,     // PDF/A 3a
   pvPDFA_3b,     // PDF/A 3b
   pvPDFA_3u      // PDF/A 3u
);

type TRectL = record
   Left: Integer;
   Top: Integer;
   Right: Integer;
   Bottom: Integer;
end;

type TShadingType =
(
   stUnknown, // cannot occur -> internal use
   stFunctionBased,
   stAxial,
   stRadial,
   stFreeFormGouraud,
   stLatticeFormGouraud,
   stCoonsPatch,
   stTensorProduct
);

type TSpoolConvFlags = Integer;
const
   spcDefault            = 0;
   spcIgnorePaperFormat  = 1; // If set, the current page format is used as is for the entire spool file.
   spcDontAddMargins     = 2; // If set, the page format is calculated from the EMF files as is. The current page format is not used to calculate
                              // margins which are maybe required. Note that the parameters LeftMargin and TopMargin will still be considered.
   spcLoadSpoolFontsOnly = 4; // If set, only embedded fonts will be loaded. The EMF files must be converted with the flag mfIgnoreEmbFonts in this
                              // case. This flag can be useful if you want to use your own code to convert the EMF files of the spool file.
   spcFlushPages         = 8; // If set, the function writes every finish page directly to the output file to reduce the memory usage. This flag
                              // is meaningful only if the PDF file is not created in memory. Note also that it is not possible to access already
                              // flushed pages again with EditPage().

type TStdPattern =
(
   spHorizontal, { ----- }
   spVertical,   { ||||| }
   spRDiagonal,  { \\\\\ }
   spLDiagonal,  { ///// }
   spCross,      { +++++ }
   spDiaCross    { xxxxx }
);

type TSubmitFlags = Integer;
   const sfNone             = $00000000;
   const sfExlude           = $00000001; // if set, the fields in a sumbmit or reset form action are excluded
   const sfInclNoValFields  = $00000002;
   const sfHTML             = $00000004;
   const sfGetMethod        = $00000008;
   const sfSubmCoords       = $00000010;
   const sfXML              = $00000024;
   const sfInclAppSaves     = $00000040;
   const sfInclAnnots       = $00000080;
   const sfPDF              = $00000100;
   const sfCanonicalFormat  = $00000200;
   const sfExlNonUserAnnots = $00000400;
   const sfExlFKey          = $00000800;
   const sfEmbedForm        = $00002000; // PDF 1.5 embed the entire form into a file stream inside the FDF file -> requires the full version of Adobe's Acrobat

type TPDFViewport = record
   StructSize: Cardinal;  // Must be set to sizeof(TPDFViewport)
   BBox:       TFltRect;  // Bounding box
   Measure:    IMSR;      // Optional -> GetMeasureObj()
   NameA:      PAnsiChar; // Optional name
   NameW:      PWideChar; // Optional name
   PtData:     IPTD;      // Pointer of a Point Data dictionary. The value can be accessed with GetPtDataObj().
end;
   
type TViewerPreference = Integer;
const
  vpUseNone             = $00000000;
  vpHideToolBar         = $00000001;
  vpHideMenuBar         = $00000002;
  vpHideWindowUI        = $00000004;
  vpFitWindow           = $00000008;
  vpCenterWindow        = $00000010;
  vpDisplayDocTitle     = $00000020;
  vpNonFullScrPageMode  = $00000040;
  vpDirection           = $00000080;
  vpViewArea            = $00000100;
  vpViewClip            = $00000200;
  vpPrintArea           = $00000400;
  vpPrintClip           = $00000800;

type TViewPrefAddVal = Integer;
const
  avNone                   = $00000000;
  avNonFullScrUseNone      = $00000001;
  avNonFullScrUseOutlines  = $00000002;
  avNonFullScrUseThumbs    = $00000004;
  avNonFullScrUseOC        = $00000400; // PDF 1.6
  avDirectionL2R           = $00000008;
  avDirectionR2L           = $00000010;
  avViewPrintArtBox        = $00000020;
  avViewPrintBleedBox      = $00000040;
  avViewPrintCropBox       = $00000080;
  avViewPrintMediaBox      = $00000100;
  avViewPrintTrimBox       = $00000200;

  // Use these masks to determine which value is defined.
  AV_NON_FULL_SRC_MASK     = $00000005;
  AV_DIRECTION_MASK        = $00000018;
  AV_VIEW_PRINT_MASK       = $000003E0;

{ --------------------------------------------- Callback functions ---------------------------------------------- }

// Data is always a user defined pointer which is passed unchanged to the callback funtion
type TErrorProc      = function(const Data: Pointer; ErrCode: Integer; const ErrMessage: PAnsiChar; ErrType: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TEnumFontProc   = function(const Data: Pointer; const FamilyName: PWideChar; const PostScriptName: PAnsiChar; Style: TFStyle): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TEnumFontProc2  = function(const Data, PDFFont: Pointer; FontType: TFontType; const BaseFont, FontName: PAnsiChar; Embedded, IsFormFont: LongBool; Flags: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TEnumFontProcEx = function(const Data: Pointer; const FamilyName: PWideChar; const PostScriptName: PAnsiChar; Style: TFStyle; BaseType: TFontBaseType; Flags: TEnumFontProcFlags; const FilePath: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};

{ ---------------------------------- Callback functions for CheckConformance() ---------------------------------- }

type TOnFontNotFoundProc  = function(const Data, PDFFont: Pointer; const FontName: PAnsiChar; Style: TFStyle; StdFontIndex: Integer; IsSymbolFont: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TOnReplaceICCProfile = function(const Data: Pointer; ProfileType: TICCProfileType; ColorSpace: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};

{ ------------------------------------- Callback function for WriteFText ---------------------------------------- }

// The TOnPageBreakProc function is called by WriteFText when the rectangle was filled with text, or when a
// manual page break was inserted to the text and more text is available for output.
// You must set the text rectangle with SetTextRect to continue the text on a new position or simply call
// EndPage() and then Append() or EditPage() to continue the text on an arbitrary new page. You can make some
// changes inside the callback function such as setting another font, font color or other thinks.

// When the callback function returns a value greater or equal zero, processing continues on the new
// position or page that was set inside the callback function.

// The text alignment can also be changed with the following return values:

const NEW_ALIGN_LEFT    = 1;
const NEW_ALIGN_RIGHT   = 2;
const NEW_ALIGN_CENTER  = 3;
const NEW_ALIGN_JUSTIFY = 4;

type TOnPageBreakProc = function(const Data: Pointer; LastPosX, LastPosY: Double; PageBreak: LongBool): Integer; cdecl;

// When the return value of the callback function is one of the values listed above, then the alignment will
// be changed before the remaining text is processed. A return value of 0 determines that processing should
// be continued without changing the alignment.

// A negative return value breaks processing and WriteFText returns emmidiatly with a warning. Note that
// processing stops entirely when warnings should be handled as fatal errors (see SetErrorMode() in the help
// file for further information).

// NOTE: The coordinate system is always set to bottom up when the callback function is called, independent
// of the coordinate system that was set before WriteFText was called. WriteFText restores the coordinate system
// and all other temporarily changed variables when the function is leaved. The last text text position is
// also returned in bottom up coordinates. LastPosY is the position of the last baseline.

// You can change the coordinate system inside the callback function when needed, DynaPDF checks the state
// before processing continues.

{ ------------------------------- Callback functions to control a progress bar ---------------------------------- }

type TProgType = Integer;
const ptImportPage = 0;
const ptWritePage  = 1;

// TInitProgress is called before TProgress is called the first time
type TInitProgressProc = function(const Data: Pointer; ProgType: TProgType; MaxCount: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TProgressProc     = function(const Data: Pointer; ActivePage: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};

{------------------------------------------- Parser Interface ---------------------------------------------------}

type TApplyPattern        = function(const Data, PDFObject: Pointer; PatternType: TPatternType; const Pattern: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TBeginLayer          = function(const Data, IOCG: Pointer; InVisible: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TBeginPattern        = function(const Data: Pointer; Fill: LongBool; Handle: Integer; PatternType: TPatternType; var BBox: TPDFRect; Matrix: PCTM; XStep, YStep: PDouble): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TBeginTemplate       = function(const Data, PDFObject: Pointer; Handle: Integer; var BBox: TPDFRect; Matrix: PCTM): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TBezierTo1           = function(const Data, PDFObject: Pointer; x1, y1, x3, y3: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TBezierTo2           = function(const Data, PDFObject: Pointer; x2, y2, x3, y3: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TBezierTo3           = function(const Data, PDFObject: Pointer; x1, y1, x2, y2, x3, y3: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TClipPath            = function(const Data, PDFObject: Pointer; EvenOdd: LongBool; Mode: TPathFillMode): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TClosePath           = function(const Data, PDFObject: Pointer; Mode: TPathFillMode): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TDrawShading         = function(const Data, PDFObject: Pointer; ShadType: TShadingType; const Shading: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TEndLayer            = procedure(const Data, IOCG: Pointer; InVisible: LongBool); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TEndPattern          = procedure(const Data: Pointer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TEndTemplate         = procedure(const Data: Pointer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TInsertImage         = function(const Data: Pointer; var Image: TPDFImage): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TLineTo              = function(const Data, PDFObject: Pointer; x, y: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TMoveTo              = function(const Data, PDFObject: Pointer; x, y: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TMulMatrix           = procedure(const Data, PDFObject: Pointer; var Matrix: TCTM); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TRectangle           = function(const Data, PDFObject: Pointer; x, y, w, h: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TRestoreGraphicState = function(const Data: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSaveGraphicState    = function(const Data: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetCharSpacing      = procedure(const Data, PDFObject: Pointer; Value: Double); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetExtGState        = procedure(const Data, PDFObject: Pointer; var GS: TPDFExtGState2); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetFillColor        = procedure(const Data, PDFObject: Pointer; Color: PDouble; NumComps: Cardinal; CS: TExtColorSpace; const IColorSpace: Pointer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetFont             = procedure(const Data, PDFObject: Pointer; FontType: TFontType; Embedded: LongBool; const FontName: PAnsiChar; Style: TFStyle; FontSize: Double; const Font: Pointer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetLeading          = procedure(const Data, PDFObject: Pointer; Value: Double); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetLineCapStyle     = procedure(const Data, PDFObject: Pointer; Style: TLineCapStyle); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetLineDashPattern  = procedure(const Data, PDFObject: Pointer; Dash: PDouble; NumValues, Phase: Cardinal); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetLineJoinStyle    = procedure(const Data, PDFObject: Pointer; Style: TLineJoinStyle); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetLineWidth        = procedure(const Data, PDFObject: Pointer; Value: Double); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetMiterLimit       = procedure(const Data, PDFObject: Pointer; Value: Double); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetStrokeColor      = procedure(const Data, PDFObject: Pointer; Color: PDouble; NumComps: Cardinal; CS: TExtColorSpace; const IColorSpace: Pointer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetTextDrawMode     = procedure(const Data, PDFObject: Pointer; Mode: TDrawMode); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetTextScale        = procedure(const Data, PDFObject: Pointer; Value: Double); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TSetWordSpacing      = procedure(const Data, PDFObject: Pointer; Value: Double); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TShowTextArrayA      = function(const Data: Pointer; Obj: PAnsiChar; var Matrix: TCTM; const Source: TTextRecordAPtr; Count: Cardinal; Width: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
type TShowTextArrayW      = function(const Data: Pointer; const Source: TTextRecordAPtr; var Matrix: TCTM; const Kerning: TTextRecordWPtr; Count: Cardinal; Width: Double; Decoded: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};

// All callback functions are optional. Set unnecessary callback functions to nil.
type TPDFParseInterface = record
   ApplyPattern:        TApplyPattern;
   BeginPattern:        TBeginPattern;
   BeginTemplate:       TBeginTemplate;
   BezierTo1:           TBezierTo1;
   BezierTo2:           TBezierTo2;
   BezierTo3:           TBezierTo3;
   ClipPath:            TClipPath;
   ClosePath:           TClosePath;
   DrawShading:         TDrawShading;
   EndPattern:          TEndPattern;
   EndTemplate:         TEndTemplate;
   LineTo:              TLineTo;
   MoveTo:              TMoveTo;
   MulMatrix:           TMulMatrix;
   Rectangle:           TRectangle;
   RestoreGraphicState: TRestoreGraphicState;
   SaveGraphicState:    TSaveGraphicState;
   SetCharSpacing:      TSetCharSpacing;
   SetExtGState:        TSetExtGState;
   SetFillColor:        TSetFillColor;
   SetFont:             TSetFont;
   SetLeading:          TSetLeading;
   SetLineCapStyle:     TSetLineCapStyle;
   SetLineDashPattern:  TSetLineDashPattern;
   SetLineJoinStyle:    TSetLineJoinStyle;
   SetLineWidth:        TSetLineWidth;
   SetMiterLimit:       TSetMiterLimit;
   SetStrokeColor:      TSetStrokeColor;
   SetTextDrawMode:     TSetTextDrawMode;
   SetTextScale:        TSetTextScale;
   SetWordSpacing:      TSetWordSpacing;
   Reserved001:         Pointer; // The old callabck functions TShowTextA and TShowTextW are no longer defined
   Reserved002:         Pointer; // The old callabck functions TShowTextA and TShowTextW are no longer defined
   ShowTextArrayW:      TShowTextArrayW;
   InsertImage:         TInsertImage;
   ShowTextArrayA:      TShowTextArrayA;
   BeginLayer:          TBeginLayer;
   EndLayer:            TEndLayer;
   { These variables must be initialized with nil! }
   Reserved01: Pointer;
   Reserved02: Pointer;
   Reserved03: Pointer;
   Reserved04: Pointer;
   Reserved05: Pointer;
   Reserved06: Pointer;
   Reserved07: Pointer;
   Reserved08: Pointer;
   Reserved09: Pointer;
   Reserved10: Pointer;
   Reserved11: Pointer;
   Reserved12: Pointer;
   Reserved13: Pointer;
   Reserved14: Pointer;
   Reserved15: Pointer;
   Reserved16: Pointer;
   Reserved17: Pointer;
   Reserved18: Pointer;
   Reserved19: Pointer;
   Reserved20: Pointer;
   Reserved21: Pointer;
   Reserved22: Pointer;
   Reserved23: Pointer;
   Reserved24: Pointer;
   Reserved25: Pointer;
end;

{ ----------------------------------- Rendering API ----------------------------------- }

type TPDFPixFormat =
(
   pxf1Bit,
   pxfGray,
   pxfRGB,
   pxfBGR,
   pxfRGBA,
   pxfBGRA,
   pxfARGB,
   pxfABGR,
   pxfCMYK,
   pxfCMYKA
);

type TPDFPageScale =
(
   psFitWidth,  // Scale the page to the width of the image buffer
   psFitHeight, // Scale the page to the height of the image buffer
   psFitBest,   // Scale the page so that it fits fully into the image buffer
   psFitZoom    // This mode should be used if the scaling factors of the transformation matrix are <> 1.0
);

type TRasterFlags = Cardinal;
const
   rfDefault               = $00000000; // Render the page as usual
   rfScaleToMediaBox       = $00000001; // Render the real paper format. Contents outside the crop box is clipped
   rfIgnoreCropBox         = $00000002; // Ignore the crop box and render anything inside the media box without clipping
   // Only one of these flags must be set at time!
   rfClipToArtBox          = $00000004; // Clip the page to the art box if any
   rfClipToBleedBox        = $00000008; // Clip the page to the bleed box if any
   rfClipToTrimBox         = $00000010; // Clip the page to the trim box if any
   rfExclAnnotations       = $00000020; // Don't render annotations
   rfExclFormFields        = $00000040; // Don't render form fields
   rfRotate90              = $00000100; // Rotate the page 90 degress
   rfRotate180             = $00000200; // Rotate the page 180 degress
   rfRotate270             = $00000400; // Rotate the page 270 degress
   rfInitBlack             = $00000800; // Initialize the image buffer to black before rendering (RGBA or GrayA must be initialized to black)
   rfCompositeWhite        = $00001000; // Composite pixel formats with an alpha channel finally with a white background. The alpha channel is
                                        // 255 everywhere after composition. This flag is mainly provided for debug purposes but it can also be
                                        // useful if the image must be copied on screen with a function that doesn't support alpha blending.
   rfExclPageContent       = $00002000; // If set; only annotations and form fields will be rendered (if any).

   // If you want to render specific field types with RenderAnnotOrField() then use the following flags to exclude these fields.
   // If all fields should be skipped then set the flag rfExclFormFields instead.
   rfExclButtons           = $00004000;
   rfExclCheckBoxes        = $00008000;
   rfExclComboBoxes        = $00010000;
   rfExclListBoxes         = $00020000;
   rfExclTextFields        = $00040000;
   rfExclSigFields         = $00080000;

   // ---------------------------------
   rfScaleToBBox           = $00100000; // Considered only, if the flag rfClipToArtBox, rfClipToBleedBox, or rfClipToTrimBox is set.
                                        // If set, the picture size is set to the size of the whished bounding box.
   rfDisableAAClipping     = $00200000; // Disable Anti-Aliasing for clipping paths. This flag is the most important one since clipping paths
                                        // cause often visible artefacts in PDF files with flattened transparency.
   rfDisableAAText         = $00400000; // Disable Anti-Aliasing for text.
   rfDisableAAVector       = $00800000; // Disable Anti-Aliasing for vector graphics.
   rfDisableAntiAliasing   = rfDisableAAClipping or rfDisableAAText or rfDisableAAVector; // Fully disable Anti-Aliasing.
   rfDisableBiLinearFilter = $01000000; // Disable the BiLevel filter for images. Sometetimes useful if sharp images are needed, e.g. for barcodes.


type TOnUpdateWindow = function(const Data: Pointer; var Area: TIntRect): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};

type TPDFRasterImage = record
   StructSize:      Cardinal;        // Must be set to sizeof(TPDFRasterImage)
   Flags:           TRasterFlags;    // This is a bit mask. Flags can be combined with a binary or operator
   DefScale:        TPDFPageScale;   // Specifies how the page should be scaled into the image buffer.
   InitWhite:       LongBool;        // If true, the image buffer is initialized to white before rendering.
                                     // When a clipping rectangle is set, only the area inside the clipping
                                     // rectangle is initialized to white.

   ClipRect:        TIntRect;        // Optional clipping rectangle defined in device coordinates (Pixels), default 0,0,0,0 (no clipping)
   Matrix:          TCTM;            // Optional transformation matrix. Initialize the variable to the identity matrix (1,0,0,1,0,0)
                                     // if you don't need it. The matrix can be used to move and scale the picture inside the image. 
   PageSpace:       TCTM;            // Out -> The matrix represents the mapping from page space to device space. This matrix
                                     // is required when further objects should be drawn on the page, e.g. the bounding boxes.

   DrawFrameRect:   LongBool;        // If true, the area outside the page's bounding box is filled with the
                                     // frame color. InitWhite can still be used, with or without a clipping
                                     // rectangle.
   FrameColor:      Cardinal;        // Must be defined in the color space of the pixel format but in the natural
                                     // component order, e.g. RGB.

   OnUpdateWindow:  TOnUpdateWindow; // Optional, UpdateOnPathCount and UpdateOnImageCoverage define when the function should be called
   OnInitDecoder:   Pointer;         // Not yet defined
   OnDecodeLine:    Pointer;         // Not yet defined
   UserData:        Pointer;         // Arbitrary pointer that should be passed to the callback functions

   UpdateOnPathCount: Cardinal;      // Optional -> Call OnUpdateWindow when the limit was reached.
                                     // Clipping paths increment the number too.
                                     // Only full paths are considered, independent of the number of vertices
                                     // they contain. The value should be larger than 50 and smaller than 10000.

   UpdateOnImageCoverage: Single;    // Optional -> DynaPDF multiplies the output image width and height with this
                                     // factor to calculate the coverage limit. When an image is inserted the unscaled
                                     // width and height is added to the current coverage value. When the number
                                     // reaches the limit the OnUpdateWindow event is raised.
                                     // The factor should be around 0.5 through 5.0. Larger values cause less
                                     // frequently update events.
   // Statistics...
   NumAnnots:         Cardinal;      // Out -> Number of rendered annotations (excluding invisible annotation but including annotations with no appearance)
   NumBezierCurves:   Cardinal;      // Out -> Number of bezier curves which where rendered. Glyph outlines are not taken into account.
   NumClipPaths:      Cardinal;      // Out -> Number of clipping paths used in the page. Should be small as possible!
   NumFormFields:     Cardinal;      // Out -> Number of rendered form fields (excluding invisible fields but including fields with no appearance)
   NumGlyphs:         Cardinal;      // Out -> When the number of glyphs equals NumTextRecords then there is probably some room for optimization...
   NumImages:         Cardinal;      // Out -> Number of images which were rendered
   NumLineTo:         Cardinal;      // Out -> Number of LineTo operators
   NumPaths:          Cardinal;      // Out -> Number of paths which were processed
   NumPatterns:       Cardinal;      // Out -> Number of pattern which were processed
   NumRectangles:     Cardinal;      // Out -> Number of rectangle operators
   NumRestoreGState:  Cardinal;      // Out -> Should be equal to NumSaveGState
   NumSaveGState:     Cardinal;      // Out -> The number of save graphics state operators
   NumShadings:       Cardinal;      // Out -> Number shadings which were processed
   NumSoftMasks:      Cardinal;      // Out -> Number of soft masks that were processed. Alpha channels of images are not taken into account.
   NumTextRecords:    Cardinal;      // Out -> Number of text records which were rendered
end;

type PPDFRasterImage = ^TPDFRasterImage;

{ ------------------------------------------------------------------------------------- }

{------------------------------------------ Function Prototypes --------------------------------------------------}

type
   { ------------------------------------------------- Font API ------------------------------------------------- }
   TfntBuildFamilyNameAndStyle = function(const IFont: Pointer; Name: PAnsiChar {Array[0..127] of AnsiChar}; var Style: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TfntGetFont                 = function(const IFont: Pointer; var F: TPDFFontObj): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TfntGetFontInfo             = function(const IFont: Pointer; var F: TPDFFontInfo): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TfntGetSpaceWidth           = function(const IFont: Pointer; FontSize: Double): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TfntGetTextWidth            = function(const IFont: Pointer; const Text: PAnsiChar; Len: Cardinal; CharSpacing, WordSpacing, TextScale: Single): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TfntTranslateRawCode        = function(const IFont: Pointer; const Text: PAnsiChar; Len: Cardinal; var Width: Double; OutText: PWideChar {Must be 32 characters long!}; var OutLen: Integer; var Decoded: LongBool; CharSpacing, WordSpacing, TextScale: Single): Cardinal; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TfntTranslateString         = function(var Stack: TPDFStack; Buffer: PWideChar; Size, Flags: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TfntTranslateString2        = function(const IFont: Pointer; const Text: PAnsiChar; Len: Cardinal; OutBuf: PWideChar; Size, Flags: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};

   { --------------------------------------------- Rendering Engine --------------------------------------------- }

   TrasAbort              = procedure(RasPtr: PPointer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasAttachImageBuffer  = function(RasPtr: Pointer; Rows: PPointer; Buffer: Pointer; Width, Height: Cardinal; ScanlineLen: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasCalcPagePixelSize  = procedure(PagePtr: Pointer; DefScale: TPDFPageScale; Scale: Single; FrameWidth, FrameHeight: Cardinal; Flags: TRasterFlags; var Width, Height: Cardinal); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasCreateRasterizer   = function(const IPDF: Pointer; Rows: PPointer; Buffer: Pointer; Width, Height: Cardinal; ScanlineLen: Integer; PixelFormat: TPDFPixFormat): Pointer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasCreateRasterizerEx = function(const IPDF: Pointer; DC: HDC; Width, Height: Cardinal; PixFmt: TPDFPixFormat): Pointer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasDeleteRasterizer   = procedure(RasPtr: PPointer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasRedraw             = procedure(RasPtr: Pointer; DC: HDC; DestX, DestY: Integer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasResizeBitmap       = function(RasPtr: Pointer; DC: HDC; Width, Height: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};

   { ------------------------------------------------ Page Cache ------------------------------------------------ }

   type IPGC = Pointer;

   type TInitCacheFlags = Cardinal;
   const
      icfDefault          = 0;
      icfIgnoreOpenAction = 1;
      icfIgnorePageLayout = 2;

   {
      The path names can be set in Ansi (code page 1252 on Windows) or Unicode format. The Ansi version accepts
      UTF-8 strings on non-Windows operating systems. UTF-16 Unicode strings are converted to UTF-8 on non-Windows
      operating systems.

      In general, the DefInXXX profiles are used if no other profile is available for the color space. Possible
      sources are DefaultGray, DefaultRGB, DefaultCMYK, and the Rendering Intents.

      The SoftProof profile emulates the output device. This is typically a printer profile or a default CMYK
      profile. If no profile is set then no device will be emulated. What you see is maybe not what you get on
      a printer.

      To disable color management set the parameter Profiles of TPDFPageCache.InitColormanagement() to nil.
   }
   type TPDFColorProfiles = record
      StructSize:     Cardinal;  // Must be set to sizeof(TPDFColorProfile)
      DefInGrayA:     PAnsiChar; // Optional
      DefInGrayW:     PWideChar; // Optional
      DefInRGBA:      PAnsiChar; // Optional, sRGB is the default. The "A" stands for Ansi string and not for Alpha...
      DefInRGBW:      PWideChar; // Optional
      DefInCMYKA:     PAnsiChar; // Optional, CMYK colors are the problematic ones. The other profiles can be created on demand
      DefInCMYKW:     PWideChar; // but this is not possible with a CMYK profile. So, this is the most important input profile.
      DeviceProfileA: PAnsiChar; // Optional, the output profile must be compatible with the output color space.
      DeviceProfileW: PWideChar; // At this time only Gray or RGB profiles are supported. This is the monitor profile! Default is sRGB.
      SoftProofA:     PAnsiChar; // Optional, this profile emulates the output device.
      SoftProofW:     PWideChar; // Optional.
   end;
   type PPDFColorProfiles = ^TPDFColorProfiles;

   type TPDFInitCMFlags = Integer;
   const
      icmDefault        = 0;  // Default rules.
      icmBPCompensation = 1;  // Black point compensation preserves the black point when converting CMYK colors to different color spaces.
                              // Should be set by default to increae the rendering quality of CMYK images.
      icmCheckBlackPoint = 2; // If set, soft proofing will be disabled if the black point of the output intent is probably invalid.
      
   type TPDFCursor =
   (
      pcrHandNormal,
      pcrHandClosed,
      pcrHandPoint,
      pcrIBeam
   );
      
   type TPDFThreadPriority =
   (
      ttpLowest,
      ttpIdle,
      ttpBelowNormal, // This is the default value. Normal can be used too but scrolling is smoother in this mode.
      ttpNormal,
      ttpAboveNormal,
      ttpHighest,     // Not really useful...
      ttpTimeCritical // Don't do that!
   );

   type TUpdBmkAction = Integer;
   const
      ubaDoNothing     = 0;  // Nothing to do
      ubaOpenPage      = 1;  // Jump to the new page. This flag is set if the bookmark contained a destination or go to action.
      ubaPageScale     = 2;  // Update the page scale with SetPageScale().
      ubaZoom          = 4;  // Zoom into the page, update the scroll ranges, and set the scroll positions.
      ubaUpdScrollBars = 8;  // This flag is always set if ubaZoom is set.
      ubaExecAction    = 16; // Check the parameter Action to execute further code. This flag can occur with or without ubaOpenPage.

   type TUpdScrollbar = Integer;
   const
      usbNoUpdate         = 0;  // Nothing to do
      usbVertRange        = 1;  // Update the vertical scroll range
      usbVertScrollPos    = 2;  // Update the vertical scroll position
      usbHorzRange        = 4;  // Update the horizontal scroll range
      usbHorzScrollPos    = 8;  // Update the horizontal scroll position
      usbUpdateAll        = 15; // Update both scroll ranges and the scroll positions
      // The cursor constants are set by MouseMove. Since we have only one cursor there is never more than one constant set.
      usbCursorHandNormal = 16;  // This is the default if the left mouse button is not pressed and if we are not over an action field
      usbCursorHandClosed = 32;  // Occurs if we leave an action field and if the left mouse button is pressed
      usbCursorHandPoint  = 64;  // Occurs if we enter link or button field
      usbCursorIBeam      = 128; // Occurs if we enter an action field that accepts text input
      usbCursorMask       = 240; // Bitmask to mask out the cursor constants

type
   TrasChangeBackColor     = procedure(CachePtr: IPGC; Value: Cardinal); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasCloseFile           = procedure(CachePtr: IPGC); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasCreatePageCache     = function(const IPDF: Pointer; PixFmt: TPDFPixFormat; HBorder, VBorder, BackColor: Cardinal): IPGC; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasDeletePageCache     = procedure(var CachePtr: IPGC); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasExecBookmark        = function(CachePtr: IPGC; Handle: Cardinal; var NewX, NewY: Integer; var NewZoom: Single; var NewPageScale: TPDFPageScale; Action: Pointer): TUpdBmkAction; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasGetCurrPage         = function(CachePtr: IPGC): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasGetCurrZoom         = function(CachePtr: IPGC): Single; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasGetDefPageLayout    = function(CachePtr: IPGC): TPageLayout; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasGetPageAt           = function(CachePtr: IPGC; ScrollX, ScrollY, X, Y: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasGetPageLayout       = function(CachePtr: IPGC): TPageLayout; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasGetPageScale        = function(CachePtr: IPGC): TPDFPageScale; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasGetRotate           = function(CachePtr: IPGC): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasGetScrollLineDelta  = function(CachePtr: IPGC; Vertical: LongBool): Cardinal; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasGetScrollPos        = function(CachePtr: IPGC; Vertical: LongBool; PageNum: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasGetScrollRange      = function(CachePtr: IPGC; Vertical: LongBool; var Max, SmallChange, LargeChange: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasInitBaseObjects     = function(CachePtr: IPGC; Width, Height: Integer; Flags: TInitCacheFlags): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasInitColorManagement = function(CachePtr: IPGC; Profiles: PPDFColorProfiles; Flags: TPDFInitCMFlags): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasInitialize          = function(CachePtr: IPGC; Priority: TPDFThreadPriority): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasMouseDown           = function(CachePtr: IPGC; X, Y: Integer): TPDFCursor; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasMouseMove           = function(CachePtr: IPGC; hWnd: HWND; LeftBtnDown: LongBool; var ScrollX, ScrollY: Integer; X, Y: Integer): TUpdScrollbar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasPaint               = function(CachePtr: IPGC; DC: HDC; var ScrollX, ScrollY: Integer): TUpdScrollbar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasProcessErrors       = function(CachePtr: IPGC): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasResetMousePos       = procedure(CachePtr: IPGC); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasResize              = function(CachePtr: IPGC; Width, Height: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasScroll              = function(CachePtr: IPGC; Vertical: LongBool; ScrollCode: Integer; var ScrollX, ScrollY: Integer): TUpdScrollbar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasSetCMapDirA         = function(CachePtr: IPGC; const Path: PAnsiChar; Flags: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasSetCMapDirW         = function(CachePtr: IPGC; const Path: PWideChar; Flags: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasSetDefPageLayout    = procedure(CachePtr: IPGC; Value: TPageLayout); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasSetMinLineWidth     = function(CachePtr: IPGC; Value: Single): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasSetPageLayout       = procedure(CachePtr: IPGC; Value: TPageLayout); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasSetPageScale        = procedure(CachePtr: IPGC; Value: TPDFPageScale); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasSetRotate           = procedure(CachePtr: IPGC; Value: Integer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasSetScrollLineDelta  = function(CachePtr: IPGC; Vertical: LongBool; Value: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasSetThreadPriority   = procedure(CachePtr: IPGC; UpdateThread, RenderThread: TPDFThreadPriority); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TrasZoom                = function(CachePtr: IPGC; Value: Single; var HorzPos, VertPos: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};

   { ------------------------------------------------- Table API ------------------------------------------------ }

   type ITBL = Pointer;

   type TCellContType =
   (
      cctText,
      cctImage,
      cctTable,
      cctTemplate
   );

   type TDeleteContent = Integer;
   const  
      dcText       = $00000001; // Text is always a foreground object
      dcImage      = $00000002;
      dcTemplate   = $00000004; // PDF or EMF objects
      dcTable      = $00000008;
      dcAllCont    = $0000001F; // Delete all content types
      dcForeGround = $10000000;
      dcBackGround = $20000000;
      dcBoth       = $30000000; // Delete both foreground and background objects

   type TCellAlign =
   (
      coLeft,
      coTop    = coLeft,
      coRight,
      coBottom = coRight,
      coCenter
   );

   type TColumnAdjust =
   (
      coaUniqueWidth, // Set the column widths uniquely to TableWidth / NumColumns
      coaAdjLeft,     // Decrease or increase the column widths starting from the left side
      coaAdjRight     // Decrease or increase the column widths starting from the right side
   );
      
   type TTableColor =
   (
      tcBackColor,     // Table, Columns, Rows, Cells -> default none (transparent)
      tcBorderColor,   // Table, Columns, Rows, Cells -> default black
      tcGridHorzColor, // Table                       -> default black
      tcGridVertColor, // Table                       -> default black
      tcImageColor,    // Table, Columns, Rows, Cells -> default RGB black
      tcTextColor      // Table, Columns, Rows, Cells -> default black
   );

   type TTableBoxProperty =
   (
      tbpBorderWidth, // Table, Columns, Rows, Cells -> default (0, 0, 0, 0)
      tbpCellSpacing, // Table, Columns, Rows, Cells -> default (0, 0, 0, 0)
      tbpCellPadding  // Table, Columns, Rows, Cells -> default (0, 0, 0, 0)
   );

   type TTableFlags = Integer;
   const
      tfDefault     = 0;
      tfStatic      = 1;  // This flag marks a row, column, or cell as static to avoid the deletion of the content with ClearContent().
      tfHeaderRow   = 2;  // Header rows are drawn first after a page break occurred
      tfNoLineBreak = 4;  // Prohibit line breaks in cells whith text -> Can be set to the entire table, columns, rows, and cells
      tfScaleToRect = 8;  // If set, the specified output width and height represents the maximum size of the image or template.
                          // The image or template is scaled into this rectangle without changing the aspect ratio.
      tfUseImageCS  = 16; // If set, images are inserted in the native image color space.

type
   TtblAddColumn          = function(const Table: ITBL; Left: LongBool; Width: Single): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblAddRow             = function(const Table: ITBL; Height: Single): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblAddRows            = function(const Table: ITBL; Count: Cardinal; Height: Single): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblClearColumn        = procedure(const Table: ITBL; Col: Cardinal; Types: TDeleteContent); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblClearContent       = procedure(const Table: ITBL; Types: TDeleteContent); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblClearRow           = procedure(const Table: ITBL; Row: Cardinal; Types: TDeleteContent); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblCreateTable        = function(const IPDF: Pointer; AllocRows, NumCols: Cardinal; Width, DefRowHeight: Single): ITBL; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblDeleteCol          = procedure(const Table: ITBL; Col: Cardinal); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblDeleteRow          = procedure(const Table: ITBL; Row: Cardinal); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblDeleteRows         = procedure(const Table: ITBL); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblDeleteTable        = procedure(var Table: ITBL); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblDrawTable          = function(const Table: ITBL; x, y, MaxHeight: Single): Single; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblGetFirstRow        = function(const Table: ITBL): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblGetNextHeight      = function(const Table: ITBL; MaxHeight: Single; NextRow: PInteger): Single; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblGetNextRow         = function(const Table: ITBL): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblGetNumCols         = function(const Table: ITBL): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblGetNumRows         = function(const Table: ITBL): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblGetTableHeight     = function(const Table: ITBL): Single; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblGetTableWidth      = function(const Table: ITBL): Single; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblHaveMore           = function(const Table: ITBL): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetBoxProperty     = function(const Table: ITBL; Row, Col: Integer; PropType: TTableBoxProperty; Left, Right, Top, Bottom: Single): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetCellImageA      = function(const Table: ITBL; Row, Col: Integer; ForeGround: LongBool; HAlign, VAlign: TCellAlign; Width, Height: Single; const Image: PAnsiChar; Index: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetCellImageW      = function(const Table: ITBL; Row, Col: Integer; ForeGround: LongBool; HAlign, VAlign: TCellAlign; Width, Height: Single; const Image: PWideChar; Index: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetCellImageEx     = function(const Table: ITBL; Row, Col: Integer; ForeGround: LongBool; HAlign, VAlign: TCellAlign; Width, Height: Single; const Buffer: Pointer; BufSize, Index: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetCellOrientation = function(const Table: ITBL; Row, Col, Orientation: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetCellTable       = function(const Table: ITBL; Row, Col: Cardinal; HAlign, VAlign: TCellAlign; const SubTable: ITBL): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetCellTemplate    = function(const Table: ITBL; Row, Col: Integer; ForeGround: LongBool; HAlign, VAlign: TCellAlign; TmplHandle: Cardinal; Width, Height: Single): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetCellTextA       = function(const Table: ITBL; Row, Col, HAlign: TTextAlign; VAlign: TCellAlign; const Text: PAnsiChar; Len: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetCellTextW       = function(const Table: ITBL; Row, Col, HAlign: TTextAlign; VAlign: TCellAlign; const Text: PWideChar; Len: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetColor           = function(const Table: ITBL; Row, Col: Integer; ClrType: TTableColor; CS: TPDFColorSpace; Color: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetColorEx         = function(const Table: ITBL; Row, Col: Integer; ClrType: TTableColor; Color: PSingle; NumComps: Cardinal; CS: TExtColorSpace; Handle: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetColWidth        = function(const Table: ITBL; Col: Cardinal; Width: Single; ExtTable: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetFlags           = function(const Table: ITBL; Row, Col: Integer; Flags: TTableFlags): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetFontA           = function(const Table: ITBL; Row, Col: Integer; const Name: PAnsiChar; Style: TFStyle; Embed: LongBool; CP: TCodepage): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetFontW           = function(const Table: ITBL; Row, Col: Integer; const Name: PWideChar; Style: TFStyle; Embed: LongBool; CP: TCodepage): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetFontSelMode     = function(const Table: ITBL; Row, Col: Integer; Mode: TFontSelMode): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetFontSize        = function(const Table: ITBL; Row, Col: Integer; Value: Single): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetGridWidth       = function(const Table: ITBL; Horz, Vert: Single): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetRowHeight       = function(const Table: ITBL; Row: Integer; Value: Single): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TtblSetTableWidth      = procedure(const Table: ITBL; Value: Single; AdjustType: TColumnAdjust; MinColWidth: Single); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   
   { ------------------------------------------------ DynaPDF API ----------------------------------------------- }
   TpdfAddActionToObj = function(const IPDF: Pointer; ObjType, Event: Integer; ActHandle, ObjHandle: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddArticle = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddBookmarkA = function(const IPDF: Pointer; const Title: PAnsiChar; Parent: Integer; DestPage: Cardinal; Open: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddBookmarkW = function(const IPDF: Pointer; const Title: PWideChar; Parent: Integer; DestPage: Cardinal; Open: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddBookmarkExA = function(const IPDF: Pointer; const Title: PAnsiChar; Parent: Integer; NamedDest: Cardinal; Open: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddBookmarkExW = function(const IPDF: Pointer; const Title: PWideChar; Parent: Integer; NamedDest: Cardinal; Open: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddBookmarkEx2A = function(const IPDF: Pointer; const Title: PAnsiChar; Parent: Integer; const NamedDest: Pointer; Unicode, Open: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddBookmarkEx2W = function(const IPDF: Pointer; const Title: PWideChar; Parent: Integer; const NamedDest: Pointer; Unicode, Open: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddButtonImageA = function(const IPDF: Pointer; BtnHandle: Cardinal; State: Integer; const Caption, ImgFile: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddButtonImageW = function(const IPDF: Pointer; BtnHandle: Cardinal; State: Integer; const Caption, ImgFile: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddButtonImageEx = function(const IPDF: Pointer; BtnHandle: Cardinal; State: Integer; const Caption: PAnsiChar; Handle: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddContinueTextA = function(const IPDF: Pointer; const AText: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddContinueTextW = function(const IPDF: Pointer; const AText: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddDeviceNProcessColorants = function(const IPDF: Pointer; DeviceNCS: Cardinal; const Colorants: PPAnsiChar; NumColorants: Cardinal; ProcessCS: TExtColorSpace; Handle: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddDeviceNSeparations = function(const IPDF: Pointer; DeviceNCS: Cardinal; const Colorants: PPAnsiChar; SeparationCS: PInteger; NumColorants: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddFieldToFormAction = function(const IPDF: Pointer; Action, AField: Cardinal; Include: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddFieldToHideAction = function(const IPDF: Pointer; HideAct, AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddFileCommentA = function(const IPDF: Pointer; const AText: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddFileCommentW = function(const IPDF: Pointer; const AText: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddFontSearchPath = function(const IPDF: Pointer; const APath: PAnsiChar; Recursive: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddImage = function(const IPDF: Pointer; Filter: TCompressionFilter; Flags: Cardinal; var Image: TPDFImage): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddJavaScriptA = function(const IPDF: Pointer; const Name, Script: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddJavaScriptW = function(const IPDF: Pointer; const Name: PAnsiChar; const Script: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddLayerToDisplTreeA = function(const IPDF: Pointer; Parent: Pointer; Layer: Integer; const Title: PAnsiChar): Pointer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddLayerToDisplTreeW = function(const IPDF: Pointer; Parent: Pointer; Layer: Integer; const Title: PWideChar): Pointer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddMaskImage = function(const IPDF: Pointer; BaseImage: Cardinal; const Buffer: Pointer; BufSize: Cardinal; Stride: Integer; BitsPerPixel, Width, Height: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddObjectToLayer = function(const IPDF: Pointer; OCG: Cardinal; ObjType: TOCObject; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddOCGToAppEvent = function(const IPDF: Pointer; Handle: Cardinal; Events: TOCAppEvent; Categories: TOCGUsageCategory): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddOutputIntentA = function(const IPDF: Pointer; const ICCFile: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddOutputIntentW = function(const IPDF: Pointer; const ICCFile: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddOutputIntentEx = function(const IPDF, Buffer: Pointer; BufSize: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddPageLabelA = function(const IPDF: Pointer; StartRange: Cardinal; Format: TPageLabelFormat; const Prefix: PAnsiChar; AddNum: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddPageLabelW = function(const IPDF: Pointer; StartRange: Cardinal; Format: TPageLabelFormat; const Prefix: PWideChar; AddNum: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddRasImage = function(const IPDF: Pointer; RasPtr: Pointer; Filter: TCompressionFilter): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddValToChoiceFieldA = function(const IPDF: Pointer; Field: Cardinal; const ExpValue, Value: PAnsiChar; Selected: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAddValToChoiceFieldW = function(const IPDF: Pointer; Field: Cardinal; const ExpValue: PAnsiChar; const Value: PWideChar; Selected: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAppend = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfApplyPattern = function(const IPDF: Pointer; PattHandle: Integer; ColorMode: TColorMode; Color: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfApplyShading = function(const IPDF: Pointer; ShadHandle: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAssociateEmbFile = function(const IPDF: Pointer; DestObject: TAFDestObject; DestHandle: Integer; Relationship: TAFRelationship; EmbFile: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAttachFileA = function(const IPDF: Pointer; const FilePath, Description: PAnsiChar; Compress: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAttachFileW = function(const IPDF: Pointer; const FilePath, Description: PWideChar; Compress: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAttachFileExA = function(const IPDF, Buffer: Pointer; BufSize: Cardinal; const FileName, Description: PAnsiChar; Compress: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAttachFileExW = function(const IPDF, Buffer: Pointer; BufSize: Cardinal; const FileName, Description: PWideChar; Compress: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfAutoTemplate = function(const IPDF: Pointer; Templ: Cardinal; PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfBeginClipPath = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfBeginContinueText = function(const IPDF: Pointer; PosX, PosY: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfBeginLayer = function(const IPDF: Pointer; OCG: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfBeginPageTemplate = function(const IPDF: Pointer; const Name: PAnsiChar; UseAutoTemplates: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfBeginPattern = function(const IPDF: Pointer; PatternType: TPatternType; TilingType: TTilingType; Width, Height: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfBeginTemplate = function(const IPDF: Pointer; Width, Height: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfBeginTemplateEx = function(const IPDF: Pointer; var BBox: TPDFRect; var Matrix: TCTM): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfBeginTransparencyGroup = function(const IPDF: Pointer; x1, y1, x2, y2: Double; Isolated, Knockout: LongBool; CS: TExtColorSpace; CSHandle: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfBezier_1_2_3 = function(const IPDF: Pointer; x1, y1, x2, y2, x3, y3: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfBezier_1_3 = function(const IPDF: Pointer; x1, y1, x3, y3: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfBezier_2_3 = function(const IPDF: Pointer; x2, y2, x3, y3: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCalcWidthHeight = function(const IPDF: Pointer; OrgWidth, OrgHeight, ScaledWidth, ScaledHeight: Double): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeAnnotNameA = function(const IPDF: Pointer; Handle: Cardinal; const Name: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeAnnotNameW = function(const IPDF: Pointer; Handle: Cardinal; const Name: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeAnnotPos = function(const IPDF: Pointer; Handle: Cardinal; PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeBookmarkA = function(const IPDF: Pointer; ABmk: Integer; const Title: PAnsiChar; Parent: Integer; DestPage: Cardinal; Open: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeBookmarkW = function(const IPDF: Pointer; ABmk: Integer; const Title: PWideChar; Parent: Integer; DestPage: Cardinal; Open: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeFont = function(const IPDF: Pointer; Handle: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeFontSize = function(const IPDF: Pointer; Size: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeFontStyle = function(const IPDF: Pointer; Style: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeFontStyleEx = function(const IPDF: Pointer; Style: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeJavaScriptA = function(const IPDF: Pointer; Handle: Cardinal; const NewScript: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeJavaScriptW = function(const IPDF: Pointer; Handle: Cardinal; const NewScript: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeJavaScriptActionA = function(const IPDF: Pointer; Handle: Cardinal; const NewScript: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeJavaScriptActionW = function(const IPDF: Pointer; Handle: Cardinal; const NewScript: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeJavaScriptNameA = function(const IPDF: Pointer; Handle: Cardinal; const Name: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeJavaScriptNameW = function(const IPDF: Pointer; Handle: Cardinal; const Name: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeLinkAnnot = function(const IPDF: Pointer; Handle: Cardinal; const URL: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfChangeSeparationColor = function(const IPDF: Pointer; CSHandle, NewColor: Cardinal; Alternate: TExtColorSpace; AltHandle: Integer): LongBool; stdcall;
   TpdfCheckCollection = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCheckConformance = function(const IPDF: Pointer; ConfType: Integer; Options: Cardinal; const UserData: Pointer; OnFontNotFound: TOnFontNotFoundProc; OnReplaceICCProfile: TOnReplaceICCProfile): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCheckFieldNames = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCircleAnnotA = function(const IPDF: Pointer; PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCircleAnnotW = function(const IPDF: Pointer; PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfClearAutoTemplates = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfClearErrorLog = procedure(const IPDF: Pointer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfClearHostFonts = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfClipPath = function(const IPDF: Pointer; ClipMode: TClippingMode; FillMode: TPathFillMode): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCloseAndSignFile = function(const IPDF: Pointer; const CertFile, Password, Reason, Location: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCloseAndSignFileEx = function(const IPDF: Pointer; const OpenPwd, OwnerPwd: PAnsiChar; KeyLen, Restrict: Integer; const CertFile, Password, Reason, Location: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCloseAndSignFileExt = function(const IPDF: Pointer; var SigParms: TPDFSigParms): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCloseFile = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCloseFileEx = function(const IPDF: Pointer; const OpenPwd, OwnerPwd: PAnsiChar; KeyLen, Restrict: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCloseImage = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCloseImportFile = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCloseImportFileEx = function(IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfClosePath = function(const IPDF: Pointer; FillMode: TPathFillMode): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCloseTag = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfComputeBBox = function(const IPDF: Pointer; var BBox: TPDFRect; Flags: TCompBBoxFlags): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfConvColor = function(const Color: PDouble; NumComps: Cardinal; SourceCS: Integer; const IColorSpace: Pointer; DestCS: Integer): Cardinal; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfConvertColors = function(const IPDF: Pointer; Flags: TColorConvFlags; Add: PSingle): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfConvertEMFSpoolA = function(const IPDF: Pointer; const SpoolFile: PAnsiChar; LeftMargin, TopMargin: Double; Flags: Integer): Integer; stdcall;
   TpdfConvertEMFSpoolW = function(const IPDF: Pointer; const SpoolFile: PWideChar; LeftMargin, TopMargin: Double; Flags: Integer): Integer; stdcall;
   TpdfConvToUnicode = function(const IPDF: Pointer; const AString: PAnsiChar; CP: Integer): PWideChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreate3DAnnot = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; const Author, AnnotName, U3DFile, ImageFile: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreate3DBackground = function(const IPDF, IView: Pointer; BackColor: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreate3DGotoViewAction = function(const IPDF: Pointer; Base3DAnnot: Cardinal; const IView: Pointer; Named: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreate3DProjection = function(const IPDF, IView: Pointer; ProjType, ScaleType: Integer; Diameter, FOV: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreate3DView = function(const IPDF: Pointer; Base3DAnnot: Cardinal; const Name: PAnsiChar; SetAsDefault: LongBool; const Matrix: PDouble; CamDistance: Double; RM, LS: Integer): Pointer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateAnnotAP = function(const IPDF: Pointer; Annot: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateArticleThreadA = function(const IPDF: Pointer; const ThreadName: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateArticleThreadW = function(const IPDF: Pointer; const ThreadName: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateAxialShading = function(const IPDF: Pointer; sX, sY, eX, eY, SCenter: Double; SColor, EColor: Cardinal; Extend1, Extend2: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateBarcodeField = function(const IPDF: Pointer; const Name: PAnsiChar; Parent: Integer; PosX, PosY, Width, Height: Double; var Barcode: TPDFBarcode): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateButtonA = function(const IPDF: Pointer; const Name, Caption: PAnsiChar; Parent: Integer; PosX, PosY, Width, Height: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateButtonW = function(const IPDF: Pointer; const Name: PAnsiChar; const Caption: PWideChar; Parent: Integer; PosX, PosY, Width, Height: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateCheckBox = function(const IPDF: Pointer; const Name, ExpValue: PAnsiChar; Checked, Parent: Integer; PosX, PosY, Width, Height: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateCIEColorSpace = function(const IPDF: Pointer; Base: TExtColorSpace; WhitePoint, BlackPoint, Gamma, Matrix: PSingle): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateCollection = function(const IPDF: Pointer; View: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateCollectionFieldA = function(const IPDF: Pointer; ColType, Column: Integer; const Name, Key: PAnsiChar; Visible, Editable: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateCollectionFieldW = function(const IPDF: Pointer; ColType, Column: Integer; const Name: PWideChar; const Key: PAnsiChar; Visible, Editable: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateColItemDate = function(const IPDF: Pointer; EmbFile: Cardinal; const Key: PAnsiChar; Date: LongInt; const Prefix: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateColItemNumber = function(const IPDF: Pointer; EmbFile: Cardinal; const Key: PAnsiChar; Value: Double; const Prefix: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateColItemStringA = function(const IPDF: Pointer; EmbFile: Cardinal; const Key, Value, Prefix: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateColItemStringW = function(const IPDF: Pointer; EmbFile: Cardinal; const Key: PAnsiChar; const Value, Prefix: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateComboBox = function(const IPDF: Pointer; const Name: PAnsiChar; Sort, Parent: Integer; PosX, PosY, Width, Height: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateDeviceNColorSpace = function(const IPDF: Pointer; const Colorants: PPAnsiChar; NumColorants: Cardinal; const PostScriptFunc: PAnsiChar; Alternate: TExtColorSpace; Handle: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateExtGState = function(const IPDF: Pointer; var GS: TPDFExtGState): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateGoToAction = function(const IPDF: Pointer; DestType: TDestType; PageNum: Cardinal; a, b, c, d: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateGoToActionEx = function(const IPDF: Pointer; NamedDest: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateGoToEActionA = function(const IPDF: Pointer; Location: TEmbFileLocation; const Source: PAnsiChar; SrcPage: Cardinal; const Target, DestName: PAnsiChar; DestPage: Cardinal; NewWindow: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateGoToEActionW = function(const IPDF: Pointer; Location: TEmbFileLocation; const Source: PWideChar; SrcPage: Cardinal; const Target, DestName: PWideChar; DestPage: Cardinal; NewWindow: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateGoToRAction = function(const IPDF: Pointer; const FileName: PAnsiChar; PageNum: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateGoToRActionW = function(const IPDF: Pointer; const FileName: PWideChar; PageNum: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateGoToRActionExA = function(const IPDF: Pointer; const FileName: PAnsiChar; DestName: PAnsiChar; NewWindow: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateGoToRActionExW = function(const IPDF: Pointer; const FileName: PAnsiChar; DestName: PWideChar; NewWindow: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateGoToRActionExUA = function(const IPDF: Pointer; const FileName: PWideChar; DestName: PAnsiChar; NewWindow: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateGoToRActionExUW = function(const IPDF: Pointer; const FileName: PWideChar; DestName: PWideChar; NewWindow: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateGroupField = function(const IPDF: Pointer; const Name: PAnsiChar; Parent: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateHideAction = function(const IPDF: Pointer; AField: Cardinal; Hide: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateICCBasedColorSpaceA = function(const IPDF: Pointer; const ICCProfile: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateICCBasedColorSpaceW = function(const IPDF: Pointer; const ICCProfile: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateICCBasedColorSpaceEx = function(const IPDF, Buffer: Pointer; BufSize: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateImageA = function(const IPDF: Pointer; const FileName: PAnsiChar; Format: TImageFormat): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateImageW = function(const IPDF: Pointer; const FileName: PWideChar; Format: TImageFormat): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateImpDataActionA = function(const IPDF: Pointer; const DataFile: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateImpDataActionW = function(const IPDF: Pointer; const DataFile: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateIndexedColorSpace = function(const IPDF: Pointer; Base, Handle: Integer; const ColorTable: Pointer; NumColors: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateJSActionA = function(const IPDF: Pointer; const Script: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateJSActionW = function(const IPDF: Pointer; const Script: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateLaunchAction = function(const IPDF: Pointer; OP: Integer; const FileName, DefDir, Param: PAnsiChar; NewWindow: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateLaunchActionExA = function(const IPDF: Pointer; const FileName: PAnsiChar; NewWindow: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateLaunchActionExW = function(const IPDF: Pointer; const FileName: PWideChar; NewWindow: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateListBox = function(const IPDF: Pointer; const Name: PAnsiChar; Sort: LongBool; Parent: Integer; PosX, PosY, Width, Height: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateNamedAction = function(const IPDF: Pointer; Action: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateNamedDestA = function(const IPDF: Pointer; const Name: PAnsiChar; DestPage: Cardinal; DestType: TDestType; a, b, c, d: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateNamedDestW = function(const IPDF: Pointer; const Name: PWideChar; DestPage: Cardinal; DestType: TDestType; a, b, c, d: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateNewPDFA = function(const IPDF: Pointer; const OutPDF: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateNewPDFW = function(const IPDF: Pointer; const OutPDF: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateOCGA = function(const IPDF: Pointer; const Name: PAnsiChar; DisplayInUI, Visible: LongBool; Intent: TOCGIntent): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateOCGW = function(const IPDF: Pointer; const Name: PWideChar; DisplayInUI, Visible: LongBool; Intent: TOCGIntent): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateOCMD = function(const IPDF: Pointer; Visibility: TOCVisibility; const OCGs: PCardinal; Count: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateRadialShading = function(const IPDF: Pointer; sX, sY, R1, eX, eY, R2, SCenter: Double; SColor, EColor: Cardinal; Extend1, Extend2: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateRadioButton = function(const IPDF: Pointer; const Name, ExpValue: PAnsiChar; Checked, Parent: Integer; PosX, PosY, Width, Height: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateResetAction = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateSeparationCS = function(const IPDF: Pointer; const Colorant: PAnsiChar; Alternate, Handle: Integer; Color: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateSetOCGStateAction = function(const IPDF: Pointer; const On_: PCardinal; OnCount: Cardinal; const Off: PCardinal; OffCount: Cardinal; const Toggle: PCardinal; ToggleCount: Cardinal; PreserveRB: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateSigField = function(const IPDF: Pointer; const Name: PAnsiChar; Parent: Integer; PosX, PosY, Width, Height: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateSigFieldAP = function(const IPDF: Pointer; SigField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateSoftMask = function(const IPDF: Pointer; TranspGroup: Cardinal; MaskType: TSoftMaskType; BackColor: Cardinal): Pointer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateStdPattern = function(const IPDF: Pointer; Pattern: Integer; LineWidth, Distance: Double; LineColor, BackColor: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateStructureTree = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateSubmitAction = function(const IPDF: Pointer; Flags: Integer; const URL: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateTextField = function(const IPDF: Pointer; const Name: PAnsiChar; Parent, Multiline, MaxLen: Integer; PosX, PosY, Width, Height: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfCreateURIAction = function(const IPDF: Pointer; const URL: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDecryptPDFA = function(const IPDF: Pointer; const FileName: PAnsiChar; PwdType: Integer; const Password: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDecryptPDFW = function(const IPDF: Pointer; const FileName: PWideChar; PwdType: Integer; const Password: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteAcroForm = procedure(const IPDF: Pointer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteActionFromObj = function(const IPDF: Pointer; ObjType: Integer; ActHandle, ObjHandle: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteActionFromObjEx = function(const IPDF: Pointer; ObjType: Integer; ObjHandle, ActIndex: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteAnnotation = function(const IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteBookmark = function(const IPDF: Pointer; ABmk: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteEmbeddedFile = function(const IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteField = function(const IPDF: Pointer; AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteFieldEx = function(const IPDF: Pointer; const Name: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteJavaScripts = procedure(const IPDF: Pointer; DelJavaScriptActions: LongBool); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteOCGFromAppEvent = function(const IPDF: Pointer; Handle: Cardinal; Events: TOCAppEvent; Categories: TOCGUsageCategory; DelCategoryOnly: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteOutputIntent = function(const IPDF: Pointer; Index: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeletePageLabels = procedure(const IPDF: Pointer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeletePage = function(const IPDF: Pointer; PageNum: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeletePDF = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteSeparationInfo = function(const IPDF: Pointer; AllPages: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteTemplate = function(const IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteTemplateEx = function(const IPDF: Pointer; Index: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDeleteXFAForm = procedure(const IPDF: Pointer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDrawArc = function(const IPDF: Pointer; PosX, PosY, Radius, StartAngle, EndAngle: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDrawArcEx = function(const IPDF: Pointer; PosX, PosY, Width, Height, StartAngle, EndAngle: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDrawChord = function(const IPDF: Pointer; PosX, PosY, Width, Height, StartAngle, EndAngle: Double; FillMode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDrawCircle = function(const IPDF: Pointer; PosX, PosY, Radius: Double; FillMode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfDrawPie = function(const IPDF: Pointer; PosX, PosY, Width, Height, StartAngle, EndAngle: Double; FillMode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEditPage = function(const IPDF: Pointer; PageNum: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEditTemplate = function(const IPDF: Pointer; Index: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEditTemplate2 = function(const IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEllipse = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; FillMode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEncryptPDFA = function(const IPDF: Pointer; const FileName, OpenPwd, OwnerPwd: PAnsiChar; KeyLen: TKeyLen; Restrict: TRestrictions): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEncryptPDFW = function(const IPDF: Pointer; const FileName: PWideChar; const OpenPwd, OwnerPwd: PAnsiChar; KeyLen: TKeyLen; Restrict: TRestrictions): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEndContinueText = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEndLayer = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEndPage = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEndPattern = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEndTemplate = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEnumDocFonts = function(const IPDF, Data: Pointer; EnumProc: TEnumFontProc2): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEnumHostFonts = function(const IPDF: Pointer; const Data: Pointer; EnumProc: TEnumFontProc): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfEnumHostFontsEx = function(const IPDF, Data: Pointer; EnumProc: TEnumFontProcEx): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfExchangeBookmarks = function(const IPDF: Pointer; Bmk1, Bmk2: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfExchangePages = function(const IPDF: Pointer; First, Second: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFileAttachAnnotA = function(const IPDF: Pointer; PosX, PosY: Double; Icon: Integer; const Author, Desc, AFile: PAnsiChar; Compress: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFileAttachAnnotW = function(const IPDF: Pointer; PosX, PosY: Double; Icon: Integer; const Author, Desc, AFile: PWideChar; Compress: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFileLinkA = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; const AFilePath: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFileLinkW = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; const AFilePath: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFindBookmarkA = function(const IPDF: Pointer; DestPage: Integer; const Title: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFindBookmarkW = function(const IPDF: Pointer; DestPage: Integer; const Title: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFindFieldA = function(const IPDF: Pointer; const Name: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFindFieldW = function(const IPDF: Pointer; const Name: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFindLinkAnnot = function(const IPDF: Pointer; const URL: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFindNextBookmark = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFinishSignature = function(const IPDF, PKCS7Obj: Pointer; Length: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFlattenAnnots = function(IPDF: Pointer; Flags: TAnnotFlattenFlags): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFlattenForm = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFlushPageContent = function(const IPDF: Pointer; var Stack: TPDFStack): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFlushPages = function(const IPDF: Pointer; Flags: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFreeImageBuffer = procedure(const IPDF: Pointer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFreeImageObj = function(const IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFreeImageObjEx = function(const IPDF, ImagePtr: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFreePDF = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFreeTextAnnotA = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; const Author, AText: PAnsiChar; Align: TTextAlign): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFreeTextAnnotW = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; const Author, AText: PWideChar; Align: TTextAlign): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfFreeUniBuf = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetActionCount = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetActionType = function(const IPDF: Pointer; ActHandle: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetActionTypeEx = function(const IPDF: Pointer; ObjType: TObjType; ObjHandle, ActIndex: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetActiveFont = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetAllocBy = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetAnnot = function(const IPDF: Pointer; Handle: Cardinal; var Annot: TPDFAnnotation): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetAnnotBBox = function(const IPDF: Pointer; Handle: Cardinal; var BBox: TPDFRect): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetAnnotCount = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetAnnotEx = function(const IPDF: Pointer; Handle: Cardinal; var Annot: TPDFAnnotationEx): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetAnnotFlags = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetAnnotLink = function(const IPDF: Pointer; Handle: Cardinal): PAnsiChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetAnnotType = function(const IPDF: Pointer; Handle: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetAscent = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetBarcodeDict = function(const IBarcode: Pointer; var Barcode: TPDFBarcode): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetBBox = function(const IPDF: Pointer; Boundary: TPageBoundary; var BBox: TPDFRect): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetBidiMode = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetBookmark = function(const IPDF: Pointer; Handle: Integer; var Bmk: TBookmark): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetBookmarkCount = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetBorderStyle = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetBuffer = function(const IPDF: Pointer; var BufSize: Cardinal): PAnsiChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetCapHeight = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetCharacterSpacing = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetCheckBoxChar = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetCheckBoxCharEx = function(const IPDF: Pointer; AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetCheckBoxDefState = function(const IPDF: Pointer; AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetCMap = function(const IPDF: Pointer; Index: Cardinal; var CMap: TPDFCMap): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetCMapCount = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetColorSpace = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetColorSpaceCount = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetColorSpaceObj = function(const IPDF: Pointer; Handle: Cardinal; var CS: TPDFColorSpaceObj): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetColorSpaceObjEx = function(const IColorSpace: Pointer; var CS: TPDFColorSpaceObj): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetCompressionFilter = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetCompressionLevel = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetContent = function(const IPDF: Pointer; var Buffer: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetDefBitsPerPixel = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetDescent = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetDeviceNAttributes = function(const IAttributes: Pointer; var Attributes: TDeviceNAttributes): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetDocInfo = function(const IPDF: Pointer; DInfo: Integer; var Value: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetDocInfoCount = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetDocInfoEx = function(const IPDF: Pointer; Index: Cardinal; var DInfo: TDocumentInfo; var Key, Value: PAnsiChar; var Unicode: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetDocUsesTransparency = function(const IPDF: Pointer; Flags: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetDrawDirection = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetDynaPDFVersion = function(): PAnsiChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetEmbeddedFile = function(const IPDF: Pointer; Handle: Cardinal; var FileSpec: TPDFFileSpec; Decompress: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetEmbeddedFileCount = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetEMFPatternDistance = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetErrLogMessage = function(const IPDF: Pointer; Index: Cardinal; var Err: TPDFError): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetErrLogMessageCount = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetErrorMessage = function(IPDF: Pointer): PAnsiChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetErrorMode = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetField = function(const IPDF: Pointer; Handle: Cardinal; var Field: TPDFField): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldBackColor = function(IPDF: Pointer): Cardinal; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldBorderColor = function(IPDF: Pointer): Cardinal; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldBorderStyle = function(const IPDF: Pointer; AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldBorderWidth = function(const IPDF: Pointer; AField: Cardinal): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldChoiceValue = function(const IPDF: Pointer; AField, ValIndex: Cardinal; var Value: TPDFChoiceValue): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldColor = function(const IPDF: Pointer; AField: Cardinal; ColorType: Integer; var ColorSpace: Integer; var Color: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldCount = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldEx = function(const IPDF: Pointer; Handle: Cardinal; var Field: TPDFFieldEx): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldEx2 = function(const IField: Pointer; var Field: TPDFFieldEx): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldExpValCount = function(const IPDF: Pointer; AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldExpValue = function(const IPDF: Pointer; AField: Cardinal; var Value: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldExpValueEx = function(const IPDF: Pointer; AField, ValIndex: Cardinal; var Value, ExpValue: PAnsiChar; var Selected: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldFlags = function(const IPDF: Pointer; AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldGroupType = function(const IPDF: Pointer; AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldHighlightMode = function(const IPDF: Pointer; AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldIndex = function(const IPDF: Pointer; AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldMapName = function(const IPDF: Pointer; AField: Cardinal; var Value: Pointer; var Unicode: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldName = function(const IPDF: Pointer; AField: Cardinal; var Name: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldOrientation = function(const IPDF: Pointer; AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldTextAlign = function(const IPDF: Pointer; AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldTextColor = function(IPDF: Pointer): Cardinal; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldToolTip = function(const IPDF: Pointer; AField: Cardinal; var Value: Pointer; var Unicode: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFieldType = function(const IPDF: Pointer; AField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFillColor = function(IPDF: Pointer): Cardinal; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFontCount = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFontEx = function(const IPDF: Pointer; Handle: Cardinal; var F: TPDFFontObj): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFontInfoEx = function(const IPDF: Pointer; Handle: Cardinal; var F: TPDFFontInfo): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFontOrigin = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFontSearchOrder = procedure(const IPDF: Pointer; Order: PFontBaseType{Array[0..3] of TFontBaseType}); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFontSelMode = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFontWeight = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFTextHeightA = function(const IPDF: Pointer; Align: Integer; const AText: PAnsiChar): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFTextHeightExA = function(const IPDF: Pointer; Width: Double; Align: Integer; const AText: PAnsiChar): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFTextHeightExW = function(const IPDF: Pointer; Width: Double; Align: Integer; const AText: PWideChar): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetFTextHeightW = function(const IPDF: Pointer; Align: Integer; const AText: PWideChar): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetGStateFlags = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetIconColor = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetImageBuffer = function(const IPDF: Pointer; var BufSize: Cardinal): PAnsiChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetImageCountA = function(const IPDF: Pointer; const FileName: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetImageCountW = function(const IPDF: Pointer; const FileName: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetImageCountEx = function(const IPDF: Pointer; const Buffer: Pointer; BufSize: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetImageHeight = function(const IPDF: Pointer; Handle: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetImageObj = function(const IPDF: Pointer; Handle: Cardinal; Flags: TParseFlags; var Image: TPDFImage): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetImageObjCount = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetImageObjEx = function(const IPDF, ImagePtr: Pointer; Flags: Integer; var Image: TPDFImage): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetImageWidth = function(const IPDF: Pointer; Handle: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetImportFlags = function(IPDF: Pointer): Cardinal; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInBBox = function(const IPDF: Pointer; PageNum: Cardinal; Boundary: Integer; var BBox: TPDFRect): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInDocInfo = function(const IPDF: Pointer; DInfo: Integer; var Value: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInDocInfoCount = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInDocInfoEx = function(const IPDF: Pointer; Index: Cardinal; var DInfo: Integer; var Key, Value: PAnsiChar; var Unicode: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInEncryptionFlags = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInFieldCount = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInIsCollection = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInIsEncrypted = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInIsSigned = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInIsTrapped = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInIsXFAForm = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInMetadata = function(const IPDF: Pointer; PageNum: Integer; var Buffer: PByte; var BufSize: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInOrientation = function(const IPDF: Pointer; PageNum: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInPageCount = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInPDFVersion = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInPrintSettings = function(const IPDF: Pointer; var Settings: TPDFPrintSettings): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetInRepairMode = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetIsFixedPitch = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetIsTaggingEnabled = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetItalicAngle = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetJavaScript = function(const IPDF: Pointer; Handle: Cardinal; var Len: Cardinal; var Unicode: LongBool): PAnsiChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetJavaScriptAction = function(const IPDF: Pointer; Handle: Cardinal; var Len: Cardinal; var Unicode: LongBool): PAnsiChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetJavaScriptAction2 = function(const IPDF: Pointer; ObjType: TObjType; ObjHandle, ActIndex: Cardinal; var Len: Cardinal; var Unicode: LongBool; var Event: TObjEvent): PAnsiChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetJavaScriptCount = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetJavaScriptEx = function(const IPDF: Pointer; const Name: PAnsiChar; var Len: Cardinal; var Unicode: LongBool): PAnsiChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetJavaScriptName = function(const IPDF: Pointer; Handle: Cardinal; var Len: Cardinal; var Unicode: LongBool): PAnsiChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetJPEGQuality = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetLanguage = function(IPDF: Pointer): PAnsiChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetLastTextPosX = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetLastTextPosY = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetLeading = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetLineCapStyle = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetLineJoinStyle = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetLineWidth = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetLinkHighlightMode = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetLogMetafileSizeA = function(const IPDF: Pointer; const FileName: PAnsiChar; var R: TRectL): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetLogMetafileSizeW = function(const IPDF: Pointer; const FileName: PWideChar; var R: TRectL): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetLogMetafileSizeEx = function(const IPDF: Pointer; const Buffer: Pointer; BufSize: Cardinal; var R: TRectL): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetMatrix = function(const IPDF: Pointer; var M: TCTM): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetMaxFieldLen = function(const IPDF: Pointer; TxtField: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetMeasureObj = function(const Measure: IMSR; var Value: TPDFMeasure): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetMetaConvFlags = function(IPDF: Pointer): Cardinal; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetMetadata = function(const IPDF: Pointer; ObjType: TMetadataObj; Handle: Integer; var Buffer: PByte; var BufSize: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetMissingGlyphs = function(const IPDF: Pointer; var Count: Cardinal): PCardinal; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetMiterLimit = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetNamedDest = function(const IPDF: Pointer; Index: Cardinal; var Dest: TPDFNamedDest): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetNamedDestCount = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetNeedAppearance = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetNumberFormatObj = function(NumberFmt: INFM; var Value: TPDFNumberFormat): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetObjActionCount = function(const IPDF: Pointer; ObjType: Integer; ObjHandle: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetOCG = function(const IPDF: Pointer; Handle: Cardinal; var Value: TPDFOCG): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetOCGContUsage = function(const IPDF: Pointer; Handle: Cardinal; var Value: TPDFOCGContUsage): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetOCGCount = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetOCGUsageUserName = function(const IPDF: Pointer; Handle, Index: Cardinal; var NameA: PAnsiChar; var NameW: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetOpacity = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetOrientation = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetOutputIntent = function(const IPDF: Pointer; Index: Cardinal; var Intent: TPDFOutputIntent): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetOutputIntentCount = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageAnnot = function(const IPDF: Pointer; Index: Cardinal; var Annot: TPDFAnnotation): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageAnnotCount = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageAnnotEx = function(const IPDF: Pointer; Index: Cardinal; var Annot: TPDFAnnotationEx): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageBBox = function(PagePtr: Pointer; Boundary: TPageBoundary; var BBox: TFltRect): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageCoords = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageCount = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageField = function(const IPDF: Pointer; Index: Cardinal; var Field: TPDFField): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageFieldCount = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageFieldEx = function(const IPDF: Pointer; Index: Cardinal; var Field: TPDFFieldEx): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageHeight = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageLabel = function(const IPDF: Pointer; Index: Cardinal; var Lbl: TPDFPageLabel): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageLabelCount = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageLayout = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageMode = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageNum = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageObject = function(const IPDF: Pointer; PageNum: Cardinal): Pointer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageOrientation = function(PagePtr: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageText = function(const IPDF: Pointer; var Stack: TPDFStack): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPageWidth = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPDFVersion = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPrintSettings = function(const IPDF: Pointer; var Settings: TPDFPrintSettings): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPtDataArray = function(const PtData: IPTD; Index: Cardinal; var DataType: PAnsiChar; var Values: PSingle; var ValCount: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetPtDataObj = function(const PtData: IPTD; var Subtype: PAnsiChar; var NumArrays: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetResolution = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetSaveNewImageFormat = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetSeparationInfo = function(const IPDF: Pointer; var Colorant: PAnsiChar; var CS: TExtColorSpace): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetSigDict = function(const ISignature: Pointer; var SigDict: TPDFSigDict): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetStrokeColor = function(IPDF: Pointer): Cardinal; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetSysFontInfo = function(const IPDF: Pointer; Handle: Cardinal; var Value: TPDFSysFont): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTabLen = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTemplCount = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTemplHandle = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTemplHeight = function(const IPDF: Pointer; TmplHandle: Integer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTemplWidth = function(const IPDF: Pointer; TmplHandle: Integer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTextDrawMode = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTextFieldValue = function(const IPDF: Pointer; AField: Cardinal; var Value: PAnsiChar; var ValIsUnicode: LongBool; var DefValue: PAnsiChar; var DefValIsUnicode: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTextRect = function(const IPDF: Pointer; var PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTextRise = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTextScaling = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTextWidthA = function(const IPDF: Pointer; const AText: PAnsiChar): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTextWidthW = function(const IPDF: Pointer; const AText: PWideChar): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTextWidthExA = function(const IPDF: Pointer; const AText: PAnsiChar; Len: Cardinal): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTextWidthExW = function(const IPDF: Pointer; const AText: PWideChar; Len: Cardinal): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTrapped = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetTransparentColor = function(IPDF: Pointer): Cardinal; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetUseExactPwd = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetUseGlobalImpFiles = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetUserRights = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetUserUnit = function(const IPDF: Pointer): Single; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetUseStdFonts = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetUseSystemFonts = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetUseTransparency = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetUseVisibleCoords = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetViewerPrefrences = function(const IPDF: Pointer; var Preference, AddVal: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetViewport = function(const IPDF: Pointer; PageNum, Index: Cardinal; var VP: TPDFViewport): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetViewportCount = function(const IPDF: Pointer; PageNum: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetWMFDefExtent = function(const IPDF: Pointer; var Width, Height: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetWMFPixelPerInch = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfGetWordSpacing = function(IPDF: Pointer): Double; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfHaveOpenDoc = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfHaveOpenPage = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfHighlightAnnotA = function(const IPDF: Pointer; SubType: TAnnotType; PosX, PosY, Width, Height: Double; Color: Cardinal; const Author, Subject, Comment: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfHighlightAnnotW = function(const IPDF: Pointer; SubType: TAnnotType; PosX, PosY, Width, Height: Double; Color: Cardinal; const Author, Subject, Comment: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfImportBookmarks = function(const IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfImportCatalogObjects = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfImportDocInfo = function(const IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfImportPage = function(const IPDF: Pointer; PageNum: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfImportPageEx = function(const IPDF: Pointer; PageNum: Cardinal; ScaleFactX, ScaleFactY: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfImportPDFFile = function(const IPDF: Pointer; DestPage: Cardinal; ScaleFactX, ScaleFactY: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInitColorManagement = function(const IPDF: Pointer; Profiles: PPDFColorProfiles; DestSpace: TPDFColorSpace; Flags: TPDFInitCMFlags): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInitExtGState = function(var GS: TPDFExtGState): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInitOCGContUsage = function(var Value: TPDFOCGContUsage): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInitStack = function(const IPDF: Pointer; var Stack: TPDFStack): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertBMPFromBuffer = function(const IPDF: Pointer; PosX, PosY, ScaleWidth, ScaleHeight: Double; const Buffer: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertBMPFromHandle = function(const IPDF: Pointer; PosX, PosY, ScaleWidth, ScaleHeight: Double; Handle: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertBookmarkA   = function(const IPDF: Pointer; const Title: PAnsiChar; Parent: Integer; DestPage: Cardinal; Open, AddChildren: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertBookmarkW   = function(const IPDF: Pointer; const Title: PWideChar; Parent: Integer; DestPage: Cardinal; Open, AddChildren: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertBookmarkExA = function(const IPDF: Pointer; const Title: PAnsiChar; Parent: Integer; NamedDest: Cardinal; Open, AddChildren: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertBookmarkExW = function(const IPDF: Pointer; const Title: PWideChar; Parent: Integer; NamedDest: Cardinal; Open, AddChildren: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertImage = function(const IPDF: Pointer; PosX, PosY, ScaleWidth, ScaleHeight: Double; const AFile: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertImageEx = function(const IPDF: Pointer; PosX, PosY, ScaleWidth, ScaleHeight: Double; const AFile: PAnsiChar; Index: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertImageExW = function(const IPDF: Pointer; PosX, PosY, ScaleWidth, ScaleHeight: Double; const Image: PWideChar; Index: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertImageFromBuffer = function(const IPDF: Pointer; PosX, PosY, ScaleWidth, ScaleHeight: Double; const Buffer: Pointer; BufSize, Index: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertMetafileA = function(const IPDF: Pointer; const FileName: PAnsiChar; PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertMetafileW = function(const IPDF: Pointer; const FileName: PWideChar; PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertMetafileEx = function(const IPDF: Pointer; const Buffer: Pointer; BufSize: Cardinal; PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertMetafileExtA = function(const IPDF: Pointer; const FileName: PAnsiChar; var View: TRectL; PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertMetafileExtW = function(const IPDF: Pointer; const FileName: PWideChar; var View: TRectL; PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertMetafileExtEx = function(const IPDF: Pointer; const Buffer: Pointer; BufSize: Cardinal; var View: TRectL; PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertMetafileFromHandle = function(const IPDF, hEnhMetafile: Pointer; PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertMetafileFromHandleEx = function(const IPDF, hEnhMetafile: Pointer; var View: TRectL; PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertRawImage = function(const IPDF: Pointer; const Buffer: Pointer; BitsPerPixel, ColorCount, ImgWidth, ImgHeight: Cardinal; PosX, PosY, ScaleWidth, ScaleHeight: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfInsertRawImageEx = function(const IPDF: Pointer; PosX, PosY, ScaleWidth, ScaleHeight: Double; var Image: TPDFRawImage): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfIsBidiText = function(const IPDF: Pointer; const AText: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfIsColorPage = function(const IPDF: Pointer; GrayIsColor: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfIsEmptyPage = function(IPDF: Pointer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfLineAnnotA = function(const IPDF: Pointer; x1, y1, x2, y2, LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfLineAnnotW = function(const IPDF: Pointer; x1, y1, x2, y2, LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfLineTo = function(const IPDF: Pointer; PosX, PosY: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfLoadCMap = function(const IPDF: Pointer; const CMapName: PAnsiChar; Embed: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfLoadFDFDataA = function(const IPDF: Pointer; const FileName, Password: PAnsiChar; Flags: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfLoadFDFDataW = function(const IPDF: Pointer; const FileName: PWideChar; const Password: PAnsiChar; Flags: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfLoadFDFDataEx = function(const IPDF, Buffer: Pointer; BufSize: Cardinal; const Password: PAnsiChar; Flags: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfLoadFont = function(const IPDF, Buffer: Pointer; BufSize: Cardinal; Style: Integer; Size: Double; Embed: LongBool; CP: Integer): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfLoadFontExA = function(const IPDF: Pointer; const FontFile: PAnsiChar; Index: Cardinal; Style: TFStyle; Size: Double; Embed: LongBool; CP: TCodepage): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfLoadFontExW = function(const IPDF: Pointer; const FontFile: PWideChar; Index: Cardinal; Style: TFStyle; Size: Double; Embed: LongBool; CP: TCodepage): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfLockLayer = function(const IPDF: Pointer; Layer: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfMovePage = function(const IPDF: Pointer; Source, Dest: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfMoveTo = function(const IPDF: Pointer; PosX, PosY: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfNewPDF = function(): Pointer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfOpenImportBuffer = function(const IPDF: Pointer; const Buffer: Pointer; BufSize: Cardinal; PwdType: Integer; const Password: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfOpenImportFileA = function(const IPDF: Pointer; const FileName: PAnsiChar; PwdType: Integer; const Password: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfOpenImportFileW = function(const IPDF: Pointer; const FileName: PWideChar; PwdType: Integer; const Password: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfOpenOutputFileA = function(const IPDF: Pointer; const OutPDF: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfOpenOutputFileW = function(const IPDF: Pointer; const OutPDF: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfOpenTagA = function(const IPDF: Pointer; Tag: Integer; const Lang, AltText, Expansion: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfOpenTagW = function(const IPDF: Pointer; Tag: Integer; const Lang: PAnsiChar; const AltText, Expansion: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfOptimize = function(const IPDF: Pointer; Flags: Cardinal; Parms: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPageLink = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; DestPage: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPageLink2 = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; NamedDest: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPageLink3A = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; const NamedDest: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPageLink3W = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; const NamedDest: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPageLinkEx = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; DestType: Integer; DestPage: Cardinal; a, b, c, d: Double): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfParseContent = function(const IPDF, Data: Pointer; var Stack: TPDFParseInterface; Flags: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPlaceImage = function(const IPDF: Pointer; ImgHandle: Integer; PosX, PosY, ScaleWidth, ScaleHeight: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPlaceSigFieldValidateIcon = function(const IPDF: Pointer; SigField: Cardinal; PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPlaceTemplate = function(const IPDF: Pointer; TmplHandle: Integer; PosX, PosY, ScaleWidth, ScaleHeight: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPlaceTemplateEx = function(const IPDF: Pointer; TmplHandle: Integer; PosX, PosY, ScaleWidth, ScaleHeight: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPlaceTemplByMatrix = function(const IPDF: Pointer; TmplHandle: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPolygonAnnotA = function(const IPDF: Pointer; Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPolygonAnnotW = function(const IPDF: Pointer; Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPolyLineAnnotA = function(const IPDF: Pointer; Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfPolyLineAnnotW = function(const IPDF: Pointer; Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReadImageFormatA = function(const IPDF: Pointer; const FileName: PAnsiChar; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReadImageFormatW = function(const IPDF: Pointer; const FileName: PWideChar; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReadImageFormat2A = function(const IPDF: Pointer; const FileName: PAnsiChar; Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReadImageFormat2W = function(const IPDF: Pointer; const FileName: PWideChar; Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReadImageFormatEx = function(const IPDF: Pointer; hBitmap: Pointer; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReadImageFormatFromBuffer = function(IPDF, Buffer: Pointer; BufSize, Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReadImageResolutionA = function(const IPDF: Pointer; const FileName: PAnsiChar; Index: Cardinal; var ResX, ResY: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReadImageResolutionW = function(const IPDF: Pointer; const FileName: PWideChar; Index: Cardinal; var ResX, ResY: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReadImageResolutionEx = function(const IPDF, Buffer: Pointer; BufSize, Index: Cardinal; var ResX, ResY: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRectangle = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; FillMode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReEncryptPDFA = function(const IPDF: Pointer; const FileName: PAnsiChar; PwdType: Integer; const InPwd, NewOpenPwd, NewOwnerPwd: PAnsiChar; NewKeyLen: TKeyLen; Restrict: TRestrictions): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReEncryptPDFW = function(const IPDF: Pointer; const FileName: PWideChar; PwdType: Integer; const InPwd, NewOpenPwd, NewOwnerPwd: PAnsiChar; NewKeyLen: TKeyLen; Restrict: TRestrictions): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRenameSpotColor = function(const IPDF: Pointer; const Colorant, NewName: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRenderAnnotOrField = function(const IPDF: Pointer; Handle: Cardinal; IsAnnot: LongBool; State: TButtonState; var Matrix: TCTM; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; var OutImage: TPDFBitmap): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRenderPage = function(const IPDF: Pointer; PagePtr, RasPtr: Pointer; var Img: TPDFRasterImage): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRenderPageEx = function(const IPDF: Pointer; DC: HDC; var DestX, DestY: Integer; PagePtr, RasPtr: Pointer; var Img: TPDFRasterImage): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRenderPageToImageA = function(const IPDF: Pointer; PageNum: Cardinal; const OutFile: PAnsiChar; Resolution, Width, Height: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRenderPageToImageW = function(const IPDF: Pointer; PageNum: Cardinal; const OutFile: PWideChar; Resolution, Width, Height: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRenderPDFFileA = function(const IPDF: Pointer; const OutFile: PAnsiChar; Resolution: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRenderPDFFileW = function(const IPDF: Pointer; const OutFile: PWideChar; Resolution: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReOpenImportFile = function(const IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReplacePageTextA = function(const IPDF: Pointer; const NewText: PAnsiChar; var Stack: TPDFStack): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReplacePageTextExA = function(const IPDF: Pointer; const NewText: PAnsiChar; var Stack: TPDFStack): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReplacePageTextExW = function(const IPDF: Pointer; const NewText: PWideChar; var Stack: TPDFStack): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfResetLineDashPattern = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRestoreGraphicState = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRotateCoords = function(const IPDF: Pointer; alpha, OriginX, OriginY: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRoundRect = function(const IPDF: Pointer; PosX, PosY, Width, Height, Radius: Double; FillMode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfRoundRectEx = function(const IPDF: Pointer; PosX, PosY, Width, Height, rWidth, rHeight: Double; FillMode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSaveGraphicState = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfScaleCoords = function(const IPDF: Pointer; sx, sy: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSelfTest = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSet3DAnnotProps = function(const IPDF: Pointer; Annot: Cardinal; ActType, DeActType, InstType, DeInstType: Integer; DisplToolbar, DisplModelTree: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSet3DAnnotScriptA = function(const IPDF: Pointer; Annot: Cardinal; const Value: PAnsiChar; Len: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAllocBy = function(const IPDF: Pointer; Value: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotBorderStyle = function(const IPDF: Pointer; Handle: Cardinal; Style: TBorderStyle): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotBorderWidth = function(const IPDF: Pointer; Handle: Cardinal; LineWidth: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotColor = function(const IPDF: Pointer; Handle: Cardinal; ColorType: TAnnotColor; CS: TPDFColorSpace; Color: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotFlags = function(const IPDF: Pointer; Flags: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotFlagsEx = function(const IPDF: Pointer; Handle: Cardinal; Flags: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotHighlightMode = function(const IPDF: Pointer; Handle: Cardinal; Mode: THighlightMode): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotIcon = function(const IPDF: Pointer; Handle: Cardinal; Icon: TAnnotIcon): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotLineEndStyle = function(const IPDF: Pointer; Handle: Cardinal; LE1, LE2: TLineEndStyle): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotOpenState = function(const IPDF: Pointer; Handle: Cardinal; Open: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotOrFieldDate = function(const IPDF: Pointer; Handle: Cardinal; IsField: LongBool; DateType: TDateType; DateTime: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotStringA = function(const IPDF: Pointer; Handle: Cardinal; StringType: Integer; const Value: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotStringW = function(const IPDF: Pointer; Handle: Cardinal; StringType: Integer; const Value: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotSubjectA = function(const IPDF: Pointer; Handle: Cardinal; const Value: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetAnnotSubjectW = function(const IPDF: Pointer; Handle: Cardinal; const Value: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetBBox = function(const IPDF: Pointer; Boundary: Integer; X0, Y0, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetBidiMode = function(const IPDF: Pointer; Mode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetBookmarkDest = function(const IPDF: Pointer; ABmk, DestType: Integer; a, b, c, d: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetBookmarkStyle = function(const IPDF: Pointer; ABmk: Integer; Style: TFStyle; RGBColor: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetBorderStyle = function(const IPDF: Pointer; Style: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetCharacterSpacing = function(const IPDF: Pointer; Value: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetCheckBoxChar = function(const IPDF: Pointer; CheckBoxChar: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetCheckBoxDefState = function(const IPDF: Pointer; AField: Cardinal; Checked: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetCheckBoxState = function(const IPDF: Pointer; AField: Cardinal; Checked: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetCIDFontA = function(const IPDF: Pointer; CMapHandle: Cardinal; const Name: PAnsiChar; Style: Integer; Size: Double; Embed: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetCIDFontW = function(const IPDF: Pointer; CMapHandle: Cardinal; const Name: PWideChar; Style: Integer; Size: Double; Embed: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetCMapDirA = function(const IPDF: Pointer; const Path: PAnsiChar; Recursive: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetCMapDirW = function(const IPDF: Pointer; const Path: PWideChar; Recursive: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetColDefFile = function(const IPDF: Pointer; EmbFile: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetColorMask = function(const IPDF: Pointer; ImageHandle: Cardinal; Mask: PInteger; Count: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetColors = function(const IPDF: Pointer; Color: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetColorSpace = function(const IPDF: Pointer; ColorSpace: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetColSortField = function(const IPDF: Pointer; ColField: Cardinal; AscendingOrder: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetCompressionFilter = function(const IPDF: Pointer; Filter: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetCompressionLevel = function(const IPDF: Pointer; CompressLevel: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetContent = function(const IPDF: Pointer; const Buffer: PAnsiChar; BufSize: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetDateTimeFormat = function(const IPDF: Pointer; TxtField: Cardinal; Fmt: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetDefBitsPerPixel = function(const IPDF: Pointer; Value: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetDocInfoA = function(const IPDF: Pointer; DInfo: TDocumentInfo; const Value: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetDocInfoW = function(const IPDF: Pointer; DInfo: TDocumentInfo; const Value: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetDocInfoExA = function(const IPDF: Pointer; DInfo: TDocumentInfo; const Key, Value: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetDocInfoExW = function(const IPDF: Pointer; DInfo: TDocumentInfo; const Key: PAnsiChar; const Value: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetDrawDirection = function(const IPDF: Pointer; Direction: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetEMFFrameDPI = function(const IPDF: Pointer; DPIX, DPIY: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetEMFPatternDistance = function(const IPDF: Pointer; Value: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetErrorMode = function(const IPDF: Pointer; ErrMode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetExtColorSpace = function(const IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetExtFillColorSpace = function(const IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetExtGState = function(const IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetExtStrokeColorSpace = function(const IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldBackColor = function(const IPDF: Pointer; AColor: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldBBox = function(const IPDF: Pointer; AField: Cardinal; var BBox: TPDFRect): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldBorderColor = function(const IPDF: Pointer; AColor: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldBorderStyle = function(const IPDF: Pointer; AField: Cardinal; Style: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldBorderWidth = function(const IPDF: Pointer; AField: Cardinal; LineWidth: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldColor = function(const IPDF: Pointer; AField: Cardinal; ColorType, CS: Integer; Color: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldExpValue = function(const IPDF: Pointer; AField, ValIndex: Cardinal; const Value, ExpValue: PAnsiChar; Selected: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldExpValueEx = function(const IPDF: Pointer; AField, ValIndex: Cardinal; Selected, DefSelected: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldFlags = function(const IPDF: Pointer; AField: Cardinal; Flags: Integer; Reset: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldFontA = function(const IPDF: Pointer; Field: Cardinal; const Name: PAnsiChar; Style: Integer; Size: Double; Embed: LongBool; CP: TCodepage): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldFontW = function(const IPDF: Pointer; Field: Cardinal; const Name: PWideChar; Style: Integer; Size: Double; Embed: LongBool; CP: TCodepage): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldFontEx = function(const IPDF: Pointer; Field, Handle: Cardinal; FontSize: double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldFontSize = function(const IPDF: Pointer; AField: Cardinal; FontSize: double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldHighlightMode = function(const IPDF: Pointer; AField: Cardinal; Mode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldIndex = function(const IPDF: Pointer; AField, Index: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldMapNameA = function(const IPDF: Pointer; AField: Cardinal; const Name: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldMapNameW = function(const IPDF: Pointer; AField: Cardinal; const Name: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldName = function(const IPDF: Pointer; AField: Cardinal; const NewName: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldOrientation = function(const IPDF: Pointer; AField: Cardinal; Value: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldTextAlign = function(const IPDF: Pointer; AField: Cardinal; Align: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldTextColor = function(const IPDF: Pointer; Color: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldToolTipA = function(const IPDF: Pointer; AField: Cardinal; const Value: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFieldToolTipW = function(const IPDF: Pointer; AField: Cardinal; const Value: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFillColor = function(const IPDF: Pointer; Color: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFillColorEx = function(const IPDF: Pointer; const Color: PByte; NumComponents: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFillColorF = function(const IPDF: Pointer; const Color: PSingle; NumComponents: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFillColorSpace = procedure(const IPDF: Pointer; CS: Integer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFontA = function(const IPDF: Pointer; const Name: PAnsiChar; Style: TFStyle; Size: Double; Embed: LongBool; CP: TCodepage): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFontW = function(const IPDF: Pointer; const Name: PWideChar; Style: TFStyle; Size: Double; Embed: LongBool; CP: TCodepage): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFontExA = function(const IPDF: Pointer; const Name: PAnsiChar; Style: Integer; Size: Double; Embed: LongBool; CP: TCodepage): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFontExW = function(const IPDF: Pointer; const Name: PWideChar; Style: Integer; Size: Double; Embed: LongBool; CP: TCodepage): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFontOrigin = function(const IPDF: Pointer; Origin: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFontSearchOrder = procedure(const IPDF: Pointer; Order: PFontBaseType{Array[0..3] of TFontBaseType}); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFontSearchOrderEx = procedure(const IPDF: Pointer; S1, S2, S3, S4: TFontBaseType); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFontSelMode = function(const IPDF: Pointer; Mode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetFontWeight = function(const IPDF: Pointer; Weight: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetGStateFlags = function(const IPDF: Pointer; Flags: TGStateFlags; Reset: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetIconColor = function(const IPDF: Pointer; Color: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetImportFlags = function(const IPDF: Pointer; Flags: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetImportFlags2 = function(const IPDF: Pointer; Flags: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetItalicAngle = function(const IPDF: Pointer; Angle: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetJPEGQuality = function(const IPDF: Pointer; Value: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetLanguage = function(const IPDF: Pointer; const ISOTag: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetLeading = function(const IPDF: Pointer; Value: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetLicenseKey = function(const IPDF: Pointer; const Key: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetLineAnnotParms = function(const IPDF: Pointer; Handle: Cardinal; FontHandle: Integer; FontSize: Double; const Parms: PLineAnnotParms): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetLineCapStyle = function(const IPDF: Pointer; Style: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetLineDashPattern = function(const IPDF: Pointer; const Dash: PAnsiChar; Phase: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetLineDashPatternEx = function(const IPDF: Pointer; const Dash: Pointer; NumValues: Cardinal; Phase: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetLineJoinStyle = function(const IPDF: Pointer; Style: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetLineWidth = function(const IPDF: Pointer; Value: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetLinkHighlightMode = function(const IPDF: Pointer; Mode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetListFont = function(const IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetMatrix = function(const IPDF: Pointer; var Matrix: TCTM): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetMaxErrLogMsgCount = procedure(const IPDF: Pointer; Value: Cardinal); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetMaxFieldLen = function(const IPDF: Pointer; TxtField: Cardinal; MaxLen: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetMetaConvFlags = function(const IPDF: Pointer; Flags: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetMetadata = function(const IPDF: Pointer; ObjType: TMetadataObj; Handle: Integer; const Buffer: Pointer; BufSize: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetMiterLimit = function(const IPDF: Pointer; Value: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetNeedAppearance = function(const IPDF: Pointer; Value: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetNumberFormat = function(const IPDF: Pointer; TxtField: Cardinal; Sep: Integer; DecPlaces: Cardinal; NegStyle: Integer; const CurrStr: PAnsiChar; Prepend: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetOCGContUsage = function(const IPDF: Pointer; Handle: Cardinal; var Value: TPDFOCGContUsage): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetOCGState = function(const IPDF: Pointer; Handle: Cardinal; Visible, SaveState: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetOnErrorProc = function(const IPDF: Pointer; const Data: Pointer; ErrProc: TErrorProc): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetOnPageBreakProc = function(const IPDF: Pointer; const Data: Pointer; OnBreakProc: TOnPageBreakProc): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetOpacity = function(const IPDF: Pointer; Value: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetOrientation = function(const IPDF: Pointer; Value: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetOrientationEx = function(const IPDF: Pointer; Value: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetPageCoords = function(const IPDF: Pointer; PageCoords: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetPageFormat = function(const IPDF: Pointer; Value: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetPageHeight = function(const IPDF: Pointer; Value: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetPageLayout = function(const IPDF: Pointer; Layout: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetPageMode = function(const IPDF: Pointer; Mode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetPageWidth = function(const IPDF: Pointer; Value: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetPDFVersion = function(const IPDF: Pointer; Version: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetPrintSettings = function(const IPDF: Pointer; Mode: TDuplexMode; PickTrayByPDFSize: Integer; NumCopies: Cardinal; PrintScaling: TPrintScaling; const PrintRanges: PCardinal; NumRanges: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetProgressProc = function(const IPDF: Pointer; const Data: Pointer; InitProgress: TInitProgressProc; Progress: TProgressProc): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetResolution = function(const IPDF: Pointer; Value: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReplaceFontA = function(const IPDF, PDFFont: Pointer; const Name: PAnsiChar; Style: Integer; NameIsFamilyName: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReplaceFontW = function(const IPDF, PDFFont: Pointer; const Name: PWideChar; Style: Integer; NameIsFamilyName: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReplaceFontExA = function(const IPDF, PDFFont: Pointer; const FontFile: PAnsiChar; Embed: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReplaceFontExW = function(const IPDF, PDFFont: Pointer; const FontFile: PWideChar; Embed: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReplaceICCProfileA = function(const IPDF: Pointer; ColorSpace: Cardinal; const ICCFile: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfReplaceICCProfileW = function(const IPDF: Pointer; ColorSpace: Cardinal; const ICCFile: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetSaveNewImageFormat = function(const IPDF: Pointer; Value: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetSeparationInfo = function(const IPDF: Pointer; Handle: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetStrokeColor = function(const IPDF: Pointer; Color: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetStrokeColorEx = function(const IPDF: Pointer; const Color: PByte; NumComponents: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetStrokeColorF = function(const IPDF: Pointer; const Color: PSingle; NumComponents: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetStrokeColorSpace = procedure(const IPDF: Pointer; CS: Integer); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetTabLen = function(const IPDF: Pointer; TabLen: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetTextDrawMode = function(const IPDF: Pointer; Mode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetTextFieldValueA = function(const IPDF: Pointer; Field: Cardinal; const Value, DefValue: PAnsiChar; Align: TTextAlign): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetTextFieldValueW = function(const IPDF: Pointer; Field: Cardinal; const Value, DefValue: PWideChar; Align: TTextAlign): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetTextRect = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetTextRise = function(const IPDF: Pointer; Value: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetTextScaling = function(const IPDF: Pointer; Value: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetTrapped = procedure(const IPDF: Pointer; Value: LongBool); {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetTransparentColor = function(const IPDF: Pointer; AColor: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetUseExactPwd = function(const IPDF: Pointer; Value: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetUseGlobalImpFiles = function(const IPDF: Pointer; Value: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetUserUnit = function(const IPDF: Pointer; Value: Single): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetUseStdFonts = function(const IPDF: Pointer; Value: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetUseSwapFile = function(const IPDF: Pointer; SwapContents: LongBool; SwapLimit: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetUseSwapFileEx = function(const IPDF: Pointer; SwapContents: LongBool; SwapLimit: Cardinal; const SwapDir: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetUseSystemFonts = function(const IPDF: Pointer; Value: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetUseTransparency = function(const IPDF: Pointer; Value: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetUseVisibleCoords = function(const IPDF: Pointer; Value: LongBool): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetViewerPreferences = function(const IPDF: Pointer; Value, AddVal: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetWMFDefExtent = function(const IPDF: Pointer; Width, Height: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetWMFPixelPerInch = function(const IPDF: Pointer; Value: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetWordSpacing = function(const IPDF: Pointer; Value: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSkewCoords = function(const IPDF: Pointer; alpha, beta, OriginX, OriginY: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSortFieldsByIndex = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSortFieldsByName = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSquareAnnotA = function(const IPDF: Pointer; PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSquareAnnotW = function(const IPDF: Pointer; PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfStampAnnotA = function(const IPDF: Pointer; SubType: TRubberStamp; PosX, PosY, Width, Height: Double; const Author, Subject, Comment: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfStampAnnotW = function(const IPDF: Pointer; SubType: TRubberStamp; PosX, PosY, Width, Height: Double; const Author, Subject, Comment: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfStrokePath = function(IPDF: Pointer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfTextAnnotA = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; const Author, Text: PAnsiChar; Icon: TAnnotIcon; Open: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfTextAnnotW = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; const Author, Text: PWideChar; Icon: TAnnotIcon; Open: LongBool): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetTextFieldValueExA = function(const IPDF: Pointer; Field: Cardinal; const Value: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfSetTextFieldValueExW = function(const IPDF: Pointer; Field: Cardinal; const Value: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfTestGlyphsA = function(const IPDF: Pointer; FontHandle: Integer; const Text: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfTestGlyphsW = function(const IPDF: Pointer; FontHandle: Integer; const Text: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfTestGlyphsExA = function(const IPDF: Pointer; FontHandle: Integer; const Text: PAnsiChar; Len: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfTestGlyphsExW = function(const IPDF: Pointer; FontHandle: Integer; const Text: PWideChar; Len: Cardinal): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfTranslateCoords = function(const IPDF: Pointer; NewOriginX, NewOriginY: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfTriangle = function(const IPDF: Pointer; x1, y1, x2, y2, x3, y3: Double; FillMode: Integer): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfUnLockLayer = function(const IPDF: Pointer; Layer: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfUTF16ToUTF32 = function(const IPDF: Pointer; const Source: PWideChar): Pointer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfUTF16ToUTF32Ex = function(const IPDF: Pointer; const Source: PWideChar; var Len: Cardinal): Pointer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfUTF32ToUTF16 = function(const IPDF: Pointer; const Source: Pointer): PWideChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfUTF32ToUTF16Ex = function(const IPDF: Pointer; const Source: Pointer; var Len: Cardinal): PWideChar; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWebLinkA = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; const URL: PAnsiChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWebLinkW = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; const URL: PWideChar): Integer; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteAngleTextA = function(const IPDF: Pointer; const AText: PAnsiChar; Angle, PosX, PosY, Radius, YOrigin: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteAngleTextW = function(const IPDF: Pointer; const AText: PWideChar; Angle, PosX, PosY, Radius, YOrigin: Double): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteFTextA = function(const IPDF: Pointer; Align: Integer; const AText: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteFTextW = function(const IPDF: Pointer; Align: Integer; const AText: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteFTextExA = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; Align: Integer; const AText: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteFTextExW = function(const IPDF: Pointer; PosX, PosY, Width, Height: Double; Align: Integer; const AText: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteTextMatrixA = function(const IPDF: Pointer; var Matrix: TCTM; const AText: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteTextMatrixW = function(const IPDF: Pointer; var Matrix: TCTM; const AText: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteTextMatrixExA = function(const IPDF: Pointer; var M: TCTM; const AText: PAnsiChar; Len: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteTextMatrixExW = function(const IPDF: Pointer; var M: TCTM; const AText: PWideChar; Len: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteTextA = function(const IPDF: Pointer; PosX, PosY: Double; const AText: PAnsiChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteTextW = function(const IPDF: Pointer; PosX, PosY: Double; const AText: PWideChar): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteTextExA = function(const IPDF: Pointer; PosX, PosY: Double; const AText: PAnsiChar; Len: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};
   TpdfWriteTextExW = function(const IPDF: Pointer; PosX, PosY: Double; const AText: PWideChar; Len: Cardinal): LongBool; {$IFDEF MSWINDOWS}stdcall{$ELSE}cdecl{$ENDIF};

type TPDF = class(TObject)
 private
   FInstance: Pointer;
 public
 {$IFDEF MSWINDOWS}
   constructor Create(const LibName: WideString = 'dynapdf.dll'); overload;
 {$ELSE}
   {$IFDEF MACOS}
      constructor Create(const LibName: WideString = 'libdynapdf.dylib'); overload;
   {$ELSE}
      constructor Create(const LibName: WideString = 'libdynapdf.so'); overload;
   {$ENDIF}
{$ENDIF}
   destructor Destroy(); override;
   procedure Abort(RasPtr: Pointer);
   function AddActionToObj(ObjType: TObjType; Event: TObjEvent; ActHandle, ObjHandle: Cardinal): Integer;
   function AddArticle(PosX, PosY, Width, Height: Double): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function AddBookmark(const Title: AnsiString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Integer; overload;
   function AddBookmark(const Title: WideString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Integer; overload;
{$endif}
   function AddBookmarkA(const Title: AnsiString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Integer;
   function AddBookmarkW(const Title: WideString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function AddBookmarkEx(const Title: AnsiString; Parent: Integer; NamedDest: Cardinal; Open: Boolean): Integer; overload;
   function AddBookmarkEx(const Title: WideString; Parent: Integer; NamedDest: Cardinal; Open: Boolean): Integer; overload;
{$endif}
   function AddBookmarkExA(const Title: AnsiString; Parent: Integer; NamedDest: Cardinal; Open: Boolean): Integer;
   function AddBookmarkExW(const Title: WideString; Parent: Integer; NamedDest: Cardinal; Open: Boolean): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function AddBookmarkEx2(const Title: AnsiString; Parent: Integer; const NamedDest: AnsiString; Open: Boolean): Integer; overload;
   function AddBookmarkEx2(const Title: AnsiString; Parent: Integer; const NamedDest: WideString; Open: Boolean): Integer; overload;
   function AddBookmarkEx2(const Title: WideString; Parent: Integer; const NamedDest: AnsiString; Open: Boolean): Integer; overload;
   function AddBookmarkEx2(const Title: WideString; Parent: Integer; const NamedDest: WideString; Open: Boolean): Integer; overload;
{$endif}
   function AddBookmarkEx2AA(const Title: AnsiString; Parent: Integer; const NamedDest: AnsiString; Open: Boolean): Integer;
   function AddBookmarkEx2AW(const Title: AnsiString; Parent: Integer; const NamedDest: WideString; Open: Boolean): Integer;
   function AddBookmarkEx2WA(const Title: WideString; Parent: Integer; const NamedDest: AnsiString; Open: Boolean): Integer;
   function AddBookmarkEx2WW(const Title: WideString; Parent: Integer; const NamedDest: WideString; Open: Boolean): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function AddButtonImage(BtnHandle: Cardinal; State: TButtonState; const Caption, ImgFile: AnsiString): Boolean; overload;
   function AddButtonImage(BtnHandle: Cardinal; State: TButtonState; const Caption, ImgFile: WideString): Boolean; overload;
{$else}
   function AddButtonImage(BtnHandle: Cardinal; State: TButtonState; const Caption, ImgFile: AnsiString): Boolean;
{$endif}
   function AddButtonImageA(BtnHandle: Cardinal; State: TButtonState; const Caption, ImgFile: AnsiString): Boolean;
   function AddButtonImageW(BtnHandle: Cardinal; State: TButtonState; const Caption, ImgFile: WideString): Boolean;
   function AddButtonImageEx(BtnHandle: Cardinal; State: TButtonState; const Caption: AnsiString; Handle: HBITMAP): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function AddContinueText(const AText: AnsiString): Integer; overload;
   function AddContinueText(const AText: WideString): Integer; overload;
{$endif}
   function AddContinueTextA(const AText: AnsiString): Integer;
   function AddContinueTextW(const AText: WideString): Integer;
   function AddDeviceNProcessColorants(DeviceNCS: Cardinal; const Colorants: Array of AnsiString; NumColorants: Cardinal; ProcessCS: TExtColorSpace; Handle: Integer): Boolean;
   function AddDeviceNSeparations(DeviceNCS: Cardinal; const Colorants: Array of AnsiString; SeparationCS: Array of Integer; NumColorants: Cardinal): Boolean;
   function AddFieldToFormAction(Action, AField: Cardinal; Include: Boolean): Integer;
   function AddFieldToHideAction(HideAct, AField: Cardinal): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function AddFileComment(const AText: AnsiString): Boolean; overload;
   function AddFileComment(const AText: WideString): Boolean; overload;
{$endif}
   function AddFileCommentA(const AText: AnsiString): Boolean;
   function AddFileCommentW(const AText: WideString): Boolean;
   function AddFontSearchPath(const APath: AnsiString; Recursive: Boolean): Integer;
   function AddImage(Filter: TCompressionFilter; Flags: TImageConversionFlags; var Image: TPDFImage): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function AddJavaScript(const Name, Script: AnsiString): Integer; overload;
   function AddJavaScript(const Name: AnsiString; const Script: WideString): Integer; overload;
{$endif}
   function AddJavaScriptA(const Name, Script: AnsiString): Integer;
   function AddJavaScriptW(const Name: AnsiString; const Script: WideString): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function AddLayerToDisplTree(Parent: Pointer; Layer: Integer; const Title: AnsiString): Pointer; overload;
   function AddLayerToDisplTree(Parent: Pointer; Layer: Integer; const Title: WideString): Pointer; overload;
{$endif}
   function AddLayerToDisplTreeA(Parent: Pointer; Layer: Integer; const Title: AnsiString): Pointer;
   function AddLayerToDisplTreeW(Parent: Pointer; Layer: Integer; const Title: WideString): Pointer;
   function AddObjectToLayer(OCG: Cardinal; ObjType: TOCObject; Handle: Cardinal): Boolean;
   function AddOCGToAppEvent(Handle: Cardinal; Events: TOCAppEvent; Categories: TOCGUsageCategory): Boolean;
   function AddMaskImage(BaseImage: Cardinal; const Buffer: Pointer; BufSize: Cardinal; Stride: Integer; BitsPerPixel, Width, Height: Cardinal): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function AddOutputIntent(const ICCFile: AnsiString): Integer; overload;
   function AddOutputIntent(const ICCFile: WideString): Integer; overload;
{$endif}
   function AddOutputIntentA(const ICCFile: AnsiString): Integer;
   function AddOutputIntentW(const ICCFile: WideString): Integer;
   function AddOutputIntentEx(const Buffer: Pointer; BufSize: Cardinal): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function AddPageLabel(StartRange: Cardinal; Format: TPageLabelFormat; const Prefix: AnsiString; AddNum: Integer): Integer; overload;
   function AddPageLabel(StartRange: Cardinal; Format: TPageLabelFormat; const Prefix: WideString; AddNum: Integer): Integer; overload;
{$endif}
   function AddPageLabelA(StartRange: Cardinal; Format: TPageLabelFormat; const Prefix: AnsiString; AddNum: Integer): Integer;
   function AddPageLabelW(StartRange: Cardinal; Format: TPageLabelFormat; const Prefix: WideString; AddNum: Integer): Integer;
   function AddRasImage(RasPtr: Pointer; Filter: TCompressionFilter): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   // These functions were incorrectly named. Please use AddOuputIntent() instead
   function AddRenderingIntent(const ICCFile: AnsiString): Integer; overload;
   function AddRenderingIntent(const ICCFile: WideString): Integer; overload;
{$endif}
   function AddRenderingIntentA(const ICCFile: AnsiString): Integer;
   function AddRenderingIntentW(const ICCFile: WideString): Integer;
   function AddRenderingIntentEx(const Buffer: Pointer; BufSize: Cardinal): Integer;
   function AddValToChoiceFieldA(Field: Cardinal; const ExpValue, Value: AnsiString; Selected: Boolean): Boolean; 
   function AddValToChoiceFieldW(Field: Cardinal; const ExpValue: AnsiString; const Value: WideString; Selected: Boolean): Boolean; 
   function Append(): Boolean;
   function ApplyPattern(PattHandle: Integer; ColorMode: TColorMode; Color: Cardinal): Boolean;
   function ApplyShading(ShadHandle: Integer): Boolean;
   function AssociateEmbFile(DestObject: TAFDestObject; DestHandle: Integer; Relationship: TAFRelationship; EmbFile: Cardinal): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function AttachFile(const FilePath, Description: AnsiString; Compress: Boolean): Integer; overload;
   function AttachFile(const FilePath, Description: WideString; Compress: Boolean): Integer; overload;
   function AttachFileEx(const Buffer: Pointer; BufSize: Cardinal; const FileName, Description: AnsiString; Compress: Boolean): Integer; overload;
   function AttachFileEx(const Buffer: Pointer; BufSize: Cardinal; const FileName, Description: WideString; Compress: Boolean): Integer; overload;
{$endif}
   function AttachFileA(const FilePath, Description: AnsiString; Compress: Boolean): Integer;
   function AttachFileW(const FilePath, Description: WideString; Compress: Boolean): Integer;
   function AttachFileExA(const Buffer: Pointer; BufSize: Cardinal; const FileName, Description: AnsiString; Compress: Boolean): Integer;
   function AttachFileExW(const Buffer: Pointer; BufSize: Cardinal; const FileName, Description: WideString; Compress: Boolean): Integer;
   function AttachImageBuffer(RasPtr: Pointer; Rows: PPointer; Buffer: Pointer; Width, Height: Cardinal; ScanlineLen: Integer): Boolean;
   function AutoTemplate(Templ: Cardinal; PosX, PosY, Width, Height: Double): Boolean;
   function BeginClipPath(): Boolean;
   function BeginContinueText(PosX, PosY: Double): Integer;
   function BeginLayer(OCG: Cardinal): Boolean;
   function BeginPageTemplate(const Name: AnsiString; UseAutoTemplates: Boolean): Boolean;
   function BeginPattern(PatternType: TPatternType; TilingType: TTilingType; Width, Height: Double): Integer;
   function BeginTemplate(Width, Height: Double): Integer;
   function BeginTemplateEx(var BBox: TPDFRect; var Matrix: TCTM): Integer;
   function BeginTransparencyGroup(x1, y1, x2, y2: Double; Isolated, Knockout: Boolean; CS: TExtColorSpace; CSHandle: Integer): Integer;
   function Bezier_1_2_3(x1, y1, x2, y2, x3, y3: Double): Boolean;
   function Bezier_1_3(x1, y1, x3, y3: Double): Boolean;
   function Bezier_2_3(x2, y2, x3, y3: Double): Boolean;
   function BuildFamilyNameAndStyle(const IFont: Pointer; Name: PAnsiChar{Array[0..127] of AnsiChar}; var Style: TFStyle): Boolean;
   procedure CalcPagePixelSize(PagePtr: Pointer; DefScale: TPDFPageScale; Scale: Single; FrameWidth, FrameHeight: Cardinal; Flags: TRasterFlags; var Width, Height: Cardinal);
   function CalcWidthHeight(OrgWidth, OrgHeight, ScaledWidth, ScaledHeight: Double): Double;
   function ChangeAnnotNameA(Handle: Cardinal; const Name: AnsiString): Boolean;
   function ChangeAnnotNameW(Handle: Cardinal; const Name: WideString): Boolean;
   function ChangeAnnotPos(Handle: Cardinal; PosX, PosY, Width, Height: Double): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function ChangeBookmark(ABmk: Integer; const Title: AnsiString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Boolean; overload;
   function ChangeBookmark(ABmk: Integer; const Title: WideString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Boolean; overload;
{$endif}
   function ChangeBookmarkA(ABmk: Integer; const Title: AnsiString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Boolean; 
   function ChangeBookmarkW(ABmk: Integer; const Title: WideString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Boolean; 
   function ChangeFont(Handle: Integer): Boolean;
   function ChangeFontSize(Size: Double): Boolean;
   function ChangeFontStyle(Style: TFStyle): Boolean;
   function ChangeFontStyleEx(Style: TFStyle): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function ChangeJavaScript(Handle: Cardinal; const NewScript: AnsiString): Boolean; overload;
   function ChangeJavaScript(Handle: Cardinal; const NewScript: WideString): Boolean; overload;
   function ChangeJavaScriptAction(Handle: Cardinal; const NewScript: AnsiString): Boolean; overload;
   function ChangeJavaScriptAction(Handle: Cardinal; const NewScript: WideString): Boolean; overload;
   function ChangeJavaScriptName(Handle: Cardinal; const Name: AnsiString): Boolean; overload;
   function ChangeJavaScriptName(Handle: Cardinal; const Name: WideString): Boolean; overload;
{$endif}
   function ChangeJavaScriptA(Handle: Cardinal; const NewScript: AnsiString): Boolean;
   function ChangeJavaScriptW(Handle: Cardinal; const NewScript: WideString): Boolean;
   function ChangeJavaScriptActionA(Handle: Cardinal; const NewScript: AnsiString): Boolean;
   function ChangeJavaScriptActionW(Handle: Cardinal; const NewScript: WideString): Boolean;
   function ChangeJavaScriptNameA(Handle: Cardinal; const Name: AnsiString): Boolean;
   function ChangeJavaScriptNameW(Handle: Cardinal; const Name: WideString): Boolean;
   function ChangeLinkAnnot(Handle: Cardinal; const URL: AnsiString): Boolean;
   function ChangeSeparationColor(CSHandle, NewColor: Cardinal; Alternate: TExtColorSpace; AltHandle: Integer): Boolean;
   function CheckCollection(): Boolean;
   function CheckConformance(ConfType: TConformanceType; Options: TCheckOptions; UserData: Pointer; OnFontNotFound: TOnFontNotFoundProc; OnReplaceICCProfile: TOnReplaceICCProfile): Integer;
   function CheckFieldNames(): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function CircleAnnot(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: AnsiString): Integer; overload;
   function CircleAnnot(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: WideString): Integer; overload;
{$endif}
   function CircleAnnotA(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: AnsiString): Integer;
   function CircleAnnotW(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: WideString): Integer;
   function ClearAutoTemplates(): Boolean;
   procedure ClearErrorLog();
   function ClearHostFonts(): Boolean;
   function ClipPath(ClipMode: TClippingMode; FillMode: TPathFillMode): Boolean;
   function CloseAndSignFile(const CertFile, Password, Reason, Location: AnsiString): Boolean;
   function CloseAndSignFileEx(const OpenPwd, OwnerPwd: AnsiString; KeyLen: TKeyLen; Restrict: TRestrictions; const CertFile, Password, Reason, Location: AnsiString): Boolean;
   function CloseAndSignFileExt(var SigParms: TPDFSigParms): Boolean;
   function CloseFile(): Boolean;
   function CloseFileEx(const OpenPwd, OwnerPwd: AnsiString; KeyLen: TKeyLen; Restrict: TRestrictions): Boolean;
   function CloseImage(): Boolean;
   function CloseImportFile(): Boolean;
   function CloseImportFileEx(Handle: Cardinal): Boolean;
   function ClosePath(FillMode: TPathFillMode): Boolean;
   function CloseTag(): Boolean;
   function ConvColor(const Color: PDouble; NumComps: Cardinal; SourceCS: TExtColorSpace; const IColorSpace: Pointer; DestCS: TExtColorSpace): Cardinal;
   function ConvertColors(Flags: TColorConvFlags; Add: Single = 0.0): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function ConvertEMFSpool(const SpoolFile: AnsiString; LeftMargin, TopMargin: Double; Flags: TSpoolConvFlags): Integer; overload;
   function ConvertEMFSpool(const SpoolFile: WideString; LeftMargin, TopMargin: Double; Flags: TSpoolConvFlags): Integer; overload;
{$endif}
   function ConvertEMFSpoolA(const SpoolFile: AnsiString; LeftMargin, TopMargin: Double; Flags: TSpoolConvFlags): Integer;
   function ConvertEMFSpoolW(const SpoolFile: WideString; LeftMargin, TopMargin: Double; Flags: TSpoolConvFlags): Integer;
   function ComputeBBox(var BBox: TPDFRect; Flags: TCompBBoxFlags): Integer;
   function ConvToUnicode(const AString: AnsiString; CP: TCodepage): WideString;
   function Create3DAnnot(PosX, PosY, Width, Height: Double; const Author, AnnotName, U3DFile, ImageFile: AnsiString): Integer;
   function Create3DBackground(const IView: Pointer; BackColor: Cardinal): Boolean;
   function Create3DGotoViewAction(Base3DAnnot: Cardinal; const IView: Pointer; Named: T3DNamedAction): Integer;
   function Create3DProjection(const IView: Pointer; ProjType: T3DProjType; ScaleType: T3DScaleType; Diameter, FOV: Double): Boolean;
   function Create3DView(Base3DAnnot: Cardinal; const Name: AnsiString; SetAsDefault: Boolean; Matrix: PDouble; CamDistance: Double; RM: T3DRenderingMode; LS: T3DLightingSheme): Pointer;
   function CreateAnnotAP(Annot: Cardinal): Integer;
   function CreateArticleThreadA(const ThreadName: AnsiString): Integer;
   function CreateArticleThreadW(const ThreadName: WideString): Integer; 
   function CreateAxialShading(sX, sY, eX, eY, SCenter: Double; SColor, EColor: Cardinal; Extend1, Extend2: Boolean): Integer;
   function CreateBarcodeField(const Name: AnsiString; Parent: Integer; PosX, PosY, Width, Height: Double; var Barcode: TPDFBarcode): Integer;
   function CreateButtonA(const Name, Caption: AnsiString; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
   function CreateButtonW(const Name: AnsiString; const Caption: WideString; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
   function CreateCheckBox(const Name, ExpValue: AnsiString; Checked: Boolean; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
   function CreateCIEColorSpace(Base: TExtColorSpace; WhitePoint, BlackPoint, Gamma, Matrix: PSingle): Integer;
   function CreateCollection(View: TColView): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function CreateCollectionField(ColType: TColColumnType; Column: Integer; const Name, Key: AnsiString; Visible, Editable: Boolean): Integer; overload;
   function CreateCollectionField(ColType: TColColumnType; Column: Integer; const Name: WideString; const Key: AnsiString; Visible, Editable: Boolean): Integer; overload;
{$endif}
   function CreateCollectionFieldA(ColType: TColColumnType; Column: Integer; const Name, Key: AnsiString; Visible, Editable: Boolean): Integer;
   function CreateCollectionFieldW(ColType: TColColumnType; Column: Integer; const Name: WideString; const Key: AnsiString; Visible, Editable: Boolean): Integer;
   function CreateColItemDate(EmbFile: Cardinal; const Key: AnsiString; Date: LongInt; const Prefix: AnsiString): Boolean;
   function CreateColItemNumber(EmbFile: Cardinal; const Key: AnsiString; Value: Double; const Prefix: AnsiString): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function CreateColItemString(EmbFile: Cardinal; const Key, Value, Prefix: AnsiString): Boolean; overload;
   function CreateColItemString(EmbFile: Cardinal; const Key: AnsiString; const Value, Prefix: WideString): Boolean; overload;
{$endif}
   function CreateColItemStringA(EmbFile: Cardinal; const Key, Value, Prefix: AnsiString): Boolean;
   function CreateColItemStringW(EmbFile: Cardinal; const Key: AnsiString; const Value, Prefix: WideString): Boolean;
   function CreateComboBox(const Name: AnsiString; Sort: Boolean; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
   function CreateDeviceNColorSpace(const Colorants: Array of PAnsiChar; NumColorants: Cardinal; const PostScriptFunc: AnsiString; Alternate: TExtColorSpace; Handle: Integer): Integer;
   function CreateExtGState(var GS: TPDFExtGState): Integer;
   function CreateGoToAction(DestType: TDestType; PageNum: Cardinal; a, b, c, d: Double): Integer;
   function CreateGoToActionEx(NamedDest: Cardinal): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function CreateGoToEAction(Location: TEmbFileLocation; const Source: AnsiString; SrcPage: Cardinal; const Target, DestName: AnsiString; DestPage: Cardinal; NewWindow: Boolean): Integer; overload;
   function CreateGoToEAction(Location: TEmbFileLocation; const Source: WideString; SrcPage: Cardinal; const Target, DestName: WideString; DestPage: Cardinal; NewWindow: Boolean): Integer; overload;
   function CreateGoToRAction(const FileName: AnsiString; PageNum: Cardinal): Integer; overload;
   function CreateGoToRAction(const FileName: WideString; PageNum: Cardinal): Integer; overload;
{$else}
   function CreateGoToRAction(const FileName: AnsiString; PageNum: Cardinal): Integer;
{$endif}
   function CreateGoToEActionA(Location: TEmbFileLocation; const Source: AnsiString; SrcPage: Cardinal; const Target, DestName: AnsiString; DestPage: Cardinal; NewWindow: Boolean): Integer;
   function CreateGoToEActionW(Location: TEmbFileLocation; const Source: WideString; SrcPage: Cardinal; const Target, DestName: WideString; DestPage: Cardinal; NewWindow: Boolean): Integer;
   function CreateGoToRActionW(const FileName: WideString; PageNum: Cardinal): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function CreateGoToRActionEx(const FileName: AnsiString; DestName: AnsiString; NewWindow: Boolean): Integer; overload;
   function CreateGoToRActionEx(const FileName: AnsiString; DestName: WideString; NewWindow: Boolean): Integer; overload;
   function CreateGoToRActionExU(const FileName: WideString; DestName: AnsiString; NewWindow: Boolean): Integer; overload;
   function CreateGoToRActionExU(const FileName: WideString; DestName: WideString; NewWindow: Boolean): Integer; overload;
{$endif}
   function CreateGoToRActionExA(const FileName: AnsiString; DestName: AnsiString; NewWindow: Boolean): Integer;
   function CreateGoToRActionExW(const FileName: AnsiString; DestName: WideString; NewWindow: Boolean): Integer;
   function CreateGoToRActionExUA(const FileName: WideString; DestName: AnsiString; NewWindow: Boolean): Integer;
   function CreateGoToRActionExUW(const FileName: WideString; DestName: WideString; NewWindow: Boolean): Integer;
   function CreateGroupField(const Name: AnsiString; Parent: Integer): Integer;
   function CreateHideAction(AField: Cardinal; Hide: Boolean): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function CreateICCBasedColorSpace(const ICCProfile: AnsiString): Integer; overload;
   function CreateICCBasedColorSpace(const ICCProfile: WideString): Integer; overload;
   function CreateImage(const FileName: AnsiString; Format: TImageFormat): Boolean; overload;
   function CreateImage(const FileName: WideString; Format: TImageFormat): Boolean; overload;
   function CreateImpDataAction(const DataFile: AnsiString): Integer; overload;
   function CreateImpDataAction(const DataFile: WideString): Integer; overload;
{$endif}
   function CreateICCBasedColorSpaceA(const ICCProfile: AnsiString): Integer;
   function CreateICCBasedColorSpaceW(const ICCProfile: WideString): Integer;
   function CreateICCBasedColorSpaceEx(const Buffer: Pointer; BufSize: Cardinal): Integer;
   function CreateImageA(const FileName: AnsiString; Format: TImageFormat): Boolean;
   function CreateImageW(const FileName: WideString; Format: TImageFormat): Boolean;
   function CreateImpDataActionA(const DataFile: AnsiString): Integer;
   function CreateImpDataActionW(const DataFile: WideString): Integer;
   function CreateIndexedColorSpace(Base: TExtColorSpace; Handle: Integer; const ColorTable: Pointer; NumColors: Cardinal): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function CreateJSAction(const Script: AnsiString): Integer; overload;
   function CreateJSAction(const Script: WideString): Integer; overload;
   function CreateLaunchActionEx(const FileName: AnsiString; NewWindow: Boolean): Integer; overload;
   function CreateLaunchActionEx(const FileName: WideString; NewWindow: Boolean): Integer; overload;
{$endif}
   function CreateJSActionA(const Script: AnsiString): Integer;
   function CreateJSActionW(const Script: WideString): Integer;
   function CreateLaunchAction(OP: TFileOP; const FileName, DefDir, Param: AnsiString; NewWindow: Boolean): Integer;
   function CreateLaunchActionExA(const FileName: AnsiString; NewWindow: Boolean): Integer;
   function CreateLaunchActionExW(const FileName: WideString; NewWindow: Boolean): Integer;
   function CreateListBox(const Name: PAnsiChar; Sort: Boolean; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
   function CreateNamedAction(Action: TNamedAction): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function CreateNamedDest(const Name: AnsiString; DestPage: Cardinal; DestType: TDestType; a, b, c, d: Double): Integer; overload;
   function CreateNamedDest(const Name: WideString; DestPage: Cardinal; DestType: TDestType; a, b, c, d: Double): Integer; overload;
   function CreateNewPDF(const OutPDF: AnsiString): Boolean; overload;
   function CreateNewPDF(const OutPDF: WideString): Boolean; overload;
   function CreateOCG(const Name: AnsiString; DisplayInUI, Visible: Boolean; Intent: TOCGIntent): Integer; overload;
   function CreateOCG(const Name: WideString; DisplayInUI, Visible: Boolean; Intent: TOCGIntent): Integer; overload;
{$endif}
   function CreateNamedDestA(const Name: AnsiString; DestPage: Cardinal; DestType: TDestType; a, b, c, d: Double): Integer;
   function CreateNamedDestW(const Name: WideString; DestPage: Cardinal; DestType: TDestType; a, b, c, d: Double): Integer;
   function CreateNewPDFA(const OutPDF: AnsiString): Boolean;
   function CreateNewPDFW(const OutPDF: WideString): Boolean;
   function CreateOCGA(const Name: AnsiString; DisplayInUI, Visible: Boolean; Intent: TOCGIntent): Integer;
   function CreateOCGW(const Name: WideString; DisplayInUI, Visible: Boolean; Intent: TOCGIntent): Integer;
   function CreateOCMD(Visibility: TOCVisibility; const OCGs: PCardinal; Count: Cardinal): Integer;
   function CreateRadialShading(sX, sY, R1, eX, eY, R2, SCenter: Double; SColor, EColor: Cardinal; Extend1, Extend2: Boolean): Integer;
   function CreateRadioButton(const Name, ExpValue: AnsiString; Checked: Boolean; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
   function CreateRasterizer(Rows: PPointer; Buffer: Pointer; Width, Height: Cardinal; ScanlineLen: Integer; PixelFormat: TPDFPixFormat): Pointer;
   function CreateRasterizerEx(DC: HDC; Width, Height: Cardinal; PixFmt: TPDFPixFormat): Pointer;
   function CreateResetAction(): Integer;
   function CreateSeparationCS(const Colorant: AnsiString; Alternate: TExtColorSpace; Handle: Integer; Color: Cardinal): Integer;
   function CreateSetOCGStateAction(const On_: PCardinal; OnCount: Cardinal; const Off: PCardinal; OffCount: Cardinal; const Toggle: PCardinal; ToggleCount: Cardinal; PreserveRB: Boolean): Integer;
   function CreateSigField(const Name: AnsiString; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
   function CreateSigFieldAP(SigField: Cardinal): Integer;
   function CreateSoftMask(TranspGroup: Cardinal; MaskType: TSoftMaskType; BackColor: Cardinal): Pointer;
   function CreateStdPattern(Pattern: TStdPattern; LineWidth, Distance: Double; LineColor, BackColor: Cardinal): Integer;
   function CreateStructureTree(): Boolean;
   function CreateSubmitAction(Flags: TSubmitFlags; const URL: AnsiString): Integer;
   function CreateTextField(const Name: AnsiString; Parent: Integer; Multiline: Boolean; MaxLen: Integer; PosX, PosY, Width, Height: Double): Integer;
   function CreateURIAction(const URL: AnsiString): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function DecryptPDF(const FileName: AnsiString; PwdType: TPwdType; const Password: AnsiString): Integer; overload;
   function DecryptPDF(const FileName: WideString; PwdType: TPwdType; const Password: AnsiString): Integer; overload;
{$endif}
   function DecryptPDFA(const FileName: AnsiString; PwdType: TPwdType; const Password: AnsiString): Integer; 
   function DecryptPDFW(const FileName: WideString; PwdType: TPwdType; const Password: AnsiString): Integer; 
   procedure DeleteAcroForm();
   function DeleteActionFromObj(ObjType: TObjType; ActHandle, ObjHandle: Cardinal): Integer;
   function DeleteActionFromObjEx(ObjType: TObjType; ObjHandle, ActIndex: Cardinal): Integer;
   function DeleteAnnotation(Handle: Cardinal): Boolean;
   function DeleteBookmark(ABmk: Cardinal): Integer;
   function DeleteEmbeddedFile(Handle: Cardinal): Boolean;
   function DeleteField(AField: Cardinal): Integer;
   function DeleteFieldEx(const Name: AnsiString): Integer;
   procedure DeleteJavaScripts(DelJavaScriptActions: Boolean);
   function DeleteOCGFromAppEvent(Handle: Cardinal; Events: TOCAppEvent; Categories: TOCGUsageCategory; DelCategoryOnly: Boolean): Boolean;
   function DeleteOutputIntent(Index: Integer): Integer;
   function DeletePage(PageNum: Cardinal): Integer;
   procedure DeletePageLabels();
   procedure DeleteRasterizer(RasPtr: PPointer);
   function DeleteSeparationInfo(AllPages: Boolean): Boolean;
   function DeleteTemplate(Handle: Cardinal): Boolean;
   function DeleteTemplateEx(Index: Cardinal): Integer;
   procedure DeleteXFAForm();
   function DrawArc(PosX, PosY, Radius, StartAngle, EndAngle: Double): Boolean;
   function DrawArcEx(PosX, PosY, Width, Height, StartAngle, EndAngle: Double): Boolean;
   function DrawChord(PosX, PosY, Width, Height, StartAngle, EndAngle: Double; FillMode: TPathFillMode): Boolean;
   function DrawCircle(PosX, PosY, Radius: Double; FillMode: TPathFillMode): Boolean;
   function DrawPie(PosX, PosY, Width, Height, StartAngle, EndAngle: Double; FillMode: TPathFillMode): Boolean;
   function EditPage(PageNum: Integer): Boolean;
   function EditTemplate(Index: Cardinal): Boolean;
   function EditTemplate2(Handle: Cardinal): Boolean;
   function Ellipse(PosX, PosY, Width, Height: Double; FillMode: TPathFillMode): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function EncryptPDF(const FileName, OpenPwd, OwnerPwd: AnsiString; KeyLen: TKeyLen; Restrict: TRestrictions): Integer; overload;
   function EncryptPDF(const FileName: WideString; const OpenPwd, OwnerPwd: AnsiString; KeyLen: TKeyLen; Restrict: TRestrictions): Integer; overload;
{$endif}
   function EncryptPDFA(const FileName, OpenPwd, OwnerPwd: AnsiString; KeyLen: TKeyLen; Restrict: TRestrictions): Integer; 
   function EncryptPDFW(const FileName: WideString; const OpenPwd, OwnerPwd: AnsiString; KeyLen: TKeyLen; Restrict: TRestrictions): Integer; 
   function EndContinueText(): Integer;
   function EndLayer(): Boolean;
   function EndPage(): Boolean;
   function EndPattern(): Boolean;
   function EndTemplate(): Boolean;
   function EnumDocFonts(const Data: Pointer; EnumProc: TEnumFontProc2): Integer;
   function EnumHostFonts(const Data: Pointer; EnumProc: TEnumFontProc): Integer;
   function EnumHostFontsEx(const Data: Pointer; EnumProc: TEnumFontProcEx): Integer;
   function ExchangeBookmarks(Bmk1, Bmk2: Integer): Boolean;
   function ExchangePages(First, Second: Cardinal): Boolean;
   function FileAttachAnnotA(PosX, PosY: Double; Icon: TFileAttachIcon; const Author, Desc, AFile: AnsiString; Compress: Boolean): Integer;
   function FileAttachAnnotW(PosX, PosY: Double; Icon: TFileAttachIcon; const Author, Desc, AFile: WideString; Compress: Boolean): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function FileLink(PosX, PosY, Width, Height: Double; const AFilePath: AnsiString): Integer; overload;
   function FileLink(PosX, PosY, Width, Height: Double; const AFilePath: WideString): Integer; overload;
{$endif}
   function FileLinkA(PosX, PosY, Width, Height: Double; const AFilePath: AnsiString): Integer; 
   function FileLinkW(PosX, PosY, Width, Height: Double; const AFilePath: WideString): Integer; 
   function FindBookmarkA(DestPage: Integer; const Title: AnsiString): Integer; 
   function FindBookmarkW(DestPage: Integer; const Title: WideString): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function FindField(const Name: AnsiString): Integer; overload;
   function FindField(const Name: WideString): Integer; overload;
{$endif}
   function FindFieldA(const Name: AnsiString): Integer;
   function FindFieldW(const Name: WideString): Integer;
   function FindLinkAnnot(const URL: AnsiString): Integer;
   function FindNextBookmark(): Integer;
   function FinishSignature(const PKCS7Obj: Pointer; Length: Cardinal): Boolean;
   function FlattenAnnots(Flags: TAnnotFlattenFlags): Integer;
   function FlattenForm(): Boolean;
   function FlushPageContent(var Stack: TPDFStack): Boolean;
   function FlushPages(Flags: TFlushPageFlags): Boolean;
   procedure FreeImageBuffer();
   function FreeImageObj(Handle: Cardinal): Boolean;
   function FreeImageObjEx(const ImagePtr: Pointer): Boolean;
   function FreePDF(): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function FreeTextAnnot(PosX, PosY, Width, Height: Double; const Author, AText: AnsiString; Align: TTextAlign): Integer; overload;
   function FreeTextAnnot(PosX, PosY, Width, Height: Double; const Author, AText: WideString; Align: TTextAlign): Integer; overload;
{$endif}
   function FreeTextAnnotA(PosX, PosY, Width, Height: Double; const Author, AText: AnsiString; Align: TTextAlign): Integer; 
   function FreeTextAnnotW(PosX, PosY, Width, Height: Double; const Author, AText: WideString; Align: TTextAlign): Integer;
   function FreeUniBuf(): Boolean;
   function GetActionCount(): Integer;
   function GetActionType(ActHandle: Cardinal): Integer;
   function GetActionTypeEx(ObjType: TObjType; ObjHandle, ActIndex: Cardinal): Integer;
   function GetActiveFont(): Integer;
   function GetAllocBy(): Integer;
   function GetAnnot(Handle: Cardinal; var Annot: TPDFAnnotation): Boolean;
   function GetAnnotBBox(Handle: Cardinal; var BBox: TPDFRect): Boolean;
   function GetAnnotCount(): Integer;
   function GetAnnotEx(Handle: Cardinal; var Annot: TPDFAnnotationEx): Boolean;
   function GetAnnotFlags(): Integer;
   function GetAnnotLink(Handle: Cardinal): AnsiString;
   function GetAnnotType(Handle: Cardinal): Integer;
   function GetAscent(): Double;
   function GetBarcodeDict(const IBarcode: Pointer; var Barcode: TPDFBarcode): Boolean;
   function GetBBox(Boundary: TPageBoundary; var BBox: TPDFRect): Boolean;
   function GetBidiMode(): TPDFBidiMode;
   function GetBookmark(Handle: Integer; var Bmk: TBookmark): Boolean;
   function GetBookmarkCount(): Integer;
   function GetBorderStyle(): TBorderStyle;
   function GetBuffer(var BufSize: Cardinal): PAnsiChar;
   function GetCapHeight(): Double;
   function GetCharacterSpacing(): Double;
   function GetCheckBoxChar(): Integer;
   function GetCheckBoxCharEx(AField: Cardinal): Integer;
   function GetCheckBoxDefState(AField: Cardinal): Integer;
   function GetCMap(Index: Cardinal; var CMap: TPDFCMap): Boolean;
   function GetCMapCount(): Integer;
   function GetColorSpace(): Integer;
   function GetColorSpaceCount(): Integer;
   function GetColorSpaceObj(Handle: Cardinal; var CS: TPDFColorSpaceObj): Boolean;
   function GetColorSpaceObjEx(const IColorSpace: Pointer; var CS: TPDFColorSpaceObj): Boolean;
   function GetCompressionFilter(): Integer;
   function GetCompressionLevel(): Integer;
   function GetContent(var Buffer: PAnsiChar): Integer;
   function GetDefBitsPerPixel(): Integer;
   function GetDescent(): Double;
   function GetDeviceNAttributes(IAttributes: Pointer; var Attributes: TDeviceNAttributes): Boolean;
   function GetDocInfo(DInfo: TDocumentInfo; var Value: PWideChar): Integer;
   function GetDocInfoCount(): Integer;
   function GetDocInfoEx(Index: Cardinal; var DInfo: TDocumentInfo; var Key, Value: PAnsiChar; var Unicode: LongBool): Integer;
   function GetDocUsesTransparency(Flags: Cardinal): Boolean;
   function GetDrawDirection(): Integer;
   function GetDynaPDFVersion(): AnsiString;
   function GetEmbeddedFile(Handle: Cardinal; var FileSpec: TPDFFileSpec; Decompress: Boolean): Boolean;
   function GetEmbeddedFileCount(): Integer;
   function GetEMFPatternDistance(): Double;
   function GetErrLogMessage(Index: Cardinal; var Err: TPDFError): Boolean;
   function GetErrLogMessageCount(): Integer;
   function GetErrorMessage(): AnsiString;
   function GetErrorMode(): Integer;
   function GetField(Handle: Cardinal; var Field: TPDFField): Boolean;
   function GetFieldBackColor(): Cardinal;
   function GetFieldBorderColor(): Cardinal;
   function GetFieldBorderStyle(AField: Cardinal): Integer;
   function GetFieldBorderWidth(AField: Cardinal): Double;
   function GetFieldChoiceValue(AField, ValIndex: Cardinal; var Value: TPDFChoiceValue): Boolean;
   function GetFieldColor(AField: Cardinal; ColorType: TFieldColor; var ColorSpace: Integer; var Color: Cardinal): Boolean;
   function GetFieldCount(): Integer;
   function GetFieldEx(Handle: Cardinal; var Field: TPDFFieldEx): Boolean;
   function GetFieldEx2(const IField: Pointer; var Field: TPDFFieldEx): Boolean;
   function GetFieldExpValCount(AField: Cardinal): Integer;
   function GetFieldExpValue(AField: Cardinal; var Value: AnsiString): Integer;
   function GetFieldExpValueEx(AField, ValIndex: Cardinal; var Value, ExpValue: AnsiString; var Selected: Boolean): Boolean;
   function GetFieldFlags(AField: Cardinal): Integer;
   function GetFieldGroupType(AField: Cardinal): Integer;
   function GetFieldHighlightMode(AField: Cardinal): Integer;
   function GetFieldIndex(AField: Cardinal): Integer;
   function GetFieldMapName(AField: Cardinal; var Value: Pointer; var Unicode: LongBool): Integer;
   function GetFieldName(AField: Cardinal; var Name: AnsiString): Integer;
   function GetFieldOrientation(AField: Cardinal): Integer;
   function GetFieldTextAlign(AField: Cardinal): Integer;
   function GetFieldTextColor(): Cardinal;
   function GetFieldToolTip(AField: Cardinal; var Value: Pointer; var Unicode: LongBool): Integer;
   function GetFieldType(AField: Cardinal): Integer;
   function GetFillColor(): Cardinal;
   function GetFont(const IFont: Pointer; var F: TPDFFontObj): Boolean;
   function GetFontInfo(const IFont: Pointer; var F: TPDFFontInfo): Boolean;
   function GetFontCount(): Integer;
   function GetFontEx(Handle: Cardinal; var F: TPDFFontObj): Boolean;
   function GetFontInfoEx(Handle: Cardinal; var F: TPDFFontInfo): Boolean;
   function GetFontOrigin(): Integer;
   procedure GetFontSearchOrder(Order: PFontBaseType {Array[0..3] of TFontBaseType});
   function GetFontSelMode(): Integer;
   function GetFontWeight(): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function GetFTextHeight(Align: TTextAlign; const AText: AnsiString): Double; overload;
   function GetFTextHeight(Align: TTextAlign; const AText: WideString): Double; overload;
   function GetFTextHeightEx(Width: Double; Align: TTextAlign; const AText: AnsiString): Double; overload;
   function GetFTextHeightEx(Width: Double; Align: TTextAlign; const AText: WideString): Double; overload;
{$endif}
   function GetFTextHeightA(Align: TTextAlign; const AText: AnsiString): Double;
   function GetFTextHeightW(Align: TTextAlign; const AText: WideString): Double;
   function GetFTextHeightExA(Width: Double; Align: TTextAlign; const AText: AnsiString): Double;
   function GetFTextHeightExW(Width: Double; Align: TTextAlign; const AText: WideString): Double;
   function GetGStateFlags(): Integer;
   function GetIconColor(): Integer;
   function GetImageBuffer(var BufSize: Cardinal): PAnsiChar;
{$ifdef DELPHI6_OR_HIGHER}
   function GetImageCount(const FileName: AnsiString): Integer; overload;
   function GetImageCount(const FileName: WideString): Integer; overload;
{$else}
   function GetImageCount(const FileName: AnsiString): Integer;
{$endif}
   function GetImageCountW(const FileName: WideString): Integer;
   function GetImageCountEx(const Buffer: Pointer; BufSize: Cardinal): Integer;
   function GetImageHeight(Handle: Cardinal): Integer;
   function GetImageObj(Handle: Cardinal; Flags: TParseFlags; var Image: TPDFImage): Boolean;
   function GetImageObjCount(): Integer;
   function GetImageObjEx(const ImagePtr: Pointer; Flags: TParseFlags; var Image: TPDFImage): Boolean;
   function GetImageWidth(Handle: Cardinal): Integer;
   function GetImportFlags(): Cardinal;
   function GetInBBox(PageNum: Cardinal; Boundary: TPageBoundary; var BBox: TPDFRect): Boolean;
   function GetInDocInfo(DInfo: TDocumentInfo; var Value: PWideChar): Integer;
   function GetInDocInfoCount(): Integer;
   function GetInDocInfoEx(Index: Cardinal; var DInfo: TDocumentInfo; var Key, Value: PAnsiChar; var Unicode: LongBool): Boolean;
   function GetInEncryptionFlags(): Integer;
   function GetInFieldCount(): Integer;
   function GetInIsCollection(): Boolean;
   function GetInIsEncrypted(): Boolean;
   function GetInIsSigned(): Boolean;
   function GetInIsTrapped(): Boolean;
   function GetInIsXFAForm(): Boolean;
   function GetInMetadata(PageNum: Integer; var Buffer: PByte; var BufSize: Cardinal): Boolean;
   function GetInOrientation(PageNum: Integer): Integer;
   function GetInPageCount(): Integer;
   function GetInPDFVersion(): Integer;
   function GetInPrintSettings(var Settings: TPDFPrintSettings): Boolean;
   function GetInRepairMode(): Boolean;
   function GetIsFixedPitch(): Integer;
   function GetIsTaggingEnabled(): Boolean;
   function GetItalicAngle(): Double;
   function GetJavaScript(Handle: Cardinal; var Len: Cardinal; var Unicode: LongBool): PAnsiChar;
   function GetJavaScriptAction(Handle: Cardinal; var Len: Cardinal; var Unicode: LongBool): PAnsiChar;
   function GetJavaScriptAction2(ObjType: TObjType; ObjHandle, ActIndex: Cardinal; var Len: Cardinal; var Unicode: LongBool; var Event: TObjEvent): PAnsiChar;
   function GetJavaScriptCount(): Integer;
   function GetJavaScriptEx(const Name: AnsiString; var Len: Cardinal; var Unicode: LongBool): PAnsiChar;
   function GetJavaScriptName(Handle: Cardinal; var Len: Cardinal; var Unicode: LongBool): PAnsiChar;
   function GetJPEGQuality(): Integer;
   function GetLanguage(): AnsiString;
   function GetLastTextPosX(): Double;
   function GetLastTextPosY(): Double;
   function GetLeading(): Double;
   function GetLibHandle(): HMODULE;
   function GetLineCapStyle(): Integer;
   function GetLineJoinStyle(): Integer;
   function GetLineWidth(): Double;
   function GetLinkHighlightMode(): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function GetLogMetafileSize(const FileName: AnsiString; var R: TRectL): Boolean; overload;
   function GetLogMetafileSize(const FileName: WideString; var R: TRectL): Boolean; overload;
{$else}
   function GetLogMetafileSize(const FileName: AnsiString; var R: TRectL): Boolean;
{$endif}
   function GetLogMetafileSizeA(const FileName: AnsiString; var R: TRectL): Boolean;
   function GetLogMetafileSizeW(const FileName: WideString; var R: TRectL): Boolean;
   function GetLogMetafileSizeEx(const Buffer: Pointer; BufSize: Cardinal; var R: TRectL): Boolean;
   function GetMeasureObj(const MeasurePtr: IMSR; var Value: TPDFMeasure): Boolean;
   function GetMatrix(var M: TCTM): Boolean;
   function GetMaxFieldLen(TxtField: Cardinal): Integer;
   function GetMetaConvFlags(): Cardinal;
   function GetMetadata(ObjType: TMetadataObj; Handle: Integer; var Buffer: PByte; var BufSize: Cardinal): Boolean;
   function GetMissingGlyphs(var Count: Cardinal): PCardinal;
   function GetMiterLimit(): Double;
   function GetNamedDest(Index: Cardinal; var Dest: TPDFNamedDest): Boolean;
   function GetNamedDestCount(): Integer;
   function GetNeedAppearance(): Boolean;
   function GetNumberFormatObj(const NumberFmtPtr: INFM; var Value: TPDFNumberFormat): Boolean;
   function GetObjActionCount(ObjType: TObjType; ObjHandle: Cardinal): Integer;
   function GetOCG(Handle: Cardinal; var Value: TPDFOCG): Boolean;
   function GetOCGContUsage(Handle: Cardinal; var Value: TPDFOCGContUsage): Boolean;
   function GetOCGCount(): Integer;
   function GetOCGUsageUserName(Handle, Index: Cardinal; var NameA: PAnsiChar; var NameW: PWideChar): Boolean;
   function GetOpacity(): Double;
   function GetOrientation(): Integer;
   function GetOutputIntent(Index: Cardinal; var Intent: TPDFOutputIntent): Boolean;
   function GetOutputIntentCount(): Integer;
   function GetPageAnnot(Index: Cardinal; var Annot: TPDFAnnotation): Boolean;
   function GetPageAnnotCount(): Integer;
   function GetPageAnnotEx(Index: Cardinal; var Annot: TPDFAnnotationEx): Boolean;
   function GetPageBBox(PagePtr: Pointer; Boundary: TPageBoundary; var BBox: TFltRect): Boolean;
   function GetPageCoords(): TPageCoord;
   function GetPageCount(): Integer;
   function GetPageField(Index: Cardinal; var Field: TPDFField): Boolean;
   function GetPageFieldCount(): Integer;
   function GetPageFieldEx(Index: Cardinal; var Field: TPDFFieldEx): Boolean;
   function GetPageHeight(): Double;
   function GetPageLabel(Index: Cardinal; var Lbl: TPDFPageLabel): Boolean;
   function GetPageLabelCount(): Integer;
   function GetPageLayout(): Integer;
   function GetPageMode(): Integer;
   function GetPageNum(): Integer;
   function GetPageObject(PageNum: Cardinal): Pointer;
   function GetPageOrientation(PagePtr: Pointer): Integer;
   function GetPageText(var Stack: TPDFStack): Boolean;
   function GetPageWidth(): Double;
   function GetPDFVersion(): Integer;
   function GetPrintSettings(var Settings: TPDFPrintSettings): Boolean;
   function GetPtDataArray(const PtData: IPTD; Index: Cardinal; var DataType: PAnsiChar; var Values: PSingle; var ValCount: Cardinal): Boolean;
   function GetPtDataObj(const PtData: IPTD; var Subtype: PAnsiChar; var NumArrays: Cardinal): Boolean;
   function GetResolution(): Integer;
   function GetSaveNewImageFormat(): Integer;
   function GetSeparationInfo(var Colorant: AnsiString; var CS: TExtColorSpace): Boolean;
   function GetSigDict(const ISignature: Pointer; var SigDict: TPDFSigDict): Boolean;
   function GetSpaceWidth(const IFont: Pointer; FontSize: Double): Double;
   function GetStrokeColor(): Cardinal;
   function GetSysFontInfo(Handle: Cardinal; var Value: TPDFSysFont): Integer;
   function GetTabLen(): Integer;
   function GetTemplCount(): Integer;
   function GetTemplHandle(): Integer;
   function GetTemplHeight(TmplHandle: Integer): Double;
   function GetTemplWidth(TmplHandle: Integer): Double;
   function GetTextDrawMode(): Integer;
   function GetTextFieldValue(AField: Cardinal; var Value: PAnsiChar; var ValIsUnicode: LongBool; var DefValue: PAnsiChar; var DefValIsUnicode: LongBool): Boolean;
   function GetTextRect(var PosX, PosY, Width, Height: Double): Boolean;
   function GetTextRise(): Double;
   function GetTextScaling(): Double;
{$ifdef DELPHI6_OR_HIGHER}
   function GetTextWidth(const IFont: Pointer; const Text: PAnsiChar; Len: Cardinal; CharSpacing, WordSpacing, TextScale: Single): Double; overload;
   function GetTextWidth(const AText: AnsiString): Double; overload;
   function GetTextWidth(const AText: WideString): Double; overload;
   function GetTextWidthEx(const AText: AnsiString; Len: Cardinal): Double; overload;
   function GetTextWidthEx(const AText: WideString; Len: Cardinal): Double; overload;
{$else}
   function GetTextWidth(const IFont: Pointer; const Text: PAnsiChar; Len: Cardinal; CharSpacing, WordSpacing, TextScale: Single): Double;
{$endif}
   function GetTextWidthA(const AText: AnsiString): Double;
   function GetTextWidthW(const AText: WideString): Double;
   function GetTextWidthExA(const AText: AnsiString; Len: Cardinal): Double;
   function GetTextWidthExW(const AText: WideString; Len: Cardinal): Double;
   function GetTrapped(): Boolean;
   function GetTransparentColor(): Cardinal;
   function GetUseExactPwd(): Boolean;
   function GetUseGlobalImpFiles(): Boolean;
   function GetUserRights(): Integer;
   function GetUserUnit(): Single;
   function GetUseStdFonts(): Boolean;
   function GetUseSystemFonts(): Boolean;
   function GetUseTransparency(): Boolean;
   function GetUseVisibleCoords(): Boolean;
   function GetViewerPrefrences(var Preference, AddVal: Integer): Boolean;
   function GetViewport(PageNum, Index: Cardinal; var VP: TPDFViewport): Boolean;
   function GetViewportCount(PageNum: Cardinal): Integer;
   function GetWMFDefExtent(var Width, Height: Cardinal): Boolean;
   function GetWMFPixelPerInch(): Integer;
   function GetWordSpacing(): Double;
   function HaveOpenDoc(): Boolean;
   function HaveOpenPage(): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function HighlightAnnot(SubType: TAnnotType; PosX, PosY, Width, Height: Double; Color: Cardinal; const Author, Subject, Comment: AnsiString): Integer; overload;
   function HighlightAnnot(SubType: TAnnotType; PosX, PosY, Width, Height: Double; Color: Cardinal; const Author, Subject, Comment: WideString): Integer; overload;
{$endif}
   function HighlightAnnotA(SubType: TAnnotType; PosX, PosY, Width, Height: Double; Color: Cardinal; const Author, Subject, Comment: AnsiString): Integer;
   function HighlightAnnotW(SubType: TAnnotType; PosX, PosY, Width, Height: Double; Color: Cardinal; const Author, Subject, Comment: WideString): Integer;
   function ImportBookmarks(): Integer;
   function ImportCatalogObjects(): Boolean;
   function ImportDocInfo(): Boolean;
   function ImportPage(PageNum: Cardinal): Integer;
   function ImportPageEx(PageNum: Cardinal; ScaleFactX, ScaleFactY: Double): Integer;
   function ImportPDFFile(DestPage: Cardinal; ScaleFactX, ScaleFactY: Double): Integer;
   function InitColorManagement(Profiles: PPDFColorProfiles; DestSpace: TPDFColorSpace; Flags: TPDFInitCMFlags): Boolean;
   function InitExtGState(var GS: TPDFExtGState): Boolean;
   function InitOCGContUsage(var Value: TPDFOCGContUsage): Boolean;
   function InitStack(var Stack: TPDFStack): Boolean;
   function InsertBMPFromBuffer(PosX, PosY, ScaleWidth, ScaleHeight: Double; const Buffer: Pointer): Integer;
   function InsertBMPFromHandle(PosX, PosY, ScaleWidth, ScaleHeight: Double; Handle: HBITMAP): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function InsertBookmark(const Title: AnsiString; Parent: Integer; DestPage: Cardinal; Open, AddChildren: Boolean): Integer; overload;
   function InsertBookmark(const Title: WideString; Parent: Integer; DestPage: Cardinal; Open, AddChildren: Boolean): Integer; overload;
   function InsertBookmarkEx(const Title: AnsiString; Parent: Integer; NamedDest: Cardinal; Open, AddChildren: Boolean): Integer; overload;
   function InsertBookmarkEx(const Title: WideString; Parent: Integer; NamedDest: Cardinal; Open, AddChildren: Boolean): Integer; overload;
{$endif}
   function InsertBookmarkA(const Title: AnsiString; Parent: Integer; DestPage: Cardinal; Open, AddChildren: Boolean): Integer;
   function InsertBookmarkW(const Title: WideString; Parent: Integer; DestPage: Cardinal; Open, AddChildren: Boolean): Integer;
   function InsertBookmarkExA(const Title: AnsiString; Parent: Integer; NamedDest: Cardinal; Open, AddChildren: Boolean): Integer;
   function InsertBookmarkExW(const Title: WideString; Parent: Integer; NamedDest: Cardinal; Open, AddChildren: Boolean): Integer;
   function InsertImage(PosX, PosY, ScaleWidth, ScaleHeight: Double; const AFile: AnsiString): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function InsertImageEx(PosX, PosY, ScaleWidth, ScaleHeight: Double; const AFile: AnsiString; Index: Cardinal): Integer; overload;
   function InsertImageEx(PosX, PosY, ScaleWidth, ScaleHeight: Double; const AFile: WideString; Index: Cardinal): Integer; overload;
{$else}
   function InsertImageEx(PosX, PosY, ScaleWidth, ScaleHeight: Double; const AFile: AnsiString; Index: Cardinal): Integer;
{$endif}
   function InsertImageExW(PosX, PosY, ScaleWidth, ScaleHeight: Double; const AFile: WideString; Index: Cardinal): Integer;
   function InsertImageFromBuffer(PosX, PosY, ScaleWidth, ScaleHeight: Double; const Buffer: Pointer; BufSize, Index: Cardinal): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function InsertMetafile(const FileName: AnsiString; PosX, PosY, Width, Height: Double): Boolean; overload;
   function InsertMetafile(const FileName: WideString; PosX, PosY, Width, Height: Double): Boolean; overload;
{$else}
   function InsertMetafile(const FileName: AnsiString; PosX, PosY, Width, Height: Double): Boolean;
{$endif}
   function InsertMetafileA(const FileName: AnsiString; PosX, PosY, Width, Height: Double): Boolean;
   function InsertMetafileW(const FileName: WideString; PosX, PosY, Width, Height: Double): Boolean;
   function InsertMetafileEx(const Buffer: Pointer; BufSize: Cardinal; PosX, PosY, Width, Height: Double): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function InsertMetafileExt(const FileName: AnsiString; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean; overload;
   function InsertMetafileExt(const FileName: WideString; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean; overload;
{$else}
   function InsertMetafileExt(const FileName: AnsiString; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean;
{$endif}
   function InsertMetafileExtA(const FileName: AnsiString; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean;
   function InsertMetafileExtW(const FileName: WideString; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean;
   function InsertMetafileExtEx(const Buffer: Pointer; BufSize: Cardinal; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean;
   function InsertMetafileFromHandle(Handle: HENHMETAFILE; PosX, PosY, Width, Height: Double): Boolean;
   function InsertMetafileFromHandleEx(Handle: HENHMETAFILE; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean;
   function InsertRawImage(const Buffer: Pointer; BitsPerPixel, ColorCount, ImgWidth, ImgHeight: Cardinal; PosX, PosY, ScaleWidth, ScaleHeight: Double): Integer;
   function InsertRawImageEx(PosX, PosY, ScaleWidth, ScaleHeight: Double; var Image: TPDFRawImage): Integer;
   function IsBidiText(const AText: WideString): Integer;
   function IsColorPage(GrayIsColor: Boolean): Integer;
   function IsEmptyPage(): Integer;
   function IsWrongPwd(ErrCode: Integer): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function LineAnnot(x1, y1, x2, y2, LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: AnsiString): Integer; overload;
   function LineAnnot(x1, y1, x2, y2, LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: WideString): Integer; overload;
{$endif}
   function LineAnnotA(x1, y1, x2, y2, LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: AnsiString): Integer;
   function LineAnnotW(x1, y1, x2, y2, LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: WideString): Integer;
   function LineTo(PosX, PosY: Double): Boolean;
   function LoadCMap(const CMapName: AnsiString; Embed: Boolean): Integer;
   function LoadFDFDataA(const FileName, Password: AnsiString; Flags: Cardinal): Boolean;
   function LoadFDFDataW(const FileName: WideString; const Password: AnsiString; Flags: Cardinal): Boolean;
   function LoadFDFDataEx(const Buffer: Pointer; BufSize: Cardinal; const Password: AnsiString; Flags: Cardinal): Boolean;
   function LoadFont(const Buffer: Pointer; BufSize: Cardinal; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function LoadFontEx(const FontFile: AnsiString; Index: Cardinal; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer; overload;
   function LoadFontEx(const FontFile: WideString; Index: Cardinal; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer; overload;
{$endif}
   function LoadFontExA(const FontFile: AnsiString; Index: Cardinal; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
   function LoadFontExW(const FontFile: WideString; Index: Cardinal; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
   function LockLayer(Layer: Cardinal): Boolean;
   function MovePage(Source, Dest: Cardinal): Boolean;
   function MoveTo(PosX, PosY: Double): Boolean;
   procedure MultiplyMatrix(var M1, M2, NewMatrix: TCTM);
   function OpenImportBuffer(const Buffer: Pointer; BufSize: Cardinal; PwdType: TPwdType; const Password: AnsiString): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function OpenImportFile(const FileName: AnsiString; PwdType: TPwdType; const Password: AnsiString): Integer; overload;
   function OpenImportFile(const FileName: WideString; PwdType: TPwdType; const Password: AnsiString): Integer; overload;
   function OpenOutputFile(const OutPDF: AnsiString): Boolean; overload;
   function OpenOutputFile(const OutPDF: WideString): Boolean; overload;
   function OpenTag(Tag: TPDFBaseTag; const Lang, AltText, Expansion: AnsiString): Boolean; overload;
   function OpenTag(Tag: TPDFBaseTag; const Lang: AnsiString; const AltText, Expansion: WideString): Boolean; overload;
{$endif}
   function OpenImportFileA(const FileName: AnsiString; PwdType: TPwdType; const Password: AnsiString): Integer;
   function OpenImportFileW(const FileName: WideString; PwdType: TPwdType; const Password: AnsiString): Integer; 
   function OpenOutputFileA(const OutPDF: AnsiString): Boolean;
   function OpenOutputFileW(const OutPDF: WideString): Boolean;
   function OpenTagA(Tag: TPDFBaseTag; const Lang, AltText, Expansion: AnsiString): Boolean;
   function OpenTagW(Tag: TPDFBaseTag; const Lang: AnsiString; const AltText, Expansion: WideString): Boolean;
   function Optimize(Flags: Cardinal; Parms: Pointer): Boolean;
   function PageLink(PosX, PosY, Width, Height: Double; DestPage: Cardinal): Integer;
   function PageLink2(PosX, PosY, Width, Height: Double; NamedDest: Cardinal): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function PageLink3(PosX, PosY, Width, Height: Double; const NamedDest: AnsiString): Integer; overload;
   function PageLink3(PosX, PosY, Width, Height: Double; const NamedDest: WideString): Integer; overload;
{$endif}
   function PageLink3A(PosX, PosY, Width, Height: Double; const NamedDest: AnsiString): Integer;
   function PageLink3W(PosX, PosY, Width, Height: Double; const NamedDest: WideString): Integer;
   function PageLinkEx(PosX, PosY, Width, Height: Double; DestType: TDestType; DestPage: Cardinal; a, b, c, d: Double): Integer;
   function ParseContent(const UserData: Pointer; var Stack: TPDFParseInterface; Flags: TParseFlags): Boolean;
   property PDFInstance: Pointer read FInstance;
   function PlaceImage(ImgHandle: Integer; PosX, PosY, ScaleWidth, ScaleHeight: Double): Boolean;
   function PlaceSigFieldValidateIcon(SigField: Cardinal; PosX, PosY, Width, Height: Double): Boolean;
   function PlaceTemplate(TmplHandle: Integer; PosX, PosY, ScaleWidth, ScaleHeight: Double): Boolean;
   function PlaceTemplateEx(TmplHandle: Integer; PosX, PosY, ScaleWidth, ScaleHeight: Double): Boolean;
   function PlaceTemplByMatrix(TmplHandle: Integer): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function PolygonAnnot(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: AnsiString): Integer; overload;
   function PolygonAnnot(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: WideString): Integer; overload;
   function PolyLineAnnot(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: AnsiString): Integer; overload;
   function PolyLineAnnot(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: WideString): Integer; overload;
{$endif}
   function PolygonAnnotA(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: AnsiString): Integer;
   function PolygonAnnotW(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: WideString): Integer;
   function PolyLineAnnotA(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: AnsiString): Integer;
   function PolyLineAnnotW(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: WideString): Integer;
   function ReadImageFormatA(const FileName: AnsiString; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
   function ReadImageFormatW(const FileName: WideString; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
   function ReadImageFormat2A(const FileName: AnsiString; Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
   function ReadImageFormat2W(const FileName: WideString; Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function ReadImageFormat(const FileName: AnsiString; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean; overload;
   function ReadImageFormat(const FileName: WideString; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean; overload;
   function ReadImageFormat2(const FileName: AnsiString; Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean; overload;
   function ReadImageFormat2(const FileName: WideString; Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean; overload;
{$else}
   function ReadImageFormat(const FileName: AnsiString; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
   function ReadImageFormat2(const FileName: AnsiString; Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
{$endif}
   function ReadImageFormatEx(hBitmap: HBITMAP; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
   function ReadImageFormatFromBuffer(const Buffer: Pointer; BufSize, Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function ReadImageResolution(const FileName: AnsiString; Index: Cardinal; var ResX, ResY: Cardinal): Boolean; overload;
   function ReadImageResolution(const FileName: WideString; Index: Cardinal; var ResX, ResY: Cardinal): Boolean; overload;
{$else}
   function ReadImageResolution(const FileName: AnsiString; Index: Cardinal; var ResX, ResY: Cardinal): Boolean;
{$endif}
   function ReadImageResolutionA(const FileName: AnsiString; Index: Cardinal; var ResX, ResY: Cardinal): Boolean;
   function ReadImageResolutionW(const FileName: WideString; Index: Cardinal; var ResX, ResY: Cardinal): Boolean;
   function ReadImageResolutionEx(const Buffer: Pointer; BufSize, Index: Cardinal; var ResX, ResY: Cardinal): Boolean;
   function Rectangle(PosX, PosY, Width, Height: Double; FillMode: TPathFillMode): Boolean;
   procedure Redraw(RasPtr: Pointer; DC: HDC; DestX, DestY: Integer);
{$ifdef DELPHI6_OR_HIGHER}
   function ReEncryptPDF(const FileName: AnsiString; PwdType: TPwdType; const InPwd, NewOpenPwd, NewOwnerPwd: AnsiString; NewKeyLen: TKeyLen; Restrict: TRestrictions): Integer; overload;
   function ReEncryptPDF(const FileName: WideString; PwdType: TPwdType; const InPwd, NewOpenPwd, NewOwnerPwd: AnsiString; NewKeyLen: TKeyLen; Restrict: TRestrictions): Integer; overload;
{$endif}
   function ReEncryptPDFA(const FileName: AnsiString; PwdType: TPwdType; const InPwd, NewOpenPwd, NewOwnerPwd: AnsiString; NewKeyLen: TKeyLen; Restrict: TRestrictions): Integer; 
   function ReEncryptPDFW(const FileName: WideString; PwdType: TPwdType; const InPwd, NewOpenPwd, NewOwnerPwd: AnsiString; NewKeyLen: TKeyLen; Restrict: TRestrictions): Integer; 
   function RenameSpotColor(const Colorant, NewName: AnsiString): Integer;
   function RenderAnnotOrField(Handle: Cardinal; IsAnnot: Boolean; State: TButtonState; var Matrix: TCTM; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; var OutImage: TPDFBitmap): Boolean;
   function RenderPage(PagePtr, RasPtr: Pointer; var Img: TPDFRasterImage): Boolean;
   function RenderPageEx(DC: HDC; var DestX, DestY: Integer; PagePtr, RasPtr: Pointer; var Img: TPDFRasterImage): Boolean;
   function RenderPageToImageA(PageNum: Cardinal; const OutFile: AnsiString; Resolution, Width, Height: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean;
   function RenderPageToImageW(PageNum: Cardinal; const OutFile: WideString; Resolution, Width, Height: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function RenderPageToImage(PageNum: Cardinal; const OutFile: AnsiString; Resolution, Width, Height: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean; overload;
   function RenderPageToImage(PageNum: Cardinal; const OutFile: WideString; Resolution, Width, Height: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean; overload;
   function RenderPDFFile(const OutFile: AnsiString; Resolution: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean; overload;
   function RenderPDFFile(const OutFile: WideString; Resolution: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean; overload;
   function ReplaceFont(const PDFFont: Pointer; const Name: AnsiString; Style: TFStyle; NameIsFamilyName: Boolean): Integer; overload;
   function ReplaceFont(const PDFFont: Pointer; const Name: WideString; Style: TFStyle; NameIsFamilyName: Boolean): Integer; overload;
   function ReplaceFontEx(const PDFFont: Pointer; const FontFile: AnsiString; Embed: Boolean): Integer; overload;
   function ReplaceFontEx(const PDFFont: Pointer; const FontFile: WideString; Embed: Boolean): Integer; overload;
   function ReplaceICCProfile(ColorSpace: Cardinal; const ICCFile: AnsiString): Integer; overload;
   function ReplaceICCProfile(ColorSpace: Cardinal; const ICCFile: WideString): Integer; overload;
{$endif}
   function ReOpenImportFile(Handle: Cardinal): Boolean;
   function RenderPDFFileA(const OutFile: AnsiString; Resolution: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean;
   function RenderPDFFileW(const OutFile: WideString; Resolution: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean;
   function ReplaceFontA(const PDFFont: Pointer; const Name: AnsiString; Style: TFStyle; NameIsFamilyName: Boolean): Integer;
   function ReplaceFontW(const PDFFont: Pointer; const Name: WideString; Style: TFStyle; NameIsFamilyName: Boolean): Integer;
   function ReplaceFontExA(const PDFFont: Pointer; const FontFile: AnsiString; Embed: Boolean): Integer;
   function ReplaceFontExW(const PDFFont: Pointer; const FontFile: WideString; Embed: Boolean): Integer;
   function ReplaceICCProfileA(ColorSpace: Cardinal; const ICCFile: AnsiString): Integer;
   function ReplaceICCProfileW(ColorSpace: Cardinal; const ICCFile: WideString): Integer;
   function ReplacePageText(const NewText: AnsiString; var Stack: TPDFStack): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function ReplacePageTextEx(const NewText: AnsiString; var Stack: TPDFStack): Boolean; overload;
   function ReplacePageTextEx(const NewText: WideString; var Stack: TPDFStack): Boolean; overload;
{$endif}
   function ReplacePageTextExA(const NewText: AnsiString; var Stack: TPDFStack): Boolean;
   function ReplacePageTextExW(const NewText: WideString; var Stack: TPDFStack): Boolean;
   function ResetLineDashPattern(): Boolean;
   function ResizeBitmap(RasPtr: Pointer; DC: HDC; Width, Height: Cardinal): Boolean;
   function RestoreGraphicState(): Boolean;
   function RotateCoords(alpha, OriginX, OriginY: Double): Boolean;
   function RoundRect(PosX, PosY, Width, Height, Radius: Double; FillMode: TPathFillMode): Boolean;
   function RoundRectEx(PosX, PosY, Width, Height, rWidth, rHeight: Double; FillMode: TPathFillMode): Boolean;
   function SaveGraphicState(): Boolean;
   function ScaleCoords(sx, sy: Double): Boolean;
   function SelfTest(): Boolean;
   function Set3DAnnotProps(Annot: Cardinal; ActType: T3DActivationType; DeActType: T3DDeActivateType; InstType: T3DInstanceType; DeInstType: T3DDeActInstance; DisplToolbar, DisplModelTree: Boolean): Boolean;
   function Set3DAnnotScriptA(Annot: Cardinal; const Value: AnsiString): Boolean;
   function SetAllocBy(Value: Integer): Boolean;
   function SetAnnotBorderStyle(Handle: Cardinal; Style: TBorderStyle): Boolean;
   function SetAnnotBorderWidth(Handle: Cardinal; LineWidth: Double): Boolean;
   function SetAnnotColor(Handle: Cardinal; ColorType: TAnnotColor; CS: TPDFColorSpace; Color: Cardinal): Boolean;
   function SetAnnotFlags(Flags: TAnnotFlags): Boolean;
   function SetAnnotFlagsEx(Handle: Cardinal; Flags: Integer): Boolean;
   function SetAnnotHighlightMode(Handle: Cardinal; Mode: THighlightMode): Boolean;
   function SetAnnotIcon(Handle: Cardinal; Icon: TAnnotIcon): Boolean;
   function SetAnnotLineEndStyle(Handle: Cardinal; StartStyle, EndStyle: TLineEndStyle): Boolean;
   function SetAnnotOpenState(Handle: Cardinal; Open: Boolean): Boolean;
   function SetAnnotOrFieldDate(Handle: Cardinal; IsField: Boolean; DateType: TDateType; DateTime: Cardinal): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function SetAnnotString(Handle: Cardinal; StringType: TAnnotString; const Value: AnsiString): Boolean; overload;
   function SetAnnotString(Handle: Cardinal; StringType: TAnnotString; const Value: WideString): Boolean; overload;
   function SetAnnotSubject(Handle: Cardinal; const Value: AnsiString): Boolean; overload;
   function SetAnnotSubject(Handle: Cardinal; const Value: WideString): Boolean; overload;
{$endif}
   function SetAnnotStringA(Handle: Cardinal; StringType: TAnnotString; const Value: AnsiString): Boolean;
   function SetAnnotStringW(Handle: Cardinal; StringType: TAnnotString; const Value: WideString): Boolean;
   function SetAnnotSubjectA(Handle: Cardinal; const Value: AnsiString): Boolean;
   function SetAnnotSubjectW(Handle: Cardinal; const Value: WideString): Boolean;
   function SetBBox(Boundary: TPageBoundary; X0, Y0, Width, Height: Double): Boolean;
   function SetBidiMode(Mode: TPDFBidiMode): Boolean;
   function SetBookmarkDest(ABmk: Integer; DestType: TDestType; a, b, c, d: Double): Boolean;
   function SetBookmarkStyle(ABmk: Integer; Style: TFStyle; RGBColor: Cardinal): Boolean;
   function SetBorderStyle(Style: TBorderStyle): Boolean;
   function SetCharacterSpacing(Value: Double): Boolean;
   function SetCheckBoxChar(CheckBoxChar: TCheckBoxChar): Boolean;
   function SetCheckBoxDefState(AField: Cardinal; Checked: Boolean): Boolean;
   function SetCheckBoxState(AField: Cardinal; Checked: Boolean): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function SetCIDFont(CMapHandle: Cardinal; const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean): Integer; overload;
   function SetCIDFont(CMapHandle: Cardinal; const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean): Integer; overload;
   function SetCMapDir(const Path: AnsiString; Flags: TLoadCMapFlags): Integer; overload;
   function SetCMapDir(const Path: WideString; Flags: TLoadCMapFlags): Integer; overload;
{$endif}
   function SetCIDFontA(CMapHandle: Cardinal; const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean): Integer;
   function SetCIDFontW(CMapHandle: Cardinal; const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean): Integer;
   function SetCMapDirA(const Path: AnsiString; Flags: TLoadCMapFlags): Integer;
   function SetCMapDirW(const Path: WideString; Flags: TLoadCMapFlags): Integer;
   function SetColDefFile(EmbFile: Cardinal): Boolean;
   function SetColorMask(ImageHandle: Cardinal; Mask: PInteger; Count: Cardinal): Boolean;
   function SetColors(Color: Cardinal): Boolean;
   function SetColorSpace(ColorSpace: TPDFColorSpace): Boolean;
   function SetColSortField(ColField: Cardinal; AscendingOrder: Boolean): Boolean;
   function SetCompressionFilter(Filter: TCompressionFilter): Boolean;
   function SetCompressionLevel(CompressLevel: TCompressionLevel): Boolean;
   function SetContent(const Buffer: PAnsiChar; BufSize: Cardinal): Boolean;
   function SetDateTimeFormat(TxtField: Cardinal; Fmt: TPDFDateTime): Boolean;
   function SetDefBitsPerPixel(Value: Integer): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function SetDocInfo(DInfo: TDocumentInfo; const Value: AnsiString): Boolean; overload;
   function SetDocInfo(DInfo: TDocumentInfo; const Value: WideString): Boolean; overload;
   function SetDocInfoEx(DInfo: TDocumentInfo; const Key, Value: AnsiString): Boolean; overload;
   function SetDocInfoEx(DInfo: TDocumentInfo; const Key: AnsiString; const Value: WideString): Boolean; overload;
{$endif}
   function SetDocInfoA(DInfo: TDocumentInfo; const Value: AnsiString): Boolean; 
   function SetDocInfoW(DInfo: TDocumentInfo; const Value: WideString): Boolean;
   function SetDocInfoExA(DInfo: TDocumentInfo; const Key, Value: AnsiString): Boolean;
   function SetDocInfoExW(DInfo: TDocumentInfo; const Key: AnsiString; const Value: WideString): Boolean;
   function SetDrawDirection(Direction: TDrawDirection): Boolean;
   function SetEMFFrameDPI(DPIX, DPIY: Cardinal): Boolean;
   function SetEMFPatternDistance(Value: Double): Boolean;
   function SetErrorMode(ErrMode: TErrMode): Boolean;
   function SetExtColorSpace(Handle: Cardinal): Boolean;
   function SetExtFillColorSpace(Handle: Cardinal): Boolean;
   function SetExtGState(Handle: Cardinal): Boolean;
   function SetExtStrokeColorSpace(Handle: Cardinal): Boolean;
   function SetFieldBackColor(AColor: Cardinal): Boolean;
   function SetFieldBBox(AField: Cardinal; var BBox: TPDFRect): Boolean;
   function SetFieldBorderColor(AColor: Cardinal): Boolean;
   function SetFieldBorderStyle(AField: Cardinal; Style: TBorderStyle): Boolean;
   function SetFieldBorderWidth(AField: Cardinal; LineWidth: Double): Boolean;
   function SetFieldColor(AField: Cardinal; ColorType: TFieldColor; CS: TPDFColorSpace; Color: Cardinal): Boolean;
   function SetFieldExpValue(AField, ValIndex: Cardinal; const Value, ExpValue: AnsiString; Selected: Boolean): Boolean;
   function SetFieldExpValueEx(AField, ValIndex: Cardinal; Selected, DefSelected: Boolean): Boolean;
   function SetFieldFlags(AField: Cardinal; Flags: TFieldFlags; Reset: Boolean): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function SetFieldFont(Field: Cardinal; const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer; overload;
   function SetFieldFont(Field: Cardinal; const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer; overload;
{$else}
   function SetFieldFontA(Field: Cardinal; const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
   function SetFieldFontW(Field: Cardinal; const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
{$endif}
   function SetFieldFontEx(Field, Handle: Cardinal; FontSize: Double): Boolean;
   function SetFieldFontSize(AField: Cardinal; FontSize: double): Boolean;
   function SetFieldHighlightMode(AField: Cardinal; Mode: THighlightMode): Boolean;
   function SetFieldIndex(AField, Index: Cardinal): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function SetFieldMapName(AField: Cardinal; const Name: AnsiString): Boolean; overload;
   function SetFieldMapName(AField: Cardinal; const Name: WideString): Boolean; overload;
{$endif}
   function SetFieldMapNameA(AField: Cardinal; const Name: AnsiString): Boolean;
   function SetFieldMapNameW(AField: Cardinal; const Name: WideString): Boolean;
   function SetFieldName(AField: Cardinal; const NewName: AnsiString): Boolean;
   function SetFieldOrientation(AField: Cardinal; Value: Integer): Boolean;
   function SetFieldTextAlign(AField: Cardinal; Align: TTextAlign): Boolean;
   function SetFieldTextColor(Color: Cardinal): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function SetFieldToolTip(AField: Cardinal; const Value: AnsiString): Boolean; overload;
   function SetFieldToolTip(AField: Cardinal; const Value: WideString): Boolean; overload;
{$endif}
   function SetFieldToolTipA(AField: Cardinal; const Value: AnsiString): Boolean;
   function SetFieldToolTipW(AField: Cardinal; const Value: WideString): Boolean;
   function SetFillColor(Color: Cardinal): Boolean;
   function SetFillColorEx(const Color: Array of Byte; NumComponents: Cardinal): Boolean;
   function SetFillColorF(const Color: PSingle; NumComponents: Cardinal): Boolean;
   procedure SetFillColorSpace(CS: TPDFColorSpace);
{$ifdef DELPHI6_OR_HIGHER}
   function SetFont(const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage = cp1252): Integer; overload;
   function SetFont(const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage = cp1252): Integer; overload;
   function SetFontEx(const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer; overload;
   function SetFontEx(const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer; overload;
{$endif}
   function SetFontA(const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage = cp1252): Integer; 
   function SetFontW(const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage = cp1252): Integer; 
   function SetFontExA(const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
   function SetFontExW(const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
   function SetFontOrigin(Origin: TOrigin): Boolean;
   procedure SetFontSearchOrder(Order: PFontBaseType{Array[0..3] of TFontBaseType});
   procedure SetFontSearchOrderEx(S1, S2, S3, S4: TFontBaseType);
   function SetFontSelMode(Mode: TFontSelMode): Boolean;
   function SetFontWeight(Weight: Integer): Boolean;
   function SetGStateFlags(Flags: TGStateFlags; Reset: Boolean): Boolean;
   function SetIconColor(Color: Cardinal): Boolean;
   function SetImportFlags(Flags: TImportFlags): Boolean;
   function SetImportFlags2(Flags: TImportFlags2): Boolean;
   function SetItalicAngle(Angle: Double): Boolean;
   function SetJPEGQuality(Value: Integer): Boolean;
   function SetLanguage(const ISOTag: AnsiString): Boolean;
   function SetLeading(Value: Double): Boolean;
   function SetLicenseKey(const Key: AnsiString): Boolean;
   function SetLineAnnotParms(Handle: Cardinal; FontHandle: Integer; FontSize: Double; const Parms: PLineAnnotParms): Boolean;
   function SetLineCapStyle(Style: TLineCapStyle): Boolean;
   function SetLineDashPattern(const Dash: AnsiString; Phase: Integer): Boolean;
   function SetLineDashPatternEx(const Dash: Array of Double; NumValues: Cardinal; Phase: Integer): Boolean;
   function SetLineJoinStyle(Style: TLineJoinStyle): Boolean;
   function SetLineWidth(Value: Double): Boolean;
   function SetLinkHighlightMode(Mode: THighlightMode): Boolean;
   function SetListFont(Handle: Cardinal): Boolean;
   function SetMatrix(var Matrix: TCTM): Boolean;
   procedure SetMaxErrLogMsgCount(Value: Cardinal);
   function SetMaxFieldLen(TxtField: Cardinal; MaxLen: Integer): Boolean;
   function SetMetaConvFlags(Flags: TMetaFlags): Boolean;
   function SetMetadata(ObjType: TMetadataObj; Handle: Integer; const Buffer: Pointer; BufSize: Cardinal): Boolean;
   function SetMiterLimit(Value: Double): Boolean;
   function SetNeedAppearance(Value: Boolean): Boolean;
   function SetNumberFormat(TxtField: Cardinal; Sep: TDecSeparator; DecPlaces: Cardinal; NegStyle: TNegativeStyle; const CurrStr: AnsiString; Prepend: Boolean): Boolean;
   function SetOCGContUsage(Handle: Cardinal; var Value: TPDFOCGContUsage): Boolean;
   function SetOCGState(Handle: Cardinal; Visible, SaveState: Boolean): Boolean;
   function SetOnErrorProc(const Data: Pointer; ErrProc: TErrorProc): Boolean;
   function SetOnPageBreakProc(const Data: Pointer; OnBreakProc: TOnPageBreakProc): Boolean;
   function SetOpacity(Value: Double): Boolean;
   function SetOrientation(Value: Integer): Boolean;
   function SetOrientationEx(Value: Integer): Boolean;
   function SetPageCoords(PageCoords: TPageCoord): Boolean;
   function SetPageFormat(Value: TPageFormat): Boolean;
   function SetPageHeight(Value: Double): Boolean;
   function SetPageLayout(Layout: TPageLayout): Boolean;
   function SetPageMode(Mode: TPageMode): Boolean;
   function SetPageWidth(Value: Double): Boolean;
   function SetPDFVersion(Version: TPDFVersion): Boolean;
   function SetPrintSettings(Mode: TDuplexMode; PickTrayByPDFSize: Integer; NumCopies: Cardinal; PrintScaling: TPrintScaling; const PrintRanges: Array of Cardinal; NumRanges: Cardinal): Boolean;
   function SetProgressProc(const Data: Pointer; InitProgressProc: TInitProgressProc; ProgressProc: TProgressProc): Boolean;
   function SetResolution(Value: Cardinal): Boolean;
   function SetSaveNewImageFormat(Value: Boolean): Boolean;
   function SetSeparationInfo(Handle: Cardinal): Boolean;
   function SetStrokeColor(Color: Cardinal): Boolean;
   function SetStrokeColorEx(const Color: Array of Byte; NumComponents: Cardinal): Boolean;
   function SetStrokeColorF(const Color: PSingle; NumComponents: Cardinal): Boolean;
   procedure SetStrokeColorSpace(CS: TPDFColorSpace);
   function SetTabLen(TabLen: Integer): Boolean;
   function SetTextDrawMode(Mode: TDrawMode): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function SetTextFieldValue(Field: Cardinal; const Value, DefValue: AnsiString; Align: TTextAlign): Boolean; overload;
   function SetTextFieldValue(Field: Cardinal; const Value, DefValue: WideString; Align: TTextAlign): Boolean; overload;
   function SetTextFieldValueEx(Field: Cardinal; const Value: AnsiString): Boolean; overload;
   function SetTextFieldValueEx(Field: Cardinal; const Value: WideString): Boolean; overload;
{$endif}
   function SetTextFieldValueA(Field: Cardinal; const Value, DefValue: AnsiString; Align: TTextAlign): Boolean; 
   function SetTextFieldValueW(Field: Cardinal; const Value, DefValue: WideString; Align: TTextAlign): Boolean;
   function SetTextFieldValueExA(Field: Cardinal; const Value: AnsiString): Boolean;
   function SetTextFieldValueExW(Field: Cardinal; const Value: WideString): Boolean;
   function SetTextRect(PosX, PosY, Width, Height: Double): Boolean;
   function SetTextRise(Value: Double): Boolean;
   function SetTextScaling(Value: Double): Boolean;
   procedure SetTrapped(Value: Boolean);
   function SetTransparentColor(AColor: Cardinal): Boolean;
   function SetUseExactPwd(Value: Boolean): Boolean;
   function SetUseGlobalImpFiles(Value: Boolean): Boolean;
   function SetUserUnit(Value: Single): Boolean;
   function SetUseStdFonts(Value: Boolean): Boolean;
   function SetUseSwapFile(SwapContents: Boolean; SwapLimit: Cardinal): Boolean;
   function SetUseSwapFileEx(SwapContents: Boolean; SwapLimit: Cardinal; const SwapDir: AnsiString): Boolean;
   function SetUseSystemFonts(Value: Boolean): Boolean;
   function SetUseTransparency(Value: Boolean): Boolean;
   function SetUseVisibleCoords(Value: Boolean): Boolean;
   function SetViewerPreferences(Value: TViewerPreference; AddVal: TViewPrefAddVal): Boolean;
   function SetWMFDefExtent(Width, Height: Cardinal): Boolean;
   function SetWMFPixelPerInch(Value: Cardinal): Boolean;
   function SetWordSpacing(Value: Double): Boolean;
   function SkewCoords(alpha, beta, OriginX, OriginY: Double): Boolean;
   function SortFieldsByIndex(): Boolean;
   function SortFieldsByName(): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function SquareAnnot(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: AnsiString): Integer; overload;
   function SquareAnnot(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: WideString): Integer; overload;
{$endif}
   function SquareAnnotA(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: AnsiString): Integer;
   function SquareAnnotW(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: WideString): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function StampAnnot(SubType: TRubberStamp; PosX, PosY, Width, Height: Double; const Author, Subject, Comment: AnsiString): Integer; overload;
   function StampAnnot(SubType: TRubberStamp; PosX, PosY, Width, Height: Double; const Author, Subject, Comment: WideString): Integer; overload;
{$endif}
   function StampAnnotA(SubType: TRubberStamp; PosX, PosY, Width, Height: Double; const Author, Subject, Comment: AnsiString): Integer;
   function StampAnnotW(SubType: TRubberStamp; PosX, PosY, Width, Height: Double; const Author, Subject, Comment: WideString): Integer;
   function StrokePath(): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function ToUnicode(const S: AnsiString): WideString; overload;
   function ToUnicode(const S: PAnsiChar; Len: Integer): WideString; overload;
{$endif}
   function ToUnicodeA(const S: AnsiString): WideString;
   function ToUnicodeW(const S: PAnsiChar; Len: Integer): WideString;
   function TranslateRawCode(const IFont: Pointer; const Text: PAnsiChar; Len: Cardinal; var Width: Double; OutText: PWideChar{Array[0..31] of WideChar}; var OutLen: Integer; var Decoded: LongBool; CharSpacing, WordSpacing, TextScale: Single): Cardinal;
   function TranslateString(var Stack: TPDFStack; var Buffer: PWideChar; var Size: Cardinal; Flags: Cardinal): Integer;
   function TranslateString2(const IFont: Pointer; const Text: PAnsiChar; Len: Cardinal; OutBuf: WideString; Size, Flags: Cardinal): Integer;
{$ifdef DELPHI6_OR_HIGHER}
   function TestGlyphs(FontHandle: Integer; const Text: AnsiString): Integer; overload;
   function TestGlyphs(FontHandle: Integer; const Text: WideString): Integer; overload;
   function TestGlyphsEx(FontHandle: Integer; const Text: AnsiString; Len: Cardinal): Integer; overload;
   function TestGlyphsEx(FontHandle: Integer; const Text: WideString; Len: Cardinal): Integer; overload;
{$else}
   function TestGlyphsA(FontHandle: Integer; const Text: AnsiString): Integer;
   function TestGlyphsW(FontHandle: Integer; const Text: WideString): Integer;
   function TestGlyphsExA(FontHandle: Integer; const Text: AnsiString; Len: Cardinal): Integer;
   function TestGlyphsExW(FontHandle: Integer; const Text: WideString; Len: Cardinal): Integer;
{$endif}
{$ifdef DELPHI6_OR_HIGHER}
   function TextAnnot(PosX, PosY, Width, Height: Double; const Author, Text: AnsiString; Icon: TAnnotIcon; Open: Boolean): Integer; overload;
   function TextAnnot(PosX, PosY, Width, Height: Double; const Author, Text: WideString; Icon: TAnnotIcon; Open: Boolean): Integer; overload;
{$endif}
   function TextAnnotA(PosX, PosY, Width, Height: Double; const Author, Text: AnsiString; Icon: TAnnotIcon; Open: Boolean): Integer;
   function TextAnnotW(PosX, PosY, Width, Height: Double; const Author, Text: WideString; Icon: TAnnotIcon; Open: Boolean): Integer;
   function TranslateCoords(NewOriginX, NewOriginY: Double): Boolean;
   function Triangle(x1, y1, x2, y2, x3, y3: Double; FillMode: TPathFillMode): Boolean;
   function UnLockLayer(Layer: Cardinal): Boolean;
   function UTF16ToUTF32(const Source: WideString): Pointer;
   function UTF16ToUTF32Ex(const Source: WideString; var Len: Cardinal): Pointer;
   function UTF32ToUTF16(const Source: Pointer): PWideChar;
   function UTF32ToUTF16Ex(const Source: Pointer; var Len: Cardinal): PWideChar;
{$ifdef DELPHI6_OR_HIGHER}
   function WebLink(PosX, PosY, Width, Height: Double; const URL: AnsiString): Integer; overload;
   function WebLink(PosX, PosY, Width, Height: Double; const URL: WideString): Integer; overload;
{$endif}
   function WebLinkA(PosX, PosY, Width, Height: Double; const URL: AnsiString): Integer; 
   function WebLinkW(PosX, PosY, Width, Height: Double; const URL: WideString): Integer; 
   function WeigthFromStyle(Style: TFStyle): Cardinal;
   function WeightToStyle(Weight: Cardinal): TFStyle;
   function WidthFromStyle(Style: TFStyle): Cardinal;
   function WidthToStyle(Width: Cardinal): TFStyle;
{$ifdef DELPHI6_OR_HIGHER}
   function WriteAngleText(const AText: AnsiString; Angle, PosX, PosY, Radius, YOrigin: Double): Boolean; overload;
   function WriteAngleText(const AText: WideString; Angle, PosX, PosY, Radius, YOrigin: Double): Boolean; overload;
   function WriteFText(Align: TTextAlign; const AText: AnsiString): Boolean; overload;
   function WriteFText(Align: TTextAlign; const AText: WideString): Boolean; overload;
   function WriteFTextEx(PosX, PosY, Width, Height: Double; Align: TTextAlign; const AText: AnsiString): Boolean; overload;
   function WriteFTextEx(PosX, PosY, Width, Height: Double; Align: TTextAlign; const AText: WideString): Boolean; overload;
   function WriteText(PosX, PosY: Double; const AText: AnsiString): Boolean; overload;
   function WriteText(PosX, PosY: Double; const AText: WideString): Boolean; overload;
   function WriteTextEx(PosX, PosY: Double; const AText: AnsiString; Len: Cardinal): Boolean; overload;
   function WriteTextEx(PosX, PosY: Double; const AText: WideString; Len: Cardinal): Boolean; overload;
   function WriteTextMatrix(var Matrix: TCTM; const AText: AnsiString): Boolean; overload;
   function WriteTextMatrix(var Matrix: TCTM; const AText: WideString): Boolean; overload;
   function WriteTextMatrixEx(var M: TCTM; const AText: AnsiString; Len: Cardinal): Boolean; overload;
   function WriteTextMatrixEx(var M: TCTM; const AText: WideString; Len: Cardinal): Boolean; overload;
{$endif}
   function WriteAngleTextA(const AText: AnsiString; Angle, PosX, PosY, Radius, YOrigin: Double): Boolean; 
   function WriteAngleTextW(const AText: WideString; Angle, PosX, PosY, Radius, YOrigin: Double): Boolean; 
   function WriteFTextA(Align: TTextAlign; const AText: AnsiString): Boolean; 
   function WriteFTextW(Align: TTextAlign; const AText: WideString): Boolean; 
   function WriteFTextExA(PosX, PosY, Width, Height: Double; Align: TTextAlign; const AText: AnsiString): Boolean;
   function WriteFTextExW(PosX, PosY, Width, Height: Double; Align: TTextAlign; const AText: WideString): Boolean; 
   function WriteTextA(PosX, PosY: Double; const AText: AnsiString): Boolean;
   function WriteTextW(PosX, PosY: Double; const AText: WideString): Boolean;
   function WriteTextExA(PosX, PosY: Double; const AText: AnsiString; Len: Cardinal): Boolean;
   function WriteTextExW(PosX, PosY: Double; const AText: WideString; Len: Cardinal): Boolean;
   function WriteTextMatrixA(var Matrix: TCTM; const AText: AnsiString): Boolean;
   function WriteTextMatrixW(var Matrix: TCTM; const AText: WideString): Boolean;
   function WriteTextMatrixExA(var M: TCTM; const AText: AnsiString; Len: Cardinal): Boolean;
   function WriteTextMatrixExW(var M: TCTM; const AText: WideString; Len: Cardinal): Boolean;
 protected
   fntBuildFamilyNameAndStyle: TfntBuildFamilyNameAndStyle;
   fntGetFont:                 TfntGetFont;
   fntGetFontInfo:             TfntGetFontInfo;
   fntGetTextWidth:            TfntGetTextWidth;
   fntGetSpaceWidth:           TfntGetSpaceWidth;
   fntTranslateRawCode:        TfntTranslateRawCode;
   fntTranslateString:         TfntTranslateString;
   fntTranslateString2:        TfntTranslateString2;

   // Rendering Engine
   rasAbort:              TrasAbort;
   rasAttachImageBuffer:  TrasAttachImageBuffer;
   rasCalcPagePixelSize:  TrasCalcPagePixelSize;
   rasCreateRasterizer:   TrasCreateRasterizer;
   rasCreateRasterizerEx: TrasCreateRasterizerEx;
   rasDeleteRasterizer:   TrasDeleteRasterizer;
   rasRedraw:             TrasRedraw;
   rasResizeBitmap:       TrasResizeBitmap;

   // DynaPDF API
   pdfAddActionToObj: TpdfAddActionToObj;
   pdfAddArticle: TpdfAddArticle;
   pdfAddBookmarkA: TpdfAddBookmarkA;
   pdfAddBookmarkW: TpdfAddBookmarkW;
   pdfAddBookmarkExA: TpdfAddBookmarkExA;
   pdfAddBookmarkExW: TpdfAddBookmarkExW;
   pdfAddBookmarkEx2A: TpdfAddBookmarkEx2A;
   pdfAddBookmarkEx2W: TpdfAddBookmarkEx2W;
   pdfAddButtonImageA: TpdfAddButtonImageA;
   pdfAddButtonImageW: TpdfAddButtonImageW;
   pdfAddButtonImageEx: TpdfAddButtonImageEx;
   pdfAddContinueTextA: TpdfAddContinueTextA;
   pdfAddContinueTextW: TpdfAddContinueTextW;
   pdfAddDeviceNProcessColorants: TpdfAddDeviceNProcessColorants;
   pdfAddDeviceNSeparations: TpdfAddDeviceNSeparations;
   pdfAddFieldToFormAction: TpdfAddFieldToFormAction;
   pdfAddFieldToHideAction: TpdfAddFieldToHideAction;
   pdfAddFileCommentA: TpdfAddFileCommentA;
   pdfAddFileCommentW: TpdfAddFileCommentW;
   pdfAddFontSearchPath: TpdfAddFontSearchPath;
   pdfAddImage: TpdfAddImage;
   pdfAddJavaScriptA: TpdfAddJavaScriptA;
   pdfAddJavaScriptW: TpdfAddJavaScriptW;
   pdfAddLayerToDisplTreeA: TpdfAddLayerToDisplTreeA;
   pdfAddLayerToDisplTreeW: TpdfAddLayerToDisplTreeW;
   pdfAddMaskImage: TpdfAddMaskImage;
   pdfAddObjectToLayer: TpdfAddObjectToLayer;
   pdfAddOCGToAppEvent: TpdfAddOCGToAppEvent;
   pdfAddOutputIntentA: TpdfAddOutputIntentA;
   pdfAddOutputIntentW: TpdfAddOutputIntentW;
   pdfAddOutputIntentEx: TpdfAddOutputIntentEx;
   pdfAddPageLabelA: TpdfAddPageLabelA;
   pdfAddPageLabelW: TpdfAddPageLabelW;
   pdfAddRasImage: TpdfAddRasImage;
   pdfAddValToChoiceFieldA: TpdfAddValToChoiceFieldA;
   pdfAddValToChoiceFieldW: TpdfAddValToChoiceFieldW;
   pdfAppend: TpdfAppend;
   pdfApplyPattern: TpdfApplyPattern;
   pdfApplyShading: TpdfApplyShading;
   pdfAssociateEmbFile: TpdfAssociateEmbFile;
   pdfAttachFileA: TpdfAttachFileA;
   pdfAttachFileW: TpdfAttachFileW;
   pdfAttachFileExA: TpdfAttachFileExA;
   pdfAttachFileExW: TpdfAttachFileExW;
   pdfAutoTemplate: TpdfAutoTemplate;
   pdfBeginClipPath: TpdfBeginClipPath;
   pdfBeginContinueText: TpdfBeginContinueText;
   pdfBeginLayer: TpdfBeginLayer;
   pdfBeginPageTemplate: TpdfBeginPageTemplate;
   pdfBeginPattern: TpdfBeginPattern;
   pdfBeginTemplate: TpdfBeginTemplate;
   pdfBeginTemplateEx: TpdfBeginTemplateEx;
   pdfBeginTransparencyGroup: TpdfBeginTransparencyGroup;
   pdfBezier_1_2_3: TpdfBezier_1_2_3;
   pdfBezier_1_3: TpdfBezier_1_3;
   pdfBezier_2_3: TpdfBezier_2_3;
   pdfCalcWidthHeight: TpdfCalcWidthHeight;
   pdfChangeAnnotNameA: TpdfChangeAnnotNameA;
   pdfChangeAnnotNameW: TpdfChangeAnnotNameW;
   pdfChangeAnnotPos: TpdfChangeAnnotPos;
   pdfChangeBookmarkA: TpdfChangeBookmarkA;
   pdfChangeBookmarkW: TpdfChangeBookmarkW;
   pdfChangeFont: TpdfChangeFont;
   pdfChangeFontSize: TpdfChangeFontSize;
   pdfChangeFontStyle: TpdfChangeFontStyle;
   pdfChangeFontStyleEx: TpdfChangeFontStyleEx;
   pdfChangeJavaScriptA: TpdfChangeJavaScriptA;
   pdfChangeJavaScriptW: TpdfChangeJavaScriptW;
   pdfChangeJavaScriptActionA: TpdfChangeJavaScriptActionA;
   pdfChangeJavaScriptActionW: TpdfChangeJavaScriptActionW;
   pdfChangeJavaScriptNameA: TpdfChangeJavaScriptNameA;
   pdfChangeJavaScriptNameW: TpdfChangeJavaScriptNameW;
   pdfChangeLinkAnnot: TpdfChangeLinkAnnot;
   pdfChangeSeparationColor: TpdfChangeSeparationColor;
   pdfCheckCollection: TpdfCheckCollection;
   pdfCheckConformance: TpdfCheckConformance;
   pdfCheckFieldNames: TpdfCheckFieldNames;
   pdfCircleAnnotA: TpdfCircleAnnotA;
   pdfCircleAnnotW: TpdfCircleAnnotW;
   pdfClearAutoTemplates: TpdfClearAutoTemplates;
   pdfClearErrorLog: TpdfClearErrorLog;
   pdfClearHostFonts: TpdfClearHostFonts;
   pdfClipPath: TpdfClipPath;
   pdfCloseAndSignFile: TpdfCloseAndSignFile;
   pdfCloseAndSignFileEx: TpdfCloseAndSignFileEx;
   pdfCloseAndSignFileExt: TpdfCloseAndSignFileExt;
   pdfCloseFile: TpdfCloseFile;
   pdfCloseFileEx: TpdfCloseFileEx;
   pdfCloseImage: TpdfCloseImage;
   pdfCloseImportFile: TpdfCloseImportFile;
   pdfCloseImportFileEx: TpdfCloseImportFileEx;
   pdfClosePath: TpdfClosePath;
   pdfCloseTag: TpdfCloseTag;
   pdfComputeBBox: TpdfComputeBBox;
   pdfConvColor: TpdfConvColor;
   pdfConvertColors: TpdfConvertColors;
   pdfConvertEMFSpoolA: TpdfConvertEMFSpoolA;
   pdfConvertEMFSpoolW: TpdfConvertEMFSpoolW;
   pdfConvToUnicode: TpdfConvToUnicode;
   pdfCreate3DAnnot: TpdfCreate3DAnnot;
   pdfCreate3DBackground: TpdfCreate3DBackground;
   pdfCreate3DGotoViewAction: TpdfCreate3DGotoViewAction;
   pdfCreate3DProjection: TpdfCreate3DProjection;
   pdfCreate3DView: TpdfCreate3DView;
   pdfCreateAnnotAP: TpdfCreateAnnotAP;
   pdfCreateArticleThreadA: TpdfCreateArticleThreadA;
   pdfCreateArticleThreadW: TpdfCreateArticleThreadW;
   pdfCreateAxialShading: TpdfCreateAxialShading;
   pdfCreateBarcodeField: TpdfCreateBarcodeField;
   pdfCreateButtonA: TpdfCreateButtonA;
   pdfCreateButtonW: TpdfCreateButtonW;
   pdfCreateCheckBox: TpdfCreateCheckBox;
   pdfCreateCIEColorSpace: TpdfCreateCIEColorSpace;
   pdfCreateCollection: TpdfCreateCollection;
   pdfCreateCollectionFieldA: TpdfCreateCollectionFieldA;
   pdfCreateCollectionFieldW: TpdfCreateCollectionFieldW;
   pdfCreateColItemDate: TpdfCreateColItemDate;
   pdfCreateColItemNumber: TpdfCreateColItemNumber;
   pdfCreateColItemStringA: TpdfCreateColItemStringA;
   pdfCreateColItemStringW: TpdfCreateColItemStringW;
   pdfCreateComboBox: TpdfCreateComboBox;
   pdfCreateDeviceNColorSpace: TpdfCreateDeviceNColorSpace;
   pdfCreateExtGState: TpdfCreateExtGState;
   pdfCreateGoToAction: TpdfCreateGoToAction;
   pdfCreateGoToActionEx: TpdfCreateGoToActionEx;
   pdfCreateGoToEActionA: TpdfCreateGoToEActionA;
   pdfCreateGoToEActionW: TpdfCreateGoToEActionW;
   pdfCreateGoToRAction: TpdfCreateGoToRAction;
   pdfCreateGoToRActionW: TpdfCreateGoToRActionW;
   pdfCreateGoToRActionExA: TpdfCreateGoToRActionExA;
   pdfCreateGoToRActionExW: TpdfCreateGoToRActionExW;
   pdfCreateGoToRActionExUA: TpdfCreateGoToRActionExUA;
   pdfCreateGoToRActionExUW: TpdfCreateGoToRActionExUW;
   pdfCreateGroupField: TpdfCreateGroupField;
   pdfCreateHideAction: TpdfCreateHideAction;
   pdfCreateICCBasedColorSpaceA: TpdfCreateICCBasedColorSpaceA;
   pdfCreateICCBasedColorSpaceW: TpdfCreateICCBasedColorSpaceW;
   pdfCreateICCBasedColorSpaceEx: TpdfCreateICCBasedColorSpaceEx;
   pdfCreateImageA: TpdfCreateImageA;
   pdfCreateImageW: TpdfCreateImageW;
   pdfCreateImpDataActionA: TpdfCreateImpDataActionA;
   pdfCreateImpDataActionW: TpdfCreateImpDataActionW;
   pdfCreateIndexedColorSpace: TpdfCreateIndexedColorSpace;
   pdfCreateJSActionA: TpdfCreateJSActionA;
   pdfCreateJSActionW: TpdfCreateJSActionW;
   pdfCreateLaunchAction: TpdfCreateLaunchAction;
   pdfCreateLaunchActionExA: TpdfCreateLaunchActionExA;
   pdfCreateLaunchActionExW: TpdfCreateLaunchActionExW;
   pdfCreateListBox: TpdfCreateListBox;
   pdfCreateNamedAction: TpdfCreateNamedAction;
   pdfCreateNamedDestA: TpdfCreateNamedDestA;
   pdfCreateNamedDestW: TpdfCreateNamedDestW;
   pdfCreateNewPDFA: TpdfCreateNewPDFA;
   pdfCreateNewPDFW: TpdfCreateNewPDFW;
   pdfCreateOCGA: TpdfCreateOCGA;
   pdfCreateOCGW: TpdfCreateOCGW;
   pdfCreateOCMD: TpdfCreateOCMD;
   pdfCreateRadialShading: TpdfCreateRadialShading;
   pdfCreateRadioButton: TpdfCreateRadioButton;
   pdfCreateResetAction: TpdfCreateResetAction;
   pdfCreateSeparationCS: TpdfCreateSeparationCS;
   pdfCreateSetOCGStateAction: TpdfCreateSetOCGStateAction;
   pdfCreateSigField: TpdfCreateSigField;
   pdfCreateSigFieldAP: TpdfCreateSigFieldAP;
   pdfCreateSoftMask: TpdfCreateSoftMask;
   pdfCreateStdPattern: TpdfCreateStdPattern;
   pdfCreateStructureTree: TpdfCreateStructureTree;
   pdfCreateSubmitAction: TpdfCreateSubmitAction;
   pdfCreateTextField: TpdfCreateTextField;
   pdfCreateURIAction: TpdfCreateURIAction;
   pdfDecryptPDFA: TpdfDecryptPDFA;
   pdfDecryptPDFW: TpdfDecryptPDFW;
   pdfDeleteAcroForm: TpdfDeleteAcroForm;
   pdfDeleteActionFromObj: TpdfDeleteActionFromObj;
   pdfDeleteActionFromObjEx: TpdfDeleteActionFromObjEx;
   pdfDeleteAnnotation: TpdfDeleteAnnotation;
   pdfDeleteBookmark: TpdfDeleteBookmark;
   pdfDeleteEmbeddedFile: TpdfDeleteEmbeddedFile;
   pdfDeleteField: TpdfDeleteField;
   pdfDeleteFieldEx: TpdfDeleteFieldEx;
   pdfDeleteJavaScripts: TpdfDeleteJavaScripts;
   pdfDeleteOCGFromAppEvent: TpdfDeleteOCGFromAppEvent;
   pdfDeleteOutputIntent: TpdfDeleteOutputIntent;
   pdfDeletePage: TpdfDeletePage;
   pdfDeletePageLabels: TpdfDeletePageLabels;
   pdfDeleteSeparationInfo: TpdfDeleteSeparationInfo;
   pdfDeleteTemplate: TpdfDeleteTemplate;
   pdfDeleteTemplateEx: TpdfDeleteTemplateEx;
   pdfDeleteXFAForm: TpdfDeleteXFAForm;
   pdfDrawArc: TpdfDrawArc;
   pdfDrawArcEx: TpdfDrawArcEx;
   pdfDrawChord: TpdfDrawChord;
   pdfDrawCircle: TpdfDrawCircle;
   pdfDrawPie: TpdfDrawPie;
   pdfEditPage: TpdfEditPage;
   pdfEditTemplate: TpdfEditTemplate;
   pdfEditTemplate2: TpdfEditTemplate2;
   pdfEllipse: TpdfEllipse;
   pdfEncryptPDFA: TpdfEncryptPDFA;
   pdfEncryptPDFW: TpdfEncryptPDFW;
   pdfEndContinueText: TpdfEndContinueText;
   pdfEndLayer: TpdfEndLayer;
   pdfEndPage: TpdfEndPage;
   pdfEndPattern: TpdfEndPattern;
   pdfEndTemplate: TpdfEndTemplate;
   pdfEnumDocFonts: TpdfEnumDocFonts;
   pdfEnumHostFonts: TpdfEnumHostFonts;
   pdfEnumHostFontsEx: TpdfEnumHostFontsEx;
   pdfExchangeBookmarks: TpdfExchangeBookmarks;
   pdfExchangePages: TpdfExchangePages;
   pdfFileAttachAnnotA: TpdfFileAttachAnnotA;
   pdfFileAttachAnnotW: TpdfFileAttachAnnotW;
   pdfFileLinkA: TpdfFileLinkA;
   pdfFileLinkW: TpdfFileLinkW;
   pdfFindBookmarkA: TpdfFindBookmarkA;
   pdfFindBookmarkW: TpdfFindBookmarkW;
   pdfFindFieldA: TpdfFindFieldA;
   pdfFindFieldW: TpdfFindFieldW;
   pdfFindLinkAnnot: TpdfFindLinkAnnot;
   pdfFindNextBookmark: TpdfFindNextBookmark;  
   pdfFinishSignature: TpdfFinishSignature;
   pdfFlattenAnnots: TpdfFlattenAnnots;
   pdfFlattenForm: TpdfFlattenForm;
   pdfFlushPageContent: TpdfFlushPageContent;
   pdfFlushPages: TpdfFlushPages;
   pdfFreeImageBuffer: TpdfFreeImageBuffer;
   pdfFreeImageObj: TpdfFreeImageObj;
   pdfFreeImageObjEx: TpdfFreeImageObjEx;
   pdfFreePDF: TpdfFreePDF;
   pdfFreeTextAnnotA: TpdfFreeTextAnnotA;
   pdfFreeTextAnnotW: TpdfFreeTextAnnotW;
   pdfFreeUniBuf: TpdfFreeUniBuf;
   pdfGetActionCount: TpdfGetActionCount;
   pdfGetActionType: TpdfGetActionType;
   pdfGetActionTypeEx: TpdfGetActionTypeEx;
   pdfGetActiveFont: TpdfGetActiveFont;
   pdfGetAllocBy: TpdfGetAllocBy;
   pdfGetAnnot: TpdfGetAnnot;
   pdfGetAnnotBBox: TpdfGetAnnotBBox;
   pdfGetAnnotCount: TpdfGetAnnotCount;
   pdfGetAnnotEx: TpdfGetAnnotEx;
   pdfGetAnnotFlags: TpdfGetAnnotFlags;
   pdfGetAnnotLink: TpdfGetAnnotLink;
   pdfGetAnnotType: TpdfGetAnnotType;
   pdfGetAscent: TpdfGetAscent;
   pdfGetBarcodeDict: TpdfGetBarcodeDict;
   pdfGetBBox: TpdfGetBBox;
   pdfGetBidiMode: TpdfGetBidiMode;
   pdfGetBookmark: TpdfGetBookmark;
   pdfGetBookmarkCount: TpdfGetBookmarkCount;
   pdfGetBorderStyle: TpdfGetBorderStyle;
   pdfGetBuffer: TpdfGetBuffer;
   pdfGetCapHeight: TpdfGetCapHeight;
   pdfGetCharacterSpacing: TpdfGetCharacterSpacing;
   pdfGetCheckBoxChar: TpdfGetCheckBoxChar;
   pdfGetCheckBoxCharEx: TpdfGetCheckBoxCharEx;
   pdfGetCheckBoxDefState: TpdfGetCheckBoxDefState;
   pdfGetCMap: TpdfGetCMap;
   pdfGetCMapCount: TpdfGetCMapCount;
   pdfGetColorSpace: TpdfGetColorSpace;
   pdfGetColorSpaceCount: TpdfGetColorSpaceCount;
   pdfGetColorSpaceObj: TpdfGetColorSpaceObj;
   pdfGetColorSpaceObjEx: TpdfGetColorSpaceObjEx;
   pdfGetCompressionFilter: TpdfGetCompressionFilter;
   pdfGetCompressionLevel: TpdfGetCompressionLevel;
   pdfGetContent: TpdfGetContent;
   pdfGetDefBitsPerPixel: TpdfGetDefBitsPerPixel;
   pdfGetDescent: TpdfGetDescent;
   pdfGetDeviceNAttributes: TpdfGetDeviceNAttributes;
   pdfGetDocInfo: TpdfGetDocInfo;
   pdfGetDocInfoCount: TpdfGetDocInfoCount;
   pdfGetDocInfoEx: TpdfGetDocInfoEx;
   pdfGetDocUsesTransparency: TpdfGetDocUsesTransparency;
   pdfGetDrawDirection: TpdfGetDrawDirection;
   pdfGetDynaPDFVersion: TpdfGetDynaPDFVersion;
   pdfGetEmbeddedFile: TpdfGetEmbeddedFile;
   pdfGetEmbeddedFileCount: TpdfGetEmbeddedFileCount;
   pdfGetEMFPatternDistance: TpdfGetEMFPatternDistance;
   pdfGetErrLogMessage: TpdfGetErrLogMessage;
   pdfGetErrLogMessageCount: TpdfGetErrLogMessageCount;
   pdfGetErrorMessage: TpdfGetErrorMessage;
   pdfGetErrorMode: TpdfGetErrorMode;
   pdfGetField: TpdfGetField;
   pdfGetFieldBackColor: TpdfGetFieldBackColor;
   pdfGetFieldBorderColor: TpdfGetFieldBorderColor;
   pdfGetFieldBorderStyle: TpdfGetFieldBorderStyle;
   pdfGetFieldBorderWidth: TpdfGetFieldBorderWidth;
   pdfGetFieldChoiceValue: TpdfGetFieldChoiceValue;
   pdfGetFieldColor: TpdfGetFieldColor;
   pdfGetFieldCount: TpdfGetFieldCount;
   pdfGetFieldEx: TpdfGetFieldEx;
   pdfGetFieldEx2: TpdfGetFieldEx2;
   pdfGetFieldExpValCount: TpdfGetFieldExpValCount;
   pdfGetFieldExpValue: TpdfGetFieldExpValue;
   pdfGetFieldExpValueEx: TpdfGetFieldExpValueEx;
   pdfGetFieldFlags: TpdfGetFieldFlags;
   pdfGetFieldGroupType: TpdfGetFieldGroupType;
   pdfGetFieldHighlightMode: TpdfGetFieldHighlightMode;
   pdfGetFieldIndex: TpdfGetFieldIndex;
   pdfGetFieldMapName: TpdfGetFieldMapName;
   pdfGetFieldName: TpdfGetFieldName;
   pdfGetFieldOrientation: TpdfGetFieldOrientation;
   pdfGetFieldTextAlign: TpdfGetFieldTextAlign;
   pdfGetFieldTextColor: TpdfGetFieldTextColor;
   pdfGetFieldToolTip: TpdfGetFieldToolTip;
   pdfGetFieldType: TpdfGetFieldType;
   pdfGetFillColor: TpdfGetFillColor;
   pdfGetFontCount: TpdfGetFontCount;
   pdfGetFontEx: TpdfGetFontEx;
   pdfGetFontInfoEx: TpdfGetFontInfoEx;
   pdfGetFontOrigin: TpdfGetFontOrigin;
   pdfGetFontSearchOrder: TpdfGetFontSearchOrder;
   pdfGetFontSelMode: TpdfGetFontSelMode;
   pdfGetFontWeight: TpdfGetFontWeight;
   pdfGetFTextHeightA: TpdfGetFTextHeightA;
   pdfGetFTextHeightExA: TpdfGetFTextHeightExA;
   pdfGetFTextHeightExW: TpdfGetFTextHeightExW;
   pdfGetFTextHeightW: TpdfGetFTextHeightW;
   pdfGetGStateFlags: TpdfGetGStateFlags;
   pdfGetIconColor: TpdfGetIconColor;
   pdfGetImageBuffer: TpdfGetImageBuffer;
   pdfGetImageCountA: TpdfGetImageCountA;
   pdfGetImageCountW: TpdfGetImageCountW;
   pdfGetImageCountEx: TpdfGetImageCountEx;
   pdfGetImageHeight: TpdfGetImageHeight;
   pdfGetImageObj: TpdfGetImageObj;
   pdfGetImageObjCount: TpdfGetImageObjCount;
   pdfGetImageObjEx: TpdfGetImageObjEx;
   pdfGetImageWidth: TpdfGetImageWidth;
   pdfGetImportFlags: TpdfGetImportFlags;
   pdfGetInBBox: TpdfGetInBBox;
   pdfGetInDocInfo: TpdfGetInDocInfo;
   pdfGetInDocInfoCount: TpdfGetInDocInfoCount;
   pdfGetInDocInfoEx: TpdfGetInDocInfoEx;
   pdfGetInEncryptionFlags: TpdfGetInEncryptionFlags;
   pdfGetInFieldCount: TpdfGetInFieldCount;
   pdfGetInIsCollection: TpdfGetInIsCollection;
   pdfGetInIsEncrypted: TpdfGetInIsEncrypted;
   pdfGetInIsSigned: TpdfGetInIsSigned;
   pdfGetInIsTrapped: TpdfGetInIsTrapped;
   pdfGetInIsXFAForm: TpdfGetInIsXFAForm;
   pdfGetInMetadata: TpdfGetInMetadata;
   pdfGetInOrientation: TpdfGetInOrientation;
   pdfGetInPageCount: TpdfGetInPageCount;
   pdfGetInPDFVersion: TpdfGetInPDFVersion;
   pdfGetInPrintSettings: TpdfGetInPrintSettings;
   pdfGetInRepairMode: TpdfGetInRepairMode;
   pdfGetIsFixedPitch: TpdfGetIsFixedPitch;
   pdfGetIsTaggingEnabled: TpdfGetIsTaggingEnabled;
   pdfGetItalicAngle: TpdfGetItalicAngle;
   pdfGetJavaScript: TpdfGetJavaScript;
   pdfGetJavaScriptAction: TpdfGetJavaScriptAction;
   pdfGetJavaScriptAction2: TpdfGetJavaScriptAction2;
   pdfGetJavaScriptCount: TpdfGetJavaScriptCount;
   pdfGetJavaScriptEx: TpdfGetJavaScriptEx;
   pdfGetJavaScriptName: TpdfGetJavaScriptName;
   pdfGetJPEGQuality: TpdfGetJPEGQuality;
   pdfGetLanguage: TpdfGetLanguage;
   pdfGetLastTextPosX: TpdfGetLastTextPosX;
   pdfGetLastTextPosY: TpdfGetLastTextPosY;
   pdfGetLeading: TpdfGetLeading;
   pdfGetLineCapStyle: TpdfGetLineCapStyle;
   pdfGetLineJoinStyle: TpdfGetLineJoinStyle;
   pdfGetLineWidth: TpdfGetLineWidth;
   pdfGetLinkHighlightMode: TpdfGetLinkHighlightMode;
   pdfGetLogMetafileSizeA: TpdfGetLogMetafileSizeA;
   pdfGetLogMetafileSizeW: TpdfGetLogMetafileSizeW;
   pdfGetLogMetafileSizeEx: TpdfGetLogMetafileSizeEx;
   pdfGetMatrix: TpdfGetMatrix;
   pdfGetMaxFieldLen: TpdfGetMaxFieldLen;
   pdfGetMeasureObj: TpdfGetMeasureObj;
   pdfGetMetaConvFlags: TpdfGetMetaConvFlags;
   pdfGetMetadata: TpdfGetMetadata;
   pdfGetMissingGlyphs: TpdfGetMissingGlyphs;
   pdfGetMiterLimit: TpdfGetMiterLimit;
   pdfGetNamedDest: TpdfGetNamedDest;
   pdfGetNamedDestCount: TpdfGetNamedDestCount;
   pdfGetNeedAppearance: TpdfGetNeedAppearance;
   pdfGetNumberFormatObj: TpdfGetNumberFormatObj;
   pdfGetObjActionCount: TpdfGetObjActionCount;
   pdfGetOCG: TpdfGetOCG;
   pdfGetOCGContUsage: TpdfGetOCGContUsage;
   pdfGetOCGCount: TpdfGetOCGCount;
   pdfGetOCGUsageUserName: TpdfGetOCGUsageUserName;
   pdfGetOpacity: TpdfGetOpacity;
   pdfGetOrientation: TpdfGetOrientation;
   pdfGetOutputIntent: TpdfGetOutputIntent;
   pdfGetOutputIntentCount: TpdfGetOutputIntentCount;
   pdfGetPageAnnot: TpdfGetPageAnnot;
   pdfGetPageAnnotCount: TpdfGetPageAnnotCount;
   pdfGetPageAnnotEx: TpdfGetPageAnnotEx;
   pdfGetPageBBox: TpdfGetPageBBox;
   pdfGetPageCoords: TpdfGetPageCoords;
   pdfGetPageCount: TpdfGetPageCount;
   pdfGetPageField: TpdfGetPageField;
   pdfGetPageFieldCount: TpdfGetPageFieldCount;
   pdfGetPageFieldEx: TpdfGetPageFieldEx;
   pdfGetPageHeight: TpdfGetPageHeight;
   pdfGetPageLabel: TpdfGetPageLabel;
   pdfGetPageLabelCount: TpdfGetPageLabelCount;
   pdfGetPageLayout: TpdfGetPageLayout;
   pdfGetPageMode: TpdfGetPageMode;
   pdfGetPageNum: TpdfGetPageNum;
   pdfGetPageObject: TpdfGetPageObject;
   pdfGetPageOrientation: TpdfGetPageOrientation;
   pdfGetPageText: TpdfGetPageText;
   pdfGetPageWidth: TpdfGetPageWidth;
   pdfGetPDFVersion: TpdfGetPDFVersion;
   pdfGetPrintSettings: TpdfGetPrintSettings;
   pdfGetPtDataArray: TpdfGetPtDataArray;
   pdfGetPtDataObj: TpdfGetPtDataObj;
   pdfGetResolution: TpdfGetResolution;
   pdfGetSaveNewImageFormat: TpdfGetSaveNewImageFormat;
   pdfGetSeparationInfo: TpdfGetSeparationInfo;
   pdfGetSigDict: TpdfGetSigDict;
   pdfGetStrokeColor: TpdfGetStrokeColor;
   pdfGetSysFontInfo: TpdfGetSysFontInfo;
   pdfGetTabLen: TpdfGetTabLen;
   pdfGetTemplCount: TpdfGetTemplCount;
   pdfGetTemplHandle: TpdfGetTemplHandle;
   pdfGetTemplHeight: TpdfGetTemplHeight;
   pdfGetTemplWidth: TpdfGetTemplWidth;
   pdfGetTextDrawMode: TpdfGetTextDrawMode;
   pdfGetTextFieldValue: TpdfGetTextFieldValue;
   pdfGetTextRect: TpdfGetTextRect;
   pdfGetTextRise: TpdfGetTextRise;
   pdfGetTextScaling: TpdfGetTextScaling;
   pdfGetTextWidthA: TpdfGetTextWidthA;
   pdfGetTextWidthW: TpdfGetTextWidthW;
   pdfGetTextWidthExA: TpdfGetTextWidthExA;
   pdfGetTextWidthExW: TpdfGetTextWidthExW;
   pdfGetTrapped: TpdfGetTrapped;
   pdfGetTransparentColor: TpdfGetTransparentColor;
   pdfGetUseExactPwd: TpdfGetUseExactPwd;
   pdfGetUseGlobalImpFiles: TpdfGetUseGlobalImpFiles;
   pdfGetUserRights: TpdfGetUserRights;
   pdfGetUserUnit: TpdfGetUserUnit;
   pdfGetUseStdFonts: TpdfGetUseStdFonts;
   pdfGetUseSystemFonts: TpdfGetUseSystemFonts;
   pdfGetUseTransparency: TpdfGetUseTransparency;
   pdfGetUseVisibleCoords: TpdfGetUseVisibleCoords;
   pdfGetViewerPrefrences: TpdfGetViewerPrefrences;
   pdfGetViewport: TpdfGetViewport;
   pdfGetViewportCount: TpdfGetViewportCount;
   pdfGetWMFDefExtent: TpdfGetWMFDefExtent;
   pdfGetWMFPixelPerInch: TpdfGetWMFPixelPerInch;
   pdfGetWordSpacing: TpdfGetWordSpacing;
   pdfHaveOpenDoc: TpdfHaveOpenDoc;
   pdfHaveOpenPage: TpdfHaveOpenPage;
   pdfHighlightAnnotA: TpdfHighlightAnnotA;
   pdfHighlightAnnotW: TpdfHighlightAnnotW;
   pdfImportBookmarks: TpdfImportBookmarks;
   pdfImportCatalogObjects: TpdfImportCatalogObjects;
   pdfImportDocInfo: TpdfImportDocInfo;
   pdfImportPage: TpdfImportPage;
   pdfImportPageEx: TpdfImportPageEx;
   pdfImportPDFFile: TpdfImportPDFFile;
   pdfInitColorManagement: TpdfInitColorManagement;
   pdfInitExtGState: TpdfInitExtGState;
   pdfInitOCGContUsage: TpdfInitOCGContUsage;
   pdfInitStack: TpdfInitStack;
   pdfInsertBMPFromBuffer: TpdfInsertBMPFromBuffer;
   pdfInsertBMPFromHandle: TpdfInsertBMPFromHandle;
   pdfInsertBookmarkA: TpdfInsertBookmarkA;
   pdfInsertBookmarkW: TpdfInsertBookmarkW;
   pdfInsertBookmarkExA: TpdfInsertBookmarkExA;
   pdfInsertBookmarkExW: TpdfInsertBookmarkExW;
   pdfInsertImage: TpdfInsertImage;
   pdfInsertImageEx: TpdfInsertImageEx;
   pdfInsertImageExW: TpdfInsertImageExW;
   pdfInsertImageFromBuffer: TpdfInsertImageFromBuffer;
   pdfInsertMetafileA: TpdfInsertMetafileA;
   pdfInsertMetafileW: TpdfInsertMetafileW;
   pdfInsertMetafileEx: TpdfInsertMetafileEx;
   pdfInsertMetafileExtA: TpdfInsertMetafileExtA;
   pdfInsertMetafileExtW: TpdfInsertMetafileExtW;
   pdfInsertMetafileExtEx: TpdfInsertMetafileExtEx;
   pdfInsertMetafileFromHandle: TpdfInsertMetafileFromHandle;
   pdfInsertMetafileFromHandleEx: TpdfInsertMetafileFromHandleEx;
   pdfInsertRawImage: TpdfInsertRawImage;
   pdfInsertRawImageEx: TpdfInsertRawImageEx;
   pdfIsBidiText: TpdfIsBidiText;
   pdfIsColorPage: TpdfIsColorPage;
   pdfIsEmptyPage: TpdfIsEmptyPage;
   pdfLineAnnotA: TpdfLineAnnotA;
   pdfLineAnnotW: TpdfLineAnnotW;
   pdfLineTo: TpdfLineTo;
   pdfLoadCMap: TpdfLoadCMap;
   pdfLoadFDFDataA: TpdfLoadFDFDataA;
   pdfLoadFDFDataW: TpdfLoadFDFDataW;
   pdfLoadFDFDataEx: TpdfLoadFDFDataEx;
   pdfLoadFont: TpdfLoadFont;
   pdfLoadFontExA: TpdfLoadFontExA;
   pdfLoadFontExW: TpdfLoadFontExW;
   pdfLockLayer: TpdfLockLayer;
   pdfMovePage: TpdfMovePage;
   pdfMoveTo: TpdfMoveTo;
   pdfOpenImportBuffer: TpdfOpenImportBuffer;
   pdfOpenImportFileA: TpdfOpenImportFileA;
   pdfOpenImportFileW: TpdfOpenImportFileW;
   pdfOpenOutputFileA: TpdfOpenOutputFileA;
   pdfOpenOutputFileW: TpdfOpenOutputFileW;
   pdfOpenTagA: TpdfOpenTagA;
   pdfOpenTagW: TpdfOpenTagW;
   pdfOptimize: TpdfOptimize;
   pdfPageLink: TpdfPageLink;
   pdfPageLink2: TpdfPageLink2;
   pdfPageLink3A: TpdfPageLink3A;
   pdfPageLink3W: TpdfPageLink3W;
   pdfPageLinkEx: TpdfPageLinkEx;
   pdfParseContent: TpdfParseContent;
   pdfPlaceImage: TpdfPlaceImage;
   pdfPlaceSigFieldValidateIcon: TpdfPlaceSigFieldValidateIcon;
   pdfPlaceTemplate: TpdfPlaceTemplate;
   pdfPlaceTemplateEx: TpdfPlaceTemplateEx;
   pdfPlaceTemplByMatrix: TpdfPlaceTemplByMatrix;
   pdfPolygonAnnotA: TpdfPolygonAnnotA;
   pdfPolygonAnnotW: TpdfPolygonAnnotW;
   pdfPolyLineAnnotA: TpdfPolyLineAnnotA;
   pdfPolyLineAnnotW: TpdfPolyLineAnnotW;
   pdfReadImageFormatA: TpdfReadImageFormatA;
   pdfReadImageFormatW: TpdfReadImageFormatW;
   pdfReadImageFormat2A: TpdfReadImageFormat2A;
   pdfReadImageFormat2W: TpdfReadImageFormat2W;
   pdfReadImageFormatEx: TpdfReadImageFormatEx;
   pdfReadImageFormatFromBuffer: TpdfReadImageFormatFromBuffer;
   pdfReadImageResolutionA: TpdfReadImageResolutionA;
   pdfReadImageResolutionW: TpdfReadImageResolutionW;
   pdfReadImageResolutionEx: TpdfReadImageResolutionEx;
   pdfRectangle: TpdfRectangle;
   pdfReEncryptPDFA: TpdfReEncryptPDFA;
   pdfReEncryptPDFW: TpdfReEncryptPDFW;
   pdfRenameSpotColor: TpdfRenameSpotColor;
   pdfRenderAnnotOrField: TpdfRenderAnnotOrField;
   pdfRenderPage: TpdfRenderPage;
   pdfRenderPageEx: TpdfRenderPageEx;
   pdfRenderPageToImageA: TpdfRenderPageToImageA;
   pdfRenderPageToImageW: TpdfRenderPageToImageW;
   pdfRenderPDFFileA: TpdfRenderPDFFileA;
   pdfRenderPDFFileW: TpdfRenderPDFFileW;
   pdfReOpenImportFile: TpdfReOpenImportFile;
   pdfReplaceFontA: TpdfReplaceFontA;
   pdfReplaceFontW: TpdfReplaceFontW;
   pdfReplaceFontExA: TpdfReplaceFontExA;
   pdfReplaceFontExW: TpdfReplaceFontExW;
   pdfReplaceICCProfileA: TpdfReplaceICCProfileA;
   pdfReplaceICCProfileW: TpdfReplaceICCProfileW;
   pdfReplacePageTextA: TpdfReplacePageTextA;
   pdfReplacePageTextExA: TpdfReplacePageTextExA;
   pdfReplacePageTextExW: TpdfReplacePageTextExW;
   pdfResetLineDashPattern: TpdfResetLineDashPattern;
   pdfRestoreGraphicState: TpdfRestoreGraphicState;
   pdfRotateCoords: TpdfRotateCoords;
   pdfRoundRect: TpdfRoundRect;
   pdfRoundRectEx: TpdfRoundRectEx;
   pdfSaveGraphicState: TpdfSaveGraphicState;
   pdfScaleCoords: TpdfScaleCoords;
   pdfSelfTest: TpdfSelfTest;
   pdfSet3DAnnotProps: TpdfSet3DAnnotProps;
   pdfSet3DAnnotScriptA: TpdfSet3DAnnotScriptA;
   pdfSetAllocBy: TpdfSetAllocBy;
   pdfSetAnnotBorderStyle: TpdfSetAnnotBorderStyle;
   pdfSetAnnotBorderWidth: TpdfSetAnnotBorderWidth;
   pdfSetAnnotColor: TpdfSetAnnotColor;
   pdfSetAnnotFlags: TpdfSetAnnotFlags;
   pdfSetAnnotFlagsEx: TpdfSetAnnotFlagsEx;
   pdfSetAnnotHighlightMode: TpdfSetAnnotHighlightMode;
   pdfSetAnnotIcon: TpdfSetAnnotIcon;
   pdfSetAnnotLineEndStyle: TpdfSetAnnotLineEndStyle;
   pdfSetAnnotOpenState: TpdfSetAnnotOpenState;
   pdfSetAnnotOrFieldDate: TpdfSetAnnotOrFieldDate;
   pdfSetAnnotStringA: TpdfSetAnnotStringA;
   pdfSetAnnotStringW: TpdfSetAnnotStringW;
   pdfSetAnnotSubjectA: TpdfSetAnnotSubjectA;
   pdfSetAnnotSubjectW: TpdfSetAnnotSubjectW;
   pdfSetBBox: TpdfSetBBox;
   pdfSetBidiMode: TpdfSetBidiMode;
   pdfSetBookmarkDest: TpdfSetBookmarkDest;
   pdfSetBookmarkStyle: TpdfSetBookmarkStyle;
   pdfSetBorderStyle: TpdfSetBorderStyle;
   pdfSetCharacterSpacing: TpdfSetCharacterSpacing;
   pdfSetCheckBoxChar: TpdfSetCheckBoxChar;
   pdfSetCheckBoxDefState: TpdfSetCheckBoxDefState;
   pdfSetCheckBoxState: TpdfSetCheckBoxState;
   pdfSetCIDFontA: TpdfSetCIDFontA;
   pdfSetCIDFontW: TpdfSetCIDFontW;
   pdfSetCMapDirA: TpdfSetCMapDirA;
   pdfSetCMapDirW: TpdfSetCMapDirW;
   pdfSetColDefFile: TpdfSetColDefFile;
   pdfSetColorMask: TpdfSetColorMask;
   pdfSetColors: TpdfSetColors;
   pdfSetColorSpace: TpdfSetColorSpace;
   pdfSetColSortField: TpdfSetColSortField;
   pdfSetCompressionFilter: TpdfSetCompressionFilter;
   pdfSetCompressionLevel: TpdfSetCompressionLevel;
   pdfSetContent: TpdfSetContent;
   pdfSetDateTimeFormat: TpdfSetDateTimeFormat;
   pdfSetDefBitsPerPixel: TpdfSetDefBitsPerPixel;
   pdfSetDocInfoA: TpdfSetDocInfoA;
   pdfSetDocInfoW: TpdfSetDocInfoW;
   pdfSetDocInfoExA: TpdfSetDocInfoExA;
   pdfSetDocInfoExW: TpdfSetDocInfoExW;
   pdfSetDrawDirection: TpdfSetDrawDirection;
   pdfSetEMFFrameDPI: TpdfSetEMFFrameDPI;
   pdfSetEMFPatternDistance: TpdfSetEMFPatternDistance;
   pdfSetErrorMode: TpdfSetErrorMode;
   pdfSetExtColorSpace: TpdfSetExtColorSpace;
   pdfSetExtFillColorSpace: TpdfSetExtFillColorSpace;
   pdfSetExtGState: TpdfSetExtGState;
   pdfSetExtStrokeColorSpace: TpdfSetExtStrokeColorSpace;
   pdfSetFieldBackColor: TpdfSetFieldBackColor;
   pdfSetFieldBBox: TpdfSetFieldBBox;
   pdfSetFieldBorderColor: TpdfSetFieldBorderColor;
   pdfSetFieldBorderStyle: TpdfSetFieldBorderStyle;
   pdfSetFieldBorderWidth: TpdfSetFieldBorderWidth;
   pdfSetFieldColor: TpdfSetFieldColor;
   pdfSetFieldExpValue: TpdfSetFieldExpValue;
   pdfSetFieldExpValueEx: TpdfSetFieldExpValueEx;
   pdfSetFieldFlags: TpdfSetFieldFlags;
   pdfSetFieldFontA: TpdfSetFieldFontA;
   pdfSetFieldFontW: TpdfSetFieldFontW;
   pdfSetFieldFontEx: TpdfSetFieldFontEx;
   pdfSetFieldFontSize: TpdfSetFieldFontSize;
   pdfSetFieldHighlightMode: TpdfSetFieldHighlightMode;
   pdfSetFieldIndex: TpdfSetFieldIndex;
   pdfSetFieldMapNameA: TpdfSetFieldMapNameA;
   pdfSetFieldMapNameW: TpdfSetFieldMapNameW;
   pdfSetFieldName: TpdfSetFieldName;
   pdfSetFieldOrientation: TpdfSetFieldOrientation;
   pdfSetFieldTextAlign: TpdfSetFieldTextAlign;
   pdfSetFieldTextColor: TpdfSetFieldTextColor;
   pdfSetFieldToolTipA: TpdfSetFieldToolTipA;
   pdfSetFieldToolTipW: TpdfSetFieldToolTipW;
   pdfSetFillColor: TpdfSetFillColor;
   pdfSetFillColorEx: TpdfSetFillColorEx;
   pdfSetFillColorF: TpdfSetFillColorF;
   pdfSetFillColorSpace: TpdfSetFillColorSpace;
   pdfSetFontA: TpdfSetFontA;
   pdfSetFontW: TpdfSetFontW;
   pdfSetFontExA: TpdfSetFontExA;
   pdfSetFontExW: TpdfSetFontExW;
   pdfSetFontOrigin: TpdfSetFontOrigin;
   pdfSetFontSearchOrder: TpdfSetFontSearchOrder;
   pdfSetFontSearchOrderEx: TpdfSetFontSearchOrderEx;
   pdfSetFontSelMode: TpdfSetFontSelMode;
   pdfSetFontWeight: TpdfSetFontWeight;
   pdfSetGStateFlags: TpdfSetGStateFlags;
   pdfSetIconColor: TpdfSetIconColor;
   pdfSetImportFlags: TpdfSetImportFlags;
   pdfSetImportFlags2: TpdfSetImportFlags2;
   pdfSetItalicAngle: TpdfSetItalicAngle;
   pdfSetJPEGQuality: TpdfSetJPEGQuality;
   pdfSetLanguage: TpdfSetLanguage;
   pdfSetLeading: TpdfSetLeading;
   pdfSetLicenseKey: TpdfSetLicenseKey;
   pdfSetLineAnnotParms: TpdfSetLineAnnotParms;
   pdfSetLineCapStyle: TpdfSetLineCapStyle;
   pdfSetLineDashPattern: TpdfSetLineDashPattern;
   pdfSetLineDashPatternEx: TpdfSetLineDashPatternEx;
   pdfSetLineJoinStyle: TpdfSetLineJoinStyle;
   pdfSetLineWidth: TpdfSetLineWidth;
   pdfSetLinkHighlightMode: TpdfSetLinkHighlightMode;
   pdfSetListFont: TpdfSetListFont;
   pdfSetMatrix: TpdfSetMatrix;
   pdfSetMaxErrLogMsgCount: TpdfSetMaxErrLogMsgCount;
   pdfSetMaxFieldLen: TpdfSetMaxFieldLen;
   pdfSetMetaConvFlags: TpdfSetMetaConvFlags;
   pdfSetMetadata: TpdfSetMetadata;
   pdfSetMiterLimit: TpdfSetMiterLimit;
   pdfSetNeedAppearance: TpdfSetNeedAppearance;
   pdfSetNumberFormat: TpdfSetNumberFormat;
   pdfSetOCGContUsage: TpdfSetOCGContUsage;
   pdfSetOCGState: TpdfSetOCGState;
   pdfSetOnErrorProc: TpdfSetOnErrorProc;
   pdfSetOnPageBreakProc: TpdfSetOnPageBreakProc;
   pdfSetOpacity: TpdfSetOpacity;
   pdfSetOrientation: TpdfSetOrientation;
   pdfSetOrientationEx: TpdfSetOrientationEx;
   pdfSetPageCoords: TpdfSetPageCoords;
   pdfSetPageFormat: TpdfSetPageFormat;
   pdfSetPageHeight: TpdfSetPageHeight;
   pdfSetPageLayout: TpdfSetPageLayout;
   pdfSetPageMode: TpdfSetPageMode;
   pdfSetPageWidth: TpdfSetPageWidth;
   pdfSetPDFVersion: TpdfSetPDFVersion;
   pdfSetPrintSettings: TpdfSetPrintSettings;
   pdfSetProgressProc: TpdfSetProgressProc;
   pdfSetResolution: TpdfSetResolution;
   pdfSetSaveNewImageFormat: TpdfSetSaveNewImageFormat;
   pdfSetSeparationInfo: TpdfSetSeparationInfo;
   pdfSetStrokeColor: TpdfSetStrokeColor;
   pdfSetStrokeColorEx: TpdfSetStrokeColorEx;
   pdfSetStrokeColorF: TpdfSetStrokeColorF;
   pdfSetStrokeColorSpace: TpdfSetStrokeColorSpace;
   pdfSetTabLen: TpdfSetTabLen;
   pdfSetTextDrawMode: TpdfSetTextDrawMode;
   pdfSetTextFieldValueA: TpdfSetTextFieldValueA;
   pdfSetTextFieldValueW: TpdfSetTextFieldValueW;
   pdfSetTextFieldValueExA: TpdfSetTextFieldValueExA;
   pdfSetTextFieldValueExW: TpdfSetTextFieldValueExW;
   pdfSetTextRect: TpdfSetTextRect;
   pdfSetTextRise: TpdfSetTextRise;
   pdfSetTextScaling: TpdfSetTextScaling;
   pdfSetTrapped: TpdfSetTrapped;
   pdfSetTransparentColor: TpdfSetTransparentColor;
   pdfSetUseExactPwd: TpdfSetUseExactPwd;
   pdfSetUseGlobalImpFiles: TpdfSetUseGlobalImpFiles;
   pdfSetUserUnit: TpdfSetUserUnit;
   pdfSetUseStdFonts: TpdfSetUseStdFonts;
   pdfSetUseSwapFile: TpdfSetUseSwapFile;
   pdfSetUseSwapFileEx: TpdfSetUseSwapFileEx;
   pdfSetUseSystemFonts: TpdfSetUseSystemFonts;
   pdfSetUseTransparency: TpdfSetUseTransparency;
   pdfSetUseVisibleCoords: TpdfSetUseVisibleCoords;
   pdfSetViewerPreferences: TpdfSetViewerPreferences;
   pdfSetWMFDefExtent: TpdfSetWMFDefExtent;
   pdfSetWMFPixelPerInch: TpdfSetWMFPixelPerInch;
   pdfSetWordSpacing: TpdfSetWordSpacing;
   pdfSkewCoords: TpdfSkewCoords;
   pdfSortFieldsByIndex: TpdfSortFieldsByIndex;
   pdfSortFieldsByName: TpdfSortFieldsByName;
   pdfSquareAnnotA: TpdfSquareAnnotA;
   pdfSquareAnnotW: TpdfSquareAnnotW;
   pdfStampAnnotA: TpdfStampAnnotA;
   pdfStampAnnotW: TpdfStampAnnotW;
   pdfStrokePath: TpdfStrokePath;
   pdfTestGlyphsExA: TpdfTestGlyphsExA;
   pdfTestGlyphsExW: TpdfTestGlyphsExW;
   pdfTextAnnotA: TpdfTextAnnotA;
   pdfTextAnnotW: TpdfTextAnnotW;
   pdfTranslateCoords: TpdfTranslateCoords;
   pdfTriangle: TpdfTriangle;
   pdfUnLockLayer: TpdfUnLockLayer;
   pdfUTF16ToUTF32: TpdfUTF16ToUTF32;
   pdfUTF16ToUTF32Ex: TpdfUTF16ToUTF32Ex;
   pdfUTF32ToUTF16: TpdfUTF32ToUTF16;
   pdfUTF32ToUTF16Ex: TpdfUTF32ToUTF16Ex;
   pdfWebLinkA: TpdfWebLinkA;
   pdfWebLinkW: TpdfWebLinkW;
   pdfWriteAngleTextA: TpdfWriteAngleTextA;
   pdfWriteAngleTextW: TpdfWriteAngleTextW;
   pdfWriteFTextA: TpdfWriteFTextA;
   pdfWriteFTextW: TpdfWriteFTextW;
   pdfWriteFTextExA: TpdfWriteFTextExA;
   pdfWriteFTextExW: TpdfWriteFTextExW;
   pdfWriteTextA: TpdfWriteTextA;
   pdfWriteTextW: TpdfWriteTextW;
   pdfWriteTextExA: TpdfWriteTextExA;
   pdfWriteTextExW: TpdfWriteTextExW;
   pdfWriteTextMatrixA: TpdfWriteTextMatrixA;
   pdfWriteTextMatrixW: TpdfWriteTextMatrixW;
   pdfWriteTextMatrixExA: TpdfWriteTextMatrixExA;
   pdfWriteTextMatrixExW: TpdfWriteTextMatrixExW;
   FDLL_Handle: HMODULE;
   procedure NewPDF();
   procedure LoadFunction(var Func: Pointer; const FuncName: String);
end;

type TPDFPageCache = class(TObject)
 public
   constructor Create(PDFInst: TPDF; PixFmt: TPDFPixFormat; HBorder, VBorder, BackColor: Cardinal; Priority: TPDFThreadPriority); overload;
   destructor  Destroy(); override;
   procedure ChangeBackColor(Color: Cardinal);
   procedure CloseFile();
   function  ExecBookmark(Handle: Cardinal; var NewX, NewY: Integer; var NewZoom: Single; var NewPageScale: TPDFPageScale; Action: Pointer): TUpdBmkAction;
   function  GetCurrPage(): Integer;
   function  GetCurrZoom(): Single;
   function  GetDefPageLayout(): TPageLayout;
   function  GetPageAt(ScrollX, ScrollY: Integer; X, Y: Integer): Integer;
   function  GetPageLayout(): TPageLayout;
   function  GetPageScale(): TPDFPageScale;
   function  GetRotate(): Integer;
   function  GetScrollLineDelta(Vertical: Boolean): Cardinal;
   function  GetScrollPos(Vertical: Boolean; PageNum: Cardinal): Integer;
   function  GetScrollRange(Vertical: Boolean; var Max, SmallChange, LargeChange: Integer): Boolean;
   function  InitBaseObjects(Width, Height: Integer; Flags: TInitCacheFlags): Integer;
   function  InitColorManagement(Profiles: PPDFColorProfiles; Flags: TPDFInitCMFlags): Boolean;
   function  MouseDown(X, Y: Integer): TPDFCursor;
   function  MouseMove(hWnd: HWND; LeftBtnDown: Boolean; var ScrollX, ScrollY: Integer; X, Y: Integer): TUpdScrollbar;
   function  Paint(DC: HDC; var ScrollX, ScrollY: Integer): TUpdScrollbar;
   function  ProcessErrors(): Boolean;
   procedure ResetMousePos();
   function  Resize(Width, Height: Integer): Boolean;
   function  Scroll(Vertical: Boolean; ScrollCode: Integer; var ScrollX, ScrollY: Integer): TUpdScrollbar;
{$ifdef DELPHI6_OR_HIGHER}
   function SetCMapDir(const Path: AnsiString; Flags: TLoadCMapFlags): Integer; overload;
   function SetCMapDir(const Path: WideString; Flags: TLoadCMapFlags): Integer; overload;
{$endif}
   function  SetCMapDirA(const Path: AnsiString; Flags: TLoadCMapFlags): Integer;
   function  SetCMapDirW(const Path: WideString; Flags: TLoadCMapFlags): Integer;
   procedure SetDefPageLayout(Value: TPageLayout);
   function  SetMinLineWidth(Value: Single): Boolean;
   procedure SetPageLayout(Value: TPageLayout);
   procedure SetPageScale(Value: TPDFPageScale);
   procedure SetRotate(Value: Integer);
   function  SetScrollLineDelta(Vertical: Boolean; Value: Cardinal): Boolean;
   procedure SetThreadPriority(UpdateThread, RenderThread: TPDFThreadPriority);
   function  Zoom(Value: Single; var HorzPos, VertPos: Integer): Boolean;
 protected
   m_Cache:                IPGC;
   rasChangeBackColor:     TrasChangeBackColor;
   rasCloseFile:           TrasCloseFile;
   rasCreatePageCache:     TrasCreatePageCache;
   rasDeletePageCache:     TrasDeletePageCache;
   rasExecBookmark:        TrasExecBookmark;
   rasGetCurrPage:         TrasGetCurrPage;
   rasGetCurrZoom:         TrasGetCurrZoom;
   rasGetDefPageLayout:    TrasGetDefPageLayout;
   rasGetPageAt:           TrasGetPageAt;
   rasGetPageLayout:       TrasGetPageLayout;
   rasGetPageScale:        TrasGetPageScale;
   rasGetRotate:           TrasGetRotate;
   rasGetScrollLineDelta:  TrasGetScrollLineDelta;
   rasGetScrollPos:        TrasGetScrollPos;
   rasGetScrollRange:      TrasGetScrollRange;
   rasInitBaseObjects:     TrasInitBaseObjects;
   rasInitColorManagement: TrasInitColorManagement;
   rasInitialize:          TrasInitialize;
   rasMouseDown:           TrasMouseDown;
   rasMouseMove:           TrasMouseMove;
   rasPaint:               TrasPaint;
   rasProcessErrors:       TrasProcessErrors;
   rasResetMousePos:       TrasResetMousePos;
   rasResize:              TrasResize;
   rasScroll:              TrasScroll;
   rasSetCMapDirA:         TrasSetCMapDirA;
   rasSetCMapDirW:         TrasSetCMapDirW;
   rasSetDefPageLayout:    TrasSetDefPageLayout;
   rasSetMinLineWidth:     TrasSetMinLineWidth;
   rasSetPageLayout:       TrasSetPageLayout;
   rasSetPageScale:        TrasSetPageScale;
   rasSetRotate:           TrasSetRotate;
   rasSetScrollLineDelta:  TrasSetScrollLineDelta;
   rasSetThreadPriority:   TrasSetThreadPriority;
   rasZoom:                TrasZoom;
end;

type TPDFTable = class(TObject)
 public
   constructor Create(PDFInst: TPDF; AllocRows, NumCols: Cardinal; Width, DefRowHeight: Single); overload;
   destructor  Destroy(); override;
   function  AddColumn(Left: Boolean; Width: Single): Integer;
   function  AddRow(Height: Single): Integer;
   function  AddRows(Count: Cardinal; Height: Single): Integer;
   procedure ClearColumn(Col: Cardinal; Types: TDeleteContent);
   procedure ClearContent(Types: TDeleteContent);
   procedure ClearRow(Row: Cardinal; Types: TDeleteContent);
   procedure DeleteCol(Col: Cardinal);
   procedure DeleteRow(Row: Cardinal);
   procedure DeleteRows();
   function  DrawTable(x, y, MaxHeight: Single): Single;
   function  GetFirstRow(): Integer;
   function  GetNextHeight(MaxHeight: Single; NextRow: PInteger): Single;
   function  GetNextRow(): Integer;
   function  GetNumCols(): Integer;
   function  GetNumRows(): Integer;
   function  GetTableHeight(): Single;
   function  GetTableWidth(): Single;
   function  HaveMore(): Boolean;
   function  SetBoxProperty(Row, Col: Integer; PropType: TTableBoxProperty; Left, Right, Top, Bottom: Single): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function  SetCellImage(Row, Col: Integer; ForeGround: Boolean; HAlign, VAlign: TCellAlign; Width, Height: Single; const Image: AnsiString; Index: Cardinal): Boolean; overload;
   function  SetCellImage(Row, Col: Integer; ForeGround: Boolean; HAlign, VAlign: TCellAlign; Width, Height: Single; const Image: WideString; Index: Cardinal): Boolean; overload;
{$endif}
   function  SetCellImageA(Row, Col: Integer; ForeGround: Boolean; HAlign, VAlign: TCellAlign; Width, Height: Single; const Image: AnsiString; Index: Cardinal): Boolean;
   function  SetCellImageW(Row, Col: Integer; ForeGround: Boolean; HAlign, VAlign: TCellAlign; Width, Height: Single; const Image: WideString; Index: Cardinal): Boolean;
   function  SetCellImageEx(Row, Col: Integer; ForeGround: Boolean; HAlign, VAlign: TCellAlign; Width, Height: Single; const Buffer: Pointer; BufSize, Index: Cardinal): Boolean;
   function  SetCellOrientation(Row, Col, Orientation: Integer): Boolean;
   function  SetCellTable(Row, Col: Cardinal; HAlign, VAlign: TCellAlign; const SubTable: TPDFTable): Boolean;
   function  SetCellTemplate(Row, Col: Integer; ForeGround: Boolean; HAlign, VAlign: TCellAlign; TmplHandle: Cardinal; Width, Height: Single): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function  SetCellText(Row, Col: Integer; HAlign: TTextAlign; VAlign: TCellAlign; const Text: AnsiString): Boolean; overload;
   function  SetCellText(Row, Col: Integer; HAlign: TTextAlign; VAlign: TCellAlign; const Text: WideString): Boolean; overload;
{$endif}
   function  SetCellTextA(Row, Col: Integer; HAlign: TTextAlign; VAlign: TCellAlign; const Text: AnsiString): Boolean;
   function  SetCellTextW(Row, Col: Integer; HAlign: TTextAlign; VAlign: TCellAlign; const Text: WideString): Boolean;
   function  SetColor(Row, Col: Integer; ClrType: TTableColor; CS: TPDFColorSpace; Color: Cardinal): Boolean;
   function  SetColorEx(Row, Col: Integer; ClrType: TTableColor; Color: PSingle; NumComps: Cardinal; CS: TExtColorSpace; Handle: Integer): Boolean;
   function  SetColWidth(Col: Cardinal; Width: Single; ExtTable: Boolean): Boolean;
   function  SetFlags(Row, Col: Integer; Flags: TTableFlags): Boolean;
{$ifdef DELPHI6_OR_HIGHER}
   function  SetFont(Row, Col: Integer; const Name: AnsiString; Style: TFStyle; Embed: Boolean; CP: TCodepage): Boolean; overload;
   function  SetFont(Row, Col: Integer; const Name: WideString; Style: TFStyle; Embed: Boolean; CP: TCodepage): Boolean; overload;
{$endif}
   function  SetFontA(Row, Col: Integer; const Name: AnsiString; Style: TFStyle; Embed: Boolean; CP: TCodepage): Boolean;
   function  SetFontW(Row, Col: Integer; const Name: WideString; Style: TFStyle; Embed: Boolean; CP: TCodepage): Boolean;
   function  SetFontSelMode(Row, Col: Integer; Mode: TFontSelMode): Boolean;
   function  SetFontSize(Row, Col: Integer; Value: Single): Boolean;
   function  SetGridWidth(Horz, Vert: Single): Boolean;
   function  SetRowHeight(Row: Integer; Value: Single): Boolean;
   procedure SetTableWidth(Value: Single; AdjustType: TColumnAdjust; MinColWidth: Single);
 protected
   tblAddColumn:          TtblAddColumn;
   tblAddRow:             TtblAddRow;
   tblAddRows:            TtblAddRows;
   tblClearColumn:        TtblClearColumn;
   tblClearContent:       TtblClearContent;
   tblClearRow:           TtblClearRow;
   tblCreateTable:        TtblCreateTable;
   tblDeleteCol:          TtblDeleteCol;
   tblDeleteRow:          TtblDeleteRow;
   tblDeleteRows:         TtblDeleteRows;
   tblDeleteTable:        TtblDeleteTable;
   tblDrawTable:          TtblDrawTable;
   tblGetFirstRow:        TtblGetFirstRow;
   tblGetNextHeight:      TtblGetNextHeight;
   tblGetNextRow:         TtblGetNextRow;
   tblGetNumCols:         TtblGetNumCols;
   tblGetNumRows:         TtblGetNumRows;
   tblGetTableHeight:     TtblGetTableHeight;
   tblGetTableWidth:      TtblGetTableWidth;
   tblHaveMore:           TtblHaveMore;
   tblSetBoxProperty:     TtblSetBoxProperty;
   tblSetCellImageA:      TtblSetCellImageA;
   tblSetCellImageW:      TtblSetCellImageW;
   tblSetCellImageEx:     TtblSetCellImageEx;
   tblSetCellOrientation: TtblSetCellOrientation;
   tblSetCellTable:       TtblSetCellTable;
   tblSetCellTemplate:    TtblSetCellTemplate;
   tblSetCellTextA:       TtblSetCellTextA;
   tblSetCellTextW:       TtblSetCellTextW;
   tblSetColor:           TtblSetColor;
   tblSetColorEx:         TtblSetColorEx;
   tblSetColWidth:        TtblSetColWidth;
   tblSetFlags:           TtblSetFlags;
   tblSetFontA:           TtblSetFontA;
   tblSetFontW:           TtblSetFontW;
   tblSetFontSelMode:     TtblSetFontSelMode;
   tblSetFontSize:        TtblSetFontSize;
   tblSetGridWidth:       TtblSetGridWidth;
   tblSetRowHeight:       TtblSetRowHeight;
   tblSetTableWidth:      TtblSetTableWidth;
   m_Table:               ITBL;

   function GetInstancePtr(): ITBL;
end;

implementation

uses SysUtils;

{$IFDEF POSIX}
function MakeWord(A, B: Byte): Word; inline;
begin
  Result := A or B shl 8;
end;
{$ENDIF POSIX}

{ TPDFPageCache }

constructor TPDFPageCache.Create(PDFInst: TPDF; PixFmt: TPDFPixFormat; HBorder, VBorder, BackColor: Cardinal; Priority: TPDFThreadPriority);
var libHandle: HMODULE;
begin
   libHandle              := PDFInst.GetLibHandle();
   rasChangeBackColor     := GetProcAddress(libHandle, 'rasChangeBackColor');
   rasCloseFile           := GetProcAddress(libHandle, 'rasCloseFile');
   rasCreatePageCache     := GetProcAddress(libHandle, 'rasCreatePageCache');
   rasDeletePageCache     := GetProcAddress(libHandle, 'rasDeletePageCache');
   rasExecBookmark        := GetProcAddress(libHandle, 'rasExecBookmark');
   rasGetCurrPage         := GetProcAddress(libHandle, 'rasGetCurrPage');
   rasGetCurrZoom         := GetProcAddress(libHandle, 'rasGetCurrZoom');
   rasGetDefPageLayout    := GetProcAddress(libHandle, 'rasGetDefPageLayout');
   rasGetPageAt           := GetProcAddress(libHandle, 'rasGetPageAt');
   rasGetPageLayout       := GetProcAddress(libHandle, 'rasGetPageLayout');
   rasGetPageScale        := GetProcAddress(libHandle, 'rasGetPageScale');
   rasGetRotate           := GetProcAddress(libHandle, 'rasGetRotate');
   rasGetScrollPos        := GetProcAddress(libHandle, 'rasGetScrollPos');
   rasGetScrollRange      := GetProcAddress(libHandle, 'rasGetScrollRange');
   rasInitBaseObjects     := GetProcAddress(libHandle, 'rasInitBaseObjects');
   rasInitColorManagement := GetProcAddress(libHandle, 'rasInitColorManagement');
   rasInitialize          := GetProcAddress(libHandle, 'rasInitialize');
   rasMouseDown           := GetProcAddress(libHandle, 'rasMouseDown');
   rasMouseMove           := GetProcAddress(libHandle, 'rasMouseMove');
   rasPaint               := GetProcAddress(libHandle, 'rasPaint');
   rasProcessErrors       := GetProcAddress(libHandle, 'rasProcessErrors');
   rasResetMousePos       := GetProcAddress(libHandle, 'rasResetMousePos');
   rasResize              := GetProcAddress(libHandle, 'rasResize');
   rasScroll              := GetProcAddress(libHandle, 'rasScroll');
   rasSetCMapDirA         := GetProcAddress(libHandle, 'rasSetCMapDirA');
   rasSetCMapDirW         := GetProcAddress(libHandle, 'rasSetCMapDirW');
   rasSetDefPageLayout    := GetProcAddress(libHandle, 'rasSetDefPageLayout');
   rasSetMinLineWidth     := GetProcAddress(libHandle, 'rasSetMinLineWidth');
   rasSetPageLayout       := GetProcAddress(libHandle, 'rasSetPageLayout');
   rasSetPageScale        := GetProcAddress(libHandle, 'rasSetPageScale');
   rasSetRotate           := GetProcAddress(libHandle, 'rasSetRotate');
   rasSetScrollLineDelta  := GetProcAddress(libHandle, 'rasSetScrollLineDelta');
   rasSetThreadPriority   := GetProcAddress(libHandle, 'rasSetThreadPriority');
   rasZoom                := GetProcAddress(libHandle, 'rasZoom');
   if @rasCreatePageCache = nil then raise Exception.Create('Cannot load the Page Cache API! Check the DynaPDF version!');
   m_Cache := rasCreatePageCache(PDFInst.PDFInstance, PixFmt, HBorder, VBorder, BackColor);
   if m_Cache = nil then raise Exception.Create('Cannot create page cache instance!');
   if not rasInitialize(m_Cache, Priority) then raise Exception.Create('Out of memory!');
end;

destructor TPDFPageCache.Destroy;
begin
   if m_Cache <> nil then rasDeletePageCache(m_Cache);
   inherited;
end;

procedure TPDFPageCache.ChangeBackColor(Color: Cardinal);
begin
   rasChangeBackColor(m_Cache, Color);
end;

procedure TPDFPageCache.CloseFile();
begin
   rasCloseFile(m_Cache);
end;

function TPDFPageCache.ExecBookmark(Handle: Cardinal; var NewX, NewY: Integer; var NewZoom: Single; var NewPageScale: TPDFPageScale; Action: Pointer): TUpdBmkAction;
begin
   Result := rasExecBookmark(m_Cache, Handle, NewX, NewY, NewZoom, NewPageScale, Action);
end;

function TPDFPageCache.GetCurrPage: Integer;
begin
   Result := rasGetCurrPage(m_Cache);
end;

function TPDFPageCache.GetCurrZoom: Single;
begin
   Result := rasGetCurrZoom(m_Cache);
end;

function TPDFPageCache.GetDefPageLayout: TPageLayout;
begin
   Result := rasGetDefPageLayout(m_Cache);
end;

function TPDFPageCache.GetPageAt(ScrollX, ScrollY, X, Y: Integer): Integer;
begin
   Result := rasGetPageAt(m_Cache, ScrollX, ScrollY, X, Y);
end;

function TPDFPageCache.GetPageLayout: TPageLayout;
begin
   Result := rasGetPageLayout(m_Cache);
end;

function TPDFPageCache.GetPageScale: TPDFPageScale;
begin
   Result := rasGetPageScale(m_Cache);
end;

function TPDFPageCache.GetRotate: Integer;
begin
   Result := rasGetRotate(m_Cache);
end;

function TPDFPageCache.GetScrollLineDelta(Vertical: Boolean): Cardinal;
begin
   Result := rasGetScrollLineDelta(m_Cache, Vertical);
end;

function TPDFPageCache.GetScrollPos(Vertical: Boolean; PageNum: Cardinal): Integer;
begin
   Result := rasGetScrollPos(m_Cache, Vertical, PageNum);
end;

function TPDFPageCache.GetScrollRange(Vertical: Boolean; var Max, SmallChange, LargeChange: Integer): Boolean;
begin
   Result := rasGetScrollRange(m_Cache, Vertical, Max, SmallChange, LargeChange);
end;

function TPDFPageCache.InitBaseObjects(Width, Height: Integer; Flags: TInitCacheFlags): Integer;
begin
   Result := rasInitBaseObjects(m_Cache, Width, Height, Flags);
end;

function TPDFPageCache.InitColorManagement(Profiles: PPDFColorProfiles; Flags: TPDFInitCMFlags): Boolean;
begin
   Result := rasInitColorManagement(m_Cache, Profiles, Flags);
end;

function TPDFPageCache.MouseDown(X, Y: Integer): TPDFCursor;
begin
   Result := rasMouseDown(m_Cache, X, Y);
end;

function TPDFPageCache.MouseMove(hWnd: HWND; LeftBtnDown: Boolean; var ScrollX, ScrollY: Integer; X, Y: Integer): TUpdScrollbar;
begin
   Result := rasMouseMove(m_Cache, hWnd, LeftBtnDown, ScrollX, ScrollY, X, Y);
end;

function TPDFPageCache.Paint(DC: HDC; var ScrollX, ScrollY: Integer): TUpdScrollbar;
begin
   Result := rasPaint(m_Cache, DC, ScrollX, ScrollY);
end;

function TPDFPageCache.ProcessErrors;
begin
   Result := rasProcessErrors(m_Cache);
end;

procedure TPDFPageCache.ResetMousePos;
begin
   rasResetMousePos(m_Cache);
end;

function TPDFPageCache.Resize(Width, Height: Integer): Boolean;
begin
   Result := rasResize(m_Cache, Width, Height);
end;

function TPDFPageCache.Scroll(Vertical: Boolean; ScrollCode: Integer; var ScrollX, ScrollY: Integer): TUpdScrollbar;
begin
   Result := rasScroll(m_Cache, Vertical, ScrollCode, ScrollX, ScrollY);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDFPageCache.SetCMapDir(const Path: AnsiString; Flags: TLoadCMapFlags): Integer;
begin
   Result := rasSetCMapDirA(m_Cache, PAnsiChar(Path), Flags);
end;

function TPDFPageCache.SetCMapDir(const Path: WideString; Flags: TLoadCMapFlags): Integer;
begin
   Result := rasSetCMapDirW(m_Cache, PWideChar(Path), Flags);
end;
{$endif}

function TPDFPageCache.SetCMapDirA(const Path: AnsiString; Flags: TLoadCMapFlags): Integer;
begin
   Result := rasSetCMapDirA(m_Cache, PAnsiChar(Path), Flags);
end;

function TPDFPageCache.SetCMapDirW(const Path: WideString; Flags: TLoadCMapFlags): Integer;
begin
   Result := rasSetCMapDirW(m_Cache, PWideChar(Path), Flags);
end;

procedure TPDFPageCache.SetDefPageLayout(Value: TPageLayout);
begin
   rasSetDefPageLayout(m_Cache, Value);
end;

function TPDFPageCache.SetMinLineWidth(Value: Single): Boolean;
begin
   Result := rasSetMinLineWidth(m_Cache, Value);
end;

procedure TPDFPageCache.SetPageLayout(Value: TPageLayout);
begin
   rasSetPageLayout(m_Cache, Value);
end;

procedure TPDFPageCache.SetPageScale(Value: TPDFPageScale);
begin
   rasSetPageScale(m_Cache, Value);
end;

procedure TPDFPageCache.SetRotate(Value: Integer);
begin
   rasSetRotate(m_Cache, Value);
end;

function TPDFPageCache.SetScrollLineDelta(Vertical: Boolean; Value: Cardinal): Boolean;
begin
   Result := rasSetScrollLineDelta(m_Cache, Vertical, Value);
end;

procedure TPDFPageCache.SetThreadPriority(UpdateThread, RenderThread: TPDFThreadPriority);
begin
   rasSetThreadPriority(m_Cache, UpdateThread, RenderThread);
end;

function TPDFPageCache.Zoom(Value: Single; var HorzPos, VertPos: Integer): Boolean;
begin
   Result := rasZoom(m_Cache, Value, HorzPos, VertPos);
end;

{--------------------------------------------------------------------------------------------------------------------------------------------------------------}

{ TPDFTable }

constructor TPDFTable.Create(PDFInst: TPDF; AllocRows, NumCols: Cardinal; Width, DefRowHeight: Single);
var libHandle: HMODULE;
begin
   libHandle             := PDFInst.GetLibHandle();
   tblAddColumn          := GetProcAddress(libHandle, 'tblAddColumn');
   tblAddRow             := GetProcAddress(libHandle, 'tblAddRow');
   tblAddRows            := GetProcAddress(libHandle, 'tblAddRows');
   tblClearColumn        := GetProcAddress(libHandle, 'tblClearColumn');
   tblClearContent       := GetProcAddress(libHandle, 'tblClearContent');
   tblClearRow           := GetProcAddress(libHandle, 'tblClearRow');
   tblCreateTable        := GetProcAddress(libHandle, 'tblCreateTable');
   tblDeleteCol          := GetProcAddress(libHandle, 'tblDeleteCol');
   tblDeleteRow          := GetProcAddress(libHandle, 'tblDeleteRow');
   tblDeleteRows         := GetProcAddress(libHandle, 'tblDeleteRows');
   tblDeleteTable        := GetProcAddress(libHandle, 'tblDeleteTable');
   tblDrawTable          := GetProcAddress(libHandle, 'tblDrawTable');
   tblGetFirstRow        := GetProcAddress(libHandle, 'tblGetFirstRow');
   tblGetNextHeight      := GetProcAddress(libHandle, 'tblGetNextHeight');
   tblGetNextRow         := GetProcAddress(libHandle, 'tblGetNextRow');
   tblGetNumCols         := GetProcAddress(libHandle, 'tblGetNumCols');
   tblGetNumRows         := GetProcAddress(libHandle, 'tblGetNumRows');
   tblGetTableHeight     := GetProcAddress(libHandle, 'tblGetTableHeight');
   tblGetTableWidth      := GetProcAddress(libHandle, 'tblGetTableWidth');
   tblHaveMore           := GetProcAddress(libHandle, 'tblHaveMore');
   tblSetBoxProperty     := GetProcAddress(libHandle, 'tblSetBoxProperty');
   tblSetCellImageA      := GetProcAddress(libHandle, 'tblSetCellImageA');
   tblSetCellImageW      := GetProcAddress(libHandle, 'tblSetCellImageW');
   tblSetCellImageEx     := GetProcAddress(libHandle, 'tblSetCellImageEx');
   tblSetCellOrientation := GetProcAddress(libHandle, 'tblSetCellOrientation');
   tblSetCellTable       := GetProcAddress(libHandle, 'tblSetCellTable');
   tblSetCellTemplate    := GetProcAddress(libHandle, 'tblSetCellTemplate');
   tblSetCellTextA       := GetProcAddress(libHandle, 'tblSetCellTextA');
   tblSetCellTextW       := GetProcAddress(libHandle, 'tblSetCellTextW');
   tblSetColor           := GetProcAddress(libHandle, 'tblSetColor');
   tblSetColorEx         := GetProcAddress(libHandle, 'tblSetColorEx');
   tblSetColWidth        := GetProcAddress(libHandle, 'tblSetColWidth');
   tblSetFlags           := GetProcAddress(libHandle, 'tblSetFlags');
   tblSetFontA           := GetProcAddress(libHandle, 'tblSetFontA');
   tblSetFontW           := GetProcAddress(libHandle, 'tblSetFontW');
   tblSetFontSelMode     := GetProcAddress(libHandle, 'tblSetFontSelMode');
   tblSetFontSize        := GetProcAddress(libHandle, 'tblSetFontSize');
   tblSetGridWidth       := GetProcAddress(libHandle, 'tblSetGridWidth');
   tblSetRowHeight       := GetProcAddress(libHandle, 'tblSetRowHeight');
   tblSetTableWidth      := GetProcAddress(libHandle, 'tblSetTableWidth');

   if @tblCreateTable = nil then raise Exception.Create('Cannot load Table API! Check the DynaPDF version!');
   m_Table := tblCreateTable(PDFInst.PDFInstance, AllocRows, NumCols, Width, DefRowHeight);
   if m_Table = nil then raise Exception.Create('Out of memory!');
end;

destructor TPDFTable.Destroy;
begin
   if m_Table <> nil then tblDeleteTable(m_Table);
   inherited;
end;

function TPDFTable.AddColumn(Left: Boolean; Width: Single): Integer;
begin
   Result := tblAddColumn(m_Table, Left, Width);
end;

function TPDFTable.AddRow(Height: Single): Integer;
begin
   Result := tblAddRow(m_Table, Height);
end;

function TPDFTable.AddRows(Count: Cardinal; Height: Single): Integer;
begin
   Result := tblAddRows(m_Table, Count, Height);
end;

procedure TPDFTable.ClearColumn(Col: Cardinal; Types: TDeleteContent);
begin
   tblClearColumn(m_Table, Col, Types);
end;

procedure TPDFTable.ClearContent(Types: TDeleteContent);
begin
   tblClearContent(m_Table, Types);
end;

procedure TPDFTable.ClearRow(Row: Cardinal; Types: TDeleteContent);
begin
   tblClearRow(m_Table, Row, Types);
end;

procedure TPDFTable.DeleteCol(Col: Cardinal);
begin
   tblDeleteCol(m_Table, Col);
end;

procedure TPDFTable.DeleteRow(Row: Cardinal);
begin
   tblDeleteRow(m_Table, Row);
end;

procedure TPDFTable.DeleteRows;
begin
   tblDeleteRows(m_Table);
end;

function TPDFTable.DrawTable(x, y, MaxHeight: Single): Single;
begin
   Result := tblDrawTable(m_Table, x, y, MaxHeight);
end;

function TPDFTable.GetFirstRow: Integer;
begin
   Result := tblGetFirstRow(m_Table);
end;

function TPDFTable.GetInstancePtr: ITBL;
begin
   Result := m_Table;
end;

function TPDFTable.GetNextHeight(MaxHeight: Single; NextRow: PInteger): Single;
begin
   Result := tblGetNextHeight(m_Table, MaxHeight, NextRow);
end;

function TPDFTable.GetNextRow: Integer;
begin
   Result := tblGetNextRow(m_Table);
end;

function TPDFTable.GetNumCols: Integer;
begin
   Result := tblGetNumCols(m_Table);
end;

function TPDFTable.GetNumRows: Integer;
begin
   Result := tblGetNumRows(m_Table);
end;

function TPDFTable.GetTableHeight: Single;
begin
   Result := tblGetTableHeight(m_Table);
end;

function TPDFTable.GetTableWidth: Single;
begin
   Result := tblGetTableWidth(m_Table);
end;

function TPDFTable.HaveMore: Boolean;
begin
   Result := tblHaveMore(m_Table);
end;

function TPDFTable.SetBoxProperty(Row, Col: Integer; PropType: TTableBoxProperty; Left, Right, Top, Bottom: Single): Boolean;
begin
   Result := tblSetBoxProperty(m_Table, Row, Col, PropType, Left, Right, Top, Bottom);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDFTable.SetCellImage(Row, Col: Integer; ForeGround: Boolean; HAlign, VAlign: TCellAlign; Width, Height: Single; const Image: AnsiString; Index: Cardinal): Boolean;
begin
   Result := tblSetCellImageA(m_Table, Row, Col, ForeGround, HAlign, VAlign, Width, Height, PAnsiChar(Image), Index);
end;

function TPDFTable.SetCellImage(Row, Col: Integer; ForeGround: Boolean; HAlign, VAlign: TCellAlign; Width, Height: Single; const Image: WideString; Index: Cardinal): Boolean;
begin
   Result := tblSetCellImageW(m_Table, Row, Col, ForeGround, HAlign, VAlign, Width, Height, PWideChar(Image), Index);
end;
{$endif}

function TPDFTable.SetCellImageA(Row, Col: Integer; ForeGround: Boolean; HAlign, VAlign: TCellAlign; Width, Height: Single; const Image: AnsiString; Index: Cardinal): Boolean;
begin
   Result := tblSetCellImageA(m_Table, Row, Col, ForeGround, HAlign, VAlign, Width, Height, PAnsiChar(Image), Index);
end;

function TPDFTable.SetCellImageW(Row, Col: Integer; ForeGround: Boolean; HAlign, VAlign: TCellAlign; Width, Height: Single; const Image: WideString; Index: Cardinal): Boolean;
begin
   Result := tblSetCellImageW(m_Table, Row, Col, ForeGround, HAlign, VAlign, Width, Height, PWideChar(Image), Index);
end;

function TPDFTable.SetCellImageEx(Row, Col: Integer; ForeGround: Boolean; HAlign, VAlign: TCellAlign; Width, Height: Single; const Buffer: Pointer; BufSize, Index: Cardinal): Boolean;
begin
   Result := tblSetCellImageEx(m_Table, Row, Col, ForeGround, HAlign, VAlign, Width, Height, Buffer, BufSize, Index);
end;

function TPDFTable.SetCellOrientation(Row, Col, Orientation: Integer): Boolean;
begin
   Result := tblSetCellOrientation(m_Table, Row, Col, Orientation);
end;

function TPDFTable.SetCellTable(Row, Col: Cardinal; HAlign, VAlign: TCellAlign; const SubTable: TPDFTable): Boolean;
begin
   Result := tblSetCellTable(m_Table, Row, Col, HAlign, VAlign, SubTable.GetInstancePtr());
end;

function TPDFTable.SetCellTemplate(Row, Col: Integer; ForeGround: Boolean; HAlign, VAlign: TCellAlign; TmplHandle: Cardinal; Width, Height: Single): Boolean;
begin
   Result := tblSetCellTemplate(m_Table, Row, Col, ForeGround, HAlign, VAlign, TmplHandle, Width, Height);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDFTable.SetCellText(Row, Col: Integer; HAlign: TTextAlign; VAlign: TCellAlign; const Text: AnsiString): Boolean;
begin
   Result := tblSetCellTextA(m_Table, Row, Col, HAlign, VAlign, PAnsiChar(Text), Length(Text));
end;

function TPDFTable.SetCellText(Row, Col: Integer; HAlign: TTextAlign; VAlign: TCellAlign; const Text: WideString): Boolean;
begin
   Result := tblSetCellTextW(m_Table, Row, Col, HAlign, VAlign, PWideChar(Text), Length(Text));
end;
{$endif}

function TPDFTable.SetCellTextA(Row, Col: Integer; HAlign: TTextAlign; VAlign: TCellAlign; const Text: AnsiString): Boolean;
begin
   Result := tblSetCellTextA(m_Table, Row, Col, HAlign, VAlign, PAnsiChar(Text), Length(Text));
end;

function TPDFTable.SetCellTextW(Row, Col: Integer; HAlign: TTextAlign; VAlign: TCellAlign; const Text: WideString): Boolean;
begin
   Result := tblSetCellTextW(m_Table, Row, Col, HAlign, VAlign, PWideChar(Text), Length(Text));
end;

function TPDFTable.SetColor(Row, Col: Integer; ClrType: TTableColor; CS: TPDFColorSpace; Color: Cardinal): Boolean;
begin
   Result := tblSetColor(m_Table, Row, Col, ClrType, CS, Color);
end;

function TPDFTable.SetColorEx(Row, Col: Integer; ClrType: TTableColor; Color: PSingle; NumComps: Cardinal; CS: TExtColorSpace; Handle: Integer): Boolean;
begin
   Result := tblSetColorEx(m_Table, Row, Col, ClrType, Color, NumComps, CS, Handle);
end;

function TPDFTable.SetColWidth(Col: Cardinal; Width: Single; ExtTable: Boolean): Boolean;
begin
   Result := tblSetColWidth(m_Table, Col, Width, ExtTable);
end;

function TPDFTable.SetFlags(Row, Col: Integer; Flags: TTableFlags): Boolean;
begin
   Result := tblSetFlags(m_Table, Row, Col, Flags);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDFTable.SetFont(Row, Col: Integer; const Name: AnsiString; Style: TFStyle; Embed: Boolean; CP: TCodepage): Boolean;
begin
   Result := tblSetFontA(m_Table, Row, Col, PAnsiChar(Name), Style, Embed, CP);
end;

function TPDFTable.SetFont(Row, Col: Integer; const Name: WideString; Style: TFStyle; Embed: Boolean; CP: TCodepage): Boolean;
begin
   Result := tblSetFontW(m_Table, Row, Col, PWideChar(Name), Style, Embed, CP);
end;
{$endif}

function TPDFTable.SetFontA(Row, Col: Integer; const Name: AnsiString; Style: TFStyle; Embed: Boolean; CP: TCodepage): Boolean;
begin
   Result := tblSetFontA(m_Table, Row, Col, PAnsiChar(Name), Style, Embed, CP);
end;

function TPDFTable.SetFontW(Row, Col: Integer; const Name: WideString; Style: TFStyle; Embed: Boolean; CP: TCodepage): Boolean;
begin
   Result := tblSetFontW(m_Table, Row, Col, PWideChar(Name), Style, Embed, CP);
end;

function TPDFTable.SetFontSelMode(Row, Col: Integer; Mode: TFontSelMode): Boolean;
begin
   Result := tblSetFontSelMode(m_Table, Row, Col, Mode);
end;

function TPDFTable.SetFontSize(Row, Col: Integer; Value: Single): Boolean;
begin
   Result := tblSetFontSize(m_Table, Row, Col, Value);
end;

function TPDFTable.SetGridWidth(Horz, Vert: Single): Boolean;
begin
   Result := tblSetGridWidth(m_Table, Horz, Vert);
end;

function TPDFTable.SetRowHeight(Row: Integer; Value: Single): Boolean;
begin
   Result := tblSetRowHeight(m_Table, Row, Value);
end;

procedure TPDFTable.SetTableWidth(Value: Single; AdjustType: TColumnAdjust; MinColWidth: Single);
begin
   tblSetTableWidth(m_Table, Value, AdjustType, MinColWidth);
end;

{--------------------------------------------------------------------------------------------------------------------------------------------------------------}

{ TPDF }

// The constructor was changed in Version 2.0.49.485 to achieve compability with Delphi 2009 or higher.
{$IFDEF MSWINDOWS}
   constructor TPDF.Create(const LibName: WideString = 'dynapdf.dll');
{$ELSE}
   {$IFDEF MACOS}
      constructor TPDF.Create(const LibName: WideString = 'libdynapdf.dylib');
   {$ELSE}
      constructor TPDF.Create(const LibName: WideString = 'libdynapdf.so');
   {$ENDIF}
{$ENDIF}

begin
{$IFNDEF FPC}
	FDLL_Handle := LoadLibraryW(PWideChar(LibName));
{$ELSE}
	FDLL_Handle := LoadLibrary(LibName);
{$ENDIF}
   if FDLL_Handle = 0 then raise Exception.Create('Error: Cannot find '+ LibName);
   NewPDF;
end;

destructor TPDF.Destroy;
var pdfDeletePDF: TpdfDeletePDF;
begin
   if FDLL_Handle = 0 then Exit;
   // To avoid memory leaks it is highly recommended to delete the used instance before unloading the library.
   pdfDeletePDF := GetProcAddress(FDLL_Handle, 'pdfDeletePDF');
   if @pdfDeletePDF = nil then begin
      // Maybe a wrong library loaded?
      // If this function cannot be found then most other functions should not be found too.
      // We assume that this error was already handled...
      FreeLibrary(FDLL_Handle);
      Exit;
   end;
   pdfDeletePDF(FInstance);
   FreeLibrary(FDLL_Handle);
   inherited;
end;

procedure TPDF.LoadFunction(var Func: Pointer; const FuncName: String);
begin
   if Func = nil then begin
   {$IFDEF DELPHI_UNICODE}
      Func := GetProcAddress(FDLL_Handle, PWideChar(FuncName));
   {$ELSE}
      Func := GetProcAddress(FDLL_Handle, PAnsiChar(FuncName));
   {$ENDIF}
      if Func = nil then raise Exception.Create(Format('Error loading function: "%s"', [FuncName]));
   end;
end;

procedure TPDF.Abort(RasPtr: Pointer);
begin
   LoadFunction(@rasAbort, 'rasAbort');
   rasAbort(RasPtr);
end;

function TPDF.AddActionToObj(ObjType: TObjType; Event: TObjEvent; ActHandle, ObjHandle: Cardinal): Integer;
begin
   LoadFunction(@pdfAddActionToObj, 'pdfAddActionToObj');
   Result := pdfAddActionToObj(FInstance, Integer(ObjType), Integer(Event), ActHandle, ObjHandle);
end;

function TPDF.AddArticle(PosX, PosY, Width, Height: Double): Integer;
begin
   LoadFunction(@pdfAddArticle, 'pdfAddArticle');
   Result := pdfAddArticle(FInstance, PosX, PosY, Width, Height);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.AddBookmark(const Title: AnsiString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkA, 'pdfAddBookmarkA');
   Result := pdfAddBookmarkA(FInstance, PAnsiChar(Title), Parent, DestPage, Integer(Open));
end;

function TPDF.AddBookmark(const Title: WideString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkW, 'pdfAddBookmarkW');
   Result := pdfAddBookmarkW(FInstance, PWideChar(Title), Parent, DestPage, Integer(Open));
end;
{$endif}

function TPDF.AddBookmarkA(const Title: AnsiString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkA, 'pdfAddBookmarkA');
   Result := pdfAddBookmarkA(FInstance, PAnsiChar(Title), Parent, DestPage, Integer(Open));
end;

function TPDF.AddBookmarkW(const Title: WideString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkW, 'pdfAddBookmarkW');
   Result := pdfAddBookmarkW(FInstance, PWideChar(Title), Parent, DestPage, Integer(Open));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.AddBookmarkEx(const Title: AnsiString; Parent: Integer; NamedDest: Cardinal; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkExA, 'pdfAddBookmarkExA');
   Result := pdfAddBookmarkExA(FInstance, PAnsiChar(Title), Parent, NamedDest, Open);
end;

function TPDF.AddBookmarkEx(const Title: WideString; Parent: Integer; NamedDest: Cardinal; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkExW, 'pdfAddBookmarkExW');
   Result := pdfAddBookmarkExW(FInstance, PWideChar(Title), Parent, NamedDest, Open);
end;
{$endif}

function TPDF.AddBookmarkExA(const Title: AnsiString; Parent: Integer; NamedDest: Cardinal; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkExA, 'pdfAddBookmarkExA');
   Result := pdfAddBookmarkExA(FInstance, PAnsiChar(Title), Parent, NamedDest, Open);
end;

function TPDF.AddBookmarkExW(const Title: WideString; Parent: Integer; NamedDest: Cardinal; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkExW, 'pdfAddBookmarkExW');
   Result := pdfAddBookmarkExW(FInstance, PWideChar(Title), Parent, NamedDest, Open);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.AddBookmarkEx2(const Title: AnsiString; Parent: Integer; const NamedDest: AnsiString; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkEx2A, 'pdfAddBookmarkEx2A');
   Result := pdfAddBookmarkEx2A(FInstance, PAnsiChar(Title), Parent, PAnsiChar(NamedDest), false, Open);
end;

function TPDF.AddBookmarkEx2(const Title: AnsiString; Parent: Integer; const NamedDest: WideString; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkEx2A, 'pdfAddBookmarkEx2A');
   Result := pdfAddBookmarkEx2A(FInstance, PAnsiChar(Title), Parent, PWideChar(NamedDest), true, Open);
end;

function TPDF.AddBookmarkEx2(const Title: WideString; Parent: Integer; const NamedDest: AnsiString; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkEx2W, 'pdfAddBookmarkEx2W');
   Result := pdfAddBookmarkEx2W(FInstance, PWideChar(Title), Parent, PAnsiChar(NamedDest), false, Open);
end;

function TPDF.AddBookmarkEx2(const Title: WideString; Parent: Integer; const NamedDest: WideString; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkEx2W, 'pdfAddBookmarkEx2W');
   Result := pdfAddBookmarkEx2W(FInstance, PWideChar(Title), Parent, PWideChar(NamedDest), true, Open);
end;
{$endif}

function TPDF.AddBookmarkEx2AA(const Title: AnsiString; Parent: Integer; const NamedDest: AnsiString; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkEx2A, 'pdfAddBookmarkEx2A');
   Result := pdfAddBookmarkEx2A(FInstance, PAnsiChar(Title), Parent, PAnsiChar(NamedDest), false, Open);
end;

function TPDF.AddBookmarkEx2AW(const Title: AnsiString; Parent: Integer; const NamedDest: WideString; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkEx2A, 'pdfAddBookmarkEx2A');
   Result := pdfAddBookmarkEx2A(FInstance, PAnsiChar(Title), Parent, PWideChar(NamedDest), true, Open);
end;

function TPDF.AddBookmarkEx2WA(const Title: WideString; Parent: Integer; const NamedDest: AnsiString; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkEx2W, 'pdfAddBookmarkEx2W');
   Result := pdfAddBookmarkEx2W(FInstance, PWideChar(Title), Parent, PAnsiChar(NamedDest), false, Open);
end;

function TPDF.AddBookmarkEx2WW(const Title: WideString; Parent: Integer; const NamedDest: WideString; Open: Boolean): Integer;
begin
   LoadFunction(@pdfAddBookmarkEx2W, 'pdfAddBookmarkEx2W');
   Result := pdfAddBookmarkEx2W(FInstance, PWideChar(Title), Parent, PWideChar(NamedDest), true, Open);
end;

function TPDF.AddButtonImage(BtnHandle: Cardinal; State: TButtonState; const Caption, ImgFile: AnsiString): Boolean;
begin
   LoadFunction(@pdfAddButtonImageA, 'pdfAddButtonImageA');
   Result := pdfAddButtonImageA(FInstance, BtnHandle, Integer(State), PAnsiChar(Caption), PAnsiChar(ImgFile));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.AddButtonImage(BtnHandle: Cardinal; State: TButtonState; const Caption, ImgFile: WideString): Boolean;
begin
   LoadFunction(@pdfAddButtonImageW, 'pdfAddButtonImageW');
   Result := pdfAddButtonImageW(FInstance, BtnHandle, Integer(State), PWideChar(Caption), PWideChar(ImgFile));
end;
{$endif}

function TPDF.AddButtonImageA(BtnHandle: Cardinal; State: TButtonState; const Caption, ImgFile: AnsiString): Boolean;
begin
   LoadFunction(@pdfAddButtonImageA, 'pdfAddButtonImageA');
   Result := pdfAddButtonImageA(FInstance, BtnHandle, Integer(State), PAnsiChar(Caption), PAnsiChar(ImgFile));
end;

function TPDF.AddButtonImageW(BtnHandle: Cardinal; State: TButtonState; const Caption, ImgFile: WideString): Boolean;
begin
   LoadFunction(@pdfAddButtonImageW, 'pdfAddButtonImageW');
   Result := pdfAddButtonImageW(FInstance, BtnHandle, Integer(State), PWideChar(Caption), PWideChar(ImgFile));
end;

function TPDF.AddButtonImageEx(BtnHandle: Cardinal; State: TButtonState; const Caption: AnsiString; Handle: HBITMAP): Boolean;
begin
   LoadFunction(@pdfAddButtonImageEx, 'pdfAddButtonImageEx');
   Result := pdfAddButtonImageEx(FInstance, BtnHandle, Integer(State), PAnsiChar(Caption), Pointer(Handle));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.AddContinueText(const AText: AnsiString): Integer;
begin
   LoadFunction(@pdfAddContinueTextA, 'pdfAddContinueTextA');
   Result := pdfAddContinueTextA(FInstance, PAnsiChar(AText));
end;

function TPDF.AddContinueText(const AText: WideString): Integer;
begin
   LoadFunction(@pdfAddContinueTextW, 'pdfAddContinueTextW');
   Result := pdfAddContinueTextW(FInstance, PWideChar(AText));
end;
{$endif}

function TPDF.AddContinueTextA(const AText: AnsiString): Integer;
begin
   LoadFunction(@pdfAddContinueTextA, 'pdfAddContinueTextA');
   Result := pdfAddContinueTextA(FInstance, PAnsiChar(AText));
end;

function TPDF.AddContinueTextW(const AText: WideString): Integer;
begin
   LoadFunction(@pdfAddContinueTextW, 'pdfAddContinueTextW');
   Result := pdfAddContinueTextW(FInstance, PWideChar(AText));
end;

function TPDF.AddDeviceNProcessColorants(DeviceNCS: Cardinal; const Colorants: Array of AnsiString; NumColorants: Cardinal; ProcessCS: TExtColorSpace; Handle: Integer): Boolean;
begin
   LoadFunction(@pdfAddDeviceNProcessColorants, 'pdfAddDeviceNProcessColorants');
   Result := pdfAddDeviceNProcessColorants(FInstance, DeviceNCS, @Colorants, NumColorants, ProcessCS, Handle);
end;

function TPDF.AddDeviceNSeparations(DeviceNCS: Cardinal; const Colorants: Array of AnsiString; SeparationCS: Array of Integer; NumColorants: Cardinal): Boolean;
begin
   LoadFunction(@pdfAddDeviceNSeparations, 'pdfAddDeviceNSeparations');
   Result := pdfAddDeviceNSeparations(FInstance, DeviceNCS, @Colorants, @SeparationCS, NumColorants);
end;

function TPDF.AddFieldToFormAction(Action, AField: Cardinal; Include: Boolean): Integer;
begin
   LoadFunction(@pdfAddFieldToFormAction, 'pdfAddFieldToFormAction');
   Result := pdfAddFieldToFormAction(FInstance, Action, AField, Include);
end;

function TPDF.AddFieldToHideAction(HideAct, AField: Cardinal): Integer;
begin
   LoadFunction(@pdfAddFieldToHideAction, 'pdfAddFieldToHideAction');
   Result := pdfAddFieldToHideAction(FInstance, HideAct, AField);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.AddFileComment(const AText: AnsiString): Boolean;
begin
   LoadFunction(@pdfAddFileCommentA, 'pdfAddFileCommentA');
   Result := pdfAddFileCommentA(FInstance, PAnsiChar(AText));
end;

function TPDF.AddFileComment(const AText: WideString): Boolean;
begin
   LoadFunction(@pdfAddFileCommentW, 'pdfAddFileCommentW');
   Result := pdfAddFileCommentW(FInstance, PWideChar(AText));
end;
{$endif}

function TPDF.AddFileCommentA(const AText: AnsiString): Boolean;
begin
   LoadFunction(@pdfAddFileCommentA, 'pdfAddFileCommentA');
   Result := pdfAddFileCommentA(FInstance, PAnsiChar(AText));
end;

function TPDF.AddFileCommentW(const AText: WideString): Boolean;
begin
   LoadFunction(@pdfAddFileCommentW, 'pdfAddFileCommentW');
   Result := pdfAddFileCommentW(FInstance, PWideChar(AText));
end;

function TPDF.AddFontSearchPath(const APath: AnsiString; Recursive: Boolean): Integer;
begin
   LoadFunction(@pdfAddFontSearchPath, 'pdfAddFontSearchPathA');
   Result := pdfAddFontSearchPath(FInstance, PAnsiChar(APath), Integer(Recursive));
end;

function TPDF.AddImage(Filter: TCompressionFilter; Flags: TImageConversionFlags; var Image: TPDFImage): Boolean;
begin
   LoadFunction(@pdfAddImage, 'pdfAddImage');
   Result := pdfAddImage(FInstance, Filter, Flags, Image);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.AddJavaScript(const Name, Script: AnsiString): Integer;
begin
   LoadFunction(@pdfAddJavaScriptA, 'pdfAddJavaScriptA');
   Result := pdfAddJavaScriptA(FInstance, PAnsiChar(Name), PAnsiChar(Script));
end;

function TPDF.AddJavaScript(const Name: AnsiString; const Script: WideString): Integer;
begin
   LoadFunction(@pdfAddJavaScriptW, 'pdfAddJavaScriptW');
   Result := pdfAddJavaScriptW(FInstance, PAnsiChar(Name), PWideChar(Script));
end;
{$endif}

function TPDF.AddJavaScriptA(const Name, Script: AnsiString): Integer;
begin
   LoadFunction(@pdfAddJavaScriptA, 'pdfAddJavaScriptA');
   Result := pdfAddJavaScriptA(FInstance, PAnsiChar(Name), PAnsiChar(Script));
end;

function TPDF.AddJavaScriptW(const Name: AnsiString; const Script: WideString): Integer;
begin
   LoadFunction(@pdfAddJavaScriptW, 'pdfAddJavaScriptW');
   Result := pdfAddJavaScriptW(FInstance, PAnsiChar(Name), PWideChar(Script));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.AddLayerToDisplTree(Parent: Pointer; Layer: Integer; const Title: AnsiString): Pointer;
begin
   LoadFunction(@pdfAddLayerToDisplTreeA, 'pdfAddLayerToDisplTreeA');
   Result := pdfAddLayerToDisplTreeA(FInstance, Parent, Layer, PAnsiChar(Title));
end;

function TPDF.AddLayerToDisplTree(Parent: Pointer; Layer: Integer; const Title: WideString): Pointer;
begin
   LoadFunction(@pdfAddLayerToDisplTreeW, 'pdfAddLayerToDisplTreeW');
   Result := pdfAddLayerToDisplTreeW(FInstance, Parent, Layer, PWideChar(Title));
end;
{$endif}

function TPDF.AddLayerToDisplTreeA(Parent: Pointer; Layer: Integer; const Title: AnsiString): Pointer;
begin
   LoadFunction(@pdfAddLayerToDisplTreeA, 'pdfAddLayerToDisplTreeA');
   Result := pdfAddLayerToDisplTreeA(FInstance, Parent, Layer, PAnsiChar(Title));
end;

function TPDF.AddLayerToDisplTreeW(Parent: Pointer; Layer: Integer; const Title: WideString): Pointer;
begin
   LoadFunction(@pdfAddLayerToDisplTreeW, 'pdfAddLayerToDisplTreeW');
   Result := pdfAddLayerToDisplTreeW(FInstance, Parent, Layer, PWideChar(Title));
end;

function TPDF.AddMaskImage(BaseImage: Cardinal; const Buffer: Pointer; BufSize: Cardinal; Stride: Integer; BitsPerPixel, Width, Height: Cardinal): Boolean;
begin
   LoadFunction(@pdfAddMaskImage, 'pdfAddMaskImage');
   Result := pdfAddMaskImage(FInstance, BaseImage, Buffer, BufSize, Stride, BitsPerPixel, Width, Height);
end;

function TPDF.AddObjectToLayer(OCG: Cardinal; ObjType: TOCObject; Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfAddObjectToLayer, 'pdfAddObjectToLayer');
   Result := pdfAddObjectToLayer(FInstance, OCG, ObjType, Handle);
end;

function TPDF.AddOCGToAppEvent(Handle: Cardinal; Events: TOCAppEvent; Categories: TOCGUsageCategory): Boolean;
begin
   LoadFunction(@pdfAddOCGToAppEvent, 'pdfAddOCGToAppEvent');
   Result := pdfAddOCGToAppEvent(FInstance, Handle, Events, Categories);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.AddOutputIntent(const ICCFile: AnsiString): Integer;
begin
   LoadFunction(@pdfAddOutputIntentA, 'pdfAddOutputIntentA');
   Result := pdfAddOutputIntentA(FInstance, PAnsiChar(ICCFile));
end;

function TPDF.AddOutputIntent(const ICCFile: WideString): Integer;
begin
   LoadFunction(@pdfAddOutputIntentW, 'pdfAddOutputIntentW');
   Result := pdfAddOutputIntentW(FInstance, PWideChar(ICCFile));
end;
{$endif}

function TPDF.AddOutputIntentA(const ICCFile: AnsiString): Integer;
begin
   LoadFunction(@pdfAddOutputIntentA, 'pdfAddOutputIntentA');
   Result := pdfAddOutputIntentA(FInstance, PAnsiChar(ICCFile));
end;

function TPDF.AddOutputIntentW(const ICCFile: WideString): Integer;
begin
   LoadFunction(@pdfAddOutputIntentW, 'pdfAddOutputIntentW');
   Result := pdfAddOutputIntentW(FInstance, PWideChar(ICCFile));
end;

function TPDF.AddOutputIntentEx(const Buffer: Pointer; BufSize: Cardinal): Integer;
begin
   LoadFunction(@pdfAddOutputIntentEx, 'pdfAddOutputIntentEx');
   Result := pdfAddOutputIntentEx(FInstance, Buffer, BufSize);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.AddPageLabel(StartRange: Cardinal; Format: TPageLabelFormat; const Prefix: AnsiString; AddNum: Integer): Integer;
begin
   LoadFunction(@pdfAddPageLabelA, 'pdfAddPageLabelA');
   Result := pdfAddPageLabelA(FInstance, StartRange, Format, PAnsiChar(Prefix), AddNum);
end;

function TPDF.AddPageLabel(StartRange: Cardinal; Format: TPageLabelFormat; const Prefix: WideString; AddNum: Integer): Integer;
begin
   LoadFunction(@pdfAddPageLabelW, 'pdfAddPageLabelW');
   Result := pdfAddPageLabelW(FInstance, StartRange, Format, PWideChar(Prefix), AddNum);
end;
{$endif}

function TPDF.AddPageLabelA(StartRange: Cardinal; Format: TPageLabelFormat; const Prefix: AnsiString; AddNum: Integer): Integer;
begin
   LoadFunction(@pdfAddPageLabelA, 'pdfAddPageLabelA');
   Result := pdfAddPageLabelA(FInstance, StartRange, Format, PAnsiChar(Prefix), AddNum);
end;

function TPDF.AddPageLabelW(StartRange: Cardinal; Format: TPageLabelFormat; const Prefix: WideString; AddNum: Integer): Integer;
begin
   LoadFunction(@pdfAddPageLabelW, 'pdfAddPageLabelW');
   Result := pdfAddPageLabelW(FInstance, StartRange, Format, PWideChar(Prefix), AddNum);
end;

function TPDF.AddRasImage(RasPtr: Pointer; Filter: TCompressionFilter): Boolean;
begin
   LoadFunction(@pdfAddRasImage, 'pdfAddRasImage');
   Result := pdfAddRasImage(FInstance, RasPtr, Filter);
end;

{$ifdef DELPHI6_OR_HIGHER}
{
  These functions were incorrectly named. Please use AddOutputIntent() instead.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------ }

function TPDF.AddRenderingIntent(const ICCFile: AnsiString): Integer;
begin
   LoadFunction(@pdfAddOutputIntentA, 'pdfAddRenderingIntentA');
   Result := pdfAddOutputIntentA(FInstance, PAnsiChar(ICCFile));
end;

function TPDF.AddRenderingIntent(const ICCFile: WideString): Integer;
begin
   LoadFunction(@pdfAddOutputIntentW, 'pdfAddRenderingIntentW');
   Result := pdfAddOutputIntentW(FInstance, PWideChar(ICCFile));
end;
{$endif}

function TPDF.AddRenderingIntentA(const ICCFile: AnsiString): Integer;
begin
   LoadFunction(@pdfAddOutputIntentA, 'pdfAddRenderingIntentA');
   Result := pdfAddOutputIntentA(FInstance, PAnsiChar(ICCFile));
end;

function TPDF.AddRenderingIntentW(const ICCFile: WideString): Integer;
begin
   LoadFunction(@pdfAddOutputIntentW, 'pdfAddRenderingIntentW');
   Result := pdfAddOutputIntentW(FInstance, PWideChar(ICCFile));
end;

function TPDF.AddRenderingIntentEx(const Buffer: Pointer; BufSize: Cardinal): Integer;
begin
   LoadFunction(@pdfAddOutputIntentEx, 'pdfAddRenderingIntentEx');
   Result := pdfAddOutputIntentEx(FInstance, Buffer, BufSize);
end;

{ ------------------------------------------------------------------------------------------------------------------------------------------------------------ }

function TPDF.AddValToChoiceFieldA(Field: Cardinal; const ExpValue, Value: AnsiString; Selected: Boolean): Boolean;
begin
   LoadFunction(@pdfAddValToChoiceFieldA, 'pdfAddValToChoiceFieldA');
   Result := pdfAddValToChoiceFieldA(FInstance, Field, PAnsiChar(ExpValue), PAnsiChar(Value), Integer(Selected));
end;

function TPDF.AddValToChoiceFieldW(Field: Cardinal; const ExpValue: AnsiString; const Value: WideString; Selected: Boolean): Boolean;
begin
   LoadFunction(@pdfAddValToChoiceFieldW, 'pdfAddValToChoiceFieldW');
   Result := pdfAddValToChoiceFieldW(FInstance, Field, PAnsiChar(ExpValue), PWideChar(Value), Integer(Selected));
end;

function TPDF.Append(): Boolean;
begin
   LoadFunction(@pdfAppend, 'pdfAppend');
   Result := pdfAppend(FInstance);
end;

function TPDF.ApplyPattern(PattHandle: Integer; ColorMode: TColorMode; Color: Cardinal): Boolean;
begin
   LoadFunction(@pdfApplyPattern, 'pdfApplyPattern');
   Result := pdfApplyPattern(FInstance, PattHandle, ColorMode, Color);
end;

function TPDF.ApplyShading(ShadHandle: Integer): Boolean;
begin
   LoadFunction(@pdfApplyShading, 'pdfApplyShading');
   Result := pdfApplyShading(FInstance, ShadHandle);
end;

function TPDF.AssociateEmbFile(DestObject: TAFDestObject; DestHandle: Integer; Relationship: TAFRelationship; EmbFile: Cardinal): Boolean;
begin
   LoadFunction(@pdfAssociateEmbFile, 'pdfAssociateEmbFile');
   Result := pdfAssociateEmbFile(FInstance, DestObject, DestHandle, Relationship, EmbFile);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.AttachFile(const FilePath, Description: AnsiString; Compress: Boolean): Integer;
begin
   LoadFunction(@pdfAttachFileA, 'pdfAttachFileA');
   Result := pdfAttachFileA(FInstance, PAnsiChar(FilePath), PAnsiChar(Description), Compress);
end;

function TPDF.AttachFile(const FilePath, Description: WideString; Compress: Boolean): Integer;
begin
   LoadFunction(@pdfAttachFileW, 'pdfAttachFileW');
   Result := pdfAttachFileW(FInstance, PWideChar(FilePath), PWideChar(Description), Compress);
end;
{$endif}

function TPDF.AttachFileA(const FilePath, Description: AnsiString; Compress: Boolean): Integer;
begin
   LoadFunction(@pdfAttachFileA, 'pdfAttachFileA');
   Result := pdfAttachFileA(FInstance, PAnsiChar(FilePath), PAnsiChar(Description), Compress);
end;

function TPDF.AttachFileW(const FilePath, Description: WideString; Compress: Boolean): Integer;
begin
   LoadFunction(@pdfAttachFileW, 'pdfAttachFileW');
   Result := pdfAttachFileW(FInstance, PWideChar(FilePath), PWideChar(Description), Compress);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.AttachFileEx(const Buffer: Pointer; BufSize: Cardinal; const FileName, Description: AnsiString; Compress: Boolean): Integer;
begin
   LoadFunction(@pdfAttachFileExA, 'pdfAttachFileExA');
   Result := pdfAttachFileExA(FInstance, Pointer(Buffer), BufSize, PAnsiChar(FileName), PAnsiChar(Description), Compress);
end;

function TPDF.AttachFileEx(const Buffer: Pointer; BufSize: Cardinal; const FileName, Description: WideString; Compress: Boolean): Integer;
begin
   LoadFunction(@pdfAttachFileExW, 'pdfAttachFileExW');
   Result := pdfAttachFileExW(FInstance, Pointer(Buffer), BufSize, PWideChar(FileName), PWideChar(Description), Compress);
end;
{$endif}

function TPDF.AttachFileExA(const Buffer: Pointer; BufSize: Cardinal; const FileName, Description: AnsiString; Compress: Boolean): Integer;
begin
   LoadFunction(@pdfAttachFileExA, 'pdfAttachFileExA');
   Result := pdfAttachFileExA(FInstance, Pointer(Buffer), BufSize, PAnsiChar(FileName), PAnsiChar(Description), Compress);
end;

function TPDF.AttachFileExW(const Buffer: Pointer; BufSize: Cardinal; const FileName, Description: WideString; Compress: Boolean): Integer;
begin
   LoadFunction(@pdfAttachFileExW, 'pdfAttachFileExW');
   Result := pdfAttachFileExW(FInstance, Pointer(Buffer), BufSize, PWideChar(FileName), PWideChar(Description), Compress);
end;

function TPDF.AttachImageBuffer(RasPtr: Pointer; Rows: PPointer; Buffer: Pointer; Width, Height: Cardinal; ScanlineLen: Integer): Boolean;
begin
   LoadFunction(@rasAttachImageBuffer, 'rasAttachImageBuffer');
   Result := rasAttachImageBuffer(RasPtr, Rows, Buffer, Width, Height, ScanlineLen);
end;

function TPDF.AutoTemplate(Templ: Cardinal; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfAutoTemplate, 'pdfAutoTemplate');
   Result := pdfAutoTemplate(FInstance, Templ, PosX, PosY, Width, Height);
end;

function TPDF.BeginClipPath(): Boolean;
begin
   LoadFunction(@pdfBeginClipPath, 'pdfBeginClipPath');
   Result := pdfBeginClipPath(FInstance);
end;

function TPDF.BeginContinueText(PosX, PosY: Double): Integer;
begin
   LoadFunction(@pdfBeginContinueText, 'pdfBeginContinueText');
   Result := pdfBeginContinueText(FInstance, PosX, PosY);
end;

function TPDF.BeginLayer(OCG: Cardinal): Boolean;
begin
   LoadFunction(@pdfBeginLayer, 'pdfBeginLayer');
   Result := pdfBeginLayer(FInstance, OCG);
end;

function TPDF.BeginPageTemplate(const Name: AnsiString; UseAutoTemplates: Boolean): Boolean;
begin
   LoadFunction(@pdfBeginPageTemplate, 'BeginPageTemplate');
   Result := pdfBeginPageTemplate(FInstance, PAnsiChar(Name), UseAutoTemplates);
end;

function TPDF.BeginPattern(PatternType: TPatternType; TilingType: TTilingType; Width, Height: Double): Integer;
begin
   LoadFunction(@pdfBeginPattern, 'pdfBeginPattern');
   Result := pdfBeginPattern(FInstance, PatternType, TilingType, Width, Height);
end;

function TPDF.BeginTemplate(Width, Height: Double): Integer;
begin
   LoadFunction(@pdfBeginTemplate, 'pdfBeginTemplate');
   Result := pdfBeginTemplate(FInstance, Width, Height);
end;

function TPDF.BeginTemplateEx(var BBox: TPDFRect; var Matrix: TCTM): Integer;
begin
   LoadFunction(@pdfBeginTemplateEx, 'pdfBeginTemplateEx');
   Result := pdfBeginTemplateEx(FInstance, BBox, Matrix);
end;

function TPDF.BeginTransparencyGroup(x1, y1, x2, y2: Double; Isolated, Knockout: Boolean; CS: TExtColorSpace; CSHandle: Integer): Integer;
begin
   LoadFunction(@pdfBeginTransparencyGroup, 'pdfBeginTransparencyGroup');
   Result := pdfBeginTransparencyGroup(FInstance, x1, y1, x2, y2, Isolated, Knockout, CS, CSHandle);
end;

function TPDF.Bezier_1_2_3(x1, y1, x2, y2, x3, y3: Double): Boolean;
begin
   LoadFunction(@pdfBezier_1_2_3, 'pdfBezier_1_2_3');
   Result := pdfBezier_1_2_3(FInstance, x1, y1, x2, y2, x3, y3);
end;

function TPDF.Bezier_1_3(x1, y1, x3, y3: Double): Boolean;
begin
   LoadFunction(@pdfBezier_1_3, 'pdfBezier_1_3');
   Result := pdfBezier_1_3(FInstance, x1, y1, x3, y3);
end;

function TPDF.Bezier_2_3(x2, y2, x3, y3: Double): Boolean;
begin
   LoadFunction(@pdfBezier_2_3, 'pdfBezier_2_3');
   Result := pdfBezier_2_3(FInstance, x2, y2, x3, y3);
end;

function TPDF.BuildFamilyNameAndStyle(const IFont: Pointer; Name: PAnsiChar; var Style: TFStyle): Boolean;
begin
   LoadFunction(@fntBuildFamilyNameAndStyle, 'fntBuildFamilyNameAndStyle');
   Result := fntBuildFamilyNameAndStyle(IFont, Name, Style);
end;

procedure TPDF.CalcPagePixelSize(PagePtr: Pointer; DefScale: TPDFPageScale; Scale: Single; FrameWidth, FrameHeight: Cardinal; Flags: TRasterFlags; var Width, Height: Cardinal);
begin
   LoadFunction(@rasCalcPagePixelSize, 'rasCalcPagePixelSize');
   rasCalcPagePixelSize(PagePtr, DefScale, Scale, FrameWidth, FrameHeight, Flags, Width, Height);
end;

function TPDF.CalcWidthHeight(OrgWidth, OrgHeight, ScaledWidth, ScaledHeight: Double): Double;
begin
   LoadFunction(@pdfCalcWidthHeight, 'pdfCalcWidthHeight');
   Result := pdfCalcWidthHeight(FInstance, OrgWidth, OrgHeight, ScaledWidth, ScaledHeight);
end;

function TPDF.ChangeAnnotNameA(Handle: Cardinal; const Name: AnsiString): Boolean;
begin
   LoadFunction(@pdfChangeAnnotNameA, 'pdfChangeAnnotNameA');
   Result := pdfChangeAnnotNameA(FInstance, Handle, PAnsiChar(Name));
end;

function TPDF.ChangeAnnotNameW(Handle: Cardinal; const Name: WideString): Boolean;
begin
   LoadFunction(@pdfChangeAnnotNameW, 'pdfChangeAnnotNameW');
   Result := pdfChangeAnnotNameW(FInstance, Handle, PWideChar(Name));
end;

function TPDF.ChangeAnnotPos(Handle: Cardinal; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfChangeAnnotPos, 'pdfChangeAnnotPos');
   Result := pdfChangeAnnotPos(FInstance, Handle, PosX, PosY, Width, Height);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.ChangeBookmark(ABmk: Integer; const Title: AnsiString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Boolean;
begin
   LoadFunction(@pdfChangeBookmarkA, 'pdfChangeBookmarkA');
   Result := pdfChangeBookmarkA(FInstance, ABmk, PAnsiChar(Title), Parent, DestPage, Open);
end;

function TPDF.ChangeBookmark(ABmk: Integer; const Title: WideString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Boolean;
begin
   LoadFunction(@pdfChangeBookmarkW, 'pdfChangeBookmarkW');
   Result := pdfChangeBookmarkW(FInstance, ABmk, PWideChar(Title), Parent, DestPage, Open);
end;
{$endif}

function TPDF.ChangeBookmarkA(ABmk: Integer; const Title: AnsiString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Boolean;
begin
   LoadFunction(@pdfChangeBookmarkA, 'pdfChangeBookmarkA');
   Result := pdfChangeBookmarkA(FInstance, ABmk, PAnsiChar(Title), Parent, DestPage, Open);
end;

function TPDF.ChangeBookmarkW(ABmk: Integer; const Title: WideString; Parent: Integer; DestPage: Cardinal; Open: Boolean): Boolean;
begin
   LoadFunction(@pdfChangeBookmarkW, 'pdfChangeBookmarkW');
   Result := pdfChangeBookmarkW(FInstance, ABmk, PWideChar(Title), Parent, DestPage, Open);
end;

function TPDF.ChangeFont(Handle: Integer): Boolean;
begin
   LoadFunction(@pdfChangeFont, 'pdfChangeFont');
   Result := pdfChangeFont(FInstance, Handle);
end;

function TPDF.ChangeFontSize(Size: Double): Boolean;
begin
   LoadFunction(@pdfChangeFontSize, 'pdfChangeFontSize');
   Result := pdfChangeFontSize(FInstance, Size);
end;

function TPDF.ChangeFontStyle(Style: TFStyle): Boolean;
begin
   LoadFunction(@pdfChangeFontStyle, 'pdfChangeFontStyle');
   Result := pdfChangeFontStyle(FInstance, Style);
end;

function TPDF.ChangeFontStyleEx(Style: TFStyle): Boolean;
begin
   LoadFunction(@pdfChangeFontStyleEx, 'pdfChangeFontStyleEx');
   Result := pdfChangeFontStyleEx(FInstance, Style);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.ChangeJavaScript(Handle: Cardinal; const NewScript: AnsiString): Boolean;
begin
   LoadFunction(@pdfChangeJavaScriptA, 'pdfChangeJavaScriptA');
   Result := pdfChangeJavaScriptA(FInstance, Handle, PAnsiChar(NewScript));
end;

function TPDF.ChangeJavaScript(Handle: Cardinal; const NewScript: WideString): Boolean;
begin
   LoadFunction(@pdfChangeJavaScriptW, 'pdfChangeJavaScriptW');
   Result := pdfChangeJavaScriptW(FInstance, Handle, PWideChar(NewScript));
end;
{$endif}

function TPDF.ChangeJavaScriptA(Handle: Cardinal; const NewScript: AnsiString): Boolean;
begin
   LoadFunction(@pdfChangeJavaScriptA, 'pdfChangeJavaScriptA');
   Result := pdfChangeJavaScriptA(FInstance, Handle, PAnsiChar(NewScript));
end;

function TPDF.ChangeJavaScriptW(Handle: Cardinal; const NewScript: WideString): Boolean;
begin
   LoadFunction(@pdfChangeJavaScriptW, 'pdfChangeJavaScriptW');
   Result := pdfChangeJavaScriptW(FInstance, Handle, PWideChar(NewScript));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.ChangeJavaScriptAction(Handle: Cardinal; const NewScript: AnsiString): Boolean;
begin
   LoadFunction(@pdfChangeJavaScriptActionA, 'pdfChangeJavaScriptActionA');
   Result := pdfChangeJavaScriptActionA(FInstance, Handle, PAnsiChar(NewScript));
end;

function TPDF.ChangeJavaScriptAction(Handle: Cardinal; const NewScript: WideString): Boolean;
begin
   LoadFunction(@pdfChangeJavaScriptActionW, 'pdfChangeJavaScriptActionW');
   Result := pdfChangeJavaScriptActionW(FInstance, Handle, PWideChar(NewScript));
end;
{$endif}

function TPDF.ChangeJavaScriptActionA(Handle: Cardinal; const NewScript: AnsiString): Boolean;
begin
   LoadFunction(@pdfChangeJavaScriptActionA, 'pdfChangeJavaScriptActionA');
   Result := pdfChangeJavaScriptActionA(FInstance, Handle, PAnsiChar(NewScript));
end;

function TPDF.ChangeJavaScriptActionW(Handle: Cardinal; const NewScript: WideString): Boolean;
begin
   LoadFunction(@pdfChangeJavaScriptActionW, 'pdfChangeJavaScriptActionW');
   Result := pdfChangeJavaScriptActionW(FInstance, Handle, PWideChar(NewScript));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.ChangeJavaScriptName(Handle: Cardinal; const Name: AnsiString): Boolean;
begin
   LoadFunction(@pdfChangeJavaScriptNameA, 'pdfChangeJavaScriptNameA');
   Result := pdfChangeJavaScriptNameA(FInstance, Handle, PAnsiChar(Name));
end;

function TPDF.ChangeJavaScriptName(Handle: Cardinal; const Name: WideString): Boolean;
begin
   LoadFunction(@pdfChangeJavaScriptNameW, 'pdfChangeJavaScriptNameW');
   Result := pdfChangeJavaScriptNameW(FInstance, Handle, PWideChar(Name));
end;
{$endif}

function TPDF.ChangeJavaScriptNameA(Handle: Cardinal; const Name: AnsiString): Boolean;
begin
   LoadFunction(@pdfChangeJavaScriptNameA, 'pdfChangeJavaScriptNameA');
   Result := pdfChangeJavaScriptNameA(FInstance, Handle, PAnsiChar(Name));
end;

function TPDF.ChangeJavaScriptNameW(Handle: Cardinal; const Name: WideString): Boolean;
begin
   LoadFunction(@pdfChangeJavaScriptNameW, 'pdfChangeJavaScriptNameW');
   Result := pdfChangeJavaScriptNameW(FInstance, Handle, PWideChar(Name));
end;

function TPDF.ChangeLinkAnnot(Handle: Cardinal; const URL: AnsiString): Boolean;
begin
   LoadFunction(@pdfChangeLinkAnnot, 'pdfChangeLinkAnnot');
   Result := pdfChangeLinkAnnot(FInstance, Handle, PAnsiChar(URL));
end;

function TPDF.ChangeSeparationColor(CSHandle, NewColor: Cardinal; Alternate: TExtColorSpace; AltHandle: Integer): Boolean;
begin
   LoadFunction(@pdfChangeSeparationColor, 'pdfChangeSeparationColor');
   Result := pdfChangeSeparationColor(FInstance, CSHandle, NewColor, Alternate, AltHandle);
end;

function TPDF.CheckCollection: Boolean;
begin
   LoadFunction(@pdfCheckCollection, 'pdfCheckCollection');
   Result := pdfCheckCollection(FInstance);
end;

function TPDF.CheckConformance(ConfType: TConformanceType; Options: TCheckOptions; UserData: Pointer; OnFontNotFound: TOnFontNotFoundProc; OnReplaceICCProfile: TOnReplaceICCProfile): Integer;
begin
   LoadFunction(@pdfCheckConformance, 'pdfCheckConformance');
   Result := pdfCheckConformance(FInstance, Integer(ConfType), Options, UserData, OnFontNotFound, OnReplaceICCProfile);
end;

function TPDF.CheckFieldNames(): Integer;
begin
   LoadFunction(@pdfCheckFieldNames, 'pdfCheckFieldNames');
   Result := pdfCheckFieldNames(FInstance);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CircleAnnot(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: AnsiString): Integer;
begin
   LoadFunction(@pdfCircleAnnotA, 'pdfCircleAnnotA');
   Result := pdfCircleAnnotA(FInstance, PosX, PosY, Width, Height, LineWidth, FillColor, StrokeColor, CS, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Comment));
end;

function TPDF.CircleAnnot(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: WideString): Integer;
begin
   LoadFunction(@pdfCircleAnnotW, 'pdfCircleAnnotW');
   Result := pdfCircleAnnotW(FInstance, PosX, PosY, Width, Height, LineWidth, FillColor, StrokeColor, CS, PWideChar(Author), PWideChar(Subject), PWideChar(Comment));
end;
{$endif}

function TPDF.CircleAnnotA(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: AnsiString): Integer;
begin
   LoadFunction(@pdfCircleAnnotA, 'pdfCircleAnnotA');
   Result := pdfCircleAnnotA(FInstance, PosX, PosY, Width, Height, LineWidth, FillColor, StrokeColor, CS, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Comment));
end;

function TPDF.CircleAnnotW(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: WideString): Integer;
begin
   LoadFunction(@pdfCircleAnnotW, 'pdfCircleAnnotW');
   Result := pdfCircleAnnotW(FInstance, PosX, PosY, Width, Height, LineWidth, FillColor, StrokeColor, CS, PWideChar(Author), PWideChar(Subject), PWideChar(Comment));
end;

function TPDF.ClearAutoTemplates: Boolean;
begin
   LoadFunction(@pdfClearAutoTemplates, 'pdfClearAutoTemplates');
   Result := pdfClearAutoTemplates(FInstance);
end;

procedure TPDF.ClearErrorLog;
begin
   LoadFunction(@pdfClearErrorLog, 'pdfClearErrorLog');
   pdfClearErrorLog(FInstance);
end;

function TPDF.ClearHostFonts(): Boolean;
begin
   LoadFunction(@pdfClearHostFonts, 'pdfClearHostFonts');
   Result := pdfClearHostFonts(FInstance);
end;

function TPDF.ClipPath(ClipMode: TClippingMode; FillMode: TPathFillMode): Boolean;
begin
   LoadFunction(@pdfClipPath, 'pdfClipPath');
   Result := pdfClipPath(FInstance, ClipMode, FillMode);
end;

function TPDF.CloseAndSignFile(const CertFile, Password, Reason, Location: AnsiString): Boolean;
begin
   LoadFunction(@pdfCloseAndSignFile, 'pdfCloseAndSignFile');
   Result := pdfCloseAndSignFile(FInstance, PAnsiChar(CertFile), PAnsiChar(Password), PAnsiChar(Reason), PAnsiChar(Location));
end;

function TPDF.CloseAndSignFileEx(const OpenPwd, OwnerPwd: AnsiString; KeyLen: TKeyLen; Restrict: TRestrictions; const CertFile, Password, Reason, Location: AnsiString): Boolean;
begin
   LoadFunction(@pdfCloseAndSignFileEx, 'pdfCloseAndSignFileEx');
   Result := pdfCloseAndSignFileEx(FInstance, PAnsiChar(OpenPwd), PAnsiChar(OwnerPwd), Ord(KeyLen), Ord(Restrict), PAnsiChar(CertFile), PAnsiChar(Password), PAnsiChar(Reason), PAnsiChar(Location));
end;

function TPDF.CloseAndSignFileExt(var SigParms: TPDFSigParms): Boolean;
begin
   LoadFunction(@pdfCloseAndSignFileExt, 'pdfCloseAndSignFileExt');
   Result := pdfCloseAndSignFileExt(FInstance, SigParms);
end;

function TPDF.CloseFile(): Boolean;
begin
   LoadFunction(@pdfCloseFile, 'pdfCloseFile');
   Result := pdfCloseFile(FInstance);
end;

function TPDF.CloseFileEx(const OpenPwd, OwnerPwd: AnsiString; KeyLen: TKeyLen; Restrict: TRestrictions): Boolean;
begin
   LoadFunction(@pdfCloseFileEx, 'pdfCloseFileEx');
   Result := pdfCloseFileEx(FInstance, PAnsiChar(OpenPwd), PAnsiChar(OwnerPwd), Ord(KeyLen), Ord(Restrict));
end;

function TPDF.CloseImage: Boolean;
begin
   LoadFunction(@pdfCloseImage, 'pdfCloseImage');
   Result := pdfCloseImage(FInstance);
end;

function TPDF.CloseImportFile(): Boolean;
begin
   LoadFunction(@pdfCloseImportFile, 'pdfCloseImportFile');
   Result := pdfCloseImportFile(FInstance);
end;

function TPDF.CloseImportFileEx(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfCloseImportFileEx, 'pdfCloseImportFileEx');
   Result := pdfCloseImportFileEx(FInstance, Handle);
end;

function TPDF.ClosePath(FillMode: TPathFillMode): Boolean;
begin
   LoadFunction(@pdfClosePath, 'pdfClosePath');
   Result := pdfClosePath(FInstance, FillMode);
end;

function TPDF.CloseTag: Boolean;
begin
   LoadFunction(@pdfCloseTag, 'pdfCloseTag');
   Result := pdfCloseTag(FInstance);
end;

function TPDF.ComputeBBox(var BBox: TPDFRect; Flags: TCompBBoxFlags): Integer;
begin
   LoadFunction(@pdfComputeBBox, 'pdfComputeBBox');
   Result := pdfComputeBBox(FInstance, BBox, Flags);
end;

function TPDF.ConvColor(const Color: PDouble; NumComps: Cardinal; SourceCS: TExtColorSpace; const IColorSpace: Pointer; DestCS: TExtColorSpace): Cardinal;
begin
   LoadFunction(@pdfConvColor, 'pdfConvColor');
   Result := pdfConvColor(Color, NumComps, Integer(SourceCS), IColorSpace, Integer(DestCS));
end;

function TPDF.ConvertColors(Flags: TColorConvFlags; Add: Single): Integer;
begin
   LoadFunction(@pdfConvertColors, 'pdfConvertColors');
   Result := pdfConvertColors(FInstance, Flags, @Add);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.ConvertEMFSpool(const SpoolFile: AnsiString; LeftMargin, TopMargin: Double; Flags: TSpoolConvFlags): Integer;
begin
   LoadFunction(@pdfConvertEMFSpoolA, 'pdfConvertEMFSpoolA');
   Result := pdfConvertEMFSpoolA(FInstance, PAnsiChar(SpoolFile), LeftMargin, TopMargin, Integer(Flags));
end;

function TPDF.ConvertEMFSpool(const SpoolFile: WideString; LeftMargin, TopMargin: Double; Flags: TSpoolConvFlags): Integer;
begin
   LoadFunction(@pdfConvertEMFSpoolW, 'pdfConvertEMFSpoolW');
   Result := pdfConvertEMFSpoolW(FInstance, PWideChar(SpoolFile), LeftMargin, TopMargin, Integer(Flags));
end;
{$endif}

function TPDF.ConvertEMFSpoolA(const SpoolFile: AnsiString; LeftMargin, TopMargin: Double; Flags: TSpoolConvFlags): Integer;
begin
   LoadFunction(@pdfConvertEMFSpoolA, 'pdfConvertEMFSpoolA');
   Result := pdfConvertEMFSpoolA(FInstance, PAnsiChar(SpoolFile), LeftMargin, TopMargin, Integer(Flags));
end;

function TPDF.ConvertEMFSpoolW(const SpoolFile: WideString; LeftMargin, TopMargin: Double; Flags: TSpoolConvFlags): Integer;
begin
   LoadFunction(@pdfConvertEMFSpoolW, 'pdfConvertEMFSpoolW');
   Result := pdfConvertEMFSpoolW(FInstance, PWideChar(SpoolFile), LeftMargin, TopMargin, Integer(Flags));
end;

function TPDF.ConvToUnicode(const AString: AnsiString; CP: TCodepage): WideString;
begin
   LoadFunction(@pdfConvToUnicode, 'pdfConvToUnicode');
   Result := pdfConvToUnicode(FInstance, PAnsiChar(AString), Integer(CP));
end;

function TPDF.Create3DAnnot(PosX, PosY, Width, Height: Double; const Author, AnnotName, U3DFile, ImageFile: AnsiString): Integer;
begin
   LoadFunction(@pdfCreate3DAnnot, 'pdfCreate3DAnnot');
   Result := pdfCreate3DAnnot(FInstance, PosX, PosY, Width, Height, PAnsiChar(Author), PAnsiChar(AnnotName), PAnsiChar(U3DFile), PAnsiChar(ImageFile));
end;

function TPDF.Create3DBackground(const IView: Pointer; BackColor: Cardinal): Boolean;
begin
   LoadFunction(@pdfCreate3DBackground, 'pdfCreate3DBackground');
   Result := pdfCreate3DBackground(FInstance, IView, BackColor);
end;

function TPDF.Create3DGotoViewAction(Base3DAnnot: Cardinal; const IView: Pointer; Named: T3DNamedAction): Integer;
begin
   LoadFunction(@pdfCreate3DGotoViewAction, 'pdfCreate3DGotoViewAction');
   Result := pdfCreate3DGotoViewAction(FInstance, Base3DAnnot, IView, Integer(Named));
end;

function TPDF.Create3DProjection(const IView: Pointer; ProjType: T3DProjType; ScaleType: T3DScaleType; Diameter, FOV: Double): Boolean;
begin
   LoadFunction(@pdfCreate3DProjection, 'pdfCreate3DProjection');
   Result := pdfCreate3DProjection(FInstance, IView, Integer(ProjType), Integer(ScaleType), Diameter, FOV);
end;

function TPDF.Create3DView(Base3DAnnot: Cardinal; const Name: AnsiString; SetAsDefault: Boolean; Matrix: PDouble; CamDistance: Double; RM: T3DRenderingMode; LS: T3DLightingSheme): Pointer;
begin
   LoadFunction(@pdfCreate3DView, 'pdfCreate3DView');
   Result := pdfCreate3DView(FInstance, Base3DAnnot, PAnsiChar(Name), SetAsDefault, Matrix, CamDistance, Integer(RM), Integer(LS));
end;

function TPDF.CreateAnnotAP(Annot: Cardinal): Integer;
begin
   LoadFunction(@pdfCreateAnnotAP, 'pdfCreateAnnotAP');
   Result := pdfCreateAnnotAP(FInstance, Annot);
end;

function TPDF.CreateArticleThreadA(const ThreadName: AnsiString): Integer;
begin
   LoadFunction(@pdfCreateArticleThreadA, 'pdfCreateArticleThreadA');
   Result := pdfCreateArticleThreadA(FInstance, PAnsiChar(ThreadName));
end;

function TPDF.CreateArticleThreadW(const ThreadName: WideString): Integer;
begin
   LoadFunction(@pdfCreateArticleThreadW, 'pdfCreateArticleThreadW');
   Result := pdfCreateArticleThreadW(FInstance, PWideChar(ThreadName));
end;

function TPDF.CreateAxialShading(sX, sY, eX, eY, SCenter: Double; SColor, EColor: Cardinal; Extend1, Extend2: Boolean): Integer;
begin
   LoadFunction(@pdfCreateAxialShading, 'pdfCreateAxialShading');
   Result := pdfCreateAxialShading(FInstance, sX, sY, eX, eY, SCenter, SColor, EColor, Integer(Extend1), Integer(Extend2));
end;

function TPDF.CreateBarcodeField(const Name: AnsiString; Parent: Integer; PosX, PosY, Width, Height: Double; var Barcode: TPDFBarcode): Integer;
begin
   LoadFunction(@pdfCreateBarcodeField, 'pdfCreateBarcodeField');
   Barcode.StructSize := sizeof(TPDFBarcode);
   Result := pdfCreateBarcodeField(FInstance, PAnsiChar(Name), Parent, PosX, PosY, Width, Height, Barcode);
end;

function TPDF.CreateButtonA(const Name, Caption: AnsiString; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
begin
   LoadFunction(@pdfCreateButtonA, 'pdfCreateButtonA');
   Result := pdfCreateButtonA(FInstance, PAnsiChar(Name), PAnsiChar(Caption), Parent, PosX, PosY, Width, Height);
end;

function TPDF.CreateButtonW(const Name: AnsiString; const Caption: WideString; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
begin
   LoadFunction(@pdfCreateButtonW, 'pdfCreateButtonW');
   Result := pdfCreateButtonW(FInstance, PAnsiChar(Name), PWideChar(Caption), Parent, PosX, PosY, Width, Height);
end;

function TPDF.CreateCheckBox(const Name, ExpValue: AnsiString; Checked: Boolean; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
begin
   LoadFunction(@pdfCreateCheckBox, 'pdfCreateCheckBox');
   Result := pdfCreateCheckBox(FInstance, PAnsiChar(Name), PAnsiChar(ExpValue), Integer(Checked), Parent, PosX, PosY, Width, Height);
end;

function TPDF.CreateCIEColorSpace(Base: TExtColorSpace; WhitePoint, BlackPoint, Gamma, Matrix: PSingle): Integer;
begin
   LoadFunction(@pdfCreateCIEColorSpace, 'pdfCreateCIEColorSpace');
   Result := pdfCreateCIEColorSpace(FInstance, Base, WhitePoint, BlackPoint, Gamma, Matrix);
end;

function TPDF.CreateCollection(View: TColView): Boolean;
begin
   LoadFunction(@pdfCreateCollection, 'pdfCreateCollection');
   Result := pdfCreateCollection(FInstance, Integer(View));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateCollectionField(ColType: TColColumnType; Column: Integer; const Name, Key: AnsiString; Visible, Editable: Boolean): Integer;
begin
   LoadFunction(@pdfCreateCollectionFieldA, 'pdfCreateCollectionFieldA');
   Result := pdfCreateCollectionFieldA(FInstance, Integer(ColType), Column, PAnsiChar(Name), PAnsiChar(Key), Visible, Editable);
end;

function TPDF.CreateCollectionField(ColType: TColColumnType; Column: Integer; const Name: WideString; const Key: AnsiString; Visible, Editable: Boolean): Integer;
begin
   LoadFunction(@pdfCreateCollectionFieldW, 'pdfCreateCollectionFieldW');
   Result := pdfCreateCollectionFieldW(FInstance, Integer(ColType), Column, PWideChar(Name), PAnsiChar(Key), Visible, Editable);
end;
{$endif}

function TPDF.CreateCollectionFieldA(ColType: TColColumnType; Column: Integer; const Name, Key: AnsiString; Visible, Editable: Boolean): Integer;
begin
   LoadFunction(@pdfCreateCollectionFieldA, 'pdfCreateCollectionFieldA');
   Result := pdfCreateCollectionFieldA(FInstance, Integer(ColType), Column, PAnsiChar(Name), PAnsiChar(Key), Visible, Editable);
end;

function TPDF.CreateCollectionFieldW(ColType: TColColumnType; Column: Integer; const Name: WideString; const Key: AnsiString; Visible, Editable: Boolean): Integer;
begin
   LoadFunction(@pdfCreateCollectionFieldW, 'pdfCreateCollectionFieldW');
   Result := pdfCreateCollectionFieldW(FInstance, Integer(ColType), Column, PWideChar(Name), PAnsiChar(Key), Visible, Editable);
end;

function TPDF.CreateColItemDate(EmbFile: Cardinal; const Key: AnsiString; Date: Integer; const Prefix: AnsiString): Boolean;
begin
   LoadFunction(@pdfCreateColItemDate, 'pdfCreateColItemDate');
   Result := pdfCreateColItemDate(FInstance, EmbFile, PAnsiChar(Key), Date, PAnsiChar(Prefix));
end;

function TPDF.CreateColItemNumber(EmbFile: Cardinal; const Key: AnsiString; Value: Double; const Prefix: AnsiString): Boolean;
begin
   LoadFunction(@pdfCreateColItemNumber, 'pdfCreateColItemNumber');
   Result := pdfCreateColItemNumber(FInstance, EmbFile, PAnsiChar(Key), Value, PAnsiChar(Prefix));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateColItemString(EmbFile: Cardinal; const Key, Value, Prefix: AnsiString): Boolean;
begin
   LoadFunction(@pdfCreateColItemStringA, 'pdfCreateColItemStringA');
   Result := pdfCreateColItemStringA(FInstance, EmbFile, PAnsiChar(Key), PAnsiChar(Value), PAnsiChar(Prefix));
end;

function TPDF.CreateColItemString(EmbFile: Cardinal; const Key: AnsiString; const Value, Prefix: WideString): Boolean;
begin
   LoadFunction(@pdfCreateColItemStringW, 'pdfCreateColItemStringW');
   Result := pdfCreateColItemStringW(FInstance, EmbFile, PAnsiChar(Key), PWideChar(Value), PWideChar(Prefix));
end;
{$endif}

function TPDF.CreateColItemStringA(EmbFile: Cardinal; const Key, Value, Prefix: AnsiString): Boolean;
begin
   LoadFunction(@pdfCreateColItemStringA, 'pdfCreateColItemStringA');
   Result := pdfCreateColItemStringA(FInstance, EmbFile, PAnsiChar(Key), PAnsiChar(Value), PAnsiChar(Prefix));
end;

function TPDF.CreateColItemStringW(EmbFile: Cardinal; const Key: AnsiString; const Value, Prefix: WideString): Boolean;
begin
   LoadFunction(@pdfCreateColItemStringW, 'pdfCreateColItemStringW');
   Result := pdfCreateColItemStringW(FInstance, EmbFile, PAnsiChar(Key), PWideChar(Value), PWideChar(Prefix));
end;

function TPDF.CreateComboBox(const Name: AnsiString; Sort: Boolean; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
begin
   LoadFunction(@pdfCreateComboBox, 'pdfCreateComboBox');
   Result := pdfCreateComboBox(FInstance, PAnsiChar(Name), Integer(Sort), Parent, PosX, PosY, Width, Height);
end;

function TPDF.CreateDeviceNColorSpace(const Colorants: Array of PAnsiChar; NumColorants: Cardinal; const PostScriptFunc: AnsiString; Alternate: TExtColorSpace; Handle: Integer): Integer;
begin
   LoadFunction(@pdfCreateDeviceNColorSpace, 'pdfCreateDeviceNColorSpace');
   Result := pdfCreateDeviceNColorSpace(FInstance, @Colorants, NumColorants, PAnsiChar(PostScriptFunc), Alternate, Handle);
end;

function TPDF.CreateExtGState(var GS: TPDFExtGState): Integer;
begin
   LoadFunction(@pdfCreateExtGState, 'pdfCreateExtGState');
   Result := pdfCreateExtGState(FInstance, GS);
end;

function TPDF.CreateGoToAction(DestType: TDestType; PageNum: Cardinal; a, b, c, d: Double): Integer;
begin
   LoadFunction(@pdfCreateGoToAction, 'pdfCreateGoToAction');
   Result := pdfCreateGoToAction(FInstance, DestType, PageNum, a, b, c, d);
end;

function TPDF.CreateGoToActionEx(NamedDest: Cardinal): Integer;
begin
   LoadFunction(@pdfCreateGoToActionEx, 'pdfCreateGoToActionEx');
   Result := pdfCreateGoToActionEx(FInstance, NamedDest);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateGoToEAction(Location: TEmbFileLocation; const Source: AnsiString; SrcPage: Cardinal; const Target, DestName: AnsiString; DestPage: Cardinal; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateGoToEActionA, 'pdfCreateGoToEActionA');
   Result := pdfCreateGoToEActionA(FInstance, Location, PAnsiChar(Source), SrcPage, PAnsiChar(Target), PAnsiChar(DestName), DestPage, NewWindow);
end;

function TPDF.CreateGoToEAction(Location: TEmbFileLocation; const Source: WideString; SrcPage: Cardinal; const Target, DestName: WideString; DestPage: Cardinal; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateGoToEActionW, 'pdfCreateGoToEActionW');
   Result := pdfCreateGoToEActionW(FInstance, Location, PWideChar(Source), SrcPage, PWideChar(Target), PWideChar(DestName), DestPage, NewWindow);
end;
{$endif}

function TPDF.CreateGoToEActionA(Location: TEmbFileLocation; const Source: AnsiString; SrcPage: Cardinal; const Target, DestName: AnsiString; DestPage: Cardinal; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateGoToEActionA, 'pdfCreateGoToEActionA');
   Result := pdfCreateGoToEActionA(FInstance, Location, PAnsiChar(Source), SrcPage, PAnsiChar(Target), PAnsiChar(DestName), DestPage, NewWindow);
end;

function TPDF.CreateGoToEActionW(Location: TEmbFileLocation; const Source: WideString; SrcPage: Cardinal; const Target, DestName: WideString; DestPage: Cardinal; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateGoToEActionW, 'pdfCreateGoToEActionW');
   Result := pdfCreateGoToEActionW(FInstance, Location, PWideChar(Source), SrcPage, PWideChar(Target), PWideChar(DestName), DestPage, NewWindow);
end;

function TPDF.CreateGoToRAction(const FileName: AnsiString; PageNum: Cardinal): Integer;
begin
   LoadFunction(@pdfCreateGoToRAction, 'pdfCreateGoToRAction');
   Result := pdfCreateGoToRAction(FInstance, PAnsiChar(FileName), PageNum);
end;

function TPDF.CreateGoToRActionW(const FileName: WideString; PageNum: Cardinal): Integer;
begin
   LoadFunction(@pdfCreateGoToRActionW, 'pdfCreateGoToRActionW');
   Result := pdfCreateGoToRActionW(FInstance, PWideChar(FileName), PageNum);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateGoToRAction(const FileName: WideString; PageNum: Cardinal): Integer;
begin
   LoadFunction(@pdfCreateGoToRActionW, 'pdfCreateGoToRActionW');
   Result := pdfCreateGoToRActionW(FInstance, PWideChar(FileName), PageNum);
end;

function TPDF.CreateGoToRActionEx(const FileName: AnsiString; DestName: AnsiString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateGoToRActionExA, 'pdfCreateGoToRActionExA');
   Result := pdfCreateGoToRActionExA(FInstance, PAnsiChar(FileName), PAnsiChar(DestName), NewWindow);
end;

function TPDF.CreateGoToRActionEx(const FileName: AnsiString; DestName: WideString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateGoToRActionExW, 'pdfCreateGoToRActionExW');
   Result := pdfCreateGoToRActionExW(FInstance, PAnsiChar(FileName), PWideChar(DestName), NewWindow);
end;
{$endif}

function TPDF.CreateGoToRActionExA(const FileName: AnsiString; DestName: AnsiString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateGoToRActionExA, 'pdfCreateGoToRActionExA');
   Result := pdfCreateGoToRActionExA(FInstance, PAnsiChar(FileName), PAnsiChar(DestName), NewWindow);
end;

function TPDF.CreateGoToRActionExW(const FileName: AnsiString; DestName: WideString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateGoToRActionExW, 'pdfCreateGoToRActionExW');
   Result := pdfCreateGoToRActionExW(FInstance, PAnsiChar(FileName), PWideChar(DestName), NewWindow);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateGoToRActionExU(const FileName: WideString; DestName: AnsiString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateGoToRActionExUA, 'pdfCreateGoToRActionExUA');
   Result := pdfCreateGoToRActionExUA(FInstance, PWideChar(FileName), PAnsiChar(DestName), NewWindow);
end;

function TPDF.CreateGoToRActionExU(const FileName: WideString; DestName: WideString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateGoToRActionExUW, 'pdfCreateGoToRActionExUW');
   Result := pdfCreateGoToRActionExUW(FInstance, PWideChar(FileName), PWideChar(DestName), NewWindow);
end;
{$endif}

function TPDF.CreateGoToRActionExUA(const FileName: WideString; DestName: AnsiString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateGoToRActionExUA, 'pdfCreateGoToRActionExUA');
   Result := pdfCreateGoToRActionExUA(FInstance, PWideChar(FileName), PAnsiChar(DestName), NewWindow);
end;

function TPDF.CreateGoToRActionExUW(const FileName: WideString; DestName: WideString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateGoToRActionExUW, 'pdfCreateGoToRActionExUW');
   Result := pdfCreateGoToRActionExUW(FInstance, PWideChar(FileName), PWideChar(DestName), NewWindow);
end;

function TPDF.CreateGroupField(const Name: AnsiString; Parent: Integer): Integer;
begin
   LoadFunction(@pdfCreateGroupField, 'pdfCreateGroupField');
   Result := pdfCreateGroupField(FInstance, PAnsiChar(Name), Parent);
end;

function TPDF.CreateHideAction(AField: Cardinal; Hide: Boolean): Integer;
begin
   LoadFunction(@pdfCreateHideAction, 'pdfCreateHideAction');
   Result := pdfCreateHideAction(FInstance, AField, Hide);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateICCBasedColorSpace(const ICCProfile: AnsiString): Integer;
begin
   LoadFunction(@pdfCreateICCBasedColorSpaceA, 'pdfCreateICCBasedColorSpaceA');
   Result := pdfCreateICCBasedColorSpaceA(FInstance, PAnsiChar(ICCProfile));
end;

function TPDF.CreateICCBasedColorSpace(const ICCProfile: WideString): Integer;
begin
   LoadFunction(@pdfCreateICCBasedColorSpaceW, 'pdfCreateICCBasedColorSpaceW');
   Result := pdfCreateICCBasedColorSpaceW(FInstance, PWideChar(ICCProfile));
end;
{$endif}

function TPDF.CreateICCBasedColorSpaceA(const ICCProfile: AnsiString): Integer;
begin
   LoadFunction(@pdfCreateICCBasedColorSpaceA, 'pdfCreateICCBasedColorSpaceA');
   Result := pdfCreateICCBasedColorSpaceA(FInstance, PAnsiChar(ICCProfile));
end;

function TPDF.CreateICCBasedColorSpaceW(const ICCProfile: WideString): Integer;
begin
   LoadFunction(@pdfCreateICCBasedColorSpaceW, 'pdfCreateICCBasedColorSpaceW');
   Result := pdfCreateICCBasedColorSpaceW(FInstance, PWideChar(ICCProfile));
end;

function TPDF.CreateICCBasedColorSpaceEx(const Buffer: Pointer; BufSize: Cardinal): Integer;
begin
   LoadFunction(@pdfCreateICCBasedColorSpaceEx, 'pdfCreateICCBasedColorSpaceEx');
   Result := pdfCreateICCBasedColorSpaceEx(FInstance, Buffer, BufSize);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateImage(const FileName: AnsiString; Format: TImageFormat): Boolean;
begin
   LoadFunction(@pdfCreateImageA, 'pdfCreateImageA');
   Result := pdfCreateImageA(FInstance, PAnsiChar(FileName), Format);
end;

function TPDF.CreateImage(const FileName: WideString; Format: TImageFormat): Boolean;
begin
   LoadFunction(@pdfCreateImageW, 'pdfCreateImageW');
   Result := pdfCreateImageW(FInstance, PWideChar(FileName), Format);
end;
{$endif}

function TPDF.CreateImageA(const FileName: AnsiString; Format: TImageFormat): Boolean;
begin
   LoadFunction(@pdfCreateImageA, 'pdfCreateImageA');
   Result := pdfCreateImageA(FInstance, PAnsiChar(FileName), Format);
end;

function TPDF.CreateImageW(const FileName: WideString; Format: TImageFormat): Boolean;
begin
   LoadFunction(@pdfCreateImageW, 'pdfCreateImageW');
   Result := pdfCreateImageW(FInstance, PWideChar(FileName), Format);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateImpDataAction(const DataFile: AnsiString): Integer;
begin
   LoadFunction(@pdfCreateImpDataActionA, 'pdfCreateImpDataActionA');
   Result := pdfCreateImpDataActionA(FInstance, PAnsiChar(DataFile));
end;

function TPDF.CreateImpDataAction(const DataFile: WideString): Integer;
begin
   LoadFunction(@pdfCreateImpDataActionW, 'pdfCreateImpDataActionW');
   Result := pdfCreateImpDataActionW(FInstance, PWideChar(DataFile));
end;
{$endif}

function TPDF.CreateImpDataActionA(const DataFile: AnsiString): Integer;
begin
   LoadFunction(@pdfCreateImpDataActionA, 'pdfCreateImpDataActionA');
   Result := pdfCreateImpDataActionA(FInstance, PAnsiChar(DataFile));
end;

function TPDF.CreateImpDataActionW(const DataFile: WideString): Integer;
begin
   LoadFunction(@pdfCreateImpDataActionW, 'pdfCreateImpDataActionW');
   Result := pdfCreateImpDataActionW(FInstance, PWideChar(DataFile));
end;

function TPDF.CreateIndexedColorSpace(Base: TExtColorSpace; Handle: Integer; const ColorTable: Pointer; NumColors: Cardinal): Integer;
begin
   LoadFunction(@pdfCreateIndexedColorSpace, 'pdfCreateIndexedColorSpace');
   Result := pdfCreateIndexedColorSpace(FInstance, Integer(Base), Handle, ColorTable, NumColors);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateJSAction(const Script: AnsiString): Integer;
begin
   LoadFunction(@pdfCreateJSActionA, 'pdfCreateJSActionA');
   Result := pdfCreateJSActionA(FInstance, PAnsiChar(Script));
end;

function TPDF.CreateJSAction(const Script: WideString): Integer;
begin
   LoadFunction(@pdfCreateJSActionW, 'pdfCreateJSActionW');
   Result := pdfCreateJSActionW(FInstance, PWideChar(Script));
end;
{$endif}

function TPDF.CreateJSActionA(const Script: AnsiString): Integer;
begin
   LoadFunction(@pdfCreateJSActionA, 'pdfCreateJSActionA');
   Result := pdfCreateJSActionA(FInstance, PAnsiChar(Script));
end;

function TPDF.CreateJSActionW(const Script: WideString): Integer;
begin
   LoadFunction(@pdfCreateJSActionW, 'pdfCreateJSActionW');
   Result := pdfCreateJSActionW(FInstance, PWideChar(Script));
end;

function TPDF.CreateLaunchAction(OP: TFileOP; const FileName, DefDir, Param: AnsiString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateLaunchAction, 'pdfCreateLaunchAction');
   Result := pdfCreateLaunchAction(FInstance, Ord(OP), PAnsiChar(FileName), PAnsiChar(DefDir), PAnsiChar(Param), NewWindow);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateLaunchActionEx(const FileName: AnsiString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateLaunchActionExA, 'pdfCreateLaunchActionExA');
   Result := pdfCreateLaunchActionExA(FInstance, PAnsiChar(FileName), NewWindow);
end;

function TPDF.CreateLaunchActionEx(const FileName: WideString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateLaunchActionExW, 'pdfCreateLaunchActionExW');
   Result := pdfCreateLaunchActionExW(FInstance, PWideChar(FileName), NewWindow);
end;
{$endif}

function TPDF.CreateLaunchActionExA(const FileName: AnsiString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateLaunchActionExA, 'pdfCreateLaunchActionExA');
   Result := pdfCreateLaunchActionExA(FInstance, PAnsiChar(FileName), NewWindow);
end;

function TPDF.CreateLaunchActionExW(const FileName: WideString; NewWindow: Boolean): Integer;
begin
   LoadFunction(@pdfCreateLaunchActionExW, 'pdfCreateLaunchActionExW');
   Result := pdfCreateLaunchActionExW(FInstance, PWideChar(FileName), NewWindow);
end;

function TPDF.CreateListBox(const Name: PAnsiChar; Sort: Boolean; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
begin
   LoadFunction(@pdfCreateListBox, 'pdfCreateListBox');
   Result := pdfCreateListBox(FInstance, PAnsiChar(Name), Sort, Parent, PosX, PosY, Width, Height);
end;

function TPDF.CreateNamedAction(Action: TNamedAction): Integer;
begin
   LoadFunction(@pdfCreateNamedAction, 'pdfCreateNamedAction');
   Result := pdfCreateNamedAction(FInstance, Ord(Action));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateNamedDest(const Name: AnsiString; DestPage: Cardinal; DestType: TDestType; a, b, c, d: Double): Integer;
begin
   LoadFunction(@pdfCreateNamedDestA, 'pdfCreateNamedDestA');
   Result := pdfCreateNamedDestA(FInstance, PAnsiChar(Name), DestPage, DestType, a, b, c, d);
end;

function TPDF.CreateNamedDest(const Name: WideString; DestPage: Cardinal; DestType: TDestType; a, b, c, d: Double): Integer;
begin
   LoadFunction(@pdfCreateNamedDestW, 'pdfCreateNamedDestW');
   Result := pdfCreateNamedDestW(FInstance, PWideChar(Name), DestPage, DestType, a, b, c, d);
end;
{$endif}

function TPDF.CreateNamedDestA(const Name: AnsiString; DestPage: Cardinal; DestType: TDestType; a, b, c, d: Double): Integer;
begin
   LoadFunction(@pdfCreateNamedDestA, 'pdfCreateNamedDestA');
   Result := pdfCreateNamedDestA(FInstance, PAnsiChar(Name), DestPage, DestType, a, b, c, d);
end;

function TPDF.CreateNamedDestW(const Name: WideString; DestPage: Cardinal; DestType: TDestType; a, b, c, d: Double): Integer;
begin
   LoadFunction(@pdfCreateNamedDestW, 'pdfCreateNamedDestW');
   Result := pdfCreateNamedDestW(FInstance, PWideChar(Name), DestPage, DestType, a, b, c, d);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateNewPDF(const OutPDF: AnsiString): Boolean;
begin
   LoadFunction(@pdfCreateNewPDFA, 'pdfCreateNewPDFA');
   Result := pdfCreateNewPDFA(FInstance, PAnsiChar(OutPDF));
end;

function TPDF.CreateNewPDF(const OutPDF: WideString): Boolean;
begin
   LoadFunction(@pdfCreateNewPDFW, 'pdfCreateNewPDFW');
   Result := pdfCreateNewPDFW(FInstance, PWideChar(OutPDF));
end;
{$endif}

function TPDF.CreateNewPDFA(const OutPDF: AnsiString): Boolean;
begin
   LoadFunction(@pdfCreateNewPDFA, 'pdfCreateNewPDFA');
   Result := pdfCreateNewPDFA(FInstance, PAnsiChar(OutPDF));
end;

function TPDF.CreateNewPDFW(const OutPDF: WideString): Boolean;
begin
   LoadFunction(@pdfCreateNewPDFW, 'pdfCreateNewPDFW');
   Result := pdfCreateNewPDFW(FInstance, PWideChar(OutPDF));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.CreateOCG(const Name: AnsiString; DisplayInUI, Visible: Boolean; Intent: TOCGIntent): Integer;
begin
   LoadFunction(@pdfCreateOCGA, 'pdfCreateOCGA');
   Result := pdfCreateOCGA(FInstance, PAnsiChar(Name), DisplayInUI, Visible, Intent);
end;

function TPDF.CreateOCG(const Name: WideString; DisplayInUI, Visible: Boolean; Intent: TOCGIntent): Integer;
begin
   LoadFunction(@pdfCreateOCGW, 'pdfCreateOCGW');
   Result := pdfCreateOCGW(FInstance, PWideChar(Name), DisplayInUI, Visible, Intent);
end;
{$endif}

function TPDF.CreateOCGA(const Name: AnsiString; DisplayInUI, Visible: Boolean; Intent: TOCGIntent): Integer;
begin
   LoadFunction(@pdfCreateOCGA, 'pdfCreateOCGA');
   Result := pdfCreateOCGA(FInstance, PAnsiChar(Name), DisplayInUI, Visible, Intent);
end;

function TPDF.CreateOCGW(const Name: WideString; DisplayInUI, Visible: Boolean; Intent: TOCGIntent): Integer;
begin
   LoadFunction(@pdfCreateOCGW, 'pdfCreateOCGW');
   Result := pdfCreateOCGW(FInstance, PWideChar(Name), DisplayInUI, Visible, Intent);
end;

function TPDF.CreateOCMD(Visibility: TOCVisibility; const OCGs: PCardinal; Count: Cardinal): Integer;
begin
   LoadFunction(@pdfCreateOCMD, 'pdfCreateOCMD');
   Result := pdfCreateOCMD(FInstance, Visibility, OCGs, Count);
end;

function TPDF.CreateRadialShading(sX, sY, R1, eX, eY, R2, SCenter: Double; SColor, EColor: Cardinal; Extend1, Extend2: Boolean): Integer;
begin
   LoadFunction(@pdfCreateRadialShading, 'pdfCreateRadialShading');
   Result := pdfCreateRadialShading(FInstance, sX, sY, R1, eX, eY, R2, SCenter, SColor, EColor, Integer(Extend1), Integer(Extend2));
end;

function TPDF.CreateRadioButton(const Name, ExpValue: AnsiString; Checked: Boolean; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
begin
   LoadFunction(@pdfCreateRadioButton, 'pdfCreateRadioButton');
   Result := pdfCreateRadioButton(FInstance, PAnsiChar(Name), PAnsiChar(ExpValue), Integer(Checked), Parent, PosX, PosY, Width, Height);
end;

function TPDF.CreateRasterizer(Rows: PPointer; Buffer: Pointer; Width, Height: Cardinal; ScanlineLen: Integer; PixelFormat: TPDFPixFormat): Pointer;
begin
   LoadFunction(@rasCreateRasterizer, 'rasCreateRasterizer');
   Result := rasCreateRasterizer(FInstance, Rows, Buffer, Width, Height, ScanlineLen, PixelFormat);
end;

function TPDF.CreateRasterizerEx(DC: HDC; Width, Height: Cardinal; PixFmt: TPDFPixFormat): Pointer;
begin
   LoadFunction(@rasCreateRasterizerEx, 'rasCreateRasterizerEx');
   Result := rasCreateRasterizerEx(FInstance, DC, Width, Height, PixFmt);
end;

function TPDF.CreateResetAction(): Integer;
begin
   LoadFunction(@pdfCreateResetAction, 'pdfCreateResetAction');
   Result := pdfCreateResetAction(FInstance);
end;

function TPDF.CreateSeparationCS(const Colorant: AnsiString; Alternate: TExtColorSpace; Handle: Integer; Color: Cardinal): Integer;
begin
   LoadFunction(@pdfCreateSeparationCS, 'pdfCreateSeparationCS');
   Result := pdfCreateSeparationCS(FInstance, PAnsiChar(Colorant), Integer(Alternate), Handle, Color);
end;

function TPDF.CreateSetOCGStateAction(const On_: PCardinal; OnCount: Cardinal; const Off: PCardinal; OffCount: Cardinal; const Toggle: PCardinal; ToggleCount: Cardinal; PreserveRB: Boolean): Integer;
begin
   LoadFunction(@pdfCreateSetOCGStateAction, 'pdfCreateSetOCGStateAction');
   Result := pdfCreateSetOCGStateAction(FInstance, On_, OnCount, Off, OffCount, Toggle, ToggleCount, PreserveRB);
end;

function TPDF.CreateSigField(const Name: AnsiString; Parent: Integer; PosX, PosY, Width, Height: Double): Integer;
begin
   LoadFunction(@pdfCreateSigField, 'pdfCreateSigField');
   Result := pdfCreateSigField(FInstance, PAnsiChar(Name), Parent, PosX, PosY, Width, Height);
end;

function TPDF.CreateSigFieldAP(SigField: Cardinal): Integer;
begin
   LoadFunction(@pdfCreateSigFieldAP, 'pdfCreateSigFieldAP');
   Result := pdfCreateSigFieldAP(FInstance, SigField);
end;

function TPDF.CreateSoftMask(TranspGroup: Cardinal; MaskType: TSoftMaskType; BackColor: Cardinal): Pointer;
begin
   LoadFunction(@pdfCreateSoftMask, 'pdfCreateSoftMask');
   Result := pdfCreateSoftMask(FInstance, TranspGroup, MaskType, BackColor);
end;

function TPDF.CreateStdPattern(Pattern: TStdPattern; LineWidth, Distance: Double; LineColor, BackColor: Cardinal): Integer;
begin
   LoadFunction(@pdfCreateStdPattern, 'pdfCreateStdPattern');
   Result := pdfCreateStdPattern(FInstance, Ord(Pattern), LineWidth, Distance, LineColor, BackColor);
end;

function TPDF.CreateStructureTree: Boolean;
begin
   LoadFunction(@pdfCreateStructureTree, 'pdfCreateStructureTree');
   Result := pdfCreateStructureTree(FInstance);
end;

function TPDF.CreateSubmitAction(Flags: TSubmitFlags; const URL: AnsiString): Integer;
begin
   LoadFunction(@pdfCreateSubmitAction, 'pdfCreateSubmitAction');
   Result := pdfCreateSubmitAction(FInstance, Flags, PAnsiChar(URL));
end;

function TPDF.CreateTextField(const Name: AnsiString; Parent: Integer; Multiline: Boolean; MaxLen: Integer; PosX, PosY, Width, Height: Double): Integer;
begin
   LoadFunction(@pdfCreateTextField, 'pdfCreateTextField');
   Result := pdfCreateTextField(FInstance, PAnsiChar(Name), Parent, Integer(Multiline), MaxLen, PosX, PosY, Width, Height);
end;

function TPDF.CreateURIAction(const URL: AnsiString): Integer;
begin
   LoadFunction(@pdfCreateURIAction, 'pdfCreateURIAction');
   Result := pdfCreateURIAction(FInstance, PAnsiChar(URL));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.DecryptPDF(const FileName: AnsiString; PwdType: TPwdType; const Password: AnsiString): Integer;
begin
   LoadFunction(@pdfDecryptPDFA, 'pdfDecryptPDFA');
   Result := pdfDecryptPDFA(FInstance, PAnsiChar(FileName), Ord(PwdType), PAnsiChar(Password));
end;

function TPDF.DecryptPDF(const FileName: WideString; PwdType: TPwdType; const Password: AnsiString): Integer;
begin
   LoadFunction(@pdfDecryptPDFW, 'pdfDecryptPDFW');
   Result := pdfDecryptPDFW(FInstance, PWideChar(FileName), Ord(PwdType), PAnsiChar(Password));
end;
{$endif}

function TPDF.DecryptPDFA(const FileName: AnsiString; PwdType: TPwdType; const Password: AnsiString): Integer;
begin
   LoadFunction(@pdfDecryptPDFA, 'pdfDecryptPDFA');
   Result := pdfDecryptPDFA(FInstance, PAnsiChar(FileName), Ord(PwdType), PAnsiChar(Password));
end;

function TPDF.DecryptPDFW(const FileName: WideString; PwdType: TPwdType; const Password: AnsiString): Integer;
begin
   LoadFunction(@pdfDecryptPDFW, 'pdfDecryptPDFW');
   Result := pdfDecryptPDFW(FInstance, PWideChar(FileName), Ord(PwdType), PAnsiChar(Password));
end;

procedure TPDF.DeleteAcroForm;
begin
   LoadFunction(@pdfDeleteAcroForm, 'pdfDeleteAcroForm');
   pdfDeleteAcroForm(FInstance);
end;

function TPDF.DeleteActionFromObj(ObjType: TObjType; ActHandle, ObjHandle: Cardinal): Integer;
begin
   LoadFunction(@pdfDeleteActionFromObj, 'pdfDeleteActionFromObj');
   Result := pdfDeleteActionFromObj(FInstance, Ord(ObjType), ActHandle, ObjHandle);
end;

function TPDF.DeleteActionFromObjEx(ObjType: TObjType; ObjHandle, ActIndex: Cardinal): Integer;
begin
   LoadFunction(@pdfDeleteActionFromObjEx, 'pdfDeleteActionFromObjEx');
   Result := pdfDeleteActionFromObjEx(FInstance, Ord(ObjType), ObjHandle, ActIndex);
end;

function TPDF.DeleteAnnotation(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfDeleteAnnotation, 'pdfDeleteAnnotation');
   Result := pdfDeleteAnnotation(FInstance, Handle);
end;

function TPDF.DeleteBookmark(ABmk: Cardinal): Integer;
begin
   LoadFunction(@pdfDeleteBookmark, 'pdfDeleteBookmark');
   Result := pdfDeleteBookmark(FInstance, ABmk);
end;

function TPDF.DeleteEmbeddedFile(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfDeleteEmbeddedFile, 'pdfDeleteEmbeddedFile');
   Result := pdfDeleteEmbeddedFile(FInstance, Handle);
end;

function TPDF.DeleteField(AField: Cardinal): Integer;
begin
   LoadFunction(@pdfDeleteField, 'pdfDeleteField');
   Result := pdfDeleteField(FInstance, AField);
end;

function TPDF.DeleteFieldEx(const Name: AnsiString): Integer;
begin
   LoadFunction(@pdfDeleteFieldEx, 'pdfDeleteFieldEx');
   Result := pdfDeleteFieldEx(FInstance, PAnsiChar(Name));
end;

procedure TPDF.DeleteJavaScripts(DelJavaScriptActions: Boolean);
begin
   LoadFunction(@pdfDeleteJavaScripts, 'pdfDeleteJavaScripts');
   pdfDeleteJavaScripts(FInstance, DelJavaScriptActions);
end;

function TPDF.DeleteOCGFromAppEvent(Handle: Cardinal; Events: TOCAppEvent; Categories: TOCGUsageCategory; DelCategoryOnly: Boolean): Boolean;
begin
   LoadFunction(@pdfDeleteOCGFromAppEvent, 'pdfDeleteOCGFromAppEvent');
   Result := pdfDeleteOCGFromAppEvent(FInstance, Handle, Events, Categories, DelCategoryOnly);
end;

function TPDF.DeleteOutputIntent(Index: Integer): Integer;
begin
   LoadFunction(@pdfDeleteOutputIntent, 'pdfDeleteOutputIntent');
   Result := pdfDeleteOutputIntent(FInstance, Index);
end;

function TPDF.DeletePage(PageNum: Cardinal): Integer;
begin
   LoadFunction(@pdfDeletePage, 'pdfDeletePage');
   Result := pdfDeletePage(FInstance, PageNum);
end;

procedure TPDF.DeletePageLabels;
begin
   LoadFunction(@pdfDeletePageLabels, 'pdfDeletePageLabels');
   pdfDeletePageLabels(FInstance);
end;

procedure TPDF.DeleteRasterizer(RasPtr: PPointer);
begin
   LoadFunction(@rasDeleteRasterizer, 'rasDeleteRasterizer');
   rasDeleteRasterizer(RasPtr);
end;

function TPDF.DeleteSeparationInfo(AllPages: Boolean): Boolean;
begin
   LoadFunction(@pdfDeleteSeparationInfo, 'pdfDeleteSeparationInfo');
   Result := pdfDeleteSeparationInfo(FInstance, AllPages);
end;

function TPDF.DeleteTemplate(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfDeleteTemplate, 'pdfDeleteTemplate');
   Result := pdfDeleteTemplate(FInstance, Handle);
end;

function TPDF.DeleteTemplateEx(Index: Cardinal): Integer;
begin
   LoadFunction(@pdfDeleteTemplateEx, 'pdfDeleteTemplateEx');
   Result := pdfDeleteTemplateEx(FInstance, Index);
end;

procedure TPDF.DeleteXFAForm;
begin
   LoadFunction(@pdfDeleteXFAForm, 'pdfDeleteXFAForm');
   pdfDeleteXFAForm(FInstance);
end;

function TPDF.DrawArc(PosX, PosY, Radius, StartAngle, EndAngle: Double): Boolean;
begin
   LoadFunction(@pdfDrawArc, 'pdfDrawArc');
   Result := pdfDrawArc(FInstance, PosX, PosY, Radius, StartAngle, EndAngle);
end;

function TPDF.DrawArcEx(PosX, PosY, Width, Height, StartAngle, EndAngle: Double): Boolean;
begin
   LoadFunction(@pdfDrawArcEx, 'pdfDrawArcEx');
   Result := pdfDrawArcEx(FInstance, PosX, PosY, Width, Height, StartAngle, EndAngle);
end;

function TPDF.DrawChord(PosX, PosY, Width, Height, StartAngle, EndAngle: Double; FillMode: TPathFillMode): Boolean;
begin
   LoadFunction(@pdfDrawChord, 'pdfDrawChord');
   Result := pdfDrawChord(FInstance, PosX, PosY, Width, Height, StartAngle, EndAngle, Ord(FillMode));
end;

function TPDF.DrawCircle(PosX, PosY, Radius: Double; FillMode: TPathFillMode): Boolean;
begin
   LoadFunction(@pdfDrawCircle, 'pdfDrawCircle');
   Result := pdfDrawCircle(FInstance, PosX, PosY, Radius, Ord(FillMode));
end;

function TPDF.DrawPie(PosX, PosY, Width, Height, StartAngle, EndAngle: Double; FillMode: TPathFillMode): Boolean;
begin
   LoadFunction(@pdfDrawPie, 'pdfDrawPie');
   Result := pdfDrawPie(FInstance, PosX, PosY, Width, Height, StartAngle, EndAngle, Ord(FillMode));
end;

function TPDF.EditPage(PageNum: Integer): Boolean;
begin
   LoadFunction(@pdfEditPage, 'pdfEditPage');
   Result := pdfEditPage(FInstance, PageNum);
end;

function TPDF.EditTemplate(Index: Cardinal): Boolean;
begin
   LoadFunction(@pdfEditTemplate, 'pdfEditTemplate');
   Result := pdfEditTemplate(FInstance, Index);
end;

function TPDF.EditTemplate2(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfEditTemplate2, 'pdfEditTemplate2');
   Result := pdfEditTemplate2(FInstance, Handle);
end;

function TPDF.Ellipse(PosX, PosY, Width, Height: Double; FillMode: TPathFillMode): Boolean;
begin
   LoadFunction(@pdfEllipse, 'pdfEllipse');
   Result := pdfEllipse(FInstance, PosX, PosY, Width, Height, Ord(FillMode));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.EncryptPDF(const FileName, OpenPwd, OwnerPwd: AnsiString; KeyLen: TKeyLen; Restrict: TRestrictions): Integer;
begin
   LoadFunction(@pdfEncryptPDFA, 'pdfEncryptPDFA');
   Result := pdfEncryptPDFA(FInstance, PAnsiChar(FileName), PAnsiChar(OpenPwd), PAnsiChar(OwnerPwd), KeyLen, Restrict);
end;

function TPDF.EncryptPDF(const FileName: WideString; const OpenPwd, OwnerPwd: AnsiString; KeyLen: TKeyLen; Restrict: TRestrictions): Integer;
begin
   LoadFunction(@pdfEncryptPDFW, 'pdfEncryptPDFW');
   Result := pdfEncryptPDFW(FInstance, PWideChar(FileName), PAnsiChar(OpenPwd), PAnsiChar(OwnerPwd), KeyLen, Restrict);
end;
{$endif}

function TPDF.EncryptPDFA(const FileName, OpenPwd, OwnerPwd: AnsiString; KeyLen: TKeyLen; Restrict: TRestrictions): Integer;
begin
   LoadFunction(@pdfEncryptPDFA, 'pdfEncryptPDFA');
   Result := pdfEncryptPDFA(FInstance, PAnsiChar(FileName), PAnsiChar(OpenPwd), PAnsiChar(OwnerPwd), KeyLen, Restrict);
end;

function TPDF.EncryptPDFW(const FileName: WideString; const OpenPwd, OwnerPwd: AnsiString; KeyLen: TKeyLen; Restrict: TRestrictions): Integer;
begin
   LoadFunction(@pdfEncryptPDFW, 'pdfEncryptPDFW');
   Result := pdfEncryptPDFW(FInstance, PWideChar(FileName), PAnsiChar(OpenPwd), PAnsiChar(OwnerPwd), KeyLen, Restrict);
end;

function TPDF.EndContinueText(): Integer;
begin
   LoadFunction(@pdfEndContinueText, 'pdfEndContinueText');
   Result := pdfEndContinueText(FInstance);
end;

function TPDF.EndLayer: Boolean;
begin
   LoadFunction(@pdfEndLayer, 'pdfEndLayer');
   Result := pdfEndLayer(FInstance);
end;

function TPDF.EndPage(): Boolean;
begin
   LoadFunction(@pdfEndPage, 'pdfEndPage');
   Result := pdfEndPage(FInstance);
end;

function TPDF.EndPattern(): Boolean;
begin
   LoadFunction(@pdfEndPattern, 'pdfEndPattern');
   Result := pdfEndPattern(FInstance);
end;

function TPDF.EndTemplate(): Boolean;
begin
   LoadFunction(@pdfEndTemplate, 'pdfEndTemplate');
   Result := pdfEndTemplate(FInstance);
end;

function TPDF.EnumDocFonts(const Data: Pointer; EnumProc: TEnumFontProc2): Integer;
begin
   LoadFunction(@pdfEnumDocFonts, 'pdfEnumDocFonts');
   Result := pdfEnumDocFonts(FInstance, Data, EnumProc);
end;

function TPDF.EnumHostFonts(const Data: Pointer; EnumProc: TEnumFontProc): Integer;
begin
   LoadFunction(@pdfEnumHostFonts, 'pdfEnumHostFonts');
   Result := pdfEnumHostFonts(FInstance, Data, EnumProc);
end;

function TPDF.EnumHostFontsEx(const Data: Pointer; EnumProc: TEnumFontProcEx): Integer;
begin
   LoadFunction(@pdfEnumHostFontsEx, 'pdfEnumHostFontsEx');
   Result := pdfEnumHostFontsEx(FInstance, Data, EnumProc);
end;

function TPDF.ExchangeBookmarks(Bmk1, Bmk2: Integer): Boolean;
begin
   LoadFunction(@pdfExchangeBookmarks, 'pdfExchangeBookmarks');
   Result := pdfExchangeBookmarks(FInstance, Bmk1, Bmk2);
end;

function TPDF.ExchangePages(First, Second: Cardinal): Boolean;
begin
   LoadFunction(@pdfExchangePages, 'pdfExchangePages');
   Result := pdfExchangePages(FInstance, First, Second);
end;

function TPDF.FileAttachAnnotA(PosX, PosY: Double; Icon: TFileAttachIcon; const Author, Desc, AFile: AnsiString; Compress: Boolean): Integer;
begin
   LoadFunction(@pdfFileAttachAnnotA, 'pdfFileAttachAnnotA');
   Result := pdfFileAttachAnnotA(FInstance, PosX, PosY, Integer(Icon), PAnsiChar(Author), PAnsiChar(Desc), PAnsiChar(AFile), Compress);
end;

function TPDF.FileAttachAnnotW(PosX, PosY: Double; Icon: TFileAttachIcon; const Author, Desc, AFile: WideString; Compress: Boolean): Integer;
begin
   LoadFunction(@pdfFileAttachAnnotW, 'pdfFileAttachAnnotW');
   Result := pdfFileAttachAnnotW(FInstance, PosX, PosY, Integer(Icon), PWideChar(Author), PWideChar(Desc), PWideChar(AFile), Compress);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.FileLink(PosX, PosY, Width, Height: Double; const AFilePath: AnsiString): Integer;
begin
   LoadFunction(@pdfFileLinkA, 'pdfFileLinkA');
   Result := pdfFileLinkA(FInstance, PosX, PosY, Width, Height, PAnsiChar(AFilePath));
end;

function TPDF.FileLink(PosX, PosY, Width, Height: Double; const AFilePath: WideString): Integer;
begin
   LoadFunction(@pdfFileLinkW, 'pdfFileLinkW');
   Result := pdfFileLinkW(FInstance, PosX, PosY, Width, Height, PWideChar(AFilePath));
end;
{$endif}

function TPDF.FileLinkA(PosX, PosY, Width, Height: Double; const AFilePath: AnsiString): Integer;
begin
   LoadFunction(@pdfFileLinkA, 'pdfFileLinkA');
   Result := pdfFileLinkA(FInstance, PosX, PosY, Width, Height, PAnsiChar(AFilePath));
end;

function TPDF.FileLinkW(PosX, PosY, Width, Height: Double; const AFilePath: WideString): Integer;
begin
   LoadFunction(@pdfFileLinkW, 'pdfFileLinkW');
   Result := pdfFileLinkW(FInstance, PosX, PosY, Width, Height, PWideChar(AFilePath));
end;

function TPDF.FindBookmarkA(DestPage: Integer; const Title: AnsiString): Integer;
begin
   LoadFunction(@pdfFindBookmarkA, 'pdfFindBookmarkA');
   Result := pdfFindBookmarkA(FInstance, DestPage, PAnsiChar(Title));
end;

function TPDF.FindBookmarkW(DestPage: Integer; const Title: WideString): Integer;
begin
   LoadFunction(@pdfFindBookmarkW, 'pdfFindBookmarkW');
   Result := pdfFindBookmarkW(FInstance, DestPage, PWideChar(Title));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.FindField(const Name: AnsiString): Integer;
begin
   LoadFunction(@pdfFindFieldA, 'pdfFindFieldA');
   Result := pdfFindFieldA(FInstance, PAnsiChar(Name));
end;

function TPDF.FindField(const Name: WideString): Integer;
begin
   LoadFunction(@pdfFindFieldW, 'pdfFindFieldW');
   Result := pdfFindFieldW(FInstance, PWideChar(Name));
end;
{$endif}

function TPDF.FindFieldA(const Name: AnsiString): Integer;
begin
   LoadFunction(@pdfFindFieldA, 'pdfFindFieldA');
   Result := pdfFindFieldA(FInstance, PAnsiChar(Name));
end;

function TPDF.FindFieldW(const Name: WideString): Integer;
begin
   LoadFunction(@pdfFindFieldW, 'pdfFindFieldW');
   Result := pdfFindFieldW(FInstance, PWideChar(Name));
end;

function TPDF.FindLinkAnnot(const URL: AnsiString): Integer;
begin
   LoadFunction(@pdfFindLinkAnnot, 'pdfFindLinkAnnot');
   Result := pdfFindLinkAnnot(FInstance, PAnsiChar(URL));
end;

function TPDF.FindNextBookmark(): Integer;
begin
   LoadFunction(@pdfFindNextBookmark, 'pdfFindNextBookmark');
   Result := pdfFindNextBookmark(FInstance);
end;

function TPDF.FinishSignature(const PKCS7Obj: Pointer; Length: Cardinal): Boolean;
begin
   LoadFunction(@pdfFinishSignature, 'pdfFinishSignature');
   Result := pdfFinishSignature(FInstance, PKCS7Obj, Length);
end;

function TPDF.FlattenAnnots(Flags: TAnnotFlattenFlags): Integer;
begin
   LoadFunction(@pdfFlattenAnnots, 'pdfFlattenAnnots');
   Result := pdfFlattenAnnots(FInstance, Flags);
end;

function TPDF.FlattenForm(): Boolean;
begin
   LoadFunction(@pdfFlattenForm, 'pdfFlattenForm');
   Result := pdfFlattenForm(FInstance);
end;

function TPDF.FlushPageContent(var Stack: TPDFStack): Boolean;
begin
   LoadFunction(@pdfFlushPageContent, 'pdfFlushPageContent');
   Result := pdfFlushPageContent(FInstance, Stack);
end;

function TPDF.FlushPages(Flags: TFlushPageFlags): Boolean;
begin
   LoadFunction(@pdfFlushPages, 'pdfFlushPages');
   Result := pdfFlushPages(FInstance, Integer(Flags));
end;

procedure TPDF.FreeImageBuffer;
begin
   LoadFunction(@pdfFreeImageBuffer, 'pdfFreeImageBuffer');
   pdfFreeImageBuffer(FInstance);
end;

function TPDF.FreeImageObj(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfFreeImageObj, 'pdfFreeImageObj');
   Result := pdfFreeImageObj(FInstance, Handle);
end;

function TPDF.FreeImageObjEx(const ImagePtr: Pointer): Boolean;
begin
   LoadFunction(@pdfFreeImageObjEx, 'pdfFreeImageObjEx');
   Result := pdfFreeImageObjEx(FInstance, ImagePtr);
end;

function TPDF.FreePDF(): Boolean;
begin
   LoadFunction(@pdfFreePDF, 'pdfFreePDF');
   Result := pdfFreePDF(FInstance);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.FreeTextAnnot(PosX, PosY, Width, Height: Double; const Author, AText: AnsiString; Align: TTextAlign): Integer;
begin
   LoadFunction(@pdfFreeTextAnnotA, 'pdfFreeTextAnnotA');
   Result := pdfFreeTextAnnotA(FInstance, PosX, PosY, Width, Height, PAnsiChar(Author), PAnsiChar(AText), Align);
end;

function TPDF.FreeTextAnnot(PosX, PosY, Width, Height: Double; const Author, AText: WideString; Align: TTextAlign): Integer;
begin
   LoadFunction(@pdfFreeTextAnnotW, 'pdfFreeTextAnnotW');
   Result := pdfFreeTextAnnotW(FInstance, PosX, PosY, Width, Height, PWideChar(Author), PWideChar(AText), Align);
end;
{$endif}

function TPDF.FreeTextAnnotA(PosX, PosY, Width, Height: Double; const Author, AText: AnsiString; Align: TTextAlign): Integer;
begin
   LoadFunction(@pdfFreeTextAnnotA, 'pdfFreeTextAnnotA');
   Result := pdfFreeTextAnnotA(FInstance, PosX, PosY, Width, Height, PAnsiChar(Author), PAnsiChar(AText), Align);
end;

function TPDF.FreeTextAnnotW(PosX, PosY, Width, Height: Double; const Author, AText: WideString; Align: TTextAlign): Integer;
begin
   LoadFunction(@pdfFreeTextAnnotW, 'pdfFreeTextAnnotW');
   Result := pdfFreeTextAnnotW(FInstance, PosX, PosY, Width, Height, PWideChar(Author), PWideChar(AText), Align);
end;

function TPDF.FreeUniBuf(): Boolean;
begin
   LoadFunction(@pdfFreeUniBuf, 'pdfFreeUniBuf');
   Result := pdfFreeUniBuf(FInstance);
end;

function TPDF.GetActionCount(): Integer;
begin
   LoadFunction(@pdfGetActionCount, 'pdfGetActionCount');
   Result := pdfGetActionCount(FInstance);
end;

function TPDF.GetActionType(ActHandle: Cardinal): Integer;
begin
   LoadFunction(@pdfGetActionType, 'pdfGetActionType');
   Result := pdfGetActionType(FInstance, ActHandle);
end;

function TPDF.GetActionTypeEx(ObjType: TObjType; ObjHandle, ActIndex: Cardinal): Integer;
begin
   LoadFunction(@pdfGetActionTypeEx, 'pdfGetActionTypeEx');
   Result := pdfGetActionTypeEx(FInstance, ObjType, ObjHandle, ActIndex);
end;

function TPDF.GetActiveFont: Integer;
begin
   LoadFunction(@pdfGetActiveFont, 'pdfGetActiveFont');
   Result := pdfGetActiveFont(FInstance);
end;

function TPDF.GetAllocBy(): Integer;
begin
   LoadFunction(@pdfGetAllocBy, 'pdfGetAllocBy');
   Result := pdfGetAllocBy(FInstance);
end;

function TPDF.GetAnnot(Handle: Cardinal; var Annot: TPDFAnnotation): Boolean;
begin
   LoadFunction(@pdfGetAnnot, 'pdfGetAnnot');
   Result := pdfGetAnnot(FInstance, Handle, Annot);
end;

function TPDF.GetAnnotBBox(Handle: Cardinal; var BBox: TPDFRect): Boolean;
begin
   LoadFunction(@pdfGetAnnotBBox, 'pdfGetAnnotBBox');
   Result := pdfGetAnnotBBox(FInstance, Handle, BBox);
end;

function TPDF.GetAnnotCount(): Integer;
begin
   LoadFunction(@pdfGetAnnotCount, 'pdfGetAnnotCount');
   Result := pdfGetAnnotCount(FInstance);
end;

function TPDF.GetAnnotEx(Handle: Cardinal; var Annot: TPDFAnnotationEx): Boolean;
begin
   LoadFunction(@pdfGetAnnotEx, 'pdfGetAnnotEx');
   Result := pdfGetAnnotEx(FInstance, Handle, Annot);
end;

function TPDF.GetAnnotFlags(): Integer;
begin
   LoadFunction(@pdfGetAnnotFlags, 'pdfGetAnnotFlags');
   Result := pdfGetAnnotFlags(FInstance);
end;

function TPDF.GetAnnotLink(Handle: Cardinal): AnsiString;
begin
   LoadFunction(@pdfGetAnnotLink, 'pdfGetAnnotLink');
   Result := pdfGetAnnotLink(FInstance, Handle);
end;

function TPDF.GetAnnotType(Handle: Cardinal): Integer;
begin
   LoadFunction(@pdfGetAnnotType, 'pdfGetAnnotType');
   Result := pdfGetAnnotType(FInstance, Handle);
end;

function TPDF.GetAscent(): Double;
begin
   LoadFunction(@pdfGetAscent, 'pdfGetAscent');
   Result := pdfGetAscent(FInstance);
end;

function TPDF.GetBarcodeDict(const IBarcode: Pointer; var Barcode: TPDFBarcode): Boolean;
begin
   LoadFunction(@pdfGetBarcodeDict, 'pdfGetBarcodeDict');
   Barcode.StructSize := sizeof(TPDFBarcode);
   Result := pdfGetBarcodeDict(IBarcode, Barcode);
end;

function TPDF.GetBBox(Boundary: TPageBoundary; var BBox: TPDFRect): Boolean;
begin
   LoadFunction(@pdfGetBBox, 'pdfGetBBox');
   Result := pdfGetBBox(FInstance, Boundary, BBox);
end;

function TPDF.GetBidiMode: TPDFBidiMode;
begin
   LoadFunction(@pdfGetBidiMode, 'pdfGetBidiMode');
   Result := TPDFBidiMode(pdfGetBidiMode(FInstance));
end;

function TPDF.GetBookmark(Handle: Integer; var Bmk: TBookmark): Boolean;
begin
   LoadFunction(@pdfGetBookmark, 'pdfGetBookmark');
   Result := pdfGetBookmark(FInstance, Handle, Bmk);
end;

function TPDF.GetBookmarkCount(): Integer;
begin
   LoadFunction(@pdfGetBookmarkCount, 'pdfGetBookmarkCount');
   Result := pdfGetBookmarkCount(FInstance);
end;

function TPDF.GetBorderStyle(): TBorderStyle;
begin
   LoadFunction(@pdfGetBorderStyle, 'pdfGetBorderStyle');
   Result := TBorderStyle(pdfGetBorderStyle(FInstance));
end;

function TPDF.GetBuffer(var BufSize: Cardinal): PAnsiChar;
begin
   LoadFunction(@pdfGetBuffer, 'pdfGetBuffer');
   Result := pdfGetBuffer(FInstance, BufSize);
end;

function TPDF.GetCapHeight(): Double;
begin
   LoadFunction(@pdfGetCapHeight, 'pdfGetCapHeight');
   Result := pdfGetCapHeight(FInstance);
end;

function TPDF.GetCharacterSpacing(): Double;
begin
   LoadFunction(@pdfGetCharacterSpacing, 'pdfGetCharacterSpacing');
   Result := pdfGetCharacterSpacing(FInstance);
end;

function TPDF.GetCheckBoxChar: Integer;
begin
   LoadFunction(@pdfGetCheckBoxChar, 'pdfGetCheckBoxChar');
   Result := pdfGetCheckBoxChar(FInstance);
end;

function TPDF.GetCheckBoxCharEx(AField: Cardinal): Integer;
begin
   LoadFunction(@pdfGetCheckBoxCharEx, 'pdfGetCheckBoxCharEx');
   Result := pdfGetCheckBoxCharEx(FInstance, AField);
end;

function TPDF.GetCheckBoxDefState(AField: Cardinal): Integer;
begin
   LoadFunction(@pdfGetCheckBoxDefState, 'pdfGetCheckBoxDefState');
   Result := pdfGetCheckBoxDefState(FInstance, AField);
end;

function TPDF.GetCMap(Index: Cardinal; var CMap: TPDFCMap): Boolean;
begin
   LoadFunction(@pdfGetCMap, 'pdfGetCMap');
   CMap.StructSize := sizeof(CMap);
   Result := pdfGetCMap(FInstance, Index, CMap);
end;

function TPDF.GetCMapCount: Integer;
begin
   LoadFunction(@pdfGetCMapCount, 'pdfGetCMapCount');
   Result := pdfGetCMapCount(FInstance);
end;

function TPDF.GetColorSpace(): Integer;
begin
   LoadFunction(@pdfGetColorSpace, 'pdfGetColorSpace');
   Result := pdfGetColorSpace(FInstance);
end;

function TPDF.GetColorSpaceCount: Integer;
begin
   LoadFunction(@pdfGetColorSpaceCount, 'pdfGetColorSpaceCount');
   Result := pdfGetColorSpaceCount(FInstance);
end;

function TPDF.GetColorSpaceObj(Handle: Cardinal; var CS: TPDFColorSpaceObj): Boolean;
begin
   LoadFunction(@pdfGetColorSpaceObj, 'pdfGetColorSpaceObj');
   Result := pdfGetColorSpaceObj(FInstance, Handle, CS);
end;

function TPDF.GetColorSpaceObjEx(const IColorSpace: Pointer; var CS: TPDFColorSpaceObj): Boolean;
begin
   LoadFunction(@pdfGetColorSpaceObjEx, 'pdfGetColorSpaceObjEx');
   Result := pdfGetColorSpaceObjEx(IColorSpace, CS);
end;

function TPDF.GetCompressionFilter: Integer;
begin
   LoadFunction(@pdfGetCompressionFilter, 'pdfGetCompressionFilter');
   Result := pdfGetCompressionFilter(FInstance);
end;

function TPDF.GetCompressionLevel(): Integer;
begin
   LoadFunction(@pdfGetCompressionLevel, 'pdfGetCompressionLevel');
   Result := pdfGetCompressionLevel(FInstance);
end;

function TPDF.GetContent(var Buffer: PAnsiChar): Integer;
begin
   LoadFunction(@pdfGetContent, 'pdfGetContent');
   Result := pdfGetContent(FInstance, Buffer);
end;

function TPDF.GetDefBitsPerPixel(): Integer;
begin
   LoadFunction(@pdfGetDefBitsPerPixel, 'pdfGetDefBitsPerPixel');
   Result := pdfGetDefBitsPerPixel(FInstance);
end;

function TPDF.GetDescent(): Double;
begin
   LoadFunction(@pdfGetDescent, 'pdfGetDescent');
   Result := pdfGetDescent(FInstance);
end;

function TPDF.GetDeviceNAttributes(IAttributes: Pointer; var Attributes: TDeviceNAttributes): Boolean;
begin
   LoadFunction(@pdfGetDeviceNAttributes, 'pdfGetDeviceNAttributes');
   Result := pdfGetDeviceNAttributes(IAttributes, Attributes);
end;

function TPDF.GetDocInfo(DInfo: TDocumentInfo; var Value: PWideChar): Integer;
begin
   LoadFunction(@pdfGetDocInfo, 'pdfGetDocInfo');
   Result := pdfGetDocInfo(FInstance, Ord(DInfo), Value);
end;

function TPDF.GetDocInfoCount: Integer;
begin
   LoadFunction(@pdfGetDocInfoCount, 'pdfGetDocInfoCount');
   Result := pdfGetDocInfoCount(FInstance);
end;

function TPDF.GetDocInfoEx(Index: Cardinal; var DInfo: TDocumentInfo; var Key, Value: PAnsiChar; var Unicode: LongBool): Integer;
begin
   LoadFunction(@pdfGetDocInfoEx, 'pdfGetDocInfoEx');
   Result := pdfGetDocInfoEx(FInstance, Index, DInfo, Key, Value, Unicode);
end;

function TPDF.GetDocUsesTransparency(Flags: Cardinal): Boolean;
begin
   LoadFunction(@pdfGetDocUsesTransparency, 'pdfGetDocUsesTransparency');
   Result := pdfGetDocUsesTransparency(FInstance, Flags);
end;

function TPDF.GetDrawDirection(): Integer;
begin
   LoadFunction(@pdfGetDrawDirection, 'pdfGetDrawDirection');
   Result := pdfGetDrawDirection(FInstance);
end;

function TPDF.GetDynaPDFVersion: AnsiString;
begin
   LoadFunction(@pdfGetDynaPDFVersion, 'pdfGetDynaPDFVersion');
   Result := pdfGetDynaPDFVersion();
end;

function TPDF.GetEmbeddedFile(Handle: Cardinal; var FileSpec: TPDFFileSpec; Decompress: Boolean): Boolean;
begin
   LoadFunction(@pdfGetEmbeddedFile, 'pdfGetEmbeddedFile');
   Result := pdfGetEmbeddedFile(FInstance, Handle, FileSpec, Decompress);
end;

function TPDF.GetEmbeddedFileCount: Integer;
begin
   LoadFunction(@pdfGetEmbeddedFileCount, 'pdfGetEmbeddedFileCount');
   Result := pdfGetEmbeddedFileCount(FInstance);
end;

function TPDF.GetEMFPatternDistance(): Double;
begin
   LoadFunction(@pdfGetEMFPatternDistance, 'pdfGetEMFPatternDistance');
   Result := pdfGetEMFPatternDistance(FInstance);
end;

function TPDF.GetErrLogMessage(Index: Cardinal; var Err: TPDFError): Boolean;
begin
   LoadFunction(@pdfGetErrLogMessage, 'pdfGetErrLogMessage');
   Result := pdfGetErrLogMessage(FInstance, Index, Err);
end;

function TPDF.GetErrLogMessageCount: Integer;
begin
   LoadFunction(@pdfGetErrLogMessageCount, 'pdfGetErrLogMessageCount');
   Result := pdfGetErrLogMessageCount(FInstance);
end;

function TPDF.GetErrorMessage(): AnsiString;
begin
   LoadFunction(@pdfGetErrorMessage, 'pdfGetErrorMessage');
   Result := pdfGetErrorMessage(FInstance);
end;

function TPDF.GetErrorMode(): Integer;
begin
   LoadFunction(@pdfGetErrorMode, 'pdfGetErrorMode');
   Result := pdfGetErrorMode(FInstance);
end;

function TPDF.GetField(Handle: Cardinal; var Field: TPDFField): Boolean;
begin
   LoadFunction(@pdfGetField, 'pdfGetField');
   {
      Note: FieldName can be an Unicode string!
      To determine whether FieldName contains an ANSI or Unicode string
      proceed as follows:
      if f.FielNameLen > 0 and StrLen(f.FieldName) <> f.FieldNameLen then begin
         // f.FieldName contains a null-terminated Unicode string
         // f.FieldNameLen is the length in Unicode characters, but we have an ANSI string.
         // So, we must multiply the length with 2 to convert the entire string!
         nameW := pdf.ToUnicode(f.FieldName, f.FieldNameLen * 2);         
      end;
   }
   Result := pdfGetField(FInstance, Handle, Field);
end;

function TPDF.GetFieldBackColor(): Cardinal;
begin
   LoadFunction(@pdfGetFieldBackColor, 'pdfGetFieldBackColor');
   Result := pdfGetFieldBackColor(FInstance);
end;

function TPDF.GetFieldBorderColor(): Cardinal;
begin
   LoadFunction(@pdfGetFieldBorderColor, 'pdfGetFieldBorderColor');
   Result := pdfGetFieldBorderColor(FInstance);
end;

function TPDF.GetFieldBorderStyle(AField: Cardinal): Integer;
begin
   LoadFunction(@pdfGetFieldBorderStyle, 'pdfGetFieldBorderStyle');
   Result := pdfGetFieldBorderStyle(FInstance, AField);
end;

function TPDF.GetFieldBorderWidth(AField: Cardinal): Double;
begin
   LoadFunction(@pdfGetFieldBorderWidth, 'pdfGetFieldBorderWidth');
   Result := pdfGetFieldBorderWidth(FInstance, AField);
end;

function TPDF.GetFieldChoiceValue(AField, ValIndex: Cardinal; var Value: TPDFChoiceValue): Boolean;
begin
   LoadFunction(@pdfGetFieldChoiceValue, 'pdfGetFieldChoiceValue');
   Result := pdfGetFieldChoiceValue(FInstance, AField, ValIndex, Value);
end;

function TPDF.GetFieldColor(AField: Cardinal; ColorType: TFieldColor; var ColorSpace: Integer; var Color: Cardinal): Boolean;
begin
   LoadFunction(@pdfGetFieldColor, 'pdfGetFieldColor');
   Result := pdfGetFieldColor(FInstance, AField, Integer(ColorType), ColorSpace, Color);
end;

function TPDF.GetFieldCount(): Integer;
begin
   LoadFunction(@pdfGetFieldCount, 'pdfGetFieldCount');
   Result := pdfGetFieldCount(FInstance);
end;

function TPDF.GetFieldEx(Handle: Cardinal; var Field: TPDFFieldEx): Boolean;
begin
   LoadFunction(@pdfGetFieldEx, 'pdfGetFieldEx');
   Field.StructSize := sizeof(Field);
   Result := pdfGetFieldEx(FInstance, Handle, Field);
end;

function TPDF.GetFieldEx2(const IField: Pointer; var Field: TPDFFieldEx): Boolean;
begin
   LoadFunction(@pdfGetFieldEx2, 'pdfGetFieldEx2');
   Field.StructSize := sizeof(Field);
   Result := pdfGetFieldEx2(IField, Field);
end;

function TPDF.GetFieldExpValCount(AField: Cardinal): Integer;
begin
   LoadFunction(@pdfGetFieldExpValCount, 'pdfGetFieldExpValCount');
   Result := pdfGetFieldExpValCount(FInstance, AField);
end;

function TPDF.GetFieldExpValue(AField: Cardinal; var Value: AnsiString): Integer;
var retval: PAnsiChar;
begin
   retval := nil;
   LoadFunction(@pdfGetFieldExpValue, 'pdfGetFieldExpValue');
   Result := pdfGetFieldExpValue(FInstance, AField, retval);
   Value := retval;
end;

function TPDF.GetFieldExpValueEx(AField, ValIndex: Cardinal; var Value, ExpValue: AnsiString; var Selected: Boolean): Boolean;
var val, expVal: PAnsiChar; sel: LongBool;
begin
   LoadFunction(@pdfGetFieldExpValueEx, 'pdfGetFieldExpValueEx');
   val      := nil;
   expVal   := nil;
   sel      := false;
   Result   := pdfGetFieldExpValueEx(FInstance, AField, ValIndex, val, expVal, sel);
   Value    := val;
   ExpValue := expVal;
   Selected := sel;
end;

function TPDF.GetFieldFlags(AField: Cardinal): Integer;
begin
   LoadFunction(@pdfGetFieldFlags, 'pdfGetFieldFlags');
   Result := pdfGetFieldFlags(FInstance, AField);
end;

function TPDF.GetFieldGroupType(AField: Cardinal): Integer;
begin
   LoadFunction(@pdfGetFieldGroupType, 'pdfGetFieldGroupType');
   Result := pdfGetFieldGroupType(FInstance, AField);
end;

function TPDF.GetFieldHighlightMode(AField: Cardinal): Integer;
begin
   LoadFunction(@pdfGetFieldHighlightMode, 'pdfGetFieldHighlightMode');
   Result := pdfGetFieldHighlightMode(FInstance, AField);
end;

function TPDF.GetFieldIndex(AField: Cardinal): Integer;
begin
   LoadFunction(@pdfGetFieldIndex, 'pdfGetFieldIndex');
   Result := pdfGetFieldIndex(FInstance, AField);
end;

function TPDF.GetFieldMapName(AField: Cardinal; var Value: Pointer; var Unicode: LongBool): Integer;
begin
   LoadFunction(@pdfGetFieldMapName, 'pdfGetFieldMapName');
   Result := pdfGetFieldMapName(FInstance, AField, Value, Unicode);
end;

function TPDF.GetFieldName(AField: Cardinal; var Name: AnsiString): Integer;
var retval: PAnsiChar; i, p, len: Integer;
begin
   retval := nil;
   LoadFunction(@pdfGetFieldName, 'pdfGetFieldName');
   Result := pdfGetFieldName(FInstance, AField, retval);
   if Result > 0 then begin
      len := StrLen(retval);
      if len <> Result then
         len := Result * 2
      else
         len := Result;
      SetLength(Name, len);
      {
         A field name can be an Unicode string. To determine whether we have an ANSI or an Unicode
         string proceed as follows:
         nameLen := pdf.GetFieldName(myField, name);
         if nameLen > 0 then begin
            if Length(name) <> nameLen then begin
               nameW := pdf.ToUnicode(name);
               ...
            end else begin
               ...
            end;            
         end;
      }
      p := 0;
      for i := 1 to len do begin
         Name[i] := retval[p];
         Inc(p);
      end;
   end else
      Name := '';
end;

function TPDF.GetFieldOrientation(AField: Cardinal): Integer;
begin
   LoadFunction(@pdfGetFieldOrientation, 'pdfGetFieldOrientation');
   Result := pdfGetFieldOrientation(FInstance, AField);
end;

function TPDF.GetFieldTextAlign(AField: Cardinal): Integer;
begin
   LoadFunction(@pdfGetFieldTextAlign, 'pdfGetFieldTextAlign');
   Result := pdfGetFieldTextAlign(FInstance, AField);
end;

function TPDF.GetFieldTextColor(): Cardinal;
begin
   LoadFunction(@pdfGetFieldTextColor, 'pdfGetFieldTextColor');
   Result := pdfGetFieldTextColor(FInstance);
end;

function TPDF.GetFieldToolTip(AField: Cardinal; var Value: Pointer; var Unicode: LongBool): Integer;
begin
   LoadFunction(@pdfGetFieldToolTip, 'pdfGetFieldToolTip');
   Result := pdfGetFieldToolTip(FInstance, AField, Value, Unicode);
end;

function TPDF.GetFieldType(AField: Cardinal): Integer;
begin
   LoadFunction(@pdfGetFieldType, 'pdfGetFieldType');
   Result := pdfGetFieldType(FInstance, AField);
end;

function TPDF.GetFillColor(): Cardinal;
begin
   LoadFunction(@pdfGetFillColor, 'pdfGetFillColor');
   Result := pdfGetFillColor(FInstance);
end;

function TPDF.GetFont(const IFont: Pointer; var F: TPDFFontObj): Boolean;
begin
   // if F.Length1 = 0 and if F.FontFile <> nil then the variable contains
   // the file path to the used font file. This string is always an Ansi string at this time.
   LoadFunction(@fntGetFont, 'fntGetFont');
   Result := fntGetFont(IFont, F);
end;

function TPDF.GetFontCount: Integer;
begin
   LoadFunction(@pdfGetFontCount, 'pdfGetFontCount');
   Result := pdfGetFontCount(FInstance);
end;

function TPDF.GetFontEx(Handle: Cardinal; var F: TPDFFontObj): Boolean;
begin
   // if F.Length1 = 0 and if F.FontFile <> nil then the variable contains
   // the file path to the used font file. This string is always an Ansi string at this time.
   LoadFunction(@pdfGetFontEx, 'pdfGetFontEx');
   Result := pdfGetFontEx(FInstance, Handle, F);
end;

function TPDF.GetFontInfo(const IFont: Pointer; var F: TPDFFontInfo): Boolean;
begin
   LoadFunction(@fntGetFontInfo, 'fntGetFontInfo');
   Result := fntGetFontInfo(IFont, F);
end;

function TPDF.GetFontInfoEx(Handle: Cardinal; var F: TPDFFontInfo): Boolean;
begin
   LoadFunction(@pdfGetFontInfoEx, 'pdfGetFontInfoEx');
   Result := pdfGetFontInfoEx(FInstance, Handle, F);
end;

function TPDF.GetFontOrigin(): Integer;
begin
   LoadFunction(@pdfGetFontOrigin, 'pdfGetFontOrigin');
   Result := pdfGetFontOrigin(FInstance);
end;

procedure TPDF.GetFontSearchOrder(Order: PFontBaseType);
begin
   LoadFunction(@pdfGetFontSearchOrder, 'pdfGetFontSearchOrder');
   pdfGetFontSearchOrder(FInstance, Order);
end;

function TPDF.GetFontSelMode(): Integer;
begin
   LoadFunction(@pdfGetFontSelMode, 'pdfGetFontSelMode');
   Result := pdfGetFontSelMode(FInstance);
end;

function TPDF.GetFontWeight(): Integer;
begin
   LoadFunction(@pdfGetFontWeight, 'pdfGetFontWeight');
   Result := pdfGetFontWeight(FInstance);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.GetFTextHeight(Align: TTextAlign; const AText: AnsiString): Double;
begin
   LoadFunction(@pdfGetFTextHeightA, 'pdfGetFTextHeightA');
   Result := pdfGetFTextHeightA(FInstance, Ord(Align), PAnsiChar(AText));
end;

function TPDF.GetFTextHeight(Align: TTextAlign; const AText: WideString): Double;
begin
   LoadFunction(@pdfGetFTextHeightW, 'pdfGetFTextHeightW');
   Result := pdfGetFTextHeightW(FInstance, Ord(Align), PWideChar(AText));
end;
{$endif}

function TPDF.GetFTextHeightA(Align: TTextAlign; const AText: AnsiString): Double;
begin
   LoadFunction(@pdfGetFTextHeightA, 'pdfGetFTextHeightA');
   Result := pdfGetFTextHeightA(FInstance, Ord(Align), PAnsiChar(AText));
end;

function TPDF.GetFTextHeightW(Align: TTextAlign; const AText: WideString): Double;
begin
   LoadFunction(@pdfGetFTextHeightW, 'pdfGetFTextHeightW');
   Result := pdfGetFTextHeightW(FInstance, Ord(Align), PWideChar(AText));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.GetFTextHeightEx(Width: Double; Align: TTextAlign; const AText: AnsiString): Double;
begin
   LoadFunction(@pdfGetFTextHeightExA, 'pdfGetFTextHeightExA');
   Result := pdfGetFTextHeightExA(FInstance, Width, Ord(Align), PAnsiChar(AText));
end;

function TPDF.GetFTextHeightEx(Width: Double; Align: TTextAlign; const AText: WideString): Double;
begin
   LoadFunction(@pdfGetFTextHeightExW, 'pdfGetFTextHeightExW');
   Result := pdfGetFTextHeightExW(FInstance, Width, Ord(Align), PWideChar(AText));
end;
{$endif}

function TPDF.GetFTextHeightExA(Width: Double; Align: TTextAlign; const AText: AnsiString): Double;
begin
   LoadFunction(@pdfGetFTextHeightExA, 'pdfGetFTextHeightExA');
   Result := pdfGetFTextHeightExA(FInstance, Width, Ord(Align), PAnsiChar(AText));
end;

function TPDF.GetFTextHeightExW(Width: Double; Align: TTextAlign; const AText: WideString): Double;
begin
   LoadFunction(@pdfGetFTextHeightExW, 'pdfGetFTextHeightExW');
   Result := pdfGetFTextHeightExW(FInstance, Width, Ord(Align), PWideChar(AText));
end;

function TPDF.GetGStateFlags: Integer;
begin
   LoadFunction(@pdfGetGStateFlags, 'pdfGetGStateFlags');
   Result := pdfGetGStateFlags(FInstance);
end;

function TPDF.GetIconColor(): Integer;
begin
   LoadFunction(@pdfGetIconColor, 'pdfGetIconColor');
   Result := pdfGetIconColor(FInstance);
end;

function TPDF.GetImageBuffer(var BufSize: Cardinal): PAnsiChar;
begin
   LoadFunction(@pdfGetImageBuffer, 'pdfGetImageBuffer');
   Result := pdfGetImageBuffer(FInstance, BufSize);
end;

function TPDF.GetImageCount(const FileName: AnsiString): Integer;
begin
   LoadFunction(@pdfGetImageCountA, 'pdfGetImageCountA');
   Result := pdfGetImageCountA(FInstance, PAnsiChar(FileName));
end;
{$ifdef DELPHI6_OR_HIGHER}
function TPDF.GetImageCount(const FileName: WideString): Integer;
begin
   LoadFunction(@pdfGetImageCountW, 'pdfGetImageCountW');
   Result := pdfGetImageCountW(FInstance, PWideChar(FileName));
end;
{$endif}
function TPDF.GetImageCountW(const FileName: WideString): Integer;
begin
   LoadFunction(@pdfGetImageCountW, 'pdfGetImageCountW');
   Result := pdfGetImageCountW(FInstance, PWideChar(FileName));
end;

function TPDF.GetImageCountEx(const Buffer: Pointer; BufSize: Cardinal): Integer;
begin
   LoadFunction(@pdfGetImageCountEx, 'pdfGetImageCountEx');
   Result := pdfGetImageCountEx(FInstance, Buffer, BufSize);
end;

function TPDF.GetImageHeight(Handle: Cardinal): Integer;
begin
   LoadFunction(@pdfGetImageHeight, 'pdfGetImageHeight');
   Result := pdfGetImageHeight(FInstance, Handle);
end;

function TPDF.GetImageObj(Handle: Cardinal; Flags: TParseFlags; var Image: TPDFImage): Boolean;
begin
   LoadFunction(@pdfGetImageObj, 'pdfGetImageObj');
   Result := pdfGetImageObj(FInstance, Handle, Flags, Image);
end;

function TPDF.GetImageObjCount: Integer;
begin
   LoadFunction(@pdfGetImageObjCount, 'pdfGetImageObjCount');
   Result := pdfGetImageObjCount(FInstance);
end;

function TPDF.GetImageObjEx(const ImagePtr: Pointer; Flags: TParseFlags; var Image: TPDFImage): Boolean;
begin
   LoadFunction(@pdfGetImageObjEx, 'pdfGetImageObjEx');
   Result := pdfGetImageObjEx(FInstance, ImagePtr, Flags, Image);
end;

function TPDF.GetImageWidth(Handle: Cardinal): Integer;
begin
   LoadFunction(@pdfGetImageWidth, 'pdfGetImageWidth');
   Result := pdfGetImageWidth(FInstance, Handle);
end;

function TPDF.GetImportFlags(): Cardinal;
begin
   LoadFunction(@pdfGetImportFlags, 'pdfGetImportFlags');
   Result := pdfGetImportFlags(FInstance);
end;

function TPDF.GetInBBox(PageNum: Cardinal; Boundary: TPageBoundary; var BBox: TPDFRect): Boolean;
begin
   LoadFunction(@pdfGetInBBox, 'pdfGetInBBox');
   Result := pdfGetInBBox(FInstance, PageNum, Ord(Boundary), BBox);
end;

function TPDF.GetInDocInfo(DInfo: TDocumentInfo; var Value: PWideChar): Integer;
begin
   LoadFunction(@pdfGetInDocInfo, 'pdfGetInDocInfo');
   Result := pdfGetInDocInfo(FInstance, Ord(DInfo), Value);
end;

function TPDF.GetInDocInfoCount: Integer;
begin
   LoadFunction(@pdfGetInDocInfoCount, 'pdfGetInDocInfoCount');
   Result := pdfGetInDocInfoCount(FInstance);
end;

function TPDF.GetInDocInfoEx(Index: Cardinal; var DInfo: TDocumentInfo; var Key, Value: PAnsiChar; var Unicode: LongBool): Boolean;
begin
   LoadFunction(@pdfGetInDocInfoEx, 'pdfGetInDocInfoEx');
   Result := pdfGetInDocInfoEx(FInstance, Index, Integer(DInfo), Key, Value, Unicode);
end;

function TPDF.GetInEncryptionFlags: Integer;
begin
   LoadFunction(@pdfGetInEncryptionFlags, 'pdfGetInEncryptionFlags');
   Result := pdfGetInEncryptionFlags(FInstance);
end;

function TPDF.GetInFieldCount: Integer;
begin
   LoadFunction(@pdfGetInFieldCount, 'pdfGetInFieldCount');
   Result := pdfGetInFieldCount(FInstance);
end;

function TPDF.GetInIsCollection: Boolean;
var retval: Integer;
begin
   LoadFunction(@pdfGetInIsCollection, 'pdfGetInIsCollection');
   retval := pdfGetInIsCollection(FInstance);
   if retval < 0 then
      Result := false
   else
      Result := retval <> 0;
end;

function TPDF.GetInIsEncrypted: Boolean;
var retval: Integer;
begin
   LoadFunction(@pdfGetInIsEncrypted, 'pdfGetInIsEncrypted');
   retval := pdfGetInIsEncrypted(FInstance);
   if retval < 0 then
      Result := false
   else
      Result := retval <> 0;
end;

function TPDF.GetInIsSigned: Boolean;
var retval: Integer;
begin
   LoadFunction(@pdfGetInIsSigned, 'pdfGetInIsSigned');
   retval := pdfGetInIsSigned(FInstance);
   if retval < 0 then
      Result := false
   else
      Result := retval <> 0;
end;

function TPDF.GetInIsTrapped: Boolean;
var retval: Integer;
begin
   LoadFunction(@pdfGetInIsTrapped, 'pdfGetInIsTrapped');
   retval := pdfGetInIsTrapped(FInstance);
   if retval < 0 then
      Result := false
   else
      Result := retval <> 0;
end;

function TPDF.GetInIsXFAForm: Boolean;
var retval: Integer;
begin
   LoadFunction(@pdfGetInIsXFAForm, 'pdfGetInIsXFAForm');
   retval := pdfGetInIsXFAForm(FInstance);
   if retval < 0 then
      Result := false
   else
      Result := retval <> 0;
end;

function TPDF.GetInMetadata(PageNum: Integer; var Buffer: PByte; var BufSize: Cardinal): Boolean;
begin
   LoadFunction(@pdfGetInMetadata, 'pdfGetInMetadata');
   Result := pdfGetInMetadata(FInstance, PageNum, Buffer, BufSize);
end;

function TPDF.GetInOrientation(PageNum: Integer): Integer;
begin
   LoadFunction(@pdfGetInOrientation, 'pdfGetInOrientation');
   Result := pdfGetInOrientation(FInstance, PageNum);
end;

function TPDF.GetInPageCount(): Integer;
begin
   LoadFunction(@pdfGetInPageCount, 'pdfGetInPageCount');
   Result := pdfGetInPageCount(FInstance);
end;

function TPDF.GetInPDFVersion(): Integer;
begin
   LoadFunction(@pdfGetInPDFVersion, 'pdfGetInPDFVersion');
   Result := pdfGetInPDFVersion(FInstance);
end;

function TPDF.GetInPrintSettings(var Settings: TPDFPrintSettings): Boolean;
begin
   LoadFunction(@pdfGetInPrintSettings, 'pdfGetInPrintSettings');
   Result := pdfGetInPrintSettings(FInstance, Settings);
end;

function TPDF.GetInRepairMode: Boolean;
begin
   LoadFunction(@pdfGetInRepairMode, 'pdfGetInRepairMode');
   Result := pdfGetInRepairMode(FInstance);
end;

function TPDF.GetIsFixedPitch(): Integer;
begin
   LoadFunction(@pdfGetIsFixedPitch, 'pdfGetIsFixedPitch');
   Result := pdfGetIsFixedPitch(FInstance);
end;

function TPDF.GetIsTaggingEnabled: Boolean;
begin
   LoadFunction(@pdfGetIsTaggingEnabled, 'pdfGetIsTaggingEnabled');
   Result := pdfGetIsTaggingEnabled(FInstance);
end;

function TPDF.GetItalicAngle(): Double;
begin
   LoadFunction(@pdfGetItalicAngle, 'pdfGetItalicAngle');
   Result := pdfGetItalicAngle(FInstance);
end; 

function TPDF.GetJavaScript(Handle: Cardinal; var Len: Cardinal; var Unicode: LongBool): PAnsiChar;
begin
   LoadFunction(@pdfGetJavaScript, 'pdfGetJavaScript');
   Result := pdfGetJavaScript(FInstance, Handle, Len, Unicode);
end;

function TPDF.GetJavaScriptAction(Handle: Cardinal; var Len: Cardinal; var Unicode: LongBool): PAnsiChar;
begin
   LoadFunction(@pdfGetJavaScriptAction, 'pdfGetJavaScriptAction');
   Result := pdfGetJavaScriptAction(FInstance, Handle, Len, Unicode);
end;

function TPDF.GetJavaScriptAction2(ObjType: TObjType; ObjHandle, ActIndex: Cardinal; var Len: Cardinal; var Unicode: LongBool; var Event: TObjEvent): PAnsiChar;
begin
   LoadFunction(@pdfGetJavaScriptAction2, 'pdfGetJavaScriptAction2');
   Result := pdfGetJavaScriptAction2(FInstance, ObjType, ObjHandle, ActIndex, Len, Unicode, Event);
end;

function TPDF.GetJavaScriptCount(): Integer;
begin
   LoadFunction(@pdfGetJavaScriptCount, 'pdfGetJavaScriptCount');
   Result := pdfGetJavaScriptCount(FInstance);
end;

function TPDF.GetJavaScriptEx(const Name: AnsiString; var Len: Cardinal; var Unicode: LongBool): PAnsiChar;
begin
   LoadFunction(@pdfGetJavaScriptEx, 'pdfGetJavaScriptEx');
   Result := pdfGetJavaScriptEx(FInstance, PAnsiChar(Name), Len, Unicode);
end;

function TPDF.GetJavaScriptName(Handle: Cardinal; var Len: Cardinal; var Unicode: LongBool): PAnsiChar;
begin
   LoadFunction(@pdfGetJavaScriptName, 'pdfGetJavaScriptName');
   Result := pdfGetJavaScriptName(FInstance, Handle, Len, Unicode);
end;

function TPDF.GetJPEGQuality: Integer;
begin
   LoadFunction(@pdfGetJPEGQuality, 'pdfGetJPEGQuality');
   Result := pdfGetJPEGQuality(FInstance);
end;

function TPDF.GetLanguage(): AnsiString;
begin
   LoadFunction(@pdfGetLanguage, 'pdfGetLanguage');
   Result := pdfGetLanguage(FInstance);
end;

function TPDF.GetLastTextPosX(): Double;
begin
   LoadFunction(@pdfGetLastTextPosX, 'pdfGetLastTextPosX');
   Result := pdfGetLastTextPosX(FInstance);
end;

function TPDF.GetLastTextPosY(): Double;
begin
   LoadFunction(@pdfGetLastTextPosY, 'pdfGetLastTextPosY');
   Result := pdfGetLastTextPosY(FInstance);
end;

function TPDF.GetLeading(): Double;
begin
   LoadFunction(@pdfGetLeading, 'pdfGetLeading');
   Result := pdfGetLeading(FInstance);
end;

function TPDF.GetLibHandle: HMODULE;
begin
   Result := FDLL_Handle;
end;

function TPDF.GetLineCapStyle(): Integer;
begin
   LoadFunction(@pdfGetLineCapStyle, 'pdfGetLineCapStyle');
   Result := pdfGetLineCapStyle(FInstance);
end;

function TPDF.GetLineJoinStyle(): Integer;
begin
   LoadFunction(@pdfGetLineJoinStyle, 'pdfGetLineJoinStyle');
   Result := pdfGetLineJoinStyle(FInstance);
end;

function TPDF.GetLineWidth(): Double;
begin
   LoadFunction(@pdfGetLineWidth, 'pdfGetLineWidth');
   Result := pdfGetLineWidth(FInstance);
end;

function TPDF.GetLinkHighlightMode(): Integer;
begin
   LoadFunction(@pdfGetLinkHighlightMode, 'pdfGetLinkHighlightMode');
   Result := pdfGetLinkHighlightMode(FInstance);
end;

function TPDF.GetLogMetafileSize(const FileName: AnsiString; var R: TRectL): Boolean;
begin
   LoadFunction(@pdfGetLogMetafileSizeA, 'pdfGetLogMetafileSizeA');
   Result := pdfGetLogMetafileSizeA(FInstance, PAnsiChar(FileName), R);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.GetLogMetafileSize(const FileName: WideString; var R: TRectL): Boolean;
begin
   LoadFunction(@pdfGetLogMetafileSizeW, 'pdfGetLogMetafileSizeW');
   Result := pdfGetLogMetafileSizeW(FInstance, PWideChar(FileName), R);
end;
{$endif}

function TPDF.GetLogMetafileSizeA(const FileName: AnsiString; var R: TRectL): Boolean;
begin
   LoadFunction(@pdfGetLogMetafileSizeA, 'pdfGetLogMetafileSizeA');
   Result := pdfGetLogMetafileSizeA(FInstance, PAnsiChar(FileName), R);
end;

function TPDF.GetLogMetafileSizeW(const FileName: WideString; var R: TRectL): Boolean;
begin
   LoadFunction(@pdfGetLogMetafileSizeW, 'pdfGetLogMetafileSizeW');
   Result := pdfGetLogMetafileSizeW(FInstance, PWideChar(FileName), R);
end;

function TPDF.GetLogMetafileSizeEx(const Buffer: Pointer; BufSize: Cardinal; var R: TRectL): Boolean;
begin
   LoadFunction(@pdfGetLogMetafileSizeEx, 'pdfGetLogMetafileSizeEx');
   Result := pdfGetLogMetafileSizeEx(FInstance, Buffer, BufSize, R);
end;

function TPDF.GetMatrix(var M: TCTM): Boolean;
begin
   LoadFunction(@pdfGetMatrix, 'pdfGetMatrix');
   Result := pdfGetMatrix(FInstance, M);
end;

function TPDF.GetMaxFieldLen(TxtField: Cardinal): Integer;
begin
   LoadFunction(@pdfGetMaxFieldLen, 'pdfGetMaxFieldLen');
   Result := pdfGetMaxFieldLen(FInstance, TxtField);
end;

function TPDF.GetMeasureObj(const MeasurePtr: IMSR; var Value: TPDFMeasure): Boolean;
begin
   LoadFunction(@pdfGetMeasureObj, 'pdfGetMeasureObj');
   Result := pdfGetMeasureObj(MeasurePtr, Value);
end;

function TPDF.GetMetaConvFlags(): Cardinal;
begin
   LoadFunction(@pdfGetMetaConvFlags, 'pdfGetMetaConvFlags');
   Result := pdfGetMetaConvFlags(FInstance);
end;

function TPDF.GetMetadata(ObjType: TMetadataObj; Handle: Integer; var Buffer: PByte; var BufSize: Cardinal): Boolean;
begin
   LoadFunction(@pdfGetMetadata, 'pdfGetMetadata');
   Result := pdfGetMetadata(FInstance, ObjType, Handle, Buffer, BufSize);
end;

function TPDF.GetMissingGlyphs(var Count: Cardinal): PCardinal;
begin
   LoadFunction(@pdfGetMissingGlyphs, 'pdfGetMissingGlyphs');
   Result := pdfGetMissingGlyphs(FInstance, Count);
end;

function TPDF.GetMiterLimit(): Double;
begin
   LoadFunction(@pdfGetMiterLimit, 'pdfGetMiterLimit');
   Result := pdfGetMiterLimit(FInstance);
end;

function TPDF.GetNamedDest(Index: Cardinal; var Dest: TPDFNamedDest): Boolean;
begin
   LoadFunction(@pdfGetNamedDest, 'pdfGetNamedDest');
   Dest.StructSize := sizeof(TPDFNamedDest);
   Result := pdfGetNamedDest(FInstance, Index, Dest);
end;

function TPDF.GetNamedDestCount: Integer;
begin
   LoadFunction(@pdfGetNamedDestCount, 'pdfGetNamedDestCount');
   Result := pdfGetNamedDestCount(FInstance);
end;

function TPDF.GetNeedAppearance: Boolean;
begin
   LoadFunction(@pdfGetNeedAppearance, 'pdfGetNeedAppearance');
   Result := pdfGetNeedAppearance(FInstance);
end;

function TPDF.GetNumberFormatObj(const NumberFmtPtr: INFM; var Value: TPDFNumberFormat): Boolean;
begin
   LoadFunction(@pdfGetNumberFormatObj, 'pdfGetNumberFormatObj');
   Result := pdfGetNumberFormatObj(NumberFmtPtr, Value);
end;

function TPDF.GetObjActionCount(ObjType: TObjType; ObjHandle: Cardinal): Integer;
begin
   LoadFunction(@pdfGetObjActionCount, 'pdfGetObjActionCount');
   Result := pdfGetObjActionCount(FInstance, Ord(ObjType), ObjHandle);
end;

function TPDF.GetOCG(Handle: Cardinal; var Value: TPDFOCG): Boolean;
begin
   LoadFunction(@pdfGetOCG, 'pdfGetOCG');
   Result := pdfGetOCG(FInstance, Handle, Value);
end;

function TPDF.GetOCGContUsage(Handle: Cardinal; var Value: TPDFOCGContUsage): Boolean;
begin
   LoadFunction(@pdfGetOCGContUsage, 'pdfGetOCGContUsage');
   Result := pdfGetOCGContUsage(FInstance, Handle, Value);
end;

function TPDF.GetOCGCount: Integer;
begin
   LoadFunction(@pdfGetOCGCount, 'pdfGetOCGCount');
   Result := pdfGetOCGCount(FInstance);
end;

function TPDF.GetOCGUsageUserName(Handle, Index: Cardinal; var NameA: PAnsiChar; var NameW: PWideChar): Boolean;
begin
   LoadFunction(@pdfGetOCGUsageUserName, 'pdfGetOCGUsageUserName');
   Result := pdfGetOCGUsageUserName(FInstance, Handle, Index, NameA, NameW);
end;

function TPDF.GetOpacity(): Double;
begin
   LoadFunction(@pdfGetOpacity, 'pdfGetOpacity');
   Result := pdfGetOpacity(FInstance);
end;

function TPDF.GetOrientation(): Integer;
begin
   LoadFunction(@pdfGetOrientation, 'pdfGetOrientation');
   Result := pdfGetOrientation(FInstance);
end;

function TPDF.GetOutputIntent(Index: Cardinal; var Intent: TPDFOutputIntent): Boolean;
begin
   LoadFunction(@pdfGetOutputIntent, 'pdfGetOutputIntent');
   Result := pdfGetOutputIntent(FInstance, Index, Intent);
end;

function TPDF.GetOutputIntentCount: Integer;
begin
   LoadFunction(@pdfGetOutputIntentCount, 'pdfGetOutputIntentCount');
   Result := pdfGetOutputIntentCount(FInstance);
end;

function TPDF.GetPageAnnot(Index: Cardinal; var Annot: TPDFAnnotation): Boolean;
begin
   LoadFunction(@pdfGetPageAnnot, 'pdfGetPageAnnot');
   Result := pdfGetPageAnnot(FInstance, Index, Annot);
end;

function TPDF.GetPageAnnotCount: Integer;
begin
   LoadFunction(@pdfGetPageAnnotCount, 'pdfGetPageAnnotCount');
   Result := pdfGetPageAnnotCount(FInstance);
end;

function TPDF.GetPageAnnotEx(Index: Cardinal; var Annot: TPDFAnnotationEx): Boolean;
begin
   LoadFunction(@pdfGetPageAnnotEx, 'pdfGetPageAnnotEx');
   Result := pdfGetPageAnnotEx(FInstance, Index, Annot);
end;

function TPDF.GetPageBBox(PagePtr: Pointer; Boundary: TPageBoundary; var BBox: TFltRect): Boolean;
begin
   LoadFunction(@pdfGetPageBBox, 'pdfGetPageBBox');
   Result := pdfGetPageBBox(PagePtr, Boundary, BBox);
end;

function TPDF.GetPageCoords(): TPageCoord;
begin
   LoadFunction(@pdfGetPageCoords, 'pdfGetPageCoords');
   Result := TPageCoord(pdfGetPageCoords(FInstance));
end;

function TPDF.GetPageCount(): Integer;
begin
   LoadFunction(@pdfGetPageCount, 'pdfGetPageCount');
   Result := pdfGetPageCount(FInstance);
end;

function TPDF.GetPageField(Index: Cardinal; var Field: TPDFField): Boolean;
begin
   {
      Note: FieldName can be an Unicode string!
      To determine whether FieldName contains an ANSI or Unicode string
      proceed as follows:
      if f.FielNameLen > 0 and StrLen(f.FieldName) <> f.FieldNameLen then begin
         // f.FieldName contains a null-terminated Unicode string
         // f.FieldNameLen is the length in Unicode characters, but we have an ANSI string!
         // So, we must multiply the length with 2 to convert the entire string!
         nameW := pdf.ToUnicode(f.FieldName, f.FieldNameLen * 2);         
      end;
   }
   LoadFunction(@pdfGetPageField, 'pdfGetPageField');
   Result := pdfGetPageField(FInstance, Index, Field);
end;

function TPDF.GetPageFieldCount(): Integer;
begin
   LoadFunction(@pdfGetPageFieldCount, 'pdfGetPageFieldCount');
   Result := pdfGetPageFieldCount(FInstance);
end;

function TPDF.GetPageFieldEx(Index: Cardinal; var Field: TPDFFieldEx): Boolean;
begin
   LoadFunction(@pdfGetPageFieldEx, 'pdfGetPageFieldEx');
   Field.StructSize := sizeof(Field);
   Result := pdfGetPageFieldEx(FInstance, Index, Field);
end;

function TPDF.GetPageHeight(): Double;
begin
   LoadFunction(@pdfGetPageHeight, 'pdfGetPageHeight');
   Result := pdfGetPageHeight(FInstance);
end;

function TPDF.GetPageLabel(Index: Cardinal; var Lbl: TPDFPageLabel): Boolean;
begin
   LoadFunction(@pdfGetPageLabel, 'pdfGetPageLabel');
   Result := pdfGetPageLabel(FInstance, Index, Lbl);
end;

function TPDF.GetPageLabelCount: Integer;
begin
   LoadFunction(@pdfGetPageLabelCount, 'pdfGetPageLabelCount');
   Result := pdfGetPageLabelCount(FInstance);
end;

function TPDF.GetPageLayout(): Integer;
begin
   LoadFunction(@pdfGetPageLayout, 'pdfGetPageLayout');
   Result := pdfGetPageLayout(FInstance);
end;

function TPDF.GetPageMode(): Integer;
begin
   LoadFunction(@pdfGetPageMode, 'pdfGetPageMode');
   Result := pdfGetPageMode(FInstance);
end;

function TPDF.GetPageNum: Integer;
begin
   LoadFunction(@pdfGetPageNum, 'pdfGetPageNum');
   Result := pdfGetPageNum(FInstance);
end;

function TPDF.GetPageObject(PageNum: Cardinal): Pointer;
begin
   LoadFunction(@pdfGetPageObject, 'pdfGetPageObject');
   Result := pdfGetPageObject(FInstance, PageNum);
end;

function TPDF.GetPageOrientation(PagePtr: Pointer): Integer;
begin
   LoadFunction(@pdfGetPageOrientation, 'pdfGetPageOrientation');
   Result := pdfGetPageOrientation(PagePtr);
end;

function TPDF.GetPageText(var Stack: TPDFStack): Boolean;
begin
   LoadFunction(@pdfGetPageText, 'pdfGetPageText');
   Result := pdfGetPageText(FInstance, Stack);
end;

function TPDF.GetPageWidth(): Double;
begin
   LoadFunction(@pdfGetPageWidth, 'pdfGetPageWidth');
   Result := pdfGetPageWidth(FInstance);
end;

function TPDF.GetPDFVersion(): Integer;
begin
   LoadFunction(@pdfGetPDFVersion, 'pdfGetPDFVersion');
   Result := pdfGetPDFVersion(FInstance);
end;

function TPDF.GetPrintSettings(var Settings: TPDFPrintSettings): Boolean;
begin
   LoadFunction(@pdfGetPrintSettings, 'pdfGetPrintSettings');
   Result := pdfGetPrintSettings(FInstance, Settings);
end;

function TPDF.GetPtDataArray(const PtData: IPTD; Index: Cardinal; var DataType: PAnsiChar; var Values: PSingle; var ValCount: Cardinal): Boolean;
begin
   LoadFunction(@pdfGetPtDataArray, 'pdfGetPtDataArray');
   Result := pdfGetPtDataArray(PtData, Index, DataType, Values, ValCount);
end;

function TPDF.GetPtDataObj(const PtData: IPTD; var Subtype: PAnsiChar; var NumArrays: Cardinal): Boolean;
begin
   LoadFunction(@pdfGetPtDataObj, 'pdfGetPtDataObj');
   Result := pdfGetPtDataObj(PtData, Subtype, NumArrays);
end;

function TPDF.GetResolution(): Integer;
begin
   LoadFunction(@pdfGetResolution, 'pdfGetResolution');
   Result := pdfGetResolution(FInstance);
end;

function TPDF.GetSaveNewImageFormat(): Integer;
begin
   LoadFunction(@pdfGetSaveNewImageFormat, 'pdfGetSaveNewImageFormat');
   Result := pdfGetSaveNewImageFormat(FInstance);
end;

function TPDF.GetSeparationInfo(var Colorant: AnsiString; var CS: TExtColorSpace): Boolean;
var clt: PAnsiChar;
begin
   LoadFunction(@pdfGetSeparationInfo, 'pdfGetSeparationInfo');
   clt := nil;
   Result := pdfGetSeparationInfo(FInstance, clt, CS);
   Colorant := clt;
end;

function TPDF.GetSigDict(const ISignature: Pointer; var SigDict: TPDFSigDict): Boolean;
begin
   LoadFunction(@pdfGetSigDict, 'pdfGetSigDict');
   SigDict.StructSize := sizeof(TPDFSigDict);
   Result := pdfGetSigDict(ISignature, SigDict);
end;

function TPDF.GetSpaceWidth(const IFont: Pointer; FontSize: Double): Double;
begin
   LoadFunction(@fntGetSpaceWidth, 'fntGetSpaceWidth');
   Result := fntGetSpaceWidth(IFont, FontSize);
end;

function TPDF.GetStrokeColor(): Cardinal;
begin
   LoadFunction(@pdfGetStrokeColor, 'pdfGetStrokeColor');
   Result := pdfGetStrokeColor(FInstance);
end;

function TPDF.GetSysFontInfo(Handle: Cardinal; var Value: TPDFSysFont): Integer;
begin
   LoadFunction(@pdfGetSysFontInfo, 'pdfGetSysFontInfo');
   Result := pdfGetSysFontInfo(FInstance, Handle, Value);
end;

function TPDF.GetTabLen: Integer;
begin
   LoadFunction(@pdfGetTabLen, 'pdfGetTabLen');
   Result := pdfGetTabLen(FInstance);
end;

function TPDF.GetTemplCount: Integer;
begin
   LoadFunction(@pdfGetTemplCount, 'pdfGetTemplCount');
   Result := pdfGetTemplCount(FInstance);
end;

function TPDF.GetTemplHandle: Integer;
begin
   LoadFunction(@pdfGetTemplHandle, 'pdfGetTemplHandle');
   Result := pdfGetTemplHandle(FInstance);
end;

function TPDF.GetTemplHeight(TmplHandle: Integer): Double;
begin
   LoadFunction(@pdfGetTemplHeight, 'pdfGetTemplHeight');
   Result := pdfGetTemplHeight(FInstance, TmplHandle);
end;

function TPDF.GetTemplWidth(TmplHandle: Integer): Double;
begin
   LoadFunction(@pdfGetTemplWidth, 'pdfGetTemplWidth');
   Result := pdfGetTemplWidth(FInstance, TmplHandle);
end;

function TPDF.GetTextDrawMode(): Integer;
begin
   LoadFunction(@pdfGetTextDrawMode, 'pdfGetTextDrawMode');
   Result := pdfGetTextDrawMode(FInstance);
end;

function TPDF.GetTextFieldValue(AField: Cardinal; var Value: PAnsiChar; var ValIsUnicode: LongBool; var DefValue: PAnsiChar; var DefValIsUnicode: LongBool): Boolean;
begin
   LoadFunction(@pdfGetTextFieldValue, 'pdfGetTextFieldValue');
   Result := pdfGetTextFieldValue(FInstance, AField, Value, ValIsUnicode, DefValue, DefValIsUnicode);
end;

function TPDF.GetTextRect(var PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfGetTextRect, 'pdfGetTextRect');
   Result := pdfGetTextRect(FInstance, PosX, PosY, Width, Height);
end;

function TPDF.GetTextRise(): Double;
begin
   LoadFunction(@pdfGetTextRise, 'pdfGetTextRise');
   Result := pdfGetTextRise(FInstance);
end;

function TPDF.GetTextScaling(): Double;
begin
   LoadFunction(@pdfGetTextScaling, 'pdfGetTextScaling');
   Result := pdfGetTextScaling(FInstance);
end;

function TPDF.GetTextWidth(const IFont: Pointer; const Text: PAnsiChar; Len: Cardinal; CharSpacing, WordSpacing, TextScale: Single): Double;
begin
   LoadFunction(@fntGetTextWidth, 'fntGetTextWidth');
   Result := fntGetTextWidth(IFont, Text, Len, CharSpacing, WordSpacing, TextScale);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.GetTextWidth(const AText: AnsiString): Double;
begin
   LoadFunction(@pdfGetTextWidthA, 'pdfGetTextWidthA');
   Result := pdfGetTextWidthA(FInstance, PAnsiChar(AText));
end;

function TPDF.GetTextWidth(const AText: WideString): Double;
begin
   LoadFunction(@pdfGetTextWidthW, 'pdfGetTextWidthW');
   Result := pdfGetTextWidthW(FInstance, PWideChar(AText));
end;

function TPDF.GetTextWidthEx(const AText: AnsiString; Len: Cardinal): Double;
begin
   LoadFunction(@pdfGetTextWidthExA, 'pdfGetTextWidthExA');
   Result := pdfGetTextWidthExA(FInstance, PAnsiChar(AText), Len);
end;

function TPDF.GetTextWidthEx(const AText: WideString; Len: Cardinal): Double;
begin
   LoadFunction(@pdfGetTextWidthExW, 'pdfGetTextWidthExW');
   Result := pdfGetTextWidthExW(FInstance, PWideChar(AText), Len);
end;
{$endif}

function TPDF.GetTextWidthA(const AText: AnsiString): Double;
begin
   LoadFunction(@pdfGetTextWidthA, 'pdfGetTextWidthA');
   Result := pdfGetTextWidthA(FInstance, PAnsiChar(AText));
end;

function TPDF.GetTextWidthW(const AText: WideString): Double;
begin
   LoadFunction(@pdfGetTextWidthW, 'pdfGetTextWidthW');
   Result := pdfGetTextWidthW(FInstance, PWideChar(AText));
end;

function TPDF.GetTextWidthExA(const AText: AnsiString; Len: Cardinal): Double;
begin
   LoadFunction(@pdfGetTextWidthExA, 'pdfGetTextWidthExA');
   Result := pdfGetTextWidthExA(FInstance, PAnsiChar(AText), Len);
end;

function TPDF.GetTextWidthExW(const AText: WideString; Len: Cardinal): Double;
begin
   LoadFunction(@pdfGetTextWidthExW, 'pdfGetTextWidthExW');
   Result := pdfGetTextWidthExW(FInstance, PWideChar(AText), Len);
end;

function TPDF.GetTransparentColor(): Cardinal;
begin
   LoadFunction(@pdfGetTransparentColor, 'pdfGetTransparentColor');
   Result := pdfGetTransparentColor(FInstance);
end;

function TPDF.GetTrapped: Boolean;
begin
   LoadFunction(@pdfGetTrapped, 'pdfGetTrapped');
   Result := pdfGetTrapped(FInstance);
end;

function TPDF.GetUseTransparency(): Boolean;
begin
   LoadFunction(@pdfGetUseTransparency, 'pdfGetUseTransparency');
   Result := pdfGetUseTransparency(FInstance);
end;

function TPDF.GetUseVisibleCoords(): Boolean;
begin
   LoadFunction(@pdfGetUseVisibleCoords, 'pdfGetUseVisibleCoords');
   Result := pdfGetUseVisibleCoords(FInstance);
end;

function TPDF.GetUseExactPwd(): Boolean;
begin
   LoadFunction(@pdfGetUseExactPwd, 'pdfGetUseExactPwd');
   Result := pdfGetUseExactPwd(FInstance);
end;

function TPDF.GetUseGlobalImpFiles(): Boolean;
begin
   LoadFunction(@pdfGetUseGlobalImpFiles, 'pdfGetUseGlobalImpFiles');
   Result := pdfGetUseGlobalImpFiles(FInstance);
end;

function TPDF.GetUserRights(): Integer;
begin
   LoadFunction(@pdfGetUserRights, 'pdfGetUserRights');
   Result := pdfGetUserRights(FInstance);
end;

function TPDF.GetUserUnit: Single;
begin
   LoadFunction(@pdfGetUserUnit, 'pdfGetUserUnit');
   Result := pdfGetUserUnit(FInstance);
end;

function TPDF.GetUseStdFonts(): Boolean;
begin
   LoadFunction(@pdfGetUseStdFonts, 'pdfGetUseStdFonts');
   Result := pdfGetUseStdFonts(FInstance);
end;

function TPDF.GetUseSystemFonts(): Boolean;
begin
   LoadFunction(@pdfGetUseSystemFonts, 'pdfGetUseSystemFonts');
   Result := pdfGetUseSystemFonts(FInstance);
end;

function TPDF.GetViewerPrefrences(var Preference, AddVal: Integer): Boolean;
begin
   LoadFunction(@pdfGetViewerPrefrences, 'pdfGetViewerPrefrences');
   Result := pdfGetViewerPrefrences(FInstance, Preference, AddVal);
end;

function TPDF.GetViewport(PageNum, Index: Cardinal; var VP: TPDFViewport): Boolean;
begin
   LoadFunction(@pdfGetViewport, 'pdfGetViewport');
   Result := pdfGetViewport(FInstance, PageNum, Index, VP);
end;

function TPDF.GetViewportCount(PageNum: Cardinal): Integer;
begin
   LoadFunction(@pdfGetViewportCount, 'pdfGetViewportCount');
   Result := pdfGetViewportCount(FInstance, PageNum);
end;

function TPDF.GetWMFDefExtent(var Width, Height: Cardinal): Boolean;
begin
   LoadFunction(@pdfGetWMFDefExtent, 'pdfGetWMFDefExtent');
   Result := pdfGetWMFDefExtent(FInstance, Width, Height);
end;

function TPDF.GetWMFPixelPerInch(): Integer;
begin
   LoadFunction(@pdfGetWMFPixelPerInch, 'pdfGetWMFPixelPerInch');
   Result := pdfGetWMFPixelPerInch(FInstance);
end;

function TPDF.GetWordSpacing(): Double;
begin
   LoadFunction(@pdfGetWordSpacing, 'pdfGetWordSpacing');
   Result := pdfGetWordSpacing(FInstance);
end;

function TPDF.HaveOpenDoc: Boolean;
begin
   LoadFunction(@pdfHaveOpenDoc, 'pdfHaveOpenDoc');
   Result := pdfHaveOpenDoc(FInstance);
end;

function TPDF.HaveOpenPage: Boolean;
begin
   LoadFunction(@pdfHaveOpenPage, 'pdfHaveOpenPage');
   Result := pdfHaveOpenPage(FInstance);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.HighlightAnnot(SubType: TAnnotType; PosX, PosY, Width, Height: Double; Color: Cardinal; const Author, Subject, Comment: AnsiString): Integer;
begin
   LoadFunction(@pdfHighlightAnnotA, 'pdfHighlightAnnotA');
   Result := pdfHighlightAnnotA(FInstance, SubType, PosX, PosY, Width, Height, Color, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Comment));
end;

function TPDF.HighlightAnnot(SubType: TAnnotType; PosX, PosY, Width, Height: Double; Color: Cardinal; const Author, Subject, Comment: WideString): Integer;
begin
   LoadFunction(@pdfHighlightAnnotW, 'pdfHighlightAnnotW');
   Result := pdfHighlightAnnotW(FInstance, SubType, PosX, PosY, Width, Height, Color, PWideChar(Author), PWideChar(Subject), PWideChar(Comment));
end;
{$endif}

function TPDF.HighlightAnnotA(SubType: TAnnotType; PosX, PosY, Width, Height: Double; Color: Cardinal; const Author, Subject, Comment: AnsiString): Integer;
begin
   LoadFunction(@pdfHighlightAnnotA, 'pdfHighlightAnnotA');
   Result := pdfHighlightAnnotA(FInstance, SubType, PosX, PosY, Width, Height, Color, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Comment));
end;

function TPDF.HighlightAnnotW(SubType: TAnnotType; PosX, PosY, Width, Height: Double; Color: Cardinal; const Author, Subject, Comment: WideString): Integer;
begin
   LoadFunction(@pdfHighlightAnnotW, 'pdfHighlightAnnotW');
   Result := pdfHighlightAnnotW(FInstance, SubType, PosX, PosY, Width, Height, Color, PWideChar(Author), PWideChar(Subject), PWideChar(Comment));
end;

function TPDF.ImportBookmarks: Integer;
begin
   LoadFunction(@pdfImportBookmarks, 'pdfImportBookmarks');
   Result := pdfImportBookmarks(FInstance);
end;

function TPDF.ImportCatalogObjects: Boolean;
begin
   LoadFunction(@pdfImportCatalogObjects, 'pdfImportCatalogObjects');
   Result := pdfImportCatalogObjects(FInstance);
end;

function TPDF.ImportDocInfo: Boolean;
begin
   LoadFunction(@pdfImportDocInfo, 'pdfImportDocInfo');
   Result := pdfImportDocInfo(FInstance);
end;

function TPDF.ImportPage(PageNum: Cardinal): Integer;
begin
   LoadFunction(@pdfImportPage, 'pdfImportPage');
   Result := pdfImportPage(FInstance, PageNum);
end;

function TPDF.ImportPageEx(PageNum: Cardinal; ScaleFactX, ScaleFactY: Double): Integer;
begin
   LoadFunction(@pdfImportPageEx, 'pdfImportPageEx');
   Result := pdfImportPageEx(FInstance, PageNum, ScaleFactX, ScaleFactY);
end;

function TPDF.ImportPDFFile(DestPage: Cardinal; ScaleFactX, ScaleFactY: Double): Integer;
begin
   LoadFunction(@pdfImportPDFFile, 'pdfImportPDFFile');
   Result := pdfImportPDFFile(FInstance, DestPage, ScaleFactX, ScaleFactY);
end;

function TPDF.InitColorManagement(Profiles: PPDFColorProfiles; DestSpace: TPDFColorSpace; Flags: TPDFInitCMFlags): Boolean;
begin
   LoadFunction(@pdfInitColorManagement, 'pdfInitColorManagement');
   Result := pdfInitColorManagement(FInstance, Profiles, DestSpace, Flags);
end;

function TPDF.InitExtGState(var GS: TPDFExtGState): Boolean;
begin
   LoadFunction(@pdfInitExtGState, 'pdfInitExtGState');
   Result := pdfInitExtGState(GS);
end;

function TPDF.InitOCGContUsage(var Value: TPDFOCGContUsage): Boolean;
begin
   LoadFunction(@pdfInitOCGContUsage, 'pdfInitOCGContUsage');
   Result := pdfInitOCGContUsage(Value);
end;

function TPDF.InitStack(var Stack: TPDFStack): Boolean;
begin
   LoadFunction(@pdfInitStack, 'pdfInitStack');
   Result := pdfInitStack(FInstance, Stack);
end;

function TPDF.InsertBMPFromBuffer(PosX, PosY, ScaleWidth, ScaleHeight: Double; const Buffer: Pointer): Integer;
begin
   LoadFunction(@pdfInsertBMPFromBuffer, 'pdfInsertBMPFromBuffer');
   Result := pdfInsertBMPFromBuffer(FInstance, PosX, PosY, ScaleWidth, ScaleHeight, Buffer);
end;

function TPDF.InsertBMPFromHandle(PosX, PosY, ScaleWidth, ScaleHeight: Double; Handle: HBITMAP): Integer;
begin
   LoadFunction(@pdfInsertBMPFromHandle, 'pdfInsertBMPFromHandle');
   Result := pdfInsertBMPFromHandle(FInstance, PosX, PosY, ScaleWidth, ScaleHeight, Pointer(Handle));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.InsertBookmark(const Title: AnsiString; Parent: Integer; DestPage: Cardinal; Open, AddChildren: Boolean): Integer;
begin
   LoadFunction(@pdfInsertBookmarkA, 'pdfInsertBookmarkA');
   Result := pdfInsertBookmarkA(FInstance, PAnsiChar(Title), Parent, DestPage, Open, AddChildren);
end;

function TPDF.InsertBookmark(const Title: WideString; Parent: Integer; DestPage: Cardinal; Open, AddChildren: Boolean): Integer;
begin
   LoadFunction(@pdfInsertBookmarkW, 'pdfInsertBookmarkW');
   Result := pdfInsertBookmarkW(FInstance, PWideChar(Title), Parent, DestPage, Open, AddChildren);
end;

function TPDF.InsertBookmarkEx(const Title: AnsiString; Parent: Integer; NamedDest: Cardinal; Open, AddChildren: Boolean): Integer;
begin
   LoadFunction(@pdfInsertBookmarkExA, 'pdfInsertBookmarkExA');
   Result := pdfInsertBookmarkExA(FInstance, PAnsiChar(Title), Parent, NamedDest, Open, AddChildren);
end;

function TPDF.InsertBookmarkEx(const Title: WideString; Parent: Integer; NamedDest: Cardinal; Open, AddChildren: Boolean): Integer;
begin
   LoadFunction(@pdfInsertBookmarkExW, 'pdfInsertBookmarkExW');
   Result := pdfInsertBookmarkExW(FInstance, PWideChar(Title), Parent, NamedDest, Open, AddChildren);
end;
{$endif}

function TPDF.InsertBookmarkA(const Title: AnsiString; Parent: Integer; DestPage: Cardinal; Open, AddChildren: Boolean): Integer;
begin
   LoadFunction(@pdfInsertBookmarkA, 'pdfInsertBookmarkA');
   Result := pdfInsertBookmarkA(FInstance, PAnsiChar(Title), Parent, DestPage, Open, AddChildren);
end;

function TPDF.InsertBookmarkW(const Title: WideString; Parent: Integer; DestPage: Cardinal; Open, AddChildren: Boolean): Integer;
begin
   LoadFunction(@pdfInsertBookmarkW, 'pdfInsertBookmarkW');
   Result := pdfInsertBookmarkW(FInstance, PWideChar(Title), Parent, DestPage, Open, AddChildren);
end;

function TPDF.InsertBookmarkExA(const Title: AnsiString; Parent: Integer; NamedDest: Cardinal; Open, AddChildren: Boolean): Integer;
begin
   LoadFunction(@pdfInsertBookmarkExA, 'pdfInsertBookmarkExA');
   Result := pdfInsertBookmarkExA(FInstance, PAnsiChar(Title), Parent, NamedDest, Open, AddChildren);
end;

function TPDF.InsertBookmarkExW(const Title: WideString; Parent: Integer; NamedDest: Cardinal; Open, AddChildren: Boolean): Integer;
begin
   LoadFunction(@pdfInsertBookmarkExW, 'pdfInsertBookmarkExW');
   Result := pdfInsertBookmarkExW(FInstance, PWideChar(Title), Parent, NamedDest, Open, AddChildren);
end;

function TPDF.InsertImage(PosX, PosY, ScaleWidth, ScaleHeight: Double; const AFile: AnsiString): Integer;
begin
   LoadFunction(@pdfInsertImage, 'pdfInsertImage');
   Result := pdfInsertImage(FInstance, PosX, PosY, ScaleWidth, ScaleHeight, PAnsiChar(AFile));
end;

function TPDF.InsertImageEx(PosX, PosY, ScaleWidth, ScaleHeight: Double; const AFile: AnsiString; Index: Cardinal): Integer;
begin
   LoadFunction(@pdfInsertImageEx, 'pdfInsertImageEx');
   Result := pdfInsertImageEx(FInstance, PosX, PosY, ScaleWidth, ScaleHeight, PAnsiChar(AFile), Index);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.InsertImageEx(PosX, PosY, ScaleWidth, ScaleHeight: Double; const AFile: WideString; Index: Cardinal): Integer;
begin
   LoadFunction(@pdfInsertImageExW, 'pdfInsertImageExW');
   Result := pdfInsertImageExW(FInstance, PosX, PosY, ScaleWidth, ScaleHeight, PWideChar(AFile), Index);
end;
{$endif}

function TPDF.InsertImageExW(PosX, PosY, ScaleWidth, ScaleHeight: Double; const AFile: WideString; Index: Cardinal): Integer;
begin
   LoadFunction(@pdfInsertImageExW, 'pdfInsertImageExW');
   Result := pdfInsertImageExW(FInstance, PosX, PosY, ScaleWidth, ScaleHeight, PWideChar(AFile), Index);
end;

function TPDF.InsertImageFromBuffer(PosX, PosY, ScaleWidth, ScaleHeight: Double; const Buffer: Pointer; BufSize, Index: Cardinal): Integer;
begin
   LoadFunction(@pdfInsertImageFromBuffer, 'pdfInsertImageFromBuffer');
   Result := pdfInsertImageFromBuffer(FInstance, PosX, PosY, ScaleWidth, ScaleHeight, Buffer, BufSize, Index);
end;

function TPDF.InsertRawImage(const Buffer: Pointer; BitsPerPixel, ColorCount, ImgWidth, ImgHeight: Cardinal; PosX, PosY, ScaleWidth, ScaleHeight: Double): Integer;
begin
   LoadFunction(@pdfInsertRawImage, 'pdfInsertRawImage');
   Result := pdfInsertRawImage(FInstance, Buffer, BitsPerPixel, ColorCount, ImgWidth, ImgHeight, PosX, PosY, ScaleWidth, ScaleHeight);
end;

function TPDF.InsertRawImageEx(PosX, PosY, ScaleWidth, ScaleHeight: Double; var Image: TPDFRawImage): Integer;
begin
   LoadFunction(@pdfInsertRawImageEx, 'pdfInsertRawImageEx');
   Result := pdfInsertRawImageEx(FInstance, PosX, PosY, ScaleWidth, ScaleHeight, Image);
end;

function TPDF.InsertMetafile(const FileName: AnsiString; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfInsertMetafileA, 'pdfInsertMetafileA');
   Result := pdfInsertMetafileA(FInstance, PAnsiChar(FileName), PosX, PosY, Width, Height);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.InsertMetafile(const FileName: WideString; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfInsertMetafileW, 'pdfInsertMetafileW');
   Result := pdfInsertMetafileW(FInstance, PWideChar(FileName), PosX, PosY, Width, Height);
end;
{$endif}

function TPDF.InsertMetafileA(const FileName: AnsiString; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfInsertMetafileA, 'pdfInsertMetafileA');
   Result := pdfInsertMetafileA(FInstance, PAnsiChar(FileName), PosX, PosY, Width, Height);
end;

function TPDF.InsertMetafileW(const FileName: WideString; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfInsertMetafileW, 'pdfInsertMetafileW');
   Result := pdfInsertMetafileW(FInstance, PWideChar(FileName), PosX, PosY, Width, Height);
end;

function TPDF.InsertMetafileEx(const Buffer: Pointer; BufSize: Cardinal; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfInsertMetafileEx, 'pdfInsertMetafileEx');
   Result := pdfInsertMetafileEx(FInstance, Buffer, BufSize, PosX, PosY, Width, Height);
end;

function TPDF.InsertMetafileExt(const FileName: AnsiString; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfInsertMetafileExtA, 'pdfInsertMetafileExtA');
   Result := pdfInsertMetafileExtA(FInstance, PAnsiChar(FileName), View, PosX, PosY, Width, Height);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.InsertMetafileExt(const FileName: WideString; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfInsertMetafileExtW, 'pdfInsertMetafileExtW');
   Result := pdfInsertMetafileExtW(FInstance, PWideChar(FileName), View, PosX, PosY, Width, Height);
end;
{$endif}

function TPDF.InsertMetafileExtA(const FileName: AnsiString; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfInsertMetafileExtA, 'pdfInsertMetafileExtA');
   Result := pdfInsertMetafileExtA(FInstance, PAnsiChar(FileName), View, PosX, PosY, Width, Height);
end;

function TPDF.InsertMetafileExtW(const FileName: WideString; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfInsertMetafileExtW, 'pdfInsertMetafileExtW');
   Result := pdfInsertMetafileExtW(FInstance, PWideChar(FileName), View, PosX, PosY, Width, Height);
end;

function TPDF.InsertMetafileExtEx(const Buffer: Pointer; BufSize: Cardinal; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfInsertMetafileExtEx, 'pdfInsertMetafileExtEx');
   Result := pdfInsertMetafileExtEx(FInstance, Buffer, BufSize, View, PosX, PosY, Width, Height);
end;

function TPDF.InsertMetafileFromHandle(Handle: HENHMETAFILE; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfInsertMetafileFromHandle, 'pdfInsertMetafileFromHandle');
   Result := pdfInsertMetafileFromHandle(FInstance, Pointer(Handle), PosX, PosY, Width, Height);
end;

function TPDF.InsertMetafileFromHandleEx(Handle: HENHMETAFILE; var View: TRectL; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfInsertMetafileFromHandleEx, 'pdfInsertMetafileFromHandleEx');
   Result := pdfInsertMetafileFromHandleEx(FInstance, Pointer(Handle), View, PosX, PosY, Width, Height);
end;

function TPDF.IsBidiText(const AText: WideString): Integer;
begin
   LoadFunction(@pdfIsBidiText, 'pdfIsBidiText');
   Result := pdfIsBidiText(FInstance, PWideChar(AText));
end;

{
   Return values:
   0: // Page is black & white
   1: // Page use colors
   Negative values indicate an error. The page must be opened beforehand with EditPage().
}

function TPDF.IsColorPage(GrayIsColor: Boolean): Integer;
begin
   LoadFunction(@pdfIsColorPage, 'pdfIsColorPage');
   Result := pdfIsColorPage(FInstance, GrayIsColor);
end;

{
   Return values:
   0: // Page is not empty
   1: // Page is empty
   Negative values indicate an error. The page must be opened beforehand with EditPage().
}

function TPDF.IsEmptyPage(): Integer;
begin
   LoadFunction(@pdfIsEmptyPage, 'pdfIsEmptyPage');
   Result := pdfIsEmptyPage(FInstance);
end;

function TPDF.IsWrongPwd(ErrCode: Integer): Boolean;
begin
   Result := (ErrCode = ENEED_PWD) or (ErrCode = EWRONG_OPEN_PWD) or (ErrCode = EWRONG_OWNER_PWD) or (ErrCode = EWRONG_PWD);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.LineAnnot(x1, y1, x2, y2, LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: AnsiString): Integer;
begin
   LoadFunction(@pdfLineAnnotA, 'pdfLineAnnotA');
   Result := pdfLineAnnotA(FInstance, x1, y1, x2, y2, LineWidth, lStart, lEnd, FillColor, StrokeColor, CS, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Content));
end;

function TPDF.LineAnnot(x1, y1, x2, y2, LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: WideString): Integer;
begin
   LoadFunction(@pdfLineAnnotW, 'pdfLineAnnotW');
   Result := pdfLineAnnotW(FInstance, x1, y1, x2, y2, LineWidth, lStart, lEnd, FillColor, StrokeColor, CS, PWideChar(Author), PWideChar(Subject), PWideChar(Content));
end;
{$endif}

function TPDF.LineAnnotA(x1, y1, x2, y2, LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: AnsiString): Integer;
begin
   LoadFunction(@pdfLineAnnotA, 'pdfLineAnnotA');
   Result := pdfLineAnnotA(FInstance, x1, y1, x2, y2, LineWidth, lStart, lEnd, FillColor, StrokeColor, CS, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Content));
end;

function TPDF.LineAnnotW(x1, y1, x2, y2, LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: WideString): Integer;
begin
   LoadFunction(@pdfLineAnnotW, 'pdfLineAnnotW');
   Result := pdfLineAnnotW(FInstance, x1, y1, x2, y2, LineWidth, lStart, lEnd, FillColor, StrokeColor, CS, PWideChar(Author), PWideChar(Subject), PWideChar(Content));
end;

function TPDF.LineTo(PosX, PosY: Double): Boolean;
begin
   LoadFunction(@pdfLineTo, 'pdfLineTo');
   Result := pdfLineTo(FInstance, PosX, PosY);
end;

function TPDF.LoadCMap(const CMapName: AnsiString; Embed: Boolean): Integer;
begin
   LoadFunction(@pdfLoadCMap, 'pdfLoadCMap');
   Result := pdfLoadCMap(FInstance, PAnsiChar(CMapName), Embed);
end;

function TPDF.LoadFont(const Buffer: Pointer; BufSize: Cardinal; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfLoadFont, 'pdfLoadFont');
   Result := pdfLoadFont(FInstance, Buffer, BufSize, Style, Size, Embed, Integer(CP));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.LoadFontEx(const FontFile: AnsiString; Index: Cardinal; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfLoadFontExA, 'pdfLoadFontExA');
   Result := pdfLoadFontExA(FInstance, PAnsiChar(FontFile), Index, Style, Size, Embed, CP);
end;

function TPDF.LoadFontEx(const FontFile: WideString; Index: Cardinal; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfLoadFontExW, 'pdfLoadFontExW');
   Result := pdfLoadFontExW(FInstance, PWideChar(FontFile), Index, Style, Size, Embed, CP);
end;
{$endif}

function TPDF.LoadFontExA(const FontFile: AnsiString; Index: Cardinal; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfLoadFontExA, 'pdfLoadFontExA');
   Result := pdfLoadFontExA(FInstance, PAnsiChar(FontFile), Index, Style, Size, Embed, CP);
end;

function TPDF.LoadFontExW(const FontFile: WideString; Index: Cardinal; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfLoadFontExW, 'pdfLoadFontExW');
   Result := pdfLoadFontExW(FInstance, PWideChar(FontFile), Index, Style, Size, Embed, CP);
end;

function TPDF.LockLayer(Layer: Cardinal): Boolean;
begin
   LoadFunction(@pdfLockLayer, 'pdfLockLayer');
   Result := pdfLockLayer(FInstance, Layer);
end;

function TPDF.LoadFDFDataA(const FileName, Password: AnsiString; Flags: Cardinal): Boolean;
begin
   LoadFunction(@pdfLoadFDFDataA, 'pdfLoadFDFDataA');
   Result := pdfLoadFDFDataA(FInstance, PAnsiChar(FileName), PAnsiChar(Password), Flags);
end;

function TPDF.LoadFDFDataW(const FileName: WideString; const Password: AnsiString; Flags: Cardinal): Boolean;
begin
   LoadFunction(@pdfLoadFDFDataW, 'pdfLoadFDFDataW');
   Result := pdfLoadFDFDataW(FInstance, PWideChar(FileName), PAnsiChar(Password), Flags);
end;

function TPDF.LoadFDFDataEx(const Buffer: Pointer; BufSize: Cardinal; const Password: AnsiString; Flags: Cardinal): Boolean;
begin
   LoadFunction(@pdfLoadFDFDataEx, 'pdfLoadFDFDataEx');
   Result := pdfLoadFDFDataEx(FInstance, Buffer, BufSize, PAnsiChar(Password), Flags);
end;

function TPDF.MovePage(Source, Dest: Cardinal): Boolean;
begin
   LoadFunction(@pdfMovePage, 'pdfMovePage');
   Result := pdfMovePage(FInstance, Source, Dest);
end;

function TPDF.MoveTo(PosX, PosY: Double): Boolean;
begin
   LoadFunction(@pdfMoveTo, 'pdfMoveTo');
   Result := pdfMoveTo(FInstance, PosX, PosY);
end;

procedure TPDF.MultiplyMatrix(var M1, M2, NewMatrix: TCTM);
begin
   NewMatrix.a := M2.a * M1.a + M2.b * M1.c;
   NewMatrix.b := M2.a * M1.b + M2.b * M1.d;
   NewMatrix.c := M2.c * M1.a + M2.d * M1.c;
   NewMatrix.d := M2.c * M1.b + M2.d * M1.d;
   NewMatrix.x := M2.x * M1.a + M2.y * M1.c + M1.x;
   NewMatrix.y := M2.x * M1.b + M2.y * M1.d + M1.y;
end;

procedure TPDF.NewPDF();
var pdfNewPDF: TpdfNewPDF;
begin
   pdfNewPDF := GetProcAddress(FDLL_Handle, 'pdfNewPDF');
   if @pdfNewPDF = nil then raise Exception.Create('Error loading function: pdfNewPDF');
   FInstance := pdfNewPDF;
   if (FInstance = nil) then Exception.Create('Out of memory!');
end;

function TPDF.OpenImportBuffer(const Buffer: Pointer; BufSize: Cardinal; PwdType: TPwdType; const Password: AnsiString): Integer;
begin
   LoadFunction(@pdfOpenImportBuffer, 'pdfOpenImportBuffer');
   Result := pdfOpenImportBuffer(FInstance, Buffer, BufSize, Ord(PwdType), PAnsiChar(Password));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.OpenImportFile(const FileName: AnsiString; PwdType: TPwdType; const Password: AnsiString): Integer;
begin
   LoadFunction(@pdfOpenImportFileA, 'pdfOpenImportFileA');
   Result := pdfOpenImportFileA(FInstance, PAnsiChar(FileName), Ord(PwdType), PAnsiChar(Password));
end;

function TPDF.OpenImportFile(const FileName: WideString; PwdType: TPwdType; const Password: AnsiString): Integer;
begin
   LoadFunction(@pdfOpenImportFileW, 'pdfOpenImportFileW');
   Result := pdfOpenImportFileW(FInstance, PWideChar(FileName), Ord(PwdType), PAnsiChar(Password));
end;

function TPDF.OpenOutputFile(const OutPDF: AnsiString): Boolean;
begin
   LoadFunction(@pdfOpenOutputFileA, 'pdfOpenOutputFileA');
   Result := pdfOpenOutputFileA(FInstance, PAnsiChar(OutPDF));
end;

function TPDF.OpenOutputFile(const OutPDF: WideString): Boolean;
begin
   LoadFunction(@pdfOpenOutputFileW, 'pdfOpenOutputFileW');
   Result := pdfOpenOutputFileW(FInstance, PWideChar(OutPDF));
end;

function TPDF.OpenTag(Tag: TPDFBaseTag; const Lang, AltText, Expansion: AnsiString): Boolean;
begin
   LoadFunction(@pdfOpenTagA, 'pdfOpenTagA');
   Result := pdfOpenTagA(FInstance, Integer(Tag), PAnsiChar(Lang), PAnsiChar(AltText), PAnsiChar(Expansion));
end;

function TPDF.OpenTag(Tag: TPDFBaseTag; const Lang: AnsiString; const AltText, Expansion: WideString): Boolean;
begin
   LoadFunction(@pdfOpenTagW, 'pdfOpenTagW');
   Result := pdfOpenTagW(FInstance, Integer(Tag), PAnsiChar(Lang), PWideChar(AltText), PWideChar(Expansion));
end;
{$endif}

function TPDF.OpenImportFileA(const FileName: AnsiString; PwdType: TPwdType; const Password: AnsiString): Integer;
begin
   LoadFunction(@pdfOpenImportFileA, 'pdfOpenImportFileA');
   Result := pdfOpenImportFileA(FInstance, PAnsiChar(FileName), Ord(PwdType), PAnsiChar(Password));
end;

function TPDF.OpenImportFileW(const FileName: WideString; PwdType: TPwdType; const Password: AnsiString): Integer;
begin
   LoadFunction(@pdfOpenImportFileW, 'pdfOpenImportFileW');
   Result := pdfOpenImportFileW(FInstance, PWideChar(FileName), Ord(PwdType), PAnsiChar(Password));
end;

function TPDF.OpenOutputFileA(const OutPDF: AnsiString): Boolean;
begin
   LoadFunction(@pdfOpenOutputFileA, 'pdfOpenOutputFileA');
   Result := pdfOpenOutputFileA(FInstance, PAnsiChar(OutPDF));
end;

function TPDF.OpenOutputFileW(const OutPDF: WideString): Boolean;
begin
   LoadFunction(@pdfOpenOutputFileW, 'pdfOpenOutputFileW');
   Result := pdfOpenOutputFileW(FInstance, PWideChar(OutPDF));
end;

function TPDF.OpenTagA(Tag: TPDFBaseTag; const Lang, AltText, Expansion: AnsiString): Boolean;
begin
   LoadFunction(@pdfOpenTagA, 'pdfOpenTagA');
   Result := pdfOpenTagA(FInstance, Integer(Tag), PAnsiChar(Lang), PAnsiChar(AltText), PAnsiChar(Expansion));
end;

function TPDF.OpenTagW(Tag: TPDFBaseTag; const Lang: AnsiString; const AltText, Expansion: WideString): Boolean;
begin
   LoadFunction(@pdfOpenTagW, 'pdfOpenTagW');
   Result := pdfOpenTagW(FInstance, Integer(Tag), PAnsiChar(Lang), PWideChar(AltText), PWideChar(Expansion));
end;

function TPDF.Optimize(Flags: Cardinal; Parms: Pointer): Boolean;
begin
   LoadFunction(@pdfOptimize, 'pdfOptimize');
   Result := pdfOptimize(FInstance, Flags, Parms);
end;

function TPDF.PageLink(PosX, PosY, Width, Height: Double; DestPage: Cardinal): Integer;
begin
   LoadFunction(@pdfPageLink, 'pdfPageLink');
   Result := pdfPageLink(FInstance, PosX, PosY, Width, Height, DestPage);
end;

function TPDF.PageLink2(PosX, PosY, Width, Height: Double; NamedDest: Cardinal): Integer;
begin
   LoadFunction(@pdfPageLink2, 'pdfPageLink2');
   Result := pdfPageLink2(FInstance, PosX, PosY, Width, Height, NamedDest);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.PageLink3(PosX, PosY, Width, Height: Double; const NamedDest: AnsiString): Integer;
begin
   LoadFunction(@pdfPageLink3A, 'pdfPageLink3A');
   Result := pdfPageLink3A(FInstance, PosX, PosY, Width, Height, PAnsiChar(NamedDest));
end;

function TPDF.PageLink3(PosX, PosY, Width, Height: Double; const NamedDest: WideString): Integer;
begin
   LoadFunction(@pdfPageLink3W, 'pdfPageLink3W');
   Result := pdfPageLink3W(FInstance, PosX, PosY, Width, Height, PWideChar(NamedDest));
end;
{$endif}

function TPDF.PageLink3A(PosX, PosY, Width, Height: Double; const NamedDest: AnsiString): Integer;
begin
   LoadFunction(@pdfPageLink3A, 'pdfPageLink3A');
   Result := pdfPageLink3A(FInstance, PosX, PosY, Width, Height, PAnsiChar(NamedDest));
end;

function TPDF.PageLink3W(PosX, PosY, Width, Height: Double; const NamedDest: WideString): Integer;
begin
   LoadFunction(@pdfPageLink3W, 'pdfPageLink3W');
   Result := pdfPageLink3W(FInstance, PosX, PosY, Width, Height, PWideChar(NamedDest));
end;

function TPDF.PageLinkEx(PosX, PosY, Width, Height: Double; DestType: TDestType; DestPage: Cardinal; a, b, c, d: Double): Integer;
begin
   LoadFunction(@pdfPageLinkEx, 'pdfPageLinkEx');
   Result := pdfPageLinkEx(FInstance, PosX, PosY, Width, Height, Integer(DestType), DestPage, a, b, c, d);
end;

function TPDF.ParseContent(const UserData: Pointer; var Stack: TPDFParseInterface; Flags: TParseFlags): Boolean;
begin
   LoadFunction(@pdfParseContent, 'pdfParseContent');
   Result := pdfParseContent(FInstance, UserData, Stack, Flags);
end;

function TPDF.PlaceImage(ImgHandle: Integer; PosX, PosY, ScaleWidth, ScaleHeight: Double): Boolean;
begin
   LoadFunction(@pdfPlaceImage, 'pdfPlaceImage');
   Result := pdfPlaceImage(FInstance, ImgHandle, PosX, PosY, ScaleWidth, ScaleHeight);
end;

function TPDF.PlaceSigFieldValidateIcon(SigField: Cardinal; PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfPlaceSigFieldValidateIcon, 'pdfPlaceSigFieldValidateIcon');
   Result := pdfPlaceSigFieldValidateIcon(FInstance, SigField, PosX, PosY, Width, Height);
end;

function TPDF.PlaceTemplate(TmplHandle: Integer; PosX, PosY, ScaleWidth, ScaleHeight: Double): Boolean;
begin
   LoadFunction(@pdfPlaceTemplate, 'pdfPlaceTemplate');
   Result := pdfPlaceTemplate(FInstance, TmplHandle, PosX, PosY, ScaleWidth, ScaleHeight);
end;

function TPDF.PlaceTemplateEx(TmplHandle: Integer; PosX, PosY, ScaleWidth, ScaleHeight: Double): Boolean;
begin
   LoadFunction(@pdfPlaceTemplateEx, 'pdfPlaceTemplateEx');
   Result := pdfPlaceTemplateEx(FInstance, TmplHandle, PosX, PosY, ScaleWidth, ScaleHeight);
end;

function TPDF.PlaceTemplByMatrix(TmplHandle: Integer): Boolean;
begin
   LoadFunction(@pdfPlaceTemplByMatrix, 'pdfPlaceTemplByMatrix');
   Result := pdfPlaceTemplByMatrix(FInstance, TmplHandle);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.PolygonAnnot(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: AnsiString): Integer;
begin
   LoadFunction(@pdfPolygonAnnotA, 'pdfPolygonAnnotA');
   Result := pdfPolygonAnnotA(FInstance, Vertices, NumVertices, LineWidth, FillColor, StrokeColor, CS, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Content));
end;

function TPDF.PolygonAnnot(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: WideString): Integer;
begin
   LoadFunction(@pdfPolygonAnnotW, 'pdfPolygonAnnotW');
   Result := pdfPolygonAnnotW(FInstance, Vertices, NumVertices, LineWidth, FillColor, StrokeColor, CS, PWideChar(Author), PWideChar(Subject), PWideChar(Content));
end;
{$endif}

function TPDF.PolygonAnnotA(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: AnsiString): Integer;
begin
   LoadFunction(@pdfPolygonAnnotA, 'pdfPolygonAnnotA');
   Result := pdfPolygonAnnotA(FInstance, Vertices, NumVertices, LineWidth, FillColor, StrokeColor, CS, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Content));
end;

function TPDF.PolygonAnnotW(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: WideString): Integer;
begin
   LoadFunction(@pdfPolygonAnnotW, 'pdfPolygonAnnotW');
   Result := pdfPolygonAnnotW(FInstance, Vertices, NumVertices, LineWidth, FillColor, StrokeColor, CS, PWideChar(Author), PWideChar(Subject), PWideChar(Content));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.PolyLineAnnot(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: AnsiString): Integer;
begin
   LoadFunction(@pdfPolyLineAnnotA, 'pdfPolyLineAnnotA');
   Result := pdfPolyLineAnnotA(FInstance, Vertices, NumVertices, LineWidth, lStart, lEnd, FillColor, StrokeColor, CS, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Content));
end;

function TPDF.PolyLineAnnot(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: WideString): Integer;
begin
   LoadFunction(@pdfPolyLineAnnotW, 'pdfPolyLineAnnotW');
   Result := pdfPolyLineAnnotW(FInstance, Vertices, NumVertices, LineWidth, lStart, lEnd, FillColor, StrokeColor, CS, PWideChar(Author), PWideChar(Subject), PWideChar(Content));
end;
{$endif}

function TPDF.PolyLineAnnotA(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: AnsiString): Integer;
begin
   LoadFunction(@pdfPolyLineAnnotA, 'pdfPolyLineAnnotA');
   Result := pdfPolyLineAnnotA(FInstance, Vertices, NumVertices, LineWidth, lStart, lEnd, FillColor, StrokeColor, CS, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Content));
end;

function TPDF.PolyLineAnnotW(Vertices: PFltPoint; NumVertices: Cardinal; LineWidth: Double; lStart, lEnd: TLineEndStyle; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Content: WideString): Integer;
begin
   LoadFunction(@pdfPolyLineAnnotW, 'pdfPolyLineAnnotW');
   Result := pdfPolyLineAnnotW(FInstance, Vertices, NumVertices, LineWidth, lStart, lEnd, FillColor, StrokeColor, CS, PWideChar(Author), PWideChar(Subject), PWideChar(Content));
end;

function TPDF.ReadImageFormat(const FileName: AnsiString; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
begin
   LoadFunction(@pdfReadImageFormatA, 'pdfReadImageFormatA');
   Result := pdfReadImageFormatA(FInstance, PAnsiChar(FileName), Width, Height, BitsPerPixel, UseZip);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.ReadImageFormat(const FileName: WideString; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
begin
   LoadFunction(@pdfReadImageFormatW, 'pdfReadImageFormatW');
   Result := pdfReadImageFormatW(FInstance, PWideChar(FileName), Width, Height, BitsPerPixel, UseZip);
end;
{$endif}

function TPDF.ReadImageFormatA(const FileName: AnsiString; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
begin
   LoadFunction(@pdfReadImageFormatA, 'pdfReadImageFormatA');
   Result := pdfReadImageFormatA(FInstance, PAnsiChar(FileName), Width, Height, BitsPerPixel, UseZip);
end;

function TPDF.ReadImageFormatW(const FileName: WideString; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
begin
   LoadFunction(@pdfReadImageFormatW, 'pdfReadImageFormatW');
   Result := pdfReadImageFormatW(FInstance, PWideChar(FileName), Width, Height, BitsPerPixel, UseZip);
end;

function TPDF.ReadImageFormat2(const FileName: AnsiString; Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
begin
   LoadFunction(@pdfReadImageFormat2A, 'pdfReadImageFormat2A');
   Result := pdfReadImageFormat2A(FInstance, PAnsiChar(FileName), Index, Width, Height, BitsPerPixel, UseZip);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.ReadImageFormat2(const FileName: WideString; Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
begin
   LoadFunction(@pdfReadImageFormat2W, 'pdfReadImageFormat2W');
   Result := pdfReadImageFormat2W(FInstance, PWideChar(FileName), Index, Width, Height, BitsPerPixel, UseZip);
end;
{$endif}

function TPDF.ReadImageFormat2A(const FileName: AnsiString; Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
begin
   LoadFunction(@pdfReadImageFormat2A, 'pdfReadImageFormat2A');
   Result := pdfReadImageFormat2A(FInstance, PAnsiChar(FileName), Index, Width, Height, BitsPerPixel, UseZip);
end;

function TPDF.ReadImageFormat2W(const FileName: WideString; Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
begin
   LoadFunction(@pdfReadImageFormat2W, 'pdfReadImageFormat2W');
   Result := pdfReadImageFormat2W(FInstance, PWideChar(FileName), Index, Width, Height, BitsPerPixel, UseZip);
end;

function TPDF.ReadImageFormatEx(hBitmap: HBITMAP; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
begin
   LoadFunction(@pdfReadImageFormatEx, 'pdfReadImageFormatEx');
   Result := pdfReadImageFormatEx(FInstance, Pointer(hBitmap), Width, Height, BitsPerPixel, UseZip);
end;

function TPDF.ReadImageFormatFromBuffer(const Buffer: Pointer; BufSize, Index: Cardinal; var Width, Height: Cardinal; var BitsPerPixel, UseZip: Integer): Boolean;
begin
   LoadFunction(@pdfReadImageFormatFromBuffer, 'pdfReadImageFormatFromBuffer');
   Result := pdfReadImageFormatFromBuffer(FInstance, Buffer, BufSize, Index, Width, Height, BitsPerPixel, UseZip);
end;

function TPDF.ReadImageResolution(const FileName: AnsiString; Index: Cardinal; var ResX, ResY: Cardinal): Boolean;
begin
   LoadFunction(@pdfReadImageResolutionA, 'pdfReadImageResolutionA');
   Result := pdfReadImageResolutionA(FInstance, PAnsiChar(FileName), Index, ResX, ResY);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.ReadImageResolution(const FileName: WideString; Index: Cardinal; var ResX, ResY: Cardinal): Boolean;
begin
   LoadFunction(@pdfReadImageResolutionW, 'pdfReadImageResolutionW');
   Result := pdfReadImageResolutionW(FInstance, PWideChar(FileName), Index, ResX, ResY);
end;
{$endif}

function TPDF.ReadImageResolutionA(const FileName: AnsiString; Index: Cardinal; var ResX, ResY: Cardinal): Boolean;
begin
   LoadFunction(@pdfReadImageResolutionA, 'pdfReadImageResolutionA');
   Result := pdfReadImageResolutionA(FInstance, PAnsiChar(FileName), Index, ResX, ResY);
end;

function TPDF.ReadImageResolutionW(const FileName: WideString; Index: Cardinal; var ResX, ResY: Cardinal): Boolean;
begin
   LoadFunction(@pdfReadImageResolutionW, 'pdfReadImageResolutionW');
   Result := pdfReadImageResolutionW(FInstance, PWideChar(FileName), Index, ResX, ResY);
end;

function TPDF.ReadImageResolutionEx(const Buffer: Pointer; BufSize, Index: Cardinal; var ResX, ResY: Cardinal): Boolean;
begin
   LoadFunction(@pdfReadImageResolutionEx, 'pdfReadImageResolutionEx');
   Result := pdfReadImageResolutionEx(FInstance, Buffer, BufSize, Index, ResX, ResY);
end;

function TPDF.Rectangle(PosX, PosY, Width, Height: Double; FillMode: TPathFillMode): Boolean;
begin
   LoadFunction(@pdfRectangle, 'pdfRectangle');
   Result := pdfRectangle(FInstance, PosX, PosY, Width, Height, Ord(FillMode));
end;

procedure TPDF.Redraw(RasPtr: Pointer; DC: HDC; DestX, DestY: Integer);
begin
   LoadFunction(@rasRedraw, 'rasRedraw');
   rasRedraw(RasPtr, DC, DestX, DestY);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.ReEncryptPDF(const FileName: AnsiString; PwdType: TPwdType; const InPwd, NewOpenPwd, NewOwnerPwd: AnsiString; NewKeyLen: TKeyLen; Restrict: TRestrictions): Integer;
begin
   LoadFunction(@pdfReEncryptPDFA, 'pdfReEncryptPDFA');
   Result := pdfReEncryptPDFA(FInstance, PAnsiChar(FileName), Ord(PwdType), PAnsiChar(InPwd), PAnsiChar(NewOpenPwd), PAnsiChar(NewOwnerPwd), NewKeyLen, Restrict);
end;

function TPDF.ReEncryptPDF(const FileName: WideString; PwdType: TPwdType; const InPwd, NewOpenPwd, NewOwnerPwd: AnsiString; NewKeyLen: TKeyLen; Restrict: TRestrictions): Integer;
begin
   LoadFunction(@pdfReEncryptPDFW, 'pdfReEncryptPDFW');
   Result := pdfReEncryptPDFW(FInstance, PWideChar(FileName), Ord(PwdType), PAnsiChar(InPwd), PAnsiChar(NewOpenPwd), PAnsiChar(NewOwnerPwd), NewKeyLen, Restrict);
end;
{$endif}

function TPDF.ReEncryptPDFA(const FileName: AnsiString; PwdType: TPwdType; const InPwd, NewOpenPwd, NewOwnerPwd: AnsiString; NewKeyLen: TKeyLen; Restrict: TRestrictions): Integer;
begin
   LoadFunction(@pdfReEncryptPDFA, 'pdfReEncryptPDFA');
   Result := pdfReEncryptPDFA(FInstance, PAnsiChar(FileName), Ord(PwdType), PAnsiChar(InPwd), PAnsiChar(NewOpenPwd), PAnsiChar(NewOwnerPwd), NewKeyLen, Restrict);
end;

function TPDF.ReEncryptPDFW(const FileName: WideString; PwdType: TPwdType; const InPwd, NewOpenPwd, NewOwnerPwd: AnsiString; NewKeyLen: TKeyLen; Restrict: TRestrictions): Integer;
begin
   LoadFunction(@pdfReEncryptPDFW, 'pdfReEncryptPDFW');
   Result := pdfReEncryptPDFW(FInstance, PWideChar(FileName), Ord(PwdType), PAnsiChar(InPwd), PAnsiChar(NewOpenPwd), PAnsiChar(NewOwnerPwd), NewKeyLen, Restrict);
end;

function TPDF.RenameSpotColor(const Colorant, NewName: AnsiString): Integer;
begin
   LoadFunction(@pdfRenameSpotColor, 'pdfRenameSpotColor');
   Result := pdfRenameSpotColor(FInstance, PAnsiChar(Colorant), PAnsiChar(NewName));
end;

function TPDF.RenderAnnotOrField(Handle: Cardinal; IsAnnot: Boolean; State: TButtonState; var Matrix: TCTM; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; var OutImage: TPDFBitmap): Boolean;
begin
   LoadFunction(@pdfRenderAnnotOrField, 'pdfRenderAnnotOrField');
   Result := pdfRenderAnnotOrField(FInstance, Handle, IsAnnot, State, Matrix, Flags, PixFmt, Filter, OutImage);
end;

function TPDF.RenderPage(PagePtr, RasPtr: Pointer; var Img: TPDFRasterImage): Boolean;
begin
   LoadFunction(@pdfRenderPage, 'pdfRenderPage');
   Result := pdfRenderPage(FInstance, PagePtr, RasPtr, Img);
end;

function TPDF.RenderPageEx(DC: HDC; var DestX, DestY: Integer; PagePtr, RasPtr: Pointer; var Img: TPDFRasterImage): Boolean;
begin
   LoadFunction(@pdfRenderPageEx, 'pdfRenderPageEx');
   Result := pdfRenderPageEx(FInstance, DC, DestX, DestY, PagePtr, RasPtr, Img);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.RenderPageToImage(PageNum: Cardinal; const OutFile: AnsiString; Resolution, Width, Height: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean;
begin
   LoadFunction(@pdfRenderPageToImageA, 'pdfRenderPageToImageA');
   Result := pdfRenderPageToImageA(FInstance, PageNum, PAnsiChar(OutFile), Resolution, Width, Height, Flags, PixFmt, Filter, Format);
end;

function TPDF.RenderPageToImage(PageNum: Cardinal; const OutFile: WideString; Resolution, Width, Height: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean;
begin
   LoadFunction(@pdfRenderPageToImageW, 'pdfRenderPageToImageW');
   Result := pdfRenderPageToImageW(FInstance, PageNum, PWideChar(OutFile), Resolution, Width, Height, Flags, PixFmt, Filter, Format);
end;
{$endif}

function TPDF.RenderPageToImageA(PageNum: Cardinal; const OutFile: AnsiString; Resolution, Width, Height: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean;
begin
   LoadFunction(@pdfRenderPageToImageA, 'pdfRenderPageToImageA');
   Result := pdfRenderPageToImageA(FInstance, PageNum, PAnsiChar(OutFile), Resolution, Width, Height, Flags, PixFmt, Filter, Format);
end;

function TPDF.RenderPageToImageW(PageNum: Cardinal; const OutFile: WideString; Resolution, Width, Height: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean;
begin
   LoadFunction(@pdfRenderPageToImageW, 'pdfRenderPageToImageW');
   Result := pdfRenderPageToImageW(FInstance, PageNum, PWideChar(OutFile), Resolution, Width, Height, Flags, PixFmt, Filter, Format);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.RenderPDFFile(const OutFile: AnsiString; Resolution: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean;
begin
   LoadFunction(@pdfRenderPDFFileA, 'pdfRenderPDFFileA');
   Result := pdfRenderPDFFileA(FInstance, PAnsiChar(OutFile), Resolution, Flags, PixFmt, Filter, Format);
end;

function TPDF.RenderPDFFile(const OutFile: WideString; Resolution: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean;
begin
   LoadFunction(@pdfRenderPDFFileW, 'pdfRenderPDFFileW');
   Result := pdfRenderPDFFileW(FInstance, PWideChar(OutFile), Resolution, Flags, PixFmt, Filter, Format);
end;

function TPDF.ReplaceFont(const PDFFont: Pointer; const Name: AnsiString; Style: TFStyle; NameIsFamilyName: Boolean): Integer;
begin
   LoadFunction(@pdfReplaceFontA, 'pdfReplaceFontA');
   Result := pdfReplaceFontA(FInstance, PDFFont, PAnsiChar(Name), Integer(Style), NameIsFamilyName);
end;

function TPDF.ReplaceFont(const PDFFont: Pointer; const Name: WideString; Style: TFStyle; NameIsFamilyName: Boolean): Integer;
begin
   LoadFunction(@pdfReplaceFontW, 'pdfReplaceFontW');
   Result := pdfReplaceFontW(FInstance, PDFFont, PWideChar(Name), Integer(Style), NameIsFamilyName);
end;

function TPDF.ReplaceFontEx(const PDFFont: Pointer; const FontFile: AnsiString; Embed: Boolean): Integer;
begin
   LoadFunction(@pdfReplaceFontExA, 'pdfReplaceFontExA');
   Result := pdfReplaceFontExA(FInstance, PDFFont, PAnsiChar(FontFile), Embed);
end;

function TPDF.ReplaceFontEx(const PDFFont: Pointer; const FontFile: WideString; Embed: Boolean): Integer;
begin
   LoadFunction(@pdfReplaceFontExW, 'pdfReplaceFontExW');
   Result := pdfReplaceFontExW(FInstance, PDFFont, PWideChar(FontFile), Embed);
end;

function TPDF.ReplaceICCProfile(ColorSpace: Cardinal; const ICCFile: AnsiString): Integer;
begin
   LoadFunction(@pdfReplaceICCProfileA, 'pdfReplaceICCProfileA');
   Result := pdfReplaceICCProfileA(FInstance, ColorSpace, PAnsiChar(ICCFile));
end;

function TPDF.ReplaceICCProfile(ColorSpace: Cardinal; const ICCFile: WideString): Integer;
begin
   LoadFunction(@pdfReplaceICCProfileW, 'pdfReplaceICCProfileW');
   Result := pdfReplaceICCProfileW(FInstance, ColorSpace, PWideChar(ICCFile));
end;

function TPDF.ReplacePageTextEx(const NewText: AnsiString; var Stack: TPDFStack): Boolean;
begin
   LoadFunction(@pdfReplacePageTextExA, 'pdfReplacePageTextExA');
   Result := pdfReplacePageTextExA(FInstance, PAnsiChar(NewText), Stack);
end;

function TPDF.ReplacePageTextEx(const NewText: WideString; var Stack: TPDFStack): Boolean;
begin
   LoadFunction(@pdfReplacePageTextExW, 'pdfReplacePageTextExW');
   Result := pdfReplacePageTextExW(FInstance, PWideChar(NewText), Stack);
end;
{$endif}

function TPDF.RenderPDFFileA(const OutFile: AnsiString; Resolution: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean;
begin
   LoadFunction(@pdfRenderPDFFileA, 'pdfRenderPDFFileA');
   Result := pdfRenderPDFFileA(FInstance, PAnsiChar(OutFile), Resolution, Flags, PixFmt, Filter, Format);
end;

function TPDF.RenderPDFFileW(const OutFile: WideString; Resolution: Cardinal; Flags: TRasterFlags; PixFmt: TPDFPixFormat; Filter: TCompressionFilter; Format: TImageFormat): Boolean;
begin
   LoadFunction(@pdfRenderPDFFileW, 'pdfRenderPDFFileW');
   Result := pdfRenderPDFFileW(FInstance, PWideChar(OutFile), Resolution, Flags, PixFmt, Filter, Format);
end;

function TPDF.ReOpenImportFile(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfReOpenImportFile, 'pdfReOpenImportFile');
   Result := pdfReOpenImportFile(FInstance, Handle);
end;

function TPDF.ReplaceFontA(const PDFFont: Pointer; const Name: AnsiString; Style: TFStyle; NameIsFamilyName: Boolean): Integer;
begin
   LoadFunction(@pdfReplaceFontA, 'pdfReplaceFontA');
   Result := pdfReplaceFontA(FInstance, PDFFont, PAnsiChar(Name), Integer(Style), NameIsFamilyName);
end;

function TPDF.ReplaceFontW(const PDFFont: Pointer; const Name: WideString; Style: TFStyle; NameIsFamilyName: Boolean): Integer;
begin
   LoadFunction(@pdfReplaceFontW, 'pdfReplaceFontW');
   Result := pdfReplaceFontW(FInstance, PDFFont, PWideChar(Name), Integer(Style), NameIsFamilyName);
end;

function TPDF.ReplaceFontExA(const PDFFont: Pointer; const FontFile: AnsiString; Embed: Boolean): Integer;
begin
   LoadFunction(@pdfReplaceFontExA, 'pdfReplaceFontExA');
   Result := pdfReplaceFontExA(FInstance, PDFFont, PAnsiChar(FontFile), Embed);
end;

function TPDF.ReplaceFontExW(const PDFFont: Pointer; const FontFile: WideString; Embed: Boolean): Integer;
begin
   LoadFunction(@pdfReplaceFontExW, 'pdfReplaceFontExW');
   Result := pdfReplaceFontExW(FInstance, PDFFont, PWideChar(FontFile), Embed);
end;

function TPDF.ReplaceICCProfileA(ColorSpace: Cardinal; const ICCFile: AnsiString): Integer;
begin
   LoadFunction(@pdfReplaceICCProfileA, 'pdfReplaceICCProfileA');
   Result := pdfReplaceICCProfileA(FInstance, ColorSpace, PAnsiChar(ICCFile));
end;

function TPDF.ReplaceICCProfileW(ColorSpace: Cardinal; const ICCFile: WideString): Integer;
begin
   LoadFunction(@pdfReplaceICCProfileW, 'pdfReplaceICCProfileW');
   Result := pdfReplaceICCProfileW(FInstance, ColorSpace, PWideChar(ICCFile));
end;

function TPDF.ReplacePageText(const NewText: AnsiString; var Stack: TPDFStack): Boolean;
begin
   LoadFunction(@pdfReplacePageTextA, 'pdfReplacePageTextA');
   Result := pdfReplacePageTextA(FInstance, PAnsiChar(NewText), Stack);
end;

function TPDF.ReplacePageTextExA(const NewText: AnsiString; var Stack: TPDFStack): Boolean;
begin
   LoadFunction(@pdfReplacePageTextExA, 'pdfReplacePageTextExA');
   Result := pdfReplacePageTextExA(FInstance, PAnsiChar(NewText), Stack);
end;

function TPDF.ReplacePageTextExW(const NewText: WideString; var Stack: TPDFStack): Boolean;
begin
   LoadFunction(@pdfReplacePageTextExW, 'pdfReplacePageTextExW');
   Result := pdfReplacePageTextExW(FInstance, PWideChar(NewText), Stack);
end;

function TPDF.ResetLineDashPattern(): Boolean;
begin
   LoadFunction(@pdfResetLineDashPattern, 'pdfResetLineDashPattern');
   Result := pdfResetLineDashPattern(FInstance);
end;

function TPDF.ResizeBitmap(RasPtr: Pointer; DC: HDC; Width, Height: Cardinal): Boolean;
begin
   LoadFunction(@rasResizeBitmap, 'rasResizeBitmap');
   Result := rasResizeBitmap(RasPtr, DC, Width, Height);
end;

function TPDF.RestoreGraphicState(): Boolean;
begin
   LoadFunction(@pdfRestoreGraphicState, 'pdfRestoreGraphicState');
   Result := pdfRestoreGraphicState(FInstance);
end;

function TPDF.RotateCoords(alpha, OriginX, OriginY: Double): Boolean;
begin
   LoadFunction(@pdfRotateCoords, 'pdfRotateCoords');
   Result := pdfRotateCoords(FInstance, alpha, OriginX, OriginY);
end;

function TPDF.RoundRect(PosX, PosY, Width, Height, Radius: Double; FillMode: TPathFillMode): Boolean;
begin
   LoadFunction(@pdfRoundRect, 'pdfRoundRect');
   Result := pdfRoundRect(FInstance, PosX, PosY, Width, Height, Radius, Ord(FillMode));
end;

function TPDF.RoundRectEx(PosX, PosY, Width, Height, rWidth, rHeight: Double; FillMode: TPathFillMode): Boolean;
begin
   LoadFunction(@pdfRoundRectEx, 'pdfRoundRectEx');
   Result := pdfRoundRectEx(FInstance, PosX, PosY, Width, Height, rWidth, rHeight, Ord(FillMode));
end;

function TPDF.SaveGraphicState(): Boolean;
begin
   LoadFunction(@pdfSaveGraphicState, 'pdfSaveGraphicState');
   Result := pdfSaveGraphicState(FInstance);
end;

function TPDF.ScaleCoords(sx, sy: Double): Boolean;
begin
   LoadFunction(@pdfScaleCoords, 'pdfScaleCoords');
   Result := pdfScaleCoords(FInstance, sx, sy);
end;

function TPDF.SelfTest(): Boolean;
begin
   LoadFunction(@pdfSelfTest, 'pdfSelfTest');
   Result := pdfSelfTest(FInstance);
end;

function TPDF.Set3DAnnotProps(Annot: Cardinal; ActType: T3DActivationType; DeActType: T3DDeActivateType; InstType: T3DInstanceType; DeInstType: T3DDeActInstance; DisplToolbar, DisplModelTree: Boolean): Boolean;
begin
   LoadFunction(@pdfSet3DAnnotProps, 'pdfSet3DAnnotProps');
   Result := pdfSet3DAnnotProps(FInstance, Annot, Ord(ActType), Ord(DeActType), Ord(InstType), Ord(DeInstType), DisplToolbar, DisplModelTree);
end;

function TPDF.Set3DAnnotScriptA(Annot: Cardinal; const Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSet3DAnnotScriptA, 'pdfSet3DAnnotScriptA');
   Result := pdfSet3DAnnotScriptA(FInstance, Annot, PAnsiChar(Value), Length(Value));
end;

function TPDF.SetAllocBy(Value: Integer): Boolean;
begin
   LoadFunction(@pdfSetAllocBy, 'pdfSetAllocBy');
   Result := pdfSetAllocBy(FInstance, Value);
end;

function TPDF.SetAnnotBorderStyle(Handle: Cardinal; Style: TBorderStyle): Boolean;
begin
   LoadFunction(@pdfSetAnnotBorderStyle, 'pdfSetAnnotBorderStyle');
   Result := pdfSetAnnotBorderStyle(FInstance, Handle, Style);
end;

function TPDF.SetAnnotBorderWidth(Handle: Cardinal; LineWidth: Double): Boolean;
begin
   LoadFunction(@pdfSetAnnotBorderWidth, 'pdfSetAnnotBorderWidth');
   Result := pdfSetAnnotBorderWidth(FInstance, Handle, LineWidth);
end;

function TPDF.SetAnnotColor(Handle: Cardinal; ColorType: TAnnotColor; CS: TPDFColorSpace; Color: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetAnnotColor, 'pdfSetAnnotColor');
   Result := pdfSetAnnotColor(FInstance, Handle, ColorType, CS, Color);
end;

function TPDF.SetAnnotFlags(Flags: TAnnotFlags): Boolean;
begin
   LoadFunction(@pdfSetAnnotFlags, 'pdfSetAnnotFlags');
   Result := pdfSetAnnotFlags(FInstance, Flags);
end;

function TPDF.SetAnnotFlagsEx(Handle: Cardinal; Flags: Integer): Boolean;
begin
   LoadFunction(@pdfSetAnnotFlagsEx, 'pdfSetAnnotFlagsEx');
   Result := pdfSetAnnotFlagsEx(FInstance, Handle, Flags);
end;

function TPDF.SetAnnotHighlightMode(Handle: Cardinal; Mode: THighlightMode): Boolean;
begin
   LoadFunction(@pdfSetAnnotHighlightMode, 'pdfSetAnnotHighlightMode');
   Result := pdfSetAnnotHighlightMode(FInstance, Handle, Mode);
end;

function TPDF.SetAnnotIcon(Handle: Cardinal; Icon: TAnnotIcon): Boolean;
begin
   LoadFunction(@pdfSetAnnotIcon, 'pdfSetAnnotIcon');
   Result := pdfSetAnnotIcon(FInstance, Handle, Icon);
end;

function TPDF.SetAnnotLineEndStyle(Handle: Cardinal; StartStyle, EndStyle: TLineEndStyle): Boolean;
begin
   LoadFunction(@pdfSetAnnotLineEndStyle, 'pdfSetAnnotLineEndStyle');
   Result := pdfSetAnnotLineEndStyle(FInstance, Handle, StartStyle, EndStyle);
end;

function TPDF.SetAnnotOpenState(Handle: Cardinal; Open: Boolean): Boolean;
begin
   LoadFunction(@pdfSetAnnotOpenState, 'pdfSetAnnotOpenState');
   Result := pdfSetAnnotOpenState(FInstance, Handle, Open);
end;

function TPDF.SetAnnotOrFieldDate(Handle: Cardinal; IsField: Boolean; DateType: TDateType; DateTime: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetAnnotOrFieldDate, 'pdfSetAnnotOrFieldDate');
   Result := pdfSetAnnotOrFieldDate(FInstance, Handle, IsField, DateType, DateTime);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.SetAnnotString(Handle: Cardinal; StringType: TAnnotString; const Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetAnnotStringA, 'pdfSetAnnotStringA');
   Result := pdfSetAnnotStringA(FInstance, Handle, Integer(StringType), PAnsiChar(Value));
end;

function TPDF.SetAnnotString(Handle: Cardinal; StringType: TAnnotString; const Value: WideString): Boolean;
begin
   LoadFunction(@pdfSetAnnotStringW, 'pdfSetAnnotStringW');
   Result := pdfSetAnnotStringW(FInstance, Handle, Integer(StringType), PWideChar(Value));
end;

function TPDF.SetAnnotSubject(Handle: Cardinal; const Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetAnnotSubjectA, 'pdfSetAnnotSubjectA');
   Result := pdfSetAnnotSubjectA(FInstance, Handle, PAnsiChar(Value));
end;

function TPDF.SetAnnotSubject(Handle: Cardinal; const Value: WideString): Boolean;
begin
   LoadFunction(@pdfSetAnnotSubjectW, 'pdfSetAnnotSubjectW');
   Result := pdfSetAnnotSubjectW(FInstance, Handle, PWideChar(Value));
end;
{$endif}

function TPDF.SetAnnotStringA(Handle: Cardinal; StringType: TAnnotString; const Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetAnnotStringA, 'pdfSetAnnotStringA');
   Result := pdfSetAnnotStringA(FInstance, Handle, Integer(StringType), PAnsiChar(Value));
end;

function TPDF.SetAnnotStringW(Handle: Cardinal; StringType: TAnnotString; const Value: WideString): Boolean;
begin
   LoadFunction(@pdfSetAnnotStringW, 'pdfSetAnnotStringW');
   Result := pdfSetAnnotStringW(FInstance, Handle, Integer(StringType), PWideChar(Value));
end;

function TPDF.SetAnnotSubjectA(Handle: Cardinal; const Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetAnnotSubjectA, 'pdfSetAnnotSubjectA');
   Result := pdfSetAnnotSubjectA(FInstance, Handle, PAnsiChar(Value));
end;

function TPDF.SetAnnotSubjectW(Handle: Cardinal; const Value: WideString): Boolean;
begin
   LoadFunction(@pdfSetAnnotSubjectW, 'pdfSetAnnotSubjectW');
   Result := pdfSetAnnotSubjectW(FInstance, Handle, PWideChar(Value));
end;

function TPDF.SetBBox(Boundary: TPageBoundary; X0, Y0, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfSetBBox, 'pdfSetBBox');
   Result := pdfSetBBox(FInstance, Ord(Boundary), X0, Y0, Width, Height);
end;

function TPDF.SetBidiMode(Mode: TPDFBidiMode): Boolean;
begin
   LoadFunction(@pdfSetBidiMode, 'pdfSetBidiMode');
   Result := pdfSetBidiMode(FInstance, Integer(Mode));
end;

function TPDF.SetBookmarkDest(ABmk: Integer; DestType: TDestType; a, b, c, d: Double): Boolean;
begin
   LoadFunction(@pdfSetBookmarkDest, 'pdfSetBookmarkDest');
   Result := pdfSetBookmarkDest(FInstance, ABmk, Ord(DestType), a, b, c, d);
end;

function TPDF.SetBookmarkStyle(ABmk: Integer; Style: TFStyle; RGBColor: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetBookmarkStyle, 'pdfSetBookmarkStyle');
   Result := pdfSetBookmarkStyle(FInstance, ABmk, Style, RGBColor);
end;

function TPDF.SetBorderStyle(Style: TBorderStyle): Boolean;
begin
   LoadFunction(@pdfSetBorderStyle, 'pdfSetBorderStyle');
   Result := pdfSetBorderStyle(FInstance, Integer(Style));
end;

function TPDF.SetCharacterSpacing(Value: Double): Boolean;
begin
   LoadFunction(@pdfSetCharacterSpacing, 'pdfSetCharacterSpacing');
   Result := pdfSetCharacterSpacing(FInstance, Value);
end;

function TPDF.SetCheckBoxChar(CheckBoxChar: TCheckBoxChar): Boolean;
begin
   LoadFunction(@pdfSetCheckBoxChar, 'pdfSetCheckBoxChar');
   Result := pdfSetCheckBoxChar(FInstance, Ord(CheckBoxChar));
end;

function TPDF.SetCheckBoxDefState(AField: Cardinal; Checked: Boolean): Boolean;
begin
   LoadFunction(@pdfSetCheckBoxDefState, 'pdfSetCheckBoxDefState');
   Result := pdfSetCheckBoxDefState(FInstance, AField, Checked);
end;

function TPDF.SetCheckBoxState(AField: Cardinal; Checked: Boolean): Boolean;
begin
   LoadFunction(@pdfSetCheckBoxState, 'pdfSetCheckBoxState');
   Result := pdfSetCheckBoxState(FInstance, AField, Checked);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.SetCIDFont(CMapHandle: Cardinal; const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean): Integer;
begin
   LoadFunction(@pdfSetCIDFontA, 'pdfSetCIDFontA');
   Result := pdfSetCIDFontA(FInstance, CMapHandle, PAnsiChar(Name), Style, Size, Embed);
end;

function TPDF.SetCIDFont(CMapHandle: Cardinal; const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean): Integer;
begin
   LoadFunction(@pdfSetCIDFontW, 'pdfSetCIDFontW');
   Result := pdfSetCIDFontW(FInstance, CMapHandle, PWideChar(Name), Style, Size, Embed);
end;

function TPDF.SetCMapDir(const Path: AnsiString; Flags: TLoadCMapFlags): Integer;
begin
   LoadFunction(@pdfSetCMapDirA, 'pdfSetCMapDirA');
   Result := pdfSetCMapDirA(FInstance, PAnsiChar(Path), Flags);
end;

function TPDF.SetCMapDir(const Path: WideString; Flags: TLoadCMapFlags): Integer;
begin
   LoadFunction(@pdfSetCMapDirW, 'pdfSetCMapDirW');
   Result := pdfSetCMapDirW(FInstance, PWideChar(Path), Flags);
end;
{$endif}

function TPDF.SetCIDFontA(CMapHandle: Cardinal; const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean): Integer;
begin
   LoadFunction(@pdfSetCIDFontA, 'pdfSetCIDFontA');
   Result := pdfSetCIDFontA(FInstance, CMapHandle, PAnsiChar(Name), Style, Size, Embed);
end;

function TPDF.SetCIDFontW(CMapHandle: Cardinal; const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean): Integer;
begin
   LoadFunction(@pdfSetCIDFontW, 'pdfSetCIDFontW');
   Result := pdfSetCIDFontW(FInstance, CMapHandle, PWideChar(Name), Style, Size, Embed);
end;

function TPDF.SetCMapDirA(const Path: AnsiString; Flags: TLoadCMapFlags): Integer;
begin
   LoadFunction(@pdfSetCMapDirA, 'pdfSetCMapDirA');
   Result := pdfSetCMapDirA(FInstance, PAnsiChar(Path), Flags);
end;

function TPDF.SetCMapDirW(const Path: WideString; Flags: TLoadCMapFlags): Integer;
begin
   LoadFunction(@pdfSetCMapDirW, 'pdfSetCMapDirW');
   Result := pdfSetCMapDirW(FInstance, PWideChar(Path), Flags);
end;

function TPDF.SetColDefFile(EmbFile: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetColDefFile, 'pdfSetColDefFile');
   Result := pdfSetColDefFile(FInstance, EmbFile);
end;

function TPDF.SetColorMask(ImageHandle: Cardinal; Mask: PInteger; Count: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetColorMask, 'pdfSetColorMask');
   Result := pdfSetColorMask(FInstance, ImageHandle, Mask, Count);
end;

function TPDF.SetColors(Color: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetColors, 'pdfSetColors');
   Result := pdfSetColors(FInstance, Color);
end;

function TPDF.SetColorSpace(ColorSpace: TPDFColorSpace): Boolean;
begin
   LoadFunction(@pdfSetColorSpace, 'pdfSetColorSpace');
   Result := pdfSetColorSpace(FInstance, Ord(ColorSpace));
end;

function TPDF.SetColSortField(ColField: Cardinal; AscendingOrder: Boolean): Boolean;
begin
   LoadFunction(@pdfSetColSortField, 'pdfSetColSortField');
   Result := pdfSetColSortField(FInstance, ColField, AscendingOrder);
end;

function TPDF.SetCompressionFilter(Filter: TCompressionFilter): Boolean;
begin
   LoadFunction(@pdfSetCompressionFilter, 'pdfSetCompressionFilter');
   Result := pdfSetCompressionFilter(FInstance, Ord(Filter));
end;

function TPDF.SetCompressionLevel(CompressLevel: TCompressionLevel): Boolean;
begin
   LoadFunction(@pdfSetCompressionLevel, 'pdfSetCompressionLevel');
   Result := pdfSetCompressionLevel(FInstance, Ord(CompressLevel));
end;

function TPDF.SetContent(const Buffer: PAnsiChar; BufSize: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetContent, 'pdfSetContent');
   Result := pdfSetContent(FInstance, Buffer, BufSize);
end;

function TPDF.SetDateTimeFormat(TxtField: Cardinal; Fmt: TPDFDateTime): Boolean;
begin
   LoadFunction(@pdfSetDateTimeFormat, 'pdfSetDateTimeFormat');
   Result := pdfSetDateTimeFormat(FInstance, TxtField, Integer(Fmt));
end;

function TPDF.SetDefBitsPerPixel(Value: Integer): Boolean;
begin
   LoadFunction(@pdfSetDefBitsPerPixel, 'pdfSetDefBitsPerPixel');
   Result := pdfSetDefBitsPerPixel(FInstance, Value);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.SetDocInfo(DInfo: TDocumentInfo; const Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetDocInfoA, 'pdfSetDocInfoA');
   Result := pdfSetDocInfoA(FInstance, DInfo, PAnsiChar(Value));
end;

function TPDF.SetDocInfo(DInfo: TDocumentInfo; const Value: WideString): Boolean;
begin
   LoadFunction(@pdfSetDocInfoW, 'pdfSetDocInfoW');
   Result := pdfSetDocInfoW(FInstance, DInfo, PWideChar(Value));
end;

function TPDF.SetDocInfoEx(DInfo: TDocumentInfo; const Key, Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetDocInfoExA, 'pdfSetDocInfoExA');
   Result := pdfSetDocInfoExA(FInstance, DInfo, PAnsiChar(Key), PAnsiChar(Value));
end;

function TPDF.SetDocInfoEx(DInfo: TDocumentInfo; const Key: AnsiString; const Value: WideString): Boolean;
begin
   LoadFunction(@pdfSetDocInfoExW, 'pdfSetDocInfoExW');
   Result := pdfSetDocInfoExW(FInstance, DInfo, PAnsiChar(Key), PWideChar(Value));
end;
{$endif}

function TPDF.SetDocInfoA(DInfo: TDocumentInfo; const Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetDocInfoA, 'pdfSetDocInfoA');
   Result := pdfSetDocInfoA(FInstance, DInfo, PAnsiChar(Value));
end;

function TPDF.SetDocInfoW(DInfo: TDocumentInfo; const Value: WideString): Boolean;
begin
   LoadFunction(@pdfSetDocInfoW, 'pdfSetDocInfoW');
   Result := pdfSetDocInfoW(FInstance, DInfo, PWideChar(Value));
end;

function TPDF.SetDocInfoExA(DInfo: TDocumentInfo; const Key, Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetDocInfoExA, 'pdfSetDocInfoExA');
   Result := pdfSetDocInfoExA(FInstance, DInfo, PAnsiChar(Key), PAnsiChar(Value));
end;

function TPDF.SetDocInfoExW(DInfo: TDocumentInfo; const Key: AnsiString; const Value: WideString): Boolean;
begin
   LoadFunction(@pdfSetDocInfoExW, 'pdfSetDocInfoExW');
   Result := pdfSetDocInfoExW(FInstance, DInfo, PAnsiChar(Key), PWideChar(Value));
end;

function TPDF.SetDrawDirection(Direction: TDrawDirection): Boolean;
begin
   LoadFunction(@pdfSetDrawDirection, 'pdfSetDrawDirection');
   Result := pdfSetDrawDirection(FInstance, Ord(Direction));
end;

function TPDF.SetEMFFrameDPI(DPIX, DPIY: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetEMFFrameDPI, 'pdfSetEMFFrameDPI');
   Result := pdfSetEMFFrameDPI(FInstance, DPIX, DPIY);
end;

function TPDF.SetEMFPatternDistance(Value: Double): Boolean;
begin
   LoadFunction(@pdfSetEMFPatternDistance, 'pdfSetEMFPatternDistance');
   Result := pdfSetEMFPatternDistance(FInstance, Value);
end;

function TPDF.SetErrorMode(ErrMode: TErrMode): Boolean;
begin
   LoadFunction(@pdfSetErrorMode, 'pdfSetErrorMode');
   Result := pdfSetErrorMode(FInstance, ErrMode);
end;

function TPDF.SetExtColorSpace(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetExtColorSpace, 'pdfSetExtColorSpace');
   Result := pdfSetExtColorSpace(FInstance, Handle);
end;

function TPDF.SetExtFillColorSpace(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetExtFillColorSpace, 'pdfSetExtFillColorSpace');
   Result := pdfSetExtFillColorSpace(FInstance, Handle);
end;

function TPDF.SetExtGState(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetExtGState, 'pdfSetExtGState');
   Result := pdfSetExtGState(FInstance, Handle);
end;

function TPDF.SetExtStrokeColorSpace(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetExtStrokeColorSpace, 'pdfSetExtStrokeColorSpace');
   Result := pdfSetExtStrokeColorSpace(FInstance, Handle);
end;

function TPDF.SetFieldBackColor(AColor: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetFieldBackColor, 'pdfSetFieldBackColor');
   Result := pdfSetFieldBackColor(FInstance, AColor);
end;

function TPDF.SetFieldBBox(AField: Cardinal; var BBox: TPDFRect): Boolean;
begin
   LoadFunction(@pdfSetFieldBBox, 'pdfSetFieldBBox');
   Result := pdfSetFieldBBox(FInstance, AField, BBox);
end;

function TPDF.SetFieldBorderColor(AColor: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetFieldBorderColor, 'pdfSetFieldBorderColor');
   Result := pdfSetFieldBorderColor(FInstance, AColor);
end;

function TPDF.SetFieldBorderStyle(AField: Cardinal; Style: TBorderStyle): Boolean;
begin
   LoadFunction(@pdfSetFieldBorderStyle, 'pdfSetFieldBorderStyle');
   Result := pdfSetFieldBorderStyle(FInstance, AField, Integer(Style));
end;

function TPDF.SetFieldBorderWidth(AField: Cardinal; LineWidth: Double): Boolean;
begin
   LoadFunction(@pdfSetFieldBorderWidth, 'pdfSetFieldBorderWidth');
   Result := pdfSetFieldBorderWidth(FInstance, AField, LineWidth);
end;

function TPDF.SetFieldColor(AField: Cardinal; ColorType: TFieldColor; CS: TPDFColorSpace; Color: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetFieldColor, 'pdfSetFieldColor');
   Result := pdfSetFieldColor(FInstance, AField, Integer(ColorType), Integer(CS), Color);
end;

function TPDF.SetFieldExpValue(AField, ValIndex: Cardinal; const Value, ExpValue: AnsiString; Selected: Boolean): Boolean;
begin
   LoadFunction(@pdfSetFieldExpValue, 'pdfSetFieldExpValue');
   Result := pdfSetFieldExpValue(FInstance, AField, ValIndex, PAnsiChar(Value), PAnsiChar(ExpValue), Selected);
end;

function TPDF.SetFieldExpValueEx(AField, ValIndex: Cardinal; Selected, DefSelected: Boolean): Boolean;
begin
   LoadFunction(@pdfSetFieldExpValueEx, 'pdfSetFieldExpValueEx');
   Result := pdfSetFieldExpValueEx(FInstance, AField, ValIndex, Selected, DefSelected);
end;

function TPDF.SetFieldFlags(AField: Cardinal; Flags: TFieldFlags; Reset: Boolean): Boolean;
begin
   LoadFunction(@pdfSetFieldFlags, 'pdfSetFieldFlags');
   Result := pdfSetFieldFlags(FInstance, AField, Integer(Flags), Reset);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.SetFieldFont(Field: Cardinal; const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfSetFieldFontA, 'pdfSetFieldFontA');
   Result := pdfSetFieldFontA(FInstance, Field, PAnsiChar(Name), Style, Size, Embed, CP);
end;

function TPDF.SetFieldFont(Field: Cardinal; const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfSetFieldFontW, 'pdfSetFieldFontW');
   Result := pdfSetFieldFontW(FInstance, Field, PWideChar(Name), Style, Size, Embed, CP);
end;

{$else}

function TPDF.SetFieldFontA(Field: Cardinal; const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfSetFieldFontA, 'pdfSetFieldFontA');
   Result := pdfSetFieldFontA(FInstance, Field, PAnsiChar(Name), Style, Size, Embed, CP);
end;

function TPDF.SetFieldFontW(Field: Cardinal; const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfSetFieldFontW, 'pdfSetFieldFontW');
   Result := pdfSetFieldFontW(FInstance, Field, PWideChar(Name), Style, Size, Embed, CP);
end;
{$endif}

function TPDF.SetFieldFontEx(Field, Handle: Cardinal; FontSize: Double): Boolean;
begin
   LoadFunction(@pdfSetFieldFontEx, 'pdfSetFieldFontEx');
   Result := pdfSetFieldFontEx(FInstance, Field, Handle, FontSize);
end;

function TPDF.SetFieldFontSize(AField: Cardinal; FontSize: double): Boolean;
begin
   LoadFunction(@pdfSetFieldFontSize, 'pdfSetFieldFontSize');
   Result := pdfSetFieldFontSize(FInstance, AField, FontSize);
end;

function TPDF.SetFieldHighlightMode(AField: Cardinal; Mode: THighlightMode): Boolean;
begin
   LoadFunction(@pdfSetFieldHighlightMode, 'pdfSetFieldHighlightMode');
   Result := pdfSetFieldHighlightMode(FInstance, AField, Integer(Mode));
end;

function TPDF.SetFieldIndex(AField, Index: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetFieldIndex, 'pdfSetFieldIndex');
   Result := pdfSetFieldIndex(FInstance, AField, Index);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.SetFieldMapName(AField: Cardinal; const Name: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetFieldMapNameA, 'pdfSetFieldMapNameA');
   Result := pdfSetFieldMapNameA(FInstance, AField, PAnsiChar(Name));
end;

function TPDF.SetFieldMapName(AField: Cardinal; const Name: WideString): Boolean;
begin
   LoadFunction(@pdfSetFieldMapNameW, 'pdfSetFieldMapNameW');
   Result := pdfSetFieldMapNameW(FInstance, AField, PWideChar(Name));
end;
{$endif}

function TPDF.SetFieldMapNameA(AField: Cardinal; const Name: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetFieldMapNameA, 'pdfSetFieldMapNameA');
   Result := pdfSetFieldMapNameA(FInstance, AField, PAnsiChar(Name));
end;

function TPDF.SetFieldMapNameW(AField: Cardinal; const Name: WideString): Boolean;
begin
   LoadFunction(@pdfSetFieldMapNameW, 'pdfSetFieldMapNameW');
   Result := pdfSetFieldMapNameW(FInstance, AField, PWideChar(Name));
end;

function TPDF.SetFieldName(AField: Cardinal; const NewName: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetFieldName, 'pdfSetFieldName');
   Result := pdfSetFieldName(FInstance, AField, PAnsiChar(NewName));
end;

function TPDF.SetFieldOrientation(AField: Cardinal; Value: Integer): Boolean;
begin
   LoadFunction(@pdfSetFieldOrientation, 'pdfSetFieldOrientation');
   Result := pdfSetFieldOrientation(FInstance, AField, Value);
end;

function TPDF.SetFieldTextAlign(AField: Cardinal; Align: TTextAlign): Boolean;
begin
   LoadFunction(@pdfSetFieldTextAlign, 'pdfSetFieldTextAlign');
   Result := pdfSetFieldTextAlign(FInstance, AField, Integer(Align));
end;

function TPDF.SetFieldTextColor(Color: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetFieldTextColor, 'pdfSetFieldTextColor');
   Result := pdfSetFieldTextColor(FInstance, Color);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.SetFieldToolTip(AField: Cardinal; const Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetFieldToolTipA, 'pdfSetFieldToolTipA');
   Result := pdfSetFieldToolTipA(FInstance, AField, PAnsiChar(Value));
end;

function TPDF.SetFieldToolTip(AField: Cardinal; const Value: WideString): Boolean;
begin
   LoadFunction(@pdfSetFieldToolTipW, 'pdfSetFieldToolTipW');
   Result := pdfSetFieldToolTipW(FInstance, AField, PWideChar(Value));
end;
{$endif}

function TPDF.SetFieldToolTipA(AField: Cardinal; const Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetFieldToolTipA, 'pdfSetFieldToolTipA');
   Result := pdfSetFieldToolTipA(FInstance, AField, PAnsiChar(Value));
end;

function TPDF.SetFieldToolTipW(AField: Cardinal; const Value: WideString): Boolean;
begin
   LoadFunction(@pdfSetFieldToolTipW, 'pdfSetFieldToolTipW');
   Result := pdfSetFieldToolTipW(FInstance, AField, PWideChar(Value));
end;

function TPDF.SetFillColor(Color: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetFillColor, 'pdfSetFillColor');
   Result := pdfSetFillColor(FInstance, Color);
end;

function TPDF.SetFillColorEx(const Color: Array of Byte; NumComponents: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetFillColorEx, 'pdfSetFillColorEx');
   Result := pdfSetFillColorEx(FInstance, @Color[0], NumComponents);
end;

function TPDF.SetFillColorF(const Color: PSingle; NumComponents: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetFillColorF, 'pdfSetFillColorF');
   Result := pdfSetFillColorF(FInstance, Color, NumComponents);
end;

procedure TPDF.SetFillColorSpace(CS: TPDFColorSpace);
begin
   LoadFunction(@pdfSetFillColorSpace, 'pdfSetFillColorSpace');
   pdfSetFillColorSpace(FInstance, Integer(CS));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.SetFont(const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfSetFontA, 'pdfSetFontA');
   Result := pdfSetFontA(FInstance, PAnsiChar(Name), Style, Size, Embed, CP);
end;

function TPDF.SetFont(const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfSetFontW, 'pdfSetFontW');
   Result := pdfSetFontW(FInstance, PWideChar(Name), Style, Size, Embed, CP);
end;

function TPDF.SetFontEx(const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfSetFontExA, 'pdfSetFontExA');
   Result := pdfSetFontExA(FInstance, PAnsiChar(Name), Style, Size, Embed, CP);
end;

function TPDF.SetFontEx(const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfSetFontExW, 'pdfSetFontExW');
   Result := pdfSetFontExW(FInstance, PWideChar(Name), Style, Size, Embed, CP);
end;
{$endif}

function TPDF.SetFontA(const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage = cp1252): Integer;
begin
   LoadFunction(@pdfSetFontA, 'pdfSetFontA');
   Result := pdfSetFontA(FInstance, PAnsiChar(Name), Style, Size, Embed, CP);
end;

function TPDF.SetFontW(const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage = cp1252): Integer;
begin
   LoadFunction(@pdfSetFontW, 'pdfSetFontW');
   Result := pdfSetFontW(FInstance, PWideChar(Name), Style, Size, Embed, CP);
end;

function TPDF.SetFontExA(const Name: AnsiString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfSetFontExA, 'pdfSetFontExA');
   Result := pdfSetFontExA(FInstance, PAnsiChar(Name), Style, Size, Embed, CP);
end;

function TPDF.SetFontExW(const Name: WideString; Style: TFStyle; Size: Double; Embed: Boolean; CP: TCodepage): Integer;
begin
   LoadFunction(@pdfSetFontExW, 'pdfSetFontExW');
   Result := pdfSetFontExW(FInstance, PWideChar(Name), Style, Size, Embed, CP);
end;

function TPDF.SetFontOrigin(Origin: TOrigin): Boolean;
begin
   LoadFunction(@pdfSetFontOrigin, 'pdfSetFontOrigin');
   Result := pdfSetFontOrigin(FInstance, Integer(Origin));
end;

procedure TPDF.SetFontSearchOrder(Order: PFontBaseType);
begin
   LoadFunction(@pdfSetFontSearchOrder, 'pdfSetFontSearchOrder');
   pdfSetFontSearchOrder(FInstance, Order);
end;

procedure TPDF.SetFontSearchOrderEx(S1, S2, S3, S4: TFontBaseType);
begin
   LoadFunction(@pdfSetFontSearchOrderEx, 'pdfSetFontSearchOrderEx');
   pdfSetFontSearchOrderEx(FInstance, S1, S2, S3, S4);
end;

function TPDF.SetFontSelMode(Mode: TFontSelMode): Boolean;
begin
   LoadFunction(@pdfSetFontSelMode, 'pdfSetFontSelMode');
   Result := pdfSetFontSelMode(FInstance, Ord(Mode));
end;

function TPDF.SetFontWeight(Weight: Integer): Boolean;
begin
   LoadFunction(@pdfSetFontWeight, 'pdfSetFontWeight');
   Result := pdfSetFontWeight(FInstance, Weight);
end;

function TPDF.SetGStateFlags(Flags: TGStateFlags; Reset: Boolean): Boolean;
begin
   LoadFunction(@pdfSetGStateFlags, 'pdfSetGStateFlags');
   Result := pdfSetGStateFlags(FInstance, Flags, Reset);
end;

function TPDF.SetIconColor(Color: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetIconColor, 'pdfSetIconColor');
   Result := pdfSetIconColor(FInstance, Color);
end;

function TPDF.SetImportFlags(Flags: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetImportFlags, 'pdfSetImportFlags');
   Result := pdfSetImportFlags(FInstance, Flags);
end;

function TPDF.SetImportFlags2(Flags: TImportFlags2): Boolean;
begin
   LoadFunction(@pdfSetImportFlags2, 'pdfSetImportFlags2');
   Result := pdfSetImportFlags2(FInstance, Flags);
end;

function TPDF.SetItalicAngle(Angle: Double): Boolean;
begin
   LoadFunction(@pdfSetItalicAngle, 'pdfSetItalicAngle');
   Result := pdfSetItalicAngle(FInstance, Angle);
end;

function TPDF.SetJPEGQuality(Value: Integer): Boolean;
begin
   LoadFunction(@pdfSetJPEGQuality, 'pdfSetJPEGQuality');
   Result := pdfSetJPEGQuality(FInstance, Value);
end;

function TPDF.SetLanguage(const ISOTag: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetLanguage, 'pdfSetLanguage');
   Result := pdfSetLanguage(FInstance, PAnsiChar(ISOTag));
end;

function TPDF.SetLeading(Value: Double): Boolean;
begin
   LoadFunction(@pdfSetLeading, 'pdfSetLeading');
   Result := pdfSetLeading(FInstance, Value);
end;

function TPDF.SetLicenseKey(const Key: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetLicenseKey, 'pdfSetLicenseKey');
   Result := pdfSetLicenseKey(FInstance, PAnsiChar(Key));
end;

function TPDF.SetLineAnnotParms(Handle: Cardinal; FontHandle: Integer; FontSize: Double; const Parms: PLineAnnotParms): Boolean;
begin
   LoadFunction(@pdfSetLineAnnotParms, 'pdfSetLineAnnotParms');
   Result := pdfSetLineAnnotParms(FInstance, Handle, FontHandle, FontSize, Parms);
end;

function TPDF.SetLineCapStyle(Style: TLineCapStyle): Boolean;
begin
   LoadFunction(@pdfSetLineCapStyle, 'pdfSetLineCapStyle');
   Result := pdfSetLineCapStyle(FInstance, Integer(Style));
end;

function TPDF.SetLineDashPattern(const Dash: AnsiString; Phase: Integer): Boolean;
begin
   LoadFunction(@pdfSetLineDashPattern, 'pdfSetLineDashPattern');
   Result := pdfSetLineDashPattern(FInstance, PAnsiChar(Dash), Phase);
end;

function TPDF.SetLineDashPatternEx(const Dash: Array of Double; NumValues: Cardinal; Phase: Integer): Boolean;
begin
   LoadFunction(@pdfSetLineDashPatternEx, 'pdfSetLineDashPatternEx');
   Result := pdfSetLineDashPatternEx(FInstance, Pointer(@Dash), NumValues, Phase);
end;

function TPDF.SetLineJoinStyle(Style: TLineJoinStyle): Boolean;
begin
   LoadFunction(@pdfSetLineJoinStyle, 'pdfSetLineJoinStyle');
   Result := pdfSetLineJoinStyle(FInstance, Integer(Style));
end;

function TPDF.SetLineWidth(Value: Double): Boolean;
begin
   LoadFunction(@pdfSetLineWidth, 'pdfSetLineWidth');
   Result := pdfSetLineWidth(FInstance, Value);
end;

function TPDF.SetLinkHighlightMode(Mode: THighlightMode): Boolean;
begin
   LoadFunction(@pdfSetLinkHighlightMode, 'pdfSetLinkHighlightMode');
   Result := pdfSetLinkHighlightMode(FInstance, Ord(Mode));
end;

function TPDF.SetListFont(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetListFont, 'pdfSetListFont');
   Result := pdfSetListFont(FInstance, Handle);
end;

function TPDF.SetMatrix(var Matrix: TCTM): Boolean;
begin
   LoadFunction(@pdfSetMatrix, 'pdfSetMatrix');
   Result := pdfSetMatrix(FInstance, Matrix);
end;

procedure TPDF.SetMaxErrLogMsgCount(Value: Cardinal);
begin
   LoadFunction(@pdfSetMaxErrLogMsgCount, 'pdfSetMaxErrLogMsgCount');
   pdfSetMaxErrLogMsgCount(FInstance, Value);
end;

function TPDF.SetMaxFieldLen(TxtField: Cardinal; MaxLen: Integer): Boolean;
begin
   LoadFunction(@pdfSetMaxFieldLen, 'pdfSetMaxFieldLen');
   Result := pdfSetMaxFieldLen(FInstance, TxtField, MaxLen);
end;

function TPDF.SetMetaConvFlags(Flags: TMetaFlags): Boolean;
begin
   LoadFunction(@pdfSetMetaConvFlags, 'pdfSetMetaConvFlags');
   Result := pdfSetMetaConvFlags(FInstance, Flags);
end;

function TPDF.SetMetadata(ObjType: TMetadataObj; Handle: Integer; const Buffer: Pointer; BufSize: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetMetadata, 'pdfSetMetadata');
   Result := pdfSetMetadata(FInstance, ObjType, Handle, Buffer, BufSize);
end;

function TPDF.SetMiterLimit(Value: Double): Boolean;
begin
   LoadFunction(@pdfSetMiterLimit, 'pdfSetMiterLimit');
   Result := pdfSetMiterLimit(FInstance, Value);
end;

function TPDF.SetNeedAppearance(Value: Boolean): Boolean;
begin
   LoadFunction(@pdfSetNeedAppearance, 'pdfSetNeedAppearance');
   Result := pdfSetNeedAppearance(FInstance, Value);
end;

function TPDF.SetNumberFormat(TxtField: Cardinal; Sep: TDecSeparator; DecPlaces: Cardinal; NegStyle: TNegativeStyle; const CurrStr: AnsiString; Prepend: Boolean): Boolean;
begin
   LoadFunction(@pdfSetNumberFormat, 'pdfSetNumberFormat');
   Result := pdfSetNumberFormat(FInstance, TxtField, Integer(Sep), DecPlaces, Integer(NegStyle), PAnsiChar(CurrStr), Prepend);
end;

function TPDF.SetOCGContUsage(Handle: Cardinal; var Value: TPDFOCGContUsage): Boolean;
begin
   LoadFunction(@pdfSetOCGContUsage, 'pdfSetOCGContUsage');
   Result := pdfSetOCGContUsage(FInstance, Handle, Value);
end;

function TPDF.SetOCGState(Handle: Cardinal; Visible, SaveState: Boolean): Boolean;
begin
   LoadFunction(@pdfSetOCGState, 'pdfSetOCGState');
   Result := pdfSetOCGState(FInstance, Handle, Visible, SaveState);
end;

function TPDF.SetOnErrorProc(const Data: Pointer; ErrProc: TErrorProc): Boolean;
begin
   LoadFunction(@pdfSetOnErrorProc, 'pdfSetOnErrorProc');
   Result := pdfSetOnErrorProc(FInstance, Data, ErrProc);
end;

function TPDF.SetOnPageBreakProc(const Data: Pointer; OnBreakProc: TOnPageBreakProc): Boolean;
begin
   LoadFunction(@pdfSetOnPageBreakProc, 'pdfSetOnPageBreakProc');
   Result := pdfSetOnPageBreakProc(FInstance, Data, OnBreakProc);
end;

function TPDF.SetOpacity(Value: Double): Boolean;
begin
   LoadFunction(@pdfSetOpacity, 'pdfSetOpacity');
   Result := pdfSetOpacity(FInstance, Value);
end;

function TPDF.SetOrientation(Value: Integer): Boolean;
begin
   LoadFunction(@pdfSetOrientation, 'pdfSetOrientation');
   Result := pdfSetOrientation(FInstance, Value);
end;

function TPDF.SetOrientationEx(Value: Integer): Boolean;
begin
   LoadFunction(@pdfSetOrientationEx, 'pdfSetOrientationEx');
   Result := pdfSetOrientationEx(FInstance, Value);
end;

function TPDF.SetPageCoords(PageCoords: TPageCoord): Boolean;
begin
   LoadFunction(@pdfSetPageCoords, 'pdfSetPageCoords');
   Result := pdfSetPageCoords(FInstance, Ord(PageCoords));
end;

function TPDF.SetPageFormat(Value: TPageFormat): Boolean;
begin
   LoadFunction(@pdfSetPageFormat, 'pdfSetPageFormat');
   Result := pdfSetPageFormat(FInstance, Integer(Value));
end;

function TPDF.SetPageHeight(Value: Double): Boolean;
begin
   LoadFunction(@pdfSetPageHeight, 'pdfSetPageHeight');
   Result := pdfSetPageHeight(FInstance, Value);
end;

function TPDF.SetPageLayout(Layout: TPageLayout): Boolean;
begin
   LoadFunction(@pdfSetPageLayout, 'pdfSetPageLayout');
   if Layout = plDefault then
      Result := pdfSetPageLayout(FInstance, -1)
   else
      Result := pdfSetPageLayout(FInstance, Ord(Layout));
end;

function TPDF.SetPageMode(Mode: TPageMode): Boolean;
begin
   LoadFunction(@pdfSetPageMode, 'pdfSetPageMode');
   Result := pdfSetPageMode(FInstance, Ord(Mode));
end;

function TPDF.SetPageWidth(Value: Double): Boolean;
begin
   LoadFunction(@pdfSetPageWidth, 'pdfSetPageWidth');
   Result := pdfSetPageWidth(FInstance, Value);
end;

function TPDF.SetPDFVersion(Version: TPDFVersion): Boolean;
begin
   LoadFunction(@pdfSetPDFVersion, 'pdfSetPDFVersion');
   Result := pdfSetPDFVersion(FInstance, Ord(Version));
end;

function TPDF.SetPrintSettings(Mode: TDuplexMode; PickTrayByPDFSize: Integer; NumCopies: Cardinal; PrintScaling: TPrintScaling; const PrintRanges: Array of Cardinal; NumRanges: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetPrintSettings, 'pdfSetPrintSettings');
   Result := pdfSetPrintSettings(FInstance, Mode, PickTrayByPDFSize, NumCopies, PrintScaling, @PrintRanges, NumRanges);
end;

function TPDF.SetProgressProc(const Data: Pointer; InitProgressProc: TInitProgressProc; ProgressProc: TProgressProc): Boolean;
begin
   LoadFunction(@pdfSetProgressProc, 'pdfSetProgressProc');
   Result := pdfSetProgressProc(FInstance, Data, InitProgressProc, ProgressProc);
end;

function TPDF.SetResolution(Value: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetResolution, 'pdfSetResolution');
   Result := pdfSetResolution(FInstance, Value);
end;

function TPDF.SetSaveNewImageFormat(Value: Boolean): Boolean;
begin
   LoadFunction(@pdfSetSaveNewImageFormat, 'pdfSetSaveNewImageFormat');
   Result := pdfSetSaveNewImageFormat(FInstance, Value);
end;

function TPDF.SetSeparationInfo(Handle: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetSeparationInfo, 'pdfSetSeparationInfo');
   Result := pdfSetSeparationInfo(FInstance, Handle);
end;

function TPDF.SetStrokeColor(Color: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetStrokeColor, 'pdfSetStrokeColor');
   Result := pdfSetStrokeColor(FInstance, Color);
end;

function TPDF.SetStrokeColorEx(const Color: Array of Byte; NumComponents: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetStrokeColorEx, 'pdfSetStrokeColorEx');
   Result := pdfSetStrokeColorEx(FInstance, @Color[0], NumComponents);
end;

function TPDF.SetStrokeColorF(const Color: PSingle; NumComponents: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetStrokeColorF, 'pdfSetStrokeColorF');
   Result := pdfSetStrokeColorF(FInstance, Color, NumComponents);
end;

procedure TPDF.SetStrokeColorSpace(CS: TPDFColorSpace);
begin
   LoadFunction(@pdfSetStrokeColorSpace, 'pdfSetStrokeColorSpace');
   pdfSetStrokeColorSpace(FInstance, Integer(CS));
end;

function TPDF.SetTabLen(TabLen: Integer): Boolean;
begin
   LoadFunction(@pdfSetTabLen, 'pdfSetTabLen');
   Result := pdfSetTabLen(FInstance, TabLen);
end;

function TPDF.SetTextDrawMode(Mode: TDrawMode): Boolean;
begin
   LoadFunction(@pdfSetTextDrawMode, 'pdfSetTextDrawMode');
   Result := pdfSetTextDrawMode(FInstance, Ord(Mode));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.SetTextFieldValue(Field: Cardinal; const Value, DefValue: AnsiString; Align: TTextAlign): Boolean;
begin
   LoadFunction(@pdfSetTextFieldValueA, 'pdfSetTextFieldValueA');
   Result := pdfSetTextFieldValueA(FInstance, Field, PAnsiChar(Value), PAnsiChar(DefValue), Align);
end;

function TPDF.SetTextFieldValue(Field: Cardinal; const Value, DefValue: WideString; Align: TTextAlign): Boolean;
begin
   LoadFunction(@pdfSetTextFieldValueW, 'pdfSetTextFieldValueW');
   Result := pdfSetTextFieldValueW(FInstance, Field, PWideChar(Value), PWideChar(DefValue), Align);
end;

function TPDF.SetTextFieldValueEx(Field: Cardinal; const Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetTextFieldValueExA, 'pdfSetTextFieldValueExA');
   Result := pdfSetTextFieldValueExA(FInstance, Field, PAnsiChar(Value));
end;

function TPDF.SetTextFieldValueEx(Field: Cardinal; const Value: WideString): Boolean;
begin
   LoadFunction(@pdfSetTextFieldValueExW, 'pdfSetTextFieldValueExW');
   Result := pdfSetTextFieldValueExW(FInstance, Field, PWideChar(Value));
end;
{$endif}

function TPDF.SetTextFieldValueA(Field: Cardinal; const Value, DefValue: AnsiString; Align: TTextAlign): Boolean;
begin
   LoadFunction(@pdfSetTextFieldValueA, 'pdfSetTextFieldValueA');
   Result := pdfSetTextFieldValueA(FInstance, Field, PAnsiChar(Value), PAnsiChar(DefValue), Align);
end;

function TPDF.SetTextFieldValueW(Field: Cardinal; const Value, DefValue: WideString; Align: TTextAlign): Boolean;
begin
   LoadFunction(@pdfSetTextFieldValueW, 'pdfSetTextFieldValueW');
   Result := pdfSetTextFieldValueW(FInstance, Field, PWideChar(Value), PWideChar(DefValue), Align);
end;

function TPDF.SetTextFieldValueExA(Field: Cardinal; const Value: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetTextFieldValueExA, 'pdfSetTextFieldValueExA');
   Result := pdfSetTextFieldValueExA(FInstance, Field, PAnsiChar(Value));
end;

function TPDF.SetTextFieldValueExW(Field: Cardinal; const Value: WideString): Boolean;
begin
   LoadFunction(@pdfSetTextFieldValueExW, 'pdfSetTextFieldValueExW');
   Result := pdfSetTextFieldValueExW(FInstance, Field, PWideChar(Value));
end;

function TPDF.SetTextRect(PosX, PosY, Width, Height: Double): Boolean;
begin
   LoadFunction(@pdfSetTextRect, 'pdfSetTextRect');
   Result := pdfSetTextRect(FInstance, PosX, PosY, Width, Height);
end;

function TPDF.SetTextRise(Value: Double): Boolean;
begin
   LoadFunction(@pdfSetTextRise, 'pdfSetTextRise');
   Result := pdfSetTextRise(FInstance, Value);
end;

function TPDF.SetTextScaling(Value: Double): Boolean;
begin
   LoadFunction(@pdfSetTextScaling, 'pdfSetTextScaling');
   Result := pdfSetTextScaling(FInstance, Value);
end;

function TPDF.SetTransparentColor(AColor: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetTransparentColor, 'pdfSetTransparentColor');
   Result := pdfSetTransparentColor(FInstance, AColor);
end;

procedure TPDF.SetTrapped(Value: Boolean);
begin
   LoadFunction(@pdfSetTrapped, 'pdfSetTrapped');
   pdfSetTrapped(FInstance, Value);
end;

function TPDF.SetUseExactPwd(Value: Boolean): Boolean;
begin
   LoadFunction(@pdfSetUseExactPwd, 'pdfSetUseExactPwd');
   Result := pdfSetUseExactPwd(FInstance, Ord(Value));
end;

function TPDF.SetUseGlobalImpFiles(Value: Boolean): Boolean;
begin
   LoadFunction(@pdfSetUseGlobalImpFiles, 'pdfSetUseGlobalImpFiles');
   Result := pdfSetUseGlobalImpFiles(FInstance, Value);
end;

function TPDF.SetUserUnit(Value: Single): Boolean;
begin
   LoadFunction(@pdfSetUserUnit, 'pdfSetUserUnit');
   Result := pdfSetUserUnit(FInstance, Value);
end;

function TPDF.SetUseStdFonts(Value: Boolean): Boolean;
begin
   LoadFunction(@pdfSetUseStdFonts, 'pdfSetUseStdFonts');
   Result := pdfSetUseStdFonts(FInstance, Value);
end;

function TPDF.SetUseSwapFile(SwapContents: Boolean; SwapLimit: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetUseSwapFile, 'pdfSetUseSwapFile');
   Result := pdfSetUseSwapFile(FInstance, SwapContents, SwapLimit);
end;

function TPDF.SetUseSwapFileEx(SwapContents: Boolean; SwapLimit: Cardinal; const SwapDir: AnsiString): Boolean;
begin
   LoadFunction(@pdfSetUseSwapFileEx, 'pdfSetUseSwapFileEx');
   Result := pdfSetUseSwapFileEx(FInstance, SwapContents, SwapLimit, PAnsiChar(SwapDir));
end;

function TPDF.SetUseSystemFonts(Value: Boolean): Boolean;
begin
   LoadFunction(@pdfSetUseSystemFonts, 'pdfSetUseSystemFonts');
   Result := pdfSetUseSystemFonts(FInstance, Value);
end;

function TPDF.SetUseTransparency(Value: Boolean): Boolean;
begin
   LoadFunction(@pdfSetUseTransparency, 'pdfSetUseTransparency');
   Result := pdfSetUseTransparency(FInstance, Value);
end;

function TPDF.SetUseVisibleCoords(Value: Boolean): Boolean;
begin
   LoadFunction(@pdfSetUseVisibleCoords, 'pdfSetUseVisibleCoords');
   Result := pdfSetUseVisibleCoords(FInstance, Value);
end;

function TPDF.SetViewerPreferences(Value: TViewerPreference; AddVal: TViewPrefAddVal): Boolean;
begin
   LoadFunction(@pdfSetViewerPreferences, 'pdfSetViewerPreferences');
   Result := pdfSetViewerPreferences(FInstance, Value, AddVal);
end;

function TPDF.SetWMFDefExtent(Width, Height: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetWMFDefExtent, 'pdfSetWMFDefExtent');
   Result := pdfSetWMFDefExtent(FInstance, Width, Height);
end;

function TPDF.SetWMFPixelPerInch(Value: Cardinal): Boolean;
begin
   LoadFunction(@pdfSetWMFPixelPerInch, 'pdfSetWMFPixelPerInch');
   Result := pdfSetWMFPixelPerInch(FInstance, Value);
end;

function TPDF.SetWordSpacing(Value: Double): Boolean;
begin
   LoadFunction(@pdfSetWordSpacing, 'pdfSetWordSpacing');
   Result := pdfSetWordSpacing(FInstance, Value);
end;

function TPDF.SkewCoords(alpha, beta, OriginX, OriginY: Double): Boolean;
begin
   LoadFunction(@pdfSkewCoords, 'pdfSkewCoords');
   Result := pdfSkewCoords(FInstance, alpha, beta, OriginX, OriginY);
end;

function TPDF.SortFieldsByIndex(): Boolean;
begin
   LoadFunction(@pdfSortFieldsByIndex, 'pdfSortFieldsByIndex');
   Result := pdfSortFieldsByIndex(FInstance);
end;

function TPDF.SortFieldsByName(): Boolean;
begin
   LoadFunction(@pdfSortFieldsByName, 'pdfSortFieldsByName');
   Result := pdfSortFieldsByName(FInstance);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.SquareAnnot(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: AnsiString): Integer;
begin
   LoadFunction(@pdfSquareAnnotA, 'pdfSquareAnnotA');
   Result := pdfSquareAnnotA(FInstance, PosX, PosY, Width, Height, LineWidth, FillColor, StrokeColor, CS, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Comment));
end;

function TPDF.SquareAnnot(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: WideString): Integer;
begin
   LoadFunction(@pdfSquareAnnotW, 'pdfSquareAnnotW');
   Result := pdfSquareAnnotW(FInstance, PosX, PosY, Width, Height, LineWidth, FillColor, StrokeColor, CS, PWideChar(Author), PWideChar(Subject), PWideChar(Comment));
end;
{$endif}

function TPDF.SquareAnnotA(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: AnsiString): Integer;
begin
   LoadFunction(@pdfSquareAnnotA, 'pdfSquareAnnotA');
   Result := pdfSquareAnnotA(FInstance, PosX, PosY, Width, Height, LineWidth, FillColor, StrokeColor, CS, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Comment));
end;

function TPDF.SquareAnnotW(PosX, PosY, Width, Height, LineWidth: Double; FillColor, StrokeColor: Cardinal; CS: TPDFColorSpace; const Author, Subject, Comment: WideString): Integer;
begin
   LoadFunction(@pdfSquareAnnotW, 'pdfSquareAnnotW');
   Result := pdfSquareAnnotW(FInstance, PosX, PosY, Width, Height, LineWidth, FillColor, StrokeColor, CS, PWideChar(Author), PWideChar(Subject), PWideChar(Comment));
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.StampAnnot(SubType: TRubberStamp; PosX, PosY, Width, Height: Double; const Author, Subject, Comment: AnsiString): Integer;
begin
   LoadFunction(@pdfStampAnnotA, 'pdfStampAnnotA');
   Result := pdfStampAnnotA(FInstance, SubType, PosX, PosY, Width, Height, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Comment));
end;

function TPDF.StampAnnot(SubType: TRubberStamp; PosX, PosY, Width, Height: Double; const Author, Subject, Comment: WideString): Integer;
begin
   LoadFunction(@pdfStampAnnotW, 'pdfStampAnnotW');
   Result := pdfStampAnnotW(FInstance, SubType, PosX, PosY, Width, Height, PWideChar(Author), PWideChar(Subject), PWideChar(Comment));
end;
{$endif}

function TPDF.StampAnnotA(SubType: TRubberStamp; PosX, PosY, Width, Height: Double; const Author, Subject, Comment: AnsiString): Integer;
begin
   LoadFunction(@pdfStampAnnotA, 'pdfStampAnnotA');
   Result := pdfStampAnnotA(FInstance, SubType, PosX, PosY, Width, Height, PAnsiChar(Author), PAnsiChar(Subject), PAnsiChar(Comment));
end;

function TPDF.StampAnnotW(SubType: TRubberStamp; PosX, PosY, Width, Height: Double; const Author, Subject, Comment: WideString): Integer;
begin
   LoadFunction(@pdfStampAnnotW, 'pdfStampAnnotW');
   Result := pdfStampAnnotW(FInstance, SubType, PosX, PosY, Width, Height, PWideChar(Author), PWideChar(Subject), PWideChar(Comment));
end;

function TPDF.StrokePath(): Boolean;
begin
   LoadFunction(@pdfStrokePath, 'pdfStrokePath');
   Result := pdfStrokePath(FInstance);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.TestGlyphs(FontHandle: Integer; const Text: AnsiString): Integer;
begin
   LoadFunction(@pdfTestGlyphsExA, 'pdfTestGlyphsExA');
   Result := pdfTestGlyphsExA(FInstance, FontHandle, PAnsiChar(Text), Length(Text));
end;

function TPDF.TestGlyphs(FontHandle: Integer; const Text: WideString): Integer;
begin
   LoadFunction(@pdfTestGlyphsExW, 'pdfTestGlyphsExW');
   Result := pdfTestGlyphsExW(FInstance, FontHandle, PWideChar(Text), Length(Text));
end;

function TPDF.TestGlyphsEx(FontHandle: Integer; const Text: AnsiString; Len: Cardinal): Integer;
begin
   if Len > Cardinal(Length(Text)) then Len := Length(Text);
   LoadFunction(@pdfTestGlyphsExA, 'pdfTestGlyphsExA');
   Result := pdfTestGlyphsExA(FInstance, FontHandle, PAnsiChar(Text), Len);
end;

function TPDF.TestGlyphsEx(FontHandle: Integer; const Text: WideString; Len: Cardinal): Integer;
begin
   if Len > Cardinal(Length(Text)) then Len := Length(Text);
   LoadFunction(@pdfTestGlyphsExW, 'pdfTestGlyphsExW');
   Result := pdfTestGlyphsExW(FInstance, FontHandle, PWideChar(Text), Len);
end;

{$else}

function TPDF.TestGlyphsA(FontHandle: Integer; const Text: AnsiString): Integer;
begin
   LoadFunction(@pdfTestGlyphsExA, 'pdfTestGlyphsExA');
   Result := pdfTestGlyphsExA(FInstance, FontHandle, PAnsiChar(Text), Length(Text));
end;

function TPDF.TestGlyphsW(FontHandle: Integer; const Text: WideString): Integer;
begin
   LoadFunction(@pdfTestGlyphsExW, 'pdfTestGlyphsExW');
   Result := pdfTestGlyphsExW(FInstance, FontHandle, PWideChar(Text), Length(Text));
end;

function TPDF.TestGlyphsExA(FontHandle: Integer; const Text: AnsiString; Len: Cardinal): Integer;
begin
   if Len > Cardinal(Length(Text)) then Len := Length(Text);
   LoadFunction(@pdfTestGlyphsExA, 'pdfTestGlyphsExA');
   Result := pdfTestGlyphsExA(FInstance, FontHandle, PAnsiChar(Text), Len);
end;

function TPDF.TestGlyphsExW(FontHandle: Integer; const Text: WideString; Len: Cardinal): Integer;
begin
   if Len > Cardinal(Length(Text)) then Len := Length(Text);
   LoadFunction(@pdfTestGlyphsExW, 'pdfTestGlyphsExW');
   Result := pdfTestGlyphsExW(FInstance, FontHandle, PWideChar(Text), Len);
end;
{$endif}

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.TextAnnot(PosX, PosY, Width, Height: Double; const Author, Text: AnsiString; Icon: TAnnotIcon; Open: Boolean): Integer;
begin
   LoadFunction(@pdfTextAnnotA, 'pdfTextAnnotA');
   Result := pdfTextAnnotA(FInstance, PosX, PosY, Width, Height, PAnsiChar(Author), PAnsiChar(Text), Icon, Open);
end;

function TPDF.TextAnnot(PosX, PosY, Width, Height: Double; const Author, Text: WideString; Icon: TAnnotIcon; Open: Boolean): Integer;
begin
   LoadFunction(@pdfTextAnnotW, 'pdfTextAnnotW');
   Result := pdfTextAnnotW(FInstance, PosX, PosY, Width, Height, PWideChar(Author), PWideChar(Text), Icon, Open);
end;
{$endif}

function TPDF.TextAnnotA(PosX, PosY, Width, Height: Double; const Author, Text: AnsiString; Icon: TAnnotIcon; Open: Boolean): Integer;
begin
   LoadFunction(@pdfTextAnnotA, 'pdfTextAnnotA');
   Result := pdfTextAnnotA(FInstance, PosX, PosY, Width, Height, PAnsiChar(Author), PAnsiChar(Text), Icon, Open);
end;

function TPDF.TextAnnotW(PosX, PosY, Width, Height: Double; const Author, Text: WideString; Icon: TAnnotIcon; Open: Boolean): Integer;
begin
   LoadFunction(@pdfTextAnnotW, 'pdfTextAnnotW');
   Result := pdfTextAnnotW(FInstance, PosX, PosY, Width, Height, PWideChar(Author), PWideChar(Text), Icon, Open);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.ToUnicode(const S: AnsiString): WideString;
var i, p, len: Integer;
begin
   len := Length(S) div 2;
   if len = 0 then Exit;
   SetLength(Result, len);
   p := 1;
   for i := 1 to len do begin
      Result[i] := WideChar(MakeWord(Byte(S[p]), Byte(S[p+1])));
      Inc(p, 2);
   end;
end;

function TPDF.ToUnicode(const S: PAnsiChar; Len: Integer): WideString;
var i, p: Integer;
begin
   Len := Len div 2;
   if Len = 0 then Exit;
   SetLength(Result, Len);
   p := 0;
   for i := 1 to Len do begin
      Result[i] := WideChar(MakeWord(Byte(S[p]), Byte(S[p+1])));
      Inc(p, 2);
   end;
end;
{$endif}

function TPDF.ToUnicodeA(const S: AnsiString): WideString;
var i, p, len: Integer;
begin
   len := Length(S) div 2;
   if len = 0 then Exit;
   SetLength(Result, len);
   p := 1;
   for i := 1 to len do begin
      Result[i] := WideChar(MakeWord(Byte(S[p]), Byte(S[p+1])));
      Inc(p, 2);
   end;
end;

function TPDF.ToUnicodeW(const S: PAnsiChar; Len: Integer): WideString;
var i, p: Integer;
begin
   Len := Len div 2;
   if Len = 0 then Exit;
   SetLength(Result, Len);
   p := 0;
   for i := 1 to Len do begin
      Result[i] := WideChar(MakeWord(Byte(S[p]), Byte(S[p+1])));
      Inc(p, 2);
   end;
end;

function TPDF.TranslateCoords(NewOriginX, NewOriginY: Double): Boolean;
begin
   LoadFunction(@pdfTranslateCoords, 'pdfTranslateCoords');
   Result := pdfTranslateCoords(FInstance, NewOriginX, NewOriginY);
end;

function TPDF.TranslateRawCode(const IFont: Pointer; const Text: PAnsiChar; Len: Cardinal; var Width: Double; OutText: PWideChar; var OutLen: Integer; var Decoded: LongBool; CharSpacing, WordSpacing, TextScale: Single): Cardinal;
begin
   LoadFunction(@fntTranslateRawCode, 'fntTranslateRawCode');
   Result := fntTranslateRawCode(IFont, Text, Len, Width, OutText, OutLen, Decoded, CharSpacing, WordSpacing, TextScale);
end;

function TPDF.TranslateString(var Stack: TPDFStack; var Buffer: PWideChar; var Size: Cardinal; Flags: Cardinal): Integer;
begin
   // The buffer should be initially allocated with enough room for 128 characters
   if Size < Stack.TextLen then begin
      Inc(Size, Stack.TextLen);
      ReAllocMem(Buffer, Size * sizeof(WideChar));
   end;
   LoadFunction(@fntTranslateString, 'fntTranslateString');
   Result := fntTranslateString(Stack, Buffer, Size, Flags);
end;

function TPDF.TranslateString2(const IFont: Pointer; const Text: PAnsiChar; Len: Cardinal; OutBuf: WideString; Size, Flags: Cardinal): Integer;
begin
   LoadFunction(@fntTranslateString2, 'fntTranslateString2');
   Result := fntTranslateString2(IFont, Text, Len, PWideChar(OutBuf), Size, Flags);
end;

function TPDF.Triangle(x1, y1, x2, y2, x3, y3: Double; FillMode: TPathFillMode): Boolean;
begin
   LoadFunction(@pdfTriangle, 'pdfTriangle');
   Result := pdfTriangle(FInstance, x1, y1, x2, y2, x3, y3, Ord(FillMode));
end;

function TPDF.UnLockLayer(Layer: Cardinal): Boolean;
begin
   LoadFunction(@pdfUnLockLayer, 'pdfUnLockLayer');
   Result := pdfUnLockLayer(FInstance, Layer);
end;

function TPDF.UTF16ToUTF32(const Source: WideString): Pointer;
begin
   LoadFunction(@pdfUTF16ToUTF32, 'pdfUTF16ToUTF32');
   Result := pdfUTF16ToUTF32(FInstance, PWideChar(Source));
end;

function TPDF.UTF16ToUTF32Ex(const Source: WideString; var Len: Cardinal): Pointer;
begin
   LoadFunction(@pdfUTF16ToUTF32Ex, 'pdfUTF16ToUTF32Ex');
   Result := pdfUTF16ToUTF32Ex(FInstance, PWideChar(Source), Len);
end;

function TPDF.UTF32ToUTF16(const Source: Pointer): PWideChar;
begin
   LoadFunction(@pdfUTF32ToUTF16, 'pdfUTF32ToUTF16');
   Result := pdfUTF32ToUTF16(FInstance, Source);
end;

function TPDF.UTF32ToUTF16Ex(const Source: Pointer; var Len: Cardinal): PWideChar;
begin
   LoadFunction(@pdfUTF32ToUTF16Ex, 'pdfUTF32ToUTF16Ex');
   Result := pdfUTF32ToUTF16Ex(FInstance, Source, Len);
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.WebLink(PosX, PosY, Width, Height: Double; const URL: AnsiString): Integer;
begin
   LoadFunction(@pdfWebLinkA, 'pdfWebLinkA');
   Result := pdfWebLinkA(FInstance, PosX, PosY, Width, Height, PAnsiChar(URL));
end;

function TPDF.WebLink(PosX, PosY, Width, Height: Double; const URL: WideString): Integer;
begin
   LoadFunction(@pdfWebLinkW, 'pdfWebLinkW');
   Result := pdfWebLinkW(FInstance, PosX, PosY, Width, Height, PWideChar(URL));
end;
{$endif}

function TPDF.WebLinkA(PosX, PosY, Width, Height: Double; const URL: AnsiString): Integer;
begin
   LoadFunction(@pdfWebLinkA, 'pdfWebLinkA');
   Result := pdfWebLinkA(FInstance, PosX, PosY, Width, Height, PAnsiChar(URL));
end;

function TPDF.WebLinkW(PosX, PosY, Width, Height: Double; const URL: WideString): Integer;
begin
   LoadFunction(@pdfWebLinkW, 'pdfWebLinkW');
   Result := pdfWebLinkW(FInstance, PosX, PosY, Width, Height, PWideChar(URL));
end;

function TPDF.WeigthFromStyle(Style: TFStyle): Cardinal;
begin
   Result := (Style and not $C00FFFFF) shr 20;
end;

function TPDF.WeightToStyle(Weight: Cardinal): TFStyle;
begin
   Result := Weight shl 20;
end;

function TPDF.WidthFromStyle(Style: TFStyle): Cardinal;
begin
   Result := (Style and not $FFF2D00F) shr 8;
end;

function TPDF.WidthToStyle(Width: Cardinal): TFStyle;
begin
   Result := Width shl 8;
end;

{$ifdef DELPHI6_OR_HIGHER}
function TPDF.WriteAngleText(const AText: AnsiString; Angle, PosX, PosY, Radius, YOrigin: Double): Boolean;
begin
   LoadFunction(@pdfWriteAngleTextA, 'pdfWriteAngleTextA');
   Result := pdfWriteAngleTextA(FInstance, PAnsiChar(AText), Angle, PosX, PosY, Radius, YOrigin);
end;

function TPDF.WriteAngleText(const AText: WideString; Angle, PosX, PosY, Radius, YOrigin: Double): Boolean;
begin
   LoadFunction(@pdfWriteAngleTextW, 'pdfWriteAngleTextW');
   Result := pdfWriteAngleTextW(FInstance, PWideChar(AText), Angle, PosX, PosY, Radius, YOrigin);
end;

function TPDF.WriteFText(Align: TTextAlign; const AText: AnsiString): Boolean;
begin
   LoadFunction(@pdfWriteFTextA, 'pdfWriteFTextA');
   Result := pdfWriteFTextA(FInstance, Ord(Align), PAnsiChar(AText));
end;

function TPDF.WriteFText(Align: TTextAlign; const AText: WideString): Boolean;
begin
   LoadFunction(@pdfWriteFTextW, 'pdfWriteFTextW');
   Result := pdfWriteFTextW(FInstance, Ord(Align), PWideChar(AText));
end;

function TPDF.WriteFTextEx(PosX, PosY, Width, Height: Double; Align: TTextAlign; const AText: AnsiString): Boolean;
begin
   LoadFunction(@pdfWriteFTextExA, 'pdfWriteFTextExA');
   Result := pdfWriteFTextExA(FInstance, PosX, PosY, Width, Height, Ord(Align), PAnsiChar(AText));
end;

function TPDF.WriteFTextEx(PosX, PosY, Width, Height: Double; Align: TTextAlign; const AText: WideString): Boolean;
begin
   LoadFunction(@pdfWriteFTextExW, 'pdfWriteFTextExW');
   Result := pdfWriteFTextExW(FInstance, PosX, PosY, Width, Height, Ord(Align), PWideChar(AText));
end;

function TPDF.WriteText(PosX, PosY: Double; const AText: AnsiString): Boolean;
begin
   LoadFunction(@pdfWriteTextA, 'pdfWriteTextA');
   Result := pdfWriteTextA(FInstance, PosX, PosY, PAnsiChar(AText));
end;

function TPDF.WriteText(PosX, PosY: Double; const AText: WideString): Boolean;
begin
   LoadFunction(@pdfWriteTextW, 'pdfWriteTextW');
   Result := pdfWriteTextW(FInstance, PosX, PosY, PWideChar(AText));
end;

function TPDF.WriteTextEx(PosX, PosY: Double; const AText: AnsiString; Len: Cardinal): Boolean;
begin
   LoadFunction(@pdfWriteTextExA, 'pdfWriteTextExA');
   Result := pdfWriteTextExA(FInstance, PosX, PosY, PAnsiChar(AText), Len);
end;

function TPDF.WriteTextEx(PosX, PosY: Double; const AText: WideString; Len: Cardinal): Boolean;
begin
   LoadFunction(@pdfWriteTextExW, 'pdfWriteTextExW');
   Result := pdfWriteTextExW(FInstance, PosX, PosY, PWideChar(AText), Len);
end;

function TPDF.WriteTextMatrix(var Matrix: TCTM; const AText: AnsiString): Boolean;
begin
   LoadFunction(@pdfWriteTextMatrixA, 'pdfWriteTextMatrixA');
   Result := pdfWriteTextMatrixA(FInstance, Matrix, PAnsiChar(AText));
end;

function TPDF.WriteTextMatrix(var Matrix: TCTM; const AText: WideString): Boolean;
begin
   LoadFunction(@pdfWriteTextMatrixW, 'pdfWriteTextMatrixW');
   Result := pdfWriteTextMatrixW(FInstance, Matrix, PWideChar(AText));
end;

function TPDF.WriteTextMatrixEx(var M: TCTM; const AText: AnsiString; Len: Cardinal): Boolean;
begin
   LoadFunction(@pdfWriteTextMatrixExA, 'pdfWriteTextMatrixExA');
   Result := pdfWriteTextMatrixExA(FInstance, M, PAnsiChar(AText), Len);
end;

function TPDF.WriteTextMatrixEx(var M: TCTM; const AText: WideString; Len: Cardinal): Boolean;
begin
   LoadFunction(@pdfWriteTextMatrixExW, 'pdfWriteTextMatrixExW');
   Result := pdfWriteTextMatrixExW(FInstance, M, PWideChar(AText), Len);
end;
{$endif}

function TPDF.WriteAngleTextA(const AText: AnsiString; Angle, PosX, PosY, Radius, YOrigin: Double): Boolean;
begin
   LoadFunction(@pdfWriteAngleTextA, 'pdfWriteAngleTextA');
   Result := pdfWriteAngleTextA(FInstance, PAnsiChar(AText), Angle, PosX, PosY, Radius, YOrigin);
end;

function TPDF.WriteAngleTextW(const AText: WideString; Angle, PosX, PosY, Radius, YOrigin: Double): Boolean;
begin
   LoadFunction(@pdfWriteAngleTextW, 'pdfWriteAngleTextW');
   Result := pdfWriteAngleTextW(FInstance, PWideChar(AText), Angle, PosX, PosY, Radius, YOrigin);
end;

function TPDF.WriteFTextA(Align: TTextAlign; const AText: AnsiString): Boolean;
begin
   LoadFunction(@pdfWriteFTextA, 'pdfWriteFTextA');
   Result := pdfWriteFTextA(FInstance, Ord(Align), PAnsiChar(AText));
end;

function TPDF.WriteFTextW(Align: TTextAlign; const AText: WideString): Boolean;
begin
   LoadFunction(@pdfWriteFTextW, 'pdfWriteFTextW');
   Result := pdfWriteFTextW(FInstance, Ord(Align), PWideChar(AText));
end;

function TPDF.WriteFTextExA(PosX, PosY, Width, Height: Double; Align: TTextAlign; const AText: AnsiString): Boolean;
begin
   LoadFunction(@pdfWriteFTextExA, 'pdfWriteFTextExA');
   Result := pdfWriteFTextExA(FInstance, PosX, PosY, Width, Height, Ord(Align), PAnsiChar(AText));
end;

function TPDF.WriteFTextExW(PosX, PosY, Width, Height: Double; Align: TTextAlign; const AText: WideString): Boolean;
begin
   LoadFunction(@pdfWriteFTextExW, 'pdfWriteFTextExW');
   Result := pdfWriteFTextExW(FInstance, PosX, PosY, Width, Height, Ord(Align), PWideChar(AText));
end;

function TPDF.WriteTextA(PosX, PosY: Double; const AText: AnsiString): Boolean;
begin
   LoadFunction(@pdfWriteTextA, 'pdfWriteTextA');
   Result := pdfWriteTextA(FInstance, PosX, PosY, PAnsiChar(AText));
end;

function TPDF.WriteTextW(PosX, PosY: Double; const AText: WideString): Boolean;
begin
   LoadFunction(@pdfWriteTextW, 'pdfWriteTextW');
   Result := pdfWriteTextW(FInstance, PosX, PosY, PWideChar(AText));
end;

function TPDF.WriteTextExA(PosX, PosY: Double; const AText: AnsiString; Len: Cardinal): Boolean;
begin
   LoadFunction(@pdfWriteTextExA, 'pdfWriteTextExA');
   Result := pdfWriteTextExA(FInstance, PosX, PosY, PAnsiChar(AText), Len);
end;

function TPDF.WriteTextExW(PosX, PosY: Double; const AText: WideString; Len: Cardinal): Boolean;
begin
   LoadFunction(@pdfWriteTextExW, 'pdfWriteTextExW');
   Result := pdfWriteTextExW(FInstance, PosX, PosY, PWideChar(AText), Len);
end;

function TPDF.WriteTextMatrixA(var Matrix: TCTM; const AText: AnsiString): Boolean;
begin
   LoadFunction(@pdfWriteTextMatrixA, 'pdfWriteTextMatrixA');
   Result := pdfWriteTextMatrixA(FInstance, Matrix, PAnsiChar(AText));
end;

function TPDF.WriteTextMatrixW(var Matrix: TCTM; const AText: WideString): Boolean;
begin
   LoadFunction(@pdfWriteTextMatrixW, 'pdfWriteTextMatrixW');
   Result := pdfWriteTextMatrixW(FInstance, Matrix, PWideChar(AText));
end;

function TPDF.WriteTextMatrixExA(var M: TCTM; const AText: AnsiString; Len: Cardinal): Boolean;
begin
   LoadFunction(@pdfWriteTextMatrixExA, 'pdfWriteTextMatrixExA');
   Result := pdfWriteTextMatrixExA(FInstance, M, PAnsiChar(AText), Len);
end;

function TPDF.WriteTextMatrixExW(var M: TCTM; const AText: WideString; Len: Cardinal): Boolean;
begin
   LoadFunction(@pdfWriteTextMatrixExW, 'pdfWriteTextMatrixExW');
   Result := pdfWriteTextMatrixExW(FInstance, M, PWideChar(AText), Len);
end;

end.

