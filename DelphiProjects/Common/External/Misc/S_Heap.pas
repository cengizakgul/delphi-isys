unit S_Heap;

{ Secrets of Delphi 2, by Ray Lischner. (1996, Waite Group Press).
  Chapter 19: Heaps of Secrets
  Copyright � 1996 The Waite Group, Inc. }

interface

uses Windows;

{ New heap manager for Delphi 2.0, which just uses the Windows heap
  routines directly. Add this unit to the uses statement in the
  project file. Make sure this unit is first, to avoid clashes
  between the default memory manager and this memory manager. }

function AdditionalHeap: THandle;

implementation

{ Save a function call each time by caching the process heap handle. }
var
  Heap: THandle;

function AdditionalHeap: THandle;
begin
  Result := Heap;
end;

{ Allocate Size bytes and return a pointer to uninitialized memory,
  or nil for a failure. }
function S_GetMem(Size: Integer): Pointer;
begin
  Result := HeapAlloc(Heap, 0, Size);
end;

{ Free the pointer. Return 0 for success, and 1 for failure. }
function S_FreeMem(Ptr: Pointer): Integer;
begin
  if HeapFree(Heap, 0, Ptr) then
    Result := 0
  else
    Result := 1;
end;

{ Reallocate Size bytes, and return the new pointer, or nil. }
function S_ReallocMem(Ptr: Pointer; Size: Integer): Pointer;
begin
  Result := HeapRealloc(Heap, 0, Ptr, Size);
end;

{ Setup the new memory manager. }
procedure InitMemoryManager;
var
  MemMgr: TMemoryManager;
begin
  Heap := HeapCreate(0, 16* 1024* 1024, 0); //GetProcessHeap;
  MemMgr.GetMem := S_GetMem;
  MemMgr.FreeMem := S_FreeMem;
  MemMgr.ReallocMem := S_ReallocMem;
  SetMemoryManager(MemMgr);
end;

initialization
  { Install the new memory manager, so it will be used for all
    future memory management. }
  InitMemoryManager;
end.


