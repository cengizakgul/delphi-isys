unit pdh;

{$WEAKPACKAGEUNIT}

interface

uses Windows;

// version info
const
  PDH_CVERSION_WIN40 = $0400;
// v1.1 revision of PDH -- basic log functions
// v1.2 of the PDH -- adds variable instance counters
// v1.3 of the PDH -- adds log service control & stubs for NT5/PDH v2 fn's
  PDH_VERSION = PDH_CVERSION_WIN40 + 3;

//
//  Time value constants
//
  MAX_TIME_VALUE = $7FFFFFFFFFFFFFFF;
  MIN_TIME_VALUE = 0;

//
// dwFormat flag values
//
  PDH_FMT_RAW     = $00000010;
  PDH_FMT_ANSI    = $00000020;
  PDH_FMT_UNICODE = $00000040;
  PDH_FMT_LONG    = $00000100;
  PDH_FMT_DOUBLE  = $00000200;
  PDH_FMT_LARGE   = $00000400;
  PDH_FMT_NOSCALE = $00001000;
  PDH_FMT_1000    = $00002000;
  PDH_FMT_NODATA  = $00004000;

  PERF_DETAIL_COSTLY   = $00010000;
  PERF_DETAIL_STANDARD = $0000FFFF;

  PDH_MAX_SCALE    = 7;
  PDH_MIN_SCALE    = -7;

  PDH_LOG_READ_ACCESS     = $00010000;
  PDH_LOG_WRITE_ACCESS    = $00020000;
  PDH_LOG_ACCESS_MASK     = $000F0000;

  PDH_LOG_CREATE_NEW      = $00000001;
  PDH_LOG_CREATE_ALWAYS   = $00000002;
  PDH_LOG_OPEN_ALWAYS     = $00000003;
  PDH_LOG_OPEN_EXISTING   = $00000004;
  PDH_LOG_CREATE_MASK     = $0000000F;

  PDH_LOG_OPT_USER_STRING = $01000000;
  PDH_LOG_OPT_MASK        = $0F000000;

  PDH_LOG_TYPE_UNDEFINED      = 0;
  PDH_LOG_TYPE_CSV            = 1;
  PDH_LOG_TYPE_TSV            = 2;
  PDH_LOG_TYPE_BINARY         = 3;
  PDH_LOG_TYPE_TRACE_KERNEL   = 4;
  PDH_LOG_TYPE_TRACE_GENERIC  = 5;

  PDH_FLAGS_CLOSE_QUERY = $00000001;
//
//  Data source selection dialog
//
  PDH_FLAGS_FILE_BROWSER_ONLY = $00000001;

// flags for the log service api's
  PDH_LOGSVC_NO_WAIT          = $80000000;
  PDH_LOGSVC_ALL_QUERIES      = $00000001;
  PDH_LOGSVC_TRACE_LOG        = $00000002;

  PDH_LOGSVC_CMD_START        = $00000010;
  PDH_LOGSVC_CMD_STOP         = $00000020;

  PDH_LOGSVC_CTRL_ADD         = $00000100;
  PDH_LOGSVC_CTRL_REMOVE      = $00000200;
  PDH_LOGSVC_CTRL_INFO        = $00000400;

  PDH_LOGSVC_STATUS_RUNNING   = $00001000;
  PDH_LOGSVC_STATUS_STOPPED   = $00002000;
  PDH_LOGSVC_STATUS_PAUSED    = $00004000;
  PDH_LOGSVC_STATUS_ERROR     = $00008000;
  PDH_LOGSVC_STATUS_PENDING   = $00010000;

  PDH_LOGSVC_NAME_UNDEFINED  = -1;
  PDH_LOGSVC_NAME_MMDDHH     = 0;
  PDH_LOGSVC_NAME_NNNNNN     = 1;
  PDH_LOGSVC_NAME_YYDDD      = 2;
  PDH_LOGSVC_NAME_YYMM       = 3;
  PDH_LOGSVC_NAME_YYMMDD     = 4;
  PDH_LOGSVC_NAME_YYMMDDHH   = 5;

  PDH_LOGSVC_RENAME_UNDEFINED = -1;
  PDH_LOGSVC_RENAME_HOURS     = 0;
  PDH_LOGSVC_RENAME_DAYS      = 1;
  PDH_LOGSVC_RENAME_MONTHS    = 2;
  PDH_LOGSVC_RENAME_KBYTES    = 3;
  PDH_LOGSVC_RENAME_MBYTES    = 4;

  PDH_CSTATUS_VALID_DATA           = $00000000;
  PDH_CSTATUS_NEW_DATA             = $00000001;
  PDH_CSTATUS_NO_MACHINE           = $800007D0;
  PDH_CSTATUS_NO_INSTANCE          = $800007D1;
  PDH_MORE_DATA                    = $800007D2;
  PDH_CSTATUS_ITEM_NOT_VALIDATED   = $800007D3;
  PDH_RETRY                        = $800007D4;
  PDH_NO_DATA                      = $800007D5;
  PDH_CALC_NEGATIVE_DENOMINATOR    = $800007D6;
  PDH_CALC_NEGATIVE_TIMEBASE       = $800007D7;
  PDH_CALC_NEGATIVE_VALUE          = $800007D8;
  PDH_DIALOG_CANCELLED             = $800007D9;
  PDH_END_OF_LOG_FILE              = $800007DA;
  PDH_CSTATUS_NO_OBJECT            = $C0000BB8;
  PDH_CSTATUS_NO_COUNTER           = $C0000BB9;
  PDH_CSTATUS_INVALID_DATA         = $C0000BBA;
  PDH_MEMORY_ALLOCATION_FAILURE    = $C0000BBB;
  PDH_INVALID_HANDLE               = $C0000BBC;
  PDH_INVALID_ARGUMENT             = $C0000BBD;
  PDH_FUNCTION_NOT_FOUND           = $C0000BBE;
  PDH_CSTATUS_NO_COUNTERNAME       = $C0000BBF;
  PDH_CSTATUS_BAD_COUNTERNAME      = $C0000BC0;
  PDH_INVALID_BUFFER               = $C0000BC1;
  PDH_INSUFFICIENT_BUFFER          = $C0000BC2;
  PDH_CANNOT_CONNECT_MACHINE       = $C0000BC3;
  PDH_INVALID_PATH                 = $C0000BC4;
  PDH_INVALID_INSTANCE             = $C0000BC5;
  PDH_INVALID_DATA                 = $C0000BC6;
  PDH_NO_DIALOG_DATA               = $C0000BC7;
  PDH_CANNOT_READ_NAME_STRINGS     = $C0000BC8;
  PDH_LOG_FILE_CREATE_ERROR        = $C0000BC9;
  PDH_LOG_FILE_OPEN_ERROR          = $C0000BCA;
  PDH_LOG_TYPE_NOT_FOUND           = $C0000BCB;
  PDH_NO_MORE_DATA                 = $C0000BCC;
  PDH_ENTRY_NOT_IN_LOG_FILE        = $C0000BCD;
  PDH_DATA_SOURCE_IS_LOG_FILE      = $C0000BCE;
  PDH_DATA_SOURCE_IS_REAL_TIME     = $C0000BCF;
  PDH_UNABLE_READ_LOG_HEADER       = $C0000BD0;
  PDH_FILE_NOT_FOUND               = $C0000BD1;
  PDH_FILE_ALREADY_EXISTS          = $C0000BD2;
  PDH_NOT_IMPLEMENTED              = $C0000BD3;
  PDH_STRING_NOT_FOUND             = $C0000BD4;
  PDH_UNABLE_MAP_NAME_FILES        = $80000BD5;
  PDH_UNKNOWN_LOG_FORMAT           = $C0000BD6;
  PDH_UNKNOWN_LOGSVC_COMMAND       = $C0000BD7;
  PDH_LOGSVC_QUERY_NOT_FOUND       = $C0000BD8;
  PDH_LOGSVC_NOT_OPENED            = $C0000BD9;

type
// data type definitions
  PDH_STATUS = LongInt;
  HCOUNTER = THandle;
  HQUERY = THandle;
  HLOG = THandle;

  TPDH_RAW_COUNTER = record
    CStatus : DWORD;
    TimeStamp : FILETIME;
    FirstValue : LONGLONG;
    SecondValue : LONGLONG;
    MultiCount : DWORD
  end;
  PPDH_RAW_COUNTER = ^TPDH_RAW_COUNTER;

  TPDH_RAW_COUNTER_ITEM_A = record
    szName : PChar;
    RawValue : TPDH_RAW_COUNTER
  end;
  PPDH_RAW_COUNTER_ITEM_A = ^TPDH_RAW_COUNTER_ITEM_A;
  TPDH_RAW_COUNTER_ITEM = TPDH_RAW_COUNTER_ITEM_A;
  PPDH_RAW_COUNTER_ITEM = ^TPDH_RAW_COUNTER_ITEM;

  TPDH_RAW_COUNTER_ITEM_W = record
    szName : PWideChar;
    RawValue : TPDH_RAW_COUNTER
  end;
  PPDH_RAW_COUNTER_ITEM_W = ^TPDH_RAW_COUNTER_ITEM_W;

  TPDH_FMT_COUNTERVALUE = record
  CStatus : DWORD;
  dummy : DWORD;
  case DWORD of
    0 : (longValue : LongInt);
    1 : (doubleValue : double);
    2 : (largeValue : LONGLONG);
    3 : (AnsiStringValue : PChar);
    4 : (WideStringValue : PWideChar)
  end;
  PPDH_FMT_COUNTERVALUE = ^TPDH_FMT_COUNTERVALUE;

  TPDH_FMT_COUNTERVALUE_ITEM_A = record
    szName : PChar;
    FmtValue : TPDH_FMT_COUNTERVALUE
  end;
  PPDH_FMT_COUNTERVALUE_ITEM_A = ^TPDH_FMT_COUNTERVALUE_ITEM_A;
  TPDH_FMT_COUNTERVALUE_ITEM = TPDH_FMT_COUNTERVALUE_ITEM_A;
  PPDH_FMT_COUNTERVALUE_ITEM = ^TPDH_FMT_COUNTERVALUE_ITEM;

  TPDH_FMT_COUNTERVALUE_ITEM_W = record
    szName : PWideChar;
    FmtValue : TPDH_FMT_COUNTERVALUE
  end;
  PPDH_FMT_COUNTERVALUE_ITEM_W = ^TPDH_FMT_COUNTERVALUE_ITEM_W;

  TPDH_STATISTICS  = record
    dwFormat : DWORD;
    count : DWORD;
    min : TPDH_FMT_COUNTERVALUE;
    max : TPDH_FMT_COUNTERVALUE;
    mean : TPDH_FMT_COUNTERVALUE
  end;
  PPDH_STATISTICS = ^TPDH_STATISTICS;

  TPDH_COUNTER_PATH_ELEMENTS_A = record
    szMachineName : PChar;
    szObjectName : PChar;
    szInstanceName : PChar;
    szParentInstance : PChar;
    dwInstanceIndex : DWORD;
    szCounterName : PChar
  end;
  PPDH_COUNTER_PATH_ELEMENTS_A = ^TPDH_COUNTER_PATH_ELEMENTS_A;
  TPDH_COUNTER_PATH_ELEMENTS = TPDH_COUNTER_PATH_ELEMENTS_A;
  PPDH_COUNTER_PATH_ELEMENTS = ^TPDH_COUNTER_PATH_ELEMENTS;

  TPDH_COUNTER_PATH_ELEMENTS_W = record
    szMachineName : PWideChar;
    szObjectName : PWideChar;
    szInstanceName : PWideChar;
    szParentInstance : PWideChar;
    dwInstanceIndex : DWORD;
    szCounterName : PWideChar
  end;
  PPDH_COUNTER_PATH_ELEMENTS_W = ^TPDH_COUNTER_PATH_ELEMENTS_W;


  TPDH_DATA_ITEM_PATH_ELEMENTS_A = record
    szMachineName : PChar;
    ObjectGUID : TGUID;
    dwItemId : DWORD;
    szInstanceName : PChar
  end;
  PPDH_DATA_ITEM_PATH_ELEMENTS_A = ^TPDH_DATA_ITEM_PATH_ELEMENTS_A;
  TPDH_DATA_ITEM_PATH_ELEMENTS = TPDH_DATA_ITEM_PATH_ELEMENTS_A;
  PPDH_DATA_ITEM_PATH_ELEMENTS = ^TPDH_DATA_ITEM_PATH_ELEMENTS;

  TPDH_DATA_ITEM_PATH_ELEMENTS_W = record
    szMachineName : PWideChar;
    ObjectGUID : TGUID;
    dwItemId : DWORD;
    szInstanceName : PWideChar
  end;
  PPDH_DATA_ITEM_PATH_ELEMENTS_W = ^TPDH_DATA_ITEM_PATH_ELEMENTS_W;

  TPathDetailsA = record
    case byte of
      0 : (
            DataItemPath : TPDH_DATA_ITEM_PATH_ELEMENTS_A;
          );
      1 : (
            CounterPath : TPDH_COUNTER_PATH_ELEMENTS_A;
          );

      2 : (
            szMachineName : PChar;
            szObjectName : PChar;
            szInstanceName : PChar;
            szParentInstance : PChar;
            dwInstanceIndex : DWORD;
            szCounterName : PChar;
          );
  end;

  TPathDetailsW = record
    case byte of
      0 : (
            DataItemPath : TPDH_DATA_ITEM_PATH_ELEMENTS_W;
          );
      1 : (
            CounterPath : TPDH_COUNTER_PATH_ELEMENTS_W;
          );

      2 : (
            szMachineName : PWideChar;
            szObjectName : PWideChar;
            szInstanceName : PWideChar;
            szParentInstance : PWideChar;
            dwInstanceIndex : DWORD;
            szCounterName : PWideChar;
          );
  end;

  TPDH_COUNTER_INFO_A = record
    dwLength : DWORD;
    dwType : DWORD;
    CVersion : DWORD;
    CStatus : DWORD;
    lScale : LongInt;
    lDefaultScale : LongInt;
    dwUserData : DWORD;
    dwQueryUserData : DWORD;
    szFullPath : PChar;
    pathDetails : TPathDetailsA;
    szExplainText : PChar;
    DataBuffer : array [0..0] of DWORD
  end;
  PPDH_COUNTER_INFO_A = ^TPDH_COUNTER_INFO_A;
  TPDH_COUNTER_INFO = TPDH_COUNTER_INFO_A;
  PPDH_COUNTER_INFO = ^TPDH_COUNTER_INFO;

  TPDH_COUNTER_INFO_W = record
    dwLength : DWORD;
    dwType : DWORD;
    CVersion : DWORD;
    CStatus : DWORD;
    lScale : LongInt;
    lDefaultScale : LongInt;
    dwUserData : DWORD;
    dwQueryUserData : DWORD;
    szFullPath : PWideChar;
    pathDetails : TPathDetailsW;
    szExplainText : PWideChar;
    DataBuffer : array [0..0] of DWORD
  end;
  PPDH_COUNTER_INFO_W = ^TPDH_COUNTER_INFO_W;

  TPDH_TIME_INFO = record
    StartTime : LONGLONG;
    EndTime : LONGLONG;
    SampleCount : DWORD
  end;
  PPDH_TIME_INFO = ^TPDH_TIME_INFO;

  TPDH_RAW_LOG_RECORD = record
    dwStructureSize : DWORD;
    dwRecordType : DWORD;
    dwItems : DWORD;
    RawBytes : array [0..0] of byte;
  end;
  PPDH_RAW_LOG_RECORD = ^TPDH_RAW_LOG_RECORD;

  TPDH_LOG_SERVICE_QUERY_INFO_A = record
    dwSize : DWORD;
    dwFlags : DWORD;
    dwLogQuota : DWORD;
    szLogFileCaption : PChar;
    szDefaultDir : PChar;
    szBaseFileName : PChar;
    dwFileType : DWORD;
    dwReserved : DWORD;
    case byte of
      0 : (
            PdlAutoNameInterval : DWORD;
            PdlAutoNameUnits : DWORD;
            PdlCommandFilename : PChar;
            PdlCounterList : PChar;
            PdlAutoNameFormat : DWORD;
            PdlSampleInterval : DWORD;
            PdlLogStartTime : FILETIME;
            PdlLogEndTime : FILETIME;
          );
      1 : (
            TlNumberOfBuffers : DWORD;
            TlMinimumBuffers : DWORD;
            TlMaximumBuffers : DWORD;
            TlFreeBuffers : DWORD;
            TlBufferSize : DWORD;
            TlEventsLost : DWORD;
            TlLoggerThreadId : DWORD;
            TlBuffersWritten : DWORD;
            TlLogHandle : DWORD;
            TlLogFileName : PChar;
          );
  end;
  PPDH_LOG_SERVICE_QUERY_INFO_A = ^TPDH_LOG_SERVICE_QUERY_INFO_A;
  TPDH_LOG_SERVICE_QUERY_INFO = TPDH_LOG_SERVICE_QUERY_INFO_A;
  PPDH_LOG_SERVICE_QUERY_INFO = ^TPDH_LOG_SERVICE_QUERY_INFO;

  TPDH_LOG_SERVICE_QUERY_INFO_W = record
    dwSize : DWORD;
    dwFlags : DWORD;
    dwLogQuota : DWORD;
    szLogFileCaption : PWideChar;
    szDefaultDir : PWideChar;
    szBaseFileName : PWideChar;
    dwFileType : DWORD;
    dwReserved : DWORD;
    case byte of
      0 : (
            PdlAutoNameInterval : DWORD;
            PdlAutoNameUnits : DWORD;
            PdlCommandFilename : PWideChar;
            PdlCounterList : PWideChar;
            PdlAutoNameFormat : DWORD;
            PdlSampleInterval : DWORD;
            PdlLogStartTime : FILETIME;
            PdlLogEndTime : FILETIME;
          );
      1 : (
            TlNumberOfBuffers : DWORD;
            TlMinimumBuffers : DWORD;
            TlMaximumBuffers : DWORD;
            TlFreeBuffers : DWORD;
            TlBufferSize : DWORD;
            TlEventsLost : DWORD;
            TlLoggerThreadId : DWORD;
            TlBuffersWritten : DWORD;
            TlLogHandle : DWORD;
            TlLogFileName : PWideChar;
          );
  end;
  PPDH_LOG_SERVICE_QUERY_INFO_W = ^TPDH_LOG_SERVICE_QUERY_INFO_W;

  CounterPathCallBack = function (dw : DWORD) : PDH_STATUS; stdcall;

  TBrowseDlgConfigFlag = (
    bIncludeInstanceIndex,
    bSingleCounterPerAdd,
    bSingleCounterPerDialog,
    bLocalCountersOnly,
    bWildCardInstances,
    bHideDetailBox,
    bInitializePath,
    bDisableMachineSelection,
    bIncludeCostlyObjects);

  TBrowseDlgConfigFlags = set of TBrowseDlgConfigFlag;

  TPDH_BrowseDlgConfig_W = record
    flags : TBrowseDlgConfigFlags;
    hWndOwner : HWND;
    szDataSource : PWideChar;
    szReturnPathBuffer : PWideChar;
    cchReturnPathLength : DWORD;
    pCallBack : CounterPathCallBack;
    dwCallBackArg : DWORD;
    CallBackStatus : PDH_STATUS;
    dwDefaultDetailLevel : DWORD;
    szDialogBoxCaption : PWideChar;
  end;
  PPDH_BROWSE_DLG_CONFIG_W = ^TPDH_BrowseDlgConfig_W;

  TPDH_BrowseDlgConfig_A = record
    flags : TBrowseDlgConfigFlags;
    hWndOwner : HWND;
    szDataSource : PChar;
    szReturnPathBuffer : PChar;
    cchReturnPathLength : DWORD;
    pCallBack : CounterPathCallBack;
    dwCallBackArg : DWORD;
    CallBackStatus : PDH_STATUS;
    dwDefaultDetailLevel : DWORD;
    szDialogBoxCaption : PChar;
  end;
  PPDH_BROWSE_DLG_CONFIG_A = ^TPDH_BrowseDlgConfig_A;
  TPDH_BrowseDlgConfig = TPDH_BrowseDlgConfig_A;
  PPDH_BrowseDlgConfig = ^TPDH_BrowseDlgConfig;

// function definitions

function PdhGetDllVersion(
    var lpdwVersion : DWORD
) : PDH_STATUS; stdcall;

//
//  Query Functions
//

(*function PdhOpenQueryW (
    szDataSource : PWideChar;
    dwUserData : DWORD;
    var phQuery : HQUERY
) : PDH_STATUS; stdcall;

function PdhOpenQueryA (
    szDataSource : PChar;
    dwUserData : DWORD;
    var phQuery : HQUERY
) : PDH_STATUS; stdcall;
*)
function PdhOpenQuery (
    szDataSource : PChar;
    dwUserData : DWORD;
    var phQuery : HQUERY
) : PDH_STATUS; stdcall;

function PdhAddCounterW (
    hQuery : HQUERY;
    szFullCounterPath : PWideChar;
    dwUserData : DWORD;
    var phCounter : HCOUNTER
) : PDH_STATUS; stdcall;

function PdhAddCounterA (
    hQuery : HQUERY;
    szFullCounterPath : PChar;
    dwUserData : DWORD;
    var phCounter : HCOUNTER
) : PDH_STATUS; stdcall;

function PdhAddCounter (
    hQuery : HQUERY;
    szFullCounterPath : PChar;
    dwUserData : DWORD;
    var phCounter : HCOUNTER
) : PDH_STATUS; stdcall;

function PdhRemoveCounter (
    hCounter : HCOUNTER
) : PDH_STATUS; stdcall;

function PdhCollectQueryData (
    hQuery: HQUERY
) : PDH_STATUS; stdcall;

function PdhCloseQuery (
    hQuery : HQUERY
) : PDH_STATUS; stdcall;

//
//  Counter Functions
//

function PdhGetFormattedCounterValue (
    hCounter : HCOUNTER;
    dwFormat : DWORD;
    lpdwType : PWORD;
    var pValue : TPDH_FMT_COUNTERVALUE
) : PDH_STATUS; stdcall;

function PdhGetFormattedCounterArrayA (
    hCounter : HCOUNTER;
    dwFormat : DWORD;
    var lpdwBufferSize : DWORD;
    var lpdwItemCount : DWORD;
    ItemBuffer : PPDH_FMT_COUNTERVALUE_ITEM_A
) : PDH_STATUS; stdcall;

function PdhGetFormattedCounterArrayW (
    hCounter : HCOUNTER;
    dwFormat : DWORD;
    var lpdwBufferSize : DWORD;
    var lpdwItemCount : DWORD;
    ItemBuffer : PPDH_FMT_COUNTERVALUE_ITEM_W
) : PDH_STATUS; stdcall;

function PdhGetFormattedCounterArray (
    hCounter : HCOUNTER;
    dwFormat : DWORD;
    var lpdwBufferSize : DWORD;
    var lpdwItemCount : DWORD;
    ItemBuffer : PPDH_FMT_COUNTERVALUE_ITEM
) : PDH_STATUS; stdcall;

function PdhGetRawCounterValue (
    hCounter : HCOUNTER;
    var lpdwType : DWORD;
    pValue : PPDH_RAW_COUNTER
) : PDH_STATUS; stdcall;

function PdhGetRawCounterArrayA (
    hCounter : HCOUNTER;
    var lpdwBufferSize : DWORD;
    var lpdwItemCount : DWORD;
    ItemBuffer : PPDH_RAW_COUNTER_ITEM_A
) : PDH_STATUS; stdcall;

function PdhGetRawCounterArrayW (
    hCounter : HCOUNTER;
    var lpdwBufferSize : DWORD;
    var lpdwItemCount : DWORD;
    ItemBuffer : PPDH_RAW_COUNTER_ITEM_W
) : PDH_STATUS; stdcall;

function PdhGetRawCounterArray (
    hCounter : HCOUNTER;
    var lpdwBufferSize : DWORD;
    var lpdwItemCount : DWORD;
    ItemBuffer : PPDH_RAW_COUNTER_ITEM
) : PDH_STATUS; stdcall;

function PdhCalculateCounterFromRawValue (
    hCounter : HCOUNTER;
    dwFormat : DWORD;
    rawValue1 : PPDH_RAW_COUNTER;
    rawValue2 : PPDH_RAW_COUNTER;
    fmtValue : PPDH_FMT_COUNTERVALUE
) : PDH_STATUS; stdcall;

function PdhComputeCounterStatistics (
    hCounter : HCounter;
    dwFormat : DWORD;
    dwFirstEntry : DWORD;
    dwNumEntries : DWORD;
    lpRawValueArray : PPDH_RAW_COUNTER;
    data : PPDH_STATISTICS
) : PDH_STATUS; stdcall;

function PdhGetCounterInfoW (
    hCounter : HCOUNTER;
    bRetrieveExplainText : BOOL;
    var pdwBufferSize : DWORD;
    lpBuffer : PPDH_COUNTER_INFO_W
) : PDH_STATUS; stdcall;

function PdhGetCounterInfoA (
    hCounter : HCOUNTER;
    bRetrieveExplainText : BOOL;
    var pdwBufferSize : DWORD;
    lpBuffer : PPDH_COUNTER_INFO_A
) : PDH_STATUS; stdcall;

function PdhGetCounterInfo (
    hCounter : HCOUNTER;
    bRetrieveExplainText : BOOL;
    var pdwBufferSize : DWORD;
    lpBuffer : PPDH_COUNTER_INFO
) : PDH_STATUS; stdcall;

function PdhSetCounterScaleFactor (
    hCounter : HCOUNTER;
    lFactor : longint
) : PDH_STATUS; stdcall;
//
//   Browsing and enumeration functions
//
function PdhConnectMachineW (
    szMachineName : PWideChar
) : PDH_STATUS; stdcall;

function PdhConnectMachineA (
    szMachineName : PChar
) : PDH_STATUS; stdcall;

function PdhConnectMachine (
    szMachineName : PChar
) : PDH_STATUS; stdcall;

function PdhEnumMachinesW (
    szDataSource : PWideChar;
    mszMachineList : PWideChar;
    var pcchBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhEnumMachinesA (
    szDataSource : PChar;
    mszMachineList : PChar;
    var pcchBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhEnumMachines (
    szDataSource : PChar;
    mszMachineList : PChar;
    var pcchBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhEnumObjectsW (
    szDataSource : PWideChar;
    szMachineName : PWideChar;
    mszObjectList : PWideChar;
    var pcchBufferSize : DWORD;
    dwDetailLevel : DWORD;
    bRefresh : BOOL
) : PDH_STATUS; stdcall;

function PdhEnumObjectsA (
    szDataSource : PChar;
    szMachineName : PChar;
    mszObjectList : PChar;
    var pcchBufferSize : DWORD;
    dwDetailLevel : DWORD;
    bRefresh : BOOL
) : PDH_STATUS; stdcall;

function PdhEnumObjects (
    szDataSource : PChar;
    szMachineName : PChar;
    mszObjectList : PChar;
    var pcchBufferSize : DWORD;
    dwDetailLevel : DWORD;
    bRefresh : BOOL
) : PDH_STATUS; stdcall;

function PdhEnumObjectItemsW (
    szDataSource : PWideChar;
    szMachineName : PWideChar;
    szObjectName : PWideChar;
    mszCounterList : PWideChar;
    var pcchCounterListLength : DWORD;
    mszInstanceList : PWideChar;
    var pcchInstanceListLength : DWORD;
    dwDetailLevel : DWORD;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhEnumObjectItemsA (
    szDataSource : PChar;
    szMachineName : PChar;
    szObjectName : PChar;
    mszCounterList : PChar;
    var pcchCounterListLength : DWORD;
    mszInstanceList : PChar;
    var pcchInstanceListLength : DWORD;
    dwDetailLevel : DWORD;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhEnumObjectItems (
    szDataSource : PChar;
    szMachineName : PChar;
    szObjectName : PChar;
    mszCounterList : PChar;
    var pcchCounterListLength : DWORD;
    mszInstanceList : PChar;
    var pcchInstanceListLength : DWORD;
    dwDetailLevel : DWORD;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhMakeCounterPathW (
    var pCounterPathElements : PPDH_COUNTER_PATH_ELEMENTS_W;
    szFullPathBuffer : PWideChar;
    var pcchBufferSize : DWORD;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhMakeCounterPathA (
    var pCounterPathElements : PPDH_COUNTER_PATH_ELEMENTS_A;
    szFullPathBuffer : PChar;
    var pcchBufferSize : DWORD;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhMakeCounterPath (
    var pCounterPathElements : PPDH_COUNTER_PATH_ELEMENTS;
    szFullPathBuffer : PChar;
    var pcchBufferSize : DWORD;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhParseCounterPathW (
    szFullPathBuffer : PWideChar;
    var pCounterPathElements : PPDH_COUNTER_PATH_ELEMENTS_W;
    var pdwBufferSize : DWORD;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhParseCounterPathA (
    szFullPathBuffer : PChar;
    var pCounterPathElements : PPDH_COUNTER_PATH_ELEMENTS_A;
    var pdwBufferSize : DWORD;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhParseCounterPath (
    szFullPathBuffer : PChar;
    var pCounterPathElements : PPDH_COUNTER_PATH_ELEMENTS;
    var pdwBufferSize : DWORD;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhParseInstanceNameW (
    szInstanceString : PWideChar;
    szInstanceName : PWideChar;
    var pcchInstanceNameLength : DWORD;
    szParentName : PWideChar;
    var pcchParentNameLength : DWORD;
    var lpIndex : DWORD
) : PDH_STATUS; stdcall;

function PdhParseInstanceNameA (
    szInstanceString : PChar;
    szInstanceName : PChar;
    var pcchInstanceNameLength : DWORD;
    szParentName : PChar;
    var pcchParentNameLength : DWORD;
    var lpIndex : DWORD
) : PDH_STATUS; stdcall;

function PdhParseInstanceName (
    szInstanceString : PChar;
    szInstanceName : PChar;
    var pcchInstanceNameLength : DWORD;
    szParentName : PChar;
    var pcchParentNameLength : DWORD;
    var lpIndex : DWORD
) : PDH_STATUS; stdcall;

function PdhValidatePathW (
    szFullPathBuffer : PWideChar
) : PDH_STATUS; stdcall;

function PdhValidatePathA (
    szFullPathBuffer : PChar
) : PDH_STATUS; stdcall;

function PdhValidatePath (
    szFullPathBuffer : PChar
) : PDH_STATUS; stdcall;

function PdhGetDefaultPerfObjectW (
    szDataSource : PWideChar;
    szMachineName : PWideChar;
    szDefaultObjectName : PWideChar;
    var pcchBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhGetDefaultPerfObjectA (
    szDataSource : PChar;
    szMachineName : PChar;
    szDefaultObjectName : PChar;
    var pcchBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhGetDefaultPerfObject (
    szDataSource : PChar;
    szMachineName : PChar;
    szDefaultObjectName : PChar;
    var pcchBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhGetDefaultPerfCounterW (
    szDataSource : PWideChar;
    szMachineName : PWideChar;
    szObjectName : PWideChar;
    szDefaultCounterName : PWideChar;
    var pcchBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhGetDefaultPerfCounterA (
    szDataSource : PChar;
    szMachineName : PChar;
    szObjectName : PChar;
    szDefaultCounterName : PChar;
    var pcchBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhGetDefaultPerfCounter (
    szDataSource : PChar;
    szMachineName : PChar;
    szObjectName : PChar;
    szDefaultCounterName : PChar;
    var pcchBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhBrowseCountersW (
  const data : TPDH_BrowseDlgConfig_W
) : PDH_STATUS; stdcall;

function PdhBrowseCountersA (
  const data : TPDH_BrowseDlgConfig_A
) : PDH_STATUS; stdcall;

function PdhBrowseCounters (
  const data : TPDH_BrowseDlgConfig
) : PDH_STATUS; stdcall;


function PdhExpandCounterPathW (
    szWildCardPath : PWideChar;
    mszExpandedPathList : PWideChar;
    var pcchPathListLength : DWORD
) : PDH_STATUS; stdcall;

function PdhExpandCounterPathA (
    szWildCardPath : PChar;
    mszExpandedPathList : PChar;
    var pcchPathListLength : DWORD
) : PDH_STATUS; stdcall;

function PdhExpandCounterPath (
    szWildCardPath : PChar;
    mszExpandedPathList : PChar;
    var pcchPathListLength : DWORD
) : PDH_STATUS; stdcall;

//
//  v2.0 functions
//
function PdhLookupPerfNameByIndexW (
    szMachineName : PWideChar;
    dwNameIndex : DWORD;
    szNameBuffer : PWideChar;
    var pcchNameBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhLookupPerfNameByIndexA (
    szMachineName : PChar;
    dwNameIndex : DWORD;
    szNameBuffer : PChar;
    var pcchNameBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhLookupPerfNameByIndex (
    szMachineName : PChar;
    dwNameIndex : DWORD;
    szNameBuffer : PChar;
    var pcchNameBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhLookupPerfIndexByNameW (
    szMachineName : PWideChar;
    szNameBuffer : PWideChar;
    var pdwIndex : DWORD
) : PDH_STATUS; stdcall;

function PdhLookupPerfIndexByNameA (
    szMachineName : PChar;
    szNameBuffer : PChar;
    var pdwIndex : DWORD
) : PDH_STATUS; stdcall;

function PdhLookupPerfIndexByName (
    szMachineName : PChar;
    szNameBuffer : PChar;
    var pdwIndex : DWORD
) : PDH_STATUS; stdcall;

//
//   Logging Functions
//

function PdhOpenLogW (
    szLogFileName : PWideChar;
    dwAccessFlags : DWORD;
    var lpdwLogType : DWORD;
    hQuery : HQUERY;
    dwMaxRecords : DWORD;
    szUserCaption : PWideChar;
    var phLog : HLOG
) : PDH_STATUS; stdcall;

function PdhOpenLogA (
    szLogFileName : PChar;
    dwAccessFlags : DWORD;
    var lpdwLogType : DWORD;
    hQuery : HQUERY;
    dwMaxRecords : DWORD;
    szUserCaption : PChar;
    var phLog : HLOG
) : PDH_STATUS; stdcall;

function PdhOpenLog (
    szLogFileName : PChar;
    dwAccessFlags : DWORD;
    var lpdwLogType : DWORD;
    hQuery : HQUERY;
    dwMaxRecords : DWORD;
    szUserCaption : PChar;
    var phLog : HLOG
) : PDH_STATUS; stdcall;

function PdhUpdateLogW (
    hLog : HLOG;
    szUserString : PWideChar
) : PDH_STATUS; stdcall;

function PdhUpdateLogA (
    hLog : HLOG;
    szUserString : PChar
) : PDH_STATUS; stdcall;

function PdhUpdateLog (
    hLog : HLOG;
    szUserString : PChar
) : PDH_STATUS; stdcall;

function PdhGetLogFileSize (
    hLog : HLOG;
    var llSize : LONGLONG
) : PDH_STATUS; stdcall;

function PdhCloseLog(
    hLog : HLOG;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhSelectDataSourceW (
    hWndOwner : HWND;
    dwFlags : DWORD;
    szDataSource : PWideChar;
    var pcchBufferLength : DWORD
) : PDH_STATUS; stdcall;

function PdhSelectDataSourceA (
    hWndOwner : HWND;
    dwFlags : DWORD;
    szDataSource : PChar;
    var pcchBufferLength : DWORD
) : PDH_STATUS; stdcall;

function PdhSelectDataSource (
    hWndOwner : HWND;
    dwFlags : DWORD;
    szDataSource : PChar;
    var pcchBufferLength : DWORD
) : PDH_STATUS; stdcall;

function PdhIsRealTimeQuery (
    hQuery : HQUERY
) : BOOL; stdcall;

function PdhSetQueryTimeRange (
    hQuery : HQUERY;
    pInfo : PPDH_TIME_INFO
) : PDH_STATUS; stdcall;

function PdhGetDataSourceTimeRangeW (
    szDataSource : PWideChar;
    var pdwNumEntries : DWORD;
    pInfo : PPDH_TIME_INFO;
    var pdwBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhGetDataSourceTimeRangeA (
    szDataSource : PChar;
    var pdwNumEntries : DWORD;
    pInfo : PPDH_TIME_INFO;
    var pdwBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhGetDataSourceTimeRange (
    szDataSource : PChar;
    var pdwNumEntries : DWORD;
    pInfo : PPDH_TIME_INFO;
    var pdwBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhCollectQueryDataEx (
    hQuery : HQUERY;
    dwIntervalTime : DWORD;
    hNewDataEvent : THandle
) : PDH_STATUS; stdcall;

function PdhFormatFromRawValue (
    dwCounterType : DWORD;
    dwFormat : DWORD;
    var pTimeBase : LONGLONG;
    pRawValue1 : PPDH_RAW_COUNTER;
    pRawValue2 : PPDH_RAW_COUNTER;
    pFmtValue : PPDH_FMT_COUNTERVALUE
) : PDH_STATUS; stdcall;

function PdhGetCounterTimeBase (
    hCounter : HCOUNTER;
    var pTimeBase : LONGLONG
) : PDH_STATUS; stdcall;

function PdhEncodeWmiPathA (
    szFullPathBufer : PChar;
    pDataItemPathElements : PPDH_DATA_ITEM_PATH_ELEMENTS_A;
    var pdwBuferSize : DWORD;
    langID : LANGID;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhEncodeWmiPath (
    szFullPathBufer : PChar;
    pDataItemPathElements : PPDH_DATA_ITEM_PATH_ELEMENTS;
    var pdwBuferSize : DWORD;
    langID : LANGID;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhEncodeWmiPathW (
    szFullPathBufer : PWideChar;
    pDataItemPathElements : PPDH_DATA_ITEM_PATH_ELEMENTS_A;
    var pdwBuferSize : DWORD;
    langID : LANGID;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhDecodeWmiPathA (
    pDataItemPathElements : PPDH_DATA_ITEM_PATH_ELEMENTS_A;
    pCounterPathElements : PPDH_COUNTER_PATH_ELEMENTS_A;
    var pdwBufferSize : DWORD;
    longID : LANGID;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhDecodeWmiPath (
    pDataItemPathElements : PPDH_DATA_ITEM_PATH_ELEMENTS;
    pCounterPathElements : PPDH_COUNTER_PATH_ELEMENTS;
    var pdwBufferSize : DWORD;
    longID : LANGID;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;

function PdhDecodeWmiPathW (
    pDataItemPathElements : PPDH_DATA_ITEM_PATH_ELEMENTS_W;
    pCounterPathElements : PPDH_COUNTER_PATH_ELEMENTS_W;
    var pdwBufferSize : DWORD;
    longID : LANGID;
    dwFlags : DWORD
) : PDH_STATUS; stdcall;


function PdhReadRawLogRecord (
    hLog : HLOG;
    ftRecord : FILETIME;
    pRawLogRecord : PPDH_RAW_LOG_RECORD;
    var pdwBufferLength : DWORD
) : PDH_STATUS; stdcall;

function PdhLogServiceCommandA (
    szMachineName : PChar;
    szQueryName : PChar;
    dwFlags : DWORD;
    var pdwStatus : DWORD
) : PDH_STATUS; stdcall;

function PdhLogServiceCommand (
    szMachineName : PChar;
    szQueryName : PChar;
    dwFlags : DWORD;
    var pdwStatus : DWORD
) : PDH_STATUS; stdcall;

function PdhLogServiceCommandW (
    szMachineName : PWideChar;
    szQueryName : PWideChar;
    dwFlags : DWORD;
    var pdwStatus : DWORD
) : PDH_STATUS; stdcall;

function PdhLogServiceControlA (
    szMachineName : PChar;
    szQueryName : PChar;
    dwFlags : DWORD;
    pInfoBuffer : PPDH_LOG_SERVICE_QUERY_INFO_A;
    var pdwBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhLogServiceControl (
    szMachineName : PChar;
    szQueryName : PChar;
    dwFlags : DWORD;
    pInfoBuffer : PPDH_LOG_SERVICE_QUERY_INFO;
    var pdwBufferSize : DWORD
) : PDH_STATUS; stdcall;

function PdhLogServiceControlW (
    szMachineName : PWideChar;
    szQueryName : PWideChar;
    dwFlags : DWORD;
    pInfoBuffer : PPDH_LOG_SERVICE_QUERY_INFO_W;
    var pdwBufferSize : DWORD
) : PDH_STATUS; stdcall;

function IsSuccessSeverity (ErrorCode : DWORD) : boolean;
function IsInformationalSeverity (ErrorCode : DWORD) : boolean;
function IsWarningSeverity (ErrorCode : DWORD) : boolean;
function IsErrorSeverity (ErrorCode : DWORD) : boolean;

implementation

const pdhapi = 'pdh.dll';

// define severity masks
function IsSuccessSeverity (ErrorCode : DWORD) : boolean;
begin
  result := (ErrorCode and $C0000000) = 0
end;

function IsInformationalSeverity (ErrorCode : DWORD) : boolean;
begin
  result := (ErrorCode and $C0000000) = $40000000
end;

function IsWarningSeverity (ErrorCode : DWORD) : boolean;
begin
  result := (ErrorCode and $C0000000) = $80000000
end;

function IsErrorSeverity (ErrorCode : DWORD) : boolean;
begin
  result := (ErrorCode and $C0000000) = $C0000000
end;

function PdhGetDllVersion; external pdhapi;
(*
function PdhOpenQueryW; external pdhapi;
function PdhOpenQueryA; external pdhapi;
*)
function PdhOpenQuery; external pdhapi (*name 'PdhOpenQueryA'*);
function PdhAddCounterW; external pdhapi;
function PdhAddCounterA; external pdhapi;
function PdhAddCounter; external pdhapi name 'PdhAddCounterA';
function PdhRemoveCounter; external pdhapi;
function PdhCollectQueryData; external pdhapi;
function PdhCloseQuery; external pdhapi;
function PdhGetFormattedCounterValue; external pdhapi;
function PdhGetFormattedCounterArrayA; external pdhapi;
function PdhGetFormattedCounterArrayW; external pdhapi;
function PdhGetFormattedCounterArray; external pdhapi name 'PdhGetFormattedCounterArrayA';
function PdhGetRawCounterValue; external pdhapi;
function PdhGetRawCounterArrayA; external pdhapi;
function PdhGetRawCounterArrayW; external pdhapi;
function PdhGetRawCounterArray; external pdhapi name 'PdhGetRawCounterArrayA';
function PdhCalculateCounterFromRawValue; external pdhapi;
function PdhComputeCounterStatistics; external pdhapi;
function PdhGetCounterInfoW; external pdhapi;
function PdhGetCounterInfoA; external pdhapi;
function PdhGetCounterInfo; external pdhapi name 'PdhGetCounterInfoA';
function PdhSetCounterScaleFactor; external pdhapi;
function PdhConnectMachineW; external pdhapi;
function PdhConnectMachineA; external pdhapi;
function PdhConnectMachine; external pdhapi name 'PdhConnectMachineA';
function PdhEnumMachinesW; external pdhapi;
function PdhEnumMachinesA; external pdhapi;
function PdhEnumMachines; external pdhapi name 'PdhEnumMachinesA';
function PdhEnumObjectsW; external pdhapi;
function PdhEnumObjectsA; external pdhapi;
function PdhEnumObjects; external pdhapi name 'PdhEnumObjectsA';
function PdhEnumObjectItemsW; external pdhapi;
function PdhEnumObjectItemsA; external pdhapi;
function PdhEnumObjectItems; external pdhapi name 'PdhEnumObjectItemsA';
function PdhMakeCounterPathW; external pdhapi;
function PdhMakeCounterPathA; external pdhapi;
function PdhMakeCounterPath; external pdhapi name 'PdhMakeCounterPathA';
function PdhParseCounterPathW; external pdhapi;
function PdhParseCounterPathA; external pdhapi;
function PdhParseCounterPath; external pdhapi name 'PdhParseCounterPathA';
function PdhParseInstanceNameW; external pdhapi;
function PdhParseInstanceNameA; external pdhapi;
function PdhParseInstanceName; external pdhapi name 'PdhParseInstanceNameA';
function PdhValidatePathW; external pdhapi;
function PdhValidatePathA; external pdhapi;
function PdhValidatePath; external pdhapi name 'PdhValidatePathA';
function PdhGetDefaultPerfObjectW; external pdhapi;
function PdhGetDefaultPerfObjectA; external pdhapi;
function PdhGetDefaultPerfObject; external pdhapi name 'PdhGetDefaultPerfObjectA';
function PdhGetDefaultPerfCounterW; external pdhapi;
function PdhGetDefaultPerfCounterA; external pdhapi;
function PdhGetDefaultPerfCounter; external pdhapi name 'PdhGetDefaultPerfCounterA';
function PdhBrowseCountersW; external pdhapi;
function PdhBrowseCountersA; external pdhapi;
function PdhBrowseCounters; external pdhapi name 'PdhBrowseCountersA';
function PdhExpandCounterPathW; external pdhapi;
function PdhExpandCounterPathA; external pdhapi;
function PdhExpandCounterPath; external pdhapi name 'PdhExpandCounterPathA';
function PdhLookupPerfNameByIndexW; external pdhapi;
function PdhLookupPerfNameByIndexA; external pdhapi;
function PdhLookupPerfNameByIndex; external pdhapi name 'PdhLookupPerfNameByIndexA';
function PdhLookupPerfIndexByNameW; external pdhapi;
function PdhLookupPerfIndexByNameA; external pdhapi;
function PdhLookupPerfIndexByName; external pdhapi name 'PdhLookupPerfIndexByNameA';
function PdhOpenLogW; external pdhapi;
function PdhOpenLogA; external pdhapi;
function PdhOpenLog; external pdhapi name 'PdhOpenLogA';
function PdhUpdateLogW; external pdhapi;
function PdhUpdateLogA; external pdhapi;
function PdhUpdateLog; external pdhapi name 'PdhUpdateLogA';
function PdhGetLogFileSize; external pdhapi;
function PdhCloseLog; external pdhapi;
function PdhSelectDataSourceW; external pdhapi;
function PdhSelectDataSourceA; external pdhapi;
function PdhSelectDataSource; external pdhapi name 'PdhSelectDataSourceA';
function PdhIsRealTimeQuery; external pdhapi;
function PdhSetQueryTimeRange; external pdhapi;
function PdhGetDataSourceTimeRangeW; external pdhapi;
function PdhGetDataSourceTimeRangeA; external pdhapi;
function PdhGetDataSourceTimeRange; external pdhapi name 'PdhGetDataSourceTimeRangeA';
function PdhCollectQueryDataEx; external pdhapi;
function PdhFormatFromRawValue; external pdhapi;
function PdhGetCounterTimeBase; external pdhapi;
function PdhEncodeWmiPathA; external pdhapi;
function PdhEncodeWmiPath; external pdhapi name 'PdhEncodeWmiPathA';
function PdhEncodeWmiPathW; external pdhapi;
function PdhDecodeWmiPathA; external pdhapi;
function PdhDecodeWmiPath; external pdhapi name 'PdhDecodeWmiPathA';
function PdhDecodeWmiPathW; external pdhapi;
function PdhReadRawLogRecord; external pdhapi;
function PdhLogServiceCommandA; external pdhapi;
function PdhLogServiceCommand; external pdhapi name 'PdhLogServiceCommandA';
function PdhLogServiceCommandW; external pdhapi;
function PdhLogServiceControlA; external pdhapi;
function PdhLogServiceControl; external pdhapi name 'PdhLogServiceControlA';
function PdhLogServiceControlW; external pdhapi;

end.
