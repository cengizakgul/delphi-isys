{*************************************************************}
{                                                             }
{       Borland Delphi Visual Component Library               }
{       InterBase Express core components                     }
{                                                             }
{       Copyright (c) 1998-2002 Borland Software Corporation  }
{                                                             }
{    InterBase Express is based in part on the product        }
{    Free IB Components, written by Gregory H. Deatz for      }
{    Hoagland, Longo, Moran, Dunst & Doukas Company.          }
{    Free IB Components is used under license.                }
{                                                             }
{    Additional code created by Jeff Overcash and used        }
{    with permission.                                         }
{*************************************************************}
unit IBBlob;

interface

uses
  SysUtils, Classes, IBHeader, IBErrorCodes, IBExternals, DB, IB, IBDatabase,
  IBIntf, IBUtils, EvStreamUtils;

const
  DefaultBlobSegmentSize =  High(WORD);

type
  { TIBBlobStream }
  TIBBlobStream = class(TStream)
  private
    FBase: TIBBase;
    FBlobID: TISC_QUAD;
    FBlobMaxSegmentSize: Long;
    FBlobNumSegments: Long;
    FBlobSize: Int64;
    FBlobType: Short;  { 0 = segmented, 1 = streamed }
    FBuffer: IisStream;
    FBlobInitialized: Boolean;
    FHandle: TISC_BLOB_HANDLE;
    FMode: TBlobStreamMode;
    FModified: Boolean;
    FGDSLibrary : IGDSLibrary;
  protected
    procedure CloseBlob;
    procedure CreateBlob;
    procedure EnsureBlobInitialized;
    procedure GetBlobInfo;
    function GetDatabase: TIBDatabase;
    function GetDBHandle: PISC_DB_HANDLE;
    function GetTransaction: TIBTransaction;
    function GetTRHandle: PISC_TR_HANDLE;
    procedure OpenBlob;
    procedure SetBlobID(Value: TISC_QUAD);
    procedure SetDatabase(Value: TIBDatabase);
    procedure SetMode(Value: TBlobStreamMode);
    procedure SetTransaction(Value: TIBTransaction);
  public
    constructor Create;
    destructor Destroy; override;
    function  Call(ErrCode: ISC_STATUS; RaiseError: Boolean): ISC_STATUS;
    procedure Cancel;
    procedure CheckReadable;
    procedure CheckWritable;
    procedure Finalize;
    procedure LoadFromFile(Filename: string);
    procedure LoadFromStream(Stream: IisStream);
    function  Read(var Buffer; Count: Longint): Longint; override;
    procedure SaveToFile(Filename: string);
    procedure SaveToStream(Stream: IisStream);
    function  Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
    procedure SetSize(const NewSize: Int64); override;
    procedure Truncate;
    function Write(const Buffer; Count: Longint): Longint; override;
    property Handle: TISC_BLOB_HANDLE read FHandle;
    property BlobID: TISC_QUAD read FBlobID write SetBlobID;
    property BlobMaxSegmentSize: Long read FBlobMaxSegmentSize;
    property BlobNumSegments: Long read FBlobNumSegments;
    property BlobSize: Int64 read FBlobSize;
    property BlobType: Short read FBlobType;
    property Database: TIBDatabase read GetDatabase write SetDatabase;
    property DBHandle: PISC_DB_HANDLE read GetDBHandle;
    property Mode: TBlobStreamMode read FMode write SetMode;
    property Modified: Boolean read FModified;
    property Transaction: TIBTransaction read GetTransaction write SetTransaction;
    property TRHandle: PISC_TR_HANDLE read GetTRHandle;
  end;

  procedure GetBlobInfo(hBlobHandle: PISC_BLOB_HANDLE; var NumSegments, MaxSegmentSize: Long; var TotalSize: Int64; var BlobType: Short);
  procedure ReadBlob(hBlobHandle: PISC_BLOB_HANDLE; Buffer: IisStream; BlobSize: Int64);
  procedure WriteBlob(hBlobHandle: PISC_BLOB_HANDLE; Buffer: IisStream);

implementation

uses IBCustomDataSet;

procedure GetBlobInfo(hBlobHandle: PISC_BLOB_HANDLE; var NumSegments, MaxSegmentSize: Long;
                      var TotalSize: Int64; var BlobType: Short);
var
  items: array[0..3] of Char;
  results: array[0..99] of Char;
  i, item_length: Integer;
  item: Integer;
begin
  items[0] := Char(isc_info_blob_num_segments);
  items[1] := Char(isc_info_blob_max_segment);
  items[2] := Char(isc_info_blob_total_length);
  items[3] := Char(isc_info_blob_type);

  if GetGDSLibrary.isc_blob_info(StatusVector, hBlobHandle, 4, @items[0], SizeOf(results),
                    @results[0]) > 0 then
    IBDatabaseError;

  i := 0;
  while (i < SizeOf(results)) and (results[i] <> Char(isc_info_end)) do
  begin
    item := Integer(results[i]); Inc(i);
    item_length := GetGDSLibrary.isc_vax_integer(@results[i], 2); Inc(i, 2);
    case item of
      isc_info_blob_num_segments:
        NumSegments := GetGDSLibrary.isc_vax_integer(@results[i], item_length);
      isc_info_blob_max_segment:
        MaxSegmentSize := GetGDSLibrary.isc_vax_integer(@results[i], item_length);
      isc_info_blob_total_length:
        TotalSize := GetGDSLibrary.isc_portable_integer(@results[i], item_length);
      isc_info_blob_type:
        BlobType := GetGDSLibrary.isc_vax_integer(@results[i], item_length);
    end;
    Inc(i, item_length);
  end;

  // Now I nee to correct stupid overflow of TotalSize on FB server side. Damn!
  // See details here: http://firebirder.ru/firebird_and_large_blob
  // Aleksey
  TotalSize :=  TotalSize + ((Int64(NumSegments) * Int64(MaxSegmentSize)) div High(Cardinal)) * High(Cardinal);
end;

procedure ReadBlob(hBlobHandle: PISC_BLOB_HANDLE; Buffer: IisStream; BlobSize: Int64);
var
  BytesRead, SegLen: UShort;
  LocalBuffer: PChar;
begin
  Buffer.Size := 0;
  SegLen := UShort(DefaultBlobSegmentSize);
  LocalBuffer := AllocMem(SegLen);
  try
    while (Buffer.Size < BlobSize) do
    begin
      if (Buffer.Position + SegLen > BlobSize) then
        SegLen := BlobSize - Buffer.Position;

      BytesRead := 0;
      if not ((GetGDSLibrary.isc_get_segment(StatusVector, hBlobHandle, @BytesRead, SegLen, LocalBuffer) = 0) or (StatusVectorArray[1] = isc_segment)) then
        IBDatabaseError;

      Buffer.WriteBuffer(LocalBuffer^, BytesRead);
    end;
  finally
    FreeMem(LocalBuffer);
  end;

  Buffer.Position := 0;
end;

procedure WriteBlob(hBlobHandle: PISC_BLOB_HANDLE; Buffer: IisStream);
var
  SegLen: Long;
  LocalBuffer: PChar;
begin
  SegLen := DefaultBlobSegmentSize;
  LocalBuffer := AllocMem(SegLen);
  Buffer.Position := 0;
  try
    while not Buffer.EOS  do
    begin
      if (Buffer.Position + SegLen > Buffer.Size) then
        SegLen := Buffer.Size - Buffer.Position;
      Buffer.Read(LocalBuffer^, SegLen);
      if GetGDSLibrary.isc_put_segment(StatusVector, hBlobHandle, SegLen, LocalBuffer) > 0 then
        IBDatabaseError;
    end;
  finally
    FreeMem(LocalBuffer);
  end;
end;


{ TIBBlobStream }
constructor TIBBlobStream.Create;
begin
  inherited Create;
  FBase := TIBBase.Create(Self);
  FBuffer := TisStream.Create;
  FBlobSize := 0;
  FGDSLibrary := GetGDSLibrary;
end;

destructor TIBBlobStream.Destroy;
begin
  if (FHandle <> nil) and
     (Call(FGDSLibrary.isc_close_blob(StatusVector, @FHandle), False) > 0) then
    IBDataBaseError;
  FBase.Free;
  SetSize(Int64(0));
  FGDSLibrary := nil;
  inherited Destroy;
end;

function TIBBlobStream.Call(ErrCode: ISC_STATUS; RaiseError: Boolean): ISC_STATUS;
begin
  result := 0;
  if Transaction <> nil then
    result := Transaction.Call(ErrCode, RaiseError)
  else
    if RaiseError and (ErrCode > 0) then
      IBDataBaseError;
end;

procedure TIBBlobStream.CheckReadable;
begin
  if FMode = bmWrite then
    IBError(ibxeBlobCannotBeRead, [nil]);
end;

procedure TIBBlobStream.CheckWritable;
begin
  if FMode = bmRead then
    IBError(ibxeBlobCannotBeWritten, [nil]);
end;

procedure TIBBlobStream.CloseBlob;
begin
  Finalize;
  if (FHandle <> nil) and
     (Call(FGDSLibrary.isc_close_blob(StatusVector, @FHandle), False) > 0) then
    IBDataBaseError;
end;

procedure TIBBlobStream.CreateBlob;
begin
  CheckWritable;
  FBlobID.gds_quad_high := 0;
  FBlobID.gds_quad_low := 0;
  Truncate;
end;

procedure TIBBlobStream.EnsureBlobInitialized;
begin
  if not FBlobInitialized then
    case FMode of
      bmWrite:
        CreateBlob;
      bmReadWrite: begin
        if (FBlobID.gds_quad_high = 0) and
           (FBlobID.gds_quad_low = 0) then
          CreateBlob
        else
          OpenBlob;
      end;
      else
        OpenBlob;
    end;
  FBlobInitialized := True;
end;

procedure TIBBlobStream.Finalize;
begin
  if (not FBlobInitialized) or (FMode = bmRead) then
    exit;
  { need to start writing to a blob, create one }
  Call(FGDSLibrary.isc_create_blob2(StatusVector, DBHandle, TRHandle, @FHandle, @FBlobID,
                       0, nil), True);

  IBBlob.WriteBlob(@FHandle, FBuffer);

  Call(FGDSLibrary.isc_close_blob(StatusVector, @FHandle), True);
  FModified := False;
end;

procedure TIBBlobStream.GetBlobInfo;
var
  iBlobSize: Int64;
begin
  IBBlob.GetBlobInfo(@FHandle, FBlobNumSegments, FBlobMaxSegmentSize,
    iBlobSize, FBlobType);
  SetSize(iBlobSize);
end;

function TIBBlobStream.GetDatabase: TIBDatabase;
begin
  result := FBase.Database;
end;

function TIBBlobStream.GetDBHandle: PISC_DB_HANDLE;
begin
  result := FBase.DBHandle;
end;

function TIBBlobStream.GetTransaction: TIBTransaction;
begin
  result := FBase.Transaction;
end;

function TIBBlobStream.GetTRHandle: PISC_TR_HANDLE;
begin
  result := FBase.TRHandle;
end;

procedure TIBBlobStream.LoadFromFile(Filename: string);
var
  Stream: IisStream;
begin
  Stream := TisStream.CreateFromFile(FileName);
  LoadFromStream(Stream);
end;

procedure TIBBlobStream.LoadFromStream(Stream: IisStream);
begin
  CheckWritable;
  EnsureBlobInitialized;
  Stream.Position := 0;
  FBuffer.CopyFrom(Stream, 0);
  FModified := True;
end;

procedure TIBBlobStream.OpenBlob;
begin
  CheckReadable;
  Call(FGDSLibrary.isc_open_blob2(StatusVector, DBHandle, TRHandle, @FHandle,
                     @FBlobID, 0, nil), True);
  try
    GetBlobInfo;
    SetSize(FBlobSize);
    IBBlob.ReadBlob(@FHandle, FBuffer, FBlobSize);
  except
    Call(FGDSLibrary.isc_close_blob(StatusVector, @FHandle), False);
    raise;
  end;
  Call(FGDSLibrary.isc_close_blob(StatusVector, @FHandle), True);
end;

function TIBBlobStream.Read(var Buffer; Count: Longint): Longint;
begin
  CheckReadable;
  EnsureBlobInitialized;

  if (Count <= 0) then
  begin
    result := 0;
    exit;
  end;

  Result := FBuffer.Read(Buffer, Count);
end;

procedure TIBBlobStream.SaveToFile(Filename: string);
var
  Stream: IisStream;
begin
  Stream := TisStream.CreateFromNewFile(FileName);
  SaveToStream(Stream);
end;

procedure TIBBlobStream.SaveToStream(Stream: IisStream);
begin
  CheckReadable;
  EnsureBlobInitialized;
  if FBlobSize <> 0 then
    Stream.CopyFrom(FBuffer, 0);
end;

function TIBBlobStream.Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
begin
  EnsureBlobInitialized;
  FBuffer.Seek(Offset, Origin);
  result := FBuffer.Position;
end;

procedure TIBBlobStream.SetBlobID(Value: TISC_QUAD);
begin
  System.Move(Value, FBlobID, SizeOf(TISC_QUAD));
  FBlobInitialized := False;
end;

procedure TIBBlobStream.SetDatabase(Value: TIBDatabase);
begin
  FBase.Database := Value;
  FBlobInitialized := False;
end;

procedure TIBBlobStream.SetMode(Value: TBlobStreamMode);
begin
  FMode := Value;
  FBlobInitialized := False;
end;

procedure TIBBlobStream.SetSize(const NewSize: Int64);
begin
  if (NewSize <> FBlobSize) then
  begin
    FBlobSize := NewSize;
    FBuffer.Size := FBlobSize;
  end;
end;

procedure TIBBlobStream.SetTransaction(Value: TIBTransaction);
begin
  FBase.Transaction := Value;
  FBlobInitialized := False;
end;

procedure TIBBlobStream.Truncate;
begin
  SetSize(Int64(0));
end;

function TIBBlobStream.Write(const Buffer; Count: Longint): Longint;
begin
  CheckWritable;
  EnsureBlobInitialized;
  result := Count;

  if Count <= 0 then
    exit;

  FBuffer.WriteBuffer(Buffer, Count);
  FModified := True;
end;

procedure TIBBlobStream.Cancel;
begin
  if (not FBlobInitialized) or (FMode = bmRead) then
    exit;
  if FModified then
    OpenBlob;
  FModified := False;
end;

end.
