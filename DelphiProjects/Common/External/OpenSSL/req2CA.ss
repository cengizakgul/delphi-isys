Certificate Request:
    Data:
        Version: 0 (0x0)
        Subject: C=AU, O=Dodgy Brothers, CN=Dodgy CA
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
            RSA Public Key: (1024 bit)
                Modulus (1024 bit):
                    00:c0:44:e5:d6:90:18:2a:14:2c:fa:7a:bd:80:fb:
                    cf:03:d6:fc:2a:14:91:10:a0:a6:47:eb:43:52:9a:
                    c2:2a:dc:1f:52:81:35:0d:fe:86:c4:45:b7:d7:6a:
                    23:93:77:c2:e8:3c:2e:11:42:8a:8a:f4:82:fa:a7:
                    f4:62:db:6c:d7:5e:df:6f:49:ff:3a:bb:ef:50:19:
                    32:97:ca:29:83:ba:05:21:cc:24:c3:ad:b2:ab:e5:
                    55:3e:c4:fa:74:9e:ff:a4:03:84:45:cf:6d:30:bf:
                    5b:f6:d9:8d:f9:ff:4a:ad:f3:4d:1f:3b:d8:cb:a2:
                    d1:5e:82:71:90:b4:5d:fc:03
                Exponent: 65537 (0x10001)
        Attributes:
            a0:00
    Signature Algorithm: sha1WithRSAEncryption
        95:e4:86:1d:4c:49:ad:60:35:8b:e1:37:e6:87:a3:c5:98:a2:
        3d:31:a3:9a:93:2c:1a:65:e2:5a:d9:11:32:89:c6:7e:15:e4:
        74:27:ba:eb:ec:fe:d1:0f:a6:9a:73:bb:d4:b7:d5:4b:4b:46:
        d9:98:57:82:59:bc:a4:7f:79:78:24:a7:63:82:92:34:aa:25:
        cb:ad:74:a1:35:b4:cc:fe:b6:bc:d3:00:9f:a5:c2:1a:a8:16:
        2e:f1:32:a1:7e:bc:09:f2:c9:fc:8a:e1:89:af:3d:26:f2:17:
        3d:bb:c9:1f:d3:05:d0:ed:73:20:7a:81:37:37:f0:22:73:55:
        8b:04
-----BEGIN CERTIFICATE REQUEST-----
MIIBeDCB4gIBADA5MQswCQYDVQQGEwJBVTEXMBUGA1UEChMORG9kZ3kgQnJvdGhl
cnMxETAPBgNVBAMTCERvZGd5IENBMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKB
gQDAROXWkBgqFCz6er2A+88D1vwqFJEQoKZH60NSmsIq3B9SgTUN/obERbfXaiOT
d8LoPC4RQoqK9IL6p/Ri22zXXt9vSf86u+9QGTKXyimDugUhzCTDrbKr5VU+xPp0
nv+kA4RFz20wv1v22Y35/0qt800fO9jLotFegnGQtF38AwIDAQABoAAwDQYJKoZI
hvcNAQEFBQADgYEAleSGHUxJrWA1i+E35oejxZiiPTGjmpMsGmXiWtkRMonGfhXk
dCe66+z+0Q+mmnO71LfVS0tG2ZhXglm8pH95eCSnY4KSNKoly610oTW0zP62vNMA
n6XCGqgWLvEyoX68CfLJ/Irhia89JvIXPbvJH9MF0O1zIHqBNzfwInNViwQ=
-----END CERTIFICATE REQUEST-----
