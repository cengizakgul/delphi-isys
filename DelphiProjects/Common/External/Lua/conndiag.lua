--[[

This is a lua script that drives connection diagnostics process
Information on the lua language syntax 
is available at http://www.lua.org/docs.html

]]

-- SB specific parameters -----------------------------------------------------

-- SB host where evolution server runs ('Server' field in the Evo login dialog)
sb_host = 'www.isystemsllc.com'   --change it before release!!!

-- SB host where SB's webserver is running if any
-- set to nil if none
-- sb_webhost = nil
sb_webhost = 'www.paydata.com'   --change it before release!!!

-- SB host where SB's mailserver is running if any
-- set to nil if none
-- sb_mailhost = nil
sb_mailhost = 'pop.paydata.com'
--sb_mailhost = 'pop.yaroslavl.ru' --remove it before release!!!

-- hardcoded DNS server that is used in case when default dns server 
-- is unavailable if any
-- set to nil if none
alternative_dns = 'univ.uniyar.ac.ru' --change it before release!!!

-- Common parameters ----------------------------------------------------------

-- well known host to check connectivity to the Internet
well_known_host = 'www.yahoo.com'

-------------------------------------------------------------------------------

--[[

The following diagnostic functions are available:

print( str ) - puts str in report

The functions below in case of error return two values:
  1. nil	
  2. error message

execute( cmdline ) -
  executes cmdline 
  returns two values:
    1. exit code of program
    2. output of program 

resolve( host [, dnsserv] ) - 
  makes DNS request for host (using default dns
  server or a particular dns server passed in optional 
  parameter dnsserv)
  returns string with IP address of a given host

head( url ) - 
  makes HEAD http request for a given url
  returns server response

tcp_ping( host, port ) -
  tries to establish connection to a host on a given port 
  and immediately closes it
  on success returns boolean value of true

Also all standard lua library functions are available.

]]



-- executes a command
function exec(cmdline)
    print( "*** Started '" .. cmdline .. "'")
	exit_code, output = execute(cmdline)
	if exit_code == nil then
	    print( "*** Failed to execute '" .. cmdline .. "', error: " .. output);
	else	
		print( output )
    	print( "*** Finished '" .. cmdline .. "', exit code: " .. exit_code);
	end
    print( "\n" ); --new line
end

function resolve_alt(host)
   	ip, err = resolve(host, alternative_dns)
	return assert(ip, "Failed to resolve "..host.." using " .. alternative_dns .. ", error: "..err)
end 

function http_ping(host)
	url = "http://" .. host
    print("***Sending HEAD request to '"..url.."'")
    resp, err = head(url)
    if resp == nil then
		print("***Failed to send HEAD request to "..url..", error: "..err);
	else	
		diag = "";
		if string.find(resp,"HTTP") == nil then
			diag = ", it doesn't look like HTTP respose"
		end
		print("***Got response from '"..url.."': '"..resp.."'"..diag)
	end
    print( "\n" ); 
end

function tcp_ping_evo(host, port)
	print("*** Connecting to "..host..":"..port)
	retv, err = tcp_ping(host, port)
	if retv == nil then
		print("*** Failed to establish connection to "..host..":"..port..", error:"..err)		
	else
		print("*** Successfully established connection to "..host..":"..port)
	end
	print("\n")
end

function pop3_ping(host)
	tcp_ping_evo(host, 110)
end;

function dump_params()
	print( 'Parameters:')
	print( 'sb_host = '..sb_host )
	print( 'sb_webhost = '..sb_webhost)
	print( 'sb_mailhost = '..sb_mailhost)
	print( 'alternative_dns = '..alternative_dns)
	print( 'well_known_host = '..well_known_host)
	print( '\n')
end

function diagnose()

    -- show current parameters (IP address, netmask, gateway, DNS servers)
    exec( 'ipconfig /all')

    -- validate DNS lookups
    exec( 'nslookup ' .. well_known_host)
    exec( 'nslookup ' .. sb_host)
	if sb_webhost then
	    exec( 'nslookup ' .. sb_webhost)
	end
	if sb_mailhost then
	    exec( 'nslookup ' .. sb_mailhost)
	end

    if resolve(well_known_host) == nil then
		if alternative_dns then
            print( "*** Trying to use hardcoded dns server (".. alternative_dns ..")")
            exec( 'nslookup ' .. well_known_host .. ' '.. alternative_dns)
            well_known_host = resolve_alt(well_known_host)
            sb_host = resolve_alt(sb_host)
            if sb_webhost then
               sb_webhost = resolve_alt(sb_webhost)
            end
            if sb_mailhost then
               sb_mailhost = resolve_alt(sb_mailhost)
            end
		else
			print( "DNS service is not available. Test cancelled.")
			return;
		end
    end
    
    -- verify IP connectivity to the internet at large
	http_ping(well_known_host)

	-- verify IP connectivity to the SB office for http
	if sb_webhost then
		http_ping(sb_webhost)
	end
	if sb_mailhost then
		pop3_ping(sb_mailhost)
	end
	
	-- verify connection to servers at port 9901..9903
	tcp_ping_evo(sb_host, 9901);
	tcp_ping_evo(sb_host, 9902);
	tcp_ping_evo(sb_host, 9903);
end


print( 'Connection test\n')

dump_params()
diagnose()

print("\n")
print( 'End of connection test')


