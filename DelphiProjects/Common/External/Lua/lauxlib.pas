(*
** $Id: lauxlib.pas,v 1.1 2005/04/24 19:31:20 jfgoulet Exp $
** Auxiliary functions for building Lua libraries
** See Copyright Notice in lua.h
*)
unit lauxlib;

{$IFNDEF lauxlib_h}
{$DEFINE lauxlib_h}
{$ENDIF}

interface

uses
  lua;

function IsLoaded: boolean;
procedure Init;

type
  luaL_reg = record
    name: PChar;
    func: lua_CFunction;
  end;
  PluaL_reg = ^luaL_reg;

type TluaL_openlib = procedure(L: Plua_State; libname: PChar; R: PluaL_reg; Nup: Integer); cdecl;
var luaL_openlib: TluaL_openlib;
type TluaL_getmetafield = function(L: Plua_State; Obj: Integer; const E: PChar): Integer; cdecl;
var luaL_getmetafield: TluaL_getmetafield;
type TluaL_callmeta = function(L: Plua_State; Obj: Integer; const E: PChar): Integer; cdecl;
var luaL_callmeta: TluaL_callmeta;
type TluaL_typerror = function(L: Plua_State; NArg: Integer; const TName: PChar): Integer; cdecl;
var luaL_typerror: TluaL_typerror;
type TluaL_argerror = function(L: Plua_State; NumArg: Integer; const ExtraMsg: PChar): Integer; cdecl;
var luaL_argerror: TluaL_argerror;
type TluaL_checklstring = function(L: Plua_State; NumArg: Integer; S: Psize_t): PChar; cdecl;
var luaL_checklstring: TluaL_checklstring;
type TluaL_optlstring = function(L: Plua_State; NumArg: Integer; const Def: PChar; S: Psize_t): PChar; cdecl;
var luaL_optlstring: TluaL_optlstring;
type TluaL_checknumber = function(L: Plua_State; NumArg: Integer): lua_Number; cdecl;
var luaL_checknumber: TluaL_checknumber;
type TluaL_optnumber = function(L: Plua_State; NArg: Integer; Def: Lua_Number): lua_Number; cdecl;
var luaL_optnumber: TluaL_optnumber;

type TluaL_checkstack = procedure(L: Plua_State; SZ: Integer; const Msg: PChar); cdecl;
var luaL_checkstack: TluaL_checkstack;
type TluaL_checktype = procedure(L: Plua_State; NArg: Integer; T: Integer); cdecl;
var luaL_checktype: TluaL_checktype;
type TluaL_checkany = procedure(L: Plua_State; NArg: Integer); cdecl;
var luaL_checkany: TluaL_checkany;

type TluaL_newmetatable = function(L: Plua_State; const TName: PChar): Integer; cdecl;
var luaL_newmetatable: TluaL_newmetatable;
type TluaL_getmetatable = procedure(L: Plua_State; const TName: PChar); cdecl;
var luaL_getmetatable: TluaL_getmetatable;
type TluaL_checkudata = function(L: Plua_State; UD: Integer; const TName: PChar): Pointer; cdecl;
var luaL_checkudata: TluaL_checkudata;

type TluaL_where = procedure(L: Plua_State; LVL: Integer); cdecl;
var luaL_where: TluaL_where;
type TluaL_error = function(L: Plua_State; const Fmt: PChar): Integer;  cdecl;
var luaL_error: TluaL_error;

type TluaL_findstring = function(const ST: PChar; lst: array of PChar): Integer; cdecl;
var luaL_findstring: TluaL_findstring;

type TluaL_ref = function(L: Plua_State; T: Integer): Integer; cdecl;
var luaL_ref: TluaL_ref;
type TluaL_unref = procedure(L: Plua_State; T: Integer; Ref: Integer); cdecl;
var luaL_unref: TluaL_unref;

type TluaL_getn = function(L: Plua_State; T: Integer): Integer; cdecl;
var luaL_getn: TluaL_getn;
type TluaL_setn = procedure(L: Plua_State; T: Integer; N: Integer); cdecl;
var luaL_setn: TluaL_setn;

type TluaL_loadfile = function(L: Plua_State; const FileName: PChar): Integer; cdecl;
var luaL_loadfile: TluaL_loadfile;
type TluaL_loadbuffer = function(L: Plua_State; const Buff: PChar; SZ: size_t; const Name: PChar): Integer; cdecl;
var luaL_loadbuffer: TluaL_loadbuffer;



(*
** ===============================================================
** some useful macros
** ===============================================================
*)

function luaL_argcheck(L: Plua_State; Cond: Boolean; NumArg: Integer; const ExtraMsg: PChar): Integer;
function luaL_checkstring(L: Plua_State; N: Integer): PChar;
function luaL_optstring(L: Plua_State; N: Integer; const D: PChar): PChar;
function luaL_checkint(L: Plua_State; N: Integer): Integer;
function luaL_checklong(L: Plua_State; N: Integer): Integer;
function luaL_optint(L: Plua_State; N: Integer; D: Lua_Number): Integer;
function luaL_optlong(L: Plua_State; N: Integer; D: Lua_Number): Integer;

(*
** {======================================================
** Generic Buffer manipulation
** =======================================================
*)


{$IFNDEF BUFSIZ}
const
  BUFSIZ = 1024;
{$ENDIF}

{$IFNDEF LUAL_BUFFERSIZE}
const
  LUAL_BUFFERSIZE = BUFSIZ;
{$ENDIF}

type
  luaL_Buffer = record
    P: PChar;    (* current position in buffer *)
    lvl: Integer; (* number of strings in the stack(level) *)
    L: Plua_State;
    buffer: array [0..LUAL_BUFFERSIZE - 1] of Char;
  end;
  PluaL_Buffer = ^luaL_Buffer;

procedure luaL_putchar(B: PluaL_Buffer; C: Char);

function luaL_addsize(B: PLuaL_Buffer; N: Integer): PChar;

type TluaL_buffinit = procedure(L: Plua_State; B: PluaL_Buffer); cdecl;
var luaL_buffinit: TluaL_buffinit;
type TluaL_prepbuffer = function(B: PluaL_Buffer): PChar; cdecl;
var luaL_prepbuffer: TluaL_prepbuffer;
type TluaL_addlstring = procedure(B: PluaL_Buffer; const S: PChar; L: size_t); cdecl;
var luaL_addlstring: TluaL_addlstring;
type TluaL_addstring = procedure(B: PluaL_Buffer; const S: PChar); cdecl;
var luaL_addstring: TluaL_addstring;
type TluaL_addvalue = procedure(B: PluaL_Buffer); cdecl;
var luaL_addvalue: TluaL_addvalue;
type TluaL_pushresult = procedure(B: PluaL_Buffer); cdecl;
var luaL_pushresult: TluaL_pushresult;


(* }====================================================== *)



(*
** Compatibility macros and functions
*)

type Tlua_dofile = function(L: Plua_State; const FileName: PChar): Integer; cdecl;
var lua_dofile: Tlua_dofile;
type Tlua_dostring = function(L: Plua_State; const Str: PChar): Integer; cdecl;
var lua_dostring: Tlua_dostring;
type Tlua_dobuffer = function(L: Plua_State; const Buff: PChar; SZ: size_t; const N: PChar): Integer; cdecl;
var lua_dobuffer: Tlua_dobuffer;

function luaL_check_lstr(L: Plua_State; NumArg: Integer; S: Psize_t): PChar;
function luaL_opt_lstr(L: Plua_State; NumArg: Integer;
                                           const Def: PChar; S: Psize_t): PChar;
function luaL_check_number(L: Plua_State; NumArg: Integer): lua_Number;
function luaL_opt_number(L: Plua_State; NArg: Integer; Def: Lua_Number): lua_Number;
procedure luaL_arg_check(L: Plua_State; Cond: Boolean; NumArg: Integer; const ExtraMsg: PChar);
function luaL_check_string(L: Plua_State; N: Integer): PChar;
function luaL_opt_string(L: Plua_State; N: Integer; const D: PChar): PChar;
function luaL_check_int(L: Plua_State; N: Integer): Integer;
function luaL_check_long(L: Plua_State; N: Integer): Integer;
function luaL_opt_int(L: Plua_State; N: Integer; D: Lua_Number): Integer;
function luaL_opt_long(L: Plua_State; N: Integer; D: Lua_Number): Integer;

implementation

uses
	sysutils, windows;

function luaL_argcheck(L: Plua_State; Cond: Boolean; NumArg: Integer; const ExtraMsg: PChar): Integer;
begin
 Result := 0;
 if(not Cond) then
   Result := luaL_argerror(L, NumArg, ExtraMsg)
end;

function luaL_checkstring(L: Plua_State; N: Integer): PChar;
begin
  Result := luaL_checklstring(L, N, nil);
end;

function luaL_optstring(L: Plua_State; N: Integer; const D: PChar): PChar;
begin
  Result := luaL_optlstring(L, N, D, nil);
end;

function luaL_checkint(L: Plua_State; N: Integer): Integer;
begin
  Result := Trunc(luaL_checknumber(L, N));
end;

function luaL_checklong(L: Plua_State; N: Integer): Integer;
begin
  Result := Trunc(luaL_checknumber(L, N));
end;

function luaL_optint(L: Plua_State; N: Integer; D: Lua_Number): Integer;
begin
  Result := Trunc(luaL_optnumber(L, N, D));
end;

function luaL_optlong(L: Plua_State; N: Integer; D: Lua_Number): Integer;
begin
  Result := Trunc(luaL_optnumber(L, N, D));
end;

procedure luaL_putchar(B: PluaL_Buffer; C: Char);
begin
  if ((B.P <= @B.Buffer[High(B.Buffer)]) or (luaL_prepbuffer(B) <> #0)) then
  begin
    B.P^ := C;
    Inc(B.P);
  end;
end;

function luaL_addsize(B: PLuaL_Buffer; N: Integer): PChar;
begin
  Inc(B.P, N);
  Result := B.P;
end;


function luaL_check_lstr(L: Plua_State; NumArg: Integer; S: Psize_t): PChar;
begin
  Result := luaL_checklstring(L, NumArg, S);
end;

function luaL_opt_lstr(L: Plua_State; NumArg: Integer;
                                           const Def: PChar; S: Psize_t): PChar;
begin
  Result := luaL_optlstring(L, NumArg, Def, S);
end;

function luaL_check_number(L: Plua_State; NumArg: Integer): lua_Number;
begin
  Result := luaL_checknumber(L, NumArg);
end;

function luaL_opt_number(L: Plua_State; NArg: Integer; Def: Lua_Number): lua_Number;
begin
  Result := luaL_optnumber(L, NArg, Def);
end;

procedure luaL_arg_check(L: Plua_State; Cond: Boolean; NumArg: Integer; const ExtraMsg: PChar);
begin
  luaL_argcheck(L, Cond, NumArg, ExtraMsg);
end;

function luaL_check_string(L: Plua_State; N: Integer): PChar;
begin
  Result := luaL_checkstring(L, N);
end;

function luaL_opt_string(L: Plua_State; N: Integer; const D: PChar): PChar;
begin
  Result := luaL_optstring(L, N, D);
end;

function luaL_check_int(L: Plua_State; N: Integer): Integer;
begin
  Result := luaL_checkint(L, N);
end;

function luaL_check_long(L: Plua_State; N: Integer): Integer;
begin
  Result := luaL_checklong(L, N);
end;

function luaL_opt_int(L: Plua_State; N: Integer; D: Lua_Number): Integer;
begin
  Result := luaL_optint(L, N, D);
end;

function luaL_opt_long(L: Plua_State; N: Integer; D: Lua_Number): Integer;
begin
  Result := luaL_optlong(L, N, D);
end;

var
	hlua: HMODULE;

function IsLoaded: boolean;
begin
	Result := hlua <> 0;
end;

procedure Init;
begin
	hlua := SafeLoadLibrary('lua.dll');
	if hlua > HINSTANCE_ERROR then
	begin
		@luaL_openlib := GetProcAddress(hlua, 'luaL_openlib');
		@luaL_getmetafield := GetProcAddress(hlua, 'luaL_getmetafield');
		@luaL_callmeta := GetProcAddress(hlua, 'luaL_callmeta');
		@luaL_typerror := GetProcAddress(hlua, 'luaL_typerror');
		@luaL_argerror := GetProcAddress(hlua, 'luaL_argerror');
		@luaL_checklstring := GetProcAddress(hlua, 'luaL_checklstring');
		@luaL_optlstring := GetProcAddress(hlua, 'luaL_optlstring');
		@luaL_checknumber := GetProcAddress(hlua, 'luaL_checknumber');
		@luaL_optnumber := GetProcAddress(hlua, 'luaL_optnumber');
		@luaL_checkstack := GetProcAddress(hlua, 'luaL_checkstack');
		@luaL_checktype := GetProcAddress(hlua, 'luaL_checktype');
		@luaL_checkany := GetProcAddress(hlua, 'luaL_checkany');
		@luaL_newmetatable := GetProcAddress(hlua, 'luaL_newmetatable');
		@luaL_getmetatable := GetProcAddress(hlua, 'luaL_getmetatable');
		@luaL_checkudata := GetProcAddress(hlua, 'luaL_checkudata');
		@luaL_where := GetProcAddress(hlua, 'luaL_where');
		@luaL_error := GetProcAddress(hlua, 'luaL_error');
		@luaL_findstring := GetProcAddress(hlua, 'luaL_findstring');
		@luaL_ref := GetProcAddress(hlua, 'luaL_ref');
		@luaL_unref := GetProcAddress(hlua, 'luaL_unref');
		@luaL_getn := GetProcAddress(hlua, 'luaL_getn');
		@luaL_setn := GetProcAddress(hlua, 'luaL_setn');
		@luaL_loadfile := GetProcAddress(hlua, 'luaL_loadfile');
		@luaL_loadbuffer := GetProcAddress(hlua, 'luaL_loadbuffer');
		@luaL_buffinit := GetProcAddress(hlua, 'luaL_buffinit');
		@luaL_prepbuffer := GetProcAddress(hlua, 'luaL_prepbuffer');
		@luaL_addlstring := GetProcAddress(hlua, 'luaL_addlstring');
		@luaL_addstring := GetProcAddress(hlua, 'luaL_addstring');
		@luaL_addvalue := GetProcAddress(hlua, 'luaL_addvalue');
		@luaL_pushresult := GetProcAddress(hlua, 'luaL_pushresult');
		@lua_dofile := GetProcAddress(hlua, 'lua_dofile');
		@lua_dostring := GetProcAddress(hlua, 'lua_dostring');
		@lua_dobuffer := GetProcAddress(hlua, 'lua_dobuffer');
	
	end
end;

procedure Deinit;
begin
  if hlua > HINSTANCE_ERROR then
  begin
    FreeLibrary(hlua);
    hlua := 0;
  end;
end;

initialization
	Init;

finalization
  Deinit;
end.


