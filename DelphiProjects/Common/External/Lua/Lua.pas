(*
** $Id: lua.pas,v 1.1 2005/04/24 19:31:20 jfgoulet Exp $
** Lua - An Extensible Extension Language
** Tecgraf: Computer Graphics Technology Group, PUC-Rio, Brazil
** http://www.lua.org	mailto:info@lua.org
** See Copyright Notice at the end of this file
*)
unit lua;

{$IFNDEF lua_h}
{$DEFINE lua_h}
{$ENDIF}

interface

function IsLoaded: boolean;
procedure Init;

const
 CONST_LUA_VERSION = 'Lua 5.0';
 CONST_LUA_COPYRIGHT = 'Copyright (C) 1994-2003 Tecgraf, PUC-Rio';
 CONST_LUA_AUTHORS = 'R. Ierusalimschy, L. H. de Figueiredo & W. Celes';

(* option for multiple returns in `lua_pcall' and `lua_call' *)
 LUA_MULTRET = (-1);


(*
** pseudo-indices
*)
  LUA_REGISTRYINDEX = (-10000);
  LUA_GLOBALSINDEX = (-10001);

  function lua_upvalueindex(I: Integer): Integer;

const

(* error codes for `lua_load' and `lua_pcall' *)
  LUA_ERRRUN = 1;
  LUA_ERRFILE = 2;
  LUA_ERRSYNTAX = 3;
  LUA_ERRMEM = 4;
  LUA_ERRERR = 5;


type
  lua_State = record
  end;
  Plua_State = ^lua_State;

  lua_CFunction = function (L: Plua_State): Integer; cdecl;
  size_t = Integer;
  Psize_t = ^size_t;

(*
** functions that read/write blocks when loading/dumping Lua chunks
*)
  lua_Chunkreader = function (L: Plua_State; UD: Pointer; var SZ: size_t): PChar;
  lua_Chunkwriter = function (L: Plua_State; const P: Pointer; SZ: size_t; UD: Pointer): Integer;

const
(*
** basic types
*)
  LUA_TNONE = (-1);

  LUA_TNIL = 0;
  LUA_TBOOLEAN = 1;
  LUA_TLIGHTUSERDATA = 2;
  LUA_TNUMBER = 3;
  LUA_TSTRING = 4;
  LUA_TTABLE = 5;
  LUA_TFUNCTION = 6;
  LUA_TUSERDATA = 7;
  LUA_TTHREAD = 8;


(* minimum Lua stack available to a C function *)
  LUA_MINSTACK = 20;


(*
** generic extra include file
*)
{$IFDEF LUA_USER_H}
{$INCLUDE} LUA_USER_H
{$ENDIF}


(* type of numbers in Lua *)
type
{$IFNDEF LUA_NUMBER}
  lua_Number = Double;
{$ELSE}
  lua_Number = LUA_NUMBER;
{$ENDIF}


(* mark for all API functions *)


(*
** state manipulation
*)
type Tlua_open = function: Plua_State; cdecl;
var lua_open: Tlua_open;
type Tlua_close = procedure(L: Plua_State); cdecl;
var lua_close: Tlua_close;
type Tlua_newthread = function(L: Plua_State): Plua_State; cdecl;
var lua_newthread: Tlua_newthread;

type Tlua_atpanic = function(L: Plua_State; Panicf: lua_CFunction): lua_CFunction; cdecl;
var lua_atpanic: Tlua_atpanic;


(*
** basic stack manipulation
*)
type Tlua_gettop = function(L: Plua_State): Integer; cdecl;
var lua_gettop: Tlua_gettop;
type Tlua_settop = procedure(L: Plua_State; Idx: Integer); cdecl;
var lua_settop: Tlua_settop;
type Tlua_pushvalue = procedure(L: Plua_State; Idx: Integer); cdecl;
var lua_pushvalue: Tlua_pushvalue;
type Tlua_remove = procedure(L: Plua_State; Idx: Integer); cdecl;
var lua_remove: Tlua_remove;
type Tlua_insert = procedure(L: Plua_State; Idx: Integer); cdecl;
var lua_insert: Tlua_insert;
type Tlua_replace = procedure(L: Plua_State; Idx: Integer); cdecl;
var lua_replace: Tlua_replace;
type Tlua_checkstack = function(L: Plua_State; SZ: Integer): Integer; cdecl;
var lua_checkstack: Tlua_checkstack;

type Tlua_xmove = procedure (Src, Dst: Plua_State; N: Integer); cdecl;
var lua_xmove: Tlua_xmove;


(*
** access functions (stack -> C)
*)

type Tlua_isnumber = function(L: Plua_State; Idx: Integer): Integer; cdecl;
var lua_isnumber: Tlua_isnumber;
type Tlua_isstring = function(L: Plua_State; Idx: Integer): Integer; cdecl;
var lua_isstring: Tlua_isstring;
type Tlua_iscfunction = function(L: Plua_State; Idx: Integer): Integer; cdecl;
var lua_iscfunction: Tlua_iscfunction;
type Tlua_isuserdata = function(L: Plua_State; Idx: Integer): Integer; cdecl;
var lua_isuserdata: Tlua_isuserdata;
type Tlua_type = function(L: Plua_State; Idx: Integer): Integer; cdecl;
var lua_type: Tlua_type;
type Tlua_typename = function(L: Plua_State; TP: Integer): PChar; cdecl;
var lua_typename: Tlua_typename;

type Tlua_equal = function(L: Plua_State; Idx1: Integer; Idx2: Integer): Integer; cdecl;
var lua_equal: Tlua_equal;
type Tlua_rawequal = function(L: Plua_State; Idx1: Integer; Idx2: Integer): Integer; cdecl;
var lua_rawequal: Tlua_rawequal;
type Tlua_lessthan = function(L: Plua_State; Idx1: Integer; Idx2: Integer): Integer; cdecl;
var lua_lessthan: Tlua_lessthan;

type Tlua_tonumber = function(L: Plua_State; Idx: Integer): lua_Number; cdecl;
var lua_tonumber: Tlua_tonumber;
type Tlua_toboolean = function(L: Plua_State; Idx: Integer): Integer; cdecl;
var lua_toboolean: Tlua_toboolean;
type Tlua_tostring = function(L: Plua_State; Idx: Integer): PChar; cdecl;
var lua_tostring: Tlua_tostring;
type Tlua_strlen = function(L: Plua_State; Idx: Integer): size_t; cdecl;
var lua_strlen: Tlua_strlen;
type Tlua_tocfunction = function(L: Plua_State; Idx: Integer): lua_CFunction; cdecl;
var lua_tocfunction: Tlua_tocfunction;
type Tlua_touserdata = function(L: Plua_State; Idx: Integer): Pointer; cdecl;
var lua_touserdata: Tlua_touserdata;
type Tlua_tothread = function(L: Plua_State; Idx: Integer): Plua_State; cdecl;
var lua_tothread: Tlua_tothread;
type Tlua_topointer = function(L: Plua_State; Idx: Integer): Pointer; cdecl;
var lua_topointer: Tlua_topointer;


(*
** push functions (C -> stack)
*)
type Tlua_pushnil = procedure(L: Plua_State); cdecl;
var lua_pushnil: Tlua_pushnil;
type Tlua_pushnumber = procedure(L: Plua_State; N: lua_Number); cdecl;
var lua_pushnumber: Tlua_pushnumber;
type Tlua_pushlstring = procedure(L: Plua_State; const S: PChar; N: size_t); cdecl;
var lua_pushlstring: Tlua_pushlstring;
type Tlua_pushstring = procedure(L: Plua_State; const S: PChar); cdecl;
var lua_pushstring: Tlua_pushstring;
type Tlua_pushvfstring = function(L: Plua_State; const Fmt: PChar; Argp: Pointer): PChar; cdecl;
var lua_pushvfstring: Tlua_pushvfstring;
type Tlua_pushfstring = function(L: Plua_State; const Fmt: PChar): PChar;  cdecl;
var lua_pushfstring: Tlua_pushfstring;
type Tlua_pushcclosure = procedure(L: Plua_State; Fn: lua_CFunction; N: Integer); cdecl;
var lua_pushcclosure: Tlua_pushcclosure;
type Tlua_pushboolean = procedure(L: Plua_State; B: Integer); cdecl;
var lua_pushboolean: Tlua_pushboolean;
type Tlua_pushlightuserdata = procedure(L: Plua_State; P: Pointer); cdecl;
var lua_pushlightuserdata: Tlua_pushlightuserdata;


(*
** get functions (Lua -> stack)
*)
type Tlua_gettable = procedure(L: Plua_State; Idx: Integer); cdecl;
var lua_gettable: Tlua_gettable;
type Tlua_rawget = procedure(L: Plua_State; Idx: Integer); cdecl;
var lua_rawget: Tlua_rawget;
type Tlua_rawgeti = procedure(L: Plua_State; Idx: Integer; N: Integer); cdecl;
var lua_rawgeti: Tlua_rawgeti;
type Tlua_newtable = procedure(L: Plua_State); cdecl;
var lua_newtable: Tlua_newtable;
type Tlua_newuserdata = function(L: Plua_State; SZ: size_t): Pointer; cdecl;
var lua_newuserdata: Tlua_newuserdata;
type Tlua_getmetatable = function(L: Plua_State; ObjIndex: Integer): Integer; cdecl;
var lua_getmetatable: Tlua_getmetatable;
type Tlua_getfenv = procedure(L: Plua_State; Idx: Integer); cdecl;
var lua_getfenv: Tlua_getfenv;


(*
** set functions (stack -> Lua)
*)
type Tlua_settable = procedure(L: Plua_State; Idx: Integer); cdecl;
var lua_settable: Tlua_settable;
type Tlua_rawset = procedure(L: Plua_State; Idx: Integer); cdecl;
var lua_rawset: Tlua_rawset;
type Tlua_rawseti = procedure(L: Plua_State; Idx: Integer; N: Integer); cdecl;
var lua_rawseti: Tlua_rawseti;
type Tlua_setmetatable = function(L: Plua_State; ObjIndex: Integer): Integer; cdecl;
var lua_setmetatable: Tlua_setmetatable;
type Tlua_setfenv = function(L: Plua_State; Idx: Integer): Integer; cdecl;
var lua_setfenv: Tlua_setfenv;


(*
** `load' and `call' functions (load and run Lua code)
*)
type Tlua_call = procedure(L: Plua_State; NArgs: Integer; NResults: Integer); cdecl;
var lua_call: Tlua_call;
type Tlua_pcall = function(L: Plua_State; NArgs: Integer; NResults: Integer; ErrFunc: Integer): Integer; cdecl;
var lua_pcall: Tlua_pcall;
type Tlua_cpcall = function(L: Plua_State; Func: lua_CFunction; UD: Pointer): Integer; cdecl;
var lua_cpcall: Tlua_cpcall;
type Tlua_load = function(L: Plua_State; Reader: lua_Chunkreader; DT: Pointer; const ChunkName: PChar): Integer; cdecl;
var lua_load: Tlua_load;

type Tlua_dump = function(L: Plua_State; Writer: lua_Chunkwriter; Data: Pointer): Integer; cdecl;
var lua_dump: Tlua_dump;


(*
** coroutine functions
*)
type Tlua_yield = function(L: Plua_State; NResults: Integer): Integer; cdecl;
var lua_yield: Tlua_yield;
type Tlua_resume = function(L: Plua_State; NArg: Integer): Integer; cdecl;
var lua_resume: Tlua_resume;

(*
** garbage-collection functions
*)
type Tlua_getgcthreshold = function(L: Plua_State): Integer; cdecl;
var lua_getgcthreshold: Tlua_getgcthreshold;
type Tlua_getgccount = function(L: Plua_State): Integer; cdecl;
var lua_getgccount: Tlua_getgccount;
type Tlua_setgcthreshold = procedure(L: Plua_State; NewThreshold: Integer); cdecl;
var lua_setgcthreshold: Tlua_setgcthreshold;

(*
** miscellaneous functions
*)

type Tlua_version = function: PChar; cdecl;
var lua_version: Tlua_version;

type Tlua_error = function(L: Plua_State): Integer; cdecl;
var lua_error: Tlua_error;

type Tlua_next = function(L: Plua_State; Idx: Integer): Integer; cdecl;
var lua_next: Tlua_next;

type Tlua_concat = procedure(L: Plua_State; N: Integer); cdecl;
var lua_concat: Tlua_concat;



(*
** ===============================================================
** some useful macros
** ===============================================================
*)

function lua_boxpointer(L: Plua_State; U: Pointer): Pointer;
function lua_unboxpointer(L: Plua_State; I: Integer): Pointer;
procedure lua_pop(L: Plua_State; N: Integer);
procedure lua_register(L: Plua_State; const N: PChar; F: lua_CFunction);
procedure lua_pushcfunction(L: Plua_State; F: lua_CFunction);
function lua_isfunction(L: Plua_State; N: Integer): Boolean;
function lua_istable(L: Plua_State; N: Integer): Boolean;
function lua_islightuserdata(L: Plua_State; N: Integer): Boolean;
function lua_isnil(L: Plua_State; N: Integer): Boolean;
function lua_isboolean(L: Plua_State; N: Integer): Boolean;
function lua_isnone(L: Plua_State; N: Integer): Boolean;
function lua_isnoneornil(L: Plua_State; N: Integer): Boolean;
procedure lua_pushliteral(L: Plua_State; const S: PChar);

(*
** compatibility macros and functions
*)

type Tlua_pushupvalues = function(L: Plua_State): Integer; cdecl;
var lua_pushupvalues: Tlua_pushupvalues;

procedure lua_getregistry(L: Plua_State);
procedure lua_setglobal(L: Plua_State; const S: PChar);
procedure lua_getglobal(L: Plua_State; const S: PChar);

(* compatibility with ref system *)

(* pre-defined references *)
const
  LUA_NOREF = (-2);
  LUA_REFNIL = (-1);

procedure lua_ref(L: Plua_State; Lock: Integer);
procedure lua_unref(L: Plua_State; Ref: Integer);
procedure lua_getref(L: Plua_State; Ref: Integer);

(*
** {======================================================================
** useful definitions for Lua kernel and libraries
** =======================================================================
*)

(* formats for Lua numbers *)
{$IFNDEF LUA_NUMBER_SCAN}
const
  LUA_NUMBER_SCAN = '%lf';
{$ENDIF}

{$IFNDEF LUA_NUMBER_FMT}
const
  LUA_NUMBER_FMT = '%.14g';
{$ENDIF}

(* }====================================================================== *)


(*
** {======================================================================
** Debug API
** =======================================================================
*)

const
(*
** Event codes
*)
  LUA_HOOKCALL = 0;
  LUA_HOOKRET = 1;
  LUA_HOOKLINE = 2;
  LUA_HOOKCOUNT = 3;
  LUA_HOOKTAILRET = 4;


(*
** Event masks
*)
  LUA_MASKCALL = (1 shl LUA_HOOKCALL);
  LUA_MASKRET = (1 shl LUA_HOOKRET);
  LUA_MASKLINE = (1 shl LUA_HOOKLINE);
  LUA_MASKCOUNT = (1 shl LUA_HOOKCOUNT);

const
  LUA_IDSIZE = 60;

type
  lua_Debug = record
    event: Integer;
    name: PChar; (* (n) *)
    namewhat: PChar; (* (n) `global', `local', `field', `method' *)
    what: PChar; (* (S) `Lua', `C', `main', `tail' *)
    source: PChar; (* (S) *)
    currentline: Integer;  (* (l) *)
    nups: Integer;   (* (u) number of upvalues *)
    linedefined: Integer;  (* (S) *)
    short_src: array [0..LUA_IDSIZE - 1] of Char; (* (S) *)
    (* private part *)
    i_ci: Integer;  (* active function *)
  end;
  Plua_Debug = ^lua_Debug;

  lua_Hook = procedure (L: Plua_State; AR: Plua_Debug); cdecl;


type Tlua_getstack = function(L: Plua_State; Level: Integer; AR: Plua_Debug): Integer; cdecl;
var lua_getstack: Tlua_getstack;
type Tlua_getinfo = function(L: Plua_State; const What: PChar; AR: Plua_Debug): Integer; cdecl;
var lua_getinfo: Tlua_getinfo;
type Tlua_getlocal = function(L: Plua_State; const AR: Plua_Debug; N: Integer): PChar; cdecl;
var lua_getlocal: Tlua_getlocal;
type Tlua_setlocal = function(L: Plua_State; const AR: Plua_Debug; N: Integer): PChar; cdecl;
var lua_setlocal: Tlua_setlocal;
type Tlua_getupvalue = function(L: Plua_State; FuncIndex: Integer; N: Integer): PChar; cdecl;
var lua_getupvalue: Tlua_getupvalue;
type Tlua_setupvalue = function(L: Plua_State; FuncIndex: Integer; N: Integer): PChar; cdecl;
var lua_setupvalue: Tlua_setupvalue;

type Tlua_sethook = function(L: Plua_State; Func: lua_Hook; Mask: Integer; Count: Integer): Integer; cdecl;
var lua_sethook: Tlua_sethook;
type Tlua_gethook = function(L: Plua_State): lua_Hook; cdecl;
var lua_gethook: Tlua_gethook;
type Tlua_gethookmask = function(L: Plua_State): Integer; cdecl;
var lua_gethookmask: Tlua_gethookmask;
type Tlua_gethookcount = function(L: Plua_State): Integer; cdecl;
var lua_gethookcount: Tlua_gethookcount;

(* }====================================================================== *)


(******************************************************************************
* Copyright (C) 1994-2003 Tecgraf, PUC-Rio.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************)


implementation

uses
  SysUtils, lauxlib, windows;

const
  MAX_SIZE = High(Integer);
  MAX_POINTER_ARRAY = MAX_SIZE div SizeOf(Pointer);

type
  TPointerArray = array [0..MAX_POINTER_ARRAY - 1] of Pointer;
  PPointerArray = ^TPointerArray;

function lua_upvalueindex(I: Integer): Integer;
begin
  Result := LUA_GLOBALSINDEX - I;
end;

function lua_boxpointer(L: Plua_State; U: Pointer): Pointer;
begin
  PPointerArray(lua_newuserdata(L, sizeof(U)))^[0] := U;
  Result := U;
end;

function lua_unboxpointer(L: Plua_State; I: Integer): Pointer;
begin
  Result := PPointerArray(lua_touserdata(L, I))^[0];
end;

procedure lua_pop(L: Plua_State; N: Integer);
begin
  lua_settop(L, -(N)-1);
end;

procedure lua_register(L: Plua_State; const N: PChar; F: lua_CFunction);
begin
  lua_pushstring(L, N);
  lua_pushcfunction(L, F);
  lua_settable(L, LUA_GLOBALSINDEX);
end;

procedure lua_pushcfunction(L: Plua_State; F: lua_CFunction);
begin
  lua_pushcclosure(L, F, 0);
end;

function lua_isfunction(L: Plua_State; N: Integer): Boolean;
begin
  Result := lua_type(L, n) = LUA_TFUNCTION;
end;

function lua_istable(L: Plua_State; N: Integer): Boolean;
begin
  Result := lua_type(L, n) = LUA_TTABLE;
end;

function lua_islightuserdata(L: Plua_State; N: Integer): Boolean;
begin
  Result := lua_type(L, n) = LUA_TLIGHTUSERDATA;
end;

function lua_isnil(L: Plua_State; N: Integer): Boolean;
begin
  Result := lua_type(L, n) = LUA_TNIL;
end;

function lua_isboolean(L: Plua_State; N: Integer): Boolean;
begin
  Result := lua_type(L, n) = LUA_TBOOLEAN;
end;

function lua_isnone(L: Plua_State; N: Integer): Boolean;
begin
  Result := lua_type(L, n) = LUA_TNONE;
end;

function lua_isnoneornil(L: Plua_State; N: Integer): Boolean;
begin
  Result := lua_type(L, n) <= 0;
end;

procedure lua_pushliteral(L: Plua_State; const S: PChar);
begin
  lua_pushlstring(L, S, StrLen(S));
end;

procedure lua_getregistry(L: Plua_State);
begin
  lua_pushvalue(L, LUA_REGISTRYINDEX);
end;

procedure lua_setglobal(L: Plua_State; const S: PChar);
begin
  lua_pushstring(L, S);
  lua_insert(L, -2);
  lua_settable(L, LUA_GLOBALSINDEX);
end;

procedure lua_getglobal(L: Plua_State; const S: PChar);
begin
  lua_pushstring(L, S);
  lua_gettable(L, LUA_GLOBALSINDEX);
end;

procedure lua_ref(L: Plua_State; Lock: Integer);
begin
  if (Lock <> 0) then
  begin
    luaL_ref(L, LUA_REGISTRYINDEX);
  end else
  begin
    lua_pushstring(L, 'unlocked references are obsolete');
    lua_error(L);
  end;
end;

procedure lua_unref(L: Plua_State; Ref: Integer);
begin
  luaL_unref(L, LUA_REGISTRYINDEX, Ref);
end;

procedure lua_getref(L: Plua_State; Ref: Integer);
begin
  lua_rawgeti(L, LUA_REGISTRYINDEX, Ref);
end;

var
	hlua: HMODULE;

function IsLoaded: boolean;
begin
	Result := hlua <> 0;
end;

procedure Init;
begin
	hlua := SafeLoadLibrary('lua.dll');
	if hlua > HINSTANCE_ERROR then
	begin
		@lua_open := GetProcAddress(hlua, 'lua_open');
		@lua_close := GetProcAddress(hlua, 'lua_close');
		@lua_newthread := GetProcAddress(hlua, 'lua_newthread');
		@lua_atpanic := GetProcAddress(hlua, 'lua_atpanic');
		@lua_gettop := GetProcAddress(hlua, 'lua_gettop');
		@lua_settop := GetProcAddress(hlua, 'lua_settop');
		@lua_pushvalue := GetProcAddress(hlua, 'lua_pushvalue');
		@lua_remove := GetProcAddress(hlua, 'lua_remove');
		@lua_insert := GetProcAddress(hlua, 'lua_insert');
		@lua_replace := GetProcAddress(hlua, 'lua_replace');
		@lua_checkstack := GetProcAddress(hlua, 'lua_checkstack');
		@lua_xmove := GetProcAddress(hlua, 'lua_xmove');
		@lua_isnumber := GetProcAddress(hlua, 'lua_isnumber');
		@lua_isstring := GetProcAddress(hlua, 'lua_isstring');
		@lua_iscfunction := GetProcAddress(hlua, 'lua_iscfunction');
		@lua_isuserdata := GetProcAddress(hlua, 'lua_isuserdata');
		@lua_type := GetProcAddress(hlua, 'lua_type');
		@lua_typename := GetProcAddress(hlua, 'lua_typename');
		@lua_equal := GetProcAddress(hlua, 'lua_equal');
		@lua_rawequal := GetProcAddress(hlua, 'lua_rawequal');
		@lua_lessthan := GetProcAddress(hlua, 'lua_lessthan');
		@lua_tonumber := GetProcAddress(hlua, 'lua_tonumber');
		@lua_toboolean := GetProcAddress(hlua, 'lua_toboolean');
		@lua_tostring := GetProcAddress(hlua, 'lua_tostring');
		@lua_strlen := GetProcAddress(hlua, 'lua_strlen');
		@lua_tocfunction := GetProcAddress(hlua, 'lua_tocfunction');
		@lua_touserdata := GetProcAddress(hlua, 'lua_touserdata');
		@lua_tothread := GetProcAddress(hlua, 'lua_tothread');
		@lua_topointer := GetProcAddress(hlua, 'lua_topointer');
		@lua_pushnil := GetProcAddress(hlua, 'lua_pushnil');
		@lua_pushnumber := GetProcAddress(hlua, 'lua_pushnumber');
		@lua_pushlstring := GetProcAddress(hlua, 'lua_pushlstring');
		@lua_pushstring := GetProcAddress(hlua, 'lua_pushstring');
		@lua_pushvfstring := GetProcAddress(hlua, 'lua_pushvfstring');
		@lua_pushfstring := GetProcAddress(hlua, 'lua_pushfstring');
		@lua_pushcclosure := GetProcAddress(hlua, 'lua_pushcclosure');
		@lua_pushboolean := GetProcAddress(hlua, 'lua_pushboolean');
		@lua_pushlightuserdata := GetProcAddress(hlua, 'lua_pushlightuserdata');
		@lua_gettable := GetProcAddress(hlua, 'lua_gettable');
		@lua_rawget := GetProcAddress(hlua, 'lua_rawget');
		@lua_rawgeti := GetProcAddress(hlua, 'lua_rawgeti');
		@lua_newtable := GetProcAddress(hlua, 'lua_newtable');
		@lua_newuserdata := GetProcAddress(hlua, 'lua_newuserdata');
		@lua_getmetatable := GetProcAddress(hlua, 'lua_getmetatable');
		@lua_getfenv := GetProcAddress(hlua, 'lua_getfenv');
		@lua_settable := GetProcAddress(hlua, 'lua_settable');
		@lua_rawset := GetProcAddress(hlua, 'lua_rawset');
		@lua_rawseti := GetProcAddress(hlua, 'lua_rawseti');
		@lua_setmetatable := GetProcAddress(hlua, 'lua_setmetatable');
		@lua_setfenv := GetProcAddress(hlua, 'lua_setfenv');
		@lua_call := GetProcAddress(hlua, 'lua_call');
		@lua_pcall := GetProcAddress(hlua, 'lua_pcall');
		@lua_cpcall := GetProcAddress(hlua, 'lua_cpcall');
		@lua_load := GetProcAddress(hlua, 'lua_load');
		@lua_dump := GetProcAddress(hlua, 'lua_dump');
		@lua_yield := GetProcAddress(hlua, 'lua_yield');
		@lua_resume := GetProcAddress(hlua, 'lua_resume');
		@lua_getgcthreshold := GetProcAddress(hlua, 'lua_getgcthreshold');
		@lua_getgccount := GetProcAddress(hlua, 'lua_getgccount');
		@lua_setgcthreshold := GetProcAddress(hlua, 'lua_setgcthreshold');
		@lua_version := GetProcAddress(hlua, 'lua_version');
		@lua_error := GetProcAddress(hlua, 'lua_error');
		@lua_next := GetProcAddress(hlua, 'lua_next');
		@lua_concat := GetProcAddress(hlua, 'lua_concat');
		@lua_pushupvalues := GetProcAddress(hlua, 'lua_pushupvalues');
		@lua_getstack := GetProcAddress(hlua, 'lua_getstack');
		@lua_getinfo := GetProcAddress(hlua, 'lua_getinfo');
		@lua_getlocal := GetProcAddress(hlua, 'lua_getlocal');
		@lua_setlocal := GetProcAddress(hlua, 'lua_setlocal');
		@lua_getupvalue := GetProcAddress(hlua, 'lua_getupvalue');
		@lua_setupvalue := GetProcAddress(hlua, 'lua_setupvalue');
		@lua_sethook := GetProcAddress(hlua, 'lua_sethook');
		@lua_gethook := GetProcAddress(hlua, 'lua_gethook');
		@lua_gethookmask := GetProcAddress(hlua, 'lua_gethookmask');
		@lua_gethookcount := GetProcAddress(hlua, 'lua_gethookcount');
	
	end	
end;

procedure Deinit;
begin
  if hlua > HINSTANCE_ERROR then
  begin
    FreeLibrary(hlua);
    hlua := 0;
  end;
end;

initialization
	Init;

finalization
  Deinit;

end.
