(*
** $Id: lualib.pas,v 1.1 2005/04/24 19:31:20 jfgoulet Exp $
** Lua standard libraries
** See Copyright Notice in lua.h
*)
unit lualib;

{$IFNDEF lualib_h}
{$DEFINE lualib_h}
{$ENDIF}

interface

uses
  lua;

function IsLoaded: boolean;
procedure Init;

const
  LUA_COLIBNAME = 'coroutine';
  LUA_TABLIBNAME = 'table';
  LUA_IOLIBNAME = 'io';
  LUA_OSLIBNAME = 'os';
  LUA_STRLIBNAME = 'string';
  LUA_MATHLIBNAME = 'math';
  LUA_DBLIBNAME = 'debug';

type Tluaopen_base = function(L: Plua_State): Integer; cdecl;
var luaopen_base: Tluaopen_base;

type Tluaopen_table = function(L: Plua_State): Integer; cdecl;
var luaopen_table: Tluaopen_table;

type Tluaopen_io = function(L: Plua_State): Integer; cdecl;
var luaopen_io: Tluaopen_io;

type Tluaopen_string = function(L: Plua_State): Integer; cdecl;
var luaopen_string: Tluaopen_string;

type Tluaopen_math = function(L: Plua_State): Integer; cdecl;
var luaopen_math: Tluaopen_math;

type Tluaopen_debug = function(L: Plua_State): Integer; cdecl;
var luaopen_debug: Tluaopen_debug;


type Tluaopen_loadlib = function(L: Plua_State): Integer; cdecl;
var luaopen_loadlib: Tluaopen_loadlib;


(* to help testing the libraries *)
{$IFNDEF lua_assert}
//#define lua_assert(c)   (* empty *)
{$ENDIF}


(* compatibility code *)
function lua_baselibopen(L: Plua_State): Integer;
function lua_tablibopen(L: Plua_State): Integer;
function lua_iolibopen(L: Plua_State): Integer;
function lua_strlibopen(L: Plua_State): Integer;
function lua_mathlibopen(L: Plua_State): Integer;
function lua_dblibopen(L: Plua_State): Integer;

implementation

uses
	sysutils, windows;

function lua_baselibopen(L: Plua_State): Integer;
begin
  Result := luaopen_base(L);
end;

function lua_tablibopen(L: Plua_State): Integer;
begin
  Result := luaopen_table(L);
end;

function lua_iolibopen(L: Plua_State): Integer;
begin
  Result := luaopen_io(L);
end;

function lua_strlibopen(L: Plua_State): Integer;
begin
  Result := luaopen_string(L);
end;

function lua_mathlibopen(L: Plua_State): Integer;
begin
  Result := luaopen_math(L);
end;

function lua_dblibopen(L: Plua_State): Integer;
begin
  Result := luaopen_debug(L);
end;

var
	hlua: HMODULE;

function IsLoaded: boolean;
begin
	Result := hlua <> 0;
end;

procedure Init;
begin
	hlua := SafeLoadLibrary('lua.dll');
	if hlua > HINSTANCE_ERROR then
	begin
		@luaopen_base := GetProcAddress(hlua, 'luaopen_base');
		@luaopen_table := GetProcAddress(hlua, 'luaopen_table');
		@luaopen_io := GetProcAddress(hlua, 'luaopen_io');
		@luaopen_string := GetProcAddress(hlua, 'luaopen_string');
		@luaopen_math := GetProcAddress(hlua, 'luaopen_math');
		@luaopen_debug := GetProcAddress(hlua, 'luaopen_debug');
		@luaopen_loadlib := GetProcAddress(hlua, 'luaopen_loadlib');
	
	end	
end;

procedure Deinit;
begin
  if hlua > HINSTANCE_ERROR then
  begin
    FreeLibrary(hlua);
    hlua := 0;
  end;
end;

initialization
	Init;

finalization
  Deinit;

end.
