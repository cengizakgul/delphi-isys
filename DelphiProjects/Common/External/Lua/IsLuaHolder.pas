unit isluaholder;

interface

uses
  lua, lualib, lauxlib, luautils, sysutils;

type
	TLuaException = class(Exception);
	TLuaDllNotFound = class(Exception);

	TLua = class	
	protected
		L: Plua_State;
	protected
		procedure CheckLuaLoaded;
	public
		procedure print( s: string ); virtual; abstract;
    function ExceptionToLuaRet: integer;

		class function GetObj(L: Plua_State): TLua;
		constructor Create;
		destructor Destroy; override;
		procedure run(fname: string);
	end;


implementation

procedure TLua.CheckLuaLoaded;
begin
	if not lua.IsLoaded or not lauxlib.IsLoaded or not lualib.IsLoaded then
		raise TLuaDllNotFound.Create('Cannot find lua.dll.');
end;

constructor TLua.Create;
begin
	CheckLuaLoaded;
	l := lua_open();

	luaopen_base(L);             
	luaopen_table(L);            
	luaopen_io(L);               
	luaopen_string(L);           
	luaopen_math(L);             


	lua_pushlightuserdata(L, Self );
	lua_setglobal(L, 'cookie');

	LuaRegister(L, 'print', lua_print);
	LuaRegister(L, 'io.write', lua_io_write);  
end;

destructor TLua.Destroy;
begin
	if lua.IsLoaded then
  	lua_close(L);
end;

procedure TLua.run(fname: string);
begin
  LuaLoadBufferFromFile(L, fname, ExtractFileName(fname) );
  LuaPCall( L, 0, 0, 0);
end;

class function TLua.GetObj(L: Plua_State): TLua;
begin
	lua_getglobal(L, 'cookie');
	Result := TLua(lua_topointer(L, -1));
	Assert(Result <> nil);
	lua_pop(L, 1);
end;

procedure HandleLuaStdOut(L: Plua_State; S: PChar; N: Integer);
var
	ss : string;
begin
	SetString( ss, s, N );		
	TLua.GetObj(L).print(ss);
end;

function TLua.ExceptionToLuaRet: integer;
begin
  Assert(ExceptObject <> nil);
  Assert(ExceptObject is Exception);

  lua_pushnil(L);
  lua_pushstring( L, PChar((ExceptObject as Exception).Message) );
  Result := 2;
end;

initialization
	luautils.OnLuaStdOut := HandleLuaStdOut;

finalization
	luautils.OnLuaStdOut := nil;

end.