{##############################################################################}
{# NexusDB: nxReplacementMemoryManager.pas 2.00                               #}
{# NexusDB Memory Manager: nxReplacementMemoryManager.pas 3.04                #}
{# Copyright (c) Nexus Database Systems Pty. Ltd. 2003                        #}
{# All rights reserved.                                                       #}
{##############################################################################}
{# NexusDB: Replacement for Borland Memory Management                         #}
{##############################################################################}

{$I nxDefine.inc}

unit nxReplacementMemoryManager;

interface

uses
  nxllMemoryManager;

var
  OrgMemoryManager : TMemoryManager;

{$IFDEF BCB}//BCB support
//make public
procedure nxMMInitialization;
procedure nxMMFinalization;
function nxMMInitialized:boolean;
{$ENDIF}//$IFDEF BCB

implementation
{$IFDEF BCB}//BCB support
uses
  nxllFastCpuDetect,
  nxllFastMove,
  nxllFastFillChar,
  nxllMemoryManagerImpl;
{$ENDIF}

//econs4 BCB support - 15-Feb-2005
var
  AlreadyReplace : boolean = false;

procedure ReplaceMM;
begin
  if not AlreadyReplace then begin
    AlreadyReplace := true;
    if GetHeapStatus.TotalAllocated <> 0 then
      {$IFNDEF DCC6OrLater}
      RunError(2 {reInvalidPtr});
      {$ELSE}
      Error(reInvalidPtr);
      {$ENDIF}

    GetMemoryManager(OrgMemoryManager);
    SetMemoryManager(_nxMemoryManager);
  end;
end;

{$IFDEF BCB}//BCB support
function nxMMInitialized:boolean;
begin
  Result := nxllMemoryManagerImpl.nxMMInitialized;
end;

procedure nxMMFinalization;
begin
  // right order of Unit finalization part
  nxllMemoryManager.Done;
  nxllMemoryManagerImpl.Done;
end;

procedure nxMMInitialization;
begin
  // right order of Unit initialization part
  nxllFastCpuDetect.DoInitialization;  // detect cpu type
  nxllFastMove.FindMove;               // detect move proc
  nxllFastFillChar.FindFillChar;       // detect FillChar proc
  nxllMemoryManagerImpl.Init;
  nxllMemoryManager.Init;              // init memory manager
  ReplaceMM;
end;
{$ENDIF}//$IFNDEF BCB

initialization
  ReplaceMM; //econs4 BCB support - 15-Feb-2005
finalization
end.
