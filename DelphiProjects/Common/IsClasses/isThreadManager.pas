{$D-}
{$L-}
unit isThreadManager;

interface

uses Classes, SyncObjs, SysUtils, Windows, Messages, Forms, isBaseClasses, Math,
     isTypes, isStatistics, isVCLBugFix;


type
  TTaskParamList = TisDynVariantArray;

type

  PTaskProc = procedure (const Params: TTaskParamList);
  PTaskProcObj = procedure (const Params: TTaskParamList) of object;

  TTaskID = THandle;

  TThreadManagerStatus = record
    TaskDescriptions: String;
  end;

  
  IThreadManager = interface
  ['{995E7838-BDC8-4B99-A342-6B3318B267F2}']
    function  RunTask(const Task: PTaskProc; const Params: TTaskParamList; const ATaskDescription: String = ''; const APriority: TThreadPriority = tpNormal): TTaskID; overload;
    function  RunTask(const Task: PTaskProcObj; const AObject: TObject; const Params: TTaskParamList; const ATaskDescription: String = ''; const APriority: TThreadPriority = tpNormal): TTaskID; overload;
    procedure RunInMainThread(const Task: PTaskProc; const Params: TTaskParamList; const ABlockingMode: Boolean = True); overload;
    procedure RunInMainThread(const AObject: TObject; const Task: PTaskProcObj; const Params: TTaskParamList; const ABlockingMode: Boolean = True); overload;
    procedure WaitForTaskEnd(const ATaskID: TTaskID = 0); overload;
    function  WaitForTaskEnd(const ATaskID: TTaskID; const ATimeOut: Integer): Boolean; overload;
    procedure SetCapacity(const AValue: Integer);
    function  GetCapacity: Integer;
    function  GetStatus: TThreadManagerStatus;
    procedure TerminateTask(const ATaskID: TTaskID = 0);
    function  TaskIsTerminated(ATaskID: TTaskID = 0): Boolean;
    function  GetThreadIDByTaskID(const ATaskID: TTaskID): THandle;
    function  GetIdleThreadDeleteTimeout: Cardinal;
    procedure SetIdleThreadDeleteTimeout(const AValue: Cardinal);
    function  AllThreadsAreBusy: Boolean;
    function  AllThreadsAreReady: Boolean;
    procedure CleanupThreadPool;

    property  Capacity: Integer read GetCapacity write SetCapacity;
    property  IdleThreadDeleteTimeout: Cardinal read GetIdleThreadDeleteTimeout write SetIdleThreadDeleteTimeout;
  end;


  TTaskCallType = (ctProcedure, ctMethod);

  TTaskRec = packed record
    case CallType: TTaskCallType of
      ctProcedure: (TaskProc: PTaskProc);
      ctMethod:    (Method: TMethod);
  end;


  TThreadManager = class;

  TTaskThread = class(TThread)
  private
    FOwner: TThreadManager;
    FCS: TCriticalSection;
    FTaskRec: TTaskRec;
    FParams: TTaskParamList;
    FProcOwner: IInterface;
    FDescription: String;
    FWakeUpEvent: THandle;
    FTerminateTaskEvent: THandle;
    FEndTaskEvent: THandle;    
    FTaskID: TTaskID;
    FStatEvt: IisSimpleStatEvent;
    procedure SetNewTaskID;
    function  GetTaskTerminated: Boolean;
  protected
    procedure Execute; override;
  public
    function  Run(const Task: PTaskProc; const Params: TTaskParamList; const ATaskDescription: String; const APriority: TThreadPriority): TTaskID; overload;
    function  Run(const AObject: TObject; const Task: PTaskProcObj; const Params: TTaskParamList; const ATaskDescription: String; const APriority: TThreadPriority): TTaskID; overload;
    function  Busy: Boolean;
    function  CurrentTaskID: TTaskID;
    procedure TerminateTask;
    procedure WaitForTaskEnd; overload;
    function  WaitForTaskEnd(const ATimeOut: Integer): Boolean; overload;
    procedure ReleaseThread;
    property  TaskTerminated: Boolean read GetTaskTerminated;

    constructor  Create(const AOwner: TThreadManager); reintroduce;
    destructor   Destroy; override;
  end;


  TThreadManager = class(TInterfacedObject, IThreadManager)
  private
    FName: String;
    FList: TThreadList;
    FCapacity: Integer;
    FThreadPoolStatusChangeEvent: THandle;
    FIdleThreadDeleteTimeout: Cardinal;
    procedure RegisterThread(const AThread: TTaskThread);
    procedure UnRegisterThread(const AThread: TTaskThread);
    procedure TerminateAllTasks;
    function  GetThreadFromPool: TTaskThread;
    function  TimeToExitForAllLoops: Boolean;
  protected
    function  RunTask(const Task: PTaskProc; const Params: TTaskParamList; const ATaskDescription: String = ''; const APriority: TThreadPriority = tpNormal): TTaskID; overload;
    function  RunTask(const Task: PTaskProcObj; const AObject: TObject; const Params: TTaskParamList; const ATaskDescription: String = ''; const APriority: TThreadPriority = tpNormal): TTaskID; overload;
    procedure RunInMainThread(const Task: PTaskProc; const Params: TTaskParamList; const ABlockingMode: Boolean = True); overload;
    procedure RunInMainThread(const AObject: TObject; const Task: PTaskProcObj; const Params: TTaskParamList; const ABlockingMode: Boolean = True); overload;
    procedure WaitForTaskEnd(const ATaskID: TTaskID = 0); overload;
    function  WaitForTaskEnd(const ATaskID: TTaskID; const ATimeOut: Integer): Boolean; overload;
    procedure SetCapacity(const AValue: Integer);
    function  GetCapacity: Integer;
    function  GetStatus: TThreadManagerStatus;
    procedure TerminateTask(const ATaskID: TTaskID = 0);
    function  TaskIsTerminated(ATaskID: TTaskID = 0): Boolean;
    function  GetThreadIDByTaskID(const ATaskID: TTaskID): THandle;
    function  GetIdleThreadDeleteTimeout: Cardinal;
    procedure SetIdleThreadDeleteTimeout(const AValue: Cardinal);
    function  AllThreadsAreBusy: Boolean;
    function  AllThreadsAreReady: Boolean;
    procedure CleanupThreadPool;
  public
    constructor  Create(const AName: String);
    destructor   Destroy; override;
  end;


  // Sleeping until "Wakeup" has called, then run "DoWork" then going to sleep again.
  TReactiveThread = class
  private
    FAlarmEvent: THandle;
    FTaskID: TTaskID;
    FWaitTime: Cardinal;
    procedure Loop(const Params: TTaskParamList);
    procedure SetFWaitTime(const AValue: Cardinal);
  protected
    procedure DoOnStart; virtual;
    procedure DoWork; virtual; abstract;
    procedure DoOnStop; virtual;
  public
    procedure   Wakeup;
    constructor Create(const AThreadName: String; const APriority: TThreadPriority = tpNormal);
    destructor  Destroy; override;
    function    IsActive: Boolean;
    property    WaitTime: Cardinal read FWaitTime write SetFWaitTime;
  end;


  function  GlobalThreadManager: IThreadManager;
  function  GlobalThreadManagerIsActive: Boolean;
  procedure DeactivateGlobalThreadManager;
  function  MakeTaskParams(const Params: array of const): TTaskParamList;
  function  EmptyParams: TTaskParamList;
  function  CurrentThreadIsMain: Boolean;
  function  CurrentThreadTaskTerminated: Boolean;
  function  CurrentTaskDuration: Cardinal;
  function  CurrentTaskStartTime: TDateTime;
  function  GetCurrentThreadTaskID: TTaskID;
  procedure AbortIfCurrentThreadTaskTerminated;
  function  HowManyProcessors: Integer;
  procedure SetCurrentThreadName(const AName: String);
  function  Snooze(const AMilliseconds: Cardinal; const AWakeEvent: THandle = 0): Boolean;
  procedure RegisterTMCallbacks(const OnCreateProc: TProcedure; const OnDestroyProc: TProcedure);
  procedure UnregisterTMCallbacks(const OnCreateProc: TProcedure; const OnDestroyProc: TProcedure);
  procedure SetAppTerminatingFlag;

implementation

uses isErrorUtils, isLogFile, isBasicUtils;

type
  TCallbackList = class(TThreadList)
  public
    procedure   ProcessCallbacks;
    constructor Create;
  end;

  TSynchronizer = class
  private
    FHandle: HWND;
    procedure WndProc(var Message: TMessage);
  public
    procedure   Run(const ATaskEntryPoint: TTaskRec; const AParams: TTaskParamList; const ABlockingMode: Boolean);
    constructor Create;
    destructor  Destroy; override;
  end;

  TSyncInfo = class
  private
    FTaskEntryPoint: TTaskRec;
    FParams: TTaskParamList;
    FEvent: THandle;
    procedure MarkAsFinished;
    procedure WaitUntilFinished;
  public
    function    BlockingMode: Boolean;  
    procedure   Execute;
    constructor Create(const ATaskEntryPoint: TTaskRec; const AParams: TTaskParamList; const ABlockingMode: Boolean);
    destructor  Destroy; override;
  end;

var FThreadManager: IThreadManager;
var TMwasActive: Boolean;
var UnitFinalization: Boolean;
var TaskIDCount: TTaskID;
var OnThreadCreateCallbacks: TCallbackList;
var OnThreadDestroyCallbacks: TCallbackList;
var AppIsTerminating: Boolean;
var Synchronizer: TSynchronizer;

threadvar ThreadObject: TTaskThread;

const CM_SYNCHRONIZE = WM_USER + 1;

{
// The alternative way to set thread name
// please see MSDN at http://msdn2.microsoft.com/en-us/library/xcb2z8hs(VS.71).aspx
type
  tagTHREADNAME_INFO = record
	  dwType:     DWORD;  // must be 0x1000
	  szName:     LPCSTR; // pointer to name (in same addr space)
   	dwThreadID: DWORD;  // thread ID (-1 caller thread)
	  dwFlags:    DWORD;  // reserved for future use, most be zero
  end;

procedure SetCurrentThreadName(const AName: String);
const
  MS_VC_EXCEPTION = $406D1388;
var
  info: tagTHREADNAME_INFO;
begin
	info.dwType := $1000;
	info.szName := PAnsiChar(AName);
	info.dwThreadID := GetCurrentThreadId;
	info.dwFlags := 0;
	try
    RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) div sizeof(DWORD), PDWORD(@info));
  except
  end;
end;
}

procedure SetCurrentThreadName(const AName: String);
begin
  // using MadExcept feature
  SetThreadName(GetCurrentThreadId, AName);
end;


procedure SetAppTerminatingFlag;
begin
  AppIsTerminating := True;
  if not IsConsole and (Application <> nil) and not Application.Terminated then
    PostMessage(Application.Handle, WM_QUIT, 0, 0);
end;


procedure RegisterTMCallbacks(const OnCreateProc: TProcedure; const OnDestroyProc: TProcedure);
begin
  if Assigned(OnCreateProc) then
    OnThreadCreateCallbacks.Add(@OnCreateProc);

  if Assigned(OnDestroyProc) then
    OnThreadDestroyCallbacks.Add(@OnDestroyProc);
end;

procedure UnregisterTMCallbacks(const OnCreateProc: TProcedure; const OnDestroyProc: TProcedure);
begin
  OnThreadCreateCallbacks.Remove(@OnCreateProc);
  OnThreadDestroyCallbacks.Remove(@OnDestroyProc);
end;

function Snooze(const AMilliseconds: Cardinal; const AWakeEvent: THandle = 0): Boolean;
var
  TotalTime, SleepTime: Cardinal;
begin
  Result := True;
  TotalTime := 0;
  SleepTime := Min(AMilliseconds, 100);

  while TotalTime < AMilliseconds do
  begin
    AbortIfCurrentThreadTaskTerminated;
    if AWakeEvent <> 0 then
    begin
      if WaitForSingleObject(AWakeEvent, SleepTime) <> WAIT_TIMEOUT then
      begin
        Result := False;
        break;
      end;
    end
    else
      Sleep(SleepTime);

    Inc(TotalTime, SleepTime);
    if SleepTime > AMilliseconds - TotalTime then
      SleepTime := AMilliseconds - TotalTime;
  end;
end;


{ TThreadManager }

function GlobalThreadManager: IThreadManager;
begin
  if not GlobalThreadManagerIsActive then
  begin
    Assert(not TMwasActive, 'Global Thread Manager is destroyed');
    FThreadManager := TThreadManager.Create('Global TM');
    TMwasActive := True;
    FThreadManager.Capacity := 1000; // virtually limitless quantity of threads
  end;

  Result := FThreadManager;
end;

procedure DeactivateGlobalThreadManager;
begin
  if GlobalThreadManagerIsActive then
  begin
    FThreadManager.TerminateTask;
    FThreadManager.WaitForTaskEnd;
    FThreadManager.CleanupThreadPool;
    FThreadManager := nil;
  end;
end;

function GlobalThreadManagerIsActive: Boolean;
begin
  Result := Assigned(FThreadManager);
end;

function HowManyProcessors: Integer;
var
   SysInf: SYSTEM_INFO;
begin
  GetSystemInfo(SysInf);
  Result := SysInf.dwNumberOfProcessors;
end;

function MakeTaskParams(const Params: array of const): TTaskParamList;
begin
  Result := ConstsToDynVarArray(Params);
end;

function EmptyParams: TTaskParamList;
begin
end;

function  CurrentThreadIsMain: Boolean;
begin
  Result := GetCurrentThreadId = MainThreadId;
end;

function CurrentThreadTaskTerminated: Boolean;
begin
  Result := Assigned(ThreadObject) and ThreadObject.TaskTerminated or
            AppIsTerminating or
            not IsConsole and Application.Terminated;
end;

function  GetCurrentThreadTaskID: TTaskID;
begin
  if Assigned(ThreadObject) then
    Result := ThreadObject.FTaskID
  else
    Result := 0;
end;

function CurrentTaskDuration: Cardinal;
begin
  if Assigned(ThreadObject) then
    Result := ThreadObject.FStatEvt.Duration
  else
    Result := 0;
end;

function CurrentTaskStartTime: TDateTime;
begin
  if Assigned(ThreadObject) then
    Result := ThreadObject.FStatEvt.BeginTime
  else
    Result := 0;
end;

procedure AbortIfCurrentThreadTaskTerminated;
begin
  if CurrentThreadTaskTerminated then
    AbortEx;
end;

function TThreadManager.AllThreadsAreBusy: Boolean;
var
  L: TList;
  i: Integer;
begin
  L := FList.LockList;
  try
    if L.Count < FCapacity then
      Result := False

    else
    begin
      Result := True;
      for i := 0 to L.Count - 1 do
        if not TTaskThread(L[i]).Busy then
        begin
          Result := False;
          break;
        end;
      end;
  finally
    FList.UnlockList;
  end;
end;

procedure TThreadManager.CleanupThreadPool;
var
  L: TList;
  AllDone: Boolean;
begin
  if UnitFinalization then  // Delphi simply kills all threads before getting into finalization, so TM is not avare of that
    Exit;

  SetCapacity(0);

  repeat
    L := FList.LockList;
    try
      AllDone := L.Count = 0;
    finally
      FList.UnlockList;
    end;

    if not AllDone then
      WaitForSingleObject(FThreadPoolStatusChangeEvent, INFINITE);

  until AllDone;

  Sleep(50);
end;

constructor TThreadManager.Create(const AName: String);
begin
  FName := AName;
  FList := TThreadList.Create;
  FCapacity := HowManyProcessors * 4; // it's very subjective and depends on nature of running tasks
  FIdleThreadDeleteTimeout := 15 * 60000;  // default: 15 mins
  FThreadPoolStatusChangeEvent := CreateEvent(nil, False, False, nil);
end;

destructor TThreadManager.Destroy;
begin
  TerminateAllTasks;
  WaitForTaskEnd;
  CleanupThreadPool;

  inherited;
  FreeAndNil(FList);
  CloseHandle(FThreadPoolStatusChangeEvent);
end;

function TThreadManager.GetCapacity: Integer;
begin
  Result := FCapacity;
end;

function TThreadManager.GetIdleThreadDeleteTimeout: Cardinal;
begin
  Result := FIdleThreadDeleteTimeout;
end;

function TThreadManager.GetStatus: TThreadManagerStatus;
var
  L: TList;
  i: Integer;
begin
  Result.TaskDescriptions := '';
  L := FList.LockList;
  try
    for i := 0 to L.Count - 1 do
    begin
      if i > 0 then
        Result.TaskDescriptions := Result.TaskDescriptions + '  ';
      Result.TaskDescriptions := Result.TaskDescriptions + 'Proc ' + IntToStr(i + 1) + ': ' + TTaskThread(L[i]).FDescription;
    end;
  finally
    FList.UnlockList;
  end;
end;

function TThreadManager.GetThreadFromPool: TTaskThread;
var
  L: TList;
  i: Integer;
begin
  Result := nil;

  while not Assigned(Result) and not TimeToExitForAllLoops do
  begin
    L := FList.LockList;
    try
      for i := 0 to L.Count - 1 do
        if not TTaskThread(L[i]).Busy and not TTaskThread(L[i]).Terminated  then
        begin
          Result := TTaskThread(L[i]);
          break;
        end;

      if not Assigned(Result) and (L.Count < FCapacity) then
        Result := TTaskThread.Create(Self);

    finally
      FList.UnlockList;
    end;

    if not Assigned(Result) then
      WaitForSingleObject(FThreadPoolStatusChangeEvent, INFINITE);
  end;
end;

function TThreadManager.GetThreadIDByTaskID(const ATaskID: TTaskID): THandle;
var
  L: TList;
  i: Integer;
begin
  Result := 0;

  if ATaskID = 0 then
    Exit;

  L := FList.LockList;
  try
    for i := 0 to L.Count - 1 do
      if TTaskThread(L[i]).CurrentTaskID = ATaskID then
      begin
        Result := TTaskThread(L[i]).ThreadID;
        break;
      end;
  finally
    FList.UnlockList;
  end;
end;

procedure TThreadManager.RegisterThread(const AThread: TTaskThread);
var
  L: TList;
begin
  L := FList.LockList;
  try
    L.Add(AThread);
  finally
    FList.UnlockList;
  end;

  SetEvent(FThreadPoolStatusChangeEvent);
end;

function TThreadManager.RunTask(const Task: PTaskProc; const Params: TTaskParamList; const ATaskDescription: String = ''; const APriority: TThreadPriority = tpNormal): TTaskID;
var
  T: TTaskThread;
begin
  Result := 0;

  while Result = 0 do
  begin
    T := GetThreadFromPool;

    if not Assigned(T) then  // manager termination happened
      Exit;

    Result := T.Run(Task, Params, ATaskDescription, APriority);
  end;
end;

procedure TThreadManager.RunInMainThread(const Task: PTaskProc; const Params: TTaskParamList;
  const ABlockingMode: Boolean = True);
var
  TaskEntryPoint: TTaskRec;
begin
  TaskEntryPoint.CallType := ctProcedure;
  TaskEntryPoint.TaskProc := Task;
  Synchronizer.Run(TaskEntryPoint, Params, ABlockingMode);
end;

procedure TThreadManager.RunInMainThread(const AObject: TObject; const Task: PTaskProcObj;
  const Params: TTaskParamList; const ABlockingMode: Boolean = True);
var
  TaskEntryPoint: TTaskRec;
begin
  TaskEntryPoint.CallType := ctMethod;
  TaskEntryPoint.Method.Code := @Task;
  TaskEntryPoint.Method.Data := AObject;

  Synchronizer.Run(TaskEntryPoint, Params, ABlockingMode);
end;

function TThreadManager.RunTask(const Task: PTaskProcObj; const AObject: TObject; const Params: TTaskParamList; const ATaskDescription: String; const APriority: TThreadPriority): TTaskID;
var
  T: TTaskThread;
begin
  Result := 0;

  while Result = 0 do
  begin
    T := GetThreadFromPool;

    if not Assigned(T) then  // manager termination happened
      Exit;

    Result := T.Run(AObject, Task, Params, ATaskDescription, APriority);
  end;
end;


procedure TThreadManager.SetCapacity(const AValue: Integer);
var
  L: TList;
  i: Integer;
begin
  if FCapacity > AValue then
  begin
    L := FList.LockList;
    try
      for i :=  L.Count - 1 downto AValue do
        TTaskThread(L[i]).ReleaseThread;
    finally
      FList.UnlockList;
    end;
  end;

  FCapacity := AValue;
  SetEvent(FThreadPoolStatusChangeEvent);  
end;

procedure TThreadManager.SetIdleThreadDeleteTimeout(const AValue: Cardinal);
begin
  FIdleThreadDeleteTimeout := AValue;
end;

function TThreadManager.TaskIsTerminated(ATaskID: TTaskID = 0): Boolean;
var
  L: TList;
  i: Integer;
begin
  Result := True;

  if ATaskID = 0 then
    if Assigned(ThreadObject) then
      ATaskID := ThreadObject.CurrentTaskID
    else
      Exit;

  L := FList.LockList;
  try
    for i := 0 to L.Count - 1 do
      if TTaskThread(L[i]).CurrentTaskID = ATaskID then
      begin
        Result := not TTaskThread(L[i]).Busy or TTaskThread(L[i]).Terminated;
        break;
      end;
  finally
    FList.UnlockList;
  end;
end;

procedure TThreadManager.TerminateAllTasks;
var
  L: TList;
  i: Integer;
begin
  L := FList.LockList;
  try
    for i := 0 to L.Count - 1 do
      TTaskThread(L[i]).TerminateTask;
  finally
    FList.UnlockList;
  end;
end;


procedure TThreadManager.TerminateTask(const ATaskID: TTaskID);
var
  L: TList;
  i: Integer;
begin
  if ATaskID = 0 then
    TerminateAllTasks

  else
  begin
    L := FList.LockList;
    try
      for i := 0 to L.Count - 1 do
        if TTaskThread(L[i]).CurrentTaskID = ATaskID then
        begin
          TTaskThread(L[i]).TerminateTask;
          break;
        end;
    finally
      FList.UnlockList;
    end;
  end;
end;


function TThreadManager.TimeToExitForAllLoops: Boolean;
begin
  Result := FCapacity = 0;
end;

procedure TThreadManager.UnRegisterThread(const AThread: TTaskThread);
var
  L: TList;
  i: Integer;
begin
  L := FList.LockList;
  try
    i := L.IndexOf(AThread);
    Assert(i >= 0);
    L.Delete(i);
  finally
    FList.UnlockList;
  end;

  SetEvent(FThreadPoolStatusChangeEvent);
end;


procedure TThreadManager.WaitForTaskEnd(const ATaskID: TTaskID);
var
  TaskIsDone: Boolean;
  L: TList;
  i: Integer;
  T: TTaskThread;
begin
  TaskIsDone := False;

  while not TaskIsDone and not TimeToExitForAllLoops do
  begin
    L := FList.LockList;
    try
      TaskIsDone := True;
      T := nil;
      for i := 0 to L.Count - 1 do
      begin
        T := TTaskThread(L[i]);
        if (ATaskID <> 0) and (T.CurrentTaskID = ATaskID) or (ATaskID = 0) and T.Busy then
        begin
          TaskIsDone := False;
          break;
        end;
      end;
    finally
      FList.UnlockList;
    end;

    if not TaskIsDone then
    begin
      T.WaitForTaskEnd;
      if ATaskID <> 0 then
        break;
    end;
  end;
end;


function TThreadManager.AllThreadsAreReady: Boolean;
var
  L: TList;
  i: Integer;
begin
  L := FList.LockList;
  try
    Result := True;
    for i := 0 to L.Count - 1 do
      if TTaskThread(L[i]).Busy then
      begin
        Result := False;
        break;
      end;
  finally
    FList.UnlockList;
  end;
end;

function TThreadManager.WaitForTaskEnd(const ATaskID: TTaskID; const ATimeOut: Integer): Boolean;
var
  L: TList;
  i: Integer;
  T: TTaskThread;
begin
  T := nil;

  L := FList.LockList;
  try
    for i := 0 to L.Count - 1 do
      if TTaskThread(L[i]).CurrentTaskID = ATaskID then
      begin
        T := TTaskThread(L[i]);
        break;
      end;
  finally
    FList.UnlockList;
  end;

  if Assigned(T) then
    Result := T.WaitForTaskEnd(ATimeOut)
  else
    Result := True;
end;


{ TTaskThread }

function TTaskThread.Busy: Boolean;
begin
  Result := (CurrentTaskID <> 0) or (WaitForSingleObject(FEndTaskEvent, 0) <> WAIT_OBJECT_0);
end;

constructor TTaskThread.Create(const AOwner: TThreadManager);
begin
  FCS := TCriticalSection.Create;
  FStatEvt := CreateSimpleStatEvent;

  FTerminateTaskEvent := CreateEvent(nil, True, False, nil);
  FEndTaskEvent := CreateEvent(nil, True, True, nil);
  FWakeUpEvent := CreateEvent(nil, False, False, nil);

  FOwner := AOwner;
  FreeOnTerminate := True;
  FOwner.RegisterThread(Self);

  inherited Create(False);
end;

destructor TTaskThread.Destroy;
begin
  FOwner.UnregisterThread(Self);
  FreeAndNil(FCS);

  CloseHandle(FTerminateTaskEvent);
  CloseHandle(FEndTaskEvent);
  CloseHandle(FWakeUpEvent);

  inherited;
end;

procedure TTaskThread.Execute;
var
  LastThreadName: String;
  WaitResult: Integer;
begin
  ThreadObject := Self;

  OnThreadCreateCallbacks.ProcessCallbacks;
  try
    while not Terminated do
    begin
      try
        // sleep and wait for new RunTask
        WaitResult := WaitForSingleObjectEx(FWakeUpEvent, FOwner.FIdleThreadDeleteTimeout, True);

        if WaitResult = WAIT_TIMEOUT then               // Thread was ideling for too long time. Let's terminate it
        begin
          FCS.Enter;
          try
            // in case if a new task was "lucky" enough to setup for running in this thread during timeout
            if not Busy then
              Terminate;
          finally
            FCS.Leave;
          end;
        end
        else if WaitResult = WAIT_IO_COMPLETION then     // Overlapped I/O operation completed. It is not time yet to run!
          Continue;

        if Terminated then
          break
        else if not Busy then
          continue;

        try
          SetCurrentThreadName(FOwner.FName + ': ' + FDescription + '   [' + FormatDateTime('mm/dd/yy hh:nn:ss zzz', Now) + ']');

          FStatEvt.StartWatch;

          if FTaskRec.CallType = ctProcedure then
            FTaskRec.TaskProc(FParams)
          else if FTaskRec.CallType = ctMethod then
            PTaskProcObj(FTaskRec.Method)(FParams)
          else
            Assert(False);

          FStatEvt.StopWatch;
         finally
           Priority := tpNormal; // restore priority back to normal
           LastThreadName := FDescription;

           try
             try
               FParams := nil;
             finally
               FProcOwner := nil;
             end;
           finally
             FCS.Enter;
             try
               FTaskRec.TaskProc := nil;
               FTaskRec.Method.Code := nil;
               FTaskRec.Method.Data := nil;
               FDescription := '';
               FTaskID := 0;
               SetCurrentThreadName(FOwner.FName + ': Free Thread');
             finally
               SetEvent(FEndTaskEvent);
               FCS.Leave;
               SetEvent(FOwner.FThreadPoolStatusChangeEvent);
             end;
           end;
         end;

      except
        on E: Exception do
          if (GlobalLogFile <> nil) and not (E is EAbort) then
          begin
            if LastThreadName = '' then
              LastThreadName := '#' + IntToStr(ThreadID);
            GlobalLogFile.AddEvent(etFatalError, 'Internal', 'Unexpected error in thread "' + LastThreadName + '"', E.Message + #13#13 + GetStack(E, ExceptAddr));
            LastThreadName := '';
          end;
      end;
    end;

  finally
    if Assigned(OnThreadDestroyCallbacks) then
      OnThreadDestroyCallbacks.ProcessCallbacks;
  end;

  ThreadObject := nil;
end;


function TTaskThread.Run(const Task: PTaskProc; const Params: TTaskParamList; const ATaskDescription: String; const APriority: TThreadPriority): TTaskID;
begin
  Result := 0;

  FCS.Enter;
  try
    if Busy or Terminated then
      Exit;
    SetNewTaskID;
  finally
    FCS.Leave;
  end;

  FTaskRec.TaskProc := Task;
  FParams := Copy(Params);
  FTaskRec.CallType := ctProcedure;
  FDescription := ATaskDescription;
  Priority := APriority;

  Result := CurrentTaskID;

  ResetEvent(FEndTaskEvent);
  ResetEvent(FTerminateTaskEvent);
  SetEvent(FWakeUpEvent);
end;

function TTaskThread.GetTaskTerminated: Boolean;
begin
  if FTerminateTaskEvent <> 0 then
    Result := (WaitForSingleObject(FTerminateTaskEvent, 0) = WAIT_OBJECT_0) or not Busy
  else
    Result := True;
end;

function TTaskThread.Run(const AObject: TObject; const Task: PTaskProcObj; const Params: TTaskParamList; const ATaskDescription: String; const APriority: TThreadPriority): TTaskID;
begin
  Result := 0;

  FCS.Enter;
  try
    if Busy or Terminated then
      Exit;
    SetNewTaskID;
  finally
    FCS.Leave;
  end;

  FTaskRec.Method.Code := @Task;
  FTaskRec.Method.Data := AObject;

  AObject.GetInterface(IInterface, FProcOwner);

  FParams := Copy(Params);
  FTaskRec.CallType := ctMethod;
  FDescription := ATaskDescription;
  Priority := APriority;

  Result := CurrentTaskID;

  ResetEvent(FEndTaskEvent);
  ResetEvent(FTerminateTaskEvent);
  SetEvent(FWakeUpEvent);
end;

procedure TTaskThread.SetNewTaskID;
begin
  // must be thread-safe call
  Inc(TaskIDCount);
  if TaskIDCount = 0 then
    Inc(TaskIDCount);

  FTaskID := TaskIDCount;

// todo: TaskID: Int64;  
// todo: need to make sure it's unique nbr!!!
end;

procedure TTaskThread.TerminateTask;
begin
  SetEvent(FTerminateTaskEvent);
end;

procedure TTaskThread.ReleaseThread;
begin
  Terminate;
  TerminateTask;
  SetEvent(FWakeUpEvent);
end;


function TTaskThread.CurrentTaskID: TTaskID;
begin
  FCS.Enter;
  try
    Result := FTaskID;
  finally
    FCS.Leave;
  end;
end;

procedure TTaskThread.WaitForTaskEnd;
begin
  // avoiding of deadlock if this thread is trying to wait for itself
  if GetCurrentThreadTaskID <> CurrentTaskID then
    WaitForSingleObject(FEndTaskEvent, INFINITE);
end;

function TTaskThread.WaitForTaskEnd(const ATimeOut: Integer): Boolean;
begin
  // avoiding of deadlock if this thread is trying to wait for itself
  if GetCurrentThreadTaskID <> CurrentTaskID then
  begin
    if ATimeOut = 0 then
    begin
      WaitForSingleObject(FEndTaskEvent, INFINITE);
      Result := True;
    end
    else
      Result := WaitForSingleObject(FEndTaskEvent, ATimeOut) = WAIT_OBJECT_0;
  end
  else
    Result := True;
end;

{ TReactiveThread }

constructor TReactiveThread.Create(const AThreadName: String; const APriority: TThreadPriority = tpNormal);
begin
  FAlarmEvent := CreateEvent(nil, False, False, nil);
  FTaskID := GlobalThreadManager.RunTask(Loop, Self, MakeTaskParams([]), AThreadName, APriority);
end;

destructor TReactiveThread.Destroy;
begin
  if FTaskID <> 0 then
  begin
    if GlobalThreadManagerIsActive then
      GlobalThreadManager.TerminateTask(FTaskID);
    Wakeup;
    if GlobalThreadManagerIsActive then
      GlobalThreadManager.WaitForTaskEnd(FTaskID);
  end;
  CloseHandle(FAlarmEvent);
  inherited;
end;

procedure TReactiveThread.DoOnStart;
begin
end;

procedure TReactiveThread.DoOnStop;
begin
end;

function TReactiveThread.IsActive: Boolean;
begin
  Result := GlobalThreadManager.GetThreadIDByTaskID(FTaskID) <> 0;
end;

procedure TReactiveThread.Loop(const Params: TTaskParamList);
var
  EventList: TWOHandleArray;
  wt: Cardinal;
begin
  EventList[0] := ThreadObject.FTerminateTaskEvent;
  EventList[1] := FAlarmEvent;

  DoOnStart;
  try
    while True do
    begin
      if FWaitTime = 0 then
        wt := INFINITE
      else
        wt := FWaitTime;
      WaitForMultipleObjects(2, @EventList, False, wt);
      if CurrentThreadTaskTerminated then
        break;
      DoWork;
    end;
  finally
    DoOnStop;
  end;
end;

procedure TReactiveThread.SetFWaitTime(const AValue: Cardinal);
begin
  FWaitTime := AValue;
  Wakeup;
end;

procedure TReactiveThread.Wakeup;
begin
  SetEvent(FAlarmEvent);
end;

{ TCallbackList }

constructor TCallbackList.Create;
begin
  inherited Create;
  Duplicates := dupIgnore;
end;

procedure TCallbackList.ProcessCallbacks;
var
  i: Integer;
  L: TList;
begin
  L := LockList;
  try
    for i := 0 to L.Count - 1 do
      TProcedure(L[i]);
  finally
    UnlockList;
  end;
end;


{ TSynchronizer }

constructor TSynchronizer.Create;
begin
  FHandle := isVCLBugFix.AllocateHwnd(WndProc); // must be called in main thread!
end;

destructor TSynchronizer.Destroy;
begin
  isVCLBugFix.DeallocateHWnd(FHandle);
  inherited;
end;

procedure TSynchronizer.Run(const ATaskEntryPoint: TTaskRec; const AParams: TTaskParamList;
  const ABlockingMode: Boolean);
var
  O: TSyncInfo;
begin
  if CurrentThreadIsMain and ABlockingMode then
  begin
    O := TSyncInfo.Create(ATaskEntryPoint, AParams, False);
    try
      O.Execute;
    finally
      O.Free;
    end;
  end

  else
  begin
    O := TSyncInfo.Create(ATaskEntryPoint, AParams, ABlockingMode);
    try
      PostMessage(FHandle, CM_SYNCHRONIZE, Integer(O), 0);
    finally
      if ABlockingMode then
      begin
        O.WaitUntilFinished;
        O.Free;
      end;
    end;  
  end;
end;

procedure TSynchronizer.WndProc(var Message: TMessage);
var
  O: TSyncInfo;
begin
  if Message.Msg = CM_SYNCHRONIZE then
  begin
    O := TSyncInfo(Pointer(Message.WParam));
    try
      O.Execute;
    finally
      if not O.BlockingMode then
        O.Free;
    end;
  end;
end;

{ TSyncInfo }

function TSyncInfo.BlockingMode: Boolean;
begin
  Result := FEvent <> 0;
end;

constructor TSyncInfo.Create(const ATaskEntryPoint: TTaskRec; const AParams: TTaskParamList;
  const ABlockingMode: Boolean);
begin
  FTaskEntryPoint := ATaskEntryPoint;
  FParams := AParams;
  if ABlockingMode then
    FEvent := CreateEvent(nil, True, False, nil);
end;

destructor TSyncInfo.Destroy;
begin
  if BlockingMode then
    CloseHandle(FEvent);
  inherited;
end;

procedure TSyncInfo.Execute;
begin
  try
    if FTaskEntryPoint.CallType = ctMethod then
      PTaskProcObj(FTaskEntryPoint.Method)(FParams)
    else
      FTaskEntryPoint.TaskProc(FParams);
  finally
    MarkAsFinished;
  end;  
end;

procedure TSyncInfo.MarkAsFinished;
begin
  if BlockingMode then
    SetEvent(FEvent);
end;

procedure TSyncInfo.WaitUntilFinished;
begin
  if BlockingMode then
    WaitForSingleObject(FEvent, INFINITE);
end;

initialization
  Synchronizer := TSynchronizer.Create;
  OnThreadCreateCallbacks := TCallbackList.Create;
  OnThreadDestroyCallbacks := TCallbackList.Create;
  SetCurrentThreadName('Main Thread');
  GlobalThreadManager;


finalization
  UnitFinalization := True;
  DeactivateGlobalThreadManager;
  FreeAndNil(Synchronizer);
  FreeAndNil(OnThreadCreateCallbacks);
  FreeAndNil(OnThreadDestroyCallbacks);

end.

