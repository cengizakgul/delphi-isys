unit isSettings;

interface

uses Classes, Windows, SysUtils, IniFiles, Variants, SyncObjs, EvStreamUtils, isBasicUtils, isBaseClasses,
     Registry, SimpleXML, DISQLite3IniFile;

type
  IisSettings = interface
  ['{68055D99-6907-46D1-8E21-C82F861C902C}']
    procedure Lock;
    procedure Unlock;
    procedure BeginUpdate;
    procedure EndUpdate;
    function  TopNodeName: String;
    procedure SetValue(const AName: String; const AValue: Variant);
    function  GetValue(const AName: String; const ADefaultValue: Variant): Variant;
    procedure SetAsString(const AName, AValue: String);
    function  GetAsString(const AName: String): String;
    procedure SetAsInteger(const AName: String; const AValue: Integer);
    function  GetAsInteger(const AName: String): Integer;
    procedure SetAsFloat(const AName: String; const AValue: Double);
    function  GetAsFloat(const AName: String): Double;
    procedure SetAsDateTime(const AName: String; const AValue: TDateTime);
    function  GetAsDateTime(const AName: String): TDateTime;
    procedure SetAsBoolean(const AName: String; const AValue: Boolean);
    function  GetAsBoolean(const AName: String): Boolean;
    procedure SetAsBlob(const AName: String; const AValue: IisStream);
    function  GetAsBlob(const AName: String): IisStream;
    function  GetValueNames(const ANodeName: String): IisStringListRO;
    function  GetChildNodes(const ANodeName: String): IisStringListRO;
    procedure DeleteNode(const ANodeName: String);
    procedure ClearNode(const ANodeName: String);
    procedure DeleteValue(const AName: String);
    property  AsString[const AName: String]: String read GetAsString write SetAsString; default;
    property  AsInteger[const AName: String]: Integer read GetAsInteger write SetAsInteger;
    property  AsFloat[const AName: String]: Double read GetAsFloat write SetAsFloat;
    property  AsDateTime[const AName: String]: TDateTime read GetAsDateTime write SetAsDateTime;
    property  AsBoolean[const AName: String]: Boolean read GetAsBoolean write SetAsBoolean;
    property  AsBlob[const AName: String]: IisStream read GetAsBlob write SetAsBlob;
  end;


  TisCustomSettings = class(TisInterfacedObject, IisSettings)
  private
    FPrimaryPath: String;
  protected
    procedure BeginUpdate; virtual;
    procedure EndUpdate; virtual;
    function  TopNodeName: String;
    procedure SetAsString(const AName, AValue: String);
    function  GetAsString(const AName: String): String;
    procedure SetAsInteger(const AName: String; const AValue: Integer);
    function  GetAsInteger(const AName: String): Integer;
    procedure SetAsFloat(const AName: String; const AValue: Double);
    function  GetAsFloat(const AName: String): Double;
    procedure SetAsDateTime(const AName: String; const AValue: TDateTime);
    function  GetAsDateTime(const AName: String): TDateTime;
    procedure SetAsBoolean(const AName: String; const AValue: Boolean);
    function  GetAsBoolean(const AName: String): Boolean;
    procedure SetAsBlob(const AName: String; const AValue: IisStream);
    function  GetAsBlob(const AName: String): IisStream;
    procedure ClearNode(const ANodeName: String);

    procedure SetValue(const AName: String; const AValue: Variant); virtual; abstract;
    function  GetValue(const AName: String; const ADefaultValue: Variant): Variant; virtual; abstract;
    function  GetValueNames(const ANodeName: String): IisStringListRO; virtual; abstract;
    function  GetChildNodes(const ANodeName: String): IisStringListRO; virtual; abstract;
    procedure DeleteNode(const ANodeName: String); virtual; abstract;
    procedure DeleteValue(const AName: String); virtual; abstract;
  public
    constructor Create(const APrimaryPath: String); reintroduce;
  end;


  TisSettingsRegistry = class(TisCustomSettings)
  private
    FHolder: TRegistry;
  protected
    procedure SetValue(const AName: String; const AValue: Variant); override;
    function  GetValue(const AName: String; const ADefaultValue: Variant): Variant; override;
    function  GetValueNames(const ANodeName: String): IisStringListRO; override;
    function  GetChildNodes(const ANodeName: String): IisStringListRO; override;
    procedure DeleteNode(const ANodeName: String); override;
    procedure DeleteValue(const AName: String); override;
  public
    constructor Create(const ARootKey: DWORD; const APrimaryPath: String); reintroduce;
    destructor  Destroy; override;
  end;


  TisSettingsINI = class(TisCustomSettings)
  private
    FHolder: TCustomIniFile;
    function PathToName(const APath: String): String;
    function IsBlobFileName(const AFileName: String): Boolean;
  protected
    function CreateHolder(const AFileName: String): TCustomIniFile; virtual;

    procedure SetValue(const AName: String; const AValue: Variant); override;
    function  GetValue(const AName: String; const ADefaultValue: Variant): Variant; override;
    function  GetValueNames(const ANodeName: String): IisStringListRO; override;
    function  GetChildNodes(const ANodeName: String): IisStringListRO; override;
    procedure DeleteNode(const ANodeName: String); override;
    procedure DeleteValue(const AName: String); override;
  public
    constructor Create(const AFileName: String; const APrimaryPath: String = ''); reintroduce;
    constructor CreateFromStrings(const AContent: TStrings; const APrimaryPath: String = '');
    destructor  Destroy; override;
  end;


  TisSettingsXML = class(TisCustomSettings)
  private
    FHolder: IXmlDocument;
    FFileName: String;
    function  BuildXPath(const AName: String): String;
    procedure Save;
  protected
    procedure SetValue(const AName: String; const AValue: Variant); override;
    function  GetValue(const AName: String; const ADefaultValue: Variant): Variant; override;
    function  GetValueNames(const ANodeName: String): IisStringListRO; override;
    function  GetChildNodes(const ANodeName: String): IisStringListRO; override;
    procedure DeleteNode(const ANodeName: String); override;
    procedure DeleteValue(const AName: String); override;
  public
    constructor Create(const AFileName: String; const APrimaryPath: String = '';
                       const ARootElement: String = ''); reintroduce;
  end;


  TisSettingsFolder = class(TisCustomSettings)
  private
    FRootPath: String;
  protected
    procedure SetValue(const AName: String; const AValue: Variant); override;
    function  GetValue(const AName: String; const ADefaultValue: Variant): Variant; override;
    function  GetValueNames(const ANodeName: String): IisStringListRO; override;
    function  GetChildNodes(const ANodeName: String): IisStringListRO; override;
    procedure DeleteNode(const ANodeName: String); override;
    procedure DeleteValue(const AName: String); override;
  public
    constructor Create(const ARootPath: String; const APrimaryPath: String = ''); reintroduce;
  end;


  TisSettingsSQLite = class(TisSettingsINI)
  protected
    function CreateHolder(const AFileName: String): TCustomIniFile; override;

    procedure BeginUpdate; override;
    procedure EndUpdate; override;
    procedure SetValue(const AName: String; const AValue: Variant); override;
    procedure DeleteNode(const ANodeName: String); override;
    procedure DeleteValue(const AName: String); override;
  end;

  procedure WriteToIniFile(const AFileName: String; const APath: String; const AValue: Variant);
  function  ReadFromIniFile(const AFileName: String; const APath: String; const ADefaultValue: Variant): Variant;

implementation

type
  TisIniFile = class(TIniFile)
  public
    function ReadString(const Section, Ident, Default: string): string; override;
  end;

function EncodeIntoOneStr(const AText: String): String;
begin
  Result := StringReplace(AText, #13, '&013', [rfReplaceAll]);
  Result := StringReplace(Result, #10, '&010', [rfReplaceAll]);
end;


function DecodeFromOneStr(const AEncodedText: String): String;
begin
  Result := StringReplace(AEncodedText, '&013', #13, [rfReplaceAll]);
  Result := StringReplace(Result, '&010', #10, [rfReplaceAll]);
end;


procedure WriteToIniFile(const AFileName: String; const APath: String; const AValue: Variant);
var
  Ini: IisSettings;
begin
  Ini := TisSettingsINI.Create(AFileName);
  Ini.SetValue(APath, AValue);
end;


function ReadFromIniFile(const AFileName: String; const APath: String; const ADefaultValue: Variant): Variant;
var
  Ini: IisSettings;
begin
  Ini := TisSettingsINI.Create(AFileName);
  Result := Ini.GetValue(APath, ADefaultValue);
end;


procedure ErrorBlobNotSupported;
begin
  raise Exception.Create('Blob type is not supported');
end;


{ TisCustomSettings }

procedure TisCustomSettings.BeginUpdate;
begin
  Lock;
end;

procedure TisCustomSettings.ClearNode(const ANodeName: String);
var
  L: IisStringListRO;
  i: Integer;
begin
  L := GetValueNames(ANodeName);
  for i := 0 to L.Count - 1 do
    DeleteValue(NormalizePath(ANodeName) + L[i]);

  L := GetChildNodes(ANodeName);
  for i := 0 to L.Count - 1 do
    DeleteNode(NormalizePath(ANodeName) + L[i]);
end;

constructor TisCustomSettings.Create(const APrimaryPath: String);
begin
  inherited Create(nil, True);

  FPrimaryPath := Trim(APrimaryPath);
  if FPrimaryPath <> '' then
    FPrimaryPath := NormalizePath(FPrimaryPath);
end;

procedure TisCustomSettings.EndUpdate;
begin
  Unlock;
end;

function TisCustomSettings.GetAsBlob(const AName: String): IisStream;
var
  S: IisStream;
begin
  Result := IInterface(GetValue(AName, S)) as IisStream;
end;

function TisCustomSettings.GetAsBoolean(const AName: String): Boolean;
begin
  Result := GetValue(AName, False);
end;

function TisCustomSettings.GetAsDateTime(const AName: String): TDateTime;
var
 s: String;
begin
  s := GetValue(AName, '');
  try
    Result := TextToDate(s);
  except
    Result := 0;
  end;
end;

function TisCustomSettings.GetAsFloat(const AName: String): Double;
begin
  Result := GetValue(AName, VarAsType(0, varDouble));
end;

function TisCustomSettings.GetAsInteger(const AName: String): Integer;
begin
  Result := GetValue(AName, Integer(0));
end;

function TisCustomSettings.GetAsString(const AName: String): String;
begin
  Result := GetValue(AName, '');
end;

procedure TisCustomSettings.SetAsBlob(const AName: String; const AValue: IisStream);
begin
  SetValue(AName, AValue);
end;

procedure TisCustomSettings.SetAsBoolean(const AName: String; const AValue: Boolean);
begin
  SetValue(AName, AValue);
end;

procedure TisCustomSettings.SetAsDateTime(const AName: String; const AValue: TDateTime);
begin
  SetValue(AName, DateToText(AValue));
end;

procedure TisCustomSettings.SetAsFloat(const AName: String; const AValue: Double);
begin
  SetValue(AName, AValue);
end;

procedure TisCustomSettings.SetAsInteger(const AName: String; const AValue: Integer);
begin
  SetValue(AName, AValue);
end;

procedure TisCustomSettings.SetAsString(const AName, AValue: String);
begin
  SetValue(AName, AValue);
end;

function TisCustomSettings.TopNodeName: String;
begin
  Result := FPrimaryPath;
end;


{ TisIniFile }

function TisIniFile.ReadString(const Section, Ident, Default: string): string;
var
  Buffer: PChar;
  n, s: Integer;
begin
  s := 0;
  n := 2048;
  Buffer := nil;
  try
    while True do
    begin
      ReallocMem(Buffer, n);
      s := GetPrivateProfileString(PChar(Section), PChar(Ident), PChar(Default), Buffer, n, PChar(FileName));
      if s <> n - 1 then
        break;
      n := n * 2;
    end;

    SetString(Result, Buffer, s);
  finally
    FreeMem(Buffer, n);
  end;
end;


{ TisSettingsRegistry }

constructor TisSettingsRegistry.Create(const ARootKey: DWORD; const APrimaryPath: String);
begin
  inherited Create(APrimaryPath);
  FHolder := TRegistry.Create;
  FHolder.RootKey := ARootKey;
end;

procedure TisSettingsRegistry.DeleteNode(const ANodeName: String);
begin
  Lock;
  try
    FHolder.DeleteKey(FPrimaryPath + ANodeName);
  finally
    Unlock;
  end;
end;

procedure TisSettingsRegistry.DeleteValue(const AName: String);
var
  sPath, sValName: String;
begin
  Lock;
  try
    sPath := AName;
    sValName := GetPrevStrValue(sPath, '\');
    if FHolder.OpenKey(FPrimaryPath + sPath, False) then
      try
        FHolder.DeleteValue(sValName);
      finally
        FHolder.CloseKey;
      end;
  finally
    Unlock;
  end;
end;

destructor TisSettingsRegistry.Destroy;
begin
  FreeAndNil(FHolder);
  inherited;
end;

function TisSettingsRegistry.GetChildNodes(const ANodeName: String): IisStringListRO;
var
  Str: TStringList;
  OldAccess: Cardinal;
begin
  Lock;
  try
    OldAccess := FHolder.Access;
    Result := TisStringList.Create;
    if FHolder.OpenKeyReadOnly(FPrimaryPath + ANodeName) then
      try
        Str := TStringList.Create;
        try
          FHolder.GetKeyNames(Str);
          (Result as IisStringList).Text := Str.Text;
        finally
          Str.Free;
        end;
      finally
        FHolder.CloseKey;
        FHolder.Access := OldAccess;
      end;
  finally
    Unlock;
  end;
end;

function TisSettingsRegistry.GetValue(const AName: String; const ADefaultValue: Variant): Variant;
var
  sPath, sVal, sValName: String;
  DataType: TRegDataType;
  BinDataSize: Integer;
  OldAccess: Cardinal;
  BinData: IisStream;
begin
  Lock;
  try
    sPath := AName;
    sValName := GetPrevStrValue(sPath, '\');

    OldAccess := FHolder.Access;
    if FHolder.OpenKeyReadOnly(FPrimaryPath + sPath) then
      try
        DataType := FHolder.GetDataType(sValName);
        case DataType of
          rdInteger: sVal := IntToStr(FHolder.ReadInteger(sValName));
          rdBinary: begin
                      BinDataSize := FHolder.GetDataSize(sValName);
                      SetLength(sVal, BinDataSize);
                      if BinDataSize > 0 then
                        FHolder.ReadBinaryData(sValName, sVal[1], BinDataSize);
                    end;
        else
          sVal := FHolder.ReadString(sValName);
        end;

        try
          if (sVal = '') and (VarType(ADefaultValue) <> varString) then
            Result := ADefaultValue
          else
          begin
            if VarType(ADefaultValue) = varUnknown then
            begin
              if DataType = rdBinary then
              begin
                BinData := TisStream.Create(Length(sVal));
                BinData.AsString := sVal;
                BinData.Position := 0;
                Result := BinData;
              end
              else
                ErrorBlobNotSupported;
            end
            else
              Result := VarAsType(sVal, VarType(ADefaultValue));
          end;
        except
          Result := ADefaultValue;
        end;
      finally
        FHolder.CloseKey;
        FHolder.Access := OldAccess;
      end
    else
      Result := ADefaultValue;
  finally
    Unlock;
  end;
end;

function TisSettingsRegistry.GetValueNames(const ANodeName: String): IisStringListRO;
var
  Str: TStringList;
  OldAccess: Cardinal;
begin
  Lock;
  try
    Result := TisStringList.Create;
    OldAccess := FHolder.Access;
    if FHolder.OpenKeyReadOnly(FPrimaryPath + ANodeName) then
      try
        Str := TStringList.Create;
        try
          FHolder.GetValueNames(Str);
          (Result as IisStringList).Text := Str.Text;
        finally
          Str.Free;
        end;
      finally
        FHolder.CloseKey;
        FHolder.Access := OldAccess;
      end;
  finally
    Unlock;
  end;
end;

procedure TisSettingsRegistry.SetValue(const AName: String; const AValue: Variant);
var
  sPath, sVal, sValName: String;
  BinData: IisStream;
  Buf: Pointer;
begin
  sPath := AName;
  sValName := GetPrevStrValue(sPath, '\');

  if VarType(AValue) = varUnknown then
  begin
    BinData := IInterface(AValue) as IisStream;
    Lock;
    try
      if FHolder.OpenKey(FPrimaryPath + sPath, Assigned(BinData) and (BinData.Size > 0)) then
        try
          if not Assigned(BinData) or (BinData.Size = 0) then
            FHolder.DeleteValue(sValName)
          else
          begin
            GetMem(Buf, BinData.Size);
            try
              BinData.Position := 0;
              BinData.ReadBuffer(Buf^, BinData.Size);
              FHolder.WriteBinaryData(sValName, Buf^, BinData.Size);
            finally
              FreeMem(Buf);
            end
          end;
        finally
          FHolder.CloseKey;
        end;
    finally
      Unlock;
    end;
  end

  else
  begin
    if VarType(AValue) = varBoolean then
      sVal := IntToStr(Ord(Boolean(AValue)))
    else
      sVal := VarToStr(AValue);

    Lock;
    try
      if FHolder.OpenKey(FPrimaryPath + sPath, sVal <> '') then
        try
          if sVal = '' then
            FHolder.DeleteValue(sValName)
          else
            FHolder.WriteString(sValName, sVal);
        finally
          FHolder.CloseKey;
        end;
    finally
      Unlock;
    end;
  end;
end;


{ TisSettingsINI }

constructor TisSettingsINI.Create(const AFileName, APrimaryPath: String);
begin
  inherited Create(APrimaryPath);
  FHolder := CreateHolder(AFileName);
end;

procedure TisSettingsINI.DeleteNode(const ANodeName: String);
var
  Sections: IisStringListRO;
  i: Integer;

  procedure DoEraseSection(const APath: String);
  var
    L: IisStringListRO;
    sBlobFile: String;
    i: Integer;
  begin
    L := GetValueNames(APath);
    for i := 0 to L.Count - 1 do
    begin
      sBlobFile := GetAsString(NormalizePath(APath) + L[i]);
      if IsBlobFileName(sBlobFile) then
      begin
        sBlobFile := ChangeFilePath(sBlobFile, ExtractFilePath(FHolder.FileName));
        DeleteFile(sBlobFile);
      end;
    end;

    FHolder.EraseSection(PathToName(APath));
  end;

begin
  Lock;
  try
    Sections := GetChildNodes(ANodeName);
    for i := 0 to Sections.Count - 1 do
      if Sections[i] <> '' then
        DeleteNode(NormalizePath(ANodeName) + Sections[i]);

    for i := 0 to Sections.Count - 1 do
      DoEraseSection(NormalizePath(ANodeName) + Sections[i]);

    DoEraseSection(ANodeName);
  finally
    Unlock;
  end;
end;

procedure TisSettingsINI.DeleteValue(const AName: String);
var
  Ident, Section: String;
begin
  Lock;
  try
    Section := FPrimaryPath + AName;
    Ident := GetPrevStrValue(Section, '\');
    Section := PathToName(Section);
    FHolder.DeleteKey(Section, Ident);
  finally
    Unlock;
  end;
end;

destructor TisSettingsINI.Destroy;
begin
  FreeAndNil(FHolder);
  inherited;
end;

function TisSettingsINI.GetChildNodes(const ANodeName: String): IisStringListRO;
var
  Str: TStringList;
  i: Integer;
  SectionFragment: String;
  s: String;
begin
  Lock;
  try
    Result := TisStringList.Create;
    Str := TStringList.Create;
    try
      FHolder.ReadSections(Str);
      SectionFragment := PathToName(NormalizePath(FPrimaryPath + ANodeName));
      if SectionFragment <> '' then
        for i := Str.Count - 1 downto 0 do
          if StartsWith(Str[i], SectionFragment) then
          begin
            S := Str[i];
            Delete(S, 1, Length(SectionFragment));
            S := GetNextStrValue(S, '.');
            if S <> '' then
              Str[i] := S
            else
              Str.Delete(i);
          end
          else
            Str.Delete(i);

      (Result as IisStringList).Text := Str.Text;
    finally
      FreeAndNil(Str);
    end;
  finally
    Unlock;
  end;
end;

function TisSettingsINI.GetValue(const AName: String; const ADefaultValue: Variant): Variant;
var
  Ident, Section, h: String;
  S: IEvDualStream;
  sBlobFile: String;
begin
  Lock;
  try
    Result := ADefaultValue;

    try
      Section := FPrimaryPath + AName;
      Ident := GetPrevStrValue(Section, '\');
      Section := PathToName(Section);

      if VarType(ADefaultValue) = varUnknown then
      begin
        sBlobFile := FHolder.ReadString(Section, Ident, '');
        if not IsBlobFileName(sBlobFile) then
          Exit;
        sBlobFile := ChangeFilePath(sBlobFile, ExtractFilePath(FHolder.FileName));
        S := TisStream.CreateFromFile(sBlobFile, False, False, True);
        Result := S;
      end

      else if VarIsArray(ADefaultValue) then
      begin
        S := TisStream.CreateInMemory;
        FHolder.ReadBinaryStream(Section, Ident, S.RealStream);
        Result := S.AsVarArrayOfBytes;
      end

      else
      begin
        h := FHolder.ReadString(Section, Ident, '');
        h := DecodeFromOneStr(h);

        if h = '' then // workaround for some weird bug
          h := VarToStr(ADefaultValue);

        Result := VarAsType(h, VarType(ADefaultValue));
      end;
    except
      // in case if settings file is not accessible
    end;
  finally
    Unlock;
  end;
end;

function TisSettingsINI.GetValueNames(const ANodeName: String): IisStringListRO;
var
  Str: TStringList;
begin
  Lock;
  try
    Result := TisStringList.Create;
    Str := TStringList.Create;
    try
      FHolder.ReadSection(PathToName(FPrimaryPath + ANodeName), Str);
      (Result as IisStringList).Text := Str.Text;
    finally
      FreeAndNil(Str);
    end;
  finally
    Unlock;
  end;
end;

function TisSettingsINI.CreateHolder(const AFileName: String): TCustomIniFile;
var
  sFileName: String;
begin
  sFileName := AFileName;
  if (sFileName <> '') and not FileExists(sFileName) then
  begin
    try
      TFileStream.Create(sFileName, fmCreate).Free;  // create an empty file if possible (no rights case)
    except
      sFileName := '';
    end;
  end;

  if sFileName = '' then
    Result := TMemIniFile.Create(sFileName)
  else
    Result := TisIniFile.Create(sFileName);
end;

function TisSettingsINI.IsBlobFileName(const AFileName: String): Boolean;
begin
  Result := (Length(AFileName) = 32 + 5) and FinishesWith(AFileName, '.blob');
end;

function TisSettingsINI.PathToName(const APath: String): String;
begin
  Result := StringReplace(APath, '\', '.', [rfReplaceAll]);
end;

procedure TisSettingsINI.SetValue(const AName: String; const AValue: Variant);
var
  Ident, Section: String;
  S: IEvDualStream;
  Val: String;
  sBlobFile: String;
  BlobStream, BlobFileStream: IisStream;
begin
  Lock;
  try
    try
      Section := FPrimaryPath + AName;
      Ident := GetPrevStrValue(Section, '\');
      Section := PathToName(Section);

      Assert(Section <> '');
      Assert(Ident <> '');

      if VarType(AValue) = varUnknown then
      begin
        sBlobFile := FHolder.ReadString(Section, Ident, '');

        if not IsBlobFileName(sBlobFile) then
          sBlobFile := GetUniqueID + '.blob';

        sBlobFile := ChangeFilePath(sBlobFile, ExtractFilePath(FHolder.FileName));

        BlobStream := IInterface(AValue) as IisStream;
        if Assigned(BlobStream) then
        begin
          if FileExists(sBlobFile) and AnsiSameText(BlobStream.FileName, sBlobFile) then
            BlobStream.Flush
          else
          begin
            if FileExists(sBlobFile) then
              BlobFileStream := TisStream.CreateFromFile(sBlobFile, False, False, True)
            else
              BlobFileStream := TisStream.CreateFromNewFile(sBlobFile, False, True);
            BlobFileStream.Size := 0;
            BlobFileStream.CopyFrom(BlobStream, 0);

            FHolder.WriteString(Section, Ident, ExtractFileName(sBlobFile))
          end;
        end

        else
        begin
          DeleteFile(sBlobFile);
          FHolder.WriteString(Section, Ident, '');
        end
      end

      else if VarIsArray(AValue) then
      begin
        S := TisStream.CreateFromVariant(AValue);
        S.Position := 0;
        FHolder.WriteBinaryStream(Section, Ident, S.RealStream);
      end

      else
      begin
        Val := EncodeIntoOneStr(VarToStr(AValue));
        if Trim(Val) = Val then
          FHolder.WriteString(Section, Ident, Val)
        else
          FHolder.WriteString(Section, Ident, '"' + Val + '"');
      end;

    except
      // in case if settings file is not accessible
    end;
  finally
    Unlock;
  end;
end;

constructor TisSettingsINI.CreateFromStrings(const AContent: TStrings; const APrimaryPath: String);
begin
  Create('', APrimaryPath);
  (FHolder as TMemIniFile).SetStrings(AContent);
end;

{ TisSettingsXML }

constructor TisSettingsXML.Create(const AFileName, APrimaryPath, ARootElement: String);
begin
  inherited Create(APrimaryPath);

  FFileName := AFileName;
  if (FFileName = '') or not FileExists(FFileName) then
  begin
    if ARootElement = '' then
      FHolder := CreateXmlDocument('root')
    else
      FHolder := CreateXmlDocument(ARootElement);
  end
  else
    FHolder := LoadXmlDocument(FFileName);
end;

procedure TisSettingsXML.DeleteNode(const ANodeName: String);
var
  Item: IXmlItem;
  Parent: IXmlNode;
  sPath: String;
begin
  Lock;
  try
    sPath := BuildXPath(NormalizePath(ANodeName));
    Item := FHolder.DocumentElement.SelectNode(sPath);
    if Assigned(Item) then
    begin
      Parent := (Item as IXmlNode).ParentNode;
      Parent.RemoveChild(Item as IXmlNode);
      Item := nil;
      Save;
    end;
  finally
    Unlock;
  end;
end;

procedure TisSettingsXML.DeleteValue(const AName: String);
var
  Item: IXmlItem;
  Node: IXmlNode;
  sPath: String;
begin
  Lock;
  try
    sPath := BuildXPath(DenormalizePath(AName));
    Item := FHolder.DocumentElement.SelectNode(sPath);
    if Assigned(Item) then
    begin
      Node := (Item as IXmlAttribute).Node;
      Node.RemoveAttr((Item as IXmlAttribute).AttrName);
      Item := nil;
      Save;
    end;
  finally
    Unlock;
  end;
end;

function TisSettingsXML.GetChildNodes(const ANodeName: String): IisStringListRO;
var
  L: IisList;
  i: Integer;
  Res: IisStringList;
  sPath: String;
begin
  Lock;
  try
    sPath := BuildXPath(NormalizePath(ANodeName)) + '/*';
    L := FHolder.DocumentElement.SelectNodes(sPath);

    Res := TisStringList.Create;
    Res.Capacity := L.Count;
    for i := 0 to L.Count - 1 do
      Res.AddObject((L[i] as IXmlElement).NodeName, L[i]);
  finally
    Unlock;
  end;
  Result := Res as IisStringListRO;
end;

function TisSettingsXML.GetValue(const AName: String; const ADefaultValue: Variant): Variant;
var
  Item: IXmlItem;
  sPath: String;
begin
  if VarType(ADefaultValue) = varUnknown then
    ErrorBlobNotSupported;

  Result := ADefaultValue;
  Lock;
  try
    sPath := BuildXPath(AName);
    Item := FHolder.DocumentElement.SelectNode(sPath);
    if Assigned(Item) then
    begin
      if Item.ItemType = NODE_ATTRIBUTE then
        Result := (Item as IXmlAttribute).Value
      else if Item.ItemType = NODE_ELEMENT then
        Result := (Item as IXmlElement).Text;

      try
        if VarIsNull(Result) or (VarType(ADefaultValue) <> varString) and (VarToStr(Result) = '') then
          Result := ADefaultValue
        else
          Result := VarAsType(Result, VarType(ADefaultValue));
      except
        Result := ADefaultValue;
      end;
    end;
  finally
    Unlock;
  end;
end;

function TisSettingsXML.GetValueNames(const ANodeName: String): IisStringListRO;
var
  L: IisList;
  i: Integer;
  Res: IisStringList;
  sPath: String;
begin
  Lock;
  try
    sPath := BuildXPath(NormalizePath(ANodeName)) + '/@*';
    L := FHolder.DocumentElement.SelectNodes(sPath);

    Res := TisStringList.Create;
    Res.Capacity := L.Count;
    for i := 0 to L.Count - 1 do
      Res.AddObject((L[i] as IXmlAttribute).AttrName, L[i]);
  finally
    Unlock;
  end;
  Result := Res as IisStringListRO;
end;

function TisSettingsXML.BuildXPath(const AName: String): String;
var
  sPath, sAttrName: String;
begin
  sPath := StringReplace(FPrimaryPath + Trim(AName), '\', '/', [rfReplaceAll]);

  sAttrName := GetPrevStrValue(sPath, '/');
  if sAttrName <> '' then
    sPath := sPath + '/@' + sAttrName;

  Result := '/' + sPath;
end;

procedure TisSettingsXML.SetValue(const AName: String; const AValue: Variant);
var
  Item: IXmlItem;

  function EnsurePath(aXPath: String): IXmlItem;
  var
    sEndName: String;
  begin
    Result := FHolder.DocumentElement.SelectNode(aXPath);
    if Assigned(Result) then
      Exit;

    sEndName := GetPrevStrValue(aXPath, '/');
    Result := EnsurePath(aXPath);
    if StartsWith(sEndName, '@') then
    begin
      (Result as IXmlElement).SetVarAttr(sEndName, AValue);
      Result := (Result as IXmlElement).GetAttribute(sEndName);
    end
    else
    begin
      sEndName := GetNextStrValue(sEndName, '[');
      Result := (Result as IXmlElement).AppendElement(sEndName);
    end;
  end;

begin
  if VarType(AValue) = varUnknown then
    ErrorBlobNotSupported;

  Lock;
  try
    Item := EnsurePath(BuildXPath(AName));
    if Item.ItemType = NODE_ELEMENT then
     (Item as IXmlElement).Text := VarToStr(aValue)
    else if Item.ItemType = NODE_ATTRIBUTE then
     (Item as IXmlAttribute).Value := aValue;

    Save;
  finally
    Unlock;
  end;
end;

procedure TisSettingsXML.Save;
begin
  if FFileName <> '' then
    FHolder.Save(FFileName);
end;


{ TisSettingsFolder }

constructor TisSettingsFolder.Create(const ARootPath, APrimaryPath: String);
begin
  inherited Create(APrimaryPath);
  FRootPath := NormalizePath(Trim(ARootPath));
end;

procedure TisSettingsFolder.DeleteNode(const ANodeName: String);
var
  NodePath: String;
begin
  NodePath := FRootPath + FPrimaryPath + ANodeName;

  Lock;
  try
    ClearDir(NodePath, True);
  finally
    Unlock;
  end;
end;

procedure TisSettingsFolder.DeleteValue(const AName: String);
var
  ValFileName: String;
begin
  ValFileName := FRootPath + FPrimaryPath + AName;

  Lock;
  try
    DeleteFile(ValFileName);
  finally
    Unlock;
  end;
end;

function TisSettingsFolder.GetChildNodes(const ANodeName: String): IisStringListRO;
var
  NodePath: String;
  Folders: IisStringList;
  i: Integer;
begin
  NodePath := FRootPath + FPrimaryPath + ANodeName;

  Lock;
  try
    Folders := GetSubDirs(NodePath);
  finally
    Unlock;
  end;

  for i := 0 to Folders.Count - 1 do
    Folders[i] := ExtractFileName(Folders[i]);

  Result := Folders as IisStringListRO;
end;

function TisSettingsFolder.GetValue(const AName: String; const ADefaultValue: Variant): Variant;
var
  ValFileName, Val: String;
  ValFile: IisStream;
begin
  ValFileName := FRootPath + FPrimaryPath + AName;
  Result := ADefaultValue;

  Lock;
  try
    if FileExists(ValFileName) then
    begin
      if VarType(ADefaultValue) = varUnknown then
      begin
        ValFile := TisStream.CreateFromFile(ValFileName, False, False, True);
        Result := ValFile;
      end

      else if VarIsArray(ADefaultValue) then
      begin
        ValFile := TisStream.CreateFromFile(ValFileName, False, True, True);
        Result := ValFile.ReadVariant;
      end

      else
      begin
        ValFile := TisStream.CreateFromFile(ValFileName, False, True, True);
        Val := ValFile.AsString;
        try
          Result := VarAsType(Val, VarType(ADefaultValue));
        except
          Result := ADefaultValue;
        end;  
      end;
    end;
  finally
    Unlock;
  end;
end;

function TisSettingsFolder.GetValueNames(const ANodeName: String): IisStringListRO;
var
  NodePath: String;
  Files: IisStringList;
  i: Integer;
begin
  NodePath := NormalizePath(FRootPath + FPrimaryPath + ANodeName) + '*';

  Lock;
  try
    Files := GetFilesByMask(NodePath);
  finally
    Unlock;
  end;

  for i := 0 to Files.Count - 1 do
    Files[i] := ExtractFileName(Files[i]);

  Result := Files as IisStringListRO;
end;

procedure TisSettingsFolder.SetValue(const AName: String; const AValue: Variant);
var
  ValFile: IisStream;
  ValFileName: String;
  BlobStream: IisStream;

  function GetValFile: IisStream;
  begin
    if FileExists(ValFileName) then
      Result := TisStream.CreateFromFile(ValFileName, False, False, True)
    else
    begin
      ForceDirectories(ExtractFilePath(ValFileName));
      Result := TisStream.CreateFromNewFile(ValFileName, False, True);
    end;
    Result.Size := 0;
  end;

begin
  ValFileName := FRootPath + FPrimaryPath + AName;

  Lock;
  try
    if VarType(AValue) = varUnknown then
    begin
      BlobStream := IInterface(AValue) as IisStream;
      if Assigned(BlobStream) then
      begin
        if AnsiSameText(BlobStream.FileName, ValFileName) then
          BlobStream.Flush
        else
        begin
          ValFile := GetValFile;
          ValFile.CopyFrom(BlobStream, 0);
        end;
      end

      else
        DeleteFile(ValFileName);
    end

    else if VarIsNull(AValue) then
      DeleteFile(ValFileName)

    else if VarIsArray(AValue) then
    begin
      ValFile := GetValFile;
      ValFile.WriteVariant(AValue);
    end

    else
    begin
      ValFile := GetValFile;
      ValFile.AsString := VarToStr(AValue);
    end;

  finally
    Unlock;
  end;
end;


{ TisSettingsSQLite }

procedure TisSettingsSQLite.BeginUpdate;
begin
  inherited;
  TDISQLite3IniFile(FHolder).DB.BeginUpdate;
end;

procedure TisSettingsSQLite.EndUpdate;
begin
  TDISQLite3IniFile(FHolder).DB.EndUpdate;
  inherited;
end;

function TisSettingsSQLite.CreateHolder(const AFileName: String): TCustomIniFile;
var
  sFileName: String;
begin
  sFileName := AFileName;
  if (sFileName <> '') and not FileExists(sFileName) then
  begin
    try
      TFileStream.Create(sFileName, fmCreate).Free;  // create an empty file if possible (no rights case)
      DeleteFile(sFileName);
    except
      sFileName := ':memory:';
    end;
  end;

  Result := TDISQLite3IniFile.Create(sFileName);
end;

procedure TisSettingsSQLite.DeleteNode(const ANodeName: String);
begin
  BeginUpdate;
  try
    inherited;
  finally
    EndUpdate;
  end;
end;

procedure TisSettingsSQLite.DeleteValue(const AName: String);
begin
  BeginUpdate;
  try
    inherited;
  finally
    EndUpdate;
  end;
end;

procedure TisSettingsSQLite.SetValue(const AName: String; const AValue: Variant);
begin
  BeginUpdate;
  try
    inherited;
  finally
    EndUpdate;
  end;
end;

end.

