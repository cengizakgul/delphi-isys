{$WARN SYMBOL_PLATFORM OFF}

unit isTestUtilsImpl;

interface

uses
  isTestUtils, isExceptions;

function TestingEngine: IisTestingEngine;

implementation

uses Messages, SysUtils, Controls, Classes, Forms, Menus,
     ISTestControlPanelFrm, Dialogs, ExtCtrls, ISBasicUtils,
     typinfo, windows, isTestUtilsScript;

type
  TisTEAppStateRec = record
    LastEventTime: DWORD;
    ForegroundWndHandle: THandle;
    MouseEventTarget: TControl; //used only during recording to track control that supposed to have mouse capture

    PopupMenuHandle: HMENU;
    PopupMenuNesting: integer;

    //this stuff is used only by menu recorder
    PopupMenuStack: TDynStringArray;
    PopupMenuCommandSelected: boolean;
    PopupMenuCmdId: integer;
  end;

  PisTEAppStateRec = ^TisTEAppStateRec;

  TisCommandExecutor = class
  public
    constructor Create( appState: PisTEAppStateRec; aScheduledTime: DWORD );
    function Next: boolean; virtual; abstract;
    function FillEventMsg( var EventStruct: TEventMsg ): longint; virtual; abstract;
  protected
    FAppState: PisTEAppStateRec;
    FScheduledTime: DWORD;  // in ticks
    function CalcDelay: longint;
    procedure CheckCurrentWindow;
  end;

  TisTestingEngineSinks = class (TInterfacedObject, IisTECallBackHolder, IisTestingEngineCP )
  private
    FList: TInterfaceList;

    {IisTECallBackHolder}
    procedure BeforeStartRecording(aScript: IisTEScript2);
    procedure AfterStopRecording(aScript: IisTEScript2);
    procedure BeforeStartPlayback(aScript: IisTEScript2);
    procedure AfterStopPlayback(aScript: IisTEScript2; status: TisTestScriptStatus);

    {IisTestingEngineCP}
    procedure Advise( const aSink: IisTECallBackHolder );
    procedure UnAdvise( const aSink: IisTECallBackHolder );
  public
    constructor Create;
    destructor Destroy; override;
  end;

  TFreeNotificationEvent = procedure(Sender: TObject; AComponent: TComponent) of object;
  TFreeNotificationListener = class(TComponent)
  private
    FOnFreeNotify: TFreeNotificationEvent;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    property OnFreeNotify: TFreeNotificationEvent read FOnFreeNotify write FOnFreeNotify;
  end;

  TisTestingEngine = class(TInterfacedObject, IisTestingEngine, IisTestingEngineCP)
  private
    FState_: TisTstEngineState;
    FScript: IisTEScript2;
    FCommandExecutor: TisCommandExecutor;
    FCommandIndex: integer;
    FAppState: TisTEAppStateRec;
    FGetMessageHook: HHOOK;
    FCallWndProcRetHook: HHOOK;

    //recording stuff
    FJournalRecordHook: HHOOK;
    FFreeNotificationListener: TFreeNotificationListener;

    //playback stuff
    FJournalPlaybackHook: HHOOK;
    FWaitObjectTimer: TTimer;
    FPauseRequested: boolean;

    //common secondary stuff
    FControlPanel: TisTestControlPanel;
    FSink: IisTECallBackHolder;
    FTECP: IisTestingEngineCP;
    FConfigInfo: TisTEConfigRec;
    FAppKnowledge: IisApplicationKnowledgeSource;

    procedure SetMouseEventTarget( met: TControl );
    procedure HandleFreeNotification(Sender: TObject; AComponent: TComponent);

    function ScriptObjFromHandle( hForegroundWnd: THandle ): TisTEScriptObjectRec;
    function ScriptObjFromControl( C: TControl ): TisTEScriptObjectRec;

    procedure CreateExecutorOrStartWaiting;
    function CreateExecutor( cmd: IisTEScriptCommand ): TisCommandExecutor;

    procedure Unhook( var h: HHOOK );
    procedure RequireState( aStates: TisTstEngineStates );
    procedure SetState( aState: TisTstEngineState );
    procedure SetPlaybackHooks;
    procedure RemovePlaybackHooks;
    procedure SetRecordingHooks;
    procedure RemoveRecordingHooks;

    procedure RecordMsg(const EventStruct: TEventMsg);
    function  PlaybackMsg(ACode: Integer; var EventStruct: TEventMsg): LongInt;
    procedure CallWndProcRet( const CWPRetStruct: TCWPRetStruct );
    procedure CancelJournal;
    procedure GotCommandMessage( const Msg: TMsg );
    function  LookForWaitedObject: Boolean;
    procedure InitAppState;
    procedure OnWaitObjectEvent(Sender: TObject);
    procedure ShowControlPanel;
    procedure HideControlPanel;

    procedure BeginWaiting;
    procedure EndWaiting;
    procedure InternalStop( status: TisTestScriptStatus);

    {IisTestingEngine}
    procedure ExternalStop;
    procedure IisTestingEngine.Stop = ExternalStop;
    procedure Resume;
    procedure Pause;
    procedure StartRecording(aScript: IisTEScript2);
    procedure StartPlayback(aScript: IisTEScript2);
    function  GetState: TisTstEngineState;
    function  GetConfig: TisTEConfigRec;
    procedure SetConfig(const AConfigInfo: TisTEConfigRec);

    procedure SetApplicationKnowledgeSource( ks: IisApplicationKnowledgeSource );
    property State: TisTstEngineState read GetState;
    function CanInterrupt: boolean;
  protected
    property TECP: IisTestingEngineCP read FTECP implements IisTestingEngineCP;
  public
    constructor Create;
    destructor  Destroy; override;
  end;


{$WARNINGS OFF}
procedure HeapCheck;
begin
  if GetHeapStatus.HeapErrorCode <> 0 then
  begin
    ODS('GetHeapStatus.HeapErrorCode <> 0');
    DebugBreak;
  end
end;
{$WARNINGS ON}

//function BlockInput( fBlockIt: BOOL ): BOOL; external user32 name 'BlockInput';

const
  cErrWrongFileFormat = 'Wrong file format';
  cErrCannotFindObject = 'Cannot find object: %s(%s)';
  cErrUnexpectedWindow = 'Unexpected Window: %s';
  cErrUnknownCommand = 'Unknown command: %s';
  cErrCannotRecordMenu = 'Cannot record menu (cannot retrieve menu item text): %s';
  cErrCannotFindPopupMenuItem = 'Cannot find menu item: <%s>';
  cErrCannotFindPopupMenu = 'Popup menu expected but nothing found';

var
  GTestingEngine: IisTestingEngine = nil;
  GTestingEngineObj: TisTestingEngine = nil;

function  TestingEngine: IisTestingEngine;
begin
  if not Assigned(GTestingEngine) then
  begin
    GTestingEngineObj := TisTestingEngine.Create;
    GTestingEngine := GTestingEngineObj as IisTestingEngine;
  end;
  Result := GTestingEngine;
end;


function GetMessageProc( Code:integer; wParam: Integer; var Msg: TMsg ): Longint; stdcall;
begin
  Result := 0;
  try
    Result := CallNextHookEx( GTestingEngineObj.FGetMessageHook, Code, wParam, Integer(@Msg) );
    if Code = HC_ACTION then
    begin
      case Msg.message of
        WM_CANCELJOURNAL: GTestingEngineObj.CancelJournal;
        WM_COMMAND: if GTestingEngineObj.State = isTESRecording then
                       GTestingEngineObj.GotCommandMessage( Msg );
        {WM_MENUCOMMAND,WM_SYSCOMMAND}
      end
    end
  except
//    ODS('got exception in GetMessageProc');
  end;
end;

function CallWndProcRetProc(code: Integer; wParam: Integer; var CWPRetStruct: TCWPRetStruct): Longint; stdcall;
begin
  Result := 0;
  try
    if Code < 0 then
      Result := CallNextHookEx( GTestingEngineObj.FCallWndProcRetHook, Code, wParam, Integer(@CWPRetStruct) )
    else
    begin
      GTestingEngineObj.CallWndProcRet( CWPRetStruct );
      Result := CallNextHookEx( GTestingEngineObj.FCallWndProcRetHook, Code, wParam, Integer(@CWPRetStruct) );
    end
  except
  //  ODS('got exception in CallWndProcRetProc');
  end;
end;

function JournalRecordProc(code: Integer; wParam: Integer; var EventStruct: TEventMsg): Longint; stdcall;
begin
  Result := 0;
  try
    if Code < 0 then
      Result := CallNextHookEx( GTestingEngineObj.FJournalRecordHook, Code, wParam, Longint(@EventStruct) )
    else
    begin
      case Code of
        HC_ACTION: GTestingEngineObj.RecordMsg(EventStruct);
        HC_SYSMODALON:
          begin
            ODS('JournalRecordProc: HC_SYSMODALON');
            GTestingEngineObj.Pause;
          end;
        HC_SYSMODALOFF: ODS('JournalRecordProc: HC_SYSMODALOFF');
      end;
    end
  except
//    on E: Exception do ODS('got exception in JournalRecordProc:' + E.Message {+ #13 + GetStack});
  end;
end;

function MsgPlaybackProc(Code:integer; wParam: Integer; var EventStruct: TEVENTMSG): Longint; stdcall;
begin
  Result := 0;
  try
//    ODS( 'MsgPlaybackProc: %d', [Code]);
    if Code < 0 then
      Result := CallNextHookEx( GTestingEngineObj.FJournalPlaybackHook, Code, wParam, Longint(@EventStruct))
    else
    begin
      case Code of
        HC_SKIP,
        HC_GETNEXT: Result := GTestingEngineObj.PlaybackMsg(Code, EventStruct);
        HC_NOREMOVE: ODS('MsgPlaybackProc: HC_NOREMOVE');
        HC_SYSMODALON: // will we ever get these codes?
          begin
            ODS('MsgPlaybackProc: HC_SYSMODALON');
            GTestingEngineObj.Pause;
          end;
        HC_SYSMODALOFF: ODS('MsgPlaybackProc: HC_SYSMODALOFF');
      end;
    end
  except
//    ODS('got exception in MsgPlaybackProc');
  end;
end;

{ TisScriptHolder }

constructor TisTestingEngine.Create;
begin
  FSink := TisTestingEngineSinks.Create;
  FTECP := FSink as IisTestingEngineCP;

  FWaitObjectTimer := TTimer.Create(nil);
  FWaitObjectTimer.Interval := 1;
  FWaitObjectTimer.Enabled := False;
  FWaitObjectTimer.OnTimer := OnWaitObjectEvent;

  //if you are bothered by this magic number then use GetSystemTimeAdjustment to determinate
  //GetTickCount resolution
  FConfigInfo.StepDelay := 55;
  FConfigInfo.RealTyping := True;

  FFreeNotificationListener := TFreeNotificationListener.Create(nil);
  FFreeNotificationListener.OnFreeNotify := HandleFreeNotification;

  FState_ := isTESStoped;
end;

destructor TisTestingEngine.Destroy;
begin
  InternalStop( psFailure ); //may be I should just assert that State = Stopped?
  FreeAndNil( FControlPanel );
  FreeAndNil( FFreeNotificationListener );
  FreeAndNil(FWaitObjectTimer);
  FSink := nil;
  FTECP := nil;
  inherited;
end;

procedure TisTestingEngine.InitAppState;
begin
  FAppState.ForegroundWndHandle := 0;
  SetMouseEventTarget( nil );
  FAppState.LastEventTime := 0;

  FAppState.PopupMenuHandle := 0;
  FAppState.PopupMenuCommandSelected := false;

  FPauseRequested := false;
  FreeAndNil( FCommandExecutor );

  //!!fixme side effect
//  Assert( FScript.Count > 0 );
  FCommandIndex := 0;

  Application.MainForm.SetFocus;
end;

procedure TisTestingEngine.RequireState(aStates: TisTstEngineStates);
  function statesToStr: string;
  var
    s: TisTstEngineState;
  begin
    Result := '';
    for s := low(TisTstEngineState) to high(TisTstEngineState) do
      if s in aStates then
      begin
        if Result <> '' then
          Result := Result + ', ';
        Result := Result + GetEnumName(TypeInfo(TisTstEngineState),integer(s))
      end;
    Result := '[' + Result + ']';
  end;
begin
  if not ( State in aStates ) then
    raise EisException.CreateFmt( 'Internal error in testing engine: expected %s instead of %s ', [statesToStr, GetEnumName(TypeInfo(TisTstEngineState),integer(State))] );
end;

procedure TisTestingEngine.SetState(aState: TisTstEngineState);
begin
  ODS( 'State: %s -> %s', [GetEnumName(typeinfo(TisTstEngineState), integer(State)),
    GetEnumName(typeinfo(TisTstEngineState), integer(aState)) ]);
  FState_ := aState;
//  if State <> isTESIntermediate then
    FControlPanel.ForceUpdate;
  case State of
    isTESPlayback: CreateExecutorOrStartWaiting;
  end;
end;

procedure TisTestingEngine.StartRecording(aScript: IisTEScript2);
begin
  RequireState( [isTESStoped] );
  Assert( CanInterrupt ); //!! for test

  FSink.BeforeStartRecording( aScript );

//  SetState( isTESIntermediate );
  FScript := aScript;
  InitAppState;
  ShowControlPanel;
  SetRecordingHooks;
  SetState( isTESRecording );
end;

procedure TisTestingEngine.InternalStop( status: TisTestScriptStatus);
var
  s: IisTEScript2;
begin
  RequireState( [isTESStoped, isTESRecording, isTESPlayback, isTESPlaybackWaiting, isTESPlaybackPaused, isTESRecordingPaused] );

  FPauseRequested := false;
  s := FScript;

  if State in [isTESRecording, isTESRecordingPaused] then
  begin
//    SetState( isTESIntermediate );
    if State = isTESRecording then
      RemoveRecordingHooks;
    FScript := nil;
    HideControlPanel;
    SetState( isTESStoped );

    FSink.AfterStopRecording( s );
  end
  else if State in [isTESPlayback, isTESPlaybackWaiting, isTESPlaybackPaused] then
  begin
//    SetState( isTESIntermediate );
    if State = isTESPlayback then
      RemovePlaybackHooks;
//    if State = isTESPlaybackWaiting then
//      Win32Check( BlockInput( false ) );
    FWaitObjectTimer.Enabled := False;
    FScript := nil;
    HideControlPanel;
    SetState( isTESStoped );
    FSink.AfterStopPlayback(s, status);
  end
  else if State = isTESStoped then
    ;//nope
end;

procedure TisTestingEngine.ExternalStop;
begin
  InternalStop( psUserStopped );
end;

procedure TisTestingEngine.CancelJournal;
begin
  RequireState( [isTESRecording, isTESPlayback] );
  //system removes journal hooks when user pressed Ctrl-Esc, Ctrl-Alt-Del or maybe does smth else
  if State = isTESRecording then
    FJournalRecordHook := 0
  else if State in [isTESPlayback] then
    FJournalPlaybackHook := 0;
  InternalStop( psUserStopped );
end;

procedure TisTestingEngine.SetPlaybackHooks;
begin
  FGetMessageHook := SetWindowsHookEx(WH_GETMESSAGE, @GetMessageProc, hInstance, GetCurrentThreadId);
  Win32Check( FGetMessageHook <> 0 );
  FCallWndProcRetHook := SetWindowsHookEx(WH_CALLWNDPROCRET, @CallWndProcRetProc, hInstance, GetCurrentThreadId);
  Win32Check( FCallWndProcRetHook <> 0 );

  FJournalPlaybackHook := SetWindowsHookEx(WH_JOURNALPLAYBACK, @MsgPlaybackProc, hInstance, 0);
  Win32Check( FJournalPlaybackHook <> 0 );
end;

procedure TisTestingEngine.RemovePlaybackHooks;
begin
  //FJournalRecordHook may be set to 0 in CancelJournal
  if FJournalPlaybackHook <> 0 then
    Unhook( FJournalPlaybackHook );
  Unhook( FGetMessageHook );
  Unhook( FCallWndProcRetHook );
end;

procedure TisTestingEngine.SetRecordingHooks;
begin
  FGetMessageHook := SetWindowsHookEx(WH_GETMESSAGE, @GetMessageProc, hInstance, GetCurrentThreadId);
  Win32Check( FGetMessageHook <> 0 );
  FCallWndProcRetHook := SetWindowsHookEx(WH_CALLWNDPROCRET, @CallWndProcRetProc, hInstance, GetCurrentThreadId);
  Win32Check( FCallWndProcRetHook <> 0 );

  FJournalRecordHook := SetWindowsHookEx(WH_JOURNALRECORD, @JournalRecordProc, hInstance, 0);
  Win32Check( FJournalRecordHook <> 0 );
end;

procedure TisTestingEngine.RemoveRecordingHooks;
begin
  //FJournalRecordHook may be set to 0 in CancelJournal
  if FJournalRecordHook <> 0 then
    Unhook( FJournalRecordHook );
  Unhook( FGetMessageHook );
  Unhook( FCallWndProcRetHook );
end;

procedure TisTestingEngine.StartPlayback(aScript: IisTEScript2);
begin
  RequireState( [isTESStoped] );
  FSink.BeforeStartPlayback(aScript);
  FScript := aScript;
//  SetState( isTESIntermediate );
  InitAppState;
  ShowControlPanel;
  SetPlaybackHooks;
  SetState( isTESPlayback );
end;

procedure TisTestingEngine.BeginWaiting;
begin
  RequireState( [isTESPlayback] );
//  SetState( isTESIntermediate );
  RemovePlaybackHooks;
  FWaitObjectTimer.Enabled := True;
//  Win32Check( BlockInput( true ) );
  SetState( isTESPlaybackWaiting );
end;

procedure TisTestingEngine.EndWaiting;
begin
  RequireState( [isTESPlaybackWaiting] );
//  SetState( isTESIntermediate );
//  Win32Check( BlockInput( false ) );
  FWaitObjectTimer.Enabled := False;
  SetPlaybackHooks;
  SetState( isTESPlayback );
end;

procedure TisTestingEngine.OnWaitObjectEvent(Sender: TObject);
begin
  try
    RequireState( [isTESPlaybackWaiting] );
    if LookForWaitedObject then
      EndWaiting
    else
      if (GetTickCount - FAppState.LastEventTime) > 1000 then
        raise EisException.CreateFmt(cErrCannotFindObject,
          [FScript.Command[FCommandIndex].ForegroundWindow.ClassName,
           FScript.Command[FCommandIndex].ForegroundWindow.Name]);
  except
    InternalStop( psFailure );
    raise;
  end
end;

procedure TisTestingEngine.Pause;
begin
  RequireState( [isTESRecording, isTESPlayback] );

  FPauseRequested := false;

  if State = isTESRecording then
  begin
//    SetState( isTESIntermediate );
    RemoveRecordingHooks;
    SetState( isTESRecordingPaused );
  end
  else if State = isTESPlayback then
  begin
//    SetState( isTESIntermediate );
    RemovePlaybackHooks;
    FAppState.ForegroundWndHandle := 0;
    SetState( isTESPlaybackPaused );
  end
end;

procedure TisTestingEngine.Resume;
begin
  RequireState( [isTESRecordingPaused, isTESPlaybackPaused] );
  if State = isTESRecordingPaused then
  begin
//    SetState( isTESIntermediate );
    if IsWindow( FAppState.ForegroundWndHandle ) then
      SetForegroundWindow( FAppState.ForegroundWndHandle );
    //  if IsWindow( FAppState.FocusedWindowHandle ) then
    //    SetFocus( FAppState.FocusedWindowHandle );
    SetRecordingHooks;
    SetState( isTESRecording );
  end
  else if State = isTESPlaybackPaused then
  begin
//    SetState( isTESIntermediate );
    SetPlaybackHooks;
    SetState( isTESPlayback );
  end
end;

//KeyboardStateToShiftState
function IsKeyOrMouseDown: boolean;
var
  keys: TKeyboardState;
  i: integer;
begin
  Result := false;
  if not GetKeyboardState( keys ) then
  begin
    Result := true; //for safety -- no action occurs if a key is down
    exit; //!!not so fatal to throw exception -- just give up
  end;
  for i := low(keys) to high(keys) do
    if (keys[i] and byte($80)) <> 0 then
    begin
      Result := true;
      break;
    end;
end;

function IsMouseButtonDown: boolean;
begin
  Result := ((GetKeyState(VK_LBUTTON) and $fffe) <> 0) or ((GetKeyState(VK_RBUTTON) and $fffe) <> 0);
//  ODS('IsMouseButtonDown: %d', [ord(Result)]);
end;

function TisTestingEngine.CanInterrupt: boolean;
var
  gti: TGUIThreadinfo;
begin
  Result := not assigned(FCommandExecutor) and not IsKeyOrMouseDown;
  if Result then
  begin
    gti.cbSize := sizeof( gti );
    GetGUIThreadInfo( GetCurrentThreadId, gti );
    Result := Result and
      ( (gti.flags and (GUI_INMENUMODE or GUI_INMOVESIZE or GUI_POPUPMENUMODE or GUI_SYSTEMMENUMODE)) = 0 ) ;
  end;

(*
  ODS( 'CanInterrupt: ' + BoolToStr(Result) );

    + ' lbd: ' + BoolToStr(FAppState.LeftButtonDown)
    + ' rbd: '+BoolToStr(FAppState.RightButtonDown)
    + ' kd: '+BoolToStr(FAppState.KeyDown)
    + ' gui: '+inttostr( (gti.flags and (GUI_INMENUMODE or GUI_INMOVESIZE or GUI_POPUPMENUMODE or GUI_SYSTEMMENUMODE)) )
  );*)
end;

function FindFormByHandle(AHandle: THandle): TForm;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Screen.FormCount - 1 do
    if Screen.Forms[i].Handle = AHandle then
    begin
      Result := Screen.Forms[i];
      break;
    end;
end;

function FindFormByName(const FormName: String): TForm;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Screen.FormCount - 1 do
    if SameText(Screen.Forms[i].Name, FormName) then
      Result := Screen.Forms[i];
end;

function MakePathName(AControl: TControl): String;
var
  C: TControl;

  function CheckName(Ctrl: TControl): String;
  var
    i: Integer;
  begin
    Result := '';

    if Ctrl.Name = '' then
    begin
      if Assigned(Ctrl.Parent) then
        for i := 0 to Ctrl.Parent.ControlCount - 1 do
          if Ctrl.Parent.Controls[i] = Ctrl then
          begin
            Result := IntToStr(i);
            break;
          end;
    end
    else
      Result := Ctrl.Name;
  end;

begin
  Result := '';
  C := AControl;

  repeat
    if Result <> '' then
      Result := '.' + Result;

    Result := CheckName(C) + Result;
    C := C.Parent;
  until not Assigned(C);
end;

function TisTestingEngine.ScriptObjFromHandle( hForegroundWnd: THandle ): TisTEScriptObjectRec;
var
  C: TControl;
  Buf: Array [0..255] of Char;
begin
  C := FindFormByHandle(HForegroundWnd);
  Result.ThisIsNull := false;
  Result.IsZoomed := IsZoomed(HForegroundWnd);
  Result.IsDelphiObj := Assigned(C);
  if Result.ISDelphiObj then
  begin
    Result.ClassName := C.ClassName;
    Result.Name := MakePathName(C);
  end
  else
  begin
    GetClassName(HForegroundWnd, Buf, Length(Buf));
    Result.ClassName := Buf;
    GetWindowText(HForegroundWnd, Buf, Length(Buf));
    Result.Name := Buf;
  end;
  Result.Annotation := Result.Name;
end;

function TisTestingEngine.ScriptObjFromControl( C: TControl ): TisTEScriptObjectRec;
begin
  Result.ThisIsNull := not assigned(C);
  if not Result.ThisIsNull then
  begin
    Result.IsDelphiObj := true;
    Result.IsZoomed := false;
    Result.ClassName := C.ClassName;
    Result.Name := MakePathName(C);
    Result.Annotation := '';
    if assigned(FAppKnowledge) then
      Result.Annotation := FAppKnowledge.GetAnnotation(C);
    if Result.Annotation <> '' then
      Result.Annotation := '''' + Result.Annotation + ''' - ';
    Result.Annotation := Result.Annotation + Result.Name;
  end
end;

function FindControlByPoint(aPos: TPoint; AParent: TWinControl): TControl;
var
  C: TControl;
begin
  C := AParent.ControlAtPos( AParent.ScreenToClient(aPos), True, True);
  if C is TWinControl then
  begin
    Result := FindControlByPoint(aPos, TWinControl(C));
    if not Assigned(Result) then
      Result := C;
  end
  else
    Result := C;
end;

procedure TisTestingEngine.RecordMsg(const EventStruct: TEventMsg);
  function IsSupportForm( h: HWND): boolean;
  var
    f: TCustomForm;
    pvout: Pointer;
  begin
    f := FindFormByHandle(h);
    Result := Assigned(f) and f.Getinterface( IisTestEngineSupportControl, pvout);
  end;
var
  lPos: TPoint;
  lDelay: Integer;
begin // RecordMsg
  try
    if FAppState.LastEventTime <> 0 then
    begin
      lDelay := int64(EventStruct.Time) - int64(FAppState.LastEventTime);
      if lDelay < 0 then
        lDelay := 0;
    end
    else
      lDelay := 0;

    // filter out idle mouse movements
    if (EventStruct.message = WM_MOUSEMOVE) and CanInterrupt then
    begin
      if lDelay >= 500 then
        FAppState.LastEventTime := EventStruct.Time - 500;
      exit;
    end;

    // record nothing while spying over popup menu
    // the better way is to record but erase on successfull wM_COMMAND detection
    //!! I will implement this with new script file format
    if FAppState.PopupMenuHandle <> 0 then
    begin
      FAppState.LastEventTime := EventStruct.Time;
      exit;
    end;

    FAppState.PopupMenuCommandSelected := false; //look at comment in GotCommandMessage function

    FAppState.LastEventTime := EventStruct.Time;
    FAppState.ForegroundWndHandle := GetForegroundWindow;

    if IsSupportForm( FAppState.ForegroundWndHandle ) then
      Exit;

    // Mouse events
    if (EventStruct.message >= WM_MOUSEFIRST) and (EventStruct.message <= WM_MOUSELAST) then
    begin
      lPos := Point(EventStruct.paramL, EventStruct.paramH);

      //assume that mouse is captured
      //!!todo: try to replace this hack with GetGUIThreadInfo hCapture
      if not IsMouseButtonDown  then //else use old value of FAppState.MouseEventTarget
      begin
        if Assigned(Screen.ActiveForm) and (Screen.ActiveForm.Handle = FAppState.ForegroundWndHandle) then
          SetMouseEventTarget( FindControlByPoint( lpos, Screen.ActiveForm) )
        else
          SetMouseEventTarget( nil );
      end;

      // Convert position into local coordinates
      if Assigned(FAppState.MouseEventTarget) then
        lPos := FAppState.MouseEventTarget.ScreenToClient(lPos)
      else
        ScreenToClient(FAppState.ForegroundWndHandle, lPos);

      FScript.AddMouseCommand(
        lDelay,
        ScriptObjFromHandle( FAppState.ForegroundWndHandle ),
        ScriptObjFromControl( FAppState.MouseEventTarget ),
        EventStruct.message,
        lPos
      );
    end
    else if (EventStruct.message >= WM_KEYFIRST) and (EventStruct.message <= WM_KEYLAST) then // Keyboard events
    begin
      FScript.AddKeyboardCommand(
        lDelay,
        ScriptObjFromHandle( FAppState.ForegroundWndHandle ),
        EventStruct.message,
        EventStruct.paramL,
        EventStruct.paramH
      );
    end
    else
      ODS( 'RecordMsg: Unknown message: ' + inttohex(EventStruct.message, 4) );
  except
    InternalStop( psFailure );
    raise;
  end;
end;

procedure TisTestingEngine.CreateExecutorOrStartWaiting;
var
  c: IisTEScriptCommand;
  ce: TisCommandExecutor;
begin
  Assert(FScript <> nil);
  if FCommandIndex < FScript.Count then
  begin
    if not LookForWaitedObject then
      BeginWaiting
    else
    begin
      c := FScript.Command[FCommandIndex];
      ce := CreateExecutor( c );
      FCommandExecutor := ce;
      c := nil;
    end
  end    
  else
    InternalStop( psSuccess );
end;

function TisTestingEngine.PlaybackMsg(ACode: Integer; var EventStruct: TEventMsg): LongInt;
begin
  RequireState( [isTESPlayback] );
  Result := 0;
  try
    if ACode = HC_SKIP then
    begin
//      ODS( 'Playback: HC_SKIP: ' + GetEnumName( typeinfo(TisTEScriptCmd), integer(FScript.Cmd.Cmd) ) );
      if not assigned(FCommandExecutor) or not FCommandExecutor.Next then
      begin
        FreeAndNil( FCommandExecutor );
        inc( FCommandIndex );
        if FCommandIndex > FScript.Count-1 then
          InternalStop( psSuccess )
        else
        begin
          if ((GetAsyncKeyState( VK_ESCAPE ) <> 0) or FPauseRequested) then
          begin
            FPauseRequested := true;
            if CanInterrupt then
              Pause
            else
              CreateExecutorOrStartWaiting;
          end
          else
            CreateExecutorOrStartWaiting;
        end;
      end;
    end
    else if ACode = HC_GETNEXT then
    begin
      Result := FCommandExecutor.FillEventMsg( EventStruct );
      if Result = 0 then
        FAppState.LastEventTime := GetTickCount;
    end;
  except
    on E: Exception do
    begin
      try
        InternalStop( psFailure );
      except
      end;  
      MessageDlg(E.Message, mtError,[mbOK], 0);
    end;
  end;
end;

function GetMenuItemCaption( menu: HMENU; IdItem: integer; fk: integer ): string;
var
  buf: array[0..255] of char; //!!fixme
  i: integer;
  mi: TMenuItem;
  pm: TPopupMenu;
  hm: HMENU;
begin
  if GetMenuString( Menu, IDItem, buf, sizeof(buf),fk) <> 0 then
  begin
    Result := buf;
    Exit;
  end;

  //it may be Delphi's owner drawn menu item
  mi := nil;
  pm := nil; //to make compiler happy
  if fk = MF_BYPOSITION then
  begin
    hm := GetSubMenu( menu, idItem); //look for menu
    if hm = 0 then
    begin
      Result := GetMenuItemCaption( menu, GetMenuItemID( menu, idItem ), MF_BYCOMMAND );
      Exit;
    end
  end
  else
    hm := menu;
  for i := 0 to PopupList.Count - 1 do
  begin
    pm := TPopupMenu(PopupList.Items[I]);
    if pm.Items.Handle = hm then
      mi := pm.Items
    else
      mi := pm.FindItem( hm, fkHandle );
    if mi <> nil then
      break;
  end;
  
  if assigned(mi) then
    if fk <> MF_BYPOSITION then
      mi := pm.FindItem( idItem, fkCommand); //assume that in this menu all ids are unique
  if assigned(mi) then
  begin
    Result := mi.Caption;
    ODS( 'GetMenuString succeeded: '+Result);
  end
  else
  begin
    Result := '';
    ODS( 'GetMenuString failed.');
  end
end;

procedure TisTestingEngine.CallWndProcRet(const CWPRetStruct: TCWPRetStruct);
var
  IDItem: Word;
  MenuFlag: Word;
  Menu: HMENU;
//  mii: TMenuItemInfo;
  fk: integer;
begin
  RequireState( [isTESRecording, isTESPlayback] );
  case CWPRetStruct.message of
    WM_MENUSELECT:
      if State = isTESRecording then
      begin
        if FAppState.PopupMenuHandle <> 0 then
        begin
          MenuFlag := HiWord(DWORD(CWPRetStruct.wParam));
          IDItem := LoWord(DWORD(CWPRetStruct.wParam));
          Menu := CWPRetStruct.lParam;
          if (MenuFlag = $ffff) and (Menu = 0) then
            ODS( 'WM_MENUSELECT: menu closed')
          else
          begin
            Assert( Menu <> 0 );
            if (MenuFlag and MF_POPUP) <> 0 then
              fk := MF_BYPOSITION
            else
            begin
              fk := MF_BYCOMMAND;
              FAppState.PopupMenuCmdId := IDItem;
            end;
  //          ODS( 'WM_MENUSELECT: %d %d %d',[ integer(IDItem), integer(MenuFlag), Menu ]);
  //          ODS( 'WM_MENUSELECT: ISMenu: '+ inttostr(integer(IsMenu(Menu))));
{
            mii.cbSize := sizeof(mii);
            mii.fMask := MIIM_FTYPE;
            if GetMenuItemInfo( Menu, IDItem, fk = MF_BYPOSITION, mii )  then
              ODS( 'WM_MENUSELECT: fType: %d', [mii.fType] )
            else
              ODS( 'WM_MENUSELECT: GetMenuItemInfo: '+ SysErrorMessage(GetLastError));
 }
            FAppState.PopupMenuCommandSelected := (MenuFlag and MF_POPUP) = 0;
            SetLength( FAppState.PopupMenuStack, FAppState.PopupMenuNesting );
            FAppState.PopupMenuStack[high(FAppState.PopupMenuStack)] := StripHotkey( GetMenuItemCaption( Menu, IDItem, fk ) );
            //!!we should also store mi position to handle WM_MENUCOMMAND but give up for now (VCL doesn't use it)
            //!! handle empty menu name later in GotCommandMessage
          end
        end;
      end;
    WM_INITMENUPOPUP:
      begin
        ODS('WM_INITMENUPOPUP');
        if FAppState.PopupMenuHandle = 0 then
        begin
//          Assert( FAppState.PopupMenuNesting = 0 );
          (* --gdy--
            Maybe I can still do this in WM_ENTERMENULOOP and check for GUI_SYSTEMMENUMODE flag
          *)
          if hiword(CWPRetStruct.lParam) = 0 then //not system menu
          begin
            FAppState.PopupMenuStack := nil; //actually don't need this
            FAppState.PopupMenuNesting := 1;
            FAppState.PopupMenuHandle := HMENU(CWPRetStruct.wParam);
            FAppState.PopupMenuCommandSelected := false;
          end
        end
        else
        begin
          inc(FAppState.PopupMenuNesting);
          FAppState.PopupMenuHandle := HMENU(CWPRetStruct.wParam);
        end;
        ODS('WM_INITMENUPOPUP: PopupMenuNesting %d, PopupMenuHandle %d', [FAppState.PopupMenuNesting, FAppState.PopupMenuHandle]);
      end;
    WM_UNINITMENUPOPUP:
      begin
        ODS('WM_UNINITMENUPOPUP');
        if FAppState.PopupMenuHandle <> 0 then
        begin
          dec(FAppState.PopupMenuNesting);
          if FAppState.PopupMenuNesting = 0 then
          begin
            FAppState.PopupMenuHandle := 0;
            ODS('uninit menu: ' + SetToStr(FAppState.PopupMenuStack, '|') + ': CommandSelected='+BoolToStr(FAppState.PopupMenuCommandSelected) );
          end
        end
      end;
  end;
end;

procedure TisTestingEngine.GotCommandMessage(const Msg: TMsg);
var
  i: integer;
begin
  try
    RequireState( [isTESRecording] );
    (* --gdy--
      We need this to sort out cancelled menu interactions
      The only way to do it (AFAIK, please let me know if I'm wrong here) is to check if we get
      WM_COMMAND (also WM_MENUCOMMAND) message posted by windows during menu cleanup
      Posted messages have highest priority in message queue so we will get here before
      any input message will be passed to WH_JOURNALPLAYBACK hook.
      Reference:
        Jeffrey Richter
        Programming Applications for Microsoft Windows
        (The Algorithm for Extracting Messages from a Thread's Queue)
    *)
    Assert( FAppState.PopupMenuHandle = 0 );
    if FAppState.PopupMenuCommandSelected then
    begin
      FAppState.PopupMenuCommandSelected := false;
      if (Msg.message = WM_COMMAND) and (loword(Msg.wParam) = FAppState.PopupMenuCmdId)
        and (hiword(Msg.wparam) = 0{menu}) then
      begin
        for i := low(FAppState.PopupMenuStack) to high(FAppState.PopupMenuStack) do
          if FAppState.PopupMenuStack[i] = '' then //!!we failed to get text (we may confuse this with empty string as menu caption, but don't care for now)
            raise EisException.CreateFmt( cErrCannotRecordMenu, [SetToStr(FAppState.PopupMenuStack, '|')] );
        FScript.AddPopupMenuCommand(
          ScriptObjFromHandle( FAppState.ForegroundWndHandle ),
          FAppState.PopupMenuStack
        );
      end
    end
  except
    on e: exception do
    begin
      MessageDlg(E.Message, mtError,[mbOK], 0);
      raise;
    end  
  end;
end;

procedure TisTestingEngine.HideControlPanel;
begin
  FControlPanel.Hide;
end;

procedure TisTestingEngine.ShowControlPanel;
begin
  if not Assigned(FControlPanel) then
    FControlPanel := TisTestControlPanel.Create(nil);
  FControlPanel.Show;
end;

function TisTestingEngine.GetState: TisTstEngineState;
begin
  Result := FState_;
end;

function TisTestingEngine.GetConfig: TisTEConfigRec;
begin
  Result := FConfigInfo;
end;

procedure TisTestingEngine.SetConfig(const AConfigInfo: TisTEConfigRec);
begin
  FConfigInfo := AConfigInfo;
end;

function TisTestingEngine.LookForWaitedObject: Boolean;
var
  Obj: TisTEScriptObjectRec;
  C: TControl;
  Buf1, Buf2: array [0..255] of Char;
begin
  FAppState.ForegroundWndHandle := 0;
  Obj := FScript.Command[FCommandIndex].ForegroundWindow;

  if Obj.IsDelphiObj then
  begin
    // Delphi's Form
    C := FindFormByName(Obj.Name);
    if Assigned(C) and SameText(C.ClassName, Obj.ClassName) then
      FAppState.ForegroundWndHandle := TForm(C).Handle;
  end
  else
  begin
    // System Window
    StrPCopy(Buf1, Obj.ClassName);
    StrPCopy(Buf2, Obj.Name);
    FAppState.ForegroundWndHandle := FindWindow(Buf1, Buf2);
  end;

  Result := FAppState.ForegroundWndHandle <> 0;
  if Result then //!! assume we will succeed
  begin
    if Obj.IsZoomed then
      ShowWindow( FAppState.ForegroundWndHandle, SW_MAXIMIZE )
    else
      ShowWindow( FAppState.ForegroundWndHandle, SW_RESTORE );
    SetForegroundWindow( FAppState.ForegroundWndHandle );
  end;
end;

procedure TisTestingEngine.Unhook(var h: HHOOK);
begin
  Assert( h <> 0 );
  if h <> 0 then
  begin
    Win32Check( UnhookWindowsHookEx(h) );
    h := 0;
  end;
end;

type
  PisTEConfigRec = ^TisTEConfigRec;
  
  TisSingleMsgCommandExecutor = class (TisCommandExecutor)
  public
    function Next: boolean; override;
  end;

  TisMouseCommandExecutor = class (TisSingleMsgCommandExecutor)
  public
    constructor Create( appState: PisTEAppStateRec; aScheduledTime: DWORD; cmd: IisTEScriptMouseCommand );
    function FillEventMsg( var EventStruct: TEventMsg ): longint; override;
    function GetMouseTarget: TControl;
  private
    FCmd: IisTEScriptMouseCommand;
  end;

  TisKeyboardCommandExecutor = class (TisSingleMsgCommandExecutor)
  public
    constructor Create( appState: PisTEAppStateRec; aScheduledTime: DWORD; cmd: IisTEScriptKeyCommand );
    function FillEventMsg( var EventStruct: TEventMsg ): longint; override;
  private
    FCmd: IisTEScriptKeyCommand;
  end;

  TisPopupMenuCommandExecutor = class (TisCommandExecutor)
  public
    constructor Create( appState: PisTEAppStateRec; aConfigInfo: PisTEConfigRec; aScheduledTime: DWORD; cmd: IisTEScriptPopupMenuCommand );
    function Next: boolean; override;
    function FillEventMsg( var EventStruct: TEventMsg ): longint; override;
  private
    FConfigInfo: PisTEConfigRec;
    FMenuPath: TDynStringArray;
    FRequiredNesting: integer;
    FKeyboardMsgCount: integer;
    FKeyboardMsgPostedCount: integer;
    procedure Check;
  end;

function TisTestingEngine.CreateExecutor( cmd: IisTEScriptCommand): TisCommandExecutor;
var
  sched: DWORD;
begin
  ODS('CreateExecutor: %s delay: %d', [GetEnumName(typeinfo(TisTEScriptCommandType), ord(cmd.CommandType)), cmd.Delay] );
  if FAppState.LastEventTime = 0 then
    FAppState.LastEventTime := GetTickCount;
  sched := FAppState.LastEventTime + cmd.Delay;

  case cmd.CommandType of
    teSCMouse:
      Result := TisMouseCommandExecutor.Create( @FAppState, sched, cmd as IisTEScriptMouseCommand );
    teSCKey:
      begin
        if not FConfigInfo.RealTyping then
          sched := FAppState.LastEventTime + FConfigInfo.StepDelay;
        Result := TisKeyboardCommandExecutor.Create( @FAppState, sched, cmd as IisTEScriptKeyCommand );
      end;
    teSCPopupMenu:
      Result := TisPopupMenuCommandExecutor.Create( @FAppState, @FConfigInfo, sched, cmd as IisTEScriptPopupMenuCommand );
    else
      raise EisException.Createfmt( 'unknown command type: %s',[GetEnumName(typeinfo(TisTEScriptCommandType), ord(cmd.CommandType))] );
  end;
end;

{ TisCommandExecutor }

function TisCommandExecutor.CalcDelay: longint;
begin
  Result := int64(FScheduledTime) - int64(GetTickCount);
  if Result < 0 then //we are late
    Result := 0;
end;

procedure TisCommandExecutor.CheckCurrentWindow;
var
  s: String;
  WH: THandle;
  Buf1: array [0..255] of Char;
begin
  WH := GetForegroundWindow;
  if IsWindow(FAppState.ForegroundWndHandle) and (FAppState.ForegroundWndHandle <> WH) then
  begin
    GetClassName(WH, Buf1, Length(Buf1));
    s := Buf1;
    GetWindowText(WH, Buf1, Length(Buf1));
    s := s + '(' + Buf1 + ')';
    raise EisException.CreateFmt(cErrUnexpectedWindow, [s]);
  end;
end;

constructor TisCommandExecutor.Create(appState: PisTEAppStateRec;
  aScheduledTime: DWORD);
begin
  FAppState := appState;
  FScheduledTime := aScheduledTime;
end;

{ TisSingleMsgCommandExecutor }

function TisSingleMsgCommandExecutor.Next: boolean;
begin
  Result := false;
end;

{ TisKeyboardCommandExecutor }

constructor TisKeyboardCommandExecutor.Create(appState: PisTEAppStateRec; aScheduledTime: DWORD; cmd: IisTEScriptKeyCommand );
begin
  inherited Create( appState, aScheduledTime);
  FCmd := cmd;
end;

function TisKeyboardCommandExecutor.FillEventMsg( var EventStruct: TEventMsg): longint;
begin
  CheckCurrentWindow;
  EventStruct.message := FCmd.Msg;
  EventStruct.ParamL := FCmd.VKey;
  EventStruct.ParamH := FCmd.ScanCode;
  Result := CalcDelay;
end;

{ TisMouseCommandExecutor }

constructor TisMouseCommandExecutor.Create(appState: PisTEAppStateRec; aScheduledTime: DWORD; cmd: IisTEScriptMouseCommand);
begin
  inherited Create( appState, aScheduledTime);
  FCmd := cmd;
end;

function TisMouseCommandExecutor.GetMouseTarget: TControl;
  function FindControl(ControlName: String; ControlClassName: string): TControl;
  var
    h: String;
    W: TWinControl;
    i: Integer;
  begin
    h := GetNextStrValue(ControlName, '.');
    Result := FindFormByName(h);
    if Assigned(Result) then
      while ControlName <> '' do
      begin
        h := GetNextStrValue(ControlName, '.');
        W := TWinControl(Result);
        Result := nil;

        if (h[1] >= '0') and (h[1] <= '9') then
        begin
          i := StrToInt(h);
          if W.ControlCount > i then
            Result := W.Controls[i];
        end
        else
          for i := 0 to W.ControlCount - 1 do
            if SameText(W.Controls[i].Name, h) then
            begin
              Result := W.Controls[i];
              break;
            end;

        if not Assigned(Result) then
          break;
      end;
    if Result <> nil then
      if not SameText(Result.ClassName, ControlClassName ) then
        Result := nil;
  end;
var
  t: TisTEScriptObjectRec;
begin
  Result := nil;
  t := FCmd.Target;
  if not t.ThisIsNull then
  begin
    Assert( t.IsDelphiObj );
    Result := FindControl( t.Name, t.ClassName );
  end
end;

function TisMouseCommandExecutor.FillEventMsg(var EventStruct: TEventMsg): longint;
var
  p: TPoint;
  C: TControl;
begin
  CheckCurrentWindow;
  C := GetMouseTarget;
  p := FCmd.Point;
  if Assigned(C) then
    p := C.ClientToScreen(p)
  else
    ClientToScreen(FAppState.ForegroundWndHandle, p);

  EventStruct.message := FCmd.Msg;
  EventStruct.ParamL := p.X;
  EventStruct.ParamH := p.Y;
  Result := CalcDelay;
end;

{ TisPopupMenuCommandExecutor }

procedure TisPopupMenuCommandExecutor.Check;
var
  bLastKeyup: boolean;
begin
  bLastKeyup := (FRequiredNesting = Length(FMenuPath)) and (FKeyboardMsgPostedCount = FKeyboardMsgCount-1);
  if not bLastKeyup then
    CheckCurrentWindow;
  //assert that we have menu handle or it is last keyup nobody cares about
  if (FAppState.PopupMenuHandle = 0) and
    not (bLastKeyup) then
    raise EisException.CreateFmt( cErrCannotFindPopupMenu, [] );
end;

constructor TisPopupMenuCommandExecutor.Create(appState: PisTEAppStateRec; aConfigInfo: PisTEConfigRec; aScheduledTime: DWORD; cmd: IisTEScriptPopupMenuCommand );
begin
  inherited Create( appState, aScheduledTime );
  FConfigInfo := aConfigInfo;
  FMenuPath := cmd.MenuPath;
  FRequiredNesting := 1;
  FKeyboardMsgCount := 0; //means that we have to calculate
  FKeyboardMsgPostedCount := 0;
end;

function TisPopupMenuCommandExecutor.FillEventMsg(var EventStruct: TEventMsg): longint;
var
  nItemCount: integer;
  i: integer;
  pos: integer;
begin
  Check;
//  ODS('FRequiredNesting %d, PopupMenuHandle %d',[FRequiredNesting, FAppState.PopupMenuHandle]);

  if FKeyboardMsgCount = 0 then
  begin
    Assert( FRequiredNesting = FAppState.PopupMenuNesting );
    Assert( FAppState.PopupMenuHandle <> 0 );
    nItemCount := GetMenuItemCount( FAppState.PopupMenuHandle );
    Win32Check( nItemCount >= 0 );
//    ODS( 'nItemCount: %d', [nItemCount]);
    pos := -1;
    for i := 0 to nItemCount-1 do
    begin
      ODS(StripHotkey( GetMenuItemCaption( FAppState.PopupMenuHandle, i, MF_BYPOSITION ) ));
      if StripHotkey( GetMenuItemCaption( FAppState.PopupMenuHandle, i, MF_BYPOSITION ) ) = FMenuPath[FRequiredNesting-1] then
      begin
        pos := i;
        break;
      end;
    end;
    if pos = -1 then
      raise EisException.CreateFmt( cErrCannotFindPopupMenuItem, [FMenuPath[FRequiredNesting-1]] );
    FKeyboardMsgPostedCount := 0;
    FKeyboardMsgCount := (pos+1+ord(FRequiredNesting=1))*2;
    // if popup menu is just shown than we need to simulate VK_DOWN (for hilight to appear)
    // on the contrary submenus already has first item highlighted when opened
    // this gives us one extra keystroke for FRequiredNesting = 1
    // next we should simulate pos VK_DOWNs (pos is zero based) to select needed menu item
    // and finally we should send VK_RIGHT to open submenu or VK_RETURN to execure command
  end;

  // here we decode FKeyboardMsgPostedCount...
  if FKeyboardMsgPostedCount < FKeyboardMsgCount-2 then
    EventStruct.paramL := VK_DOWN
  else
    if FRequiredNesting = Length(FMenuPath) then
      EventStruct.paramL := VK_RETURN
    else
      EventStruct.paramL := VK_RIGHT;
  EventStruct.paramH := VkKeyScan( char(EventStruct.paramL) );
  if Odd(FKeyboardMsgPostedCount) then
    EventStruct.message := WM_KEYUP
  else
    EventStruct.message := WM_KEYDOWN;
//  ODS( 'FKeyboardMsgCount: %d FKeyboardMsgPostedCount: %d; vk: %d msg: %d', [FKeyboardMsgCount, FKeyboardMsgPostedCount, EventStruct.paramL,EventStruct.message]);

  Result := CalcDelay;
end;

function TisPopupMenuCommandExecutor.Next: boolean;
begin
  inc(FKeyboardMsgPostedCount);
  if FKeyboardMsgPostedCount = FKeyboardMsgCount then //finished with this level of menu
  begin
    if FRequiredNesting < Length(FMenuPath) then //lets go to submenu
    begin
      inc(FRequiredNesting);
      FKeyboardMsgCount := 0; //to calculate
      FKeyboardMsgPostedCount := 0;
      Result := true;
    end
    else
      Result := false; // Done!
  end
  else
    Result := true; //keep sending keys
  FScheduledTime := GetTickCount + FConfigInfo.StepDelay;
end;

{ TisTestingEngineSinks }

procedure TisTestingEngineSinks.Advise(const aSink: IisTECallBackHolder);
begin
  Assert( aSink <> nil );
  if FList.IndexOf(aSink) < 0 then
    FList.Add(aSink as IUnknown);
end;

procedure TisTestingEngineSinks.AfterStopPlayback(aScript: IisTEScript2;
  status: TisTestScriptStatus);
var
  i: integer;
begin
  for i := 0 to FList.Count-1 do
    (FList[i] as IisTECallBackHolder).AfterStopPlayback( aScript, status );
end;

procedure TisTestingEngineSinks.AfterStopRecording(aScript: IisTEScript2);
var
  i: integer;
begin
  for i := 0 to FList.Count-1 do
    (FList[i] as IisTECallBackHolder).AfterStopRecording( aScript );
end;

procedure TisTestingEngineSinks.BeforeStartPlayback(aScript: IisTEScript2);
var
  i: integer;
begin
  for i := 0 to FList.Count-1 do
    (FList[i] as IisTECallBackHolder).BeforeStartPlayback( aScript );
end;

procedure TisTestingEngineSinks.BeforeStartRecording(aScript: IisTEScript2);
var
  i: integer;
begin
  for i := 0 to FList.Count-1 do
    (FList[i] as IisTECallBackHolder).BeforeStartRecording( aScript );
end;

constructor TisTestingEngineSinks.Create;
begin
  inherited;
  FList := TInterfaceList.Create;
end;

destructor TisTestingEngineSinks.Destroy;
begin
//  Assert( not assigned(FList) or (FList.Count = 0) );

  inherited;
  FreeAndNil( FList );
end;

procedure TisTestingEngineSinks.UnAdvise(const aSink: IisTECallBackHolder);
begin
  Assert( aSink <> nil );
  FList.Remove( aSink as IUnknown );
end;

procedure TisTestingEngine.SetApplicationKnowledgeSource(
  ks: IisApplicationKnowledgeSource);
begin
  FAppKnowledge := ks;
end;

{ TFreeNotificationListener }

procedure TFreeNotificationListener.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  if (Operation = opRemove) and Assigned(FOnFreeNotify) then
    FOnFreeNotify(Self, AComponent);
  inherited;
end;

procedure TisTestingEngine.HandleFreeNotification(Sender: TObject;
  AComponent: TComponent);
begin
  if FAppState.MouseEventTarget = AComponent then
    FAppState.MouseEventTarget := nil;
end;

procedure TisTestingEngine.SetMouseEventTarget(met: TControl);
begin
  if assigned(FAppState.MouseEventTarget) then
    FAppState.MouseEventTarget.RemoveFreeNotification( FFreeNotificationListener );

  FAppState.MouseEventTarget := met;

  if assigned(FAppState.MouseEventTarget) then
    FAppState.MouseEventTarget.FreeNotification( FFreeNotificationListener );
end;

initialization
  GTestingEngineObj := nil;
  GTestingEngine := nil;

finalization
  GTestingEngineObj := nil;
  GTestingEngine := nil;

end.

