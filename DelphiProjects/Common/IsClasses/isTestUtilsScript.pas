unit isTestUtilsScript;

interface

uses
  classes, windows, controls, isTestUtilsScriptWrapper, isExceptions;

type
  TisTEScriptObjectRec = record
    ThisIsNull: boolean; //if true then ignore other fields
    IsDelphiObj:  Boolean;
    Name:       String;
    ClassName:  String;
    IsZoomed:   boolean;
    Annotation: string;
  end;

  TisTEScriptCommandType = (teSCMouse, teSCKey, teSCPopupMenu );

  IisTEScriptCommand = interface
    ['{AE30CB01-88FD-4B32-A393-B69D48218E2F}']
    function Delay: DWORD;
    function ForegroundWindow: TisTEScriptObjectRec;
    function CommandType: TisTEScriptCommandType;
  end;

  IisTEScriptMouseCommand = interface
    ['{90BF2275-DEE8-4407-833F-5BC53DE15285}']
    function GetMsg: DWORD;
    function GetPoint: TPoint;
    function GetTarget: TisTEScriptObjectRec;
    property Point: TPoint read GetPoint;
    property Msg: DWORD read GetMsg;
    property Target: TisTEScriptObjectRec read GetTarget;
  end;

  IisTEScriptKeyCommand = interface
    ['{AF8B269B-D88F-47CB-8A8B-A231F005EB1D}']
    function GetVKey: UINT;
    function GetScanCode: UINT;
    function GetMsg: DWORD;
    property VKey: UINT read GetVKey;
    property ScanCode: UINT read GetScanCode;
    property Msg: DWORD read GetMsg;
  end;

  TDynStringArray = array of string;

  IisTEScriptPopupMenuCommand = interface
    ['{2399EC03-7866-4317-99BD-608ECBD8D7DF}']
    function GetMenuPath: TDynStringArray;
    property MenuPath: TDynStringArray read GetMenuPath;
  end;

  IIsTEScriptEventSink = interface
  ['{EA0135F5-A121-418A-9B1A-A9EA57D75143}']
    procedure CommandDeleted( idx: integer );
    procedure CommandAdded( idx: integer );
  end;

  IisTEScript2 = interface
    ['{1693201E-F3D3-4AA2-AA33-F032D7BCC7C7}']
    procedure AddMouseCommand( delay: DWORD; const fore: TisTEScriptObjectRec; const target: TisTEScriptObjectRec; msg: DWORD; pos: TPoint );
    procedure AddKeyboardCommand( delay: DWORD; fore: TisTEScriptObjectRec; msg: DWORD; vk, scan: UINT );
    procedure AddPopupMenuCommand( fore: TisTEScriptObjectRec; menupath: TDynStringArray );
    procedure ClearCommands;
    function GetCount: integer;
    function GetCommand(i: integer): IisTEScriptCommand;
    procedure Save;
    procedure SaveAs( filename: string );
    function AsText: string;
    function GetFilename: string;
    function  GetComments: String;
    procedure SetComments(const AComments: String);
    function IsModified: boolean;
    procedure DeleteToEnd(i: integer);
    procedure Advise( sink: IIsTEScriptEventSink ); //!!one slot; call Advise(nil) to unadvise

    property Count: integer read GetCount;
    property Command[i: integer ]: IisTEScriptCommand read GetCommand;
    property Comments: string read GetComments write SetComments;
    property Filename: string read GetFilename;
  end;

function TestScriptFromFile(aFilename: string): IisTEScript2;

function GetNextStrValue(var AStr: string; const ADelim: String): String;
function SetToStr( const ss: array of string; const separator: string ): string;
function NameFromMsg( msg: DWORD): string;

implementation

uses
  sysutils, forms, messages, math, menus, variants, istestutils,
  xmldom, {oxmldom, }xercesxmldom;

type
  IisTEScriptPrivate = interface //(IisTEScript2)
    ['{CFFDFBBE-13A5-43FE-B0E8-D1A9AB6648B4}']
    function FindObjectById( id: integer ): TisTEScriptObjectRec;
  end;

  TisTEScriptXML = class(TInterfacedObject, IisTEScript2, IisTEScriptPrivate)
  private
    FFilename: string;
    FModified: boolean;
    FRoot: IXMLTecontainerType;
    FScript: IXMLScriptType;
    FSink: IIsTEScriptEventSink;
    procedure CommandAdded;
  private
    function FindOrAddObject( const fore: TisTEScriptObjectRec ): integer; //returns Id
    procedure PurgeUnusedObjects;
    function  AddCommand(delay: DWORD; const fore: TisTEScriptObjectRec): IXMLCommandType;

    {IisTEScript2}
    procedure AddMouseCommand( delay: DWORD; const fore: TisTEScriptObjectRec; const target: TisTEScriptObjectRec; msg: DWORD; pos: TPoint );
    procedure AddKeyboardCommand( delay: DWORD; fore: TisTEScriptObjectRec; msg: DWORD; vk, scan: UINT );
    procedure AddPopupMenuCommand( fore: TisTEScriptObjectRec; menupath: TDynStringArray );
    procedure ClearCommands;
    function GetCommand(i: integer): IisTEScriptCommand;
    function GetCount: integer;
    procedure Save;
    procedure SaveAs( afilename: string );
    function AsText: string;
    function  GetComments: String;
    procedure SetComments(const AComments: String);
    function GetFilename: string;
    function IsModified: boolean;
    procedure DeleteToEnd(i: integer);
    procedure Advise( sink: IIsTEScriptEventSink );

    property Count: integer read GetCount;

    {IisTEScriptPrivate}
    function FindObjectById( id: integer ): TisTEScriptObjectRec;

  public
    constructor Create(const AFileName: String);
    destructor  Destroy; override;
  end;

  TisTEScriptCommand = class(TInterfacedObject, IisTEScriptCommand, IisTEScriptMouseCommand, IisTEScriptKeyCommand, IisTEScriptPopupMenuCommand)
  private
    FScript: IisTEScriptPrivate;
    FCommand: IXMLCommandType;
    FCommandType: TisTEScriptCommandType;
    FForegroundWindowObj: TisTEScriptObjectRec;
  private
    {IisTEScriptCommand}
    function Delay: DWORD;
    function ForegroundWindow: TisTEScriptObjectRec;
    function CommandType: TisTEScriptCommandType;
    {IisTEScriptMouseCommand}
    function Mouse_GetMsg: DWORD;
    function IisTEScriptMouseCommand.GetMsg = Mouse_GetMsg;
    function GetPoint: TPoint;
    function GetTarget: TisTEScriptObjectRec;
    {IisTEScriptKeyCommand}
    function Key_GetMsg: DWORD;
    function IisTEScriptKeyCommand.GetMsg = Key_GetMsg;
    function GetVKey: UINT;
    function GetScanCode: UINT;
    {IisTEScriptPopupMenuCommand}
    function GetMenuPath: TDynStringArray;
  public
    constructor Create( aScript: IisTEScriptPrivate; aCommand: IXMLCommandType);
  end;

function TestScriptFromFile( aFilename: string ): IisTEScript2;
begin
  Result := TisTEScriptXML.Create( aFilename );
end;

{ TisTEScriptXML }

function TisTEScriptXML.AddCommand(delay: DWORD;
  const fore: TisTEScriptObjectRec): IXMLCommandType;
var
  id: integer;
begin
  FModified := true;
  id := FindOrAddObject( fore );
  Result := FScript.Commands.Add;
  Result.Delay := delay;
  Result.window_id := id;
end;

const
  map: array [0..9] of record a: string; m: DWORD; u: string end =
  (
    (a: 'WM_LBUTTONDOWN'; m: WM_LBUTTONDOWN; u: 'Left mouse button down'),
    (a: 'WM_LBUTTONUP'; m: WM_LBUTTONUP; u: 'Left mouse button up'),
    (a: 'WM_RBUTTONDOWN'; m: WM_RBUTTONDOWN; u: 'Right mouse button down'),
    (a: 'WM_RBUTTONUP'; m: WM_RBUTTONUP; u: 'Right mouse button up'),
    (a: 'WM_MOUSEWHEEL'; m: WM_MOUSEWHEEL; u: '[Mouse wheel]'),
    (a: 'WM_MOUSEMOVE'; m: WM_MOUSEMOVE; u: 'Mouse moved'),

    (a: 'WM_KEYDOWN'; m: WM_KEYDOWN; u: 'Key down'),
    (a: 'WM_KEYUP'; m: WM_KEYUP; u: 'Key up'),
    (a: 'WM_SYSKEYDOWN'; m: WM_SYSKEYDOWN; u: 'System key down'),
    (a: 'WM_SYSKEYUP'; m: WM_SYSKEYUP; u: 'System key up')
  );

function ActionFromMsg( msg: DWORD): string;
var
  i: integer;
begin
  for i := low(map) to high(map) do
    if map[i].m = msg then
    begin
      Result := map[i].a;
      exit;
    end;
  raise EisException.CreateFmt( 'Unknown message: %s', [inttohex(msg,4)]);
end;

function MsgFromAction( act: string): DWORD;
var
  i: integer;
begin
  for i := low(map) to high(map) do
    if SameText(map[i].a, act) then
    begin
      Result := map[i].m;
      exit;
    end;
  raise EisException.CreateFmt( 'Unknown action: %', [act]);
end;

function NameFromMsg( msg: DWORD): string;
var
  i: integer;
begin
  for i := low(map) to high(map) do
    if map[i].m = msg then
    begin
      Result := map[i].u;
      exit;
    end;
  raise EisException.CreateFmt( 'Unknown message: %s', [inttohex(msg,4)]);
end;

procedure TisTEScriptXML.AddKeyboardCommand(delay: DWORD;
  fore: TisTEScriptObjectRec; msg: DWORD; vk, scan: UINT);
var
  c: IXMLCommandType;
  k: IXMLKeyType;
begin
  c := AddCommand( delay, fore );
  k := c.Key;
  k.Action := ActionFromMsg( msg );
  ODS( 'AddKeyboardCommand: ' + k.Action);
  k.Vk := vk;
  k.Scan := scan;
  //quick trick
//  if k.Vk and $ff00 = 0 then
    c.Comment := '[Test] Maybe it is ' + ShortCutToText(TShortCut(k.Vk));
  CommandAdded;
end;

procedure TisTEScriptXML.AddMouseCommand(delay: DWORD; const fore: TisTEScriptObjectRec; const target: TisTEScriptObjectRec; msg: DWORD; pos: TPoint);
var
  c: IXMLCommandType;
  m: IXMLMouseType;
begin
  c := AddCommand( delay, fore );
  m := c.Mouse;
  m.Action := ActionFromMsg( msg );
  m.X := pos.X;
  m.Y := pos.y;
  if not target.ThisIsNull then
  begin
    m.Target_id := FindOrAddObject( target );
  end
  else
  begin
//    m.Target_id := c.Window_id;
  end;
  ODS( 'AddMouseCommand: ' + m.Action);
  CommandAdded;
end;

//from gdystrset
function SetToStr( const ss: array of string; const separator: string ): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to Length(ss)-1 do
  begin
    if Result <> '' then
      Result := Result + separator;
    Result := Result + ss[i];
  end;
end;

procedure TisTEScriptXML.AddPopupMenuCommand(fore: TisTEScriptObjectRec;
  menupath: TDynStringArray);
var
  c: IXMLCommandType;
  pm: IXMLPopupmenuType;
begin
  c := AddCommand( 0, fore );
  pm := c.Popupmenu;
  pm.Path := SetToStr(menupath, '|');
  CommandAdded;
end;

constructor TisTEScriptXML.Create(const AFileName: String);
begin
  FFilename := AFileName;
  FModified := false;
  if FileExists(aFileName) then
  begin
    try
      FRoot := Loadtecontainer( AFileName );
    except
      on E: Exception do
        raise EisException.CreateFmt( 'Failed to load Test Script <%s>.'#13#10'%s', [AFileName, e.message] );
    end;
  end
  else
  begin
    FRoot := Newtecontainer;
    FRoot.Version := 1;
  end;
  FScript := FRoot.Script;
end;

destructor TisTEScriptXML.Destroy;
begin
  FScript := nil;
  FRoot := nil;
  inherited;
end;

function TisTEScriptXML.FindObjectById( id: integer ): TisTEScriptObjectRec;
var
  i: integer;
  ob: IXMLObjectsType;
  obj: IXMLObjectType;
begin
  ob := FScript.Objects;
  for i := 0 to ob.Count-1 do
  begin
    obj := ob[i];
    Assert( obj.id >= 0 );
    if obj.id = id then
    begin
      Result.ThisIsNull := false;
      Result.Name := obj.Winname;
      Result.ClassName := obj.Winclass;
      Result.IsDelphiObj := obj.Delphi;
      Result.IsZoomed := obj.Zoomed;
      Result.Annotation := obj.Annotation;
      Exit;
    end
  end;
  raise EisException.CreateFmt( 'Cannot find object with id=%d',[id] );
end;

function TisTEScriptXML.FindOrAddObject(
  const fore: TisTEScriptObjectRec): integer;
var
  i: integer;
  maxid: integer;
  ob: IXMLObjectsType;
  obj: IXMLObjectType;
begin
  Assert( not fore.ThisIsNull );
  ob := FScript.Objects;
  maxid := -1;
  for i := 0 to ob.Count-1 do
  begin
    obj := ob[i];
    Assert( obj.id >= 0 );
    maxid := max( maxid, obj.id );
    if (obj.Winname = fore.Name)
      and (obj.Winclass = fore.ClassName)
      and (obj.Delphi = fore.IsDelphiObj)
      and (obj.Zoomed = fore.IsZoomed)
      and (obj.Annotation = fore.Annotation)
      then
    begin
      Result := obj.id;
      Exit;
    end
  end;
  obj := FScript.Objects.Add;
  obj.Id := maxid + 1;
  obj.Winname := fore.Name;
  obj.Winclass := fore.ClassName;
  obj.Delphi := fore.IsDelphiObj;
  obj.Zoomed := fore.IsZoomed;
  obj.Annotation := fore.Annotation;
  Result := obj.Id;
end;

procedure TisTEScriptXML.PurgeUnusedObjects;
var
  i: integer;
  id: integer;
  ob: IXMLObjectsType;
  used: array of integer;
  function val2idx( id: integer): integer;
  var
    j: integer;
  begin
    Result := -1;
    for j := low(used) to high(used) do
    begin
      if used[j] = id then
      begin
        Result := j;
        break;
      end
    end;
  end;
begin
  for i := 0 to FScript.Commands.Count-1 do
  begin
    id := FScript.Commands[i].window_id;
    if val2idx(id) = -1 then
    begin
      SetLength( used, Length(used)+1 );
      used[high(used)] := id;
    end;
    if (FScript.Commands[i].ChildNodes.FindNode('mouse') <> nil) and
       FScript.Commands[i].Mouse.HasAttribute('target_id') then
    begin
      id := FScript.Commands[i].Mouse.Target_id;
      if val2idx(id) = -1 then
      begin
        SetLength( used, Length(used)+1 );
        used[high(used)] := id;
      end;
    end;
  end;
  ob := FScript.Objects;
  i := 0;
  while i < ob.Count do
  begin
    if val2idx(ob[i].id) = -1 then
      ob.Delete(i)
    else
      inc(i);
  end
end;

function TisTEScriptXML.GetCommand(i: integer): IisTEScriptCommand;
begin
//  ODS('GetCommand-enter');
  Result := TisTEScriptCommand.Create( Self, FScript.Commands[i] );
//  ODS('GetCommand-leave');
end;

function TisTEScriptXML.GetComments: String;
begin
  Result := FScript.Comment;
end;

function TisTEScriptXML.GetCount: integer;
begin
  Result := FScript.Commands.Count;
end;

procedure TisTEScriptXML.SetComments(const AComments: String);
begin
  if FScript.Comment <> AComments then
  begin
    FScript.Comment := AComments;
    FModified := true;
  end;
end;

procedure TisTEScriptXML.ClearCommands;
begin
  FScript.Commands.Clear;
  FScript.Objects.Clear;
end;

procedure TisTEScriptXML.Save;
begin
  if FModified then
  begin
    Assert(FFilename <> '');
    PurgeUnusedObjects;
    CopyFile( PChar(FFileName), PChar( ChangeFileExt(FFileName, '.bak') ), FALSe );
    FScript.OwnerDocument.SaveToFile( FFileName );
    FModified := false;
  end;
end;

function TisTEScriptXML.GetFilename: string;
begin
  Result := FFilename;
end;

function TisTEScriptXML.AsText: string;
begin
  Result := FScript.XML;
end;

function TisTEScriptXML.IsModified: boolean;
begin
  Result := FModified;
end;

procedure TisTEScriptXML.DeleteToEnd(i: integer);
begin
  Assert( i < Count );
  while Count > i do
  begin
    FScript.Commands.Delete(i);
    if assigned(FSink) then
      FSink.CommandDeleted(i);
  end;
  FModified := true;
end;

procedure TisTEScriptXML.Advise(sink: IIsTEScriptEventSink);
begin
  FSink := sink;
end;

procedure TisTEScriptXML.CommandAdded;
begin
  if assigned(FSink) then
    FSink.CommandAdded( Count-1 );
end;

procedure TisTEScriptXML.SaveAs(afilename: string);
begin
  FFilename := afilename;
  FModified := true;
  Save;
end;

{ TisTEScriptCommand }

function TisTEScriptCommand.CommandType: TisTEScriptCommandType;
begin
  Result := FCommandType;
end;

constructor TisTEScriptCommand.Create(aScript: IisTEScriptPrivate; aCommand: IXMLCommandType);
begin
  FScript := aScript;
  FCommand := aCommand;
  if FCommand.ChildNodes.IndexOf('mouse') <> -1 then
    FCommandType := teSCMouse
  else if FCommand.ChildNodes.IndexOf('key') <> -1 then
    FCommandType := teSCKey
  else if FCommand.ChildNodes.IndexOf('popupmenu') <> -1 then
    FCommandType := teSCPopupMenu
  else
    raise EisException.Create('Unknown command');
  FForegroundWindowObj := FScript.FindObjectById( FCommand.Window_id )
end;

function TisTEScriptCommand.Delay: DWORD;
begin
  Result := FCommand.Delay;
end;

function TisTEScriptCommand.ForegroundWindow: TisTEScriptObjectRec;
begin
  Result := FForegroundWindowObj;
end;

function GetNextStrValue(var AStr: string; const ADelim: String): String;
var
  j: Integer;
begin
   j := Pos(ADelim, AStr);
   if j = 0 then
   begin
     Result := AStr;
     AStr := '';
   end
   else
   begin
     Result := Copy(AStr, 1, j - 1);
     Delete(AStr, 1, j-1+Length(ADelim));
   end;
end;

//from gdystrset
function StrToSet( str, separator: string): TDynStringArray;
var
  s: string;
begin
  str := trim(str);
  s := GetNextStrValue( str, separator );
  while s <> '' do
  begin
    SetLength( Result, Length(Result)+1 );
    Result[high(Result)] := s;
    str := trim(str);
    s := GetNextStrValue( str, separator );
  end
end;

function TisTEScriptCommand.GetMenuPath: TDynStringArray;
begin
  Assert( FCommandType = teSCPopupMenu );
  Result := StrToSet( FCommand.Popupmenu.Path, '|' );
end;

function TisTEScriptCommand.GetPoint: TPoint;
begin
  Assert( FCommandType = teSCMouse );
  Result := Point( FCommand.Mouse.X, FCommand.Mouse.Y );
end;

function TisTEScriptCommand.GetTarget: TisTEScriptObjectRec;
begin
  Assert( FCommandType = teSCMouse );
  if FCommand.Mouse.HasAttribute('target_id') then
    Result := FScript.FindObjectById( FCommand.Mouse.Target_id )
  else
    Result.ThisIsNull := true;
end;

function TisTEScriptCommand.Key_GetMsg: DWORD;
begin
  Assert( FCommandType = teSCKey );
  Result := MsgFromAction( FCommand.Key.Action );
end;

function TisTEScriptCommand.GetScanCode: UINT;
begin
  Assert( FCommandType = teSCKey );
  Result := FCommand.Key.Scan;
end;

function TisTEScriptCommand.GetVKey: UINT;
begin
  Assert( FCommandType = teSCKey );
  Result := FCommand.Key.Vk;
end;

function TisTEScriptCommand.Mouse_GetMsg: DWORD;
begin
  Assert( FCommandType = teSCMouse );
  Result := MsgFromAction( FCommand.Mouse.Action );
end;

initialization
  DefaultDOMVendor := 'Open XML';
//  DefaultDOMVendor := 'Xerces XML';

end.
