unit EvTransportInterfaces;

interface

uses SysUtils, isBaseClasses, EvStreamUtils, ISZippingRoutines, isStreamTCP,
     isTransmitter, isCryptoApiWrapper;

type
  TevSessionState = (ssInactive, ssPerformingHandshake, ssWaitingForLogin, ssPerformingLogin,
                     ssActive, ssMerging, ssInvalid);
  TevEncryptionType = (etNone, etProprietary, etSSL);

  IevDatagramHeader = interface
  ['{F25056BB-55AE-4094-AFC1-45BEA7391AEF}']
    function  ProtocolVersion: Byte;
    function  GetSequenceNbr: Int64;
    function  GetSessionID: TisGUID;
    function  GetContextID: TisGUID;
    procedure SetSequenceNbr(const AValue: Int64);
    procedure SetSessionID(const AValue: TisGUID);
    procedure SetContextID(const AValue: TisGUID);
    function  GetMethod: String;
    procedure SetMethod(const AValue: String);
    function  GetUser: String;
    procedure SetUser(const AValue: String);
    function  GetPassword: String;
    procedure SetPassword(const AValue: String);
    procedure Assign(const ASource: IevDatagramHeader);
    function  GetIsResponse: Boolean; // Indicates that datagram is a response on client's call
    property  SequenceNbr: Int64 read GetSequenceNbr write SetSequenceNbr; // Sequentional datagram number for making request-response calls
    property  SessionID: TisGUID read GetSessionID write SetSessionID;
    property  ContextID: TisGUID read GetContextID write SetContextID;
    property  Method: String read GetMethod write SetMethod;
    property  IsResponse: Boolean read GetIsResponse;
    property  User: String read GetUser write SetUser;
    property  Password: String read GetPassword write SetPassword;
  end;

  IevDatagram = interface
  ['{2EBA7B99-0790-41F4-A432-1E462F957369}']
    function  Header: IevDatagramHeader;
    function  Params: IisListOfValues;
    procedure SetParamData(const AStream: IisStream);
    function  GetParamData: IisStream;    
    function  IsError: Boolean; // Indicates that datagram returned an error
    function  GetErrorText: String;
    function  GetException: Exception;

    // For statistic only
    function  GetTransmittedBytes: Cardinal;
    procedure SetTransmittedBytes(const AValue: Cardinal);
    function  GetTransmissionTime: Cardinal;
    procedure SetTransmissionTime(const AValue: Cardinal);
    property  TransmittedBytes: Cardinal read GetTransmittedBytes write SetTransmittedBytes;
    property  TransmissionTime: Cardinal read GetTransmissionTime write SetTransmissionTime;
  end;


  IevSession = interface
  ['{E43E2013-E4E6-4616-B5A8-37A7163B7CBE}']
    procedure Lock;
    procedure UnLock;
    function  GetSessionID: TisGUID;
    procedure SetSessionID(const AValue: TisGUID);
    function  GetState: TevSessionState;
    procedure SetState(const AValue: TevSessionState);
    function  KeepAlive: Boolean;
    function  GetMergingWith: IevSession;
    procedure SetMergingWith(const AValue: IevSession);
    function  GetLogin: String;
    procedure SetLogin(const AValue: String);
    function  GetPassword: String;
    procedure SetPassword(const AValue: String);
    function  GetErrorMessage: String;
    function  GetAppID: String;
    procedure SetAppID(const AValue: String);
    function  GetEncryptionType: TevEncryptionType;
    procedure SetEncryptionType(const AValue: TevEncryptionType);
    function  EncryptionTypeDscr: String;
    function  GetCompressionLevel: TisCompressionLevel;
    procedure SetCompressionLevel(const AValue: TisCompressionLevel);
    function  GetConnectionCount: Integer;
    function  AddConnection(const AConnection: IisTCPStreamAsyncConnection): Boolean;
    function  GetEncryptor: IisCryptoApiWrapper;
    procedure SendDatagram(const ADatagram: IevDatagram);
    procedure SendData(const AData: IevDualStream; const ADatagramHeader: IevDatagramHeader; out ASentBytes: Cardinal);
    function  SendRequest(const ADatagram: IevDualStream; const ASequenceNbr: Int64;
                          const ABlockingMode: Boolean; out ASentBytes, AReceivedBytes, ATransmissionTime: Cardinal): IevDualStream; overload;
    function  SendRequest(const ADatagram: IevDatagram; const ABlockingMode: Boolean = True): IevDatagram; overload;
    function  SendRequest(const ADatagram: IevDatagram; const AMaxWaitTime: Integer): IevDatagram; overload;
    function  GetResponseDataFor(const ADatagramSequenceNbr: Int64; const AWaitTime: Cardinal;
                                 out AReceivedBytes, ATransmissionTime: Cardinal): IEvDualStream;
    function  GetResponseDatagramFor(const ADatagramSequenceNbr: Int64; const AWaitTime: Cardinal = 0): IevDatagram;
    function  GetTransmitter: IisTCPStreamTransmitter;
    function  GetLastActivity: TDateTime;
    function  GetRemoteIPAddress: String;
    function  GetExtension: IInterface;
    function  StateDescr: String;
    function  CheckIfAlive: Boolean;
    property  SessionID: TisGUID read GetSessionID write SetSessionID;
    property  AppID: String read GetAppID write SetAppID;
    property  Login: String read GetLogin write SetLogin;
    property  Password: String read GetPassword write SetPassword;
    property  State: TevSessionState read GetState write SetState;
    property  MergingWith: IevSession read GetMergingWith write SetMergingWith;
    property  EncryptionType: TevEncryptionType read GetEncryptionType write SetEncryptionType;
    property  CompressionLevel: TisCompressionLevel read GetCompressionLevel write SetCompressionLevel;
    property  Transmitter: IisTCPStreamTransmitter read GetTransmitter;
    property  Encryptor: IisCryptoApiWrapper read GetEncryptor;
    property  LastActivity: TDateTime read GetLastActivity;
    property  Extension: IInterface read GetExtension;
    property  ErrorMessage: String read GetErrorMessage;
  end;


  IevDatagramDispatcher = interface
  ['{7FB80AA9-3587-4E04-BB9B-6EAC0641E91D}']
    function  GetThreadPoolCapacity: Integer;
    procedure SetThreadPoolCapacity(const AValue: Integer);
    function  GetSessionTimeout: Integer;
    procedure SetSessionTimeout(const AValue: Integer);
    procedure SetTransmitter(const AValue: IisTCPStreamTransmitter);
    function  GetTransmitter: IisTCPStreamTransmitter;
    function  SessionCount: Integer;
    function  GetSessions: IisList;
    function  GetSessionByIndex(const AIndex: Integer): IevSession;
    function  FindSessionByID(const ASessionID: TisGUID): IevSession;
    procedure DeleteSession(const ASession: IevSession);
    procedure AddSession(const ASession: IevSession);
    function  CreateSession(const ASessionID: TisGUID): IevSession;
    procedure LockSessionList;
    procedure UnlockSessionList;
    function  ThisSession: IevSession;
    procedure DispatchDatagram(const ADatagram: IevDatagram; out AResult: IevDatagram);
    function  GetExecutingDatagramInfo: IisParamsCollection;
    procedure TerminateDispatch;
    procedure WaitDispatchToFinish;
    property  Transmitter: IisTCPStreamTransmitter read GetTransmitter write SetTransmitter;
    property  ThreadPoolCapacity: Integer read GetThreadPoolCapacity write SetThreadPoolCapacity;
    property  SessionTimeout: Integer read GetSessionTimeout write SetSessionTimeout; //session timeout in seconds. Default is 0, which means remove immediately after disconnect.
  end;


  IevMessageDispatcher = interface
  ['{6BC4BFCE-8396-466C-8ED4-0123DF2F896D}']
    procedure SetTransmitter(const AValue: IisUDPStreamTransmitter);
    function  GetTransmitter: IisUDPStreamTransmitter;
    function  GetThreadPoolCapacity: Integer;
    procedure SetThreadPoolCapacity(const AValue: Integer);
    procedure SendMessage(const ASocketID: TisGUID; const ADatagram: IevDatagram);    
    property  Transmitter: IisUDPStreamTransmitter read GetTransmitter write SetTransmitter;
    property  ThreadPoolCapacity: Integer read GetThreadPoolCapacity write SetThreadPoolCapacity;
  end;


  IevTCPServer = interface
  ['{52D94E7D-B068-4022-B0C6-198079F50768}']
    procedure SetSSLTimeout(const Value: Integer);
    function  GetPort: String;
    procedure SetPort(const AValue: String);
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetDatagramDispatcher: IevDatagramDispatcher;
    property  Active: Boolean read GetActive write SetActive;
    property  Port: String read GetPort write SetPort;
    property  SSLTimeout: Integer write SetSSLTimeout;
    property  DatagramDispatcher: IevDatagramDispatcher read GetDatagramDispatcher;
  end;


  IisTCPClient = interface
  ['{9476005C-5D4F-4B6C-9D87-ED205C6F9108}']
    function  GetDatagramDispatcher: IevDatagramDispatcher;
    property  DatagramDispatcher: IevDatagramDispatcher read GetDatagramDispatcher;
  end;

implementation

end.