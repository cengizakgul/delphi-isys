unit InitStackTracer;

interface

uses SysUtils, SyncObjs, Windows, Forms, isLogFile, ISErrorUtils, ISUtils,
     madExcept, madLinkDisAsm, madListHardware, madListProcesses, madListModules,
     isExceptions, EvStreamUtils, isBasicStackTracer;

implementation

uses isThreadManager;

var
  GScs: TCriticalSection;

function MadExceptGetStack(E: Exception = nil; ExceptAddr: Pointer = nil): string;
var
  exc : IMEException;
begin
  GScs.Enter;
  try
    exc := NewException(etNormal, E, ExceptAddr);
    exc.ShowPleaseWaitBox := False;
    exc.ListThreads := E is EisDumpAllThreads;
    exc.ShowCpuRegisters := False;
    exc.ShowStackDump := False;
    exc.ShowDisAsm := False;
    exc.PluginEnabled['processes'] := False;
    exc.PluginEnabled['modules'] := False;
    exc.PluginEnabled['hardware'] := False;
    result := exc.BugReport;
  finally
    GScs.Leave;
  end;
end;


procedure AllExceptionHandler(const ExceptIntf: IMEException; var Handled: boolean);

  procedure AddToLog;
  var
    err: String;
  begin
    if GlobalLogFile <> nil then
    begin
      try
        Err := MadExceptGetStack(Exception(ExceptIntf.ExceptObject), ExceptIntf.ExceptAddr);
      except
        Err := Exception(ExceptIntf.ExceptObject).Message + #13 + 'Stack info is unavailable';
      end;

      GlobalLogFile.AddEvent(etFatalError, 'Internal',
        'Unexpected error in thread "' + madExcept.GetThreadName(ExceptIntf.CrashedThreadId) + '"', Err);
    end;
  end;

begin
  case GetUnhandledErrorSettings of
    uesShowMTLogOthers:
    begin
      if ExceptIntf.CrashedThreadId = MainThreadID then
        Handled := False
      else
      begin
        AddToLog;
        Handled := True;
      end;
    end;

    uesLogAllShowNothing:
    begin
      AddToLog;
      Handled := True;
    end;
  else
    Handled := False;
  end;
end;

procedure MainThreadExceptionHandler(const ExceptIntf: IMEException; var Handled: boolean);

  procedure DoInMainThread(const Params: TTaskParamList);
  var
    E: Exception;
    S: IEvDualStream;
  begin
    S := IInterface(Params[0]) as IEvDualStream;
    S.Position := 0;

    E := RestoreException(S);
    try
      if Assigned(Application.OnException) and Assigned(Application.MainForm) then
      begin
        try
          Application.OnException(nil, E)
        except
          ISExceptMessage(E);
        end;
      end
      else
        ISExceptMessage(E);
    finally
      E.Free;
    end;
  end;

var
  E: EisException;
  S: IEvDualStream;
begin
  if (GetUnhandledErrorSettings = uesShowMTLogOthers) and (GetCurrentThreadId = MainThreadID) then
  begin
    if ExceptIntf.ExceptObject is EisException then
      E := EisException(ExceptIntf.ExceptObject)
    else
      E := EisVCLException.Create(Exception(ExceptIntf.ExceptObject));

    try
      if E.StackInfo = '' then
        try
          E.StackInfo := MadExceptGetStack(Exception(ExceptIntf.ExceptObject), ExceptIntf.ExceptAddr);
        except
          E.StackInfo := 'Stack info is unavailable';
        end;

      S := E.GetStream;

      if CurrentThreadIsMain and not GlobalThreadManagerIsActive then
        DoInMainThread(MakeTaskParams([S]))
      else
        GlobalThreadManager.RunInMainThread(@DoInMainThread, MakeTaskParams([S]), False);

      Handled := True;
    finally
      if E <> ExceptIntf.ExceptObject then
        E.Free;
    end
  end

  else
    Handled := False;
end;

procedure MadExceptSetThreadName(threadID: dword; threadName: string);
begin
  MadExcept.NameThread(threadID, threadName);
end;

function MadExceptGetThreadName(threadID: dword): string;
begin
  Result := MadExcept.GetThreadName(threadID);
end;


initialization
  GScs := TCriticalSection.Create;
  InitGetStackProc(@MadExceptGetStack);
  InitSetThreadNameProc(@MadExceptSetThreadName, @MadExceptGetThreadName);
  UnRegisterBasicExceptionHandler;
  RegisterExceptionHandler(AllExceptionHandler, stDontSync, epMainPhase);
  RegisterExceptionHandler(MainThreadExceptionHandler, stTrySyncCallAlways, epMainPhase);

finalization
  FreeAndNil(GScs);

end.
