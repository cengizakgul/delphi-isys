unit isStatistics;

interface

uses Classes, SysUtils, Windows, isBaseClasses, EvStreamUtils;

type
  IisSimpleStatEvent = interface
  ['{FEFE68D4-369B-4FD5-9C2D-4C13514F094B}']
    procedure StartWatch;
    procedure StopWatch;
    function  BeginTime: TDateTime;
    function  Duration: Cardinal;
    procedure AddCorrection(const AValue: Integer);
    procedure ChangeBeginTime(const AValue: TDateTime);
  end;


  IisStatEvent = interface
  ['{E33A7437-12A0-4A96-80EC-887FE7F5CA84}']
    function  Name: String;
    function  BeginTime: TDateTime;
    function  Duration: Cardinal;
    procedure AddCorrection(const AValue: Integer);
    procedure ChangeBeginTime(const AValue: TDateTime);
    function  Properties: IisListOfValues;
    function  AddEvent(const AName: String): IisStatEvent; overload;
    function  AddEvent(const AEvent: IisStatEvent): Integer; overload;
    procedure DeleteEvent(const AIndex: Integer);
    function  GetEvent(AIndex: Integer): IisStatEvent;
    function  EventsCount: Integer;
    procedure Clear;
    procedure WriteToStream(const AStream: IisStream);
    procedure ReadFromStream(const AStream: IisStream);
    function  GetAsStream: IisStream;
    procedure SetAsStream(const AStream: IisStream);
    property  AsStream: IisStream read GetAsStream write SetAsStream;
    property  Events[Index: Integer]: IisStatEvent read GetEvent;
  end;


  IisStatistics = interface
  ['{42D4EE82-2F58-4425-B069-7CE6386E90BB}']
    procedure BeginEvent(const AName: String);
    procedure EndEvent;
    function  ActiveEvent: IisStatEvent;
    function  TopEvent: IisStatEvent;
    procedure Clear;
  end;


  IisStatisticsFile = interface
  ['{9E20F27F-AE63-41AD-85DC-8D595700E2E4}']
    procedure AppendEvent(const AEvent: IisStatEvent);
    function  Count: Integer;
    function  GetEvent(Index: Integer): IisStatEvent;
    property  Events[Index: Integer]: IisStatEvent read GetEvent;
  end;


  TisStatistics = class(TisInterfacedObject, IisStatistics)
  private
    FTopEvent: IisStatEvent;
    FActiveEvent: IisStatEvent;
    FStack: IisInterfaceList;
  protected
    procedure OnFinishTopEvent; virtual;

    procedure BeginEvent(const AName: String);
    procedure EndEvent;
    function  ActiveEvent: IisStatEvent;
    function  TopEvent: IisStatEvent;
    procedure Clear;
  end;


  TisStatisticsFile = class(TisInterfacedObject, IisStatisticsFile)
  private
    FStream: IisStream;
    FRecIndex: TList;
    procedure InitRecIndex;
  protected
    procedure DoOnDestruction; override;

    procedure AppendEvent(const AEvent: IisStatEvent);
    function  Count: Integer;
    function  GetEvent(Index: Integer): IisStatEvent;
  public
    constructor Create(const AFileName: String; const AReadOnly: Boolean = False); reintroduce;
  end;


  function CreateStatEvent(const AName: String): IisStatEvent;
  function CreateSimpleStatEvent: IisSimpleStatEvent;

implementation

uses DateUtils;

type
  TPerformaceCounter = record
    CounterB: Int64;
    CounterE: Int64;
    CounterFreq: Int64;
  end;

  TisSimpleStatEvent = class(TisInterfacedObject, IisSimpleStatEvent)
  private
    FBeginTime: TDateTime;
    FCounter: TPerformaceCounter;
    FDuration: Cardinal;
  protected
    procedure StartWatch;
    procedure StopWatch;
    function  BeginTime: TDateTime;
    function  Duration: Cardinal;
    procedure AddCorrection(const AValue: Integer);
    procedure ChangeBeginTime(const AValue: TDateTime);
  end;


  TisStatEvent = class(TisSimpleStatEvent, IisStatEvent)
  private
    FName: String;
    FProperties: IisListOfValues;
    FEvents: IisInterfaceList;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;

    function  Name: String;
    function  Properties: IisListOfValues;
    function  AddEvent(const AName: String): IisStatEvent; overload;
    function  AddEvent(const AEvent: IisStatEvent): Integer; overload;
    procedure DeleteEvent(const AIndex: Integer);
    function  GetEvent(AIndex: Integer): IisStatEvent;
    function  EventsCount: Integer;
    procedure Clear;
  public
    class function GetTypeID: String; override;
    constructor Create(const AName: String); reintroduce;
  end;

function CreateStatEvent(const AName: String): IisStatEvent;
begin
  Result := TisStatEvent.Create(AName);
end;

function CreateSimpleStatEvent: IisSimpleStatEvent;
begin
  Result := TisSimpleStatEvent.Create;
end;


{ TisStatistics }

function TisStatistics.ActiveEvent: IisStatEvent;
begin
  Result := FActiveEvent;
end;

procedure TisStatistics.BeginEvent(const AName: String);
begin
  if not Assigned(FStack) then
    FStack := TisInterfaceList.Create;

  FStack.Add(FActiveEvent);

  if not Assigned(FActiveEvent) then
  begin
    if not Assigned(FTopEvent) then
      FTopEvent := TisStatEvent.Create(AName)
    else
    begin
      FTopEvent.Clear;
      TisStatEvent((FTopEvent as IisInterfacedObject).GetImplementation).FName := AName;
    end;
    FActiveEvent := FTopEvent;
  end
  else
    FActiveEvent := FActiveEvent.AddEvent(AName);

  TisStatEvent((FActiveEvent as IisInterfacedObject).GetImplementation).StartWatch;
end;

procedure TisStatistics.Clear;
begin
  FActiveEvent := nil;
  FStack := nil;
  FTopEvent := nil;
end;

procedure TisStatistics.EndEvent;
begin
  TisStatEvent((FActiveEvent as IisInterfacedObject).GetImplementation).StopWatch;

  FActiveEvent := FStack[FStack.Count - 1] as IisStatEvent;
  FStack.Delete(FStack.Count - 1);

  if not Assigned(FActiveEvent) then
    OnFinishTopEvent;
end;

procedure TisStatistics.OnFinishTopEvent;
begin
end;

function TisStatistics.TopEvent: IisStatEvent;
begin
  Result := FTopEvent;
end;

{ TisStatEvent }

function TisStatEvent.AddEvent(const AName: String): IisStatEvent;
begin
  Result := TisStatEvent.Create(AName);
  AddEvent(Result);
end;

function TisStatEvent.EventsCount: Integer;
begin
  Result := FEvents.Count;
end;

procedure TisStatEvent.Clear;
begin
  FProperties.Clear;
  FEvents.Clear;
end;

procedure TisStatEvent.DoOnConstruction;
begin
  inherited;
  FProperties := TisListOfValues.Create;
  FEvents := TisInterfaceList.Create;
end;

function TisStatEvent.GetEvent(AIndex: Integer): IisStatEvent;
begin
  Result := FEvents[AIndex] as IisStatEvent;
end;

procedure TisStatEvent.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
var
  i, n: Integer;
  Evt: IisStatEvent;
begin
  inherited;
  FName := AStream.ReadShortString;
  FBeginTime := AStream.ReadDouble;
  FDuration := Cardinal(AStream.ReadInteger);
  FProperties.ReadFromStream(AStream);

  FEvents.Clear;
  n := AStream.ReadInteger;
  FEvents.Capacity := n;
  for i := 1 to n do
  begin
    Evt := AddEvent('');
    Evt.ReadFromStream(AStream);
  end;
end;

procedure TisStatEvent.WriteSelfToStream(const AStream: IisStream);
var
  i: Integer;
begin
  inherited;
  AStream.WriteShortString(FName);
  AStream.WriteDouble(FBeginTime);
  AStream.WriteInteger(FDuration);
  FProperties.WriteToStream(AStream);

  AStream.WriteInteger(EventsCount);
  for i := 0 to EventsCount - 1 do
    GetEvent(i).WriteToStream(AStream);
end;

procedure TisStatEvent.DeleteEvent(const AIndex: Integer);
begin
  FEvents.Delete(AIndex);
end;

function TisStatEvent.Name: String;
begin
  Result := FName;
end;

function TisStatEvent.Properties: IisListOfValues;
begin
  Result := FProperties;
end;

function TisStatEvent.AddEvent(const AEvent: IisStatEvent): Integer;
begin
  Result := FEvents.Add(AEvent);
end;

class function TisStatEvent.GetTypeID: String;
begin
  Result := 'StatEvt'; 
end;

constructor TisStatEvent.Create(const AName: String);
begin
  inherited Create;
  FName := AName;
end;

{ TisStatisticsFile }

procedure TisStatisticsFile.AppendEvent(const AEvent: IisStatEvent);
var
  S: IisStream;
  p: Integer;
begin
  S := AEvent.AsStream;
  FStream.Position := FStream.Size;
  p := FStream.Position;
  FStream.WriteStream(S);

  if Assigned(FRecIndex) then
    FRecIndex.Add(Pointer(p));
end;

function TisStatisticsFile.Count: Integer;
begin
  InitRecIndex;
  Result := FRecIndex.Count;
end;

constructor TisStatisticsFile.Create(const AFileName: String; const AReadOnly: Boolean = False);
begin
  inherited Create;
  if FileExists(AFileName) then
    FStream := TisStream.CreateFromFile(AFileName, False, AReadOnly, True)
  else
  begin
    ForceDirectories(ExtractFilePath(AFileName));
    FStream := TisStream.CreateFromNewFile(AFileName, False, True);
  end;
end;

procedure TisStatisticsFile.DoOnDestruction;
begin
  FRecIndex.Free;
  inherited;
end;

function TisStatisticsFile.GetEvent(Index: Integer): IisStatEvent;
var
  S: IisStream;
begin
  InitRecIndex;

  FStream.Position := Integer(FRecIndex[Index]);
  S := FStream.ReadStream(nil);
  Result := TisStatEvent.Create('');
  Result.AsStream := S;
end;

procedure TisStatisticsFile.InitRecIndex;
var
  EventSize: Integer;
begin
  if not Assigned(FRecIndex) then
  begin
    FRecIndex := TList.Create;

    FStream.Position := 0;
    while not FStream.EOS do
    begin
      FRecIndex.Add(Pointer(FStream.Position));
      EventSize := FStream.ReadInteger;
      FStream.Position := FStream.Position + EventSize;
    end;
  end;
end;

{ TisSimpleStatEvent }

procedure TisSimpleStatEvent.AddCorrection(const AValue: Integer);
begin
  if FCounter.CounterB = 0 then
  begin
    if AValue > 0 then
      FDuration := FDuration + Cardinal(AValue)
    else if AValue < 0 then
      FDuration := FDuration - Cardinal(AValue);
  end

  else
  begin
    if AValue > 0 then
      FCounter.CounterB := FCounter.CounterB - Round(AValue / 1000 * FCounter.CounterFreq)
    else if AValue < 0 then
      FCounter.CounterB := FCounter.CounterB + Round(AValue / 1000 * FCounter.CounterFreq);
  end;
end;

function TisSimpleStatEvent.BeginTime: TDateTime;
begin
  Result := FBeginTime;
end;

procedure TisSimpleStatEvent.ChangeBeginTime(const AValue: TDateTime);
begin
  FBeginTime := AValue;
end;

function TisSimpleStatEvent.Duration: Cardinal;
begin
  if FCounter.CounterB = 0 then
    Result := FDuration
  else
  begin
    QueryPerformanceCounter(FCounter.CounterE);
    Result := Round((FCounter.CounterE - FCounter.CounterB) / FCounter.CounterFreq  * 1000);
  end;
end;

procedure TisSimpleStatEvent.StartWatch;
begin
  FBeginTime := Now;
  FDuration := 0;
  QueryPerformanceFrequency(FCounter.CounterFreq);
  QueryPerformanceCounter(FCounter.CounterB);
end;

procedure TisSimpleStatEvent.StopWatch;
begin
  QueryPerformanceCounter(FCounter.CounterE);
  FDuration := Round((FCounter.CounterE - FCounter.CounterB) / FCounter.CounterFreq  * 1000);
  FCounter.CounterB := 0;
end;

initialization
  ObjectFactory.Register([TisStatEvent]);

finalization
  SafeObjectFactoryUnRegister([TisStatEvent]);

end.
