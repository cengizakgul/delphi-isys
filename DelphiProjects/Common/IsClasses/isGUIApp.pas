unit isGUIApp;

interface

uses SysUtils, Forms, isBasicUtils, isUtils;

procedure RunISGUI(const AppTitle, AppID: String; const InitProc, DeinitProc: TProcedure);

implementation

procedure RunISGUI(const AppTitle, AppID: String; const InitProc, DeinitProc: TProcedure);
begin
  try
    Application.Initialize;
    Application.Title := AppTitle;

    SetAppID(AppID);

    CheckIfTampered;

    if Assigned(InitProc) then
      InitProc;

    try
      Application.Run;
    finally
      if Assigned(DeinitProc) then
        DeinitProc;
    end;
    
  except
    on E: Exception do
      Application.HandleException(E);
  end
end;


end.