unit IsCryptoModule;

interface

uses {wcrypt2,} Classes, SysUtils, Windows, EvStreamUtils;

type

  IevCryptoModule = interface
  ['{4800D44A-65CD-4D43-8AF7-1C3B9BF03C6D}']
    procedure SetCipherKey(const ACipherKey: String);
    procedure EncryptStream(const InStream: IevDualStream; const OutStream: IevDualStream);
    procedure DecryptStream(const InStream: IevDualStream; const OutStream: IevDualStream);
    function EncryptString(const InString: String): string;
    function DecryptString(const InString: String): string;
    function EncryptStringHex(const InString: string): String;
    function DecryptHexString(const InStringHex: string): String;
    property CipherKey: String write SetCipherKey;    
  end;

function GetCryptoModule: IevCryptoModule;

implementation

uses SEncryptionRoutines;

type
  TevBlowFishCrypto = class(TInterfacedObject, IevCryptoModule)
  private
    FCipherKey: String;
  public
    constructor Create;
    destructor Destroy; override;
    procedure SetCipherKey(const ACipherKey: String);
    procedure EncryptStream(const InStream: IevDualStream; const OutStream: IevDualStream);
    procedure DecryptStream(const InStream: IevDualStream; const OutStream: IevDualStream);
    function EncryptString(const InString: String): string;
    function DecryptString(const InString: String): string;
    function EncryptStringHex(const InString: string): String;
    function DecryptHexString(const InStringHex: string): String;
    property CipherKey: String write SetCipherKey;
  end;


function GetCryptoModule: IevCryptoModule;
begin
  Result := TevBlowFishCrypto.Create;
end;


{ TevBlowFishCrypto }

constructor TevBlowFishCrypto.Create;
begin
  inherited;
  FCipherKey := DefaultCipherKey;
end;

function TevBlowFishCrypto.DecryptHexString(
  const InStringHex: string): String;
begin
  Result := SEncryptionRoutines.DecryptHexString(InStringHex, FCipherKey);
end;

procedure TevBlowFishCrypto.DecryptStream(const InStream,
  OutStream: IevDualStream);
begin
  SEncryptionRoutines.DecryptMemoryStream(InStream.RealStream, OutStream.RealStream, FCipherKey);
end;

function TevBlowFishCrypto.DecryptString(const InString: String): string;
begin
  Result := SEncryptionRoutines.DecryptString(InString, FCipherKey);
end;

destructor TevBlowFishCrypto.Destroy;
begin

  inherited;
end;

function TevBlowFishCrypto.EncryptStringHex(
  const InString: string): String;
begin
  Result := SEncryptionRoutines.EncryptHexString(InString, FCipherKey);
end;

procedure TevBlowFishCrypto.EncryptStream(const InStream,
  OutStream: IevDualStream);
begin
  SEncryptionRoutines.EncryptMemoryStream(InStream.RealStream, OutStream.RealStream, FCipherKey);
end;

function TevBlowFishCrypto.EncryptString(const InString: String): string;
begin
  Result := SEncryptionRoutines.EncryptString(InString, FCipherKey);
end;

procedure TevBlowFishCrypto.SetCipherKey(const ACipherKey: String);
begin
  FCipherKey := ACipherKey;
end;


end.
