unit IsTestLogicHooks;

interface

uses
  typinfo, Types, Forms, Classes, IsTypes;

const
  PROPERTY_NOT_FOUND = 'Property ''%s'' not found';
  ACTION_NOT_FOUND = 'Action ''%s'' not found';

type
  ELogicTestingException = class(IsException);

  ETestPropertyNotFound = class(ELogicTestingException);
  ETestActionNotFound = class(ELogicTestingException);

  TTestParam = record
    testParamName: string;
    testParamType: TTypeKind;
    testParamDesc: string;
    testParamIndexes: Integer;
  end;
  TTestParamList = array of TTestParam;

  ILogicTesting = interface
  ['{1BAD1302-8974-433C-84D6-E49BFB6A22FD}']
    function testGetControlName: string;

    function testGetUserModifiableProps: TtestParamList;
    function testGetAvailableProps: TtestParamList;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer);

    function testGetUserActions: TtestParamList;
    function testGetActionParamList(const ActionName: string): TtestParamList;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant);
  end;

procedure testNotifyUserPropChange(const testControl: ILogicTesting; const PropName: string; const Value: Variant; const PropIndexes: array of Integer; const makePending: Boolean = False);
procedure testNotifyUserAction(const testControl: ILogicTesting; const ActionName: string; const ActionParams: array of Variant);

procedure testSetUserPropValue(const ControlName: string; const PropName: string; const Value: Variant; const PropIndexes: array of Integer);
procedure testInvokeUserAction(const ControlName: string; const ActionName: string; const ActionParams: array of Variant);

function GetTestControlName(const control: TComponent): string;
function FindActionByIndex(const testControl: ILogicTesting; const index: Integer): string;
function FindPropByIndex(const testControl: ILogicTesting; const index: Integer): string;

function GetStandardProps: TTestParamList;
function GetEditableProps: TTestParamList;

procedure StartLogicRecording;
procedure StopLogicRecording;

var
  IsLogicRecording: Boolean;

implementation

uses Variants, SysUtils, StrUtils;

var
  LastUserPropChange: record
    pending: Boolean;
    controlName: string;
    propName: string;
    value: Variant;
    PropIndexes: array of Integer;
  end;

procedure recordLine(const line: string);
begin

end;

function VarToStrExt(const value: Variant): string;
var
  vt: Word;
begin
  vt := VarType(Value);
  if (vt = varOleStr) or
     (vt = varString) then
    Result := '''' + VarToStr(value) + ''''
  else
    Result := VarToStr(value);
end;

procedure recordUserPropChange(const controlName, propName: string; const Value: Variant; const PropIndexes: array of Integer);
var
  s: string;
  i: Integer;
begin
  s := controlName + '.' + propName;
  if Length(PropIndexes) > 0 then
  begin
    s := s + '[';
    for i := 0 to High(PropIndexes) do
      s := s + IntToStr(PropIndexes[i]) + ',';
    s := StuffString(s, Length(s), 1, ']');
  end;
  s := s + ' := ' + VarToStrExt(value) + ';';
  recordLine(s);
end;

procedure recordUserPropAction(const controlName, actionName: string; const ActionParams: array of Variant);
var
  s: string;
  i: Integer;
begin
  s := controlName + '.' + actionName;
  if Length(ActionParams) > 0 then
  begin
    s := s + '[';
    for i := 0 to High(ActionParams) do
      s := s + VarToStrExt(ActionParams[i]) + ',';
    s := StuffString(s, Length(s), 1, ']');
  end;
  s := s + ';';
  recordLine(s);
end;

procedure FlushPendingChange;
begin
  with LastUserPropChange do
    if pending then
    begin
      recordUserPropChange(controlName, propName, value, PropIndexes);

      LastUserPropChange.pending := False;
      LastUserPropChange.controlName := '';
      LastUserPropChange.propName := '';
      LastUserPropChange.value := Unassigned;
      SetLength(LastUserPropChange.PropIndexes, 0);
    end;
end;

procedure StartLogicRecording;
begin
  IsLogicRecording := True;
end;

procedure StopLogicRecording;
begin
  FlushPendingChange;
  IsLogicRecording := False;
end;

procedure testNotifyUserPropChange(const testControl: ILogicTesting; const PropName: string; const Value: Variant; const PropIndexes: array of Integer; const makePending: Boolean = False);
var
  i: Integer;

  function IsPropIndexesEqual(const PropIndexes1, PropIndexes2: array of Integer): Boolean;
  var
    i: Integer;
  begin
    Result := Length(PropIndexes1) = Length(PropIndexes2);
    if Result then
      for i := 0 to High(PropIndexes1) do
        if PropIndexes1[i] <> PropIndexes2[i] then
        begin
          Result := False;
          Break;
        end;
  end;
begin
  if IsLogicRecording then
  begin
    if makePending then
    begin
      if not LastUserPropChange.pending or
         (LastUserPropChange.controlName <> testControl.testGetControlName) or
         (LastUserPropChange.propName <> PropName) or
         not IsPropIndexesEqual(LastUserPropChange.PropIndexes, PropIndexes) then
      begin
        FlushPendingChange;
        LastUserPropChange.pending := True;
        LastUserPropChange.controlName := testControl.testGetControlName;
        LastUserPropChange.propName := PropName;
        setLength(LastUserPropChange.PropIndexes, Length(PropIndexes));
        for i := 0 to High(PropIndexes) do
          LastUserPropChange.PropIndexes[i] := PropIndexes[i];
      end;
      LastUserPropChange.value := Value;
    end
    else
    begin
      FlushPendingChange;
      recordUserPropChange(testControl.testGetControlName, PropName, Value, PropIndexes);
    end;
  end;
end;

procedure testNotifyUserAction(const testControl: ILogicTesting; const ActionName: string; const ActionParams: array of Variant);
begin
  if IsLogicRecording then
    recordUserPropAction(testControl.testGetControlName, ActionName, ActionParams);
end;

function FindTestControl(const ControlName: string): ILogicTesting;
var
  comp: TComponent;
begin
  comp := Application.MainForm.FindComponent(ControlName);
  Assert(Assigned(comp), ControlName + ' is not found');
  Assert(comp.GetInterface(ILogicTesting, Result), ControlName + ' does not support testing interface');
end;

function GetTestControlName(const control: TComponent): string;
begin
  Result := control.Name;
end;

procedure testSetUserPropValue(const ControlName: string; const PropName: string; const Value: Variant; const PropIndexes: array of Integer);
begin
  FindTestControl(ControlName).testSetUserPropValue(PropName, Value, PropIndexes);
end;

procedure testInvokeUserAction(const ControlName: string; const ActionName: string; const ActionParams: array of Variant);
begin
  FindTestControl(ControlName).testInvokeUserAction(ActionName, ActionParams);
end;

function FindActionByIndex(const testControl: ILogicTesting; const index: Integer): string;
begin
  Result := testControl.testGetUserActions[index].testParamName;
end;

function FindPropByIndex(const testControl: ILogicTesting; const index: Integer): string;
begin
  Result := testControl.testGetAvailableProps[index].testParamName;
end;

function GetStandardProps: TTestParamList;
begin
  SetLength(Result, 2);

  Result[0].testParamName := 'Visible';
  Result[0].testParamType := tkEnumeration;
  Result[0].testParamDesc := 'Visible property';

  Result[1].testParamName := 'Enabled';
  Result[1].testParamType := tkEnumeration;
  Result[1].testParamDesc := 'Enabled property';
end;

function GetEditableProps: TTestParamList;
begin
  Result := GetStandardProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'ReadOnly';
  Result[High(Result)].testParamType := tkEnumeration;
  Result[High(Result)].testParamDesc := 'ReadOnly property';
end;

end.
