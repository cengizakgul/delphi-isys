unit ISBasicClasses;

interface

uses
  Windows, ISKbmMemDataSet, ComCtrls, Messages, Controls, CommCtrl, ActnList, Classes,
  CheckLst, StdCtrls, Grids, SysUtils, Graphics, wwriched, Spin, ExtCtrls, DBCtrls, DB,
  wwdbspin, wwdotdot, wwdbcomb, wwdatsrc, wwDBDlg, wwDBLook, wwlocate, wwstr, ISBasicUtils,
  Menus, wwdbedit, wwDBGrid, wwdbigrd, Dialogs, Forms, wwexport, Registry, EvStreamUtils,
  StrUtils, ImgList, wwDBDateTimePicker, Buttons, typinfo, wwcommon, pngimage,
  ISDataAccessComponents, IniFiles, IsHwInfoClasses, IsTestLogicHooks, Variants,
  isExceptions, IsSYLK, XLSFile, XLSWorkbook, GIFImage,
  // removed to better separation of ISMF utilities with Evolution - ticket 99407
  //isMetaFileUnit,
  isVCLBugFix, SHDocVw,
  //all of the following added 7/25/2012 by Craig. New ancestors for GUI 2.0
  isUISpeedButton, isUIwwDBComboBox, isUILMDButton, isUIwwDBEdit, isUIDBMemo,
  isUIEdit, isUILMDDBRadioGroup, isUIwwDBDateTimePicker, isUIwwDBLookupCombo, isUIFashionPanel,
  isUIDBImage;

const
  WM_PANELDBLCLICK = WM_USER + $531;
  WM_PANELCLICK = WM_USER + $532;
  VersionedControlColor = $00CCFFFF;
  ControlGlowColor = $00FF9933;

var
  DEFAULT_REGISTRY: string;

type
  TisGlowBorder = class
  public
    class procedure Erase(AControl: TControl);
    class procedure Draw(AControl: TControl);
    class procedure DrawRadioGroup(AControl: TControl; RightTopStart: Integer);
  end;


  TwwReverseSelectionRecordEvent = procedure (Grid: TwwDBGrid;  var Accept: boolean) of object;

  IisDBFieldControl = interface
  ['{B584153D-3D98-40EF-AB2D-5C0B515AF797}']
    function GetDataSource: TDataSource;
    function GetFieldName: String;

    property DataSource: TDataSource read GetDataSource;
    property FieldName: String read GetFieldName;
  end;


  TMacro = class(TCollectionItem)
  private
    FName: string;
    FAsString: string;
    function IsNotEmpty: Boolean;
  public
    procedure Assign(Source: TPersistent); override;
    procedure Clear;
  published
    property Name: string read FName write FName;
    property AsString: string read FAsString write FAsString stored IsNotEmpty;
  end;

  TMacros = class(TCollection)
  private
    FOwner: TPersistent;
    function GetItem(Index: Integer): TMacro;
    procedure SetItem(Index: Integer; const Value: TMacro);
  protected
    function  GetOwner: TPersistent; override;
  public
    constructor Create(Owner: TPersistent); overload;
    function Add: TMacro;
    function MacroByName(const Value: string): TMacro;
    property Items[Index: Integer]: TMacro read GetItem write SetItem; default;
  end;

  TExtendedMemIniFile = class(TMemIniFile)
  public
    function KeyExists(const Section, Ident: String): Boolean;
  end;

  TIsLockedMemIniFile = class(TExtendedMemIniFile)
  private
    FSynchronizer: TMultiReadExclusiveWriteSynchronizer;
    FOwnedSynchronizer: Boolean;
    FOnUpdateFile: TNotifyEvent;
    FModified: Boolean;
  public
    constructor Create(const FileName: string; const ExternalSynchronizer: TMultiReadExclusiveWriteSynchronizer); overload;
    constructor Create(const FileName: string); overload;
    destructor Destroy; override;
    procedure Clear;
    procedure DeleteKey(const Section, Ident: String); override;
    procedure EraseSection(const Section: string); override;
    procedure GetStrings(List: TStrings);
    procedure ReadSection(const Section: string; Strings: TStrings); override;
    procedure ReadSections(Strings: TStrings); override;
    procedure ReadSectionValues(const Section: string; Strings: TStrings); override;
    function ReadString(const Section, Ident, Default: string): string; override;
    procedure Rename(const FileName: string; Reload: Boolean);
    procedure SetStrings(List: TStrings);
    procedure UpdateFile; override;
    procedure WriteString(const Section, Ident, Value: String); override;
    procedure Reload;
    procedure ClearModified;
    property OnUpdateFile: TNotifyEvent read FOnUpdateFile write FOnUpdateFile;
    property Modified: Boolean read FModified;
    property Synchronizer: TMultiReadExclusiveWriteSynchronizer read FSynchronizer;
  end;

  TIsLockedHistMemIniFile = class(TIsLockedMemIniFile)
  private
    FHistoryIniFile: TExtendedMemIniFile;
  public
    constructor Create(const FileName: string; const ExternalSynchronizer: TMultiReadExclusiveWriteSynchronizer); overload;
    constructor Create(const FileName: string); overload;
    destructor Destroy; override;
    procedure EraseSection(const Section: string); override;
    procedure DeleteKey(const Section, Ident: String); override;
    procedure WriteString(const Section, Ident, Value: String); override;
    procedure UpdateFile; override;
    property HistoryIniFile: TExtendedMemIniFile read FHistoryIniFile;
  end;


  TPanelClickEvent = procedure(Sender: TObject; PanelIndex: Integer) of object;

  TISStatusBar = class(TStatusBar, ILogicTesting)
  private
    FOnPanelDblClick: TPanelClickEvent;
    FOnPanelClick: TPanelClickEvent;
    procedure CNNotify(var Message: TWMNotify); message CN_NOTIFY;
    procedure WMPanelDblClick(var Message: TMessage); message WM_PANELDBLCLICK;
    procedure WMPanelClick(var Message: TMessage); message WM_PANELCLICK;
  protected
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  published
    property OnPanelDblClick: TPanelClickEvent read FOnPanelDblClick write FOnPanelDblClick;
    property OnPanelClick: TPanelClickEvent read FOnPanelClick write FOnPanelClick;
  end;

  TISToolBar = class(TToolBar);

  TISActionList = class(TActionList);

  TISToolButton = class(TToolButton, ILogicTesting)
  private
    FActionList: TISActionList;
    FAction: TAction;
    FShortCut: TShortCut;
    function GetShortCut: TShortCut;
    procedure SetShortCut(const Value: TShortCut);
    procedure ActionExecute(Sender: TObject);
    procedure SetVisible(Value: Boolean);
    procedure CMDialogChar(var Message: TCMDialogChar); message CM_DIALOGCHAR;
  protected
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Click; override;
  published
    property Visible write SetVisible;
    property ShortCut: TShortCut read GetShortCut write SetShortCut;
  end;

  TISCheckListBox = class(TCheckListBox, ILogicTesting)
  protected
    //These should be broken out into their own unit, TisUICheckListBox
    //but for speend, and not forcing Aleksey to have to add them in
    //they work just fine here for now.
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure SetBorder(AColor: TColor);
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;

    procedure ClickCheck; override;
  end;

  TISImageList = class(TImageList);

  TISRadioButton = class(TRadioButton, ILogicTesting)
  protected
    procedure SetChecked(Value: Boolean); override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISInplaceEdit = class(TInplaceEdit)
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  end;

  TSortStyle = (ssAutomatic, ssNormal, ssNumeric, ssNumericExtended, ssDateTime, ssTime, ssCustom);
  TSortCustom = function (const Str1, Str2: String): Integer;
  TSortDirection = (sdAscending, sdDescending);

  TSortOptions = class(TPersistent)
  private
    fSecondaryIndex: String;
    fCanSort: Boolean;
    fSortStyle: TSortStyle;
    fSortCaseSen: Boolean;
    fSortCol: Integer;
    fSortDirection: TSortDirection;
    fSortCustom: TSortCustom;
  public
    constructor Create;
    property SortCustom: TSortCustom read fSortCustom write fSortCustom;  //Used only if SortStyle = ssCustom.
  private
  published
    property CanSort: Boolean read fCanSort write fCanSort;
    property SecondaryIndex: String read fSecondaryIndex write fSecondaryIndex;
    property SortStyle: TSortStyle read fSortStyle write fSortStyle;
    property SortCaseSensitive: Boolean read fSortCaseSen write fSortCaseSen;
    property SortCol: Integer read fSortCol write fSortCol;
    property SortDirection: TSortDirection read fSortDirection write fSortDirection;
  end;

  TSortedListEntry = record
    Str: String;
    RowNum: integer;
    SortOption: TSortOptions;
  end;
  PSortedListEntry = ^TSortedListEntry;
  TSortedList = class(TList)
  public
    function GetItem(const I: Integer): PSortedListEntry;
    procedure Reset;
  end;

  TGetSortStyleEvent = procedure(Sender: TObject; Col: integer; var SortStyle: TSortStyle) of object;

  TISStringGrid = class(TStringGrid, ILogicTesting)
  private
    FMouseDownCol: Integer;
    fSortedList: TSortedList;
    fSorting: Boolean;
    FSortOptions: TSortOptions;
    fOnGetSortStyle: TGetSortStyleEvent;
    BitmapUp, BitmapDown: TBitmap;
    function ResizingCol(X, Y: integer): boolean;
  protected
    function CreateEditor: TInplaceEdit; override;
    procedure SetSortOptions(value: TSortOptions);
    function DetermineSortStyle(const ACol: integer): TSortStyle;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);override;
    procedure DrawCell(ACol, ARow: integer; ARect: TRect; AState: TGridDrawState); override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;

    procedure SetEditText(ACol, ARow: Longint; const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function AddLine: TStrings;
    function AddSortedLine(const Caption: string): TStrings;
    procedure DeleteLine(const Index: Integer);
    //Sort the column
    procedure SortColumn(ACol: integer);
    //The grid is being sorted
    property Sorting: Boolean read fSorting;
  published
    //Decide the sortstyle runtime
    property OnGetSortStyle: TGetSortStyleEvent read fOnGetSortStyle write fOnGetSortStyle;
    property SortOptions: TSortOptions read fSortOptions write SetSortOptions;
  end;

  TIsStringGridHwInfo = class(TIsHwInfoBase)
  public
    procedure ReportToStringGrid(const g: TISStringGrid);
  end;

  TISMemo = class(TMemo, ILogicTesting)
  protected
    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISRichEdit = class(TRichEdit, ILogicTesting)
  protected
    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISDBRichEdit = class(TwwDBRichEdit, ILogicTesting, IisDBFieldControl)
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;

    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISSpinEdit = class(TSpinEdit, ILogicTesting)
  protected
    //These should be broken out into their own unit, TisUIDBSpinEdit
    //but for speed, and not forcing Aleksey to have to add them in
    //they work just fine here for now.
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure SetBorder(AColor: TColor);
    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISRadioGroup = class(TRadioGroup, ILogicTesting)
  protected
    procedure Click; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISImage = class(TImage);

  TISDBImage = class(TisUIDBImage, IisDBFieldControl)
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;
  end;


  TISListView = class(TListView, ILogicTesting)
  private
    FUpdateCount: Integer;
    procedure CNNotify(var Message: TWMNotify); message CN_NOTIFY;
  protected
    procedure Change(Item: TListItem; Change: Integer); override;

  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  public
    procedure BeginUpdate;
    procedure EndUpdate;
    function  IsUpdating: Boolean;
  published
    property OnCreateItemClass;  
  end;

  TISTreeView = class(TTreeView, ILogicTesting)
  protected
    procedure Change(Node: TTreeNode); override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISDBTreeView = class;

  TISTreeViewDataLink = class(TDataLink)
  private
    FCaptionField: string;
    FUplinkField: string;
    FKeyField: string;
    FTreeView: TISDBTreeView;
  protected
    procedure ActiveChanged; override;
    procedure RecordChanged(Field: TField); override;
  public
    property KeyField: string read FKeyField write FKeyField;
    property CaptionField: string read FCaptionField write FCaptionField;
    property UplinkField: string read FUplinkField write FUplinkField;
    property TreeView: TISDBTreeView read FTreeView write FTreeView;
  end;

  TISDBTreeView = class(TISTreeView)
  private
    FDataLink: TISTreeViewDataLink;
    FSuppressChangeEvent: Boolean;
    function GetDataSource: TDataSource;
    procedure SetDataSource(const Value: TDataSource);
    procedure ActiveChange(Sender: TObject);
    procedure UpdateCurrentNode;
    procedure ChangeCurrentNode;
    function GetCaptionField: string;
    function GetKeyField: string;
    function GetUpLinkField: string;
    procedure SetCaptionField(const Value: string);
    procedure SetKeyField(const Value: string);
    procedure SetUpLinkField(const Value: string);
  protected
    procedure Delete(Node: TTreeNode); override;
    procedure Change(Node: TTreeNode); override;
    function CanCollapse(Node: TTreeNode): Boolean; override;
    procedure DragOver(Source: TObject; X, Y: Integer; State: TDragState;
      var Accept: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DragDrop(Source: TObject; X, Y: Integer); override;
    procedure RebuildTreeView;
    function LocateCurrentRecordNode: TTreeNode;
    function FindRecordedNode(const Id: Variant): TTreeNode;
    function AddRecordedNode(const Caption: string; const Uplink, Id: Variant): TTreeNode;
  published
    property CaptionField: string read GetCaptionField write SetCaptionField;
    property KeyField: string read GetKeyField write SetKeyField;
    property UpLinkField: string read GetUpLinkField write SetUpLinkField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
  end;

  TISProvider = class(TComponent)
  private
    FDataSet: TISBasicClientDataSet;
    function GetData: Variant;
  public
    property Data: Variant read GetData;
  published
    property DataSet: TISBasicClientDataSet read FDataSet write FDataSet;
  end;

  TISEdit = class(TisUIEdit, ILogicTesting)
  protected
    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISDBSpinEdit = class(TwwDBSpinEdit, ILogicTesting, IisDBFieldControl)
  private
    //These should be broken out into their own unit, TisUIDBSpinEdit
    //but for speend, and not forcing Aleksey to have to add them in
    //they work just fine here for now.
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure SetBorder(AColor: TColor);
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;
  
    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISDBMemo = class(TisUIDBMemo, ILogicTesting, IisDBFieldControl)
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;

    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISBevel = class(TBevel);

  TISCheckBox = class(TCheckBox, ILogicTesting)
  protected
    procedure SetChecked(Value: Boolean); override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISComboBox = class(TComboBox, ILogicTesting)
  protected
    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISSplitter = class(TSplitter);

  TISDBComboDlg = class(TwwDBComboDlg, ILogicTesting, IisDBFieldControl)
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;
  
    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISDBComboBox = class(TisUIwwDBComboBox, ILogicTesting, IisDBFieldControl)
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;

    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property ShowMatchText default True;
  end;

  TISDataSource = class(TwwDataSource);

  TISDBLookupComboDlg = class(TwwDBLookupComboDlg, ILogicTesting, IisDBFieldControl)
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;
  
    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISDBLookupCombo = class(TisUIwwDBLookupCombo, ILogicTesting, IisDBFieldControl)
  private
    F: TNotInListEvent;
    fc: Integer;
    SortIndex: string;
    FFieldLinkActive: procedure (Sender: TObject) of object;
    FGlowing: Boolean;
    procedure EvFieldLinkActive(Sender: TObject);
    procedure CustomNotInList(Sender: TObject; LookupTable: TDataSet; NewValue: string; var Accept: boolean);
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;

    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;

    function  FindRecord(KeyValue: string; LookupField: string; MatchType: TwwLocateMatchType;
                        CaseSensitive: boolean; PerformLookup: boolean = False): boolean; override;
  public
    procedure CloseUp (modified: boolean); override;
    procedure DropDown; override;                     
    constructor Create(AOwner: TComponent); override;
    Property Glowing: Boolean read FGlowing write FGlowing;
  published
    property OnSetLookupText;  
  end;

  TISDBRadioGroup = class(TDBRadioGroup, ILogicTesting, IisDBFieldControl)
  private
    FTabStop: Boolean;
    FInClick: Boolean;
    procedure CheckFocus;
    procedure ISDataChange(Sender: TObject);
    function GetButtonValue(Index: Integer): string;
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;
  
    procedure Click; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;

    procedure Change; override;
    procedure DoEnter; override;
    procedure DoExit; override;
    function CanModify: Boolean; override;
  public
    procedure UpdateRecord;
    procedure SetFocus; override;
    constructor Create(AOwner: TComponent); override;
  published
    property TabStop default True;
  end;

  TISGroupBox = class(TGroupBox);

  TISPopupMenu = class(TPopupMenu);

  TISDBEdit = class(TisUIwwDBEdit, ILogicTesting, IisDBFieldControl)
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;

    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  end;

  TISDBText = class(TDBText, IisDBFieldControl)
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;
  end;

  TOnFilterChange = procedure (Sender: TObject; DataSet: TDataSet; NewFilter: string) of object;
  TISIniAttributes = class(TwwIniAttributes)
  public
    constructor Create;
  published
    property SaveToRegistry default True;
    property CheckNewFields default True;
    property Enabled default True;
  end;


  TOnCreatingSortIndexEvent = procedure (Sender: TObject; var AFields: string; var AOptions: TIndexOptions; var ADescFields: string) of object;

  TISDBGrid = class(TwwDBGrid, ILogicTesting)
  private
    DesignSelected: TStrings;
    FilterBmp, FilteredBmp: TBitmap;
    FSorting: Boolean;
    FFilterDlg: Boolean;
    FLocateDlg: Boolean;
    FLastClickX: integer;
    FLastClickY: integer;
    FFirstButtonClick: TNotifyEvent;
    FNextButtonClick: TNotifyEvent;
    FOnFilterChange: TOnFilterChange;
    FFocusColorEnabled : boolean;
    FDefaultSort: string;
    FFieldNameComboBoxChange: TNotifyEvent;
    FOnSelectionChanged : TNotifyEvent;
    FRecordCountHintEnabled: boolean;
    testInSelection: Boolean;
    FOldIndexFieldNames: String;
    FNoFire: Boolean;  //added to prevent a second CTRL-E event
    FOnCreatingSortIndex: TOnCreatingSortIndexEvent;
    FOnSortingChanged: TNotifyEvent;
    procedure SetSorting(const Value: Boolean);
    procedure SetFilterDlg(const Value: Boolean);
    procedure IndicatorButtonClick(Sender: TObject);
    procedure CloseDlgQuery(Sender: TObject; var CanClose: Boolean);
    procedure InitDialog(Dialog: TwwLocateDlg);
    function  IsDefaultSort: Boolean;
    procedure FirstButtonClick(Sender: TObject);
    procedure NextButtonClick(Sender: TObject);
    procedure FieldNameComboBoxChange(Sender: TObject);
    procedure DoOnSelectionChange;
    function  CreateSortingIndex(AFields: string; AOptions: TIndexOptions; ADescFields: string): Boolean;
    function  FindKeyFields: string;
    procedure testNotifySelectedChange(const old, new: TList);
    procedure CMHintShow(var Message: TMessage); message CM_HINTSHOW;
  protected
    FTitleImageList: TImageList;
    FShift: TShiftState;
    FUseCaseInsensitiveFilterExpression: Boolean;

    procedure LayoutChanged; override;
    procedure LinkActive(Value: Boolean); override;
    procedure DoCalcTitleImage(Sender: TObject; Field: TField;
         var TitleImageAttributes: TwwTitleImageAttributes); override;
    procedure DoTitleButtonClick(AFieldName: string); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    procedure Loaded; override;
    procedure DataChanged; override;
    function  MakeInfoHint: string; virtual;
    procedure CreateWnd; override;
    function  IsFiltered: Boolean; virtual;
    procedure UpdateIndicatorButton;

    procedure SelectRecordRange(bkmrk1, bkmrk2: TBookmark); override;
    procedure RemoveSelected(bkmrk1, bkmrk2: TBookmark); override;
    procedure DoRowChanged; override;

  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  public
    procedure SelectRecord; override;
    procedure SelectAll; reintroduce; virtual;
    procedure UnselectRecord; override;
    Procedure UnselectAll; override;
    Procedure LoadFromIniFile; override;
    Procedure SaveToIniFile; override;
    procedure ColEnter; override;
    procedure DoCalcTitleAttributes(AFieldName: string; AFont: TFont; ABrush: TBrush;
	     var FTitleAlignment: TAlignment); override;
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    function LastClickWasOnData: boolean;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DoCalcCellColors(Field: TField; State: TGridDrawState;
	     highlight: boolean; AFont: TFont; ABrush: TBrush); override;
    procedure ExecLocateDlg;
    procedure ExecHideDlg;
  published
    property DefaultSort: string read FDefaultSort write FDefaultSort stored IsDefaultSort;
    property TitleButtons default True;
    property Options default [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines,
      dgRowLines, dgConfirmDelete, dgCancelOnExit,
      dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing];
    property LocateDlg: Boolean read FLocateDlg write FLocateDlg default True;
    property FilterDlg: Boolean read FFilterDlg write SetFilterDlg default True;
    property Sorting: Boolean read FSorting write SetSorting default True;
    property MultiSelectOptions default [msoAutoUnSelect, msoShiftSelect];
    property OnFilterChange: TOnFilterChange read FOnFilterChange write FOnFilterChange;
    property FocusColorEnabled : Boolean read FFocusColorEnabled write FFocusColorEnabled default True;
    property RecordCountHintEnabled: boolean read FRecordCountHintEnabled write FRecordCountHintEnabled default true;
    property OnSelectionChanged : TNotifyEvent read FOnSelectionChanged write FOnSelectionChanged;
    property NoFire: Boolean read FNoFire write FNoFire;
    property OnCreatingSortIndex: TOnCreatingSortIndexEvent read FOnCreatingSortIndex write FOnCreatingSortIndex;
    property OnSortingChanged: TNotifyEvent read FOnSortingChanged write FOnSortingChanged;
  end;

  TISSpeedButton = class(TisUISpeedButton, ILogicTesting)
  private
    FCustomEnabled: Boolean;
    FEnabled: Boolean;
    FActionList: TActionList;
    FAction: TAction;
    FShortCut: TShortCut;
    procedure SetCustomEnabled(const Value: Boolean);
    procedure SetMyEnabled(const Value: Boolean);
    function GetShortCut: TShortCut;
    procedure SetShortCut(const Value: TShortCut);
    procedure ActionExecute(Sender: TObject);
    procedure SetVisible(Value: Boolean);
    procedure CMDialogChar(var Message: TCMDialogChar); message CM_DIALOGCHAR;
  protected
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Click; override;
  published
    property CustomEnabled: Boolean write SetCustomEnabled stored False;
    property Enabled: Boolean read FEnabled write SetMyEnabled default True;
    property Visible write SetVisible;
    property ShortCut: TShortCut read GetShortCut write SetShortCut;
  end;

  TISDBCheckGrid = class(TISDBGrid)
  private
    FSelIndictorButton: TSpeedButton;
    FSelPopupMenu: TPopupMenu;
    FPrevOnFilter: TFilterRecordEvent;
    FOldFiltered: Boolean;
    FFilterSetup: Boolean;
    FShowSelectedOnly: Boolean;
    FSelRecs: TList;
    FFltRecNo: Integer;
    FGrayMode: Boolean;
    FOnReverseSelectionRecords : TwwReverseSelectionRecordEvent;

    procedure FDropMenuDown(Sender: TObject);
    procedure FSelectAll(Sender: TObject);
    procedure FUnSelectAll(Sender: TObject);
    procedure FReverseSelection(Sender: TObject);
    procedure FShowSelection(Sender: TObject);
    procedure FilterSelection(DataSet: TDataSet; var Accept: Boolean);
    procedure ConvertBookMarksIntoRecNo(ARunFilting: Boolean);
    function  GetMultiselection: Boolean;
    procedure SetMultiselection(const Value: Boolean);
    procedure SetGrayMode(const Value: Boolean);

  protected
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    function  HighlightCell(DataCol, DataRow: Integer; const Value: string; AState: TGridDrawState): Boolean; override;
    procedure DrawSelCheckBox(ARect: TRect; ACol, ARow: integer; val: boolean);
    function  MakeInfoHint: string; override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure SelectAll; reintroduce; virtual;
    procedure UnSelectAll; override;
    procedure ReverseSelection; virtual;
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    property  MultiSelection: Boolean read GetMultiselection write SetMultiselection;
    property  GrayMode: Boolean read FGrayMode write SetGrayMode;
  published
    property OnMultiSelectReverseRecords : TwwReverseSelectionRecordEvent read FOnReverseSelectionRecords write FOnReverseSelectionRecords;

  end;

  TISBitBtn = class(TisUILMDButton, ILogicTesting)
  protected
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  public
    procedure Click; override;
  end;

  TISButton = class(TisUILMDButton, ILogicTesting)
  private
    FCustomEnabled: Boolean;
    FEnabled: Boolean;
    procedure SetCustomEnabled(const Value: Boolean);
    procedure SetMyEnabled(const Value: Boolean);
  protected
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Click; override;
  published
    property CustomEnabled: Boolean write SetCustomEnabled stored False;
    property Enabled: Boolean read FEnabled write SetMyEnabled default True;
  end;

  TISPageControl = class(TPageControl, ILogicTesting)
  private
    procedure SetActivePage(Page: TTabSheet);
    procedure EnableSpeedButtons;
  protected
    procedure Change; override;
    procedure ShowControl(AControl: TControl); override;
  {ILogicTesting}
    function testGetControlName: string; virtual;

    //  If the property TabVisible of a TabSheet is changed when the PageControl,
    //  for whatever reason, has no handle, a "List Index Out of Bounds" will be raised.
    //  solution   overwrite TPagecontrol.GetImageindex
    //  http://qc.embarcadero.com/wc/qcmain.aspx?d=5936
    function GetImageIndex(TabIndex: Integer): Integer; override;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  public
    procedure ActivatePage(Page: TTabSheet);
    procedure SetTabVisible(const t: TTabSheet; const Value: Boolean);
  published
    property ActivePage write SetActivePage;
  end;

  TISLabel = class(TLabel)
  private
    FCustomEnabled: Boolean;
    FEnabled: Boolean;
    procedure SetCustomEnabled(const Value: Boolean);
    procedure SetMyEnabled(const Value: Boolean);
  public
    constructor Create(AOwner: TComponent); override;
  published
    property CustomEnabled: Boolean write SetCustomEnabled stored False;
    property Enabled: Boolean read FEnabled write SetMyEnabled default True;
  end;

  TISTabControl = class(TTabControl, ILogicTesting)
  private
    procedure CMDialogChar(var Message: TCMDialogChar); message CM_DIALOGCHAR;
  protected
    procedure SetTabIndex(Value: Integer); override;
    procedure CreateParams(var Params: TCreateParams); override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  public
    procedure UpdateTabImages;
  end;

  TISPanel = class(TPanel)
  protected
    procedure SetEnabled(Value: Boolean); override;
  end;

  TISDBDateTimePicker = class(TisUIwwDBDateTimePicker, ILogicTesting, IisDBFieldControl)
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;

    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  public
    constructor Create(AOwner: TComponent); override;
  end;

  TISDateTimePicker = class(TDateTimePicker, ILogicTesting)
  private
    FCustomEnabled: Boolean;
    FEnabled: Boolean;
    procedure SetCustomEnabled(const Value: Boolean);
    procedure SetMyEnabled(const Value: Boolean);
  protected
    procedure Change; override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property CustomEnabled: Boolean write SetCustomEnabled stored False;
    property Enabled: Boolean read FEnabled write SetMyEnabled default True;
  end;

  TISDBCheckBox = class(TDBCheckBox, ILogicTesting, IisDBFieldControl)
  private
    FOnChange: TNotifyEvent;
  protected
    //IisDBFieldControl
    function GetDataSource: TDataSource;
    function GetFieldName: String;

    procedure SetChecked(Value: Boolean); override;
  {ILogicTesting}
    function testGetControlName: string; virtual;
    function testGetUserModifiableProps: TtestParamList; virtual;
    function testGetAvailableProps: TtestParamList; virtual;
    function testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant; virtual;
    procedure testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer); virtual;
    function testGetUserActions: TtestParamList; virtual;
    function testGetActionParamList(const ActionName: string): TtestParamList; virtual;
    procedure testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant); virtual;

    procedure Toggle; override;
  public
    procedure UpdateRecord;
  published
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

  TISLocateDialog = class(TwwLocateDialog)
  protected
    function GetFieldNameFromTitle(fieldTitle: string): string;
  public
    function Execute: boolean; override;
  end;

  TisWebBrowser = class(TWebBrowser)
  private
    procedure CMDialogChar(var Message: TCMDialogChar); message CM_DIALOGCHAR;
  end;


function ExtendedCompare(const Str1, Str2: String): Integer;

procedure Register;

implementation

{$R *.RES}

uses
  ISUtils, SFilterDialog, SShowColumnsInGrid, SIbProviders, wwpaintoptions;


procedure Register;
begin
  RegisterComponents('ISClasses', [
      TISActionList,
      TISPopupMenu,

      TISPanel,
      TISUIFashionPanel,
      TISGroupBox,
      TISBevel,
      TISSplitter,
      TISPageControl,
      TISTabControl,

      TISToolBar,
      TISToolButton,
      TISSpeedButton,
      TISBitBtn,
      TISButton,
      TISStatusBar,

      TISLabel,
      TISDBText,

      TISCheckListBox,
      TISStringGrid,
      TISDBGrid,
      TISDBCheckGrid,

      TISCheckBox,
      TISDBCheckBox,
      TISComboBox,
      TISDBComboBox,
      TISDBComboDlg,
      TISDBLookupComboDlg,
      TISDBLookupCombo,
      TISLocateDialog,

      TISMemo,
      TISDBMemo,
      TISRichEdit,
      TISDBRichEdit,
      TISEdit,
      TISDBEdit,
      TISSpinEdit,
      TISDBSpinEdit,
      TISDateTimePicker,
      TISDBDateTimePicker,
      TisWebBrowser,

      TISRadioButton,
      TISRadioGroup,
      TISDBRadioGroup,

      TISImage,
      TISDBImage,

      TISListView,
      TISTreeView,
      TISDBTreeView,

      TISDataSource,
      TISBasicClientDataSet,
      TISProvider
  ]);
end;



{ TISStatusBar }

procedure TISStatusBar.CNNotify(var Message: TWMNotify);
begin
  inherited;
  with Message do
  begin
    case NMHdr^.code of
      NM_DBLCLK:
        with PNMMouse(NMHdr)^ do
        begin
          Result := 1;
          PostMessage(Handle, WM_PANELDBLCLICK, Integer(dwItemSpec), 0);
        end;
      NM_CLICK:
        with PNMMouse(NMHdr)^ do
        begin
          Result := 1;
          PostMessage(Handle, WM_PANELCLICK, Integer(dwItemSpec), 0);
        end;
    end;
  end;
end;

function TISStatusBar.testGetActionParamList(const ActionName: string): TtestParamList;
begin
  SetLength(Result, 1);
  Result[0].testParamName := 'PanelIndex';
  Result[0].testParamType := tkInteger;
  Result[0].testParamDesc := 'Index of clicked panel';
end;

function TISStatusBar.testGetAvailableProps: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISStatusBar.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISStatusBar.testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant;
begin
  Result := null;
end;

function TISStatusBar.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 2);

  Result[0].testParamName := 'Click';
  Result[0].testParamDesc := 'Click action';
  Result[0].testParamIndexes := 1;

  Result[1].testParamName := 'DblClick';
  Result[1].testParamDesc := 'Double click action';
  Result[1].testParamIndexes := 1;
end;

function TISStatusBar.testGetUserModifiableProps: TtestParamList;
begin
  Result := testGetAvailableProps;
end;

procedure TISStatusBar.testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant);
var
  msg: TMessage;
begin
  if SameText(ActionName, FindActionByIndex(Self, 0)) then
  begin
    msg.Msg := WM_PANELCLICK;
    msg.WParam := ActionParams[0];
    WMPanelClick(msg);
  end
  else if SameText(ActionName, FindActionByIndex(Self, 1)) then
  begin
    msg.Msg := WM_PANELDBLCLICK;
    msg.WParam := ActionParams[0];
    WMPanelDblClick(msg);
  end;
end;

procedure TISStatusBar.testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

procedure TISStatusBar.WMPanelClick(var Message: TMessage);
begin
  inherited;
  if Assigned(FOnPanelClick) then
    FOnPanelClick(Self, Message.wParam);
  testNotifyUserAction(Self, FindActionByIndex(Self, 0), [Message.wParam]);
end;

procedure TISStatusBar.WMPanelDblClick(var Message: TMessage);
begin
  inherited;
  if Assigned(FOnPanelDblClick) then
    FOnPanelDblClick(Self, Message.wParam);
  testNotifyUserAction(Self, FindActionByIndex(Self, 1), [Message.wParam]);
end;


{ TISToolButton }

procedure TISToolButton.ActionExecute(Sender: TObject);
begin
  Click;
end;

procedure TISToolButton.Click;
begin
  if Enabled and Visible then
  begin
    inherited;
    testNotifyUserAction(Self, FindActionByIndex(Self, 0), []);
  end;
end;

procedure TISToolButton.CMDialogChar(var Message: TCMDialogChar);
begin
  // Fixing VCL bug, which does not check ALT key
  if (Message.KeyData and $20000000) = 0 then
    Message.Result := 0 // Ignore the key
  else
    inherited;  // Try to process a shortcut
end;

constructor TISToolButton.Create(AOwner: TComponent);
begin
  inherited;
  if not (csDesigning in ComponentState) then
  begin
    FActionList := TISActionList.Create(AOwner);
    FAction := TAction.Create(FActionList);
    FAction.ActionList := FActionList;
    FAction.OnExecute := ActionExecute;
  end;
end;

function TISToolButton.GetShortCut: TShortCut;
begin
  if (csDesigning in ComponentState) then
    Result := FShortCut
  else
    Result := FAction.ShortCut;
end;

procedure TISToolButton.SetShortCut(const Value: TShortCut);
begin
  if (csDesigning in ComponentState) then
    FShortCut := Value
  else
    FAction.ShortCut := Value;
end;

procedure TISToolButton.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if not (csDesigning in ComponentState) then
    FAction.Enabled := inherited Enabled and Visible;
end;

function TISToolButton.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISToolButton.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;
end;

function TISToolButton.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISToolButton.testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant;
begin
  Result := GetPropValue(Self, PropName, False);
end;

function TISToolButton.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0].testParamName := 'Click';
  Result[0].testParamDesc := 'Click action';
end;

function TISToolButton.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 0);
end;

procedure TISToolButton.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  Click;
end;

procedure TISToolButton.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

{ TISInplaceEdit }

procedure TISInplaceEdit.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.Style := Params.Style and not ES_MULTILINE;
end;

{ TISStringGrid }

constructor TISStringGrid.Create(AOwner: TComponent);
begin
  inherited;
  fMouseDownCol := -1;
  fSortedList := TSortedList.Create;
  FSortOptions := TSortOptions.Create;
  BitMapUp := TBitMap.Create;
  BitMapDown := TBitMap.Create;
  BitMapUp.LoadFromResourceName(HInstance,'UP');
  BitMapDown.LoadFromResourceName(HInstance,'DOWN');
end;

destructor TISStringGrid.Destroy;
begin
  BitmapUp.Free;
  BitmapDown.Free;
  fSortedList.Free;
  FSortOptions.Free;
  inherited;
end;

function TISStringGrid.AddLine: TStrings;
begin
  RowCount := RowCount+ 1;
  Result := Rows[RowCount-1];
end;

function TISStringGrid.AddSortedLine(const Caption: string): TStrings;
var
  i, j: Integer;
  l: TStrings;
begin
  i := 1;
  l := Cols[0];
  if (RowCount = 2) and (Cells[0, 1] = '') then
    Result := Rows[1]
  else
  begin
    while (i < l.Count) and (AnsiCompareText(Caption, l[i]) > 0) do
      Inc(i);
    RowCount := RowCount+ 1;
    for j := RowCount-1 downto i+1 do
      Rows[j].Assign(Rows[j-1]);
    Result := Rows[i];
  end;
  Result.Text := Caption;
end;

function TISStringGrid.CreateEditor: TInplaceEdit;
begin
  Result := TISInplaceEdit.Create(Self);
end;

procedure TISStringGrid.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
var
  ARow,ACol: integer;
begin
  inherited;
  MouseToCell(X,Y,ACol,ARow);
  if (ARow = 0) and (ACol = fMouseDownCol) and not ResizingCol(X,Y) then
  begin
    if (SortOptions.SortDirection = sdAscending) and (SortOptions.fSortCol = ACol) then
      SortOptions.fSortDirection := sdDescending
    else
      SortOptions.fSortDirection := sdAscending;
    if Button = mbRight then
      SortOptions.fSortDirection := sdDescending;

    SortColumn(ACol);
    Exit;
  end;
end;

procedure TISStringGrid.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
var
    ARow, ACol: Integer;
begin
  inherited;
    MouseToCell(x,y,ACol,ARow);
    if ARow = -1 then ARow := RowCount-1;

    fMouseDownCol := ACol;//If col changes betweem mouse Up&Down, don't sort, bacause col has been moved
end;

procedure TISStringGrid.DeleteLine(const Index: Integer);
var
  j: Integer;
begin
  if RowCount = 2 then
    Rows[1].Clear
  else
  begin
    for j := Index+1 to RowCount-1 do
      Rows[j-1].Assign(Rows[j]);
    RowCount := RowCount- 1;
  end;
end;

function TISStringGrid.ResizingCol(X, Y: integer): boolean;
var
  ACol,ARow: Integer;
begin
  //If cursor inside +/- 5 pixels of a cell edge it is resizing the col
  MouseToCell(X,Y,ACol,ARow);
  Result := ((CellRect(ACol,0).Left-5  <= X) and (CellRect(ACol,0).Left+5  >= X) or
             (CellRect(ACol,0).Right-5 <= X) and (CellRect(ACol,0).Right+5 >= X) ) and (ARow=0);
end;

procedure TISStringGrid.DrawCell(ACol, ARow: integer; ARect: TRect;
  AState: TGridDrawState);
var
  xOffset: Integer;
  Width: Integer;
  Text: String;
  I: Integer;
begin
  xOffset := 0;
  inherited;
  Text := Cells[ACol, ARow];
  //Text displayed in Header
  if ARow = 0 then
  begin
    Width := ColWidths[ACol]-1;
    if (SortOptions.fSortCol = ACol) and FSortOptions.FCanSort then//Use Offset for up/down arrow IF the col is sorted
        Dec(Width,11);//width of up/down bitmap
    if Canvas.TextWidth(Text) > Width then
    begin
      Text[Length(Text)-1] := '.';
      Text[Length(Text)] := '.';
      while (Canvas.TextWidth(Text) > Width) and (Length(Text) > 3) do
      begin
          Delete(Text, Length(Text), 1);
          Text[Length(Text)-1] := '.';
      end;
    end;

    //Draw Up/Down Bitmap
    if (SortOptions.fSortCol = ACol) and FSortOptions.FCanSort then
    begin
        if ARow = 0 then
            Inc(xOffset,11); //width of up/down bitmap
        //Width ~ a X-OffSet value, so Up/Down Bitmap fits, when HeaderWidth less that Bitmap.width
        if ARect.Right-ARect.Left > 10 then Width := 8
        else Width := ARect.Right-ARect.Left-3;
        //I ~ an Y-Offset value for Up/Down Bitmap
        I := (RowHeights[0] div 2) -4;

        Canvas.TextRect(ARect, ARect.Left+xOffset, ARect.Top, Text);
        if SortOptions.SortDirection = sdAscending then
            Canvas.CopyRect(Rect(ARect.Left+3,I,ARect.Left+Width+3,I+7), BitmapUp.Canvas, Rect(0,0,Width,7))
        else
            Canvas.CopyRect(Rect(ARect.Left+3,I,ARect.Left+Width+3,I+7), BitmapDown.Canvas, Rect(0,0,Width,7));
    end
    else
      Canvas.TextRect(ARect, ARect.Left+2, ARect.Top, Text);
  end;

end;

{*******Sort*******************************************************************}

procedure TISStringGrid.SetSortOptions(value: TSortOptions);
begin
 fSortOptions.Assign(value);
end;

function TSortedList.GetItem(const i: Integer): PSortedListEntry;
begin
  //Cast the pointer.
  Result:=PSortedListEntry(Items[i]);
end;

procedure TSortedList.Reset;
var
  I: Integer;
begin
     //Dispose of anything in the list first.
     for i:=0 to Count-1 do
         if Items[i] <> nil then
            Dispose(PSortedListEntry(Items[i]));
     //Now clear the list.
     Clear;
end;

function TISStringGrid.DetermineSortStyle(const ACol: integer): TSortStyle;
var
  i: Integer;
  DoNumeric, DoDateTime, DoTime: Boolean;
begin
  DoNumeric:=True;
  DoDateTime:=True;
  DoTime:=True;

  //Note: We only go through the rows once.
  //This code depends on the fact that no
  //entry can be both a date and number.
  for i:=1 to RowCount-1 do
  begin
    if DoNumeric then
    begin
      try
        StrToFloat(Cells[ACol, i]);
      except
        on EConvertError do DoNumeric:=False;
      end;
    end;

    if DoTime then
    begin
      try
        StrToTime(Cells[ACol, i]);
      except
        on EConvertError do DoTime:=False;
      end;
    end;

    if DoDateTime then
    begin
      try
        StrToDateTime(Cells[ACol, i]);
      except
        on EConvertError do DoDateTime:=False;
      end;
    end;
  end;

  if DoNumeric then Result := ssNumeric
  else if DoDateTime then Result := ssDateTime
  else if DoTime then Result := ssTime
  else Result := ssNormal;
end;

function NormalCompare(const Str1, Str2: String): Integer;
begin
  Result := StrComp(PChar(Str1), PChar(Str2));
end;

function DateTimeCompare(const Str1, Str2: String): Integer;
var
  Val1, Val2: TDateTime;
begin
  try
    Val1:=StrToDateTime(Str1);
    Val2:=StrToDateTime(Str2);
    if Val1 < Val2 then Result:=-1
    else if Val2 < Val1 then Result:=1
    else Result:=0;
  except
    Result:= NormalCompare(Str1, Str2);
  end;
end;

function TimeCompare(const Str1, Str2: String): Integer;
var
  Val1, Val2: TDateTime;
begin
  try
    Val1:=StrToTime(Str1);
    Val2:=StrToTime(Str2);
    if Val1 < Val2 then Result:=-1
    else if Val2 < Val1 then Result:=1
    else Result:=0;
   except
     Result:= NormalCompare(Str1, Str2);
   end;
end;

function NumericCompare(const Str1, Str2: String): Integer;
var
  Val1, Val2: Extended;
begin
  try
    Val1:=StrToFloat(Str1);
    Val2:=StrToFloat(Str2);
    if Val1 < Val2 then Result:=-1
    else if Val2 < Val1 then Result:=1
    else Result:=0;
  except
    Result:= NormalCompare(Str1, Str2);
  end;
end;

function Compare(Item1, Item2: Pointer): Integer;
var
  Entry1, Entry2: PSortedListEntry;
begin
  Entry1 := Item1;
  Entry2 := Item2;

  //Handle Case-Insensitivity.
  if Entry1^.SortOption.fSortCaseSen = False then
  begin
    Entry1^.Str := Lowercase(Entry1^.Str);
    Entry2^.Str := Lowercase(Entry2^.Str);
  end;

  //Determine compare type and do the comparison.
  case Entry1^.SortOption.SortStyle of
      ssNumericExtended: Result:=ExtendedCompare(Entry1^.Str, Entry2^.Str);
      ssNumeric: Result:=NumericCompare(Entry1^.Str, Entry2^.Str);
      ssDateTime: Result:=DateTimeCompare(Entry1^.Str, Entry2^.Str);
      ssTime: Result:=TimeCompare(Entry1^.Str, Entry2^.Str);
      ssCustom: Result:=Entry1^.SortOption.SortCustom(Entry1^.Str, Entry2^.Str)
    else
      Result:=NormalCompare(Entry1^.Str, Entry2^.Str);
  end;

  //Now, make sure we don't swap the rows if the Keys are equal.
  //If they're equal then we sort by row number.
  if Result = 0 then
  begin
    if Entry1^.RowNum < Entry2^.RowNum then Result:=-1
    else if Entry1^.RowNum > Entry2^.RowNum then Result:=1
    else Result:=0; //Sometimes an item does get compared to itself.
  end
    else //Reverse polarity if descending sort.
    if Entry1^.SortOption.SortDirection = sdDescending then
      Result:=-1*Result;
end;

function ExtendedCompare(const Str1, Str2: String): Integer;
var
  St1,St2, S1,S2: String;
  i,len, iPos1,iPos2: Integer;
begin
  S1 := Str1;
  S2 := Str2;

  if (S1 = '') or (S2 = '') then
  begin
    Result := -1;
    Exit;
  end;

  ///***Deleting first chars if equal and not numbers
  len := Length(Str1);
  if Length(Str2) < len then len := Length(Str2);//finder korteste string

  S1 := '';
  S2 := '';
  for I := 1 to Len do//Copy all non equal chars to S and S2, but all numbers
    if (Str1[i] <> Str2[i]) or (Str1[i] in ['0'..'9']) then
    begin
      if i = Len then
      begin
         S1 := S1+Copy(Str1,i,length(Str1));
         S2 := S2+Copy(Str2,i,length(Str2));
      end
      else
      begin
         S1:= S1+Str1[i];
         S2:= S2+Str2[i];
      end;
    end;

  ///***Deleting last chars if equal and not numbers
  St1 := S1;
  St2 := S2;
  len := Length(St1);
  if Length(St2) < len then len := Length(St2);//finder korteste string

  iPos1 := Length(St1);
  iPos2 := Length(St2);
  for i := 0 to Len-1 do//Copy all non equal chars to S and S2, but all numbers
    if (St1[iPos1-i] = St2[iPos2-i]) and not (St1[iPos1-i] in ['0'..'9']) then
    begin
      delete(S1, Length(S1),1);
      delete(S2, Length(S2),1);
    end;

  try
    if S1 = S2 then Result := 0
    else if StrToInt(S1) < StrToInt(S2) then Result:=-1
    else Result:=1;
  except
    Result:= NormalCompare(S1, S2);
  end;
end;

//Sorts the variable rows using Column ACol as a key
procedure TISStringGrid.SortColumn(ACol: integer);
var
  I,I2,iRow,iCol: Integer;
  Item: PSortedListEntry;
  BufferGrid, BufferGrid2: array of TStrings;//add to dyn array, because faster than to cells property
  StringList: TStringList;
  SortStyle: TSortStyle;
begin
  if not FSortOptions.FCanSort then Exit;//Don't sort

  SortOptions.fSortCol := ACol;//set before so it can be changed in OnBeginSort if needed
  fSorting:=True;//Sorting

  StringList := TStringList.Create;
  StringList.CommaText := SortOptions.fSecondaryIndex;
  if StringList.IndexOf(IntToStr(ACol)) > -1 then
    StringList.Delete( StringList.IndexOf(IntToStr(ACol)) );//don't sort col twice
  StringList.Insert(0, IntToStr(ACol));

  //copy grid to dynamic grid, faster
  SetLength(BufferGrid,RowCount);
  SetLength(BufferGrid2,RowCount);
  for iRow := 0 to RowCount-1 do
  begin
    BufferGrid[iRow] := TStringList.Create;
    BufferGrid[iRow].Assign( Rows[iRow] );
    BufferGrid2[iRow] := TStringList.Create;
  end;

  for I2 := StringList.Count-1 downto 0 do//Loop to sort all columns
  begin
    iCol := StrToInt(StringList[I2]);

    fSortedList.Reset;
    //Insert the Row Number and Key (Str) into tList class
    for I:=1 to RowCount-1 do
    begin
      New(Item);
      Item^.Str := BufferGrid[i][iCol];
      Item^.RowNum := I;
      Item^.SortOption := SortOptions;

      fSortedList.Add(Item);
    end;

    if Assigned(fOnGetSortStyle) then
    begin
        fOnGetSortStyle(Self, iCol, SortStyle);//Get sortstyle via OnEvent
        SortOptions.fSortStyle := SortStyle;
    end
    else if SortOptions.fSortStyle = ssAutomatic then//Determine sortstyle auto.
       SortOptions.fSortStyle := DetermineSortStyle(iCol);

    //Sortere
    fSortedList.Sort(Compare);

    //Now rearrange the rows of the grid in sorted order.
    if iCol = StrToInt( StringList[0] ) then//write to cells property if last sorted col
    for i:=0 to fSortedList.Count-1 do
    begin
      Item := fSortedList.GetItem(i);
      Rows[i+1].Assign( BufferGrid[ Item^.RowNum ]);
    end
    else//else write to temp dynamic array
    begin
      for i:=0 to fSortedList.Count-1 do
      begin
        Item := fSortedList.GetItem(i);
        BufferGrid2[i+1].Assign( BufferGrid[ Item^.RowNum ]);
      end;

      for i := 0 to RowCount-1 do
        BufferGrid[i].Assign (BufferGrid2[i]);
    end;
  end;

  StringList.Free;
  for iRow := 0 to RowCount-1 do
  begin
    BufferGrid[iRow].Free;
    BufferGrid2[iRow].Free;
  end;

  FSorting := False;
  SortOptions.SortCustom := nil;
end;

function TISStringGrid.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISStringGrid.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Cell';
  Result[High(Result)].testParamType := tkString;
  Result[High(Result)].testParamDesc := 'Text of cell';
  Result[High(Result)].testParamIndexes := 2;
end;

function TISStringGrid.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISStringGrid.testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetStandardProps)), PropName) then
    Result := Cells[PropIndexes[0], PropIndexes[1]]
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISStringGrid.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISStringGrid.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetStandardProps)];
end;

procedure TISStringGrid.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISStringGrid.testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetStandardProps)), PropName) then
    Cells[PropIndexes[0], PropIndexes[1]] := Value
  else
    SetPropValue(Self, PropName, Value);
end;

procedure TISStringGrid.SetEditText(ACol, ARow: Integer; const Value: string);
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetStandardProps)), Value, [ACol, ARow], True);
end;

{ TSortOptions }

constructor TSortOptions.Create;
begin
  fSortDirection := sdAscending;
  fSortCol := -1;
  fSortStyle := ssNormal;
  fSortCaseSen := False;
  fCanSort := False;
end;

{ TISDBTreeView }

type
  TtvRecord = record
    Id, UpLinkId: Variant;
  end;
  PtvRecord = ^TtvRecord;

procedure TISDBTreeView.ActiveChange(Sender: TObject);
begin
  if TDataLink(Sender).Active then
  begin
    if not (TDataLink(Sender).DataSet is TISBasicClientDataSet) then
      raise EisException.Create('Assigned dataset has to be TISBasicClientDataSet');
    RebuildTreeView;
  end
  else
    Items.Clear;
end;

function TISDBTreeView.AddRecordedNode(const Caption: string; const Uplink,
  Id: Variant): TTreeNode;
var
  r: PtvRecord;
begin
  Result := Items.AddChild(FindRecordedNode(Uplink), Caption);
  New(r);
  r.Id := Id;
  r.UpLinkId := Uplink;
  Result.Data := r;
end;

function TISDBTreeView.CanCollapse(Node: TTreeNode): Boolean;
begin
  Result := False;
end;

procedure TISDBTreeView.Change(Node: TTreeNode);
begin
  inherited;
  if not FSuppressChangeEvent then
  begin
    Assert(Assigned(Node));
    if FDataLink.Active then
      if PtvRecord(Node.Data)^.Id <> FDataLink.DataSet[FDataLink.KeyField] then
        FDataLink.DataSet.Locate(FDataLink.KeyField, PtvRecord(Node.Data)^.Id, []);
  end;
end;

procedure TISDBTreeView.ChangeCurrentNode;
begin
  if (FDataLink.DataSet.FieldByName(FDataLink.KeyField).AsInteger > 0) then
  begin
    if LocateCurrentRecordNode = nil then
    begin
      AddRecordedNode(FDataLink.DataSet[FDataLink.CaptionField],
        FDataLink.DataSet[FDataLink.UpLinkField], FDataLink.DataSet[FDataLink.KeyField]);
      LocateCurrentRecordNode;
    end;
    UpdateCurrentNode;
  end;
end;

constructor TISDBTreeView.Create(AOwner: TComponent);
begin
  inherited;
  FSuppressChangeEvent := False;
  RowSelect := True;
  AutoExpand := True;
  HideSelection := False;
  DragMode := dmAutomatic;
  FDataLink := TISTreeViewDataLink.Create;
  FDataLink.TreeView := Self;
end;

procedure TISDBTreeView.Delete(Node: TTreeNode);
var
  r: PtvRecord;
begin
  r := Node.Data;
  Dispose(r);
  inherited;
end;

destructor TISDBTreeView.Destroy;
begin
  inherited;
  FreeAndNil(FDataLink);
end;

procedure TISDBTreeView.DragDrop(Source: TObject; X, Y: Integer);
begin
  AbortEx;
end;

procedure TISDBTreeView.DragOver(Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept := (Source = Self) and Assigned(GetNodeAt(X, Y)) and Assigned(DropTarget) and (DropTarget <> Selected);
end;

function TISDBTreeView.FindRecordedNode(const Id: Variant): TTreeNode;
begin
  Result := Items.GetFirstNode;
  while Assigned(Result) and (PtvRecord(Result.Data)^.Id <> Id) do
    Result := Result.GetNext;
end;

function TISDBTreeView.GetCaptionField: string;
begin
  Result := FDataLink.CaptionField;
end;

function TISDBTreeView.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

function TISDBTreeView.GetKeyField: string;
begin
  Result := FDataLink.KeyField;
end;

function TISDBTreeView.GetUpLinkField: string;
begin
  Result := FDataLink.UplinkField;
end;

function TISDBTreeView.LocateCurrentRecordNode: TTreeNode;
begin
  Result := FindRecordedNode(FDataLink.DataSet[FDataLink.KeyField]);
  if Assigned(Result) and (Result <> Selected) then
  begin
    Result.Selected := True;
    Result.Focused := True;
  end;
end;

procedure TISDBTreeView.RebuildTreeView;
var
  cdClone: TISBasicClientDataSet;
  cdShadow: TISBasicClientDataSet;
begin
  Items.BeginUpdate;
  cdClone := TISBasicClientDataSet.Create(nil);
  try
    cdClone.CloneCursor(TISBasicClientDataSet(DataSource.DataSet), False);
    cdShadow := CreateLookupDataset(cdClone, cdClone);
    cdShadow.IndexFieldNames := UpLinkField;
    cdShadow.First;
    while not cdShadow.Eof do
    begin
      AddRecordedNode(cdShadow[CaptionField], cdShadow[UpLinkField], cdShadow[KeyField]);
      cdShadow.Next;
    end;
  finally
    cdClone.Free;
    Items.EndUpdate;
  end;
  if Items.GetFirstNode <> nil then
    Items.GetFirstNode.Selected := True;
end;

procedure TISDBTreeView.SetCaptionField(const Value: string);
begin
  FDataLink.CaptionField := Value;
end;

procedure TISDBTreeView.SetDataSource(const Value: TDataSource);
begin
  FDataLink.DataSource := Value;
end;

procedure TISDBTreeView.SetKeyField(const Value: string);
begin
  FDataLink.KeyField := Value;
end;

procedure TISDBTreeView.SetUpLinkField(const Value: string);
begin
  FDataLink.UplinkField := Value;
end;

procedure TISDBTreeView.UpdateCurrentNode;
var
  n, n2: TTreeNode;
begin
  FSuppressChangeEvent := True;
  try
    n := Selected; 
    if Assigned(n) then
    begin
      n.Text := FDataLink.DataSet[FDataLink.CaptionField];
      if PtvRecord(n.Data)^.UpLinkId <> FDataLink.DataSet[FDataLink.UpLinkField] then
      begin
        n2 := FindRecordedNode(FDataLink.DataSet[FDataLink.UpLinkField]);
        PtvRecord(n.Data)^.UpLinkId := FDataLink.DataSet[FDataLink.UpLinkField];
        n.MoveTo(n2, naAddChild);
        n.Selected := True;
      end;
    end;
  finally
    FSuppressChangeEvent := False;
  end;
end;

{ TISTreeViewDataLink }

procedure TISTreeViewDataLink.ActiveChanged;
begin
  inherited;
  TreeView.ActiveChange(Self);
end;

procedure TISTreeViewDataLink.RecordChanged(Field: TField);
begin
  inherited;
  if not Assigned(Field) and not Editing then
    TreeView.ChangeCurrentNode;
end;

{ TISProvider }

function TISProvider.GetData: Variant;
begin
  Result := DataSet.GetFilteredData;
end;

{ TISDBComboBox }

procedure TISDBComboBox.Change;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetStandardProps)), ItemIndex, []);
end;

constructor TISDBComboBox.Create(AOwner: TComponent);
begin
  inherited;
  ShowMatchText := True;
end;

function TISDBComboBox.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBComboBox.GetFieldName: String;
begin
  Result := DataField;
end;

function TISDBComboBox.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBComboBox.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'ItemIndex';
  Result[High(Result)].testParamType := tkInteger;
  Result[High(Result)].testParamDesc := 'Combo box item index';
end;

function TISDBComboBox.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDBComboBox.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetStandardProps)), PropName) then
    Result := ItemIndex
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISDBComboBox.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBComboBox.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetStandardProps)];
end;

procedure TISDBComboBox.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISDBComboBox.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    ItemIndex := Value
  else
    SetPropValue(Self, PropName, Value);
end;

{ TISDBLookupCombo }

procedure TISDBLookupCombo.CloseUp(modified: boolean);
begin
  if fc = 0 then
  begin
    F := OnNotInList;
    OnNotInList := CustomNotInList;
  end;
  Inc(fc);
  try
    inherited;
  finally
    Dec(fc);
    if fc = 0 then
      OnNotInList := F;
  end;
end;

constructor TISDBLookupCombo.Create(AOwner: TComponent);
begin
  inherited;
  FFieldLinkActive := FFieldLink.OnDataChange;
  FFieldLink.OnDataChange := EvFieldLinkActive;
end;

procedure TISDBLookupCombo.CustomNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: string; var Accept: boolean);
begin
  Accept := False;
  if Assigned(F) then
    F(Sender, LookupTable, NewValue, Accept);
end;

procedure TISDBLookupCombo.EvFieldLinkActive(Sender: TObject);
begin
  FFieldLinkActive(Sender);
  if Assigned(DataSource) and
     Assigned(DataSource.DataSet) and
     DataSource.DataSet.ControlsDisabled and
     Assigned(LookupTable) and
     Assigned(LookupTable.FindField(LookupField)) then
    LookupField := LookupField;
end;

function TISDBLookupCombo.FindRecord(KeyValue, LookupField: string;
  MatchType: TwwLocateMatchType; CaseSensitive,
  PerformLookup: boolean): boolean;
begin
  KeyValue := StringReplace(KeyValue, '''', '''''', [rfReplaceAll]); //Workaround for single quote
  Result := Assigned(LookupTable) and LookupTable.Active and inherited FindRecord(KeyValue, LookupField, MatchType, CaseSensitive);
end;

procedure TISDBLookupCombo.DropDown;
var
  ixLookDef:TIndexDef;
  DisplField1Name:string;
  curPos:integer;
  T: TISBasicClientDataSet;
begin
  if OrderByDisplay and
     Assigned(LookupTable) and
     (LookupTable is TISBasicClientDataSet) and
     (TISBasicClientDataSet(LookupTable).MasterFields = '') and
     (TISBasicClientDataSet(LookupTable).MasterSource = nil)
  then
  begin
    T := TISBasicClientDataSet(LookupTable);
    T.DisableControls;
    try
      T.IndexDefs.Update;
      curPos := 1;
      DisplField1Name := strGetToken(Selected[0], #9, curpos);

      ixLookDef := T.IndexDefs.GetIndexForFields(DisplField1Name, true);
      if ixLookDef <> nil then
        T.IndexName := ixLookDef.Name
      else
        if T.Active then
        begin
          if T.FieldByName(DisplField1Name).FieldKind in [fkData, fkInternalCalc] then
          begin
            SortIndex := RandomString(10);
            T.AddIndex(SortIndex, DisplField1Name, [ixCaseInsensitive], '', '', 0);
            T.IndexDefs.Update;
            T.IndexName := SortIndex;
          end
      end;
    finally
      T.EnableControls;
    end;
  end;

  inherited;
end;

procedure TISDBLookupCombo.Change;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), LookupValue, []);
end;

function TISDBLookupCombo.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBLookupCombo.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Value';
  Result[High(Result)].testParamType := tkString;
  Result[High(Result)].testParamDesc := 'Combo value';
end;

function TISDBLookupCombo.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDBLookupCombo.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Result := LookupValue
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISDBLookupCombo.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBLookupCombo.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISDBLookupCombo.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISDBLookupCombo.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    LookupValue := Value
  else
    SetPropValue(Self, PropName, Value);
end;

function TISDBLookupCombo.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBLookupCombo.GetFieldName: String;
begin
  Result := DataField;
end;

{ TISDBRadioGroup }

function TISDBRadioGroup.CanModify: Boolean;
begin
  try
    Result := inherited CanModify;
    if not Result then
      CheckFocus;
  except
    CheckFocus;
    raise;
  end;
end;

procedure TISDBRadioGroup.Change;
begin
  inherited;
  CheckFocus;
end;

procedure TISDBRadioGroup.CheckFocus;
var
  i, ind: Integer;
begin
  ind := -1;
  for i := 0 to Pred(ControlCount) do
    if Controls[i] is TRadioButton and
       TRadioButton(Controls[i]).Focused then
    begin
      ind := i;
      Break;
    end;
  if ind >= 0 then
    for i := 0 to Pred(ControlCount) do
      if Controls[i] is TRadioButton and
         TRadioButton(Controls[i]).Checked and
         (i <> ind) then
      begin
        TRadioButton(Controls[i]).SetFocus;
        Break;
      end;
end;

procedure TISDBRadioGroup.Click;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetStandardProps)), ItemIndex, []);
end;

constructor TISDBRadioGroup.Create(AOwner: TComponent);
begin
  inherited;
  TabStop := True;
  DataLink.OnDataChange := ISDataChange;
end;

procedure TISDBRadioGroup.DoEnter;
begin
  FTabStop := TabStop;
  TabStop := False;
  inherited;
end;

procedure TISDBRadioGroup.DoExit;
begin
  inherited;
  TabStop := FTabStop;
end;

function TISDBRadioGroup.GetButtonValue(Index: Integer): string;
begin
  if (Index < Values.Count) and (Values[Index] <> '') then
    Result := Values[Index]
  else if Index < Items.Count then
    Result := Items[Index]
  else
    Result := '';
end;

function TISDBRadioGroup.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBRadioGroup.GetFieldName: String;
begin
  Result := DataField;
end;

procedure TISDBRadioGroup.ISDataChange(Sender: TObject);
var
  s: string;
  WasFocused: Boolean;
  I, Index: Integer;
  F: TCustomForm;
begin
  if DataLink.Field <> nil then
    s := DataLink.Field.Text
  else
    s := '';
  WasFocused := (ItemIndex > -1) and (Buttons[ItemIndex].Focused);
  Index := -1;
  for I := 0 to Items.Count - 1 do
    if s = GetButtonValue(I) then
    begin
      Index := I;
      Break;
    end;
  if (Index = -1) and WasFocused then
  begin
    F := GetParentForm(Buttons[ItemIndex]);
    if Assigned(F) then
      F.ActiveControl := nil;
  end;

  if Value <> s then
    Value := s;
end;

procedure TISDBRadioGroup.SetFocus;
var
  bFound: Boolean;
  i, Ind: Integer;
begin
  if FInClick then
    Exit;
  FInClick := True;
  try
    bFound := False;
    for i := 0 to Pred(ControlCount) do
      if Controls[i] is TRadioButton and
         TRadioButton(Controls[i]).Checked then
      begin
        if TWinControl(Controls[i]).CanFocus then
          TWinControl(Controls[i]).SetFocus;
        bFound := True;
        Break;
      end;
    if not bFound and (ControlCount > 0) then
    begin
      Ind := 0;
      for i := 0 to Pred(ControlCount) do
        if Controls[i] is TRadioButton and TRadioButton(Controls[i]).Focused then
          Exit;

      for i := 0 to Pred(ControlCount) do
        if Controls[i] is TRadioButton and TRadioButton(Controls[i]).Checked then
        begin
          Ind := i;
          Break;
        end;
      TWinControl(Controls[Ind]).TabStop := True;
      if TWinControl(Controls[Ind]).CanFocus then
      begin
        if DataSource.DataSet.State = dsBrowse then
        begin
          DataSource.DataSet.DisableControls;
          TWinControl(Controls[Ind]).SetFocus;
          DataSource.DataSet.Cancel;
          DataSource.DataSet.EnableControls;
        end
        else
          TWinControl(Controls[Ind]).SetFocus;
      end;
    end;
  finally
    FInClick := False;
  end;
end;

function TISDBRadioGroup.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBRadioGroup.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'ItemIndex';
  Result[High(Result)].testParamType := tkInteger;
  Result[High(Result)].testParamDesc := 'Radio group item index';
end;

function TISDBRadioGroup.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDBRadioGroup.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetStandardProps)), PropName) then
    Result := ItemIndex
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISDBRadioGroup.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBRadioGroup.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetStandardProps)];
end;

procedure TISDBRadioGroup.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISDBRadioGroup.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    ItemIndex := Value
  else
    SetPropValue(Self, PropName, Value);
end;

procedure TISDBRadioGroup.UpdateRecord;
begin
  if DataLink.Editing then
  begin
    DataLink.Modified;
    DataLink.UpdateRecord;
  end;  
end;

{ TISDBGrid }

procedure TISDBGrid.SetSorting(const Value: Boolean);
begin
  FSorting := Value;
  if Value then
    TitleButtons := True;
end;

procedure TISDBGrid.SetFilterDlg(const Value: Boolean);
begin
  FFilterDlg := Value;
  if Value and (dgIndicator in Options) then
  begin
    IndicatorButton := TwwIButton.Create(Self);
    UpdateIndicatorButton;
    IndicatorButton.Parent := Self;
    IndicatorButton.Width := Succ(IndicatorButtonWidth);
    IndicatorButton.Height := Succ(RowHeights[0]);
    IndicatorButton.OnClick := IndicatorButtonClick;
  end
  else
  begin
    IndicatorButton.Free;
    IndicatorButton := nil;
  end;
  Invalidate;
end;

procedure TISDBGrid.IndicatorButtonClick(Sender: TObject);
var
  TempDialog: TFilterDialog;
  TempBookmark: TBookMark;
  s: string;
  t: TStringList;
  i: Integer;
begin
  inherited;
  with DataSource do
  begin
    if not DataSet.Active then
      Exit;

    if DataSet.State in [dsEdit, dsInsert] then
    begin
      ISErrMessage('You have to Post or Cancel your changes first');
      Exit;
    end;

    if DataSet is TISBasicClientDataSet then
      if not TISBasicClientDataSet(DataSet).UserFiltered then
        S := ''
      else
        S := TISBasicClientDataSet(DataSet).UserFilter
    else
      if not DataSet.Filtered then
        S := ''
      else
        S := DataSet.Filter;

    TempDialog := TFilterDialog.Create(Self);
    try
      if Selected.Count = 0 then
      begin
        t := TStringList.Create;
        try
          for i := 0 to Pred(DataSet.FieldCount) do
            if DataSet.Fields[i].Visible then
              t.Add(DataSet.Fields[i].FieldName + #9#9 + DataSet.Fields[i].DisplayName);
          TempDialog.Initialize(S, t, DataSet);
        finally
          t.Free;
        end;
      end
      else
        TempDialog.Initialize(S, Selected, DataSet);

      if TempDialog.ShowModal = mrOK then
      begin
        TempBookmark := nil;
        try
          if DataSet.Active then
            TempBookmark := DataSet.GetBookmark;
          DataSet.FilterOptions := DataSet.FilterOptions + [foCaseInsensitive];
          if DataSet is TISBasicClientDataSet then
          begin
            TISBasicClientDataSet(DataSet).UserFilter := TempDialog.Filter;
            TISBasicClientDataSet(DataSet).UserFiltered := (Length(TISBasicClientDataSet(DataSet).UserFilter) > 0);
          end
          else
          begin
            DataSet.Filter := TempDialog.Filter(FUseCaseInsensitiveFilterExpression);
            DataSet.Filtered := False;
            DataSet.Filtered := (Length(DataSet.Filter) > 0);
          end;

          if Assigned(TempBookmark) then
          try
            DataSet.GotoBookmark(TempBookmark);
          except
            on EDatabaseError do
              DataSet.First
            else
              raise;
          end;
          if Assigned(FOnFilterChange) then
            FOnFilterChange(Self, DataSet, TempDialog.Filter(FUseCaseInsensitiveFilterExpression));

          UpdateIndicatorButton;            
        finally
          DataSet.FreeBookmark(TempBookmark);
        end;
      end;
    finally
      TempDialog.Free;
    end;
  end;
end;

procedure TISDBGrid.ExecLocateDlg;
var
  i: Integer;
  Field: TField;
  LocateDlg: TISLocateDialog;
  a: array of boolean;
begin
  LocateDlg := TISLocateDialog.Create(Self);
  try
    if Assigned(DataSource) and
       Assigned(DataSource.DataSet) and
       DataSource.DataSet.Active then
    begin
      LocateDlg.FieldSelection := fsVisibleFields;
      LocateDlg.DataSource := DataSource;
      if Selected.Count > 0 then
      begin
        DataSource.DataSet.DisableControls;
        try
          SetLength(a, DataSource.DataSet.FieldCount);
          for i := 0 to Pred(DataSource.DataSet.FieldCount) do
          begin
            a[i] := DataSource.DataSet.Fields[i].Visible;
            DataSource.DataSet.Fields[i].Visible := False;
          end;
          for i := 0 to Pred(Selected.Count) do
          begin
            Field := DataSource.DataSet.FieldByName(Copy(Selected[i], 1, Pred(Pos(#9, Selected[i]))));
            if Assigned(Field) then
            begin
              if Columns[i].DisplayLabel <> '' then
                Field.DisplayLabel := Columns[i].DisplayLabel;
              Field.Visible := True;
            end;
          end;
        finally
          DataSource.DataSet.EnableControls;
        end;
      end;

      if GetActiveField <> nil then
        LocateDlg.SearchField := GetActiveField.FieldName
      else
        LocateDlg.SearchField := '';
      LocateDlg.OnInitDialog := InitDialog;
      try
        LocateDlg.Execute;
      finally
        if Selected.Count > 0 then
        begin
          DataSource.DataSet.DisableControls;
          try
            for i := 0 to Pred(DataSource.DataSet.FieldCount) do
              DataSource.DataSet.Fields[i].Visible := a[i];
          finally
            DataSource.DataSet.EnableControls;
          end;
        end;
      end;
    end;
  finally
    LocateDlg.Free;
  end;
end;

procedure TISDBGrid.ExecHideDlg;
var
  t, ts: TStringList;
  i, j: Integer;
  s: string;
begin
  if not Assigned(DataSource) or
     not Assigned(DataSource.DataSet) or
     not DataSource.DataSet.Active then
    Exit;

  t := TStringList.Create;
  ts := TStringList.Create;
  with TShowColumnsInGrid.Create(nil) do
  try
    if DesignSelected.Count = 0 then
      for i := 0 to Pred(DataSource.DataSet.FieldCount) do
        t.Add(DataSource.DataSet.Fields[i].FieldName + #9#9 + DataSource.DataSet.Fields[i].DisplayName)
    else
      t.Assign(DesignSelected);

    if Selected.Count = 0 then
      for i := 0 to Pred(DataSource.DataSet.FieldCount) do
        ts.Add(DataSource.DataSet.Fields[i].FieldName + #9#9 + DataSource.DataSet.Fields[i].DisplayName)
    else
      ts.Assign(Selected);

    for i := 0 to Pred(t.Count) do
    begin
      s := t[i];
      GetNextStrValue(s, #9);
      GetNextStrValue(s, #9);
      s := GetNextStrValue(s, #9);
      cbColumns.Items.Add(s);
    end;

    for i := 0 to Pred(ts.Count) do
    begin
      s := ts[i];
      GetNextStrValue(s, #9);
      GetNextStrValue(s, #9);
      s := GetNextStrValue(s, #9);
      j := cbColumns.Items.IndexOf(s);
      if j >= 0 then
        cbColumns.Checked[j] := True;
    end;
    if ShowModal = mrOK then
    begin
      for i := Pred(t.Count) downto 0 do
      begin
        s := t[i];
        GetNextStrValue(s, #9);
        GetNextStrValue(s, #9);
        s := GetNextStrValue(s, #9);
        if not cbColumns.Checked[cbColumns.Items.IndexOf(s)] then
          t.Delete(i);
      end;
      Selected.Assign(t);
      ApplySelected;
    end;
  finally
    t.Free;
    ts.Free;
    Free;
  end;
end;

procedure TISDBGrid.CloseDlgQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  with Sender as TForm do
    if ModalResult = mrOK then
    begin
      CanClose := False;
      ModalResult := mrNone;
    end;
end;

procedure TISDBGrid.InitDialog(Dialog: TwwLocateDlg);
begin
  Dialog.OnCloseQuery := CloseDlgQuery;
  FFieldNameComboBoxChange := Dialog.FieldNameComboBox.OnChange;
  Dialog.FieldNameComboBox.OnChange := FieldNameComboBoxChange;
  FFirstButtonClick := Dialog.FirstButton.OnClick;
  Dialog.FirstButton.OnClick := FirstButtonClick;
  FNextButtonClick := Dialog.NextButton.OnClick;
  Dialog.NextButton.OnClick := NextButtonClick;
end;

procedure TISDBGrid.FieldNameComboBoxChange(Sender: TObject);
var
  s: string;
begin
  s := TwwLocateDlg(TComboBox(Sender).Owner).SearchValue.Text;
  try
    if Assigned(FFieldNameComboBoxChange) then
      FFieldNameComboBoxChange(Sender);
  finally
    TwwLocateDlg(TComboBox(Sender).Owner).SearchValue.Text := s;
  end;
end;

function TISDBGrid.IsDefaultSort: Boolean;
begin
  Result := Trim(FDefaultSort) <> '';
end;

procedure TISDBGrid.FirstButtonClick(Sender: TObject);
begin
  if Assigned(FFirstButtonClick) then
    FFirstButtonClick(Sender);
  if TwwLocateDlg(TButton(Sender).Owner).ModalResult = mrOK then
  begin
    TwwLocateDlg(TButton(Sender).Owner).FirstButton.Default:= False;
    TwwLocateDlg(TButton(Sender).Owner).NextButton.Default:= True;
  end
  else
  begin
    TwwLocateDlg(TButton(Sender).Owner).FirstButton.Default:= True;
    TwwLocateDlg(TButton(Sender).Owner).NextButton.Default:= False;
  end;
end;

procedure TISDBGrid.NextButtonClick(Sender: TObject);
begin
  if Assigned(FNextButtonClick) then
    FNextButtonClick(Sender);
  if TwwLocateDlg(TButton(Sender).Owner).ModalResult = mrOK then
  begin
    TwwLocateDlg(TButton(Sender).Owner).FirstButton.Default:= False;
    TwwLocateDlg(TButton(Sender).Owner).NextButton.Default:= True;
  end
  else
  begin
    TwwLocateDlg(TButton(Sender).Owner).FirstButton.Default:= True;
    TwwLocateDlg(TButton(Sender).Owner).NextButton.Default:= False;
  end;
end;

procedure TISDBGrid.CMHintShow(var Message: TMessage);
var
  h: String;
begin
  h := MakeInfoHint;

  if Trim(TCMHintShow(Message).HintInfo^.HintStr ) <> '' then
    TCMHintShow(Message).HintInfo^.HintStr := h + #13 +TCMHintShow(Message).HintInfo^.HintStr
  else
    TCMHintShow(Message).HintInfo^.HintStr := h;

  inherited;
end;

procedure TISDBGrid.LayoutChanged;
begin
  inherited;
  if Assigned(IndicatorButton) then
    IndicatorButton.Height := Succ(RowHeights[0]);
end;

function TISDBGrid.MakeInfoHint: String;
const
  sRecCountHintFormat = 'Record Count: %d';
begin
  if RecordCountHintEnabled and  Assigned(DataSource) and
     Assigned(DataSource.DataSet) and DataSource.DataSet.Active then

    Result := Format(sRecCountHintFormat, [DataSource.DataSet.RecordCount])
  else
    Result := '';
end;

procedure TISDBGrid.ColEnter;
begin
  CurrentCustomEdit := nil;
  inherited;
end;

procedure TISDBGrid.LinkActive(Value: Boolean);
var
  i: Integer;
  Nbr: Integer;
  s: string;
begin
  inherited;
  UpdateIndicatorButton;

  if not (csDesigning in ComponentState) and
     (FDefaultSort <> '-') and Assigned(DataSource) and Assigned(DataSource.DataSet) and
     (DataSource.DataSet is TISBasicClientDataSet) and Value and
     (TISBasicClientDataSet(DataSource.DataSet).IndexName <> 'SORT') then
  begin
    if TISBasicClientDataSet(DataSource.DataSet).IndexDefs.IndexOf('SORT') >= 0 then
    begin
      Nbr := 0;
      if DataSource.DataSet.Active and Assigned(DataSource.DataSet.FindField(DataSource.DataSet.Name + '_NBR')) then
        Nbr := DataSource.DataSet.FindField(DataSource.DataSet.Name + '_NBR').AsInteger;
      TISBasicClientDataSet(DataSource.DataSet).IndexName := 'SORT';
      if DataSource.DataSet.Active and (Nbr > 0) then
        DataSource.DataSet.Locate(DataSource.DataSet.Name + '_NBR', Nbr, [])
      else
        DataSource.DataSet.First;
      Exit;
    end
    else if FDefaultSort <> '' then
    begin
      if CreateSortingIndex(FDefaultSort, [ixCaseInsensitive], '') then
      begin
        TISBasicClientDataSet(DataSource.DataSet).IndexName := 'SORT';
        if Assigned(FOnSortingChanged) then
          FOnSortingChanged(Self);
      end;
    end
    else if Selected.Text <> '' then
    begin
      s := Copy(Selected.Text, 1, Pred(Pos(#9, Selected.Text)));
      DoTitleButtonClick(s);
      if DataSource.DataSet.FieldByName(s).DataType in [ftDate, ftTime, ftDateTime] then
        DoTitleButtonClick(s);
    end
    else
      for i := 0 to Pred(DataSource.DataSet.FieldCount) do
        if DataSource.DataSet.Fields[i].Visible then
        begin
          DoTitleButtonClick(DataSource.DataSet.Fields[i].FieldName);
          Break;
        end;
    DataSource.DataSet.First;
  end;
end;

procedure TISDBGrid.DoCalcTitleImage(Sender: TObject; Field: TField;
  var TitleImageAttributes: TwwTitleImageAttributes);
var
  i: Integer;
begin
  if Sorting and
     Assigned(DataSource) and
     Assigned(DataSource.DataSet) and
     (DataSource.DataSet is TISBasicClientDataSet) then
    with DataSource.DataSet as TISBasicClientDataSet do
    begin
      TitleImageAttributes.ImageIndex := -1;
      TitleImageAttributes.Alignment := taRightJustify;
      if IndexName = 'SORT' then
      begin
        i := IndexDefs.IndexOf(IndexName);
        if i <> -1 then
        begin
          if ((ixDescending in IndexDefs[i].Options) and
              (Pos(';' + Field.FieldName + ';', ';' + IndexDefs[i].Fields + ';') <> 0)) or
             (Pos(';' + Field.FieldName + ';', ';' + IndexDefs[i].DescFields + ';') <> 0) then
            TitleImageAttributes.ImageIndex := 0
          else if Pos(';' + Field.FieldName + ';', ';' + IndexDefs[i].Fields + ';') <> 0 then
            TitleImageAttributes.ImageIndex := 1;
        end;    
      end;
    end;
  inherited;
end;

procedure TISDBGrid.DoTitleButtonClick(AFieldName: string);

  function CheckSubStr(const Substr, Str: string): Boolean;
  begin
    Result := Pos(';' + Substr + ';', ';' + Str + ';') <> 0;
  end;

  procedure DeleteSubStrFrom(const Substr, MasterStr: string; var Str: string);
  var
    i: Integer;
  begin
    i := Pos(';' + Substr + ';', ';' + Str + ';');
    if i <> 0 then
    begin
      if ssCtrl in FShift then
      begin
        Str := ';' + Str + ';';
        Str := Copy(Str, 1, i - 1) + ';' + Copy(Str, i + Length(Substr) + 2, 255);
        if (Str <> '') and (Str[1] = ';') then
          Delete(Str, 1, 1);
        if (Str <> '') and (Str[Length(Str)] = ';') then
          Delete(Str, Length(Str), 1);
      end
      else
        Str := MasterStr;
    end;
  end;

  procedure AddSubStrTo(const Substr, MasterStr: string; var Str: string);
  begin
    if ssCtrl in FShift then
    begin
      if not CheckSubStr(Substr, Str)
      and (Substr <> '') then
      begin
        if Str <> '' then
          Str := Str + ';';
        Str := Str + Substr;
      end;
    end
    else
    begin
      if MasterStr <> '' then
        Str := MasterStr + ';' + Substr
      else
        Str := Substr;
    end;
  end;

var
  s, ds: string;
  i: Integer;
  F: TField;
begin
  if Sorting and
     Assigned(DataSource) and
     Assigned(DataSource.DataSet) and
     (DataSource.DataSet is TISBasicClientDataSet) then

    with DataSource.DataSet as TISBasicClientDataSet do
      begin
        if State in [dsEdit, dsInsert] then
        begin
          ISErrMessage('You have to Post or Cancel your changes first');
          Exit;
        end;

        F := FieldByName(AFieldName);
        if F is TBlobField then
          Exit;
        DisableControls;
        try
          IndexDefs.Update;
          i := IndexDefs.IndexOf('SORT');
          if i <> -1 then
            if ixDescending in IndexDefs[i].Options then
            begin
              s := '';
              ds := IndexDefs[i].Fields;
            end
            else
            begin
              s := IndexDefs[i].Fields;
              ds := IndexDefs[i].DescFields;
            end
          else
          begin
            FOldIndexFieldNames := IndexFieldNames;
            s := '';
            ds := '';
          end;

          if CheckSubStr(AFieldName, ds) then
          begin
            DeleteSubStrFrom(AFieldName, '', ds);
            AddSubStrTo('', MasterFields, s);
          end
          else if CheckSubStr(AFieldName, s) then
          begin
            DeleteSubStrFrom(AFieldName, MasterFields, s);
            AddSubStrTo(AFieldName, '', ds);
          end
          else
          begin
            AddSubStrTo(AFieldName, MasterFields, s);
            AddSubStrTo('', '', ds);
          end;

          if s <> '' then
            CreateSortingIndex(s, [ixCaseInsensitive], ds)
          else if ds <> '' then
            CreateSortingIndex(ds, [ixCaseInsensitive, ixDescending], '')
          else
          begin
            if i <> -1 then
            begin
              DeleteIndex('SORT');
              IndexDefs.Update;
            end;
          end;

          i := IndexDefs.IndexOf('SORT');
          if i <> -1 then
            IndexName := 'SORT'
          else
          begin
            IndexFieldNames := FOldIndexFieldNames;
            FOldIndexFieldNames := '';
          end;

          if Assigned(FOnSortingChanged) then
            FOnSortingChanged(Self);
        finally
          EnableControls;
        end;
      end;
  inherited;
end;

procedure TISDBGrid.DrawCell(ACol, ARow: Integer; ARect: TRect;
  AState: TGridDrawState);
var
  t: TImageList;
begin
  if Assigned(DataSource) and
     Assigned(DataSource.DataSet) and
     not DataSource.DataSet.Active then
    Exit;

  if Sorting and
     Assigned(DataSource) and
     Assigned(DataSource.DataSet) and
     (DataSource.DataSet is TISBasicClientDataSet) and
     (TISBasicClientDataSet(DataSource.DataSet).IndexName = 'SORT') then
  begin
    t := TitleImageList;
    TitleImageList := FTitleImageList;
    try
      inherited;
    finally
      TitleImageList := t;
    end;
  end
  else
    inherited;
end;

procedure TISDBGrid.GetChildren(Proc: TGetChildProc; Root: TComponent);
begin
end;

procedure TISDBGrid.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  FShift := Shift;
  FLastClickX := X;
  FLastClickY := Y;
  inherited;
end;

procedure TISDBGrid.Loaded;
  function Parse_(const s: string): string;
  var
    i: Integer;
  begin
    Result := s;
    for i := 1 to Length(Result) do
      if Result[i] = '_' then Result[i] := ' ';
  end;
var
  i: Integer;
  s1, sWidth, sFieldName, sDisplayName, sNewDisplayName: string;
begin
  inherited;
  Sorting := Sorting;
  if Assigned(Selected) then
    for i := 0 to Selected.Count-1 do
    begin
      s1 := Selected[i];
      sFieldName := GetNextStrValue(s1, #9);
      sWidth := GetNextStrValue(s1, #9);
      sDisplayName := GetNextStrValue(s1, #9);
      if (sFieldName = sDisplayName) or (sDisplayName = 'TYPE') then
      begin
        sNewDisplayName := '';
        while sDisplayName <> '' do
          sNewDisplayName := sNewDisplayName+ ' '+ UpCaseFirst(GetNextStrValue(sDisplayName, '_'));
        Selected[i] := sFieldName+ #9+ sWidth+ #9+ Copy(sNewDisplayName, 2, 255)+ #9+ s1;
      end;
    end;
  Options := Options + [dgTrailingEllipsis, dgDblClickColSizing];

  if IniAttributes.Enabled then
  begin
    IniAttributes.FileName := DEFAULT_REGISTRY + 'Grids\';
    IniAttributes.SectionName := Owner.ClassName + '\' + Name;
  end;

  ExportOptions.ExportType := wwgetSYLK;
  ExportOptions.Options := [esoShowHeader,esoDblQuoteFields,esoClipboard];
end;

procedure TISDBGrid.DataChanged;
var
  i: integer;
  affected: boolean;
begin
  UpdateIndicatorButton;

  if assigned( DataSource ) and assigned( DataSource.DataSet ) then
  begin
    affected := false;
    for i := SelectedList.Count-1 downto 0 do
      if not DataSource.DataSet.BookmarkValid( SelectedList[i]) then
      begin
        DataSource.DataSet.FreeBookmark( SelectedList[i] );
        SelectedList.Delete( i );
        affected := true;
      end;
    if affected then
      Invalidate;
  end;
  inherited;
end;

procedure TISDBGrid.LoadFromIniFile;
var
  TmpReg: TRegInifile;
  SectionValues: TStringList;
  DeletedList: TStringList;
  i: Integer;
  s: string;
begin
  if not (csDesigning in ComponentState) and IniAttributes.Enabled and IniAttributes.SaveToRegistry then
  begin
    TmpReg := TRegInifile.Create(IniAttributes.FileName);
    SectionValues := TStringList.Create;
    DeletedList := TStringList.Create;
    try
      TmpReg.ReadSectionValues(IniAttributes.SectionName, SectionValues);
      for i := 0 to Pred(SectionValues.Count) do
        if LeftStr(SectionValues[i], 1) = '-' then
        begin
          TmpReg.DeleteKey(IniAttributes.SectionName, SectionValues.Names[i]);
          DeletedList.Add(Copy(SectionValues.Names[i], 2, 2048));
        end;
      inherited;
      for i := Pred(Selected.Count) downto 0 do
      begin
        s := Selected[i];
        if DeletedList.IndexOf(GetNextStrValue(s, #9)) >= 0 then
          Selected.Delete(i);
      end;
      ApplySelected;
      for i := 0 to Pred(DeletedList.Count) do
        TmpReg.WriteString(IniAttributes.SectionName, '-' + DeletedList[i], '--');
    finally
      TmpReg.Free;
      SectionValues.Free;
      DeletedList.Free;
    end;
  end
  else
    inherited;
end;

procedure TISDBGrid.SaveToIniFile;
var
  TmpReg: TRegInifile;
  SectionValues: TStringList;
  i, j: Integer;
  s: string;
  b: Boolean;
begin
  inherited;
  if not (csDesigning in ComponentState) and IniAttributes.Enabled and IniAttributes.SaveToRegistry then
  begin
    TmpReg := TRegInifile.Create(IniAttributes.FileName);
    SectionValues := TStringList.Create;
    try
      TmpReg.ReadSectionValues(IniAttributes.SectionName, SectionValues);
      for i := 0 to Pred(DesignSelected.Count)  do
      begin
        b := False;
        s := DesignSelected[i];
        s := GetNextStrValue(s, #9);
        for j := 0 to Pred(SectionValues.Count) do
          if SectionValues.Names[j] = s then
          begin
            b := True;
            Break;
          end;
        if not b then
          TmpReg.WriteString(IniAttributes.SectionName, '-' + s, '--');
      end;
    finally
      TmpReg.Free;
      SectionValues.Free;
    end;
  end
end;

constructor TISDBGrid.Create(AOwner: TComponent);
begin
  inherited;
  IniAttributes.Free;
  IniAttributes := TISIniAttributes.Create;
  with IniAttributes do begin
    FileName := '';
    SectionName := '';
    Delimiter := ';;';
    Owner := self;
  end;

  DesignSelected := TStringList.Create;
  FilterBmp := TBitmap.Create;
  FilteredBmp := TBitmap.Create;
  FilterBmp.LoadFromResourceName(HInstance, 'FILTER');
  FilteredBmp.LoadFromResourceName(HInstance, 'FILTERED');
  MultiSelectOptions := [msoAutoUnSelect, msoShiftSelect];
  Options := [dgEditing, dgTitles, dgIndicator, dgColumnResize,
    dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit,
    dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing];
  //PaintOptions.AlternatingRowColor := clCream;
  FTitleImageList := TImageList.Create(Self);
  with FTitleImageList do
  begin
    Height := 8;
    Width := 8;
    GetInstRes(HInstance, rtBitmap, 'UP', 8, [lrTransparent], clWhite);
    GetInstRes(HInstance, rtBitmap, 'DOWN', 8, [lrTransparent], clWhite);
  end;
  Sorting := True;
  FilterDlg := True;
  LocateDlg := True;
  FFocusColorEnabled := True;
  FRecordCountHintEnabled := true;

  //added to support Evolution color scheme.
  Self.PaintOptions.ActiveRecordColor := clBlack;
  Self.PaintOptions.AlternatingRowColor := $00DDECDD;
end;

destructor TISDBGrid.Destroy;
begin
  FilterBmp.Free;
  FilteredBmp.Free;
  inherited;
  DesignSelected.Free;
end;


procedure TISDBGrid.DoCalcTitleAttributes(AFieldName: string; AFont: TFont;
  ABrush: TBrush; var FTitleAlignment: TAlignment);
var
  s: string;
  i: Integer;
begin
  if Sorting and
     Assigned(DataSource) and
     Assigned(DataSource.DataSet) and
     (DataSource.DataSet is TISBasicClientDataSet) and
     DataSource.DataSet.Active and
     (Trim(AFieldName) <> '') then
    with DataSource.DataSet as TISBasicClientDataSet do
    begin
      if not (FieldByName(AFieldName).FieldKind in [fkData, fkInternalCalc]) then
        ABrush.Color := ShadeColor(ABrush.Color, 5)
      else if IndexName = 'SORT' then
      begin
        i := IndexDefs.IndexOf(IndexName);
        if i <> -1 then
        begin
          s := IndexDefs[i].Fields + ';' + IndexDefs[i].DescFields;
          if Pos(';' + AFieldName + ';', ';' + s + ';') <> 0 then
          begin
            AFont.Color := clBlue;
            ABrush.Color := clYellow;
          end;
        end;  
      end;
    end;
  inherited;
end;

procedure GridToXls(Grid: TISDBGrid; XLS: TXLSFile);
var
  S: TSheet;
  R, i: integer;
begin
  S := XLS.Workbook.Sheets[0];
  Grid.DataSource.DataSet.DisableControls;
  try
    for i := 0 to Grid.ColCount - 1 do
      S.Cells[0, i].Value := Grid.Columns[i].DisplayLabel;
    R := 1;
    Grid.DataSource.DataSet.First;
    while not Grid.DataSource.DataSet.Eof do
    begin
      for i := 0 to Grid.Datalink.FieldCount - 1 do
        if not Grid.Datalink.Fields[i].IsNull then
          S.Cells[R, i].Value := Grid.Datalink.Fields[i].Value;
      Grid.DataSource.DataSet.Next;
      Inc(R);
    end;
  finally
    Grid.DataSource.DataSet.First;
    Grid.DataSource.DataSet.EnableControls;
  end;
end;

procedure TISDBGrid.KeyDown(var Key: Word; Shift: TShiftState);
var
  wwControl: TWinControl;
  XLS: TXLSFile;
begin
    
    if FLocateDlg and (Char(Key) = 'F') and (Shift = [ssCtrl]) then
      ExecLocateDlg;
    if (Char(Key) = 'Y') and (Shift = [ssCtrl]) then
      ExecHideDlg;
    if FFilterDlg and Assigned(IndicatorButton) and (Char(Key) = 'R') and (Shift = [ssCtrl]) then
      IndicatorButton.Click;
    if (Char(Key) = 'E') and (Shift = [ssCtrl]) then
    begin
      with TSaveDialog.Create(nil) do
      try
        Options := Options + [ofOverwritePrompt];
        DefaultExt := 'xls';
        Filter := 'XLS files (*.xls)|*.xls|SYLK files (*.slk)|*.slk';
        if Execute then
        begin
          if FilterIndex = 1 then
          begin
            XLS := TXLSFile.Create;
            try
              GridToXls(Self, XLS);
              XLS.SaveAs(FileName);
            finally
              XLS.Free;
            end;
          end
          else
          begin
            ExportOptions.FileName := FileName;
            ExportOptions.Options := ExportOptions.Options - [esoClipboard];
            ExportOptions.Save;
          end;
        end;
      finally
        Free;
      end;
    end;

  if (Key = VK_RETURN) and
     isCustomEditCell(col, row, wwControl) and
     (wwControl is TwwDBDateTimePicker) then
    setFocus;
  inherited;
end;

procedure TISDBGrid.DoCalcCellColors(Field: TField; State: TGridDrawState;
	     highlight: boolean; AFont: TFont; ABrush: TBrush);
begin
  //added as default for white backgrounds
  if ABrush.Color = clWhite then AFont.Color := clBlack;

  if FFocusColorEnabled  then
    if Focused and (gdSelected in State) and (PaintOptions.ActiveRecordColor <> clNone) then
    begin
      //GUI 2.0, the following eliminates to horrendous cyan color in the active grid field.
      AFont.Color := clBlack;
      if ABrush.Color = clHighlight then
        //the following 2 were updated to fix (again) the highlighted cell color old were 1 and 2
        //fixes check lines bug in Res# 97204
      begin
        //moved font color in here to correct 97212
        AFont.Color := clWhite;
        ABrush.Color := ShadeColor(ABrush.Color, 5);   //like a highight, med. blue
      end
      else if ((ABrush.Color = 0) or (ABrush.Color = clGrayText)) then
        AFont.Color := clWhite
      else
      begin
        //moved font color in here to correct 97212
        AFont.Color := clBlack;
        ABrush.Color := ShadeColor(ABrush.Color, 5);   //medium gray
      end;
    end;

  inherited;
end;

procedure TISDBGrid.CreateWnd;
var
  tb, ta: TStringList;
  b: Boolean;
  s1, s2: string;
  s11, s12: string;
  i, j: Integer;
begin
  if csDesigning in ComponentState then
  begin
    inherited;
    Exit;
  end;

  DesignSelected.Assign(Selected);
  if not IniAttributes.Enabled or UseTFields then
  begin
    IniAttributes.Enabled := False;
    inherited;
    Exit;
  end;
  tb := TStringList.Create;
  ta := TStringList.Create;
  try
    tb.Assign(Selected);
    tb.Sort;
    inherited;
    ta.Assign(Selected);
    ta.Sort;
    b := ta.Count > tb.Count;
    if not b then
      for i := 0 to Pred(ta.Count) do
      begin
        s1 := ta[i];
        s11 := GetNextStrValue(s1, #9);
        for j := 0 to Pred(tb.Count) do
        begin
          s2 := tb[j];
          s12 := GetNextStrValue(s2, #9);
          if s12 >= s11 then
            Break;
        end;
        b := s11 <> s12;
        if not b then
        begin
          GetNextStrValue(s1, #9);
          GetNextStrValue(s2, #9);
          b := GetNextStrValue(s1, #9) <> GetNextStrValue(s2, #9);
          if not b then
          begin
            s1 := GetNextStrValue(s1, #9);
            s2 := GetNextStrValue(s2, #9);
            b := (s1 <> '') and (s2 <> '') and (s1 <> s2) or
                 (s1 = '') and (s2 <> '') and (s2 <> 'F') or
                 (s2 = '') and (s1 <> '') and (s1 <> 'F');
          end;
          if b then
            Break;
        end;
      end;
    if b then
      RestoreDesignSelected;
  finally
    ta.Free;
    tb.Free;
  end;
end;

function TISDBGrid.LastClickWasOnData: boolean;
begin
  with MouseCoord( FLastClickX, FLastClickY ) do
    Result := (x <> 0) and (y<>0);
end;

function TISDBGrid.IsFiltered: Boolean;
begin
  Result :=  Assigned(DataSource) and Assigned(DataSource.DataSet) and DataSource.DataSet.Active and
            (DataSource.DataSet is TISBasicClientDataSet) and
            (TISBasicClientDataSet(DataSource.DataSet).UserFilter <> '') and TISBasicClientDataSet(DataSource.DataSet).UserFiltered;
end;

procedure TISDBGrid.UpdateIndicatorButton;
begin
  if Assigned(IndicatorButton) then
    if IsFiltered then
      IndicatorButton.Glyph.Assign(FilteredBmp)
    else
      IndicatorButton.Glyph.Assign(FilterBmp);
end;

function TISDBGrid.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  if SameText(ActionName, FindActionByIndex(Self, 0)) then
  begin
    SetLength(Result, 1);
    Result[0].testParamName := 'Nbr';
    Result[0].testParamType := tkInteger;
    Result[0].testParamDesc := 'Internal number';
  end
  else
  begin
    SetLength(Result, 1);
    Result[0].testParamName := 'Nbrs';
    Result[0].testParamType := tkDynArray;
    Result[0].testParamDesc := 'Internal numbers';
  end;
end;

function TISDBGrid.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Selected';
  Result[High(Result)].testParamType := tkDynArray;
  Result[High(Result)].testParamDesc := 'Selected internal numbers';
end;

function TISDBGrid.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDBGrid.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
var
  cds: TISBasicClientDataSet;
  i: Integer;
  fields: string;
begin
  if SameText(PropName, FindPropByIndex(Self, Length(GetEditableProps))) then
  begin
    fields := FindKeyFields;
    if fields <> '' then
    begin
      Result := VarArrayCreate([0, Pred(SelectedList.Count)], varInteger);
      cds := TISBasicClientDataSet.Create(nil);
      try
        cds.CloneCursor(DataSource.DataSet as TISBasicClientDataSet, False);
        for i := 0 to Pred(SelectedList.Count) do
        begin
          cds.GotoBookmark(SelectedList[i]);
          Result[i] := cds[fields];
        end;
      finally
        cds.Free;
      end;
    end
    else
      Result := VarArrayCreate([0, -1], varInteger);
  end
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISDBGrid.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 3);

  Result[0].testParamName := 'FindByNbr';
  Result[0].testParamDesc := 'Find By Internal Number';
  Result[0].testParamIndexes := 1;

  Result[1].testParamName := 'Select';
  Result[1].testParamDesc := 'Select records';
  Result[1].testParamIndexes := 1;

  Result[2].testParamName := 'Unselect';
  Result[2].testParamDesc := 'Unselect records';
  Result[2].testParamIndexes := 1;
end;

function TISDBGrid.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISDBGrid.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
var
  sFields: string;
  cds: TISBasicClientDataSet;
  i: Integer;
  bSelect: Boolean;
begin
  sFields := FindKeyFields;
  if sFields <> '' then
    if SameText(ActionName, FindActionByIndex(Self, 0)) then
      DataSource.DataSet.Locate(sFields, ActionParams[0], [])
    else
    begin
      bSelect := SameText(ActionName, FindActionByIndex(Self, 1));
      SelectedList.Clear;
      cds := TISBasicClientDataSet.Create(nil);
      try
        cds.CloneCursor(DataSource.DataSet as TISBasicClientDataSet, False);
        for i := 0 to High(ActionParams) do
          if cds.Locate(sFields, ActionParams[i], []) then
            if bSelect then
              SelectRecord
            else
              UnselectRecord;
      finally
        cds.Free;
      end;
    end;
end;

procedure TISDBGrid.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

function TISDBGrid.FindKeyFields: string;
var
  s: string;
  ProviderName: string;
begin
  Result := '';
  if DataSource.DataSet is TISBasicClientDataSet then
  begin
    ProviderName := TISBasicClientDataSet(DataSource.DataSet).ProviderName;
    if Copy(ProviderName, Length(ProviderName)-4, 5) = '_PROV' then
      s := Copy(ProviderName, 1, Length(ProviderName)-5)
    else
      s := DataSource.DataSet.Name;
    if s <> '' then
    begin
      if DataSource.DataSet.FindField(s+ '_NBR') <> nil then
        Result := s+ '_NBR'
      else if s = 'TMP_CL' then
      begin
        if DataSource.DataSet.FindField('CL_NBR') <> nil then
          Result := 'CL_NBR';
      end
      else if Copy(s, 1, 4) = 'TMP_' then
      begin
        if (DataSource.DataSet.FindField('CL_NBR') <> nil) and
           (DataSource.DataSet.FindField(Copy(s, 5, 255)+ '_NBR') <> nil) then
          Result := 'CL_NBR;'+ Copy(s, 5, 255)+ '_NBR';
      end;
    end;
  end;
end;

procedure TISDBGrid.RemoveSelected(bkmrk1, bkmrk2: TBookmark);
var
  oldSelectedList: TList;
begin
  if not testInSelection then
  begin
    testInSelection := True;
    oldSelectedList := TList.Create;
    try
      oldSelectedList.Assign(SelectedList);
      inherited;
      testNotifySelectedChange(oldSelectedList, SelectedList);
    finally
      oldSelectedList.Free;
      testInSelection := False;
    end;
  end
  else
    inherited;

  DoOnSelectionChange;
end;

procedure TISDBGrid.SelectRecord;
var
  oldSelectedList: TList;
begin
  if not testInSelection then
  begin
    testInSelection := True;
    oldSelectedList := TList.Create;
    try
      oldSelectedList.Assign(SelectedList);
      inherited;
      testNotifySelectedChange(oldSelectedList, SelectedList);
    finally
      oldSelectedList.Free;
      testInSelection := False;
    end;
  end
  else
    inherited;

  DoOnSelectionChange;    
end;

procedure TISDBGrid.SelectRecordRange(bkmrk1, bkmrk2: TBookmark);
var
  oldSelectedList: TList;
begin
  if not testInSelection then
  begin
    testInSelection := True;
    oldSelectedList := TList.Create;
    try
      oldSelectedList.Assign(SelectedList);
      inherited;
      testNotifySelectedChange(oldSelectedList, SelectedList);
    finally
      oldSelectedList.Free;
      testInSelection := False;
    end;
  end
  else
    inherited;

  DoOnSelectionChange;  
end;

procedure TISDBGrid.UnselectRecord;
var
  oldSelectedList: TList;
begin
  if not testInSelection then
  begin
    testInSelection := True;
    oldSelectedList := TList.Create;
    try
      oldSelectedList.Assign(SelectedList);
      inherited;
      testNotifySelectedChange(oldSelectedList, SelectedList);
    finally
      oldSelectedList.Free;
      testInSelection := False;
    end;
  end
  else
    inherited;

  DoOnSelectionChange;    
end;

procedure TISDBGrid.testNotifySelectedChange(const old, new: TList);
var
  tmpList: TList;
  sfields: string;

  procedure testNotify(const list: TList; const command: string);
  var
    cds: TISBasicClientDataSet;
    i: Integer;
    v: variant;
  begin
    if IsLogicRecording then
    begin
      cds := TISBasicClientDataSet.Create(nil);
      try
        cds.CloneCursor(DataSource.DataSet as TISBasicClientDataSet, False);
        v := VarArrayCreate([0, Pred(list.Count)], varVariant);
        for i := 0 to Pred(list.Count) do
        begin
          cds.GotoBookmark(list[i]);
          v[i] := cds[sfields];
        end;
        testNotifyUserAction(self, command, [v]);
      finally
        cds.Free;
      end;
    end;
  end;
begin
  sFields := FindKeyFields;
  if sfields <> '' then
  begin
    tmpList := TList.Create;
    try
    // Get all deleted elements
      tmpList.Assign(old, laSrcUnique, new);
      testNotify(tmpList, FindActionByIndex(Self, 2));
    // Get all added elements
      tmpList.Assign(old, laDestUnique, new);
      testNotify(tmpList, FindActionByIndex(Self, 1));
    finally
      tmpList.Free;
    end;
  end;
end;

procedure TISDBGrid.UnselectAll;
var
  oldSelectedList: TList;
begin
  if not testInSelection then
  begin
    testInSelection := True;
    oldSelectedList := TList.Create;
    try
      oldSelectedList.Assign(SelectedList);
      inherited;
      testNotifySelectedChange(oldSelectedList, SelectedList);
    finally
      oldSelectedList.Free;
      testInSelection := False;
    end;
  end
  else
    inherited;

  DoOnSelectionChange;    
end;

procedure TISDBGrid.DoRowChanged;
var
  sFields: string;
begin
  inherited;
  sFields := FindKeyFields;
  if sFields <> '' then
    testNotifyUserAction(self, FindActionByIndex(Self, 0), [DataSource.DataSet[sFields]]);
end;

procedure TISDBGrid.DoOnSelectionChange;
begin
  if Assigned(FOnSelectionChanged) then
    FOnSelectionChanged(Self);
end;

procedure TISDBGrid.SelectAll;
begin
  inherited SelectAll;
  DoOnSelectionChange;
end;

function TISDBGrid.CreateSortingIndex(AFields: string; AOptions: TIndexOptions; ADescFields: string): Boolean;
var
  i: Integer;
begin
  if Assigned(OnCreatingSortIndex) then
    OnCreatingSortIndex(Self, AFields, AOptions, ADescFields);

  if (AFields <> '') or (ADescFields <> '') then
  begin
    i := TISBasicClientDataSet(DataSource.DataSet).IndexDefs.IndexOf('SORT');
    if i <> -1 then
      TISBasicClientDataSet(DataSource.DataSet).DeleteIndex('SORT');

    TISBasicClientDataSet(DataSource.DataSet).AddIndex('SORT', AFields, AOptions, ADescFields);
    TISBasicClientDataSet(DataSource.DataSet).IndexDefs.Update;

    Result := True;
  end
  else
    Result := False;
end;

{ TISIniAttributes }

constructor TISIniAttributes.Create;
begin
  inherited;
  SaveToRegistry := True;
  CheckNewFields := True;
  Enabled := True;
end;

{ TISDBCheckGrid }

procedure TISDBCheckGrid.ConvertBookMarksIntoRecNo(ARunFilting: Boolean);
var
  i: Integer;
  BM: TBookmark;
  flt: Boolean;
begin
  FSelRecs := TList.Create;

  try
    FSelRecs.Count := SelectedList.Count;
    BM := DataLink.DataSet.GetBookmark;
    DataLink.DataSet.DisableControls;
    try
      Flt := DataLink.DataSet.Filtered;
      DataLink.DataSet.Filtered := False;
      for i := 0 to SelectedList.Count - 1 do
      begin
        DataLink.DataSet.GotoBookmark(SelectedList[i]);
        FSelRecs[i] := Pointer(DataLink.DataSet.RecNo);
      end;

      FFltRecNo := 0;
      if ARunFilting then
        DataLink.DataSet.Filtered := True
      else
        DataLink.DataSet.Filtered := Flt;

      if DataLink.DataSet.BookmarkValid(BM) then
        DataLink.DataSet.GotoBookmark(BM);

    finally
      DataLink.DataSet.EnableControls;
      DataLink.DataSet.FreeBookmark(BM);
    end;

  finally
    FSelRecs.Free;
    FSelRecs := nil;
  end;  
end;

const
  cIndColWidth = 26;

constructor TISDBCheckGrid.Create(AOwner: TComponent);
var
  MI: TMenuItem;
begin
  inherited;

  FSelPopupMenu := TPopupMenu.Create(Self);
  MI := TMenuItem.Create(FSelPopupMenu);
  MI.Caption := 'Select All';
  MI.OnClick := FSelectAll;
  FSelPopupMenu.Items.Add(MI);
  MI := TMenuItem.Create(FSelPopupMenu);
  MI.Caption := 'UnSelect All';
  MI.OnClick := FUnSelectAll;
  FSelPopupMenu.Items.Add(MI);
  MI := TMenuItem.Create(FSelPopupMenu);
  MI.Caption := 'Reverse Selection';
  MI.OnClick := FReverseSelection;
  FSelPopupMenu.Items.Add(MI);
  MI := TMenuItem.Create(FSelPopupMenu);
  MI.Caption := '-';
  FSelPopupMenu.Items.Add(MI);
  MI := TMenuItem.Create(FSelPopupMenu);
  MI.Caption := 'Show Selected Only';
  MI.OnClick := FShowSelection;
  MI.Enabled := False;
  FSelPopupMenu.Items.Add(MI);

  IndicatorButton.Width := cIndColWidth div 2;
  FSelIndictorButton := TSpeedButton.Create(Self);
  FSelIndictorButton.Visible := False;
  FSelIndictorButton.Parent := Self;
  FSelIndictorButton.OnClick := FDropMenuDown;
  FSelIndictorButton.Glyph.LoadFromResourceName(HInstance, 'SELECT');
  FSelIndictorButton.Margin := 0;

  FFilterSetup := False;
  FShowSelectedOnly := False;

  Options := Options - [dgEditing];
  MultiSelectOptions := MultiSelectOptions - [msoAutoUnSelect] + [msoShiftSelect];
  ReadOnly := True;

  MultiSelection := True;
  FGrayMode := False;
end;


procedure TISDBCheckGrid.DrawCell(ACol, ARow: Integer; ARect: TRect;
  AState: TGridDrawState);
var
  DF, Sel: Boolean;
  R, R2: TRect;
  OldActive: Integer;
begin
  if FGrayMode and (SelectedList.Count > 0) then
  begin
    FGrayMode := False;
    invalidate;
  end;

  if (gdFixed in AState) and (ARow > 0) and (dgMultiSelect in Options) then
  begin
    R := ARect;
    R.Top := R.Top + 3;
    R.Bottom := R.Bottom - 3;
    R.Right := R.Right - 3;
    R.Left := R.Right - (R.Bottom - R.Top);
    R2 := ARect;
    R2.Right := R.Left;

    Canvas.Brush.Color := TitleColor;
    Canvas.FillRect(ARect);

    DF :=  DefaultDrawing;
    try
      DefaultDrawing := False;
      inherited DrawCell(ACol, ARow, R2, AState);
    finally
      DefaultDrawing := DF;
    end;

    if DataLink.Active then
    begin
      OldActive := DataLink.ActiveRecord;
      try
        DataLink.ActiveRecord := ARow - 1;
        if FShowSelectedOnly then
          Sel := not DataLink.DataSet.IsEmpty
        else
          Sel := IsSelectedRecord;
      finally
        DataLink.ActiveRecord := OldActive;
      end;
    end
    else
      Sel := False;

    DrawSelCheckBox(R, ACol, ARow, Sel);

    if DefaultDrawing then
      Draw3DLines(ARect, ACol, ARow, AState);
  end

  else
    inherited;
end;


procedure TISDBCheckGrid.DrawSelCheckBox(ARect: TRect; ACol, ARow: integer; val: boolean);
begin
  Canvas.Pen.Width:= 1;
  if FGrayMode then
    Canvas.Brush.Color := $00E2E6E7
  else
    Canvas.Brush.Color := clWindow;

  { Draw checkbox frame }
  Canvas.FillRect(ARect);
  Canvas.Pen.Color:= clBlack;
  Canvas.MoveTo(ARect.right-1, ARect.Top);
  Canvas.LineTo(ARect.left, ARect.Top);
  Canvas.LineTo(ARect.left, ARect.Bottom+1);

  Canvas.Pen.Color:= clGrayText;
  Canvas.MoveTo(ARect.left+1, ARect.Bottom);
  Canvas.LineTo(ARect.right, ARect.Bottom);
  Canvas.LineTo(ARect.right, ARect.Top-1);

  Canvas.Pen.Color:= clWhite;
  Canvas.MoveTo(ARect.left, ARect.Bottom+1);
  Canvas.LineTo(ARect.right+1, ARect.Bottom+1);
  Canvas.LineTo(ARect.right+1, ARect.Top-1);

  Canvas.Pen.Color:= clGray;
  Canvas.MoveTo(ARect.right, ARect.Top-1);
  Canvas.LineTo(ARect.left-1, ARect.Top-1);
  Canvas.LineTo(ARect.left-1, ARect.Bottom+1);

  if val or FGrayMode then
  begin
    if FGrayMode then
      Canvas.Pen.Color:= clGrayText
    else
      Canvas.Pen.Color:= clBlue;

   { Draw checkbox lines }

    Canvas.Pen.Width:=1;
    Canvas.MoveTo(ARect.Left+2,ARect.Top+8 div 2);
    Canvas.LineTo(ARect.Left+2,ARect.Bottom-3);
    Canvas.MoveTo(ARect.Left+3,ARect.Top+10 div 2);
    Canvas.LineTo(ARect.Left+3,ARect.Bottom-2);
    Canvas.MoveTo(ARect.Left+4,ARect.Top+12 div 2);
    Canvas.LineTo(ARect.Left+4,ARect.Bottom-1);
    Canvas.MoveTo(ARect.Left+5,ARect.Top+10 div 2);
    Canvas.LineTo(ARect.Left+5,ARect.Bottom-2);
    Canvas.MoveTo(ARect.Left+6,ARect.Top+8 div 2);
    Canvas.LineTo(ARect.Left+6,ARect.Bottom-3);
    Canvas.MoveTo(ARect.Left+7,ARect.Top+6 div 2);
    Canvas.LineTo(ARect.Left+7,ARect.Bottom-4);
    Canvas.MoveTo(ARect.Left+8,ARect.Top+4 div 2);
    Canvas.LineTo(ARect.Left+8,ARect.Bottom-5);
  end;

  if Canvas<>Canvas then begin
    Canvas.CopyMode := cmSrcCopy;
    InflateRect(ARect, 1, 1);
    ARect.right:= ARect.right + 1;
    ARect.bottom:= ARect.bottom + 1;
    Canvas.CopyRect(ARect, Canvas, ARect);
  end
end;


procedure TISDBCheckGrid.FDropMenuDown(Sender: TObject);
var
  P: TPoint;
begin
  if DataLink.Active then
  begin
    P := FSelIndictorButton.ClientToScreen(Point(0, FSelIndictorButton.Height));
    FSelPopupMenu.Popup(P.X, P.Y);
  end;
end;

procedure TISDBCheckGrid.FilterSelection(DataSet: TDataSet; var Accept: Boolean);
begin
  Inc(FFltRecNo);

  if Assigned(FPrevOnFilter) then
    FPrevOnFilter(DataSet, Accept);

  if Accept and FShowSelectedOnly and Assigned(FSelRecs) then
  begin
    Accept := FSelRecs.IndexOf(Pointer(FFltRecNo)) <> -1;
    if FFltRecNo = DataSet.RecordCount then
      FFltRecNo := 0;
  end;
end;

procedure TISDBCheckGrid.FReverseSelection(Sender: TObject);
begin
  ReverseSelection;
end;


procedure TISDBCheckGrid.ReverseSelection;
var
  Sel: TList;
  saveBK, BM: TBookmark;
  i: integer;
  fl: Boolean;
  Accept : boolean;
begin
   If Assigned(FOnReverseSelectionRecords) then
   begin
      Accept:= True;
      FOnReverseSelectionRecords(Self, Accept);
      if not Accept then exit;
   end;

   Sel := TList.Create;
   try
     with DataSource.Dataset do
     begin
       for i:= 0 to SelectedList.Count-1 do
         Sel.Add(SelectedList.Items[i]);

       SelectedList.Clear;

       saveBK := GetBookmark;
       DisableControls;
       First;
       while (not Eof) do
       begin
         BM := GetBookmark;
         fl := False;
         for i := 0 to Sel.Count - 1 do
           if CompareBookmarks(BM, Sel[i]) = 0 then
           begin
             fl := True;
             FreeBookmark(Sel[i]);
             Sel.Delete(i);
             break;
           end;

           if not fl then
             SelectedList.Add(BM);

         Next;
       end;

       for i := 0 to Sel.Count - 1 do
         FreeBookmark(Sel[i]);

       GotoBookmark(saveBK);
       Freebookmark(saveBK);
       EnableControls;
     end;

   finally
     Sel.Free;
   end;

   DoOnSelectionChange;
end;

procedure TISDBCheckGrid.FSelectAll(Sender: TObject);
begin
  SelectAll;
end;

procedure TISDBCheckGrid.FShowSelection(Sender: TObject);
begin
  TMenuItem(Sender).Checked := not TMenuItem(Sender).Checked;
  FShowSelectedOnly := TMenuItem(Sender).Checked;

  if FShowSelectedOnly and not FFilterSetup then
  begin
    DataLink.DataSet.DisableControls;
    try
      FPrevOnFilter := DataLink.DataSet.OnFilterRecord;
      FOldFiltered := DataLink.DataSet.Filtered;
      DataLink.DataSet.OnFilterRecord := FilterSelection;
      ConvertBookMarksIntoRecNo(True);
      FFilterSetup := True;
    finally
      DataLink.DataSet.EnableControls;
    end;
  end

  else if FFilterSetup then
  begin
    DataLink.DataSet.DisableControls;
    try
      DataLink.DataSet.OnFilterRecord := FPrevOnFilter;
      DataLink.DataSet.Filtered := False;
      DataLink.DataSet.Filtered := FOldFiltered;
      FFilterSetup := False;
    finally
      DataLink.DataSet.EnableControls;
    end;
  end;
end;

procedure TISDBCheckGrid.FUnSelectAll(Sender: TObject);
begin
  UnSelectAll;
end;

function TISDBCheckGrid.GetMultiselection: Boolean;
begin
  Result := dgMultiSelect in Options;
end;

function TISDBCheckGrid.HighlightCell(DataCol, DataRow: Integer; const Value: string; AState: TGridDrawState): Boolean;
begin
  Result := (gdSelected in AState) and ((dgAlwaysShowSelection in Options) or  Focused);
end;


procedure TISDBCheckGrid.KeyDown(var Key: Word; Shift: TShiftState);
var
  R: TRect;
begin
  if (Key = VK_SPACE) and not (dgEditing in Options) then
  begin
    R := CellRect(Col, Row);
    MouseDown(mbLeft, Shift+[ssCtrl], R.Left + 1, R.Top + 1);
  end;

  inherited;
end;


const
  sRecSelectedHintFormat = 'Selected Records: %d';

function TISDBCheckGrid.MakeInfoHint: string;
var
  h: String;
begin
  Result := inherited MakeInfoHint;

  if MultiSelection and
     RecordCountHintEnabled and  Assigned(DataSource) and
     Assigned(DataSource.DataSet) and DataSource.DataSet.Active then
  begin
    h := Format(sRecSelectedHintFormat, [SelectedList.Count]);

    Result := Trim(Result);
    if Length(Result) > 0 then
      Result := Result + #13 + h
    else
      Result := h;
  end;
end;


procedure TISDBCheckGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  GC: TGridCoord;
  RunInh: Boolean;
begin
  RunInh := True;

  if Button = mbLeft then
  begin
    GC := MouseCoord(X, Y);
    if (GC.X = 0) and (GC.Y > 0) then
      if not (ssCtrl in Shift) then
      begin
        if not (ssShift in Shift) then
          Shift := Shift + [ssCtrl];
        inherited MouseDown(Button, Shift, IndicatorButtonWidth + 1, Y);
        RunInh := False;
      end;
  end;

  if RunInh then
    inherited;
end;

procedure TISDBCheckGrid.SelectAll;
begin
  FGrayMode := False;
  inherited SelectAll;
end;

procedure TISDBCheckGrid.SetGrayMode(const Value: Boolean);
begin
  if Value then
    UnSelectAll;
  FGrayMode := Value;    
  Invalidate;
end;

procedure TISDBCheckGrid.SetMultiselection(const Value: Boolean);
begin
  if Value then
  begin
    Options := Options + [dgMultiSelect];
    IndicatorButton.Width := cIndColWidth div 2;
    FSelIndictorButton.Width := IndicatorButton.Width;
    FSelIndictorButton.Height := IndicatorButton.Height;
    FSelIndictorButton.Top := IndicatorButton.Top;
    FSelIndictorButton.Left := IndicatorButton.Width;
    FSelIndictorButton.Visible := True;
    IndicatorButtonWidth := cIndColWidth;
    LayoutChanged;
  end
  else
  begin
    Options := Options - [dgMultiSelect];
    FSelIndictorButton.Visible := False;
    IndicatorButtonWidth := IndicatorButton.Width - 1;
    if SelectedList.Count > 0 then
      UnselectAll;
    LayoutChanged;
  end;
end;


procedure TISDBCheckGrid.UnSelectAll;
begin
  FGrayMode := False;
  try
    inherited;  //To prevent poping of exception SParentRequired when it trys to draw something on destruction
  except
  end;
end;

{ TISPageControl }

procedure TISPageControl.ActivatePage(Page: TTabSheet);
begin
  SendMessage(Handle, TCM_SETCURFOCUS, Page.PageIndex, 0);
end;

function TISPageControl.GetImageIndex(TabIndex: Integer): Integer;
var
  Index: integer;
begin
  if Assigned (OnGetImageIndex) then
    Result := inherited GetImageIndex(TabIndex)
    else begin
    Result := -1;
    for Index := 0 to PageCount - 1 do begin
      if Pages [Index].TabVisible then
        dec (TabIndex);
      if TabIndex < 0 then begin
        Result := Pages [Index].ImageIndex;
        break;
        end;
      end;
    end;
end;


procedure TISPageControl.Change;
begin
  inherited;
  EnableSpeedButtons;
end;

procedure TISPageControl.EnableSpeedButtons;
var
  i, j: Integer;
begin
  for i := 0 to Pred(PageCount) do
    with Pages[i] do
      for j := 0 to Pred(ControlCount) do
        if Controls[j] is TISSpeedButton then
          TISSpeedButton(Controls[j]).CustomEnabled := Pages[i] = ActivePage;
end;

procedure TISPageControl.SetActivePage(Page: TTabSheet);
begin
  inherited ActivePage := Page;
  EnableSpeedButtons;
  testNotifyUserAction(self, FindActionByIndex(Self, 0), [ActivePageIndex]);
end;

procedure TISPageControl.SetTabVisible(const t: TTabSheet;
  const Value: Boolean);
var
  i: Integer;
begin
  if not Value and (t.TabIndex = t.PageControl.ActivePageIndex) then
    for i := 0 to t.PageControl.PageCount- 1 do
      if (t.PageControl.Pages[i] <> t) and t.PageControl.Pages[i].TabVisible then
      begin
        t.PageControl.ActivePageIndex := i;
        Break;
      end;
  t.TabVisible := Value;
end;

procedure TISPageControl.ShowControl(AControl: TControl);
begin
  if (AControl is TTabSheet) and (TTabSheet(AControl).PageControl = Self) then
    ActivePage := TTabSheet(AControl);
  inherited;
end;

function TISPageControl.testGetActionParamList(const ActionName: string): TtestParamList;
begin
  SetLength(Result, 1);
  Result[0].testParamName := 'PageIndex';
  Result[0].testParamType := tkInteger;
  Result[0].testParamDesc := 'Page index';
end;

function TISPageControl.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'ActivePage';
  Result[High(Result)].testParamType := tkInteger;
  Result[High(Result)].testParamDesc := 'Active page';
end;

function TISPageControl.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISPageControl.testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant;
begin
  if SameText(PropName, FindPropByIndex(Self, Length(GetStandardProps))) then
    Result := ActivePageIndex
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISPageControl.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0].testParamName := 'Click';
  Result[0].testParamDesc := 'Click action';
  Result[0].testParamIndexes := 1;
end;

function TISPageControl.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetStandardProps)];
end;

procedure TISPageControl.testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant);
begin
  SendMessage(Handle, TCM_SETCURFOCUS, ActionParams[0], 0);
end;

procedure TISPageControl.testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

{ TISBitBtn }

procedure TISBitBtn.Click;
begin
  if Enabled and Visible then
  begin
    inherited;
    testNotifyUserAction(Self, FindActionByIndex(Self, 0), []);
  end;
end;

function TISBitBtn.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISBitBtn.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;
end;

function TISBitBtn.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISBitBtn.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  Result := GetPropValue(Self, PropName, False);
end;

function TISBitBtn.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0].testParamName := 'Click';
  Result[0].testParamDesc := 'Click action';
end;

function TISBitBtn.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 0);
end;

procedure TISBitBtn.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  Click;
end;

procedure TISBitBtn.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

{ TISSpeedButton }

procedure TISSpeedButton.ActionExecute(Sender: TObject);
begin
  Click;
end;

procedure TISSpeedButton.Click;
begin
  if Enabled and Visible then
  begin
    inherited;
    testNotifyUserAction(Self, FindActionByIndex(Self, 0), []);
  end;
end;

procedure TISSpeedButton.CMDialogChar(var Message: TCMDialogChar);
begin
  // Fixing VCL bug, which does not check ALT key
  if (Message.KeyData and $20000000) = 0 then
    Message.Result := 0 // Ignore the key
  else
    inherited;  // Try to process a shortcut
end;

constructor TISSpeedButton.Create(AOwner: TComponent);
begin
  inherited;
  FCustomEnabled := True;
  FEnabled := inherited Enabled;
  if not (csDesigning in ComponentState) then
  begin
    FActionList := TActionList.Create(AOwner);
    FAction := TAction.Create(FActionList);
    FAction.ActionList := FActionList;
    FAction.OnExecute := ActionExecute;
  end;
end;

function TISSpeedButton.GetShortCut: TShortCut;
begin
  if (csDesigning in ComponentState) then
    Result := FShortCut
  else
    Result := FAction.ShortCut;
end;

procedure TISSpeedButton.SetCustomEnabled(const Value: Boolean);
begin
  FCustomEnabled := Value;
  Enabled := Enabled;
end;

procedure TISSpeedButton.SetMyEnabled(const Value: Boolean);
begin
  FEnabled := Value;
  inherited Enabled := Value and FCustomEnabled;
  if not (csDesigning in ComponentState) then
    FAction.Enabled := inherited Enabled and Visible;
end;

procedure TISSpeedButton.SetShortCut(const Value: TShortCut);
begin
  if (csDesigning in ComponentState) then
    FShortCut := Value
  else
    FAction.ShortCut := Value;
end;

procedure TISSpeedButton.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if not (csDesigning in ComponentState) then
    FAction.Enabled := inherited Enabled and Visible;
end;

function TISSpeedButton.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISSpeedButton.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;
end;

function TISSpeedButton.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISSpeedButton.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  Result := GetPropValue(Self, PropName, False);
end;

function TISSpeedButton.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0].testParamName := 'Click';
  Result[0].testParamDesc := 'Click action';
end;

function TISSpeedButton.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 0);
end;

procedure TISSpeedButton.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  Click;
end;

procedure TISSpeedButton.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

{ TISButton }

procedure TISButton.Click;
begin
  if Enabled and Visible then
  begin
    inherited;
    testNotifyUserAction(Self, FindActionByIndex(Self, 0), []);
  end;
end;

constructor TISButton.Create(AOwner: TComponent);
begin
  inherited;
  FCustomEnabled := True;
  FEnabled := inherited Enabled;
end;

procedure TISButton.SetCustomEnabled(const Value: Boolean);
begin
  FCustomEnabled := Value;
  Enabled := Enabled;
end;

procedure TISButton.SetMyEnabled(const Value: Boolean);
begin
  FEnabled := Value;
  inherited Enabled := Value and FCustomEnabled;
end;

function TISButton.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISButton.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;
end;

function TISButton.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISButton.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  Result := GetPropValue(Self, PropName, False);
end;

function TISButton.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0].testParamName := 'Click';
  Result[0].testParamDesc := 'Click action';
end;

function TISButton.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 0);
end;

procedure TISButton.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  Click;
end;

procedure TISButton.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

{ TISLabel }

constructor TISLabel.Create(AOwner: TComponent);
begin
  inherited;
  FCustomEnabled := True;
  FEnabled := inherited Enabled;
end;

procedure TISLabel.SetCustomEnabled(const Value: Boolean);
begin
  FCustomEnabled := Value;
  Enabled := Enabled;
end;

procedure TISLabel.SetMyEnabled(const Value: Boolean);
begin
  FEnabled := Value;
  inherited Enabled := Value and FCustomEnabled;
end;

{ TISTabControl }

procedure TISTabControl.CMDialogChar(var Message: TCMDialogChar);
begin
  // Fixing VCL bug, which does not check ALT key
  if (Message.KeyData and $20000000) = 0 then
    Message.Result := 0 // Ignore the key
  else
    inherited;  // Try to process a shortcut
end;

procedure TISTabControl.CreateParams(var Params: TCreateParams);
begin
  inherited;
  with Params do
    Style := Style or TCS_FORCELABELLEFT;
end;

procedure TISTabControl.SetTabIndex(Value: Integer);
begin
  inherited;
  testNotifyUserAction(self, FindActionByIndex(Self, 0), [Value]);
end;

function TISTabControl.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 1);
  Result[0].testParamName := 'TabIndex';
  Result[0].testParamType := tkInteger;
  Result[0].testParamDesc := 'Tab index';
end;

function TISTabControl.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'ActiveTab';
  Result[High(Result)].testParamType := tkInteger;
  Result[High(Result)].testParamDesc := 'Active tab';
end;

function TISTabControl.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISTabControl.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(PropName, FindPropByIndex(Self, Length(GetStandardProps))) then
    Result := TabIndex
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISTabControl.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0].testParamName := 'Click';
  Result[0].testParamDesc := 'Click action';
  Result[0].testParamIndexes := 1;
end;

function TISTabControl.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetStandardProps)];
end;

procedure TISTabControl.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  TabIndex := ActionParams[0];
end;

procedure TISTabControl.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

procedure TISTabControl.UpdateTabImages;
begin
  inherited;
end;

{ TISPanel }

procedure TISPanel.SetEnabled(Value: Boolean);
var
  i: Integer;
  v: Integer;
begin
  inherited;
  v := Integer(Value);
  for i := 0 to Pred(ControlCount) do
    if IsPublishedProp(Controls[i], 'CustomEnabled') then
      SetOrdProp(Controls[i], 'CustomEnabled', v);
end;

{ TISDateTimePicker }

procedure TISDateTimePicker.Change;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), DateTime, [], True);
end;

constructor TISDateTimePicker.Create(AOwner: TComponent);
begin
  inherited;
  Time := 0;
  FCustomEnabled := True;
  FEnabled := inherited Enabled;
end;

procedure TISDateTimePicker.SetCustomEnabled(const Value: Boolean);
begin
  FCustomEnabled := Value;
  Enabled := Enabled;
end;

procedure TISDateTimePicker.SetMyEnabled(const Value: Boolean);
begin
  FEnabled := Value;
  inherited Enabled := Value and FCustomEnabled;
end;

function TISDateTimePicker.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDateTimePicker.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'DateTime';
  Result[High(Result)].testParamType := tkFloat;
  Result[High(Result)].testParamDesc := 'Date time picker value';
end;

function TISDateTimePicker.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDateTimePicker.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  Result := GetPropValue(Self, PropName, False);
end;

function TISDateTimePicker.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDateTimePicker.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISDateTimePicker.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISDateTimePicker.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  SetPropValue(Self, PropName, Value);
end;

{ TISLocateDialog }

function TISLocateDialog.Execute: boolean;
var
   field: TField;
   i: integer;
begin
   result:= True;
   with TwwLocateDlg.create(Application) do
   try
      if (DataSource=Nil) or (DataSource.dataSet=Nil) or
	 (not DataSource.dataSet.active) then begin
	 ISErrMessage('DataSource does not reference an active DataSet');
	 exit;
      end;
      DlgComponent:= Self;
      field:= DataSource.DataSet.findField(SearchField);
      FieldNameComboBox.items.clear;
      if SortFields = fsSortByFieldNo then
	 FieldNameComboBox.sorted:= False;

      if DefaultButton = dbFindFirst then begin
	 FirstButton.Default:= True;
	 NextButton.Default:= False;
      end
      else begin
	 FirstButton.Default:= False;
	 NextButton.Default:= True;
      end;

      with DataSource.DataSet do begin
	 for i:= 0 to fieldCount-1 do begin
	       if (fields[i].dataType=ftGraphic) or
		  (fields[i].dataType = ftVarBytes) or (fields[i].dataType=ftBytes) then
		  continue;
	       if (FieldSelection = fsAllFields) or (fields[i].visible) then
		  FieldNameComboBox.items.add(strReplaceChar(fields[i].DisplayLabel, '~',' '));
	 end
      end;
      if field<>Nil then begin
	 SearchValue.Text:= fieldValue;
	 FieldNameCombobox.itemIndex:=
	    FieldNameComboBox.items.indexOf(strReplaceChar(Field.displayLabel, '~',' '));
      end
      else SearchValue.text:= '';

      DataSet:= dataSource.DataSet;
      caseSensitiveCheckBox.checked:= caseSensitive;
      case matchType of
	 mtExactMatch: ExactMatchBtn.checked:= True;
	 mtPartialMatchStart: PartialMatchStartBtn.checked:= True;
	 mtPartialMatchAny: PartialMatchAnyBtn.checked:= True;
      end;

      Caption:=  self.Caption;
      Result := ShowModal = IDOK;

      { Use user selections from dialog to update this component }
      if ExactMatchBtn.Checked then
	 MatchType:= mtExactmatch
      else if PartialMatchStartBtn.Checked then
	 MatchType:= mtPartialMatchStart
      else MatchType:= mtPartialMatchAny;
      caseSensitive:= caseSensitiveCheckBox.checked;
      fieldValue:= SearchValue.Text;
      SearchField:= getfieldNameFromTitle(FieldNameComboBox.text);

   finally
      Free;
   end
end;

function TISLocateDialog.GetFieldNameFromTitle(fieldTitle: string): string;
var
  i: integer;
begin
   result:= '';

   with DataSource.DataSet do begin
      { Give priority to non-calculated fields of the same name, if they exist }
      for i:= 0 to fieldCount-1 do begin
	 if wwisNonPhysicalField(fields[i]) then continue;
	 if strReplaceChar(fields[i].displayLabel,'~',' ')=strReplaceChar(fieldTitle,'~',' ') then begin
	    result:= fields[i].FieldName;
	    exit;
	 end
      end;

      for i:= 0 to fieldCount-1 do begin
	 if not wwisNonPhysicalField(fields[i]) then continue;
	 if strReplaceChar(fields[i].displayLabel,'~', ' ')=strReplaceChar(fieldTitle,'~', ' ') then begin
	    result:= fields[i].FieldName;
	    exit;
	 end
      end
   end;
end;

{ TMacro }

procedure TMacro.Assign(Source: TPersistent);
begin
  if Source is TMacro then
    AsString := TMacro(Source).AsString
  else
    inherited;
end;

procedure TMacro.Clear;
begin
  AsString := '';
end;

function TMacro.IsNotEmpty: Boolean;
begin
  Result := AsString <> '';
end;

{ TMacros }

function TMacros.Add: TMacro;
begin
  Result := (inherited Add) as TMacro;
end;

constructor TMacros.Create(Owner: TPersistent);
begin
  inherited Create(TMacro);
  FOwner := Owner;
end;

function TMacros.GetItem(Index: Integer): TMacro;
begin
  Result := (inherited Items[Index]) as TMacro;
end;

function TMacros.GetOwner: TPersistent;
begin
  Result := FOwner;
end;

function TMacros.MacroByName(const Value: string): TMacro;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count-1 do
    if Items[i].Name = Value then
    begin
      Result := Items[i];
      break;
    end;
  if not Assigned(Result) then
    raise EisException.Create('Macro not found');
end;

procedure TMacros.SetItem(Index: Integer; const Value: TMacro);
begin

end;

{ TIsLockedMemIniFile }

procedure TIsLockedMemIniFile.Clear;
begin
  FSynchronizer.BeginWrite;
  try
    inherited;
    FModified := True;
  finally
    FSynchronizer.EndWrite;
  end;
end;

constructor TIsLockedMemIniFile.Create(const FileName: string;
  const ExternalSynchronizer: TMultiReadExclusiveWriteSynchronizer);
begin
  inherited Create(FileName);
  FSynchronizer := ExternalSynchronizer;
  FOwnedSynchronizer := False;
end;

procedure TIsLockedMemIniFile.ClearModified;
begin
  FModified := False;
end;

constructor TIsLockedMemIniFile.Create(const FileName: string);
begin
  inherited Create(FileName);
  FSynchronizer := TMultiReadExclusiveWriteSynchronizer.Create;
  FOwnedSynchronizer := True;
end;

procedure TIsLockedMemIniFile.DeleteKey(const Section, Ident: String);
begin
  FSynchronizer.BeginWrite;
  try
    inherited;
    FModified := True;
  finally
    FSynchronizer.EndWrite;
  end;
end;

destructor TIsLockedMemIniFile.Destroy;
begin
  if FOwnedSynchronizer then
    FreeAndNil(FSynchronizer);
  inherited;
end;

procedure TIsLockedMemIniFile.EraseSection(const Section: string);
begin
  FSynchronizer.BeginWrite;
  try
    inherited;
    FModified := True;
  finally
    FSynchronizer.EndWrite;
  end;
end;

procedure TIsLockedMemIniFile.GetStrings(List: TStrings);
begin
  FSynchronizer.BeginRead;
  try
    inherited;
  finally
    FSynchronizer.EndRead;
  end;
end;

procedure TIsLockedMemIniFile.ReadSection(const Section: string;
  Strings: TStrings);
begin
  FSynchronizer.BeginRead;
  try
    inherited;
  finally
    FSynchronizer.EndRead;
  end;
end;

procedure TIsLockedMemIniFile.ReadSections(Strings: TStrings);
begin
  FSynchronizer.BeginRead;
  try
    inherited;
  finally
    FSynchronizer.EndRead;
  end;
end;

procedure TIsLockedMemIniFile.ReadSectionValues(const Section: string;
  Strings: TStrings);
begin
  FSynchronizer.BeginRead;
  try
    inherited;
  finally
    FSynchronizer.EndRead;
  end;
end;

function TIsLockedMemIniFile.ReadString(const Section, Ident,
  Default: string): string;
begin
  FSynchronizer.BeginRead;
  try
    Result := inherited ReadString(Section, Ident, Default);
  finally
    FSynchronizer.EndRead;
  end;
end;

procedure TIsLockedMemIniFile.Reload;
begin
  Rename(FileName, True); // based on details of implementation of Rename
end;

procedure TIsLockedMemIniFile.Rename(const FileName: string;
  Reload: Boolean);
begin
  FSynchronizer.BeginWrite;
  try
    inherited;
    if Reload then
      FModified := False;
  finally
    FSynchronizer.EndWrite;
  end;
end;

procedure TIsLockedMemIniFile.SetStrings(List: TStrings);
begin
  FSynchronizer.BeginWrite;
  try
    inherited;
    FModified := False;
  finally
    FSynchronizer.EndWrite;
  end;
end;

procedure TIsLockedMemIniFile.UpdateFile; // need to redefine since GetStrings is not virtual
var
  List: TStringList;
begin
  if FileName <> '' then
  begin
    List := TStringList.Create;
    try
      GetStrings(List);
      List.SaveToFile(FileName);
    finally
      List.Free;
    end;
  end;
  if Assigned(OnUpdateFile) then
    OnUpdateFile(Self);
  FModified := False;
end;

procedure TIsLockedMemIniFile.WriteString(const Section, Ident,
  Value: String);
begin
  FSynchronizer.BeginWrite;
  try
    inherited;
    FModified := True;
  finally
    FSynchronizer.EndWrite;
  end;
end;

{ TIsStringGridHwInfo }

procedure TIsStringGridHwInfo.ReportToStringGrid(const g: TISStringGrid);
var
  l: TStringList;
  i: Integer;
begin
  l := TStringList.Create;
  try
    FillStringList(l);
    for i := 0 to l.Count-1 do
      g.AddLine.CommaText := l[i];
  finally
    FreeAndNil(l);
  end;
end;

{ TISCheckListBox }

procedure TISCheckListBox.ClickCheck;
begin
  inherited;
  if Checked[ItemIndex] then
    testNotifyUserAction(Self, FindActionByIndex(Self, 0), [ItemIndex])
  else
    testNotifyUserAction(Self, FindActionByIndex(Self, 1), [ItemIndex]);
end;


procedure TISCheckListBox.SetBorder(AColor: TColor);
var
  Canvas: TCanvas;
begin
    Canvas := TCanvas.Create;
  try
    Canvas.Handle := GetWindowDC(Handle);
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := AColor;
    Canvas.Brush.Style := bsClear;
    Canvas.Rectangle(0, 0, Width, Height);
    Canvas.Pen.Color := clWindow;                 //Draw the outer rectangle
    Canvas.Rectangle(1,1,Width - 1, Height - 1);  //wipe out the existing 3D
  finally
    ReleaseDC(Handle, Canvas.Handle);
    Canvas.Free;
  end;
end;

function TISCheckListBox.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 1);
  Result[0].testParamName := 'ItemIndex';
  Result[0].testParamType := tkInteger;
  Result[0].testParamDesc := 'Item index';
end;

function TISCheckListBox.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'CheckedState';
  Result[High(Result)].testParamType := tkDynArray;
  Result[High(Result)].testParamDesc := 'Shows if items are checked or unchecked';

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'EnabledState';
  Result[High(Result)].testParamType := tkDynArray;
  Result[High(Result)].testParamDesc := 'Shows if items are enabled or disabled';
end;

function TISCheckListBox.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISCheckListBox.testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant;
var
  i: Integer;
begin
  if SameText(PropName, FindPropByIndex(Self, Length(GetStandardProps))) then
  begin
    Result := VarArrayCreate([0, Pred(Items.Count)], varVariant);
    for i := 0 to Pred(Items.Count) do
      Result[i] := Checked[i];
  end
  else if SameText(PropName, FindPropByIndex(Self, Length(GetStandardProps) + 1)) then
  begin
    Result := VarArrayCreate([0, Pred(Items.Count)], varVariant);
    for i := 0 to Pred(Items.Count) do
      Result[i] := ItemEnabled[i];
  end
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISCheckListBox.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 2);

  Result[0].testParamName := 'Check';
  Result[0].testParamDesc := 'Check item';
  Result[0].testParamIndexes := 1;

  Result[1].testParamName := 'Uncheck';
  Result[1].testParamDesc := 'Uncheck item';
  Result[1].testParamIndexes := 1;
end;

function TISCheckListBox.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 0);
end;

procedure TISCheckListBox.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
var
  b: Boolean;
begin
  b := SameText(ActionName, FindActionByIndex(Self, 0));
  if Checked[ActionParams[0]] <> b then
  begin
    Checked[ActionParams[0]] := b;
    ClickCheck;
  end;
end;

procedure TISCheckListBox.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

procedure TISCheckListBox.WMPaint(var Message: TWMPaint);
begin
  inherited;
  SetBorder(clBtnShadow);
end;


{ TISRadioButton }

procedure TISRadioButton.SetChecked(Value: Boolean);
begin
  inherited;
  if Value then
    testNotifyUserAction(Self, FindActionByIndex(Self, 0), []);
end;

function TISRadioButton.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISRadioButton.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Checked';
  Result[High(Result)].testParamType := tkEnumeration;
  Result[High(Result)].testParamDesc := 'Shows if radio button is checked or unchecked';
end;

function TISRadioButton.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISRadioButton.testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant;
begin
  Result := GetPropValue(Self, PropName, False);
end;

function TISRadioButton.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0].testParamName := 'Check';
  Result[0].testParamDesc := 'Check radio button';
end;

function TISRadioButton.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 0);
end;

procedure TISRadioButton.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  SetChecked(True);
end;

procedure TISRadioButton.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

{ TISMemo }

procedure TISMemo.Change;
begin
  inherited;
  if IsLogicRecording then
    testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), Lines.CommaText, [], True);
end;

function TISMemo.testGetActionParamList(const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISMemo.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Text';
  Result[High(Result)].testParamType := tkString;
  Result[High(Result)].testParamDesc := 'Memo text';
end;

function TISMemo.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISMemo.testGetPropValue(const PropName: string; const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Result := Lines.CommaText
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISMemo.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISMemo.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISMemo.testInvokeUserAction(const ActionName: string; const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISMemo.testSetUserPropValue(const PropName: string; const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Lines.CommaText := Value
  else
    SetPropValue(Self, PropName, Value);
end;

{ TISRichEdit }

procedure TISRichEdit.Change;
begin
  inherited;
  if IsLogicRecording then
    testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), Lines.CommaText, [], True);
end;

function TISRichEdit.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISRichEdit.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Text';
  Result[High(Result)].testParamType := tkString;
  Result[High(Result)].testParamDesc := 'Rich edit text';
end;

function TISRichEdit.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISRichEdit.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Result := Lines.CommaText
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISRichEdit.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISRichEdit.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISRichEdit.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISRichEdit.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Lines.CommaText := Value
  else
    SetPropValue(Self, PropName, Value);
end;

{ TISDBRichEdit }

procedure TISDBRichEdit.Change;
begin
  inherited;
  if IsLogicRecording then
    testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), Lines.CommaText, [], True);
end;

function TISDBRichEdit.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBRichEdit.GetFieldName: String;
begin
  Result := DataField;
end;

function TISDBRichEdit.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBRichEdit.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Text';
  Result[High(Result)].testParamType := tkString;
  Result[High(Result)].testParamDesc := 'Rich edit text';
end;

function TISDBRichEdit.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDBRichEdit.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Result := Lines.CommaText
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISDBRichEdit.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBRichEdit.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISDBRichEdit.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISDBRichEdit.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Lines.CommaText := Value
  else
    SetPropValue(Self, PropName, Value);
end;

{ TISSpinEdit }

procedure TISSpinEdit.Change;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), Value, [], True);
end;

procedure TISSpinEdit.SetBorder(AColor: TColor);
var
  Canvas: TCanvas;
begin
    Canvas := TCanvas.Create;
  try
    Canvas.Handle := GetWindowDC(Handle);
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := AColor;
    Canvas.Brush.Style := bsClear;
    Canvas.Rectangle(0, 0, Width, Height);
    Canvas.Pen.Color := clWindow;                 //Draw the outer rectangle
    Canvas.Rectangle(1,1,Width - 1, Height - 1);  //wipe out the existing 3D
  finally
    ReleaseDC(Handle, Canvas.Handle);
    Canvas.Free;
  end;
end;

function TISSpinEdit.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISSpinEdit.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Value';
  Result[High(Result)].testParamType := tkString;
  Result[High(Result)].testParamDesc := 'Spin edit value';
end;

function TISSpinEdit.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISSpinEdit.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Result := Value
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISSpinEdit.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISSpinEdit.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISSpinEdit.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISSpinEdit.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Self.Value := Value
  else
    SetPropValue(Self, PropName, Value);
end;

procedure TISSpinEdit.WMPaint(var Message: TWMPaint);
begin
  inherited;
  SetBorder(clBtnShadow);
end;

{ TISRadioGroup }

procedure TISRadioGroup.Click;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetStandardProps)), ItemIndex, []);
end;

function TISRadioGroup.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISRadioGroup.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'ItemIndex';
  Result[High(Result)].testParamType := tkInteger;
  Result[High(Result)].testParamDesc := 'Radio group item index';
end;

function TISRadioGroup.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISRadioGroup.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetStandardProps)), PropName) then
    Result := ItemIndex
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISRadioGroup.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISRadioGroup.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetStandardProps)];
end;

procedure TISRadioGroup.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISRadioGroup.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    ItemIndex := Value
  else
    SetPropValue(Self, PropName, Value);
end;

{ TISListView }

procedure TISListView.BeginUpdate;
begin
  Items.BeginUpdate;
  Inc(FUpdateCount);
end;

procedure TISListView.Change(Item: TListItem; Change: Integer);
begin
  if not IsUpdating and not (csDestroying in ComponentState) then  
    inherited;
end;

procedure TISListView.CNNotify(var Message: TWMNotify);
begin
  inherited;
  with PNMListView(Message.NMHdr)^ do
    if (uChanged = LVIF_STATE) and
       (uOldState and LVIS_SELECTED = 0) and
       (uNewState and LVIS_SELECTED <> 0) then
      testNotifyUserAction(Self, FindActionByIndex(Self, 0), [iItem]);
end;

procedure TISListView.EndUpdate;
begin
  Dec(FUpdateCount);
  Items.EndUpdate;

  if not IsUpdating then
    Change(nil, LVIF_STATE);
end;

function TISListView.IsUpdating: Boolean;
begin
  Result := FUpdateCount > 0;
end;

function TISListView.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 1);
  Result[0].testParamName := 'ItemIndex';
  Result[0].testParamType := tkInteger;
  Result[0].testParamDesc := 'Item index';
end;

function TISListView.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;
end;

function TISListView.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISListView.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  Result := GetPropValue(Self, PropName, False);
end;

function TISListView.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0].testParamName := 'Select';
  Result[0].testParamDesc := 'Select item';
  Result[0].testParamIndexes := 1;
end;

function TISListView.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 0);
end;

procedure TISListView.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  Selected := Items[ActionParams[0]];
end;

procedure TISListView.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

{ TISTreeView }

procedure TISTreeView.Change(Node: TTreeNode);
var
  s: string;
begin
  inherited;
  if Assigned(Node) then
  begin
    s := '';
    repeat
      s := ',' + IntToStr(Node.Index) + s;
      Node := Node.Parent;
    until not Assigned(Node);
    System.Delete(s, 1, 1);
  end;

  testNotifyUserAction(Self, FindActionByIndex(Self, 0), [s]);
end;

function TISTreeView.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 1);
  Result[0].testParamName := 'ItemId';
  Result[0].testParamType := tkString;
  Result[0].testParamDesc := 'Item id';
end;

function TISTreeView.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;
end;

function TISTreeView.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISTreeView.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  Result := GetPropValue(Self, PropName, False);
end;

function TISTreeView.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0].testParamName := 'Select';
  Result[0].testParamDesc := 'Select item';
  Result[0].testParamIndexes := 1;
end;

function TISTreeView.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 0);
end;

procedure TISTreeView.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
var
  s: string;
  index, iPos: Integer;
  node: TTreeNode;
begin
  node := nil;
  s := ActionParams[0];
  repeat
    iPos := Pos(',', s);
    if iPos > 0 then
    begin
      index := StrToInt(LeftStr(s, Pred(iPos)));
      s := Copy(s, Succ(iPos), 4096);
    end
    else
    begin
      index := StrToInt(s);
      s := '';
    end;
    if Assigned(node) then
      node := node.Item[index]
    else
      node := Items[index];
  until s = '';
  Selected := node;
end;

procedure TISTreeView.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

{ TISEdit }

procedure TISEdit.Change;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), Text, [], True);
end;

function TISEdit.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISEdit.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Text';
  Result[High(Result)].testParamType := tkString;
  Result[High(Result)].testParamDesc := 'Edit text';
end;

function TISEdit.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISEdit.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Result := Text
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISEdit.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISEdit.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISEdit.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISEdit.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Text := Value
  else
    SetPropValue(Self, PropName, Value);
end;

{ TISDBSpinEdit }

procedure TISDBSpinEdit.Change;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), Value, [], True);
end;

procedure TISDBSpinEdit.SetBorder(AColor: TColor);
var
  Canvas: TCanvas;
begin
    Canvas := TCanvas.Create;
  try
    Canvas.Handle := GetWindowDC(Handle);
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := AColor;
    Canvas.Brush.Style := bsClear;
    Canvas.Rectangle(0, 0, Width, Height);
    Canvas.Pen.Color := clWindow;                 //Draw the outer rectangle
    Canvas.Rectangle(1,1,Width - 1, Height - 1);  //wipe out the existing 3D
  finally
    ReleaseDC(Handle, Canvas.Handle);
    Canvas.Free;
  end;
end;

function TISDBSpinEdit.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBSpinEdit.GetFieldName: String;
begin
  Result := DataField;
end;

function TISDBSpinEdit.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBSpinEdit.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Value';
  Result[High(Result)].testParamType := tkString;
  Result[High(Result)].testParamDesc := 'Spin edit value';
end;

function TISDBSpinEdit.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDBSpinEdit.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Result := Value
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISDBSpinEdit.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBSpinEdit.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISDBSpinEdit.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISDBSpinEdit.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Self.Value := Value
  else
    SetPropValue(Self, PropName, Value);
end;

procedure TISDBSpinEdit.WMPaint(var Message: TWMPaint);
begin
  inherited;
  SetBorder(clBtnShadow);
end;

{ TISDBMemo }

procedure TISDBMemo.Change;
begin
  inherited;
  if IsLogicRecording then
    testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), Lines.CommaText, [], True);
end;

function TISDBMemo.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBMemo.GetFieldName: String;
begin
  Result := DataField;
end;

function TISDBMemo.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBMemo.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Text';
  Result[High(Result)].testParamType := tkString;
  Result[High(Result)].testParamDesc := 'Memo text';
end;

function TISDBMemo.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDBMemo.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Result := Lines.CommaText
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISDBMemo.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBMemo.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISDBMemo.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISDBMemo.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Lines.CommaText := Value
  else
    SetPropValue(Self, PropName, Value);
end;

{ TISCheckBox }

procedure TISCheckBox.SetChecked(Value: Boolean);
begin
  inherited;
  if Value then
    testNotifyUserAction(Self, FindActionByIndex(Self, 0), [])
  else
    testNotifyUserAction(Self, FindActionByIndex(Self, 1), []);
end;

function TISCheckBox.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISCheckBox.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Checked';
  Result[High(Result)].testParamType := tkEnumeration;
  Result[High(Result)].testParamDesc := 'Shows if check box is checked or unchecked';
end;

function TISCheckBox.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISCheckBox.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  Result := GetPropValue(Self, PropName, False);
end;

function TISCheckBox.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 2);

  Result[0].testParamName := 'Check';
  Result[0].testParamDesc := 'Check check box';

  Result[1].testParamName := 'Uncheck';
  Result[1].testParamDesc := 'Uncheck check box';
end;

function TISCheckBox.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 0);
end;

procedure TISCheckBox.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  SetChecked(SameText(ActionName, FindActionByIndex(Self, 0)));
end;

procedure TISCheckBox.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

{ TISComboBox }

procedure TISComboBox.Change;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetStandardProps)), ItemIndex, []);
end;

function TISComboBox.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISComboBox.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'ItemIndex';
  Result[High(Result)].testParamType := tkInteger;
  Result[High(Result)].testParamDesc := 'Combo box item index';
end;

function TISComboBox.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISComboBox.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetStandardProps)), PropName) then
    Result := ItemIndex
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISComboBox.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISComboBox.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetStandardProps)];
end;

procedure TISComboBox.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISComboBox.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    ItemIndex := Value
  else
    SetPropValue(Self, PropName, Value);
end;

{ TISDBComboDlg }

procedure TISDBComboDlg.Change;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), Text, []);
end;

function TISDBComboDlg.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBComboDlg.GetFieldName: String;
begin
  Result := DataField;
end;

function TISDBComboDlg.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBComboDlg.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Text';
  Result[High(Result)].testParamType := tkString;
  Result[High(Result)].testParamDesc := 'Combo dialog text';
end;

function TISDBComboDlg.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDBComboDlg.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Result := Text
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISDBComboDlg.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBComboDlg.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISDBComboDlg.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISDBComboDlg.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Text := Value
  else
    SetPropValue(Self, PropName, Value);
end;

{ TISDBLookupComboDlg }

procedure TISDBLookupComboDlg.Change;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), LookupValue, []);
end;

function TISDBLookupComboDlg.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBLookupComboDlg.GetFieldName: String;
begin
  Result := DataField;
end;

function TISDBLookupComboDlg.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBLookupComboDlg.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Value';
  Result[High(Result)].testParamType := tkString;
  Result[High(Result)].testParamDesc := 'Combo dialog value';
end;

function TISDBLookupComboDlg.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDBLookupComboDlg.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Result := LookupValue
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISDBLookupComboDlg.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBLookupComboDlg.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISDBLookupComboDlg.testInvokeUserAction(
  const ActionName: string; const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISDBLookupComboDlg.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    LookupValue := Value
  else
    SetPropValue(Self, PropName, Value);
end;

{ TISDBEdit }

procedure TISDBEdit.Change;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), Text, [], True);
end;

function TISDBEdit.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBEdit.GetFieldName: String;
begin
  Result := DataField;
end;

function TISDBEdit.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBEdit.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Text';
  Result[High(Result)].testParamType := tkString;
  Result[High(Result)].testParamDesc := 'Edit text';
end;

function TISDBEdit.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDBEdit.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Result := Text
  else
    Result := GetPropValue(Self, PropName, False);
end;

function TISDBEdit.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBEdit.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISDBEdit.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISDBEdit.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  if SameText(FindPropByIndex(Self, Length(GetEditableProps)), PropName) then
    Text := Value
  else
    SetPropValue(Self, PropName, Value);
end;

{ TIsLockedHistMemIniFile }

constructor TIsLockedHistMemIniFile.Create(const FileName: string);
begin
  FHistoryIniFile := TExtendedMemIniFile.Create('');
  inherited;
end;

constructor TIsLockedHistMemIniFile.Create(const FileName: string;
  const ExternalSynchronizer: TMultiReadExclusiveWriteSynchronizer);
begin
  FHistoryIniFile := TExtendedMemIniFile.Create('');
  inherited;
end;

procedure TIsLockedHistMemIniFile.DeleteKey(const Section, Ident: String);
begin
  Assert(False, 'DeleteKey is not supported');
  inherited;
end;

destructor TIsLockedHistMemIniFile.Destroy;
begin
  inherited;
  FreeAndNil(FHistoryIniFile);
end;

procedure TIsLockedHistMemIniFile.EraseSection(const Section: string);
begin
  Assert(False, 'EraseSection is not supported');
  inherited;
end;

procedure TIsLockedHistMemIniFile.UpdateFile;
begin
  Synchronizer.BeginWrite;
  try
    inherited;
    FHistoryIniFile.Clear;
  finally
    Synchronizer.EndWrite;
  end;
end;

procedure TIsLockedHistMemIniFile.WriteString(const Section, Ident,
  Value: String);
var
  sPrevValue: string;
begin
  Synchronizer.BeginWrite;
  try
    sPrevValue := ReadString(Section, Ident, '');
    if not KeyExists(Section, Ident)
    or (Value <> sPrevValue) then
    begin
      inherited;
      if not FHistoryIniFile.KeyExists(Section, Ident) then
        FHistoryIniFile.WriteString(Section, Ident, sPrevValue); 
    end;
  finally
    Synchronizer.EndWrite;
  end;
end;

{ TExtendedMemIniFile }

function TExtendedMemIniFile.KeyExists(const Section,
  Ident: String): Boolean;
var
  l: TStringList;
begin
  l := TStringList.Create;
  try
    ReadSection(Section, l);
    Result := l.IndexOf(Ident) <> -1;
  finally
    l.Free;
  end;
end;

{ TISDBDateTimePicker }

procedure TISDBDateTimePicker.Change;
begin
  inherited;
  testNotifyUserPropChange(Self, FindPropByIndex(Self, Length(GetEditableProps)), DateTime, [], True);
end;

constructor TISDBDateTimePicker.Create(AOwner: TComponent);
begin
  inherited;
  CalendarAttributes.PopupYearOptions.StartYear := 2000;
end;

function TISDBDateTimePicker.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBDateTimePicker.GetFieldName: String;
begin
  Result := DataField;
end;

function TISDBDateTimePicker.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBDateTimePicker.testGetAvailableProps: TtestParamList;
begin
  Result := GetEditableProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'DateTime';
  Result[High(Result)].testParamType := tkFloat;
  Result[High(Result)].testParamDesc := 'Date time picker value';
end;

function TISDBDateTimePicker.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDBDateTimePicker.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  Result := GetPropValue(Self, PropName, False);
end;

function TISDBDateTimePicker.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBDateTimePicker.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 1);

  Result[0] := testGetAvailableProps[Length(GetEditableProps)];
end;

procedure TISDBDateTimePicker.testInvokeUserAction(
  const ActionName: string; const ActionParams: array of Variant);
begin
  raise ETestPropertyNotFound.CreateFmt(ACTION_NOT_FOUND, [ActionName]);
end;

procedure TISDBDateTimePicker.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  SetPropValue(Self, PropName, Value);
end;

{ TISDBCheckBox }

function TISDBCheckBox.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBCheckBox.GetFieldName: String;
begin
  Result := DataField;
end;

procedure TISDBCheckBox.SetChecked(Value: Boolean);
begin
  inherited;
  if Value then
    testNotifyUserAction(Self, FindActionByIndex(Self, 0), [])
  else
    testNotifyUserAction(Self, FindActionByIndex(Self, 1), []);
end;

function TISDBCheckBox.testGetActionParamList(
  const ActionName: string): TtestParamList;
begin
  SetLength(Result, 0);
end;

function TISDBCheckBox.testGetAvailableProps: TtestParamList;
begin
  Result := GetStandardProps;

  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].testParamName := 'Checked';
  Result[High(Result)].testParamType := tkEnumeration;
  Result[High(Result)].testParamDesc := 'Shows if check box is checked or unchecked';
end;

function TISDBCheckBox.testGetControlName: string;
begin
  Result := GetTestControlName(Self);
end;

function TISDBCheckBox.testGetPropValue(const PropName: string;
  const PropIndexes: array of Integer): Variant;
begin
  Result := GetPropValue(Self, PropName, False);
end;

function TISDBCheckBox.testGetUserActions: TtestParamList;
begin
  SetLength(Result, 2);

  Result[0].testParamName := 'Check';
  Result[0].testParamDesc := 'Check check box';

  Result[1].testParamName := 'Uncheck';
  Result[1].testParamDesc := 'Uncheck check box';
end;

function TISDBCheckBox.testGetUserModifiableProps: TtestParamList;
begin
  SetLength(Result, 0);
end;

procedure TISDBCheckBox.testInvokeUserAction(const ActionName: string;
  const ActionParams: array of Variant);
begin
  SetChecked(SameText(ActionName, FindActionByIndex(Self, 0)));
end;

procedure TISDBCheckBox.testSetUserPropValue(const PropName: string;
  const Value: Variant; const PropIndexes: array of Integer);
begin
  raise ETestPropertyNotFound.CreateFmt(PROPERTY_NOT_FOUND, [PropName]);
end;

procedure TISDBCheckBox.Toggle;
var
  OldState: TCheckBoxState;
begin
  OldState := State;
  inherited;
  if Assigned(FOnChange) and (State <> OldState) then
    FOnChange(Self);
end;

procedure TISDBCheckBox.UpdateRecord;
begin
  Perform(CM_EXIT, 0, 0);
end;

{ TISDBImage }

function TISDBImage.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBImage.GetFieldName: String;
begin
  Result := DataField;
end;

{ TISDBText }

function TISDBText.GetDataSource: TDataSource;
begin
  Result := DataSource;
end;

function TISDBText.GetFieldName: String;
begin
  Result := DataField;
end;

{ TisGlowBorder }

class procedure TisGlowBorder.Draw(AControl: TControl);
var
  ParentCanvas: TCanvas;
  ControlTopLeft,
  ControlBottomRight: TPoint;
begin
  if not Assigned(AControl) or not Assigned(AControl.Parent) then
    Exit;

  ParentCanvas := TCanvas.Create;
  try
    ParentCanvas.Handle := GetWindowDC(AControl.Parent.Handle);

    ParentCanvas.pen.Color:= ControlGlowColor;
    ParentCanvas.Pen.Style := psSolid;
    ParentCanvas.Brush.Style := bsClear;
    ControlTopLeft.X := AControl.Left - 2;
    ControlTopLeft.Y := AControl.Top - 2;
    ControlBottomRight.X := AControl.Left + AControl.Width + 2;
    ControlBottomRight.Y := AControl.Top + AControl.Height + 2;
    ParentCanvas.Rectangle(ControlTopLeft.x, ControlTopLeft.y, ControlBottomRight.x, ControlBottomRight.y);
  finally
    ReleaseDC(AControl.Parent.Handle, ParentCanvas.Handle);
    ParentCanvas.Free;
  end;
end;

class procedure TisGlowBorder.DrawRadioGroup(AControl: TControl; RightTopStart: Integer);
var
  ControlTopLeft,
  ControlBottomRight: TPoint;
  ParentCanvas: TCanvas;
begin
  if not Assigned(aControl) or not Assigned(aControl.Parent) then
    Exit;

  ParentCanvas := TCanvas.Create;
  try
    ParentCanvas.Handle := GetWindowDC(aControl.Parent.Handle);

    ParentCanvas.pen.Color:= ControlGlowColor;
    ParentCanvas.Pen.Style := psSolid;
    ParentCanvas.Brush.Style := bsClear;
    ControlTopLeft.X := aControl.Left - 2;
    ControlTopLeft.Y := aControl.Top + 4;   //offset to deal with the top difference
    ControlBottomRight.X := aControl.Left + aControl.Width;
    ControlBottomRight.Y := aControl.Top + aControl.Height;
    //Draw the left side partial top
    ParentCanvas.MoveTo(ControlTopLeft.X, ControlTopLeft.Y - 1);
    ParentCanvas.LineTo(ControlTopLeft.X + 5, ControlTopLeft.Y - 1);
    //Draw the right side partial top
    ParentCanvas.MoveTo(ControlBottomRight.x , ControlTopLeft.Y - 1);
    ParentCanvas.LineTo(ControlBottomRight.X - 5, ControlTopLeft.Y - 1);
    //Draw the left side
    ParentCanvas.MoveTo(ControlTopLeft.X, ControlTopLeft.Y);
    ParentCanvas.LineTo(ControlTopLeft.X, ControlBottomRight.Y);
    //Draw the bottom side
    ParentCanvas.MoveTo(ControlTopLeft.X, ControlBottomRight.Y);
    ParentCanvas.LineTo(ControlBottomRight.X, ControlBottomRight.Y);
    //Draw the right side
    ParentCanvas.MoveTo(ControlBottomRight.X, ControlTopLeft.Y);
    ParentCanvas.LineTo(ControlBottomRight.X, ControlBottomRight.Y);
  finally
    ReleaseDC(aControl.Parent.Handle, ParentCanvas.Handle);
    ParentCanvas.Free;
  end;
end;

class procedure TisGlowBorder.Erase(AControl: TControl);
var
  R: TRect;
begin
  if Assigned(AControl) and Assigned(AControl.Parent) then
  begin
    R := AControl.BoundsRect;
    InflateRect(R, 5, 5);
    InvalidateRect(AControl.Parent.Handle, @R, True);
  end;
end;

{ TisWebBrowser }

procedure TisWebBrowser.CMDialogChar(var Message: TCMDialogChar);
begin
  // Fixing VCL bug, which does not check ALT key
  if (Message.KeyData and $20000000) = 0 then
    Message.Result := 0 // Ignore the key
  else
    inherited;  // Try to process a shortcut
end;

initialization
  DEFAULT_REGISTRY := 'SOFTWARE\' + ChangeFileExt(ExtractFileName(Application.ExeName), '') + '\';

end.

