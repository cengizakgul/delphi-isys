{Copyright 2012 iSystems, LLC

woll-2-woll components are critical to Evolution, as such, they will
not be re-descended from LMD components. Instead, the wwComponents
are now overriden to paint themselves like an LMD component. }
unit isUIwwDBComboBox;

interface

uses
  Windows, Graphics, Messages, SysUtils, Classes, Controls, StdCtrls, Mask,
  wwdbedit, Wwdotdot, Wwdbcomb, Wwcommon, ExtCtrls;

type
  TisUICustomComboBox = class(TCustomComboBox)
  private
    { Private declarations }
    FGlowing : Boolean;
  protected
    { Protected declarations }
    FAlignment: TAlignment;
    procedure SetAlignment(Value: TAlignment);
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure SetBorder(AColor: TColor);
    procedure SetGlowing(Value: Boolean);
    procedure SetParent(AParent: TWinControl); override;
  public
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure Invalidate; override;
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    Property Glowing: Boolean read FGlowing write SetGlowing default False;

  end;

type
  TisUIComboBox = class(TisUICustomComboBox)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
    property Alignment: TAlignment read FAlignment write SetAlignment default taLeftJustify;
    property AutoComplete default True;
    property AutoDropDown default False;
    property BevelEdges;
    property BevelInner;
    property BevelKind default bkNone;
    property BevelOuter;
    property Style; {Must be published before Items}
    property Anchors;
    property BiDiMode;
    property CharCase;
    property Color;
    property Constraints;
    property Ctl3D;
    property DragCursor;
    property DragKind;
    property DragMode;
    property DropDownCount;
    property Enabled;
    property Font;
    property ImeMode;
    property ImeName;
    property ItemHeight;
    property ItemIndex default -1;
    property MaxLength;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property Sorted;
    property TabOrder;
    property TabStop;
    property Text;
    property Visible;
    property OnChange;
    property OnClick;
    property OnCloseUp;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDrawItem;
    property OnDropDown;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMeasureItem;
    property OnSelect;
    property OnStartDock;
    property OnStartDrag;
    property Items; { Must be published after OnMeasureItem }
   
  end;

type
  TisUIwwDBComboBox = class(TwwDBComboBox)
  private
    { Private declarations }
    FGlowing: Boolean;
  protected
    { Protected declarations }
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure SetBorder(AColor: TColor);
    procedure SetGlowing(Value: Boolean);
  published
    { Published declarations }
    Property Glowing: Boolean read FGlowing write SetGlowing default False;
    procedure SetParent(AParent: TWinControl); override;
  public
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure Invalidate; override;
    constructor Create(AOwner: TComponent); override;  
  end;

procedure Register;

implementation
uses isBasicClasses;

procedure Register;
begin
  RegisterComponents('Evolution GUI 2.0', [TisUIwwDBComboBox]);
end;

{ TisUICustomComboBox }
procedure TisUICustomComboBox.WMPaint(var Message: TWMPaint);
const
  InnerStyles: array[TBevelCut] of Integer = (0, BDR_SUNKENINNER, BDR_RAISEDINNER, 0);
  OuterStyles: array[TBevelCut] of Integer = (0, BDR_SUNKENOUTER, BDR_RAISEDOUTER, 0);
  EdgeStyles: array[TBevelKind] of Integer = (0, 0, BF_SOFT, BF_FLAT);
  Ctl3DStyles: array[Boolean] of Integer = (BF_MONO, 0);
var
  EdgeSize: Integer;
  WinStyle: Longint;
  C: TControlCanvas;
  R: TRect;
begin
  inherited;
  if BevelKind = bkNone then Exit;
  C := TControlCanvas.Create;
  try
    C.Control:=Self;
    with C do
    begin
      R := ClientRect;
      C.Brush.Color := Color;
      FrameRect(R);
      InflateRect(R,-1,-1);
      FrameRect(R);
      if BevelKind <> bkNone then
      begin
        EdgeSize := 5;
        if BevelInner <> bvNone then Inc(EdgeSize, BevelWidth);
        if BevelOuter <> bvNone then Inc(EdgeSize, BevelWidth);
        if EdgeSize = 0 then
        begin
          R := ClientRect;
          C.Brush.Color := Color;
          FrameRect(R);
          InflateRect(R,-1,-1);
          FrameRect(R);
        end;
        R := ClientRect;
        with BoundsRect do
        begin
          WinStyle := GetWindowLong(Handle, GWL_STYLE);
          if beLeft in BevelEdges then Dec(Left, EdgeSize);
          if beTop in BevelEdges then Dec(Top, EdgeSize);
          if beRight in BevelEdges then Inc(Right, EdgeSize);
          if (WinStyle and WS_VSCROLL) <> 0 then Inc(Right, GetSystemMetrics(SM_CYVSCROLL));
          if beBottom in BevelEdges then Inc(Bottom, EdgeSize);
          if (WinStyle and WS_HSCROLL) <> 0 then Inc(Bottom, GetSystemMetrics(SM_CXHSCROLL));
        end;
        {DrawEdge(C.Handle, R, InnerStyles[BevelInner] or OuterStyles[BevelOuter],
          Byte(BevelEdges) or EdgeStyles[BevelKind] or Ctl3DStyles[Ctl3D] or BF_ADJUST); }

        R.Left := R.Right - GetSystemMetrics(SM_CXHTHUMB) - 1;
        if DroppedDown then
          DrawFrameControl(C.Handle, R, DFC_SCROLL, DFCS_FLAT or DFCS_SCROLLCOMBOBOX);{
        else
          DrawFrameControl(C.Handle, R, DFC_SCROLL, DFCS_FLAT or DFCS_SCROLLCOMBOBOX);  }

        R := ClientRect;
        SetBorder(clBtnShadow);          
      end;
    end;
  finally
    C.Free;
  end;
  //if FGlowing then AddGlowEffect(self);
end;

procedure TisUICustomComboBox.SetBorder(AColor: TColor);
var
  Canvas: TCanvas;
begin
    Canvas := TCanvas.Create;
  try
    Canvas.Handle := GetWindowDC(Handle);
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := AColor;
    Canvas.Brush.Style := bsClear;
    Canvas.Rectangle(0, 0, Width, Height);
    Canvas.Pen.Color := clWindow;                 //Draw the outer rectangle
    Canvas.Rectangle(1,1,Width - 1, Height - 1);  //wipe out the existing 3D
  finally
    ReleaseDC(Handle, Canvas.Handle);
    Canvas.Free;
  end;
end;
{ TisUICustomCombBox }

procedure TisUICustomComboBox.SetGlowing(Value: Boolean);
begin
  if FGlowing <> Value then
  begin
    FGlowing := Value;
    Invalidate;
  end;
end;

procedure TisUICustomComboBox.Invalidate;
begin
  if(Parent is TPanel) then
    TisGlowBorder.Erase(Self)
  else
  begin
    if FGlowing then TisGlowBorder.Erase(Self);
  end;
  inherited;
end;

procedure TisUICustomComboBox.SetBounds(ALeft, ATop, AWidth,
  AHeight: Integer);
begin
  // Erase previous position border
  if FGlowing then
    TisGlowBorder.Erase(Self);
  inherited;
  Invalidate;
end;

procedure TisUICustomComboBox.SetParent(AParent: TWinControl);
begin
  if (Parent <> AParent) and FGlowing then
    TisGlowBorder.Erase(Self);

  inherited;
end;

{ TisUIwwDBComboBox }

constructor TisUICustomComboBox.Create(AOwner: TComponent);
begin
  inherited;
  FAlignment   := taLeftJustify;
  FGlowing     := False;
end;

procedure TisUICustomComboBox.SetAlignment(Value: TAlignment);
begin
  FAlignment := Value;
  Invalidate;
end;

{ TisUIwwDBComboBox }

constructor TisUIwwDBComboBox.Create(AOwner: TComponent);
begin
  inherited;
  FGlowing := False;
end;

procedure TisUIwwDBComboBox.Invalidate;
begin
  if(Parent is TPanel) then
    TisGlowBorder.Erase(Self)
  else
  begin
    if FGlowing then TisGlowBorder.Erase(Self);
  end;
  inherited;
end;

procedure TisUIwwDBComboBox.SetBorder(AColor: TColor);
var
  Canvas: TCanvas;
begin
    Canvas := TCanvas.Create;
  try
    Canvas.Handle := GetWindowDC(Handle);
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := AColor;
    Canvas.Brush.Style := bsClear;
    Canvas.Rectangle(0, 0, Width, Height);
    Canvas.Pen.Color := clWindow;                 //Draw the outer rectangle
    Canvas.Rectangle(1,1,Width - 1, Height - 1);  //wipe out the existing 3D
  finally
    ReleaseDC(Handle, Canvas.Handle);
    Canvas.Free;
  end;
end;

procedure TisUIwwDBComboBox.SetBounds(ALeft, ATop, AWidth,
  AHeight: Integer);
begin
  // Erase previous position border
  if FGlowing then
    TisGlowBorder.Erase(Self);
  inherited;
  Invalidate;
end;

procedure TisUIwwDBComboBox.SetGlowing(Value: Boolean);
begin
  if FGlowing <> Value then
  begin
    FGlowing := Value;
    Invalidate;
  end;
end;

procedure TisUIwwDBComboBox.SetParent(AParent: TWinControl);
begin
  if (Parent <> AParent) and FGlowing then
    TisGlowBorder.Erase(Self);

  inherited;
end;

procedure TisUIwwDBComboBox.WMPaint(var Message: TWMPaint);
begin
  inherited;
  SetBorder(clBtnShadow);
  if FGlowing then TisGlowBorder.Draw(self);
end;



{ TisUIwwDBComboBox }


end.




