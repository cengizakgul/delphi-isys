unit isUILMDLabel;

interface

uses
  SysUtils, Classes, Controls, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseLabel, LMDCustomLabel, LMDLabel;

type
  TisUILMDLabel = class(TLMDLabel)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Evolution GUI 2.0', [TisUILMDLabel]);
end;

end.
