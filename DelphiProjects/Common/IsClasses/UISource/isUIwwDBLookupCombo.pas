{Copyright 2012 iSystems, LLC

woll-2-woll components are critical to Evolution, as such, they will
not be re-descended from LMD components. Instead, the wwComponents
are now overriden to paint themselves like an LMD component.

Currently, the frame is painted correctly. As time permits, the mouse over
on the button needs to glow. }

unit isUIwwDBLookupCombo;

interface

uses
  Windows, Graphics, Messages, SysUtils, Classes, Controls, StdCtrls, wwdblook,
  ExtCtrls;

type
  TisUIwwDBLookupCombo = class(TwwDBLookupCombo)
  private
    { Private declarations }
    FGlowing: Boolean;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure SetBorder(AColor: TColor);
    procedure SetGlowing(Value: Boolean);
  protected
    { Protected declarations }
    procedure SetParent(AParent: TWinControl); override;
  public
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure Invalidate; override;
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    Property Glowing: Boolean read FGlowing write SetGlowing default False;
  end;

procedure Register;

implementation
 uses ISBasicClasses;

procedure Register;
begin
  RegisterComponents('Evolution GUI 2.0', [TisUIwwDBLookupCombo]);
end;

{ TisUIwwDBLookupCombo }



{ TisUIwwDBLookupCombo }


constructor TisUIwwDBLookupCombo.Create(AOwner: TComponent);
begin
  inherited;
  FGlowing := False;
end;

procedure TisUIwwDBLookupCombo.Invalidate;
begin
  if FGlowing then TisGlowBorder.Erase(Self);
  inherited;
end;

procedure TisUIwwDBLookupCombo.SetBorder(AColor: TColor);
var
  Canvas: TCanvas;
begin
  //Self.Ctl3D := False;       //this can be turned on to make
  Canvas := TCanvas.Create;
  try
    Canvas.Handle := GetWindowDC(Handle);
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := AColor;
    Canvas.Brush.Style := bsClear;
    Canvas.Rectangle(0, 0, Width, Height);
    Canvas.Pen.Color := clWindow;                 //Draw the outer rectangle
    Canvas.Rectangle(1,1,Width - 1, Height - 1);  //wipe out the existing 3D
  finally
    ReleaseDC(Handle, Canvas.Handle);
    Canvas.Free;
  end;

end;

procedure TisUIwwDBLookupCombo.SetBounds(ALeft, ATop, AWidth,
  AHeight: Integer);
begin
  // Erase previous position border
  if FGlowing then
    TisGlowBorder.Erase(Self);
  inherited;
  Invalidate;
end;

procedure TisUIwwDBLookupCombo.SetGlowing(Value: Boolean);
begin
  if FGlowing <> Value then
  begin
    Invalidate;
    FGlowing := Value;
  end;
end;

procedure TisUIwwDBLookupCombo.SetParent(AParent: TWinControl);
begin
  if (Parent <> AParent) and FGlowing then
    TisGlowBorder.Erase(Self);
    
  inherited;
end;

procedure TisUIwwDBLookupCombo.WMPaint(var Message: TWMPaint);
begin
  inherited;
  SetBorder(clBtnShadow);
  if FGlowing then TisGlowBorder.Draw(self);
end;

end.




