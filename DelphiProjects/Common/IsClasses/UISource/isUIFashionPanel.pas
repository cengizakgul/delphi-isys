unit isUIFashionPanel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Math;

type

	TThemeType = ( ttBlue, ttPurple, ttGreen, ttYellow, ttJulianYerfino,
                 ttMatelec, ttMRitmo, ttCustom );

  TisUIMargins = class;

  TisUIFashionPanel  = class(TPanel)
  private
    FRoundRect  : boolean;
    FTitleColor : TColor;
    FTitleFont  : TFont;
    FTitle : String;
    FLineColor : TColor;
    FLineWidth : integer;
    FTheme : TThemeType;
    FShadowColor : TColor;
    FShowShadow  : boolean;
    FShadowDepth : integer;
    FromPaint : boolean;
    FShadowSpace : integer;
    FAbout : string;
    FTitleHeight: Integer;
    FMargins: TisUIMargins;
    procedure SetTitleColor(const Value: TColor);
    procedure SetTitleFont(const Value: TFont);
    procedure SetTitle(const Value: String);
    procedure SetLineColor(const Value: TColor);
    procedure SetTheme(const Value: TThemeType);
    procedure SetShadowColor(const Value: TColor);
    procedure SetShowShadow(const Value: boolean);
    procedure SetShadowDepth(const Value: integer);
    procedure SetShadowSpace(const Value: integer);
    procedure	FontChanged ( Sender : TObject) ;
    procedure	MarginsChanged ( Sender : TObject) ;
    procedure SetAbout(const Value: string);
    procedure SetRoundRect(const Value: boolean);
    procedure SetLineWidth(const Value: integer);
    function  GetTileHight: Integer;
    procedure CMParentColorChanged(var Message: TMessage); message CM_PARENTCOLORCHANGED;
    procedure SetMargins(const Value: TisUIMargins);
  protected
    procedure Paint;override;
    procedure AdjustClientRect(var Rect: TRect); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy;override;
    property TitleHight: Integer read GetTileHight;
  published
    property RoundRect  : boolean  read FRoundRect write SetRoundRect;
		property About			: string  read FAbout write SetAbout;
    property ShadowDepth: integer read FShadowDepth write SetShadowDepth;
    property ShadowSpace: integer read FShadowSpace write SetShadowSpace;
    property ShowShadow : boolean read FShowShadow write SetShowShadow;
    property ShadowColor: TColor  read FShadowColor write SetShadowColor;
    property TitleColor : TColor  read FTitleColor write SetTitleColor;
    property TitleFont  : TFont   read FTitleFont  write SetTitleFont;
    property Title 		  : String  read FTitle write SetTitle;
    property LineWidth  : integer read FLineWidth write SetLineWidth;
    property LineColor  : TColor  read FLineColor write SetLineColor;
    property Theme		  : TThemeType read FTheme write SetTheme;
    property Margins  : TisUIMargins read FMargins  write SetMargins;
  end;


  TisUIMargin = 0..MaxInt;

  TisUIMargins = class(TPersistent)
  private
    FControl: TControl;
    FOnChange: TNotifyEvent;
    FLeft: TisUIMargin;
    FTop: TisUIMargin;
    FRight: TisUIMargin;
    FBottom: TisUIMargin;
    procedure SetMargins(Index: Integer; Value: TisUIMargin);
  protected
    procedure Change; virtual;
    procedure AssignTo(Dest: TPersistent); override;
    property  Control: TControl read FControl;
  public
    constructor Create(Control: TControl); virtual;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  published
    property Top: TisUIMargin index 0 read FTop write SetMargins default 0;
    property Left: TisUIMargin index 1 read FLeft write SetMargins default 0;
    property Right: TisUIMargin index 2 read FRight write SetMargins default 0;
    property Bottom: TisUIMargin index 3 read FBottom write SetMargins default 0;
  end;


procedure Register;

implementation

uses Types;

procedure Register;
begin
  RegisterComponents('Evolution GUI 2.0', [TisUIFashionPanel ]);
end;


{ TisUIFashionPanel  }

procedure TisUIFashionPanel.AdjustClientRect(var Rect: TRect);
begin
//  inherited;     Do not need default ajustment

  InflateRect(Rect, - LineWidth, - LineWidth);
  InflateRect(Rect, - BorderWidth, - BorderWidth);

  Rect.Top := Rect.Top + GetTileHight + Margins.Top;
  Rect.Left := Rect.Left + Margins.Left;
  Rect.Right := Rect.Right - Margins.Right;
  Rect.Bottom := Rect.Bottom - Margins.Bottom;

  if ShowShadow then
  begin
    Rect.Right := Rect.Right - ShadowDepth;
    Rect.Bottom := Rect.Bottom - ShadowDepth;
  end;
end;

constructor TisUIFashionPanel .Create(AOwner: TComponent);
begin
  inherited Create( AOwner );

  ControlStyle := ControlStyle - [csOpaque];

  FAbout := '';
  Caption := '';
  BevelOuter := bvNone;
  Color := RGB ( 224, 224, 224 );        //Evolution GUI 2.0 Stock Setting
	FShadowSpace := 8;          //Evolution GUI 2.0 Stock Setting
  FRoundRect := true;
	FShadowColor := clSilver;   //Evolution GUI 2.0 Stock Setting
  FShowShadow := True;        //Evolution GUI 2.0 Stock Setting
	FShadowDepth := 8;
  Height := 200;
  Width  := 200;
	FTitleColor := clGrayText;  //Evolution GUI 2.0 Stock Setting

  FTitleFont := TFont.Create;
  FTitleFont.Name := 'Arial';
  FTitleFont.Size := 10;       //Evolution GUI 2.0 Stock Setting
  FTitleFont.Color := clWhite;
  FTitleFont.Style := FTitleFont.Style + [fsBold];
  FTitleFont.OnChange := FontChanged;

  FMargins := TisUIMargins.Create(Self);
  FMargins.OnChange := MarginsChanged;

  FTitle := 'Title';
  FLineWidth := 0;
  FLineColor := clWhite;      //Evolution GUI 2.0 Stock Setting
  FTheme := ttCustom;         //Evolution GUI 2.0 Stock Setting
  FromPaint := false;
  BorderWidth := 12;
end;

destructor TisUIFashionPanel .Destroy;
begin
	FTitleFont.Free;
  FMargins.Free;  
  inherited Destroy;
end;

procedure TisUIFashionPanel .FontChanged ( Sender : TObject) ;
begin
  FTitleHeight := 0;
  Realign;
  Invalidate;
end;

function TisUIFashionPanel.GetTileHight: Integer;
begin
  if FTitleHeight = 0 then
  begin
    if HandleAllocated then
    begin
      Canvas.Font := TitleFont;
      FTitleHeight := Canvas.TextHeight('W') + 8;
    end
  end;

  Result := FTitleHeight;
end;

procedure TisUIFashionPanel .Paint;
var
  Rect, R: TRect;
	otherRect : TRect;
begin
 	inherited Paint();

  //Everything on the parent gets painted
  Rect := GetClientRect;
	Canvas.Brush.Color := Parent.Brush.Color;
  Canvas.Brush.Style := bsSolid;
	Canvas.FillRect( Rect );

  Rect.Left := Rect.Left  + Margins.Left;
  Rect.Top := Rect.Top  + Margins.Top;
  Rect.Right := Rect.Right - Margins.Right;
  Rect.Bottom := Rect.Bottom  - Margins.Bottom;

	//The Shadow
	if ( FShowShadow ) then
  begin
    R := Rect;
    R.Top := R.Top + FShadowSpace;
    R.Left := R.Left  + FShadowSpace;

    //Reset the rectangle to where the body will be.
    Rect.Right := Rect.Right - FShadowDepth;
    Rect.Bottom := Rect.Bottom - FShadowDepth;

    //Build the shadow rectangle
  	Canvas.Brush.Color := FShadowColor;
    Canvas.Brush.Style := bsSolid;
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := FShadowColor;

    if ( FRoundRect ) then
    begin
      Canvas.RoundRect(R.Left, R.Top, R.Right, R.Bottom, 10, 10);
    end else
      Canvas.Rectangle(R);
  end;

  //Draw the panel outline
  Canvas.Brush.Color := FLineColor;
  Canvas.Pen.Color := FLineColor;
  if ( FRoundRect ) then
  begin
    Canvas.RoundRect( Rect.Left, Rect.Top,
                      Rect.Right, Rect.Bottom, 10, 10 );
  end
  else
    Canvas.Rectangle( Rect.Left, ClientRect.Top ,
                      Rect.Right, Rect.Bottom );

  Rect.Right := Rect.Right - FLineWidth;
  Rect.Left := Rect.Left + FLineWidth;
  Rect.Top := Rect.Top + FLineWidth;
  Rect.Bottom := Rect.Bottom - FLineWidth;

  otherRect := Rect;
  otherRect.Top := otherrect.Top + 19;

  //Body Fill
  with Canvas do
  begin
    Brush.Color := Color;
    Pen.Style := psClear;
    pen.Color := Color;
    //create a rectangular interface with the titel
    if (FRoundRect) then
    begin
      RoundRect(otherRect.Left, otherRect.Top, otherRect.Right, otherRect.Bottom, 10, 10);
      Rectangle(otherRect.Left, otherRect.Top, otherRect.Right, otherRect.top + 10);
    end else
      Rectangle(otherRect);
  end;

  //Build the title
  Canvas.Brush.Color := FTitleColor;
  Canvas.Pen.Color := FTitleColor;
  if ( FRoundRect ) then
  begin
    Canvas.RoundRect( Rect.Left, Rect.Top,
                      Rect.Right, Rect.Top + GetTileHight, 10, 10 );
    Canvas.Rectangle( Rect.Left, Rect.Top + GetTileHight - 10 ,
                      Rect.Right, Rect.Top + GetTileHight);
  end
  else
  	Canvas.Rectangle( Rect.Left, Rect.Top,
   							 		  Rect.Right, Rect.Top + GetTileHight);


  // Title
	Canvas.Font.Assign(FTitleFont);
  Canvas.Brush.Style := bsClear;
  Canvas.TextOut( Rect.Left + 10, Rect.Top + 2, FTitle );
  Canvas.Pen.Style := psSolid;

end;

procedure TisUIFashionPanel .SetAbout(const Value: string);
begin
//  FAbout := Value;
end;


procedure TisUIFashionPanel .SetLineColor(const Value: TColor);
begin
  FLineColor := Value;
  FTheme := ttCustom;
  Invalidate; //Paint();
end;

procedure TisUIFashionPanel .SetLineWidth(const Value: integer);
begin
  FLineWidth := Value;
  Realign;
  Invalidate; //Paint();
end;

procedure TisUIFashionPanel .SetRoundRect(const Value: boolean);
begin
  FRoundRect := Value;
  Invalidate; //Paint();
end;

procedure TisUIFashionPanel .SetShadowColor(const Value: TColor);
begin
  FShadowColor := Value;
  FTheme := ttCustom;
  Invalidate; //Paint();
end;

procedure TisUIFashionPanel .SetShadowDepth(const Value: integer);
begin
  FShadowDepth := Value;
  FTheme := ttCustom;
  Realign;
  Invalidate;
end;

procedure TisUIFashionPanel .SetShadowSpace(const Value: integer);
begin
  FShadowSpace := Value;
  FTheme := ttCustom;
  Realign;
  Invalidate;
end;

procedure TisUIFashionPanel .SetShowShadow(const Value: boolean);
begin
  FShowShadow := Value;
  FTheme := ttCustom;
  Realign;
  Invalidate;
end;

procedure TisUIFashionPanel .SetTheme(const Value: TThemeType);
begin
  FTheme := Value;
  case FTheme of
		ttBlue 	 : begin
					      Color := $00E3DED5;
								FTitleColor := RGB ( 0, 128, 192 );
                FShowShadow := false;
                FLineWidth := 0;
							  FTitleFont.Color := clWhite;
    					 end;
    ttPurple : begin
      					Color := $00CBC5C8;
                FTitleColor := clPurple;
                FShowShadow := false;
                FLineWidth := 0;
                FTitleFont.Color := clWhite;
    					 end;
    ttGreen  : begin
    						Color := $00B9CEBB;
                FTitleColor := clGreen;
                FShowShadow := false;
                FLineWidth := 0;
                FTitleFont.Color := clWhite;
    					 end;
    ttYellow : begin
								Color := $00BED2D8;
    						FTitleColor := $00B9FFFF;
                FTitleFont.Color := clBlack;
                FShowShadow := false;
                FLineWidth := 0;
    					 end;
		ttJulianYerfino :
    					begin
 								Color := $00B9CDD0;
    						FTitleColor := clGray;
                FLineColor := clGray;
                FShowShadow := true;
                FLineWidth := 1;
                FShadowDepth := 3;
                FShadowSpace := 5;
                FTitleFont.Color := clWhite;
								if ( FTitle = 'Title' ) then
	                FTitle := 'Julian Yerfino <yerfinojul@ufasta.edu.ar> Programming';
    					end;
  	ttMatelec: begin
								Color := RGB( 231, 232, 219);
    						FTitleColor := RGB( 141, 141, 141);
                FLineColor := clGray;
							  FTitleFont.Color := clWhite;
                FShowShadow := false;
                FLineWidth := 1;
                FLineColor := FTitleColor;
               end;
    ttMRitmo : begin
 								Color := clGray;;
    						FTitleColor := clBlack;
                FLineColor := clSilver;
                FShowShadow := true;
                FLineWidth := 1;
                FTitleFont.Color := $000080FF;
                FShadowDepth := 4;
                FShadowSpace := 10;
								if ( FTitle = 'Title' ) then
	                FTitle := 'MRitmo <vazquezger@ufasta.edu.ar> Help';
    				   end;
  end;
  Invalidate; //Paint();
end;

procedure TisUIFashionPanel .SetTitle(const Value: String);
begin
  FTitle := Value;
  Invalidate; //Paint();
end;

procedure TisUIFashionPanel .SetTitleColor(const Value: TColor);
begin
	FTitleColor := Value;
  FTheme := ttCustom;
  Invalidate; //Paint();
end;

procedure TisUIFashionPanel .SetTitleFont(const Value: TFont);
begin
	FTitleFont.Assign(Value);
  FTheme := ttCustom;
  Invalidate; //
end;


procedure TisUIFashionPanel.CMParentColorChanged(var Message: TMessage);
begin
  Invalidate;
end;

procedure TisUIFashionPanel.SetMargins(const Value: TisUIMargins);
begin
	FMargins.Assign(Value);
end;

procedure TisUIFashionPanel.MarginsChanged(Sender: TObject);
begin
  Realign;
  Invalidate;
end;

{ TisUIMargins }

procedure TisUIMargins.AssignTo(Dest: TPersistent);
begin
  if Dest is TisUIMargins then
    with TisUIMargins(Dest) do
    begin
      FTop := Self.Top;
      FLeft := Self.Left;
      FRight := Self.Right;
      FBottom := Self.Bottom;
      Change;
    end
  else
    inherited AssignTo(Dest);
end;

procedure TisUIMargins.Change;
begin
  if Assigned(FOnChange) then FOnChange(Self);
end;

constructor TisUIMargins.Create(Control: TControl);
begin
  inherited Create;
  FControl := Control;
end;

procedure TisUIMargins.SetMargins(Index: Integer; Value: TisUIMargin);
begin
  case Index of
    0:
      if Value <> FTop then
      begin
        FTop := Value;
        Change;
      end;
    1:
      if Value <> FLeft then
      begin
        FLeft := Value;
        Change;
      end;
    2:
      if Value <> FRight then
      begin
        FRight := Value;
        Change;
      end;
    3:
      if Value <> FBottom then
      begin
        FBottom := Value;
        Change;
      end;
  end;
end;

end.
