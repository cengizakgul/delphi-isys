unit isUIDBMemo;

interface

uses
  Windows, Graphics, Messages, SysUtils, Classes, Controls, StdCtrls, DBCtrls,
  DB, VDBConsts, ExtCtrls;

  //October 12, 2012
  //The follwing isUI Components are redesigns of the components.
  //The Windows 7 look and feel applied to InfoPower components wasn't working well.
  //Painting and refreshing in the memos and comboboxes was bad at best.
type
  TisUICustomMemo = Class(TCustomMemo)
  private
    { Private declarations }
    FGlowing:  Boolean;
    procedure SetGlowing(Value: Boolean);
  protected
    { Protected declarations }
    procedure SetBorder(AColor: TColor);
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure SetParent(AParent: TWinControl); override;
  public
    { Public declarations }
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure Invalidate; override;
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    Property Glowing: Boolean read FGlowing write SetGlowing default False;
  end;

type
  TisUIMemo = class(TisUICustomMemo)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
    property Align;
    property Alignment;
    property Anchors;
    property BevelEdges;
    property BevelInner;
    property BevelKind default bkNone;
    property BevelOuter;
    property BiDiMode;
    property BorderStyle;
    property Color;
    property Constraints;
    property Ctl3D;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property HideSelection;
    property ImeMode;
    property ImeName;
    property Lines;
    property MaxLength;
    property OEMConvert;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ScrollBars;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property WantReturns;
    property WantTabs;
    property WordWrap;
    property OnChange;
    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;    
  end;


type
  TisUIDBMemo = class(TisUICustomMemo)
  private
    FDataLink: TFieldDataLink;
    FAutoDisplay: Boolean;
    FFocused: Boolean;
    FMemoLoaded: Boolean;
    FPaintControl: TPaintControl;
    procedure DataChange(Sender: TObject);
    procedure EditingChange(Sender: TObject);
    function GetDataField: string;
    function GetDataSource: TDataSource;
    function GetField: TField;
    function GetReadOnly: Boolean;
    procedure SetDataField(const Value: string);
    procedure SetDataSource(Value: TDataSource);
    procedure SetReadOnly(Value: Boolean);
    procedure SetFocused(Value: Boolean);
    procedure UpdateData(Sender: TObject);
    procedure WMCut(var Message: TMessage); message WM_CUT;
    procedure WMPaste(var Message: TMessage); message WM_PASTE;
    procedure WMUndo(var Message: TMessage); message WM_UNDO;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure WMLButtonDblClk(var Message: TWMLButtonDblClk); message WM_LBUTTONDBLCLK;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure CMGetDataLink(var Message: TMessage); message CM_GETDATALINK;
  protected
    procedure SetAutoDisplay(Value: Boolean); virtual;
    function  GetAutoDisplay: Boolean; virtual;
    procedure Change; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure Loaded; override;
    procedure Notification(AComponent: TComponent;
      Operation: TOperation); override;
    procedure WndProc(var Message: TMessage); override;
    function  FieldIsBlob: Boolean; virtual;
    function  GetFieldText: String; virtual;
    procedure SetFieldText(const AValue: String); virtual;
    function  GetNotLoadedText: String; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function ExecuteAction(Action: TBasicAction): Boolean; override;
    procedure LoadMemo; virtual;
    function UpdateAction(Action: TBasicAction): Boolean; override;
    function UseRightToLeftAlignment: Boolean; override;
    property Field: TField read GetField;
  published
    property Align;
    property Alignment;
    property Anchors;
    property AutoDisplay: Boolean read GetAutoDisplay write SetAutoDisplay default True;
    property BevelEdges;
    property BevelInner;
    property BevelOuter;
    property BevelKind;
    property BevelWidth;
    property BiDiMode;
    property BorderStyle;
    property Color;
    property Constraints;
    property Ctl3D;
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property HideSelection;    
    property ImeMode;
    property ImeName;
    property MaxLength;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
    property ScrollBars;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property WantReturns;
    property WantTabs;
    property WordWrap;
    property OnChange;
    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
  end;


procedure Register;

implementation

uses ISBasicClasses;

procedure Register;
begin
  RegisterComponents('Evolution GUI 2.0', [TisUIDBMemo]);
end;

{ TisUICustomMemo }

procedure TisUICustomMemo.WMPaint(var Message: TWMPaint);
var
  DC, MemDC: HDC;
  MemBitmap, OldBitmap: HBITMAP;
  PS: TPaintStruct;
begin
  if not FDoubleBuffered or (Message. DC <> 0) then
  begin
    if not (csCustomPaint in ControlState) and (ControlCount = 0) then
      inherited
    else
      PaintHandler(Message);
  end
  else
  begin
    DC := GetDC(0);
    MemBitmap := CreateCompatibleBitmap(DC, ClientRect.Right, ClientRect.Bottom);
    ReleaseDC(0, DC);
    MemDC := CreateCompatibleDC(0);
    OldBitmap := SelectObject(MemDC, MemBitmap);
    try
      DC := BeginPaint(Handle, PS);
      Perform(WM_ERASEBKGND, MemDC, MemDC);
      Message.DC := MemDC;
      WMPaint(Message);
      Message.DC := 0;
      BitBlt(DC, 0, 0, ClientRect.Right, ClientRect.Bottom, MemDC, 0, 0, SRCCOPY);
      EndPaint(Handle, PS);
    finally
      SelectObject(MemDC, OldBitmap);
      DeleteDC(MemDC);
      DeleteObject(MemBitmap);
    end;
  end;
  SetBorder(clBtnShadow);
//  if Self.Color <> VersionedControlColor then exit;
//  Self.Color := clWindow;
  if FGlowing then TisGlowBorder.Draw(self);
end;

procedure TisUICustomMemo.SetBorder(AColor: TColor);
var
  Canvas: TCanvas;
begin
    Canvas := TCanvas.Create;
  try
    Canvas.Handle := GetWindowDC(Handle);
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := AColor;
    Canvas.Brush.Style := bsClear;
    Canvas.Rectangle(0, 0, Width, Height);
    Canvas.Pen.Color := clWindow;                //paint the outside border
    Canvas.Rectangle(1,1,Width - 1, Height - 1); //paint the inside border white
  finally
    ReleaseDC(Handle, Canvas.Handle);
    Canvas.Free;
  end;
end;

{ TisUICustomMemo }

constructor TisUICustomMemo.Create(AOwner: TComponent);
begin
  inherited;
  FGlowing := False;
end;

procedure TisUICustomMemo.SetGlowing(Value: Boolean);
begin
  if FGlowing <> Value then
  begin
    FGlowing := Value;
    Invalidate;
  end;
end;

procedure TisUICustomMemo.SetParent(AParent: TWinControl);
begin
  if (Parent <> AParent) and FGlowing then
    TisGlowBorder.Erase(Self);
    
  inherited;
end;

procedure TisUICustomMemo.Invalidate;
begin
  if(Parent is TPanel) then
    TisGlowBorder.Erase(Self)
  else
  begin
    if FGlowing then TisGlowBorder.Erase(Self);
  end;
  inherited;
end;

procedure TisUICustomMemo.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  // Erase previous position border
  if FGlowing then
    TisGlowBorder.Erase(Self);
  inherited;
  Invalidate;
end;

{ TisUIDBMemo }

constructor TisUIDBMemo.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  inherited ReadOnly := True;
  ControlStyle := ControlStyle + [csReplicatable];
  FAutoDisplay := True;
  FDataLink := TFieldDataLink.Create;
  FDataLink.Control := Self;
  FDataLink.OnDataChange := DataChange;
  FDataLink.OnEditingChange := EditingChange;
  FDataLink.OnUpdateData := UpdateData;
  FPaintControl := TPaintControl.Create(Self, 'EDIT');
end;

destructor TisUIDBMemo.Destroy;
begin
  FPaintControl.Free;
  FDataLink.Free;
  FDataLink := nil;
  inherited Destroy;
end;

procedure TisUIDBMemo.Loaded;
begin
  inherited Loaded;
  if (csDesigning in ComponentState) then DataChange(Self);
end;

procedure TisUIDBMemo.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (FDataLink <> nil) and
    (AComponent = DataSource) then DataSource := nil;
end;

function TisUIDBMemo.UseRightToLeftAlignment: Boolean;
begin
  Result := DBUseRightToLeftAlignment(Self, Field);
end;

procedure TisUIDBMemo.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
  if FMemoLoaded then
  begin
    if (Key = VK_DELETE) or ((Key = VK_INSERT) and (ssShift in Shift)) then
      FDataLink.Edit;
  end;
end;

procedure TisUIDBMemo.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  if FMemoLoaded then
  begin
    case Key of
      ^H, ^I, ^J, ^M, ^V, ^X, #32..#255:
        FDataLink.Edit;
      #27:
        FDataLink.Reset;
    end;
  end else
  begin
    if Key = #13 then LoadMemo;
    Key := #0;
  end;
end;

procedure TisUIDBMemo.Change;
begin
  if FMemoLoaded then FDataLink.Modified;
  FMemoLoaded := True;
  inherited Change;
end;

function TisUIDBMemo.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

procedure TisUIDBMemo.SetDataSource(Value: TDataSource);
begin
  if not (FDataLink.DataSourceFixed and (csLoading in ComponentState)) then
    FDataLink.DataSource := Value;
  if Value <> nil then Value.FreeNotification(Self);
end;

function TisUIDBMemo.GetDataField: string;
begin
  Result := FDataLink.FieldName;
end;

procedure TisUIDBMemo.SetDataField(const Value: string);
begin
  FDataLink.FieldName := Value;
end;

function TisUIDBMemo.GetReadOnly: Boolean;
begin
  Result := FDataLink.ReadOnly;
end;

procedure TisUIDBMemo.SetReadOnly(Value: Boolean);
begin
  FDataLink.ReadOnly := Value;
end;

function TisUIDBMemo.GetField: TField;
begin
  Result := FDataLink.Field;
end;

procedure TisUIDBMemo.LoadMemo;
begin
  if not FMemoLoaded and FieldIsBlob then
  begin
    try
      Lines.Text := GetFieldText;
      Invalidate;
      FMemoLoaded := True;
    except
      { Memo too large }
      on E:EInvalidOperation do
        Lines.Text := Format('(%s)', [E.Message]);
    end;
    EditingChange(Self);
  end;
end;

procedure TisUIDBMemo.DataChange(Sender: TObject);
begin
  if FDataLink.Field <> nil then
    if FieldIsBlob then
    begin
      if AutoDisplay or (FDataLink.Editing and FMemoLoaded) then
      begin
        FMemoLoaded := False;
        LoadMemo;
      end else
      begin
        Text := '';
        FMemoLoaded := False;
      end;
    end else
    begin
      if FFocused and FDataLink.CanModify then
        Text := FDataLink.Field.Text
      else
        Text := FDataLink.Field.DisplayText;
      FMemoLoaded := True;
    end
  else
  begin
    if csDesigning in ComponentState then Text := Name else Text := '';
    FMemoLoaded := False;
  end;

  if HandleAllocated then
    RedrawWindow(Handle, nil, 0, RDW_INVALIDATE or RDW_ERASE or RDW_FRAME);
end;

procedure TisUIDBMemo.EditingChange(Sender: TObject);
begin
  inherited ReadOnly := not (FDataLink.Editing and FMemoLoaded);
end;

procedure TisUIDBMemo.UpdateData(Sender: TObject);
begin
  SetFieldText(Text);
end;

procedure TisUIDBMemo.SetFocused(Value: Boolean);
begin
  if FFocused <> Value then
  begin
    FFocused := Value;
    if not FieldIsBlob then
      FDataLink.Reset;
  end;
end;

procedure TisUIDBMemo.WndProc(var Message: TMessage);
begin
  with Message do
    if (Msg = WM_CREATE) or (Msg = WM_WINDOWPOSCHANGED) or
      (Msg = CM_FONTCHANGED) then FPaintControl.DestroyHandle;
  inherited;
end;

procedure TisUIDBMemo.CMEnter(var Message: TCMEnter);
begin
  SetFocused(True);
  inherited;
  if SysLocale.FarEast and FDataLink.CanModify then
    inherited ReadOnly := False;
end;

procedure TisUIDBMemo.CMExit(var Message: TCMExit);
begin
  try
    FDataLink.UpdateRecord;
  except
    SetFocus;
    raise;
  end;
  SetFocused(False);
  inherited;
end;

procedure TisUIDBMemo.SetAutoDisplay(Value: Boolean);
begin
  if FAutoDisplay <> Value then
  begin
    FAutoDisplay := Value;
    if Value then LoadMemo;
  end;
end;

procedure TisUIDBMemo.WMLButtonDblClk(var Message: TWMLButtonDblClk);
begin
  if not FMemoLoaded then LoadMemo else inherited;
end;

procedure TisUIDBMemo.WMCut(var Message: TMessage);
begin
  FDataLink.Edit;
  inherited;
end;

procedure TisUIDBMemo.WMUndo(var Message: TMessage);
begin
  FDataLink.Edit;
  inherited;
end;

procedure TisUIDBMemo.WMPaste(var Message: TMessage);
begin
  FDataLink.Edit;
  inherited;
end;

procedure TisUIDBMemo.CMGetDataLink(var Message: TMessage);
begin
  Message.Result := Integer(FDataLink);
end;

procedure TisUIDBMemo.WMPaint(var Message: TWMPaint);
var
  S: string;
  R, TextR: TRect;
  Canvas: TCanvas;
begin
  inherited;

  if (FDataLink.Field <> nil) and FieldIsBlob and not AutoDisplay and not FMemoLoaded then
  begin
    Canvas := TCanvas.Create;
    try
      Canvas.Handle := GetWindowDC(Handle);
      Canvas.Brush.Style := bsClear;
      Canvas.Font := Self.Font;
      Canvas.Font.Color := clGrayText;
      Canvas.Font.Style := [fsBold];
      S := GetNotLoadedText;
      TextR := ClientRect;
      DrawText(Canvas.Handle, PChar(S), Length(S), TextR, DT_WORDBREAK or DT_CENTER or DT_CALCRECT);

      R := ClientRect;
      OffsetRect(TextR, (R.Right - TextR.Right) div 2, (R.Bottom - TextR.Bottom) div 2);
      DrawText(Canvas.Handle, PChar(S), Length(S), TextR, DT_WORDBREAK or DT_CENTER);
    finally
      ReleaseDC(Handle, Canvas.Handle);
      Canvas.Free;
    end;
  end;
  if FGlowing then TisGlowBorder.Draw(self);
end;

function TisUIDBMemo.ExecuteAction(Action: TBasicAction): Boolean;
begin
  Result := inherited ExecuteAction(Action) or (FDataLink <> nil) and
    FDataLink.ExecuteAction(Action);
end;

function TisUIDBMemo.UpdateAction(Action: TBasicAction): Boolean;
begin
  Result := inherited UpdateAction(Action) or (FDataLink <> nil) and
    FDataLink.UpdateAction(Action);
end;

function TisUIDBMemo.FieldIsBlob: Boolean;
begin
  Result := Assigned(FDataLink.Field) and FDataLink.Field.IsBlob;
end;

function TisUIDBMemo.GetFieldText: String;
begin
  Result := FDataLink.Field.AsString;
end;

procedure TisUIDBMemo.SetFieldText(const AValue: String);
begin
  FDataLink.Field.AsString := AValue;
end;

function TisUIDBMemo.GetAutoDisplay: Boolean;
begin
  Result := FAutoDisplay;
end;

function TisUIDBMemo.GetNotLoadedText: String;
begin
  Result := Format('(%s)', [FDataLink.Field.DisplayLabel]);
end;

end.



