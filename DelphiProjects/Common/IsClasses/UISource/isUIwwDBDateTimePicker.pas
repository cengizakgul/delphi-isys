{Copyright 2012 iSystems, LLC

woll-2-woll components are critical to Evolution, as such, they will
not be re-descended from LMD components. Instead, the wwComponents
are now overriden to paint themselves like an LMD component. }
unit isUIwwDBDateTimePicker;

interface

uses
  Windows, Graphics, Messages, SysUtils, Classes, Controls, StdCtrls,
  wwdbdatetimepicker, ExtCtrls;

type
  TisUIwwDBDateTimePicker = class(TwwDBDateTimePicker)
  private
    { Private declarations }
    FGlowing: Boolean;
  protected
    { Protected declarations }
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure SetBorder(AColor: TColor);
    procedure SetGlowing(Value: Boolean);
    procedure SetParent(AParent: TWinControl); override;
  public
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure Invalidate; override;
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    Property Glowing: Boolean read FGlowing write SetGlowing default False;
  end;

procedure Register;

implementation
uses ISBasicClasses;

procedure Register;
begin
  RegisterComponents('Evolution GUI 2.0', [TisUIwwDBDateTimePicker]);
end;

{ TisUIwwDBDateTimePicker }


{ TisUIwwDBDateTimePicker }

constructor TisUIwwDBDateTimePicker.Create(AOwner: TComponent);
begin
  inherited;
  FGlowing := False;
end;

procedure TisUIwwDBDateTimePicker.Invalidate;
begin
  if(Parent is TPanel) then
    TisGlowBorder.Erase(Self)
  else
  begin
    if FGlowing then TisGlowBorder.Erase(Self);
  end;
  inherited;
end;

procedure TisUIwwDBDateTimePicker.SetBorder(AColor: TColor);
var
  Canvas: TCanvas;
begin
    Canvas := TCanvas.Create;
  try
    Canvas.Handle := GetWindowDC(Handle);
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := AColor;
    Canvas.Brush.Style := bsClear;
    Canvas.Rectangle(0, 0, Width, Height);
    Canvas.Pen.Color := clWindow;                 //Draw the outer rectangle
    Canvas.Rectangle(1,1,Width - 1, Height - 1);  //wipe out the existing 3D
  finally
    ReleaseDC(Handle, Canvas.Handle);
    Canvas.Free;
  end;

end;

procedure TisUIwwDBDateTimePicker.SetBounds(ALeft, ATop, AWidth,
  AHeight: Integer);
begin
  // Erase previous position border
  if FGlowing then
    TisGlowBorder.Erase(Self);
  inherited;
  Invalidate;
end;

procedure TisUIwwDBDateTimePicker.SetGlowing(Value: Boolean);
begin
  if FGlowing <> Value then
  begin
    FGlowing := Value;
    Invalidate;
  end;
end;

procedure TisUIwwDBDateTimePicker.SetParent(AParent: TWinControl);
begin
  if (Parent <> AParent) and FGlowing then
    TisGlowBorder.Erase(Self);
    
  inherited;
end;

procedure TisUIwwDBDateTimePicker.WMPaint(var Message: TWMPaint);
begin
  inherited;
  SetBorder(clBtnShadow);
  if FGlowing then TisGlowBorder.Draw(self);
end;

end.




