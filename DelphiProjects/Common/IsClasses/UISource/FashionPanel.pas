{Copyright 2012 iSystems, LLC

This is a neat control that paints itself like 2 panels, arranged
in a way to simulate drop shadows, along with a title bar and title font. }

unit FashionPanel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls;

type

	TThemeType = ( ttBlue, ttPurple, ttGreen, ttYellow, ttJulianYerfino,
                 ttMatelec, ttMRitmo, ttCustom );

  TFashionPanel  = class(TPanel)
  private
    { Private declarations }
    FRoundRect  : boolean;
    FTitleColor : TColor;
    FTitleFont  : TFont;
    FTitle : String;
    FLineColor : TColor;
    FLineWidth : integer;
    FTheme : TThemeType;
    FShadowColor : TColor;
    FShowShadow  : boolean;
    FShadowDepth : integer;
    FromPaint : boolean;
    FShadowSpace : integer;
    FAbout : string;
    procedure SetTitleColor(const Value: TColor);
    procedure SetTitleFont(const Value: TFont);
    procedure SetTitle(const Value: String);
    procedure SetLineColor(const Value: TColor);
    procedure SetTheme(const Value: TThemeType);
    procedure SetShadowColor(const Value: TColor);
    procedure SetShowShadow(const Value: boolean);
    procedure SetShadowDepth(const Value: integer);
    procedure SetShadowSpace(const Value: integer);
    procedure	FontChanged ( Sender : TObject) ;
    procedure SetAbout(const Value: string);
    procedure SetRoundRect(const Value: boolean);
    procedure SetLineWidth(const Value: integer);
  protected
    { Protected declarations }
    procedure Paint;override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy;override;
  published
    { Published declarations }
    property RoundRect  : boolean  read FRoundRect write SetRoundRect;
		property About			: string  read FAbout write SetAbout;
    property ShadowDepth: integer read FShadowDepth write SetShadowDepth;
    property ShadowSpace: integer read FShadowSpace write SetShadowSpace;
    property ShowShadow : boolean read FShowShadow write SetShowShadow;
    property ShadowColor: TColor  read FShadowColor write SetShadowColor;
    property TitleColor : TColor  read FTitleColor write SetTitleColor;
    property TitleFont  : TFont   read FTitleFont  write SetTitleFont;
    property Title 		  : String  read FTitle write SetTitle;
    property LineWidth  : integer read FLineWidth write SetLineWidth;
    property LineColor  : TColor  read FLineColor write SetLineColor;
    property Theme		  : TThemeType read FTheme write SetTheme;
  end;

implementation

{ TFashionPanel  }

constructor TFashionPanel .Create(AOwner: TComponent);
begin
  inherited Create( AOwner );
  FAbout := '';
  BevelOuter := bvNone;
  Color := $00E3DED5;
	FShadowSpace := 5;
  FRoundRect := true;
	FShadowColor := clBlack;
  FShowShadow := false;
	FShadowDepth := 2;
	FTitleColor := RGB ( 0, 128, 192 );
  FTitleFont := TFont.Create;
  FTitleFont.Name := 'Arial';
  FTitleFont.Color := clWhite;
  FTitleFont.Style := FTitleFont.Style + [fsBold];
  FTitleFont.OnChange := FontChanged;
  FTitle := 'Title';
  FLineWidth := 0;
  FLineColor := clBlack;
  FTheme := ttBlue;
  FromPaint := false;
end;

destructor TFashionPanel .Destroy;
begin
	FTitleFont.Free;
  inherited Destroy;
end;

procedure TFashionPanel .FontChanged ( Sender : TObject) ;
begin
	Paint();
end;

procedure TFashionPanel .Paint;
var
  Rect: TRect;
	otherRect : TRect;
begin
	inherited Paint();

  //Everything on the parent gets painted
  Rect := GetClientRect;
	Canvas.Brush.Color := Parent.Brush.Color;
	Canvas.FillRect( Rect );

	//The Shadow
	if ( FShowShadow ) then
  begin
    //Build the shadow rectangle
  	Canvas.Brush.Color := FShadowColor;
    Canvas.Brush.Style := bsSolid;
    Rect.Top := Rect.Top + FShadowSpace;
    Rect.Left := Rect.Left  + FShadowSpace;
    if ( FRoundRect ) then
    begin
      Canvas.RoundRect(Rect.Left, Rect.Top, Rect.Right, Rect.Bottom, 10, 10);
    end else
      Canvas.Rectangle(Rect);
    //Reset the rectangle to where the body will be.
    Rect.Top := Rect.Top - FShadowSpace;
    Rect.Left := Rect.Left  - FShadowSpace;
    Rect.Right := Rect.Right - FShadowDepth;
    Rect.Bottom := Rect.Bottom - FShadowDepth;
  end;

  //Draw the panel outline
  Canvas.Brush.Color := FLineColor;
  Canvas.Pen.Color := FLineColor;
  if ( FRoundRect ) then
  begin
    Canvas.RoundRect( Rect.Left, Rect.Top,
                      Rect.Right, Rect.Bottom, 10, 10 );
    //top rounded corners
    {Canvas.RoundRect( Rect.Left, Rect.Top,
                      Rect.Right, Rect.Top + 20, 10, 10 );
    //to make the title/body intersection be rectangular
    Canvas.Rectangle( Rect.Left, ClientRect.Top + 10,
                      Rect.Right, Rect.Bottom - 10);
    //bottom rounded corners
    Canvas.RoundRect( Rect.Left, Rect.Bottom + 10,
                      Rect.Right, Rect.Bottom, 10, 10 ); }
  end
  else
    Canvas.Rectangle( Rect.Left, ClientRect.Top ,
                      Rect.Right, Rect.Bottom );

  Rect.Right := Rect.Right - FLineWidth;
  Rect.Left := Rect.Left + FLineWidth;
  Rect.Top := Rect.Top + FLineWidth;
  Rect.Bottom := Rect.Bottom - FLineWidth;

  otherRect := Rect; otherRect.Top := otherrect.Top + 19;

  //Body Fill
  with Canvas do
  begin
    Brush.Color := Color;
    Pen.Style := psClear;
    pen.Color := Color;
    //create a rectangular interface with the titel
    if (FRoundRect) then
    begin
      RoundRect(otherRect.Left, otherRect.Top, otherRect.Right, otherRect.Bottom, 10, 10);
      Rectangle(otherRect.Left, otherRect.Top, otherRect.Right, otherRect.top + 10);
    end else
      Rectangle(otherRect);
  end;

  //Build the title
  Canvas.Brush.Color := FTitleColor;
  Canvas.Pen.Color := FTitleColor;
  if ( FRoundRect ) then
  begin
    Canvas.RoundRect( Rect.Left, Rect.Top,
                      Rect.Right, Rect.Top + 19 - FLineWidth, 10, 10 );
    Canvas.Rectangle( Rect.Left, ClientRect.Top + 10,
                      Rect.Right, Rect.Top + 19 - FLineWidth );
  end
  else
  	Canvas.Rectangle( Rect.Left, Rect.Top,
   							 		  Rect.Right, Rect.Top + 19 - FLineWidth);


  // Title
	Canvas.Font.Assign(FTitleFont);
  Canvas.TextOut( Rect.Left + 10, Rect.Top + 2, FTitle );
  Canvas.Pen.Style := psSolid;

end;

procedure TFashionPanel .SetAbout(const Value: string);
begin
//  FAbout := Value;
end;


procedure TFashionPanel .SetLineColor(const Value: TColor);
begin
  FLineColor := Value;
  FTheme := ttCustom;
  Paint();
end;

procedure TFashionPanel .SetLineWidth(const Value: integer);
begin
  FLineWidth := Value;
  Paint();
end;

procedure TFashionPanel .SetRoundRect(const Value: boolean);
begin
  FRoundRect := Value;
  Paint();
end;

procedure TFashionPanel .SetShadowColor(const Value: TColor);
begin
  FShadowColor := Value;
  FTheme := ttCustom;
  Paint();
end;

procedure TFashionPanel .SetShadowDepth(const Value: integer);
begin
  FShadowDepth := Value;
  FTheme := ttCustom;
  Paint();
end;

procedure TFashionPanel .SetShadowSpace(const Value: integer);
begin
  FShadowSpace := Value;
  FTheme := ttCustom;
  Paint();
end;

procedure TFashionPanel .SetShowShadow(const Value: boolean);
begin
  FShowShadow := Value;
  FTheme := ttCustom;
  Paint();
end;

procedure TFashionPanel .SetTheme(const Value: TThemeType);
begin
  FTheme := Value;
  case FTheme of
		ttBlue 	 : begin
					      Color := $00E3DED5;
								FTitleColor := RGB ( 0, 128, 192 );
                FShowShadow := false;
                FLineWidth := 0;
							  FTitleFont.Color := clWhite;
    					 end;
    ttPurple : begin
      					Color := $00CBC5C8;
                FTitleColor := clPurple;
                FShowShadow := false;
                FLineWidth := 0;
                FTitleFont.Color := clWhite;
    					 end;
    ttGreen  : begin
    						Color := $00B9CEBB;
                FTitleColor := clGreen;
                FShowShadow := false;
                FLineWidth := 0;
                FTitleFont.Color := clWhite;
    					 end;
    ttYellow : begin
								Color := $00BED2D8;
    						FTitleColor := $00B9FFFF;
                FTitleFont.Color := clBlack;
                FShowShadow := false;
                FLineWidth := 0;
    					 end;
		ttJulianYerfino :
    					begin
 								Color := $00B9CDD0;
    						FTitleColor := clGray;
                FLineColor := clGray;
                FShowShadow := true;
                FLineWidth := 1;
                FShadowDepth := 3;
                FShadowSpace := 5;
                FTitleFont.Color := clWhite;
								if ( FTitle = 'Title' ) then
	                FTitle := 'Julian Yerfino <yerfinojul@ufasta.edu.ar> Programming';
    					end;
  	ttMatelec: begin
								Color := RGB( 231, 232, 219);
    						FTitleColor := RGB( 141, 141, 141);
                FLineColor := clGray;
							  FTitleFont.Color := clWhite;
                FShowShadow := false;
                FLineWidth := 1;
                FLineColor := FTitleColor;
               end;
    ttMRitmo : begin
 								Color := clGray;;
    						FTitleColor := clBlack;
                FLineColor := clSilver;
                FShowShadow := true;
                FLineWidth := 1;
                FTitleFont.Color := $000080FF;
                FShadowDepth := 4;
                FShadowSpace := 10;
								if ( FTitle = 'Title' ) then
	                FTitle := 'MRitmo <vazquezger@ufasta.edu.ar> Help';
    				   end;
  end;
  Paint();
end;

procedure TFashionPanel .SetTitle(const Value: String);
begin
  FTitle := Value;
  Paint();
end;

procedure TFashionPanel .SetTitleColor(const Value: TColor);
begin
	FTitleColor := Value;
  FTheme := ttCustom;
  Paint();
end;

procedure TFashionPanel .SetTitleFont(const Value: TFont);
begin
	FTitleFont.Assign(Value);
  FTheme := ttCustom;  
end;

end.
