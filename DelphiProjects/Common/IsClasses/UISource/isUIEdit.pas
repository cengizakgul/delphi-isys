unit isUIEdit;

interface

uses
  Windows, Graphics, Messages, SysUtils, Classes, StdCtrls, Controls, ExtCtrls;

type
  TisUIEdit = class(TEdit)
  private
    { Private declarations }
    FGlowing : Boolean;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure SetBorder(AColor: TColor);
    procedure SetGlowing(Value: Boolean);
  protected
    { Protected declarations }
    procedure SetParent(AParent: TWinControl); override;
  public
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure Invalidate; override;
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    Property Glowing: Boolean read FGlowing write SetGlowing default False;
  end;

procedure Register;

implementation
uses ISBasicClasses;

procedure Register;
begin
  RegisterComponents('Evolution GUI 2.0', [TisUIEdit]);
end;

{ TisUIEdit }


{ TisUIEdit }

constructor TisUIEdit.Create(AOwner: TComponent);
begin
  inherited;
  FGlowing := False;
end;

procedure TisUIEdit.Invalidate;
begin
  if(Parent is TPanel) then
    TisGlowBorder.Erase(Self)
  else
  begin
    if FGlowing then TisGlowBorder.Erase(Self);
  end;
  inherited;
end;

procedure TisUIEdit.SetBorder(AColor: TColor);
var
  Canvas: TCanvas;
begin
    Canvas := TCanvas.Create;
  try
    Canvas.Handle := GetWindowDC(Handle);
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := AColor;
    Canvas.Brush.Style := bsClear;
    Canvas.Rectangle(0, 0, Width, Height);
    Canvas.Pen.Color := clWindow;                 //Draw the outer rectangle
    Canvas.Rectangle(1,1,Width - 1, Height - 1);  //wipe out the existing 3D
  finally
    ReleaseDC(Handle, Canvas.Handle);
    Canvas.Free;
  end;

end;

procedure TisUIEdit.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  // Erase previous position border
  if FGlowing then
    TisGlowBorder.Erase(Self);
  inherited;
  Invalidate;
end;

procedure TisUIEdit.SetGlowing(Value: Boolean);
begin
  if FGlowing <> Value then
  begin
    FGlowing := Value;
    Invalidate;
  end;
end;

procedure TisUIEdit.SetParent(AParent: TWinControl);
begin
  if (Parent <> AParent) and FGlowing then
    TisGlowBorder.Erase(Self);

  inherited;
end;

procedure TisUIEdit.WMPaint(var Message: TWMPaint);
begin
  inherited;
  SetBorder(clBtnShadow);
  if FGlowing then TisGlowBorder.Draw(Self);
end;

end.



