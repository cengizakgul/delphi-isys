unit isUILMDButton;

interface

uses
  SysUtils, Classes, Controls, StdCtrls, LMDCustomButton, LMDButton, Buttons,
  Graphics;

type
  TisUILMDButton = class(TLMDButton)
  private
    { Private declarations }
    FKind          : TBitBtnKind;
    FMargin        : Integer;
    FLayout        : TButtonLayout;
    FStyle         : TButtonStyle;
    FColor         : TColor;
    procedure SetMargin(Value: Integer);
    procedure SetLayout(Value: TButtonLayout);
    procedure SetStyle(Value: TButtonStyle);
    function GetKind: TBitBtnKind;
    procedure SetKind(const Value: TBitBtnKind);
    procedure SetColor(const Value: TColor);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    property Layout: TButtonLayout read FLayout write SetLayout default blGlyphLeft;
    property Margin: Integer read FMargin write SetMargin default -1;
    property Style: TButtonStyle read FStyle write SetStyle default bsAutoDetect;
    property Kind: TBitBtnKind read GetKind write SetKind default bkCustom;
    property Color: TColor read FColor write SetColor default clBtnFace;
    property ParentColor default False; //Reso #  97273
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Evolution GUI 2.0', [TisUILMDButton]);
end;

{ TisUILMDButton }

constructor TisUILMDButton.Create(AOwner: TComponent);
begin
  inherited;
  //Universally turn off ParentColor property. It looks bad
  //on the login screen and other places where the parent is not
  //a typical control, like a tpanel
  //Reso #  97273
  ParentColor := False;
  
end;

function TisUILMDButton.GetKind: TBitBtnKind;
begin
  Result := FKind;
end;

procedure TisUILMDButton.SetColor(const Value: TColor);
begin
  if FColor <> Value then
  begin
    FColor := Value;
  end;
end;

procedure TisUILMDButton.SetKind(const Value: TBitBtnKind);
begin
  FKind := Value;
end;

procedure TisUILMDButton.SetLayout(Value: TButtonLayout);
begin
  if FLayout <> Value then
  begin
    FLayout := Value;
    Invalidate;
  end;
end;

procedure TisUILMDButton.SetMargin(Value: Integer);
begin
  if (Value <> FMargin) and (Value >= -1) then
  begin
    FMargin := Value;
    Invalidate;
  end;
end;

procedure TisUILMDButton.SetStyle(Value: TButtonStyle);
begin
  if Value <> FStyle then
  begin
    FStyle := Value;
    Invalidate;
  end;
end;

end.

