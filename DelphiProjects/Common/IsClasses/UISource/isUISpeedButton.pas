{Compatibility component to allow isSpeedButton to descend
from an LMD Ancestor}
unit isUISpeedButton;

interface

uses
  SysUtils, Classes, Controls, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, Buttons,
  ActnPopup, Menus, Messages, Graphics;

type
  TisUISpeedButton = class(TLMDSpeedButton)
  private
    { Private declarations }
//    FGroupIndex    : Integer;
    //FDown          : Boolean;
    FMargin        : Integer;
    FFlat          : Boolean;
    FLayout        : TButtonLayout;
    FSpacing       : Integer;
    FTransparent   : Boolean;
    //FDropDown      : TPopupMenu;
    //procedure SetTransparent(Value: Boolean);
//    procedure UpdateExclusive;
//    procedure SetGroupIndex(Value: Integer);
    procedure SetMargin(Value: Integer);
    //procedure SetDown(Value: Boolean);
    procedure SetFlat(Value: Boolean);
    procedure SetLayout(Value: TButtonLayout);
//    procedure SetSpacing(Value: Integer);
    procedure SetTransparent(Value: Boolean);
  protected
    { Protected declarations }
    procedure Paint; override;
    procedure SetParent(AParent: TWinControl); override;
  public
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure Invalidate; override;
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Click; override;

  published
    { Published declarations }
    property Layout: TButtonLayout read FLayout write SetLayout default blGlyphLeft;
    property Transparent: Boolean read FTransparent write SetTransparent default False;
    property Margin: Integer read FMargin write SetMargin default -1;
    property Flat: Boolean read FFlat write SetFlat default False;
    property PopupMenu;
    //parent color had to be exposed to solve Reso # 97273
    property ParentColor;
  end;

procedure Register;

implementation
uses isBasicClasses;

procedure Register;
begin
  RegisterComponents('Evolution GUI 2.0', [TisUISpeedButton]);
end;

{ TisUISpeedButton }

procedure TisUISpeedButton.Click;
begin
  inherited Click;

end;

constructor TisUISpeedButton.Create(AOwner: TComponent);
begin
  inherited;
  FSpacing := 4;
  FMargin := -1;
  FLayout := blGlyphLeft;
  FTransparent := False;
  //Universally turn off ParentColor property. It looks bad
  //on the login screen and other places where the parent is not
  //a typical control, like a tpanel
  //Reso #  97273
  ParentColor := False;
  
end;

destructor TisUISpeedButton.Destroy;
begin

  inherited;
end;

{procedure TisUISpeedButton.SetDown(Value: Boolean);
begin
  if FGroupIndex = 0 then Value := False;
  if Value <> FDown then
  begin
    if FDown and (not FAllowAllUp) then Exit;
    FDown := Value;
    if Value then
    begin
      if FState = bsUp then Invalidate;
      FState := bsExclusive
    end
    else
    begin
      FState := bsUp;
      Repaint;
    end;
    if Value then UpdateExclusive;
  end;  
end;   }

procedure TisUISpeedButton.Invalidate;
begin
  TisGlowBorder.Erase(Self);
  inherited;
end;

procedure TisUISpeedButton.Paint;
begin
  inherited;
  if self.Tag = 99999 then TisGlowBorder.Draw(self);
end;

procedure TisUISpeedButton.SetBounds(ALeft, ATop, AWidth,
  AHeight: Integer);
begin
  // Erase previous position border
  if Self.Tag = 99999 then
    TisGlowBorder.Erase(Self);
  inherited;
  Invalidate;
end;

procedure TisUISpeedButton.SetFlat(Value: Boolean);
begin
  if Value <> FFlat then
  begin
    FFlat := Value;
    Invalidate;
  end;
end;

{
procedure TisUISpeedButton.SetGroupIndex(Value: Integer);
begin
  if FGroupIndex <> Value then
  begin
    FGroupIndex := Value;
    UpdateExclusive;
  end;
end;
}

procedure TisUISpeedButton.SetLayout(Value: TButtonLayout);
begin
  if FLayout <> Value then
  begin
    FLayout := Value;
    Invalidate;
  end;
end;

procedure TisUISpeedButton.SetMargin(Value: Integer);
begin
  if (Value <> FMargin) and (Value >= -1) then
  begin
    FMargin := Value;
    Invalidate;
  end;
end;

{
procedure TisUISpeedButton.SetSpacing(Value: Integer);
begin
  if Value <> FSpacing then
  begin
    FSpacing := Value;
    Invalidate;
  end;
end;
}

procedure TisUISpeedButton.SetParent(AParent: TWinControl);
begin
  if (Parent <> AParent) and (Self.Tag = 99999) then
    TisGlowBorder.Erase(Self);

  inherited;
end;

procedure TisUISpeedButton.SetTransparent(Value: Boolean);
begin
  if Value <> FTransparent then
  begin
    FTransparent := Value;
    if Value then
      ControlStyle := ControlStyle - [csOpaque] else
      ControlStyle := ControlStyle + [csOpaque];
    Invalidate;
  end;
end;

{
procedure TisUISpeedButton.UpdateExclusive;
var
  Msg: TMessage;
begin
  if (FGroupIndex <> 0) and (Parent <> nil) then
  begin
    Msg.Msg := CM_BUTTONPRESSED;
    Msg.WParam := FGroupIndex;
    Msg.LParam := Longint(Self);
    Msg.Result := 0;
    Parent.Broadcast(Msg);
  end;
end;
}

end.



