unit isUILMDDBRadioGroup;

interface

uses
  SysUtils, Classes, Controls, LMDControl, LMDCustomControl,
  LMDCustomPanel, LMDCustomBevelPanel, LMDCustomParentPanel,
  LMDCustomGroupBox, LMDCustomButtonGroup, LMDCustomRadioGroup,
  LMDDBRadioGroup;

type
  TisUILMDDBRadioGroup = class(TLMDDBRadioGroup)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Evolution GUI 2.0', [TisUILMDDBRadioGroup]);
end;

end.
