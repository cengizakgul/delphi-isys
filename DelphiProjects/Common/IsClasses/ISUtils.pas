unit ISUtils;

interface

uses
  Windows, Classes, ISBasicClasses, Dialogs, Controls, syncobjs, SysUtils, Forms, Types,
  Graphics, ISDataAccessComponents, Registry, isExceptions, isTypes, isBasicUtils, Math,
  EvStreamUtils, ShellAPI;

function ShadeColor(InColor : TColor; ShadingDepth: Integer) : TColor;

function CreateLookupDataset(const ds: TISBasicClientDataSet; const Owner: TComponent): TISBasicClientDataSet;
function CreateLookupData(const ds: TISBasicClientDataSet): Variant;

procedure ScreenShot(x : integer;
                     y : integer;
                     Width : integer;
                     Height : integer;
                     bm : TBitMap);
//Show messages
function  ISMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons;
                    DefaultButton: TMsgDlgBtn = mbOK; HelpCtx: Integer = 0; const IncludeScreen: Boolean = False): Word; overload;
function  ISMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; Captions: TStrings;
                    DefaultButton: TMsgDlgBtn = mbOK; HelpCtx: Integer = 0; const IncludeScreen: Boolean = False): Word; overload;
procedure ISMessage(const Msg: string; HelpCtx: Integer = 0); overload;
procedure ISErrMessage(const Msg: string; HelpCtx: Integer = 0; const IncludeScreen: Boolean = False);
procedure ISExceptMessage(const AException: Exception; const IncludeScreen: Boolean = False);
function ISDialog(const ACaption, APrompt: string; var Value: string; ASecure: Boolean = False): Boolean;
function ISNbrDialog(const ACaption, APrompt: string; var Value: Extended;
                     AIncrement: Extended = 1; AMinVal: Extended = 0; AMaxVal: Extended = 0): Boolean;
function ISChboxDialog(const ACaption, APrompt, ALabel: string;
                     var Checked: Boolean): Boolean;
function ISDateDialog(const ACaption, APrompt: string; var Value: TDate ): Boolean;
function ISDefaultDialog(const ACaption, APrompt, ADefault: string): string;

function AreYouSure(const AQuestion: String = 'Are you sure you want to delete this item?'): Boolean;

function StrPCopySafe(Dest: PChar; const Source: string; const DestBufSize: Cardinal): PChar;
function StrCopySafe(Dest: PChar; const Source: PChar; const DestBufSize: Cardinal): PChar;

function StrToChar(const s: string): Char;

function CheckEscape: boolean;

procedure DisableProcessWindowsGhosting;
function  GetIBPath: String;

function VersionRecToStr(const AVerInfo: TisVersionInfo): String;
function StrToVersionRec(const AVerStr: String): TisVersionInfo;
function CompareVersions(const AVer1, AVer2: String): TValueRelationship; overload;
function CompareVersions(const AVer1, AVer2: TisVersionInfo): TValueRelationship; overload;

function GetFileIcon(const AFileName: String): IisStream;
function IconToBitmap(const AIcon: IisStream): IisStream;

implementation

uses
  messages,  SShowMessage;


function AreYouSure(const AQuestion: String): Boolean;
begin
  Result := MessageDlg(AQuestion,  mtConfirmation, [mbYes, mbNo], 0) = mrYes;
end;

function StrPCopySafe(Dest: PChar; const Source: string; const DestBufSize: Cardinal): PChar;
begin
  if Cardinal(Length(Source)) + 1 > DestBufSize then
    raise ERangeError.Create('Cannot perform string copy operation. Allocated buffer is too small.');

  Result := StrPCopy(Dest, Source);
end;

function StrCopySafe(Dest: PChar; const Source: PChar; const DestBufSize: Cardinal): PChar;
begin
  if Cardinal(StrLen(Source)) + 1 > DestBufSize then
    raise ERangeError.Create('Cannot perform string copy operation. Allocated buffer is too small.');

  Result := StrCopy(Dest, Source);
end;

function StrToChar(const s: string): Char;
begin
  Assert(Length(s) >= 1);
  Result := s[1];
end;

function CreateLookupDataset(const ds: TISBasicClientDataSet; const Owner: TComponent): TISBasicClientDataSet;
begin
  Result := TISBasicClientDataSet.Create(Owner);
  Result.Data := CreateLookupData(ds);
end;

function CreateLookupData(const ds: TISBasicClientDataSet): Variant;
begin
  with TISProvider.Create(nil) do
  try
    DataSet := ds;
    Result := Data;
  finally
    Free;
  end;
end;

function ISMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; DefaultButton: TMsgDlgBtn = mbOK; HelpCtx: Integer = 0; const IncludeScreen: Boolean = False): Word;
begin
  Result := TShowMessage.ShowMessage(Msg, DlgType, Buttons, DefaultButton, HelpCtx, IncludeScreen);
end;

function ISMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; Captions: TStrings; DefaultButton: TMsgDlgBtn = mbOK; HelpCtx: Integer = 0; const IncludeScreen: Boolean = False): Word;
begin
  Result := TShowMessage.ShowMessageEx(Msg, DlgType, Buttons, Captions, DefaultButton, HelpCtx, IncludeScreen);
end;

procedure ISMessage(const Msg: string; HelpCtx: Integer = 0); overload;
begin
  ISMessage(Msg, mtCustom, [mbOK], mbOK, HelpCtx);
end;


procedure ISErrMessage(const Msg: string; HelpCtx: Integer = 0; const IncludeScreen: Boolean = False);
begin
  ISMessage(Msg, mtError, [mbOK], mbOK, HelpCtx, IncludeScreen);
end;

procedure ISExceptMessage(const AException: Exception; const IncludeScreen: Boolean = False);
begin
  if AException is EisException then
    TShowMessage.ShowException(EisException(AException), IncludeScreen)
  else
    ISErrMessage(AException.Message, AException.HelpContext, IncludeScreen);
end;

function ISDialog(const ACaption, APrompt: string; var Value: string; ASecure: Boolean = False): Boolean;
begin
  Result := TShowMessage.ShowDialog(ACaption, APrompt, Value, ASecure);
end;

function ISNbrDialog(const ACaption, APrompt: string; var Value: Extended;
                     AIncrement: Extended = 1; AMinVal: Extended = 0; AMaxVal: Extended = 0): Boolean;
begin
  Result := TShowMessage.ShowNbrDialog(ACaption, APrompt, Value, AIncrement, AMinVal, AMaxVal);
end;

function ISChboxDialog(const ACaption, APrompt, ALabel: string;
  var Checked: Boolean): Boolean;
begin
  Result := TShowMessage.ShowChboxDialog(ACaption, APrompt, ALabel, Checked);
end;

function ISDateDialog(const ACaption, APrompt: string; var Value: TDate ): Boolean;
begin
  Result := TShowMessage.ShowDateDialog(ACaption, APrompt, Value);
end;

function ISDefaultDialog(const ACaption, APrompt, ADefault: string): string;
begin
  if not ISDialog(ACaption, APrompt, Result) then
    Result := ADefault;
end;

procedure ScreenShot(x : integer;
                     y : integer;
                     Width : integer;
                     Height : integer;
                     bm : TBitMap);
var
  dc: HDC;
  lpPal : PLOGPALETTE;
begin
 {test width and height}
  if ((Width < 1) OR
      (Height < 1)) then begin
    exit;
  end;
  bm.Width := Width;
  bm.Height := Height;
 {get the screen dc}
  dc := GetDc(0);
  if (dc = 0) then begin
    exit;
  end;
 {do we have a palette device?}
  if (GetDeviceCaps(dc, RASTERCAPS) AND
      RC_PALETTE = RC_PALETTE) then begin
   {allocate memory for a logical palette}
    GetMem(lpPal,
           sizeof(TLOGPALETTE) +
           (255 * sizeof(TPALETTEENTRY)));
   {zero it out to be neat}
    FillChar(lpPal^,
             sizeof(TLOGPALETTE) +
             (255 * sizeof(TPALETTEENTRY)),
             #0);
   {fill in the palette version}
    lpPal^.palVersion := $300;
   {grab the system palette entries}
    lpPal^.palNumEntries :=
      GetSystemPaletteEntries(dc,
                              0,
                              256,
                              lpPal^.palPalEntry);
    if (lpPal^.PalNumEntries <> 0) then begin
     {create the palette}
      bm.Palette := CreatePalette(lpPal^);
    end;
    FreeMem(lpPal, sizeof(TLOGPALETTE) +
            (255 * sizeof(TPALETTEENTRY)));
  end;
 {copy from the screen to the bitmap}
  BitBlt(bm.Canvas.Handle,
         0,
         0,
         Width,
         Height,
         Dc,
         x,
         y,
         SRCCOPY);
 {release the screen dc}
  ReleaseDc(0, dc);
end;

function ShadeColor(InColor : TColor; ShadingDepth: Integer) : TColor;
var
  b1, b2, b3: Byte;
begin
 Result := ColorToRGB(InColor);
 b1 := Result mod 256;
 b2 := Result div 256 mod 256;
 b3 := Result div 256 div 256 mod 256;
 if b1 * ShadingDepth div 6 > 255 then
   b1 := 255
 else
   b1 := b1 * ShadingDepth div 6;
 if b2 * ShadingDepth div 6 > 255 then
   b2 := 255
 else
   b2 := b2 * ShadingDepth div 6;
 if b3 * ShadingDepth div 6 > 255 then
   b3 := 255
 else
   b3 := b3 * ShadingDepth div 6;
 Result := b1 + b2 * 256 + b3 * 256 * 256;
end;

function CheckEscape: boolean;
var
  msg: TMsg;
begin
  Result := false;
  while PeekMessage( msg, 0, WM_KEYFIRST, WM_KEYLAST, PM_REMOVE ) do
    if ( msg.message = WM_KEYUP ) and (msg.wParam = VK_ESCAPE) then
      Result := true;
end;

function GetIBPath: String;
begin
  with TRegistry.Create do
  try
    RootKey := HKEY_LOCAL_MACHINE;
    Access := KEY_READ;
    if OpenKey('Software\Firebird Project\Firebird Server\Instances', False) then
      Result := ReadString('DefaultInstance') + 'bin\'
    else if OpenKey('Software\FirebirdSQL\Firebird\CurrentVersion', False) then
      Result := ReadString('RootDirectory') + 'bin\'
    else if OpenKey('Software\Borland\Interbase\CurrentVersion', False) then
      Result := ReadString('RootDirectory') + 'bin\'
    else
      Result := ExtractFilePath(Application.ExeName);
  finally
    Free;
  end;
end;

function VersionRecToStr(const AVerInfo: TisVersionInfo): String;
begin
  Result := IntToStr(AVerInfo.Major) + '.' + IntToStr(AVerInfo.Minor) + '.' + IntToStr(AVerInfo.Patch) + '.' + IntToStr(AVerInfo.Build);
end;

function StrToVersionRec(const AVerStr: String): TisVersionInfo;
var
  s: String;
begin
  s := AVerStr;
  Result.Major := StrToIntDef(GetNextStrValue(s, '.'), 0);
  Result.Minor := StrToIntDef(GetNextStrValue(s, '.'), 0);
  Result.Patch := StrToIntDef(GetNextStrValue(s, '.'), 0);
  Result.Build := StrToIntDef(s, 0);
end;

function CompareVersions(const AVer1, AVer2: String): TValueRelationship;
begin
  Result := CompareVersions(StrToVersionRec(AVer1), StrToVersionRec(AVer2));
end;

function CompareVersions(const AVer1, AVer2: TisVersionInfo): TValueRelationship;
var
  v1, v2: Int64;
begin
  // xxx.xxx.xxx.xxx
  v1 := AVer1.Major * 1000000 + AVer1.Minor * 10000 + AVer1.Patch * 100 + AVer1.Build;
  v2 := AVer2.Major * 1000000 + AVer2.Minor * 10000 + AVer2.Patch * 100 + AVer2.Build;
  Result := CompareValue(v1, v2)
end;

//RE 34513
//http://qc.borland.com/qc/wc/qcmain.aspx?d=3700
procedure DisableProcessWindowsGhosting;
var
  DisableProcessWindowsGhostingImp: procedure;
begin
  @DisableProcessWindowsGhostingImp := GetProcAddress(GetModuleHandle('user32.dll'), 'DisableProcessWindowsGhosting');
  if @DisableProcessWindowsGhostingImp <> nil then
    DisableProcessWindowsGhostingImp;
end;

function GetFileIcon(const AFileName: String): IisStream;
var
  s: String;
  iconIndex: Word;
  IconHandle: HICON;
  Icon: TIcon;
begin
  iconIndex := 0;
  s := ExtractFileExt(AFileName);
  if AnsiSameText(s, '.EXE') or AnsiSameText(s, '.DLL') then
    IconHandle := ExtractIcon(HInstance, PAnsiChar(AFileName), iconIndex)
  else
    IconHandle := 0;

  if IconHandle = 0 then
  begin
    s := AFileName + StringOfChar(#0, 1024);
    IconHandle := ExtractAssociatedIcon(HInstance, PAnsiChar(s), iconIndex);
  end;

  if IconHandle <> 0 then
  begin
    Icon := TIcon.Create;
    try
      Icon.Handle := IconHandle;
      Result := TisStream.Create;
      Icon.SaveToStream(Result.RealStream);
      Result.Position := 0;
    finally
      Icon.Free;
    end;
  end;
end;

function IconToBitmap(const AIcon: IisStream): IisStream;
var
  Icon: TIcon;
  Bitmap: TBitmap;
begin
  if not Assigned(AIcon) or (AIcon.Size = 0) then
    Exit;

  Icon := TIcon.Create;
  AIcon.Position := 0;
  Icon.LoadFromStream(AIcon.RealStream);
  Bitmap := TBitmap.Create;
  try
    Bitmap.Width := Icon.Width;
    Bitmap.Height := Icon.Height;
    Bitmap.Canvas.Draw(0, 0, Icon);
    Result := TisStream.Create;
    Bitmap.SaveToStream(Result.RealStream);
    Result.Position := 0;
  finally
    Bitmap.Free;
    Icon.Free;
  end;
end;

end.
