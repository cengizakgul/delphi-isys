// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit ISErrorUtils;


interface

uses
  Classes, syncobjs, SysUtils, Windows, Dialogs, isExceptions;

type
  TisUnhandledErrorHandlerSetting = (uesDontHandle, uesShowMTLogOthers, uesLogAllShowNothing);

type
  TisGetStackProc = function (E: Exception = nil; ExceptAddr: Pointer = nil): string;
  TisSetThreadNameProc = procedure (threadID: dword; threadName: string);
  TisGetThreadNameFunct = function (threadID: dword): string;

procedure InitGetStackProc(const Proc: TisGetStackProc);
procedure InitSetThreadNameProc(const SetProc: TisSetThreadNameProc; const GetProc: TisGetThreadNameFunct);
function  GetStack(E: Exception = nil; ExceptAddr: Pointer = nil): String;
procedure SetThreadName(threadID: dword; threadName: string);
function  GetThreadName(threadID: dword): String;

procedure SetUnhandledErrorSettings(const AValue: TisUnhandledErrorHandlerSetting);
function  GetUnhandledErrorSettings: TisUnhandledErrorHandlerSetting;

function  EnsureCallStack(const E: Exception): EisException;
function  MakeFullError(const E: Exception): String;
function  GetErrorDetails(const E: Exception): String;
function  GetErrorCallStack(const E: Exception): String;
function  GetErrorDetailsStack(const E: Exception): String;
function  BuildStackedErrorStr(const E: Exception): String;


implementation

uses isBasicUtils;

var
  vGetStack: TisGetStackProc;
  vSetThreadName: TisSetThreadNameProc;
  vGetThreadName: TisGetThreadNameFunct;
  vUnhandledErrorSettings: TisUnhandledErrorHandlerSetting = uesShowMTLogOthers;


procedure InitGetStackProc(const Proc: TisGetStackProc);
begin
  vGetStack := Proc;
end;


procedure DummySetThreadName(threadID: dword; threadName: string);
begin
end;

function DummyGetThreadName(threadID: dword): string;
begin
  Result := IntToStr(threadID);
end;

procedure InitSetThreadNameProc(const SetProc: TisSetThreadNameProc; const GetProc: TisGetThreadNameFunct);
begin
  if Assigned(SetProc) then
    vSetThreadName := SetProc
  else
    vSetThreadName := @DummySetThreadName;

  if Assigned(GetProc) then
    vGetThreadName := GetProc
  else
    vGetThreadName := @DummyGetThreadName;
end;

function GetStack(E: Exception = nil; ExceptAddr: Pointer = nil): String;
begin
  if Assigned(vGetStack) then
    Result := vGetStack(E, ExceptAddr)
  else
    Result := 'Stack info is not available';
end;

procedure SetThreadName(threadID: dword; threadName: string);
begin
  if Assigned(vSetThreadName) then
    vSetThreadName(threadID, threadName);
end;

function  GetThreadName(threadID: dword): String;
begin
  if Assigned(vGetThreadName) then
    Result := vGetThreadName(threadID)
  else
    Result := DummyGetThreadName(threadID);
end;

function DummyGetStack(E: Exception = nil; ExceptAddr: Pointer = nil): string;
begin
  Result := 'Stack info is not available';
end;


function EnsureCallStack(const E: Exception): EisException;
begin
  if not (E is EisException) then
    Result := EisVCLException.Create(E)
  else
    Result := EisException(E);

  if Result.StackInfo = '' then
    Result.StackInfo := GetStack;
end;

function MakeFullError(const E: Exception): String;
begin
  if E is EisException then
    Result := EisException(E).GetFullMessage
  else
    Result := E.Message;
end;

function  GetErrorDetails(const E: Exception): String;
begin
  if E is EisException then
    Result := EisException(E).Details
  else
    Result := '';
end;

function  GetErrorCallStack(const E: Exception): String;
begin
  if E is EisException then
    Result := EisException(E).StackInfo
  else
    Result := '';
end;

function  GetErrorDetailsStack(const E: Exception): String;
begin
  if E is EisException then
  begin
    Result := EisException(E).Details;
    AddStrValue(Result, EisException(E).StackInfo, #13);
  end
  else
    Result := '';
end;

function BuildStackedErrorStr(const E: Exception): String;
begin
  Result := MakeFullError(E);
  if GetErrorCallStack(E) = '' then
    Result := Result + #13 + GetStack;
end;

procedure SetUnhandledErrorSettings(const AValue: TisUnhandledErrorHandlerSetting);
begin
  vUnhandledErrorSettings := AValue;
end;

function  GetUnhandledErrorSettings: TisUnhandledErrorHandlerSetting;
begin
  Result := vUnhandledErrorSettings;
end;

initialization
  InitGetStackProc(@DummyGetStack);
  InitSetThreadNameProc(@DummySetThreadName, @DummyGetThreadName);

finalization


end.
