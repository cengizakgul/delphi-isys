unit isBaseClasses;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

interface

uses Classes, Windows, SysUtils, Variants, EvStreamUtils, SyncObjs, Math, RTLConsts;

const
  isGUIDLength = 32;

{Object type IDs
O = InterfacedObject
N = NamedObject
C = Collection
S = StringList
V = NamedValue
L = ListOfValues
PI = ParamsCollectionItem
PC = ParamsCollection
VL = VariantList
VM = ValueMap
IL = InterfacedList
PL = PointerList
NL = IntegerList
}


type
  TisGUID = String;
  TisGUIDList = array of TisGUID;

  // Wrappers for simple types

  IisVariant = interface
  ['{9587E104-756C-44B8-A6EE-3B5F39460C3A}']
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
    property  Value: Variant read GetValue write SetValue;
  end;

  TisVariant = class(TInterfacedObject, IisVariant)
  private
    FValue: Variant;
  protected
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
  public
    constructor CreateAs(const AValue: Variant);
  end;

  // Interfaced Critical Section
  IisLock = interface
  ['{EA75B0AE-DD0C-4DD1-A28F-39C98EBFC7EB}']
    procedure Lock;
    function  TryLock: Boolean;
    procedure Unlock;
  end;

  TisCriticalSection = class(TCriticalSection)
  public
    function TryLock: Boolean;
  end;

  // Critical section
  TisLock = class(TInterfacedObject, IisLock)
  private
    FCS: TisCriticalSection;
  protected
    procedure Lock;
    function  TryLock: Boolean;
    procedure Unlock;
  public
    constructor Create;
    destructor  Destroy; override;
  end;


  // Named Mutex
  TisNamedMutex = class
  private
    FHandle: THandle;
    FName: String;
    FError: Integer;
  public
    constructor Create(const AName: String; const AGlobal: Boolean);
   // Reserved is a workaround for stupid C++ warning
    constructor Open(const AName: String; const AGlobal: Boolean; const Reserved: Boolean = False);
    destructor  Destroy; override;
    property Name: String read FName;
    property Handle: THandle read FHandle;
    property Error: Integer read FError;
  end;


  // Interprocess Critical Section
  TisInterProcessLock = class(TInterfacedObject, IisLock)
  private
    FLock: IisLock;
    FMutex: TisNamedMutex;
    FLockCount: Integer;
    FName: String;
  protected
    procedure   Lock;
    function    TryLock: Boolean;
    procedure   Unlock;
  public
    constructor Create(const AName: String);
    destructor  Destroy; override;
  end;


  // Simple list

  TListCompareItemProc = function (const Item1, Item2: IInterface): Integer;

  IisList = interface(IInterfaceList)
  ['{AE68E510-F525-4973-B11B-24F38EEC4E22}']
    procedure SetCompareItemProc(const AProc: TListCompareItemProc);
    procedure Sort;
    procedure SaveItemsToStream(const AStream: IisStream);
    procedure LoadItemsFromStream(const AStream: IisStream);
    function  GetCopy: IisList;
  end;

  TisList = class(TInterfaceList, IisList)
  private
    FCompareProc: TListCompareItemProc;
    procedure QuickSort(L, R: Integer);
  protected
    procedure SetCompareItemProc(const AProc: TListCompareItemProc);
    function  CompareItems(const Item1, Item2: IInterface): Integer; virtual;
    procedure SaveItemsToStream(const AStream: IisStream);
    procedure LoadItemsFromStream(const AStream: IisStream);
    procedure Sort;
    function  GetCopy: IisList;
  end;

  IisInterfacedObject = interface;
  TisInterfacedObject = class;
  IisCollection = interface;
  TisCollection = class;

  TisRevision = 0..255;

  // Storage Writer/Reader

  IisFormatedReader = interface
  ['{B0AC314F-1E85-47A6-985A-B2B14E9B1512}']
    function  BeginReadGroup(const AName: String): Integer;
    procedure EndReadGroup;
    function  ReadValueByIndex(const AIndex: Integer): Variant;
    function  ReadObjectByIndex(const AIndex: Integer; const ATarget: IisInterfacedObject = nil): IisInterfacedObject;
    function  ReadValue(const AName: String; const ADefault: Variant): Variant;
    function  ReadObject(const AName: String; const ATarget: IisInterfacedObject): IisInterfacedObject;
  end;

  IisFormatedWriter = interface
  ['{6EF92D87-A445-4E21-AB48-F55E00617D41}']
    procedure BeginWriteGroup(const AName: String);
    procedure EndWriteGroup;
    procedure WriteValue(const AName: String; const AValue: Variant);
    procedure WriteObject(const AName: String; const ASource: IisInterfacedObject);
  end;


  // An abstract top ancestor of all interfaced objects

  IisInterfacedObject = interface
  ['{0939F3AF-401F-4D9F-816C-344635985994}']
    function  TypeID: String;
    function  Revision: TisRevision;
    procedure Lock;
    function  TryLock: Boolean;
    procedure Unlock;
    function  GetImplementation: TisInterfacedObject;
    function  IsNamedObject: Boolean;
    procedure WriteToStream(const AStream: IisStream);
    procedure ReadFromStream(const AStream: IisStream);
    function  GetClone: IisInterfacedObject;
    function  GetAsStream: IisStream;
    procedure SetAsStream(const AStream: IisStream);
    procedure FormatedWrite(const AStorage: IisFormatedWriter);
    procedure FormatedRead(const AStorage: IisFormatedReader);
    procedure LoadFromStorage;
    procedure SaveToStorage;
    procedure RemoveFromStorage;
    function  GetOwner: IisCollection;
    procedure SetOwner(const AOwner: IisCollection);
    property  Owner: IisCollection read GetOwner write SetOwner;
    property  AsStream: IisStream read GetAsStream write SetAsStream;
  end;


  TisInterfacedObject = class(TObject, IInterface, IisInterfacedObject)
  private
    FLock:     IisLock;
    FOwner:    TisCollection;
    function   IntGetOwner: TisCollection;
    procedure  IntSetOwner(const AOwner: TisCollection);

  // ********* Interfaced object functionality *********
  protected
    FRefCount: Integer;
    function  QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    function  GetRefCount: Integer;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    class function NewInstance: TObject; override;
    function   Destroying: Boolean;
    property RefCount: Integer read GetRefCount;
  //****************************************************

  protected
    procedure  DoOnConstruction; virtual; // For all custom construction actions!
    procedure  DoOnDestruction; virtual; // For all custom destruction actions!
    procedure  InitLock;
    function   LockActive: Boolean;
    procedure  Lock;
    function   TryLock: Boolean;
    procedure  Unlock;
    function   TypeID: String;
    function   Revision: TisRevision; virtual;
    function   IsNamedObject: Boolean; virtual;
    function   GetImplementation: TisInterfacedObject;
    procedure  WriteSelfToStream(const AStream: IisStream); virtual;
    procedure  ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); virtual;
    procedure  WriteToStream(const AStream: IisStream); virtual;
    procedure  ReadFromStream(const AStream: IisStream); virtual;
    function   GetClone: IisInterfacedObject;
    function   GetAsStream: IisStream;
    procedure  SetAsStream(const AStream: IisStream); virtual;
    procedure  FormatedWrite(const AStorage: IisFormatedWriter); virtual;
    procedure  FormatedRead(const AStorage: IisFormatedReader); virtual;
    procedure  LoadFromStorage; virtual;
    procedure  SaveToStorage; virtual;
    procedure  RemoveFromStorage; virtual;
    function   GetOwner: IisCollection;
    procedure  SetOwner(const AOwner: IisCollection);
  public
    class function GetTypeID: String; virtual;
    constructor Create(const AOwner: IisCollection = nil; const AInitLock: Boolean = False); reintroduce;
    destructor  Destroy; override;
  end;

  TisInterfacedObjectClass = class of TisInterfacedObject;


  // Interface List
  IisInterfaceList = interface
  ['{AE29B4F0-D4BD-4E1F-A3AD-222C12CC2A52}']
    procedure Lock;
    function  TryLock: Boolean;
    procedure Unlock;
    function  Get(Index: Integer): IInterface;
    procedure Put(Index: Integer; const Item: IInterface);
    function  GetCapacity: Integer;
    procedure SetCapacity(const ANewCapacity: Integer);
    function  Count: Integer;
    procedure Clear;
    procedure Delete(const AIndex: Integer); overload;
    procedure Delete(const AItem: IInterface); overload;
    function  IndexOf(const AItem: IInterface): Integer;
    function  Add(const AItem: IInterface): Integer;
    function  GetClone: IisInterfaceList;
    function  GetList: IisList;
    procedure Insert(const AIndex: Integer; const AItem: IInterface);
    procedure Move(const ACurIndex, ANewIndex: Integer);
    property  Capacity: Integer read GetCapacity write SetCapacity;
    property  Items[Index: Integer]: IInterface read Get write Put; default;
  end;

  TisInterfaceList = class(TisInterfacedObject, IisInterfaceList)
  private
    FList: TList;
    FIndex: TList;
    function  FindInIndex(const AItem: IInterface; var AIndex: Integer): Boolean;
    procedure InternalUpdateIndex(const AStartIndex, ACorrection: Integer);
    procedure InternalDeleteFromIndex(const AIndex: Integer);
    procedure AddToIndex(const AObjIndex: Integer);
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;

    procedure FormatedWrite(const AStorage: IisFormatedWriter); override;
    procedure FormatedRead(const AStorage: IisFormatedReader); override;
    function  Get(Index: Integer): IInterface;
    procedure Put(Index: Integer; const Item: IInterface);
    function  GetCapacity: Integer;
    procedure SetCapacity(const ANewCapacity: Integer);
    function  Count: Integer;
    procedure Clear;
    procedure Delete(const AIndex: Integer); overload;
    procedure Delete(const AItem: IInterface); overload;
    function  IndexOf(const AItem: IInterface): Integer;
    function  Add(const AItem: IInterface): Integer;
    procedure Insert(const AIndex: Integer; const AItem: IInterface);
    procedure Move(const ACurIndex, ANewIndex: Integer);
    function  GetClone: IisInterfaceList;
    function  GetList: IisList;
  public
    class function GetTypeID: String; override;
  end;


  IisPointerList = interface
  ['{C417E32E-5B78-4410-974B-A2DD25B7F910}']
    function  Add(const aItem: Pointer): Integer;
    procedure Clear;
    procedure Delete(const aIndex: Integer);
    procedure Exchange(const aIndex1, aIndex2: Integer);
    function  IndexOf(const aItem: Pointer): Integer;
    procedure Insert(const aIndex: Integer; const aItem: Pointer);
    procedure Move(const aCurIndex, aNewIndex: Integer);
    function  Remove(const aItem: Pointer): Integer;
    procedure Pack;
    function  GetCapacity: Integer;
    procedure SetCapacity(const aNewCapacity: Integer);
    function  GetCount: Integer;
    procedure SetCount(const aNewCount: Integer);
    function  GetItem(Index: Integer): Pointer;
    procedure SetItem(Index: Integer; const aItem: Pointer);
    property  Capacity: Integer read GetCapacity write SetCapacity;
    property  Count: Integer read GetCount write SetCount;
    property  Items[Index: Integer]: Pointer read GetItem write SetItem; default;
  end;

  TisPointerList = class(TisInterfacedObject, IisPointerList)
  private
    FList: TList;
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;

    function  Add(const aItem: Pointer): Integer;
    procedure Clear;
    procedure Delete(const aIndex: Integer);
    procedure Exchange(const aIndex1, aIndex2: Integer);
    function  IndexOf(const aItem: Pointer): Integer;
    procedure Insert(const aIndex: Integer; const aItem: Pointer);
    procedure Move(const aCurIndex, aNewIndex: Integer);
    function  Remove(const aItem: Pointer): Integer;
    procedure Pack;
    function  GetCapacity: Integer;
    procedure SetCapacity(const aNewCapacity: Integer);
    function  GetCount: Integer;
    procedure SetCount(const aNewCount: Integer);
    function  GetItem(Index: Integer): Pointer;
    procedure SetItem(Index: Integer; const aItem: Pointer);
  public
    class function GetTypeID: String; override;
  end;


  IisIntegerList = interface
  ['{2BFC6FDC-061F-4D34-AC34-82292E8889B8}']
    function  Add(const aItem: Integer): Integer;
    procedure Clear;
    procedure Delete(const aIndex: Integer);
    procedure Exchange(const aIndex1, aIndex2: Integer);
    function  IndexOf(const aItem: Integer): Integer;
    procedure Insert(const aIndex: Integer; const aItem: Integer);
    procedure Move(const aCurIndex, aNewIndex: Integer);
    function  Remove(const aItem: Integer): Integer;
    function  GetCapacity: Integer;
    procedure SetCapacity(const aNewCapacity: Integer);
    function  GetCount: Integer;
    procedure SetCount(const aNewCount: Integer);
    function  GetItem(Index: Integer): Integer;
    procedure SetItem(Index: Integer; const aItem: Integer);
    function  GetCommaText: string;
    procedure SetCommaText(const AValue: string);
    property  Capacity: Integer read GetCapacity write SetCapacity;
    property  Count: Integer read GetCount write SetCount;
    property  Items[Index: Integer]: Integer read GetItem write SetItem; default;
    property  CommaText: string read GetCommaText write SetCommaText;
  end;


  TIntList = class;

  TisIntegerList = class(TisInterfacedObject, IisIntegerList)
  private
    FList: TIntList;
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;

    function  Add(const aItem: Integer): Integer;
    procedure Clear;
    procedure Delete(const aIndex: Integer);
    procedure Exchange(const aIndex1, aIndex2: Integer);
    function  IndexOf(const aItem: Integer): Integer;
    procedure Insert(const aIndex: Integer; const aItem: Integer);
    procedure Move(const aCurIndex, aNewIndex: Integer);
    function  Remove(const aItem: Integer): Integer;
    function  GetCapacity: Integer;
    procedure SetCapacity(const aNewCapacity: Integer);
    function  GetCount: Integer;
    procedure SetCount(const aNewCount: Integer);
    function  GetItem(Index: Integer): Integer;
    procedure SetItem(Index: Integer; const aItem: Integer);
    function  GetCommaText: string;
    procedure SetCommaText(const AValue: string);
  public
    class function GetTypeID: String; override;
  end;


  // Object factory
  IisObjectFactory = interface
  ['{DD665F27-47FF-408D-B0CC-349B25EDBFAA}']
    procedure   Register(const IID: TGUID; const AImplementation: TisInterfacedObjectClass); overload;
    procedure   Register(const AImplementation: TisInterfacedObjectClass); overload;
    procedure   Register(const AImplementations: array of TisInterfacedObjectClass); overload;
    procedure   UnRegister(const IID: TGUID); overload;
    procedure   UnRegister(const AImplementation: TisInterfacedObjectClass); overload;
    procedure   UnRegister(const AImplementations: array of TisInterfacedObjectClass); overload;
    procedure   UnRegister(const AID: String); overload;
    function    IsRegistered(const IID: TGUID): Boolean; overload;
    function    IsRegistered(const AID: String): Boolean; overload;
    procedure   CreateInstanceOf(const IID: TGUID; out Obj); overload;
    function    CreateInstanceOf(const AID: String): IisInterfacedObject; overload;
    procedure   SaveInstanceToStream(const AObject: IisInterfacedObject; const AStream: IisStream);
    function    CreateInstanceFromStream(const AStream: IisStream): IisInterfacedObject;
    function    GetImplementationOf(const IID: TGUID): TisInterfacedObjectClass; overload;
    function    GetImplementationOf(const AID: String): TisInterfacedObjectClass; overload;
    function    GetRegisteredClasses: IisPointerList; overload;
  end;


   // Named object
   IisNamedObject = interface(IisInterfacedObject)
   ['{7A071AD7-80F0-4013-866C-53A1C3022EC0}']
     function  GetName: String;
     procedure SetName(const AName: String);
     property  Name: String read GetName write SetName;
   end;


   TisNamedObject = class(TisInterfacedObject, IisNamedObject)
   private
     FName: String;
   protected
     function  IsNamedObject: Boolean; override;
     function  GetName: String;
     procedure SetName(const AName: String); virtual;
   public
     class function GetTypeID: String; override;
   end;


  // Delphi's TComponent simplified analog

  IisCollection = interface(IisNamedObject)
  ['{75D35FB4-E13E-490A-85B1-9F2B0F7D4D2B}']
    function  AddChild(const AChildObject: IisInterfacedObject): Integer;
    procedure InsertChild(const AIndex: Integer; const AChildObject: IisInterfacedObject);
    procedure RemoveChild(const AChildObject: IisInterfacedObject); overload;
    procedure RemoveChild(const AIndex: Integer); overload;
    procedure RemoveAllChildren;
    procedure MoveChild(const AChildObject: IisInterfacedObject; const ANewIndex: Integer); overload;
    procedure MoveChild(const AIndex: Integer; const ANewIndex: Integer); overload;
    function  IndexOf(const AChildObject: IisInterfacedObject): Integer; overload;
    function  IndexOf(const AName: String): Integer; overload;
    procedure Delete(const AIndex: Integer);
    function  SubstituteChild(const ASource: IisInterfacedObject; const ATarget: IisInterfacedObject): Boolean;
    function  FindChildByName(const AName: String): IisNamedObject;
    function  GetChildrenByType(const ATypeID: String): IisList;
    function  GetChild(Index: Integer): IisInterfacedObject;
    function  ChildCount: Integer;
    procedure SaveAllToStorage;
    procedure LoadAllFromStorage;
    procedure LoadChildrenFromStorage;
    function  GetChildrenList: IisList; //copy of children list
    procedure Sort(const ADirection: String = 'asc');
    property  Children[Index: Integer]: IisInterfacedObject read GetChild;
  end;


  IisStringList = interface;

  TisCollection = class(TisNamedObject, IisCollection)
  private
    FChildren:  IisInterfaceList;
    FIndex:     IisStringList;
    procedure   IntCheckUnique(const AName: String);
    procedure   SetChildName(const AChild: IisNamedObject; const AName: String);
    function    GetChildName(const AChild: IisNamedObject): String;
    procedure   AddToIndex(const AChild: IisInterfacedObject);
    procedure   DeleteFromIndex(const AChild: IisInterfacedObject);
    function    IntAddChild(const AChildObject: IisInterfacedObject): Integer;
    procedure   IntInsChild(const AIndex: Integer; const AChildObject: IisInterfacedObject);
  protected
    FDeleteChildrenBeforeRead: Boolean;

    procedure   DoOnConstruction; override;
    function    CreateChildByTypeID(const ATypeID: String): IisInterfacedObject;
    procedure   WriteChildrenToStream(const AStream: IisStream); virtual;
    procedure   ReadChildrenFromStream(const AStream: IisStream); virtual;
    procedure   WriteToStream(const AStream: IisStream); override;
    procedure   ReadFromStream(const AStream: IisStream); override;
    procedure   FormatedWrite(const AStorage: IisFormatedWriter); override;
    procedure   FormatedRead(const AStorage: IisFormatedReader); override;
    procedure   SetAsStream(const AStream: IisStream); override;
    function    AddChild(const AChildObject: IisInterfacedObject): Integer;
    procedure   InsertChild(const AIndex: Integer; const AChildObject: IisInterfacedObject);
    procedure   RemoveChild(const AChildObject: IisInterfacedObject); overload;
    procedure   RemoveChild(const AIndex: Integer); overload;
    procedure   RemoveAllChildren;
    procedure   MoveChild(const AChildObject: IisInterfacedObject; const ANewIndex: Integer); overload;
    procedure   MoveChild(const AIndex: Integer; const ANewIndex: Integer); overload;
    function    IndexOf(const AChildObject: IisInterfacedObject): Integer; overload;
    function    IndexOf(const AName: String): Integer; overload;
    procedure   Delete(const AIndex: Integer);
    function    SubstituteChild(const ASource: IisInterfacedObject; const ATarget: IisInterfacedObject): Boolean;
    function    GetChildrenList: IisList;
    function    FindChildByName(const AName: String): IisNamedObject;
    function    GetChildrenByType(const ATypeID: String): IisList;
    function    GetChild(Index: Integer): IisInterfacedObject;
    function    ChildCount: Integer;
    procedure   RemoveFromStorage; override;
    procedure   SaveAllToStorage;
    procedure   LoadChildrenFromStorage; virtual;
    procedure   LoadAllFromStorage;
    procedure   Sort(const ADirection: String = 'asc');
  protected
    function    NameIsValid(const AName: String): Boolean; virtual;
  public
    class function GetTypeID: String; override;
    destructor Destroy; override;
  end;


  // StringList
  IisStringList = interface
  ['{583FD8CA-2942-4A26-B88C-0E1B81783976}']
    procedure Lock;
    function  TryLock: Boolean;
    procedure Unlock;
    function  GetString(Index: Integer): string;
    procedure SetString(Index: Integer; const S: string);
    function  GetObject(Index: Integer): IInterface;
    procedure SetObject(Index: Integer; AObject: IInterface);
    function  Add(const S: string): Integer;
    function  AddObject(const S: string; AObject: IInterface): Integer;
    procedure Append(const S: string);
    procedure AddStrings(Strings: IisStringList);
    procedure Clear;
    procedure Delete(Index: Integer);
    function  Equals(Strings: IisStringList): Boolean;
    procedure Exchange(Index1, Index2: Integer);
    function  IndexOf(const S: string): Integer;
    function  IndexOfName(const Name: string): Integer;
    function  IndexOfObject(AObject: IInterface): Integer;
    procedure Insert(Index: Integer; const S: string);
    procedure InsertObject(Index: Integer; const S: string; AObject: IInterface);
    procedure LoadFromFile(const FileName: string);
    procedure LoadFromStream(Stream: TStream);
    procedure Move(CurIndex, NewIndex: Integer);
    procedure SaveToFile(const FileName: string);
    procedure SaveToStream(Stream: TStream);
    function  GetCapacity: Integer;
    procedure SetCapacity(NewCapacity: Integer);
    function  GetCommaText: string;
    procedure SetCommaText(const AValue: string);
    function  GetCount: Integer;
    function  GetDelimiter: Char;
    procedure SetDelimiter(const AValue: Char);
    function  GetDelimitedText: string;
    procedure SetDelimitedText(const AValue: string);
    function  GetName(Index: Integer): string;
    function  GetQuoteChar: Char;
    procedure SetQuoteChar(const AValue: Char);
    function  GetValue(const AName: string): string;
    procedure SetValue(const AName, AValue: string);
    function  GetValueFromIndex(Index: Integer): string;
    procedure SetValueFromIndex(Index: Integer; const AValue: string);
    function  GetNameValueSeparator: Char;
    procedure SetNameValueSeparator(const AValue: Char);
    function  GetText: string;
    procedure SetText(const AValue: string);
    function  Find(const S: string; var Index: Integer): Boolean;
    procedure Sort(const ADirection: String = 'asc');
    function  GetDuplicates: TDuplicates;
    procedure SetDuplicates(const AValue: TDuplicates);
    function  GetSorted: Boolean;
    procedure SetSorted(const AValue: Boolean);
    function  GetCaseSensitive: Boolean;
    procedure SetCaseSensitive(const AValue: Boolean);
    function  VCLStrings: TStrings;
    property  Capacity: Integer read GetCapacity write SetCapacity;
    property  CommaText: string read GetCommaText write SetCommaText;
    property  Count: Integer read GetCount;
    property  Delimiter: Char read GetDelimiter write SetDelimiter;
    property  DelimitedText: string read GetDelimitedText write SetDelimitedText;
    property  Names[Index: Integer]: string read GetName;
    property  QuoteChar: Char read GetQuoteChar write SetQuoteChar;
    property  Values[const Name: string]: string read GetValue write SetValue;
    property  ValueFromIndex[Index: Integer]: string read GetValueFromIndex write SetValueFromIndex;
    property  NameValueSeparator: Char read GetNameValueSeparator write SetNameValueSeparator;
    property  Text: string read GetText write SetText;
    property  Duplicates: TDuplicates read GetDuplicates write SetDuplicates;
    property  Sorted: Boolean read GetSorted write SetSorted;
    property  CaseSensitive: Boolean read GetCaseSensitive write SetCaseSensitive;
    property  Strings[Index: Integer]: string read GetString write SetString; default;
    property  Objects[Index: Integer]: IInterface read GetObject write SetObject;
  end;


  IisStringListRO = interface
  ['{003722B8-578B-42BD-A88B-AEA7492C7B43}']
    procedure Lock;
    function  TryLock: Boolean;
    procedure Unlock;
    function  GetString(Index: Integer): string;
    function  GetObject(Index: Integer): IInterface;
    function  Equals(Strings: IisStringList): Boolean;
    function  IndexOf(const S: string): Integer;
    function  IndexOfName(const Name: string): Integer;
    function  IndexOfObject(AObject: IInterface): Integer;
    procedure SaveToFile(const FileName: string);
    procedure SaveToStream(Stream: TStream);
    function  GetCapacity: Integer;
    function  GetCommaText: string;
    function  GetCount: Integer;
    function  GetDelimiter: Char;
    function  GetDelimitedText: string;
    function  GetName(Index: Integer): string;
    function  GetQuoteChar: Char;
    function  GetValue(const AName: string): string;
    function  GetValueFromIndex(Index: Integer): string;
    function  GetNameValueSeparator: Char;
    function  GetText: string;
    function  Find(const S: string; var Index: Integer): Boolean;
    function  GetDuplicates: TDuplicates;
    function  GetSorted: Boolean;
    function  GetCaseSensitive: Boolean;
    property  CommaText: string read GetCommaText;
    property  Count: Integer read GetCount;
    property  Delimiter: Char read GetDelimiter;
    property  DelimitedText: string read GetDelimitedText;
    property  Names[Index: Integer]: string read GetName;
    property  Values[const Name: string]: string read GetValue;
    property  ValueFromIndex[Index: Integer]: string read GetValueFromIndex;
    property  Text: string read GetText;
    property  QuoteChar: Char read GetQuoteChar;
    property  NameValueSeparator: Char read GetNameValueSeparator;
    property  Duplicates: TDuplicates read GetDuplicates;
    property  Sorted: Boolean read GetSorted;
    property  CaseSensitive: Boolean read GetCaseSensitive;
    property  Strings[Index: Integer]: string read GetString; default;
    property  Objects[Index: Integer]: IInterface read GetObject;
  end;


  TisStringList = class(TisInterfacedObject, IisStringList, IisStringListRO)
  private
    FList: TStringList;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;
    procedure FormatedWrite(const AStorage: IisFormatedWriter); override;
    procedure FormatedRead(const AStorage: IisFormatedReader); override;

    function  GetString(Index: Integer): string;
    procedure SetString(Index: Integer; const S: string);
    function  GetObject(Index: Integer): IInterface;
    procedure SetObject(Index: Integer; AObject: IInterface);
    function  Add(const S: string): Integer;
    function  AddObject(const S: string; AObject: IInterface): Integer;
    procedure Append(const S: string);
    procedure AddStrings(Strings: IisStringList);
    procedure Clear;
    procedure Delete(Index: Integer);
    function  Equals(Strings: IisStringList): Boolean;
    procedure Exchange(Index1, Index2: Integer);
    function  IndexOf(const S: string): Integer;
    function  IndexOfName(const Name: string): Integer;
    function  IndexOfObject(AObject: IInterface): Integer;
    procedure Insert(Index: Integer; const S: string);
    procedure InsertObject(Index: Integer; const S: string; AObject: IInterface);
    procedure LoadFromFile(const FileName: string);
    procedure LoadFromStream(Stream: TStream);
    procedure Move(CurIndex, NewIndex: Integer);
    procedure SaveToFile(const FileName: string);
    procedure SaveToStream(Stream: TStream);
    function  GetCapacity: Integer;
    procedure SetCapacity(NewCapacity: Integer);
    function  GetCommaText: string;
    procedure SetCommaText(const AValue: string);
    function  GetCount: Integer;
    function  GetDelimiter: Char;
    procedure SetDelimiter(const AValue: Char);
    function  GetDelimitedText: string;
    procedure SetDelimitedText(const AValue: string);
    function  GetName(Index: Integer): string;
    function  GetQuoteChar: Char;
    procedure SetQuoteChar(const AValue: Char);
    function  GetValue(const AName: string): string;
    procedure SetValue(const AName, AValue: string);
    function  GetValueFromIndex(Index: Integer): string;
    procedure SetValueFromIndex(Index: Integer; const AValue: string);
    function  GetNameValueSeparator: Char;
    procedure SetNameValueSeparator(const AValue: Char);
    function  GetText: string;
    procedure SetText(const AValue: string);
    function  Find(const S: string; var Index: Integer): Boolean;
    procedure Sort(const ADirection: String = 'asc');
    function  GetDuplicates: TDuplicates;
    procedure SetDuplicates(const AValue: TDuplicates);
    function  GetSorted: Boolean;
    procedure SetSorted(const AValue: Boolean);
    function  GetCaseSensitive: Boolean;
    procedure SetCaseSensitive(const AValue: Boolean);
    function  VCLStrings: TStrings;
  public
    class function GetTypeID: String; override;
    constructor CreateAsStrings(const AStrings: TStrings; const AInitLock: Boolean = False);
    constructor CreateAsIndex(const AInitLock: Boolean = False);
// Reserved is a workaround for stupid C++ warning
    constructor CreateUnique(const AInitLock: Boolean = False; const Reserved: Boolean = False);
    destructor  Destroy; override;
  end;


  // VariantList
  IisVariantList = interface
  ['{6A674905-8944-4F47-9B96-A023FA5F7690}']
    procedure Lock;
    function  TryLock: Boolean;
    procedure Unlock;
    function  GetVariant(Index: Integer): Variant;
    procedure SetVariant(Index: Integer; const V: Variant);
    function  Add(const V: Variant): Integer;
    procedure AddVariants(const Variants: IisVariantList);
    procedure Clear;
    procedure Delete(const Index: Integer);
    procedure Exchange(const Index1, Index2: Integer);
    function  IndexOf(const V: Variant): Integer;
    procedure Insert(const Index: Integer; const V: Variant);
    procedure LoadFromFile(const FileName: string);
    procedure LoadFromStream(const Stream: IisStream);
    procedure Move(const CurIndex, NewIndex: Integer);
    procedure SaveToFile(const FileName: string);
    procedure SaveToStream(Stream: IisStream);
    function  GetCapacity: Integer;
    procedure SetCapacity(const NewCapacity: Integer);
    function  GetCount: Integer;
    property  Capacity: Integer read GetCapacity write SetCapacity;
    property  Count: Integer read GetCount;
    property  Varians[Index: Integer]: Variant read GetVariant write SetVariant; default;
  end;

  IisVariantListRO = interface
  ['{CD17A869-70B3-4370-81D3-47CF60D12839}']
    procedure Lock;
    function  TryLock: Boolean;
    procedure Unlock;
    function  GetVariant(Index: Integer): Variant;
    function  IndexOf(const V: Variant): Integer;
    procedure SaveToFile(const FileName: string);
    procedure SaveToStream(Stream: IisStream);
    function  GetCount: Integer;
    property  Count: Integer read GetCount;
    property  Varians[Index: Integer]: Variant read GetVariant; default;
  end;

  TisVariantList = class(TisInterfacedObject, IisVariantList, IisVariantListRO)
  private
    FList: TList;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;
    procedure FormatedWrite(const AStorage: IisFormatedWriter); override;
    procedure FormatedRead(const AStorage: IisFormatedReader); override;
    function  GetVariant(Index: Integer): Variant;
    procedure SetVariant(Index: Integer; const V: Variant);
    function  Add(const V: Variant): Integer;
    procedure AddVariants(const Variants: IisVariantList);
    procedure Clear;
    procedure Delete(const Index: Integer);
    procedure Exchange(const Index1, Index2: Integer);
    function  IndexOf(const V: Variant): Integer;
    procedure Insert(const Index: Integer; const V: Variant);
    procedure LoadFromFile(const FileName: string);
    procedure LoadFromStream(const Stream: IisStream);
    procedure Move(const CurIndex, NewIndex: Integer);
    procedure SaveToFile(const FileName: string);
    procedure SaveToStream(Stream: IisStream);
    function  GetCapacity: Integer;
    procedure SetCapacity(const NewCapacity: Integer);
    function  GetCount: Integer;
  public
    class function GetTypeID: String; override;
    destructor     Destroy; override;
  end;


  // List of named values
  IisNamedValue = interface
  ['{942160A1-5CF0-4A11-AD71-5153CAF23D63}']
    procedure Lock;
    function  TryLock: Boolean;
    procedure Unlock;
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
    function  GetName: String;
    property  Name:  String read GetName;
    property  Value: Variant read GetValue write SetValue;
  end;


  TisNamedValue = class(TisNamedObject, IisNamedValue)
  private
    FValue: Variant;
  protected
    procedure  WriteSelfToStream(const AStream: IisStream); override;
    procedure  ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;
    procedure  FormatedWrite(const AStorage: IisFormatedWriter); override;
    procedure  FormatedRead(const AStorage: IisFormatedReader); override;
    function   GetValue: Variant;
    procedure  SetValue(const AValue: Variant);
  public
    class function GetTypeID: String; override;
  end;


  IisListOfValues = interface
  ['{D8E908A6-B3AB-4748-B519-931778C4D629}']
    procedure Lock;
    function  TryLock: Boolean;
    procedure Unlock;
    procedure Delete(const AIndex: Integer);
    function  AddValue(const AName: String; const AValue: Variant): Integer;
    procedure DeleteValue(const AName: String); overload;
    procedure DeleteValue(const AIndex: Integer); overload;
    function  FindValue(const AName: String): IisNamedValue;
    procedure MoveValue(const AName: String; const ANewIndex: Integer); overload;
    procedure MoveValue(const AIndex: Integer; const ANewIndex: Integer); overload;
    function  GetValue(const AIndex: Integer): IisNamedValue;
    function  GetValueByName(const AName: String): Variant;
    procedure SetValueByName(const AName: String; const AValue: Variant);
    function  Count: Integer;
    procedure Clear;
    procedure Sort(const ADirection: String = 'asc');
    procedure WriteToStream(const AStream: IisStream);
    procedure ReadFromStream(const AStream: IisStream);
    function  GetClone: IisListOfValues;
    function  GetAsStream: IisStream;
    procedure SetAsStream(const AStream: IisStream);
    procedure Assign(const ASource: IisListOfValues);
    function  ValueExists(const AName: String): Boolean;
    function  TryGetValue(const AName: String; const ADefault: Variant): Variant;
    function  IndexOf(const AName: String): Integer;
    property  Values[const AIndex: Integer]: IisNamedValue read GetValue; default;
    property  Value[const AName: String]: Variant read GetValueByName write SetValueByName;
    property  AsStream: IisStream read GetAsStream write SetAsStream;
  end;


  TisListOfValues = class(TisCollection, IisListOfValues)
  protected
    procedure Assign(const ASource: IisListOfValues);
    function  AddValue(const AName: String; const AValue: Variant): Integer;
    procedure DeleteValue(const AName: String); overload;
    procedure DeleteValue(const AIndex: Integer); overload;
    function  FindValue(const AName: String): IisNamedValue;
    procedure MoveValue(const AName: String; const ANewIndex: Integer); overload;
    procedure MoveValue(const AIndex: Integer; const ANewIndex: Integer); overload;
    function  GetValue(const AIndex: Integer): IisNamedValue;
    function  GetValueByName(const AName: String): Variant;
    procedure SetValueByName(const AName: String; const AValue: Variant);
    function  ValueExists(const AName: String): Boolean;
    function  TryGetValue(const AName: String; const ADefault: Variant): Variant;
    function  Count: Integer;
    procedure Clear;
    function  GetClone: IisListOfValues;
  public
    class function GetTypeID: String; override;
  end;


  // Simple Params Collection

  TisParamsCollectionItem = class(TisListOfValues)
  protected
    procedure  WriteSelfToStream(const AStream: IisStream); override;
    procedure  ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;
    procedure  FormatedWrite(const AStorage: IisFormatedWriter); override;
    procedure  FormatedRead(const AStorage: IisFormatedReader); override;
  public
    class function GetTypeID: String; override;
  end;

  IisParamsCollection = interface
  ['{5FA22D3D-93FD-4CAD-B1FC-1F1C40474914}']
    procedure Lock;
    function  TryLock: Boolean;
    procedure Unlock;
    procedure DeleteParams(const AIndex: Integer); overload;
    procedure DeleteParams(const AParam: IisListOfValues); overload;
    procedure DeleteParams(const AParam: String); overload;
    function  AddParams(const AName: String = ''): IisListOfValues;
    function  GetParams(const AIndex: Integer): IisListOfValues;
    function  Count: Integer;
    function  ParamsByName(const AName: String): IisListOfValues;
    function  ParamName(const AIndex: Integer): String;
    function  IndexOf(const AName: String): Integer;
    procedure Clear;
    procedure WriteToStream(const AStream: IisStream);
    procedure ReadFromStream(const AStream: IisStream);
    function  GetClone: IisParamsCollection;
    function  GetAsStream: IisStream;
    procedure SetAsStream(const AStream: IisStream);
    procedure Sort(const ADirection: String = 'asc');
    property  Params[const AIndex: Integer]: IisListOfValues read GetParams; default;
    property  AsStream: IisStream read GetAsStream write SetAsStream;
  end;

  TisParamsCollection = class(TisCollection, IisParamsCollection)
  protected
    procedure DeleteParams(const AIndex: Integer); overload;
    procedure DeleteParams(const AParam: IisListOfValues); overload;
    procedure DeleteParams(const AParam: String); overload;
    function  AddParams(const AName: String = ''): IisListOfValues;
    function  ParamsByName(const AName: String): IisListOfValues;
    function  ParamName(const AIndex: Integer): String;
    function  GetParams(const AIndex: Integer): IisListOfValues;
    function  GetClone: IisParamsCollection;
    function  Count: Integer;
    procedure Clear;
  public
    class function GetTypeID: String; override;
  end;


  // Map of old and new values

  TisValueMapItem = record
    OrigValue: Variant;
    NewValue: Variant;
  end;
  PTisValueMapItem = ^TisValueMapItem;

  IisValueMap = interface
  ['{B0222ABC-0BE1-4C78-BDD5-F9775F7DA373}']
    procedure EncodeValue(const AOrigValue, ANewValue: Variant);
    function  TryDecodeValue(const AOrigValue: Variant; out AMapValue: Variant): Boolean;
    function  DecodeValue(const AOrigValue: Variant): Variant;
    function  Count:Integer;
    function  GetValueMap(Index: Integer): TisValueMapItem;
    procedure Clear;
    function  GetClone: IisValueMap;
    property  ValuesMap[Index: Integer]: TisValueMapItem read GetValueMap; default;
  end;

  TisValueMap = class(TisInterfacedObject, IisValueMap)
  private
    FList: TStringList;
    function GetItem(const AIndex: Integer): PTisValueMapItem;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;
    procedure FormatedWrite(const AStorage: IisFormatedWriter); override;
    procedure FormatedRead(const AStorage: IisFormatedReader); override;

    procedure EncodeValue(const AOrigValue, ANewValue: Variant);
    function  TryDecodeValue(const AOrigValue: Variant; out AMapValue: Variant): Boolean;
    function  DecodeValue(const AOrigValue: Variant): Variant;
    function  Count:Integer;
    function  GetValueMap(Index: Integer): TisValueMapItem;
    procedure Clear;
    function  GetClone: IisValueMap;
  public
    destructor Destroy; override;
    class function GetTypeID: String; override;
  end;


  // Custom global junction box
  IisAppMainboard = interface
  ['{B4C02FF2-CD34-4F89-AA7B-C2EB341463E9}']
    procedure RegisterInterface(const IID: TGUID; const AImplementation: TisInterfacedObjectClass);
    procedure UnregisterInterface(const IID: TGUID);
    function  CreateInstanceOf(const IID: TGUID; const ANoError: Boolean = False): IInterface;
    procedure Initialize;
    procedure Deinitialize;
  end;


  TisAppMainboard = class(TisInterfacedObject, IisAppMainboard)
  protected
    procedure RegisterInterface(const IID: TGUID; const AImplementation: TisInterfacedObjectClass);
    procedure UnregisterInterface(const IID: TGUID);
    function  IsRegisteredInterface(const IID: TGUID): Boolean;
    function  CreateInstanceOf(const IID: TGUID; const ANoError: Boolean = False): IInterface;
    procedure Initialize; virtual;
    procedure Deinitialize; virtual;
  public
    class function GetTypeID: String; override;
  end;


  // Integers List

  PIntListArr = ^TIntListArr;
  TIntListArr = array[0..MaxListSize - 1] of Integer;

  TIntList = class
  private
    FList: PIntListArr;
    FCount: Integer;
    FCapacity: Integer;
    procedure QuickSort(L, R: Integer);
  protected
    function  Get(Index: Integer): Integer;
    procedure Grow; virtual;
    procedure Put(Index: Integer; Item: Integer);
    procedure SetCapacity(NewCapacity: Integer);
    procedure SetCount(NewCount: Integer);
  public
    destructor Destroy; override;
    function  Add(Item: Integer): Integer;
    procedure Clear; virtual;
    procedure Delete(Index: Integer);
    procedure Exchange(Index1, Index2: Integer);
    function  Expand: TIntList;
    function  Extract(Item: Integer): Integer;
    function  IndexOf(Item: Integer): Integer;
    procedure Insert(Index: Integer; Item: Integer);
    procedure Move(CurIndex, NewIndex: Integer);
    function  Remove(Item: Integer): Integer;
    procedure Sort;
    procedure Assign(ListA: TIntList; AOperator: TListAssignOp = laCopy; ListB: TIntList = nil);
    property  Capacity: Integer read FCapacity write SetCapacity;
    property  Count: Integer read FCount write SetCount;
    property  Items[Index: Integer]: Integer read Get write Put; default;
  end;


  function  ObjectFactory: IisObjectFactory;
  procedure SafeObjectFactoryUnRegister(const IID: TGUID); overload;
  procedure SafeObjectFactoryUnRegister(const AImplementation: TisInterfacedObjectClass); overload;
  procedure SafeObjectFactoryUnRegister(const AImplementations: array of TisInterfacedObjectClass); overload;
  procedure SafeObjectFactoryUnRegister(const AID: String); overload;


{$IFDEF WATCH_OBJECTS}
var ObjectsList: TThreadList;
{$ENDIF}

implementation

uses isBasicUtils, isExceptions, isStorage;

var
  FObjectFactory: IisObjectFactory;
  FFinalized: Boolean;

type
  TisObjectFactory = class(TisInterfacedObject, IisObjectFactory)
  private
    FIntList: TThreadList;
    FIdList: TStringList;
    function  IndexOf(const IID: TGUID): Integer;
    function  GetImplementation(const IID: TGUID): TisInterfacedObjectClass; overload;
    function  GetImplementation(const AID: String): TisInterfacedObjectClass; overload;
  protected
    procedure DoOnConstruction; override;
    procedure Register(const IID: TGUID; const AImplementation: TisInterfacedObjectClass); overload;
    procedure Register(const AImplementation: TisInterfacedObjectClass); overload;
    procedure Register(const AImplementations: array of TisInterfacedObjectClass); overload;
    procedure UnRegister(const IID: TGUID); overload;
    procedure UnRegister(const AImplementation: TisInterfacedObjectClass); overload;
    procedure UnRegister(const AImplementations: array of TisInterfacedObjectClass); overload;
    procedure UnRegister(const AID: String); overload;
    function  IsRegistered(const IID: TGUID): Boolean; overload;
    function  IsRegistered(const AID: String): Boolean; overload;
    procedure CreateInstanceOf(const IID: TGUID; out Obj); overload;
    function  CreateInstanceOf(const AID: String): IisInterfacedObject; overload;
    procedure SaveInstanceToStream(const AObject: IisInterfacedObject; const AStream: IisStream);
    function  CreateInstanceFromStream(const AStream: IisStream): IisInterfacedObject;
    function  GetImplementationOf(const IID: TGUID): TisInterfacedObjectClass; overload;
    function  GetImplementationOf(const AID: String): TisInterfacedObjectClass; overload;
    function  GetRegisteredClasses: IisPointerList; overload;
  public
    class function GetTypeID: String; override;
    destructor  Destroy; override;
  end;

  TRegisteredItem = record
    IID: TGUID;
    ImplementationClass: TisInterfacedObjectClass;
  end;

  PRegisteredItem = ^TRegisteredItem;


function ObjectFactory: IisObjectFactory;
begin
  if not FFinalized and not Assigned(FObjectFactory) then
    FObjectFactory := TisObjectFactory.Create;

  Result := FObjectFactory;
end;

procedure SafeObjectFactoryUnRegister(const IID: TGUID); overload;
begin
  if not FFinalized then
    ObjectFactory.UnRegister(IID);
end;

procedure SafeObjectFactoryUnRegister(const AImplementation: TisInterfacedObjectClass); overload;
begin
  if not FFinalized then
    ObjectFactory.UnRegister(AImplementation);
end;

procedure SafeObjectFactoryUnRegister(const AImplementations: array of TisInterfacedObjectClass); overload;
begin
  if not FFinalized then
    ObjectFactory.UnRegister(AImplementations);
end;

procedure SafeObjectFactoryUnRegister(const AID: String); overload;
begin
  if not FFinalized then
    ObjectFactory.UnRegister(AID);
end;



{ TisObjectFactory }

procedure TisObjectFactory.CreateInstanceOf(const IID: TGUID; out Obj);
var
  Cl: TisInterfacedObjectClass;
  O:  TisInterfacedObject;
begin
  Cl := GetImplementationOf(IID);

  O := Cl.Create;
  try
    CheckCondition(O.GetInterface(IID, Obj), 'Class "' + O.ClassName + '" does not support interface "' + GUIDToString(IID));
  except
    FreeAndNil(O);
  end;
end;

function TisObjectFactory.CreateInstanceOf(const AID: String): IisInterfacedObject;
var
  Cl: TisInterfacedObjectClass;
begin
  Cl := GetImplementationOf(AID);
  Result := Cl.Create;
end;

destructor TisObjectFactory.Destroy;
var
  i: Integer;
  L: TList;
begin
  L := FIntList.LockList;
  try
    for i := 0 to L.Count - 1 do
      Dispose(PRegisteredItem(L[i]));
  finally
    FIntList.UnlockList;
  end;

  FreeAndNil(FIntList);
  FreeAndNil(FIdList);

  inherited;
end;


procedure TisObjectFactory.DoOnConstruction;
begin
  inherited;
  FIntList := TThreadList.Create;

  FIdList := TStringList.Create;
  FIdList.Duplicates := dupError;
  FIdList.CaseSensitive := False;
  FIdList.Sorted := True;
end;

function TisObjectFactory.IndexOf(const IID: TGUID): Integer;
var
  i: Integer;
  L: TList;
begin
  Result := -1;

  L := FIntList.LockList;
  try
    for i := 0 to L.Count - 1 do
      if IsEqualGUID(PRegisteredItem(L[i])^.IID, IID) then
      begin
        Result := i;
        break;
      end;
  finally
    FIntList.UnlockList;
  end;
end;

procedure TisObjectFactory.Register(const IID: TGUID; const AImplementation: TisInterfacedObjectClass);
var
  L: TList;
  RegItem: PRegisteredItem;
  i: Integer;
begin
  if not Assigned(AImplementation) then
    Exit;

  CheckCondition(Supports(AImplementation, IID), 'Class "' + AImplementation.ClassName + '" does not support interface "' + GUIDToString(IID) + '"' );

  L := FIntList.LockList;
  try
    i := IndexOf(IID);

    if i = -1 then
    begin
      New(RegItem);
      RegItem.IID := IID;
      L.Add(RegItem);
    end
    else
      RegItem := PRegisteredItem(L[i]);

    RegItem.ImplementationClass := AImplementation;
  finally
    FIntList.UnlockList;
  end;
end;

procedure TisObjectFactory.UnRegister(const IID: TGUID);
var
  L: TList;
  i: Integer;
begin
  L := FIntList.LockList;
  try
    i := IndexOf(IID);
    if i <> -1 then
    begin
      Dispose(PRegisteredItem(L[i]));
      L.Delete(i);
    end;
  finally
    FIntList.UnlockList;
  end;
end;


procedure TisObjectFactory.Register(const AImplementation: TisInterfacedObjectClass);
var
  i: Integer;
begin
  if not Assigned(AImplementation) then
    Exit;

  FIntList.LockList;
  try
    i := FIdList.IndexOf(AImplementation.GetTypeID);

    if i = -1 then
      FIdList.AddObject(AImplementation.GetTypeID, Pointer(AImplementation))
    else
      CheckCondition(FIdList.Objects[i] = Pointer(AImplementation), 'Object type "' +
          AImplementation.GetTypeID + '" (' + AImplementation.ClassName + ') is already registered as ' +
          (FIdList.Objects[i] as TisInterfacedObjectClass).ClassName);
  finally
    FIntList.UnlockList;
  end;
end;

procedure TisObjectFactory.UnRegister(const AID: String);
var
  i: Integer;
begin
  FIntList.LockList;
  try
    i := FIdList.IndexOf(AID);
    if i <> -1 then
      FIdList.Delete(i);
  finally
    FIntList.UnlockList;
  end;
end;

procedure TisObjectFactory.Register(const AImplementations: array of TisInterfacedObjectClass);
var
  i: Integer;
begin
  for i := Low(AImplementations) to High(AImplementations) do
    Register(AImplementations[i]);
end;

function TisObjectFactory.IsRegistered(const IID: TGUID): Boolean;
begin
  Result := GetImplementation(IID) <> nil;
end;

function TisObjectFactory.IsRegistered(const AID: String): Boolean;
begin
  Result := GetImplementation(AID) <> nil;
end;

class function TisObjectFactory.GetTypeID: String;
begin
  Result := 'ObjectFactory';
end;

procedure TisObjectFactory.UnRegister(const AImplementations: array of TisInterfacedObjectClass);
var
  i: Integer;
begin
  for i := Low(AImplementations) to High(AImplementations) do
    UnRegister(AImplementations[i]);
end;

procedure TisObjectFactory.UnRegister(const AImplementation: TisInterfacedObjectClass);
begin
  UnRegister(AImplementation.GetTypeID);
end;

function TisObjectFactory.CreateInstanceFromStream(const AStream: IisStream): IisInterfacedObject;
var
  ObjType: String;
begin
  ObjType := AStream.ReadShortString;
  Result := CreateInstanceOf(ObjType);
  Result.ReadFromStream(AStream);
end;

function TisObjectFactory.GetImplementation(const IID: TGUID): TisInterfacedObjectClass;
var
  L:  TList;
  i:  Integer;
begin
  L := FIntList.LockList;
  try
    i := IndexOf(IID);
    if i <> -1 then
      Result := PRegisteredItem(L[i]).ImplementationClass
    else
      Result := nil;
  finally
    FIntList.UnlockList;
  end;
end;

function TisObjectFactory.GetImplementation(const AID: String): TisInterfacedObjectClass;
var
  i:  Integer;
begin
  FIntList.LockList;
  try
    i := FIdList.IndexOf(AID);
    if i <> -1 then
      Result := TisInterfacedObjectClass(FIdList.Objects[i])
    else
      Result := nil; 
  finally
    FIntList.UnlockList;
  end;
end;

function TisObjectFactory.GetImplementationOf(const IID: TGUID): TisInterfacedObjectClass;
var
  L:  TList;
  i:  Integer;
begin
  L := FIntList.LockList;
  try
    i := IndexOf(IID);
    CheckCondition(i > -1, 'Interface "' + GUIDToString(IID) + '" is not registered');
    Result := PRegisteredItem(L[i]).ImplementationClass;
  finally
    FIntList.UnlockList;
  end;
end;

function TisObjectFactory.GetImplementationOf(const AID: String): TisInterfacedObjectClass;
var
  i:  Integer;
begin
  FIntList.LockList;
  try
    i := FIdList.IndexOf(AID);
    CheckCondition(i > -1, 'Object type "' + AID + '" is not registered');
    Result := TisInterfacedObjectClass(FIdList.Objects[i]);
  finally
    FIntList.UnlockList;
  end;
end;

procedure TisObjectFactory.SaveInstanceToStream(
  const AObject: IisInterfacedObject; const AStream: IisStream);
begin
  AStream.WriteShortString(AObject.TypeID);
  AObject.WriteToStream(AStream);
end;

function TisObjectFactory.GetRegisteredClasses: IisPointerList;
var
  i:  Integer;
begin
  Result := TisPointerList.Create;
  FIntList.LockList;
  try
    for i :=0 to FIdList.Count - 1 do
      Result.Add(FIdList.Objects[i]);
  finally
    FIntList.UnlockList;
  end;
end;

{ TisInterfacedObject }

function TisInterfacedObject.GetClone: IisInterfacedObject;
var
  S: IisStream;
  Cl: TisInterfacedObjectClass;
begin
  S := GetAsStream;
  Cl := TisInterfacedObjectClass(ClassType);
  Result := Cl.Create(nil, LockActive);
  Result.SetAsStream(S);
end;

function TisInterfacedObject.GetAsStream: IisStream;
begin
  Result := TisStream.Create;

  Result.WriteShortString(GetTypeID);

  Lock;
  try
    WriteToStream(Result);
  finally
    Unlock;
  end;
end;

function TisInterfacedObject.GetImplementation: TisInterfacedObject;
begin
  Result := Self;
end;


class function TisInterfacedObject.GetTypeID: String;
begin
  Result := 'O';
end;

procedure TisInterfacedObject.ReadFromStream(const AStream: IisStream);
var
  Rev: TisRevision;
begin
{$IFDEF EXCLUDE_REVISION}
  Rev := 0;
{$ELSE}
  Rev := AStream.ReadByte;
{$ENDIF}
  ReadSelfFromStream(AStream, Rev);
end;

procedure TisInterfacedObject.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
begin
end;

procedure TisInterfacedObject.SetAsStream(const AStream: IisStream);
var
  s: String;
begin
  AStream.Position := 0;
  s := AStream.ReadShortString;

  // Retry logic for backward compatibility (7/24/2009)
  // todo: remove in couple releases
  if s <> GetTypeID then
  begin
    AStream.Position := 0;
    s := AStream.ReadString;
  end
  else
    CheckCondition(s = GetTypeID, 'Stream data and object are not compatible. Expected "' + GetTypeID + '" but "' + s + '" met');

  Lock;
  try
    ReadFromStream(AStream);
  finally
    Unlock;
  end;
end;

procedure TisInterfacedObject.WriteSelfToStream(const AStream: IisStream);
begin
end;

procedure TisInterfacedObject.WriteToStream(const AStream: IisStream);
begin
{$IFNDEF EXCLUDE_REVISION}
  AStream.WriteByte(Revision);
{$ENDIF}
  WriteSelfToStream(AStream);
end;

procedure TisInterfacedObject.DoOnConstruction;
begin
end;

function TisInterfacedObject.IsNamedObject: Boolean;
begin
  Result := False;
end;

procedure TisInterfacedObject.LoadFromStorage;
begin
end;

procedure TisInterfacedObject.RemoveFromStorage;
begin
end;

procedure TisInterfacedObject.SaveToStorage;
begin
end;

function TisInterfacedObject.GetOwner: IisCollection;
begin
  Result := IntGetOwner;
end;

procedure TisInterfacedObject.IntSetOwner(const AOwner: TisCollection);
begin
  InterlockedExchange(Integer(Pointer(FOwner)), Integer(Pointer(AOwner)))
end;

procedure TisInterfacedObject.SetOwner(const AOwner: IisCollection);
var
  Own: TisCollection;
begin
  Own := IntGetOwner;

  if Assigned(AOwner) and (Own = AOwner.GetImplementation) then
    Exit;

  if Assigned(Own) then
    Own.RemoveChild(Self as IisInterfacedObject);

  if Assigned(AOwner) then
    AOwner.AddChild(Self as IisInterfacedObject);
end;

constructor TisInterfacedObject.Create(const AOwner: IisCollection; const AInitLock: Boolean);
begin
  if AInitLock then
    InitLock;
  DoOnConstruction;
  SetOwner(AOwner);
end;

function TisInterfacedObject.IntGetOwner: TisCollection;
begin
  Result := TisCollection(Pointer(InterlockedExchange(Integer(Pointer(FOwner)), Integer(Pointer(FOwner)))));
end;

function TisInterfacedObject.TypeID: String;
begin
  Result := GetTypeID;
end;

function TisInterfacedObject.Revision: TisRevision;
begin
  Result := 0;
end;

procedure TisInterfacedObject.DoOnDestruction;
begin
end;

destructor TisInterfacedObject.Destroy;
begin
  DoOnDestruction;
  inherited;
end;

procedure TisInterfacedObject.InitLock;
begin
  if not LockActive then
    FLock := TisLock.Create;
end;

procedure TisInterfacedObject.Lock;
begin
  if LockActive then
    FLock.Lock;
end;

function TisInterfacedObject.TryLock: Boolean;
begin
  if LockActive then
    Result := FLock.TryLock
  else
    Result := True;
end;

procedure TisInterfacedObject.Unlock;
begin
  if LockActive then
    FLock.Unlock;
end;

function TisInterfacedObject.LockActive: Boolean;
begin
  Result := Assigned(FLock);
end;

procedure TisInterfacedObject.FormatedRead(const AStorage: IisFormatedReader);
begin
end;

procedure TisInterfacedObject.FormatedWrite(const AStorage: IisFormatedWriter);
begin
end;

function TisInterfacedObject._AddRef: Integer;
begin
  Result := InterlockedIncrement(FRefCount);
end;

function TisInterfacedObject._Release: Integer;
begin
  Result := InterlockedDecrement(FRefCount);
  if Result = 0 then
  begin
    FRefCount := -1000000;  // to avoid calling destroy again if destructor used implicit _AddRef
    Destroy;
  end;
end;

procedure TisInterfacedObject.AfterConstruction;
begin
// Release the constructor's implicit refcount
  InterlockedDecrement(FRefCount);

{$IFDEF WATCH_OBJECTS}
  if not Assigned(ObjectsList) then
    ObjectsList := TThreadList.Create;
  ObjectsList.Add(Self);
{$ENDIF}
end;

procedure TisInterfacedObject.BeforeDestruction;
{$IFDEF WATCH_OBJECTS}
var
  L: TList;
begin
  L := ObjectsList.LockList;
  try
    L.Delete(L.IndexOf(Self));
  finally
    ObjectsList.UnlockList;
  end;
{$ELSE}
begin  
{$ENDIF}

  if (RefCount <> 0) and (RefCount <> -1000000) then
    System.Error(reInvalidPtr);
end;

class function TisInterfacedObject.NewInstance: TObject;
begin
  Result := inherited NewInstance;
  TisInterfacedObject(Result).FRefCount := 1;
end;

function TisInterfacedObject.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TisInterfacedObject.GetRefCount: Integer;
begin
  InterlockedExchange(Result, FRefCount);
end;

function TisInterfacedObject.Destroying: Boolean;
begin
  Result := GetRefCount < 0;
end;

{ TisCollection }

function TisCollection.AddChild(const AChildObject: IisInterfacedObject): Integer;
begin
  Lock;
  try
    Result := FChildren.IndexOf(AChildObject as IisInterfacedObject);
    if Result = -1 then
    begin
      if AChildObject.IsNamedObject then
        IntCheckUnique((AChildObject as IisNamedObject).Name);
      IntAddChild(AChildObject);
    end;
  finally
    Unlock;
  end;
end;

function TisCollection.IndexOf(const AChildObject: IisInterfacedObject): Integer;
begin
  Lock;
  try
    Result := FChildren.IndexOf(AChildObject as IisInterfacedObject);
  finally
    Unlock;
  end;
end;

procedure TisCollection.RemoveChild(const AChildObject: IisInterfacedObject);
begin
  if Assigned(AChildObject) then
  begin
    Lock;
    try
      FChildren.Delete(AChildObject as IisInterfacedObject);
      DeleteFromIndex(AChildObject);
      AChildObject.GetImplementation.IntSetOwner(nil);
    finally
      Unlock;
    end;
  end;
end;

function TisCollection.NameIsValid(const AName: String): Boolean;
begin
  Result := True;
end;


function TisCollection.FindChildByName(const AName: String): IisNamedObject;
var
  i: Integer;
begin
  Lock;
  try
    i := FIndex.IndexOf(AName);
    if i <> -1 then
      Result := FIndex.Objects[i] as IisNamedObject
    else
      Result := nil;
  finally
    Unlock;
  end;
end;

procedure TisCollection.IntCheckUnique(const AName: String);
begin
  if AName <> '' then
    CheckCondition(FindChildByName(AName) = nil, 'Object with name "' + AName + '" is already registered');
end;

function TisCollection.GetChild(Index: Integer): IisInterfacedObject;
begin
  Lock;
  try
    Result := FChildren[Index] as IisInterfacedObject;
  finally
    Unlock;
  end;
end;

function TisCollection.ChildCount: Integer;
begin
  Lock;
  try
    Result := FChildren.Count;
  finally
    Unlock;
  end;
end;

procedure TisCollection.RemoveAllChildren;
var
  i: Integer;
begin
  Lock;
  try
    for i := 0 to ChildCount - 1 do
      GetChild(i).GetImplementation.IntSetOwner(nil);

    FIndex.Clear;
    FChildren.Clear;
  finally
    Unlock;
  end;
end;

destructor TisCollection.Destroy;
begin
  RemoveAllChildren;
  inherited;
end;


procedure TisCollection.LoadAllFromStorage;
begin
  LoadFromStorage;
  LoadChildrenFromStorage;
end;

procedure TisCollection.SaveAllToStorage;
var
  i: Integer;
begin
  SaveToStorage;

  for i := 0 to ChildCount - 1 do
    if Supports(GetChild(i), IisCollection) then
      (GetChild(i) as IisCollection).SaveAllToStorage
    else
      GetChild(i).SaveToStorage;
end;

procedure TisCollection.LoadChildrenFromStorage;
begin
end;

procedure TisCollection.RemoveFromStorage;
var
  i: Integer;
begin
  for i := 0 to ChildCount - 1 do
    GetChild(i).RemoveFromStorage;
    
  inherited;
end;


procedure TisCollection.ReadFromStream(const AStream: IisStream);
begin
  inherited;
  if FDeleteChildrenBeforeRead then
    RemoveAllChildren;
  ReadChildrenFromStream(AStream);
end;

procedure TisCollection.WriteToStream(const AStream: IisStream);
begin
  inherited;
  WriteChildrenToStream(AStream);
end;

procedure TisCollection.ReadChildrenFromStream(const AStream: IisStream);
var
  i, n: Integer;
  s: String;
  Chld: IisInterfacedObject;
begin
  n := AStream.ReadInteger;

  FChildren.Capacity := n;
  FIndex.Capacity := n;
  for i := 1 to n do
  begin
    s := AStream.ReadShortString;
    Chld := CreateChildByTypeID(s);
    CheckCondition(Assigned(Chld), 'Child object is not created. Unknown type ID "' + s + '"');
    Chld.ReadFromStream(AStream);
  end;
end;

procedure TisCollection.WriteChildrenToStream(const AStream: IisStream);
var
  i: Integer;
begin
  AStream.WriteInteger(ChildCount);

  for i := 0 to ChildCount - 1 do
  begin
    AStream.WriteShortString(GetChild(i).TypeID);
    GetChild(i).WriteToStream(AStream);
  end;
end;


function TisCollection.CreateChildByTypeID(const ATypeID: String): IisInterfacedObject;
begin
  Result := ObjectFactory.CreateInstanceOf(ATypeID);
  if Assigned(Result) then
  begin
    Lock;
    try
      IntAddChild(Result);
    finally
      Unlock;
    end;
  end;
end;

procedure TisCollection.SetAsStream(const AStream: IisStream);
begin
  RemoveAllChildren;
  inherited;
end;


function TisCollection.GetChildrenByType(const ATypeID: String): IisList;
var
  i: Integer;
  Chld: IisInterfacedObject;
begin
  Result := TisList.Create;

  Lock;
  try
    for i := 0 to FChildren.Count - 1 do
    begin
      Chld := GetChild(i);
      if AnsiSameText(Chld.TypeID, ATypeID) then
        Result.Add(Chld);
    end;
  finally
    Unlock;
  end;
end;

procedure TisCollection.DoOnConstruction;
begin
  inherited;
  FChildren := TisInterfaceList.Create;
  FIndex := TisStringList.CreateAsIndex;
  FDeleteChildrenBeforeRead := True;  
end;

function TisCollection.SubstituteChild(const ASource, ATarget: IisInterfacedObject): Boolean;
var
  i: Integer;
begin
  Lock;
  try
    i := IndexOf(ASource);
    Result := i <> -1;
    if Result then
    begin
      ATarget.Owner := Self;
      FChildren.Insert(i, ATarget as IisInterfacedObject);
      ASource.Owner := nil;
    end;
  finally
    Unlock;
  end;
end;


procedure TisCollection.SetChildName(const AChild: IisNamedObject; const AName: String);
begin
  Lock;
  try
    if AnsiSameText(AName, AChild.Name) then
      Exit;

    CheckCondition(NameIsValid(AName), 'Object name "' + AName + '" is invalid');
    IntCheckUnique(AName);
    DeleteFromIndex(AChild);
    TisNamedObject(AChild.GetImplementation).FName := AName;
    AddToIndex(AChild);
  finally
    Unlock;
  end;
end;

function TisCollection.GetChildName(const AChild: IisNamedObject): String;
begin
  Lock;
  try
    Result := TisNamedObject(AChild.GetImplementation).FName; 
  finally
    Unlock;
  end;
end;

class function TisCollection.GetTypeID: String;
begin
  Result := 'C';
end;

procedure TisCollection.AddToIndex(const AChild: IisInterfacedObject);
begin
  if AChild.IsNamedObject and ((AChild as IisNamedObject).Name <> '') then
  begin
    if FIndex.Count = FIndex.Capacity then
      FIndex.Capacity := FIndex.Capacity + 100;  
    FIndex.AddObject((AChild as IisNamedObject).Name, AChild);
  end;
end;

procedure TisCollection.DeleteFromIndex(const AChild: IisInterfacedObject);
var
  i: Integer;
begin
  if AChild.IsNamedObject then
  begin
    i := FIndex.IndexOf((AChild as IisNamedObject).Name);
    if i <> -1 then
      FIndex.Delete(i);
  end;
end;

function TisCollection.GetChildrenList: IisList;
var
  i: Integer;
begin
  Result := TisList.Create;
  Result.Capacity := ChildCount;
  Lock;
  try
    for i := 0 to ChildCount - 1 do
      Result.Add(GetChild(i));
  finally
    Unlock;
  end;
end;

function TisCollection.IndexOf(const AName: String): Integer;
var
  O: IisInterfacedObject;
begin
  O := FindChildByName(AName);
  if Assigned(O) then
    Result := IndexOf(O)
  else
    Result := -1;  
end;

procedure TisCollection.Sort(const ADirection: String = 'asc');
var
  i: Integer;
begin
  Lock;
  try
    for i := 0 to FIndex.Count - 1 do
      FChildren.Delete(FIndex.Objects[i] as IisInterfacedObject);

    if AnsiSameText(ADirection, 'asc') then
    begin
      for i := 0 to FIndex.Count - 1 do
        FChildren.Add(FIndex.Objects[i] as IisInterfacedObject);
    end
    else
    begin
      for i := FIndex.Count - 1 downto 0 do
        FChildren.Add(FIndex.Objects[i] as IisInterfacedObject);
    end;
  finally
    Unlock;
  end;
end;

function TisCollection.IntAddChild(const AChildObject: IisInterfacedObject): Integer;
begin
  Result := FChildren.Add(AChildObject as IisInterfacedObject);
  AChildObject.GetImplementation.IntSetOwner(Self);
  AddToIndex(AChildObject);
end;

procedure TisCollection.Delete(const AIndex: Integer);
var
  O: IisInterfacedObject;
begin
  Lock;
  try
    O := GetChild(AIndex);
    RemoveChild(O);
  finally
    Unlock;
  end;
end;

procedure TisCollection.FormatedRead(const AStorage: IisFormatedReader);
var
  L: IisInterfaceList;
  i: Integer;
begin
  inherited;
  
  if FDeleteChildrenBeforeRead then
    RemoveAllChildren;

  L := TisInterfaceList.Create;
  (L as IisInterfacedObject).FormatedRead(AStorage);
  for i := 0 to L.Count - 1 do
    IntAddChild(L[i] as IisInterfacedObject);
end;

procedure TisCollection.FormatedWrite(const AStorage: IisFormatedWriter);
begin
  inherited;
  (FChildren as IisInterfacedObject).FormatedWrite(AStorage);
end;

procedure TisCollection.InsertChild(const AIndex: Integer; const AChildObject: IisInterfacedObject);
var
  i: Integer;
begin
  Lock;
  try
    i := FChildren.IndexOf(AChildObject as IisInterfacedObject);
    if i = -1 then
    begin
      if AChildObject.IsNamedObject then
        IntCheckUnique((AChildObject as IisNamedObject).Name);
      IntInsChild(AIndex, AChildObject);
    end;
  finally
    Unlock;
  end;
end;

procedure TisCollection.IntInsChild(const AIndex: Integer; const AChildObject: IisInterfacedObject);
begin
  FChildren.Insert(AIndex, AChildObject as IisInterfacedObject);
  AChildObject.GetImplementation.IntSetOwner(Self);
  AddToIndex(AChildObject);
end;

procedure TisCollection.RemoveChild(const AIndex: Integer);
var
  O: IisInterfacedObject;
begin
  if AIndex <> -1 then
  begin
    Lock;
    try
      O := GetChild(AIndex);
      FChildren.Delete(AIndex);
      DeleteFromIndex(O);
      O.GetImplementation.IntSetOwner(nil);
    finally
      Unlock;
    end;
  end;
end;

procedure TisCollection.MoveChild(const AIndex, ANewIndex: Integer);
var
  O: IisInterfacedObject;
begin
  Lock;
  try
    O := GetChild(AIndex);
    RemoveChild(AIndex);
    InsertChild(ANewIndex, O);
  finally
    Unlock;
  end;
end;

procedure TisCollection.MoveChild(const AChildObject: IisInterfacedObject; const ANewIndex: Integer);
var
  i: Integer;
begin
  Lock;
  try
    i := IndexOf(AChildObject);
    MoveChild(i, ANewIndex);
  finally
    Unlock;
  end;
end;

{ TisStringList }

function TisStringList.Add(const S: string): Integer;
begin
  Lock;
  try
    Result := FList.Add(S);
  finally
    Unlock;
  end;
end;

function TisStringList.AddObject(const S: string; AObject: IInterface): Integer;
begin
  Lock;
  try
    Result := FList.AddObject(S, Pointer(AObject as IInterface));
    if Assigned(AObject) then
      AObject._AddRef;
  finally
    Unlock;
  end;
end;

procedure TisStringList.AddStrings(Strings: IisStringList);
begin
  Lock;
  try
    FList.AddStrings(TisStringList((Strings as IisInterfacedObject).GetImplementation).FList);
  finally
    Unlock;
  end;
end;

procedure TisStringList.Append(const S: string);
begin
  Lock;
  try
    FList.Append(S);
  finally
    Unlock;
  end;
end;

procedure TisStringList.Clear;
var
  i: Integer;
begin
  Lock;
  try
    for i := 0 to FList.Count - 1 do
      if Assigned(FList.Objects[i]) then
        IInterface(Pointer(FList.Objects[i]))._Release;
    FList.Clear;
  finally
    Unlock;
  end;
end;

constructor TisStringList.CreateAsIndex(const AInitLock: Boolean = False);
begin
  Create(nil, AInitLock);
  SetDuplicates(dupError);
  SetCaseSensitive(False);
  SetSorted(True);
end;

constructor TisStringList.CreateAsStrings(const AStrings: TStrings; const AInitLock: Boolean);
begin
  Create(nil, AInitLock);
  Clear;
  FList.Text := AStrings.Text;
end;

constructor TisStringList.CreateUnique(const AInitLock: Boolean; const Reserved: Boolean);
begin
  CreateAsIndex(AInitLock);
  SetDuplicates(dupIgnore);
end;

procedure TisStringList.Delete(Index: Integer);
begin
  Lock;
  try
    if Assigned(FList.Objects[Index]) then
      IInterface(Pointer(FList.Objects[Index]))._Release;
    FList.Delete(Index);
  finally
    Unlock;
  end;
end;

destructor TisStringList.Destroy;
begin
  Clear;
  FreeAndNil(FList);
  inherited;
end;

procedure TisStringList.DoOnConstruction;
begin
  inherited;
  FList := TStringList.Create;
end;

function TisStringList.Equals(Strings: IisStringList): Boolean;
var
  i: Integer;
begin
  Result := False;
  Lock;
  try
    if GetCount <> Strings.Count then
      Exit;

    for i := 0 to GetCount - 1 do
      if GetString(i) <> Strings[i] then
        Exit;

    Result := True;
  finally
    Unlock;
  end;
end;

procedure TisStringList.Exchange(Index1, Index2: Integer);
begin
  Lock;
  try
    FList.Exchange(Index1, Index2);
  finally
    Unlock;
  end;
end;

function TisStringList.Find(const S: string; var Index: Integer): Boolean;
begin
  Lock;
  try
    Result := FList.Find(S, Index);
  finally
    Unlock;
  end;
end;

procedure TisStringList.FormatedRead(const AStorage: IisFormatedReader);
var
  i, n: Integer;
begin
  inherited;
  n := AStorage.BeginReadGroup('Items');
  try
    SetCapacity(n);
    for i := 0 to n - 1 do
      FList.Add(AStorage.ReadValueByIndex(i));
  finally
    AStorage.EndReadGroup;
  end;
end;

procedure TisStringList.FormatedWrite(const AStorage: IisFormatedWriter);
var
  i: Integer;
begin
  inherited;
  AStorage.BeginWriteGroup('Items');
  try
    for i := 0 to FList.Count - 1 do
      AStorage.WriteValue('I' + IntToStr(i), FList[i]);
  finally
    AStorage.EndWriteGroup;
  end;
end;

function TisStringList.GetCapacity: Integer;
begin
  Lock;
  try
    Result := FList.Capacity;
  finally
    Unlock;
  end;
end;

function TisStringList.GetCaseSensitive: Boolean;
begin
  Lock;
  try
    Result := FList.CaseSensitive;
  finally
    Unlock;
  end;
end;

function TisStringList.GetCommaText: string;
begin
  Lock;
  try
    Result := FList.CommaText;
  finally
    Unlock;
  end;
end;

function TisStringList.GetCount: Integer;
begin
  Lock;
  try
    Result := FList.Count;
  finally
    Unlock;
  end;
end;

function TisStringList.GetDelimitedText: string;
begin
  Lock;
  try
    Result := FList.DelimitedText;
  finally
    Unlock;
  end;
end;

function TisStringList.GetDelimiter: Char;
begin
  Lock;
  try
    Result := FList.Delimiter;
  finally
    Unlock;
  end;
end;

function TisStringList.GetDuplicates: TDuplicates;
begin
  Lock;
  try
    Result := FList.Duplicates;
  finally
    Unlock;
  end;
end;

function TisStringList.GetName(Index: Integer): string;
begin
  Lock;
  try
    Result := FList.Names[Index];
  finally
    Unlock;
  end;
end;

function TisStringList.GetNameValueSeparator: Char;
begin
  Lock;
  try
    Result := FList.NameValueSeparator;
  finally
    Unlock;
  end;
end;

function TisStringList.GetObject(Index: Integer): IInterface;
begin
  Lock;
  try
    Result := IInterface(Pointer(FList.Objects[Index]));
  finally
    Unlock;
  end;
end;

function TisStringList.GetQuoteChar: Char;
begin
  Lock;
  try
    Result := FList.QuoteChar;
  finally
    Unlock;
  end;
end;

function TisStringList.GetSorted: Boolean;
begin
  Lock;
  try
    Result := FList.Sorted;
  finally
    Unlock;
  end;
end;

function TisStringList.GetString(Index: Integer): string;
begin
  Lock;
  try
    Result := Flist[Index];
  finally
    Unlock;
  end;
end;

function TisStringList.GetText: string;
begin
  Lock;
  try
    Result := FList.Text;
  finally
    Unlock;
  end;
end;

class function TisStringList.GetTypeID: String;
begin
  Result := 'S';
end;

function TisStringList.GetValue(const AName: string): string;
begin
  Lock;
  try
    Result := FList.Values[AName];
  finally
    Unlock;
  end;
end;

function TisStringList.GetValueFromIndex(Index: Integer): string;
begin
  Lock;
  try
    Result := FList.ValueFromIndex[Index];
  finally
    Unlock;
  end;
end;

function TisStringList.IndexOf(const S: string): Integer;
begin
  Lock;
  try
    Result := FList.IndexOf(S);
  finally
    Unlock;
  end;
end;

function TisStringList.IndexOfName(const Name: string): Integer;
begin
  Lock;
  try
    Result := FList.IndexOfName(Name);
  finally
    Unlock;
  end;
end;

function TisStringList.IndexOfObject(AObject: IInterface): Integer;
begin
  Lock;
  try
    Result := FList.IndexOfObject(Pointer(AObject as IInterface));
  finally
    Unlock;
  end;
end;

procedure TisStringList.Insert(Index: Integer; const S: string);
begin
  Lock;
  try
    FList.Insert(Index, S);
  finally
    Unlock;
  end;
end;

procedure TisStringList.InsertObject(Index: Integer; const S: string; AObject: IInterface);
begin
  Lock;
  try
    FList.InsertObject(Index, S, Pointer(AObject as IInterface));
    if Assigned(AObject) then
      AObject._AddRef;
  finally
    Unlock;
  end;
end;

procedure TisStringList.LoadFromFile(const FileName: string);
begin
  Lock;
  try
    FList.LoadFromFile(FileName);
  finally
    Unlock;
  end;
end;

procedure TisStringList.LoadFromStream(Stream: TStream);
begin
  Lock;
  try
    FList.LoadFromStream(Stream);
  finally
    Unlock;
  end;
end;

procedure TisStringList.Move(CurIndex, NewIndex: Integer);
begin
  Lock;
  try
    FList.Move(CurIndex, NewIndex);
  finally
    Unlock;
  end;
end;

procedure TisStringList.ReadSelfFromStream(const AStream: IisStream;
  const ARevision: TisRevision);
var
  i, cnt: Integer;  
begin
  inherited;
  cnt := AStream.ReadInteger;
  FList.Capacity := cnt;
  for i := 0 to cnt - 1 do
    FList.Add(AStream.ReadString);
end;

procedure TisStringList.SaveToFile(const FileName: string);
begin
  Lock;
  try
    FList.SaveToFile(FileName);
  finally
    Unlock;
  end;
end;

procedure TisStringList.SaveToStream(Stream: TStream);
begin
  Lock;
  try
    FList.SaveToStream(Stream);
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetCapacity(NewCapacity: Integer);
begin
  Lock;
  try
    FList.Capacity := NewCapacity;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetCaseSensitive(const AValue: Boolean);
begin
  Lock;
  try
    FList.CaseSensitive := AValue;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetCommaText(const AValue: string);
begin
  Lock;
  try
    FList.CommaText := AValue;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetDelimitedText(const AValue: string);
begin
  Lock;
  try
    FList.DelimitedText := AValue;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetDelimiter(const AValue: Char);
begin
  Lock;
  try
    FList.Delimiter := AValue;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetDuplicates(const AValue: TDuplicates);
begin
  Lock;
  try
    FList.Duplicates := AValue;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetNameValueSeparator(const AValue: Char);
begin
  Lock;
  try
    FList.NameValueSeparator := AValue;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetObject(Index: Integer; AObject: IInterface);
begin
  Lock;
  try
    if Assigned(FList.Objects[Index]) then
      IInterface(Pointer(FList.Objects[Index]))._Release;
    FList.Objects[Index] := Pointer(AObject as IInterface);
    if Assigned(AObject) then
      AObject._AddRef;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetQuoteChar(const AValue: Char);
begin
  Lock;
  try
    FList.QuoteChar := AValue;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetSorted(const AValue: Boolean);
begin
  Lock;
  try
    FList.Sorted := AValue;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetString(Index: Integer; const S: string);
begin
  Lock;
  try
    Flist[Index] := S;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetText(const AValue: string);
begin
  Lock;
  try
    FList.Text := AValue;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetValue(const AName, AValue: string);
begin
  Lock;
  try
    FList.Values[AName] := AValue;
  finally
    Unlock;
  end;
end;

procedure TisStringList.SetValueFromIndex(Index: Integer; const AValue: string);
begin
  Lock;
  try
    FList.ValueFromIndex[Index] := AValue;
  finally
    Unlock;
  end;
end;

function CompDesc(List: TStringList; Index1, Index2: Integer): Integer;
begin
  if List.CaseSensitive then
    Result := AnsiCompareStr(List[Index2], List[Index1])
  else
    Result := AnsiCompareText(List[Index2], List[Index1]);
end;

procedure TisStringList.Sort(const ADirection: String);
begin
  Lock;
  try
    if AnsiSameText(ADirection, 'asc') then
      FList.Sort
    else
    begin
      FList.Sorted := False;
      FList.CustomSort(CompDesc);
    end;
  finally
    Unlock;
  end;
end;

function TisStringList.VCLStrings: TStrings;
begin
  Result := FList;
end;

procedure TisStringList.WriteSelfToStream(const AStream: IisStream);
var
  i: Integer;
begin
  inherited;
  AStream.WriteInteger(FList.Count);
  for i := 0 to FList.Count - 1 do
    AStream.WriteString(FList[i]);
end;

{ TisNamedValue }

class function TisNamedValue.GetTypeID: String;
begin
  Result := 'V';
end;

function TisNamedValue.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TisNamedValue.SetValue(const AValue: Variant);
begin
  FValue := AValue;
end;

const sObjectValID = #1;

procedure TisNamedValue.WriteSelfToStream(const AStream: IisStream);
var
  Obj: IisInterfacedObject;
begin
  inherited;

  if (VarType(FValue) = varUnknown) and Supports(FValue, IisInterfacedObject) then
  begin
    AStream.WriteShortString(sObjectValID + GetName);
    Obj := IInterface(FValue) as IisInterfacedObject;
    AStream.WriteShortString(Obj.TypeID);
    Obj.WriteToStream(AStream);
  end

  else
  begin
    AStream.WriteShortString(GetName);
    AStream.WriteVariant(FValue);
  end;
end;

procedure TisNamedValue.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
var
  ObjType, sName: String;
  Obj: IisInterfacedObject;
begin
  inherited;
  sName := AStream.ReadShortString;

  if (sName <> '') and (sName[1] = sObjectValID) then
  begin
    Delete(sName,1,1);
    ObjType := AStream.ReadShortString;
    Obj := ObjectFactory.CreateInstanceOf(ObjType);
    Obj.ReadFromStream(AStream);
    FValue := Obj;
  end

  else
    FValue := AStream.ReadVariant;

  SetName(sName);
end;


procedure TisNamedValue.FormatedRead(const AStorage: IisFormatedReader);
begin
  inherited;
  SetName(AStorage.ReadValue('Name', GetName));
  FValue := AStorage.ReadValue('Value', FValue);
end;

procedure TisNamedValue.FormatedWrite(const AStorage: IisFormatedWriter);
begin
  inherited;
  AStorage.WriteValue('Name', GetName);
  AStorage.WriteValue('Value', FValue);
end;

{ TisListOfValues }

function TisListOfValues.AddValue(const AName: String; const AValue: Variant): Integer;
var
  Val: IisNamedValue;
begin
  Lock;
  try
    Val := FindValue(AName);
    if not Assigned(Val) then
    begin
      Val := TisNamedValue.Create;
      (Val as IisNamedObject).Name := AName;
      Result := IntAddChild(Val as IisInterfacedObject);
    end
    else
      Result := -1;

    Val.Value := AValue;
  finally
    Unlock;
  end;
end;

procedure TisListOfValues.Assign(const ASource: IisListOfValues);
var
  i: Integer;
begin
  Lock;
  try
    Clear;
    if Assigned(ASource) then
    begin
      ASource.Lock;
      try
        for i := 0 to ASource.Count - 1 do
          AddValue(ASource[i].Name, ASource[i].Value);
      finally
        ASource.Unlock;
      end;
    end;
  finally
    Unlock;
  end;
end;

procedure TisListOfValues.Clear;
begin
  RemoveAllChildren;
end;

function TisListOfValues.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TisListOfValues.DeleteValue(const AName: String);
var
  i: Integer;
begin
  Lock;
  try
    i := IndexOf(AName);
    if i <> -1 then
      DeleteValue(i);
  finally
    Unlock;
  end;
end;

procedure TisListOfValues.DeleteValue(const AIndex: Integer);
begin
  Lock;
  try
    RemoveChild(AIndex);
  finally
    Unlock;
  end;
end;

function TisListOfValues.FindValue(const AName: String): IisNamedValue;
begin
  Result := FindChildByName(AName) as IisNamedValue;
end;

function TisListOfValues.GetClone: IisListOfValues;
begin
  Result := inherited GetClone as IisListOfValues;
end;

class function TisListOfValues.GetTypeID: String;
begin
  Result := 'L';
end;

function TisListOfValues.GetValue(const AIndex: Integer): IisNamedValue;
begin
  Result := GetChild(AIndex) as IisNamedValue;
end;

function TisListOfValues.GetValueByName(const AName: String): Variant;
var
  Val: IisNamedValue;
begin
  Val := FindValue(AName);
  CheckCondition(Assigned(Val), 'Item "' + AName + '" does not exist');
  Result := Val.Value;
end;

procedure TisListOfValues.MoveValue(const AName: String; const ANewIndex: Integer);
begin
  Lock;
  try
    MoveValue(IndexOf(AName), ANewIndex);
  finally
    Unlock;
  end;
end;

procedure TisListOfValues.MoveValue(const AIndex, ANewIndex: Integer);
begin
  MoveChild(AIndex, ANewIndex);
end;

procedure TisListOfValues.SetValueByName(const AName: String; const AValue: Variant);
var
  Val: IisNamedValue;
begin
  Val := FindValue(AName);
  CheckCondition(Assigned(Val), 'Item "' + AName + '" does not exist');
  Val.Value := AValue;
end;

function TisListOfValues.TryGetValue(const AName: String; const ADefault: Variant): Variant;
begin
  if ValueExists(AName) then
    Result := GetValueByName(AName)
  else
    Result := ADefault;  
end;

function TisListOfValues.ValueExists(const AName: String): Boolean;
begin
  Result:= FindValue(AName) <> nil;
end;


{ TisNamedObject }

function TisNamedObject.GetName: String;
var
  Own: TisCollection;
begin
  Own := IntGetOwner;

  if Assigned(Own) then
    Result := Own.GetChildName(Self)
  else
    Result := FName;
end;

class function TisNamedObject.GetTypeID: String;
begin
  Result := 'N';
end;

function TisNamedObject.IsNamedObject: Boolean;
begin
  Result := True;
end;

procedure TisNamedObject.SetName(const AName: String);
var
  Own: TisCollection;
begin
  Own := IntGetOwner;

  if Assigned(Own) then
    Own.SetChildName(Self, AName)
  else
    FName := AName;
end;

{ TisList }

function TisList.CompareItems(const Item1, Item2: IInterface): Integer;
begin
  if Assigned(FCompareProc) then
    Result := FCompareProc(Item1, Item2)
  else
  begin
    Result := 0;
    CheckCondition(False, 'Cannot compare items');
  end;
end;

function TisList.GetCopy: IisList;
var
  i: Integer;
begin
  Result := TisList.Create;
  Lock;
  try
    Result.Capacity := Count;
    for i := 0 to Count - 1 do
      Result.Add(Get(i));
  finally
    Unlock;
  end;
end;

procedure TisList.LoadItemsFromStream(const AStream: IisStream);
var
  i, n: Integer;
  s: String;
  Item: IisInterfacedObject;
begin
  n := AStream.ReadInteger;
  Capacity := Count + n;
  for i := 1 to n do
  begin
    s := AStream.ReadShortString;
    Item := ObjectFactory.CreateInstanceOf(s);
    CheckCondition(Assigned(Item), 'Item object is not created. Unknown type ID "' + s + '"');
    Item.ReadFromStream(AStream);
    Add(Item);
  end;
end;

procedure TisList.QuickSort(L, R: Integer);
var
  I, J, P: Integer;
begin
  repeat
    I := L;
    J := R;
    P := (L + R) shr 1;
    repeat
      while CompareItems(Items[I], Items[P]) < 0 do Inc(I);
      while CompareItems(Items[J], Items[P]) > 0 do Dec(J);
      if I <= J then
      begin
        Exchange(I, J);
        if P = I then
          P := J
        else if P = J then
          P := I;
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then QuickSort(L, J);
    L := I;
  until I >= R;
end;

procedure TisList.SaveItemsToStream(const AStream: IisStream);
var
  i: Integer;
begin
  AStream.WriteInteger(Count);
  for i := 0 to Count - 1 do
  begin
    AStream.WriteShortString((Items[i] as IisInterfacedObject).TypeID);
    (Items[i] as IisInterfacedObject).WriteToStream(AStream);
  end;
end;

procedure TisList.SetCompareItemProc(const AProc: TListCompareItemProc);
begin
  FCompareProc := AProc;
end;

procedure TisList.Sort;
begin
  Lock;
  try
    if Count > 0 then
      QuickSort(0, Count - 1);
  finally
    Unlock;
  end;
end;

{ TisVariant }

constructor TisVariant.CreateAs(const AValue: Variant);
begin
  FValue := AValue;
end;

function TisVariant.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TisVariant.SetValue(const AValue: Variant);
begin
  FValue := AValue;
end;


{ TisParamsCollection }

function TisParamsCollection.AddParams(const AName: String = ''): IisListOfValues;
begin
  Result := TisParamsCollectionItem.Create(nil, LockActive);
  (Result as IisCollection).Name := AName;

  Lock;
  try
    IntAddChild(Result as IisCollection);
  finally
    Unlock;
  end;
end;

procedure TisParamsCollection.Clear;
begin
  RemoveAllChildren;
end;

function TisParamsCollection.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TisParamsCollection.DeleteParams(const AIndex: Integer);
var
  P: IisInterfacedObject;
begin
  Lock;
  try
    P := GetChild(AIndex);
    if Assigned(P) then
      RemoveChild(P);
  finally
    Unlock;
  end;
end;

procedure TisParamsCollection.DeleteParams(const AParam: IisListOfValues);
begin
  RemoveChild(AParam as IisInterfacedObject);
end;

procedure TisParamsCollection.DeleteParams(const AParam: String);
var
  P: IisListOfValues;
begin
  Lock;
  try
    P := ParamsByName(AParam);
    if Assigned(P) then
      DeleteParams(P);
  finally
    Unlock;
  end;
end;

function TisParamsCollection.GetClone: IisParamsCollection;
begin
  Result := inherited GetClone as IisParamsCollection;
end;

function TisParamsCollection.GetParams(const AIndex: Integer): IisListOfValues;
begin
  Result := GetChild(AIndex) as IisListOfValues;
end;

class function TisParamsCollection.GetTypeID: String;
begin
  Result := 'PC';
end;

function TisParamsCollection.ParamName(const AIndex: Integer): String;
begin
  Result := (GetChild(AIndex) as IisNamedObject).Name;
end;

function TisParamsCollection.ParamsByName(const AName: String): IisListOfValues;
begin
  Result := FindChildByName(AName) as IisListOfValues;
end;


{ TisInterProcessLock }

constructor TisInterProcessLock.Create(const AName: String);
begin
  FLock := TisLock.Create;
  
  // The name of mutex is limited to MAX_PATH characters and
  // can contain any character except the backslash path-separator character (\) (see MSDN)
  FName := StringReplace(AName, '\', '/', [rfReplaceAll]);
  if Length(FName) >= MAX_PATH then
   Delete(FName, 1, Length(FName) + 1 - MAX_PATH); // in case if name is file name let's leave the most meaningful part of it
end;

destructor TisInterProcessLock.Destroy;
begin
  FreeAndNil(FMutex);
  inherited;
end;

procedure TisInterProcessLock.Lock;
begin
  FLock.Lock;
  Inc(FLockCount);

  if not Assigned(FMutex) then
  begin
    FMutex := TisNamedMutex.Create(FName, True);
    Win32Check(FMutex.Handle <> 0);
    WaitForSingleObject(FMutex.Handle, INFINITE);
  end;
end;

function TisInterProcessLock.TryLock: Boolean;
begin
  Result := FLock.TryLock;
  if Result then
    try
      Inc(FLockCount);
      Result := Assigned(FMutex);
      if not Result then
        try
          FMutex := TisNamedMutex.Create(FName, True);
          Win32Check(FMutex.Handle <> 0);
          Result := WaitForSingleObject(FMutex.Handle, 0) = WAIT_OBJECT_0;
        finally
          if not Result then
            FreeAndNil(FMutex);
        end;
    except
      Dec(FLockCount);
      FLock.Unlock;
    end;
end;

procedure TisInterProcessLock.Unlock;
begin
  try
    if FLockCount = 1 then
      try
        Win32Check(ReleaseMutex(FMutex.Handle));
      finally
        FreeAndNil(FMutex);
      end;
  finally
    Dec(FLockCount);
    Flock.Unlock;
  end;
end;

{ TisParamsCollectionItem }

procedure TisParamsCollectionItem.FormatedRead(const AStorage: IisFormatedReader);
begin
  inherited;
  SetName(AStorage.ReadValue('Name', GetName));
end;

procedure TisParamsCollectionItem.FormatedWrite(const AStorage: IisFormatedWriter);
begin
  inherited;
  AStorage.WriteValue('Name', GetName);
end;

class function TisParamsCollectionItem.GetTypeID: String;
begin
  Result := 'PI';
end;

procedure TisParamsCollectionItem.ReadSelfFromStream(
  const AStream: IisStream; const ARevision: TisRevision);
begin
  inherited;
  SetName(AStream.ReadShortString);
end;

procedure TisParamsCollectionItem.WriteSelfToStream(
  const AStream: IisStream);
begin
  inherited;
  AStream.WriteShortString(GetName);
end;

{ TisLock }

constructor TisLock.Create;
begin
  FCS := TisCriticalSection.Create;
end;

destructor TisLock.Destroy;
begin
  FreeAndNil(FCS);
  inherited;
end;

procedure TisLock.Lock;
begin
  FCS.Enter;
end;

function TisLock.TryLock: Boolean;
begin
  Result := FCS.TryLock;
end;

procedure TisLock.Unlock;
begin
  FCS.Leave;
end;

{ TisCriticalSection }

function TisCriticalSection.TryLock: Boolean;
begin
  Result := TryEnterCriticalSection(FSection);
end;

{ TisVariantList }

function TisVariantList.Add(const V: Variant): Integer;
var
  PV: PVariant;
begin
  New(PV);
  PV^ := V;

  Lock;
  try
    Result := FList.Add(PV);
  finally
    Unlock;
  end;
end;

procedure TisVariantList.AddVariants(const Variants: IisVariantList);
var
  i: Integer;
begin
  Lock;
  try
    if GetCount + Variants.Count > GetCapacity then
      SetCapacity(GetCount + Variants.Count);

    for i := 0 to Variants.Count - 1 do
      Add(Variants[i]);
  finally
    Unlock;
  end;
end;

procedure TisVariantList.Clear;
var
  i: Integer;
begin
  Lock;
  try
    for i := 0 to GetCount - 1 do
      Dispose(PVariant(FList[i]));
    FList.Clear;
  finally
    Unlock;
  end;
end;

procedure TisVariantList.Delete(const Index: Integer);
begin
  Lock;
  try
    Dispose(PVariant(FList[Index]));
    FList.Delete(Index);
  finally
    Unlock;
  end;
end;

destructor TisVariantList.Destroy;
begin
  Clear;
  inherited;
end;

procedure TisVariantList.DoOnConstruction;
begin
  inherited;
  FList := TList.Create;
end;

procedure TisVariantList.Exchange(const Index1, Index2: Integer);
begin
  Lock;
  try
    FList.Exchange(Index1, Index2);
  finally
    Unlock;
  end;
end;

procedure TisVariantList.FormatedRead(const AStorage: IisFormatedReader);
var
  i, n: Integer;
begin
  inherited;
  Clear;
  n := AStorage.BeginReadGroup('Items');
  try
    SetCapacity(n);
    for i := 0 to n - 1 do
      Add(AStorage.ReadValueByIndex(i));
  finally
    AStorage.EndReadGroup;
  end;
end;

procedure TisVariantList.FormatedWrite(const AStorage: IisFormatedWriter);
var
  i: Integer;
begin
  inherited;
  AStorage.BeginWriteGroup('Items');
  try
    for i := 0 to GetCount - 1 do
      AStorage.WriteValue('I' + IntToStr(i), GetVariant(i));
  finally
    AStorage.EndWriteGroup;
  end;
end;

function TisVariantList.GetCapacity: Integer;
begin
  Lock;
  try
    Result := FList.Capacity;
  finally
    Unlock;
  end;
end;

function TisVariantList.GetCount: Integer;
begin
  Lock;
  try
    Result := FList.Count;
  finally
    Unlock;
  end;
end;

class function TisVariantList.GetTypeID: String;
begin
  Result := 'VL';
end;

function TisVariantList.GetVariant(Index: Integer): Variant;
begin
  Lock;
  try
    Result := PVariant(FList[Index])^;
  finally
    Unlock;
  end;
end;

function TisVariantList.IndexOf(const V: Variant): Integer;
var
  i: Integer;
begin
  Result := -1;
  Lock;
  try
    for i  := 0 to GetCount - 1 do
      if VarCompareValue(GetVariant(i), V) = vrEqual then
      begin
        Result := i;
        break;
      end;
  finally
    Unlock;
  end;
end;

procedure TisVariantList.Insert(const Index: Integer; const V: Variant);
var
  PV: PVariant;
begin
  New(PV);
  PV^ := V;

  Lock;
  try
    FList.Insert(Index, PV);
  finally
    Unlock;
  end;
end;

procedure TisVariantList.LoadFromFile(const FileName: string);
var
  F: IisStream;
begin
  F := TisStream.CreateFromFile(FileName);
  Lock;
  try
    LoadFromStream(F);
  finally
    Unlock;
  end;
end;

procedure TisVariantList.LoadFromStream(const Stream: IisStream);
var
  i, n: Integer;
begin
  Lock;
  try
    n := Stream.ReadInteger;
    FList.Clear;
    FList.Capacity := n;
    for i := 0 to n - 1 do
      Add(Stream.ReadVariant);
  finally
    Unlock;
  end;
end;

procedure TisVariantList.Move(const CurIndex, NewIndex: Integer);
begin
  Lock;
  try
    FList.Move(CurIndex, NewIndex);
  finally
    Unlock;
  end;
end;

procedure TisVariantList.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
begin
  inherited;
  LoadFromStream(AStream);
end;

procedure TisVariantList.SaveToFile(const FileName: string);
var
  F: IisStream;
begin
  F := TisStream.CreateFromNewFile(FileName);
  SaveToStream(F);
end;

procedure TisVariantList.SaveToStream(Stream: IisStream);
var
  i: Integer;
begin
  Lock;
  try
    Stream.WriteInteger(GetCount);
    for i := 0 to GetCount - 1 do
      Stream.WriteVariant(GetVariant(i));
  finally
    Unlock;
  end;
end;

procedure TisVariantList.SetCapacity(const NewCapacity: Integer);
begin
  Lock;
  try
    FList.Capacity := NewCapacity;
  finally
    Unlock;
  end;
end;

procedure TisVariantList.SetVariant(Index: Integer; const V: Variant);
begin
  Lock;
  try
    PVariant(FList[Index])^ := V;
  finally
    Unlock;
  end;
end;

procedure TisVariantList.WriteSelfToStream(const AStream: IisStream);
begin
  inherited;
  SaveToStream(AStream);
end;


{ TisValueMap }

procedure TisValueMap.Clear;
var
 i: Integer;
 P: PTisValueMapItem;
begin
  Lock;
  try
    for i := 0 to FList.Count - 1 do
    begin
      P := GetItem(i);
      Dispose(P);
    end;
    FList.Clear;
  finally
    Unlock;
  end;
end;

function TisValueMap.Count: Integer;
begin
  Lock;
  try
    Result := FList.Count;  
  finally
    Unlock;
  end;
end;

function TisValueMap.DecodeValue(const AOrigValue: Variant): Variant;
var
  i: Integer;
begin
  Lock;
  try
    i := FList.IndexOf(VarToStr(AOrigValue));
    if i <> -1 then
      Result := GetItem(i).NewValue
    else
      raise EisException.Create('Cannot decode value ' + VarToStr(AOrigValue));
  finally
    Unlock;
  end;
end;

destructor TisValueMap.Destroy;
begin
  Clear;
  FList.Free;
  inherited;
end;

procedure TisValueMap.DoOnConstruction;
begin
  inherited;
  FList := TStringList.Create;
  FList.Duplicates := dupError;
  FList.Sorted := True;
end;

procedure TisValueMap.EncodeValue(const AOrigValue, ANewValue: Variant);
var
  s: String;
  P: PTisValueMapItem;
begin
  Lock;
  try
    s := VarToStr(AOrigValue);
    if FList.IndexOf(s) = -1 then
    begin
      New(P);
      P.OrigValue := AOrigValue;
      P.NewValue := ANewValue;

      if FList.Count = FList.Capacity then
        FList.Capacity := FList.Capacity + 100;
      FList.AddObject(s, Pointer(P));
    end;
  finally
    Unlock;
  end;
end;

procedure TisValueMap.FormatedRead(const AStorage: IisFormatedReader);
var
  i, n: Integer;
  P: PTisValueMapItem;
begin
  inherited;
  Clear;
  FList.Sorted := False;
  n := AStorage.BeginReadGroup('Items') div 2;
  try
    FList.Capacity := n;
    for i := 0 to n - 1 do
    begin
      New(P);
      P.OrigValue := AStorage.ReadValueByIndex(i * 2);
      P.NewValue := AStorage.ReadValueByIndex(i * 2 + 1);
      FList.AddObject(VarToStr(P.OrigValue), Pointer(P));
    end;
  finally
    AStorage.EndReadGroup;
  end;
  FList.Sorted := True;
end;


procedure TisValueMap.FormatedWrite(const AStorage: IisFormatedWriter);
var
  i: Integer;
begin
  inherited;

  AStorage.BeginWriteGroup('Items');
  try
    for i := 0 to Count - 1 do
    begin
      AStorage.WriteValue('Old' + IntToStr(i), GetItem(i).OrigValue);
      AStorage.WriteValue('New' + IntToStr(i), GetItem(i).NewValue);
    end;
  finally
    AStorage.EndWriteGroup;
  end;
end;

function TisValueMap.GetClone: IisValueMap;
begin
  Result := inherited GetClone as IisValueMap;
end;

function TisValueMap.GetItem(const AIndex: Integer): PTisValueMapItem;
begin
  Result := PTisValueMapItem(FList.Objects[AIndex]);
end;

function TisValueMap.GetValueMap(Index: Integer): TisValueMapItem;
begin
  Result := GetItem(Index)^;
end;

class function TisValueMap.GetTypeID: String;
begin
  Result := 'VM';
end;

procedure TisValueMap.ReadSelfFromStream(const AStream: IisStream;
  const ARevision: TisRevision);
var
  i, n: Integer;
  P: PTisValueMapItem;
begin
  inherited;
  Clear;
  FList.Sorted := False;
  n := AStream.ReadInteger;
  FList.Capacity := n;
  for i := 1 to n do
  begin
    New(P);
    P.OrigValue := AStream.ReadVariant;
    P.NewValue := AStream.ReadVariant;
    FList.AddObject(VarToStr(P.OrigValue), Pointer(P));
  end;
  FList.Sorted := True;
end;

function TisValueMap.TryDecodeValue(const AOrigValue: Variant; out AMapValue: Variant): Boolean;
var
  i: Integer;
begin
  Lock;
  try
    i := FList.IndexOf(VarToStr(AOrigValue));
    if i <> -1 then
    begin
      AMapValue := GetItem(i).NewValue;
      Result := True;
    end
    else
      Result := False;
  finally
    Unlock;
  end;

end;

procedure TisValueMap.WriteSelfToStream(const AStream: IisStream);
var
  i: Integer;
begin
  inherited;
  AStream.WriteInteger(FList.Count);
  for i := 0 to FList.Count - 1 do
  begin
    AStream.WriteVariant(GetItem(i).OrigValue);
    AStream.WriteVariant(GetItem(i).NewValue);
  end;
end;


{ TisInterfaceList }

function TisInterfaceList.Add(const AItem: IInterface): Integer;
begin
  Lock;
  try
    Result := FList.Add(Pointer(AItem as IInterface));
    if Assigned(AItem) then
      AItem._AddRef;
    AddToIndex(Result);
  finally
    Unlock;
  end;
end;

procedure TisInterfaceList.AddToIndex(const AObjIndex: Integer);
var
  i: Integer;
begin
  FindInIndex(Get(AObjIndex), i);
  FIndex.Insert(i, Pointer(AObjIndex));
end;

procedure TisInterfaceList.Clear;
var
  i: Integer;
begin
  Lock;
  try
    for i := 0 to FList.Count - 1 do
      if Assigned(FList[i]) then
        IInterface(FList[i])._Release;

    FList.Clear;
    FIndex.Clear;
  finally
    Unlock;
  end;
end;

procedure TisInterfaceList.Delete(const AIndex: Integer);
var
  i: Integer;
  O: IInterface;
begin
  Lock;
  try
    O := Get(AIndex);
    Assert(FindInIndex(O, i));
    InternalDeleteFromIndex(i);
    FList.Delete(AIndex);
  finally
    Unlock;
  end;

  if Assigned(O) then
    O._Release;
end;

procedure TisInterfaceList.Delete(const AItem: IInterface);
var
  i: Integer;
  O: IInterface;
begin
  Lock;
  try
    if FindInIndex(AItem, i) then
    begin
      O := Get(Integer(FIndex[i]));
      FList.Delete(Integer(FIndex[i]));
      InternalDeleteFromIndex(i);
    end;
  finally
    Unlock;
  end;

  if Assigned(O) then
    O._Release;
end;

function TisInterfaceList.FindInIndex(const AItem: IInterface; var AIndex: Integer): Boolean;
var
  L, H, i, C: Integer;
  O: Integer;
begin
  Result := False;
  O := Integer(Pointer(AItem as IInterface));
  L := 0;
  H := FIndex.Count - 1;
  while L <= H do
  begin
    i := (L + H) shr 1;
    C := CompareValue(Integer(FList[Integer(FIndex[i])]), O);
    if C < 0 then
      L := i + 1
    else
    begin
      H := i - 1;
      if C = 0 then
      begin
        Result := True;
        L := i;
      end;
    end;
  end;
  AIndex := L;
end;

function TisInterfaceList.Get(Index: Integer): IInterface;
begin
  Lock;
  try
    Result := IInterface(FList[Index]);
  finally
    Unlock;
  end;
end;

function TisInterfaceList.GetCapacity: Integer;
begin
  Lock;
  try
    Result := FList.Capacity;
  finally
    Unlock;
  end;
end;

function TisInterfaceList.Count: Integer;
begin
  Lock;
  try
    Result := FList.Count;
  finally
    Unlock;
  end;
end;

function TisInterfaceList.IndexOf(const AItem: IInterface): Integer;
begin
  Lock;
  try
    if FindInIndex(AItem, Result) then
      Result := Integer(FIndex[Result])
    else
      Result := -1;
  finally
    Unlock;
  end;
end;

procedure TisInterfaceList.Insert(const AIndex: Integer; const AItem: IInterface);
begin
  Lock;
  try
    FList.Insert(AIndex, Pointer(AItem as IInterface));
    if Assigned(AItem) then
      AItem._AddRef;
    InternalUpdateIndex(AIndex, 1);
    AddToIndex(AIndex);
  finally
    Unlock;
  end;
end;

procedure TisInterfaceList.Put(Index: Integer; const Item: IInterface);
var
  i: Integer;
  O: IInterface;
begin
  if Item = nil then
    raise EisException.Create('Empty item is not allowed');

  Lock;
  try
    O := Get(Index);
    Assert(FindInIndex(O, i));
    FIndex.Delete(i); // exception of rules! Do not call InternalDeleteFromIndex here
    if Assigned(O) then
      O._Release;

    FList[Index] := Pointer(Item as IInterface);
    if Assigned(Item) then
      Item._AddRef;
    AddToIndex(Index);
  finally
    Unlock;
  end;
end;

procedure TisInterfaceList.SetCapacity(const ANewCapacity: Integer);
begin
  Lock;
  try
    FList.Capacity := ANewCapacity;
    FIndex.Capacity := ANewCapacity;
  finally
    Unlock;
  end;
end;

procedure TisInterfaceList.DoOnConstruction;
begin
  inherited;
  FList := TList.Create;
  FIndex := TList.Create;
end;

procedure TisInterfaceList.DoOnDestruction;
begin
  Clear;
  FreeAndNil(FList);
  FreeAndNil(FIndex);
  inherited;
end;

procedure TisInterfaceList.InternalDeleteFromIndex(const AIndex: Integer);
var
  DelItemInd: Integer;
begin
  // Delete index item and correct all references
  DelItemInd := Integer(FIndex[AIndex]);
  FIndex.Delete(AIndex);
  InternalUpdateIndex(DelItemInd + 1, -1);
end;


function TisInterfaceList.GetClone: IisInterfaceList;
var
  i: Integer;
begin
  Result := TisInterfaceList.Create;

  Lock;
  try
    Result.Capacity := GetCapacity;
    for i := 0 to Count - 1 do
      Result.Add(Get(i));
  finally
    Unlock;
  end;
end;


procedure TisInterfaceList.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
var
  i, n: Integer;
  s: String;
  Chld: IisInterfacedObject;
begin
  inherited;
  n := AStream.ReadInteger;

  FList.Capacity := n;
  FIndex.Capacity := n;
  for i := 1 to n do
  begin
    s := AStream.ReadShortString;
    if s = '' then
      Add(nil)
    else
    begin
      Chld := ObjectFactory.CreateInstanceOf(s);
      CheckCondition(Assigned(Chld), 'Child object is not created. Unknown type ID "' + s + '"');
      Chld.ReadFromStream(AStream);
      Add(Chld);
    end;
  end;
end;

procedure TisInterfaceList.WriteSelfToStream(const AStream: IisStream);
var
  i: Integer;
  Item: IisInterfacedObject;
begin
  inherited;
  AStream.WriteInteger(FList.Count);

  for i := 0 to Count - 1 do
    if Supports(Get(i), IisInterfacedObject) then
    begin
      Item := Get(i) as IisInterfacedObject;
      AStream.WriteShortString(Item.TypeID);
      Item.WriteToStream(AStream);
    end
    else
      AStream.WriteShortString('');
end;

procedure TisInterfaceList.FormatedRead(const AStorage: IisFormatedReader);
var
  i, n: Integer;
  Item: IisInterfacedObject;
begin
  inherited;

  Clear;
  n := AStorage.BeginReadGroup('Items');
  try
    SetCapacity(n);
    for i := 0 to n - 1 do
    begin
      Item := AStorage.ReadObjectByIndex(i);
      Add(Item);
    end;
  finally
    AStorage.EndReadGroup;
  end;
end;

procedure TisInterfaceList.FormatedWrite(const AStorage: IisFormatedWriter);
var
  i: Integer;
  Item: IisInterfacedObject;
begin
  inherited;

  AStorage.BeginWriteGroup('Items');
  try
    for i := 0 to Count - 1 do
    begin
      if Supports(Get(i), IisInterfacedObject) then
        Item := Get(i) as IisInterfacedObject
      else
        Item := nil;
      AStorage.WriteObject('I' + IntToStr(i), Item);
    end;
  finally
    AStorage.EndWriteGroup;
  end;
end;

class function TisInterfaceList.GetTypeID: String;
begin
  Result := 'IL';
end;


procedure TisInterfaceList.Move(const ACurIndex, ANewIndex: Integer);
var
  i: Integer;
  O: IInterface;
begin
  Lock;
  try
    O := Get(ACurIndex);
    Assert(FindInIndex(O, i));
    InternalDeleteFromIndex(i);
    FList.Delete(ACurIndex);

    FList.Insert(ANewIndex, Pointer(O));
    InternalUpdateIndex(ANewIndex, 1);
    AddToIndex(ANewIndex);
  finally
    Unlock;
  end;
end;

procedure TisInterfaceList.InternalUpdateIndex(const AStartIndex, ACorrection: Integer);
var
  i: Integer;
begin
  for i := 0 to FIndex.Count - 1 do
    if Integer(FIndex[i]) >= AStartIndex then
      FIndex[i] := Pointer(Integer(FIndex[i]) + ACorrection);
end;

function TisInterfaceList.GetList: IisList;
var
  i: Integer;
begin
  Result := TisList.Create;
  Result.Capacity := Count;
  Lock;
  try
    for i := 0 to Count - 1 do
      Result.Add(Get(i));
  finally
    Unlock;
  end;
end;

{ TisPointerList }

function TisPointerList.Add(const aItem: Pointer): Integer;
begin
  Lock;
  try
    Result := FList.Add(aItem);
  finally
    Unlock;
  end;
end;

procedure TisPointerList.Clear;
begin
  Lock;
  try
    FList.Clear;
  finally
    Unlock;
  end;
end;

procedure TisPointerList.Delete(const aIndex: Integer);
begin
  Lock;
  try
    FList.Delete(aIndex);
  finally
    Unlock;
  end;
end;

procedure TisPointerList.DoOnConstruction;
begin
  inherited;
  FList := TList.Create;
end;

procedure TisPointerList.DoOnDestruction;
begin
  FreeandNil(FList);
  inherited;
end;

procedure TisPointerList.Exchange(const aIndex1, aIndex2: Integer);
begin
  Lock;
  try
    FList.Exchange(aIndex1, aIndex2);
  finally
    Unlock;
  end;
end;

function TisPointerList.GetCapacity: Integer;
begin
  Lock;
  try
    Result := FList.Capacity;
  finally
    Unlock;
  end;
end;

function TisPointerList.GetCount: Integer;
begin
  Lock;
  try
    Result := FList.Count;
  finally
    Unlock;
  end;
end;

function TisPointerList.GetItem(Index: Integer): Pointer;
begin
  Lock;
  try
    Result := FList[Index];
  finally
    Unlock;
  end;
end;

class function TisPointerList.GetTypeID: String;
begin
  Result := 'PL';
end;

function TisPointerList.IndexOf(const aItem: Pointer): Integer;
begin
  Lock;
  try
    Result := FList.IndexOf(aItem);
  finally
    Unlock;
  end;
end;

procedure TisPointerList.Insert(const aIndex: Integer; const aItem: Pointer);
begin
  Lock;
  try
    FList.Insert(aIndex, aItem);
  finally
    Unlock;
  end;
end;

procedure TisPointerList.Move(const aCurIndex, aNewIndex: Integer);
begin
  Lock;
  try
    FList.Move(aCurIndex, aNewIndex);
  finally
    Unlock;
  end;
end;

procedure TisPointerList.Pack;
begin
  Lock;
  try
    FList.Pack;
  finally
    Unlock;
  end;
end;

procedure TisPointerList.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
var
  i: Integer;
begin
  inherited;
  Clear;
  SetCount(AStream.ReadInteger);
  for i := 0 to GetCount - 1 do
    SetItem(i, Pointer(AStream.ReadInteger));
end;

function TisPointerList.Remove(const aItem: Pointer): Integer;
begin
  Lock;
  try
    Result := FList.Remove(aItem);
  finally
    Unlock;
  end;
end;

procedure TisPointerList.SetCapacity(const aNewCapacity: Integer);
begin
  Lock;
  try
    FList.Capacity := aNewCapacity;
  finally
    Unlock;
  end;
end;

procedure TisPointerList.SetCount(const aNewCount: Integer);
begin
  Lock;
  try
    FList.Count := aNewCount;
  finally
    Unlock;
  end;
end;

procedure TisPointerList.SetItem(Index: Integer; const aItem: Pointer);
begin
  Lock;
  try
    FList[Index] := aItem;
  finally
    Unlock;
  end;
end;

procedure TisPointerList.WriteSelfToStream(const AStream: IisStream);
var
  i: Integer;
begin
  inherited;
  AStream.WriteInteger(GetCount);
  for i := 0 to GetCount - 1 do
    AStream.WriteInteger(Integer(GetItem(i)));
end;


{ TisNamedMutex }

constructor TisNamedMutex.Create(const AName: String; const AGlobal: Boolean);

  procedure Initialize;
  var
    s: String;
    secAttr: SECURITY_ATTRIBUTES;
    secDesc: array [1..SECURITY_DESCRIPTOR_MIN_LENGTH] of Char;
  begin
    if AGlobal then
      s := 'Global\' + FName   // Global for entire machine
    else
      s := 'Local\' + FName;   // Local for user session

    // Remove any security restriction
    secAttr.nLength := SizeOf(secAttr);
    secAttr.bInheritHandle := False;
    secAttr.lpSecurityDescriptor := @secDesc;
    InitializeSecurityDescriptor(secAttr.lpSecurityDescriptor, SECURITY_DESCRIPTOR_REVISION);
    SetSecurityDescriptorDacl(secAttr.lpSecurityDescriptor, True, nil, False);

    FHandle := CreateMutex(@secAttr, False, PAnsiChar(s));
    FError := GetLastError;
  end;

begin
  FName := AName;
  Initialize;
end;

destructor TisNamedMutex.Destroy;
begin
  if FHandle <> 0 then
    CloseHandle(FHandle);

  inherited;
end;


constructor TisNamedMutex.Open(const AName: String; const AGlobal: Boolean; const Reserved: Boolean = False);

  procedure Initialize;
  var
    s: String;
  begin
    if AGlobal then
      s := 'Global\' + FName   // Global for entire machine
    else
      s := 'Local\' + FName;   // Local for user session

    FHandle := OpenMutex(MUTEX_ALL_ACCESS, False, PAnsiChar(s));
    FError := GetLastError;
  end;

begin
  FName := AName;
  Initialize;
end;

{ TisAppMainboard }

function TisAppMainboard.CreateInstanceOf(const IID: TGUID; const ANoError: Boolean): IInterface;
begin
  if ANoError then
    if not IsRegisteredInterface(IID) then
    begin
      Result := nil;
      Exit;
    end;

  ObjectFactory.CreateInstanceOf(IID, Result);
end;

procedure TisAppMainboard.Deinitialize;
begin
end;

class function TisAppMainboard.GetTypeID: String;
begin
  Result := 'AppMainboard';
end;

procedure TisAppMainboard.Initialize;
begin
end;

function TisAppMainboard.IsRegisteredInterface(const IID: TGUID): Boolean;
begin
  Result := ObjectFactory.IsRegistered(IID);
end;

procedure TisAppMainboard.RegisterInterface(const IID: TGUID; const AImplementation: TisInterfacedObjectClass);
begin
  ObjectFactory.Register(IID, AImplementation);
end;

procedure TisAppMainboard.UnregisterInterface(const IID: TGUID);
begin
  ObjectFactory.UnRegister(IID);
end;

{ TIntList }

function TIntList.Add(Item: Integer): Integer;
begin
  Result := FCount;
  if Result = FCapacity then
    Grow;
  FList^[Result] := Item;
  Inc(FCount);
end;

procedure TIntList.Assign(ListA: TIntList; AOperator: TListAssignOp; ListB: TIntList);
var
  I: Integer;
  LTemp, LSource: TIntList;
begin
  // ListB given?
  if ListB <> nil then
  begin
    LSource := ListB;
    Assign(ListA);
  end
  else
    LSource := ListA;

  // on with the show
  case AOperator of

    // 12345, 346 = 346 : only those in the new list
    laCopy:
      begin
        Clear;
        Capacity := LSource.Capacity;
        for I := 0 to LSource.Count - 1 do
          Add(LSource[I]);
      end;

    // 12345, 346 = 34 : intersection of the two lists
    laAnd:
      for I := Count - 1 downto 0 do
        if LSource.IndexOf(Items[I]) = -1 then
          Delete(I);

    // 12345, 346 = 123456 : union of the two lists
    laOr:
      for I := 0 to LSource.Count - 1 do
        if IndexOf(LSource[I]) = -1 then
          Add(LSource[I]);

    // 12345, 346 = 1256 : only those not in both lists
    laXor:
      begin
        LTemp := TIntList.Create; // Temp holder of 4 byte values
        try
          LTemp.Capacity := LSource.Count;
          for I := 0 to LSource.Count - 1 do
            if IndexOf(LSource[I]) = -1 then
              LTemp.Add(LSource[I]);
          for I := Count - 1 downto 0 do
            if LSource.IndexOf(Items[I]) <> -1 then
              Delete(I);
          I := Count + LTemp.Count;
          if Capacity < I then
            Capacity := I;
          for I := 0 to LTemp.Count - 1 do
            Add(LTemp[I]);
        finally
          LTemp.Free;
        end;
      end;

    // 12345, 346 = 125 : only those unique to source
    laSrcUnique:
      for I := Count - 1 downto 0 do
        if LSource.IndexOf(Items[I]) <> -1 then
          Delete(I);

    // 12345, 346 = 6 : only those unique to dest
    laDestUnique:
      begin
        LTemp := TIntList.Create;
        try
          LTemp.Capacity := LSource.Count;
          for I := LSource.Count - 1 downto 0 do
            if IndexOf(LSource[I]) = -1 then
              LTemp.Add(LSource[I]);
          Assign(LTemp);
        finally
          LTemp.Free;
        end;
      end;
  end;
end;

procedure TIntList.Clear;
begin
  SetCount(0);
  SetCapacity(0);
end;

procedure TIntList.Delete(Index: Integer);
begin
  if (Index < 0) or (Index >= FCount) then
    raise EListError.CreateFmt(SListIndexError, [Index]);
  Dec(FCount);
  if Index < FCount then
    System.Move(FList^[Index + 1], FList^[Index],
      (FCount - Index) * SizeOf(Pointer));
end;

destructor TIntList.Destroy;
begin
  Clear;
  inherited;
end;

procedure TIntList.Exchange(Index1, Index2: Integer);
var
  Item: Integer;
begin
  if (Index1 < 0) or (Index1 >= FCount) then
    raise EListError.CreateFmt(SListIndexError, [Index1]);
  if (Index2 < 0) or (Index2 >= FCount) then
    raise EListError.CreateFmt(SListIndexError, [Index2]);
  Item := FList^[Index1];
  FList^[Index1] := FList^[Index2];
  FList^[Index2] := Item;
end;

function TIntList.Expand: TIntList;
begin
  if FCount = FCapacity then
    Grow;
  Result := Self;
end;

function TIntList.Extract(Item: Integer): Integer;
var
  I: Integer;
begin
  Result := 0;
  I := IndexOf(Item);
  if I >= 0 then
  begin
    Result := Item;
    FList^[I] := 0;
    Delete(I);
  end;
end;

function TIntList.Get(Index: Integer): Integer;
begin
  if (Index < 0) or (Index >= FCount) then
    raise EListError.CreateFmt(SListIndexError, [Index]);
  Result := FList^[Index];
end;

procedure TIntList.Grow;
var
  Delta: Integer;
begin
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else
    if FCapacity > 8 then
      Delta := 16
    else
      Delta := 4;
  SetCapacity(FCapacity + Delta);
end;

function TIntList.IndexOf(Item: Integer): Integer;
begin
  Result := 0;
  while (Result < FCount) and (FList^[Result] <> Item) do
    Inc(Result);
  if Result = FCount then
    Result := -1;
end;

procedure TIntList.Insert(Index, Item: Integer);
begin
  if (Index < 0) or (Index > FCount) then
    raise EListError.CreateFmt(SListIndexError, [Index]);
  if FCount = FCapacity then
    Grow;
  if Index < FCount then
    System.Move(FList^[Index], FList^[Index + 1],
      (FCount - Index) * SizeOf(Integer));
  FList^[Index] := Item;
  Inc(FCount);
end;

procedure TIntList.Move(CurIndex, NewIndex: Integer);
var
  Item: Integer;
begin
  if CurIndex <> NewIndex then
  begin
    if (NewIndex < 0) or (NewIndex >= FCount) then
      raise EListError.CreateFmt(SListIndexError, [NewIndex]);
    Item := Get(CurIndex);
    FList^[CurIndex] := 0;
    Delete(CurIndex);
    Insert(NewIndex, 0);
    FList^[NewIndex] := Item;
  end;
end;

procedure TIntList.Put(Index, Item: Integer);
begin
  if (Index < 0) or (Index >= FCount) then
    raise EListError.CreateFmt(SListIndexError, [Index]);
  if Item <> FList^[Index] then
    FList^[Index] := Item;
end;

procedure TIntList.QuickSort(L, R: Integer);
var
  I, J, P: Integer;
begin
  repeat
    I := L;
    J := R;
    P := (L + R) shr 1;
    repeat
      while Items[I] < Items[P] do Inc(I);
      while Items[J] > Items[P] do Dec(J);
      if I <= J then
      begin
        Exchange(I, J);
        if P = I then
          P := J
        else if P = J then
          P := I;
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then QuickSort(L, J);
    L := I;
  until I >= R;
end;

function TIntList.Remove(Item: Integer): Integer;
begin
  Result := IndexOf(Item);
  if Result >= 0 then
    Delete(Result);
end;

procedure TIntList.SetCapacity(NewCapacity: Integer);
begin
  if (NewCapacity < FCount) or (NewCapacity > MaxListSize) then
    raise EListError.CreateFmt(SListIndexError, [NewCapacity]);
  if NewCapacity <> FCapacity then
  begin
    ReallocMem(FList, NewCapacity * SizeOf(Integer));
    FCapacity := NewCapacity;
  end;
end;

procedure TIntList.SetCount(NewCount: Integer);
var
  I: Integer;
begin
  if (NewCount < 0) or (NewCount > MaxListSize) then
    raise EListError.CreateFmt(SListIndexError, [NewCount]);
  if NewCount > FCapacity then
    SetCapacity(NewCount);
  if NewCount > FCount then
    FillChar(FList^[FCount], (NewCount - FCount) * SizeOf(Integer), 0)
  else
    for I := FCount - 1 downto NewCount do
      Delete(I);
  FCount := NewCount;
end;

procedure TIntList.Sort;
begin
  if (FList <> nil) and (Count > 0) then
    QuickSort(0, Count - 1);
end;

{ TisIntegerList }

function TisIntegerList.Add(const aItem: Integer): Integer;
begin
  Lock;
  try
    Result := FList.Add(aItem);
  finally
    Unlock;
  end;
end;

procedure TisIntegerList.Clear;
begin
  Lock;
  try
    FList.Clear;
  finally
    Unlock;
  end;
end;

procedure TisIntegerList.Delete(const aIndex: Integer);
begin
  Lock;
  try
    FList.Delete(aIndex);
  finally
    Unlock;
  end;
end;

procedure TisIntegerList.DoOnConstruction;
begin
  inherited;
  FList := TIntList.Create;
end;

procedure TisIntegerList.DoOnDestruction;
begin
  FreeandNil(FList);
  inherited;
end;

procedure TisIntegerList.Exchange(const aIndex1, aIndex2: Integer);
begin
  Lock;
  try
    FList.Exchange(aIndex1, aIndex2);
  finally
    Unlock;
  end;
end;

function TisIntegerList.GetCapacity: Integer;
begin
  Lock;
  try
    Result := FList.Capacity;
  finally
    Unlock;
  end;
end;

function TisIntegerList.GetCommaText: string;
var
  i: Integer;
begin
  Result := '';
  Lock;
  try
    for i := 0 to GetCount - 1 do
      AddStrValue(Result, IntToStr(GetItem(i)), ',');
  finally
    Unlock;
  end;
end;

function TisIntegerList.GetCount: Integer;
begin
  Lock;
  try
    Result := FList.Count;
  finally
    Unlock;
  end;
end;

function TisIntegerList.GetItem(Index: Integer): Integer;
begin
  Lock;
  try
    Result := FList.Items[Index];
  finally
    Unlock;
  end;
end;

class function TisIntegerList.GetTypeID: String;
begin
  Result := 'NL';
end;

function TisIntegerList.IndexOf(const aItem: Integer): Integer;
begin
  Lock;
  try
    Result := FList.IndexOf(aItem);
  finally
    Unlock;
  end;
end;

procedure TisIntegerList.Insert(const aIndex, aItem: Integer);
begin
  Lock;
  try
    FList.Insert(aItem, aItem);
  finally
    Unlock;
  end;
end;

procedure TisIntegerList.Move(const aCurIndex, aNewIndex: Integer);
begin
  Lock;
  try
    FList.Move(aCurIndex, aNewIndex);
  finally
    Unlock;
  end;
end;

procedure TisIntegerList.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
var
  i: Integer;
begin
  inherited;
  Clear;
  SetCount(AStream.ReadInteger);
  for i := 0 to GetCount - 1 do
    SetItem(i, AStream.ReadInteger);
end;

function TisIntegerList.Remove(const aItem: Integer): Integer;
begin
  Lock;
  try
    Result := FList.Remove(aItem);
  finally
    Unlock;
  end;
end;

procedure TisIntegerList.SetCapacity(const aNewCapacity: Integer);
begin
  Lock;
  try
    FList.SetCapacity(aNewCapacity);
  finally
    Unlock;
  end;
end;

procedure TisIntegerList.SetCommaText(const AValue: string);
var
  s: String;
begin
  s := AValue;
  Lock;
  try
    while AValue <> '' do
    begin
      if GetCount = GetCapacity then
        SetCapacity(GetCount + 100);
      Add(StrToInt(GetNextStrValue(s, ',')));
    end;
    SetCapacity(GetCount);
  finally
    Unlock;
  end;
end;

procedure TisIntegerList.SetCount(const aNewCount: Integer);
begin
  Lock;
  try
    FList.SetCount(aNewCount);
  finally
    Unlock;
  end;
end;

procedure TisIntegerList.SetItem(Index: Integer; const aItem: Integer);
begin
  Lock;
  try
    FList.Items[Index] := aItem;
  finally
    Unlock;
  end;
end;

procedure TisIntegerList.WriteSelfToStream(const AStream: IisStream);
var
  i: Integer;
begin
  inherited;
  AStream.WriteInteger(GetCount);
  for i := 0 to GetCount - 1 do
    AStream.WriteInteger(GetItem(i));
end;

initialization
  ObjectFactory.Register([TisInterfacedObject, TisNamedObject, TisCollection,
                          TisStringList, TisNamedValue, TisListOfValues, TisParamsCollection,
                          TisParamsCollectionItem, TisVariantList, TisValueMap, TisInterfaceList,
                          TisPointerList, TisIntegerList]);
finalization
  FObjectFactory := nil;
  FFinalized := True;

end.

