unit isTestQueueManager;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ToolWin, ComCtrls, ExtCtrls, IsTestUtilsScriptQueue,
  ImgList, ISBasicClasses, Menus, StdActns;

type
  TisTestQueueManagerFrame = class(TFrame, IisTestScriptQueueEventSink)
    Panel1: TPanel;
    ToolBar1: TToolBar;
    ActionList1: TActionList;
    FormState: TAction;
    lvQueue: TListView;
    ImageList1: TImageList;
    RunTests: TAction;
    moveup: TAction;
    movedown: TAction;
    Remove: TAction;
    ToolButton1: TToolButton;
    MainMenu1: TMainMenu;
    Queue1: TMenuItem;
    Action1: TMenuItem;
    Edit1: TMenuItem;
    FileExit1: TAction;
    RunTests1: TMenuItem;
    Exit1: TMenuItem;
    N1: TMenuItem;
    fileSave: TAction;
    FileSaveAs1: TFileSaveAs;
    FileOpen1: TFileOpen;
    fileNew: TAction;
    New1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    SaveAs1: TMenuItem;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    Clear: TAction;
    Remove1: TMenuItem;
    PopupMenu1: TPopupMenu;
    Remove2: TMenuItem;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    Clear1: TMenuItem;
    N2: TMenuItem;
    MoveUp1: TMenuItem;
    MoveDown1: TMenuItem;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    FileOpen2: TFileOpen;
    ToolButton11: TToolButton;
    Add1: TMenuItem;
    SwitchToEditor: TAction;
    Openineditor1: TMenuItem;
    Openineditor2: TMenuItem;
    evStatusBar: TStatusBar;
    procedure FormStateExecute(Sender: TObject);
    procedure FormStateUpdate(Sender: TObject);
    procedure RunTestsUpdate(Sender: TObject);
    procedure RunTestsExecute(Sender: TObject);
    procedure FileExit1Execute(Sender: TObject);
    procedure fileSaveExecute(Sender: TObject);
    procedure fileSaveUpdate(Sender: TObject);
    procedure FileSaveAs1Accept(Sender: TObject);
    procedure FileOpen1Accept(Sender: TObject);
    procedure fileNewExecute(Sender: TObject);
    procedure ClearUpdate(Sender: TObject);
    procedure ClearExecute(Sender: TObject);
    procedure FileOpen2Accept(Sender: TObject);
    procedure RemoveUpdate(Sender: TObject);
    procedure RemoveExecute(Sender: TObject);
    procedure moveupUpdate(Sender: TObject);
    procedure moveupExecute(Sender: TObject);
    procedure movedownExecute(Sender: TObject);
    procedure movedownUpdate(Sender: TObject);
    procedure SwitchToEditorUpdate(Sender: TObject);
    procedure SwitchToEditorExecute(Sender: TObject);
  private
    {IisTestScriptQueueEventSink}
    procedure QueueChanged;
    procedure QueueStatusChanged;
  protected
    procedure Loaded; override;  
  public
    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;
    function CloseQuery: boolean;
  end;

implementation

uses
  istestutils;
{$R *.dfm}

constructor TisTestQueueManagerFrame.Create(Owner: TComponent);
begin
  inherited;
  TestScriptQueue.Advise(Self);
end;

destructor TisTestQueueManagerFrame.Destroy;
begin
  TestScriptQueue.UnAdvise(Self);
  inherited;
end;

procedure TisTestQueueManagerFrame.FormStateExecute(Sender: TObject);
begin
//intentionally left blank
end;

procedure TisTestQueueManagerFrame.FormStateUpdate(Sender: TObject);
var
  s: string;
begin
  s := TestScriptQueue.filename;
  if s = '' then
    s := '[Unnamed]';
  (Sender as TCustomAction).Caption := s + ' - Test Script Queue (' + inttostr(TestScriptQueue.Count)+')';
end;

procedure TisTestQueueManagerFrame.Loaded;
begin
  inherited;
  QueueChanged;
end;

procedure TisTestQueueManagerFrame.QueueChanged;
var
  i: integer;
  ii: integer;
begin
  if assigned(lvQueue.Selected) then
    ii := lvQueue.Selected.Index
  else
    ii := -1;
  lvQueue.Items.BeginUpdate;
  try
    lvQueue.Items.Clear;
    for i := 0 to TestScriptQueue.Count -1 do
      with lvQueue.Items.Add do
      begin
        Data := pointer( i );
        Caption := TestScriptQueue.ScriptFilename[i];
        Subitems.Add( '' );
        Subitems.Add( '' );
        Subitems.Add( '' );
      end;
    if ii > lvQueue.Items.Count-1 then
      ii := lvQueue.Items.Count-1;
    if (ii < 0) and (lvQueue.Items.Count > 0) then
      ii := 0;
    if ii = -1 then
      lvQueue.Selected := nil
    else
      lvQueue.Selected := lvQueue.Items[ii];
    QueueStatusChanged;
  finally
    lvQueue.Items.endUpdate;
  end;
end;

procedure TisTestQueueManagerFrame.QueueStatusChanged;
var
  missingfile, i: integer;
begin
  lvQueue.Items.BeginUpdate;
  missingfile := 0;
  try
    evStatusBar.Panels[0].Text := ' Total File : ' + IntToStr(TestScriptQueue.Count);
    for i := 0 to TestScriptQueue.Count -1 do
    begin
      lvQueue.Items[i].Subitems[0] := TestScriptQueue.ScriptComments[i];
      if Trim(TestScriptQueue.ScriptComments[i]) = '' then
      begin
       lvQueue.Items[i].Subitems[2] := 'Missing File';
       inc(missingfile);
      end;

      lvQueue.Items[i].Subitems[1] := StrFromScriptStatus(TestScriptQueue.ScriptStatus[i]);
      case TestScriptQueue.ScriptStatus[i] of
        tsesWaiting:   lvQueue.Items[i].ImageIndex := 0;
        tsesRunning:   lvQueue.Items[i].ImageIndex := 3;
        tsesCompleted: lvQueue.Items[i].ImageIndex := 1;
        tsesFailed:    lvQueue.Items[i].ImageIndex := 2;
      else
        lvQueue.Items[i].ImageIndex := -1;
      end;
    end
  finally
    evStatusBar.Panels[1].Text := ' Missing File : ' + IntToStr(missingfile);
    lvQueue.Items.endUpdate;
  end;
end;

procedure TisTestQueueManagerFrame.RunTestsUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := TestScriptQueue.Count > 0;
end;

procedure TisTestQueueManagerFrame.RunTestsExecute(Sender: TObject);
begin
  TestScriptQueue.RunTests(0);
end;

procedure TisTestQueueManagerFrame.FileExit1Execute(Sender: TObject);
begin
  //!! ugly
  if Owner is TCustomForm then
    (Owner as TCustomForm).Close;
end;

function TisTestQueueManagerFrame.CloseQuery: boolean;
var
  res: integer;
begin
  Result := true;
  if TestScriptQueue.IsModified then
  begin
    res := MessageDlg('Save changes in the script queue?',mtConfirmation,mbYesNoCancel,0);
    case res of
      mrYes: fileSave.Execute;
      mrNo: TestScriptQueue.MarkAsUnmodififed;
      mrCancel: Result := false;
    end;
  end;
end;

procedure TisTestQueueManagerFrame.fileSaveExecute(Sender: TObject);
begin
  if TestScriptQueue.Filename <> '' then
    TestScriptQueue.Save
  else
    fileSaveAs1.Execute;
end;

procedure TisTestQueueManagerFrame.fileSaveUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := TestScriptQueue.IsModified;
end;

procedure TisTestQueueManagerFrame.FileSaveAs1Accept(Sender: TObject);
begin
  TestScriptQueue.SaveAs( (Sender as TFileSaveAs).Dialog.FileName );
end;

procedure TisTestQueueManagerFrame.FileOpen1Accept(Sender: TObject);
begin
  if CloseQuery then
    TestScriptQueue.Open( (Sender as TFileOpen).Dialog.Filename );
end;

procedure TisTestQueueManagerFrame.fileNewExecute(Sender: TObject);
begin
  if CloseQuery then
    TestScriptQueue.New;
end;

procedure TisTestQueueManagerFrame.ClearUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := TestScriptQueue.Count > 0;
end;

procedure TisTestQueueManagerFrame.ClearExecute(Sender: TObject);
begin
  TestScriptQueue.DeleteAll;
end;

procedure TisTestQueueManagerFrame.FileOpen2Accept(Sender: TObject);
begin
  TestScriptQueue.AddScript( (Sender as TFileOpen).Dialog.Filename );
end;

procedure TisTestQueueManagerFrame.RemoveUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(lvQueue.Selected);
end;

procedure TisTestQueueManagerFrame.RemoveExecute(Sender: TObject);
begin
  TestScriptQueue.Delete( lvQueue.Selected.Index );
end;

procedure TisTestQueueManagerFrame.moveupUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(lvQueue.Selected) and ( lvQueue.Selected.Index > 0 );
end;

procedure TisTestQueueManagerFrame.moveupExecute(Sender: TObject);
begin
  lvQueue.Selected := lvQueue.Items[lvQueue.Selected.Index-1];
  TestScriptQueue.MoveUp( lvQueue.Selected.Index );
end;

procedure TisTestQueueManagerFrame.movedownExecute(Sender: TObject);
begin
  lvQueue.Selected := lvQueue.Items[lvQueue.Selected.Index+1];
  TestScriptQueue.MoveDown( lvQueue.Selected.Index );
end;

procedure TisTestQueueManagerFrame.movedownUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(lvQueue.Selected) and ( lvQueue.Selected.Index < TestScriptQueue.Count-1) ;
end;

procedure TisTestQueueManagerFrame.SwitchToEditorUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(lvQueue.Selected);
end;

procedure TisTestQueueManagerFrame.SwitchToEditorExecute(Sender: TObject);
begin
  LaunchEditor( TestScriptQueue.ScriptFilename[lvQueue.Selected.Index] );
end;

end.


