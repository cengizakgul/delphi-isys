// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SShowColumnsInGrid;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, CheckLst, ISBasicClasses;

type
  TShowColumnsInGrid = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    cbColumns: TCheckListBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
