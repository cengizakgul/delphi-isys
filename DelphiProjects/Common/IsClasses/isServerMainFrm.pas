unit isServerMainFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  ExtCtrls, isBasicUtils, isExceptions, isTypes, isBaseClasses;

type
  TisServerMainForm = class(TForm)
    btnShowStack: TButton;
    Timer: TTimer;
    gbMemStatus: TGroupBox;
    Label1: TLabel;
    lAllocatedVAS: TLabel;
    Label3: TLabel;
    lMaxAllocatedVAS: TLabel;
    Label2: TLabel;
    lIntObjCount: TLabel;
    procedure btnShowStackClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure gbMemStatusClick(Sender: TObject);
  private
    FAllocatedVAS: Cardinal;
    FMaxAllocatedVAS: Cardinal;
    procedure CmBringAppToFront(var Message: TMessage); message CM_BringAppToFront;
    procedure UpdateMemStatus;
  public
  end;

implementation

{$R *.dfm}

procedure TisServerMainForm.btnShowStackClick(Sender: TObject);
begin
  raise EisDumpAllThreads.Create('Stack snapshot has been taken');
end;

procedure TisServerMainForm.FormCreate(Sender: TObject);
begin
  Caption := Application.Title;
end;

procedure TisServerMainForm.TimerTimer(Sender: TObject);
begin
  UpdateMemStatus;
end;

procedure TisServerMainForm.gbMemStatusClick(Sender: TObject);
begin
  UpdateMemStatus;
end;

procedure TisServerMainForm.UpdateMemStatus;
var
  MemoryStatus: TMemoryStatus;
begin
  MemoryStatus := GetMemStatus;
  FAllocatedVAS := MemoryStatus.dwTotalVirtual - MemoryStatus.dwAvailVirtual;

  if FMaxAllocatedVAS < FAllocatedVAS then
    FMaxAllocatedVAS := FAllocatedVAS;

  lAllocatedVAS.Caption := SizeToString(FAllocatedVAS);
  lMaxAllocatedVAS.Caption := SizeToString(FMaxAllocatedVAS);

{$IFDEF WATCH_OBJECTS}
  lIntObjCount.Caption := IntToStr(ObjectsList.LockList.Count);
  ObjectsList.UnlockList;
{$ENDIF}  
end;

procedure TisServerMainForm.CmBringAppToFront(var Message: TMessage);
begin
  Application.Minimize;
  Application.Restore;
  Application.BringToFront;
end;

end.
