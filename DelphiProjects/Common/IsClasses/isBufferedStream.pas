unit isBufferedStream;

interface

uses
	classes{, windows};

type
  TBufferState = (bsRead, bsWrite);

{$IFDEF INHERIT_FROM_FILE_STREAM}
	TBufferedStream = class(TFileStream)
{$ELSE}
	TBufferedStream = class(THandleStream)
{$ENDIF}
	protected
	  FBuffer: PChar;
    FState: TBufferState;
    FBytesUsed: Longint;
    FVirtualPosition: Integer; //needed only when FState = bsRead

    procedure CreateBuffer;
    procedure FillBuffer;
    function WriteToBuffer(data: PChar; nBytes: integer): Integer; //cannot fail
    function ReadFromBuffer(data: PChar; nBytes: integer): Integer; //cannot fail
	public
//    procedure CheckInvariant;
    function FlushBuffer: boolean;

    constructor Create(AHandle: Integer); overload;

{$IFDEF INHERIT_FROM_FILE_STREAM}
    constructor Create(const FileName: string; Mode: Word); overload;
    constructor Create(const FileName: string; Mode: Word; Rights: Cardinal); overload;
{$ENDIF}

    function Read(var Buffer; Count: Longint): Longint; override;
    function Write(const Buffer; Count: Longint): Longint; override;
    function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;

    destructor Destroy; override;
    procedure  BeforeDestruction; override;
	end;

const
  INVALID_SET_FILE_POINTER = -1; //defined in winbase.h as DWORD(-1), missing in Delphi

const
  BufferSize = 4096;//8192;

implementation

uses
  sysutils{, math};

{ TBufferedFileStream }
{
procedure TBufferedFileStream.CheckInvariant;
begin
  Assert( FBuffer <> nil );
  Assert( FBytesUsed >= 0 );
  Assert( FBytesUsed <= BufferSize );
  case FState of
    bsRead:
      begin
        Assert(FVirtualPosition >= 0);
        Assert(FVirtualPosition <= FBytesUsed);
      end;
    bsWrite:
      begin
        Assert(FVirtualPosition = FBytesUsed);
      end;
    else
      Assert(false);
  end
end;
}

constructor TBufferedStream.Create(AHandle: Integer);
begin
  CreateBuffer;
  inherited;
end;

{$IFDEF INHERIT_FROM_FILE_STREAM}
constructor TBufferedStream.Create(const FileName: string; Mode: Word);
begin
  CreateBuffer;
  inherited;
end;

constructor TBufferedStream.Create(const FileName: string; Mode: Word; Rights: Cardinal);
begin
  CreateBuffer;
  inherited;
end;
{$ENDIF}

procedure TBufferedStream.CreateBuffer;
begin
  FBytesUsed := 0;
  FState := bsWrite;
  FVirtualPosition := 0;
  GetMem(FBuffer, BufferSize);
  //CheckInvariant;
end;

destructor TBufferedStream.Destroy;
begin
  inherited;
  if assigned(FBuffer) then
    FreeMem(FBuffer);
end;

function TBufferedStream.FlushBuffer: boolean;
var
  nBytesWritten: Longint;
begin
  //CheckInvariant;
  try
    Result := true;
    case FState of
      bsRead:
        begin
          if FVirtualPosition < FBytesUsed then
            Result := FileSeek(FHandle, FVirtualPosition-FBytesUsed, 1) <> INVALID_SET_FILE_POINTER;
          if Result then
          begin
            FBytesUsed := 0;
            FVirtualPosition := 0;
          end
        end;
      bsWrite:
        begin
          if FBytesUsed > 0 then
          begin
            nBytesWritten := inherited Write(FBuffer^, FBytesUsed);
            Result := nBytesWritten = FBytesUsed;
            Move((FBuffer + nBytesWritten)^, FBuffer^, FBytesUsed - nBytesWritten);
            FBytesUsed := FBytesUsed - nBytesWritten;
            FVirtualPosition := FBytesUsed;
          end
        end;
      else
      begin
        Assert(false);
        Result := false; //to make compiler happy
      end
    end
  finally
    //CheckInvariant;
  end
end;

function TBufferedStream.Read(var Buffer; Count: Integer): Longint;
var
  nBytesRemaining: integer;
begin
  //CheckInvariant;
  try
    if FState = bsWrite then
      if not FlushBuffer then
      begin
        Result := 0;
        Exit;
      end
      else
        FState := bsRead;
    Result := ReadFromBuffer(@Buffer, Count);
    nBytesRemaining := Count - Result;
    if nBytesRemaining > 0 then
    begin
      if FlushBuffer then //should always succeed
      begin
        if nBytesRemaining > BufferSize then
          Result := Result + inherited Read((PChar(@Buffer) + Result)^, nBytesRemaining)
        else
        begin
          FillBuffer;
          Result := Result + ReadFromBuffer(PChar(@Buffer) + Result, nBytesRemaining);
        end
      end
    end
  finally
    //CheckInvariant;
  end;
end;


function TBufferedStream.ReadFromBuffer(data: PChar;
  nBytes: integer): Integer;
begin
  //CheckInvariant;
  try
    if FBytesUsed - FVirtualPosition < nBytes then
      Result := FBytesUsed - FVirtualPosition
    else
      Result := nBytes;
    Move( (FBuffer + FVirtualPosition)^, data^, Result );
    FVirtualPosition := FVirtualPosition + Result;
  finally
    //CheckInvariant;
  end;
end;

procedure TBufferedStream.FillBuffer;
begin
  //CheckInvariant;
  try
    Assert( FBytesUsed = 0 ); //new stream or FlushBuffer was called
    FBytesUsed := inherited Read(FBuffer^, BufferSize);
    //FVirtualPosition is already zero
  finally
    //CheckInvariant;
  end;
end;

function TBufferedStream.Write(const Buffer; Count: Integer): Longint;
var
  nBytesRemaining: integer;
begin
  //CheckInvariant;
  try
    if FState = bsRead then
      if not FlushBuffer then
      begin
        Result := 0;
        Exit;
      end
      else
        FState := bsWrite;
    if (FBytesUsed = 0) and (Count > BufferSize) then
      Result := 0
    else
      Result := WriteToBuffer(@Buffer, Count);
    nBytesRemaining := Count - Result;
    if nBytesRemaining > 0 then
    begin
      if FlushBuffer then
      begin
        if nBytesRemaining > BufferSize then
        begin
          Result := Result + inherited Write( (PChar(@Buffer) + Result)^, nBytesRemaining)
        end
        else
          Result := Result + WriteToBuffer(PChar(@Buffer) + Result, nBytesRemaining);
      end
    end;
  finally
    //CheckInvariant;
  end;
end;

function TBufferedStream.WriteToBuffer(data: PChar; nBytes: integer): Integer;
begin
  //CheckInvariant;
  try
    if BufferSize - FBytesUsed < nBytes then
      Result := BufferSize - FBytesUsed
    else
      Result := nBytes;
    Move( data^, (FBuffer + FBytesUsed)^, Result);
    FBytesUsed := FBytesUsed + Result;
    FVirtualPosition := FBytesUsed;
  finally
    //CheckInvariant;
  end;
end;

function TBufferedStream.Seek(const Offset: Int64;
  Origin: TSeekOrigin): Int64;
begin
  //CheckInvariant;
  try
    if not FlushBuffer then
    begin
      RaiseLastOSError;
      Result := INVALID_SET_FILE_POINTER; //to make compiler happy
    end
    else
      Result := inherited Seek(Offset, Origin);
  finally
    //CheckInvariant;
  end;
end;

procedure TBufferedStream.BeforeDestruction;
begin
  if assigned(FBuffer) then
    if not FlushBuffer then
      RaiseLastOSError;

  inherited;
end;

end.
