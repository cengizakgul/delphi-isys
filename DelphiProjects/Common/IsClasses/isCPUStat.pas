unit isCPUStat;

interface

uses Windows, SysUtils, PDH, isBaseClasses, isBasicUtils;

const
  PC_CPU_TOTAL = '% CPU Total';
  PC_PROCESS_CPU_TOTAL = '% Process CPU Total';

type
  TisPerfCounterType = (pcCPUTotal, pcProcessCPUTotal);

  IisPerformanceCounter = interface
  ['{64A8FDCC-7740-4345-A30A-06C77C4C3756}']
    procedure AddCounter(const ACounterType: TisPerfCounterType);
    procedure DeleteCounter(const ACounterType: TisPerfCounterType);
    function  CollectData: IisListOfValues;
  end;

function PerformanceCounter: IisPerformanceCounter;

implementation

type
  TCounterFormat = record
    Name: String;
    Format: Cardinal;
  end;
  TCounterTemplate = record
    ObjectIndex:integer;
    CounterIndex:integer;
    InstanceName:string;
  end;
  TisPerformanceCounter = class(TisCollection, IisPerformanceCounter)
  private
    FQuery: THandle;
    FCounters: array [Low(TisPerfCounterType)..High(TisPerfCounterType)] of THandle;
  protected
    procedure AddCounter(const ACounterType: TisPerfCounterType);
    procedure DeleteCounter(const ACounterType: TisPerfCounterType);
    function  CollectData: IisListOfValues;
  public
    destructor Destroy; override;
  end;


const
      CountersPath: array [Low(TisPerfCounterType)..High(TisPerfCounterType)] of String =
       ('\Processor(_Total)\% Processor Time',
        '\Process($ProcessName$)\% Processor Time');

      CountersFormats: array [Low(TisPerfCounterType)..High(TisPerfCounterType)] of TCounterFormat =
      ((Name: PC_CPU_TOTAL; Format: PDH_FMT_LONG),
       (Name: PC_PROCESS_CPU_TOTAL; Format: PDH_FMT_LONG));

      CountersTemplates : array [Low(TisPerfCounterType)..High(TisPerfCounterType)] of TCounterTemplate =
      (
      (ObjectIndex:238;CounterIndex:6;InstanceName:'_Total'),
      (ObjectIndex:230;CounterIndex:6;InstanceName:'$ProcessName$')
      );

var FPerformanceCounter: IisPerformanceCounter;

function PerformanceCounter: IisPerformanceCounter;
begin
  Result := FPerformanceCounter;
end;


{ TisPerformanceCounter }

procedure TisPerformanceCounter.AddCounter(const ACounterType: TisPerfCounterType);
var
  Counter: THandle;
  s: string;

  function  MakeFullCounterPath:string;
  var
    pdhObjectName, pdhCounterName: AnsiString;
    sz: DWORD;
    pName: PAnsiChar;
  begin
    sz := 256;
    GetMem(pName, sz);
    try
      if PdhLookupPerfNameByIndexA(nil, CountersTemplates[ACounterType].ObjectIndex, pName, sz)   <> ERROR_SUCCESS then
         RaiseLastOSError;
      pdhObjectName := pName;
      sz := 256;
      if PdhLookupPerfNameByIndexA(nil, CountersTemplates[ACounterType].CounterIndex, pName, sz)  <> ERROR_SUCCESS then
         RaiseLastOSError;
      pdhCounterName := pName;
    finally
      FreeMem(pName);
    end;

//    \\Computer\PerfObject(ParentInstance/ObjectInstance#InstanceIndex)\Counter
    Result := '\' + pdhObjectName + '(' + CountersTemplates[ACounterType].InstanceName + ')\' + pdhCounterName;
    Result := StringReplace(Result, '$ProcessName$', ChangeFileExt(ExtractFileName(AppFileName), ''), [rfReplaceAll]);
  end;

begin
  Lock;
  try
    if FQuery = 0 then
      if PdhOpenQuery(nil, 0, FQuery) <> ERROR_SUCCESS then
        RaiseLastOSError;

    if FCounters[ACounterType] = 0 then
    begin
      s := MakeFullCounterPath;
      Counter :=0;
      if PdhAddCounter(FQuery, PAnsiChar(s), 0, Counter) <> ERROR_SUCCESS then
         RaiseLastOSError;
      FCounters[ACounterType] := Counter;
    end;
  finally
    Unlock;
  end;
end;

function TisPerformanceCounter.CollectData: IisListOfValues;

  procedure CollectCounters(const AStopOnError: Boolean; out AError: Boolean);
  var
    Data: TPDH_FMT_COUNTERVALUE;
    i: TisPerfCounterType;
    V: Variant;
  begin
    if PdhCollectQueryData(FQuery) <> ERROR_SUCCESS then
      RaiseLastOSError;

    AError := False;

    Result := TisListOfValues.Create;
    for i := Low(FCounters) to High(FCounters) do
    begin
      if FCounters[i] <> 0 then
      begin
        if PdhGetFormattedCounterValue(FCounters[i], CountersFormats[i].Format, nil, Data) = ERROR_SUCCESS then
        begin
          if (CountersFormats[i].Format and PDH_FMT_LONG) <> 0 then
            v := Data.longValue
          else if (CountersFormats[i].Format and PDH_FMT_DOUBLE) <> 0 then
            v := Data.doubleValue
          else if (CountersFormats[i].Format and PDH_FMT_DOUBLE) <> 0 then
            v := Data.doubleValue
          else if (CountersFormats[i].Format and PDH_FMT_LARGE) <> 0 then
            v := Data.largeValue
          else if (CountersFormats[i].Format and PDH_FMT_ANSI) <> 0 then
            v := String(Data.AnsiStringValue);
        end

        else
        begin
          AError := True;
          if AStopOnError then
            break;

          // Don't want to loose all counters if one has failed, so let's put some blanks
          if (CountersFormats[i].Format and PDH_FMT_ANSI) <> 0 then
            v := ''
          else
            v := 0;
        end;

        Result.AddValue(CountersFormats[i].Name, V);
      end;
    end;
  end;

var
  BufSize: Cardinal;
  Err: Boolean;
begin
  Lock;
  try
    CollectCounters(True, Err);

    if Err then
    begin
      // Need to refresh counter list for OS prior server 2003. Read MSDN for details.
      BufSize := 0;
      if PdhEnumObjects(nil, nil, nil, BufSize, PERF_DETAIL_STANDARD, True) <> PDH_STATUS(PDH_MORE_DATA) then
        RaiseLastOSError;

      CollectCounters(False, Err);  // try again
    end;
  finally
    Unlock;
  end;
end;

procedure TisPerformanceCounter.DeleteCounter(const ACounterType: TisPerfCounterType);
begin
  Lock;
  try
    if (FQuery <> 0) and (FCounters[ACounterType] <> 0) then
    begin
      if PdhRemoveCounter(FCounters[ACounterType]) <> ERROR_SUCCESS then
        RaiseLastOSError;
      FCounters[ACounterType] := 0;
    end;
  finally
    Unlock;
  end;
end;

destructor TisPerformanceCounter.Destroy;
var
  i: TisPerfCounterType;
begin
  try
    for i := Low(TisPerfCounterType) to High(TisPerfCounterType) do
      DeleteCounter(i);
  finally
    if FQuery <> 0 then
      PdhCloseQuery(FQuery);
  end;
      
  inherited;
end;

initialization
  FPerformanceCounter := TisPerformanceCounter.Create;

finalization
  FPerformanceCounter := nil;  

end.
