unit IsSYLK;

interface

uses SysUtils, Classes, Windows, Graphics, Controls, wwDBGrid, Forms, EvStreamUtils;

procedure ExportToSylkFile(Grid: TwwDBGrid; const FileName: String);
procedure ExportToSylkStream(Grid: TwwDBGrid; fs: IevDualStream);

implementation

uses wwstr, db, wwDBComb, wwRadioGroup, wwCheckbox, wwDBiGrd, wwcommon, wwmemo, Dialogs, clipbrd;

const
  wwCRLF = #13#10;

function getDbCol(Grid: TwwDBGrid; Col: Integer): Integer;
begin
  Result := Col;
  if wwdbigrd.dgIndicator in Grid.Options then
    Result := Col - 1;
end;

function AddToSYLKFontList(aFont: TFont; FontList: TStrings): String;
var
  FontStr: String;
  BoldFlag: Boolean;
  ItalicFlag: Boolean;
  UnderlineFlag: Boolean;
  FontCount: Integer;
begin
  Result := '';
  if FontList = nil then
    FontList := TStringList.Create;
  BoldFlag := False;
  ItalicFlag := False;
  UnderlineFlag := False;
  FontStr := 'P;F' + aFont.Name + ';M' + IntToStr(aFont.Size * 20);
  if fsBold in aFont.Style then
  begin
    FontStr := Fontstr+';SB';
    BoldFlag := True;
  end;
  if fsItalic in aFont.Style then begin
    if BoldFlag then FontStr := FontStr + 'I'
    else FontStr := FontStr+';SI';
    ItalicFlag := True;
  end;
  if fsUnderline in aFont.Style then begin
    if (not BoldFlag) and (not ItalicFlag) then
       Fontstr := Fontstr+';SU'
    else Fontstr := Fontstr + 'U';
    UnderlineFlag := True;
  end;
  if fsStrikeOut in aFont.Style then begin
    if (not BoldFlag) and (not ItalicFlag) and (not UnderlineFlag) then
       Fontstr := Fontstr+';SS'
    else Fontstr := Fontstr + 'U';
  end;
  if (FontList.IndexOfName(FontStr)>-1) then
  begin
    Result := FontList.Values[FontStr];
    Exit;
  end;
  FontCount := FontList.Count;
  FontList.Add(FontStr + '=' + IntToStr(FontCount));
  Result := IntToStr(FontCount);
end;

function GetSYLKAlignment(Value: TAlignment):string;
begin
  Result := '';
  case Value of
  taLeftJustify: Result := 'L';
  taRightJustify: Result := 'R';
  taCenter: Result := 'C';
  end;
end;

function AddQuotes(s: String): String;
begin
  Result := '"' + s + '"';
end;

function ReplaceStrWithStr(str: String; RemoveStr: String; ReplaceStr: String): String;
var
  i: integer;
begin
  Result := '';
  i:=1;
  while i<=length(str) do
  begin
    if (strlcomp(PChar(Copy(str,i,length(removestr))), PChar(removestr),
      length(removestr))<>0) then
    begin
      Result := Result + str[i];
      i := i + 1;
    end
    else
    begin
      Result := Result + ReplaceStr;
      i := i + length(RemoveStr);
    end;
  end;
end;

procedure ExportToSylkFile(Grid: TwwDBGrid; const FileName: String);
var
  fs: IevDualStream;
  curBookmark: TBookmark;
begin
  Screen.cursor:= crHourGlass;
  try
    Grid.DataSource.Dataset.DisableControls;
    try
      curBookmark := Grid.DataSource.Dataset.GetBookmark;
      try
        fs := TevDualStreamHolder.Create;
        ExportToSylkStream(Grid, fs);
        fs.SaveToFile(FileName);
      finally
        Grid.DataSource.Dataset.GotoBookmark(CurBookmark);
        Grid.DataSource.Dataset.FreeBOokmark(CurBookmark);
      end;
    finally
      Grid.DataSource.Dataset.EnableControls;
    end;
  finally
    Screen.cursor:= crDefault;
  end;
end;

procedure ExportToSylkStream(Grid: TwwDBGrid; fs: IevDualStream);
var
  i: Integer;
  accept : Boolean;
  RecordStr: String;
  CurRowCount: Integer;
  SYLKFontList: TStrings;
  ExportFieldList: TStringList;
  TotalPixelCount: Integer;
  SYLKFormatIndex: TStringList;

  function getDisplayLabelForLine(lbl: String; lineno: Integer): String;
  var
    APos, CurLineNo: Integer;
    line: string;
  begin
    APos:= 1;
    result := '';
    if lineno = 0 then exit;
    curlineno := 1;
    while True do
    begin
      line:= strGetToken(lbl, '~', APos);
      if curlineno = lineno then
      begin
        Result := line;
        Exit;
      end;
      if (line='') and ((APos<=0) or (APos>=length(line))) then
        break;
      inc(CurLineNo);
    end;
  end;

  function CheckFonts: String;
  var
    i, cnt: Integer;
    GridFont:TFont;
    GridBrush:TBrush;
  begin
    cnt := 1;
    Result := '';
    Grid.Datasource.Dataset.First;
    while not Grid.DataSource.DataSet.EOF do
    begin
      Inc(cnt);
      for i := 0 to ExportFieldList.Count - 1 do
      begin
        if (ExportFieldList[i] = 'T') then
        begin
          gridFont := Grid.Canvas.Font;
          gridFont.Assign(Grid.Font);
          gridBrush := Grid.Canvas.Brush;
          GridBrush.Color := Grid.Color;
          AddToSYLKFontList(gridFont, SYLKFontList);
        end;
      end;
      Grid.DataSource.DataSet.Next;
    end;
    Result := IntToStr(cnt);
  end;

  function WriteSYLKHeader: String;
  var
    RowStr, cnt: String;
    i,j,k,count, StartCol, EndCol: Integer;
    GroupColCount: Integer;
    RecOffset: Integer;
    FontTag, FormatStr: String;
    gridFont: TFont;
    gridBrush: TBrush;
    gridTitleAlignment: TAlignment;
    SYLKFormatList: TStrings;
    TempDisplayLabel: String;
    FontList: String;
    ExportFieldCount: Integer; // 10/01/2001 -PYW - Use current numbered exported field instead of fieldnumber
  begin
    SYLKFontList:=TStringList.Create;
    SYLKFormatList := TStringList.Create;

    cnt := CheckFonts;

    //Add Grid TitleFont
    gridFont := Grid.Canvas.Font;
    gridFont.assign(TwwDBGrid(Grid).TitleFont);
    AddToSYLKFontList(gridFont,SYLKFontList);
    //Add GridFont
    gridFont := Grid.Canvas.Font;
    gridFont.assign(TwwDBGrid(Grid).Font);
    AddToSYLKFontList(gridFont,SYLKFontList);

    for i:= 0 to SYLKFontList.Count-1 do
      FontList := FontList + SYLKFontList.Names[i] + wwCRLF;

    //Set Identifier and Program name
    RowStr := 'ID;P'+'W2W 3000'+';N;E'+wwCRLF;
    // 9/26/2001-Add general format strings for date/time/datetime and currency to header. - PYW
    SYLKFormatList.Add('General');
    count:=1;
    // 9/26/2001-Build list of unique formats and store appropriate index into list for field export.
    for i:= 0 to exportFieldList.Count-1 do
    begin
      case Grid.Datalink.Fields[i].datatype of
        ftDate: FormatStr:='m/d/yyyy';
        ftTime: FormatStr:='h:mm\ AM/PM';
        {$ifdef wwDelphi6Up}
        ftTimeStamp,
        {$endif}
        ftDateTime: FormatStr:='m/d/yyyy\ h:mm\ AM/PM';
        ftCurrency: FormatStr:='"'+'$'+'"'+'#,##0.00_);;[Red]\('+'"'+'$'+'"'+'#,##0.00\)';
        else
          FormatStr := 'General';
      end;
      Grid.DoExportSYLKFormat(grid,Grid.Datalink.Fields[i], FormatStr);
      if SYLKFormatList.IndexOf(FormatStr) < 0 then
      begin
        SYLKFormatList.Add(FormatStr);
        SYLKFormatIndex[i] := IntToStr(Count);
        Inc(Count);
      end
      else SYLKFormatIndex[i] := IntToStr(SYLKFormatList.IndexOf(FormatStr));
    end;

    // Place formatlist into header.
    for i := 0 to SYLKFormatList.count - 1 do
      RowStr := RowStr+'P;P' + SYLKFormatList[i] + wwCRLF;

    SYLKFormatList.Free;

    //Set dimensions.
    //NOTE: Don't know rowcount yet so leave blank.

    RowStr := RowStr + FontList;

    RowStr := RowStr + 'B;Y' + cnt + ';X' + IntToStr(ExportFieldList.Count) + wwCRLF;

    // 3/13/2002-PYW-Add new property to allow use of A1 reference in Excel vs R1C1.
    // RowStr := RowStr + 'O;L' + wwCRLF;

    //Turn off grid lines in excel.
    RowStr := RowStr+ 'F;G;M255' + wwCRLF;

    Count := 0;
    RecOffset := 0;

    for i:= 0 to ExportFieldList.Count-1 do
    begin
      if (ExportFieldList[i] = 'T') then
      begin
        Inc(Count);
        RowStr := RowStr + 'F;W' + IntToStr(count) + ' ' + IntToStr(count) + ' ' +
          IntToStr(Grid.Columns[getDbCol(Grid, I + grid.xIndicatorOffset)].DisplayWidth) + wwCRLF;
       end;
    end;

    for j := 0 to Grid.TitleLines - 1 do
    begin
      i := 0; //Start back at the first field.
      ExportFieldCount := RecOffset;
      while i < exportFieldList.Count do
      begin
        if (ExportFieldList[i] = 'F') then
        begin
          inc(i);
          continue;
        end;
        //If we are on a subgroup or a nongroup title cell
        if (j > 0) or (grid.Columns[GetDbCol(Grid,i+grid.xIndicatorOffset)].GroupName = '') then
        begin
          if (exportFieldList[i] = 'T') then
          begin
            gridFont := Grid.Canvas.Font;
            gridFont.assign(TwwDBGrid(Grid).TitleFont);
            gridBrush := Grid.Canvas.Brush;
            GridBrush.Color := TwwDBGrid(Grid).TitleColor;
            gridTitleALignment := Grid.TitleAlignment;

            FontTag := AddToSYLKFontList(gridFont,SYLKFontList);

            if (grid.Columns[getdbcol(Grid,i+grid.xIndicatorOffset)].GroupName = '') then
            begin
              TempDisplayLabel := StrReplaceCharWithStr(grid.Columns[getdbcol(Grid,i+grid.xIndicatorOffset)].DisplayLabel,';',';;');
              TempDisplayLabel := GetDisplayLabelForLine(TempDisplayLabel,j+1);
              RowStr := RowStr+'F;SLR';
              if j = 0 then
                RowStr := RowStr + 'T';
              if (j=Grid.TitleLines-1) then
                RowStr := RowStr+'B';
              // 10/01/2001 -PYW - Use current numbered exported field instead of fieldnumber
              RowStr := RowStr + 'M' + FontTag + ';FG0' + GetSYLKAlignment(gridTitleALignment)+
                ';Y'+IntToStr(j+1)+';X'+IntToStr(ExportFieldCount+1) + wwCRLF;
              if TempDisplayLabel <> '' then
                RowStr := RowStr + 'C;K"'+ TempDisplayLabel + '"' + wwCRLF;
              Inc(ExportFieldCount);
            end
            else
            begin
              if j < (grid.titlelines div 2) then
              begin
                TempDisplayLabel := strReplaceCharWithStr(grid.Columns[getdbcol(Grid,i+grid.xIndicatorOffset)].GroupName,';',';;');
                TempDisplayLabel := getdisplaylabelforline(tempdisplaylabel,j+1);
                RowStr := RowStr + 'F;SL';
                if j + 1 >= (grid.titlelines div 2) then
                  RowStr := RowStr + 'B';
                // 10/01/2001 -PYW - Use current numbered exported field instead of fieldnumber
                RowStr := RowStr + 'M0;FG0C;Y' + IntToStr(j+1) + ';X' +
                  IntToStr(exportfieldcount+1)+wwCRLF;
//                        rowstr := rowstr+'F;SLBRM'+fonttag+';FG0'+GetSYLKAlignment(gridTitleALignment)+';Y2;X'+Inttostr(i+1+recoffset)+wwCRLF;
                RowStr := RowStr + 'C;K"' + TempDisplayLabel + '"' + wwCRLF;
                groupColCount := 0;
                //!!11/29/2001
                for k := StartCol to EndCol do
                  if ExportFieldList[k-1] = 'T' then
                    Inc(groupColcount);

                for k := i + 1 to i + GroupColCount - 1 do
                begin
                  if ExportFieldList[k] = 'T' then
                  begin
                    RowStr := Rowstr + 'F;S';
                    if (j + 1 >= grid.titlelines div 2) then
                      RowStr := RowStr + 'B';
                    RowStr := RowStr + 'M0;FG0C;X' + IntToStr(k + 1 + RecOffset) + wwCRLF;
                    RowStr := RowStr + 'C;K""' + wwCRLF;
                  end;
                end;
                Inc(i, EndCol-StartCol);
                Inc(ExportFieldCount);
              end
              else
              begin
                TempDisplayLabel := strReplaceCharWithStr(grid.Columns[getdbcol(Grid,i+grid.xIndicatorOffset)].DisplayLabel,';',';;');
                TempDisplayLabel := getdisplaylabelforline(TempDisplayLabel,j+1-(grid.titlelines div 2));
                RowStr := RowStr + 'F;SLR';
                if (j-1)<(grid.titlelines div 2) then
                  RowStr := RowStr + 'T';
                if (j = Grid.TitleLines - 1) then
                  RowStr := RowStr+'B';
                // 10/01/2001 -PYW - Use current numbered exported field instead of fieldnumber
                RowStr := RowStr + 'M' + FontTag + ';FG0' +
                  GetSYLKAlignment(gridTitleALignment)+';Y'+
                  IntToStr(j + 1) + ';X' + IntToStr(ExportFieldCount+1) + wwCRLF;
                if TempDisplayLabel <> '' then
                   RowStr := RowStr + 'C;K"' + TempDisplayLabel + '"' + wwCRLF;
                Inc(ExportFieldCount);
              end;
            end;
          end;
          Inc(i);
        end
        else
        begin //First Row of a Group Cell.  Add tag and get next cell
          grid.GroupNameCellRect(i + grid.xIndicatorOffset,0, StartCol, EndCol, False);
          groupColCount := 0;
          for k := StartCol to EndCol do
          begin
            if ExportFieldList[k-1] = 'T' then
              Inc(groupColcount);
          end;
          if groupcolCount > 0 then
          begin
            // 10/01/2001 -PYW - Use current numbered exported field instead of fieldnumber
            RowStr := RowStr + 'F;SLTM0;FG0C;Y1;X' + IntToStr(ExportFieldCount + 1) + wwCRLF;
            Inc(ExportFieldCount);
            TempDisplayLabel := strReplaceCharWithStr(grid.Columns[GetDbCol(Grid, i + grid.xIndicatorOffset)].GroupName,';',';;');
            TempDisplayLabel := getDisplayLabelForLine(TempDisplayLabel, j + 1);
            RowStr := RowStr + 'C;K"' + TempDisplayLabel + '"' + wwCRLF;
            for k := i + 1 to i + GroupColCount - 1 do
            begin
              if ExportFieldList[k] = 'T' then
              begin
                RowStr := RowStr + 'F;STM0;FG0C;X' + IntToStr(k + 1 + RecOffset) + wwCRLF;
                RowStr := RowStr + 'C;K""' + wwCRLF;
                Inc(ExportFieldCount);
              end;
            end;
          end;
          Inc(i, EndCol - StartCol + 1);
        end;
      end;
    end;
    Result := RowStr + wwCRLF;
  end;

  function WriteSYLKDataRow: String;
  var
    i: Integer;
    RowStr, FontTag, TempStr: String;
    RecOffset: Integer;
    GridFont:TFont;
    GridBrush:TBrush;
    ExportFieldCount: Integer;
    Pos1, RowNumberOffset: Integer; 
    AControlType, Parameters: wwSmallString;
    CustomEdit: TWinControl;
    ControlName: String;
  begin
    with Grid do
    begin
      RowStr := '';
      RecOffset :=0;
      //9/26/2001-Only increment the rownumberoffset by as many rows that are in the header.
      RowNumberOffset := TitleLines;
      // 10/01/2001 -PYW - Use current numbered exported field instead of fieldnumber
      ExportFieldCount := RecOffset;
      for i := 0 to ExportFieldList.Count-1 do
      begin
        if (ExportFieldList[i] = 'T') then
        begin
          inc(ExportFieldCount);
          gridFont := Grid.Canvas.Font;
          gridFont.assign(Font);
          gridBrush := Grid.Canvas.Brush;
          GridBrush.Color:=Color;

          FontTag := AddToSYLKFontList(gridFont,SYLKFontList);

          //9/26/2001-Specify the associated format based on field and datatype.
          // 10/01/2001 -PYW - Use current numbered exported field instead of fieldnumber
          case Grid.Datalink.Fields[i].datatype of
         {$ifdef wwDelphi6Up}
          ftTimeStamp,
         {$endif}
          ftFloat,ftDate,ftTime,ftDateTime,ftCurrency:
            RowStr := RowStr+'F;P'+SYLKFormatIndex[i]+';SLTRBM'+ FontTag + ';FG0'+GetSYLKAlignment(Grid.Datalink.Fields[i].alignment)+';Y'+IntToStr(curRowCount+rownumberoffset)+';X'+IntToStr(exportfieldcount)+wwCRLF;
          else
            RowStr := RowStr+'F;SLTRBM'+fonttag+';FG0'+GetSYLKAlignment(Grid.Datalink.Fields[i].alignment)+';Y'+IntToStr(curRowCount+rownumberoffset)+';X'+IntToStr(exportfieldcount)+wwCRLF;
          end;

          TempStr := strReplaceCharWithStr(GetFieldValue(i),';',';;');
          if not Grid.DataLink.Fields[i].IsNull then
          case Grid.Datalink.Fields[i].datatype of
            ftBoolean:
            begin
              if Grid.DataLink.Fields[i].asBoolean then
                 TempStr := 'TRUE'
              else Tempstr := 'FALSE';
              TempStr := 'C;K' + AddQuotes(TempStr) + wwCRLF;
            end;
            ftLargeInt,ftSmallInt,ftWord,ftInteger:
            begin
              //2/1/2002 - PYW - Added code to handle special controls in grid.
              GetControlInfo(Datalink.Fields[i].FieldName, AControlType, Parameters);
              if (AControlType = 'CustomEdit') then begin
                 Pos1:= 1;
                 ControlName:= strGetToken(Parameters, ';', Pos1);
                 CustomEdit:= GetComponent(ControlName);
                 if (CustomEdit is TwwDBComboBox) then
                 begin
                   TempStr := 'C;K' + IntToStr(Grid.DataLink.Fields[i].asInteger)+wwCRLF;
                 end
                 else if (CustomEdit is TwwRadioGroup) or (CustomEdit is TwwCheckBox) then
                 begin
                   TempStr := 'C;K' + IntToStr(Grid.DataLink.Fields[i].asInteger) + wwCRLF;
                 end
                 else
                   TempStr := 'C;K' + TempStr + wwCRLF;
              end
              else
                TempStr := 'C;K' + TempStr + wwCRLF;
            end;
            //9/26/2001-Added handling of the following additional data types.
//                ftfloat,ftCurrency: tempstr := 'C;K'+FloatToStr(Grid.DataLink.Fields[i].asFloat)+wwCRLF;
            ftfloat, ftCurrency: begin
               //3/13/2002-PYW-Make sure the SYLK format is stored with decimals
              TempStr := 'C;K'+FloatToStr(Grid.DataLink.Fields[i].asFloat)+wwCRLF;
              if DecimalSeparator <> '.' then
                TempStr := ReplaceStrWithStr(TempStr, DecimalSeparator,'.');
            end;
            ftDate: TempStr := 'C;K'+IntToStr(Trunc(Grid.DataLink.Fields[i].asDateTime)) + wwCRLF;
            ftTime: begin
              TempStr := 'C;K' + FloatToStr(Frac(Grid.DataLink.Fields[i].asDateTime)) + wwCRLF;
              if DecimalSeparator <> '.' then
                TempStr := ReplaceStrWithStr(TempStr,DecimalSeparator,'.');
            end;

           {$ifdef wwDelphi6Up}
            ftTimeStamp,
           {$endif}
            ftDateTime:
            begin
              TempStr := 'C;K'+FloatToStr(Grid.DataLink.Fields[i].asFloat)+wwCRLF;
              if DecimalSeparator <> '.' then
                TempStr := ReplaceStrWithStr(tempstr,DecimalSeparator,'.');
            end;
          else
            //SYLK does not like #13 and #10 carriage return line feed characters in string.
            //Replace them with spaces.
            TempStr := ReplaceStrWithStr(TempStr, #13#10,' ');
//                tempstr := strReplaceCharWithStr(tempstr,#10,' ');
            //Then copy the 1st 255 characters as SYLK does not like cells longer than 255.
            TempStr := Copy(TempStr, 1, 255);
            //Add SYLK Data tags.
            if TempStr = '' then
              TempStr := 'C;K""'+wwCRLF
            else
              TempStr := 'C;K' + AddQuotes(TempStr) + wwCRLF;
          end
          else
            TempStr := 'C;K""' + wwCRLF;
          RowStr := RowStr + TempStr;
        end;
      end;
      Result := RowStr;
    end;
  end;

begin
  if not Grid.Datalink.Active then
    Exit;

  SYLKFontList:= nil;
  SYLKFormatIndex := nil;
  exportfieldList := TStringList.Create;
  SYLKFormatIndex := TStringList.Create;
  try
    with Grid do
    begin
      TotalPixelCount := 0;
//      if esoShowRecordNo in self.Options then TotalPixelCount := 0;
      // Save columns to export and total pixel width for later use.
      for i := 0 to Datalink.FieldCount - 1 do
      begin
        Accept := True;
        DoExportField(Grid,DataLink.Fields[i],Accept);
        if Accept then
        begin
          ExportFieldList.Add('T');
          TotalPixelCount := TotalPixelCount + Grid.ColWidthsPixels[i+xindicatorOffset] + 5;
        end
        else exportFieldList.Add('F');
        SYLKFormatIndex.Add('0');
      end;

      //6/13/2001-SYLK format has additional header initialization that needs to be called.
      //7/26/2001-PYW-Handle Header

      RecordStr := WriteSYLKHeader;
      fs.WriteBuffer(Pointer(RecordStr)^, Length(RecordStr));

      CurRowCount := 0;

      Datasource.Dataset.First;
      while not DataSource.DataSet.EOF do
      begin
        Inc(CurRowCount);
        RecordStr := WriteSYLKDataRow;
        fs.WriteBuffer(Pointer(recordstr)^, Length(recordstr));
        DataSource.DataSet.Next;
      end;
    end;
    RecordStr := 'E';
    fs.WriteBuffer(Pointer(RecordStr)^, Length(RecordStr));
  finally
    exportFieldList.Free;
    SYLKFontList.Free;
    SYLKFormatIndex.Free;
  end;
end;

end.


