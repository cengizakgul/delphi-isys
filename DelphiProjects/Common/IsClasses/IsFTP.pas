unit IsFtp;

interface

uses Classes, EvStreamUtils, WinInet, isBaseClasses, isExceptions;

const
  FtpAgent = 'EVOLUTION';

type
  TIsFTP = class(TisInterfacedObject)
  private
    FHost: String;
    FUser: String;
    FPort: Integer;
    FPassword: String;
    FDirectoryListing: TStringList;
    FNet, FFtp: HINTERNET;
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
  public
    property Host: String read FHost write FHost;
    property Port: Integer read FPort write FPort;
    property User: String read FUser write FUser;
    property Password: String read FPassword write FPassword;
    procedure Connect;
    procedure Disconnect;
    procedure ReadDirectory;
    property  DirectoryListing: TStringList read FDirectoryListing;
    procedure Delete(const AFileName: String);
    procedure Put(const AFileName: String); overload;
    procedure Get(const ANewFile, ARemoteFile: String; AResume: Boolean = False); overload;
    procedure MakeDir(const ADirName: String);
    procedure ChangeDir(const ADirName: String);
    procedure Put(const stm: IevDualStream; const AFileName: String); overload;
  end;

function FtpDownloadFile(const strHost, strUser, strPwd: string;
  Port: Integer; const ftpDir, ftpFile, localFile: string): Boolean;

function FtpUploadFile(const strHost, strUser, strPwd: string;
  Port: Integer; const ftpDir, ftpFile, localFile: string): Boolean;

function FtpDownloadStream(const strHost, strUser, strPwd: string;
  Port: Integer; const ftpDir, ftpFile: string; stm: IevDualStream): Boolean;

function FtpUploadStream(const strHost, strUser, strPwd: string;
  Port: Integer; const ftpDir, ftpFile: string; stm: IevDualStream): Boolean;

function FtpTest(const strHost, strUser, strPwd: string;
  Port: Integer = 21): Boolean;

implementation

uses SysUtils, Windows;

const
  WRITE_BUFFERSIZE = 4096;  // or 256, 512, :
  READ_BUFFERSIZE  = 4096;  // or 256, 512, :

function FtpUploadFile(const strHost, strUser, strPwd: string;
  Port: Integer; const ftpDir, ftpFile, localFile: string): Boolean;
var
  stm: IEvDualStream;
begin
  stm := TEvDualStreamHolder.CreateFromFile(localFile);
  Result := FtpUploadStream(strHost, strUser, strPwd, Port, ftpDir, ftpFile, stm);
end;

function FtpDownloadFile(const strHost, strUser, strPwd: string;
  Port: Integer; const ftpDir, ftpFile, localFile: string): Boolean;
var
  stm: IEvDualStream;
begin
  stm := TEvDualStreamHolder.CreateInMemory;
  Result := FtpDownloadStream(strHost, strUser, strPwd, Port, ftpDir, ftpFile, stm);
  stm.SaveToFile(localFile);
end;

function FtpDownloadStream(const strHost, strUser, strPwd: string;
  Port: Integer; const ftpDir, ftpFile: string; stm: IevDualStream): Boolean;
var
  hNet, hFTP, hFile: HINTERNET;
  buffer: array[0..READ_BUFFERSIZE - 1] of Char;
  bufsize, fileSize: DWORD;
  sRec: TWin32FindData;
begin
  Result := False; 
  { Open an internet session }
  hNet := InternetOpen(FtpAgent, // Agent
                       INTERNET_OPEN_TYPE_PRECONFIG, // AccessType
                       nil,  // ProxyName
                       nil, // ProxyBypass
                       0); // or INTERNET_FLAG_ASYNC / INTERNET_FLAG_OFFLINE
  if hNet = nil then
    raise EisException.Create('Unable to get access to WinInet.Dll');

  try
    { Connect to the FTP Server }
    hFTP := InternetConnect(hNet, // Handle from InternetOpen
                            PChar(strHost), // FTP server
                            port, // (INTERNET_DEFAULT_FTP_PORT),
                            PChar(StrUser), // username
                            PChar(strPwd),  // password
                            INTERNET_SERVICE_FTP, // FTP, HTTP, or Gopher?
                            0, // flag: 0 or INTERNET_FLAG_PASSIVE
                            0);// User defined number for callback

    if hFTP = nil then
      raise EisException.CreateFmt('Host "%s" is not available',[strHost]);
    try
      { Change directory }
      if not FtpSetCurrentDirectory(hFTP, PChar(ftpDir)) then
        raise EisException.CreateFmt('Cannot set directory to %s.',[ftpDir]);

      { Read size of file }
      if FtpFindFirstFile(hFTP, PChar(ftpFile), sRec, 0, 0) <> nil then
        fileSize := sRec.nFileSizeLow
      else
        raise EisException.CreateFmt('Cannot find file %s',[ftpFile]);

      { Open the file }
      hFile := FtpOpenFile(hFTP, // Handle to the ftp session
                           PChar(ftpFile), // filename
                           GENERIC_READ, // dwAccess
                           FTP_TRANSFER_TYPE_BINARY, // dwFlags
                           0); // This is the context used for callbacks.
      if hFile = nil then
        raise EisException.CreateFmt('Cannot open file %s',[ftpFile]);

      try
        bufsize := READ_BUFFERSIZE;
        while (bufsize > 0) do
        begin
          if not InternetReadFile(hFile,
                                  @buffer, // address of a buffer that receives the data
                                  READ_BUFFERSIZE, // number of bytes to read from the file
                                  bufsize) then
            raise EisException.CreateFmt('Cannot read file %s', [ftpFile]);
          if (bufsize > 0) and (bufsize <= READ_BUFFERSIZE) then
            stm.WriteBuffer(buffer, bufsize);
        end;
      finally
        InternetCloseHandle(hFile);
      end;
      Result := fileSize = DWORD(stm.Size);
    finally
      InternetCloseHandle(hFTP);
    end;
  finally
    InternetCloseHandle(hNet);
  end;
end;

function FtpUploadStream(const strHost, strUser, strPwd: string;
  Port: Integer; const ftpDir, ftpFile: string; stm: IevDualStream): Boolean;
var
  hNet, hFTP, hFile: HINTERNET;
  buffer: array[0..WRITE_BUFFERSIZE - 1] of Char;
  bufsize, fileSize: DWORD;
  sRec: TWin32FindData;
begin
  Result := False;
  { Open an internet session }
  hNet := InternetOpen(FtpAgent, // Agent
                       INTERNET_OPEN_TYPE_PRECONFIG, // AccessType
                       nil,  // ProxyName
                       nil, // ProxyBypass
                       0); // or INTERNET_FLAG_ASYNC / INTERNET_FLAG_OFFLINE
  if hNet = nil then
    raise EisException.Create('Unable to get access to WinInet.Dll');

  try
    { Connect to the FTP Server }
    hFTP := InternetConnect(hNet, // Handle from InternetOpen
                            PChar(strHost), // FTP server
                            port, // (INTERNET_DEFAULT_FTP_PORT),
                            PChar(StrUser), // username
                            PChar(strPwd),  // password
                            INTERNET_SERVICE_FTP, // FTP, HTTP, or Gopher?
                            0, // flag: 0 or INTERNET_FLAG_PASSIVE
                            0);// User defined number for callback

    if hFTP = nil then
      raise EisException.CreateFmt('Host "%s" is not available',[strHost]);
    try
      { Change directory }
      if not FtpSetCurrentDirectory(hFTP, PChar(ftpDir)) then
        raise EisException.CreateFmt('Cannot set directory to %s.',[ftpDir]);

      { Open the file }
      hFile := FtpOpenFile(hFTP, // Handle to the ftp session
                           PChar(ftpFile), // filename
                           GENERIC_WRITE, // dwAccess
                           FTP_TRANSFER_TYPE_BINARY, // dwFlags
                           0); // This is the context used for callbacks.
      if hFile = nil then
        raise EisException.CreateFmt('Cannot create file %s',[ftpFile]);

      try
        while stm.Position < stm.Size do
        begin
          bufsize := stm.RealStream.Read(buffer, SizeOf(buffer));
          if not InternetWriteFile(hFile,
                                  @buffer, // address of a buffer that receives the data
                                  bufsize, // number of bytes to read from the file
                                  bufsize) then
            raise EisException.CreateFmt('Cannot write file %s', [ftpFile]);
        end;
      finally
        InternetCloseHandle(hFile);
      end;
      { Read size of file }
      if FtpFindFirstFile(hFTP, PChar(ftpFile), sRec, 0, 0) <> nil then
        fileSize := sRec.nFileSizeLow
      else
        raise EisException.CreateFmt('Cannot find file %s',[ftpFile]);
      Result := fileSize = DWORD(stm.Size);
    finally
      InternetCloseHandle(hFTP);
    end;
  finally
    InternetCloseHandle(hNet);
  end;
end;

function FtpTest(const strHost, strUser, strPwd: string;
  Port: Integer = 21): Boolean;
var
  hNet, hFTP: HINTERNET;
begin
  Result := False;
  { Open an internet session }
  hNet := InternetOpen(FtpAgent, // Agent
                       INTERNET_OPEN_TYPE_PRECONFIG, // AccessType
                       nil,  // ProxyName
                       nil, // ProxyBypass
                       0); // or INTERNET_FLAG_ASYNC / INTERNET_FLAG_OFFLINE
  if hNet = nil then
    raise EisException.Create('Unable to get access to WinInet.Dll');

  try
    { Connect to the FTP Server }
    hFTP := InternetConnect(hNet, // Handle from InternetOpen
                            PChar(strHost), // FTP server
                            port, // (INTERNET_DEFAULT_FTP_PORT),
                            PChar(StrUser), // username
                            PChar(strPwd),  // password
                            INTERNET_SERVICE_FTP, // FTP, HTTP, or Gopher?
                            0, // flag: 0 or INTERNET_FLAG_PASSIVE
                            0);// User defined number for callback

    if hFTP = nil then
      raise EisException.CreateFmt('Host "%s" is not available',[strHost]);
    InternetCloseHandle(hFTP);
  finally
    InternetCloseHandle(hNet);
  end;
end;

{ TIsFTP }

procedure TIsFTP.Connect;
begin
  FNet := InternetOpen(FtpAgent, // Agent
                       INTERNET_OPEN_TYPE_PRECONFIG, // AccessType
                       nil,  // ProxyName
                       nil, // ProxyBypass
                       0); // or INTERNET_FLAG_ASYNC / INTERNET_FLAG_OFFLINE
  if FNet = nil then
    raise EisException.Create('Unable to get access to WinInet.Dll');
  FFtp := InternetConnect(FNet, // Handle from InternetOpen
                          PChar(FHost), // FTP server
                          port,
                          PChar(FUser),
                          PChar(FPassword),
                          INTERNET_SERVICE_FTP,
                          0,
                          0);
  if FFtp = nil then
    raise EisException.CreateFmt('Host "%s" is not available',[FHost]);
end;

procedure TIsFTP.Disconnect;
begin
  if Assigned(FFtp) then
    InternetCloseHandle(FFtp);
  if Assigned(FNet) then
    InternetCloseHandle(FNet);
  FDirectoryListing.Clear;
end;

procedure TIsFTP.ReadDirectory;
var
  sRec: TWin32FindData;
  hSearch: HINTERNET;
begin
  FDirectoryListing.Clear;
  if not Assigned(FFtp) then
    raise EisException.Create('No connection to the FTP server');
  hSearch := FtpFindFirstFile(FFtp, nil, sRec, INTERNET_FLAG_NO_CACHE_WRITE OR INTERNET_FLAG_RELOAD, 0);
  if hSearch <> nil then
  try
    repeat
      if {((sRec.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) <> 0)} True
      and (String(sRec.cFileName) <> '.')
      and (String(sRec.cFileName) <> '..')
      then
        FDirectoryListing.Add(sRec.cFileName);
    until not InternetFindNextFile(hSearch, @sRec);
  finally
    InternetCloseHandle(hSearch);
  end;
end;

procedure TIsFTP.Delete(const AFileName: String);
begin
  if not Assigned(FFtp) then
    raise EisException.Create('No connection to the FTP server');
  if not FtpDeleteFile(FFtp, PChar(AFileName)) then
    raise EisException.CreateFmt('Cannot delete file %s',[AFileName]);
end;

procedure TIsFTP.Put(const AFileName: String);
var
  hFile: HINTERNET;
  buffer: array[0..WRITE_BUFFERSIZE - 1] of Char;
//  sRec: TWin32FindData;
  bufsize: DWORD;
  stm: IEvDualStream;
begin
  stm := TEvDualStreamHolder.CreateFromFile(ExtractFileName(AFileName));
  stm.Position := 0;
  { Open the file }
  hFile := FtpOpenFile(FFtp,
                       PChar(AFileName),
                       GENERIC_WRITE,
                       FTP_TRANSFER_TYPE_BINARY,
                       0);
  if hFile = nil then
    raise EisException.CreateFmt('Cannot create file %s',[AFileName]);

  try
    while stm.Position < stm.Size do
    begin
      bufsize := stm.RealStream.Read(buffer, SizeOf(buffer));
      if not InternetWriteFile(hFile,
                              @buffer, // address of a buffer that receives the data
                              bufsize, // number of bytes to read from the file
                              bufsize) then
        raise EisException.CreateFmt('Cannot write file %s', [AFileName]);
    end;
  finally
    InternetCloseHandle(hFile);
  end;
  { Read size of file }
(*  if FtpFindFirstFile(FFtp, PChar(AFileName), sRec, 0, 0) <> nil then
  begin
    if stm.Size <> Integer(sRec.nFileSizeLow) then
      raise EisException.CreateFmt('Cannot find file %s',[AFileName]);
  end
  else
    raise EisException.CreateFmt('Cannot find file %s',[AFileName]);
*)
end;

procedure TIsFTP.Get(const ANewFile, ARemoteFile: String; AResume: Boolean = False);
var
  hFile: HINTERNET;
  buffer: array[0..WRITE_BUFFERSIZE - 1] of Char;
  bufsize: DWORD;
  stm: IEvDualStream;
begin
  if FileExists(ANewFile) then
  begin
    if not AResume then
      raise EisException.CreateFmt('file %s already exists',[ANewFile]);
    stm := TEvDualStreamHolder.CreateFromFile(ANewFile, False, False);
    stm.Position := stm.Size;
    if not FtpCommand(
      FFtp,
      False,
      FTP_TRANSFER_TYPE_BINARY,
      PChar(Format('REST %d',[stm.Size])),
      0
    ) then raise EisException.CreateFmt('Cannot download file %s',[ARemoteFile]);
    hFile := FtpOpenFile(FFtp,
                         PChar(ARemoteFile),
                         GENERIC_READ,
                         FTP_TRANSFER_TYPE_BINARY,
                         0);
    if hFile = nil then
      raise EisException.CreateFmt('Cannot open file %s',[ARemoteFile]);

    try
      bufsize := READ_BUFFERSIZE;
      while True do
      begin
        FillChar(buffer, SizeOf(buffer), 0);
        bufsize := READ_BUFFERSIZE;
        if not InternetReadFile(hFile,
                                @buffer,
                                READ_BUFFERSIZE,
                                bufsize) then
          raise EisException.CreateFmt('Cannot read file %s', [ARemoteFile]);
        if bufsize > 0 then
          stm.WriteBuffer(buffer, bufsize)
        else
          break;
      end;
    finally
      InternetCloseHandle(hFile);
    end;
  end
  else
    if not FtpGetFile(FFtp,
                      PChar(ARemoteFile),
                      PChar(ANewFile),
                      True,
                      FTP_TRANSFER_TYPE_BINARY,
                      INTERNET_FLAG_RELOAD,
                      0) then
      raise EisException.CreateFmt('Cannot download file %s', [ARemoteFile]);
end;

procedure TIsFTP.MakeDir(const ADirName: String);
begin
  if not Assigned(FFtp) then
    raise EisException.Create('No connection to the FTP server');
  if not FtpCreateDirectory(FFtp, PChar(ADirName)) then
    raise EisException.CreateFmt('Cannot create directory %s',[ADirName]);
end;

procedure TIsFTP.ChangeDir(const ADirName: String);
begin
  if not Assigned(FFtp) then
    raise EisException.Create('No connection to the FTP server');
  if not FtpSetCurrentDirectory(FFtp, PChar(ADirName)) then
    raise EisException.CreateFmt('Cannot set current directory %s',[ADirName]);
end;

procedure TIsFTP.Put(const stm: IevDualStream; const AFileName: String);
var
  hFile: HINTERNET;
  buffer: array[0..WRITE_BUFFERSIZE - 1] of Char;
//  sRec: TWin32FindData;
  bufsize: DWORD;
begin
  stm.Position := 0;
  { Open the file }
  hFile := FtpOpenFile(FFtp,
                       PChar(AFileName),
                       GENERIC_WRITE,
                       FTP_TRANSFER_TYPE_BINARY,
                       0);
  if hFile = nil then
    raise EisException.CreateFmt('Cannot create file %s',[AFileName]);

  try
    while stm.Position < stm.Size do
    begin
      bufsize := stm.RealStream.Read(buffer, SizeOf(buffer));
      if not InternetWriteFile(hFile,
                              @buffer, // address of a buffer that receives the data
                              bufsize, // number of bytes to read from the file
                              bufsize) then
        raise EisException.CreateFmt('Cannot write file %s', [AFileName]);
    end;
  finally
    InternetCloseHandle(hFile);
  end;
  { Read size of file }
//  if FtpFindFirstFile(FFtp, PChar(AFileName), sRec, 0, 0) <> nil then
//  begin
//    if stm.Size <> Integer(sRec.nFileSizeLow) then
//      raise EisException.CreateFmt('Cannot find file %s',[AFileName]);
//  end
//  else
//    raise EisException.CreateFmt('Cannot find file %s',[AFileName]);
end;

procedure TIsFTP.DoOnConstruction;
begin
  inherited;
  FPort := INTERNET_DEFAULT_FTP_PORT;
  FDirectoryListing := TStringList.Create;
end;

procedure TIsFTP.DoOnDestruction;
begin
  Disconnect;
  FDirectoryListing.Free;
  inherited;
end;

end.

