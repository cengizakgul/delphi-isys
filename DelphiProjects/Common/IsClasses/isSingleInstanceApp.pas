unit isSingleInstanceApp;

interface

uses SysUtils, isBaseClasses;

function CheckSingleInstanceApp(const AppID: String; const AGlobal: Boolean = True): Boolean;
function AppIsRunning(const AppID: String; const AGlobal: Boolean = True): Boolean;

implementation

type
  IisSingleInstanceApp = interface
  ['{24CC62DD-4D03-44F9-BD1C-118489275D95}']
    function FirstInstance: Boolean;
  end;

  TisSingleInstanceApp = class(TisInterfacedObject, IisSingleInstanceApp)
  private
    FMutex: TisNamedMutex;
    FFirstInstance: Boolean;
    function FirstInstance: Boolean;
  public
    constructor Create(const AppID: String; const AGlobal: Boolean = True);
    destructor  Destroy; override;
  end;

var
  IDs: IisStringList;

function CheckSingleInstanceApp(const AppID: String; const AGlobal: Boolean = True): Boolean;
var
  i: Integer;
  ID: IisSingleInstanceApp;
begin
  if not Assigned(IDs) then
    IDs := TisStringList.CreateAsIndex;

  i := IDs.IndexOf(AppID);

  if i = -1 then
  begin
    ID := TisSingleInstanceApp.Create(AppID, AGlobal);
    IDs.AddObject(AppID, ID);
  end
  else
    ID := IDs.Objects[i] as IisSingleInstanceApp;

  Result := ID.FirstInstance;
end;

function AppIsRunning(const AppID: String; const AGlobal: Boolean = True): Boolean;
var
  AppMutex: TisNamedMutex;
begin
  AppMutex := TisNamedMutex.Open(AppID, True);
  try
    Result := AppMutex.Handle <> 0;
  finally
    FreeAndNil(AppMutex);
  end;
end;


{ TisSingleInstanceApp }

constructor TisSingleInstanceApp.Create(const AppID: String; const AGlobal: Boolean = True);
begin
  FMutex := TisNamedMutex.Create(AppID, AGlobal);
  FFirstInstance := (FMutex.Handle <> 0) and (FMutex.Error = 0);
end;

destructor TisSingleInstanceApp.Destroy;
begin
  FreeAndNil(FMutex);
  inherited;
end;

function TisSingleInstanceApp.FirstInstance: Boolean;
begin
  Result := FFirstInstance;
end;

end.

