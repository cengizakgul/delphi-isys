unit IsHwInfoClasses;

interface

uses Classes, SysUtils, Windows, Registry, Printers;

type
  TOSInfo = class(TPersistent)
  private
    FVersion: string;
    FOSName: string;
    FSPVersion: string;
    FSubVersion: string;
    FPlatformID: DWord;
  published
    property PlatformID: DWord read FPlatformID write FPlatformID;
    property OSName: string read FOSName write FOSName;
    property Version: string read FVersion write FVersion;
    property SubVersion: string read FSubVersion write FSubVersion;
    property SPVersion: string read FSPVersion write FSPVersion;
  end;

  TProcessorInfo = class(TPersistent)
  private
    FSpeed: Comp;
    FIdentifier: string;
    FVendorIdentifier: string;
  published
    property Identifier: string read FIdentifier write FIdentifier;
    property VendorIdentifier: string read FVendorIdentifier write FVendorIdentifier;
    property Speed: Comp read FSpeed write FSpeed;
  end;

  TProcessorInfoItem = class(TCollectionItem)
  private
    FProcessorInfo: TProcessorInfo;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
  published
    property ProcessorInfo: TProcessorInfo read FProcessorInfo write FProcessorInfo;
  end;

  TProcessors = class(TCollection)
  private
    function GetProcessorInfo(i: Integer): TProcessorInfo;
  public
    constructor Create; reintroduce;
    property ProcessorInfo[i: Integer]: TProcessorInfo read GetProcessorInfo; default;
  end;

  TMemoryInfo = class(TPersistent)
  private
    FFree: Int64;
    FTotal: Int64;
  published
    property TotalAmount: Int64 read FTotal write FTotal;
    property FreeAmount: Int64 read FFree write FFree;
  end;

  TDriveInfo = class(TMemoryInfo)
  private
    FDrive: char;
  published
    property Drive: char read FDrive write FDrive;
  end;

  TDriveInfoItem = class(TCollectionItem)
  private
    FDriveInfo: TDriveInfo;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
  published
    property DriveInfo: TDriveInfo read FDriveInfo write FDriveInfo;
  end;

  TDrives = class(TCollection)
  private
    function GetDriveInfo(i: Integer): TDriveInfo;
  public
    constructor Create; reintroduce;
    property DriveInfo[i: Integer]: TDriveInfo read GetDriveInfo; default;
  end;

  TPrinterInfo = class(TPersistent)
  private
    FDefaultPrinter: Boolean;
    FPrinterName: string;
  published
    property PrinterName: string read FPrinterName write FPrinterName;
    property DefaultPrinter: Boolean read FDefaultPrinter write FDefaultPrinter;
  end;

  TPrinterInfoItem = class(TCollectionItem)
  private
    FPrinterInfo: TPrinterInfo;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
  published
    property PrinterInfo: TPrinterInfo read FPrinterInfo write FPrinterInfo;
  end;

  TPrinters = class(TCollection)
  private
    function GetPrinterInfo(i: Integer): TPrinterInfo;
  public
    constructor Create; reintroduce;
    property PrinterInfo[i: Integer]: TPrinterInfo read GetPrinterInfo; default;
  end;

  TIsHwInfoBase = class(TComponent)
  private
    FOSInfo: TOSInfo;
    FProcessors: TProcessors;
    FPhysicalMemory: TMemoryInfo;
    FPagingFile: TMemoryInfo;
    FFixedDrives: TDrives;
    FPrinters: TPrinters;
  public
    procedure Initialize;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure FillStringList(const List: TStringList);
  published
    property OSInfo: TOSInfo read FOSInfo write FOSInfo;
    property Processors: TProcessors read FProcessors write FProcessors;
    property PhysicalMemory: TMemoryInfo read FPhysicalMemory write FPhysicalMemory;
    property PagingFile: TMemoryInfo read FPagingFile write FPagingFile;
    property FixedDrives: TDrives read FFixedDrives write FFixedDrives;
    property Printers: TPrinters read FPrinters write FPrinters;
  end;

  TIsModuleInfoBase = class(TComponent)
  private
  public
    procedure FillStringList(const List: TStringList);
  end;

implementation

function GetCpuSpeed: Comp;
{ x := Format('%.1f MHz', [GetCpuSpeed]); }
var
  t: Int64;
  mhi, mlo, nhi, nlo: DWORD;
  t0, t1, chi, clo, shr32: Comp;
begin
  shr32 := 65536;
  shr32 := shr32 * 65536;

  t := Windows.GetTickCount;
  while t = Windows.GetTickCount do begin end;
  asm
    DB 0FH
    DB 031H
    mov mhi,edx
    mov mlo,eax
  end;

  while Windows.GetTickCount < (t + 1000) do begin end;
  asm
    DB 0FH
    DB 031H
    mov nhi,edx
    mov nlo,eax
  end;

//{$warnings off}
  chi := mhi;
//  if mhi < 0 then chi := chi + shr32;

  clo := mlo;
//  if mlo < 0 then clo := clo + shr32;

  t0 := chi * shr32 + clo;

  chi := nhi;
//  if nhi < 0 then chi := chi + shr32;

  clo := nlo;
//  if nlo < 0 then clo := clo + shr32;

  t1 := chi * shr32 + clo;

  Result := (t1 - t0) / 1E6;
//{$warnings on}
end;

{ TIsHwInfoBase }

constructor TIsHwInfoBase.Create(AOwner: TComponent);
begin
  inherited;
  OSInfo := TOSInfo.Create;
  Processors := TProcessors.Create;
  PhysicalMemory := TMemoryInfo.Create;
  PagingFile := TMemoryInfo.Create;
  FixedDrives := TDrives.Create;
  Printers := TPrinters.Create;
end;

destructor TIsHwInfoBase.Destroy;
begin
  OSInfo.Free;
  Processors.Free;
  FixedDrives.Free;
  Printers.Free;
  PhysicalMemory.Free;
  PagingFile.Free;
  inherited;
end;

procedure TIsHwInfoBase.FillStringList(const List: TStringList);
var
  Strings: array [0..2] of string;
  
  procedure PushLine;
  begin
    List.Append(Format('%s,%s,%s', [AnsiQuotedStr(Strings[0], '"'), AnsiQuotedStr(Strings[1], '"'), AnsiQuotedStr(Strings[2], '"')]));
    Strings[0] := '';
    Strings[1] := '';
    Strings[2] := '';
  end;
  
var
  s: string;
  i: Integer;
begin
  Strings[0] := '';
  Strings[1] := '';
  Strings[2] := '';

  Strings[0] := 'OS';
  s := '';
  if OSInfo.PlatformId = VER_PLATFORM_WIN32_WINDOWS then
    s := OSInfo.OSName + ' Version ' +
         OSInfo.Version + ' ' +
         OSInfo.SPVersion
  else if OSInfo.PlatformId = VER_PLATFORM_WIN32_NT then
    s := OSInfo.OSName + ' Version ' +
         OSInfo.Version + '.' +
         OSInfo.SubVersion + ' ' +
         OSInfo.SPVersion;
  Strings[1] := s;
  PushLine;

  for i := 0 to Processors.Count - 1 do
    begin
      Strings[0] := 'Processor ' + IntToStr(i);
      s := Format('~%.1f MHz', [Processors[i].Speed])+ ' '+
           Processors[i].Identifier + ' ' +
           Processors[i].VendorIdentifier;
      Strings[1] := s;
      PushLine;
    end;

  Strings[0] := 'Free (Total) Physical Memory';
  Strings[1] := FormatFloat('#,##0', PhysicalMemory.FreeAmount div 1024) + ' (' + FormatFloat('#,##0', PhysicalMemory.TotalAmount div 1024) + ') Kb';
  PushLine;

  Strings[0] := 'Free (Total) Paging File';
  Strings[1] := FormatFloat('#,##0', PagingFile.FreeAmount div 1024) + ' (' + FormatFloat('#,##0', PagingFile.TotalAmount div 1024) + ') Kb';
  PushLine;

  for i := 0 to FixedDrives.Count - 1 do
    begin
      Strings[0] := 'Free (Total) Space Drive ' + FixedDrives[i].Drive;
      Strings[1] := FormatFloat('#,##0', FixedDrives[i].FreeAmount div 1024) +
            ' (' + FormatFloat('#,##0', FixedDrives[i].TotalAmount div 1024) + ') Kb';
      PushLine;
    end;

  for i := 0 to Printers.Count - 1 do
    begin
      Strings[0] := 'Printer ' + IntToStr(i);
      if Printers[i].DefaultPrinter then
        Strings[1] := Printers[i].PrinterName + ' (default)'
      else
        Strings[1] := Printers[i].PrinterName;
      PushLine;
    end;
end;

procedure TIsHwInfoBase.Initialize;
var
  OSI: OSVERSIONINFO;
  Ini: TRegIniFile;
  ld, pd: PChar;
  cfs, ts, fs: Int64;
  MemoryStatus: TMemoryStatus;
  t: TStringList;
  i: Integer;
begin
  OSI.dwOSVersionInfoSize := SizeOf(OSVERSIONINFO);
  GetVersionEx(OSI);
  Ini := TRegIniFile.Create;
  with Ini do
  try
    RootKey:=HKey_Local_Machine;
    Access := KEY_READ;
    OSInfo.PlatformID := OSI.dwPlatformID;
    if OSI.dwPlatformId = VER_PLATFORM_WIN32_WINDOWS then
    begin
      OSInfo.OSName := ReadString('Software\Microsoft\Windows\CurrentVersion', 'ProductName', '');
      OSInfo.Version := ReadString('Software\Microsoft\Windows\CurrentVersion', 'VersionNumber', '');
      OSInfo.SubVersion := '';
      OSInfo.SPVersion := ReadString('Software\Microsoft\Windows\CurrentVersion', 'SubVersionNumber', '');
    end
    else if OSI.dwPlatformId = VER_PLATFORM_WIN32_NT then
    begin
      OSInfo.OSName := ReadString('Software\Microsoft\Windows NT\CurrentVersion', 'ProductName', '');
      OSInfo.Version := ReadString('Software\Microsoft\Windows NT\CurrentVersion', 'CurrentVersion', '');
      OSInfo.SubVersion := ReadString('Software\Microsoft\Windows NT\CurrentVersion', 'CurrentBuildNumber', '');
      OSInfo.SPVersion := ReadString('Software\Microsoft\Windows NT\CurrentVersion', 'CSDVersion', '');
    end;

    t := TStringList.Create;
    try
      t.Sorted := True;
      OpenKey('\Hardware\Description\System\CentralProcessor', False);
      ReadSections(t);
      for i := 0 to t.Count - 1 do
        with TProcessorInfoItem(Processors.Add).ProcessorInfo do
        begin
          Identifier := ReadString(t[i], 'Identifier', '');
          VendorIdentifier := ReadString(t[i], 'VendorIdentifier', '');
          if OSI.dwPlatformId = VER_PLATFORM_WIN32_WINDOWS then
            Speed := GetCpuSpeed
          else if OSI.dwPlatformId = VER_PLATFORM_WIN32_NT then
          begin
            if OpenKey(t[i], False) then
              Speed := TRegistry(Ini).ReadInteger('~MHz')
            else
              Speed := 0;
            OpenKey('\Hardware\Description\System\CentralProcessor', False);
          end;
        end;
    finally
      t.Free;
    end;

  finally
    Free;
  end;

  MemoryStatus.dwLength := SizeOf(MemoryStatus);
  GlobalMemoryStatus(MemoryStatus);
  PhysicalMemory.TotalAmount := MemoryStatus.dwTotalPhys;
  PhysicalMemory.FreeAmount := MemoryStatus.dwAvailPhys;
  PagingFile.TotalAmount := MemoryStatus.dwTotalPageFile;
  PagingFile.FreeAmount := MemoryStatus.dwAvailPageFile;

  GetMem(ld, 4096);
  try
    GetLogicalDriveStrings(4096, ld);
    pd := ld;

    repeat
      if GetDriveType(pd) = DRIVE_FIXED then
        with TDriveInfoItem(FixedDrives.Add).DriveInfo do
        begin
          Drive := pd^;
          GetDiskFreeSpaceEx(pd, cfs, ts, @fs);
          TotalAmount := ts;
          FreeAmount := cfs;
        end;
      pd := StrEnd(pd);
      Inc(pd);
    until pd^ = #0;
  finally
    FreeMem(ld);
  end;

  for i := 0 to Printer.Printers.Count - 1 do
    with TPrinterInfoItem(Printers.Add).PrinterInfo do
    begin
      PrinterName := Printer.Printers[i];
      try
        DefaultPrinter := i = Printer.PrinterIndex;
      except
      end;
    end;
end;

{ TDriveInfoItem }

constructor TDriveInfoItem.Create(Collection: TCollection);
begin
  inherited;
  DriveInfo := TDriveInfo.Create;
end;

destructor TDriveInfoItem.Destroy;
begin
  Driveinfo.Free;
  inherited;
end;

{ TPrinterInfoItem }

constructor TPrinterInfoItem.Create(Collection: TCollection);
begin
  inherited;
  PrinterInfo := TPrinterInfo.Create;
end;

destructor TPrinterInfoItem.Destroy;
begin
  PrinterInfo.Free;
  inherited;
end;

{ TProcessorInfoItem }

constructor TProcessorInfoItem.Create(Collection: TCollection);
begin
  inherited;
  ProcessorInfo := TProcessorInfo.Create;
end;

destructor TProcessorInfoItem.Destroy;
begin
  ProcessorInfo.Free;
  inherited;
end;

{ TProcessors }

constructor TProcessors.Create;
begin
  inherited Create(TProcessorInfoItem);
end;

function TProcessors.GetProcessorInfo(i: Integer): TProcessorInfo;
begin
  Result := (Items[i] as TProcessorInfoItem).ProcessorInfo;
end;

{ TDrives }

constructor TDrives.Create;
begin
  inherited Create(TDriveInfoItem);
end;

function TDrives.GetDriveInfo(i: Integer): TDriveInfo;
begin
  Result := (Items[i] as TDriveInfoItem).DriveInfo;
end;

{ TPrinters }

constructor TPrinters.Create;
begin
  inherited Create(TPrinterInfoItem);
end;

function TPrinters.GetPrinterInfo(i: Integer): TPrinterInfo;
begin
  Result := (Items[i] as TPrinterInfoItem).PrinterInfo;
end;

{ TIsModuleInfoBase }

function ENum(HInstance: Integer; Data: Pointer): Boolean;
  function Iff(const Condition: Boolean; const TruePart, FalsePart: string): string;
  begin
    if Condition then
      Result := TruePart
    else
      Result := FalsePart;
  end;
var
  s: PChar;
  Buffer: array [1..1024] of Char;
  PInfo: Pointer;
  L: Cardinal;
  s1, s2, s3: string;
begin
  Result := True;
  GetMem(s, 2048);
  if GetModuleFileName(HInstance, s, 2048) <> 0 then
  begin
    s1 := AnsiQuotedStr(LowerCase(ExtractFileName(s)), '"');
    s2 := AnsiQuotedStr(LowerCase(ExtractFilePath(s)), '"');
    if GetFileVersionInfo(s, 0, 1024, @Buffer) and VerQueryValue(@Buffer, '\', PInfo, L) then
      s3 := AnsiQuotedStr(
        IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionMS div $10000) + '.' +
        IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionMS mod $10000) + '.' +
        IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionLS div $10000) + '.' +
        IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionLS mod $10000), '"')
    else
      s3 := '';
    TStringList(Data).Append(Format('%s,%s,%s', [s1, s2, s3]));
  end;
  FreeMem(s);
end;

procedure TIsModuleInfoBase.FillStringList(const List: TStringList);
begin
  ENumModules(ENum, List);
end;

initialization
  RegisterClasses([TOSInfo, TProcessors, TMemoryInfo, TDrives, TPrinters]);

finalization
  UnregisterClasses([TOSInfo, TProcessors, TMemoryInfo, TDrives, TPrinters]);

end.
