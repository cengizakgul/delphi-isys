unit IsSMTPClient;

interface

uses Windows, Classes, WinSock, Contnrs, Registry, isExceptions, isBasicUtils,
     EvStreamUtils;

type
  TIsSMTPClientAuthType = (atNone, atPlain, atLogin, atCramMD5, atCramSha1, atAutoSelect);

  TIsSMTPClientAuthTypeSet = set of TIsSMTPClientAuthType;

  TIsMultiPartMixedAttachment = class
  private
    FContentType: string;
    FFileName: string;
    FFileData: IisStream;
  public
    procedure LoadFromFile(const AFileName: string);
    procedure AttachStream(const AFileName: String; const AStream: IisStream);
    property  ContentType: string read FContentType;
    property  FileName: string read FFileName;
    property  FileData: IisStream read FFileData;
  end;


  TIsMultiPartMixedAttachments = class(TObjectList)
  protected
    function  GetItem(Index: Integer): TIsMultiPartMixedAttachment;
    procedure SetItem(Index: Integer; AObject: TIsMultiPartMixedAttachment);
  public
    function  Add: TIsMultiPartMixedAttachment; overload;
    function  Add(AObject: TIsMultiPartMixedAttachment): Integer; overload;
    function  Remove(AObject: TIsMultiPartMixedAttachment): Integer;
    function  IndexOf(AObject: TIsMultiPartMixedAttachment): Integer;
    procedure Insert(Index: Integer; AObject: TIsMultiPartMixedAttachment);
    property  Items[Index: Integer]: TIsMultiPartMixedAttachment read GetItem write SetItem; default;
  end;


  TIsSMTPClientHeader = class
  Private
    FSendTo: String;
    FSender: String;
    FMessageID: String;
    Fbcc: String;
    FContentTransferEncoding: String;
    FComments: String;
    FMIMEVersion: String;
    FPriority: String;
    FReplyTo: String;
    FSubject: String;
    FFrom: String;
    FDate: String;
    FDispositionNotificationTo: String;
    FReferences: String;
    Fcc: String;
    FContentType: String;
    FCustomHeaders: TStrings;
    function  GetRawHeaderText: String;
    procedure SetRawHeaderText(const aRawHeaderText: string);
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure Clear;

    property From: String read FFrom write FFrom;
    property Sender: String read FSender write FSender;
    property SendTo: String read FSendTo write FSendTo;
    property cc: String read Fcc write Fcc;
    property bcc: String read Fbcc write Fbcc;
    property ReplyTo: String read FReplyTo write FReplyTo;
    property Subject: String read FSubject write FSubject;
    property MessageID: String read FMessageID write FMessageID;
    property References: String read FReferences write FReferences;
    property Comments: String read FComments write FComments;
    property Date: String read FDate write FDate;
    property ContentType: String read FContentType write FContentType;
    property ContentTransferEncoding: String read FContentTransferEncoding write FContentTransferEncoding;
    property MIMEVersion: String read FMIMEVersion write FMIMEVersion;
    property Priority: String read FPriority write FPriority;
    property DispositionNotificationTo: String read FDispositionNotificationTo write FDispositionNotificationTo;
    property CustomHeaders: Tstrings read FCustomHeaders;
    Property RawHeaderText: String read GetRawHeaderText write SetRawHeaderText;
  end;

  TArrayOfString = array of string;

  TIsSMTPClient = class
  private
    FHost: String;
    FPort: Integer;
    FUser: String;
    FPassword: String;
    FAuthType: TIsSMTPClientAuthType;

    FWSAData: TWSAData;
    Fconnected: Boolean;
    FSocketDescriptor: Integer;
    FAuthTypesSupported: TIsSMTPClientAuthTypeSet;
    Ftimeout: Integer;

    procedure CheckError(Error: Boolean);
    function  SendCmd(aCmd:String; OkResponses: array of Word): String;
    function  GetResponse(OkResponses: array of Word): String;
    function  SocketWrite(var Buffer; Count: Longint): Longint;
    function  SocketRead(var Buffer; Count: Longint): Longint;
    function  MakePath(const APath: String): String;
    function  DelimitedStrToArray(const AStr: String): TArrayOfString;
    function  GetDataMultipartMixed(aHeader: TIsSMTPClientHeader; aInlineText, aInlineTextContentType: String; aAttachments: TIsMultiPartMixedAttachments): String;

    function  GetAuthTypeFromEhloResponse(EhloResponse: string): TIsSMTPClientAuthTypeSet;
    procedure DoSendMail(const AFrom: String; const ARcptLst: TArrayOfString; const AMailData: String);

  protected
    function  Connect(const aHost: String; APort: Integer): String;
    procedure Disconnect;
    function  Helo: String;
    function  Ehlo: String;
    function  Auth(const AUserName, APassword: String; aAuthType: TIsSMTPClientAuthType): String;
    function  Vrfy(const aUserName: String): String;
    function  MailFrom(const AFromName: String): String;
    function  RcptTo(const ARcptNameLst: TArrayOfString): String;
    function  Data(aMailData: String): String;
    function  Quit: String;
    function  Rset: String;
  public
    constructor Create(const AHost: String; const APort: Integer = 25;
                       const AUser: String = ''; const APassword: String = '';
                       const AAuthType: TIsSMTPClientAuthType = atNone);
    destructor  Destroy; override;

    procedure SendQuickMessage(const AFrom, ASendTo, ASubject, ABody: String);
    procedure SendQuickAttach(const AFrom, ASendTo, ASubject, ABody, AFileName: String);
    procedure SendMailMultipartMixed(const AFrom: String; const ASendTo: String;
                                     const AHeader: TIsSMTPClientHeader;
                                     const AInlineText, AInlineTextContentType: String;
                                     const AAttachments: TIsMultiPartMixedAttachments);
  end;

function IsGetDefaultMIMEContentTypeFromExt(aExt: String): String;

implementation

uses SysUtils,
     SysConst,
     IniFiles;

var vIsMimeContentTypeByExtList: Tstrings; {.htm=text/html}
    vIsExtbyMimeContentTypeList: Tstrings; {text/html=.htm}

type
  PALMimeBase64Byte4 = ^TALMimeBase64Byte4;
  TALMimeBase64Byte4 = packed record
    b1: Byte;
    b2: Byte;
    b3: Byte;
    b4: Byte;
  end;

  PALMimeBase64Byte3 = ^TALMimeBase64Byte3;

  TALMimeBase64Byte3 = packed record
    b1: Byte;
    b2: Byte;
    b3: Byte;
  end;


const cALMime_Base64_Buffer_Size = $3000;
      cALMime_Base64_Encoded_Line_Break = 76;
      cALMime_Base64_Decoded_Line_Break = cALMime_Base64_Encoded_Line_Break div 4 * 3;
      cALMime_Base64_Encode_Table: array[0..63] of Byte =
      (
        065, 066, 067, 068, 069, 070, 071, 072, //  00 - 07
        073, 074, 075, 076, 077, 078, 079, 080, //  08 - 15
        081, 082, 083, 084, 085, 086, 087, 088, //  16 - 23
        089, 090, 097, 098, 099, 100, 101, 102, //  24 - 31
        103, 104, 105, 106, 107, 108, 109, 110, //  32 - 39
        111, 112, 113, 114, 115, 116, 117, 118, //  40 - 47
        119, 120, 121, 122, 048, 049, 050, 051, //  48 - 55
        052, 053, 054, 055, 056, 057, 043, 047  // 56 - 63
       );
      cALMime_Base64_Pad_Char = Byte('=');
      cAlMime_Base64_Decode_Table: array[Byte] of Cardinal =
      (
        255, 255, 255, 255, 255, 255, 255, 255, //   0 -   7
        255, 255, 255, 255, 255, 255, 255, 255, //   8 -  15
        255, 255, 255, 255, 255, 255, 255, 255, //  16 -  23
        255, 255, 255, 255, 255, 255, 255, 255, //  24 -  31
        255, 255, 255, 255, 255, 255, 255, 255, //  32 -  39
        255, 255, 255, 062, 255, 255, 255, 063, //  40 -  47
        052, 053, 054, 055, 056, 057, 058, 059, //  48 -  55
        060, 061, 255, 255, 255, 255, 255, 255, //  56 -  63
        255, 000, 001, 002, 003, 004, 005, 006, //  64 -  71
        007, 008, 009, 010, 011, 012, 013, 014, //  72 -  79
        015, 016, 017, 018, 019, 020, 021, 022, //  80 -  87
        023, 024, 025, 255, 255, 255, 255, 255, //  88 -  95
        255, 026, 027, 028, 029, 030, 031, 032, //  96 - 103
        033, 034, 035, 036, 037, 038, 039, 040, // 104 - 111
        041, 042, 043, 044, 045, 046, 047, 048, // 112 - 119
        049, 050, 051, 255, 255, 255, 255, 255, // 120 - 127
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255
     );

type
  TIsMultipartMixedEncoder = class
  private
    FStream: IisStream;
    FBoundary: string;
    FTopHeaderContentType: string;

    function  GenerateUniqueBoundary: string;
    procedure AddInlineText(const ContentType, Text: string);
    procedure AddAttachment(const FileName, ContentType: string; const AFileData: IisStream);
    procedure CloseBoundary;
    procedure Encode(InlineText, InlineTextContentType: String; Attachments: TIsMultiPartMixedAttachments);

    property  Boundary: string read FBoundary;
    property  TopHeaderContentType: string read FTopHeaderContentType;
    property  Stream: IisStream read FStream;
  public
    constructor	Create;
  end;


procedure IsMimeBase64EncodeNoCRLF(const InputBuffer; const InputByteCount: Cardinal; out OutputBuffer);
var B, InnerLimit, OuterLimit: Cardinal;
    InPtr: PALMimeBase64Byte3;
    OutPtr: PALMimeBase64Byte4;
begin
  if InputByteCount = 0 then Exit;

  InPtr := @InputBuffer;
  OutPtr := @OutputBuffer;

  OuterLimit := InputByteCount div 3 * 3;

  InnerLimit := Cardinal(InPtr);
  Inc(InnerLimit, OuterLimit);

  { Last line loop. }
  while Cardinal(InPtr) < InnerLimit do begin
    { Read 3 bytes from InputBuffer. }
    B := InPtr^.b1;
    B := B shl 8;
    B := B or InPtr^.b2;
    B := B shl 8;
    B := B or InPtr^.b3;
    Inc(InPtr);
    { Write 4 bytes to OutputBuffer (in reverse order). }
    OutPtr^.b4 := CALMIME_Base64_ENCODE_TABLE[B and $3F];
    B := B shr 6;
    OutPtr^.b3 := CALMIME_Base64_ENCODE_TABLE[B and $3F];
    B := B shr 6;
    OutPtr^.b2 := CALMIME_Base64_ENCODE_TABLE[B and $3F];
    B := B shr 6;
    OutPtr^.b1 := CALMIME_Base64_ENCODE_TABLE[B];
    Inc(OutPtr);
  end;

  { End of data & padding. }
  case InputByteCount - OuterLimit of
    1:
      begin
        B := InPtr^.b1;
        B := B shl 4;
        OutPtr.b2 := CALMIME_Base64_ENCODE_TABLE[B and $3F];
        B := B shr 6;
        OutPtr.b1 := CALMIME_Base64_ENCODE_TABLE[B];
        OutPtr.b3 := CALMIME_Base64_PAD_CHAR; { Pad remaining 2 bytes. }
        OutPtr.b4 := CALMIME_Base64_PAD_CHAR;
      end;
    2:
      begin
        B := InPtr^.b1;
        B := B shl 8;
        B := B or InPtr^.b2;
        B := B shl 2;
        OutPtr.b3 := CALMIME_Base64_ENCODE_TABLE[B and $3F];
        B := B shr 6;
        OutPtr.b2 := CALMIME_Base64_ENCODE_TABLE[B and $3F];
        B := B shr 6;
        OutPtr.b1 := CALMIME_Base64_ENCODE_TABLE[B];
        OutPtr.b4 := CALMIME_Base64_PAD_CHAR; { Pad remaining byte. }
      end;
  end;
end;

procedure IsMimeBase64EncodeFullLines(const InputBuffer; const InputByteCount: Cardinal; out OutputBuffer);
var
  B, InnerLimit, OuterLimit: Cardinal;
  InPtr: PALMimeBase64Byte3;
  OutPtr: PALMimeBase64Byte4;
begin
  { Do we have enough input to encode a full line? }
  if InputByteCount < CALMIME_Base64_DECODED_LINE_BREAK then Exit;

  InPtr := @InputBuffer;
  OutPtr := @OutputBuffer;

  InnerLimit := Cardinal(InPtr);
  Inc(InnerLimit, CALMIME_Base64_DECODED_LINE_BREAK);

  OuterLimit := Cardinal(InPtr);
  Inc(OuterLimit, InputByteCount);

  { Multiple line loop. }
  repeat
    { Single line loop. }
    repeat
      { Read 3 bytes from InputBuffer. }
      B := InPtr^.b1;
      B := B shl 8;
      B := B or InPtr^.b2;
      B := B shl 8;
      B := B or InPtr^.b3;
      Inc(InPtr);
      { Write 4 bytes to OutputBuffer (in reverse order). }
      OutPtr^.b4 := CALMIME_Base64_ENCODE_TABLE[B and $3F];
      B := B shr 6;
      OutPtr^.b3 := CALMIME_Base64_ENCODE_TABLE[B and $3F];
      B := B shr 6;
      OutPtr^.b2 := CALMIME_Base64_ENCODE_TABLE[B and $3F];
      B := B shr 6;
      OutPtr^.b1 := CALMIME_Base64_ENCODE_TABLE[B];
      Inc(OutPtr);
    until Cardinal(InPtr) >= InnerLimit;

    { Write line break (CRLF). }
    OutPtr^.b1 := 13;
    OutPtr^.b2 := 10;
    Inc(Cardinal(OutPtr), 2);

    Inc(InnerLimit, CALMIME_Base64_DECODED_LINE_BREAK);
  until InnerLimit > OuterLimit;
end;

procedure IsMimeBase64Encode(const InputBuffer; const InputByteCount: Cardinal; out OutputBuffer);
var IDelta, ODelta: Cardinal;
begin
  IsMimeBase64EncodeFullLines(InputBuffer, InputByteCount, OutputBuffer);
  IDelta := InputByteCount div CALMIME_Base64_DECODED_LINE_BREAK; // Number of lines processed so far.
  ODelta := IDelta * (CALMIME_Base64_ENCODED_LINE_BREAK + 2);
  IDelta := IDelta * CALMIME_Base64_DECODED_LINE_BREAK;
  IsMimeBase64EncodeNoCRLF(Pointer(Cardinal(@InputBuffer) + IDelta)^, InputByteCount - IDelta, Pointer(Cardinal(@OutputBuffer) + ODelta)^);
end;

function IsMimeBase64EncodedSize(const InputSize: Cardinal): Cardinal;
begin
  if InputSize > 0 then Result := (InputSize + 2) div 3 * 4 + (InputSize - 1) div cALMIME_Base64_DECODED_LINE_BREAK * 2
  else Result := InputSize;
end;

function IsMimeBase64EncodeString(const S: AnsiString): AnsiString;
var L: Cardinal;
begin
  if Pointer(S) <> nil then begin
    L := PCardinal(Cardinal(S) - 4)^;
    SetLength(Result, IsMimeBase64EncodedSize(L));
    IsMimeBase64Encode(Pointer(S)^, L, Pointer(Result)^);
  end
  else Result := '';
end;

function IsMimeBase64EncodedSizeNoCRLF(const InputSize: Cardinal): Cardinal;
begin
  Result := (InputSize + 2) div 3 * 4;
end;

function IsMimeBase64EncodeStringNoCRLF(const S: AnsiString): AnsiString;
var L: Cardinal;
begin
  if Pointer(S) <> nil then begin
    L := PCardinal(Cardinal(S) - 4)^;
    SetLength(Result, IsMimeBase64EncodedSizeNoCRLF(L));
    IsMimeBase64EncodeNoCRLF(Pointer(S)^, L, Pointer(Result)^);
  end
  else Result := '';
end;

procedure IsMimeBase64EncodeStream(const InputStream: TStream; const OutputStream: TStream);
var InputBuffer: array [0..CALMIME_Base64_BUFFER_SIZE - 1] of Byte;
    OutputBuffer: array [0..((CALMIME_Base64_BUFFER_SIZE + 2) div 3) * 4 - 1] of Byte;
    BytesRead: Integer;
begin
  BytesRead := InputStream.Read(InputBuffer, SizeOf(InputBuffer));
  while BytesRead > 0 do begin
    IsMimeBase64Encode(InputBuffer, BytesRead, OutputBuffer);
    OutputStream.Write(OutputBuffer, IsMimeBase64EncodedSize(BytesRead));
    BytesRead := InputStream.Read(InputBuffer, SizeOf(InputBuffer));
  end;
end;

function IsHostToIP(HostName: string; var Ip: string): Boolean;
var
  WSAData : TWSAData;
  hostEnt : PHostEnt;
  addr : PChar;
begin
  WSAData.wVersion := 0;
  WSAStartup (MAKEWORD(2,2), WSAData);
  try
    hostEnt := gethostbyname ( Pchar(hostName));
    if Assigned (hostEnt) then begin
      if Assigned (hostEnt^.h_addr_list) then begin
        addr := hostEnt^.h_addr_list^;
        if Assigned (addr) then begin
          IP := Format ('%d.%d.%d.%d', [byte (addr [0]),
          byte (addr [1]), byte (addr [2]), byte (addr [3])]);
          Result := True;
        end
        else Result := False;
      end
      else Result := False
    end
    else Result := False;

  finally
    if WSAData.wVersion = 2 then WSACleanup;
  end
end;

function  IsGetLocalHostName: string;
var Buffer: array [0..255] of char;
    WSAData: TWSAData;
begin
  WSAData.wVersion := 0;
  WSAStartup(MAKEWORD(2,2), WSAData);
  Try

    if gethostname(Buffer, SizeOf(Buffer)) <> 0 then
        raise EisException.Create('Winsock GetHostName failed');
    Result := StrPas(Buffer);

  finally
    if WSAData.wVersion = 2 then WSACleanup;
  end;
end;

const
  Rfc822DaysOfWeek: array[1..7] of string =
  ('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');

  Rfc822MonthNames: array[1..12] of string =
  ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

{aValue is a GMT TDateTime - result is "Sun, 06 Nov 1994 08:49:37 GMT"}
function  GMTDateTimeToRfc822Str(const aValue: TDateTime): String;
var
  aDay, aMonth, aYear: Word;
begin
  DecodeDate(aValue, aYear, aMonth, aDay);
  Result := Format
  ('%s, %.2d %s %.4d %s %s',
   [
    Rfc822DaysOfWeek[DayOfWeek(aValue)],
    aDay,
    Rfc822MonthNames[aMonth],
    aYear,
    FormatDateTime('hh":"nn":"ss', aValue),
    'GMT'
   ]);
end;

{aValue is a Local TDateTime - result is "Sun, 06 Nov 1994 08:49:37 GMT"}
function IsDateTimeToRfc822Str(const aValue: TDateTime): String;

  function InternalCalcTimeZoneBias : TDateTime;
  const Time_Zone_ID_DayLight = 2;
  var TZI: TTimeZoneInformation;
      TZIResult: Integer;
      aBias : Integer;
  begin
    TZIResult := GetTimeZoneInformation(TZI);
    if TZIResult = -1 then Result := 0
    else begin
      if TZIResult = Time_Zone_ID_DayLight then aBias := TZI.Bias + TZI.DayLightBias
      else aBias := TZI.Bias + TZI.StandardBias;
      Result := EncodeTime(Abs(aBias) div 60, Abs(aBias) mod 60, 0, 0);
      if aBias < 0 then Result := -Result;
    end;
  end;

begin
  Result := GMTDateTimeToRfc822Str(aValue + InternalCalcTimeZoneBias);
end;

function  IsMakeKeyStrByGUID: String;
var
  aGUID: TGUID;
begin
  CreateGUID(aGUID);
  Result := GUIDToString(aGUID);
  Delete(Result,1,1);
  Delete(Result,Length(Result),1);
end;

function IsSMTPClientGenerateMessageID: String;
Begin
  Result := StringReplace(IsMakeKeyStrByGUID,'-','',[rfReplaceAll]) + '@' + IsGetLocalHostName;
end;

procedure TIsSMTPClientHeader.Clear;
begin
  FSendTo := '';
  FSender := '';
  FMessageID := '';
  Fbcc := '';
  FContentTransferEncoding := '';
  FComments := '';
//  FMIMEVersion := '';
  FMIMEVersion := '1.0';
  FPriority := '';
  FReplyTo := '';
  FSubject := '';
  FFrom := '';
  FDate := '';
  FDispositionNotificationTo := '';
  FReferences := '';
  Fcc := '';
  FContentType := '';
  FCustomHeaders.Clear;
end;

constructor TIsSMTPClientHeader.Create;
begin
  inherited create;
  FCustomHeaders:= TStringList.create;
  FCustomHeaders.NameValueSeparator := ':';
  Clear;
  FMessageID := 'AUTO';
  FMIMEVersion := '1.0';
  FDate := 'NOW';
  FContentType := 'text/plain';
end;

destructor TIsSMTPClientHeader.Destroy;
begin
  FCustomHeaders.free;
  inherited;
end;

function TIsSMTPClientHeader.GetRawHeaderText: String;
var
  i : Integer;
  Str: String;
begin
  Result := '';
  if Trim(FFrom) <> '' then Result := Result + 'From: ' + trim(fFrom) + #13#10;
  if Trim(FSender) <> '' then Result := Result + 'Sender: ' + trim(fSender) + #13#10;
  if Trim(FSendTo) <> '' then Result := Result + 'To: ' + trim(fSendTo) + #13#10;
  if Trim(Fcc) <> '' then Result := Result + 'cc: ' + trim(fcc) + #13#10;
  if Trim(Fbcc) <> '' then Result := Result + 'bcc: ' + trim(fbcc) + #13#10;
  if Trim(FReplyTo) <> '' then Result := Result + 'Reply-To: ' + trim(fReplyTo) + #13#10;
  if Trim(FSubject) <> '' then Result := Result + 'Subject: ' + trim(fSubject) + #13#10;
  Str := FMessageID;
  if Trim(str) <> '' then begin
    if sametext(Str, 'AUTO') then Str := '<' + IsSMTPClientGenerateMessageID + '>';
    Result := Result + 'Message-ID: ' + Trim(str) + #13#10;
  end;
  if Trim(FReferences) <> '' then Result := Result + 'References: ' + Trim(fReferences) + #13#10;
  if Trim(FComments) <> '' then Result := Result + 'Comments: ' + Trim(fComments) + #13#10;
  Str := FDate;
  if Trim(str) <> '' then begin
    if sametext(Str, 'NOW') then Str := IsDateTimeToRfc822Str(Now);
    Result := Result + 'Date: ' + Trim(str) + #13#10;
  end;
  if Trim(FContentType) <> '' then Result := Result + 'Content-Type: ' + trim(fContentType) + #13#10;
  if Trim(FContentTransferEncoding) <> '' then Result := Result + 'Content-Transfer-Encoding: ' + trim(fContentTransferEncoding) + #13#10;
  if Trim(FMIMEVersion) <> '' then Result := Result + 'MIME-Version: ' + trim(fMIMEVersion) + #13#10;
  if Trim(FPriority) <> '' then Result := Result + 'Priority: ' + trim(fPriority) + #13#10;
  if Trim(FDispositionNotificationTo) <> '' then Result := Result + 'Disposition-Notification-To: ' + trim(fDispositionNotificationTo) + #13#10;
  for i := 0 to FCustomHeaders.Count - 1 do
    if (Trim(FCustomHeaders.Names[i]) <> '') and (trim(FCustomHeaders.ValueFromIndex[i]) <> '') then
      Result := Result + FCustomHeaders.names[i] + ': ' + trim(FCustomHeaders.ValueFromIndex[i]) + #13#10;
end;

procedure TIsSMTPClientHeader.SetRawHeaderText(const aRawHeaderText: string);
var
  aRawHeaderLst: TstringList;

  function Alg001(aName: String): String;
  var i: Integer;
      Str: String;
  Begin
    I := aRawHeaderLst.IndexOfName(aName);
    If I >= 0 then Begin
      Result := Trim(aRawHeaderLst.ValueFromIndex[i]);
      aRawHeaderLst.Delete(i);
      while True do begin
        if i >= aRawHeaderLst.Count then break;
        str := aRawHeaderLst[i];
        if (str = '') or (not (str[1] in [' ',#9])) then break; //(1) an empty line or (2) a line that does not start with a space, a tab, or a field name followed by a colon
        Result := trim(Result + ' ' + trim(str));
        aRawHeaderLst.Delete(i);
      end;
    end
    else Result := '';
  end;

var
  Str1, Str2: String;
  j: Integer;
begin
  aRawHeaderLst := TstringList.create;
  try
    aRawHeaderLst.NameValueSeparator := ':';
    aRawHeaderLst.Text := aRawHeaderText;

    fFrom:= Alg001('From');
    fSender:= Alg001('Sender');
    fSendTo:= Alg001('To');
    fcc:= Alg001('cc');
    fbcc:= Alg001('bcc');
    fReplyTo:= Alg001('Reply-To');
    fSubject:= Alg001('Subject');
    fMessageID:= Alg001('Message-ID');
    fReferences:= Alg001('References');
    fComments:= Alg001('Comments');
    fDate:= Alg001('Date');
    fContentType:= Alg001('Content-Type');
    fContentTransferEncoding:= Alg001('Content-Transfer-Encoding');
    fMIMEVersion:= Alg001('MIME-Version');
    fPriority:= Alg001('Priority');
    fDispositionNotificationTo:= Alg001('Disposition-Notification-To');

    FCustomHeaders.clear;
    J := 0;
    while j <= aRawHeaderLst.count - 1 do begin
      Str1 := trim(aRawHeaderLst.Names[j]);
      If (trim(str1) <> '') and (not (str1[1] in [' ',#9])) then begin
        Str1 := trim(Str1) + ': ' + trim(aRawHeaderLst.ValueFromIndex[j]);
        inc(j);
        While True do begin
          If j >= aRawHeaderLst.Count then break;
          str2 := aRawHeaderLst[j];
          If (str2 = '') or
             (not (str2[1] in [' ',#9])) then break; //(1) an empty line or (2) a line that does not start with a space, a tab, or a field name followed by a colon
          Str1 := trim(Str1 + ' ' + trim(str2));
          inc(j);
        end;
        FCustomHeaders.Add(Str1);
      end
      else inc(j);
    end;

  finally
    aRawHeaderLst.Free;
  end;
end;

constructor TIsSMTPClient.Create(const AHost: String; const APort: Integer = 25;
  const AUser: String = ''; const APassword: String = '';
  const AAuthType: TIsSMTPClientAuthType = atNone);
begin
  FHost := AHost;
  FPort := APort;
  FUser := AUser;
  FPassword := APassword;
  FAuthType := AAuthType;

  FWSAData.wVersion := 0;
  Fconnected:= False;
  FSocketDescriptor:= INVALID_SOCKET;
  FAuthTypesSupported:= [];
  Ftimeout:= 60000;
  Randomize;
end;

destructor TIsSMTPClient.Destroy;
begin
  If Fconnected then Disconnect;
  inherited;
end;

procedure TIsSMTPClient.CheckError(Error: Boolean);
var ErrCode: Integer;
    S: string;
begin
  ErrCode := WSAGetLastError;
  if Error and (ErrCode <> 0) then begin
    Case ErrCode Of
      WSAEINTR: S := 'Interrupted function call';
      WSAEACCES: S := 'Permission denied';
      WSAEFAULT: S := 'Bad address';
      WSAEINVAL: S := 'Invalid argument';
      WSAEMFILE: S := 'Too many open files';
      WSAEWOULDBLOCK: S := 'Resource temporarily unavailable';
      WSAEINPROGRESS: S := 'Operation now in progress';
      WSAEALREADY: S := 'Operation already in progress';
      WSAENOTSOCK: S := 'Socket operation on nonsocket';
      WSAEDESTADDRREQ: S := 'Destination address required';
      WSAEMSGSIZE: S := 'Message too long';
      WSAEPROTOTYPE: S := 'Protocol wrong type for socket';
      WSAENOPROTOOPT: S := 'Bad protocol option';
      WSAEPROTONOSUPPORT: S := 'Protocol not supported';
      WSAESOCKTNOSUPPORT: S := 'Socket type not supported';
      WSAEOPNOTSUPP: S := 'Operation not supported';
      WSAEPFNOSUPPORT: S := 'Protocol family not supported';
      WSAEAFNOSUPPORT: S := 'Address family not supported by protocol family';
      WSAEADDRINUSE: S := 'Address already in use';
      WSAEADDRNOTAVAIL: S := 'Cannot assign requested address';
      WSAENETDOWN: S := 'Network is down';
      WSAENETUNREACH: S := 'Network is unreachable';
      WSAENETRESET: S := 'Network dropped connection on reset';
      WSAECONNABORTED: S := 'Software caused connection abort';
      WSAECONNRESET: S := 'Connection reset by peer';
      WSAENOBUFS: S := 'No buffer space available';
      WSAEISCONN: S := 'Socket is already connected';
      WSAENOTCONN: S := 'Socket is not connected';
      WSAESHUTDOWN: S := 'Cannot send after socket shutdown';
      WSAETIMEDOUT: S := 'Connection timed out';
      WSAECONNREFUSED: S := 'Connection refused';
      WSAEHOSTDOWN: S := 'Host is down';
      WSAEHOSTUNREACH: S := 'No route to host';
      WSAEPROCLIM: S := 'Too many processes';
      WSASYSNOTREADY: S := 'Network subsystem is unavailable';
      WSAVERNOTSUPPORTED: S := 'Winsock.dll version out of range';
      WSANOTINITIALISED: S := 'Successful WSAStartup not yet performed';
      WSAEDISCON: S := 'Graceful shutdown in progress';
      WSAHOST_NOT_FOUND: S := 'Host not found';
      WSATRY_AGAIN: S := 'Nonauthoritative host not found';
      WSANO_RECOVERY: S := 'This is a nonrecoverable error';
      WSANO_DATA: S := 'Valid name, no data record of requested type';
      else Begin
        SetLength(S, 256);
        FormatMessage(
                      FORMAT_MESSAGE_FROM_SYSTEM or FORMAT_MESSAGE_FROM_HMODULE,
                      Pointer(GetModuleHandle('wsock32.dll')),
                      ErrCode,
                      0,
                      PChar(S),
                      Length(S),
                      nil
                     );
        SetLength(S, StrLen(PChar(S)));
        while (Length(S) > 0) and (S[Length(S)] in [#10, #13]) do SetLength(S, Length(S) - 1);
      end;
    end;
     raise EisException.CreateFmt('%s (Error code:%s)', [S, inttostr(ErrCode)]);      { Do not localize }
  end;
end;

function TIsSMTPClient.Connect(const aHost: String; APort: Integer): String;

  procedure CallServer(Server:string; Port:word);
  var
    SockAddr: Sockaddr_in;
    IP: String;
  begin
    FSocketDescriptor := Socket(AF_INET,SOCK_STREAM,IPPROTO_IP);
    CheckError(FSocketDescriptor = INVALID_SOCKET);
    FillChar(SockAddr,SizeOf(SockAddr),0);
    SockAddr.sin_family := AF_INET;
    SockAddr.sin_port := swap(Port);
    SockAddr.sin_addr.S_addr := inet_addr(Pchar(Server));
    if SockAddr.sin_addr.S_addr = INADDR_NONE then
    begin
      checkError(IsHostToIP(Server, IP));
      SockAddr.sin_addr.S_addr := inet_addr(Pchar(IP));
    end;
    CheckError(WinSock.Connect(FSocketDescriptor,SockAddr,SizeOf(SockAddr)) = SOCKET_ERROR);
  end;

begin
  if FConnected then
    raise EisException.Create('SMTP component already connected');

  try
    WSAStartup (MAKEWORD(2,2), FWSAData);
    Fconnected := True;  //Aleksey: Otherwise Disconnect won't work

    CallServer(aHost,aPort);
    CheckError(setsockopt(FSocketDescriptor,SOL_SOCKET,SO_RCVTIMEO,PChar(@FTimeOut),SizeOf(Integer))=SOCKET_ERROR);
    CheckError(setsockopt(FSocketDescriptor,SOL_SOCKET,SO_SNDTIMEO,PChar(@FTimeOut),SizeOf(Integer))=SOCKET_ERROR);
    Result := GetResponse([220]);
    FAuthTypesSupported := [];
  except
    Disconnect;
    raise;
  end;
end;

procedure TIsSMTPClient.Disconnect;
begin
  If Fconnected then
  begin
    if FSocketDescriptor <> INVALID_SOCKET then
    begin
      ShutDown(FSocketDescriptor,SD_BOTH);
      CloseSocket(FSocketDescriptor);
      FSocketDescriptor := INVALID_SOCKET;
    end;

    if FWSAData.wVersion = 2 then
      WSACleanup;
    FWSAData.wVersion := 0;

    Fconnected := False;
  end;
end;

{EhloResponse is like:
 250-ec-is.net Hello your_name, ravi de vous rencontrer
 250-VRFY
 250-ETRN
 250-AUTH=LOGIN
 250-AUTH LOGIN CRAM-MD5
 250-8BITMIME
 250 SIZE 0}
function TIsSMTPClient.GetAuthTypeFromEhloResponse(EhloResponse: string): TIsSMTPClientAuthTypeSet;
var
  k, J: Integer;
  Str1, Str2: String;
  Lst: TStringlist;
begin
  Result := [];
  Lst := TstringList.Create;
  Try
    Lst.Text := UpperCase(Trim(EhloResponse));
    For j := 0 to Lst.Count - 1 do begin
      Str1 := trim(Lst[J]);  //250-AUTH=LOGIN
      Delete(Str1, 1, 4); //AUTH=LOGIN
      Str2 := copy(Str1, 1, 5); //AUTH=
      if (str2='AUTH ') or (Str2='AUTH=') then begin
        Str1 := copy(Str1, 6, maxint); //LOGIN
        Str1 := StringReplace(Str1, '=', ' ', [rfReplaceAll]); //LOGIN
        while (str1 <> '') do begin
          K := Pos(' ', Str1);
          if K <= 0 then begin
            Str2 := trim(Str1);
            Str1 := '';
          end
          else begin
            Str2 := Trim(copy(Str1, 1, k - 1));
            Delete(Str1, 1, k);
          end;

          if Str2 = ('PLAIN') then Result := Result + [atPlain]
          else if Str2 = ('LOGIN') then Result := Result + [atLogin]
          else if Str2 = ('CRAM-MD5') then Result := Result + [atCramMD5]
          else if Str2 = ('CRAM-SHA1') then Result := Result + [atCramSHA1];

        end;
      end;
    end;
  finally
    Lst.free;
  end;
end;

function TIsSMTPClient.Helo: String;
begin
  Result := SendCmd('HELO '+IsGetLocalHostName,[250]);
end;

function TIsSMTPClient.Ehlo: String;
begin
  Result := SendCmd('EHLO '+IsGetLocalHostName,[250]);
  FAuthTypesSupported := GetAuthTypeFromEhloResponse(Result);
end;

function TIsSMTPClient.MailFrom(const AFromName: String): String;
begin
  Result := SendCmd('MAIL From:' + MakePath(AFromName), [250]);
end;

function TIsSMTPClient.Auth(const AUserName, APassword: String; aAuthType: TIsSMTPClientAuthType): String;

  function InternalDoAuthPlain: String;
  var aAuthPlain : String;
  begin
    If aUserName='' then raise EisException.Create('UserName is empty');
    If aPassword='' then raise EisException.Create('Password is empty');
    aAuthPlain := IsMimeBase64EncodeStringNoCRLF(aUserName + #0 + aUserName + #0 + aPassword);
    Result := SendCmd('AUTH PLAIN ' + aAuthPlain,[235]);
  end;

  function InternalDoAuthLogin: String;
  begin
    If aUserName='' then raise EisException.Create('UserName is empty');
    If aPassword='' then raise EisException.Create('Password is empty');
    SendCmd('AUTH LOGIN',[334]);
    SendCmd(IsMimeBase64EncodeStringNoCRLF(aUsername),[334]);
    Result := SendCmd(IsMimeBase64EncodeStringNoCRLF(aPassword),[235]);
  end;

var
  tmpAuthType: TIsSMTPClientAuthType;
begin
  if AUserName = '' then
    aAuthType := atNone;

  if aAuthType = atAutoSelect then begin
    if atPlain in FAuthTypesSupported then
      tmpAuthType := atPlain
    else if atLogin in FAuthTypesSupported then
      tmpAuthType := atLogin
    else if atCramMD5 in FAuthTypesSupported then
      tmpAuthType := atCramMD5
    else if atCramSHA1 in FAuthTypesSupported then
      tmpAuthType := atCramSHA1
    else
      tmpAuthType := atNone
  end
  else
    tmpAuthType := aAuthType;

  case tmpAuthType of
    atPlain:    Result := InternalDoAuthPlain;
    atLogin:    Result := InternalDoAuthLogin;
    atCramMD5:  raise EisException.Create('CRAM-MD5 Authentication is not supported yet!');
    atCramSHA1: raise EisException.Create('CRAM-SHA1 Authentication is not supported yet!');
  end;

end;

function TIsSMTPClient.RcptTo(const ARcptNameLst: TArrayOfString): String;
var
  i: Integer;
  Path, Cmd: String;
begin
  Result := '';

  if Length(ARcptNameLst) = 0 then
    raise EisException.Create('RcptName list is empty');

  for i := Low(ARcptNameLst) to High(ARcptNameLst) do
  begin
    Path := MakePath(ARcptNameLst[i]);
    if Path <> '' then
    begin
      Cmd := 'RCPT To:' + Path;
      AddStrValue(Result, SendCmd(Cmd,[250, 251]), #13#10);
    end
  end;
end;

function TIsSMTPClient.Data(aMailData: String): String;
var I : Integer;
begin
  SendCmd('DATA',[354]);

  i := 2;
  while i <= Length(aMailData) do
  begin
    if (aMailData[i] = '.') and (aMailData[i-1] = #10) and (aMailData[i-2] = #13)
      then Insert('.',aMailData,i);
    inc(i);
  end;

  Result := SendCmd(aMailData + #13#10 + '.',[250]);
end;

function TIsSMTPClient.GetDataMultipartMixed(aHeader: TIsSMTPClientHeader;
                                          aInlineText, aInlineTextContentType: String;
                                          aAttachments: TIsMultiPartMixedAttachments): String;
var
  aMultipartMixedEncoder: TisMultipartMixedEncoder;
  Str: String;
begin
  aMultipartMixedEncoder := TisMultipartMixedEncoder.create;
  try
    aMultipartMixedEncoder.Encode(aInlineText,
                                  aInlineTextContentType,
                                  aAttachments);

    aHeader.ContentType := aMultipartMixedEncoder.TopHeaderContentType;
    aMultipartMixedEncoder.CloseBoundary;
    Str := aMultipartMixedEncoder.Stream.AsString;

    Result := aHeader.GetRawHeaderText + #13#10#13#10 + Str;
  finally
    aMultipartMixedEncoder.free;
  end;
end;

function TIsSMTPClient.Quit: String;
begin
  Result := SendCmd('QUIT',[221]);
  Disconnect;
end;

function TIsSMTPClient.Vrfy(const aUserName: String): String;
begin
  Result := SendCmd('VRFY ' + aUserName,[250]);
end;

function TIsSMTPClient.Rset: String;
begin
  Result := SendCmd('RSET',[250]);
end;


procedure TIsSMTPClient.SendQuickAttach(const AFrom, ASendTo, ASubject, ABody, AFileName: String);
var
  Atts: TIsMultiPartMixedAttachments;
  Rcpt: TArrayOfString;
  Head: TIsSmtpClientHeader;
  MailData: String;
begin
  Atts := TIsMultiPartMixedAttachments.Create;
  try
    Atts.Add.LoadFromFile(AFileName);
    Head := TIsSmtpClientHeader.Create;
    try
      Head.From := AFrom;
      Head.SendTo := ASendTo;
      Head.Subject := ASubject;

      MailData := GetDataMultipartMixed(Head, ABody, '', Atts);
    finally
      Head.Free;
    end;
  finally
    Atts.Free;
  end;

  Rcpt := DelimitedStrToArray(ASendTo);
  DoSendMail(AFrom, Rcpt, MailData);
end;


procedure TIsSMTPClient.SendQuickMessage(const AFrom, ASendTo, ASubject, ABody: String);
var
  Rcpt: TArrayOfString;
  Head: TIsSmtpClientHeader;
begin
  Rcpt := DelimitedStrToArray(ASendTo);

  Head := TIsSmtpClientHeader.Create;
  try
    Head.From := AFrom;
    Head.SendTo := ASendTo;
    Head.Subject := ASubject;

    if Contains(ABody, '<html') then
      Head.ContentType := 'text/html'
    else
      Head.ContentType := 'text/plain';

    DoSendMail(AFrom, Rcpt, Head.RawHeaderText + #13#10#13#10 + ABody);
  finally
    Head.Free;
  end;
end;

procedure TIsSMTPClient.DoSendMail(const AFrom: String; const ARcptLst: TArrayOfString; const AMailData: String);
begin
  Connect(FHost, FPort);
  try
    if FAuthType = atAutoSelect then
      Ehlo
    else
      Helo;

    if FAuthType <> atNone then
      Auth(FUser, FPassword, FAuthType);

    mailFrom(AFrom);
    RcptTo(ARcptLst);
    Data(AMailData);
    Quit;
  finally
    Disconnect;
  end;
end;

procedure TIsSMTPClient.SendMailMultipartMixed(const AFrom: String; const ASendTo: String;
 const AHeader: TIsSMTPClientHeader; const AInlineText, AInlineTextContentType: String;
 const AAttachments: TIsMultiPartMixedAttachments);
var
  Rcpt: TArrayOfString;
  MailData: String;
begin
  Rcpt := DelimitedStrToArray(ASendTo);
  MailData := GetDataMultipartMixed(AHeader, AInlineText, AInlineTextContentType, AAttachments);
  DoSendMail(AFrom, Rcpt, MailData);
end;


function TIsSMTPClient.SendCmd(aCmd: String; OkResponses: array of Word): String;
var
  P: Pchar;
  L: Integer;
  ByteSent: Integer;
begin
  if (length(aCmd) <= 1) or
     (aCmd[length(aCmd)] <> #10) or
     (aCmd[length(aCmd) - 1] <> #13)
  then aCmd := aCmd + #13#10;
  p:=@aCmd[1]; // pchar
  l:=length(aCmd);
  while l>0 do begin
    ByteSent:=SocketWrite(p^,l);
    if ByteSent<=0 then raise EisException.Create('Connection close gracefully!');
    inc(p,ByteSent);
    dec(l,ByteSent);
  end;
  Result := GetResponse(OkResponses);
end;

function TIsSMTPClient.GetResponse(OkResponses: array of Word): String;

  function Internalstpblk(PValue : PChar) : PChar;
  begin
    Result := PValue;
    while Result^ in [' ', #9, #10, #13] do Inc(Result);
  end;

  function InternalGetInteger(Data: PChar; var Number : Integer) : PChar;
  var bSign : Boolean;
  begin
    Number := 0;
    Result := InternalStpBlk(Data);
    if (Result = nil) then Exit;
    { Remember the sign }
    if Result^ in ['-', '+'] then begin
      bSign := (Result^ = '-');
      Inc(Result);
    end
    else bSign  := FALSE;
    { Convert any number }
    while (Result^ <> #0) and (Result^ in ['0'..'9']) do begin
      Number := Number * 10 + ord(Result^) - ord('0');
      Inc(Result);
    end;
    { Correct for sign }
    if bSign then Number := -Number;
  end;

var
  aBuffStr: String;
  aBuffStrLength: Integer;
  aResponse: String;
  aStatusCode: Integer;
  aGoodResponse: Boolean;
  ALst : TstringList;
  P: Pchar;
  i, j: Integer;
begin
  Result := '';
  while True do
  begin
    {Read the response from the socket - end of the response is show by <CRLF>}
    aResponse := '';
    while True do
    begin
      Setlength(aBuffStr,512); //The maximum total length of a reply line including the reply code and the <CRLF> is 512 characters. (http://www.freesoft.org/CIE/RFC/821/24.htm)
      aBuffStrLength := SocketRead(aBuffStr[1], length(aBuffStr));
      aResponse := AResponse + copy(aBuffStr, 1, aBuffStrLength);
      If aResponse = '' then raise EisException.Create('Connection close gracefully!');
      If (aBuffStrLength > 1) and
         (aBuffStr[aBuffStrLength] = #10) and
         (aBuffStr[aBuffStrLength - 1] = #13) then Break;
    end;
    Result := Result + aResponse;

    {The format for multiline replies requires that every line, except the last,
     begin with the reply code, followed immediately by a hyphen, "-" (also known as minus),
     followed by text. The last line will begin with the reply code, followed immediately
     by <SP>, optionally some text, and <CRLF>.}
    ALst := TstringList.create;
    try
      Alst.Text := aResponse;
      If Alst.count = 0 then raise EisException.Create('Emtpy response');
      For j := 0 to Alst.count - 1 do begin
        aResponse := Alst[j];
        p := InternalGetInteger(@aResponse[1], aStatusCode);
        aGoodResponse := False;
        for I := 0 to High(OkResponses) do
          if OkResponses[I] = aStatusCode then begin
            aGoodResponse := True;
            Break;
          end;

        If not aGoodResponse then raise EisException.Create(aResponse);
        if p^ <> '-' then Begin
          If J <> Alst.count - 1 then raise EisException.Create(aResponse);
          Exit;
        end;
      end;
    finally
      ALst.Free;
    end;
  end;
end;

function TIsSMTPClient.SocketWrite(var Buffer; Count: Longint): Longint;
begin
  Result := Send(FSocketDescriptor,Buffer,Count,0);
  CheckError(Result =  SOCKET_ERROR);
end;

function TIsSMTPClient.SocketRead(var Buffer; Count: Longint): Longint;
begin
  Result := Recv(FSocketDescriptor,Buffer,Count,0);
  CheckError(Result = SOCKET_ERROR);
end;

function IsGetDefaultMIMEContentTypeFromExt(aExt: String): String;
var Index : Integer;
    LExt: string;
begin
  LExt := LowerCase(trim(aExt));
  If (LExt = '') or (LExt[1] <> '.') then LExt := '.' + LExt;
  Index := vIsMimeContentTypeByExtList.IndexOfName(LExt);
  if Index <> -1 then Result := vIsMimeContentTypeByExtList.ValueFromIndex[Index]
  else Result := 'application/octet-stream';
end;

procedure TIsMultiPartMixedAttachment.LoadFromFile(const AFileName: string);
var
  S: IisStream;
begin
  S := TisStream.CreateFromFile(AFileName);
  AttachStream(AFileName, S);
end;

function TIsMultiPartMixedAttachments.Add(AObject: TIsMultiPartMixedAttachment): Integer;
begin
  Result := inherited Add(AObject);
end;

function TIsMultiPartMixedAttachments.Add: TIsMultiPartMixedAttachment;
begin
  Result := TIsMultiPartMixedAttachment.Create;
  try
    Add(Result);
  except
    Result.Free;
    raise;
  end;
end;

function TIsMultiPartMixedAttachments.GetItem(Index: Integer): TIsMultiPartMixedAttachment;
begin
  Result := TIsMultiPartMixedAttachment(inherited Items[Index]);
end;

function TIsMultiPartMixedAttachments.IndexOf(AObject: TIsMultiPartMixedAttachment): Integer;
begin
  Result := inherited IndexOf(AObject);
end;

procedure TIsMultiPartMixedAttachments.Insert(Index: Integer; AObject: TIsMultiPartMixedAttachment);
begin
  inherited Insert(Index, AObject);
end;

function TIsMultiPartMixedAttachments.Remove(AObject: TIsMultiPartMixedAttachment): Integer;
begin
  Result := inherited Remove(AObject);
end;

procedure TIsMultiPartMixedAttachments.SetItem(Index: Integer; AObject: TIsMultiPartMixedAttachment);
begin
  inherited Items[Index] := AObject;
end;

procedure TIsMultipartMixedEncoder.AddAttachment(const FileName, ContentType: string;
  const AFileData: IisStream);
var
  sFormFieldInfo: string;
  Buffer: String;
begin
  if FStream.Position > 0 then
    sFormFieldInfo := #13#10
  else
    sFormFieldInfo := #13#10 +
                      'This is a multi-part message in MIME format.'+ #13#10 +
                      #13#10;

  sFormFieldInfo := sFormFieldInfo + Format(
                                            '--' + Boundary + #13#10 +
                                            'Content-Type: %s; name="%s"' + #13#10 +
                                            'Content-Transfer-Encoding: base64' + #13#10 +
                                            'Content-Disposition: attachment; filename="%s"' + #13#10 +
                                            #13#10,
                                            [ContentType, ExtractFileName(FileName), ExtractFileName(FileName)]
                                           );

  FStream.WriteBuffer(Pointer(sFormFieldInfo)^, Length(sFormFieldInfo));
  Buffer := AFileData.AsString;
  Buffer := IsMimeBase64EncodeString(Buffer) + #13#10;
  FStream.WriteBuffer(Buffer[1], Length(Buffer));
end;

procedure TIsMultipartMixedEncoder.AddInlineText(const ContentType, Text: string);
var
  sFormFieldInfo: string;
begin
  If FStream.Position > 0 then
    sFormFieldInfo := #13#10
  else
   sFormFieldInfo := #13#10 +
                    'This is a multi-part message in MIME format.' + #13#10 +
                    #13#10;

  sFormFieldInfo := sFormFieldInfo + Format(
                                            '--' + Boundary + #13#10 +
                                            'Content-Type: %s' + #13#10 +
                                            #13#10,
                                            [ContentType]) + Text;

  FStream.WriteBuffer(Pointer(sFormFieldInfo)^, Length(sFormFieldInfo));
end;

procedure TIsMultipartMixedEncoder.CloseBoundary;
var
  sFormFieldInfo: string;
begin
  sFormFieldInfo := #13#10 +
                    '--' + Boundary + '--' + #13#10;
  FStream.WriteBuffer(Pointer(sFormFieldInfo)^, Length(sFormFieldInfo));
end;

constructor TIsMultipartMixedEncoder.Create;
begin
  FStream := TisStream.Create;
  FBoundary := GenerateUniqueBoundary;
  FTopHeaderContentType := 'multipart/mixed; boundary="' + FBoundary + '"';
end;

procedure TIsMultipartMixedEncoder.Encode(InlineText,
 InlineTextContentType: String; Attachments: TIsMultiPartMixedAttachments);
var
  i: Integer;
begin
  FStream.Clear;
  AddInlineText(InlineTextContentType, InlineText);
  if Assigned(Attachments) then
    for i := 0 to Attachments.Count - 1 do
      AddAttachment(Attachments[i].FileName,
                    Attachments[i].ContentType,
                    Attachments[i].FileData);
end;


procedure IsFillMimeContentTypeByExtList(AMIMEList : TStrings);
var reg: TRegistry;
    KeyList: TStrings;
    i: Integer;
    aExt, aContentType: String;
begin
  AMIMEList.Clear;

  Reg := TRegistry.Create(KEY_READ);
  try

    KeyList := TStringList.create;
    try

      Reg.RootKey := HKEY_CLASSES_ROOT;
      if Reg.OpenKeyReadOnly('\') then begin
        Reg.GetKeyNames(KeyList);
        Reg.CloseKey;
        for i := 0 to KeyList.Count - 1 do begin
          aExt := KeyList[i];
          if (length(aExt) > 1) and (aExt[1] = '.') then begin
            if reg.OpenKeyReadOnly('\' + aExt) then begin
              aExt := Trim(aExt);
              If (length(aExt) > 1) then begin
                aContentType := trim(Reg.ReadString('Content Type'));
                if aContentType <> '' then
                  AMIMEList.Values[LowerCase(aExt)] := LowerCase(aContentType);
              end;
              Reg.CloseKey;
            end;
          end;
        end;
      end;

    finally
      KeyList.Free;
    end;

  finally
    reg.free;
  end;
end;

procedure IsFillExtByMimeContentTypeList(AMIMEList : TStrings);
var reg: TRegistry;
    KeyList: TStrings;
    i: Integer;
    aExt, aContentType: String;
begin
  AMIMEList.Clear;
  Reg := TRegistry.Create(KEY_READ);
  try
    KeyList := TStringList.create;
    try
      Reg.RootKey := HKEY_CLASSES_ROOT;
      if Reg.OpenKeyreadOnly('\MIME\Database\Content Type') then begin
        Reg.GetKeyNames(KeyList);
        Reg.CloseKey;
        for i := 0 to KeyList.Count - 1 do begin
          aContentType := KeyList[i];
          If aContentType <> '' then begin
            if Reg.OpenKeyreadOnly('\MIME\Database\Content Type\' + aContentType) then begin
              aContenttype := trim(aContentType);
              if aContentType <> '' then begin
                aExt := reg.ReadString('Extension');
                if aExt <> '' then begin
                  If (aExt[1] <> '.') then aExt := '.' + aExt;
                  AMIMEList.Values[LowerCase(aContentType)] := LowerCase(aExt)
                end;
              end;
              Reg.CloseKey;
            end;
          end;
        end;
      end;

    finally
      KeyList.Free;
    end;

  finally
    reg.free;
  end;
end;

function IsGetDefaultFileExtFromMimeContentType(aContentType: String): String;
Var P: integer;
    Index : Integer;
Begin
  Result := '';

  aContentType := LowerCase(aContentType);
  P := Pos(';',aContentType);
  if (P > 0) then delete(aContentType,P,MaxInt);
  aContentType := Trim(AContentType);

  Index := vIsExtbyMimeContentTypeList.IndexOfName(aContentType);
  if Index <> -1 then Result := vIsExtbyMimeContentTypeList.ValueFromIndex[Index];
end;

function TIsSMTPClient.MakePath(const APath: String): String;
begin
  Result := Trim(APath);

  if Contains(APath, '<') then
  begin
    GetNextStrValue(Result, '<');
    Result := GetNextStrValue(Result, '>');
  end;

  Result := '<' + Result + '>';
end;

function TIsMultipartMixedEncoder.GenerateUniqueBoundary: string;
begin
  Result := '---------------------------' + FormatDateTime('mmddyyhhnnsszzz', Now);
end;

function TIsSMTPClient.DelimitedStrToArray(const AStr: String): TArrayOfString;
var
  s: String;
begin
  SetLength(Result, 0);
  s := AStr;
  while s <> '' do
  begin
    SetLength(Result, Length(Result) + 1);
    Result[High(Result)] := GetNextStrValue(s, ';');
  end;
end;

procedure TIsMultiPartMixedAttachment.AttachStream(
  const AFileName: String; const AStream: IisStream);
begin
  FFileData := AStream;
  FFileName := AFileName;
  FContentType := IsGetDefaultMIMEContentTypeFromExt(ExtractfileExt(AFileName));
end;

initialization
  vIsMimeContentTypeByExtList := ThashedStringList.Create;
  vIsExtbyMimeContentTypeList := ThashedStringList.Create;
  IsFillMimeContentTypeByExtList(vIsMimeContentTypeByExtList);
  IsFillExtByMimeContentTypeList(vIsExtbyMimeContentTypeList);

finalization
  vIsMimeContentTypeByExtList.Free;
  vIsExtbyMimeContentTypeList.Free;

end.
