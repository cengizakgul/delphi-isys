unit isConsoleApp;

interface 

uses Windows, isBaseClasses;

type
  TColor = -1..$FFFFFF;

const
  clDefault = TColor(-1);

  clBlack = TColor($000000);
  clMaroon = TColor($000080);
  clGreen = TColor($008000);
  clOlive = TColor($008080);
  clNavy = TColor($800000);
  clPurple = TColor($800080);
  clTeal = TColor($808000);
  clGray = TColor($808080);
  clSilver = TColor($C0C0C0);
  clRed = TColor($0000FF);
  clLime = TColor($00FF00);
  clYellow = TColor($00FFFF);
  clBlue = TColor($FF0000);
  clFuchsia = TColor($FF00FF);
  clAqua = TColor($FFFF00);
  clWhite = TColor($FFFFFF);

type
  IisConsole = interface
  ['{C4E63DFF-80AC-4C39-B170-A552749968FC}']
    procedure SetForegroundColor(const AValue: TColor);
    procedure SetBackgroundColor(const AValue: TColor);
    function  GetForegroundColor: TColor;
    function  GetBackgroundColor: TColor;
    property  BackgroundColor: TColor read GetBackgroundColor write SetBackgroundColor;
    property  ForegroundColor: TColor read GetForegroundColor write SetForegroundColor;

    procedure Write(const s: string; aFGColor: TColor = clDefault; aBGColor: TColor = clDefault);
    procedure WriteLn(const s: string = ''; aFGColor: TColor = clDefault; aBGColor: TColor = clDefault);
    procedure WriteErr(const s: string; aFGColor: TColor = clDefault; aBGColor: TColor = clDefault);
    procedure WriteErrLn(const s: string = ''; aFGColor: TColor = clDefault; aBGColor: TColor = clDefault);

    procedure SetWindowTitle(const sTitle: string);
    procedure ShowCursor(iSize: Integer);
    procedure HideCursor;
    procedure GetCursorPos(var x, y: Integer);
    procedure SetCursorTo(x, y: Integer);
    procedure ClearScreen;
    function  GetScreenLeft: Integer;
    function  GetScreenTop: Integer;
    function  GetScreenHeight: Integer;
    function  GetScreenWidth: Integer;
    procedure ClearBuffer;
    function  GetBufferHeight: Integer;
    function  GetBufferWidth: Integer;
    procedure GetCharAtPos(x, y: Integer; var rCharInfo: Char);
  end;

  function Console: IisConsole;

implementation

var vConsole: IisConsole;

type
  TisConsole = class(TisInterfacedObject, IisConsole)
  private 
    FhStdIn            : THandle;  // Handle to the standard input
    FhStdOut           : THandle;  // Handle to the standard output 
    FhStdErr           : THandle;  // Handle to the standard error (Output)
    FbConsoleAllocated : Boolean;  // Creation Flag
    FBgAttrib          : Cardinal; // Currently set BackGround Attribs.
    FFgAttrib          : Cardinal; // Currently set ForeGround Attribs.
    FOrigBgAttrib      : Cardinal; // Original BackGround Attribs.
    FOrigFgAttrib      : Cardinal; // Original ForeGround Attribs.


    procedure SetFGColor(bRed,bGreen,bBlue,bIntensity : Boolean);
    procedure SetBGColor(bRed,bGreen,bBlue,bIntensity : Boolean);
    procedure ReadColorBack;

    procedure DoWrite(const s: string; aFGColor: TColor; aBGColor: TColor; AError: Boolean);

  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;

    procedure SetForegroundColor(const AValue: TColor);
    procedure SetBackgroundColor(const AValue: TColor);
    function  GetForegroundColor: TColor;
    function  GetBackgroundColor: TColor;

    procedure Write(const s: string; aFGColor: TColor = clDefault; aBGColor: TColor = clDefault);
    procedure WriteLn(const s: string = ''; aFGColor: TColor = clDefault; aBGColor: TColor = clDefault);
    procedure WriteErr(const s: string; aFGColor: TColor = clDefault; aBGColor: TColor = clDefault);
    procedure WriteErrLn(const s: string = ''; aFGColor: TColor = clDefault; aBGColor: TColor = clDefault);

    (* Change the Windowtitle of the command window. If the program has been
       executed from a CMD-box the title change is only active while the
       programs execution time *)
    procedure SetWindowTitle (const sTitle : string);

    (* some Cursor manipulation functions *)
    procedure ShowCursor ( iSize : Integer);
    procedure HideCursor;
    procedure GetCursorPos( var x,y : integer);
    procedure SetCursorTo(x,y : integer);

    (* screen operations:
       the screen ist the visible part of a cmd window. Behind the window there
       is a screenbuffer. The screenbuffer may be larger than the visible
       window *)
    procedure ClearScreen;
    function GetScreenLeft   : integer;
    function GetScreenTop    : Integer;
    function GetScreenHeight : integer;
    function GetScreenWidth  : integer;

    (* screenbuffer operations *)
    procedure ClearBuffer;
    function GetBufferHeight : integer;
    function GetBufferWidth  : integer;

    (* sample to read characters from then screenbuffer *)
    procedure GetCharAtPos(x,y : Integer;var rCharInfo : Char);
  end;


function Console: IisConsole;
begin
  if not Assigned(vConsole) then
    vConsole := TisConsole.Create;
  Result := vConsole;
end;


{ TisConsole }

procedure TisConsole.ClearBuffer;
var 
  SBInfo         : TConsoleScreenBufferInfo; 
  ulWrittenChars : Cardinal; 
  TopLeft        : TCoord;
begin 
  TopLeft.X := 0; 
  TopLeft.Y := 0; 
  GetConsoleScreenBufferInfo(FhStdOut,SBInfo);

  FillConsoleOutputCharacter(FhStdOut,' ', 
                              SBInfo.dwSize.X * SBInfo.dwSize.Y, 
                             TopLeft, 
                             ulWrittenChars); 

  FillConsoleOutputAttribute( FhStdOut, 
                              FOREGROUND_RED or FOREGROUND_BLUE or FOREGROUND_GREEN, 
                              (SBInfo.srWindow.Right - SBInfo.srWindow.Left) * 
                              (SBInfo.srWindow.Bottom - SBInfo.srWindow.Top), 
                              TopLeft, 
                              ulWrittenChars); 
end; 

procedure TisConsole.ClearScreen; 
var 
  SBInfo         : TConsoleScreenBufferInfo; 
  ulWrittenChars : Cardinal; 
  TopLeft        : TCoord; 

begin 
  GetConsoleScreenBufferInfo(FhStdOut,SBInfo); 
  TopLeft.X := SBInfo.srWindow.Left; 
  TopLeft.Y := SBInfo.srWindow.Top; 
  FillConsoleOutputCharacter(FhStdOut,' ', 
                             (SBInfo.srWindow.Right - SBInfo.srWindow.Left) * 
                             (SBInfo.srWindow.Bottom - SBInfo.srWindow.Top), 
                             TopLeft, 
                             ulWrittenChars); 
  FillConsoleOutputAttribute(FhStdOut,FOREGROUND_RED or FOREGROUND_BLUE or FOREGROUND_GREEN, 
                             (SBInfo.srWindow.Right - SBInfo.srWindow.Left) * 
                             (SBInfo.srWindow.Bottom - SBInfo.srWindow.Top), 
                             TopLeft, 
                             ulWrittenChars);
end;

procedure TisConsole.DoOnConstruction;
begin
  inherited;
// A process can be associated with only one console, so the AllocConsole
// function fails if the calling process already has a console.
  FbConsoleAllocated := AllocConsole;

// initializing the needed handles
  FhStdOut := GetStdHandle(STD_OUTPUT_HANDLE);
  FhStdErr := GetStdHandle(STD_ERROR_HANDLE);
  FhStdIn  := GetStdHandle(STD_INPUT_HANDLE);

  ReadColorBack;
  FOrigBgAttrib := FBgAttrib;
  FOrigFgAttrib := FFgAttrib;
end;

procedure TisConsole.DoOnDestruction;
begin
  SetConsoleTextAttribute(FhStdOut, FOrigBgAttrib or FOrigFgAttrib);

  if FbConsoleAllocated then
    FreeConsole;
  inherited;
end;

procedure TisConsole.DoWrite(const s: string; aFGColor, aBGColor: TColor; AError: Boolean);
var
  ulLength : Cardinal;
  Bg, Fg: TColor;
  H: THandle;
begin
  Bg := GetBackgroundColor;
  Fg := GetForegroundColor;
  try
    SetBackgroundColor(aBGColor);
    SetForegroundColor(aFGColor);

    if AError then
      H := FhStdErr
    else
      H := FhStdOut;

    WriteFile(H, (PChar(s))^, Length(s), ulLength, nil);
  finally
    SetBackgroundColor(Bg);
    SetForegroundColor(Fg);
  end;
end;

function TisConsole.GetBackgroundColor: TColor;
var
  ColorInt: Byte;
begin
  Result := 0;
  if (FBgAttrib and BACKGROUND_INTENSITY) <> 0 then
    ColorInt := $FF
  else
    ColorInt := $80;
  if (FBgAttrib and BACKGROUND_RED) <> 0 then
    Result := Result or ColorInt;
  if (FBgAttrib and BACKGROUND_GREEN) <> 0 then
    Result := Result or (ColorInt shl 8);
  if (FBgAttrib and BACKGROUND_BLUE) <> 0 then
    Result := Result or (ColorInt shl 16);
end;

function TisConsole.GetBufferHeight: integer;
var 
  SBInfo : TConsoleScreenBufferInfo; 
begin
  GetConsoleScreenBufferInfo(FhStdOut,SBInfo);
  Result := SBInfo.dwSize.Y;
end;

function TisConsole.GetBufferWidth: integer;
var
  SBInfo : TConsoleScreenBufferInfo;

begin
  GetConsoleScreenBufferInfo(FhStdOut,SBInfo);
  Result := SBInfo.dwSize.X; 
end; 

procedure TisConsole.GetCharAtPos(x, y: Integer; var rCharInfo : Char); 
var 
  CharInfo : array [0..10] of Char; 
  TopLeft  : TCoord; 
  CharsRead : Cardinal; 
begin 
  TopLeft.x := X; 
  TopLeft.Y := Y; 
  ReadConsoleOutputCharacter(FhStdOut,CharInfo,10,TopLeft,CharsRead); 
  rCharInfo   := CharInfo[0]; 
end; 

procedure TisConsole.GetCursorPos(var x, y: integer); 
var 
  SBInfo : TConsoleScreenBufferInfo; 

begin 
  GetConsoleScreenBufferInfo(FhStdOut,SBInfo); 
  x := SBInfo.dwCursorPosition.X; 
  y := SBInfo.dwCursorPosition.Y; 
end; 

function TisConsole.GetForegroundColor: TColor;
var
  ColorInt: Byte;
begin
  Result := 0;
  if (FFgAttrib and FOREGROUND_INTENSITY) <> 0 then
    ColorInt := $FF
  else
    ColorInt := $80;
  if (FFgAttrib and FOREGROUND_RED) <> 0 then
    Result := Result or ColorInt;
  if (FFgAttrib and FOREGROUND_GREEN) <> 0 then
    Result := Result or (ColorInt shl 8);
  if (FFgAttrib and FOREGROUND_BLUE) <> 0 then
    Result := Result or (ColorInt shl 16);
end;

function TisConsole.GetScreenHeight: integer;
var 
  SBInfo : TConsoleScreenBufferInfo; 

begin 
  GetConsoleScreenBufferInfo(FhStdOut,SBInfo); 
  Result := SBInfo.srWindow.Bottom -SBInfo.srWindow.Top; 
end; 

function TisConsole.GetScreenLeft: integer; 
var 
  SBInfo : TConsoleScreenBufferInfo; 

begin 
  GetConsoleScreenBufferInfo(FhStdOut,SBInfo); 
  Result := SBInfo.srWindow.Left; 
end; 

function TisConsole.GetScreenTop: Integer; 
var 
  SBInfo : TConsoleScreenBufferInfo; 

begin 
  GetConsoleScreenBufferInfo(FhStdOut,SBInfo); 
  Result := SBInfo.srWindow.Top; 
end; 

function TisConsole.GetScreenWidth: integer; 
var 
  SBInfo : TConsoleScreenBufferInfo; 

begin 
  GetConsoleScreenBufferInfo(FhStdOut,SBInfo); 
  Result := SBInfo.srWindow.Right - SBInfo.srWindow.Left; 
end; 

procedure TisConsole.HideCursor; 
var 
  ConsoleCursorInfo : TConsoleCursorInfo; 
begin 
  GetConsoleCursorInfo(FhStdOut,ConsoleCursorInfo); 
  if ConsoleCursorInfo.bVisible then begin 
    ConsoleCursorInfo.bVisible := False; 
    SetConsoleCursorInfo(FhStdOut,ConsoleCursorInfo); 
  end; 
end;

procedure TisConsole.ReadColorBack;
var
  SBInfo: TConsoleScreenBufferInfo;
begin
  GetConsoleScreenBufferInfo(FhStdOut, SBInfo);
  FFgAttrib := SBInfo.wAttributes and $0F;
  FBgAttrib := SBInfo.wAttributes and $F0;
end;

procedure TisConsole.SetBackgroundColor(const AValue: TColor);
var
  R, G, B: Byte;
begin
  if (AValue <> clDefault) and (AValue <> GetBackgroundColor) then
  begin
    R := GetRValue(AValue);
    G := GetGValue(AValue);
    B := GetBValue(AValue);
    SetBGColor(R <> 0, G <> 0, B <> 0, (R > 128) or (G > 128) or (B > 128));
  end;
end;

procedure TisConsole.SetBGColor(bRed, bGreen, bBlue, bIntensity: Boolean);
begin
  FBgAttrib := 0;
  if bRed       then FBgAttrib := FBgAttrib or BACKGROUND_RED;
  if bGreen     then FBgAttrib := FBgAttrib or BACKGROUND_GREEN; 
  if bBlue      then FBgAttrib := FBgAttrib or BACKGROUND_BLUE;
  if bIntensity then FBgAttrib := FBgAttrib or BACKGROUND_INTENSITY;
  SetConsoleTextAttribute(FhStdOut,FBgAttrib or FFgAttrib); 
end; 

procedure TisConsole.SetCursorTo(x, y: integer); 
var 
  Coords : TCoord; 
  SBInfo : TConsoleScreenBufferInfo; 

begin 
  GetConsoleScreenBufferInfo(FhStdOut,SBInfo); 
  if x < 0 then Exit; 
  if y < 0 then Exit; 
  if x > SbInfo.dwSize.X then Exit; 
  if y > SbInfo.dwSize.Y then Exit; 

  Coords.X := x; 
  Coords.Y := y; 
  SetConsoleCursorPosition(FhStdOut,Coords); 
end; 

procedure TisConsole.SetFGColor(bRed, bGreen, bBlue, 
  bIntensity: Boolean); 

begin 
  FFgAttrib := 0;
  if bRed       then FFgAttrib := FFgAttrib or FOREGROUND_RED;
  if bGreen     then FFgAttrib := FFgAttrib or FOREGROUND_GREEN;
  if bBlue      then FFgAttrib := FFgAttrib or FOREGROUND_BLUE;
  if bIntensity then FFgAttrib := FFgAttrib or FOREGROUND_INTENSITY;
  SetConsoleTextAttribute(FhStdOut,FBgAttrib or FFgAttrib);
end;

procedure TisConsole.SetForegroundColor(const AValue: TColor);
var
  R, G, B: Byte;
begin
  if (AValue <> clDefault) and (AValue <> GetForegroundColor) then
  begin
    R := GetRValue(AValue);
    G := GetGValue(AValue);
    B := GetBValue(AValue);
    SetFGColor(R <> 0, G <> 0, B <> 0, (R > 128) or (G > 128) or (B > 128));
  end;
end;

procedure TisConsole.SetWindowTitle(const sTitle: string);
begin
  SetConsoleTitle(PChar(sTitle));
end;

procedure TisConsole.ShowCursor(iSize: Integer);
var
  ConsoleCursorInfo : TConsoleCursorInfo;
begin
  GetConsoleCursorInfo(FhStdOut,ConsoleCursorInfo);
  if (not ConsoleCursorInfo.bVisible) or
     (    ConsoleCursorInfo.dwSize <> DWORD(iSize) )  then begin
    ConsoleCursorInfo.bVisible := True; 
    ConsoleCursorInfo.dwSize   := iSize;
    SetConsoleCursorInfo(FhStdOut,ConsoleCursorInfo);
  end; 
end;

procedure TisConsole.Write(const s: string; aFGColor: TColor = clDefault; aBGColor: TColor = clDefault);
begin
  DoWrite(s, aFGColor, aBGColor, False);
end;

procedure TisConsole.WriteErr(const s: string; aFGColor, aBGColor: TColor);
begin
  DoWrite(s, aFGColor, aBGColor, True);
end;

procedure TisConsole.WriteErrLn(const s: string; aFGColor, aBGColor: TColor);
begin
  WriteErr(s, aFGColor, aBGColor);
  WriteErr(#13#10);
end;

procedure TisConsole.WriteLn(const s: string = ''; aFGColor: TColor = clDefault; aBGColor: TColor = clDefault);
begin
  Write(s, aFGColor, aBGColor);
  Write(#13#10);
end;

initialization

finalization
  vConsole := nil;

end.

