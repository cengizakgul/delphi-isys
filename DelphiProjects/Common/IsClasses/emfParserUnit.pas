unit emfParserUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Dialogs, StdCtrls, isBaseClasses, Graphics,
  EvStreamUtils;

const
  Version = '1';
  EmfParserFontsList = 'EmfParserFontsList';
  PDFBackground = 'PDFBackground';
  ConstVersion = 'Version';
  ConstMetafile = 'Metafile';
  DefaultParsedFileExtention = 'ismf';
  DefaultFileDescription = 'iSystems metafile';

type
  TEmfParserException = class(Exception);
  TMetafileEnumEnhFunc = function (DC: HDC; HTable: PHandleTable; EMFR: PEnhMetaRecord; nObj: Integer; Sender: TObject): BOOL; stdcall;


  IEmfParser = interface
    ['{E1FCECF0-7AF9-4236-8F01-85E0C7283197}']
    function GetVersion : String;

    procedure GetMetafile(const AStream : IevDualStream);

    procedure GetPDFBackground(const AStream : TStream);
    procedure AddPDFBackground(AStream : TStream);
    procedure DeletePDFBackground;
    function PDFExists : boolean;

    function GetFontSize(const AFontName : String) : integer;
    function GetFontData(const AFontName : String; var Buffer; const ACount:integer) : integer;
    procedure GetFontsList(const AStringList : TSTringList);
    function FindFont(const AFontName : String) : boolean;

    procedure SaveToStream(const AStream : IEvDualStream);
    function GetDefaultFileExtention : String;
  end;


  TEmfParser = class(TInterfacedObject, IEmfParser)
  private
    FStorage : IisListOfValues;
    FFontsList : TStringList;
  protected
    FEnumCallback : TMetafileEnumEnhFunc; // it should be Protected, because we reassign it in Two Pilot Descendant
    procedure Parse(AStream :IEvDualStream);
    function FindStringByName(const AString : String; const AStringList : TStringList) : boolean;
    procedure Init; virtual;
  public

    function GetVersion : String;
    function GetDefaultFileExtention : String;

    procedure GetMetafile(const AStream : IEvDualStream);

    procedure AddPDFBackground(AStream : TStream);
    procedure GetPDFBackground(const AStream : TStream);
    procedure DeletePDFBackground;
    function PDFExists : boolean;

    function GetFontSize(const AFontName : String) : integer;
    function GetFontData(const AFontName : String; var Buffer; const ACount:integer) : integer;
    procedure GetFontsList(const AStringList : TSTringList);
    function FindFont(const AFontName : String) : boolean;
    procedure AddFont(const AFontName : String; AStream : IEvDualStream);

    procedure SaveToStream(const AStream : IEvDualStream);

    constructor CreateFromFile(const AFileName : String);
    constructor CreateFromStream(const AStream : IEvDualStream);

    // Reserved is a workaround for stupid C++ warning
    constructor CreateFromEmfFile(const AFileName : String; const Reserved: Boolean = False);
    constructor CreateFromEmfStream(const AStream : IEvDualStream; const Reserved: Boolean = False);

    destructor Destroy; override;

  end;

implementation

procedure SaveFontToStream(ADC: HDC; AFont : PEMRExtCreateFontIndirect; AStream : IEvDualStream);
var
  Res : integer;
  metrics : OUTLINETEXTMETRIC;
  FontHandle : HFONT;
  Buf : PByte;
  size : integer;
  FontName : String;
  i : integer;
  S: array [0..260] of char;
begin
  Buf := nil;
  FontName := WideCharToString(AFont^.elfw.elfLogFont.lfFaceName);

  // create logical font and select it
  FontHandle := CreateFontIndirectW(AFont^.elfw.elfLogFont);
  if FontHandle = 0 then
    raise TEmfParserException.Create('Font: ' + FontName + ' Error in CreateFontIndirectW call: '+ IntToStr(GetLastError));
  SelectObject(ADC, FontHandle);

  // check this font for non embedding
  res := GetOutlineTextmetrics(ADC, SizeOf(metrics), @metrics);
  if res <= 0 then
    raise TEmfParserException.Create('Font: ' + FontName + ' Error in GetOutlineTextmetrics call: '+ IntToStr(GetLastError));
  i := metrics.otmfsType and $00000001;
  if i = 1 then
    raise TEmfParserException.Create('Font: ' + FontName + ' This font cannot be embedded');

  // check this font for substutition
  GetTextFace(ADC, 260, S);
  if UpperCase(S) <> UpperCase(FontName) then
    raise TEmfParserException.Create('Font: ' + FontName + ' Windows trying to substitute different font: ' + metrics.otmpFullName);

  // getting size of font's data
  size := Windows.GetFontData(ADC, 0,  0, nil, 0);
  if size <= 0 then
    raise TEmfParserException.Create('Font: ' + FontName + ' Error in the first GetFontData call: '+ IntToStr(GetLastError));

  // getting font's data and saving them to file
  System.GetMem(Buf, size);
  try
    res := Windows.GetFontData(ADC, 0,  0, Buf, size);
    if res <= 0 then
      raise TEmfParserException.Create('Font: ' + FontName + ' Error in the second GetFontData call: '+ IntToStr(GetLastError));
    if res <> size then
      raise TEmfParserException.Create('Font: ' + FontName + ' The second GetFontData call returned less data that size');
    AStream.WriteBuffer(Buf^, size);
  finally
    FreeMem(Buf);
  end;
end;

function EnumEnhFunc(DC: HDC; HTable: PHandleTable; EMFR: PEnhMetaRecord; nObj: Integer; Sender: TObject): BOOL; stdcall;
var
  pFont : PEMRExtCreateFontIndirect;
  tmpFontName : String;
  tmpStream : IEvDualStream;
begin
  case EMFR^.iType of
    EMR_EXTCREATEFONTINDIRECTW:
      begin
        pFont := PEMRExtCreateFontIndirect(EMFR);
        tmpFontName := WideCharToString(pFont^.elfw.elfLogFont.lfFaceName);
        // add font's name to list
        if not ((Sender as TEmfParser).FindFont(tmpFontName)) then
        begin
          tmpStream := TEvDualStreamHolder.CreateInMemory;
          try
            SaveFontToStream(DC, pFont, tmpStream);
            (Sender as TEmfParser).AddFont(tmpFontName, tmpStream);
          finally
            tmpStream := nil;
          end;
        end;
      end;
  end;
  Result := true;
end;

{ TEmfParser }

procedure TEmfParser.AddFont(const AFontName: String; AStream: IEvDualStream);
begin
  FFontsList.Add(AFontName + '=' + IntToStr(AStream.Size));
  FStorage.AddValue(AFontName, AStream);
end;

procedure TEmfParser.Init;
begin
  FFontsList := TStringList.Create;
  FStorage := TisListOfValues.Create;
  FEnumCallback := @EnumEnhFunc;
end;

destructor TEmfParser.Destroy;
begin
  FFontsList.Clear; FFontsList.Free;
  FStorage := nil;
  inherited;
end;

function TEmfParser.FindFont(const AFontName: String): boolean;
begin
  result := FindStringByName(AFontName, FFontsList);
end;

function TEmfParser.FindStringByName(const AString : String;
  const AStringList: TStringList): boolean;
var
  i : integer;
begin
  result := false;
  for i := 0 to AStringList.Count - 1 do
    if AString = AStringList.Names[i] then
    begin
      result := true;
      exit;
    end;
end;

function TEmfParser.GetFontData(const AFontName: String;
  var Buffer; const ACount: integer): integer;
var
  size : integer;
  tmpStream : IEVDualStream;
begin
  result := 0;
  if not FStorage.ValueExists(AFontName) then raise TEmfParserException.Create('Font ' + AFontName + ' doesn''t exists');
  size := GetFontSize(AFontName);
  if ACount < size then TEmfParserException.Create('Buffer has not enough length');
  tmpStream := (IInterface(FStorage.GetValueByName(AFontName)) as IEvDualStream);
  tmpStream.Position := 0;
  tmpStream.ReadBuffer(Buffer, ACount);
end;

function TEmfParser.GetFontSize(const AFontName: String): integer;
begin
  result := 0;
  if AFontName = '' then raise TEmfParserException.Create('Font''s name is empty');
  if not FindStringByName(AFontName, FFontsList) then exit
  else
    result := StrToInt(FFontsList.Values[AFontName]);
end;

procedure TEmfParser.GetFontsList(const AStringList: TSTringList);
var
  i : integer;
begin
  AStringList.Clear;
  for i := 0 to FFontsList.Count - 1 do
    AStringList.Add(FFontslist.Names[i]);
end;

procedure TEmfParser.GetMetafile(const AStream: IEvDualStream);
var
  tmpStream : IEvDualStream;
begin
  if not FStorage.ValueExists(ConstMetafile) then raise TEmfParserException.Create('Storage has no metafile object');
  tmpStream := IInterface(FStorage.GetValueByName(ConstMetafile)) as IEVDualStream;
  tmpStream.Position := 0;
  AStream.Clear;
  AStream.RealStream.CopyFrom(tmpStream.RealStream, 0);
end;

constructor TEmfParser.CreateFromStream(const AStream: IEvDualStream);
var
  tmp : Variant;
begin
  Init;
  AStream.Position := 0;
  FStorage.ReadFromStream(AStream);
  if FStorage.ValueExists(EmfParserfontslist) then
  begin
    tmp := FStorage.Value[EmfParserfontslist];
    if tmp <> null then
    begin
      FFontsList.Text := tmp;
      exit;
    end;
  end;
  raise TEmfParserException.Create('Input stream contains no fonts list');
end;

constructor TEmfParser.CreateFromEmfFile(const AFileName: String; const Reserved: Boolean);
var
  tmpStream : IEvDualStream;
begin
  if not FileExists(AFileName) then raise TEmfParserException.Create('File "' + AFileName + '" doesn''t exists');
  tmpStream := TEvDualStreamHolder.CreateFromFile(AFileName);
  CreateFromEmfStream(tmpStream);
end;

procedure TEmfParser.SaveToStream(const AStream: IEvDualStream);
begin
  FStorage.WriteToStream(AStream);
end;

function TEmfParser.GetVersion : String;
var
  tmp : Variant;
begin
  result := '';
  if FStorage.ValueExists(ConstVersion) then
  begin
    tmp := FStorage.Value[ConstVersion];
    if tmp <> null then
      result := tmp;
      exit
  end;
  raise TEmfParserException.Create('No version avaliable');
end;

procedure TEmfParser.Parse(AStream : IEvDualStream);
var
  R: TRect;
  hDC : HWnd;
  tmpStream : IEvDualStream;
  MetaFile : TMetaFile;
begin
  MetaFile := TMetaFile.Create;
  try
    AStream.Position := 0;
    MetaFile.LoadFromStream(AStream.RealStream);
    hDC := GetDC(0);
    try
      R.Top := 0; R.Left := 0;
      R.Right := MetaFile.MMWidth;
      R.Bottom := MetaFile.MMHeight;
      //EnumEnhMetaFile(hDC, MetaFile.Handle, @EnumEnhFunc, Self, R);
      EnumEnhMetaFile(hDC, MetaFile.Handle, @FEnumCallback, self, R);
    finally
      ReleaseDC(0, hDC);
    end;
    FStorage.AddValue(EmfParserFontsList, FFontsList.Text);

    // save metafaile to storage
    tmpStream := TEvDualStreamHolder.CreateInMemory;
    MetaFile.SaveToStream(tmpStream.RealStream);
    FStorage.AddValue(ConstMetafile, tmpStream);
  finally
    MetaFile.Free;
  end;
end;

constructor TEmfParser.CreateFromEmfStream(const AStream: IEvDualStream; const Reserved: Boolean);
begin
  Init;
  FStorage.AddValue(ConstVersion, Version);
  if AStream.Size = 0 then raise TEmfParserException.Create('Input stream is empty');
  Parse(AStream);
end;

constructor TEmfParser.CreateFromFile(const AFileName: String);
var
  tmpStream : IEvDualStream;
begin
  if not FileExists(AFileName) then raise TEmfParserException.Create('File "' + AFileName + '" doesn''t exists');
  tmpStream := TEvDualStreamHolder.CreateFromFile(AFileName);
  CreateFromStream(tmpStream);
end;

function TEmfParser.GetDefaultFileExtention: String;
begin
  result := DefaultParsedFileExtention;
end;

procedure TEmfParser.GetPDFBackground(
  const AStream: TStream);
var
  tmpStream : IEvDualStream;
begin
  if not FStorage.ValueExists(PDFBackground) then
    exit;
  tmpStream := IInterface(FStorage.GetValueByName(PDFBackground)) as IEVDualStream;
  AStream.CopyFrom(tmpStream.RealStream, 0);
end;

procedure TEmfParser.AddPDFBackground(AStream: TStream);
var
  tmpStream : IEvDualStream;
begin
  if FStorage.ValueExists(PDFBackground) then
    FStorage.DeleteValue(PDFBackground);
  tmpStream := TEvDualStreamHolder.Create;
  tmpStream.RealStream.CopyFrom(AStream, 0);
  FStorage.AddValue(PDFBackground, tmpStream);
end;

procedure TEmfParser.DeletePDFBackground;
begin
  if FStorage.ValueExists(PDFBackground) then
    FStorage.DeleteValue(PDFBackground);
end;

function TEmfParser.PDFExists: boolean;
begin
  result := FStorage.ValueExists(PDFBackground);
end;

end.
