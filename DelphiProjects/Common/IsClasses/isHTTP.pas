unit isHttp;

interface

uses evStreamUtils, WinInet, Windows, SysUtils, isExceptions, isBasicUtils;

type
  TisHttpClient = class
  public
    class function Get(const aURL: string; const aOutStream: IisStream = nil): IisStream;
    class function GetFavIcon(const aURL: string; const aOutStream: IisStream = nil): IisStream;
  end;

implementation

const
  WRITE_BUFFERSIZE = 4096;  // or 256, 512, :
  READ_BUFFERSIZE  = 4096;  // or 256, 512, :

class function TisHttpClient.Get(const aURL: string; const aOutStream: IisStream): IisStream;
var
  hNet, hFile: HINTERNET;
  buffer: array[0..READ_BUFFERSIZE - 1] of Char;
  bufsize: DWORD;
  Sz, Rz: DWORD;
  EM, ES: String;
begin
  Result := aOutStream;
  if not Assigned(Result) then
    Result := TisStream.Create;

  hNet := InternetOpen('ISYSTEMS', INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if hNet = nil then
    raise EisException.Create('Unable to get access to WinInet.Dll');
  try
    hFile := InternetOpenURL(hNet, PChar(aURL), nil, 0,
      INTERNET_FLAG_PRAGMA_NOCACHE or INTERNET_FLAG_RELOAD or INTERNET_FLAG_KEEP_CONNECTION, 0);
    if hFile = nil then
      raise EisException.CreateFmt('Cannot open %s',[aURL]);
    try
      Sz := 10000;
      SetLength(EM, Sz);

      Rz := 0;
      HttpQueryInfo(hFile, HTTP_QUERY_RAW_HEADERS_CRLF, PChar(EM), Sz, Rz);
      SetLength(EM, Sz);
      ES := EM;
      if pos(#13#10, ES) > 0 then
        ES := copy(ES, 1, pos(#13#10, ES)-1);
      if pos('200', ES) = 0 then
        raise EisException.CreateFmt('%s',[EM]);

      bufsize := READ_BUFFERSIZE;
      while (bufsize > 0) do
      begin
        if not InternetReadFile(hFile,
                                @buffer, // address of a buffer that receives the data
                                READ_BUFFERSIZE, // number of bytes to read from the file
                                bufsize) then
          raise EisException.Create('Cannot read data');
        if (bufsize > 0) and (bufsize <= READ_BUFFERSIZE) then
          Result.WriteBuffer(buffer, bufsize);
      end;
    finally
      InternetCloseHandle(hFile);
    end;
  finally
    InternetCloseHandle(hNet);
  end;
end;

class function TisHttpClient.GetFavIcon(const aURL: string; const aOutStream: IisStream): IisStream;
var
  s, newURL: String;
begin
  s := aURL;
  newURL := GetNextStrValue(s, '//');
  newURL := newURL + '//' + GetNextStrValue(s, '/') + '/favicon.ico';
  Result := Get(newURL, aOutStream);
end;

end.
