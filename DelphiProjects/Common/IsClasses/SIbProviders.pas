// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SIbProviders;

{$W-}
{$O+}
{$Q-}
{$R-}

interface

uses IBSQL, SqlTimSt, IBHeader, Variants, Classes, EvStreamUtils, SUniversalDataReader,
     IsBasicUtils, isThreadManager, isVCLBugFix;

type
  TEvIbFieldPump = procedure (const Field: TIBXSQLVAR) of object;

  TOnGetBlobData = procedure (const ABlobField: TIBXSQLVAR; out AData: String) of object;

  TEvIbFieldInfoRecord = record
    Field: TIBXSQLVAR;
    Pump: TEvIbFieldPump;
  end;

  TEvIbFieldInfoRecordArray = array of TEvIbFieldInfoRecord;

  TEvIbSqlProvider = class
  private
    FFieldInfo: TEvIbFieldInfoRecordArray;
  protected
    FRecCount: Integer;
    procedure OnBeforeGetData(const IBSQL: TIBSQL); virtual; abstract;
    function  OnAfterGetData: IEvDualStream; virtual; abstract;
    procedure OnBeforeNewRecord; virtual;
    procedure OnAfterNewRecord; virtual;
    procedure GetFieldPump(const Field: TIBXSQLVAR; out Pump: TEvIbFieldPump); virtual; abstract;
  public
    function GetData(const IBSQL: TIBSQL; ValueFieldNames: string): IEvQueryResult;
    property RecCount: Integer read FRecCount;
  end;

  TEvIbSqlKbmMemTableProvider = class(TEvIbSqlProvider)
  private
    FVersionSignature: Word;
    FStream: IEvDualStream;
    FOnGetBlobData: TOnGetBlobData;
    function  IsNull(const Field: TIBXSQLVAR): Boolean;
    procedure PumpInteger(const Field: TIBXSQLVAR);
    procedure PumpString(const Field: TIBXSQLVAR);
    procedure PumpShortString(const Field: TIBXSQLVAR);
    procedure PumpChar(const Field: TIBXSQLVAR);
    procedure PumpBlob(const Field: TIBXSQLVAR);
    procedure PumpDouble(const Field: TIBXSQLVAR);
    procedure PumpDateTime(const Field: TIBXSQLVAR);
  protected
    procedure OnBeforeGetData(const IBSQL: TIBSQL); override;
    function  OnAfterGetData: IEvDualStream; override;
    procedure GetFieldPump(const Field: TIBXSQLVAR; out Pump: TEvIbFieldPump); override;
  public
    procedure AfterConstruction; override;
    property  OnGetBlobData: TOnGetBlobData read FOnGetBlobData write FOnGetBlobData;
  end;

implementation

uses Windows, DB, SysUtils, VarUtils, Math;


function TEvIbSqlProvider.GetData(const IBSQL: TIBSQL; ValueFieldNames: string): IEvQueryResult;
var
  i, j, iFieldCount: Integer;
  PkValues: TNamedArrayOfIntArray;
  iValuesHigh, iValuesALength: Integer;
begin
  Assert(IBSQL.Open, 'IBSQL has to be open');
  Assert(IBSQL.FieldCount > 0, 'IBSQL has to have fields');
  FRecCount := 0;
  OnBeforeGetData(IBSQL);
  iFieldCount := IBSQL.FieldCount;
  SetLength(FFieldInfo, iFieldCount);
  for i := 0 to iFieldCount-1 do
  begin
    FFieldInfo[i].Field := IBSQL.Fields[i];
    GetFieldPump(FFieldInfo[i].Field, FFieldInfo[i].Pump);
    Assert(Assigned(FFieldInfo[i].Pump), 'Pump procedure should be assigned for all fields');
  end;
  SetLength(PkValues, 0);
  iValuesALength := 1000;
  while ValueFieldNames <> '' do
  begin
    SetLength(PkValues, Length(PkValues)+1);
    with PkValues[High(PkValues)] do
    begin
      Name := GetNextStrValue(ValueFieldNames, ',');
      Tag := Integer(IBSQL.FieldByName(Name));
      SetLength(A, iValuesALength)
    end;
  end;

  try
    j := 0;
    if Length(PkValues) = 0 then // optimized branch
    begin
      while not IBSQL.Eof do
      begin
        // Process termination logic
        if j = 500 then
        begin
          if CurrentThreadTaskTerminated then
            AbortEx;
          j := 0;
        end;
        Inc(j);

        OnBeforeNewRecord;
        for i := 0 to iFieldCount-1 do
          with FFieldInfo[i] do
            Pump(Field);
        OnAfterNewRecord;
        Inc(FRecCount);
        IBSQL.Next;
      end
    end

    else
    begin
      iValuesHigh := High(PkValues);
      while not IBSQL.Eof do
      begin
        // Process termination logic
        if j = 500 then
        begin
          if CurrentThreadTaskTerminated then
            AbortEx;
          j := 0;
        end;
        Inc(j);

        if FRecCount = iValuesALength then
        begin
          iValuesALength := iValuesALength* 2;
          for i := 0 to iValuesHigh do
            SetLength(PkValues[i].A, iValuesALength);
        end;
        for i := 0 to iValuesHigh do
          with PkValues[i] do
            A[FRecCount] := TIBXSQLVAR(Tag).AsInteger;
        OnBeforeNewRecord;
        for i := 0 to iFieldCount-1 do
          with FFieldInfo[i] do
            Pump(Field);
        OnAfterNewRecord;
        Inc(FRecCount);
        IBSQL.Next;
      end;

      for i := 0 to iValuesHigh do
        SetLength(PkValues[i].A, FRecCount);
    end;
  except
    on E: Exception do
    begin
      E.Message := 'Stopped on record '+ IntToStr(FRecCount)+ #13+ E.Message;
      raise;
    end;
  end;
  Result := TEvQueryResult.Create(sfOpenQuery, OnAfterGetData, PkValues);
  Result.DualStream.Position := 0;
end;

{ TEvIbSqlKbmMemTableProvider }

procedure TEvIbSqlKbmMemTableProvider.AfterConstruction;
begin
  FVersionSignature := $02FF;
  inherited;
end;

procedure TEvIbSqlKbmMemTableProvider.GetFieldPump(const Field: TIBXSQLVAR;
  out Pump: TEvIbFieldPump);
begin
  case Field.SQLType of
    SQL_VARYING,
    SQL_TEXT:
      if Field.Size = 1 then
        Pump := PumpChar
      else if Field.Size <= 255 then
        Pump := PumpShortString      
      else
        Pump := PumpString;

    SQL_INT64,
    SQL_DOUBLE:
      Pump := PumpDouble;

    SQL_LONG,
    SQL_SHORT:
      Pump := PumpInteger;

    SQL_TIMESTAMP:
      Pump := PumpDateTime;

    SQL_BLOB:
      Pump := PumpBlob;

    SQL_TYPE_DATE:
      Pump := PumpDateTime;
  else
    Assert(False);
  end;
end;

function TEvIbSqlKbmMemTableProvider.IsNull(const Field: TIBXSQLVAR): Boolean;
begin
  Result := Field.IsNull;
  FStream.WriteBoolean(Result);
end;

function TEvIbSqlKbmMemTableProvider.OnAfterGetData: IEvDualStream;
begin
  FStream.TruncateToPosition;
  FStream.Position := SizeOf(FVersionSignature);
  FStream.WriteInteger(FRecCount);
  FStream.Position := 0;
  Result := FStream;
end;

procedure TEvIbSqlKbmMemTableProvider.OnBeforeGetData(const IBSQL: TIBSQL);
var
  i: Integer;
begin
  FStream := TEvDualStreamHolder.Create;
  FStream.GrowChunkSize := 64 * 1024;

  //  ***** DATA HEADER ******

  FStream.WriteWord(FVersionSignature); // Version tag
  FStream.WriteInteger(0);              // Reserve record count

  // Field structure
  FStream.WriteInteger(IBSQL.FieldCount);
  for i := 0 to IBSQL.FieldCount-1 do
  begin
    FStream.WriteString(IBSQL.Fields[i].Name);

    case IBSQL.Fields[i].SQLType of
      SQL_VARYING,
      SQL_TEXT:
      begin
        FStream.WriteInteger(Ord(ftString));
        FStream.WriteInteger(IBSQL.Fields[i].Size);
      end;

      SQL_INT64,
      SQL_DOUBLE:
      begin
        FStream.WriteInteger(Ord(ftFloat));
        FStream.WriteInteger(0);
      end;

      SQL_LONG,
      SQL_SHORT:
      begin
        FStream.WriteInteger(Ord(ftInteger));
        FStream.WriteInteger(0);
      end;

      SQL_TYPE_DATE:
      begin
        FStream.WriteInteger(Ord(ftDate));
        FStream.WriteInteger(0);
      end;

      SQL_TIMESTAMP:
      begin
        FStream.WriteInteger(Ord(ftDateTime));
        FStream.WriteInteger(0);
      end;

      SQL_BLOB:
      begin
        FStream.WriteInteger(Ord(ftBlob));
        FStream.WriteInteger(0);
      end;

    else
      Assert(False, 'Unexpected SQLType = '+ IntToStr(IBSQL.Fields[i].SQLType));
    end;

    FStream.WriteBoolean(not IBSQL.Fields[i].IsNullable);
  end;
end;

procedure TEvIbSqlKbmMemTableProvider.PumpBlob(const Field: TIBXSQLVAR);
var
  s: String;
begin
  if not IsNull(Field) then
  begin
    if Assigned(FOnGetBlobData) then
      FOnGetBlobData(Field, s)
    else
      s := Field.AsString;
    FStream.WriteString(s);
  end;
end;

procedure TEvIbSqlKbmMemTableProvider.PumpChar(const Field: TIBXSQLVAR);
var
  s: String;
begin
  if not IsNull(Field) then
  begin
    s := Field.AsString;
    if s = '' then
      FStream.WriteByte(0)    
    else
      FStream.WriteByte(Ord(s[1]));
  end;
end;

procedure TEvIbSqlKbmMemTableProvider.PumpDateTime(const Field: TIBXSQLVAR);
begin
  if not IsNull(Field) then
    FStream.WriteDouble(Field.AsDateTime);
end;

procedure TEvIbSqlKbmMemTableProvider.PumpDouble(const Field: TIBXSQLVAR);
begin
  if not IsNull(Field) then
    FStream.WriteDouble(Field.AsDouble);
end;

procedure TEvIbSqlKbmMemTableProvider.PumpInteger(const Field: TIBXSQLVAR);
begin
  if not IsNull(Field) then
    FStream.WriteInteger(Field.AsInteger);
end;

procedure TEvIbSqlKbmMemTableProvider.PumpShortString(const Field: TIBXSQLVAR);
begin
  if not IsNull(Field) then
    FStream.WriteShortString(Field.AsString);
end;

procedure TEvIbSqlKbmMemTableProvider.PumpString(const Field: TIBXSQLVAR);
begin
  if not IsNull(Field) then
    FStream.WriteString(Field.AsString);
end;

procedure TEvIbSqlProvider.OnAfterNewRecord;
begin
end;

procedure TEvIbSqlProvider.OnBeforeNewRecord;
begin
end;

end.

