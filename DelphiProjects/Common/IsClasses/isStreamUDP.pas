unit isStreamUDP;

interface

uses Windows, Classes, SyncObjs, SysUtils, WinSock2, isSocket, EvStreamUtils,
     isBasicUtils, isBaseClasses, isExceptions, isStatistics;

type
    IisAsyncUDPStreamSocket = interface(IisAsyncUDPSocket)
    ['{59313A0E-59C2-4D23-A054-BF88A01D3D6F}']
      function  Send(const AStream: IEvDualStream): Boolean;
    end;

    TOnIncomingStream = procedure (const ASocket: IisAsyncUDPStreamSocket; const AStream: IEvDualStream; const ATransmissionTime: Cardinal) of object;
    TOnSendComplete    = procedure (const ASocket: IisAsyncUDPStreamSocket; const AStream: IEvDualStream) of object;

    IisUDPStreamCommunicator = interface
    ['{3A3FDD8F-77AB-4D19-828E-F65225AA4136}']
       function  SocketCount: Integer;
       function  GetSocket(AIndex: Integer): IisAsyncUDPStreamSocket;
       function  FindSocketByID(const ASocketID: TisGUID): IisAsyncUDPStreamSocket;
       procedure DestroySocket(const ASocketID: TisGUID);       
       function  GetOnSendComplete: TOnSendComplete;
       function  GetOnIncomingStream: TOnIncomingStream;
       procedure SetOnSendComplete(const Value: TOnSendComplete);
       procedure SetOnIncomingStream(const Value: TOnIncomingStream);
       property  OnIncomingStream: TOnIncomingStream read GetOnIncomingStream write SetOnIncomingStream;
       property  OnSendComplete: TOnSendComplete read GetOnSendComplete write SetOnSendComplete;
    end;


    // UDP Socket

    TisIncomingDataBuffer = class;
    TisOutgoingDataBuffer = class;

    TisAsyncUDPStreamSocket = class(TisAsyncUDPSocket, IisAsyncUDPStreamSocket)
    private
      FIncomingData: TisIncomingDataBuffer;    // Currently incoming data Stream
      FOutgoingData: TisOutgoingDataBuffer;    // Currently outgoing data Stream
      function  UDPStreamCommunicator: IisUDPStreamCommunicator;
    protected
      procedure  DoRead; override;
      procedure  DoWrite; override;
      function   Send(const AStream: IEvDualStream): Boolean;
    public
      constructor Create; override;
      destructor  Destroy; override;
    end;


    // UDP Peer

    TisAsyncUDPStreamPeer = class(TisAsyncUDPPeer, IisUDPStreamCommunicator)
    private
      FOnIncomingStream: TOnIncomingStream;
      FOnSendComplete: TOnSendComplete;
      function  GetOnSendComplete: TOnSendComplete;
      function  GetOnIncomingStream: TOnIncomingStream;
      procedure SetOnSendComplete(const AValue: TOnSendComplete);
      procedure SetOnIncomingStream(const AValue: TOnIncomingStream);
    protected
      function  SocketCount: Integer;
      function  GetSocket(AIndex: Integer): IisAsyncUDPStreamSocket;
      function  FindSocketByID(const ASocketID: TisGUID): IisAsyncUDPStreamSocket;
      function  GetSocketClass: TisAsyncUDPSocketClass; override;
      procedure DestroySocket(const ASocketID: TisGUID);
    end;


    // Abstract Data Buffer for transfering streams

    TisDataBuffer = class
    private
      FCS: TCriticalSection;
      FSocket: TisSocket;
      FBuffer: array of Byte;
      FStream: IEvDualStream;
      FComplete: Boolean;
      FStatEvent: IisSimpleStatEvent;
    public
      procedure   Lock;
      procedure   Unlock;
      procedure   PrepareForNextCycle;
      function    Complete: Boolean;
      function    Empty: Boolean;
      procedure   DoTransmission; virtual;
      constructor Create(const ASocket: TisSocket);
      destructor  Destroy; override;
    end;


    // Storage for unfinished incoming Stream

    TisIncomingDataBuffer = class(TisDataBuffer)
    public
      procedure DoTransmission; override;
    end;


   // Storage for unfinished outgoing Stream

    TisOutgoingDataBuffer = class(TisDataBuffer)
    public
      procedure DoTransmission; override;
    end;


implementation



{ TisAsyncUDPStreamSocket }

constructor TisAsyncUDPStreamSocket.Create;
begin
  FIncomingData := TisIncomingDataBuffer.Create(Self);
  FOutgoingData := TisOutgoingDataBuffer.Create(Self);
  inherited;
end;

destructor TisAsyncUDPStreamSocket.Destroy;
begin
  FreeAndNil(FIncomingData);
  FreeAndNil(FOutgoingData);
  inherited;
end;

procedure TisAsyncUDPStreamSocket.DoRead;
var
  S: IEvDualStream;
  bComplete: Boolean;
begin
  inherited;

  LockRead;
  try
    FIncomingData.DoTransmission;

    bComplete := FIncomingData.Complete;
    if bComplete then
    begin
      S := FIncomingData.FStream;
      S.Position := 0;
      FIncomingData.PrepareForNextCycle;
    end;

  finally
    UnLockRead;
  end;

  if bComplete and Assigned(UDPStreamCommunicator.OnIncomingStream) then
    UDPStreamCommunicator.OnIncomingStream(Self, S, FIncomingData.FStatEvent.Duration);
end;

procedure TisAsyncUDPStreamSocket.DoWrite;
var
  S: IEvDualStream;
  bComplete: Boolean;
begin
  inherited;
  LockWrite;
  try
    if FOutgoingData.Empty then
      Exit;

    FOutgoingData.DoTransmission;
    bComplete := FOutgoingData.Complete;
    if bComplete then
    begin
      S := FOutgoingData.FStream;
      FOutgoingData.PrepareForNextCycle;
    end;

  finally
    UnLockWrite;
  end;

  if bComplete and Assigned(UDPStreamCommunicator.OnSendComplete) then
    UDPStreamCommunicator.OnSendComplete(Self, S);
end;

function TisAsyncUDPStreamSocket.Send(const AStream: IEvDualStream): Boolean;
begin
  if AStream.Size > Length(FOutgoingData.FBuffer) then
    raise EisException.Create('Stream is to big for UDP');

  FOutgoingData.Lock;
  try
    if FOutgoingData.Empty then
    begin
      FOutgoingData.FStream := AStream;
      FOutgoingData.FStream.Position := 0; 
      SocketsGroup.GenerateSocketEvent(Self, FD_WRITE);
      Result := True;
    end
    else
      Result := False;
  finally
    FOutgoingData.Unlock;
  end;
end;

function TisAsyncUDPStreamSocket.UDPStreamCommunicator: IisUDPStreamCommunicator;
begin
  Result := TisInterfacedObject(SocketsGroup.Owner) as IisUDPStreamCommunicator;
end;

{ TisIncomingDataBuffer }

procedure TisIncomingDataBuffer.DoTransmission;
var
  Len: Cardinal;
begin
  Len := FSocket.Read(FBuffer[0], Length(FBuffer));
  if Len > 0 then
  begin
    FStatEvent.StartWatch;
    FStream := TEvDualStreamHolder.Create(Len);
    FStream.WriteBuffer(FBuffer[0], Len);
    FComplete := True;
    FStatEvent.StopWatch;    
  end;

  inherited;
end;


{ TisDataBuffer }

function TisDataBuffer.Complete: Boolean;
begin
  Result := not Empty and FComplete;
end;

constructor TisDataBuffer.Create(const ASocket: TisSocket);
begin
  FCS := TCriticalSection.Create;
  FSocket := ASocket;
  SetLength(FBuffer, 64 * 1024);
  FStatEvent := CreateSimpleStatEvent;
end;

destructor TisDataBuffer.Destroy;
begin
  FBuffer := nil;
  inherited;
  FreeAndNil(FCS);
end;

procedure TisDataBuffer.DoTransmission;
begin
end;

function TisDataBuffer.Empty: Boolean;
begin
  Lock;
  try
    Result := not Assigned(FStream);
  finally
    Unlock;
  end;
end;

procedure TisDataBuffer.Lock;
begin
  FCS.Enter;
end;

procedure TisDataBuffer.PrepareForNextCycle;
begin
  Lock;
  try
    FStream := nil;
    FComplete := False;
  finally
    Unlock;
  end;
end;

procedure TisDataBuffer.Unlock;
begin
  FCS.Leave;
end;

{ TisOutgoingDataBuffer }

procedure TisOutgoingDataBuffer.DoTransmission;
var
  Len: Integer;
begin
  if FStream.Position = 0 then
    FStream.ReadBuffer(FBuffer[0], FStream.Size);

  Len := FSocket.Write(FBuffer[0], FStream.Size);
  if Len = FStream.Size then
    FComplete := True;

  inherited;
end;


{ TisAsyncUDPStreamPeer }

function TisAsyncUDPStreamPeer.FindSocketByID(const ASocketID: TisGUID): IisAsyncUDPStreamSocket;
begin
  Result := FSocketsGroup.FindSocketByID(ASocketID) as IisAsyncUDPStreamSocket;
end;

function TisAsyncUDPStreamPeer.GetOnSendComplete: TOnSendComplete;
begin
  Result := FOnSendComplete;
end;

function TisAsyncUDPStreamPeer.GetOnIncomingStream: TOnIncomingStream;
begin
  Result := FOnIncomingStream;
end;

function TisAsyncUDPStreamPeer.GetSocketClass: TisAsyncUDPSocketClass;
begin
  Result := TisAsyncUDPStreamSocket;
end;

procedure TisAsyncUDPStreamPeer.SetOnSendComplete(const AValue: TOnSendComplete);
begin
  FOnSendComplete := AValue;
end;

procedure TisAsyncUDPStreamPeer.SetOnIncomingStream(const AValue: TOnIncomingStream);
begin
  FOnIncomingStream := AValue;
end;

function TisAsyncUDPStreamPeer.SocketCount: Integer;
begin
  Result := FSocketsGroup.Count;
end;

function TisAsyncUDPStreamPeer.GetSocket(AIndex: Integer): IisAsyncUDPStreamSocket;
begin
  Result := FSocketsGroup.Sockets[AIndex] as IisAsyncUDPStreamSocket;
end;

procedure TisAsyncUDPStreamPeer.DestroySocket(const ASocketID: TisGUID);
var
  Socket: IisAsyncUDPStreamSocket;
begin
  Socket := FindSocketByID(ASocketID);
  Close(Socket);
end;

end.