unit isLogFile;

interface

uses  Windows, Classes, SysUtils, SyncObjs, isBaseClasses, EvStreamUtils, IsBasicUtils,
      isThreadManager, isExceptions, TypInfo;

const
  DefaultPurgeRecThreshold = 5000;
  DefaultPurgeRecCount = 500;

type
  TLogEventType = (etUnknown, etInformation, etWarning, etError, etFatalError);
  TLogEventTypes = set of TLogEventType;


  ILogEvent = interface
  ['{A453B438-02F3-4693-B03F-7EDDE3E45CFF}']
    function GetNbr: Integer;
    function GetTimeStamp: TDateTime;
    function GetEventType: TLogEventType;
    function GetEventClass: String;
    function GetText: String;
    function GetDetails: String;
    property Nbr: Integer read GetNbr;
    property TimeStamp: TDateTime read GetTimeStamp;
    property EventType: TLogEventType read GetEventType;
    property EventClass: String read GetEventClass;
    property Text: String read GetText;
    property Details: String read GetDetails;
  end;


  PILogEvent = ^ILogEvent;

  TEventQuery = record
    StartTime: TDateTime;
    EndTime: TDateTime;
    Types: TLogEventTypes;
    EventClass: String;
    MaxCount: Integer;
  end;

  ILogFileCallback = interface
  ['{F974998C-CA94-4CF9-A8DA-24D7B10E4F81}']
    procedure NewEventLogged(const ALogEvent: ILogEvent);
  end;

  ILogFile = interface
  ['{CB168289-3B9C-4812-9737-7B0994DD3E8B}']
    function  GetFileName: String;
    function  AddEvent(const AEventType: TLogEventType; const AEventClass, AText, ADetails: String): ILogEvent;
    function  AddEventAs(const AEvent: ILogEvent): ILogEvent;
    function  Count: Integer;
    procedure Lock;
    procedure UnLock;
    function  GetPurgeRecCount: Integer;
    function  GetPurgeRecThreshold: Integer;
    procedure SetPurgeRecCount(const AValue: Integer);
    procedure SetPurgeRecThreshold(const AValue: Integer);
    procedure Clear;
    procedure RegisterCallback(const ACallback: ILogFileCallback);
    procedure UnregisterCallback(const ACallback: ILogFileCallback);
    function  GetItem(Index: Integer): ILogEvent;
    function  GetPage(const APageNbr, AItemsPerPage: Integer): IisList;  // list of ILogEvent
    function  GetEvents(const AQuery: TEventQuery): IisList;  // list of ILogEvent
    property  Items[Index: Integer]: ILogEvent read GetItem; default;
    property  FileName: String read GetFileName;
    property  PurgeRecThreshold: Integer read GetPurgeRecThreshold write SetPurgeRecThreshold;
    property  PurgeRecCount: Integer read GetPurgeRecCount write SetPurgeRecCount;
  end;


  TLogFile = class(TisInterfacedObject, ILogFile)
  private
    FFileRO: IEvDualStream;
    FIndexRO: IEvDualStream;
    FFileName: String;
    FFileIndexName: String;
    FInterProcessCS: IisLock;
    FEventQueue: IisList;
    FBackgroundTaskID: TTaskID;
    FCallbackList: IisList;
    FPurgeRecThreshold: Integer;
    FPurgeRecCount: Integer;

    procedure BackgroundFileWrite(const Params: TTaskParamList);
    procedure BackgroundLoadLog(const Params: TTaskParamList);
    procedure FlushQueue;
    procedure Initialize;
    function  GetBackgroundTaskID: TTaskID;
    function  FileRO: IEvDualStream;
    function  IndexRO: IEvDualStream;
    procedure CheckLogSize;
    function  GetItemStreamPos(Index: Integer): Integer;

  protected
    procedure DoOnConstruction; override;
    function  GetFileName: String;
    function  AddEvent(const AEventType: TLogEventType; const AEventClass, AText, ADetails: String): ILogEvent;
    function  AddEventAs(const AEvent: ILogEvent): ILogEvent;
    function  Count: Integer;
    function  GetItem(Index: Integer): ILogEvent;
    function  GetPage(const APageNbr, AItemsPerPage: Integer): IisList;  // list of ILogEvent
    function  GetEvents(const AQuery: TEventQuery): IisList;  // list of ILogEvent
    procedure Clear;
    procedure RegisterCallback(const ACallback: ILogFileCallback);
    procedure UnregisterCallback(const ACallback: ILogFileCallback);
    function  GetPurgeRecCount: Integer;
    function  GetPurgeRecThreshold: Integer;
    procedure SetPurgeRecCount(const AValue: Integer);
    procedure SetPurgeRecThreshold(const AValue: Integer);
  public
    constructor Create(const AFileName: String); reintroduce;
    destructor  Destroy; override;
  end;


  procedure OpenGlobalLogFile(const AFileName: String);
  procedure CloseGlobalLogFile;
  function  GlobalLogFile: ILogFile;
  procedure SetGlobalLogFile(const ALogFile: ILogFile);
  function  LogEventTypesToByte(const ALogEventTypes: TLogEventTypes): Byte;
  function  ByteToLogEventTypes(const AValue: Byte): TLogEventTypes;

implementation

var
  FGlobalLogFile: ILogFile;


type
  TLogEvent = class(TisInterfacedObject, ILogEvent)
  private
    FNbr: Integer;
    FTimeStamp: TDateTime;
    FEventType: TLogEventType;
    FEventClass: String;
    FText: String;
    FDetails: String;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetNbr: Integer;
    function  GetTimeStamp: TDateTime;
    function  GetEventType: TLogEventType;
    function  GetEventClass: String;
    function  GetText: String;
    function  GetDetails: String;
  public
    constructor CreateEmpty;
    constructor Create(const AEventType: TLogEventType; const AEventClass, AText, ADetails: String); reintroduce;
  end;


procedure SetGlobalLogFile(const ALogFile: ILogFile);
begin
  FGlobalLogFile := ALogFile;
end;


function GlobalLogFile: ILogFile;
begin
  Result := FGlobalLogFile;
end;


function  LogEventTypesToByte(const ALogEventTypes: TLogEventTypes): Byte;
var
  i: TLogEventType;
begin
  Result := 0;
  for i := Low(TLogEventType) to High(TLogEventType) do
    if i in ALogEventTypes then
      Result := Result or (1 shl Ord(i));
end;

function ByteToLogEventTypes(const AValue: Byte): TLogEventTypes;
var
  i: TLogEventType;
begin
  Result := [];
  for i := Low(TLogEventType) to High(TLogEventType) do
    if (AValue and (1 shl Ord(i))) <> 0 then
      Include(Result, i);
end;

procedure OpenGlobalLogFile(const AFileName: String);
var
  F: ILogFile;
begin
  CheckCondition(GlobalLogFile = nil, 'Log file is already opened');
  F := TLogFile.Create(AFileName);
  SetGlobalLogFile(F);
end;


procedure CloseGlobalLogFile;
begin
  SetGlobalLogFile(nil);
end;


{ TLogFile }

function TLogFile.AddEvent(const AEventType: TLogEventType; const AEventClass, AText, ADetails: String): ILogEvent;
var
  i: Integer;
begin
  Result := TLogEvent.Create(AEventType, AEventClass, AText, ADetails);

  Lock;
  try
    FEventQueue.Add(Result);
    FlushQueue;
  finally
    Unlock
  end;

  FCallbackList.Lock;
  try
    for i := 0 to FCallbackList.Count - 1 do
      (FCallbackList[i] as ILogFileCallback).NewEventLogged(Result);
  finally
    FCallbackList.Unlock;
  end;
end;

function TLogFile.AddEventAs(const AEvent: ILogEvent): ILogEvent;
var
  i: Integer;
begin
  Result := (AEvent as IisInterfacedObject).GetClone as ILogEvent;

  Lock;
  try
    FEventQueue.Add(Result);
    FlushQueue;
  finally
    Unlock
  end;

  FCallbackList.Lock;
  try
    for i := 0 to FCallbackList.Count - 1 do
      (FCallbackList[i] as ILogFileCallback).NewEventLogged(Result);
  finally
    FCallbackList.Unlock;
  end;
end;

procedure TLogFile.BackgroundFileWrite(const Params: TTaskParamList);
var
  Event: ILogEvent;
  i, N: Integer;
  FileRW: IEvDualStream;
  IndexRW: IEvDualStream;

  procedure OnExit;
  begin
    FileRW := nil;
    IndexRW := nil;
    FFileRO := nil;
    FIndexRO := nil;
    InterlockedExchange(Integer(FBackgroundTaskID), 0);
  end;

begin
  FInterProcessCS.Lock;
  try
    Lock;
    try
      CheckLogSize;
    finally
      Unlock;
    end;

    while True do
    begin
      N := FEventQueue.Count;

      if N > 0 then
      begin
        if not Assigned(FileRW) then
        begin
          try
            FileRW := TEvDualStreamHolder.CreateFromFile(FFileName, False, False, True);
            IndexRW := TEvDualStreamHolder.CreateFromFile(FFileIndexName, False, False, True);
          except
            // If files are not accesible for writing just ignore that
            OnExit;
            break;
          end;
        end;

        for i := 0 to N - 1 do
        begin
          Event := FEventQueue[i] as ILogEvent;

          IndexRW.Position := IndexRW.Size;
          IndexRW.WriteInteger(FileRW.Size);
          FileRW.Position := FileRW.Size;
          (Event as IisInterfacedObject).WriteToStream(FileRW);
        end;

        for i := 0 to N - 1 do
          FEventQueue.Delete(0);
      end;

      Lock;
      try
        if FEventQueue.Count = 0 then
        begin
          OnExit;
          break;
        end;
      finally
        Unlock;
      end;
    end;

  finally
    FInterProcessCS.Unlock;
  end;
end;

procedure TLogFile.BackgroundLoadLog(const Params: TTaskParamList);
begin
  try
    Initialize;
    BackgroundFileWrite(Params);
  finally
    InterlockedExchange(Integer(FBackgroundTaskID), 0);
  end;  
end;

procedure TLogFile.CheckLogSize;
var
  PurgePosition: Integer;
  RecordsToPurge: Integer;

  procedure Purge;
  var
    StreamRW, StreamRO: IevDualStream;
  begin
    StreamRW := TevDualStreamHolder.CreateFromNewFile(FFileName);
    StreamRO := TevDualStreamHolder.CreateFromFile(FFileName + '.purge', True, True);
    StreamRO.Position := PurgePosition;
    StreamRW.CopyFrom(StreamRO, StreamRO.Size - StreamRO.Position);
  end;

begin
  if (FPurgeRecThreshold = 0) or (FPurgeRecCount = 0) then
    Exit;

  if Count >= FPurgeRecThreshold then
  begin
    RecordsToPurge := FPurgeRecCount;
    if RecordsToPurge < Count - FPurgeRecThreshold then
      RecordsToPurge := Count - FPurgeRecThreshold;
    PurgePosition := GetItemStreamPos(RecordsToPurge);

    FFileRO := nil;
    FIndexRO := nil;
    DeleteFile(FFileName + '.purge');
    if RenameFile(FFileName, FFileName + '.purge') then
    begin
      try
        DeleteFile(FFileIndexName);
        Purge;
      except
        RenameFile(FFileName + '.purge', FFileName);
      end;
      DeleteFile(FFileName + '.purge');

      Initialize;
      AddEvent(etInformation, 'Internal', IntToStr(RecordsToPurge) + ' records were purged', '');
    end;
  end;
end;

procedure TLogFile.Clear;
begin
  Lock;
  try
    DeleteFile(FFileName);
    DeleteFile(FFileIndexName);
    Initialize;
  finally
    UnLock
  end;
end;

function TLogFile.Count: Integer;
begin
  Lock;
  try
    Result := IndexRO.Size div SizeOf(Integer);
  finally
    Unlock;
  end;
end;

constructor TLogFile.Create(const AFileName: String);
begin
  inherited Create(nil, True);

  FEventQueue := TisList.Create;
  FFileName := AFileName;
  FFileIndexName := FFileName + '.ind';
  FInterProcessCS := TisInterProcessLock.Create(FFileName);
  FPurgeRecThreshold := DefaultPurgeRecThreshold;
  FPurgeRecCount := DefaultPurgeRecCount;

  FBackgroundTaskID := GlobalThreadManager.RunTask(BackgroundLoadLog, Self, MakeTaskParams([]), 'Log File Initialization', tpLower);
  Sleep(50); // Cheap solution. Initialization should start working first
end;

destructor TLogFile.Destroy;
var
  T: TTaskID;
begin
  T := GetBackgroundTaskID;
  if (T <> 0) and GlobalThreadManagerIsActive then
    GlobalThreadManager.WaitForTaskEnd(T);

  inherited;
end;

procedure TLogFile.DoOnConstruction;
begin
  inherited;
  FCallbackList := TisList.Create;  
end;

function TLogFile.FileRO: IEvDualStream;
begin
  if not Assigned(FFileRO) then
    FFileRO := TEvDualStreamHolder.CreateFromFile(FFileName, False, True, True);
  Result := FFileRO;  
end;

procedure TLogFile.FlushQueue;
begin
  if GetBackgroundTaskID = 0 then
    if GlobalThreadManagerIsActive then
      FBackgroundTaskID := GlobalThreadManager.RunTask(BackgroundFileWrite, Self, MakeTaskParams([]), 'Log File Write', tpLowest)
    else
      BackgroundFileWrite(MakeTaskParams([]));
end;

function TLogFile.GetBackgroundTaskID: TTaskID;
begin
  InterlockedExchange(Integer(Result), FBackgroundTaskID);
end;

function TLogFile.GetEvents(const AQuery: TEventQuery): IisList;
var
  i: Integer;
  bOK: Boolean;
  Evt: ILogEvent;
begin
  Result := TisList.Create;
  if AQuery.MaxCount = 0 then
    Result.Capacity := 1000
  else
    Result.Capacity := AQuery.MaxCount;

  Lock;
  try
    for i := Count - 1 downto 0 do
    begin
      Evt := GetItem(i);
      bOK := (AQuery.StartTime = 0) or (Evt.TimeStamp <= AQuery.StartTime);
      bOK := bOK and ((AQuery.EndTime = 0) or (Evt.TimeStamp >= AQuery.EndTime));

      if not bOK then
        break;

      bOK := bOK and ((AQuery.Types = []) or (Evt.EventType in AQuery.Types));
      bOK := bOK and ((AQuery.EventClass = '') or AnsiSameText(Evt.EventClass, AQuery.EventClass));

      if bOK then
      begin
        Result.Add(Evt);
        if (AQuery.MaxCount > 0) and (Result.Count = AQuery.MaxCount) then
          break;
      end;
    end;
  finally
    UnLock;
  end;
end;

function TLogFile.GetFileName: String;
begin
  Result := FFileName;
end;

function TLogFile.GetItem(Index: Integer): ILogEvent;
begin
  Result := nil;
  Lock;
  try
    try
      if (Count <= Index) or (Index < 0) then
        Result := TLogEvent.Create(etInformation, 'Log File',
          'Event number is out of log range', 'Most likely log file was automatically truncated.')
      else
      begin
        Result := TLogEvent.CreateEmpty;
        FileRO.Position := GetItemStreamPos(Index);
        (Result as IisInterfacedObject).ReadFromStream(FileRO);
      end;

      TLogEvent((Result as IisInterfacedObject).GetImplementation).FNbr := Index;
    except
      on E: Exception do
        Result := TLogEvent.Create(etFatalError, 'Log File', 'Log file is corrupted', E.Message);
    end;
  finally
    Unlock
  end;
end;

function TLogFile.GetItemStreamPos(Index: Integer): Integer;
begin
  IndexRO.Position := SizeOf(Integer) * Index;
  Result := IndexRO.ReadInteger;
end;

function TLogFile.GetPage(const APageNbr, AItemsPerPage: Integer): IisList;
var
  i: Integer;
  BegNbr, EndNbr: Integer;
begin
  if APageNbr <= 0 then
    raise EisException.CreateFmt('Page number %d is out of bounds', [APageNbr]);
  Result := TisList.Create;
  Result.Capacity := AItemsPerPage;

  Lock;
  try
    BegNbr := Count - (APageNbr - 1) * AItemsPerPage - 1;
    EndNbr := BegNbr - AItemsPerPage + 1;
    if EndNbr < 0 then
      EndNbr := 0;

    if BegNbr > Count - 1 then
      raise EisException.CreateFmt('Page number %d is out of bounds', [APageNbr]);

    for i := BegNbr downto EndNbr do
      Result.Add(GetItem(i));
  finally
    UnLock;
  end;
end;

function TLogFile.GetPurgeRecCount: Integer;
begin
  Lock;
  try
    Result := FPurgeRecCount;
  finally
    Unlock;
  end;
end;

function TLogFile.GetPurgeRecThreshold: Integer;
begin
  Lock;
  try
    Result := FPurgeRecThreshold;
  finally
    Unlock;
  end;
end;

function TLogFile.IndexRO: IEvDualStream;
begin
  if not Assigned(FIndexRO) then
    FIndexRO := TEvDualStreamHolder.CreateFromFile(FFileIndexName, False, True, True);
  Result := FIndexRO;
end;

procedure TLogFile.Initialize;
var
  Event: ILogEvent;
  IndexRW: IEvDualStream;
  FileRW: IEvDualStream;
  iPos: Integer;
begin
  Lock;
  try
    if FileExists(FFileName) then
    begin
      FileRW := TEvDualStreamHolder.CreateFromFile(FFileName, False, False, True);
      if not FileExists(FFileIndexName) then
      begin
        IndexRW := TEvDualStreamHolder.CreateFromNewFile(FFileIndexName, False, True);
        while not FileRW.EOS do
        begin
          AbortIfCurrentThreadTaskTerminated;
          iPos := FileRW.Position;
          try
            Event := TLogEvent.CreateEmpty;
            (Event as IisInterfacedObject).ReadFromStream(FileRW);
          except
            FileRW.Size := iPos;

            // Add fatal error about log coruption
            Event := TLogEvent.Create(etFatalError, 'Log File', 'Log file has been truncated because of corruption.', '');
            IndexRW.Position := IndexRW.Size;
            IndexRW.WriteInteger(FileRW.Size);
            FileRW.Position := FileRW.Size;
            (Event as IisInterfacedObject).WriteToStream(FileRW);

            break;
          end;
          IndexRW.Position := IndexRW.Size;
          IndexRW.WriteInteger(iPos);
        end;
      end
    end

    else
    begin
      FileRW := TEvDualStreamHolder.CreateFromNewFile(FFileName, False, True);
      IndexRW := TEvDualStreamHolder.CreateFromNewFile(FFileIndexName, False, True);
    end;
  finally
    IndexRW := nil;
    FileRW := nil;
    Unlock;
  end;
end;

procedure TLogFile.RegisterCallback(const ACallback: ILogFileCallback);
begin
  FCallbackList.Add(ACallback);
end;

procedure TLogFile.SetPurgeRecCount(const AValue: Integer);
begin
  Lock;
  try
    FPurgeRecCount := AValue;
  finally
    Unlock;
  end;
end;

procedure TLogFile.SetPurgeRecThreshold(const AValue: Integer);
begin
  Lock;
  try
    FPurgeRecThreshold := AValue;
  finally
    Unlock;
  end;
end;

procedure TLogFile.UnregisterCallback(const ACallback: ILogFileCallback);
begin
  FCallbackList.Remove(ACallback);
end;

{ TLogEvent }

constructor TLogEvent.Create(const AEventType: TLogEventType; const AEventClass, AText, ADetails: String);
begin
  inherited Create;

  FTimeStamp := Now;
  FEventType := AEventType;
  FEventClass := AEventClass;
  FText := AText;
  FDetails := ADetails;
end;

constructor TLogEvent.CreateEmpty;
begin
  inherited Create;
end;

function TLogEvent.GetDetails: String;
begin
  Result := FDetails;
end;

function TLogEvent.GetEventClass: String;
begin
  Result := FEventClass;
end;

function TLogEvent.GetNbr: Integer;
begin
  Result := FNbr;
end;

function TLogEvent.GetEventType: TLogEventType;
begin
  Result := FEventType;
end;

function TLogEvent.GetText: String;
begin
  Result := FText;
end;

function TLogEvent.GetTimeStamp: TDateTime;
begin
  Result := FTimeStamp;
end;

procedure TLogEvent.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FTimeStamp := TDateTime(AStream.ReadDouble);
  FEventType := TLogEventType(AStream.ReadByte);
{$IFDEF EXCLUDE_REVISION}
  FEventClass := AStream.ReadString;
{$ELSE}
  FEventClass := AStream.ReadShortString;
{$ENDIF}
  FText := AStream.ReadString;
  FDetails := AStream.ReadString;
end;

procedure TLogEvent.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteDouble(FTimeStamp);
  AStream.WriteByte(Ord(FEventType));
{$IFDEF EXCLUDE_REVISION}
  AStream.WriteString(FEventClass);
{$ELSE}
  AStream.WriteShortString(FEventClass);
{$ENDIF}
  AStream.WriteString(FText);
  AStream.WriteString(FDetails);
end;

end.

