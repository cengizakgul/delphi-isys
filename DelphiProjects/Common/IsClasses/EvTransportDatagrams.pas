unit EvTransportDatagrams;

interface

uses Classes, SysUtils, Windows, isBaseClasses, EvStreamUtils, isBasicUtils,
     EvTransportInterfaces, isExceptions;

type
  TevDatagram = class(TisInterfacedObject, IevDatagram)
  private
    FHeader: IevDatagramHeader;
    FParams: IisListOfValues;
    FParamData: IisStream;
    FPeeper: Boolean;
    FTransmissionTime: Cardinal;
    FTransmittedBytes: Cardinal;
    constructor CreatePeeper(const AStream: IisStream);
    class function CheckSignature(const AStream: IisStream): Byte;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;
    function  IsError: Boolean;
    function  GetErrorText: String;
    function  GetException: Exception;
    function  Header: IevDatagramHeader;
    function  Params: IisListOfValues;
    procedure SetParamData(const AStream: IisStream);
    function  GetParamData: IisStream;
    function  GetTransmittedBytes: Cardinal;
    procedure SetTransmittedBytes(const AValue: Cardinal);
    function  GetTransmissionTime: Cardinal;
    procedure SetTransmissionTime(const AValue: Cardinal);
  public
    constructor Create(const AMethod: String); reintroduce;
    constructor CreateFromStream(const AStream: IisStream; const AKeepParamsStreamed: Boolean = False);
    constructor CreateResponseFor(const ASourceDatagramHeader: IevDatagramHeader);
    constructor CreateErrorResponseFor(const ASourceDatagramHeader: IevDatagramHeader; const AErrorText: String); overload;
    constructor CreateErrorResponseFor(const ASourceDatagramHeader: IevDatagramHeader; const AException: EisException); overload;
    class function GetDatagramHeader(const AStream: IisStream): IevDatagramHeader;
    class procedure PatchHeader(const AOriginalDatagram: IisStream; const AHeader: IevDatagramHeader);
  end;

implementation

const
   METHOD_RESULT = 'Result';
   METHOD_PARAM_ERROR = 'Error';
   METHOD_PARAM_EXCEPTION = 'Exception';

type
  TevDatagramHeader = class(TisInterfacedObject, IevDatagramHeader)
  private
    FProtocolVersion: Byte;
    FSequenceNbr: Int64;
    FSessionID: TisGUID;
    FContextID: TisGUID;
    FMethod: String;
    FUser: String;
    FPassword: String;
  protected
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;
    function  ProtocolVersion: Byte;
    function  GetSequenceNbr: Int64;
    function  GetSessionID: TisGUID;
    function  GetContextID: TisGUID;
    procedure SetSequenceNbr(const AValue: Int64);
    procedure SetSessionID(const AValue: TisGUID);
    procedure SetContextID(const AValue: TisGUID);
    function  GetMethod: String;
    procedure SetMethod(const AValue: String);
    function  GetUser: String;
    procedure SetUser(const AValue: String);
    function  GetPassword: String;
    procedure SetPassword(const AValue: String);
    function  GetIsResponse: Boolean;
    procedure Assign(const ASource: IevDatagramHeader);
    property  Method: String read GetMethod write SetMethod;
    property  User: String read GetUser write SetUser;
    property  Password: String read GetPassword write SetPassword;
  end;

  IevDatagramRegister = interface
  ['{BE478BAD-D231-4E13-9EC0-9A65A29B8542}']
    function  GetNextSequenceNbr: Int64;
  end;

  TevDatagramRegister = class(TisInterfacedObject, IevDatagramRegister)
  private
    FSequenceNbr: Int64;
  protected
    procedure DoOnConstruction; override;
  public
    function  GetNextSequenceNbr: Int64;
  end;

const
   DATAGRAM_SIGNATURE_HEADER = 'IS';
   DATAGRAM_SIGNATURE_PROTOCOL = '02';
   DATAGRAM_SIGNATURE = DATAGRAM_SIGNATURE_HEADER + DATAGRAM_SIGNATURE_PROTOCOL;
  
var
  FDatagramRegister: IevDatagramRegister;


function  DatagramRegister: IevDatagramRegister;
begin
  Result := FDatagramRegister;
end;


{ TevDatagram }

procedure TevDatagram.DoOnConstruction;
begin
  inherited;
  FHeader := TevDatagramHeader.Create;
  FParams := TisListOfValues.Create;
end;

class function TevDatagram.GetDatagramHeader(const AStream: IisStream): IevDatagramHeader;
var
  D: IevDatagram;
begin
  D := TevDatagram.CreatePeeper(AStream);
  AStream.Position := 0;
  Result := D.Header;
end;

function TevDatagram.Header: IevDatagramHeader;
begin
  Result := FHeader;
end;

procedure TevDatagram.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
var
  HeaderOffset: Integer;
begin
  TevDatagramHeader((FHeader as IisInterfacedObject).GetImplementation).FProtocolVersion := CheckSignature(AStream);
  HeaderOffset := AStream.ReadInteger;

  if FPeeper then
    AStream.Position := HeaderOffset
  else
  begin
    if Assigned(FParamData) then
    begin
      FParamData.Size := 0;
      FParamData.CopyFrom(AStream, HeaderOffset - AStream.Position);
      FParamData.Position := 0;
    end
    else
      FParams.ReadFromStream(AStream);
  end;

  (FHeader as IisInterfacedObject).ReadFromStream(AStream);
end;

procedure TevDatagram.WriteSelfToStream(const AStream: IisStream);
var
  HeaderOffset, p1, p2: Integer;
begin
  AStream.WriteBuffer(DATAGRAM_SIGNATURE[1], Length(DATAGRAM_SIGNATURE));

  p1 := AStream.Position;
  HeaderOffset := 0;
  AStream.WriteInteger(HeaderOffset); // reserve for header's offset

  if Assigned(FParamData) then
  begin
    FParamData.Position := 0;
    AStream.CopyFrom(FParamData, FParamData.Size);
  end
  else
    FParams.WriteToStream(AStream);

  HeaderOffset := AStream.Position;
  (FHeader as IisInterfacedObject).WriteToStream(AStream);

  p2 := AStream.Position;
  AStream.Position := p1;
  AStream.WriteInteger(HeaderOffset); // update header's offset
  AStream.Position := p2;
end;

constructor TevDatagram.Create(const AMethod: String);
begin
  inherited Create;
  FHeader.Method := AMethod;
  FHeader.SequenceNbr := 0;
end;

procedure TevDatagramRegister.DoOnConstruction;
begin
  inherited;
  InitLock;
end;

function TevDatagramRegister.GetNextSequenceNbr: Int64;
begin
  Lock;
  try
    Inc(FSequenceNbr);
    Result := FSequenceNbr;
    if FSequenceNbr = High(Int64) then
      FSequenceNbr := 1;
  finally
    Unlock;
  end;
end;

function TevDatagram.Params: IisListOfValues;
begin
  if Assigned(FParamData) then
  begin
    FParamData.Position := 0;
    FParams.ReadFromStream(FParamData);
    FParamData := nil;
  end;
  Result := FParams;
end;

constructor TevDatagram.CreateFromStream(const AStream: IisStream; const AKeepParamsStreamed: Boolean = False);
begin
  inherited Create;
  AStream.Position := 0;
  if AKeepParamsStreamed then
    FParamData := TisStream.Create;
  ReadFromStream(AStream);
end;

function TevDatagram.GetErrorText: String;
var
  Par: IisNamedValue;
begin
  Par := FParams.FindValue(METHOD_PARAM_ERROR);
  if Assigned(Par) then
    Result := Par.Value
  else
    Result := '';
end;

function TevDatagram.IsError: Boolean;
begin
  Result := Header.IsResponse and
           (FParams.ValueExists(METHOD_PARAM_ERROR) or FParams.ValueExists(METHOD_PARAM_EXCEPTION));
end;

constructor TevDatagram.CreatePeeper(const AStream: IisStream);
begin
  FPeeper := True;
  CreateFromStream(AStream);
end;

constructor TevDatagram.CreateResponseFor(const ASourceDatagramHeader: IevDatagramHeader);
var
  DHs, DHt: TevDatagramHeader;
begin
  inherited Create;
  DHs := TevDatagramHeader((ASourceDatagramHeader as IisInterfacedObject).GetImplementation);
  DHt := TevDatagramHeader((FHeader as IisInterfacedObject).GetImplementation);
  DHt.FSequenceNbr := DHs.FSequenceNbr;
  DHt.FSessionID := DHs.FSessionID;
  DHt.FContextID := DHs.FContextID;
  FHeader.Method := METHOD_RESULT + '.' + ASourceDatagramHeader.Method;
end;

class procedure TevDatagram.PatchHeader(const AOriginalDatagram: IisStream; const AHeader: IevDatagramHeader);
var
  HeaderOffset: Integer;
begin
  AOriginalDatagram.Position := 0;
  AOriginalDatagram.ReadByte;
  CheckSignature(AOriginalDatagram);
  HeaderOffset := AOriginalDatagram.ReadInteger;
  AOriginalDatagram.Position := HeaderOffset;
  (AHeader as IisInterfacedObject).WriteToStream(AOriginalDatagram);
end;

class function TevDatagram.CheckSignature(const AStream: IisStream): Byte;
var
  Signature: String;
  sProtocol: String;
begin
  SetLength(Signature, Length(DATAGRAM_SIGNATURE));
  AStream.ReadBuffer(Signature[1], Length(Signature));

  if not AnsiSameStr(Signature, DATAGRAM_SIGNATURE) then
    if StartsWith(Signature, DATAGRAM_SIGNATURE_HEADER, True) then
    begin
      sProtocol := Copy(Signature, Length(DATAGRAM_SIGNATURE_HEADER) + 1, Length(DATAGRAM_SIGNATURE_PROTOCOL));
      Result := StrToIntDef(sProtocol, 0);
      if (Result = 0) or (Result > StrToInt(DATAGRAM_SIGNATURE_PROTOCOL)) then
        raise EisException.CreateFmt('Protocol version %s is invalid. Expected version %s.', [Signature, DATAGRAM_SIGNATURE]);
    end
    else
      raise EisException.Create('Unknown data format')
  else
    Result := StrToInt(DATAGRAM_SIGNATURE_PROTOCOL);
end;

constructor TevDatagram.CreateErrorResponseFor(const ASourceDatagramHeader: IevDatagramHeader; const AErrorText: String);
begin
  CreateResponseFor(ASourceDatagramHeader);
  Params.AddValue(METHOD_PARAM_ERROR, AErrorText);
end;

function TevDatagram.GetException: Exception;
var
  Par: IisNamedValue;
  S: IisStream;
  Err: String;
begin
  Result := nil;

  Par := FParams.FindValue(METHOD_PARAM_EXCEPTION);
  if Assigned(Par) then
  begin
    S := IInterface(Par.Value) as IisStream;
    if Assigned(S) then
      Result := RestoreException(S);
  end;

  if not Assigned(Result) then
  begin
    Err := GetErrorText;
    if Err <> '' then
      Result := EisException.Create(Err);
  end;
end;

constructor TevDatagram.CreateErrorResponseFor(const ASourceDatagramHeader: IevDatagramHeader;
  const AException: EisException);
begin
  CreateErrorResponseFor(ASourceDatagramHeader, AException.Message); // todo: remove soon
  Params.AddValue(METHOD_PARAM_EXCEPTION, AException.GetStream);
end;

function TevDatagram.GetTransmittedBytes: Cardinal;
begin
  Result := FTransmittedBytes;
end;

procedure TevDatagram.SetTransmittedBytes(const AValue: Cardinal);
begin
  FTransmittedBytes := AValue;
end;

function TevDatagram.GetTransmissionTime: Cardinal;
begin
  Result := FTransmissionTime;
end;

procedure TevDatagram.SetTransmissionTime(const AValue: Cardinal);
begin
  FTransmissionTime := AValue;
end;

procedure TevDatagram.SetParamData(const AStream: IisStream);
begin
  FParamData := AStream;
end;

function TevDatagram.GetParamData: IisStream;
begin
  Result := FParamData;

  if not Assigned(Result) then
  begin
    Result := TisStream.Create;
    FParams.WriteToStream(Result);
  end;
end;

{ TevDatagramHeader }

function TevDatagramHeader.GetSessionID: TisGUID;
begin
  Result := FSessionID;
end;

function TevDatagramHeader.GetMethod: String;
begin
  Result := FMethod;
end;

function TevDatagramHeader.GetPassword: String;
begin
  Result := FPassword;
end;

function TevDatagramHeader.GetUser: String;
begin
  Result := FUser;
end;

procedure TevDatagramHeader.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
begin
  FSequenceNbr := AStream.ReadInt64;
  FSessionID := AStream.ReadShortString;
  FContextID := AStream.ReadShortString;
  FMethod := AStream.ReadShortString;
  FUser := AStream.ReadShortString;
  FPassword := AStream.ReadShortString;
end;

procedure TevDatagramHeader.WriteSelfToStream(const AStream: IisStream);
begin
  AStream.WriteInt64(FSequenceNbr);
  AStream.WriteShortString(FSessionID);
  AStream.WriteShortString(FContextID);
  AStream.WriteShortString(FMethod);
  AStream.WriteShortString(FUser);
  AStream.WriteShortString(FPassword);
end;

procedure TevDatagramHeader.SetMethod(const AValue: String);
begin
  FMethod := AValue;
end;

procedure TevDatagramHeader.SetPassword(const AValue: String);
begin
  FPassword := AValue;
end;

procedure TevDatagramHeader.SetUser(const AValue: String);
begin
  FUser := AValue;
end;

function TevDatagramHeader.GetIsResponse: Boolean;
begin
  Result := AnsiSameText(FMethod, METHOD_RESULT) or StartsWith(FMethod, METHOD_RESULT + '.');
end;

function TevDatagramHeader.GetSequenceNbr: Int64;
begin
  Result := FSequenceNbr;
end;

function TevDatagramHeader.GetContextID: TisGUID;
begin
  Result := FContextID;
end;

procedure TevDatagramHeader.SetContextID(const AValue: TisGUID);
begin
  FContextID := AValue;
end;

procedure TevDatagramHeader.SetSequenceNbr(const AValue: Int64);
begin
  if AValue = 0 then
    FSequenceNbr := FDatagramRegister.GetNextSequenceNbr
  else
    FSequenceNbr := AValue;
end;

procedure TevDatagramHeader.SetSessionID(const AValue: TisGUID);
begin
  FSessionID := AValue;
end;

function TevDatagramHeader.ProtocolVersion: Byte;
begin
  Result := FProtocolVersion;
end;

procedure TevDatagramHeader.Assign(const ASource: IevDatagramHeader);
begin
  FProtocolVersion := ASource.ProtocolVersion;
  FSequenceNbr := ASource.SequenceNbr;
  FSessionID := ASource.SessionID;
  FContextID := ASource.ContextID;
  FMethod := ASource.Method;
  FUser := ASource.User;
  FPassword := ASource.Password;
end;

initialization
  FDatagramRegister := TevDatagramRegister.Create;

end.
