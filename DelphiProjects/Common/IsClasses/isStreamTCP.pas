unit isStreamTCP;

interface

uses Windows, Classes, SyncObjs, SysUtils, WinSock2, isSocket, EvStreamUtils, isBasicUtils, isBaseClasses,
     isLogFile, isErrorUtils, isStatistics;

type
    // This header for any data. All extensions should be grown from here
    TisBasicDataHeader = packed record
      HeaderSize: Word;
    end;

    PTisBasicDataHeader = ^TisBasicDataHeader;

    // This one says what data Stream size is

    TisDataStreamHeader = packed record
      BasicHeader: TisBasicDataHeader;
      StreamDataSize: Cardinal;
    end;

    PTisDataStreamHeader = ^TisDataStreamHeader;

    TRawData = array of Byte;
    PTRawData = ^TRawData;

    TisDataDirection = (ddBoth, ddInbound, ddOutbound);

    IisTCPStreamAsyncConnection = interface(IisTCPSocket)
    ['{BC33F17D-5FE8-46FB-BD59-9D1900F4FE11}']
      function  GetDataDirection: TisDataDirection;
      procedure SetDataDirection(const AValue: TisDataDirection);
      function  Send(const AStream: IEvDualStream; const AHeader: String): Boolean;
      property  DataDirection: TisDataDirection read GetDataDirection write SetDataDirection;
    end;

    TOnIncomingHeader = procedure (const AConnection: IisTCPStreamAsyncConnection; const AHeader: String; var Accept: Boolean) of object;
    TOnIncomingStream = procedure (const AConnection: IisTCPStreamAsyncConnection; const AHeader: String; const AStream: IEvDualStream; const ATransmissionTime: Cardinal) of object;
    TOnSendComplete    = procedure (const AConnection: IisTCPStreamAsyncConnection;  const AHeader: String; const AStream: IEvDualStream; const AFailed: Boolean) of object;
    TConnectionNotifyEvent = procedure (const AConnection: IisTCPStreamAsyncConnection) of object;

    IisTCPStreamCommunicator = interface
    ['{9D4794AA-927C-4CB5-8FA2-F8784D25805A}']
       function  ConnectionCount: Integer;
       function  GetTransmissionTimeout: Integer;
       procedure SetTransmissionTimeout(const AValue: Integer);       
       function  GetConnection(AIndex: Integer): IisTCPStreamAsyncConnection;
       function  FindConnectionByID(const AConnectionID: TisGUID): IisTCPStreamAsyncConnection;
       function  GetAllConnections: IisList;
       procedure DestroyConnection(const AConnectionID: TisGUID);
       function  GetOnSendComplete: TOnSendComplete;
       function  GetOnIncomingHeader: TOnIncomingHeader;
       function  GetOnIncomingStream: TOnIncomingStream;
       procedure SetOnSendComplete(const Value: TOnSendComplete);
       procedure SetOnIncomingStream(const Value: TOnIncomingStream);
       procedure SetOnIncomingHeader(const Value: TOnIncomingHeader);
       function  GetOnConnect: TConnectionNotifyEvent;
       procedure SetOnConnect(const AValue: TConnectionNotifyEvent);
       function  GetOnDisconnect: TConnectionNotifyEvent;
       procedure SetOnDisconnect(const AValue: TConnectionNotifyEvent);
       function  GetConnectionsBySessionID(const ASessionID: TisGUID): IisList;
       procedure UpdateConnectionsSessionID(const OldSessionID, NewSessionID: TisGUID);
       property  Connections[Index: Integer]: IisTCPStreamAsyncConnection read GetConnection;
       property  TransmissionTimeout: Integer read GetTransmissionTimeout write SetTransmissionTimeout;
       property  OnConnect: TConnectionNotifyEvent read GetOnConnect write SetOnConnect;
       property  OnDisconnect: TConnectionNotifyEvent read GetOnDisconnect write SetOnDisconnect;
       property  OnIncomingHeader: TOnIncomingHeader read GetOnIncomingHeader write SetOnIncomingHeader;
       property  OnIncomingStream: TOnIncomingStream read GetOnIncomingStream write SetOnIncomingStream;
       property  OnSendComplete: TOnSendComplete read GetOnSendComplete write SetOnSendComplete;
    end;


    // TCP Socket

    TisIncomingDataBuffer = class;
    TisOutgoingDataBuffer = class;

    TisAsyncTCPStreamConnection = class(TisAsyncTCPSocket, IisTCPStreamAsyncConnection)
    private
      FDataDirection: TisDataDirection;
      FIncomingData: TisIncomingDataBuffer;    // Currently incoming data Stream
      FOutgoingData: TisOutgoingDataBuffer;    // Currently outgoing data Stream
      function  TCPStreamCommunicator: IisTCPStreamCommunicator;
    protected
      procedure  DoRead; override;
      procedure  DoWrite; override;
      procedure  DoClose; override;
      function   Send(const AStream: IEvDualStream; const AHeader: String): Boolean;
      function   GetDataDirection: TisDataDirection;
      procedure  SetDataDirection(const AValue: TisDataDirection);
    public
      constructor Create; override;
      destructor  Destroy; override;
    end;


    // TCP Server

    TisAsyncTCPStreamServer = class(TisAsyncTCPServer, IisTCPStreamCommunicator)
    private
      FTransmissionTimeout: Integer;
      FOnIncomingHeader: TOnIncomingHeader;
      FOnIncomingStream: TOnIncomingStream;
      FOnSendComplete: TOnSendComplete;
      FOnConnect: TConnectionNotifyEvent;
      FOnDisconnect: TConnectionNotifyEvent;
    protected
      procedure DoOnConstruction; override;
      function  GetOnSendComplete: TOnSendComplete;
      function  GetOnIncomingHeader: TOnIncomingHeader;
      procedure SetOnIncomingHeader(const AValue: TOnIncomingHeader);
      function  GetOnIncomingStream: TOnIncomingStream;
      procedure SetOnSendComplete(const AValue: TOnSendComplete);
      procedure SetOnIncomingStream(const AValue: TOnIncomingStream);
      function  GetOnConnect: TConnectionNotifyEvent;
      procedure SetOnConnect(const AValue: TConnectionNotifyEvent);
      function  GetOnDisconnect: TConnectionNotifyEvent;
      procedure SetOnDisconnect(const AValue: TConnectionNotifyEvent);
      function  ConnectionCount: Integer;
      function  GetTransmissionTimeout: Integer;
      procedure SetTransmissionTimeout(const AValue: Integer);
      function  GetConnection(AIndex: Integer): IisTCPStreamAsyncConnection;
      function  FindConnectionByID(const AConnectionID: TisGUID): IisTCPStreamAsyncConnection;
      function  GetAllConnections: IisList;
      function  GetClientSocketClass: TisAsyncTCPSocketClass; override;
      procedure DoOnConnectClient(ASocket: IisTCPSocket); override;
      procedure DoOnDisconnectClient(ASocket: IisTCPSocket); override;
      function  GetConnectionsBySessionID(const ASessionID: TisGUID): IisList;
      procedure UpdateConnectionsSessionID(const OldSessionID, NewSessionID: TisGUID);
      procedure DestroyConnection(const AConnectionID: TisGUID);
    end;


    // TCP Client

    TisAsyncTCPStreamClient = class(TisAsyncTCPClient, IisTCPStreamCommunicator)
    private
      FTransmissionTimeout: Integer;
      FOnIncomingHeader: TOnIncomingHeader;
      FOnIncomingStream: TOnIncomingStream;
      FOnSendComplete: TOnSendComplete;
      FOnConnect: TConnectionNotifyEvent;
      FOnDisconnect: TConnectionNotifyEvent;
      function  GetOnIncomingHeader: TOnIncomingHeader;
      procedure SetOnIncomingHeader(const AValue: TOnIncomingHeader);
      function  GetOnSendComplete: TOnSendComplete;
      function  GetOnIncomingStream: TOnIncomingStream;
      procedure SetOnSendComplete(const AValue: TOnSendComplete);
      procedure SetOnIncomingStream(const AValue: TOnIncomingStream);
      function  GetOnConnect: TConnectionNotifyEvent;
      procedure SetOnConnect(const AValue: TConnectionNotifyEvent);
      function  GetOnDisconnect: TConnectionNotifyEvent;
      procedure SetOnDisconnect(const AValue: TConnectionNotifyEvent);
    protected
      procedure DoOnConstruction; override;
      function  ConnectionCount: Integer;
      function  GetTransmissionTimeout: Integer;
      procedure SetTransmissionTimeout(const AValue: Integer);
      function  GetConnection(AIndex: Integer): IisTCPStreamAsyncConnection;
      function  FindConnectionByID(const AConnectionID: TisGUID): IisTCPStreamAsyncConnection;
      function  GetAllConnections: IisList;
      function  GetSocketClass: TisAsyncTCPSocketClass; override;
      procedure DoOnConnect(ASocket: IisTCPSocket); override;
      procedure DoOnDisconnect(ASocket: IisTCPSocket); override;
      function  GetConnectionsBySessionID(const ASessionID: TisGUID): IisList;
      procedure UpdateConnectionsSessionID(const OldSessionID, NewSessionID: TisGUID);
      procedure DestroyConnection(const AConnectionID: TisGUID);
    end;


    // Abstract Data Buffer for transfering streams

    TisDataBuffer = class
    private
      FCS: TCriticalSection;
      FSocket: TisSocket;
      FFragmentBuffer: array of Byte;
      FFragmentBufferSize: Cardinal;
      FBufferPos: Cardinal;
      FHeaderData: String;
      FStream: IEvDualStream;
      FLastTransmission: TDateTime;
      FStatEvent: IisSimpleStatEvent;
    public
      procedure   Lock;
      procedure   Unlock;
      procedure   PrepareForNextCycle;
      function    Complete: Boolean; virtual; abstract;
      function    Empty: Boolean;
      procedure   DoNextFragment; virtual; abstract;
      constructor Create(const ASocket: TisSocket); virtual;
      destructor  Destroy; override;
    end;


    // Storage for unfinished incoming Stream

    TisIncomingDataBuffer = class(TisDataBuffer)
    private
      FStreamDataSize: Cardinal;
      procedure   StartNewOperation;
    public
      function    Complete: Boolean; override;
      procedure   DoNextFragment; override;
      constructor Create(const ASocket: TisSocket); override;
    end;


   // Storage for unfinished outgoing Stream

    TisOutgoingDataBuffer = class(TisDataBuffer)
    private
      procedure   StartNewOperation;
    public
      function    Complete: Boolean; override;
      procedure   DoNextFragment; override;
      constructor Create(const ASocket: TisSocket); override;
    end;

var
  cStreamReadBufferSize: Cardinal = 64 * 1024;  // Max size of fragment of data Stream while transmission
  cStreamWriteBufferSize: Cardinal = 8 * 1024;  // Max size of fragment of data Stream while transmission


implementation

uses DateUtils;

const TRANSMISSION_TIMEOUT = 120; // seconds timeout


{ TisAsyncTCPStreamServer }

function TisAsyncTCPStreamServer.FindConnectionByID(const AConnectionID: TisGUID): IisTCPStreamAsyncConnection;
begin
  Result := FSocketsGroup.FindSocketByID(AConnectionID) as IisTCPStreamAsyncConnection;
end;

function TisAsyncTCPStreamServer.GetClientSocketClass: TisAsyncTCPSocketClass;
begin
  Result := TisAsyncTCPStreamConnection;
end;

function TisAsyncTCPStreamServer.GetOnSendComplete: TOnSendComplete;
begin
  Result := FOnSendComplete;
end;

function TisAsyncTCPStreamServer.GetOnIncomingStream: TOnIncomingStream;
begin
  Result := FOnIncomingStream;
end;

procedure TisAsyncTCPStreamServer.SetOnSendComplete(const AValue: TOnSendComplete);
begin
  FOnSendComplete := AValue;
end;

procedure TisAsyncTCPStreamServer.SetOnIncomingStream(const AValue: TOnIncomingStream);
begin
  FOnIncomingStream := AValue;
end;

function TisAsyncTCPStreamServer.ConnectionCount: Integer;
begin
  Result := FSocketsGroup.Count;
end;

function TisAsyncTCPStreamServer.GetConnection(AIndex: Integer): IisTCPStreamAsyncConnection;
begin
  Result := FSocketsGroup.Sockets[AIndex] as IisTCPStreamAsyncConnection;
end;

function TisAsyncTCPStreamServer.GetOnConnect: TConnectionNotifyEvent;
begin
  Result := FOnConnect;
end;

function TisAsyncTCPStreamServer.GetOnDisconnect: TConnectionNotifyEvent;
begin
  Result := FOnDisconnect;
end;

procedure TisAsyncTCPStreamServer.SetOnConnect(const AValue: TConnectionNotifyEvent);
begin
  FOnConnect := AValue;
end;

procedure TisAsyncTCPStreamServer.SetOnDisconnect(const AValue: TConnectionNotifyEvent);
begin
  FOnDisconnect := AValue;
end;

procedure TisAsyncTCPStreamServer.DoOnConnectClient(ASocket: IisTCPSocket);
begin
  inherited;
  if Assigned(FOnConnect) then
    FOnConnect(ASocket as IisTCPStreamAsyncConnection);
end;

procedure TisAsyncTCPStreamServer.DoOnDisconnectClient(ASocket: IisTCPSocket);
begin
  inherited;
  if Assigned(FOnDisconnect) then
    FOnDisconnect(ASocket as IisTCPStreamAsyncConnection);
end;

function TisAsyncTCPStreamServer.GetConnectionsBySessionID(const ASessionID: TisGUID): IisList;
begin
  Result := FSocketsGroup.GetSocketsBySessionID(ASessionID);
end;

procedure TisAsyncTCPStreamServer.UpdateConnectionsSessionID(const OldSessionID, NewSessionID: TisGUID);
var
  Connections: IisList;
  i: Integer;
begin
  Connections := GetConnectionsBySessionID(OldSessionID);
  for i := 0 to Connections.Count - 1 do
    (Connections[i] as IisTCPStreamAsyncConnection).SessionID := NewSessionID;
end;

procedure TisAsyncTCPStreamServer.DestroyConnection(const AConnectionID: TisGUID);
var
  Conn: IisTCPStreamAsyncConnection;
begin
  Conn := FindConnectionByID(AConnectionID);
  Disconnect(Conn);
end;

function TisAsyncTCPStreamServer.GetOnIncomingHeader: TOnIncomingHeader;
begin
  Result := FOnIncomingHeader;
end;

procedure TisAsyncTCPStreamServer.SetOnIncomingHeader(const AValue: TOnIncomingHeader);
begin
  FOnIncomingHeader := AValue;
end;

function TisAsyncTCPStreamServer.GetAllConnections: IisList;
begin
  Result := FSocketsGroup.GetAllSockets;
end;

function TisAsyncTCPStreamServer.GetTransmissionTimeout: Integer;
begin
  Result := FTransmissionTimeout;
end;

procedure TisAsyncTCPStreamServer.SetTransmissionTimeout(const AValue: Integer);
begin
  FTransmissionTimeout := AValue;
end;

procedure TisAsyncTCPStreamServer.DoOnConstruction;
begin
  inherited;
  FTransmissionTimeout := TRANSMISSION_TIMEOUT;
end;

{ TisAsyncTCPStreamConnection }

constructor TisAsyncTCPStreamConnection.Create;
begin
  FIncomingData := TisIncomingDataBuffer.Create(Self);
  FOutgoingData := TisOutgoingDataBuffer.Create(Self);
  inherited;
end;

destructor TisAsyncTCPStreamConnection.Destroy;
begin
  FreeAndNil(FIncomingData);
  FreeAndNil(FOutgoingData);
  inherited;
end;

procedure TisAsyncTCPStreamConnection.DoClose;
begin
  try
    if not FOutgoingData.Empty and not FOutgoingData.Complete and
       Assigned(TCPStreamCommunicator.OnSendComplete) then
     TCPStreamCommunicator.OnSendComplete(Self, FOutgoingData.FHeaderData, FOutgoingData.FStream, True);
  except
    on E: Exception do
      if GlobalLogFile <> nil then
        GlobalLogFile.AddEvent(etFatalError, 'Async Sockets', E.Message, GetStack(E));
  end;

  inherited;
end;

procedure TisAsyncTCPStreamConnection.DoRead;
var
  S: IEvDualStream;
  H: String;
  bComplete, bCloseHangedConnection: Boolean;
begin
  inherited;

  H := '';
  LockRead;
  try
    FIncomingData.DoNextFragment;
    if FIncomingData.FBufferPos > 0 then
      SocketsGroup.GenerateSocketEvent(Self, FD_READ);

    bComplete := FIncomingData.Complete;
    if bComplete then
    begin
      S := FIncomingData.FStream;
      S.Position := 0;
      H := FIncomingData.FHeaderData;
      FIncomingData.PrepareForNextCycle;
      FIncomingData.FStatEvent.StopWatch;
    end;

    bCloseHangedConnection := not bComplete and (FIncomingData.FLastTransmission <> 0) and
      (SecondsBetween(Now, FIncomingData.FLastTransmission) > TCPStreamCommunicator.TransmissionTimeout)

  finally
    UnLockRead;
  end;

  if bComplete then
  begin
    if Assigned(TCPStreamCommunicator.OnIncomingStream) then
      TCPStreamCommunicator.OnIncomingStream(Self, H, S, FIncomingData.FStatEvent.Duration);

    if IsSSLSocket then
      DoWrite;
  end

  else if bCloseHangedConnection then
  begin
    if GlobalLogFile <> nil then
      GlobalLogFile.AddEvent(etError, 'Async Sockets', 'Client connection was timed out',
        'Remote side did not send next data fragment within timeout (' +
        IntToStr(TCPStreamCommunicator.TransmissionTimeout) + ' sec). Connection is closed.');
    DoClose;
  end;
end;


procedure TisAsyncTCPStreamConnection.DoWrite;
var
  S: IEvDualStream;
  H: String;
  bComplete, bCloseHangedConnection: Boolean;
begin
  inherited;

  H := '';
  LockWrite;
  try
    if FOutgoingData.Empty then
      Exit;

    FOutgoingData.DoNextFragment;

    bComplete := FOutgoingData.Complete;

    if bComplete then
    begin
      S := FOutgoingData.FStream;
      H := FOutgoingData.FHeaderData;
      FOutgoingData.PrepareForNextCycle;
    end;

    bCloseHangedConnection := not bComplete and (FOutgoingData.FLastTransmission <> 0) and
     (SecondsBetween(Now, FOutgoingData.FLastTransmission) > TCPStreamCommunicator.TransmissionTimeout);

  finally
    UnLockWrite;
  end;


  if bComplete then
  begin
    if Assigned(TCPStreamCommunicator.OnSendComplete) then
      TCPStreamCommunicator.OnSendComplete(Self, H, S, False);
  end

  else if bCloseHangedConnection then
  begin
    if GlobalLogFile <> nil then
      GlobalLogFile.AddEvent(etError, 'Async Sockets', 'Client connection was timed out',
        'Remote side did not finish reading of previous data fragment within timeout (' +
        IntToStr(TCPStreamCommunicator.TransmissionTimeout) + ' sec). Connection is closed.');
    DoClose;
  end

  else if IsSSLSocket then
    SocketsGroup.GenerateSocketEvent(Self, FD_WRITE, 0, 100);
end;


function TisAsyncTCPStreamConnection.GetDataDirection: TisDataDirection;
begin
  Result := FDataDirection;
end;

function TisAsyncTCPStreamConnection.Send(const AStream: IEvDualStream; const AHeader: String): Boolean;
begin
  FOutgoingData.Lock;
  try
    if FOutgoingData.Empty then
    begin
      FOutgoingData.FStream := AStream;
      FOutgoingData.FStream.Position := 0; 
      FOutgoingData.FHeaderData := AHeader;
      SocketsGroup.GenerateSocketEvent(Self, FD_WRITE);
      Result := True;
    end
    else
      Result := False;
  finally
    FOutgoingData.Unlock;
  end;
end;

procedure TisAsyncTCPStreamConnection.SetDataDirection(const AValue: TisDataDirection);
begin
  FDataDirection := AValue;
end;

function TisAsyncTCPStreamConnection.TCPStreamCommunicator: IisTCPStreamCommunicator;
begin
  Result := TisInterfacedObject(SocketsGroup.Owner) as IisTCPStreamCommunicator;
end;

{ TisIncomingDataBuffer }

procedure TisIncomingDataBuffer.DoNextFragment;
var
  Len, LastPartLen: Cardinal;
begin
  if Empty then
  begin
    FStatEvent.StartWatch;
    StartNewOperation   // read data Stream header and prepare for transmission
  end

  else
  begin
    Len := FSocket.Read(FFragmentBuffer[0], Length(FFragmentBuffer));

    // What if in buffer we've got the end of current stream and the beggining of next stream?
    if Cardinal(FStream.Position) + Len > FStreamDataSize then
    begin
      LastPartLen := FStreamDataSize - Cardinal(FStream.Position);
      FStream.WriteBuffer(FFragmentBuffer[0], LastPartLen);
      Move(FFragmentBuffer[LastPartLen], FFragmentBuffer[0], Len - LastPartLen);  // Shift data to the left
      FBufferPos := Len - LastPartLen;
    end

    else
    begin
      FStream.WriteBuffer(FFragmentBuffer[0], Len);
      FBufferPos := 0;
    end;

    if Len > 0 then
      FLastTransmission := Now;
  end;
end;

function TisIncomingDataBuffer.Complete: Boolean;
begin
  Result := not Empty and (Cardinal(FStream.Position) = FStreamDataSize);
end;

procedure TisIncomingDataBuffer.StartNewOperation;
var
  Len: Cardinal;
  PHeader: PTisDataStreamHeader;
  n: Cardinal;
  bAccept: Boolean;
begin
  // Reading Stream header
  Len := FSocket.Read(FFragmentBuffer[FBufferPos], FFragmentBufferSize - FBufferPos);
  Inc(FBufferPos, Len);

  if Len > 0 then
    FLastTransmission := Now;

  PHeader := PTisDataStreamHeader(Addr(FFragmentBuffer[0]));

  if FBufferPos >= SizeOf(TisBasicDataHeader) then
  begin
    n := PHeader^.BasicHeader.HeaderSize;
    if n > FFragmentBufferSize then
      raise EisSocketError.Create(FSocket as IisSocket, 0, 0, 'Data header size is too big');

    if FBufferPos >= n then
    begin
      // Reading of header is complete

      Dec(n, SizeOf(TisDataStreamHeader));
      SetLength(FHeaderData, n);
      if n > 0 then
        Move(FFragmentBuffer[SizeOf(TisDataStreamHeader)], FHeaderData[1], n);
      if Assigned(TisAsyncTCPStreamConnection(FSocket).TCPStreamCommunicator.OnIncomingHeader) then
      begin
        bAccept := True;
        TisAsyncTCPStreamConnection(FSocket).TCPStreamCommunicator.OnIncomingHeader(
          FSocket as IisTCPStreamAsyncConnection, FHeaderData, bAccept);
        if not bAccept then
          raise EisSocketError.Create(FSocket as IisSocket, 0, 0, 'Data header is invalid');
      end;

      FStreamDataSize := PHeader^.StreamDataSize;

      FStream := TEvDualStreamHolder.Create(FStreamDataSize);

      // Add the rest of data to stream
      Len := FBufferPos - PHeader^.BasicHeader.HeaderSize;

      // What if in buffer we've got the end of current stream and the beggining of next stream?
      if Len > FStreamDataSize then
      begin
        FStream.WriteBuffer(FFragmentBuffer[PHeader^.BasicHeader.HeaderSize], FStreamDataSize);
        Dec(FBufferPos, PHeader^.BasicHeader.HeaderSize + FStreamDataSize);
        Move(FFragmentBuffer[PHeader^.BasicHeader.HeaderSize + FStreamDataSize], FFragmentBuffer[0], FBufferPos);
      end

      else
      begin
        if Len > 0 then
          FStream.WriteBuffer(FFragmentBuffer[PHeader^.BasicHeader.HeaderSize], Len);
        FBufferPos := 0;
      end;
    end;
  end;
end;

constructor TisIncomingDataBuffer.Create(const ASocket: TisSocket);
begin
  FFragmentBufferSize := cStreamReadBufferSize;
  inherited;
end;


{ TisDataBuffer }

constructor TisDataBuffer.Create(const ASocket: TisSocket);
begin
  FCS := TCriticalSection.Create;
  FSocket := ASocket;
  SetLength(FFragmentBuffer, FFragmentBufferSize);
  FStatEvent := CreateSimpleStatEvent;
end;

destructor TisDataBuffer.Destroy;
begin
  FFragmentBuffer := nil;
  inherited;
  FreeAndNil(FCS);
end;

function TisDataBuffer.Empty: Boolean;
begin
  Lock;
  try
    Result := not Assigned(FStream);
  finally
    Unlock;
  end;
end;

procedure TisDataBuffer.Lock;
begin
  FCS.Enter;
end;

procedure TisDataBuffer.PrepareForNextCycle;
begin
  Lock;
  try
    FStream := nil;
    FHeaderData := '';
    FLastTransmission := 0;
  finally
    Unlock;
  end;
end;

procedure TisDataBuffer.Unlock;
begin
  FCS.Leave;
end;

{ TisOutgoingDataBuffer }

function TisOutgoingDataBuffer.Complete: Boolean;
begin
  Result := not Empty and (FStream.Position = FStream.Size) and (FBufferPos = 0);
end;

constructor TisOutgoingDataBuffer.Create(const ASocket: TisSocket);
begin
  FFragmentBufferSize := cStreamWriteBufferSize;
  inherited;
end;

procedure TisOutgoingDataBuffer.DoNextFragment;
var
  Len, LastPartLen : Cardinal;
begin
  if FStream.Position = 0 then
    StartNewOperation;    // Write data Stream header and prepare for transmission

  repeat
    Len := FFragmentBufferSize - FBufferPos;
    if Cardinal(FStream.Size -  FStream.Position) < Len then
      Len := FStream.Size -  FStream.Position;

    if Len > 0 then
    begin
      FStream.ReadBuffer(FFragmentBuffer[FBufferPos], Len);
      Inc(FBufferPos, Len);
    end;

    Len := FSocket.Write(FFragmentBuffer[0], FBufferPos);

    if Len > 0 then
    begin
      FLastTransmission := Now;

      // If writing operation was not completed
      if Len < FBufferPos then
      begin
        LastPartLen := FBufferPos - Len;
        Move(FFragmentBuffer[Len], FFragmentBuffer[0], LastPartLen);  // Shift data to the left
        FBufferPos := LastPartLen;
      end
      else
        FBufferPos := 0;
    end
    else
      Len := 0;
  until (Len = 0) or Complete;
end;

procedure TisOutgoingDataBuffer.StartNewOperation;
var
  FHeader: TisDataStreamHeader;
begin
  // Write header
  FHeader.BasicHeader.HeaderSize := SizeOf(TisDataStreamHeader) + Length(FHeaderData);
  FHeader.StreamDataSize := FStream.Size;
  FBufferPos := 0;
  Move(FHeader, FFragmentBuffer[FBufferPos], SizeOf(FHeader));
  Inc(FBufferPos, FHeader.BasicHeader.HeaderSize);

  if Length(FHeaderData) > 0 then
    Move(FHeaderData[1], FFragmentBuffer[SizeOf(TisDataStreamHeader)], Length(FHeaderData));
end;


{ TisAsyncTCPStreamClient }

function TisAsyncTCPStreamClient.FindConnectionByID(const AConnectionID: TisGUID): IisTCPStreamAsyncConnection;
begin
  Result := FSocketsGroup.FindSocketByID(AConnectionID) as IisTCPStreamAsyncConnection;
end;

function TisAsyncTCPStreamClient.GetOnSendComplete: TOnSendComplete;
begin
  Result := FOnSendComplete;
end;

function TisAsyncTCPStreamClient.GetOnIncomingStream: TOnIncomingStream;
begin
  Result := FOnIncomingStream;
end;

function TisAsyncTCPStreamClient.GetSocketClass: TisAsyncTCPSocketClass;
begin
  Result := TisAsyncTCPStreamConnection;
end;

procedure TisAsyncTCPStreamClient.SetOnSendComplete(const AValue: TOnSendComplete);
begin
  FOnSendComplete := AValue;
end;

procedure TisAsyncTCPStreamClient.SetOnIncomingStream(const AValue: TOnIncomingStream);
begin
  FOnIncomingStream := AValue;
end;

function TisAsyncTCPStreamClient.ConnectionCount: Integer;
begin
  Result := FSocketsGroup.Count;
end;

function TisAsyncTCPStreamClient.GetConnection(AIndex: Integer): IisTCPStreamAsyncConnection;
begin
  Result := FSocketsGroup.Sockets[AIndex] as IisTCPStreamAsyncConnection;
end;

function TisAsyncTCPStreamClient.GetOnConnect: TConnectionNotifyEvent;
begin
  Result := FOnConnect;
end;

function TisAsyncTCPStreamClient.GetOnDisconnect: TConnectionNotifyEvent;
begin
  Result := FOnDisconnect;
end;

procedure TisAsyncTCPStreamClient.SetOnConnect(const AValue: TConnectionNotifyEvent);
begin
  FOnConnect := AValue;
end;

procedure TisAsyncTCPStreamClient.SetOnDisconnect(const AValue: TConnectionNotifyEvent);
begin
  FOnDisconnect := AValue;
end;

procedure TisAsyncTCPStreamClient.DoOnConnect(ASocket: IisTCPSocket);
begin
  inherited;
  if Assigned(FOnConnect) then
    FOnConnect(ASocket as IisTCPStreamAsyncConnection);
end;

procedure TisAsyncTCPStreamClient.DoOnDisconnect(ASocket: IisTCPSocket);
begin
  inherited;
  if Assigned(FOnDisconnect) then
    FOnDisconnect(ASocket as IisTCPStreamAsyncConnection);
end;

function TisAsyncTCPStreamClient.GetConnectionsBySessionID(const ASessionID: TisGUID): IisList;
begin
  Result := FSocketsGroup.GetSocketsBySessionID(ASessionID);
end;

procedure TisAsyncTCPStreamClient.UpdateConnectionsSessionID(const OldSessionID, NewSessionID: TisGUID);
var
  Connections: IisList;
  i: Integer;
begin
  Connections := GetConnectionsBySessionID(OldSessionID);
  for i := 0 to Connections.Count - 1 do
    (Connections[i] as IisTCPStreamAsyncConnection).SessionID := NewSessionID;
end;

procedure TisAsyncTCPStreamClient.DestroyConnection(const AConnectionID: TisGUID);
var
  Conn: IisTCPStreamAsyncConnection;
begin
  Conn := FindConnectionByID(AConnectionID);
  Disconnect(Conn);
end;

function TisAsyncTCPStreamClient.GetOnIncomingHeader: TOnIncomingHeader;
begin
  Result := FOnIncomingHeader;
end;

procedure TisAsyncTCPStreamClient.SetOnIncomingHeader(const AValue: TOnIncomingHeader);
begin
  FOnIncomingHeader := AValue;
end;

function TisAsyncTCPStreamClient.GetAllConnections: IisList;
begin
  Result := FSocketsGroup.GetAllSockets;
end;

procedure TisAsyncTCPStreamClient.DoOnConstruction;
begin
  inherited;
  FTransmissionTimeout := TRANSMISSION_TIMEOUT;
end;

function TisAsyncTCPStreamClient.GetTransmissionTimeout: Integer;
begin
  Result := FTransmissionTimeout;
end;

procedure TisAsyncTCPStreamClient.SetTransmissionTimeout(const AValue: Integer);
begin
  FTransmissionTimeout := AValue;
end;

end.
