
{********************************************************************************}
{                                                                                }
{                                XML Data Binding                                }
{                                                                                }
{         Generated on: 12/6/2004 5:50:55 PM                                     }
{       Generated from: E:\job\ISystems\ISClasses\isTestUtilsScriptWrapper.xml   }
{   Settings stored in: E:\job\ISystems\ISClasses\isTestUtilsScriptWrapper.xdb   }
{                                                                                }
{********************************************************************************}

unit isTestUtilsScriptWrapper;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTecontainerType = interface;
  IXMLScriptType = interface;
  IXMLObjectsType = interface;
  IXMLObjectType = interface;
  IXMLCommandsType = interface;
  IXMLCommandType = interface;
  IXMLMouseType = interface;
  IXMLKeyType = interface;
  IXMLPopupmenuType = interface;

{ IXMLTecontainerType }

  IXMLTecontainerType = interface(IXMLNode)
    ['{4BEA7A5A-FFA2-44AE-9B65-5427B0FDF45A}']
    { Property Accessors }
    function Get_Version: Integer;
    function Get_Script: IXMLScriptType;
    procedure Set_Version(Value: Integer);
    { Methods & Properties }
    property Version: Integer read Get_Version write Set_Version;
    property Script: IXMLScriptType read Get_Script;
  end;

{ IXMLScriptType }

  IXMLScriptType = interface(IXMLNode)
    ['{232DBE07-864E-49AA-9C37-2F571495E5B6}']
    { Property Accessors }
    function Get_Comment: WideString;
    function Get_Objects: IXMLObjectsType;
    function Get_Commands: IXMLCommandsType;
    procedure Set_Comment(Value: WideString);
    { Methods & Properties }
    property Comment: WideString read Get_Comment write Set_Comment;
    property Objects: IXMLObjectsType read Get_Objects;
    property Commands: IXMLCommandsType read Get_Commands;
  end;

{ IXMLObjectsType }

  IXMLObjectsType = interface(IXMLNodeCollection)
    ['{8B1A4E01-CEA6-4AE0-B268-FA131C18C71D}']
    { Property Accessors }
    function Get_Object_(Index: Integer): IXMLObjectType;
    { Methods & Properties }
    function Add: IXMLObjectType;
    function Insert(const Index: Integer): IXMLObjectType;
    property Object_[Index: Integer]: IXMLObjectType read Get_Object_; default;
  end;

{ IXMLObjectType }

  IXMLObjectType = interface(IXMLNode)
    ['{F31A9A7A-5069-4BB0-B557-FA9985D17735}']
    { Property Accessors }
    function Get_Id: Integer;
    function Get_Winname: WideString;
    function Get_Winclass: WideString;
    function Get_Delphi: Boolean;
    function Get_Zoomed: Boolean;
    function Get_Annotation: WideString;
    procedure Set_Id(Value: Integer);
    procedure Set_Winname(Value: WideString);
    procedure Set_Winclass(Value: WideString);
    procedure Set_Delphi(Value: Boolean);
    procedure Set_Zoomed(Value: Boolean);
    procedure Set_Annotation(Value: WideString);
    { Methods & Properties }
    property Id: Integer read Get_Id write Set_Id;
    property Winname: WideString read Get_Winname write Set_Winname;
    property Winclass: WideString read Get_Winclass write Set_Winclass;
    property Delphi: Boolean read Get_Delphi write Set_Delphi;
    property Zoomed: Boolean read Get_Zoomed write Set_Zoomed;
    property Annotation: WideString read Get_Annotation write Set_Annotation;
  end;

{ IXMLCommandsType }

  IXMLCommandsType = interface(IXMLNodeCollection)
    ['{AE322642-2074-4D90-84C7-CA8F428E8433}']
    { Property Accessors }
    function Get_Command(Index: Integer): IXMLCommandType;
    { Methods & Properties }
    function Add: IXMLCommandType;
    function Insert(const Index: Integer): IXMLCommandType;
    property Command[Index: Integer]: IXMLCommandType read Get_Command; default;
  end;

{ IXMLCommandType }

  IXMLCommandType = interface(IXMLNode)
    ['{D91214DA-FDF3-48C4-A630-DD1F253E1DCB}']
    { Property Accessors }
    function Get_Window_id: Integer;
    function Get_Delay: Integer;
    function Get_Comment: WideString;
    function Get_Mouse: IXMLMouseType;
    function Get_Key: IXMLKeyType;
    function Get_Popupmenu: IXMLPopupmenuType;
    procedure Set_Window_id(Value: Integer);
    procedure Set_Delay(Value: Integer);
    procedure Set_Comment(Value: WideString);
    { Methods & Properties }
    property Window_id: Integer read Get_Window_id write Set_Window_id;
    property Delay: Integer read Get_Delay write Set_Delay;
    property Comment: WideString read Get_Comment write Set_Comment;
    property Mouse: IXMLMouseType read Get_Mouse;
    property Key: IXMLKeyType read Get_Key;
    property Popupmenu: IXMLPopupmenuType read Get_Popupmenu;
  end;

{ IXMLMouseType }

  IXMLMouseType = interface(IXMLNode)
    ['{B0B84774-D045-4972-B169-868A408648DA}']
    { Property Accessors }
    function Get_Action: WideString;
    function Get_X: Integer;
    function Get_Y: Integer;
    function Get_Target_id: Integer;
    procedure Set_Action(Value: WideString);
    procedure Set_X(Value: Integer);
    procedure Set_Y(Value: Integer);
    procedure Set_Target_id(Value: Integer);
    { Methods & Properties }
    property Action: WideString read Get_Action write Set_Action;
    property X: Integer read Get_X write Set_X;
    property Y: Integer read Get_Y write Set_Y;
    property Target_id: Integer read Get_Target_id write Set_Target_id;
  end;

{ IXMLKeyType }

  IXMLKeyType = interface(IXMLNode)
    ['{3557266C-F372-4917-96A8-5A4D122043B0}']
    { Property Accessors }
    function Get_Action: WideString;
    function Get_Vk: Integer;
    function Get_Scan: Integer;
    procedure Set_Action(Value: WideString);
    procedure Set_Vk(Value: Integer);
    procedure Set_Scan(Value: Integer);
    { Methods & Properties }
    property Action: WideString read Get_Action write Set_Action;
    property Vk: Integer read Get_Vk write Set_Vk;
    property Scan: Integer read Get_Scan write Set_Scan;
  end;

{ IXMLPopupmenuType }

  IXMLPopupmenuType = interface(IXMLNode)
    ['{0958DD04-6C52-4C76-98B0-9CB39E123E08}']
    { Property Accessors }
    function Get_Path: WideString;
    procedure Set_Path(Value: WideString);
    { Methods & Properties }
    property Path: WideString read Get_Path write Set_Path;
  end;

{ Forward Decls }

  TXMLTecontainerType = class;
  TXMLScriptType = class;
  TXMLObjectsType = class;
  TXMLObjectType = class;
  TXMLCommandsType = class;
  TXMLCommandType = class;
  TXMLMouseType = class;
  TXMLKeyType = class;
  TXMLPopupmenuType = class;

{ TXMLTecontainerType }

  TXMLTecontainerType = class(TXMLNode, IXMLTecontainerType)
  protected
    { IXMLTecontainerType }
    function Get_Version: Integer;
    function Get_Script: IXMLScriptType;
    procedure Set_Version(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLScriptType }

  TXMLScriptType = class(TXMLNode, IXMLScriptType)
  protected
    { IXMLScriptType }
    function Get_Comment: WideString;
    function Get_Objects: IXMLObjectsType;
    function Get_Commands: IXMLCommandsType;
    procedure Set_Comment(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLObjectsType }

  TXMLObjectsType = class(TXMLNodeCollection, IXMLObjectsType)
  protected
    { IXMLObjectsType }
    function Get_Object_(Index: Integer): IXMLObjectType;
    function Add: IXMLObjectType;
    function Insert(const Index: Integer): IXMLObjectType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLObjectType }

  TXMLObjectType = class(TXMLNode, IXMLObjectType)
  protected
    { IXMLObjectType }
    function Get_Id: Integer;
    function Get_Winname: WideString;
    function Get_Winclass: WideString;
    function Get_Delphi: Boolean;
    function Get_Zoomed: Boolean;
    function Get_Annotation: WideString;
    procedure Set_Id(Value: Integer);
    procedure Set_Winname(Value: WideString);
    procedure Set_Winclass(Value: WideString);
    procedure Set_Delphi(Value: Boolean);
    procedure Set_Zoomed(Value: Boolean);
    procedure Set_Annotation(Value: WideString);
  end;

{ TXMLCommandsType }

  TXMLCommandsType = class(TXMLNodeCollection, IXMLCommandsType)
  protected
    { IXMLCommandsType }
    function Get_Command(Index: Integer): IXMLCommandType;
    function Add: IXMLCommandType;
    function Insert(const Index: Integer): IXMLCommandType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCommandType }

  TXMLCommandType = class(TXMLNode, IXMLCommandType)
  protected
    { IXMLCommandType }
    function Get_Window_id: Integer;
    function Get_Delay: Integer;
    function Get_Comment: WideString;
    function Get_Mouse: IXMLMouseType;
    function Get_Key: IXMLKeyType;
    function Get_Popupmenu: IXMLPopupmenuType;
    procedure Set_Window_id(Value: Integer);
    procedure Set_Delay(Value: Integer);
    procedure Set_Comment(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLMouseType }

  TXMLMouseType = class(TXMLNode, IXMLMouseType)
  protected
    { IXMLMouseType }
    function Get_Action: WideString;
    function Get_X: Integer;
    function Get_Y: Integer;
    function Get_Target_id: Integer;
    procedure Set_Action(Value: WideString);
    procedure Set_X(Value: Integer);
    procedure Set_Y(Value: Integer);
    procedure Set_Target_id(Value: Integer);
  end;

{ TXMLKeyType }

  TXMLKeyType = class(TXMLNode, IXMLKeyType)
  protected
    { IXMLKeyType }
    function Get_Action: WideString;
    function Get_Vk: Integer;
    function Get_Scan: Integer;
    procedure Set_Action(Value: WideString);
    procedure Set_Vk(Value: Integer);
    procedure Set_Scan(Value: Integer);
  end;

{ TXMLPopupmenuType }

  TXMLPopupmenuType = class(TXMLNode, IXMLPopupmenuType)
  protected
    { IXMLPopupmenuType }
    function Get_Path: WideString;
    procedure Set_Path(Value: WideString);
  end;

{ Global Functions }

function Gettecontainer(Doc: IXMLDocument): IXMLTecontainerType;
function Loadtecontainer(const FileName: WideString): IXMLTecontainerType;
function Newtecontainer: IXMLTecontainerType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Gettecontainer(Doc: IXMLDocument): IXMLTecontainerType;
begin
  Result := Doc.GetDocBinding('tecontainer', TXMLTecontainerType, TargetNamespace) as IXMLTecontainerType;
end;

function Loadtecontainer(const FileName: WideString): IXMLTecontainerType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('tecontainer', TXMLTecontainerType, TargetNamespace) as IXMLTecontainerType;
end;

function Newtecontainer: IXMLTecontainerType;
begin
  Result := NewXMLDocument.GetDocBinding('tecontainer', TXMLTecontainerType, TargetNamespace) as IXMLTecontainerType;
end;

{ TXMLTecontainerType }

procedure TXMLTecontainerType.AfterConstruction;
begin
  RegisterChildNode('script', TXMLScriptType);
  inherited;
end;

function TXMLTecontainerType.Get_Version: Integer;
begin
  Result := AttributeNodes['version'].NodeValue;
end;

procedure TXMLTecontainerType.Set_Version(Value: Integer);
begin
  SetAttribute('version', Value);
end;

function TXMLTecontainerType.Get_Script: IXMLScriptType;
begin
  Result := ChildNodes['script'] as IXMLScriptType;
end;

{ TXMLScriptType }

procedure TXMLScriptType.AfterConstruction;
begin
  RegisterChildNode('objects', TXMLObjectsType);
  RegisterChildNode('commands', TXMLCommandsType);
  inherited;
end;

function TXMLScriptType.Get_Comment: WideString;
begin
  Result := ChildNodes['comment'].Text;
end;

procedure TXMLScriptType.Set_Comment(Value: WideString);
begin
  ChildNodes['comment'].NodeValue := Value;
end;

function TXMLScriptType.Get_Objects: IXMLObjectsType;
begin
  Result := ChildNodes['objects'] as IXMLObjectsType;
end;

function TXMLScriptType.Get_Commands: IXMLCommandsType;
begin
  Result := ChildNodes['commands'] as IXMLCommandsType;
end;

{ TXMLObjectsType }

procedure TXMLObjectsType.AfterConstruction;
begin
  RegisterChildNode('object', TXMLObjectType);
  ItemTag := 'object';
  ItemInterface := IXMLObjectType;
  inherited;
end;

function TXMLObjectsType.Get_Object_(Index: Integer): IXMLObjectType;
begin
  Result := List[Index] as IXMLObjectType;
end;

function TXMLObjectsType.Add: IXMLObjectType;
begin
  Result := AddItem(-1) as IXMLObjectType;
end;

function TXMLObjectsType.Insert(const Index: Integer): IXMLObjectType;
begin
  Result := AddItem(Index) as IXMLObjectType;
end;

{ TXMLObjectType }

function TXMLObjectType.Get_Id: Integer;
begin
  Result := AttributeNodes['id'].NodeValue;
end;

procedure TXMLObjectType.Set_Id(Value: Integer);
begin
  SetAttribute('id', Value);
end;

function TXMLObjectType.Get_Winname: WideString;
begin
  Result := ChildNodes['winname'].Text;
end;

procedure TXMLObjectType.Set_Winname(Value: WideString);
begin
  ChildNodes['winname'].NodeValue := Value;
end;

function TXMLObjectType.Get_Winclass: WideString;
begin
  Result := ChildNodes['winclass'].Text;
end;

procedure TXMLObjectType.Set_Winclass(Value: WideString);
begin
  ChildNodes['winclass'].NodeValue := Value;
end;

function TXMLObjectType.Get_Delphi: Boolean;
begin
  Result := ChildNodes['delphi'].NodeValue;
end;

procedure TXMLObjectType.Set_Delphi(Value: Boolean);
begin
  ChildNodes['delphi'].NodeValue := Value;
end;

function TXMLObjectType.Get_Zoomed: Boolean;
begin
  Result := ChildNodes['zoomed'].NodeValue;
end;

procedure TXMLObjectType.Set_Zoomed(Value: Boolean);
begin
  ChildNodes['zoomed'].NodeValue := Value;
end;

function TXMLObjectType.Get_Annotation: WideString;
begin
  Result := ChildNodes['annotation'].Text;
end;

procedure TXMLObjectType.Set_Annotation(Value: WideString);
begin
  ChildNodes['annotation'].NodeValue := Value;
end;

{ TXMLCommandsType }

procedure TXMLCommandsType.AfterConstruction;
begin
  RegisterChildNode('command', TXMLCommandType);
  ItemTag := 'command';
  ItemInterface := IXMLCommandType;
  inherited;
end;

function TXMLCommandsType.Get_Command(Index: Integer): IXMLCommandType;
begin
  Result := List[Index] as IXMLCommandType;
end;

function TXMLCommandsType.Add: IXMLCommandType;
begin
  Result := AddItem(-1) as IXMLCommandType;
end;

function TXMLCommandsType.Insert(const Index: Integer): IXMLCommandType;
begin
  Result := AddItem(Index) as IXMLCommandType;
end;

{ TXMLCommandType }

procedure TXMLCommandType.AfterConstruction;
begin
  RegisterChildNode('mouse', TXMLMouseType);
  RegisterChildNode('key', TXMLKeyType);
  RegisterChildNode('popupmenu', TXMLPopupmenuType);
  inherited;
end;

function TXMLCommandType.Get_Window_id: Integer;
begin
  Result := AttributeNodes['window_id'].NodeValue;
end;

procedure TXMLCommandType.Set_Window_id(Value: Integer);
begin
  SetAttribute('window_id', Value);
end;

function TXMLCommandType.Get_Delay: Integer;
begin
  Result := AttributeNodes['delay'].NodeValue;
end;

procedure TXMLCommandType.Set_Delay(Value: Integer);
begin
  SetAttribute('delay', Value);
end;

function TXMLCommandType.Get_Comment: WideString;
begin
  Result := ChildNodes['comment'].Text;
end;

procedure TXMLCommandType.Set_Comment(Value: WideString);
begin
  ChildNodes['comment'].NodeValue := Value;
end;

function TXMLCommandType.Get_Mouse: IXMLMouseType;
begin
  Result := ChildNodes['mouse'] as IXMLMouseType;
end;

function TXMLCommandType.Get_Key: IXMLKeyType;
begin
  Result := ChildNodes['key'] as IXMLKeyType;
end;

function TXMLCommandType.Get_Popupmenu: IXMLPopupmenuType;
begin
  Result := ChildNodes['popupmenu'] as IXMLPopupmenuType;
end;

{ TXMLMouseType }

function TXMLMouseType.Get_Action: WideString;
begin
  Result := AttributeNodes['action'].Text;
end;

procedure TXMLMouseType.Set_Action(Value: WideString);
begin
  SetAttribute('action', Value);
end;

function TXMLMouseType.Get_X: Integer;
begin
  Result := AttributeNodes['x'].NodeValue;
end;

procedure TXMLMouseType.Set_X(Value: Integer);
begin
  SetAttribute('x', Value);
end;

function TXMLMouseType.Get_Y: Integer;
begin
  Result := AttributeNodes['y'].NodeValue;
end;

procedure TXMLMouseType.Set_Y(Value: Integer);
begin
  SetAttribute('y', Value);
end;

function TXMLMouseType.Get_Target_id: Integer;
begin
  Result := AttributeNodes['target_id'].NodeValue;
end;

procedure TXMLMouseType.Set_Target_id(Value: Integer);
begin
  SetAttribute('target_id', Value);
end;

{ TXMLKeyType }

function TXMLKeyType.Get_Action: WideString;
begin
  Result := AttributeNodes['action'].Text;
end;

procedure TXMLKeyType.Set_Action(Value: WideString);
begin
  SetAttribute('action', Value);
end;

function TXMLKeyType.Get_Vk: Integer;
begin
  Result := AttributeNodes['vk'].NodeValue;
end;

procedure TXMLKeyType.Set_Vk(Value: Integer);
begin
  SetAttribute('vk', Value);
end;

function TXMLKeyType.Get_Scan: Integer;
begin
  Result := AttributeNodes['scan'].NodeValue;
end;

procedure TXMLKeyType.Set_Scan(Value: Integer);
begin
  SetAttribute('scan', Value);
end;

{ TXMLPopupmenuType }

function TXMLPopupmenuType.Get_Path: WideString;
begin
  Result := AttributeNodes['path'].Text;
end;

procedure TXMLPopupmenuType.Set_Path(Value: WideString);
begin
  SetAttribute('path', Value);
end;

end. 