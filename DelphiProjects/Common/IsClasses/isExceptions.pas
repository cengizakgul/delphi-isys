unit isExceptions;

interface

uses SysUtils, Classes, Windows, isBaseClasses, isBasicUtils, EvStreamUtils;

type
  // Not classified error
  // all custom exceptions should be inherited from this ancestor
  EisException = class(Exception)
  private
    FErrorCode: Integer;
    FDetails: String;
    FStackInfo: String;
    FTimeStamp: TDateTime;
  protected
    procedure RestoreFromStream(const AStream: IisStream); virtual;   // read from stream
  public
    procedure AfterConstruction; override;
    function  GetStream: IisStream; virtual;                           // write to stream
    function  GetDatailedMessage: String;
    function  GetFullMessage: String;
    procedure AddStackIfEmpty;
    property  ErrorCode: Integer read FErrorCode write FErrorCode;
    property  TimeStamp: TDateTime read FTimeStamp write FTimeStamp;
    property  Details: String read FDetails write FDetails;
    property  StackInfo: String read FStackInfo write FStackInfo;

    constructor Create(const AMessage: string; const AIncludeStack: Boolean = False;
                       const AErrorCode: Integer = 0; const AHelpContext: Integer = 0); reintroduce;
    constructor CreateDetailed(const AMessage, ADetails: string; const AIncludeStack: Boolean = False;
                       const AErrorCode: Integer = 0; const AHelpContext: Integer = 0);
  end;


  // Wrapper for non EisException exceptions
  EisVCLException = class(EisException)
  private
    FVCLExceptClassName: String;
  protected  
    procedure RestoreFromStream(const AStream: IisStream); override;
  public
    function VCLExceptClassName: String;
    function GetStream: IisStream; override;
    constructor Create(const E: Exception); reintroduce;
  end;


  EisDumpAllThreads = class(EisException);

  EisSuspiciousBinary = class(EisException);

  EisStreamingException = class(EisException);

  // R/O exception holder
  IisException = interface
  ['{3B4E4118-8D80-4EF2-9007-9B1D279A9835}']
    function ClassName: String;
    function Message: String;
    function ErrorCode: Integer;
    function TimeStamp: TDateTime;
    function Details: String;
    function StackInfo: String;
    function CreateException: Exception;
  end;

  procedure RegisterExceptions(const AExceptClasses: array of ExceptClass);
  procedure UnregisterExceptions(const AExceptClasses: array of ExceptClass);
  function  RestoreException(const AException: IisStream): Exception;
  function  SnapshotException(const E: Exception): IisException;

implementation

uses isErrorUtils;

var
  ExceptionRegistry: IisListOfValues;

type
  TisException = class(TisInterfacedObject, IisException)
  private
    FClassName: String;
    FMessage:   String;
    FErrorCode: Integer;
    FDetails:   String;
    FStackInfo: String;
    FTimeStamp: TDateTime;
  protected
    function ClassName: String;
    function Message: String;
    function ErrorCode: Integer;
    function TimeStamp: TDateTime;
    function Details: String;
    function StackInfo: String;
    function CreateException: Exception;
  public
    constructor Create(const E: Exception); reintroduce;
  end;


procedure RegisterExceptions(const AExceptClasses: array of ExceptClass);
var
  i: Integer;
begin
  ExceptionRegistry.Lock;
  try
    for i := Low(AExceptClasses) to High(AExceptClasses) do
      ExceptionRegistry.AddValue(AExceptClasses[i].ClassName, Integer(AExceptClasses[i]));
  finally
    ExceptionRegistry.Unlock;
  end;
end;

procedure UnregisterExceptions(const AExceptClasses: array of ExceptClass);
var
  i: Integer;
begin
  ExceptionRegistry.Lock;
  try
    for i := Low(AExceptClasses) to High(AExceptClasses) do
      ExceptionRegistry.DeleteValue(AExceptClasses[i].ClassName);
  finally
    ExceptionRegistry.Unlock;
  end;
end;


function RestoreException(const AException: IisStream): Exception;
var
  sExecptionClassName: String;
  tV: IisNamedValue;
  Cl: ExceptClass;
begin
  AException.Position := 0;
  sExecptionClassName := AException.ReadShortString;

  tV := ExceptionRegistry.FindValue(sExecptionClassName);
  if Assigned(tV) then
    Cl := ExceptClass(Pointer(Integer(tV.Value)))
  else
    Cl := EisException;

  Result := Cl.Create('');
  (Result as EisException).RestoreFromStream(AException);
end;


function  SnapshotException(const E: Exception): IisException;
begin
  Result := TisException.Create(E);
end;


{ EisException }

procedure EisException.AddStackIfEmpty;
begin
  if StackInfo = '' then
    StackInfo := GetStack; 
end;

procedure EisException.AfterConstruction;
begin
  inherited;
  FTimeStamp := Now;
end;

constructor EisException.Create(const AMessage: string; const AIncludeStack: Boolean;
  const AErrorCode, AHelpContext: Integer);
begin
  CreateDetailed(AMessage, '', AIncludeStack, AErrorCode, AHelpContext);
end;

constructor EisException.CreateDetailed(const AMessage, ADetails: string;
  const AIncludeStack: Boolean; const AErrorCode, AHelpContext: Integer);
begin
  inherited CreateHelp(AMessage, AHelpContext);
  FDetails := ADetails;
  FErrorCode := AErrorCode;
  if AIncludeStack then
    FStackInfo := GetStack(Self, ExceptAddr);
end;

function EisException.GetDatailedMessage: String;
begin
  Result := Message;
  if Details <> '' then
    AddStrValue(Result, Details, #13);
end;

function EisException.GetFullMessage: String;
begin
  Result := GetDatailedMessage;
  if StackInfo <> '' then
    AddStrValue(Result, StackInfo, #13);
end;

function EisException.GetStream: IisStream;
begin
  Result := TevDualStreamHolder.Create;
  Result.WriteShortString(ClassName);
  Result.WriteInteger(ErrorCode);
  Result.WriteInteger(HelpContext);
  Result.WriteString(Message);
  Result.WriteDouble(FTimeStamp);
  Result.WriteString(FDetails);
  Result.WriteString(FStackInfo);
end;

procedure EisException.RestoreFromStream(const AStream: IisStream);
begin
  // Class name item has been read on caller lavel
  ErrorCode := AStream.ReadInteger;
  HelpContext := AStream.ReadInteger;
  Message := AStream.ReadString;
  FTimeStamp := AStream.ReadDouble;
  FDetails := AStream.ReadString;
  FStackInfo := AStream.ReadString;
end;

{ EisVCLException }

constructor EisVCLException.Create(const E: Exception);
begin
  FVCLExceptClassName := E.ClassName;
  CreateDetailed(E.Message, '', False, 0, E.HelpContext);
end;

function EisVCLException.GetStream: IisStream;
begin
  Result := inherited GetStream;
  Result.WriteShortString(FVCLExceptClassName);
end;

procedure EisVCLException.RestoreFromStream(const AStream: IisStream);
begin
  inherited;
  FVCLExceptClassName := AStream.ReadShortString;
end;

function EisVCLException.VCLExceptClassName: String;
begin
  Result := FVCLExceptClassName;
end;

{ TisException }

function TisException.ClassName: String;
begin
  Result := FClassName;
end;

constructor TisException.Create(const E: Exception);
begin
  inherited Create;
  FClassName := E.ClassName;
  FMessage := E.Message;

  if E is EisException then
  begin
    FErrorCode := EisException(E).ErrorCode;
    FDetails := EisException(E).Details;
    FStackInfo := EisException(E).StackInfo;
    FTimeStamp := EisException(E).TimeStamp;
  end
  else
    FTimeStamp := Now;
end;

function TisException.Details: String;
begin
  Result := FDetails;
end;

function TisException.ErrorCode: Integer;
begin
  Result := FErrorCode;
end;

function TisException.Message: String;
begin
  Result := FMessage;
end;

function TisException.CreateException: Exception;
var
  tvItem: IisNamedValue;
  Cl: ExceptClass;
begin
  tvItem := ExceptionRegistry.FindValue(ClassName);
  if Assigned(tvItem) then
    Cl := ExceptClass(Pointer(Integer(tvItem.Value)))
  else
    Cl := EisException;

  Result := Cl.Create(Message);

  if Result is EisException then
  begin
    EisException(Result).ErrorCode := ErrorCode;
    EisException(Result).Details := Details;
    EisException(Result).StackInfo := StackInfo;
    EisException(Result).TimeStamp := TimeStamp;
  end;
end;

function TisException.StackInfo: String;
begin
  Result := FStackInfo;
end;

function TisException.TimeStamp: TDateTime;
begin
  Result := FTimeStamp;
end;

initialization
  ExceptionRegistry := TisListOfValues.Create;
  RegisterExceptions([EisException, EisVCLException]);
end.
