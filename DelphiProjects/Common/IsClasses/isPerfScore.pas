unit isPerfScore;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}
{$OPTIMIZATION ON}

interface

uses Classes, Windows, SysUtils, isThreadManager, isBasicUtils;

type
  TisTestResult = record
    Counter: Cardinal;
    Time: Extended;
  end;

  TisScoreMeter = class
  private
    function RunCalc(const ATask: PTaskProc; const ACalcTime: Cardinal; const AThreads: Cardinal): TisTestResult;
  public
    function CPUScore: Cardinal;
    function MEMScore: Cardinal;
    function IOScore: Cardinal;
  end;


implementation

{ TisScoreMeter }

function TisScoreMeter.CPUScore: Cardinal;

  procedure CalcCPUSpeed(const Params: TTaskParamList);
  var
    i, j, n: Integer;
    a, b: Double;
    Counter: PCardinal;
    RunTime: Cardinal;
    StartEvent: THandle;
    PerfCounterB, PerfCounterE, PerfCounterFreq: Int64;
  begin
    Counter := PCardinal(Integer(Params[0]));
    RunTime := Params[1];
    StartEvent := Params[2];
    n := 0;
    QueryPerformanceFrequency(PerfCounterFreq);

    a := 2.34;
    b := 2.34;

    WaitForSingleObject(StartEvent, INFINITE);
    QueryPerformanceCounter(PerfCounterB);

    repeat
      for i := 1 to 200 do
      begin
        for j := 1 to 20 do
        begin
          // One step is "EVOP" (Evolution Operation)
          a := a * b;
          a := a / b;
          a := a + b;
          a := a - b;
        end;
        Inc(n);
      end;

      QueryPerformanceCounter(PerfCounterE);
    until ((PerfCounterE - PerfCounterB) / PerfCounterFreq  * 1000) >= RunTime;

    Counter^ := n;
  end;

var
  Res: TisTestResult;
begin
  Res := RunCalc(@CalcCPUSpeed, 1000, HowManyProcessors * 2);
  Result := Round(Res.Counter / Res.Time);
end;

function TisScoreMeter.IOScore: Cardinal;

  procedure CalcIOSpeed(const Params: TTaskParamList);
  const
    DataSize = 1024 * 1024;
  var
    n: Integer;
    FileName: String;
    Counter: PCardinal;
    RunTime: Cardinal;
    StartEvent: THandle;
    PerfCounterB, PerfCounterE, PerfCounterFreq: Int64;
    F: TFileStream;
    B: PChar;
  begin
    Counter := PCardinal(Integer(Params[0]));
    RunTime := Params[1];
    StartEvent := Params[2];
    n := 0;
    ForceDirectories(AppTempFolder);
    FileName := AppTempFolder + GetUniqueID + '.tmp';

    QueryPerformanceFrequency(PerfCounterFreq);
    GetMem(B, DataSize);

    WaitForSingleObject(StartEvent, INFINITE);
    QueryPerformanceCounter(PerfCounterB);
    repeat
      F := TFileStream.Create(FileName, fmCreate);
      F.Write(B^, DataSize);
      F.Free;

      F := TFileStream.Create(FileName, fmOpenRead);
      F.Read(B^, F.Size);
      F.Free;

      DeleteFile(FileName);

      Inc(n);

      QueryPerformanceCounter(PerfCounterE);
    until ((PerfCounterE - PerfCounterB) / PerfCounterFreq  * 1000) >= RunTime;

    FreeMem(B);
    Counter^ := n;
  end;

var
  Res: TisTestResult;
begin
  Res := RunCalc(@CalcIOSpeed, 1000, 1);
  Result := Round(Res.Counter * (1000 / Res.Time));
end;

function TisScoreMeter.MEMScore: Cardinal;

  procedure CalcMemSpeed(const Params: TTaskParamList);
  const
    BlockSize = 1024;
  var
    i, n: Integer;
    P1, P2: PChar;
    Counter: PCardinal;
    RunTime: Cardinal;
    StartEvent: THandle;
    PerfCounterB, PerfCounterE, PerfCounterFreq: Int64;
  begin
    Counter := PCardinal(Integer(Params[0]));
    RunTime := Params[1];
    StartEvent := Params[2];
    n := 0;
    QueryPerformanceFrequency(PerfCounterFreq);

    P1 := GlobalAllocPtr(2, BlockSize);
    WaitForSingleObject(StartEvent, INFINITE);

    QueryPerformanceCounter(PerfCounterB);
    repeat
      for i := 1 to 100 do
      begin
        // One step is "EVOP" (Evolution Operation)
        P2 := GlobalAllocPtr(2, BlockSize);
        Move(P1^, P2^, BlockSize);
        GlobalFreePtr(P2);

        Inc(n);
      end;

      QueryPerformanceCounter(PerfCounterE);
    until ((PerfCounterE - PerfCounterB) / PerfCounterFreq  * 1000) >= RunTime;

    GlobalFreePtr(P1);
    Counter^ := n;
  end;

var
  Res: TisTestResult;
begin
  Res := RunCalc(@CalcMemSpeed, 1000, 1);
  Result := Round(Res.Counter / Res.Time);
end;

function TisScoreMeter.RunCalc(const ATask: PTaskProc; const ACalcTime: Cardinal;
  const AThreads: Cardinal): TisTestResult;
var
  StartEvent: THandle;
  i: Integer;
  PerfCounterB, PerfCounterE, PerfCounterFreq: Int64;
  Res: array of record
                  TaskID:  TTaskID;
                  Counter: Cardinal;
                end;
begin
  StartEvent := CreateEvent(nil, True, False, nil);
  try
    // create as many threads as many CPUs
    SetLength(Res, AThreads);
    for i := Low(Res) to High(Res) do
      Res[i].TaskID := GlobalThreadManager.RunTask(ATask,
        MakeTaskParams([@Res[i].Counter, ACalcTime, StartEvent]), 'Score Calc', tpTimeCritical);

     Sleep(10); // let all threads to get to the starting point

     Win32Check(QueryPerformanceFrequency(PerfCounterFreq));
     QueryPerformanceCounter(PerfCounterB);
     SetEvent(StartEvent); // Run!

     // Wait for the end of threads
     for i := Low(Res) to High(Res) do
       GlobalThreadManager.WaitForTaskEnd(Res[i].TaskID);

     QueryPerformanceCounter(PerfCounterE);
     Result.Time := (PerfCounterE - PerfCounterB) / PerfCounterFreq * 1000;
  finally
    CloseHandle(StartEvent);
  end;

  Result.Counter := 0;
  for i := Low(Res) to High(Res) do
    Inc(Result.Counter, Res[i].Counter);
end;


end.