unit ISNamedPipeStreams;

interface

uses Classes, Windows, SysUtils, SyncObjs;

const
  NMPWAIT_WAIT_FOREVER = $FFFFFFFF;
  NMPWAIT_USE_DEFAULT_WAIT = 0;
  NMPWAIT_NOWAIT = 1;

type
  ENamedPipeException = class(Exception)
  end;

  TClientNamedPipeStream = class(THandleStream)
  private
    function GetConnected: Boolean;
  protected
    procedure SetSize(const NewSize: Int64); override;
  public
    constructor CreateLocal(const ShortPipeName: string; const TimeOutMillSec: Cardinal = NMPWAIT_WAIT_FOREVER);
    constructor Create(const FullPipeName: string; const TimeOutMillSec: Cardinal = NMPWAIT_WAIT_FOREVER; const AvoidingCompilerWarning: Byte = 0);
    function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
    destructor Destroy; override;
    property Connected: Boolean read GetConnected;
  end;

  TServerNamedPipeStream = class(THandleStream)
  private
    FConnected: Boolean;
    function GetConnected: Boolean;
  protected
    procedure SetSize(const NewSize: Int64); override;
  public
    constructor CreateLocal(const ShortPipeName: string);
    constructor Create(const FullPipeName: string; const AvoidingCompilerWarning: Byte = 0);
    function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
    destructor Destroy; override;
    property Connected: Boolean read GetConnected;
  end;

implementation

const
  INVALID_HANDLE_VALUE = -1; // redefined system constant to avoid type warnings between Integer and DWORD

{ TClientNamedPipeStream }

constructor TClientNamedPipeStream.Create(const FullPipeName: string;
  const TimeOutMillSec: Cardinal; const AvoidingCompilerWarning: Byte);
var
  PipeStateConst: Cardinal;
begin
  if WaitNamedPipe(PChar(FullPipeName), TimeOutMillSec) then
    FHandle := CreateFile(PChar(FullPipeName), GENERIC_WRITE+ GENERIC_READ, 0, nil, OPEN_EXISTING, 0, 0)
  else
    FHandle := INVALID_HANDLE_VALUE;
  if FHandle = INVALID_HANDLE_VALUE then
    raise ENamedPipeException.CreateFmt('Failed to open client named pipe "%s" with error "%s"',
      [FullPipeName, SysErrorMessage(GetLastError)]);
  PipeStateConst := PIPE_READMODE_BYTE+ PIPE_WAIT;
  if not SetNamedPipeHandleState(FHandle, PipeStateConst, nil, nil) then
    raise ENamedPipeException.CreateFmt('Failed to set state on client named pipe "%s" with error "%s"',
      [FullPipeName, SysErrorMessage(GetLastError)]);
end;

constructor TClientNamedPipeStream.CreateLocal(const ShortPipeName: string;
  const TimeOutMillSec: Cardinal);
begin
  Create('\\.\pipe\'+ ShortPipeName, TimeOutMillSec);
end;

destructor TClientNamedPipeStream.Destroy;
begin
  CloseHandle(FHandle);
  inherited;
end;

function TClientNamedPipeStream.GetConnected: Boolean;
begin
  Result := PeekNamedPipe(FHandle, nil, 0, nil, nil, nil);
end;

function TClientNamedPipeStream.Seek(const Offset: Int64;
  Origin: TSeekOrigin): Int64;
begin
  Assert(False, 'Seek is called');
  Result := 0;
end;

procedure TClientNamedPipeStream.SetSize(const NewSize: Int64);
begin
  Assert(False, 'SetSize is called');
end;

{ TServerNamedPipeStream }

constructor TServerNamedPipeStream.Create(const FullPipeName: string; const AvoidingCompilerWarning: Byte);
begin
  FConnected := False;
  FHandle := CreateNamedPipe(PChar(FullPipeName), PIPE_ACCESS_DUPLEX, PIPE_TYPE_BYTE+ PIPE_READMODE_BYTE+ PIPE_WAIT,
    PIPE_UNLIMITED_INSTANCES, 16384, 16384, 0, nil);
  if FHandle = INVALID_HANDLE_VALUE then
    raise ENamedPipeException.CreateFmt('Failed to create server named pipe "%s" with error "%s"',
      [FullPipeName, SysErrorMessage(GetLastError)]);
end;

constructor TServerNamedPipeStream.CreateLocal(
  const ShortPipeName: string);
begin
  Create('\\.\pipe\'+ ShortPipeName);
end;

destructor TServerNamedPipeStream.Destroy;
begin
  CloseHandle(FHandle);
  inherited;
end;

function TServerNamedPipeStream.GetConnected: Boolean;
var
  ErrCode: Cardinal;
  PipeStateConst: Cardinal;
begin
  PipeStateConst := PIPE_READMODE_BYTE+ PIPE_NOWAIT;
  if not SetNamedPipeHandleState(FHandle, PipeStateConst, nil, nil) then
    raise ENamedPipeException.CreateFmt('Failed to set state on client named pipe with error "%s"',
      [SysErrorMessage(GetLastError)]);
  try
    ConnectNamedPipe(FHandle, nil);
    ErrCode := GetLastError;
    if ErrCode = ERROR_PIPE_CONNECTED then
      Result := True
    else
    begin
      Result := False;
      if ErrCode = ERROR_NO_DATA then
        DisconnectNamedPipe(FHandle);
    end;
  finally
    PipeStateConst := PIPE_READMODE_BYTE+ PIPE_WAIT;
    if not SetNamedPipeHandleState(FHandle, PipeStateConst, nil, nil) then
      raise ENamedPipeException.CreateFmt('Failed to set state on client named pipe with error "%s"',
        [SysErrorMessage(GetLastError)]);
  end;
end;

function TServerNamedPipeStream.Seek(const Offset: Int64;
  Origin: TSeekOrigin): Int64;
begin
  Assert(False, 'Seek is called');
  Result := 0;
end;

procedure TServerNamedPipeStream.SetSize(const NewSize: Int64);
begin
  Assert(False, 'SetSize is called');
end;

end.
