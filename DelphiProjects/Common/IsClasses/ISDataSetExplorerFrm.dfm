object isDataSetExplorer: TisDataSetExplorer
  Left = 402
  Top = 258
  Width = 506
  Height = 263
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'DataSetExplorer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object grData: TDBGrid
    Left = 0
    Top = 0
    Width = 490
    Height = 225
    Align = alClient
    DataSource = DataSource
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DataSource: TDataSource
    Left = 200
    Top = 104
  end
end
