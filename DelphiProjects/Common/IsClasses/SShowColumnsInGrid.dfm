object ShowColumnsInGrid: TShowColumnsInGrid
  Left = 462
  Top = 255
  ActiveControl = cbColumns
  BorderStyle = bsDialog
  Caption = 'Choose columns to show'
  ClientHeight = 320
  ClientWidth = 313
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  DesignSize = (
    313
    320)
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 79
    Top = 286
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object CancelBtn: TButton
    Left = 159
    Top = 286
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object cbColumns: TCheckListBox
    Left = 8
    Top = 8
    Width = 297
    Height = 271
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 0
  end
end
