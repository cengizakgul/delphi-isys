unit rwPCLCanvas;

interface

uses  Classes, SysUtils, Windows, Graphics;

const
      cInch = 25.4;       //Millimeters
      cDecipoint = 1/720; //inches
      cPLU = 1/1016;      //inches

      pESC = 27;
      pCR  = 13;
      pLF  = 10;
      pFF  = 12;


      //One byte commands
      pResetPrinter = Ord('E');
      pResetMargins = Ord('9');


type

  TrwPCLPaperSize = (psExecutive, psLetter, psLegal, psLedger, psA5, psA4, psA3, psJISB5, psJISB4,
                     psHagakiPostcard, psOufukuHagakiPostcard, psMonarchEnvelope, psCommercialEnvelope,
                     psInternationalDL, psInternationalC5, psInternationalB5, psCustom);


  TrwPCLPrinterOrientation = (poPortrait, poLandscape, poReversePortrait, poReverseLandscape);


  TrwLineTermination = (ltNormal, ltCRChanged, ltLFFFChanged, ltAllChanged);


//  TrwFontSymbolSet = (fssUnknown, fss8M_HPMath_8, fssISO8859_1_Latin1, fssHPRoman8, fssOCRA, fssPC8, fssISO4_UnitedKingdom,
//                      fssISO21German, fssHPUSLegal, fssASCII, fssWindowsANSI);


  TrwFontStrokeWeight = (fswUltraThin, fswExtraThin, fswThin, fswExtraLight, fswLight, fswDemiLight, fswSemiLight,
                         fswMedium, fswSemiBold, fswDemiBold, fswBold, fswExtraBold, fswBlack, fswExtraBlack, fswUltraBlack);


  TrwFontStyle = (pfsUnknown, pfsUpright, pfsItalic, pfsCondensed, pfsCondensedItalic, pfsCompressedExtraCondensed,
                  pfsExpanded, pfsOutline, pfsInline, pfsShadowed, pfsOutlineShadowed);


  TrwUnderlineType = (pulNone, pulFixed, pulFloating);

  TrwPCLPatternType = (ptSolidBlack, ptSolidWhite, ptShading, ptCrossHatch, ptUserDefined);


  TrwPageRotateDegree = (prd0, prd90, prd180, prd270);


  TrwRasterGrMode = (gmCurPrintDirection, gmAlongPageWidth);


  TrwGrLeftMarginType = (lmtDefault, lmtCurCursorPos);


  TrwGrCompressionType = (gctUnencoded, gctRunLength, gctTIFF4, gctDeltaRow, gctReserved, gctAdaptive);


  TrwGrColorType = (gtCMY, gtBlackAndWhite, gtRGB);


  TrwSoftFont = class;

  TrwFontRec = record
    SymbolSet:      String[3];            //Symbol Set
    Proportional:   Boolean;              //Proportional or Fixed
    Height:         Extended;             //Height of the font in pixels
    StrokeWeight:   TrwFontStrokeWeight;  //Thickness or weight of the stroke
    Style:          TrwFontStyle;         //Style
    Typeface:       String;               //Typeface
    Pitch:          Extended;             //Pitch in characters/inch
    Handle:         HFONT;                //Handle of logical font
    SoftFont:       TrwSoftFont;          //Pointer to SoftFont;
  end;


  TrwRasterGraphicRec = record
    Resolution:     Integer;              //Raster Graphics Resolution (dots-per-inch)
    Mode:           TrwRasterGrMode;
    ColorType:      TrwGrColorType;       //Simple Color
    LeftMarginType: TrwGrLeftMarginType;
    Compression:    TrwGrCompressionType;
    ReceivingData:  Boolean;
    Data:           array of Byte;         //BMP representation
    DataSize:       Integer;               //Size in bytes
    RowsCount:      Integer;               //Rows in raster
  end;


  TrwFontDescriptor0Rec = record
    HeaderSize:           Word;
    HeaderFormat:         Byte;
    FontType:             Byte;
    StyleMSB:             Byte;
    Reserved:             Byte;
    BaselinePos:          Word;
    CellWidth:            Word;
    CellHeight:           Word;
    Orientation:          Byte;
    Spacing:              Byte;
    SymbolSet:            Word;
    Pitch:                Word;
    Height:               Word;
    xHeight:              Word;
    WidthType:            Byte;
    StyleLSB:             Byte;
    StrokeWeight:         Byte;
    TypefaceLSB:          Byte;
    TypefaceMSB:          Byte;
    SerifStyle:           Byte;
    Quality:              Byte;
    Placement:            Byte;
    UnderlineDistance:    Byte;
    UnderlineHeight:      Byte;
    TextHeight:           Word;
    TextWidth:            Word;
    FirstCode:            Word;
    LastCode:             Word;
    PitchExtended:        Byte;
    HeightExtended:       Byte;
    CapHeight:            Word;
    FontNumber:           DWord;
    FontName:             array [0..15] of char;
  end;


  TrwCharDescriptorBMPRec = record
    DescriptorSize:       Byte;
    DescriptorClass:      Byte;
    Orientation:          Byte;
    Reserved:             Byte;
    LeftOffset:           Word;
    TopOffset:            Word;
    CharWidth:            Word;
    CharHeight:           Word;
    DeltaX:               Word;
  end;


  PCLProc = procedure of object;

  TrwParamCommandRec = record
    ParamChar:  Byte;
    GroupChar:  Byte;
    ParamData:  array of byte;
    Terminator: Byte;
    Syntax:     String;
    ParamValue: array of Variant;
    Proc:       PCLProc;
  end;

  TrwPCLCommandRec = record
    Cmd:     ShortString;
    Proc:    PCLProc;
  end;

  TrwPCLGroupRec = record
    Group: String[2];
    Items: array of TrwPCLCommandRec;
  end;

  TrwPCLCommands = array of TrwPCLGroupRec;


  TrwSoftChar = class(TCollectionItem)
  public
    CharHeader:     TrwCharDescriptorBMPRec;
    CharCode:       Word;
    CharFormat:     Byte;
    CharData:       array of Byte;

    procedure Assign(ASource: TPersistent); override;
  end;


  TrwSoftFont = class(TCollectionItem)
  private
    FCorrected: Boolean;
    FChars: TCollection;
    function GetItem(Index: Integer): TrwSoftChar;

  public
    FontID: Word;
    Permanent: Boolean;
    Header:    TrwFontDescriptor0Rec;
    Copyright: String;

    property   Items[Index: Integer]: TrwSoftChar read GetItem; default;

    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    function    FindChar(AChar: Word): TrwSoftChar;
    function    Count: Integer;
    procedure   Clear;
    function    Add: TrwSoftChar;
    procedure   Assign(ASource: TPersistent); override;
  end;

  TrwSoftFonts = class(TCollection)
  private
    FLoadingFont: TrwSoftFont;

    function  GetItem(Index: Integer): TrwSoftFont;
    procedure SetItem(Index: Integer; const Value: TrwSoftFont);
    procedure DeleteFons(ATempOnly: Boolean);
    procedure DeleteFont(AFontID: Word);
    procedure SetFontAttr(AFontID: Word; APermanent: Boolean);
    procedure CorrectFont(AFont: TrwSoftFont);

  public
    property Items[Index: Integer]: TrwSoftFont read GetItem write SetItem; default;

    constructor Create;
    destructor  Destroy; override;
    function    FindFontByID(const AFontID: Word): TrwSoftFont;
    function    FindFontByDescriptor(const ADescr: TrwFontRec): TrwSoftFont;
  end;


  TrwPCLCanvas = class
  private
    FCanvas:          TCanvas;
    FDevPhysOffSet:   TPoint;
    FPCLCommands:     TrwPCLCommands;
    FPCLCommand:      TrwParamCommandRec;
    FCanvasRes:       Integer;
    FData:            array of byte;
    FCurPos:          Integer;
    FPainting:        Boolean;
    FCalcTextRect:    Boolean;

    FPaperSize:       TrwPCLPaperSize;
    FPaperWidth:      Word;                //Pixels
    FPaperHeight:     Word;                //Pixels
    FPageOrientation: TrwPCLPrinterOrientation;
    FLeftOffset:      Integer;             //Pixels
    FTopOffset:       Integer;             //Pixels
    FTopMargin:       Integer;             //Pixels
    FLeftMargin:      Integer;             //Pixels
    FRightMargin:     Integer;             //Pixels
    FVMI:             Integer;             //Vertical Motion Index (Number of 1/48 inch increments between rows)
    FHMI:             Integer;             //Horizontal Motion Index (Number of 1/120 inch increments)
    FPCLUnit:         Integer;             //Number of PCL units/inch
    FTextLength:      Integer;             //Pixels (Height)
    FPerforationSkip: Boolean;
    FLineTermination: TrwLineTermination;  //Controls the way the printer interprets CR, LF, and FF control codes
    FCursorPos:       TPoint;
    FRectSize:        TPoint;
    FTextData:        String;
    FPictFrameAnchor: TPoint;               //Picture Frame Anchor Point
    FPictFrameHorSize:Integer;              //Picture Frame Horizontal Size
    FPictFrameVertSize:Integer;             //Picture Frame Vertical Size
    FPrimaryFont:     TrwFontRec;          //Primary Font information
    FSecondaryFont:   TrwFontRec;          //Secondary Font information
    FTransparent:     Boolean;
    FPatternType:     TrwPCLPatternType;
    FPatternShading:  Integer;
    FRotatePattern:   Boolean;
    FRotateDegree:    TrwPageRotateDegree; //Degrees of rotation (0, 90, 180, 270)
    FRaster:          TrwRasterGraphicRec;
    FTextParsMethod:  Word;
    FFontID:          Word;                //Font Identification number (0..32767)
    FHPGL2Mode:       Boolean;
    FFonts:           TrwSoftFonts;
    FUnderline:       TrwUnderlineType;

    FOnSizeChange:    TNotifyEvent;
    FNoMargins: Boolean;

    procedure InitPCLCommands;
    procedure CalcPCLPageSize;
    function  DecipointToPixels(ADecipoint: Extended): Integer;
    function  UnitsToPixels(AUnits: Extended): Integer;
    function  GetCommandText: String;
    procedure ErrorNotImplemented;
    procedure ErrorWrongParameter;
    function  NextByte: Byte;
//    function  PrevByte: Byte;
    procedure ResetPrinter;
    procedure ResetMargins;
    procedure DrawPCLText;
    procedure SoftFontTextOut;
    procedure ParseTwoCharacterPCLCommand;
    procedure ParseOneCharacterPCLCommand;
    function  PreparePCLCommand: Boolean;
    procedure ExecParameterizedPCLCommand;
    procedure ParseParameterizedPCLCommand;
    procedure ParsePCLCommand;
    procedure DeletePrimFontHandle;
    procedure DeleteSecFontHandle;
    procedure SetCursorPosition(const  APoint: TPoint);
    function  NelcoPCLFonts: TrwSoftFonts;

    //PCL instructions
    procedure PCLNone;
    procedure PCLPageSize;
    procedure PCLPageOrientation;
    procedure PCLVMI;
    procedure PCLHMI;
    procedure PCLLineSpacing;
    procedure PCLTopMargin;
    procedure PCLTextLength;
    procedure PCLSetLeftOffset;
    procedure PCLSetTopOffset;
    procedure PCLPerforationSkip;
    procedure PCLLineTermination;
    procedure PCLPrintDirection;
    procedure PCLLeftMargin;
    procedure PCLRightMargin;
    procedure PCLHorCurPosCols;
    procedure PCLHorCurPosDecip;
    procedure PCLVertCurPosDecip;
    procedure PCLVertCurPosRows;
    procedure PCLTextParsingMethod;
    procedure PCLHorCurPosUnits;
    procedure PCLVertCurPosUnits;
    procedure PCLPatternRefPoint;
    procedure PCLPrimFontSymbolSet;
    procedure PCLPrimFontSpacing;
    procedure PCLPrimFontHeight;
    procedure PCLPrimFontStrokeWeight;
    procedure PCLPrimFontStyle;
    procedure PCLPrimFontTypeface;
    procedure PCLPrimFontPitch;
    procedure PCLSecFontSymbolSet;
    procedure PCLSecFontSpacing;
    procedure PCLSecFontHeight;
    procedure PCLSecFontStrokeWeight;
    procedure PCLSecFontStyle;
    procedure PCLSecFontTypeface;
    procedure PCLSecFontPitch;
    procedure PCLFontDescription;
    procedure PCLFontUnderlineOn;
    procedure PCLFontUnderlineOff;
    procedure PCLFontID;
    procedure PCLFontControl;
    procedure PCLVertRectSizeDCP;
    procedure PCLHorRectSizeDCP;
    procedure PCLHorRectSizeUnits;
    procedure PCLVertRectSizeUnits;
    procedure PCLPatternID;
    procedure PCLUserPattern;
    procedure PCLFillRect;
    procedure PCLPictFrameHorSize;
    procedure PCLPictFrameVertSize;
    procedure PCLPictFrameAnchor;
    procedure PCLCharCode;
    procedure PCLCharData;
    procedure PCLSoftFontAsPrimary;
    procedure PCLSoftFontAsSecondary;
    procedure PCLSimpleColor;
    procedure PCLRasterGraphicsMode;
    procedure PCLStartGraphics;
    procedure PCLEndGraphics;
    procedure PCLUnitofMeasure;
    procedure PCLRasterGraphicsResolution;
    procedure PCLPatternTransparency;
    procedure PCLCurrentPattern;
    procedure PCLGrCompression;
    procedure PCLRasterRowData;
    procedure PCLHPGL2On;
    procedure PCLHPGL2Off;

    procedure   RunPCL(AStream: TStream);
    procedure   Paint;

  public
    property    PaperWidth:  Word read FPaperWidth;
    property    PaperHeight: Word read FPaperHeight;
    property    NoMargins:   Boolean read FNoMargins write FNoMargins;
    property    PrimaryFont: TrwFontRec read FPrimaryFont;

    property    OnSizeChange: TNotifyEvent read FOnSizeChange write FOnSizeChange;

    constructor Create;
    destructor  Destroy; override;
    procedure   PaintTo(AStream: TStream; ACanvas: TCanvas; ACursorPos: TPoint; ADPI: Integer = 0); overload;
    procedure   PaintTo(AStream: TStream; AMetafile: TMetafile; ACursorPos: TPoint; ADPI: Integer = 0); overload;
    function    CalcTextRect(AStream: TStream; ACanvas: TCanvas; ADPI: Integer = 0): TRect;
  end;


{$R rwPCLCanvas.RES}

implementation

uses Math, DateUtils;

const
    cScreenCanvasRes = 96;    // The constant! ever!

var
  FNelcoPCLFontsSrc: TrwSoftFonts = nil;

threadvar
  FNelcoPCLFonts: TrwSoftFonts;


function WholeBytes(ABits: Word): Word;
begin
  Result := ABits div 8;
  if ABits mod 8 > 0 then
    Inc(Result);
end;


function WholeWords(ABytes: Word): Word;
begin
  Result := ABytes div 2;
  if ABytes mod 2 > 0 then
    Inc(Result);
end;


procedure InvertBitmap(var BMP: array of Byte);
var
  i: Integer;
begin
  for i := Low(BMP) to High(BMP) do
    BMP[i] := not BMP[i];
end;

{
function NativeSymbSetToType(const AValue: String): TrwFontSymbolSet;
begin
  if AValue = '8M' then
    Result := fss8M_HPMath_8
  else if AValue = '0N' then
    Result := fssISO8859_1_Latin1
  else if AValue = '8U' then
    Result := fssHPRoman8
  else if AValue = '0O' then
    Result := fssOCRA
  else if AValue = '10U' then
    Result := fssPC8
  else if AValue = '1E' then
    Result := fssISO4_UnitedKingdom
  else if AValue = '1G' then
    Result := fssISO21German
  else if AValue = '1U' then
    Result := fssHPUSLegal
  else if AValue = '0U' then
    Result := fssASCII
  else if AValue = '19U' then
    Result := fssWindowsANSI
  else
  begin
    Result := fssUnknown;
  end;
end;
}

function NativeFontStyleToType(const AValue: Integer): TrwFontStyle;
begin
  case AValue of
    0:  Result := pfsUpright;
    1:  Result := pfsItalic;
    4:  Result := pfsCondensed;
    5:  Result := pfsCondensedItalic;
    8:  Result := pfsCompressedExtraCondensed;
    24: Result := pfsExpanded;
    32: Result := pfsOutline;
    64: Result := pfsInline;
    128:Result := pfsShadowed;
    160:Result := pfsOutlineShadowed;
  else
    Result := pfsUnknown;
  end;
end;


function NativeStrokeWeightToType(const AValue: Integer): TrwFontStrokeWeight;
begin
  //  -7 - Ultra thin 1 - Semi Bold
  //  -6 - Extra Thin 2 - Demi Bold
  //  -5 - Thin 3 - Bold
  //  -4 - Extra Light 4 - Extra Bold
  //  -3 - Light 5 - Black
  //  -2 - Demi Light 6 - Extra Black
  //  -1 - Semi Light 7 - Ultra Black
  //  0 - Medium

  Result := TrwFontStrokeWeight(AValue + 7);
end;


function NativeTypefaceToType(const AValue: Integer): String;
begin
  case AValue of
    0:      Result := 'Line Printer';
    2:      Result := 'Elite';
    3:      Result := 'Courier';
    4:      Result := 'Helvetica';
    5:      Result := 'Times Roman';
    6:      Result := 'Gothic';
    7:      Result := 'Script';
    8:      Result := 'Prestige';
    52:     Result := 'Univers';
    4099:   Result := 'Courier Scalable';
    4101:   Result := 'CG Times';
    4119:   Result := 'CG Century Schoolbook';
    4127:   Result := 'ITC AvantGarde';
    4148:   Result := 'Univers';
    16602:  Result := 'Arial';
  else
    Result := '';
  end;
end;


{ TrwPCLCanvas }

constructor TrwPCLCanvas.Create;
begin
  FNoMargins := False;
  FCalcTextRect := False;
  FFonts := TrwSoftFonts.Create;
  InitPCLCommands;
end;


destructor TrwPCLCanvas.Destroy;
begin
  DeletePrimFontHandle;
  DeleteSecFontHandle;

  FFonts.Free;

  inherited;
end;


procedure TrwPCLCanvas.InitPCLCommands;

  procedure AddGroup(const AIndex: Integer; const AGroup: String; const ACmd: array of String;  const AProc: array of PCLProc);
  var
    i: Integer;
  begin
    FPCLCommands[AIndex].Group := AGroup;
    SetLength(FPCLCommands[AIndex].Items, Length(ACmd));
    for i := Low(ACmd) to High(ACmd) do
    begin
      FPCLCommands[AIndex].Items[i].Cmd := ACmd[i];
      FPCLCommands[AIndex].Items[i].Proc := AProc[i];
    end
  end;

begin
  SetLength(FPCLCommands, 23);

  AddGroup(0, '&l', ['#A',    //Page Size
                     '#H',    //Paper (Media) Source
                     '#P',    //Page Length
                     '#O',    //Page Orientation
                     '#E',    //Top Margin
                     '#F',    //Text Length
                     '#L',    //Perforation Skip
                     '#C',    //Vertical Motion Index
                     '#D',    //Line Spacing
                     '#X',    //Number of Copies
                     '#U',    //Left Offset
                     '#Z'     //Top Offset
                    ],
                    [PCLPageSize,
                     PCLNone,
                     PCLNone,
                     PCLPageOrientation,
                     PCLTopMargin,
                     PCLTextLength,
                     PCLPerforationSkip,
                     PCLVMI,
                     PCLLineSpacing,
                     PCLNone,
                     PCLSetLeftOffset,
                     PCLSetTopOffset
                    ]);

  AddGroup(1, '&a', ['#P',    //Print Direction
                     '#L',    //Left Margin
                     '#M',    //Right Margin
                     '#C',    //Horizontal Cursor Positioning (in Columns)
                     '#H',    //Horizontal Cursor Positioning (in Decipoints)
                     '#R',    //Vertical Cursor Positioning (Rows)
                     '#V'     //Vertical Cursor Positioning (Decipoints)
                    ],
                    [PCLPrintDirection,
                     PCLLeftMargin,
                     PCLRightMargin,
                     PCLHorCurPosCols,
                     PCLHorCurPosDecip,
                     PCLVertCurPosRows,
                     PCLVertCurPosDecip
                    ]);

  AddGroup(2, '&c', ['#T'     //Character Text Path Direction
                    ],
                    [nil
                    ]);

  AddGroup(3, '&t', ['#P'     //Text Parsing Method
                    ],
                    [PCLTextParsingMethod
                    ]);


  AddGroup(4, '&k', ['#H',     //Horizontal Motion Index
                     '#G'      //Line Termination
                    ],
                    [PCLHMI,
                     PCLLineTermination
                    ]);


  AddGroup(5, '*p', ['#X',    //Horizontal Cursor Positioning (PCL units)
                     '#Y',    //Vertical Cursor Positioning (PCL units)
                     '#R'     //Pattern Reference Point
                    ],
                    [PCLHorCurPosUnits,
                     PCLVertCurPosUnits,
                     PCLPatternRefPoint
                    ]);

  AddGroup(6, '&f', ['#S'     //Push/Pop Cursor Position
                    ],
                    [nil
                    ]);

  AddGroup(7, '(s', ['#P',    //Primary Font Spacing
                     '#H',    //Primary Font Pitch
                     '#V',    //Primary Font Height (Point Size)
                     '#S',    //Primary Font Style
                     '#B',    //Primary Font Stroke Weight
                     '#T',    //Primary Font Typeface Selection
                     '#W'     //Character descriptor and character data [DATA]
                    ],
                    [PCLPrimFontSpacing,
                     PCLPrimFontPitch,
                     PCLPrimFontHeight,
                     PCLPrimFontStyle,
                     PCLPrimFontStrokeWeight,
                     PCLPrimFontTypeface,
                     PCLCharData
                    ]);

  AddGroup(8, ')s', ['#P',    //Secondary Font Spacing
                     '#H',    //Secondary Font Pitch
                     '#V',    //Secondary Font Height (Point Size)
                     '#S',    //Secondary Font Style
                     '#B',    //Secondary Font Stroke Weight
                     '#T',    //Secondary Font Typeface Selection
                     '#W'     //Font descriptor [DATA]
                    ],
                    [PCLSecFontSpacing,
                     PCLSecFontPitch,
                     PCLSecFontHeight,
                     PCLSecFontStyle,
                     PCLSecFontStrokeWeight,
                     PCLSecFontTypeface,
                     PCLFontDescription
                    ]);

  AddGroup(9, '&p', ['#X'  //Transparent Print Data  [DATA]
                    ],
                    [nil
                    ]);

  AddGroup(10, '&d', ['#D',    //Underline
                      '@'      //Underline off
                     ],
                     [PCLFontUnderlineOn,
                      PCLFontUnderlineOff
                     ]);

  AddGroup(11, '*c', ['#D',    //Font ID #
                      '#F',    //Font Control
                      '#R',    //Symbol Set ID Code
                      '#S',    //Symbol Set Control
                      '#H',    //Horizontal Rectangle Size (decipoints)
                      '#A',    //Horizontal Rectangle Size (dots)
                      '#V',    //Vertical Rectangle Size (decipoints)
                      '#B',    //Vertical Rectangle Size (dots)
                      '#G',    //Pattern ID
                      '#P',    //Fill Rectangular Area
                      '#W',    //User Defined Pattern  [DATA]
                      '#Q',    //Pattern Control
                      '#X',    //Picture Frame Horizontal Size
                      '#Y',    //Picture Frame Vertical Size
                      '#T',    //Picture Frame Anchor Point
                      '#E'     //Character code to download
                     ],
                     [PCLFontID,
                      PCLFontControl,
                      nil,
                      nil,
                      PCLHorRectSizeDCP,
                      PCLHorRectSizeUnits,
                      PCLVertRectSizeDCP,
                      PCLVertRectSizeUnits,
                      PCLPatternID,
                      PCLFillRect,
                      PCLUserPattern,
                      nil,
                      PCLPictFrameHorSize,
                      PCLPictFrameVertSize,
                      PCLPictFrameAnchor,
                      PCLCharCode
                     ]);


  AddGroup(12, '&n', ['#W'  //Alphanumeric ID  [DATA]
                     ],
                     [nil
                     ]);


  AddGroup(13, '(f', ['#W' //Define Symbol Set  [DATA]
                     ],
                     [nil
                     ]);


  AddGroup(14, '(', ['#X',         //Designates soft font as primary
                     '#[CHAR]',    //Primary font Symbol Set
                     '3@'          //Default primary font characteristics
                    ],
                    [PCLSoftFontAsPrimary,
                     PCLPrimFontSymbolSet,
                     nil
                    ]);


  AddGroup(15, ')', ['#X',         //Designates soft font as secondary
                     '#[CHAR]',    //Secondary font Symbol Set
                     '3@'          //Default secondary font characteristics
                    ],
                    [PCLSoftFontAsSecondary,
                     PCLSecFontSymbolSet,
                     nil
                    ]);

  AddGroup(16, '*r', ['#U',         //Simple Color
                      '#F',         //Raster Graphics Presentation Mode
                      '#A',         //Specifies the left raster graphics margin
                      'B'           //End of raster data
                     ],
                     [PCLSimpleColor,
                      PCLRasterGraphicsMode,
                      PCLStartGraphics,
                      PCLEndGraphics
                     ]);

  AddGroup(17, '&u', ['#D'         //Unit of Measure
                     ],
                     [PCLUnitofMeasure
                     ]);

  AddGroup(18, '*t', ['#R'         //Raster Graphics Resolution
                     ],
                     [PCLRasterGraphicsResolution
                     ]);

  AddGroup(19, '*v', ['#O',         //Set Transparency
                      '#T'          //Select Current Pattern
                     ],
                     [PCLPatternTransparency,
                      PCLCurrentPattern
                     ]);

  AddGroup(20, '*b', ['#M',         //Set Compression Method
                      '#W'          //Transfer Raster Data [DATA]
                     ],
                     [PCLGrCompression,
                      PCLRasterRowData
                     ]);


  AddGroup(21, '*l', ['#R'         //Raster Graphics Resolution
                     ],
                     [PCLNone
                     ]);

  AddGroup(22, '%',  ['#B',        //HP GL/2 mode ON
                      '#A',        //HP GL/2 mode OFF
                      '-12345X'    //Universal Exit Language
                     ],
                     [PCLHPGL2On,
                      PCLHPGL2Off,
                      PCLHPGL2Off
                     ]);

end;


function TrwPCLCanvas.DecipointToPixels(ADecipoint: Extended): Integer;
begin
  Result := Round(ADecipoint * cDecipoint * FCanvasRes);
end;


function TrwPCLCanvas.UnitsToPixels(AUnits: Extended): Integer;
begin
  Result := DecipointToPixels(AUnits / (FPCLUnit * cDecipoint));
end;


function TrwPCLCanvas.GetCommandText: String;
var
  i: Integer;
begin
  Result := Char(FPCLCommand.ParamChar);
  if FPCLCommand.GroupChar <> 0 then
    Result := Result + Char(FPCLCommand.GroupChar);
  for i := Low(FPCLCommand.ParamData) to High(FPCLCommand.ParamData) do
    Result := Result + Chr(FPCLCommand.ParamData[i]);
  Result := Result + Chr(FPCLCommand.Terminator);
end;


procedure TrwPCLCanvas.ErrorNotImplemented;
begin
  raise Exception.Create('Command ' + GetCommandText + ' has no implementation');
end;


procedure TrwPCLCanvas.ErrorWrongParameter;
begin
  raise Exception.Create('Command ' + GetCommandText + ' has wrong parameters');
end;


function TrwPCLCanvas.NextByte: Byte;
begin
  Inc(FCurPos);
  if FCurPos > High(FData) then
    raise Exception.Create('Out of data range: ' + IntToStr(FCurPos));
  Result := FData[FCurPos];
end;

{
function TrwPCLCanvas.PrevByte: Byte;
begin
  Dec(FCurPos);
  if FCurPos < Low(FData) then
    raise Exception.Create('Out of data range: ' + IntToStr(FCurPos));
  Result := FData[FCurPos];
end;
}

procedure TrwPCLCanvas.ResetPrinter;
begin
  FPCLUnit := 300;
  FPageOrientation := poPortrait;
  FPaperSize := psLetter;
  FPaperWidth := UnitsToPixels((8.5 * FPCLUnit));
  FPaperHeight := UnitsToPixels(Round(11 * FPCLUnit));

  FCursorPos.X := 0;
  FCursorPos.Y := 0;
  FPerforationSkip := False;
  FTextLength := 0;

  FPrimaryFont.SymbolSet := '8U';
  FPrimaryFont.Proportional := False;
  FPrimaryFont.Pitch := 10;
  FPrimaryFont.Height := 12;
  FPrimaryFont.Style := pfsUpright;
  FPrimaryFont.StrokeWeight := fswMedium;
  FPrimaryFont.Typeface := 'Courier Scalable';

  FRotatePattern := True;
  FHPGL2Mode := False;
  FUnderline := pulNone;

  FLeftOffset := - FDevPhysOffSet.x;
  FTopOffset := - FDevPhysOffSet.y;
  ResetMargins;
  DeletePrimFontHandle;
  DeleteSecFontHandle
end;


procedure TrwPCLCanvas.ResetMargins;
begin
  if NoMargins then
  begin
    FTopMargin := 0;
    FLeftMargin  := 0;
    FRightMargin := 0;
  end
  else
  begin
    FTopMargin := UnitsToPixels(Round(0.5  * FPCLUnit));
    FLeftMargin  := UnitsToPixels(Round(0.25  * FPCLUnit));
    FRightMargin := UnitsToPixels(Round(0.25  * FPCLUnit));
  end;
end;


procedure TrwPCLCanvas.DrawPCLText;
var
  dx, dy: Integer;
  PrevH: THandle;
  fh: Integer;
  TM: TTextMetricA;

  function FindSoftFont: Boolean;
  begin
    FPrimaryFont.SoftFont :=  FFonts.FindFontByDescriptor(FPrimaryFont);
    if not Assigned(FPrimaryFont.SoftFont) then
      FPrimaryFont.SoftFont :=  NelcoPCLFonts.FindFontByDescriptor(FPrimaryFont);
    Result := Assigned(FPrimaryFont.SoftFont);
  end;


  procedure CreateFontHandle;
  var
    f: TLogFont;
    h: String;
  begin
    f.lfHeight := -fh;
    if FPrimaryFont.Proportional then
    begin
      f.lfWidth := 0;
      f.lfPitchAndFamily := VARIABLE_PITCH;
    end
    else
    begin
      f.lfWidth := Round(FCanvasRes/ FPrimaryFont.Pitch);
      f.lfPitchAndFamily := FIXED_PITCH;
    end;

    f.lfPitchAndFamily := f.lfPitchAndFamily or FF_DONTCARE;

    case FRotateDegree of
      prd0:   f.lfEscapement := 0;
      prd90:  f.lfEscapement := 900;
      prd180: f.lfEscapement := 1800;
      prd270: f.lfEscapement := 2700;
    end;

    f.lfOrientation := 0;

    case FPrimaryFont.StrokeWeight of
      fswUltraThin:    f.lfWeight := FW_DONTCARE;
      fswExtraThin:    f.lfWeight := FW_THIN;
      fswThin:         f.lfWeight := FW_EXTRALIGHT;
      fswExtraLight:   f.lfWeight := FW_ULTRALIGHT;
      fswLight:        f.lfWeight := FW_LIGHT;
      fswDemiLight:    f.lfWeight := FW_NORMAL;
      fswSemiLight:    f.lfWeight := FW_REGULAR;
      fswMedium:       f.lfWeight := FW_MEDIUM;
      fswSemiBold:     f.lfWeight := FW_SEMIBOLD;
      fswDemiBold:     f.lfWeight := FW_DEMIBOLD;
      fswBold:         f.lfWeight := FW_BOLD;
      fswExtraBold:    f.lfWeight := FW_EXTRABOLD;
      fswBlack:        f.lfWeight := FW_ULTRABOLD;
      fswExtraBlack:   f.lfWeight := FW_HEAVY;
      fswUltraBlack:   f.lfWeight := FW_BLACK;
    end;

    f.lfItalic := 0;
    f.lfUnderline := 0;
    f.lfStrikeOut := 0;

    case FPrimaryFont.Style of
      pfsUpright:;
      pfsItalic:                     f.lfItalic := 1;
      pfsCondensed:;
      pfsCondensedItalic:;
      pfsCompressedExtraCondensed:;
      pfsExpanded:;
      pfsOutline:;
      pfsInline:;
      pfsShadowed:;
      pfsOutlineShadowed:;
    end;


    f.lfCharSet := ANSI_CHARSET;
    f.lfOutPrecision :=  OUT_TT_PRECIS;
    f.lfClipPrecision := CLIP_CHARACTER_PRECIS;
    f.lfQuality := DEFAULT_QUALITY;

    h := FPrimaryFont.TypeFace + #0;
    CopyMemory(@f.lfFaceName[0], @h[1], Length(h));

    FPrimaryFont.Handle := CreateFontIndirect(f);
  end;

begin
  fh := Round(FPrimaryFont.Height * cDecipoint * 10 * FCanvasRes);

  if (FPrimaryFont.Handle = 0) then
    if Assigned(FPrimaryFont.SoftFont) then
    begin
       SoftFontTextOut;
       Exit;
    end
    else if FindSoftFont then
    begin
       SoftFontTextOut;
       Exit;
    end
    else
      CreateFontHandle;

  PrevH := SelectObject(FCanvas.Handle, FPrimaryFont.Handle);
  try
    GetTextMetrics(FCanvas.Handle, TM);

    case FRotateDegree of
      prd0:
        begin
          dx := 0;
          dy := - (TM.tmAscent - TM.tmInternalLeading div 2);
        end;

      prd90:
        begin
          dx := - TM.tmAscent;
          dy := 0
        end;

      prd180:
        begin
          dx := 0;
          dy := - TM.tmAscent;
        end;

    else
        begin
          dx := TM.tmAscent;
          dy := 0
        end;
    end;


    FCanvas.Brush.Color := clNone;
    FCanvas.Brush.Style := bsClear;
    if not FCalcTextRect then
      FCanvas.TextOut(FCursorPos.X + dx + FLeftOffset + FLeftMargin, FCursorPos.Y + dy + FTopOffset + FTopMargin, FTextData);

    if dx = 0 then
      SetCursorPosition(Point(FCursorPos.X + FCanvas.TextWidth(FTextData)  * Sign(dx), FCursorPos.Y))
    else
      SetCursorPosition(Point(FCursorPos.X, FCursorPos.Y + FCanvas.TextWidth(FTextData) * Sign(dy)))

  finally
    SelectObject(FCanvas.Handle, PrevH);
  end;
end;


procedure TrwPCLCanvas.ParseTwoCharacterPCLCommand;
begin
  case FData[FCurPos] of
    pResetPrinter:    ResetPrinter;
    pResetMargins:    ResetMargins;
  end;
end;


procedure TrwPCLCanvas.ParseOneCharacterPCLCommand;
begin
end;


function TrwPCLCanvas.PreparePCLCommand: Boolean;
var
  i, j: Integer;
  fl: Boolean;
  V: Variant;
  s: String;
begin
  Result := True;

  SetLength(FPCLCommand.ParamValue, 0);

  i := 1;
  j := 0;
  while i <= Length(FPCLCommand.Syntax) do
  begin
    fl := Copy(FPCLCommand.Syntax, i, 6) = '[CHAR]';
    if fl then
      break

    else if FPCLCommand.Syntax[i] = AnsiUpperCase(Char(FPCLCommand.Terminator))[1] then
      break

    else
    begin
      fl := FPCLCommand.Syntax[i] = '#';
      if fl then
      begin
        s := '';
        while (j <= High(FPCLCommand.ParamData)) and (FPCLCommand.ParamData[j] in [Ord('0')..Ord('9'), Ord('.'), Ord('-'), Ord('+')]) do
        begin
          s := s + Chr(FPCLCommand.ParamData[j]);
          Inc(j);
        end;
        if s = '' then
          V := 0
        else
          V := StrToFloat(s);
      end
      else
      begin
        V := FPCLCommand.ParamData[j];
        Inc(j);
      end;
    end;


    if fl then
    begin
      SetLength(FPCLCommand.ParamValue, Length(FPCLCommand.ParamValue) + 1);
      FPCLCommand.ParamValue[High(FPCLCommand.ParamValue)] := V;
    end;

    Inc(i);
  end;
end;


procedure TrwPCLCanvas.ExecParameterizedPCLCommand;
var
  i, j, l: Integer;
  h: String;
  C, T: Char;
begin
  h := Char(FPCLCommand.ParamChar);
  if FPCLCommand.GroupChar <> 0 then
   h := h + Char(FPCLCommand.GroupChar);
  T := AnsiUpperCase(Chr(FPCLCommand.Terminator))[1];

  FPCLCommand.Syntax := '';
  FPCLCommand.Proc := nil;
  for i := Low(FPCLCommands) to High(FPCLCommands) do
    if CompareStr(FPCLCommands[i].Group, h) = 0 then
    begin
      for j := Low(FPCLCommands[i].Items) to High(FPCLCommands[i].Items) do
      begin
        l := Length(FPCLCommands[i].Items[j].Cmd);
        C := FPCLCommands[i].Items[j].Cmd[l];
        if (Copy(FPCLCommands[i].Items[j].Cmd, l - 5, 6) = '[CHAR]') or (C = T) then
        begin
          FPCLCommand.Syntax := FPCLCommands[i].Items[j].Cmd;
          FPCLCommand.Proc := FPCLCommands[i].Items[j].Proc;
          break;
        end;
      end;
      break;
    end;

  if Length(FPCLCommand.Syntax) = 0 then
    raise Exception.Create('Unknow PCL command'#13#13 + GetCommandText);

  if PreparePCLCommand then
    if Assigned(FPCLCommand.Proc) then
      FPCLCommand.Proc
    else
      ErrorNotImplemented;
end;


procedure TrwPCLCanvas.ParseParameterizedPCLCommand;
var
  i, n: Integer;
begin
  i := FCurPos;
  while not (FData[FCurPos] in [64..94, 97..122]) do
    NextByte;

  n := FCurPos - i;
  SetLength(FPCLCommand.ParamData, n);
  if n > 0 then
    CopyMemory(Addr(FPCLCommand.ParamData[Low(FPCLCommand.ParamData)]), Addr(FData[i]), n);
  FPCLCommand.Terminator := FData[FCurPos];

  ExecParameterizedPCLCommand;

  if not (FPCLCommand.Terminator in [64..94]) then
  begin
    NextByte;
    ParseParameterizedPCLCommand;
  end;
end;


procedure TrwPCLCanvas.ParsePCLCommand;
begin
  if FData[FCurPos] = pESC then
  begin
    NextByte;
    if FData[FCurPos] in [48..126] then
      ParseTwoCharacterPCLCommand

    else if FData[FCurPos] in [33..47] then
    begin
      FPCLCommand.ParamChar := FData[FCurPos];
      if NextByte in [96..126] then
      begin
        FPCLCommand.GroupChar := FData[FCurPos];
        NextByte;
      end
      else
        FPCLCommand.GroupChar := 0;
      ParseParameterizedPCLCommand;
    end;
  end

  else
    ParseOneCharacterPCLCommand;
end;


procedure TrwPCLCanvas.CalcPCLPageSize;
var
  w, h: Word;
begin
  w := FPaperWidth;
  h := FPaperHeight;

  case FPaperSize of
    psExecutive:
          begin
            w := Round(7.25 * FPCLUnit);
            h := Round(10.5 * FPCLUnit);
          end;

    psLetter:
          begin
            w := Round(8.5 * FPCLUnit);
            h := Round(11 * FPCLUnit);
          end;

    psLegal:
          begin
            w := Round(8.5 * FPCLUnit);
            h := Round(14 * FPCLUnit);
          end;

    psLedger:
          begin
            w := Round(11 * FPCLUnit);
            h := Round(17 * FPCLUnit);
          end;

    psA5:
          begin
            w := Round(148 / cInch * FPCLUnit);
            h := Round(210 / cInch * FPCLUnit);
          end;

    psA4:
          begin
            w := Round(210 / cInch * FPCLUnit);
            h := Round(297 / cInch * FPCLUnit);
          end;

    psA3:
          begin
            w := Round(297 /cInch * FPCLUnit);
            h := Round(420 /cInch * FPCLUnit);
          end;

    psJISB5:
          begin
            w := Round(182 / cInch * FPCLUnit);
            h := Round(257 / cInch * FPCLUnit);
          end;

    psJISB4:
          begin
            w := Round(250 / cInch * FPCLUnit);
            h := Round(354 / cInch * FPCLUnit);
          end;

    psHagakiPostcard:
          begin
            w := Round(100 / cInch * FPCLUnit);
            h := Round(148 / cInch * FPCLUnit);
          end;

    psOufukuHagakiPostcard:
          begin
            w := Round(200 / cInch * FPCLUnit);
            h := Round(148 / cInch * FPCLUnit);
          end;

    psMonarchEnvelope:
          begin
            w := Round((3 + 7/8) * FPCLUnit);
            h := Round(7.5 * FPCLUnit);
          end;

    psCommercialEnvelope:
          begin
            w := Round((4 + 1/8) * FPCLUnit);
            h := Round(9.5 * FPCLUnit);
          end;

    psInternationalDL:
          begin
            w := Round(110 / cInch * FPCLUnit);
            h := Round(220 / cInch * FPCLUnit);
          end;

    psInternationalC5:
          begin
            w := Round(162 / cInch * FPCLUnit);
            h := Round(229 / cInch * FPCLUnit);
          end;

    psInternationalB5:
          begin
            w := Round(176 / cInch * FPCLUnit);
            h := Round(250 / cInch * FPCLUnit);
          end;

    psCustom:
         begin
           if FTextLength <> 0 then
             h := FTextLength;
         end;
  end;

  if FPaperSize <> psCustom then
  begin
    FPaperWidth := UnitsToPixels(w);
    FPaperHeight := UnitsToPixels(h);
  end
  else
  begin
    FPaperWidth := w;
    FPaperHeight := h;
  end;

//  FCanvas.Brush.Color := clWhite;
//  FCanvas.Brush.Style := bsSolid;
//  FCanvas.FillRect(Rect(0, 0, FPaperWidth, FPaperHeight));

  if FPageOrientation in [poLandscape, poReverseLandscape] then
  begin
    w := FPaperWidth;
    FPaperWidth := FPaperHeight;
    FPaperHeight := w;
  end;

  if Assigned(FOnSizeChange) then
    FOnSizeChange(Self);
end;


procedure TrwPCLCanvas.PCLNone;
begin
  //Dummy instruction
end;


procedure TrwPCLCanvas.PCLPageSize;
var
  t: TrwPCLPaperSize;
begin
  //Designates the physical paper size which in turn defines the logical page
  case Integer(FPCLCommand.ParamValue[0]) of
    1:    t := psExecutive;
    2:    t := psLetter;
    3:    t := psLegal;
    6:    t := psLedger;
    25:   t := psA5;
    26:   t := psA4;
    27:   t := psA3;
    45:   t := psJISB5;
    46:   t := psJISB4;
    71:   t := psHagakiPostcard;
    72:   t := psOufukuHagakiPostcard;
    80:   t := psMonarchEnvelope;
    81:   t := psCommercialEnvelope;
    90:   t := psInternationalDL;
    91:   t := psInternationalC5;
    100:  t := psInternationalB5;
    110:  t := psCustom;
  else
    t := psCustom;
    ErrorWrongParameter;
  end;

  FPaperSize := t;
  CalcPCLPageSize;
end;


procedure TrwPCLCanvas.PCLPageOrientation;
begin
  //Designates the logical page position with respect to the physical page
  FPageOrientation := TrwPCLPrinterOrientation(Integer(FPCLCommand.ParamValue[0]));
  CalcPCLPageSize;
end;


procedure TrwPCLCanvas.PCLVMI;
begin
  //Designates the distance between rows (1/48 inch increments)
  FVMI := UnitsToPixels(FPCLCommand.ParamValue[0] / 48 * FPCLUnit);
end;


procedure TrwPCLCanvas.PCLHMI;
begin
  //Designates the distance between columns (1/120 inch increments)
  FHMI := UnitsToPixels(FPCLCommand.ParamValue[0] / 120 * FPCLUnit);
end;


procedure TrwPCLCanvas.PCLLineSpacing;
begin
  //Sets the number of lines printed per inch
  FVMI := UnitsToPixels(FPCLUnit / Integer(FPCLCommand.ParamValue[0]));
end;


procedure TrwPCLCanvas.PCLTopMargin;
begin
  //Designates number of lines between top of logical page to top of text area
  FTopMargin := Integer(FPCLCommand.ParamValue[0]) *  FVMI;
end;


procedure TrwPCLCanvas.PCLTextLength;
begin
  //Designates the length of the text area in lines
  FTextLength := Integer(FPCLCommand.ParamValue[0]) * FVMI;
  FPaperSize := psCustom;
  CalcPCLPageSize;
end;


procedure TrwPCLCanvas.PCLSetLeftOffset;
begin
  //Designates the position of the logical page across
  //the width (short side) of the physical page
  FLeftOffset := DecipointToPixels(FPCLCommand.ParamValue[0]) - FDevPhysOffSet.x;
end;


procedure TrwPCLCanvas.PCLSetTopOffset;
begin
  //Designates the position of the logical page along
  //the length (long side) of the physical page
  FTopOffset := DecipointToPixels(FPCLCommand.ParamValue[0]) - FDevPhysOffSet.y;
end;


procedure TrwPCLCanvas.PCLPerforationSkip;
begin
  FPerforationSkip := Boolean(Integer(FPCLCommand.ParamValue[0]));
end;


procedure TrwPCLCanvas.PCLLineTermination;
begin
  // 0 - CR = CR, LF = LF, FF = FF
  // 1 - CR = CR+LF, LF = LF, FF = FF
  // 2 - CR = CR, LF = CR+LF, FF = CR+FF
  // 3 - CR = CR+LF, LF = CR+LF, FF = CR+FF
  FLineTermination := TrwLineTermination(Integer(FPCLCommand.ParamValue[0]));
end;


procedure TrwPCLCanvas.PCLPrintDirection;
var
  rt: TrwPageRotateDegree;
begin
  //Rotates the logical page coordinate system counterclockwise in 90 degree
  //increments with respect to the orientation of the current logical page.
  case Integer(FPCLCommand.ParamValue[0]) of
    0:   rt := prd0;
    90:  rt := prd90;
    180: rt := prd180;
    270: rt := prd270;
  else
    rt := FRotateDegree;
  end;

  FRotateDegree := rt;
end;


procedure TrwPCLCanvas.PCLLeftMargin;
begin
  //Sets the left margin to the left edge of the specified column
  FLeftMargin := FPCLCommand.ParamValue[0] * FHMI;
end;


procedure TrwPCLCanvas.PCLRightMargin;
begin
  //Sets the right margin to the right edge of the specified column
  FRightMargin := FPCLCommand.ParamValue[0] * FHMI;
end;


procedure TrwPCLCanvas.PCLHorCurPosCols;
begin
  //Moves the cursor to a new column on the current line
  SetCursorPosition(Point(FPCLCommand.ParamValue[0] * FHMI, FCursorPos.Y));
end;


procedure TrwPCLCanvas.PCLHorCurPosDecip;
begin
  //Moves the cursor to a new position along the x-axis
  SetCursorPosition(Point(DecipointToPixels(FPCLCommand.ParamValue[0]), FCursorPos.Y));
end;


procedure TrwPCLCanvas.PCLVertCurPosDecip;
begin
  //Moves the cursor to a new vertical position along the y-axis
  SetCursorPosition(Point(FCursorPos.X, DecipointToPixels(FPCLCommand.ParamValue[0])));
end;


procedure TrwPCLCanvas.PCLVertCurPosRows;
begin
  //Moves the cursor to a new row in the same column
  SetCursorPosition(Point(FCursorPos.X, FPCLCommand.ParamValue[0] * FVMI));
end;


procedure TrwPCLCanvas.PCLTextParsingMethod;
begin
  //Specifies PCL parsing method as either 1-byte or 2-byte characters codes
  FTextParsMethod := Integer(FPCLCommand.ParamValue[0]);
end;


procedure TrwPCLCanvas.PCLHorCurPosUnits;
begin
  //Moves the cursor to a new position along the x-axis (PCL units)
  SetCursorPosition(Point(UnitsToPixels(FPCLCommand.ParamValue[0]), FCursorPos.Y));
end;


procedure TrwPCLCanvas.PCLVertCurPosUnits;
begin
  //Moves the cursor to a new position along the y-axis (PCL units)
  SetCursorPosition(Point(FCursorPos.X, UnitsToPixels(FPCLCommand.ParamValue[0])));
end;


procedure TrwPCLCanvas.PCLPatternRefPoint;
begin
  //Specifies whether the pattern rotates with the print direction or remains fixed
  if Integer(FPCLCommand.ParamValue[0]) = 0 then
    FRotatePattern := True
  else if Integer(FPCLCommand.ParamValue[0]) = 1 then
    FRotatePattern := False;
end;


procedure TrwPCLCanvas.PCLPrimFontSymbolSet;
var
  h:  String;
begin
  //Designates the set of symbols or characters contained in a font
  h := IntToStr(Integer(FPCLCommand.ParamValue[0])) + Chr(FPCLCommand.Terminator);
  FPrimaryFont.SymbolSet := h;
  DeletePrimFontHandle;
end;


procedure TrwPCLCanvas.PCLPrimFontSpacing;
begin
  //Designates either a fixed or proportionally spaced font
  FPrimaryFont.Proportional := Boolean(Integer(FPCLCommand.ParamValue[0]));
  DeletePrimFontHandle;
end;


procedure TrwPCLCanvas.PCLPrimFontHeight;
begin
  //Designates the height of the font in points
  FPrimaryFont.Height := Round(FPCLCommand.ParamValue[0]);
  DeletePrimFontHandle;
end;


procedure TrwPCLCanvas.PCLPrimFontStrokeWeight;
begin
  //Designates the thickness or weight of the stroke that composes the characters of a font
  FPrimaryFont.StrokeWeight := NativeStrokeWeightToType(Integer(FPCLCommand.ParamValue[0]));
  DeletePrimFontHandle;
end;


procedure TrwPCLCanvas.PCLPrimFontStyle;
begin
  //Designates the font style
  FPrimaryFont.Style := NativeFontStyleToType(Integer(FPCLCommand.ParamValue[0]));
  if FPrimaryFont.Style = pfsUnknown then
    ErrorWrongParameter;

  DeletePrimFontHandle;
end;


procedure TrwPCLCanvas.PCLPrimFontTypeface;
begin
  //Designates the design of the font
  FPrimaryFont.Typeface := NativeTypefaceToType(Integer(FPCLCommand.ParamValue[0]));
  if FPrimaryFont.Typeface = '' then
    ErrorWrongParameter;
  DeletePrimFontHandle;  
end;


procedure TrwPCLCanvas.PCLPrimFontPitch;
begin
  //Designates the horizontal spacing of a fixed spaced font in terms of the
  //number of characters per inch
  FPrimaryFont.Pitch := FPCLCommand.ParamValue[0];
  DeletePrimFontHandle;
end;


procedure TrwPCLCanvas.PCLCharData;
var
  n, i: Integer;
  Ch: TrwSoftChar;

  procedure ReadHeaderBMP;
  begin
    CopyMemory(Addr(Ch.CharHeader), Addr(FData[FCurPos]), SizeOf(Ch.CharHeader));
    with Ch.CharHeader do
    begin
      LeftOffset  := Swap(LeftOffset);
      TopOffset   := Swap(TopOffset);
      CharWidth   := Swap(CharWidth);
      CharHeight  := Swap(CharHeight);
      DeltaX      := Swap(DeltaX);
    end;

    Inc(FCurPos, Ch.CharHeader.DescriptorSize);
  end;

begin
  //Downloads the character descriptor and character data
  Ch := FFonts.FLoadingFont.Items[FFonts.FLoadingFont.Count - 1];
  n := Integer(FPCLCommand.ParamValue[0]) + FCurPos;
  Ch.CharFormat := NextByte;

  if NextByte = 0 then
  begin
    Inc(FCurPos);
    if Ch.CharFormat = 4 then
      ReadHeaderBMP
    else
      raise Exception.Create('Unknown char description format ' + IntToStr(Ch.CharFormat));
  end;

  i := High(Ch.CharData) + 1;
  n := n - FCurPos + 1;
  SetLength(Ch.CharData, Length(Ch.CharData) + n);
  if n > 0 then
  begin
    Assert((i >= 0) and (i <= High(Ch.CharData)));
    CopyMemory(Addr(Ch.CharData[i]), Addr(FData[FCurPos]), n);
  end;
  Inc(FCurPos, n - 1);
end;


procedure TrwPCLCanvas.PCLFontDescription;
var
  n: Integer;
  s: Word;
  F: TrwSoftFont;

  procedure ReadHeader0;
  begin
    CopyMemory(Addr(FFonts.FLoadingFont.Header), Addr(FData[FCurPos]), s);

    with FFonts.FLoadingFont.Header do
    begin
      HeaderSize  := Swap(HeaderSize);
      BaselinePos := Swap(BaselinePos);
      CellWidth   := Swap(CellWidth);
      CellHeight  := Swap(CellHeight);
      SymbolSet   := Swap(SymbolSet);
      Pitch       := Swap(Pitch);
      Height      := Swap(Height);
      xHeight     := Swap(xHeight);
      TextHeight  := Swap(TextHeight);
      TextWidth   := Swap(TextWidth);
      FirstCode   := Swap(FirstCode);
      LastCode    := Swap(LastCode);
      CapHeight   := Swap(CapHeight);
      FontNumber  := Swap(FontNumber);
    end;
  end;

begin
  //Downloads the font descriptor
  F := FFonts.FindFontByID(FFontID);
  if Assigned(F) then
    F.Free;

  FFonts.FLoadingFont := TrwSoftFont(FFonts.Add);
  FFonts.FLoadingFont.FontID := FFontID;

  n := Integer(FPCLCommand.ParamValue[0]) + FCurPos;
  Inc(FCurPos);
  s := MakeWord(FData[FCurPos + 1], FData[FCurPos]);
  if s = 64 then
    ReadHeader0
  else
    raise Exception.Create('Unknown font format ' + IntToStr(s));
  Inc(FCurPos, s - 1);

  FFonts.FLoadingFont.Copyright := '';
  while FCurPos < n do
    FFonts.FLoadingFont.Copyright :=  FFonts.FLoadingFont.Copyright + Chr(NextByte);
end;


procedure TrwPCLCanvas.PCLFontUnderlineOff;
begin
  FUnderline := pulNone;
end;

procedure TrwPCLCanvas.PCLFontUnderlineOn;
begin
  case Integer(FPCLCommand.ParamValue[0]) of
    0:  FUnderline := pulFixed;
    1:  FUnderline := pulFloating;
  end;  
end;

procedure TrwPCLCanvas.PCLFontID;
begin
  //Specifies an identification number (ID #) for use in subsequent font management commands
  FFontID := Integer(FPCLCommand.ParamValue[0]);
  DeletePrimFontHandle;
  FPrimaryFont.SoftFont := FFonts.FindFontByID(FFontID);
end;


procedure TrwPCLCanvas.PCLFontControl;
begin
  //Provides the means for manipulating soft fonts within the printer
  //0 - Delete all soft fonts
  //1 - Delete all temporary soft fonts
  //2 - Delete soft font (last ID specified)
  //3 - Delete Character Code (last ID and character code)
  //4 - Make soft font temporary (last ID specified)
  //5 - Make soft font permanent (last ID specified)
  //6 - Copy/Assign current invoked font as temporary

  case Integer(FPCLCommand.ParamValue[0]) of
    0:  FFonts.DeleteFons(False);
    1:  FFonts.DeleteFons(True);
    2:  FFonts.DeleteFont(FFontID);
    4:  FFonts.SetFontAttr(FFontID, False);
    5:  FFonts.SetFontAttr(FFontID, True);
  else
    ErrorWrongParameter;  
  end;
end;


procedure TrwPCLCanvas.PCLVertRectSizeDCP;
begin
  //Specifies the rectangular fill area height in decipoints or dots
  FRectSize.Y := DecipointToPixels(FPCLCommand.ParamValue[0]);
end;


procedure TrwPCLCanvas.PCLHorRectSizeDCP;
begin
  //Specifies the rectangular fill area width in decipoints or dots
  FRectSize.X := DecipointToPixels(FPCLCommand.ParamValue[0]);
end;


procedure TrwPCLCanvas.PCLHorRectSizeUnits;
begin
  //Specifies the rectangular fill area width in decipoints or dots
  FRectSize.X := UnitsToPixels(FPCLCommand.ParamValue[0]);
end;


procedure TrwPCLCanvas.PCLVertRectSizeUnits;
begin
  //Specifies the rectangular fill area height in decipoints or dots
  FRectSize.Y := UnitsToPixels(FPCLCommand.ParamValue[0]);
end;


procedure TrwPCLCanvas.PCLPatternID;
begin
  //Specifies the level of shading or type of cross-hatch to select via Fill
  //Rectangular Area command
  FPatternShading := Integer(FPCLCommand.ParamValue[0]);
end;


procedure TrwPCLCanvas.PCLUserPattern;
var
  n, i: Integer;
begin
  //Downloading the binary pattern data that defines the user pattern
  //!!!
  n := Integer(FPCLCommand.ParamValue[0]);
  i := 1;
  while i <= n do
  begin
    NextByte;
    Inc(i);
  end;
end;


procedure TrwPCLCanvas.PCLFillRect;
var
  R: TRect;
  c: Byte;
begin
  //Causes the defined rectangular area to be filled with the specified rule pattern

  R := Rect(FCursorPos.X + FLeftOffset + FLeftMargin, FCursorPos.Y + FTopOffset + FTopMargin,
      FCursorPos.X + FLeftOffset + FLeftMargin + FRectSize.X, FCursorPos.Y  + FTopOffset + FTopMargin + FRectSize.Y);

  case Integer(FPCLCommand.ParamValue[0]) of
    //Black fill (rule)
    0:  begin
          FCanvas.Brush.Color := clBlack;
          FCanvas.Brush.Style := bsSolid;
          FCanvas.Pen.Mode := pmCopy;
        end;

    //Erase (white) fill
    1:  begin
          FCanvas.Brush.Color := clWhite;
          FCanvas.Brush.Style := bsSolid;
          FCanvas.Pen.Mode := pmCopy;
        end;

    //Shaded fill
    2:  begin
          c := 255 - Round(255 * FPatternShading / 100);
          FCanvas.Brush.Color := PaletteRGB(c, c, c);
          FCanvas.Brush.Style := bsSolid;
          FCanvas.Pen.Mode := pmMask;
        end;

    //Cross-hatch fill
    3:  begin
           FCanvas.Brush.Color := clBlack;
           case FPatternShading of
             0: begin
                  FCanvas.Brush.Color := clNone;
                  FCanvas.Brush.Style := bsClear;
                end;
             1: FCanvas.Brush.Style := bsHorizontal;
             2: FCanvas.Brush.Style := bsVertical;
             3: FCanvas.Brush.Style := bsBDiagonal;
             4: FCanvas.Brush.Style := bsFDiagonal;
             5: FCanvas.Brush.Style := bsCross;
             6: FCanvas.Brush.Style := bsDiagCross;
           else
             ErrorWrongParameter;
           end;
           FCanvas.Pen.Mode := pmMask;
         end;

    //User-defined pattern fill
    4:  begin
          ErrorWrongParameter;
        end;

    //Current pattern fill
    5:  begin
          ErrorWrongParameter;
        end;
  end;

  FCanvas.Pen.Color := FCanvas.Brush.Color;
  FCanvas.Rectangle(R);
end;


procedure TrwPCLCanvas.PCLPictFrameHorSize;
begin
  //Specifies the horizontal dimension of the area to be allocated for rendering an HP-GL/2 plot
  FPictFrameHorSize := DecipointToPixels(Integer(FPCLCommand.ParamValue[0]));
end;


procedure TrwPCLCanvas.PCLPictFrameVertSize;
begin
  //Specifies the vertical dimension of the area to be allocated for rendering an HP-GL/2 plot
  FPictFrameVertSize := DecipointToPixels(Integer(FPCLCommand.ParamValue[0]));
end;


procedure TrwPCLCanvas.PCLPictFrameAnchor;
begin
  //Sets the picture frame anchor point to current PCL cursor position
  if Integer(FPCLCommand.ParamValue[0]) = 0 then
    FPictFrameAnchor := FCursorPos;
end;


procedure TrwPCLCanvas.PCLCharCode;
var
  Ch: TrwSoftChar;
begin
  //Establishes the decimal character code that will be associated with the
  //next character downloaded or deleted

  Ch := FFonts.FLoadingFont.FindChar(Word(FPCLCommand.ParamValue[0]));
  if not Assigned(Ch) then
    Ch := TrwSoftChar(FFonts.FLoadingFont.Add);
  SetLength(Ch.CharData, 0);
  Ch.CharCode := Word(FPCLCommand.ParamValue[0]);
end;


procedure TrwPCLCanvas.PCLSoftFontAsPrimary;
begin
  //Font Identification number ID
  FFontID := Integer(FPCLCommand.ParamValue[0]);
  DeletePrimFontHandle;
  FPrimaryFont.SoftFont := FFonts.FindFontByID(FFontID);
end;


procedure TrwPCLCanvas.PCLSimpleColor;
var
  c: TrwGrColorType;
begin
  //Creates a fixed-size palette whose color specifications cannot be modified
  //-3   3 planes, device CMY palette
  // 1   Single plane black and white palette
  // 3   3 planes, device RGB palette

  case Integer(FPCLCommand.ParamValue[0]) of
    -3: c := gtCMY;
     1: c := gtBlackAndWhite;
     3: c := gtRGB;
  else
    ErrorWrongParameter;
    c := gtRGB;
  end;

  FRaster.ColorType := c;
end;


procedure TrwPCLCanvas.PCLRasterGraphicsMode;
var
  gm: TrwRasterGrMode;
begin
  //Specifies the presentation of the raster image on the logical page
  
  if FRaster.ReceivingData then
    Exit;

  //Specifies the presentation of the raster image on the logical page
  case Integer(FPCLCommand.ParamValue[0]) of
    0: gm := gmCurPrintDirection;
    3: gm := gmAlongPageWidth;
  else
    ErrorWrongParameter;
    gm := gmCurPrintDirection;
  end;

  FRaster.Mode := gm;
end;


procedure TrwPCLCanvas.PCLStartGraphics;
var
  m: TrwGrLeftMarginType;
begin
  //Specifies the left raster graphics margin

  if FRaster.ReceivingData then
    Exit;

  //Specifies the left raster graphics margin
  case Integer(FPCLCommand.ParamValue[0]) of
    1: m := lmtCurCursorPos;
  else
    m := lmtDefault;
  end;

  FRaster.LeftMarginType := m;
  SetLength(FRaster.Data, 0);
  FRaster.DataSize := 0;
  FRaster.RowsCount := 0;
  FRaster.ReceivingData := True;
end;


procedure TrwPCLCanvas.PCLEndGraphics;
var
  BMP: TBitmap;
  bmpRec: tagBITMAP;
  R: TRect;
  k: Extended;
  NewData: array of Byte;
  i, j, m, CurrWidth: Integer;
begin
  //Signifies the end of a raster graphic image transfer

  if not FRaster.ReceivingData then
    Exit;

  SetLength(FRaster.Data, FRaster.DataSize);
  InvertBitmap(FRaster.Data);

  BMP := TBitmap.Create;
  try
    CurrWidth := FRaster.DataSize div FRaster.RowsCount;
    if CurrWidth mod 2 > 0 then
    begin
      // Correct bitmap data
      SetLength(NewData, Length(FRaster.Data) + FRaster.RowsCount);
      m := 0;
      for i := 0 to FRaster.RowsCount - 1 do
      begin
        for j := 0 to CurrWidth - 1 do
        begin
          NewData[m] := FRaster.Data[CurrWidth * i + j];
          Inc(m);
        end;
        NewData[m] := $FF;
        Inc(m);
      end;

      FRaster.DataSize := Length(NewData);
      SetLength(FRaster.Data, FRaster.DataSize);
      for i := Low(NewData) to High(NewData) do
        FRaster.Data[i] := NewData[i];
    end;

    bmpRec.bmType := 0;
    bmpRec.bmWidthBytes := FRaster.DataSize div FRaster.RowsCount;
    bmpRec.bmWidth := bmpRec.bmWidthBytes * 8;
    bmpRec.bmHeight := FRaster.RowsCount;
    bmpRec.bmPlanes := 1;
    bmpRec.bmBitsPixel := 1;
    bmpRec.bmBits := @(FRaster.Data[0]);

    BMP.TransparentColor := 1;
    BMP.TransparentMode := tmFixed;
    BMP.Transparent := True;

    BMP.Handle := CreateBitmapIndirect(bmpRec);
    if BMP.Handle = 0 then
      RaiseLastOSError;

    k := FCanvasRes / FRaster.Resolution;
    R := Rect(FCursorPos.X + FLeftOffset + FLeftMargin, FCursorPos.Y + FTopOffset + FTopMargin,
      Round(FCursorPos.X + FLeftOffset + FLeftMargin + bmpRec.bmWidth * k),
      Round(FCursorPos.Y + FTopOffset + FTopMargin + bmpRec.bmHeight * k));

    FCanvas.StretchDraw(R, BMP);
  finally
    BMP.Free;
  end;

  SetLength(FRaster.Data, 0);
  FRaster.ReceivingData := False;
end;


procedure TrwPCLCanvas.PCLUnitofMeasure;
begin
  //Establishes the unit of measure for the PCL unit
  FPCLUnit := Integer(FPCLCommand.ParamValue[0]);
  CalcPCLPageSize;
end;


procedure TrwPCLCanvas.PCLRasterGraphicsResolution;
begin
  //Designates the graphics resolution for raster data operations

  if FRaster.ReceivingData then
    Exit;
  FRaster.Resolution := Integer(FPCLCommand.ParamValue[0]);
end;


procedure TrwPCLCanvas.PCLPatternTransparency;
begin
  //Sets the pattern's transparency mode to transparent or opaque
  FTransparent := Boolean(Integer(FPCLCommand.ParamValue[0]));
end;


procedure TrwPCLCanvas.PCLCurrentPattern;
begin
  //Identifies the type of pattern to be applied to the source
  FPatternType := TrwPCLPatternType(Integer(FPCLCommand.ParamValue[0]));
end;


procedure TrwPCLCanvas.PCLGrCompression;
var
  c: TrwGrCompressionType;
begin
  //Determines how the printer interprets (decodes) the binary data in the
  //transfer Raster Data command

  case Integer(FPCLCommand.ParamValue[0]) of
    1: c := gctRunLength;
    2: c := gctTIFF4;
    3: c := gctDeltaRow;
    4: c := gctReserved;
    5: c := gctAdaptive;
  else
    c := gctUnencoded;
  end;

  FRaster.Compression := c;
end;


procedure TrwPCLCanvas.PCLRasterRowData;
var
  i, n: Integer;

  procedure CheckArraySize;
  begin
    if FRaster.DataSize = Length(FRaster.Data) then
      SetLength(FRaster.Data, Length(FRaster.Data) + 1024);
  end;

  procedure ComprUnencoded;
  begin
    CheckArraySize;
    FRaster.Data[FRaster.DataSize] := NextByte;
    Inc(FRaster.DataSize);
    Inc(i);
  end;

  procedure ComprRunLength;
  var
    b_rep, b_lit: Byte;
    j: Integer;
  begin
    b_rep := NextByte;
    Inc(i);
    if i > n then
      Exit;
    b_lit := NextByte;
    Inc(i);
    for j := 0 to Integer(b_rep) do
    begin
      CheckArraySize;
      FRaster.Data[FRaster.DataSize] := b_lit;
      Inc(FRaster.DataSize);
    end;
  end;

  procedure ComprTIFF4;
  var
    b_rep, b_lit: Byte;
    j: Integer;
  begin
    b_rep := NextByte;
    Inc(i);

    if b_rep in [0..127] then
      for j := 0 to Integer(b_rep) do
      begin
        if i > n then
          Exit;
        CheckArraySize;
        FRaster.Data[FRaster.DataSize] := NextByte;
        Inc(FRaster.DataSize);
        Inc(i);
      end

    else
    begin
      b_rep := not b_rep + 1;
      if i > n then
        Exit;
      b_lit := NextByte;
      Inc(i);
      for j := 0 to Integer(b_rep) do
      begin
        CheckArraySize;
        FRaster.Data[FRaster.DataSize] := b_lit;
        Inc(FRaster.DataSize);
      end;
    end;
  end;

begin
  //Transfers a row of raster graphics to the printer
  n := Integer(FPCLCommand.ParamValue[0]);

  i := 1;
  while i <= n do
  begin
    case FRaster.Compression of
      gctUnencoded: ComprUnencoded;

      gctRunLength: ComprRunLength;

      gctTIFF4:     ComprTIFF4;
    else
      raise Exception.Create('Unknown graphic compression method');
    end;
  end;

  FRaster.RowsCount := FRaster.RowsCount + 1;
end;


procedure TrwPCLCanvas.PCLHPGL2On;
begin
  //Causes printer to begin interpreting the incoming data stream as HP-GL/2
  //commands instead of PCL commands
  FHPGL2Mode := True;
end;


procedure TrwPCLCanvas.PCLHPGL2Off;
begin
  //Causes printer to return to PCL mode from HP-GL/2 mode
  FHPGL2Mode := False;
end;


procedure TrwPCLCanvas.RunPCL(AStream: TStream);
begin
  AStream.Position := 0;
  SetLength(FData, AStream.Size);
  AStream.Read(FData[0], AStream.Size);

  FPainting := False;
  Paint;
end;


procedure TrwPCLCanvas.Paint;
begin
  if FPainting then
    Exit;

  FPainting := True;
  try
    FCurPos := Low(FData);

    while FCurPos <= High(FData) do
    begin
      FTextData := '';
      while not (FData[FCurPos] in [pESC, pCR, pLF, pFF]) do
      begin
        if FData[FCurPos] <> 0 then
          FTextData := FTextData + Chr(FData[FCurPos]);

        if FCurPos = High(FData) then
          break;
        NextByte;
      end;

      if Length(FTextData) > 0 then
        if FHPGL2Mode then
  //        HPGL2Parser
        else
          DrawPCLText;

      if FCurPos >= High(FData) then
        break;

      ParsePCLCommand;

      if FCurPos >= High(FData) then
        break;

      NextByte;
    end;

  finally
    FPainting := False;
  end;
end;


procedure TrwPCLCanvas.DeletePrimFontHandle;
begin
  if FPrimaryFont.Handle <> 0 then
  begin
    DeleteObject(FPrimaryFont.Handle);
    FPrimaryFont.Handle := 0;
  end;

  FPrimaryFont.SoftFont := nil;
end;


procedure TrwPCLCanvas.DeleteSecFontHandle;
begin
  if FSecondaryFont.Handle <> 0 then
  begin
    DeleteObject(FSecondaryFont.Handle);
    FSecondaryFont.Handle := 0;
  end;

  FSecondaryFont.SoftFont := nil;
end;


procedure TrwPCLCanvas.SoftFontTextOut;
var
  i, lofs, tofs: Integer;
  Ch: TrwSoftChar;
  BMP: TBitmap;
  bmpRec: tagBITMAP;
  R, Rmax: TRect;
  k: Extended;
  P: TPoint;
  lPitch: Integer;
begin
  BMP := TBitmap.Create;
  try
    BMP.TransparentColor := clWhite;
    BMP.TransparentMode := tmFixed;
    BMP.Transparent := True;

    bmpRec.bmType := 0;
    bmpRec.bmPlanes := 1;
    bmpRec.bmBitsPixel := 1;
    k := FCanvasRes / 300;

    if FCalcTextRect then
    begin
      P := Point(10000, 10000);

      case FPrimaryFont.SoftFont.Header.Orientation of
        0, 2: FCursorPos := Point(0, Round(FPrimaryFont.SoftFont.Header.CellHeight * k));
        1, 3: FCursorPos := Point(Round(FPrimaryFont.SoftFont.Header.CellWidth * k), 0);
      end;

      R := Rect(P.X, P.Y, P.X + FCursorPos.X, P.Y + FCursorPos.Y);
      P := Point(P.X + FCursorPos.X, P.Y + FCursorPos.Y);
    end
    else
    begin
      P := Point(FCursorPos.X + FLeftOffset + FLeftMargin, FCursorPos.Y + FTopOffset + FTopMargin);
      R := Rect(P.X, P.Y, P.X, P.Y);
    end;

    Rmax := R;

    if FPrimaryFont.SoftFont.Header.Spacing <> 0 then
      lPitch := 0
    else
      lPitch := Round(FCanvasRes / ((300 * 4) / FPrimaryFont.SoftFont.Header.Pitch));

    for i := 1 to Length(FTextData) do
    begin
      Ch := FPrimaryFont.SoftFont.FindChar(Ord(FTextData[i]));

      if Assigned(Ch) and (FPrimaryFont.SoftFont.Header.Orientation = Ch.CharHeader.Orientation) then
      begin
        bmpRec.bmWidthBytes := WholeWords(WholeBytes(Ch.CharHeader.CharWidth)) * 2;
        bmpRec.bmWidth :=  Ch.CharHeader.CharWidth;
        bmpRec.bmHeight := Ch.CharHeader.CharHeight;
        bmpRec.bmBits := @(Ch.CharData[0]);
        BMP.Handle := CreateBitmapIndirect(bmpRec);
        lofs := Round(Smallint(Ch.CharHeader.LeftOffset) * k);
        tofs := Round(Smallint(Ch.CharHeader.TopOffset) * k);

        case FPrimaryFont.SoftFont.Header.Orientation of
          0: begin
                R := Rect(P.X + lofs, P.Y - tofs, Round(P.X + bmpRec.bmWidth * k + lofs), Round(P.Y + bmpRec.bmHeight * k) - tofs);

                if not FCalcTextRect then
                  FCanvas.StretchDraw(R, BMP);

                if (FPrimaryFont.SoftFont.Header.Spacing <> 0) and (Ch.CharHeader.DeltaX <> 0) then
                  P.X := P.X + Round((Smallint(Ch.CharHeader.DeltaX) / 4) * k)
                else
                  P.X := R.Right;

                if lPitch > 0 then
                  P.X := P.X + (lPitch - Round(bmpRec.bmWidth * k));

                FCursorPos := P;
              end;

          1: begin
                R := Rect(P.X + lofs, P.Y - tofs, Round(P.X + bmpRec.bmWidth * k), Round(P.Y + bmpRec.bmHeight * k - tofs));

                if not FCalcTextRect then
                  FCanvas.StretchDraw(R, BMP);

                if (FPrimaryFont.SoftFont.Header.Spacing <> 0) and (Ch.CharHeader.DeltaX <> 0) then
                  P.Y := P.Y - Round((Smallint(Ch.CharHeader.DeltaX) / 4) * k)
                else
                  P.Y := R.Top;

                if lPitch > 0 then
                  P.Y := P.Y + (lPitch - Round(bmpRec.bmHeight * k));

                FCursorPos := P;
              end;
        else
          raise Exception.Create('PCL Soft Font: Not implemented branch');
        end;
      end

      else
      begin
        case FPrimaryFont.SoftFont.Header.Orientation of
          0: begin
               R := Rect(P.X, P.Y, Round(P.X + FPrimaryFont.SoftFont.Header.CellWidth * k), Round(P.Y + FPrimaryFont.SoftFont.Header.CellHeight * k));
               P.X := R.Right;
               FCursorPos := P;
             end;

          1: begin
                R := Rect(P.X, P.Y, Round(P.X + FPrimaryFont.SoftFont.Header.CellWidth * k), Round(P.Y + FPrimaryFont.SoftFont.Header.CellHeight * k));
                P.Y := R.Top;
                FCursorPos := P;
             end;
        else
          raise Exception.Create('PCL Soft Font: Not implemented branch');
        end;
      end;

      if FCalcTextRect then
      begin
        if Rmax.Left > R.Left then
          Rmax.Left := R.Left;
        if Rmax.Right < R.Right then
          Rmax.Right := R.Right;
        if Rmax.Top > R.Top then
          Rmax.Top := R.Top;
        if Rmax.Bottom < R.Bottom then
          Rmax.Bottom := R.Bottom;
      end;
    end;

    if FCalcTextRect then
      FCursorPos := Point(Abs(Rmax.Right - Rmax.Left), Abs(Rmax.Bottom - Rmax.Top));

  finally
    BMP.Free;
  end;
end;


procedure TrwPCLCanvas.PaintTo(AStream: TStream; ACanvas: TCanvas; ACursorPos: TPoint; ADPI: Integer = 0);
var
  k: Extended;
begin
  FCanvas := ACanvas;

//  FCanvasRes := GetDeviceCaps(FCanvas.Handle, LOGPIXELSX);
  FCanvasRes := cScreenCanvasRes;

  if not NoMargins then
  begin
    FDevPhysOffSet.X := GetDeviceCaps(FCanvas.Handle, PHYSICALOFFSETX);
    FDevPhysOffSet.Y := GetDeviceCaps(FCanvas.Handle, PHYSICALOFFSETY);
  end;

  if ADPI > 0 then
  begin
    k := ADPI / FCanvasRes;
    FDevPhysOffSet.X := Round(FDevPhysOffSet.X * k);
    FDevPhysOffSet.Y := Round(FDevPhysOffSet.Y * k);
    FCanvasRes := ADPI;
  end;

  ResetPrinter;
  FCursorPos := ACursorPos;
  RunPCL(AStream);
end;


procedure TrwPCLCanvas.PaintTo(AStream: TStream; AMetafile: TMetafile; ACursorPos: TPoint; ADPI: Integer = 0);
var
  C: TMetafileCanvas;
begin
  if (AMetafile.Width = 0) and (AMetafile.Height = 0) then
  begin
    C := TMetafileCanvas.Create(AMetafile, 0);
    try
     PaintTo(AStream, C, ACursorPos, ADPI);
    finally
     C.Free;
    end;

    AMetafile.Clear;
    AMetafile.Width := FPaperWidth;
    AMetafile.Height := FPaperHeight;
  end;

  AMetafile.Transparent := True;
  C := TMetafileCanvas.Create(AMetafile, 0);
  try
    PaintTo(AStream, C, ACursorPos, ADPI);
  finally
   C.Free;
  end;
end;


function TrwPCLCanvas.CalcTextRect(AStream: TStream; ACanvas: TCanvas; ADPI: Integer): TRect;
begin
  NoMargins := True;
  FCalcTextRect := True;
  try
    PaintTo(AStream, ACanvas, Point(0, 0), ADPI);
    Result := Rect(0, 0, FCursorPos.X, FCursorPos.Y);
  finally
    FCalcTextRect := False;
  end;
end;


procedure TrwPCLCanvas.SetCursorPosition(const APoint: TPoint);
var d: Integer;

  procedure DrawUnderline;
  var
    p: Integer;
  begin
    FCanvas.Pen.Color := clBlack;
    FCanvas.Pen.Mode := pmBlack;
    FCanvas.Pen.Style := psSolid;

    case FUnderline of
      pulFixed, pulFloating:
        begin
          p := UnitsToPixels(5);
          FCanvas.Pen.Width := UnitsToPixels(3);
          FCanvas.MoveTo(FCursorPos.X, FCursorPos.Y + p);
          FCanvas.LineTo(FCursorPos.X + d, FCursorPos.Y + p);
        end;
    end;
  end;

begin
  d := APoint.X - FCursorPos.X;
  if (d > 0) and (FUnderline <> pulNone) then
    DrawUnderline;
  FCursorPos := APoint;
end;

function TrwPCLCanvas.NelcoPCLFonts: TrwSoftFonts;

  function GetFontList(hModule: THandle; lpszType: PAnsiChar; lpszName: PAnsiChar; lParam: Cardinal): Boolean; stdcall;
  begin
    TrwSoftFont(FNelcoPCLFontsSrc.Add).Copyright := lpszName;
    Result := True;
  end;

  procedure InitializeNelcoFonts;
  var
    i: Integer;
    HR: HRSRC;
    SR: Cardinal;
    PR: Pointer;
    C:  TrwPCLCanvas;
  begin
    EnumResourceNames(HINSTANCE, 'PCLFONT', @GetFontList, 0);

    C := TrwPCLCanvas.Create;
    try

      for i := 0 to FNelcoPCLFontsSrc.Count - 1 do
      begin
        HR := FindResource(HINSTANCE, PAnsiChar(FNelcoPCLFontsSrc[i].Copyright), 'PCLFONT');
        SR := SizeofResource(HINSTANCE, HR);
        PR := Pointer(LoadResource(HINSTANCE, HR));
        SetLength(C.FData, SR);
        CopyMemory(@(C.FData[0]), PR, SR);
        C.FPainting := False;
        C.Paint;
      end;

      FNelcoPCLFontsSrc.Assign(C.FFonts);
      
    finally
      C.Free;
    end;
  end;

begin
  if not Assigned(FNelcoPCLFonts) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(FNelcoPCLFontsSrc) then
      begin
        FNelcoPCLFontsSrc := TrwSoftFonts.Create;
        InitializeNelcoFonts;
      end;
      FNelcoPCLFonts := FNelcoPCLFontsSrc;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := FNelcoPCLFonts;
end;



procedure TrwPCLCanvas.PCLSecFontSymbolSet;
var
  h:  String;
begin
  //Designates the set of symbols or characters contained in a font
  h := IntToStr(Integer(FPCLCommand.ParamValue[0])) + Chr(FPCLCommand.Terminator);
  FSecondaryFont.SymbolSet := h;
  DeleteSecFontHandle;
end;

procedure TrwPCLCanvas.PCLSoftFontAsSecondary;
begin
  //Font Identification number ID
  FFontID := Integer(FPCLCommand.ParamValue[0]);
  DeleteSecFontHandle;
  FSecondaryFont.SoftFont := FFonts.FindFontByID(FFontID);
end;

procedure TrwPCLCanvas.PCLSecFontHeight;
begin
  //Designates the height of the font in points
  FSecondaryFont.Height := Round(FPCLCommand.ParamValue[0]);
  DeleteSecFontHandle;
end;

procedure TrwPCLCanvas.PCLSecFontPitch;
begin
  //Designates the horizontal spacing of a fixed spaced font in terms of the
  //number of characters per inch
  FSecondaryFont.Pitch := FPCLCommand.ParamValue[0];
  DeleteSecFontHandle;
end;

procedure TrwPCLCanvas.PCLSecFontSpacing;
begin
  //Designates either a fixed or proportionally spaced font
  FSecondaryFont.Proportional := Boolean(Integer(FPCLCommand.ParamValue[0]));
  DeleteSecFontHandle;
end;

procedure TrwPCLCanvas.PCLSecFontStrokeWeight;
begin
  //Designates the thickness or weight of the stroke that composes the characters of a font
  FSecondaryFont.StrokeWeight := NativeStrokeWeightToType(Integer(FPCLCommand.ParamValue[0]));
  DeleteSecFontHandle;
end;

procedure TrwPCLCanvas.PCLSecFontStyle;
begin
  //Designates the font style
  FSecondaryFont.Style := NativeFontStyleToType(Integer(FPCLCommand.ParamValue[0]));
  if FSecondaryFont.Style = pfsUnknown then
    ErrorWrongParameter;

  DeleteSecFontHandle;
end;

procedure TrwPCLCanvas.PCLSecFontTypeface;
begin
  //Designates the design of the font
  FSecondaryFont.Typeface := NativeTypefaceToType(Integer(FPCLCommand.ParamValue[0]));
  if FSecondaryFont.Typeface = '' then
    ErrorWrongParameter;
  DeleteSecFontHandle;
end;

{ TrwSoftFonts }

constructor TrwSoftFonts.Create;
begin
  inherited Create(TrwSoftFont);
  FLoadingFont := nil;
end;


destructor TrwSoftFonts.Destroy;
begin
  FLoadingFont.Free;
  inherited;
end;


procedure TrwSoftFonts.DeleteFons(ATempOnly: Boolean);
var
 i: Integer;
begin
  if ATempOnly then
  begin
    i := 0;
    while i < Count do
    begin
      if not Items[i].Permanent then
        Items[i].Free
      else
        Inc(i);
    end
  end
  else
    Clear;
end;


procedure TrwSoftFonts.DeleteFont(AFontID: Word);
var
  F: TrwSoftFont;
begin
  F := FindFontByID(AFontID);
  if Assigned(F) then
    F.Free;
end;


function TrwSoftFonts.FindFontByID(const AFontID: Word): TrwSoftFont;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if Items[i].FontID = AFontID then
    begin
      Result := Items[i];
      break;
    end;
end;


function TrwSoftFonts.GetItem(Index: Integer): TrwSoftFont;
begin
  Result := TrwSoftFont(inherited Items[Index]);
end;


procedure TrwSoftFonts.SetFontAttr(AFontID: Word; APermanent: Boolean);
var
  F: TrwSoftFont;
begin
  F := FindFontByID(AFontID);
  if Assigned(F) then
  begin
    F.Permanent := APermanent;
    CorrectFont(F);
  end;
end;


procedure TrwSoftFonts.SetItem(Index: Integer; const Value: TrwSoftFont);
begin
  inherited Items[Index].Assign(Value);
end;


function TrwSoftFonts.FindFontByDescriptor(const ADescr: TrwFontRec): TrwSoftFont;
var
  i: Integer;
  SS, PT, HT: Word;
  L: TList;

  function CompareProp(ALevel: Byte; AFnt: TrwSoftFont): Boolean;
  begin
    case ALevel of
      1: Result := AFnt.Header.SymbolSet = SS;
      2: Result := Boolean(AFnt.Header.Spacing) = ADescr.Proportional;
      3: Result := AFnt.Header.Pitch = PT;
      4: Result := AFnt.Header.Height = HT;
      5: Result := NativeFontStyleToType(AFnt.Header.StyleMSB) = ADescr.Style;
      6: Result := NativeStrokeWeightToType(AFnt.Header.StrokeWeight) = ADescr.StrokeWeight;
      7: Result := AnsiSameText(NativeTypefaceToType(MakeWord(AFnt.Header.TypefaceMSB, AFnt.Header.TypefaceLSB)), ADescr.Typeface);
    else
      Result := False;
    end;
  end;

  procedure CompareLevel(ALevel: Byte);
  var
    i: Integer;
  begin
    i := 0;
    while i < L.Count do
      if CompareProp(ALevel, TrwSoftFont(L[i])) then
        Inc(i)
      else
        L.Delete(i);
  end;

begin
  Result := nil;
  i := Length(ADescr.SymbolSet);
  SS := StrToInt(Copy(ADescr.SymbolSet, 1, i - 1)) * 32 + (Ord(ADescr.SymbolSet[i]) - 64);
  PT := Round(1 / ADescr.Pitch * 300 * 4);
  HT := Round(ADescr.Height / 72 * 300 * 4);

  L := TList.Create;
  try
    for i := 0 to Count - 1 do
      L.Add(Items[i]);

    for i := 1 to 5 do
      if L.Count = 0 then
        break
      else
        CompareLevel(i);

    if L.Count > 1 then
      CompareLevel(6);
    if L.Count > 1 then
      CompareLevel(7);

    if L.Count >= 1 then
      Result := TrwSoftFont(L[0]);

  finally
    L.Free;
  end;
end;


procedure TrwSoftFonts.CorrectFont(AFont: TrwSoftFont);
var
  i: Integer;

  procedure CorrectChar(AChar: TrwSoftChar);
  var
    j, w, ww: Integer;
    C: array of byte;
  begin
    InvertBitmap(AChar.CharData);
    w := WholeBytes(AChar.CharHeader.CharWidth);
    if (w mod 2) <> 0 then
    begin
      ww := WholeWords(w);
      SetLength(C, ww * 2 * AChar.CharHeader.CharHeight);
      for j := 0 to AChar.CharHeader.CharHeight - 1 do
      begin
        CopyMemory(Addr(C[j * ww * 2]), Addr(AChar.CharData[j * w]), w);
        C[j * ww * 2 + w] := 0;
      end;

      SetLength(AChar.CharData, Length(C));
      CopyMemory(Addr(AChar.CharData[0]), Addr(C[0]), Length(C));
    end;
  end;

  procedure DecompressChar(AChar: TrwSoftChar);
  var
    i, w, cnt, j, cw, beg_line: Integer;
    C: array of byte;
    bMask, bRest: Byte;
    bRestSize: Integer;

    bRep: Byte;
    b: Byte;
    clWht: Boolean;
  begin
    SetLength(C, WholeBytes(AChar.CharHeader.CharWidth) * AChar.CharHeader.CharHeight);
    FillMemory(Addr(C[0]), Length(C), 0);

    cnt := Low(C);
    i := Low(AChar.CharData);
    while i <= High(AChar.CharData) do
    begin
      bRep := AChar.CharData[i];
      Inc(i);

      clWht := True;
      bRest := 0;
      bRestSize := 0;
      cw := 0;
      beg_line := cnt;
      while cw < AChar.CharHeader.CharWidth do
      begin
        b := AChar.CharData[i];
        Inc(cw, b);
        Inc(i);

        while bRestSize > 0 do
        begin
          j := bRestSize + 1;
          bMask := 0;
          while (j <= 8) and (b > 0) do
          begin
            bMask := (bMask shr 1) or (128 shr bRestSize);
            Dec(b);
            Inc(j);
          end;
          if clWht then
            bRest := (not bMask) and bRest
          else
            bRest := bMask or bRest;

          if (j = 9) or (cw = AChar.CharHeader.CharWidth) then
          begin
            bRestSize := 0;
            C[cnt] := bRest;
            Inc(cnt);
          end
          else
          begin
            clWht := not clWht;
            bRestSize := j - 1;
            b := AChar.CharData[i];
            Inc(cw, b);
            Inc(i);
          end;
        end;

        w := b div 8;
        for j := 1 to w do
        begin
          if clWht then
            C[cnt] := 0
          else
            C[cnt] := 255;
          Inc(cnt);
        end;

        bRestSize := b mod 8;
        bRest := 0;
        if not clWht then
        begin
          bMask := 0;
          for j := 1 to bRestSize do
            bMask := (bMask shr 1) or 128;
          bRest := bMask or bRest;
        end;

        clWht := not clWht;
      end;

      if bRestSize > 0 then
      begin
        C[cnt] := bRest;
        Inc(cnt);
      end;

      w := WholeBytes(AChar.CharHeader.CharWidth);
      for j := 1 to Integer(bRep) do
      begin
        CopyMemory(Addr(C[cnt]), Addr(C[beg_line]), w);
        Inc(cnt, w);
      end;
    end;

    SetLength(AChar.CharData, Length(C));
    CopyMemory(Addr(AChar.CharData[0]), Addr(C[0]), Length(C));
  end;

begin
  if AFont.FCorrected then
    Exit;

  for i := 0 to AFont.Count - 1 do
  begin
    if Length(AFont[i].CharData) = 0 then
      Continue;

    if AFont[i].CharHeader.DescriptorClass = 1 then   //Bitmap
      CorrectChar(AFont[i])

    else if AFont[i].CharHeader.DescriptorClass = 2 then   //Compressed Bitmap
    begin
      DecompressChar(AFont[i]);
      CorrectChar(AFont[i]);
    end

    else
      raise Exception.Create('Unknown char class ' + IntToStr(AFont[i].CharHeader.DescriptorClass));
  end;

  AFont.FCorrected := True;
end;



{ TrwSoftFont }

constructor TrwSoftFont.Create(Collection: TCollection);
begin
  inherited;
  FChars := TCollection.Create(TrwSoftChar);
  FCorrected := False;  
end;


destructor TrwSoftFont.Destroy;
begin
  FChars.Free;

  inherited;
end;


function TrwSoftFont.Count: Integer;
begin
  Result := FChars.Count;
end;


function TrwSoftFont.FindChar(AChar: Word): TrwSoftChar;
var
  i: Integer;
begin
  Result := nil;
  
  for i := 0 to Count - 1 do
    if Items[i].CharCode = AChar then
    begin
      Result := Items[i];
      break;
    end;
end;


function TrwSoftFont.GetItem(Index: Integer): TrwSoftChar;
begin
  Result := TrwSoftChar(FChars.Items[Index]);
end;


procedure TrwSoftFont.Clear;
begin
  FChars.Clear;
end;

function TrwSoftFont.Add: TrwSoftChar;
begin
  Result := TrwSoftChar(FChars.Add);
end;


procedure TrwSoftFont.Assign(ASource: TPersistent);
begin
  FChars.Assign(TrwSoftFont(ASource).FChars);
  FontID := TrwSoftFont(ASource).FontID;
  Permanent := TrwSoftFont(ASource).Permanent;
  Header := TrwSoftFont(ASource).Header;
  Copyright := TrwSoftFont(ASource).Copyright;
end;


{ TrwSoftChar }

procedure TrwSoftChar.Assign(ASource: TPersistent);
var
  s: Integer;
begin
  CharHeader := TrwSoftChar(ASource).CharHeader;
  CharCode := TrwSoftChar(ASource).CharCode;
  CharFormat := TrwSoftChar(ASource).CharFormat;
  s := Length(TrwSoftChar(ASource).CharData);
  SetLength(CharData, s);
  CopyMemory(@(CharData[0]), @(TrwSoftChar(ASource).CharData[0]), s);
end;

initialization

finalization
  if Assigned(FNelcoPCLFontsSrc) then
    FNelcoPCLFontsSrc.Free;

end.
