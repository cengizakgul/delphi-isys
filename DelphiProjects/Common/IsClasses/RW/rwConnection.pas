unit rwConnection;

interface

uses Windows, SysUtils, Messages, Forms,
     isBaseClasses, isObjectPool, rwConnectionInt, EvStreamUtils, isBasicUtils,
     evTransportInterfaces, evTransportShared, evTransportDatagrams, isThreadManager,
     isUtils, Dialogs, isSettings, isExceptions;

type
  IevRWCommClient = interface
  ['{BCA17F9E-0195-412F-AFB7-C5FD89DD0AF5}']
    function  GetDatagramDispatcher: IevDatagramDispatcher;
    function  Connect(const APort: String): TisGUID;
  end;

  IevRWConnectionPool = interface
  ['{37F2FFBB-D180-48A1-BD62-7EED36B8F07D}']
    function  CommClient: IevRWCommClient;
    function  GetRWConnection(const ARWObjectName: String): IInterface;
    procedure ReturnRWConnection(const ARWConnection: IInterface);
    function  CheckRWConnection(const ARWConnection: IInterface): Boolean;
  end;


  function RWConnectionPool: IevRWConnectionPool;

implementation

type
  TevRWCommClient = class(TevCustomTCPClient, IevRWCommClient)
  protected
    function  Connect(const APort: String): TisGUID;
    function  CreateDispatcher: IevDatagramDispatcher; override;
  end;

  TevRWDatagramDispatcher = class(TevDatagramDispatcher)
  protected
    procedure DoOnIdle; override;
  end;


  TevRWConnectionPool = class(TisObjectPool, IevRWConnectionPool)
  private
    FCommClient: IevRWCommClient;
  protected
    procedure DoOnConstruction; override;
    function  CreateObject(const AObjectName: String): IisObjectFromPool; override;
    function  CommClient: IevRWCommClient;
    function  GetRWConnection(const ARWObjectName: String): IInterface;
    procedure ReturnRWConnection(const ARWConnection: IInterface);
    function  CheckRWConnection(const ARWConnection: IInterface): Boolean;
  end;


  IrwProxy = interface
  ['{9AF24399-E147-4D98-A6E6-8DEB18AA1387}']
    function IsAlive: Boolean;
  end;

  TrwProxy = class(TisObjectFromPool, IrwProxy)
  private
    FSession: IevSession;
    function  CreateDatagram(const AMethod: String): IevDatagram;
    function  GetExeName: String; virtual; abstract;
    procedure Connect;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  IsAlive: Boolean;
  protected
    procedure  DoOnConstruction; override;
    destructor Destroy; override;
  end;

  
  // Engine
  TrwEngineAppProxy = class(TrwProxy, IrwEngineApp)
  private
    FReportStructure: IrwReportStructure;
    FProcessController: IrwProcessController;

    procedure OpenReportFromStream(const AStream: IevDualStream);
    procedure OpenReportFromClass(const AClassName: String);
    function  GetActiveReport: IrwReportStructure;
    procedure CloseActiveReport;
    function  RunActiveReport(const ARunSettings: TrwRunReportSettings): TrwReportResult;
    function  RunReport(const AReportData: IevDualStream; const AReportName: String; out AReportResult: IevDualStream): Variant;
    function  GetProcessController: IrwProcessController;
    procedure InitializeAppAdapter(const AModuleName: String; const AAppCustomData: Variant);
    function  RWAtoPDF(const ARWAData: IevDualStream; const APDFInfo: TrwPDFFileInfo): IevDualStream;
    function  CallAppAdapterFunction(const AFunctionName: String; const AParams: Variant): Variant;
    function  GetClassInfo(const AClassName: String): TrwClassInfo;
    function  ClassInheritsFrom(const AClassName, AAncestorClassName: String): Boolean;
    function  ChangeRWAInfo(const ARWAData: IevDualStream; const ACurrentPassword: String;
                            const ANewInfo: TrwRWAFileInfo):IevDualStream;
    procedure SetExternalModuleData(const AModuleName: String; const AData: IevDualStream);
    function  OpenQueryBuilder(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
    function  GetMainWindowHandle: Integer;
    function  RunQuery(const AQueryData: IevDualStream; const AParams: Variant): Variant;
    function  QueryInfo(const AItemName: String): String;
    function  OpenQueryBuilder2(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
    function  IsQueryBuilderFormClosed(out AQueryData: IevDualStream): Boolean;
  protected
    procedure DoOnConstruction; override;
    function  GetExeName: String; override;
  end;


  TrwProcessController = class(TisInterfacedObject, IrwProcessController)
  private
    FOwner: TrwEngineAppProxy;
      
    procedure Run(const ARunAction: TrwRunAction);
    procedure Pause;
    procedure Stop;
    function  GetVMRegisters: TrwVMRegisters;
    function  GetState: TrwProcessState;
    function  CalcExpression(const AExpression: String; const ADebugSymbolInfo: String): TrwVMExpressionResult;
    function  GetVMCallStack: Variant;
    procedure SetBreakpoints(const ABreakpoints: Variant);
    procedure OpenQueryBuilder(const AQueryData: IevDualStream; const ADescriptionView: Boolean);
  public
    constructor Create(const AOwner: TrwEngineAppProxy);
  end;


  TrwReportStructure = class(TisInterfacedObject, IrwReportStructure, IrwReportInputForm, IrwReportParameters)
  private
    FOwner: TrwEngineAppProxy;

    // IrwReportStructure
    function  GetReportParameters: IrwReportParameters;
    function  GetData: IevDualStream;
    function  GetReportInputForm: IrwReportInputForm;
    function  GetStructureInfo: TrwReportStructureInfo;
    procedure Compile;
    procedure PrepareForRunning;

    // IrwReportInputForm
    procedure CloseOK;
    procedure CloseCancel;
    function  Show(const AParentWndHandle: Integer; const ASetupMode: Boolean): Boolean;
    function  GetFormBounds: TrwBounds;
    procedure SetFormBounds(const ABounds: TrwBounds);

    // IrwReportParameters
    function  GetCount: Integer;
    function  ParamByName(const AParamName: String): TrwReportParam;
    function  IndexOf(const AParamName: String): Integer;
    function  GetItems(const AIndex: Integer): TrwReportParam;
    function  GetParamValue(const AParamName: String): Variant;
    procedure SetParamValue(const AParamName: String; const AValue: Variant);
    procedure ParamsFromStream(const AParams: IevDualStream);
    function  ParamsToStream: IevDualStream;
  public
    constructor Create(const AOwner: TrwEngineAppProxy);
  end;


  // Preview
  TrwPreviewAppProxy = class(TrwProxy, IrwPreviewApp)
  private
    function  QueryInfo(const AItemName: String): String;
    procedure Preview(const AReportResult: IevDualStream; const AParentWindow: Integer);
    procedure Print(const AReportResult: IevDualStream; const APrintJobInfo: TrwPrintJobInfo);
    function  ReportsInGroup: Integer;
    procedure ClearGroup;
    procedure AddToGroup(const AReportResult: IevDualStream; const AReportResultInfo: TrwReportResultInfo;
                               const APrintJobInfo: TrwPrintJobInfo);
    procedure PreviewGroup(const AParentWindow: Integer);
    procedure PrintGroup(const APrintJobInfo: TrwPrintJobInfo);
    function  MergeGroup: IevDualStream;
    function  GetFormBounds: TrwBounds;
    procedure SetFormBounds(const ABounds: TrwBounds);
  protected
    function  GetExeName: String; override;
  end;


  // Designer
  TrwDesignerAppProxy = class(TrwProxy, IrwDesignerApp)
  private
    function  OpenDesigner(const AReportData: IevDualStream; const AReportName: String;
                           const ADefaultReportType: Integer): Boolean;
    function  OpenQueryBuilder(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
    procedure InitializeAppAdapter(const AModuleName: String; const AAppCustomData: Variant);
    function  CallAppAdapterFunction(const AFunctionName: String; const AParams: Variant): Variant;
    function  RunQuery(const AQueryData: IevDualStream; const AParams: Variant): Variant;
    function  QueryInfo(const AItemName: String): Variant;
    function  OpenDesigner2(const AReportData: IevDualStream; const AReportName: String;
                            const ADefaultReportType: Integer): Boolean;
    function  IsDesignerFormClosed(out AReportData: IevDualStream): Boolean;
    function  OpenQueryBuilder2(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
    function  IsQueryBuilderFormClosed(out AQueryData: IevDualStream): Boolean;
  protected
    function  GetExeName: String; override;
  end;


var FRWConnectionPool: IevRWConnectionPool;
var InitLock: IisLock;
var AppMutexName: String; // Sort of unique process ID
var AppMutex: THandle;    // Used by RW to terminate itself if App dies 


function RWConnectionPool: IevRWConnectionPool;
begin
  InitLock.Lock;
  try
    if not Assigned(FRWConnectionPool) then
    begin
      FRWConnectionPool := TevRWConnectionPool.Create('RW Pool Garbage Collector', nil);
      (RWConnectionPool as IisObjectPool).MaxPoolSize := HowManyProcessors * 2;
      (RWConnectionPool as IisObjectPool).ObjectExpirationTime := 15;
    end;

    Result := FRWConnectionPool;
  finally
    InitLock.Unlock;
  end;
end;

{ TevRWConnectionPool }

function TevRWConnectionPool.CheckRWConnection(const ARWConnection: IInterface): Boolean;
begin
  Result := (ARWConnection as IrwProxy).IsAlive;
end;

function TevRWConnectionPool.CommClient: IevRWCommClient;
begin
  Result := FCommClient;
end;

function TevRWConnectionPool.CreateObject(const AObjectName: String): IisObjectFromPool;
begin
  if AObjectName = 'RW Engine' then
    Result := TrwEngineAppProxy.Create(AObjectName)
  else if AObjectName = 'RW Designer' then
    Result := TrwDesignerAppProxy.Create(AObjectName)
  else if AObjectName = 'RW Preview' then
    Result := TrwPreviewAppProxy.Create(AObjectName);
end;

procedure TevRWConnectionPool.DoOnConstruction;
begin
  inherited;
  FCommClient := TevRWCommClient.Create;
end;

function TevRWConnectionPool.GetRWConnection(const ARWObjectName: String): IInterface;
begin
  try
    repeat
      Result := AcquireObject(ARWObjectName);
    until CheckRWConnection(Result);
  except
    Result := AcquireObject(ARWObjectName); // do it one more time for broken connections
  end;
end;

procedure TevRWConnectionPool.ReturnRWConnection(const ARWConnection: IInterface);
begin
  if CheckRWConnection(ARWConnection) then
    ReturnObject(ARWConnection as IisObjectFromPool);
end;

{ TevRWCommClient }

function TevRWCommClient.Connect(const APort: String): TisGUID;
begin
  Result := CreateConnection('localhost', APort, '', '', '', '');
end;

function TevRWCommClient.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevRWDatagramDispatcher.Create;
  Result.ThreadPoolCapacity := 2;
end;

{ TrwProxy }

procedure TrwProxy.Connect;
var
  SessionID: TisGUID;
  sPortFile, sPort: String;
  i: integer;

  function ReadPortNumberFromFile(const AFileName: String): String;
  var
    sPortFileStream: IevDualStream;
    sErrorMessage: String;
    i: Integer;
  begin
    // Retry cycle upto 30 sec
    sErrorMessage := '';
    for i := 1 to 30 do
      try
        sPortFileStream := TevDualStreamHolder.CreateFromFile(AFileName);
        break;
      except
        on E: Exception do
        begin
          sErrorMessage := E.Message;
          sPortFileStream := nil;
          Snooze(1000);
        end;
      end;

    if not Assigned(sPortFileStream) then
      raise EisException.CreateFmt('Cannot open communication file "%s". Error:  "%s"', [AFileName, sErrorMessage]);
      
    Result := sPortFileStream.ReadLn;
  end;

begin
  try
    sPortFile := AppDir + 'rwPort';  // debugging reason
    if not FileExists(sPortFile) then
    begin
      sPortFile := AppTempFolder + GetUniqueID + '.tmp';
      RunIsolatedProcess(AppDir + GetExeName, '/server "' + sPortFile + '" /owner ' + AppMutexName, []);
    end;

    // Wait for file creation upto 30 sec
    for i := 1 to 300 do
    begin
      if FileExists(sPortFile) then
        break;
      Snooze(100);
    end;

    if not FileExists(sPortFile) then
      raise EisException.CreateFmt('Communication file "%s" is missing', [sPortFile]);

    try
      sPort := ReadPortNumberFromFile(sPortFile);
    finally
      DeleteFile(sPortFile);
    end;

    SessionID := RWConnectionPool.CommClient.Connect(sPort);
    FSession := RWConnectionPool.CommClient.GetDatagramDispatcher.FindSessionByID(SessionID);
  except
    on E: Exception do
    begin
      E.Message := 'Cannot initialize Report Writer' + #13 + E.Message;
      raise;
    end;
  end;
end;

function TrwProxy.CreateDatagram(const AMethod: String): IevDatagram;
begin
  Result := TevDatagram.Create(AMethod);
  Result.Header.SessionID := FSession.SessionID;
end;

destructor TrwProxy.Destroy;
begin
  if Assigned(FRWConnectionPool) then
    RWConnectionPool.CommClient.GetDatagramDispatcher.DeleteSession(FSession);
  inherited;
end;

procedure TrwProxy.DoOnConstruction;
begin
  inherited;
  Connect;
end;


function TrwProxy.IsAlive: Boolean;
begin
  Result := FSession.State = ssActive;
end;

function TrwProxy.SendRequest(const ADatagram: IevDatagram): IevDatagram;
begin
  Result := FSession.SendRequest(ADatagram);
end;

{ TrwPreviewAppProxy }

function TrwPreviewAppProxy.GetExeName: String;
begin
  Result := 'isRWPreview.exe';
end;

procedure TrwPreviewAppProxy.AddToGroup(const AReportResult: IevDualStream;
  const AReportResultInfo: TrwReportResultInfo;
  const APrintJobInfo: TrwPrintJobInfo);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWPREVIEW_ADDTOGROUP);
  D.Params.AddValue('AReportResult', AReportResult);

  D.Params.AddValue('AReportResultInfo.Name', AReportResultInfo.Name);
  D.Params.AddValue('AReportResultInfo.FormatType', AReportResultInfo.FormatType);
  D.Params.AddValue('AReportResultInfo.SecuredMode', AReportResultInfo.SecuredMode);
  D.Params.AddValue('AReportResultInfo.PreviewPwd', AReportResultInfo.PreviewPwd);
  D.Params.AddValue('AReportResultInfo.PrintPwd', AReportResultInfo.PrintPwd);

  D.Params.AddValue('APrintJobInfo.PrintJobName', APrintJobInfo.PrintJobName);
  D.Params.AddValue('APrintJobInfo.PrinterName', APrintJobInfo.PrinterName);
  D.Params.AddValue('APrintJobInfo.TrayName', APrintJobInfo.TrayName);
  D.Params.AddValue('APrintJobInfo.OutBinNbr', APrintJobInfo.OutBinNbr);
  D.Params.AddValue('APrintJobInfo.DuplexingMode', APrintJobInfo.DuplexingMode);
  D.Params.AddValue('APrintJobInfo.PrinterVerticalOffset', APrintJobInfo.PrinterVerticalOffset);
  D.Params.AddValue('APrintJobInfo.PrinterHorizontalOffset', APrintJobInfo.PrinterHorizontalOffset);
  D.Params.AddValue('APrintJobInfo.FirstPage', APrintJobInfo.FirstPage);
  D.Params.AddValue('APrintJobInfo.LastPage', APrintJobInfo.LastPage);
  D.Params.AddValue('APrintJobInfo.Layers', APrintJobInfo.Layers);
  D.Params.AddValue('APrintJobInfo.Copies', APrintJobInfo.Copies);

  D := SendRequest(D);
end;

procedure TrwPreviewAppProxy.ClearGroup;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWPREVIEW_CLEARGROUP);
  D := SendRequest(D);
end;

function TrwPreviewAppProxy.GetFormBounds: TrwBounds;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWPREVIEW_GETFORMBOUNDS);
  D := SendRequest(D);
  Result.Left := D.Params.Value[METHOD_RESULT + '.Left'];
  Result.Top := D.Params.Value[METHOD_RESULT + '.Top'];
  Result.Width := D.Params.Value[METHOD_RESULT + '.Width'];
  Result.Height := D.Params.Value[METHOD_RESULT + '.Height'];
end;

function TrwPreviewAppProxy.MergeGroup: IevDualStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWPREVIEW_MERGEGROUP);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDualStream;
end;

procedure TrwPreviewAppProxy.Preview(const AReportResult: IevDualStream; const AParentWindow: Integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWPREVIEW_PREVIEW);
  D.Params.AddValue('AReportResult', AReportResult);
  D.Params.AddValue('AParentWindow', AParentWindow);
  D := SendRequest(D);
end;

procedure TrwPreviewAppProxy.PreviewGroup(const AParentWindow: Integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWPREVIEW_PREVIEWGROUP);
  D.Params.AddValue('AParentWindow', AParentWindow);
  D := SendRequest(D);
end;

procedure TrwPreviewAppProxy.Print(const AReportResult: IevDualStream;
  const APrintJobInfo: TrwPrintJobInfo);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWPREVIEW_PRINT);
  D.Params.AddValue('AReportResult', AReportResult);
  D.Params.AddValue('APrintJobInfo.PrintJobName', APrintJobInfo.PrintJobName);
  D.Params.AddValue('APrintJobInfo.PrinterName', APrintJobInfo.PrinterName);
  D.Params.AddValue('APrintJobInfo.TrayName', APrintJobInfo.TrayName);
  D.Params.AddValue('APrintJobInfo.OutBinNbr', APrintJobInfo.OutBinNbr);
  D.Params.AddValue('APrintJobInfo.DuplexingMode', APrintJobInfo.DuplexingMode);
  D.Params.AddValue('APrintJobInfo.PrinterVerticalOffset', APrintJobInfo.PrinterVerticalOffset);
  D.Params.AddValue('APrintJobInfo.PrinterHorizontalOffset', APrintJobInfo.PrinterHorizontalOffset);
  D.Params.AddValue('APrintJobInfo.FirstPage', APrintJobInfo.FirstPage);
  D.Params.AddValue('APrintJobInfo.LastPage', APrintJobInfo.LastPage);
  D.Params.AddValue('APrintJobInfo.Layers', APrintJobInfo.Layers);
  D.Params.AddValue('APrintJobInfo.Copies', APrintJobInfo.Copies);

  D := SendRequest(D);
end;

procedure TrwPreviewAppProxy.PrintGroup(const APrintJobInfo: TrwPrintJobInfo);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWPREVIEW_PRINTGROUP);
  D.Params.AddValue('APrintJobInfo.PrintJobName', APrintJobInfo.PrintJobName);
  D.Params.AddValue('APrintJobInfo.PrinterName', APrintJobInfo.PrinterName);
  D.Params.AddValue('APrintJobInfo.TrayName', APrintJobInfo.TrayName);
  D.Params.AddValue('APrintJobInfo.OutBinNbr', APrintJobInfo.OutBinNbr);
  D.Params.AddValue('APrintJobInfo.DuplexingMode', APrintJobInfo.DuplexingMode);
  D.Params.AddValue('APrintJobInfo.PrinterVerticalOffset', APrintJobInfo.PrinterVerticalOffset);
  D.Params.AddValue('APrintJobInfo.PrinterHorizontalOffset', APrintJobInfo.PrinterHorizontalOffset);
  D.Params.AddValue('APrintJobInfo.FirstPage', APrintJobInfo.FirstPage);
  D.Params.AddValue('APrintJobInfo.LastPage', APrintJobInfo.LastPage);
  D.Params.AddValue('APrintJobInfo.Layers', APrintJobInfo.Layers);
  D.Params.AddValue('APrintJobInfo.Copies', APrintJobInfo.Copies);

  D := SendRequest(D);
end;

function TrwPreviewAppProxy.QueryInfo(const AItemName: String): String;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWPREVIEW_QUERYINFO);
  D.Params.AddValue('AItemName', AItemName);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwPreviewAppProxy.ReportsInGroup: Integer;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWPREVIEW_REPORTSINGROUP);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TrwPreviewAppProxy.SetFormBounds(const ABounds: TrwBounds);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWPREVIEW_SETFORMBOUNDS);
  D.Params.AddValue('ABounds.Left', ABounds.Left);
  D.Params.AddValue('ABounds.Top', ABounds.Top);
  D.Params.AddValue('ABounds.Width', ABounds.Width);
  D.Params.AddValue('ABounds.Height', ABounds.Height);

  D := SendRequest(D);
end;


{ TrwEngineAppProxy }

function TrwEngineAppProxy.CallAppAdapterFunction(
  const AFunctionName: String; const AParams: Variant): Variant;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_CallAppAdapterFunction);
  D.Params.AddValue('AFunctionName', AFunctionName);
  D.Params.AddValue('AParams', AParams);

  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwEngineAppProxy.ChangeRWAInfo(const ARWAData: IevDualStream;
  const ACurrentPassword: String; const ANewInfo: TrwRWAFileInfo):IevDualStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_ChangeRWAInfo);
  D.Params.AddValue('ARWAData', ARWAData);
  D.Params.AddValue('ACurrentPassword', ACurrentPassword);
  D.Params.AddValue('ANewInfo.PreviewPassword', ANewInfo.PreviewPassword);
  D.Params.AddValue('ANewInfo.PrintPassword', ANewInfo.PrintPassword);
  D.Params.AddValue('ANewInfo.Compression', ANewInfo.Compression);

  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDualStream;
end;

function TrwEngineAppProxy.ClassInheritsFrom(const AClassName,
  AAncestorClassName: String): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_ClassInheritsFrom);
  D.Params.AddValue('AClassName', AClassName);
  D.Params.AddValue('AAncestorClassName', AAncestorClassName);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TrwEngineAppProxy.CloseActiveReport;
var
  D: IevDatagram;
begin
  FReportStructure := nil;
  D := CreateDatagram(METHOD_RWENGINE_CloseActiveReport);
  D := SendRequest(D);
end;

procedure TrwEngineAppProxy.DoOnConstruction;
begin
  inherited;
  FProcessController := TrwProcessController.Create(Self); 
end;

function TrwEngineAppProxy.GetActiveReport: IrwReportStructure;
begin
  Result := FReportStructure;
end;

function TrwEngineAppProxy.GetClassInfo(const AClassName: String): TrwClassInfo;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_GetClassInfo);
  D.Params.AddValue('AClassName', AClassName);

  D := SendRequest(D);
  Result.ClassName := D.Params.Value[METHOD_RESULT + '.ClassName'];  
  Result.AncestorClassName := D.Params.Value[METHOD_RESULT + '.AncestorClassName'];
  Result.DisplayName := D.Params.Value[METHOD_RESULT + '.DisplayName']
end;

function TrwEngineAppProxy.GetExeName: String;
begin
  Result := 'isRWEngine.exe';
end;

function TrwEngineAppProxy.GetMainWindowHandle: Integer;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_GetMainWindowHandle);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwEngineAppProxy.GetProcessController: IrwProcessController;
begin
  Result := FProcessController;
end;

procedure TrwEngineAppProxy.InitializeAppAdapter(const AModuleName: String; const AAppCustomData: Variant);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_InitializeAppAdapter);
  D.Params.AddValue('AModuleName', AModuleName);
  D.Params.AddValue('AAppCustomData', AAppCustomData);
  D := SendRequest(D);
end;

function TrwEngineAppProxy.IsQueryBuilderFormClosed(out AQueryData: IevDualStream): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_IsQueryBuilderFormClosed);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
  AQueryData := IInterface(D.Params.Value['AQueryData']) as IevDualStream;
end;

function TrwEngineAppProxy.OpenQueryBuilder(const AQueryData: IevDualStream;
  const ADescriptionView: Boolean): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_OpenQueryBuilder);
  D.Params.AddValue('AQueryData', AQueryData);
  D.Params.AddValue('ADescriptionView', ADescriptionView);

  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwEngineAppProxy.OpenQueryBuilder2(const AQueryData: IevDualStream;
  const ADescriptionView: Boolean): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_OpenQueryBuilder2);
  D.Params.AddValue('AQueryData', AQueryData);
  D.Params.AddValue('ADescriptionView', ADescriptionView);

  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TrwEngineAppProxy.OpenReportFromClass(const AClassName: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_OpenReportFromClass);
  D.Params.AddValue('AClassName', AClassName);
  D := SendRequest(D);

  FReportStructure := TrwReportStructure.Create(Self);
end;

procedure TrwEngineAppProxy.OpenReportFromStream(const AStream: IevDualStream);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_OpenReportFromStream);
  D.Params.AddValue('AStream', AStream);
  D := SendRequest(D);

  FReportStructure := TrwReportStructure.Create(Self);
end;

function TrwEngineAppProxy.QueryInfo(const AItemName: String): String;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_QueryInfo);
  D.Params.AddValue('AItemName', AItemName);

  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwEngineAppProxy.RunActiveReport(const ARunSettings: TrwRunReportSettings): TrwReportResult;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_RunActiveReport);
  D.Params.AddValue('ARunSettings.ReturnParamsBack', ARunSettings.ReturnParamsBack);
  D.Params.AddValue('ARunSettings.Duplexing', ARunSettings.Duplexing);
  D.Params.AddValue('ARunSettings.ShowInputForm', ARunSettings.ShowInputForm);
  D.Params.AddValue('ARunSettings.Suspended', ARunSettings.Suspended);
  D.Params.AddValue('ARunSettings.ReportName', ARunSettings.ReportName);

  D := SendRequest(D);
  Result.Data := IInterface(D.Params.Value[METHOD_RESULT + '.Data']) as IevDualStream;
  Result.FormatType := TrwReportResultFormatType(D.Params.Value[METHOD_RESULT + '.FormatType']);
  Result.PageInfo := D.Params.Value[METHOD_RESULT + '.PageInfo'];
  Result.MiscInfo := D.Params.Value[METHOD_RESULT + '.MiscInfo'];
end;

function TrwEngineAppProxy.RunQuery(const AQueryData: IevDualStream;
  const AParams: Variant): Variant;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_RunQuery);
  D.Params.AddValue('AQueryData', AQueryData);
  D.Params.AddValue('AParams', AParams);

  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwEngineAppProxy.RunReport(const AReportData: IevDualStream;
  const AReportName: String; out AReportResult: IevDualStream): Variant;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_RunReport);
  D.Params.AddValue('AReportData', AReportData);
  D.Params.AddValue('AReportName', AReportName);
  D := SendRequest(D);

  Result := D.Params.Value[METHOD_RESULT];
  AReportResult := IInterface(D.Params.Value['AReportResult']) as IevDualStream;
end;

function TrwEngineAppProxy.RWAtoPDF(const ARWAData: IevDualStream;
  const APDFInfo: TrwPDFFileInfo): IevDualStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_RWAtoPDF);
  D.Params.AddValue('ARWAData', ARWAData);
  D.Params.AddValue('APDFInfo.Author', APDFInfo.Author);
  D.Params.AddValue('APDFInfo.Creator', APDFInfo.Creator);
  D.Params.AddValue('APDFInfo.Subject', APDFInfo.Subject);
  D.Params.AddValue('APDFInfo.Title', APDFInfo.Title);
  D.Params.AddValue('APDFInfo.Compression', APDFInfo.Compression);
  D.Params.AddValue('APDFInfo.ProtectionOptions', APDFInfo.ProtectionOptions);
  D.Params.AddValue('APDFInfo.OwnerPassword', APDFInfo.OwnerPassword);
  D.Params.AddValue('APDFInfo.UserPassword', APDFInfo.UserPassword);

  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDualStream;
end;

procedure TrwEngineAppProxy.SetExternalModuleData(
  const AModuleName: String; const AData: IevDualStream);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWENGINE_SetExternalModuleData);
  D.Params.AddValue('AModuleName', AModuleName);
  D.Params.AddValue('AData', AData);
  D := SendRequest(D);
end;

{ TrwReportStructure }

procedure TrwReportStructure.CloseCancel;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RI_CloseCancel);
  D := FOwner.SendRequest(D);
end;

procedure TrwReportStructure.CloseOK;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RI_CloseOK);
  D := FOwner.SendRequest(D);
end;

procedure TrwReportStructure.Compile;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RS_Compile);
  D := FOwner.SendRequest(D);
end;

constructor TrwReportStructure.Create(const AOwner: TrwEngineAppProxy);
begin
  inherited Create;
  FOwner := AOwner;
end;

function TrwReportStructure.GetCount: Integer;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RP_GetCount);
  D := FOwner.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwReportStructure.GetData: IevDualStream;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RS_GetData);
  D := FOwner.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDualStream;
end;

function TrwReportStructure.GetFormBounds: TrwBounds;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RI_GetFormBounds);
  D := FOwner.SendRequest(D);

  Result.Left := D.Params.Value[METHOD_RESULT + '.Left'];
  Result.Top := D.Params.Value[METHOD_RESULT + '.Top'];
  Result.Width := D.Params.Value[METHOD_RESULT + '.Width'];
  Result.Height := D.Params.Value[METHOD_RESULT + '.Height'];
end;

function TrwReportStructure.GetItems(const AIndex: Integer): TrwReportParam;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RP_GetItems);
  D.Params.AddValue('AIndex', AIndex);

  D := FOwner.SendRequest(D);
  Result.Name := D.Params.Value[METHOD_RESULT + '.Name'];
  Result.Value := D.Params.Value[METHOD_RESULT + '.Value'];
  Result.RunTimeParam := D.Params.Value[METHOD_RESULT + '.RunTimeParam'];
  Result.ParamType := TrwReportParamType(D.Params.Value[METHOD_RESULT + '.ParamType']);
end;

function TrwReportStructure.GetParamValue(const AParamName: String): Variant;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RP_GetParamValue);
  D.Params.AddValue('AParamName', AParamName);
  D := FOwner.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwReportStructure.GetReportInputForm: IrwReportInputForm;
begin
  Result := Self;
end;

function TrwReportStructure.GetReportParameters: IrwReportParameters;
begin
  Result := Self;
end;

function TrwReportStructure.GetStructureInfo: TrwReportStructureInfo;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RS_GetStructureInfo);
  D := FOwner.SendRequest(D);

  Result.ReportClassName  := D.Params.Value[METHOD_RESULT + '.ReportClassName'];
  Result.AncestorClassName  := D.Params.Value[METHOD_RESULT + '.AncestorClassName'];
  Result.UsedLibComponents  := D.Params.Value[METHOD_RESULT + '.UsedLibComponents'];
  Result.Description  := D.Params.Value[METHOD_RESULT + '.Description'];
end;

function TrwReportStructure.IndexOf(const AParamName: String): Integer;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RP_IndexOf);
  D.Params.AddValue('AParamName', AParamName);
  D := FOwner.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwReportStructure.ParamByName(const AParamName: String): TrwReportParam;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RP_ParamByName);
  D.Params.AddValue('AParamName', AParamName);

  D := FOwner.SendRequest(D);
  Result.Name := D.Params.Value[METHOD_RESULT + '.Name'];
  Result.Value := D.Params.Value[METHOD_RESULT + '.Value'];
  Result.RunTimeParam := D.Params.Value[METHOD_RESULT + '.RunTimeParam'];
  Result.ParamType := TrwReportParamType(D.Params.Value[METHOD_RESULT + '.ParamType']);
end;

procedure TrwReportStructure.ParamsFromStream(const AParams: IevDualStream);
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RP_ParamsFromStream);
  D.Params.AddValue('AParams', AParams);
  D := FOwner.SendRequest(D);
end;

function TrwReportStructure.ParamsToStream: IevDualStream;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RP_ParamsToStream);
  D := FOwner.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDualStream;  
end;

procedure TrwReportStructure.PrepareForRunning;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RS_PrepareForRunning);
  D := FOwner.SendRequest(D);
end;

procedure TrwReportStructure.SetFormBounds(const ABounds: TrwBounds);
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RI_SetFormBounds);
  D.Params.AddValue('ABounds.Left', ABounds.Left);
  D.Params.AddValue('ABounds.Top', ABounds.Top);
  D.Params.AddValue('ABounds.Width', ABounds.Width);
  D.Params.AddValue('ABounds.Height', ABounds.Height);
  D := FOwner.SendRequest(D);
end;

procedure TrwReportStructure.SetParamValue(const AParamName: String; const AValue: Variant);
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RP_SetParamValue);
  D.Params.AddValue('AParamName', AParamName);
  D.Params.AddValue('AValue', AValue);
  D := FOwner.SendRequest(D);
end;

function TrwReportStructure.Show(const AParentWndHandle: Integer; const ASetupMode: Boolean): Boolean;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_RI_Show);
  D.Params.AddValue('AParentWndHandle', AParentWndHandle);
  D.Params.AddValue('ASetupMode', ASetupMode);
  D := FOwner.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;


{ TrwProcessController }

function TrwProcessController.CalcExpression(const AExpression,
  ADebugSymbolInfo: String): TrwVMExpressionResult;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_PC_CalcExpression);
  D.Params.AddValue('AExpression', AExpression);
  D.Params.AddValue('ADebugSymbolInfo', ADebugSymbolInfo);
  D := FOwner.SendRequest(D);
  Result.ResultType := D.Params.Value[METHOD_RESULT + '.ResultType'];
  Result.Result := D.Params.Value[METHOD_RESULT + '.Result'];
  Result.ResultTypeText := D.Params.Value[METHOD_RESULT + '.ResultTypeText'];
  Result.ResultText := D.Params.Value[METHOD_RESULT + '.ResultText'];
end;

constructor TrwProcessController.Create(const AOwner: TrwEngineAppProxy);
begin
  inherited Create;
  FOwner := AOwner;
end;

function TrwProcessController.GetState: TrwProcessState;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_PC_GetState);
  D := FOwner.SendRequest(D);
  Result.VMState := D.Params.Value[METHOD_RESULT + '.VMState'];
  Result.ExceptionText := D.Params.Value[METHOD_RESULT + '.ExceptionText'];
end;

function TrwProcessController.GetVMCallStack: Variant;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_PC_GetVMCallStack);
  D := FOwner.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwProcessController.GetVMRegisters: TrwVMRegisters;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_PC_GetVMRegister);
  D := FOwner.SendRequest(D);
  Result.RegA := D.Params.Value[METHOD_RESULT + '.RegA'];
  Result.RegB := D.Params.Value[METHOD_RESULT + '.RegB'];
  Result.RegF := D.Params.Value[METHOD_RESULT + '.RegF'];
  Result.CSIP := D.Params.Value[METHOD_RESULT + '.CSIP'];
end;

procedure TrwProcessController.OpenQueryBuilder(const AQueryData: IevDualStream;
  const ADescriptionView: Boolean);
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_PC_OpenQueryBuilder);
  D.Params.AddValue('AQueryData', AQueryData);
  D.Params.AddValue('ADescriptionView', ADescriptionView);
  D := FOwner.SendRequest(D);
end;

procedure TrwProcessController.Pause;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_PC_Pause);
  D := FOwner.SendRequest(D);
end;

procedure TrwProcessController.Run(const ARunAction: TrwRunAction);
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_PC_Run);
  D.Params.AddValue('ARunAction', ARunAction);
  D := FOwner.SendRequest(D);
end;

procedure TrwProcessController.SetBreakpoints(const ABreakpoints: Variant);
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_PC_SetBreakpoints);
  D.Params.AddValue('ABreakpoints', ABreakpoints);
  D := FOwner.SendRequest(D);
end;

procedure TrwProcessController.Stop;
var
  D: IevDatagram;
begin
  D := FOwner.CreateDatagram(METHOD_RWENGINE_PC_Stop);
  D := FOwner.SendRequest(D);
end;

{ TrwDesignerAppProxy }

function TrwDesignerAppProxy.CallAppAdapterFunction(
  const AFunctionName: String; const AParams: Variant): Variant;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWDESIGNER_CallAppAdapterFunction);
  D.Params.AddValue('AFunctionName', AFunctionName);
  D.Params.AddValue('AParams', AParams);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwDesignerAppProxy.GetExeName: String;
begin
  Result := 'isRWDesigner.exe';
end;

procedure TrwDesignerAppProxy.InitializeAppAdapter(const AModuleName: String;
  const AAppCustomData: Variant);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWDESIGNER_InitializeAppAdapter);
  D.Params.AddValue('AModuleName', AModuleName);
  D.Params.AddValue('AAppCustomData', AAppCustomData);
  D := SendRequest(D);
end;

function TrwDesignerAppProxy.IsDesignerFormClosed(
  out AReportData: IevDualStream): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWDESIGNER_IsDesignerFormClosed);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
  AReportData := IInterface(D.Params.Value['AReportData']) as IevDualStream;
end;

function TrwDesignerAppProxy.IsQueryBuilderFormClosed(
  out AQueryData: IevDualStream): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWDESIGNER_IsQueryBuilderFormClosed);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
  AQueryData := IInterface(D.Params.Value['AQueryData']) as IevDualStream;
end;

function TrwDesignerAppProxy.OpenDesigner(const AReportData: IevDualStream;
  const AReportName: String; const ADefaultReportType: Integer): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWDESIGNER_OpenDesigner);
  D.Params.AddValue('AReportData', AReportData);
  D.Params.AddValue('AReportName', AReportName);
  D.Params.AddValue('ADefaultReportType', ADefaultReportType);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwDesignerAppProxy.OpenDesigner2(
  const AReportData: IevDualStream; const AReportName: String;
  const ADefaultReportType: Integer): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWDESIGNER_OpenDesigner2);
  D.Params.AddValue('AReportData', AReportData);
  D.Params.AddValue('AReportName', AReportName);
  D.Params.AddValue('ADefaultReportType', ADefaultReportType);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwDesignerAppProxy.OpenQueryBuilder(
  const AQueryData: IevDualStream;
  const ADescriptionView: Boolean): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWDESIGNER_OpenQueryBuilder);
  D.Params.AddValue('AQueryData', AQueryData);
  D.Params.AddValue('ADescriptionView', ADescriptionView);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwDesignerAppProxy.OpenQueryBuilder2(
  const AQueryData: IevDualStream;
  const ADescriptionView: Boolean): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWDESIGNER_OpenDesigner2);
  D.Params.AddValue('AQueryData', AQueryData);
  D.Params.AddValue('ADescriptionView', ADescriptionView);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwDesignerAppProxy.QueryInfo(const AItemName: String): Variant;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWDESIGNER_QueryInfo);
  D.Params.AddValue('AItemName', AItemName);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TrwDesignerAppProxy.RunQuery(const AQueryData: IevDualStream;
  const AParams: Variant): Variant;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWDESIGNER_RunQuery);
  D.Params.AddValue('AQueryData', AQueryData);
  D.Params.AddValue('AParams', AParams);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

{ TevRWDatagramDispatcher }

procedure TevRWDatagramDispatcher.DoOnIdle;
var
  Msg: TMsg;
begin
  inherited;
  if CurrentThreadIsMain then
  begin
    try
      while PeekMessage(Msg, 0, WM_PAINT, WM_PAINT, PM_REMOVE) do
        DispatchMessage(Msg);
    except
    end;
  end;
end;

initialization
  InitLock := TisLock.Create;
  AppMutexName := GetUniqueID;
  AppMutex := CreateMutex(nil, True, PAnsiChar(AppMutexName));

finalization
  CloseHandle(AppMutex);

end.
