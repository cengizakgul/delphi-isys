unit rwCustomDataDictionary;

interface

uses Classes, SysUtils, EvStreamUtils, rwEngineTypes;

const cFieldValueRef = '#Ref';

type
  // Abstract Data Dictionary classes

  TDataDictionary = class;

  TInterfacedCollectionItem = class(TCollectionItem, IInterface)
  protected
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    function  QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
  end;

  TInterfacedCollection = class(TCollection, IInterface)
  protected
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    function  QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
  end;


  TCustomDataDictItem = class(TInterfacedCollectionItem)
  public
    function  DataDictionaryObject: TDataDictionary;
    procedure Assign(Source: TPersistent); override;
  end;


  TDataDictNamedItem = class(TCustomDataDictItem)
  private
    FNotes: String;
    FDisplayName: String;
    FName: String;
    function  NotesNotEmpty: Boolean;
    function  DisplayNameNotEmpty: Boolean;
    procedure SetName(const AValue: String);

  protected
    function  GetDisplayName: string; override;
    procedure SetDisplayName(const Value: string); override;

  public
    procedure Assign(Source: TPersistent); override;

  published
    property  Name: String read FName write SetName;
    property  DisplayName stored DisplayNameNotEmpty;
    property  Notes: String read FNotes write FNotes stored NotesNotEmpty;
  end;


  TDataDictNamedCollection = class(TOwnedCollection)
  private
    FIndex: TStringList;
    procedure UpdateIndex(Item: TDataDictNamedItem);
  protected
    procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
  public
    destructor Destroy; override;
    procedure  AfterConstruction; override;
    function   IndexOf(const AItem: TDataDictNamedItem): Integer;
    function   ItemByName(const AName: String): TDataDictNamedItem;
  end;


  TDataDictKeyField = class;
  TDataDictField = class;

  TDataDictKey = class(TOwnedCollection)
  private
    function  GetItem(Index: Integer): TDataDictKeyField;
    procedure SetItem(Index: Integer; const Value: TDataDictKeyField);
  public
    procedure FixupReferences; virtual;
    function  AddKeyField(const AField: TDataDictField): TDataDictKeyField;
    function  FieldByName(const FieldName: String): TDataDictKeyField;
    function  FindField(const AField: TDataDictField): TDataDictKeyField;
    property Items[Index: Integer]: TDataDictKeyField read GetItem write SetItem; default;
  end;


  TDataDictKeyField = class(TCustomDataDictItem)
  private
    procedure ReadField(Reader: TReader);
    procedure WriteField(Writer: TWriter);
  protected
    FReadFieldName: String;
    FField: TDataDictField;
    procedure DefineProperties(Filer: TFiler); override;
  public
    procedure Assign(Source: TPersistent); override;
    procedure FixupReferences; virtual; abstract;
    property  Field: TDataDictField read FField write FField;
  end;




  // Data Dictionary class-wrapper

  TDataDictTables = class;
  TDataDictTable = class;
  TDataDictPrimKeyField = class;
  TDataDictFieldsClass = class of TDataDictFields;
  TDataDictTablesClass = class of TDataDictTables;
  TDataDictParamsClass = class of TDataDictParams;
  TDataDictFieldClass = class of TDataDictField;
  TDataDictTableClass = class of TDataDictTable;
  TDataDictParamClass = class of TDataDictParam;
  TDataDictPrimKeyFieldClass = class of TDataDictPrimKeyField;
  TDataDictForeignKeyFieldClass = class of TDataDictForeignKeyField;
  TDataDictForeignKeyClass = class of TDataDictForeignKey;

  TDataDictionary = class(TComponent)
  private
    FNotes: String;
    FTables: TDataDictTables;
    procedure SetTables(const Value: TDataDictTables);
    function  NotesNotEmpty: Boolean;
    procedure ReadTables(Reader: TReader);
    procedure WriteTables(Writer: TWriter);

  protected
    procedure Loaded; override;
    procedure DefineProperties(Filer: TFiler); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    // Override these if you need to use your own class
    function GetTablesClass: TDataDictTablesClass; virtual;
    function GetFieldsClass: TDataDictFieldsClass; virtual;
    function GetParamsClass:TDataDictParamsClass; virtual;
    function GetTableClass: TDataDictTableClass; virtual;
    function GetFieldClass: TDataDictFieldClass; virtual;
    function GetParamClass: TDataDictParamClass; virtual;
    function GetPrimKeyFieldClass: TDataDictPrimKeyFieldClass; virtual;
    function GetForeignKeyClass: TDataDictForeignKeyClass; virtual;
    function GetForeignKeyFieldClass: TDataDictForeignKeyFieldClass; virtual;

    // Use these procedures for safe deletion
    // It automatically removes broken references
    procedure   DeleteTable(const ATable: TDataDictTable); virtual;
    procedure   DeleteField(const AField: TDataDictField); virtual;
    procedure   DeletePrimaryKeyField(const APrimKeyField: TDataDictPrimKeyField); virtual;

    // Load-Save
    procedure   SaveToFile(const FileName: String);
    procedure   SaveToStream(const Stream: TStream); virtual;
    procedure   LoadFromFile(const FileName: String);
    procedure   LoadFromStream(const Stream: TStream); virtual;

    procedure FixupReferences;
    procedure Assign(Source: TPersistent); override;
    property Tables: TDataDictTables read FTables write SetTables;  // List of Tables

  published
    property Notes: String read FNotes write FNotes stored NotesNotEmpty;  // User notes
  end;



  // List of Tables

  TDataDictTables = class(TDataDictNamedCollection)
  private
    function  GetItem(Index: Integer): TDataDictTable;
    procedure SetItem(Index: Integer; const Value: TDataDictTable);
  public
    property Items[Index: Integer]: TDataDictTable read GetItem write SetItem; default;
    function AddTable: TDataDictTable;
    function TableByName(const ATableName: String): TDataDictTable;
  end;


  // Table
  TDataDictFields = class;
  TDataDictParams = class;
  TDataDictForeignKeys = class;
  TDataDictPrimKey = class;

  TDataDictTable = class(TDataDictNamedItem)
  private
    FFields: TDataDictFields;
    FParams: TDataDictParams;
    FPrimaryKey: TDataDictPrimKey;
    FForeignKeys: TDataDictForeignKeys;
    FLogicalKey: TDataDictPrimKey;
    FDisplayLogicalKey: TDataDictPrimKey;
    function  ParamsNotEmpty: Boolean;
    procedure SetFields(const Value: TDataDictFields);
    procedure SetParams(const Value: TDataDictParams);
    procedure ReadPrimaryKey(Reader: TReader);
    procedure WritePrimaryKey(Writer: TWriter);
    procedure ReadLogicalKey(Reader: TReader);
    procedure WriteLogicalKey(Writer: TWriter);
    procedure ReadDisplayLogicalKey(Reader: TReader);
    procedure WriteDisplayLogicalKey(Writer: TWriter);
    procedure ReadForeignKeys(Reader: TReader);
    procedure WriteForeignKeys(Writer: TWriter);
    procedure SetForeignKeys(const Value: TDataDictForeignKeys);
    procedure SetPrimaryKey(const Value: TDataDictPrimKey);
    procedure SetLogicalKey(const Value: TDataDictPrimKey);
    procedure SetDisplayLogicalKey(const Value: TDataDictPrimKey);

  protected
    procedure DefineProperties(Filer: TFiler); override;

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;
    procedure   FixupReferences; virtual;

    property PrimaryKey: TDataDictPrimKey read FPrimaryKey write SetPrimaryKey;
    property LogicalKey: TDataDictPrimKey read FLogicalKey write SetLogicalKey;
    property ForeignKeys: TDataDictForeignKeys read FForeignKeys write SetForeignKeys;
    property DisplayLogicalKey: TDataDictPrimKey read FDisplayLogicalKey write SetDisplayLogicalKey;

  published
    property Fields: TDataDictFields read FFields write SetFields;
    property Params: TDataDictParams read FParams write SetParams stored ParamsNotEmpty;
  end;


  // List of Fields
  TDataDictFields = class(TDataDictNamedCollection)
  private
    function  GetItem(Index: Integer): TDataDictField;
    procedure SetItem(Index: Integer; const Value: TDataDictField);
  public
    property Items[Index: Integer]: TDataDictField read GetItem write SetItem; default;
    function FieldByName(const AFieldName: String): TDataDictField;
    function AddField: TDataDictField;
  end;


  // Field
  TDataDictDataType = TrwDDType;

  TDataDictField = class(TDataDictNamedItem)
  private
    FFieldType: TDataDictDataType;
    FSize: Integer;
    FFieldValues: TStrings;
    function FieldValuesNotEmpty: Boolean;
    procedure SetFieldValues(const Value: TStrings);
  protected
    procedure SetFieldType(const Value: TDataDictDataType); virtual;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function  Table: TDataDictTable;
    function  FieldValuesReal: TIniString;
  published
    property FieldType: TDataDictDataType read FFieldType write SetFieldType;
    property Size: Integer read FSize write FSize default 0;
    property FieldValues: TStrings read FFieldValues write SetFieldValues stored FieldValuesNotEmpty;
  end;


  // List of table Params
  TDataDictParam = class;

  TDataDictParams = class(TDataDictNamedCollection)
  private
    function  GetItem(Index: Integer): TDataDictParam;
    procedure SetItem(Index: Integer; const Value: TDataDictParam);
  public
    function AddParam: TDataDictParam;
    property Items[Index: Integer]: TDataDictParam read GetItem write SetItem; default;
    function ParamByName(const AParamName: String): TDataDictParam;
  end;


  // Table Param
  TDataDictParam = class(TDataDictNamedItem)
  private
    FParamType: TDataDictDataType;
    FParamValues: TStrings;
    function ParamValuesNotEmpty: Boolean;
    procedure SetParamValues(const Value: TStrings);

  protected
    procedure SetParamType(const Value: TDataDictDataType); virtual;

  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function  ParamValuesReal: TIniString;

  published
    property ParamType: TDataDictDataType read FParamType write SetParamType;
    property ParamValues: TStrings read FParamValues write SetParamValues stored ParamValuesNotEmpty;
  end;


  // Primary Key

  TDataDictPrimKey = class(TDataDictKey)
  private
    function  GetItem(Index: Integer): TDataDictPrimKeyField;
    procedure SetItem(Index: Integer; const Value: TDataDictPrimKeyField);
  public
    procedure FixupReferences; override;
    property Items[Index: Integer]: TDataDictPrimKeyField read GetItem write SetItem; default;
  end;


  TDataDictPrimKeyField = class(TDataDictKeyField)
  public
    procedure FixupReferences; override;
  end;


  // Foreign Key
  TDataDictForeignKey = class;

  TDataDictForeignKeys = class(TOwnedCollection)
  private
    function  GetItem(Index: Integer): TDataDictForeignKey;
    procedure SetItem(Index: Integer; const Value: TDataDictForeignKey);
  public
    procedure FixupReferences; virtual;
    function  AddForeignKey(const ATable: TDataDictTable): TDataDictForeignKey;
    function  ForeignKeyByTable(const ATable: TDataDictTable; const StartSearchFrom: Integer = 0): TDataDictForeignKey;
    function  ForeignKeyByField(const AField: TDataDictField; const StartSearchFrom: Integer = 0): TDataDictForeignKey;
    property  Items[Index: Integer]: TDataDictForeignKey read GetItem write SetItem; default;
  end;


  TDataDictForeignKeyField = class;

  TDataDictForeignKeyFields = class(TDataDictKey)
  private
    function  GetItem(Index: Integer): TDataDictForeignKeyField;
    procedure SetItem(Index: Integer; const Value: TDataDictForeignKeyField);
  public
    procedure FixupReferences; override;
    property  Items[Index: Integer]: TDataDictForeignKeyField read GetItem write SetItem; default;
  end;


  TDataDictForeignKeyField = class(TDataDictKeyField)
  public
    procedure FixupReferences; override;
  end;


  TDataDictForeignKey = class(TCustomDataDictItem)
  private
    FFields:  TDataDictForeignKeyFields;
    procedure ReadTable(Reader: TReader);
    procedure WriteTable(Writer: TWriter);
    procedure SetFields(const Value: TDataDictForeignKeyFields);

  protected
    FReadTableName: String;
    FTable: TDataDictTable;
    procedure DefineProperties(Filer: TFiler); override;

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;
    procedure   FixupReferences; virtual;

    property  Table: TDataDictTable read FTable write FTable;

  published
    property Fields: TDataDictForeignKeyFields read FFields write SetFields;
  end;


implementation


function GetAllowedValues(AFieldValues: TStrings; ADataDictionary: TDataDictionary): TIniString;
var
  i, j: Integer;
  Val, Desc, h: String;

  function DecodeReference(const AReference: String): TIniString;
  var
    i: Integer;
    Tbl, Fld: String;
    oTbl: TDataDictTable;
    oFld: TDataDictField;
  begin
    Result := '';

    i := Pos('.', AReference);
    if i = 0 then
      Exit;

    Tbl := Copy(AReference, 1, i - 1);
    Fld := Copy(AReference, i + 1, Length(AReference) - i);

    oTbl := ADataDictionary.Tables.TableByName(Tbl);
    if not Assigned(oTbl) then
      Exit;

    oFld := oTbl.Fields.FieldByName(Fld);
    if not Assigned(oFld) then
      Exit;

    Result := oFld.FieldValuesReal;
  end;

begin
  Result := '';

  for i := 0 to AFieldValues.Count - 1 do
  begin
    Desc := AFieldValues[i];
    j := Pos('=', Desc);
    Val := Copy(Desc, 1, j - 1);
    Delete(Desc, 1, j);
    if SameText(Copy(Val, 1, 4), cFieldValueRef) then
      h := DecodeReference(Desc)
    else
      h := AFieldValues[i];

    if h <> '' then
    begin
      if i > 0 then
        Result := Result + #13;
      Result := Result + h;
    end;
  end;
end;


{ TDataDictionary }

procedure TDataDictionary.Assign(Source: TPersistent);
begin
  Tables := TDataDictionary(Source).Tables;
  Notes := TDataDictionary(Source).Notes;
end;

constructor TDataDictionary.Create(AOwner: TComponent);
begin
  inherited;
  FTables := GetTablesClass.Create(Self, GetTableClass);
end;

procedure TDataDictionary.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('Tables', ReadTables, WriteTables, True);
end;

destructor TDataDictionary.Destroy;
begin
  FreeAndNil(FTables);
  inherited;
end;

function TDataDictionary.GetFieldClass: TDataDictFieldClass;
begin
  Result := TDataDictField;
end;

function TDataDictionary.GetForeignKeyFieldClass: TDataDictForeignKeyFieldClass;
begin
  Result := TDataDictForeignKeyField;
end;

function TDataDictionary.GetParamClass: TDataDictParamClass;
begin
  Result := TDataDictParam;
end;

function TDataDictionary.GetPrimKeyFieldClass: TDataDictPrimKeyFieldClass;
begin
  Result := TDataDictPrimKeyField;
end;

function TDataDictionary.GetTableClass: TDataDictTableClass;
begin
  Result := TDataDictTable;
end;

procedure TDataDictionary.Loaded;
begin
  inherited;
  FixupReferences;
end;

function TDataDictionary.NotesNotEmpty: Boolean;
begin
  Result := FNotes <> '';
end;

procedure TDataDictionary.ReadTables(Reader: TReader);
begin
  Reader.ReadValue;
  Reader.ReadCollection(FTables);
end;

procedure TDataDictionary.WriteTables(Writer: TWriter);
begin
  Writer.WriteCollection(FTables);
end;

procedure TDataDictionary.SetTables(const Value: TDataDictTables);
begin
  FTables.Assign(Value);
end;

procedure TDataDictionary.FixupReferences;
var
  i: Integer;
begin
  for i := 0 to Tables.Count - 1 do
    Tables[i].FixupReferences;
end;

procedure TDataDictionary.DeleteTable(const ATable: TDataDictTable);
var
  i: Integer;
  FK: TDataDictForeignKey;
begin
  for i := 0 to Tables.Count - 1 do
    repeat
      FK := Tables[i].ForeignKeys.ForeignKeyByTable(ATable);
      if Assigned(FK) then
        FK.Free;
    until not Assigned(FK);

  ATable.Free;
end;

procedure TDataDictionary.DeleteField(const AField: TDataDictField);
var
  KF: TDataDictPrimKeyField;
  FK: TDataDictForeignKey;
  T: TDataDictTable;
begin
  T := TDataDictTable(TDataDictFields(AField.Collection).Owner);
  KF := TDataDictPrimKeyField(T.PrimaryKey.FindField(AField));
  if Assigned(KF) then
    DeletePrimaryKeyField(KF);

  repeat
    FK := T.ForeignKeys.ForeignKeyByField(AField);
    if Assigned(FK) then
      FK.Free;
  until not Assigned(FK);

  AField.Free;
end;


procedure TDataDictionary.DeletePrimaryKeyField(const APrimKeyField: TDataDictPrimKeyField);
var
  i, j: Integer;
  T: TDataDictTable;
  FK: TDataDictForeignKey;
begin
  T := TDataDictTable(TDataDictPrimKey(APrimKeyField.Collection).Owner);

  for i := 0 to Tables.Count - 1 do
    repeat
      FK := Tables[i].ForeignKeys.ForeignKeyByTable(T);
      if Assigned(FK) then
      begin
        j := APrimKeyField.Index;
        FK.Fields[j].Free;
        if FK.Fields.Count = 0 then
          FK.Free;
      end;
    until not Assigned(FK);

  APrimKeyField.Free;
end;

procedure TDataDictionary.LoadFromFile(const FileName: String);
var
  FS: TEvFileStream;
begin
  FS := TEvFileStream.Create(FileName, fmOpenRead);
  try
    LoadFromStream(FS);
  finally
    FS.Free;
  end;
end;

procedure TDataDictionary.LoadFromStream(const Stream: TStream);
begin
  Stream.ReadComponent(Self);
end;

procedure TDataDictionary.SaveToFile(const FileName: String);
var
  FS: TEvFileStream;
begin
  FS := TEvFileStream.Create(FileName, fmCreate);
  try
    SaveToStream(FS);
  finally
    FS.Free;
  end;
end;

procedure TDataDictionary.SaveToStream(const Stream: TStream);
begin
  Stream.WriteComponent(Self);
end;


function TDataDictionary.GetFieldsClass: TDataDictFieldsClass;
begin
  Result := TDataDictFields;
end;

function TDataDictionary.GetParamsClass: TDataDictParamsClass;
begin
  Result := TDataDictParams;
end;

function TDataDictionary.GetTablesClass: TDataDictTablesClass;
begin
  Result := TDataDictTables;
end;

function TDataDictionary.GetForeignKeyClass: TDataDictForeignKeyClass;
begin
  Result := TDataDictForeignKey;
end;

{ TCustomDataDictItem }

procedure TCustomDataDictItem.Assign(Source: TPersistent);
begin
end;

function TCustomDataDictItem.DataDictionaryObject: TDataDictionary;
var
  C: TPersistent;
begin
  Result := nil;
  C := Collection.Owner;
  while Assigned(C) do
    if C is TDataDictionary then
    begin
      Result := TDataDictionary(C);
      break;
    end
    else if C is TCollectionItem then
      C := TCollectionItem(C).Collection.Owner
    else
      break;
end;


{ TDataDictNamedItem }

procedure TDataDictNamedItem.Assign(Source: TPersistent);
begin
  inherited;
  Name := TDataDictNamedItem(Source).Name;
  FDisplayName := TDataDictNamedItem(Source).DisplayName;
  FNotes := TDataDictNamedItem(Source).Notes;
end;

function TDataDictNamedItem.DisplayNameNotEmpty: Boolean;
begin
  Result := FDisplayName <> '';
end;

function TDataDictNamedItem.GetDisplayName: string;
begin
  Result := FDisplayName;
end;

function TDataDictNamedItem.NotesNotEmpty: Boolean;
begin
  Result := FNotes <> '';
end;

procedure TDataDictNamedItem.SetDisplayName(const Value: string);
begin
  FDisplayName := Value;
end;

procedure TDataDictNamedItem.SetName(const AValue: String);
var
  OldName: String;
begin
  OldName := FName;
  try
    FName := AValue;
    if Assigned(Collection) and (Collection is TDataDictNamedCollection) then
      TDataDictNamedCollection(Collection).UpdateIndex(Self);
  except
    FName := OldName;
    raise;
  end;
end;

{ TDataDictTables }

function TDataDictTables.AddTable: TDataDictTable;
begin
  Result := TDataDictTable(Add);
end;

function TDataDictTables.GetItem(Index: Integer): TDataDictTable;
begin
  Result := TDataDictTable(inherited Items[Index]);
end;

procedure TDataDictTables.SetItem(Index: Integer; const Value: TDataDictTable);
begin
  (inherited Items[Index]).Assign(Value);
end;

function TDataDictTables.TableByName(const ATableName: String): TDataDictTable;
begin
  Result := TDataDictTable(ItemByName(ATableName));
end;



{ TDataDictTable }

constructor TDataDictTable.Create(Collection: TCollection);
begin
  inherited;
  with DataDictionaryObject do
  begin
    FFields := GetFieldsClass.Create(Self, GetFieldClass);
    FParams := GetParamsClass.Create(Self, GetParamClass);
    FPrimaryKey := TDataDictPrimKey.Create(Self, GetPrimKeyFieldClass);
    FLogicalKey := TDataDictPrimKey.Create(Self, GetPrimKeyFieldClass);
    FDisplayLogicalKey := TDataDictPrimKey.Create(Self, GetPrimKeyFieldClass);
    FForeignKeys := TDataDictForeignKeys.Create(Self, GetForeignKeyClass);
  end;
end;

destructor TDataDictTable.Destroy;
begin
  FreeAndNil(FForeignKeys);
  FreeAndNil(FPrimaryKey);
  FreeAndNil(FLogicalKey);
  FreeAndNil(FDisplayLogicalKey);
  FreeAndNil(FParams);
  FreeAndNil(FFields);
  inherited;
end;

procedure TDataDictTable.Assign(Source: TPersistent);
begin
  inherited;
  Fields := TDataDictTable(Source).Fields;
  Params := TDataDictTable(Source).Params;
  PrimaryKey := TDataDictTable(Source).PrimaryKey;
  LogicalKey := TDataDictTable(Source).LogicalKey;
  DisplayLogicalKey := TDataDictTable(Source).DisplayLogicalKey;
  ForeignKeys := TDataDictTable(Source).ForeignKeys;
end;

function TDataDictTable.ParamsNotEmpty: Boolean;
begin
  Result := FParams.Count > 0;
end;

procedure TDataDictTable.SetFields(const Value: TDataDictFields);
begin
  FFields.Assign(Value);
end;

procedure TDataDictTable.SetParams(const Value: TDataDictParams);
begin
  FParams.Assign(Value);
end;

procedure TDataDictTable.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('PrimaryKey', ReadPrimaryKey, WritePrimaryKey, FPrimaryKey.Count > 0);
  Filer.DefineProperty('LogicalKey', ReadLogicalKey, WriteLogicalKey, FLogicalKey.Count > 0);
  Filer.DefineProperty('ForeignKeys', ReadForeignKeys, WriteForeignKeys, FForeignKeys.Count > 0);
  Filer.DefineProperty('DisplayLogicalKey', ReadDisplayLogicalKey, WriteDisplayLogicalKey, FDisplayLogicalKey.Count > 0);
end;

procedure TDataDictTable.ReadPrimaryKey(Reader: TReader);
begin
  Reader.ReadValue;
  Reader.ReadCollection(FPrimaryKey);
end;

procedure TDataDictTable.WritePrimaryKey(Writer: TWriter);
begin
  Writer.WriteCollection(FPrimaryKey);
end;



procedure TDataDictTable.ReadForeignKeys(Reader: TReader);
begin
  Reader.ReadValue;
  Reader.ReadCollection(FForeignKeys);
end;

procedure TDataDictTable.WriteForeignKeys(Writer: TWriter);
begin
  Writer.WriteCollection(FForeignKeys);
end;

procedure TDataDictTable.SetForeignKeys(const Value: TDataDictForeignKeys);
begin
  FForeignKeys.Assign(Value);
end;

procedure TDataDictTable.SetPrimaryKey(const Value: TDataDictPrimKey);
begin
  FPrimaryKey.Assign(Value);
end;

procedure TDataDictTable.FixupReferences;
begin
  PrimaryKey.FixupReferences;
  LogicalKey.FixupReferences;
  DisplayLogicalKey.FixupReferences;
  ForeignKeys.FixupReferences;
end;

procedure TDataDictTable.SetLogicalKey(const Value: TDataDictPrimKey);
begin
  FLogicalKey.Assign(Value);
end;

procedure TDataDictTable.ReadLogicalKey(Reader: TReader);
begin
  Reader.ReadValue;
  Reader.ReadCollection(FLogicalKey);
end;

procedure TDataDictTable.WriteLogicalKey(Writer: TWriter);
begin
  Writer.WriteCollection(FLogicalKey);
end;


procedure TDataDictTable.ReadDisplayLogicalKey(Reader: TReader);
begin
  Reader.ReadValue;
  Reader.ReadCollection(FDisplayLogicalKey);
end;

procedure TDataDictTable.WriteDisplayLogicalKey(Writer: TWriter);
begin
  Writer.WriteCollection(FDisplayLogicalKey);
end;

procedure TDataDictTable.SetDisplayLogicalKey(const Value: TDataDictPrimKey);
begin
  FDisplayLogicalKey.Assign(Value);
end;

{ TDataDictKeyField }

procedure TDataDictKeyField.Assign(Source: TPersistent);
begin
  inherited;
  FReadFieldName := TDataDictKeyField(Source).Field.Name;
  FixupReferences;
end;

procedure TDataDictKeyField.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('Field', ReadField, WriteField, True);
end;

procedure TDataDictKeyField.ReadField(Reader: TReader);
begin
  FReadFieldName := Reader.ReadString;
end;

procedure TDataDictKeyField.WriteField(Writer: TWriter);
begin
  Writer.WriteString(FField.Name);
end;


{ TDataDictPrimKeyField }

procedure TDataDictPrimKeyField.FixupReferences;
begin
  if FReadFieldName <> '' then
  begin
    FField := TDataDictTable(Collection.Owner).Fields.FieldByName(FReadFieldName);
    FReadFieldName := '';
  end;
end;


{ TDataDictFields }

function TDataDictFields.AddField: TDataDictField;
begin
  Result := TDataDictField(Add);
end;

function TDataDictFields.FieldByName(const AFieldName: String): TDataDictField;
begin
  Result := TDataDictField(ItemByName(AFieldName));
end;

function TDataDictFields.GetItem(Index: Integer): TDataDictField;
begin
  Result := TDataDictField(inherited Items[Index]);
end;

procedure TDataDictFields.SetItem(Index: Integer;  const Value: TDataDictField);
begin
  (inherited Items[Index]).Assign(Value);
end;


{ TDataDictNamedCollection }

procedure TDataDictNamedCollection.AfterConstruction;
begin
  inherited;
  FIndex := TStringList.Create;
  FIndex.Sorted := True;
  FIndex.CaseSensitive := False;
  FIndex.Duplicates := dupError;  
end;

destructor TDataDictNamedCollection.Destroy;
begin
  inherited;
  FreeAndNil(FIndex);
end;

function TDataDictNamedCollection.IndexOf(const AItem: TDataDictNamedItem): Integer;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to Count - 1 do
    if Items[i] = AItem then
    begin
      Result := i;
      break;
    end;
end;

function TDataDictNamedCollection.ItemByName(const AName: String): TDataDictNamedItem;
var
  i: Integer;
begin
  i := FIndex.IndexOf(AName);
  if i = -1 then
    Result := nil
  else
    Result := TDataDictNamedItem(FIndex.Objects[i]);
end;



procedure TDataDictNamedCollection.Notify(Item: TCollectionItem; Action: TCollectionNotification);
var
  i: Integer;
begin
  inherited;
  case Action of
    cnAdded:      UpdateIndex(Item as TDataDictNamedItem);

    cnExtracting:
      begin
        i := FIndex.IndexOfObject(Item);
        if i <> -1 then
          FIndex.Delete(i);
      end;
  end;
end;

procedure TDataDictNamedCollection.UpdateIndex(Item: TDataDictNamedItem);
var
  i: Integer;
begin
  inherited;
  i := FIndex.IndexOfObject(Item);
  if i <> -1 then
  begin
    FIndex.Sorted := False;
    try
      FIndex[i] := Item.Name;
    finally
      FIndex.Sorted := True;
    end;
  end
  else
    FIndex.AddObject(Item.Name, Item);
end;

{ TDataDictForeignKey }

constructor TDataDictForeignKey.Create(Collection: TCollection);
begin
  inherited;
  FFields := TDataDictForeignKeyFields.Create(Self, DataDictionaryObject.GetForeignKeyFieldClass);
end;

destructor TDataDictForeignKey.Destroy;
begin
  FreeAndNil(FFields);
  inherited;
end;

procedure TDataDictForeignKey.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('Table', ReadTable, WriteTable, True);
end;

procedure TDataDictForeignKey.FixupReferences;
begin
  if FReadTableName <> '' then
  begin
    FTable := DataDictionaryObject.Tables.TableByName(FReadTableName);
    FReadTableName := '';
  end;

  Fields.FixupReferences;  
end;

procedure TDataDictForeignKey.ReadTable(Reader: TReader);
begin
  FReadTableName := Reader.ReadString;
end;

procedure TDataDictForeignKey.SetFields(const Value: TDataDictForeignKeyFields);
begin
  FFields.Assign(Value);
end;

procedure TDataDictForeignKey.WriteTable(Writer: TWriter);
begin
  Writer.WriteString(FTable.Name);
end;

procedure TDataDictForeignKey.Assign(Source: TPersistent);
begin
  inherited;
  FReadTableName := TDataDictForeignKey(Source).Table.Name;
  Fields := TDataDictForeignKey(Source).Fields;
  FixupReferences;
end;



{ TDataDictForeignKeyField }

procedure TDataDictForeignKeyField.FixupReferences;
begin
  if FReadFieldName <> '' then
  begin
    FField := TDataDictTable(TDataDictForeignKey(Collection.Owner).Collection.Owner).Fields.FieldByName(FReadFieldName);
    FReadFieldName := '';
  end;
end;


{ TDataDictKey }

function TDataDictKey.AddKeyField(const AField: TDataDictField): TDataDictKeyField;
begin
  Result := TDataDictKeyField(Add);
  Result.Field := AField;
end;

function TDataDictKey.FieldByName(const FieldName: String): TDataDictKeyField;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if AnsiSameText(Items[i].Field.Name, FieldName) then
    begin
      Result := Items[i];
      break;
    end;
end;

function TDataDictKey.FindField(const AField: TDataDictField): TDataDictKeyField;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if Items[i].Field = AField then
    begin
      Result := Items[i];
      break;
    end;
end;

procedure TDataDictKey.FixupReferences;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    TDataDictKeyField(Items[i]).FixupReferences;
end;

function TDataDictKey.GetItem(Index: Integer): TDataDictKeyField;
begin
  Result := TDataDictKeyField(inherited Items[Index]);
end;

procedure TDataDictKey.SetItem(Index: Integer; const Value: TDataDictKeyField);
begin
  (inherited Items[Index]).Assign(Value);
end;



{ TDataDictField }

procedure TDataDictField.Assign(Source: TPersistent);
begin
  inherited;
  FFieldType := TDataDictField(Source).FieldType;
  FieldValues := TDataDictField(Source).FieldValues;
  FSize := TDataDictField(Source).Size;
end;

constructor TDataDictField.Create(Collection: TCollection);
begin
  inherited;
  FFieldValues := TStringList.Create;
end;

destructor TDataDictField.Destroy;
begin
  FreeAndNil(FFieldValues);
  inherited;
end;

function TDataDictField.FieldValuesNotEmpty: Boolean;
begin
  Result := FFieldValues.Count > 0;
end;

function TDataDictField.FieldValuesReal: TIniString;
begin
  Result := GetAllowedValues(FieldValues, DataDictionaryObject);
end;

procedure TDataDictField.SetFieldType(const Value: TDataDictDataType);
begin
  FFieldType := Value;
end;

procedure TDataDictField.SetFieldValues(const Value: TStrings);
begin
  FFieldValues.Assign(Value);
end;

function TDataDictField.Table: TDataDictTable;
begin
  Result := TDataDictTable(Collection.Owner);
end;


{ TDataDictParam }

procedure TDataDictParam.Assign(Source: TPersistent);
begin
  inherited;
  ParamType := TDataDictParam(Source).ParamType;
  ParamValues := TDataDictParam(Source).ParamValues;
end;

constructor TDataDictParam.Create(Collection: TCollection);
begin
  inherited;
  FParamValues := TStringList.Create;
end;

destructor TDataDictParam.Destroy;
begin
  FreeAndNil(FParamValues);
  inherited;
end;

procedure TDataDictParam.SetParamValues(const Value: TStrings);
begin
  FParamValues.Assign(Value);
end;

function TDataDictParam.ParamValuesNotEmpty: Boolean;
begin
  Result := FParamValues.Count > 0;
end;

procedure TDataDictParam.SetParamType(const Value: TDataDictDataType);
begin
  FParamType := Value;
end;

function TDataDictParam.ParamValuesReal: TIniString;
begin
  Result := GetAllowedValues(ParamValues, DataDictionaryObject);
end;


{ TDataDictForeignKeys }

function TDataDictForeignKeys.AddForeignKey(const ATable: TDataDictTable): TDataDictForeignKey;
begin
  Result := TDataDictForeignKey(Add);
  Result.Table := ATable;
end;

procedure TDataDictForeignKeys.FixupReferences;
var
  i: Integer;
  DD: TDataDictionary;
begin
  for i := 0 to Count - 1 do
    TDataDictForeignKey(Items[i]).FixupReferences;

  DD := TDataDictTable(Owner).DataDictionaryObject;
  for i := Count - 1 downto 0 do
  begin
    if (TDataDictForeignKey(Items[i]).Table = nil) or
       (TDataDictForeignKey(Items[i]).Fields.Count = 0) or
       (DD.Tables.IndexOf(TDataDictForeignKey(Items[i]).Table) = -1) then
      Items[i].Free;
  end;  
end;

function TDataDictForeignKeys.ForeignKeyByField(const AField: TDataDictField; const StartSearchFrom: Integer): TDataDictForeignKey;
var
  i: Integer;
begin
  Result := nil;
  for i := StartSearchFrom to Count - 1 do
    if Items[i].Fields.FindField(AField) <> nil then
    begin
      Result := Items[i];
      break;
    end;
end;

function TDataDictForeignKeys.ForeignKeyByTable(const ATable: TDataDictTable; const StartSearchFrom: Integer): TDataDictForeignKey;
var
  i: Integer;
begin
  Result := nil;
  for i := StartSearchFrom to Count - 1 do
    if Items[i].Table = ATable then
    begin
      Result := Items[i];
      break;
    end;
end;

function TDataDictForeignKeys.GetItem(Index: Integer): TDataDictForeignKey;
begin
  Result := TDataDictForeignKey(inherited Items[Index]);
end;

procedure TDataDictForeignKeys.SetItem(Index: Integer; const Value: TDataDictForeignKey);
begin
  (inherited Items[Index]).Assign(Value);
end;


{ TDataDictParams }

function TDataDictParams.AddParam: TDataDictParam;
begin
  Result := TDataDictParam(Add);
end;

function TDataDictParams.GetItem(Index: Integer): TDataDictParam;
begin
  Result := TDataDictParam(inherited Items[Index]);
end;

function TDataDictParams.ParamByName(const AParamName: String): TDataDictParam;
begin
  Result := TDataDictParam(ItemByName(AParamName));
end;

procedure TDataDictParams.SetItem(Index: Integer; const Value: TDataDictParam);
begin
  (inherited Items[Index]).Assign(Value);
end;



{ TDataDictPrimKey }

procedure TDataDictPrimKey.FixupReferences;
var
  i: Integer;
  T: TDataDictTable;
begin
  inherited;

  T := TDataDictTable(Owner);

  for i := Count - 1 downto 0 do
    if (Items[i].FField = nil) or (T.Fields.IndexOf(Items[i].FField) = -1) then
      Items[i].Free;
end;

function TDataDictPrimKey.GetItem(Index: Integer): TDataDictPrimKeyField;
begin
  Result := TDataDictPrimKeyField(inherited Items[Index]);
end;

procedure TDataDictPrimKey.SetItem(Index: Integer; const Value: TDataDictPrimKeyField);
begin
  (inherited Items[Index]).Assign(Value);
end;



{ TDataDictForeignKeyFields }

procedure TDataDictForeignKeyFields.FixupReferences;
var
  i: Integer;
  T: TDataDictTable;
begin
  inherited;

  T := TDataDictTable(TDataDictForeignKeys(TDataDictForeignKey(Owner).Collection).Owner);

  for i := Count - 1 downto 0 do
    if (Items[i].FField = nil) or (T.Fields.IndexOf(Items[i].FField) = -1) then
      Items[i].Free;
end;

function TDataDictForeignKeyFields.GetItem(Index: Integer): TDataDictForeignKeyField;
begin
  Result := TDataDictForeignKeyField(inherited Items[Index]);
end;

procedure TDataDictForeignKeyFields.SetItem(Index: Integer; const Value: TDataDictForeignKeyField);
begin
  (inherited Items[Index]).Assign(Value);
end;

{ TInterfacedCollectionItem }

function TInterfacedCollectionItem._AddRef: Integer;
begin
  Result := -1;
end;

function TInterfacedCollectionItem._Release: Integer;
begin
  Result := -1;
end;

function TInterfacedCollectionItem.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

{ TInterfacedCollection }

function TInterfacedCollection._AddRef: Integer;
begin
  Result := -1;
end;

function TInterfacedCollection._Release: Integer;
begin
  Result := -1;
end;

function TInterfacedCollection.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

end.
