{*********************************************************}
{* Report Writer: rwEngineTypes.pas                      *}
{* Copyright (c) iSystems LLC 1999, 2004                 *}
{* All rights reserved.                                  *}
{*********************************************************}
{* Definition of interfaces and types for RW Engine      *}
{*********************************************************}

unit rwEngineTypes;

interface

uses
  Classes, SysUtils, EvStreamUtils, isExceptions;

type

  IrwDataDictionary       = interface;
  IrwLibraryComponents    = interface;
  IrwLibraryFunctions     = interface;
  IrwSQLConstants         = interface;
  IrwInputForm            = interface;
  IrwResult               = interface;
  IrwParams               = interface;
  IrwParam                = interface;
  IrwDBCache              = interface;
  IrwSBTable              = interface;
  IrwSBField              = interface;


  //Utility structures

  TCommaDilimitedString = string; // Comma delimited string
  TIniString = string; // String in INI format (Value=Description) broken up by VK_RETURN


  TrwDDType = (ddtUnknown, ddtInteger, ddtFloat, ddtCurrency, ddtDateTime,
               ddtString, ddtArray, ddtBLOB, ddtMemo, ddtGraphic, ddtBoolean);

  TrwReportFormatType = (rwRFTbinary, rwRFTtext);

  TrwReportRec = record
    Data:       TStream;             // Report data (RWR/TXT format)
    ReportClassName: String;         // Library Report Class Name
    ReportAncestorClassName: String; // Library Report Class Name

    FormatType: TrwReportFormatType; // Stream format type
    Password:   String;              // Provide password for secured report
    ReportName: String;              // Working thread name, so it can be identified in call stack.
  end;

  PTrwReportRec = ^TrwReportRec;

  TrwReportContentRec = record
    ClassName: String;
    AncestorClassName: String;
    Description: String;          
    UsedLibComponents: TCommaDilimitedString; // List of Library Components used in the report
  end;

  TrwInputFormMode = (rwIFRunTime, rwIFSetUpTime);

  TrwRPType = (rwvUnknown, rwvInteger, rwvBoolean, rwvString, rwvFloat, rwvDate, rwvArray, rwvPointer, rwvCurrency, rwvVariant);

  TrwReportParamRec = record
    Name:         String;
    ParamType:    TrwRPType;
    Value:        Variant;
    RunTimeParam: Boolean;
  end;

  TrwReportParamList = array of TrwReportParamRec;
  PTrwReportParamList = ^TrwReportParamList;

  WndHandle = type LongWord;

  TrwShowInputFormRec = record
    Params:       PTrwReportParamList; // Report input parameters. Shouldn't be nil
    Mode:         TrwInputFormMode;    // Hide or show "run-time only" controls
    Parent:       WndHandle;           // The place where to embed. If it's nil it will be shown as modal dialog
  end;

  PTrwShowInputFormRec = ^TrwShowInputFormRec;


  TrwRunReportRec = record
    Params:           PTrwReportParamList;  // Report input parameters. Gets ignored if it's nil
    ReturnParamsBack: Boolean;              // Store all published and public global variables after execution into Params
    DoNotPrepare:     Boolean;              // Do not run preparation if report already has been prepared
    ShowInputForm:    Boolean;              // Show input form before running the report. Use only in main thread!
    Duplexing:        Boolean;              // Make all result to be duplexed
    ReturnValue:      Variant;              // Return result(code) if report was aborted
  end;

  PTrwRunReportRec = ^TrwRunReportRec;


  TrwParamRec = record
    Name: String;
    ParamType: TrwDDType;
    Value: Variant;
  end;

  TrwStrList = array of string;
  TrwParamList = array of TrwParamRec;
  TrwSBFieldList = array of IrwSBField;


  TrwDDTableInfoRec = record
    Name:        String;
    DisplayName: String;
    PrimaryKey:  TCommaDilimitedString;  // FieldName1, FieldName2, FieldName3
    LogicalKey:  TCommaDilimitedString;  // FieldName1, FieldName2, FieldName3
    ForeignKey:  TIniString;             // TableName1 = FieldName1, FieldName2
    Params:      TCommaDilimitedString;  // ParamName1, ParamName2, ParamName3
  end;


  TrwDDFieldInfoRec = record
    Name:        String;
    DisplayName: String;
    FieldType:   TrwDDType;
    Size:        Integer;
    FieldValues: TIniString; // Value = DisplayName
  end;


  TrwDDParamInfoRec = record
    Name:         String;
    DisplayName:  String;
    ParamType:    TrwDDType;
    ParamValues:  TIniString; // Value = DisplayName
    DefaultValue: String;
  end;


  TrwLayerNumber = 0..31;
  TrwPrintableLayers = set of TrwLayerNumber;

  TrwDuplexPrintType = (rdpDefault, rdpDuplex, rdpSimplex);

  TrwPrintJobInfoRec = record
    PrintJobName:            String;
    PrinterName:             String;
    TrayName:                String;
    OutBinNbr:               Integer;
    Duplexing:               TrwDuplexPrintType;
    PrintPCLAsGraphic:       Boolean;
    PrinterVerticalOffset:   Integer;
    PrinterHorizontalOffset: Integer;

    FirstPage:               Integer;
    LastPage:                Integer;
    Layers:                  TrwPrintableLayers;
    Copies:                  Integer;
  end;

  PTrwPrintJobInfoRec = ^TrwPrintJobInfoRec;

  TrwRWASecurityRec = record
    Rights:          Byte;
    Compressed:      Boolean;
    PreviewPassword: String;
    PrintPassword:   String;
  end;

  TrwPDFProtectionOption = (pdfPrint, pdfModifyStructure, pdfCopyInformation, pdfModifyAnnotation);
  TrwPDFProtectionOptions = set of TrwPDFProtectionOption;

  TrwPDFInfoRec = record
    Author: string;
    Creator: string;
    Subject: string;
    Title: string;
    Compression: Boolean;
    ProtectionOptions: TrwPDFProtectionOptions;
    OwnerPassword: string;
    UserPassword: string;
  end;

  PTrwPDFInfoRec = ^TrwPDFInfoRec;


  PTrwRWASecurityRec = ^TrwRWASecurityRec;

  TrwResultFormatType = (rwRTUnknown, rwRTRWA, rwRTASCII, rwRTPDF, rwRTHTML, rwRTXML, rwRTXLS);

  TrwRWAInfoRec = record
    Name:         String;

    Data1:        IrwResult;   // Either of these. Priority is by "Data" sequence number
    Data2:        TStream;
    Data3:        IEvDualStream;
    Data4:        TFileName;

    SecuredMode:  Boolean;     // Preview only! Do not allow to print or save.
    PreviewPswd:  String;      // Provide preview password for secured results
    PrintPswd:    String;      // Provide print password for secured results

    PrintJobInfo: TrwPrintJobInfoRec;
  end;

  PTrwRWAInfoRec = ^TrwRWAInfoRec;

  TrwRWAInfoList = array of TrwRWAInfoRec;
  PTrwRWAInfoList = ^TrwRWAInfoList;


  TrwFunctParamInfoRec = record
    Name:        String;
    DisplayName: String;
    Domain:      TIniString;
    DefaulValue: String;
  end;

  TrwExtFunctAddInfoRec = record
    DisplayName:  String;
    GroupName:    String;
    Description:  String;
    IsTextFilter: Boolean;
    ParamsInfo:   array of TrwFunctParamInfoRec;
  end;

  PTrwExtFunctAddInfoRec = ^TrwExtFunctAddInfoRec;


  TrwPreviewInfoRec = record
    Caption:        String;
    Owner:          TComponent;
    Parent:         WndHandle;        // Show modal form if Parent is nil
    Reports:        TrwRWAInfoList;
    SingleInstance: Boolean;     // If True alows to have only one preview frame. If the one exists assigns pointer to PreviewFrame field. Otherwise creates new frame.
    PreviewFrame:   TObject;     // TFrame object
  end;

  PTrwPreviewInfoRec = ^TrwPreviewInfoRec;

  ErwException = class(EisException);
  ErwAbort     = class (ErwException);


  // RW Run Process structures

  TrwVMCodeSegment    = (rcsNone, rcsReport, rcsLibFunctions, rcsLibComponents, rcsWatchList);

  TrwVMAddress = record
    Segment: TrwVMCodeSegment;
    Offset: Integer;
  end;


             {*********************************************************}
             {*          Declaration of RW Engine interfaces          *}
             {*********************************************************}


  //Common interface to access RW Engine
  IrwEngine = interface
  ['{E37785DA-EBE3-4F03-B809-30C861606082}']

    // External Modules
    function  DataDictionary: IrwDataDictionary;
    function  LibraryComponents: IrwLibraryComponents;
    function  LibraryFunctions: IrwLibraryFunctions;
    function  SQLConstants: IrwSQLConstants;

    // Show report's Input Form and return pointer to the object. If Input Form has been embedded into a container
    // user must close call IrwInputForm.Close function in order to close and destroy it in legitimate way.
    function  ShowInputForm(const pReportInfo: PTrwReportRec; const pShowInfo: PTrwShowInputFormRec): IrwInputForm;

    // Run report and return result
    function  RunReport(const pReportInfo: PTrwReportRec; const pRunInfo: PTrwRunReportRec): IrwResult;

    // Compile report and return it back
    procedure CompileReport(const pReportInfo: PTrwReportRec);

    // Prepares report before running. This process compiles report and parses all SQL statements.
    // It helps to decrease volume of requesting data from the back-end database in multi-report mode.
    procedure PrepareReport(const pReportInfo: PTrwReportRec);

    // Analyzes report content
    function AnalyzeReport(const pReportInfo: PTrwReportRec): TrwReportContentRec;

    // Preview report result
    procedure Preview(const pPreviewInfo: PTrwPreviewInfoRec);

    // For printing solely from main thread
    procedure Print(const pPreviewInfo: PTrwRWAInfoRec);

    // For printing from any thread. Windows API is not thread-safe in image related functions.
    procedure PrintFromThread(const pReportList: PTrwRWAInfoList);

    // DB Chache
    function DBCache: IrwDBCache;

    // Execute raw SUVBase SQL statements
    function  OpenSBTable(const ATableName: String; const AReadOnly: Boolean): IrwSBTable;
    function  OpenSBSQL(const ASQL: String; const Params: array of TrwParamRec): IrwSBTable;
    procedure ExecSBSQL(const ASQL: String; const Params: array of TrwParamRec);
    procedure CopySBTable(const ATable: IrwSBTable; const ANewTableName: String);

    // Executes RW query
    procedure OpenQuery(const ASQL: String; const Params: array of TrwParamRec; const ResultDataSet: TObject);

    // Routines for RWP format
    function  ParamStreamToParamList(const AParamStream: TStream): TrwReportParamList;
    procedure ParamListToParamStream(const AParamList: TrwReportParamList; const AParamStream: TStream);

    function  ReportExecutionHalted: Boolean;
    procedure HaltReportExecution;

    // Routines for RWA format
    function  MakeReportResult(const RWResults: PTrwRWAInfoList; const AType: TrwResultFormatType): IrwResult;
    procedure SetReportResultSecurity(const pPreviewInfo: PTrwRWAInfoRec; const pNewSecurityInfo: PTrwRWASecurityRec);

    // Converts RWA result into PDF
    function RWAtoPDF(const pPreviewInfo: PTrwRWAInfoRec; const pPDFInfoRec: PTrwPDFInfoRec): IEvDualStream;

    procedure RaiseException(const AErrorMessage: String);
  end;


  // RW external module such as: Library, Data Dictionary and etc.
  IrwExternalModule = interface
  ['{FEFF053A-9306-4ED3-AE12-0FBAF501F085}']
  end;


  IrwDDField = interface
  ['{01A315D2-E55A-45B7-BCB3-69FE3842BCE5}']
    function GetFieldExternalName: String;
    function GetFieldName: String;
  end;


  IrwDDTable = interface
  ['{A3B4D0B3-6B46-443E-8682-2C011D56A215}']
    function GetTableExternalName: String;
    function GetTableName: String;
    function GetPrimaryKey: TCommaDilimitedString;
    function GetLogicalKey: TCommaDilimitedString;
    function GetForeignKeys: TIniString;
    function GetForeignKeysByField(const AField: String): TIniString;
    function GetForeignKeysByTable(const ATable: String): TIniString;
    function GetFieldList: TCommaDilimitedString;
    function GetFieldByExternalName(const AExternalFieldName: String): IrwDDField;
    function GetField(const AFieldName: String): IrwDDField;
  end;


  IrwDataDictionary = interface(IrwExternalModule)
  ['{67965459-107E-4513-A84A-CD8CC6E2C679}']
    function GetTableByExternalName(const AExternalTableName: String): IrwDDTable;
    function GetTable(const ATableName: String): IrwDDTable;
  end;

  
  // Library Component
  IrwLibraryComponent = interface
  ['{40917367-E61B-4E21-96E5-19DAB4C76CE4}']
    function GetClassName: String;
    function GetAncestorClassName: String;
    function GetDisplayName: String;
    function IsInheritsFrom(const AClassName: String): Boolean;
  end;


  // Library of Components object
  IrwLibraryComponents = interface(IrwExternalModule)
  ['{6906A2CB-FCD0-47C0-B66F-F24C333E7180}']
    function GetComponentList: TCommaDilimitedString;
    function GetComponent(const AClassName: String): IrwLibraryComponent;
  end;


  IrwLibraryFunction = interface
  ['{A6BDA8F7-DC22-49DA-87C1-81C63CA343F6}']
  end;


  // Library of Functions object
  IrwLibraryFunctions = interface(IrwExternalModule)
  ['{566B242C-97AB-4B8B-9FDD-B69B41433793}']
  end;


  // SQL Constants object
  IrwSQLConstants = interface(IrwExternalModule)
  ['{4E223B67-EB4D-4A04-B7AA-7D3E173FDDAB}']
  end;


  // Input Form object
  IrwInputForm = interface
  ['{E7ABBDC3-F47D-4741-ABA3-F90DC3F00B99}']
    // Closes form and returns parameters if Params is not nil (emulation of pressing OK button).
    // If Params is nil forms just gets closed (emulation of pressing CANCEL button).
    function  Close(const Params: PTrwReportParamList): Boolean;
    function  IsModalDialog: Boolean;
    function  VCLObject: TObject;
  end;


  TrwResultPageInfo = record
    PageNumber:   Integer;
    WidthMM:      Integer;
    HeightMM:     Integer;
    Duplexing:    Boolean;
  end;


  TrwResultMiscInfo = record
    DefaultFileName: String;
  end;

  TrwResultPageInfoList = array of TrwResultPageInfo;
  PTrwResultPageInfoList = ^TrwResultPageInfoList;

  // Result of report running (RWA/TXT file)
  IrwResult = interface
  ['{4C03B17B-FBC5-40B6-919F-F754536A4854}']
    function Data: TStream;
    function FileName: String;
    function ResultType: TrwResultFormatType;
    function PageInfo: PTrwResultPageInfoList;
    function MiscInfo: TrwResultMiscInfo;
  end;


  // List of abstract parameters
  IrwParam = interface
  ['{59FCF9E1-4790-4AF4-BEE7-78034E5A9468}']
    procedure SetName(const AName: String);
    function  GetName: String;
    procedure SetValue(const AValue: Variant);
    function  GetValue: Variant;
    property  Name: string read GetName write SetName;
    property  Value: Variant read GetValue write SetValue;
  end;

  IrwParams = interface
  ['{3051C8C3-080A-4CED-90A1-AF262C90A278}']
    function  GetParamByName(const Name: String): IrwParam;
    function  GetParamByIndex(const Index: Integer): IrwParam;
  end;


  // Internal RW table for storing content of Data Dictionary item.
  IrwSBTable = interface
  ['{4F44093C-A1B0-42BD-830D-C9BB7D35F089}']

    function RealObject: TObject;  
    function TableFileName: String;
    // Function for filling up the table with data coming from back-end database
    // Referencing works by field index: 0, 1, 2 etc.
    function  GetFields: TrwSBFieldList;
    function  GetFieldByName(const FieldName: String): IrwSBField;
    procedure Append;
    procedure Insert;
    procedure Post;
    procedure Edit;
    procedure Delete;
    procedure First;
    procedure Next;
    procedure Prior;
    procedure Last;
    function  IsEof: Boolean;
    function  IsBof: Boolean;
    procedure AppendRecord(const FieldValues: array of Variant);
    procedure SetFilter(const AFilter: String);
    function  GetFilter: String;
    procedure SetFiltered(const AFiltered: Boolean);
    function  GetFiltered: Boolean;
  end;


  IrwSBField = interface
  ['{E03AB4A1-8D17-4B02-BC8C-42D320398681}']
    function  GetFieldName: string;
    function  GetDataType: TrwDDType;
    function  GetSize: Byte;
    function  GetIndex: Integer;
    procedure SetValue(const AValue: Variant);
    function  GetValue: Variant;
    function  GetAsString: String;
    procedure SetAsString(const AValue: String);
    function  GetAsInteger: Integer;
    function  GetAsCardinal: Cardinal;    
    procedure SetAsInteger(const AValue: Integer);
    function  GetAsByte: Byte;
    procedure SetAsByte(const AValue: Byte);
    function  GetAsDateTime: TDateTime;
    procedure SetAsDateTime(const AValue: TDateTime);
    function  GetAsCurrency: Currency;
    procedure SetAsCurrency(const AValue: Currency);
    function  GetAsBoolean: ByteBool;
    procedure SetAsBoolean(const AValue: ByteBool);
    function  GetAsFloat: Double;
    procedure SetAsFloat(const AValue: Double);
  end;


  IrwCachedTable = interface;
  
  IrwCachedTableImage = interface
  ['{DA8CAE67-6854-4C9B-92BC-938DC4E13D29}']
    function CachedTable: IrwCachedTable;
    function GetParams: TrwParamList;
    function GetSBTable: IrwSBTable;
  end;


  IrwCachedTable = interface
  ['{B526F991-83B6-40FC-9234-C51577D7FBC8}']
    function  ddTable: IrwDDTable;
    function  GetTableImageByParams(const Params: array of TrwParamRec): IrwCachedTableImage;
    function  GetFields: TrwStrList;
    procedure SetFields(const FieldList: TrwStrList);
    function  GetExternalFields: TrwStrList;
    function  GetFilter: String;
  end;


  IrwDBCache = interface
  ['{E1A89352-9859-4B26-B68D-AAF363DAECC8}']
    function  GetCachedTable(const ATable: String): IrwCachedTable;

    // Destroys all cache collected information
    procedure ClearCache;

    // Initiates DB cache cleanining process
    // It fires OnDBCacheCleaning event for each cached table
    procedure EmptyCachedTables;

    // Returns DB cache folder name
    function GetCachePath: String;
  end;



  // Security information for Designer

  TrwSecItem = record
    Modify: Boolean;
  end;

  TrwDesignerSecurityRec = record
    DataDictionary:  TrwSecItem;
    QBTemplates:     TrwSecItem;
    QBConstants:     TrwSecItem;
    LibComponents:   TrwSecItem;
    LibFunctions:    TrwSecItem;
    LicencedReports: TrwSecItem;
  end;





             {*********************************************************}
             {*                  Callback interface                  *}
             {*        Needs to be handled by the host application    *}
             {*********************************************************}


  IrwAppAdapter = interface
  ['{5FBD5F22-A5F8-4652-B593-7EAA5AA4700A}']
    procedure Activate(const RWEngine: IrwEngine);
    procedure Deactivate;
    procedure Terminate;

    //           ENGINE CALLBACK
    // Returns temp folder path
    function GetTempDir: String;

    // Handler for all run-time Exceptions
    procedure HandleException(var ErrorMessage: String);

    // Handling Exteral Modules events (Data Dictionary, Library etc.)
    procedure LoadDataDictionary(const Data: TStream);
    procedure UnloadDataDictionary(const Data: TStream);
    procedure LoadQBTemplates(const Data: TStream);
    procedure UnloadQBTemplates(const Data: TStream);
    procedure LoadLibraryComponents(const Data: TStream);
    procedure UnloadLibraryComponents(const Data: TStream);
    procedure LoadLibraryFunctions(const Data: TStream);
    procedure UnloadLibraryFunctions(const Data: TStream);
    procedure LoadSQLConstants(const Data: TStream);
    procedure UnloadSQLConstants(const Data: TStream);

    // Data Dictionary Editor and Synchronization
    function  GetDDTables: TCommaDilimitedString; // Returns tables external names
    function  GetDDTableInfo(const ATable: String): TrwDDTableInfoRec;
    function  GetDDFields(const ATable: String): TCommaDilimitedString; // Returns fields external names for table
    function  GetDDFieldInfo(const ATable, AField: String): TrwDDFieldInfoRec;
    function  GetDDParamInfo(const ATable, Param: String): TrwDDParamInfoRec;

    // RW Library Extension
    // GetFunctionDeclarations returns list of function headers written on RW language and delimited by symbol #13
    // Example:  procedure OpenClient(Client: Integer)
    //           function  CalcBalance(Dat_b: Date; Dat_e: Date): Currency
    //           etc.
    procedure GetExternalFunctionDeclarations(out Result: TIniString);
    function  GetExternalFunctionAdditionalInfo(const AFunctionName: String): TrwExtFunctAddInfoRec;
    procedure ExecuteExternalFunction(const FunctionID: Integer; var Params: array of Variant; out Result: Variant);


    // Handling DB requests. Happens whenever RW needs to get data from back-end database
    procedure BeforeCacheTable(const CachedTable: IrwCachedTable; const Params: Array of TrwParamRec);
    procedure TableDataRequest(const CachedTable: IrwCachedTable; const rwTable: IrwSBTable; const Params: Array of TrwParamRec);
    procedure AfterCacheTable(const CachedTableImage: IrwCachedTableImage);

    // Handling cleaning process of the internal cache
    procedure DBCacheCleaning(const DataDictExternalTableName: String; var Accept: Boolean);

    // Handling progress bar status during long-time internal processes
    procedure StartEngineProgressBar(const Title: String; const MaxValue: Integer = -1);
    procedure UpdateEngineProgressBar(const ProgressText: String; const CurrentValue: Integer = -1);
    procedure EndEngineProgressBar;

    //Misc (TrwDBText callback)
    function SubstDefaultClassName(const ADefaultClassName: String): String;
    function SubstituteValue(const Value: Variant; const SubstID: String): Variant;

    function CurrentDateTime: TDateTime;

    // Returns password to encrypt/decrypt report
    function GetLicencePassword(const LicenceID: String): String;

    procedure AfterClosePreview(const APreviewFrame: TObject);

    // Provides SOME custom data for adapter using application specific format (e.g. login and password etc.)
    procedure SetAppCustomData(var AData: Variant);
    function  DispatchCustomFunction(const FunctionName: String; const Params: Variant): Variant;

    // Report parameters rutines
    function  ReadReportParamsFromStream(const Data: TStream): TrwReportParamList;
    procedure WriteReportParamsToStream(const AParamList: PTrwReportParamList; const Data: TStream);

    //           DESIGNER CALLBACK

    // Handling progress bar status during long-time internal processes
    procedure StartDesignerProgressBar(const Title: String; const MaxValue: Integer = -1);
    procedure UpdateDesignerProgressBar(const ProgressText: String; const CurrentValue: Integer = -1);
    procedure EndDesignerProgressBar;

    function  GetSecurityDescriptor: TrwDesignerSecurityRec;
    function  GetCustomControl(const Destination: String): TObject;
  end;



  // function  ReportWriterAppAdapter: IrwAppAdapter
  // procedure Initialize
  // procedure Finalize
  // Must be exported within AppAdapter DLL!

  TGetRWAppAdapterProc = function : IrwAppAdapter; stdcall;
  TrwInitializeProc = procedure; stdcall;
  TrwFinalizeProc = procedure; stdcall;

const cReportWriterAppAdapter = 'ReportWriterAppAdapter';
const cInitialize = 'Initialize';
const cFinalize = 'Finalize';


var AppAdapterModuleName: String; // Current session AppAdapter
var AppAdapterAppCustomData: Variant; // Current session AppAdapter AppCustomData

implementation

end.
