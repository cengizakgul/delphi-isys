unit rwCallback;

interface

uses Classes, SysUtils, Windows, Forms, rwEngineTypes, isBaseClasses;

type
  TrwECBTempDirProc = procedure (out TempDir: String) of object;
  TrwECBHandleExceptionProc = procedure (var ErrorMessage: String) of object;
  TrwECBExternalResourceProc = procedure (Data: TStream) of object;
  TrwECBDDTableListProc = procedure (out Tables: TCommaDilimitedString) of object;
  TrwECBDDTableInfoProc = procedure (const ATable: String; out TableInfo: TrwDDTableInfoRec) of object;
  TrwECBDDFieldListProc = procedure (const ATable: String; out Fields: TCommaDilimitedString) of object;
  TrwECBDDFieldInfoProc = procedure (const ATable: String; const AField: String; out FieldInfo: TrwDDFieldInfoRec) of object;
  TrwECBDDParamInfoProc = procedure (const ATable, Param: String; out ParmInfo: TrwDDParamInfoRec) of object;
  TrwECBExtFunctionDeclProc = procedure (const FunctionHeaders: TStringList) of object;
  TrwECBExtFunctionAddInfoProc = procedure (const FunctionName: String; out FunctInfo: TrwExtFunctAddInfoRec) of object;
  TrwECBExtFunctionExecProc = procedure (const FunctionID: Integer; var Params: array of Variant; out Result: Variant) of object;

  TrwECBBeforeCacheTableProc = procedure (const CachedTable: IrwCachedTable; const Params: array of TrwParamRec) of object;
  TrwECBTableDataRequestProc = procedure (const CachedTable: IrwCachedTable; const sbTable: IrwSBTable; const Params: array of TrwParamRec) of object;
  TrwECBAfterCacheTableProc  = procedure (const CachedTableImage: IrwCachedTableImage) of object;

  TrwECBDBCacheCleaningProc = procedure (const DataDictExternalTableName: String; var Accept: Boolean) of object;

  TrwECBStartProgressBarProc = procedure (const Title: String; MaxValue: Integer = -1) of object;
  TrwECBUpdateProgressBarProc = procedure (const ProgressText: String; CurrentValue: Integer = -1) of object;
  TrwECBEndProgressBarProc = procedure of object;

  TrwECBSubstituteValueProc = procedure (var AValue: Variant; const SubstID: String) of object;

  TrwECBCurrentDateTimeProc = procedure (var ADateTime: TDateTime) of object;

  TrwECBAfterClosePreviewProc = procedure (const APreviewFrame: TObject) of object;

  TrwECBLicencePasswordProc = procedure (const ALicenceID: String; var APassword: String) of object;

  TrwECBSubstDefaultClassNameProc = procedure (var ADefaultClassName: String) of object;

  TrwECBSetAppCustomDataProc = procedure (var AData: Variant) of object;
  TrwECBDispatchCustomFunctionProc = procedure (const FunctionName: String; const Params: Variant; var Result: Variant) of object;

  TrwECBReadReportParamsFromStreamProc = procedure (const AData: TStream; var AParamList: TrwReportParamList) of object;
  TrwECBWriteReportParamsToStreamProc = procedure (var AParamList: TrwReportParamList; const Data: TStream) of object;

  TrwDCBGetSecurityDescriptorProc = procedure (var ASecurityDescriptor: TrwDesignerSecurityRec) of object;
  TrwDCBGetCustomControlProc = procedure (const Destination: String; var AContainer: TForm) of object;


  TrwAppAdapter = class(TComponent, IrwAppAdapter)
  private
    FEngine: IrwEngine;

    FOnActivate: TNotifyEvent;
    FOnDeactivate: TNotifyEvent;
    FOnTerminate: TNotifyEvent;
    FOnGetTempDir: TrwECBTempDirProc;
    FOnLoadDataDictionary: TrwECBExternalResourceProc;
    FOnUnloadDataDictionary: TrwECBExternalResourceProc;
    FOnLoadQBTemplates: TrwECBExternalResourceProc;
    FOnUnloadQBTemplates: TrwECBExternalResourceProc;
    FOnLoadLibraryComponents: TrwECBExternalResourceProc;
    FOnUnloadLibraryComponents: TrwECBExternalResourceProc;
    FOnLoadLibraryFunctions: TrwECBExternalResourceProc;
    FOnUnloadLibraryFunctions: TrwECBExternalResourceProc;
    FOnLoadSQLConstants: TrwECBExternalResourceProc;
    FOnUnloadSQLConstants: TrwECBExternalResourceProc;
    FOnGetDDTables: TrwECBDDTableListProc;
    FOnGetDDTableInfo: TrwECBDDTableInfoProc;
    FOnGetDDFields: TrwECBDDFieldListProc;
    FOnGetDDFieldInfo: TrwECBDDFieldInfoProc;
    FOnGetDDParamInfo: TrwECBDDParamInfoProc;
    FOnGetExternalFunctionDeclarations: TrwECBExtFunctionDeclProc;
    FOnExecuteExternalFunction: TrwECBExtFunctionExecProc;
    FOnTableDataRequest: TrwECBTableDataRequestProc;
    FBeforeCacheTable: TrwECBBeforeCacheTableProc;
    FAfterCacheTable: TrwECBAfterCacheTableProc;
    FOnDBCacheCleaning: TrwECBDBCacheCleaningProc;
    FOnStartEngineProgressBar: TrwECBStartProgressBarProc;
    FOnUpdateEngineProgressBar: TrwECBUpdateProgressBarProc;
    FOnEndEngineProgressBar: TrwECBEndProgressBarProc;
    FOnSubstituteValue: TrwECBSubstituteValueProc;
    FOnGetCurrentDateTime: TrwECBCurrentDateTimeProc;
    FAfterClosePreview: TrwECBAfterClosePreviewProc;
    FOnGetLicencePassword: TrwECBLicencePasswordProc;
    FOnGetExternalFunctionAdditionalInfo: TrwECBExtFunctionAddInfoProc;
    FOnSubstDefaultClassName: TrwECBSubstDefaultClassNameProc;
    FOnSetAppCustomData: TrwECBSetAppCustomDataProc;
    FOnDispatchCustomFunction: TrwECBDispatchCustomFunctionProc;

    FOnStartDesignerProgressBar: TrwECBStartProgressBarProc;
    FOnUpdateDesignerProgressBar: TrwECBUpdateProgressBarProc;
    FOnEndDesignerProgressBar: TrwECBEndProgressBarProc;
    FOnReadReportParamsFromStream: TrwECBReadReportParamsFromStreamProc;
    FOnWriteReportParamsToStream: TrwECBWriteReportParamsToStreamProc;
    FOnGetSecurityDescriptor: TrwDCBGetSecurityDescriptorProc;
    FOnGetCustomControl: TrwDCBGetCustomControlProc;
    FOnException: TrwECBHandleExceptionProc;

  protected
    procedure Activate(const RWEngine: IrwEngine);
    procedure Deactivate;
    procedure Terminate;

    function  GetTempDir: String;
    procedure HandleException(var ErrorMessage: String);
    procedure LoadDataDictionary(const Data: TStream);
    procedure UnloadDataDictionary(const Data: TStream);
    procedure LoadQBTemplates(const Data: TStream);
    procedure UnloadQBTemplates(const Data: TStream);
    procedure LoadLibraryComponents(const Data: TStream);
    procedure UnloadLibraryComponents(const Data: TStream);
    procedure LoadLibraryFunctions(const Data: TStream);
    procedure UnloadLibraryFunctions(const Data: TStream);
    procedure LoadSQLConstants(const Data: TStream);
    procedure UnloadSQLConstants(const Data: TStream);
    function  GetDDTables: TCommaDilimitedString;
    function  GetDDTableInfo(const ATable: String): TrwDDTableInfoRec;
    function  GetDDFields(const ATable: String): TCommaDilimitedString;
    function  GetDDFieldInfo(const ATable, AField: String): TrwDDFieldInfoRec;
    function  GetDDParamInfo(const ATable, Param: String): TrwDDParamInfoRec;
    procedure GetExternalFunctionDeclarations(out FunctionHeaders: TIniString);
    function  GetExternalFunctionAdditionalInfo(const AFunctionName: String): TrwExtFunctAddInfoRec;
    procedure ExecuteExternalFunction(const FunctionID: Integer; var Params: array of Variant; out Result: Variant);
    procedure BeforeCacheTable(const CachedTable: IrwCachedTable; const Params: array of TrwParamRec);
    procedure TableDataRequest(const CachedTable: IrwCachedTable; const sbTable: IrwSBTable; const Params: array of TrwParamRec);
    procedure AfterCacheTable(const CachedTableImage: IrwCachedTableImage);
    procedure DBCacheCleaning(const DataDictExternalTableName: String; var Accept: Boolean);
    procedure StartEngineProgressBar(const Title: String; const MaxValue: Integer = -1);
    procedure UpdateEngineProgressBar(const ProgressText: String; const CurrentValue: Integer = -1);
    procedure EndEngineProgressBar;
    function  SubstituteValue(const Value: Variant; const SubstID: String): Variant;
    function  CurrentDateTime: TDateTime;
    function  SubstDefaultClassName(const ADefaultClassName: String): String;
    procedure AfterClosePreview(const APreviewFrame: TObject);
    function  GetLicencePassword(const LicenceID: String): String;
    procedure SetAppCustomData(var AData: Variant);
    function  DispatchCustomFunction(const FunctionName: String; const Params: Variant): Variant;
    function  ReadReportParamsFromStream(const Data: TStream): TrwReportParamList;
    procedure WriteReportParamsToStream(const AParamList: PTrwReportParamList; const Data: TStream);

    procedure StartDesignerProgressBar(const Title: String; const MaxValue: Integer = -1);
    procedure UpdateDesignerProgressBar(const ProgressText: String; const CurrentValue: Integer = -1);
    procedure EndDesignerProgressBar;
    function  GetSecurityDescriptor: TrwDesignerSecurityRec;
    function  GetCustomControl(const Destination: String): TObject;

  public
    property rwEngine: IrwEngine read FEngine;

  published
    property OnActivate: TNotifyEvent read FOnActivate write FOnActivate;
    property OnDeactivate: TNotifyEvent read FOnDeactivate write FOnDeactivate;
    property OnTerminate: TNotifyEvent read FOnTerminate write FOnTerminate;          
    property OnGetTempDir: TrwECBTempDirProc read FOnGetTempDir write FOnGetTempDir;
    property OnException: TrwECBHandleExceptionProc read FOnException write FOnException;
    property OnLoadDataDictionary: TrwECBExternalResourceProc read FOnLoadDataDictionary write FOnLoadDataDictionary;
    property OnUnloadDataDictionary: TrwECBExternalResourceProc read FOnUnloadDataDictionary write FOnUnloadDataDictionary;
    property OnLoadQBTemplates: TrwECBExternalResourceProc read FOnLoadQBTemplates write FOnLoadQBTemplates;
    property OnUnloadQBTemplates: TrwECBExternalResourceProc read FOnUnloadQBTemplates write FOnUnloadQBTemplates;
    property OnLoadLibraryComponents: TrwECBExternalResourceProc read FOnLoadLibraryComponents write FOnLoadLibraryComponents;
    property OnUnloadLibraryComponents: TrwECBExternalResourceProc read FOnUnloadLibraryComponents write FOnUnloadLibraryComponents;
    property OnLoadLibraryFunctions: TrwECBExternalResourceProc read FOnLoadLibraryFunctions write FOnLoadLibraryFunctions;
    property OnUnloadLibraryFunctions: TrwECBExternalResourceProc read FOnUnloadLibraryFunctions write FOnUnloadLibraryFunctions;
    property OnLoadSQLConstants: TrwECBExternalResourceProc read FOnLoadSQLConstants write FOnLoadSQLConstants;
    property OnUnloadSQLConstants: TrwECBExternalResourceProc read FOnUnloadSQLConstants write FOnUnloadSQLConstants;
    property OnGetDDTables: TrwECBDDTableListProc read FOnGetDDTables write FOnGetDDTables;
    property OnGetDDTableInfo: TrwECBDDTableInfoProc read FOnGetDDTableInfo write FOnGetDDTableInfo;
    property OnGetDDFields: TrwECBDDFieldListProc read FOnGetDDFields write FOnGetDDFields;
    property OnGetDDFieldInfo: TrwECBDDFieldInfoProc read FOnGetDDFieldInfo write FOnGetDDFieldInfo;
    property OnGetDDParamInfo: TrwECBDDParamInfoProc read FOnGetDDParamInfo write FOnGetDDParamInfo;
    property OnGetExternalFunctionDeclarations: TrwECBExtFunctionDeclProc read FOnGetExternalFunctionDeclarations write FOnGetExternalFunctionDeclarations;
    property OnGetExternalFunctionAdditionalInfo: TrwECBExtFunctionAddInfoProc read FOnGetExternalFunctionAdditionalInfo write FOnGetExternalFunctionAdditionalInfo;
    property OnExecuteExternalFunction: TrwECBExtFunctionExecProc read FOnExecuteExternalFunction write FOnExecuteExternalFunction;
    property OnTableDataRequest: TrwECBTableDataRequestProc read FOnTableDataRequest write FOnTableDataRequest;
    property OnBeforeCacheTable: TrwECBBeforeCacheTableProc read FBeforeCacheTable write FBeforeCacheTable;
    property OnAfterCacheTable: TrwECBAfterCacheTableProc read FAfterCacheTable write FAfterCacheTable;
    property OnDBCacheCleaning: TrwECBDBCacheCleaningProc read FOnDBCacheCleaning write FOnDBCacheCleaning;
    property OnStartEngineProgressBar: TrwECBStartProgressBarProc read FOnStartEngineProgressBar write FOnStartEngineProgressBar;
    property OnUpdateEngineProgressBar: TrwECBUpdateProgressBarProc read FOnUpdateEngineProgressBar write FOnUpdateEngineProgressBar;
    property OnEndEngineProgressBar: TrwECBEndProgressBarProc read FOnEndEngineProgressBar write FOnEndEngineProgressBar;
    property OnSubstituteValue: TrwECBSubstituteValueProc read FOnSubstituteValue write FOnSubstituteValue;
    property OnGetCurrentDateTime: TrwECBCurrentDateTimeProc read FOnGetCurrentDateTime write FOnGetCurrentDateTime;
    property OnAfterClosePreview: TrwECBAfterClosePreviewProc read FAfterClosePreview write FAfterClosePreview;
    property OnGetLicencePassword: TrwECBLicencePasswordProc read FOnGetLicencePassword write FOnGetLicencePassword;
    property OnSubstDefaultClassName: TrwECBSubstDefaultClassNameProc read FOnSubstDefaultClassName write FOnSubstDefaultClassName;
    property OnSetAppCustomData: TrwECBSetAppCustomDataProc read FOnSetAppCustomData write FOnSetAppCustomData;
    property OnDispatchCustomFunction: TrwECBDispatchCustomFunctionProc read FOnDispatchCustomFunction write FOnDispatchCustomFunction;
    property OnReadReportParamsFromStream: TrwECBReadReportParamsFromStreamProc read FOnReadReportParamsFromStream write FOnReadReportParamsFromStream;
    property OnWriteReportParamsToStream: TrwECBWriteReportParamsToStreamProc read FOnWriteReportParamsToStream write FOnWriteReportParamsToStream;

    property OnStartDesignerProgressBar: TrwECBStartProgressBarProc read FOnStartDesignerProgressBar write FOnStartDesignerProgressBar;
    property OnUpdateDesignerProgressBar: TrwECBUpdateProgressBarProc read FOnUpdateDesignerProgressBar write FOnUpdateDesignerProgressBar;
    property OnEndDesignerProgressBar: TrwECBEndProgressBarProc read FOnEndDesignerProgressBar write FOnEndDesignerProgressBar;
    property OnGetSecurityDescriptor: TrwDCBGetSecurityDescriptorProc read FOnGetSecurityDescriptor write FOnGetSecurityDescriptor;
    property OnGetCustomControl: TrwDCBGetCustomControlProc read FOnGetCustomControl write FOnGetCustomControl;
  end;


  procedure Register;

implementation

{$R *.res}

procedure Register;
begin
  RegisterComponents('ISClasses', [TrwAppAdapter]);
end;


{ TrwAppAdapter }

procedure TrwAppAdapter.DBCacheCleaning(const DataDictExternalTableName: String; var Accept: Boolean);
begin
  if Assigned(FOnDBCacheCleaning) then FOnDBCacheCleaning(DataDictExternalTableName, Accept);
end;

procedure TrwAppAdapter.EndEngineProgressBar;
begin
  if Assigned(FOnEndEngineProgressBar) then FOnEndEngineProgressBar();
end;

procedure TrwAppAdapter.ExecuteExternalFunction(const FunctionID: Integer; var Params: array of Variant; out Result: Variant);
begin
  if Assigned(FOnExecuteExternalFunction) then FOnExecuteExternalFunction(FunctionID, Params, Result);
end;

function TrwAppAdapter.GetDDFieldInfo(const ATable, AField: String): TrwDDFieldInfoRec;
begin
  ZeroMemory(@Result, SizeOf(Result));
  Result.Name := AField;
  if Assigned(FOnGetDDFieldInfo) then FOnGetDDFieldInfo(ATable, AField, Result);
end;

function TrwAppAdapter.GetDDFields(const ATable: String): TCommaDilimitedString;
begin
  if Assigned(FOnGetDDFields) then FOnGetDDFields(ATable, Result);
end;

function TrwAppAdapter.GetDDParamInfo(const ATable, Param: String): TrwDDParamInfoRec;
begin
  ZeroMemory(@Result, SizeOf(Result));
  Result.Name := Param;
  if Assigned(FOnGetDDParamInfo) then FOnGetDDParamInfo(ATable, Param, Result);
end;

function TrwAppAdapter.GetDDTableInfo(const ATable: String): TrwDDTableInfoRec;
begin
  ZeroMemory(@Result, SizeOf(Result));
  Result.Name := ATable;
  if Assigned(FOnGetDDTableInfo) then FOnGetDDTableInfo(ATable, Result);
end;

function TrwAppAdapter.GetDDTables: TCommaDilimitedString;
begin
  if Assigned(FOnGetDDTables) then FOnGetDDTables(Result);
end;

procedure TrwAppAdapter.GetExternalFunctionDeclarations(out FunctionHeaders: TIniString);
var
  L: TStringList;
  i: Integer;
begin
  if Assigned(FOnGetExternalFunctionDeclarations) then
  begin
    L := TStringList.Create;
    try
      FOnGetExternalFunctionDeclarations(L);
      FunctionHeaders := '';
      for i := 0 to L.Count - 1 do
      begin
        if i > 0 then
          FunctionHeaders := FunctionHeaders + #13;
        FunctionHeaders := FunctionHeaders + L[i] + '=' + IntToStr(Integer(L.Objects[i]));
      end;
    finally
      L.Free;
    end;
  end;
end;

function TrwAppAdapter.GetTempDir: String;
begin
  if Assigned(FOnGetTempDir) then FOnGetTempDir(Result)
end;

procedure TrwAppAdapter.LoadDataDictionary(const Data: TStream);
begin
  if Assigned(FOnLoadDataDictionary) then FOnLoadDataDictionary(Data);
end;

procedure TrwAppAdapter.LoadLibraryComponents(const Data: TStream);
begin
  if Assigned(FOnLoadLibraryComponents) then FOnLoadLibraryComponents(Data);
end;

procedure TrwAppAdapter.LoadLibraryFunctions(const Data: TStream);
begin
  if Assigned(FOnLoadLibraryFunctions) then FOnLoadLibraryFunctions(Data);
end;

procedure TrwAppAdapter.LoadQBTemplates(const Data: TStream);
begin
  if Assigned(FOnLoadQBTemplates) then FOnLoadQBTemplates(Data);
end;

procedure TrwAppAdapter.LoadSQLConstants(const Data: TStream);
begin
  if Assigned(FOnLoadSQLConstants) then FOnLoadSQLConstants(Data);
end;

procedure TrwAppAdapter.StartEngineProgressBar(const Title: String; const MaxValue: Integer);
begin
  if Assigned(FOnStartEngineProgressBar) then FOnStartEngineProgressBar(Title, MaxValue);
end;

procedure TrwAppAdapter.TableDataRequest(const CachedTable: IrwCachedTable; const sbTable: IrwSBTable; const Params: array of TrwParamRec);
begin
  if Assigned(FOnTableDataRequest) then
    FOnTableDataRequest(CachedTable, sbTable, Params);
end;

procedure TrwAppAdapter.UnloadDataDictionary(const Data: TStream);
begin
  if Assigned(FOnUnloadDataDictionary) then FOnUnloadDataDictionary(Data);
end;

procedure TrwAppAdapter.UnloadLibraryComponents(const Data: TStream);
begin
  if Assigned(FOnUnloadLibraryComponents) then FOnUnloadLibraryComponents(Data);
end;

procedure TrwAppAdapter.UnloadLibraryFunctions(const Data: TStream);
begin
  if Assigned(FOnUnloadLibraryFunctions) then FOnUnloadLibraryFunctions(Data);
end;

procedure TrwAppAdapter.UnloadQBTemplates(const Data: TStream);
begin
  if Assigned(FOnUnloadQBTemplates) then FOnUnloadQBTemplates(Data);
end;

procedure TrwAppAdapter.UnloadSQLConstants(const Data: TStream);
begin
  if Assigned(FOnUnloadSQLConstants) then FOnUnloadSQLConstants(Data);
end;

procedure TrwAppAdapter.UpdateEngineProgressBar(const ProgressText: String; const CurrentValue: Integer);
begin
  if Assigned(FOnUpdateEngineProgressBar) then FOnUpdateEngineProgressBar(ProgressText, CurrentValue);
end;

function TrwAppAdapter.SubstituteValue(const Value: Variant; const SubstID: String): Variant;
begin
  Result := Value;
  if Assigned(FOnSubstituteValue) then FOnSubstituteValue(Result, SubstID);
end;


procedure TrwAppAdapter.AfterCacheTable(const CachedTableImage: IrwCachedTableImage);
begin
  if Assigned(FAfterCacheTable) then
    FAfterCacheTable(CachedTableImage);
end;

procedure TrwAppAdapter.BeforeCacheTable(const CachedTable: IrwCachedTable; const Params: array of TrwParamRec);
begin
  if Assigned(FBeforeCacheTable) then
    FBeforeCacheTable(CachedTable, Params);
end;

function TrwAppAdapter.CurrentDateTime: TDateTime;
begin
  Result := Now;
  if Assigned(FOnGetCurrentDateTime) then FOnGetCurrentDateTime(Result);
end;


procedure TrwAppAdapter.AfterClosePreview(const APreviewFrame: TObject);
begin
  if Assigned(FAfterClosePreview) then FAfterClosePreview(APreviewFrame);
end;


function TrwAppAdapter.GetLicencePassword(const LicenceID: String): String;
begin
  Result := '';
  if Assigned(FOnGetLicencePassword) then FOnGetLicencePassword(LicenceID, Result);
end;


function TrwAppAdapter.GetExternalFunctionAdditionalInfo(const AFunctionName: String): TrwExtFunctAddInfoRec;
begin
  if Assigned(FOnGetExternalFunctionAdditionalInfo) then FOnGetExternalFunctionAdditionalInfo(AFunctionName, Result);
end;

function TrwAppAdapter.SubstDefaultClassName(const ADefaultClassName: String): String;
begin
  Result := ADefaultClassName;
  if Assigned(FOnSubstDefaultClassName) then
    FOnSubstDefaultClassName(Result);
end;


procedure TrwAppAdapter.SetAppCustomData(var AData: Variant);
begin
  if Assigned(FOnSetAppCustomData) then FOnSetAppCustomData(AData);
end;

procedure TrwAppAdapter.EndDesignerProgressBar;
begin
  if Assigned(FOnEndDesignerProgressBar) then FOnEndDesignerProgressBar();
end;

function TrwAppAdapter.GetSecurityDescriptor: TrwDesignerSecurityRec;
begin
  Result.DataDictionary.Modify := False;
  Result.QBTemplates.Modify := False;
  Result.QBConstants.Modify := False;
  Result.LibComponents.Modify := False;
  Result.LibFunctions.Modify := False;
  Result.LicencedReports.Modify := False;

  if Assigned(FOnGetSecurityDescriptor) then FOnGetSecurityDescriptor(Result);
end;

function TrwAppAdapter.ReadReportParamsFromStream(const Data: TStream): TrwReportParamList;
begin
  SetLength(Result, 0);
  if Assigned(FOnReadReportParamsFromStream) then FOnReadReportParamsFromStream(Data, Result);
end;

procedure TrwAppAdapter.StartDesignerProgressBar(const Title: String; const MaxValue: Integer);
begin
  if Assigned(FOnStartDesignerProgressBar) then FOnStartDesignerProgressBar(Title, MaxValue);
end;

procedure TrwAppAdapter.UpdateDesignerProgressBar(const ProgressText: String; const CurrentValue: Integer);
begin
  if Assigned(FOnUpdateDesignerProgressBar) then FOnUpdateDesignerProgressBar(ProgressText, CurrentValue);
end;


procedure TrwAppAdapter.Activate(const RWEngine: IrwEngine);
begin
  FEngine := RWEngine;
  if Assigned(FOnActivate) then
    OnActivate(Self);
end;

procedure TrwAppAdapter.Deactivate;
begin
  FEngine := nil;
  if Assigned(FOnDeactivate) then
    OnDeactivate(Self);
end;

procedure TrwAppAdapter.WriteReportParamsToStream(const AParamList: PTrwReportParamList; const Data: TStream);
begin
  if Assigned(FOnWriteReportParamsToStream) then FOnWriteReportParamsToStream(AParamList^, Data);
end;

function TrwAppAdapter.DispatchCustomFunction(const FunctionName: String; const Params: Variant): Variant;
begin
  if Assigned(FOnDispatchCustomFunction) then FOnDispatchCustomFunction(FunctionName, Params, Result);
end;

function TrwAppAdapter.GetCustomControl(const Destination: String): TObject;
var
  Res: TForm;
begin
  Res := nil;
  if Assigned(FOnGetCustomControl) then FOnGetCustomControl(Destination, Res);
  Result := Res;
end;

procedure TrwAppAdapter.HandleException(var ErrorMessage: String);
begin
  try
    if Assigned(FOnException) then FOnException(ErrorMessage)
  except
  end;  
end;

procedure TrwAppAdapter.Terminate;
begin
  if Assigned(FOnTerminate) then FOnTerminate(Self);
end;

end.
