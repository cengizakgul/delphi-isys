unit rwConnectionInt;

interface

uses Windows, Messages, SysUtils, Forms, isBaseClasses, EvStreamUtils,
     evTransportShared, evTransportInterfaces, isThreadManager, isSocket,
     isBasicUtils, isLogFile, isErrorUtils, isSettings;

const
  METHOD_RESULT                             = 'Result';

  METHOD_RWENGINE_OpenReportFromStream      = 'rwEngine.OpenReportFromStream';
  METHOD_RWENGINE_OpenReportFromClass       = 'rwEngine.OpenReportFromClass';
  METHOD_RWENGINE_CloseActiveReport         = 'rwEngine.CloseActiveReport';
  METHOD_RWENGINE_RunActiveReport           = 'rwEngine.RunActiveReport';
  METHOD_RWENGINE_RunReport                 = 'rwEngine.RunReport';
  METHOD_RWENGINE_InitializeAppAdapter      = 'rwEngine.InitializeAppAdapter';
  METHOD_RWENGINE_RWAtoPDF                  = 'rwEngine.RWAtoPDF';
  METHOD_RWENGINE_CallAppAdapterFunction    = 'rwEngine.CallAppAdapterFunction';
  METHOD_RWENGINE_GetClassInfo              = 'rwEngine.GetClassInfo';
  METHOD_RWENGINE_ClassInheritsFrom         = 'rwEngine.ClassInheritsFrom';  
  METHOD_RWENGINE_ChangeRWAInfo             = 'rwEngine.ChangeRWAInfo';
  METHOD_RWENGINE_SetExternalModuleData     = 'rwEngine.SetExternalModuleData';
  METHOD_RWENGINE_OpenQueryBuilder          = 'rwEngine.OpenQueryBuilder';
  METHOD_RWENGINE_GetMainWindowHandle       = 'rwEngine.GetMainWindowHandle';
  METHOD_RWENGINE_RunQuery                  = 'rwEngine.RunQuery';
  METHOD_RWENGINE_QueryInfo                 = 'rwEngine.QueryInfo';
  METHOD_RWENGINE_OpenQueryBuilder2         = 'rwEngine.OpenQueryBuilder2';
  METHOD_RWENGINE_IsQueryBuilderFormClosed  = 'rwEngine.IsQueryBuilderFormClosed';
  METHOD_RWENGINE_PC_Run                    = 'rwEngine.ProcCtrl.Run';
  METHOD_RWENGINE_PC_Pause                  = 'rwEngine.ProcCtrl.Pause';
  METHOD_RWENGINE_PC_Stop                   = 'rwEngine.ProcCtrl.Stop';
  METHOD_RWENGINE_PC_GetVMRegister          = 'rwEngine.ProcCtrl.GetVMRegister';
  METHOD_RWENGINE_PC_GetState               = 'rwEngine.ProcCtrl.GetState';
  METHOD_RWENGINE_PC_CalcExpression         = 'rwEngine.ProcCtrl.CalcExpression';
  METHOD_RWENGINE_PC_GetVMCallStack         = 'rwEngine.ProcCtrl.GetVMCallStack';
  METHOD_RWENGINE_PC_SetBreakpoints         = 'rwEngine.ProcCtrl.SetBreakpoints';
  METHOD_RWENGINE_PC_OpenQueryBuilder       = 'rwEngine.ProcCtrl.OpenQueryBuilder';
  METHOD_RWENGINE_RS_GetData                = 'rwEngine.RepStruct.GetData';
  METHOD_RWENGINE_RS_GetStructureInfo       = 'rwEngine.RepStruct.GetStructureInfo';
  METHOD_RWENGINE_RS_Compile                = 'rwEngine.RepStruct.Compile';
  METHOD_RWENGINE_RS_PrepareForRunning      = 'rwEngine.RepStruct.PrepareForRunning';
  METHOD_RWENGINE_RP_GetCount               = 'rwEngine.RepParams.GetCount';
  METHOD_RWENGINE_RP_ParamByName            = 'rwEngine.ParamByName';
  METHOD_RWENGINE_RP_IndexOf                = 'rwEngine.IndexOf';
  METHOD_RWENGINE_RP_GetItems               = 'rwEngine.GetItems';
  METHOD_RWENGINE_RP_GetParamValue          = 'rwEngine.GetParamValue';
  METHOD_RWENGINE_RP_SetParamValue          = 'rwEngine.SetParamValue';
  METHOD_RWENGINE_RP_ParamsFromStream       = 'rwEngine.ParamsFromStream';
  METHOD_RWENGINE_RP_ParamsToStream         = 'rwEngine.ParamsToStream';
  METHOD_RWENGINE_RI_CloseOK                = 'rwEngine.RepInFrm.CloseOK';
  METHOD_RWENGINE_RI_CloseCancel            = 'rwEngine.RepInFrm.CloseCancel';
  METHOD_RWENGINE_RI_Show                   = 'rwEngine.RepInFrm.Show';
  METHOD_RWENGINE_RI_GetFormBounds          = 'rwEngine.RepInFrm.GetFormBounds';
  METHOD_RWENGINE_RI_SetFormBounds          = 'rwEngine.RepInFrm.SetFormBounds';

  METHOD_RWPREVIEW_QUERYINFO                = 'rwPreview.QueryInfo';
  METHOD_RWPREVIEW_PREVIEW                  = 'rwPreview.Preview';
  METHOD_RWPREVIEW_PRINT                    = 'rwPreview.Print';
  METHOD_RWPREVIEW_REPORTSINGROUP           = 'rwPreview.ReportsInGroup';
  METHOD_RWPREVIEW_CLEARGROUP               = 'rwPreview.ClearGroup';
  METHOD_RWPREVIEW_ADDTOGROUP               = 'rwPreview.AddToGroup';
  METHOD_RWPREVIEW_PREVIEWGROUP             = 'rwPreview.PreviewGroup';
  METHOD_RWPREVIEW_PRINTGROUP               = 'rwPreview.PrintGroup';
  METHOD_RWPREVIEW_MERGEGROUP               = 'rwPreview.MergeGroup';
  METHOD_RWPREVIEW_GETFORMBOUNDS            = 'rwPreview.GetFormBounds';
  METHOD_RWPREVIEW_SETFORMBOUNDS            = 'rwPreview.SetFormBounds';

  METHOD_RWDESIGNER_OpenDesigner              = 'rwDesigner.OpenDesigner';
  METHOD_RWDESIGNER_OpenQueryBuilder          = 'rwDesigner.OpenQueryBuilder';
  METHOD_RWDESIGNER_InitializeAppAdapter      = 'rwDesigner.InitializeAppAdapter';
  METHOD_RWDESIGNER_CallAppAdapterFunction    = 'rwDesigner.CallAppAdapterFunction';
  METHOD_RWDESIGNER_RunQuery                  = 'rwDesigner.RunQuery';
  METHOD_RWDESIGNER_QueryInfo                 = 'rwDesigner.QueryInfo';
  METHOD_RWDESIGNER_OpenDesigner2             = 'rwDesigner.OpenDesigner2';
  METHOD_RWDESIGNER_IsDesignerFormClosed      = 'rwDesigner.IsDesignerFormClosed';
  METHOD_RWDESIGNER_OpenQueryBuilder2         = 'rwDesigner.OpenQueryBuilder2';
  METHOD_RWDESIGNER_IsQueryBuilderFormClosed  = 'rwDesigner.IsQueryBuilderFormClosed';

type
  TrwReportParamType = (rwvUnknown, rwvInteger, rwvBoolean, rwvString,
                        rwvFloat, rwvDate, rwvArray, rwvPointer,
                        rwvCurrency, rwvVariant);

  TrwVMStateType = (rwVMSTStopped, rwVMSTPaused, rwVMSTRunning);

  TrwRunAction = (rwRARun, rwRATraceOver, rwRATraceIn);

  TrwReportResultFormatType = (rwRRTUnknown, rwRRTRWA, rwRRTASCII,
                               rwRRTPDF, rwRRTHTML, rwRRTXML, rwRRTXLS);


  TrwReportParam = record
    Name:  String;
    Value: Variant;
    RunTimeParam: Boolean;
    ParamType: TrwReportParamType;
  end;

  TrwRunReportSettings = record
    ReturnParamsBack: Boolean;
    Duplexing: Boolean;
    ShowInputForm: Boolean;
    Suspended: Boolean;
    ReportName: String;
  end;

  TrwVMRegisters = record
    RegA: Variant;
    RegB: Variant;
    RegF: Variant;
    CSIP: Variant;
  end;

  TrwVMExpressionResult = record
    ResultType: TrwReportParamType;
    Result:     Variant;
    ResultTypeText: String;
    ResultText: String;
  end;

  TrwReportStructureInfo = record
    ReportClassName: String;
    AncestorClassName: String;
    UsedLibComponents: String;
    Description: String;
  end;

  TrwPDFFileInfo = record
    Author: String;
    Creator: String;
    Subject: String;
    Title: String;
    Compression: Boolean;
    ProtectionOptions: Integer;
    OwnerPassword: String;
    UserPassword: String;
  end;

  TrwRWAFileInfo = record
    PreviewPassword: String;
    PrintPassword: String;
    Compression: Boolean;
  end;

  TrwProcessState = record
    VMState: TrwVMStateType;
    ExceptionText: String;
  end;

  TrwBounds = record
    Left:   Integer;
    Top:    Integer;
    Width:  Integer;
    Height: Integer;
  end;

  TrwDuplexingMode = (rdmDefault, rdmDuplex, rdmSimplex);

  TrwReportResultInfo = record
    Name:         String;
    FormatType:   TrwReportResultFormatType;
    SecuredMode:  Boolean;
    PreviewPwd:   String;
    PrintPwd:     String;
  end;

  TrwPrintJobInfo = record
    PrintJobName:           String;
    PrinterName:            String;
    TrayName:               String;
    OutBinNbr:              Integer;
    DuplexingMode:          TrwDuplexingMode;
    PrinterVerticalOffset:  Integer;
    PrinterHorizontalOffset:Integer;
    FirstPage:              Integer;
    LastPage:               Integer;
    Layers:                 String;
    Copies:                 Integer;
  end;


  TrwClassInfo = record
    ClassName:          String;
    AncestorClassName:  String;
    DisplayName:        String;
  end;


  TrwReportResult = record
    Data: IevDualStream;
    FormatType: TrwReportResultFormatType;
    PageInfo: Variant;
    MiscInfo: Variant;
  end;


  IrwReportParameters = interface
  ['{DB517F7B-2FB7-4934-B20C-6966FD2BE5DB}']
    function  GetCount: Integer;
    function  ParamByName(const AParamName: String): TrwReportParam;
    function  IndexOf(const AParamName: String): Integer;
    function  GetItems(const AIndex: Integer): TrwReportParam;
    function  GetParamValue(const AParamName: String): Variant;
    procedure SetParamValue(const AParamName: String; const AValue: Variant);
    procedure ParamsFromStream(const AParams: IevDualStream);
    function  ParamsToStream: IevDualStream;
    property  Count: Integer read GetCount;
    property  Items[const Index: Integer]: TrwReportParam read GetItems;
    property  ParamValue[const ParamName: String]: Variant read GetParamValue write SetParamValue;
  end;


  IrwReportInputForm = interface
  ['{C56AD854-BB4E-4088-966D-2F983453F384}']
    procedure CloseOK;
    procedure CloseCancel;
    function  Show(const AParentWndHandle: Integer; const ASetupMode: Boolean): Boolean;
    function  GetFormBounds: TrwBounds;
    procedure SetFormBounds(const ABounds: TrwBounds);
    property  FormBounds: TrwBounds read GetFormBounds write SetFormBounds;
  end;


  IrwReportStructure = interface
  ['{BB000EA9-E956-4BDA-9821-3C0FADBF839D}']
    function  GetReportParameters: IrwReportParameters;
    function  GetData: IevDualStream;
    function  GetReportInputForm: IrwReportInputForm;
    function  GetStructureInfo: TrwReportStructureInfo;
    procedure Compile;
    procedure PrepareForRunning;
    property  ReportParameters: IrwReportParameters read GetReportParameters;
    property  Data: IevDualStream read GetData;
    property  ReportInputForm: IrwReportInputForm read GetReportInputForm;
    property  StructureInfo: TrwReportStructureInfo read GetStructureInfo;
  end;

  IrwProcessController = interface
  ['{E18BF9BB-2843-4140-B7BA-71F76665D6C7}']
    procedure Run(const ARunAction: TrwRunAction);
    procedure Pause;
    procedure Stop;
    function  GetVMRegisters: TrwVMRegisters;
    function  GetState: TrwProcessState;
    function  CalcExpression(const AExpression: String; const ADebugSymbolInfo: String): TrwVMExpressionResult;
    function  GetVMCallStack: Variant;
    procedure SetBreakpoints(const ABreakpoints: Variant);
    procedure OpenQueryBuilder(const AQueryData: IevDualStream; const ADescriptionView: Boolean);
  end;


  IrwEngineApp = interface
  ['{48FC14E9-7582-4B14-AF8F-6890E5DB1D39}']
    procedure OpenReportFromStream(const AStream: IevDualStream);
    procedure OpenReportFromClass(const AClassName: String);
    function  GetActiveReport: IrwReportStructure;
    procedure CloseActiveReport;
    function  RunActiveReport(const ARunSettings: TrwRunReportSettings): TrwReportResult;
    function  RunReport(const AReportData: IevDualStream; const AReportName: String; out AReportResult: IevDualStream): Variant;

    function  GetProcessController: IrwProcessController;
    procedure InitializeAppAdapter(const AModuleName: String; const AAppCustomData: Variant);
    function  RWAtoPDF(const ARWAData: IevDualStream; const APDFInfo: TrwPDFFileInfo): IevDualStream;

    function  CallAppAdapterFunction(const AFunctionName: String; const AParams: Variant): Variant;
    function  GetClassInfo(const AClassName: String): TrwClassInfo;
    function  ClassInheritsFrom(const AClassName, AAncestorClassName: String): Boolean;
    function ChangeRWAInfo(const ARWAData: IevDualStream; const ACurrentPassword: String;
                            const ANewInfo: TrwRWAFileInfo): IevDualStream;
    procedure SetExternalModuleData(const AModuleName: String; const AData: IevDualStream);
    function  OpenQueryBuilder(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
    function  GetMainWindowHandle: Integer;
    function  RunQuery(const AQueryData: IevDualStream; const AParams: Variant): Variant;
    function  QueryInfo(const AItemName: String): String;
    function  OpenQueryBuilder2(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
    function  IsQueryBuilderFormClosed(out AQueryData: IevDualStream): Boolean;
    property  ProcessController: IrwProcessController read GetProcessController;
  end;


  IrwPreviewApp = interface
  ['{F7C24CFD-6F8C-4F69-96F8-FD61E500CBA7}']
    function  QueryInfo(const AItemName: String): String;
    procedure Preview(const AReportResult: IevDualStream; const AParentWindow: Integer);
    procedure Print(const AReportResult: IevDualStream; const APrintJobInfo: TrwPrintJobInfo);
    function  ReportsInGroup: Integer;
    procedure ClearGroup;
    procedure AddToGroup(const AReportResult: IevDualStream; const AReportResultInfo: TrwReportResultInfo;
                               const APrintJobInfo: TrwPrintJobInfo);
    procedure PreviewGroup(const AParentWindow: Integer);
    procedure PrintGroup(const APrintJobInfo: TrwPrintJobInfo);
    function  MergeGroup: IevDualStream;
    function  GetFormBounds: TrwBounds;
    procedure SetFormBounds(const ABounds: TrwBounds);
    property  FormBounds: TrwBounds read GetFormBounds write SetFormBounds;
  end;


  IrwDesignerApp = interface
  ['{3010A953-AF9F-4DB3-832C-A5F92C18BA35}']
    function  OpenDesigner(const AReportData: IevDualStream; const AReportName: String;
                           const ADefaultReportType: Integer): Boolean;
    function  OpenQueryBuilder(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
    procedure InitializeAppAdapter(const AModuleName: String; const AAppCustomData: Variant);
    function  CallAppAdapterFunction(const AFunctionName: String; const AParams: Variant): Variant;
    function  RunQuery(const AQueryData: IevDualStream; const AParams: Variant): Variant;
    function  QueryInfo(const AItemName: String): Variant;
    function  OpenDesigner2(const AReportData: IevDualStream; const AReportName: String;
                            const ADefaultReportType: Integer): Boolean;
    function  IsDesignerFormClosed(out AReportData: IevDualStream): Boolean;
    function  OpenQueryBuilder2(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
    function  IsQueryBuilderFormClosed(out AQueryData: IevDualStream): Boolean;
  end;


  // Common classes
  IrwServer = interface
  ['{FF76A67E-6163-4DCE-963D-7C11523184E0}']
  end;

  TrwServer = class(TevCustomTCPServer, IrwServer)
  private
    FWatchDogTsk: TTaskID;
    procedure TermWatchDog(const Params: TTaskParamList);
  protected
    procedure DoOnConstruction; override;
    procedure Close;
    procedure DoOnTerminate; virtual;
  public
    destructor Destroy; override;
  end;


  TrwServerDatagramDispatcher = class(TevDatagramDispatcher)
  private
    FServer: TrwServer;
  protected
    procedure DoInMainThread(const Params: TTaskParamList);
    procedure OnDisconnect(const ASessionID: TisGUID); override;
  end;

implementation

type
  PMethodProc = procedure (const ADatagram, AResult: IevDatagram);


{ TrwServer }

procedure TrwServer.Close;
var
  Tsk: TTaskID;
begin
  Tsk := FWatchDogTsk;
  FWatchDogTsk := 0;
  if Tsk <> 0 then
  begin
    GlobalThreadManager.TerminateTask(Tsk);
    GlobalThreadManager.WaitForTaskEnd(Tsk);
  end;
end;

destructor TrwServer.Destroy;
begin
  SetActive(False);
  Close;
  inherited;
end;

procedure TrwServer.DoOnConstruction;
var
  Port: Integer;
  sPortFile: IEvDualStream;
begin
  inherited;
  ((GetDatagramDispatcher as IisInterfacedObject).GetImplementation as TrwServerDatagramDispatcher).FServer := Self;
  (GetDatagramDispatcher.Transmitter.TCPCommunicator as IisAsyncTCPServer).ThreadPoolCapacity := 1;
  GetDatagramDispatcher.ThreadPoolCapacity := 2;

  SetIPAddress('127.0.0.1');
  SetPort('0');  // any random port
  SetActive(True);

  if AppSwitches.Value['server'] <> '' then
  begin
    Port := ((GetDatagramDispatcher.Transmitter.TCPCommunicator as IisSocketServer).
        ServerSockets[0] as IisSocket).GetLocalPort;

    sPortFile := TEvDualStreamHolder.CreateFromNewFile(AppSwitches.Value['server']);
    sPortFile.WriteLn(IntToStr(Port));
    sPortFile := nil;
  end;

  FWatchDogTsk := GlobalThreadManager.RunTask(TermWatchDog, Self, MakeTaskParams([]), 'Termination watch dog');
end;

procedure TrwServer.DoOnTerminate;
begin
end;

procedure TrwServer.TermWatchDog(const Params: TTaskParamList);
var
  OwnerAppID: String;
  OwnerAppMutex: THandle;
  S: IisSettings;
  SleepTime: Integer;
begin
  S := TisSettingsINI.Create(AppDir + 'rwSettings.ini');
  SleepTime := S.GetValue('Main\WDSleepTime', 5000);
  S := nil;

  // Loop here and watch for state of Owner Application
  OwnerAppID := AppSwitches.TryGetValue('owner', '');
  try
    while not CurrentThreadTaskTerminated do
    begin
      Snooze(SleepTime);

      if GetDatagramDispatcher.SessionCount = 0 then
      begin
        if GlobalLogFile <> nil then
          GlobalLogFile.AddEvent(etInformation, 'Server', 'Owner disconnected', 'Closing because owner application has disconnected.');
        break;     // Owner App has disconnected
      end;

      if OwnerAppID <> '' then
      begin
        OwnerAppMutex := OpenMutex(SYNCHRONIZE, False, PAnsiChar(OwnerAppID));
        try
          if OwnerAppMutex = 0 then
          begin
            if GlobalLogFile <> nil then
              GlobalLogFile.AddEvent(etInformation, 'Server', 'Owner is gone', 'Closing because owner application is terminated. Mutex name: ' + OwnerAppID);
            break;  // Owner App has died or terminated abnormaly
          end;
        finally
          CloseHandle(OwnerAppMutex);
        end;
      end;
    end;
  finally
    try
      DoOnTerminate;
    finally
      SetAppTerminatingFlag;
    end;
  end;
end;


{ TrwServerDatagramDispatcher }

procedure TrwServerDatagramDispatcher.DoInMainThread(
  const Params: TTaskParamList);
var
  ADatagram, AResult, Error: IevDatagram;
begin
  try
    ADatagram := IInterface(Params[0]) as IevDatagram;
    AResult := IInterface(Params[1]) as IevDatagram;
    PMethodProc(Pointer(Integer(Params[2])))(ADatagram, AResult);
  except
    on E: Exception do
    begin
      try
        Error := CreateErrorResponseFor(ADatagram.Header, E);
        (AResult as IisInterfacedObject).AsStream := (Error as IisInterfacedObject).AsStream;
      except
        on E: Exception do
          GlobalLogFile.AddEvent(etError, 'Communicator', E.Message, GetStack(E));
      end;
    end;
  end;
end;

procedure TrwServerDatagramDispatcher.OnDisconnect(
  const ASessionID: TisGUID);
begin
  inherited;
  FServer.Close;
end;

end.
