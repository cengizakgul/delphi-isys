// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SEncryptionRoutines;

interface

uses
  SysUtils, Classes, ISBasicUtils, isSSL, Windows, OnGuard, OgUtil, cHash;


const
  DefaultCipherKey = 'd98frtj454n4jkgr';
  MIN_KEY_LENGTH = 16;
  BlocksPerBuf = 512;
  SSLCLIB_DLL_name = 'libeay32.dll'; 

procedure EncryptMemoryStream(const InStream: TStream; const OutStream: TStream; Key: string = DefaultCipherKey);
procedure DecryptMemoryStream(const InStream: TStream; const OutStream: TStream; Key: string = DefaultCipherKey);

function EncryptString(const InString: string; Key: string = DefaultCipherKey): string;
function DecryptString(const InString: string; Key: string = DefaultCipherKey): string;

function EncryptHexString(const InString: string; Key: string = DefaultCipherKey): string;
function DecryptHexString(const InString: string; Key: string = DefaultCipherKey): string;

procedure EnsureKeyLength(var Key: string);
function GetComputerCipherKey: string;
function EncryptStringLocaly(const InString: string): string;
function DecryptStringLocaly(const InString: string): string; overload;
function DecryptStringLocaly(const InString: string; const ErrorResult: String): string; overload;

function CalcMD5(const AStream: TStream): String;

const
  OPENSSL_BF_ROUNDS = 16;

type
  BF_KEY = packed record
	  P: array [1..OPENSSL_BF_ROUNDS+2] of Cardinal;
	  S: array [1..4*256] of Cardinal;
	end;
  PBF_KEY = ^BF_KEY;

var
  BF_set_key: procedure(bfkey: PBF_KEY; len: Integer; const data: Pointer); cdecl = nil;
  BF_crypt: procedure(const pin, pout: Pointer; const key: PBF_KEY; const enc: Integer); cdecl = nil;

implementation

uses isThreadManager;

function GetComputerCipherKey: string;
var
  i: Integer;
begin
  i := CreateMachineID([midUser, midSystem, midNetwork, midDrives]){GenerateMachineModifierPrim};
  Result := BufferToHex(i, SizeOf(i));
  EnsureKeyLength(Result);
end;

function EncryptStringLocaly(const InString: string): string;
begin
  Result := EncryptString(InString, GetComputerCipherKey);
end;

function DecryptStringLocaly(const InString: string): string;
begin
  Result := DecryptString(InString, GetComputerCipherKey);
end;

function DecryptStringLocaly(const InString: string; const ErrorResult: String): string; overload;
begin
  try
    if InString <> '' then
      Result := DecryptStringLocaly(InString)
    else
      Result := ErrorResult;
  except
    Result := ErrorResult;
  end;
end;

procedure EncryptMemoryStream(const InStream: TStream; const OutStream: TStream; Key: string = DefaultCipherKey);
var
  bfkey: BF_KEY;
  Count: Longint;
  ThisBlockSize, BufSize, I, N: Integer;
  Buf: Pointer;
  BufOut: Int64;
  P: PByte;
  LastBuf: Boolean;
  l: SmallInt;
begin
  EnsureKeyLength(Key);
  InStream.Seek(0, soFromBeginning);
  OutStream.Seek(0, soFromBeginning);
  OutStream.Size := InStream.Size;
  l := 1;  // Encryption version
  OutStream.WriteBuffer(l, SizeOf(l));
  Assert(Assigned(BF_set_key) and Assigned(BF_crypt), SSLCLIB_DLL_name + ' was not loaded');
  BF_set_key(@bfkey, Length(key), PChar(key + #0));

  P := nil; // Suppresses superfluous compiler warning.
  Count := 0; // Ditto.
  ThisBlockSize := SizeOf(Int64);
  BufSize := ThisBlockSize * BlocksPerBuf;
  GetMem(Buf, BufSize);
  while True do begin
    Count := InStream.Read(Buf^, BufSize);
    P := Buf;
    LastBuf := Count < BufSize;
    if LastBuf then N := Count div ThisBlockSize else N := BlocksPerBuf;
    for I := 1 to N do begin
      BF_crypt(P, @BufOut, @bfkey, BF_ENCRYPT);
      MoveMemory(P, @BufOut, ThisBlockSize);
      Inc(P, ThisBlockSize);
    end;
    if LastBuf then Break;
    OutStream.Write(Buf^, Count);
  end;
  // We're at the end of the data in a not-completely-full-buffer (or, in the
  // case of Plaintext.Size mod BufSize = 0, at the beginning of an empty
  // buffer, which is just a special case of the former). Now we use the last
  // byte of the current block to give the number of extra padding bytes.
  // This will be a number from 1..ThisBlockSize. Specifically, if the
  // Plaintext length is an exact multiple of ThisBlockSize, the number of
  // extra padding bytes will be ThisBlockSize, i.e. the entire final block
  // is junk. In any other case, the last block has at least some ciphertext.
  Inc(P, ThisBlockSize - 1);
  P^ := Byte(ThisBlockSize - Count mod ThisBlockSize);
  Inc(Count, P^);
  Dec(P, ThisBlockSize - 1);
  BF_crypt(P, @BufOut, @bfkey, BF_ENCRYPT);
  MoveMemory(P, @BufOut, ThisBlockSize);
  OutStream.Write(Buf^, Count);
  FreeMem(Buf);
end;

procedure DecryptMemoryStream(const InStream: TStream; const OutStream: TStream; Key: string = DefaultCipherKey);
var
  bfkey: BF_KEY;
  Count: Longint;
  ThisBlockSize, BufSize, I, J: Integer;
  Buf: Pointer;
  BufOut: array [0..7] of Byte;
  P: PByte;
  l: SmallInt;
begin
  EnsureKeyLength(Key);
  InStream.Seek(0, soFromBeginning);
  InStream.ReadBuffer(l, SizeOf(l));
  OutStream.Seek(0, soFromBeginning);
  if InStream.Size > 10 then
    OutStream.Size := InStream.Size - 10;
  Assert(Assigned(BF_set_key) and Assigned(BF_crypt), SSLCLIB_DLL_name + ' was not loaded');
  BF_set_key(@bfkey, Length(key), PChar(key + #0));

  ThisBlockSize := SizeOf(Int64);
  Count := InStream.Size - InStream.Position;
  if (Count = 0) or (Count mod ThisBlockSize <> 0) then
    Exit;
//    raise EisException.CreateFmt('Ciphertext length is not a multiple of %d.', [ThisBlockSize]);
  BufSize := ThisBlockSize * BlocksPerBuf;
  GetMem(Buf, BufSize);
  for I := 1 to Count div BufSize do begin
    InStream.Read(Buf^, BufSize);
    P := Buf;
    for J := 1 to BlocksPerBuf do begin
      BF_crypt(P, @BufOut, @bfkey, BF_DECRYPT);
      MoveMemory(P, @BufOut, ThisBlockSize);
      Inc(P, ThisBlockSize);
    end;
    OutStream.Write(Buf^, BufSize);
  end;
  Count := Count mod BufSize;
  InStream.Read(Buf^, Count);
  P := Buf;
  for J := 1 to Count div ThisBlockSize do begin
    BF_crypt(P, @BufOut, @bfkey, BF_DECRYPT);
    MoveMemory(P, @BufOut, ThisBlockSize);
    Inc(P, ThisBlockSize);
  end;
  Dec(P);
  OutStream.Write(Buf^, Count - P^);
  FreeMem(Buf);
end;

function EncryptString(const InString: string; Key: string = DefaultCipherKey): string;
var
  InStream, OutStream: TStringStream;
begin
  InStream := TStringStream.Create(InString);
  OutStream := TStringStream.Create('');
  try
    EncryptMemoryStream(InStream, OutStream, Key);
    Result := OutStream.DataString;
  finally
    OutStream.Free;
    InStream.Free;
  end;
end;

function DecryptString(const InString: string; Key: string = DefaultCipherKey): string;
var
  InStream, OutStream: TStringStream;
begin
  InStream := TStringStream.Create(InString);
  OutStream := TStringStream.Create('');
  try
    DecryptMemoryStream(InStream, OutStream, Key);
    Result := OutStream.DataString;
  finally
    OutStream.Free;
    InStream.Free;
  end;
end;

procedure EnsureKeyLength(var Key: string);
var
  i: Integer;
  iXor: Byte;
  s, AKey: string;                
begin
  CheckCondition((Length(Key)>0), 'Key cannot be empty!');
  iXor := 1;
  AKey := Key;
  while Length(Key) < MIN_KEY_LENGTH do
  begin
    s := AKey;
    for i := 1 to Length(s) do
      s[i] := Char(Byte(s[i]) xor iXor);
    Inc(iXor);
    Key := Key + s;
  end;
end;

function EncryptHexString(const InString: string; Key: string = DefaultCipherKey): string;
var
  P: PChar;
  s1: string;
begin
  GetMem(P, 4096);
  try
    s1 := EncryptString(InString, DefaultCipherKey);
    BinToHex(PChar(s1 + #0), P, Length(s1));
    SetString(Result, P, Length(s1) * 2);
  finally
    FreeMem(P);
  end;
end;

function DecryptHexString(const InString: string; Key: string = DefaultCipherKey): string;
var
  P: PChar;
  s: string;
begin
  GetMem(P, Length(InString));
  try
    HexToBin(PChar(LowerCase(InString) + #0), P, Length(InString));
    SetString(s, P, Length(InString) div 2);
    Result := DecryptString(s, DefaultCipherKey);
  finally
    FreeMem(P);
  end;
end;


function CalcMD5(const AStream: TStream): String;
const
  BufSize = 64 * 1024;
var
  i, j: Integer;
  Buf, P : PByte;
  ReadBufSize: Integer;
  Res: T128BitDigest;
begin
  Buf := AllocMem(BufSize);
  try
    MD5InitDigest(Res);
    AStream.Position := 0;
    while AStream.Position < AStream.Size do
    begin
      ReadBufSize := AStream.Read(Buf^, BufSize);

      P := Buf;
      if ReadBufSize <= 0 then
        i := 0
      else
        i := ReadBufSize;
      j := (i div 64) * 64;

      MD5Buf (Res, P^, j);
      Inc (P, j);
      Dec (i, j);

      if (i > 0) or (AStream.Position = AStream.Size) then
        MD5FinalBuf (Res, P^, i, AStream.Size);

      AbortIfCurrentThreadTaskTerminated;
    end;

  finally
    FreeMem(Buf, BufSize);
  end;

  Result := UpperCase(MD5DigestToHex(Res));
end;


var
  hIdCrypto : Integer = 0;
initialization
  hIdCrypto := LoadLibrary(SSLCLIB_DLL_name);
  if hIdCrypto <> 0 then
  begin
    @BF_set_key := GetProcAddress(hIdCrypto, 'BF_set_key');
    @BF_crypt := GetProcAddress(hIdCrypto, 'BF_ecb_encrypt');
  end;

finalization
  if hIdCrypto <> 0 then
    FreeLibrary(hIdCrypto);

end.
