unit ISDataAccessComponents;

interface

uses
  ISKbmMemDataSet, Classes, DB, SysUtils;

type
  TListOfValuesStringList = class(TStringList)
  private
    ExcludeNulls: Boolean;
    Field: TField;
    s: string;
    procedure IteratorCallback(const Version, Index, Total: Longint);
  end;

  TISBasicClientDataSet = class(TISKbmMemDataSet)
  private
    FFilter: string;
    FFiltered: Boolean;
    FUserFilter: string;
    FUserFiltered: Boolean;
    FCancel: Boolean;
    procedure SetUserFilter(const Value: string);
    procedure SetUserFiltered(const Value: Boolean);
    procedure SetIFilter(const Value: string);
    procedure SetIFiltered(const Value: Boolean);
    procedure SetInheritedFilter;
  protected
    FLookupFields: TStringList;
    FCalcFields: TStringlist;
    procedure DoAfterPost; override;
    procedure DoAfterOpen; override;
    procedure DoAfterCancel; override;
    procedure DoBeforeCancel; override;
    procedure DoAfterClose; override;
    procedure DoBeforeOpen; override;
    procedure DataEvent(Event: TDataEvent; Info: Longint); override;
    procedure WriteState(Writer: TWriter); override;
    procedure DoInheritedCalcFields;
    procedure DoOnCalcFields; override;
  public
    procedure CloneCursor(Source :TISKbmMemDataSet; Reset: Boolean; KeepSettings: Boolean = False); override;
    function  GetFieldValueList(const FieldName: string; const ExcludeNulls: Boolean = False): TStringList;
    property  UserFilter: string read FUserFilter write SetUserFilter;
    property  UserFiltered: Boolean read FUserFiltered write SetUserFiltered default False;
    procedure CreateFields; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Filter: string read FFilter write SetIFilter;
    property Filtered: Boolean read FFiltered write SetIFiltered default False;
  end;

implementation

{ TISBasicClientDataSet }

procedure TISBasicClientDataSet.CreateFields;
begin
  inherited;
end;

procedure TISBasicClientDataSet.DataEvent(Event: TDataEvent; Info: Integer);
begin
  if not FCancel or
     (Event <> deUpdateState) or
     not ControlsDisabled then
    inherited;
end;

procedure TISBasicClientDataSet.DoAfterCancel;
begin
  inherited;
  FCancel := False;
end;

procedure TISBasicClientDataSet.DoAfterOpen;
begin
  inherited;
  FCancel := False;
end;

procedure TISBasicClientDataSet.DoAfterPost;
begin
  inherited;
  FCancel := False;
end;

procedure TISBasicClientDataSet.DoBeforeCancel;
begin
  inherited;
  FCancel := True;
end;

procedure TISBasicClientDataSet.SetUserFilter(const Value: string);
begin
  if FUserFilter <> Value then
  begin
    FUserFilter := Value;
    SetInheritedFilter;
  end;
end;

procedure TISBasicClientDataSet.SetUserFiltered(const Value: Boolean);
begin
  if FUserFiltered <> Value then
  begin
    FUserFiltered := Value;
    SetInheritedFilter;
  end;
end;

procedure TISBasicClientDataSet.SetIFilter(const Value: string);
begin
  if FFilter <> Value then
  begin
    FFilter := Value;
    SetInheritedFilter;
  end;
end;

procedure TISBasicClientDataSet.SetIFiltered(const Value: Boolean);
begin
  if FFiltered <> Value then
  begin
    FFiltered := Value;
    SetInheritedFilter;
  end;
end;

procedure TISBasicClientDataSet.SetInheritedFilter;
var
  s: string;
begin
  if (Trim(Filter) <> '') and Filtered then
    s := '(' + Filter + ')'
  else
    s := '';
  if (Trim(UserFilter) <> '') and UserFiltered then
    if s <> '' then
      s := s + ' and (' + UserFilter + ')'
    else
      s := UserFilter;
  inherited Filtered := Filtered or UserFiltered;
  inherited Filter := s;
end;

procedure TISBasicClientDataSet.CloneCursor(Source: TISKbmMemDataSet;
  Reset, KeepSettings: Boolean);
begin
  inherited;
  if Reset then
  begin
    FFilter := '';
    FFiltered := False;
    FUserFilter := '';
    FUserFiltered := False;
  end
  else if not KeepSettings
  and (Source is TISBasicClientDataSet) then
  begin
    FFilter := TISBasicClientDataSet(Source).FFilter;
    FFiltered := TISBasicClientDataSet(Source).FFiltered;
    FUserFilter := TISBasicClientDataSet(Source).FUserFilter;
    FUserFiltered := TISBasicClientDataSet(Source).FUserFiltered;
  end;
end;

function TISBasicClientDataSet.GetFieldValueList(
  const FieldName: string; const ExcludeNulls: Boolean = False): TStringList;
begin
  Result := TListOfValuesStringList.Create;
  TListOfValuesStringList(Result).Field := FieldByName(FieldName);
  TListOfValuesStringList(Result).ExcludeNulls := ExcludeNulls;
  IterateRecords(TListOfValuesStringList(Result).IteratorCallback, True, True, False, False);
end;

constructor TISBasicClientDataSet.Create(AOwner: TComponent);
begin
  inherited;
  FLookupFields := TStringList.Create;
  FLookupFields.Sorted := True;
  FLookupFields.Duplicates := dupIgnore;
  FCalcFields := TStringlist.Create;
  FCalcFields.Sorted := True;
  FCalcFields.Duplicates := dupIgnore;
end;

procedure TISBasicClientDataSet.DoAfterClose;
var
  i: Integer;
begin
  inherited;
  for i := 0 to Pred(FLookupFields.Count) do
    FieldByName(FLookupFields[i]).FieldKind := fkLookup;
  for i := 0 to Pred(FCalcFields.Count) do
    FieldByName(FCalcFields[i]).FieldKind := fkCalculated;
  FLookupFields.Clear;
  FCalcFields.Clear;
end;

destructor TISBasicClientDataSet.Destroy;
begin
  FLookupFields.Free;
  FCalcFields.Free;
  inherited;
end;

procedure TISBasicClientDataSet.WriteState(Writer: TWriter);
  procedure RestoreFieldKinds;
  var
    i: Integer;
  begin
    for i := 0 to Pred(FLookupFields.Count) do
      if FieldByName( FLookupFields[i] ).FieldKind = fkInternalCalc then
        FieldByName( FLookupFields[i] ).FieldKind := fkLookup;
    for i := 0 to Pred(FCalcFields.Count) do
      if FieldByName( FCalcFields[i] ).FieldKind = fkInternalCalc then
        FieldByName( FCalcFields[i] ).FieldKind := fkCalculated;
  end;
begin
  RestoreFieldKinds;
  inherited;
end;

procedure TISBasicClientDataSet.DoBeforeOpen;
var
  i: Integer;
begin
  FLookupFields.Clear;
  FCalcFields.Clear;
  for i := 0 to Fields.Count-1 do
    if Fields[i].FieldKind = fkLookup then
    begin
      FLookupFields.Add(Fields[i].FieldName);
      Fields[i].FieldKind := fkInternalCalc;
    end
    else if Fields[i].FieldKind = fkCalculated then
    begin
      FCalcFields.Add(Fields[i].FieldName);
      Fields[i].FieldKind := fkInternalCalc;
    end;
  inherited;
end;

procedure TISBasicClientDataSet.DoOnCalcFields;
var
  i: Integer;
begin
  for i := 0 to Pred(FieldCount) do
    with Fields[i] do
      if (FieldKind = fkInternalCalc) and
         (FLookupFields.IndexOf(FieldName) >= 0) then
        if Assigned(LookupDataSet) then
          Value := LookupDataSet.Lookup(LookupKeyFields, FieldValues[KeyFields], LookupResultField)
        else
          Clear;
  inherited;
end;

procedure TISBasicClientDataSet.DoInheritedCalcFields;
begin
  inherited DoOnCalcFields;
end;

{ TListOfValuesStringList }

procedure TListOfValuesStringList.IteratorCallback(const Version, Index,
  Total: Integer);
begin
  if not ExcludeNulls or not Field.IsNull then
  begin
    s := Field.AsString;
    if IndexOf(s) = -1 then
      Append(s);
  end;
end;

end.
