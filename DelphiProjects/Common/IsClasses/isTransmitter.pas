unit isTransmitter;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvStreamUtils, isStreamTCP, isStreamUDP, isThreadManager,
     isSocket, isBasicUtils, isLogFile;

type
    // In/Out data queue

    IisStreamQueueItem = interface
    ['{90D9ECBB-4E92-4D15-866E-35900E49B1D3}']
      function GetSessionID: TisGUID;
      function GetStream: IEvDualStream;
      function GetHeader: String;
      function GetTransmissionTime: Cardinal;
      property SessionID: TisGUID read GetSessionID;
      property Stream: IEvDualStream read GetStream;
      property Header: String read GetHeader;
      property TransmissionTime: Cardinal read GetTransmissionTime;
    end;


    TisStreamQueueItem = class(TisInterfacedObject, IisStreamQueueItem)
    private
      FSessionID: TisGUID;
      FHeader: String;
      FStream: IEvDualStream;
      FTransmissionTime: Cardinal;
    protected
      function  GetSessionID: TisGUID;
      function  GetStream: IEvDualStream;
      function  GetHeader: String;
      function  GetTransmissionTime: Cardinal;
    public
      constructor Create(const ASessionID: TisGUID; const AHeader: String; const AStream: IEvDualStream;
                         const ATransmissionTime: Cardinal);
    end;


    TisStreamQueue = class
    private
      FList: IInterfaceList;
      function    GetItem(AIndex: Integer): IisStreamQueueItem;
    public
      procedure   AddItem(const AStreamQueueItem: IisStreamQueueItem; const AFirst: Boolean = False);
      procedure   RemoveItem(const AStreamQueueItem: IisStreamQueueItem);
      function    GetFirstItem: IisStreamQueueItem;
      function    Count: Integer;
      procedure   Lock;
      procedure   UnLock;
      property    Item[Index: Integer]: IisStreamQueueItem read GetItem; default;
      constructor Create;
    end;


    IisTransmitterCallbacks = interface
    ['{69988002-AE4B-4074-A47E-56B4AC85843B}']
      procedure BeforeQueueIncomingData(const ASessionID: TisGUID; const AHeader: String;
                                        const AStream: IEvDualStream; const ATransmissionTime: Cardinal; var Handled: Boolean);
      procedure OnIncomingHeader(const ASessionID: TisGUID; const AHeader: String; var Accept: Boolean);
      procedure OnIncomingData(const ASessionID: TisGUID; const AHeader: String; const AStream: IEvDualStream;
                               const ATransmissionTime: Cardinal);
      procedure OnDisconnect(const ASessionID: TisGUID);
      procedure OnDiscardItems(const AItems: IisList);   // list of IisStreamQueueItem
    end;


    //

    TisStreamTransmitter = class;

    TisQueueHandlerThread = class(TReactiveThread)
    private
      FOwner: TisStreamTransmitter;
      FDirection: Char;
    protected
      procedure DoWork; override;
    public
      constructor Create(AOwner: TisStreamTransmitter; ADirection: Char);
    end;


    // Stream Transmitter

    IisStreamTransmitter = interface
    ['{6F6F6E30-CFBB-41D4-BE32-0CB9754625E8}']
      procedure Send(const ASessionID: TisGUID; const AHeader: String; const AData: IEvDualStream);
      function  Receive(out ASessionID: TisGUID; out AHeader: String; out AData: IEvDualStream;
                        out ATransmissionTime: Cardinal): Boolean;
      procedure SetCallbacks(const AValue: IisTransmitterCallbacks);
      function  GetCallbacks: IisTransmitterCallbacks;
      procedure QueueStatus(out ReceivingQueueSize, SendingQueueSize: Integer);
      property  Callbacks: IisTransmitterCallbacks read GetCallbacks write SetCallbacks;
    end;


    TisStreamTransmitter = class(TisInterfacedObject, IisStreamTransmitter)
    private
      FQueueFloodLock: IisLock;
      FIncomingStreamQueue: TisStreamQueue;
      FOutgoingStreamQueue: TisStreamQueue;
      FIncomingQueueHandlerThread: TisQueueHandlerThread;
      FOutgoingQueueHandlerThread: TisQueueHandlerThread;
      FCallbacks: IisTransmitterCallbacks;
    protected
      procedure SetCallbacks(const AValue: IisTransmitterCallbacks);
      function  GetCallbacks: IisTransmitterCallbacks;
      procedure DoOnConstruction; override;
      procedure DoSendData; virtual; abstract;
      procedure DoOnNewOutData(const AQueueItem: IisStreamQueueItem); virtual; abstract;
      procedure DoOnNewInData(const AQueueItem: IisStreamQueueItem); virtual; abstract;
      procedure AddToReceiveQueue(const  ASessionID: TisGUID; const AHeader: String; const AData: IEvDualStream; const ATransmissionTime: Cardinal);
      procedure QueueStatus(out ReceivingQueueSize, SendingQueueSize: Integer);
      procedure Send(const ASessionID: TisGUID; const AHeader: String; const AData: IEvDualStream);
      function  Receive(out ASessionID: TisGUID; out AHeader: String; out AData: IEvDualStream; out ATransmissionTime: Cardinal): Boolean;
      property  Callbacks: IisTransmitterCallbacks read GetCallbacks write SetCallbacks;
    public
      destructor  Destroy; override;
    end;


    // TCP transmitter (one session - many connections)

    TisTCPStreamTransmitter = class;


    IisTCPStreamTransmitter = interface(IisStreamTransmitter)
    ['{BE26A298-6F29-4876-8A4A-80EFF5AB74B3}']
      function  GetTCPCommunicator: IisTCPStreamCommunicator;
      procedure SetTCPCommunicator(const ATCPCommunicator: IisTCPStreamCommunicator);
      procedure CheckSendingQueue;
      function  IsActive: Boolean;
      property  TCPCommunicator: IisTCPStreamCommunicator read GetTCPCommunicator write SetTCPCommunicator;
    end;


    TisTCPStreamTransmitter = class(TisStreamTransmitter, IisTCPStreamTransmitter)
    private
      FTCPCommunicator: IisTCPStreamCommunicator;
    protected
      function  GetTCPCommunicator: IisTCPStreamCommunicator;
      procedure SetTCPCommunicator(const ATCPCommunicator: IisTCPStreamCommunicator);
      procedure DoOnNewInData(const AQueueItem: IisStreamQueueItem); override;
      procedure DoOnNewOutData(const AQueueItem: IisStreamQueueItem); override;
      procedure DoOnConnect(const AConnection: IisTCPStreamAsyncConnection); virtual;
      procedure DoOnDisconnect(const AConnection: IisTCPStreamAsyncConnection); virtual;
      procedure DoOnHeaderReceived(const AConnection: IisTCPStreamAsyncConnection; const AHeader: String; var Accept: Boolean); virtual;
      procedure DoOnReceiveComplete(const AConnection: IisTCPStreamAsyncConnection;
                                    const AHeader: String; const AStream: IEvDualStream;
                                    const ATransmissionTime: Cardinal); virtual;
      procedure DoOnSendComplete(const AConnection: IisTCPStreamAsyncConnection; const AHeader: String; const AStream: IEvDualStream; const AFailed: Boolean); virtual;
      procedure CheckSendingQueue;
      procedure CheckReceivingQueue;
      procedure DoSendData; override;
      function  IsActive: Boolean;            // Should be always True. False means data queue is malfunctioning
    public
      destructor  Destroy; override;
    end;



    // UDP Transmitter

    TisUDPStreamTransmitter = class;

    IisUDPStreamTransmitter = interface(IisStreamTransmitter)
    ['{4F006DB1-96CF-4320-8AA6-9455D5881F76}']
      function  GetUDPCommunicator: IisUDPStreamCommunicator;
      procedure SetUDPCommunicator(const AUDPCommunicator: IisUDPStreamCommunicator);
      procedure CheckSendingQueue;
      function  IsActive: Boolean;
      property  UDPCommunicator: IisUDPStreamCommunicator read GetUDPCommunicator write SetUDPCommunicator;
    end;


    TisUDPStreamTransmitter = class(TisStreamTransmitter, IisUDPStreamTransmitter)
    private
      FUDPCommunicator: IisUDPStreamCommunicator;
    protected
      function  GetUDPCommunicator: IisUDPStreamCommunicator;
      procedure SetUDPCommunicator(const AUDPCommunicator: IisUDPStreamCommunicator);
      procedure DoOnNewInData(const AQueueItem: IisStreamQueueItem); override;
      procedure DoOnNewOutData(const AQueueItem: IisStreamQueueItem); override;
      procedure DoOnReceiveComplete(const AConnection: IisAsyncUDPStreamSocket;
                                    const AStream: IEvDualStream; const ATransmissionTime: Cardinal); virtual;
      procedure DoOnSendComplete(const AConnection: IisAsyncUDPStreamSocket; const AStream: IEvDualStream); virtual;
      procedure CheckSendingQueue;
      procedure CheckReceivingQueue;
      procedure DoSendData; override;
      function  IsActive: Boolean;            // Should be always True. False means data queue is malfunctioning
    public
      destructor  Destroy; override;
    end;


implementation

const cQueueCapacity = 100000;  // maximum items in the queue, just to keep system up if queue is flooded


{ TisStreamQueueItem }

constructor TisStreamQueueItem.Create(const ASessionID: TisGUID; const AHeader: String;
  const AStream: IEvDualStream; const ATransmissionTime: Cardinal);
begin
  FSessionID := ASessionID;
  FHeader := AHeader;
  FStream := AStream;
  FTransmissionTime := ATransmissionTime;
end;

function TisStreamQueueItem.GetHeader: String;
begin
  Result := FHeader;
end;

function TisStreamQueueItem.GetSessionID: TisGUID;
begin
  Result := FSessionID;
end;

function TisStreamQueueItem.GetStream: IEvDualStream;
begin
  Result := FStream;
end;


function TisStreamQueueItem.GetTransmissionTime: Cardinal;
begin
  Result := FTransmissionTime;
end;

{ TisStreamQueue }

constructor TisStreamQueue.Create;
begin
  FList := TInterfaceList.Create;
end;

procedure TisStreamQueue.AddItem(const AStreamQueueItem: IisStreamQueueItem; const AFirst: Boolean = False);
begin
  if AFirst then
    FList.Insert(0, AStreamQueueItem as IisStreamQueueItem)  
  else
    FList.Add(AStreamQueueItem as IisStreamQueueItem);
end;

function TisStreamQueue.GetFirstItem: IisStreamQueueItem;
begin
  Lock;
  try
    if Count > 0 then
    begin
      Result := GetItem(0);
      RemoveItem(Result);
    end
    else
      Result := nil;
  finally
    Unlock;
  end;
end;


procedure TisStreamQueue.RemoveItem(const AStreamQueueItem: IisStreamQueueItem);
begin
  if FList.Remove(AStreamQueueItem) = -1 then
    Assert(False);
end;

function TisStreamQueue.Count: Integer;
begin
  Result := FList.Count;
end;

procedure TisStreamQueue.Lock;
begin
  FList.Lock;
end;

procedure TisStreamQueue.UnLock;
begin
  FList.UnLock;
end;

function TisStreamQueue.GetItem(AIndex: Integer): IisStreamQueueItem;
begin
  Result := FList[AIndex] as IisStreamQueueItem;
end;

{ TisStreamTransmitter }

destructor TisStreamTransmitter.Destroy;
begin
  FreeAndNil(FIncomingStreamQueue);
  FreeAndNil(FOutgoingStreamQueue);
  FreeAndNil(FIncomingQueueHandlerThread);
  FreeAndNil(FOutgoingQueueHandlerThread);
  inherited;
end;

function TisStreamTransmitter.Receive(out ASessionID: TisGUID; out AHeader: String;
  out AData: IEvDualStream; out ATransmissionTime: Cardinal): Boolean;
var
  QueueItem: IisStreamQueueItem;
begin
  QueueItem := FIncomingStreamQueue.GetFirstItem;
  Result := Assigned(QueueItem);

  if Result then
  begin
    ASessionID := QueueItem.SessionID;
    AData := QueueItem.Stream;
    AHeader := QueueItem.Header;
    ATransmissionTime := QueueItem.TransmissionTime;
  end;
end;

procedure TisStreamTransmitter.Send(const ASessionID: TisGUID; const AHeader: String; const AData: IEvDualStream);
var
  QueueItem: IisStreamQueueItem;
begin
  QueueItem := TisStreamQueueItem.Create(ASessionID, AHeader, AData, 0);

  FQueueFloodLock.Lock;
  try
    // check for queue flood
    if FOutgoingStreamQueue.Count >= cQueueCapacity then
       while FOutgoingStreamQueue.Count >= cQueueCapacity do
         Sleep(10);

    FOutgoingStreamQueue.AddItem(QueueItem);     
  finally
    FQueueFloodLock.Unlock;
  end;

  DoOnNewOutData(QueueItem);
end;

procedure TisStreamTransmitter.AddToReceiveQueue(const ASessionID: TisGUID; const AHeader: String;
  const AData: IEvDualStream; const ATransmissionTime: Cardinal);
var
  QueueItem: IisStreamQueueItem;
begin
  QueueItem := TisStreamQueueItem.Create(ASessionID, AHeader, AData, ATransmissionTime);
  FIncomingStreamQueue.AddItem(QueueItem);
  DoOnNewInData(QueueItem);
end;

procedure TisStreamTransmitter.DoOnConstruction;
begin
  inherited;
  FQueueFloodLock := TisLock.Create;
  FIncomingStreamQueue := TisStreamQueue.Create;
  FOutgoingStreamQueue := TisStreamQueue.Create;
  FIncomingQueueHandlerThread := TisQueueHandlerThread.Create(Self, 'I');
  FOutgoingQueueHandlerThread := TisQueueHandlerThread.Create(Self, 'O');
end;

function TisStreamTransmitter.GetCallbacks: IisTransmitterCallbacks;
begin
  Result := FCallbacks;
end;

procedure TisStreamTransmitter.SetCallbacks(
  const AValue: IisTransmitterCallbacks);
begin
  FCallbacks := AValue;
end;

procedure TisStreamTransmitter.QueueStatus(out ReceivingQueueSize, SendingQueueSize: Integer);
begin
  ReceivingQueueSize := FIncomingStreamQueue.Count;
  SendingQueueSize := FOutgoingStreamQueue.Count;
end;

{ TisTCPStreamTransmitter }

procedure TisTCPStreamTransmitter.CheckReceivingQueue;
begin
  FIncomingQueueHandlerThread.Wakeup;
end;

procedure TisTCPStreamTransmitter.CheckSendingQueue;
begin
  FOutgoingQueueHandlerThread.Wakeup;
end;

destructor TisTCPStreamTransmitter.Destroy;
begin
  SetTCPCommunicator(nil);
  inherited;
end;

procedure TisTCPStreamTransmitter.DoOnConnect(const AConnection: IisTCPStreamAsyncConnection);
begin
  AConnection.SessionID := GetUniqueID;
end;

procedure TisTCPStreamTransmitter.DoOnDisconnect(const AConnection: IisTCPStreamAsyncConnection);
begin
  if Assigned(Callbacks) then
    Callbacks.OnDisconnect(AConnection.SessionID);
end;

procedure TisTCPStreamTransmitter.DoOnHeaderReceived(const AConnection: IisTCPStreamAsyncConnection;
  const AHeader: String; var Accept: Boolean);
begin
  if Assigned(FCallbacks) then
    FCallbacks.OnIncomingHeader(AConnection.SessionID, AHeader, Accept);
end;

procedure TisTCPStreamTransmitter.DoOnNewInData(const AQueueItem: IisStreamQueueItem);
begin
  CheckReceivingQueue;
end;

procedure TisTCPStreamTransmitter.DoOnNewOutData(const AQueueItem: IisStreamQueueItem);
begin
  CheckSendingQueue;
end;

procedure TisTCPStreamTransmitter.DoOnReceiveComplete(const AConnection: IisTCPStreamAsyncConnection;
  const AHeader: String; const AStream: IEvDualStream; const ATransmissionTime: Cardinal);
var
  bHandled: Boolean;
begin
  bHandled := False;
  if Assigned(FCallbacks) then
    FCallbacks.BeforeQueueIncomingData(AConnection.SessionID, AHeader, AStream, ATransmissionTime, bHandled);

  if not bHandled then
    AddToReceiveQueue(AConnection.SessionID, AHeader, AStream, ATransmissionTime);
end;

procedure TisTCPStreamTransmitter.DoOnSendComplete(const AConnection: IisTCPStreamAsyncConnection;
  const AHeader: String; const AStream: IEvDualStream; const AFailed: Boolean);
var
  QueueItem: IisStreamQueueItem;
begin
  // If send operation failded it should put data back to queue ans resend it ASAP
  if AFailed then
  begin
    QueueItem := TisStreamQueueItem.Create(AConnection.SessionID, AHeader, AStream, 0);
    FOutgoingStreamQueue.AddItem(QueueItem, True);
  end;
  CheckSendingQueue;
end;

procedure TisTCPStreamTransmitter.DoSendData;
var
  Conn: IisTCPStreamAsyncConnection;
  Item: IisStreamQueueItem;
  i, j: Integer;
  bRemoveItem: Boolean;
  ConnectionList: IisList;
  BusySessions: TStringList;
  bThereAreOutConn: Boolean;
  ItemsToDiscard: IisList;
begin
  if GetTCPCommunicator = nil then
    Exit;

  ItemsToDiscard := nil;

  BusySessions := TStringList.Create;
  try
    BusySessions.Capacity := GetTCPCommunicator.ConnectionCount;
    BusySessions.Sorted := True;
    BusySessions.Duplicates := dupIgnore;

    FOutgoingStreamQueue.Lock;
    try
      i := 0;
      while i < FOutgoingStreamQueue.Count do
      begin
        Item := FOutgoingStreamQueue[i];

        if BusySessions.IndexOf(Item.SessionID) <> -1 then
        begin
          Inc(i);
          Continue;
        end;

        // Create list of outbound connections
        ConnectionList := GetTCPCommunicator.GetConnectionsBySessionID(Item.SessionID); //List of IisTCPStreamAsyncConnection

        // Check if there are outbound connections
        bThereAreOutConn := False;
        for j := 0 to ConnectionList.Count - 1 do
        begin
          Conn := ConnectionList[j] as IisTCPStreamAsyncConnection;
          if Conn.DataDirection in [ddBoth, ddOutbound] then
          begin
            bThereAreOutConn := True;
            Conn := nil;
            break;
          end;
        end;

        // Cleanup inbound connections if there are outbound ones
        // Otherwise use inbound connection for outbound data
        if bThereAreOutConn then
          for j := ConnectionList.Count - 1 downto 0 do
          begin
            Conn := ConnectionList[j] as IisTCPStreamAsyncConnection;
            if Conn.DataDirection = ddInbound then
            begin
              ConnectionList.Delete(j);
              Conn := nil;
            end;
          end;

        bRemoveItem := False;
        if ConnectionList.Count > 0 then
        begin
          // Go by all connections of this session until none busy connection is found
          for j := 0 to ConnectionList.Count - 1 do
          begin
            Conn := ConnectionList[j] as IisTCPStreamAsyncConnection;
            if Conn.Send(Item.Stream, Item.Header) then // Is connection busy with sending another data?
            begin
              bRemoveItem := True;                            // it's ready for sending
              break;
            end;
          end;
          Conn := nil;
        end
        else
        begin
          bRemoveItem := True; // Session lost all connection. Report it back and discard this item
          if Assigned(Callbacks) then
          begin
            if not Assigned(ItemsToDiscard) then
              ItemsToDiscard := TisList.Create;
            ItemsToDiscard.Add(Item);
          end;
        end;

        if bRemoveItem then
          FOutgoingStreamQueue.RemoveItem(Item)
        else
        begin
          BusySessions.Add(Item.SessionID);
          Inc(i);
        end;
      end;

    finally
      FOutgoingStreamQueue.UnLock;
    end;
  finally
    FreeAndNil(BusySessions);
  end;

  if Assigned(ItemsToDiscard) then
    Callbacks.OnDiscardItems(ItemsToDiscard);
end;

function TisTCPStreamTransmitter.GetTCPCommunicator: IisTCPStreamCommunicator;
begin
  Result := FTCPCommunicator;
end;

function TisTCPStreamTransmitter.IsActive: Boolean;
begin
  Result := Assigned(FIncomingQueueHandlerThread) and FIncomingQueueHandlerThread.IsActive and
            Assigned(FOutgoingQueueHandlerThread) and FOutgoingQueueHandlerThread.IsActive;
end;

procedure TisTCPStreamTransmitter.SetTCPCommunicator(const ATCPCommunicator: IisTCPStreamCommunicator);
begin
  if (ATCPCommunicator <> FTCPCommunicator) and Assigned(FTCPCommunicator) then
  begin
    FTCPCommunicator.OnIncomingStream := nil;
    FTCPCommunicator.OnSendComplete := nil;
    FTCPCommunicator.OnConnect := nil;
    FTCPCommunicator.OnDisconnect := nil;
  end;

  FTCPCommunicator := ATCPCommunicator;

  if Assigned(FTCPCommunicator) then
  begin
    FTCPCommunicator.OnIncomingHeader := DoOnHeaderReceived;  
    FTCPCommunicator.OnIncomingStream := DoOnReceiveComplete;
    FTCPCommunicator.OnSendComplete := DoOnSendComplete;
    FTCPCommunicator.OnConnect := DoOnConnect;
    FTCPCommunicator.OnDisconnect := DoOnDisconnect;
  end;
end;

{ TisQueueHandlerThread }

constructor TisQueueHandlerThread.Create(AOwner: TisStreamTransmitter; ADirection: Char);
var
  s: String;
begin
  FOwner := AOwner;
  FDirection := ADirection;

  if FDirection = 'O' then
    s := 'Send outbound data'
  else
    s := 'Dispatch inbound data';

  inherited Create(s);
end;

procedure TisQueueHandlerThread.DoWork;
var
  Session: TisGUID;
  Data: IEvDualStream;
  Header: String;
  TransmissionTime: Cardinal;
begin
  if FDirection = 'O' then
    FOwner.DoSendData
  else
    while FOwner.Receive(Session, Header, Data, TransmissionTime) do
      if Assigned(FOwner.Callbacks) then
        FOwner.Callbacks.OnIncomingData(Session, Header, Data, TransmissionTime);
end;

{ TisUDPStreamTransmitter }

procedure TisUDPStreamTransmitter.CheckReceivingQueue;
begin
  FIncomingQueueHandlerThread.Wakeup;
end;

procedure TisUDPStreamTransmitter.CheckSendingQueue;
begin
  FOutgoingQueueHandlerThread.Wakeup;
end;

destructor TisUDPStreamTransmitter.Destroy;
begin
  SetUDPCommunicator(nil);
  inherited;
end;

procedure TisUDPStreamTransmitter.DoOnNewInData(const AQueueItem: IisStreamQueueItem);
begin
  CheckReceivingQueue;
end;

procedure TisUDPStreamTransmitter.DoOnNewOutData(const AQueueItem: IisStreamQueueItem);
begin
  CheckSendingQueue;
end;

procedure TisUDPStreamTransmitter.DoOnReceiveComplete(const AConnection: IisAsyncUDPStreamSocket;
 const AStream: IEvDualStream; const ATransmissionTime: Cardinal);
var
  bHandled: Boolean;
begin
  bHandled := False;
  if Assigned(FCallbacks) then
    FCallbacks.BeforeQueueIncomingData(AConnection.GetSocketID, '', AStream, ATransmissionTime, bHandled);

  if not bHandled then
    AddToReceiveQueue(AConnection.GetSocketID, '', AStream, ATransmissionTime);
end;

procedure TisUDPStreamTransmitter.DoOnSendComplete(const AConnection: IisAsyncUDPStreamSocket;
   const AStream: IEvDualStream);
begin
  CheckSendingQueue;
end;

procedure TisUDPStreamTransmitter.DoSendData;
var
  Socket: IisAsyncUDPStreamSocket;
  Item: IisStreamQueueItem;
  i: Integer;
  bRemoveItem: Boolean;
  BusySockets: TStringList;
begin
  if GetUDPCommunicator = nil then
    Exit;

  BusySockets := TStringList.Create;
  try
    BusySockets.Capacity := GetUDPCommunicator.SocketCount;
    BusySockets.Sorted := True;
    BusySockets.Duplicates := dupIgnore;

    FOutgoingStreamQueue.Lock;
    try
      i := 0;
      while i < FOutgoingStreamQueue.Count do
      begin
        Item := FOutgoingStreamQueue[i];

        if BusySockets.IndexOf(Item.SessionID) <> -1 then
        begin
          Inc(i);
          Continue;
        end;

        Socket := GetUDPCommunicator.FindSocketByID(Item.SessionID);

        if Assigned(Socket) then
          if Socket.Send(Item.Stream) then     // Is connection busy with sending another data?
            bRemoveItem := True                // it's ready for sending
          else
            bRemoveItem := False
        else
          bRemoveItem := True; // Session is closed. Discard this item

        if bRemoveItem then
          FOutgoingStreamQueue.RemoveItem(Item)
        else
        begin
          BusySockets.Add(Item.SessionID);
          Inc(i);
        end;
      end;

    finally
      FOutgoingStreamQueue.UnLock;
    end;
  finally
    FreeAndNil(BusySockets);
  end;
end;

function TisUDPStreamTransmitter.GetUDPCommunicator: IisUDPStreamCommunicator;
begin
  Result := FUDPCommunicator;
end;

function TisUDPStreamTransmitter.IsActive: Boolean;
begin
  Result := Assigned(FIncomingQueueHandlerThread) and FIncomingQueueHandlerThread.IsActive and
            Assigned(FOutgoingQueueHandlerThread) and FOutgoingQueueHandlerThread.IsActive;
end;

procedure TisUDPStreamTransmitter.SetUDPCommunicator(const AUDPCommunicator: IisUDPStreamCommunicator);
begin
  if (AUDPCommunicator <> FUDPCommunicator) and Assigned(FUDPCommunicator) then
  begin
    FUDPCommunicator.OnIncomingStream := nil;
    FUDPCommunicator.OnSendComplete := nil;
  end;

  FUDPCommunicator := AUDPCommunicator;

  if Assigned(FUDPCommunicator) then
  begin
    FUDPCommunicator.OnIncomingStream := DoOnReceiveComplete;
    FUDPCommunicator.OnSendComplete := DoOnSendComplete;
  end;
end;

end.

