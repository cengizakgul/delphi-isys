// Activate Nexus Memory Manager

  nxReplacementMemoryManager,
  nxllMemoryManager,
  nxllTypes,
  nxllFastMove,
  nxllFastFillChar,
  nxllFastCpuDetect,
  nxllLockedFuncs,
  nxllMemoryManagerImpl,

{$HINTS ON}
{$WARNINGS ON}

