unit isLocale;

interface

uses Windows, SysUtils;

procedure SetupLocale;
procedure RestoreLocale;


implementation

var OrigLCID: LCID;

function MAKELCID(lgid:WORD; srtid:WORD):DWORD;
begin
    Result := (DWORD(srtid) shl 16) or DWORD(lgid);
end;

function MAKELANGID(p:word; s:word):WORD;
begin
  Result := (WORD(s) shl 10) or WORD(p)
end;

procedure SetupLocale();
var
  DefLCID: LCID;
begin
  OrigLCID := GetThreadLocale;
  SetThreadLocale(MAKELCID(MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), SORT_DEFAULT));
  DefLCID := GetThreadLocale;

  if SetLocaleInfo(DefLCID, LOCALE_SDecimal,'.') then
     DecimalSeparator := '.';

  if SetLocaleInfo(DefLCID, LOCALE_SDATE, '/') then
    DateSeparator := '/';

  if SetLocaleInfo(DefLCID, LOCALE_SSHORTDATE, 'M/d/yyyy') then
    ShortDateFormat := 'M/d/yyyy';

  if SetLocaleInfo(DefLCID, LOCALE_STIME, ':') then
    TimeSeparator := ':';

//  The following doesn't work right. There is no time to figure out why. Hope it's not important.
//  Win32Check(SetLocaleInfo(DefLCID, LOCALE_ITLZERO, '0'));
//  Win32Check(SetLocaleInfo(DefLCID, LOCALE_ITIME, '0'));
//  Win32Check(SetLocaleInfo(DefLCID, LOCALE_ITIMEMARKPOSN, '0'));

  if SetLocaleInfo(DefLCID, LOCALE_STIMEFORMAT, 'h:mm:ss tt') then
  begin
    TimeAMString := 'AM';
    TimePMString := 'PM';
    ShortTimeFormat := 'h:mm AMPM';
    LongTimeFormat := 'h:mm:ss AMPM';
  end;
end;

procedure RestoreLocale();
begin
  SetThreadLocale(OrigLCID);
end;


initialization
  SetupLocale;

finalization
  RestoreLocale;

end.
