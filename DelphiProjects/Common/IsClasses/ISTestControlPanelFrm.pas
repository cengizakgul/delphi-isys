unit ISTestControlPanelFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, ActnList, ISBasicClasses, ExtCtrls, istestutils;

type
  TisTestControlPanel = class(TForm, IisTestEngineSupportControl)
    actionList1: TActionList;
    PauseResume: TAction;
    ActionForm: TAction;
    pnlControl: TPanel;
    btnRecord: TSpeedButton;
    btnPause: TSpeedButton;
    pnlPlayback: TPanel;
    Label1: TLabel;
    procedure btnRecordClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PauseResumeUpdate(Sender: TObject);
    procedure PauseResumeExecute(Sender: TObject);
    procedure ActionFormUpdate(Sender: TObject);
    procedure ActionFormExecute(Sender: TObject);
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure WmNCHitTest( var Message: TWMNCHITTEST ); message  WM_NCHITTEST;
  public
    procedure ForceUpdate; 
  end;

implementation

{$R *.dfm}

procedure TisTestControlPanel.btnRecordClick(Sender: TObject);
begin
  TestingEngine.Stop;
end;

procedure TisTestControlPanel.CreateParams(var Params: TCreateParams);
begin
  inherited;
 // Params.Style := Params.style AND not WS_CAPTION;
end;

procedure TisTestControlPanel.FormResize(Sender: TObject);
begin
  Left := Monitor.WorkareaRect.Right - Monitor.WorkareaRect.Left - Width;
  Top := 0;
end;

procedure TisTestControlPanel.FormShow(Sender: TObject);
begin
  FormResize(Sender);
//  FlashWindow( Handle, false );
  inherited
end;

procedure TisTestControlPanel.PauseResumeUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := TestingEngine.State in [isTESRecordingPaused, isTESRecording,
    isTESPlaybackPaused, isTESPlayback];
  (Sender as TCustomAction).Checked := TestingEngine.State in [isTESRecordingPaused, isTESPlaybackPaused];
  case TestingEngine.State of
    isTESRecordingPaused, isTESPlaybackPaused: (Sender as TCustomAction).Caption := 'Resume';
    isTESRecording, isTESPlayback: (Sender as TCustomAction).Caption := 'Pause';
  end;
end;

procedure TisTestControlPanel.PauseResumeExecute(Sender: TObject);
begin
  case TestingEngine.State of
    isTESRecordingPaused, isTESPlaybackPaused: TestingEngine.Resume;
    isTESRecording, isTESPlayback: TestingEngine.Pause;
  else
    Assert( false );
  end;
end;

procedure TisTestControlPanel.WmNCHitTest(var Message: TWMNCHITTEST);
begin
  inherited;
  if Message.Result = HTCAPTION then
    Message.Result := HTBORDER;
end;

procedure TisTestControlPanel.ActionFormUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Caption := StrFromState( TestingEngine.State );
end;

procedure TisTestControlPanel.ActionFormExecute(Sender: TObject);
begin
// intentionally left blank -- form will be disabled if there is no OnExecute handler
end;

procedure TisTestControlPanel.ForceUpdate;
begin
  pnlControl.Visible := TestingEngine.State in [isTESRecordingPaused, isTESRecording, isTESPlaybackPaused];
  pnlPlayback.Visible := not pnlControl.Visible;
  UpdateActions;
  Repaint;
end;

end.


