unit isDataSet;

interface

uses  Classes, SysUtils, EvStreamUtils, DB, isBaseClasses, ISKbmMemDataSet,
      kbmMemBinaryStreamFormat;


type
  IDataSet = interface
  ['{84294933-7F06-4756-86C1-81C533C30454}']
    function  vclDataSet: TisKbmMemDataSet;
    function  GetFields: TFields;
    function  GetBof: Boolean;
    function  GetEof: Boolean;
    function  GetFieldCount: Integer;
    function  GetRecordCount: Integer;
    function  GetRecNo: Integer;
    procedure SetRecNo(const AValue: Integer);
    function  GetFieldValue(const Index: string): Variant;
    procedure SetFieldValue(const Index: string; const AValue: Variant);
    function  GetIndexFieldNames: String;
    procedure SetIndexFieldNames(const AValue: String);
    function  GetFilter: String;
    procedure SetFilter(const AValue: String);
    function  GetFiltered: Boolean;
    procedure SetFiltered(const AValue: Boolean);
    procedure First;
    procedure Last;
    procedure Next;
    procedure Prior;
    procedure AppendRecord(const AValues: array of const);
    procedure Append;
    procedure Insert;
    procedure Delete;
    procedure DeleteAll;
    procedure Edit;
    procedure Post;
    procedure Cancel;
    function  Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;
    procedure Clear;
    function  FindKey(const KeyValues:array of const): Boolean;
    function  FindNearest(const KeyValues:array of const): Boolean;
    function  GetData: IEvDualStream;
    procedure SetData(const AData: IEvDualStream);
    function  FieldByName(const AFieldName: String): TField;
    procedure SetRange(const StartValues, EndValues:array of const);
    procedure CancelRange;
    function  GetState: TDataSetState;
    function  GetLogChanges: Boolean;
    procedure SetLogChanges(const AValue: Boolean);
    function  GetColumnValues(const AFieldName: String): IisStringList;
    property  Fields: TFields read GetFields;
    property  Bof: Boolean read GetBof;
    property  Eof: Boolean read GetEof;
    property  FieldCount: Integer read GetFieldCount;
    property  FieldValues[const FieldName: string]: Variant read GetFieldValue write SetFieldValue; default;
    property  RecordCount: Integer read GetRecordCount;
    property  RecNo: Integer read GetRecNo write SetRecNo;
    property  Data: IEvDualStream read GetData write SetData;
    property  IndexFieldNames:string read GetIndexFieldNames write SetIndexFieldNames;
    property  Filter: string read GetFilter write SetFilter;
    property  Filtered: Boolean read GetFiltered write SetFiltered;
    property  State: TDataSetState read GetState;
    property  LogChanges: Boolean read GetLogChanges write SetLogChanges;
  end;



  TDSFieldDef = record
    FieldName: String;
    DataType: TFieldType;
    Size: Integer;
    Required: Boolean;
  end;


  TisDataSet = class(TisInterfacedObject, IDataSet)
  private
    FDataSet: TisKbmMemDataSet;
  protected
    procedure DoOnConstruction; override;
    function  Revision: TisRevision; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  CreateVCLDataSet: TisKbmMemDataSet; virtual;
    procedure BeforeOpen; virtual;
    function  GetData: IEvDualStream;
    procedure SetData(const AData: IEvDualStream);
    procedure Clear;
    function  FindKey(const KeyValues:array of const): Boolean;
    function  FindNearest(const KeyValues:array of const): Boolean;
    function  GetIndexFieldNames: String;
    procedure SetIndexFieldNames(const AValue: String);

    function  vclDataSet: TisKbmMemDataSet;
    function  GetFields: TFields;
    function  FieldByName(const AFieldName: String): TField;
    function  GetBof: Boolean;
    function  GetEof: Boolean;
    function  GetFieldCount: Integer;
    function  GetRecordCount: Integer;
    function  GetRecNo: Integer;
    procedure SetRecNo(const AValue: Integer);
    function  GetFieldValue(const Index: string): Variant;
    procedure SetFieldValue(const Index: string; const AValue: Variant);
    function  GetFilter: String;
    procedure SetFilter(const AValue: String);
    function  GetFiltered: Boolean;
    procedure SetFiltered(const AValue: Boolean);
    procedure First;
    procedure Last;
    procedure Next;
    procedure Prior;
    procedure AppendRecord(const AValues: array of const);
    procedure Append;
    procedure Insert;
    procedure Delete;
    procedure DeleteAll;
    procedure Edit;
    procedure Post;
    procedure Cancel;
    function  Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;
    procedure SetRange(const StartValues, EndValues:array of const);
    procedure CancelRange;
    function  GetState: TDataSetState;
    function  GetLogChanges: Boolean;
    procedure SetLogChanges(const AValue: Boolean);
    function  GetColumnValues(const AFieldName: String): IisStringList;    
  public
    class function GetTypeID: String; override;
    constructor Create(const AFields: array of TDSFieldDef; const AInitLock: Boolean = False); reintroduce; overload;
    constructor Create(const AFields: TFields; const AInitLock: Boolean = False); reintroduce; overload;
    constructor Create(const AFields: TFieldDefs; const AInitLock: Boolean = False); reintroduce; overload;
    constructor Create(const ASource: TDataSet; const AInitLock: Boolean = False); reintroduce; overload;
    constructor Create(const ASource: TDataSet; const AExtFields: array of TDSFieldDef; const AInitLock: Boolean = False); reintroduce; overload;
    constructor CreateForDataSet(const ADataSet: TisKbmMemDataSet; const AInitLock: Boolean = False);
    destructor  Destroy; override;
  end;



implementation

uses kbmMemTable;


{ TisDataSet }

procedure TisDataSet.Append;
begin
  FDataSet.Append;
end;

procedure TisDataSet.AppendRecord(const AValues: array of const);
begin
  FDataSet.AppendRecord(AValues);
end;

constructor TisDataSet.Create(const AFields: TFields; const AInitLock: Boolean = False);
var
  i: Integer;
begin
  inherited Create(nil, AInitLock);

  for i := 0 to AFields.Count - 1 do
    FDataSet.FieldDefs.Add(AFields[i].FieldName, AFields[i].DataType, AFields[i].Size);

  BeforeOpen;
  FDataSet.Open;
end;

procedure TisDataSet.Clear;
begin
  FDataSet.EmptyTable;
  FDataSet.IndexName := '';
  FDataSet.IndexDefs.Clear;
  FDataSet.Indexes.Clear;
end;

constructor TisDataSet.Create(const ASource: TDataSet; const AInitLock: Boolean = False);
begin
  Create(ASource, [], AInitLock);
end;

constructor TisDataSet.Create(const AFields: TFieldDefs; const AInitLock: Boolean = False);
begin
  inherited Create(nil, AInitLock);
  FDataSet.FieldDefs := AFields;
  BeforeOpen;
  FDataSet.Open;
end;

procedure TisDataSet.Delete;
begin
  FDataSet.Delete;
end;

destructor TisDataSet.Destroy;
begin
  FreeAndNil(FDataSet);
  inherited;
end;

procedure TisDataSet.Edit;
begin
  FDataSet.Edit;
end;

function TisDataSet.FindKey(const KeyValues: array of const): Boolean;
begin
  Result := FDataSet.FindKey(KeyValues);
end;

procedure TisDataSet.First;
begin
  FDataSet.First;
end;

function TisDataSet.GetBof: Boolean;
begin
  Result := FDataSet.Bof;
end;

function TisDataSet.GetData: IEvDualStream;
begin
  Result := FDataSet.GetDataStream(True, True);
  Result.Position := 0;
end;

function TisDataSet.GetEof: Boolean;
begin
  Result := FDataSet.Eof;
end;

function TisDataSet.GetFieldCount: Integer;
begin
  Result := FDataSet.FieldCount;
end;

function TisDataSet.GetFields: TFields;
begin
  Result := FDataSet.Fields;
end;

function TisDataSet.GetFieldValue(const Index: string): Variant;
begin
  Result := FDataSet[Index]; 
end;

function TisDataSet.GetIndexFieldNames: String;
begin
  Result := FDataSet.IndexFieldNames;
end;

function TisDataSet.GetRecNo: Integer;
begin
  Result := FDataSet.RecNo;
end;

function TisDataSet.GetRecordCount: Integer;
begin
  Result := FDataSet.RecordCount;
end;

procedure TisDataSet.Insert;
begin
  FDataSet.Insert;
end;

procedure TisDataSet.Last;
begin
  FDataSet.Last;
end;

procedure TisDataSet.Next;
begin
  FDataSet.Next;
end;

procedure TisDataSet.Post;
begin
  FDataSet.Post;
end;

procedure TisDataSet.Prior;
begin
  FDataSet.Prior;
end;

procedure TisDataSet.SetData(const AData: IEvDualStream);
begin
  FDataSet.Close;
  BeforeOpen;
  AData.Position := 0;
  FDataSet.LoadFromUniversalStream(AData);
end;

procedure TisDataSet.SetFieldValue(const Index: string; const AValue: Variant);
begin
  FDataSet[Index] := AValue;
end;

procedure TisDataSet.SetIndexFieldNames(const AValue: String);
begin
  if FDataSet.Active and (FDataSet.Indexes.GetByFieldNames(AValue) = nil) then
    FDataSet.AddIndex('IND_'+ StringReplace(AValue, ';', '__', [rfReplaceAll]), AValue, []);

  FDataSet.IndexFieldNames := AValue;
end;

procedure TisDataSet.SetRecNo(const AValue: Integer);
begin
  FDataSet.RecNo := AValue;
end;

function TisDataSet.vclDataSet: TisKbmMemDataSet;
begin
  Result := FDataSet; 
end;

constructor TisDataSet.Create(const AFields: array of TDSFieldDef; const AInitLock: Boolean = False);
var
  i: Integer;
begin
  inherited Create(nil, AInitLock);

  for i := Low(AFields) to High(AFields) do
    FDataSet.FieldDefs.Add(AFields[i].FieldName, AFields[i].DataType, AFields[i].Size, AFields[i].Required);

  BeforeOpen;
  FDataSet.Open;
end;

function TisDataSet.Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;
begin
  Result := FDataSet.Locate(KeyFields, KeyValues, Options);
end;

procedure TisDataSet.DoOnConstruction;
begin
  inherited;
  FDataSet := CreateVCLDataSet;
end;

class function TisDataSet.GetTypeID: String;
begin
  Result := 'isDataSet';
end;

function TisDataSet.FieldByName(const AFieldName: String): TField;
begin
  Result := FDataSet.FieldByName(AFieldName);
end;

function TisDataSet.GetFilter: String;
begin
  Result := FDataSet.Filter;
end;

procedure TisDataSet.SetFilter(const AValue: String);
begin
  FDataSet.Filter := AValue;
end;

function TisDataSet.GetFiltered: Boolean;
begin
  Result := FDataSet.Filtered;
end;

procedure TisDataSet.SetFiltered(const AValue: Boolean);
begin
  FDataSet.Filtered := AValue;
end;

function TisDataSet.CreateVCLDataSet: TisKbmMemDataSet;
begin
  Result := TISKbmMemDataSet.Create(nil);
end;

procedure TisDataSet.BeforeOpen;
begin
  FDataSet.CreateTable;
end;

procedure TisDataSet.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IevDualStream;
begin
  inherited;

  // todo: Remove some day
  if ARevision < 1 then
  begin
    S := AStream.ReadStream(nil);
    S.Position := 0;
    TISKbmMemDataSet(FDataSet).LoadFromStream(S.RealStream);
  end
  else
  begin
    AStream.ReadInteger; // Legacy! Need to skip stream size
    TISKbmMemDataSet(FDataSet).LoadFromUniversalStream(AStream);
  end;
end;


procedure TisDataSet.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(0); // Legacy! Need to write stream size  
  TISKbmMemDataSet(FDataSet).SaveToUniversalStream(True, True, AStream);
end;

function TisDataSet.Revision: TisRevision;
begin
  Revision := 1;
end;

procedure TisDataSet.CancelRange;
begin
  TISKbmMemDataSet(FDataSet).CancelRange;
end;

procedure TisDataSet.SetRange(const StartValues,
  EndValues: array of const);
begin
  TISKbmMemDataSet(FDataSet).SetRange(StartValues, EndValues);
end;

procedure TisDataSet.Cancel;
begin
  FDataSet.Cancel;
end;

constructor TisDataSet.CreateForDataSet(const ADataSet: TisKbmMemDataSet; const AInitLock: Boolean = False);
begin
  inherited Create(nil, AInitLock);

  FreeAndNil(FDataSet);
  FDataSet := ADataSet;
  if ADataSet.Owner <> nil then
    ADataSet.Owner.RemoveComponent(ADataSet);

  First;
end;

procedure TisDataSet.DeleteAll;
begin
  FDataSet.DisableControls;
  try
    FDataSet.Last;
    while not FDataSet.Eof do
      FDataSet.Delete;
  finally
    FDataSet.EnableControls;
  end;
end;

function TisDataSet.GetState: TDataSetState;
begin
  Result := TISKbmMemDataSet(FDataSet).State;
end;

function TisDataSet.GetLogChanges: Boolean;
begin
  Result := TISKbmMemDataSet(FDataSet).LogChanges;
end;

procedure TisDataSet.SetLogChanges(const AValue: Boolean);
begin
  TISKbmMemDataSet(FDataSet).LogChanges := AValue;
end;

function TisDataSet.FindNearest(const KeyValues: array of const): Boolean;
begin
  Result := FDataSet.FindNearest(KeyValues);
end;

constructor TisDataSet.Create(const ASource: TDataSet; const AExtFields: array of TDSFieldDef; const AInitLock: Boolean = False);
var
  i: Integer;
begin
  inherited Create(nil, AInitLock);

  for i := 0 to ASource.Fields.Count - 1 do
    FDataSet.FieldDefs.Add(ASource.Fields[i].FieldName, ASource.Fields[i].DataType, ASource.Fields[i].Size);

  for i := 0 to High(AExtFields) do
    FDataSet.FieldDefs.Add(AExtFields[i].FieldName, AExtFields[i].DataType, AExtFields[i].Size, AExtFields[i].Required);

  BeforeOpen;
  FDataSet.Open;

  SetLogChanges(False);
  ASource.First;
  while not ASource.Eof do
  begin
    Append;
    for i := 0 to ASource.FieldCount - 1 do
      GetFields[i].Value := ASource.Fields[i].Value;
    Post;

    ASource.Next;
  end;
  SetLogChanges(True);
  First;
end;

function TisDataSet.GetColumnValues(const AFieldName: String): IisStringList;
var
  BM: String;
  Fld: TField;
begin
  Fld := FieldByName(AFieldName);
  Result := TisStringList.Create;
  Result.Capacity := GetRecordCount;

  vclDataSet.DisableControls;
  try
    BM := vclDataSet.Bookmark;
    try
      First;
      while not GetEof do
      begin
        Result.Add(Fld.AsString);
        Next;
      end;
    finally
      vclDataSet.Bookmark := BM;
    end;
  finally
    vclDataSet.EnableControls;
  end;
end;

initialization
  ObjectFactory.Register(TisDataSet);

finalization
  SafeObjectFactoryUnRegister(TisDataSet);

end.
