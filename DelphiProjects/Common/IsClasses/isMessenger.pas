unit isMessenger;

interface

uses Windows, Messages, SysUtils, Dialogs, Classes, isBaseClasses, isThreadManager,
     isLogFile, isVCLBugFix;

type
  // Message broadcast/unicast for entire application

  IisMessageRecipient = interface
  ['{E0B00F18-2232-4384-B3CA-7E5EB023AB74}']
    procedure Notify(const AMessage: String; const AParams: IisListOfValues);
    function  Destroying: Boolean;
  end;

  IisMessenger = interface
  ['{D42CC635-2128-4127-8761-FFA4589405E1}']
    procedure Clear;
    procedure Subscribe(const ARecipient: IisMessageRecipient; const AMessages: array of String;
                        const AMainThreadOnly: Boolean);
    procedure Unsubscribe(const ARecipient: IisMessageRecipient; const AMessages: array of String);
    procedure Broadcast(const AMessage: String; const AParams: IisListOfValues = nil);
    procedure PostMessage(const ARecipient: IisMessageRecipient; const AMessage: String; const AParams: IisListOfValues = nil);
  end;

  function  CreateMessenger: IisMessenger;

implementation

const
  CM_UNICAST = WM_USER + 1;

type
  TisMessenger = class;

  TisDispather = class(TReactiveThread)
  private
    FOwner: TisMessenger;
    FWindow: THandle;
    procedure SendBroadcast(const ARecipients: IisPointerList; const AMessage: String; const AParams: IisListOfValues);
    procedure SendUnicast(const ARecipient: IisMessageRecipient; const AMessage: String; const AParams: IisListOfValues);
    procedure PostUnicast(const ARecipient: IisMessageRecipient; const AMessage: String; const AParams: IisListOfValues);
    procedure WndProc(var Message: TMessage); // for main thread messages
  protected
    procedure DoWork; override;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
  end;


  TisMessenger = class(TisInterfacedObject, IisMessenger)
  private
    FDispather: TisDispather;
    FSubscribers: IisStringList;
    FMTOnlyMembers: TList;
    FQueue: IisStringList;

    function  StillSubscribed(const ARecipient: Pointer; const AMessage: String): Boolean;

    procedure Clear;
    procedure Subscribe(const ARecipient: IisMessageRecipient; const AMessages: array of String;
                        const AMainThreadOnly: Boolean);
    procedure Unsubscribe(const ARecipient: IisMessageRecipient; const AMessages: array of String);
    procedure Broadcast(const AMessage: String; const AParams: IisListOfValues = nil);
    procedure PostMessage(const ARecipient: IisMessageRecipient; const AMessage: String; const AParams: IisListOfValues = nil);
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
  end;


function CreateMessenger: IisMessenger;
begin
  Result := TisMessenger.Create;  // MUST BE CALLED FROM MAIN THREAD
end;

{ TisMessenger }

procedure TisMessenger.Broadcast(const AMessage: String; const AParams: IisListOfValues);
begin
  Assert(not AnsiSameText(AMessage, 'ALL'));

  FQueue.Lock;
  try
    FQueue.AddObject(AMessage, AParams);
  finally
    FQueue.UnLock;
  end;

  FDispather.Wakeup;
  Sleep(0);
end;

procedure TisMessenger.Clear;
begin
  FSubscribers.Clear;
  FMTOnlyMembers.Clear;
  FQueue.Clear;
end;

procedure TisMessenger.DoOnConstruction;
begin
  inherited;
  FSubscribers := TisStringList.CreateAsIndex(True);
  FMTOnlyMembers := TList.Create;
  FQueue := TisStringList.Create(nil, True);

  FDispather := TisDispather.Create('Messenger');
  FDispather.FOwner := Self;
end;

procedure TisMessenger.DoOnDestruction;
begin
  Clear;
  FreeAndNil(FDispather);
  FreeAndNil(FMTOnlyMembers);
  inherited;
end;

procedure TisMessenger.PostMessage(const ARecipient: IisMessageRecipient;
  const AMessage: String; const AParams: IisListOfValues);
begin
  FDispather.PostUnicast(ARecipient, AMessage, AParams);
  Sleep(0);
end;

function TisMessenger.StillSubscribed(const ARecipient: Pointer; const AMessage: String): Boolean;
var
  i: Integer;
  L: IisPointerList;
begin
  FSubscribers.Lock;
  try
    i := FSubscribers.IndexOf(AMessage);
    if i <> -1 then
    begin
      L := FSubscribers.Objects[i] as IisPointerList;
      i := L.IndexOf(ARecipient);
      Result := i <> -1;
    end
    else
      Result := False;
  finally
    FSubscribers.Unlock;
  end;
end;

procedure TisMessenger.Subscribe(const ARecipient: IisMessageRecipient; const AMessages: array of String;
  const AMainThreadOnly: Boolean);
var
  i, j: Integer;
  L: IisPointerList;
  P: Pointer;
begin
  Assert(Assigned(ARecipient));
  P := Pointer(ARecipient as IisMessageRecipient);

  FSubscribers.Lock;
  try
    // Check if already subscribed for all messages
    j := FSubscribers.IndexOf('ALL');
    if j <> -1 then
    begin
      L := FSubscribers.Objects[j] as IisPointerList;
      if L.IndexOf(P) <> -1 then
        Exit;
    end;

    if Length(AMessages) = 0 then
    begin
      // Subscribe for all messages
      Unsubscribe(ARecipient, []);
      j := FSubscribers.IndexOf('ALL');
      if j = -1 then
        j := FSubscribers.AddObject('ALL', TisPointerList.Create);
      L := FSubscribers.Objects[j] as IisPointerList;
      L.Add(P);
    end

    else
    begin
      // Subscribe only for specified messages
      for i := Low(AMessages) to High(AMessages) do
      begin
        j := FSubscribers.IndexOf(AMessages[i]);
        if j = -1 then
          j := FSubscribers.AddObject(AMessages[i], TisPointerList.Create);

        L := FSubscribers.Objects[j] as IisPointerList;
        if L.IndexOf(P) = -1 then
          L.Add(P);
      end;
    end;

    if AMainThreadOnly and (FMTOnlyMembers.IndexOf(P) = -1) then
      FMTOnlyMembers.Add(P);

  finally
    FSubscribers.UnLock;
  end;
end;

procedure TisMessenger.Unsubscribe(const ARecipient: IisMessageRecipient; const AMessages: array of String);
var
  i, j, k: Integer;
  L: IisPointerList;
  P: Pointer;
  bStillSubscribed: Boolean;
begin
  Assert(Assigned(ARecipient));
  P := Pointer(ARecipient as IisMessageRecipient);

  FSubscribers.Lock;
  try
    if Length(AMessages) = 0 then
    begin
      for i := FSubscribers.Count - 1 downto 0 do
      begin
        L := FSubscribers.Objects[i] as IisPointerList;
        j := L.IndexOf(P);
        if j <> -1 then
        begin
          L.Delete(j);
          if L.Count = 0 then
            FSubscribers.Delete(i);
        end;
      end;
    end

    else
      for i := Low(AMessages) to High(AMessages) do
      begin
        j := FSubscribers.IndexOf(AMessages[i]);
        if j <> -1 then
        begin
          L := FSubscribers.Objects[j] as IisPointerList;
          k := L.IndexOf(P);
          if k <> -1 then
          begin
            L.Delete(k);
            if L.Count = 0 then
              FSubscribers.Delete(j);
          end;
        end;
      end;


    j := FMTOnlyMembers.IndexOf(P);
    if j <> -1 then
    begin
      bStillSubscribed := False;
      for i := 0 to FSubscribers.Count - 1 do
      begin
        L := FSubscribers.Objects[i] as IisPointerList;
        if L.IndexOf(P) <> -1 then
        begin
          bStillSubscribed := True;
          break;
        end;
      end;

      if not bStillSubscribed then
        FMTOnlyMembers.Delete(j);
    end;

  finally
    FSubscribers.UnLock;
  end;
end;


{ TisDispather }

procedure TisDispather.AfterConstruction;
begin
  inherited;
  FWindow := isVCLBugFix.AllocateHWnd(WndProc);
end;

procedure TisDispather.BeforeDestruction;
begin
  isVCLBugFix.DeallocateHWnd(FWindow);
  FWindow := 0;
  inherited;
end;

procedure TisDispather.DoWork;
var
  i, j: Integer;
  sMessage: String;
  Params: IisListOfValues;
  MsgList: IisStringList;
begin
  // copy messages to avoid lockups
  MsgList := TisStringList.Create;
  FOwner.FQueue.Lock;
  try
    MsgList.Capacity := FOwner.FQueue.Count;
    for i := 0 to FOwner.FQueue.Count - 1 do
      MsgList.AddObject(FOwner.FQueue[i], FOwner.FQueue.Objects[i]);
    FOwner.FQueue.Clear;
  finally
    FOwner.FQueue.Unlock;
  end;


  for i := 0 to MsgList.Count - 1 do
  begin
    sMessage := MsgList[i];
    Params := MsgList.Objects[i] as IisListOfValues;

    FOwner.FSubscribers.Lock;
    try
      j := FOwner.FSubscribers.IndexOf(sMessage);
      if j <> - 1 then
        SendBroadcast(FOwner.FSubscribers.Objects[j] as IisPointerList, sMessage, Params);

      j := FOwner.FSubscribers.IndexOf('ALL');
      if j <> - 1 then
        SendBroadcast(FOwner.FSubscribers.Objects[j] as IisPointerList, sMessage, Params);
    finally
      FOwner.FSubscribers.UnLock;
    end;
  end;
end;

procedure TisDispather.PostUnicast(const ARecipient: IisMessageRecipient;
  const AMessage: String; const AParams: IisListOfValues);
var
  UnicastParams: IisListOfValues;
begin
  UnicastParams := TisListOfValues.Create;

  UnicastParams.AddValue('Recipient', Integer(Pointer(ARecipient)));
  UnicastParams.AddValue('Message', AMessage);
  UnicastParams.AddValue('Params', AParams);
  UnicastParams._AddRef; // check in

  Windows.PostMessage(FWindow, CM_UNICAST, Integer(Pointer(UnicastParams)), 0);
end;

procedure TisDispather.SendBroadcast(const ARecipients: IisPointerList;
  const AMessage: String; const AParams: IisListOfValues);
var
  i: Integer;
  R: IisMessageRecipient;
  P: Pointer;
begin
  for i := 0 to ARecipients.Count - 1 do
    try
      P := ARecipients[i];
      R := IisMessageRecipient(P);
      R._AddRef;
      try
        if not R.Destroying then
          if FOwner.FMTOnlyMembers.IndexOf(P) = -1 then
            SendUnicast(R, AMessage, AParams)
          else
            PostUnicast(R, AMessage, AParams);
      finally
        R._Release;
      end;
    except
      on E: Exception do
        if GlobalLogFile <> nil then
          GlobalLogFile.AddEvent(etFatalError, 'Messenger', E.Message, '');
    end;
end;

procedure TisDispather.SendUnicast(const ARecipient: IisMessageRecipient;
  const AMessage: String; const AParams: IisListOfValues);
begin
  ARecipient.Notify(AMessage, AParams);
end;

procedure TisDispather.WndProc(var Message: TMessage);
var
  P: IisListOfValues;
  Recipient: IisMessageRecipient;
  sMessage: String;
  Rep: Pointer;
begin
  try
    if Message.Msg = CM_UNICAST then
    begin
      P := IInterface(Pointer(Message.WParam)) as IisListOfValues;
      P._Release; // check out

      sMessage := P.Value['Message'];
      Rep := Pointer(Integer(P.Value['Recipient']));

      if FOwner.StillSubscribed(Rep, sMessage) then
      begin
        Recipient := IInterface(Rep) as IisMessageRecipient;
        SendUnicast(Recipient, P.Value['Message'], IInterface(P.Value['Params']) as IisListOfValues);
      end;
    end
    else if Message.Msg = WM_QUERYENDSESSION then
      Message.Result := 1;
  except
    // Eat up all errors
  end;
end;

end.
