unit isMetaFileUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Dialogs, StdCtrls, isBaseClasses, Graphics,
  EvStreamUtils, emfParserUnit;

const
  FontAndFilenameDelimiter = '###';

type
  TIsInterfacedMetafileException = class(Exception);
  TIsMetafileException = class(Exception);

  TIsMetafile = class(TMetafile)
  private
    FEmfParser : IEmfParser;
    RegisteredFonts : TStringList;
    FMakeFontsEnumerable: boolean;
    procedure SetMakeFontsEnumerable(const Value: boolean);
  protected
    procedure RegisterAllFonts;
    procedure RegisterFont(AFontName : String);
    procedure UnregisterAllFonts;
    procedure ReadData(Stream: TStream); override;
    procedure WriteData(Stream: TStream); override;
  public
    property MakeFontsEnumerable : boolean read FMakeFontsEnumerable write SetMakeFontsEnumerable;
    procedure Assign(Source: TPersistent); override;
    procedure LoadFromStream(Stream: TStream); override;
    procedure SaveToFile(const Filename: String); override;
    procedure SaveToStream(Stream: TStream); override;  // in fact, the only FEMFParser field is saved
    procedure GetFontsList(const AStringList : TSTringList);
    procedure GetPDFBackground(const AStream : TStream);
    function PDFExists : boolean;
    constructor Create; override;
    destructor Destroy; override;
  end;

  TIsInterfacedMetafile = class;
  IIsMetafile = interface
    ['{A0679FDF-0B9B-43D8-B00C-404DB6101592}']
    function GetMetafile : TMetafile;
    procedure GetFontsList(const AStringList : TSTringList);
    procedure LoadFromStream(Stream: IEvDualStream);
  end;

  TIsInterfacedMetafile = class(TInterfacedObject, IIsMetafile)
  private
    FMetafile: TMetaFile;
    FEmfParser : IEmfParser;
    RegisteredFonts : TStringList;
  protected
    procedure RegisterAllFonts;
    procedure RegisterFont(AFontName : String);
    procedure UnregisterAllFonts;
  public
    function GetMetafile : TMetafile;
    constructor Create;
    destructor Destroy; override;
    procedure LoadFromStream(Stream: IEvDualStream);
    procedure GetFontsList(const AStringList : TSTringList);
  end;

implementation

uses ISBasicUtils;

{ TIsInterfacedMetafile }


{ TIsInterfacedMetafile }

constructor TIsInterfacedMetafile.Create;
begin
  FMetafile := TMetaFile.Create;
  RegisteredFonts := TSTringList.Create;
end;

destructor TIsInterfacedMetafile.Destroy;
begin
  FMetafile.Free;
  UnregisterAllFonts;
  RegisteredFonts.Free;
  inherited;
end;

procedure TIsInterfacedMetafile.GetFontsList(const AStringList: TSTringList);
begin
  if FEmfParser <> nil then
    FEmfParser.GetFontsList(AStringList);
end;

function TIsInterfacedMetafile.GetMetafile: TMetafile;
begin
  result := FMetafile;
end;

procedure TIsInterfacedMetafile.LoadFromStream(Stream: IEvDualStream);
var
  tmpStream2 : IEvDualStream;
begin
  // delete current
  UnregisterAllFonts;
  FEmfParser := nil;
  // read FevfParser
  Stream.Position := 0;
  FEmfParser := TEmfParser.CreateFromStream(Stream);
  // register fonts
  RegisterAllFonts;
  // load metafile
  tmpStream2 := TEvDualStreamHolder.CreateInMemory;
  FEmfParser.GetMetafile(tmpStream2);
  tmpStream2.Position := 0;
  FMetafile.LoadFromStream(tmpStream2.RealStream);
end;

procedure TIsInterfacedMetafile.RegisterAllFonts;
var
  i : integer;
  tmpList : TStringList;
begin
  if FEmfParser <> nil then
  begin
    tmpList := TSTringList.Create;
    try
      FEmfParser.GetFontsList(tmpList);
      for i := 0 to tmpList.Count - 1 do
        RegisterFont(tmpList[i]);
    finally
      tmpList.Free;
    end;
  end;  
end;

procedure TIsInterfacedMetafile.RegisterFont(AFontName: String);
var
  Buf : PByte;
  size : integer;
  amount : DWORD;
  tmpHandle : THandle;
begin
  amount := 0;
  size := FEmfParser.GetFontSize(AFontName);
  System.GetMem(Buf, size);
  try
    FEmfParser.GetFontData(AFontName, Buf^, size);
    tmpHandle := AddFontMemResourceEx(Buf, size, nil, @amount);
    if (tmpHandle = 0) or (amount = 0) then raise TIsInterfacedMetafileException.Create('Cannot register font: ' + AFontName);
      registeredFonts.AddObject(AFontName, Pointer(TmpHandle));
  finally
    FreeMem(Buf);
  end;
end;

procedure TIsInterfacedMetafile.UnregisterAllFonts;
var
  i : integer;
  res : boolean;
begin
  for i := 0 to RegisteredFonts.Count - 1 do
  begin
    res := RemoveFontMemResourceEx(THandle(RegisteredFonts.Objects[i]));
    if not res then raise TIsInterfacedMetafileException.Create('Cannot unregister font: ' + RegisteredFonts[i]);
  end;
  RegisteredFonts.Clear;
end;

{ TIsMetafile }

constructor TIsMetafile.Create;
begin
  RegisteredFonts := TSTringList.Create;
  FEmfParser := nil;
  inherited;
  Enhanced := true;
end;

destructor TIsMetafile.Destroy;
begin
  UnregisterAllFonts;
  RegisteredFonts.Free;
  FEmfParser := nil;
  inherited;
end;

procedure TIsMetafile.GetFontsList(const AStringList: TSTringList);
begin
  if FEmfParser <> nil then
    FEmfParser.GetFontsList(AStringList);
end;

procedure TIsMetafile.LoadFromStream(Stream: TStream);
var
  tmpStream1 : IEvDualStream;
  tmpStream2 : IEvDualStream;
begin
  // delete current
  UnregisterAllFonts;
  FEmfParser := nil;
  // read FevfParser
  tmpStream1 := TEvDualStreamHolder.CreateInMemory;
  Stream.Position := 0;
  tmpStream1.RealStream.CopyFrom(Stream, Stream.Size);
  tmpStream1.Position := 0;
  FEmfParser := TEmfParser.CreateFromStream(tmpStream1);
  tmpStream1 := nil;
  // register fonts
  RegisterAllFonts;
  // load metafile
  tmpStream2 := TEvDualStreamHolder.CreateInMemory;
  FEmfParser.GetMetafile(tmpStream2);
  tmpStream2.Position := 0;
  inherited LoadFromStream(tmpStream2.RealStream);
  tmpStream2 := nil;
end;

procedure TIsMetafile.RegisterAllFonts;
var
  i : integer;
  tmpList : TStringList;
begin
  if FEmfParser <> nil then
  begin
    tmpList := TSTringList.Create;
    try
      FEmfParser.GetFontsList(tmpList);
      for i := 0 to tmpList.Count - 1 do
        RegisterFont(tmpList[i]);
    finally
      tmpList.Free;
    end;
  end;  
end;

procedure TIsMetafile.RegisterFont(AFontName: String);
var
  Buf : PByte;
  size : integer;
  amount : DWORD;
  tmpHandle : THandle;
  tmpFile : String;
  tmpStream : TFileStream;
begin
  if not MakeFontsEnumerable then
  begin
    // creating fonts in memory
    amount := 0;
    size := FEmfParser.GetFontSize(AFontName);
    System.GetMem(Buf, size);
    try
      FEmfParser.GetFontData(AFontName, Buf^, size);
      tmpHandle := AddFontMemResourceEx(Buf, size, nil, @amount);
      if (tmpHandle = 0) or (amount = 0) then
        raise TIsInterfacedMetafileException.Create('Cannot register font (in memory): ' + AFontName);
      registeredFonts.AddObject(AFontName, Pointer(TmpHandle));
    finally
      FreeMem(Buf);
    end;
  end
  else
  begin
    // creating fonts in files
    tmpFile := AppTempFolder + GetUniqueId + '.ttf';
    amount := 0;
    size := FEmfParser.GetFontSize(AFontName);
    System.GetMem(Buf, size);
    try
      FEmfParser.GetFontData(AFontName, Buf^, size);
      tmpStream := TFileStream.Create(tmpFile, fmCreate);
      try
        tmpStream.WriteBuffer(Buf^, size);
      finally
        tmpStream.Free;
      end;
      amount := AddFontResourceEx(PChar(tmpFile), FR_PRIVATE, nil);
      if (amount = 0) then raise TIsInterfacedMetafileException.Create('Cannot register font (in file): ' + AFontName);
      registeredFonts.Add(AFontName + FontAndFileNameDelimiter + tmpFile);
    finally
      FreeMem(Buf);
    end;
  end;
end;

procedure TIsMetafile.UnregisterAllFonts;
var
  i, j : integer;
  res : boolean;
  tmpFilename : String;
begin
  tmpFileName := '';
  for i := 0 to RegisteredFonts.Count - 1 do
  begin
    if (not MakeFontsEnumerable) or (Pos(FontAndFilenameDelimiter, RegisteredFonts[i]) = 0) then
    begin
      // deleting fonts which has been created in memory
      res := RemoveFontMemResourceEx(THandle(RegisteredFonts.Objects[i]));
      if not res then raise TIsInterfacedMetafileException.Create('Cannot unregister font (font in memory): ' + RegisteredFonts[i]);
    end
    else
    begin
      // deleting fonts which has been created in files
      j := Pos(FontAndFilenameDelimiter, RegisteredFonts[i]);
      tmpFileName := Copy(RegisteredFonts[i], j + Length(FontAndFilenameDelimiter), Length(RegisteredFonts[i]));
      res := RemoveFontResourceEx(PChar(tmpFilename), FR_PRIVATE, nil);
      if FileExists(tmpFileName) then
        DeleteFile(tmpFilename);
      if not res then raise TIsInterfacedMetafileException.Create('Cannot unregister font (font in file): ' + RegisteredFonts[i]);
    end;
  end;
  RegisteredFonts.Clear;
end;

procedure TIsMetafile.WriteData(Stream: TStream);
var
  tmpStream : IEvDualStream;
  Size : Longint;
begin
  if FEmfParser <> nil then
  begin
    tmpStream := TEvDualStreamHolder.CreateInMemory;
    FEmfParser.SaveToStream(tmpStream);
    Size := tmpStream.Size;
    Stream.Write(Size, Sizeof(Size));
    tmpStream.Position := 0;
    Stream.CopyFrom(tmpStream.RealStream, 0);
    tmpStream := nil;
  end
  else
  begin
    Size := 0;
    Stream.Write(Size, Sizeof(Size));
  end;
end;

procedure TIsMetafile.ReadData(Stream: TStream);
var
  Length: Longint;
  tmpStream : IEvDualStream;
begin
  Stream.Read(Length, SizeOf(Longint));
  if Length = 0 then
    FEmfParser := nil
  else
  begin
    tmpStream := TEvDualStreamHolder.CreateInMemory;
    tmpStream.RealStream.CopyFrom(Stream, Length);
    LoadFromStream(tmpStream.RealStream);
    tmpStream := nil;
  end;
  PaletteModified := Palette <> 0;
  Changed(Self);
end;

procedure TIsMetafile.Assign(Source: TPersistent);
var
  tmpStream : IEvDualStream;
begin
  if (Source = nil) or (Source is TIsMetafile) then
  begin
    if Assigned(Source) then
    begin
      FMakeFontsEnumerable := (Source as TISMetafile).FMakeFontsEnumerable;
      tmpStream := TEvDualStreamHolder.CreateInMemory;
      (Source as TIsMetafile).FEmfParser.SaveToStream(tmpStream);
      tmpStream.Position := 0;
      LoadFromStream(tmpStream.RealStream);
    end
    else
    begin
      UnregisterAllFonts;
      FEmfParser := nil;
      inherited Assign(Source);
    end;
  end
  else
    inherited Assign(Source);
end;

procedure TIsMetafile.SaveToFile(const Filename: String);
var
  tmpStream : IEvDualStream;
begin
  tmpStream := TEvDualStreamHolder.CreateFromNewFile(FileName);
  FEmfParser.SaveToStream(tmpStream);
end;

// in fact, the only FEMFParser field is saved
procedure TIsMetafile.SaveToStream(Stream: TStream);
var
  tmpStream : IEvDualStream;
begin
  tmpStream := TEvDualStreamHolder.CreateInMemory;
  FEmfParser.SaveToStream(tmpStream);
  Stream.CopyFrom(tmpStream.RealStream, 0);
end;

procedure TIsMetafile.SetMakeFontsEnumerable(const Value: boolean);
begin
  FMakeFontsEnumerable := Value;
end;

procedure TIsMetafile.GetPDFBackground(
  const AStream: TStream);
begin
  FEMFParser.GetPDFBackground(AStream);
end;

function TIsMetafile.PDFExists: boolean;
begin
  result := FEMFParser.PDFExists;
end;

initialization
  TPicture.RegisterFileFormat(DefaultParsedFileExtention, DefaultFileDescription, TIsMetafile);

finalization
  TPicture.UnregisterGraphicClass(TIsMetafile);
  
end.

