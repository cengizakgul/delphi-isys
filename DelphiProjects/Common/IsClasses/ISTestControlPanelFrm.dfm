object isTestControlPanel: TisTestControlPanel
  Left = 346
  Top = 391
  Action = ActionForm
  AlphaBlendValue = 200
  AutoSize = True
  BorderIcons = []
  BorderStyle = bsDialog
  ClientHeight = 25
  ClientWidth = 113
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDefault
  OnClick = ActionFormExecute
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlControl: TPanel
    Left = 0
    Top = 0
    Width = 104
    Height = 22
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 0
    object btnRecord: TSpeedButton
      Left = 56
      Top = 0
      Width = 48
      Height = 22
      Caption = 'Stop'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = False
      OnClick = btnRecordClick
    end
    object btnPause: TSpeedButton
      Left = 0
      Top = 0
      Width = 57
      Height = 22
      Action = PauseResume
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = False
    end
  end
  object pnlPlayback: TPanel
    Left = 0
    Top = 0
    Width = 113
    Height = 25
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 1
    object Label1: TLabel
      Left = 0
      Top = 8
      Width = 121
      Height = 13
      AutoSize = False
      Caption = 'Press Esc to pause '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object ActionList1: TActionList
    Left = 40
    Top = 48
    object PauseResume: TAction
      Caption = 'xxxxxx'
      OnExecute = PauseResumeExecute
      OnUpdate = PauseResumeUpdate
    end
    object ActionForm: TAction
      OnExecute = ActionFormExecute
      OnUpdate = ActionFormUpdate
    end
  end
end
