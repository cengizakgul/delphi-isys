unit isSSL;

interface

uses Windows, SysUtils, isBaseClasses, isBasicUtils, isExceptions;

const
  BF_ROUNDS = 16;
  BF_ENCRYPT = 1;
  BF_DECRYPT = 0;
  SSL_VERIFY_CLIENT_ONCE = $04;
  SSL_VERIFY_FAIL_IF_NO_PEER_CERT = $02;
  SSL_VERIFY_NONE = $00;
  SSL_VERIFY_PEER = $01;

  SSL_MODE_ENABLE_PARTIAL_WRITE = 1;
  SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER = 2;
  SSL_MODE_AUTO_RETRY = 4;
  SSL_MODE_NO_AUTO_CHAIN = 8;

  SSL_ERROR_NONE = 0;
  SSL_ERROR_SSL	= 1;
  SSL_ERROR_WANT_READ = 2;
  SSL_ERROR_WANT_WRITE = 3;
  SSL_ERROR_WANT_X509_LOOKUP = 4;
  SSL_ERROR_SYSCALL = 5;
  SSL_ERROR_ZERO_RETURN = 6;
  SSL_ERROR_WANT_CONNECT = 7;
  SSL_ERROR_WANT_ACCEPT	= 8;

  SSL_CTRL_OPTIONS = 32;  
  SSL_CTRL_MODE =	33;

  SSL_OP_ALL = $00000FFF;
  SSL_OP_NO_SSLv2 = $01000000;

  CRYPTO_LOCK = 1;

type
  TsslLockingCallback = procedure (mode, n : integer; Afile : PChar; line : integer) cdecl;
  TsslThreadIDCallback = function: integer cdecl;

  IisSSLLibrary = interface
  ['{A83B525E-C854-4083-B1A0-0995340145FD}']
    function  NewServerContext(const RootCertFile: String; const CertFile: String; const KeyFile: String): Pointer;
    function  NewClientContext(const CAFile: String): Pointer;
    function  SSL_CTX_set_mode(ctx: Pointer; mode: Longint): Longint;
    procedure FreeSSLContext(ctx: Pointer);
    function  GetErrorStr(const AErrorCode: Integer): String;
  end;

var
  SSLv3_server_method: function: Pointer cdecl;
  SSLv3_client_method: function: Pointer cdecl;
  SSLv23_server_method: function: Pointer cdecl;
  SSLv23_client_method: function: Pointer cdecl;
  SSLv2_server_method: function: Pointer cdecl;
  SSLv2_client_method: function: Pointer cdecl;
  SSLv3_method: function: Pointer cdecl;
  SSL_CTX_new: function(meth: Pointer): Pointer cdecl;
  SSL_CTX_free: procedure(arg0: Pointer) cdecl;
  SSL_CTX_set_default_passwd_cb: procedure(ctx: Pointer; cb: Pointer) cdecl;
  SSL_CTX_set_default_passwd_cb_userdata: procedure(ctx: Pointer; u: Pointer) cdecl;
  SSL_CTX_set_default_verify_paths: function(ctx: Pointer): Integer; cdecl;
  SSL_CTX_load_verify_locations: function(ctx: Pointer; const CAfile: PChar; const CApath: PChar):Integer cdecl;
  SSL_CTX_use_certificate_file: function(ctx: Pointer; const _file: PChar; _type: Integer):Integer cdecl;
  SSL_CTX_use_PrivateKey_file: function(ctx: Pointer; const _file: PChar; _type: Integer):Integer cdecl;
  SSL_CTX_check_private_key: function(ctx: Pointer):Integer cdecl;
  SSL_CTX_set_cipher_list: function(arg0: Pointer; str: PChar):Integer cdecl;
  SSL_CTX_set_verify: procedure(ctx: Pointer; mode: Integer; arg2: Pointer) cdecl = nil;
  SSL_CTX_set_session_id_context: function(ctx: Pointer; const sid_ctx: PChar; sid_ctx_len: Longint):Integer; cdecl;
  SSL_CTX_set_client_CA_list: procedure(ctx: Pointer; list: Pointer); cdecl;
  SSL_CTX_ctrl: function(ctx: Pointer; cmd: Integer; larg: Longint; parg: Pointer): Longint cdecl;
  SSL_CTX_set_cert_verify_callback: function(ctx: Pointer; cb: Pointer): Longint; cdecl;
  X509_STORE_CTX_get_current_cert : function(ctx: Pointer):PPointer cdecl = nil;
  X509_STORE_CTX_get_error: function(ctx: Pointer):Integer cdecl = nil;
  X509_get_issuer_name: function(a: Pointer): PChar cdecl = nil;
  X509_get_subject_name: function(a: Pointer): PChar cdecl = nil;
  X509_NAME_oneline: function(a: Pointer; buf: PChar; size: Integer):PChar cdecl = nil;

  SSL_connect: function(ssl: Pointer):Integer cdecl;
  SSL_load_error_strings: procedure cdecl;
  SSL_library_init: function:Integer cdecl;
  SSL_new: function(ctx: Pointer):Pointer cdecl;
  SSL_free: procedure(ssl: Pointer) cdecl;
  SSL_set_fd: function(s: Pointer; fd: Integer):Integer cdecl;
  SSL_accept: function(ssl: Pointer):Integer cdecl;
  SSL_read: function(ssl: Pointer; buf: PChar; num: Integer):Integer cdecl;
  SSL_shutdown: function(ssl: Pointer):Integer cdecl;
  SSL_write: function(ssl: Pointer; const buf: PChar; num: Integer):Integer cdecl;
  SSL_get_error: function(ssl: Pointer; ret_code: Integer):Integer cdecl;
  ERR_get_error: function: Cardinal cdecl;
  ERR_error_string: function(e: Integer; Text: PChar): Integer; cdecl;

  BIO_new_ssl: function(ctx: Pointer; client: Integer): Pointer cdecl;
  BIO_set_fd: function(bio: Pointer; fd: Integer; close_flag: Integer):Integer cdecl;

  RSA_generate_key: function(num: Integer; e: LongWord; cb, cb_arg: Pointer): Pointer; cdecl;
  RAND_seed: procedure(const buf: Pointer; num: Integer); cdecl;
  EVP_PKEY_free: procedure(key: Pointer); cdecl;
  EVP_PKEY_new: function: Pointer; cdecl;
  CRYPTO_set_id_callback: procedure(ACallback : TsslThreadIDCallback) cdecl;
  CRYPTO_set_locking_callback: procedure(ACallback : TsslLockingCallback) cdecl;
  CRYPTO_num_locks: function: integer cdecl;

  function  OpenSSLLib: IisSSLLibrary;

implementation

type
  TisSSLLibrary = class(TisInterfacedObject, IisSSLLibrary)
  private
    FSSLDLL: HMODULE;
    FCRYPTODLL: HMODULE;
    FLocks: IisList;
    procedure InitFuncEntryPoints;
    procedure InitCallbacks;
    function  GetLockByIndex(const AIndex: Integer): IisLock;
    procedure CheckError(const AFunctRes: Integer);

    function  NewServerContext(const RootCertFile: String; const CertFile: String; const KeyFile: String): Pointer;
    function  NewClientContext(const CAFile: String): Pointer;
    function  SSL_CTX_set_mode(ctx: Pointer; mode: Longint): Longint;
    procedure FreeSSLContext(ctx: Pointer);
    function  GetErrorStr(const AErrorCode: Integer): String;

  protected
    procedure DoOnConstruction; override;
  public
    destructor Destroy; override;
  end;


const
  ServerDefaultCipherList = 'RC4-SHA';
  ClientDefaultCipherList = 'ALL:!ADH:@STRENGTH';

var SSLLib: IisSSLLibrary;
    SSLLibObj: TisSSLLibrary;
    Lock: IisLock;

function OpenSSLLib: IisSSLLibrary;
begin
  Lock.Lock;
  try
    if not Assigned(SSLLib) then
      SSLLib := TisSSLLibrary.Create;
  finally
    Lock.Unlock;
  end;

  Result := SSLLib;
end;

function VerifyCallback(Ok: Integer; ctx: Pointer): Integer; cdecl;
var
  hcert: Pointer;
  IssuerName, SubjectName: String;
begin
  Result := Ok;
  Lock.Lock;
  try
    hcert := X509_STORE_CTX_get_current_cert(ctx);
    SetLength(IssuerName, 1000);
    SetLength(SubjectName, 1000);
    X509_NAME_oneline(X509_get_issuer_name(hcert), @IssuerName[1], 1000);
    X509_NAME_oneline(X509_get_subject_name(hcert), @SubjectName[1], 1000);
    IssuerName := PChar(IssuerName);
    SubjectName := PChar(SubjectName);
  finally
    Lock.Unlock;
  end;
end;

{ TisSSLLibrary }

procedure TisSSLLibrary.CheckError(const AFunctRes: Integer);
begin
  if AFunctRes <= 0 then
    raise EisException.Create('OpenSSL error: ' + GetErrorStr(ERR_get_error));
end;

destructor TisSSLLibrary.Destroy;
begin
  if FCRYPTODLL <> 0 then
  begin
    CRYPTO_set_locking_callback(nil);
    CRYPTO_set_id_callback(nil);

    FreeLibrary(FCRYPTODLL);
  end;

  if FSSLDLL <> 0 then
     FreeLibrary(FSSLDLL);

  inherited;
end;

procedure TisSSLLibrary.DoOnConstruction;
begin
  inherited;
  FSSLDLL := LoadLibrary('ssleay32.dll');
  CheckCondition(FSSLDLL <> 0, 'Cannot load ssleay32.dll');

  FCRYPTODLL := LoadLibrary('libeay32.dll');
  CheckCondition(FCRYPTODLL <> 0, 'Cannot load libeay32.dll');

  InitFuncEntryPoints;

  CheckCondition(SSL_library_init > 0, 'Cannot initialize OpenSSL library');
  SSL_load_error_strings;

  InitCallbacks;

  SSLLibObj := Self;
end;

procedure TisSSLLibrary.FreeSSLContext(ctx: Pointer);
begin
  try
    SSL_CTX_free(ctx);
  except
    // to avoid stupid error
  end;
end;

function TisSSLLibrary.GetErrorStr(const AErrorCode: Integer): String;
var
  buf: String;
begin
  SetLength(buf, 1000);
  ERR_error_string(AErrorCode, @buf[1]);
  Result := PChar(buf);
end;

function TisSSLLibrary.GetLockByIndex(const AIndex: Integer): IisLock;
begin
  Result := FLocks[AIndex] as IisLock;
end;

procedure TisSSLLibrary.InitCallbacks;

  function sslThreadIDCallback: integer cdecl;
  begin
    Result := GetCurrentThreadId;
  end;

  procedure sslLockingCallback(mode, n : integer; Afile : PChar; line : integer) cdecl;
  var
    L: IisLock;
  begin
    L := SSLLibObj.GetLockByIndex(n);
    if (mode and CRYPTO_LOCK) <> 0 then
      L.Lock
    else
      L.Unlock;
  end;

var
  i, n: Integer;
begin
  FLocks := TisList.Create;
  n := CRYPTO_num_locks;
  for i := 1 to n do
    FLocks.Add(TisLock.Create);

  CRYPTO_set_locking_callback(@sslLockingCallback);

  CRYPTO_set_id_callback(@sslThreadIDCallback);
end;

procedure TisSSLLibrary.InitFuncEntryPoints;
begin
  @SSL_library_init := GetProcAddress(FSSLDLL, 'SSL_library_init');
  @SSL_load_error_strings := GetProcAddress(FSSLDLL, 'SSL_load_error_strings');
  @SSLv3_server_method := GetProcAddress(FSSLDLL, 'SSLv3_server_method');
  @SSLv3_client_method := GetProcAddress(FSSLDLL, 'SSLv3_client_method');
  @SSLv23_server_method := GetProcAddress(FSSLDLL, 'SSLv23_server_method');
  @SSLv23_client_method := GetProcAddress(FSSLDLL, 'SSLv23_client_method');
  @SSLv2_server_method := GetProcAddress(FSSLDLL, 'SSLv2_server_method');
  @SSLv2_client_method := GetProcAddress(FSSLDLL, 'SSLv2_client_method');
  @SSLv3_method := GetProcAddress(FSSLDLL, 'SSLv3_method');
  @SSL_CTX_new := GetProcAddress(FSSLDLL, 'SSL_CTX_new');
  @SSL_CTX_free := GetProcAddress(FSSLDLL, 'SSL_CTX_free');
  @SSL_CTX_set_default_passwd_cb := GetProcAddress(FSSLDLL, 'SSL_CTX_set_default_passwd_cb');
  @SSL_CTX_set_default_passwd_cb_userdata := GetProcAddress(FSSLDLL, 'SSL_CTX_set_default_passwd_cb_userdata');
  @SSL_CTX_use_certificate_file := GetProcAddress(FSSLDLL, 'SSL_CTX_use_certificate_file');
  @SSL_CTX_set_default_verify_paths := GetProcAddress(FSSLDLL, 'SSL_CTX_set_default_verify_paths');
  @SSL_CTX_load_verify_locations := GetProcAddress(FSSLDLL, 'SSL_CTX_load_verify_locations');
  @SSL_CTX_use_PrivateKey_file := GetProcAddress(FSSLDLL, 'SSL_CTX_use_PrivateKey_file');
  @SSL_CTX_check_private_key := GetProcAddress(FSSLDLL, 'SSL_CTX_check_private_key');
  @SSL_CTX_set_cipher_list := GetProcAddress(FSSLDLL, 'SSL_CTX_set_cipher_list');
  @SSL_CTX_set_verify := GetProcAddress(FSSLDLL, 'SSL_CTX_set_verify');
  @SSL_CTX_set_session_id_context := GetProcAddress(FSSLDLL, 'SSL_CTX_set_session_id_context');
  @SSL_CTX_set_client_CA_list := GetProcAddress(FSSLDLL, 'SSL_CTX_set_client_CA_list');
  @SSL_CTX_ctrl := GetProcAddress(FSSLDLL, 'SSL_CTX_ctrl');
  @X509_STORE_CTX_get_current_cert := GetProcAddress(FCRYPTODLL, 'X509_STORE_CTX_get_current_cert');
  @X509_STORE_CTX_get_error := GetProcAddress(FCRYPTODLL, 'X509_STORE_CTX_get_error');
  @X509_get_issuer_name := GetProcAddress(FCRYPTODLL, 'X509_get_issuer_name');
  @X509_get_subject_name := GetProcAddress(FCRYPTODLL, 'X509_get_subject_name');
  @X509_NAME_oneline := GetProcAddress(FCRYPTODLL, 'X509_NAME_oneline');
  @SSL_new := GetProcAddress(FSSLDLL, 'SSL_new');
  @SSL_free := GetProcAddress(FSSLDLL, 'SSL_free');
  @SSL_connect := GetProcAddress(FSSLDLL, 'SSL_connect');
  @SSL_accept := GetProcAddress(FSSLDLL, 'SSL_accept');
  @SSL_set_fd := GetProcAddress(FSSLDLL, 'SSL_set_fd');
  @SSL_read := GetProcAddress(FSSLDLL, 'SSL_read');
  @SSL_write := GetProcAddress(FSSLDLL, 'SSL_write');
  @SSL_shutdown := GetProcAddress(FSSLDLL, 'SSL_shutdown');
  @SSL_get_error := GetProcAddress(FSSLDLL, 'SSL_get_error');
  @ERR_get_error := GetProcAddress(FCRYPTODLL, 'ERR_get_error');
  @ERR_error_string := GetProcAddress(FCRYPTODLL, 'ERR_error_string');
  @BIO_new_ssl := GetProcAddress(FSSLDLL, 'BIO_new_ssl');
  @BIO_set_fd := GetProcAddress(FSSLDLL, 'BIO_set_fd');
  @CRYPTO_set_id_callback := GetProcAddress(FCRYPTODLL, 'CRYPTO_set_id_callback');
  @CRYPTO_set_locking_callback := GetProcAddress(FCRYPTODLL, 'CRYPTO_set_locking_callback');
  @CRYPTO_num_locks := GetProcAddress(FCRYPTODLL, 'CRYPTO_num_locks');
  @RSA_generate_key := GetProcAddress(FCRYPTODLL, 'RSA_generate_key');
  @RAND_seed := GetProcAddress(FCRYPTODLL, 'RAND_seed');
  @EVP_PKEY_new := GetProcAddress(FCRYPTODLL, 'EVP_PKEY_new');
  @EVP_PKEY_free := GetProcAddress(FCRYPTODLL, 'EVP_PKEY_free');

end;

function TisSSLLibrary.NewClientContext(const CAFile: String): Pointer;
begin
  Result := SSL_CTX_new(SSLv23_client_method);

  if Length(CAFile) > 0 then
  begin
    CheckError(SSL_CTX_set_default_verify_paths(Result));
    CheckError(SSL_CTX_load_verify_locations(Result, PChar(CAFile), nil));
    SSL_CTX_set_client_CA_list(Result, PChar(CAFile));
    SSL_CTX_ctrl(Result, SSL_CTRL_OPTIONS, SSL_OP_ALL or SSL_OP_NO_SSLv2, nil);
    CheckError(SSL_CTX_set_cipher_list(Result, ClientDefaultCipherList));
    SSL_CTX_set_verify(Result, SSL_VERIFY_PEER, @VerifyCallback);
  end;
end;

function TisSSLLibrary.NewServerContext(const RootCertFile, CertFile, KeyFile: String): Pointer;
begin
  Result := SSL_CTX_new(SSLv23_server_method);

  CheckError(SSL_CTX_set_default_verify_paths(Result));
  CheckError(SSL_CTX_load_verify_locations(Result, PAnsiChar(RootCertFile), PAnsiChar(ExtractFilePath(RootCertFile))));
  CheckError(SSL_CTX_use_certificate_file(Result, PAnsiChar(CertFile), 1));
  CheckError(SSL_CTX_use_PrivateKey_file(Result, PAnsiChar(KeyFile), 1));
  CheckError(SSL_CTX_check_private_key(Result));
  SSL_CTX_ctrl(Result, SSL_CTRL_OPTIONS, SSL_OP_ALL or SSL_OP_NO_SSLv2, nil);
  CheckError(SSL_CTX_set_cipher_list(Result, ServerDefaultCipherList));
end;

function TisSSLLibrary.SSL_CTX_set_mode(ctx: Pointer; mode: Integer): Longint;
begin
  Result := SSL_CTX_ctrl(ctx, SSL_CTRL_MODE, mode, nil);
  if Result <> mode then
    raise EisException.Create('SSL_CTX_ctrl error');
end;

initialization
  Lock := TisLock.Create;

end.
