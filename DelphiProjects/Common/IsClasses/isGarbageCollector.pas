unit isGarbageCollector;

interface

uses Windows, Classes, SysUtils, isBaseClasses, isThreadManager;

type
  IisGarbageCollector = interface
  ['{065B1A52-A347-437A-B353-58AD26A8CBF5}']
    procedure Collect(const AObject: IInterface); overload;
    procedure Collect(const AObjects: array of IInterface); overload;
    procedure Collect(const AObject: TObject); overload;
    procedure Collect(const AObjects: array of TObject); overload;
  end;

  function GarbageCollector: IisGarbageCollector;

implementation

type
  TisObjInfo = record
    Obj: TObject;
    Int: IInterface;
  end;

  TisObjBin = array of TisObjInfo;
  PTisObjBin = ^TisObjBin;

  TisGarbageCollector = class;

  TisGBThread = class(TReactiveThread)
  private
    FOwner: TisGarbageCollector;
  protected
    procedure DoWork; override;
  end;

  TisGarbageCollector = class(TisInterfacedObject, IisGarbageCollector)
  private
    FLock: IisLock;
    FBin: PTisObjBin;
    FBinTopPos: Integer;
    FThread: TisGBThread;
    procedure CreateNewBin;
    function  ChangeBin: PTisObjBin;
    procedure CleanBin(const ABin: PTisObjBin);
  protected
    procedure DoOnConstruction; override;
    procedure Collect(const AObject: IInterface); overload;
    procedure Collect(const AObjects: array of IInterface); overload;
    procedure Collect(const AObject: TObject); overload;
    procedure Collect(const AObjects: array of TObject); overload;
  public
    class function GetTypeID: String; override;
    destructor  Destroy; override;
  end;

var FGarbageCollector: IisGarbageCollector;
const GC_BIN_SIZE = 10000;

function GarbageCollector: IisGarbageCollector;
begin
  Result := FGarbageCollector;
end;

{ TisGarbageCollector }

procedure TisGarbageCollector.Collect(const AObjects: array of IInterface);
var
  i: Integer;
begin
  FLock.Lock;
  try
    for i := Low(AObjects) to High(AObjects) do
      if AObjects[i] <> nil then
      begin
        Inc(FBinTopPos);
        if FBinTopPos <= High(FBin^) then
          FBin^[FBinTopPos].Int := AObjects[i]
        else
        begin
          Dec(FBinTopPos);
          FThread.Wakeup;          
          break;
        end;
      end;
  finally
    FLock.Unlock;
  end;
end;

procedure TisGarbageCollector.Collect(const AObject: IInterface);
begin
  if AObject <> nil then
  begin
    FLock.Lock;
    try
      Inc(FBinTopPos);
      if FBinTopPos <= High(FBin^) then
        FBin^[FBinTopPos].Int := AObject
      else
      begin
        Dec(FBinTopPos);
        FThread.Wakeup;
      end;
    finally
      FLock.Unlock;
    end;
  end;
end;

function TisGarbageCollector.ChangeBin: PTisObjBin;
begin
  FLock.Lock;
  try
    if FBinTopPos = -1 then
      Result := nil
    else
    begin
      if (FBin <> nil) and (FBinTopPos < Length(FBin^)) then
        SetLength(FBin^, FBinTopPos + 1);
      Result := FBin;
      CreateNewBin;
    end;  
  finally
    FLock.Unlock;  
  end;
end;

procedure TisGarbageCollector.Collect(const AObjects: array of TObject);
var
  i, bFreeInd: Integer;

begin
  bFreeInd := -1;
  FLock.Lock;
  try
    for i := Low(AObjects) to High(AObjects) do
      if AObjects[i] <> nil then
      begin
        Inc(FBinTopPos);
        if FBinTopPos <= High(FBin^) then
          FBin^[FBinTopPos].Obj := AObjects[i]
        else
        begin
          Dec(FBinTopPos);
          bFreeInd := i;
          break;
        end;
      end;
  finally
    FLock.Unlock;
  end;

  if bFreeInd <> -1 then
  begin
    FThread.Wakeup;
    for i := bFreeInd to High(AObjects) do
      AObjects[i].Free;
  end;    
end;

procedure TisGarbageCollector.Collect(const AObject: TObject);
var
  bFree: Boolean;
begin
  if AObject <> nil then
  begin
    bFree := False;
    FLock.Lock;
    try
      Inc(FBinTopPos);
      if FBinTopPos <= High(FBin^) then
        FBin^[FBinTopPos].Obj := AObject
      else
      begin
        Dec(FBinTopPos);
        bFree := True;
      end;
    finally
      FLock.Unlock;
    end;

    if bFree then
    begin
      FThread.Wakeup;    
      AObject.Free;
    end;
  end;
end;

destructor TisGarbageCollector.Destroy;
begin
  FreeAndNil(FThread);
  CleanBin(FBin);
  inherited;
end;

procedure TisGarbageCollector.DoOnConstruction;
begin
  inherited;
  FLock := TisLock.Create;
  CreateNewBin;
  FThread := TisGBThread.Create('Garbage Collector');
  FThread.WaitTime := 10000;
  FThread.FOwner := Self;
end;

class function TisGarbageCollector.GetTypeID: String;
begin
  Result := 'GarbageCollector';
end;

procedure TisGarbageCollector.CleanBin(const ABin: PTisObjBin);
var
  i: Integer;
  PObjInf: ^TisObjInfo;
begin
  if ABin <> nil then
    try
      for i := Low(ABin^) to High(ABin^) do
        try
          PObjInf := @ABin^[i];
          if PObjInf^.Obj <> nil then
            FreeAndNil(PObjInf^.Obj);
          PObjInf^.Int := nil;
        except
          // need to log errors?
        end;
    finally
      Dispose(ABin);
    end;
end;

procedure TisGarbageCollector.CreateNewBin;
begin
  New(FBin);
  SetLength(FBin^, GC_BIN_SIZE);
  ZeroMemory(@FBin^[0], GC_BIN_SIZE);
  FBinTopPos := -1;
end;

{ TisGBThread }

procedure TisGBThread.DoWork;
var
  B: PTisObjBin;
begin
  B := FOwner.ChangeBin;
  if B <> nil then
    FOwner.CleanBin(B);
end;

initialization
  FGarbageCollector := TisGarbageCollector.Create;

end.