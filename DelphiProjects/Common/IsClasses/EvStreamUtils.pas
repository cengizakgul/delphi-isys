// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvStreamUtils;

{$W-}
{$O+}
{$Q-}
{$R-}

interface

uses
  Classes, Math, PsAPI, IsTypes,
  Windows, ActiveX, AxCtrls, ComObj, isBufferedStream,
  SysUtils, Variants, RTLConsts;

type
  IisStream = interface
  ['{68A5CDAD-B912-4AC6-9556-29ADAA09F143}']
    procedure WriteBuffer(const Buffer; Count: Longint);
    procedure ReadBuffer(var Buffer; Count: Longint);
    function  Read(var Buffer; Count: Longint): Longint;
    procedure WriteInteger(const i: Integer);
    procedure WriteInt64(const i: Int64);
    procedure WriteString(const s: string);
    procedure WriteShortString(const s: string);
    procedure WriteByte(const b: Byte);
    procedure WriteWord(const w: Word);
    procedure WriteBoolean(const b: Boolean);
    procedure WriteDouble(const d: Double);
    procedure WriteCurrency(const c: Currency);
    procedure WriteVarArrayOfByte(const v: Variant);
    procedure WriteVariant(const v: Variant);
    procedure WriteStream(const s: IisStream);
    procedure WriteObject(const AObject: IInterface);
    procedure WriteLn(const AText: String);
    procedure WriteDynVarArray(const Arr: TisDynVariantArray);
    procedure WriteComponent(Instance: TComponent);
    function  ReadUntil(const AByte: Byte): String; overload;
    function  ReadLn(const EOLByte: Byte = 10): String;
    function  ReadInteger: Integer;
    function  ReadInt64: Int64;
    function  ReadString: string;
    function  ReadShortString: string;
    function  ReadBoolean: Boolean;
    function  ReadByte: Byte;
    function  ReadWord: Word;
    function  ReadDouble: Double;
    function  ReadCurrency: Currency;
    function  ReadStream(s: IisStream; const DoNotCreateEmpty: Boolean = False): IisStream;
    function  ReadObject(const AObject: IInterface): IInterface;
    function  ReadVarArrayOfByte: Variant;
    function  ReadVariant: Variant;
    function  ReadDynVarArray: TisDynVariantArray;
    function  ReadComponent(Instance: TComponent): TComponent;
    function  RealStream: TStream;
    function  EOS: Boolean;
    function  GetSize: Int64;
    procedure SetSize(const Value: Int64);
    function  GetPosition: Int64;
    procedure SetPosition(const Value: Int64);
    function  Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
    procedure Clear;
    function  AsVariant: Variant;
    function  AsVarArrayOfBytes: Variant;
    function  Memory: Pointer;
    procedure LoadFromFile(const FileName: string);
    procedure SaveToFile(const FileName: string);
    function  GetOleAdapter: IStream;
    function  GetClone: IisStream;
    function  FileName: String;
    procedure Flush;
    function  GetAsOleVariant: OleVariant;
    procedure SetAsOleVariant(const AValue: OleVariant);
    function  GetAsString: String;
    procedure SetAsString(const AValue: String);
    function  CopyFrom(const AStream: IisStream; ACount: Int64 = 0): Int64;
    function  GetGrowChunkSize: Integer;
    procedure SetGrowChunkSize(const AValue: Integer);
    procedure TruncateToPosition;
    function  IsEqualTo(const AStream: IisStream): Boolean;
    property  Size: Int64 read GetSize write SetSize;
    property  Position: Int64 read GetPosition write SetPosition;
    property  AsOleVariant: OleVariant read GetAsOleVariant write SetAsOleVariant;
    property  AsString: String read GetAsString write SetAsString;
    property  GrowChunkSize: Integer read GetGrowChunkSize write SetGrowChunkSize;
  end;

  IEvDualStream = IisStream;

  TisMemoryStream = class(TMemoryStream)
  protected
    FMemoryDelta: Integer;
    function Realloc(var NewCapacity: Longint): Pointer; override;
  public
    constructor Create;
  end;


  TEvFileStream = class(TBufferedStream)
  private
    FFileName: string;
  public
    constructor Create(const FileName: string; Mode: Word); overload;
    constructor Create(const FileName: string; Mode: Word; Rights: Cardinal); overload;
    destructor  Destroy; override;
  end;


  TEvUniqueFileStream = class (TBufferedStream)
  private
    FFileName: string;
    FAutoDelete: Boolean;
  protected
    procedure SetSize(const NewSize: Int64); override;
  public
    constructor Create(const FileName: string; const AutoRename, AutoDelete: Boolean);
    constructor CreateEx(var FileName: string; const AutoRename, AutoDelete: Boolean);
    destructor Destroy; override;
  end;


  TAutoDeleteFileStream = class (TEvFileStream)
  private
    FFileName: string;
  protected
    procedure SetSize(const NewSize: Int64); override;
  public
    constructor Create(const FileName: string; Mode: Word);
    destructor Destroy; override;
  end;


  TVarArrayStream = class (TCustomMemoryStream)
  private
    FVariant: Variant;
  public
    property BaseVariant: Variant read FVariant;
    constructor Create(const v: Variant);
    destructor Destroy; override;
    procedure SetSize(NewSize: Longint); override;
    function Write(const Buffer; Count: Longint): Longint; override;
  end;


  TEvStreamAdapter = class(TStreamAdapter)
  private
    FStream: IisStream;
  public
    constructor Create(const AStream: IisStream);
  end;

  // Fixing Borland's "features"  :o)
  TEvOleStream = class(TOleStream)
  protected
    procedure SetSize(NewSize: Longint); override;  // TOleStream is read only stream !? Only for SetSize operation??? Ah! What a usefull "feature"! :)
  end;


  TisStream = class (TInterfacedObject, IisStream)
  private
    FStream: TStream;
    FIsFileStream: Boolean;
    FDoNotSwapToFile: Boolean;
    FExternalStream: Boolean;
    FGrowChunkSize: Integer;
    FLastMemCheckPos: Int64;
    function GetSize: Int64;
    procedure SetSize(const Value: Int64);
    function GetPosition: Int64;
    procedure SetPosition(const Value: Int64);
    function  WillFitInMemory(const ANewSize: Int64): Boolean;
    procedure SwitchToFileStream;
    procedure CheckStreamType;
  protected
  public
    constructor Create(const ProjectedSize: Int64 = -1; const InMemoryOnly: Boolean = False);
    constructor CreateInMemory;
    constructor CreateFromFile(const ExistingFileName: string; const DeleteOnDestroy: Boolean = False;
                               const ReadOnlyAccess: Boolean = True; const ShareMode: Boolean = False;
                               const CreateIfNotExist: Boolean = False; const RetryTimes: Integer = 0);
    constructor CreateFromNewFile(const FileName: string; const DeleteOnDestroy: Boolean = False;
                                  const ShareMode: Boolean = False);
    constructor CreateFromVariant(const v: Variant);
    constructor CreateFromAuxStream(const AuxStream: TStream; const ATakeOwnership: Boolean = True);
    constructor CreateFromOleStream(const Stream: IStream);
    destructor  Destroy; override;
    //
    function  CheckBeforeWrite(const Count: Int64): Boolean;
    procedure WriteBuffer(const Buffer; Count: Longint);
    procedure ReadBuffer(var Buffer; Count: Longint);
    function  Read(var Buffer; Count: Longint): Longint;
    procedure WriteInteger(const i: Integer);
    procedure WriteInt64(const i: Int64);
    procedure WriteString(const s: string);
    procedure WriteShortString(const s: string);
    procedure WriteByte(const b: Byte);
    procedure WriteWord(const w: Word);
    procedure WriteBoolean(const b: Boolean);
    procedure WriteDouble(const d: Double);
    procedure WriteCurrency(const c: Currency);
    procedure WriteVarArrayOfByte(const v: Variant);
    procedure WriteVariant(const v: Variant);
    procedure WriteDynVarArray(const Arr: TisDynVariantArray);
    procedure WriteStream(const s: IisStream);
    procedure WriteObject(const AObject: IInterface);
    procedure WriteLn(const AText: String);
    procedure WriteComponent(Instance: TComponent);
    function  ReadUntil(const AByte: Byte): String; overload;
    function  ReadLn(const EOLByte: Byte = 10): String;
    function  ReadInteger: Integer;
    function  ReadInt64: Int64;
    function  ReadString: string;
    function  ReadShortString: string;
    function  ReadBoolean: Boolean;
    function  ReadByte: Byte;
    function  ReadWord: Word;
    function  ReadDouble: Double;
    function  ReadCurrency: Currency;
    function  ReadStream(s: IisStream; const DoNotCreateEmpty: Boolean = False): IisStream;
    function  ReadObject(const AObject: IInterface): IInterface;
    function  ReadVarArrayOfByte: Variant;
    function  ReadVariant: Variant;
    function  ReadDynVarArray: TisDynVariantArray;
    function  ReadComponent(Instance: TComponent): TComponent;    
    function  RealStream: TStream;
    function  EOS: Boolean;
    property  Size: Int64 read GetSize write SetSize;
    property  Position: Int64 read GetPosition write SetPosition;
    function  Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
    procedure Clear;
    function  AsVariant: Variant;
    function  AsVarArrayOfBytes: Variant;
    function  Memory: Pointer;
    procedure LoadFromFile(const FileName: string);
    procedure SaveToFile(const FileName: string);
    function  GetOleAdapter: IStream;
    function  FileName: String;
    procedure Flush;
    function  GetAsOleVariant: OleVariant;
    procedure SetAsOleVariant(const AValue: OleVariant);
    function  GetAsString: String;
    procedure SetAsString(const AValue: String);
    function  CopyFrom(const AStream: IisStream; ACount: Int64 = 0): Int64;
    function  GetClone: IisStream;
    function  GetGrowChunkSize: Integer;
    procedure SetGrowChunkSize(const AValue: Integer);
    procedure TruncateToPosition;
    function  IsEqualTo(const AStream: IisStream): Boolean;
  end;

  TEvDualStreamHolder = TisStream;


function StreamIsAssigned(const AStream: IisStream): Boolean;

implementation

// stream methods

uses
  ISBasicUtils, isExceptions, isBaseClasses;


{ TisStream }

function TisStream.AsVarArrayOfBytes: Variant;
var
  p: Pointer;
begin
   Position := 0;
   Result := VarArrayCreate([0, Size- 1], VarByte);
   try
      p := VarArrayLock(Result);
      try
        ReadBuffer(p^, Size);
      finally
         VarArrayUnlock(Result);
         Position := 0;
      end;
   except
      Result:=Unassigned;
   end;
end;

function TisStream.AsVariant: Variant;
begin
  Position := 0;
  Result := ReadVariant;
end;

procedure TisStream.Clear;
begin
  FStream.Size := 0;
end;

constructor TisStream.Create(const ProjectedSize: Int64 = -1; const InMemoryOnly: Boolean = False);
begin
  if InMemoryOnly or (ProjectedSize = -1) or WillFitInMemory(ProjectedSize) then
    FStream := TisMemoryStream.Create
  else
    FStream := TEvUniqueFileStream.Create(AppTempFolder + GetUniqueID + '.tmp', True, True);

  CheckStreamType;
  FDoNotSwapToFile := InMemoryOnly;
end;

constructor TisStream.CreateFromAuxStream(const AuxStream: TStream; const ATakeOwnership: Boolean = True);
begin
  FStream := AuxStream;
  FDoNotSwapToFile := True;
  FExternalStream := not ATakeOwnership;    
end;

constructor TisStream.CreateFromFile(const ExistingFileName: string; const DeleteOnDestroy: Boolean = False;
  const ReadOnlyAccess: Boolean = True; const ShareMode: Boolean = False;
  const CreateIfNotExist: Boolean = False; const RetryTimes: Integer = 0);
var
  fm: Word;
  i: Integer;

  procedure DoCreateStream;
  begin
    if DeleteOnDestroy then
      FStream := TAutoDeleteFileStream.Create(ExistingFileName, fm)
    else
      FStream := TEvFileStream.Create(ExistingFileName, fm);
  end;

begin
  if ReadOnlyAccess then
    fm := fmOpenRead
  else
    fm := fmOpenReadWrite;

  if ShareMode then
    fm := fm OR fmShareDenyNone
  else
    fm := fm OR fmShareDenyWrite;

  if RetryTimes > 0 then
  begin
    for i := 1 to RetryTimes do
      try
        if CreateIfNotExist and not FileExists(ExistingFileName) then
        begin
          CreateFromNewFile(ExistingFileName, DeleteOnDestroy, ShareMode);
          break;
        end
        else
        begin
          WaitIfFileUsed(ExistingFileName);
          DoCreateStream;
          break;
        end;
      except
        FStream := nil;
        if i = RetryTimes then
          raise;
      end;
  end
  else
    DoCreateStream;

  CheckStreamType;
end;

constructor TisStream.CreateFromNewFile(const FileName: string;
  const DeleteOnDestroy: Boolean = False; const ShareMode: Boolean = False);
var
  fm: Word;
begin
  if ShareMode then
    fm := fmCreate OR fmShareDenyNone  
  else
    fm := fmCreate OR fmShareDenyWrite;

  if DeleteOnDestroy then
    FStream := TAutoDeleteFileStream.Create(FileName, fm)
  else
    FStream := TEvFileStream.Create(FileName, fm);

  CheckStreamType;    
end;

constructor TisStream.CreateFromOleStream(const Stream: IStream);
begin
  FStream := TEvOleStream.Create(Stream);
  FDoNotSwapToFile := True;
end;

constructor TisStream.CreateFromVariant(const v: Variant);
begin
  FStream := TVarArrayStream.Create(v);
end;

constructor TisStream.CreateInMemory;
begin
  Create(-1, True);
end;

destructor TisStream.Destroy;
begin
  if not FExternalStream then
    FreeAndNil(FStream);
  inherited;
end;

function TisStream.EOS: Boolean;
begin
  Result := FStream.Position = FStream.Size;
end;

function TisStream.FileName: String;
begin
  if FStream is TEvFileStream then
    Result := TEvFileStream(FStream).FFileName
  else if FStream is TEvUniqueFileStream then
    Result := TEvUniqueFileStream(FStream).FFileName
  else
    Result := '';
end;

procedure TisStream.Flush;
begin
  if FStream is TBufferedStream then
    TBufferedStream(FStream).FlushBuffer;

 if FStream is THandleStream then
   FlushFileBuffers(THandleStream(FStream).Handle);
end;

function TisStream.GetOleAdapter: IStream;
begin
  Result := TEvStreamAdapter.Create(Self);
end;

function TisStream.GetPosition: Int64;
begin
  Result := FStream.Position;
end;

function TisStream.GetSize: Int64;
begin
  Result := FStream.Size;
end;

procedure TisStream.LoadFromFile(const FileName: string);
var
  FS: IisStream;
begin
  FS := TisStream.CreateFromFile(FileName, False, True, True);
  SetSize(0);
  CopyFrom(FS, 0);
end;

function TisStream.Memory: Pointer;
begin
  if FStream is TMemoryStream then
    Result := TMemoryStream(FStream).Memory
  else
    Result := nil;
end;

function TisStream.ReadBoolean: Boolean;
begin
  Result := ReadByte = 1;
end;

procedure TisStream.ReadBuffer(var Buffer; Count: Integer);
begin
  FStream.ReadBuffer(Buffer, Count);
end;

function TisStream.ReadByte: Byte;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TisStream.ReadCurrency: Currency;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TisStream.ReadDouble: Double;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TisStream.ReadInt64: Int64;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TisStream.ReadInteger: Integer;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

function TisStream.ReadLn(const EOLByte: Byte = 10): String;
begin
  Result := ReadUntil(EOLByte);
  if (Length(Result) > 0) and (Result[Length(Result)] = #13) then
    SetLength(Result, Length(Result) - 1);
end;

function TisStream.ReadStream(s: IisStream; const DoNotCreateEmpty: Boolean = False): IisStream;
begin
  if not Assigned(s) then
    Result := TisStream.Create
  else
    Result := s;

  Result.Size := ReadInteger;
  if Result.Size > 0 then
  begin
    Result.CopyFrom(Self, Result.Size);
    Result.Position := 0;
  end
  else if  DoNotCreateEmpty then
    Result := nil;
end;

function TisStream.ReadString: string;
var
  i: Integer;
begin
  i := ReadInteger;

  CheckCondition(i <= Size - Position, 'Stream read error');

  SetLength(Result, i);
  if i > 0 then
    ReadBuffer(Result[1], i);
end;

function TisStream.ReadVarArrayOfByte: Variant;
var
  i: Integer;
  p: Pointer;
begin
  i := ReadInteger;
  if i > 0 then
  begin
    Result := VarArrayCreate([0, i-1], varByte);
    p := VarArrayLock(Result);
    try
      ReadBuffer(p^, i)
    finally
      VarArrayUnlock(Result);
    end;
  end
  else
    Result := Null;
end;

function TisStream.ReadVariant: Variant;

  procedure StreamToVar(var Res: Variant); overload;
  var
    VarType: Integer;
    I, DimCount, ElemCount, DimElemCount, ElemSize, Len: Integer;
    StrLen: Integer;
    ArrayData: Pointer;
    ArrayDims: array of Integer;
    IntStream: IisStream;
  begin
    ReadBuffer(VarType, SizeOf(Integer));
    if (VarType and varArray) <> 0 then
    begin
      ReadBuffer(DimCount, SizeOf(DimCount));
      SetLength(ArrayDims, DimCount * 2);
      ElemCount:= 1;
      for I:= 0 to DimCount - 1 do
      begin
        ReadBuffer(DimElemCount , SizeOf(DimElemCount));
        ElemCount:= ElemCount * DimElemCount;
        ArrayDims[I * 2]:= 0;
        ArrayDims[I * 2 + 1]:= DimElemCount - 1;
      end;
      Res := VarArrayCreate(ArrayDims, VarType and varTypeMask);
      try
        ArrayData:= VarArrayLock(Res);
        for I:= 0 to ElemCount - 1 do
        begin
          case VarType and varTypeMask of
            varShortInt, varByte:
            begin
              ElemSize:= SizeOf(Byte);
              ReadBuffer(PByte(ArrayData)^, ElemSize);
            end;
            varSmallint, varWord:
            begin
              ElemSize:= SizeOf(SmallInt);
              ReadBuffer(PSmallInt(ArrayData)^, ElemSize);
            end;
            varInteger, varLongWord:
            begin
              ElemSize:= SizeOf(Integer);
              ReadBuffer(PInteger(ArrayData)^, ElemSize);
            end;
            varSingle:
            begin
              ElemSize:= SizeOf(Single);
              ReadBuffer(PSingle(ArrayData)^, ElemSize);
            end;
            varDouble:
            begin
              ElemSize:= SizeOf(Double);
              ReadBuffer(PDouble(ArrayData)^, ElemSize);
            end;
            varCurrency:
            begin
              ElemSize:= SizeOf(Currency);
              ReadBuffer(Currency(ArrayData^), ElemSize);
            end;
            varDate:
            begin
              ElemSize:= SizeOf(TDateTime);
              ReadBuffer(TDateTime(ArrayData^), ElemSize);
            end;
            varOleStr:
            begin
              ElemSize:= SizeOf(PWideChar);
              ReadBuffer(Len, SizeOf(Len));
              PWideString(ArrayData)^ := WideString(StringOfChar(' ', Len));
              ReadBuffer(PWideString(ArrayData)^[1], Len * SizeOf(WideChar));
            end;
  {         varString:    This array item type is not supported by Delphi!
            begin
              ElemSize:= SizeOf(PChar);
              ReadBuffer(Len, SizeOf(Len));
              string(ArrayData^) := StringOfChar(' ', Len);
              Read(PChar(ArrayData)^, Len * SizeOf(Char));
            end;
  }
            varBoolean:
            begin
              ElemSize:= SizeOf(WordBool);
              ReadBuffer(WordBool(ArrayData^), ElemSize);
            end;
            varVariant:
            begin
              ElemSize:= SizeOf(TVarData);
              Variant(PVarData(ArrayData)^) := Unassigned;
              StreamToVar(Variant(PVarData(ArrayData)^));
            end;
          else
            raise ISException.Create('Unsupported type: ' + IntToStr(VarType));
            ElemSize := 0;
          end;
          ArrayData:= Pointer(Integer(ArrayData) + ElemSize);
        end;
      finally
        VarArrayUnlock(Res);
      end;
    end
    else
    begin
      case VarType and varTypeMask of
      varEmpty:
        Res := Unassigned;
      varNull:
        Res := null;
      varSmallint:
        ReadBuffer(TVarData(Res).VSmallint , SizeOf(TVarData(Res).VSmallint));
      varShortint:
        ReadBuffer(TVarData(Res).VShortint , SizeOf(TVarData(Res).VShortint));
      varInteger:
        ReadBuffer(TVarData(Res).VInteger , SizeOf(TVarData(Res).VInteger));
      varInt64:
        ReadBuffer(TVarData(Res).VInt64 , SizeOf(TVarData(Res).VInt64));
      varSingle:
        ReadBuffer(TVarData(Res).VSingle, SizeOf(TVarData(Res).VSingle));
      varDouble:
        ReadBuffer(TVarData(Res).VDouble, SizeOf(TVarData(Res).VDouble));
      varCurrency:
        ReadBuffer(TVarData(Res).VCurrency, SizeOf(TVarData(Res).VCurrency));
      varDate:
        ReadBuffer(TVarData(Res).VDate, SizeOf(TVarData(Res).VDate));
      varBoolean:
        ReadBuffer(TVarData(Res).VBoolean, SizeOf(TVarData(Res).VBoolean));
      varByte:
        ReadBuffer(TVarData(Res).VByte, SizeOf(TVarData(Res).VByte));
      varWord:
        ReadBuffer(TVarData(Res).VWord, SizeOf(TVarData(Res).VWord));
      varLongWord:
        ReadBuffer(TVarData(Res).VLongWord, SizeOf(TVarData(Res).VLongWord));
      varOleStr:
      begin
        Read(StrLen, SizeOf(StrLen));
        Res := WideString(StringOfChar(' ', StrLen));
        Read(TVarData(Res).VOleStr^, StrLen*SizeOf(WideChar));
      end;
      varString:
      begin
        Read(StrLen, SizeOf(StrLen));
        Res := StringOfChar(' ', StrLen);
        Read(TVarData(Res).VString^, StrLen*SizeOf(Char));
      end;
      varUnknown:
      begin
        Read(StrLen, SizeOf(StrLen));
        if StrLen = -1 then
          IntStream := nil
        else
        begin
          IntStream := TisStream.Create(StrLen);
          if StrLen > 0 then
          begin
            IntStream.CopyFrom(Self, StrLen);
            IntStream.Position := 0;
          end;
        end;

        Res := IntStream;
        IntStream := nil;
      end;

      else
        raise ISException.Create('Unsupported type: ' + IntToStr(VarType));
      end;
      
      TVarData(Res).VType := VarType;
    end;
  end;

var
  ver: byte;
begin
  ReadBuffer(ver, SizeOf(ver));
  Assert(ver = $FF);
  StreamToVar(Result);
end;

function TisStream.RealStream: TStream;
begin
  Result := FStream;
end;

procedure TisStream.SaveToFile(const FileName: string);
var
  FS: IisStream;
begin
  FS := TisStream.CreateFromNewFile(FileName);
  FS.CopyFrom(Self, 0);
end;

function TisStream.Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
begin
  Result := FStream.Seek(Offset, Origin);
end;

procedure TisStream.SetPosition(const Value: Int64);
begin
  FStream.Position := Value;
end;

procedure TisStream.SetSize(const Value: Int64);
begin
  if not FIsFileStream and (Value > FStream.Size) then
    CheckBeforeWrite(Value - FStream.Size);
  FStream.Size := Value;
  FLastMemCheckPos := Value;
end;

procedure TisStream.WriteBoolean(const b: Boolean);
begin
  if b then
    WriteByte(1)
  else
    WriteByte(0);
end;

procedure TisStream.SwitchToFileStream;
var
  OldStream, NewStream: TStream;
  OldPosition: Int64;
begin
  OldStream := FStream;
  OldPosition := FStream.Position;

  NewStream := TEvUniqueFileStream.Create(AppTempFolder + GetUniqueID + '.tmp', True, True);
  try
    OldStream.Position := 0;
    if OldStream.Size > 0 then
      NewStream.WriteBuffer(TMemoryStream(OldStream).Memory^, OldStream.Size);
    NewStream.Position := OldPosition;
    FStream := NewStream;
    OldStream.Free;
  except
    NewStream.Free;
  end;

  CheckStreamType;
end;

function TisStream.CheckBeforeWrite(const Count: Int64): Boolean;
var
  ProjectedSize: Int64;
  NeedToGrow: Boolean;
begin
  ProjectedSize := FStream.Position + Count;
  NeedToGrow := ProjectedSize > FStream.Size;

  if NeedToGrow and not FIsFileStream and (FGrowChunkSize > 0) then
    Inc(ProjectedSize, FGrowChunkSize);

  if NeedToGrow and not FDoNotSwapToFile and not FIsFileStream and not WillFitInMemory(ProjectedSize) then
  begin
    SwitchToFileStream;
    Result := True;
  end
  else
    Result := False;

  if not FIsFileStream and NeedToGrow then
    try
      FStream.Size := ProjectedSize;
    except
      on E: EOutOfMemory do
      begin
        SwitchToFileStream;
        Result := True;
      end;
    end;
end;

procedure TisStream.WriteBuffer(const Buffer; Count: Integer);
begin
  CheckBeforeWrite(Count);
  FStream.WriteBuffer(Buffer, Count);
end;

procedure TisStream.WriteByte(const b: Byte);
begin
  WriteBuffer(b, SizeOf(b));
end;

procedure TisStream.WriteCurrency(const c: Currency);
begin
  WriteBuffer(c, SizeOf(c));
end;

procedure TisStream.WriteDouble(const d: Double);
begin
  WriteBuffer(d, SizeOf(d));
end;

procedure TisStream.WriteInteger(const i: Integer);
begin
  WriteBuffer(i, SizeOf(i));
end;

procedure TisStream.WriteInt64(const i: Int64);
begin
  WriteBuffer(i, SizeOf(i));
end;

procedure TisStream.WriteLn(const AText: String);
begin
  if Length(AText) > 0 then
    WriteBuffer(AText[1], Length(AText));
  WriteBuffer(#13#10, 2);
end;

procedure TisStream.WriteStream(const s: IisStream);
begin
  if Assigned(s) then
  begin
    WriteInteger(s.Size);
    CopyFrom(s, 0);
  end
  else
    WriteInteger(0);
end;

procedure TisStream.WriteString(const s: string);
var
  i: Integer;
begin
  i := Length(s);
  WriteInteger(i);
  if i > 0 then
    WriteBuffer(s[1], i);
end;

procedure TisStream.WriteVarArrayOfByte(const v: Variant);
var
  p: Pointer;
  i: Integer;
begin
  if VarIsNull(v) then
    WriteInteger(0)
  else
  begin
    if VarType(v) <> (varByte or varArray) then
      raise EisException.Create('Should be Vararray of Bytes');
    Assert(VarArrayLowBound(v, 1)=0, IntToStr(VarArrayLowBound(v, 1)));
    p := VarArrayLock(v);
    i := VarArrayHighBound(v, 1)- VarArrayLowBound(v, 1)+ 1;
    try
      WriteInteger(i);
      WriteBuffer(p^, i);
    finally
      VarArrayUnlock(v);
    end;
  end;
end;

procedure TisStream.WriteVariant(const v: Variant);

  procedure WriteInterfacedData(const AInt: IInterface);
  var
    S: IisStream;
    n: Integer;
  begin
    S := AInt as IisStream;
    if Assigned(S) then
    begin
      S.Position := 0;
      n := S.Size;
      WriteBuffer(n, SizeOf(n));
      CopyFrom(S, n);
    end
    else
    begin
      n := -1;
      WriteBuffer(n, SizeOf(n));
    end;
  end;

  procedure VarToStream(const AValue: Variant);
  var
    I, DimCount, ElemCount, DimElemCount, ElemSize, Len: Integer;
    ArrayData: Pointer;
  begin
    i := VarType(AValue);
    WriteBuffer(i, SizeOf(i));
    if VarIsArray(AValue) then
    begin
      DimCount:= VarArrayDimCount(AValue);
      WriteBuffer(DimCount, SizeOf(DimCount));
      ElemCount:= 1;
      for I:= 0 to DimCount - 1 do
      begin
        DimElemCount:= VarArrayHighBound(AValue, I + 1) - VarArrayLowBound(AValue, I + 1) + 1;
        ElemCount:= ElemCount * DimElemCount;
        WriteBuffer(DimElemCount, SizeOf(DimElemCount));
      end;
      try
        ArrayData:= VarArrayLock(AValue);
        for I:= 0 to ElemCount - 1 do
        begin
          case VarType(AValue) and varTypeMask of
            varShortInt, varByte:
            begin
              ElemSize:= SizeOf(Byte);
              WriteBuffer(PByte(ArrayData)^, ElemSize);
            end;
            varSmallint, varWord:
            begin
              ElemSize:= SizeOf(SmallInt);
              WriteBuffer(PSmallInt(ArrayData)^, ElemSize);
            end;
            varInteger, varLongWord:
            begin
              ElemSize:= SizeOf(Integer);
              WriteBuffer(PInteger(ArrayData)^, ElemSize);
            end;
            varSingle:
            begin
              ElemSize:= SizeOf(Single);
              WriteBuffer(PSingle(ArrayData)^, ElemSize);
            end;
            varDouble:
            begin
              ElemSize:= SizeOf(Double);
              WriteBuffer(PDouble(ArrayData)^, ElemSize);
            end;
            varCurrency:
            begin
              ElemSize:= SizeOf(Currency);
              WriteBuffer(PCurrency(ArrayData)^, ElemSize);
            end;
            varDate:
            begin
              ElemSize:= SizeOf(TDateTime);
              WriteBuffer(PDateTime(ArrayData)^, ElemSize);
            end;
            varOleStr:
              begin
                ElemSize:= SizeOf(PWideChar);
                Len := Length(PWideString(ArrayData)^);
                WriteBuffer(Len, SizeOf(Len));
                WriteBuffer(PWideString(ArrayData)^[1], Len * SizeOf(WideChar));
              end;
{           varString:  This array item type is not supported by Delphi!
              begin
                ElemSize:= SizeOf(PChar);
                Len := Length(PChar(ArrayData));
                WriteData(StrLen, SizeOf(Len));
                WriteData(String(PChar(ArrayData))[1], Len * SizeOf(Char));
              end;
}
            varBoolean:
            begin
              ElemSize:= SizeOf(WordBool);
              WriteBuffer(PWordBool(ArrayData)^, ElemSize);
            end;
            varVariant:
            begin
              ElemSize:= SizeOf(TVarData);
              VarToStream(Variant(PVarData(ArrayData)^));
            end;
          else
            raise ISException.Create('Unsupported type: ' + IntToStr(VarType(AValue)));
            ElemSize := 0;
          end;
          ArrayData:= Pointer(Integer(ArrayData) + ElemSize);
        end;
      finally
        VarArrayUnlock(AValue);
      end;
    end
    else
    begin
      case VarType(AValue) of
        varEmpty, varNull:;
        varShortInt, varByte:
          WriteBuffer(TVarData(AValue).VByte, SizeOf(TVarData(AValue).VByte));
        varSmallint, varWord:
          WriteBuffer(TVarData(AValue).VSmallInt, SizeOf(TVarData(AValue).VSmallInt));
        varInteger, varLongWord:
          WriteBuffer(TVarData(AValue).VInteger, SizeOf(TVarData(AValue).VInteger));
        varInt64:
          WriteBuffer(TVarData(AValue).VInt64, SizeOf(TVarData(AValue).VInt64));
        varSingle:
          WriteBuffer(TVarData(AValue).VSingle, SizeOf(TVarData(AValue).VSingle));
        varDouble:
          WriteBuffer(TVarData(AValue).VDouble, SizeOf(TVarData(AValue).VDouble));
        varCurrency:
          WriteBuffer(TVarData(AValue).VCurrency, SizeOf(TVarData(AValue).VCurrency));
        varDate:
          WriteBuffer(TVarData(AValue).VDate, SizeOf(TVarData(AValue).VDate));
        varOleStr:
          begin
            i := Length(TVarData(AValue).VOleStr);
            WriteBuffer(i, SizeOf(i));
            WriteBuffer(TVarData(AValue).VOleStr^, i*SizeOf(WideChar));
          end;
        varString:
          begin
            i := Length(String(TVarData(AValue).VString));
            WriteBuffer(i, SizeOf(i));
            WriteBuffer(TVarData(AValue).VString^, i*SizeOf(Char));
          end;
        varBoolean:
          WriteBuffer(TVarData(AValue).VBoolean, SizeOf(TVarData(AValue).VBoolean));
        varUnknown:
          WriteInterfacedData(IInterface(TVarData(AValue).VUnknown));
      else
        raise ISException.Create('Unsupported type: ' + IntToStr(VarType(AValue)));
      end;
    end;
  end;

var
  ver: Byte;
begin
  ver := $FF;
  WriteBuffer(ver, SizeOf(ver));
  VarToStream(v);
  TruncateToPosition;
end;

function TisStream.ReadDynVarArray: TisDynVariantArray;
var
  i: Integer;
begin
  SetLength(Result, ReadInteger);
  for i := Low(Result) to High(Result) do
    Result[i] := ReadVariant;
end;

procedure TisStream.WriteDynVarArray(const Arr: TisDynVariantArray);
var
  i: Integer;
begin
  WriteInteger(Length(Arr));
  for i := Low(Arr) to High(Arr) do
    WriteVariant(Arr[i]);
end;


procedure TisStream.WriteShortString(const s: string);
var
  i: Integer;
begin
  i := Length(s);
  CheckCondition(i <= 255, 'String is long');
  WriteByte(i);
  if i > 0 then
    WriteBuffer(s[1], i);
end;

function TisStream.ReadShortString: string;
var
  i: Integer;
begin
  i := ReadByte;
  CheckCondition(i <= Size - Position, 'Stream read error');

  SetLength(Result, i);
  if i > 0 then
    ReadBuffer(Result[1], i);
end;

function TisStream.GetAsOleVariant: OleVariant;
var
  p: Pointer;
  OldPosition: LongInt;
begin
  OldPosition := Position;
  Position := 0;
  Result := VarArrayCreate([0, Size - 1], VarByte);
  p := VarArrayLock(Result);
  try
    ReadBuffer(p^, Size);
  finally
     VarArrayUnlock(Result);
     Position := OldPosition;
  end;
end;


procedure TisStream.SetAsOleVariant(const AValue: OleVariant);
var
  p: Pointer;
begin
  Position := 0;
  Size := 0;

  if not VarIsClear(AValue) then
  begin
    CheckCondition(VarIsArray(AValue), 'Value is not an array');
    p := VarArrayLock(AValue);
    try
      WriteBuffer(p^, VarArrayHighBound(AValue, 1) -  VarArrayLowBound(AValue, 1) + 1);
      Position := 0;
    finally
       VarArrayUnlock(AValue);
    end;
  end;
end;

function TisStream.CopyFrom(const AStream: IisStream; ACount: Int64): Int64;
const
  ChunkSize: Integer = 1024 * 1024 * 1024;  // 1GB 
var
  p: ^Byte;
  chunkPos: Int64;
  writtenBytes: Int64;
  bytesToWrite: Int64;
begin
  if ACount = 0 then
  begin
    ACount := AStream.Size;
    AStream.Position := 0;
  end;

  CheckBeforeWrite(ACount);

  // Handle the situation of copying more than 2GB stream.
  // It should be done by a smaller chunks.
  if ACount > ChunkSize then
  begin
    chunkPos := 0;
    while chunkPos < ACount do
    begin
      bytesToWrite := ACount - chunkPos;
      if bytesToWrite > ChunkSize then
        bytesToWrite := ChunkSize;

      writtenBytes := CopyFrom(AStream, bytesToWrite); // recursion
      Inc(chunkPos, writtenBytes);
    end;

    Result := ACount;
    Exit;
  end;

  if AStream.Memory <> nil then
  begin
    p := AStream.Memory;
    Inc(p, AStream.Position);
    FStream.WriteBuffer(p^, ACount);
    AStream.Position := AStream.Position + ACount;
    Result := ACount;
  end

  else if Memory <> nil then
  begin
    p := Memory;
    Inc(p, Position);
    if Position + ACount > Size then
      Size := Position + ACount;
    Result := AStream.Read(p^, ACount);
    Position := Position + Result;
  end

  else
    Result := FStream.CopyFrom(AStream.RealStream, ACount);
end;

function TisStream.WillFitInMemory(const ANewSize: Int64): Boolean;
const ProcAddrSpace = $80000000; // 2 Gb
      ProcMemThreshold = $80000000 div 5;  //20%
      MemCheckThreshold = 64 * 1024;  // 64 Kb
      MaxMemoryStreamSize = 50 * 1024 * 1024; // 50 Mb
var
  VirtualAvailableMem, PhysicalAvailableMem: Int64;
  MemStatus: TMemoryStatus;
  ProcMemCounters: TProcessMemoryCounters;
begin
  if ANewSize >= MaxMemoryStreamSize then
    Result := False

  else
    if ANewSize - Cardinal(FLastMemCheckPos) > MemCheckThreshold then
    begin
      MemStatus := GetMemStatus;
      ProcMemCounters := GetProcMemStatus;
      if MemStatus.dwTotalPhys > ProcAddrSpace then
      begin
        VirtualAvailableMem :=(ProcAddrSpace - ProcMemCounters.WorkingSetSize); // 2Gb - Used mem
        PhysicalAvailableMem := Min(MemStatus.dwAvailPhys, VirtualAvailableMem);
        Result := (PhysicalAvailableMem - ANewSize) >=  ProcMemThreshold;
      end
      else
        Result := (MemStatus.dwMemoryLoad < 80) and (ANewSize < MemStatus.dwAvailPhys div 2);

      if Assigned(FStream) then
        FLastMemCheckPos := GetSize;
    end
    else
      Result := True;
end;


procedure TisStream.CheckStreamType;
begin
  FIsFileStream := FStream is TBufferedStream;
end;

function TisStream.GetClone: IisStream;
begin
  SetPosition(0);
  Result := TisStream.Create(GetSize);
  Result.CopyFrom(Self, GetSize);
end;

function TisStream.GetAsString: String;
var
  OldPosition: LongInt;
begin
  OldPosition := Position;
  Position := 0;
  try
    SetLength(Result, Size);
    ReadBuffer(Result[1], Size);
  finally
    Position := OldPosition;
  end;
end;

procedure TisStream.SetAsString(const AValue: String);
begin
  Position := 0;
  Size := 0;
  if AValue <> '' then
  begin
    WriteBuffer(AValue[1], Length(AValue));
    Position := 0;
  end;
end;

function TisStream.Read(var Buffer; Count: Integer): Longint;
begin
  Result := FStream.Read(Buffer, Count);
end;

function TisStream.ReadUntil(const AByte: Byte): String;
const
  BufSize = 256;
var
  Buf: String;
  i, ReadBytes: Integer;
begin
  Result := '';
  SetLength(Buf, BufSize);
  while not EOS do
  begin
    ReadBytes := Read(Buf[1], BufSize);
    if ReadBytes < BufSize then
      SetLength(Buf, ReadBytes);

    i := Pos(Char(AByte), Buf);
    if i = 0 then
      Result := Result + Buf
    else
    begin
      Result := Result + Copy(Buf, 1, i - 1);
      SetPosition(GetPosition - (ReadBytes - i));
      break;
    end;
  end;
end;

function TisStream.ReadWord: Word;
begin
  ReadBuffer(Result, SizeOf(Result));
end;

procedure TisStream.WriteWord(const w: Word);
begin
  WriteBuffer(w, SizeOf(w));
end;

function TisStream.GetGrowChunkSize: Integer;
begin
  Result := FGrowChunkSize;
end;

procedure TisStream.SetGrowChunkSize(const AValue: Integer);
begin
  FGrowChunkSize := AValue;
end;

procedure TisStream.TruncateToPosition;
begin
  FStream.Size := FStream.Position;
end;

procedure TisStream.WriteObject(const AObject: IInterface);
begin
  if Assigned(AObject) then
  begin
    WriteByte(1);
    CheckBeforeWrite(4096);
    WriteShortString((AObject as IisInterfacedObject).TypeID);
    (AObject as IisInterfacedObject).WriteToStream(Self);
  end
  else
    WriteByte(0);
end;

function TisStream.ReadObject(const AObject: IInterface): IInterface;
var
  ObjType: String;
begin
  if ReadByte = 0 then
  begin
    Result := nil;
    Exit;
  end;

  if Assigned(AObject) then
  begin
    Result := AObject;
    ObjType := ReadShortString;

    CheckCondition(ObjType = (Result as IisInterfacedObject).TypeID,
      'Object type is not compatible. Expected "' + (Result as IisInterfacedObject).TypeID + '" but "' + ObjType + '" met');

    (Result as IisInterfacedObject).ReadFromStream(Self);
  end
  else
    Result := ObjectFactory.CreateInstanceFromStream(Self);
end;

function TisStream.IsEqualTo(const AStream: IisStream): Boolean;
const
  cMaxBufSize = 512 * 1024;
var
  Buf1, Buf2: ^Byte;
  BufSize: LongInt;
  BufReadBytesCount: LongInt;
  P1, P2: LongInt;
begin
  Result := Size = AStream.Size;

  if Result then
  begin
    Buf1 := Memory;
    Buf2 := AStream.Memory;

    if Assigned(Buf1) and Assigned(Buf2) then
      Result := CompareMem(Buf1, Buf2, Size)

    else
    begin
      P1 := Position;
      P2 := AStream.Position;
      BufSize := Size;
      if BufSize > cMaxBufSize then
        BufSize := cMaxBufSize;

      GetMem(Buf1, BufSize);
      GetMem(Buf2, BufSize);
      try
        Position := 0;
        AStream.Position := 0;

        while not EOS do
        begin
          BufReadBytesCount := RealStream.Read(Buf1^, BufSize);
          Assert(AStream.RealStream.Read(Buf2^, BufSize) = BufReadBytesCount);

          Result := CompareMem(Buf1, Buf2, BufReadBytesCount);
          if not Result then
            Break;
        end;

      finally
        FreeMem(Buf1, BufSize);
        FreeMem(Buf2, BufSize);

        Position := P1;
        AStream.Position := P2;
      end;
    end;
  end;
end;

procedure TisStream.WriteComponent(Instance: TComponent);
var
  S: IisStream;
begin
  if FIsFileStream then
    FStream.WriteComponent(Instance)
  else
  begin
    S := TisStream.Create;
    S.RealStream.WriteComponent(Instance);
    S.Position := 0;
    CopyFrom(S, 0);
  end;
end;

function TisStream.ReadComponent(Instance: TComponent): TComponent;
begin
  Result := FStream.ReadComponent(Instance);
end;

{ TAutoDeleteFileStream }

constructor TAutoDeleteFileStream.Create(const FileName: string; Mode: Word);
begin
  FFileName := FileName;
  inherited Create(FileName, Mode);
end;

destructor TAutoDeleteFileStream.Destroy;
begin
  inherited;
  DeleteFile(FFileName);
end;

procedure TAutoDeleteFileStream.SetSize(const NewSize: Int64);
var
  i: Int64;
begin
  i := Position;
  inherited;
  if i <= NewSize then
    Position := i
  else
    Position := NewSize;
end;


function StreamIsAssigned(const AStream: IisStream): Boolean;
begin
  Result := Assigned(AStream) and (AStream.Size > 0);
end;


{ TEvUniqueFileStream }

constructor TEvUniqueFileStream.Create(const FileName: string;
  const AutoRename, AutoDelete: Boolean);
var
  s: string;
begin
  s := FileName;
  CreateEx(s, AutoRename, AutoDelete);
end;

constructor TEvUniqueFileStream.CreateEx(var FileName: string; const AutoRename, AutoDelete: Boolean);
var
  FHandle: THandle;

  procedure OpenFile(const AFileName: String);
  begin
    if AutoDelete then
      FHandle := CreateFile(PChar(AFileName), GENERIC_READ or GENERIC_WRITE,
        FILE_SHARE_READ, nil, CREATE_NEW, FILE_ATTRIBUTE_TEMPORARY or FILE_FLAG_SEQUENTIAL_SCAN, 0)
    else
      FHandle := CreateFile(PChar(AFileName), GENERIC_READ or GENERIC_WRITE,
        FILE_SHARE_READ, nil, CREATE_NEW, FILE_FLAG_SEQUENTIAL_SCAN, 0);
  end;

begin
  FHandle := INVALID_HANDLE_VALUE;
  if not ForceDirectories(ExtractFilePath(FileName)) then
    RaiseLastOSErrorEx('Path: '+ExtractFilePath(FileName));
  OpenFile(FileName);

  if (FHandle = INVALID_HANDLE_VALUE) and AutoRename then
  begin
    FileName := ExtractFilePath(FileName) + GetUniqueID + ExtractFileExt(FileName);
    OpenFile(FileName);
  end;

  if FHandle = INVALID_HANDLE_VALUE then
    RaiseLastOSErrorEx('File: '+FileName);

  FFileName := FileName;
  FAutoDelete := AutoDelete;

  inherited Create(FHandle);
end;

destructor TEvUniqueFileStream.Destroy;
begin
  CloseHandle(THandle(Handle));
  if FAutoDelete and (DWORD(Handle) <> INVALID_HANDLE_VALUE) then
    DeleteFile(FFileName);
  inherited;
end;

procedure TEvUniqueFileStream.SetSize(const NewSize: Int64);
var
  i: Int64;
begin
  i := Position;
  inherited;
  if i <= NewSize then
    Position := i
  else
    Position := NewSize;
end;

{ TVarArrayStream }

constructor TVarArrayStream.Create(const v: Variant);
var
  p: Pointer;
  i: Integer;
begin
  Assert(VarIsType(v, varByte or varArray), 'Variant has to be an array of bytes, VarType: ' + VarTypeAsText(VarType(v)));
  FVariant := v;
  p := VarArrayLock(FVariant);
  i := VarArrayHighBound(FVariant, 1)- VarArrayLowBound(FVariant, 1)+ 1;
  SetPointer(p, i);
end;

destructor TVarArrayStream.Destroy;
begin
  SetPointer(nil, 0);
  if VarIsType(FVariant, varByte or varArray) then
    VarArrayUnlock(FVariant);
  inherited;
end;

procedure TVarArrayStream.SetSize(NewSize: Integer);
begin
  Assert(False, 'SetSize is not allowed '+ IntToStr(NewSize));
end;

function TVarArrayStream.Write(const Buffer; Count: Integer): Longint;
begin
  Result := 0;
  Assert(False, 'Write is not implemented');
end;

function VariantToStream(const AValue: Variant; const FStream: TStream = nil): TStream;

  procedure VarToStream(const AValue: Variant; const FStream: TStream; const FUseChunksForWriting: Boolean);
  var
    I, DimCount, ElemCount, DimElemCount, ElemSize, Len: Integer;
    ArrayData: Pointer;

    procedure WriteData(const Buffer; const Count: Longint);
    begin
      if (FStream.Position + Count >= FStream.Size) and FUseChunksForWriting then
        FStream.Size := FStream.Size + 1024;
      FStream.WriteBuffer(Buffer, Count);
    end;

    procedure WriteInterfacedData(const AInt: IInterface);
    var
      S: IisStream;
      n: Integer;
    begin
      S := AInt as IisStream;
      if Assigned(S) then
      begin
        S.Position := 0;
        n := S.Size;
        FStream.WriteBuffer(n, SizeOf(n));
        FStream.CopyFrom(S.RealStream, n);
      end
      else
      begin
        n := -1;
        FStream.WriteBuffer(n, SizeOf(n));
      end;
    end;

  begin
    i := VarType(AValue);
    WriteData(i, SizeOf(i));
    if VarIsArray(AValue) then
    begin
      DimCount:= VarArrayDimCount(AValue);
      WriteData(DimCount, SizeOf(DimCount));
      ElemCount:= 1;
      for I:= 0 to DimCount - 1 do
      begin
        DimElemCount:= VarArrayHighBound(AValue, I + 1) - VarArrayLowBound(AValue, I + 1) + 1;
        ElemCount:= ElemCount * DimElemCount;
        WriteData(DimElemCount, SizeOf(DimElemCount));
      end;
      try
        ArrayData:= VarArrayLock(AValue);
        for I:= 0 to ElemCount - 1 do
        begin
          case VarType(AValue) and varTypeMask of
            varShortInt, varByte:
            begin
              ElemSize:= SizeOf(Byte);
              WriteData(PByte(ArrayData)^, ElemSize);
            end;
            varSmallint, varWord:
            begin
              ElemSize:= SizeOf(SmallInt);
              WriteData(PSmallInt(ArrayData)^, ElemSize);
            end;
            varInteger, varLongWord:
            begin
              ElemSize:= SizeOf(Integer);
              WriteData(PInteger(ArrayData)^, ElemSize);
            end;
            varSingle:
            begin
              ElemSize:= SizeOf(Single);
              WriteData(PSingle(ArrayData)^, ElemSize);
            end;
            varDouble:
            begin
              ElemSize:= SizeOf(Double);
              WriteData(PDouble(ArrayData)^, ElemSize);
            end;
            varCurrency:
            begin
              ElemSize:= SizeOf(Currency);
              WriteData(PCurrency(ArrayData)^, ElemSize);
            end;
            varDate:
            begin
              ElemSize:= SizeOf(TDateTime);
              WriteData(PDateTime(ArrayData)^, ElemSize);
            end;
            varOleStr:
              begin
                ElemSize:= SizeOf(PWideChar);
                Len := Length(PWideString(ArrayData)^);
                WriteData(Len, SizeOf(Len));
                WriteData(PWideString(ArrayData)^[1], Len * SizeOf(WideChar));
              end;
{           varString:  This array item type is not supported by Delphi!
              begin
                ElemSize:= SizeOf(PChar);
                Len := Length(PChar(ArrayData));
                WriteData(StrLen, SizeOf(Len));
                WriteData(String(PChar(ArrayData))[1], Len * SizeOf(Char));
              end;
}
            varBoolean:
            begin
              ElemSize:= SizeOf(WordBool);
              WriteData(PWordBool(ArrayData)^, ElemSize);
            end;
            varVariant:
            begin
              ElemSize:= SizeOf(TVarData);
              VarToStream(Variant(PVarData(ArrayData)^), FStream, FUseChunksForWriting);
            end;
          else
            raise ISException.Create('Unsupported type: ' + IntToStr(VarType(AValue)));
            ElemSize := 0;
          end;
          ArrayData:= Pointer(Integer(ArrayData) + ElemSize);
        end;
      finally
        VarArrayUnlock(AValue);
      end;
    end
    else
    begin
      case VarType(AValue) of
        varEmpty, varNull:;
        varShortInt, varByte:
          WriteData(TVarData(AValue).VByte, SizeOf(TVarData(AValue).VByte));
        varSmallint, varWord:
          WriteData(TVarData(AValue).VSmallInt, SizeOf(TVarData(AValue).VSmallInt));
        varInteger, varLongWord:
          WriteData(TVarData(AValue).VInteger, SizeOf(TVarData(AValue).VInteger));
        varInt64:
          WriteData(TVarData(AValue).VInt64, SizeOf(TVarData(AValue).VInt64));
        varSingle:
          WriteData(TVarData(AValue).VSingle, SizeOf(TVarData(AValue).VSingle));
        varDouble:
          WriteData(TVarData(AValue).VDouble, SizeOf(TVarData(AValue).VDouble));
        varCurrency:
          WriteData(TVarData(AValue).VCurrency, SizeOf(TVarData(AValue).VCurrency));
        varDate:
          WriteData(TVarData(AValue).VDate, SizeOf(TVarData(AValue).VDate));
        varOleStr:
          begin
            i := Length(TVarData(AValue).VOleStr);
            WriteData(i, SizeOf(i));
            WriteData(TVarData(AValue).VOleStr^, i*SizeOf(WideChar));
          end;
        varString:
          begin
            i := Length(String(TVarData(AValue).VString));
            WriteData(i, SizeOf(i));
            WriteData(TVarData(AValue).VString^, i*SizeOf(Char));
          end;
        varBoolean:
          WriteData(TVarData(AValue).VBoolean, SizeOf(TVarData(AValue).VBoolean));
        varUnknown:
          WriteInterfacedData(IInterface(TVarData(AValue).VUnknown));
      else
        raise ISException.Create('Unsupported type: ' + IntToStr(VarType(AValue)));
      end;
    end;
  end;
var
  ver: Byte;
begin
  if Assigned(FStream) then
    Result := FStream
  else
    Result := TisMemoryStream.Create;
  ver := $FF;
  Result.WriteBuffer(ver, SizeOf(ver));
  VarToStream(AValue, Result, VarIsArray(AValue));
  Result.Size := Result.Position;
end;

{ TisMemoryStream }

constructor TisMemoryStream.Create;
begin
  inherited;
  FMemoryDelta := 32 * 1024;
end;

function TisMemoryStream.Realloc(var NewCapacity: Longint): Pointer;
begin
  if (NewCapacity > 0) and (FMemoryDelta > 0) then
    NewCapacity := (NewCapacity + (FMemoryDelta - 1)) and not (FMemoryDelta - 1);

  Result := Memory;
  if NewCapacity <> Capacity then
    try
      if NewCapacity = 0 then
      begin
        FreeMem(Result);
        Result := nil;
      end
      else
        ReallocMem(Result, NewCapacity);
    except
      on E: EOutOfMemory do
      begin
        E.Message := E.Message+ ' (Requested size: '+ IntToStr(NewCapacity)+ ')';
        raise;
      end;
      on E: EInvalidPointer do
      begin
        E.Message := E.Message+ ' (Old: '+ IntToStr(Capacity)+ ' Requested size: '+ IntToStr(NewCapacity)+ ')';
        raise;
      end;
      on E: Exception do
        raise;
    end;

  if (NewCapacity <> 0) and (Result = nil) then
    raise EStreamError.Create(SMemoryStreamError+ #13'(Requested size: '+ IntToStr(NewCapacity)+ ')');
end;

{ TEvStreamAdapter }

constructor TEvStreamAdapter.Create(const AStream: IisStream);
begin
  FStream := AStream;
  inherited Create(FStream.RealStream, soReference);
end;

{ TEvOleStream }

procedure TEvOleStream.SetSize(NewSize: Integer);
begin
  GetIStream.SetSize(NewSize);
end;


{ TEvFileStream }

constructor TEvFileStream.Create(const FileName: string; Mode: Word);
begin
  Create(Filename, Mode, 0);
  FFileName := Filename;
end;

constructor TEvFileStream.Create(const FileName: string; Mode: Word; Rights: Cardinal);
var
  iHandle: Integer;
begin
  if Mode = fmCreate then
    iHandle := FileCreate(FileName, Rights)
  else
    iHandle := FileOpen(FileName, Mode);

  if DWORD(iHandle) = INVALID_HANDLE_VALUE then
    raise EFCreateError.CreateResFmt(@SFCreateErrorEx, [ExpandFileName(FileName), SysErrorMessage(GetLastError)]);

  try
    inherited Create(iHandle);
  except
    on E: Exception do
    begin
      CloseHandle(iHandle);
      raise;
    end;
  end;

  FFileName := Filename;  
end;


destructor TEvFileStream.Destroy;
begin
  inherited;
  if DWORD(FHandle) <> INVALID_HANDLE_VALUE then
    FileClose(FHandle);
end;

end.
