unit isSocket;

interface

uses Classes, WinSock2, RTLConsts, SysUtils, Windows, SyncObjs, Messages, isLogFile,
     isBaseClasses, isExceptions, isBasicUtils, isThreadManager, isSSL, EvStreamUtils,
     isErrorUtils, isVCLBugFix, isSettings;

const
  CM_SOCKETMESSAGE = WM_USER + $0002;
  BROADCAST_IP = '255.255.255.255';

type
  IisSocketLib = interface
  ['{B4E0F8EA-0D13-48B3-829E-095DF8144AEB}']
    function LookupHostAddr(const AHost: string): String;
    function LookupHostName(const AIPAddress: string): String;
    function GetLocalHostName: String;
    function GetLocalIPs: IisStringList;
    function IsLocalAddress(const AIPAddress: String): Boolean;
   end;


  TSocket = LongWord;

  IisSocket = interface
  ['{165CBEAA-D813-4D4C-9434-0EECCB9698BF}']
    function   GetSocketHandle: TSocket;
    function   GetSocketID: TisGUID;
    procedure  SetSocketID(const AValue: TisGUID);
    function   GetSessionID: String;
    procedure  SetSessionID(const AValue: String);
    procedure  LockRead;
    procedure  UnLockRead;
    procedure  LockWrite;
    procedure  UnLockWrite;
    function   GetLocalIPAddress: String;
    function   GetLocalPort: Integer;
    function   Read(var ABuffer; const ASize: Integer): Integer;
    function   Write(var ABuffer; const ASize: Integer): Integer;
    property   SessionID: TisGUID read GetSessionID write SetSessionID;    
  end;

  IisSocketServer = interface(IisInterfacedObject)
  ['{3121DD67-01DF-4FFA-9304-6C528E990F16}']
    function  ServerSockets: IisList;
    function  GetPort: String;
    procedure SetPort(const AValue: String);
    function  GetIPAddress: String;
    procedure SetIPAddress(const AValue: String);
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    property  Active: Boolean read GetActive write SetActive;
    property  IPAddress: String read GetIPAddress write SetIPAddress;
    property  Port: String read GetPort write SetPort;
  end;


  IisTCPSocket = interface(IisSocket)
  ['{89D2C7FF-56D2-4789-B2E8-B9FE9323F48C}']
    function   GetRemoteIPAddress: String;
    function   GetRemotePort: Integer;
    function   Connected: Boolean;
    function   GetLastError: String;
  end;


  IisAsyncTCPServer = interface(IisSocketServer)
  ['{CE7DB772-B39A-4D8C-9617-CAF870F73BEA}']
    procedure SetSSLTimeout(const Value: Integer);
    function  GetSSLTimeout: Integer;
    function  GetThreadPoolCapacity: Integer;
    procedure SetThreadPoolCapacity(const AValue: Integer);
    procedure Disconnect(const ASocket: IisTCPSocket);
    function  GetSSLRootCertFileName: String;
    procedure SetSSLRootCertFileName(const AFileName: String);
    function  GetSSLCertFileName: String;
    procedure SetSSLCertFileName(const AFileName: String);
    function  GetSSLKeyFileName: String;
    procedure SetSSLKeyFileName(const AFileName: String);
    property  ThreadPoolCapacity: Integer read GetThreadPoolCapacity write SetThreadPoolCapacity;
    property  SSLRootCertFile: string read GetSSLRootCertFileName write SetSSLRootCertFileName;
    property  SSLCertFile: string read GetSSLCertFileName write SetSSLCertFileName;
    property  SSLKeyFile: string read GetSSLKeyFileName write SetSSLKeyFileName;
    property  SSLTimeout: Integer read GetSSLTimeout write SetSSLTimeout;
  end;


  IisAsyncTCPClient = interface
  ['{EAED7608-4127-4548-B4AC-A4A60F8F55CC}']
    procedure RegisterSSLHost(const AHost: String; const APort: Integer; const ACertFile: String);
    procedure UnRegisterSSLHost(const AHost: String; const APort: Integer);
    function  GetThreadPoolCapacity: Integer;
    procedure SetThreadPoolCapacity(const AValue: Integer);
    function  CreateConnectionTo(const AHost: String; const APort: Integer; const ALocalIPAddress: String = ''): IisTCPSocket;
    procedure Disconnect(const ASocket: IisTCPSocket);
    property  ThreadPoolCapacity: Integer read GetThreadPoolCapacity write SetThreadPoolCapacity;
  end;

  // Simple socket

  TisSocket = class(TisInterfacedObject, IisSocket)
  private
    FHandle: TSocket;
    FSocketID: TisGUID;
    FSessionID: String;
    FSocketReadLock: TCriticalSection;
    FSocketWriteLock: TCriticalSection;
    FLastErrorCode: Integer;
    FLastError: String;
    FInDelayedEvents: Boolean;
    function   CheckSocketResult(const AResCode: Integer; const AOper: string): Boolean;
    function   IsValidSocket: Boolean;
  protected
    procedure  DoOnCloseSocket; virtual;
    function   GetSocketHandle: TSocket;
    function   GetSocketID: TisGUID;
    procedure  SetSocketID(const AValue: TisGUID); virtual;
    procedure  LockRead;
    procedure  UnLockRead;
    procedure  LockWrite;
    procedure  UnLockWrite;
    function   GetLocalIPAddress: String;
    function   GetLocalPort: Integer;
    function   GetSessionID: String;
    procedure  SetSessionID(const AValue: String); virtual;
  public
    function   Read(var ABuffer; const ASize: Integer): Integer; virtual;
    function   Write(var ABuffer; const ASize: Integer): Integer; virtual;
    function   ReadReady: Boolean;

    constructor Create; virtual;
    constructor CreateFromHandle(const ASocketHandle: TSocket); virtual;
    destructor  Destroy; override;
  end;


  // Sockets List

  TisSocketsList = class
  private
    FScktList: IisStringList;
    FScktIDList: IisStringList;
    FScktSesIDList: IisStringList;
    function    GetItem(AIndex: Integer): IisSocket;
  public
    procedure   Add(const ASocket: IisSocket);
    procedure   Remove(const ASocket: IisSocket);
    function    FindSocketByHandle(const ASocketHandle: TSocket): IisSocket;
    function    FindSocketByID(const ASocketID: TisGUID): IisSocket;
    function    GetSocketsBySessionID(const ASessionID: TisGUID): IisList;
    procedure   Lock;
    procedure   UnLock;
    function    Count: Integer;
    property    Items[Index: Integer]: IisSocket read GetItem; default;
    constructor Create;
  end;


  // Abstract socket server

  TisSocketServer = class(TisInterfacedObject, IisSocketServer)
  private
    FServerSockets: IisList;
    FPort: String;
    FIPAddress: String;
  protected
    function  FindSocketByHandle(const ASocketHandle: TSocket): IisSocket; virtual;
    procedure DoOnActivate; virtual;
    procedure DoOnDeactivate; virtual;
    function  ServerSockets: IisList;
    procedure SetIPAddress(const AValue: String);
    procedure SetPort(const AValue: String);
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  CreateServerSockets: IisList; virtual; abstract;
  public
    function   GetIPAddress: String;
    function   GetPort: String;
    destructor Destroy; override;
  end;



  // Asynchronous socket

  TisSSLOperation = (soNone, soConnect, soAccept, soWrite, soRead);
  TisAsyncSocketGroup = class;

  TisAsyncSocket = class(TisSocket)
  private
    FSocketsGroup: TisAsyncSocketGroup;
    FSSLObj: Pointer;
    FSSLLock: IisLock;
    FUnfinishedSSLOp: TisSSLOperation;
    FSSLWaitingEvent: Integer;
    FSSLReadBuffer: PAnsiChar;
    FSSLReadBufferSize: Integer;
    FSSLWriteBuffer: PAnsiChar;
    FSSLWriteBufferSize: Integer;
    FSSLTimeout: Integer;
    procedure SSLConnect;
    procedure SSLAccept;
    procedure SSLRead;
    procedure SSLWrite;
    procedure SSLClearErrQueue;
    function  CheckUnfinishedSSLOperation(const ASocketEvent: Integer): Boolean;
    procedure InitSSLSocket(const ASSLContext: Pointer);
  protected
    function  SocketsGroup: TisAsyncSocketGroup;
    procedure DoAccept; virtual;
    procedure DoRead; virtual;
    procedure DoWrite; virtual;
    procedure DoConnect; virtual;
    procedure DoClose; virtual;
    procedure DoOOB; virtual;
    procedure DoQOS; virtual;
    procedure DoGroupQOS; virtual;
    procedure DoRoutingInterfaceChange; virtual;
    procedure DoAddressListChange; virtual;
    procedure DoError(const ASocketEvent, AErrorCode: Integer); virtual;
    procedure DoException(const AException: Exception; var Handled: Boolean); virtual;
    function  GetLastError: String;
    function  IsSSLSocket: Boolean;
    procedure SetSocketID(const AValue: TisGUID); override;
    procedure SetSessionID(const AValue: String); override;
  public
    function  Read(var ABuffer; const ASize: Integer): Integer; override;
    function  Write(var ABuffer; const ASize: Integer): Integer; override;
    procedure SetSSLTimeout(const Value: Integer);
  end;



  TisSocketMessageWindow = class;

  TCMSocketMessage = record
    Msg: Cardinal;
    SocketHandle: TSocket;
    SelectEvent: Word;
    SelectError: Word;
    Result: Longint;
  end;


  TisAsyncSocketGroup = class
  private
    FCS: TCriticalSection;
    FOwner: TObject;
    FSockets: TisSocketsList;
    FMessageWindow: TisSocketMessageWindow;
    FThreadManager: IThreadManager;
    procedure WndProc(var Message: TMessage);
    procedure CMSocketMessage(var Message: TCMSocketMessage); message CM_SOCKETMESSAGE;
    function  GetActive: Boolean;
    procedure SetActive(const Value: Boolean);
    function  GetSocket(AIndex: Integer): IisSocket;
  protected
    procedure Lock;
    procedure Unlock;
  public
    function  FindSocketByHandle(const ASocketHandle: TSocket): IisSocket; virtual;
    function  FindSocketByID(const ASocketID: TisGUID): IisSocket; virtual;
    function  GetSocketsBySessionID(const ASessionID: TisGUID): IisList;
    procedure GenerateSocketEvent(const ASocket: IisSocket; AEvent: Word; AError: Word = 0; ADelay: Integer = 0);
    procedure DefaultHandler(var Message); override;
    procedure AddSocket(const ASocket: IisSocket); virtual;
    procedure RemoveSocket(const ASocket: IisSocket); virtual;
    function  Count: Integer;
    function  GetAllSockets: IisList;
    property  Active: Boolean read GetActive write SetActive;
    property  ThreadManager: IThreadManager read FThreadManager write FThreadManager;
    property  Sockets[Index: Integer]: IisSocket read GetSocket;
    property  Owner: TObject read FOwner;

    constructor Create(AOwner: TObject);
    destructor  Destroy; override;
  end;


  // Asynchronous TCP socket

  TisAsyncTCPServerSocket = class;
  TisAsyncTCPClient = class;

  TisAsyncTCPSocket = class(TisAsyncSocket, IisTCPSocket)
  private
    FServerSocket: Boolean;
    FConnected: Boolean;
    FRemoteIPAddress: String;
    FRemotePort: Integer;
    function  WaitConnected: Boolean;
    procedure SetKeepAlive(const AValue: Boolean);
  protected
    function  GetRemoteIPAddress: String;
    function  GetRemotePort: Integer;
    procedure DoClose; override;
    procedure DoConnect; override;
    procedure DoAccept; override;
    procedure DoException(const AException: Exception; var Handled: Boolean); override;
    function  Connected: Boolean;
  public
    constructor Create; override;
    constructor CreateForServer(const AServerSocket: TisAsyncTCPServerSocket; const ASSLTimeout: Integer = 0);
    constructor CreateForClient(const AClient: TisAsyncTCPClient; const AHost: String;
                                const APort: Integer; const ALocalIPAddress: String = '');
  end;

  TisAsyncTCPSocketClass = class of TisAsyncTCPSocket;

  // Asynchronous TCP server

  TisAsyncTCPServerSocketGroup = class(TisAsyncSocketGroup)
  public
    function  FindSocketByHandle(const ASocketHandle: TSocket): IisSocket; override;
    function  FindSocketByID(const ASocketID: TisGUID): IisSocket; override;
  end;


  TisAsyncTCPServer = class(TisSocketServer, IisAsyncTCPServer)
  private
    FSSLRootCertFileName: string;
    FSSLCertFileName: string;
    FSSLKeyFileName: string;
    FSSLContext: Pointer;
    FSSLTimeout: Integer;
  protected
    FSocketsGroup: TisAsyncTCPServerSocketGroup;
    procedure SetSSLTimeout(const Value: Integer);
    function  GetSSLTimeout: Integer;
    function  FindSocketByHandle(const ASocketHandle: TSocket): IisSocket; override;
    function  CreateServerSockets: IisList; override;
    function  GetClientSocketClass: TisAsyncTCPSocketClass; virtual; abstract;
    function  GetThreadPoolCapacity: Integer;
    procedure SetThreadPoolCapacity(const AValue: Integer);
    procedure DoOnActivate; override;
    procedure DoOnDeactivate; override;
    procedure DoOnConnectClient(ASocket: IisTCPSocket); virtual;
    procedure DoOnDisconnectClient(ASocket: IisTCPSocket); virtual;
    procedure Disconnect(const ASocket: IisTCPSocket);
    function  GetSSLRootCertFileName: String;
    procedure SetSSLRootCertFileName(const AFileName: String);
    function  GetSSLCertFileName: String;
    procedure SetSSLCertFileName(const AFileName: String);
    function  GetSSLKeyFileName: String;
    procedure SetSSLKeyFileName(const AFileName: String);
  public
    procedure   GenerateSocketEvent(const ASocket: IisSocket; AEvent: Word; AError: Word = 0);
    constructor Create; virtual;
    destructor  Destroy; override;
  end;


  TisAsyncTCPServerSocket = class(TisAsyncSocket)
  private
    FServer: TisAsyncTCPServer;
    FSSL: Boolean;
  public
    procedure   DoAccept; override;
    constructor CreateForServer(const AServer: TisAsyncTCPServer; const APort: Integer; const ASSL: Boolean);
  end;


  // Asynchronous TCP Client

  TisAsyncTCPClient = class(TisInterfacedObject, IisAsyncTCPClient)
  private
    FSSLContexts: TStringList;
    function GetSSLContextFor(const AHost: String; const APort: Integer): Pointer;
  protected
    FSocketsGroup: TisAsyncSocketGroup;
    procedure DoOnConstruction; override;
    function  GetSocketClass: TisAsyncTCPSocketClass; virtual; abstract;
    procedure DoOnConnect(ASocket: IisTCPSocket); virtual;
    procedure DoOnDisconnect(ASocket: IisTCPSocket); virtual;
    procedure RegisterSSLHost(const AHost: String; const APort: Integer; const ACertFile: String);
    procedure UnRegisterSSLHost(const AHost: String; const APort: Integer);
    function  GetThreadPoolCapacity: Integer;
    procedure SetThreadPoolCapacity(const AValue: Integer);
    function  CreateConnectionTo(const AHost: String; const APort: Integer; const ALocalIPAddress: String = ''): IisTCPSocket;
    procedure Disconnect(const ASocket: IisTCPSocket);
  public
    procedure   GenerateSocketEvent(const ASocket: IisSocket; AEvent: Word; AError: Word = 0);
    destructor  Destroy; override;
  end;


  // Blocking UDP Socket

  TisSocketAddr = record
    Host: String;
    Port: Integer;
  end;

  IisUDPSocket = interface(IisSocket)
  ['{9541F667-EAD0-4D95-B8B3-8AE06DB6464C}']
    procedure Send(var ABuffer; const ASize: Integer; const AHost: string; const ARemotePort: Integer);
    function  Receive(var ABuffer; var ASize: Integer; const AHost: string = ''; const ARemotePort: Integer = 0): TisSocketAddr;
    procedure SendStream(const AStream: IevDualStream; const AHost: string; const ARemotePort: Integer);
    function  ReceiveStream(const AHost: string = ''; const ARemotePort: Integer = 0): IevDualStream;
    procedure SendString(const AString: String; const AHost: string; const ARemotePort: Integer);
    function  ReceiveString(const AHost: string = ''; const ARemotePort: Integer = 0): String;
    procedure Close;
    function  GetLastRemoteAddress: TisSocketAddr;
  end;

  TisUDPSocket = class(TisSocket, IisUDPSocket)
  private
    FLastRemoteAddress: TisSocketAddr;
    function GetAddrStruct(const AHost: String; const APort: Integer): TSockAddrIn;
  protected
    procedure Send(var ABuffer; const ASize: Integer; const AHost: string; const ARemotePort: Integer);
    function  Receive(var ABuffer; var ASize: Integer; const AHost: string = ''; const ARemotePort: Integer = 0): TisSocketAddr;
    procedure SendStream(const AStream: IevDualStream; const AHost: string; const ARemotePort: Integer);
    function  ReceiveStream(const AHost: string = ''; const ARemotePort: Integer = 0): IevDualStream;
    procedure SendString(const AString: String; const AHost: string; const ARemotePort: Integer);
    function  ReceiveString(const AHost: string = ''; const ARemotePort: Integer = 0): String;
    procedure Close;
    function  GetLastRemoteAddress: TisSocketAddr;
  public
    constructor Create(const ALocalIPAddress: String = ''; const ALocalPort: Integer = 0); reintroduce;
  end;

  // Async UDP Socket

  IisAsyncUDPSocket = interface(IisSocket)
  ['{CC893411-DF59-4621-9541-29810E3F44C0}']
    function   GetRemoteIPAddress: String;
    function   GetRemotePort: Integer;
    procedure  SetPeerSocket(const AHost: String; const APort: Integer);
    function   GetLastError: String;
  end;

  TisAsyncUDPPeer = class;

  TisAsyncUDPSocket = class(TisAsyncSocket, IisAsyncUDPSocket)
  private
    FPeerAddr: TSockAddrIn;
    FRemoteIPAddress: String;
    FRemotePort: Integer;
  protected
    function  GetRemoteIPAddress: String;
    function  GetRemotePort: Integer;
    procedure SetPeerSocket(const AHost: String; const APort: Integer);
  public
    function    Write(var ABuffer; const ASize: Integer): Integer; override;
    constructor Create; override;
    constructor CreateForPeer(const APeer: TisAsyncUDPPeer; const AHost: String; const ARemotePort, ALocalPort: Integer);
  end;

  TisAsyncUDPSocketClass = class of TisAsyncUDPSocket;


  // Asynchronous UDP Peer
  IisAsyncUDPPeer = interface
  ['{302DDE9C-F195-4EAE-BDE5-B64B79EE049D}']
    function  GetThreadPoolCapacity: Integer;
    procedure SetThreadPoolCapacity(const AValue: Integer);
    function  Open(const AHost: String; const ARemotePort: Integer; const ALocalPort: Integer = 0): IisAsyncUDPSocket;
    function  OpenForBroadcast(const ARemotePort: Integer; const ALocalPort: Integer = 0): IisAsyncUDPSocket;
    procedure Close(const ASocket: IisAsyncUDPSocket);
    property  ThreadPoolCapacity: Integer read GetThreadPoolCapacity write SetThreadPoolCapacity;
  end;


  TisAsyncUDPPeer = class(TisInterfacedObject, IisAsyncUDPPeer)
  protected
    FSocketsGroup: TisAsyncSocketGroup;
    procedure DoOnConstruction; override;
    function  GetSocketClass: TisAsyncUDPSocketClass; virtual; abstract;
    function  GetThreadPoolCapacity: Integer;
    procedure SetThreadPoolCapacity(const AValue: Integer);
    function  Open(const AHost: String; const ARemotePort: Integer; const ALocalPort: Integer = 0): IisAsyncUDPSocket;
    function  OpenForBroadcast(const ARemotePort: Integer; const ALocalPort: Integer = 0): IisAsyncUDPSocket;
    procedure Close(const ASocket: IisAsyncUDPSocket);
  public
    procedure   GenerateSocketEvent(const ASocket: IisSocket; AEvent: Word; AError: Word = 0);
    destructor  Destroy; override;
  end;


  // Internal window for receiving socket messages

  TisSocketMessageWindow = class
  private
    FHandle: HWND;            // created in message pumping thread
    FWndProc: TWndMethod;     // message handler entry point
    FThreadManager: IThreadManager;
    FPumpThreadTask: TTaskID;
    procedure PumpSocketMessages(const Params: TTaskParamList);  // working in separate thread
  public
    constructor Create(const AWndProc: TWndMethod; const AThreadManager: IThreadManager);
    destructor  Destroy; override;
    procedure   Activate;
  end;


  EisSocketError = class(EisException)
  public
    Socket: IisSocket;
    SocketEvent: Integer;
    constructor Create(const ASocket: IisSocket; const ASocketEvent: Integer;
                       const AErrorCode: Integer; const AError: String); reintroduce;
  end;


  // WinSock2 library holder

  TWinsSocket2Lib = class(TisInterfacedObject, IisSocketLib)
  private
    FWSAData: TWSAData;
    procedure   CheckResult(const AResCode: Integer; const AOper: string);
    function    IPAddrToString(const h_addr: PChar): String;
  protected
    function    LookupHostAddr(const AHost: string): String;
    function    LookupHostName(const AIPAddress: string): String;
    function    GetLocalHostName: String;
    function    GetLocalIPs: IisStringList;
    function    IsLocalAddress(const AHost: String): Boolean;
  public
    constructor Create; reintroduce;
    destructor  Destroy; override;
  end;


var
  cSocketReadBufferSize: Cardinal =  64 * 1024;
  cSocketWriteBufferSize: Cardinal = 64 * 1024;

  function SocketLib: IisSocketLib;

implementation

var
  WinsSocket2Lib: IisSocketLib;


function SocketLib: IisSocketLib;
begin
  Result := WinsSocket2Lib;
end;

function WinSockErrorToString(const Err:integer): string;
begin
 case err of
    WSAEINTR:      Result := 'Interrupted system call';
    WSAEBADF:      Result := 'Bad file number';
    WSAEACCES:      Result := 'Permission denied';
    WSAEFAULT:      Result := 'Bad address';
    WSAEINVAL:      Result := 'Invalid argument';
    WSAEMFILE:      Result := 'Too many open files';
    WSAEWOULDBLOCK:      Result := 'Operation would block';
    WSAEINPROGRESS:      Result := 'Operation now in progress';
    WSAEALREADY:      Result := 'Operation already in progress';
    WSAENOTSOCK:      Result := 'Socket operation on non-socket';
    WSAEDESTADDRREQ:      Result := 'Destination address required';
    WSAEMSGSIZE:      Result := 'Message too long';
    WSAEPROTOTYPE:      Result := 'Protocol wrong type for socket';
    WSAENOPROTOOPT:      Result := 'Protocol not available';
    WSAEPROTONOSUPPORT:      Result := 'Protocol not supported';
    WSAESOCKTNOSUPPORT:      Result := 'Socket type not supported';
    WSAEOPNOTSUPP:      Result := 'Operation not supported on socket';
    WSAEPFNOSUPPORT:      Result := 'Protocol family not supported';
    WSAEAFNOSUPPORT:      Result := 'Address family not supported by protocol family';
    WSAEADDRINUSE:      Result := 'Address already in use';
    WSAEADDRNOTAVAIL:      Result := 'Can''t assign requested address';
    WSAENETDOWN:      Result := 'Network is down';
    WSAENETUNREACH:      Result := 'Network is unreachable';
    WSAENETRESET:      Result := 'Network dropped connection on reset';
    WSAECONNABORTED:      Result := 'Software caused connection abort';
    WSAECONNRESET:      Result := 'Connection reset by peer';
    WSAENOBUFS:      Result := 'No buffer space available';
    WSAEISCONN:      Result := 'Socket is already connected';
    WSAENOTCONN:      Result := 'Socket is not connected';
    WSAESHUTDOWN:      Result := 'Can''t send after socket shutdown';
    WSAETOOMANYREFS:      Result := 'Too many references: can''t splice';
    WSAETIMEDOUT:      Result := 'Connection timed out';
    WSAECONNREFUSED:      Result := 'Connection refused';
    WSAELOOP:      Result := 'Too many levels of symbolic links';
    WSAENAMETOOLONG:      Result := 'File name too long';
    WSAEHOSTDOWN:      Result := 'Host is down';
    WSAEHOSTUNREACH:      Result := 'No route to host';
    WSAENOTEMPTY:      Result := 'Directory not empty';
    WSAEPROCLIM:      Result := 'Too many processes';
    WSAEUSERS:      Result := 'Too many users';
    WSAEDQUOT:      Result := 'Disc quota exceeded';
    WSAESTALE:      Result := 'Stale NFS file handle';
    WSAEREMOTE:      Result := 'Too many levels of remote in path';
    WSASYSNOTREADY:      Result := 'Network sub-system is unusable';
    WSAVERNOTSUPPORTED:      Result := 'WinSock DLL cannot support this application';
    WSANOTINITIALISED:      Result := 'WinSock not initialized';
    WSAHOST_NOT_FOUND:      Result := 'Host not found';
    WSATRY_AGAIN:      Result := 'Non-authoritative host not found';
    WSANO_RECOVERY:      Result := 'Non-recoverable error';
    WSANO_DATA:      Result := 'No Data';
    else
      Result := 'Not a WinSock error';
  end;
end;


{ TisSocketServer }

destructor TisSocketServer.Destroy;
begin
  SetActive(False);
  inherited;
end;

procedure TisSocketServer.DoOnActivate;
begin
  FServerSockets := CreateServerSockets;
end;

procedure TisSocketServer.DoOnDeactivate;
begin
  FServerSockets := nil;
end;

function TisSocketServer.FindSocketByHandle(const ASocketHandle: TSocket): IisSocket;
var
  i: Integer;
begin
  Result := nil;

  if Assigned(FServerSockets) then
    for i := 0 to FServerSockets.Count - 1 do
      if (FServerSockets[i] as IisSocket).GetSocketHandle = ASocketHandle then
      begin
        Result := FServerSockets[i] as IisSocket;
        break;
      end;
end;

function TisSocketServer.GetActive: Boolean;
begin
  Result := Assigned(FServerSockets);
end;

function TisSocketServer.GetIPAddress: String;
begin
  Result := FIPAddress;
end;

function TisSocketServer.GetPort: String;
begin
  Result := FPort;
end;

function TisSocketServer.ServerSockets: IisList;
begin
  Result := FServerSockets;
end;

procedure TisSocketServer.SetActive(const AValue: Boolean);
begin
  if AValue <> GetActive then
    if AValue and not GetActive then
      DoOnActivate
    else
      DoOnDeactivate;
end;

procedure TisSocketServer.SetIPAddress(const AValue: String);
begin
  if not GetActive then
    FIPAddress := AValue;
end;

procedure TisSocketServer.SetPort(const AValue: String);
begin
  if not GetActive then
    FPort := AValue;
end;


{ TisAsyncTCPServer }

constructor TisAsyncTCPServer.Create;
begin
  inherited Create;
  FSocketsGroup := TisAsyncTCPServerSocketGroup.Create(Self);
end;


function TisAsyncTCPServer.CreateServerSockets: IisList;
var
  s, sSSL, sPort: String;
  p: Integer;
  Sock: IisSocket;
begin
  Result := TisList.Create;
  s := GetPort;
  while s <> '' do
  begin
    sSSL := Trim(GetNextStrValue(s, ','));
    sPort := GetNextStrValue(sSSL, ':');

    p := StrToIntDef(sPort, -1);
    if p <> -1 then
    begin
      Sock := TisAsyncTCPServerSocket.CreateForServer(Self, p, AnsiSameText(sSSL, 'SSL'));
      Result.Add(Sock);
    end;
  end;
end;

destructor TisAsyncTCPServer.Destroy;
begin
  inherited;
  FreeAndNil(FSocketsGroup);  
end;

procedure TisAsyncTCPServer.Disconnect(const ASocket: IisTCPSocket);
begin
  if Assigned(ASocket) then
    TisAsyncTCPSocket((ASocket as IisInterfacedObject).GetImplementation).DoClose;
end;

procedure TisAsyncTCPServer.DoOnActivate;
begin
  if (FSSLRootCertFileName <> '') and (FSSLCertFileName <> '') and (FSSLKeyFileName <> '') then
  begin
    FSSLContext := OpenSSLLib.NewServerContext(FSSLRootCertFileName, FSSLCertFileName, FSSLKeyFileName);
    OpenSSLLib.SSL_CTX_set_mode(FSSLContext, SSL_MODE_ENABLE_PARTIAL_WRITE or SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER);
  end;

  FSocketsGroup.Active := True;
  inherited;  
end;

procedure TisAsyncTCPServer.DoOnConnectClient(ASocket: IisTCPSocket);
begin
end;

procedure TisAsyncTCPServer.DoOnDeactivate;
begin
  FSocketsGroup.Active := False;
  while FSocketsGroup.Count > 0 do
    Disconnect(FSocketsGroup.Sockets[0] as IisTCPSocket);

  inherited;

  if Assigned(FSSLContext) then
  begin
    OpenSSLLib.FreeSSLContext(FSSLContext);
    FSSLContext := nil;
  end;
end;

procedure TisAsyncTCPServer.DoOnDisconnectClient(ASocket: IisTCPSocket);
begin
end;

function TisAsyncTCPServer.FindSocketByHandle(const ASocketHandle: TSocket): IisSocket;
begin
  Result := FSocketsGroup.FindSocketByHandle(ASocketHandle);
end;

procedure TisAsyncTCPServer.GenerateSocketEvent(const ASocket: IisSocket; AEvent, AError: Word);
begin
  FSocketsGroup.GenerateSocketEvent(ASocket, AEvent, AError);
end;

function TisAsyncTCPServer.GetSSLCertFileName: String;
begin
  Result := FSSLCertFileName;
end;

function TisAsyncTCPServer.GetSSLKeyFileName: String;
begin
  Result := FSSLKeyFileName;
end;

function TisAsyncTCPServer.GetSSLRootCertFileName: String;
begin
  Result := FSSLRootCertFileName;
end;

function TisAsyncTCPServer.GetSSLTimeout: Integer;
begin
  Result := FSSLTimeout;
end;

function TisAsyncTCPServer.GetThreadPoolCapacity: Integer;
begin
  Result := FSocketsGroup.ThreadManager.Capacity;
end;

procedure TisAsyncTCPServer.SetSSLCertFileName(const AFileName: String);
begin
  FSSLCertFileName := AFileName;
end;

procedure TisAsyncTCPServer.SetSSLKeyFileName(const AFileName: String);
begin
  FSSLKeyFileName := AFileName;
end;

procedure TisAsyncTCPServer.SetSSLRootCertFileName(const AFileName: String);
begin
  FSSLRootCertFileName := AFileName;
end;

procedure TisAsyncTCPServer.SetSSLTimeout(const Value: Integer);
begin
  FSSLTimeout := Value;
end;

procedure TisAsyncTCPServer.SetThreadPoolCapacity(const AValue: Integer);
begin
  if AValue >= 2 then // it needs one thread for socket message pumping and at least one for running event handlers
    FSocketsGroup.ThreadManager.Capacity := AValue
  else
    FSocketsGroup.ThreadManager.Capacity := 2;
end;


{ TWinsSocket2Lib }

constructor TWinsSocket2Lib.Create;
var
  ErrorCode: Integer;
begin
  inherited Create;
  ErrorCode := WSAStartup($0202, FWSAData);
  CheckCondition(ErrorCode = 0,
    Format(sWindowsSocketError, [SysErrorMessage(ErrorCode), ErrorCode, 'WSAStartup']),
    EisSocketError);
end;

destructor TWinsSocket2Lib.Destroy;
var
  ErrorCode: Integer;
begin
  ErrorCode := WSACleanup;
  CheckCondition(ErrorCode = 0,
    Format(sWindowsSocketError, [SysErrorMessage(ErrorCode), ErrorCode, 'WSAStartup']),
    EisSocketError);

  inherited;
end;

procedure TWinsSocket2Lib.CheckResult(const AResCode: Integer; const AOper: string);
var
  ErrorCode: Integer;
  Error: String;
begin
  if AResCode = SOCKET_ERROR then
  begin
    ErrorCode := WSAGetLastError;
    Error := Format(sWindowsSocketError, [SysErrorMessage(ErrorCode), ErrorCode, AOper]);
    raise EisSocketError.Create(nil, 0, ErrorCode, Error);
  end;
end;

function TWinsSocket2Lib.GetLocalHostName: String;
begin
  SetLength(Result, 250);
  GetHostName(PChar(Result), Length(Result));
  Result := String(PChar(Result));
end;

function TWinsSocket2Lib.GetLocalIPs: IisStringList;
type
  TaPInAddr = array[0..250] of PInAddr;
  PaPInAddr = ^TaPInAddr;
var
  i: integer;
  PHost: PHostEnt;
  PAdrPtr: PaPInAddr;
begin
  Result := TisStringList.Create;

  PHost := GetHostByName(PChar(GetLocalHostName));
  if PHost = nil then
    CheckResult(SOCKET_ERROR, 'GetHostByName')
  else
  begin
    PAdrPtr := PAPInAddr(PHost^.h_addr_list);
    i := 0;
    while PAdrPtr^[i] <> nil do
    begin
      Result.Add(IPAddrToString(PChar(@(PAdrPtr^[i]^.S_un_b))));
      Inc(i);
    end;
  end;
end;

function TWinsSocket2Lib.LookupHostAddr(const AHost: string): String;
var
  h: PHostEnt;
begin
  Result := '';

  if AHost <> '' then
  begin
    if AHost[1] in ['0'..'9'] then
    begin
      if inet_addr(pchar(AHost)) <> INADDR_NONE then
        Result := AHost;
    end

    else
    begin
      h := gethostbyname(pchar(AHost));
      if h <> nil then
        Result := IPAddrToString(h^.h_addr^);
    end;
  end

  else
    Result := '0.0.0.0';
end;


function TWinsSocket2Lib.LookupHostName(const AIPAddress: string): String;
var
  h: PHostEnt;
  addr: TSockAddr;
begin
  Result := '';
  addr.sin_addr.s_addr := inet_addr(pchar(AIPAddress));
  if addr.sin_addr.s_addr <> INADDR_NONE then
  begin
    h := gethostbyaddr(@addr.sin_addr.s_addr, sizeof(addr), AF_INET);
    if h <> nil then
      Result := h^.h_name;
  end;
end;

function TWinsSocket2Lib.IPAddrToString(const h_addr: PChar): String;
begin
  Result := Format('%d.%d.%d.%d', [ord(h_addr[0]), ord(h_addr[1]), ord(h_addr[2]), ord(h_addr[3])]);
end;

function TWinsSocket2Lib.IsLocalAddress(const AHost: String): Boolean;
var
  L: IisStringList;
  IP: String;
begin
  IP := LookupHostAddr(AHost);
  Result := StartsWith(IP, '127.0.');

  if not Result then
  begin
    L := GetLocalIPs;
    L.CaseSensitive := False;
    Result := L.IndexOf(IP) <> -1;
  end;
end;

{ TisSocket }

function TisSocket.CheckSocketResult(const AResCode: Integer; const AOper: string): Boolean;
begin
  if AResCode = SOCKET_ERROR then
  begin
    Result := False;
    FLastErrorCode := WSAGetLastError;
    FLastError := Format(sWindowsSocketError, [SysErrorMessage(FLastErrorCode), FLastErrorCode, AOper]);

    // if there is no data in buffer it is not a problem
    // otherwise generate exception

    if FLastErrorCode <> WSAEWOULDBLOCK then
      raise EisSocketError.Create(nil, 0, FLastErrorCode, FLastError);
  end
  else
    Result := True;
end;

constructor TisSocket.Create;
begin
  inherited Create;
  FSocketReadLock := TCriticalSection.Create;
  FSocketWriteLock := TCriticalSection.Create;
  FSocketID := GetUniqueID;
end;

constructor TisSocket.CreateFromHandle(const ASocketHandle: TSocket);
begin
  FHandle := ASocketHandle;
  Create;
  CheckCondition(IsValidSocket, 'Socket handle is not valid', EisSocketError);
end;

destructor TisSocket.Destroy;
begin
  LockRead;
  LockWrite;
  try
    if IsValidSocket then
      DoOnCloseSocket;
  finally
    FHandle := INVALID_SOCKET;
    UnlockWrite;
    UnlockRead;
  end;

  FreeAndNil(FSocketReadLock);
  FreeAndNil(FSocketWriteLock);

  inherited;
end;

procedure TisSocket.DoOnCloseSocket;
begin
  CheckSocketResult(closesocket(FHandle), 'closesocket');
end;

function TisSocket.GetLocalIPAddress: String;
var
  SockAddrIn: TSockAddrIn;
  Size: Integer;
begin
  LockRead;
  LockWrite;
  try
    Result := '';
    if IsValidSocket then
    begin
      Size := SizeOf(SockAddrIn);
      if getsockname(FHandle, @SockAddrIn, Size) = 0 then
        Result := inet_ntoa(SockAddrIn.sin_addr);
    end;
  finally
    UnlockWrite;
    UnlockRead;
  end;
end;

function TisSocket.GetLocalPort: Integer;
var
  SockAddrIn: TSockAddrIn;
  Size: Integer;
begin
  LockRead;
  LockWrite;
  try
    Result := -1;

    if IsValidSocket then
    begin
      Size := SizeOf(SockAddrIn);
      if getsockname(FHandle, @SockAddrIn, Size) = 0 then
        Result := ntohs(SockAddrIn.sin_port);
    end;
  finally
    UnlockWrite;
    UnlockRead;
  end;
end;

function TisSocket.GetSessionID: String;
begin
  Result := FSessionID;
end;

function TisSocket.GetSocketHandle: TSocket;
begin
  Result := FHandle;
end;

function TisSocket.GetSocketID: TisGUID;
begin
  Result := FSocketID;
end;

function TisSocket.IsValidSocket: Boolean;
begin
  Result := FHandle <> INVALID_SOCKET;
end;

procedure TisSocket.LockRead;
begin
  FSocketReadLock.Enter;
end;

procedure TisSocket.LockWrite;
begin
  FSocketWriteLock.Enter;
end;

function TisSocket.Read(var ABuffer; const ASize: Integer): Integer;
begin
  LockRead;
  try
    Result := recv(FHandle, ABuffer, ASize, 0);
    if not CheckSocketResult(Result, 'recv') then
      Result := 0; // in case if buffer was empty
  finally
    UnLockRead;
  end;
end;

function TisSocket.ReadReady: Boolean;
var
  CheckBuf: Char;
begin
  Result := recv(FHandle, CheckBuf, 1, MSG_PEEK) > 0;
end;

procedure TisSocket.SetSessionID(const AValue: String);
begin
  FSessionID := AValue;
end;

procedure TisSocket.SetSocketID(const AValue: TisGUID);
begin
  FSocketID := AValue;
end;

procedure TisSocket.UnLockRead;
begin
  FSocketReadLock.Leave;
end;

procedure TisSocket.UnLockWrite;
begin
  FSocketWriteLock.Leave;
end;

function TisSocket.Write(var ABuffer; const ASize: Integer): Integer;
begin
  LockWrite;
  try
    Result := send(FHandle, ABuffer, ASize, 0);
    if not CheckSocketResult(Result, 'send') then
      Result := 0; // in case if buffer was full
  finally
    UnLockWrite;
  end;
end;

{ TisAsyncTCPServerSocket }

constructor TisAsyncTCPServerSocket.CreateForServer(const AServer: TisAsyncTCPServer; const APort: Integer;
  const ASSL: Boolean);
var
  bVal: Boolean;
  BufSize: Integer;
  Linger: TLinger;
  LocalAddr: TSockAddr;
  Events: Integer;
begin
  FServer := AServer;
  FSSL := ASSL;
  CreateFromHandle(WSASocket(AF_Inet, SOCK_STREAM, IPPROTO_TCP, nil, 0, 0));

  // socket options
  Linger.l_onoff := 1;
  Linger.l_linger := 5;
  CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_DONTLINGER, PChar(@Linger), SizeOf(Linger)), 'setsockopt');

  bVal := False;
  CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_REUSEADDR, PChar(@bVal), SizeOf(bVal)), 'setsockopt');

  BufSize :=cSocketReadBufferSize;
  CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_RCVBUF, PChar(@BufSize), SizeOf(BufSize)), 'setsockopt');
  BufSize :=cSocketWriteBufferSize;
  CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_SNDBUF, PChar(@BufSize), SizeOf(BufSize)), 'setsockopt');

  // bind socket to address and port
  ZeroMemory(@LocalAddr, SizeOf(LocalAddr));
  LocalAddr.sin_family := PF_INET;

  if FServer.FIPAddress = '' then
    LocalAddr.sin_addr.s_addr := INADDR_ANY
  else
    LocalAddr.sin_addr.s_addr := inet_addr(PChar(FServer.FIPAddress));

  LocalAddr.sin_port := htons(APort);

  CheckSocketResult(bind(FHandle, @LocalAddr, SizeOf(LocalAddr)), 'bind');

  // set asychronous mode
  Events := FD_CLOSE or FD_ACCEPT or FD_READ or FD_WRITE;
  CheckSocketResult(WSAAsyncSelect(FHandle, FServer.FSocketsGroup.FMessageWindow.FHandle, CM_SOCKETMESSAGE,
                                   Events), 'WSAAsyncSelect');

  // start listenning for connections
  CheckSocketResult(listen(FHandle, SOMAXCONN), 'listen');
end;


procedure TisAsyncTCPServerSocket.DoAccept;
var
  ClientSocket: IisTCPSocket;
begin
  inherited;
  ClientSocket := FServer.GetClientSocketClass.CreateforServer(Self, FServer.GetSSLTimeout);
end;

{ TisSocketMessageWindow }

procedure TisSocketMessageWindow.Activate;
begin
  FPumpThreadTask := FThreadManager.RunTask(PumpSocketMessages, Self, MakeTaskParams([]), 'Sockets message loop', tpHigher);

  // wait until window gets created
  while InterlockedExchange(Integer(FHandle), FHandle) = 0 do
    Sleep(1);
end;

constructor TisSocketMessageWindow.Create(const AWndProc: TWndMethod; const AThreadManager: IThreadManager);
begin
  FWndProc := AWndProc;
  FThreadManager := AThreadManager;
end;

destructor TisSocketMessageWindow.Destroy;
begin
  FThreadManager.TerminateTask(FPumpThreadTask);
  PostMessage(FHandle, WM_QUIT, 0, 0);
  FThreadManager.WaitForTaskEnd(FPumpThreadTask);
  inherited;
end;

procedure TisSocketMessageWindow.PumpSocketMessages(const Params: TTaskParamList);

  function DoPump: LongBool;
  var
    Msg: TMsg;
  begin
    Result := GetMessage(Msg, FHandle, 0, 0);
    if Result then
      DispatchMessage(Msg);
  end;

begin
  InterlockedExchange(Integer(FHandle), isVCLBugFix.AllocateHwnd(FWndProc));  // Must be created in the same thread
  if FHandle = 0 then
  begin
    InterlockedExchange(Integer(FHandle), -1);
    RaiseLastOSError;
  end;

  try
    while not CurrentThreadTaskTerminated do
      if not DoPump then
        break;
  finally
    isVCLBugFix.DeallocateHWnd(FHandle);
  end;
end;


{ TisAsyncTCPSocket }

function TisAsyncTCPSocket.Connected: Boolean;
begin
  Result := FConnected;
end;

constructor TisAsyncTCPSocket.Create;
begin
  inherited;
  FSessionID := FSocketID;
  FSocketsGroup.AddSocket(Self);
end;

constructor TisAsyncTCPSocket.CreateForClient(const AClient: TisAsyncTCPClient;
  const AHost: String; const APort: Integer; const ALocalIPAddress: String = '');
var
  bVal: Boolean;
  Linger: TLinger;
  LocalAddr, RemoteAddr: TSockAddr;
  BufSize: Integer;
  bKeepTryingToConnect: Boolean;
  Events: Integer;
  i: Integer;
  SSLContext: Pointer;
begin
  FServerSocket := False;
  FSocketsGroup := AClient.FSocketsGroup;
  FRemoteIPAddress := AHost;
  FRemotePort := APort;
  SSLContext := AClient.GetSSLContextFor(AHost, APort);

  try
    CreateFromHandle(WSASocket(AF_Inet, SOCK_STREAM, IPPROTO_TCP, nil, 0, 0));

    // socket options
    Linger.l_onoff := 1;
    Linger.l_linger := 5;
    CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_DONTLINGER, PChar(@Linger), SizeOf(Linger)), 'setsockopt');

    bVal := False;
    CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_REUSEADDR, PChar(@bVal), SizeOf(bVal)), 'setsockopt');
//    CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_KEEPALIVE, nil, 0), 'setsockopt');

    BufSize :=cSocketReadBufferSize;
    CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_RCVBUF, PChar(@BufSize), SizeOf(BufSize)), 'setsockopt');
    BufSize :=cSocketWriteBufferSize;
    CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_SNDBUF, PChar(@BufSize), SizeOf(BufSize)), 'setsockopt');


    // bind socket to address and port
    ZeroMemory(@LocalAddr, SizeOf(LocalAddr));
    LocalAddr.sin_family := PF_INET;

    if ALocalIPAddress = '' then
      LocalAddr.sin_addr.s_addr := INADDR_ANY
    else
      LocalAddr.sin_addr.s_addr := inet_addr(PChar(ALocalIPAddress));
    LocalAddr.sin_port := htons(0);
    CheckSocketResult(bind(FHandle, @LocalAddr, SizeOf(LocalAddr)), 'bind');

    // connect to host
    ZeroMemory(@RemoteAddr, SizeOf(RemoteAddr));
    RemoteAddr.sin_family := PF_INET;
    RemoteAddr.sin_addr.s_addr := inet_addr(PChar(SocketLib.LookupHostAddr(FRemoteIPAddress)));
    RemoteAddr.sin_port := htons(FRemotePort);

    // Connecting...
    if Assigned(SSLContext) then
    begin
      InitSSLSocket(SSLContext);
      FUnfinishedSSLOp := soConnect;
    end;

    bKeepTryingToConnect := True;
    for i := 1 to 60 do
    begin
      try
        CheckSocketResult(connect(FHandle, @RemoteAddr, SizeOf(RemoteAddr)), 'connect');
        bKeepTryingToConnect := False;
      except
        on E: EisSocketError do
          if E.ErrorCode = WSAEADDRINUSE then
            Sleep(1000)
          else
            raise;
        else
          raise;
      end;

      if not bKeepTryingToConnect then
        break;
    end;

    CheckCondition(not bKeepTryingToConnect, 'Socket error 10048: Address already in use');

    Sleep(100);  // just in case to make sure that server has done all initialization parts
    DoConnect;

    // set asychronous mode
    Events := FD_CLOSE or FD_READ or FD_WRITE;
    CheckSocketResult(WSAAsyncSelect(FHandle, FSocketsGroup.FMessageWindow.FHandle, CM_SOCKETMESSAGE,
                                     Events), 'WSAAsyncSelect');

    SetKeepAlive(True);

    if Assigned(SSLContext) then
    begin
      // waiting for finishing of SSL handshake
      for i :=  1 to 300 do
      begin
        if FUnfinishedSSLOp = soNone then
          break;
        sleep(100);
      end;
      CheckCondition(FUnfinishedSSLOp = soNone, 'Cannot establish SSL connection');
    end;

  except
    FSocketsGroup.RemoveSocket(Self);
    raise;
  end;
end;

constructor TisAsyncTCPSocket.CreateForServer(const AServerSocket: TisAsyncTCPServerSocket; const ASSLTimeout: Integer = 0);
var
  addr: TSockAddr;
  len: Integer;
  BufSize: Integer;
  H: THandle;
begin
  FServerSocket := True;
  FSocketsGroup := AServerSocket.FServer.FSocketsGroup;
  SetSSLTimeout(ASSLTimeout);
  try
    len := sizeof(addr);
    ZeroMemory(@addr, SizeOf(addr));

    H := WSAAccept(AServerSocket.GetSocketHandle, @addr, @len, nil, 0);
    if H = INVALID_SOCKET then
    begin
      CheckSocketResult(SOCKET_ERROR, 'accept');
      raise EisSocketError.Create(nil, FD_ACCEPT, FLastErrorCode, FLastError);
    end;

    CreateFromHandle(H);

    if AServerSocket.FSSL then
    begin
      InitSSLSocket(AServerSocket.FServer.FSSLContext);
      FUnfinishedSSLOp := soAccept;
    end;

    BufSize :=cSocketReadBufferSize;
    CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_RCVBUF, PChar(@BufSize), SizeOf(BufSize)), 'setsockopt');
    BufSize := cSocketWriteBufferSize;
    CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_SNDBUF, PChar(@BufSize), SizeOf(BufSize)), 'setsockopt');

    FRemoteIPAddress := inet_ntoa(addr.sin_addr);
    FRemotePort := ntohs(addr.sin_port);

    if not WaitConnected then
      raise EisSocketError.Create(nil, FD_CONNECT, 0, 'Connection failed to establish');

    DoConnect;
  except
    FSocketsGroup.RemoveSocket(Self);
    raise;
  end;
end;

procedure TisAsyncTCPSocket.DoAccept;
begin
  inherited;

end;

procedure TisAsyncTCPSocket.DoClose;
var
  WasConnected: Boolean;
begin
  inherited;

  if Assigned(FSSLObj) then
  begin
    FSSLLock.Lock;
    try
      SSL_free(FSSLObj);
      FSSLObj := nil;
    finally
     FSSLLock.Unlock;
    end;
  end;

  if FConnected then
  begin
    WasConnected := FConnected;
    FConnected := False;
    FSocketsGroup.RemoveSocket(Self);

    if WasConnected then
    begin
      CheckSocketResult(shutdown(FHandle, SD_SEND), 'shutdown');
      if FServerSocket then
        TisAsyncTCPServer(FSocketsGroup.FOwner).DoOnDisconnectClient(Self)
      else
        TisAsyncTCPClient(FSocketsGroup.FOwner).DoOnDisconnect(Self);
    end;
  end;
end;

procedure TisAsyncTCPSocket.DoConnect;
begin
  inherited;
  FConnected := True;
  if FServerSocket then
    TisAsyncTCPServer(FSocketsGroup.FOwner).DoOnConnectClient(Self)
  else
    TisAsyncTCPClient(FSocketsGroup.FOwner).DoOnConnect(Self);
end;

procedure TisAsyncTCPSocket.DoException(const AException: Exception; var Handled: Boolean);
begin
  inherited;

  if FLastErrorCode <> 10053 then  // if it's not a connection drop
    try
      if GlobalLogFile <> nil then
        GlobalLogFile.AddEvent(etError, 'Async Sockets', 'Connection was forced to closed due errors',
          'IP Address: ' + GetRemoteIPAddress + #13#13 + GetStack(AException, nil));
    except
    end;

  Handled := True;
  DoClose;
end;

function TisAsyncTCPSocket.GetRemoteIPAddress: String;
begin
  Result := FRemoteIPAddress;
end;

function TisAsyncTCPSocket.GetRemotePort: Integer;
begin
  Result := FRemotePort;
end;

procedure TisAsyncTCPSocket.SetKeepAlive(const AValue: Boolean);
const
  SIO_KEEPALIVE_VALS = 2550136836;
type
  tcp_keepalive = record
    onoff:  Longint;
    keepalivetime: Longint;
    keepaliveinterval: Longint;
  end;
var
  Opt: tcp_keepalive;
  Ret: Integer;
  RetLen: Integer;
begin
  Opt.onoff := Ord(AValue);
  Opt.keepalivetime := 30000;
  Opt.keepaliveinterval := 1000;

  CheckSocketResult(WSAIoctl(FHandle, SIO_KEEPALIVE_VALS, @Opt, SizeOf(Opt),
    @Ret, SizeOf(Ret), @RetLen, nil, nil), 'WSAIoctl');
end;

function TisAsyncTCPSocket.WaitConnected: Boolean;
var
  SetW, SetE: TFDSet;
  timeout: TTimeVal;
begin
  FD_Zero(SetW);
  FD_Set(GetSocketHandle, SetW);
  FD_Zero(SetE);
  FD_Set(GetSocketHandle, SetE);

  timeout.tv_sec := 60;
  timeout.tv_usec := 0;
  Select(0, nil, @SetW, @SetE, @timeout);
  Result := FD_IsSet(GetSocketHandle, SetW);
end;

{ TisSocketsList }

procedure TisSocketsList.Add(const ASocket: IisSocket);
begin
  Lock;
  try
    FScktList.AddObject(IntToStr(ASocket.GetSocketHandle), ASocket);
    FScktIDList.AddObject(ASocket.GetSocketID, ASocket);
    if ASocket.SessionID <> '' then
      FScktSesIDList.AddObject(ASocket.SessionID, ASocket);
  finally
    UnLock;
  end;
end;

function TisSocketsList.Count: Integer;
begin
  Result := FScktList.Count;
end;

constructor TisSocketsList.Create;
begin
  FScktList := TisStringList.CreateAsIndex(True);
  FScktIDList := TisStringList.CreateAsIndex(False);

  FScktSesIDList := TisStringList.Create;
  FScktSesIDList.Duplicates := dupAccept;
  FScktSesIDList.CaseSensitive := False;
  FScktSesIDList.Sorted := True;  
end;

function TisSocketsList.FindSocketByHandle(const ASocketHandle: TSocket): IisSocket;
var
  i: Integer;
begin
  Lock;
  try
    i := FScktList.IndexOf(IntToStr(ASocketHandle));
    if i <> - 1 then
      Result := GetItem(i)
    else
      Result := nil;
  finally
    UnLock;
  end;
end;

function TisSocketsList.FindSocketByID(const ASocketID: TisGUID): IisSocket;
var
  i: Integer;
begin
  Lock;
  try
    i := FScktIDList.IndexOf(ASocketID);
    if i <> - 1 then
      Result := FScktIDList.Objects[i] as IisSocket
    else
      Result := nil;
  finally
    UnLock;
  end;
end;

function TisSocketsList.GetItem(AIndex: Integer): IisSocket;
begin
  Result := FScktList.Objects[AIndex] as IisSocket;
end;

function TisSocketsList.GetSocketsBySessionID(const ASessionID: TisGUID): IisList;
var
  i, iFirstIndex: Integer;
begin
  Result := TisList.Create;
  Lock;
  try
    iFirstIndex := FScktSesIDList.IndexOf(ASessionID);
    if iFirstIndex <> -1 then
    begin
      for i := iFirstIndex downto 0 do
        if AnsiSameText((FScktSesIDList.Objects[i] as IisSocket).SessionID, ASessionID) then
          Result.Add(FScktSesIDList.Objects[i])
        else
          break;

      for i := iFirstIndex + 1 to FScktSesIDList.Count - 1 do
        if AnsiSameText((FScktSesIDList.Objects[i] as IisSocket).SessionID, ASessionID) then
          Result.Add(FScktSesIDList.Objects[i])
        else
          break;
    end;
  finally
    Unlock;
  end;
end;

procedure TisSocketsList.Lock;
begin
  FScktList.Lock;
end;

procedure TisSocketsList.Remove(const ASocket: IisSocket);
var
  i: Integer;
begin
  Lock;
  try
    i := FScktList.IndexOfObject(ASocket);
    if i <> - 1 then
      FScktList.Delete(i);
    i := FScktIDList.IndexOf(ASocket.GetSocketID);
    if i <> - 1 then
      FScktIDList.Delete(i);
    i := FScktSesIDList.IndexOfObject(ASocket);
    if i <> - 1 then
      FScktSesIDList.Delete(i);
  finally
    UnLock;
  end;
end;

procedure TisSocketsList.UnLock;
begin
  FScktList.UnLock;
end;

{ EisSocketError }

constructor EisSocketError.Create(const ASocket: IisSocket; const ASocketEvent, AErrorCode: Integer; const AError: String);
begin
  CreateDetailed(AError, '', False, AErrorCode);
  Socket := ASocket;
  SocketEvent := ASocketEvent;
end;

{ TisAsyncSocket }

procedure TisAsyncSocket.DoAccept;
begin
end;

procedure TisAsyncSocket.DoAddressListChange;
begin
end;

procedure TisAsyncSocket.DoClose;
begin
end;

procedure TisAsyncSocket.DoConnect;
begin
end;

procedure TisAsyncSocket.DoError(const ASocketEvent, AErrorCode: Integer);
begin
  FLastErrorCode := AErrorCode;
  FLastError := SysErrorMessage(AErrorCode);
  raise EisSocketError.Create(Self, ASocketEvent, AErrorCode, SysErrorMessage(AErrorCode));
end;

procedure TisAsyncSocket.DoException(const AException: Exception; var Handled: Boolean);
begin
end;

procedure TisAsyncSocket.DoGroupQOS;
begin

end;

procedure TisAsyncSocket.DoOOB;
begin

end;

procedure TisAsyncSocket.DoQOS;
begin

end;

procedure TisAsyncSocket.DoRead;
begin
end;

procedure TisAsyncSocket.DoRoutingInterfaceChange;
begin
end;

procedure TisAsyncSocket.DoWrite;
begin
end;

function TisAsyncSocket.GetLastError: String;
begin
  Result := FLastError;
end;

procedure TisAsyncSocket.SSLConnect;
var
  SSLErr: Integer;
begin
  SSLClearErrQueue;
  SSLErr := SSL_connect(FSSLObj);
  if SSLErr <= 0 then
  begin
    SSLErr := SSL_get_error(FSSLObj, SSLErr);
    if (SSLErr = SSL_ERROR_WANT_READ) or (SSLErr = SSL_ERROR_WANT_WRITE) then
    begin
      FUnfinishedSSLOp := soConnect;
      if SSLErr = SSL_ERROR_WANT_READ then
        FSSLWaitingEvent := FD_READ
      else
        FSSLWaitingEvent := FD_WRITE;
    end
    else
      raise EisException.Create('SSL_connect error: ' + OpenSSLLib.GetErrorStr(SSLErr));
  end
  else
  begin
    FUnfinishedSSLOp := soNone;
    FSSLWaitingEvent := 0;
  end;
end;

procedure TisAsyncSocket.SSLAccept;
var
  SSLErr: Integer;
begin
  SSLClearErrQueue;
  SSLErr := SSL_accept(FSSLObj);
  if SSLErr <= 0 then
  begin
    SSLErr := SSL_get_error(FSSLObj, SSLErr);
    if (SSLErr = SSL_ERROR_WANT_READ) or (SSLErr = SSL_ERROR_WANT_WRITE) then
    begin
      if FSSLTimeout <> 0 then
        SocketsGroup.GenerateSocketEvent(Self, FD_QOS, 0, FSSLTimeout);
      FUnfinishedSSLOp := soAccept;
      if SSLErr = SSL_ERROR_WANT_READ then
        FSSLWaitingEvent := FD_READ
      else
        FSSLWaitingEvent := FD_WRITE;
    end
    else
      raise EisException.Create('SSL_accept error: ' + OpenSSLLib.GetErrorStr(SSLErr));
  end
  else
  begin
    FUnfinishedSSLOp := soNone;
    FSSLWaitingEvent := 0;
  end;
end;

function TisAsyncSocket.CheckUnfinishedSSLOperation(const ASocketEvent: Integer): Boolean;
begin
  Result := False;

  if IsSSLSocket then
  begin
    FSSLLock.Lock;
    try
      if Assigned(FSSLObj) and (FUnfinishedSSLOp <> soNone) then
      begin
        if (FSSLTimeout <> 0) and (ASocketEvent = FD_QOS) and (FUnfinishedSSLOp = soAccept) then
        begin
          raise EisException.Create('SSL connection timeout expired');
        end
        else
        if (FSSLWaitingEvent = 0) or (FSSLWaitingEvent = ASocketEvent) then
          case FUnfinishedSSLOp of
            soConnect: SSLConnect;
            soAccept:  SSLAccept;
            soWrite:   SSLWrite;
            soRead:    SSLRead;
          end
        else
          SocketsGroup.GenerateSocketEvent(Self, ASocketEvent, 0, 100);

        Result := FUnfinishedSSLOp <> soNone;
      end;
    finally
      FSSLLock.Unlock;
    end;
  end;
end;


procedure TisAsyncSocket.SSLRead;
var
  ReadBytes, SSLErr: Integer;
begin
  SSLClearErrQueue;
  ReadBytes := SSL_read(FSSLObj, FSSLReadBuffer, FSSLReadBufferSize);
  if ReadBytes <= 0 then
  begin
    SSLErr := SSL_get_error(FSSLObj, ReadBytes);
    if (SSLErr = SSL_ERROR_WANT_READ) or (SSLErr = SSL_ERROR_WANT_WRITE) then
    begin
      FUnfinishedSSLOp := soRead;
      if SSLErr = SSL_ERROR_WANT_READ then
        FSSLWaitingEvent := FD_READ
      else
        FSSLWaitingEvent := FD_WRITE;
    end
    else
      raise EisException.Create('SSL_read error: ' + OpenSSLLib.GetErrorStr(SSLErr));
  end
  else
  begin
    FSSLReadBufferSize := ReadBytes;
    FUnfinishedSSLOp := soNone;
    FSSLWaitingEvent := 0;
  end;
end;

procedure TisAsyncSocket.SSLWrite;
var
  WriteBytes, SSLErr: Integer;
begin
  SSLClearErrQueue;
  WriteBytes := SSL_write(FSSLObj, FSSLWriteBuffer, FSSLWriteBufferSize);
  if WriteBytes <= 0 then
  begin
    SSLErr := SSL_get_error(FSSLObj, WriteBytes);
    if (SSLErr = SSL_ERROR_WANT_READ) or (SSLErr = SSL_ERROR_WANT_WRITE) then
    begin
      FUnfinishedSSLOp := soWrite;
      if SSLErr = SSL_ERROR_WANT_READ then
        FSSLWaitingEvent := FD_READ
      else
        FSSLWaitingEvent := FD_WRITE;
    end
    else
      raise EisException.Create('SSL_write error: ' + OpenSSLLib.GetErrorStr(SSLErr));
  end
  else
  begin
    FSSLWriteBufferSize := WriteBytes;
    FUnfinishedSSLOp := soNone;
    FSSLWaitingEvent := 0;
  end;
end;

function TisAsyncSocket.Read(var ABuffer; const ASize: Integer): Integer;
begin
  if Assigned(FSSLObj) then
  begin
    Result := 0;
    FSSLLock.Lock;
    try
      if FUnfinishedSSLOp = soNone then
      begin
        if Assigned(FSSLReadBuffer) then
        begin
          // Result of previously finished SSL read
          Result := FSSLReadBufferSize;
          FSSLReadBuffer := nil;
          FSSLReadBufferSize := 0;
        end
        else
        begin
          if ReadReady then // check that buffer is not empty indeed
          begin
            FSSLReadBuffer := PAnsiChar(@ABuffer);
            FSSLReadBufferSize := ASize;
            SSLRead;
            if FUnfinishedSSLOp = soNone then
            begin
              Result := FSSLReadBufferSize;
              FSSLReadBuffer := nil;
              FSSLReadBufferSize := 0;
            end;
          end;
        end;
      end;
    finally
      FSSLLock.Unlock;
    end;
  end

  else
    Result := inherited Read(ABuffer, ASize);
end;

function TisAsyncSocket.Write(var ABuffer; const ASize: Integer): Integer;
begin
  if Assigned(FSSLObj) then
  begin
    Result := 0;
    FSSLLock.Lock;
    try
      if FUnfinishedSSLOp = soNone then
      begin
        if Assigned(FSSLWriteBuffer) then
        begin
          // Result of previously finished SSL write
          Result := FSSLWriteBufferSize;
          FSSLWriteBuffer := nil;
          FSSLWriteBufferSize := 0;
        end
        else
        begin
          if ReadReady then
            exit;
          FSSLWriteBuffer := PAnsiChar(@ABuffer);
          FSSLWriteBufferSize := ASize;
          SSLWrite;
          if FUnfinishedSSLOp = soNone then
          begin
            Result := FSSLWriteBufferSize;
            FSSLWriteBuffer := nil;
            FSSLWriteBufferSize := 0;
          end;
        end;
      end;
    finally
      FSSLLock.Unlock;
    end;
  end

  else
    Result := inherited Write(ABuffer, ASize);
end;

function TisAsyncSocket.SocketsGroup: TisAsyncSocketGroup;
begin
  Result := FSocketsGroup;
end;

procedure TisAsyncSocket.InitSSLSocket(const ASSLContext: Pointer);
begin
  FSSLLock := TisLock.Create;

  FSSLObj := SSL_new(ASSLContext);
  if SSL_set_fd(FSSLObj, FHandle) <= 0 then
    raise EisException.Create('SSL_set_fd error');
end;

function TisAsyncSocket.IsSSLSocket: Boolean;
begin
  Result := Assigned(FSSLLock);
end;

procedure TisAsyncSocket.SSLClearErrQueue;
begin
  while ERR_get_error <> 0 do;
end;


procedure TisAsyncSocket.SetSessionID(const AValue: String);
var
  i: Integer;
begin
  FSocketsGroup.FSockets.Lock;
  try
    if FSocketsGroup.FindSocketByHandle(Self.GetSocketHandle) <> nil then
    begin
      inherited;
      i := FSocketsGroup.FSockets.FScktSesIDList.IndexOfObject(Self);
      if i <> - 1 then
        FSocketsGroup.FSockets.FScktSesIDList.Delete(i);
      FSocketsGroup.FSockets.FScktSesIDList.AddObject(AValue, Self);
    end;
  finally
    FSocketsGroup.FSockets.Unlock;
  end;
end;

procedure TisAsyncSocket.SetSocketID(const AValue: TisGUID);
var
  i: Integer;
begin
  FSocketsGroup.FSockets.Lock;
  try
    if FSocketsGroup.FindSocketByHandle(Self.GetSocketHandle) <> nil then
    begin
      inherited;
      i := FSocketsGroup.FSockets.FScktIDList.IndexOfObject(Self);
      if i <> - 1 then
        FSocketsGroup.FSockets.FScktIDList.Delete(i);
      FSocketsGroup.FSockets.FScktIDList.AddObject(AValue, Self);
    end;
  finally
    FSocketsGroup.FSockets.Unlock;
  end;
end;

procedure TisAsyncSocket.SetSSLTimeout(const Value: Integer);
begin
  FSSLTimeout := Value;
end;

{ TisAsyncSocketGroup }

procedure TisAsyncSocketGroup.AddSocket(const ASocket: IisSocket);
begin
  FSockets.Add(ASocket);
end;

procedure TisAsyncSocketGroup.CMSocketMessage(var Message: TCMSocketMessage);

  procedure RunAction(const Params: TTaskParamList);
  var
    Owner: TisAsyncSocketGroup;
    SelectError, SelectEvent: Integer;
    SocketHandle: TSocket;
    Socket: IisSocket;
    SocketObject: TisAsyncSocket;
    Handled: Boolean;
  begin
    Owner := TisAsyncSocketGroup(Pointer(Integer(Params[0])));
    SocketHandle := Params[1];
    SelectEvent := Params[2];
    SelectError := Params[3];
    Socket := Owner.FindSocketByHandle(SocketHandle);

    if Assigned(Socket) then
    begin
      SocketObject := TisAsyncSocket((Socket as IisInterfacedObject).GetImplementation);
      try
        if SelectError = 0 then
        begin
          if not SocketObject.CheckUnfinishedSSLOperation(SelectEvent) then
          begin
            case SelectEvent of
              FD_ACCEPT:    SocketObject.DoAccept;
              FD_READ:      SocketObject.DoRead;
              FD_WRITE:     SocketObject.DoWrite;
              FD_CONNECT:   SocketObject.DoConnect;
              FD_CLOSE:     SocketObject.DoClose;
              FD_OOB:       SocketObject.DoOOB;
              FD_QOS:       SocketObject.DoQOS;
              FD_GROUP_QOS: SocketObject.DoGroupQOS;
              FD_ROUTING_INTERFACE_CHANGE: SocketObject.DoRoutingInterfaceChange;
            end;
          end;
        end

        else
          SocketObject.DoError(SelectEvent, SelectError);

      except
        on E: Exception do
        begin
          if (E is EisSocketError) and not Assigned(EisSocketError(E).Socket) then
            EisSocketError(E).Socket := Socket;

          SocketObject.DoException(E, Handled);
          if not Handled then
            raise;
        end;
      end;
    end;
(*    else
    begin
      SocketObject := nil;
    end; *)
  end;

var
  Socket: IisSocket;
  SocketObject: TisAsyncSocket;
  Handled: Boolean;
begin
  if Message.SelectEvent = FD_ACCEPT then
  begin
    Socket := FindSocketByHandle(Message.SocketHandle);
    if Assigned(Socket) then
    begin
      SocketObject := TisAsyncSocket((Socket as IisInterfacedObject).GetImplementation);
      try
        if (Message.SelectError = 0) and (not SocketObject.CheckUnfinishedSSLOperation(FD_ACCEPT)) then
          SocketObject.DoAccept;
      except
        on E: Exception do
        begin
          if (E is EisSocketError) and not Assigned(EisSocketError(E).Socket) then
            EisSocketError(E).Socket := Socket;
          SocketObject.DoException(E, Handled);
        end;
      end;
    end;
  end
  else
  with Message do
    FThreadManager.RunTask(@RunAction, MakeTaskParams([Self, SocketHandle, SelectEvent, SelectError]), 'Socket Event');
end;

function TisAsyncSocketGroup.Count: Integer;
begin
  Result := FSockets.Count;
end;

constructor TisAsyncSocketGroup.Create(AOwner: TObject);
begin
  FCS := TCriticalSection.Create;
  FOwner := AOwner;
  FSockets := TisSocketsList.Create;
  FThreadManager := TThreadManager.Create('Async Socket TM') as IThreadManager;
  FThreadManager.Capacity := HowManyProcessors * 2;
  FThreadManager.IdleThreadDeleteTimeout := 30 * 60000;
end;

procedure TisAsyncSocketGroup.DefaultHandler(var Message);
begin
  inherited;

  with TMessage(Message) do
    if Assigned(FMessageWindow) and (FMessageWindow.FHandle <> 0) then
      Result := CallWindowProc(@DefWindowProc, FMessageWindow.FHandle, Msg, wParam, lParam);
end;

destructor TisAsyncSocketGroup.Destroy;
begin
  Active := False;
  FreeAndNil(FSockets);
  FreeAndNil(FCS);
  inherited;
end;

function TisAsyncSocketGroup.FindSocketByHandle(const ASocketHandle: TSocket): IisSocket;
begin
  Result := FSockets.FindSocketByHandle(ASocketHandle);
end;

function TisAsyncSocketGroup.FindSocketByID(const ASocketID: TisGUID): IisSocket;
begin
  Result := FSockets.FindSocketByID(ASocketID);
end;

procedure TisAsyncSocketGroup.GenerateSocketEvent(const ASocket: IisSocket; AEvent, AError: Word; ADelay: Integer);

  procedure DelayedSocketEvent(const Params: TTaskParamList);
  var
    SG: TisAsyncSocketGroup;
  begin
    TisSocket((IInterface(Params[1]) as IisInterfacedObject).GetImplementation).FInDelayedEvents := True;
    try
      Sleep(Params[4]);
      SG := TisAsyncSocketGroup(Pointer(Integer(Params[0])));
      SG.GenerateSocketEvent(IInterface(Params[1]) as IisSocket, Params[2], Params[3], 0);
    finally
      TisSocket((IInterface(Params[1]) as IisInterfacedObject).GetImplementation).FInDelayedEvents := False;    
    end;
  end;

begin
  if ADelay > 0 then
  begin
    if not TisSocket((ASocket as IisInterfacedObject).GetImplementation).FInDelayedEvents then
      GlobalThreadManager.RunTask(@DelayedSocketEvent,
        MakeTaskParams([Integer(Pointer(Self)), ASocket, AEvent, AError, ADelay]), 'Delayed Socket Event');
    Exit;    
  end;

  if Assigned(FMessageWindow) then
    if not PostMessage(FMessageWindow.FHandle, CM_SOCKETMESSAGE, ASocket.GetSocketHandle, MakeLParam(AEvent, AError)) then
       RaiseLastOSError;
end;

function TisAsyncSocketGroup.GetActive: Boolean;
begin
  Lock;
  try
    Result := Assigned(FMessageWindow);
  finally
    Unlock;
  end;
end;

function TisAsyncSocketGroup.GetAllSockets: IisList;
var
  i: Integer;
begin
  Result := TisList.Create;
  FSockets.Lock;
  try
     Result.Capacity := FSockets.Count;
     for i := 0 to FSockets.Count - 1 do
       Result.Add(FSockets[i]);
  finally
    FSockets.Unlock;
  end;
end;

function TisAsyncSocketGroup.GetSocket(AIndex: Integer): IisSocket;
begin
  Result := FSockets[AIndex];
end;

function TisAsyncSocketGroup.GetSocketsBySessionID(const ASessionID: TisGUID): IisList;
begin
  Result := FSockets.GetSocketsBySessionID(ASessionID);
end;

procedure TisAsyncSocketGroup.Lock;
begin
  FCS.Enter;
end;

procedure TisAsyncSocketGroup.RemoveSocket(const ASocket: IisSocket);
begin
  FSockets.Remove(ASocket);
end;

procedure TisAsyncSocketGroup.SetActive(const Value: Boolean);
begin
  Lock;
  try
    if Active and not Value then
    begin
      FreeAndNil(FMessageWindow);
      FThreadManager.TerminateTask;
      FThreadManager.WaitForTaskEnd;
    end

    else if not Active and Value then
    begin
      FMessageWindow := TisSocketMessageWindow.Create(WndProc, FThreadManager);
      try
        FMessageWindow.Activate;
      except
        FreeAndNil(FMessageWindow);
        raise;
      end;
    end;
  finally
    Unlock;
  end;
end;

procedure TisAsyncSocketGroup.Unlock;
begin
  FCS.Leave;
end;

procedure TisAsyncSocketGroup.WndProc(var Message: TMessage);
begin
  try
    Dispatch(Message);
  except
    on E: Exception do
      if GlobalLogFile <> nil then
        GlobalLogFile.AddEvent(etFatalError, 'Async Sockets', E.Message, GetStack(E));
  end;
end;

{ TisAsyncTCPServerSocketGroup }

function TisAsyncTCPServerSocketGroup.FindSocketByHandle(const ASocketHandle: TSocket): IisSocket;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to TisAsyncTCPServer(Owner).FServerSockets.Count - 1 do
    if (TisAsyncTCPServer(Owner).FServerSockets[i] as IisSocket).GetSocketHandle = ASocketHandle then
    begin
      Result := TisAsyncTCPServer(Owner).FServerSockets[i] as IisSocket;
      break;
    end;

  if not Assigned(Result) then
    Result := inherited FindSocketByHandle(ASocketHandle);
end;

function TisAsyncTCPServerSocketGroup.FindSocketByID(const ASocketID: TisGUID): IisSocket;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to TisAsyncTCPServer(Owner).FServerSockets.Count - 1 do
    if (TisAsyncTCPServer(Owner).FServerSockets[i] as IisSocket).GetSocketID = ASocketID then
    begin
      Result := TisAsyncTCPServer(Owner).FServerSockets[i] as IisSocket;
      break;
    end;

  if not Assigned(Result) then
    Result := inherited FindSocketByID(ASocketID);
end;


{ TisAsyncTCPClient }

function TisAsyncTCPClient.CreateConnectionTo(const AHost: String; const APort: Integer;
 const ALocalIPAddress: String = ''): IisTCPSocket;
begin
  FSocketsGroup.Active := True;
  Result := GetSocketClass.CreateForClient(Self, AHost, APort, ALocalIPAddress);
  if not Result.Connected then
    Disconnect(Result);
end;

destructor TisAsyncTCPClient.Destroy;
var
  i: Integer;
  SSLContext: Pointer;
begin
  FreeAndNil(FSocketsGroup);

  for i := 0 to FSSLContexts.Count - 1 do
  begin
    SSLContext := FSSLContexts.Objects[i];
    OpenSSLLib.FreeSSLContext(SSLContext);
  end;
  FreeAndNil(FSSLContexts);

  inherited;
end;

procedure TisAsyncTCPClient.GenerateSocketEvent(const ASocket: IisSocket; AEvent, AError: Word);
begin
  FSocketsGroup.GenerateSocketEvent(ASocket, AEvent, AError);
end;

function TisAsyncTCPClient.GetThreadPoolCapacity: Integer;
begin
  Result := FSocketsGroup.ThreadManager.Capacity;
end;

procedure TisAsyncTCPClient.SetThreadPoolCapacity(const AValue: Integer);
begin
  FSocketsGroup.ThreadManager.Capacity := AValue;
end;

procedure TisAsyncTCPClient.DoOnConnect(ASocket: IisTCPSocket);
begin
end;

procedure TisAsyncTCPClient.DoOnDisconnect(ASocket: IisTCPSocket);
begin
end;

procedure TisAsyncTCPClient.Disconnect(const ASocket: IisTCPSocket);
begin
  if Assigned(ASocket) then
    TisAsyncTCPSocket((ASocket as IisInterfacedObject).GetImplementation).DoClose;
end;


procedure TisAsyncTCPClient.RegisterSSLHost(const AHost: String; const APort: Integer;
  const ACertFile: String);
var
  SSLContext: Pointer;
begin
  FSocketsGroup.Lock;
  try
    if GetSSLContextFor(AHost, APort) = nil then
    begin
      SSLContext := OpenSSLLib.NewClientContext(ACertFile);
      OpenSSLLib.SSL_CTX_set_mode(SSLContext, SSL_MODE_ENABLE_PARTIAL_WRITE or SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER);
      FSSLContexts.AddObject(AHost + ':' + IntToStr(APort), SSLContext);
    end;
  finally
    FSocketsGroup.Unlock;
  end;
end;

procedure TisAsyncTCPClient.DoOnConstruction;
begin
  inherited;
  FSSLContexts := TStringList.Create;
  FSocketsGroup := TisAsyncSocketGroup.Create(Self);
end;


procedure TisAsyncTCPClient.UnRegisterSSLHost(const AHost: String; const APort: Integer);
var
  i: Integer;
  SSLContext: Pointer;
begin
  FSocketsGroup.Lock;
  try
    i := FSSLContexts.IndexOf(AHost + ':' + IntToStr(APort));
    if i <> -1 then
    begin
      SSLContext := FSSLContexts.Objects[i];
      FSSLContexts.Delete(i);
      OpenSSLLib.FreeSSLContext(SSLContext);
    end;
  finally
    FSocketsGroup.Unlock;
  end;
end;

function TisAsyncTCPClient.GetSSLContextFor(const AHost: String; const APort: Integer): Pointer;
var
  i: Integer;
begin
  FSocketsGroup.Lock;
  try
    i := FSSLContexts.IndexOf(AHost + ':' + IntToStr(APort));
    if i <> -1 then
      Result := FSSLContexts.Objects[i]
    else
      Result := nil;
  finally
    FSocketsGroup.Unlock;
  end;
end;

{TisUDPSocket}

constructor TisUDPSocket.Create(const ALocalIPAddress: String = ''; const ALocalPort: Integer = 0);
var
  bReuse: Boolean;
  Addr: TSockAddrIn;
begin
  CreateFromHandle(WSASocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, nil, 0, 0));

  bReuse := True;
  CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_REUSEADDR, @bReuse, SizeOf(bReuse)),'setsockopt');

  Addr := GetAddrStruct(ALocalIPAddress, ALocalPort);
  CheckSocketResult(bind(FHandle, @Addr, SizeOf(Addr)),'bind');
end;

procedure TisUDPSocket.Send(var ABuffer; const ASize: Integer; const AHost: string; const ARemotePort: Integer);
var
  Addr: TSockAddrIn;
  bFlag: Boolean;
  Arg: Cardinal;
begin
  Addr := GetAddrStruct(AHost, ARemotePort);
  bFlag := Addr.sin_addr.S_addr = INADDR_BROADCAST;
  CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_BROADCAST, PChar(@bFlag), SizeOf(bFlag)), 'setsockopt');

  Arg := 0;
  IOCtlSocket(FHandle, FIONBIO, Arg);

  CheckSocketResult(sendto(FHandle, ABuffer, ASize, 0, @Addr, SizeOf(Addr)), 'sendto');
end;

function TisUDPSocket.Receive(var ABuffer; var ASize: Integer; const AHost: string = ''; const ARemotePort: Integer = 0): TisSocketAddr;
var
  Addr: TSockAddrIn;
  iSize, iRes, BytesToRead: Integer;
  bFlag: Boolean;
  Arg: Cardinal;
begin
  Addr := GetAddrStruct(AHost, ARemotePort);
  bFlag := Addr.sin_addr.S_addr = INADDR_BROADCAST;
  CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_BROADCAST, PChar(@bFlag), SizeOf(bFlag)), 'setsockopt');

  if not bFlag then
    connect(FHandle, @Addr, SizeOf(Addr))
  else
  begin
    Addr.sin_addr.S_addr := INADDR_ANY;
    connect(FHandle, @Addr, SizeOf(Addr));
  end;

  Arg := 1;
  IOCtlSocket(FHandle, FIONBIO, Arg);
  iSize := SizeOf(Addr);
  BytesToRead := ASize;
  ASize := 0;

  while not CurrentThreadTaskTerminated do
  begin
    iRes := recvfrom(FHandle, ABuffer, BytesToRead, 0, @Addr, @iSize);
    if not CheckSocketResult(iRes, 'recvfrom') then
      Snooze(5)
    else
    begin
      ASize := iRes;
      Result.Host := inet_ntoa(Addr.sin_addr);
      Result.Port := ntohs(Addr.sin_port);
      break;
    end;
  end;

  FLastRemoteAddress := Result;
end;


function TisUDPSocket.GetAddrStruct(const AHost: String; const APort: Integer): TSockAddrIn;
begin
  ZeroMemory(@Result, SizeOf(Result));
  Result.sin_family := AF_INET;
  Result.sin_port := htons(APort);

  if AHost = '' then
    Result.sin_addr.s_addr := INADDR_ANY
  else if AnsiSameText(AHost, BROADCAST_IP) then
    Result.sin_addr.s_addr := INADDR_BROADCAST
  else
    Result.sin_addr.s_addr := inet_addr(PChar(SocketLib.LookupHostAddr(AHost)));
end;

function TisUDPSocket.ReceiveStream(const AHost: string; const ARemotePort: Integer): IevDualStream;
var
  Buf: array of byte;
  iSize: Integer;
begin
  iSize := 64 * 1024;
  SetLength(Buf, iSize);
  Receive(Buf[0], iSize, AHost, ARemotePort);
  Result := TEvDualStreamHolder.Create(iSize);
  Result.WriteBuffer(Buf[0], iSize);
  Result.Position := 0;
end;

procedure TisUDPSocket.SendStream(const AStream: IevDualStream; const AHost: string; const ARemotePort: Integer);
var
  Buf: array of byte;
begin
  if AStream.Size > 0 then
  begin
    SetLength(Buf, AStream.Size);
    AStream.Position := 0;
    AStream.ReadBuffer(Buf[0], AStream.Size);
  end
  else
    SetLength(Buf, 1);

  Send(Buf[0], AStream.Size, AHost, ARemotePort);
end;

function TisUDPSocket.ReceiveString(const AHost: string; const ARemotePort: Integer): String;
var
  Buf: array of byte;
  iSize: Integer;
begin
  iSize := 64 * 1024; // Maximum size of UDP packet (64Kb)
  SetLength(Buf, iSize);
  Receive(Buf[0], iSize, AHost, ARemotePort);
  SetLength(Result, iSize);
  if iSize > 0 then
    MoveMemory(@Result[1], @Buf[0], iSize);
end;

procedure TisUDPSocket.SendString(const AString, AHost: string; const ARemotePort: Integer);
var
  Buf: array of byte;
begin
  if Length(AString) > 0 then
  begin
    SetLength(Buf, Length(AString));
    MoveMemory(@Buf[0], @AString[1], Length(AString));
  end
  else
    SetLength(Buf, 1);

  Send(Buf[0], Length(AString), AHost, ARemotePort)
end;

procedure TisUDPSocket.Close;
begin
  DoOnCloseSocket;
  FHandle := INVALID_SOCKET;
end;

function TisUDPSocket.GetLastRemoteAddress: TisSocketAddr;
begin
  Result := FLastRemoteAddress;
end;

{ TisAsyncUDPSocket }

constructor TisAsyncUDPSocket.Create;
begin
  inherited;
  FSessionID := FSocketID;
  FSocketsGroup.AddSocket(Self);
end;

constructor TisAsyncUDPSocket.CreateForPeer(const APeer: TisAsyncUDPPeer;
  const AHost: String; const ARemotePort, ALocalPort: Integer);
var
  bFlag: Boolean;
  Addr: TSockAddrIn;
begin
  FSocketsGroup := APeer.FSocketsGroup;
  try
    CreateFromHandle(WSASocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, nil, 0, 0));

    bFlag := True;
    CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_REUSEADDR, @bFlag, SizeOf(bFlag)),'setsockopt');

    ZeroMemory(@Addr, SizeOf(Addr));
    Addr.sin_family := PF_INET;
    Addr.sin_port := htons(ALocalPort);
    Addr.sin_addr.s_addr := INADDR_ANY;
    CheckSocketResult(bind(FHandle, @Addr, SizeOf(Addr)), 'bind');

    SetPeerSocket(AHost, ARemotePort);

    // set asychronous mode
    CheckSocketResult(WSAAsyncSelect(FHandle, FSocketsGroup.FMessageWindow.FHandle, CM_SOCKETMESSAGE,
      FD_READ or FD_WRITE), 'WSAAsyncSelect');
  except
    FSocketsGroup.RemoveSocket(Self);
    raise;
  end;
end;

function TisAsyncUDPSocket.GetRemoteIPAddress: String;
begin
  Result := FRemoteIPAddress;
end;

function TisAsyncUDPSocket.GetRemotePort: Integer;
begin
  Result := FRemotePort;
end;

procedure TisAsyncUDPSocket.SetPeerSocket(const AHost: String; const APort: Integer);
var
  bFlag: Boolean;
  sAddr: String;
begin
  LockRead;
  LockWrite;
  try
    FRemoteIPAddress := AHost;
    FRemotePort := APort;

    ZeroMemory(@FPeerAddr, SizeOf(FPeerAddr));
    FPeerAddr.sin_family := PF_INET;
    FPeerAddr.sin_port := htons(FRemotePort);

    if FRemoteIPAddress = '' then
    begin
      bFlag := False;
      FPeerAddr.sin_addr.s_addr := INADDR_ANY;
      CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_BROADCAST, PChar(@bFlag), SizeOf(bFlag)), 'setsockopt');
      CheckSocketResult(connect(FHandle, @FPeerAddr, SizeOf(FPeerAddr)), 'connect');
    end

    else if AnsiSameText(FRemoteIPAddress, BROADCAST_IP) then
    begin
      bFlag := True;
      CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_BROADCAST, PChar(@bFlag), SizeOf(bFlag)), 'setsockopt');
      FPeerAddr.sin_addr.s_addr := INADDR_ANY;
      connect(FHandle, @FPeerAddr, SizeOf(FPeerAddr));
      FPeerAddr.sin_addr.s_addr := INADDR_BROADCAST;
    end

    else
    begin
      bFlag := False;
      sAddr := SocketLib.LookupHostAddr(FRemoteIPAddress);
      FPeerAddr.sin_addr.s_addr := inet_addr(PChar(sAddr));
      CheckSocketResult(setsockopt(FHandle, SOL_SOCKET, SO_BROADCAST, PChar(@bFlag), SizeOf(bFlag)), 'setsockopt');
      CheckSocketResult(connect(FHandle, @FPeerAddr, SizeOf(FPeerAddr)), 'connect');
    end;
  finally
    UnlockWrite;
    UnLockRead;
  end;
end;

function TisAsyncUDPSocket.Write(var ABuffer; const ASize: Integer): Integer;
begin
  LockWrite;
  try
    Result := sendto(FHandle, ABuffer, ASize, 0, @FPeerAddr, SizeOf(FPeerAddr));
    if not CheckSocketResult(Result, 'sendto') then
      Result := 0; // in case if buffer was full
  finally
    UnLockWrite;
  end;
end;

{ TisAsyncUDPPeer }

procedure TisAsyncUDPPeer.Close(const ASocket: IisAsyncUDPSocket);
begin
  FSocketsGroup.RemoveSocket(ASocket);
end;

destructor TisAsyncUDPPeer.Destroy;
begin
  FreeAndNil(FSocketsGroup);
  inherited;
end;

procedure TisAsyncUDPPeer.DoOnConstruction;
begin
  inherited;
  FSocketsGroup := TisAsyncSocketGroup.Create(Self);
end;

procedure TisAsyncUDPPeer.GenerateSocketEvent(const ASocket: IisSocket; AEvent, AError: Word);
begin
  FSocketsGroup.GenerateSocketEvent(ASocket, AEvent, AError);
end;

function TisAsyncUDPPeer.GetThreadPoolCapacity: Integer;
begin
  Result := FSocketsGroup.ThreadManager.Capacity;
end;

function TisAsyncUDPPeer.Open(const AHost: String; const ARemotePort, ALocalPort: Integer): IisAsyncUDPSocket;
begin
  FSocketsGroup.Active := True;
  Result := GetSocketClass.CreateForPeer(Self, AHost, ARemotePort, ALocalPort);
end;

function TisAsyncUDPPeer.OpenForBroadcast(const ARemotePort, ALocalPort: Integer): IisAsyncUDPSocket;
begin
  Result := Open(BROADCAST_IP, ARemotePort, ALocalPort);
end;

procedure TisAsyncUDPPeer.SetThreadPoolCapacity(const AValue: Integer);
begin
  FSocketsGroup.ThreadManager.Capacity := AValue;
end;

initialization
  WinsSocket2Lib := TWinsSocket2Lib.Create;

end.


