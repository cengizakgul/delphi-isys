{$WARN SYMBOL_PLATFORM OFF}

unit ISExecuteProcess;

interface

type
  TExecuteProcessResult = record
    ExitCode: Cardinal;
    Output: string;
  end;

function ExecuteProcess( cmd: string): TExecuteProcessResult;
function StrOemToAnsi(const S: AnsiString): AnsiString;

implementation

uses
  windows, sysutils;
  
type
  TRWHandles = record
    Read: THandle;
    Write: THandle;
  end;

  TRWEHandles = record
    Read: THandle;
    Write: THandle;
    Error: THandle;
  end;

function SafeCloseHandle(var H: THandle): Boolean;
begin
  if H <> 0 then
  begin
    Result := CloseHandle(H);
    if Result then
      H := 0;
  end
  else
    Result := True;
end;

function StrOemToAnsi(const S: AnsiString): AnsiString;
begin
  SetLength(Result, Length(S));
  OemToAnsiBuff(@S[1], @Result[1], Length(S));
end;

procedure ConstructPipe(var ConsoleHandles: TRWEHandles; var LocalHandles: TRWHandles);
var
  LSecurityAttr: TSecurityAttributes;
begin
  { http://support.microsoft.com/default.aspx?scid=KB;EN-US;q190351& }
  { http://community.borland.com/article/0,1410,10387,00.html }
  { http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dllproc/base/creating_a_child_process_with_redirected_input_and_output.asp}

  ZeroMemory(@ConsoleHandles, SizeOf(ConsoleHandles));
  ZeroMemory(@LocalHandles, SizeOf(LocalHandles));

  try
    // Set up the security attributes struct.
    ZeroMemory(@LSecurityAttr, SizeOf(TSecurityAttributes));
    LSecurityAttr.nLength := SizeOf(TSecurityAttributes);
    LSecurityAttr.lpSecurityDescriptor := nil;
    LSecurityAttr.bInheritHandle := True;

    // Create the child output pipe.
    Win32Check( CreatePipe(LocalHandles.Read, ConsoleHandles.Write, @LSecurityAttr, 0) );
    Win32Check( SetHandleInformation( LocalHandles.Read, HANDLE_FLAG_INHERIT, 0) );

    { Create a duplicate of the output write handle for the std error
      write handle. This is necessary in case the child application
      closes one of its std output handles.
    }
    Win32Check( DuplicateHandle(GetCurrentProcess, ConsoleHandles.Write,
      GetCurrentProcess,
      @ConsoleHandles.Error, // Address of new handle.
      0, True, // Make it inheritable.
      DUPLICATE_SAME_ACCESS) );

    // Create the child input pipe.
    Win32Check( CreatePipe(ConsoleHandles.Read, LocalHandles.Write, @LSecurityAttr, 0) );
    Win32Check( SetHandleInformation( LocalHandles.Write, HANDLE_FLAG_INHERIT, 0) );
  except
    SafeCloseHandle(ConsoleHandles.Read);
    SafeCloseHandle(ConsoleHandles.Write);
    SafeCloseHandle(ConsoleHandles.Error);
    SafeCloseHandle(LocalHandles.Read);
    SafeCloseHandle(LocalHandles.Write);
    raise;
  end;
end;

function ExecuteProcess( cmd: string ): TExecuteProcessResult;
var
  lConsoleHandles: TRWEHandles;
  lLocalHandles: TRWHandles;
  lStartupInfo: TStartupInfo;
  lProcessInfo: TProcessInformation;
  pBuff: PChar;
  cBuffSize: Cardinal;
  cTotalBytesRead: Cardinal;
  cRead: Cardinal;
begin
  ZeroMemory( @lProcessInfo, sizeof(lProcessInfo) );

  Result.ExitCode := 0;
  Result.Output := '';

  ConstructPipe( lConsoleHandles, lLocalHandles );
  try
    try
      SafeCloseHandle(lLocalHandles.Write);

      // Set up the start up info struct.
      ZeroMemory( @lStartupInfo, sizeof(lStartupInfo) );
      lStartupInfo.cb := sizeof(STARTUPINFO);
      lStartupInfo.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW or STARTF_FORCEOFFFEEDBACK;
      lStartupInfo.hStdOutput := lConsoleHandles.Write;
      lStartupInfo.hStdInput  := lConsoleHandles.Read;
      lStartupInfo.hStdError  := lConsoleHandles.Error;
      lStartupInfo.wShowWindow := SW_HIDE;
      Win32check( CreateProcess(nil,
                                pchar(cmd),
                                nil,
                                nil,
                                true,
                                CREATE_NEW_CONSOLE, //CREATE_NO_WINDOW , DETACHED_PROCESS
                                nil,
                                nil,
                                lStartupInfo,
                                lProcessInfo) );
      SafeCloseHandle(lProcessInfo.hThread);
    finally
      SafeCloseHandle(lConsoleHandles.Write);
      SafeCloseHandle(lConsoleHandles.Read);
      SafeCloseHandle(lConsoleHandles.Error);
    end;

    cTotalBytesRead := 0;
    cBuffSize := $1000;
    GetMem(pBuff, cBuffSize );
    try
      while true do
      begin
        if cBuffSize = cTotalBytesRead then
        begin
          cBuffSize := cBuffSize + $10000;
          ReallocMem( pBuff, cBuffSize);
        end;
        if ReadFile( lLocalHandles.Read, (pBuff + cTotalBytesRead)^, cBuffSize - cTotalBytesRead, cRead, nil) then
        begin
          if cRead <> 0 then
            cTotalBytesRead := cTotalBytesRead + cRead
          else
            break; //EOF
        end
        else
        begin
          if GetLastError <> ERROR_BROKEN_PIPE then
            Win32Check( false )
          else
            break; // the pipe is closed on the other side
        end
      end;
      SetString(Result.Output, pBuff, cTotalBytesRead);
    finally
      FreeMem(pBuff);
      pBuff := nil;
    end;

    case WaitForSingleObject( lProcessInfo.hProcess, INFINITE) of
      WAIT_FAILED: Win32Check(false);
      WAIT_OBJECT_0: ; //ok
    else
      Assert(false)
    end;

    Win32Check( GetExitCodeProcess( lProcessInfo.hProcess, Result.ExitCode) );
    Assert( Result.ExitCode <> STILL_ACTIVE );
  finally
    SafeCloseHandle(lProcessInfo.hProcess);
    SafeCloseHandle(lLocalHandles.Read);
  end;
end;


end.
