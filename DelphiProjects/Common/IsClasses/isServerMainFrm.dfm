object isServerMainForm: TisServerMainForm
  Left = 466
  Top = 194
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'IsServer'
  ClientHeight = 188
  ClientWidth = 267
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 8
    Top = 120
    Width = 93
    Height = 13
    Caption = 'Interfaced Objects: '
  end
  object lIntObjCount: TLabel
    Left = 114
    Top = 120
    Width = 58
    Height = 13
    Caption = 'lIntObjCount'
  end
  object btnShowStack: TButton
    Left = 68
    Top = 152
    Width = 121
    Height = 25
    Caption = 'Snapshot Stack'
    TabOrder = 0
    OnClick = btnShowStackClick
  end
  object gbMemStatus: TGroupBox
    Left = 8
    Top = 8
    Width = 249
    Height = 97
    Cursor = crHandPoint
    Caption = 'VAS Status'
    TabOrder = 1
    OnClick = gbMemStatusClick
    object Label1: TLabel
      Left = 9
      Top = 25
      Width = 71
      Height = 13
      Caption = 'Allocated VAS:'
    end
    object lAllocatedVAS: TLabel
      Left = 133
      Top = 25
      Width = 67
      Height = 13
      Caption = 'lAllocatedVAS'
    end
    object Label3: TLabel
      Left = 9
      Top = 57
      Width = 94
      Height = 13
      Caption = 'Max Allocated VAS:'
    end
    object lMaxAllocatedVAS: TLabel
      Left = 133
      Top = 57
      Width = 67
      Height = 13
      Caption = 'lAllocatedVAS'
    end
  end
  object Timer: TTimer
    Interval = 5000
    OnTimer = TimerTimer
    Left = 216
    Top = 24
  end
end
