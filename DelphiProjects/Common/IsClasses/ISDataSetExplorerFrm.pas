unit ISDataSetExplorerFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, isDataSet;

type
  TisDataSetExplorer = class(TForm)
    grData: TDBGrid;
    DataSource: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FRecNbr: Integer;
    FEof: Boolean;
    FBof: Boolean;
    procedure Init(const ADataSet: TDataSet);
  public
  end;

  procedure ShowDS(const ADataSet: TDataSet);
  procedure ShowDS2(const ADataSet: IDataSet);

implementation

{$R *.dfm}

procedure ShowDS2(const ADataSet: IDataSet);
begin
  if Assigned(ADataSet) then
    ShowDS(ADataSet.vclDataSet);
end;

procedure ShowDS(const ADataSet: TDataSet);
var
  Frm: TisDataSetExplorer;
begin
  if not Assigned(ADataSet) then
    Exit;

  Frm := TisDataSetExplorer.Create(nil);
  try
    Frm.Init(ADataSet);
    Frm.ShowModal;
  finally
    Frm.Free;
  end;  
end;

{ TisDataSetExplorer }

procedure TisDataSetExplorer.Init(const ADataSet: TDataSet);
var
  i: Integer;
begin
  Caption := Caption + ' (' + ADataSet.Name + ')';

  for i := 0 to ADataSet.FieldCount - 1 do
    ADataSet.Fields[i].Visible := True;

  FRecNbr := ADataSet.RecNo;
  FEof := ADataSet.Eof;
  FBof := ADataSet.Bof;

  DataSource.DataSet := ADataSet;
end;

procedure TisDataSetExplorer.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if FEof then
  begin
    DataSource.DataSet.Last;
    DataSource.DataSet.Next;
  end
  else if FBof then
  begin
    DataSource.DataSet.First;
    DataSource.DataSet.Prior;
  end
  else
    DataSource.DataSet.RecNo := FRecNbr;

  DataSource.DataSet := nil;
end;

initialization
// Workaround!
// saying linker to include this function
  ShowDS(nil);
  ShowDS2(nil);
end.
