// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit ISZippingRoutines;

interface

uses
  Classes, SysUtils, AbDfEnc, AbDfBase, AbUnzPrc, AbUnzper, AbZipper, AbArcTyp, AbUtils,
  EvStreamUtils, isBaseClasses, AbConst, isExceptions, AbZipTyp;

type
  TisCompressionLevel = (clNone, clBestSpeed, clNormal, clBestCompression);

  TisFileUnZipper = class(TAbUnZipper)
  private
    FFilesToExtract: IisStringList;
  private
    procedure DoOnConfirmProcessItem(Sender: TObject; Item: TAbArchiveItem;
                                     ProcessType: TAbProcessType; var Confirm: Boolean);
    procedure DoOnProcessItemFailure(Sender : TObject; Item : TAbArchiveItem; ProcessType : TAbProcessType;
                                     ErrorClass : TAbErrorClass; ErrorCode : Integer);
  public
    procedure AfterConstruction; override;
    property  FilesToExtract: IisStringList read FFilesToExtract write FFilesToExtract;
  end;


procedure DeflateStream(const StrFrom, StrTo: TStream; const CompressionLevel: TisCompressionLevel = clNormal);
procedure InflateStream(const StrFrom, StrTo: TStream);

function  IsZipCompressedStream(const AData: IisStream): Boolean;
procedure CompressStreamToStream(const StrFrom, StrTo: IEvDualStream);
function  UnCompressStreamFromStream(const StrFrom: IEvDualStream; const StrTo: IEvDualStream = nil): IEvDualStream;

procedure ExtractFromFileToStream(const ZipFileName, FileName: string; const StrTo: IEvDualStream; const Password: string = '');
procedure AddToFileFromStream(const ZipFileName, FileName: string; const StrFrom: TStream; const Password: string = '');
procedure ExtractFromFile(const ZipFileName, Destination, FileMask: string; const Password: string = ''); overload;
procedure ExtractFromFile(const ZipFileName, Destination: String; const FileList: IisStringList;
                          const Password: string = ''); overload;
procedure AddToFile(const ZipFileName, Source, FileMask: string; const Password: string = ''; const ASubDirs: Boolean = False;
                  const ACompression: TAbZipSupportedMethod = smBestMethod);


implementation

uses AbZBrows, isBasicUtils;


function IsZipCompressedStream(const AData: IisStream): Boolean;
const
  ZipSign = 'PK'#03#04;
var
  s: AnsiString;
  OldPos: Int64;
begin
  if AData.Size < Length(ZipSign) then
  begin
    Result := False;
    Exit;
  end;

  OldPos := AData.Position;
  try
    SetLength(s, Length(ZipSign));
    AData.Position := 0;
    AData.Read(s[1], Length(ZipSign));
    Result := s = ZipSign;
  finally
    AData.Position := OldPos;
  end;
end;

procedure DeflateStream(const StrFrom, StrTo: TStream; const CompressionLevel: TisCompressionLevel = clNormal);
var
  aHelper: TAbDeflateHelper;
begin
  aHelper := TAbDeflateHelper.Create;
  try
    case CompressionLevel of
    clNone: aHelper.PKZipOption := '0';
    clBestSpeed: aHelper.PKZipOption := 'f';
    clNormal: aHelper.PKZipOption := 'n';
    clBestCompression: aHelper.PKZipOption := 'x';
    end;
    Deflate(StrFrom, StrTo, aHelper);
  finally
    aHelper.Free;
  end;
end;

procedure InflateStream(const StrFrom, StrTo: TStream);
begin
  AbUnzPrc.InflateStream(StrFrom, StrTo);
end;

procedure CompressStreamToStream(const StrFrom, StrTo: IEvDualStream);
var
  ms: IEvDualStream;
begin
  StrTo.WriteInt64(StrFrom.Size);
  ms := TEvDualStreamHolder.Create(StrFrom.Size);
  StrFrom.Position := 0;
  DeflateStream(StrFrom.RealStream, ms.RealStream);
  ms.TruncateToPosition;
  StrTo.WriteStream(ms);
end;

function UnCompressStreamFromStream(const StrFrom: IEvDualStream; const StrTo: IEvDualStream = nil): IEvDualStream;
var
  i: Int64;
  ms: IEvDualStream;
begin
  i := StrFrom.ReadInt64;
  if not Assigned(StrTo) then
    Result := TEvDualStreamHolder.Create(i)
  else
  begin
    StrTo.Position := 0;
    StrTo.Size := i;
    Result := StrTo;
  end;
  ms := TEvDualStreamHolder.Create(i);
  StrFrom.ReadStream(ms);
  ms.Position := 0;
  InflateStream(ms.RealStream, Result.RealStream);
  Assert(Result.Position = i);
  Result.Position := 0;
end;

procedure ExtractFromFileToStream(const ZipFileName, FileName: string; const StrTo: IEvDualStream; const Password: string = '');
var
  FUnZipper: TAbUnZipper;
begin
  FUnZipper := TisFileUnZipper.Create(nil);
  try
    FUnZipper.Password := Password;
    FUnZipper.FileName := ZipFileName;
    FUnZipper.ExtractToStream(FileName, StrTo.RealStream);
  finally
    FUnZipper.Free;
  end;
end;

procedure AddToFileFromStream(const ZipFileName, FileName: string; const StrFrom: TStream; const Password: string = '');
var
  FZipper: TAbZipper;
begin
  FZipper := TAbZipper.Create(nil);
  try
    FZipper.Password := Password;
    FZipper.FileName := ZipFileName;
    FZipper.AddFromStream(FileName, StrFrom);
    FZipper.TempDirectory := AppTempFolder;
  finally
    FZipper.Free;
  end;
end;

procedure ExtractFromFile(const ZipFileName, Destination, FileMask: string; const Password: string = '');
var
  FUnZipper: TAbUnZipper;
begin
  FUnZipper := TisFileUnZipper.Create(nil);
  try
    FUnZipper.Password := Password;
    FUnZipper.BaseDirectory := Destination;
    FUnZipper.FileName := ZipFileName;
    FUnZipper.ExtractFiles(FileMask);
  finally
    FUnZipper.Free;
  end;
end;


procedure ExtractFromFile(const ZipFileName, Destination: String; const FileList: IisStringList;
                          const Password: string = '');
var
  FUnZipper: TisFileUnZipper;
begin
  FUnZipper := TisFileUnZipper.Create(nil);
  try
    FileList.Sorted := True;
    FUnZipper.FFilesToExtract := FileList;
    FUnZipper.Password := Password;
    FUnZipper.BaseDirectory := Destination;
    FUnZipper.FileName := ZipFileName;
    FUnZipper.ExtractFiles('*.*');
  finally
    FUnZipper.Free;
  end;
end;


procedure AddToFile(const ZipFileName, Source, FileMask: string; const Password: string = ''; const ASubDirs: Boolean = False;
                  const ACompression: TAbZipSupportedMethod = smBestMethod);
var
  FZipper: TAbZipper;
begin
  FZipper := TAbZipper.Create(nil);
  try
    FZipper.Password := Password;
    FZipper.BaseDirectory := Source;
    FZipper.FileName := ZipFileName;
    if ASubDirs then
      FZipper.StoreOptions := FZipper.StoreOptions + [soRecurse];
    FZipper.AutoSave := False;
    FZipper.AddFiles(FileMask, faAnyFile);
    FZipper.TempDirectory := AppTempFolder;
    FZipper.CompressionMethodToUse:=  ACompression;
    FZipper.Save;
  finally
    FZipper.Free;
  end;
end;

{ TisFileUnZipper }

procedure TisFileUnZipper.AfterConstruction;
begin
  inherited;
  OnConfirmProcessItem := DoOnConfirmProcessItem;
  OnProcessItemFailure := DoOnProcessItemFailure;
  ExtractOptions := [eoCreateDirs, eoRestorePath];
  TempDirectory := AppTempFolder;   
end;

procedure TisFileUnZipper.DoOnConfirmProcessItem(Sender: TObject; Item: TAbArchiveItem;
  ProcessType: TAbProcessType; var Confirm: Boolean);
begin
  Confirm := not Assigned(FFilesToExtract) or (FFilesToExtract.IndexOf(Item.FileName) <> -1);
end;

procedure TisFileUnZipper.DoOnProcessItemFailure(Sender : TObject; Item : TAbArchiveItem;
  ProcessType : TAbProcessType; ErrorClass : TAbErrorClass; ErrorCode : Integer);
begin
  if ErrorClass = ecAbbrevia then
    raise EisException.Create(AbStrRes(ErrorCode))
  else
    raise EisException.CreateFmt('Cannot extract file %s', [Item.FileName]);
end;

end.
