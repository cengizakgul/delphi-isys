object isTestEditorHolder: TisTestEditorHolder
  Left = 158
  Top = 219
  Width = 783
  Height = 540
  Action = Editor.FormStatus
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = Editor.MainMenu1
  OldCreateOrder = False
  Position = poDefault
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inline Editor: TisTestEditorFrame
    Left = 0
    Top = 0
    Width = 775
    Height = 494
    Align = alClient
    TabOrder = 0
    inherited ToolBar1: TToolBar
      Width = 775
    end
    inherited PageControl1: TPageControl
      Width = 775
      Height = 470
      inherited tbCommands: TTabSheet
        inherited pnlView: TPanel
          Width = 767
          Height = 442
          inherited pnlDetails: TPanel
            Top = 402
            Width = 767
          end
          inherited lvScript: TListView
            Width = 767
            Height = 402
          end
        end
      end
    end
  end
end
