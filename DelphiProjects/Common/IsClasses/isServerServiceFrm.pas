unit isServerServiceFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, SvcMgr, Dialogs,
  isSingleInstanceApp, IsBasicUtils, isSettings, isTypes, isGUIApp, isLogFile;

type
  TisServerService = class(TService)
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure ServiceAfterInstall(Sender: TService);
  private
    FInitialized: Boolean;
    FInitProc: TProcedure;
    FDeinitProc: TProcedure;
    FDescription: String;
    procedure DoInit;
    procedure DoDeinit;
  public
    function GetServiceController: TServiceController; override;
    property Description: String read FDescription write FDescription;
  end;

  TcsAlreadyRunningProc = procedure (var Handled: Boolean);

  procedure RunISServer(const AppTitle, AppDescription, AppID: String; const InitProc, DeinitProc, ServiceControl: TProcedure;
                        const AlreadyRunningProc: TcsAlreadyRunningProc = nil);


implementation

uses isServerMainFrm;

{$R *.DFM}

var
  ServerService: TisServerService;
  ServerMainForm: TisServerMainForm;
  OrigInitProc: TProcedure;

procedure RunAsApp(const AppTitle: String; const InitProc, DeinitProc: TProcedure);

  procedure _Init;
  begin
    if Assigned(OrigInitProc) then
      OrigInitProc;

    if Forms.Application.MainForm = nil then
    begin
      Forms.Application.CreateForm(TisServerMainForm, ServerMainForm);
      ServerMainForm.Name := AppID;
      ServerMainForm.Caption := Forms.Application.Title;
    end;
  end;

begin
  OrigInitProc := InitProc;
  RunISGUI(AppTitle, AppID, @_Init, DeinitProc);
end;

procedure RunAsService(const AppTitle, AppDescription: String; const InitProc, DeinitProc: TProcedure);
begin
  SvcMgr.Application.Initialize;
  SvcMgr.Application.CreateForm(TisServerService, ServerService);
  ServerService.Name := AppID;
  ServerService.DisplayName := AppTitle;
  ServerService.Description := AppDescription;
  ServerService.FInitProc := InitProc;
  ServerService.FDeinitProc := DeinitProc;
  SvcMgr.Application.Run;
end;

procedure RunISServer(const AppTitle, AppDescription, AppID: String; const InitProc, DeinitProc, ServiceControl: TProcedure;
                      const AlreadyRunningProc: TcsAlreadyRunningProc = nil);

  procedure AlreadyRunningDefault(var Handled: Boolean);
  var
    Wnd: HWND;
  begin
    Wnd := FindWindow(nil, PChar(AppTitle));
    Handled := Wnd <> 0;
    if Handled then
      PostMessage(Wnd, CM_BringAppToFront, 0, 0);
  end;

var
  Handled: Boolean;
begin
  SetAppID(AppID);

  if AppSwitches.ValueExists('install') or AppSwitches.ValueExists('uninstall') then
  begin
    if AppNeedsElevation then
    begin
      if not RequestElevation then
        MessageDlg('Elevation request has failed', mtError, [mbOK], 0);
    end
    else
      RunAsService(AppTitle, AppDescription, nil, nil);
  end

  else if StartedAsService then
  begin
    if CheckSingleInstanceApp(AppID) then
      RunAsService(AppTitle, AppDescription, InitProc, DeinitProc);
  end

  else
  begin
    if CheckSingleInstanceApp(AppID) then
    begin
      RunAsApp(AppTitle, InitProc, DeinitProc);
      Handled := True;
    end

    else if Assigned(ServiceControl) and
            not (GetServiceStatus(AppID) in [ssUnknown, ssStopped]) then
    begin
      if CheckSingleInstanceApp(AppID + '_Ctrl', False) then
      begin
        Forms.Application.Initialize;
        ServiceControl;
        Handled := True;
      end
      else
        Handled := False;
    end

    else if Assigned(AlreadyRunningProc) then
    begin
      Handled := False;
      AlreadyRunningProc(Handled)
    end
    else
    begin
      Handled := False;
      AlreadyRunningDefault(Handled);
    end;

    if not Handled then
      MessageDlg('Application is already running: ' + AppTitle, mtWarning, [mbOK], 0);
  end;
end;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  ServerService.Controller(CtrlCode);
end;

function TisServerService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TisServerService.ServiceStart(Sender: TService; var Started: Boolean);
begin
  ServiceThread.Synchronize(ServiceThread, DoInit);
  Started := FInitialized;
end;

procedure TisServerService.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  ServiceThread.Synchronize(ServiceThread, DoDeinit);
end;

procedure TisServerService.ServiceExecute(Sender: TService);
begin
  while not Terminated do
    ServiceThread.ProcessRequests(True);
end;

procedure TisServerService.DoInit;
begin
  CheckIfTampered;

  if Assigned(FInitProc) then
    FInitProc;

  FInitialized := True;
end;

procedure TisServerService.DoDeinit;
begin
  if Assigned(FDeinitProc) then
    FDeinitProc;
end;

procedure TisServerService.ServiceAfterInstall(Sender: TService);
var
  Reg: IisSettings;
begin
  Reg := TisSettingsRegistry.Create(HKEY_LOCAL_MACHINE, '');
  Reg.AsString['\SYSTEM\CurrentControlSet\Services\' + Name + '\Description'] := FDescription;
end;

end.
