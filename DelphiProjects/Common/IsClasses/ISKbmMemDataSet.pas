unit ISKbmMemDataSet;

interface

{$W-}
{$O+}
{$Q-}
{$R-}

uses kbmMemTable, EvStreamUtils, Classes, DB, isExceptions;

type
  TISKbmMemDataSet = class;
  TevKbmAggregateType = (atNone, atSum, atMax);
  TevKbmAggregate = class(TCollectionItem)
  private
    FActive: Boolean;
    FDirty: Boolean;
    FExpression: string;
    FAggregateName: string;
    FValue, FUpdatingValue: Variant;
    procedure SetActive(const Value: Boolean);
    procedure SetExpression(const Value: string);
    function GetValue: Variant;
  protected
    FDataSet: TISKbmMemDataSet;
    FFieldName: string;
    FAggregateType: TevKbmAggregateType;
    FAddProc: procedure(const f: TField; var Value: Variant) of object;
    FRemoveProc: procedure(const f: TField; var Value: Variant) of object;
    procedure Add_Sum(const f: TField; var Value: Variant);
    procedure Remove_Sum(const f: TField; var Value: Variant);
    procedure Add_Max(const f: TField; var Value: Variant);
    procedure Remove_Max(const f: TField; var Value: Variant);
    procedure Add_Max_Str(const f: TField; var Value: Variant);
    procedure Remove_Max_Str(const f: TField; var Value: Variant);
    procedure BeginUpdating;
    procedure EndUpdating(const Commit: Boolean);
    procedure Add;
    procedure Remove;
  public
    procedure ReCalcAggregate;
    procedure Assign(Source: TPersistent); override;
    property AggregateName: string read FAggregateName write FAggregateName;
    property Expression: string read FExpression write SetExpression;
    property Active: Boolean read FActive write SetActive;
    property Dirty: Boolean read FDirty;
    property Value: Variant read GetValue;
  end;
  TevKbmAggregates = class(TCollection)
  private
    FOwner: TPersistent;
    FActive: Boolean;
    function GetItem(Index: Integer): TevKbmAggregate;
    procedure SetItem(Index: Integer; Value: TevKbmAggregate);
    procedure SetActive(const Value: Boolean);
  protected
    function GetOwner: TPersistent; override;
    procedure GlobalBeginUpdating;
    procedure GlobalEndUpdating(const Commit: Boolean);
    procedure GlobalAdd;
    procedure GlobalRemove;
    procedure DoBeforeDelete;
    procedure DoAfterDelete;
    procedure DoBeforeEdit;
    procedure DoBeforeInsert;
    procedure DoAfterPost;
  public
    constructor Create(Owner: TPersistent);
    procedure MarkDirty;
    function Add: TevKbmAggregate;
    function Find(const AggregateName: string): TevKbmAggregate;
    function IndexOf(const AggregateName: string): Integer;
    property Items[Index: Integer]: TevKbmAggregate read GetItem write SetItem; default;
    property Active: Boolean read FActive write SetActive;
  end;
  TISIterateRecordsCallback = procedure (const Version, Index, Total: Longint) of object;
  TISKbmMemDataSet = class(TkbmCustomMemTable)
  private
    //FIndexFieldNames: string;
    FProviderName: string;
    FPictureMasks: TStrings;
    FAggregates: TevKbmAggregates;
    FParams: TParams;
    FStoreDefs: Boolean;
    FValidateWithMask: Boolean;
    FControlType: TStrings;
    FCloneDefaultFields: Boolean;
    FPacketRecords: Integer;
    FRecordIdPosition: Integer;
    FBooleanFieldPresented: Boolean;
    FBackupIndexDefs: TIndexDefs;
    procedure SetProviderName(const Value: string); virtual;
    //procedure SetIndexFieldNames(const Value: string);
    procedure SetPictureMasks(const Value: TStrings);
    procedure SetLogChanges(const Value: Boolean);
    procedure SetAggregatesActive(const Value: Boolean);
    function GetAggregatesActive: Boolean;
    procedure SetAggregates(const Value: TevKbmAggregates);
    procedure SetParams(const Value: TParams);
    function GetData: Variant;
    procedure SetData(const Value: Variant);
    procedure SetControlType(const Value: TStrings);
    function GetDelta: IEvDualStream;
    function GetLogChanges: Boolean;
    function GetCompleteUpdateStatus: TUpdateStatus;
    procedure SetIndexName(const Value: string);
    function GetIndexName: string;
    function GetStoredAggregates: Boolean;
    function GetStoredFieldDefs: Boolean;
    function GetStoredIndexDefs: Boolean;
  protected
    FFilterIndexIsLocked: Boolean;
    procedure SaveRecordIdPosition;
    procedure RestoreRecordIdPosition;
    procedure DataEvent(Event: TDataEvent; Info: Longint); override;
    procedure DoBeforeClose; override;
    procedure DoAfterClose; override;
    procedure DoAfterOpen; override;
    procedure DoBeforeDelete; override;
    procedure DoAfterDelete; override;
    procedure DoBeforeEdit; override;
    procedure DoAfterInsert; override;
    procedure DoBeforeInsert; override;
    procedure DoAfterPost; override;
    procedure InternalPost; override;
    procedure InternalOpen; override;
    procedure InternalClose; override;
    procedure OnBeforeFieldDefAdded(const FieldName: string; var DataType: TFieldType; var DataLength: Integer; var Required: Boolean); virtual;
    procedure IterateRecords(const CallBack: TISIterateRecordsCallback; const UseCurrentIndex, UseFilterAndRange, IncludeHistory, AllowEdit: Boolean);
    procedure SwitchToIndex(Index:TkbmIndex); override;
    procedure SetFiltered(Value: Boolean); override;
    procedure SetFilterText(const Value:string); override;
    procedure SetDelta(const Value: IEvDualStream); virtual;
    procedure SetIsFiltered; override;
    function GetAggregateValue(Field: TField): Variant; override;
    procedure RefreshInternalCalcFields(Buffer: PChar); override;
    property FilterIndexIsLocked: Boolean read FFilterIndexIsLocked;
    procedure SetIndexFieldNames(FieldNames:string); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure LoadFromUniversalStream(const ms: IEvDualStream); virtual;
    procedure SaveToUniversalStream(const DataAndIntCalcOnly, IgnoreFilterAndRange: Boolean; const AStream: IEvDualStream);
    procedure CloneCursor(Source :TISKbmMemDataSet; Reset: Boolean; KeepSettings: Boolean = False); virtual;
    procedure GotoCurrent(DataSet: TISKbmMemDataSet);
    procedure CreateDataSet; virtual;
    procedure StartTransaction; override;
    procedure Commit; override;
    procedure MergeChangeLog; virtual;
    procedure CancelUpdates;
    procedure EmptyDataSet;
    procedure AddIndex(const Name, Fields: string; Options: TIndexOptions;
      const DescFields: string = ''; const CaseInsFields: string = '';
      const GroupingLevel: Integer = 0);
    function GetDataStream(const DataAndIntCalcOnly, IgnoreFilterAndRange: Boolean): IEvDualStream;
    function GetFullData: Variant;
    function GetFilteredData: Variant;
    function ChangeCount: Integer;
    procedure AssignDataFrom(const ds: TISKbmMemDataSet); virtual;
    procedure AppendData(const Data: OleVariant; const HitEOF: Boolean);
    procedure GotoSeqRecordNumber(Number: Integer);
    procedure SetDefaultFields(const AValue: Boolean);
    property Data: Variant read GetData write SetData;
    property LogChanges: Boolean read GetLogChanges write SetLogChanges;
    property Delta: IEvDualStream read GetDelta write SetDelta;
    property CompleteUpdateStatus: TUpdateStatus read GetCompleteUpdateStatus;
  published
    property ProviderName: string read FProviderName write SetProviderName;
    property IndexName:string read GetIndexName write SetIndexName;
    //property IndexFieldNames:string read FIndexFieldNames write SetIndexFieldNames;
    property IndexFieldNames;
    property Aggregates: TevKbmAggregates read FAggregates write SetAggregates stored GetStoredAggregates;
    property AggregatesActive: Boolean read GetAggregatesActive write SetAggregatesActive default False;
    // for dfm level compatibility
    property Params: TParams read FParams write SetParams stored False;
    property StoreDefs: Boolean read FStoreDefs write FStoreDefs stored False;
    property ValidateWithMask: Boolean read FValidateWithMask write FValidateWithMask stored False;
    property PictureMasks: TStrings read FPictureMasks write SetPictureMasks;
    property ControlType: TStrings read FControlType write SetControlType;
    property PacketRecords: Integer read FPacketRecords write FPacketRecords stored False;
    {property AutoReposition stored False default True;
    property RecalcOnIndex stored False default False;
    property RecalcOnFetch stored False default False;
    property AutoCalcFields stored False;
    property DesignActivation stored False;
    property AttachedAutoRefresh stored False;
    property AttachMaxCount stored False;
    property SortOptions stored False;
    property Performance stored False;
    property Standalone stored False;
    property PersistentBackup stored False;
    property ProgressFlags stored False;
    property VersioningMode stored False;
    property FilterOptions stored False;
    property Version stored False;
    property LanguageID stored False;
    property SortID stored False;
    property SubLanguageID stored False;
    property LocaleID stored False;
    property EnableVersioning stored False;}
    property Active;
    property FieldDefs stored GetStoredFieldDefs;
    property IndexDefs stored GetStoredIndexDefs;
    property ReadOnly;
    property Filter;
    property MasterFields;
    property DetailFields;
    property MasterSource;
    property BeforeOpen;
    property AfterOpen;
    property BeforeClose;
    property AfterClose;
    property BeforeInsert;
    property AfterInsert;
    property BeforeEdit;
    property AfterEdit;
    property BeforePost;
    property AfterPost;
    property BeforeCancel;
    property AfterCancel;
    property BeforeDelete;
    property AfterDelete;
    property BeforeScroll;
    property AfterScroll;
    property OnCalcFields;
    property OnFilterRecord;
    property OnNewRecord;
    property OnDeleteError;
    property OnEditError;
    property OnPostError;
    property BeforeRefresh;
    property DataSetField;
  end;
  TIsKbmMemDataSetDeltaHandler = class;
  TOnUpdateRecordDeltaHandlerEvent = procedure (const DeltaHandler: TIsKbmMemDataSetDeltaHandler; const UpdateStatus:TUpdateStatus) of object;
  TIsKbmMemDataSetDeltaHandler = class(TkbmCustomDeltaHandler)
  private
    FOnUpdateRecord: TOnUpdateRecordDeltaHandlerEvent;
  protected
     procedure InsertRecord(var Retry:boolean; var State:TUpdateStatus); override;
     procedure DeleteRecord(var Retry:boolean; var State:TUpdateStatus); override;
     procedure ModifyRecord(var Retry:boolean; var State:TUpdateStatus); override;
     procedure CallOnUpdateRecord(const UpdateStatus:TUpdateStatus);
  published
    property DataSet;
    property OnUpdateRecord: TOnUpdateRecordDeltaHandlerEvent read FOnUpdateRecord write FOnUpdateRecord;
  end;

implementation

uses SUniversalDataReader, SysUtils, Variants, VarUtils, IsBasicUtils, kbmMemBinaryStreamFormat,
  Math;

const
  evKbmAggregateTypeName: array [TevKbmAggregateType] of string = ('', 'SUM', 'MAX');
  evKbmInternalIndexPrefix = 'xxxxxxx';

type
  TProtIntegerField = class(TIntegerField);
  TProtStringField = class(TStringField);
  TPumpClass1Pump = procedure (const ff: TField) of object;
  TPumpClass1 = class
  public
    FStream: IisStream;
    function  PumpNull(const ff: TField): Boolean;
    procedure PumpInteger(const ff: TField);
    procedure PumpInt64(const ff: TField);
    procedure PumpFloat(const ff: TField);
    procedure PumpString(const ff: TField);
    procedure PumpBlob(const ff: TField);
    procedure PumpBool(const ff: TField);
  end;

  TEvKbmDataReader = class(TEvUnversalDataReader)
  private
    FKbmTable: TISKbmMemDataSet;
  protected
    procedure BeforeAssignFieldDefs(const FieldCount: Integer); override;
    procedure AssignFieldDef(const FieldInfo: TEvUnversalDataReaderFieldRecord; out PumpIndex: Integer); override;
    procedure AfterAssignFieldDefs; override;
    procedure BeforeRead(const RecordCount: Cardinal); override;
    procedure AssignPumpData(const FieldInfo: TEvUnversalDataReaderFieldRecord; out Data: Pointer); override;
    procedure AfterRead; override;
    procedure BeforeNewRecord; override;
    procedure AfterNewRecord; override;
    procedure PumpNull(const Data: Pointer); override;
    procedure PumpBoolean(const Data: Pointer; const b: Boolean); override;
    procedure PumpInteger(const Data: Pointer; const i: Integer); override;
    procedure PumpInt64(const Data: Pointer; const i: Int64); override;
    procedure PumpString(const Data: Pointer; const s: string); override;
    procedure PumpDouble(const Data: Pointer; const d: Double); override;
    procedure PumpBlob(const Data: Pointer; const DataSize: Integer; const DataStr: string); override;
  public
    property KbmTable: TISKbmMemDataSet read FKbmTable write FKbmTable;
  end;
  TkbmProtCustomMemTable = class(TkbmCustomMemTable);
  TkbmProtCommon = class(TkbmCommon);

{ TISKbmMemDataSet }

procedure TISKbmMemDataSet.AddIndex(const Name, Fields: string;
  Options: TIndexOptions; const DescFields: string;
  const CaseInsFields: string;
  const GroupingLevel: Integer);
var
  id: TIndexDef;
begin
  Assert(GroupingLevel = 0);
  Assert(IndexDefs.IndexOf(Name)=-1);
  id := TIndexDef.Create(IndexDefs);
  try
    id.Name := Name;
    id.Fields := Fields;
    id.DescFields := DescFields;
    id.CaseInsFields := CaseInsFields;
    id.Options := Options;
    Indexes.Add(id);
  except
    id.Free;
    raise;
  end;
end;

procedure TISKbmMemDataSet.AppendData(const Data: OleVariant;
  const HitEOF: Boolean);
var
  t: TISKbmMemDataSet;
begin
  t := TISKbmMemDataSet.Create(nil);
  try
    t.Data := Data;
    LoadFromDataSet(t, [mtcpoAppend]);
  finally
    t.Free;
  end;
end;

procedure TISKbmMemDataSet.AssignDataFrom(const ds: TISKbmMemDataSet);
begin
  LoadFromUniversalStream(ds.GetDataStream(True, True));
end;

procedure TISKbmMemDataSet.CancelUpdates;
begin
  Cancel;
  if EnableVersioning then
  begin
    if AttachedTo <> nil then
      AttachedTo.Rollback
    else
      Rollback;
  end;
end;

function TISKbmMemDataSet.ChangeCount: Integer;
begin
  if IsDataModified then
    Result := 1
  else
    Result := 0;
end;

procedure TISKbmMemDataSet.CloneCursor(Source: TISKbmMemDataSet; Reset,
  KeepSettings: Boolean);
var
  i: Integer;
begin
  Close;
  FCloneDefaultFields := FieldCount = 0;
  if not KeepSettings then
  begin
    IndexDefs.Clear;
    for i := 0 to Source.Indexes.Count-1 do
      with Source.Indexes.GetIndex(i) do
        if not IsInternal then
          IndexDefs.Add(Name, IndexFields, CompareOptions2IndexOptions(IndexOptions));
    if Reset then
    begin
      Filtered := False;
      Filter := '';
      IndexName := '';
      IndexFieldNames := '';
    end
    else
    begin
      Filtered := Source.Filtered;
      Filter := Source.Filter;
      if Source.IndexName <> '' then
        IndexName := Source.IndexName
      else
        IndexFieldNames := Source.IndexFieldNames;
    end;
  end;
  if Source.AttachedTo <> nil then
    AttachedTo := Source.AttachedTo
  else
    AttachedTo := Source;

  if Source.Active then
    Open;
end;

procedure TISKbmMemDataSet.Commit;
begin
  inherited;
end;

constructor TISKbmMemDataSet.Create(AOwner: TComponent);
begin
  inherited;
  FBackupIndexDefs := TIndexDefs.Create(nil);
  FPictureMasks := TStringList.Create;
  FControlType := TStringList.Create;
  FAggregates := TevKbmAggregates.Create(Self);
  FParams := TParams.Create;
  RangeIgnoreNullKeyValues := False;
  Standalone := True;
  //Standalone := False;
  AttachMaxCount := 4;
  //AttachMaxCount := 1;
  Filtered := False;
  Performance := mtpfFast;
  //Performance := mtpfSmall;
  AutoReposition := True;
  LogChanges := True;
  RecalcOnFetch := False;
  RecalcOnIndex := False;
  AllDataFormat := TkbmBinaryStreamFormat.Create(Self);
  DefaultFormat := AllDataFormat;
  VersioningMode := mtvm1SinceCheckPoint;
  EnableVersioning := True;
end;

procedure TISKbmMemDataSet.CreateDataSet;
begin
  CheckInActive;
  Open;
  DoCustomAfterOpen;
end;

procedure TISKbmMemDataSet.DataEvent(Event: TDataEvent; Info: Integer);
begin
  inherited;
end;

destructor TISKbmMemDataSet.Destroy;
begin
  FreeAndNil(FPictureMasks);
  FreeAndNil(FControlType);
  FreeAndNil(FAggregates);
  FreeAndNil(FParams);
  inherited;
  FreeAndNil(FBackupIndexDefs);
end;

procedure TISKbmMemDataSet.DoAfterClose;
var
  i: Integer;
begin
  for i := Indexes.Count-1 downto 0 do
    if Copy(Indexes.GetIndex(i).Name, 1, Length(evKbmInternalIndexPrefix)) = evKbmInternalIndexPrefix then
      Indexes.DeleteIndex(Indexes.GetIndex(i));
  for i := IndexDefs.Count-1 downto 0 do
    if Copy(IndexDefs[i].Name, 1, Length(evKbmInternalIndexPrefix)) = evKbmInternalIndexPrefix then
      IndexDefs.Delete(i);
  inherited;
end;

procedure TISKbmMemDataSet.DoAfterDelete;
begin
  inherited;
  FAggregates.DoAfterDelete;
end;

procedure TISKbmMemDataSet.DoAfterInsert;
var
  i: Integer;
begin
  if FBooleanFieldPresented then
    for i := 0 to FieldCount-1 do
      if Fields[i] is TBooleanField then
        Fields[i].AsBoolean := False;
  inherited;
end;

procedure TISKbmMemDataSet.DoAfterOpen;
var
  i: Integer;
  lSavedFilterIndexIsLocked: Boolean;
begin
  for i := 0 to FieldCount-1 do
    if (Fields[i] is TStringField)
    and (TStringField(Fields[i]).Size > 32 * 1024) then
      Assert(False, 'Limit reached')
    else
    if (Fields[i] is TWideStringField) then
      Assert(False, 'Wide string fields are not supported');
  lSavedFilterIndexIsLocked := FFilterIndexIsLocked;
  try
    FFilterIndexIsLocked := False;
    inherited;
  finally
    FFilterIndexIsLocked := lSavedFilterIndexIsLocked;
  end;
  FBooleanFieldPresented := False;
  for i := 0 to FieldCount-1 do
    if (Fields[i] is TBooleanField) then
    begin
      FBooleanFieldPresented := True;
      Break;
    end;
  FAggregates.MarkDirty;
  //if FIndexFieldNames <> '' then
  //  SetIndexFieldNames(FIndexFieldNames);
end;

procedure TISKbmMemDataSet.DoAfterPost;
begin
  inherited;
  FAggregates.DoAfterPost;
end;

procedure TISKbmMemDataSet.DoBeforeClose;
begin
  inherited;
end;

procedure TISKbmMemDataSet.DoBeforeDelete;
begin
  inherited;
  FAggregates.DoBeforeDelete;
end;

procedure TISKbmMemDataSet.DoBeforeEdit;
begin
  inherited;
  FAggregates.DoBeforeEdit;
end;

procedure TISKbmMemDataSet.DoBeforeInsert;
begin
  inherited;
  FAggregates.DoBeforeInsert;
end;

procedure TISKbmMemDataSet.EmptyDataSet;
begin
  EmptyTable;
end;

function TISKbmMemDataSet.GetAggregatesActive: Boolean;
begin
  Result := FAggregates.Active;
end;

function TISKbmMemDataSet.GetAggregateValue(Field: TField): Variant;
const
  AggrPrefix = 'xxxzzz';
var
  a: TevKbmAggregate;
begin
  a := Aggregates.Find(AggrPrefix+ Field.FieldName);
  if not Assigned(a) then
  begin
    a := Aggregates.Add;
    a.AggregateName := AggrPrefix+ Field.FieldName;
  end;
  if a.Expression <> TAggregateField(Field).Expression then
    a.Expression := TAggregateField(Field).Expression;
  a.Active := TAggregateField(Field).Active;
  Result := a.Value;
end;

function TISKbmMemDataSet.GetCompleteUpdateStatus: TUpdateStatus;
var
  ARecord: PkbmRecord;
begin
  Result := UpdateStatus;
  if Result = usModified then
  begin
    ARecord := GetActiveRecord;
    while ARecord^.PrevRecordVersion <> nil do
      ARecord := ARecord^.PrevRecordVersion;
    if ARecord^.UpdateStatus = usInserted then
      Result := usInserted;
  end;
end;

function TISKbmMemDataSet.GetData: Variant;
var
  s: IEvDualStream;
begin
  s := GetDataStream(True, True);
//  Result := kbmMemTable.StreamToVariant(s.RealStream);
  Result := s;
end;

function TISKbmMemDataSet.GetDataStream(const DataAndIntCalcOnly, IgnoreFilterAndRange: Boolean): IEvDualStream;
var
  LRecords: TkbmList;
  iProjectedSize: Integer;
begin
  if IgnoreFilterAndRange then
    LRecords := TkbmProtCommon(Common).FRecords
  else
    LRecords := FCurIndex.References;

  iProjectedSize := RecordSize* LRecords.Count;
  Result := TEvDualStreamHolder.Create(iProjectedSize);

  SaveToUniversalStream(DataAndIntCalcOnly, IgnoreFilterAndRange, Result);
  Result.Position := 0;
end;

function TISKbmMemDataSet.GetDelta: IEvDualStream;
var
  f: TkbmBinaryStreamFormat;
  iRecNo: Integer;
begin
  CheckBrowseMode;
  iRecNo := RecNo;
  f := TkbmBinaryStreamFormat.Create(nil);
  try
    f.sfIndexDef:= []; //[sfSaveIndexDef,sfLoadIndexDef];
    f.sfDeltas := [sfSaveDeltas, sfLoadDeltas]; // [];
    f.sfFiltered := []; //[sfSaveFiltered];
    f.sfUsingIndex := []; //[sfSaveUsingIndex];
    Result := TEvDualStreamHolder.CreateInMemory;
    SaveToStreamViaFormat(Result.RealStream, f);
  finally
    f.Free;
    if iRecNo <> -1 then
      RecNo := iRecNo;
  end;
end;

function TISKbmMemDataSet.GetFilteredData: Variant;
var
  s: IEvDualStream;
begin
  s := GetDataStream(False, False);
//  Result := kbmMemTable.StreamToVariant(s.RealStream);
  Result := s;
end;

function TISKbmMemDataSet.GetFullData: Variant;
var
  s: IEvDualStream;
begin
  s := GetDataStream(False, True);
//  Result := kbmMemTable.StreamToVariant(s.RealStream);
  Result := s;
end;

function TISKbmMemDataSet.GetIndexName: string;
begin
  Result := inherited IndexName;
end;

function TISKbmMemDataSet.GetLogChanges: Boolean;
begin
  Result := EnableVersioning;
end;

function TISKbmMemDataSet.GetStoredAggregates: Boolean;
begin
  Result := Aggregates.Count > 0;
end;

function TISKbmMemDataSet.GetStoredFieldDefs: Boolean;
begin
  Result := FieldDefs.Count > 0;
end;

function TISKbmMemDataSet.GetStoredIndexDefs: Boolean;
begin
  Result := IndexDefs.Count > 0;
end;

procedure TISKbmMemDataSet.GotoCurrent(DataSet: TISKbmMemDataSet);
var
  id:integer;
  iRecNo:integer;
begin
  id := PkbmRecord(DataSet.ActiveBuffer)^.RecordID;
  CurIndex.SearchRecordID(id,iRecNo);
  if (iRecNo<0) or (iRecNo>=CurIndex.References.Count)
    or (not FilterRecord(CurIndex.References.Items[iRecNo], False)) then
        raise EisException.Create('Record not found');
  DoBeforeScroll;
  FRecNo := iRecNo;
  DoAfterScroll;
  Resync([]);
end;

procedure TISKbmMemDataSet.GotoSeqRecordNumber(Number: Integer);
begin
  First;
  while Number > 1 do
  begin
    Next;
    Dec(Number);
  end;
end;

procedure TISKbmMemDataSet.InternalClose;
var
  i: Integer;
begin
  inherited;
  if AttachedTo <> nil then
  begin
    AttachedTo := nil;
    if FCloneDefaultFields then
      Fields.Clear;
  end;
  for i := IndexDefs.Count-1 downto 0 do
    if TDefCollection(FBackupIndexDefs).Find(IndexDefs[i].Name) = nil then //TDefCollection does not raise an expeption if not found, TIndexDefs does
    begin
      if IndexDefs[i].Name = IndexName then
        IndexName := '';
      IndexDefs.Delete(i);
    end;
  FBackupIndexDefs.Clear;
end;

procedure TISKbmMemDataSet.InternalOpen;
begin
  RecalcOnIndex := False;
  inherited;
  FBackupIndexDefs.Assign(IndexDefs);
end;

procedure TISKbmMemDataSet.InternalPost;
begin
  ClearCalcFields(PChar(PkbmRecord(ActiveBuffer)));
  GetCalcFields(PChar(PkbmRecord(ActiveBuffer)));
  inherited;
end;

procedure TISKbmMemDataSet.IterateRecords(
  const CallBack: TISIterateRecordsCallback; const UseCurrentIndex,
  UseFilterAndRange, IncludeHistory, AllowEdit: Boolean);
var
  i, iCount, iVersion: Longint;
  bFAttachedAutoRefresh: Boolean;
begin
  Assert(State = dsBrowse);
  Assert(Assigned(CallBack));
  bFAttachedAutoRefresh := False; //avoiding compiler hint
  if AllowEdit then
  begin
    SetTempState(dsEdit);
    bFAttachedAutoRefresh := FAttachedAutoRefresh;
    FAttachedAutoRefresh := False;
  end;
  Common.Lock;
  try
    if UseCurrentIndex then
    begin
      with FCurIndex.References do
      begin
        iCount := Count;
        for i := 0 to iCount-1 do
        begin
          FOverrideActiveRecordBuffer := Items[i];
          if not UseFilterAndRange
          or not FIsFiltered
          or FilterRecord(FOverrideActiveRecordBuffer, False) then
            CallBack(0, i, iCount);
        end
      end;
    end
    else
    begin
      with TkbmProtCommon(Common).FRecords do
      begin
        iCount := Count;
        for i := 0 to iCount-1 do
        begin
          FOverrideActiveRecordBuffer := Items[i];
          if FOverrideActiveRecordBuffer = nil then Continue;
          iVersion := 0;
          repeat
            if not UseFilterAndRange
            or not (FIsFiltered or Filtered)
            or FilterRecord(FOverrideActiveRecordBuffer, True) then
              CallBack(iVersion, i, iCount);
            FOverrideActiveRecordBuffer := FOverrideActiveRecordBuffer^.PrevRecordVersion;
            Inc(iVersion);
          until (FOverrideActiveRecordBuffer = nil) or (not IncludeHistory);
        end
      end;
    end;
  finally
    try
      FOverrideActiveRecordBuffer := nil;
      if AllowEdit then
      begin
        RestoreState(dsBrowse);
        FAttachedAutoRefresh := bFAttachedAutoRefresh;
        Common.RebuildCurrentIndexes;
        if FAttachedAutoRefresh then
          Common.RefreshTables(nil);

      end;
    finally
      Common.Unlock;
    end;
  end;
end;

procedure TISKbmMemDataSet.LoadFromUniversalStream(const ms: IEvDualStream);
var
  r: TEvKbmDataReader;
  lc: Boolean;
begin
  Close;
  lc := LogChanges;
  r := TEvKbmDataReader.Create;
  try
    LogChanges := False;
    r.KbmTable := Self;
    r.Read(ms);
  finally
    r.Free;
    LogChanges := lc;
  end;
end;

procedure TISKbmMemDataSet.MergeChangeLog;
var
  i: LongInt;
begin
  if EnableVersioning then
  begin
    i := RecNo;
    DisableControls;
    try
      if AttachedTo <> nil then
        AttachedTo.CheckPoint
      else
        CheckPoint;
    finally
      RecNo := i;
      EnableControls;
    end;
    IsDataModified := False;
  end;
end;

procedure TISKbmMemDataSet.OnBeforeFieldDefAdded(const FieldName: string;
  var DataType: TFieldType; var DataLength: Integer;
  var Required: Boolean);
begin
end;

procedure TISKbmMemDataSet.RefreshInternalCalcFields(Buffer: PChar);
begin
  CalculateFields(Buffer);
end;

procedure TISKbmMemDataSet.RestoreRecordIdPosition;
var
  Index: Integer;
begin
  if (State = dsBrowse) and (GetActiveRecord <> nil) and (GetActiveRecord^.RecordID <> FRecordIdPosition) then
  begin
    CurIndex.SearchRecordID(FRecordIdPosition, Index);
    if Index <> 0 then
      RecNo := Index;
  end;
end;

procedure TISKbmMemDataSet.SaveToUniversalStream(const DataAndIntCalcOnly, IgnoreFilterAndRange: Boolean; const AStream: IEvDualStream);
var
  f: array of record f: TField; p: TPumpClass1Pump end;
  LRecords: TkbmList;
  i, j, fHigh, RecCount, WrittenRecCount: Integer;
  pc: TPumpClass1;
  OldGrowChunkSize: Integer;
  OrigStreamPos: Integer;
begin
  pc := TPumpClass1.Create;
  try
    SetLength(f, 0);
    for i := 0 to FieldCount-1 do
      if (Fields[i].FieldKind in [fkData, fkInternalCalc])
      or not DataAndIntCalcOnly then
      begin
        SetLength(f, Length(f)+1);
        f[High(f)].f := Fields[i];
        case Fields[i].DataType of
        ftInteger:
          f[High(f)].p := pc.PumpInteger;
        ftLargeint:
          f[High(f)].p := pc.PumpInt64;
        ftFloat, ftCurrency, ftDateTime, ftDate:
          f[High(f)].p := pc.PumpFloat;
        ftString:
          f[High(f)].p := pc.PumpString;
        ftBlob, ftGraphic, ftMemo:
          f[High(f)].p := pc.PumpBlob;
        ftBoolean:
          f[High(f)].p := pc.PumpBool;
        else
          Assert(False, 'Unexpected data type '+ FieldTypeNames[Fields[i].DataType]);
        end;
      end;

    fHigh := High(f);
    if IgnoreFilterAndRange then
      LRecords := TkbmProtCommon(Common).FRecords
    else
      LRecords := FCurIndex.References;

    OldGrowChunkSize := AStream.GrowChunkSize;
    if AStream.GrowChunkSize <  64 * 1024 then
      AStream.GrowChunkSize := 64 * 1024;

    OrigStreamPos := AStream.Position;
    pc.FStream := AStream;
    AStream.WriteBuffer(UniversalDataReaderVersionSignature, SizeOf(UniversalDataReaderVersionSignature)); // version info
    AStream.WriteInteger(-1);
    AStream.WriteInteger(fHigh + 1);
    for i := 0 to fHigh do
    begin
      AStream.WriteString(f[i].f.FieldName);
      AStream.WriteInteger(Ord(f[i].f.DataType));
      AStream.WriteInteger(f[i].f.Size);
      AStream.WriteBoolean(f[i].f.Required);
    end;
    Common.Lock;
    try
      WrittenRecCount := 0;
      RecCount := LRecords.Count;
      for i := 0 to RecCount-1 do
      begin
        FOverrideActiveRecordBuffer := LRecords[i];
        if (FOverrideActiveRecordBuffer = nil)
        or (FOverrideActiveRecordBuffer^.UpdateStatus = usDeleted) then
          Continue;
        if RecalcOnFetch then
          GetCalcFields(PChar(FOverrideActiveRecordBuffer));
        if IgnoreFilterAndRange
        or not FIsFiltered
        or FilterRecord(FOverrideActiveRecordBuffer, False) then
        begin
          Inc(WrittenRecCount);
          for j := 0 to fHigh do
            with f[j] do
              p(f);
        end;
      end;
    finally
      Common.Unlock;
      FOverrideActiveRecordBuffer := nil;
    end;
    AStream.TruncateToPosition;
    AStream.Position := OrigStreamPos + SizeOf(UniversalDataReaderVersionSignature);
    AStream.WriteInteger(WrittenRecCount);
    AStream.Position := AStream.Size;
    AStream.GrowChunkSize := OldGrowChunkSize;
  finally
    pc.Free;
  end;
end;

procedure TISKbmMemDataSet.SaveRecordIdPosition;
begin
  if (State = dsBrowse) and (GetActiveRecord <> nil) then
    FRecordIdPosition := GetActiveRecord^.RecordID;
end;

procedure TISKbmMemDataSet.SetAggregates(const Value: TevKbmAggregates);
begin
  FAggregates.Assign(Value);
end;

procedure TISKbmMemDataSet.SetAggregatesActive(const Value: Boolean);
begin
  FAggregates.Active := Value;
end;

procedure TISKbmMemDataSet.SetControlType(const Value: TStrings);
begin
  FControlType.Assign(Value);
end;

procedure TISKbmMemDataSet.SetData(const Value: Variant);
var
  s: IisStream;
begin
  Close;
  EmptyDataSet;
  if not VarIsNull(Value) then
  begin
    if VarIsStr(Value) then
    begin
      s := TisStream.Create(Length(String(Value)));
      s.AsString := Value;
    end

    else if VarIsArray(Value) then
    begin
      s := TisStream.CreateFromVariant(Value);
    end

    else
    begin
      s := IInterface(Value) as IisStream;
    end;  

    s.Position := 0;
    LoadFromUniversalStream(s);
  end;
end;

procedure TISKbmMemDataSet.SetDelta(const Value: IEvDualStream);
var
  f: TkbmBinaryStreamFormat;
begin
  f := TkbmBinaryStreamFormat.Create(nil);
  try
    Value.Position := 0;
    //IndexName := kbmRowOrderIndex;
    LoadFromStreamViaFormat(Value.RealStream, f);
    CurIndex.UpdateStatus := [usModified, usDeleted, usInserted];
    CurIndex.Rebuild;
    First;
  finally
    f.Free;
  end;
end;

procedure TISKbmMemDataSet.SetFiltered(Value: Boolean);
begin
  if Filtered <> Value then
    Assert(not FFilterIndexIsLocked);
  inherited;
end;

procedure TISKbmMemDataSet.SetFilterText(const Value: string);
var
  s: string;
begin
  s := StringReplace(StringReplace(Value, '%''', '*''', [rfReplaceAll]), '''%', '''*', [rfReplaceAll]);
  if s <> Filter then
    Assert(not FFilterIndexIsLocked);
  inherited SetFilterText(s);
end;

{procedure TISKbmMemDataSet.SetIndexFieldNames(const Value: string);
begin
  if Active then
  begin
    if Indexes.GetByFieldNames(Value) = nil then
      AddIndex(evKbmInternalIndexPrefix+ StringReplace(Value, ';', '', [rfReplaceAll]), Value, []);
    inherited SetIndexFieldNames(Value);
  end;
  FIndexFieldNames := Value;
end;}

procedure TISKbmMemDataSet.SetIndexFieldNames(FieldNames: string);
begin
  if Active
  and (Indexes.GetByFieldNames(FieldNames) = nil) then
    AddIndex(evKbmInternalIndexPrefix+ StringReplace(FieldNames, ';', '', [rfReplaceAll]), FieldNames, []);
  inherited;
end;

procedure TISKbmMemDataSet.SetIndexName(const Value: string);
begin
  if Active and (Indexes.Get(Value) = nil) and (IndexDefs.IndexOf(Value) <> -1) then
    Indexes.Add(IndexDefs.Find(Value));
  inherited IndexName := Value;
end;

procedure TISKbmMemDataSet.SetIsFiltered;
begin
  inherited;
  Aggregates.MarkDirty;
end;

procedure TISKbmMemDataSet.SetLogChanges(const Value: Boolean);
begin
  EnableVersioning := Value;
end;

procedure TISKbmMemDataSet.SetParams(const Value: TParams);
begin
  FParams.Assign(Value);
end;

procedure TISKbmMemDataSet.SetPictureMasks(const Value: TStrings);
begin
  FPictureMasks.Assign(Value);
end;

procedure TISKbmMemDataSet.SetProviderName(const Value: string);
begin
  FProviderName := Value;
end;

procedure TISKbmMemDataSet.StartTransaction;
begin
  inherited;
end;

procedure TISKbmMemDataSet.SwitchToIndex(Index: TkbmIndex);
begin
  Assert(not FFilterIndexIsLocked);
  if Active then
    CheckBrowseMode;
  inherited;
  Aggregates.MarkDirty;
end;

procedure TISKbmMemDataSet.SetDefaultFields(const AValue: Boolean);
begin
  inherited SetDefaultFields(AValue);
end;

{ TEvKbmDataReader }

procedure TEvKbmDataReader.AfterAssignFieldDefs;
begin
  KbmTable.Open;
end;

procedure TEvKbmDataReader.AfterNewRecord;
begin
  with TkbmProtCustomMemTable(KbmTable), TkbmProtCommon(KbmTable.Common) do
  begin
    FOverrideActiveRecordBuffer^.RecordID:=FRecordID;
    inc(FRecordID);
    FOverrideActiveRecordBuffer^.UniqueRecordID:=FUniqueRecordID;
    inc(FUniqueRecordID);
    FOverrideActiveRecordBuffer^.Flag:=kbmrfInTable;
    FRecords.Add(FOverrideActiveRecordBuffer);
    inc(FLoadCount);
    //ClearCalcFields(PChar(FOverrideActiveRecordBuffer));
    GetCalcFields(PChar(FOverrideActiveRecordBuffer));
  end;
end;

procedure TEvKbmDataReader.AfterRead;
begin
  with TkbmProtCustomMemTable(KbmTable), TkbmProtCommon(KbmTable.Common) do
  begin
    try
      FOverrideActiveRecordBuffer := nil;
      RestoreState(dsBrowse);
      FIgnoreReadOnly:=false;
      FIgnoreAutoIncPopulation:=false;
      FEnableIndexes := True;
      ClearModified;
      DoAfterOpen;
      DoCustomAfterOpen;
      //Common.MarkIndexesDirty;
      //CurIndex.Rebuild;
    finally
      Common.Unlock;
    end;

    ClearBuffers;
    MasterChanged(nil);
    First;
    TableState:=mtstBrowse;
    EnableControls;
    Refresh;
  end;
end;

procedure TEvKbmDataReader.AssignFieldDef(
  const FieldInfo: TEvUnversalDataReaderFieldRecord;
  out PumpIndex: Integer);
var
  lFieldName: string;
  lDataType: TFieldType;
  lDataLength: Integer;
  lRequired: Boolean;
begin
  PumpIndex := 0;
  if KbmTable.FieldCount = 0 then
  begin
    lFieldName := FieldInfo.FieldName;
    lDataType := FieldInfo.DataType;
    lDataLength := FieldInfo.DataLength;
    lRequired := FieldInfo.Required;
    lRequired := False; // no required field mapping.
    KbmTable.OnBeforeFieldDefAdded(lFieldName, lDataType, lDataLength, lRequired);
    KbmTable.FieldDefs.Add(lFieldName, lDataType, lDataLength, lRequired);
  end;
end;

procedure TEvKbmDataReader.AssignPumpData(
  const FieldInfo: TEvUnversalDataReaderFieldRecord; out Data: Pointer);
begin
  Data := KbmTable.FindField(FieldInfo.FieldName);
end;

procedure TEvKbmDataReader.BeforeAssignFieldDefs(
  const FieldCount: Integer);
begin
  KbmTable.DisableControls;
  KbmTable.Persistent := False;
  KbmTable.Close;
  KbmTable.EmptyTable;
  KbmTable.FieldDefs.Clear;
end;

procedure TEvKbmDataReader.BeforeNewRecord;
begin
  with TkbmProtCustomMemTable(KbmTable), TkbmProtCommon(KbmTable.Common) do
  begin
    FOverrideActiveRecordBuffer:=_InternalAllocRecord;
  end;
end;

procedure TEvKbmDataReader.BeforeRead(const RecordCount: Cardinal);
begin
  KbmTable.Capacity := RecordCount;
  with TkbmProtCustomMemTable(KbmTable), TkbmProtCommon(KbmTable.Common) do
  begin
    Common.Lock;
    FEnableIndexes := False;
    FIgnoreReadOnly := true;
    FIgnoreAutoIncPopulation := true;
    SetTempState(dsInsert);
    ResetAutoInc;
    TableState := mtstLoad;
  end;
end;

procedure TEvKbmDataReader.PumpBlob(const Data: Pointer; const DataSize: Integer; const DataStr: string); 
var
  DestinationBuffer: PChar;
  ppVarLength:PPkbmVarLength;
begin
  DestinationBuffer := TkbmProtCommon(KbmTable.Common).GetFieldPointer(KbmTable.GetActiveRecord, TField(Data));
  DestinationBuffer^:=kbmffData;
  Inc(DestinationBuffer);
  ppVarLength := PPkbmVarLength(DestinationBuffer);
  if DataSize > 0 then
    ppVarLength^ := AllocVarLengthAs(@(DataStr[1]), DataSize)
  else
    ppVarLength^ := AllocVarLength(0);
end;

procedure TEvKbmDataReader.PumpBoolean(const Data: Pointer; const b: Boolean);
begin
  TField(Data).AsBoolean := b;
end;

procedure TEvKbmDataReader.PumpDouble(const Data: Pointer; const d: Double);
begin
  TField(Data).AsFloat := d;
end;

procedure TEvKbmDataReader.PumpInt64(const Data: Pointer; const i: Int64);
begin
  TField(Data).Value := i;
end;

procedure TEvKbmDataReader.PumpInteger(const Data: Pointer; const i: Integer);
begin
  TField(Data).AsInteger := i;
end;

procedure TEvKbmDataReader.PumpNull(const Data: Pointer);
begin
  //TField(Data).Clear;
end;

procedure TEvKbmDataReader.PumpString(const Data: Pointer; const s: string);
begin
  TField(Data).AsString := s
end;

{ TevKbmAggregates }

function TevKbmAggregates.Add: TevKbmAggregate;
begin
  Result := TevKbmAggregate(inherited Add);
  Result.FDataSet := TISKbmMemDataSet(GetOwner);
end;

procedure TevKbmAggregates.GlobalBeginUpdating;
var
  i: Integer;
begin
  for i := 0 to Count- 1 do
    with Items[i] do
      if Active then
        BeginUpdating;
end;

constructor TevKbmAggregates.Create(Owner: TPersistent);
begin
  inherited Create(TevKbmAggregate);
  Assert(Owner is TISKbmMemDataSet);
  FOwner := Owner;
end;

procedure TevKbmAggregates.DoAfterDelete;
begin
  if Active then
    GlobalEndUpdating(True);
end;

procedure TevKbmAggregates.DoAfterPost;
begin
  if Active then
  begin
    GlobalAdd;
    GlobalEndUpdating(True);
  end;
end;

procedure TevKbmAggregates.DoBeforeDelete;
begin
  if Active then
  begin
    GlobalBeginUpdating;
    GlobalRemove;
  end;
end;

procedure TevKbmAggregates.DoBeforeEdit;
begin
  if Active then
  begin
    GlobalBeginUpdating;
    GlobalRemove;
  end;
end;

procedure TevKbmAggregates.DoBeforeInsert;
begin
  if Active then
    GlobalBeginUpdating;
end;

procedure TevKbmAggregates.GlobalEndUpdating(const Commit: Boolean);
var
  i: Integer;
begin
  for i := 0 to Count- 1 do
    with Items[i] do
      if Active then
        EndUpdating(Commit);
end;

function TevKbmAggregates.Find(const AggregateName: string): TevKbmAggregate;
var
  i: Integer;
begin
  i := IndexOf(AggregateName);
  if i = -1 then
    Result := nil
  else
    Result := Items[i];
end;

function TevKbmAggregates.GetItem(Index: Integer): TevKbmAggregate;
begin
  Result := TevKbmAggregate(inherited GetItem(Index));
end;

function TevKbmAggregates.GetOwner: TPersistent;
begin
  Result := FOwner;
end;

function TevKbmAggregates.IndexOf(const AggregateName: string): Integer;
begin
  for Result := 0 to Count- 1 do
    if AnsiCompareText(TevKbmAggregate(Items[Result]).AggregateName, AggregateName) = 0 then
      Exit;
  Result := -1;
end;

procedure TevKbmAggregates.SetItem(Index: Integer; Value: TevKbmAggregate);
begin
  inherited SetItem(Index, Value);
end;

procedure TevKbmAggregates.GlobalAdd;
var
  i: Integer;
begin
  for i := 0 to Count- 1 do
    with Items[i] do
      if Active then
        Add;
end;

procedure TevKbmAggregates.GlobalRemove;
var
  i: Integer;
begin
  for i := 0 to Count- 1 do
    with Items[i] do
      if Active then
        Remove;
end;

procedure TevKbmAggregates.SetActive(const Value: Boolean);
begin
  if not FActive and Value then
    MarkDirty;
  FActive := Value;
end;

procedure TevKbmAggregates.MarkDirty;
var
  i: Integer;
begin
  if Active then
    for i := 0 to Count-1 do
      with Items[i] do
        FDirty := True;
end;

{ TevKbmAggregate }

procedure TevKbmAggregate.Add;
begin
  if not FDirty then
    FAddProc(FDataSet.FieldByName(FFieldName), FUpdatingValue);
end;

procedure TevKbmAggregate.Add_Max(const f: TField; var Value: Variant);
var
  d: Double;
begin
  d := f.AsFloat;
  if d > Value then
    Value := d;
end;

procedure TevKbmAggregate.Add_Max_Str(const f: TField; var Value: Variant);
var
  s: string;
begin
  s := f.AsString;
  if s > Value then
    Value := s;
end;

procedure TevKbmAggregate.Add_Sum(const f: TField; var Value: Variant);
begin
  Value := Value+ f.AsFloat;
end;

procedure TevKbmAggregate.Assign(Source: TPersistent);
begin
  if Source is TevKbmAggregate then
  begin
    AggregateName := TevKbmAggregate(Source).AggregateName;
    Expression := TevKbmAggregate(Source).Expression;
    Active := TevKbmAggregate(Source).Active;
  end
  else
    inherited;
end;

procedure TevKbmAggregate.BeginUpdating;
begin
  FUpdatingValue := FValue;
end;

procedure TevKbmAggregate.EndUpdating(const Commit: Boolean);
begin
  if Commit then
    FValue := FUpdatingValue;
end;

function TevKbmAggregate.GetValue: Variant;
begin
  if FDirty then
    ReCalcAggregate;
  Result := FValue;
end;

procedure TevKbmAggregate.ReCalcAggregate;
var
  f: TField;
  i: Integer;
begin
  Assert(Active, 'Aggregate needs to be active');
  Assert(FDataSet.Active, 'Aggregate needs active dataset');
  f := FDataSet.FindField(FFieldName);
  Assert(Assigned(f), 'Aggregated field '+ FFieldName+ ' is missing in dataset');
  case FAggregateType of
  atSum:
  begin
    FAddProc:= Add_Sum;
    FRemoveProc := Remove_Sum;
    FValue := 0;
  end;
  atMax:
    if f is TStringField then
    begin
      FAddProc:= Add_Max_Str;
      FRemoveProc := Remove_Max_Str;
      FValue := '';
    end
    else
    begin
      FAddProc:= Add_Max;
      FRemoveProc := Remove_Max;
      FValue := 0;
    end
  else
    Assert(False, 'Unsupported aggregation type '+ evKbmAggregateTypeName[FAggregateType]);
  end;
  try
    with FDataSet.FCurIndex.References, FDataSet do
      for i := 0 to Count-1 do
      begin
        FOverrideActiveRecordBuffer := Items[i];
        if not FIsFiltered
        or FilterRecord(FOverrideActiveRecordBuffer, False) then
          FAddProc(f, FValue);
      end;
  finally
    FDataSet.FOverrideActiveRecordBuffer := nil;
  end;
  FDirty := False;
end;

procedure TevKbmAggregate.Remove;
begin
  if not FDirty then
    FRemoveProc(FDataSet.FieldByName(FFieldName), FUpdatingValue);
end;

procedure TevKbmAggregate.Remove_Max(const f: TField; var Value: Variant);
begin
  if f.AsFloat = Value then
    FDirty := True;
end;

procedure TevKbmAggregate.Remove_Max_Str(const f: TField;
  var Value: Variant);
begin
  if f.AsString = Value then
    FDirty := True;
end;

procedure TevKbmAggregate.Remove_Sum(const f: TField; var Value: Variant);
begin
  Value := Value- f.AsFloat;
end;

procedure TevKbmAggregate.SetActive(const Value: Boolean);
begin
  if Value and not FActive then
    FDirty := True;
  FActive := Value;
end;

procedure TevKbmAggregate.SetExpression(const Value: string);
var
  t: TevKbmAggregateType;
  i: Integer;
  f: string;
  s, sValue: string;
begin
  sValue := Trim(Value);
  for t := High(evKbmAggregateTypeName) downto atNone do
  begin
    s := evKbmAggregateTypeName[t]+ '(';
    if AnsiCompareText(s, Copy(sValue, 1, Length(s))) = 0 then
      Break;
  end;
  if t <> atNone then
  begin
    Delete(sValue, 1, Length(s));
    i := Pos(')', sValue);
    if i = Length(sValue) then
      f := Copy(sValue, 1, i-1)
    else
      t := atNone;
  end;
  Assert(t <> atNone, 'Unsupported expression: '+ Value);
  FExpression := Value;
  FFieldName := f;
  FAggregateType := t;
  FAddProc:= nil;
  FRemoveProc := nil;
  FDirty := True;
end;

{ TIsKbmMemDataSetDeltaHandler }

procedure TIsKbmMemDataSetDeltaHandler.CallOnUpdateRecord(
  const UpdateStatus: TUpdateStatus);
begin
  if Assigned(FOnUpdateRecord) then
    FOnUpdateRecord(Self, UpdateStatus);
end;

procedure TIsKbmMemDataSetDeltaHandler.DeleteRecord(var Retry: boolean;
  var State: TUpdateStatus);
begin
  CallOnUpdateRecord(State);
end;

procedure TIsKbmMemDataSetDeltaHandler.InsertRecord(var Retry: boolean;
  var State: TUpdateStatus);
begin
  CallOnUpdateRecord(State);
end;

procedure TIsKbmMemDataSetDeltaHandler.ModifyRecord(var Retry: boolean;
  var State: TUpdateStatus);
begin
  CallOnUpdateRecord(State);
end;

{ TPumpClass1 }

procedure TPumpClass1.PumpBlob(const ff: TField);
begin
  if not PumpNull(ff) then
    FStream.WriteString(ff.AsString);
end;

procedure TPumpClass1.PumpBool(const ff: TField);
begin
  if not PumpNull(ff) then
    FStream.WriteBoolean(ff.AsBoolean);
end;

procedure TPumpClass1.PumpFloat(const ff: TField);
begin
  if not PumpNull(ff) then
    FStream.WriteDouble(ff.AsFloat);
end;

procedure TPumpClass1.PumpInt64(const ff: TField);
begin
  if not PumpNull(ff) then
    FStream.WriteInt64(ff.Value);
end;

procedure TPumpClass1.PumpInteger(const ff: TField);
begin
  if not PumpNull(ff) then
    FStream.WriteInteger(ff.AsInteger);
end;

function TPumpClass1.PumpNull(const ff: TField): Boolean;
begin
  FStream.WriteBoolean(ff.IsNull);
  Result := ff.IsNull;
end;

procedure TPumpClass1.PumpString(const ff: TField);
var
  s: String;
begin
  if not PumpNull(ff) then
  begin
    s := ff.AsString;
    if ff.Size = 1 then
      if s = '' then
        FStream.WriteByte(0)
      else
        FStream.WriteByte(Ord(s[1]))

    else if ff.Size <= 255 then
      FStream.WriteShortString(s)

    else
      FStream.WriteString(s);
  end;
end;

end.
