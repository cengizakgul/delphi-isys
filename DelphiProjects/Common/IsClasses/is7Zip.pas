unit is7Zip;

interface

uses SysUtils, isBaseClasses, SevenZip, evStreamUtils, isBasicUtils, isExceptions;

type
  Tis7ZipCompressionLevel = 0..9;

  function  CompressStream7z(const ASource: IevDualStream; const ACompressionLevel: Tis7ZipCompressionLevel): IevDualStream;
  function  DecompressStream7z(const ASource: IevDualStream): IevDualStream;
  function  CompressFile7z(const AFile: String; const ACompressionLevel: Tis7ZipCompressionLevel): IevDualStream;
  function  CompressFiles7z(const AFolder: String; const AFiles: String; const ACompressionLevel: Tis7ZipCompressionLevel): IevDualStream;
  procedure ExtractFiles7z(const AArchiveFilename, AFileList, TargetFolder: string);

implementation

function CompressStream7z(const ASource: IevDualStream; const ACompressionLevel: Tis7ZipCompressionLevel): IevDualStream;
var
  sSourceFile, sResultFile: String;
begin
  LoadSevenZipDLL;
  sSourceFile := GetUniqueID + '.tmp';
  sResultFile := ChangeFileExt(sSourceFile, '.7z');
  try
    ASource.SaveToFile(AppTempFolder + sSourceFile);
    SevenZipCreateArchive(0, sResultFile, AppTempFolder, sSourceFile, ACompressionLevel, False, False, False);
  finally
    DeleteFile(AppTempFolder + sSourceFile);
  end;

  Result := TEvDualStreamHolder.CreateFromFile(AppTempFolder + sResultFile, True);
end;

function DecompressStream7z(const ASource: IevDualStream): IevDualStream;
var
  sSourceFile, sResultFileDir, s: String;
  F: IisStringList;
begin
  LoadSevenZipDLL;
  sSourceFile := AppTempFolder + GetUniqueID + '.tmp';
  sResultFileDir := AppTempFolder + GetUniqueID + '\';
  try
    CreateDir(sResultFileDir);
    ASource.SaveToFile(sSourceFile);
    SevenZipExtractArchive(0, sSourceFile, '*', False, False, sResultFileDir, False);
    F := GetFilesByMask(sResultFileDir + '\*');
    CheckCondition(F.Count = 1, 'Incompatible stream format');
    s := AppTempFolder + ExtractFileName(F[0]);
    DeleteFile(s);
    MoveFile(F[0], s);
    Result := TEvDualStreamHolder.CreateFromFile(s, True);
  finally
    DeleteFile(sSourceFile);
    ClearDir(sResultFileDir, True);
  end;
end;

function CompressFile7z(const AFile: String; const ACompressionLevel: Tis7ZipCompressionLevel): IevDualStream;
var
  sResultFile: String;
begin
  LoadSevenZipDLL;
  sResultFile := AppTempFolder + GetUniqueID + '.tmp';
  if SevenZipCreateArchive(0, sResultFile, ExtractFilePath(AFile), ExtractFileName(AFile), ACompressionLevel, False, False, False) <> 0 then
    raise EisException.Create('SevenZipCreateArchive returned an error');
  Result := TEvDualStreamHolder.CreateFromFile(sResultFile, True);
end;

function CompressFiles7z(const AFolder: String; const AFiles: String; const ACompressionLevel: Tis7ZipCompressionLevel): IevDualStream;
var
  sResultFile: String;
begin
  LoadSevenZipDLL;
  sResultFile := AppTempFolder + GetUniqueID + '.tmp';
  if SevenZipCreateArchive(0, sResultFile, AFolder, AFiles, ACompressionLevel, True, False, False) <> 0 then
    raise EisException.Create('SevenZipCreateArchive returned an error');
  Result := TEvDualStreamHolder.CreateFromFile(sResultFile, True);
end;

procedure ExtractFiles7z(const AArchiveFilename, AFileList, TargetFolder: string);
begin
  LoadSevenZipDLL;
  SevenZipExtractArchive(0, AArchiveFilename, AFileList, False, False, TargetFolder, False);
end;

initialization
  LoadSevenZipDLL;

finalization
  UnloadSevenZipDLL;

end.