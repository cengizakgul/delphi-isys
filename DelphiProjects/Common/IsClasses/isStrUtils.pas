unit isStrUtils;

interface

uses Windows, DB, SysUtils, Graphics;

function  GetNextStrValue(var AStr: string; const ADelim: String): String;
procedure AddStrValue(var AStr: String; const AValue: String; const ADelim: String = ',');

function  FormatDisplayKey(const AFields: TFields; const ADisplayKeyFormat: String): string;

implementation

function GetNextStrValue(var AStr: string; const ADelim: String): String;
var
  j: Integer;
begin
  j := Pos(ADelim, AStr);
  if j = 0 then
  begin
    Result := AStr;
    AStr := '';
  end

  else
  begin
    Result := Copy(AStr, 1, j - 1);
    Delete(AStr, 1, j - 1 + Length(ADelim));
  end;
end;

function FormatDisplayKey(const AFields: TFields; const ADisplayKeyFormat: String): string;
var
  j, fldWidth: Integer;
  FmtStr, s, fmt, sFldName, sOpt, fldJust, txt: String;
  Fld: TField;
begin
  Result := '';
  FmtStr := ADisplayKeyFormat;
  while FmtStr <> '' do
  begin
    txt := GetNextStrValue(FmtStr, '[');
    Result := Result + txt;
    
    if FmtStr = '' then
      break;

    s := GetNextStrValue(FmtStr, ']');
    sFldName := GetNextStrValue(s, ':');
    fldWidth := 0;
    fldJust := '<';
    while s <> '' do
    begin
      sOpt := GetNextStrValue(s, ':');
      if sOpt[1] = 'W' then
      begin
        Delete(sOpt, 1, 1);
        fldWidth := StrToIntDef(sOpt, 0);
      end
      else if sOpt[1] = 'J' then
      begin
        Delete(sOpt, 1, 1);
        fldJust := sOpt;
      end;
    end;

    j := StrToIntDef(sFldName, -1);

    if j = -1 then
      Fld := AFields.FindField(sFldName)
    else
      Fld := AFields.Fields[j];

    if Assigned(Fld) then
      s := Fld.AsString
    else
      s := '[UNKNOWN_FIELD]';

    fmt := '%';
    if fldJust = '<' then
      fmt := fmt + '-';

    if fldWidth > 0 then
      fmt := fmt + IntToStr(fldWidth);

    fmt := fmt + 's';

    s := Format(fmt, [s]);

    Result := Result + s;
  end;
end;

procedure AddStrValue(var AStr: String; const AValue: String; const ADelim: String = ',');
begin
  if AStr <> '' then
    AStr := AStr + ADelim + AValue
  else
    AStr := AStr + AValue;
end;


end.