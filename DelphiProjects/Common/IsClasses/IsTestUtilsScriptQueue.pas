unit IsTestUtilsScriptQueue;

interface
uses
  classes;

type
  TisQueuedTestScriptStatus = ( tsesWaiting, tsesRunning, tsesCompleted, tsesFailed );

  IisTestScriptQueueEventSink = interface
['{1A3DCEA4-932F-4882-B53F-E9FB75A54F13}']
    procedure QueueChanged;
    procedure QueueStatusChanged;
  end;

  IisTestScriptQueue = interface
['{EFBFC052-4368-44CD-8C23-8AF7A5712D68}']
    procedure AddScript( filename: string );
    procedure Advise( sink: IisTestScriptQueueEventSink );
    procedure Unadvise( sink: IisTestScriptQueueEventSink );
    function Count: integer;
    function IsModified: boolean;
    procedure MarkAsUnmodififed; //!!
    function Filename: string;

    procedure New;
    procedure Open( afilename: string );
    procedure Save;
    procedure SaveAs( afilename: string );

    procedure Delete( idx: integer );
    procedure DeleteAll;
    procedure MoveUp( idx: integer );
    procedure MoveDown( idx: integer );

    procedure RunTests( from: integer );

    function GetScriptFilename(idx: integer): string;
    function GetScriptComments(idx: integer): string;
    function GetScriptStatus(idx: integer): TisQueuedTestScriptStatus;

    property ScriptFilename[idx: integer]: string read GetScriptFilename;
    property ScriptComments[idx: integer]: string read GetScriptComments;
    property ScriptStatus[idx: integer]: TisQueuedTestScriptStatus read GetScriptStatus;
  end;

function TestScriptQueue: IisTestScriptQueue;

function StrFromScriptStatus( st: TisQueuedTestScriptStatus ): string;

implementation

uses
  sysutils, istestutils, typinfo;

var
  GTestScriptQueue: IisTestScriptQueue;

type
  TisTestScriptData = class(TCollectionItem)
  private
    FFileName: string;
    FComments: string;
    FStatus: TisQueuedTestScriptStatus;
  public
    property Filename: string read FFilename write FFilename;
    property Comments: string read FComments write FComments;
    property Status: TisQueuedTestScriptStatus read FStatus write FStatus;
  end;

  TisTestScriptCollection = class ( TCollection )
  private
    function GetItem(Index: Integer): TisTestScriptData;
    procedure SetItem(Index: Integer; const Value: TisTestScriptData);
  public
    constructor Create;
    function Add: TisTestScriptData;
    function IndexOf( afilename: string ): integer;
    function IndexOfRunningScript: integer;
    property Items[Index: Integer]: TisTestScriptData read GetItem write SetItem; default;
  end;

  TisTestScriptQueue = class(TInterfacedObject, IisTestScriptQueue, IisTECallBackHolder)
  private
  private
    {IisTestScriptQueue}
    procedure AddScript( afilename: string );
    procedure Advise( sink: IisTestScriptQueueEventSink );
    procedure Unadvise( sink: IisTestScriptQueueEventSink );
    function IsModified: boolean;
    procedure MarkAsUnmodififed; //!!
    function Filename: string;

    procedure Delete( idx: integer );
    procedure DeleteAll;
    procedure MoveUp( idx: integer );
    procedure MoveDown( idx: integer );

    procedure New;
    procedure Open( afilename: string );
    procedure Save;
    procedure SaveAs( afilename: string );

    procedure RunTests( from: integer );
    function Count: integer;
    function GetScriptFilename(idx: integer): string;
    function GetScriptComments(idx: integer): string;
    function GetScriptStatus(idx: integer): TisQueuedTestScriptStatus;
    property ScriptFilename[idx: integer]: string read GetScriptFilename;

    {IisTECallBackHolder}
    procedure BeforeStartRecording(aScript: IisTEScript2);
    procedure AfterStopRecording(aScript: IisTEScript2);
    procedure BeforeStartPlayback(aScript: IisTEScript2);
    procedure AfterStopPlayback(aScript: IisTEScript2; status: TisTestScriptStatus);
  public
    constructor Create;
    destructor Destroy; override;
  private
    FQueue: TisTestScriptCollection;
    FSink: IisTestScriptQueueEventSink;
    FFilename: string;
    FModified: boolean;
    procedure FireQueueChanged;
    procedure FireQueueStatusChanged;
    procedure RunNextScript( idx: integer );
  end;

function TestScriptQueue: IisTestScriptQueue;
begin
  if GTestScriptQueue = nil then
  begin
    GTestScriptQueue := TisTestScriptQueue.Create;
  end;
  Result := GTestScriptQueue;
end;

{ TTestScriptQueue }

function TisTestScriptCollection.Add: TisTestScriptData;
begin
  Result := inherited Add as TisTestScriptData;
end;

constructor TisTestScriptCollection.Create;
begin
  inherited Create(TisTestScriptData);
end;

function TisTestScriptCollection.GetItem(Index: Integer): TisTestScriptData;
begin
  Result := inherited Items[ Index ] as TisTestScriptData;
end;

function TisTestScriptCollection.IndexOf(afilename: string): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to count-1 do
    if Items[i].Filename = afilename then
    begin
      Result := i;
      break;
    end;
end;

function TisTestScriptCollection.IndexOfRunningScript: integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to count-1 do
    if Items[i].Status = tsesRunning then
    begin
      Result := i;
      break;
    end;
end;

procedure TisTestScriptCollection.SetItem(Index: Integer;
  const Value: TisTestScriptData);
begin
  Items[Index] := Value;
end;

{ TisTestScriptQueue }

procedure TisTestScriptQueue.AddScript(afilename: string);
begin
  with FQueue.Add do
  begin
    Comments := LoadTestScript(afilename).Comments;
    Filename := afilename;
    Status := tsesWaiting;
  end;
  FModified := true;
  if assigned(FSink) then
    FSink.QueueChanged;
end;

procedure TisTestScriptQueue.Advise(sink: IisTestScriptQueueEventSink);
begin
  Assert( sink <> nil );
  FSink := sink;
end;

function TisTestScriptQueue.Count: integer;
begin
  Result := FQueue.Count;
end;

constructor TisTestScriptQueue.Create;
begin
  inherited;
  FQueue := TisTestScriptCollection.Create;
end;

destructor TisTestScriptQueue.Destroy;
begin
  inherited;
  FreeAndNil( FQueue );
end;

function TisTestScriptQueue.Filename: string;
begin
  Result := FFilename;
end;

function TisTestScriptQueue.GetScriptFilename(idx: integer): string;
begin
  Result := FQueue[idx].Filename;
end;

function TisTestScriptQueue.GetScriptComments(idx: integer): string;
begin
  Result := FQueue[idx].Comments;
end;

function TisTestScriptQueue.GetScriptStatus(
  idx: integer): TisQueuedTestScriptStatus;
begin
  Result := FQueue[idx].Status;
end;

function TisTestScriptQueue.IsModified: boolean;
begin
  Result := FModified;
end;

procedure TisTestScriptQueue.New;
begin
  FQueue.Clear;
  FFilename := '';
  FModified := false;
  FireQueueChanged;
end;

procedure TisTestScriptQueue.Open(afilename: string);
var
  sl: TStringList;
  i: integer;
begin
  FQueue.Clear;
  sl := TStringList.Create;
  try
    sl.LoadFromFile( afilename );
    for i := 0 to sl.Count-1 do
      AddScript( sl[i] );
  finally
    FreeAndNil(sl);
  end;
  FFilename := afilename;
  FModified := false;
  FireQueueChanged;
end;

procedure TisTestScriptQueue.FireQueueChanged;
begin
  if assigned(FSink) then
    FSink.QueueChanged;
end;

procedure TisTestScriptQueue.RunTests(from: integer);
var
  i: integer;
begin
  for i := from to Count-1 do
    FQueue[i].Status := tsesWaiting;
  FireQueueStatusChanged;
//  TestingEngine.Advise( Self );//!!
  RunNextScript(from-1);
end;

procedure TisTestScriptQueue.Save;
var
  sl: TStringList;
  i: integer;
begin
  sl := TStringList.Create;
  try
    for i := 0 to Count-1 do
      sl.Add( ScriptFilename[i] );
    sl.SaveToFile( FFilename );
  finally
    FreeAndNil(sl);
  end;
  FModified := false;
end;

procedure TisTestScriptQueue.SaveAs(afilename: string);
begin
  FFilename := afilename;
  FModified := true;
  Save;
end;

procedure TisTestScriptQueue.Unadvise(sink: IisTestScriptQueueEventSink);
begin
  Assert( FSink = sink );
  FSink := nil;
end;

function StrFromScriptStatus( st: TisQueuedTestScriptStatus ): string;
begin
  case st of
    tsesWaiting:   Result := 'Waiting';
    tsesRunning:   Result := 'Running';
    tsesCompleted: Result := 'Executed';
    tsesFailed:    Result := 'Failed';
  else
    Result := 'Unexpected status';
  end;
end;

procedure TisTestScriptQueue.Delete(idx: integer);
begin
  FQueue.Delete(idx);
//  if (Count = 0) and (FFilename = '') then
//    FModified := false;
  FireQueueChanged;
end;

procedure TisTestScriptQueue.DeleteAll;
begin
  FQueue.Clear;
//  if (Count = 0) and (FFilename = '') then
//    FModified := false;
  FireQueueChanged;
end;

procedure TisTestScriptQueue.MoveDown(idx: integer);
begin
  FQueue.Items[idx].Index := idx-1;
  FireQueueChanged;
end;

procedure TisTestScriptQueue.MoveUp(idx: integer);
begin
  FQueue.Items[idx].Index := idx+1;
  FireQueueChanged;
end;

procedure TisTestScriptQueue.AfterStopPlayback(aScript: IisTEScript2; status: TisTestScriptStatus);
var
  idx: integer;
begin
//  ODS( 'TisTestScriptQueue.AfterStopPlayback: %s', [GetEnumName(typeinfo(TisTestScriptStatus), ord(status))] );
  (TestingEngine as IisTestingEngineCP).Unadvise( self );
  idx := FQueue.IndexOfRunningScript;
  Assert( idx <> -1 );
  Assert( aScript.Filename = FQueue[idx].Filename );
//  ODS( 'TisTestScriptQueue.AfterStopPlayback: idx=%d', [idx] );
  if idx <> -1 then
  begin
    case status of
      psSuccess:     FQueue[idx].Status := tsesCompleted;
      psUserStopped: FQueue[idx].Status := tsesFailed;
      psFailure:     FQueue[idx].Status := tsesFailed;
    else
      Assert(false);
    end;
    FireQueueStatusChanged;
    if status = psSuccess then
      RunNextScript(idx);
  end
end;

procedure TisTestScriptQueue.AfterStopRecording(aScript: IisTEScript2);
begin
end;

procedure TisTestScriptQueue.BeforeStartPlayback(aScript: IisTEScript2);
begin
end;

procedure TisTestScriptQueue.BeforeStartRecording(aScript: IisTEScript2);
begin
end;

procedure TisTestScriptQueue.FireQueueStatusChanged;
begin
  if assigned(FSink) then
    FSink.QueueStatusChanged;
end;

procedure TisTestScriptQueue.RunNextScript(idx: integer);
var
  i: integer;
begin
  for i := idx+1 to Count-1 do
    if FQueue[i].Status = tsesWaiting then
    begin
      (TestingEngine as IisTestingEngineCP).Advise( Self );
      TestingEngine.StartPlayback( LoadTestScript(FQueue[i].Filename) );
      FQueue[i].Status := tsesRunning;
      FireQueueStatusChanged;
      break;
    end
end;

procedure TisTestScriptQueue.MarkAsUnmodififed;
begin
  FModified := false;
end;

initialization

finalization
  GTestScriptQueue := nil;

end.
