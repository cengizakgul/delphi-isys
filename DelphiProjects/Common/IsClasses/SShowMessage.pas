// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SShowMessage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, 
  StdCtrls, ExtCtrls, ImgList, Buttons, jpeg, ActnList, menus, isExceptions,
  ISBasicClasses, ISBasicUtils, ISErrorUtils, ISUtils, Consts;

type
  TShowMessage = class(TForm)
    Image: TImage;
    Memo: TMemo;
    Images: TImageList;
    btnSend: TSpeedButton;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnSendClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    FCancelExists: Boolean;
    FDetailsVisible: Boolean;
    FForDialog: Boolean;
    FMaxMsgLineCount: Integer;
    function  ShowUnattendedModal: Integer;
    procedure ShowHelp(Sender: TObject);
    procedure ClickDetailsButton(Sender: TObject);
    procedure SetHeight;
    procedure AdjustWindowSize;
    procedure SetMessage;
    procedure SetForm(const AMsg, ATimeStamp, ADetails: string;
                      const DlgType: TMsgDlgType;
                      const Buttons: TMsgDlgButtons;
                      const DefaultButton: TMsgDlgBtn;
                      const HelpCtx: Longint;
                      const ForDialog: Boolean);

  protected
    FMessage: String;
    FDetails: String;
    FDateTime: String;
    procedure Send;
  public
    IncludeScreen: Boolean;
    function ShowModal: Integer; override;
    class procedure ShowException(const AException: EisException; const AInclScreen: Boolean = False);
    class function  ShowMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons;
                                DefaultButton: TMsgDlgBtn; HelpCtx: Longint;
                                const InclScreen: Boolean = False): Word;
    class function  ShowDetailedMessage(const Msg, Details: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons;
                                DefaultButton: TMsgDlgBtn; HelpCtx: Longint;
                                const InclScreen: Boolean = False): Word;
    class function  ShowDialog(const ACaption, APrompt: string; var Value: string; ASecure: Boolean = False): Boolean;
    class function  ShowNbrDialog(const ACaption, APrompt: string; var Value: Extended;
                                  AIncrement: Extended = 1; AMinVal: Extended = 0; AMaxVal: Extended = 0): Boolean;
    class function  ShowChboxDialog(const ACaption, APrompt, ALabel: string; var Checked: Boolean): Boolean;
    class function  ShowDateDialog(const ACaption, APrompt: string; var Value: TDate ): Boolean;
    class function  ShowRadioDialog(const ACaption, APrompt: string; const Items: TStrings; var Index: Integer ): Boolean;
    class function  ShowMessageEx(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; const Captions: TStrings;
                                   DefaultButton: TMsgDlgBtn; HelpCtx: Longint;
                                   const InclScreen: Boolean = False): Word;
  end;


implementation

{$R *.DFM}

uses
  EvSendMail;

procedure TShowMessage.SetForm(const AMsg, ATimeStamp, ADetails: string;
  const DlgType: TMsgDlgType; const Buttons: TMsgDlgButtons; const DefaultButton: TMsgDlgBtn;
  const HelpCtx: Longint; const ForDialog: Boolean);
var
  lLeft: Integer;
  CompNbr: Integer;
  i, j, n, m: Integer;
  Btn: TISBitBtn;

  function CreateButton(ACaption: String; AType: TMsgDlgBtn; AResult, ImageIndex: Integer; AWidth: Integer = 75): TISBitBtn;
  var
    aBitmap: TBitmap;
  begin
    Result := TISBitBtn.Create(Self);
    Result.Width := AWidth;
    Result.Height := 25;
    Result.Parent := Self;
    Result.Top := Memo.Top + Memo.Height + 10;
    lLeft := lLeft - Result.Width - 10;
    Result.Left := lLeft;
    Result.ModalResult := AResult;
    Result.Caption := ACaption;
    Result.Anchors := [akRight, akBottom];
    Result.Tag := Ord(AType);
    if ImageIndex <> -1 then
    begin
      aBitmap := TBitmap.Create;
      try
        Images.GetBitmap(ImageIndex, aBitmap);
        aBitmap.Width := 32;
        aBitmap.Height := 16;
        Result.Glyph.Assign(aBitmap);
      finally
        aBitmap.Free;
      end;
      Result.NumGlyphs := 2;
    end;
  end;

  procedure SetCancelButton(AModalResult: TModalResult);
  var
    i: Integer;
  begin
    if FCancelExists then
      Exit;

    for i := CompNbr to ComponentCount - 1 do
      if TISBitBtn(Components[i]).ModalResult = AModalResult then
      begin
        TISBitBtn(Components[i]).Cancel := True;
        FCancelExists := True;
        break;
      end;
  end;

  procedure SetDefaultButton(AButton: TMsgDlgBtn);
  var
    i: Integer;
  begin
    for i := CompNbr to ComponentCount - 1 do
      if TISBitBtn(Components[i]).Tag = Ord(AButton) then
      begin
        ActiveControl := TISBitBtn(Components[i]);
        TISBitBtn(Components[i]).Default := True;
        Break;
      end;
  end;


begin
  btnSend.Visible := DlgType = mtError;

  Caption := Application.Title;
  lLeft := ClientWidth;
  CompNbr := ComponentCount;
  FDetailsVisible := False;
  FForDialog := ForDialog;

  FDetails := ADetails;
  FMessage := StringReplace(AMsg, #10, '', [rfReplaceAll]);
  FDateTime := ATimeStamp;
  Caption := Caption + ' ' + FDateTime;

  while (Length(FMessage) > 0) and (FMessage[Length(FMessage)] = #13) do
    Delete(FMessage, Length(FMessage), 1);

  if Length(FDetails) > 0 then
  begin
    Btn := CreateButton('Show Details', mbHelp, mrNone, 14, 100);
    Btn.OnClick := ClickDetailsButton;
  end;

  if HelpCtx <> 0 then
  begin
    Btn := CreateButton('Help', mbHelp, mrNone, 4);
    Btn.OnClick := ShowHelp;
    HelpContext := HelpCtx;
  end;

  if mbYesToAll in Buttons then
    CreateButton('Yes to All', mbYesToAll, mrYesToAll, 11);

  if mbNoToAll in Buttons then
    CreateButton('No to All', mbNoToAll, mrNoToAll, 9);

  if mbIgnore in Buttons then
    CreateButton('Ignore', mbIgnore, mrIgnore, 12);

  if mbRetry in Buttons then
    CreateButton('Retry', mbRetry, mrRetry, 13);

  if mbAbort in Buttons then
    CreateButton('Abort', mbAbort, mrAbort, 7);

  if mbCancel in Buttons then
    CreateButton('Cancel', mbCancel, mrCancel, 6);

  if mbOK in Buttons then
    CreateButton('OK', mbOK, mrOK, 5);

  if mbNo in Buttons then
    CreateButton('No', mbNo, mrNo, 8);

  if mbYes in Buttons then
    CreateButton('Yes', mbYes, mrYes, 10);

  j := 1;
  for i := ComponentCount - 1 downto CompNbr do
  begin
    TISBitBtn(Components[i]).TabOrder := j;
    Inc(j);
  end;

  if DlgType <> mtCustom then
    Images.GetBitmap(Ord(DlgType), Image.Picture.Bitmap)
  else
    Image.Visible := False;

  SetMessage;

  FCancelExists := False;

  AdjustWindowSize;

  if CompNbr < ComponentCount then
  begin
    ActiveControl := TISBitBtn(Components[ComponentCount - 1]);
    if btnSend.Visible then
      m := btnSend.BoundsRect.Right + 5
    else
      m := 10;
    if TISBitBtn(Components[ComponentCount - 1]).Left < m then
      Width := Width + (m - TISBitBtn(Components[ComponentCount - 1]).Left);

    n := (TISBitBtn(Components[ComponentCount - 1]).Left - m) div 2;

    for i := CompNbr to ComponentCount - 1 do
      TISBitBtn(Components[i]).Left := TISBitBtn(Components[i]).Left - n;

    SetCancelButton(mrCancel);
    SetCancelButton(mrAbort);
    SetCancelButton(mrNo);
    SetCancelButton(mrNoToAll);
  end;

  SetDefaultButton(DefaultButton);
end;


procedure TShowMessage.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if  not FCancelExists and (Key = VK_ESCAPE) then
  begin
    ModalResult := mrCancel;
    Close;
  end

  else if (Key = VK_F1) and (HelpContext <> 0) then
    ShowHelp(nil);
end;


procedure TShowMessage.ShowHelp(Sender: TObject);
begin
  Application.HelpContext(HelpContext);
end;

class function TShowMessage.ShowDialog(const ACaption, APrompt: string;
  var Value: string; ASecure: Boolean = False): Boolean;
var
  Frm: TShowMessage;
  anEdit: TISEdit;
begin
  Frm := TShowMessage.Create(Application);
  try
    Frm.SetForm(APrompt, '', '', mtCustom, [mbOK, mbCancel], mbOK, 0, True);
    if Frm.Width < 300 then
      Frm.Width := 300;
    anEdit := TISEdit.Create(Frm);
    anEdit.Width := Frm.Memo.Width;
    anEdit.Height := 21;
    anEdit.Parent := Frm;
    anEdit.Left := Frm.Memo.Left;
    anEdit.Top := Frm.Memo.Top + Frm.Memo.Height;
    anEdit.Text := Value;
    if ASecure then
      anEdit.PasswordChar := '*';
    Frm.ActiveControl := anEdit;
    Frm.Caption := ACaption;
    Result := Frm.ShowModal = mrOK;
    if Result then
      Value := anEdit.Text;
  finally
    Frm.Free;
  end;
end;

class function TShowMessage.ShowMessage(const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; DefaultButton: TMsgDlgBtn; HelpCtx: Integer; const InclScreen: Boolean = False): Word;
begin
  Result := ShowDetailedMessage(Msg, '', DlgType, Buttons, DefaultButton, HelpCtx, InclScreen);
end;

class function TShowMessage.ShowNbrDialog(const ACaption, APrompt: string;
  var Value: Extended; AIncrement, AMinVal, AMaxVal: Extended): Boolean;
var
  Frm: TShowMessage;
  anEdit: TISDBSpinEdit;
begin
  Frm := TShowMessage.Create(Application);
  try
    Frm.SetForm(APrompt, '', '', mtCustom, [mbOK, mbCancel], mbOK, 0, True);
    if Frm.Width < 100 then
      Frm.Width := 100;
    anEdit := TISDBSpinEdit.Create(Frm);
    anEdit.Width := Frm.Memo.Width;
    anEdit.Height := 21;
    anEdit.Parent := Frm;
    anEdit.Left := Frm.Memo.Left;
    anEdit.Top := Frm.Memo.Top + Frm.Memo.Height;
    anEdit.Increment := AIncrement;
    anEdit.MinValue := AMinVal;
    anEdit.MaxValue := AMaxVal;
    anEdit.Value := Value;

    Frm.ActiveControl := anEdit;
    Frm.Caption := ACaption;
    Result := Frm.ShowModal = mrOK;
    if Result then
      Value := anEdit.Value;
  finally
    Frm.Free;
  end;
end;

class function TShowMessage.ShowChboxDialog(const ACaption, APrompt, ALabel: string;
  var Checked: Boolean): Boolean;
var
  Frm: TShowMessage;
  ChBox: TISCheckBox;
begin
  Frm := TShowMessage.Create(Application);
  try
    Frm.SetForm(APrompt, '', '', mtCustom, [mbOK, mbCancel], mbOK, 0, True);
    if Frm.Width < 100 then
      Frm.Width := 100;
    ChBox := TISCheckBox.Create(Frm);
    ChBox.Caption := ALabel;
    ChBox.Width := Frm.Memo.Width;
    ChBox.Height := 21;
    ChBox.Parent := Frm;
    ChBox.Left := Frm.Memo.Left;
    ChBox.Top := Frm.Memo.Top + Frm.Memo.Height;
    ChBox.Checked := Checked;

    Frm.ActiveControl := ChBox;
    Frm.Caption := ACaption;
    Result := Frm.ShowModal = mrOK;
    if Result then
      Checked := ChBox.Checked;
  finally
    Frm.Free;
  end;
end;

procedure TShowMessage.Send;
var
  DetailName, FileName, h: string;
  t: TStringList;
  b: TBitMap;
  j: TJPEGImage;
  F: TextFile;
begin
  inherited;
  FileName := AppTempFolder + '_ErrorMessage.jpg';
  DetailName := AppTempFolder + '_ErrorDetails.txt';
  try
    t := TStringList.Create;
    try
      if IncludeScreen then
      begin
        b := TBitMap.Create;
        j := TJPEGImage.Create;
        try
          ScreenShot(0, 0, Screen.Width, Screen.Height, b);
          j.CompressionQuality := 50;
          j.Assign(b);
          j.SaveToFile(FileName);
          t.Add(FileName);
        finally
          b.Free;
          j.Free;
        end;
      end;
      if Length(FDetails) > 0 then
      begin
        AssignFile(F, DetailName);
        try
          Rewrite(F);
          Writeln(F, AdjustLineBreaks(FMessage));
          Write(F, AdjustLineBreaks(FDetails));
        finally
          CloseFile(F);
        end;
        t.Add(DetailName);
      end;
      h := FMessage;
      MailTo(nil, nil, nil, t, ExtractFileName(Application.ExeName) + ' error - ' + FDateTime,
             AdjustLineBreaks(h));
    finally
      t.Free;
    end;
  finally
//    DeleteFile(FileName);
//    DeleteFile(DetailName);
  end;
end;

class function TShowMessage.ShowDateDialog(const ACaption, APrompt: string;
  var Value: TDate): Boolean;
var
  Frm: TShowMessage;
  anEdit: TISDateTimePicker;
begin
  Frm := TShowMessage.Create(Application);
  try
    Frm.SetForm(APrompt, '', '', mtCustom, [mbOK, mbCancel], mbOK, 0, True);
    if Frm.Width < 300 then
      Frm.Width := 300;
    anEdit := TISDateTimePicker.Create(Frm);
    anEdit.Width := 150;//Frm.Memo.Width;
    anEdit.Height := 21;
    anEdit.Parent := Frm;
    anEdit.Left := Frm.Memo.Left;
    anEdit.Top := Frm.Memo.Top + Frm.Memo.Height;
    if Value <> 0 then
      anEdit.Date := Value;
//    anEdit.Time := 0;
    Frm.ActiveControl := anEdit;
    Frm.Caption := ACaption;
    Result := Frm.ShowModal = mrOK;
    if Result then
      Value := anEdit.Date;
  finally
    Frm.Free;
  end;
end;

procedure TShowMessage.AdjustWindowSize;
var
  i, w, maxwidth, MaxTextWidth, CaptionWidth: Integer;
  NonClientMetrics: TNonClientMetrics;
  sl: TStringList;
begin
  Canvas.Font.Assign(Memo.Font);

  if Image.Visible then
    Memo.Left := Image.Width + Image.Left*2
  else
    Memo.Left := 6;

  NonClientMetrics.cbSize := sizeof(NonClientMetrics);
  if SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(NonClientMetrics), @NonClientMetrics, 0) then
  begin
    Canvas.Font.Handle := CreateFontIndirect(NonClientMetrics.lfCaptionFont);
    try
      CaptionWidth := Canvas.TextWidth(Caption)+35;
    finally
      DeleteObject(Canvas.Font.Handle);
    end;
  end
  else CaptionWidth := Canvas.TextWidth(Caption)+35;

  MaxTextWidth := 0;
  sl := TStringList.Create;
  try
    Canvas.Font.Assign(Memo.Font);
    sl.Text := Memo.Lines.Text;

    for i := 0 to sl.Count - 1 do
    begin
      if FDetailsVisible then
        w := Canvas.TextWidth(sl[i]) + GetSystemMetrics(SM_CXVSCROLL) + 8
      else
        w := Canvas.TextWidth(sl[i]);

      if MaxTextWidth < w then
        MaxTextWidth := w;
    end;
  finally
    sl.Free;
  end;

  maxwidth := MaxTextWidth + Memo.Left + 6;

  if maxwidth > (Screen.Width div 2) then
    maxwidth := Screen.Width div 2;

  ClientWidth := maxwidth;

  if Width < CaptionWidth then
    Width := CaptionWidth;

  Memo.Width := MaxTextWidth;
  if Memo.Width > (ClientWidth - Memo.Left - 6) then
    Memo.Width := (ClientWidth - Memo.Left - 6);

  SetHeight;

  if Memo.Left < ((ClientWidth - Memo.Width) div 2) then
    Memo.Left := (ClientWidth - Memo.Width) div 2;

  Memo.Lines.Text := Memo.Lines.Text;
  Left := (Screen.Width - Width) div 2;
  Top := (Screen.Height - Height) div 2;
end;

procedure TShowMessage.ClickDetailsButton(Sender: TObject);
var
  i: Integer;
  aBitmap: TBitmap;
begin
  FDetailsVisible := not FDetailsVisible;
  if FDetailsVisible then
  begin
    TISBitBtn(Sender).Caption := 'Hide Details';
    i := 15;
  end
  else
  begin
    TISBitBtn(Sender).Caption := 'Show Details';
    i := 14;
  end;

  aBitmap := TBitmap.Create;
  try
    Images.GetBitmap(i, aBitmap);
    aBitmap.Width := 32;
    aBitmap.Height := 16;
    TISBitBtn(Sender).Glyph.Assign(aBitmap);
  finally
    aBitmap.Free;
  end;

  Memo.Lines.Text := '';
  if FDetailsVisible then
    Memo.Lines.Text := FMessage + #13 + FDetails
  else
    SetMessage;

  AdjustWindowSize;

  if FDetailsVisible then
  begin
    Memo.ScrollBars := ssBoth;
    Memo.WordWrap := False;
    Memo.BorderStyle := bsSingle;
  end;
end;

procedure TShowMessage.SetHeight;
var
  i: Integer;
begin
  Memo.WordWrap := True;
  Memo.BorderStyle := bsNone;

  if 13 * Memo.Lines.Count > 267 then
  begin
    Height := 350;
    Memo.ScrollBars := ssVertical;
  end
  else
  begin
    Memo.ScrollBars := ssNone;
    i := 13 * Memo.Lines.Count;

    if FDetailsVisible then
    begin
      if i < 100 then
        i := 100;
    end
    else if i < Image.Height then
      i := Image.Height;

    Height := Height - (Memo.Height - i);
  end;

  if FForDialog then
  begin
    Height := Height + 21;
    Memo.Height := Memo.Height - 21;
  end;
end;

procedure TShowMessage.FormCreate(Sender: TObject);
begin
  Height := 350;
  Width := 700;
end;

procedure TShowMessage.btnSendClick(Sender: TObject);
begin
  Send;
end;

class function TShowMessage.ShowRadioDialog(const ACaption,
  APrompt: string; const Items: TStrings; var Index: Integer): Boolean;
var
  Frm: TShowMessage;
  anGroup: TISRadioGroup;
begin
  Frm := TShowMessage.Create(Application);
  try
    Frm.SetForm(APrompt, '', '', mtCustom, [mbOK, mbCancel], mbOK, 0, True);
    if Frm.Width < 300 then
      Frm.Width := 300;
    anGroup := TISRadioGroup.Create(Frm);
    anGroup.Width := Frm.Memo.Width;
    anGroup.Columns := Items.Count;
    anGroup.Height := 35;
    anGroup.Parent := Frm;
    anGroup.Left := Frm.Memo.Left;
    anGroup.Top := Frm.Memo.Top + Frm.Memo.Height - 10;
    anGroup.Items.Assign(Items);
    anGroup.ItemIndex := Index;
    Frm.ActiveControl := anGroup;
    Frm.Caption := ACaption;
    Result := Frm.ShowModal = mrOK;
    if Result then
      Index := anGroup.ItemIndex;
  finally
    Frm.Free;
  end;
end;

class function TShowMessage.ShowMessageEx(const Msg: string;
  DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; const Captions: TStrings;
  DefaultButton: TMsgDlgBtn; HelpCtx: Integer;
  const InclScreen: Boolean): Word;
var
  Frm: TShowMessage;
  i: integer;
  Idx: integer;
begin
  Frm := TShowMessage.Create(Application);
  try
    Frm.IncludeScreen := InclScreen;
    Frm.SetForm(Msg, '', '', DlgType, Buttons, DefaultButton, HelpCtx, False);

    if Assigned(Captions) and (Captions.Count > 0) then
    for i := 0 to Frm.ComponentCount - 1 do
      if Frm.Components[i] is TISBitBtn then
      begin
        idx := Captions.IndexOfName( TISBitBtn(Frm.Components[i]).Caption );
        if idx >= 0 then
        begin
          TISBitBtn(Frm.Components[i]).Caption := Captions.ValueFromIndex[idx];
        end;
      end;

    Result := Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;

class procedure TShowMessage.ShowException(const AException: EisException; const AInclScreen: Boolean);
var
  Frm: TShowMessage;
  sDet: String;
begin
  Frm := TShowMessage.Create(Application);
  try
    Frm.IncludeScreen := AInclScreen;
    Frm.FMaxMsgLineCount := 5;

    sDet := AException.Details;
    AddStrValue(sDet, AException.StackInfo, #13);

    Frm.SetForm(AException.Message, FormatDateTime('mm/dd/yy hh:nn:ss', AException.TimeStamp),
                sDet, mtError, [mbOK], mbOK, AException.HelpContext, False);
    Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;

procedure TShowMessage.SetMessage;
var
  s, sMsg, sLine: String;
  i: Integer;
begin
  if FMaxMsgLineCount > 0 then
  begin
    s := FMessage;
    sMsg := '';
    i := 0;
    while s <> '' do
    begin
      sLine := Trim(GetNextStrValue(s, #13));
      if sLine <> '' then
      begin
        AddStrValue(sMsg, sLine, #13);
        Inc(i);
        if i = FMaxMsgLineCount then
          break;
      end;
    end;
    if s <> '' then
      AddStrValue(sMsg, '...', #13);
  end
  else
    sMsg := FMessage;
  Memo.Lines.Text := sMsg;
end;

class function TShowMessage.ShowDetailedMessage(const Msg, Details: string;
  DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; DefaultButton: TMsgDlgBtn;
  HelpCtx: Integer; const InclScreen: Boolean): Word;
var
  Frm: TShowMessage;
begin
  Frm := TShowMessage.Create(Application);
  try
    Frm.IncludeScreen := InclScreen;
    Frm.SetForm(Msg, '', Details, DlgType, Buttons, DefaultButton, HelpCtx, False);
    Result := Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;

function TShowMessage.ShowModal: Integer;
begin
  if Application.Terminated then
    Result := ShowUnattendedModal
  else
    Result := inherited ShowModal;
end;

function TShowMessage.ShowUnattendedModal: Integer;
begin
  btnSend.Hide;
  Screen.Cursor := crDefault;  
  Show;
  try
    ModalResult := 0;
    repeat
      Application.HandleMessage;
      if (ModalResult = 0) and not Visible then
        ModalResult := mrCancel;
    until ModalResult <> 0;
    Result := ModalResult;
  finally
    Hide;
  end;
end;

end.
