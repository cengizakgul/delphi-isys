unit DataDictPrimaryKeyEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictItemEditFrm, DataDictNamedItemEditFrm,
  DataDictParamEditFrm, StdCtrls, ExtCtrls, DataDictCollectionEditFrm,
  rwCustomDataDictionary, DataDictAvailableItemsFrm;

type
  TDataDictPrimaryKeyEdit = class(TDataDictCollectionEdit)
  private
  protected
    function  GetItemText(const AItem: TCollectionItem): String; override;
    procedure SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean); override;
    procedure DeleteItem(const AItem: TCollectionItem); override;
  public
  end;

implementation

{$R *.dfm}

{ TddPrimaryKeyEdit }

procedure TDataDictPrimaryKeyEdit.DeleteItem(const AItem: TCollectionItem);
begin
  TDataDictPrimKeyField(AItem).DataDictionaryObject.DeletePrimaryKeyField(TDataDictPrimKeyField(AItem));
end;

function TDataDictPrimaryKeyEdit.GetItemText(const AItem: TCollectionItem): String;
begin
  Result := TDataDictKeyField(AItem).Field.Name;
end;

procedure TDataDictPrimaryKeyEdit.SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean);
var
  flds: String;
  i: Integer;
begin
  flds := '';
  for i := 0 to TableEditor.ddTable.Fields.Count - 1 do
    if TableEditor.ddTable.PrimaryKey.FindField(TableEditor.ddTable.Fields[i]) = nil then
    begin
      if flds <> '' then
        flds := flds + Chr(VK_RETURN);
      flds := flds + TableEditor.ddTable.Fields[i].Name;
    end;

  flds := ShowAvailableFields(flds, TableEditor, False, 'Available Table Fields');

  Accept := flds <> '';
  if Accept  then
    TDataDictKeyField(AItem).Field := TableEditor.ddTable.Fields.FieldByName(flds);
end;

end.
