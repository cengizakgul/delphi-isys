unit DataDictNamedItemEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, rwCustomDataDictionary, DataDictItemEditFrm, ExtCtrls;

type
  TDataDictNamedItemEdit = class(TDataDictItemEdit)
    lName: TLabel;
    edName: TEdit;
    lDisplayName: TLabel;
    edDisplayName: TEdit;
    lNotes: TLabel;
    memNotes: TMemo;
    procedure memNotesExit(Sender: TObject);
    procedure edNameExit(Sender: TObject);
    procedure edDisplayNameExit(Sender: TObject);
    procedure edNameChange(Sender: TObject);
    procedure edDisplayNameChange(Sender: TObject);
    procedure memNotesChange(Sender: TObject);
  protected
    function ddItem: TDataDictNamedItem;
  public
    procedure OnShowItem; override;
  end;

implementation

{$R *.dfm}

function TDataDictNamedItemEdit.ddItem: TDataDictNamedItem;
begin
  Result := TDataDictNamedItem(DataDictionaryItem);
end;

procedure TDataDictNamedItemEdit.OnShowItem;
begin
  if ddItem <> nil then
  begin
    Enabled := True;
    edName.Text := ddItem.Name;
    edDisplayName.Text := ddItem.DisplayName;
    memNotes.Text := ddItem.Notes;
  end
  else
  begin
    Enabled := False;
    edName.Text := '';
    edDisplayName.Text := '';
    memNotes.Text := '';
  end;
end;

procedure TDataDictNamedItemEdit.memNotesExit(Sender: TObject);
begin
  ddItem.Notes := memNotes.Text;
end;

procedure TDataDictNamedItemEdit.edNameExit(Sender: TObject);
begin
  OnExitEdit(Sender);
  ddItem.Name := edName.Text;
end;

procedure TDataDictNamedItemEdit.edDisplayNameExit(Sender: TObject);
begin
  OnExitEdit(Sender);
  ddItem.DisplayName := edDisplayName.Text;
end;

procedure TDataDictNamedItemEdit.edNameChange(Sender: TObject);
begin
  OnChange;
end;

procedure TDataDictNamedItemEdit.edDisplayNameChange(Sender: TObject);
begin
  OnChange;
end;

procedure TDataDictNamedItemEdit.memNotesChange(Sender: TObject);
begin
  OnChange;
end;

end.
