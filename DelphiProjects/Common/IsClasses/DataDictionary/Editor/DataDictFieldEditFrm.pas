unit DataDictFieldEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictNamedItemEditFrm, StdCtrls, DataDictDataTypeFrm, Grids,
  ValEdit, rwCustomDataDictionary, ExtCtrls, ComCtrls, rwEngineTypes;

type
  TDataDictFieldEdit = class(TDataDictNamedItemEdit)
    pcFieldProp: TPageControl;
    tsFieldInfo: TTabSheet;
    tsFieldValues: TTabSheet;
    vleValues: TValueListEditor;
    FieldType: TDataDictDataTypes;
    lSize: TLabel;
    edSize: TEdit;
    procedure vleValuesExit(Sender: TObject);
    procedure vleValuesStringsChange(Sender: TObject);
    procedure FieldTypecbTypeChange(Sender: TObject);
    procedure FieldTypecbTypeExit(Sender: TObject);
    procedure edSizeChange(Sender: TObject);
    procedure edSizeExit(Sender: TObject);
  private
  public
    procedure AfterConstruction; override;
    procedure OnShowItem; override;  
  end;


implementation

uses DataDictItemEditFrm;

{$R *.dfm}

{ TDataDictFieldEdit }

procedure TDataDictFieldEdit.OnShowItem;
begin
  inherited;
  if ddItem <> nil then
  begin
    FieldType.DataType := TDataDictField(ddItem).FieldType;
    edSize.Text := IntToStr(TDataDictField(ddItem).Size);
    vleValues.Strings := TDataDictField(ddItem).FieldValues;
  end
  else
  begin
    FieldType.DataType := ddtUnknown;
    edSize.Text := '';
    vleValues.Strings.Text := '';
  end;
end;

procedure TDataDictFieldEdit.vleValuesExit(Sender: TObject);
begin
  TDataDictField(ddItem).FieldValues := vleValues.Strings;
end;

procedure TDataDictFieldEdit.vleValuesStringsChange(Sender: TObject);
begin
  OnChange;
end;

procedure TDataDictFieldEdit.FieldTypecbTypeChange(Sender: TObject);
begin
  OnChange;
end;

procedure TDataDictFieldEdit.FieldTypecbTypeExit(Sender: TObject);
begin
  TDataDictField(ddItem).FieldType := FieldType.DataType;
end;

procedure TDataDictFieldEdit.AfterConstruction;
begin
  inherited;
  pnlContainer.Parent := tsFieldInfo;
end;

procedure TDataDictFieldEdit.edSizeChange(Sender: TObject);
begin
  OnChange;
end;

procedure TDataDictFieldEdit.edSizeExit(Sender: TObject);
begin
  OnExitEdit(Sender);
  TDataDictField(ddItem).Size := StrToInt(edSize.Text);
end;

end.
