object DataDictTableEditor: TDataDictTableEditor
  Left = 0
  Top = 0
  Width = 386
  Height = 487
  AutoScroll = False
  Constraints.MinHeight = 250
  Constraints.MinWidth = 267
  Enabled = False
  TabOrder = 0
  DesignSize = (
    386
    487)
  object pcTableProps: TPageControl
    Left = 0
    Top = 0
    Width = 386
    Height = 487
    ActivePage = tsTableInfo
    Align = alClient
    TabOrder = 0
    object tsTableInfo: TTabSheet
      Caption = 'Table Info'
    end
    object tsFields: TTabSheet
      Caption = 'Fields'
      ImageIndex = 1
    end
    object tsPrimaryKey: TTabSheet
      Caption = 'Primary Key'
      ImageIndex = 2
    end
    object tsForeignKey: TTabSheet
      Caption = 'Foreign Keys'
      ImageIndex = 3
    end
  end
  object btnCancel: TButton
    Left = 215
    Top = 452
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    Enabled = False
    ModalResult = 2
    TabOrder = 2
    OnClick = btnCancelClick
  end
  object btnOK: TButton
    Left = 130
    Top = 452
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Enabled = False
    ModalResult = 1
    TabOrder = 1
    OnClick = btnOKClick
  end
  object btnApply: TButton
    Left = 299
    Top = 452
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Apply'
    Enabled = False
    TabOrder = 3
    OnClick = btnApplyClick
  end
end
