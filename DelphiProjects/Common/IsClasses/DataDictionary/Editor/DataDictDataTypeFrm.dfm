object DataDictDataTypes: TDataDictDataTypes
  Left = 0
  Top = 0
  Width = 193
  Height = 21
  AutoScroll = False
  Constraints.MinHeight = 21
  TabOrder = 0
  DesignSize = (
    193
    21)
  object lName: TLabel
    Left = 0
    Top = 2
    Width = 24
    Height = 13
    Caption = 'Type'
  end
  object cbType: TComboBox
    Left = 82
    Top = 0
    Width = 111
    Height = 21
    Style = csDropDownList
    Anchors = [akLeft, akTop, akRight]
    ItemHeight = 13
    TabOrder = 0
  end
end
