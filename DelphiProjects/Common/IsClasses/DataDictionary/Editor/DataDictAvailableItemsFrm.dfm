object DataDictAvailableItems: TDataDictAvailableItems
  Left = 469
  Top = 219
  Width = 193
  Height = 245
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'Available Items'
  Color = clBtnFace
  Constraints.MinHeight = 157
  Constraints.MinWidth = 190
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    185
    218)
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 26
    Top = 194
    Width = 75
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object Button2: TButton
    Left = 110
    Top = 194
    Width = 75
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object lbItems: TListBox
    Left = 0
    Top = 0
    Width = 185
    Height = 189
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    Sorted = True
    TabOrder = 2
  end
end
