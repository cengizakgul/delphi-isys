unit DataDictForeignKeyFieldEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, rwCustomDataDictionary;

type
  TDataDictForeignKeyFieldEdit = class(TFrame)
    lFieldName: TLabel;
    lSeparator: TLabel;
    cbFields: TComboBox;
  private
    function  GetField: TDataDictField;
    procedure SetField(const Value: TDataDictField);
  public
    procedure Initialize(const AKeyField: TDataDictField; const AFields: TDataDictFields); virtual;
    property  Field: TDataDictField read GetField write SetField;
  end;

implementation

{$R *.dfm}


{ TDataDictForeignKeyField }

function TDataDictForeignKeyFieldEdit.GetField: TDataDictField;
begin
  if cbFields.ItemIndex <> -1 then
    Result := TDataDictField(cbFields.Items.Objects[cbFields.ItemIndex])
  else
    Result := nil;  
end;

procedure TDataDictForeignKeyFieldEdit.Initialize(const AKeyField: TDataDictField; const AFields: TDataDictFields);
var
  i: Integer;
begin
  cbFields.Clear;
  for i := 0 to AFields.Count -1 do
  begin
    if AKeyField.FieldType = AFields[i].FieldType then
      cbFields.AddItem(AFields[i].Name, AFields[i]);
  end;
  cbFields.Sorted := True;

  lFieldName.Caption := AKeyField.Name;

  Field := AFields.FieldByName(AKeyField.Name);
end;

procedure TDataDictForeignKeyFieldEdit.SetField(const Value: TDataDictField);
begin
  cbFields.ItemIndex := cbFields.Items.IndexOfObject(Value);
end;

end.
