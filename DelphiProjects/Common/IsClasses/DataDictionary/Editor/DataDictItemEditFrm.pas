unit DataDictItemEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DataDictTableEditorFrm, ExtCtrls;

type
  TDataDictItemEdit = class(TFrame)
    pnlContainer: TPanel;
    procedure OnExitEdit(Sender: TObject);
  private
    FDataDictionaryItem: TObject;
    procedure SetDataDictionaryItem(const Value: TObject);
  protected
    function TableEditor: TDataDictTableEditor;
  public
    property  DataDictionaryItem: TObject read FDataDictionaryItem write SetDataDictionaryItem;
    procedure OnShowItem; virtual; abstract;
    procedure OnChange;
  end;

implementation

{$R *.dfm}


{ TDataDictItemEdit }

procedure TDataDictItemEdit.OnChange;
begin
  TableEditor.Modified := True;
end;

procedure TDataDictItemEdit.OnExitEdit(Sender: TObject);
begin
  TEdit(Sender).Text := Trim(TEdit(Sender).Text);
end;


procedure TDataDictItemEdit.SetDataDictionaryItem(const Value: TObject);
begin
  FDataDictionaryItem := Value;
  OnShowItem;
end;

function TDataDictItemEdit.TableEditor: TDataDictTableEditor;
var
  O: TComponent;
begin
  Result := nil;
  O := Owner;
  while Assigned(O) and not(O is TForm) do
    if O is TDataDictTableEditor then
    begin
      Result := TDataDictTableEditor(O);
      break;
    end
    else
      O := O.Owner;
end;

end.
