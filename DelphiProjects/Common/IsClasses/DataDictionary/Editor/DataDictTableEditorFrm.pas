unit DataDictTableEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ComCtrls, rwCustomDataDictionary;

type
  TDataDictTableItemType = (ddTITPrimaryKey, ddForeignKeys);

  TDataDictTableEditor = class(TFrame)
    pcTableProps: TPageControl;
    tsTableInfo: TTabSheet;
    tsFields: TTabSheet;
    tsPrimaryKey: TTabSheet;
    tsForeignKey: TTabSheet;
    btnCancel: TButton;
    btnOK: TButton;
    btnApply: TButton;
    procedure btnApplyClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    FEditCollection: TOwnedCollection;
    FEditingTable: TDataDictTable;
    FOnApplyChanges: TNotifyEvent;
    function GetddTable: TDataDictTable;
    function GetModified: Boolean;
    procedure SetModified(const Value: Boolean);
  protected
    procedure PrepareToEdit; virtual; abstract;
  public
    DataDictionary: TDataDictionary;

    destructor Destroy; override;
    procedure  ApplyChanges;
    procedure  ShowTable(const Table: TDataDictTable);
    procedure  RefreshTableItem(const ATableItemType: TDataDictTableItemType); virtual; abstract;

    property   ddTable: TDataDictTable read GetddTable;
    property   Modified: Boolean read GetModified write SetModified;
    property   OnApplyChanges: TNotifyEvent read FOnApplyChanges write FOnApplyChanges;
  end;

implementation

{$R *.dfm}

{ TDataDictTableEditor }

destructor TDataDictTableEditor.Destroy;
begin
  FreeAndNil(FEditCollection);
  inherited;
end;

procedure TDataDictTableEditor.ShowTable(const Table: TDataDictTable);
begin
  if Assigned(FEditingTable) and Modified and
     (MessageDlg('Do you want to save changes?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    ApplyChanges;

  // Copy Table into internal similar object
  FEditingTable := Table;
  FreeAndNil(FEditCollection);
  FEditCollection := TOwnedCollection.Create(DataDictionary, DataDictionary.GetTableClass);
  FEditCollection.Add;

  if Assigned(Table) then
    ddTable.Assign(FEditingTable);

  Enabled := Assigned(Table);
  btnCancel.Enabled := Enabled;
  btnOk.Enabled := Enabled;

  PrepareToEdit;

  Modified := False;  
end;

function TDataDictTableEditor.GetddTable: TDataDictTable;
begin
  Result := TDataDictTable(FEditCollection.Items[0]);
end;

procedure TDataDictTableEditor.btnApplyClick(Sender: TObject);
begin
  ApplyChanges;
end;

procedure TDataDictTableEditor.ApplyChanges;
begin
  FEditingTable.Assign(ddTable);
  Modified := False;
  if Assigned(FOnApplyChanges) then
    FOnApplyChanges(Self);
end;

function TDataDictTableEditor.GetModified: Boolean;
begin
  Result := btnApply.Enabled;
end;

procedure TDataDictTableEditor.SetModified(const Value: Boolean);
begin
  btnApply.Enabled := Value;
  btnCancel.Enabled := Value;
end;

procedure TDataDictTableEditor.btnOKClick(Sender: TObject);
begin
  if Modified then
    ApplyChanges;
end;

procedure TDataDictTableEditor.btnCancelClick(Sender: TObject);
begin
  if not (fsModal in Screen.ActiveForm.FormState) then
  begin
    ddTable.Assign(FEditingTable);
    PrepareToEdit;
    Modified := False;
  end;
end;


end.
