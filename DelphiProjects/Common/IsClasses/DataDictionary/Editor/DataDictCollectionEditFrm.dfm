inherited DataDictCollectionEdit: TDataDictCollectionEdit
  Width = 311
  Height = 149
  inherited pnlContainer: TPanel
    Width = 311
    Height = 149
    DesignSize = (
      311
      149)
    object lbCollection: TListBox
      Left = 0
      Top = 0
      Width = 137
      Height = 149
      Align = alLeft
      DragMode = dmAutomatic
      ItemHeight = 13
      TabOrder = 0
      OnClick = lbCollectionClick
      OnDragOver = lbCollectionDragOver
      OnEndDrag = lbCollectionEndDrag
    end
    object btnAddItem: TButton
      Left = 148
      Top = 126
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'New Item'
      TabOrder = 1
      OnClick = btnAddItemClick
    end
    object btnDeleteItem: TButton
      Left = 232
      Top = 126
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'Delete Item'
      TabOrder = 2
      OnClick = btnDeleteItemClick
    end
  end
end
