inherited DataDictFieldEdit: TDataDictFieldEdit
  Width = 293
  Height = 205
  inherited pnlContainer: TPanel
    Width = 293
    Height = 205
    DesignSize = (
      293
      205)
    inherited lName: TLabel
      Width = 53
      Caption = 'Field Name'
    end
    inherited lDisplayName: TLabel
      Top = 29
    end
    inherited lNotes: TLabel
      Top = 88
    end
    object lSize: TLabel [3]
      Left = 209
      Top = 56
      Width = 20
      Height = 13
      Caption = 'Size'
    end
    inherited edName: TEdit
      Width = 210
    end
    inherited edDisplayName: TEdit
      Width = 210
    end
    inherited memNotes: TMemo
      Top = 106
      Width = 293
      Height = 99
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 4
    end
    inline FieldType: TDataDictDataTypes
      Left = 0
      Top = 54
      Width = 193
      Height = 21
      AutoScroll = False
      Constraints.MinHeight = 21
      TabOrder = 2
      inherited lName: TLabel
        Width = 49
        Caption = 'Field Type'
      end
      inherited cbType: TComboBox
        OnChange = FieldTypecbTypeChange
        OnExit = FieldTypecbTypeExit
      end
    end
    object edSize: TEdit
      Left = 238
      Top = 54
      Width = 32
      Height = 21
      TabOrder = 3
      OnChange = edSizeChange
      OnExit = edSizeExit
    end
  end
  object pcFieldProp: TPageControl
    Left = 0
    Top = 0
    Width = 293
    Height = 205
    ActivePage = tsFieldInfo
    Align = alClient
    TabOrder = 1
    object tsFieldInfo: TTabSheet
      Caption = 'Field Info'
    end
    object tsFieldValues: TTabSheet
      Caption = 'Field Values'
      ImageIndex = 1
      object vleValues: TValueListEditor
        Left = 0
        Top = 0
        Width = 285
        Height = 177
        Align = alClient
        DefaultRowHeight = 17
        KeyOptions = [keyEdit, keyAdd, keyDelete, keyUnique]
        ScrollBars = ssVertical
        Strings.Strings = (
          '=')
        TabOrder = 0
        TitleCaptions.Strings = (
          'Value'
          'Description')
        OnExit = vleValuesExit
        OnStringsChange = vleValuesStringsChange
        ColWidths = (
          47
          232)
      end
    end
  end
end
