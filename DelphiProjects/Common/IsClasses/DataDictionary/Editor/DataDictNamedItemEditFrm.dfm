inherited DataDictNamedItemEdit: TDataDictNamedItemEdit
  Width = 249
  Height = 120
  inherited pnlContainer: TPanel
    Width = 249
    Height = 120
    DesignSize = (
      249
      120)
    object lName: TLabel
      Left = 0
      Top = 2
      Width = 28
      Height = 13
      Caption = 'Name'
    end
    object lDisplayName: TLabel
      Left = 0
      Top = 30
      Width = 65
      Height = 13
      Caption = 'Display Name'
    end
    object lNotes: TLabel
      Left = 0
      Top = 57
      Width = 60
      Height = 13
      AutoSize = False
      Caption = 'Description'
    end
    object edName: TEdit
      Left = 82
      Top = 0
      Width = 166
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      OnChange = edNameChange
      OnExit = edNameExit
    end
    object edDisplayName: TEdit
      Left = 82
      Top = 27
      Width = 167
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 1
      OnChange = edDisplayNameChange
      OnExit = edDisplayNameExit
    end
    object memNotes: TMemo
      Left = 0
      Top = 73
      Width = 249
      Height = 47
      Anchors = [akLeft, akRight, akBottom]
      TabOrder = 2
      OnChange = memNotesChange
      OnExit = memNotesExit
    end
  end
end
