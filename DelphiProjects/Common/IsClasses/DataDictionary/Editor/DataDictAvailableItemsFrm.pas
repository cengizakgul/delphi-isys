unit DataDictAvailableItemsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  // Show abstract list of items user provides (VK_RETURN delimited string)
  // Returns list of selected items (VK_RETURN delimited string)

  TDataDictAvailableItems = class(TForm)
    Button1: TButton;
    Button2: TButton;
    lbItems: TListBox;
  private
  public
  end;

  function ShowAvailableFields(const AItemList: String; const AOwner: TComponent = nil;
                               const AMultiSelect: Boolean = True; const ATitle: String = ''): String;

implementation

{$R *.dfm}



function ShowAvailableFields(const AItemList: String; const AOwner: TComponent = nil;
                             const AMultiSelect: Boolean = True; const ATitle: String = ''): String;
var
  Frm: TDataDictAvailableItems;
  i: Integer;
begin
  Result := '';
  Frm := TDataDictAvailableItems.Create(AOwner);
  with Frm do
    try
      if ATitle <> '' then
        Caption := ATitle;

      lbItems.Items.Text := AItemList;
      lbItems.MultiSelect := AMultiSelect;

      if ShowModal = mrOK then
        for i := 0 to lbItems.Count - 1 do
          if lbItems.Selected[i] then
          begin
            if Result <> '' then
              Result := Result + Chr(VK_RETURN);
            Result := Result + lbItems.Items[i];
          end;
    finally
      Free;
    end;
end;


end.
 