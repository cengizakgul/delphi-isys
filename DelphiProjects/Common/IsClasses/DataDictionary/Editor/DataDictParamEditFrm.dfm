inherited DataDictParamEdit: TDataDictParamEdit
  Width = 259
  Height = 147
  inherited pnlContainer: TPanel
    Width = 259
    Height = 147
    DesignSize = (
      259
      147)
    inherited lNotes: TLabel
      Top = 86
    end
    inherited edName: TEdit
      Width = 177
    end
    inherited edDisplayName: TEdit
      Width = 178
    end
    inherited memNotes: TMemo
      Top = 104
      Width = 259
      Height = 43
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 3
    end
    inline ParamType: TDataDictDataTypes
      Left = -1
      Top = 56
      Width = 260
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      AutoScroll = False
      Constraints.MinHeight = 21
      TabOrder = 2
      inherited cbType: TComboBox
        Width = 178
        OnChange = ParamTypecbTypeChange
        OnExit = ParamTypecbTypeExit
      end
    end
  end
  object pcParamProp: TPageControl
    Left = 0
    Top = 0
    Width = 259
    Height = 147
    ActivePage = tsParamInfo
    Align = alClient
    TabOrder = 1
    object tsParamInfo: TTabSheet
      Caption = 'Param Info'
    end
    object tsParamValues: TTabSheet
      Caption = 'Param Values'
      ImageIndex = 1
      object vleValues: TValueListEditor
        Left = 0
        Top = 0
        Width = 251
        Height = 119
        Align = alClient
        DefaultRowHeight = 17
        KeyOptions = [keyEdit, keyAdd, keyDelete, keyUnique]
        ScrollBars = ssVertical
        Strings.Strings = (
          '=')
        TabOrder = 0
        TitleCaptions.Strings = (
          'Value'
          'Description')
        OnExit = vleValuesExit
        OnStringsChange = vleValuesStringsChange
        ColWidths = (
          47
          198)
      end
    end
  end
end
