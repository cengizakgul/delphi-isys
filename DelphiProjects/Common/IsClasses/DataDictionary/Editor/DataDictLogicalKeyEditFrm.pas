unit DataDictLogicalKeyEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictCollectionEditFrm, StdCtrls, ExtCtrls, rwCustomDataDictionary,
  DataDictAvailableItemsFrm;

type
  TDataDictLogicalKeyEdit = class(TDataDictCollectionEdit)
  private
  protected
    function  GetItemText(const AItem: TCollectionItem): String; override;
    procedure SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean); override;
  public
  end;

implementation

{$R *.dfm}

{ TDataDictLogicalKeyEdit }

function TDataDictLogicalKeyEdit.GetItemText(const AItem: TCollectionItem): String;
begin
  Result := TDataDictKeyField(AItem).Field.Name;
end;

procedure TDataDictLogicalKeyEdit.SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean);
var
  flds: String;
  i: Integer;
begin
  flds := '';
  for i := 0 to TableEditor.ddTable.Fields.Count - 1 do
    if TableEditor.ddTable.LogicalKey.FindField(TableEditor.ddTable.Fields[i]) = nil then
    begin
      if flds <> '' then
        flds := flds + Chr(VK_RETURN);
      flds := flds + TableEditor.ddTable.Fields[i].Name;
    end;

  flds := ShowAvailableFields(flds, TableEditor, False, 'Available Table Fields');

  Accept := flds <> '';
  if Accept  then
    TDataDictKeyField(AItem).Field := TableEditor.ddTable.Fields.FieldByName(flds);
end;

end.
