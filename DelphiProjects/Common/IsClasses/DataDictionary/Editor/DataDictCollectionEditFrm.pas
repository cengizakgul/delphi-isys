unit DataDictCollectionEditFrm;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictItemEditFrm, StdCtrls, rwCustomDataDictionary, ExtCtrls;

type
  TDataDictCollectionEdit = class(TDataDictItemEdit)
    lbCollection: TListBox;
    btnAddItem: TButton;
    btnDeleteItem: TButton;
    procedure btnAddItemClick(Sender: TObject);
    procedure btnDeleteItemClick(Sender: TObject);
    procedure lbCollectionClick(Sender: TObject);
    procedure lbCollectionEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure lbCollectionDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);

  protected
    FItemFrame: TDataDictItemEdit;
    function  GetItemText(const AItem: TCollectionItem): String; virtual; abstract;
    procedure SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean); virtual; abstract;
    function  UpdateItem(const AItem: TCollectionItem): String;
    procedure DeleteItem(const AItem: TCollectionItem); virtual;

  public
    procedure OnShowItem; override;
    procedure SelectItem(const Index: Integer);
    function  SelectedItem: TCollectionItem;
  end;

implementation

uses DataDictTableEditorFrm;

{$R *.dfm}


{ TDataDictCollectionEdit }

procedure TDataDictCollectionEdit.OnShowItem;
var
  i: Integer;
begin
  lbCollection.Clear;
  lbCollection.Items.BeginUpdate;
  try
    for i := 0 to TCollection(DataDictionaryItem).Count - 1 do
      lbCollection.AddItem(GetItemText(TCollection(DataDictionaryItem).Items[i]), TCollection(DataDictionaryItem).Items[i]);
  finally
    lbCollection.Items.EndUpdate;
  end;

  if TCollection(DataDictionaryItem).Count > 0 then
    SelectItem(0)
  else
    SelectItem(-1);
end;


procedure TDataDictCollectionEdit.btnAddItemClick(Sender: TObject);
var
  C: TCollectionItem;
  fl: Boolean;
begin
  C := TCollection(DataDictionaryItem).Add;
  fl := True;
  SetDefaultValues(C, fl);
  if fl then
  begin
    SelectItem(lbCollection.Items.AddObject(GetItemText(C), C));
    OnChange;
  end
  else
    C.Free;
end;

procedure TDataDictCollectionEdit.btnDeleteItemClick(Sender: TObject);
var
  n: Integer;
begin
  if SelectedItem <> nil then
  begin
    n := lbCollection.ItemIndex;
    DeleteItem(SelectedItem);
    lbCollection.Items.Delete(lbCollection.ItemIndex);
    OnChange;
    if n > lbCollection.Count - 1 then
      Dec(n);
    SelectItem(n);
  end;
end;

procedure TDataDictCollectionEdit.lbCollectionClick(Sender: TObject);
var
  ch: Boolean;
begin
  if Assigned(FItemFrame) then
  begin
    ch := TableEditor.Modified;
    FItemFrame.DataDictionaryItem := SelectedItem;
    TableEditor.Modified := ch;
  end;
end;

function TDataDictCollectionEdit.UpdateItem(const AItem: TCollectionItem): String;
var
  i: Integer;
begin
  i := lbCollection.Items.IndexOfObject(AItem);
  if i <> -1 then
    lbCollection.Items[i] := GetItemText(AItem);
end;

function TDataDictCollectionEdit.SelectedItem: TCollectionItem;
begin
  if lbCollection.ItemIndex <> -1 then
    Result := TCollectionItem(lbCollection.Items.Objects[lbCollection.ItemIndex])
  else
    Result := nil;
end;

procedure TDataDictCollectionEdit.SelectItem(const Index: Integer);
begin
  lbCollection.ItemIndex := Index;
  lbCollection.OnClick(nil);
end;

procedure TDataDictCollectionEdit.lbCollectionEndDrag(Sender, Target: TObject; X, Y: Integer);
var
  i: Integer;
  P: TPoint;
begin
  if Assigned(Target) and (lbCollection.ItemIndex <> -1) then
  begin
    P := Point(X, Y);
    i := lbCollection.ItemAtPos(P, True);
    if i = -1 then
      i := lbCollection.Count - 1;
    if lbCollection.ItemIndex <> i then
    begin
      TCollection(DataDictionaryItem).Items[lbCollection.ItemIndex].Index := i;
      lbCollection.Items.Move(lbCollection.ItemIndex, i);
      lbCollection.ItemIndex := i;
      OnChange;
    end;
  end;
end;

procedure TDataDictCollectionEdit.lbCollectionDragOver(Sender,
  Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (Sender = lbCollection);
end;

procedure TDataDictCollectionEdit.DeleteItem(const AItem: TCollectionItem);
begin
  AItem.Free;
end;

end.
