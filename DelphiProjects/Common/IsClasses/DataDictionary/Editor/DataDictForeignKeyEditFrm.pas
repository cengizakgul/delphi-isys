unit DataDictForeignKeyEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DataDictForeignKeyFieldEditFrm, ExtCtrls, rwCustomDataDictionary,
  Math;

type
  TDataDictForeignKeyEdit = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    cbTables: TComboBox;
    gbLink: TGroupBox;
    pnlTitle: TPanel;
    lRefTable: TLabel;
    lTable: TLabel;
    procedure cbTablesChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
  protected
    FForeignKey: TDataDictForeignKey;
    procedure Initialize;
    procedure DeleteFields;
    procedure AddFields;
  public
  end;

  function ShowForeignKey(const AOwner: TComponent; const AForeignKey: TDataDictForeignKey): Boolean;

implementation

{$R *.dfm}

function ShowForeignKey(const AOwner: TComponent; const AForeignKey: TDataDictForeignKey): Boolean;
var
  Frm: TDataDictForeignKeyEdit;
  FKF: TDataDictForeignKeyField;
  i: Integer;
begin
  Frm := TDataDictForeignKeyEdit.Create(AOwner);
  with Frm do
    try
      FForeignKey := AForeignKey;
      Result := ShowModal = mrOK;
      if Result then
      begin
        AForeignKey.Fields.Clear;
        AForeignKey.Table := TDataDictTable(cbTables.Items.Objects[cbTables.ItemIndex]);

        for i := 0 to gbLink.ControlCount - 1 do
          if gbLink.Controls[i] is TDataDictForeignKeyFieldEdit then
          begin
            FKF := TDataDictForeignKeyField(AForeignKey.Fields.Add);
            FKF.Field := TDataDictForeignKeyFieldEdit(gbLink.Controls[i]).Field;
          end;
      end;
    finally
      Free;
    end;

end;

{ TDataDictForeignKeyEdit }

procedure TDataDictForeignKeyEdit.Initialize;
var
  T: TDataDictTables;
  i, j: Integer;
begin
  T := FForeignKey.DataDictionaryObject.Tables;

  cbTables.Clear;
  for i := 0 to T.Count - 1 do
    if T[i].PrimaryKey.Count > 0 then
      cbTables.AddItem(T[i].Name, T[i]);

  cbTables.ItemIndex := cbTables.Items.IndexOfObject(FForeignKey.Table);
  cbTables.OnChange(nil);

  j := 0;
  for i := 0 to gbLink.ControlCount - 1 do
    if gbLink.Controls[i] is TDataDictForeignKeyFieldEdit then
    begin
      if j < FForeignKey.Fields.Count then
      begin
        TDataDictForeignKeyFieldEdit(gbLink.Controls[i]).Field := FForeignKey.Fields[j].Field;
        Inc(j);
      end;
    end;

  lTable.Caption := TDataDictTable(TDataDictForeignKeys(FForeignKey.Collection).Owner).Name;
end;

procedure TDataDictForeignKeyEdit.cbTablesChange(Sender: TObject);
begin
  DeleteFields;

  if cbTables.ItemIndex <> -1 then
  begin
    AddFields;
    lRefTable.Caption := cbTables.Text;
  end
  else
    lRefTable.Caption := '';
end;

procedure TDataDictForeignKeyEdit.DeleteFields;
var
  i: Integer;
begin
  for i := gbLink.ControlCount - 1 downto 0 do
    if gbLink.Controls[i] is TDataDictForeignKeyFieldEdit then
       gbLink.Controls[i].Free;
end;

procedure TDataDictForeignKeyEdit.AddFields;
var
  T: TDataDictTable;
  Flds: TDataDictFields;
  i, y: Integer;
  F: TDataDictForeignKeyFieldEdit;
begin
  T := TDataDictTable(cbTables.Items.Objects[cbTables.ItemIndex]);
  Flds := TDataDictTable(TDataDictForeignKeys(FForeignKey.Collection).Owner).Fields;

  pnlTitle.Top := 0;
  y := pnlTitle.Top + pnlTitle.Height + 1;
  for i := 0 to T.PrimaryKey.Count - 1 do
  begin
    F := TDataDictForeignKeyFieldEdit.Create(Self);
    F.Name := F.Name + IntToStr(i);
    F.Parent := gbLink;    
    F.Initialize(T.PrimaryKey[i].Field, Flds);
    F.Align := alTop;
    F.Top := y;
    Inc(y, F.Height + 1);
  end;

  gbLink.Height := y + 6;
  ClientHeight := gbLink.Top + gbLink.Height + 35;
end;

procedure TDataDictForeignKeyEdit.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  i: Integer;
begin
  if ModalResult = mrOk then
  begin
    CanClose := cbTables.ItemIndex <> -1;
    for i := 0 to gbLink.ControlCount - 1 do
      if gbLink.Controls[i] is TDataDictForeignKeyFieldEdit then
         CanClose := CanClose and (TDataDictForeignKeyFieldEdit(gbLink.Controls[i]).Field <> nil);
  end;
end;

procedure TDataDictForeignKeyEdit.FormShow(Sender: TObject);
begin
  Initialize;
end;

end.
