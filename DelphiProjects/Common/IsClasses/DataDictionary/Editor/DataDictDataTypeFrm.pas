unit DataDictDataTypeFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, rwCustomDataDictionary, TypInfo, rwEngineTypes;

type
  TDataDictDataTypes = class(TFrame)
    lName: TLabel;
    cbType: TComboBox;
  private
    function GetDataType: TDataDictDataType;
    procedure SetDataType(const Value: TDataDictDataType);
  public
    procedure AfterConstruction; override;
    property DataType: TDataDictDataType read GetDataType write SetDataType;
  end;

implementation

{$R *.dfm}



{ TDataDictDataType }

procedure TDataDictDataTypes.AfterConstruction;
var
  t: TDataDictDataType;
  h: String;
begin
  inherited;
  for t := Low(TDataDictDataType) to High(TDataDictDataType) do
  begin
    h := GetEnumName(TypeInfo(TDataDictDataType), Ord(t));
    Delete(h, 1, 3);
    cbType.AddItem(h, Pointer(Ord(t)));
  end;
  cbType.DropDownCount := Ord(High(TDataDictDataType)) - Ord(Low(TDataDictDataType)) + 1;
end;

function TDataDictDataTypes.GetDataType: TDataDictDataType;
begin
  Result := TDataDictDataType(Integer(cbType.Items.Objects[cbType.ItemIndex]));
end;

procedure TDataDictDataTypes.SetDataType(const Value: TDataDictDataType);
var
  i: Integer;
begin
  i := cbType.Items.IndexOfObject(Pointer(Ord(Value)));
  if i = -1 then
    cbType.ItemIndex := cbType.Items.IndexOfObject(Pointer(Ord(ddtUnknown)))
  else
    cbType.ItemIndex := i;
end;

end.
