unit DataDictDisplayLogicalKeyEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictLogicalKeyEditFrm, StdCtrls, ExtCtrls, rwCustomDataDictionary,
  DataDictAvailableItemsFrm;

type
  TDataDictDisplayLogicalKeyEdit = class(TDataDictLogicalKeyEdit)
  protected
    procedure SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TDataDictDisplayLogicalKeyEdit }

procedure TDataDictDisplayLogicalKeyEdit.SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean);
var
  flds: String;
  i: Integer;
begin
  flds := '';
  for i := 0 to TableEditor.ddTable.Fields.Count - 1 do
    if TableEditor.ddTable.DisplayLogicalKey.FindField(TableEditor.ddTable.Fields[i]) = nil then
    begin
      if flds <> '' then
        flds := flds + Chr(VK_RETURN);
      flds := flds + TableEditor.ddTable.Fields[i].Name;
    end;

  flds := ShowAvailableFields(flds, TableEditor, False, 'Available Table Fields');

  Accept := flds <> '';
  if Accept  then
    TDataDictKeyField(AItem).Field := TableEditor.ddTable.Fields.FieldByName(flds);
end;

end.
