unit DataDictParamEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictNamedItemEditFrm, StdCtrls, DataDictDataTypeFrm,
  rwCustomDataDictionary, ExtCtrls, Grids, ValEdit, ComCtrls, rwEngineTypes;

type
  TDataDictParamEdit = class(TDataDictNamedItemEdit)
    ParamType: TDataDictDataTypes;
    pcParamProp: TPageControl;
    tsParamInfo: TTabSheet;
    tsParamValues: TTabSheet;
    vleValues: TValueListEditor;
    procedure ParamTypecbTypeExit(Sender: TObject);
    procedure ParamTypecbTypeChange(Sender: TObject);
    procedure vleValuesExit(Sender: TObject);
    procedure vleValuesStringsChange(Sender: TObject);
  protected
  public
    procedure AfterConstruction; override;
    procedure OnShowItem; override;
  end;

implementation

{$R *.dfm}

procedure TDataDictParamEdit.OnShowItem;
begin
  inherited;
  if ddItem <> nil then
  begin
    ParamType.DataType := TDataDictParam(ddItem).ParamType;
    vleValues.Strings := TDataDictParam(ddItem).ParamValues;
  end
  else
  begin
    ParamType.DataType := ddtUnknown;
    vleValues.Strings.Text := '';
  end;
end;

procedure TDataDictParamEdit.ParamTypecbTypeExit(Sender: TObject);
begin
  TDataDictParam(ddItem).ParamType := ParamType.DataType;
end;

procedure TDataDictParamEdit.ParamTypecbTypeChange(Sender: TObject);
begin
  OnChange;
end;

procedure TDataDictParamEdit.AfterConstruction;
begin
  inherited;
  pnlContainer.Parent := tsParamInfo;
end;

procedure TDataDictParamEdit.vleValuesExit(Sender: TObject);
begin
  TDataDictParam(ddItem).ParamValues := vleValues.Strings;
end;

procedure TDataDictParamEdit.vleValuesStringsChange(Sender: TObject);
begin
  OnChange;
end;

end.
