object DataDictForeignKeyEdit: TDataDictForeignKeyEdit
  Left = 325
  Top = 157
  BorderStyle = bsToolWindow
  Caption = 'Foreign Key'
  ClientHeight = 187
  ClientWidth = 537
  Color = clBtnFace
  Constraints.MinHeight = 90
  Constraints.MinWidth = 482
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  DesignSize = (
    537
    187)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 11
    Width = 89
    Height = 15
    AutoSize = False
    Caption = 'Reference to'
  end
  object Button1: TButton
    Left = 374
    Top = 162
    Width = 75
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object Button2: TButton
    Left = 458
    Top = 162
    Width = 75
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object cbTables: TComboBox
    Left = 103
    Top = 8
    Width = 422
    Height = 21
    Style = csDropDownList
    DropDownCount = 16
    ItemHeight = 13
    TabOrder = 2
    OnChange = cbTablesChange
  end
  object gbLink: TGroupBox
    Left = 6
    Top = 39
    Width = 527
    Height = 113
    Caption = 'Foreign Key Fields'
    TabOrder = 3
    object pnlTitle: TPanel
      Left = 2
      Top = 15
      Width = 523
      Height = 27
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object lRefTable: TLabel
        Left = 8
        Top = 4
        Width = 232
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Table Primary Key'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
      end
      object lTable: TLabel
        Left = 258
        Top = 4
        Width = 254
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Reference Field'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
      end
    end
  end
end
