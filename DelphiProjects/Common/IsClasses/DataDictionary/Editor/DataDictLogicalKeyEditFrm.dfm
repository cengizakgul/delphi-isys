inherited DataDictLogicalKeyEdit: TDataDictLogicalKeyEdit
  Width = 263
  Height = 144
  inherited pnlContainer: TPanel
    Width = 263
    Height = 144
    DesignSize = (
      263
      144)
    inherited lbCollection: TListBox
      Width = 174
      Height = 144
      Anchors = [akLeft, akTop, akRight, akBottom]
    end
    inherited btnAddItem: TButton
      Left = 182
      Top = 0
      Anchors = [akTop, akRight]
      Caption = 'Add Field'
    end
    inherited btnDeleteItem: TButton
      Left = 182
      Top = 27
      Anchors = [akTop, akRight]
      Caption = 'Delete Field'
    end
  end
end
