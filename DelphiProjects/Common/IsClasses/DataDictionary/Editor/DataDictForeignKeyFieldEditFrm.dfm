object DataDictForeignKeyFieldEdit: TDataDictForeignKeyFieldEdit
  Left = 0
  Top = 0
  Width = 521
  Height = 27
  AutoScroll = False
  TabOrder = 0
  object lFieldName: TLabel
    Left = 8
    Top = 0
    Width = 233
    Height = 20
    AutoSize = False
    Caption = 'lFieldName'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Layout = tlCenter
  end
  object lSeparator: TLabel
    Left = 244
    Top = 0
    Width = 11
    Height = 20
    Alignment = taCenter
    Caption = '='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Layout = tlCenter
  end
  object cbFields: TComboBox
    Left = 258
    Top = 0
    Width = 256
    Height = 21
    Style = csDropDownList
    DropDownCount = 16
    ItemHeight = 13
    TabOrder = 0
  end
end
