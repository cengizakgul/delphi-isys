unit ddTypes;

interface

uses isBaseClasses, isTypes, EvStreamUtils;

const cFieldValueRef = '#Ref';

type
  TddDataType = (ddtUnknown, ddtInteger, ddtFloat, ddtCurrency, ddtDateTime,
                 ddtString, ddtBoolean, ddtArray, ddtBLOB, ddtMemo, ddtGraphic);

  TddTableType = (dttUnknown, dttSystem, dttTemp, dttExternal, dttExternalQuery,
                  dttVirtualQuery, dttComponent);


  IddKey = interface;
  IddFields = interface;
  IddForeignKeys = interface;
  IddParams = interface;

  IddTable = interface
  ['{45C8580A-452A-4D89-A9E6-95D3D8AAC528}']
    function  GetName: string;
    procedure SetName(const AValue: string);
    function  GetDisplayName: string;
    procedure SetDisplayName(const AValue: string);
    function  GetNotes: string;
    procedure SetNotes(const AValue: string);
    function  GetExternalName: String;
    procedure SetExternalName(const AValue: String);
    function  GetGroup: string;
    procedure SetGroup(const AValue: string);
    function  GetQuery: IisStream;
    procedure SetQuery(const AValue: IisStream);
    function  GetTableType: TddTableType;
    procedure SetTableType(const AValue: TddTableType);
    function  GetDisplayKey: IddKey;
    function  GetFields: IddFields;
    function  GetForeignKeys: IddForeignKeys;
    function  GetLogicalKey: IddKey;
    function  GetParams: IddParams;
    function  GetPrimaryKey: IddKey;
    property  Name: String read GetName write SetName;
    property  DisplayName: String read GetDisplayName write SetDisplayName;
    property  Notes: String read GetNotes write SetNotes;
    property  Group: string read GetGroup write SetGroup;
    property  Params: IddParams read GetParams;
    property  Fields: IddFields read GetFields;
    property  ExternalName: String read GetExternalName write SetExternalName;
    property  Query: IisStream read GetQuery write SetQuery;
    property  PrimaryKey: IddKey read GetPrimaryKey;
    property  ForeignKeys: IddForeignKeys read GetForeignKeys;
    property  LogicalKey: IddKey read GetLogicalKey;
    property  DisplayKey: IddKey read GetDisplayKey;
  end;


  IddTables = interface
  ['{D78C5588-E8D7-496D-B191-4B9AA26E4602}']
    function Count: Integer;
    function GetItem(Index: Integer): IddTable;
    function AddTable: IddTable;
    function TableByName(const AName: String): IddTable;
    function TableByExternalName(const AExternalName: String): IddTable;
    property Items[Index: Integer]: IddTable read GetItem; default;
  end;


  IddField = interface
  ['{42561EE7-3621-4D7D-ACDC-B8D1CC628281}']
    function  GetName: string;
    procedure SetName(const AValue: string);
    function  GetDisplayName: string;
    procedure SetDisplayName(const AValue: string);
    function  GetExternalName: String;
    procedure SetExternalName(const AValue: String);
    function  GetNotes: string;
    procedure SetNotes(const AValue: string);
    procedure SetFieldType(const AValue: TddDataType);
    function  GetFieldType: TddDataType;
    function  GetSize: Integer;
    procedure SetSize(const AValue: Integer);
    function  GetFieldValues: IisStringList;
    function  FieldValuesReal: TisIniString;
    function  Table: IddTable;
    property  Name: String read GetName write SetName;
    property  DisplayName: String read GetDisplayName write SetDisplayName;
    property  ExternalName: String read GetExternalName write SetExternalName;
    property  Notes: String read GetNotes write SetNotes;
    property  FieldType: TddDataType read GetFieldType write SetFieldType;
    property  Size: Integer read GetSize write SetSize;
    property  FieldValues: IisStringList read GetFieldValues;
  end;


  IddFields = interface
  ['{6EEC1FA2-863D-4D16-9D2F-5DBAB4002498}']
    function Count: Integer;
    function GetItem(Index: Integer): IddField;
    function FieldByName(const AName: String): IddField;
    function FieldByExternalName(const AExternalName: String): IddField;
    function AddField: IddField;
    property Items[Index: Integer]: IddField read GetItem; default;
  end;


  IddParam = interface
  ['{2C87E9F8-25B1-493D-9470-8DA1D89B3DCC}']
    function  GetName: string;
    procedure SetName(const AValue: string);
    function  GetDisplayName: string;
    procedure SetDisplayName(const AValue: string);
    function  GetNotes: string;
    procedure SetNotes(const AValue: string);
    procedure SetParamType(const AValue: TddDataType);
    function  GetParamType: TddDataType;
    function  GetParamValues: IisStringList;
    function  ParamValuesReal: TisIniString;
    function  Table: IddTable;
    property  Name: String read GetName write SetName;
    property  DisplayName: String read GetDisplayName write SetDisplayName;
    property  Notes: String read GetNotes write SetNotes;
    property  ParamType: TddDataType read GetParamType write SetParamType;
    property  ParamValues: IisStringList read GetParamValues;
  end;


  IddParams = interface
  ['{6579605E-BF31-41AC-8008-D392FC9E06E8}']
    function Count: Integer;
    function GetItem(Index: Integer): IddParam;
    function AddParam: IddParam;
    function ParamByName(const AName: String): IddParam;
    property Items[Index: Integer]: IddParam read GetItem; default;
  end;


  IddKey = interface
  ['{DF0774BB-83FA-4C8B-BB37-FFE9EB5D36D2}']
    function  Table: IddTable;
    function  GetField(Index: Integer): IddField;
    procedure AddKeyField(const AField: IddField);
    function  FieldByName(const AName: String): IddField;
    function  FieldExists(const AField: IddField): Boolean;
    function  Count: Integer;
    property  Fields[Index: Integer]: IddField read GetField; default;
  end;


  IddForeignKeys = interface
  ['{556AD236-DD10-4F32-8BF8-EC992A8BDA08}']
    function  Count: Integer;
    function  GetItem(Index: Integer): IddKey;
    function  AddKey: IddKey;
    function  KeyByTable(const ATable: IddTable; const AStartSearchFrom: Integer = 0): IddKey;
    function  KeyByField(const AField: IddField; const AStartSearchFrom: Integer = 0): IddKey;
    property  Items[Index: Integer]: IddKey read GetItem; default;
  end;


  IddDataDictionary = interface
  ['{6F99364A-A77A-4D91-96E1-54A0F9F7C913}']
    function  GetTables: IddTables;
    function  GetNotes: String;
    procedure SetNotes(const AValue: String);
    property Tables: IddTables read GetTables;
    property Notes: String read GetNotes write SetNotes;
  end;


implementation

end.
 