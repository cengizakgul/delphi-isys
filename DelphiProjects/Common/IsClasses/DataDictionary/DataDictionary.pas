unit DataDictionary;

interface

uses Classes, SysUtils, isBaseClasses, EvStreamUtils, ddTypes, IsTypes, isExceptions;

type
  // Abstract Data Dictionary classes

  TddTablesClass = class of TddTables;
  TddFieldsClass = class of TddFields;
  TddParamsClass = class of TddParams;
  TddForeignKeysClass = class of TddForeignKeys;
  TddTableClass = class of TddTable;
  TddFieldClass = class of TddField;
  TddParamClass = class of TddParam;
  TddKeyClass = class of TddKey;


  TddDataDictionary = class(TisInterfacedObject, IddDataDictionary)
  private
    FNotes: String;
    FTables: IddTables;
  protected
    procedure DoOnConstruction; override;
    function  GetTables: IddTables;
    function  GetNotes: String;
    procedure SetNotes(const AValue: String);

    // Override these if you need to use your own class
    function GetTablesClass: TddTablesClass; virtual;
    function GetFieldsClass: TddFieldsClass; virtual;
    function GetParamsClass:TddParamsClass; virtual;
    function GetTableClass: TddTableClass; virtual;
    function GetFieldClass: TddFieldClass; virtual;
    function GetParamClass: TddParamClass; virtual;
    function GetPrimaryKeyClass: TddKeyClass; virtual;
    function GetLogicalKeyClass: TddKeyClass; virtual;
    function GetDisplayKeyClass: TddKeyClass; virtual;
    function GetForeignKeyClass: TddKeyClass; virtual;
    function GetForeignKeysClass: TddForeignKeysClass; virtual;

{    // Use these procedures for safe deletion
    // It automatically removes broken references
    procedure   DeleteTable(const ATable: TDataDictTable); virtual;
    procedure   DeleteField(const AField: TDataDictField); virtual;
    procedure   DeletePrimaryKeyField(const APrimKeyField: TDataDictPrimKeyField); virtual;

    // Load-Save
    procedure   SaveToFile(const FileName: String);
    procedure   SaveToStream(const Stream: TStream); virtual;
    procedure   LoadFromFile(const FileName: String);
    procedure   LoadFromStream(const Stream: TStream); virtual;
}
  end;


  // Named Item
  TddItem = class(TisNamedObject)
  private
    FNotes: String;
    FDisplayName: String;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure Assign(const ASource: IisInterfacedObject);
    function  GetDisplayName: string;
    procedure SetDisplayName(const AValue: string);
    function  GetNotes: string;
    procedure SetNotes(const AValue: string);
  end;


  // List of Tables
  TddTables = class(TisCollection, IddTables)
  private
    FDataDictionary: TddDataDictionary;
  protected
    function Count: Integer;
    function GetItem(Index: Integer): IddTable;
    function AddTable: IddTable;
    function TableByName(const AName: String): IddTable;
    function TableByExternalName(const AExternalName: String): IddTable;
  public
    class function GetTypeID: String; override;
    constructor Create(const ADataDictionary: TddDataDictionary); reintroduce;
  end;


  // Table
  TddTable = class(TddItem, IddTable)
  private
    FTableType: TddTableType;
    FExternalName: String;
    FGroup: String;
    FQuery: IisStream;
    FFields: IddFields;
    FParams: IddParams;
    FPrimaryKey: IddKey;
    FForeignKeys: IddForeignKeys;
    FLogicalKey: IddKey;
    FDisplayKey: IddKey;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure DoOnConstruction; override;
    function  DataDictionary: TddDataDictionary;

    function  GetExternalName: String;
    procedure SetExternalName(const AValue: String);
    function  GetGroup: String;
    procedure SetGroup(const AValue: String);
    function  GetQuery: IisStream;
    procedure SetQuery(const AValue: IisStream);
    function  GetTableType: TddTableType;
    procedure SetTableType(const AValue: TddTableType);
    function  GetDisplayKey: IddKey;
    function  GetFields: IddFields;
    function  GetForeignKeys: IddForeignKeys;
    function  GetLogicalKey: IddKey;
    function  GetParams: IddParams;
    function  GetPrimaryKey: IddKey;
  public
    class function GetTypeID: String; override;
  end;


  // List of Parameters
  TddParams = class(TisCollection, IddParams)
  private
    FTable:  TddTable;
  protected
    function  Table: TddTable;

    function Count: Integer;
    function GetItem(Index: Integer): IddParam;
    function AddParam: IddParam; virtual;
    function ParamByName(const AName: String): IddParam;
  public
    class function GetTypeID: String; override;
    constructor Create(const ATable: TddTable); reintroduce;
  end;


  // Parameter
  TddParam  = class(TddItem, IddParam)
  private
    FParamType: TddDataType;
    FParamValues: IisStringList;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure DoOnConstruction; override;
    procedure SetParamType(const AValue: TddDataType);
    function  GetParamType: TddDataType;
    function  GetParamValues: IisStringList;
    function  ParamValuesReal: TisIniString;
    function  Table: IddTable;
  public
    class function GetTypeID: String; override;
  end;

  
  // List of Fields
  TddFields = class(TisCollection, IddFields)
  private
    FTable: TddTable;
  protected
    function Table: TddTable;

    function Count: Integer;
    function GetItem(Index: Integer): IddField;
    function AddField: IddField; virtual;
    function FieldByName(const AName: String): IddField;
    function FieldByExternalName(const AExternalName: String): IddField;
  public
    class function GetTypeID: String; override;
    constructor Create(const ATable: TddTable); reintroduce;
  end;


  // Field
  TddField = class(TddItem, IddField)
  private
    FExternalName: String;
    FFieldType: TddDataType;
    FSize: Integer;
    FFieldValues: IisStringList;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetExternalName: String;
    procedure SetExternalName(const AValue: String);
    procedure SetFieldType(const AValue: TddDataType);
    function  GetFieldType: TddDataType;
    function  GetSize: Integer;
    procedure SetSize(const AValue: Integer);
    function  GetFieldValues: IisStringList;
    function  FieldValuesReal: TisIniString;
    function  Table: IddTable;
  public
    class function GetTypeID: String; override;
  end;


  // Key (Primary or Foreign)
  TddKey = class(TisInterfacedObject, IddKey)
  private
    FTable:  TddTable;
    FFields: IisStringList;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure DoOnConstruction; override;
    function  Count: Integer;
    function  Table: IddTable;

    function  GetField(Index: Integer): IddField;
    procedure AddKeyField(const AField: IddField);
    function  FieldByName(const AName: String): IddField;
    function  FieldExists(const AField: IddField): Boolean;
  public
    class function GetTypeID: String; override;
    constructor Create(const ATable: TddTable); reintroduce;
  end;


  // List of foreign keys
  TddForeignKeys = class(TisInterfaceList, IddForeignKeys)
  private
    FTable:  TddTable;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  Table: TddTable;    

    function  GetItem(Index: Integer): IddKey;
    function  AddKey: IddKey;
    function  KeyByTable(const ATable: IddTable; const AStartSearchFrom: Integer = 0): IddKey;
    function  KeyByField(const AField: IddField; const AStartSearchFrom: Integer = 0): IddKey;
  public
    class function GetTypeID: String; override;
    constructor Create(const ATable: TddTable); reintroduce;
  end;

implementation

{ TddItem }

procedure TddItem.Assign(const ASource: IisInterfacedObject);
var
  S: IEvDualStream;
begin
  S := ASource.AsStream;
  SetAsStream(S);
end;

function TddItem.GetDisplayName: string;
begin
  Result := FDisplayName;
end;

function TddItem.GetNotes: string;
begin
  Result := FNotes;
end;

procedure TddItem.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  SetName(AStream.ReadShortString);
  FDisplayName := AStream.ReadShortString;
  FNotes := AStream.ReadString;
end;

procedure TddItem.SetDisplayName(const AValue: string);
begin
  FDisplayName := AValue;
end;

procedure TddItem.SetNotes(const AValue: string);
begin
  FDisplayName := AValue;
end;

procedure TddItem.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(GetName);
  AStream.WriteShortString(FDisplayName);
  AStream.WriteString(FNotes);
end;


{ TddTables }

function TddTables.AddTable: IddTable;
begin
  Result := FDataDictionary.GetTableClass.Create;
  AddChild(Result as IisInterfacedObject);
end;

function TddTables.GetItem(Index: Integer): IddTable;
begin
  Result := GetChild(Index) as IddTable;
end;

function TddTables.TableByName(const AName: String): IddTable;
begin
  Result := FindChildByName(AName) as IddTable;
end;

class function TddTables.GetTypeID: String;
begin
  Result := 'ddTables';
end;

constructor TddTables.Create(const ADataDictionary: TddDataDictionary);
begin
  inherited Create;
  FDataDictionary := ADataDictionary;
end;

function TddTables.Count: Integer;
begin
  Result := ChildCount;
end;

function TddTables.TableByExternalName(const AExternalName: String): IddTable;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if AnsiSameText(AExternalName, GetItem(i).ExternalName) then
    begin
      Result := GetItem(i);
      break;
    end;
end;

{ TddFields }

function TddFields.AddField: IddField;
begin
  Result := Table.DataDictionary.GetFieldClass.Create;
  AddChild(Result as IisInterfacedObject);
end;

function TddFields.Count: Integer;
begin
  Result := ChildCount;
end;

constructor TddFields.Create(const ATable: TddTable);
begin
  inherited Create(nil);
  FTable := ATable;
end;

function TddFields.FieldByExternalName(const AExternalName: String): IddField;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if AnsiSameText(AExternalName, GetItem(i).ExternalName) then
    begin
      Result := GetItem(i);
      break;
    end;
end;

function TddFields.FieldByName(const AName: String): IddField;
begin
  Result := FindChildByName(AName) as IddField;
end;

function TddFields.GetItem(Index: Integer): IddField;
begin
  Result := GetChild(Index) as IddField;
end;

class function TddFields.GetTypeID: String;
begin
  Result := 'ddFields';
end;

function TddFields.Table: TddTable;
begin
  Result := FTable;
end;

{ TddField }

function TddField.FieldValuesReal: TisIniString;
begin
// todo
end;

function TddField.GetFieldType: TddDataType;
begin
  Result := FFieldType;
end;

function TddField.GetFieldValues: IisStringList;
begin
  Result := FFieldValues;
end;

function TddField.GetSize: Integer;
begin
  Result := FSize;
end;

procedure TddField.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FFieldType := TddDataType(AStream.ReadByte);
  FSize := AStream.ReadInteger;
  FFieldValues.Text := AStream.ReadString;
end;

procedure TddField.SetFieldType(const AValue: TddDataType);
begin
  FFieldType := AValue;  
end;

procedure TddField.SetSize(const AValue: Integer);
begin
  FSize := AValue;
end;

function TddField.Table: IddTable;
begin
  Result := TddFields(GetOwner.GetImplementation).FTable;
end;

class function TddField.GetTypeID: String;
begin
  Result := 'ddField';
end;

procedure TddField.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteByte(Ord(FFieldType));
  AStream.WriteInteger(FSize);
  AStream.WriteString(FFieldValues.Text);
end;

function TddField.GetExternalName: String;
begin
  Result := FExternalName;
end;

procedure TddField.SetExternalName(const AValue: String);
begin
  FExternalName := AValue;
end;

{ TddParams }

function TddParams.AddParam: IddParam;
begin
  Result := Table.DataDictionary.GetParamClass.Create;
  AddChild(Result as IisInterfacedObject);
end;

function TddParams.GetItem(Index: Integer): IddParam;
begin
  Result := GetChild(Index) as IddParam;
end;

function TddParams.ParamByName(const AName: String): IddParam;
begin
  Result := FindChildByName(AName) as IddParam;
end;

class function TddParams.GetTypeID: String;
begin
  Result := 'ddParams';
end;

constructor TddParams.Create(const ATable: TddTable);
begin
  inherited Create;
  FTable := ATable;
end;

function TddParams.Count: Integer;
begin
  Result := ChildCount;
end;

function TddParams.Table: TddTable;
begin
  Result := FTable;
end;

{ TddParam }

procedure TddParam.DoOnConstruction;
begin
  inherited;
  FParamValues := TisStringList.Create;
end;

function TddParam.GetParamType: TddDataType;
begin
  Result := FParamType;
end;

function TddParam.GetParamValues: IisStringList;
begin
  Result := FParamValues;
end;

function TddParam.ParamValuesReal: TisIniString;
begin
//todo  Result := GetAllowedValues(ParamValues, DataDictionaryObject);
end;

procedure TddParam.ReadSelfFromStream(const AStream: IEvDualStream;
  const ARevision: TisRevision);
begin
  inherited;
  FParamType := TddDataType(AStream.ReadByte);
  FParamValues.Text := AStream.ReadString;
end;

procedure TddParam.SetParamType(const AValue: TddDataType);
begin
  FParamType := AValue;
end;

class function TddParam.GetTypeID: String;
begin
  Result := 'ddParam';
end;

procedure TddParam.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteByte(Ord(FParamType));
  AStream.WriteString(FParamValues.Text);
end;

function TddParam.Table: IddTable;
begin
  Result := TddFields(GetOwner.GetImplementation).FTable;
end;

{ TddTable }

function TddTable.DataDictionary: TddDataDictionary;
begin
  Result := TddTables(GetOwner.GetImplementation).FDataDictionary;
end;

procedure TddTable.DoOnConstruction;
begin
  inherited;
  FFields := DataDictionary.GetFieldsClass.Create(Self);
  FParams := DataDictionary.GetParamsClass.Create(Self);
  FPrimaryKey := DataDictionary.GetPrimaryKeyClass.Create(Self);
  FForeignKeys := DataDictionary.GetForeignKeysClass.Create(Self);
  FLogicalKey := DataDictionary.GetLogicalKeyClass.Create(Self);
  FDisplayKey := DataDictionary.GetDisplayKeyClass.Create(Self);
end;

function TddTable.GetDisplayKey: IddKey;
begin
  Result := FDisplayKey;
end;

function TddTable.GetExternalName: String;
begin
  Result := FExternalName;
end;

function TddTable.GetFields: IddFields;
begin
  Result := FFields;
end;

function TddTable.GetForeignKeys: IddForeignKeys;
begin
  Result := FForeignKeys;
end;

function TddTable.GetGroup: string;
begin
  Result := FGroup;
end;

function TddTable.GetLogicalKey: IddKey;
begin
  Result := FLogicalKey;
end;

function TddTable.GetParams: IddParams;
begin
  Result := FParams;
end;

function TddTable.GetPrimaryKey: IddKey;
begin
  Result := FPrimaryKey;
end;

function TddTable.GetQuery: IisStream;
begin
  Result := FQuery;
end;

function TddTable.GetTableType: TddTableType;
begin
  Result := FTableType;
end;

class function TddTable.GetTypeID: String;
begin
  Result := 'ddTable';
end;


procedure TddTable.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FTableType := TddTableType(AStream.ReadByte);
  FGroup := AStream.ReadShortString;
  FExternalName := AStream.ReadString;
  FQuery := AStream.ReadStream(nil);
  (FParams as IisInterfacedObject).AsStream := AStream.ReadStream(nil);
  (FFields as IisInterfacedObject).AsStream := AStream.ReadStream(nil);
  (FPrimaryKey as IisInterfacedObject).AsStream := AStream.ReadStream(nil);
  (FForeignKeys as IisInterfacedObject).AsStream := AStream.ReadStream(nil);
  (FLogicalKey as IisInterfacedObject).AsStream := AStream.ReadStream(nil);
  (FDisplayKey as IisInterfacedObject).AsStream := AStream.ReadStream(nil);
end;

procedure TddTable.SetExternalName(const AValue: String);
begin
  FExternalName := AValue;
end;

procedure TddTable.SetGroup(const AValue: string);
begin
  FGroup := AValue;
end;

procedure TddTable.SetQuery(const AValue: IisStream);
begin
  FQuery := AValue;
end;

procedure TddTable.SetTableType(const AValue: TddTableType);
begin
  FTableType := AValue;
end;

procedure TddTable.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;

  AStream.WriteByte(Ord(FTableType));
  AStream.WriteShortString(FGroup);
  AStream.WriteString(FExternalName);
  AStream.WriteStream(FQuery);
  AStream.WriteStream((FParams as IisInterfacedObject).AsStream);
  AStream.WriteStream((FFields as IisInterfacedObject).AsStream);
  AStream.WriteStream((FPrimaryKey as IisInterfacedObject).AsStream);
  AStream.WriteStream((FForeignKeys as IisInterfacedObject).AsStream);
  AStream.WriteStream((FLogicalKey as IisInterfacedObject).AsStream);
  AStream.WriteStream((FDisplayKey as IisInterfacedObject).AsStream);
end;

{ TddKey }

procedure TddKey.AddKeyField(const AField: IddField);
begin
  if (FTable as IisInterfacedObject) <> (AField.Table as IisInterfacedObject) then
    raise ISException.Create('Table is not the same');

  FFields.AddObject(AField.Name, AField);
end;

function TddKey.Count: Integer;
begin
  Result := FFields.Count;
end;

constructor TddKey.Create(const ATable: TddTable);
begin
  inherited Create;
  FTable := ATable;
end;

procedure TddKey.DoOnConstruction;
begin
  inherited;
  FFields := TisStringList.Create;
  FFields.CaseSensitive := False;
end;

function TddKey.FieldByName(const AName: String): IddField;
var
  i: Integer;
begin
  i := FFields.IndexOf(AName);
  if i = -1 then
    Result := nil
  else
    Result := GetField(i);
end;

function TddKey.FieldExists(const AField: IddField): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to FFields.Count - 1 do
    if GetField(i) = (AField as IddField) then
    begin
      Result := True;
      break;
    end;
end;

function TddKey.GetField(Index: Integer): IddField;
begin
  Result := FFields.Objects[Index] as IddField;
  if not Assigned(Result) then
  begin
    // to do
  end;
end;


class function TddKey.GetTypeID: String;
begin
  Result := 'ddKey';
end;

procedure TddKey.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FFields.Text := AStream.ReadString;
end;

function TddKey.Table: IddTable;
begin
  Result := FTable;
end;

procedure TddKey.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FFields.Text);
end;


{ TddForeignKeys }

function TddForeignKeys.AddKey: IddKey;
begin
  Result := TddKey.Create(FTable);
  Add(Result);
end;

function TddForeignKeys.KeyByField(const AField: IddField;
  const AStartSearchFrom: Integer): IddKey;
var
  i: Integer;
begin
  Result := nil;
  for i := AStartSearchFrom to Count - 1 do
  begin
    if (GetItem(i).Table as IddTable) = (AField.Table as IddTable) then
      if GetItem(i).FieldByName(AField.Name) <> nil then
      begin
        Result := GetItem(i);
        break;
      end;
  end;
end;

function TddForeignKeys.KeyByTable(const ATable: IddTable;
  const AStartSearchFrom: Integer): IddKey;
var
  i: Integer;
begin
  Result := nil;
  for i := AStartSearchFrom to Count - 1 do
    if (GetItem(i).Table as IddTable) = (ATable as IddTable) then
    begin
      Result := GetItem(i);
      break;
    end;
end;

function TddForeignKeys.GetItem(Index: Integer): IddKey;
begin
  Result := Get(Index) as IddKey;
end;

constructor TddForeignKeys.Create(const ATable: TddTable);
begin
  inherited Create;
  FTable := ATable;
end;

class function TddForeignKeys.GetTypeID: String;
begin
  Result := 'ddFKeys';
end;

procedure TddForeignKeys.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  i, n: Integer;
  S: IEvDualStream;
  K: IddKey;
begin
  inherited;
  n := AStream.ReadInteger;
  SetCapacity(n);
  for i := 0 to n - 1 do
  begin
    S := AStream.ReadStream(nil);
    K := AddKey;
    (K as IisInterfacedObject).AsStream := S;
  end;
end;


procedure TddForeignKeys.WriteSelfToStream(const AStream: IEvDualStream);
var
  i: Integer;
  S: IEvDualStream;
begin
  inherited;
  AStream.WriteInteger(Count);
  for i := 0 to Count - 1 do
  begin
    S := (GetItem(i) as IisInterfacedObject).AsStream;
    AStream.WriteStream(S);
  end;
end;


function TddForeignKeys.Table: TddTable;
begin
  Result := FTable;
end;

{ TddDataDictionary }

procedure TddDataDictionary.DoOnConstruction;
begin
  inherited;
  FTables := GetTablesClass.Create(Self);
end;

function TddDataDictionary.GetDisplayKeyClass: TddKeyClass;
begin
  Result := TddKey;
end;

function TddDataDictionary.GetFieldClass: TddFieldClass;
begin
  Result := TddField;
end;

function TddDataDictionary.GetFieldsClass: TddFieldsClass;
begin
  Result := TddFields;
end;

function TddDataDictionary.GetForeignKeyClass: TddKeyClass;
begin
  Result := TddKey;
end;

function TddDataDictionary.GetForeignKeysClass: TddForeignKeysClass;
begin
  Result := TddForeignKeys;
end;

function TddDataDictionary.GetLogicalKeyClass: TddKeyClass;
begin
  Result := TddKey;
end;

function TddDataDictionary.GetNotes: String;
begin
  Result := FNotes;
end;

function TddDataDictionary.GetParamClass: TddParamClass;
begin
  Result := TddParam;
end;

function TddDataDictionary.GetParamsClass: TddParamsClass;
begin
  Result := TddParams;
end;

function TddDataDictionary.GetPrimaryKeyClass: TddKeyClass;
begin
  Result := TddKey;
end;

function TddDataDictionary.GetTableClass: TddTableClass;
begin
  Result := TddTable;
end;

function TddDataDictionary.GetTables: IddTables;
begin
  Result := FTables;
end;

function TddDataDictionary.GetTablesClass: TddTablesClass;
begin
  Result := TddTables;
end;

procedure TddDataDictionary.SetNotes(const AValue: String);
begin
  FNotes := AValue;
end;

end.
