unit isCryptoApiWrapper;

interface

uses
  Windows, wCrypt2, EvStreamUtils, SysUtils, SEncryptionRoutines, isBaseClasses,
  isExceptions;

type
  IisCryptoApiWrapper = interface
  ['{C83FBC88-4A6C-4094-BDDB-B62A14B7E07D}']
    function  GetLocalPublicKey: IevDualStream;
    procedure SetSessionKey(ASessionKey: IevDualStream);
    function  Encrypt(AData: IevDualStream): IevDualStream;
    function  Decrypt(AData: IevDualStream): IevDualStream;
    function  GetSessionKey(ARemotePublicKey: IevDualStream): IevDualStream;
  end;

  TisCryptoApiWrapperBlowfish = class(TisInterfacedObject, IisCryptoApiWrapper)
  private
    FProvider: HCRYPTPROV;
    FPrivateKey: HCRYPTKEY;
    FSessionKey: TGUID;
  protected
    procedure DoOnConstruction; override;
    function  GetLocalPublicKey: IevDualStream;
    procedure SetSessionKey(ASessionKey: IevDualStream);
    function  Encrypt(AData: IevDualStream): IevDualStream;
    function  Decrypt(AData: IevDualStream): IevDualStream;
    function  GetSessionKey(ARemotePublicKey: IevDualStream): IevDualStream;
  public
    destructor Destroy; override;
  end;

  TisCryptoApiWrapper = class
  private
    FProvider: HCRYPTPROV;
  public
    constructor Create;
    destructor Destroy; override;
    function  GenKeyPair(const APublicKey: IevDualStream; const ALength: Integer = 1024): HCRYPTKEY;
    function  GenSessionKey(const APublicKey: HCRYPTKEY; ASessionKey: IevDualStream): HCRYPTKEY;
    function  LoadKeyFromStream(const AStream: IevDualStream): HCRYPTKEY;
    function  LoadKeyFromFile(const AFileName: String): HCRYPTKEY;
    function  LoadSessionKey(const APrivateKey: HCRYPTKEY; ASessionKey: IevDualStream): HCRYPTKEY;
    procedure EncryptWithPublicKey(const AStream: IevDualStream; const AKey: HCRYPTKEY);
    procedure EncryptWithSessionKey(const ASrcStream, ADstStream: IevDualStream; const AKey: HCRYPTKEY);
    procedure Decrypt(const AStream: IevDualStream; const AKey: HCRYPTKEY);
    procedure DestroyKey(const AKey: HCRYPTKEY);
  end;

implementation

{ TisCryptoApiWrapper }

procedure Chk(const AResult: Boolean);
var
  err: Int64;
begin
  if not AResult then
  begin
    err := GetLastError;
    case err of
    ERROR_INVALID_HANDLE: raise EisException.Create('Invalid handle');
    ERROR_INVALID_PARAMETER: raise EisException.Create('Invalid parameter');
    NTE_BAD_ALGID: raise EisException.Create('Bad ALGID');
    NTE_BAD_DATA: raise EisException.Create('Bad data');
    NTE_BAD_FLAGS: raise EisException.Create('Bad flags');
    NTE_BAD_HASH: raise EisException.Create('Bad hash');
    NTE_BAD_KEY: raise EisException.Create('Bad key');
    NTE_BAD_LEN: raise EisException.Create('Bad length');
    NTE_BAD_UID: raise EisException.Create('Bad UID');
    NTE_DOUBLE_ENCRYPT: raise EisException.Create('Double encrypt');
    NTE_NO_KEY: raise EisException.Create('No key');
    NTE_FAIL: raise EisException.Create('Fail');
    else raise EisException.Create('Unknown error');
    end;
  end;
end;

constructor TisCryptoApiWrapper.Create;
begin
  inherited;
  FProvider := 0;
  CryptAcquireContext(@FProvider, nil, nil, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);
end;

destructor TisCryptoApiWrapper.Destroy;
begin
  CryptReleaseContext(FProvider, 0);
  inherited;
end;

function TisCryptoApiWrapper.GenKeyPair(const APublicKey: IevDualStream; const ALength: Integer = 1024): HCRYPTKEY;
var
  Param, Flag: Integer;
  PublicKey: String;
  Key: HCRYPTKEY;
begin
  Param := ALength;
  Flag := Param shl 16;
  Chk(CryptGenKey(FProvider, AT_KEYEXCHANGE, Flag or CRYPT_EXPORTABLE, @Key));
  Chk(CryptExportKey(Key, 0, PUBLICKEYBLOB, 0, nil, @Param));
  SetLength(PublicKey, Param);
  Chk(CryptExportKey(Key, 0, PUBLICKEYBLOB, 0, @PublicKey[1], @Param));
  APublicKey.WriteBuffer(PublicKey[1], Length(PublicKey));
  APublicKey.Position := 0;
  Result := Key;
end;

procedure TisCryptoApiWrapper.EncryptWithPublicKey(const AStream: IevDualStream; const AKey: HCRYPTKEY);
var
  buf: array[0..1023] of Byte;
  Tmp: IevDualStream;
  Param, BlockSize: Integer;
  Final: Boolean;
begin
  BlockSize := 0;
  Param := SizeOf(BlockSize);
  Chk(CryptGetKeyParam(AKey, KP_BLOCKLEN, @BlockSize, @Param, 0));
  BlockSize := BlockSize div 8;
  Tmp := TevDualStreamHolder.Create;
  while AStream.Position < AStream.Size do
  begin
    Param := AStream.Read(buf, BlockSize - 11);
    Final := not (AStream.Position < AStream.Size);
    Chk(CryptEncrypt(AKey, 0, Final, 0, @buf, @Param, BlockSize));
    Tmp.WriteBuffer(buf, Param);
  end;
  Tmp.Position := 0;
  AStream.Clear;
  AStream.CopyFrom(Tmp, Tmp.Size);
end;

procedure TisCryptoApiWrapper.DestroyKey(const AKey: HCRYPTKEY);
begin
  if AKey <> 0 then
    Chk(CryptDestroyKey(AKey));
end;

function TisCryptoApiWrapper.LoadKeyFromStream(const AStream: IevDualStream): HCRYPTKEY;
var
  KeyStr: String;
  Key: HCRYPTKEY;  
begin
  AStream.Position := 0;
  SetLength(KeyStr, AStream.Size);
  AStream.Read(KeyStr[1], AStream.Size);
  Chk(CryptImportKey(FProvider, @KeyStr[1], AStream.Size, 0, 0, @Key));
  Result := Key;
end;

function TisCryptoApiWrapper.LoadKeyFromFile(const AFileName: String): HCRYPTKEY;
var
  m: IevDualStream;
begin
  m := TevDualStreamHolder.Create;
  m.LoadFromFile(AFileName);
  Result := LoadKeyFromStream(m);
end;

procedure TisCryptoApiWrapper.Decrypt(const AStream: IevDualStream; const AKey: HCRYPTKEY);
var
  buf: array[0..1023] of Byte;
  Tmp: IevDualStream;
  Param: Integer;
  Final: Boolean;
  BlockSize: Integer;
begin
  BlockSize := 0;
  Param := SizeOf(BlockSize);
  Chk(CryptGetKeyParam(AKey, KP_BLOCKLEN, @BlockSize, @Param, 0));
  BlockSize := BlockSize div 8;
  Param := BlockSize;
  if Param < 10 then
  while (BlockSize + Param) <= 512 do
    BlockSize := BlockSize + Param;

  Tmp := TevDualStreamHolder.Create;
  AStream.Position := 0;
  while AStream.Position < AStream.Size do
  begin
    Param := AStream.Read(buf, BlockSize);
    Final := not (AStream.Position < AStream.Size);
    Chk(CryptDecrypt(AKey, 0, Final, 0, @buf, @Param));
    Tmp.WriteBuffer(buf, Param);
  end;
  Tmp.Position := 0;
  AStream.Clear;
  AStream.CopyFrom(Tmp, Tmp.Size);
end;

procedure TisCryptoApiWrapper.EncryptWithSessionKey(const ASrcStream, ADstStream: IevDualStream; const AKey: HCRYPTKEY);
var
  buf: array[0..2047] of Byte;
  Param: Integer;
  Final: Boolean;
  BlockSize: Integer;
begin
  BlockSize := 0;
  Param := SizeOf(BlockSize);
  Chk(CryptGetKeyParam(AKey, KP_BLOCKLEN, @BlockSize, @Param, 0));
  BlockSize := BlockSize div 8;
  Param := BlockSize;
  if Param < 10 then
  while (BlockSize + Param) <= (SizeOf(buf) div 2) do
    BlockSize := BlockSize + Param;
  ASrcStream.Position := 0;
  while ASrcStream.Position < ASrcStream.Size do
  begin
    Param := ASrcStream.Read(buf, BlockSize);
    Final := not (ASrcStream.Position < ASrcStream.Size);
    Chk(CryptEncrypt(AKey, 0, Final, 0, @buf, @Param, BlockSize * 2));
    ADstStream.WriteBuffer(buf, Param);
  end;
  ADstStream.Position := 0;
  ASrcStream.Position := 0;
end;

function TisCryptoApiWrapper.GenSessionKey(const APublicKey: HCRYPTKEY; ASessionKey: IevDualStream): HCRYPTKEY;
var
  Param: Integer;
  KeyStr: String;
  Key: HCRYPTKEY;
begin
  ASessionKey.Clear;
  Chk(CryptGenKey(FProvider, CALG_3DES, CRYPT_EXPORTABLE, @Key));
  Chk(CryptExportKey(Key, APublicKey, SIMPLEBLOB, 0, nil, @Param));
  SetLength(KeyStr, Param);
  Chk(CryptExportKey(Key, APublicKey, SIMPLEBLOB, 0, @KeyStr[1], @Param));
  ASessionKey.WriteBuffer(KeyStr[1], Length(KeyStr));
  ASessionKey.Position := 0;
  Result := Key;
end;

function  TisCryptoApiWrapper.LoadSessionKey(const APrivateKey: HCRYPTKEY; ASessionKey: IevDualStream): HCRYPTKEY;
var
  KeyStr: String;
  Key: HCRYPTKEY;
begin
  ASessionKey.Position := 0;
  SetLength(KeyStr, ASessionKey.Size);
  ASessionKey.Read(KeyStr[1], ASessionKey.Size);
  Chk(CryptImportKey(FProvider, @KeyStr[1], ASessionKey.Size, APrivateKey, 0, @Key));
  Result := Key;
end;

{ TisCryptoApiWrapperBlowfish }

function TisCryptoApiWrapperBlowfish.Encrypt(
  AData: IevDualStream): IevDualStream;
var
  OutStream: IevDualStream;
  Cipher: String;
begin
  SetLength(Cipher, SizeOf(FSessionKey));
  Move(FSessionKey, Cipher[1], SizeOf(FSessionKey));
  OutStream := TevDualStreamHolder.Create;
  SEncryptionRoutines.EncryptMemoryStream(AData.RealStream, OutStream.RealStream, Cipher);
  Result := OutStream;
end;

function TisCryptoApiWrapperBlowfish.Decrypt(
  AData: IevDualStream): IevDualStream;
var
  OutStream: IevDualStream;
  Cipher: String;
begin
  SetLength(Cipher, SizeOf(FSessionKey));
  Move(FSessionKey, Cipher[1], SizeOf(FSessionKey));
  OutStream := TevDualStreamHolder.Create;
  SEncryptionRoutines.DecryptMemoryStream(AData.RealStream, OutStream.RealStream, Cipher);
  Result := OutStream;
end;

function TisCryptoApiWrapperBlowfish.GetLocalPublicKey: IevDualStream;
var
  Param, Flag: Integer;
  KeyStr: String;
  Stream: IevDualStream;
begin
  Param := 2048;
  Flag := Param shl 16;
  Chk(CryptGenKey(FProvider, AT_KEYEXCHANGE, Flag or CRYPT_EXPORTABLE, @FPrivateKey));
  Chk(CryptExportKey(FPrivateKey, 0, PUBLICKEYBLOB, 0, nil, @Param));
  SetLength(KeyStr, Param);
  Chk(CryptExportKey(FPrivateKey, 0, PUBLICKEYBLOB, 0, @KeyStr[1], @Param));
  Stream := TevDualStreamHolder.Create;
  Stream.WriteBuffer(KeyStr[1], Length(KeyStr));
  Stream.Position := 0;
  Result := Stream;
end;

procedure TisCryptoApiWrapperBlowfish.SetSessionKey(
  ASessionKey: IevDualStream);
var
  BlockSize: Integer;
  Param: Integer;
  Tmp: IevDualStream;
  buf: array[0..2047] of Byte;
  Final: Boolean;
begin
  BlockSize := 0;
  Param := SizeOf(BlockSize);
  Chk(CryptGetKeyParam(FPrivateKey, KP_BLOCKLEN, @BlockSize, @Param, 0));
  BlockSize := BlockSize div 8;
  Param := BlockSize;
  if Param < 10 then
  while (BlockSize + Param) <= 512 do
    BlockSize := BlockSize + Param;

  Tmp := TevDualStreamHolder.Create;
  ASessionKey.Position := 0;
  while ASessionKey.Position < ASessionKey.Size do
  begin
    Param := ASessionKey.Read(buf, BlockSize);
    Final := not (ASessionKey.Position < ASessionKey.Size);
    Chk(CryptDecrypt(FPrivateKey, 0, Final, 0, @buf, @Param));
    Tmp.WriteBuffer(buf, Param);
  end;
  Tmp.Position := 0;
  Tmp.ReadBuffer(FSessionKey, SizeOf(FSessionKey));
end;

destructor TisCryptoApiWrapperBlowfish.Destroy;
begin
  CryptReleaseContext(FProvider, 0);
  inherited;
end;

procedure TisCryptoApiWrapperBlowfish.DoOnConstruction;
begin
  inherited;
  FProvider := 0;
  CryptAcquireContext(@FProvider, nil, nil, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);
end;

function TisCryptoApiWrapperBlowfish.GetSessionKey(
  ARemotePublicKey: IevDualStream): IevDualStream;
var
  buf: array[0..2047] of Byte;
  KeyStr: String;
  PublicKey: HCRYPTKEY;
  Stream, Tmp: IevDualStream;
  Param, BlockSize: Integer;
  Final: Boolean;
begin
  ARemotePublicKey.Position := 0;
  SetLength(KeyStr, ARemotePublicKey.Size);
  ARemotePublicKey.Read(KeyStr[1], ARemotePublicKey.Size);
  Chk(CryptImportKey(FProvider, @KeyStr[1], ARemotePublicKey.Size, 0, 0, @PublicKey));

  Stream := TevDualStreamHolder.Create;
  CreateGUID(FSessionKey);
  Stream.WriteBuffer(FSessionKey, SizeOf(FSessionKey));
  Stream.Position := 0;

  BlockSize := 0;
  Param := SizeOf(BlockSize);
  Chk(CryptGetKeyParam(PublicKey, KP_BLOCKLEN, @BlockSize, @Param, 0));
  BlockSize := BlockSize div 8;
  
  Tmp := TevDualStreamHolder.Create;
  while Stream.Position < Stream.Size do
  begin
    Param := Stream.Read(buf, BlockSize - 11);
    Final := not (Stream.Position < Stream.Size);
    Chk(CryptEncrypt(PublicKey, 0, Final, 0, @buf, @Param, BlockSize));
    Tmp.WriteBuffer(buf, Param);
  end;
  Tmp.Position := 0;
  Stream.Clear;
  Stream.CopyFrom(Tmp, Tmp.Size);
  Stream.Position := 0;
  Result := Stream;
end;

end.
