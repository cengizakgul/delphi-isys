unit isBasicStackTracer;

interface

uses SysUtils, SyncObjs, Windows, madExcept, madLinkDisAsm, madListHardware, madListProcesses, madListModules;

procedure RegisterBasicExceptionHandler;
procedure UnRegisterBasicExceptionHandler;

implementation

procedure AllExceptionBasicHandler(const ExceptIntf: IMEException; var Handled: boolean);

  function MadExceptGetStack(E: Exception = nil; ExceptAddr: Pointer = nil): string;
  var
    exc : IMEException;
  begin
    exc := NewException(etNormal, E, ExceptAddr);
    exc.ShowPleaseWaitBox := False;
    exc.ListThreads := False;
    exc.ShowCpuRegisters := False;
    exc.ShowStackDump := False;
    exc.ShowDisAsm := False;
    exc.PluginEnabled['processes'] := False;
    exc.PluginEnabled['modules'] := False;
    exc.PluginEnabled['hardware'] := False;
    result := exc.BugReport;
  end;

begin
  Handled := True;
  MessageBox(0, PAnsiChar(MadExceptGetStack), PansiChar(ExtractFileName(ParamStr(0))), MB_OK or MB_ICONERROR);
end;

procedure MadExceptSetThreadName(threadID: dword; threadName: string);
begin
  MadExcept.NameThread(threadID, threadName);
end;

function MadExceptGetThreadName(threadID: dword): string;
begin
  Result := MadExcept.GetThreadName(threadID);
end;

procedure RegisterBasicExceptionHandler;
begin
  RegisterExceptionHandler(AllExceptionBasicHandler, stDontSync, epMainPhase);
end;

procedure UnRegisterBasicExceptionHandler;
begin
  UnRegisterExceptionHandler(AllExceptionBasicHandler);
end;

initialization
  RegisterBasicExceptionHandler;

end.
