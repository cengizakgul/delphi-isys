unit isLogFileDataSet;

interface

uses
  SysUtils, Classes, DB, isLogFile;

type

  PBookmarkData = ^Pointer;

type
  PRecInfo = ^TRecInfo;
  TRecInfo = packed record
    RecordNumber: Integer;
    BookmarkFlag: TBookmarkFlag;
  end;

  TisLogDataSet = class(TDataSet)
  private
    FRecNo: Integer;
    FLogFile: ILogFile;
    FFileName: String;
    FIdx: TList;
  protected
    function GetBookmarkFlag(Buffer: PChar): TBookmarkFlag; override;
    procedure InternalGotoBookmark(Bookmark: Pointer); override;
    procedure SetBookmarkFlag(Buffer: PChar; Value: TBookmarkFlag); override;
    procedure GetBookmarkData(Buffer: PChar; Data: Pointer); override;
    procedure SetBookmarkData(Buffer: PChar; Data: Pointer); override;
    function AllocRecordBuffer: PChar; override;
    procedure FreeRecordBuffer(var Buffer: PChar); override;
    function GetRecord(Buffer: PChar; GetMode: TGetMode; DoCheck: Boolean): TGetResult; override;
    function GetRecordSize: Word; override;
    procedure InternalClose; override;
    procedure InternalFirst; override;
    procedure InternalHandleException; override;
    procedure InternalInitFieldDefs; override;
    procedure InternalInitRecord(Buffer: PChar); override;
    procedure InternalLast; override;
    procedure InternalOpen; override;
    procedure InternalSetToRecord(Buffer: PChar); override;
    function IsCursorOpen: Boolean; override;
    procedure SetFieldData(Field: TField; Buffer: Pointer); override;
  protected
    function GetRecordCount: Integer; override;
    function GetRecNo: Integer; override;
    procedure SetRecNo(Value: Integer); override;
  public
    function CompareBookmarks(Bookmark1, Bookmark2: TBookmark): Integer; override;  
    constructor Create(AOwner: TComponent); override;
    function GetFieldData(Field: TField; Buffer: Pointer): Boolean; override;
    property LogFile: ILogFile read FLogFile;
    procedure ApplyFilter(AUnknown, AInformation, AWarning, AError, AFatalError: Boolean; const AText: String);
  published
    property Active;
    property FileName: String read FFileName write FFileName;
  end;

procedure Register;

implementation

procedure TisLogDataSet.InternalOpen;
var
  i, cnt: Integer;
begin
  FRecNo := -1;
  if Assigned(FIdx)
    then FIdx.Free;

  FLogFile := nil;
  FLogFile := TLogFile.Create(FFileName);

  FIdx := TList.Create;
  cnt := FLogFile.Count;
  FIdx.Capacity := cnt;

  while FIdx.Count < cnt do
    FIdx.Add(Pointer(cnt-FIdx.Count-1));

  FieldDefs.Clear;

  FieldDefs.Add('EventType', ftString, 30, True);
  FieldDefs.Add('TimeStamp', ftString, 30, True);
  FieldDefs.Add('EventClass', ftString, 100, True);
  FieldDefs.Add('Text', ftString, 100, True);
  FieldDefs.Add('Details', ftString, 32000, True);
  FieldDefs.Add('PhisicalIndex', ftString, 10, True);

  InternalInitFieldDefs;
  CreateFields;
  BindFields(True);
  for i := 0 to Fields.Count - 1 do Fields[i].ReadOnly := True;

end;

procedure TisLogDataSet.InternalClose;
begin
  FreeAndNil(FIdx);
  DestroyFields;
  FRecNo := -1;
end;

function TisLogDataSet.IsCursorOpen: Boolean;
begin
  Result := Assigned(FIdx);
end;

procedure TisLogDataSet.InternalInitFieldDefs;
begin
end;

procedure TisLogDataSet.InternalHandleException;
begin
//  Application.HandleException(Self);
end;

procedure TisLogDataSet.InternalSetToRecord(Buffer: PChar);
begin
  InternalGotoBookmark(Buffer);
end;

function TisLogDataSet.GetRecordSize: Word;
begin
  Result := SizeOf(TRecInfo);
end;

function TisLogDataSet.AllocRecordBuffer: PChar;
var
  P: Pointer;
begin
  GetMem(P, GetRecordSize);
  Result := P;
end;

procedure TisLogDataSet.InternalInitRecord(Buffer: PChar);
begin
  PRecInfo(Buffer).RecordNumber := 0;
  PRecInfo(Buffer).BookmarkFlag := bfCurrent;
end;

procedure TisLogDataSet.SetFieldData(Field: TField; Buffer: Pointer);
begin
end;

procedure TisLogDataSet.InternalFirst;
begin
  FRecNo := -1;
end;

procedure TisLogDataSet.InternalLast;
begin
  FRecNo := FIdx.Count;
end;

function TisLogDataSet.GetRecordCount: Longint;
begin
  Result := FIdx.Count;
end;

function TisLogDataSet.GetRecNo: Longint;
begin
  UpdateCursorPos;
  if (FRecNo = -1) and (RecordCount > 0) then
    Result := 0 else
    Result := FRecNo;
end;

procedure TisLogDataSet.SetRecNo(Value: Integer);
begin
  if (Value >= 0) and (Value < FIdx.Count) then
  begin
    FRecNo := Value;
    Resync([]);
  end;
end;

function TisLogDataSet.GetBookmarkFlag(Buffer: PChar): TBookmarkFlag;
begin
  Result := PRecInfo(Buffer).BookmarkFlag;
end;

procedure TisLogDataSet.FreeRecordBuffer(var Buffer: PChar);
begin
  FreeMem(Buffer, SizeOf(TRecInfo));
end;

function TisLogDataSet.GetRecord(Buffer: PChar; GetMode: TGetMode;
  DoCheck: Boolean): TGetResult;
var
  P: PRecInfo;
begin
  if FIdx.Count < 1 then
    Result := grEOF else
  begin
    Result := grOK;
    case GetMode of
      gmNext:
        if FRecNo >= RecordCount - 1  then
          Result := grEOF else
          Inc(FRecNo);
      gmPrior:
        if FRecNo <= 0 then
          Result := grBOF else
          Dec(FRecNo);
      gmCurrent:
        if (FRecNo < 0) then
         Result := grBOF
        else if (FRecNo >= RecordCount) then
          Result := grEOF;
    end;
    if Result = grOK then
    begin
      P := PRecInfo(Buffer);
      P.BookmarkFlag := bfCurrent;
      P.RecordNumber := FRecNo;
    end else if (Result = grError) and DoCheck then DatabaseError('No Records');
  end;
end;

function TisLogDataSet.GetFieldData(Field: TField; Buffer: Pointer): Boolean;
var
  No: Integer;
  FieldValue: String;

  function EventTypeName(AType: TLogEventType): String;
  begin
    case AType of
    etUnknown     : Result := 'Unknown';
    etInformation : Result := 'Information';
    etWarning     : Result := 'Warning';
    etError       : Result := 'Error';
    etFatalError  : Result := 'FatalError';
    else            Result := 'Unknown';
    end;
  end;

begin
  Result := False;
  if State = dsInsert then
    exit;
  Result := True;
  No := PRecInfo(ActiveBuffer).RecordNumber;
  if (No >= 0) and (FIdx.Count > 0) then
  begin
    case Field.Index of
    0: FieldValue := EventTypeName(FLogFile.Items[Integer(FIdx[No])].EventType);
    1: FieldValue := DateTimeToStr(FLogFile.Items[Integer(FIdx[No])].TimeStamp);
    2: FieldValue := FLogFile.Items[Integer(FIdx[No])].EventClass;
    3: FieldValue := FLogFile.Items[Integer(FIdx[No])].Text;
    4: FieldValue := FLogFile.Items[Integer(FIdx[No])].Details;
    5: FieldValue := IntToStr(Integer(FIdx[No]));
    end;
    FieldValue := copy(FieldValue,1, Field.DataSize-1);
  end
  else
    FieldValue := '';
    
  StrPCopy(Buffer, FieldValue);

end;

procedure TisLogDataSet.SetBookmarkFlag(Buffer: PChar; Value: TBookmarkFlag);
begin
  PRecInfo(Buffer).BookmarkFlag := Value;
end;

procedure TisLogDataSet.InternalGotoBookmark(Bookmark: Pointer);
begin
  FRecNo := PRecInfo(Bookmark)^.RecordNumber;
end;

procedure Register;
begin
  RegisterComponents('ISClasses', [TisLogDataSet]);
end;

procedure TisLogDataSet.ApplyFilter(AUnknown, AInformation, AWarning, AError,
  AFatalError: Boolean; const AText: String);
var
  i: Integer;
  RecordFiltered: Boolean;
begin
  FIdx.Clear;
  for i := 0 to LogFile.Count - 1 do
  begin
    RecordFiltered := False;

    RecordFiltered := RecordFiltered or
                    (not AUnknown and (LogFile.Items[i].EventType = etUnknown));

    RecordFiltered := RecordFiltered or
                    (not AInformation and (LogFile.Items[i].EventType = etInformation));

    RecordFiltered := RecordFiltered or
                    (not AWarning and (LogFile.Items[i].EventType = etWarning));

    RecordFiltered := RecordFiltered or
                    (not AError and (LogFile.Items[i].EventType = etError));

    RecordFiltered := RecordFiltered or
                    (not AFatalError and (LogFile.Items[i].EventType = etFatalError));

    RecordFiltered := RecordFiltered or
                    ((Length(AText) <> 0) and
                    (pos(UpperCase(AText), UpperCase(LogFile.Items[i].Text)) = 0)
                    and (pos(UpperCase(AText), UpperCase(LogFile.Items[i].Details)) = 0));

    if not RecordFiltered then
      FIdx.Insert(0, Pointer(i));
//      FIdx.Add(Pointer(i));

  end;
  FRecNo := -1;
  Refresh;
end;

constructor TisLogDataSet.Create(AOwner: TComponent);
begin
  inherited;
  BookmarkSize := 4;
end;

procedure TisLogDataSet.GetBookmarkData(Buffer: PChar; Data: Pointer);
begin
  Move(Buffer^, Data^, SizeOf(TRecInfo));
end;

procedure TisLogDataSet.SetBookmarkData(Buffer: PChar; Data: Pointer);
begin
  Move(Data^, Buffer^, SizeOf(TRecInfo));
end;

function TisLogDataSet.CompareBookmarks(Bookmark1,
  Bookmark2: TBookmark): Integer;
begin
  if PRecInfo(Bookmark1).RecordNumber >
     PRecInfo(Bookmark2).RecordNumber then
    Result := 1
  else if PRecInfo(Bookmark1).RecordNumber <
     PRecInfo(Bookmark2).RecordNumber then
    Result := -1
  else Result := 0;  
end;

end.
