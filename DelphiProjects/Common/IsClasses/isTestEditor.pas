unit isTestEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ExtCtrls, ComCtrls, isTestUtils, isTestUtilsScript, ActnList,
  ToolWin, StdCtrls, ImgList, StdActns, Menus;

type
  TisTestEditorFrame = class(TFrame, IIsTEScriptEventSink)
    ActionList1: TActionList;
    FormStatus: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ImageList1: TImageList;
    Play: TAction;
    Stop: TAction;
    Pause: TAction;
    Rec: TAction;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    FileOpen1: TFileOpen;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    EditDelete1: TEditDelete;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    FileSave: TAction;
    FileSaveAs1: TFileSaveAs;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    FileExit1: TAction;
    Exit1: TMenuItem;
    Open1: TMenuItem;
    SaveAs1: TMenuItem;
    SaveAs2: TMenuItem;
    N1: TMenuItem;
    FileNew: TAction;
    ToolButton11: TToolButton;
    New1: TMenuItem;
    Edit1: TMenuItem;
    Delete1: TMenuItem;
    Action1: TMenuItem;
    Pause1: TMenuItem;
    Play1: TMenuItem;
    Stop1: TMenuItem;
    Rec1: TMenuItem;
    PopupMenu1: TPopupMenu;
    Deletefromtheselectedcommandtotheend1: TMenuItem;
    PageControl1: TPageControl;
    tbNotes: TTabSheet;
    tbCommands: TTabSheet;
    pnlView: TPanel;
    pnlDetails: TPanel;
    lvScript: TListView;
    mNotes: TMemo;
    procedure FormStatusExecute(Sender: TObject);
    procedure FormStatusUpdate(Sender: TObject);
    procedure FileOpen1Accept(Sender: TObject);
    procedure FileSaveAs1Accept(Sender: TObject);
    procedure FileNewExecute(Sender: TObject);
    procedure FileSaveExecute(Sender: TObject);
    procedure FileSaveUpdate(Sender: TObject);
    procedure EditDelete1Update(Sender: TObject);
    procedure EditDelete1Execute(Sender: TObject);
    procedure FileExit1Execute(Sender: TObject);
    procedure PlayUpdate(Sender: TObject);
    procedure PlayExecute(Sender: TObject);
    procedure RecUpdate(Sender: TObject);
    procedure RecExecute(Sender: TObject);
    procedure PauseUpdate(Sender: TObject);
    procedure PauseExecute(Sender: TObject);
    procedure StopUpdate(Sender: TObject);
    procedure StopExecute(Sender: TObject);
    procedure mNotesChange(Sender: TObject);
  private
    FScript: IisTEScript2;
    procedure CommandDeleted( idx: integer );
    procedure CommandAdded( idx: integer );
    procedure AddCommand(idx: integer);
  protected
    procedure Loaded; override;
  public
    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;
    procedure OpenScript( aScript: IisTEScript2 );
    function CloseQuery: boolean;
    property Script: IisTEScript2 read FScript;
  end;

implementation

uses isTestQueueManagerHolderFrm;

{$R *.dfm}

{ TisTestEditorFrame }

procedure TisTestEditorFrame.AddCommand(idx: integer);
var
  c: IisTEScriptCommand;
  mc: IisTEScriptMouseCommand;
  kc: IisTEScriptKeyCommand;
  namestr: string;
  paramstr: string;
  targetstr: string;
begin
  with lvScript.Items.Add do
  begin
    Data := pointer( idx );
    c := FScript.Command[idx];
    targetstr := '';
    case c.CommandType of
      teSCMouse:
        begin
          mc := c as IisTEScriptMouseCommand;
          namestr := NameFromMsg(mc.Msg);
          paramstr := format('(%d, %d)',[mc.Point.X, mc.Point.Y]);
          if not mc.Target.ThisIsNull then
            targetstr := mc.Target.Annotation;
        end;
      teSCKey:
        begin
          kc := c as IisTEScriptKeyCommand;
          namestr := NameFromMsg(kc.Msg);
          paramstr := format('VKey: %d Scan: %d',[kc.VKey, kc.ScanCode]);
        end;
      teSCPopupMenu:
        begin
          namestr := 'Popup menu';
          paramstr := SetToStr( (c as IisTEScriptPopupMenuCommand).MenuPath, ' | ' );
        end
    end;
    Caption := namestr;
    Subitems.Add( paramstr );
    Subitems.Add( targetstr );
    Subitems.Add( c.ForegroundWindow.Annotation );
    Subitems.Add( FloatToStr(c.Delay / 1000.0) );
  end;
end;

procedure TisTestEditorFrame.OpenScript(aScript: IisTEScript2);
var
  i: integer;
begin
  if assigned(FScript) then
    FScript.Advise( nil );
  FScript := aScript;
  FScript.Advise( Self );

  mNotes.Lines.Text := FScript.Comments;
  lvScript.Items.BeginUpdate;
  try
    lvScript.Items.Clear;
    for i := 0 to FScript.Count -1 do
      AddCommand( i );
  finally
    lvScript.Items.EndUpdate;
  end
end;

procedure TisTestEditorFrame.FormStatusExecute(Sender: TObject);
begin
//intentionally left blank
end;

procedure TisTestEditorFrame.FormStatusUpdate(Sender: TObject);
var
  s: string;
begin
  s := FScript.filename;
  if s = '' then
    s := '[Unnamed]';

  (Sender as TCustomAction).Caption := s + ' - Test Script - ' + StrFromState( TestingEngine.State )+'';
end;

constructor TisTestEditorFrame.Create(Owner: TComponent);
begin
  inherited;
  OpenScript( TestScriptFromFile('') );
end;

destructor TisTestEditorFrame.Destroy;
begin
  if assigned(FScript) then
    FScript.Advise( nil );
  FScript := nil;
  inherited;
end;

procedure TisTestEditorFrame.FileOpen1Accept(Sender: TObject);
begin
  if CloseQuery then
    OpenScript( TestScriptFromFile( (Sender as TFileOpen).Dialog.Filename ) );
end;

function TisTestEditorFrame.CloseQuery: boolean;
var
  res: integer;
begin
  Result := true;
  if FScript.IsModified then
  begin
    res := MessageDlg('Save changes in the test script?',mtConfirmation,mbYesNoCancel,0);
    case res of
      mrYes: fileSave.Execute;
//      mrNo: //noop
      mrCancel: Result := false;
    end;
  end;
end;

procedure TisTestEditorFrame.FileSaveAs1Accept(Sender: TObject);
begin
  FScript.SaveAs( (Sender as TFileSaveAs).Dialog.FileName );
end;

procedure TisTestEditorFrame.FileNewExecute(Sender: TObject);
begin
  if CloseQuery then
    OpenScript( TestScriptFromFile('') );
end;

procedure TisTestEditorFrame.FileSaveExecute(Sender: TObject);
begin
  if FScript.Filename = '' then
    FileSaveAs1.Execute
  else
    FScript.Save;
end;

procedure TisTestEditorFrame.FileSaveUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FScript.IsModified;
end;

procedure TisTestEditorFrame.EditDelete1Update(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(lvScript.Selected);
end;

procedure TisTestEditorFrame.EditDelete1Execute(Sender: TObject);
begin
  if MessageDlg('Delete from the selected command to the end?',mtConfirmation,mbOkCancel,0) = mrOk then
  begin
    lvScript.Items.BeginUpdate;
    try
      FScript.DeleteToEnd(lvScript.Selected.Index);
    finally
      lvScript.Items.EndUpdate;
    end;
  end
end;

procedure TisTestEditorFrame.FileExit1Execute(Sender: TObject);
begin
  //!! ugly                      
  if Owner is TCustomForm then
    (Owner as TCustomForm).Close;
end;

procedure TisTestEditorFrame.PlayUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := TestingEngine.State in [isTESStoped];
end;

procedure TisTestEditorFrame.PlayExecute(Sender: TObject);
begin
  TestingEngine.StartPlayback( FScript );
end;

procedure TisTestEditorFrame.RecUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := TestingEngine.State in [isTESStoped];
end;

procedure TisTestEditorFrame.RecExecute(Sender: TObject);
begin
  TestingEngine.StartRecording( FScript );
end;

procedure TisTestEditorFrame.PauseUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := TestingEngine.State in [isTESRecordingPaused, isTESRecording,
    isTESPlaybackPaused, isTESPlayback];
  (Sender as TCustomAction).Checked := TestingEngine.State in [isTESRecordingPaused, isTESPlaybackPaused];
  case TestingEngine.State of
    isTESRecordingPaused, isTESPlaybackPaused: (Sender as TCustomAction).Caption := 'Resume';
    isTESRecording, isTESPlayback: (Sender as TCustomAction).Caption := 'Pause';
  end;
end;

procedure TisTestEditorFrame.PauseExecute(Sender: TObject);
begin
  case TestingEngine.State of
    isTESRecordingPaused, isTESPlaybackPaused: TestingEngine.Resume;
    isTESRecording, isTESPlayback: TestingEngine.Pause;
  else
    Assert( false );
  end;
end;

procedure TisTestEditorFrame.StopUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled :=
    TestingEngine.State in [isTESRecording, isTESPlayback, isTESPlaybackWaiting,
                            isTESPlaybackPaused, isTESRecordingPaused];
end;

procedure TisTestEditorFrame.StopExecute(Sender: TObject);
begin
  TestingEngine.Stop;
end;

procedure TisTestEditorFrame.CommandAdded(idx: integer);
begin
  AddCommand(idx);
end;

procedure TisTestEditorFrame.CommandDeleted(idx: integer);
var
  i: integer;
begin
  i := lvScript.Selected.Index;
  lvScript.Items.Delete(idx);
  if i = lvScript.Items.Count then
    i := lvScript.Items.Count-1;
  if i >= 0 then
    lvScript.Selected := lvScript.Items[i];
end;

procedure TisTestEditorFrame.mNotesChange(Sender: TObject);
begin
  FScript.Comments := mNotes.Lines.Text;
end;

procedure TisTestEditorFrame.Loaded;
begin
  inherited;
  PageControl1.ActivePage := tbCommands;
end;

end.
