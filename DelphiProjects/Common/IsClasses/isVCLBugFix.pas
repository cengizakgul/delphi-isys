unit isVCLBugFix;

interface

uses Classes, Windows, SyncObjs, SysConst, SysUtils;

type
  // > 2GB files support
  TSearchRecEx = record
    Time: Integer;
    Size: Int64;  // original is Integer
    Attr: Integer;
    Name: TFileName;
    ExcludeAttr: Integer;
    FindHandle: THandle;
    FindData: TWin32FindData;
  end;


// AV fix
procedure AbortEx;

// Thread-safe issue fix
function  MakeObjectInstance(Method: TWndMethod): Pointer;
procedure FreeObjectInstance(ObjectInstance: Pointer);
function  AllocateHWnd(Method: TWndMethod): HWND;
procedure DeallocateHWnd(Wnd: HWND);


// > 2GB files support
function  FindFirstEx(const Path: string; Attr: Integer; var  F: TSearchRecEx): Integer;
function  FindNextEx(var F: TSearchRecEx): Integer;
procedure FindCloseEx(var F: TSearchRecEx);

implementation

var
  ObjectInstanceLock: TCriticalSection;

function MakeObjectInstance(Method: TWndMethod): Pointer;
begin
  ObjectInstanceLock.Enter;
  try
    Result := Classes.MakeObjectInstance(Method);
  finally
    ObjectInstanceLock.Leave;
  end;
end;

procedure FreeObjectInstance(ObjectInstance: Pointer);
begin
  ObjectInstanceLock.Enter;
  try
    Classes.FreeObjectInstance(ObjectInstance);
  finally
    ObjectInstanceLock.Leave;
  end;
end;


function AllocateHWnd(Method: TWndMethod): HWND;
begin
  ObjectInstanceLock.Enter;
  try
    Result := Classes.AllocateHWnd(Method);
  finally
    ObjectInstanceLock.Leave;
  end;
end;

procedure DeallocateHWnd(Wnd: HWND);
begin
  ObjectInstanceLock.Enter;
  try
    Classes.DeallocateHWnd(Wnd);
  finally
    ObjectInstanceLock.Leave;
  end;
end;

procedure AbortEx;
begin
  raise EAbort.CreateRes(@SOperationAborted);
end;

function FindMatchingFileEx(var F: TSearchRecEx): Integer;
var
  LocalFileTime: TFileTime;
begin
  with F do
  begin
    while FindData.dwFileAttributes and ExcludeAttr <> 0 do
      if not Windows.FindNextFile(FindHandle, FindData) then
      begin
        Result := GetLastError;
        Exit;
      end;
    FileTimeToLocalFileTime(FindData.ftLastWriteTime, LocalFileTime);
    FileTimeToDosDateTime(LocalFileTime, LongRec(Time).Hi,
      LongRec(Time).Lo);
    Size := (Int64(FindData.nFileSizeHigh) shl 32) or FindData.nFileSizeLow;
    Attr := FindData.dwFileAttributes;
    Name := FindData.cFileName;
  end;
  Result := 0;
end;

function FindFirstEx(const Path: string; Attr: Integer; var  F: TSearchRecEx): Integer;
{$WARNINGS OFF}
const
  faSpecial = faHidden or faSysFile or faVolumeID or faDirectory;
{$WARNINGS ON}  
begin
  F.ExcludeAttr := not Attr and faSpecial;
  F.FindHandle :=  Windows.FindFirstFile(PChar(Path), F.FindData);
  if F.FindHandle <> INVALID_HANDLE_VALUE then
  begin
    Result := FindMatchingFileEx(F);
    if Result <> 0 then FindCloseEx(F);
  end else
    Result := GetLastError;
end;

function FindNextEx(var F: TSearchRecEx): Integer;
begin
  if Windows.FindNextFile(F.FindHandle, F.FindData) then
    Result := FindMatchingFileEx(F) else
    Result := GetLastError;
end;

procedure FindCloseEx(var F: TSearchRecEx);
begin
  if F.FindHandle <> INVALID_HANDLE_VALUE then
  begin
    Windows.FindClose(F.FindHandle);
    F.FindHandle := INVALID_HANDLE_VALUE;
  end;
end;


initialization
  ObjectInstanceLock := TCriticalSection.Create;

finalization
  ObjectInstanceLock.Free;

end.
