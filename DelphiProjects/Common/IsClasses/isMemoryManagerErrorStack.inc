// Activate Nexus Memory Manager and MAD Except

  nxReplacementMemoryManager,
  nxllMemoryManager,
  nxllTypes,
  nxllFastMove,
  nxllFastFillChar,
  nxllFastCpuDetect,
  nxllLockedFuncs,
  nxllMemoryManagerImpl,
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  isBasicStackTracer,  
  InitStackTracer,

{$HINTS ON}
{$WARNINGS ON}

