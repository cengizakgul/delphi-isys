unit EvTransportShared;

interface

uses Windows, SyncObjs, Classes, SysUtils, isBaseClasses, isSocket, isStreamTCP, isTransmitter,
     EvTransportDatagrams, isBasicUtils, isThreadManager, EvStreamUtils, isCryptoModule, isErrorUtils,
     ISZippingRoutines, EvTransportInterfaces, isCryptoApiWrapper, isStreamUDP, isExceptions, Variants;

const
  METHOD_TRANSPORT                     = 'Transport';
  METHOD_TRANSPORT_HANDSHAKE           = METHOD_TRANSPORT + '.Handshake';
  METHOD_TRANSPORT_LOGIN               = METHOD_TRANSPORT + '.Login';
  METHOD_TRANSPORT_BEGINMERGESESSIONS  = METHOD_TRANSPORT + '.BeginMergeSessions';
  METHOD_TRANSPORT_ENDMERGESESSIONS    = METHOD_TRANSPORT + '.EndMergeSessions';
  METHOD_TRANSPORT_SETCONDIRECTION     = METHOD_TRANSPORT + '.SetConDirection';
  METHOD_TRANSPORT_LOGOUT              = METHOD_TRANSPORT + '.Logout';
  METHOD_TRANSPORT_RESTORESESSION      = METHOD_TRANSPORT + '.RestoreSession';
  METHOD_TRANSPORT_PING                = METHOD_TRANSPORT + '.Ping';

  CLIENT_SESSION_KEEP_ALIVE_FOREVER = -1;   // never destroy after disconnect
  CLIENT_SESSION_NEVER_KEEP_ALIVE = 0;      // destroy immediately after disconnect
  CLIENT_SESSION_KEEP_ALIVE_TIMEOUT = 60;   // in seconds. destroy after timeout if disconnected

type
  EevTransport = class(EisException);
  EevConnection = class(EevTransport);
  EevProtocol = class(EevTransport);
  EevRequestTimeOut = class(EevProtocol);
  EevSessionLost = class(EevProtocol);

  TevDatagramHandler = procedure(const ADatagram: IevDatagram; out AResult: IevDatagram) of object;

  TevCustomDatagramDispatcher = class(TisCollection, IisTransmitterCallbacks)
  private
    FThreadManager: IThreadManager;
    FDatagramHandlers: TStringList;
    procedure HandleDatagram(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure HandleIncomingDataInThread(const Params: TTaskParamList); virtual;
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    procedure AddHandler(const AMethod: String; AHandler: TevDatagramHandler);
    function  HandlerExists(const AMethod: String): Boolean;
    procedure RegisterHandlers; virtual;
    function  GetThreadPoolCapacity: Integer;
    procedure SetThreadPoolCapacity(const AValue: Integer);
    function  CreateDatagram(const AStream: IEvDualStream): IevDatagram;
    function  CreateResponseFor(const ASourceDatagramHeader: IevDatagramHeader): IevDatagram;
    function  CreateErrorResponseFor(const ASourceDatagramHeader: IevDatagramHeader; const AException: Exception): IevDatagram;
    procedure BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader); virtual;
    procedure DispatchDatagram(const ADatagram: IevDatagram; out AResult: IevDatagram); virtual;
    procedure AfterDispatchDatagram(const ADatagramIn, ADatagramOut: IevDatagram); virtual;
    procedure BeforeQueueIncomingData(const ASessionID: TisGUID; const AHeader: String;
                                      const AStream: IEvDualStream; const ATransmissionTime: Cardinal; var Handled: Boolean);
    procedure OnIncomingHeader(const ASessionID: TisGUID; const AHeader: String; var Accept: Boolean);
    procedure OnIncomingData(const ASessionID: TisGUID; const AHeader: String; const AStream: IEvDualStream; const ATransmissionTime: Cardinal);
    procedure OnDisconnect(const ASessionID: TisGUID); virtual;
    procedure OnDiscardItems(const AItems: IisList); virtual;
    procedure TerminateDispatch;
    procedure WaitDispatchToFinish;
  end;


  TevDatagramDispatcher = class(TevCustomDatagramDispatcher, IevDatagramDispatcher)
  private
    FTransmitter: IisTCPStreamTransmitter;
    FExecutingDatagramInfo: IisParamsCollection;
    FSessionTimeout: Integer;
    FSessionExpiratorTask: TTaskID;
    procedure CloseAllSessionConnections(const ASessionID: TisGUID);
    procedure HandleIncomingDataInThread(const Params: TTaskParamList); override;
    procedure HandleIncomingData(const ASessionID: TisGUID; const AStream: IEvDualStream; const ATransmissionTime: Cardinal);
    procedure SessionExpirator(const Params: TTaskParamList);
  protected
    // Transport Datagrams
    procedure dmHandshake(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmLogin(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmBeginMergeSessions(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmEndMergeSessions(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetConDirection(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmLogout(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRestoreSession(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPing(const ADatagram: IevDatagram; out AResult: IevDatagram);

    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    function  CreateSessionExtension: IInterface; virtual;
    procedure DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState); virtual;
    procedure DoOnHandshake(var AEncryption: TevEncryptionType; var ACompression: TisCompressionLevel); virtual;
    function  DoOnLogin(const ADatagram: IevDatagram): Boolean; virtual;
    function  CheckClientAppID(const AClientAppID: String): Boolean; virtual;
    procedure BeforeDispatchIncomingData(const ASession: IevSession;  const ADatagramHeader: IevDatagramHeader;
                                         const AStream: IEvDualStream; var Handled: Boolean); virtual;
    procedure BeforeSendMethodResult(const ASession: IevSession; const AMethod, AResult: IevDatagram); virtual;
    procedure OnFatalError(const ASession: IevSession; const AError: String; const AData: IevDualStream); virtual;
    procedure DoOnIdle; virtual;
    procedure OnDisconnect(const ASessionID: TisGUID); override;
    procedure OnDiscardItems(const AItems: IisList); override;

    function  GetSessionTimeout: Integer;
    procedure SetSessionTimeout(const AValue: Integer);
    function  GetTransmitter: IisTCPStreamTransmitter;
    procedure SetTransmitter(const AValue: IisTCPStreamTransmitter);
    procedure RegisterHandlers; override;
    function  SessionCount: Integer;
    function  GetSessions: IisList;
    function  GetSessionByIndex(const AIndex: Integer): IevSession;
    function  FindSessionByID(const ASessionID: TisGUID): IevSession;
    procedure DeleteSession(const ASession: IevSession);
    procedure AddSession(const ASession: IevSession);
    function  CreateSession(const ASessionID: TisGUID): IevSession;
    procedure LockSessionList;
    procedure UnlockSessionList;
    function  ThisSession: IevSession;
    function  GetExecutingDatagramInfo: IisParamsCollection;
  end;


  TevMessageDispatcher = class(TevCustomDatagramDispatcher, IevMessageDispatcher)
  private
    FTransmitter: IisUDPStreamTransmitter;
    procedure HandleIncomingDataInThread(const Params: TTaskParamList); override;
    procedure HandleIncomingData(const ASocketID: TisGUID; const AStream: IEvDualStream);
  protected
    procedure DoOnConstruction; override;
    function  GetTransmitter: IisUDPStreamTransmitter;
    procedure SetTransmitter(const AValue: IisUDPStreamTransmitter);
    procedure SendMessage(const ASocketID: TisGUID; const ADatagram: IevDatagram);
  end;


  TevCustomTCPClient = class(TisInterfacedObject, IisTCPClient)
  private
    FCommunicator: IisAsyncTCPClient;
    FTransmitter: IisTCPStreamTransmitter;
    FDispatcher: IevDatagramDispatcher;
    FEncryptionType: TevEncryptionType;
    FCompressionLevel: TisCompressionLevel;
  protected
    FSSLCertFile: String;
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    function  FindSession(const ASessionID: TisGUID): IevSession;
    function  CreateDispatcher: IevDatagramDispatcher; virtual; abstract;
    function  CreateConnection(const AHost, APort, AUser, APassword: String;
                               const ASessionID, AContextID: TisGUID;
                               const ALocalIPAddr: String = ''; const AAppID: String = ''): TisGUID;
    procedure DestroyConnection(const ASessionID: String);
    procedure OnConnectStatusChange(const AText: String; const AProgress: Integer); virtual;
    procedure Disconnect(const ASessionID: String);
    function  GetSession(const ASessionID: TisGUID): IevSession;
    function  GetEncryptionType: TevEncryptionType;
    procedure SetEncryptionType(const AValue: TevEncryptionType);
    function  GetCompressionLevel: TisCompressionLevel;
    procedure SetCompressionLevel(const AValue: TisCompressionLevel);
    function  GetDatagramDispatcher: IevDatagramDispatcher;
  end;


  TevCustomTCPServer = class(TisInterfacedObject, IevTCPServer)
  private
    FCommunicator: IisAsyncTCPServer;
    FTransmitter:  IisTCPStreamTransmitter;
    FDispatcher:   IevDatagramDispatcher;
  protected
    procedure SetSSLTimeout(const Value: Integer);
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    function  CreateDispatcher: IevDatagramDispatcher; virtual; abstract;
    function  GetActive: Boolean;
    function  GetIPAddress: String;
    procedure SetIPAddress(const AValue: String);
    function  GetPort: String;
    procedure SetPort(const AValue: String);
    procedure SetActive(const AValue: Boolean); virtual;
    function  GetDatagramDispatcher: IevDatagramDispatcher;
  end;


  TevCustomUDPMessenger = class(TisInterfacedObject)
  private
    FCommunicator: IisAsyncUDPPeer;
    FTransmitter:  IisUDPStreamTransmitter;
    FDispatcher:   IevMessageDispatcher;
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    function  CreateDispatcher: IevMessageDispatcher; virtual; abstract;
    function  GetMessageDispatcher: IevMessageDispatcher;
    function  OpenSocket(const AHost: String; const ARemotePort: Integer; const ALocalPort: Integer = 0): TisGUID;
    function  OpenSocketForBroadcast(const ARemotePort: Integer; const ALocalPort: Integer = 0): TisGUID;
    procedure Close(const ASocketID: TisGUID);
  end;


  function IsTransportMethod(const ADatagramHeader: IevDatagramHeader): Boolean;
  function IsTransportResponse(const ADatagramHeader: IevDatagramHeader): Boolean;
  function IsTransportDatagram(const ADatagramHeader: IevDatagramHeader): Boolean;

implementation

uses DateUtils;

threadvar tvThisSession: IevSession;

const
   TRASPORT_PROTOCOL_SIGNATURE = 'IS';
   EV_RESPONSE_TIMEOUT = 1000; // Check connection every 1 sec
                               //(in case if server drops client connection and for callback purpose)

   aEncrDescr: array [Low(TevEncryptionType)..High(TevEncryptionType)] of string = ('None', 'ISP', 'SSL');

type
  IevSessionEx = interface(IevSession)
  ['{EA9F95A9-6BF4-4945-8478-596C820B9719}']
    procedure AddDelayedData(const ATransmitterQueueItem: IisStreamQueueItem);
    procedure SendDelayedData;
    procedure SetKeepAlive(const AValue: Boolean);
    function  DisconnectTime: TDateTime;
    procedure SetLastActivity(const AValue: TDateTime);
    procedure SetExtension(const AValue: IInterface);
  end;


  TevSession = class(TisCollection, IevSession, IevSessionEx)
  private
    FState: TevSessionState;
    FEncryptionType: TevEncryptionType;
    FCompressionLevel: TisCompressionLevel;
    FAppID: String;
    FLogin: String;
    FPassword: String;
    FErrorMessage: String;
    FEncryptor: IisCryptoApiWrapper;
    FMergingWith: IevSession;
    FDelayedData: IisList;
    FKeepAlive: Boolean;
    FDisconnectTime: TDateTime;
    FLastActivity: TDateTime;
    FExtension: IInterface;
    function  AfterReceive(const AStream: IevDualStream): IevDualStream;
    function  BeforeSend(const AStream: IevDualStream): IevDualStream;
    function  GetResponseHandler(const AName: String): IisNamedObject;
    procedure CleanUpResponseHandlers;
    procedure AddDelayedData(const ATransmitterQueueItem: IisStreamQueueItem);
    procedure SendDelayedData;
    procedure SetKeepAlive(const AValue: Boolean);
    function  DisconnectTime: TDateTime;
    procedure SetLastActivity(const AValue: TDateTime);
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    function  GetState: TevSessionState;
    procedure SetState(const AValue: TevSessionState);
    function  StateDescr: String;
    function  GetMergingWith: IevSession;
    procedure SetMergingWith(const AValue: IevSession);
    function  GetAppID: String;
    procedure SetAppID(const AValue: String);
    function  GetLogin: String;
    procedure SetLogin(const AValue: String);
    function  GetPassword: String;
    procedure SetPassword(const AValue: String);
    function  GetSessionID: TisGUID;
    procedure SetSessionID(const AValue: TisGUID);
    function  KeepAlive: Boolean;
    function  GetConnectionCount: Integer;
    function  GetErrorMessage: String;
    function  AddConnection(const AConnection: IisTCPStreamAsyncConnection): Boolean;
    function  SendRequest(const ADatagram: IevDualStream; const ADatagramSequenceNbr: Int64;
                          const ABlockingMode: Boolean; out ASentBytes, AReceivedBytes, ATransmissionTime: Cardinal): IevDualStream; overload;
    function  SendRequest(const ADatagram: IevDatagram; const ABlockingMode: Boolean = True): IevDatagram; overload;
    procedure SendDatagram(const ADatagram: IevDatagram);
    function  SendRequest(const ADatagram: IevDatagram; const AMaxWaitTime: Integer): IevDatagram; overload;
    procedure SendData(const AData: IevDualStream; const ADatagramHeader: IevDatagramHeader; out ASentBytes: Cardinal);
    function  GetResponseDataFor(const ADatagramSequenceNbr: Int64; const AWaitTime: Cardinal;
                                 out AReceivedBytes, ATransmissionTime: Cardinal): IEvDualStream;
    function  GetResponseDatagramFor(const ADatagramSequenceNbr: Int64; const AWaitTime: Cardinal = 0): IevDatagram;
    function  GetEncryptionType: TevEncryptionType;
    procedure SetEncryptionType(const AValue: TevEncryptionType);
    function  EncryptionTypeDscr: String;
    function  GetEncryptor: IisCryptoApiWrapper;
    function  GetCompressionLevel: TisCompressionLevel;
    procedure SetCompressionLevel(const AValue: TisCompressionLevel);
    function  GetTransmitter: IisTCPStreamTransmitter;
    function  GetLastActivity: TDateTime;
    function  GetRemoteIPAddress: String;
    procedure SetExtension(const AValue: IInterface);
    function  GetExtension: IInterface;
    function  CheckIfAlive: Boolean;
  end;


  // Each such object tracks down server response for request
  IevDatagramResponseHandler = interface
  ['{A99FBA64-4B61-4C36-AF22-EA91742DD89D}']
     function ReceivedSize: Cardinal;
     function TransmissionTime: Cardinal;     
     function Wait(const AWaitTime: Cardinal = 0): IevDualStream;
  end;

  TevDatagramResponseHandler = class(TisNamedObject, IevDatagramResponseHandler)
  private
    FResponseReceivedEvent: THandle;
    FResponseDatagram: IevDualStream;
    FReceivedSize: Cardinal;
    FTransmissionTime: Cardinal;
    function    SessionIsAlive: Boolean;
    function    ReceivedSize: Cardinal;
    function    TransmissionTime: Cardinal;
    function    Wait(const AWaitTime: Cardinal = 0): IevDualStream;
    procedure   SetResponse(const AResponseDatagram: IevDualStream; const AReceivedSize: Cardinal; const ATransmissionTime: Cardinal);
  protected
    procedure   DoOnConstruction; override;
    procedure   DoOnDestruction; override;
  end;



function IsTransportMethod(const ADatagramHeader: IevDatagramHeader): Boolean;
begin
  Result := StartsWith(ADatagramHeader.Method, METHOD_TRANSPORT + '.') or
            ADatagramHeader.IsResponse and Contains(ADatagramHeader.Method, METHOD_TRANSPORT + '.');
end;

function IsTransportResponse(const ADatagramHeader: IevDatagramHeader): Boolean;
begin
  Result := ADatagramHeader.IsResponse and Contains(ADatagramHeader.Method, METHOD_TRANSPORT + '.');
end;

function IsTransportDatagram(const ADatagramHeader: IevDatagramHeader): Boolean;
begin
  Result := IsTransportMethod(ADatagramHeader) or IsTransportResponse(ADatagramHeader);
end;



{ TevDatagramDispatcher }

procedure TevDatagramDispatcher.AddSession(const ASession: IevSession);
begin
  AddChild(ASession as IisInterfacedObject);
end;

procedure TevDatagramDispatcher.OnDisconnect(const ASessionID: TisGUID);
var
  Session: IevSession;
begin
  inherited;
  Lock;
  try
    Session := FindSessionByID(ASessionID);
    if Assigned(Session) and (GetTransmitter.GetTCPCommunicator.GetConnectionsBySessionID(ASessionID).Count = 0) then
    begin
      if not Session.KeepAlive or (Session.State = ssInactive) then
        DeleteSession(Session)  // close session on connection drop or client Logout
      else
      begin
        CloseAllSessionConnections(ASessionID); // keep session alive for future restore
        Session.State := ssInactive;
      end;
    end;
  finally
    Unlock;
  end;
end;

procedure TevDatagramDispatcher.DeleteSession(const ASession: IevSession);
begin
  if Assigned(ASession) then
    try
      CloseAllSessionConnections(ASession.SessionID);
      ASession.State := ssInvalid;
    finally
      RemoveChild(ASession as IisInterfacedObject);
    end;
end;

procedure TevDatagramDispatcher.DoOnConstruction;
begin
  inherited;
  FExecutingDatagramInfo := TisParamsCollection.Create(nil, True);
end;

function TevDatagramDispatcher.FindSessionByID(
  const ASessionID: TisGUID): IevSession;
begin
  Result := FindChildByName(ASessionID) as IevSession;
end;

function TevDatagramDispatcher.GetTransmitter: IisTCPStreamTransmitter;
begin
  Result := FTransmitter;
end;

procedure TevDatagramDispatcher.SetTransmitter(const AValue: IisTCPStreamTransmitter);
begin
  if FTransmitter <> AValue then
  begin
    if Assigned(FTransmitter) then
      FTransmitter.Callbacks := nil;
    FTransmitter := AValue;
    if Assigned(FTransmitter) then
      FTransmitter.Callbacks := Self;
  end;
end;

procedure TevDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_TRANSPORT_HANDSHAKE, dmHandshake);
  AddHandler(METHOD_TRANSPORT_LOGIN, dmLogin);
  AddHandler(METHOD_TRANSPORT_BEGINMERGESESSIONS, dmBeginMergeSessions);
  AddHandler(METHOD_TRANSPORT_ENDMERGESESSIONS, dmEndMergeSessions);
  AddHandler(METHOD_TRANSPORT_SETCONDIRECTION, dmSetConDirection);
  AddHandler(METHOD_TRANSPORT_LOGOUT, dmLogout);
  AddHandler(METHOD_TRANSPORT_RESTORESESSION, dmRestoreSession);
  AddHandler(METHOD_TRANSPORT_PING, dmPing);
end;

function TevDatagramDispatcher.GetSessionByIndex(const AIndex: Integer): IevSession;
begin
  Result := GetChild(AIndex) as IevSession;
end;

function TevDatagramDispatcher.SessionCount: Integer;
begin
  Result := ChildCount;
end;


procedure TevDatagramDispatcher.LockSessionList;
begin
  Lock;
end;

procedure TevDatagramDispatcher.UnlockSessionList;
begin
  Unlock;
end;

function TevDatagramDispatcher.CreateSession(const ASessionID: TisGUID): IevSession;
begin
  Result := TevSession.Create;
  Result.SessionID := ASessionID;
  (Result as IevSessionEx).SetExtension(CreateSessionExtension);
  AddSession(Result);
end;

procedure TevDatagramDispatcher.HandleIncomingData(const ASessionID: TisGUID; const AStream: IEvDualStream;
  const ATransmissionTime: Cardinal);
var
  Datagram, ResultDatagram: IevDatagram;
  Session: IevSession;
  Stream: IEvDualStream;
  ResponseHandler: IisInterfacedObject;
  DH: IevDatagramHeader;
  bHandled: Boolean;
  L: IisList;
  DMLogRec: IisListOfValues;
  s: String;
begin
  Session := FindSessionByID(ASessionID) as IevSession;

  AStream.Position := 0;
  if Assigned(Session) then
  begin
    if (Session.State = ssMerging) then
      Session := Session.MergingWith;

    CheckCondition(Session.State <> ssInvalid, 'Attempt to use invalid session', EevProtocol);
    Stream := TevSession((Session as IisInterfacedObject).GetImplementation).AfterReceive(AStream);
  end
  else
    Stream := AStream;

  DH := TevDatagram.GetDatagramHeader(Stream);

  DMLogRec := FExecutingDatagramInfo.AddParams(''); // punch in
  try
    DMLogRec.AddValue('StartTime',  CurrentTaskStartTime);
    DMLogRec.AddValue('dmHeader', DH);

    if not Assigned(Session) then
    begin
      L := GetTransmitter.TCPCommunicator.GetConnectionsBySessionID(ASessionID);

      if L.Count = 0 then  // Session is no more but there is data in receiving queue.
        exit;              // Hard to say how to handle this... discard it.

      Session := FindSessionByID(DH.SessionID) as IevSession;  // try to find existing session
      if not Assigned(Session) then
        Session := CreateSession(DH.SessionID) // new session
      else
      begin
        // Restore session
        if Session.KeepAlive and (Session.State = ssActive) then
          CloseAllSessionConnections(Session.SessionID);
      end;

      Assert(Session.AddConnection(L[0] as IisTCPStreamAsyncConnection));
    end
    else
      CheckCondition(DH.SessionID = Session.SessionID, 'Datagram does not belong to specified session', EevProtocol);

    tvThisSession := Session;
    try
      (Session as IevSessionEx).SetLastActivity(Now);

      bHandled := False;
      BeforeDispatchIncomingData(Session, DH, Stream, bHandled);

      if not bHandled then
        if DH.IsResponse then
        begin
          s := IntToStr(DH.SequenceNBR); // DELPHI BUG !!! Assign to var first!!!  Aleksey
          ResponseHandler := TevSession((Session as IisInterfacedObject).GetImplementation).GetResponseHandler(s);
          if Assigned(ResponseHandler) then
            TevDatagramResponseHandler(ResponseHandler.GetImplementation).SetResponse(Stream, AStream.Size, ATransmissionTime);
        end
        else
        begin
          try
            Datagram := nil;
            BeforeDispatchDatagram(DH);
            try
              try
                Datagram := CreateDatagram(Stream);
                Datagram.TransmissionTime := ATransmissionTime;
                DispatchDatagram(Datagram, ResultDatagram);  // Here is an entry point into server's command handler
              except
                on E: Exception do
                  ResultDatagram := CreateErrorResponseFor(DH, E);
              end;
            finally
              AfterDispatchDatagram(Datagram, ResultDatagram);
            end;
          except
            on E: Exception do
              // always return the ERROR_ACK if something wrong
              ResultDatagram := CreateErrorResponseFor(DH, E);
          end;

          if Assigned(ResultDatagram) then
          begin
            BeforeSendMethodResult(Session, Datagram, ResultDatagram);
            try
              Session.SendDatagram(ResultDatagram); // may invoke internal objects streaming, which may fail
            except
              on E: Exception do
              begin
                // always return the ERROR_ACK if something wrong
                ResultDatagram := CreateErrorResponseFor(DH, E);
                Session.SendDatagram(ResultDatagram);
              end;
            end;
          end;
        end;

    finally
      tvThisSession := nil;
    end;
  finally
    FExecutingDatagramInfo.DeleteParams(DMLogRec);  // punch out
  end;
end;

procedure TevDatagramDispatcher.CloseAllSessionConnections(const ASessionID: TisGUID);
var
  L: IisList;
  i: Integer;
  Session: IevSession;
begin
  Session := FindSessionByID(ASessionID);
  if Assigned(Session) and not Session.KeepAlive then
    TevSession((Session as IisInterfacedObject).GetImplementation).CleanUpResponseHandlers;

  if Assigned(FTransmitter) and Assigned(FTransmitter.TCPCommunicator) then
  begin
    Lock;
    try
      L := FTransmitter.TCPCommunicator.GetConnectionsBySessionID(ASessionID);
      for i := L.Count - 1 downto 0 do
      begin
        FTransmitter.TCPCommunicator.DestroyConnection((L[i] as IisTCPStreamAsyncConnection).GetSocketID);
        L.Delete(i);
      end;
    finally
      Unlock;
    end;
  end;
end;

procedure TevDatagramDispatcher.dmHandshake(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Session: IevSession;
  EncryptionType: TevEncryptionType;
  CompressionLevel: TisCompressionLevel;
  Connections: IisList;
begin
  // This is temporary session for new client connection or existing restoring session
  Session := FindSessionByID(ADatagram.Header.SessionID) as IevSession;

  // Handshake is allowed to be performed only once
  CheckCondition(Session.State = ssInactive, 'Incorrect session state', EevProtocol);

  CheckCondition(CheckClientAppID(ADatagram.Params.Value['AppID']), 'Invalid application type', EevProtocol);

  Session.AppID := ADatagram.Params.Value['AppID'];
  Session.State := ssPerformingHandshake;

  // Send response
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('AppID', AppID);

  EncryptionType := ADatagram.Params.TryGetValue('EncryptionType', Session.EncryptionType);
  CompressionLevel := ADatagram.Params.TryGetValue('CompressionLevel', Session.CompressionLevel);
  DoOnHandshake(EncryptionType, CompressionLevel);
  AResult.Params.AddValue('EncryptionType', EncryptionType);
  AResult.Params.AddValue('CompressionLevel', CompressionLevel);
  Session.EncryptionType := TevEncryptionType(EncryptionType);
  Session.CompressionLevel := CompressionLevel;

  if Session.EncryptionType = etProprietary then
    AResult.Params.AddValue('SessionKey',
      Session.Encryptor.GetSessionKey(IInterface(ADatagram.Params.Value['PublicKey']) as IevDualStream));

  Connections := GetTransmitter.TCPCommunicator.GetConnectionsBySessionID(Session.SessionID);
  Assert(Connections.Count = 1);
  (Connections[0] as IisTCPStreamAsyncConnection).SetSocketID(ADatagram.Params.Value['ConnectionID']);
end;

procedure TevDatagramDispatcher.dmLogin(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Session: IevSession;
begin
  // This is temporary session for new client connection or restoring session
  Session := FindSessionByID(ADatagram.Header.SessionID) as IevSession;
  CheckCondition(Assigned(Session), 'Session is not found', EevProtocol);
  try
    CheckCondition(Session.State = ssWaitingForLogin, 'Incorrect session state', EevProtocol);

    Session.State := ssPerformingLogin;

    if not Session.KeepAlive then
    begin
      Session.Login := ADatagram.Header.User;
      Session.Password := ADatagram.Header.Password;      
      CheckCondition(DoOnLogin(ADatagram), 'Invalid login', EevProtocol);
    end;

    AResult := CreateResponseFor(ADatagram.Header);
    Session.State := ssActive;
    if ADatagram.Params.TryGetValue('KeepAlive', False) then
      (Session as IevSessionEx).SetKeepAlive(GetSessionTimeout <> CLIENT_SESSION_NEVER_KEEP_ALIVE)
    else
      (Session as IevSessionEx).SetKeepAlive(False);
  except
    Session.State := ssInvalid;
    raise;
  end;
end;

function TevDatagramDispatcher.DoOnLogin(const ADatagram: IevDatagram): Boolean;
begin
  Result := True;
end;

procedure TevDatagramDispatcher.BeforeDispatchIncomingData(const ASession: IevSession;
  const ADatagramHeader: IevDatagramHeader; const AStream: IEvDualStream; var Handled: Boolean);
begin
end;

procedure TevDatagramDispatcher.DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState);
begin
end;

function TevDatagramDispatcher.CheckClientAppID(const AClientAppID: String): Boolean;
begin
  Result := True;
end;

procedure TevDatagramDispatcher.dmBeginMergeSessions(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Session, AttachingSession: IevSession;
begin
  Session := FindSessionByID(ADatagram.Header.SessionID) as IevSession;
  CheckCondition(Assigned(Session), 'Session is not found', EevProtocol);
  CheckCondition(Session.State = ssActive, 'Incorrect session state', EevProtocol);

  AttachingSession := FindSessionByID(ADatagram.Params.Value['SessionID']) as IevSession;
  CheckCondition(Assigned(AttachingSession), 'Merging session is not found', EevProtocol);
  CheckCondition(AttachingSession.State = ssActive, 'Incorrect session state', EevProtocol);

  AttachingSession.State := ssMerging;
  AttachingSession.MergingWith := Session;

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevDatagramDispatcher.dmEndMergeSessions(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Session, AttachingSession: IevSession;
  i: Integer;
  Connection: IisTCPStreamAsyncConnection;
  Connections: IisList;
begin
  Session := FindSessionByID(ADatagram.Header.SessionID) as IevSession;
  CheckCondition(Assigned(Session), 'Session is not found', EevProtocol);
  CheckCondition(Session.State = ssActive, 'Incorrect session state', EevProtocol);

  AttachingSession := FindSessionByID(ADatagram.Params.Value['SessionID']) as IevSession;
  CheckCondition(Assigned(AttachingSession), 'Merging session is not found', EevProtocol);
  CheckCondition(AttachingSession.State = ssMerging, 'Incorrect session state', EevProtocol);

  Connections := GetTransmitter.TCPCommunicator.GetConnectionsBySessionID(AttachingSession.SessionID);

  for i := 0 to Connections.Count - 1 do
  begin
    Connection := Connections[i] as IisTCPStreamAsyncConnection;
    CheckCondition(Session.AddConnection(Connection), 'Cannot attach session', EevProtocol);
  end;

  AttachingSession.State := ssInvalid;
  AttachingSession.MergingWith := nil;
  DeleteSession(AttachingSession);

  AResult := CreateResponseFor(ADatagram.Header);
end;

function TevDatagramDispatcher.GetExecutingDatagramInfo: IisParamsCollection;
begin
  Result := FExecutingDatagramInfo;
end;

function TevDatagramDispatcher.GetSessions: IisList;
begin
  Result := GetChildrenList;
end;

procedure TevDatagramDispatcher.HandleIncomingDataInThread(const Params: TTaskParamList);
var
  SessionID: String;
  Session: IevSession;
  Stream: IEvDualStream;
  i: Integer;
  L: IisList;
  TransmissionTime: Cardinal;
begin
  tvThisSession := nil;
  SessionID := Params[0];
  Stream := IInterface(Params[1]) as IEvDualStream;
  TransmissionTime := Params[2];
  try
    HandleIncomingData(SessionID, Stream, TransmissionTime);
  except
    on E: Exception do
    begin
      OnFatalError(Session, E.Message, Stream);
      
      // Unexpected transport errors! DISCONNECT!
      try
        Session := FindSessionByID(SessionID) as IevSession;
        if Assigned(Session) then
          DeleteSession(Session)
        else
        begin
          // session is not created yet! it means it's a new incoming connection and
          // handshake has failed because of low level problem (e.g. datagram format is wrong)
          if Assigned(FTransmitter) and Assigned(FTransmitter.TCPCommunicator) then
          begin
            L := FTransmitter.TCPCommunicator.GetConnectionsBySessionID(SessionID);
            for i := L.Count - 1 downto 0 do
            begin
              FTransmitter.TCPCommunicator.DestroyConnection((L[i] as IisTCPStreamAsyncConnection).GetSocketID);
              L.Delete(i);
            end;
          end;
        end;
      except
      end;

      raise;
    end;
  end;
end;

procedure TevDatagramDispatcher.DoOnIdle;
begin
end;

procedure TevDatagramDispatcher.BeforeSendMethodResult(const ASession: IevSession; const AMethod, AResult: IevDatagram);
begin
end;

procedure TevDatagramDispatcher.OnFatalError(const ASession: IevSession; const AError: String; const AData: IevDualStream);
begin
end;

procedure TevDatagramDispatcher.dmSetConDirection(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Session: IevSession;
  Directions: IisListOfValues;
  i: Integer;
  Connections: IisList;
begin
  Session := FindSessionByID(ADatagram.Header.SessionID) as IevSession;
  CheckCondition(Assigned(Session), 'Session is not found', EevProtocol);
  CheckCondition(Session.State = ssActive, 'Incorrect session state', EevProtocol);

  Directions := IInterface(ADatagram.Params.Value['Directions']) as IisListOfValues;
  Connections := GetTransmitter.TCPCommunicator.GetConnectionsBySessionID(Session.SessionID);

  CheckCondition(Connections.Count = Directions.Count, 'Connections don''t match', EevProtocol);

  for i := 0 to Connections.Count - 1 do
  begin
   (Connections[i] as IisTCPStreamAsyncConnection).DataDirection :=
     TisDataDirection(Directions.Value[(Connections[i] as IisTCPStreamAsyncConnection).GetSocketID]);
  end;

  AResult := CreateResponseFor(ADatagram.Header);
end;


function TevDatagramDispatcher.GetSessionTimeout: Integer;
begin
  Result := FSessionTimeout;
end;

procedure TevDatagramDispatcher.SetSessionTimeout(const AValue: Integer);
begin
  if (FSessionTimeout <> AValue) and (FSessionExpiratorTask <> 0) then
  begin
    GlobalThreadManager.TerminateTask(FSessionExpiratorTask);
    GlobalThreadManager.WaitForTaskEnd(FSessionExpiratorTask);
    FSessionExpiratorTask := 0;
  end;

  FSessionTimeout := AValue;

  if FSessionTimeout > CLIENT_SESSION_NEVER_KEEP_ALIVE then
    FSessionExpiratorTask := GlobalThreadManager.RunTask(SessionExpirator, Self, MakeTaskParams([]), 'Session expirator', tpLower);
end;

procedure TevDatagramDispatcher.dmLogout(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Session: IevSession;
begin
  Session := FindSessionByID(ADatagram.Header.SessionID) as IevSession;
  CheckCondition(Assigned(Session), 'Session is not found', EevProtocol);
  AResult := CreateResponseFor(ADatagram.Header);
  Session.State := ssInactive;
  (Session as IevSessionEx).SetKeepAlive(False);
end;

procedure TevDatagramDispatcher.OnDiscardItems(const AItems: IisList);
var
  Session: IevSession;
  i: Integer;
begin
  inherited;
  // Transmitter could not deliver these items

  for i := 0 to AItems.Count - 1 do
  begin
    (AItems[i] as IisStreamQueueItem).SessionID;
    Session := FindSessionByID((AItems[i] as IisStreamQueueItem).SessionID);
    if Assigned(Session) then
      (Session as IevSessionEx).AddDelayedData(AItems[i] as IisStreamQueueItem);
  end;
end;

procedure TevDatagramDispatcher.dmRestoreSession(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Session: IevSession;
begin
  // This is the restoring session
  Session := FindSessionByID(ADatagram.Header.SessionID) as IevSession;
  CheckCondition(Session.KeepAlive, 'Session restore error', EevProtocol);
  (Session as IevSessionEx).SendDelayedData;
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevDatagramDispatcher.SessionExpirator(const Params: TTaskParamList);

  procedure Check;
  var
    i: Integer;
    L: IisList;
    S: IevSessionEx;
  begin
    L := GetSessions;
    for i := 0 to L.Count - 1 do
    begin
      S := L[i] as IevSessionEx;
      if S.KeepAlive and (S.DisconnectTime <> 0) then
        if SecondsBetween(Now, S.DisconnectTime) >= GetSessionTimeout then
          DeleteSession(S);
    end;
  end;

begin
  while not CurrentThreadTaskTerminated do
  begin
    try
      Check;
    except
    end;

    Snooze(60000); // check every minute
  end;

  Check;
end;

function TevDatagramDispatcher.CreateSessionExtension: IInterface;
begin
  Result := nil;
end;

function TevDatagramDispatcher.ThisSession: IevSession;
begin
  Result := tvThisSession;
end;

procedure TevDatagramDispatcher.DoOnHandshake(var AEncryption: TevEncryptionType; var ACompression: TisCompressionLevel);
begin
end;

procedure TevDatagramDispatcher.dmPing(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
end;


procedure TevDatagramDispatcher.DoOnDestruction;
var
  i: Integer;
begin
  for i := SessionCount - 1 downto 0 do
    CloseAllSessionConnections(GetSessionByIndex(i).SessionID);

  if GlobalThreadManagerIsActive and (FSessionExpiratorTask <> 0) then
  begin
    GlobalThreadManager.TerminateTask(FSessionExpiratorTask);
    GlobalThreadManager.WaitForTaskEnd(FSessionExpiratorTask);
  end;

  inherited;
end;


{ TevSession }

function TevSession.AddConnection(const AConnection: IisTCPStreamAsyncConnection): Boolean;
var
  OldSession: IevSession;

  function CanAttach: Boolean;
  begin
    Result := AnsiSameStr(GetAppID, OldSession.AppID) and AnsiSameStr(GetLogin, OldSession.Login);  // sessions must belong to the same app and user
    if Result then
      Result := GetRemoteIPAddress = AConnection.GetRemoteIPAddress; // and they should be associated with one IP address
  end;

begin
  OldSession := (GetOwner as IevDatagramDispatcher).FindSessionByID(AConnection.SessionID) as IevSession;
  Lock;
  try
    if not Assigned(OldSession) or CanAttach then
    begin
      AConnection.SessionID := GetSessionID;
      Result := True;
    end
    else
      Result := False;
  finally
    Unlock;
  end;
end;

function TevSession.GetCompressionLevel: TisCompressionLevel;
begin
  Result := FCompressionLevel;
end;

function TevSession.GetConnectionCount: Integer;
var
  Transmitter: IisTCPStreamTransmitter;
begin
  Transmitter := GetTransmitter;
  if Assigned(Transmitter) then
    Result := Transmitter.TCPCommunicator.GetConnectionsBySessionID(GetSessionID).Count
  else
    Result := 0;  
end;

function TevSession.GetEncryptionType: TevEncryptionType;
begin
  Result := FEncryptionType;
end;

function TevSession.GetLogin: String;
begin
  Result := FLogin;
end;

function TevSession.GetResponseDatagramFor(const ADatagramSequenceNbr: Int64; const AWaitTime: Cardinal = 0): IevDatagram;
var
  Res: IevDualStream;
  ReceivedBytes, TransmissionTime: Cardinal;
begin
  Res := GetResponseDataFor(ADatagramSequenceNbr, AWaitTime, ReceivedBytes, TransmissionTime);
  if Assigned(Res) then
  begin
    Result := TevDatagram.CreateFromStream(Res);
    Result.TransmittedBytes := ReceivedBytes;
    Result.TransmissionTime := TransmissionTime;
    if Result.IsError then
      raise Result.GetException;
  end
  else
    Result := nil;
end;

function TevSession.GetResponseHandler(const AName: String): IisNamedObject;
begin
  Result := FindChildByName(AName);
end;

function TevSession.GetState: TevSessionState;
begin
  Result := FState;
end;

function TevSession.SendRequest(const ADatagram: IevDatagram; const ABlockingMode: Boolean): IevDatagram;
var
  S: IEvDualStream;
  Sent, Received, TransmissionTime: Cardinal;
begin
  S := TEvDualStreamHolder.Create;
  (ADatagram as IisInterfacedObject).WriteToStream(S);
  S.Position := 0;

  S := SendRequest(S, ADatagram.Header.SequenceNbr, ABlockingMode, Sent, Received, TransmissionTime);
  ADatagram.TransmittedBytes := Sent;

  if Assigned(S) then
  begin
    Result := TevDatagram.CreateFromStream(S);
    Result.TransmittedBytes := Received;
    Result.TransmissionTime := TransmissionTime;
    if Result.IsError then
      raise Result.GetException;
  end
  else
    Result := nil;
end;

procedure TevSession.SendDatagram(const ADatagram: IevDatagram);
var
  S: IEvDualStream;
  SentBytes: Cardinal;
begin
  CheckCondition(ADatagram.Header.SessionID = GetSessionID, 'Datagram does not belong to specified session', EevProtocol);

  try
    S := TEvDualStreamHolder.Create;
    (ADatagram as IisInterfacedObject).WriteToStream(S);
  except
    FErrorMessage := ADatagram.Header.Method + ': Data streaming error.';
    raise EisStreamingException.Create(ADatagram.Header.Method + ': Data streaming error.');
  end;

  if (FState = ssPerformingHandshake) and ADatagram.Header.IsResponse then
    FState := ssWaitingForLogin;

  SendData(S, ADatagram.Header, SentBytes);
  ADatagram.TransmittedBytes := SentBytes;
end;


procedure TevSession.SetCompressionLevel(const AValue: TisCompressionLevel);
begin
  FCompressionLevel := AValue;
end;

procedure TevSession.SetEncryptionType(const AValue: TevEncryptionType);
begin
  if FEncryptionType <> AValue then
  begin
    FEncryptionType := AValue;
    
    case FEncryptionType of
      etProprietary: FEncryptor := TisCryptoApiWrapperBlowfish.Create;
    else
      FEncryptor := nil;
    end;
  end;
end;

procedure TevSession.SetLogin(const AValue: String);
begin
  FLogin := AValue;
end;

procedure TevSession.SetState(const AValue: TevSessionState);
var
  PrevState: TevSessionState;
begin
  PrevState := FState;
  FState := AValue;

  if (PrevState = ssActive) and (FState = ssInactive) then
    FDisconnectTime := Now
  else if FState in [ssPerformingHandshake .. ssActive] then
    FDisconnectTime := 0;

  if (GetOwner <> nil) and (FState <> PrevState) then
    TevDatagramDispatcher(GetOwner.GetImplementation).DoOnSessionStateChange(Self, PrevState);
end;

function TevSession.GetTransmitter: IisTCPStreamTransmitter;
var
  DD: IevDatagramDispatcher;
begin
  DD := GetOwner as IevDatagramDispatcher;
  if Assigned(DD) then
    Result := DD.Transmitter
  else
    Result := nil;  
end;

function TevSession.BeforeSend(const AStream: IevDualStream): IevDualStream;
begin
  if not (FState in [ssPerformingHandshake, ssWaitingForLogin]) then
  begin
    if FCompressionLevel > clNone then
    begin
      Result := TevDualStreamHolder.Create;
      DeflateStream(AStream.RealStream, Result.RealStream, FCompressionLevel);
    end
    else
      Result := AStream;

    if Assigned(FEncryptor) then
      Result := FEncryptor.Encrypt(Result);
  end
  else
    Result := AStream;

  Result.Position := 0;
end;

function TevSession.AfterReceive(const AStream: IevDualStream): IevDualStream;
var
  S: IevDualStream;
begin
  if FState <> ssPerformingHandshake then
  begin
    if Assigned(FEncryptor) then
      Result := FEncryptor.Decrypt(AStream)
    else
      Result := AStream;

    if FCompressionLevel > clNone then
    begin
      Result.Position := 0;
      S := TEvDualStreamHolder.Create;
      InflateStream(Result.RealStream, S.RealStream);
      Result := S;
    end;

    Result.Position := 0;
  end
  else
    Result := AStream;
end;

function TevSession.GetSessionID: TisGUID;
begin
  Result := GetName;
end;

procedure TevSession.SetSessionID(const AValue: TisGUID);
begin
  SetName(AValue);
end;

function TevSession.GetEncryptor: IisCryptoApiWrapper;
begin
  Result := FEncryptor;
end;

procedure TevSession.SendData(const AData: IevDualStream; const ADatagramHeader: IevDatagramHeader; out ASentBytes: Cardinal);
var
  T: IisStreamTransmitter;
  S: IEvDualStream;
  sHeader: String;
  DH: IevDatagramHeader;
begin
  if Assigned(ADatagramHeader) then
    DH := ADatagramHeader
  else
    DH := TevDatagram.GetDatagramHeader(AData);

  if DH.IsResponse then
    sHeader := 'R'
  else
    sHeader := 'M';

  T := GetTransmitter;
  try
    CheckCondition(Assigned(T), 'Session is not active', EevProtocol);
  except
    on E: Exception do
    begin
      FErrorMessage := E.Message;
      raise;
    end;
  end;
  AData.Position := 0;
  S := BeforeSend(AData);
  ASentBytes := S.Size;
  T.Send(GetSessionID, TRASPORT_PROTOCOL_SIGNATURE + sHeader, S);
end;

function TevSession.GetResponseDataFor(const ADatagramSequenceNbr: Int64; const AWaitTime: Cardinal;
  out AReceivedBytes, ATransmissionTime: Cardinal): IEvDualStream;
var
  ResponseHandler: IevDatagramResponseHandler;
  s: String;
begin
  Result := nil;
  s := IntToStr(ADatagramSequenceNbr); // DELPHI BUG !!! Assign to var first!!!  Aleksey
  ResponseHandler := FindChildByName(s) as IevDatagramResponseHandler;

  CheckCondition(Assigned(ResponseHandler), 'Cannot find request #' + s, EevProtocol);
  Result := ResponseHandler.Wait(AWaitTime);
  AReceivedBytes := ResponseHandler.ReceivedSize;
  ATransmissionTime := ResponseHandler.TransmissionTime;
end;

function TevSession.SendRequest(const ADatagram: IevDualStream; const ADatagramSequenceNbr: Int64;
  const ABlockingMode: Boolean; out ASentBytes, AReceivedBytes, ATransmissionTime: Cardinal): IevDualStream;
var
  ResponseHandler: IisNamedObject;
begin
  ResponseHandler := TevDatagramResponseHandler.Create(Self, True);
  ResponseHandler.Name := IntToStr(ADatagramSequenceNbr);

  SendData(ADatagram, nil, ASentBytes);

  if ABlockingMode then
    Result := GetResponseDataFor(ADatagramSequenceNbr, 0, AReceivedBytes, ATransmissionTime)
  else
  begin
    Result := nil;
    AReceivedBytes := 0;
    ATransmissionTime := 0;
  end;
end;

procedure TevSession.CleanUpResponseHandlers;
var
  i: Integer;
begin
  Lock;
  try
    for i := 0 to ChildCount - 1 do
      TevDatagramResponseHandler(GetChild(i).GetImplementation).SetResponse(nil, 0, 0);
    RemoveAllChildren;
  finally
    Unlock;
  end;
end;

function TevSession.GetAppID: String;
begin
  Result := FAppID;
end;

procedure TevSession.SetAppID(const AValue: String);
begin
  FAppID := AValue;
end;

function TevSession.GetMergingWith: IevSession;
begin
  Result := FMergingWith;
end;

procedure TevSession.SetMergingWith(const AValue: IevSession);
begin
  FMergingWith := AValue;
end;

function TevSession.SendRequest(const ADatagram: IevDatagram; const AMaxWaitTime: Integer): IevDatagram;
begin
  SendRequest(ADatagram, False);
  Result := GetResponseDatagramFor(ADatagram.Header.SequenceNbr, AMaxWaitTime);
  if not Assigned(Result) then
    raise EevRequestTimeOut.Create('Server has not responded on client request within timeout');
end;

procedure TevSession.DoOnConstruction;
begin
  inherited;
  InitLock;
  FDelayedData := TisList.Create;
  FErrorMessage := '';
end;

procedure TevSession.AddDelayedData(const ATransmitterQueueItem: IisStreamQueueItem);
begin
  FDelayedData.Add(ATransmitterQueueItem);
end;

procedure TevSession.SendDelayedData;
var
  i: Integer;
  T: IisStreamTransmitter;
begin
  T := GetTransmitter;
  if Assigned(T) and (GetState = ssActive) then
  begin
    FDelayedData.Lock;
    try
      for i := 0 to FDelayedData.Count - 1 do
      begin
        T.Send(GetSessionID,
          (FDelayedData[0] as IisStreamQueueItem).Header,
          (FDelayedData[0] as IisStreamQueueItem).Stream);
      end;
      FDelayedData.Clear;
    finally
      FDelayedData.Unlock;
    end;
  end;  
end;

function TevSession.KeepAlive: Boolean;
begin
  Result := FKeepAlive;
end;

procedure TevSession.SetKeepAlive(const AValue: Boolean);
begin
  FKeepAlive := AValue;
end;

function TevSession.DisconnectTime: TDateTime;
begin
  Result := FDisconnectTime;
end;

function TevSession.GetPassword: String;
begin
  Result := FPassword;
end;

procedure TevSession.SetPassword(const AValue: String);
begin
  FPassword := AValue;
end;

function TevSession.GetLastActivity: TDateTime;
begin
  Result := FLastActivity;
end;

procedure TevSession.SetLastActivity(const AValue: TDateTime);
begin
  FLastActivity := AValue;
end;

function TevSession.GetRemoteIPAddress: String;
var
  SesConn: IisTCPStreamAsyncConnection;
  Connections: IisList;
begin
  Connections := GetTransmitter.TCPCommunicator.GetConnectionsBySessionID(GetSessionID);
  if Connections.Count > 0 then
  begin
    SesConn := Connections[0] as IisTCPStreamAsyncConnection;
    Result := SesConn.GetRemoteIPAddress;
  end
  else
    Result := '';
end;

function TevSession.GetExtension: IInterface;
begin
  Lock;
  try
    Result := FExtension;
  finally
    Unlock;
  end;
end;

procedure TevSession.SetExtension(const AValue: IInterface);
begin
  Lock;
  try
    FExtension := AValue;
  finally
    Unlock;
  end;
end;

function TevSession.StateDescr: String;
const
  SessionStates: array [Low(TevSessionState)..High(TevSessionState)] of string = (
    'Inactive', 'Performing Handshake', 'Waiting For Login', 'Performing Login',
    'Active', 'Merging', 'Invalid');
begin
  Result := SessionStates[GetState];
end;

function TevSession.EncryptionTypeDscr: String;
begin
  Result := aEncrDescr[GetEncryptionType];
end;

function TevSession.CheckIfAlive: Boolean;
var
  D: IevDatagram;
begin
  Result := False;
  if not (GetState in [ssInactive, ssInvalid]) then
  begin
    D := TevDatagram.Create(METHOD_TRANSPORT_PING);
    D.Header.SessionID := GetSessionID;
    try
      D := SendRequest(D, 20000);
      Result := True;
    except
      // ignore any exceptions
    end;
  end;
end;

procedure TevSession.DoOnDestruction;
begin
  CleanUpResponseHandlers;
  inherited;
end;

function TevSession.GetErrorMessage: String;
begin
  Result := FErrorMessage;
end;

{ TevDatagramResponseHandler }

procedure TevDatagramResponseHandler.DoOnConstruction;
begin
  inherited;
  InitLock;
  FResponseReceivedEvent := CreateEvent(nil, True, False, nil);
end;

function TevDatagramResponseHandler.Wait(const AWaitTime: Cardinal = 0): IEvDualStream;
var
  Session: IevSession;
  Dispatcher: IisInterfacedObject;
  wt: Cardinal;
begin
  Result := nil;

  if AWaitTime = 0 then
    wt := EV_RESPONSE_TIMEOUT
  else
    wt := AWaitTime;

  while not CurrentThreadTaskTerminated and SessionIsAlive and not Assigned(Result) do
  begin
    WaitForSingleObject(FResponseReceivedEvent, wt);

    Session := (GetOwner as IevSession);
    if Assigned(Session) then
    begin
      Dispatcher := (Session as IisInterfacedObject).GetOwner;
      if Assigned(Dispatcher) then
        TevDatagramDispatcher(Dispatcher.GetImplementation).DoOnIdle;
    end;

    Lock;
    try
      Result := FResponseDatagram;
    finally
      Unlock;
    end;

    if AWaitTime <> 0 then
      break;
  end;

  if (AWaitTime = 0) or Assigned(Result) or not SessionIsAlive then
  begin
    SetOwner(nil);
    if not Assigned(Result) then
      raise EevSessionLost.Create('Client request was not successfully executed');
  end;
end;

function TevDatagramResponseHandler.ReceivedSize: Cardinal;
begin
  Result := FReceivedSize;
end;

function TevDatagramResponseHandler.SessionIsAlive: Boolean;
var
  Session: IevSession;
  Transmitter: IisTCPStreamTransmitter;
begin
  Result := False;
  Session := GetOwner as IevSession;
  if not Assigned(Session) then
    Exit;
  Transmitter := Session.Transmitter;
  if not Assigned(Transmitter) then
    Exit;

  Result := Assigned(Session)
            and Assigned(Transmitter)
            and Transmitter.IsActive;

  if Result then
  begin
    Result := Transmitter.GetTCPCommunicator.GetConnectionsBySessionID(Session.SessionID).Count > 0;
    if not Result and Session.KeepAlive then
      Result := SecondsBetween((Session as IevSessionEx).DisconnectTime, Now) < CLIENT_SESSION_KEEP_ALIVE_TIMEOUT;
  end;
end;

procedure TevDatagramResponseHandler.SetResponse(const AResponseDatagram: IevDualStream;
  const AReceivedSize: Cardinal; const ATransmissionTime: Cardinal);
begin
  Lock;
  try
    FReceivedSize := AReceivedSize;
    FTransmissionTime := ATransmissionTime;    
    if Assigned(AResponseDatagram) and Assigned(FResponseDatagram) then
      Assert(False);
    FResponseDatagram := AResponseDatagram;
  finally
    Unlock;
  end;

  SetEvent(FResponseReceivedEvent);
end;

function TevDatagramResponseHandler.TransmissionTime: Cardinal;
begin
  Result := FTransmissionTime;
end;


procedure TevDatagramResponseHandler.DoOnDestruction;
begin
  CloseHandle(FResponseReceivedEvent);
  inherited;
end;

{ TevCustomTCPClient }

function TevCustomTCPClient.CreateConnection(const AHost, APort, AUser, APassword: String;
  const ASessionID, AContextID: TisGUID; const ALocalIPAddr: String; const AAppID: String): TisGUID;
var
  ExistingSession: IevSession;
  Conn1: IisTCPStreamAsyncConnection;
  D: IevDatagram;
  bRestoringSession: Boolean;
  sUser, sPassword: String;


  procedure RestoreSessionState;
  begin
    OnConnectStatusChange('Restoring session...', 0);

    // ask server to send all delayed data
    D := TevDatagram.Create(METHOD_TRANSPORT_RESTORESESSION);
    D.Header.SessionID := ExistingSession.SessionID;
    D := ExistingSession.SendRequest(D);
    (ExistingSession as IevSessionEx).SendDelayedData;
  end;


  function AddSessionConnection(const AFirstConRestore: Boolean): IisTCPStreamAsyncConnection;
  const
    sTitle = 'Connecting to server'#13#13;
  var
    Session: IevSession;
    sIPAddress: String;
    EncrType, i: TevEncryptionType;
    s, sPort: String;
  begin
    // Establish connection first
    OnConnectStatusChange(sTitle + 'Establishing connection to ' + AHost, 0);

    try
      EncrType := FEncryptionType;
      s := APort;
      sPort := GetNextStrValue(s, ':');
      if s <> '' then
      begin
        for i := Low(aEncrDescr) to High(aEncrDescr) do
          if AnsiSameText(aEncrDescr[i], s) then
          begin
            EncrType := i;
            break;
          end;
      end;

      if EncrType = etSSL then
        FCommunicator.RegisterSSLHost(AHost, StrToInt(sPort), FSSLCertFile)
      else
        FCommunicator.UnRegisterSSLHost(AHost, StrToInt(sPort));

      Result := FCommunicator.CreateConnectionTo(AHost, StrToInt(sPort), ALocalIPAddr) as IisTCPStreamAsyncConnection;
      CheckCondition(Result.Connected, Result.GetLastError, EevConnection);
    except
      on E: Exception do
      begin
        sIPAddress := SocketLib.LookupHostAddr(AHost);
        if sIPAddress = '' then
          raise EevConnection.CreateDetailed('Server ' + AHost + ' not found, IP address cannot be resolved for ' + AHost, E.Message)
        else
          raise EevConnection.CreateDetailed(Format('Cannot connect to %s (%s:%s)', [AHost, sIPAddress, APort]),
            E.Message, True);
      end;
    end;

    try
       // Perform handshake
      OnConnectStatusChange(sTitle + 'Connection authentication...', 1);

      if AFirstConRestore or not Assigned(ExistingSession) and (ASessionID <> '') then
        Result.SessionID := ASessionID;

      Session := GetSession(Result.SessionID) as IevSession;
      Session.EncryptionType := EncrType;

      Session.State := ssPerformingHandshake;
      D := TevDatagram.Create(METHOD_TRANSPORT_HANDSHAKE);
      D.Header.SessionID := Session.SessionID;
      D.Params.AddValue('ConnectionID', Result.GetSocketID);

      if AAppID <> '' then
        s := AAppID
      else
        s := AppID;
      D.Params.AddValue('AppID', s);

      if not AFirstConRestore then
      begin
        D.Params.AddValue('EncryptionType', Ord(EncrType));
        D.Params.AddValue('CompressionLevel', FCompressionLevel);
        if EncrType = etProprietary then
          D.Params.AddValue('PublicKey', Session.Encryptor.GetLocalPublicKey);
      end;

      D := Session.SendRequest(D, 30000);

      if not AFirstConRestore then
      begin
        if (D.Params.Value['EncryptionType'] > etNone) or (EncrType = etNone) then
          EncrType := D.Params.Value['EncryptionType'];
        Session.EncryptionType := EncrType;
        Session.CompressionLevel := D.Params.Value['CompressionLevel'];

        if EncrType = etProprietary then
          Session.Encryptor.SetSessionKey(IInterface(D.Params.Value['SessionKey']) as IevDualStream);
      end;

      //Login
      OnConnectStatusChange(sTitle + 'Account authorization...', 2);

      Session.Login := sUser;
      Session.Password := sPassword;

      Session.State := ssPerformingLogin;
      D := TevDatagram.Create(METHOD_TRANSPORT_LOGIN);
      D.Header.SessionID := Session.SessionID;
      D.Header.User := Session.Login;
      D.Header.Password := Session.Password;
      D.Header.ContextID := AContextID;

      if FDispatcher.SessionTimeout <> CLIENT_SESSION_NEVER_KEEP_ALIVE then
        D.Params.AddValue('KeepAlive', True);

      D := Session.SendRequest(D);

      // Merge with existing session
      if Assigned(ExistingSession) and not AFirstConRestore then
      begin
        // Phase 1
        Session.State := ssMerging;
        Session.MergingWith := ExistingSession;
        D := TevDatagram.Create(METHOD_TRANSPORT_BEGINMERGESESSIONS);
        D.Header.SessionID := ExistingSession.SessionID;
        D.Params.AddValue('SessionID', Session.SessionID);
        D := ExistingSession.SendRequest(D);

        // Phase 2
        D := TevDatagram.Create(METHOD_TRANSPORT_ENDMERGESESSIONS);
        D.Header.SessionID := ExistingSession.SessionID;
        D.Params.AddValue('SessionID', Session.SessionID);
        D := ExistingSession.SendRequest(D);

        CheckCondition(ExistingSession.AddConnection(Result), 'Cannot attach session', EevProtocol);

        FDispatcher.DeleteSession(Session);
      end
      else
        Session.State := ssActive;
    except
      FCommunicator.Disconnect(Result);
      raise;
    end;
  end;

begin
  if ASessionID <> '' then
    ExistingSession := FindSession(ASessionID)
  else
    ExistingSession := nil;

  bRestoringSession := Assigned(ExistingSession) and (ExistingSession.State = ssInactive);

  if bRestoringSession then
  begin
    sUser := ExistingSession.Login;
    sPassword := ExistingSession.Password;
  end
  else
  begin
    sUser := AUser;
    sPassword := APassword;
  end;

  Conn1 := AddSessionConnection(bRestoringSession);
  Result := Conn1.SessionID;

  if bRestoringSession then
    RestoreSessionState
  else
  begin
    ExistingSession := FindSession(Result);
    (ExistingSession as IevSessionEx).SetKeepAlive(FDispatcher.SessionTimeout <> CLIENT_SESSION_NEVER_KEEP_ALIVE);
  end;
end;

procedure TevCustomTCPClient.DestroyConnection(const ASessionID: String);
var
  L: IisList;
begin
  L := FTransmitter.TCPCommunicator.GetConnectionsBySessionID(ASessionID);
  if L.Count > 0 then
    FCommunicator.Disconnect(L[L.Count - 1] as IisTCPSocket);
end;

procedure TevCustomTCPClient.Disconnect(const ASessionID: String);
var
  Session: IevSession;
  D: IevDatagram;
begin
  Session := FindSession(ASessionID);
  if Assigned(Session) then
  begin
    if Session.State = ssActive then
    begin
      D := TevDatagram.Create(METHOD_TRANSPORT_LOGOUT);
      D.Header.SessionID := ASessionID;
      try
        (Session as IevSessionEx).SetKeepAlive(False);
        Session.State := ssInactive;        
        D := Session.SendRequest(D, 10000);
      except
      end;
    end;
      
    FDispatcher.DeleteSession(Session);
  end;
end;

procedure TevCustomTCPClient.DoOnConstruction;
begin
  inherited;
  InitLock;
  FCommunicator := TisAsyncTCPStreamClient.Create;
  FTransmitter := TisTCPStreamTransmitter.Create;
  FDispatcher := CreateDispatcher;
  FTransmitter.TCPCommunicator := FCommunicator as IisTCPStreamCommunicator;
  FDispatcher.Transmitter := FTransmitter; // This assignment LOCKS UP Transmitter and Dispatcher
end;

procedure TevCustomTCPClient.DoOnDestruction;
begin
  FDispatcher.TerminateDispatch;
  FDispatcher.WaitDispatchToFinish;

  FDispatcher.Transmitter := nil; // This assignment unlocks Transmitter and Dispatcher
  inherited;
end;

function TevCustomTCPClient.FindSession(const ASessionID: TisGUID): IevSession;
begin
  Result := FDispatcher.FindSessionByID(ASessionID) as IevSession;
end;

function TevCustomTCPClient.GetCompressionLevel: TisCompressionLevel;
begin
  Result := FCompressionLevel;
end;

function TevCustomTCPClient.GetDatagramDispatcher: IevDatagramDispatcher;
begin
  Result := FDispatcher;
end;

function TevCustomTCPClient.GetEncryptionType: TevEncryptionType;
begin
  Result := FEncryptionType;
end;

function TevCustomTCPClient.GetSession(const ASessionID: TisGUID): IevSession;
begin
  Result := FindSession(ASessionID);
  if not Assigned(Result) then
    Result := FDispatcher.CreateSession(ASessionID);
end;

procedure TevCustomTCPClient.OnConnectStatusChange(const AText: String; const AProgress: Integer);
begin
end;

procedure TevCustomTCPClient.SetCompressionLevel(const AValue: TisCompressionLevel);
begin
  FCompressionLevel := AValue;
end;

procedure TevCustomTCPClient.SetEncryptionType(const AValue: TevEncryptionType);
begin
  FEncryptionType := AValue;
end;

{ TevCustomTCPServer }

procedure TevCustomTCPServer.DoOnConstruction;
begin
  inherited;
  InitLock;
  FCommunicator := TisAsyncTCPStreamServer.Create;
  FTransmitter := TisTCPStreamTransmitter.Create;
  FDispatcher := CreateDispatcher;
  FTransmitter.TCPCommunicator := FCommunicator as IisTCPStreamCommunicator;
  FDispatcher.Transmitter := FTransmitter;  // This assignment LOCKS UP Transmitter and Dispatcher
end;

procedure TevCustomTCPServer.DoOnDestruction;
begin
  FDispatcher.TerminateDispatch;
  FDispatcher.WaitDispatchToFinish;

  FDispatcher.Transmitter := nil; // This assignment unlocks Transmitter and Dispatcher
  inherited;
end;

function TevCustomTCPServer.GetActive: Boolean;
begin
  Result := FCommunicator.Active;
end;

function TevCustomTCPServer.GetDatagramDispatcher: IevDatagramDispatcher;
begin
  Result := FDispatcher;
end;

function TevCustomTCPServer.GetIPAddress: String;
begin
  Result := FCommunicator.IPAddress;
end;

function TevCustomTCPServer.GetPort: String;
begin
  Result := FCommunicator.Port;
end;

procedure TevCustomTCPServer.SetActive(const AValue: Boolean);
begin
  FCommunicator.Active := AValue;
end;

procedure TevCustomTCPServer.SetIPAddress(const AValue: String);
begin
  FCommunicator.IPAddress := AValue;
end;

procedure TevCustomTCPServer.SetPort(const AValue: String);
begin
  FCommunicator.Port := AValue;
end;

procedure TevCustomTCPServer.SetSSLTimeout(const Value: Integer);
begin
  FCommunicator.SSLTimeout := Value;
end;

{ TevCustomUDPMessenger }

procedure TevCustomUDPMessenger.Close(const ASocketID: TisGUID);
begin
  (FCommunicator as IisUDPStreamCommunicator).DestroySocket(ASocketID);
end;

procedure TevCustomUDPMessenger.DoOnConstruction;
begin
  inherited;
  InitLock;
  FCommunicator := TisAsyncUDPStreamPeer.Create;
  (FCommunicator as IisAsyncUDPPeer).ThreadPoolCapacity := 2;
  FTransmitter := TisUDPStreamTransmitter.Create;
  FDispatcher := CreateDispatcher;
  FTransmitter.UDPCommunicator := FCommunicator as IisUDPStreamCommunicator;
  FDispatcher.Transmitter := FTransmitter;  // This assignment LOCKS UP Transmitter and Dispatcher
end;


procedure TevCustomUDPMessenger.DoOnDestruction;
begin
  FDispatcher.Transmitter := nil; // This assignment unlocks Transmitter and Dispatcher
  inherited;
end;

function TevCustomUDPMessenger.GetMessageDispatcher: IevMessageDispatcher;
begin
  Result := FDispatcher;
end;

function TevCustomUDPMessenger.OpenSocket(const AHost: String;
  const ARemotePort, ALocalPort: Integer): TisGUID;
begin
  Result := FCommunicator.Open(AHost, ARemotePort, ALocalPort).GetSocketID;
end;

function TevCustomUDPMessenger.OpenSocketForBroadcast(const ARemotePort, ALocalPort: Integer): TisGUID;
begin
  Result := FCommunicator.OpenForBroadcast(ARemotePort, ALocalPort).GetSocketID;
end;

{ TevCustomDatagramDispatcher }

procedure TevCustomDatagramDispatcher.AddHandler(const AMethod: String; AHandler: TevDatagramHandler);
var
  i: Integer;
begin
  i := FDatagramHandlers.IndexOf(AMethod);
  if i = -1 then
    FDatagramHandlers.AddObject(AMethod, @AHandler)
  else
    FDatagramHandlers.Objects[i] := @AHandler;
end;

procedure TevCustomDatagramDispatcher.AfterDispatchDatagram(
  const ADatagramIn, ADatagramOut: IevDatagram);
begin
end;

procedure TevCustomDatagramDispatcher.BeforeDispatchDatagram(
  const ADatagramHeader: IevDatagramHeader);
begin
end;

procedure TevCustomDatagramDispatcher.BeforeQueueIncomingData(const ASessionID: TisGUID;
  const AHeader: String; const AStream: IEvDualStream;
   const ATransmissionTime: Cardinal; var Handled: Boolean);
begin
  if Copy(AHeader, Length(TRASPORT_PROTOCOL_SIGNATURE) + 1 , 1) = 'R' then  // it's a response data
  begin
    // All responses should not go in receiving queue, they get assagned to request handlers directly
    GlobalThreadManager.RunTask(HandleIncomingDataInThread, Self, MakeTaskParams([ASessionID, AStream, ATransmissionTime]), 'Dispatch data for session ' + ASessionID);
    Handled := True;
  end;
end;

function TevCustomDatagramDispatcher.CreateDatagram(const AStream: IEvDualStream): IevDatagram;
begin
  Result := TevDatagram.CreateFromStream(AStream);
end;

function TevCustomDatagramDispatcher.CreateErrorResponseFor(const ASourceDatagramHeader: IevDatagramHeader;
  const AException: Exception): IevDatagram;
var
  E: EisException;
begin
  if AException is EisException then
  begin
    EisException(AException).AddStackIfEmpty;
    Result := TevDatagram.CreateErrorResponseFor(ASourceDatagramHeader, EisException(AException));
  end

  else
  begin
    E := EisVCLException.Create(AException);
    try
      E.AddStackIfEmpty;
      Result := TevDatagram.CreateErrorResponseFor(ASourceDatagramHeader, E);
    finally
      E.Free;
    end;
  end;
end;

function TevCustomDatagramDispatcher.CreateResponseFor(
  const ASourceDatagramHeader: IevDatagramHeader): IevDatagram;
begin
  Result := TevDatagram.CreateResponseFor(ASourceDatagramHeader);
end;

procedure TevCustomDatagramDispatcher.DispatchDatagram(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  HandleDatagram(ADatagram, AResult);
end;

procedure TevCustomDatagramDispatcher.DoOnConstruction;
begin
  inherited;
  InitLock;
  FDatagramHandlers := TStringList.Create;
  FDatagramHandlers.Duplicates := dupError;
  FDatagramHandlers.Capacity :=  1000;
  FDatagramHandlers.Sorted := True;
  RegisterHandlers;
  FDatagramHandlers.Capacity := FDatagramHandlers.Count;

  FThreadManager := TThreadManager.Create('Datagram Dispatcher TM');
  FThreadManager.Capacity := HowManyProcessors * 4;
end;

function TevCustomDatagramDispatcher.GetThreadPoolCapacity: Integer;
begin
  Result := FThreadManager.Capacity;
end;

procedure TevCustomDatagramDispatcher.HandleDatagram(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  DatagramHandler: TMethod;
  Idx: Integer;
  DataSize: Int64;
  i: Integer;
  stm: IisStream;
begin
  Idx := FDatagramHandlers.IndexOf(ADatagram.Header.Method);
  CheckCondition(Idx >= 0, 'Unknown method ' + ADatagram.Header.Method, EevProtocol);
  DatagramHandler.Data := Self;
  DatagramHandler.Code := Pointer(FDatagramHandlers.Objects[Idx]);
  TevDatagramHandler(DatagramHandler)(ADatagram, AResult);
  DataSize := 0;
  if Assigned(AResult) then
  begin
    for i := 0 to AResult.Params.Count - 1 do
    begin
      if (VarType(AResult.Params.Values[i].Value) = varUnknown) and Supports(IInterface(TVarData(AResult.Params.Values[i].Value).VUnknown), IisStream) then
      begin
        stm := IInterface(AResult.Params.Values[i].Value) as IisStream;
        DataSize := DataSize + stm.Size;
        stm := nil;
        if DataSize > MaxInt then
          raise Exception.Create(ADatagram.Header.Method + ': Attempt to return data streams larger than 2GB.');
      end;
    end;
  end;  
end;

procedure TevCustomDatagramDispatcher.HandleIncomingDataInThread(const Params: TTaskParamList);
begin
end;

function TevCustomDatagramDispatcher.HandlerExists(const AMethod: String): Boolean;
begin
  Result := FDatagramHandlers.IndexOf(AMethod) <> -1;
end;

procedure TevCustomDatagramDispatcher.OnDisconnect(const ASessionID: TisGUID);
begin
end;

procedure TevCustomDatagramDispatcher.OnIncomingData(const ASessionID: TisGUID;
  const AHeader: String; const AStream: IEvDualStream; const ATransmissionTime: Cardinal);
begin
  FThreadManager.RunTask(HandleIncomingDataInThread, Self, MakeTaskParams([ASessionID, AStream, ATransmissionTime]), 'Dispatch data for session ' + ASessionID);
end;

procedure TevCustomDatagramDispatcher.OnIncomingHeader(const ASessionID: TisGUID; const AHeader: String;
  var Accept: Boolean);
begin
  Accept := StartsWith(AHeader, TRASPORT_PROTOCOL_SIGNATURE, True);
end;

procedure TevCustomDatagramDispatcher.OnDiscardItems(const AItems: IisList);
begin
end;

procedure TevCustomDatagramDispatcher.RegisterHandlers;
begin
end;

procedure TevCustomDatagramDispatcher.SetThreadPoolCapacity(
  const AValue: Integer);
begin
  FThreadManager.Capacity := AValue;
end;

procedure TevCustomDatagramDispatcher.TerminateDispatch;
begin
  FThreadManager.TerminateTask(0);
end;

procedure TevCustomDatagramDispatcher.WaitDispatchToFinish;
begin
  FThreadManager.WaitForTaskEnd(0);
end;

procedure TevCustomDatagramDispatcher.DoOnDestruction;
begin
  TerminateDispatch;
  WaitDispatchToFinish;
  FDatagramHandlers.Free;

  inherited;
end;

{ TevMessageDispatcher }

procedure TevMessageDispatcher.DoOnConstruction;
begin
  inherited;
  FThreadManager.Capacity := 1;
end;

function TevMessageDispatcher.GetTransmitter: IisUDPStreamTransmitter;
begin
  Result := FTransmitter;
end;

procedure TevMessageDispatcher.HandleIncomingData(const ASocketID: TisGUID; const AStream: IEvDualStream);
var
  Datagram, ResultDatagram: IevDatagram;
begin
  try
    Datagram := CreateDatagram(AStream);
  except
    exit;
  end;
  Datagram.Header.SessionID := ASocketID;
  DispatchDatagram(Datagram, ResultDatagram);

  if Assigned(ResultDatagram) then
    SendMessage(ASocketID, ResultDatagram);
end;

procedure TevMessageDispatcher.HandleIncomingDataInThread(const Params: TTaskParamList);
var
  SocketID: String;
  Stream: IEvDualStream;
begin
  SocketID := Params[0];
  Stream := IInterface(Params[1]) as IEvDualStream;
  HandleIncomingData(SocketID, Stream);
end;

procedure TevMessageDispatcher.SendMessage(const ASocketID: TisGUID; const ADatagram: IevDatagram);
var
  S: IEvDualStream;
begin
  S := TEvDualStreamHolder.Create;
  (ADatagram as IisInterfacedObject).WriteToStream(S);
  S.Position := 0;
  GetTransmitter.Send(ASocketID, '', S);
end;

procedure TevMessageDispatcher.SetTransmitter(const AValue: IisUDPStreamTransmitter);
begin
  if FTransmitter <> AValue then
  begin
    if Assigned(FTransmitter) then
      FTransmitter.Callbacks := nil;
    FTransmitter := AValue;
    if Assigned(FTransmitter) then
      FTransmitter.Callbacks := Self;
  end;
end;

initialization
  RegisterExceptions([EevTransport, EevConnection, EevProtocol, EevRequestTimeOut,
                      EevSessionLost]);

end.
