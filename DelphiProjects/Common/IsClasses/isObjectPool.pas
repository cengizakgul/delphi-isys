unit isObjectPool;

interface

uses Classes, Windows, SysUtils, DateUtils, isBaseClasses, isThreadManager;

type
  IisObjectFromPool = interface
  ['{332230B3-27BA-4B0D-B1E0-E8C4C82CFA0E}']
    function GetName: String;
    function LastUsage: TDateTime;
    property Name: String read GetName;
  end;

  IisObjectPool = interface
  ['{CAFB92FC-3018-41BF-A747-803E6F55F6A4}']
    function  AcquireObject(const AObjectName: String): IisObjectFromPool;
    procedure ReturnObject(const AObject: IisObjectFromPool);
    function  RemoveObject(const AObjectName: String): IisObjectFromPool;     
    function  GetMaxPoolSize: Integer;
    procedure SetMaxPoolSize(const AValue: Integer);
    function  GetObjectExpirationTime: Integer;
    function  Count: Integer;
    procedure Lock;
    procedure UnLock;
    procedure Clear;    
    procedure SetObjectExpirationTime(const AValue: Integer);
    property  ObjectExpirationTime: Integer read GetObjectExpirationTime write SetObjectExpirationTime;
    property  MaxPoolSize: Integer read GetMaxPoolSize write SetMaxPoolSize;
  end;

  TisObjectFromPool = class(TisNamedObject, IisObjectFromPool)
  private
    FLastUsage: TDateTime;
  protected
    function  LastUsage: TDateTime;
    procedure OnAcquire; virtual;
    procedure OnReturn; virtual;
  public
    constructor Create(const AName: String); virtual;
  end;

  TisObjectFromPoolClass = class of TisObjectFromPool;

  TisObjectPool = class(TisInterfacedObject, IisObjectPool)
  private
    FName: String;
    FObjectExpirationTime: Integer;
    FMaxPoolSize: Integer;
    FObjectClass: TisObjectFromPoolClass;
    FList: IisStringList;
    FGarbageCollectorTaskID: TTaskID;
    FCheckPoolNowEvent: THandle;
    procedure GarbageCollectorProc(const Params: TTaskParamList);
    function  GetObjectByIndex(const AIndex: Integer): IisObjectFromPool;
  protected
    procedure  DoOnConstruction; override;
    function   CreateObject(const AObjectName: String): IisObjectFromPool; virtual;
    procedure  BeforeCheckPool; virtual;
    procedure  AfterCheckPool; virtual;
    function   AcquireObject(const AObjectName: String): IisObjectFromPool; virtual;
    procedure  ReturnObject(const AObject: IisObjectFromPool); virtual;
    function   RemoveObject(const AObjectName: String): IisObjectFromPool;
    function   GetMaxPoolSize: Integer;
    procedure  SetMaxPoolSize(const AValue: Integer);
    function   GetObjectExpirationTime: Integer;
    procedure  SetObjectExpirationTime(const AValue: Integer);
    function   Count: Integer;
    procedure  Clear;
    procedure  Lock;
    procedure  UnLock;
  public
    constructor Create(const AName: String; const AObjectClass: TisObjectFromPoolClass);
    destructor  Destroy; override;
  end;

implementation

uses SyncObjs;

type
  TisObjListByUsage = class(TisList)
  protected
    function  CompareItems(const Item1, Item2: IInterface): Integer; override;
  end;


{ TisObjectFromPool }

constructor TisObjectFromPool.Create(const AName: String);
begin
  inherited Create;
  SetName(AName);
end;

function TisObjectFromPool.LastUsage: TDateTime;
begin
  Result := FLastUsage;
end;

procedure TisObjectFromPool.OnAcquire;
begin
end;

procedure TisObjectFromPool.OnReturn;
begin
end;

{ TisObjectPool }

function TisObjectPool.AcquireObject(const AObjectName: String): IisObjectFromPool;
begin
  Result := RemoveObject(AObjectName);

  if not Assigned(Result) then
    Result := CreateObject(AObjectName);  // create new object if it's not found

  TisObjectFromPool((Result as IisInterfacedObject).GetImplementation).OnAcquire;
end;

procedure TisObjectPool.AfterCheckPool;
begin
end;

procedure TisObjectPool.BeforeCheckPool;
begin
end;

procedure TisObjectPool.Clear;
begin
  FList.Clear;
end;

function TisObjectPool.Count: Integer;
begin
  Result := FList.Count;
end;

constructor TisObjectPool.Create(const AName: String; const AObjectClass: TisObjectFromPoolClass);
begin
  FName := AName;
  FObjectClass := AObjectClass;
  inherited Create;
end;

destructor TisObjectPool.Destroy;
begin
  if GlobalThreadManagerIsActive then
  begin
    GlobalThreadManager.TerminateTask(FGarbageCollectorTaskID);
    SetEvent(FCheckPoolNowEvent);
    GlobalThreadManager.WaitForTaskEnd(FGarbageCollectorTaskID);
  end;  
  CloseHandle(FCheckPoolNowEvent);
  inherited;
end;

procedure TisObjectPool.DoOnConstruction;
begin
  inherited;
  FObjectExpirationTime := 1; // 1 min

  FList := TisStringList.CreateAsIndex(True);
  FList.Duplicates := dupAccept;

  FCheckPoolNowEvent := CreateEvent(nil, False, False, nil);

  FGarbageCollectorTaskID := GlobalThreadManager.RunTask(GarbageCollectorProc, Self, MakeTaskParams([]), FName, tpLowest);
end;

procedure TisObjectPool.GarbageCollectorProc(const Params: TTaskParamList);
var
  RestTime: Int64;

  // It has been done as a separate function because of Delphi compiler "feature"
  // Obj := nil doesn't invoke interfaced object destructor
  // Instead it holds the object until procedure has finished
  procedure CheckPool;
  var
    LastUsageList: IisList;
    ObjectsToDelete: IisList;
    Obj: IisObjectFromPool;
    i, j: Integer;
  begin
    LastUsageList := TisObjListByUsage.Create;

    FList.Lock;
    try
      for i := 0 to FList.Count - 1 do
      begin
        Obj := GetObjectByIndex(i);
        LastUsageList.Add(Obj);
      end;
      LastUsageList.Sort;

      // get rid of expired connections and overfilling
      RestTime := FObjectExpirationTime * 60 * 1000;
      for i := 0 to LastUsageList.Count - 1 do
      begin
        Obj := LastUsageList[i] as IisObjectFromPool;
        if (MinutesBetween(Now, Obj.LastUsage) >= FObjectExpirationTime) or
           (FMaxPoolSize > 0) and (FList.Count > FMaxPoolSize) then
        begin
          j := FList.IndexOfObject(Obj);
          FList.Delete(j);

          // in order ot avoid object deletion in critical section do this:
          if not Assigned(ObjectsToDelete) then
            ObjectsToDelete := TisList.Create;
          ObjectsToDelete.Add(Obj);
        end
        else
        begin
          RestTime := MilliSecondsBetween(Now, Obj.LastUsage);
          break;
        end;

        AbortIfCurrentThreadTaskTerminated;
      end;

      Obj := nil;
      LastUsageList.Clear;

    finally
      FList.UnLock;
    end;

    // Here is implicit expired object deletion
    ObjectsToDelete := nil;
  end;

begin
  // Works in dedicated thread
  while not CurrentThreadTaskTerminated do
  begin
    BeforeCheckPool;
    CheckPool;
    AfterCheckPool;
    Snooze(RestTime, FCheckPoolNowEvent);
  end;
end;

function TisObjectPool.GetMaxPoolSize: Integer;
begin
  Result := FMaxPoolSize;
end;

function TisObjectPool.GetObjectByIndex(const AIndex: Integer): IisObjectFromPool;
begin
  Result := FList.Objects[AIndex] as IisObjectFromPool;
end;

function TisObjectPool.CreateObject(const AObjectName: String): IisObjectFromPool;
begin
  Result := FObjectClass.Create(AObjectName);
end;

function TisObjectPool.GetObjectExpirationTime: Integer;
begin
  Result := FObjectExpirationTime;
end;

procedure TisObjectPool.Lock;
begin
  FList.Lock;
end;

function TisObjectPool.RemoveObject(const AObjectName: String): IisObjectFromPool;
var
  i: Integer;
begin
  Result := nil;
  
  FList.Lock;
  try
    i := FList.IndexOf(AObjectName);
    if i <> -1 then
    begin
      // object is found
      Result := GetObjectByIndex(i);
      FList.Delete(i);
    end;
  finally
    FList.UnLock;
  end;
end;

procedure TisObjectPool.ReturnObject(const AObject: IisObjectFromPool);
begin
  FList.Lock;
  try
    TisObjectFromPool((AObject as IisInterfacedObject).GetImplementation).FLastUsage := Now;
    TisObjectFromPool((AObject as IisInterfacedObject).GetImplementation).OnReturn;
    FList.AddObject(AObject.Name, AObject);
    if FList.Count > FMaxPoolSize then
      SetEvent(FCheckPoolNowEvent);
  finally
    FList.UnLock;
  end;
end;

procedure TisObjectPool.SetMaxPoolSize(const AValue: Integer);
begin
  FMaxPoolSize := AValue;
end;

procedure TisObjectPool.SetObjectExpirationTime(const AValue: Integer);
begin
  FObjectExpirationTime := AValue;
end;

procedure TisObjectPool.UnLock;
begin
  FList.Unlock;
end;

{ TisObjListByUsage }

function TisObjListByUsage.CompareItems(const Item1, Item2: IInterface): Integer;
var
  Conn1, Conn2: IisObjectFromPool;
begin
  Conn1 := Item1 as IisObjectFromPool;
  Conn2 := Item2 as IisObjectFromPool;

  if Conn1.LastUsage > Conn2.LastUsage then
    Result := 1
  else if Conn1.LastUsage < Conn2.LastUsage then
    Result := -1
  else
    Result := 0;
end;

end.
