unit IsTypes;

interface

uses
  SysUtils, Messages;

type
  ISException = class(Exception);

  TisDynVariantArray = array of Variant;
  TisDynStringArray = array of String;
  TisDynIntegerArray = array of integer;

  TMonth = (mUnknown, mJanuary, mFebruary, mMarch, mApril, mMay, mJune, mJuly,
            mAugust, mSeptember, mOctober, mNovember, mDecember);

  TDayOfWeek = (dwUnknown, dwMonday, dwTuesday, dwWednesday, dwThursday, dwFriday, dwSaturday, dwSunday);

  TSetOfChar = set of Char;

  TisCommaDilimitedString = string; // Comma delimited string
  TisIniString = string;            // String in INI format (Value=Description) broken up by VK_RETURN

  TBool3 = (bFalse, bTrue, bUnknown);

  TisVersionInfo = record
                     Major: Integer;
                     Minor: Integer;
                     Patch: Integer;
                     Build: Integer;
                   end;

  TisDate = Integer;

const
  DayShortNames: array [dwMonday..dwSunday] of String = ('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
  DayLongNames: array [dwMonday..dwSunday] of String = ('Monday', 'Tuesday', 'Wednesday', 'Thursday',
                                                        'Friday', 'Saturday', 'Sunday');
  // Show main window message
  CM_BringAppToFront = WM_USER + 100;

implementation

end.
