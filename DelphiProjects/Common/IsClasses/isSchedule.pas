unit isSchedule;

interface

uses SysUtils, DateUtils, Math, isBaseClasses, isTypes, EvStreamUtils, isBasicUtils,
     isThreadManager;

const
  dnLastDayOfMonth = 32;

  scScheduleEvent        = 'ScheduleEvent';
  scScheduleEventOnce    = 'ScheduleEventOnce';
  scScheduleEventStartup = 'ScheduleEventStartup';
  scScheduleEventDaily   = 'ScheduleEventDaily';
  scScheduleEventWeekly  = 'ScheduleEventWeekly';
  scScheduleEventMonthly = 'ScheduleEventMonthly';
  scScheduleEventYearly  = 'ScheduleEventYearly';


type
  TDaysOfWeek = set of TDayOfWeek;
  TDayNumber = 0..dnLastDayOfMonth;
  TMonths = set of TMonth;
  TWeekOfMonth = (wmUnknown, wmFirst, wmSecond, wmThird, wmFourth, wmLast);
  TRepeatInterval = (riNone, riMinutes, riHours);
  TRepeatUntil = (ruTime, ruDuration);

  TRunSchedule = array of TDateTime;

  IisScheduleEvent = interface
  ['{A0360811-ACDF-4EA8-BE69-0EE22055B2EE}']
    function  GetEnabled: Boolean;
    procedure SetEnabled(const AValue: Boolean);
    function  GetDescription: String;
    procedure SetDescription(const AValue: String);
    function  GetStartTime: TDateTime;
    procedure SetStartTime(const AValue: TDateTime);
    function  GetStartDate: TDateTime;
    procedure SetStartDate(const AValue: TDateTime);
    function  GetEndDate: TDateTime;
    procedure SetEndDate(const AValue: TDateTime);
    function  GetTaskID: String;
    procedure SetTaskID(const ATaskID: String);
    function  GetTaskParams: IisListOfValues;
    function  GetLastRunDate: TDateTime;
    procedure SetLastRunDate(const AValue: TDateTime);
    function  GetRunIfMissed: Boolean;
    procedure SetRunIfMissed(const AValue: Boolean);
    function  GetRepeatInterval: TRepeatInterval;
    function  GetRepeatIntervalNbr: Integer;
    function  GetRepeatUntil: TRepeatUntil;
    function  GetRepeatUntilTime: TDateTime;
    procedure SetRepeatInterval(const AValue: TRepeatInterval);
    procedure SetRepeatIntervalNbr(const AValue: Integer);
    procedure SetRepeatUntil(const AValue: TRepeatUntil);
    procedure SetRepeatUntilTime(const AValue: TDateTime);
    function  IsTimeToRun(const ANow: TDateTime = 0): Boolean;
    function  GetRunScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule;
    function  GetClosestStartTimeTo(const ADate: TDateTime): TDateTime;
    function  GetSchedInfo: String;
    property  Enabled: Boolean read GetEnabled write SetEnabled;
    property  Description: String read GetDescription write SetDescription;
    property  StartTime: TDateTime read GetStartTime write SetStartTime;
    property  StartDate: TDateTime read GetStartDate write SetStartDate;
    property  EndDate: TDateTime read GetEndDate write SetEndDate;
    property  TaskID: String read GetTaskID write SetTaskID;
    property  TaskParams: IisListOfValues read GetTaskParams;
    property  LastRunDate: TDateTime read GetLastRunDate write SetLastRunDate;
    property  RunIfMissed: Boolean read GetRunIfMissed write SetRunIfMissed;
    property  RepeatInterval: TRepeatInterval read GetRepeatInterval write SetRepeatInterval;
    property  RepeatIntervalNbr: Integer read GetRepeatIntervalNbr write SetRepeatIntervalNbr;
    property  RepeatUntil: TRepeatUntil read GetRepeatUntil write SetRepeatUntil;
    property  RepeatUntilTime: TDateTime read GetRepeatUntilTime write SetRepeatUntilTime;
  end;


  IisScheduleEventOnce = interface(IisScheduleEvent)
  ['{BE6EC916-0FD4-46F0-BA36-379C43C24ED8}']
  end;


  IisScheduleEventStartup = interface(IisScheduleEvent)
  ['{40332FAA-4F41-45A0-92D5-E840982C861A}']
  end;

  
  IisScheduleEventDaily = interface(IisScheduleEvent)
  ['{0F4396E2-9D4E-4C9E-B1B2-9A4F59CCD4A1}']
    function   GetEveryNbrDay: Integer;
    procedure  SetEveryNbrDay(const AValue: Integer);
    property   EveryNbrDay: Integer read GetEveryNbrDay write SetEveryNbrDay;
  end;


  IisScheduleEventWeekly = interface(IisScheduleEvent)
  ['{D3CF8458-57D5-4110-B796-E531329F65E1}']
    function   GetEveryNbrWeek: Integer;
    procedure  SetEveryNbrWeek(const AValue: Integer);
    function   GetRunDays: TDaysOfWeek;
    procedure  SetRunDays(const AValue: TDaysOfWeek);
    property   EveryNbrWeek: Integer read GetEveryNbrWeek write SetEveryNbrWeek;
    property   RunDays: TDaysOfWeek read GetRunDays write SetRunDays;
  end;


  IisScheduleEventMonthly = interface(IisScheduleEvent)
  ['{43CB1441-049B-443F-898D-79743EB50440}']
    function   GetDayOfMonth: TDayNumber;
    procedure  SetDayOfMonth(const AValue: TDayNumber);
    function   GetWeekOfMonth: TWeekOfMonth;
    procedure  SetWeekOfMonth(const AValue: TWeekOfMonth);
    function   GetDayOfWeek: TDayOfWeek;
    procedure  SetDayOfWeek(const AValue: TDayOfWeek);
    function   GetMonths: TMonths;
    procedure  SetMonths(const AValue: TMonths);
    property   DayOfMonth: TDayNumber read GetDayOfMonth write SetDayOfMonth;
    property   WeekOfMonth: TWeekOfMonth read GetWeekOfMonth write SetWeekOfMonth;
    property   DayOfWeek: TDayOfWeek read GetDayOfWeek write SetDayOfWeek;
    property   Months: TMonths read GetMonths write SetMonths;
  end;


  IisScheduleEventYearly = interface(IisScheduleEvent)
  ['{441E5FED-1226-4441-A7D3-8D8123945873}']
    function   GetEveryNbrYear: Integer;
    procedure  SetEveryNbrYear(const AValue: Integer);
    function   GetDayOfMonth: TDayNumber;
    procedure  SetDayOfMonth(const AValue: TDayNumber);
    function   GetWeekOfMonth: TWeekOfMonth;
    procedure  SetWeekOfMonth(const AValue: TWeekOfMonth);
    function   GetDayOfWeek: TDayOfWeek;
    procedure  SetDayOfWeek(const AValue: TDayOfWeek);
    function   GetMonth: TMonth;
    procedure  SetMonth(const AValue: TMonth);
    property   EveryNbrYear: Integer read GetEveryNbrYear write SetEveryNbrYear;
    property   DayOfMonth: TDayNumber read GetDayOfMonth write SetDayOfMonth;
    property   WeekOfMonth: TWeekOfMonth read GetWeekOfMonth write SetWeekOfMonth;
    property   DayOfWeek: TDayOfWeek read GetDayOfWeek write SetDayOfWeek;
    property   Month: TMonth read GetMonth write SetMonth;
  end;


  IisSchedule = interface
  ['{A5DF6EAC-34AE-4F21-8E48-412C19C08F98}']
    function  GetName: String;
    procedure Lock;
    procedure Unlock;    
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  Count: Integer;    
    procedure AddEvent(const AEvent: IisScheduleEvent);
    function  GetEventByIndex(const AIndex: Integer): IisScheduleEvent;
    procedure DeleteEvent(const AEvent: IisScheduleEvent);
    procedure Clear;
    property  Active: Boolean read GetActive write SetActive;
  end;


  TisScheduleEventClass = class of TisScheduleEvent;

  TisScheduleEvent = class(TisInterfacedObject, IisScheduleEvent)
  private
    FEnabled: Boolean;
    FDescription: String;
    FStartDate: TDateTime;
    FEndDate: TDateTime;
    FTaskID: String;
    FLastRunDate: TDateTime;
    FTaskParams: IisListOfValues;
    FRunIfMissed: Boolean;
    FRepeatInterval: TRepeatInterval;
    FRepeatIntervalNbr: Integer;
    FRepeatUntil: TRepeatUntil;
    FRepeatUntilTime: TDateTime;
    FEffectiveStartDate: TDateTime;
    function  GetCurrentTime: TDateTime;
    function  EffectiveStartDate: TDateTime;
    procedure InvalidateEffStartDate;
    function  CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule; virtual; abstract;
    function  SystemStartingUp: Boolean;
  protected
    procedure DoOnConstruction; override;
    function  GetRunScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule;
    function  GetClosestStartTimeTo(const ADate: TDateTime): TDateTime;
    function  GetEnabled: Boolean;
    procedure SetEnabled(const AValue: Boolean);
    function  GetDescription: String;
    procedure SetDescription(const AValue: String);
    function  GetStartTime: TDateTime;
    procedure SetStartTime(const AValue: TDateTime);
    function  GetStartDate: TDateTime;
    procedure SetStartDate(const AValue: TDateTime);
    function  GetEndDate: TDateTime;
    procedure SetEndDate(const AValue: TDateTime);
    function  GetTaskID: String;
    procedure SetTaskID(const ATaskID: String);
    function  GetTaskParams: IisListOfValues;
    function  GetLastRunDate: TDateTime;
    procedure SetLastRunDate(const AValue: TDateTime);
    function  GetRunIfMissed: Boolean;
    procedure SetRunIfMissed(const AValue: Boolean);
    function  GetRepeatInterval: TRepeatInterval;
    procedure SetRepeatInterval(const AValue: TRepeatInterval);
    function  GetRepeatIntervalNbr: Integer;
    procedure SetRepeatIntervalNbr(const AValue: Integer);
    function  GetRepeatUntil: TRepeatUntil;
    procedure SetRepeatUntil(const AValue: TRepeatUntil);
    function  GetRepeatUntilTime: TDateTime;
    procedure SetRepeatUntilTime(const AValue: TDateTime);
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  IsTimeToRun(const ANow: TDateTime = 0): Boolean;
    function  GetSchedInfo: String; virtual;
  public
    class function  GetTypeID: String; override;
  end;


  TisScheduleEventOnce = class(TisScheduleEvent, IisScheduleEventOnce)
  private
    function  CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule; override;
  protected
    function  GetSchedInfo: String; override;
  public
    class function GetTypeID: String; override;
  end;


  TisScheduleEventStartup = class(TisScheduleEvent, IisScheduleEventStartup)
  private
    function CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule; override;
  protected
    procedure DoOnConstruction; override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetSchedInfo: String; override;
  public
    class function GetTypeID: String; override;
  end;


  TisScheduleEventDaily = class(TisScheduleEvent, IisScheduleEventDaily)
  private
    FEveryNbrDay: Integer;
    function  CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule; override;
  protected
    procedure DoOnConstruction; override;
    function  GetEveryNbrDay: Integer;
    procedure SetEveryNbrDay(const AValue: Integer);
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetSchedInfo: String; override;
  public
    class function  GetTypeID: String; override;
  end;


  TisScheduleEventWeekly = class(TisScheduleEvent, IisScheduleEventWeekly)
  private
    FEveryNbrWeek: Integer;
    FRunDays: TDaysOfWeek;
    function  CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule; override;
  protected
    procedure DoOnConstruction; override;
    function  GetEveryNbrWeek: Integer;
    procedure SetEveryNbrWeek(const AValue: Integer);
    function  GetRunDays: TDaysOfWeek;
    procedure SetRunDays(const AValue: TDaysOfWeek);
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetSchedInfo: String; override;
  public
    class function  GetTypeID: String; override;
  end;


  TisScheduleEventMonthly = class(TisScheduleEvent, IisScheduleEventMonthly)
  private
    FDayOfMonth: TDayNumber;
    FWeekOfMonth: TWeekOfMonth;
    FDayOfWeek: TDayOfWeek;
    FMonths: TMonths;
    function  CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule; override;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetDayOfMonth: TDayNumber;
    procedure SetDayOfMonth(const AValue: TDayNumber);
    function  GetWeekOfMonth: TWeekOfMonth;
    procedure SetWeekOfMonth(const AValue: TWeekOfMonth);
    function  GetDayOfWeek: TDayOfWeek;
    procedure SetDayOfWeek(const AValue: TDayOfWeek);
    function  GetMonths: TMonths;
    procedure SetMonths(const AValue: TMonths);
    function  GetSchedInfo: String; override;
  public
    class function GetTypeID: String; override;
  end;


  TisScheduleEventYearly = class(TisScheduleEvent, IisScheduleEventYearly)
  private
    FEveryNbrYear: Integer;
    FDayOfMonth: TDayNumber;
    FWeekOfMonth: TWeekOfMonth;
    FDayOfWeek: TDayOfWeek;
    FMonth: TMonth;
    function  CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule; override;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetEveryNbrYear: Integer;
    procedure SetEveryNbrYear(const AValue: Integer);
    function  GetDayOfMonth: TDayNumber;
    procedure SetDayOfMonth(const AValue: TDayNumber);
    function  GetWeekOfMonth: TWeekOfMonth;
    procedure SetWeekOfMonth(const AValue: TWeekOfMonth);
    function  GetDayOfWeek: TDayOfWeek;
    procedure SetDayOfWeek(const AValue: TDayOfWeek);
    function  GetMonth: TMonth;
    procedure SetMonth(const AValue: TMonth);
    function  GetSchedInfo: String; override;
  public
    class function GetTypeID: String; override;
  end;


  TisSchedule = class(TisCollection, IisSchedule)
  private
    FStartingUp: Boolean;
    FSchedulingThreadTasks: TTaskID;
    procedure SchedulingThread(const Params: TTaskParamList);
    procedure Start;
    procedure Stop;
  protected
    procedure DoOnDestruction; override;
    procedure DoOnStart; virtual;
    function  BeforeCheckingTasks: Boolean; virtual;
    procedure DoOnStop; virtual;
    procedure RunTaskFor(const AEvent: IisScheduleEvent); virtual;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    procedure AddEvent(const AEvent: IisScheduleEvent);
    function  GetEventByIndex(const AIndex: Integer): IisScheduleEvent;
    procedure DeleteEvent(const AEvent: IisScheduleEvent);
    procedure Clear;
    function  Count: Integer;
  public
    constructor Create(const AName: String); reintroduce;
  end;



implementation

var FSystemStartupTime: TDateTime;


function TruncToMinutes(const ADateTime: TDateTime): TDateTime;
begin
  Result := RecodeSecond(RecodeMilliSecond(ADateTime, 0), 0);
end;


{ TisScheduleEvent }

function TisScheduleEvent.GetDescription: String;
begin
  Result := FDescription;
end;

function TisScheduleEvent.GetTaskID: String;
begin
  Result := FTaskID;
end;


function TisScheduleEvent.GetTaskParams: IisListOfValues;
begin
  Result := FTaskParams;
end;

function TisScheduleEvent.GetStartTime: TDateTime;
begin
  Result := TimeOf(FStartDate);
end;


procedure TisScheduleEvent.SetDescription(const AValue: String);
begin
  FDescription := AValue;
end;

procedure TisScheduleEvent.SetTaskID(const ATaskID: String);
begin
  FTaskID := ATaskID;
end;

procedure TisScheduleEvent.SetStartTime(const AValue: TDateTime);
begin
  FStartDate := GetStartDate + TruncToMinutes(TimeOf(AValue));
  InvalidateEffStartDate;
end;

class function TisScheduleEvent.GetTypeID: String;
begin
  Result := scScheduleEvent;
end;


procedure TisScheduleEvent.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FEnabled := AStream.ReadBoolean;
  FDescription := AStream.ReadShortString;
  FTaskID := AStream.ReadShortString;
  (FTaskParams as IisInterfacedObject).ReadFromStream(AStream);
  FStartDate := AStream.ReadDouble;
  FEndDate := AStream.ReadDouble;
  FLastRunDate := AStream.ReadDouble;
  FRunIfMissed := AStream.ReadBoolean;
  FRepeatInterval := TRepeatInterval(AStream.ReadByte);
  FRepeatIntervalNbr := AStream.ReadInteger;
  FRepeatUntil := TRepeatUntil(AStream.ReadByte);
  FRepeatUntilTime := AStream.ReadDouble;

  InvalidateEffStartDate;
end;

procedure TisScheduleEvent.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteBoolean(FEnabled);
  AStream.WriteShortString(FDescription);
  AStream.WriteShortString(FTaskID);
  (FTaskParams as IisInterfacedObject).WriteToStream(AStream);
  AStream.WriteDouble(FStartDate);
  AStream.WriteDouble(FEndDate);
  AStream.WriteDouble(FLastRunDate);
  AStream.WriteBoolean(FRunIfMissed);
  AStream.WriteByte(Ord(FRepeatInterval));
  AStream.WriteInteger(FRepeatIntervalNbr);
  AStream.WriteByte(Ord(FRepeatUntil));
  AStream.WriteDouble(FRepeatUntilTime);
end;

function TisScheduleEvent.IsTimeToRun(const ANow: TDateTime): Boolean;
var
  CurrentTime, ClosestSchedTime: TDateTime;
begin
  Result := False;
  if FEnabled then
  begin
    if DateIsEmpty(ANow) then
      CurrentTime := GetCurrentTime
    else
      CurrentTime := TruncToMinutes(ANow);

    ClosestSchedTime := GetClosestStartTimeTo(CurrentTime);

    if not DateIsEmpty(ClosestSchedTime) then
      if SystemStartingUp then
        Result := FRunIfMissed and DateTimeCompare(CurrentTime, coGreaterOrEqual, ClosestSchedTime)
      else
        Result := DateTimeCompare(CurrentTime, coGreaterOrEqual, ClosestSchedTime) and (SecondsBetween(CurrentTime, ClosestSchedTime) <= 60);
  end;
end;


function TisScheduleEvent.GetLastRunDate: TDateTime;
begin
  Result := FLastRunDate;
end;

procedure TisScheduleEvent.SetLastRunDate(const AValue: TDateTime);
begin
  FLastRunDate := AValue;
  InvalidateEffStartDate;
end;

function TisScheduleEvent.GetClosestStartTimeTo(const ADate: TDateTime): TDateTime;
var
  aSched: TRunSchedule;
  i: Integer;
begin
  if SystemStartingUp and FRunIfMissed then
  begin
    aSched := GetRunScheduleFor(FLastRunDate, ADate);

    if Length(aSched) > 0 then
    begin
      Result := aSched[High(aSched)];
      Exit;
    end;
  end;

  aSched := GetRunScheduleFor(ADate, IncYear(ADate, 1));

  // Find out run date
  if Length(aSched) > 0 then
    Result := aSched[Low(aSched)]
  else
    Result := 0;

  for i := Low(aSched) to High(aSched) do
  begin
    if DateTimeCompare(aSched[i], coGreater, ADate) then
      break;
    Result := aSched[i];
  end;
end;

function TisScheduleEvent.GetCurrentTime: TDateTime;
begin
  Result := TruncToMinutes(Now);
end;

function TisScheduleEvent.GetRunIfMissed: Boolean;
begin
  Result := FRunIfMissed;
end;

procedure TisScheduleEvent.SetRunIfMissed(const AValue: Boolean);
begin
  FRunIfMissed := AValue;
end;

function TisScheduleEvent.GetStartDate: TDateTime;
begin
  Result := DateOf(FStartDate);
end;

procedure TisScheduleEvent.SetStartDate(const AValue: TDateTime);
begin
  FStartDate := DateOf(AValue) + GetStartTime;
  InvalidateEffStartDate;  
end;

function TisScheduleEvent.GetEndDate: TDateTime;
begin
  Result := DateOf(FEndDate);
end;

procedure TisScheduleEvent.SetEndDate(const AValue: TDateTime);
begin
  FEndDate := DateOf(AValue);
  InvalidateEffStartDate;
end;

function TisScheduleEvent.GetRepeatInterval: TRepeatInterval;
begin
  Result := FRepeatInterval;
end;

function TisScheduleEvent.GetRepeatIntervalNbr: Integer;
begin
  Result := FRepeatIntervalNbr;
end;

function TisScheduleEvent.GetRepeatUntil: TRepeatUntil;
begin
  Result := FRepeatUntil;
end;

function TisScheduleEvent.GetRepeatUntilTime: TDateTime;
begin
  Result := FRepeatUntilTime;
end;

procedure TisScheduleEvent.SetRepeatInterval(const AValue: TRepeatInterval);
begin
  FRepeatInterval := AValue;
  InvalidateEffStartDate;
end;

procedure TisScheduleEvent.SetRepeatIntervalNbr(const AValue: Integer);
begin
  FRepeatIntervalNbr := AValue;
  InvalidateEffStartDate;
end;

procedure TisScheduleEvent.SetRepeatUntil(const AValue: TRepeatUntil);
begin
  FRepeatUntil := AValue;
  InvalidateEffStartDate;
end;

procedure TisScheduleEvent.SetRepeatUntilTime(const AValue: TDateTime);
begin
  FRepeatUntilTime := TruncToMinutes(TimeOf(AValue));
  InvalidateEffStartDate;  
end;

function TisScheduleEvent.GetRunScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule;
var
  aSched: TRunSchedule;
  dFrom, dTo: TDateTime;

  procedure ApplyStartEndTime;
  begin
    dFrom := EffectiveStartDate;
    if DateIsEmpty(GetEndDate) then
      dTo := ADateTo
    else
      dTo := Min(ADateTo, GetEndDate);
  end;

  procedure ApplyRepeatTaskOptions;
  var
    aNewSched: TRunSchedule;
    i, j: Integer;
    dRunDate, dNextSchedDate: TDateTime;
    dStopTime: TDateTime;
    h, m, s, ms: Word;
  begin
    j := 0;
    for i := Low(aSched) to High(aSched) do
    begin
      dRunDate := aSched[i];
      if i < High(aSched) then
        dNextSchedDate := aSched[i + 1]
      else
        dNextSchedDate := IncYear(dRunDate, 1000); // very big date

      if FRepeatUntil = ruTime then
      begin
        dStopTime := DateOf(dRunDate) + FRepeatUntilTime;
        if DateTimeCompare(TimeOf(dRunDate), coGreater, FRepeatUntilTime) then
          dStopTime := IncDay(dStopTime, 1);
      end
      else
      begin
        DecodeTime(FRepeatUntilTime, h, m, s, ms);
        dStopTime := IncHour(dRunDate, h);
        dStopTime := IncMinute(dStopTime, m);
      end;

      dStopTime := Min(dStopTime, dTo);
      
      repeat
        if j > High(aNewSched) then
          SetLength(aNewSched, Length(aNewSched) + 100);

        aNewSched[j] := dRunDate;
        Inc(j);

        if FRepeatInterval = riMinutes then
          dRunDate := IncMinute(dRunDate, FRepeatIntervalNbr)
        else
          dRunDate := IncHour(dRunDate, FRepeatIntervalNbr);
      until DateTimeCompare(dRunDate, coGreaterOrEqual, dStopTime) or
            DateTimeCompare(dRunDate, coGreaterOrEqual, dNextSchedDate);
    end;
    SetLength(aNewSched, j);
    aSched := aNewSched;
  end;


  procedure TrimPassedEvents;
  var
    i: Integer;
    d: TDateTime;
  begin
    if DateTimeCompare(TruncToMinutes(ADateFrom), coGreater, TruncToMinutes(FLastRunDate)) then
      d := IncSecond(TruncToMinutes(ADateFrom), -1)
    else
      d := TruncToMinutes(FLastRunDate);

    for i := 0 to High(aSched) do
      if DateTimeCompare(aSched[i], coGreater, d) then
      begin
        Result := Copy(aSched, i, Length(aSched) - i);
        break;
      end;
  end;

begin
  SetLength(aSched, 0);

  // Filter schedule by StartDate and EndDate
  ApplyStartEndTime;

  if DateTimeCompare(dFrom, coLessOrEqual, dTo) then
  begin
    // Calc schedule
    aSched := CalcScheduleFor(dFrom, dTo);

    // Apply "repeat task" options
    if FRepeatInterval <> riNone then
      ApplyRepeatTaskOptions;

    // Delete events before Last Run and ADateFrom
    TrimPassedEvents;
  end;
end;

procedure TisScheduleEvent.DoOnConstruction;
begin
  inherited;
  FEnabled := True;
  FTaskParams := TisListOfValues.Create;
end;

function TisScheduleEvent.GetEnabled: Boolean;
begin
  Result := FEnabled;
end;

procedure TisScheduleEvent.SetEnabled(const AValue: Boolean);
begin
  FEnabled := AValue;
end;

function TisScheduleEvent.GetSchedInfo: String;
begin
  Result := '';
end;

function TisScheduleEvent.EffectiveStartDate: TDateTime;
var
  aSched: TRunSchedule;
begin
  if FEffectiveStartDate = 0 then
  begin
    // Calculate the closest passed schedule date
    aSched := CalcScheduleFor(GetStartDate + GetStartTime, IncDay(Date, -1));
    if Length(aSched) = 0 then
      FEffectiveStartDate := GetStartDate + GetStartTime
    else
      FEffectiveStartDate := aSched[High(aSched)];
  end;

  Result := FEffectiveStartDate;
end;

procedure TisScheduleEvent.InvalidateEffStartDate;
begin
  FEffectiveStartDate := 0;
end;

function TisScheduleEvent.SystemStartingUp: Boolean;
begin
  Result := (GetOwner <> nil) and TisSchedule(GetOwner.GetImplementation).FStartingUp
end;

{ TisSchedule }

procedure TisSchedule.AddEvent(const AEvent: IisScheduleEvent);
begin
  AddChild(AEvent as IisInterfacedObject);
end;

function TisSchedule.BeforeCheckingTasks: Boolean;
begin
  Result := True;
end;

procedure TisSchedule.Clear;
begin
  RemoveAllChildren;
end;

function TisSchedule.Count: Integer;
begin
  Result := ChildCount;
end;

constructor TisSchedule.Create(const AName: String);
begin
  inherited Create(nil, True);
  SetName(AName);
end;

procedure TisSchedule.DeleteEvent(const AEvent: IisScheduleEvent);
begin
  RemoveChild(AEvent as IisInterfacedObject);
end;

procedure TisSchedule.DoOnDestruction;
begin
  Stop;
  inherited;
end;

procedure TisSchedule.DoOnStart;
begin
end;

procedure TisSchedule.DoOnStop;
begin
end;

function TisSchedule.GetActive: Boolean;
begin
  Result := FSchedulingThreadTasks <> 0;
end;

function TisSchedule.GetEventByIndex(const AIndex: Integer): IisScheduleEvent;
begin
  Result := GetChild(AIndex) as IisScheduleEvent;
end;

procedure TisSchedule.RunTaskFor(const AEvent: IisScheduleEvent);
begin
end;

procedure TisSchedule.SchedulingThread(const Params: TTaskParamList);

  procedure CheckTasks(const ANow: TDateTime);
  var
    i: Integer;
  begin
    if BeforeCheckingTasks then
    begin
      Lock;
      try
        for i := 0 to Count - 1 do
          if GetEventByIndex(i).IsTimeToRun(ANow) then
            RunTaskFor(GetEventByIndex(i));
      finally
        Unlock;
      end;
    end;
  end;

var
  TimeCounter: TDateTime;
  SnoozeTime: Cardinal;
begin
  DoOnStart;
  try
    TimeCounter := TruncToMinutes(Now);

    FStartingUp := True;
    CheckTasks(TimeCounter);
    FStartingUp := False;

    while not CurrentThreadTaskTerminated do
    begin
      CheckTasks(TimeCounter);
      TimeCounter := IncMinute(TimeCounter, 1);

      while DateTimeCompare(TimeCounter, coGreater, Now) do
      begin
        SnoozeTime := MilliSecondsBetween(TimeCounter, Now);
        if SnoozeTime > 30000 then
          SnoozeTime := 30000;
        Snooze(SnoozeTime);
      end;
    end;
  finally
    DoOnStop;
    FSchedulingThreadTasks := 0;
  end;
end;

procedure TisSchedule.SetActive(const AValue: Boolean);
begin
  if AValue then
    Start
  else
    Stop;
end;

procedure TisSchedule.Start;
begin
  if not GetActive then
    FSchedulingThreadTasks := GlobalThreadManager.RunTask(SchedulingThread, Self, MakeTaskParams([]), GetName);
end;

procedure TisSchedule.Stop;
var
  SchTsk: TTaskID;
begin
  SchTsk := FSchedulingThreadTasks;
  if SchTsk <> 0 then
  begin
    GlobalThreadManager.TerminateTask(SchTsk);
    GlobalThreadManager.WaitForTaskEnd(SchTsk);
  end;
end;

{ TisScheduleEventOnce }

function TisScheduleEventOnce.CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule;
var
  dRunDate: TDateTime;
begin
  SetLength(Result, 0);

  dRunDate := GetStartDate + GetStartTime;

  if DateTimeCompare(dRunDate, coGreaterOrEqual, TruncToMinutes(ADateFrom)) and
     DateTimeCompare(dRunDate, coLessOrEqual, TruncToMinutes(ADateTo)) then
  begin
    SetLength(Result, 1);
    Result[0] := dRunDate;
  end;
end;

function TisScheduleEventOnce.GetSchedInfo: String;
begin
  Result := 'Once on ' + FormatDateTime('mm/dd/yyyy', GetStartDate) +
            ' at ' + FormatDateTime('h:nn AM/PM', GetStartTime);
end;

class function TisScheduleEventOnce.GetTypeID: String;
begin
  Result := scScheduleEventOnce;
end;


{ TisScheduleEventWeekly }

function TisScheduleEventWeekly.GetRunDays: TDaysOfWeek;
begin
  Result := FRunDays;
end;

class function TisScheduleEventWeekly.GetTypeID: String;
begin
  Result := scScheduleEventWeekly;
end;

procedure TisScheduleEventWeekly.SetRunDays(const AValue: TDaysOfWeek);
begin
  FRunDays := AValue;
  InvalidateEffStartDate;
end;


procedure TisScheduleEventWeekly.WriteSelfToStream(const AStream: IEvDualStream);
var
  D: TDayOfWeek;
  sD: String;
begin
  inherited;

  AStream.WriteInteger(FEveryNbrWeek);

  sD := '';
  for D := dwMonday to dwSunday do
    if D in FRunDays then
      AddStrValue(sD, DayShortNames[D], ',');

  AStream.WriteShortString(sD);
end;


procedure TisScheduleEventWeekly.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  D: TDayOfWeek;
  sD: String;
begin
  inherited;

  FEveryNbrWeek := AStream.ReadInteger;  

  FRunDays := [];
  sD := AStream.ReadShortString;
  for D := dwMonday to dwSunday do
    if Pos(DayShortNames[D], sD) > 0 then
      Include(FRunDays, D);
end;


function TisScheduleEventWeekly.GetEveryNbrWeek: Integer;
begin
  Result := FEveryNbrWeek;
end;

procedure TisScheduleEventWeekly.SetEveryNbrWeek(const AValue: Integer);
begin
  if AValue < 1 then
    FEveryNbrWeek := 1
  else
    FEveryNbrWeek := AValue;

  InvalidateEffStartDate;    
end;

function TisScheduleEventWeekly.CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule;
var
  dDateFrom: TDateTime;
  dRunDate:  TDateTime;
  i: Integer;
begin
  dDateFrom := ADateFrom;

  SetLength(Result, 0);
  dRunDate := dDateFrom;
  i := 0;
  while DateTimeCompare(dRunDate, coLessOrEqual, TruncToMinutes(ADateTo)) do
  begin
    dRunDate := DateOf(dRunDate) + GetStartTime;
    if DateTimeCompare(dRunDate, coGreaterOrEqual, dDateFrom) and
       DateTimeCompare(dRunDate, coGreaterOrEqual, TruncToMinutes(ADateFrom)) and
       (TDayOfWeek(DayOfTheWeek(dRunDate)) in FRunDays) then
    begin
      if i > High(Result) then
        SetLength(Result, Length(Result) + 100);
      Result[i] := dRunDate;
      Inc(i);
    end;

    dRunDate := IncDay(dRunDate, 1);
    if (TDayOfWeek(DayOfTheWeek(dRunDate)) = dwMonday) and (i > 0) then
      dRunDate := IncWeek(dRunDate, FEveryNbrWeek - 1);
  end;
  SetLength(Result, i);
end;

procedure TisScheduleEventWeekly.DoOnConstruction;
begin
  FEveryNbrWeek := 1;
  inherited;
end;

function TisScheduleEventWeekly.GetSchedInfo: String;
begin
  Result := 'Weekly at ' + FormatDateTime('h:nn AM/PM', GetStartTime);
end;

{ TisScheduleEventDaily }

function TisScheduleEventDaily.GetEveryNbrDay: Integer;
begin
  Result := FEveryNbrDay;
end;

class function TisScheduleEventDaily.GetTypeID: String;
begin
  Result := scScheduleEventDaily;
end;

procedure TisScheduleEventDaily.SetEveryNbrDay(const AValue: Integer);
begin
  if AValue < 1 then
    FEveryNbrDay := 1
  else
    FEveryNbrDay := AValue;
  InvalidateEffStartDate;    
end;

procedure TisScheduleEventDaily.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FEveryNbrDay);
end;

procedure TisScheduleEventDaily.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FEveryNbrDay := AStream.ReadInteger;
end;

function TisScheduleEventDaily.CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule;
var
  dDateFrom: TDateTime;
  dRunDate:  TDateTime;
  i: Integer;
begin
  dDateFrom := ADateFrom;

  SetLength(Result, 0);
  dRunDate := dDateFrom;
  i := 0;
  while DateTimeCompare(dRunDate, coLessOrEqual, TruncToMinutes(ADateTo)) do
  begin
    dRunDate := DateOf(dRunDate) + GetStartTime;
    if DateTimeCompare(dRunDate, coGreaterOrEqual, dDateFrom) and
       DateTimeCompare(dRunDate, coGreaterOrEqual, TruncToMinutes(ADateFrom)) then
    begin
      if i > High(Result) then
        SetLength(Result, Length(Result) + 100);
      Result[i] := dRunDate;
      Inc(i);
    end;

    if i = 0 then
      dRunDate := IncDay(dRunDate, 1)
    else
      dRunDate := IncDay(dRunDate, FEveryNbrDay);
  end;
  SetLength(Result, i);
end;

procedure TisScheduleEventDaily.DoOnConstruction;
begin
  FEveryNbrDay := 1;
  inherited;
end;


function TisScheduleEventDaily.GetSchedInfo: String;
begin
  Result := 'Daily at ' + FormatDateTime('h:nn AM/PM', GetStartTime);
end;

{ TisScheduleEventStartup }

function TisScheduleEventStartup.CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule;
begin
  SetLength(Result, 1);
  Result[0] := TruncToMinutes(FSystemStartupTime);
end;

procedure TisScheduleEventStartup.DoOnConstruction;
begin
  inherited;
  FRunIfMissed := True;
end;

function TisScheduleEventStartup.GetSchedInfo: String;
begin
  Result := 'At startup';
end;

class function TisScheduleEventStartup.GetTypeID: String;
begin
  Result := scScheduleEventStartup;
end;

procedure TisScheduleEventStartup.ReadSelfFromStream(
  const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FRunIfMissed := True;
end;

{ TisScheduleEventMonthly }

function TisScheduleEventMonthly.GetDayOfMonth: TDayNumber;
begin
  Result := FDayOfMonth;
end;

function TisScheduleEventMonthly.GetDayOfWeek: TDayOfWeek;
begin
  Result := FDayOfWeek;
end;

function TisScheduleEventMonthly.GetMonths: TMonths;
begin
  Result := FMonths;
end;

function TisScheduleEventMonthly.CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule;
var
  dDateFrom: TDateTime;
  dRunDate:  TDateTime;
  i: Integer;

  function AdjustRunDate(const ARunDate: TDateTime): TDateTime;
  var
    D, M, Y: Word;
    dNewDate: TDateTime;
  begin
    // Adjust RunDate in accordance with setup
    DecodeDate(ARunDate, Y, M, D);

    case FWeekOfMonth of
      wmFirst..wmFourth:
      begin
        Assert(FDayOfWeek > dwUnknown);
        // Find the first right week day
        D := 1;
        dNewDate := EncodeDate(Y, M, D);
        while TDayOfWeek(DayOfTheWeek(dNewDate)) <> FDayOfWeek do
        begin
          Inc(D);
          dNewDate := EncodeDate(Y, M, D);
        end;

        if FWeekOfMonth = wmSecond then
          dNewDate := IncWeek(dNewDate, 1)
        else if FWeekOfMonth = wmThird then
          dNewDate := IncWeek(dNewDate, 2)
        else if FWeekOfMonth = wmFourth then
          dNewDate := IncWeek(dNewDate, 3);
      end;

      wmLast:
      begin
        Assert(FDayOfWeek > dwUnknown);
        // Find the last right week day
        D := DaysInAMonth(Y, M);
        dNewDate := EncodeDate(Y, M, D);
        while TDayOfWeek(DayOfTheWeek(dNewDate)) <> FDayOfWeek do
        begin
          Dec(D);
          dNewDate := EncodeDate(Y, M, D);
        end;
      end;

    else
      D := FDayOfMonth;

      // Correct end of month
      while not IsValidDate(Y, M, D) do
        Dec(D);

      dNewDate := EncodeDate(Y, M, D);
    end;

    Result := dNewDate + TimeOf(ARunDate);
  end;

begin
  dDateFrom := ADateFrom;

  SetLength(Result, 0);
  dRunDate := AdjustRunDate(dDateFrom);
  i := 0;
  while DateTimeCompare(dRunDate, coLessOrEqual, TruncToMinutes(ADateTo)) do
  begin
    dRunDate := DateOf(dRunDate) + GetStartTime;
    if DateTimeCompare(dRunDate, coGreaterOrEqual, dDateFrom) and
       DateTimeCompare(dRunDate, coGreaterOrEqual, TruncToMinutes(ADateFrom)) and
       (TMonth(MonthOf(dRunDate)) in FMonths) then
    begin
      if i > High(Result) then
        SetLength(Result, Length(Result) + 100);
      Result[i] := dRunDate;
      Inc(i);
    end;

    dRunDate := AdjustRunDate(IncMonth(dRunDate, 1));
  end;
  SetLength(Result, i);
end;

class function TisScheduleEventMonthly.GetTypeID: String;
begin
  Result := scScheduleEventMonthly;
end;

function TisScheduleEventMonthly.GetWeekOfMonth: TWeekOfMonth;
begin
  Result := FWeekOfMonth;
end;

procedure TisScheduleEventMonthly.ReadSelfFromStream(
  const AStream: IEvDualStream; const ARevision: TisRevision);
var
  M: TMonth;
  MW: Word;
begin
  inherited;

  FDayOfMonth := AStream.ReadByte;
  FWeekOfMonth := TWeekOfMonth(AStream.ReadByte);
  FDayOfWeek := TDayOfWeek(AStream.ReadByte);

  MW := AStream.ReadWord;
  FMonths := [];
  for M := mJanuary to mDecember do
    if MW and (1 shl (Ord(M) - 1)) <> 0 then
      Include(FMonths, M);
end;

procedure TisScheduleEventMonthly.SetDayOfMonth(const AValue: TDayNumber);
begin
  FDayOfMonth := AValue;
  InvalidateEffStartDate;
end;

procedure TisScheduleEventMonthly.SetDayOfWeek(const AValue: TDayOfWeek);
begin
  FDayOfWeek := AValue;
  InvalidateEffStartDate;  
end;

procedure TisScheduleEventMonthly.SetMonths(const AValue: TMonths);
begin
  FMonths := AValue;
  InvalidateEffStartDate;  
end;

procedure TisScheduleEventMonthly.SetWeekOfMonth(const AValue: TWeekOfMonth);
begin
  FWeekOfMonth := AValue;
  InvalidateEffStartDate;  
end;

procedure TisScheduleEventMonthly.WriteSelfToStream(const AStream: IEvDualStream);
var
  M: TMonth;
  MW: Word;
begin
  inherited;

  AStream.WriteByte(FDayOfMonth);
  AStream.WriteByte(Ord(FWeekOfMonth));
  AStream.WriteByte(Ord(FDayOfWeek));

  MW := 0;
  for M := mJanuary to mDecember do
    if M in FMonths then
      MW := MW or (1 shl (Ord(M) - 1));

  AStream.WriteWord(MW);
end;

function TisScheduleEventMonthly.GetSchedInfo: String;
begin
  Result := 'Monthly at ' + FormatDateTime('h:nn AM/PM', GetStartTime);
end;

{ TisScheduleEventYearly }

function TisScheduleEventYearly.CalcScheduleFor(const ADateFrom, ADateTo: TDateTime): TRunSchedule;
var
  dDateFrom: TDateTime;
  dRunDate:  TDateTime;
  i: Integer;

  function AdjustRunDate(const ARunDate: TDateTime): TDateTime;
  var
    D, M, Y: Word;
    dNewDate: TDateTime;
  begin
    // Adjust RunDate in accordance with setup
    DecodeDate(ARunDate, Y, M, D);
    M := Ord(FMonth);

    case FWeekOfMonth of
      wmFirst..wmFourth:
      begin
        Assert(FDayOfWeek > dwUnknown);
        // Find the first right week day
        D := 1;
        dNewDate := EncodeDate(Y, M, D);
        while TDayOfWeek(DayOfTheWeek(dNewDate)) <> FDayOfWeek do
        begin
          Inc(D);
          dNewDate := EncodeDate(Y, M, D);
        end;

        if FWeekOfMonth = wmSecond then
          dNewDate := IncWeek(dNewDate, 1)
        else if FWeekOfMonth = wmThird then
          dNewDate := IncWeek(dNewDate, 2)
        else if FWeekOfMonth = wmFourth then
          dNewDate := IncWeek(dNewDate, 3);
      end;

      wmLast:
      begin
        Assert(FDayOfWeek > dwUnknown);
        // Find the last right week day
        D := DaysInAMonth(Y, M);
        dNewDate := EncodeDate(Y, M, D);
        while TDayOfWeek(DayOfTheWeek(dNewDate)) <> FDayOfWeek do
        begin
          Dec(D);
          dNewDate := EncodeDate(Y, M, D);
        end;
      end;

    else
      D := FDayOfMonth;

      // Correct end of month
      while not IsValidDate(Y, M, D) do
        Dec(D);

      dNewDate := EncodeDate(Y, M, D);
    end;

    Result := dNewDate + TimeOf(ARunDate);
  end;

begin
  dDateFrom := ADateFrom;

  SetLength(Result, 0);
  dRunDate := AdjustRunDate(dDateFrom);
  i := 0;
  while DateTimeCompare(dRunDate, coLessOrEqual, TruncToMinutes(ADateTo)) do
  begin
    dRunDate := DateOf(dRunDate) + GetStartTime;
    if DateTimeCompare(dRunDate, coGreaterOrEqual, dDateFrom) and
       DateTimeCompare(dRunDate, coGreaterOrEqual, TruncToMinutes(ADateFrom)) then
    begin
      if i > High(Result) then
        SetLength(Result, Length(Result) + 100);
      Result[i] := dRunDate;
      Inc(i);
    end;

    if i = 0 then
      dRunDate := IncYear(dRunDate, 1)
    else
      dRunDate := IncYear(dRunDate, FEveryNbrYear);
    dRunDate := AdjustRunDate(dRunDate);
  end;
  SetLength(Result, i);
end;

procedure TisScheduleEventYearly.DoOnConstruction;
begin
  inherited;
  FEveryNbrYear := 1;
  FMonth := mJanuary;
end;

function TisScheduleEventYearly.GetDayOfMonth: TDayNumber;
begin
  Result := FDayOfMonth;
end;

function TisScheduleEventYearly.GetDayOfWeek: TDayOfWeek;
begin
  Result := FDayOfWeek;
end;

function TisScheduleEventYearly.GetEveryNbrYear: Integer;
begin
  Result := FEveryNbrYear;
end;

function TisScheduleEventYearly.GetMonth: TMonth;
begin
  Result := FMonth;
end;

function TisScheduleEventYearly.GetSchedInfo: String;
begin
  Result := 'Yearly at ' + FormatDateTime('h:nn AM/PM', GetStartTime);
end;

class function TisScheduleEventYearly.GetTypeID: String;
begin
  Result := scScheduleEventYearly;
end;

function TisScheduleEventYearly.GetWeekOfMonth: TWeekOfMonth;
begin
  Result := FWeekOfMonth;
end;

procedure TisScheduleEventYearly.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FEveryNbrYear := AStream.ReadByte;
  FDayOfMonth := AStream.ReadByte;
  FWeekOfMonth := TWeekOfMonth(AStream.ReadByte);
  FDayOfWeek := TDayOfWeek(AStream.ReadByte);
  FMonth := TMonth(AStream.ReadByte);
end;

procedure TisScheduleEventYearly.SetDayOfMonth(const AValue: TDayNumber);
begin
  FDayOfMonth := AValue;
  InvalidateEffStartDate;  
end;

procedure TisScheduleEventYearly.SetDayOfWeek(const AValue: TDayOfWeek);
begin
  FDayOfWeek := AValue;
  InvalidateEffStartDate;  
end;

procedure TisScheduleEventYearly.SetEveryNbrYear(const AValue: Integer);
begin
  FEveryNbrYear := AValue;
  InvalidateEffStartDate;  
end;

procedure TisScheduleEventYearly.SetMonth(const AValue: TMonth);
begin
  FMonth := AValue;
  InvalidateEffStartDate;  
end;

procedure TisScheduleEventYearly.SetWeekOfMonth(const AValue: TWeekOfMonth);
begin
  FWeekOfMonth := AValue;
  InvalidateEffStartDate;  
end;

procedure TisScheduleEventYearly.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteByte(FEveryNbrYear);
  AStream.WriteByte(FDayOfMonth);
  AStream.WriteByte(Ord(FWeekOfMonth));
  AStream.WriteByte(Ord(FDayOfWeek));
  AStream.WriteByte(Ord(FMonth));
end;

initialization
  FSystemStartupTime := Now;
  ObjectFactory.Register([TisScheduleEvent, TisScheduleEventOnce, TisScheduleEventStartup,
                          TisScheduleEventDaily, TisScheduleEventWeekly, TisScheduleEventMonthly,
                          TisScheduleEventYearly]);
finalization
  SafeObjectFactoryUnRegister([TisScheduleEvent, TisScheduleEventOnce, TisScheduleEventStartup,
                            TisScheduleEventDaily, TisScheduleEventWeekly, TisScheduleEventMonthly,
                            TisScheduleEventYearly]);
end.
