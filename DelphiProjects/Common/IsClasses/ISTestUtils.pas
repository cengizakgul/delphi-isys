unit isTestUtils;

interface

uses
  windows, controls, isTestUtilsScript;

(* --gdy--
Notes:
 - menu recording works only for popup menus
   it can be extended to work with menu bars and window system menu but i don't feel we need it in Evolution
 - //!!fixme extra KeyUp after menu -- it isn't a problem because nobody cares about it
*)

const
  cTEFileExt = '.ets';
  cTEQueueFileExt = '.etq';

type
  IisTEScript2 = isTestUtilsScript.IisTEScript2;
  
  TisTstEngineState = (
    isTESStoped,
    isTESRecording,
    isTESPlayback,
    isTESPlaybackWaiting,  // was isTESPaused
    isTESRecordingPaused,
    isTESPlaybackPaused
//    isTESIntermediate
);

  TisTstEngineStates = set of TisTstEngineState;

  TisTEConfigRec = record
    StepDelay: DWORD;
    RealTyping: Boolean;
  end;

  TisTestScriptStatus = ( psSuccess, psUserStopped, psFailure );

  IisApplicationKnowledgeSource = interface
['{1B951C09-C5E7-44E3-A516-21DF90F84085}']
    function GetAnnotation( control: TControl ): string;
  end;

  IisTECallBackHolder = interface
  ['{4EA3CFDF-44F1-4CB3-B7E8-F90BB0D432AA}']
    procedure BeforeStartRecording(aScript: IisTEScript2);
    procedure AfterStopRecording(aScript: IisTEScript2);
    procedure BeforeStartPlayback(aScript: IisTEScript2);
    procedure AfterStopPlayback(aScript: IisTEScript2; status: TisTestScriptStatus);
  end;

  IisTestingEngineCP = interface
  ['{66EB1FF9-C72F-4FF8-A4D9-3287E1BFC749}']
    procedure Advise( const aSink: IisTECallBackHolder );
    procedure UnAdvise( const aSink: IisTECallBackHolder );
  end;

  IisTestingEngine = interface
  ['{2895C82B-288A-4704-88EA-4157641316B5}']
    procedure SetApplicationKnowledgeSource( ks: IisApplicationKnowledgeSource );
    procedure StartRecording(aScript: IisTEScript2);
    procedure StartPlayback(aScript: IisTEScript2);
    procedure Stop;
    procedure Resume;
    procedure Pause;

    function  GetState: TisTstEngineState;
    function  GetConfig: TisTEConfigRec;
    procedure SetConfig(const AConfigInfo: TisTEConfigRec);

    property  State: TisTstEngineState read GetState;
    property  Config: TisTEConfigRec read GetConfig write SetConfig;
  end;

  IisTestEngineSupportControl = interface
['{809EBD81-74AE-4FB8-9CF7-7F3D3B1AECC2}']
  end;

function TestingEngine: IisTestingEngine;  //also implements IisTestingEngineCP
function LoadTestScript(aFilename: string): IisTEScript2;

procedure LaunchEditor(aFilename: string);
procedure CloseEditor; //try to close editor

procedure EnqueueTestScript( afilename: string );
procedure LaunchQueueManager;
procedure CloseQueueManager; //try to close queue manager


function StrFromState( st: TisTstEngineState ): string;
procedure ODS(const msg: string); overload;
procedure ODS(const Fmt: string; const Args: array of const); overload;

function SaveAfterRecordingAndUnadvise: IisTECallBackHolder;

implementation

uses
  istestutilsimpl, IsTestEditorFrm, IsTestUtilsScriptQueue,
  isTestQueueManagerHolderFrm, sysutils;

type
  TisTestingEngineAutoSaveAfterRecording = class (TInterfacedObject, IisTECallBackHolder )
  private
    {IisTECallBackHolder}
    procedure BeforeStartRecording(aScript: IisTEScript2);
    procedure AfterStopRecording(aScript: IisTEScript2);
    procedure BeforeStartPlayback(aScript: IisTEScript2);
    procedure AfterStopPlayback(aScript: IisTEScript2; status: TisTestScriptStatus);
  end;

function SaveAfterRecordingAndUnadvise: IisTECallBackHolder;
begin
  Result := TisTestingEngineAutoSaveAfterRecording.Create;
end;

procedure ODS(const msg: string); overload;
begin
  OutputDebugString(pchar(msg));
end;

procedure ODS(const Fmt: string; const Args: array of const); overload;
begin
  ODS( Format( Fmt, Args ) );
end;

function TestingEngine: IisTestingEngine;
begin
  Result := istestutilsimpl.TestingEngine;
end;

function LoadTestScript(aFilename: string): IisTEScript2;
var
  ef: TisTestEditorHolder;
begin
  Result := nil;
  ef := FindEditorForm;
  if ef <> nil then
    if ef.Editor.Script.Filename = aFilename then
      Result := ef.Editor.Script;
  if Result = nil then
    Result := TestScriptFromFile(aFilename);
end;

procedure LaunchEditor(aFilename: string);
begin
  with GetEditorForm do
  begin
    Editor.OpenScript( TestScriptFromFile(aFilename) );
    Show;
  end
end;

procedure CloseEditor;
begin
  if FindEditorForm <> nil then
    FindEditorForm.Close;
end;

procedure LaunchQueueManager;
begin
  with GetQueueManagerForm do
    Show;
end;

procedure EnqueueTestScript( afilename: string );
begin
  TestScriptQueue.AddScript( afilename );
end;

procedure CloseQueueManager;
begin
  if FindQueueManagerForm <> nil then
    FindQueueManagerForm.Close;
end;

function StrFromState( st: TisTstEngineState ): string;
begin
  case TestingEngine.State of
    isTESRecordingPaused: Result := 'Paused Recording';
    isTESRecording: Result := 'Recording';
    isTESPlaybackPaused: Result := 'Paused Playback';
    isTESPlayback: Result := 'Playback';
    isTESPlaybackWaiting: Result := 'Playback (waiting)';
    isTESStoped: Result := 'Stopped';
  else
    Result := 'Unexpected state';
  end;
end;


{ TisTestingEngineAutoSaveAfterRecording }

procedure TisTestingEngineAutoSaveAfterRecording.AfterStopPlayback(
  aScript: IisTEScript2; status: TisTestScriptStatus);
begin
  (TestingEngine as IisTestingEngineCP).Unadvise( Self );
end;

procedure TisTestingEngineAutoSaveAfterRecording.AfterStopRecording(
  aScript: IisTEScript2);
begin
  (TestingEngine as IisTestingEngineCP).Unadvise( Self );
  aScript.Save;
end;

procedure TisTestingEngineAutoSaveAfterRecording.BeforeStartPlayback(
  aScript: IisTEScript2);
begin
end;

procedure TisTestingEngineAutoSaveAfterRecording.BeforeStartRecording(
  aScript: IisTEScript2);
begin
end;

end.



