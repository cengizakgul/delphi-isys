unit isAppIDs;

interface

type
  TisAppInfo = record
    AppID: String;
    Name:  String;
    Abbr:  String;
  end;

const
// Evolution

  EvoSAAppInfo:                   TisAppInfo = (AppID: 'IS_A934CE05081E4000A808F5752729A1B1';
                                                Name:  'Evolution Standalone';
                                                Abbr:  'EvoSA');

  EvoClientAppInfo:               TisAppInfo = (AppID: 'IS_9439435893684D6D8A961925D20EB6F5';
                                                Name:  'Evolution';
                                                Abbr:  'Evo');

  EvoDemoAppInfo:                 TisAppInfo = (AppID: 'IS_86555428487E44C6A77E255C6B4F928A';
                                                Name:  'Evolution Demo';
                                                Abbr:  'EvoDemo');

  EvoRWAdapterAppInfo:            TisAppInfo = (AppID: 'IS_6F9E6516B552416B963A93D383DF2346';
                                                Name:  'Evolution RW Adapter';
                                                Abbr:  'EvoRW');

  EvoDemoRWAdapterAppInfo:        TisAppInfo = (AppID: 'IS_FD40B7D41E7C4CC299DC434994C64209';
                                                Name:  'Evolution Demo RW Adapter';
                                                Abbr:  'EvoRWDemo');

  EvoSARWAdapterAppInfo:          TisAppInfo = (AppID: 'IS_042847D0174C4AF2B9BE7A048EFBEAEC';
                                                Name:  'Evolution RW Adapter Standalone';
                                                Abbr:  'EvoRWSA');

  EvoRequestBrokerAppInfo:        TisAppInfo = (AppID: 'IS_CEA9B09E1B12441ABA2268DCACB878A6';
                                                Name:  'Evolution Request Broker';
                                                Abbr:  'EvoRB');

  EvoRequestProcAppInfo:          TisAppInfo = (AppID: 'IS_6DD48F9929B544DE94B71C99D5CF089E';
                                                Name:  'Evolution Request Processor';
                                                Abbr:  'EvoRP');

  EvoRequestProcSAAppInfo:        TisAppInfo = (AppID: 'IS_1D87A7F6251B4061AA2608BD3A5B701D';
                                                Name:  'Evolution Request Processor Standalone';
                                                Abbr:  'EvoRPSA');

  EvoRemoteRelayAppInfo:          TisAppInfo = (AppID: 'IS_A21AE8CD629E4C07A7D6012548A90A20';
                                                Name:  'Evolution Remote Relay';
                                                Abbr:  'EvoRR');

  EvoAPIAdapterAppInfo:           TisAppInfo = (AppID: 'IS_FC548C06D4154089A24D26A2304AD23D';
                                                Name:  'Evolution API Adapter';
                                                Abbr:  'EvoAPI');

  EvoDeployMgrAppInfo:            TisAppInfo = (AppID: 'IS_834E89E76AA4461EA65B3D2700322C49';
                                                Name:  'Evolution Deployment Manager';
                                                Abbr:  'EvoDM');

  EvoMgmtConsoleAppInfo:          TisAppInfo = (AppID: 'IS_E2B441B7159B409794B393A694CD9E9C';
                                                Name:  'Evolution Management Console';
                                                Abbr:  'EvoMC');

  EvoBigBrotherAppInfo:           TisAppInfo = (AppID: 'IS_1ECB5855E4B04EA2B29A184661BF2B2B';
                                                Name:  'Evolution Big Brother';
                                                Abbr:  'EvoBB');

  EvoADRClientAppInfo:            TisAppInfo = (AppID: 'IS_63F4A42E1385449393F631B82C331A71';
                                                Name:  'Evolution ADR Client';
                                                Abbr:  'EvoAC');

  EvoADRServerAppInfo:            TisAppInfo = (AppID: 'IS_0EFF02EC835145BD85071E0DA118F069';
                                                Name:  'Evolution ADR Server';
                                                Abbr:  'EvoAS');

  EvoUtilISystemsAppInfo:         TisAppInfo = (AppID: 'IS_0264083D8F9F4791B22021886E628605';
                                                Name:  'Evolution ISystems Utility';
                                                Abbr:  'EvoISUtil');

  EvoUtilDBMaintenanceAppInfo:    TisAppInfo = (AppID: 'IS_715731317A4D4E3B8CB35964F5FD9FDC';
                                                Name:  'Evolution Database Maintenance Utility';
                                                Abbr:  'EvoDBPatch');

// .Net

  EvoDNetConnectorAppInfo:        TisAppInfo = (AppID: 'IS_5585F7E572DD49338E6BEE2890ADBC96';
                                                Name:  'Evolution .Net Connector';
                                                Abbr:  'EvoDNet');

  EvoDNetMapleConnectorAppInfo:   TisAppInfo = (AppID: 'IS_FCB558D3E1CC435E90DC630220F44F50';
                                                Name:  'Evolution .Net Maple Connector';
                                                Abbr:  'EvoDNetMaple');

// Java

  EvoJavaESSAppInfo:              TisAppInfo = (AppID: 'IS_AC0B66E3A95440C4B53F501A50D3E330';
                                                Name:  'Evolution ESS (Java)';
                                                Abbr:  'EvoJESS');

  EvoJavaWCAppInfo:               TisAppInfo = (AppID: 'IS_E36D0BC2DDBC4F74828165201BFC0483';
                                                Name:  'Evolution Web Client (Java)';
                                                Abbr:  'EvoJWC');

  EvoJavaMyHRAdminAppInfo:        TisAppInfo = (AppID: 'IS_EF52427DB14B49E5825E105FA2E7C365';
                                                Name:  'Evolution My HR Admin (Java)';
                                                Abbr:  'EvoJMHA');

// Report Writer

  RWEngineAppInfo:                TisAppInfo = (AppID: 'IS_228B3861B7F3416CA33FB8DA269A98FE';
                                                Name:  'Report Writer Engine';
                                                Abbr:  'RWE');

  RWDesignerAppInfo:              TisAppInfo = (AppID: 'IS_5F5B5E33B85E466B971F79F0781DB72A';
                                                Name:  'Report Writer Designer';
                                                Abbr:  'RWD');

  RWPreviewAppInfo:               TisAppInfo = (AppID: 'IS_68FCC83F8BA041EEB74822935AF06260';
                                                Name:  'Report Writer Preview';
                                                Abbr:  'RWP');

  RWCompareAppInfo:               TisAppInfo = (AppID: 'IS_780D06290C434B45966AA34B4B23FC87';
                                                Name:  'Report Writer Reports Compare';
                                                Abbr:  'RWC');


// Resolution

  ResolutionAppInfo:              TisAppInfo = (AppID: 'IS_298CD88A0BE6453CB941B23D4BE4633D';
                                                Name:  'Resolution';
                                                Abbr:  'Reso');

// Project Builder

  ProjectBuilderAppInfo:          TisAppInfo = (AppID: 'IS_6535C00332434A6E96AEC2969C298AEF';
                                                Name:  'Project Builder';
                                                Abbr:  'PB');

  ProjectBuilderDNetConnectorAppInfo:   TisAppInfo = (AppID: 'IS_F1453BE9B63E49438D51BB34292CC95B';
                                                Name:  'Project Builder .Net Connector';
                                                Abbr:  'PBDNet');




  function AppInfoByID(const AppID: String): TisAppInfo;

implementation

function AppInfoByID(const AppID: String): TisAppInfo;
begin
  if AppID = EvoSAAppInfo.AppID then
    Result := EvoSAAppInfo
  else if AppID = EvoClientAppInfo.AppID then
    Result := EvoClientAppInfo
  else if AppID = EvoDemoAppInfo.AppID then
    Result := EvoDemoAppInfo
  else if AppID = EvoRWAdapterAppInfo.AppID then
    Result := EvoRWAdapterAppInfo
  else if AppID = EvoDemoRWAdapterAppInfo.AppID then
    Result := EvoDemoRWAdapterAppInfo
  else if AppID = EvoSARWAdapterAppInfo.AppID then
    Result := EvoSARWAdapterAppInfo
  else if AppID = EvoRequestBrokerAppInfo.AppID then
    Result := EvoRequestBrokerAppInfo
  else if AppID = EvoRequestProcAppInfo.AppID then
    Result := EvoRequestProcAppInfo
  else if AppID = EvoRequestProcSAAppInfo.AppID then
    Result := EvoRequestProcSAAppInfo
  else if AppID = EvoRemoteRelayAppInfo.AppID then
    Result := EvoRemoteRelayAppInfo
  else if AppID = EvoAPIAdapterAppInfo.AppID then
    Result := EvoAPIAdapterAppInfo
  else if AppID = EvoDeployMgrAppInfo.AppID then
    Result := EvoDeployMgrAppInfo
  else if AppID = EvoMgmtConsoleAppInfo.AppID then
    Result := EvoMgmtConsoleAppInfo
  else if AppID = EvoBigBrotherAppInfo.AppID then
    Result := EvoBigBrotherAppInfo
  else if AppID = EvoADRClientAppInfo.AppID then
    Result := EvoADRClientAppInfo
  else if AppID = EvoADRServerAppInfo.AppID then
    Result := EvoADRServerAppInfo
  else if AppID = EvoUtilISystemsAppInfo.AppID then
    Result := EvoUtilISystemsAppInfo
  else if AppID = RWEngineAppInfo.AppID then
    Result := RWEngineAppInfo
  else if AppID = RWDesignerAppInfo.AppID then
    Result := RWDesignerAppInfo
  else if AppID = RWPreviewAppInfo.AppID then
    Result := RWPreviewAppInfo
  else if AppID = RWCompareAppInfo.AppID then
    Result := RWCompareAppInfo
  else if AppID = ResolutionAppInfo.AppID then
    Result := ResolutionAppInfo
  else if AppID = ProjectBuilderAppInfo.AppID then
    Result := ProjectBuilderAppInfo
  else if AppID = ProjectBuilderDNetConnectorAppInfo.AppID then
    Result := ProjectBuilderDNetConnectorAppInfo
  else if AppID = EvoUtilDBMaintenanceAppInfo.AppID then
    Result := EvoUtilDBMaintenanceAppInfo
  else if AppID = EvoDNetConnectorAppInfo.AppID then
    Result := EvoDNetConnectorAppInfo
  else if AppID = EvoDNetMapleConnectorAppInfo.AppID then
    Result := EvoDNetMapleConnectorAppInfo
  else if AppID = EvoJavaESSAppInfo.AppID then
    Result := EvoJavaESSAppInfo
  else if AppID = EvoJavaWCAppInfo.AppID then
    Result := EvoJavaWCAppInfo

  else
  begin
    Result.AppID := AppID;
    Result.Name := 'Unknown';
    Result.Abbr := '';
  end;
end;

end.
