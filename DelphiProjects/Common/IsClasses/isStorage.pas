unit isStorage;

interface

uses SysUtils, Variants, isBaseClasses, isBasicUtils;

type
  IisElement = interface
  ['{5A151D36-3707-4AEA-84C9-482C06767016}']
    function  GetName: String;
    procedure SetName(const AName: String);
    property  Name: String read GetName write SetName;
  end;

  IisSimpleElement = interface(IisElement)
  ['{6D01E987-2B69-405D-B01E-04B2B570960B}']
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
    property  Value: Variant read GetValue write SetValue;
  end;

  IisComplexElement = interface(IisElement)
  ['{B9CFCD41-705D-4D4E-BF50-589BBDD29B09}']
    function  GetClassID: String;
    procedure SetClassID(const AValue: String);
    function  AddSimpleElement(const AName: String): IisSimpleElement;
    function  AddComplexElement(const AName: String): IisComplexElement;
    procedure DeleteElements;
    function  GetElement(Index: Integer): IisElement;
    function  ElementCount: Integer;
    function  FindElement(const AName: String): IisElement;
    function  IndexOfElement(const AName: String): Integer;
    property  Elements[Index: Integer]: IisElement read GetElement;
    property  ClassID: String read GetClassID write SetClassID;
  end;

  IisStorage = interface
  ['{0365255E-DA32-4B43-B3BE-46FF107EF3D4}']
    function RootElement: IisComplexElement;
  end;

implementation

type
  TisSimpleElement = class(TisNamedObject, IisElement, IisSimpleElement)
  private
    FValue: Variant;
  protected
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
  end;


  TisComplexElement = class(TisCollection, IisElement, IisComplexElement)
  private
    FClassID: String;
  protected
    function  GetClassID: String;
    procedure SetClassID(const AValue: String);
    function  AddSimpleElement(const AName: String): IisSimpleElement;
    function  AddComplexElement(const AName: String): IisComplexElement;
    procedure DeleteElements;
    function  GetElement(Index: Integer): IisElement;
    function  ElementCount: Integer;
    function  FindElement(const AName: String): IisElement;
    function  IndexOfElement(const AName: String): Integer;
  end;


  TisStorage = class(TisInterfacedObject, IisStorage, IisFormatedReader, IisFormatedWriter)
  private
    FRootElement: IisComplexElement;
    FCurrentElement: IisComplexElement;
    FStack: IisInterfaceList;
    procedure Push;
    procedure Pop;
  protected
    procedure DoOnConstruction; override;

    function  BeginReadGroup(const AName: String): Integer;
    procedure EndReadGroup;
    function  ReadValueByIndex(const AIndex: Integer): Variant;
    function  ReadObjectByIndex(const AIndex: Integer; const ATarget: IisInterfacedObject = nil): IisInterfacedObject;
    function  ReadValue(const AName: String; const ADefault: Variant): Variant;
    function  ReadObject(const AName: String; const ATarget: IisInterfacedObject): IisInterfacedObject;

    procedure BeginWriteGroup(const AName: String);
    procedure EndWriteGroup;
    procedure WriteValue(const AName: String; const AValue: Variant);
    procedure WriteObject(const AName: String; const ASource: IisInterfacedObject);

    function RootElement: IisComplexElement;
  public
    constructor Create(const ARootElement: IisComplexElement = nil); reintroduce;
  end;


{ TisSimpleElement }

function TisSimpleElement.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TisSimpleElement.SetValue(const AValue: Variant);
begin
  FValue := AValue;
end;


{ TisComplexElement }

function TisComplexElement.AddComplexElement(const AName: String): IisComplexElement;
begin
  Result := TisComplexElement.Create;
  Result.Name := AName;
  AddChild(Result as IisInterfacedObject);
end;

function TisComplexElement.AddSimpleElement(const AName: String): IisSimpleElement;
begin
  Result := TisSimpleElement.Create;
  Result.Name := AName;
  AddChild(Result as IisInterfacedObject);
end;

procedure TisComplexElement.DeleteElements;
begin
  RemoveAllChildren;
end;

function TisComplexElement.ElementCount: Integer;
begin
  Result := ChildCount;
end;

function TisComplexElement.FindElement(const AName: String): IisElement;
begin
  Result := FindChildByName(AName) as IisElement;
end;

function TisComplexElement.GetClassID: String;
begin
  Result := FClassID;
end;

function TisComplexElement.GetElement(Index: Integer): IisElement;
begin
  Result := GetChild(Index) as IisElement;
end;

function TisComplexElement.IndexOfElement(const AName: String): Integer;
begin
  Result := IndexOf(AName);
end;

procedure TisComplexElement.SetClassID(const AValue: String);
begin
  FClassID := AValue;
end;


{ TisStorage }

procedure TisStorage.DoOnConstruction;
begin
  inherited;
  FStack := TisInterfaceList.Create;
end;

function TisStorage.ReadObject(const AName: String; const ATarget: IisInterfacedObject): IisInterfacedObject;
var
  i: Integer;
begin
  i := FCurrentElement.IndexOfElement(AName);
  if i = -1 then
    Result := ATarget
  else
    Result := ReadObjectByIndex(i, ATarget);
end;

procedure TisStorage.WriteObject(const AName: String; const ASource: IisInterfacedObject);
begin
  BeginWriteGroup(AName);
  try
    if Assigned(ASource) then
    begin
      FCurrentElement.ClassID := ASource.TypeID;
      ASource.FormatedWrite(Self);
    end;
  finally
    EndWriteGroup;
  end;
end;

function TisStorage.ReadValue(const AName: String; const ADefault: Variant): Variant;
var
  i: Integer;
begin
  i := FCurrentElement.IndexOfElement(AName);
  if i = -1 then
    Result := ADefault
  else
    Result := ReadValueByIndex(i);
end;

procedure TisStorage.WriteValue(const AName: String; const AValue: Variant);
var
  Elem: IisSimpleElement;
begin
  if VarType(AValue) = varUnknown then
    WriteObject(AName, IInterface(AValue) as IisInterfacedObject)
  else
  begin
    Elem := FCurrentElement.AddSimpleElement(AName);
    Elem.Value := AValue;
  end;
end;

function TisStorage.ReadObjectByIndex(const AIndex: Integer; const ATarget: IisInterfacedObject): IisInterfacedObject;
begin
  Push;
  try
    FCurrentElement := FCurrentElement.Elements[AIndex] as IisComplexElement;
    Result := ATarget;
    if Assigned(Result) then
      CheckCondition(FCurrentElement.ClassID = ATarget.TypeID, 'ClassID does not match')
    else
      Result := ObjectFactory.CreateInstanceOf(FCurrentElement.ClassID);

    Result.FormatedRead(Self);
  finally
    Pop;
  end;
end;

function TisStorage.ReadValueByIndex(const AIndex: Integer): Variant;
begin
  if Supports(FCurrentElement.Elements[AIndex], IisSimpleElement) then
    Result := (FCurrentElement.Elements[AIndex] as IisSimpleElement).Value
  else
    Result := ReadObjectByIndex(AIndex);
end;


function TisStorage.BeginReadGroup(const AName: String): Integer;
begin
  Push;
  FCurrentElement := FCurrentElement.FindElement(AName) as IisComplexElement;
  Result := FCurrentElement.ElementCount;
end;

procedure TisStorage.BeginWriteGroup(const AName: String);
begin
  Push;
  if Assigned(FCurrentElement) then
    FCurrentElement := FCurrentElement.AddComplexElement(AName)
  else
    FCurrentElement := TisComplexElement.Create;
end;

procedure TisStorage.EndReadGroup;
begin
  Pop;
end;

procedure TisStorage.EndWriteGroup;
begin
  Pop;
end;

procedure TisStorage.Pop;
begin
  FCurrentElement := FStack[FStack.Count - 1] as IisComplexElement;
  FStack.Delete(FStack.Count - 1);
end;

procedure TisStorage.Push;
begin
  FStack.Add(FCurrentElement);
end;

constructor TisStorage.Create(const ARootElement: IisComplexElement);
begin
  inherited Create;
  if Assigned(ARootElement) then
    FRootElement := ARootElement
  else
    FRootElement := TisComplexElement.Create;

  FCurrentElement := FRootElement;
end;

function TisStorage.RootElement: IisComplexElement;
begin
  Result := FRootElement;
end;

end.