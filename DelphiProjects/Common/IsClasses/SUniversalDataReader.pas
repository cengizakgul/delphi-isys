// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SUniversalDataReader;

{$W-}
{$O+}
{$Q-}
{$R-}

interface

uses EvStreamUtils, Db, Classes;

const
  UniversalDataReaderVersionSignature: Word = $02FF;

type
  TArrayOfInteger = array of Integer;

  TEvQueryResultStreamFormat = (sfOpenQuery, sfExecQuery);

  IEvQueryResult = interface
  ['{CC8C5A5C-51BE-48D7-BFE6-2BFF76DD4159}']
    function StreamFormat: TEvQueryResultStreamFormat;
    function DualStream: IEvDualStream;
    function FieldValues(const FieldName: string): TArrayOfInteger;
    function Tags: TStringList;
  end;

  TNamedArrayOfIntArray = array of record
    Name: string;
    Tag: Integer;
    A: TArrayOfInteger;
  end;

  TIEvQueryResultArray = array of IEvQueryResult;

  TEvQueryResult = class(TInterfacedObject, IEvQueryResult)
  private
    FTags: TStringList;
    FStreamFormat: TEvQueryResultStreamFormat;
    FDualStream: IEvDualStream;
    FValueArray: TNamedArrayOfIntArray;
    function Tags: TStringList;
    function StreamFormat: TEvQueryResultStreamFormat;
    function DualStream: IEvDualStream;
    function FieldValues(const FieldName: string): TArrayOfInteger;
  public
    constructor Create(const StreamFormat: TEvQueryResultStreamFormat; const DualStream: IEvDualStream; const ValueArray: TNamedArrayOfIntArray);
    destructor  Destroy; override;
  end;

  TEvPumpFieldProcedure = procedure (const Data: Pointer) of object;

  TEvUnversalDataReaderFieldRecord = record
    Index: Integer;
    FieldName: string;
    DataType: TFieldType;
    DataLength: Integer;
    Required: Boolean;
    PumpIndex: Integer;
    PumpData: Pointer;
  end;

  TEvUnversalDataReaderFieldArray = array of TEvUnversalDataReaderFieldRecord;

  TEvUnversalDataReader = class
  protected
    procedure BeforeAssignFieldDefs(const FieldCount: Integer); virtual; abstract;
    procedure AssignFieldDef(const FieldInfo: TEvUnversalDataReaderFieldRecord; out PumpIndex: Integer); virtual; abstract;
    procedure AfterAssignFieldDefs; virtual; abstract;
    procedure BeforeRead(const RecordCount: Cardinal); virtual; abstract;
    procedure AssignPumpData(const FieldInfo: TEvUnversalDataReaderFieldRecord; out Data: Pointer); virtual; abstract;
    procedure AfterRead; virtual; abstract;
    procedure BeforeNewRecord; virtual; abstract;
    procedure AfterNewRecord; virtual; abstract;
    procedure PumpNull(const Data: Pointer); virtual; abstract;
    procedure PumpBoolean(const Data: Pointer; const b: Boolean); virtual; abstract;
    procedure PumpInteger(const Data: Pointer; const i: Integer); virtual; abstract;
    procedure PumpInt64(const Data: Pointer; const i: Int64); virtual; abstract;
    procedure PumpString(const Data: Pointer; const s: string); virtual; abstract;
    procedure PumpBlob(const Data: Pointer; const DataSize: Integer; const DataStr: string); virtual; abstract;
    procedure PumpDouble(const Data: Pointer; const d: Double); virtual; abstract;
  public
    procedure Read(const AStream: IisStream);
  end;

implementation

uses SysUtils;

{ TEvUnversalDataReader }

procedure TEvUnversalDataReader.Read(const AStream: IisStream);
var
  FFields: TEvUnversalDataReaderFieldArray;
  FPField: ^TEvUnversalDataReaderFieldRecord;
  FFieldCount: Integer;
  i, j, FRecCount: Integer;
  BlobDataStr: string;
  VersionSignatureBuffer: Word;
  b: Byte;
begin
  VersionSignatureBuffer := AStream.ReadWord;
  Assert(VersionSignatureBuffer = UniversalDataReaderVersionSignature, 'Data format does not match');

  FRecCount := AStream.ReadInteger;
  FFieldCount := AStream.ReadInteger;

  BeforeAssignFieldDefs(FFieldCount);
  SetLength(FFields, FFieldCount);
  for i := Low(FFields) to High(FFields) do
  begin
    FPField := @FFields[i];
    FPField^.FieldName := AStream.ReadString;
    FPField^.DataType := TFieldType(AStream.ReadInteger);
    FPField^.DataLength := AStream.ReadInteger;
    FPField^.Required := AStream.ReadBoolean;
    FPField^.Index := i;
    AssignFieldDef(FPField^, FPField^.PumpIndex);
  end;
  AfterAssignFieldDefs;

  for i := 0 to FFieldCount-1 do
    AssignPumpData(FFields[i], FFields[i].PumpData);

  try
    BeforeRead(FRecCount);
    for j := 0 to FRecCount-1 do
    begin
      BeforeNewRecord;
      for i := 0 to FFieldCount-1 do
        with FFields[i] do
          if PumpData = nil then
          begin
            if not AStream.ReadBoolean then
              case DataType of
                ftFloat, ftDateTime, ftDate:
                  AStream.ReadDouble;

                ftBlob, ftGraphic, ftMemo:
                  AStream.ReadString;

                ftString:
                  if DataLength = 1 then
                    AStream.ReadByte
                  else if DataLength <= 255 then
                    AStream.ReadShortString
                  else
                    AStream.ReadString;

                ftInteger:
                  AStream.ReadInteger;

                ftLargeint:
                  AStream.ReadInt64;

                ftBoolean:
                  AStream.ReadBoolean;
              else
                Assert(False);
              end;
          end

          else if AStream.ReadBoolean then
            PumpNull(PumpData)
          else
            case DataType of
            ftFloat, ftCurrency, ftDateTime, ftDate:
              PumpDouble(PumpData, AStream.ReadDouble);

            ftString:
              if DataLength = 1 then
              begin
                b := AStream.ReadByte;
                if b = 0 then
                  PumpString(PumpData, '')
                else
                  PumpString(PumpData, Char(b));
              end
              else if DataLength <= 255 then
                PumpString(PumpData, AStream.ReadShortString)
              else
                PumpString(PumpData, AStream.ReadString);

              ftInteger:
                PumpInteger(PumpData, AStream.ReadInteger);

              ftLargeint:
                PumpInt64(PumpData, AStream.ReadInt64);

              ftBoolean:
                PumpBoolean(PumpData, AStream.ReadBoolean);

              ftBlob, ftGraphic, ftMemo:
              begin
                BlobDataStr := AStream.ReadString;
                PumpBlob(PumpData, Length(BlobDataStr), BlobDataStr);
              end
            else
              Assert(False, 'Unexpected data type '+ FieldTypeNames[DataType]);
            end;
      AfterNewRecord;
    end;
  finally
    AfterRead;
  end;
end;

{ TEvQueryResult }

constructor TEvQueryResult.Create(const StreamFormat: TEvQueryResultStreamFormat; const DualStream: IEvDualStream;
  const ValueArray: TNamedArrayOfIntArray);
begin
  FStreamFormat := StreamFormat;
  FDualStream := DualStream;
  FValueArray := ValueArray;
  FTags := TStringList.Create;
end;

destructor TEvQueryResult.Destroy;
begin
  FreeAndNil(FTags);
  inherited;
end;

function TEvQueryResult.DualStream: IEvDualStream;
begin
  Result := FDualStream;
end;

function TEvQueryResult.FieldValues(
  const FieldName: string): TArrayOfInteger;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to High(FValueArray) do
    if FValueArray[i].Name = FieldName then
    begin
      Result := FValueArray[i].A;
      Break
    end;
end;

function TEvQueryResult.StreamFormat: TEvQueryResultStreamFormat;
begin
  Result := FStreamFormat; 
end;

function TEvQueryResult.Tags: TStringList;
begin
  Result := FTags;
end;

end.
