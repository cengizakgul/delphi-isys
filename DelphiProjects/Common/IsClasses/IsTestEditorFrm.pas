unit IsTestEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, isTestEditor, istestutils;

type
  TisTestEditorHolder = class(TForm, IisTestEngineSupportControl)
    Editor: TisTestEditorFrame;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
          
function FindEditorForm: TisTestEditorHolder;
function GetEditorForm: TisTestEditorHolder;

implementation

{$R *.dfm}

function FindEditorForm: TisTestEditorHolder;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to Screen.FormCount-1 do
    if Screen.Forms[i] is TisTestEditorHolder then
    begin
      Result := Screen.Forms[i] as TisTestEditorHolder;
      break;
    end;
end;

function GetEditorForm: TisTestEditorHolder;
begin
  Result := FindEditorForm;
  if Result = nil then
    Result := TisTestEditorHolder.Create(nil);
end;

procedure TisTestEditorHolder.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := Editor.CloseQuery;
end;

procedure TisTestEditorHolder.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;


procedure TisTestEditorHolder.FormShow(Sender: TObject);
begin
  Repaint;
end;

end.
