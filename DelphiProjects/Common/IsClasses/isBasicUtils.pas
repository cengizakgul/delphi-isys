unit ISBasicUtils;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

interface

uses
   Windows, psAPI, messages, DateUtils, ComObj, SysUtils, Classes, StrUtils, IsTypes, isBaseClasses,
   ShellAPI, Math, Types, EvStreamUtils, ShlObj, SHFolder, TlHelp32, WinSvc, Variants, OgProExe,
   isStrUtils, isVCLBugFix;

const
  NumberChars = ['0'..'9'];
  ValidTextChars = ['_', '0'..'9', 'A'..'Z', 'a'..'z'];

type
  TisFileInfo = record
    FileName: String;
    Size: Int64;
    Attributes: DWORD;
    CreationTime: TDateTime;
    LastAccessTime: TDateTime;
    LastWriteTime: TDateTime;
  end;

  TisRunExeOption = (roHideWindow, roWaitForClose);
  TisRunExeOptions = set of TisRunExeOption;

  TisOSFolderType =  (ftsDesktop,
                      ftsInternet,
                      ftsPrograms,
                      ftsControls,
                      ftsPrinters,
                      ftsPersonal,
                      ftsRecycleBin,
                      ftsDrives,
                      ftsNetwork,
                      ftsFonts,
                      ftsHistory,
                      ftsWindows,
                      ftsSystem,
                      ftsProgramFiles,
                      ftsProgramFiles_x86,
                      ftsProgramFilesCommon,
                      ftsProgramFilesCommon_x86,
                      ftsConnections,
                      ftsResources,
                      ftsResourvesLocalizes,
                      ftsComputersNearMe);


  TisOSFolderTypeAllUsers = (ftaStartMenu,
                             ftaPrograms,
                             ftaStartup,
                             ftaDesktopDirectory,
                             ftaFavorites,
                             ftaAppData,
                             ftaTemplates,
                             ftaDocuments,
                             ftaAdminTools,
                             ftaMusic,
                             ftaPictures,
                             ftaVideo);

  TisOSFolderTypeUser = (ftuStartMenu,
                         ftuPrograms,
                         ftuStartup,
                         ftuDesktopDirectory,
                         ftuFavorites,
                         ftuAppData,
                         ftuTemplates,
                         ftuDocuments,
                         ftuAdminTools,
                         ftuMusic,
                         ftuPictures,
                         ftuVideo,
                         ftuRecent,
                         ftuSendTo,
                         ftuNethood,
                         ftuPrinthood,
                         ftuLocalAppData,
                         ftuInternetCache,
                         ftuCookies
                         );

  TisServiceStatus = (ssUnknown, ssContinuePending, ssPausePending, ssPaused,
                      ssRunning, ssStartPending, ssStopPending, ssStopped);

  TisOSType = (osUnknown, osWin2000, osWinXP, osWinSrv2003, osWinSrv2003R2, osWinVista, osWinSrv2008, osWinSrv2008R2, osWin7);

  TisCompOperType = (coEqual, coGreater, coLess, coGreaterOrEqual, coLessOrEqual);

  TisProcOutputCB = procedure(const AData: Pointer; const ALength: Integer; const AContextData: Pointer);

  TUTCDateTime = string;

function  AppDir: String;
function  AppFileName: String;
function  AppSwitches: IisListOfValues;
function  AppTempFolder: String;
function  AppDataFolder: String;
function  AppVersion: String;
function  ModuleVersion: String;
function  ModuleFileName: String;
procedure SetAppTempFolder(const ATempFolder: String);
procedure SetAppDataFolder(const ADataFolder: String);
function  AppID: String;
procedure SetAppID(const AAppID: String);

function GetFileVersion(const aFileName: string): string;

function FetchToken(var AInput: string; const ADelim: Char = ' '): string;
function GetNextStrValue(var AStr: string; const ADelim: String = ','): String;
function GetPrevStrValue(var AStr: string; const ADelim: String = ','): String;
function GetNextTextValue(var AStr: string; const AValidChars: TSetOfChar = ValidTextChars): String;
function MakeValidComponentName(const AComponentName: string): string;
function TextIsVisiallyEmpty(const AText: String): Boolean;
function ReadDelimitedText(const AText, ADelimiter: String): TisDynStringArray;

procedure AddStrValue(var AStr: String; const AValue: String; const ADelim: String = ',');
procedure AddStrNamedValue(var AStr: String; const AName: String; const AValue: String; const ADelim: String = #13);
function GetNamedValue(const AStr, AName: string; const ADelim: String = #13; const AQuote: String = '"'): String;
function StrToBin(const AStr: String): String;
function BinToStr(const ABinStr: String): String;
function BufferToHex(const Buf; BufSize: Cardinal): string;
function HexToBuffer(const Hex: string; var Buf; BufSize: Cardinal): Boolean;
function DateToText(const ADate: TDateTime): String;
function TextToDate(const AText: String): TDateTime;
function DateTimeToText(const ADate: TDateTime; const AEmptyDate: String = ''): String;
function UnixEpochToDateTime(const AUnixEpochTime: Cardinal): TDateTime;
function DateTimeToUnixEpoch(const ADateTime: TDateTime): Cardinal;
function ValidateFileName(const s: string): string;
function GetMemStatus: TMemoryStatus;
function NormalizePath(const APath: String): String;
function DenormalizePath(const APath: String): String;
function ChangeFilePath(const AFileName, ANewPath: String): String;
function ExtractFilePathUD(const AFileName: string): string;
function ExtractFileNameUD(const AFileName: string): string;

function ClearDir(const Path: string; Delete: Boolean = False): Boolean;
procedure CheckCondition(const ACondition: Boolean; const AErrMessage: String; AExceptClass: ExceptClass = nil; AHelpContext: Integer = 0);
procedure ErrorIf(const ACondition: Boolean; const AErrMessage: String; AExceptClass: ExceptClass = nil; AHelpContext: Integer = 0);
function  SizeToString(const ASize: Int64; const AUnit: Char = 'B'): String;
function  PeriodToString(const AMilliseconds: Int64; ADetalization: String = 'ms'): String;
function  UpCaseFirst(const AStr: string): string;
function  StartsWith(const AStr, ASubStr: String; const CaseSensetive: Boolean = False): Boolean;
function  Contains(const AStr, ASubStr: String; const CaseSensetive: Boolean = False): Boolean;
function  FinishesWith(const AStr, ASubStr: String; const CaseSensetive: Boolean = False): Boolean;
procedure CopyFile(const ASourceFile, ATargetFile: String; const AFailIfExists: Boolean);
procedure MoveFile(const ASourceFile, ATargetFile: String);
procedure CopyFolder(const ASourceFolder, ATargetFolder: String);
procedure MoveFolder(const ASourceFolder, ATargetFolder: String);
procedure CopyFolderContent(const ASourceFolder, ATargetFolder: String);
procedure MoveFolderContent(const ASourceFolder, ATargetFolder: String);
function  GetFilesByMask(const AFileMask: String): IisStringList;
function  GetSubDirs(const APath: String): IisStringList;
function  GetFileInfo(const AFile: String): TisFileInfo;
function  CheckFileExt(const AFileName, AExt: String): Boolean;
function  CheckFileName(const AFileName, AName: String): Boolean;
function  CheckFilePath(const AFileName, APath: String): Boolean;
function  FileIsUsed(const AFileName: String): Boolean;
function  WaitIfFileUsed(const AFileName: String; const ASeconds: Integer = -1): Boolean;

function  RunProcess(const AFileName: String; const AParams: String; const AOptions: TisRunExeOptions;
                     AOutStream: IisStream = nil; AErrorStream: IisStream = nil; OutputCallback: TisProcOutputCB = nil;
                     ErrorsCallback: TisProcOutputCB = nil; const AContextData: Pointer = nil): Boolean;
procedure RunIsolatedProcess(const AFileName: String; const AParams: String; const AOptions: TisRunExeOptions);
function  TerminateProcesses(const AProcList: IisStringList; out ANotTerminated: IisStringList): Boolean;
procedure DispatchAppPaint;
function  GetParentProcessInfo: TProcessEntry32;
function  StartedAsService: Boolean;
function  GetServiceStatus(const AServiceName: String): TisServiceStatus;
function  AppNeedsElevation: Boolean;
function  RequestElevation: Boolean;

function  DateTimeCompare(const ALeftDate: TDateTime; const AOper: TisCompOperType; const ARightDate: TDateTime): Boolean;
function  DateCompare(const ALeftDate: TDateTime; const AOper: TisCompOperType; const ARightDate: TDateTime): Boolean;
function  DateIsEmpty(const ADate: TDateTime): Boolean;
function  FloatCompare(const ALeftValue: Extended; const AOper: TisCompOperType; const ARightValue: Extended; const AEpsilon: Extended = 0): Boolean;
function  SingleCompare(const ALeftValue: Single; const AOper: TisCompOperType; const ARightValue: Single): Boolean;
function  DoubleCompare(const ALeftValue: Double; const AOper: TisCompOperType; const ARightValue: Double): Boolean;
function  ExtendedCompare(const ALeftValue: Extended; const AOper: TisCompOperType; const ARightValue: Extended): Boolean;

function  NowUTC: TUTCDateTime;
function  GetLocalTimeZone: TTimeZoneInformation;
function  DateToUTC(const ADateTime: TDateTime): TUTCDateTime;
function  DateTimeToUTC(const ADateTime: TDateTime; const ABiasInMinutes: Integer): TUTCDateTime;
function  UTCToDateTime(const AUTCDateTime: TUTCDateTime; out ABiasInMinutes: Integer): TDateTime;
function  UTCToDate(const AUTCDate: TUTCDateTime): TDateTime;
function  LocalDateTimeToUTC(const ADateTime: TDateTime): TUTCDateTime;
function  UTCToLocalDateTime(const AUTCDateTime: TUTCDateTime): TDateTime;
function  UTCToGMTDateTime(const AUTCDateTime: TUTCDateTime): TDateTime;

function GetOSType: TisOSType;
function GetProcMemStatus: TProcessMemoryCounters;
function GetUniqueID: String;
function RandomInteger(iGreaterOrEqualThan, iLessThan: Integer): Integer;
function RandomString(const ALength: Integer; const AUseLowerCase: Boolean = False): String; overload;
function RandomString(const ALength: Integer; const ASymbolsSet: String): String; overload;
function GetComputerNameString: string;

// System folders routines
function GetOSFolder(const AFolderType: TisOSFolderType): string;
function GetOSFolderAllUsers(const AFolderType: TisOSFolderTypeAllUsers): string;
function GetOSFolderUser(const AFolderType: TisOSFolderTypeUser): string;

function  GetTimeMark: Cardinal;
function  MilliSecondsSinceTimeMark( const ATimeMark: Cardinal): Cardinal;

procedure RegisterDefaultApp(const AExeName, AFileExt, AAppDescription: String);

function DayToName(const ADayOfWeek: TDayOfWeek; const ALongName: Boolean = True): String;
function NameToDay(const ADayName: String): TDayOfWeek;

function ConstsToDynVarArray(const ArrayOfConst: array of const): TisDynVariantArray;
function isISystemsDomain: Boolean;
function isISDevelopment: Boolean;
function IIF(const AExpression: Boolean; const AThen: Variant; const AElse: Variant): Variant;
procedure RaiseLastOSErrorEx(const Mess: String);

// System Log functions
procedure SysLogAddEvent(const Msg: string; const Context: Integer; const MsgType: Word = EVENTLOG_ERROR_TYPE);
procedure SysLogAddErrorEvent(const Msg: string; const Context: Integer);
procedure SysLogAddInfoEvent(const Msg: string; const Context: Integer);
procedure SysLogAddWarningEvent(const Msg: string; const Context: Integer);


// In order to use this function the binary must be patched with isExeProtector.exe
procedure CheckIfTampered;

// Resources routines
function GetBinaryResource(const AResourceName: String; AResourceType: String; const AStream: IisStream = nil): IisStream;
function GetResourceList(const AResourceType: String): IisStringList;
function GetResourceTypes: IisStringList;

const
  //used by TrwPreviewContainer and TrwFormPreview
  //maybe should be moved to some RW type library
	WM_USER_NOTIFY_SETFOCUS = WM_USER + 385{pseudo-random};


// Win API functions are missing in Delphi

{$EXTERNALSYM SHGetFolderPath}
function SHGetFolderPath(hwndOwner: HWND; nFolder: Integer; hToken: THandle;
                         dwFlags: DWORD; pszPath: LPTSTR): HRESULT; stdcall;
function SHGetFolderPath; external 'shell32.dll' name 'SHGetFolderPathA';


{$EXTERNALSYM CheckTokenMembership}
function CheckTokenMembership(TokenHandle: THandle; SidToCheck: PSID; var IsMember: BOOL): BOOL; stdcall;
function CheckTokenMembership; external 'Advapi32.dll' name 'CheckTokenMembership';

{$EXTERNALSYM IsUserAnAdmin}
function IsUserAnAdmin: BOOL; stdcall;
function IsUserAnAdmin; external 'shell32.dll' name 'IsUserAnAdmin';


implementation

uses isThreadManager, isExceptions, isSettings, isSocket, SEncryptionRoutines;

var FAppID: String;

function GetUniqueID: String;
begin
  Result := CreateClassID;
  Result := StringReplace(Result, '-', '', [rfReplaceAll]);
  Result := Copy(Result, 2, Length(Result) - 2);
end;

function GetComputerNameString: string;
var
  b: array [0..MAX_COMPUTERNAME_LENGTH] of Char;
  i: Cardinal;
begin
  i := Succ(MAX_COMPUTERNAME_LENGTH);
  GetComputerName(PChar(@b), i);
  SetString(Result, PChar(@b),  i);
end;

function FetchToken(var AInput: string; const ADelim: Char = ' '): string;
var
  i: Integer;
begin
  for i := 1 to Length(AInput) do
    if AInput[i] = ADelim then
    begin
      Result := Copy(AInput, 1, i-1);
      Delete(AInput, 1, i);
      Exit;
    end;
  Result := AInput;
  AInput := '';
end;

function ValidateFileName(const s: string): string;
var
  i: Integer;
begin
  Result := s;
  for i := 1 to Length(Result) do
    if not (Result[i] in ['0'..'9', 'A'..'Z', 'a'..'z', '\', '.', '(', ')']) then
      Result[i] := '_';
end;

function RandomInteger(iGreaterOrEqualThan, iLessThan: Integer): Integer;
begin
  result := Trunc(Random(iLessThan - iGreaterOrEqualThan)) + iGreaterOrEqualThan;
end;

function RandomString(const ALength: Integer; const AUseLowerCase: Boolean = False): String;
const
  CharListUpperCase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890';
  CharListLowerCase = 'abcdefghijklmnopqrstuvwxyz';
begin
  if AUseLowerCase then
    Result := RandomString(ALength, CharListUpperCase + CharListLowerCase)
  else
    Result := RandomString(ALength, CharListUpperCase);
end;

function RandomString(const ALength: Integer; const ASymbolsSet: String): String;
var
  i: Integer;
  C: Char;
begin
  SetLength(Result, ALength);
  CheckCondition(Length(ASymbolsSet) > 0, 'SymbolSet cannot be empty!');
  for i := 1 to ALength do
  begin
    C := ASymbolsSet[RandomInteger(1, Length(ASymbolsSet) + 1)];
    Result[i] := C;
  end;
end;

function NormalizePath(const APath: String): String;
var
  Delim: Char;
begin
  Result := APath;
  if (Result <> '') and not (AnsiLastChar(Result)^ in [':', '/', '\']) then
  begin
    if Pos('/', Result) > 0 then
      Delim := '/'
    else
      Delim := '\';

    if (Length(Result) = 1) and (UpCase(Result[1]) in ['A'..'Z']) then
      Result := Result + DriveDelim + Delim
    else
      Result := Result + Delim;
  end;
end;

function DenormalizePath(const APath: String): String;
begin
  Result := APath;
  if (Length(Result) > 0) and (Result[Length(Result)] in ['\', '/']) then
    Delete(Result, Length(Result), 1);
end;

function ChangeFilePath(const AFileName, ANewPath: String): String;
begin
  Result := NormalizePath(ANewPath) +  ExtractFileName(AFileName);
end;

function ExtractFilePathUD(const AFileName: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := Length(AFileName) downto 1 do
    if AFileName[i] in ['\', '/'] then
    begin
      Result := Copy(AFileName, 1, i);
      Break;
    end;
end;

function ExtractFileNameUD(const AFileName: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := Length(AFileName) downto 1 do
    if AFileName[i] in ['\', '/'] then
    begin
      Result := Copy(AFileName, i + 1, Length(AFileName));
      Break;
    end;
end;

function ClearDir(const Path: string; Delete: Boolean = False): Boolean;
const
  FileNotFound = 18;
var
  FileInfo: TSearchRecEx;
  DosCode: Integer;
begin
  Result := DirectoryExists(Path);
  if not Result then Exit;
  DosCode := FindFirstEx(NormalizePath(Path) + '*.*', faAnyFile, FileInfo);
  try
    while DosCode = 0 do
    begin
      if (FileInfo.Name[1] <> '.') then
      begin
        if (FileInfo.Attr and faDirectory = faDirectory) then
          Result := ClearDir(NormalizePath(Path) + FileInfo.Name, True) and Result
        else
          Result := Sysutils.DeleteFile(NormalizePath(Path) + FileInfo.Name) and Result;
      end;
      DosCode := FindNextEx(FileInfo);
    end;
  finally
    FindCloseEx(FileInfo);
  end;
  if Delete and Result and (DosCode = FileNotFound) and
    not ((Length(Path) = 2) and (Path[2] = ':')) then
  begin
    Result := RemoveDir(Path);
//    Result := (IOResult = 0) and Result;
  end;
end;


function GetNextStrValue(var AStr: string; const ADelim: String = ','): String;
begin
  Result := isStrUtils.GetNextStrValue(AStr, ADelim);
end;


function GetNextTextValue(var AStr: string; const AValidChars: TSetOfChar = ValidTextChars): String;
var
  i: Integer;
begin
  i := 1;
  while i <= Length(AStr) do
    if not (AStr[i] in AValidChars) then
      break
    else
      Inc(i);

  if i = 1 then
  begin
    Result := '';
    Delete(AStr, 1, 1);
  end
  else
  begin
    Result := Copy(AStr, 1, i - 1);
    Delete(AStr, 1, i - 1);
  end;
end;

function GetPrevStrValue(var AStr: string; const ADelim: String = ','): String;
var
  i, j: Integer;
begin
   if AStr = '' then
   begin
     Result := '';
     Exit;
   end;

   i := 0;
   repeat
     j := i;
     if i <> 0 then
       Inc(i, Length(ADelim));
     i := PosEx(ADelim, AStr, i);
   until i = 0;

   if j = 0 then
   begin
     Result := AStr;
     AStr := '';
   end

   else
   begin
     Result := Copy(AStr, j + Length(ADelim), Length(AStr) - (j + Length(ADelim)) + 1);
     Delete(AStr, j, Length(AStr) - j + 1);
   end;
end;

procedure AddStrValue(var AStr: String; const AValue: String; const ADelim: String = ',');
begin
  isStrUtils.AddStrValue(AStr, AValue, ADelim);
end;

procedure AddStrNamedValue(var AStr: String; const AName: String; const AValue: String; const ADelim: String = #13);
begin
  if AStr <> '' then
    AStr := AStr + ADelim + AName + '=' + AValue
  else
    AStr := AStr + AName + '=' + AValue;
end;

function GetNamedValue(const AStr, AName: string; const ADelim: String = #13; const AQuote: String = '"'): String;
var
  s, l, v: String;
begin
  Result := '';
  s:= AStr;
  while s <> '' do
  begin
    l := GetNextStrValue(s, '=');

    v := '';
    if AQuote <> '' then
      while (s <> '') and (s[1] = AQuote) do
      begin
        Delete(s, 1, 1);
        v := v + GetNextStrValue(s, AQuote);
      end;
    v := v + GetNextStrValue(s, ADelim);

    if AnsiSameText(l, AName) then
    begin
      Result := v;
      break;
    end;
  end;
end;


function StrToBin(const AStr: String): String;
begin
  SetLength(Result, Length(AStr) * 2);
  if Length(AStr) > 0 then
    BinToHex(@AStr[1], @Result[1], Length(AStr));
end;


function BinToStr(const ABinStr: String): String;
begin
  SetLength(Result, Length(ABinStr) div 2);
  if Length(ABinStr) > 0 then
    HexToBin(@ABinStr[1], @Result[1], Length(Result));
end;

function DateToText(const ADate: TDateTime): String;
var
  Y, M, D, Hr, Min, Sec, MSec: Word;
begin
  if ADate = 0 then
    Result := ''

  else
  begin
    DecodeDateTime(ADate, Y, M, D, Hr, Min, Sec, MSec);
    Result := IntToStr(M) + '/' + IntToStr(D) + '/' +  IntToStr(Y);

    if (Hr <> 0) or (Min <> 0) or (Sec <> 0) or (MSec <> 0) then
    begin
      Result := Result + ' ' + IntToStr(Hr) + ':' + IntToStr(Min) + ':' + IntToStr(Sec);
      if MSec <> 0 then
        Result := Result + ':' + IntToStr(MSec);
    end;
  end;
end;

function TextToDate(const AText: String): TDateTime;
var
  Y, M, D, Hr, Min, Sec, MSec: Word;
  s: String;
begin
  if AText = '' then
    Result := 0

  else
  begin
    s := AText;
    M := StrToInt(GetNextStrValue(s, '/'));
    D := StrToInt(GetNextStrValue(s, '/'));
    Y := StrToInt(GetNextStrValue(s, ' '));
    if s <> '' then
    begin
      Hr := StrToInt(GetNextStrValue(s, ':'));
      Min := StrToInt(GetNextStrValue(s, ':'));
      Sec := StrToInt(GetNextStrValue(s, ':'));
      if s <> '' then
        MSec := StrToInt(s)
      else
        MSec := 0;
    end

    else
    begin
      Hr := 0;
      Min := 0;
      Sec := 0;
      MSec := 0;
    end;

    Result := EncodeDateTime(Y, M, D, Hr, Min, Sec, MSec);
  end;
end;

function DateTimeToText(const ADate: TDateTime; const AEmptyDate: String = ''): String;
begin
  if ADate = 0 then
    Result := AEmptyDate
  else
    Result := DateTimeToStr(ADate);
end;

procedure CheckCondition(const ACondition: Boolean; const AErrMessage: String; AExceptClass: ExceptClass = nil; AHelpContext: Integer = 0);
begin
  if not ACondition then
  begin
    if not Assigned(AExceptClass) then
      AExceptClass := EisException;

    if AHelpContext = 0 then
      raise AExceptClass.Create(AErrMessage)
    else
      raise AExceptClass.CreateHelp(AErrMessage, AHelpContext);
  end;
end;

procedure ErrorIf(const ACondition: Boolean; const AErrMessage: String; AExceptClass: ExceptClass = nil; AHelpContext: Integer = 0);
begin
  CheckCondition(not ACondition, AErrMessage, AExceptClass, AHelpContext);
end;

function SizeToString(const ASize: Int64; const AUnit: Char): String;
var
  threshold: Int64;
  unitName: String;
begin
  unitName := AUnit + 'B';
  case AUnit of
    'K': threshold := 1024;
    'M': threshold := 1024 * 1024;
    'G': threshold := 1024 * 1024 * 1024;
  else
    threshold := 1;
    unitName := 'Bytes';
  end;

  if ASize >= 1024 * 1024 * 1024 div threshold then
    Result := FormatFloat('#,##0.##',  ASize / (1024 * 1024 * 1024 div threshold)) + ' GB'
  else if ASize >= 1024 * 1024 div threshold then
    Result := FormatFloat('#,##0.##', ASize / (1024 * 1024 div threshold)) + ' MB'
  else if (ASize >= 1024 div threshold) and (ASize > 0) then
    Result := FormatFloat('#,##0.##', ASize / (1024 div threshold)) + ' KB'
  else
    Result := IntToStr(ASize) + ' ' + unitName;
end;

function PeriodToString(const AMilliseconds: Int64; ADetalization: String): String;
var
  d, h, m, s, ms: Int64;

  procedure DivMod(ADividend: Int64; ADivisor: Int64; var AResult, ARemainder: Int64);
  begin
    AResult := ADividend div ADivisor;
    ARemainder := ADividend mod ADivisor;
  end;
  
begin
  if AMilliseconds = 0 then
  begin
    Result := '0 ms';
    Exit;
  end;

  DivMod(AMilliseconds, 24 * 60 * 60 * 1000, d, ms);
  DivMod(ms, 60 * 60 * 1000, h, ms);
  DivMod(ms, 60 * 1000, m, ms);
  DivMod(ms, 1000, s, ms);

  if ADetalization = 'd' then
    if d > 0 then
    begin
      h := 0;
      m := 0;
      s := 0;
      ms := 0;
    end
    else
      ADetalization := 'h';

  if ADetalization = 'h' then
    if h > 0 then
    begin
      m := 0;
      s := 0;
      ms := 0;
    end
    else
      ADetalization := 'm';

  if ADetalization = 'm' then
    if m > 0 then
    begin
      s := 0;
      ms := 0;
    end
    else
      ADetalization := 's';

  if ADetalization = 's' then
    ms := 0;

  Result := '';

  if d > 0 then
    AddStrValue(Result, IntToStr(d) + ' d', '   ');
  if h > 0 then
    AddStrValue(Result, IntToStr(h) + ' h', '   ');
  if m > 0 then
    AddStrValue(Result, IntToStr(m) + ' m', '   ');
  if (s > 0) and (d = 0) then
    AddStrValue(Result, IntToStr(s) + ' s', '   ');
  if (ms > 0) and (d = 0) and (h = 0) then
    AddStrValue(Result, IntToStr(ms) + ' ms', '   ');
end;

function GetMemStatus: TMemoryStatus;
begin
  Result.dwLength := SizeOf(Result);
  GlobalMemoryStatus(Result);
end;


function GetProcMemStatus: TProcessMemoryCounters;
begin
  if not GetProcessMemoryInfo(GetCurrentProcess, @Result, SizeOf(Result)) then
    RaiseLastOSError;
end;


function DayToName(const ADayOfWeek: TDayOfWeek; const ALongName: Boolean = True): String;
begin
  if (ADayOfWeek >= Low(DayLongNames)) and (ADayOfWeek <= High(DayLongNames)) then
  begin
    Result := DayLongNames[ADayOfWeek];
    if not ALongName then
      Result := Copy(Result, 1, 3);
  end
  else
    Result := '';
end;


function NameToDay(const ADayName: String): TDayOfWeek;
var
  i: TDayOfWeek;
  bLongName: Boolean;
begin
  Result := dwUnknown;
  bLongName := Length(ADayName) > 3;

  for i := Low(DayLongNames) to High(DayLongNames) do
    if bLongName and AnsiSameText(DayLongNames[i], ADayName) or
       not bLongName and AnsiSameText(Copy(DayLongNames[i], 1, 3), ADayName) then
    begin
      Result := i;
      break;
    end;
end;


function ConstsToDynVarArray(const ArrayOfConst: array of const): TisDynVariantArray;
var
  i: Integer;
begin
  SetLength(Result, Length(ArrayOfConst));
  for i := Low(ArrayOfConst) to High(ArrayOfConst) do
    case ArrayOfConst[i].VType of
      vtInteger:    Result[i] := ArrayOfConst[i].VInteger;
      vtBoolean:    Result[i] := ArrayOfConst[i].VBoolean;
      vtChar:       Result[i] := ArrayOfConst[i].VChar;
      vtExtended:   Result[i] := ArrayOfConst[i].VExtended^;
      vtString:     Result[i] := ArrayOfConst[i].VString^;
      vtPointer:    Result[i] := Integer(ArrayOfConst[i].VPointer);
      vtPChar:      Result[i] := String(ArrayOfConst[i].VPChar);
      vtObject:     Result[i] := Integer(Pointer(ArrayOfConst[i].VObject));
      vtClass:      Result[i] := Integer(Pointer(ArrayOfConst[i].VClass));
      vtWideChar:   Result[i] := WideString(ArrayOfConst[i].VWideChar);
      vtPWideChar:  Result[i] := WideString(ArrayOfConst[i].VPWideChar);
      vtAnsiString: Result[i] := AnsiString(ArrayOfConst[i].VAnsiString);
      vtCurrency:   Result[i] := ArrayOfConst[i].VCurrency^;
      vtVariant:    Result[i] := ArrayOfConst[i].VVariant^;
      vtInterface:  Result[i] := IInterface(ArrayOfConst[i].VInterface);
      vtWideString: Result[i] := WideString(ArrayOfConst[i].VWideString);
      vtInt64:      Result[i] := ArrayOfConst[i].VInt64^;
    else
      Assert(False);
    end;
end;


function UpCaseFirst(const AStr: string): string;
begin
  Result := LowerCase(TrimLeft(AStr));
  if Result <> '' then
    Result[1] := UpCase(Result[1]);
end;


function StartsWith(const AStr, ASubStr: String; const CaseSensetive: Boolean = False): Boolean;
var
  Len: Integer;
begin
  Len := Length(ASubStr);

  if CaseSensetive then
    Result := AnsiSameStr(Copy(AStr, 1, Len), ASubStr)
  else
    Result := AnsiSameText(Copy(AStr, 1, Len), ASubStr);
end;

function FinishesWith(const AStr, ASubStr: String; const CaseSensetive: Boolean = False): Boolean;
var
  Len: Integer;
begin
  Len := Length(ASubStr);

  if CaseSensetive then
    Result := AnsiSameStr(Copy(AStr, Length(AStr) - Len + 1, Len), ASubStr)
  else
    Result := AnsiSameText(Copy(AStr, Length(AStr) - Len + 1, Len), ASubStr);
end;

function Contains(const AStr, ASubStr: String; const CaseSensetive: Boolean = False): Boolean;
begin
  if CaseSensetive then
    Result := Pos(ASubStr, AStr) > 0
  else
    Result := Pos(AnsiUpperCase(ASubStr), AnsiUpperCase(AStr)) > 0;
end;

function AppDir: String;
begin
  Result := NormalizePath(ExtractFilePath(AppFileName));
end;

function AppFileName: String;
begin
  Result := ParamStr(0);
end;

function ModuleFileName: String;
begin
  Result := GetModuleName(HInstance);
end;

var FAppSwitches: IisListOfValues;

function AppSwitches: IisListOfValues;
var
  i, iLastParam: Integer;
  sPar: String;
begin
  if not Assigned(FAppSwitches ) then
  begin
    FAppSwitches := TisListOfValues.Create;

    iLastParam := -1;
    for i := 1 to ParamCount do
    begin
      sPar := ParamStr(i);
      if (sPar <> '') and (sPar[1] in ['-', '/']) then
      begin
        Delete(sPar, 1, 1);
        iLastParam := FAppSwitches.AddValue(sPar, '');
      end

      else
      begin
        if iLastParam = - 1 then
          iLastParam := FAppSwitches.AddValue('', '');

        if FAppSwitches[iLastParam].Value <> '' then
          FAppSwitches[iLastParam].Value := FAppSwitches[iLastParam].Value + #13;
        FAppSwitches[iLastParam].Value := FAppSwitches[iLastParam].Value + sPar;
      end;
    end;
  end;

  Result := FAppSwitches;
end;


var FAppTempFolder: String;

procedure SetAppTempFolder(const ATempFolder: String);

  function GetEnvBasic: string;
  var
    Buf: array[0..2047] of char;
  begin
    GetTempPath(2048, @Buf);
    Result := Buf;
    Result := NormalizePath(Result) + 'ISystems\';
  end;

var
  s, subFolder: String;
begin
  s := NormalizePath(ATempFolder);
  if s = '' then
    s := GetEnvBasic;

  subFolder := ChangeFileExt(ExtractFileName(ModuleFileName), '');
  if not FinishesWith(s, '\' + subFolder + '\') then
    s := s + subFolder + '\';

  if not AnsiSameText(FAppTempFolder, s) then
  begin
    FAppTempFolder := s;
    if not ForceDirectories(FAppTempFolder) then
      raise EisException.CreateFmt('Temporary directory "%s" is missing', [FAppTempFolder]);
  end;
end;

function AppTempFolder: String;
begin
  Result := FAppTempFolder;
end;


var FAppDataFolder: String;

procedure SetAppDataFolder(const ADataFolder: String);
var
  s: String;
begin
  s := NormalizePath(ADataFolder);
  if s = '' then
    s := NormalizePath(NormalizePath(GetOSFolderAllUsers(ftaAppData)) + 'ISystems\' + ChangeFileExt(ExtractFileName(ModuleFileName), ''));

  if not AnsiSameText(FAppDataFolder, s) then
  begin
    FAppDataFolder := s;
    ForceDirectories(FAppDataFolder);
  end;
end;

function AppDataFolder: String;
begin
  Result := FAppDataFolder;
end;


function AppVersion: String;
begin
  Result := GetFileVersion(AppFileName);
end;

function ModuleVersion: String;
begin
  Result := GetFileVersion(ModuleFileName);
end;

function GetFileVersion(const aFileName: string): string;
var
  Buffer: array [1..1024] of Char;
  PInfo: Pointer;
  L: Cardinal;
begin
  if GetFileVersionInfo(PChar(aFileName), 0, 1024, @Buffer) and VerQueryValue(@Buffer, '\', PInfo, L) then
    Result := IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionMS div $10000) + '.' +
         IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionMS mod $10000) + '.' +
         IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionLS div $10000) + '.' +
         IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionLS mod $10000)
  else
    Result := '';
end;

function BufferToHex(const Buf; BufSize: Cardinal): string;
var
  Bytes: TByteArray absolute Buf;
  i: LongInt;
begin
  Result := '';
  for I := 0 to BufSize - 1 do
    Result := Result + IntToHex(Bytes[i], 2);
end;

function HexToBuffer(const Hex: string; var Buf; BufSize: Cardinal): Boolean;
var
  Bytes: TByteArray absolute Buf;
  I, C: Integer;
  Str: string;
begin
  Result := False;

  Str := '';
  for I := 1 to Length(Hex) do
    if Upcase(Hex[I]) in ['0'..'9', 'A'..'F'] then
      Str := Str + Hex[I];

  if (Cardinal(Length(Str) div 2) <> BufSize) then
    Exit;

  for I := 0 to BufSize - 1 do begin
    Val('$' + Copy(Str, (I shl 1) + 1, 2), Bytes[I], C);
    if (C <> 0) then
      Exit;
  end;

  Result := True;
end;


function AppID: String;
begin
  Result := FAppID;
end;

procedure SetAppID(const AAppID: String);
begin
  FAppID := AAppID;
end;

procedure CopyFile(const ASourceFile, ATargetFile: String; const AFailIfExists: Boolean);
begin
  Win32Check(Windows.CopyFile(PAnsiChar(ASourceFile), PAnsiChar(ATargetFile), AFailIfExists));
end;

procedure CopyFolderContent(const ASourceFolder, ATargetFolder: String);
var
  Files: IisStringList;
  i: Integer;
  s: String;
begin
  Files := GetFilesByMask(NormalizePath(ASourceFolder) + '*.*');
  for i := 0 to Files.Count - 1 do
    CopyFile(Files[i], ChangeFilePath(Files[i], ATargetFolder), False);

  Files := GetSubDirs(ASourceFolder);
  for i := 0 to Files.Count - 1 do
  begin
    s := ChangeFilePath(DenormalizePath(Files[i]), ATargetFolder);
    CreateDir(s);
    CopyFolderContent(Files[i], s);
  end;
end;

procedure MoveFolderContent(const ASourceFolder, ATargetFolder: String);
var
  Files: IisStringList;
  i: Integer;
  s: String;
begin
  Files := GetFilesByMask(NormalizePath(ASourceFolder) + '*.*');
  for i := 0 to Files.Count - 1 do
    MoveFile(Files[i], ChangeFilePath(Files[i], ATargetFolder));

  Files := GetSubDirs(ASourceFolder);
  for i := 0 to Files.Count - 1 do
  begin
    s := ChangeFilePath(DenormalizePath(Files[i]), ATargetFolder);
    CreateDir(s);
    CopyFolderContent(Files[i], s);
  end;
end;

procedure CopyFolder(const ASourceFolder, ATargetFolder: String);
var
  s: String;
begin
  s := NormalizePath(ATargetFolder) + ExtractFileName(DenormalizePath(ASourceFolder));
  MkDir(s);
  CopyFolderContent(ASourceFolder, s);
end;

procedure MoveFolder(const ASourceFolder, ATargetFolder: String);
var
  s: String;
begin
  s := NormalizePath(ATargetFolder) + ExtractFileName(DenormalizePath(ASourceFolder));
  MkDir(s);
  MoveFolderContent(ASourceFolder, s);
  ClearDir(ASourceFolder, True);
end;


procedure MoveFile(const ASourceFile, ATargetFile: String);
begin
  Win32Check(Windows.MoveFile(PAnsiChar(ASourceFile), PAnsiChar(ATargetFile)));
end;

function GetFilesByMask(const AFileMask: String): IisStringList;
var
  r: TSearchRecEx;
  sDir: String;
begin
  sDir := ExtractFileDir(AFileMask);
  Result := TisStringList.Create;
  if FindFirstEx(AFileMask, faAnyFile, r) = 0 then
    try
      repeat
        if (faDirectory and r.Attr) = 0 then
          Result.Add(NormalizePath(sDir) + r.Name);
      until FindNextEx(r) <> 0;
    finally
      FindCloseEx(r);
    end;
end;

function GetSubDirs(const APath: String): IisStringList;
var
  r: TSearchRecEx;
  sDir: String;
begin
  sDir := NormalizePath(APath);
  Result := TisStringList.Create;
  if FindFirstEx(sDir + '*', faAnyFile, r) = 0 then
    try
      repeat
        if ((R.Attr and faDirectory) <> 0) and (r.Name[1] <> '.') then
          Result.Add(sDir + r.Name);
      until FindNextEx(r) <> 0;
    finally
      FindCloseEx(r);
    end;
end;

function GetFileInfo(const AFile: String): TisFileInfo;

  function FileTime2DateTime(FileTime: TFileTime): TDateTime;
  var
     LocalFileTime: TFileTime;
     SystemTime: TSystemTime;
  begin
     FileTimeToLocalFileTime(FileTime, LocalFileTime) ;
     FileTimeToSystemTime(LocalFileTime, SystemTime) ;
     Result := SystemTimeToDateTime(SystemTime) ;
  end;

var
  R: TSearchRecEx;
begin
  if FindFirstEx(AFile, faAnyFile, R) = 0 then
  begin
    Result.FileName := AFile;
    Result.Size := R.Size;
    Result.Attributes := R.Attr;
    Result.CreationTime := FileTime2DateTime(R.FindData.ftCreationTime);
    Result.LastAccessTime := FileTime2DateTime(R.FindData.ftLastAccessTime);
    Result.LastWriteTime := FileTime2DateTime(R.FindData.ftLastWriteTime);
    FindCloseEx(R);
  end
  else
    ZeroMemory(@Result, SizeOf(Result));
end;

function RunProcess(const AFileName: String; const AParams: String; const AOptions: TisRunExeOptions;
  AOutStream: IisStream = nil; AErrorStream: IisStream = nil;
  OutputCallback: TisProcOutputCB = nil; ErrorsCallback: TisProcOutputCB = nil; const AContextData: Pointer = nil): Boolean;
var
  Cmd: String;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
  secAttr: TSecurityAttributes;
  secDesc: array [1..SECURITY_DESCRIPTOR_MIN_LENGTH] of Char;
  errorPipeRead, errorPipeWrite: THandle;
  outPipeRead, outPipeWrite: THandle;
  exitCode: Cardinal;
  errorDesc: string;
  WasIsTerminated: Boolean;
  pBuf: array[0..4095] of char;
  lpMode: DWORD;
  bShowException: Boolean;
  n: Integer;

  procedure ReadFromPipe(const APipe: THandle; const AStream: IisStream; aOutputCallback: TisProcOutputCB);
  var
    bytesRead: DWord;
  begin
    bytesRead := 0;
    while ReadFile(APipe, pBuf, Length(pBuf), bytesRead, nil) do
    begin
      if Assigned(aOutputCallback) then
        aOutputCallback(@pBuf[0], bytesRead, AContextData);
      if Assigned(AStream) then
        AStream.WriteBuffer(pBuf, bytesRead);
    end;
  end;

begin
  Result := False;
  bShowException := not Assigned(AErrorStream);

  if not StartsWith(AFileName, '"') and Contains(AFileName, ' ') then
    Cmd := '"' + AFileName + '"'
  else
    Cmd := AFileName;
  if AParams <> '' then
   Cmd := Cmd + ' ' + AParams;

 // Remove any security restriction
  ZeroMemory(@secAttr, SizeOf(secAttr));
  secAttr.nLength := SizeOf(secAttr);
  secAttr.bInheritHandle := True;
  secAttr.lpSecurityDescriptor := @secDesc;
  InitializeSecurityDescriptor(secAttr.lpSecurityDescriptor, SECURITY_DESCRIPTOR_REVISION);
  SetSecurityDescriptorDacl(secAttr.lpSecurityDescriptor, True, nil, False);

  ZeroMemory(@StartupInfo, SizeOf(StartupInfo));
  StartupInfo.cb := SizeOf(TStartupInfo);
  StartupInfo.lpDesktop := nil;
  StartupInfo.dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;

  if roHideWindow in AOptions then
    StartupInfo.wshowWindow := SW_HIDE
  else
    StartupInfo.wshowWindow := SW_SHOWNORMAL;

  if roWaitForClose in AOptions then
  begin
    Win32Check(CreatePipe(errorPipeRead, errorPipeWrite, @secAttr, 0));
    Win32Check(CreatePipe(outPipeRead, outPipeWrite, @secAttr, 0));
    lpMode := PIPE_READMODE_BYTE or PIPE_NOWAIT;
    Win32Check(SetNamedPipeHandleState(errorPipeRead, lpMode, nil, nil));
    Win32Check(SetNamedPipeHandleState(outPipeRead, lpMode, nil, nil));
    Win32Check(SetHandleInformation(outPipeRead, HANDLE_FLAG_INHERIT, 0));

    try
      try
        if not Assigned(AErrorStream) then
          AErrorStream := TisStream.CreateInMemory;

        StartupInfo.hStdOutput := outPipeWrite;
        StartupInfo.hStdError := errorPipeWrite;
        Win32Check(CreateProcess(nil, PChar(Cmd), nil, @secAttr, True, NORMAL_PRIORITY_CLASS , nil, nil, StartupInfo, ProcessInfo));
        try
          CloseHandle(errorPipeWrite);
          CloseHandle(outPipeWrite);

          WasIsTerminated := CurrentThreadTaskTerminated;
          n := 0;
          repeat
            if n = 1000 then
            begin
              n := 0;
              if not WasIsTerminated then
                AbortIfCurrentThreadTaskTerminated;
            end
            else
              Inc(n);

            ReadFromPipe(outPipeRead, AOutStream, OutputCallback);
            ReadFromPipe(errorPipeRead, AErrorStream, ErrorsCallback);
          until WaitForSingleObject(ProcessInfo.hProcess, 1) = WAIT_OBJECT_0;
        except
          TerminateProcess(ProcessInfo.hProcess, 0);
          raise;
        end;

        GetExitCodeProcess(ProcessInfo.hProcess, exitCode);
      finally
        CloseHandle(ProcessInfo.hProcess);
        CloseHandle(ProcessInfo.hThread);

        try
          ReadFromPipe(outPipeRead, AOutStream, OutputCallback);
        finally
          CloseHandle(outPipeRead);
        end;
      end;

      if exitCode <> 0 then
      begin
        ReadFromPipe(errorPipeRead, AErrorStream, nil);
        if bShowException then
        begin
          errorDesc := AErrorStream.AsString;
          if errorDesc = '' then
            errorDesc := ExtractFileName(AFileName) + ' finished with an error';
          raise EisException.Create(errorDesc);
        end;
      end
      else
        Result := True;

    finally
      CloseHandle(errorPipeRead);
    end;
  end

  else
  begin
    try
      Win32Check(CreateProcess(nil, PChar(Cmd), nil, @secAttr, True, NORMAL_PRIORITY_CLASS , nil, nil, StartupInfo, ProcessInfo));
      Result := True;
    finally
      CloseHandle(ProcessInfo.hProcess);
      CloseHandle(ProcessInfo.hThread);
    end;
  end;
end;

procedure RunIsolatedProcess(const AFileName: String; const AParams: String; const AOptions: TisRunExeOptions);
var
  sDir: String;
  RunFlags: Integer;
begin
  sDir := ExtractFilePath(AFileName);

  RunFlags := 0;
  if roHideWindow in AOptions then
    RunFlags := RunFlags or SW_HIDE
  else
    RunFlags := RunFlags or SW_SHOW;

  Assert(not(roWaitForClose in AOptions), 'Not implemented');

  if ShellExecute(0, 'open', PAnsiChar(AFileName), PAnsiChar(AParams), PAnsiChar(sDir), RunFlags) <= 32 then
    RaiseLastOSError;
end;

function TerminateProcesses(const AProcList: IisStringList; out ANotTerminated: IisStringList): Boolean;
var
  PIDList: array [0..2048] of DWORD;
  ModuleName: array [1..512] of Char;
  cb: DWORD;
  ProcCount: Integer;
  i: Integer;
  hProcess: THandle;
begin
  Result := True;
  AProcList.CaseSensitive := False;

  EnumProcesses(@PIDList, SizeOf(PIDList), cb);
  ProcCount := cb div SizeOf(DWORD);

  for i := 0 to ProcCount - 1 do
  begin
    hProcess := OpenProcess(PROCESS_QUERY_INFORMATION or
      PROCESS_VM_READ or PROCESS_TERMINATE,
      False,
      PIDList[I]);

    if (hProcess <> 0) then
    begin
      GetModuleFileNameEx(hProcess, 0, PAnsiChar(@ModuleName[1]), SizeOf(ModuleName));
      if AProcList.IndexOf(PAnsiChar(@ModuleName[1]))<> -1 then
        if not TerminateProcess(hProcess, 0) then
        begin
          if not Assigned(ANotTerminated) then
          begin
            ANotTerminated := TisStringList.CreateUnique(False);
            ANotTerminated.CaseSensitive := False;
          end;
          ANotTerminated.Add(PAnsiChar(@ModuleName[1]));
          Result := False;
        end;

      CloseHandle(hProcess);
    end;
  end;
end;

procedure DispatchAppPaint;
var
   M:tagMSG;
begin
  while PeekMessage(M, 0, WM_ERASEBKGND, WM_ERASEBKGND, PM_REMOVE) do
    DispatchMessage(M);

  while PeekMessage(M, 0, WM_PAINT, WM_PAINT, PM_REMOVE) do
    DispatchMessage(M);
end;

function CheckCompResult(const AOper: TisCompOperType; const ACompResult: TValueRelationship): Boolean;
begin
  case AOper of
    coEqual:
      Result := ACompResult = 0;

    coGreater:
      Result := ACompResult = 1;

    coLess:
      Result := ACompResult = -1;

    coGreaterOrEqual:
      Result := (ACompResult = 0) or (ACompResult = 1);

    coLessOrEqual:
      Result := (ACompResult = -1) or (ACompResult = 0);
  else
    Result := False;
  end;
end;

function DateTimeCompare(const ALeftDate: TDateTime; const AOper: TisCompOperType; const ARightDate: TDateTime): Boolean;
begin
  Result := CheckCompResult(AOper, CompareDateTime(ALeftDate, ARightDate));
end;

function DateCompare(const ALeftDate: TDateTime; const AOper: TisCompOperType; const ARightDate: TDateTime): Boolean;
begin
  Result := CheckCompResult(AOper, CompareDate(ALeftDate, ARightDate));
end;

function DateIsEmpty(const ADate: TDateTime): Boolean;
begin
  Result := DateTimeCompare(ADate, coEqual, 0);
end;

function SingleCompare(const ALeftValue: Single; const AOper: TisCompOperType; const ARightValue: Single): Boolean; overload;
begin
  Result := CheckCompResult(AOper, CompareValue(ALeftValue, ARightValue));
end;

function DoubleCompare(const ALeftValue: Double; const AOper: TisCompOperType; const ARightValue: Double): Boolean;
begin
  Result := CheckCompResult(AOper, CompareValue(ALeftValue, ARightValue));
end;

function ExtendedCompare(const ALeftValue: Extended; const AOper: TisCompOperType; const ARightValue: Extended): Boolean;
begin
  Result := CheckCompResult(AOper, CompareValue(ALeftValue, ARightValue));
end;

function FloatCompare(const ALeftValue: Extended; const AOper: TisCompOperType; const ARightValue: Extended; const AEpsilon: Extended = 0): Boolean;
begin
  Result := CheckCompResult(AOper, CompareValue(ALeftValue, ARightValue, AEpsilon));
end;

procedure RegisterDefaultApp(const AExeName, AFileExt, AAppDescription: String);
var
  Reg: IisSettings;
begin
  try
    Reg := TisSettingsRegistry.Create(HKEY_CLASSES_ROOT, '');
    Reg.AsString[AFileExt + '\'] := AAppDescription;
    Reg.AsString[AFileExt + '\OpenWithList\' + ExtractFileName(AExeName) +'\'] := ' ';
    Reg.AsString[AFileExt + '\OpenWithList\' + ExtractFileName(AExeName) +'\'] := '';
    Reg.AsString['Applications\' + ExtractFileName(AExeName) + '\shell\open\command\'] := AExeName + ' "%1"';
  except
    // it's done in case if user is not administrator
  end;
end;


function UnixEpochToDateTime(const AUnixEpochTime: Cardinal): TDateTime;
begin
  Result := IncSecond(StrToDateTime('1/1/1970'), AUnixEpochTime);
end;


function DateTimeToUnixEpoch(const ADateTime: TDateTime): Cardinal;
begin
  Result := SecondsBetween(ADateTime, StrToDateTime('1/1/1970'));
end;


function CheckFileExt(const AFileName, AExt: String): Boolean;
begin
  Result := AnsiSameText(ExtractFileExt(AFileName), AExt);
end;

function CheckFileName(const AFileName, AName: String): Boolean;
begin
  Result := AnsiSameText(ExtractFileName(AFileName), AName);
end;

function  CheckFilePath(const AFileName, APath: String): Boolean;
begin
  Result := AnsiSameText(ExtractFilePath(AFileName), APath);
end;

function FileIsUsed(const AFileName: String): Boolean;
var
  Handle: THandle;
begin
  CheckCondition(FileExists(AFileName), 'File does not exist');
  Handle := FileOpen(AFileName, fmOpenReadWrite or fmShareExclusive);
  Result := Handle = INVALID_HANDLE_VALUE;
  if not Result then
    FileClose(Handle);
end;

function WaitIfFileUsed(const AFileName: String; const ASeconds: Integer = -1): Boolean;
const
  WaitInterval = 500;
var
  WaitCycles, i: Integer;
begin
  if ASeconds = -1 then
    WaitCycles := -1
  else
    WaitCycles := (ASeconds * 1000) div WaitInterval;

  i := 0;
  repeat
    Result := not FileIsUsed(AFileName);
    if Result or (WaitCycles = 0) then
      break;
    Snooze(WaitInterval);
    Inc(i);
  until i = WaitCycles;
end;

function GetOSFolder(const AFolderType: TisOSFolderType): string;
const
  CSIDL_PROGRAM_FILESX86        = $002A;
  CSIDL_PROGRAM_FILES_COMMONX86 = $002C;
  CSIDL_Connections             = $0031;
  CSIDL_RESOURCES               = $0038;
  CSIDL_RESOURCES_LOCALIZED     = $0039;
  CSIDL_COMPUTERSNEARME         = $003D;

  Fld: array [Low(TisOSFolderType)..High(TisOSFolderType)] of Integer =
   (CSIDL_DESKTOP,
    CSIDL_INTERNET,
    CSIDL_PROGRAMS,
    CSIDL_CONTROLS,
    CSIDL_PRINTERS,
    CSIDL_PERSONAL,
    CSIDL_BITBUCKET,
    CSIDL_DRIVES,
    CSIDL_NETWORK,
    CSIDL_FONTS,
    CSIDL_HISTORY,
    CSIDL_WINDOWS,
    CSIDL_SYSTEM,
    CSIDL_PROGRAM_FILES,
    CSIDL_PROGRAM_FILESX86,
    CSIDL_PROGRAM_FILES_COMMON,
    CSIDL_PROGRAM_FILES_COMMONX86,
    CSIDL_CONNECTIONS,
    CSIDL_RESOURCES,
    CSIDL_RESOURCES_LOCALIZED,
    CSIDL_COMPUTERSNEARME);
var
  pPath: PAnsiChar;
begin
  GetMem(pPath, MAX_PATH);
  try
    if SHGetFolderPath(0, Fld[AFolderType], 0, 0, pPath) <> S_OK then
      Assert(False, 'GetOSFolder: Cannot find folder ID' + IntToStr(Ord(AFolderType)));
    Result := pPath;
  finally
    FreeMem(pPath);
  end;
end;


function GetOSFolderAllUsers(const AFolderType: TisOSFolderTypeAllUsers): string;
const
  CSIDL_COMMON_TEMPLATES  = $002D;
  CSIDL_COMMON_MUSIC      = $0035;
  CSIDL_COMMON_PICTURES   = $0036;
  CSIDL_COMMON_VIDEO      = $0037;

  Fld: array [Low(TisOSFolderTypeAllUsers)..High(TisOSFolderTypeAllUsers)] of Integer =
   (CSIDL_COMMON_STARTMENU,
    CSIDL_COMMON_PROGRAMS,
    CSIDL_COMMON_STARTUP,
    CSIDL_COMMON_DESKTOPDIRECTORY,
    CSIDL_COMMON_FAVORITES,
    CSIDL_COMMON_APPDATA,
    CSIDL_COMMON_TEMPLATES,
    CSIDL_COMMON_DOCUMENTS,
    CSIDL_COMMON_ADMINTOOLS,
    CSIDL_COMMON_MUSIC,
    CSIDL_COMMON_PICTURES,
    CSIDL_COMMON_VIDEO);
var
  pPath: PAnsiChar;
begin
  GetMem(pPath, MAX_PATH);
  try
    Win32Check(SHGetFolderPath(0, Fld[AFolderType], 0, 0, pPath) = S_OK);
    Result := pPath;
  finally
    FreeMem(pPath);
  end;
end;

function  GetOSFolderUser(const AFolderType: TisOSFolderTypeUser): string;
const
  CSIDL_MYDOCUMENTS = $000C;
  CSIDL_MYMUSIC = $000D;
  CSIDL_MYVIDEO = $000E;

  Fld: array [Low(TisOSFolderTypeUser)..High(TisOSFolderTypeUser)] of Integer =
   (CSIDL_STARTMENU,
    CSIDL_PROGRAMS,
    CSIDL_STARTUP,
    CSIDL_DESKTOPDIRECTORY,
    CSIDL_FAVORITES,
    CSIDL_APPDATA,
    CSIDL_TEMPLATES,
    CSIDL_MYDOCUMENTS,
    CSIDL_ADMINTOOLS,
    CSIDL_MYMUSIC,
    CSIDL_MYPICTURES,
    CSIDL_MYVIDEO,
    CSIDL_RECENT,
    CSIDL_SENDTO,
    CSIDL_NETHOOD,
    CSIDL_PRINTHOOD,
    CSIDL_LOCAL_APPDATA,
    CSIDL_INTERNET,
    CSIDL_COOKIES);
var
  pPath: PAnsiChar;
begin
  GetMem(pPath, MAX_PATH);
  try
    Win32Check(SHGetFolderPath(0, Fld[AFolderType], 0, 0, pPath) = S_OK);
    Result := pPath;
  finally
    FreeMem(pPath);
  end;
end;


function GetTimeMark: Cardinal;
begin
  Result := GetTickCount;
end;


function MilliSecondsSinceTimeMark(const ATimeMark: Cardinal): Cardinal;
var
  CurrTimeMark: Cardinal;
begin
  CurrTimeMark := GetTimeMark;
  if CurrTimeMark >= ATimeMark then
    Result := CurrTimeMark - ATimeMark
  else
    Result := High( Cardinal) - ATimeMark + CurrTimeMark;
end;


function GetParentProcessInfo: TProcessEntry32;
var
   ProcessesSnap: Cardinal;
   ProcInfo: TProcessEntry32;

   function FindProcessInSnapshot(const AProcID: Cardinal): Boolean;
   begin
     Result := False;
     ProcInfo.dwSize := SizeOf(TProcessEntry32);
     Process32First(ProcessesSnap, ProcInfo);
     repeat
       if ProcInfo.th32ProcessID = AProcID then
       begin
         Result := True;
         Break;
       end;
     until (not Process32Next(ProcessesSnap, ProcInfo));
   end;

begin
  ZeroMemory(@Result, SizeOf(Result));

  ProcessesSnap := CreateToolHelp32SnapShot(TH32CS_SNAPALL, 0);
  try
    if FindProcessInSnapshot(GetCurrentProcessId) then
      if FindProcessInSnapshot(ProcInfo.th32ParentProcessID) then
        Result := ProcInfo;
  finally
    CloseHandle(ProcessesSnap);
  end;
end;

function StartedAsService: Boolean;
begin
  Result := AnsiSameText(GetParentProcessInfo.szExeFile, 'services.exe');
end;

function GetServiceStatus(const AServiceName: String): TisServiceStatus;
var
  Manager, Service: Cardinal;
  Status: TServiceStatus;
begin
  Result := ssUnknown;
  Manager := OpenSCManager(nil, nil, SC_MANAGER_CONNECT);
  if Manager <> 0 then
  begin
    Service := OpenService(Manager, PAnsiChar(AServiceName), SERVICE_QUERY_STATUS);
    if Service <> 0 then
    begin
      if QueryServiceStatus(Service, Status) then
      begin
        case Status.dwCurrentState of
          SERVICE_CONTINUE_PENDING: Result := ssContinuePending;
          SERVICE_PAUSE_PENDING:    Result := ssPausePending;
          SERVICE_PAUSED:           Result := ssPaused;
          SERVICE_RUNNING:          Result := ssRunning;
          SERVICE_START_PENDING:    Result := ssStartPending;
          SERVICE_STOP_PENDING:     Result := ssStopPending;
          SERVICE_STOPPED:          Result := ssStopped;
        end;
      end;
      CloseServiceHandle(Service);
    end;
    CloseServiceHandle(Manager);
  end;
end;

function RequestElevation: Boolean;

  function RunAsAdmin(const AWnd: HWND; const AAppFilename, AParams: string): Boolean;
  var
    ExecInfo: TShellExecuteInfo;
  begin
    ZeroMemory(@ExecInfo, SizeOf(ExecInfo));
    ExecInfo.cbSize := SizeOf(TShellExecuteInfo);
    ExecInfo.Wnd := AWnd;
    ExecInfo.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI;
    ExecInfo.lpVerb := PChar('runas');
    ExecInfo.lpFile := PChar(AAppFilename);
    if AParams <> '' then
      ExecInfo.lpParameters := PChar(AParams);
    ExecInfo.nShow := SW_SHOWNORMAL;

    Result := ShellExecuteEx(@ExecInfo);
  end;

var
  Params: String;
  i: Integer;
begin
  Params := '';
  for i := 1 to ParamCount do
    AddStrValue(Params, ParamStr(i), ' ');

  Result := RunAsAdmin(0, AppFileName, Params);
end;

function AppNeedsElevation: Boolean;

  function IsElevated: Boolean;
  const
    SECURITY_BUILTIN_DOMAIN_RID = $20;
    DOMAIN_ALIAS_RID_ADMINS = $220;
  var
    SECURITY_NT_AUTHORITY: TSIDIdentifierAuthority;
    b: BOOL;
    NtAuthority: ^SID_IDENTIFIER_AUTHORITY;
    AdministratorsGroup: PSID;
  begin
    Result := False;
    ZeroMemory(@SECURITY_NT_AUTHORITY, SizeOf(SECURITY_NT_AUTHORITY));
    SECURITY_NT_AUTHORITY.Value[5] := 5;

    AdministratorsGroup := nil;
    NtAuthority := @SECURITY_NT_AUTHORITY;
    b := AllocateAndInitializeSid(
      SECURITY_NT_AUTHORITY,
      2,
      SECURITY_BUILTIN_DOMAIN_RID,
      DOMAIN_ALIAS_RID_ADMINS,
      0, 0, 0, 0, 0, 0,
      AdministratorsGroup);

    if b then
      try
        if CheckTokenMembership(0, AdministratorsGroup, b) then
          Result := b;
      finally
        FreeSid(AdministratorsGroup);
      end;
  end;

  function UACIsActive: Boolean;
  var
    Reg: IisSettings;
    val: Variant;
  begin
    try
      Reg := TisSettingsRegistry.Create(HKEY_LOCAL_MACHINE, 'Software\Microsoft\Windows\CurrentVersion\Policies\System');
      Val := Reg.GetValue('EnableLUA', -1);
      if Val = -1 then
        Result := True
      else
        Result := Val <> 0;
    except
      Result := True;
    end;
  end;

begin
  if (GetOSType >= osWinVista) and UACIsActive then
    Result := not IsElevated
  else
    Result := False;
end;

function GetOSType: TisOSType;
const
  Versions: array [Low(TisOSType)..High(TisOSType)] of String = ('', '5.0', '5.1', '5.2', '5.2', '6.0', '6.0', '6.1', '6.1');
var
  i: TisOSType;
  sVer: String;
begin
  Result := osUnknown;
  sVer := IntToStr(Win32MajorVersion) + '.' + IntToStr(Win32MinorVersion);

  for i := Low(Versions) to High(Versions) do
    if sVer = Versions[i] then
    begin
      Result := i;
      break;
    end;
end;


function isISystemsDomain: Boolean;
type
  WKSTA_INFO_100 = packed record
    wki100_platform_id: DWORD;
    wki100_computername: LPWSTR;
    wki100_langroup: LPWSTR;
    wki100_ver_major: DWORD;
    wki100_ver_minor: DWORD;
  end;
  PWKSTA_INFO_100 = ^WKSTA_INFO_100;
var
  H: THandle;
  NetWkstaGetInfo: function(ServerName: LPWSTR; Level: DWORD; PBufPtr: Pointer): Integer; stdcall;
  NetApiBufferFree: function(BufPtr: Pointer): Integer; stdcall;
  w: PWKSTA_INFO_100;
begin
  H := LoadLibrary('netapi32.dll');
  if H <> 0 then
  try
    w := nil;
    NetWkstaGetInfo := GetProcAddress(H, 'NetWkstaGetInfo');
    Result := Assigned(NetWkstaGetInfo) and
              (NetWkstaGetInfo(nil, 100, @w) = 0) and
              (w^.wki100_langroup = 'ISYSTEMS');
    if Assigned(w) then
    begin
      NetApiBufferFree := GetProcAddress(H, 'NetApiBufferFree');
      NetApiBufferFree(w);
    end;
  finally
    FreeLibrary(H);
  end
  else
    Result := False;

  if not Result then
    Result := SocketLib.LookupHostAddr('email.isystems.lan') = '192.168.2.42';
end;


function isISDevelopment: Boolean;
var
  Reg: IisSettings;
  Items: IisStringListRO;
  i: Integer;
begin
  Result := isISystemsDomain;
  if Result then
  begin
    Result := False;

    Reg := TisSettingsRegistry.Create(HKEY_CURRENT_USER, '');
    Items := Reg.GetValueNames(DecryptString(#1#0#190#112#179#153#168#71#237#110#230#251#188#230#0#204#206#74#78#190#42#35#75#225#167#6#193#250#155#161#130#91#51#156#128#216#113#160#217#109#205#111#180#175#120#186#46#226#236#53)); //Software\Borland\Delphi\7.0\Known Packages

    for i := 0 to Items.Count - 1 do
      if FinishesWith(Items[i], DecryptString(#1#0#40#219#99#10#94#40#9#190#141#56#65#83#179#212#24#69)) then  //isIDEExpert.bpl
      begin
        Result := True;
        break;
      end;
  end;
end;


procedure CheckIfTampered;
var
  res: TExeStatus;
  s: String;
begin
  res := IsExeTampered(True);
  s := '';

  case res of
    exeSizeError,
    exeIntegrityError:
      s := #1#0#238#202#253#86#42#186#3#8#85#245#131#26#11#233#194#172#201#91#126#127#91#129#214#64#210#199#139#101#128#26#145#217; //Binary file was modified: %s
    exeNotStamped:
      if not isISDevelopment then
        s := #1#0#232#207#134#57#95#140#83#221#249#104#18#84#14#22#201#250#157#14#78#118#242#161#118#60#150#24#244#67#198#174#247#122; //Protection code is missing: %s
    exeAccessDenied:
      s := #1#0#125#146#177#50#133#81#186#39#68#71#215#159#174#138#90#65#246#186#25#22#55#27#103#65#138#33#133#198#255#75#48#59; //File is not accessible: %s
  end;

  if s <> '' then
    raise EisSuspiciousBinary.Create(Format(DecryptString(s), [ExtractFileName(ModuleFileName)]));
end;


function LocalDateTimeToUTC(const ADateTime: TDateTime): TUTCDateTime;
begin
  Result := DateTimeToUTC(ADateTime, GetLocalTimeZone.Bias);
end;

function DateToUTC(const ADateTime: TDateTime): TUTCDateTime;
var
	y, m, d: Word;
begin
	DecodeDate(ADateTime, y, m, d);
	Result := Format('%.4d-%.2d-%.2d', [y, m, d]);
end;

function DateTimeToUTC(const ADateTime: TDateTime; const ABiasInMinutes: Integer): TUTCDateTime;
var
	y, m, d, h, n, s, ms: Word;
  ts: Char;
  TZ: String;
  bias: Integer;
begin
  if ABiasInMinutes > 0 then // opposite sign!
  begin
    ts := '-';
    bias := ABiasInMinutes;
  end
  else
  begin
    ts := '+';
    bias := -ABiasInMinutes;
  end;

	DecodeDateTime(ADateTime, y, m, d, h, n, s, ms);

  if bias = 0 then
    TZ  := 'Z'
  else
  	TZ := Format('%s%.2d:%.2d', [ts, bias div 60, bias mod 60]);

	Result := Format('%.4d-%.2d-%.2dT%.2d:%.2d:%.2d%s', [y, m, d, h, n, s, TZ]);
end;

function UTCToDate(const AUTCDate: TUTCDateTime): TDateTime;
var
	y, m, d: Integer;
  utc: String;
begin
  utc := AUTCDate;

  y := StrToIntDef(GetNextStrValue(utc, '-'), 0);
  m := StrToIntDef(GetNextStrValue(utc, '-'), 1);
  d := StrToIntDef(GetNextStrValue(utc, 'T'), 1);

	Result := EncodeDate(y, m, d);
end;

function UTCToDateTime(const AUTCDateTime: TUTCDateTime; out ABiasInMinutes: Integer): TDateTime;
var
	y, m, d, h, n, s, ms, tzh, tzm: Integer;
  ts: Char;
  utc, TZPart: String;
  i: Integer;
begin
  utc := AUTCDateTime;

  ABiasInMinutes := 0;

  y := StrToIntDef(GetNextStrValue(utc, '-'), 0);
  m := StrToIntDef(GetNextStrValue(utc, '-'), 1);
  d := StrToIntDef(GetNextStrValue(utc, 'T'), 1);

  if utc <> '' then
  begin
    if FinishesWith(utc, 'Z', True) then
    begin
      TZPart := '+00:00';
      Delete(utc, Length(utc), 1);
    end
    else
    begin
      i := Pos('+', utc);
      if i = 0 then
        i := Pos('-', utc);

      if i > 0 then
      begin
        TZPart := Copy(utc, i, Length(utc));
        Delete(utc, i, Length(utc));
      end
      else
        TZPart := '';
    end;

    h := StrToIntDef(GetNextStrValue(utc, ':'), 0);
    n := StrToIntDef(GetNextStrValue(utc, ':'), 0);
    s := StrToIntDef(GetNextStrValue(utc, '.'), 0);
    ms := StrToIntDef(utc, 0);

    if TZPart <> '' then
    begin
      ts := TZPart[1];
      tzh := StrToIntDef(Copy(TZPart, 2, 2), 0);
      tzm := StrToIntDef(Copy(TZPart, 5, 2), 0);

      ABiasInMinutes := tzh * 60 + tzm;
      if ts = '+' then // opposite sign!
        ABiasInMinutes := - ABiasInMinutes;
    end;
  end
  else
  begin
    h := 0;
    n := 0;
    s := 0;
    ms := 0;
  end;

	Result := EncodeDateTime(y, m, d, h, n, s, ms);
end;

function GetLocalTimeZone: TTimeZoneInformation;
begin
  if GetTimeZoneInformation(Result) = TIME_ZONE_ID_INVALID then
    RaiseLastOSError;
end;

function UTCToLocalDateTime(const AUTCDateTime: TUTCDateTime): TDateTime;
var
  bias: Integer;
begin
  Result := UTCToDateTime(AUTCDateTime, bias);
  bias := bias - GetLocalTimeZone.Bias;
  if bias <> 0 then
    Result := IncMinute(Result, bias);
end;

function UTCToGMTDateTime(const AUTCDateTime: TUTCDateTime): TDateTime;
var
  bias: Integer;
begin
  Result := UTCToDateTime(AUTCDateTime, bias);
  if bias <> 0 then
    Result := IncMinute(Result, bias);
end;

function NowUTC: TUTCDateTime;
begin
  Result := LocalDateTimeToUTC(Now);
end;

procedure RaiseLastOSErrorEx(const Mess: String);
begin
  try
    RaiseLastOSError();
  except on E:Exception do
    begin
      E.Message := E.Message+'. '+#13#10+Mess;
      raise;
    end;
  end;
end;

procedure SysLogAddEvent(const Msg: string; const Context: Integer; const MsgType: Word = EVENTLOG_ERROR_TYPE);
var
  h: THandle;
  P: Pointer;
begin
  h := OpenEventLog(nil, PChar(ModuleFileName));
  try
    P := PChar(Msg + #0);
    ReportEvent(h, MsgType, 0, Context, nil, 1, 0, @P, nil);
  finally
    CloseEventLog(h);
  end;
end;

procedure SysLogAddErrorEvent(const Msg: string; const Context: Integer);
begin
  SysLogAddEvent(Msg, Context, EVENTLOG_ERROR_TYPE);
end;

procedure SysLogAddInfoEvent(const Msg: string; const Context: Integer);
begin
  SysLogAddEvent(Msg, Context, EVENTLOG_INFORMATION_TYPE);
end;


procedure SysLogAddWarningEvent(const Msg: string; const Context: Integer);
begin
  SysLogAddEvent(Msg, Context, EVENTLOG_WARNING_TYPE);
end;


function GetBinaryResource(const AResourceName: String; AResourceType: String; const AStream: IisStream): IisStream;
var
  HR: HRSRC;
  SR: Cardinal;
  PR: Pointer;
  t: PAnsiChar;
begin
  Result := nil;

  if (AResourceType <> '') and (AResourceType[1] > ' ') then
    t := PAnsiChar(AResourceType)
  else
    t := PAnsiChar(Pointer(Integer(AResourceType[1]))); // for standard types like RT_BITMAP

  HR := FindResource(HINSTANCE, PAnsiChar(AResourceName), t);
  if HR <> 0 then
  begin
    SR := SizeofResource(HINSTANCE, HR);
    if SR > 0 then
    begin
      PR := Pointer(LoadResource(HINSTANCE, HR));
      if Assigned(AStream) then
      begin
        Result := AStream;
        Result.Size := 0;
      end
      else
        Result := TisStream.Create(SR);
      Result.WriteBuffer(PR^, SR);
    end;
  end;
end;


function GetResourceList(const AResourceType: String): IisStringList;

  function EnumResNamesProc(hModule: HMODULE; lpType, lpName: PChar; lParam: Longint): Integer; stdcall;
  begin
    IisStringList(Pointer(lParam)).Add(String(lpName));
    Result := 1;
  end;

begin
  Result := TisStringList.Create;
  EnumResourceNames(HINSTANCE, PAnsiChar(AResourceType), @EnumResNamesProc, Longint(Pointer(Result)));
end;

function GetResourceTypes: IisStringList;

  function EnumResTypesProc(hModule: HMODULE; lpType: PChar; lParam: Longint): Integer; stdcall;
  begin
    IisStringList(Pointer(lParam)).Add(String(lpType));
    Result := 1;
  end;

begin
  Result := TisStringList.Create;
  EnumResourceTypes(HINSTANCE, @EnumResTypesProc, Longint(Pointer(Result)));
end;

function MakeValidComponentName(const AComponentName: string): string;
var
  s: String;
begin
  s := AComponentName;
  Result := '';
  while s <> '' do
    Result := Result + GetNextTextValue(s);

  if (Result <> '') and (Result[1] in NumberChars) then
    Result := 'C' + Result;
end;

function TextIsVisiallyEmpty(const AText: String): Boolean;
var
  s: String;
begin
  s := StringReplace(AText, #9, '', [rfReplaceAll]); // Tab
  s := StringReplace(s, #10, '', [rfReplaceAll]);  // LF
  s := StringReplace(s, #13, '', [rfReplaceAll]); // CR
  s := StringReplace(s, ' ', '', [rfReplaceAll]); // Spaces

  Result := Length(s) = 0;
end;

function IIF(const AExpression: Boolean; const AThen: Variant; const AElse: Variant): Variant;
begin
  if AExpression then
    Result := AThen
  else
    Result := AElse;
end;

function ReadDelimitedText(const AText, ADelimiter: String): TisDynStringArray;
var
  s: String;
  i: Integer;
begin
  s := AText;

  i := -1;
  while s <> '' do
  begin
    if High(Result) = i then
      SetLength(Result, Length(Result) + 100);

    Inc(i);
    Result[i] := GetNextStrValue(s, ADelimiter);
  end;

  SetLength(Result, i + 1);
end;

initialization
  FAppID := 'UNDEFINED';
  SetAppTempFolder('');
  SetAppDataFolder('');
  AppSwitches;

end.
