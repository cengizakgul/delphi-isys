unit isTestQueueManagerHolderFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, isTestQueueManager, istestutils;

type
  TisTestQueueManagerHolder = class(TForm, IisTestEngineSupportControl)
    QueueManager: TisTestQueueManagerFrame;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function FindQueueManagerForm: TisTestQueueManagerHolder;
function GetQueueManagerForm: TisTestQueueManagerHolder;

implementation

{$R *.dfm}

function FindQueueManagerForm: TisTestQueueManagerHolder;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to Screen.FormCount-1 do
    if Screen.Forms[i] is TisTestQueueManagerHolder then
    begin
      Result := Screen.Forms[i] as TisTestQueueManagerHolder;
      break;
    end;
end;

function GetQueueManagerForm: TisTestQueueManagerHolder;
begin
  Result := FindQueueManagerForm;
  if Result = nil then
    Result := TisTestQueueManagerHolder.Create(nil);
end;


procedure TisTestQueueManagerHolder.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := not Visible or QueueManager.CloseQuery;
end;

end.
